<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libwebsamsdata.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

include_once("../../../includes/libwebsamsfunction.php");

intranet_opendb();

if ($input_userlogin != "" && $input_password != "")
{
    $websams_username = $input_userlogin;
    $websams_password = $input_password;
}
$lwebsamsdata = new libwebsamsdata();
$data = $lwebsamsdata->geteClassAttendanceData($startdate,$enddate);
?>

<?php
# Execute WebSAMS

/*
echo         "Received 'lwebsamsdata' : <br>" .
                        "size = " . sizeOf($data) . "<br>" .
                        "Date start <br>";

for ( $i=0; $i<sizeOf($data); $i++){
        for ($j=0; $j<sizeOf($data[$i]); $j++)
                echo $data[$i][$j] . " " ;
        echo "<br>";
}
echo        "Date end <br><br>";
*/


                        $websamsToEclass = new libwebsamsfunction($websams_host, $websams_port);

                        $returnLoginResult = $websamsToEclass->loginToWebsams( $websams_username, $websams_password );

						

                        /*
                        #display login result
                        if ( $returnLoginResult["succeed"] )
                                echo $returnLoginResult["succeed"] . "<br>";
                        else if ( $returnLoginResult["fail"] )
                                echo $returnLoginResult["fail"] . "<br>";
                        */

                        $selectedStudentRecord;

                        $allUpdateResult;
                        $succeedUpdatedIndex;
                        $failUpdatedIndex;

                        //test sample for unsucceeful update record
                        /*
                        $test = sizeOf($data);
                        $data[$test][1] = "2A";
                        $data[$test][2] = "3";
                        $data[$test][4] =  "WD";
                        $data[$test][3] = "LEAVE";
                        $data[$test][5] = "04";
                        $data[$test][0]= "21/03/2004";

                        $test = sizeOf($data);
                        $data[$test][1] = "8A";
                        $data[$test][2] = "3";
                        $data[$test][4] =  "WD";
                        $data[$test][3] = "LEAVE";
                        $data[$test][5] = "04";
                        $data[$test][0]= "21/03/2003";
                        */
                        //end of sample

                        for ( $i=0; $i<sizeOf($data); $i++){

                                        $selectedStudentRecord["attendDate"] = $data[$i][0];
                                        $selectedStudentRecord["classCode"] = $data[$i][1];
                                        $selectedStudentRecord["classNo"] = $data[$i][2];
                                        $selectedStudentRecord["attendType"] = $data[$i][3];
                                        $selectedStudentRecord["attendSession"] =  $data[$i][4];
                                        $selectedStudentRecord["attendReason"] = $data[$i][5];

                                        $selectedStudentRecord["classLevel"] = "S" . substr( $selectedStudentRecord["classCode"], 0, 1);

                                        $returnUpdateResult = $websamsToEclass->updateOneStudent( $selectedStudentRecord );

                                        $allUpdateResult[] = $returnUpdateResult;

                                        if ( $returnUpdateResult["succeed"] <> "" )
                                                $succeedUpdatedIndex[] = $i;

                                        if ( $returnUpdateResult["error"] <> "" )
                                                $failUpdatedIndex[] = $i;


                                        /*
                                        if ( $returnUpdateResult["succeed"] <> "" )
                                                echo $returnUpdateResult["succeed"] . "<br>";
                                        else
                                                echo "No succeed message!<br>";

                                        if ( $returnUpdateResult["error"] <> "" )
                                                echo $returnUpdateResult["error"];
                                        else
                                                echo "No error message!<br>";

                                        if ( $returnUpdateResult["detail"] <> "" )
                                                echo $returnUpdateResult["detail"] ;
                                        else
                                                echo "No student detail!<br>";
                                        */

                        }

                        /*
                        #display update result
                        $displayResult = "";
                        $displayResult .= "Total number of update request = " . sizeOf($data) . "<br>";
                        $displayResult .= "Number of successful update = " . sizeOf($succeedUpdatedIndex) . "<br>";
                        $displayResult .= "Number of unsuccessful update = " . sizeOf($failUpdatedIndex) . "<br>";

                        echo $displayResult;

                        /*
                        #display error while updating
                        if ( sizeOf($failUpdatedIndex) > 0 ){

                                $displayResult = "<br>Details as follows : <br>";

                                for ( $i=0; $i<sizeOf($failUpdatedIndex); $i++ ){

                                        $displayResult .= ($i+1) . ": " ;


                                        #$displayResult .= $data[$failUpdatedIndex[$i]][0] . " " .
                                        #                                                                $data[$failUpdatedIndex[$i]][1] . " " .
                                        #                                                                $data[$failUpdatedIndex[$i]][2] . " " .
                                        #                                                                $data[$failUpdatedIndex[$i]][3] . " " .
                                        #                                                                $data[$failUpdatedIndex[$i]][4] . " " .
                                        #                                                                $data[$failUpdatedIndex[$i]][5] . "<br>" ;


                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["detail"] ["attendDate"] . " ";
                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["detail"] ["classCode"] . " ";
                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["detail"] ["classNo"] . " ";
                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["detail"] ["attendType"] . " ";
                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["detail"] ["attendSession"] . " ";
                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["detail"] ["attendReason"] . "<br>";

                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["error"] . "<br><br>";

                                }

                        echo $displayResult;
                        }
                        */

                        $returnCloseResult = $websamsToEclass->loginOutWebsams();

                        /*
                        #display logout result
                        if ( $returnCloseResult["succeed"] )
                                echo $returnCloseResult["succeed"] . "<br>";
                        else if ($returnCloseResult["fail"] )
                                echo $returnCloseResult["fail"] . "<br>";
                        */

# account:
?>



<?= displayNavTitle($i_adminmenu_plugin, '', $i_WebSAMS_Integration, '../',$i_WebSAMS_Menu_Attendance,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<font color=green><?=$i_WebSAMS_con_RecordSynchronized?></font>
<p></p>
<?=$display?>

<script language="JavaScript" type="text/javascript">
<!--

// close status window
parent.intranet_admin_menu.runStatusWin("");
-->
</script>


<?
        // display login result
                        if ( $returnLoginResult["succeed"] )
                                echo $returnLoginResult["succeed"] . "<br>";
                        else if ( $returnLoginResult["fail"] )
                                echo $returnLoginResult["fail"] . "<br>";

        // display update result
                        $updateResult = "";
                        $updateResult .= "Total number of update request = " . sizeOf($data) . "<br>";
                        $updateResult .= "Number of successful update = " . sizeOf($succeedUpdatedIndex) . "<br>";
                        $updateResult .= "Number of unsuccessful update = " . sizeOf($failUpdatedIndex) . "<br>";

                        echo $updateResult;

        // display error while updating
                        if ( sizeOf($failUpdatedIndex) > 0 ){

                                $displayResult = "<br>Details as follows : <br>";

                                for ( $i=0; $i<sizeOf($failUpdatedIndex); $i++ ){

                                        $displayResult .= ($i+1) . ": " ;

                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["detail"] ["attendDate"] . " ";
                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["detail"] ["classCode"] . " ";
                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["detail"] ["classNo"] . " ";
                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["detail"] ["attendType"] . " ";
                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["detail"] ["attendSession"] . " ";
                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["detail"] ["attendReason"] . "<br>";

                                        $displayResult .= $allUpdateResult[$failUpdatedIndex[$i]]["error"] . "<br><br>";

                                }

                                echo $displayResult;
                        }

        // display logout result
                        if ( $returnCloseResult["succeed"] )
                                echo $returnCloseResult["succeed"] . "<br>";
                        else if ($returnCloseResult["fail"] )
                                echo $returnCloseResult["fail"] . "<br>";



?>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>
