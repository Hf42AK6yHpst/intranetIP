<?php

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();

$default_reason_type=1;

$li = new libdb();
	# get codeID for other reasons
	$sql ="SELECT ReasonType,CodeID ,ReasonText
		FROM INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE WHERE RecordType=1 ORDER By ReasonType";
	$r = $li->returnArray($sql,3);
	for($i=0;$i<sizeof($r);$i++){
		list($reason_type,$code_id,$reason_text)=$r[$i];
		if($reason_type==$default_reason_type){
			$current = $code_id." - ".$reason_text;
			break;
		}else{
			$current = "  ";
		}
	}
//	$current = $r[0][1]." - ".$r[0][2];
$select_reason = "<SELECT name=\"reasonType\" onChange='changeText(this)'>\n";
$select_reason.="<OPTION VALUE='1'>$i_Profile_Absent</OPTION>\n";
$select_reason.="<OPTION VALUE='2'>$i_Profile_Late</OPTION>\n";
$select_reason.="<OPTION VALUE='3'>$i_Profile_EarlyLeave</OPTION>\n";
$select_reason.="</SELECT>\n";
?>
<script language='javascript'>
other = new Array();
<?php
	for($i=0;$i<sizeof($r);$i++){
		list($type,$code,$text) = $r[$i];
		echo "other[$type]=\"$code - $text\";\n";
	}
?>
function changeText(obj){
	if(obj==null) return;
	index = obj.options[obj.selectedIndex].value;
	objCurrent = document.getElementById('current');
	if(objCurrent!=null){
		if(typeof(other[index])!='undefined')
			objCurrent.innerHTML = other[index];
		else objCurrent.innerHTML = '';
	}
	
}
</script>
<form name="form1" method="get" action="new_update.php">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_WebSAMS_Integration, '../../attendance_code/index.php', $i_WebSAMS_AttendanceCode_ReasonCode,'index.php',$button_new,'') ?>
<?= displayTag("head_websams_$intranet_session_language.gif", $msg) ?>
<br>
<blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=right><?=$i_WebSAMS_Attendance_Reason_ReasonCode?>: </td><td><input type="text" maxlength="3" name="codeID" size="3"></td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td align=right><?=$i_WebSAMS_Attendance_Reason_ReasonText?>: </td><td><input type="text" name="reasonText" size=50></td></tr>
<tr><td align=right></td><Td>
	<table border=0 cellpadding=0 cellspacing=0><tr><td>
		<input type="checkbox" name="otherReason" value="1"></td><td><?=$i_WebSAMS_Attendance_Reason_OtherReason_Notice?>.</td></tr>
		<tr><Td>&nbsp;</td><td><font color=red><?=$i_WebSAMS_Attendance_Reason_CurrenlyMapTo?>: <span id='current'><?=$current?></span></font></td></tr>
	</table>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td align=right><?=$i_WebSAMS_Attendance_Reason_ReasonType?>: </td><td><?=$select_reason?></td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
