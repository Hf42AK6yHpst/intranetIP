<?php
#################################
#
#	Date:	2012-10-04	YatWoon
#			Fixed: incorrect sql statement so that cannot display the reason type.  should be check with field "ReasonType", not "RecordType". [Case#2012-1003-1111-51132]
#
#################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$li= new libdb();
$lexport = new libexporttext();

$keyword = urldecode(trim($keyword));

if ($field == "") $field = 1;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;

$order_str=$order==1?" ASC ":" DESC ";

$field_array = array("CodeID","ReasonText","ReasonType");

$orderby = " ORDER BY ".$field_array[$field].$order_str;

$sql = "SELECT CodeID, 
			   ReasonText,
			   IF(ReasonType=1,'$i_Profile_Absent',IF(ReasonType=2,'$i_Profile_Late',IF(ReasonType=3,'$i_Profile_EarlyLeave',' ')))
 			   FROM INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE WHERE (ReasonText LIKE '%$keyword%' 
 			   	OR CodeID LIKE '%$keyword%')
 	   ";
$conds = $record_type ==""?"":" AND ReasonType='$record_type' ";

$sql.=$conds;
$sql.=$orderby;

$result = $li->returnArray($sql,3);

$csv = "\"$i_WebSAMS_Attendance_Reason_ReasonCode\",\"$i_WebSAMS_Attendance_Reason_ReasonText\",\"$i_WebSAMS_Attendance_Reason_ReasonType\"\n";
$utf_csv = $i_WebSAMS_Attendance_Reason_ReasonCode."\t".$i_WebSAMS_Attendance_Reason_ReasonText."\t".$i_WebSAMS_Attendance_Reason_ReasonType."\r\n";
for($i=0;$i<sizeof($result);$i++){
	list($code_id,$reason_text,$reason_type) = $result[$i];
	$reason_text = intranet_undo_htmlspecialchars($reason_text);
	$csv.="\"$code_id\",\"$reason_text\",\"$reason_type\"\n";
	$utf_csv.=$code_id."\t".$reason_text."\t".$reason_type."\r\n";
}
if(sizeof($result)<=0){
	$csv .="\"".$i_no_record_exists_msg."\"\n";
	$utf_csv .=$i_no_record_exists_msg."\r\n";
}
intranet_closedb();
$t = time();

//if (!$g_encoding_unicode) {	
	//output2browser($csv,"WebSAMS_ReasonCode_$t.csv");
//} else {
	$filename = "WebSAMS_ReasonCode_$t.csv";
	$lexport->EXPORT_FILE($filename, $utf_csv);
//}


?>