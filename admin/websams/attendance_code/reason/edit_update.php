<?php

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();

$li = new libdb();
$reasonText = trim(htmlspecialchars_decode(stripslashes($reasonText),ENT_QUOTES));

if($otherReason==1 && $reasonType!=""){
	$sql = "UPDATE INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE SET RecordType=0 WHERE ReasonType='$reasonType'";
	$li->db_db_query($sql);
}
$sql="UPDATE INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE SET CodeID='$codeID', ReasonText='".$li->Get_Safe_Sql_Query($reasonText)."', ReasonType='$reasonType', RecordType='$otherReason', DateModified=NOW() WHERE RecordID='$recordID'";
$li->db_db_query($sql);

intranet_closedb();
header("Location: edit.php?ReasonRecordID[]=$recordID&msg=2");
?>
