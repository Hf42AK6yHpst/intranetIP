<?php
# using: yat

##################################
#
#	Date:	2011-06-03	YatWoon
#			Hidden chinese sample (#2011-0527-1258-48071, requested by CS)
#
##################################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();
$limport = new libimporttext();
$linterface = new interface_html();

?>

<form name="form1" action="import_update.php" enctype="multipart/form-data" method="post">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_WebSAMS_Integration, '../../attendance_code/index.php', $i_WebSAMS_AttendanceCode_ReasonCode,'index.php',$button_import,'') ?>
<?= displayTag("head_websams_$intranet_session_language.gif", $msg) ?>

<blockquote>
<?= $i_select_file ?>: <input type=file name=classfile><br><?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>

<blockquote>
<?php
		echo $i_WebSAMS_ReasonCode_ImportInstruction;
		$sample_file= GET_CSV("attend_reason_eng_csv.csv");
		//$sample_file2= GET_CSV("attend_reason_chi_csv.csv");
		
?>
</blockquote>
<p><a class=functionlink_new href="<?=$sample_file?>"><?=$i_Class_DownloadSample?></a></p>
<? /* ?>
 (<?=$i_general_english?>)
 <p><a class=functionlink_new href="<?=$sample_file2?>"><?=$i_Class_DownloadSample?> (<?=$i_general_chinese?>)</a></p><? */ ?>
</blockquote>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>