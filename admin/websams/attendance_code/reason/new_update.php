<?php

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();

$li = new libdb();

$reasonText = trim(htmlspecialchars_decode(stripslashes($reasonText),ENT_QUOTES));
if($otherReason==1 && $reasonType!=""){
	$sql = "UPDATE INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE SET RecordType=0 WHERE ReasonType='$reasonType'";
	$li->db_db_query($sql);
}
 
$sql="INSERT INTO INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE (CodeID, ReasonText, ReasonType,RecordType,DateInput,DateModified) VALUES ('$codeID','".$li->Get_Safe_Sql_Query($reasonText)."','$reasonType','$otherReason',NOW(),NOW())";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?msg=1");
?>
