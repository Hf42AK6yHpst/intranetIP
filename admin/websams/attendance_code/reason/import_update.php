<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$limport = new libimporttext();

$li = new libdb();
$lo = new libfilesystem();

$filepath = $classfile;
$filename = $classfile_name;


$file_format_en = array("CODE_ID","EN_DES","ReasonType");
$file_format_ch = array("CODE_ID","CH_DES","ReasonType");

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php");
} else {
        $ext = strtoupper($lo->file_ext($filename));
        if($limport->CHECK_FILE_EXT($filename)) {
                # read file into array
                # return 0 if fail, return csv array if success
                //$data = $lo->file_read_csv($filepath);
                $data = $limport->GET_IMPORT_TXT($filepath);
                $title_row = $data[0];
                $formatCorrect = true;
                for ($i=0; $i<sizeof($title_row); $i++)
                {

                     if (trim($title_row[$i])!=$file_format_en[$i] && trim($title_row[$i])!=$file_format_ch[$i])
                     {
	                     
                         $formatCorrect = false;
                         break;
                     }
                }

        }
        if ($formatCorrect)
        {
            array_shift($data);                   # drop the title bar
            # Process import
            $delimiter = "";
            $toValues = "";
            for ($i=0; $i<sizeof($data); $i++)
            {
               	 list($code_id,$reason_text,$reason_type) = $data[$i];
                 $code_id = trim($code_id);
                 $reason_text = trim(htmlspecialchars_decode(stripslashes($reason_text),ENT_QUOTES));
                 #$reason_text = trim($reason_text);
                 $reason_type = trim($reason_type);
                 
                 if ($code_id!="" && $reason_text!="" && $reason_type!="")
                 {
                     $toValues .= "$delimiter ('$code_id','$reason_type','".$li->Get_Safe_Sql_Query($reason_text)."',NOW(),NOW())";
                     $delimiter = ",";
                 }
            }
            # Insert 
            $sql = "INSERT IGNORE INTO INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE (CodeID,ReasonType,ReasonText,DateInput,DateModified) VALUES $toValues";
            $li->db_db_query($sql);

        }
        else
        {
            include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
			echo displayNavTitle($i_adminmenu_plugin, '', $i_WebSAMS_Integration, '../../index.php', $i_WebSAMS_Menu_AttendanceCode, '../index.php',$i_WebSAMS_AttendanceCode_ReasonCode,'index.php',$button_import,'') ;
			echo displayTag("head_websams_$intranet_session_language.gif", $msg);
            echo "<blockquote><br>$i_import_invalid_format <br>\n";
            echo "<table width=100 border=1>\n";
            for ($i=0; $i<sizeof($file_format_en); $i++)
            {
                 echo "<tr><td>".$file_format_en[$i]."</td></tr>\n";
            }
            echo "</table><br>";
            echo "<table border=0 width=100><tr><td align=center>".$i_alert_or."</td></tr></table><br>\n";
            echo "<table width=100 border=1>\n";
  	        for ($i=0; $i<sizeof($file_format_ch); $i++)
            {
                 echo "<tr><td>".$file_format_ch[$i]."</td></tr>\n";
            }
  
            echo "</table></blockquote>\n";
            
                        echo "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                                        <tr><td><hr size=1></td></tr>
                                        <tr><td align='right'><a href='javascript:history.go(-2)'><img src='/images/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0'></a></td></tr></table>";


            //echo $li->queries;
            include_once($PATH_WRT_ROOT."templates/adminfooter.php");
        }

}
intranet_closedb();
header("Location: import.php?msg=1");
?>