<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();
if(sizeof($ReasonRecordID)>0){
	$li = new libdb();
	$record_id_list = implode(",",$ReasonRecordID);
	$sql = "DELETE FROM INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE WHERE RecordID IN ($record_id_list)";
	$li->db_db_query($sql);
	intranet_closedb();
	header("Location: index.php?msg=3");
}
intranet_closedb();
header("Location: index.php");
?>