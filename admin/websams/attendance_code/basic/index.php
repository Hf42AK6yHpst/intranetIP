<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libwebsamsattendcode.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lwebsams_code = new libwebsamsattendcode();
$lwebsams_code->retrieveBasicInfo();

$select_level = "<SELECT name=SchoolLevel>
<OPTION value=1 ".($lwebsams_code->SchoolLevel==1?"SELECTED":"").">$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Kindergarten</OPTION>
<OPTION value=2 ".($lwebsams_code->SchoolLevel==2?"SELECTED":"").">$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Primary</OPTION>
<OPTION value=3 ".($lwebsams_code->SchoolLevel==3?"SELECTED":"").">$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Secondary</OPTION>
<OPTION value=9 ".($lwebsams_code->SchoolLevel==9?"SELECTED":"").">$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Others</OPTION>
<OPTION value=-1 ".($lwebsams_code->SchoolLevel==-1?"SELECTED":"").">$i_WebSAMS_AttendanceCode_Option_SchoolLevel_DependOnClassLevel</OPTION>
</SELECT>
";

$select_session = "<SELECT name=SchoolSession>
<OPTION value=1 ".($lwebsams_code->SchoolSession==1?"SELECTED":"").">$i_WebSAMS_AttendanceCode_Option_SchoolSession_AM</OPTION>
<OPTION value=2 ".($lwebsams_code->SchoolSession==2?"SELECTED":"").">$i_WebSAMS_AttendanceCode_Option_SchoolSession_PM</OPTION>
<OPTION value=3 ".($lwebsams_code->SchoolSession==3?"SELECTED":"").">$i_WebSAMS_AttendanceCode_Option_SchoolSession_WD</OPTION>
<OPTION value=4 ".($lwebsams_code->SchoolSession==4?"SELECTED":"").">$i_WebSAMS_AttendanceCode_Option_SchoolSession_Evening</OPTION>
</SELECT>
";


?>


<?= displayNavTitle($i_adminmenu_plugin, '', $i_WebSAMS_Integration, '../../attendance_code/index.php', $i_WebSAMS_AttendanceCode_Basic,'') ?>
<?= displayTag("head_websams_$intranet_session_language.gif", $msg) ?>
<form name="form1" action="update.php" method="post">

<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td width=150 align=right><?php echo $i_WebSAMS_AttendanceCode_SchoolID; ?>:</td><td><input type=text name=SchoolID size=10 value="<?=$lwebsams_code->SchoolID?>"></td></tr>
<tr><td width=150 align=right><?php echo $i_WebSAMS_AttendanceCode_SchoolYear; ?>:</td><td><input type=text name=SchoolYear size=10 value="<?=$lwebsams_code->SchoolYear?>"></td></tr>
<tr><td width=150 align=right><?php echo $i_WebSAMS_AttendanceCode_SchoolLevel; ?>:</td><td><?=$select_level?></td></tr>
<tr><td width=150 align=right><?php echo $i_WebSAMS_AttendanceCode_SchoolSession; ?>:</td><td><?=$select_session?></td></tr>

</table>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>


</form>
<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>