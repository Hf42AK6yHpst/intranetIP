<?php
## using by : yat

################ Change Log [Start] #######################
# - 2010-08-27 YatWoon
#	hide Title
# -	2010-02-27 Ivan
#	If the system is using eEnrol and have the term-based club enhancement, hide the all ECA groups in the bottom selection
# -	2009-12-08 YatWoon
#	If the student is "Left", then cannot udpate the status of student
################ Change Log [End] #######################

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsystemaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");

include_once("$eclass_filepath/src/includes/php/lib-portfolio.php");
intranet_opendb();

$li = new libuser($UserID[0]);
$TabID = $li->RecordType;

$iportfolio_activated = false;

if($plugin['iPortfolio']){
	$lportfolio = new portfolio();
	$data = $lportfolio->returnAllActivatedStudent();
	if($TabID==2){
		for($i=0;$i<sizeof($data);$i++){
			if($UserID[0]==$data[$i][0]){
				$iportfolio_activated = true;
				break;
			}
		}
	}
}
/*
echo "<!---";
print_r($data);
echo "--->";
*/
if ($special_feature['alumni'])
{
    $i_identity_array[4] = $i_identity_alumni;
}


$lo = new libgrouping();
// $Title0 = ($li->Title==0 && strlen($li->Title)>0 ) ? "CHECKED" : "";
// $Title1 = ($li->Title==1) ? "CHECKED" : "";
// $Title2 = ($li->Title==2) ? "CHECKED" : "";
// $Title3 = ($li->Title==3) ? "CHECKED" : "";
// $Title4 = ($li->Title==4) ? "CHECKED" : "";
// $Title5 = ($li->Title==5) ? "CHECKED" : "";
$Gender0 = ($li->Gender=="M") ? "CHECKED" : "";
$Gender1 = ($li->Gender=="F") ? "CHECKED" : "";
$RecordStatus0 = ($li->RecordStatus==0) ? "CHECKED" : "";
$RecordStatus1 = ($li->RecordStatus==1) ? "CHECKED" : "";
$RecordStatus2 = ($li->RecordStatus==2) ? "CHECKED" : "";

$lclass = new libclass();
$selection = $lclass->getSelectClass("onChange=\"this.form.ClassName.value=this.value\"",$li->ClassName);
if ($li->RecordType == 1)
{
    if ($li->teaching == 1)
    {
        $tcheck_str = "CHECKED";
        $ncheck_str = "";
    }
    else
    {
        $ncheck_str = "CHECKED";
        $tcheck_str = "";
    }
    $teachingStr = "<tr><td align=right nowrap></td><td><input type=radio name=teaching value=1 $tcheck_str>$i_teachingStaff <input type=radio name=teaching value=0 $ncheck_str>$i_nonteachingStaff \n</td>\n";
}
else
{
    $teachingStr = "";
}



if ($TabID == 4)
{
    $sql = "SELECT YearOfLeft FROM INTRANET_USER WHERE UserID = '".IntegerSafe($UserID[0])."'";
    $temp = $li->returnVector($sql);
    $YearOfLeft = $temp[0];
}
if ($TabID==1)
{

    if ($intranet_session_language!="gb")
    {
        $chi_title_array = array("$i_general_Principal","$i_general_VPrincipal","$i_general_Director");
    }
    else
    {
        $chi_title_array = array();
    }
    $sql = "SELECT DISTINCT TitleChinese FROM INTRANET_USER WHERE TitleChinese IS NOT NULL AND TitleChinese !='' ORDER BY TitleChinese";
    $temp = $li->returnVector($sql);
    if (sizeof($temp)!=0)
    {
        $chi_title_array = array_merge($chi_title_array, $temp);
        $chi_title_array = array_unique($chi_title_array);
        $chi_title_array = array_values($chi_title_array);
    }
    $sql = "SELECT DISTINCT TitleEnglish FROM INTRANET_USER WHERE TitleEnglish IS NOT NULL AND TitleEnglish !='' ORDER BY TitleEnglish";
    $eng_title_array = $li->returnVector($sql);
    if (sizeof($eng_title_array)!=0)
    {
        $select_eng_title = getSelectByValue($eng_title_array, "onChange=this.form.TitleEnglish.value=this.value",$li->TitleEnglish);
    }
    else
    {
        $select_eng_title = "";
    }
    $select_chi_title = getSelectByValue($chi_title_array, "onChange=this.form.TitleChinese.value=this.value",$li->TitleChinese);
}

?>

<script language="javascript">
function checkform(obj){
     if(obj.ChineseName.value == "")
     {
        if(!check_text(obj.EnglishName, "<?php echo $i_alert_pleasefillin.$i_UserEnglishName; ?>.")) return false;
     }
//     if(!check_text(obj.LastName, "<?php echo $i_alert_pleasefillin.$i_UserLastName; ?>.")) return false;
//     if(!check_text(obj.FirstName, "<?php echo $i_alert_pleasefillin.$i_UserFirstName; ?>.")) return false;
//     if(!checkIdentity(obj, "IdentityGroupID[]")) return false;
     <? if ($TabID != 4) { ?>
     checkOptionAll(obj.elements["GroupID[]"]);
     <? } ?>
     <? if ($TabID == 3) { ?>
     checkOptionAll(obj.elements["child[]"]);
     <? } ?>

}
</script>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, 'javascript:history.back()', $button_edit, '') ?>
<?= displayTag("head_user_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_UserLogin; ?>:</td><td class=td_left_middle><?php echo $li->UserLogin;?>
<? if ($special_option['change_login']) {
?>
<a href="javascript:newWindow('changelogin.php?uid=<?=$UserID[0]?>',2)"><img src="/images/admin/button/s_btn_edit_<?=$intranet_session_language?>.gif" border="0" align=absmiddle></a>
<?
}
?>
</td></tr>
<tr><td align=right nowrap><?php echo $i_UserEmail; ?>:</td><td><?php echo $li->UserEmail;?></td></tr>
<tr><td align=right nowrap><?php echo $i_UserPassword; ?>:</td><td><a href="javascript:newWindow('password.php?uid=<?=$UserID[0]?>',2)"><img src="/images/admin/button/s_btn_reset_password_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
<tr><td align=right nowrap><?php echo $i_UserEnglishName; ?>:</td><td><input class=text type=text name=EnglishName size=20 maxlength=100 value="<?php echo $li->EnglishName;?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_UserChineseName; ?>:</td><td><input class=text type=text name=ChineseName size=20 maxlength=100 value="<?php echo $li->ChineseName;?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_UserRecordStatus; ?>:</td><td>
<? if($li->RecordStatus==3) {?>
<?=$i_status_graduate?><input type=hidden name=RecordStatus value=3>
<? } else {?>
<input type=radio name=RecordStatus value=1 <?php echo $RecordStatus1; ?>> <?php echo $i_status_approve; ?> <input type=radio name=RecordStatus value=0 <?php echo $RecordStatus0; ?>> <?php echo $i_status_pendinguser; ?>
<? } ?>
</td></tr>
<tr><td align=right nowrap><?php echo $i_identity; ?>:</td><td><?=$i_identity_array[$li->RecordType]?></td></tr>
<?=$teachingStr?>
<tr><td colspan=2><br></td></tr>
<tr><td align=right nowrap><?php echo $i_UserNickName; ?>:</td><td><input class=text type=text name=NickName size=20 maxlength=100 value="<?php echo $li->NickName;?>"></td></tr>
<?/*?><tr><td align=right nowrap><?php echo $i_UserTitle; ?>:</td><td><input type=radio name=Title value=0 <?php echo $Title0; ?>> <?php echo $i_title_mr; ?> <input type=radio name=Title value=1 <?php echo $Title1; ?>> <?php echo $i_title_miss; ?> <input type=radio name=Title value=2 <?php echo $Title2; ?>> <?php echo $i_title_mrs; ?> <input type=radio name=Title value=3 <?php echo $Title3; ?>> <?php echo $i_title_ms; ?> <input type=radio name=Title value=4 <?php echo $Title4; ?>> <?php echo $i_title_dr; ?> <input type=radio name=Title value=5 <?php echo $Title5; ?>> <?php echo $i_title_prof; ?></td></tr><?*/?>
<? if ($TabID == 1) { ?>
<tr><td align=right nowrap><?php echo $i_UserDisplayTitle_Chinese; ?>:</td><td><input class=text type=text name=TitleChinese size=20 maxlength=100 value="<?php echo $li->TitleChinese;?>"><?=$select_chi_title?></td></tr>
<tr><td align=right nowrap><?php echo $i_UserDisplayTitle_English; ?>:</td><td><input class=text type=text name=TitleEnglish size=20 maxlength=100 value="<?php echo $li->TitleEnglish;?>"><?=$select_eng_title?></td></tr>
<? } ?>
<tr><td align=right nowrap><?php echo $i_UserGender; ?>:</td><td><input type=radio name=Gender value=M <?php echo $Gender0; ?>> <?php echo $i_gender_male; ?> <input type=radio name=Gender value=F <?php echo $Gender1; ?>> <?php echo $i_gender_female; ?></td></tr>
<tr><td align=right nowrap><?php echo $i_UserDateOfBirth; ?>:</td><td><input class=text type=text name=DateOfBirth size=10 maxlength=10 value="<?php echo $li->DateOfBirth;?>"> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td align=right nowrap><?php echo $i_UserHomeTelNo; ?>:</td><td><input class=text type=text name=HomeTelNo size=15 maxlength=20 value="<?php echo $li->HomeTelNo;?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_UserOfficeTelNo; ?>:</td><td><input class=text type=text name=OfficeTelNo size=15 maxlength=20 value="<?php echo $li->OfficeTelNo;?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_UserMobileTelNo; ?>:</td><td><input class=text type=text name=MobileTelNo size=15 maxlength=20 value="<?php echo $li->MobileTelNo;?>"> <? if (isset($plugin['sms']) && $plugin['sms']){ echo $i_UserMobileSMSNotes; }?></td></tr>
<tr><td align=right nowrap><?php echo $i_UserFaxNo; ?>:</td><td><input class=text type=text name=FaxNo size=15 maxlength=20 value="<?php echo $li->FaxNo;?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_UserICQNo; ?>:</td><td><input class=text type=text name=ICQNo size=15 maxlength=20 value="<?php echo $li->ICQNo;?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_UserAddress; ?>:</td><td><input class=text type=text name=Address size=30 maxlength=255 value="<?php echo $li->Address;?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_UserCountry; ?>:</td><td><input class=text type=text name=Country size=20 maxlength=50 value="<?php echo $li->Country;?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_UserInfo; ?>:</td><td><textarea name=Info cols=30 rows=5><?php echo $li->Info;?></textarea></td></tr>
<tr><td align=right nowrap><?php echo $i_UserRemark; ?>:</td><td><textarea name=Remark cols=30 rows=5><?php echo $li->Remark;?></textarea></td></tr>
<!-- Teacher Card ID -->
<? if ($TabID == 1 && ((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) ){?>
	<tr><td align=right nowrap><?php echo $i_SmartCard_CardID; ?>:</td><td><input class=text type=text name=CardID size=20 maxlength=50 value="<?=$li->CardID?>"></td></tr>
<?php } ?>
<? if ($TabID == 2 && ((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))) { ?>
<tr><td align=right nowrap><?php echo $i_SmartCard_CardID; ?>:</td><td><input class=text type=text name=CardID size=20 maxlength=50 value="<?=$li->CardID?>"></td></tr>
<? } ?>
<? if($TabID == 2){ ?>
<tr><td align=right nowrap><?php echo $i_WebSAMS_Registration_No; ?>:</td><td><input class=text type=text name=WebSamsRegNo size=20 maxlength=20 value="<?=$li->WebSamsRegNo?>" <?=($iportfolio_activated?"DISABLED=TRUE":"")?>> <span class=extraInfo>(<?=$i_ifapplicable?>)</span><br><?=$i_WebSAMSRegNo_Format_Notice?></td></tr>
<? } ?>

<? if($special_feature['ava_hkid']) {?>
<tr><td align=right nowrap><?php echo $i_HKID; ?>:</td><td><input class=text type=text name=HKID size=10 maxlength=10 value="<?=$li->HKID?>"></td></tr>
<? } ?>
<? if($special_feature['ava_strn'] && ($TabID==2 || $TabID==4)) {?>
<tr><td align='right' nowrap><?php echo $i_STRN; ?>:</td><td><input class='text' type='text' name='STRN' size='10' maxlength='8' value="<?=$li->STRN?>"></td></tr>
<? } ?>

<tr><td colspan=2><br></td></tr>
<?
if ($TabID == 4) {
?>
<tr><td align=right nowrap><?php echo $i_Profile_DataLeftYear; ?>:</td><td><input class=text type=text name=YearOfLeft size=10 maxlength=20 value="<?=$YearOfLeft?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_UserClassName; ?>:</td><td><input class=text type=text name=ClassName size=10 maxlength=20 value="<?php echo $li->ClassName;?>"><?=$selection?></td></tr>
<? }
else
{ ?>
<? /* IP25 - this setting should be set in School Settings  ?>
<tr><td align=right nowrap><?php echo $i_UserClassName; ?>:</td><td><input class=text type=text name=ClassName size=10 maxlength=20 value="<?php echo $li->ClassName;?>"><?=$selection?></td></tr>
<? */ ?>
<input type=hidden name=ClassName value="<?php echo $li->ClassName;?>">
<? } ?>
<? /* IP25 - this setting should be set in School Settings  ?>
<tr><td align=right nowrap><?php echo $i_UserClassNumber; ?>:</td><td><input class=text type=text name=ClassNumber size=10 maxlength=20 value="<?php echo $li->ClassNumber;?>"></td></tr>
<? */ ?>
<input type=hidden name=ClassNumber value="<?php echo $li->ClassNumber;?>">
<?php
/*
$lsysacc = new libsystemaccess($UserID[0]);
if ($TabID != 4 && isset($plugin['webmail']) && $plugin['webmail'])
{
    if ($webmail_SystemType == 2)
    {
         $mail_title = $i_Mail_LinkWebmail;
    }
    else if ($webmail_SystemType == 3)
    {
         $mail_title = $i_Mail_AllowSendReceiveExternalMailEdit;
    }
    else $mail_title = $i_Mail_AllowUserUseWebmail;

    $mail_str = ($lsysacc->hasMailRight()? "CHECKED":"");
?>
<tr><td align=right nowrap></td><td><input type=checkbox name=open_webmail value=1 <?=$mail_str?>> <?php echo $mail_title; ?></td></tr>
<?php
}
?>
<?php

if ($TabID != 4 && isset($plugin['personalfile']) && $plugin['personalfile'])
{
    if ($personalfile_type=='LOCAL_FTP' || $personalfile_type=='REMOTE_FTP')
    {
        include_once("../../includes/libftp.php");
        $lftp = new libftp();
        if ($lftp->isAccountManageable)
        {
            $file_title = $i_Files_AllowUserUseFTPAccount;
        }
        else
        {
            $file_title = $i_Files_LinkFTPAccount;
        }
    }
    else if ($personalfile_type=='AERO')
    {
         $file_title = $i_Files_LinkToAero;
    }
    $file_str = ($lsysacc->hasFilesRight()? "CHECKED":"");
    ?>
<tr><td align=right nowrap></td><td><input type=checkbox name=open_file value=1 <?=$file_str?>> <?php echo $file_title; ?></td></tr>
    <?
}
*/
?>
<? if ($TabID != 4) { ?>
<tr><td align="right"><?=$i_admintitle_group?>:</td>
<td><?php echo $lo->displayUserGroups($li->UserID); ?></td></tr>
<? } ?>
<? if ($TabID == 3) { ?>
<tr><td align="right"><a name="childrenlink"><?=$i_UserParentLink?>:</a></td>
<td>
<table width=100% border=0 cellspacing=1 cellpadding=1>
<tr><td width=1>
<select name=child[] size=4 multiple>
<?php
  if ($TabID == 3) {
        $row = $li->getChildrenList();
          for($i=0;$i<sizeof($row);$i++){
                echo "<option value='".$row[$i][0]."'>".$row[$i][1]."</option>";
          }
  }
?>
<option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select>
</td><td>
<a href="javascript:newWindow('choose/index.php?fieldname=child[]',2)"><img src="/images/admin/button/s_btn_select_std_<?=$intranet_session_language?>.gif" border="0"></a>
<br>
<a href="javascript:checkOptionRemove(document.form1.elements['child[]'])"><img src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
</td></tr>
<? } ?>
</table>
</blockquote>
<input type=hidden name=UserID value="<?php echo $li->UserID; ?>">
<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=TabID value="<?=$TabID?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>
<?php
/*
if ($TabID == 3) {

$row = $li->getChildrenList();
?>
<script language="JavaScript1.2">

obj = document.form1.elements["child[]"];
checkOptionClear(obj);
<?php for($i=0; $i<sizeof($row); $i++){
                        echo "checkOptionAdd(obj, \"".$row[$i][1]."\", \"".$row[$i][0]."\");\n";

      }
?>
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");

</script>

<?
}
*/
?>


<?
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>
