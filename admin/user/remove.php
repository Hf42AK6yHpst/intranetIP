<?php

// Modifng by 

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libwebmail.php");
include_once("../../includes/libftp.php");
include_once("../../includes/libclubsenrol.php");
include_once("../../includes/libform.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libldap.php");
include_once("../../includes/libstudentpromotion.php");
intranet_opendb();

$list = implode("','", $UserID);
$lu = new libuser();

#### Check any account has non-zero balance
//$sql = "SELECT COUNT(*) FROM PAYMENT_ACCOUNT WHERE StudentID IN ($list) AND Balance >= 0.01";
$sql="SELECT COUNT(*) FROM PAYMENT_ACCOUNT AS a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b ON (a.StudentID = b.StudentID) WHERE a.StudentID IN ('$list') AND (a.Balance >=0.01 OR b.RecordStatus=0)";

$temp = $lu->returnVector($sql);

if ($temp[0]>0)
{
    # Stop this action
    include_once("../../includes/libaccount.php");
    include_once("../../lang/lang.$intranet_session_language.php");
    include_once("../../templates/adminheader_intranet.php");
?>
<?= displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, 'index.php',$button_remove,'') ?>
<?= displayTag("head_user_$intranet_session_language.gif", $msg) ?>
<?php
    $langfield = getNameFieldByLang("b.");
    $sql = "SELECT b.UserLogin, $langfield, b.ClassName, b.ClassNumber, IF(a.Balance>0.01,CONCAT('<font color=red>$', FORMAT(a.Balance, 1),'</font>'),CONCAT('$', FORMAT(a.Balance, 1))) AS Amount,c.PaymentID,d.Name,CONCAT('$',FORMAT(c.Amount,1))
                   FROM PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                   LEFT OUTER JOIN PAYMENT_PAYMENT_ITEMSTUDENT as c ON (a.StudentID = c.StudentID)
                   LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as d ON (c.ItemID = d.ItemID)
                   WHERE a.StudentID IN ($list) AND (a.Balance >= 0.01 OR c.RecordStatus=0) ORDER BY a.StudentID";
             
    $result = $lu->returnArray($sql,8);
    
    $result2 = array();
    for ($i=0; $i<sizeof($result); $i++)
    {
         list($t_userLogin,$t_name, $t_className, $t_classNum, $t_balance,$payment_id,$item_name,$unpaid_amount) = $result[$i];
	     $result2[$t_userLogin]['name']=$t_name;
	     $result2[$t_userLogin]['class']=$t_className;
	     $result2[$t_userLogin]['classnumber']=$t_classNum;
	     $result2[$t_userLogin]['balance']=$t_balance;
	     if($payment_id!=""){
	     	$result2[$t_userLogin]['items'][]=$item_name." (".$unpaid_amount.")";
	     }
    }
    
    $content = "";

    $j=0;
    foreach($result2 as $user_login => $values){
	    $name = $values['name'];
	    $class= $values['class'];
	    $classnumber = $values['classnumber'];
	    $balance = $values['balance'];
	    
	    if(is_array($values['items'])){
		    $items="<table border=0>";
		  	for($x=0;$x<sizeof($values['items']);$x++){
	    		$items .= "<tr><td><font color=red>-</font></td><td><font color=red>".$values['items'][$x]."</font></td></tr>";
	    	}
	    	$items.="</table>";
		}
	    
	    $j++;
	    $css =$css = ($j%2?"":"2");
        $content .= "<tr class=tableContent$css><td>$user_login</td><td>$name</td><td>$class</td><td>$classnumber</td><td>$balance</td><td>$items</td></tr>\n";

	}
    	    /*
         list($t_userLogin,$t_name, $t_className, $t_classNum, $t_balance,$item_name,$unpaid_amount) = $result[$i];
         $css = ($i%2?"":"2");
         $content .= "<tr class=tableContent$css><td>$t_userLogin</td><td>$t_name</td><td>$t_className</td><td>$t_classNum</td><td>$t_balance</td></tr>\n";
         */


    ?>
<blockquote>
<br>
<span class="extraInfo"><span class=subTitle><?=$i_UserRemoveStop_PaymentBalanceNonZero?></span></span>
<br>
<table width=90% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 >
<tr class=tableTitle><td><?=$i_UserLogin?></td><td><?=$i_UserName?></td><td><?=$i_UserClassName?></td><td><?=$i_UserClassNumber?></td><td><?=$i_Payment_Field_Balance?></td><td><?="$i_Payment_Field_PaymentItem ($i_Payment_Field_Amount)"?></td></tr>
<?=$content?>
</table>
<br><br>
<a class=functionlink_new href="index.php?TabID=<?=$TabID?>&filter=<?=$filter?>&order=<?=$order?>&field=<?=$field?>&pageNo=<?=$pageNo?>"><?=$button_back?></a>
</blockquote>

    <?
    intranet_closedb();
    include_once("../../templates/adminfooter.php");
    exit();

}

# SchoolNet integration
if ($plugin['SchoolNet'])
{
    $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID IN ($list)";
    $schnet_target = $lu->returnVector($sql);
    include_once("../../includes/libschoolnet.php");
    $lschoolnet = new libschoolnet();
    $lschoolnet->removeUser($schnet_target);
}


$lu->prepareUserRemoval($list);
$lsp = new libstudentpromotion();
$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list)";
$students = $lu->returnVector($sql);
if (sizeof($students)!=0)
{
    $studentList = implode(",",$students);
    $lsp->addClassHistory($studentList);
    $lsp->archiveStudentsInfo($studentList);
    if ($special_feature['alumni'] && $alumni_system_type==1)
    {
        $lu->setToAlumni($studentList);
    }
    else
    {
        $lu->removeUsers($studentList);
    }
}


$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType != 2 AND UserID IN ($list)";
$others = $lu->returnVector($sql);

if (sizeof($others)!=0)
{
    $otherList = implode(",",$others);
    
    $lsp->archiveIntranetUser($otherList);
    $lu->removeUsers($otherList);
}

# delete calendar viewer
$sql = "delete from CALENDAR_CALENDAR_VIEWER where UserID in ($list)";
$lu->db_db_query($sql);

intranet_closedb();
header("Location: index.php?TabID=$TabID&filter=$filter&order=$order&field=$field&pageNo=$pageNo&msg=3");
?>
