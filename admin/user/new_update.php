<?php
# Using By : 
############### Change Log 
# 2011-0210 Carlos - added code to open iMail Gamma account 
# - 2010-08-27 YatWoon
#	hide Title
##############################
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libregistry.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$li = new libregistry();

$license_student_left = 9999;
if (isset($account_license_student) && $account_license_student>0 && $identity==2)
{
	$license_student_used = $li->getLicenseUsed();
	$license_student_left = $account_license_student - $license_student_used;
}

//For student only
if (($license_student_left<=0) && ($identity==2))
{
	# no quota left
	echo "<table width=560  border=0 cellpadding=0 cellspacing=0 align='center'>
	<tr><td align='center'><br /><br />
	{$i_license_sys_msg_none}
	</td></tr>
	</table>";
	die();
}


$UserLogin = intranet_htmlspecialchars(trim($UserLogin));
$UserEmail = intranet_htmlspecialchars(trim($UserEmail));
//$Password = intranet_htmlspecialchars(trim($Password));
$Password = trim($Password);
$EnglishName = intranet_htmlspecialchars(trim($EnglishName));
$ChineseName = intranet_htmlspecialchars(trim($ChineseName));
$NickName = intranet_htmlspecialchars(trim($NickName));
// $Title = intranet_htmlspecialchars(trim($Title));
$Gender = intranet_htmlspecialchars(trim($Gender));
$DateOfBirth = intranet_htmlspecialchars(trim($DateOfBirth));
$HomeTelNo = intranet_htmlspecialchars(trim($HomeTelNo));
$OfficeTelNo = intranet_htmlspecialchars(trim($OfficeTelNo));
$MobileTelNo = intranet_htmlspecialchars(trim($MobileTelNo));
$FaxNo = intranet_htmlspecialchars(trim($FaxNo));
$ICQNo = intranet_htmlspecialchars(trim($ICQNo));
$Address = intranet_htmlspecialchars(trim($Address));
$Country = intranet_htmlspecialchars(trim($Country));
$Info = intranet_htmlspecialchars(trim($Info));
$Remark = intranet_htmlspecialchars(trim($Remark));
$ClassNumber = intranet_htmlspecialchars(trim($ClassNumber));
$ClassName = intranet_htmlspecialchars(trim($ClassName));
$CardID = intranet_htmlspecialchars(trim($CardID));
$YearOfLeft = intranet_htmlspecialchars(trim($YearOfLeft));
$WebSamsRegNo = intranet_htmlspecialchars(trim($WebSamsRegNo));

$error_msg="";

if($identity==2 && $WebSamsRegNo!=""){
     if(substr(trim($WebSamsRegNo),0,1)!="#")
		$WebSamsRegNo = "#".$WebSamsRegNo;
		
	### check if websams regno exists ####
	$sql="SELECT UserID FROM INTRANET_USER WHERE WebSamsRegNo='".$WebSamsRegNo."' AND RecordType=2";
	$temp = $li->returnVector($sql);
	if(sizeof($temp)!=0){ ## WebSamsRegNo exists
		$error_msg ="WebSAMS Registration Number has been used by other user.";
	}
}

## check cardid duplication ###
if($CardID!=""){
	$sql="SELECT UserID FROM INTRANET_USER WHERE CardID='$CardID'";
	$temp = $li->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg ="Smart Card ID has been used by other user.";
	}
}

# check HKID is existing or not (if include HKID field)
if($special_feature['ava_hkid'])
{
	$HKID = intranet_htmlspecialchars(trim($HKID));
	if($HKID != "")
	{
		//$sql="SELECT UserID FROM INTRANET_USER WHERE HKID='".$HKID."' AND RecordType=2";
		$sql="SELECT UserID FROM INTRANET_USER WHERE HKID='".$HKID."'";
		$temp = $li->returnVector($sql);
		if(sizeof($temp)!=0){ ## HKID exists
			$error_msg ="HKID has been used by other user.";
		}
	}
}

# check STRN is existing or not (if include STRN field)
if($special_feature['ava_strn'] && ($identity==2 || $identity==4))
{
	$STRN = intranet_htmlspecialchars(trim($STRN));
	if($STRN != "")
	{
		//$sql="SELECT UserID FROM INTRANET_USER WHERE STRN='".$STRN."' AND RecordType=2";
		$sql="SELECT UserID FROM INTRANET_USER WHERE STRN='".$STRN."' AND (RecordType=2 or RecordType=4)";
		$temp = $li->returnVector($sql);
		if(sizeof($temp)!=0){ ## STRN exists
			$error_msg ="STRN has been used by other user.";
		}
	}
}



if($error_msg!=""){
        include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
        echo " <br>\n $error_msg \n";
        echo  "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                    <tr><td><hr size=1></td></tr>
                    <tr><td align='right'><a href='javascript:history.go(-1)'><img src='/images/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0'></a></td></tr></table>";

        include_once($PATH_WRT_ROOT."templates/adminfooter.php");
        exit();
}


#$ClassLevel = htmlspecialchars(trim($ClassLevel));

$domain_name = ($intranet_email_generation_domain==""? $HTTP_SERVER_VARS["SERVER_NAME"]:$intranet_email_generation_domain);
#$domain_name = $HTTP_SERVER_VARS["SERVER_NAME"];

$target_email = ($UserEmail==""? $UserLogin . "@".$domain_name : $UserEmail);



if($li->UserNewAdd($UserLogin, $target_email, $EnglishName, $ChineseName, $RecordStatus)==1){
	
        $NewUserID = $li->db_insert_id();
        # Assign UserLogin
//        $UserLogin = 100000+$UserID;
//      $UserPassword = intranet_random_passwd(8);
        $UserPassword = $Password;
        if ($intranet_authentication_method=="HASH")
        {
            $fieldname .= "HashedPass = MD5('$UserLogin$UserPassword$intranet_password_salt'),";
        }
        else
        {
            $fieldname .= "UserPassword = '$UserPassword', ";
        }
        $fieldname .= "NickName = '$NickName', ";
//         $fieldname .= "Title = '$Title', ";
        $fieldname .= "Gender = '$Gender', ";
        $fieldname .= "DateOfBirth = '$DateOfBirth', ";
        $fieldname .= "HomeTelNo = '$HomeTelNo', ";
        $fieldname .= "OfficeTelNo = '$OfficeTelNo', ";
        $fieldname .= "MobileTelNo = '$MobileTelNo', ";
        $fieldname .= "FaxNo = '$FaxNo', ";
        $fieldname .= "ICQNo = '$ICQNo', ";
        $fieldname .= "Address = '$Address', ";
        $fieldname .= "Country = '$Country', ";
        $fieldname .= "Info = '$Info', ";
        $fieldname .= "Remark = '$Remark', ";
        $fieldname .= "RecordType = '$identity',";
        $fieldname .= "ClassNumber = '$ClassNumber' ";
        $fieldname .= ",YearOfLeft = '$YearOfLeft'";
        if($identity==2){
        	$fieldname .= ",CardID = '$CardID'";
        	$fieldname .= ",WebSAMSRegNo = '$WebSamsRegNo'";
        }
        if ($identity == 1)
        {
	        if ((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) {
        		$fieldname .= ",CardID = '$CardID'";
		    }
            $TitleEnglish = intranet_htmlspecialchars($TitleEnglish);
            $TitleChinese = intranet_htmlspecialchars($TitleChinese);
            $fieldname .= ",TitleEnglish = '$TitleEnglish'";
            $fieldname .= ",TitleChinese = '$TitleChinese'";
        }
        
        if($special_feature['ava_hkid'])
        {
	        $fieldname .= ",HKID = '$HKID'";
        }
        
        if($special_feature['ava_strn'])
        {
	        $fieldname .= ",STRN = '$STRN'";
        }


        /* Left at later part
        $fieldname .= "ClassName = '$ClassName', ";
        $fieldname .= "ClassLevel = '$ClassLevel'";
        */
        # Update Groups
        /*
        for($i=0; $i<sizeof($IdentityGroupID); $i++){
                $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID) VALUES (".$IdentityGroupID[$i].", $UserID)";
                $li->db_db_query($sql);
        }
        */
        if ($identity == 1)      # Teacher/Staff
        {
            if ($teaching == 1)    # Teaching staff -> Teacher group
            {
                $target_idgroup = 1;
                $fieldname .= ",Teaching = 1";
            }
            else                   # Non-teaching staff -> Admin staff group
            {
                $target_idgroup = 3;
            }
        }
        else if ($identity == 2)     # Student -> Student group
        {
             $target_idgroup = 2;
        }
        else if ($identity == 3)     # Parent -> Parent group
        {
             $target_idgroup = 4;
        }
        else if ($identity == 4)
        {
             $target_idgroup = $alumni_GroupID;
        }
		$fieldname .= ", DateModified = now()";
        $sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = $NewUserID";
		
        $li->db_db_query($sql);

        $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES ($target_idgroup, $NewUserID, now(), now())";
        $li->db_db_query($sql);

        if ($identity != 4)
        {

        for($i=0; $i<sizeof($GroupID); $i++){
                $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES (".$GroupID[$i].", $NewUserID, now(), now())";
                $li->db_db_query($sql);
        }

        $lclass = new libclass();
        if ($ClassName != "")
        {
            if ($lclass->isClassExist($ClassName))
            {
            /*
                $sql = "INSERT INTO INTRANET_GROUP (Title, RecordType, DateInput,DateModified) VALUES ('$ClassName',3,now(),now())";
                echo "1: $sql\n";
                $li->db_db_query($sql);
                */
                $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) SELECT GroupID, '$NewUserID', now(), now() FROM INTRANET_GROUP WHERE Title = '$ClassName'";
                $li->db_db_query($sql);
            }
            else
            {
                $ClassName = "";
            }

        }
        }
        /*
        if ($ClassLevel != "")
        {
//          $ClassLevel .= "*";                      # No more * in level group
            if ($lclass->isLevelExist($ClassLevel))
            {
                $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID) SELECT GroupID, '$UserID' FROM INTRANET_GROUP WHERE Title = '$ClassLevel'";
                $li->db_db_query($sql);
            }
            else
            {
                $ClassLevel = "";
            }
        }
*/
        # Update ClassName and ClassLevel
        $sql = "UPDATE INTRANET_USER SET ClassName = '$ClassName' WHERE UserID = $NewUserID";
        $li->db_db_query($sql);

        $msg = 1;
        # Call sendmail function
        $mailSubject = registration_title();
        $mailBody = registration_body_new($UserEmail, $UserLogin, $UserPassword, stripslashes($EnglishName), stripslashes($ChineseName));
        $mailTo = $UserEmail;
        $lu = new libsendmail();
        //$lu->SendMail($mailTo,$mailSubject, $mailBody,"");
        $lu->do_sendmail($webmaster, "", $mailTo, "", $mailSubject, $mailBody);
		
		# insert group calendar
		$cal_sql = "
				insert into CALENDAR_CALENDAR_VIEWER 
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
				select g.CalID, '$NewUserID', g.GroupID, 'E', 'R', '2f75e9', '1' 
				from INTRANET_GROUP as g inner join INTRANET_USERGROUP as u 
				on g.GroupID = u.GroupID and u.UserID = $NewUserID
			"; //echo $cal_sql.'<br><br>';
			$li->db_db_query($cal_sql);
		

#### Special handling
$li->UpdateRole_UserGroup();

if ($identity==3)             # Parent Student relation linkage
{
    $values = "";
    $delimiter = "";
    for ($i=0; $i<sizeof($child); $i++)
    {
         $childID = $child[$i];
         $values .= "$delimiter ($NewUserID,$childID)";
         $delimiter = ",";
    }
    $sql = "DELETE FROM INTRANET_PARENTRELATION WHERE ParentID = $NewUserID";
    $li->db_db_query($sql);
    if ($values != "")
    {
        $sql = "INSERT IGNORE INTO INTRANET_PARENTRELATION (ParentID, StudentID) VALUES $values";
        $li->db_db_query($sql);
    }
}

if ($identity != 4)
{

$acl_field = 0;

if ($open_file && in_array($identity,$personalfile_identity_allowed) )
{
    if ($personalfile_type=='FTP')
    {
        $lftp = new libftp();
        if ($lftp->isAccountManageable)
        {
            $file_failed = ($lftp->open_account($UserLogin,$UserPassword)? 0:1);
            $file_content = get_file_content($intranet_root."/file/account_file_quota.txt");
            if ($file_content == "")
            {
                $userquota = array(10,10,10);
            }
            else
            {
                $userquota = explode("\n", $file_content);
            }
            $target_quota = $userquota[$identity-1];
            $lftp->setTotalQuota($UserLogin,$target_quota,"iFolder");
        }
    }
    $acl_field += 2;
}
# Webmail
if ($open_webmail && in_array($identity,$webmail_identity_allowed))
{
	if($plugin['imail_gamma']===true)
	{
		include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
		include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
		$laccount = new libaccountmgmt();
		$IMap = new imap_gamma(true);
		
		## IMAP identity quota are [0] => teacher [1] => student [2] => alumni [3] => parent
		## idenntity are [1]=> teacher [2]=> student [3]=> parent
		$quota_identity = ($identity==3?3:$identity-1);
		$target_quota = $IMap->default_quota[$quota_identity];
		$fulluserlogin = trim($UserLogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		$open_account_succeed = $IMap->open_account($fulluserlogin, $UserPassword);
		if($open_account_succeed){
			$laccount->setIMapUserEmail($NewUserID,$fulluserlogin);
			$IMap->SetTotalQuota ($fulluserlogin, $target_quota, $NewUserID);
		}
	}else{
	    $lwebmail = new libwebmail();
	    if ($lwebmail->has_webmail)
	    {
	        $lwebmail->open_account($UserLogin,$UserPassword);
	        $acl_field += 1;
	        $file_content = get_file_content($intranet_root."/file/account_mail_quota.txt");
	        if ($file_content == "")
	        {
	            $userquota = array(10,10,10);
	        }
	        else
	        {
	            $userquota = explode("\n", $file_content);
	        }
	        $target_quota = $userquota[$identity-1];
	        $lwebmail->setTotalQuota($UserLogin,$target_quota,"iMail");
	    }
	}
}

$sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($NewUserID, $acl_field)";
$li->db_db_query($sql);

# Send welcome campusmail
/*
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
$lcampusmail = new libcampusmail();
$lcampusmail->sendWelcomeMail(array($UserID));
*/

}

if ($intranet_authentication_method=='LDAP')
{
    $lldap = new libldap();
    if ($lldap->isAccountControlNeeded())
    {
        $lldap->connect();
        $lldap->openAccount($UserLogin,$UserPassword);
    }
}

# Payment
if ($plugin['payment'] && ($identity == 2 || $identity == 1 ))
{
    $sql = "INSERT INTO PAYMENT_ACCOUNT (StudentID, Balance) VALUES ('$NewUserID',0)";
    $li->db_db_query($sql);
}

# iMail Quota
if ($special_feature['imail'])
{
    # Get Default Quota
    $file_content = get_file_content($intranet_root."/file/campusmail_set.txt");
    if ($file_content == "")
    {
        $quota = 10;
    }
    else
    {
        $userquota = explode("\n", $file_content);
        if ($identity != 3)
        {
            $quotaline = $userquota[$identity-1];
        }
        else
        {
            $quotaline = $userquota[3];
        }
        $temp = explode(":",$quotaline);
        $quota = $temp[1];
    }
    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USERQUOTA (UserID, Quota) VALUES ('$NewUserID', '$quota')";
    $li->db_db_query($sql);
    
    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed) VALUES ('$UserID', 0)";
    $li->db_db_query($sql);
}


# SchoolNet integration
if ($plugin['SchoolNet'])
{
    include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
    $lschoolnet = new libschoolnet();
    if ($identity == 1)
    {
        # Param: array in ($userlogin, $password, $DOB, $gender, $cname, $ename, $tel, $mobile, $address, $email, $teaching)
        $sql = "SELECT UserLogin, UserPassword, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail, Teaching FROM INTRANET_USER WHERE UserID = '".IntegerSafe($NewUserID)."'";
        $data = $li->returnArray($sql,11);
        $lschoolnet->addStaffUser($data);
    }
    else if ($identity == 2)
    {
         #($userlogin, $password, $classname, $classnum, $DOB, $gender, $cname, $ename, $tel, $mobile, $address, $email)
        $sql = "SELECT UserLogin, UserPassword,ClassName, ClassNumber, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail FROM INTRANET_USER WHERE UserID = '".IntegerSafe($NewUserID)."'";
         $data = $li->returnArray($sql,12);
         $lschoolnet->addStudentUser($data);
    }
    else if ($identity == 3)
    {
    }
}

################




}
else
{
    $msg = 14;
}


intranet_closedb();
header("Location: index.php?TabID=".$identity."&filter=$RecordStatus&order=$order&field=$field&msg=$msg&filefailed=$file_failed");
?>