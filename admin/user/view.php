<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libuser.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();

$li = new libuser($UserID);
?>

<form action=edit.php method=get>
<p class=admin_head><?php echo $i_admintitle_am; ?><?php echo displayArrow();?><a href=javascript:history.back()><?php echo $i_admintitle_am_user; ?></a><?php echo displayArrow();?><?php echo $li->UserLogin; ?></p>
<blockquote>
<p><?php echo $li->display(); ?>
<p><input class=submit type=submit value="<?php echo $button_edit; ?>">
</blockquote>
<input type=hidden name=UserID[] value="<?php echo $li->UserID; ?>">
</form>

<?php 
intranet_closedb();
include("../../templates/adminfooter.php"); 
?>
