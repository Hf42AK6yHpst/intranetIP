<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libclass.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();
include_once("../../../templates/fileheader_admin.php");


$lclass = new libclass();
$classes = $lclass->getClassList();
$class_select = getSelectByArray($classes,"name=ClassID onChange=this.form.submit()",$ClassID);


if (isset($ClassID) && $ClassID != "")
{
     $ClassName = $lclass->getClassName($ClassID);
     $students = $lclass->getStudentNameListWClassNumberByClassName($ClassName);
     $student_select = getSelectByArray($students,"name=ChooseUserID[] size=5 multiple");
}
?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];
     par.checkOption(parObj);
     i = obj.selectedIndex;
     while(i!=-1){
          par.checkOptionAdd(parObj, obj.options[i].text, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
}
</script>

<form name="form1" action="index.php" method="post">
<?= displayNavTitle($i_UserParentLink, '') ?>

<p style="padding-left:20px">
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
<tr><td style="background-image: url(../../../images/admin/pop_bg.gif);" >


<table width=100% border=0 cellpadding=10 cellspacing=0>
<tr><td>
<?php echo $i_UserParentLink_SelectClass; ?><br><img src=../../../images/space.gif border=0 width=1 height=3><br>
<?php echo $class_select; ?>
</td></tr>
<?php if(isset($ClassID)) { ?>
<tr><td>
<?php echo $i_UserParentLink_SelectStudent; ?><br><img src=../../../images/space.gif border=0 width=1 height=3><br>
<?php echo $student_select; ?>
<a href="javascript:checkOption(document.form1.elements['ChooseUserID[]']);AddOptions(document.form1.elements['ChooseUserID[]'])"><img src="/images/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border="0"></a>

</td></tr>
<?php } ?>
</table>

</td></tr>
<tr><td><img src="../../../images/admin/pop_bottom.gif" width=422 height=18 border=0></td></tr>
<tr><td align=center height="40" style="vertical-align:bottom">
<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
</p>

<input type=hidden name=fieldname value="<?php echo $fieldname; ?>">
</form>

<?php
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>