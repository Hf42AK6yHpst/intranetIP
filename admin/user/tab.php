<?php
function showTab($IdentityID, $TabID){
     global $image_path, $intranet_session_language;
     $lang = $intranet_session_language;
     $status = ($IdentityID == $TabID) ? "m" : "o";
     switch ($IdentityID){
          case 1: $name = "teacher"; break;
          case 2: $name = "student"; break;
          case 3: $name = "parent"; break;
//          case 3: $name = "admin"; break;
//          case 4: $name = "parent"; break;
          default: $name = "teacher"; break;
     }
     $image = $image_path."/admin/user/".$name."_".$status."_".$lang.".gif";
     $x = "<a href=?TabID=$IdentityID><img src=$image width=140 border=0></a>";
     return $x;
}

$x  = "<table border=0 cellpadding=0 cellspacing=0>\n";
$x .= "<tr>\n";
$x .= "<td>".showTab(1, $TabID)."</td>\n";
$x .= "<td>".showTab(2, $TabID)."</td>\n";
$x .= "<td>".showTab(3, $TabID)."</td>\n";
//$x .= "<td>".showTab(4, $TabID)."</td>\n";
$x .= "</tr>\n";
$x .= "</table>\n";

echo $x;
?>