<?php
# using: henry chow

/******************************************
 *  modification log:
 *
 *	20100528 YatWoon
 *		update set_row_array(), maybe import student csv is without CardID
 *
 * ****************************************/
 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_opendb();

$linterface = new interface_html();


$format0 = ($format==0) ? "SELECTED" : "";
$format1 = ($format==1 || $format=="") ? "SELECTED" : "";
$url = "index.php?filter=$filter&keyword=$keyword&pageNo=$pageNo&order=$order&field=$field";
$maxTab = 3;
if ($special_feature['alumni'])
{
    $i_identity_array[4] = $i_identity_alumni;
    $maxTab = 4;
}

$headline = $i_import_identity_array[$TabID];

if ($TabID > $maxTab || $TabID < 1)
{
    $TabID = "";
}
if ($TabID == "")
{
?>
<form name="form1" action="" method="get">
<?= displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, $url, $button_import, '') ?>
<?= displayTag("head_user_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo "$button_select $i_identity"; ?>:</td><td>
<SELECT name=TabID onChange=this.form.submit()>
<OPTION><?=" -- $button_select -- "?></OPTION>
<OPTION value=1><?=$i_identity_teachstaff?></OPTION>
<OPTION value=2><?=$i_identity_student?></OPTION>
<OPTION value=3><?=$i_identity_parent?></OPTION>
<?
if ($special_feature['alumni'])
{
?>
<OPTION value=4><?=$i_identity_alumni?></OPTION>
<?
}
?>
</SELECT>
</td></tr>
</table>
</form>

<?

}
else
{


$lo = new libgrouping();
if ($TabID==2)
{
	$license_student_left = 9999;
	if (isset($account_license_student) && $account_license_student>0)
	{
		$license_student_used = $lo->getLicenseUsed();
		$license_student_left = $account_license_student - $license_student_used;
	}
	
	if ($license_student_left<=0)
	{
		# no quota left
		echo "<table width=560  border=0 cellpadding=0 cellspacing=0 align='center'>
		<tr><td align='center'><br /><br />
		{$i_license_sys_msg_none}
		</td></tr>
		</table>";
		die();
	}
}

//<li></ul>

$hasSmartCard = ($plugin['attendancestudent'] || $plugin['payment'] || $plugin['Lunchbox']);
$hasTeacherSmartCard = ($module_version['StaffAttendance'] || $plugin['payment'] );

/*
$other_fields = "";
if($special_feature['ava_hkid'])
	$other_fields .= "_hkid";
if($special_feature['ava_strn'] && ($TabID==2 || $TabID==4))
	$other_fields .= "_strn";
*/

if(($TabID==2 && $hasSmartCard) || ($TabID==1 && $hasTeacherSmartCard))
	$other_fields .= "_card";
if($special_feature['ava_hkid'])
	$other_fields .= "_hkid";
if($special_feature['ava_strn'] && ($TabID==2 || $TabID==4))
	$other_fields .= "_strn";


switch ($TabID)
{
        case 1: //$sample_file = $hasTeacherSmartCard?GET_CSV("eclass_staff2$other_fields.csv"):GET_CSV("eclass_staff$other_fields.csv"); break;
        		$sample_file = GET_CSV("eclass_staff$other_fields.csv");
        		break;
        case 2: 
        	if($hasSmartCard)
        	{
        		$sample_file = GET_CSV("eclass$other_fields.csv"); 
    		}
    		else
        	{
	        	$sample_file = GET_CSV("eclass".$other_fields.".csv"); 
        	}
        	break;
        case 3: $sample_file = GET_CSV("eclass_parent$other_fields.csv"); break;
        case 4: $sample_file = GET_CSV("eclass_alumni$other_fields.csv"); break;
        default: $sample_file = GET_CSV("eclass$other_fields.csv"); break;
}

#if (isset($plugin['attendancestudent']) && $plugin['attendancestudent'] && $TabID==2)
/*
if ($hasSmartCard && $TabID==2)
{
    $sample_file = "eclass_st_card.csv";
}
*/

if ($TabID == 1)
{
    $teachingStr = "<p><input type=radio name=teaching value=1 checked>$i_teachingStaff <input type=radio name=teaching value=0>$i_nonteachingStaff \n<br><span class=extraInfo>$i_teachingDifference</span>\n";
}
else
{
    $teachingStr = "";
}

?>

<script language="javascript">
function checkform(obj){
        return true;
}
</script>

<form name="form1" action="import_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, $url, $button_import, '') ?>
<?= displayTag("head_user_$intranet_session_language.gif", $msg) ?>

<blockquote>
<span class="extraInfo">[<span class=subTitle><?=$headline?></span>]</span>
<p><?php echo $i_import_msg1; ?>:
<select name=format>
<option value=1 SELECTED>eClass (.CSV file)</option>
</select>
<p><?php echo $i_import_msg2; ?>:
<input class=file type=file name=userfile size=25><br><?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>
<?=$teachingStr?><br><br>
<?php
$account_creation = false;
if ($TabID != 4 && isset($plugin['webmail']) && $plugin['webmail'] && in_array($TabID, $webmail_identity_allowed))
{
    if ($webmail_type == 2)
    {
         $mail_title = $i_Mail_LinkWebmail;
    }
    else if ($webmail_type == 3)
    {
         $mail_title = $i_Mail_AllowSendReceiveExternalMail;
    }
    else $mail_title = $i_Mail_OpenWebmailAccount;

    $account_creation = true;
?>
<input type=checkbox name=open_webmail value=1 checked> <?php echo $mail_title; ?><br>
<?php
}
?>
<?php
if ($TabID != 4 && isset($plugin['personalfile']) && $plugin['personalfile'] && in_array($TabID,$personalfile_identity_allowed))
{
    if ($personalfile_type=='FTP')
    {
        include_once($PATH_WRT_ROOT."includes/libftp.php");
        $lftp = new libftp();
        if ($lftp->isAccountManageable)
        {
            $file_title = $i_Files_UseIFolder;
        }
        else
        {
            $file_title = $i_Files_LinkFTPAccount;
        }
    }
    else if ($personalfile_type=='AERO')
    {
         $file_title = $i_Files_LinkToAero;
    }
    $account_creation = true;
    ?>
<input type=checkbox name=open_file value=1 checked> <?php echo $file_title; ?><br>
    <?
}
if ($account_creation)
{
    echo "$i_Linux_Description_ImportUser";
}

?>

<p>
<a class=functionlink_new href='<?=$sample_file?>'><?php echo $i_import_msg9; ?></a>
</p>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<blockquote>
<table width=500 border=0 cellpadding=10 cellspacing=0><tr><td class=tableContent>
<span class="extraInfo">[<span class=subTitle><?php echo $i_import_msg4; ?></span>]</span>
<p><?php echo $i_import_msg5; ?>
<p>
<ul><li><?php echo $i_import_msg6; ?>
<li><?php echo $i_import_msg7; ?>
<li><?php echo $i_import_msg8; ?></ul>
<?=$i_import_msg_note?>
<br>
<?=($TabID==2?$i_import_msg_note2:"")?>
</td></tr></table>
</blockquote>
<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=keyword value="<?php echo $keyword; ?>">
<input type=hidden name=filter value="<?php echo $filter; ?>">
<input type=hidden name=TabID value="<?php echo $TabID; ?>">

</form>


<?php
}
intranet_closedb();
include($PATH_WRT_ROOT."templates/adminfooter.php");
?>