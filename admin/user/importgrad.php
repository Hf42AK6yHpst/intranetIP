<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");

$url = "index.php?filter=$filter&keyword=$keyword&pageNo=$pageNo&order=$order&field=$field";
$headline = $i_UserImportGraduate;

?>
<form name="form1" action="importgrad_update.php" method="post" enctype="multipart/form-data">
<?= displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, $url, $headline, '') ?>
<?= displayTag("head_user_$intranet_session_language.gif", $msg) ?>

<blockquote>
<span class="extraInfo">[<span class=subTitle><?=$headline?></span>]</span>
<p></p>
<?= $i_select_file ?>: <input type=file name=userfile>

<blockquote>
<?=$i_UserGraduate_ImportInstruction?>
</blockquote>

<p><a class=functionlink_new href="<?= GET_CSV("sample_graduate.csv")?>"><?=$i_general_clickheredownloadsample?></a></p>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>