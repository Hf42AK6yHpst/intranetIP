<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();


$status_graduate = 3;
# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath==""){
   header("Location: importgrad.php");
} else {
        # read file into array
        # return 0 if fail, return csv array if success
        $lo = new libfilesystem();
        $li = new libdb();
        $data = $lo->file_read_csv($filepath);
        $col_name = array_shift($data);                   # drop the title bar
        $delimiter = "";
        $logins = "";
        for ($i=0; $i<sizeof($data); $i++)
        {
             $login = "'".$data[$i][0]."'";
             $logins .= "$delimiter $login";
             $delimiter = ",";
        }
        $sql = "UPDATE INTRANET_USER SET RecordStatus = $status_graduate WHERE UserLogin IN ($logins)";
        $li->db_db_query($sql);

}
$page = "index.php?msg=2&";
header("Location: ".$page."filter=$status_graduate");

intranet_closedb();

//header("Location: ".$page."TabID=$TabID&filter=1&keyword=$keyword&pageNo=$pageNo&order=$order&field=$field&format=$format");
?>