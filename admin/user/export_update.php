<?php
# using: henry chow

########## Change Log [Start] #############
#
#	Date:	2010-05-20	YatWoon
#			Fixed: cannot export parent's HKID with "Export Default Format"
#
#	Date:	2010-04-19 YatWoon
#			for "LaiShan" project, add parent/guardian name to student export csv
#
#	Date:	2010-04-16 YatWoon
#			Export staff csv for import: missing check CardID should be include or not
#
########## Change Log [End] #############

$PATH_WRT_ROOT = "../../";

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbdump.php");
include_once("../../includes/libfilesystem.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lexport = new libexporttext();

if (!isset($default) || $default != 1)
{
     $li = new libdbdump();
     
     # check if there parent / guardian column is selected
     # set prefix if yes
     
     if(in_array("Parent", $Fields) || in_array("Guardian", $Fields))
     {
	     foreach($Fields as $k=>$d)
	     {
		  	if($d == "Parent" || $d == "Guardian")
		  	{
			  	if($d == "Parent")		$WithParent = 1;
			  	if($d == "Guardian")	$WithGuardian = 1;
			  	
				unset($Fields[$k]);
		  	}
	     }
     }
	$li->setFieldArray($Fields);
	$li->setTable("INTRANET_USER");
     	
     if ($TabID == "")
     {
         $conds = "";
     }
     else
     {
         $conds = "RecordType = $TabID AND ";
     }
     
     # parent
     if($TabID==3 && $parentLink!=""){
	     $li2 = new libdb();
	     $sqlParents = "SELECT DISTINCT UserID FROM INTRANET_USER as a, INTRANET_PARENTRELATION as b WHERE b.ParentID IS NOT NULL AND b.StudentID IS NOT NULL AND a.UserID=b.ParentID";
	     $r = $li2->returnVector($sqlParents,1);
	     
    	 $parentIDs = implode(",",$r);
	     
	     if($parentLink==1){
		     $conds.=" UserID IN ($parentIDs) AND ";
		 }
	     if($parentLink==2){
		     $conds .=" UserID NOT IN($parentIDs) AND";
		 }
	 }

     $li->setFilterSQL("WHERE $conds RecordStatus = '$filter' AND (UserLogin like '%$keyword%' OR UserEmail like '%$keyword%' OR ChineseName like '%$keyword%' OR EnglishName like '%$keyword%' OR ClassName LIKE '%$keyword%' OR ClassNumber LIKE '%$keyword%')");

     $url = "/file/export/eclass-user-".session_id()."-".time().".csv";
     #$lo = new libfilesystem();
	     $export_content = $li->getDumpDataWithSeparator_utf($WithParent, $WithGuardian);

     #$lo->file_write($li->getDumpDataWithSeparator(), $intranet_root.$url);
}
else # Export default format
{
     if ($TabID == "")
     {
         $tab_conds = "";
     }
     else
     {
         $tab_conds = "RecordType = $TabID AND ";
     }
     $conds = "WHERE $tab_conds RecordStatus = '$filter' AND (UserLogin like '%$keyword%' OR UserEmail like '%$keyword%' OR ChineseName like '%$keyword%' OR EnglishName like '%$keyword%' OR ClassName LIKE '%$keyword%' OR ClassNumber LIKE '%$keyword%')";
     if ($TabID == 1)	# Teacher
     {
         $col_count = 13;
         if($special_feature['ava_hkid'])	$col_count++;
         if((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) $col_count++;
         
         $sql = "SELECT UserID, UserLogin, '', UserEmail, EnglishName, ChineseName, NickName,
             Gender, MobileTelNo, FaxNo, Remark, TitleEnglish, TitleChinese";
        if((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) $sql .= ", CardID ";
		if($special_feature['ava_hkid'])	$sql .= ", HKID ";
		$sql .= " FROM INTRANET_USER $conds ORDER BY EnglishName";
		
     }
     else if ($TabID == 2)	# student
     {
	     
         $col_count = 12;
         if($special_feature['ava_hkid'])	$col_count++;
         if($special_feature['ava_strn'])	$col_count++;
         if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment'])) $col_count++;
         
         $sql = "SELECT UserID, UserLogin, '', UserEmail, EnglishName, ChineseName, NickName,
             Gender, MobileTelNo, FaxNo, Remark, DateOfBirth, ";
             $sql .= "IF(TRIM(WebSAMSRegNo)!='' AND LEFT(TRIM(WebSAMSRegNo),1)!='#',CONCAT('#',TRIM(WebSAMSRegNo)),TRIM(WebSAMSRegNo)) ";
            if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))
             	$sql .= ", CardID ";
         	if($special_feature['ava_hkid'])	$sql .= ", HKID ";
         	if($special_feature['ava_strn'])	$sql .= ", STRN ";
         $sql .=" FROM INTRANET_USER $conds ORDER BY ClassName, ClassNumber, EnglishName";
         
     }
     else	# parent
     {
         $col_count = 10;
         if($special_feature['ava_hkid'])	$col_count++;
         
         $sql = "SELECT UserID, UserLogin, '', UserEmail, EnglishName, ChineseName,
             Gender, MobileTelNo, FaxNo, Remark";
		if($special_feature['ava_hkid'])	$sql .= ", HKID ";
		$sql .= " FROM INTRANET_USER $conds ORDER BY EnglishName";
		
     }
     $li = new libdb();
     $row = $li->returnArray($sql,$col_count);
     
     if ($TabID == 3)	# parent
     {
         # Try to find children relation
         $sql = "SELECT b.ParentID, c.UserLogin, c.EnglishName FROM INTRANET_PARENTRELATION as b
                        LEFT OUTER JOIN INTRANET_USER as a ON b.ParentID = a.UserID
                        LEFT OUTER JOIN INTRANET_USER as c ON b.StudentID = c.UserID AND c.RecordType = 2
                        WHERE a.UserID IS NOT NULL AND c.UserID IS NOT NULL
                        ORDER BY a.UserID";
         $raw_relations = $li->returnArray($sql,3);
         $relations = array();
         $i = 0;
         $prev_id = 0;
         #print_r($raw_relations);
         while ($i < sizeof($raw_relations))
         {
                $temp = array();
                list($id,$login,$eng) = $raw_relations[$i];
                $prev_id = $id;
                while ($prev_id == $id)
                {
                       $temp[] = array($login,$eng);
                       $i++;
                       list($id,$login,$eng) = $raw_relations[$i];
                }
                $relations[$prev_id] = $temp;
         }
         #print_r($relations);

         $x = "UserLogin,Password,UserEmail,EnglishName,ChineseName,Gender,Mobile,Fax,Remarks";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","Gender","Mobile","Fax","Remarks");
         
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
         
		$x .= ",StudentLogin1,StudentEngName1,StudentLogin2,StudentEngName2,StudentLogin3,StudentEngName3";
		$exportColumn = array_merge($exportColumn, array("StudentLogin1","StudentEngName1","StudentLogin2","StudentEngName2","StudentLogin3","StudentEngName3"));
      }
     else if ($TabID == 1)		# teacher
     {
         $x = "UserLogin,Password,UserEmail,EnglishName,ChineseName,NickName,Gender,Mobile,Fax,Remarks,TitleEnglish,TitleChinese";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Mobile","Fax","Remarks","TitleEnglish","TitleChinese");
         if((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment']))
		{
			$x .= ",CardID";
			$exportColumn = array_merge($exportColumn, array("CardID"));
		}
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
     }
     else if ($TabID == 2)		# student
     {
         $x = "UserLogin,Password,UserEmail,EnglishName,ChineseName,NickName,Gender,Mobile,Fax,Remarks,DOB,WebSAMSRegNo";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Mobile","Fax","Remarks","DOB","WebSAMSRegNo");
		
        if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))
		{
			$x .= ",CardID";
			$exportColumn = array_merge($exportColumn, array("CardID"));
		}		
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
         
         if($special_feature['ava_strn'])
         {
			$x .= ",STRN";
			$exportColumn = array_merge($exportColumn, array("STRN"));
         }
     }
     /*
     else	# alumni
     {
         $x = "UserLogin,Password,UserEmail,EnglishName,FirstName,LastName,ChineseName,Title,Gender,Mobile,YearOfLeft";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","YearOfLeft");
         
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
     }
     */
     $x .= "\n";
  /*
     for($i=0; $i<sizeof($row); $i++)
     {
         $delim = "";
         $row_length = ($TabID==3? 11: $col_count);
         for($j=1; $j<$row_length; $j++)
         {
             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
             $x .= $delim.$row[$i][$j];
             $delim = ",";
         }
         #$x .= implode(",", $row[$i]);
         if ($TabID == 3)
         {
             $child_data = $relations[$row[$i][0]];
             for ($k=0; $k<sizeof($child_data); $k++)
             {
                  list($login, $eng) = $child_data[$k];
                  $x .= ",\"".intranet_undo_htmlspecialchars($login)."\",\"".intranet_undo_htmlspecialchars($eng)."\"";
             }

         }
         $x .= "\n";
     }
     */
     
     
     for($i=0; $i<sizeof($row); $i++)
     {
         $delim = "";
         //$row_length = ($TabID==3? 11: $col_count);
         $row_length = $col_count; 
         
         $child_data = $relations[$row[$i][0]];
         
         
         if($TabID!=3){
	         	for($j=1; $j<$row_length; $j++){
		         	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
		             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
		             $x .= $delim.$row[$i][$j];		             
		             $delim = ",";
	      		}
				#$x .= implode(",", $row[$i]);
				$utf_rows[] = $utf_row;
				unset($utf_row);
				$x .="\n";
	     }
	     
	     # parent
		 else{
			 	$child_data = $relations[$row[$i][0]];
			 	
			 	# Linked
			 	if($parentLink==1){
				 	if(sizeof($child_data)>0){
				      	for($j=1; $j<$row_length; $j++){
					      	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
				             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
				             $x .= $delim.$row[$i][$j];				             
				             $delim = ",";
	      				}
	      		        for ($k=0; $k<sizeof($child_data); $k++){
			                 list($login, $eng) = $child_data[$k];
			                 $x .= ",\"".intranet_undo_htmlspecialchars($login)."\",\"".intranet_undo_htmlspecialchars($eng)."\"";
			                 $utf_row[] = intranet_undo_htmlspecialchars($login);
			                 $utf_row[] = intranet_undo_htmlspecialchars($eng);
	            		}
// 	            		$utf_rows[] = $utf_row;
// 	            		unset($utf_row);
// 	            		$x .="\n";	 	
					}
				}
				# No Linked
			 	else if($parentLink==2){
				 	if(sizeof($child_data)<=0){
				      	for($j=1; $j<$row_length; $j++){
					      	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
				             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
				             $x .= $delim.$row[$i][$j];				             
				             $delim = ",";
	      				}
// 	      				$x .="\n";
// 	      				$utf_rows[] = $utf_row;
// 	            		unset($utf_row);
					}
				}
			 	# ALL 
			 	else if($parentLink==""){
				      	for($j=1; $j<$row_length; $j++){
					      	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
				             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
				             $x .= $delim.$row[$i][$j];				             
				             $delim = ",";
	      				}
	      		        for ($k=0; $k<sizeof($child_data); $k++){
			                 list($login, $eng) = $child_data[$k];
			                 $x .= ",\"".intranet_undo_htmlspecialchars($login)."\",\"".intranet_undo_htmlspecialchars($eng)."\"";
			                 $utf_row[] = intranet_undo_htmlspecialchars($login);
			                 $utf_row[] = intranet_undo_htmlspecialchars($eng);
	            		}	 
	            		
				}
				/*
				if($special_feature['ava_hkid'])
		         {
			         # add skip empty cell
			         for($k=sizeof($child_data); $k<3;$k++)	
			         {
				         $x .= ",\" \",\" \"";
			         	$utf_row[] = " ";
			         	$utf_row[] = " ";
			         }
			         
			         $x .= ",\"".intranet_undo_htmlspecialchars($row[$i]['HKID'])."\"";
			         $utf_row[] = intranet_undo_htmlspecialchars($row[$i]['HKID']);
		         }
		         */
		         $utf_rows[] = $utf_row;
		         unset($utf_row);	
			     $x .="\n";
	     }
     }

     #$url = "/file/export/eclass-user-".session_id()."-".time().".csv";
     #$lo = new libfilesystem();
     #$lo->file_write($x, $intranet_root.$url);
     
	 	$export_content = $lexport->GET_EXPORT_TXT($utf_rows, $exportColumn);		
	/*
     if ($TabID == "")
     {
         $tab_conds = "";
     }
     else
     {
         $tab_conds = "RecordType = $TabID AND ";
     }
     $conds = "WHERE $tab_conds RecordStatus = '$filter' AND (UserLogin like '%$keyword%' OR UserEmail like '%$keyword%' OR ChineseName like '%$keyword%' OR EnglishName like '%$keyword%')";
     if ($TabID == 1)
     {
         $col_count = 13;
         if($special_feature['ava_hkid'])	$col_count++;
         if((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) $col_count++;
         
         $sql = "SELECT UserID, UserLogin, '', UserEmail, EnglishName, FirstName, LastName, ChineseName,
             IF(Title IS NULL OR TRIM(Title) = '','',
             CASE Title
                         WHEN 0 THEN 'Mr.'
                         WHEN 1 THEN 'Miss'
                         WHEN 2 THEN 'Mrs.'
                         WHEN 3 THEN 'Ms.'
                         WHEN 4 THEN 'Dr.'
                         WHEN 5 THEN 'Prof.'
                         ELSE '' END),
             Gender, MobileTelNo, TitleEnglish, TitleChinese";
        if((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) $sql .= ", CardID ";
		if($special_feature['ava_hkid'])	$sql .= ", HKID ";
		$sql .= " FROM INTRANET_USER $conds ORDER BY EnglishName";
     }
     else if ($TabID == 2)
     {
	     
         $col_count = 13;
         if($special_feature['ava_hkid'])	$col_count++;
         if($special_feature['ava_strn'])	$col_count++;
         if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment'])) $col_count++;
         
         $sql = "SELECT UserID, UserLogin, '', UserEmail, EnglishName, FirstName, LastName, ChineseName,
             IF(Title IS NULL OR TRIM(Title) = '','',
             CASE Title
                         WHEN 0 THEN 'Mr.'
                         WHEN 1 THEN 'Miss'
                         WHEN 2 THEN 'Mrs.'
                         WHEN 3 THEN 'Ms.'
                         WHEN 4 THEN 'Dr.'
                         WHEN 5 THEN 'Prof.'
                         ELSE '' END),
             Gender, MobileTelNo, ";
             if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))
             $sql .= "CardID, ";
             $sql .= "DateOfBirth,
             IF(TRIM(WebSAMSRegNo)!='' AND LEFT(TRIM(WebSAMSRegNo),1)!='#',CONCAT('#',TRIM(WebSAMSRegNo)),TRIM(WebSAMSRegNo)) ";
         if($special_feature['ava_hkid'])	$sql .= ", HKID ";
         if($special_feature['ava_strn'])	$sql .= ", STRN ";
         $sql .=" FROM INTRANET_USER $conds ORDER BY ClassName, ClassNumber, EnglishName";
     }
     else
     {
         $col_count = 14;
         if($special_feature['ava_hkid'])	$col_count++;
         
         $sql = "SELECT UserID, UserLogin, '', UserEmail, EnglishName, FirstName, LastName, ChineseName,
             IF(Title IS NULL OR TRIM(Title) = '','',
             CASE Title
                         WHEN 0 THEN 'Mr.'
                         WHEN 1 THEN 'Miss'
                         WHEN 2 THEN 'Mrs.'
                         WHEN 3 THEN 'Ms.'
                         WHEN 4 THEN 'Dr.'
                         WHEN 5 THEN 'Prof.'
                         ELSE '' END),
             Gender, MobileTelNo, YearOfLeft ";
		if($special_feature['ava_hkid'])	$sql .= ", HKID ";
		$sql .= " FROM INTRANET_USER $conds ORDER BY EnglishName";
     }
     $li = new libdb();
     $row = $li->returnArray($sql,$col_count);
     
     if ($TabID == 3)
     {
         # Try to find children relation
         $sql = "SELECT b.ParentID, c.UserLogin, c.EnglishName FROM INTRANET_PARENTRELATION as b
                        LEFT OUTER JOIN INTRANET_USER as a ON b.ParentID = a.UserID
                        LEFT OUTER JOIN INTRANET_USER as c ON b.StudentID = c.UserID AND c.RecordType = 2
                        WHERE a.UserID IS NOT NULL AND c.UserID IS NOT NULL
                        ORDER BY a.UserID";
         $raw_relations = $li->returnArray($sql,3);
         $relations = array();
         $i = 0;
         $prev_id = 0;
         #print_r($raw_relations);
         while ($i < sizeof($raw_relations))
         {
                $temp = array();
                list($id,$login,$eng) = $raw_relations[$i];
                $prev_id = $id;
                while ($prev_id == $id)
                {
                       $temp[] = array($login,$eng);
                       $i++;
                       list($id,$login,$eng) = $raw_relations[$i];
                }
                $relations[$prev_id] = $temp;
         }
         #print_r($relations);

         $x = "UserLogin,Password,UserEmail,EnglishName,FirstName,LastName,ChineseName,Title,Gender,Mobile,StudentLogin1,StudentEngName1,StudentLogin2,StudentEngName2,StudentLogin3,StudentEngName3";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","StudentLogin1","StudentEngName1","StudentLogin2","StudentEngName2","StudentLogin3","StudentEngName3");
         
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
     }
     else if ($TabID == 1)
     {
         $x = "UserLogin,Password,UserEmail,EnglishName,FirstName,LastName,ChineseName,Title,Gender,Mobile,TitleEnglish,TitleChinese";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","TitleEnglish","TitleChinese");
         if((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment']))
		{
			$x .= ",CardID";
			$exportColumn = array_merge($exportColumn, array("CardID"));
		}
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
     }
     else if ($TabID == 2)
     {
         $x = "UserLogin,Password,UserEmail,EnglishName,FirstName,LastName,ChineseName,Title,Gender,Mobile";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile");
         
         if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))
		{
			$x .= ",CardID";
			$exportColumn = array_merge($exportColumn, array("CardID"));
		}
		$x .= ",DOB,WebSAMSRegNo";
		$exportColumn = array_merge($exportColumn,array("DOB","WebSAMSRegNo"));
		
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
         
         if($special_feature['ava_strn'])
         {
			$x .= ",STRN";
			$exportColumn = array_merge($exportColumn, array("STRN"));
         }
     }
     else
     {
         $x = "UserLogin,Password,UserEmail,EnglishName,FirstName,LastName,ChineseName,Title,Gender,Mobile,YearOfLeft";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","YearOfLeft");
         
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
     }
     $x .= "\n";     
     
     for($i=0; $i<sizeof($row); $i++)
     {
         $delim = "";
         $row_length = ($TabID==3? 11: $col_count);
         
         $child_data = $relations[$row[$i][0]];
         
         
         if($TabID!=3){
	         	for($j=1; $j<$row_length; $j++){
		         	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
		             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
		             $x .= $delim.$row[$i][$j];		             
		             $delim = ",";
	      		}
				#$x .= implode(",", $row[$i]);
				$utf_rows[] = $utf_row;
				unset($utf_row);
				$x .="\n";
	     }
	     
	     # parent
		 else{
			 	$child_data = $relations[$row[$i][0]];
			 	
			 	# Linked
			 	if($parentLink==1){
				 	if(sizeof($child_data)>0){
				      	for($j=1; $j<$row_length; $j++){
					      	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
				             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
				             $x .= $delim.$row[$i][$j];				             
				             $delim = ",";
	      				}
	      		        for ($k=0; $k<sizeof($child_data); $k++){
			                 list($login, $eng) = $child_data[$k];
			                 $x .= ",\"".intranet_undo_htmlspecialchars($login)."\",\"".intranet_undo_htmlspecialchars($eng)."\"";
			                 $utf_row[] = intranet_undo_htmlspecialchars($login);
			                 $utf_row[] = intranet_undo_htmlspecialchars($eng);
	            		}
// 	            		$utf_rows[] = $utf_row;
// 	            		unset($utf_row);
// 	            		$x .="\n";	 	
					}
				}
				# No Linked
			 	else if($parentLink==2){
				 	if(sizeof($child_data)<=0){
				      	for($j=1; $j<$row_length; $j++){
					      	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
				             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
				             $x .= $delim.$row[$i][$j];				             
				             $delim = ",";
	      				}
// 	      				$x .="\n";
// 	      				$utf_rows[] = $utf_row;
// 	            		unset($utf_row);
					}
				}
			 	# ALL 
			 	else if($parentLink==""){
				      	for($j=1; $j<$row_length; $j++){
					      	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
				             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
				             $x .= $delim.$row[$i][$j];				             
				             $delim = ",";
	      				}
	      		        for ($k=0; $k<sizeof($child_data); $k++){
			                 list($login, $eng) = $child_data[$k];
			                 $x .= ",\"".intranet_undo_htmlspecialchars($login)."\",\"".intranet_undo_htmlspecialchars($eng)."\"";
			                 $utf_row[] = intranet_undo_htmlspecialchars($login);
			                 $utf_row[] = intranet_undo_htmlspecialchars($eng);
	            		}	 
	            		
				}
				
				if($special_feature['ava_hkid'])
		         {
			         # add skip empty cell
			         for($k=sizeof($child_data); $k<3;$k++)	
			         {
				         $x .= ",\" \",\" \"";
			         	$utf_row[] = " ";
			         	$utf_row[] = " ";
			         }
			         
			         $x .= ",\"".intranet_undo_htmlspecialchars($row[$i]['HKID'])."\"";
			         $utf_row[] = intranet_undo_htmlspecialchars($row[$i]['HKID']);
		         }
		         $utf_rows[] = $utf_row;
		         unset($utf_row);	
			     $x .="\n";
	     }
     }

	 	$export_content = $lexport->GET_EXPORT_TXT($utf_rows, $exportColumn);	
	 */
}
// Output the file to user browser
#$filename = "eclass-user-".session_id()."-".time().".csv";
$filename = "eclass-user.csv";
/*
header("Pragma: public");
header("Expires: 0"); // set expiration time
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($export_content));
header("Content-Disposition: attachment; filename=\"".$filename."\"");
echo $export_content;
*/
//output2browser($export_content, $filename);

$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
#header("Location: $url");
?>
