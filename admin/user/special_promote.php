<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

$uid = (is_array($UserID)? $UserID[0] : $UserID);

$lu = new libuser($uid);
$lc = new libclass();
?>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
         if (obj.TargetClass.value=="")
         {
             alert("<?=$i_StudentPromotion_Alert_PromoteClass?>");
             obj.TargetClass.focus();
             return false;
         }
         if (obj.TargetClassNumber.value=="")
         {
             if (confirm("<?=$i_StudentPromotion_Alert_NoClassNumber?>"))
             {
                 return true;
             }
             else
             {
                 obj.TargetClassNumber.focus();
                 return false;
             }
         }
}
</SCRIPT>
<form name="form1" action="special_promote_update.php" method="post" onSubmit="return checkform(this)">
<?= displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, 'javascript:history.back()', $i_StudentPromotion_SpecialPromote, '') ?>
<?= displayTag("head_user_$intranet_session_language.gif", $msg) ?>
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=1>
<tr><td align=right><?=$i_UserLogin?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?=$i_UserChineseName?>:</td><td><?=$lu->ChineseName?></td></tr>
<tr><td align=right><?=$i_UserEnglishName?>:</td><td><?=$lu->EnglishName?></td></tr>
<tr><td align=right><?=$i_UserClassName?>:</td><td><?=$lu->ClassName?></td></tr>
<tr><td align=right><?=$i_UserClassNumber?>:</td><td><?=$lu->ClassNumber?></td></tr>
<tr><td align=right><?=$i_StudentPromotion_PromoteTo?>:</td><td><?=$lc->getSelectClass("name=TargetClass")?></td></tr>
<tr><td align=right><?=$i_StudentPromotion_NewClassNumber?>:</td><td><input type=text name=TargetClassNumber size=10></td></tr>
</table>
<?=$i_StudentPromotion_Notes_SpecialPromote?><br><Br>
<?=$i_StudentPromotion_ClickContinueToProceed?>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="<?=$image_path?>/admin/button/s_btn_continue_<?=$intranet_session_language?>.gif" border='0'>
 <a href="javascript:history.back()"><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=targetStudentID value="<?=$uid?>">
<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=keyword value="<?php echo $keyword; ?>">
<input type=hidden name=filter value="<?php echo $filter; ?>">
<input type=hidden name=TabID value="<?php echo $TabID; ?>">

</form>

<?
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>