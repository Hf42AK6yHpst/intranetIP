<?php
# using: 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libemail.php");
include_once("../../includes/libsendmail.php");
include_once("../../includes/libeclass.php");
include_once("../../lang/email.php");
include_once("../../includes/libwebmail.php");
include_once("../../includes/libftp.php");
include_once("../../includes/libldap.php");
include_once("../../templates/fileheader.php");
intranet_opendb();
$li = new libuser($uid);
if ($password1 == $password2)
{
         if ($intranet_authentication_method == "HASH")
         {
             $fieldname = "HashedPass = MD5('".$li->UserLogin.$password1.$intranet_password_salt."')";
         }
         else
         {
             $fieldname = "UserPassword = '$password1'";
         }
    $sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = '$uid'";
    $li->db_db_query($sql);

    $x = 5;
    #$lc = new libeclass();
    #$lc->eClassUserUpdatePassword($li->UserEmail,$password1);

    
    $lwebmail = new libwebmail();
    if ($lwebmail->has_webmail)
        $lwebmail->change_password($li->UserLogin,$password1,"iMail");
    
    if($plugin["imail_gamma"]==true)
	{
		include_once("../../includes/imap_gamma.php");
		$IMap = new imap_gamma(1);
		$userlogin = $li->UserLogin;
		$IMapEmail = trim($userlogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		if($IMap->is_user_exist($IMapEmail))
		{
			$IMap->change_password($IMapEmail, $password1);
		}
	}
    
    # FTP management
    if ($plugin['personalfile'])
    {
        $lftp = new libftp();
        if ($lftp->isFTP)
        {
            $lftp->changePassword($li->UserLogin,$password1,"iFolder");
        }
    }

    if ($intranet_authentication_method=='LDAP')
    {
        $lldap = new libldap();
        if ($lldap->isPasswordChangeNeeded())
        {
            $lldap->connect();
            $lldap->changePassword($UserLogin,$UserPassword);
        }
    }

    $le = new libsendmail();
    $le->send_email ($li->UserEmail,reset_password_title(),reset_password_body($li->Title, $li->EnglishName) );

    # SchoolNet
    if ($plugin['SchoolNet'])
    {
        $TabID = $li->RecordType;

        include_once("../../includes/libschoolnet.php");
        $lschoolnet = new libschoolnet();

        if ($TabID == 1)
        {
            # Param: array in ($userlogin, $password, $DOB, $gender, $cname, $ename, $tel, $mobile, $address, $email, $teaching)
            $sql = "SELECT UserLogin, UserPassword, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail, Teaching FROM INTRANET_USER WHERE UserID = '$uid'";
            $data = $li->returnArray($sql,11);
            $data[0][1] = $password1;
            $lschoolnet->addStaffUser($data);
        }
        else if ($TabID == 2)
        {
             #($userlogin, $password, $classname, $classnum, $DOB, $gender, $cname, $ename, $tel, $mobile, $address, $email)
             $sql = "SELECT UserLogin, UserPassword,ClassName, ClassNumber, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail FROM INTRANET_USER WHERE UserID = '$uid'";
             $data = $li->returnArray($sql,12);
             $data[0][1] = $password1;
             $lschoolnet->addStudentUser($data);
        }
        else if ($TabID == 3)
        {
        }
    }
}
else
{
    $x =6;
}

intranet_closedb();
include_once("../../templates/filefooter.php");

?>

<SCRIPT LANGUAGE=JAVASCRIPT>
opener.location.href = opener.location.href + '&msg=<?=$x?>';
self.close();
</SCRIPT>