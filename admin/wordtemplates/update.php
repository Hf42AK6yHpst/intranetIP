<?php
include_once("../../includes/global.php");
#include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libwordtemplates.php");

#intranet_opendb();

$lf = new libwordtemplates();
$base_dir = "$intranet_root/file/templates/";
if (!is_dir($base_dir))
{
     $lf->folder_new($base_dir);
}

$file_array = $lf->file_array;
if ($type >= sizeof($file_array) || $type < 0)
{
    header ("Location: index.php");
}
else
{
    $file_target = $file_array[$type];
    echo "1. $data ||\n";
    echo "2. ".stripslashes($data)."||\n";
    echo "3. ".intranet_htmlspecialchars($data)." ||\n";
    echo "4. ".intranet_undo_htmlspecialchars($data)."||\n";
    $data = intranet_htmlspecialchars($data);
    $lf->file_write($data,"$base_dir$file_target");
    header("Location: index.php?type=$type&msg=2");
}

?>