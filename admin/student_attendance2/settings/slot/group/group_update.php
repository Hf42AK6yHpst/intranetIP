<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
intranet_opendb();

if ($mode != 1 && $mode != 2)
{
    $mode = 0;
}
$li = new libdb();
$sql = "INSERT INTO CARD_STUDENT_ATTENDANCE_GROUP (GROUPID, Mode, DateInput, DateModified)
               VALUES ('$ClassID','$mode',now(),now())";
$li->db_db_query($sql);

if ($li->db_affected_rows()!=1)
{
    $sql = "UPDATE CARD_STUDENT_ATTENDANCE_GROUP
                   SET Mode = '$mode', DateModified = now() WHERE GROUPID = '$ClassID'";
    $li->db_db_query($sql);
}

if ($mode != 1)
{
    # Clear detail timetable
    $sql = "DELETE FROM CARD_STUDENT_GROUP_PERIOD_TIME
                   WHERE GROUPID = '$ClassID'";
    # Clear special timetable
    $sql2= "DELETE FROM CARD_STUDENT_SPECIFIC_DATE_TIME_GROUP WHERE GROUPID='$ClassID' ";
    $li->db_db_query($sql);
    $li->db_db_query($sql2);
    $url = "group.php";
}
else
{
    $url = "group_edit.php";
}

intranet_closedb();
header("Location: $url?ClassID=$ClassID&msg=2");
?>