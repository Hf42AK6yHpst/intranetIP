<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();
$nonSchoolDay=($non_school_day==""||$non_school_day==0)?0:1;

if($DayType==1 || $DayType==2||$DayType==0){  # WeekDay or # Cycle Day
		$sql = "INSERT IGNORE INTO CARD_STUDENT_CLASS_PERIOD_TIME
		(ClassID,DayType, DayValue, MorningTime, LunchStart, LunchEnd, LeaveSchoolTime, NonSchoolDay,DateInput, DateModified)
		VALUES ('$ClassID','$DayType','$DayValue','$normal_am','$normal_lunch','$normal_pm','$normal_leave','$nonSchoolDay',now(),now())";
		
		$li->db_db_query($sql);

		if ($li->db_affected_rows()==0)
		{
		    $sql = "UPDATE CARD_STUDENT_CLASS_PERIOD_TIME
		                   SET MorningTime = '$normal_am', LunchStart = '$normal_lunch',
		                       LunchEnd = '$normal_pm', LeaveSchoolTime = '$normal_leave',NonSchoolDay='$nonSchoolDay',
		                       DateModified= now()
		                   WHERE ClassID='$ClassID' AND DayType='$DayType' AND DayValue ='$DayValue'";
		    $li->db_db_query($sql);
		}

}
else { # Special Day
$sql = "INSERT IGNORE INTO CARD_STUDENT_SPECIFIC_DATE_TIME(ClassID,RecordDate,MorningTime,LunchStart,LunchEnd,LeaveSchoolTime,NonSchoolDay,DateInput,DateModified) VALUES('$ClassID','$TargetDate','$normal_am',
'$normal_lunch','$normal_pm','$normal_leave','$nonSchoolDay',now(),now())";
$li->db_db_query($sql);
		if ($li->db_affected_rows()==0)
		{
		    $sql = "UPDATE CARD_STUDENT_SPECIFIC_DATE_TIME
		                   SET MorningTime = '$normal_am', LunchStart = '$normal_lunch',
		                       LunchEnd = '$normal_pm', LeaveSchoolTime = '$normal_leave',NonSchoolDay='$nonSchoolDay',
		                       DateModified= now()
		                   WHERE ClassID='$ClassID' AND RecordDate='$TargetDate'";
		    $li->db_db_query($sql);
		}

}

intranet_closedb();
header("Location: class_edit.php?ClassID=$ClassID&msg=2#$DayType");
?>