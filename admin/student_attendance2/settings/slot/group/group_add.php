<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
//include_once("../../../../../includes/libspecialgroup.php");
//include_once("../../../../../includes/libcardstudentattendgroup.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

$lc = new libcardstudentattend2();
//$objSpecialGroup = new libcardstudentattendgroup();

$lc->retrieveSettings();
//$normal_time_array = $objSpecialGroup->getGroupTimeArray($ClassID,0,0);
$normal_time_array = $lc->getGroupTimeArray($ClassID,0,0);
list($normal_am, $normal_lunch, $normal_pm, $normal_leave) = $normal_time_array;
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
//$days = $objSpecialGroup->getDayValueWithoutSpecificForGroup($ClassID,$type);
$days = $lc->getDayValueWithoutSpecificForGroup($ClassID,$type);

if ($type==2)  # Cycle
{
    $select_title = $i_StudentAttendance_CycleDay.":";
    $select_day = getSelectByValue($days,"name=DayValue");
}
else if ($type==1)  # Week
{
    $select_title = $i_StudentAttendance_WeekDay.":";
    $select_day = "<SELECT name=DayValue>\n";
    $select_day .= "<OPTION value=''>-- $button_select --</OPTION>";
    for ($i=0; $i<sizeof($days); $i++)
    {
         $word = $i_DayType0[$days[$i]];
         $select_day .= "<OPTION value='".$days[$i]."'>".$word."</OPTION>\n";
    }
    $select_day .= "</SELECT>\n";
}
else if($type==0){ # Normal
		$select_title="$i_StudentAttendance_NormalDays";
		$select_day="";

}
else # Special
{
    $select_title = "$i_StudentAttendance_TimeSlot_SpecialDay";
	  $select_day="<input type=text name=TargetDate value='".date('Y-m-d')."' size=10>\n<script language=\"JavaScript\" type=\"text/javascript\">\n";
	  $select_day.=" <!--\n";
    $select_day.=" startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');\n";
    $select_day.="//-->\n";
    $select_day.="</script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>";
}

?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_free";
	css_array[1] = "dynCalendar_half";
	css_array[2] = "dynCalendar_full";
	var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
<!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].TargetDate.value = dateValue;
          }
function checkForm(formObj){
		if(checkForm1(formObj))
			return checkForm2(formObj);
		return false;
}
function checkForm1(formObj){
			targetDayObj = formObj.TargetDate;
			if(targetDayObj!=null){
					obj=formObj.TargetDate;
					if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
					return true;
 			}
			else{
					dayValueObj =formObj.DayValue;
					if(dayValueObj!=null){
						i= formObj.DayValue.selectedIndex;
						if(i==0){
							alert("<?if($type==2) echo $i_StudentAttendance_Warn_Please_Select_CycleDay;else if($type==1) echo $i_StudentAttendance_Warn_Please_Select_WeekDay;?>");
							return false;
						}else{
							return true;
						}
					}
			}
			
			return false;
			
}
function checkForm2(formObj){
	if(formObj.non_school_day!=null && formObj.non_school_day.checked==true)
			return true;

	amObj = formObj.normal_am;
	lunchObj=formObj.normal_lunch;
	pmObj = formObj.normal_pm;
	leaveObj = formObj.normal_leave;

	if(amObj!=null){
		am = amObj.value;
		if(!isValidTimeFormat(am)){
			amObj.focus();
			return false;
		}
	}
	if(lunchObj!=null){
		lunch = lunchObj.value;
		if(!isValidTimeFormat(lunch)){
			lunchObj.focus();
			return false;
		}
	}
	if(pmObj!=null){
		pm = pmObj.value;
		if(!isValidTimeFormat(pm)){
			pmObj.focus();
			return false;
		}
	}
	if(leaveObj!=null){
		leave = leaveObj.value;
		if(!isValidTimeFormat(leave)){
			leaveObj.focus();
			return false;
		}
	}

	if(isValidValues(amObj,lunchObj,pmObj,leaveObj))
		return true;
	return false;
	}
		function isValidTimeFormat(timeVal){
  	var re = new RegExp("^(([0-1][0-9])|2[0-3]):[0-5][0-9]$");
		if (!timeVal.match(re)) {
				alert("<?=$i_StudentAttendance_Warn_Invalid_Time_Format?>");
				return false;
		}
		return true;
}
function isValidValues(amObj,lunchObj,pmObj,leaveObj){
			if(amObj!=null){
					if(lunchObj!=null && amObj.value>=lunchObj.value){ 
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_LunchStart?>");
						amObj.focus();
						return false;
					}
					else if(pmObj!=null && amObj.value>=pmObj.value){
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_PMStart?>");
						amObj.focus();
						return false;
					}
					else if(leaveObj!=null && amObj.value>=leaveObj.value){
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_SchoolEnd?>");
						amObj.focus();
						return false;
					}
			}
			if(lunchObj!=null){
					if(pmObj!=null && lunchObj.value>=pmObj.value){
						alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_PMStart?>");
						lunchObj.focus();
						return false;
					}
					else if(leaveObj!=null && lunchObj.value>=leaveObj.value){
						alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_SchoolEnd?>");
						lunchObj.focus();
						return false;
					}
			}
			if(pmObj!=null){
					if(leaveObj!=null && pmObj.value>=leaveObj.value){
							alert("<?=$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd?>");
							pmObj.focus();
							return false;
					}
			}
			return true;
}
	function setForm(formObj){
		nonSchoolDayObj = formObj.non_school_day;
		amObj = formObj.normal_am;
		lunchObj=formObj.normal_lunch;
		pmObj = formObj.normal_pm;
		leaveObj = formObj.normal_leave;
		if(nonSchoolDayObj!=null){
			if(nonSchoolDayObj.checked){
				if(amObj!=null) amObj.disabled=true;
				if(lunchObj!=null) lunchObj.disabled=true;
				if(pmObj!=null) pmObj.disabled=true;
				if(leaveObj!=null) leaveObj.disabled=true;
			}else{
				if(amObj!=null) amObj.disabled=false;
				if(lunchObj!=null) lunchObj.disabled=false;
				if(pmObj!=null) pmObj.disabled=false;
				if(leaveObj!=null) leaveObj.disabled=false;
			}
		}
	}
		function resetForm(formObj){
			formObj.reset();
			setForm(formObj);
	}
// -->
</script>
<form name="form1" method="POST" ACTION="group_add_update.php" onSubmit="return checkForm(this)">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSlotSettings,'../',$i_StudentAttendance_Menu_Slot_Group,'index.php',$button_add,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
      <table width=500 border=0 cellpadding=2 cellspacing=0>
        <tr>
          <td ><?=$select_title?></td>
          <td><?=$select_day?></td>
        </tr>
      <?
      ### Mode 1,3,4
      if ($hasAM) {
      ?>
        <tr>
          <td ><?=$i_StudentAttendance_SetTime_AMStart?>:</td>
          <td><input type=text size=5 maxlength=5 name=normal_am value="<?=$normal_am?>"></td>
        </tr>
      <? } ?>
      <?
      ### Mode 3,4
      if ($hasLunch) {
      ?>
        <tr>
          <td ><?=$i_StudentAttendance_SetTime_LunchStart?></td>
          <td><input type=text size=5 maxlength=5 name=normal_lunch value="<?=$normal_lunch?>"></td>
        </tr>
      <? } ?>
      <?
      ### Mode 2,3,4
      if ($hasPM) {
      ?>
        <tr>
          <td ><?=$i_StudentAttendance_SetTime_PMStart?></td>
          <td><input type=text size=5 maxlength=5 name=normal_pm value="<?=$normal_pm?>"></td>
        </tr>
      <? } ?>
      <?
      ### Mode 1,2,3,4
      ?>
        <tr>
          <td ><?=$i_StudentAttendance_SetTime_SchoolEnd?></td>
          <td><input type=text size=5 maxlength=5 name=normal_leave value="<?=$normal_leave?>"></td>
        </tr>
        <? if($type!=0){?>
        <tr>
          <td ><?=$i_StudentAttendance_NonSchoolDay?></td>
          <td><input type=checkbox onClick="setForm(this.form)" name=non_school_day value="1"></td>
        </tr>
        <?}?>
      </table>
<br>
<p><?=$i_StudentAttendance_Slot_SettingsDescription?></p>
</blockquote>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit()?>
<a href="javascript:resetForm(document.form1)"><img src='/images/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif' border='0'></a>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=ClassID value="<?=$ClassID?>">
<input type=hidden name=DayType value="<?=$type?>">
<? if ($type==0) { 
 echo "<input type=hidden name=DayValue value=\"0\">";
 } ?>

</form>

<?
include_once("../../../../../templates/adminfooter.php");
intranet_closedb();
?>