<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
intranet_opendb();

if ($mode != 1 && $mode != 2)
{
    $mode = 0;
}
$li = new libdb();
$sql = "INSERT INTO CARD_STUDENT_CLASS_SPECIFIC_MODE (ClassID, Mode, DateInput, DateModified)
               VALUES ('$ClassID','$mode',now(),now())";
$li->db_db_query($sql);

if ($li->db_affected_rows()!=1)
{
    $sql = "UPDATE CARD_STUDENT_CLASS_SPECIFIC_MODE
                   SET Mode = '$mode', DateModified = now() WHERE ClassID = '$ClassID'";
    $li->db_db_query($sql);
}

if ($mode != 1)
{
    # Clear detail timetable
    $sql = "DELETE FROM CARD_STUDENT_CLASS_PERIOD_TIME
                   WHERE ClassID = '$ClassID'";
    # Clear special timetable
    $sql2= "DELETE FROM CARD_STUDENT_SPECIFIC_DATE_TIME WHERE ClassID='$ClassID' ";
    $li->db_db_query($sql);
    $li->db_db_query($sql2);
    $url = "class.php";
}
else
{
    $url = "class_edit.php";
}

intranet_closedb();
header("Location: $url?ClassID=$ClassID&msg=2");
?>