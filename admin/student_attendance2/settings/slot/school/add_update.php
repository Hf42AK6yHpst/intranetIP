<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
intranet_opendb();

$nonSchoolDay=($non_school_day==""||$non_school_day==0)?0:1;
$li = new libdb();

if($DayType!=3){
		$sql = "INSERT IGNORE INTO CARD_STUDENT_PERIOD_TIME
		(DayType, DayValue, MorningTime, LunchStart, LunchEnd, LeaveSchoolTime, NonSchoolDay,DateInput, DateModified)
		VALUES ('$DayType','$DayValue','$normal_am','$normal_lunch','$normal_pm','$normal_leave','$nonSchoolDay',now(),now())";
		$li->db_db_query($sql);
		
		if ($li->db_affected_rows()==0)
		{
		    $sql = "UPDATE CARD_STUDENT_PERIOD_TIME
		                   SET MorningTime = '$normal_am', LunchStart = '$normal_lunch',
		                       LunchEnd = '$normal_pm', LeaveSchoolTime = '$normal_leave',
		                       NonSchoolDay='$nonSchoolDay',
		                       DateModified= now()
		                   WHERE DayType='$DayType' AND DayValue ='$DayValue'";
		    $li->db_db_query($sql);
		}
}
else{
	$sql = "INSERT IGNORE INTO CARD_STUDENT_SPECIFIC_DATE_TIME(ClassID,RecordDate,MorningTime,LunchStart,LunchEnd,LeaveSchoolTime,NonSchoolDay,DateInput,DateModified) VALUES(0,'$TargetDate','$normal_am',
				'$normal_lunch','$normal_pm','$normal_leave','$nonSchoolDay',now(),now())";
	$li->db_db_query($sql);
		if ($li->db_affected_rows()==0)
		{
		    $sql = "UPDATE CARD_STUDENT_SPECIFIC_DATE_TIME
		                   SET MorningTime = '$normal_am', LunchStart = '$normal_lunch',
		                       LunchEnd = '$normal_pm', LeaveSchoolTime = '$normal_leave',NonSchoolDay='$nonSchoolDay',
		                       DateModified= now()
		                   WHERE ClassID=0 AND RecordDate='$TargetDate'";
		    $li->db_db_query($sql);
		}
}
intranet_closedb();
header("Location: index.php?msg=1#$DayType");
?>