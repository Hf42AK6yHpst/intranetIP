<?
// use by kenneth chung
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

$content_basic = trim(get_file_content("$intranet_root/file/stattend_basic.txt"));
$attendance_mode = $content_basic;

$time_table = trim(get_file_content("$intranet_root/file/time_table_mode.txt"));
$time_table_mode = $time_table; 	### 0 - Time Slot, 1 - Time Session

$student_profile_input = trim(get_file_content("$intranet_root/file/disallow_student_profile_input.txt"));

$student_attend_default_status = trim(get_file_content("$intranet_root/file/studentattend_default_status.txt"));


if (($time_table_mode == 0)||($time_table_mode == ''))
{
	$table_content = "<tr><td align=right>$i_StudentAttendance_Menu_Time_Mode".": "."</td><td><input type=radio name=time_table value=0 checked>$i_StudentAttendance_Menu_Time_Input_Mode</td></tr>";
	$table_content .= "<tr><td></td><td><input type=radio name=time_table value=1>$i_StudentAttendance_Menu_Time_Session_Mode</td></tr>";
}
if ($time_table_mode == 1)
{
	$table_content = "<tr><td align=right>$i_StudentAttendance_Menu_Time_Mode".": "."</td><td><input type=radio name=time_table value=0>$i_StudentAttendance_Menu_Time_Input_Mode</td></tr>";
	$table_content .= "<tr><td></td><td><input type=radio name=time_table value=1 checked>$i_StudentAttendance_Menu_Time_Session_Mode</td></tr>";
}
	
$select_mode = "<SELECT name=attendance_mode>
<OPTION value=0 ".($attendance_mode==0?"SELECTED":"").">$i_StudentAttendance_AttendanceMode_AM_Only</OPTION>
<OPTION value=1 ".($attendance_mode==1?"SELECTED":"").">$i_StudentAttendance_AttendanceMode_PM_Only</OPTION>
<OPTION value=2 ".($attendance_mode==2?"SELECTED":"").">$i_StudentAttendance_AttendanceMode_WD_Lunch</OPTION>
<OPTION value=3 ".($attendance_mode==3?"SELECTED":"").">$i_StudentAttendance_AttendanceMode_WD_NoLunch</OPTION>
</SELECT>
";

$student_default_status = "<SELECT name='student_default_status'>\n
														<OPTION value='".CARD_STATUS_ABSENT."' ".($student_attend_default_status==CARD_STATUS_ABSENT? "SELECTED":"").">$i_StudentAttendance_Status_PreAbsent</OPTION>\n
														<OPTION value='".CARD_STATUS_PRESENT."' ".($student_attend_default_status==CARD_STATUS_PRESENT? "SELECTED":"").">$i_StudentAttendance_Status_OnTime</OPTION>\n
										      </SELECT>\n
													";


?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_SmartCard_SystemSettings,'../',$i_general_BasicSettings,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 method=POST action=update.php>
<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_StudentAttendance_AttendanceMode; ?>:</td><td><?=$select_mode?></td></tr>
<?=$table_content?>
<BR>
<tr><td align=right><?=$i_StudentAttendance_default_attend_status?>:</td><Td><?=$student_default_status?></td></tr>
<tr><td align=right><?=$i_StudentAttendance_Setting_DisallowStudentProfileInput?>:</td><Td><input type=checkbox name='student_profile_input'  value="1" <?=($student_profile_input==1? " CHECKED":"")?>></td></tr>
</table>
<br>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>


</form>
<?
include_once("../../../../templates/adminfooter.php");
?>