<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

#class used
$LICLASS = new libclass();

# build select individual add or class
$select_type_table = "";
$x = "<SELECT name=select_type onChange=checkGet(this.form,'new.php')>\n";
if ($select_type=="")
{
    $x .= "<OPTION value=\"\"> -- $button_select -- </OPTION>\n";
}
$x .= "<OPTION value=1 ".($select_type=="1"?"SELECTED":"").">$i_SmartCard_Settings_Lunch_New_Type_Class</OPTION>\n";
$x .= "<OPTION value=2 ".($select_type=="2"?"SELECTED":"").">$i_SmartCard_Settings_Lunch_New_Type_Student</OPTION>\n";
$x .= "</SELECT>\n";
$select_type_table = $x;

# build selection of class - for class
if($select_type=="1"){
        $select_all_class_table = "";
        $class_list = $LICLASS->getClassList();
        $current_level = $class_list[0][2];

        $x = "<table>\n";
        $x .= "<tr><td>";
        for($i=0; $i<sizeOf($class_list); $i++){
                list($ClassID, $ClassName, $ClassLevelID) = $class_list[$i];
                $x .= ($current_level<>$ClassLevelID)?"</td></tr>\n<tr><td>":"";
                $x .= "<input name=class_name[] type=checkbox value=\"$ClassName\">".$ClassName.toolBarSpacer();
                $current_level = ($current_level<>$ClassLevelID)?$ClassLevelID:$current_level;
        }
        $x .= "</table>\n";

        $select_all_class_table = $x;
        $str_instruction = "$i_SmartCard_Settings_Lunch_Instruction_Class<br><br>";
}
else if($select_type=="2"){     # build selection of class - for student

        $select_class_table = "";
        $class_list = $LICLASS->getClassList();
        $x = "<SELECT name=select_class onChange=checkGet(this.form,'new.php')>\n";
        $x .= "<OPTION value=\"\"> -- $button_select -- </OPTION>\n";
        for($i=0; $i<sizeOf($class_list); $i++){
                list($ClassID, $ClassName, $ClassLevelID) = $class_list[$i];
                $selected = ($ClassName==$select_class)?"SELECTED":"";
                $x .= "<OPTION value=\"$ClassName\" $selected>$ClassName</OPTION>\n";
        }
        $x .= "<SELECT>\n";
        $select_class_table = $x;
}

#build student
if($select_class<>"" AND $select_type=="2"){
        $select_student_table = "";

        $name_field = getNameFieldByLang("a.");
        $sql = "SELECT a.UserID, $name_field, a.ClassNumber FROM INTRANET_USER as a LEFT OUTER JOIN CARD_STUDENT_LUNCH_ALLOW_LIST as b ON (a.UserID=b.StudentID) WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND a.ClassName = '$select_class' AND b.StudentID IS NULL ORDER BY a.ClassNumber, a.EnglishName";
        $student_list = $LICLASS->returnArray($sql,3);

    $checkall = "<input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'student_id[]'):setChecked(0,this.form,'student_id[]')>";

        $x = "<table border=0 cellpadding=3 cellspacing=0>\n";
        $x .= "<tr><td class=tableTitle_new>$checkall</td><td class=tableTitle_new ><u>$i_UserClassNumber</u></td><td class=tableTitle_new><u>$i_UserName</u></td></tr>\n";
        for($i=0; $i<sizeOf($student_list); $i++){
                list($UserID, $name_field, $ClassNumber) = $student_list[$i];
                $x .= "<tr><td><input name=student_id[] type=checkbox value=\"$UserID\"></td><td>$ClassNumber</td><td>$name_field</td></tr>\n";
        }
        if (sizeof($student_list)==0)
        {
            $x .= "<tr><td align=center colspan=3>$i_SmartCard_Settings_NoStudentsAvailableForSelection</td></tr>";
        }
        $x .= "</table>\n";

        $select_student_table = $x;
        $str_instruction = "$i_SmartCard_Settings_Lunch_Instruction_Student<br><br>";
}

?>

<script language="javascript">
function checkform(obj){
        <?
                if ($select_type=="1"){
                        echo "var element = 'class_name[]'";
                }
                else if ($select_type=="2"){
                        echo "var element = 'student_id[]'";
                }
        ?>

        if(countChecked(obj,element)==0) {
                alert(globalAlertMsg2);
                return false
        }

    return true;
}
</script>

<form name="form1" method="post" action="new_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_LunchSettings,'../', $i_SmartCard_Settings_Lunch_List_Title, 'index.php', $button_new, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=right><?=$i_SmartCard_Settings_Lunch_New_Type?>:&nbsp</td><td><?=$select_type_table?></td></tr>
<tr><td align=center colspan=2>&nbsp;</td></tr>
<? if($select_type==1) { ?>
<tr><td align=center colspan=2><?=$select_class_table?></td></tr>
<? }
 else if ($select_type==2) { ?>
<tr><td align=right><?=($select_type==2)?$i_ClassName.":":""?>&nbsp;</td><td><?=$select_class_table?></td></tr>
<? } ?>
<? if ($select_type<>"") { ?>
<tr><td colspan=2><hr size=1 class="hr_sub_separator"></td></tr>
<? } ?>
<tr><td align=left colspan=2><?=$str_instruction?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=center><?=$select_student_table?></td></tr>
<tr><td align=center><?=$select_all_class_table?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?
        if( ($select_class<>""&&sizeof($student_list)!=0) || $select_type=="1")
                echo btnSubmit() ." ". btnReset() . toolBarSpacer();
?>
<a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>



</form>
<?
include_once("../../../../../templates/adminfooter.php");
?>