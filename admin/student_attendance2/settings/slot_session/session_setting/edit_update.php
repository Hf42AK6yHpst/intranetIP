<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();
$lclass = new libclass();
/*
if($SetOtherClasses == 1)
{
	if(($selected_type == 1)||($selected_type == 2))
	{
		$class_list = implode(",",$class);
		if(sizeof($class)>0)
		{
			for($i=0; $i<sizeof($class); $i++)
			{
				list($target_class) = $class[$i];
				$sql = "INSERT IGNORE INTO CARD_STUDENT_CLASS_SPECIFIC_MODE (ClassID, Mode, DateInput, DateModified) VALUES ($target_class, 1, 'NOW()', 'NOW()')";
				$lclass->db_db_query($sql);
				$sql = "UPDATE CARD_STUDENT_CLASS_SPECIFIC_MODE SET Mode = 1, DateModified = 'NOW()' WHERE ClassID = $target_class";
				$lclass->db_db_query($sql);
				
				$sql = "SELECT RecordID, ClassID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE ClassID = $target_class AND DayType = $selected_type AND DayValue = $DayValue";
				$temp_class = $lclass->returnArray($sql,2);
				
				$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR (ClassID, DayType, DayValue, SessionID, DateInput, DateModified) VALUES ($target_class, $selected_type, '$DayValue', $s_id, 'NOW()', 'NOW()')";
				$lclass->db_db_query($sql);
				if(sizeof($temp_class)>0)
				{
					for($j=0; $j<sizeof($temp_class); $j++)
					{
						list($r_id, $c_id) = $temp_class[$j];
						$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = $s_id , DateModified = 'NOW()' WHERE RecordID = $r_id";
						$lclass->db_db_query($sql);
					}
				}
			}
		}
	}
	if($selected_type == 3)
	{
		$class_list = implode(",",$class);
		if(sizeof($class)>0)
		{
			for($i=0; $i<sizeof($class); $i++)
			{
				list($target_class) = $class[$i];
				$sql = "INSERT IGNORE INTO CARD_STUDENT_CLASS_SPECIFIC_MODE (ClassID, Mode, DateInput, DateModified) VALUES ($target_class, 1, 'NOW()', 'NOW()')";
				$lclass->db_db_query($sql);
				$sql = "UPDATE CARD_STUDENT_CLASS_SPECIFIC_MODE SET Mode = 1, DateModified = 'NOW()' WHERE ClassID = $target_class";
				$lclass->db_db_query($sql);
				
				$sql = "SELECT RecordID, ClassID FROM CARD_STUDENT_TIME_SESSION_DATE WHERE ClassID = $target_class AND RecordDate = $TargetDate";
				$temp_class = $lclass->returnArray($sql,2);
				
				$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_DATE (ClassID, RecordDate, SessionID, DateInput, DateModified) VALUES ($target_class, '$TargetDate', $s_id, 'NOW()', 'NOW()')";
				$lclass->db_db_query($sql);
				
				if(sizeof($temp_class)>0)
				{
					for($j=0; $j<sizeof($temp_class); $j++)
					{
						list($r_id, $c_id) = $temp_class[$j];
						$sql = "UPDATE CARD_STUDENT_TIME_SESSION_DATE SET SessionID = $s_id, DateModified = 'NOW()' WHERE RecordID = $r_id";
						$lclass->db_db_query($sql);
					}
				}
			}
		}
	}
}
*/
/*
$temp_arr = array(array(1,"$i_StudentAttendance_Weekday_Specific"),array(2,"$i_StudentAttendance_Cycleday_Specific"),array(3,"$i_StudentAttendance_SpecialDay"));
echo $selected_type."<BR>";
if($selected_type == 1)
{
	echo $DayValue."<BR>";
	
}

$class_list = implode(",",$class);
echo $class_list;
### Update the Time Table Mode
$sql = "UPDATE CARD_STUDENT_CLASS_SPECIFIC_MODE SET Mode = 1 WHERE ClassID IN ($class_list)";
//echo $sql."<BR>";
//$lclass->db_db_query($sql);
*/


if($s_id!="")
{
	if(($non_school_day==1)||($non_school_day==on)||($non_school_day==ON))
	{
		$temp = 1;
		$am_time = "";
		$lunch_time = "";
		$pm_time = "";
		$leave_time = "";
	}
	else
	{
		$temp = 0;
		if(($am_hours == "") && ($am_minutes == ""))
		{
			$am_time = "";
		}
		else
		{
			$am_time = $am_hours.":".$am_minutes;
		}
		if(($lunch_hours == "") && ($lunch_minutes == ""))
		{
			$lunch_time = "";
		}
		else
		{
			$lunch_time = $lunch_hours.":".$lunch_minutes;
		}
		if(($pm_hours == "") && ($pm_minutes == ""))
		{
			$pm_time = "";;
		}
		else
		{
			$pm_time = $pm_hours.":".$pm_minutes;
		}
		if(($leave_hours == "") && ($leave_minutes == ""))
		{
			$leave_time = "";
		}
		else
		{
			$leave_time = $leave_hours.":".$leave_minutes;
		}
	}
	
	//echo $s_id;
	$sql = "UPDATE 
					CARD_STUDENT_TIME_SESSION 
			SET
					SessionName = '$s_name',
					MorningTime = '$am_time',
					LunchStart = '$lunch_time',
					LunchEnd = '$pm_time',
					LeaveSchoolTime = '$leave_time',
					NonSchoolDay = $temp
			WHERE
					SessionID = $s_id
			";
	//echo $sql;
	$lclass->db_db_query($sql);

	if(($non_school_day==1)||($non_school_day==on)||($non_school_day==ON))
	{
		$sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE ClassID = 0 AND DayType = 0 AND DayValue = 0";
		$result = $lclass->returnVector($sql);
		if(sizeof($result)>0)
		{
			list ($curr_session) = $result[0];
			if($curr_session == $s_id)
			{
				$sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION WHERE NonSchoolDay = 0 AND RecordStatus = 1 ORDER BY SessionID LIMIT 0,1";
				$temp= $lclass->returnVector($sql);
				if(sizeof($temp)>0)
					list($new_session) = $temp[0];
				$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = $new_session WHERE ClassID = 0 AND DayType = 0 AND DayValue = 0";
				$lclass->db_db_query($sql); 
			}
		}
	}
}

Header ("Location: index.php?msg=2");
intranet_closedb();
?>