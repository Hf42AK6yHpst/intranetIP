<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();
$lclass = new libclass();

$cnt = 0;


for($i=0; $i<$no_of_element; $i++)
{
	if((${"non_school_day_$i"}==on)||(${"non_school_day_$i"}==ON)||(${"non_school_day_$i"}==1))
	{
		$temp = 1;
		${"am_time_$i"} = "";
		${"lunch_time_$i"} = "";
		${"pm_time_$i"} = "";
		${"leave_time_$i"} = "";
	}
	else
	{
		$temp = 0;
		${"am_time_$i"} = ${"am_hours_$i"}.":".${"am_minutes_$i"};
		${"lunch_time_$i"} = ${"lunch_hours_$i"}.":".${"lunch_minutes_$i"};
		${"pm_time_$i"} = ${"pm_hours_$i"}.":".${"pm_minutes_$i"};
		${"leave_time_$i"} = ${"leave_hours_$i"}.":".${"leave_minutes_$i"};
	}
	
	$sql = "INSERT INTO CARD_STUDENT_TIME_SESSION VALUES ('', '".${"s_name_$i"}."', '".${"am_time_$i"}."', '".${"lunch_time_$i"}."', '".${"pm_time_$i"}."', '".${"leave_time_$i"}."', $temp, '', 1, NOW(), NOW())";
	//echo $sql;
	
	$result = $lclass->db_db_query($sql);
	
	if($result)
	{
		$cnt++;
	}
}

$sql = "SELECT RecordID, SessionID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE ClassID = 0 AND DayType = 0 AND DayValue = 0";
$temp = $lclass->returnArray($sql,2);
if(sizeof($temp)==0)
{
	$sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION WHERE NonSchoolDay = 0 AND RecordStatus = 1 ORDER BY SessionID LIMIT 0,1";
	$default = $lclass->returnVector($sql);
	if(sizeof($default)>0)
		list ($default_session) = $default[0];
	$sql = "INSERT INTO CARD_STUDENT_TIME_SESSION_REGULAR VALUES ('', 0, 0, 0, $default_session, '', '', NOW(), NOW())";
	$lclass->db_db_query($sql);
}

if($cnt == $no_of_element)
{
	Header ("Location: index.php?msg=1");
	intranet_closedb();
}
else
{
	Header ("Location: index.php?msg=12");
	intranet_closedb();
}
?>
