<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");


$li = new libdb();
$lc = new libcardstudentattend2();
$lc->retrieveSettings();

$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

intranet_opendb();

if(($type==0)||($type==1)||($type==2))
{
	$sql = "SELECT 
					DayValue
			FROM
					CARD_STUDENT_TIME_SESSION_REGULAR
			WHERE
					DayType = $type AND ClassID = $c_id";

	$temp = $lc->returnArray($sql,1);
	if(sizeof($temp)>0)
	{
		for($i=0; $i<sizeof($temp); $i++)
		{
			$no = $i+1;
			list($day_value) = $temp[$i];
			if($type==1)
				$x .= "<tr><td>$no</td><td>$i_DayType0[$day_value]</td></tr>";
			else
				$x .= "<tr><td>$no</td><td>$day_value</td></tr>";
		}
	}
	if(sizeof($temp)==0)
	{
		$x = "<tr><td colspan=2>$i_no_record_exists_msg</td></tr>";
	}
}

if($type==3)
{
	$sql = "SELECT
					RecordDate
			FROM
					CARD_STUDENT_TIME_SESSION_DATE
			WHERE
					ClassID = $c_id";
					
	$temp = $lc->returnArray($sql,1);
	if(sizeof($temp)>0)
	{
		for($i=0; $i<sizeof($temp); $i++)
		{
			$no = $i+1;
			list($record_date) = $temp[$i];
			$x .= "<tr><td>$no</td><td>$record_date</td></tr>";
		}
	}
	if(sizeof($temp)==0)
	{
		$x .= "<tr><td colspan=2>$i_no_record_exists_msg</td></tr>";
	}
}

$layer_content = "<table border=1 width=200 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA>";
$layer_content .= "<tr class=admin_bg_menu><td colspan=8 align=right><input type=button value=' X ' onClick=hideMenu('Specialday_Info')></td></tr>";
if($type == 1)
	$layer_content .= "<tr class=tableTitle><td width=1>#</td><td>$i_BookingType_Weekdays</td>";
if($type == 2)
	$layer_content .= "<tr class=tableTitle><td width=1>#</td><td>$i_BookingType_Cycleday</td>";
if($type == 3)
	$layer_content .= "<tr class=tableTitle><td width=1>#</td><td>$i_StudentAttendance_Slot_Special</td>";

$layer_content .= $x;
	
$layer_content .= "</table>";

$response = iconv("Big5","UTF-8",$layer_content);
echo $response;

intranet_closedb();
?>