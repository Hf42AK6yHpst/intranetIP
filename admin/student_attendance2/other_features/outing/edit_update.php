<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();
$Location = intranet_htmlspecialchars($Location);
$Remark = intranet_htmlspecialchars($Remark);
$Objective = intranet_htmlspecialchars($Objective);
$PIC = intranet_htmlspecialchars($PIC);

$outTimeStr = ($OutTime ==""? "NULL":"'$OutTime'");
$backTimeStr = ($BackTime ==""? "NULL":"'$BackTime'");

$sql = "UPDATE CARD_STUDENT_OUTING SET
               RecordDate = '$OutingDate',
               OutTime = $outTimeStr,
               BackTime = $backTimeStr,
               Location = '$Location',
               Objective = '$Objective',
               FromWhere = '$FromWhere',
               Detail = '$Remark',
               PIC = '$PIC',
               DateModified = now()
        WHERE OutingID = $OutingID
               ";
$li->db_db_query($sql);
header ("Location: index.php?msg=2");
intranet_closedb();
?>
