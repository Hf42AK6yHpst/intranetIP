<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$limport = new libimporttext();

$format_array = array("ClassName","ClassNumber","Teacher","Date","StartTime","EndTime","Reason");

$li = new libdb();
$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;
if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php?failed=2");
        exit();
} else {
        $ext = strtoupper($lf->file_ext($filename));
        if($limport->CHECK_FILE_EXT($filename)) {
                # read file into array
                # return 0 if fail, return csv array if success
                //$data = $lf->file_read_csv($filepath);
                $data = $limport->GET_IMPORT_TXT($filepath);
                $toprow = array_shift($data);                   # drop the title bar
        }
        for ($i=0; $i<sizeof($format_array); $i++)
        {
             if ($toprow[$i] != $format_array[$i])
             {
                 header("Location: import.php?msg=13");
                 exit();
             }
        }
        #
        $sql = "CREATE TEMPORARY TABLE TEMP_IMPORT_STUDENT_OUTING (
                 ClassName varchar(20),
                 ClassNumber varchar(20),
                 DateOfOuting date,
                 StartTime  time,
                 EndTime time,
                 Teacher varchar(100),
                 Reason text
        )";
        $sql = $li->db_db_query($sql);
        $values = "";
        $delim = "";
        for ($i=0; $i<sizeof($data); $i++)
        {
             list($class,$classnum,$teacher,$recordDate,$startTime,$endTime,$reason) = $data[$i];
             if ($class=="" || $classnum=="" || $teacher=="") continue;
             if ($recordDate == "")
             {
                 $recordDateStr = "CURDATE()";
             }
             else
             {
                 $recordDateStr = "'$recordDate'";
             }
             $values .= "$delim ('$class','$classnum',$recordDateStr,'$startTime','$endTime','$teacher','$reason')";
             $delim = ",";
        }

        $sql = "INSERT INTO TEMP_IMPORT_STUDENT_OUTING (ClassName,ClassNumber,DateOfOuting,StartTime,EndTime,Teacher,Reason)
                       VALUES $values";
        $li->db_db_query($sql);

        # Insert to Outing table
        $sql = "INSERT INTO CARD_STUDENT_OUTING (UserID, PIC, RecordDate, OutTime, BackTime, Objective, DateInput, DateModified)
                       SELECT
                             b.UserID, a.Teacher, a.DateOfOuting, a.StartTime, a.EndTime, a.Reason, now(), now()
                       FROM TEMP_IMPORT_STUDENT_OUTING as a
                            LEFT OUTER JOIN INTRANET_USER as b
                                 ON a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber AND b.RecordType = 2 AND b.RecordStatus = 1
                       WHERE b.UserID IS NOT NULL
                       ";
        $li->db_db_query($sql);
}
intranet_closedb();

header("Location: index.php?msg=1");
?>