<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lc = new libcardstudentattend2();


$li = new libdb();
$user_field = getNameFieldByLang("b.");
$sql  = "SELECT
               b.ClassName,b.ClassNumber,
               a.RecordDate,
               a.ArrivalTime,
               a.DepartureTime,
               a.Location,a.Reason, a.Remark
         FROM
             CARD_STUDENT_DETENTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
         WHERE
              (a.RecordDate = '$keyword' OR
              b.ChineseName like '%$keyword%'
               OR b.EnglishName like '%$keyword%'
               OR a.Location LIKE '%$keyword%'
               OR a.Remark LIKE '%$keyword%')
                ";

$result = $li->returnArray($sql,8);

$lexport = new libexporttext();

$exportColumn = array("ClassName", "ClassNumber", "Date", "Arrival", "Departure", "Location", "Reason", "Remark");

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);

// Output the file to user browser
$filename = "detention_records.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>