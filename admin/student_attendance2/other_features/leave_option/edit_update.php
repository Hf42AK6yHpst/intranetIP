<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lclass = new libclass();

if(sizeof($student)>0)
{
	for($i=0; $i<sizeof($student); $i++)
	{
		$sql = "INSERT IGNORE INTO CARD_STUDENT_LEAVE_OPTION (StudentID, Weekday1, Weekday2, Weekday3, Weekday4, Weekday5, Weekday6, LastUpdateUser, DateInput, DateModified)
				VALUES
				($student[$i], ${"opt_$student[$i]_1"}, ${"opt_$student[$i]_2"}, ${"opt_$student[$i]_3"}, ${"opt_$student[$i]_4"}, ${"opt_$student[$i]_5"}, ${"opt_$student[$i]_6"}, '$PHP_AUTH_USER', NOW(), NOW())";
		//echo $sql."<BR>";
		$lclass->db_db_query($sql);
		
		if($lclass->db_affected_rows() == 0)
		{
			$sql = "UPDATE CARD_STUDENT_LEAVE_OPTION SET Weekday1 = ${"opt_$student[$i]_1"}, Weekday2 = ${"opt_$student[$i]_2"}, Weekday3 = ${"opt_$student[$i]_3"}, Weekday4 = ${"opt_$student[$i]_4"}, Weekday5 = ${"opt_$student[$i]_5"}, Weekday6 = ${"opt_$student[$i]_6"}, LastUpdateUser = '$PHP_AUTH_USER', DateModified = NOW() WHERE StudentID = $student[$i]";
			//echo "$sql";
			$lclass->db_db_query($sql);
		}
	}
}

intranet_closedb();
header("Location: edit.php?TargetClass=$TargetClass&msg=2");
?>