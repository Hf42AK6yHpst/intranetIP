<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();

$classlist = $lclass->getSelectClass("name=TargetClass onChange='this.form.submit();'", $TargetClass);

if($TargetClass != "")
{
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT
					a.UserID,
					$namefield,
					b.Weekday1,
					b.Weekday2,
					b.Weekday3,
					b.Weekday4,
					b.Weekday5,
					b.Weekday6
			FROM
					INTRANET_USER AS a LEFT OUTER JOIN
					CARD_STUDENT_LEAVE_OPTION AS b ON (a.UserID = b.StudentID)
			WHERE
					a.ClassName = '$TargetClass' AND 
					a.RecordType=2 AND 
					a.RecordStatus=1
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName";
	//echo $sql;
	$result = $lclass->returnArray($sql,8);
	
	$table_content .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
	$table_content .= "<tr class=tableTitle>
							<td width=100>$i_UserStudentName</td>
							<td width=60>$i_StudentAttendance_LeaveOption_Weekday[1]<input type=checkbox name=all_1 onClick='(this.checked)?setAll(1,1):setAll(1,0)'><br>".getSelectByArray($attendst_leave_option,'name=opt_all_1 DISABLED onChange=changeAll(1)','',0,1)."</td>
							<td width=60>$i_StudentAttendance_LeaveOption_Weekday[2]<input type=checkbox name=all_2 onClick='(this.checked)?setAll(2,1):setAll(2,0)'><br>".getSelectByArray($attendst_leave_option,'name=opt_all_2 DISABLED onChange=changeAll(2)','',0,1)."</td>
							<td width=60>$i_StudentAttendance_LeaveOption_Weekday[3]<input type=checkbox name=all_3 onClick='(this.checked)?setAll(3,1):setAll(3,0)'><br>".getSelectByArray($attendst_leave_option,'name=opt_all_3 DISABLED onChange=changeAll(3)','',0,1)."</td>
							<td width=60>$i_StudentAttendance_LeaveOption_Weekday[4]<input type=checkbox name=all_4 onClick='(this.checked)?setAll(4,1):setAll(4,0)'><br>".getSelectByArray($attendst_leave_option,'name=opt_all_4 DISABLED onChange=changeAll(4)','',0,1)."</td>
							<td width=60>$i_StudentAttendance_LeaveOption_Weekday[5]<input type=checkbox name=all_5 onClick='(this.checked)?setAll(5,1):setAll(5,0)'><br>".getSelectByArray($attendst_leave_option,'name=opt_all_5 DISABLED onChange=changeAll(5)','',0,1)."</td>
							<td width=60>$i_StudentAttendance_LeaveOption_Weekday[6]<input type=checkbox name=all_6 onClick='(this.checked)?setAll(6,1):setAll(6,0)'><br>".getSelectByArray($attendst_leave_option,'name=opt_all_6 DISABLED onChange=changeAll(6)','',0,1)."</td>
							<td width=100>&nbsp;</td></tr>\n";
	if(sizeof($result)>0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
			$table_content .= "<tr><td>$student_name</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_1",$opt_mon,0,1)."</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_2",$opt_tue,0,1)."</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_3",$opt_wed,0,1)."</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_4",$opt_thur,0,1)."</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_5",$opt_fri,0,1)."</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_6",$opt_sat,0,1)."</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_all DISABLED onChange=changeAllUserOnly($uid)",'',0,1)."<input type=checkbox name=all_".$uid." onClick=\"(this.checked)?setAllUserOnly($uid,1):setAllUserOnly($uid,0)\"></td>\n";
			$table_content .= "<input type=hidden name=student[] value=$uid>\n";
		}
	}
	$table_content .= "</table>\n";
	$table_content .= "<table width=560 border=0 cellspacing=0 cellpadding=0 align=center>\n";
	$table_content .= "<tr><td height=10px></td></tr>\n";
	$table_content .= "<tr><td align=right>".btnSubmit()."&nbsp;".btnReset()."&nbsp;<a href=\"edit.php\"><img src=\"/images/admin/button/s_btn_cancel_$intranet_session_language.gif\" border=\"0\"></a></td></tr>\n";
	$table_content .= "</table>\n";
}
?>

<SCRIPT LANGUAGE="JavaScript">
function checkForm()
{
	var obj = document.form1;
	if(obj.TargetClass.value != "")
	{
		obj.action = "edit_confirm.php";
		obj.submit();
	}
	else
	{
		alert("<?=$i_staffAttendance_LeaveOption_ClassSelection_Warning?>");
	}
}
function setAll(weekday,checked)
{
	var opt_all = eval("document.form1.opt_all_"+weekday);
	if(checked == 1)
	{
		opt_all.disabled = false;
		
		<? 
		for($i=0; $i<sizeof($result); $i++) 
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
		?>
			var temp = eval("document.form1.opt_"+<?=$uid?>+"_"+weekday);
			temp.value = eval("document.form1.opt_all_"+weekday+".value");
		<?
		}
		?>
	}
	if(checked == 0)
	{
		opt_all.disabled = true;
		
		<? 
		for($i=0; $i<sizeof($result); $i++) 
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
		?>
			var temp = eval("document.form1.opt_"+<?=$uid?>+"_"+weekday);
		<?
		}
		?>
	}
}
function setAllUserOnly(user_id,checked)
{
	var opt_AllUserOnly = eval("document.form1.opt_"+user_id+"_all");
	
	if(checked == 1)
	{
		opt_AllUserOnly.disabled = false;
		
		for(i=1; i<7; i++)
		{
			var temp = eval("document.form1.opt_"+user_id+"_"+i);
			temp.value = eval("document.form1.opt_"+user_id+"_all.value");
		}
	}
	if(checked == 0)
	{
		opt_AllUserOnly.disabled = true;
		
		for(i=1;i<7;i++)
		{
			var temp = eval("document.form1.opt_"+user_id+"_"+i);
		}
	}	
}
function changeAll(weekday)
{
	var temp = eval("document.form1.all_"+weekday);
	if(temp.checked)
	{
		<?
		for($i=0; $i<sizeof($result); $i++) 
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
		?>
			var temp = eval("document.form1.opt_"+<?=$uid?>+"_"+weekday);
			temp.value = eval("document.form1.opt_all_"+weekday+".value");
		<?
		}
		?>
	}
}
function changeAllUserOnly(user_id)
{
	var temp = eval("document.form1.all_"+user_id);
	if(temp.checked)
	{
		for(i=1;i<7;i++)
		{
			var temp = eval("document.form1.opt_"+user_id+"_"+i);
			temp.value = eval("document.form1.opt_"+user_id+"_all.value");
		}
	}
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_OtherFeatures,'../',$i_StudentAttendance_Menu_OtherFeatures_LeaveSchoolOption,'index.php',$button_edit,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name="form1" action="" method="POST" onSubmit="checkForm()">

<table width="560" border="0" cellspacing="0" cellpadding="4" align="center">
<tr><td width="200" align="right"><?=$i_ClassName?></td><td align="left"><?=$classlist?></td></tr>
<tr><td colspan="2"><hr size="1"></td></tr>
</table>

<?=$table_content?>

</form>

<?
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>