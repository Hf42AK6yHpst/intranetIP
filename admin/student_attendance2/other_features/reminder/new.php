<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$student = $_POST['student'];
$flag = $_POST['flag'];
$targetClass = $_POST['targetClass'];
$targetNum = $_POST['targetNum'];
$student_login = $_POST['student_login'];

# Class selection
if ($flag != 2)
{
    $targetClass = "";
}
$lc = new libclass();
$select_class = $lc->getSelectClass("name=targetClass onChange=changeClass(this.form)", $targetClass);

if ($flag==2 && $targetClass != "")
{
    # Get Class Num
    $target_class_name = $targetClass;
    $sql = "SELECT DISTINCT ClassNumber FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND ClassName = '$target_class_name' ORDER BY ClassNumber";
    $classnum = $lc->returnVector($sql);
    $select_classnum = getSelectByValue($classnum, "name=targetNum",$targetNum);
    $select_classnum .= "<a href=javascript:addByClassNum()><img src=\"$image_path/admin/button/s_btn_add_$intranet_session_language.gif\" border=0></a>";
}

if ($flag == 1 && $student_login != '')           # Login
{
    $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND UserLogin = '$student_login'";
    $temp = $lc->returnVector($sql);
    $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}
else if ($flag == 2 && $target_class_name != '' && $targetNum != '')
{
     $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '$target_class_name' AND ClassNumber = '$targetNum'";
     $temp = $lc->returnVector($sql);
     $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}

$allow_item_number=false;  // allow user to input item numbers

# Last selection
if (is_array($student) && sizeof($student)>0)
{
    $list = implode(",", $student);
    $namefield = getNameFieldWithClassNumberByLang();
    $sql = "SELECT UserID , $namefield FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list)";
    $array_students = $lc->returnArray($sql,2);
    if(sizeof($student)==1)
   		$allow_item_number=true;
}

if($allow_item_number){
	$select_item_num = "<SELECT name=itemcount>";
	for($i=1;$i<=20;$i++){
		$select_item_num .="<OPTION value=$i>$i</OPTION>";
	}
	$select_item_num .= "</SELECT>";
}

include_once("../../../../templates/fileheader.php");
?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_OtherFeatures,'../',$i_StudentAttendance_Reminder,'index.php',$button_new,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<SCRIPT LANGUAGE=Javascript>
function addByLogin()
{
         obj = document.form1;
         obj.flag.value = 1;
         generalFormSubmitCheck(obj);
}
function removeStudent(){
		checkOptionRemove(document.form1.elements["student[]"]);
 		submitForm();
}
function submitForm(){
         obj = document.form1;
         obj.flag.value =0;
         generalFormSubmitCheck(obj);
}
function changeClass(obj)
{
         obj.flag.value = 2;
         if (obj.targetNum != undefined)
             obj.targetNum.selectedIndex = 0;
         generalFormSubmitCheck(obj);
}
function addByClassNum()
{
         obj = document.form1;
         obj.flag.value = 2;
         generalFormSubmitCheck(obj);
}
function finishSelection()
{
         obj = document.form1;
         obj.action = 'new2.php';
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
         return true;
}
function generalFormSubmitCheck(obj)
{
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
}
function formSubmit(obj)
{
         if (obj.flag.value == 0)
         {
             obj.flag.value = 1;
             generalFormSubmitCheck(obj);
             return true;
         }
         else
         {
             return finishSelection();
         }
}
function checkForm()
{
        obj = document.form1;

        if(obj.elements["student[]"].length != 0)
                return formSubmit(obj);
        else
        {
                alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
                return false;
        }
}

</SCRIPT>
<form name=form1 ACTION="" method=POST onsubmit="return checkForm();">

<table width=80% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<tr><td class=tableTitle><?=$i_general_students_selected?></td><td>
<table width=100% border=0 cellspacing=0 cellpadding=0>
<tr><td><SELECT name=student[] size=5 multiple>
<?
for ($i=0; $i<sizeof($array_students); $i++)
{
     list ($id, $name) = $array_students[$i];
?>
<OPTION value='<?=$id?>'><?=$name?></OPTION>
<?
}
?>
</SELECT></td>
<td width=20%>
<a href='javascript:newWindow("choose/index.php?fieldname=student[]", 9)'><img src="<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border=0></a>
<br>
<a href='javascript:removeStudent();'><img src="<?=$image_path?>/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border=0></a>
</td></tr>
</table>
</td></tr>
<tr><td class=tableTitle width=50%><?=$i_UserLogin?></td><td><input type=text name=student_login size=20 maxlength=100><a href=javascript:addByLogin()><img src="<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border=0></a><br></td></tr>
<tr><td class=tableTitle><?=$i_ClassNameNumber?></td><td><?=$select_class?><?=$select_classnum?><br></td></tr>
</table>

<table border=0 width=80% align=center>
<tr><td height=5px></td></tr>
<tr><td>
<input type=image onClick="this.form.flag.value=3" src="<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif">
</td></tr>
</table>

<input type=hidden name=flag value=0>
<input type=hidden name=type value=<?=$type?>>
</form>

<?

include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>