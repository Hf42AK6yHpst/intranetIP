<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();
$Reason = intranet_htmlspecialchars($Reason);

$SelectedStudent = explode(",", $student_list);

if($repeat_status==0)
{
	if(sizeof($SelectedStudent)>0)
	{
		foreach ($SelectedStudent as $target_student)
		{
			$sql = "INSERT INTO CARD_STUDENT_REMINDER (StudentID, DateOfReminder,TeacherID,Reason,DateInput,DateModified)
        			VALUES ('$target_student','$ReminderStartDate',$TargetTeacher,'$Reason',now(),now())";
        	//echo $sql;
        	$li->db_db_query($sql);
		}
	}
}
if($repeat_status==1)
{
	$datefrom = strtotime($ReminderStartDate);    
	$dateto = strtotime($ReminderEndDate);
	$difference = $dateto - $datefrom + 86400;
	$datediff = floor($difference / 86400);
	
	if(sizeof($SelectedStudent)>0)
	{
		for($i=0; $i<$datediff; $i++)
		{
			$target_date = date("Y-m-d",strtotime($ReminderStartDate) + 86400 * $i);
			foreach ($SelectedStudent as $target_student)
			{
				$sql = "INSERT INTO CARD_STUDENT_REMINDER (StudentID, DateOfReminder,TeacherID,Reason,DateInput,DateModified)
	        			VALUES ('$target_student','$target_date',$TargetTeacher,'$Reason',now(),now())";
	        	//echo $sql."<BR><BR>";
	        	$li->db_db_query($sql);
			}
		}
	}
}
if($repeat_status==2)
{
	$start_year = substr($ReminderStartDate,0,4);
	$start_month = substr($ReminderStartDate,5,2);
	$start_day = substr($ReminderStartDate,-2);
	$start_weekday = date("w", mktime(0,0,0,$start_month,$start_day,$start_year));
	$end_year = substr($ReminderEndDate,0,4);
	$end_month = substr($ReminderEndDate,5,2);
	$end_day = substr($ReminderEndDate,-2);
	
	$year_diff = intval($end_year - $start_year);
	$month_diff = intval($end_month - $start_month);
	
	$start_weeknum = strftime("%U",strtotime($ReminderStartDate));
	$end_weeknum = strftime("%U",strtotime($ReminderEndDate));
	
	if($year_diff!=0)
		$end_weeknum = $year_diff + 52;
		
	$weekdiff = $end_weeknum - $start_weeknum;
	
	//echo $year_diff."<BR>";
	//echo $month_diff."<BR>";
	//if($month_diff != 0)
	//{
		//echo "Total Weeks Of ".$start_month.": ";
		//echo date("t", $ReminderStartDate)."<BR>";
		//echo floor(intval(date("t", $ReminderStartDate))/7)."<BR>";
		//echo date("W", $ReminderStartDate)."<BR>";
		//echo date("W", $ReminderEndDate)."<BR>";
		//echo strftime("%V",strtotime($ReminderStartDate))."<BR>";
		//echo strftime("%V",strtotime($ReminderEndDate))."<BR>";
	//}
		
	/*
	if($weekdiff == 0)
	{
		for($i=0; $i<sizeof($DayValue); $i++)
		{
			if($DayValue[$i]!=$start_weekday)
			{
				$temp_diff = $DayValue[$i] - $start_weekday;
			}
			else
			{
				if($DayValue[$i]==0)
					$temp_diff = $DayValue[$i];
				else
					$temp_diff = $DayValue[$i]-1;
			}
			$target_date = date("Y-m-d",strtotime($ReminderStartDate) + ($temp_diff * 86400));
			if(($target_date >= $ReminderStartDate) && ($target_date <= $ReminderEndDate))
			{
				foreach ($SelectedStudent as $target_student)
				{
					$sql = "INSERT INTO CARD_STUDENT_REMINDER (StudentID, DateOfReminder,TeacherID,Reason,DateInput,DateModified)
	        				VALUES ('$target_student','$target_date',$TargetTeacher,'$Reason',now(),now())";
	        		echo $sql."<BR><BR>";
	        		#$li->db_db_query($sql);
        		}
    		}
		}		
	}
	else
	{
	*/
		for($i=0; $i<=$weekdiff; $i++)
		{
			for($j=0; $j<sizeof($DayValue); $j++)
			{
				if($DayValue[$j]!=$start_weekday)
				{
					$temp_diff = $DayValue[$j] - $start_weekday;
				}
				else
				{
					if($DayValue[$j]==0)
						$temp_diff = $DayValue[$j];
					else
						$temp_diff = $DayValue[$j]-1;
				}
				$target_date = date("Y-m-d",strtotime($ReminderStartDate) + ($temp_diff * 86400) + ($i * 7 * 86400));
				if(($target_date >= $ReminderStartDate) && ($target_date <= $ReminderEndDate))
				{
					foreach ($SelectedStudent as $target_student)
					{
						$sql = "INSERT INTO CARD_STUDENT_REMINDER (StudentID, DateOfReminder,TeacherID,Reason,DateInput,DateModified)
		        				VALUES ('$target_student','$target_date',$TargetTeacher,'$Reason',now(),now())";
		        		//echo $sql."<BR><BR>";
		        		$li->db_db_query($sql);
	        		}
        		}
			}
		}
	//}
}
if($repeat_status==3)
{
	$selected_cycle_day = array();
	
	# Get The Selected Cycle Day In String 
	foreach($DayValue as $cycle_day)
	{
		$temp = "'".$cycle_day."'";
		array_push($selected_cycle_day, $temp);
	}
	$cycle_day_list = implode(",",$selected_cycle_day);
	
	# Get The Exactly Date Of The Cycle Day Bewteen The Date Range
	$sql = "SELECT RecordDate FROM INTRANET_CYCLE_DAYS WHERE TextShort IN ($cycle_day_list) AND (RecordDate BETWEEN '$ReminderStartDate' and '$ReminderEndDate')";
	$result = $li->returnArray($sql,1);
	
	if(sizeof($result)>0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			list($target_date) = $result[$i];
			foreach ($SelectedStudent as $target_student)
			{
				$sql = "INSERT INTO CARD_STUDENT_REMINDER (StudentID, DateOfReminder,TeacherID,Reason,DateInput,DateModified)
	        			VALUES ('$target_student','$target_date',$TargetTeacher,'$Reason',now(),now())";
	        	//echo $sql;
	        	$li->db_db_query($sql);
    		}
		}
	}
}

//$sql = "INSERT INTO CARD_STUDENT_REMINDER (StudentID, DateOfReminder,TeacherID,Reason,DateInput,DateModified)
//        VALUES ('$StudentID','$RemindDate',$TeacherID,'$Reason',now(),now())";
//$li->db_db_query($sql);
Header ("Location: index.php?msg=1");
intranet_closedb();
?>
