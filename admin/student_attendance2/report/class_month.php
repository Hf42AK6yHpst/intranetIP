<?
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$lcard = new libcardstudentattend2();

# Get Classes List
$classes = $lcard->getClassList();
#$select_class = getSelectByArray($classes,"name=ClassID","",1);
$select_class = getSelectByArray($classes,"name=ClassID","",0,1);

# Get Year List
$years = $lcard->getRecordYear();
$select_year = getSelectByValue($years, "name=Year",date('Y'),0,1);

# Month List
$months = $i_general_MonthShortForm;
$select_month = "<SELECT name=Month>\n";
$currMon = date('n')-1;
for ($i=0; $i<sizeof($months); $i++)
{
     $month_name = $months[$i];
     $string_selected = ($currMon==$i? "SELECTED":"");
     $select_month .= "<OPTION value=".($i+1)." $string_selected>".$month_name."</OPTION>\n";
}
$select_month .= "</SELECT>\n";


$format_array = array(
                      array(0,"Web"),
#                      array(1,"Excel"));
                       );
$select_format = getSelectByArray($format_array, "name=format",0,0,1);


?>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj) {
}
</SCRIPT>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Report,'index.php',$i_StudentAttendance_Report_ClassMonth,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 target=_blank action=class_month_report.php method=GET ONSUBMIT="return checkform(this)">
<blockquote>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr><td align=right><?="$button_select $i_ClassName"?>:</td><td><?=$select_class?></td></tr>
<tr><td align=right><?=$i_general_Year?>:</td><td><?=$select_year?></td></tr>
<tr><td align=right><?=$i_general_Month?>:</td><td><?=$select_month?></td></tr>
<tr><td align=right><?=$i_general_Format?>:</td><td><?=$select_format?></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>
<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>