<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../includes/libwordtemplates.php");

if(!$sys_custom['SmartCardAttendance_Report_Search']){
	header("Location: index.php");
}

include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$lclass= new libclass();

# class list
$class_list = $lclass->getClassList();
$select_class ="<SELECT name='class_name' multiple class='class_list'>\n";
for($i=0;$i<sizeof($class_list);$i++){
	$class_name=$class_list[$i][1];
	$select_class.="<OPTION value='$class_name'>$class_name</OPTION>\n";
}
$select_class.="</SELECT>\n";

# attendance type
$select_attend = "<SELECT name='attendance_type'>\n";
$select_attend .= "<OPTION value='' SELECTED>$i_status_all</OPTION>";
$select_attend .= "<OPTION value='".CARD_STATUS_ABSENT."'>$i_StudentAttendance_Status_Absent</OPTION>";
$select_attend .= "<OPTION value='".CARD_STATUS_LATE."'>$i_StudentAttendance_Status_Late</OPTION>";
$select_attend .= "<OPTION value='".PROFILE_TYPE_EARLY."'>$i_StudentAttendance_Status_EarlyLeave</OPTION>";
$select_attend .="</SELECT>\n";

# session
$select_session ="<SELECT name='session'>\n";
$select_session.= "<OPTION value='' SELECTED>$i_status_all</OPTION>";
$select_session.="<OPTION value='".PROFILE_DAY_TYPE_AM."'>$i_StudentAttendance_Slot_AM</OPTION>\n";
$select_session.="<OPTION value='".PROFILE_DAY_TYPE_PM."'>$i_StudentAttendance_Slot_PM</OPTION>\n";
$select_session.="</SELECT>\n";

# date range
$current_month=date('n');
$current_year =date('Y');
if($current_month>=9){
	$startDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year));
	//$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year+1));
}else{
	$startDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year-1));
	//$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year));
}
$endDate=date('Y-m-d');

# reason
$lword = new libwordtemplates();
$words = $lword->getWordListAttendance();
$select_words = "<SELECT name='words' onChange='changeReason(this.form)'>\n";
$select_words.="<OPTION value=''> -- $i_StudentAttendance_WordTemplates  -- </OPTION>\n";
for ($j=0; $j<sizeof($words); $j++){
     $select_words.="<OPTION value='$j'>".$words[$j]."</OPTION>\n";
}
$select_words.="</SELECT>\n";

# waived
$select_waived = "<SELECT name='waived'>\n";
$select_waived.="<OPTION value='' SELECTED>$i_status_all</OPTION>\n";
$select_waived.="<OPTION value=1>$i_general_yes</OPTION>\n";
$select_waived.="<OPTION value=2>$i_general_no</OPTION>\n";
$select_waived.="</SELECT>\n";

# format
$select_format ="<SELECT name='format'>\n";
$select_format .="<OPTION value=1>Web</OPTION>\n";
$select_format.="<OPTION VALUE=2>CSV</OPTION>\n";
$select_format.="</SELECT>\n";

?>
<script language="javascript">
</script>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Report,'index.php',$i_StudentAttendance_Report_Search,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<style type="text/css">
.class_list{width:100px; height=150px;}
</style>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_free";
	css_array[1] = "dynCalendar_half";
	css_array[2] = "dynCalendar_full";

	var date_array = new Array;
</script>

<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript">
<!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].startStr.value = dateValue;
          }
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback2(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].endStr.value = dateValue;
          }
              function isValidDate(obj){
   					if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
   					return true;
          }
          function changeReason(formObj){
				if(formObj==null)return;
				if(formObj.words==null || formObj.reason==null) return;
				if(formObj.words.selectedIndex<=0) {
					formObj.reason.value="";
					return;
				}
				formObj.reason.value = formObj.words.options[formObj.words.selectedIndex].text;
			}
	
          function checkform(formObj){
				if(formObj==null)return false;
	          	if(formObj.startStr==null || formObj.endStr==null) return false;
	          	if(!isValidDate(formObj.startStr) || !isValidDate(formObj.endStr)) return false;
	          	if(formObj.startStr.value>formObj.endStr.value){
		          		alert('<?=$i_Booking_EndDateWrong?>');
		          		 return false;
		         }
	          	if(formObj.class_name==null || formObj.selected_classes==null) return false;
	          	
	          	strClasses='';
	          	for(i=0;i<formObj.class_name.options.length;i++){
		          	if(formObj.class_name.options[i].selected)
		          		strClasses+=formObj.class_name.options[i].value+',';
		        }
	          	if(strClasses==''){
	          		alert('<?=$i_Discipline_System_alert_PleaseSelectClass?>');
	          		return false;
	          	}
	          	else {
		          	strClasses = strClasses.substring(0,strClasses.length-1);
		          	formObj.selected_classes.value=strClasses;
	          		return true;
	          	}
	      }
	      function submitForm(formObj){
		      if(checkform(formObj)){ 
			      selected_format = formObj.format.options[formObj.format.selectedIndex].value;
			      //if(selected_format==1){
				  //    formObj.target="_blank";
				  //}
			      formObj.submit();
			  }
		   }
       // -->
</script>
<form name=form1 action='search_report_result.php' method=GET target="_blank">
<blockquote>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr><td align=right><?=$i_general_startdate?>:</td>
	<td><input type=text  name=startStr size=12 value="<?=$startDate?>"><script language="JavaScript" type="text/javascript">
    <!-- 
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
    //-->
    </script>&nbsp;<span class=staff_extraInfo>(yyyy-mm-dd)</span></td>
</tr>
<tr><td align=right><?=$i_general_enddate?>:</td>
<td>
<input type=text  name=endStr size=12 value="<?=$endDate?>"><script language="JavaScript" type="text/javascript">
    <!--
         startCal2 = new dynCalendar('startCal2', 'calendarCallback2', '/templates/calendar/images/');
    //-->
    </script>
<span class=staff_extraInfo>(yyyy-mm-dd)</span>
</td></tr>
<td><td colspan=2>&nbsp;</td></tr>
<tr><td align=right><?="$button_select $i_ClassName"?>:</td><td><div><?=$select_class?></div></td></tr>
<td><td colspan=2>&nbsp;</td></tr>
<tr><td align=right><?="$i_Attendance_attendance_type"?>:</td><td><?=$select_attend?></td></tr>
<td><td colspan=2>&nbsp;</td></tr>

<tr><td align=right><?="$i_Attendance_DayType"?>:</td><td><?=$select_session?></td></tr>
<td><td colspan=2>&nbsp;</td></tr>

<tr><td align=right><?=$i_Attendance_Reason?>:</td><td><input type=text name=reason><br><?=$select_words?></td></tr>
<tr><td align=right></td><td><input type=checkbox name=match value=1><?=$i_StudentAttendance_exactly_matched?></td></tr>
<td><td colspan=2>&nbsp;</td></tr>

<tr><td align=right><?=$i_SmartCard_Frontend_Take_Attendance_Waived?>:</td><td><?=$select_waived?></td></tr>
<td><td colspan=2>&nbsp;</td></tr>

<tr><td align=right><?=$i_general_Format?>:</td><td><?=$select_format?></td></tr>

</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<a href='javascript:submitForm(document.form1)'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0'></a>
<?=btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=selected_classes value=''>
</form>
<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>