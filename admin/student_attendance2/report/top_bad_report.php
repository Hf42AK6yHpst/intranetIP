<?
#  using: yat
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

switch ($BadType)
{
        case CARD_BADACTION_LUNCH_NOTINLIST: $nav_title = $i_StudentAttendance_BadLogs_Type_NotInLunchList; break;
        case CARD_BADACTION_LUNCH_BACKALREADY: $nav_title = $i_StudentAttendance_BadLogs_Type_GoLunchAgain; break;
        case CARD_BADACTION_FAKED_CARD_AM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardAM; break;
        case CARD_BADACTION_FAKED_CARD_PM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardPM; break;
        case CARD_BADACTION_NO_CARD_ENTRANCE: $nav_title = $i_StudentAttendance_BadLogs_Type_NoCardRecord; break;
}


$i_title = $i_StudentAttendance_Report_StudentBadRecords." ($nav_title - $i_StudentAttendance_Top $TopNumber $i_StudentAttendance_Top_student_suffix)";
include_once("../../../templates/fileheader.php");

$lcard = new libcardstudentattend2();
$lcard->retrieveSettings();

$ts = strtotime($StartDate);
if ($ts==-1 || $StartDate =="")
{
    $StartDate = date('Y-m-d');
}
$ts = strtotime($EndDate);
if ($ts==-1 || $EndDate =="")
{
    $EndDate = date('Y-m-d');
}

$result = $lcard->retrieveTopBadRecordsCountByStudent($StartDate, $EndDate, $BadType, $TopNumber);

$display = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
//$display .= "<tr class='tableTitle'><td>#</td><td>$i_UserStudentName</td>\n";
$display .= "<tr class='tableTitle'><td>#</td>";
switch($lcard->ReportDisplayStudentNameFormat())
	{
		case 1:	
			$display .= "<td>$i_UserChineseName</td>";
			break;
		case 2:	
			$display .= "<td>$i_UserEnglishName</td>";
			break;
		default:
			$display .= "<td>$i_UserChineseName</td>";
			$display .= "<td>$i_UserEnglishName</td>";
			break;
	}
	
$display .= "<td>$i_SmartCard_ClassName</td>\n";
$display .= "<td>$i_ClassNameNumber</td>\n";
$display .= "<td>$i_StudentAttendance_ReportHeader_NumRecords</td>\n";
$display .= "</tr>\n";

$curr_pos = 1;
$last_count = 0;
for ($i=0; $i<sizeof($result); $i++)
{
     list($studentid,$student_name_ch, $student_name_en,$student_class, $student_classnum, $count) = $result[$i];
     if($student_name_ch == "")
     	$student_name_ch = $student_name_en;
     if($student_name_en == "")
     	$student_name_en = $student_name_ch;
     if($student_name_ch == "")
     	$student_name_ch = "&nbsp;";
     if($student_name_en == "")
     	$student_name_en = "&nbsp;";
     	
     if ($count != $last_count)
     {
         if ($i==0)
         {
         }
         else
         {
             $curr_pos = $i+1;
         }
     }
     else
     {
         $curr_pos = "&nbsp;";
     }
     $last_count = $count;
     $css = ($i%2? "2":"");
     //$display .= "<tr class='tableContent$css'><td>$curr_pos</td><td>$student_name</td><td>$student_class</td><td>$student_classnum</td>\n";
     $display .= "<tr class='tableContent$css'><td>$curr_pos</td>";
     switch($lcard->ReportDisplayStudentNameFormat())
	{
		case 1:
			$display .= "<td NOWRAP>$student_name_ch</td>";
			break;
		case 2:
			$display .= "<td NOWRAP>$student_name_en</td>";
			break;
		default:
			$display .= "<td NOWRAP>$student_name_ch</td>";
			$display .= "<td NOWRAP>$student_name_en</td>";
			break;
	}
//      $display .= "<td>$student_name</td>";
     $display .= "<td>$student_class</td><td>$student_classnum</td>\n";
     
     $display .= "<td>$count</td></tr>\n";
}
$display .= "</table>\n";

?>
<table width=560 border=0 cellpadding=2 cellspacing=1>
<tr><td colspan='2'><u><?=$i_title?></u></td></tr>
<tr><td><?= $i_general_startdate ?>:</td><td><?=$StartDate?></td></tr>
<tr><td><?=$i_general_enddate?>:</td><td><?=$EndDate?></td></tr>
</table>

<?=$display?>

<?
intranet_closedb();
?>
