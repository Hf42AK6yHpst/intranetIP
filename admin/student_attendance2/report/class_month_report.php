<?
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();


$lclass = new libclass();
if ($ClassID != "")
    $ClassName = $lclass->getClassName($ClassID);

$i_title = $i_StudentAttendance_Report_ClassMonth." (".$ClassName.")";
include_once("../../../templates/fileheader.php");

//$notation_symbol = array($i_StudentAttendance_Symbol_Present, $i_StudentAttendance_Symbol_Absent,$i_StudentAttendance_Symbol_Late,$i_StudentAttendance_Symbol_EarlyLeave);

$notation_symbol[0] = $i_StudentAttendance_Symbol_Present;
$notation_symbol[1] = $i_StudentAttendance_Symbol_Absent;
$notation_symbol[2] = $i_StudentAttendance_Symbol_Late;
//$notation_symbol[3] = $i_StudentAttendance_Symbol_EarlyLeave;


$lcard = new libcardstudentattend2();
$lcard->retrieveSettings();

if ($Year == "")
{
    $Year = date('Y');
}
if ($Month == "")
{
    $Month = date('m');
}
if (strlen($Month)==1)
{
    $Month = "0".$Month;
}

$result = $lcard->retrieveClassMonthData($ClassName, $Year, $Month);

# Get Student List
$namefield = getNameFieldByLang();
$sql = "SELECT UserID, ClassName, ClassNumber, $namefield FROM INTRANET_USER WHERE ClassName = '$ClassName' AND RecordStatus IN (0,1) AND RecordType = 2 ORDER BY ClassName, ClassNumber, EnglishName";
$students = $lcard->returnArray($sql,4);
$list = "";
$delim = "";
for ($i=0; $i<sizeof($students); $i++)
{
     $list .= "$delim".$students[$i][0];
     $delim = ",";
}

# Get Day List
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$Year."_".$Month;
$sql = "SELECT DISTINCT DayNumber FROM $card_log_table_name
               WHERE UserID IN ($list) ORDER BY DayNumber";
if($list!="")
	$days = $lcard->returnVector($sql);

$display = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
$display .= "<tr class='tableTitle'><td>$i_UserStudentName</td><td>$i_ClassNameNumber</td>\n";
if ($lcard->attendance_mode==2 || $lcard->attendance_mode==3)
{
    $cols_string = "colspan=2";
}
else
{
    $cols_string = "";
}
for ($j=0; $j<sizeof($days); $j++)
{
     $day_num = $days[$j];
     $display .= "<td $cols_string>$day_num</td>";
}
$display .= "</tr>\n";
if ($lcard->attendance_mode==2 || $lcard->attendance_mode==3)
{
    $display .= "<tr class='tableTitle'><td>&nbsp;</td><td>&nbsp;</td>\n";

    for ($j=0; $j<sizeof($days); $j++)
    {
         $day_num = $days[$j];
         $display .= "<td>A</td><td>P</td>";
    }

}

if ($lcard->attendance_mode==2 || $lcard->attendance_mode==3)      # WD
{
    for ($i=0; $i<sizeof($students); $i++)
    {
         list ($studentid, $classname, $classnum, $name) = $students[$i];
         $css = ($i%2?"2":"");
         $display .= "<tr class='tableContent$css'><td>$name</td><td>$classnum</td>\n";
         for ($j=0; $j<sizeof($days); $j++)
         {
              $day_num = $days[$j];
              $cell_data = $result[$studentid][$day_num];
              list($am, $pm, $leave) = $cell_data;
              $am+=0;
              $pm+=0;

				if($am == 3)
					$am = '0';
				if($pm == 3)
					$pm = '0';
			
				$symbol_am = $notation_symbol[$am];					
				$symbol_pm = $notation_symbol[$pm];

              $display .= "<td>$symbol_am</td><td>$symbol_pm</td>";
         }
         $display .= "</tr>\n";
    }

}
else if ($lcard->attendance_mode==1)
{
    for ($i=0; $i<sizeof($students); $i++)
    {
         list ($studentid, $classname, $classnum, $name) = $students[$i];
         $css = ($i%2?"2":"");
         $display .= "<tr class='tableContent$css'><td>$name</td><td>$classnum</td>\n";
         for ($j=0; $j<sizeof($days); $j++)
         {
              $day_num = $days[$j];
              $cell_data = $result[$studentid][$day_num];
              list($am, $pm, $leave) = $cell_data;
              $pm+=0;
			  if($pm == 3)
				  $pm = '0';
              $symbol_pm = $notation_symbol[$pm];
              $display .= "<td>$symbol_pm</td>";
         }
         $display .= "</tr>\n";
    }
}
else
{
    for ($i=0; $i<sizeof($students); $i++)
    {
         list ($studentid, $classname, $classnum, $name) = $students[$i];
         $css = ($i%2?"2":"");
         $display .= "<tr class='tableContent$css'><td>$name</td><td>$classnum</td>\n";
         for ($j=0; $j<sizeof($days); $j++)
         {
              $day_num = $days[$j];
              $cell_data = $result[$studentid][$day_num];
              list($am, $pm, $leave) = $cell_data;
              $am+=0;
			  if($am == 3)
				  $am = '0';
              $symbol_am = $notation_symbol[$am];
              $display .= "<td>$symbol_am</td>";
         }
         $display .= "</tr>\n";
    }

}
$display .= "</table>\n";
?>
<table width=560 border=0 cellpadding=2 cellspacing=1>
<tr><td colspan='2'><u><?=$i_title?></u></td></tr>
<? if ($ClassName != "") { ?>
<tr><td><?= $i_ClassName ?>:</td><td><?=$ClassName?></td></tr>
<? } ?>
<tr><td><?=$i_StudentAttendance_View_Date?>:</td><td><?php echo "$Year-$Month" ?></td></tr>
</table>

<?=$display?><Br>
<?=$notation_symbol[0]?> - <?=$i_StudentAttendance_Status_Present?><br>
<?=$notation_symbol[1]?> - <?=$i_StudentAttendance_Status_Absent?><br>
<?=$notation_symbol[2]?> - <?=$i_StudentAttendance_Status_Late?><br>
<?
intranet_closedb();
?>
