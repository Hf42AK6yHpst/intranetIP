<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

#class used
$LICLASS = new libclass();

$select_class = $class_name;

#build student
if($select_class<>""){
	$select_student_table = "";

	$name_field = getNameFieldByLang("a.");
	
	/*
	$sql = "SELECT a.UserID, $name_field, a.ClassNumber,
				IF(b.StudentID IS NOT NULL, CONCAT(\"1\"), CONCAT(\"0\")) 
					FROM INTRANET_USER as a LEFT OUTER JOIN CARD_STUDENT_HELPER_STUDENT as b ON (a.UserID=b.StudentID) WHERE a.RecordType = 2 AND a.RecordStatus = 1 AND a.ClassName = '$select_class' ORDER BY a.ClassNumber";
	*/
	$sql = "SELECT a.UserID, $name_field, a.ClassNumber,
				IF(b.StudentID IS NOT NULL, CONCAT(\"1\"), CONCAT(\"0\")) 
					FROM INTRANET_USER as a LEFT OUTER JOIN CARD_STUDENT_HELPER_STUDENT as b ON (a.UserID=b.StudentID) WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND a.ClassName = '$select_class' ORDER BY a.ClassNumber";

	$student_list = $LICLASS->returnArray($sql,4);

    $checkall = "<input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'student_id[]'):setChecked(0,this.form,'student_id[]')>";

	$x = "<table border=0 cellpadding=3 cellspacing=0>\n";
	$x .= "<tr><td class=tableTitle_new>$checkall</td><td class=tableTitle_new ><u>$i_UserClassNumber</u></td><td class=tableTitle_new><u>$i_UserName</u></td></tr>\n";
	for($i=0; $i<sizeOf($student_list); $i++){
		list($UserID, $name_field, $ClassNumber, $added) = $student_list[$i];
		$checked = ($added=="1")?"CHECKED":"";

		$x .= "<tr><td align=right><input name=student_id[] type=checkbox value=\"$UserID\" $checked></td><td>$ClassNumber</td><td>$name_field</td></tr>\n";
	}
	$x .= "</table>\n";

	$select_student_table = $x;
}

?>

<script language="javascript">
function checkform(obj){
	/*
	var element = "student_id[]";
	if(countChecked(obj,element)==0) {
		alert(globalAlertMsg1);
		return false
	} 
*/
    return true;
}
</script>

<form name="form1" method="post" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_StudentAttendance_Menu_ResponsibleAdmin,'../../', $i_SmartCard_Responsible_Admin_Class, '../', $i_SmartCard_Responsible_Admin_Settings_Manage, 'index.php', $button_edit, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=right width=50%><font size=4><?=$i_ClassName?>:&nbsp;</font></td><td align=left  width=50%><font size=4><?=$select_class?></font></td></tr>
<tr><td colspan=2><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=center><?=$select_student_table?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type='image'src='/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif' border='0'>
<?= btnReset() . toolBarSpacer()?>
<a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input name=class_name type=hidden value="<?=$select_class?>">
</form>

<?
include_once("../../../../../templates/adminfooter.php");
?>