<?
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lc = new libcardstudentattend2();

$li = new libdb();

$count = 0;
if ($_GET['format']==1)
{
    $sql = "SELECT UserLogin, CardID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN(0,1,2) ORDER BY UserLogin";
    $count = 2;
    $exportColumn = array("UserLogin", "CardID");
    
}
else if ($_GET['format']==2)
{
     $sql = "SELECT ClassName, ClassNumber, CardID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN(0,1,2) AND ClassName != '' ORDER BY ClassName, ClassNumber";
     $count = 3;
     $exportColumn = array("ClassName", "ClassNumber", "CardID");
}
if ($count < 1)
{
    header("Location: studentcard_export.php");
    exit();
}

$result = $li->returnArray($sql,$count);

$lexport = new libexporttext();

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);

// Output the file to user browser
$filename = "studentcard_id_$format.csv";
intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>
