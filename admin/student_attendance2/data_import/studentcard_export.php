<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libuser.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

?>

<form name="form1" method="GET" action="studentcard_export_csv.php">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_ImportCardID,'studentcard.php',$button_export,'')?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_general_Format; ?>:</td><td>
<input type=radio name=format value=1 CHECKED><?="$i_UserLogin, $i_SmartCard_CardID"?><br>
<input type=radio name=format value=2><?="$i_UserClassName, $i_UserClassNumber, $i_SmartCard_CardID"?>
</td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type=image src="<?=$image_path?>/admin/button/s_btn_export_<?=$intranet_session_language?>.gif">
<a href="javascript:history.back()"><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=targetClass value="<?=$targetClass?>">
<input type=hidden name=StudentID value="<?=$StudentID?>">
</form>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>