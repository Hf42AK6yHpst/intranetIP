<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lc = new libcardstudentattend2();


if ($type > 5 || $type < 1) $type = 1;
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;
$field_array = array("a.RecordDate","b.ClassName","b.ClassNumber","b.EnglishName");

$user_field = getNameFieldByLang("b.");

if ($type==CARD_BADACTION_LUNCH_NOTINLIST || $type==CARD_BADACTION_LUNCH_BACKALREADY || $type==CARD_BADACTION_FAKED_CARD_AM || $type==CARD_BADACTION_FAKED_CARD_PM)
{
	array_push($field_array, "a.RecordTime");
    $sql  = "SELECT
                   a.RecordDate, b.ClassName, b.ClassNumber, $user_field,
                   a.RecordTime
         FROM
             CARD_STUDENT_BAD_ACTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2
         WHERE
              a.RecordType = '$type' AND
              (b.ChineseName like '%$keyword%'
               OR b.EnglishName like '%$keyword%'
               OR b.ClassName like '$keyword'
               OR b.ClassNumber like '%$keyword%'
               OR a.RecordDate = '$keyword'
               )
          ORDER BY ".$field_array[$field].( ($order==0) ? " DESC" : " ASC");
    
}
else if ($type==CARD_BADACTION_NO_CARD_ENTRANCE)
{
    $sql  = "SELECT
                   a.RecordDate, b.ClassName, b.ClassNumber, $user_field
         FROM
             CARD_STUDENT_BAD_ACTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2
         WHERE
              a.RecordType = '$type' AND
              (b.ChineseName like '%$keyword%'
               OR b.EnglishName like '%$keyword%'
               OR b.ClassName like '$keyword'
               OR b.ClassNumber like '%$keyword%'
               OR a.RecordDate = '$keyword'
               )
          ORDER BY ".$field_array[$field].( ($order==0) ? " DESC" : " ASC");
	
}

$li = new libdb();
$result = $li->returnArray($sql, sizeof($field_array));

$lexport = new libexporttext();

if (sizeof($field_array) == 5)
	$exportColumn = array("Record Date", "Class Name", "Class No", "Student Name", "Record Time");
else
	$exportColumn = array("Record Date", "Class Name", "Class No", "Student Name");

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);

// Output the file to user browser
$filename = "badlogs_".$type.".csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>