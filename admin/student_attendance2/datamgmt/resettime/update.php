<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");

intranet_opendb();

$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || $TargetDate == "")
{
    header("Location: index.php");
    exit();
}

$year = getCurrentAcademicYear();
$semester = getCurrentSemester();
$LICS = new libcardstudentattend2();
$LICS->retrieveSettings();
$reset_temp_table = "TEMP_CARD_STUDENT_DAY_LOG_RESET";
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($LICS->attendance_mode == 1)
{
    $db_status_field = "PMStatus";
    $profile_day_type = PROFILE_DAY_TYPE_PM;
}
else
{
    $db_status_field = "AMStatus";
    $profile_day_type = PROFILE_DAY_TYPE_AM;
}

# Handle New Late Records
$sql = "SELECT b.UserID, b.ClassName, b.ClassNumber FROM $reset_temp_table as a
               LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
               WHERE a.NewStatus = '".CARD_STATUS_LATE."' AND a.NewStatus != a.OldStatus";
$lates = $LICS->returnArray($sql,3);
$late_students_list = "";
$delim = "";
for ($i=0; $i<sizeof($lates); $i++)
{
     list($student_id, $student_class, $student_classnum) = $lates[$i];
     # Insert to Student Profile
     $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE
                    (UserID, AttendanceDate, Year, Semester, DayType, RecordType,
                     ClassName, ClassNumber, DateInput, DateModified)
                     VALUES
                     ('$student_id', '$TargetDate','$year', '$semester', '$profile_day_type', '".PROFILE_TYPE_LATE."',
                     '$student_class','$student_classnum',now(),now() )
                     ";
     $LICS->db_db_query($sql);
     if ($LICS->db_affected_rows()==1)
     {
         $record_id = $LICS->db_insert_id();
     }
     else
     {
         # Get Record ID
         $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                        WHERE AttendanceDate = '$TargetDate' AND
                              DayType = '$profile_day_type' AND
                              RecordType = '".PROFILE_TYPE_LATE."'
                              AND UserID = '$student_id'";
         $temp = $LICS->returnVector($sql);
         $record_id = $temp[0];
     }

     # Insert Profile Reason record
     $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON
                    (RecordDate, StudentID, ProfileReasonID, DayType,
                     RecordType, DateInput, DateModified)
                    VALUES ('$TargetDate','$student_id','$record_id','$profile_day_type',
                             '".PROFILE_TYPE_LATE."', now(), now())";
     $LICS->db_db_query($sql);
     if ($LICS->db_affected_rows()!=1)
     {
         $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET ProfileReasonID = '$record_id'
                        WHERE RecordDate = '$TargetDate' AND StudentID = '$student_id'
                              AND DayType = '$profile_day_type' AND RecordType = '".PROFILE_TYPE_LATE."'
                              ";
         $LICS->db_db_query($sql);
     }
     $late_students_list .= "$delim $student_id";
     $delim = ",";
}
if ($late_students_list!="")
{
    $sql = "UPDATE $card_log_table_name SET $db_status_field = '".CARD_STATUS_LATE."'
                   WHERE DayNumber = '$txt_day' AND UserID IN ($late_students_list)";
    $LICS->db_db_query($sql);
}


#  Handle Not Late Students
$sql = "SELECT b.UserID FROM $reset_temp_table as a
               LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
               WHERE a.NewStatus = '".CARD_STATUS_PRESENT."' AND a.NewStatus != a.OldStatus";
$present_students = $LICS->returnVector($sql);
if (sizeof($present_students)!=0)
{
    $present_students_list = implode(",",$present_students);

     # Remove Reason record
     $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                    WHERE RecordDate = '$TargetDate'
                          AND StudentID IN ($present_students_list)
                          AND DayType = '".$profile_day_type."'
                          AND RecordType = '".PROFILE_TYPE_LATE."'";
     $LICS->db_db_query($sql);
     # Remove Profile record
     $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                    WHERE AttendanceDate = '$TargetDate'
                          AND UserID IN ($present_students_list)
                          AND DayType = '".$profile_day_type."'
                          AND RecordType = '".PROFILE_TYPE_LATE."'";
     $LICS->db_db_query($sql);

     $sql = "UPDATE $card_log_table_name SET $db_status_field = '".CARD_STATUS_PRESENT."'
                    WHERE DayNumber = '$txt_day' AND UserID IN ($present_students_list)";
     $LICS->db_db_query($sql);
}

$sql = "DROP TABLE $reset_temp_table";
$LICS->db_db_query($sql);

header( "Location: index.php?msg=2");
?>