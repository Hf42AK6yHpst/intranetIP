<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

#class used
$LICS = new libcardstudentattend2();
$LICS->retrieveSettings();
$reset_temp_table = "TEMP_CARD_STUDENT_DAY_LOG_RESET";
$sql = "DROP TABLE $reset_temp_table";
$LICS->db_db_query($sql);

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
    $TargetDate = date('Y-m-d');
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

# Get Original Late and new late
$db_status_field = ($LICS->attendance_mode == 1? "PMStatus":"AMStatus");
if ($allClass)        # All Classes
{
    $txt_class = "$i_status_all $i_SmartCard_ClassName";
    # Get From daily table
    $sql = "SELECT COUNT(*) FROM $card_log_table_name WHERE $db_status_field = '".CARD_STATUS_LATE."'
                   AND DayNumber = '$txt_day'";
    $temp = $LICS->returnVector($sql);
    $original_count = $temp[0];

    # Create temp table
    $sql = "CREATE TABLE $reset_temp_table (
             UserID int(11) NOT NULL,
             InTime time,
             OldStatus int(11),
             NewStatus int(11)
            )";
    $LICS->db_db_query($sql);

    # Put records in temp table
    $sql = "INSERT INTO $reset_temp_table (UserID, InTime, OldStatus)
                   SELECT UserID, IF(InSchoolTime='',NULL,InSchoolTime), $db_status_field
                         FROM $card_log_table_name
                         WHERE DayNumber = '$txt_day'
                         ";
    $LICS->db_db_query($sql);

    # Set New Status
    $sql = "UPDATE $reset_temp_table SET NewStatus = OldStatus WHERE InTime IS NULL";
    $LICS->db_db_query($sql);
    $sql = "UPDATE $reset_temp_table SET NewStatus = '".CARD_STATUS_LATE."' WHERE InTime IS NOT NULL AND InTime > '$NewTime'";
    $LICS->db_db_query($sql);
    $sql = "UPDATE $reset_temp_table SET NewStatus = '".CARD_STATUS_PRESENT."' WHERE InTime IS NOT NULL AND InTime <= '$NewTime'";
    $LICS->db_db_query($sql);

    # Get New late
    $sql = "SELECT COUNT(*) FROM $reset_temp_table WHERE NewStatus = '".CARD_STATUS_LATE."'";
    $temp = $LICS->returnVector($sql);
    $new_count = $temp[0];

}
else
{
    # Get Class names
    $list = implode(",",$ClassID);
    $sql = "SELECT ClassID, ClassName, ClassLevelID FROM INTRANET_CLASS WHERE ClassID IN ($list)";
    $classes = $LICS->returnArray($sql,3);
    $txt_class = "";
    $curr_lvl = "";
    $delim = "";
    $classname_list = "";
    $classname_delim = "";
    for ($i=0; $i<sizeof($classes); $i++)
    {
         list($classID, $classname, $classlevel) = $classes[$i];
         if ($classlevel!=$curr_lvl && $curr_lvl!="")
         {
             $txt_class .= "<br>\n";
             $delim = "";
         }
         $curr_lvl = $classlevel;
         $txt_class .= "$delim$classname &nbsp;\n";
         $delim = ",";

         $classname_list .= "$classname_delim '$classname'";
         $classname_delim = ',';
    }
    # Get Student List
    $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName IN ($classname_list)";
    $temp_students = $LICS->returnVector($sql);
    $temp_student_list = implode(",",$temp_students);
    # Get From daily table
    $sql = "SELECT COUNT(*) FROM $card_log_table_name WHERE $db_status_field = '".CARD_STATUS_LATE."'
                   AND DayNumber = '$txt_day' AND UserID IN ($temp_student_list)";
    $temp = $LICS->returnVector($sql);
    $original_count = $temp[0];

    # Create temp table
    $sql = "CREATE TABLE $reset_temp_table (
             UserID int(11) NOT NULL,
             InTime time,
             OldStatus int(11),
             NewStatus int(11)
            )";
    $LICS->db_db_query($sql);
    # Put records in temp table
    $sql = "INSERT INTO $reset_temp_table (UserID, InTime, OldStatus)
                   SELECT UserID, IF(InSchoolTime='',NULL,InSchoolTime), $db_status_field
                         FROM $card_log_table_name
                         WHERE DayNumber = '$txt_day' AND UserID IN ($temp_student_list)
                         ";
    $LICS->db_db_query($sql);

    # Set New Status
    $sql = "UPDATE $reset_temp_table SET NewStatus = OldStatus WHERE InTime IS NULL";
    $LICS->db_db_query($sql);
    $sql = "UPDATE $reset_temp_table SET NewStatus = '".CARD_STATUS_LATE."' WHERE InTime IS NOT NULL AND InTime > '$NewTime'";
    $LICS->db_db_query($sql);
    $sql = "UPDATE $reset_temp_table SET NewStatus = '".CARD_STATUS_PRESENT."' WHERE InTime IS NOT NULL AND InTime <= '$NewTime'";
    $LICS->db_db_query($sql);

    # Get New late
    $sql = "SELECT COUNT(*) FROM $reset_temp_table WHERE NewStatus = '".CARD_STATUS_LATE."'";
    $temp = $LICS->returnVector($sql);
    $new_count = $temp[0];

}


?>

<form name="form1" method="post" action="update.php">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DataManagement,'../',$i_StudentAttendance_Menu_DataMgmt_ResetTime, 'index.php',$TargetDate,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=4 cellspacing=1 align="center">
<tr><td align=right><?=$i_StudentAttendance_Field_Date?>:</td><td><?=$TargetDate?></td></tr>
<tr><td align=right><?=$i_StudentAttendance_New_InSchoolTime?>:</td><td><?=$NewTime?></td></tr>
<tr><td align=right><?=$i_SmartCard_ClassName?>:</td><td><?=$txt_class?></td></tr>
<tr><td align=right><?=$i_StudentAttendance_Field_NumLateOriginal?>:</td><td><?=($original_count+0)?></td></tr>
<tr><td align=right><?=$i_StudentAttendance_Field_NumLateNew?>:</td><td><?=($new_count+0)?></td></tr>
</table>
<blockquote>
<?=$i_StudentAttendance_ResetTime_Warning?>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type=image src="<?=$image_path?>/admin/button/s_btn_continue_<?=$intranet_session_language?>.gif">
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<?
if ($allClass)        # All Classes
{
?>
<input type=hidden name=allClass value=1>
<?
}
else
{
    for ($i=0; $i<sizeof($ClassID); $i++)
    {
    ?>
<input type=hidden name=ClassID[] value="<?=$ClassID[$i]?>">
    <?
    }
}
?>
<input type=hidden name=TargetDate value="<?=$TargetDate?>">
<input type=hidden name=NewTime value="<?=$NewTime?>">
</form>
<?
include_once("../../../../templates/adminfooter.php");
?>