<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$li = new libdb();
$sql = "DROP TABLE TEMP_CARD_STUDENT_LOG";
$li->db_db_query($sql);

?>

<form name="form1" method="GET" action="reminder_export.php">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_DataExport,'index.php',$i_StudentAttendance_Reminder,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_general_startdate; ?>:</td><td><input type=text size=10 name=startdate value="<?=date('Y-m-d',time()+60*60*24)?>"> (YYYY-MM-DD)</td></tr>
<tr><td align=right nowrap><?php echo $i_general_enddate; ?>:</td><td><input type=text size=10 name=enddate value="<?=date('Y-m-d',time()+60*60*24)?>"> (YYYY-MM-DD)</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
</blockquote>
</form>

<?
include_once("../../../templates/adminfooter.php");
?>
