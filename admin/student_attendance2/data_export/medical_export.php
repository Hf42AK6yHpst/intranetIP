<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lc = new libcardstudentattend2();

### Set Date from previous page
$ts_from = strtotime($FromDate);
$ts_to = strtotime($ToDate);


$result = "";
while($ts_from<=$ts_to){
	$NoColHeader = strtotime($FromDate) == $ts_from ? 0:1;
	$result .=$lc->generateMedicalReasonSummary(date('Y-m-d',$ts_from),$NoColHeader);
	if($result==-1) break;
	$ts_from +=60*60*24;
}


/*
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
*/
/*
###period
switch ($period)
{
        case "1": $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
        case "2": $display_period = $i_DayTypePM;
                                                $DayType = PROFILE_DAY_TYPE_PM;
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
}
*/
$lexport = new libexporttext();

if($result!=-1){
	intranet_closedb();
	$filename = "medical_report_".$BroadlearningClientID."_".$FromDate."_".$ToDate.".csv";
	$lexport->EXPORT_FILE($filename, $result);
}else{
	header("Location: medical.php");
}



?>
