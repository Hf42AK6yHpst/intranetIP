<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

?>


<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<? if($intranet_version=="2.0") {?>
	<?= displayOption(
                  $i_Payment_System_Admin_User_Setting ,'admin_setting.php',$plugin['attendancestudent_eAdmin'],
                  $i_SmartCard_SystemSettings, 'settings/',1,
                  $i_StudentAttendance_Menu_ResponsibleAdmin,'admin/',1,
                  $i_StudentAttendance_Menu_DailyOperation,'dailyoperation/',1,
                  $i_StudentAttendance_Menu_DataManagement,'datamgmt/',1,
                  $i_StudentAttendance_Menu_OtherFeatures, 'other_features/',1,
                  $i_StudentAttendance_Menu_DataExport,'data_export/',1,
                  $i_StudentAttendance_Menu_DataImport,'data_import/',1,
                  $i_StudentAttendance_Menu_Report,'report/',1
                                ) ?>
<? } else {?>
		<?= displayOption(	
                  $i_SmartCard_SystemSettings, 'settings/',1,
                  $i_StudentAttendance_Menu_ResponsibleAdmin,'admin/',1,
                  $i_StudentAttendance_Menu_DailyOperation,'dailyoperation/',1,
                  $i_StudentAttendance_Menu_DataManagement,'datamgmt/',1,
                  $i_StudentAttendance_Menu_OtherFeatures, 'other_features/',1,
                  $i_StudentAttendance_Menu_DataExport,'data_export/',1,
                  $i_StudentAttendance_Menu_DataImport,'data_import/',1,
                  $i_StudentAttendance_Menu_Report,'report/',1
                                ) ?>
<? } ?>                                


</blockquote>
</td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>