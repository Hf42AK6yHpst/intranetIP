<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();
$limport = new libimporttext();
$linterface = new interface_html();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();


?>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
         return true;
}
</SCRIPT>
<form name="form1" action="import_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this)">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_ImportOfflineRecords, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_select_file; ?>:</td><td><input type=file size=50 name=userfile><br><?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>
</td></tr>
<tr><td align=right nowrap><?=$i_general_Format?>:</td><td>
<?
if ($lc->attendance_mode == 2)      # Full Lunch Control Mode
{
?>
<input type=radio name=datatype value=1 CHECKED><?=$i_StudentAttendance_Offline_Import_DataType_InSchool?>
<input type=radio name=datatype value=2 ><?=$i_StudentAttendance_Offline_Import_DataType_LunchOut?>
<input type=radio name=datatype value=3 ><?=$i_StudentAttendance_Offline_Import_DataType_LunchIn?>
<input type=radio name=datatype value=4 ><?=$i_StudentAttendance_Offline_Import_DataType_AfterSchool?>

<?
}
else     # AM or PM or WD w/o Lunch
{
?>
<input type=radio name=datatype value=1 CHECKED><?=$i_StudentAttendance_Offline_Import_DataType_InSchool?>
<input type=radio name=datatype value=4 ><?=$i_StudentAttendance_Offline_Import_DataType_AfterSchool?>
<?
}
?>
</td></tr>
<tr><td align=right nowrap><?=$i_general_Format?>:</td><td>
<input type=radio name=format value=1 CHECKED><?=$i_StudentAttendance_ImportFormat_CardID?> <a class=functionlink href="<?= GET_CSV("format1.csv")?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>  <br>
<input type=radio name=format value=2><?=$i_StudentAttendance_ImportFormat_ClassNumber?> <a class=functionlink href="<?= GET_CSV("format2.csv")?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>        <br>
<input type=radio name=format value=3><?=$i_StudentAttendance_ImportFormat_From_OfflineReader." (950e)"?><br>
<br><?=$i_StudentAttendance_ImportTimeFormat?>
<br><?=$i_StudentAttendance_Import_Instruction_OneDayOnly?>
</td></tr>
</table>

</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<br><br>
</form>
<?
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>