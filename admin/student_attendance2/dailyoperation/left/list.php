<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");
intranet_opendb();

#class used
$LIDB = new libdb();
$LICS = new libcardstudentattend2();

$type += 0;
$summary = $LICS->getSummaryDetail($type);

if ($type==0)
{
        $heading = $i_StudentAttendance_LeftStatus_Type_InSchool . " - ";

    list($back, $not_yet) = $summary;
    if ($subtype==0)
        {
                $heading .= $i_StudentAttendance_InSchool_BackAlready;
                $id_list = implode(",",$back);
        }
    else # subtype = 1
        {
                $heading .= $i_StudentAttendance_InSchool_NotBackYet;
                $id_list = implode(",",$not_yet);
        }
}
else if ($type==1)
{
        $heading = $i_StudentAttendance_LeftStatus_Type_Lunch . " - ";

     list($gone_out, $back, $not_out) = $summary;
     if ($subtype==0)
     {
                $heading .= $i_StudentAttendance_Lunch_GoneOut;
                $id_list = implode(",",$gone_out);
     }
     else if ($subtype==1)
     {
                $heading .= $i_StudentAttendance_Lunch_Back;
                $id_list = implode(",",$back);
     }
     else # subtype = 2
     {
                $heading .= $i_StudentAttendance_Lunch_NotOutYet;
                $id_list = implode(",",$not_out);
     }
}
else # type = 2
{
        $heading = $i_StudentAttendance_LeftStatus_Type_AfterSchool . " - ";

    list($left, $stay) = $summary;
    if ($subtype==0)
        {
                $heading .= $i_StudentAttendance_AfterSchool_Left;
                $id_list = implode(",",$left);
        }
    else
        {
                $heading .= $i_StudentAttendance_AfterSchool_Stay;
                $id_list = implode(",",$stay);
        }
}

$sql = "SELECT UserID,ClassName,ClassNumber, ".getNameFieldByLang().", UserLogin FROM INTRANET_USER WHERE UserID in ($id_list)
               ORDER BY ClassName, ClassNumber, EnglishName";
$result = $LIDB->returnArray($sql,5);
$studentList = $result;

$display .= "<tr>
<td width=10 class=tableTitle>#</td>
<td class=tableTitle>$i_UserClassName</td>
<td class=tableTitle>$i_UserClassNumber </td>
<td class=tableTitle>$i_UserStudentName </td>
</tr>\n";
for ($i=0; $i<sizeof($studentList); $i++)
{
     $count = $i+1;
     $css = ($i%2==0? "":"2");
     list($id,$class,$classnumber,$name,$login) = $studentList[$i];
     $display .= "<tr class=tableContent$css><td width=10>$count</td><td>$class</td><td>$classnumber</td><td>$name</td></tr>\n";
}
$display .= "</table>\n";

?>

<script language="JavaScript" type="text/javascript">

</script>

<table width=80% border=0 align=center>
<tr><td align=center><?=$heading?></td></tr>
</table>

<table width=80% border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=1 align=center>
<?=$display?>
</table>

<?
include_once("../../../../templates/filefooter.php");
?>