<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libuser.php");
include_once("../../../../includes/libgrouping.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../includes/libwordtemplates.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader_admin.php");
intranet_opendb();

$lbdb = new libdb();

$lc = new libclass();
$select_class = $lc->getSelectClass("name=targetClass onChange=changeClass(this.form)", $targetClass);

//$li = new libuser($UserID[0]);
//$lo = new libgrouping();
//$Title0 = ($li->Title==0) ? "CHECKED" : "";
//$Title1 = ($li->Title==1) ? "CHECKED" : "";
//$Title2 = ($li->Title==2) ? "CHECKED" : "";
//$Title3 = ($li->Title==3) ? "CHECKED" : "";
//$Title4 = ($li->Title==4) ? "CHECKED" : "";
//$Title5 = ($li->Title==5) ? "CHECKED" : "";
//$Gender0 = ($li->Gender=="M") ? "CHECKED" : "";
//$Gender1 = ($li->Gender=="F") ? "CHECKED" : "";
//$RecordStatus0 = ($li->RecordStatus==0) ? "CHECKED" : "";
//$RecordStatus1 = ($li->RecordStatus==1) ? "CHECKED" : "";
//$RecordStatus2 = ($li->RecordStatus==2) ? "CHECKED" : "";

//$student = $_POST['student'];
//$flag = $_POST['flag'];
//$targetClass = $_POST['targetClass'];
//$targetNum = $_POST['targetNum'];
//$student_login = $_POST['student_login'];


$ts_record = strtotime($TargetDate);
		if ($ts_record == -1)
		{
		    $TargetDate = date('Y-m-d');
		    $ts_record = strtotime($TargetDate);
		}
		$txt_year = date('Y',$ts_record);
		$txt_month = date('m',$ts_record);
		$txt_day = date('d',$ts_record);
		
		$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_";
		$card_student_daily_log .= $txt_year."_";
		$card_student_daily_log .= $txt_month;
		
		switch ($period)
		{
	        case "1": $InSchool = " AND b.AMStatus IN (0,2) ";
	        		  $LeaveStatus = " ";
	                                                break;
	        case "2": $InSchool = " AND b.PMStatus IN (0,2) ";
	        		  $LeaveStatus = " AND b.LeaveStatus IS NULL ";
	                                                break;
	        default : $InSchool = " AND b.AMStatus IN (0,2) ";
	        		  $LeaveStatus = " ";
	                                                break;
		}


# Class selection
if ($flag != 2)
{
    $targetClass = "";
}
$lc = new libclass();
$select_class = $lc->getSelectClass("name=targetClass onChange=changeClass(this.form)", $targetClass);

if ($flag==2 && $targetClass != "")
{
    # Get Class Num
    $target_class_name = $targetClass;
    $sql = "SELECT DISTINCT ClassNumber FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND ClassName = '$target_class_name' ORDER BY ClassNumber";
    $sql = "SELECT 
					a.ClassNumber
			FROM 
					INTRANET_USER AS a INNER JOIN 
					$card_student_daily_log AS b ON (a.UserID = b.UserID) 
			WHERE 
					b.DayNumber = $txt_day
					$InSchool
					$LeaveStatus
					AND a.RecordType = 2 
					AND a.RecordStatus IN (0,1,2)
					AND a.ClassName = '$target_class_name' 
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";
    $classnum = $lc->returnVector($sql);
    $select_classnum = getSelectByValue($classnum, "name=targetNum",$targetNum);
    $select_classnum .= "<a href=javascript:addByClassNum()><img src=\"$image_path/admin/button/s_btn_add_$intranet_session_language.gif\" border=0></a>";
}

if ($flag == 1 && $student_login != '')           # Login
{    
	$sql = "SELECT 
					a.UserID
			FROM 
					INTRANET_USER AS a INNER JOIN 
					$card_student_daily_log AS b ON (a.UserID = b.UserID) 
			WHERE 
					b.DayNumber = $txt_day
					$InSchool
					$LeaveStatus
					AND a.UserLogin = '$student_login'
					AND a.RecordType = 2 
					AND a.RecordStatus IN (0,1,2)
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";    				
    
	//$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND UserLogin = '$student_login'";
    
    $temp = $lc->returnVector($sql);
    $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}
else if ($flag == 2 && $target_class_name != '' && $targetNum != '')
{
     //$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '$target_class_name' AND ClassNumber = '$targetNum'";
     $sql = "SELECT 
					a.UserID
			FROM 
					INTRANET_USER AS a INNER JOIN 
					$card_student_daily_log AS b ON (a.UserID = b.UserID) 
			WHERE 
					b.DayNumber = $txt_day
					$InSchool
					$LeaveStatus 
					AND a.ClassName = '$target_class_name' 
					AND a.ClassNumber = '$targetNum'
					AND a.RecordType = 2 
					AND a.RecordStatus IN (0,1,2)
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";
			
     $temp = $lc->returnVector($sql);
     $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}

//$allow_item_number=false;  // allow user to input item numbers

# Last selection
if (is_array($student) && sizeof($student)>0)
{
    $list = implode(",", $student);
    $namefield = getNameFieldWithClassNumberByLang();
    $sql = "SELECT UserID , $namefield FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list)";
    $array_students = $lc->returnArray($sql,2);
    if(sizeof($student)==1)
   		$allow_item_number=true;
}

/*
if($allow_item_number){
	$select_item_num = "<SELECT name=itemcount>";
	for($i=1;$i<=20;$i++){
		$select_item_num .="<OPTION value=$i>$i</OPTION>";
	}
	$select_item_num .= "</SELECT>";
}
*/

?>

<SCRIPT LANGUAGE=Javascript>
function addByLogin()
{
         obj = document.form1;
         obj.flag.value = 1;
         generalFormSubmitCheck(obj);
}
function removeStudent(){
		checkOptionRemove(document.form1.elements["student[]"]);
 		submitForm();
}
function submitForm(){
         obj = document.form1;
         obj.flag.value =0;
         generalFormSubmitCheck(obj);
}
function changeClass(obj)
{
         obj.flag.value = 2;
         if (obj.targetNum != undefined)
             obj.targetNum.selectedIndex = 0;
         generalFormSubmitCheck(obj);
}
function addByClassNum()
{
         obj = document.form1;
         obj.flag.value = 2;
         generalFormSubmitCheck(obj);
}
function finishSelection()
{
         obj = document.form1;
         obj.action = 'insert_update.php';
         checkOptionAll(document.form1.elements["student[]"]);
         obj.submit();
         return true;
}
function generalFormSubmitCheck(obj)
{
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
}
function formSubmit(obj)
{
         if (obj.flag.value == 0)
         {
             obj.flag.value = 1;
             generalFormSubmitCheck(obj);
             return true;
         }
         else
         {
             return finishSelection();
         }
}
function checkForm()
{
        obj = document.form1;
        var cnt = 0;
        
        if(obj.elements["student[]"].length != 0)
                cnt++;
        else
        {
                alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
                return false;
        }
        
        if(obj.early_leave_time.value != '')
        {
	        	if(obj.early_leave_time.value.length == 5)
	        	{
        				var time = obj.early_leave_time.value.split(":");
        				if((time[0].length == 2) && (time[1].length == 2))
        				{
	        				if(((time[0]>=00)&&(time[0]<=23)) && ((time[1]>=00)&&(time[1]<=59)))
	        						cnt++
	        				else
	        				{
	        					alert('<?=$i_StudentAttendance_Input_Correct_Time?>');
	        					return false;
        					}
        				}
        				else
        				{
        						alert('<?=$i_StudentAttendance_Input_Correct_Time?>');
        						return false;
    					}
	    		}
	        	else
	        	{
	        			alert('<?=$i_StudentAttendance_Input_Correct_Time?>');
	        			return false;
        		}
		}
		else
				alert('<?=$i_StudentAttendance_Input_Time?>');
        
        if(cnt==2)
        		return formSubmit(obj);
        else
        		return false;
}

</SCRIPT>

<form name="form1" action="" method="post" ONSUBMIT="return checkForm()">
<?=displayNavTitle($i_StudentAttendance_Early_Leave_Insert_Record, '')?>

<center>

<?
	if($student!='')
	{
		$ts_record = strtotime($TargetDate);
		if ($ts_record == -1)
		{
		    $TargetDate = date('Y-m-d');
		    $ts_record = strtotime($TargetDate);
		}
		$txt_year = date('Y',$ts_record);
		$txt_month = date('m',$ts_record);
		$txt_day = date('d',$ts_record);
		
		$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_";
		$card_student_daily_log .= $txt_year."_";
		$card_student_daily_log .= $txt_month;
		
		switch ($period)
		{
	        case "1": $InSchool = " AND b.AMStatus IN (0,2) ";
	        		  $LeaveStatus = " ";
	                                                break;
	        case "2": $InSchool = " AND b.PMStatus IN (0,2) ";
	        		  $LeaveStatus = " AND b.LeaveStatus IS NULL ";
	                                                break;
	        default : $InSchool = " AND b.AMStatus IN (0,2) ";
	        		  $LeaveStatus = " ";
	                                                break;
		}
		
		$student_list = implode(",", $student);
		
		$sql = "
				SELECT 
						".getNameFieldByLang("a.").", b.UserID, b.LeaveSchoolTime, b.LeaveSchoolStation, b.LeaveStatus
				FROM 
						INTRANET_USER AS a LEFT OUTER JOIN
						$card_student_daily_log AS b ON (a.UserID = b.UserID)
				WHERE
						b.DayNumber = $txt_day AND
						b.LeaveStatus IS NOT NULL AND
						b.UserID IN ($student_list)
						$InSchool
						$LeaveStatus
				ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
				";

		$result = $lbdb->returnArray($sql, 5);
		
		if(sizeof($result) > 0)
		{
			$warning = "<table border=0 cellpadding=0 cellspacing=0>";
			$warning .= "<tr><td>$i_StudentAttendance_Early_Leave_Warning</td></tr>";
			$warning .= "<tr><td height=10px></td></tr>";
			
			for($i=0; $i<sizeof($result); $i++)
			{
				list($name, $student_id, $leave_school_time, $leave_school_station, $leave_status) = $result[$i];
				$warning .= "<tr>";
				$warning .= "<td>$name</td>";
				$warning .= "</tr>";
			}
			$warning .= "</table><br \n>";
			echo $warning;
		}
	}
?>

<table width=422 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>

<tr>
	<td><?=$i_general_students_selected?></td>
	<td><table width=100% border=0 cellspacing=0 cellpadding=0>
		<tr>
			<td><SELECT name=student[] size=5 multiple>
				<?
				for ($i=0; $i<sizeof($array_students); $i++)
				{
				     list ($id, $name) = $array_students[$i];
				?>
				<OPTION value='<?=$id?>'><?=$name?></OPTION>
				<?
				}
				?>
				</SELECT></td>
			<td>
			<a href='javascript:newWindow("choose/index.php?fieldname=student[]&period=<?=$period?>&TargetDate=<?=$TargetDate?>", 9)'><img src="<?=$image_path?>/admin/button/s_btn_edit_<?=$intranet_session_language?>.gif" border=0></a>
			<br>
			<a href='javascript:removeStudent();'><img src="<?=$image_path?>/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border=0></a>
			</td>
		</tr>
	</table></td>
<tr>
	<td><?=$i_UserLogin?></td><td><input type=text name=student_login size=20 maxlength=100><a href=javascript:addByLogin()><img src="<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border=0></a></td>
</tr>
<tr>
	<td><?=$i_ClassNameNumber?></td><td><?=$select_class?><?=$select_classnum?></td>
</tr>
<tr>
	<td><?=$i_StudentAttendance_Time_Departure?></td><td><input type=text name=early_leave_time> (HH:mm) (e.g. 07:30, 16:30)</td>
</tr>
<tr>
	<td><?=$i_StudentAttendance_Field_CardStation?></td><td><input type=text name=leave_station></td>
</tr>
<!-- Submit Button -->
<tr>
	<td colspan=2><input type=image onClick="this.form.flag.value=3" src="<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td>
</tr> 

</table>

<table width=422 border=0 cellpadding=0 cellspacing=0>

<!-- Close Button -->
<tr>
	<td colspan=2 align=center height="40" style="vertical-align:bottom"><a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a></td>
</tr>
</table>

<input type=hidden name=flag value=0>
<br>


<input type=hidden name=student_list value=<?=$student_list?>>
<input type=hidden name=TargetDate value=<?=$TargetDate?>>
<input type=hidden name=Period value=<?=$period?>>

</form>


<?php
intranet_closedb();
include_once("../../../../templates/filefooter.php");
?>