<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lc = new libcardstudentattend2();


### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($period)
{
        case "1": $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
        case "2": $display_period = $i_DayTypePM;
                                                $DayType = PROFILE_DAY_TYPE_PM;
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
}

# order information
$default_order_by = " a.ClassName, a.ClassNumber, a.EnglishName";

# order information
$default_order_by = " a.ClassName, a.ClassNumber, a.EnglishName";

if($order_by_time!=1){
	$order_by = $default_order_by ;
}
else{
	#$order_str= $order== 1?" DESC ":" ASC ";
	$order_str= " ASC ";

	$order_by = "b.LeaveSchoolTime $order_str";
	
}

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Early Leave
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
                b.LeaveSchoolTime,
                IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
                c.Reason,
                c.RecordStatus
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_EARLY."'
                WHERE b.DayNumber = '$txt_day' AND b.LeaveStatus = '".CARD_LEAVE_AM."'
                      AND a.RecordType = 2 AND a.RecordStatus IN (0,1)
         ORDER BY $order_by
";
}
else     # Check PM Early Leave
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
                b.LeaveSchoolTime,
                IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
                c.Reason,
                c.RecordStatus
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_EARLY."'
                WHERE b.DayNumber = '$txt_day' AND b.LeaveStatus = '".CARD_LEAVE_PM."'
                AND a.RecordType = 2 AND a.RecordStatus IN (0,1)
         ORDER BY $order_by
";

}

$lexport = new libexporttext();
$exportColumn = array("$i_UserName", "$i_ClassName", "$i_UserClassNumber", 
					"$i_StudentAttendance_LeaveSchoolTime",
					"$i_StudentAttendance_Field_CardStation",
					"$i_SmartCard_Frontend_Take_Attendance_Status", 
					"$i_SmartCard_Frontend_Take_Attendance_Waived", 
					"$i_Attendance_Reason"
					);

$result = $lc->returnArray($sql,9);

for($i=0; $i<sizeOf($result); $i++)
{
    list($record_id, $user_id, $name,$class_name,$class_number, $leave_time, $leave_station, $reason,$record_status) = $result[$i];
    $str_status = $i_StudentAttendance_Status_EarlyLeave;
    $str_reason = $reason;
    $waived_option = $record_status == 1? $i_general_yes:$i_general_no;
	$result[$i] = array($name, $class_name, $class_number, $leave_time, $leave_station, $str_status, $waived_option, $str_reason);
}

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);

$export_content_final = $i_SmartCard_DailyOperation_ViewEarlyLeaveStatus;
$export_content_final .= " $TargetDate ($display_period)\n";

if (sizeof($result)==0)
	$export_content_final .= "$i_StudentAttendance_NoEarlyStudents\n";
else
	$export_content_final .= $export_content;

intranet_closedb();

$filename = "showearlyleave-".time().".csv";
$lexport->EXPORT_FILE($filename, $export_content_final);
?>