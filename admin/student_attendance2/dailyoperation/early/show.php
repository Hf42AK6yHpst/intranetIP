<?php

if ($period == "")
{
	header("Location: index.php");
    exit();
}

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../includes/libwordtemplates.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

### Short-cut for Daily Late / Daily Absent/ Daily Early Leave ###
$arr_page = array(
					array(1,$i_SmartCard_DailyOperation_ViewClassStatus),
					array(2,$i_SmartCard_DailyOperation_ViewLateStatus),
					array(3,$i_SmartCard_DailyOperation_ViewAbsenceStatus)
				);
$pageShortCut_Selection = getSelectByArray($arr_page," name=\"pageShortCut\" onChange=\"changePage(this.value)\" ");

### class used
$lcardattend = new libcardstudentattend2();
$lword = new libwordtemplates();
$page_load_time = time();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);


###period
switch ($period)
{
        case "1": $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
        case "2": $display_period = $i_DayTypePM;
                                                $DayType = PROFILE_DAY_TYPE_PM;
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
}

# order information
$default_order_by = " a.ClassName, a.ClassNumber, a.EnglishName";

if($order_by_time!=1){
	$order_by = $default_order_by ;
}
else{
	#$order_str= $order== 1?" DESC ":" ASC ";
	$order_str= " ASC ";

	$order_by = "b.LeaveSchoolTime $order_str";
	
}
# order info
$order_icon = "<img src='/images/schoolrecord/btn_set.gif' border=0 align=absmiddle>";
$order_by_time_link = "<a class=iconLink a href='javascript:orderByTime(1)' onMouseOver=\"window.status='$i_SmartCard_DailyOperation_OrderBy_InSchoolTime';return true;\" onMouseOut=\"window.status='';return true;\">".$order_icon.$i_SmartCard_DailyOperation_OrderBy_InSchoolTime."</a>";
$order_by_class_name_number_link = "<a class=iconLink href='javascript:orderByTime(0)' onMouseOver=\"window.status='$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber';return true;\" onMouseOut=\"window.status='';return true;\">".$order_icon.$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber."</a>";

$order_link = $order_by_time==1?$order_by_class_name_number_link:$order_by_time_link;

$order_toolbar.="<br><table width=560 border=0 cellpadding=1 cellspacing=1 align=center>";
$order_toolbar.="<tr>";
$order_toolbar.="<td align=right>".$order_link."&nbsp;</td>";
$order_toolbar.="</tr>";
$order_toolbar.="</table>";

# Get Confirmation status
$sql = "SELECT RecordID, EarlyConfirmed, EarlyConfirmTime FROM CARD_STUDENT_DAILY_DATA_CONFIRM
               WHERE RecordDate = '$TargetDate' AND RecordType = $DayType";
$temp = $lcardattend->returnArray($sql,3);
list ($recordID, $confirmed , $confirmTime) = $temp[0];


$col_width = 150;
$table_confirm = "";
$x = "<table width=100% border=0 cellpadding=2 cellspacing=0>\n";
$x .= "<tr><td width=$col_width align=right>$i_StudentAttendance_Field_Date:</td><td align=left>$TargetDate</td></tr>\n";
$x .= "<tr><td width=$col_width align=right>$i_StudentAttendance_Slot:</td><td align=left>$display_period</td></tr>\n";

if($confirmed==1)
{
        $x .= "<tr><td width=$col_width align=right>$i_StudentAttendance_Field_LastConfirmedTime:</td><td align=left>$confirmTime</td></tr>";
}
else
{
        $x .= "<tr><td width=$col_width></td><td align=left>$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record</td></tr>";
}
$x .= "</table>\n";
$table_confirm = $x;

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Early Leave
{
$sql  = "SELECT 
					b.RecordID, a.UserID,
	                ".getNameFieldWithClassNumberByLang("a.")."as name,
	                b.LeaveSchoolTime,
	                IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
	                c.Reason,
	                c.RecordStatus
	                FROM
	                $card_log_table_name as b LEFT OUTER JOIN 
	                INTRANET_USER as a ON (b.UserID = a.UserID) LEFT OUTER JOIN 
	                CARD_STUDENT_PROFILE_RECORD_REASON as c ON (c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND 
	                											c.DayType = $DayType AND c.RecordType = '".PROFILE_TYPE_EARLY."')
         WHERE 
         			b.DayNumber = '$txt_day' AND b.LeaveStatus = '".CARD_LEAVE_AM."' 
         			AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
         ORDER BY 
         			$order_by
		";
}
else     # Check PM Early Leave
{
$sql  = "SELECT 
					b.RecordID, a.UserID,
	                ".getNameFieldWithClassNumberByLang("a.")."as name,
	                b.LeaveSchoolTime,
	                IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
	                c.Reason,
	                c.RecordStatus
	                FROM
	                $card_log_table_name as b LEFT OUTER JOIN 
	                INTRANET_USER as a ON (b.UserID = a.UserID) LEFT OUTER JOIN 
	                CARD_STUDENT_PROFILE_RECORD_REASON as c ON (c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND 
	                											c.DayType = $DayType AND c.RecordType = '".PROFILE_TYPE_EARLY."')
		 WHERE 
			 		b.DayNumber = '$txt_day' AND b.LeaveStatus = '".CARD_LEAVE_PM."'
	                AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
         ORDER BY 
	         		$order_by
		";

}
$result = $lcardattend->returnArray($sql,7);

$table_attend = "";
# <td class=tableTitle>#</td>
$x = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr class=tableTitle><td>#</td><td>$i_UserName</td>
<td>$i_StudentAttendance_LeaveSchoolTime</td>
<td>$i_StudentAttendance_Field_CardStation</td>
<td>$i_SmartCard_Frontend_Take_Attendance_Status</td>
<td width=50>$i_SmartCard_Frontend_Take_Attendance_Waived<input type=checkbox name='all_wavied' onClick=\"(this.checked)?setAllWaived(document.form1,1):setAllWaived(document.form1,0)\"></td>
<td width=100>$i_Attendance_Reason</td>
<td width=13%>$i_StudentGuardian[MenuInfo]</td>";

if($sys_custom['send_sms'] and $plugin['sms'] and $bl_sms_version >= 2)
{
	include_once("../../../../includes/libsmsv2.php");
	$lsms	= new libsmsv2();
	
	//check sms system template status
	$template_status = $lsms->returnTemplateStatus("","STUDENT_ATTEND_EARLYLEAVE");
	$template_content = $lsms->returnSystemMsg("STUDENT_ATTEND_EARLYLEAVE");
	$template_status = $template_status and trim($template_content);
	if($template_status)
		$x .= "<td width=10>$i_SMS_Send <input type='checkbox' name='all_sms' onClick=\"(this.checked)?setAllSend(document.form1,1):setAllSend(document.form1,0)\"></td>";
}	

$x .= "</tr>\n";

$words = $lword->getWordListAttendance(3);
$hasWord = sizeof($words)!=0;
$reason_js_array = "";

$disable_color="#DFDFDF";  # background color for disabled text field
$enable_color="#FFFFFF";  # background color for enabled text field

$select_word = "";

for($i=0; $i<sizeOf($result); $i++)
{
        list($record_id, $user_id, $name, $leave_time, $leave_station, $reason, $record_status) = $result[$i];
        $select_status = "<SELECT name=drop_down_status[$i] onChange=\"setReasonComp(this.value, this.form.reason$i, this.form.editAllowed$i)\">\n";
        $select_status .= "<OPTION value=0>$i_StudentAttendance_Status_OnTime</OPTION>\n";
        $select_status .= "<OPTION value=3 SELECTED>$i_StudentAttendance_Status_EarlyLeave</OPTION>\n";
        $select_status .= "</SELECT>\n";
        $select_status .= "<input name=record_id[$i] type=hidden value=\"$record_id\">\n";
        $select_status .= "<input name=user_id[$i] type=hidden value=\"$user_id\">\n";

        $reason_comp = "<input type=text name=reason$i size=17 maxlength=255 value='$reason' style='background:$enable_color'>";
        if ($hasWord)
        {
	        if($remark!="")
	        	$select_word = "<a onMouseMove='overhere()' href='javascript:showSelection($i,document.form1.editAllowed$i.value,\"r_remark_".$i."\")'><img src=\"$image_path/icon_alt.gif\" border=0 alt='$i_Profile_SelectReason'></a><input type='hidden' id='r_remark_".$i."' value='$remark'>";
	        else
	        	$select_word = "<a onMouseMove='overhere()' href='javascript:showSelection($i,document.form1.editAllowed$i.value,\"\")'><img src=\"$image_path/icon_alt.gif\" border=0 alt='$i_Profile_SelectReason'></a>";
/*	        	
            $select_word = "<a onMouseMove='overhere()' href=javascript:showSelection($i,document.form1.editAllowed$i.value)><img src=\"$image_path/icon_alt.gif\" border=0 alt='$i_Profile_SelectReason'></a>";
            $txtContent = "<table width=100% border=0 cellpadding=1 cellspacing=1>";
            $txtContent.="<tr><td><b>[$i_Attendance_Standard]</b></td></tr>";
            for ($j=0; $j<sizeof($words); $j++)
            {
                 $temp = addslashes($words[$j]);
                 $temp2 = addslashes($temp);
                 #$txtContent .= "<tr><td> - <a href=\\\"javascript:putBack(document.form1.reason$i,'$temp2');\\\">$temp</a></td></tr>";
                 $txtContent .= "<tr><td><table border=0 cellpaddin=0 cellspacing=0 width=100%><tr><td valign=top width=8> - </td><td align=left><a href=\\\"javascript:putBack(document.form1.reason$i,'$temp2');\\\">$temp</a></td></tr></table></td></tr>";
            }
            $txtContent .= "</table>";
           # $reason_js_array .= "reasonSelection[$i] = \"<table border=0 cellspacing=0 cellpadding=1><tr><td class=tipborder><table width=100% border=0 cellspacing=0 cellpadding=3><tr><td class=tipbg valign=top><font size=-2>$txtContent</font></td></tr></table></td></tr></table>\";\n";
            $reason_js_array .= "reasonSelection[$i] = \"<table  border=0 cellspacing=0 cellpadding=1><tr><td class=tipborder><table border=0 cellspacing=0 cellpadding=3><tr><Td class=tipbg><table border=0 cellspacing=0 cellpadding=0><tr><td><input type=button value=' X ' onClick=hideMenu('ToolMenu')></td></tr></table></td></tr><tr><td class=tipbg valign=top><font size=-2>$txtContent</font></td></tr></table></td></tr></table>\";\n";
*/
        }

        $waived_option = "<input type=checkbox name=waived_{$user_id}".(($record_status == 1) ? " CHECKED" : " ") .">";
        $sms_option = "<input type=checkbox name=sms_{$user_id}>";
        
        $x .= "<tr class=$css><td >".($i+1)."</td><td >$name</td><td>$leave_time</td><td >$leave_station</td><td >$select_status</td><td>$waived_option</td><td>$reason_comp$select_word</td><td align=center><a onMouseMove='overhere()' href=javascript:retrieveGuardianInfo($user_id)><img src=\"$image_path/icons_guardian_info.gif\" border=0 alt='$button_select'></a></td>";
        if($sys_custom['send_sms'] and $plugin['sms'] and $bl_sms_version >= 2 and $template_status) $x .= "<td>$sms_option</td>";
        $x .= "</tr>\n";
}
if (sizeof($result)==0)
{
    $x .= "<tr class=tableContent><td colspan=9 align=center>$i_StudentAttendance_NoEarlyStudents</td></tr>\n";
}
$x .= "</table>\n";
$table_attend = $x;

$confirm_button = "<a href=\"javascript:AlertPost(document.form1,'show_update.php','$i_SmartCard_Confirm_Update_Attend?')\"><img src='$image_path/admin/button/s_btn_submit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$back_button = "<a href=\"index.php?period=$period\"><img src='$image_path/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
if($sys_custom['send_sms'] and $plugin['sms'] and $bl_sms_version >= 2 and $template_status)	
	$sms_button = "<a href=\"javascript:sendSMS(document.form1)\" class=iconLink><img src=\"$image_path/admin/icon_send_sms.gif\" border=\"0\" align=\"absmiddle\">". $i_SMS_Send  ."</a>&nbsp;";
else
	$sms_button = "<a href=\"javascript:Prompt_No_SMS_Plugin_Warning()\" class=iconLink><img src=\"$image_path/admin/icon_send_sms.gif\" border=\"0\" align=\"absmiddle\">". $i_SMS_Send  ."</a>&nbsp;";
		
$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";
//$toolbar2 = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";
//$toolbar3 = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";
//echo "<a class=iconLink href=javascript:newWindow('insert_record.php?TargetDate=$TargetDate&period=$period',2)>".newIcon().$button_new."</a>";
//$toolbar .= "<tr><td>";
//$toolbar .= "<a class=iconLink href=javascript:newWindow('insert_record.php?TargetDate=$TargetDate&period=$period',2)>".newIcon().$button_new."</a>";
//$toolbar .= "<a class=iconLink href='export.php?TargetDate=$TargetDate&period=$period&order_by_time=$order_by_time')>".exportIcon().$button_export."</a>";
//$toolbar .= "</td></tr>";
$toolbar .= "<br><a class=iconLink href=javascript:newWindow('insert_record.php?TargetDate=$TargetDate&period=$period',2)>".newIcon().$button_new."</a>";
$toolbar .= "&nbsp;&nbsp;&nbsp;<a class=iconLink href='export.php?TargetDate=$TargetDate&period=$period&order_by_time=$order_by_time')>".exportIcon().$button_export."</a>";

if ($sent == 1)
{
    $xmsg = "$i_SMS_MessageSent";
}
?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     #ToolMenu2{position:absolute; top: 0px; left: 0px; z-index:5; visibility:show;  width: 450px;}
</style>

     <script language="JavaScript">
     isMenu = true;
     </script>
     <div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>
     <div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>

<script language="JavaScript" type="text/javascript">
function Prompt_No_SMS_Plugin_Warning()
{
	alert("<?=$i_SMS['jsWarning']['NoPluginWarning']?>");
}

var reasonSelection = Array();
function temp_reasonSelection(i, remark)
{
	str = "<table width=100% border=0 cellpadding=1 cellspacing=1>";
    if(remark!="")
    {
	    remark = remark.replace("'", "&#039;");
        str += "<tr><td><font color=red><b>[<?=$i_Attendance_Others?>]</b></font></td></tr>";
    	str += "<tr><td><table border=0 cellspacing=0 cellpadding=0 width=100%><tr><td valign=top width=8><font color=red> - </font></td><td align=left><a href='javascript:putBack(document.form1.reason"+i+",\""+remark+"\");'><font color=red>"+remark+"</font></a><input type='hidden' id='rrr_remark_"+i+"' value='"+remark+"'></td></tr></table><Br></td></tr>";
   	}
   	str += "<tr><td><b>[<?=$i_Attendance_Standard?>]</b></td></tr>";

   	<?
    for ($j=0; $j<sizeof($words); $j++)
    {
		$temp = addslashes($words[$j]);
		$temp2 = addslashes($temp);
	?>
		str += "<tr><td><table border=0 cellpaddin=0 cellspacing=0 width=100%><tr><td valign=top width=8> - </td><td align=left><a href='javascript:putBack(document.form1.reason"+i+",\"rrr_"+i+"_<?=$j?>\");'><?=$temp?></a><input type='hidden' id='rrr_"+i+"_<?=$j?>' value='<?=$temp?>'></td></tr></table></td></tr>";
    <?
	}
	?>
    str += "</table>";
    
    reasonSelection[i] = "<table border=0 cellspacing=0 cellpadding=1><tr><td class=tipborder><table width=100% border=0 cellspacing=0 cellpadding=3><Td class=tipbg><table border=0 cellspacing=0 cellpadding=0><tr><td><input type=button value=' X ' onClick=hideMenu('ToolMenu')></td></tr></table></td></tr><tr><td class=tipbg valign=top><font size=-2>"+str+"</font></td></tr></table></td></tr></table>";
	
    return reasonSelection[i];
}

function setAllSend(formObj,val){
	if(formObj==null) return;
	for(i=0;i<formObj.elements.length;i++){
		obj = formObj.elements[i];
		if(typeof(obj)!="undefined"){
			if(obj.name.indexOf("sms_")>-1){
				obj.checked = val==1?true:false;
			}
		}
	}
}

function sendSMS(formObj)
{
	var sms_no=0;
	for(i=0;i<formObj.elements.length;i++)
	{
		obj = formObj.elements[i];
		if(typeof(obj)!="undefined"){
			if(obj.name.indexOf("sms_")>-1){
				if(obj.checked)	sms_no = 1;
			}
		}
	}
	
	if(sms_no)
	{
		formObj.action = "../confirm_send_sms.php";
		formObj.submit();
	}
	else
	{
		alert("<?=$i_SMS_no_student_select?>");	
	}
}	

<?//=$reason_js_array?>
<!--

function openPrintPage()
{
        newWindow("show_print.php?period=<?=$period?>&TargetDate=<?=$TargetDate?>&order_by_time=<?=$order_by_time?>",4);
}
function showSelection(i, allowed, remarkObjName)
{
		remarkObj = document.getElementById(remarkObjName);
	if(remarkObj!=null)
		remark = remarkObj.value;
	else remark = "";	
         if (allowed == 1)
         {
             showMenu('ToolMenu',temp_reasonSelection(i,remark));
         }
}
/*
function putBack(obj, value)
{
         obj.value = value;
         hideMenu('ToolMenu');
}
*/
function putBack(obj, valueObjName)
{
	     valueObj = document.getElementById(valueObjName);

		 if(valueObj!=null)
         	obj.value = valueObj.value;
         else obj.value="";	     
         hideMenu('ToolMenu');
}
function setReasonComp(value, txtComp, hiddenFlag)
{
         if (value==3)
         {
             txtComp.disabled = false;
             txtComp.style.background='<?=$enable_color?>';
             hiddenFlag.value = 1;
         }
         else
         {
             txtComp.disabled = true;
             txtComp.value="";
             txtComp.style.background='<?=$disable_color?>';
             hiddenFlag.value = 0;
         }
}
function resetForm(formObj){
	formObj.reset();
  	hideMenu('ToolMenu');
	resetFields(formObj);	
}

function resetFields(formObj){
	status_list = document.getElementsByTagName('SELECT');
	if(status_list==null) return;
	
	for(i=0;i<status_list.length;i++){
		s = status_list[i];
		if(s==null) continue;
		reasonObj = eval('formObj.reason'+i);
		if(s.selectedIndex==1){
			if(reasonObj!=null){
				reasonObj.disabled=false;
				reasonObj.style.background='<?=$enable_color?>';

			}
		}else{
			if(reasonObj!=null){
				reasonObj.value="";
				reasonObj.disabled=true;
				reasonObj.style.background='<?=$disable_color?>';
			}
		}

	}
}
function orderByTime(v){
	fObj = document.form1;
	if(fObj==null ) return;
	orderByTimeObj = fObj.order_by_time;
	if(orderByTimeObj==null) return;
	orderByTimeObj.value = v;
	fObj.action="";
	fObj.submit();
	
}
function setAllWaived(formObj,val){
	if(formObj==null) return;
	for(i=0;i<formObj.elements.length;i++){
		obj = formObj.elements[i];
		if(typeof(obj)!="undefined"){
			if(obj.name.indexOf("waived_")>-1){
				obj.checked = val==1?true:false;
			}
		}
	}
}
function moveToolTip2(lay, FromTop, FromLeft){

     if (tooltip_ns6) {

         var myElement = document.getElementById(lay);

          myElement.style.left = (FromLeft + 10) + "px";

          myElement.style.top = (FromTop + document.body.scrollTop) + "px";

     }

     else if(tooltip_ie4) {
          eval(doc + lay + sty + ".top = "  + (FromTop + document.body.scrollTop))
     }
 	else if(tooltip_ns4){eval(doc + lay + sty + ".top = "  +  FromTop)}

     if (!tooltip_ns6) eval(doc + lay + sty + ".left = " + (FromLeft + 10));
   
}
// AJAX follow-up
var callback = {
        success: function ( o )
        {
                jChangeContent( "ToolMenu2", o.responseText );
        }
}
// start AJAX
function retrieveGuardianInfo(UserID)
{
        //FormObject.testing.value = 1;
        obj = document.form1;
        var myElement = document.getElementById("ToolMenu2");
        
        showMenu("ToolMenu2","<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>Loading</td></tr></table>");
        YAHOO.util.Connect.setForm(obj);

        // page for processing and feedback
        var path = "getGuardianInfo.php?UserID=" + UserID;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

function changePage(val)
{
	var obj = document.form1;
	var path = "/admin/student_attendance2/dailyoperation";
	var period = document.form1.period.value;
	var date = document.form1.TargetDate.value;
	
	if(val == 1){
		obj.action = path+"/class/class_status.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
	if(val == 2){
		obj.action = path+"/late/showlate.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
	if(val == 3){
		obj.action = path+"/absence/show.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
}
-->
</script>
<form name="form1" method="post" action="show_update.php" >
<? $this_page_title = displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../student_attendance2/',$i_StudentAttendance_Menu_DailyOperation,'../dailyoperation/',$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus, 'early/index.php',"$TargetDate ($display_period)",'') ?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus, 'index.php',"$TargetDate ($display_period)",'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $x) ?>
<?php // data expired error message
	if($error==1){
		echo "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>";
		echo "<tr><td align=right><font color=red>$i_StudentAttendance_Warning_Data_Outdated</font></td></tr>";
		echo "</table>";
	}
?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=left><?=$table_confirm?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>
<?=$order_toolbar?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td colspan=2 align="right"><?echo $i_StudentAttendance_ShortCut.": ".$pageShortCut_Selection;?></td></tr>
<tr height="10px"><td colspan=2 align="right"></td></tr>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr>
	<td>
		<table width=100% border=0 cellpadding=0 cellspacing=0 align=center>
		<tr>
			<td class=admin_bg_menu><?=$toolbar?></td>
			<td class=admin_bg_menu align="right"><?=$sms_button?></td>
		</tr>
		</table>
	</td>
</tr>
<tr><td class=tableContent align=center><?=$table_attend?></td></tr>
<!--
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
-->
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td align=right>
<?php  $button_reset = "<a href='javascript:resetForm(document.form1)'><img src='$image_path/admin/button/s_btn_reset_$intranet_session_language.gif' border='0' align='absmiddle'></a>"; ?>

<?=$confirm_button.toolBarSpacer().$button_reset.toolBarSpacer().$back_button?></td></tr>
</table>

<input name=period type=hidden value="<?=$period?>">
<input name=TargetDate type=hidden value="<?=$TargetDate?>">
<input name=order_by_time type=hidden value="<?=$order_by_time?>">
<? for ($i=0; $i<sizeof($result); $i++) { ?>
<input type=hidden name=editAllowed<?=$i?> value=1>
<? } ?>

<input type="hidden" name="this_page_title" value="<?=$this_page_title?>">
<input type="hidden" name="this_page" value="early/show.php">
<input type="hidden" name="TemplateCode" value="STUDENT_ATTEND_EARLYLEAVE">
<input name=PageLoadTime type=hidden value="<?=$page_load_time?>">
</form>
<br><br>
<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>