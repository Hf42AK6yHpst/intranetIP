<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");

intranet_opendb();

# class used
$LIDB = new libdb();
$lcardattend = new libcardstudentattend2();


# check page load time 
$error = "";
if(sizeof($user_id) > 0)
{
	if($lcardattend->isDataOutDated($user_id,$TargetDate,$PageLoadTime)){
		$error = 1;  // data outdated
		//echo "Error<BR>";
		//echo $PageLoadTime;
		$return_page = "show.php";
		
		$return_page = $return_page."?period=$period&TargetDate=$TargetDate&error=$error";
		header("Location: $return_page");
		exit();	
	}
}

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || ($period!=1 && $period!=2) )
{
    header("Location: index.php");
    exit();
}

$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$year = getCurrentAcademicYear();
$semester = getCurrentSemester();

$my_record_date = date('Y-m-d',$ts_record);

### update daily records
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

for($i=0; $i<sizeOf($user_id); $i++)
{
        $my_user_id = $user_id[$i];
        $my_day = $txt_day;
        $my_drop_down_status = $drop_down_status[$i];
        $my_record_id = $record_id[$i];

        // Retrieve Waived only if absent
        ($my_drop_down_status == CARD_STATUS_ABSENT) ? ($my_record_status = (${"waived_".$my_user_id} == '') ? 0 : 1) : "";

        if( $period == "1")        # AM
        {
            if ($my_drop_down_status == CARD_STATUS_ABSENT)       # Absent
            {
                # Reason input
                $txtReason = ${"reason$i"};
                $txtReason = intranet_htmlspecialchars($txtReason);

                # Get ProfileRecordID
                $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                $temp = $lcardattend->returnArray($sql,2);
                list($reason_record_id, $reason_profile_id) = $temp[0];

                if ($reason_record_id == "")           # Reason record not exists
                {
                    # Search whether attendance exists by date, student and type
                    $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                   WHERE AttendanceDate = '$TargetDate'
                                         AND UserID = $my_user_id
                                         AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                         AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                    $temp = $lcardattend->returnVector($sql);
                    $attendance_id = $temp[0];
                    if ($attendance_id == "")          # Record not exists
                    {
                            if ($my_record_status != 1)
                            {
                                # Insert profile record
                                $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                $temp = $lcardattend->returnArray($sql,2);
                                list ($user_classname, $user_classnum) = $temp[0];
                                $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason";
                                $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason'";
                                $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                $lcardattend->db_db_query($sql);
                                $attendance_id = $lcardattend->db_insert_id();
                            }
                    }
                    else
                    {
                            if ($my_record_status == 1)
                            {
                                    // Delete Record if waived
                                                        $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                        $lcardattend->db_db_query($sql);
                                                        $attendance_id = "";
                            }
                            else
                            {
                                # Update Reason in profile record By AttendanceID
                                $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                       WHERE StudentAttendanceID = '$attendance_id'";
                            	$lcardattend->db_db_query($sql);
                        	}
                    }

                    # remove previous Late Record from Reason Table ( if exists)
                    $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."' AND RecordType='".PROFILE_TYPE_LATE."'";
                    $lcardattend->db_db_query($sql);

                    # remove previous Late Record from PROFILE Table ( if exists)
                    $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."' AND RecordType='".PROFILE_TYPE_LATE."'";
                    $lcardattend->db_db_query($sql);


                    # Insert to Reason table
                    $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, RecordStatus, DateInput, DateModified";
                    $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_ABSENT."', '".PROFILE_DAY_TYPE_AM."', '$my_record_status', now(), now() ";
                    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                   VALUES ($fieldsvalues)";
                    $lcardattend->db_db_query($sql);
                }
                else  # Reason record exists
                {
                    if ($reason_profile_id == "")    # Profile ID not exists
                    {
                        # Search whether attendance exists by date, student and type
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE AttendanceDate = '$TargetDate'
                                             AND UserID = $my_user_id
                                             AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                             AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];

                        if ($attendance_id == "")          # Record not exists
                        {
                                if ($my_record_status != 1)
                                {
                                    # Insert profile record
                                    $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                    $temp = $lcardattend->returnArray($sql,2);
                                    list ($user_classname, $user_classnum) = $temp[0];
                                    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason";
                                    $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason'";
                                    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                    $lcardattend->db_db_query($sql);
                                    $attendance_id = $lcardattend->db_insert_id();
                                }
                        }
                        else
                        {
                                if ($my_record_status == 1)
                                {
                                        // Delete Record if waived
                                        $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                $lcardattend->db_db_query($sql);
                                                                $attendance_id = "";
                                }
                                else
                                {
                                    # Update Reason in profile record By AttendanceID
                                    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                }
                        }
                    }
                    else  # Has Profile ID
                    {
                        # Search Attendance By ID
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE StudentAttendanceID = $reason_profile_id";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];

                        if ($attendance_id == "")          # Record not exists
                        {
                            # Search attendance by date, student and type
                            $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                           WHERE AttendanceDate = '$TargetDate'
                                                 AND UserID = $my_user_id
                                                 AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                                 AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                            $temp = $lcardattend->returnVector($sql);
                            $attendance_id = $temp[0];
                            if ($attendance_id == "")          # Record not exists
                            {
                                    if ($my_record_status != 1)
                                    {
                                        # insert reason in profile record
                                        $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                        $temp = $lcardattend->returnArray($sql,2);
                                        list ($user_classname, $user_classnum) = $temp[0];
                                        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason";
                                        $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason'";
                                        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                        $lcardattend->db_db_query($sql);
                                        $attendance_id = $lcardattend->db_insert_id();
                                    }
                            }
                            else
                            {
                                    if ($my_record_status == 1)
                                    {
                                            // Delete Record if waived
                                                                        $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                        $lcardattend->db_db_query($sql);
                                                                        $attendance_id = NULL;
                                    }
                                    else
                                    {
                                        # Update Reason in profile record By AttendanceID
                                        $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                               WHERE StudentAttendanceID = '$attendance_id'";
                                        $lcardattend->db_db_query($sql);
                                    }
                            }
                        }
                        else # profile record exists and valid
                        {
                                if ($my_record_status == 1)
                                {
                                        // Delete Record if waived
                                                                $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                $lcardattend->db_db_query($sql);
                                                                $attendance_id = NULL;
                                }
                                else
                                {
                                    # Update reason in profile record
                                    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                }
                        }
                    }

                    # remove previous Late Record from Reason Table ( if exists)
                    $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."' AND RecordType='".PROFILE_TYPE_LATE."'";
                    $lcardattend->db_db_query($sql);

                    # remove previous Late Record from PROFILE Table ( if exists)
                    $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."' AND RecordType='".PROFILE_TYPE_LATE."'";
                    $lcardattend->db_db_query($sql);


                    # Update reason table record
                    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
                                   ProfileRecordID = '$attendance_id',
                                   RecordStatus = '$my_record_status'
                                   WHERE RecordID= '$reason_record_id'";
                    $lcardattend->db_db_query($sql);
                }


                # Add medical reason
                if ($sys_custom['hku_medical_research'])
                {
                    # Try insert
                    $sql = "INSERT IGNORE INTO SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                                   (StudentID, RecordDate, DayType, MedicalReasonType, DateInput , DateModified)
                                   VALUES
                                   ('$my_user_id', '$TargetDate', '".PROFILE_DAY_TYPE_AM."',
                                   '".${"medical_reason_type_".$my_user_id}."', now(),now())";
                    $lcardattend->db_db_query($sql);
                    if ($lcardattend->db_affected_rows()!=1)
                    {
                        # Try update
                        $sql = "UPDATE SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                                       SET MedicalReasonType = '".${"medical_reason_type_".$my_user_id}."'
                                       WHERE RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                       AND StudentID = '$my_user_id'
                                       ";
                        $lcardattend->db_db_query($sql);
                    }
                }

            }
            else if ($my_drop_down_status == CARD_STATUS_PRESENT || $my_drop_down_status == CARD_STATUS_OUTING)    # On Time or Outing
            {
                 # Remove Reason record
                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                 $lcardattend->db_db_query($sql);
                 # Remove Profile record
                 $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                               WHERE AttendanceDate = '$TargetDate'
                                     AND UserID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                 $lcardattend->db_db_query($sql);
                 
                 # Set to Daily Record Table
                 # Set AMStatus and InSchoolTime
                 # Keep InSchool Time
				 ## First - Check the selected status is different from the old one or not
                 $sql = "SELECT AMStatus FROM $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = $my_user_id";
                 $arr_AMStatus = $lcardattend->returnVector($sql);
                 $tmp_AMStatus = $arr_AMStatus[0];
                 if($tmp_AMStatus != $my_drop_down_status){		## if different, then update
	                 $sql = "UPDATE $card_log_table_name
	                                SET AMStatus = '".$my_drop_down_status."',
	                                DateModified = NOW()
	                                WHERE DayNumber = '$txt_day'
	                                      AND UserID = $my_user_id";
	                 $lcardattend->db_db_query($sql);
                 }

                 if($my_drop_down_status== CARD_STATUS_PRESENT){ ## Bad Action

                           $lcardattend->removeBadActionFakedCardAM($my_user_id,$my_record_date);
                           # forgot to bring card
                           $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$old_inTime);
                     }

                 # Add medical reason
                 if ($sys_custom['hku_medical_research'])
                 {
                    # Try remove
                    $sql = "DELETE FROM SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                                       WHERE RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                       AND StudentID = '$my_user_id'
                                       ";
                    $lcardattend->db_db_query($sql);

                }


            }
            else # Unknown action
            {
                 # Do nthg
            }
        }
        else if ($period == "2") # PM
        {
            if ($my_drop_down_status == CARD_STATUS_ABSENT)       # Late
            {
                # Reason input
                $txtReason = ${"reason$i"};
                $txtReason = intranet_htmlspecialchars($txtReason);

                # Get ProfileRecordID
                $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                $temp = $lcardattend->returnArray($sql,2);
                list($reason_record_id, $reason_profile_id) = $temp[0];

                if ($reason_record_id == "")           # Reason record not exists
                {
                    # Search whether attendance exists by date, student and type
                    $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                   WHERE AttendanceDate = '$TargetDate'
                                         AND UserID = $my_user_id
                                         AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                         AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                    $temp = $lcardattend->returnVector($sql);
                    $attendance_id = $temp[0];
                    if ($attendance_id == "")          # Record not exists
                    {
                            if ($my_record_status != 1)
                            {
                                # Insert profile record
                                $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                $temp = $lcardattend->returnArray($sql,2);
                                list ($user_classname, $user_classnum) = $temp[0];
                                $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason";
                                $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason'";
                                $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                $lcardattend->db_db_query($sql);
                                $attendance_id = $lcardattend->db_insert_id();
                            }
                    }
                    else
                    {
                            if ($my_record_status == 1)
                            {
                                    // Delete Record if waived
                                                        $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                        $lcardattend->db_db_query($sql);
                                                        $attendance_id = NULL;
                            }
                            else
                            {
                                # Update Reason in profile record By AttendanceID
                                $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                       WHERE StudentAttendanceID = '$attendance_id'";
                                $lcardattend->db_db_query($sql);
                            }
                    }

                    # remove previous Late Record from Reason Table ( if exists)
                    $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."' AND RecordType='".PROFILE_TYPE_LATE."'";
                    $lcardattend->db_db_query($sql);

                    # remove previous Late Record from PROFILE Table ( if exists)
                    $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."' AND RecordType='".PROFILE_TYPE_LATE."'";
                    $lcardattend->db_db_query($sql);


                    # Insert to Reason table
                    $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, RecordStatus, DateInput, DateModified";
                    $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_ABSENT."', '".PROFILE_DAY_TYPE_PM."', '$my_record_status', now(), now() ";
                    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                   VALUES ($fieldsvalues)";
                    $lcardattend->db_db_query($sql);
                }
                else  # Reason record exists
                {
                    if ($reason_profile_id == "")    # Profile ID not exists
                    {
                        # Search whether attendance exists by date, student and type
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE AttendanceDate = '$TargetDate'
                                             AND UserID = $my_user_id
                                             AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                             AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];
                        if ($attendance_id == "")          # Record not exists
                        {
                                if ($my_record_status != 1)
                                {
                                    # Insert profile record
                                    $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                    $temp = $lcardattend->returnArray($sql,2);
                                    list ($user_classname, $user_classnum) = $temp[0];
                                    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason";
                                    $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason'";
                                    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                    $lcardattend->db_db_query($sql);
                                    $attendance_id = $lcardattend->db_insert_id();
                                }
                        }
                        else
                        {
                                if ($my_record_status == 1)
                                                        {
                                                                // Delete Record if waived
                                                                $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                $lcardattend->db_db_query($sql);
                                                                $attendance_id = NULL;
                                                        }
                                                        else
                                                        {
                                    # Update Reason in profile record By AttendanceID
                                    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                }
                        }
                    }
                    else  # Has Profile ID
                    {
                        # Search Attendance By ID
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE StudentAttendanceID = $reason_profile_id";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];
                        if ($attendance_id == "")          # Record not exists
                        {
                            # Search attendance by date, student and type
                            $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                           WHERE AttendanceDate = '$TargetDate'
                                                 AND UserID = $my_user_id
                                                 AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                                 AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                            $temp = $lcardattend->returnVector($sql);
                            $attendance_id = $temp[0];
                            if ($attendance_id == "")          # Record not exists
                            {
                                    if ($my_record_status != 1)
                                    {
                                        # insert reason in profile record
                                        $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                        $temp = $lcardattend->returnArray($sql,2);
                                        list ($user_classname, $user_classnum) = $temp[0];
                                        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason";
                                        $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason'";
                                        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                        $lcardattend->db_db_query($sql);
                                        $attendance_id = $lcardattend->db_insert_id();
                                }
                            }
                            else
                            {
                                    if ($my_record_status == 1)
                                    {
                                            // Delete Record if waived
                                                                        $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                        $lcardattend->db_db_query($sql);
                                                                        $attendance_id = "";
                                    }
                                    else
                                    {
                                        # Update Reason in profile record By AttendanceID
                                        $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                               WHERE StudentAttendanceID = '$attendance_id'";
                                        $lcardattend->db_db_query($sql);
                                    }
                            }
                        }
                        else # profile record exists and valid
                        {
                                if ($my_record_status == 1)
                                {
                                        // Delete Record if waived
                                                                $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                $lcardattend->db_db_query($sql);
                                                                $attendance_id = "";
                                }
                                else
                                {
                                    # Update reason in profile record
                                    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                }
                        }
                    }

                    # remove previous Late Record from Reason Table ( if exists)
                    $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."' AND RecordType='".PROFILE_TYPE_LATE."'";
                    $lcardattend->db_db_query($sql);

                    # remove previous Late Record from PROFILE Table ( if exists)
                    $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."' AND RecordType='".PROFILE_TYPE_LATE."'";
                    $lcardattend->db_db_query($sql);


                    # Update reason table record
                    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
                                   ProfileRecordID = '$attendance_id',
                                   RecordStatus = '$my_record_status'
                                   WHERE RecordID= '$reason_record_id'";
                    $lcardattend->db_db_query($sql);
                }

                # Add medical reason
                if ($sys_custom['hku_medical_research'])
                {
                    # Try insert
                    $sql = "INSERT IGNORE INTO SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                                   (StudentID, RecordDate, DayType, MedicalReasonType, DateInput , DateModified)
                                   VALUES
                                   ('$my_user_id', '$TargetDate', '".PROFILE_DAY_TYPE_PM."',
                                   '".${"medical_reason_type_".$my_user_id}."', now(),now())";
                    $lcardattend->db_db_query($sql);
                    if ($lcardattend->db_affected_rows()!=1)
                    {
                        # Try update
                        $sql = "UPDATE SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                                       SET MedicalReasonType = '".${"medical_reason_type_".$my_user_id}."'
                                       WHERE RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                       AND StudentID = '$my_user_id'
                                       ";
                        $lcardattend->db_db_query($sql);
                    }
                }


            }
            else if ($my_drop_down_status == CARD_STATUS_PRESENT || $my_drop_down_status == CARD_STATUS_OUTING)    # On Time or Outing
            {
                 # Remove Reason record
                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                 $lcardattend->db_db_query($sql);
                 # Remove Profile record
                 $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                               WHERE AttendanceDate = '$TargetDate'
                                     AND UserID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                 $lcardattend->db_db_query($sql);
                 # Set to Daily Record Table
                 # Set PMStatus
                 ## First - Check the selected status is different from the old one or not
                 $sql = "SELECT PMStatus FROM $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = $my_user_id";
                 $arr_PMStatus = $lcardattend->returnVector($sql);
                 $tmp_PMStatus = $arr_PMStatus[0];
                 if($tmp_PMStatus != $my_drop_down_status){		## if different, then update
	                 $sql = "UPDATE $card_log_table_name
	                                SET PMStatus = '".$my_drop_down_status."',
	                                DateModified = NOW()
	                                WHERE DayNumber = '$txt_day'
	                                      AND UserID = $my_user_id";
	                 $lcardattend->db_db_query($sql);
                 }

                if($my_drop_down_status== CARD_STATUS_PRESENT){ ## Bad Action

                           $lcardattend->removeBadActionFakedCardPM($my_user_id,$my_record_date);
                           # forgot to bring card
                           $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$old_inTime);
                     }

                 # Add medical reason
                 if ($sys_custom['hku_medical_research'])
                 {
                    # Try remove
                    $sql = "DELETE FROM SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                                       WHERE RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                       AND StudentID = '$my_user_id'
                                       ";
                    $lcardattend->db_db_query($sql);

                 }

            }
            else # Unknown action
            {
                 # Do nthg
            }

        }
        else # Unknown action
        {
               # Do nthg
        }
} # End of For-Loop

if ($lcardattend->attendance_mode=="NO")
{
    $lcardattend->retrieveSettings();
}

$need2mail = false;
if ($period==1)
{
    $period_type = PROFILE_DAY_TYPE_AM;
    # Send email
    if ($sys_custom['hku_medical_research'])
    {
        if ($sys_custom['hku_medical_research_force_send'] || $lcardattend->attendance_mode==0)
        {
            $need2mail = true;
            $mail_success = $lcardattend->sendMedicalReport($TargetDate);
        }
    }
}
else if ($period==2)
{
    $period_type = PROFILE_DAY_TYPE_PM;
    # Send email
    if ($sys_custom['hku_medical_research'])
    {
        $need2mail = true;
        $mail_success = $lcardattend->sendMedicalReport($TargetDate);
    }
}
else
{
    $period_type = "";
}
if ($period_type != "")
{
    # Update Confirm Record
    $sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM
                   SET AbsenceConfirmed = 1, AbsenceConfirmTime = now(), DateModified = now()
                   WHERE RecordDate = '$TargetDate' AND RecordType = $period_type";
    $lcardattend->db_db_query($sql);
    if ($lcardattend->db_affected_rows()!=1)         # Not Exists
    {
        # Not exists
        $sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate, AbsenceConfirmed,RecordType, AbsenceConfirmTime, DateInput, DateModified)
                       VALUES ('$TargetDate',1,$period_type,now(),now(),now())";
        $lcardattend->db_db_query($sql);
    }
}

if ($need2mail)
{
    if ($mail_success)
    {
        $mail_msg= "&mail_msg=1";
    }
    else
    {
        $mail_msg = "&mail_msg=2";
    }
}

header("Location: show.php?period=$period&TargetDate=$TargetDate&msg=2".$mail_msg);
?>