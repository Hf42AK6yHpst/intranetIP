<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lc = new libcardstudentattend2();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($period)
{
        case "1": $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
        case "2": $display_period = $i_DayTypePM;
                                                $DayType = PROFILE_DAY_TYPE_PM;
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
}

$lexport = new libexporttext();
$export_content_final = $lc->generateMedicalReasonSummary($TargetDate);

if($export_content_final!=-1){
	intranet_closedb();
	$filename = "medical_report_".$BroadlearningClientID."-".$TargetDate.".csv";
	$lexport->EXPORT_FILE($filename, $export_content_final);
}
else{ // no classes or websams class level has not been set 
/*
                    include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
                    //echo "$i_import_error <br>\n $error_msg\n";
                    echo "<BR>$i_MedicalExportError \n<BR>";
                    echo  "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                                <tr><td><hr size=1></td></tr>
                                <tr><td align='right'><a href='javascript:history.go(-1)'><img src='/images/admin/button/s_btn_back_$intranet_session_language.gif' border='0'></a></td></tr></table>";

                    //echo $li->queries;
                    include_once($PATH_WRT_ROOT."templates/adminfooter.php");
*/
                    intranet_closedb();
     header("Location: show.php?TargetDate=".$TargetDate."&period=".$period."&medical_msg=1");
}


?>
