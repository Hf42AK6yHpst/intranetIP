<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lc = new libcardstudentattend2();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($period)
{
    case "1": $display_period = $i_DayTypeAM;
              $DayType = PROFILE_DAY_TYPE_AM;
              break;
    case "2": $display_period = $i_DayTypePM;
              $DayType = PROFILE_DAY_TYPE_PM;
              break;
    default : $display_period = $i_DayTypeAM;
              $DayType = PROFILE_DAY_TYPE_AM;
              break;
}

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Late
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
                b.InSchoolTime,
                IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
                b.AMStatus,
                c.Reason,
                c.RecordStatus,
                d.Remark
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_ABSENT."'
                   LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (b.UserID = d.StudentID AND d.RecordDate='$TargetDate')
                WHERE b.DayNumber = '$txt_day'
                      AND (b.AMStatus = '".CARD_STATUS_ABSENT."' OR b.AMStatus = '".CARD_STATUS_OUTING."')
                      AND a.RecordType = 2 AND a.RecordStatus IN (0,1)
         ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
";
}
else     # Check PM Late
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
                b.LunchBackTime,
                IF(b.LunchBackStation IS NULL, '-', b.LunchBackStation),
                b.PMStatus,
                c.Reason,
                c.RecordStatus,
                d.Remark
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_ABSENT."'
                   LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (b.UserID = d.StudentID AND d.RecordDate='$TargetDate')
                WHERE b.DayNumber = '$txt_day'
                      AND (b.PMStatus = '".CARD_STATUS_ABSENT."' OR b.PMStatus = '".CARD_STATUS_OUTING."')
                AND a.RecordType = 2 AND a.RecordStatus IN (0,1)
         ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
";

}

$lexport = new libexporttext();
$exportColumn = array("$i_UserName", "$i_ClassName", "$i_UserClassNumber",
                                        "$i_SmartCard_Frontend_Take_Attendance_Status",
                                        "$i_SmartCard_Frontend_Take_Attendance_Waived",
                                        "$i_Attendance_Reason"
                                        );

if ($sys_custom['hku_medical_research']) {
        $exportColumn[] = "$i_MedicalReasonTitle";
        # Retrieve Stored Reason
        # $DayType
        $sql1  = "SELECT a.UserID, b.MedicalReasonType ";
        $sql1 .= "FROM INTRANET_USER as a ";
        $sql1 .= "LEFT OUTER JOIN SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON as b ";
        $sql1 .= "ON b.RecordDate = '$TargetDate' AND b.DayType = '$DayType' AND a.UserID = b.StudentID";

        $temp = $lc->returnArray($sql1,2);
        $data_medical = build_assoc_array($temp);
}

$result = $lc->returnArray($sql,11);

for($i=0; $i<sizeOf($result); $i++)
{
        list($record_id, $user_id, $name, $class_name, $class_number, $in_school_time, $in_school_station, $old_status, $reason,$record_status, $remark) = $result[$i];
        $str_status = ($old_status==CARD_STATUS_ABSENT?$i_StudentAttendance_Status_Absent:$i_StudentAttendance_Status_Outing);
        $str_reason = ($reason==""? $remark:$reason);
        $waived_option = $record_status == 1? $i_general_yes:$i_general_no;

        $result[$i] = array($name, $class_name, $class_number, $str_status, $waived_option, $str_reason);

        if ($sys_custom['hku_medical_research']) {
                for ($j=0; $j<sizeof($i_MedicalReasonName); $j++) {
                        list($t_reasonType, $t_reasonName) = $i_MedicalReasonName[$j];
                        $t_selected_value = $data_medical[$user_id];
                        if ($t_selected_value==$t_reasonType) {
                                $medical_reason = $t_reasonName;
                                break;
                        }
                }
                $result[$i][] = $medical_reason;
        }
}

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);

$export_content_final = $i_SmartCard_DailyOperation_ViewAbsenceStatus;
$export_content_final .= " $TargetDate ($display_period)\n";

if (sizeof($result)==0)
        $export_content_final .= "$i_StudentAttendance_NoAbsentStudents\n";
else
        $export_content_final .= $export_content;

intranet_closedb();

$filename = "showabsence-".time().".csv";
$lexport->EXPORT_FILE($filename, $export_content_final);
?>
