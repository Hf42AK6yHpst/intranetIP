<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");

intranet_opendb();

$return_url = $return_url==""?"index.php":$return_url;

if(sizeof($studentID)<=0 || sizeof($target_date)<=0 || ($dateTypeAM!=1 && $dateTypePM!=1) )
	header("Location: $return_url");

$studentID = array_unique($studentID);
$target_date = array_unique($target_date);

//$reason = intranet_htmlspecialchars($reason);
//$remark = intranet_htmlspecialchars($remark);

$values="";
$delim="";
for($i=0;$i<sizeof($studentID);$i++){
	$sid =$studentID[$i];
	for($j=0;$j<sizeof($target_date);$j++){
		$td = $target_date[$j];
		if($dateTypeAM==1){
			$values.= $delim."($sid,'$td',2,'$reason','$remark',NOW(),NOW())";
			$delim=",";
		}
		if($dateTypePM==1){
			$values.= $delim."($sid,'$td',3,'$reason','$remark',NOW(),NOW())";
			$delim=",";
		}
	}
}

$sql=" INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Remark,DateInput,DateModified) VALUES $values";

$li = new libcardstudentattend2();
$li->db_db_query($sql);	
intranet_closedb();
//header("Location: $return_urlshow.php?period=$period&TargetDate=$TargetDate&msg=2");

$t = explode("?",$return_url);
$filename = $t[0];
$str = $t[1];
//$str=$HTTP_SERVER_VARS['QUERY_STRING'];
$ary = explode("&",$str);
$found = false;
for($i=0;$i<sizeof($ary);$i++){
	if(strpos($ary[$i],"msg=")!==false){
			$ary[$i]="msg=1";
			$found = true;
			break;
	}
}
if(!$found){
	array_push($ary,"msg=1");
}
$return_url = $filename."?".implode("&",$ary);	
//echo $return_url;
header("Location: $return_url");
?>
