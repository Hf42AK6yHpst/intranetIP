<?php

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();
$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$normal_time_array = $lc->getTimeArray(0,0);
list($normal_am, $normal_lunch, $normal_pm, $normal_leave,$non_school_day) = $normal_time_array;
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
//$displayColSpanValue = $hasAM + $hasLunch + $hasPM +1;
//echo "$hasAM,$hasLunch,$hasPM";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$filter = $filter==""?2:$filter;
$today = date('Y-m-d');

switch($filter){
	case 1: $cond_filter = " AND a.RecordDate <='$today' "; break;
	case 2: $cond_filter = " AND a.RecordDate >'$today' "; break;
	case 3: $cond_filter = ""; break;
	default: $cond_filter =" AND a.RecordDate > '$today' ";
}

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

$lclass = new libclass();
$db_engine = new libdb();    
$select_class=$lclass->getSelectClass("name='class' onChange='changeClass()'",$class,0);

if($class!=""){
	$namefield = getNameFieldWithClassNumberByLang("");
	$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND ClassName='$class' ORDER BY ClassName,ClassNumber";
	$temp = $lclass->returnArray($sql,2);
	$select_student=getSelectByArray($temp,"name='studentID'",$studentID,0,0);
}

if($studentID!="")
{

//	echo "studentID [".$studentID."]<br>";
	$namefield = getNameFieldByLang();
	$sql = "SELECT UserID, $namefield, RecordType, ClassName, ClassNumber, UserLogin FROM INTRANET_USER WHERE UserID = '$studentID'";
//	echo "sql [".$sql."]<br><br><br>";
	$temp = $db_engine->returnArray($sql,6);
	list($targetUserID , $targetName, $targetType, $targetClass, $targetClassNumber, $targetUserLogin) = $temp[0];




	#GET USER BELONG TO WHICH GROUP, GROUP IS CLASS TYPE (3)
//	$groupIDString = getUserGroupID($targetUserID);
	$aryGroupIDList = getUserGroupIDArray($targetUserID);

	/*for($i = 0; $i< sizeof($aryGroupIDList); $i++)
	{
		list($aa, $bb, $cc,$dd) = $aryGroupIDList[$i];
		echo "xxxx >$aa, $bb, $cc ,$dd<br>";
	}*/

	     $null_record_variable = "NULL";

     # Get ClassID
     $sql = "SELECT ClassID FROM INTRANET_CLASS WHERE ClassName = '$targetClass'";
     $temp = $db_engine->returnVector($sql);
     $targetClassID = $temp[0];

     # Check need to take attendance or not
     $sql = "SELECT Mode FROM CARD_STUDENT_CLASS_SPECIFIC_MODE WHERE ClassID = '$targetClassID'";
     $temp = $db_engine->returnVector($sql);
     $mode = $temp[0];
	
     # Get Record
     $current_time = time();
     if ($target_date != "")
     {
         $current_time = strtotime($target_date);
     }
	 //echo "current_time [".$current_time."] date [".date('Y-m-d')."] time [".time()."]<br>";
     $month = date('m',$current_time);
     $year = date('Y',$current_time);
     $day = date('d',$current_time);
     $today = date('Y-m-d',$current_time);
     $time_string = date('H:i:s',$current_time);
     $ts_now = $current_time - strtotime($today);
//	 echo "today(".$today.")<br>";

     $time_table = trim(get_file_content("$intranet_root/file/time_table_mode.txt"));
     $time_table_mode = $time_table;         ### 0 - Time Slot, 1 - Time Session

//	 echo "time_table_mode [".$time_table_mode."]<br>";
	 
	 //**start
	$groupID_str = "";
	$groupID_TimeTable = array();
	$groupID_FindSpecificTimeTable = array();
	$compareTimeTable = array();

	# Get Time Boundaries
	# Get Cycle Day
    $sql = "SELECT TextShort FROM INTRANET_CYCLE_DAYS WHERE RecordDate = '$today'";
    $temp = $db_engine->returnVector($sql);
    $cycleDay = $temp[0];

	if (($time_table_mode == 0)||($time_table_mode == ''))
         {

             # Change the name display
             $targetName = "$targetName ($targetClass-$targetClassNumber)";



             # Get Week Day
             $weekDay = date('w');

             # Get Class-specific time boundaries

             # Get Special Day Settings
             $sql = "SELECT RecordID, TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart),
                            TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
                            NonSchoolDay
                            FROM CARD_STUDENT_SPECIFIC_DATE_TIME
                            WHERE RecordDate = '$today'
                                  AND ClassID = '$targetClassID'
                            ";
             $temp = $db_engine->returnArray($sql,6);
             list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];

             if (sizeof($temp)==0 || $ts_recordID == "")
             {
                 if ($cycleDay != ""){ $conds = " OR (a.DayType = 2 AND a.DayValue = '$cycleDay')";}
                 $sql = "SELECT a.RecordID, TIME_TO_SEC(a.MorningTime), TIME_TO_SEC(a.LunchStart),
                                TIME_TO_SEC(a.LunchEnd), TIME_TO_SEC(a.LeaveSchoolTime),                               a.NonSchoolDay
                                FROM CARD_STUDENT_CLASS_PERIOD_TIME as a
                                     LEFT OUTER JOIN INTRANET_CLASS as b ON a.ClassID = b.ClassID
                                WHERE b.ClassName = '$targetClass' AND
                                  (
                                    (a.DayType = 1 AND a.DayValue = '$weekDay') $conds  OR
                                    (a.DayType = 0 AND a.DayValue = 0)
                                  ) ORDER BY a.DayType DESC
                                  ";
                 $temp = $db_engine->returnArray($sql,6);
                 list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
                 if (sizeof($temp)==0 || $ts_recordID == "")
                 {
                     # Get Special Day based on School
                     $sql = "SELECT RecordID, TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart),
                                    TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
                                    NonSchoolDay
                             FROM CARD_STUDENT_SPECIFIC_DATE_TIME
                             WHERE RecordDate = '$today'
                                   AND ClassID = 0
                                   ";
	                     $temp = $db_engine->returnArray($sql,6);
                     list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];

                     if (sizeof($temp)==0 || $ts_recordID == "")
                     {
                         # Get School Settings
                         $sql = "SELECT a.SlotID, TIME_TO_SEC(a.MorningTime), TIME_TO_SEC(a.LunchStart),
                                        TIME_TO_SEC(a.LunchEnd), TIME_TO_SEC(a.LeaveSchoolTime),
                                        a.NonSchoolDay
                                 FROM CARD_STUDENT_PERIOD_TIME as a
                                      WHERE (a.DayType = 1 AND a.DayValue = '$weekDay') $conds OR (a.DayType = 0 AND a.DayValue = 0)
                                      ORDER BY a.DayType DESC
                                 ";
                         $temp = $db_engine->returnArray($sql,6);
//echo "<b>Time Slot Whole School CYCLE, WEEKDAY , NORMAL SQL</b> [".$sql."]<br><br>";
                         list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
                     }
                 }
             }
		} //close 	if (($time_table_mode == 0)||($time_table_mode == ''))
		if($time_table_mode == 1)
		{
			# Get Class-specific time boundaries

             # Get Special Day Settings
             $sql = "SELECT
                                             b.RecordID, TIME_TO_SEC(a.MorningTime), TIME_TO_SEC(a.LunchStart),
                                             TIME_TO_SEC(a.LunchEnd), TIME_TO_SEC(a.LeaveSchoolTime),
                                             a.NonSchoolDay
                              FROM
                                              CARD_STUDENT_TIME_SESSION AS a INNER JOIN
                                              CARD_STUDENT_TIME_SESSION_DATE AS b ON (a.SessionID = b.SessionID)
                              WHERE
                                              RecordDate = '$today' AND ClassID = '$targetClassID'";

             $temp = $db_engine->returnArray($sql,6);
             list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];


             if (sizeof($temp)==0 || $ts_recordID == "")
             {
                 if ($cycleDay != "")
                 {
                     $conds = " OR (a.DayType = 2 AND a.DayValue = '$cycleDay')";
                 }

                 $sql = "SELECT
                                                 a.RecordID, TIME_TO_SEC(b.MorningTime), TIME_TO_SEC(b.LunchStart),
                                                 TIME_TO_SEC(b.LunchEnd), TIME_TO_SEC(b.LeaveSchoolTime),
                                                 b.NonSchoolDay
                                  FROM
                                                  CARD_STUDENT_TIME_SESSION_REGULAR AS a LEFT OUTER JOIN
                                                  CARD_STUDENT_TIME_SESSION AS b ON (a.SessionID = b.SessionID)
                                         WHERE
                                                         a.ClassID = '$targetClassID' AND
                                  (
                                    (a.DayType = 1 AND a.DayValue = '$weekDay') $conds
                                      OR
                                    (a.DayType = 0 AND a.DayValue = 0)
                                  )
                                  ORDER BY a.DayType DESC";
                 $temp = $db_engine->returnArray($sql,6);
                 list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
                 if (sizeof($temp)==0 || $ts_recordID == "")
                 {
                     # Get Special Day based on School
                                 $sql = "SELECT
                                                                 a.RecordID, TIME_TO_SEC(b.MorningTime), TIME_TO_SEC(b.LunchStart),
                                                                 TIME_TO_SEC(b.LunchEnd), TIME_TO_SEC(b.LeaveSchoolTime),
                                                                 b.NonSchoolDay
                                                  FROM
                                                                  CARD_STUDENT_TIME_SESSION_DATE AS a INNER JOIN
                                                                  CARD_STUDENT_TIME_SESSION AS b ON (a.SessionID = b.SessionID)
                                                  WHERE
                                                                  a.RecordDate = '$today' AND a.ClassID = 0";

                     $temp = $db_engine->returnArray($sql,6);
                     list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];

                     if (sizeof($temp)==0 || $ts_recordID == "")
                     {
                         # Get School Settings
                         $sql = "SELECT
                                                                 a.RecordID, TIME_TO_SEC(b.MorningTime), TIME_TO_SEC(b.LunchStart),
                                                TIME_TO_SEC(b.LunchEnd), TIME_TO_SEC(b.LeaveSchoolTime),
                                                b.NonSchoolDay
                                 FROM
                                                         CARD_STUDENT_TIME_SESSION_REGULAR AS a INNER JOIN
                                                         CARD_STUDENT_TIME_SESSION as b ON (a.SessionID = b.SessionID AND a.ClassID = 0)
                                 WHERE
                                                         (a.DayType = 1 AND a.DayValue = '$weekDay') $conds OR (a.DayType = 0 AND a.DayValue = 0)
                                 ORDER BY
                                                         a.DayType DESC";

                         $temp = $db_engine->returnArray($sql,6);
                         list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
                     }
                 }
             }
		}

		$tempArray = array($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay);
		$classSchoolSlotTimeTable = array($tempArray);

		if(sizeof($aryGroupIDList) != 0)
		{
			for($i=0; $i< sizeof($aryGroupIDList); $i++)
			{
				list($groupId, $title, $desc,$mode) = $aryGroupIDList[$i];
				$groupID_str .= $groupId.",";

				#MODE = NULL ==> SCHOOL TIME TABLE
				#MDOE = 0    ==> SCHOOL TIME TABLE (SAME AS NULL)
				#MODE = 1    ==> GROUP SPECIFIC TIME TABLE
				#MODE = 2    ==> NO NEED TO TAKE ATTENDANCE
				if($mode == 1)
				{
					array_push($groupID_FindSpecificTimeTable,$aryGroupIDList[$i]);
//echo "push groupid [".$groupId."]<br>";
				}
				else
				{
					array_push($groupID_TimeTable,$aryGroupIDList[$i]);
				}
			}
			$groupID_str = substr($groupID_str, 0, -1);
		}
		
 	
		if(sizeof($groupID_FindSpecificTimeTable) > 0)
		{
			if($groupID_str != "")
			{
				//GET GROUP SPECIAL TIME TABLE

				//$sql = getGroupSlotSpecialSql ($groupID_str);
				$sql = ($time_table_mode == 1) ? getGroupSessionSpecialSql($groupID_str) : getGroupSlotSpecialSql ($groupID_str);
//				echo "<font color =red>sql [".$sql."]</font><br>";
				$groupSpecialResultSet = $db_engine->returnArray($sql,7);
//				$sql = getGroupSlotCycleWeekNormalSql($groupID_str);
				$sql = ($time_table_mode == 1) ? getGroupSessionCycleWeekNormalSql($groupID_str) : getGroupSlotCycleWeekNormalSql($groupID_str);
//				echo "getGroupCycleWeekNormalSql sql  [".$sql."]<br>";
				$groupCycleWeekNormalResultSet = $db_engine->returnArray($sql,8);
			}

			#CHECK SPECIFIC TIME TABLE FOR GROUP MODE == 1
			for($i=0; $i< sizeof($groupID_FindSpecificTimeTable); $i++)
			{
				$groupTimeTableFind = false;
				list($groupId, $title, $desc,$mode) = $groupID_FindSpecificTimeTable[$i];
//				echo "groupid ==> $groupId, $title, $desc,$mode<br>";
				for($y = 0; $y<sizeof($groupSpecialResultSet); $y++)
				{
					//CEHCK SPECIAL TIME TABLE
					list($s_groupid, $s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay) = $groupSpecialResultSet[$y];
					if($groupId == $s_groupid)
					{
						$aryTemp = array($s_groupid, $title, $desc,$mode,$s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay);
						array_push($groupID_TimeTable, $aryTemp);
						array_push($compareTimeTable, array($s_RecordID, timeToSec($s_MorningTime), timeToSec($s_LunchStart),timeToSec($s_LunchEnd), timeToSec($s_LeaveSchoolTime),$s_NonSchoolDay));
						$groupTimeTableFind = true;
						break;
					}
				}
				if($groupTimeTableFind == false)
				{	//GROUP NOT FIND IN THE SPECIAL TIME TABLE
					//FIND IN THE CYCLE, WEEKDAY, NORMAL TIME TABLE
					for($z = 0; $z< sizeof($groupCycleWeekNormalResultSet);$z++)
					{
						list($s_groupid, $s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay) = $groupCycleWeekNormalResultSet[$z];
						if($groupId == $s_groupid)
						{
							$aryTemp = array($s_groupid,  $title, $desc,$mode,$s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay);
							array_push($groupID_TimeTable, $aryTemp);
							array_push($compareTimeTable, array($s_RecordID, timeToSec($s_MorningTime), timeToSec($s_LunchStart),timeToSec($s_LunchEnd), timeToSec($s_LeaveSchoolTime),$s_NonSchoolDay));
							$groupTimeTableFind = true;
							break;
						}
					}
					//GROUP NOT FIND IN CYCLE, WEEKDAY , NORMAL TIME TABLE, SET TO SCHOOL TIME TABLE (MODE = NULL)
					if($groupTimeTableFind == false)
					{
						$aryTemp = array($groupId, $title, $desc,"");
						array_push($groupID_TimeTable, $aryTemp);
					}
				}
			}		
		}

		
		$finalTimeTable = mergeTimeTable($classSchoolSlotTimeTable,$compareTimeTable);

		list($s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay) = $classSchoolSlotTimeTable[0];

		$classDisplay = "<table width=100% border=\"1\" bordercolordark=\"#DBD6C4\" bordercolorlight=\"#FCF7E5\" cellpadding=\"2\" cellspacing=\"0\" align=\"center\">\n";
		$classDisplay .= "<tr class=\"tableTitle\">";		
		if($hasAM == 1)		{	$classDisplay .= "<td align = \"center\">$i_StudentAttendance_SetTime_AMStart</td>";}
		if($hasLunch == 1 )	{	$classDisplay .= "<td align = \"center\">$i_StudentAttendance_SetTime_LunchStart</td>";}
		if($hasPM == 1)		{	$classDisplay .= "<td align = \"center\">$i_StudentAttendance_SetTime_PMStart</td>";}
		$classDisplay .= "<td align = \"center\">$i_StudentAttendance_SetTime_SchoolEnd</td>";
		$classDisplay .= "</tr>\n";
		$classDisplay .= "<tr>";
		if($s_NonSchoolDay == 1)
		{	
			$classDisplay .= "<td colspan=\"4\" align = \"center\">".$i_StudentAttendance_NonSchoolDay."</td>";
		}
		else
		{

			if($hasAM == 1)		{	$classDisplay .= "<td align = \"center\">".secToTime($s_MorningTime)."</td>";}
			if($hasLunch == 1 )	{	$classDisplay .= "<td align = \"center\">".secToTime($s_LunchStart)."</td>";}
			if($hasPM == 1)		{	$classDisplay .= "<td align = \"center\">".secToTime($s_LunchEnd)."</td>";}			
			$classDisplay .= "<td align = \"center\">".secToTime($s_LeaveSchoolTime)."</td>";
		}
		$classDisplay .= "</tr>";
		$classDisplay .= "</table>";
	

		$groupDisplay = "";
		$groupDisplay = "<table width=100% border=\"1\" bordercolordark=\"#DBD6C4\" bordercolorlight=\"#FCF7E5\" cellpadding=\"2\" cellspacing=\"0\" align=\"center\">\n";
		$groupDisplay .= "<tr class=\"tableTitle\">";
		$groupDisplay .= "<td align=\"center\">#</td>";
		$groupDisplay .= "<td align = \"center\">$i_GroupTitle</td>";
		$groupDisplay .= "<td align = \"center\">$i_GroupDescription</td>";
		if($hasAM == 1)		{	$groupDisplay .= "<td align = \"center\">$i_StudentAttendance_SetTime_AMStart</td>";}
		if($hasLunch == 1 )	{	$groupDisplay .= "<td align = \"center\">$i_StudentAttendance_SetTime_LunchStart</td>";}
		if($hasPM == 1)		{	$groupDisplay .= "<td align = \"center\">$i_StudentAttendance_SetTime_PMStart</td>";}
		$groupDisplay .= "<td align = \"center\">$i_StudentAttendance_SetTime_SchoolEnd</td>";
		$groupDisplay .= "</tr>\n";
		$num = 0;
		for($i = 0; $i < sizeof($groupID_TimeTable); $i++)
		{
			$num = ++$num;
			list($s_groupid,  $title, $desc,$mode,$s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay) = $groupID_TimeTable[$i];
//			echo "====> [$s_groupid,  $title, $desc,$mode,$s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay]<br>";
			$groupDisplay .= "<tr>";	
			$groupDisplay .= "<td>".$num."&nbsp</td>";
			$groupDisplay .= "<td>".$title."&nbsp</td>";
			$groupDisplay .= "<td>".$desc."&nbsp</td>";
			switch($mode){
				case 1:
						if($s_NonSchoolDay == 1)
						{	
							$groupDisplay .= "<td colspan=\"4\" align = \"center\">".$i_StudentAttendance_NonSchoolDay."</td>";
						}
						else
						{
							if($hasAM == 1)		{$groupDisplay .= "<td align = \"center\">".$s_MorningTime."</td>";}
							if($hasLunch == 1 )	{$groupDisplay .= "<td align = \"center\">".$s_LunchStart."</td>";}
							if($hasPM == 1)		{$groupDisplay .= "<td align = \"center\">".$s_LunchEnd."</td>";}
							$groupDisplay .= "<td align = \"center\">".$s_LeaveSchoolTime."</td>";
						}
					break;
				case 2:
						$groupDisplay .= "<td colspan=\"4\" align = \"center\">$i_StudentAttendance_GroupMode_NoNeedToTakeAttendance</td>";
					break;
				default:
						$groupDisplay .= "<td colspan=\"4\" align = \"center\">$i_StudentAttendance_ClassMode_UseSchoolTimetable</td>";
			}		
			$groupDisplay .= "</tr>";
		}
		$groupDisplay .= "</table>\n";
		
				$finalDisplay = "<table width=100% border=\"1\" bordercolordark=\"#DBD6C4\" bordercolorlight=\"#FCF7E5\" cellpadding=\"2\" cellspacing=\"0\" align=\"center\">\n";
		$finalDisplay .= "<tr class=\"tableTitle\">";		
		if($hasAM == 1)		{	$finalDisplay .= "<td align = \"center\">$i_StudentAttendance_SetTime_AMStart</td>";}
		if($hasLunch == 1 )	{	$finalDisplay .= "<td align = \"center\">$i_StudentAttendance_SetTime_LunchStart</td>";}
		if($hasPM == 1)		{	$finalDisplay .= "<td align = \"center\">$i_StudentAttendance_SetTime_PMStart</td>";}			

		$finalDisplay .= "<td align = \"center\">$i_StudentAttendance_SetTime_SchoolEnd</td>";
		$finalDisplay .= "</tr>\n";
		$finalDisplay .= "<tr>";
		list($s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay) = $finalTimeTable[0];
		if($s_NonSchoolDay == 1)
		{	
			$finalDisplay .=  "<td colspan=\"4\">".$i_StudentAttendance_NonSchoolDay."</td>";
		}
		else
		{
			if($hasAM == 1)		{	$finalDisplay .= "<td align = \"center\">".secToTime($s_MorningTime)."</td>";}
			if($hasLunch == 1 )	{	$finalDisplay .= "<td align = \"center\">".secToTime($s_LunchStart)."</td>";}
			if($hasPM == 1)		{	$finalDisplay .= "<td align = \"center\">".secToTime($s_LunchEnd)."</td>";}			
			
			$finalDisplay .= "<td align = \"center\">".secToTime($s_LeaveSchoolTime)."</td>";
		}
		$finalDisplay .=  "</tr>";		
		$finalDisplay .=  "</table>";		

////end new
         //}//close if (($time_table_mode == 0)||($time_table_mode == ''))
	 //** end

}

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_Check_Time,'index.php',$i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<script language='javascript'>
<!--
function changeClass(){
	submitForm();
}
function submitForm(){
	obj = document.form1;
	if(obj!=null) obj.submit();
}
function openPrintPage()
{
        newWindow("browse_by_student_print.php?class=<?=$class?>&studentID=<?=$studentID?>&filter=<?=$filter?>&keyword=<?=$keyword?>&order=<?=$order?>&field=<?=$field?>",4);
}
function exportPage(newurl){
	old_url = document.form1.action;
	document.form1.action = newurl;
	document.form1.submit();
	document.form1.action = old_url;
}
-->
</script>
<form name="form1" method="GET">
<table width=560 border=0 cellpadding=3 cellspacing=0>
<tr><td nowrap align=right width=20%><?php echo $i_UserClassName; ?>: </td><td><?=$select_class?></td></tr>
<?php if($select_student!=""){?>
	<Tr><Td align=right><?=$i_UserStudentName?>: </td><td><?=$select_student?></td></tr>
	<tr><td align="right"><?=$i_StudentAttendance_Field_Date?>: </td>
	<td>
<?

	$sql="SELECT DISTINCT DATE_FORMAT(RecordDate,'%Y%m%d') FROM CARD_STUDENT_PRESET_LEAVE";
	$dates = $db_engine->returnVector($sql);

?>	
 <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
 <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_card_no_data";
         css_array[1] = "dynCalendar_card_not_confirmed";
         css_array[2] = "dynCalendar_card_confirmed";
         var date_array = new Array;
         <?
         for ($i=0; $i<sizeof($dates); $i++)
         {
	         $date_string = $dates[$i];
	         ?>
              date_array[<?=$date_string?>] = 1;
             <?
         }
         ?>
 </script>
 <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
 <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
 <script type="text/javascript">
 <!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].target_date.value = dateValue;
          }
 // -->
 </script>
<?
	$target_date = ($target_date =="")?date('Y-m-d'):$target_date;
?>
	<input type=text name=target_date value='<?=$target_date?>' size=10><script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
         startCal.setLegend('<?=$legend?>');
         startCal.differentDisplay = true;
    //-->
    </script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
	</td></tr>
	<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" ></td></tr>
<?php } ?>
</table>
<?php if($studentID!=""){
	$toolbar  = "<a class=iconLink href=javascript:checkGet(document.form1,'new.php')>".newIcon()."$button_new</a>\n".toolBarSpacer();
	$toolbar .= "<a class=iconLink href=javascript:exportPage('browse_by_student_export.php')>".exportIcon()."$button_export</a>\n".toolBarSpacer();
	$toolbar2 = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";

	
	$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'RecordID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
	$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'RecordID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";

	$filterbar ="<SELECT name=filter onChange=submitForm()>";
	$filterbar.="<OPTION value='1'".($filter==1?" SELECTED ":"").">$i_StudentAttendance_Reminder_Status_Past</OPTION>";
	$filterbar.="<OPTION value='2'".($filter==2?" SELECTED ":"").">$i_StudentAttendance_Reminder_Status_Coming</OPTION>";
	$filterbar.="<OPTION value='3'".($filter==3?" SELECTED ":"").">$i_status_all</OPTION>";
	$filterbar.="</SELECT>";

	$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
	$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
	
	$searchbar = $filterbar."&nbsp;".$searchbar;

?>
	<table width=560 border="0" cellpadding=0 cellspacing=0 align="center">
	<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
	</table>
	

	<?php 	
	echo $i_SmartCard_DailyOperation_Check_Time_School_Time_Table."";
	echo $classDisplay;
	echo "<br><Br>";
	?>
	<?php 	
	echo $i_SmartCard_DailyOperation_Check_Time_Group_Time_Table."";
	echo $groupDisplay;
		echo "<br><Br>";
	?>
	
	<?php 	
	echo $i_SmartCard_DailyOperation_Check_Time_Final_Time_Table."";
echo 		$finalDisplay;
	?>
<?php } ?>
<br><br>
</form>

<?
intranet_closedb();
include_once("../../../../templates/adminfooter.php");
?>
<?
function getUserGroupIDArray($userID)
{
	global $db_engine;

	#GET USER BELONGS TO WHICH GROUP, GROUP IS CLASS TYPE (3)
	$groupIDString = "";
	$sql = "select c.GroupID, c.Title,c.Description, e.mode
		from 
			INTRANET_USER a, INTRANET_USERGROUP b, INTRANET_GROUP c 
			left outer join INTRANET_CLASS d on d.groupid = c.groupid 
			left outer join CARD_STUDENT_ATTENDANCE_GROUP e on e.groupid = c.groupid 
		where
			a.userid = b.userid		and 
			b.groupid = c.groupid   and 
			a.userid = '$userID' and 
			c.recordtype = 3 and 
			d.groupid is null
		order by c.Title
		";
		$result = $db_engine->returnArray($sql,4);
//echo "sql [".$sql."] size of result [".sizeof($result)."]<br>";
	return $result;
}

function getUserGroupID($userID)
{
	global $db_engine;
	#GET USER BELONGS TO WHICH GROUP, GROUP IS CLASS TYPE (3)
	$groupIDString = "";
	$sql = "select a.UserID, a.CardID, a.EnglishName, a.ChineseName , c.GroupID, c.Title,c.Description, c.recordtype 
		from 
			INTRANET_USER a, INTRANET_USERGROUP b, INTRANET_GROUP c 
			left outer join INTRANET_CLASS d on d.groupid = c.groupid 
		where
			a.userid = b.userid		and 
			b.groupid = c.groupid   and 
			a.userid = '$userID' and 
			c.recordtype = 3 and 
			d.groupid is null";
		$result = $db_engine->returnArray($sql,8);

	for($i=0; $i<sizeOf($result); $i++)
	{
		list($aa, $bb, $cc, $dd, $groupid, $ff,$gg,$hh) = $result[$i];
		$groupIDString.= $groupid.",";
	}
	$groupIDString = substr($groupIDString, 0, -1);
	return $groupIDString;
}
function checkTimeResult($ts_recordID,$ts_morningTime,$ts_lunchStart,$ts_lunchEnd,$ts_leaveSchool,$ts_nonSchoolDay)
{
	global $db_engine;

	$sql = "select sec_to_time(".$ts_morningTime."), sec_to_time(".$ts_lunchStart."), sec_to_time(".$ts_lunchEnd."), sec_to_time(".$ts_leaveSchool.")";
    $aaaa = $db_engine->returnArray($sql,4);
	list($bb, $cc, $dd, $ee) = $aaaa[0];
	echo "<br>ts_recordID---><b><font color = \"red\">".$ts_recordID."</font></b>";
	echo "<br>ts_morningTime--->".$ts_morningTime." <b><font color = \"red\">[".$bb."]</font></b>";
	echo "<br>ts_lunchStart--->".$ts_lunchStart."<b><font color = \"red\"> [".$cc."]</font></b>";
	echo "<br>ts_lunchEnd--->".$ts_lunchEnd." <b><font color = \"red\">[".$dd."]</font></b>";
	echo "<br>ts_leaveSchool--->".$ts_leaveSchool." <b><font color = \"red\">[".$ee."]</font></b>";
	echo "<br>ts_nonSchoolDay---><b><font color = \"red\">".$ts_nonSchoolDay."</font></b><br><br><br>";

}


#SHOULD RECEIVE CLASS/SCHOOL TIME TABLE FIRST, THEN IS GROUP TIME TABLE
function mergeTimeTable($classTimeTableArray, $groupTimeTableArray)
{
/*
	for($i=0; $i<sizeof($classTimeTableArray); $i++)
    {
		list($aa, $bb, $cc, $dd, $ee, $ff,$gg) = $classTimeTableArray[$i];
		echo "array 1[".$aa."] [".$bb."] [".$cc."][".$dd."][".$ee."][".$ff."][".$gg."]<br>";               
    }	
	for($i=0; $i<sizeof($groupTimeTableArray); $i++)
    {
		list($aa, $bb, $cc, $dd, $ee, $ff,$gg) = $groupTimeTableArray[$i];
		echo "array 2[".$aa."] [".$bb."] [".$gg."]<br>";               
    }
*/
	$tempArray = array_merge ($classTimeTableArray, $groupTimeTableArray);


	if(sizeof($tempArray) != 0)
	{
		#$tempArray[0] MUST BE THE INFORMATION ABOUT CLASS/ SCHOOL TIME SETTING
		list($prev_RecordID, $prev_MorningTime, $prev_LunchStart, $prev_LunchEnd, $prev_LeaveSchoolTime, $prev_NonSchoolDay,$prev_DayType) = $tempArray[0];
	}

	for($i=0; $i<sizeof($tempArray); $i++)
    {
		list($_RecordID, $_MorningTime, $_LunchStart, $_LunchEnd, $_LeaveSchoolTime, $_NonSchoolDay,$_DayType) = $tempArray[$i];
			if($_DayType > $prev_DayType )
			{
				$prev_RecordID			= $_RecordID;			$prev_MorningTime		= $_MorningTime;
				$prev_LunchStart		= $_LunchStart;			$prev_LunchEnd			= $_LunchEnd;
				$prev_LeaveSchoolTime	= $_LeaveSchoolTime;	$prev_DayType			= $_DayType;
				$prev_NonSchoolDay		= $_NonSchoolDay;
			}
			else if ($_DayType == $prev_DayType)
			{
				#IF EQUAL DAY TYPE, NON SCHOOL DAY RECORD WILL NOT COUNT
				if($_NonSchoolDay != 1)
				{
					#EQUAL DAY TYPE , IF PREVIOUS RECORD IS NONSCHOOL DAY, SHOULD RESET ALL THE RECORD TO THE CURRENT RECORD
					if($prev_NonSchoolDay == 1)
					{
						$prev_RecordID			= $_RecordID;			$prev_MorningTime		= $_MorningTime;
						$prev_LunchStart		= $_LunchStart;			$prev_LunchEnd			= $_LunchEnd;
						$prev_LeaveSchoolTime	= $_LeaveSchoolTime;	$prev_DayType			= $_DayType;
						$prev_NonSchoolDay		= $_NonSchoolDay;
					}
					else
					{
						#EQUAL DAY TYPE, PREVIOUS IS SCHOOL DAY (NOT NON SCHOOL DAY), COMPARE THE TIME
						#_RecordID			TAKE ANY MIN/MAX		#_MorningTime		TAKE MIN
						#_LunchStart		TAKE MAX				#_LunchEnd			TAKE MIN
						#_LeaveSchoolTime	TAKE MAX				#_NonSchoolDay		TAKE MIN (0,1) ==> 1 IS NON SCHOOL DAY

						$prev_RecordID			= ($_RecordID		 > $prev_RecordID)		 ? $_RecordID			:$prev_RecordID;
						$prev_MorningTime		= ($_MorningTime	 > $prev_MorningTime)	 ? $prev_MorningTime	:$_MorningTime;
						$prev_LunchStart		= ($_LunchStart		 > $prev_LunchStart)	 ? $_LunchStart			:$prev_LunchStart;
						$prev_LunchEnd			= ($_LunchEnd		 > $prev_LunchEnd)		 ? $prev_LunchEnd		:$_LunchEnd;
						$prev_LeaveSchoolTime	= ($_LeaveSchoolTime > $prev_LeaveSchoolTime)? $_LeaveSchoolTime    :$prev_LeaveSchoolTime;
				//		$prev_NonSchoolDay	    = ($_NonSchoolDay    > $prev_NonSchoolDay)   ? $_NonSchoolDay	:$prev_NonSchoolDay;
				//		$prev_DayType			= $_DayType;
					}
				}
			}
			//else ($_DayType < $prev_DayType){ //skip}
		


	}

	#PRESERVE THE DATA STURTURE IN ARRAY OF ARRAY
	$arrayElement = array($prev_RecordID,$prev_MorningTime,$prev_LunchStart,$prev_LunchEnd,$prev_LeaveSchoolTime,$prev_NonSchoolDay); 

//	echo "return $prev_RecordID,$prev_MorningTime,$prev_LunchStart,$prev_LunchEnd,$prev_LeaveSchoolTime,$prev_NonSchoolDay<br>";
//checkTimeResult($prev_RecordID,$prev_MorningTime,$prev_LunchStart,$prev_LunchEnd,$prev_LeaveSchoolTime,$prev_NonSchoolDay);

	$returnArray = array($arrayElement);

	return $returnArray;
}
function getGroupSlotSpecialSql ($groupID_str)
{
	global $today;
	$sql =	"SELECT 
				groupid, RecordID, MorningTime, LunchStart,
				LunchEnd, LeaveSchoolTime,NonSchoolDay
			 FROM 
				CARD_STUDENT_SPECIFIC_DATE_TIME_GROUP
			 WHERE 
				RecordDate = '$today' AND 
				GroupId in ($groupID_str)
			 ORDER BY groupid
			";
	return $sql;
}

function getGroupSessionSpecialSql ($groupID_str)
{
	global $today;

	$sql = "SELECT 
				groupid, RecordID, MorningTime, LunchStart,
				LunchEnd, LeaveSchoolTime,NonSchoolDay
			FROM
				CARD_STUDENT_TIME_SESSION AS a INNER JOIN
				CARD_STUDENT_TIME_SESSION_DATE_GROUP AS b ON (a.SessionID = b.SessionID)
			WHERE 
				RecordDate = '$today'
				AND GroupId in ($groupID_str) 
			ORDER BY groupid
			";

	return $sql;
}

function getGroupSlotCycleWeekNormalSql($groupID_str)
{
	global $conds;
	global $weekDay;

	$sql =  "SELECT 
				a.groupid, a.RecordID, a.MorningTime, a.LunchStart,
				a.LunchEnd, a.LeaveSchoolTime,a.NonSchoolDay,a.DayType
			 FROM 
				CARD_STUDENT_GROUP_PERIOD_TIME as a
			 WHERE 
				a.GroupId in ($groupID_str) AND
				(
					(a.DayType = 1 AND a.DayValue = '$weekDay') $conds
					OR
					(a.DayType = 0 AND a.DayValue = 0)
				)
			 ORDER BY a.groupid, a.DayType DESC
			";
	return $sql;
}

function getGroupSessionCycleWeekNormalSql($groupID_str)
{
	global $conds;
	global $weekDay;

	$sql = "SELECT 
				a.groupid, a.RecordID, b.MorningTime, b.LunchStart,
				b.LunchEnd, b.LeaveSchoolTime, b.NonSchoolDay,a.DayType
			FROM 
				CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS a LEFT OUTER JOIN
				CARD_STUDENT_TIME_SESSION as b ON (a.SessionID = b.SessionID)					
			WHERE a.GroupId in ($groupID_str) AND
				(
					(a.DayType = 1 AND a.DayValue = '$weekDay') $conds
					OR
					(a.DayType = 0 AND a.DayValue = 0)
				)
			ORDER BY 
					a.groupid, a.DayType DESC, a.recordId desc
			";
	return $sql;
}

function timeToSec($_time)
{
	$timeAry = split(":",$_time);

	$returnValue = $timeAry[0] * 60 *60 + $timeAry[1] *60;
	return $returnValue;
}

function secToTime($_sec)
{
	$hours = floor ($_sec / 3600);
	$mins = $_sec % 3600;
	$mins = floor ($mins / 60);
	$hours = ($hours <= 9)? "0".$hours:$hours;
	$mins = ($mins <= 9)? "0".$mins:$mins;
	$returnValue = $hours.":".$mins;
	return $returnValue;

}
?>