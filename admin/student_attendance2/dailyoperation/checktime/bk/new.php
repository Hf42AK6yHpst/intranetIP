<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();


$return_url = $return_url==""?$HTTP_SERVER_VARS['HTTP_REFERER']:$return_url;

$lclass = new libclass();
$li = new libcardstudentattend2();
$li->retrieveSettings();

if($studentID=="")
	$studentID = array();
else if(!is_array($studentID))
	$studentID = array($studentID);

if($target_date=="")
	$target_date = array();
else if(!is_array($target_date))
	$target_date = array($target_date);
	
# submitType 1 : add by class name ( class number)
# submitType 2 : add by user login 
if($submitType==1 && $user_id!=""){
	if(!in_array($user_id,$studentID))
		array_push($studentID,$user_id);
}
else if($submitType==2 && $user_login !=""){
	$sql ="SELECT UserID FROM INTRANET_USER WHERE UserLogin='$user_login' AND RecordType=2 AND RecordStatus IN (0,1,2)";
	
	$temp = $lclass->returnVector($sql);
	if($temp[0]!="" && !in_array($temp,$studentID))
		array_push($studentID,$temp[0]);
}

if(sizeof($studentID)>0){
	$selected_student_list = implode(",",$studentID);
	$namefield = getNameFieldWithClassNumberByLang("");
	$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND UserID IN($selected_student_list) ORDER BY ClassName,ClassNumber";
	
	$temp = $lclass->returnArray($sql,2);
}else{
	 $temp = array();
	 $selected_student_list="''";
}

$select_student = getSelectByArray($temp," name='studentID[]' multiple class='select_group'","",0,1);


$select_class=$lclass->getSelectClass("name='class' onChange='changeClass()'",$class,0);

if($class!=""){
	$namefield = getNameFieldWithClassNumberByLang("");
	$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND ClassName='$class' AND UserID NOT IN ($selected_student_list) ORDER BY ClassName,ClassNumber";
	$temp = $lclass->returnArray($sql,2);
	$select_classnum=getSelectByArray($temp,"name='user_id'",$user_id,0,0);
	$btn = "<a href=\"javascript:addStudent(1)\"><img src='$image_path/admin/button/s_btn_add_$intranet_session_language.gif' border=0></a>";
	$select_classnum.="&nbsp$btn";
}

# select day type
if($li->attendance_mode==2 || $li->attendance_mode == 3){
	$select_datetype="<input type=checkbox name=dateTypeAM value='1'".($dateTypeAM==1?" CHECKED":"").">$i_DayTypeAM&nbsp;&nbsp;";
	$select_datetype.="<input type=checkbox name=dateTypePM value='1'".($dateTypePM==1?" CHECKED":"").">$i_DayTypePM";
}else if($li->attendance_mode==1){
	$select_datetype= "<input type=hidden name=dateTypePM value='1'".($dateTypePM==1?" CHECKED":"").">$i_DayTypePM";
}else {
	$select_datetype= "<input type=hidden name=dateTypeAM value='1'".($dateTypeAM==1?" CHECKED":"").">$i_DayTypeAM";
}

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_Preset_Absence,'index.php',$button_new,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<style type="text/css">
/*
.select_group{width:200px; height=150px;}
*/
</style>
<script language='javascript'>
function changeClass(){
	submitForm('');
}
function submitForm(newurl){
	obj = document.form1;
	studentObjs = document.getElementsByName('studentID[]');
	studentObj = studentObjs[0];
	if(obj==null || studentObj==null) return;
	for(i=0;i<studentObj.options.length;i++){
		studentObj.options[i].selected = true;
	}
	obj.action=newurl;
	obj.submit();
}
function addStudent(stype){
	obj = document.form1.submitType;
	if(obj==null) return;
	obj.value = stype;
	submitForm('');	
}
function addDate(){
	obj = document.getElementById('dates');
	if(obj==null) return;
	newStr = "<input type=text name='target_date[]' value='' maxlength=10><span class='extrainfo'>(yyyy-mm-dd)</span><br>";
	obj.innerHTML+=newStr;
}
function checkform(){
	objStudent = document.getElementsByName('studentID[]')[0];
	objTargetDate = document.getElementsByName('target_date[]');
	objAM = document.form1.dateTypeAM;
	objPM = document.form1.dateTypePM;
	
	if(objStudent==null || objTargetDate ==null || (objAM==null && objPM ==null)) return;
	
	if(objStudent.options.length<=0){
		alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
		 return;
	}
	for(i=0;i<objTargetDate.length;i++){
		if(!check_date(objTargetDate[i],'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')){
			objTargetDate[i].focus();
			return;
		}
	}
	if(objAM!=null && objPM!=null && !objAM.checked && !objPM.checked){
		alert('<?=$i_StudentAttendance_Report_PlsSelectSlot?>');
		return;
	}
	submitForm('new_update.php');
}
</script>
<form name="form1" method="GET">

<br />

<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td align=right><?=$i_general_students_selected?>:&nbsp;</td>
	<td width=80%><table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><?=$select_student?></td>
				<td style='vertical-align:bottom'>
				&nbsp;<a href="javascript:newWindow('choose/index.php?fieldname=studentID[]', 9)"><img src='<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif' border=0></a>
				<br />
				&nbsp;<a href="javascript:checkOptionRemove(document.form1.elements['studentID[]']);submitForm('');"><img src='<?=$image_path?>/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif' border=0></a>
				</td>
			</tr>
			</table><Br>
</td></tr>
<tr><td>&nbsp;</td><td><span class="extrainfo">(<?=$i_general_alternative?>)</span>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr><td nowrap style='vertical-align:bottom'><?=$i_UserLogin; ?></td></tr>
			<tr><td nowrap style='vertical-align:bottom'><input type="text" name="user_login" maxlength="100">&nbsp;<a href="javascript:addStudent(2)"><img src='<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif' border=0></a><br>&nbsp;</td></tr>
			<tr><td nowrap style='vertical-align:bottom'><?= $i_ClassNameNumber; ?></td></tr>
			<tr><td nowrap style='vertical-align:bottom'><?=$select_class?><?=$select_classnum?></td></tr>

		</table><Br>&nbsp;
		</td>
		</tr>
<tr><td align=right><?=$i_Attendance_Date?>:&nbsp;</td><td><span id='dates'>
<?php 
	if(sizeof($target_date)>0){
		for($i=0;$i<sizeof($target_date);$i++)
			echo "<input type='text' name='target_date[]' maxlength=10 value='".$target_date[$i]."'><span class='extrainfo'>(yyyy-mm-dd)</span><br>";
	}else
			echo "<input type='text' name='target_date[]' maxlength=10 value=''><span class='extrainfo'>(yyyy-mm-dd)</span><br>";

?>
</span>
<input type=button value=' + ' onClick='addDate()'><br>&nbsp;
</td></tr>
<tr><td align=right><?=$i_Attendance_DayType?>:&nbsp;</td><td><?=$select_datetype?><br>&nbsp;</td></tr>
<tr><td align=right><?=$i_Attendance_Reason?>:&nbsp;</td><td><textarea cols=60 rows=5 name='reason'><?=$reason?></textarea><br>&nbsp;</td></tr>
<tr><td align=right><?=$i_Attendance_Remark?>:&nbsp;</td><td><textarea cols=60 rows=5 name='remark'><?=$remark?></textarea><br>&nbsp;</td></tr>
</table>
<br><br>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<a href="javascript:checkform()"><img src='<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0'></a> 
<a href='<?=$return_url?>'><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td></tr>
</table>
<br />
<input type=hidden name=submitType value=''>
<input type=hidden name=return_url value='<?=$return_url?>'>
</form>

<?
intranet_closedb();
include_once("../../../../templates/adminfooter.php");
?>