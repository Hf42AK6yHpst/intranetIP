<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
//include_once("../../../../templates/adminheader_setting.php");
include_once("../../../../includes/libdiscipline.php");
intranet_opendb();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('j',$ts_record);
$day_of_week = date('w',$ts_record);

# create confirm log table if not exists
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm($txt_year, $txt_month);
$daily_log_table_name="CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$attendanceMode = $lcardattend->attendance_mode;
$year = getCurrentAcademicYear();
$semester = getCurrentSemester();
$LIDB = new libdb();


###period
switch ($period)
{
        case "1": $DayType = 2;break;
        case "2": $DayType = 3;break;
        default : $DayType = 2;break;
}


# select classes with non school day on TargetDate from CARD_STUDENT_SPECIFIC_DATE_TIME
	$sqlSpecial = "SELECT ClassID,IF(NonSchoolDay=1,1,2) FROM CARD_STUDENT_SPECIFIC_DATE_TIME WHERE RecordDate='$TargetDate'";
	$temp = $lcardattend->returnArray($sqlSpecial,2);
	for($i=0;$i<sizeof($temp);$i++){
			$resultSpecial[$temp[$i][0]]=$temp[$i][1];
	}

# select classes with non school day on Target Cycle Day from CARD_STUDENT_CLASS_PERIOD_TIME
  $sqlClassCycle ="select a.ClassID,IF(NonSchoolDay=1,1,2) from CARD_STUDENT_CLASS_PERIOD_TIME AS a, INTRANET_CYCLE_DAYS AS b WHERE a.DayType=2 and a.DayValue=b.TextShort AND b.RecordDate='$TargetDate'";
	$temp=$lcardattend->returnArray($sqlClassCycle,2);
	for($i=0;$i<sizeof($temp);$i++){
			$resultClassCycle[$temp[$i][0]]=$temp[$i][1];
	}

# select classes with non school day on Target Cycle Day from CARD_STUDENT_PERIOD_TIME
  $sqlSchoolCycle ="select IF(a.NonSchoolDay=1,1,2) from CARD_STUDENT_PERIOD_TIME AS a,INTRANET_CYCLE_DAYS AS b WHERE a.DayType=2 and a.DayValue=b.TextShort AND b.RecordDate='$TargetDate'";
	$resultSchoolCycle=$lcardattend->returnVector($sqlSchoolCycle);

# select classes with non school day on Target Week Day from CARD_STUDENT_PERIOD_TIME
  $sqlSchoolWeek ="select IF(NonSchoolDay=1,1,2) from CARD_STUDENT_PERIOD_TIME WHERE DayType=1 and DayValue='$day_of_week'";
	$resultSchoolWeek = $lcardattend->returnVector($sqlSchoolWeek);
	
# select classes with non school day on Target Week Day from CARD_STUDENT_CLASS_PERIOD_TIME
  $sqlClassWeek ="select ClassID,IF(NonSchoolDay=1,1,2) from CARD_STUDENT_CLASS_PERIOD_TIME WHERE DayType=1 and DayValue='$day_of_week'";
	$temp=$lcardattend->returnArray($sqlClassWeek,2);
	for($i=0;$i<sizeof($temp);$i++){
			$resultClassWeek[$temp[$i][0]]=$temp[$i][1];
	}
	
	
# select the timetable mode for each class
	$resultClassMode = $lcardattend->getClassListMode();
	$confirmCount=0;

	$off_class_list=array();

	for($i=0;$i<sizeof($resultClassMode);$i++){
		$off=false;
		$done=false;
		$classID = $resultClassMode[$i][0];
		$className = $resultClassMode[$i][1];
		$classMode = $resultClassMode[$i][2];
		$specialClassID= $classMode==0?0:$classID;
		# check if NonSchoolDay for Speical Date
		if($resultSpecial[$specialClassID]==1){
				$off=true;
				$done=true;
		}else if($resultSpecial[$specialClassID]==2){
				$off=false;
				$done=true;
		}
		# check if NonSchoolDay for Cycle Day 
		if(!$done){
					if($classMode == 1){ # Class Cycle Day
							if($resultClassCycle[$classID]==1){
									$off = true;
									$done= true;
							}else if($resultClassCycle[$classID]==2){
									$off = false;
									$done= true;
							}
					}else if($classMode !=2){  # School Cycle Day
							if(is_array($resultSchoolCycle) &&$resultSchoolCycle[0]==1){
											$off = true;
											$done = true;
							} else if($resultSchoolCycle[0]==2){
											$off = false;
											$done = true;
							}
					} 
		}
		# check if NonSchoolDay for Week Day
		if(!$done){
				if($classMode==1){ # Class Week Day
							if($resultClassWeek[$classID]==1){
									$done = true;
									$off = true;
							}else if($resultClassWeek[$classID]==2){
									$done = true;
									$off = false;
							}


				}else if($classMode!=2){ # School Week Day
							if(is_array($resultSchoolWeek) && $resultSchoolWeek[0]==1){
											$off = true;
											$done=true;
							} else 	if($resultSchoolWeek[0]==2){
											$off = false;
											$done=true;
							} 

				}
		}
		if($classMode==2 || $off){
				$off_class_list[] = $classID;
		}else{
				$confirmClasses[$confirmCount]['ClassID']=$classID;
				$confirmClasses[$confirmCount++]['ClassName']=$className;
		}
	}

	# determining the pm status 	
	if($lcardattend->attendance_mode==2 || $lcardattend->attendance_mode==3){
	    $pm_expected_field = "CASE
	                              WHEN (d.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND d.LunchOutTime IS NULL) OR (d.LunchBackTime IS NOT NULL)
	                                   THEN '".CARD_STATUS_PRESENT."'
	                              WHEN (d.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND d.LunchOutTime IS NOT NULL) AND (d.LunchBackTime IS NULL)
	                                   THEN '".CARD_STATUS_ABSENT."'
	                              WHEN (d.AMStatus IS NULL OR d.AMStatus = '".CARD_STATUS_ABSENT."') AND f.OutingID IS NULL THEN '".CARD_STATUS_ABSENT."'
	                              WHEN (d.AMStatus IS NULL OR d.AMStatus = '".CARD_STATUS_ABSENT."') AND f.OutingID IS NOT NULL THEN '".CARD_STATUS_OUTING."'
	                         END";			
	}else{
		
		$pm_expected_field = "CASE
	                              WHEN (d.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND d.LunchOutTime IS NULL) OR (d.LunchBackTime IS NOT NULL)
	                                   THEN '".CARD_STATUS_PRESENT."'
	                              WHEN d.AMStatus IS NULL AND f.OutingID IS NULL
	                                   THEN '".CARD_STATUS_ABSENT."'
	                              WHEN d.AMStatus IS NULL AND f.OutingID IS NOT NULL
	                                   THEN '".CARD_STATUS_OUTING."'
	                              ELSE '".CARD_STATUS_ABSENT."'
	                          END";
	}
/*                         
  $sqlStudentList = "SELECT c.UserID,e.ClassID,e.ClassName,d.RecordID,$pm_expected_field FROM 
  					INTRANET_USER AS c LEFT JOIN 
  					$daily_log_table_name AS d ON(c.UserID=d.UserID AND d.DayNumber='$txt_day'),
  					INTRANET_CLASS AS e
					LEFT JOIN CARD_STUDENT_OUTING AS f ON (c.UserID=f.UserID AND f.RecordDate = '$TargetDate')
  					WHERE c.ClassName=e.ClassName AND ($pm_expected_field='".CARD_STATUS_ABSENT."' AND d.PMStatus IS NULL)";
  */					
 $sqlAllStudent =   "SELECT c.UserID,e.ClassID,e.ClassName,d.RecordID,$pm_expected_field FROM 
  					INTRANET_USER AS c LEFT JOIN 
  					$daily_log_table_name AS d ON(c.UserID=d.UserID AND d.DayNumber='$txt_day'),
  					INTRANET_CLASS AS e
					LEFT JOIN CARD_STUDENT_OUTING AS f ON (c.UserID=f.UserID AND f.RecordDate = '$TargetDate')
  					WHERE c.ClassName=e.ClassName";
/*
  $sqlOutingList = "SELECT c.UserID,$pm_expected_field FROM 
  					INTRANET_USER AS c LEFT JOIN 
  					$daily_log_table_name AS d ON(c.UserID=d.UserID AND d.DayNumber='$txt_day'),
  					INTRANET_CLASS AS e
					LEFT JOIN CARD_STUDENT_OUTING AS f ON (c.UserID=f.UserID AND f.RecordDate = '$TargetDate')
  					WHERE c.ClassName=e.ClassName AND ($pm_expected_field='".CARD_STATUS_OUTING."' AND d.PMStatus IS NULL)";  					  					
  */		
  if(sizeof($off_class_list)>0){
	  /*
  		$sqlStudentList .=" AND e.ClassID NOT IN(".implode(",",$off_class_list).")";
  		$sqlOutingList.=" AND e.ClassID NOT IN(".implode(",",$off_class_list).")";
  	  */
  		$sqlAllStudent .=" AND e.ClassID NOT IN(".implode(",",$off_class_list).")";
  }
  /*
  $sqlStudentList.=" AND e.RecordStatus=1 AND c.RecordType=2 AND c.RecordStatus IN(0,1) AND c.ClassName IS NOT NULL";
  $sqlOutingList.=" AND e.RecordStatus=1 AND c.RecordType=2 AND c.RecordStatus IN(0,1) AND c.ClassName IS NOT NULL AND f.RecordDate='$TargetDate'";
  */
  $sqlAllStudent.=" AND e.RecordStatus=1 AND c.RecordType=2 AND c.RecordStatus IN(0,1) AND c.ClassName IS NOT NULL";

  /*
	$absentStudentList = $lcardattend->returnArray($sqlStudentList,5);
      
  
	//$absentStudentList = $lcardattend->returnArray($sqlStudentList,4);


### for student confirm
$tmpOutingStudentList = $lcardattend->returnVector($sqlOutingList,2);
	# create outing student list
for($z=0;$z<sizeof($tmpOutingStudentList);$z++){
	$outingStudentList[]=$tmpOutingStudentList[$z][0];
}
*/

	$allStudentList = $lcardattend->returnArray($sqlAllStudent,5);

for($i=0; $i<sizeOf($allStudentList); $i++)
{
        $my_user_id = $allStudentList[$i][0];
  			$my_record_id= $allStudentList[$i][3];
        $my_day = $txt_day;
        $my_status = $allStudentList[$i][4];
        /*
        if(is_array($outingStudentList)&& in_array($my_user_id,$outingStudentList))
		        $my_status = CARD_STATUS_OUTING;
		   else $my_status= CARD_STATUS_ABSENT;
		*/
        if( $period == "2")            # PM
        {
                # insert if not exist
                if($my_record_id=="")
                {
                        $sql = "INSERT INTO $daily_log_table_name
                                                        (UserID,DayNumber,PMStatus,DateInput,DateModified) VALUES
                                                        ($my_user_id,$my_day,$my_status,NOW(),NOW())
                                                ";
                        $LIDB->db_db_query($sql);
                }
                else  # update if exist
                {
                		$sql ="UPDATE $daily_log_table_name SET PMStatus = $my_status WHERE RecordID = $my_record_id";
                		$LIDB->db_db_query($sql);
                }
        }
}

### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

$confirmed_user_id = -1;

for($i=0;$i<sizeof($confirmClasses);$i++){
		$class_id = $confirmClasses[$i]['ClassID'];
		$class_name = $confirmClasses[$i]['ClassName'];	
		$sql = "SELECT
						a.RecordID
	          FROM $card_student_daily_class_confirm as a
	               LEFT OUTER JOIN INTRANET_CLASS as b ON (a.ClassID=b.ClassID)
	               WHERE b.ClassName = \"$class_name\" AND a.DayNumber = ".$txt_day." AND a.DayType = $DayType";
		$result = $LIDB->returnArray($sql,1);
		$confirmed_id = $result[0][0];
		if( $confirmed_id <> "" )
		{
		        # update if record exist
		        $sql = "UPDATE $card_student_daily_class_confirm SET ConfirmedUserID=$confirmed_user_id, DateModified = NOW() WHERE RecordID = '$confirmed_id'";
		        $LIDB->db_db_query($sql);
		        $msg = 2;
		}
		else
		{
		        # insert if record not exist
	
		        $sql = "INSERT INTO $card_student_daily_class_confirm
		                                        (
		                                                ClassID,
		                                                ConfirmedUserID,
		                                                DayNumber,
		                                                DayType,
		                                                DateInput,
		                                                DateModified
		                                        ) VALUES
		                                        (
		                                                '$class_id',
		                                                '$confirmed_user_id',
		                                                '$txt_day',
		                                                '$DayType',
		                                                NOW(),
		                                                NOW()
		                                        )
		                                        ";
		        $LIDB->db_db_query($sql);
		        $msg = 1;
		}
}

$return_url = "class_status.php?TargetDate=$TargetDate&period=$period&msg=$msg";

header( "Location: $return_url");

?>


