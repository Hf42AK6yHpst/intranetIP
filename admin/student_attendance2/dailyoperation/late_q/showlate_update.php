<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../includes/libdiscipline.php");

intranet_opendb();

# class used
$LIDB = new libdb();
$ldiscipline = new libdiscipline();
$lcardattend = new libcardstudentattend2();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || ($period!=1 && $period!=2) )
{
    header("Location: index.php");
    exit();
}

$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$year = getCurrentAcademicYear();
$semester = getCurrentSemester();

### update daily records
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

/*
for($i=0; $i<sizeOf($user_id); $i++)
{
        $my_user_id = $user_id[$i];
        $my_day = $txt_day;
        $my_drop_down_status = $drop_down_status[$i];
        $my_record_id = $record_id[$i];

        if( $period == "1")        # AM
        {
            if ($my_drop_down_status == 2)       # Late
            {
                # Reason input
                $txtReason = ${"reason$i"};
                $txtReason = intranet_htmlspecialchars($txtReason);

                # Get ProfileRecordID 
                $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                $temp = $lcardattend->returnArray($sql,2);
                list($reason_record_id, $reason_profile_id) = $temp[0];

                if ($reason_record_id == "")           # Reason record not exists
                {
                    # Search whether attendance exists by date, student and type
                    $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                   WHERE AttendanceDate = '$TargetDate'
                                         AND UserID = $my_user_id
                                         AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                         AND RecordType = '".PROFILE_TYPE_LATE."'";

                    $temp = $lcardattend->returnVector($sql);
                    $attendance_id = $temp[0];
                    if ($attendance_id == "")          # Record not exists
                    {
                        # Insert profile record
                        $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                        $temp = $lcardattend->returnArray($sql,2);
                        list ($user_classname, $user_classnum) = $temp[0];

                        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason";
                        $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason'";
                        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                        $lcardattend->db_db_query($sql);
                        $attendance_id = $lcardattend->db_insert_id();

						# Calculate upgrade items
						 if ($plugin['Discipline'])
						 {
							 $ldiscipline->calculateUpgradeLateToDemerit($student_id);
							 $ldiscipline->calculateUpgradeLateToDetention($student_id);
						 }
                    }
                    else
                    {
                        # Update Reason in profile record By AttendanceID
                        $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                       WHERE StudentAttendanceID = '$attendance_id'";
                        $lcardattend->db_db_query($sql);
                    }

                    # Insert to Reason table
                    $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, DateInput, DateModified";
                    $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
                    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                   VALUES ($fieldsvalues)";
                    $lcardattend->db_db_query($sql);
                }
                else  # Reason record exists
                {
                    if ($reason_profile_id == "")    # Profile ID not exists
                    {
                        # Search whether attendance exists by date, student and type
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE AttendanceDate = '$TargetDate'
                                             AND UserID = $my_user_id
                                             AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                             AND RecordType = '".PROFILE_TYPE_LATE."'";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];
                        if ($attendance_id == "")          # Record not exists
                        {
                            # Insert profile record
                            $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                            $temp = $lcardattend->returnArray($sql,2);
                            list ($user_classname, $user_classnum) = $temp[0];
                            $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason";
                            $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason'";
                            $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                            $lcardattend->db_db_query($sql);
                            $attendance_id = $lcardattend->db_insert_id();

							# Calculate upgrade items
							 if ($plugin['Discipline'])
							 {
								 $ldiscipline->calculateUpgradeLateToDemerit($student_id);
								 $ldiscipline->calculateUpgradeLateToDetention($student_id);
							 }
                        }
                        else
                        {
                            # Update Reason in profile record By AttendanceID
                            $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                            $lcardattend->db_db_query($sql);
                        }
                    }
                    else  # Has Profile ID
                    {
                        # Search Attendance By ID
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE StudentAttendanceID = $reason_profile_id";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];
                        if ($attendance_id == "")          # Record not exists
                        {
                            # Search attendance by date, student and type
                            $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                           WHERE AttendanceDate = '$TargetDate'
                                                 AND UserID = $my_user_id
                                                 AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                                 AND RecordType = '".PROFILE_TYPE_LATE."'";
                            $temp = $lcardattend->returnVector($sql);
                            $attendance_id = $temp[0];
                            if ($attendance_id == "")          # Record not exists
                            {
                                # insert reason in profile record
                                $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                $temp = $lcardattend->returnArray($sql,2);
                                list ($user_classname, $user_classnum) = $temp[0];
                                $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason";
                                $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason'";
                                $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                $lcardattend->db_db_query($sql);
                                $attendance_id = $lcardattend->db_insert_id();

								# Calculate upgrade items
								 if ($plugin['Discipline'])
								 {
									 $ldiscipline->calculateUpgradeLateToDemerit($student_id);
									 $ldiscipline->calculateUpgradeLateToDetention($student_id);
								 }
                            }
                            else
                            {
                                # Update Reason in profile record By AttendanceID
                                $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                               WHERE StudentAttendanceID = '$attendance_id'";
                                $lcardattend->db_db_query($sql);
                            }
                        }
                        else # profile record exists and valid
                        {
                            # Update reason in profile record
                            $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                            $lcardattend->db_db_query($sql);

                        }
                    }
                    # Update reason table record
                    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
                                   ProfileRecordID = '$attendance_id'
                                   WHERE RecordID= '$reason_record_id'";
                    $lcardattend->db_db_query($sql);
                }
            }
            else if ($my_drop_down_status == 0)    # On Time
            {
                 # Remove Reason record
                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                 $lcardattend->db_db_query($sql);

                 # Reset Upgrade items
                 if ($plugin['Discipline'])
                 {
                     $target_date = $TargetDate;
                     $student_id = $my_user_id;
                     $ldiscipline->resetUpgradeLateToDemerit($student_id,$target_date);
                     $ldiscipline->resetUpgradeLateToDetention($student_id,$target_date);
                 }

                 # Remove Profile record
                 $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                               WHERE AttendanceDate = '$TargetDate'
                                     AND UserID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                 $lcardattend->db_db_query($sql);

                 # Calculate upgrade items
                 if ($plugin['Discipline'])
                 {
                     $ldiscipline->calculateUpgradeLateToDemerit($student_id);
                     $ldiscipline->calculateUpgradeLateToDetention($student_id);
                 }
                 # Set to Daily Record Table
                 # Set AMStatus and InSchoolTime
                 $sql = "UPDATE $card_log_table_name
                                SET AMStatus = '".CARD_STATUS_PRESENT."',
                                InSchoolTime = NULL
                                WHERE DayNumber = '$txt_day'
                                      AND UserID = $my_user_id";
                 $lcardattend->db_db_query($sql);
            }
            else # Unknown action
            {
                 # Do nthg
            }
        }
        else if ($period == "2") # PM
        {
            if ($my_drop_down_status == 2)       # Late
            {
                # Reason input
                $txtReason = ${"reason$i"};
                $txtReason = intranet_htmlspecialchars($txtReason);
                # Get ProfileRecordID
                $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                $temp = $lcardattend->returnArray($sql,2);
                list($reason_record_id, $reason_profile_id) = $temp[0];
                if ($reason_record_id == "")           # Reason record not exists
                {
                    # Search whether attendance exists by date, student and type
                    $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                   WHERE AttendanceDate = '$TargetDate'
                                         AND UserID = $my_user_id
                                         AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                         AND RecordType = '".PROFILE_TYPE_LATE."'";
                    $temp = $lcardattend->returnVector($sql);
                    $attendance_id = $temp[0];
                    if ($attendance_id == "")          # Record not exists
                    {
                        # Insert profile record
                        $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                        $temp = $lcardattend->returnArray($sql,2);
                        list ($user_classname, $user_classnum) = $temp[0];
                        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason";
                        $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason'";
                        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                        $lcardattend->db_db_query($sql);
                        $attendance_id = $lcardattend->db_insert_id();

						# Calculate upgrade items
						if ($plugin['Discipline'])
						{
							$ldiscipline->calculateUpgradeLateToDemerit($student_id);
							$ldiscipline->calculateUpgradeLateToDetention($student_id);
						}
                    }
                    else
                    {
                        # Update Reason in profile record By AttendanceID
                        $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                       WHERE StudentAttendanceID = '$attendance_id'";
                        $lcardattend->db_db_query($sql);
                    }
                    # Insert to Reason table
                    $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, DateInput, DateModified";
                    $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
                    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                   VALUES ($fieldsvalues)";
                    $lcardattend->db_db_query($sql);
                }
                else  # Reason record exists
                {
                    if ($reason_profile_id == "")    # Profile ID not exists
                    {
                        # Search whether attendance exists by date, student and type
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE AttendanceDate = '$TargetDate'
                                             AND UserID = $my_user_id
                                             AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                             AND RecordType = '".PROFILE_TYPE_LATE."'";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];
                        if ($attendance_id == "")          # Record not exists
                        {
                            # Insert profile record
                            $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                            $temp = $lcardattend->returnArray($sql,2);
                            list ($user_classname, $user_classnum) = $temp[0];
                            $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason";
                            $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason'";
                            $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                            $lcardattend->db_db_query($sql);
                            $attendance_id = $lcardattend->db_insert_id();

							# Calculate upgrade items
							if ($plugin['Discipline'])
							{
								$ldiscipline->calculateUpgradeLateToDemerit($student_id);
								$ldiscipline->calculateUpgradeLateToDetention($student_id);
							}
                        }
                        else
                        {
                            # Update Reason in profile record By AttendanceID
                            $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                            $lcardattend->db_db_query($sql);
                        }
                    }
                    else  # Has Profile ID
                    {
                        # Search Attendance By ID
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE StudentAttendanceID = $reason_profile_id";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];
                        if ($attendance_id == "")          # Record not exists
                        {
                            # Search attendance by date, student and type
                            $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                           WHERE AttendanceDate = '$TargetDate'
                                                 AND UserID = $my_user_id
                                                 AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                                 AND RecordType = '".PROFILE_TYPE_LATE."'";
                            $temp = $lcardattend->returnVector($sql);
                            $attendance_id = $temp[0];
                            if ($attendance_id == "")          # Record not exists
                            {
                                # insert reason in profile record
                                $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                $temp = $lcardattend->returnArray($sql,2);
                                list ($user_classname, $user_classnum) = $temp[0];
                                $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason";
                                $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason'";
                                $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                $lcardattend->db_db_query($sql);
                                $attendance_id = $lcardattend->db_insert_id();

								# Calculate upgrade items
								if ($plugin['Discipline'])
								{
									$ldiscipline->calculateUpgradeLateToDemerit($student_id);
									$ldiscipline->calculateUpgradeLateToDetention($student_id);
								}
                            }
                            else
                            {
                                # Update Reason in profile record By AttendanceID
                                $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                               WHERE StudentAttendanceID = '$attendance_id'";
                                $lcardattend->db_db_query($sql);
                            }
                        }
                        else # profile record exists and valid
                        {
                            # Update reason in profile record
                            $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                            $lcardattend->db_db_query($sql);

                        }
                    }
                    # Update reason table record
                    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
                                   ProfileRecordID = '$attendance_id'
                                   WHERE RecordID= '$reason_record_id'";
                    $lcardattend->db_db_query($sql);
                }

				# Calculate upgrade items
                 if ($plugin['Discipline'])
                 {
                     $ldiscipline->calculateUpgradeLateToDemerit($student_id);
                     $ldiscipline->calculateUpgradeLateToDetention($student_id);
                 }
            }
            else if ($my_drop_down_status == 0)    # On Time
            {
                 # Remove Reason record
                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                 $lcardattend->db_db_query($sql);

                 # Reset upgrade items
                 if ($plugin['Discipline'])
                 {
                     $target_date = $TargetDate;
                     $student_id = $my_user_id;
                     $ldiscipline->resetUpgradeLateToDemerit($student_id,$target_date);
                     $ldiscipline->resetUpgradeLateToDetention($student_id,$target_date);
                 }

                 # Remove Profile record
                 $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                               WHERE AttendanceDate = '$TargetDate'
                                     AND UserID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                 $lcardattend->db_db_query($sql);

                 # Calculate upgrade items
                 if ($plugin['Discipline'])
                 {
                     $ldiscipline->calculateUpgradeLateToDemerit($student_id);
                     $ldiscipline->calculateUpgradeLateToDetention($student_id);
                 }

                 # Set to Daily Record Table
                 # Set AMStatus and InSchoolTime
                 $sql = "UPDATE $card_log_table_name
                                SET PMStatus = '".CARD_STATUS_PRESENT."',
                                LunchBackTime = NULL
                                WHERE DayNumber = '$txt_day'
                                      AND UserID = $my_user_id";
                 $lcardattend->db_db_query($sql);
            }
            else # Unknown action
            {
                 # Do nthg
            }

        }
        else # Unknown action
        {
               # Do nthg
        }
} # End of For-Loop
*/

if ($period==1)
{
    $period_type = PROFILE_DAY_TYPE_AM;
}
else if ($period==2)
{
    $period_type = PROFILE_DAY_TYPE_PM;
}
else
{
    $period_type = "";
}

if ($period_type != "")
{				
	for ($i=0; $i<sizeof($user_ids); $i++)
	{	
		$StudentID = $user_ids[$i];
		$LateType = ${"LateType_$StudentID"};		
		
		$DemeritWaived = (is_array($WaiveDemerit) && in_array($StudentID, $WaiveDemerit)) ? 1 : 0;
		$DetentionWaived = (is_array($WaiveDetention) && in_array($StudentID, $WaiveDetention)) ? 1 : 0;				
		
		# Get ProfileRecordID
        $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                       WHERE RecordDate = '$TargetDate'
                             AND StudentID = $StudentID
                             AND DayType = '$period_type'
                             AND RecordType = '".PROFILE_TYPE_LATE."'";
        $temp = $lcardattend->returnArray($sql,2);
        list($reason_record_id, $reason_profile_id) = $temp[0];
    
        if ($reason_record_id == "")           # Reason record not exists
        {
			# Search whether attendance exists by date, student and type
			$sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
			               WHERE AttendanceDate = '$TargetDate'
			                     AND UserID = $StudentID
			                     AND DayType = '$period_type'
			                     AND RecordType = '".PROFILE_TYPE_LATE."'";
			$temp = $lcardattend->returnVector($sql);
			$attendance_id = $temp[0];
            if ($attendance_id == "")          # Record not exists
            {
                # Insert profile record
                $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $StudentID";
                $temp = $lcardattend->returnArray($sql,2);
                list ($user_classname, $user_classnum) = $temp[0];

                $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput, DateModified,ClassName,ClassNumber";
                $fieldvalue = "'$StudentID','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum'";
                $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                $lcardattend->db_db_query($sql);
                $attendance_id = $lcardattend->db_insert_id();			
            }            
            
            $DemeritUpgrade = 0;
            $DetentionUpgrade = 0;
            
			# Demerit and Detention Record
            if ($LateType == 1)
            {
	            if ($DemeritWaived == 1 && $DetentionWaived == 1) continue;
	            if ($DemeritWaived == 0)
	            {
	            	$sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID = '$StudentID' AND DemeritWaived = 0 AND DemeritUpgrade = 0";
	            	$temp = $lcardattend->returnVector($sql);
	            	$DemeritUpgrade = (sizeof($temp) == 2) ? 1 : 0;

					if ($DemeritUpgrade)
	            	{
		            	# Add Demerit Record
                        $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $StudentID";
                        $temp = $lcardattend->returnArray($sql,2);
                		list ($user_classname, $user_classnum) = $temp[0];

                		# Insert Record to PROFILE_STUDENT_MERIT
                		$values = "now(),'$year','$semester','$StudentID', '".$_SESSION['UserID']."','1', '$user_classname', '$user_classnum', now(),now()";
                		$sql = "INSERT INTO PROFILE_STUDENT_MERIT (MeritDate,Year, Semester, UserID, PersonInCharge, RecordStatus, ClassName, ClassNumber, DateInput, DateModified) VALUES ($values)";
                        $lcardattend->db_db_query($sql);
                        $profileRecordID = $lcardattend->db_insert_id();
                        
		            	# Update previous records in CARD_STUDENT_PROFILE_RECORD_REASON
		            	$RecordIDs = implode(",", $temp[0]);
		            	$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DemeritUpgrade = 1 WHERE RecordID IN ($RecordIDs)";
		            	$lcardattend->db_db_query($sql);
	            	}
				}
	            if ($DetentionWaived == 0)
	            {
	            	$sql = "SELECT COUNT(*) FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID = '$StudentID' AND DetentionWaived = 0 AND DetentionUpgrade <> 1";
	            	$temp = $lcardattend->returnVector($sql);
	            	$DetentionUpgrade = (sizeof($temp) == 2) ? 1 : 0;
	            	
	            	if ($DetentionUpgrade)
	            	{
		            	$fields = "StudentID, RecordDate, PICID, Minutes";
		            	$values = "'$StudentID', now(), '".$_SESSION['UserID']."', 30";
		            	
		            	$sql = "INSERT INTO DISCIPLINE_DETENTION_RECORD ($fields) VALUES ($values)";
		            	$lcardattend->db_db_query($sql);
		            	$DetentionID = $lcardattend->db_insert_id();
		            	
		            	# Update the UpgradedToDetentionID field in PROFILE_STUDENT_ATTENDANCE
                        $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET UpgradedToDetentionID = '$DetentionID' WHERE StudentAttendanceID = '$attendance_id'";
                        $lcardattend->db_db_query($sql);
                        
                        # Update DetentionRecordID in CARD_STUDENT_PROFILE_RECORD_REASON
                        $RecordIDs = implode(",", $temp[0]);
                        $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DetentionRecordID = '$DetentionID' WHERE RecordID IN ($RecordIDs)";
                        $lcardattend->db_db_query($sql);
	            	}
            	}
            }
            elseif ($LateType == 2)
            {
	            if ($DemeritWaived == 1 && $DetentionWaived == 1) continue;
	            if ($DemeritWaived == 0)
	            {
		            # Add Demerit Record
                    $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $StudentID";
                    $temp = $lcardattend->returnArray($sql,2);
            		list ($user_classname, $user_classnum) = $temp[0];

            		# Insert record to PROFILE_STUDENT_MERIT
		            $fields = "MeritDate, Year, Semester, UserID, PersonInCharge, RecordStauts, ClassName, ClassNumber, DateInput, DateModified";
		            $values = "now(), '$year', '$semester', '".$_SESSION['UserID']."', 1, '$user_classname', '$user_classnum', now(), now()";
		            $sql = "INSERT INTO PROFILE_STUDENT_MERIT ($fields) VALUES ($values)";
		            $lcardattend->db_db_query($sql);
		            $profileRecordID = $lcardattend->db_insert_id();
	            }
	            if ($DetentionWaived == 0)
	            {
		            $fields = "StudentID, RecordDate, PICID, Minutes";
		            $values = "'$StudentID', now(), '".$_SESSION['UserID']."', 30";
		            
		            $sql = "INSERT INTO DISCIPLINE_DETENTION_RECORD ($fields) VALUES ($values)";
		            $lcardattend->db_db_query($sql);
		            $DetentionID = $lcardattend->db_insert_id();
		            
		            # Update UpgradedToDetentionID in PROFILE_STUDENT_ATTENDANCE
		            $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET UpgradedToDetentionID = '$DetentionID' WHERE StudentAttendanceID = '$attendance_id'";
		            $lcardattend->db_db_query($sql);
	            }
            }

            # Insert to Reason table
            $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, LateType, DemeritWaived, DetentionWaived, DetentionRecordID, DemeritUpgrade, DetentionUpgrade, DateInput, DateModified";
            $fieldsvalues = "'$TargetDate', '$StudentID', '$attendance_id', '".PROFILE_TYPE_LATE."', '$LateType', '$period_type', $DetentionID, '$DemeritWaived', '$DetentionWaived', $DemeritUpgrade, $DetentionUpgrade, now(), now() ";
            $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname) VALUES ($fieldsvalues)";
            $lcardattend->db_db_query($sql);
		}
		else  # Reason record exists
		{
		    if ($reason_profile_id == "")    # Profile ID not exists
		    {
		        # Search whether attendance exists by date, student and type
		        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
		                       WHERE AttendanceDate = '$TargetDate'
		                             AND UserID = $StudentID
		                             AND DayType = '$period_type'
		                             AND RecordType = '".PROFILE_TYPE_LATE."'";
		        $temp = $lcardattend->returnVector($sql);
		        $attendance_id = $temp[0];
		        if ($attendance_id == "")          # Record not exists
		        {
		            # Insert profile record
		            $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $StudentID";
		            $temp = $lcardattend->returnArray($sql,2);
		            list ($user_classname, $user_classnum) = $temp[0];
		            $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
		            $fieldvalue = "'$StudentID','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','$period_type',now(),now(),'$user_classname','$user_classnum'";
		            $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
		            $lcardattend->db_db_query($sql);
		            $attendance_id = $lcardattend->db_insert_id();		            
		        }
		    }
		    else  # Has Profile ID
		    {
		        # Search Attendance By ID
		        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
		                       WHERE StudentAttendanceID = $reason_profile_id";
		        $temp = $lcardattend->returnVector($sql);
		        $attendance_id = $temp[0];
		        if ($attendance_id == "")          # Record not exists
		        {
		            # Search attendance by date, student and type
		            $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
		                           WHERE AttendanceDate = '$TargetDate'
		                                 AND UserID = $StudentID
		                                 AND DayType = '".PROFILE_DAY_TYPE_AM."'
		                                 AND RecordType = '".PROFILE_TYPE_LATE."'";
		            $temp = $lcardattend->returnVector($sql);
		            $attendance_id = $temp[0];
		            if ($attendance_id == "")          # Record not exists
		            {
		                # insert reason in profile record
		                $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $StudentID";
		                $temp = $lcardattend->returnArray($sql,2);
		                list ($user_classname, $user_classnum) = $temp[0];
		                $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
		                $fieldvalue = "'$StudentID','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum'";
		                $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
		                $lcardattend->db_db_query($sql);
		                $attendance_id = $lcardattend->db_insert_id();						
		            }
		        }	
		    }
		    
		    # DEMERIT AND DETENTION HANDLING
		    if ($LateType == 1)
		    {
			    if ($DemeritWaived == 0)
			    {
				    $sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID = '$StudentID' AND DemeritWaived = 0 AND (DemeritUpgraded IS NULL OR DemeritUpgraded <> 1) ORDER BY RecordDate DESC LIMIT 0, 3";
				    $temp = $lcardattend->returnVector($sql);
				    $DemeritUpgrade = (in_array($reason_record_id, $temp) && sizeof($temp) == 3) ? 1 : 0;

				    if ($DemeritUpgrade)
				    {
					    # Add Demerit Record
					    $semester = getCurrentSemester();
					    $year = getCurrentAcademicYear();
					    
					    $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$StudentID'";
					    $user_array = $lcardattend->returnArray($sql, 2);
					    list ($user_classname, $user_classnum) = $user_array[0];
					    
					    # Insert Record to PROFILE_STUDENT_MERIT
					    $fields = "MeritDate, Year, Semester, UserID, RecordStatus, ClassName, ClassNumber, DateInput, DateModified";
					    $values = "now(), '$year', '$semester', '$StudentID', 1, '$user_classname', '$user_classnum', now(), now()";
					    $sql = "INSERT INTO PROFILE_STUDENT_MERIT ($fields) VALUES ($values)";
					    $lcardattend->db_db_query($sql);
					    $profileRecordID = $lcardattend->db_insert_id();
					    
					    # INSERT Record to DISCIPLINE_MERIT_RECORD
					    $fields = "RecordDate, Year, Semester, StudentID, ItemID, ItemText, MeritType, ProfileMeritType, ProfileMeritCount, ProfileMeritID, DateInput, DateModified";
					    $values = "now(), '$year', '$semester', '$StudentID', 0, '$i_Qualied_Late', -1, -1, 1, $profileRecordID, now(), now()";
					    $sql = "INSERT INTO DISCIPLINE_MERIT_RECORD ($fields) VALUES ($values)";
					    $lcardattend->db_db_query($sql);
					    
					    # Update DemeritUpgrade in CARD_STUDENT_PROFILE_RECORD_REASON
					    $RecordIDs = implode(",", $temp);
					    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DemeritUpgraded = 1 WHERE RecordID IN ($RecordIDs)";
					    $lcardattend->db_db_query($sql);
				    }
			    }
			    
			    if ($DetentionWaived == 0)
			    {
				    $sql = "SELECT RecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID = '$StudentID' AND DetentionWaived = 0 AND DetentionUpgraded <> 1 ORDER BY RecordDate DESC LIMIT 0, 3";
				    $temp = $lcardattend->returnVector($sql);
				    $DetentionUpgrade = (in_array($reason_record_id, $temp) && sizeof($temp) == 3) ? 1 : 0;
				    
				    if ($DetentionUpgrade)
				    {
					    $fields = "StudentID, RecordDate, PICID, Minutes";
					    $values = "'$StudentID', now(), '".$_SESSION['UserID']."', 30";	
					    $sql = "INSERT INTO DISCIPLINE_DETENTION_RECORD ($fields) VALUES ($values)";
					    $lcardattend->db_db_query($sql);
					    $DetentionID = $lcardattend->db_insert_id();
					    
					    # Update UpgradedToDetention
					    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET UpgradedToDententionID = '$DetentionID' WHERE StudentAttendanceID = '$attendance_id'";
					    $lcardattend->db_db_query($sql);
					    
					    # Update DetentionRecordID AND DetentionUpgrade
					    $RecordIDs = implode(",", $temp);
					    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DetentionRecordID = '$DetentionID' AND DetentionUpgrade = 1 WHERE RecordID IN ($RecordIDs)";
					    $lcardattend->db_db_query($sql);
				    }
			    }
		    }
		    elseif ($LateType == 2)
		    {
			    if ($DemeritWaived == 0)
			    {
				    # Check DemeritUpgraded
				    $sql = "SELECT DemeritUpgraded FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordID = '$reason_record_id'";
				    $demerit = $lcardattend->returnVector($sql);
				    
				    if ($demerit[0] != 1)
				    {
					    # Add Demerit Record
					    $semester = getCurrentSemester();
					    $year = getCurrentAcademicYear();
					    
					    $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$StudentID'";
					    $user_array = $lcardattend->returnArray($sql, 2);
					    list ($user_classname, $user_classnum) = $user_array[0];
					    
					    # Insert record to PROFILE_STUDENT_MERIT
					    $fields = "MeritDate, Year, Semester, UserID, RecordStatus, ClassName, ClassNumber, DateInput, DateModified";
					    $values = "now(), '$year', '$semester', '$StudentID', 1, '$user_classname', '$user_classnum', now(), now()";
					    $sql = "INSERT INTO PROFILE_STUDENT_MERIT ($fields) VALUES ($values)";
					    $lcardattend->db_db_query($sql);
					    $profileRecordID = $lcardattend->db_insert_id();
					    
					    # INSERT Record to DISCIPLINE_MERIT_RECORD
					    $fields = "RecordDate, Year, Semester, StudentID, ItemID, ItemText, MeritType, ProfileMeritType, ProfileMeritCount, ProfileMeritID, DateInput, DateModified";
					    $values = "now(), '$year', '$semester', '$StudentID', 0, '$i_Qualied_Late', -1, -1, 1, $profileRecordID, now(), now()";
					    $sql = "INSERT INTO DISCIPLINE_MERIT_RECORD ($fields) VALUES ($values)";
					    $lcardattend->db_db_query($sql);
					    					    
					    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DemeritUpgraded = 1 WHERE RecordID = '$reason_record_id'";
					    $lcardattend->db_db_query($sql);
				    }
			    }
			    elseif ($DetentionWaived == 0)
			    {
				    # Check DetentionUpgraded
				    $sql = "SELECT DetentionUpgraded FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordID = '$reason_record_id'";
				    $detention = $lcardattend->db_db_query($sql);
					
				    if ($detention[0] != 1)
				    {
					    $fields = "StudentID, RecordDate, PICID, Minutes";
					    $values = "'$StudentID', now(), '".$_SESSION['UserID']."', 30";
					    
					    $sql = "INSERT INTO DISCIPLINE_DETENTION_RECORD ($fields) VALUES ($values)";
					    $lcardattend->db_db_query($sql);
					    $DetentionID = $lcardattend->db_insert_id();
					    
					    # Update UpgradedToDetentionID in PROFILE_STUDENT_ATTENDANCE
					    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET UpgradedToDetentionID = '$DetentionID' WHERE StudentAttendanceID = '$attendance_id'";
					    $lcardattend->db_db_query($sql);
					    
					    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DetentionUpgraded = 1, DetentionRecordID = '$DetentionID' WHERE RecordID = '$reason_record_id'";
					    $lcardattend->db_db_query($sql);
				    }
			    }
		    }
		    
		    # Update reason table record
		    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET ProfileRecordID = '$attendance_id',
		                   DemeritWaived = '$DemeritWaived',
		                   DetentionWaived = '$DetentionWaived'
		                   WHERE RecordID= '$reason_record_id'";	
		    $lcardattend->db_db_query($sql);
		}
	}
	
    # Update Confirm Record
    $sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM
                   SET LateConfirmed = 1, LateConfirmTime = now(), DateModified = now()
                   WHERE RecordDate = '$TargetDate' AND RecordType = $period_type";
    $lcardattend->db_db_query($sql);
    if ($lcardattend->db_affected_rows()!=1)         # Not Exists
    {
        # Not exists
        $sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate, LateConfirmed,RecordType, LateConfirmTime, DateInput, DateModified)
                       VALUES ('$TargetDate',1,$period_type,now(),now(),now())";
        $lcardattend->db_db_query($sql);
    }
}
header("Location: showlate.php?period=$period&TargetDate=$TargetDate&msg=2");
?>