<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

### class used
$lcardattend = new libcardstudentattend2();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($period)
{
        case "1": $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
        case "2": $display_period = $i_DayTypePM;
                                                $DayType = PROFILE_DAY_TYPE_PM;
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
}



### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Late
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
                b.InSchoolTime,
                IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
                c.Reason
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_LATE."'
                WHERE b.DayNumber = '$txt_day' AND b.AMStatus = '".CARD_STATUS_LATE."'
                      AND a.RecordType = 2 AND a.RecordStatus IN (0,1)
         ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
";
}
else     # Check PM Late
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
                b.LunchBackTime,
                IF(b.LunchBackStation IS NULL, '-', b.LunchBackStation),
                c.Reason
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_LATE."'
                WHERE b.DayNumber = '$txt_day' AND b.PMStatus = '".CARD_STATUS_LATE."'
                AND a.RecordType = 2 AND a.RecordStatus IN (0,1)
         ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
";

}

$result = $lcardattend->returnArray($sql,8);

$table_attend = "";
# <td class=tableTitle>#</td>
$x = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr class=tableTitle><td>#</td><td>$i_UserName</td>
<td>$i_ClassName</td>
<td>$i_UserClassNumber</td>
<td>$i_SmartCard_Frontend_Take_Attendance_In_School_Time</td>
<td>$i_SmartCard_Frontend_Take_Attendance_In_School_Station</td>
<td>$i_SmartCard_Frontend_Take_Attendance_Status</td>
<td width=30%>$i_Attendance_Reason</td>
</tr>\n";


for($i=0; $i<sizeOf($result); $i++)
{
        list($record_id, $user_id, $name,$class_name,$class_number, $in_school_time, $in_school_station, $reason) = $result[$i];
        $str_status = $i_StudentAttendance_Status_Late;
        $str_reason = $reason;
        $x .= "<tr class=$css><td >".($i+1)."</td><td >$name</td><td>$class_name</td><td >$class_number</td><td>$in_school_time</td><td >$in_school_station</td><td >$str_status</td><td>$str_reason&nbsp;</td></tr>\n";
}
if (sizeof($result)==0)
{
    $x .= "<tr class=tableContent><td colspan=8 align=center>$i_StudentAttendance_NoLateStudents</td></tr>\n";
}
$x .= "</table>\n";
$table_attend = $x;

$i_title = "$i_SmartCard_DailyOperation_ViewLateStatus $TargetDate ($display_period)";
#$i_title = "$i_StudentAttendance_System ($i_StudentAttendance_Menu_DailyOperation)";
include_once("../../../../templates/fileheader.php");
echo $i_title;
echo $table_attend;
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>