<?php
// Editing by 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libgeneralsettings.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../includes/eclass_api/libeclass_api.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$libeclass_api = new libeclass_api();

$eClassAPIOpenUse = $libeclass_api->geteClassApiSettings($eClassAPIConfig['settings'][0]);
$eClassAPIPassword = $eClassAPIOpenUse == '1' ? $libeclass_api->geteClassApiSettings($eClassAPIConfig['settings'][1]) : '';

$display_css = $eClassAPIOpenUse == '1'? '':'style="display:none;"';

?>
<script src="../../templates/jquery/jquery-1.3.2.min.js" language="JavaScript"></script>
<script language="javascript">
function checkform(obj)
{
	 var open_use = $('input[name=eClassAPIOpenUse]:checked').val();
	 var password1 = $.trim(obj.eClassAPIPassword.value);
	 var password2 = $.trim(obj.eClassAPIPassword2.value);
	 var submit_ok = true;
	 
	 if(open_use == '1') {
	     if(!check_text(obj.eClassAPIPassword, "<?=$Lang['eClassAPI']['WarningMsg']['InputPassword']?>")) {
	     	 obj.eClassAPIPassword.focus();
	     	 submit_ok = false;
	     	 return false;
	     }
	     if(!check_text(obj.eClassAPIPassword2, "<?=$Lang['eClassAPI']['WarningMsg']['InputPassword']?>")) {
	     	 obj.eClassAPIPassword2.focus();
	     	 submit_ok = false;
	     	 return false;
	     }
	     if(!checkRegEx(obj.eClassAPIPassword.value,"<?= $Lang['eClassAPI']['WarningMsg']['InvalidPassword'] ?>")) {
	          obj.eClassAPIPassword.value = "";
	          obj.eClassAPIPassword2.value = "";
	          obj.eClassAPIPassword.focus();
	          submit_ok = false;
	          return false;
	     }
	     if(password1 != password2){
	          alert("<?php echo $i_frontpage_myinfo_password_mismatch; ?>");
	          obj.eClassAPIPassword.value = "";
		      obj.eClassAPIPassword2.value = "";
		      obj.eClassAPIPassword.focus();
		      submit_ok = false;
		      return false;
	     }
	 }
     if(submit_ok) {
		obj.submit();
     }
}

function togglePassword(isDisplay,displayClass)
{
	if(isDisplay) {
		$('.' + displayClass).show();
	}else{
		$('.' + displayClass).hide();
	}
}
</script>

<form name="form1" action="update.php" method="post" onsubmit="checkform(this);return false;">
<?= displayNavTitle($Lang['eClassAPI']['eClassApiSettings'], '') ?>
<?= displayTag("head_eclass_update_$intranet_session_language.gif", $msg) ?>
<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td>
			<blockquote>
			<p><br>
			<table border=0 cellpadding=5 cellspacing=0>
				<tr>
					<td><?=$Lang['eClassAPI']['OpenForUse']?>:</td>
					<td>
						<input type="radio" id="eClassAPIOpenUseY" name="eClassAPIOpenUse" value="1" onclick="togglePassword(this.checked,'classTogglerable');" <?=$eClassAPIOpenUse=='1'?'checked="checked"':''?> /><label for="eClassAPIOpenUseY"><?=$Lang['General']['Yes']?></label>
						&nbsp;<input type="radio" id="eClassAPIOpenUseN" name="eClassAPIOpenUse" value="0" onclick="togglePassword(!this.checked,'classTogglerable');" <?=$eClassAPIOpenUse!='1'?'checked="checked"':''?> /><label for="eClassAPIOpenUseN"><?=$Lang['General']['No']?></label>
					</td>
				</tr>
				<tr class="classTogglerable" <?=$display_css?>>
					<td><?=$Lang['eClassAPI']['ApiPassword']?>:</td>
					<td>
						<input class="password" type="password" name="eClassAPIPassword" value="<?=$eClassAPIPassword?>" size="10" maxlength="50" />
						<span class="extraInfo">(0-9a-zA-Z)</span>
					</td>
				</tr>
				<tr class="classTogglerable" <?=$display_css?>>
					<td><?=$Lang['eClassAPI']['RetypeApiPassword']?>:</td>
					<td>
						<input class="password" type="password" name="eClassAPIPassword2" value="<?=$eClassAPIPassword?>" size="10" maxlength="50" />
						<span class="extraInfo">(0-9a-zA-Z)</span>
					</td>
				</tr>
			</table>
			</blockquote>
			</p><br>
		</td>
	</tr>
	<tr>
		<td><hr size="1"></td>
	</tr>
	<tr>
		<td align="right">
		<?= btnSubmit() /* ." ". btnReset()*/ ?>
		</td>
	</tr>
</table>
</form>
<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>