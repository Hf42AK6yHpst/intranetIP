<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libcampustv.php");

$lcampustv = new libcampustv();

$public += 0;
$bulletin += 0;
$polling += 0;

$content = "$public\n$bulletin\n$polling";

$li = new libfilesystem();
$li->file_write(intranet_htmlspecialchars(trim(stripslashes($content))), $lcampustv->setting_file);

header("Location: settings.php?msg=2");
?>