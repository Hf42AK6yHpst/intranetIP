<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libcampustv.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$special_feature['portal'] = true;

$lcampustv = new libcampustv();
$LiveURL = $lcampustv->LiveURL;
$programmeList = $lcampustv->programmeList;

$list_table = "<table width=95% border=0 cellpadding=5 cellspacing=0>\n";
$list_table .= "<tr class=tableTitle_new><td align=center>$i_CampusTV_Time</td><td>&nbsp; </td><td align=center>$i_CampusTV_Programme</td></tr>\n";
for ($i=0; $i<sizeof($programmeList); $i++)
{
     list($range,$title) = $programmeList[$i];
     $list_table .= "<tr><td align=center><input type=text name=range[] value=\"$range\"></td><td>&nbsp; </td><td align=center><input type=text name=title[] value=\"$title\"></td></tr>\n";
}
$remaining = max(5,20-sizeof($programmeList));
for ($i=0; $i<$remaining; $i++)
{
     $list_table .= "<tr><td align=center><input type=text name=range[]></td><td>&nbsp; </td><td align=center><input type=text name=title[]></td></tr>\n";
}
$list_table .= "</table>\n";

?>
<script language="JavaScript">
function checkform(obj){
	<?php if ($special_feature['portal']) { ?>
	if (!obj.live_width.disabled &&
		(isNaN(parseInt(obj.live_width.value)) || parseInt(obj.live_width.value)<1 || parseInt(obj.live_width.value)>320))
	{
		alert("<?=$i_CampusTV_Live_Portal_size_width_warn?>");
		obj.live_width.focus();
		return false;
	}
	obj.live_width.value = parseInt(obj.live_width.value);
	if (!obj.live_height.disabled &&
		(isNaN(parseInt(obj.live_height.value)) || parseInt(obj.live_height.value)<1))
	{
		alert("<?=$i_CampusTV_Live_Portal_size_height_warn?>");
		obj.live_height.focus();
		return false;
	}
	obj.live_height.value = parseInt(obj.live_height.value);
	<?php } ?>

	return true;
}

function triggerSettings(myValue){
	var disabled = (!myValue);
	document.form1.live_width.disabled = disabled;
	document.form1.live_height.disabled = disabled;
}
</script>

<form name="form1" action="live_update.php" method="post" enctype="multipart/form-data">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_sc_campustv, 'index.php', $i_CampusTV_LiveBroadcastManagement, '') ?>
<?= displayTag("head_campustv_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p>
<span class="extraInfo">[<span class=subTitle><?=$i_CampusTV_LiveURL?></span>]</span>
<div> <input type=text size=80 name=LiveURL value='<?=$LiveURL?>'></div>
</p>
<?php if ($special_feature['portal']) 
{
	$live_show = $lcampustv->live_show;
	$live_width = $lcampustv->live_width;
	$live_height = $lcampustv->live_height;
	//$live_auto_play = $lcampustv->live_auto_play;
	$live_show_checked = ($live_show==1) ? "checked" : "";
	$live_setting_disabled = ($live_show==1) ? "" : "disabled";
	//$live_auto_play_checked = ($live_auto_play==1) ? "checked" : "";
	?>
<p>
<span class="extraInfo">[<span class=subTitle><?=$i_CampusTV_Live_Portal_Config?></span>]</span>
<div>
<table border=0 cellpadding=5 cellspacing=0>
<tr><td align=right nowrap><?=$i_CampusTV_Live_Portal_display?>:</td>
<td><input type=checkbox name=live_show <?=$live_show_checked?> onClick="triggerSettings(this.checked)" ></td></tr>
<tr><td align=right nowrap><?=$i_CampusTV_Live_Portal_size?>:</td>
<td><input class=text type=text name=live_width size=3 maxlength=3 value="<?=$live_width?>" <?=$live_setting_disabled?> >
<font face="arial">X</font>
<input class=text type=text name=live_height size=3 maxlength=3 value="<?=$live_height?>" <?=$live_setting_disabled?> > <?=$i_CampusTV_Live_Portal_pixel?> </td></tr>
<!--
<tr><td align=right nowrap><?=$i_CampusTV_Live_Portal_auto?>:</td>
<td><input type=checkbox name=live_auto_play <?=$live_auto_play_checked?> ></td></tr>
-->
</table></div>
</p>
<?php } ?>
<br>
<hr size=1 class="hr_sub_separator">
<p>
<span class="extraInfo">[<span class=subTitle><?=$i_CampusTV_LiveProgrammeList?></span>]</span>
<div>
<?=$list_table?>
</div>
</p>
<br>

</blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
</td>
</tr>
</table>

</form>


<?php
include_once("../../templates/adminfooter.php");
?>