<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$list = implode(",",$ClipID);
$sql = "UPDATE INTRANET_TV_MOVIECLIP SET Recommended = 2, DateModified = now() WHERE ClipID IN ($list)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: movieclip.php?msg=2");
?>