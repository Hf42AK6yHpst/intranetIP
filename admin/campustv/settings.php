<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libcampustv.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
include_once("../../lang/email.php");

intranet_opendb();

$lcampustv = new libcampustv();
$publicChecked = ($lcampustv->publicAllowed? "CHECKED":"");
$bulletinDisabled = ($lcampustv->bulletinDisabled? "CHECKED":"");
$pollingDisabled = ($lcampustv->pollingDisabled? "CHECKED":"");
$LiveURL = $lcampustv->LiveURL;

$main_url = "$website/home/plugin/campustv/";

?>
<form name="form1" action="update.php" method="post">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_sc_campustv, 'index.php', $i_CampusTV_BasicSettings, '') ?>
<?= displayTag("head_campustv_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>

<p><br>
<table border=0 cellpadding=5 cellspacing=0>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_CampusTV_AccessByPublic?><br>
<?=$i_CampusTV_URL?>: <a target=_blank href="<?=$intranet_httppath?>/home/plugin/campustv/"><?=$main_url?></a></td><td align=center><input type=checkbox name=public value=1 <?=$publicChecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_CampusTV_DisableBulletin?></td><td align=center><input type=checkbox name=bulletin value=1 <?=$bulletinDisabled?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_CampusTV_DisablePolling?></td><td align=center><input type=checkbox name=polling value=1 <?=$pollingDisabled?>></td></tr>
</table>


</blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
</td>
</tr>
</table>

</form>


<?php
include_once("../../templates/adminfooter.php");
?>