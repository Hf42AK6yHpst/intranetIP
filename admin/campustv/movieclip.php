<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdbtable.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

intranet_opendb();
# TABLE SQL
$keyword = trim($keyword);

if ($status != 1) $status = 2;
if ($filter != "")
{
    $conds = "AND a.RecordType = $filter";
}
else
{
    $conds = "";
}

$fieldname = getNameFieldWithClassNumberByLang("c.");
$sql = "SELECT
              CONCAT(a.Title,IF(a.IsSecure=1,'<br>[SECURE]',CONCAT(' <br><a target=_blank href=\\'',a.ClipURL,'\\'><img alt=\\'$i_CampusTV_Play\\' border=0 src=$image_path/btn_next.gif></a>'))),
              a.Description,
              IF(a.Recommended='2','$i_CampusTV_RecommendedStatus',''),
              IF(c.UserID IS NULL,'$i_general_sysadmin',$fieldname) as UserName,
              b.Title, a.DateModified,
              CONCAT('<input type=checkbox name=ClipID[] value=',a.ClipID,'>')
        FROM INTRANET_TV_MOVIECLIP as a
             LEFT OUTER JOIN INTRANET_USER as c ON a.UserID = c.UserID
             LEFT OUTER JOIN INTRANET_TV_CHANNEL as b ON a.RecordType = b.ChannelID
        WHERE a.RecordStatus = '$status' AND
              (a.Title like '%$keyword%' OR
               a.Description like '%$keyword%'
               ) $conds";
#echo $sql;
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.Title","a.Description","a.Recommended","UserName","b.Title","a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0);
$li->IsColOff = 2;
// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(0, $i_CampusTV_ClipName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(1, $i_general_description)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(2, $i_CampusTV_Recommended)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(3, $i_CampusTV_MovieProvider)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(4, $i_CampusTV_ChannelName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(5, $i_LastModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("ClipID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=\"javascript:checkNew('clip_new.php')\">".newIcon()."$button_new</a>";
$functionbar= "";
$functionbar .= ($status==2) ? "<a href=\"javascript:checkSuspend(document.form1,'ClipID[]','suspend.php')\"><img src='/images/admin/button/t_btn_pend_$intranet_session_language.gif' border='0' align='absmiddle'></a>" : "<a href=\"javascript:checkApprove(document.form1,'ClipID[]','approve.php')\"><img src='/images/admin/button/t_btn_approve_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'ClipID[]','clip_edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'ClipID[]','clip_remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$functionbar .= "<a href=\"javascript:checkAlert(document.form1,'ClipID[]','clip_recommend.php','$i_CampusTV_Alert_Recommend')\"><img src='/images/admin/button/t_btn_recommend_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$functionbar .= "<a href=\"javascript:checkAlert(document.form1,'ClipID[]','clip_cancel_recommend.php','$i_CampusTV_Alert_CancelRecommend')\"><img src='/images/admin/button/t_btn_cancel_recommend_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar  = "<select name=status onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value=1 ".(($status==1)?"selected":"").">$i_status_pending</option>\n";
$searchbar .= "<option value=2 ".(($status==2)?"selected":"").">$i_status_approved</option>\n";
$searchbar .= "</select>\n";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_sc_campustv, 'index.php', $i_CampusTV_VideoManagement, '') ?>
<?= displayTag("head_campustv_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>


<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>