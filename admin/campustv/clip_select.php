<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdbtable.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

intranet_opendb();
# TABLE SQL
$keyword = trim($keyword);

$status = 2;
if ($filter != "")
{
    $conds = "AND a.RecordType = $filter";
}
else
{
    $conds = "";
}

$fieldname = getNameFieldWithClassNumberByLang("c.");
$sql = "SELECT
              CONCAT(a.Title,' <br><a target=_blank href=',a.ClipURL,'><img alt=\\'$i_CampusTV_Play\\' border=0 src=$image_path/btn_next.gif></a>'),
              a.Description,
              IF(a.Recommended='2','$i_CampusTV_Recommended',''),
              IF(c.UserID IS NULL,'$i_general_sysadmin',$fieldname) as UserName,
              b.Title, a.DateModified,
              CONCAT('<input type=radio name=ClipID value=',a.ClipID,'>')
        FROM INTRANET_TV_MOVIECLIP as a
             LEFT OUTER JOIN INTRANET_USER as c ON a.UserID = c.UserID
             LEFT OUTER JOIN INTRANET_TV_CHANNEL as b ON a.RecordType = b.ChannelID
        WHERE a.RecordStatus = '$status' AND
              (a.Title like '%$keyword%' OR
               a.Description like '%$keyword%'
               ) $conds";
#echo $sql;
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.Title","a.Description","a.Recommended","UserName","b.Title","a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0);
$li->IsColOff = 2;
// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(0, $i_CampusTV_ClipName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(1, $i_general_description)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(2, $i_CampusTV_Recommended)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(3, $i_CampusTV_MovieProvider)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(4, $i_CampusTV_ChannelName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(5, $i_LastModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>"."</td>\n";

// TABLE FUNCTION BAR
$functionbar= "";
$functionbar .= "<a href=\"javascript:document.form1.action='clip_select_update.php';document.form1.method='POST';document.form1.submit()\"><img src='/images/admin/button/s_btn_selectmovie_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/s_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

?>
<script language="JavaScript1.2">
</script>

<form name=form1 action='' method=get>

<p style="padding-left:20px">
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
<tr><td style="background-image: url(../../../images/admin/pop_bg.gif);" >
<table width=422 border=0 cellpadding=10 cellspacing=0>
<tr><td><?=$i_CampusTV_SelectClip?></td></tr>
</table>
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_bar.gif" width=422 height=16 border=0></td></tr>
<tr><td ><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td ><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
</table>
<table width=422 border=0 cellpadding=10 cellspacing=0>
<tr><td align=center>
<?php echo $li->display(400); ?>
</td>
</tr>
</table>
</td></tr>
<tr><td><img src="../../../images/admin/pop_bottom.gif" width=422 height=18 border=0></td></tr>
<tr><td align=center height="40" style="vertical-align:bottom">
<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
<input type=hidden name=pos value='<?=$pos?>'>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/filefooter.php");
?>