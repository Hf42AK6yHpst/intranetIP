<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libcampustv.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$default_num = 10;

$start = date("Y-m-d");
$end = date("Y-m-d",mktime(0,0,0,date("m"),date("d")+7,date("Y")));

?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.PollingName, "<?php echo $i_alert_pleasefillin.$i_CampusTV_PollingName; ?>.")) return false;
}
</script>

<form name="form1" action="polling_new2.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_sc_campustv, 'index.php', $i_CampusTV_PollingManagement, 'polling.php', $button_new, '') ?>
<?= displayTag("head_campustv_$intranet_session_language.gif", $msg) ?>


<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_general_startdate; ?>:</td><td><input class=text type=text name=DateStart size=10 value='<?=$start?>'> (YYYY-MM-DD)</td></tr>
<tr><td align=right><?php echo $i_general_enddate; ?>:</td><td><input class=text type=text name=DateEnd size=10 value='<?=$end?>'> (YYYY-MM-DD)</td></tr>
<tr><td align=right><?php echo $i_CampusTV_PollingName; ?>:</td><td><input class=text type=text name=PollingName size=60 maxlength=200></td></tr>
<tr><td align=right><?php echo $i_CampusTV_Reference; ?>:</td><td><TEXTAREA name=Reference rows=5 cols=60></TEXTAREA></td></tr>
<tr><td align=right><?php echo $i_CampusTV_NumClips; ?>:</td><td><input class=text type=text name=num size=5 maxlength=10 value='<?=$default_num?>'></td></tr>
</table>
<br><br>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>