<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libcampustv.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$PollingName = intranet_htmlspecialchars($PollingName);
$Reference = intranet_htmlspecialchars($Reference);

$default_num = 10;
if (!is_numeric($num)) $num = $default_num;
?>
<SCRIPT LANGUAGE=Javascript>
function openSelection(pos)
{
         newWindow('clip_select.php?pos='+pos,2);
}
</SCRIPT>
<form name="form1" action="polling_new_update.php" enctype="multipart/form-data" method="post">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_sc_campustv, 'index.php', $i_CampusTV_PollingManagement, 'polling.php', $button_new, '') ?>
<?= displayTag("head_campustv_$intranet_session_language.gif", $msg) ?>


<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=center colspan=2><?=$i_CampusTV_PollingSelectClips?></td></tr>
<?
for ($i=0; $i<$num; $i++) {
?>
<tr><td align=right><?php echo $i_CampusTV_PollingCandidate." ".($i+1); ?>:</td><td><input class=text type=text name=Polling<?=$i?> size=50 disabled>
<a href=javascript:openSelection(<?=$i?>)><img border=0 src="<?=$image_path?>/admin/button/s_btn_selectmovie_<?=$intranet_session_language?>.gif"  alt='<?=$i_CampusTV_SelectClip?>'></a>
<input type=hidden name=PollID<?=$i?>>
</td></tr>
<? } ?>
</table>
<br><br>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=PollingName value='<?=$PollingName?>'>
<input type=hidden name=Reference value='<?=$Reference?>'>
<input type=hidden name=DateStart value='<?=$DateStart?>'>
<input type=hidden name=DateEnd value='<?=$DateEnd?>'>
<input type=hidden name=num value='<?=$num?>'>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>