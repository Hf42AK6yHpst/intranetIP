<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$list = implode(",",$ClipID);
$sql = "SELECT FilePath, FileName, IsSecure FROM INTRANET_TV_MOVIECLIP WHERE isFile = 2 AND ClipID IN ($list)";
$files = $li->returnArray($sql,3);

if (sizeof($files)!=0)
{
    include_once("../../includes/libftp.php");
    $lftp = new libftp();
    $lftp->host = $tv_ftp_host;
    if ($tv_host_type == "") $tv_host_type = "Windows";
    $lftp->host_type = $tv_host_type; #"Windows";
    
    #$lftp->host_type = "Windows";
    if (!$lftp->connect($tv_ftp_user,$tv_ftp_passwd))
    {
         header("Location: index.php?error=1");
         exit();
    }
    $home = $lftp->pwd();

    $secure_home = "";
    if ($tv_secure_ftp_user != "")
    {
        $secure_lftp = new libftp();
        $secure_lftp->host = $tv_ftp_host;
        $secure_lftp->host_type = $lftp->host_type;
        if ($secure_lftp->connect($tv_secure_ftp_user,$tv_secure_ftp_passwd))
        {
            $secure_home = $secure_lftp->pwd();
        }

    }

    for ($i=0; $i<sizeof($files); $i++)
    {
         list($path,$file,$secure) = $files[$i];
         if ($secure == 1 && $secure_home != "")
         {
             if ($file != "")
             {
                 if ($path != "")
                 {
                     $path = "$secure_home/$path";
                 }
                 else $path = $secure_home;
                 $secure_lftp->remove($path,$file);
             }
             if ($path != $secure_home)
             {
                 $secure_lftp->rmdir($path);
             }
         }
         else
         {
             if ($file != "")
             {
                 if ($path != "")
                 {
                     $path = "$home/$path";
                 }
                 else $path = $home;
                 $lftp->remove($path,$file);
             }
             if ($path != $home)
             {
                 $lftp->rmdir($path);
             }
         }
    }

}


$sql = "DELETE FROM INTRANET_TV_MOVIECLIP WHERE ClipID IN ($list)";

$li->db_db_query($sql);

intranet_closedb();
header("Location: movieclip.php?msg=3");
?>