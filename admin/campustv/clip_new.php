<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libcampustv.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$lcampustv = new libcampustv();
$channels = $lcampustv->returnChannels();
$channel_select = getSelectByArray($channels,"name=ChannelID","",0,1);

?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.ClipName, "<?php echo $i_alert_pleasefillin.$i_CampusTV_ClipName; ?>.")) return false;
        if(obj.ClipType[0].checked)
           if(!check_text(obj.ClipURL, "<?php echo $i_alert_pleasefillin.$i_CampusTV_MovieURL; ?>.")) return false;
}
</script>

<form name="form1" action="clip_new_update.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_sc_campustv, 'index.php', $i_CampusTV_VideoManagement, 'movieclip.php', $button_new, '') ?>
<?= displayTag("head_campustv_$intranet_session_language.gif", $msg) ?>


<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_CampusTV_ClipName; ?>:</td><td><input class=text type=text name=ClipName size=60 maxlength=200></td></tr>
<tr><td align=right><?php echo $i_general_description; ?>:</td><td><TEXTAREA name=Description rows=5 cols=60> </TEXTAREA></td></tr>
<tr><td align=right><?php echo $i_CampusTV_ChannelName; ?>:</td><td><?=$channel_select?></td></tr>
</table>
<br>
<hr size=1 class="hr_sub_separator">
<p>
<span class="extraInfo">[<span class=subTitle><?=$i_CampusTV_InputType?></span>]</span>
<div><input type=radio name=ClipType value=1 CHECKED ><span class=tableColor2><?=$i_CampusTV_Type_Link?></span>
<BLOCKQUOTE>
<?=$i_CampusTV_MovieURL?>:<br><input type=text name=ClipURL size=60>
</BLOCKQUOTE>
</div>
<div><input type=radio name=ClipType value=2 ><span class=tableColor2><?=$i_CampusTV_Type_File?></span></div>
<BLOCKQUOTE>
<?=$i_CampusTV_SelectMovieFile?>:<br><input type=file name=ClipFile size=60><br>
<? if ($tv_secure_ftp_user != "") {
?>
<?=$i_CampusTV_SecureNeeded?>: <input type=checkbox name=secure value=1>
<? } ?>
</BLOCKQUOTE>
</p>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>