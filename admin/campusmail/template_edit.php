<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libcampusmail.php");
include("../../includes/libfiletable.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();

$UserID = 0;
$li = new libcampusmail($CampusMailID);
$row = $li->returnRecipientOption($li->RecipientID);
$path = "$file_path/file/mail/".$li->Attachment;
$lu = new libfilesystem();
$lu->folder_new($path);
$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
if ($special_feature['imail_richtext'])
{
    $use_html_editor = true;
}
?>

<script language="javascript">
function checkform(obj){
     if(obj.elements["Recipient[]"].length==0){ alert("<?php echo $i_frontpage_campusmail_choose; ?>"); return false; }
     if(!check_text(obj.Subject, "<?php echo $i_alert_pleasefillin.$i_frontpage_campusmail_subject; ?>.")) return false;
     if(!check_text(obj.Message, "<?php echo $i_alert_pleasefillin.$i_frontpage_campusmail_message; ?>.")) return false;
     checkOptionAll(obj.elements["Recipient[]"]);
     checkOptionAll(obj.elements["Attachment[]"]);
}

function submitMail(num){
        var obj = document.form1;
        checkOption(obj.elements["Attachment[]"]);
        checkOption(obj.elements["Recipient[]"]);
        obj.SubmitType.value = num;
}
</script>

<form name="form1" action="template_update2.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_admintitle_im, '/admin/info/', $i_admintitle_im_campusmail, 'index.php', $button_edit, '') ?>
<?= displayTag("head_campusmail_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table border=0 cellpadding=5 cellspacing=0>
<tr><td nowrap align=right><?php echo $i_frontpage_campusmail_recipients; ?>:</td><td><select name=Recipient[] size=4 multiple><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select></td><td width="50%">
<a href="javascript:newWindow('choose/index.php?fieldname=Recipient[]',2)"><img src="/images/admin/button/s_btn_choose_recipient_<?=$intranet_session_language?>.gif" border="0"></a>
<br>
<a href="javascript:checkOptionRemove(document.form1.elements['Recipient[]'])"><img src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
<tr><td nowrap align=right><?php echo $i_frontpage_campusmail_subject; ?>:</td><td colspan=2><input class=text type=text name=Subject size=60 maxlength=255 value="<?php echo $li->Subject; ?>"></td></tr>
<tr><td nowrap align=right><?php echo $i_frontpage_campusmail_message; ?>:</td><td colspan=2>
<? if ($use_html_editor) {
                   # Components size
                   $msg_box_width = "500";
                   $msg_box_height = 320;
                       $obj_name = "Message";
                       $editor_width = $msg_box_width;
                       $editor_height = $msg_box_height;
                       $preset_message = $li->Message;
                       if ($li->isHTMLMessage($preset_message))
                       {
                           $init_html_content = $preset_message;
                       }
                       else
                       {
                           $init_html_content = "<p>".nl2br($preset_message);
                       }
                       include("../../includes/html_editor_embed.php");
                    } else { ?>
                      <textarea name=Message cols=60 rows=10><?php echo $li->Message; ?></textarea>
                   <? } ?>
</td></tr>
<tr><td align=right><?php echo $i_frontpage_campusmail_attachment; ?>:</td><td><select name=Attachment[] size=4 multiple><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select></td><td>
<a href="javascript:newWindow('attach.php?isdraftedit=1&folder=<?php echo $li->Attachment; ?>',2)"><img src="/images/admin/button/s_btn_add_attachment_<?=$intranet_session_language?>.gif" border="0"></a>
<br>
<a href="javascript:checkOptionRemove(document.form1.elements['Attachment[]'])"><img src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
<tr><td nowrap align=right><?php echo $i_frontpage_campusmail_important; ?>:</td><td colspan=2><input type=checkbox name=IsImportant value=1 <?php if($li->IsImportant) { echo "CHECKED"; } ?>></td></tr>
<tr><td nowrap align=right><?php echo $i_frontpage_campusmail_notification; ?>:</td><td colspan=2><input type=checkbox name=IsNotification value=1 <?php if($li->IsNotification) { echo "CHECKED"; } ?>></td></tr>

<!--
<tr><td colspan=3><br></td></tr>
<tr><td><br></td><td colspan=2><input class=submit type=submit value="<?php echo $button_send; ?>" onClick=checkOption(this.form.elements["Attachment[]"]);checkOption(this.form.elements["Recipient[]"]);this.form.SubmitType.value=0;><input class=submit type=submit value="<?php echo $button_save; ?>" onClick=checkOption(this.form.elements["Attachment[]"]);checkOption(this.form.elements["Recipient[]"]);this.form.SubmitType.value=1;><input class=reset type=reset value="<?php echo $button_reset; ?>"></td></tr>
-->

</table>
</blockquote>

<input type=hidden name=CampusMailID value="<?php echo $CampusMailID; ?>">
<input type=hidden name=SubmitType value=0>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_send_<?=$intranet_session_language?>.gif" border='0' onClick="submitMail(0)">
 <input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0' onClick="submitMail(1)">
 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<script language="JavaScript1.2">
obj = document.form1.elements["Attachment[]"];
checkOptionClear(obj);
<?php while (list($key, $value) = each($files)) echo "checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n";  ?>
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
obj = document.form1.elements["Recipient[]"];
checkOptionClear(obj);
<?php for($i=0; $i<sizeof($row); $i++){ echo "checkOptionAdd(obj, \"".$row[$i][1]."\", \"".$row[$i][0]."\");\n"; } ?>
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
</script>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>