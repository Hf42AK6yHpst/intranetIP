<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libfilesystem.php");
intranet_opendb();

$li = new libdb();

# Step 1: Extract valid mail IDs
$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = 0 AND CampusMailID IN (".implode(",", $CampusMailID).")";
$targetID = $li->returnVector($sql);

if (sizeof($targetID)!=0)
{
    $CampusMailIDStr = implode(",",$targetID);
    # Step 2: Extract Attachment paths
    $sql = "SELECT Attachment FROM INTRANET_CAMPUSMAIL WHERE MailType = 1 AND IsAttachment = 1 AND Deleted = 1 AND Attachment != '' AND CampusMailID IN ($CampusMailIDStr)";
    $to_remove_attachmentpaths_int = $li->returnVector($sql);
    
    # Step 3: Remove reply records
    # Remove the replies for notification mail sent
    $sql = "DELETE FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID IN (".$CampusMailIDStr.")";
    $li->db_db_query($sql);
    
    # Step 4: Remove Mail DB records
    $sql = "DELETE FROM INTRANET_CAMPUSMAIL WHERE Deleted = 1 AND CampusMailID IN ($CampusMailIDStr)";
    $li->db_db_query($sql);
    
    # Step 5: Update Notification
    $sql = "UPDATE INTRANET_CAMPUSMAIL SET IsNotification = '0' WHERE CampusMailFromID IN (".$CampusMailIDStr.")";
    $li->db_db_query($sql);
    
    # Step 6: Search for attachment path (Internal)
    $lf = new libfilesystem();
    unset($list_path_string_to_remove);
    $delim = "";
    for ($i=0; $i<sizeof($to_remove_attachmentpaths_int); $i++)
    {
         $target_path = $to_remove_attachmentpaths_int[$i];
         if ($target_path != "")
         {
             $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE Attachment = '$target_path'";
             $temp = $li->returnVector($sql);
             if (is_array($temp) && sizeof($temp)==1 && $temp[0]==0)
             {
                 # Remove Database records
                 $list_path_string_to_remove .= "$delim'$target_path'";
                 $delim = ",";

                 # Remove actual files
                 $full_path = "$file_path/file/mail/".$target_path;
                 $lf->lfs_remove($full_path);
             }

         }
    }
    $benchmark['step6 ends'] = time();

    # Step 7 : Remove Database Records
    if ($list_path_string_to_remove != "")
    {
        $sql = "DELETE FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE AttachmentPath IN ($list_path_string_to_remove)";
        $li->db_db_query($sql);
    }
    $benchmark['step7 ends'] = time();
    ############################################################

}

intranet_closedb();
header("Location: trash.php");
?>