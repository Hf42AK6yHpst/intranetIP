<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libcampusmail.php");
include("../../includes/libfilesystem.php");
include("../../includes/libfiletable.php");
intranet_opendb();

$UserID = 0;

$Subject = intranet_htmlspecialchars(trim($Subject));
#$Message = intranet_htmlspecialchars(trim($Message));
$Recipient = array_unique($Recipient);
$Recipient = array_values($Recipient);
$RecipientID = implode(",", $Recipient);

$li = new libcampusmail($CampusMailID);

$actual_recipient_array = $li->returnRecipientUserIDArray($RecipientID);
if (sizeof($actual_recipient_array)==0)
{
    header("Location: template_edit.php?CampusMailID=$CampusMailID&cmsg=1");
    exit();
}

$AttachmentStr = (sizeof($Attachment)==0) ? "" : implode(",", $Attachment);
$path = "$file_path/file/mail/".$li->Attachment;
$lo = new libfiletable("", $path, 0, 0, "");
$lu = new libfilesystem();
$files = $lo->files;
while (list($key, $value) = each($files)) {
     if(!strstr($AttachmentStr,$files[$key][0])){
          $lu->file_remove($path."/".$files[$key][0]);
     }
}


$IsAttachment = (isset($Attachment)) ? 1 : 0;
$RecordType = $SubmitType;

$sql = "UPDATE INTRANET_CAMPUSMAIL SET UserFolderID = $RecordType, RecipientID = '$RecipientID', Subject = '$Subject', Message = '$Message', IsAttachment = '$IsAttachment', IsImportant  = '$IsImportant', IsNotification = '$IsNotification', RecordType = $RecordType ,DateModified = now() WHERE CampusMailID = $CampusMailID AND UserID = 0";
$li->db_db_query($sql);

if($SubmitType==0){
/*
     $sql = "INSERT INTO INTRANET_CAMPUSMAIL (UserID, SenderID, RecipientID, Subject, Message, Attachment, IsAttachment, IsImportant, IsNotification, RecordType, DateInput, DateModified) VALUES ($UserID, $UserID, '$RecipientID', '$Subject', '$Message', '".$li->Attachment."', '$IsAttachment', '$IsImportant', '$IsNotification', '$RecordType', now(), now())";
     $li->db_db_query($sql);
     $CampusMailFromID = $li->db_insert_id();
*/
     # return all recipients UserID
     # $sql = "INSERT INTO INTRANET_CAMPUSMAIL (CampusMailFromID, UserID, SenderID, RecipientID, Subject, Message, Attachment, IsAttachment, IsImportant, IsNotification, RecordType, DateInput, DateModified) VALUES ";
     $campusmail_sql_header = "INSERT INTO INTRANET_CAMPUSMAIL (CampusMailFromID, UserID, RecordType,RecordStatus) VALUES ";
     $replies_sql_header = "INSERT INTO INTRANET_CAMPUSMAIL_REPLY (CampusMailID, UserID, UserName,IsRead,DateInput,DateModified) VALUES ";
     $sql = $campusmail_sql_header;
     $row = $actual_recipient_array;
     $replies_values = "";
     $delimiter = "";
     for($i=0; $i<sizeof($row); $i++){
          $ReceiverID = $row[$i][0];
          $ReceiverName = $row[$i][1];
          $ReceiverType = $row[$i][2];
          # $sql .= "$delimiter($CampusMailID, $ReceiverID, $UserID, '$RecipientID', '$Subject', '$Message', '".$li->Attachment."', '$IsAttachment', '$IsImportant', '$IsNotification', '2', now(), now())";
          $sql .= "$delimiter($CampusMailID, $ReceiverID, '2',1)";
          $replies_values .= "$delimiter($CampusMailID,$ReceiverID,'$ReceiverName',0,now(),now())";
          $delimiter = ",";
     }
     $li->db_db_query($sql);
     # Update all mails just inserted
     $sql = "UPDATE INTRANET_CAMPUSMAIL SET
             SenderID = $UserID, RecipientID = '$RecipientID',Subject='$Subject',Message='$Message'
             ,Attachment='".$li->Attachment."', IsAttachment='$IsAttachment',
             UserFolderID = 2
             ,IsImportant='$IsImportant', IsNotification = '$IsNotification',RecordStatus = NULL
             ,AttachmentSize = '0', DateInput = now(), DateModified = now()
             WHERE CampusMailFromID = $CampusMailID";
     $li->db_db_query($sql);
     if ($IsNotification)
     {
         $replies_sql = "$replies_sql_header $replies_values";
         $li->db_db_query($replies_sql);
     }

     #### Store Attachment details to DB
     if ($IsAttachment)
     {
         # compute size of attachment
         $lsize = new libfiletable("",$path, 0,0,"");
         $files = $lsize->files;
         $size = 0;
         $values = "";
         $delim = "";
         while (list($key, $value) = each($files)) {
                list($filename, $filesize) = $files[$key];
                $filename = addslashes($filename);
                $filesize = ceil($filesize/1000);
                $values .= "$delim($CampusMailID,'".$li->Attachment."', '$filename', '$filesize')";
                $delim = ",";
         }
         $sql = "INSERT IGNORE INTO INTRANET_IMAIL_ATTACHMENT_PART (CampusMailID, AttachmentPath, FileName, FileSize)
                        VALUES $values";
         $li->db_db_query($sql);
     }
     /*
     $sql = "UPDATE INTRANET_CAMPUSMAIL SET Attachment = '".session_id().".".time()."' WHERE CampusMailID = $CampusMailID AND UserID = $UserID";
     $li->db_db_query($sql);
     */
}

$url = ($SubmitType==0) ? "outbox.php?msg=1" : "template.php?msg=2";

intranet_closedb();
header("Location: $url");
?>