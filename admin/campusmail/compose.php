<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libfilesystem.php");
include("../../includes/libfiletable.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();

if(!session_is_registered("composeFolder_admin") || $composeFolder_admin==""){
     session_register("composeFolder_admin");
     $composeFolder_admin = session_id().".".time();
}

$li = new libfilesystem();
$personal_path = "$file_path/file/mail/u0";
if (!is_dir($personal_path))
{
    $li->folder_new($personal_path);
}
$path = "$personal_path/$composeFolder_admin"."tmp";
#$path = "$file_path/file/mail/$composeFolder_admin"."tmp";
$li->folder_new($path);
$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;

if ($special_feature['imail_richtext'])
{
    $use_html_editor = true;
}

?>

<script language="javascript">
function checkform(obj){
     if(obj.elements["Recipient[]"].length==0){ alert("<?php echo $i_frontpage_campusmail_choose; ?>"); return false; }
     if(!check_text(obj.Subject, "<?php echo $i_alert_pleasefillin.$i_frontpage_campusmail_subject; ?>.")) return false;
     
     objHiddenText=obj.hidden_text;
     msg = objHiddenText==null?obj.Message.value:editor_getText('Message');
	
     if(Trim(msg)==""){
	     alert('<?=$i_alert_pleasefillin.$i_frontpage_campusmail_message?>');
	     return false;
	 }

     checkOptionAll(obj.elements["Recipient[]"]);
     checkOptionAll(obj.elements["Attachment[]"]);
     return true;
}

function submitMail(num){
        var obj = document.form1;
        checkOption(obj.elements["Attachment[]"]);
        checkOption(obj.elements["Recipient[]"]);
        obj.SubmitType.value = num;
        if(checkform(obj))
        	obj.submit();
}
</script>

<form name="form1" action="compose_update2.php" method="post">
<?= displayNavTitle($i_adminmenu_adm, '', $i_admintitle_im, '/admin/info/', $i_admintitle_im_campusmail, 'javascript:history.back()', $i_admintitle_im_campusmail_compose, '') ?>
<?= displayTag("head_campusmail_$intranet_session_language.gif", $msg) ?>


<blockquote>
<table border=0 cellpadding=5 cellspacing=0>
<tr><td nowrap align=right><?php echo $i_frontpage_campusmail_recipients; ?>:</td><td><select name=Recipient[] size=4 multiple><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select></td><td width="50%">
<a href="javascript:newWindow('choose/index.php?fieldname=Recipient[]',2)"><img src="/images/admin/button/s_btn_choose_recipient_<?=$intranet_session_language?>.gif" border="0"></a>
<br>
<a href="javascript:checkOptionRemove(document.form1.elements['Recipient[]'])"><img src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
<tr><td nowrap align=right><?php echo $i_frontpage_campusmail_subject; ?>:</td><td colspan=2><input class=text type=text name=Subject size=60 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_frontpage_campusmail_message; ?>:</td><td colspan=2>
<? if ($use_html_editor) {
                   # Components size
                   $msg_box_width = "500";
                   $msg_box_height = 320;
                       $obj_name = "Message";
                       $editor_width = $msg_box_width;
                       $editor_height = $msg_box_height;
                       $init_html_content = "";
                       include("../../includes/html_editor_embed.php");
                    } else { ?>
                      <textarea name=Message cols=60 rows=10 ></textarea>
                   <? } ?>
</td></tr>
<tr><td nowrap align=right><?php echo $i_frontpage_campusmail_attachment; ?>:</td><td><select name=Attachment[] size=4 multiple><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select></td><td>
<a href="javascript:newWindow('attach.php?folder=<?="u0/$composeFolder_admin"."tmp"?>',2)"><img src="/images/admin/button/s_btn_add_attachment_<?=$intranet_session_language?>.gif" border="0"></a>
<br>
<a href="javascript:checkOptionRemove(document.form1.elements['Attachment[]'])"><img src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
<tr><td nowrap align=right><?php echo $i_frontpage_campusmail_important; ?>:</td><td colspan=2><input type=checkbox name=IsImportant value=1 CHECKED></td></tr>
<tr><td nowrap align=right><?php echo $i_frontpage_campusmail_notification; ?>:</td><td colspan=2><input type=checkbox name=IsNotification value=1 CHECKED></td></tr>
</table>
</blockquote>
<input type=hidden name=SubmitType value=0>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<? 
/* 
 <input type="image" src="/images/admin/button/s_btn_send_<?=$intranet_session_language?>.gif" border='0' onClick="submitMail(0)">
 <input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0' onClick="submitMail(1)">
*/
?>
 <a href='javascript:submitMail(0)'><img src="/images/admin/button/s_btn_send_<?=$intranet_session_language?>.gif" border='0'></a>
 <a href='javascript:submitMail(1)'><img src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'></a>

 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<script language="JavaScript1.2">
obj = document.form1.elements["Attachment[]"];
checkOptionClear(obj);
<?php while (list($key, $value) = each($files)) echo "checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n";  ?>
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
obj = document.form1.elements["Recipient[]"];
checkOptionClear(obj);
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
</script>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>