<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libteaching.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader_admin.php");
intranet_opendb();

$lteaching = new libteaching();
if($CIDList!="")
{
	$CIDArr = explode(",", $CIDList);
}

$sql = "SELECT 
			ClassID, 
			ClassName 
		FROM 
			INTRANET_CLASS 
		WHERE 
			RecordStatus = 1
		ORDER BY 
			ClassName
		";
$ClassArray = $lteaching->returnArray($sql,2);

$ContentTable = "<table width=422 border=0 cellpadding=3 cellspacing=0>";
$num = sizeof($ClassArray);
for($i=0; $i<$num; $i++)
{
	list($cid, $cname) = $ClassArray[$i];
	if($i%3==0)
	{
		$ContentTable .= "<tr>";
	}
	$CheckedStr = (is_array($CIDArr) && in_array($cid, $CIDArr)) ? "CHECKED='CHECKED'" : "";
	$ContentTable .= "<td width='33%'><input type=checkbox name='class_id' id='$i' value='$cid' $CheckedStr>&nbsp;<label for='$i'>".$cname."</label></td>";
	$ContentTable .= "<input type='hidden' name='class_name$i' value='".$cname."' />";
	if($i%3==2)
	{
		$ContentTable .= "</tr>";
	}
}
$ContentTable .= "</table>";
?>
<script language="javascript">
function jSUBMIT_RECORD()
{
	var obj = document.form1;
	var objLen = obj.length;

	var cid_array = new Array();
	var cname_array = new Array();
	var count = 0, pos = 0;
	
	for (var i = 0; i < objLen; i++)
    {
		if (obj.elements[i].type=="checkbox" && obj.elements[i].name=="class_id")
        {
			if(obj.elements[i].checked==true)
			{
				cid_array[count] = obj.elements[i].value;
				cname_array[count] = eval("obj.class_name"+pos+".value");
				count++;
			}
			pos++;
        }
    }
	cid_value = cid_array.join(",");
	cname_value = cname_array.join(",");
	
	window.opener.jADD_CLASSES('<?=$ID?>', cid_value, cname_value);
	self.close();
}
</script>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_fs, '', $i_ReportCard_System, 'index.php', $eReportCard["TeachingAppointmentSettings"], 'teaching.php', $button_edit, '') ?>

<p style="padding-left:20px" align='center'>
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
<tr><td style="background-image: url(../../images/admin/pop_bg.gif);">
<?=$ContentTable?>
</td></tr>
<tr><td><img src="../../images/admin/pop_bottom.gif" width=422 height=18 border=0></td></tr>
<tr><td align=center height="40" style="vertical-align:bottom">
<a href="javascript:jSUBMIT_RECORD()"><img src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border="0" /></a>
&nbsp;<?= btnReset() ?>&nbsp;
<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0" /></a>
</td></tr>
</table>

</p>
</form>

<?php
intranet_closedb();
include("../../templates/adminfooter_popup.php");
?>