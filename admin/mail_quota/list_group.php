<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/liblinux.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$lgroup = new libgroup($GroupID);


?>
<?= displayNavTitle($i_admintitle_fs, '', $i_LinuxAccountQuotaSetting, 'index.php',$i_LinuxAccount_DisplayQuota,'list.php',$lgroup->Title,'') ?>
<?= displayTag("head_storagequota_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td class=tableTitle><?=$i_UserName?></td>
<td class=tableTitle><?=$i_LinuxAccount_Quota?> (Mbytes)</td>
<td class=tableTitle><?=$i_LinuxAccount_UsedQuota?> (Mbytes)</td>
</tr>
<?
# Retrieve Quota
$llinux = new liblinux();
$list = $llinux->getQuotaTable();
for ($i=0; $i<sizeof($list); $i++)
{
     list($login,$delimiter,$used,$soft,$hard) = $list[$i];
     $quota[$login] = array($used,$soft);
}
$name_field = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT a.UserID, b.UserLogin, $name_field
        FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
        WHERE a.GroupID = $GroupID AND b.UserID IS NOT NULL
        ORDER BY b.ClassName, b.ClassNumber, b.EnglishName";
$users = $lgroup->returnArray($sql,3);
for ($i=0; $i<sizeof($users); $i++)
{
     list($uid, $login, $name) = $users[$i];
     $user_used = $quota[$login][0];
     $user_quota = $quota[$login][1];
     if (!isset($quota[$login]))
     {
         $user_quota = "N/A";
     }
     else if ($user_quota == 0)
     {
         $user_quota = "$i_LinuxAccount_NoLimit";
     }
     if (!isset($quota[$login]))
     {
         $user_used = "N/A";
     }
     $css = ($i%2? "":"2");
     $en_login = urlencode($login);
     if ($user_quota == "N/A")
     {
         echo "<tr class=tableContent$css><td>$name ($login)</td><td colspan=2 align=center>$i_LinuxAccount_NoAccount</td></tr>\n";
     }
     else
     {
         echo "<tr class=tableContent$css><td><a href=user_set.php?loginName=$en_login>$name ($login)</a></td><td>$user_quota</td><td>$user_used</td></tr>\n";
     }
}

?>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</td>
</tr>
</table>

<?php
include_once("../../templates/adminfooter.php");
?>