<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");

$li = new libfilesystem();

/*
$li->file_write(htmlspecialchars(trim(stripslashes($website))), $intranet_root."/file/email_website.txt");
$school_data = "$schoolname\n$schoolorg";
$li->file_write(htmlspecialchars(trim(stripslashes($school_data))), $intranet_root."/file/school_data.txt");
$li->file_write(htmlspecialchars(trim(stripslashes($academic_yr))), $intranet_root."/file/academic_yr.txt");

$semester_data = "";
for ($i=0; $i<sizeof($semester); $i++)
{
     $tag = ($currentsem == $i)?1:0;
     $semester[$i] = htmlspecialchars(trim(stripslashes($semester[$i])));
     if ($semester[$i] != "")
     {
         $semester_data .= $semester[$i]."::$tag\n";
     }
     else
     {
         break;
     }

}
$li->file_write(htmlspecialchars(trim(stripslashes($semester_data))), $intranet_root."/file/semester.txt");
$li->file_write(htmlspecialchars(trim(stripslashes($DayType))), $intranet_root."/file/schooldaytype.txt");


// update language
switch ($LangID){
        case 0: $LangID = 0; break;
        case 1: $LangID = 1; break;
        case 2: $LangID = 2; break;
        default: $LangID = 0; break;
}
$li->file_write(htmlspecialchars(trim(stripslashes($LangID))), $intranet_root."/file/language.txt");


// update badge
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$path = "$intranet_root/file/$imgfile";

if ($use_badge == 1)
{
    if (is_uploaded_file($badge_image))
    {
        if (is_file($path))
        {
            $li->file_remove($path);
        }
        $li->lfs_copy($badge_image, "$intranet_root/file/$badge_image_name");
        $li->file_write(htmlspecialchars(trim(stripslashes($badge_image_name))), $intranet_root."/file/schoolbadge.txt");
    }
}
else
{
    if (is_file($path))
    {
        $li->file_remove($path);
    }
    $li->file_remove("$intranet_root/file/schoolbadge.txt");
}

# Popup date
$content = "$helpstart\n$helpend";
$li = new libfilesystem();
$li->file_write(intranet_htmlspecialchars(trim(stripslashes($content))), $intranet_root."/file/popupdate.txt");
*/

// update misc
/*
$email_allowed = ($email_allowed == 1? 1: "");
$title_disabled = ($title_disabled == 1? 1: "");
$home_tel_allowed = ($home_tel_allowed == 1? 1: "");
*/


$output_text[1] = "Personal_Info";
$output_text[2] = "Personal_Info";
$output_text[3] = "Personal_Info";
for($i=0; $i<$user_info_fields[0]; $i++)
{
  for($j=1; $j<=count($output_text); $j++)
  {
    $output_text[$j] .= ",";
    $output_text[$j] .= (isset(${"personal_info_allowed_".$j}) && in_array($i, ${"personal_info_allowed_".$j}))?"1":"";
  }
}
$output_text[1] .= "\n";
$output_text[2] .= "\n";
$output_text[3] .= "\n";

$output_text[1] .= "Contact_Info";
$output_text[2] .= "Contact_Info";
$output_text[3] .= "Contact_Info";
for($i=0; $i<$user_info_fields[1]; $i++)
{
  for($j=1; $j<=count($output_text); $j++)
  {
    $output_text[$j] .= ",";
    $output_text[$j] .= (isset(${"contact_info_allowed_".$j}) && in_array($i, ${"contact_info_allowed_".$j}))?"1":"";
  }
}
$output_text[1] .= "\n";
$output_text[2] .= "\n";
$output_text[3] .= "\n";

$output_text[1] .= "Message";
$output_text[2] .= "Message";
$output_text[3] .= "Message";
for($i=0; $i<$user_info_fields[2]; $i++)
{
  for($j=1; $j<=count($output_text); $j++)
  {
    $output_text[$j] .= ",";
    $output_text[$j] .= (isset(${"message_allowed_".$j}) && in_array($i, ${"message_allowed_".$j}))?"1":"";
  }
}
$output_text[1] .= "\n";
$output_text[2] .= "\n";
$output_text[3] .= "\n";

$output_text[1] .= "Password";
$output_text[2] .= "Password";
$output_text[3] .= "Password";
for($i=0; $i<$user_info_fields[3]; $i++)
{
  for($j=1; $j<=count($output_text); $j++)
  {
    $output_text[$j] .= ",";
    $output_text[$j] .= (isset(${"password_allowed_".$j}) && in_array($i, ${"password_allowed_".$j}))?"1":"";
  }
}
$output_text[1] .= "\n";
$output_text[2] .= "\n";
$output_text[3] .= "\n";

$output_text[1] .= "Display";
$output_text[2] .= "Display";
$output_text[3] .= "Display";
for($i=0; $i<$user_info_fields[4]; $i++)
{
  for($j=1; $j<=count($output_text); $j++)
  {
    $output_text[$j] .= ",";
    $output_text[$j] .= (isset(${"display_allowed_".$j}) && in_array($i, ${"display_allowed_".$j}))?"1":"";
  }
}

for($j=1; $j<=count($output_text); $j++)
  $li->file_write($output_text[$j], $intranet_root."/file/user_info_settings_".$j.".txt");

header("Location: index.php?msg=2");
?>
