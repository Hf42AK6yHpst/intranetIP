<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

# TABLE SQL
$keyword = trim($keyword);
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
if ($filter != "") $type_conds = "AND a.RecordType = $filter";
$sql  = "SELECT
                        CONCAT('<a class=tableContentLink href=edit.php?RoleID[]=', a.RoleID, '>', a.Title, '</a>'),
                        a.Description,
                        b.CategoryName,
                        a.DateModified,
                        IF(a.RecordStatus='1','<img src=../../images/red_checkbox.gif vspace=3 hspace=4 border=0>',CONCAT('<input type=checkbox name=RoleID[] value=', a.RoleID ,'>'))
                FROM
                        INTRANET_ROLE as a LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b
                                      ON a.RecordType = b.GroupCategoryID
                WHERE
                        (a.Title like '%$keyword%' OR a.Description like '%$keyword%')
                        $type_conds
                ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.Title", "a.Description","b.CategoryName", "a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_role;
$li->column_array = array(0,1,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(0, $i_RoleTitle)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(1, $i_RoleDescription)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(2, $i_GroupCategoryName)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(3, $i_RoleDateModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("RoleID[]")."</td>\n";

// TABLE FUNCTION BAR
$lgc = new libgroupcategory();
$selection = $lgc->returnSelectCategory("name=filter onChange=this.form.submit()",false,1,$filter);
$filterbar = "$i_GroupCategoryName: $selection";

$toolbar  = "<a class=iconLink href=javascript:checkPost(document.form1,'new.php')>".newIcon()."$button_new</a>".toolBarSpacer();
$toolbar .= "<a class=iconLink href='set_default.php'><img src=../../images/red_checkbox.gif hspace=5 vspace=0 border=0 align=absmiddle>$i_RoleDefault</a>";
$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'RoleID[]','set.php')\"><img src='/images/admin/button/t_btn_preset_value_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'RoleID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'RoleID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_group_settings, '/admin/group_settings/', $i_admintitle_am_role, '') ?>
<?= displayTag("head_group_role_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $filterbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>