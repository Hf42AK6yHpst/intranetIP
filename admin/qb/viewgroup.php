<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libqb.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$lq = new libqb();
$groupList = $lq->getAccessList();

if (!in_array($GroupID,$groupList))
{
     header("Location: index.php");
     exit();
}

$lg = new libgroup($GroupID);

switch ($signal)
{
        case 1: $xmsg = $i_con_msg_update; break;
        default: $xmsg = "";
}
?>
<SCRIPT LANGUAGE=javascript>
function checkform(obj){
        checkOptionAll(obj.elements["GroupID[]"]);
}
</SCRIPT>


<form name="form1" action="update.php" method="post" onSubmit="checkform(this)">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_plugin_qb, 'index.php', $lg->Title, '') ?>
<?= displayTag("head_qb_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td>
<br>
<?=$lq->displayCategoryTable($GroupID)?>
<br>
<?=$lq->displayLevelTable($GroupID)?>
<br>
<?=$lq->displayDifficultyTable($GroupID)?>
<br>
</td></tr>
<tr><td>
</td></tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>