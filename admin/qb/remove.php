<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libqb.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();
$lg = new libgroup($GroupID);
if (!$lg->isAccessQB())
{
     header("Location: index.php");
     exit();
}
include_once("../../templates/adminheader_setting.php");
$lq = new libqb();
$selection = $lq->returnSelect($GroupID,$type,"name=target","",-1);

for ($i=0; $i<sizeof($uniqueID); $i++)
{
     $inputs .= "<input type=hidden name=uniqueID[] value=".$uniqueID[$i].">\n";
}

?>

<form name="form1" action="remove_confirm.php" method="post">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_plugin_qb, 'index.php', $lg->Title, 'javascript:history.back()', $button_remove." ".$lq->tableTitle[$type], '') ?>
<?= displayTag("head_qb_$intranet_session_language.gif", $msg) ?>


<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td><?=$i_QB_SelectRemovalTransfer1.$lq->tableTitle[$type].$i_QB_SelectRemovalTransfer2?></td></tr>
<tr><td><?=$selection?></td></tr>
</table>
<br><br>
</blockquote>

<input type=hidden name=type value=<?=$type?>>
<input type=hidden name=GroupID value=<?=$GroupID?>>
<?=$inputs?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>