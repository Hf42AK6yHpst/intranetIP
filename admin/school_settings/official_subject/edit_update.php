<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

if($prev_recordID!=0)
{
	# Get display order of the selected subject
	$sql = "SELECT DisplayOrder FROM {$eclass_db}.ASSESSMENT_SUBJECT WHERE RecordID = '$prev_recordID'";
	$row = $li->returnVector($sql);
	$prev_order = $row[0];

	# Update display order of subjects under the selected subject
	$sql = "UPDATE {$eclass_db}.ASSESSMENT_SUBJECT SET DisplayOrder = (DisplayOrder+1) WHERE DisplayOrder > '{$prev_order}'";
	$li->db_db_query($sql);

	$new_order = $prev_order+1;
}
else
{	
	# Update display order of subjects under the selected subject
	$sql = "UPDATE {$eclass_db}.ASSESSMENT_SUBJECT SET DisplayOrder = (DisplayOrder+1)";
	$li->db_db_query($sql);

	$new_order = 1;
}

# Update current subject order
$sql = "UPDATE {$eclass_db}.ASSESSMENT_SUBJECT SET DisplayOrder = '{$new_order}' WHERE RecordID = '{$RecordID}'";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?msg=2&field=$field&order=$order");
?>