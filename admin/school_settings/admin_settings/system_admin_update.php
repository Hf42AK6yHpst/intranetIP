<?php
# using: Tommy

####################################################
#
#	Date:	2015-01-07	Bill [2014-1016-1748-03029]
#			Fixed: 	different chinese lang file for Simplified Chinese and Traditional Chinese
#
#	Date:	2012-11-06	Ivan [2012-1106-1504-46071]
#			Fixed:  update the SQL statement to update "HashedPass" instead of "UserPassword" for $intranet_authentication_method=="HASH" if update user password only
#
#	Date:	2012-10-12	YatWoon
#			Fixed: 	update the md5 statement [Casae#2012-1012-1542-52073]
#
####################################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$UserLogin = intranet_htmlspecialchars(trim($UserLogin));

### check UserLogin duplicate
$sql = "select UserLogin from INTRANET_USER where UserLogin='$UserLogin' and isSystemAdmin is NULL";
$result = $li->returnVector($sql);
if(!empty($result))
{
	$msg = 14;
}
else
{
	$Password = trim($Password);
    if ($intranet_authentication_method=="HASH")
    {
        //$HashedPass = MD5('$UserLogin$UserPassword$intranet_password_salt');
        $HashedPass = md5("$UserLogin$Password$intranet_password_salt");
    }
    else
    {
        $UserPassword = $Password;
    }
    
	# check update password only or create new account
	$sql = "select UserLogin from INTRANET_USER where isSystemAdmin=1";
	$result = $li->returnVector($sql);
	if($result[0]==$UserLogin)	# update password only
	{
		//2012-1106-1504-46071
		//$sql = "update INTRANET_USER set UserPassword='$UserPassword' where UserLogin='". $result[0] ."'";
		
		if ($intranet_authentication_method=="HASH") {
	        $sql = "update INTRANET_USER set HashedPass='$HashedPass' where UserLogin='". $result[0] ."'";
	    }
	    else {
	    	$sql = "update INTRANET_USER set UserPassword='$UserPassword' where UserLogin='". $result[0] ."'";
	    }
	    $li->db_db_query($sql);
	}
	else	# create new one
	{
		# remove old system admin
		$sql = "delete from INTRANET_USER where isSystemAdmin=1";
		$li->db_db_query($sql);
		
		include($PATH_WRT_ROOT."lang/lang.en.php");
		$EnglishName = $Lang['SystemAdmin'];
		
		# include lang.gb.php if using simplified chinese
		if ($intranet_session_language == 'gb'){
			include($PATH_WRT_ROOT."lang/lang.gb.php");
		} else {
			include($PATH_WRT_ROOT."lang/lang.b5.php");
		}
		$ChineseName = $Lang['SystemAdmin'];
		
		$sql = "insert into INTRANET_USER (UserLogin,UserPassword,HashedPass,UserEmail,EnglishName,ChineseName,RecordType,RecordStatus,DateInput,isSystemAdmin)
		values
		('$UserLogin','$UserPassword','$HashedPass','na','$EnglishName','$ChineseName',1,1,now(),1)";
		$li->db_db_query($sql) or die(mysql_error());
		$thisUserID = $li->db_insert_id();
		
		### set General_Setting
		$lgeneralsettings = new libgeneralsettings();
		$temp = $lgeneralsettings->Get_General_Setting("SchoolSettings");
		$temp_admin_str = empty($temp) ? "" : $temp[admin_user].",";
		# write to table GENERAL_SETTING
		$data = array();
		$data['admin_user'] = $temp_admin_str . $thisUserID;
		
		# store in DB
		$lgeneralsettings->Save_General_Setting("SchoolSettings", $data);
	}
	
	$msg=1;
	
}

intranet_closedb();
header("Location: index.php?msg=$msg");

/*
//$lf = new libfilesystem();
$lgeneralsettings = new libgeneralsettings();
if (is_array($target) && sizeof($target)>0)
{
	$target = str_replace("&#160;", "", $target);
	$file_content = implode(",", $target);
}

$file_content = trim($file_content, ",");

# write to table GENERAL_SETTING
$data = array();
$data['admin_user'] = $file_content;

# store in DB
$lgeneralsettings->Save_General_Setting("SchoolSettings", $data);

*/

?>