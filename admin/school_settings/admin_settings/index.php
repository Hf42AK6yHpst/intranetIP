<?php
# using: 

###################################################
#
#	Date:	2011-10-17	YatWoon
#			Fixed: Admin settings stored in GENERAL_SETTING with data something like "1,2,3,,4,5" so that ",," cause MySql query error. [Case#2011-1017-1132-01067]
###################################################
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");

intranet_opendb();


function selectionList($arr){
	for ($i=0; $i<sizeof($arr); $i++)
	{
		$rx .= "<option value=\"".$arr[$i][0]."\">".$arr[$i][1]."</option>\n";
	}
	return $rx;
}

$li = new libdb();

# get admin user
$sql = "select * from GENERAL_SETTING where Module='SchoolSettings'";
$result = $li->returnArray($sql);
$AdminUser = $result[0]['SettingValue'];

$AdminUser = substr($AdminUser,0,1)=="," ? substr($AdminUser,1,strlen($AdminUser)) : $AdminUser;
$AdminUser = substr($AdminUser,-1)=="," ? substr($AdminUser,0,strlen($AdminUser)-1) : $AdminUser;

$AdminUser_Temp = explode(",",$AdminUser);
$AdminUser_new = array();
foreach($AdminUser_Temp as $k=>$d)
{
	if(trim($d))
		$AdminUser_new[] = $d;
}
$AdminUser  = implode(",",$AdminUser_new);

if(!empty($AdminUser))
{
	$sql = "select UserID, 
	CONCAT(
		if(EnglishName<>'',EnglishName,''), 
		' (', 
		if(ChineseName<>'',ChineseName,''), 
		')')
	 from INTRANET_USER where UserID IN ($AdminUser) ORDER BY EnglishName, ChineseName ";
	$row = $li->returnArray($sql, 2);
	$AdminSelectionList = selectionList($row);
}

# get teaching staff
$cond = (!empty($AdminUser)) ? " AND UserID NOT IN ($AdminUser)" : "";
$sql = "select UserID, CONCAT(
		if(EnglishName<>'',EnglishName,''), 
		' (', 
		if(ChineseName<>'',ChineseName,''), 
		')') from INTRANET_USER where recordtype=1 $cond ORDER BY EnglishName, ChineseName ";
$row = $li->returnArray($sql, 2);
$TeachingStaffList = selectionList($row);

## system admin
$sql = "select UserLogin from INTRANET_USER where isSystemAdmin=1";
$result = $li->returnVector($sql);
$sLogin = $result[0];
?>
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '../index.php', $i_ReportCard_System_Admin_User_Setting, '') ?>
<?= displayTag("head_school_setting_$intranet_session_language.gif", $msg) ?>
<script language="javascript">
function checkform(obj){
	var target_obj = eval("obj.elements['target[]']");
	checkOption(target_obj);
	for(i=0; i<target_obj.length; i++)
	{
		target_obj.options[i].selected = true;
	}
	return 0;
}

function hideOption()
{
	//document.getElementById('div_system_admin').style.visibility = 'hidden'; 
	document.getElementById('div_system_admin').style.display = 'none'; 
}

function showOption()
{
	//document.getElementById('div_system_admin').style.visibility = 'visible'; 
	document.getElementById('div_system_admin').style.display = 'inline'; 
}

function checkform2(obj)
{
	if(!check_text(obj.UserLogin, "<?php echo $i_alert_pleasefillin.$i_UserLogin; ?>.")) return false;
	if(!check_text(obj.Password, "<?php echo $i_alert_pleasefillin.$i_UserPassword; ?>.")) return false;
}


</script>

<form name="form1" method="post" action="admin_setting_update.php" onSubmit="return checkform(this);">
<table width=500 border=0 cellpadding=2 cellspacing=0 align="center">
<tr><td colspan="3" nowrap><span class="extraInfo">[<span class=subTitle><?=$i_ReportCard_System_Admin_User_Setting?></span>]</span></td></tr>
<tr><td width="30%" align="right">
	<select name="target[]" class='inputfield' size='7' multiple>
	<?=$AdminSelectionList?>
	<!--<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>//-->
	</select>
	</td>
	<td align=center nowrap width="10%">
	<p><input type='image' src='/images/admin/button/s_btn_addto_<?=$intranet_session_language?>.gif' border='0' onClick='checkOptionTransfer(this.form.elements["source[]"],this.form.elements["target[]"]);return false;'></p>
	<p><input type='image' src='/images/admin/button/s_btn_deleteto_<?=$intranet_session_language?>.gif' border='0' onClick='checkOptionTransfer(this.form.elements["target[]"],this.form.elements["source[]"]);return false;'></p>
	</td>
	<td width="50%">
	<select class='inputfield' name="source[]" size=7 multiple>
	<?=$TeachingStaffList?>
	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
	</select>
	</td>
</tr></table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom" colspan="2"><hr size=1></td></tr>
<tr>
<td>
	<a href="javascript:showOption();"><font color="blue"><?=$Lang['SetSystemAdmin']?></font></a>
</td>
<td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
<a href="javascript:window.location.reload();"><img src="/images/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif" border="0"></a>
<a href="../"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<div id="div_system_admin" style="display:none">
	<!-- System Admin setting //-->
	<form name="form2" action="system_admin_update.php" method="post" onSubmit="return checkform2(this);">
	<table width=560 border=0 cellpadding=2 cellspacing=0 align="center">
	<tr>
		<td colspan="2" nowrap><span class="extraInfo">[<span class=subTitle><?=$Lang['SetSystemAdmin']?></span>]</span></td>
	</tr>
	<tr><td nowrap><?php echo $i_UserLogin; ?>:</td><td><input class=text type=text name=UserLogin size=20 maxlength=20 value='<?=$sLogin?>'></td></tr>
	<tr><td nowrap><?php echo $i_UserPassword; ?>:</td><td><input class=text type=password name=Password size=20 maxlength=20></td></tr>
	<tr><td colspan="2" height="22" style="vertical-align:bottom"><hr size=1></td></tr>
	
	<tr><td align="right" colspan="2">
		<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
		<a href="javascript:document.form2.reset();"><img src="/images/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif" border="0"></a>
		<a href="javascript:hideOption();"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
	</td>
	</tr>
	</table>
	</form>
</div>

<table width=560 border=0 cellpadding=2 cellspacing=0 align="center">
	<tr>
		<td colspan="2" nowrap><span class="extraInfo">[<span class=subTitle><?=$Lang['WhatIsAdminUser']['Title']?></span>]</span></td>
	</tr>
	<tr><td><?=$Lang['WhatIsAdminUser']['Details']?></td></tr>
</table>

<?php
intranet_closedb();
include($PATH_WRT_ROOT."templates/adminfooter.php");
?>