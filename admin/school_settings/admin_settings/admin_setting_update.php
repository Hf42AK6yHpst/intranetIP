<?php
# USING: YAT

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

//$lf = new libfilesystem();
$lgeneralsettings = new libgeneralsettings();
if (is_array($target) && sizeof($target)>0)
{
	$target = str_replace("&#160;", "", $target);
	$file_content = implode(",", $target);
}
$file_content = trim($file_content, ",");

# make sure the system admin exists in the list 
$sql = "select UserID from INTRANET_USER where isSystemAdmin=1";
$system_admin_id_tmp = $lgeneralsettings->returnVector($sql);
$system_admin_id = $system_admin_id_tmp[0];

$pos = strrpos(",".$file_content.",", ",".$system_admin_id.",");
if ($pos === false) 
{
	$file_content .= ",".$system_admin_id;
}

# write to table GENERAL_SETTING
$data = array();
$data['admin_user'] = $file_content;

# store in DB
$lgeneralsettings->Save_General_Setting("SchoolSettings", $data);

intranet_closedb();
header("Location: index.php?msg=2");
?>