<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

if (is_array($ClassID)) $ClassID = $ClassID[0];

$lclass = new libclass();
$sql = "SELECT a.ClassName, a.RecordStatus, b.LevelName,a.WebSAMSClassCode
        FROM INTRANET_CLASS as a LEFT OUTER JOIN INTRANET_CLASSLEVEL as b ON a.ClassLevelID = b.ClassLevelID
        WHERE ClassID = $ClassID";
$array = $lclass->returnArray($sql,4);
$ClassName = $array[0][0];
$RecordStatus = $array[0][1];
$ClassLevel = $array[0][2];
$WebSamsClassCode = $array[0][3];

# TABLE SQL
$keyword = addslashes(trim($keyword));

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field == "") $field = 5;
switch ($field){
	case 0: $field = 0; break;
	case 1: $field = 1; break;
	case 2: $field = 2; break;
	case 3: $field = 3; break;
	case 4: $field = 4; break;
	case 5: $field = 5; break;
	default: $field = 5; break;
}
if($filter == "") $filter = 1;
switch ($filter){
	case 0: $filter = 0; break;
	case 1: $filter = 1; break;
	case 2: $filter = 2; break;
	case 3: $filter = 3; break;
	default: $filter = 1; break;
}

$conds = ($TabID != ""? "AND a.RecordType = $TabID":"");
$sql  = "SELECT
				a.UserLogin,
				a.EnglishName,
				a.ChineseName,
				a.ClassName,
				a.ClassNumber,
				CONCAT('<input type=checkbox name=UserID[] value=', a.UserID ,'>')
		FROM
				INTRANET_USER AS a
		WHERE
				(a.UserLogin like '%$keyword%' OR 
				a.UserEmail like '%$keyword%' OR 
				a.EnglishName like '%$keyword%' OR 
				a.ChineseName like '%$keyword%' OR 
				a.ClassName like '%$keyword%') AND 
				a.ClassName != '$ClassName' AND 
				a.RecordStatus = $filter
				$conds
       ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.UserLogin", "a.EnglishName", "a.ChineseName", "a.ClassName", "a.ClassNumber");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_user;
$li->column_array = array(0,0,0,0,0);
$li->IsColOff = 2;

# TABLE COLUMN
$li->column_list .= "<td width='1' class='tableTitle'>#</td>\n";
$li->column_list .= "<td width='20%' class='tableTitle'>".$li->column(0, $i_UserLogin)."</td>\n";
$li->column_list .= "<td width='25%' class='tableTitle'>".$li->column(1, $i_UserEnglishName)."</td>\n";
$li->column_list .= "<td width='25%' class='tableTitle'>".$li->column(2, $i_UserChineseName)."</td>\n";
$li->column_list .= "<td width='15%' class='tableTitle'>".$li->column(4, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width='15%' class='tableTitle'>".$li->column(4, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width='1' class='tableTitle'>".$li->check("UserID[]")."</td>\n";

// TABLE FUNCTION BAR
//$toolbar = "<a class=iconLink href='javascript:checkAddStudent(document.form1, \"UserID[]\", \"add_student_update.php\")'>".newIcon()."$button_add</a>\n";
$functionbar = "<a href=\"javascript:checkAddStudent(document.form1,'UserID[]', 'UserID[]', 'add_student_update.php')\"><img src='/images/admin/button/t_btn_update_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes(stripslashes($keyword))."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$toolbar = "";
$toolbar2 = "";
?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.ClassName, "<?php echo $i_alert_pleasefillin.$i_ClassName; ?>.")) return false;
}

function checkAddStudent(obj,element,page){
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else{
		if(confirm('<?= $i_SmartCard_Confirm_Update_Attend ?>')){	            
		obj.action=page;                
		obj.method="POST";
		obj.submit();				             
		}
	}
}
</script>
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '../', $i_SettingsSchool_Class, 'javascript:history.back()', $button_add." ".$i_identity_student, '') ?>
<?= displayTag("head_school_class_$intranet_session_language.gif", $msg) ?>
<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right width=30%><?php echo $i_ClassName; ?>:</td><td><?=$ClassName?></td></tr>
<tr><td align=right><?php echo $i_ClassLevel; ?>:</td><td><?=$ClassLevel?></td></tr>
</table>
</blockquote>
<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
<tr><td><hr size="1" class="hr_sub_separator"></td></tr>
</table>

<form name="form1" method="get">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar2, $functionbar); ?></td></tr>
<tr><td><img src=../../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=ClassID value="<?=$ClassID?>">
</form>
<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>