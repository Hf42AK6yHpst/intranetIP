<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$WebSamsLevel ='';

$ValidWebSamsLevel = array();

if($plugin['StudentRegistry']){
        $ValidWebSamsLevel['EE1'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_EE1;
        $ValidWebSamsLevel['EE2'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_EE2;
        $ValidWebSamsLevel['EE3'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_EE3;
        $ValidWebSamsLevel['EEP'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_EEP;
}
$ValidWebSamsLevel['K1'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_K1;
$ValidWebSamsLevel['K2'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_K2;
$ValidWebSamsLevel['K3'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_K3;

if(!$plugin['StudentRegistry']){
        $ValidWebSamsLevel['LP'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_LP;
}

$ValidWebSamsLevel['P1'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_P1;
$ValidWebSamsLevel['P2'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_P2;
$ValidWebSamsLevel['P3'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_P3;
$ValidWebSamsLevel['P4'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_P4;
$ValidWebSamsLevel['P5'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_P5;
$ValidWebSamsLevel['P6'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_P6;

if($plugin['StudentRegistry']){
        $ValidWebSamsLevel['1'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_1;
        $ValidWebSamsLevel['2'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_2;
        $ValidWebSamsLevel['3'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_3;
        $ValidWebSamsLevel['4'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_4;
        $ValidWebSamsLevel['5'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_5;
        $ValidWebSamsLevel['6'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_6;
}
if($plugin['StudentRegistry']){
        $ValidWebSamsLevel['EP6'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_EP6;
}
if(!$plugin['StudentRegistry']){
        $ValidWebSamsLevel['PP'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_PP;
        $ValidWebSamsLevel['PR'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_PR;
}

$ValidWebSamsLevel['S1'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_S1;
$ValidWebSamsLevel['S2'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_S2;
$ValidWebSamsLevel['S3'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_S3;
$ValidWebSamsLevel['S4'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_S4;
$ValidWebSamsLevel['S5'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_S5;
$ValidWebSamsLevel['S6'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_S6;
if(!$plugin['StudentRegistry']){
        $ValidWebSamsLevel['S7'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_S7;
}

if(!$plugin['StudentRegistry']){
        $ValidWebSamsLevel['SJ'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_SJ;
        $ValidWebSamsLevel['SS'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_SS;
        $ValidWebSamsLevel['UP'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_UP;
}

if($plugin['StudentRegistry']){
        $ValidWebSamsLevel['F1'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_F1;
        $ValidWebSamsLevel['F2'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_F2;
        $ValidWebSamsLevel['F3'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_F3;
        $ValidWebSamsLevel['F4'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_F4;
        $ValidWebSamsLevel['F5'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_F5;
        $ValidWebSamsLevel['F6'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_F6;
        $ValidWebSamsLevel['7'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_7;
        $ValidWebSamsLevel['8'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_8;
        $ValidWebSamsLevel['9'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_9;
        $ValidWebSamsLevel['10'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_10;
        $ValidWebSamsLevel['11'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_11;
        $ValidWebSamsLevel['12'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_12;
        $ValidWebSamsLevel['ERP'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_ERP;
        $ValidWebSamsLevel['ERG'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_ERG;
        $ValidWebSamsLevel['ERC'] = $i_WebSAMS_AttendanceCode_Option_ClassLevel_ERC;
}

$select_websams_level = "<SELECT name='WebSamsLevel'>\n";
$select_websams_level.= "<OPTION VALUE='' /> -- $button_select --";
foreach($ValidWebSamsLevel as $v=>$d){
	$select_websams_level.= "<OPTION VALUE='".$v."' ".($v==$WebSamsLevel?'SELECTED':'')."/>".$d;
}
$select_websams_level .= "</SELECT><br>  <span class=extraInfo>($i_WebSAMS_Notice_AttendanceCode)</span>\n";

?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.LevelName, "<?php echo $i_alert_pleasefillin.$i_ClassLevel; ?>.")) return false;
}
</script>


<form name="form1" action="level_new_update.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '../', $i_SettingsSchool_Class, 'javascript:history.back()', $button_new." ".$i_ClassLevel, '') ?>
<?= displayTag("head_school_class_$intranet_session_language.gif", $msg) ?>


<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_ClassLevel; ?>:</td><td><input class=text type=text name=LevelName size=50 maxlength=100></td></tr>

<?if($special_feature['websams_attendance_export']){?>
	<tr><td align=right><?php echo $i_WebSAMS_ClassLevel; ?>:</td><td><?=$select_websams_level?></td></tr>
<?php } ?>

</table>
<br><br>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>
