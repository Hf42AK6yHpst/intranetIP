<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if($field=="") $field = 3;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 3; break;
}
$sql  = "SELECT
                        a.Title,
                        CONCAT('<a href=\"javascript:view(', if(b.ParentID=0,b.BulletinID,b.ParentID), ')\"><b>', b.Subject, '</b></a>', IF(LOCATE(';0;',ReadFlag)=0, '<img src=$image_path/new.gif border=0 hspace=2 align=absmiddle> ', '')),
                        CONCAT('<a href=mailto:', b.UserEmail, '>', b.UserName, '</a>'),
                        b.DateModified,
                        CONCAT('<input type=checkbox name=BulletinID[] value=', b.BulletinID ,'>')
                FROM
                        INTRANET_GROUP AS a, INTRANET_BULLETIN AS b
                WHERE
                        a.GroupID = b.GroupID AND (
                        a.Title like '%$keyword%' OR
                        b.Subject like '%$keyword%' OR
                        b.Message like '%$keyword%' OR
                        b.UserName like '%$keyword%' OR
                        b.userEmail like '%$keyword%'
                        )";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.Title", "b.Subject", "b.UserName", "b.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_frontpage_bulletin_message;
$li->column_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(0, $i_frontpage_bulletin_group)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(1, $i_frontpage_bulletin_subject)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(2, $i_frontpage_bulletin_author)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(3, $i_frontpage_bulletin_date)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("BulletinID[]")."</td>\n";

// TABLE FUNCTION BAR
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$functionbar = "<a href=\"javascript:checkRemove(document.form1,'BulletinID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<script language="javascript">
function view(id){
        url = "view.php?BulletinID=" + id;
        newWindow(url,1);
}
</script>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_groupfunction, '/admin/groupfunction/', $i_admintitle_im_group_bulletin, '') ?>
<?= displayTag("head_group_bulletin_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>