<?php
# using : 

################## Change Log [Start] ##############
#
#	Date:	2011-12-28	Yuen
#			handled eCircular's temp files and also just count the files uploaded 1 day again as temp file
################## Change Log [End] ##############

include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
$files = 0;
$size = 0;

$timeout = 60*60*24; // one day by default
$curTime = time();


$lf = new libfilesystem();

$fileDir = "$file_path/file/mail/";
$lf->folder_new($fileDir);
#if (!is_dir($fileDir)) mkdir($fileDir, 0777);
$d = dir($fileDir);

# Campusmail
while($entry = $d->read()) {
      if ($entry != "." && $entry != ".." && substr($entry,-3) == 'tmp')
      {
          $filepath = "$fileDir$entry";
          if (is_dir($filepath) && $curTime - filemtime($filepath)>$timeout)
          {
              $tempd = dir($filepath);
              while ($tempE = $tempd->read())
              {
                     $target = "$filepath/$tempE";
                     if (is_file($target))
                     {
                         $files++;
                         $size += filesize($target);
                     }
              }
          }

      }
}

# e-notice
$fileDir = "$file_path/file/notice/";
$lf->folder_new($fileDir);
#if (!is_dir($fileDir)) mkdir($fileDir);
$d = dir($fileDir);
while($entry = $d->read()) {
      if ($entry != "." && $entry != ".." && substr($entry,-3) == 'tmp')
      {
          $filepath = "$fileDir$entry";
          if (is_dir($filepath) && $curTime - filemtime($filepath)>$timeout)
          {
              $tempd = dir($filepath);
              while ($tempE = $tempd->read())
              {
                     $target = "$filepath/$tempE";
                     if (is_file($target))
                     {
                         $files++;
                         $size += filesize($target);
                     }
              }
          }

      }
}


# e-notice
$fileDir = "$file_path/file/circular/";
$lf->folder_new($fileDir);
#if (!is_dir($fileDir)) mkdir($fileDir);
$d = dir($fileDir);
while($entry = $d->read()) {
      if ($entry != "." && $entry != ".." && substr($entry,-3) == 'tmp')
      {
          $filepath = "$fileDir$entry";
          if (is_dir($filepath) && $curTime - filemtime($filepath)>$timeout)
          {
              $tempd = dir($filepath);
              while ($tempE = $tempd->read())
              {
                     $target = "$filepath/$tempE";
                     if (is_file($target))
                     {
             			 $files++;
                         $size += filesize($target);
                     }
              }
          }

      }
}


# Export files
$fileDir = "$file_path/file/export";
$lf->folder_new($fileDir);
#if (!is_dir($fileDir)) mkdir($fileDir);
$d = dir($fileDir);
while($entry = $d->read()) {
          $filepath = "$fileDir/$entry";
          if ($entry != "." && $entry != ".." && is_dir($filepath)  && $curTime - filemtime($filepath)>$timeout)
          {
              $tempd = dir($filepath);
              while ($tempE = $tempd->read())
              {
                     $target = "$filepath/$tempE";
                     if (is_file($target))
                     {
                         $files++;
                         $size += filesize($target);
                     }
              }
          }
}


$size /= (1024*1024);
$size = round($size,2);
?>

<form action="update.php" name="form1" method="post">
<?= displayNavTitle($i_admintitle_fs, '', $i_admintitle_tmpfiles, '') ?>
<?= displayTag("head_tmpfiles_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<blockquote>
<p><br><br>
<? echo "$files $i_Tempfiles_display1 $size $i_Tempfiles_display2 <br>\n";
echo "$i_Tempfiles_confirm <br>";
?><br>
</blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0">
</td>
</tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>