<?php
# using : 

################## Change Log [Start] ##############
#
#	Date:	2011-12-28	Yuen
#			handled eCircular's temp files
################## Change Log [End] ##############

@SET_TIME_LIMIT(153600);

include("../../includes/global.php");
include("../../includes/libfilesystem.php");
$lf = new libfilesystem();

$timeout = 60*60*24; // one day by default
$curTime = time();

# Campusmail temp files
$fileDir = "$file_path/file/mail";
$d = dir($fileDir);
while($entry = $d->read()) {
	$filepath = "$fileDir/$entry";
      if ($entry != "." && $entry != ".." && substr($entry,-3) == 'tmp' && $curTime - filemtime($filepath)>$timeout)
      {
          $lf->lfs_remove($filepath);
      }
}

# e-notice temp files
$fileDir = "$file_path/file/notice";
$d = dir($fileDir);
while($entry = $d->read()) {
	$filepath = "$fileDir/$entry";
      if ($entry != "." && $entry != ".." && substr($entry,-3) == 'tmp' && $curTime - filemtime($filepath)>$timeout)
      {
          $lf->lfs_remove($filepath);
      }
}


# e-notice temp files
$fileDir = "$file_path/file/circular";
$d = dir($fileDir);
while($entry = $d->read()) {
	$filepath = "$fileDir/$entry";
      if ($entry != "." && $entry != ".." && substr($entry,-3) == 'tmp' && $curTime - filemtime($filepath)>$timeout)
      {
          $lf->lfs_remove($filepath);
      }
}


# export files
$fileDir = "$file_path/file/export";
$d = dir($fileDir);
while($entry = $d->read())
{
	$filepath = "$fileDir/$entry";
      if ($entry != "." && $entry != ".." && is_dir($filepath) && $curTime - filemtime($filepath)>$timeout)
      {
          if (is_file($filepath))
          {
              $lf->lfs_remove($filepath);
          }
      }
}


header("Location: index.php?msg=2");
?>