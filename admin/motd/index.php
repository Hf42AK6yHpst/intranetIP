<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");

$li = new libfilesystem();
$motd = $li->file_read($intranet_root."/file/motd.txt");
?>

<form name="form1" action="update.php" method="post">
<?= displayNavTitle($i_adminmenu_adm, '', $i_admintitle_im_motd, '') ?>
<?= displayTag("head_motd_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<blockquote>
<p><br><br>
<table border="0">
<tr><td><?=$i_motd_description?></td></tr>
<tr><td><input class=text type=text name=motd size=70 maxlength=1000 value="<?php echo $motd; ?>"></td></tr>
</table>
<br><br></blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>