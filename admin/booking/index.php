<?php
//using : 

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libresource.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     case 4: $field = 4; break;
     default: $field = 0; break;
}
switch ($filter){
     case 0: $filter = 0; break;
     case 1: $filter = 1; break;
     case 2: $filter = 2; break;
     case 3: $filter = 3; break;
     case 4: $filter = 4; break;
     case 5: $filter = 5; break;
     default: $filter = 0; break;
}

$sql  = "SELECT
               DATE_FORMAT(a.BookingDate,'%Y-%m-%d'),
               CONCAT('<b>',d.Title,'</b><br>',IFNULL(d.TimeRange,'')),
               CONCAT('<b>', b.ResourceCategory, '</b><br>', b.ResourceCode, '<br>' ,b.Title) AS Resource,
               CONCAT('<b>', c.EnglishName,'</b> ', IFNULL(IF(c.ClassName <> '',c.ClassName,''),''), IFNULL(IF(c.ClassNumber = 0,'',c.ClassNumber),''), '<br>', if(a.Remark is NULL,'',concat('<br>', a.Remark))) AS UserName,
               a.TimeApplied,
               CONCAT('<input type=checkbox name=BookingID[] value=', a.BookingID ,'>')
          FROM
               INTRANET_BOOKING_RECORD AS a, INTRANET_RESOURCE AS b, INTRANET_USER AS c, INTRANET_SLOT AS d
          WHERE
               a.ResourceID = b.ResourceID AND
               a.UserID = c.UserID AND
               a.TimeSlot = d.SlotSeq AND
               b.TimeSlotBatchID = d.BatchID AND
               a.RecordStatus = $filter
               ";

if ($filter2!='')
    $sql .= " AND b.ResourceCategory = '$filter2'";
if ($filter3!='')
    $sql .= " AND b.ResourceID = '$filter3'";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.BookingDate", "d.Title", "Resource", "UserName","a.TimeApplied");

$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_booking;
$li->column_array = array(0,0,0,1,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(0, $i_BookingDate)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(1, $i_BookingTimeSlots)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(2, $i_ResourceCategory."/".$i_ResourceCode."/".$i_ResourceTitle)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(3, $i_UserEnglishName."/".$i_UserRemark)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(4, $i_BookingApplied)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("BookingID[]")."</td>\n";

// TABLE FUNCTION BAR
$btn_reject = "<a href=\"javascript:checkAlert(document.form1,'BookingID[]','cancel.php','$i_Alert_Reject')\"><img src='/images/admin/button/t_btn_refuse_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$btn_checkin = "<a href=\"javascript:checkAlert(document.form1,'BookingID[]','checkin.php','$i_Alert_Checkin')\"><img src='/images/admin/button/t_btn_checkin_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$btn_checkout = "<a href=\"javascript:checkAlert(document.form1,'BookingID[]','checkout.php','$i_Alert_Checkout')\"><img src='/images/admin/button/t_btn_checkout_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$btn_approve = "<a href=\"javascript:checkApprove(document.form1,'BookingID[]','approve.php')\"><img src='/images/admin/button/t_btn_permit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$btn_remove = "<a href=\"javascript:checkRemove(document.form1,'BookingID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$btn_archive = "<a href=\"javascript:archiveRecord(document.form1)\"><img src='/images/admin/button/t_btn_keep_past_record_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
switch ($filter)
{
        case 0:
             $functionbar = "$btn_reject$btn_approve"; break;
        case 1:
             $functionbar = ""; break;
        case 2:
             $functionbar = "$btn_checkout"; break;
        case 3:
             $functionbar = ""; break;
        case 4:
             $functionbar = "$btn_reject$btn_checkin$btn_checkout"; break;
        case 5:
             $functionbar = ""; break;
        default:
             $functionbar = "";
}
/*
$functionbar .= ($filter==3 || $filter==2 || $filter==1) ? "" : "<input class=button type=button value='$button_cancel' onClick=checkRemove(this.form,'BookingID[]','cancel.php')>";
$functionbar .= ($filter==3 || $filter==2 || $filter==1) ? "" : "<input class=button type=button value='$button_checkin' onClick=checkRemove(this.form,'BookingID[]','checkin.php')>";
$functionbar .= ($filter==3 || $filter==1 || $filter==0) ? "" : "<input class=button type=button value='$button_checkout' onClick=checkRemove(this.form,'BookingID[]','checkout.php')>";
$functionbar .= "<input class=button type=button value='$button_remove' onClick=checkRemove(this.form,'BookingID[]','remove.php')>\n";
*/
$functionbar .= "$btn_remove $btn_archive\n";
$searchbar  = "<select name=filter onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value=0 ".(($filter==0)?"selected":"").">$i_BookingPending</option>\n";
$searchbar .= "<option value=1 ".(($filter==1)?"selected":"").">$i_status_cancel</option>\n";
$searchbar .= "<option value=2 ".(($filter==2)?"selected":"").">$i_status_checkin</option>\n";
$searchbar .= "<option value=3 ".(($filter==3)?"selected":"").">$i_status_checkout</option>\n";
$searchbar .= "<option value=4 ".(($filter==4)?"selected":"").">$i_status_reserved</option>\n";
$searchbar .= "<option value=5 ".(($filter==5)?"selected":"").">$i_status_rejected</option>\n";
$searchbar .= "</select>\n";

$exportLink .= "<a class=iconLink href=javascript:checkGet(document.form1,'export.php');document.form1.action='index.php'>".exportIcon()."$button_export</a>\n".toolBarSpacer();

$toolbar = "<table border=0 cellpadding=1 cellspacing=1><tr><td><img src=/images/icon_viewarchivedrecoreds.gif border=0></td><td><a class=iconLink href='view_archive.php' target='_blank'>$i_Booking_ViewArchive</a> $exportLink </td></tr></table>";
$lr = new libresource();
/*
$filter_date = "<SELECT name=filter4 onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$filter_date .= "<OPTION value=0>$i_Booking_all</option>\n";
$filter_date .= "<OPTION value=1>$i_Booking_future</option>\n";
$filter_date .= "<OPTION value=2>$i_Booking_past</option>\n";
$filter_date .= "</SELECT>\n";
*/
$filter_cat = $lr->getSelectCats("name=filter2 onChange=\"this.form.pageNo.value=1;this.form.filter3.value='';this.form.submit();\"",$filter2);
$filter_item = $lr->getSelectItems("name=filter3 onChange=\"this.form.pageNo.value=1;this.form.filter2.value='';this.form.submit();\"",$filter3);
?>


<script language="Javascript">
function archiveRecord(obj)
{
        if (confirm('<?=$i_Booking_ArchiveConfirm?>'))
        {
                obj.action='archive.php';
                obj.submit();
        }
}
</script>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_rm, '', $i_admintitle_rm_record, '') ?>
<?= displayTag("head_booking_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar.$functionbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-","-","",$filter_cat.$filter_item); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<p><br></p>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>