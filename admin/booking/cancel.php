<?php
# using: yat

#######################
# 
#	Date:	2011-04-06	YatWoon
#			check need update periodic booking's status or not
#
#######################
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libemail.php");
include("../../includes/libsendmail.php");
include("../../includes/libwebmail.php");
include("../../lang/email.php");
include_once("../../includes/librb.php");
intranet_opendb();

$li = new libdb();
$lrb = new librb();

$RecordStatus = 5;

$sql = "UPDATE INTRANET_BOOKING_RECORD SET RecordStatus = '$RecordStatus', LastAction = now() WHERE BookingID IN (".implode(",", $BookingID).")";
$li->db_db_query($sql);

# check need update periodic booking's status or not
$sql = "select PeriodicBookingID from INTRANET_BOOKING_RECORD where BookingID IN (".implode(",", $BookingID).")";
$result = $li->returnVector($sql);
for($i=0; $i<sizeof($result); $i++)
{
	$pid = $result[$i];
	$lrb->NeedUpdatePeriodicRecordStatus($pid);
}

# Call sendmail function
for($i=0; $i<sizeof($BookingID); $i++){
     $BookID = $BookingID[$i];
     $fields_name = "a.BookingDate, b.Title, a.Remark, c.ResourceCategory, c.ResourceCode, c.Title, d.EnglishName, d.ChineseName, d.UserEmail, d.UserID";
     $db_tables = "INTRANET_BOOKING_RECORD as a, INTRANET_SLOT as b, INTRANET_RESOURCE as c, INTRANET_USER as d";
     $join_conds = "a.TimeSlot = b.SlotSeq AND b.BatchID = c.TimeSlotBatchID AND c.ResourceID = a.ResourceID AND a.UserID = d.UserID";
     $conds = "BookingID = $BookID";
     $sql = "SELECT $fields_name FROM $db_tables WHERE $join_conds AND $conds";
     $row = $li->returnArray($sql,10);
     $bdate = $row[0][0];
     $slot = $row[0][1];
     $Remark = $row[0][2];
     $ResourceCategory = $row[0][3];
     $ResourceCode = $row[0][4];
     $Title = $row[0][5];
     $EnglishName = $row[0][6];
     $ChineseName = $row[0][7];
     $UserEmail = $row[0][8];
     $UID = $row[0][9];
     
     if( (isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation']) && !$sys_custom['ResourceBooking']['DisableEmailNoticifcation']) || (!isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation'])) )
     {
     	$mailSubject = booking_title();
     	$mailBody = booking_cancel_body($bdate, $slot, $Remark, $ResourceCategory, $ResourceCode, $Title, $EnglishName, $ChineseName);
     	//$mailTo = $UserEmail;
     	//$lu = new libsendmail();
     	//$lu->do_sendmail($webmaster, "", $mailTo, "", $mailSubject, $mailBody);
     	$CancelToArray = array();
     	$CancelToArray[] = $UID;
     	$lwebmail = new libwebmail();
     	$lwebmail->sendModuleMail($CancelToArray,$mailSubject,$mailBody,1);
     }
     
}

if ($dev_log_required)
{
    $dev_log_filepath = "$file_path/file/log_resource.txt";
    # Time, $PHP_AUTH_USER, $HTTP_SERVER_VARS['REMOTE_ADDR'],$BookingID
    $temp_time = date("Y-m-d H:i:s");
    $temp_user = $PHP_AUTH_USER;
    $temp_IP = $HTTP_SERVER_VARS['REMOTE_ADDR'];
    $temp_list = implode(",",$BookingID);
    $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_IP\",\"Cancel\",\"$temp_list\"\n";
    $temp_content = get_file_content($dev_log_filepath);
    $temp_content .= $temp_logentry;
    write_file_content($temp_content, $dev_log_filepath);
}


intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&pageNo=$pageNo&filter2=$filter2&filter3=$filter3&msg=2");
?>