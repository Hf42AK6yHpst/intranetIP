<?php
// using : 

/*
 *				[START] Changed Log [START]
 *********************************************************
 *
 *  2019-05-14 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 *
 *	Date :	2010-09-24 [Ronald]
 *			
 *
 *********************************************************
 *				[END] Changed Log [END]  
 */


include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libemail.php");
include("../../includes/libsendmail.php");
include("../../includes/libwebmail.php");
include("../../lang/email.php");
intranet_opendb();

$li = new libdb();

$status_reserved = 4;
$status_cancel = 1;
$status_reject = 5;
$status_pending = 0;
$approved_status = "2,3,4";
$ignore_status = "1,5";

$cancelList = "0";
$cancelMailSubject = booking_title();
$approveMailSubject = booking_approve_title();

# Set status -> approve if original is pending
//$sql = "UPDATE INTRANET_BOOKING SET RecordStatus = '$status_approve', DateModified = now() WHERE BookingID IN (".implode(",", $BookingID).")";
//$li->db_db_query($sql);

# For each record approving
# if that time slot is reserved, set record to cancel and send cancel email if update count =1
# not reserved: update to approved if original is pending
# if count = 0, continue
# else send approval mail
# for each pending record,
#  - send cancel mail, store those bookingID
# end 2 for
#
# else Set status -> cancel if pending and in cancel bookingID
for($i=0; $i<sizeof($BookingID); $i++){
     $li->db_db_query("LOCK TABLES INTRANET_BOOKING_RECORD WRITE, INTRANET_BOOKING_RECORD as a WRITE, INTRANET_RESOURCE as b READ, INTRANET_SLOT as d READ, INTRANET_USER as c READ, INTRANET_USER as a READ");
     $BookID = $BookingID[$i];
     $sql = "SELECT a.BookingDate, d.Title, a.Remark, b.ResourceCategory, b.ResourceCode, b.Title, c.EnglishName, c.ChineseName, c.UserEmail, b.ResourceID, a.TimeSlot, c.UserID FROM INTRANET_BOOKING_RECORD AS a, INTRANET_RESOURCE AS b, INTRANET_USER AS c, INTRANET_SLOT AS d WHERE a.ResourceID = b.ResourceID AND a.UserID = c.UserID AND b.TimeSlotBatchID = d.BatchID AND a.TimeSlot = d.SlotSeq AND a.BookingID = '$BookID'";
     $row = $li->returnArray($sql,12);
     $bdate = $row[0][0];
     $slot = $row[0][1];
     $Remark = $row[0][2];
     $ResourceCategory = $row[0][3];
     $ResourceCode = $row[0][4];
     $Title = $row[0][5];
     $EnglishName = $row[0][6];
     $ChineseName = $row[0][7];
     $UserEmail = $row[0][8];
     $ResourceID = $row[0][9];
     $SlotSeq = $row[0][10];
     $UID = $row[0][11];
     
     # Check the time slot
     $sql = "SELECT count(*) FROM INTRANET_BOOKING_RECORD as a WHERE RecordStatus IN ($approved_status) AND ResourceID = '$ResourceID' AND BookingDate = '$bdate' AND TimeSlot = '$SlotSeq' AND BookingID != '$BookID'";
     $booked = $li->returnVector($sql);
     if ($booked[0] != 0)        // Time slot reserved
     {
         if ($dev_log_required)
         {
             $sql = "SELECT BookingID FROM INTRANET_BOOKING_RECORD WHERE BookingID = '$BookID' AND RecordStatus = '$status_pending'";
             $temp = $li->returnVector($sql);
             $dev_log_filepath = "$file_path/file/log_resource.txt";
             # Time, $PHP_AUTH_USER, $HTTP_SERVER_VARS['REMOTE_ADDR'],$BookingID
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $PHP_AUTH_USER;
             $temp_IP = $HTTP_SERVER_VARS['REMOTE_ADDR'];
             $temp_list = implode(",",$temp);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_IP\",\"Approved->Cancel1\",\"$temp_list\"\n";
             $temp_content = get_file_content($dev_log_filepath);
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $dev_log_filepath);
         }
         
         $sql = "UPDATE INTRANET_BOOKING_RECORD SET RecordStatus = '$status_reject' , LastAction = now() WHERE BookingID = '$BookID' AND RecordStatus = '$status_pending'";
         $cancelCount = $li->db_db_query($sql);
         $li->db_db_query("UNLOCK TABLES");
         if ($cancelCount != 0)          // Updated to cancel
         {
         	if( (isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation']) && !$sys_custom['ResourceBooking']['DisableEmailNoticifcation']) || (!isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation'])) )
         	{
				$cancelMailBody = booking_cancel_body($bdate, $slot, $Remark, $ResourceCategory, $ResourceCode, $Title, $EnglishName, $ChineseName);
             	//$lfailed = new libsendmail();
             	//$lfailed->do_sendmail($webmaster,"",$UserEmail,"",$cancelMailSubject,$cancelMailBody);
             	$lwebmail = new libwebmail();
             	$CancelToArray = array();
             	$CancelToArray[] = $UID;
             	$lwebmail->sendModuleMail($CancelToArray,$cancelMailSubject,$cancelMailBody,1);
         	}
         }
     }
     else
     {
         # Update as reserved
         $sql = "UPDATE INTRANET_BOOKING_RECORD SET RecordStatus = '$status_reserved' , LastAction = now() WHERE BookingID = '$BookID' AND RecordStatus = '$status_pending'";
         $approvedCount = $li->db_db_query($sql);
         if ($approvedCount != 0)
         {
         	if( (isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation']) && !$sys_custom['ResourceBooking']['DisableEmailNoticifcation']) || (!isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation'])) )
         	{
	             # Send approval mail
	             $approveMailBody = booking_approve_body($bdate, $slot, $Remark, $ResourceCategory, $ResourceCode, $Title, $EnglishName, $ChineseName);
	             //$mailTo = $UserEmail;
	             //$lu = new libsendmail();
	             //$lu->do_sendmail($webmaster, "", $mailTo, "", $approveMailSubject, $approveMailBody);
	             
	             $lwebmail = new libwebmail();
	             $SuccessToArray = array();
	             $SuccessToArray[] = $UID;
	             $lwebmail->sendModuleMail($SuccessToArray,$approveMailSubject,$approveMailBody,1);
         	}
             
             # Search other records in this time slot
             $sql = "SELECT BookingID FROM INTRANET_BOOKING_RECORD as a WHERE ResourceID = '$ResourceID' AND BookingDate = '$bdate' AND TimeSlot = '$SlotSeq' AND RecordStatus = '$status_pending'";
             $cancelArray = $li->returnVector($sql);
             $li->db_db_query("UNLOCK TABLES");
             if (sizeof($cancelArray)!=0)
             {
             	if( (isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation']) && !$sys_custom['ResourceBooking']['DisableEmailNoticifcation']) || (!isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation'])) )
             	{
             		$cancelStr = implode(",",$cancelArray);
             		$cancelList .= ",$cancelStr";
             		$sql = "SELECT c.UserEmail, c.EnglishName, c.ChineseName, c.UserID FROM INTRANET_BOOKING_RECORD as a, INTRANET_USER as c WHERE a.UserID = c.UserID AND a.BookingID IN ($cancelStr)";
             		
             		# Send cancel mail
             		$emailList = $li->returnArray($sql,4);
             		for ($j=0; $j<sizeof($emailList); $j++)
             		{
             			//$le = new libsendmail();
             			$cancelMailBody = booking_cancel_body($bdate, $slot, $Remark, $ResourceCategory, $ResourceCode, $Title, $emailList[$j][1], $emailList[$j[2]]);
             			//$le->do_sendmail($webmaster,"",$emailList[$j][0],"",$cancelMailSubject,$cancelMailBody);
             			$lwebmail = new libwebmail();
             			$CancelToArray = array();
             			$CancelToArray[] = $emailList[$j][3];
             			$lwebmail->sendModuleMail($CancelToArray,$cancelMailSubject,$cancelMailBody,1);
             		}
             	}
             }
         }
         else
         {
             $li->db_db_query("UNLOCK TABLES");
         }
     }
}

$sql = "UPDATE INTRANET_BOOKING_RECORD SET RecordStatus = '$status_reject' , LastAction = now() WHERE BookingID IN ($cancelList) AND RecordStatus = '$status_pending'";
$li->db_db_query($sql);

if ($dev_log_required)
{
    $dev_log_filepath = "$file_path/file/log_resource.txt";
    # Time, $PHP_AUTH_USER, $HTTP_SERVER_VARS['REMOTE_ADDR'],$BookingID
    $temp_time = date("Y-m-d H:i:s");
    $temp_user = $PHP_AUTH_USER;
    $temp_IP = $HTTP_SERVER_VARS['REMOTE_ADDR'];
    $temp_list = $cancelList;
    $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_IP\",\"Approved->Cancel2\",\"$temp_list\"\n";
    $temp_content = get_file_content($dev_log_filepath);
    $temp_content .= $temp_logentry;
    write_file_content($temp_content, $dev_log_filepath);
}

intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&pageNo=$pageNo&filter2=$filter2&filter3=$filter3&msg=2");
?>