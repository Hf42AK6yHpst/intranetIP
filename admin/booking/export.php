<?php
# using: 
/*
 *  2019-06-13 Cameron
 *      - fix potential sql injection problem
 *      
 *  known problem: although setting memory and runtime to unlimit, it failed to export all data if number of data is very large ( e.g. > 240000) [case #U162294] 
 *        
 */

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbdump.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lexport = new libexporttext();

# Export default format

$filter = IntegerSafe($filter);
$filter3 = IntegerSafe($filter3);

$sql  = "SELECT
               DATE_FORMAT(a.BookingDate,'%Y-%m-%d'),
               d.Title,
               d.TimeRange,
               b.ResourceCategory, 
               b.ResourceCode, 
               b.Title as INTRANET_RESOURCE_title,
               c.EnglishName,
               c.ChineseName,
               c.ClassName,
               c.ClassNumber, 
               a.Remark,
               a.TimeApplied
          FROM
               INTRANET_BOOKING_RECORD AS a, INTRANET_RESOURCE AS b, INTRANET_USER AS c, INTRANET_SLOT AS d
          WHERE
               a.ResourceID = b.ResourceID AND
               a.UserID = c.UserID AND
               a.TimeSlot = d.SlotSeq AND
               b.TimeSlotBatchID = d.BatchID AND
               a.RecordStatus = '$filter'
               ";

if ($filter2!='')
    $sql .= " AND b.ResourceCategory = '$filter2'";
if ($filter3!='')
    $sql .= " AND b.ResourceID = '$filter3'";

     $li = new libdb();
     $row = $li->returnArray($sql,12);
     $x = "$i_BookingDate,$i_BookingTimeSlots,$i_Slot_TimeRange,$i_ResourceCategory,$i_ResourceCode,$i_BookingItem,$i_UserEnglishName,$i_UserChineseName,$i_UserClassName,$i_UserClassNumber,$i_BookingRemark,$i_BookingApplied\n";
     //$utf_x = "$i_BookingDate\t$i_BookingTimeSlots\t$i_Slot_TimeRange\t$i_ResourceCategory\t$i_ResourceCode\t$i_BookingItem\t$i_UserEnglishName\t$i_UserChineseName\t$i_UserClassName\t$i_UserClassNumber\t$i_BookingRemark\t$i_BookingApplied\r\n";
     $exportColumn = array($i_BookingDate,$i_BookingTimeSlots,$i_Slot_TimeRange,$i_ResourceCategory,$i_ResourceCode,$i_BookingItem,$i_UserEnglishName,$i_UserChineseName,$i_UserClassName,$i_UserClassNumber,$i_BookingRemark,$i_BookingApplied);
     #$x = "UserLogin,Password,UserEmail,EnglishName,FirstName,LastName,ChineseName,Title,Gender,Mobile,ClassNumber,ClassName\n";
     
     /*
     for($i=0; $i<sizeof($row); $i++)
     {
         for($j=0; $j<sizeof($row[$i]); $j++)
         {
             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
         }
         $x .= implode(",", $row[$i])."\n";
     }
     */
     for($i=0; $i<sizeof($row); $i++)
     {
		$delim = "";
     	for($j=0; $j<24; $j++){
         	 $utf_row[] = addslashes(intranet_undo_htmlspecialchars($row[$i][$j]));
             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
             $x .= $delim.$row[$i][$j];		             
             $delim = ",";
  		}
		#$x .= implode(",", $row[$i]);
		$utf_rows[] = $utf_row;
		unset($utf_row);
		$x .="\n";
     }
    
	//if ($g_encoding_unicode) 
	//{     
	 	$export_content = $lexport->GET_EXPORT_TXT($utf_rows, $exportColumn);	
 	//}
 	//else
 	//{
	// 	$export_content = $x;
 	//}
 	
     /*
     $url = "/file/export/eclass-booking-".session_id()."-".time().".csv";
     $lo = new libfilesystem();
     $lo->file_write($x, $intranet_root.$url);
     */
     $filename = "eclass-booking-".session_id()."-".time().".csv";
     //output2browser($x, $filename);

	//$export_content = $lexport->GET_EXPORT_TXT($row, $exportColumn);
     
	$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
#header("Location: $url");
?>