<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();
if ($sms_vendor == "ORANGE")
{
    include_once("../../includes/liborangesms.php");
    $lsms = new liborangesms();
}
else
{
    header("Location: index.php");
    exit();
}
# $time_send <-- from HTML form
# Get Data from DB

$li = new libdb();
$sql = "SELECT Mobile, Message FROM TEMPSTORE_SMS_FILE";
$data = $li->returnArray($sql,2);

for ($i=0; $i<sizeof($data); $i++)
{
     list($mobile, $msg) = $data[$i];
     $numbers = array($mobile);
     $records = $lsms->sendSMS($numbers, $msg, $time_send);
     $logsent = $lsms->sendLogRecords($records);
     $values = "";
     $delim = "";
     $sms_message = addslashes($msg);
     for ($j=0; $j<sizeof($records); $j++)
     {
          $jobid = $records[$j]->jobid;
          $mobile = $records[$j]->recipient;
          $values .= "$delim('$jobid','$mobile','$sms_message',1,now(),now())";
          $delim = ",";
     }
     $sql = "INSERT IGNORE INTO INTRANET_SMS_LOG (JobID, MobileNumber,Message,RecordType,DateInput,DateModified) VALUES $values";
     $li->db_db_query($sql);
}

$sql = "DELETE FROM TEMPSTORE_SMS_FILE";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?sent=1");

?>