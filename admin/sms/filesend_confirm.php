<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();
if ($scdate != "" && $sctime != "")
{
    $time_send = strtotime("$scdate $sctime");
}
else $time_send = "";
# Parse File
$lf = new libfilesystem();
$file_content = get_file_content($userfile);
$data = $lf->file_read_csv($userfile);
array_shift($data);

$li = new libdb();

$sql = "CREATE TABLE IF NOT EXISTS TEMPSTORE_SMS_FILE (
 Mobile varchar(255),
 Message varchar(255)
)";
$li->db_db_query($sql);

$sql = "DELETE FROM TEMPSTORE_SMS_FILE";
$li->db_db_query($sql);

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, 'index.php',$i_SMS_File_Send,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>
<form action=filesend_update.php method=POST enctype="multipart/form-data">
<br><br>
<blockquote>
<font size=+1><?=$i_SMS_FileConfirm?></font><br>
<table width=450 border=1 cellspacing=1 cellpadding=0>
<tr><td><?=$i_SMS_Type_Mobile?></td><td><?=$i_SMS_MessageContent?></td></tr>
<?
$values = "";
$delim = "";
for ($i=0; $i<sizeof($data); $i++)
{
     list($mobile, $sms_msg) = $data[$i];
     $sms_msg = intranet_htmlspecialchars($sms_msg);
     $mobile = addslashes($mobile);
     $db_sms_msg = addslashes($sms_msg);
     $values .= "$delim ('$mobile','$db_sms_msg')";
     $delim = ",";
?>
<tr><td><?=$mobile?></td><td><?=$sms_msg?></td></tr>
<?
}
$sql = "INSERT INTO TEMPSTORE_SMS_FILE (Mobile, Message) VALUES $values";
$li->db_db_query($sql);
?>
</table>
<input type=hidden name=time_send value="<?=$time_send?>">
<input type="image" src="/images/admin/button/s_btn_send_<?=$intranet_session_language?>.gif" border="0">
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
 </blockquote>
</form>

<?
include_once("../../templates/adminfooter.php");
?>