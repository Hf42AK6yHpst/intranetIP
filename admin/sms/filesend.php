<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, 'index.php',$i_SMS_File_Send,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>
<form action=filesend_confirm.php method=POST enctype="multipart/form-data">
<blockquote>
<!--
<?=$i_SMS_ColData?> : <input type=radio name=type value=0 CHECKED><?=$i_SMS_Type_Mobile?> <input type=radio name=type value=1><?=$i_SMS_Type_Login?><br>
-->
<?=$i_SMS_UploadFile?> : <input type=file name=userfile size=40><br>
<p>
<blockquote>
<?=$i_SMS_FileDescription?>
<br>
<p><a class=functionlink_new href=sms_sample.csv><?=$i_general_clickheredownloadsample?></a>
</blockquote><br>
<?=$i_SMS_SendDate?> : <input type=text size=10 maxlength=10 name=scdate> (YYYY-MM-DD)<br>
<?=$i_SMS_SendTime?> : <input type=text size=10 maxlength=10 name=sctime> (HH:mm:ss <?=$i_SMS_24Hr?>)<br>
<?=$i_SMS_NoteSendTime?>
<br><br>
<input type="image" src="/images/admin/button/s_btn_upload_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</form>
<br><p><?=$i_SMS_Cautions?>
<?php
   echo "<table border=0 width=90% align=left cellpadding=1 cellspacing=1>";
   for ($i=1; $i<=6; $i++)
   {
        $stmt = ${"i_SMS_Limitation$i"};
        echo "<tr><td>$i.</td><td>$stmt</td></tr>\n";
   }
   echo "</table>";
?>
</blockquote>

<?
include_once("../../templates/adminfooter.php");
?>