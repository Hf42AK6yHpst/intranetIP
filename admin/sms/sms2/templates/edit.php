<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
include_once("../../../../includes/libsmsv2.php");
intranet_opendb();

$TemplateID = (is_array($TemplateID)? $TemplateID[0]:$TemplateID);

$li 	= new libdb();
$lsms	= new libsmsv2();

if(!($plugin['sms'] and $bl_sms_version >= 2))	$TemplateType = 1;
	
if($TemplateType==1)
{
    $sql = "SELECT Title, Content FROM INTRANET_SMS_TEMPLATE WHERE TemplateID = '".IntegerSafe($TemplateID)."'";
}	
else
{
	$status = $lsms->returnTemplateStatus($TemplateID);
	$sql = "SELECT TemplateCode, Content FROM INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE WHERE TemplateID = '".IntegerSafe($TemplateID)."'";
}	
$result = $li->returnArray($sql,2);
list($Title, $Content) = $result[0];
?>

<script language="javascript">
function checkform(obj){
     <? if($TemplateType==1) {?>
		if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_SMS_TemplateName; ?>.")) return false;
	<? } ?>
     if(!check_text(obj.Content, "<?php echo $i_alert_pleasefillin.$i_SMS_TemplateContent; ?>.")) return false;
}

function addTag(obj)
{
	tag = "($" + obj.Tag.value + ")";
	obj.Content.focus();
	obj.Content.value = obj.Content.value + tag;
}

</script>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '../',$i_SMS_MessageTemplate,'index.php',$button_edit,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>

<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_SMS_Personalized_Template_Type; ?>:</td><td><?=($TemplateType==1)? $i_SMS_NormalTemplate : $i_SMS_SystemTemplate;?></td></tr>
<? if($TemplateType==1) {?>
	<tr><td align=right><?php echo $i_SMS_TemplateName; ?>:</td><td><input class=text type=text name=Title size=30 maxlength=100 value="<?=$Title?>"></td></tr>
<? } else {?>
	<tr><td align=right><?php echo $i_SMS_Personalized_Template_SendCondition; ?>:</td><td><?=$lsms->returnTemplateName($Title);?></td></tr>
<? } ?>
<tr><td align=right><?php echo $i_SMS_TemplateContent; ?>:</td><td><TEXTAREA NAME=Content ROWS=6 COLS=50><?=$Content?></TEXTAREA></td></tr>
<?
## select the tag code
if($TemplateType==2) {?>
	<tr><td colspan='2'><span class='extraInfo'><?=$i_SMS_Personalized_Template_Instruction?></span></td></tr>
	<tr>
		<td align=right><?php echo $i_SMS_Personalized_Msg_Tags; ?>:</td>
		<td><?=$lsms->returnTagAry()?> <a href="javascript:addTag(document.form1)"><img src="<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border=0 align='absmiddle'></a></td>
	</tr>
	
	<tr>
		<td align=right><?php echo $i_general_status; ?>:</td>
		<td>
			<input type="radio" name="RecordStatus" value="1" id="RecordStatus1" <?=($status==1? "checked":"")?>> <label for="RecordStatus1"><?=$i_general_active?></label>
			<input type="radio" name="RecordStatus" value="0" id="RecordStatus0" <?=($status!=1? "checked":"")?>> <label for="RecordStatus0"><?=$i_general_inactive?></label>
		</td>
	</tr>
<? } ?>

</table>
<br><br>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input type=hidden name=TemplateID value="<?=$TemplateID?>">
<input type="hidden" name="TemplateType" value="<?=$TemplateType?>">
</form>

<?php
include_once("../../../../templates/adminfooter.php");
?>