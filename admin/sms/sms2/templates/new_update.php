<?php
include("../../../../includes/global.php");
include("../../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$Title = intranet_htmlspecialchars(trim($Title));
$Content = intranet_htmlspecialchars(trim($Content));

if($TemplateType==1) 	# Normal Template
{
	$tableDB	= "INTRANET_SMS_TEMPLATE";	
	$fieldname 	= "Title,Content,DateInput,DateModified";
	$fieldvalue = "'$Title','$Content',now(),now()";
}
else					# System Template
{
	$tableDB	= "INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE";	
	$fieldname 	= "TemplateCode,Content,DateInput,DateModified,RecordStatus";
	$fieldvalue = "'$TemplateCode','$Content',now(),now(),'$RecordStatus'";
}	

$sql = "INSERT INTO $tableDB ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?msg=1&TemplateType=$TemplateType");
?>