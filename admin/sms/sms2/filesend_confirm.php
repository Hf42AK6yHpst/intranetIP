<?php

# using: yuen


###################### Change Log Start ########################
#	Date: 	2012-11-30 yuen 
#			parsed phone number to remove unnecessary " " and "-"
#
###################### Change Log End ########################


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if($userfile=="" && !$isSent){
	header("Location: filesend.php");
	exit();
}

include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$libsms = new libsmsv2();

if ($scdate != "" && $sctime != "")
{
    $time_send = "$scdate $sctime";
}
else $time_send = "";


$sql = "CREATE TABLE IF NOT EXISTS TEMPSTORE_SMS2_FILE (
 Mobile varchar(255),
 Message varchar(255)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$libsms->db_db_query($sql);

# Parse File
/*
$lf = new libfilesystem();
$file_content = get_file_content($userfile);
$data = $lf->file_read_csv($userfile);
array_shift($data);
*/

if (!$isSent)
{
	$limport = new libimporttext();
	$filepath = $userfile;
	$filename = $userfile_name;
	$data = $limport->GET_IMPORT_TXT($filepath);
	array_shift($data);
	
	$sql = "DELETE FROM TEMPSTORE_SMS2_FILE";
	$libsms->db_db_query($sql);
}
else
{
	$sql = "Select Mobile, Message From TEMPSTORE_SMS2_FILE";
	$data = $libsms->returnArray($sql);
	
	$xmsg = '';
	if ($isSent && $returnCode < 0)
		$xmsg = $libsms->Get_SMS_Status_Display($returnCode);
}


$invalid_data = array();

$values = "";
$delim = "";
for ($i=0; $i<sizeof($data); $i++)
{
     list($mobile, $sms_msg) = $data[$i];
	$mobile = $libsms->parsePhoneNumber($mobile);
     if(!$libsms->isValidPhoneNumber($mobile) || $mobile == ''){
	     $invalid_data[$mobile]['Message'] = $sms_msg;
	     $invalid_data[$mobile]['Reason'] = $i_SMS_Error_NovalidPhone;
	     continue;
	  }
#     $sms_msg = intranet_htmlspecialchars($sms_msg);
     $mobile = addslashes($mobile);
     $db_sms_msg = addslashes($sms_msg);
     $values .= "$delim ('$mobile','$db_sms_msg')";
     $delim = ",";
}

if (!$isSent)
{
	$sql = "INSERT INTO TEMPSTORE_SMS2_FILE (Mobile, Message) VALUES $values";
	$libsms->db_db_query($sql);
}

### Send / Re-try button
if ($isSent)
	$btn_img = 's_btn_retry_'.$intranet_session_language.'.gif';
else
	$btn_img = 's_btn_send_'.$intranet_session_language.'.gif';
	
### Get system message if sending sms failed
$xmsg = '';
if ($isSent && $returnCode < 0)
	$xmsg = $libsms->Get_SMS_Status_Display($returnCode);



## --------- Valid List ------------ ### 
	$sql=" SELECT a.Mobile, a.Message FROM TEMPSTORE_SMS2_FILE AS a ";

$temp = $libsms->returnArray($sql,2);

$valid_table = "<table width=500 border=0 cellpadding=1 cellspacing=1>";
$valid_table.= "<tr><td>$i_SMS_Notice_Send_To_Users</td></tr></table>";
$valid_table.= "<table width=500 border=1 cellpadding=1 cellspacing=1>";
$valid_table.= "<tr class='tableTitle'><td width='10%'>$i_SMS_MobileNumber</td><td width='50%'>$i_SMS_MessageContent</td><td width='40%'>".$Lang['SMS']['MessageCutted']."</td></tr>";

$hasValidNumber=sizeof($temp)>0?true:false;
for($i=0;$i<sizeof($temp);$i++){
	list($mobile,$msg) = $temp[$i];
	$css = $i%2==0?"tableContent":"tableContent2";
	$msg_ok = sms_substr($msg);
	$msg_cut = str_replace($msg_ok, "", $msg);
	if (trim($msg_cut)=="")
	{
		$msg_cut = "&nbsp;";
	}
	$valid_table .= "<tr class='$css'><td>".($mobile==""?"-":$mobile)."</td><td>".($msg_ok==""?"-":$msg_ok)."</td><td><font color='orange'>".$msg_cut."</font></td></tr>";
}
$valid_table.="</table>";

## ---------- invalid list ---- #####
		$invalid_table = "<table width=500 border=0 cellpadding=1 cellspacing=1>";
		$invalid_table.= "<tr><td>$i_SMS_Warning_Cannot_Send_To_Users</td></tr></table>";
		$invalid_table.= "<table width=500 border=1 cellpadding=1 cellspacing=1 >";
		$invalid_table.= "<tr class='tableTitle'><td>$i_SMS_MobileNumber</td><td>$i_SMS_MessageContent</td><td>$i_Attendance_Reason</td></tr>";
		$i=0;
		foreach($invalid_data as $mobile=>$details){
			if($mobile=="") continue;
			$msg  = $details['Message'];
			$reason= $details['Reason'];
			$css =$i%2==0?"tableContent":"tableContent2";
			$invalid_table .= "<tr class='$css'><td>".($mobile==""?"-":$mobile)."</td><td>".($msg==""?"-":$msg)."</td><td>".($reason==""?"-":$reason)."</td></tr>";
			$i++;
		}
		$invalid_table.="</table>";

# ------------ Buttons ----------- #




?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, 'index.php',$i_SMS_File_Send,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>

<script language='javascript'>
function send(){
	obj = document.form1;
	if(document.form1==null) return;
	
	document.getElementById('actionBtnDiv').style.display = "none";
	document.getElementById('sendingSmsDiv').style.display = "block";
	
	document.form1.submit();
	
}
</script>

<form id="form1" name="form1" action=filesend_update.php method=POST enctype="multipart/form-data">
<br><br>
<blockquote>

<?=$invalid_table?>
<br>
<?=$valid_table?>
<br>
<input type=hidden name=time_send value="<?=$time_send?>">
<table border=0 width=500 align=center cellpadding=0 cellspacing=0>
<tr><td><hr size=1></td></tr>
<tr><td align=right>
	<div id='actionBtnDiv'>
		<?php if($hasValidNumber){?>
			<a href='javascript:send()'><img src='/images/admin/button/<?=$btn_img?>' border='0'></a>
	    <?php } ?>
		<a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
	</div>
	<div id="sendingSmsDiv" class="tabletextrequire" style="color:red; display:none;">
		<?=$Lang['SMS']['SendingSms']?>
	</div>
</td></tr>
</table>
</form>

<?
include_once("../../../templates/adminfooter.php");
?>
