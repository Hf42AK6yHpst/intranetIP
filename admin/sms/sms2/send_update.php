<?php

# using: yuen


###################### Change Log Start ########################
#	Date: 	2012-11-30 yuen 
#			parsed phone number to remove unnecessary " " and "-"
#
###################### Change Log End ########################

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libsmsv2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$libsms = new libsmsv2();

$message = trim($Message);

$sms_message = intranet_htmlspecialchars(trim($message));
if ($scdate != "" && $sctime != "")
{
    $time_send = "$scdate $sctime";
}
else $time_send = "";

if($recipient!=""){
	$temp_numbers = explode(";",$recipient);
	//$temp_numbers = array_unique($temp_numbers);  // remove duplicated phone number
	$temp_numbers = array_keys(array_flip($temp_numbers));		// remove duplicated phone number with continous index
	
	$numbers=array();
	$valid_numbers= array();
	$invalid_numbers=array();
	for ($i=0; $i<sizeof($temp_numbers); $i++)
	{
		 if(trim($temp_numbers[$i])=="") continue; 
	     $numbers[$i] = $libsms->parsePhoneNumber($temp_numbers[$i]);
	     
	     if($libsms->isValidPhoneNumber($numbers[$i])){
	     	$valid_numbers[] = $numbers[$i];
	 	 }else if($numbers[$i]!=""){
		 	$invalid_numbers[] = $numbers[$i];
	 	 }
	}
	
	// CREATE RECIPIENT DATA
	for($i=0;$i<sizeof($valid_numbers);$i++){
		$phone = $valid_numbers[$i];
		$recipientData[] = array($phone,0,"","");
	}
	
		
	if(sizeof($recipientData)>0 && $send==1){ // user clicked send button
		$sms_message = $message;
		$targetType = 1;
		$picType = 1;
		$adminPIC =$PHP_AUTH_USER;
		$userPIC = "";
		$frModule= "";
		$deliveryTime = $time_send;
		$isIndividualMessage=false;

		$returnCode = $libsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, "","",$deliveryTime,$isIndividualMessage);
		$isSent = 1;
	}
		
	if($isSent && ($returnCode > 0 && $returnCode==true))
		header("Location: index.php?sent=1");
	else{
	
		include_once("../../../templates/adminheader_intranet.php");
		
		$xmsg = '';
		if ($isSent && $returnCode < 0)
			$xmsg = $libsms->Get_SMS_Status_Display($returnCode);
			
		echo displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, 'index.php',$i_SMS_Send_Raw,'');
		echo displayTag("head_sms_$intranet_session_language.gif", $msg);
		
			
		# ------------- invalid ------------ #
	    $reason =$i_SMS_Error_NovalidPhone;
	
		$invalids= "<table width=560 border=0 cellpadding=2 cellspacing=1 align='center'>";
		$invalids.= "<tr><td>$i_SMS_Warning_Cannot_Send_To_Users </td></tr></table>";    
		$invalids.= "<table width=560 border=1 cellpadding=2 cellspacing=1 align='center'>";
		$invalids.= "<tr class='tableTitle'><td>$i_SMS_MobileNumber</td><td width=30%>$i_Attendance_Reason</td></tr>";
	    for($i=0;$i<sizeof($invalid_numbers);$i++){
	    	        $css = $i%2==0?"tableContent":"tableContent2";
		    		$mobile = $invalid_numbers[$i];
    				$invalids.="<tr class='$css'><td>".($mobile==""?"-":$mobile)."</td><td>".($reason==""?"-":$reason)."</td></tr>";
	    }
	    $invalids.= "</table>";
	    
	    
	    # ---------------------- valid list ---------- #
		$valids= "<table width=560 border=0 cellpadding=2 cellspacing=1 align='center'>";
		$valids.= "<tr><td>$i_SMS_Notice_Send_To_Users</td></tr></table>";
	    $valids.= "<table width=560 border=1 cellpadding=2 cellspacing=1 align='center'>";
	    $valids.= "<tr class='tableTitle'><td>$i_SMS_MobileNumber</td></tr>";
	    for($i=0;$i<sizeof($valid_numbers);$i++){
		    $mobile = $valid_numbers[$i];
	    
	        $css = $i%2==0?"tableContent":"tableContent2";
    		$valids.="<tr class='$css'><Td>".($mobile==""?"-":$mobile)."</td></tr>";
	    }
	    $valids.= "</table>";
		
	    $valids.="<Br>";
		
		
	    # ------------ Button ----------- #
	    	$buttons = '';
	        $buttons .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>";
				$buttons .= "<tr><td><hr size=1></td></tr>";
				$buttons .= "<tr><td align='right'>";
		        	$buttons .= "<div id='actionBtnDiv'>";
		                if(sizeof($valid_numbers)>0){
				    		if ($isSent)
				    			$btn_img = 's_btn_retry_'.$intranet_session_language.'.gif';
				    		else
				    			$btn_img = 's_btn_send_'.$intranet_session_language.'.gif';
				    		$buttons.="<a href='javascript:send()'><img src='/images/admin/button/$btn_img' border='0'></a>";
				 		}
			 			$buttons.="&nbsp;<a href='index.php'><img src='/images/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0'></a>";
					$buttons .= "</div>";
					$buttons .= "<div id='sendingSmsDiv' style='color:red; display:none;'>";
						$buttons .= $Lang['SMS']['SendingSms'];
					$buttons .= "</div>";
				$buttons .= "</td></tr>";
			$buttons .= "</table>";
		}
		
		?>
		<script language='javascript'>
		function send(){
			obj = document.form1;
			if(document.form1==null) return;
			
			sendObj = obj.send;
			if(sendObj==null) return;
			sendObj.value = 1;
			
			document.getElementById('actionBtnDiv').style.display = "none";
			document.getElementById('sendingSmsDiv').style.display = "block";
			
			document.form1.submit();
			
		}
		</script>
		<form name=form1 action='send_update.php' method=post>
		<input type=hidden name="scdate" value="<?=$scdate?>">
		<input type=hidden name="sctime" value="<?=$sctime?>">
		<input type=hidden name="Message" value="<?=intranet_htmlspecialchars(stripslashes($message))?>">
		<input type=hidden name="recipient" value="<?=$recipient?>">
		<input type=hidden name="send" value="">
		
		<?=$invalids?>
		<br>
		<?=$valids?>
		<?=$buttons?>
		</form>
		<?php
		 
			intranet_closedb();
			include_once("../../../templates/adminfooter.php");

}

?>
