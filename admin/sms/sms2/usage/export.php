<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$libsms = new libsmsv2();
$lexport = new libexporttext();

/*
$user_field = getNameFieldWithLoginByLang("b.");

$sql = "SELECT
              a.MobileNumber, $user_field AS UserName, a.Message,
              IF (a.RecordStatus IS NULL,'$i_SMS_Status0',
              CASE a.RecordStatus
                   WHEN NULL THEN '$i_SMS_Status0'
                   WHEN 0 THEN '$i_SMS_Status2'
                   WHEN 1 THEN '$i_SMS_Status1'
                   WHEN 2 THEN '$i_SMS_Status2'
                   WHEN 3 THEN '$i_SMS_Status3'
                   WHEN 4 THEN '$i_SMS_Status4'
                   WHEN 5 THEN '$i_SMS_Status5'
                   WHEN 6 THEN '$i_SMS_Status6'
                   WHEN 7 THEN '$i_SMS_Status7'
                   WHEN 8 THEN '$i_SMS_Status8'
                   WHEN 9 THEN '$i_SMS_Status9' END)
              ,
              a.TimeStamp, a.DateInput
        FROM INTRANET_SMS_LOG as a
             LEFT OUTER JOIN INTRANET_USER as b ON a.ReceiverID = b.UserID
        WHERE (
               a.MobileNumber LIKE '%$keyword%' OR
               $user_field LIKE '%$keyword%' OR
               a.Message LIKE '%$keyword%'
               )
        ";
*/

$content_field = " IF(a.IsIndividualMessage <>1, a.Content, IF(a.Content IS NOT NULL AND TRIM(a.Content)<>'', a.Content, '[".$i_SMS_MultipleMessage."]'))";
$namefield = getNameFieldByLang("b.");

$StatusField = '';
if ($sms_vendor == 'CTM')
{
	$StatusField = $libsms->Get_SMS_Status_Field('ismr', 0);
	$StatusFieldSQL = ", IF (a.RecipientCount > 1 And (ismr.ReferenceID is Not Null Or ismr.ReferenceID != ''), 
							'[".$i_SMS['MultipleStatus']."]', 
							$StatusField) 
						as RecordStatus";
}

$sql = "
		SELECT
				Distinct(a.SourceID),
				$content_field as Content, 
				a.RecipientCount, 
				IF (a.PICType=1, a.AdminPIC, $namefield) as PIC,
				a.DateInput
				$StatusFieldSQL
		FROM 
				INTRANET_SMS2_SOURCE_MESSAGE as a
				INNER JOIN
				INTRANET_SMS2_MESSAGE_RECORD as ismr
				ON (a.SourceID = ismr.SourceMessageID)
				LEFT OUTER JOIN 
				INTRANET_USER as b 
				ON (a.UserPIC = b.UserID)
        WHERE 
				a.Content LIKE '%$keyword%'
		
		Group By
				a.SourceID
		ORDER By
				a.DateInput Desc
			
";

$li = new libdb();
$result = $li->returnArray($sql);

/*
$csv = "Message, Recipient#, PIC, Real Transmission Time\n";
$utf_csv = "Message \t Recipient# \t PIC \t Real Transmission Time\r\n";
for ($i=0; $i<sizeof($result); $i++)
{
     $delim = "";
     $utf_delim = "";
     for ($j=0; $j<sizeof($result[$i]); $j++)
     {
          $csv .= "$delim\"".$result[$i][$j]."\"";
          $utf_csv .= "$utf_delim ".$result[$i][$j];
          $delim = ",";
          $utf_delim = "\t";
     }
     $csv .= "\n";
     $utf_csv .= "\r\n";
}

//$filename = "file/export/smslog-".session_id()."-".time().".csv";
$filename = "smslog-".session_id()."-".time().".csv";
//$export_filepath = "$intranet_root/$filename";
//write_file_content($csv,$export_filepath);

if ($g_encoding_unicode) {
        $lexport->EXPORT_FILE($filename, $utf_csv);
} else {
        output2browser($csv,$filename);
}
*/

$exportColumn = array("Message", "Recipient", "PIC", "Real Transmission Time");
if ($sms_vendor == 'CTM')
	$exportColumn[] = "Status";
	
$numOfSMS = count($result);
$exportContent = array();
for ($i=0; $i<$numOfSMS; $i++)
{
	$exportContent[$i][] = $result[$i]['Content'];
	$exportContent[$i][] = $result[$i]['RecipientCount'];
	$exportContent[$i][] = $result[$i]['PIC'];
	$exportContent[$i][] = $result[$i]['DateInput'];
	
	if ($sms_vendor == 'CTM')
		$exportContent[$i][] = $result[$i]['RecordStatus'];
}


$export_content = $lexport->GET_EXPORT_TXT($exportContent, $exportColumn);
$filename = "smslog-".session_id()."-".time().".csv";

$lexport->EXPORT_FILE($filename, $export_content);

//header("Location: $intranet_httppath/$filename");
intranet_closedb();
?>