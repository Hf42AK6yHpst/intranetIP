<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$libsms = new libsmsv2();
$lexport = new libexporttext();

$namefield = getNameFieldByLang("b.");

$StatusField = '';
if ($sms_vendor == 'CTM')
{
	$StatusField = ', '.$libsms->Get_SMS_Status_Field('a', 0).' as RecordStatus';
}

$sql = "
		SELECT
			a.RecipientPhoneNumber,  
			a.RecipientName,
			$namefield as RelatedUserName,  
			a.Message,                             
			a.DateInput     
			$StatusField      
		FROM INTRANET_SMS2_MESSAGE_RECORD as a
            LEFT OUTER JOIN INTRANET_USER as b ON a.RelatedUserID = b.UserID
		WHERE
			SourceMessageID = $SourceID
			and
				(
               a.RecipientPhoneNumber 	LIKE '%$keyword%' or
               a.RecipientName 			LIKE '%$keyword%' or
               $namefield		 		LIKE '%$keyword%' or
               a.Message 				LIKE '%$keyword%' 
               )       
";
        
$li = new libdb();
$result = $li->returnArray($sql);

/*
$csv = "Mobile,Receiver Name,Related User, Message,Real Transmission Time\n";

for ($i=0; $i<sizeof($result); $i++)
{
     $delim = "";
     for ($j=0; $j<sizeof($result[$i]); $j++)
     {
          $csv .= "$delim\"".$result[$i][$j]."\"";
          $delim = ",";
     }
     $csv .= "\n";
}

$filename = "file/export/smslog-".session_id()."-".time().".csv";
$export_filepath = "$intranet_root/$filename";
write_file_content($csv,$export_filepath);
*/

$exportColumn = array("Mobile", "Receiver Name", "Related User", "Message", "Real Transmission Time");
if ($sms_vendor == 'CTM')
	$exportColumn[] = "Status";

$numOfSMS = count($result);
$exportContent = array();
for ($i=0; $i<$numOfSMS; $i++)
{
	$exportContent[$i][] = $result[$i]['RecipientPhoneNumber'];
	$exportContent[$i][] = $result[$i]['RecipientName'];
	$exportContent[$i][] = $result[$i]['RelatedUserName'];
	$exportContent[$i][] = $result[$i]['Message'];
	$exportContent[$i][] = $result[$i]['DateInput'];
	
	if ($sms_vendor == 'CTM')
		$exportContent[$i][] = $result[$i]['RecordStatus'];
}
$export_content = $lexport->GET_EXPORT_TXT($exportContent, $exportColumn);


$filename = "file/export/smslog-".session_id()."-".time().".csv";
$lexport->EXPORT_FILE($filename, $export_content);


//header("Location: $intranet_httppath/$filename");
intranet_closedb();
?>