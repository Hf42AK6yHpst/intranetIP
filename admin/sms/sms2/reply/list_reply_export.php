<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lexport = new libexporttext();


$li2 = new libdb();

if($MessageCode>0){
	$sql="SELECT Message FROM INTRANET_SMS2_MESSAGE_RECORD WHERE RecordID='$MessageCode'";
	$temp = $li2->returnVector($sql);
	$message = $temp[0];
}
else if($MessageID>0){
	$sql="SELECT Content FROM INTRANET_SMS2_SOURCE_MESSAGE WHERE SourceID='$MessageID'";
	$temp = $li2->returnVector($sql);
	$message = $temp[0];
}

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;


$cond = " AND a.RecordStatus=1 ";
if($filter!=""){
	$cond.=" AND a.ReplyMessage='$filter' ";
}


if($MessageCode>0 && $IsIndividualMsg==1){
	$cond2 = " a.MessageCode='$MessageCode'";
}else if($MessageID>0 && $IsIndividualMsg!=1){
	$cond2 = " a.MessageID = '$MessageID'";
}

$namefield = getNameFieldWithClassNumberByLang("b.");
$sql="SELECT 
	IF($namefield IS NULL,'',$namefield),
	IF(a.ReplyMessage IS NULL,'',a.ReplyMessage),
	IF(a.ReplyPhoneNumber IS NULL,'',a.ReplyPhoneNumber),
	IF(a.RecordStatus=1,DATE_FORMAT(a.DateModified,'%Y-%m-%d %h:%i:%s'),'$i_SMS_Reply_Message_Status_Not_Yet_Replied')
	FROM INTRANET_SMS2_REPLY_MESSAGE AS a LEFT OUTER JOIN INTRANET_USER AS b ON (a.RelatedUserID = b.UserID)
	WHERE $cond2 $cond ";
	$li = new libdbtable($field, $order, $pageNo);
	$li->field_array = array("$namefield","a.ReplyMessage","a.ReplyPhoneNumber","a.DateModified");
	$li->sql = $sql;

	$sql = $li->built_sql();
	$temp = $li->returnArray($sql,4);

	
$x="\n";
$utf_x="\r\n";

$x.="\"$message\"\n";
$utf_x=str_replace("\n","\r\n",$message)."\r\n";

$x.="\"$i_UserName\",\"$i_SMS_Reply_Message_Replied_Message\",\"$i_SMS_Reply_Message_Replied_PhoneNumber\",\"$i_SMS_Reply_Message_Replied_Time\"\n";
$utf_x.="$i_UserName\t$i_SMS_Reply_Message_Replied_Message\t$i_SMS_Reply_Message_Replied_PhoneNumber\t$i_SMS_Reply_Message_Replied_Time\r\n";

for($i=0;$i<sizeof($temp);$i++){
	list($reply_user,$reply_msg,$reply_phone,$reply_time)=$temp[$i];
	$x.="\"$reply_user\",\"$reply_msg\",\"$reply_phone\",\"$reply_time\"\n";
	$utf_x.="$reply_user\t$reply_msg\t$reply_phone\t$reply_time\r\n";
}
if(sizeof($temp)<=0){
	$x.="\"$i_no_record_exists_msg\"\n\n";
	$utf_x.=$i_no_record_exists_msg."\r\n\r\n";
}


intranet_closedb();
$filename="replied_messages.csv";

//if ($g_encoding_unicode) {
	$lexport->EXPORT_FILE($filename, $utf_x);
//} else {
//	output2browser($x,$filename);
//}


?>
