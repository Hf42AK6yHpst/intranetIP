<?php

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
include_once("../../../../includes/libinterface.php");

intranet_opendb();

$li2 = new libdb();

if($MessageCode+0<=0 && $MessageID+0<=0){
	header("Location: list.php");
	exit();
}

if($MessageCode>0){
	$sql="SELECT Message FROM INTRANET_SMS2_MESSAGE_RECORD WHERE RecordID='$MessageCode'";
	$temp = $li2->returnVector($sql);
	$message = $temp[0];
}
else if($MessageID>0){
	$sql="SELECT Content FROM INTRANET_SMS2_SOURCE_MESSAGE WHERE SourceID='$MessageID'";
	$temp = $li2->returnVector($sql);
	$message = $temp[0];
}
$message = nl2br($message);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 3;

/*
switch($filter){
	case -1 : $cond = " AND a.RecordStatus=1 "; break;
	case -2 : $cond = " AND (a.RecordStatus IS NULL OR a.RecordStatus<>1)"; break;
	case "" : $cond =   ""; break;
	default : $cond =  " AND a.ReplyMessage = '$filter'" ;
}
*/


$cond = " AND a.RecordStatus=1 ";
if($filter!=""){
	$cond.=" AND a.ReplyMessage='$filter' ";
}

if($MessageCode>0 && $IsIndividualMsg==1){
	$cond2 = " a.MessageCode='$MessageCode'";
}else if($MessageID>0 && $IsIndividualMsg!=1){
	$cond2 = " a.MessageID = '$MessageID'";
}

$namefield = getNameFieldWithClassNumberByLang("b.");
$sql="SELECT 
	IF($namefield IS NULL,'&nbsp;',$namefield),
	IF(a.ReplyMessage IS NULL,'&nbsp;',a.ReplyMessage),
	IF(a.ReplyPhoneNumber IS NULL,'&nbsp',a.ReplyPhoneNumber),
	IF(a.RecordStatus=1,DATE_FORMAT(a.DateModified,'%Y-%m-%d %H:%i:%s'),'$i_SMS_Reply_Message_Status_Not_Yet_Replied')
	FROM INTRANET_SMS2_REPLY_MESSAGE AS a LEFT OUTER JOIN INTRANET_USER AS b ON (a.RelatedUserID = b.UserID)
	WHERE $cond2 $cond ";
	$li = new libdbtable($field, $order, $pageNo);
	$li->field_array = array("$namefield","a.ReplyMessage","a.ReplyPhoneNumber","a.DateModified");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+1;
	$li->title = "";
	$li->column_array = array(0,0,0,0);
	$li->wrap_array = array(0,0,0,0);
	$li->IsColOff = 2;
	
	hdebug_pr($sql);

	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
	$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserName)."</td>\n";
	$li->column_list .= "<td width=40% class=tableTitle>".$li->column($pos++, $i_SMS_Reply_Message_Replied_Message)."</td>\n";
	$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_SMS_Reply_Message_Replied_PhoneNumber)."</td>\n";
	$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_SMS_Reply_Message_Replied_Time)."</td>\n";


$cond =$MessageCode>0?" MessageCode='$MessageCode'":"MessageID='$MessageID'";
$sql="SELECT DISTINCT ReplyMessage FROM INTRANET_SMS2_REPLY_MESSAGE WHERE RecordStatus=1 AND $cond ORDER BY ReplyMessage";
$temp = $li->returnVector($sql);
$select_filter="<SELECT name='filter' onChange='filterReplyStatus()'>";
$select_filter.="<OPTION value='' ".($filter==""? " SELECTED ":"").">$i_status_all</OPTION>";
for($i=0;$i<sizeof($temp);$i++){
	$t = $temp[$i];
	$select_filter.="<OPTION value='$t' ".($filter==$t? " SELECTED ":"").">$t</OPTION>";
}
//$select_filter.="<OPTION value= '-1' ".($filter==-1? " SELECTED ":"").">$i_SMS_Reply_Message_Status_Replied</OPTION>";
//$select_filter.="<OPTION value='-2' ".($filter==-2? " SELECTED ":"").">$i_SMS_Reply_Message_Status_Not_Yet_Replied</OPTION>";

$select_filter.="</SELECT>";



 $searchbar=$select_filter."&nbsp;";
$toolbar ="&nbsp;<a class='iconLink' href=\"javascript:newWindow('stat.php?MessageID=$MessageID&MessageCode=$MessageCode&IsIndividualMsg=$IsIndividualMsg',6)\"><img src=/images/admin/icon_import.gif border=0 hspace=1 vspace=0 align=absmiddle>$i_SMS_Reply_Message_Replied_Message_Stat</a>";
$toolbar .="&nbsp;<a class='iconLink' href=\"javascript:doExport()\"><img src=/images/admin/icon_export.gif border=0 hspace=1 vspace=0 align=absmiddle>$button_export</a>";
?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '../index.php',$i_SMS_Reply_Message,'index.php',$i_SMS_Reply_Message_View_Sent_Message,'list.php',$i_SMS_Reply_Message_View_Replied_Message,'') ?>

<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>
<script language='javascript'>
function filterReplyStatus(){
	document.form1.submit();
}
function doExport(){
	old_target = document.form1.target;
	old_action = document.form1.action;
	document.form1.target='_blank';
	document.form1.action='list_reply_export.php';
	document.form1.submit();
	document.form1.target = old_target;
	document.form1.action = old_action;
}
</script>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<Tr><td width=50 nowrap><b><?=$i_SMS_Reply_Message_Message?></b>:&nbsp;</td><td><?=$message?></td></tr>
</table>
<form name="form1" method="POST">
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
	<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?>&nbsp;</td></tr>
<!--	<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar2", $functionbar); ?>&nbsp;</td></tr>-->
	<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
	</table>
	<?php echo $li->display(); ?>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
	</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=MessageID value='<?=$MessageID?>'>
<input type=hidden name=MessageCode value='<?=$MessageCode?>'>
<input type=hidden name=IsIndividualMsg value='<?=$IsIndividualMsg?>'>
</form>
<?
intranet_closedb();
include_once("../../../../templates/adminfooter.php");
?>