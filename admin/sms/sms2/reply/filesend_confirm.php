<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if($userfile==""){
	header("Location: filesend.php");
	exit();
}

include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();


if ($scdate != "" && $sctime != "")
{
    $time_send = "$scdate $sctime";
}
else $time_send = "";
# Parse File
/*
$lf = new libfilesystem();
$file_content = get_file_content($userfile);
$data = $lf->file_read_csv($userfile);
array_shift($data);
*/

$limport = new libimporttext();
$filepath = $userfile;
$filename = $userfile_name;
$data = $limport->GET_IMPORT_TXT($filepath);
array_shift($data);


$li = new libsmsv2();


$sql = "DELETE FROM TEMPSTORE_SMS2_FILE";
$li->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS TEMPSTORE_SMS2_FILE (
 Mobile varchar(255),
 Message varchar(255)
)ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql);



?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '../index.php',$i_SMS_Reply_Message,'index.php',$i_SMS_File_Send,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>
<form action=filesend_update.php method=POST enctype="multipart/form-data">
<br><br>
<blockquote>
<?
$invalid_data = array();

$values = "";
$delim = "";
for ($i=0; $i<sizeof($data); $i++)
{
     list($mobile, $sms_msg) = $data[$i];
     if(!$li->isValidPhoneNumber($mobile)){
	     $invalid_data[$mobile]['Message'] = $sms_msg;
	     $invalid_data[$mobile]['Reason'] = $i_SMS_Error_NovalidPhone;
	     continue;
	  }
     $sms_msg = intranet_htmlspecialchars($sms_msg);
     $mobile = addslashes($mobile);
     $db_sms_msg = addslashes($sms_msg);
     $values .= "$delim ('$mobile','$db_sms_msg')";
     $delim = ",";
}

$sql = "INSERT INTO TEMPSTORE_SMS2_FILE (Mobile, Message) VALUES $values";
$li->db_db_query($sql);



## --------- Valid List ------------ ### 
	$sql=" SELECT a.Mobile, a.Message FROM TEMPSTORE_SMS2_FILE AS a ";

$temp = $li->returnArray($sql,2);

$valid_table = "<table width=500 border=0 cellpadding=1 cellspacing=1>";
$valid_table.= "<tr><td>$i_SMS_Notice_Send_To_Users</td></tr></table>";
$valid_table.= "<table width=500 border=1 cellpadding=1 cellspacing=1>";
$valid_table.= "<tr class='tableTitle'><td>$i_SMS_MobileNumber</td><td>$i_SMS_MessageContent</td></tr>";

$hasValidNumber=sizeof($temp)>0?true:false;
for($i=0;$i<sizeof($temp);$i++){
	list($mobile,$msg) = $temp[$i];
	$css = $i%2==0?"tableContent":"tableContent2";
	$valid_table .= "<tr class='$css'><td>".($mobile==""?"-":$mobile)."</td><td>".($msg==""?"-":$msg)."</td></tr>";
}
$valid_table.="</table>";

## ---------- invalid list ---- #####
		$invalid_table = "<table width=500 border=0 cellpadding=1 cellspacing=1>";
		$invalid_table.= "<tr><td>$i_SMS_Warning_Cannot_Send_To_Users</td></tr></table>";
		$invalid_table.= "<table width=500 border=1 cellpadding=1 cellspacing=1 >";
		$invalid_table.= "<tr class='tableTitle'><td>$i_SMS_MobileNumber</td><td>$i_SMS_MessageContent</td><td>$i_Attendance_Reason</td></tr>";
		$i=0;
		foreach($invalid_data as $mobile=>$details){
			if($mobile=="") continue;
			$msg  = $details['Message'];
			$reason= $details['Reason'];
			$css =$i%2==0?"tableContent":"tableContent2";
			$invalid_table .= "<tr class='$css'><td>".($mobile==""?"-":$mobile)."</td><td>".($msg==""?"-":$msg)."</td><td>".($reason==""?"-":$reason)."</td></tr>";
			$i++;
		}
		$invalid_table.="</table>";

# ------------ Buttons ----------- #

?>
<?=$invalid_table?>
<br>
<?=$valid_table?>
<br>
<input type=hidden name=time_send value="<?=$time_send?>">
<table border=0 width=500 align=center cellpadding=0 cellspacing=0>
<tr><td><hr size=1></td></tr>
<tr><td align=right>

<?php if($hasValidNumber){?><input type="image" src="/images/admin/button/s_btn_send_<?=$intranet_session_language?>.gif" border="0"><?php } ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td></tr></table>
</form>

<?
include_once("../../../../templates/adminfooter.php");
?>