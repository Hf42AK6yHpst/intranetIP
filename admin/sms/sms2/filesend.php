<?php
/*
 * 	Date: 2013-03-20	Ivan [2012-0928-1438-14066]
 * 		- added scheduled send date time JS validation
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");

intranet_opendb();
$libsms = new libsmsv2();

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, 'index.php',$i_SMS_File_Send,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>
<SCRIPT LANGUAGE=Javascript src=../sms.js></SCRIPT>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
         <? if ($sms_vendor != "TACTUS") { ?>
		     if (obj.scdate.value != '')
		     {
		         if (!check_date(obj.scdate,'<?=$i_invalid_date?>')) return false;
		     }
		     if (obj.sctime.value != '')
		     {
		         if (!check_time(obj.sctime, '<?=$i_SMS_InvalidTime?>')) return false;
		     }
		     
		     if (obj.scdate.value != '' && obj.sctime.value != '') {
		     	var curTs = new Date().getTime();
		     	
		     	var scheduledDateText = obj.scdate.value;
		     	var scheduledDateAry = scheduledDateText.split('-');
		     	var scheduledYear = scheduledDateAry[0];
		     	var scheduledMonth = scheduledDateAry[1] - 1;
		     	var scheduledDate = scheduledDateAry[2];
		     	
		     	var scheduledTime = obj.sctime.value;
		     	var scheduledTimeAry = scheduledTime.split(':');
		     	var scheduledHour = scheduledTimeAry[0];
		     	var scheduledMin = scheduledTimeAry[1];
		     	var scheduledSec = scheduledTimeAry[2];
		     	
				var scheduledDateObj = new Date(scheduledYear, scheduledMonth, scheduledDate, scheduledHour, scheduledMin, scheduledSec);
				var scheduledDateTs = scheduledDateObj.getTime();
				
				if (scheduledDateTs <= curTs) {
					// scheduled date has past already
					alert('<?=$Lang['SMS']['ScheduledDateIsPast']?>');
					return false;
				}
		     }
         <? } ?>
}
</SCRIPT>
<form action=filesend_confirm.php method=POST enctype="multipart/form-data" ONSUBMIT="return checkform(this)">
<blockquote>
<!--
<?=$i_SMS_ColData?> : <input type=radio name=type value=0 CHECKED><?=$i_SMS_Type_Mobile?> <input type=radio name=type value=1><?=$i_SMS_Type_Login?><br>
-->
<?=$i_SMS_UploadFile?> : <input type=file name=userfile size=40><br>
<p>
<blockquote>
<?=$i_SMS_FileDescription?>
<br>
<p><a class="functionlink_new" href="<?=GET_CSV("sms_sample.csv")?>"><?=$i_general_clickheredownloadsample?></a>
</blockquote>
<? if ($sms_vendor != "TACTUS") { ?>
	<br>
	<?=$i_SMS_SendDate?> : <input type=text size=10 maxlength=10 name=scdate> (YYYY-MM-DD)<br>
	<?=$i_SMS_SendTime?> : <input type=text size=10 maxlength=10 name=sctime> (HH:mm:ss <?=$i_SMS_24Hr?>)<br>
	<?=$i_SMS_NoteSendTime?>
	<br><br>
<? } ?>
<input type="image" src="/images/admin/button/s_btn_upload_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</form>
<?=$libsms->Get_SMS_Limitation_Note_Table()?>
</blockquote>

<?
include_once("../../../templates/adminfooter.php");
?>