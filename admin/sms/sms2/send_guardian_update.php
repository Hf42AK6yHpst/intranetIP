<?php

# using: yuen


###################### Change Log Start ########################
#	Date: 	2012-11-30 yuen 
#			parsed phone number to remove unnecessary " " and "-"
#
###################### Change Log End ########################

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libsmsv2.php");
include_once("../../../lang/lang.$intranet_session_language.php");

intranet_opendb();

if(sizeof($Recipient)<=0){
    header("Location: index.php?nouser=1");
    exit();
	
}

$libsms = new libsmsv2();

$message = trim($Message);
$sms_message = intranet_htmlspecialchars(trim($message));

##---- get user list ----------###

$Recipient = array_unique($Recipient);
$Recipient = array_values($Recipient);

$group_list = array();
$user_list = array();


for($i=0;$i<sizeof($Recipient);$i++){
	$user = $Recipient[$i];
	if(substr($user,0,1)=="G"){
		$group_list[] = substr($user,1);
	}
	else if(substr($user,0,1)=="U"){
		$user_list[] =  substr($user,1);
	}
}


if(sizeof($group_list)>0){
	$list = implode(",",$group_list);
	/*
	$sql="
		SELECT 
			b.UserID 
		FROM INTRANET_USER AS b INNER JOIN 
			 INTRANET_CLASS AS a ON (a.ClassName = b.ClassName) INNER JOIN 
			 $eclass_db.GUARDIAN_STUDENT AS c ON ( b.UserID = c.UserID )
		WHERE 
			a.ClassID IN ($list) AND 
			b.RecordType=2 AND 
			b.RecordStatus IN (0,1,2)
		";
	*/
	/*
	$sql="
		SELECT 
			b.UserID 
		FROM INTRANET_USER AS b INNER JOIN 
			 INTRANET_CLASS AS a ON (a.ClassName = b.ClassName) 
		WHERE 
			a.ClassID IN ($list) AND 
			b.RecordType=2 AND 
			b.RecordStatus IN (0,1,2)
		";
	*/
	$sql = "Select
					iu.UserID
			From
					INTRANET_USER as iu
					Inner Join
					YEAR_CLASS_USER as ycu On (iu.UserID = ycu.UserID)
					Inner Join
					YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
			Where
					yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
					And
					yc.YearClassID in ($list)
					And
					iu.RecordType = 2
					And
					iu.RecordStatus in (0,1,2)
			";
	$temp= $libsms->returnVector($sql);
	if(sizeof($temp)>0)
		$user_list = array_merge($user_list,$temp);
}

$user_list = array_unique($user_list);


if (sizeof($user_list)==0)
{
    header("Location: index.php?nouser=1");
    exit();
}


if ($scdate != "" && $sctime != "")
{
    $time_send = "$scdate $sctime";
}
else $time_send = "";

// $list = implode(",",$user_list);
$list = implode("','",$libsms->Get_Safe_Sql_Query($user_list));

# ---- end get user list ---- #



## ----- get guardian data ---- ##
$namefield  = $libsms->getSMSIntranetUserNameField("b.");
$namefield2 = $libsms->getSMSGuardianUserNameField("a.");

$sql="SELECT a.UserID, $namefield2, a.Emphone, a.Phone, a.IsMain, a.IsSMS, b.UserID, $namefield, b.ClassName, b.ClassNumber  FROM $eclass_db.GUARDIAN_STUDENT AS a RIGHT OUTER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.UserID IN ('$list') AND b.RecordType=2 AND b.RecordStatus IN (0,1,2) ORDER BY b.UserID";
$temp = $libsms->returnArray($sql,10);

for($i=0;$i<sizeof($temp);$i++){
	
	list($a_userid,$name,$emphone,$phone, $is_main,$is_sms, $b_userid, $student,$class,$classnumber) = $temp[$i];

	$target_phone = "";
	$emphone = $libsms->parsePhoneNumber($emphone);
	$phone = $libsms->parsePhoneNumber($phone);
	if($libsms->isValidPhoneNumber($emphone))
		$target_phone = $emphone;
	else if($libsms->isValidPhoneNumber($phone))
		$target_phone = $phone;
	
	$guardian_data[$b_userid][] = array($target_phone,$a_userid,$name,$student,$class,$classnumber);
	
	if($a_userid=="")	continue;
	
	
	if($is_sms)
		$sms[$b_userid] = array($target_phone,$a_userid,$name,$student,$class,$classnumber);
	else if($is_main)
		$main[$b_userid] = array($target_phone,$a_userid,$name,$student,$class,$classnumber);
	else $others[$b_userid][] = array($target_phone,$a_userid,$name,$student,$class,$classnumber);
}

/*
1. IsSMS
2. IsMain
3. Neither IsSMS nor IsMain
*/
$valid=array();
$error_list=array();
for($i=0;$i<sizeof($user_list);$i++){
	
	$userid = $user_list[$i];
	
	$hasGuardian = ($sms[$userid]!="" || $main[$userid]!="" || $others[$userid]!="")? true : false;
	
	if($sms[$userid]!="" && $sms[$userid][0]!=""){ // has IsSMS AND IsSMS phone valid
				$recipientData[] = array($sms[$userid][0],$sms[$userid][1],$sms[$userid][2],"");
				$valid[$userid] = $sms[$userid];
				continue;
	}
	else if( $main[$userid]!="" && $main[$userid][0]!=""){ 
				## ( No IsSMS OR IsSMS phone invalid ) AND  has IsMain AND IsMain phone valid
				$recipientData[] = array($main[$userid][0],$main[$userid][1],$main[$userid][2],"");
				$valid[$userid] = $main[$userid];
				continue;
	}else if($others[$userid]!=""){
			# ( No IsSMS OR IsSMS phone invalid ) AND ( No IsMain OR IsMain phone invalid )
			$other = $others[$userid];
			for($j=0;$j<sizeof($other);$j++){
				$phone = $other[$j][0];
				if($phone!=""){ 
					$recipientData[] = array($other[$j][0],$other[$j][1],$other[$j][2],"");
					$valid[$userid] = $other[$j];
					break;
				}
			}
	}
	if($valid[$userid]==""){
		if($hasGuardian){ 
			// has guardian but no valid phone num
			$reason[$userid] = $i_SMS_Error_NovalidPhone;
		}
		else{ 
			// no guardian
		   $reason[$userid] = $i_SMS_Error_No_Guardian;
		}
		$error_list[] = $userid;
	}		
}	

if(sizeof($recipientData)>0 && $send==1){
	$targetType = 4;
	$picType = 1;
	$adminPIC =$PHP_AUTH_USER;
	$userPIC = "";
	$frModule= "";
	$deliveryTime = $time_send;
	$isIndividualMessage=false;
	$sms_message = $message;
	$returnCode = $libsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, "","",$deliveryTime,$isIndividualMessage);
	$isSent = 1;
}

if($isSent && ($returnCode > 0 && $returnCode==true))
{
	intranet_closedb();
	header("Location: index.php?sent=".$isSent);
}
else
{
	## ---------  valid list----------------##
	$valid_table = "<table width=560 border=0 cellpadding=2 cellspacing=1 align='center'>";
	$valid_table.= "<tr><td>$i_SMS_Notice_Send_To_Users</td></tr></table>";
	$valid_table.= "<table width=560 border=1 cellpadding=2 cellspacing=1 align='center'>";
	$valid_table.= "<tr class='tableTitle'><td width=30%>$i_UserName</td><td width=20%>$i_ClassName</td><td width=5%>$i_ClassNumber</td><td>$i_StudentGuardian_GuardianName</td><td>$i_SMS_MobileNumber</td></tr>";
	$i=0;
	$hasValidNumber = sizeof($valid)>0?true:false;
	foreach($valid as $userid =>$values){
		list($mobile,$userid,$name,$student,$class,$classnumber) = $values;
		$css = $i%2==0?"tableContent":"tableContent2";
	    $valid_table.="<tr class='$css'><td>".($student==""?"-":$student)."</td><td>".($class==""?"-":$class)."</td><Td width=5%>".($classnumber==""?"-":$classnumber)."</td><td>".($name==""?"-":$name)."</td><td>".($mobile==""?"-":$mobile)."</td></tr>";
		$i++;
	}	
	$valid_table.="</table>";

	#### --------  invalid list ------ #
	$invalid_table = "<table width=560 border=0 cellpadding=2 cellspacing=1 align='center'>";
	$invalid_table.="<tr><td>$i_SMS_Warning_Cannot_Send_To_Users</td></tr>";
	$invalid_table.="</table>";
    $invalid_table.="<table width=560 border=1 cellpadding=2 cellspacing=1 align='center'>";
	$invalid_table.= "<tr class='tableTitle'><td width=30%>$i_UserName</td><td width=20%>$i_ClassName</td><td width=5%>$i_ClassNumber</td><td>$i_StudentGuardian_GuardianName</td><td>$i_SMS_MobileNumber</td><td>$i_Attendance_Reason</td></tr>";
        for($i=0;$i<sizeof($error_list);$i++){
   	        $css = $i%2==0?"tableContent":"tableContent2";
	        $userid = $error_list[$i];
	        $error_reason = $reason[$userid];
	        $data = $guardian_data[$userid];
	        if(sizeof($data)==1){
   		        list($mobile,$id,$name,$student,$class,$classnumber)= $data[0];
	    		$invalid_table.="<tr class='$css'><td>".($student==""?"-":$student)."</td><td>".($class==""?"-":$class)."</td><Td width=5%>".($classnumber==""?"-":$classnumber)."</td><td>".($name==""?"-":$name)."</td><td>".($mobile==""?"-":$mobile)."</td><td>".($error_reason==""?"-":$error_reason)."</td></tr>";
			}else{
				$rowspan=sizeof($data);
  		        list($mobile,$id,$name,$student,$class,$classnumber)= $data[0];
	    		$invalid_table.="<tr class='$css'><td rowspan='$rowspan'>".($student==""?"-":$student)."</td><td rowspan='$rowspan'>".($class==""?"-":$class)."</td><Td rowspan='$rowspan' width=5%>".($classnumber==""?"-":$classnumber)."</td><td>".($name==""?"-":$name)."</td><td>".($mobile==""?"-":$mobile)."</td><td>".($error_reason==""?"-":$error_reason)."</td></tr>";
		        for($j=1;$j<sizeof($data);$j++){
			        list($mobile,$id,$name,$student,$class,$classnumber)= $data[$j];
		    		$invalid_table.="<tr class='$css'><td>".($name==""?"-":$name)."</td><td>".($mobile==""?"-":$mobile)."</td><td>".($error_reason==""?"-":$error_reason)."</td></tr>";
			    }
		    }
	    }	
	$invalid_table.="</table>";

//    $buttons= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
//                    <tr><td><hr size=1></td></tr>
//                    <tr><td align='right'>";
//            	if($hasValidNumber){
//		            if ($isSent)
//				    	$btn_img = 's_btn_retry_'.$intranet_session_language.'.gif';
//				    else
//				    	$btn_img = 's_btn_send_'.$intranet_session_language.'.gif';
//			    	
//	            	$buttons.="<a href='javascript:send()'><img src='/images/admin/button/$btn_img' border='0'></a>";
//	           }
//	           $buttons.="&nbsp;<a href='index.php'><img src='/images/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0'></a></td></tr></table>";

		$buttons = '';
        $buttons .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>";
			$buttons .= "<tr><td><hr size=1></td></tr>";
			$buttons .= "<tr><td align='right'>";
	        	$buttons .= "<div id='actionBtnDiv'>";
	                if($hasValidNumber){
			    		if ($isSent)
			    			$btn_img = 's_btn_retry_'.$intranet_session_language.'.gif';
			    		else
			    			$btn_img = 's_btn_send_'.$intranet_session_language.'.gif';
			    		$buttons.="<a href='javascript:send()'><img src='/images/admin/button/$btn_img' border='0'></a>";
			 		}
		 			$buttons.="&nbsp;<a href='index.php'><img src='/images/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0'></a>";
				$buttons .= "</div>";
				$buttons .= "<div id='sendingSmsDiv' style='color:red; display:none;'>";
					$buttons .= $Lang['SMS']['SendingSms'];
				$buttons .= "</div>";
			$buttons .= "</td></tr>";
		$buttons .= "</table>";

		include_once("../../../templates/adminheader_intranet.php");
		
		$xmsg = '';
		if ($isSent && $returnCode < 0)
			$xmsg = $libsms->Get_SMS_Status_Display($returnCode);
		echo displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, 'index.php',$i_SMS_SendTo_Student_Guardian,'');
		echo displayTag("head_sms_$intranet_session_language.gif", $msg);
		?>
		<script language='javascript'>
		function send(){
			obj = document.form1;
			if(document.form1==null) return;
			
			sendObj = obj.send;
			if(sendObj==null) return;
			sendObj.value = 1;
			
			document.getElementById('actionBtnDiv').style.display = "none";
			document.getElementById('sendingSmsDiv').style.display = "block";
			
			document.form1.submit();
			
		}
		</script>
		<form name=form1 action='send_guardian_update.php' method='post'>
		<input type=hidden name="scdate" value="<?=$scdate?>">
		<input type=hidden name="sctime" value="<?=$sctime?>">
		<input type=hidden name="Message" value="<?=intranet_htmlspecialchars(stripslashes($message))?>">
		<?php
			for($i=0;$i<sizeof($Recipient);$i++)
				echo "<input type=hidden name='Recipient[]' value='".$Recipient[$i]."'>\n";
		?>
		<input type=hidden name="send" value="">
		<?=$invalid_table?>

		<BR>
			<?=$valid_table?>

		<Br>
		<?=$buttons?>
		</form>
		<?php		
        intranet_closedb();
		include_once("../../../templates/adminfooter.php");
}

?>
