<?php
# being modified by: yat

#####################################
#
#	Date:	2013-07-15	YatWoon
#			hide all the link and dipslay remark 
#
#####################################

include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libsmsv2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

if ($sent == 1)
{
    $xmsg = "$i_SMS_MessageSent";
}
if ($nouser==1)
{
    $xmsg = $i_SMS_con_NoUser;
}

$libsms = new libsmsv2();
?>


<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>

<script>
function Prompt_No_SMS_Plugin_Warning()
{
	alert("<?=$i_SMS['jsWarning']['NoPluginWarning']?>");
}
</script>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent align=left>
<blockquote>
<?php

if ($plugin['sms'])
{
	echo $libsms->Get_SMS_Fee_Notes();
	
	/*
	if ($sms_vendor == "KANHAN")
	{
		echo displayOption(         $i_SMS_Send_SelectUser, 'user.php', 1,
	                                $i_SMS_Send_Raw, 'send.php', 1,
	                                $i_SMS_File_Send, 'filesend.php', 1,
	                                $i_SMS_SendTo_Student_Guardian,'send_guardian.php',1,
	                                $i_SMS_SentRecords, 'usage/', 1,
	                                $i_SMS_UsageReport, 'usage2/', 1,
									$i_SMS_MessageTemplate, 'templates/', ($plugin['sms'] and $bl_sms_version >= 2),
									$i_SMS_Reply_Message,'reply/', ($sms_use_reply_message && $sms_vendor_for_ReplyMessage)
	                                );
	}
	else if ($sms_vendor == "TACTUS")
	{
		echo displayOption(         $i_SMS_Send_SelectUser, 'user.php', 1,
	                                $i_SMS_Send_Raw, 'send.php', 1,
	                                $i_SMS_File_Send, 'filesend.php', 1,
	                                $i_SMS_SendTo_Student_Guardian,'send_guardian.php',1,
	                                $i_SMS_SentRecords, 'usage/', 1,
	                                $i_SMS_UsageReport, 'usage2/', 1,
									$i_SMS_MessageTemplate, 'templates/', ($plugin['sms'] and $bl_sms_version >= 2)
	                                );
	}
	
	else
	{
		echo displayOption(         $i_SMS_Send_SelectUser, 'user.php', 1,
	                                $i_SMS_Send_Raw, 'send.php', 1,
	                                $i_SMS_File_Send, 'filesend.php', 1,
	                                $i_SMS_SendTo_Student_Guardian,'send_guardian.php',1,
	                                $i_SMS_SentRecords, 'usage/', 1,
									$i_SMS_MessageTemplate, 'templates/', ($plugin['sms'] and $bl_sms_version >= 2),
									$i_SMS_Reply_Message,'reply/', ($sms_use_reply_message && $sms_vendor_for_ReplyMessage)
	                                );
	}
	*/
	
} else
{
	echo "<font color='red'>".$i_SMS['NoPluginWarning']."</font><br /><br />";
	/*
	echo displayOption(         $i_SMS_Send_SelectUser, 'javascript:Prompt_No_SMS_Plugin_Warning()', 1,
                                $i_SMS_Send_Raw, 'javascript:Prompt_No_SMS_Plugin_Warning()', 1,
                                $i_SMS_File_Send, 'javascript:Prompt_No_SMS_Plugin_Warning()', 1,
                                $i_SMS_SendTo_Student_Guardian,'javascript:Prompt_No_SMS_Plugin_Warning()',1,
                                $i_SMS_SentRecords, 'javascript:Prompt_No_SMS_Plugin_Warning()', 1,
                                $i_SMS_UsageReport, 'javascript:Prompt_No_SMS_Plugin_Warning()', 1,
								$i_SMS_MessageTemplate, 'javascript:Prompt_No_SMS_Plugin_Warning()',1
                                );
                                */
}
?>
</blockquote>
</td></tr>
<tr><td><br><br><br><font color="red"><?=$Lang['AdminConsole']['SMSAlertMsg']?></font></td></tr>
</table>

<?
include_once("../../../templates/adminfooter.php");
?>