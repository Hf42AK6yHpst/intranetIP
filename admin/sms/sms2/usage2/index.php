<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$li = new libdb();
if ($refresh==1)
{
    # Retrieve from BL Server
    $xmsg = "<font color=green>".$i_general_refreshed."</font>";
}

if(!$year)	$year = date("Y");

$sql = "
		SELECT
			Month,
			MessageCount
		FROM INTRANET_SMS2_MONTHLY_REPORT 
        WHERE 	
        	Year = $year
";
$result = $li->returnArray($sql,2);

//save the data in array
$data[] = array();
for($i=0;$i<sizeof($result);$i++)
{
	list($month, $count) = $result[$i];
	$data[$month] = $count;
}

$x = "";
$x .= "<table width=560 border=0 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr>";
$x .= "<td width=33% class=tableTitle align=center>". $i_general_Month."</td>\n";
$x .= "<td width=33% class=tableTitle align=center>".$i_SMS_MessageCount."</td>\n";
$x .= "<td width=33% class=tableTitle align=center>".$i_general_refresh."</td>\n";
$x .= "</tr>";
        
for($i=1;$i<=12;$i++)
{
	$css = ($i%2) ? "" : "2";
                                        
	$x .= "<tr>";
	$x .= "<td width=33% class=tableContent$css align=center>". $i."</td>\n";
	$x .= "<td width=33% class=tableContent$css align=center>". ($data[$i]+0) ."</td>\n";
	$x .= "<td width=33% class=tableContent$css align=center><a href='javascript:refresh($i);'><img src='/images/admin/icon_refresh.gif' border='0' align='absmiddle'></a></td>\n";
	$x .= "</tr>";
}

$x .= "</table>\n";

$sql = "select min(year) from INTRANET_SMS2_MONTHLY_REPORT";
$result = $li->returnArray($sql,1);
$start_year = $result[0][0];
$start_year = $start_year == "" ? date("Y") : $start_year;

$select_year = "";
$select_year .= "<select name=\"year\" onChange=\"window.location='?year='+this.value\">";
for($i=date("Y");$i>=$start_year;$i--)
		$select_year .= "<option value='$i' ". ($i==$year? "selected":"") .">$i</option>";
$select_year .= "</select>";

$functionbar = "<table width=560 border=0 cellpadding=1 cellspacing=0 align=center>\n";
$functionbar .= "<tr>\n";
$functionbar .= "<td>&nbsp;$i_general_Year:&nbsp;$select_year</td>\n";
$functionbar .= "<td align=right><a href='javascript:refresh(0);' class=iconLink><img src='/images/admin/icon_refresh.gif' border='0' align='absmiddle'>". $i_general_refresh . ($intranet_session_language=="en"?" ":"") . $i_Homework_AllRecords ."</a>&nbsp;</td>\n";
$functionbar .= "</tr>\n";
$functionbar .= "</table>\n";

?>
<script language="javascript">
<!--
	function refresh(month)
	{
		window.location='refresh.php?year=<?=$year?>&month='+month;
	}
//-->
</script>
<form name="form1" method="get">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '../',$i_SMS_UsageReport,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?=$functionbar?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?= $x ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>