<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

if ($retrival==1)
{
    # Retrieve from BL Server
    $xmsg = $i_SMS_con_DataRetrieved;
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if ($field=="") $field = 5;
if ($order=="") $order = 0;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 5; break;
}
$order = ($order == 1) ? 1 : 0;
if($filter == "") $filter = 1;
$filter = ($filter == 1) ? 1 : 0;
$user_field = getNameFieldWithLoginByLang("b.");

$sql = "SELECT
              a.MobileNumber, $user_field AS UserName, a.Message,
              IF (a.RecordStatus IS NULL,'$i_SMS_Status0',
              CASE a.RecordStatus
                   WHEN NULL THEN '$i_SMS_Status0'
                   WHEN 0 THEN '$i_SMS_Status2'
                   WHEN 1 THEN '$i_SMS_Status1'
                   WHEN 2 THEN '$i_SMS_Status2'
                   WHEN 3 THEN '$i_SMS_Status3'
                   WHEN 4 THEN '$i_SMS_Status4'
                   WHEN 5 THEN '$i_SMS_Status5'
                   WHEN 6 THEN '$i_SMS_Status6'
                   WHEN 7 THEN '$i_SMS_Status7'
                   WHEN 8 THEN '$i_SMS_Status8'
                   WHEN 9 THEN '$i_SMS_Status9' END)
              ,
              a.TimeStamp, a.DateInput
        FROM INTRANET_SMS_LOG as a
             LEFT OUTER JOIN INTRANET_USER as b ON a.ReceiverID = b.UserID
        WHERE (
               a.MobileNumber LIKE '%$keyword%' OR
               $user_field LIKE '%$keyword%' OR
               a.Message LIKE '%$keyword%'
               )
        ";
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.MobileNumber", "UserName","a.Message", "a.RecordStatus","a.TimeStamp","a.DateInput");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,5,0,0,0);
$li->wrap_array = array(0,0,25,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(0, $i_SMS_MobileNumber)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(1, $i_SMS_Recipient)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(2, $i_SMS_MessageContent)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(3, $i_SMS_Status)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(4, $i_SMS_TargetSendTime)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(5, $i_SMS_SubmissionTime)."</td>\n";
#$li->column_list .= "<td width=1 class=tableTitle>".$li->check("RoleID[]")."</td>\n";

$toolbar  = "<a class=iconLink href=javascript:checkPost(document.form1,'clear.php')>".newIcon()."$i_SMS_ClearRecords</a>".toolBarSpacer();
$toolbar  .= "<br><a class=iconLink href=javascript:checkPost(document.form1,'export.php')>".exportIcon()."$button_export</a>".toolBarSpacer();
$toolbar  .= "<br><a class=iconLink href=javascript:checkPost(document.form1,'refresh.php')>".newIcon()."$i_SMS_RetrieveRecords</a>".toolBarSpacer();
$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

$sentSql = "SELECT COUNT(*) FROM INTRANET_SMS_LOG WHERE RecordStatus = 2 OR RecordStatus = 0 OR RecordStatus IS NULL";
$result = $li->returnVector($sentSql);
$sent = $result[0]+0;

$quota = get_file_content("$intranet_root/admin/sms/limit.txt");
$removed = get_file_content("$intranet_root/admin/sms/removed.txt");
$removed += 0;
$sent += $removed;

$left = $quota - $sent;

$left = ($left<0? "<font color=red>$left</font>":"<font color=green>$left</font>");

$infobar = "$i_SMS_MessageSentSuccessfully: $sent<br>$i_SMS_MessageQuotaLeft: $left";
?>

<form name="form1" method="get">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '../',$i_SMS_UsageReport,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $infobar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

</form>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>