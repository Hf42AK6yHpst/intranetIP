<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$lexport = new libexporttext();

$user_field = getNameFieldWithLoginByLang("b.");

$sql = "SELECT
              a.MobileNumber, $user_field AS UserName, a.Message,
              IF (a.RecordStatus IS NULL,'$i_SMS_Status0',
              CASE a.RecordStatus
                   WHEN NULL THEN '$i_SMS_Status0'
                   WHEN 0 THEN '$i_SMS_Status2'
                   WHEN 1 THEN '$i_SMS_Status1'
                   WHEN 2 THEN '$i_SMS_Status2'
                   WHEN 3 THEN '$i_SMS_Status3'
                   WHEN 4 THEN '$i_SMS_Status4'
                   WHEN 5 THEN '$i_SMS_Status5'
                   WHEN 6 THEN '$i_SMS_Status6'
                   WHEN 7 THEN '$i_SMS_Status7'
                   WHEN 8 THEN '$i_SMS_Status8'
                   WHEN 9 THEN '$i_SMS_Status9' END)
              ,
              a.TimeStamp, a.DateInput
        FROM INTRANET_SMS_LOG as a
             LEFT OUTER JOIN INTRANET_USER as b ON a.ReceiverID = b.UserID
        WHERE (
               a.MobileNumber LIKE '%$keyword%' OR
               $user_field LIKE '%$keyword%' OR
               a.Message LIKE '%$keyword%'
               )
        ";
$li = new libdb();
$csv = "Mobile,Receiver Name,Message,Status,Real Transmission Time,Request Time\n";
$utf_csv = "Mobile \t Receiver Name \t Message,Status \t Real \t Transmission Time \t Request Time\r\n";
$result = $li->returnArray($sql,6);
for ($i=0; $i<sizeof($result); $i++)
{
     $delim = "";
     $utf_delim = "";
     for ($j=0; $j<sizeof($result[$i]); $j++)
     {
          $csv .= "$delim\"".$result[$i][$j]."\"";
          $utf_csv .= "$utf_delim ".$result[$i][$j]."";
          $delim = ",";
          $utf_delim = "\t";
     }
     $csv .= "\n";
     $utf_csv .= "\r\n";
}

if (!$g_encoding_unicode) {
	$filename = "file/export/smslog-".session_id()."-".time().".csv";
	$export_filepath = "$intranet_root/$filename";
	write_file_content($csv,$export_filepath);	
	
	header("Location: $intranet_httppath/$filename");
	intranet_closedb();
	
} else {
	$filename = "smslog-".session_id()."-".time().".csv";	
	$lexport->EXPORT_FILE($filename, $utf_csv);	
	intranet_closedb();	
}

?>