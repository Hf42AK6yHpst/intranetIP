<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libhttpclient.php");
intranet_opendb();

$li = new libdb();
$sql = "SELECT MIN(UNIX_TIMESTAMP(TimeStamp)) FROM INTRANET_SMS_LOG WHERE TimeStamp IS NOT NULL";
$result = $li->returnVector($sql);
$ts = $result[0];
if ($ts == "")
{
    $ts = mktime(0,0,0,1,1,2003);
}

$host = $orangesms_log_host;
$port = 80;
$lhttp = new libhttpclient($host,$port);
$lhttp->set_path("$orangesms_log_refreshaction");
$qstring = "SchoolName=".urlencode($orangesms_log_schoolname)."&ts=$ts";
$lhttp->post_data = $qstring;
$response = $lhttp->send_request();



#$url = "http://$orangesms_log_host$orangesms_log_refreshaction?$qstring";
#echo "URL: $url\n";
#$data = @fopen($url);
$data = $response;

#echo "Response: ".$data;

$info = explode("\n",$data);
for ($i=0; $i<sizeof($info); $i++)
{
     $array = explode(",",$info[$i]);
     list($jid,$timestamp,$status) = $array;
     $sql = "UPDATE INTRANET_SMS_LOG SET TimeStamp = '$timestamp', RecordStatus = '$status'
             WHERE JobID = '$jid'";
     $li->db_db_query($sql);
}

intranet_closedb();
header("Location: index.php?retrival=1");
?>