<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

$Payment = new libpayment();
intranet_opendb();

$TransactionID = $_REQUEST['TransactionID'];

$Payment->Cancel_Cash_Deposit($TransactionID);

intranet_closedb();
header("location: cancel_cash_log.php?msg=".$Lang['Payment']['DepositCancelSuccess']);
?>