<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libpayment.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$li = new libdb();
$sql = "CREATE TABLE IF NOT EXISTS TEMP_PPS_LOG (
 PPSAccount varchar(255),
 Amount float,
 InputTime datetime
)";
$li->db_db_query($sql);

if ($clear == 1)
{
    $sql = "DELETE FROM TEMP_PPS_LOG";
    $li->db_db_query($sql);
}

$sql = "SELECT COUNT(*) FROM TEMP_PPS_LOG";
$temp = $li->returnVector($sql);
$count = $temp[0];
if ($dup ==1)
{
    $xmsg = "$i_Payment_con_FileImportedAlready";
}if($no_trans==1){
	$xmsg = "<font color=red>$i_Payment_NoTransactions_Processed</font>";
}
?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../',$i_Payment_Menu_DataImport,'index.php',$i_Payment_Menu_Import_PPSLog,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<?


if ($count != 0)
{
?>
<form name="form1" method="GET" action="">
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td ><?=$i_Payment_Import_ConfirmRemoval?></td></tr>
<tr><td ><?=$i_Payment_Import_ConfirmRemoval2?></td></tr>
<tr><td><input type=image src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif"></td></tr>
</table>
<input type=hidden name=clear value=1>
</form>
<?
}
else
{

# Check previously stored value of ChargeDeduction
$pps_charge_from_file = trim(get_file_content("$intranet_root/file/pps_charge_deduction.txt"));
$temp = explode("\n",$pps_charge_from_file);
$pps_charge_from_file = ($temp[0]==1);

?>

<form name="form1" method="POST" action="pps_log_view_kenneth.php" enctype="multipart/form-data">
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=left colspan=2><?=$i_Payment_Import_Format_PPSLog?></td></tr>
<tr><td align=right nowrap><?php echo $i_select_file; ?>:</td><td><input type=file size=50 name=userfile></td></tr>
<tr><td align=right nowrap><?php echo $i_Payment_Import_PPS_FileType; ?>:</td><td><input type=radio name=PPSFileType value=0 CHECKED><?=$i_Payment_Import_PPS_FileType_Normal?> &nbsp;<input type=radio name=PPSFileType value=1><?=$i_Payment_Import_PPS_FileType_CounterBill?></td></tr>
<tr><td align=right nowrap>&nbsp;</td><td><? if ($pps_charge_from_file==1) {
?><font color=red><?=$i_Payment_Import_Format_PPSLog_Charge_Deduction?></font><br><?=$i_Payment_Import_PPS_ChargeSeparation?><? } ?></td></tr>
<tr><td align=right nowrap>&nbsp;</td><td>
</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
</form>

<?
}
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>
