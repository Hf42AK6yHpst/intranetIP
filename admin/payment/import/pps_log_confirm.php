<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libpayment.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$li = new libdb();
$filepath = $userfile;
$filename = $userfile_name;
# Check previously stored value of ChargeDeduction
$pps_charge_from_file = trim(get_file_content("$intranet_root/file/pps_charge_deduction.txt"));
$temp = explode("\n",$pps_charge_from_file);
$pps_auto_charge = ($temp[0]==1);

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: pps_log.php");
        exit();
} else {
        $file_content = get_file_content($filepath);
        $lpayment = new libpayment();

        $data = $lpayment->parsePPSLogFile($file_content);

        # Check PPS Record Date
        if ($lpayment->isPPSLogFileUpdatedBefore($PPSFileType))
        {
            header("Location: pps_log.php?dup=1");
            exit();
        }

        # Update TEMP_PPS_LOG
        $values = "";
        $delim = "";
        for ($i=0; $i<sizeof($data); $i++)
        {
             $record = $data[$i];
             $bill_account = $record['account'];
             $credit_amount = $record['amount'];
             /*
             if ($pps_auto_charge)
             {
                 $credit_amount -= LIBPAYMENT_PPS_CHARGE;
             }
             */
             $tran_date = $record['date'];
             $tran_hr = $record['hour'];
             $tran_min = $record['min'];

             if ($PPSFileType==1) # Counter bill
             {
                 # New for counter bill
                 $tran_date_counter = $record['date_counter'];
                 $tran_hr_counter = $record['hr_counter'];
                 $tran_min_counter = $record['min_counter'];
                 if ($tran_date_counter != "" && $tran_hr_counter!="" && $tran_min_counter!="")
                 {
                     $tran_date = $tran_date_counter;
                     $tran_hr = $tran_hr_counter;
                     $tran_min = $tran_min_counter;
                 }
             }
             else
             {
             }

             $values .= "$delim ('$bill_account','$credit_amount','$tran_date$tran_hr$tran_min"."00"."')";
             $delim = ",";
        }
        $sql = "INSERT INTO TEMP_PPS_LOG (PPSAccount,Amount,InputTime) VALUES $values";
        $li->db_db_query($sql);
}

$namefield = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT a.PPSAccount, IF(b.UserID IS NULL,'$i_Payment_AccountNoMatch',$namefield), b.UserLogin, a.Amount, DATE_FORMAT(a.InputTime,'%Y-%m-%d %H:%i')
        FROM TEMP_PPS_LOG as a LEFT OUTER JOIN PAYMENT_ACCOUNT as c ON a.PPSAccount = c.PPSAccountNo
             LEFT OUTER JOIN INTRANET_USER as b ON c.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2)
        ORDER BY a.InputTime";
$result = $li->returnArray($sql,5);


//$hasTransaction = false;
//if(sizeof($result)>0)
	$hasTransaction = true;

if ($pps_auto_charge)
{
    $display = "<tr>
<td class=tableTitle width=100>$i_Payment_Field_PPSAccountNo</td>
<td class=tableTitle width=170>$i_UserStudentName</td>
<td class=tableTitle width=80>$i_UserLogin</td>
<td class=tableTitle width=50>$i_Payment_Field_CreditAmount</td>
<td class=tableTitle width=50>$i_Payment_PPS_Charge</td>
<td class=tableTitle width=110>$i_Payment_Field_TransactionTime</td>
</tr>\n";
}
else
{
    $display = "<tr>
<td class=tableTitle width=100>$i_Payment_Field_PPSAccountNo</td>
<td class=tableTitle width=200>$i_UserStudentName</td>
<td class=tableTitle width=80>$i_UserLogin</td>
<td class=tableTitle width=70>$i_Payment_Field_CreditAmount</td>
<td class=tableTitle width=110>$i_Payment_Field_TransactionTime</td>
</tr>\n";
}


$total_amount = 0;
if ($PPSFileType==1) # Counter bill
{
    $handling_charge = $lpayment->getWebDisplayAmountFormat(LIBPAYMENT_PPS_CHARGE_COUNTER_BILL);
}
else
{
    $handling_charge = $lpayment->getWebDisplayAmountFormat(LIBPAYMENT_PPS_CHARGE);
}

for ($i=0; $i<sizeof($result); $i++)
{
     list($pps,$name,$login,$amount,$tran_time) = $result[$i];
     $total_amount += $amount;
     #$original_amount = $amount+LIBPAYMENT_PPS_CHARGE;
     $amount = $lpayment->getWebDisplayAmountFormat($amount);
     #$original_amount = number_format($original_amount,1);

     $css = ($i%2? "":"2");
     if ($pps_auto_charge)
     {
         $display .= "<tr class=tableContent$css><td>$pps</td><td>$name</td><td>$login</td><td>".$amount."</td><td>".$handling_charge."</td><td>$tran_time</td></tr>\n";

     }
     else
     {
         $display .= "<tr class=tableContent$css><td>$pps</td><td>$name</td><td>$login</td><td>".$amount."</td><td>$tran_time</td></tr>\n";
     }
}
include_once("../../../templates/adminheader_setting.php");

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../',$i_Payment_Menu_DataImport,'index.php',$i_Payment_Menu_Import_PPSLog,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<form name="form1" method="GET" action="pps_log_update.php">
<?=$i_Payment_Import_Confirm?><br><Br>
&nbsp;<font color=red><?=$i_Payment_TotalAmount?> : <?=$lpayment->getWebDisplayAmountFormat($total_amount)?><br>
<?=($pps_auto_charge?$i_Payment_Import_Format_PPSLog_Charge_Deduction:"")?></font><br>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<?=$display?>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<BR>
<table width=560 border=0 cellpadding=2 cellspacing=0 align='center'>
<Tr><td><font color=red>*</font></td><td><?=$i_Payment_AccountNoMatch_Remark?></td></tr>
</table>
<table width=560 cellspacing=0 cellpadding=0>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?php if($hasTransaction){?>
<input type="image" alt="<?=$button_confirm?>" src="/images/admin/button/s_btn_confirm_<?=$intranet_session_language?>.gif" border='0'>
<?php } ?>
<a href=pps_log.php?clear=1><img alt="<?=$button_cancel?>" border=0 src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=confirm value=1>
<input type=hidden name=FileDate value="<?=$lpayment->pps_file_date?>">
<input type=hidden name=PPSFileType value="<?=$PPSFileType?>">
</form>

<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>
