<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$limport = new libimporttext();

define("ERROR_CODE_NO_MATCH_STUDENT",1);
define("ERROR_CODE_CRASHED_PPS_NO",2);
define("ERROR_CODE_MISSING_PPS_NO",3);
define("ERROR_CODE_ALREADY_HAS_PPS_NO",4);

$li = new libdb();
$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename)){ 	# import failed
        header("Location: pps_account.php?failed=2");
} else {
        /*
		$ext = strtoupper($lf->file_ext($filename));
        if($ext == ".CSV") {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $lf->file_read_csv($filepath);
                array_shift($data);                   # drop the title bar
        }
        else
        {
            header("Location: pps_account.php?failed=2");
            exit();
        }
        */
        $data = $limport->GET_IMPORT_TXT($filepath);
		array_shift($data);		# drop the title bar
		
		$total_row = 0;
		
        # Get Existing PPS Account
        $sql = "SELECT DISTINCT PPSAccountNo FROM PAYMENT_ACCOUNT WHERE PPSAccountNo IS NOT NULL
                AND PPSAccountNo != ''";
        $current_pps = $li->returnVector($sql);

        # Get Existing Students
        
        $sql = "SELECT ClassName, ClassNumber, UserID,UserLogin
                FROM INTRANET_USER
                WHERE RecordType = 2 AND RecordStatus = 1
                      AND ClassName != '' AND ClassNumber != ''
                ORDER BY ClassName, ClassNumber";
		/*
        $sql = "SELECT UserLogin, UserID
		        FROM INTRANET_USER
		        WHERE RecordType = 2 AND RecordStatus = 1
		              AND ClassName != '' AND ClassNumber != ''
		        ORDER BY ClassName, ClassNumber";
		*/
        $result = $li->returnArray($sql,4);
        for ($i=0; $i<sizeof($result); $i++)
        {
             list($class,$classnum,$uid,$userlogin) = $result[$i];
             if($format==1){
	             $students[$class][$classnum]=$uid;
	         }
             else if($format==2){
	             	$students[$userlogin] = $uid;
	         }
        }

        # Counts
        $count_successful = 0;
        $count_no_match_student = 0;
        $count_crashed_pps_no = 0;
        $count_student_has_pps_already = 0;
        $count_pps_no_missing = 0;
        $array_error = array();

        # Update PAYMENT_ACCOUNT
        for ($i=0; $i<sizeof($data); $i++)
        {
	        if(trim(implode("",$data[$i]))=="") continue;
	        
	        $total_row++;
	        
	        if($format==1){ # Format 1
		         list($class,$classnum,$pps) = $data[$i];

	             if ($class == "" && $classnum == "")
	             {
	                 $count_no_match_student++;
	                 $array_error[] = array($i,ERROR_CODE_NO_MATCH_STUDENT);
	                 continue;
	             }

	             $targetUID = $students[$class][$classnum];
		    }
	        else if($format==2){ # Format 2
	        
                 list($u_login,$pps) = $data[$i];
	             if ($u_login == "")
	             {
		             $no_match[] =array($data[$i],$i);
	                 $count_no_match_student++;
	                 $array_error[] = array($i,ERROR_CODE_NO_MATCH_STUDENT);
	                 continue;
	             }
	             
             	 $targetUID = $students[$u_login];

		    }


             if ($targetUID == "")
             {
	             
                 $count_no_match_student++;
                 $array_error[] = array($i,ERROR_CODE_NO_MATCH_STUDENT);
                 continue;
             }

             if ($pps != "")
             {
                 if (in_array($pps,$current_pps))
                 {
                     $count_crashed_pps_no++;
                     $array_error[] = array($i,ERROR_CODE_CRASHED_PPS_NO);
                     continue;
                 }
                 else $current_pps[] = $pps;
             }
             else
             {
                 $count_pps_no_missing++;
                 $array_error[] = array($i,ERROR_CODE_MISSING_PPS_NO);
                 continue;
             }

             $sql_1 = "INSERT IGONRE INTO PAYMENT_ACCOUNT (StudentID,PPSAccountNo,Balance) VALUES
                     ($targetUID,'$pps',0)";
             $li->db_db_query($sql_1);
             if ($li->db_affected_rows() != 1)
             {
                 $sql = "UPDATE PAYMENT_ACCOUNT SET PPSAccountNo = '$pps' WHERE StudentID = $targetUID AND (PPSAccountNo IS NULL OR PPSAccountNo = '')";
                 $li->db_db_query($sql);
                 /* For not update */
                 if ($li->db_affected_rows() != 1)
                 {
                     $array_error[] = array($i,ERROR_CODE_ALREADY_HAS_PPS_NO);
                     $count_student_has_pps_already++;
                 }
                 else
                 {
                     $count_successful++;
                 }

             }
             else
             {
                 $count_successful++;
             }
        }

}
?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../',$i_Payment_Menu_DataImport,'index.php',$i_Payment_Menu_Import_PPSLink,'pps_account_list.php',$button_import,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>
<blockquote>
<br>
<span class="extraInfo">[<span class=subTitle><?=$i_Payment_Import_PPSLink_Result?></span>]</span>
<br>
<table width=80% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 >
<tr><td class=tableTitle><?=$i_Payment_Import_PPSLink_Result_Summary_TotalRows?></td><td><?=$total_row?></td></tr>
<tr><td class=tableTitle><?=$i_Payment_Import_PPSLink_Result_Summary_Successful?></td><td><?=$count_successful?></td></tr>
<tr><td class=tableTitle><?=$i_Payment_Import_PPSLink_Result_Summary_NoMatch?></td><td><?=$count_no_match_student?></td></tr>
<tr><td class=tableTitle><?=$i_Payment_Import_PPSLink_Result_Summary_Occupied?></td><td><?=$count_crashed_pps_no?></td></tr>
<tr><td class=tableTitle><?=$i_Payment_Import_PPSLink_Result_Summary_Missing?></td><td><?=$count_pps_no_missing?></td></tr>
<tr><td class=tableTitle><?=$i_Payment_Import_PPSLink_Result_Summary_ExistAlready?></td><td><?=$count_student_has_pps_already?></td></tr>
</table>
<br>
<? if (sizeof($array_error) != 0) { ?>
<br>
<span class="extraInfo">[<span class=subTitle><?=$i_Payment_Import_PPSLink_Result_ErrorMsg_NotSuccessful?></span>]</span>
<br>
<table width=80% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 >
<tr class=tableTitle><td>#</td><td><?=$i_Payment_Import_PPSLink_Result_ErrorMsg_Reason?></td></tr>
<? for ($i=0; $i < sizeof($array_error); $i++)
{
   list ($t_row, $t_type) = $array_error[$i];
   $t_row++;
   $css = ($i%2? "":"2");
   switch ($t_type)
   {
           case ERROR_CODE_NO_MATCH_STUDENT:
                $t_string_error = $i_Payment_Import_PPSLink_Result_ErrorMsg_NoMatch; break;
           case ERROR_CODE_CRASHED_PPS_NO:
                $t_string_error = $i_Payment_Import_PPSLink_Result_ErrorMsg_Occupied; break;
           case ERROR_CODE_MISSING_PPS_NO:
                $t_string_error = $i_Payment_Import_PPSLink_Result_ErrorMsg_Missing; break;
           case ERROR_CODE_ALREADY_HAS_PPS_NO:
                $t_string_error = $i_Payment_Import_PPSLink_Result_ErrorMsg_ExistAlready; break;
           default:
                   $t_string_error = "Unknown Error";

   }
?>
<tr class=tableContent<?=$css?>><td><?=$t_row?></td><td><?=$t_string_error?></td></tr>
<?
}
?>
</table>
<? } ?>
<br><br>
<a class=functionlink_new href=pps_account_list.php><?=$i_Payment_Import_PPSLink_Result_GoToIndex?></a>
</blockquote>
<?

intranet_closedb();
include_once("../../../templates/adminfooter.php");

#header("Location: pps_account_list.php?msg=1");
?>
