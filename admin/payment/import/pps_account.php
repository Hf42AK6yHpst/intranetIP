<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$linterface = new interface_html();
$limport = new libimporttext();

?>

<form name="form1" method="POST" action="pps_account_update.php" enctype="multipart/form-data">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../',$i_Payment_Menu_DataImport,'index.php',$i_Payment_Menu_Import_PPSLink,'pps_account_list.php',$button_import,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_select_file; ?>:</td><td><input type=file size=50 name=userfile><br />
<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?></td></tr>
<!--<tr><td align=right nowrap>&nbsp;</td><td>
<br><?=$i_Payment_Import_Format_PPSLink?>
<br><a class=functionlink_new href=pps_account_sample.csv><?=$i_general_clickheredownloadsample?></a>
</td></tr>-->
<tr><td align=right nowrap><?=$i_general_Format?>:</td><td>
		<table border=0 cellpadding=0 cellspacing=0><tr><Td>
	<input type=radio name=format value=1 CHECKED></td><td><?=$i_Payment_Import_Format_PPSLink?><Br>&nbsp;<a class=functionlink_new href="<?= GET_CSV("pps_account_sample.csv")?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>
	<tr><td><input type=radio name=format value=2></td><td><?=$i_Payment_Import_Format_PPSLink2?><br>&nbsp;<a class=functionlink_new href="<?= GET_CSV("pps_account_sample2.csv")?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>
	</td></tr></table><br>&nbsp;
</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
</form>

<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>
