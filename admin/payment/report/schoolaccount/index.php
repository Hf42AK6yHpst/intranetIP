<?php
// using kenneth
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();

        # date range
        $today_ts = strtotime(date('Y-m-d'));
        if($FromDate=="")
                $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
        if($ToDate=="")
                $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_cond =" DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='$FromDate' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='$ToDate'";
        $sql="SELECT a.Amount,
                        a.Details,
                        a.TransactionType,
                        c.RecordType
                        FROM PAYMENT_OVERALL_TRANSACTION_LOG AS a
                             LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as c ON a.RelatedTransactionID = c.TransactionID
                        WHERE $date_cond ORDER BY a.Details, c.RecordType";
$lb = new libdbtable("","","");
$li = new libpayment();
$temp = $li->returnArray($sql,4);


$ary_types = array(1,2,3,6,7,8,9,10);

$x="<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
$x.="<Tr>";
$x.="<Td rowspan=2 class=tableTitle width=40% align=center style='vertical-align:middle'>$i_Payment_SchoolAccount_Detailed_Transaction</td>";
$x.="<td rowspan=2 class=tableTitle widht=15% align=center style='vertical-align:middle'>$i_Payment_SchoolAccount_Deposit</td>";
$x.="<td colspan=3 class=tableTitle align=center width=45% style='vertical-align:middle'>$i_Payment_SchoolAccount_Expenses</td>";
$x.="</tr>";
$x.="<Tr><td class=tableTitle align=center style='vertical-align:middle'>$i_Payment_SchoolAccount_PresetItem</td><Td class=tableTitle align=center style='vertical-align:middle'>$i_Payment_SchoolAccount_SinglePurchase</td><td class='tableTitle' align=center style='vertical-align:middle' width=12%>$i_Payment_SchoolAccount_Other</td></tr>";


$income=0;
$expense_item=0;
$expense_single=0;
$expense_other=0;


for($i=0;$i<sizeof($temp);$i++){
        list($amount,$detail,$type, $credit_transaction_type) = $temp[$i];
        if(in_array($type,$ary_types)){
                #$result[$type]["$detail"] += $amount+0;
                switch($type){
                        case 1        :
                                       $result[$type][$credit_transaction_type] += $amount;
                                       $income+=$amount+0;
                                       break;

                        case 2        :
                                       $result[$type]["$detail"] += $amount+0;
                                       $expense_item +=$amount+0;
                                       break;

                        case 3        :
                                       $result[$type]["$detail"] += $amount+0;
                                       $expense_single+=$amount+0;
                                       break;

                        case 6        :
                                       $result[$type]["$detail"] += $amount+0;
                                       $income+=$amount+0;
                                       break;

                        case 7        :
                                       $result[$type]["all"] += $amount;
                                       $expense_other +=$amount+0;
                                       break;

                        case 8        :
                                       $result[$type]["all"] += $amount;
                                       $expense_other +=$amount+0;
                                       break;
												
												case 9        :
                                       $result[$type]["all"] += $amount;
                                       $expense_other +=$amount+0;
                                       break;
                         
                        case 10        :
                                       $result[$type]["all"] += $amount;
                                       $expense_other +=$amount+0;
                                       break;
                        default        : break;
                }

        }

}

if(sizeof($result)<=0){
        $x.="<tr><td colspan=5 align=center class=tableContent2 height=40 style='vertical-align:middle'>$i_no_record_exists_msg<td></tr>";
}else{
        $count=0;

        # deposit
        $list_item = $result[1];
        if(sizeof($list_item)>0){
                foreach($list_item as $credit_type => $t_total){
                        $css =$count%2==0?"tableContent":"tableContent2";
                        $count++;
                        switch ($credit_type)
                        {
                                case 1: $name = $i_Payment_Credit_TypePPS; break;
                                case 2: $name = $i_Payment_Credit_TypeCashDeposit; break;
                                case 3: $name = $i_Payment_Credit_TypeAddvalueMachine; break;
                                default : $name = $i_Payment_Credit_TypeUnknown . "(Type=$credit_type)";

                        }
                        $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td></tr>";
                }
                //$x.="<tr><td colspan=4 class='tableContent'>&nbsp;</td></tr>";
        }

        # payment cancellation
        $list_item = $result[6];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $t_total){
                        $css =$count%2==0?"tableContent":"tableContent2";
                        $count++;
                        $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td></tr>";
                }
        }
        //$x.="<tr><td colspan=4 class='tableContent'>&nbsp;</td></tr>";

        # payment item
        $list_item = $result[2];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $t_total){
                        $css =$count%2==0?"tableContent":"tableContent2";
                        $count++;
                        $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td></tr>";
                }
        }

        # single purchase
        $list_item = $result[3];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $t_total){
                        $css =$count%2==0?"tableContent":"tableContent2";
                        $count++;
                        $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td><td class='$css'>&nbsp;</td></tr>";
                }
        }
        //$x.="<tr><td colspan=4 class='tableContent'>&nbsp;</td></tr>";

        # refund
        $list_item = $result[7];
        if(sizeof($list_item)>0){
                $item_name = $i_Payment_action_refund;
                $s_total =0;
                $css =$count%2==0?"tableContent":"tableContent2";
                foreach($list_item as $name => $t_total){
                        $count++;
                        $s_total+=$t_total;
                }
                /*
                foreach($list_item as $name => $t_total){
                        $css =$count%2==0?"tableContent":"tableContent2";
                        $count++;
                        $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>$".number_format($t_total,1)."</td></tr>";
                }
                */
                $x.="<tr><td class='$css'>$item_name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($s_total)."</td></tr>";
        }

        # pps charges
        $list_item = $result[8];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $t_total){
                        $css =$count%2==0?"tableContent":"tableContent2";
                        $count++;
                        $x.="<tr><td class='$css'>$i_Payment_PPS_Charge&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td></tr>";
                }
        }
				
				# Cancel Deposit Charge
        $list_item = $result[9];
        if(sizeof($list_item)>0){
        	$item_name = $Lang['Payment']['CancelDepositDescription'];
          $s_total =0;
          $css =$count%2==0?"tableContent":"tableContent2";
          foreach($list_item as $name => $t_total){
                  $count++;
                  $s_total+=$t_total;
          }
          
          $x.="<tr><td class='$css'>$item_name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($s_total)."</td></tr>";
          /*foreach($list_item as $name => $t_total){
                  $css =$count%2==0?"tableContent":"tableContent2";
                  $count++;
                  $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td></tr>";
          }*/
        }
        
        # Donation to school
        $list_item = $result[10];
        if(sizeof($list_item)>0){
        	$item_name = $Lang['Payment']['DonateBalanceDescription'];
          $s_total =0;
          $css =$count%2==0?"tableContent":"tableContent2";
          foreach($list_item as $name => $t_total){
                  $count++;
                  $s_total+=$t_total;
          }
          
          $x.="<tr><td class='$css'>$item_name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($s_total)."</td></tr>";
          /*      foreach($list_item as $name => $t_total){
                        $css =$count%2==0?"tableContent":"tableContent2";
                        $count++;
                        $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td></tr>";
                }*/
        }
        
        # Total
        $total_income = $income+0;

        //$css =$count%2==0?"tableContent":"tableContent2";
        //$count++;
        $css ="tableTitle2";
        $x.="<tr><td class='$css' align=right>$i_Payment_SchoolAccount_Total</td><td class='$css'>".$li->getWebDisplayAmountFormat($total_income)."</td><Td class='$css'>".$li->getWebDisplayAmountFormat($expense_item)."</td><td class='$css'>".$li->getWebDisplayAmountFormat($expense_single)."</td><Td class='$css'>".$li->getWebDisplayAmountFormat($expense_other)."</td></tr>";

        # Summary
        $total_expense = $expense_item + $expense_single + $expense_other+0;
        $net_income = $total_income - $total_expense;
        $net_income_str = ($net_income>0)? $li->getWebDisplayAmountFormat($net_income):"<font color=red>(".$li->getWebDisplayAmountFormat($net_income).")</font>";
        $y="<table width=560 border=0 cellpadding=0 cellspacing=0 align=center><tr><TD>";
        $y.="<table border=0>";
        $y.="<tr><td align=right>$i_Payment_SchoolAccount_TotalIncome:</td><td>".$li->getWebDisplayAmountFormat($total_income)."</td></tr>";
        $y.="<tr><td align=right>$i_Payment_SchoolAccount_TotalExpense:</td><td>".$li->getWebDisplayAmountFormat($total_expense)."</td></tr>";
        $y.="<tr><td align=right>$i_Payment_SchoolAccount_NetIncomeExpense:</td><td>$net_income_str</td></tr>";
        $y.="</table></td></tr></table><Br>";

}

$x.="</table>";

$display=$x;

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
$toolbar2 = "<a class=iconLink href=javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate')>".exportIcon()."$button_export</a>";

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_StatisticsReport,'../index.php',$i_Payment_Menu_Report_SchoolAccount,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
        var css_array = new Array;
        css_array[0] = "dynCalendar_free";
        css_array[1] = "dynCalendar_half";
        css_array[2] = "dynCalendar_full";
        var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
                  // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                                           document.forms['form1'].FromDate.value = dateValue;

          }
          function calendarCallback2(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                                           document.forms['form1'].ToDate.value = dateValue;

          }
        function checkForm(formObj){
                if(formObj==null)return false;
                        fromV = formObj.FromDate;
                        toV= formObj.ToDate;
                        if(!checkDate(fromV)){
                                        //formObj.FromDate.focus();
                                        return false;
                        }
                        else if(!checkDate(toV)){
                                                //formObj.ToDate.focus();
                                                return false;
                        }
                                return true;
        }
        function checkDate(obj){
                         if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
                        return true;
        }
        function submitForm(obj){
                if(checkForm(obj))
                        obj.submit();
        }
        function openPrintPage()
        {
         newWindow("print.php?FromDate=<?=$FromDate?>&ToDate=<?=$ToDate?>",8);
        }
                function exportPage(obj,url){
                        old_url = obj.action;
                obj.action=url;
                obj.submit();
                obj.action = old_url;

        }
</script>
<form name="form1" method="get" action=''>
<!-- date range -->
<table border=0 width=560 align=center>
        <tr>
                <td nowrap class=tableContent>
                <?=$i_Profile_From?>
                        <input type=text name=FromDate value="<?=$FromDate?>" size=10>
                                <script language="JavaScript" type="text/javascript">
                                        <!--
                                                startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
                                        //-->
                                </script>&nbsp;
                         <?=$i_Profile_To?>
                         <input type=text name=ToDate value="<?=$ToDate?>" size=10>
                                <script language="JavaScript" type="text/javascript">
                                        <!--
                                                startCal2 = new dynCalendar('startCal2', 'calendarCallback2', '/templates/calendar/images/');
                                        //-->
                                </script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
 <a href='javascript:submitForm(document.form1)'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0' align='absmiddle'></a></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

</form>
<?=$y?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?=$lb->displayFunctionbar("-","-",$toolbar,"")?></td></tr>
<tr><td class=admin_bg_menu><?=$lb->displayFunctionbar("-","-",$toolbar2,"")?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?=$display?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table><table width=560 border=0 cellpadding=0 cellspacin=0 align=center>
<tr><td align=right>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table> <BR><BR>

<?php
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
