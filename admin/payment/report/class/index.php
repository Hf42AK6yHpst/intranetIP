<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();

$lclass = new libclass();

$select_class = $lclass->getSelectClass("name=ClassName",$ClassName,1);

/* developing by Ronald on 20080930
$studentRange = "<input type=\"checkbox\" name=\"DataRange[]\" value=\"1\" checked>Show Current Students</input>";
$studentRange .= "<input type=\"checkbox\" name=\"DataRange[]\" value=\"2\" checked>Show Archived Students</input>";
if($special_feature['alumni'])
{
	$studentRange .= "<input type=\"checkbox\" name=\"DataRange[]\" value=\"3\" checked>Show Alumni Students</input>";
}
*/	

# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
	$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
	$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
	
	
$format_array = array(array(0,"Web"),array(1,"CSV"));
$select_format = getSelectByArray($format_array, "name=format onChange='showNotice(this)'",$format,0,1);

$display_mode_array= array(array(0,	$i_Payment_Class_Payment_Report_Display_Mode_1),array(1,$i_Payment_Class_Payment_Report_Display_Mode_2));
$select_display_mode=getSelectByArray($display_mode_array,"name='display_mode' onChange='showNotice(this)'",$display_mode,0,1);
if($display_mode==1 && $format!=1) {
	$select_display_mode.= "<span class='extraInfo'><BR> ( $i_Payment_Class_Payment_Report_Notice )</span>";
	
	$select_items_per_page="<SELECT name='ItemPerPage'>";
	$select_items_per_page.="<OPTION value=''>$i_status_all</OPTION>\n";
	for($i=1;$i<21;$i++){
		$select_items_per_page.="<OPTION value='$i'>$i</OPTION>\n";
	}
	$select_items_per_page.="</SELECT>";

}
?>


<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_StatisticsReport,'../',$i_Payment_Class_Payment_Report,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_free";
	css_array[1] = "dynCalendar_half";
	css_array[2] = "dynCalendar_full";
	var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
function showNotice(obj){

	obj.form.action='';
	obj.form.target='';
	obj.form.submit();
}
	          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           		document.forms['form1'].FromDate.value = dateValue;
                           
          }
          function calendarCallback2(date, month, year)
          {								
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
		                           document.forms['form1'].ToDate.value = dateValue;

          }
	function checkDate(obj){
 			if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
			return true;
	}
    function checkForm(formObj){
		if(formObj==null)return false;
		
			if (!formObj.StudentTypesCURRENT.checked && !formObj.StudentTypesREMOVED.checked)
			{
				alert("<?=$msg_check_at_least_one?>");
				formObj.StudentTypesCURRENT.checked = true;
				formObj.StudentTypesREMOVED.checked = true;
				formObj.StudentTypesCURRENT.focus();
				return false;
			}		
		
			fromV = formObj.FromDate;
			toV= formObj.ToDate;
			objClass = formObj.ClassName;
			if(!checkDate(fromV)){
					return false;
			}
			else if(!checkDate(toV)){
						return false;
			}else if(!check_select(objClass,'<?=$i_Discipline_System_alert_PleaseSelectClass?>','')){
				return false;
			}
			return true;
	}

	function submitForm(obj){
		if(checkForm(obj))
			obj.submit();	
	}
	
</script>

<form name=form1 action='class_report.php' method='post' target='_blank'>
<!-- date range -->
<table border=0 width=560 align=center>
<tr><td align=right width='30%'><?php echo $i_UserClassName; ?>:</td><td><?=$select_class?></td></tr>
<tr><td align=right >&nbsp;</td><td><input type="checkbox" name="StudentTypesCURRENT" id="StudentTypesCURRENT" value="1" checked="checked" /><label for="StudentTypesCURRENT"><?=$i_Payment_StudentStatus_NonRemoved?></label>
<input type="checkbox" name="StudentTypesREMOVED" id="StudentTypesREMOVED" value="1" checked="checked" /><label for="StudentTypesREMOVED"><?=$i_Payment_StudentStatus_Removed?></label>
</td></tr>

<tr><td align="right" width="30%"><?=$i_StaffAttendance_Report_Date_Range?>:</td><td><?=$studentRange;?></td></tr>

<tr><td align=right><?=$i_Profile_From?>:</td><td>
			<input type=text name=FromDate value="<?=$FromDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
					//-->
				</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
</td></tr>
<tr><td align=right><?=$i_Profile_To?>:</td><td>
		 	<input type=text name=ToDate value="<?=$ToDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal2 = new dynCalendar('startCal2', 'calendarCallback2', '/templates/calendar/images/');
					//-->
				</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
</td></tr>
<tr><Td align=right><?=$i_Payment_Class_Payment_Report_Display_Mode?>:</td><td>
<?=$select_display_mode?>
</td></tr>
<?php if($display_mode==1 && $format!=1){?>
<tr><Td align=right><?=$i_Payment_Class_Payment_Report_No_of_Items?>:</td><td>
<?=$select_items_per_page?>
</td></tr>
<?php } ?>
<tr><td align=right><?=$i_general_Format?>:</td><td><?=$select_format?><br>&nbsp;</td></tr>
<!--<tr><td colspan='2'><hr size=1 ></td></tr>-->
<tr><td></td><Td><a href='javascript:submitForm(document.form1)'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0' align='absmiddle'></a></td></tr>
</table>


</form>
<?
intranet_closedb();
include_once("../../../../templates/adminfooter.php");
?>