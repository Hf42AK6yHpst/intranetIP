<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();
$lpayment = new libpayment();
$lclass = new libclass();

# date range
$today_ts = strtotime(date('Y-m-d'));
if($TargetStartDate=="")
	$TargetStartDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($TargetEndDate=="")
	$TargetEndDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
	
$class_level_selection = $lclass->getSelectLevel("name=\"TargetClassLevel\" onChange=\"document.form1.flag.value=0; document.form1.action=''; this.form.submit();\" ", $TargetClassLevel);

if($TargetClassLevel != "") {
	
	$level_id = $lclass->getLevelID($TargetClassLevel);
	
	$arr_class_list = $lclass->returnClassListByLevel($level_id);
	if(sizeof($arr_class_list)>0) {
		//$class_name_selection = getSelectByArray($arr_class_list," name=\"TargetClassID[]\" MULTIPLE size=\"5\" onClick=\"document.form1.flag.value=0; this.form.submit();\" ",$TargetClassID,0);
		
		$class_name_selection = "<SELECT name=\"TargetClassID[]\" MULTIPLE size=\"5\" onChange=\"document.form1.flag.value=0; document.form1.action=''; this.form.submit();\" >";
		for($i=0; $i<sizeof($arr_class_list); $i++) {
			list($class_id, $class_name)=$arr_class_list[$i];
			if((sizeof($TargetClassID)>0) && (in_array($class_id,$TargetClassID))) {
				$x .= "<OPTION value='$class_id' SELECTED>$class_name</OPTION>\n";
			} else {
				$x .= "<OPTION value='$class_id'>$class_name</OPTION>\n";
			}
		} 
		
		$class_name_selection .= $x;
		$class_name_selection .= "</SELECT>";
	} else {
		$class_name_selection = "<SELECT name=\"TargetClassID[]\" MULTIPLE size=\"5\" onChange=\"document.form1.flag.value=0; document.form1.action=''; this.form.submit();\" >";
		$class_name_selection .= "</SELECT>";
	}
	
	$btn_select_all_class = "<a href=\"javascript:selectAll('TargetClassID[]')\"><img src='$image_path/admin/button/s_btn_select_all_$intranet_session_language.gif' border='0'></a>";
	$class_name_selection .= "&nbsp;".$btn_select_all_class;
}

if($TargetClassID != "") {

	$class_list = implode(",",$TargetClassID);
	
	$namefield = getNameFieldByLang2("a.");
	$sql = "SELECT a.UserID, CONCAT($namefield,' (',a.ClassName,'-',a.ClassNumber,')') FROM INTRANET_USER AS a INNER JOIN INTRANET_CLASS AS b ON a.ClassName = b.ClassName WHERE b.ClassID IN ($class_list) ORDER BY b.ClassName, a.ClassNumber";
	$arr_student = $lpayment->returnArray($sql,2);
	
	if(sizeof($arr_student)>0){
		//$student_selection = getSelectByArray($arr_student_selected, " name=\"TargetStudentID\" ");
		
		$student_selection = "<SELECT name=\"TargetStudentID[]\" MULTIPLE size=\"5\" >";
		for($i=0; $i<sizeof($arr_student); $i++) {
			list($student_id, $student_name) = $arr_student[$i];
			if((sizeof($TargetStudentID)>0) && (in_array($student_id,$TargetStudentID))) {
				$y .= "<OPTION value='$student_id' SELECTED>$student_name</OPTION>\n";
			} else {
				$y .= "<OPTION value='$student_id'>$student_name</OPTION>\n";
			}
		}
		$student_selection .= $y;
		$student_selection .= "</SELECT>";
	} else {
		$student_selection = "<SELECT name=\"TargetStudentID[]\" MULTIPLE size=\"5\" >";
		$student_selection .= "</SELECT>";
	}
	
	$btn_select_all_student = "<a href=\"javascript:selectAll('TargetStudentID[]')\"><img src='$image_path/admin/button/s_btn_select_all_$intranet_session_language.gif' border='0'></a>";
	$student_selection .= "&nbsp;".$btn_select_all_student;
}
?>

<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
 <script LANGUAGE="javascript">
	 var css_array = new Array;
	 css_array[0] = "dynCalendar_free";
	 css_array[1] = "dynCalendar_half";
	 css_array[2] = "dynCalendar_full";
	 var date_array = new Array;
 </script>

 <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
 <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
 <script type="text/javascript">
 <!--
	  // Calendar callback. When a date is clicked on the calendar
	  // this function is called so you can do as you want with it
	  function startCalendarCallback(date, month, year)
	  {
			   if (String(month).length == 1) {
				   month = '0' + month;
			   }

			   if (String(date).length == 1) {
				   date = '0' + date;
			   }
			   dateValue =year + '-' + month + '-' + date;
			   document.forms['form1'].TargetStartDate.value = dateValue;
	  }
	  function endCalendarCallback(date, month, year)
	  {
			   if (String(month).length == 1) {
				   month = '0' + month;
			   }

			   if (String(date).length == 1) {
				   date = '0' + date;
			   }
			   dateValue =year + '-' + month + '-' + date;
			   document.forms['form1'].TargetEndDate.value = dateValue;
	  }
 // -->
 </script>

<script language="javascript">
	function selectAll(element)
	{
		var obj = document.form1;
		var s = obj.elements[element];
		
		for(i=0; i<s.length; i++){
			s[i].selected = true;
		}
		obj.flag.value = 0;
		obj.submit();
	}

	function check_multi_select(obj, element)
	{
		var s = obj.elements[element];
		var select_element = 0;
		
		for(i=0; i<s.length; i++){
			if(s[i].selected){
				select_element++;
			}
		}
		
		if(select_element>0){
			return true;
		} else {
			return false;
		}
	}
		
	function checkForm()
	{
		var obj = document.form1;
		var passed = 0;
	
		if(obj.flag.value == 1){
			if(obj.TargetClassLevel.value != "") {
				if(check_multi_select(obj, 'TargetClassID[]')) {
					if(check_multi_select(obj, 'TargetStudentID[]')) {
						passed = 1;
					} else {
						alert("<?=$i_Payment_Menu_Report_Export_StudentBalance_Alert3;?>");
					}
				} else {
					alert("<?=$i_Payment_Menu_Report_Export_StudentBalance_Alert2;?>");
				}
			} else {
				alert("<?=$i_Payment_Menu_Report_Export_StudentBalance_Alert1;?>");
			}
		} else {
			passed = 0;
		}
		
		if(passed == 1) {
			obj.action = "export_student_balance.php";
			return true;
		} else {
			obj.action = "";
			return false;
		}
			
	}
</script>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../',$i_Payment_Menu_StatisticsReport,'../',$i_Payment_Menu_Report_Export_StudentBalance,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<form name="form1" method="POST" onSubmit="return checkForm();">
<table width="560" border="0" cellpadding="3" cellspacing="3" align="center">
	<tr><td align=right><?php echo $i_general_startdate; ?>:</td><td>
	<input type=text name=TargetStartDate value='<?=$TargetStartDate;?>' size=10><script language="JavaScript" type="text/javascript">
	    <!--
	         startCal = new dynCalendar('startCal', 'startCalendarCallback', '/templates/calendar/images/');
	    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
	</td></tr>
	<tr><td align=right><?php echo $i_general_enddate; ?>:</td><td>
	<input type=text name=TargetEndDate value='<?=$TargetEndDate;?>' size=10><script language="JavaScript" type="text/javascript">
	    <!--
	         endCal = new dynCalendar('endCal', 'endCalendarCallback', '/templates/calendar/images/');
	    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
	</td></tr>
	<tr><td align="right"><?=$i_ClassLevel;?></td><td><?=$class_level_selection;?></td></tr>
	<? if($TargetClassLevel != "") { ?>
	<tr><td align="right">* <?=$i_ClassName;?></td><td><?=$class_name_selection;?></td></tr>
	<? } ?>
	<? if($TargetClassID != "") { ?>
	<tr><td align="right">* <?=$i_general_choose_student;?></td><td><?=$student_selection;?></td></tr>
	<? } ?>
	<tr><td colspan="3"><hr size=1 class="hr_sub_separator"></td></tr>
	<tr><td colspan="3" align="right"><input type='image' src='<?=$image_path;?>/admin/button/s_btn_submit_<?=$intranet_session_language;?>.gif' border='0' onClick="document.form1.flag.value=1;"><?= " ". btnReset() ?></td></tr>
	<? if(($TargetClassLevel != "") || ($TargetClassID != "")) { ?>
	<tr><td colspan="3"><span class="extraInfo">* <?=$i_Payment_Menu_Report_Export_StudentBalance_MultiSelect;?></span></td></tr>
	<? } ?>
</table>

<input type="hidden" name="flag" value="0">

</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>