<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");


if($ItemID=="") header("Location: index.php");

include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$li = new libpayment();

/*
if($filter_paid==1)
	$paid_cond = " AND a.RecordStatus=1 ";
else if($filter_paid==0)
	$paid_cond = " AND a.RecordStatus=0 ";
else $paid_cond ="";
*/
$paid_cond ="";

$namefield = getNameFieldByLang("b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield="c.ChineseName";
}else  $archive_namefield ="c.EnglishName";

$sql=" SELECT a.StudentID, 
				a.Amount,
				a.SubsidyUnitID,
				IF(a.SubsidyUnitID IS NOT NULL AND a.SubsidyUnitID <>'' , IF(a.SubsidyAmount>0,a.SubsidyAmount,0),0) AS SubAmount, 
				IF(a.SubsidyUnitID IS NOT NULL,d.UnitName,''),
				a.RecordStatus, 
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), $namefield), 
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, CONCAT('<i>',c.ClassName,'</i>'), b.ClassName),
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL,CONCAT('<i>',c.ClassNumber,'</i>'),b.ClassNumber) 
		FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a 
		LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
		LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID = c.UserID)
		LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT AS d ON (a.SubsidyUnitID = d.UnitID)
		WHERE a.ItemID='$ItemID' $paid_cond ORDER BY b.ClassName,b.ClassNumber,c.ClassName,c.ClassNumber";
		
	//echo $sql;
	
/*
$sql=" SELECT a.StudentID, 
				a.Amount, 
				a.RecordStatus, 
				$namefield, 
				 b.ClassName,
				b.ClassNumber 
		FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a 
		LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
		WHERE a.ItemID='$ItemID' $paid_cond ORDER BY b.ClassName,b.ClassNumber";
*/	
$temp = $li->returnArray($sql,9);

$y="<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
$y.="<Tr class=tableTitle>";
$y.="<td width='10%'>$i_ClassName</td>"; 
$y.="<td width='10%'>$i_ClassNumber</td>"; 
$y.="<td width='30%'>$i_UserName</td>"; 
$y.="<td width='10%'>$i_Payment_Field_Amount</td>"; 
$y.="<td width='10%'>$i_Payment_Subsidy_Amount</td>"; 
$y.="<td width='20%'>$i_Payment_Subsidy_Unit</td>"; 
$y.="<td width='10%'>$i_general_status</td>"; 
$y.="</tr>";

$paidcount =0;
$unpaidcount =0;
$paid=0;
$unpaid=0;
$total_sub_amount =0;
$noRecord=false;
if(sizeof($temp)>0){

	for($i=0;$i<sizeof($temp);$i++){
		list($sid,$amount,$sub_unit_id,$sub_amount,$sub_unit_name,$isPaid,$sname,$class_name,$class_num) = $temp[$i];
		$total_sub_amount +=$sub_amount+0;
		if($isPaid){
			$paidcount++;
			$paid+=$amount+0;
			if($filter_paid=="" || $filter_paid==1){
				$result[$sid]['class']=$class_name;
				$result[$sid]['classnumber']=$class_num;
				$result[$sid]['name']=$sname;
				$result[$sid]['amount']=$amount+0;
				$result[$sid]['status'] = $i_Payment_PresetPaymentItem_PaidCount;
				$result[$sid]['subsidy_unit_id']=$sub_unit_id;
				$result[$sid]['subsidy']=$sub_amount;
				$result[$sid]['subsidy_unit']=$sub_unit_name;
			}
		}else{
			$unpaidcount++;
			$unpaid+=$amount+0;
			if($filter_paid!=1){
				$result[$sid]['class']=$class_name;
				$result[$sid]['classnumber']=$class_num;
				$result[$sid]['name']=$sname;
				$result[$sid]['amount']=$amount+0;
				$result[$sid]['status'] = $i_Payment_PresetPaymentItem_UnpaidCount;
				$result[$sid]['subsidy_unit_id']=$sub_unit_id;
				$result[$sid]['subsidy']=$sub_amount;
				$result[$sid]['subsidy_unit']=$sub_unit_name;
			}
		}
		
	}
	$j=0;
	if(sizeof($result)>0){
		foreach($result as $sid => $values){
			$r_name 		= $values['name'];
			$r_class 		= $values['class'];
			$r_classnum	= $values['classnumber'];
			$r_amount 	= $values['amount'];
			$r_status 	= $values['status'];
			$r_subsidy  = $values['subsidy'];
			$r_subsidy_name=$values['subsidy_unit'];
			$r_subsidy_id = $values['subsidy_unit_id'];
			
			$css = $j%2==0?"tableContent":"tableContent2";
			$y.="<Tr>";
			$y.="<td class='$css'>$r_class</td>";
			$y.="<td class='$css'>$r_classnum</td>";
			$y.="<td class='$css'>$r_name</td>";
			$y.="<td class='$css'>".$li->getWebDisplayAmountFormat($r_amount)."</td>";
			$y.="<td class='$css'>".($r_subsidy_id==""?"&nbsp;":$li->getWebDisplayAmountFormat($r_subsidy))."</td>";
			$y.="<td class='$css'>$r_subsidy_name&nbsp;</td>";
			$y.="<td class='$css'>$r_status</td>";
			$y.="</tr>";
			$j++;
		}
	}else{ # no record
		$noRecord=true;
	}
}else{
	$noRecord = true;
}
if($noRecord){
		$y.="<tr><td colspan='7' align='center' class='tableContent' height=40 style='vertical-align:middle' >$i_no_record_exists_msg</td></tr>";

}

$y.="</table>\n\n";



# Get Item Name , Category Name, Start Date , End Date
$sql=" SELECT a.Name,b.Name,a.StartDate,a.EndDate FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $li->returnArray($sql,4);
list($item_name,$cat_name,$start_date,$end_date) = $temp[0];

$x="<table width=560 border=0 cellpadding=2 cellspacing=0 align=center><Tr><td>";
$x.="<table border=0 width=100%%>";
$x.="<tr><td align=right>$i_Payment_Field_PaymentItem:</td><td colspan=3>$item_name</td></tr>";
$x.="<tr><td align=right>$i_Payment_Field_PaymentCategory:</td><td colspan=3>$cat_name</td></tr>";
$x.="<tr><td align=right>$i_Payment_PresetPaymentItem_PaymentPeriod:</td><td colspan=3>$start_date $i_To $end_date</td></tr>";
$x.="<tr><td align=right>$i_Payment_PresetPaymentItem_PaidAmount:</td><td>".$li->getWebDisplayAmountFormat($paid)."</td><td align=right>$i_Payment_PresetPaymentItem_PaidStudentCount:</td><td>$paidcount</td></tr>";
$x.="<tr><td align=right>$i_Payment_PresetPaymentItem_UnpaidAmount:</td><td>".$li->getWebDisplayAmountFormat($unpaid)."</td><td align=right>$i_Payment_PresetPaymentItem_UnpaidStudentCount:</td><td>$unpaidcount</td></tr>";
$x.="<tr><td align=right>$i_Payment_Subsidy_Total_Subsidy_Amount:</td><td>".$li->getWebDisplayAmountFormat($total_sub_amount)."</td><td></td></tr>";
$x.="</table></td></tr></table><br>\n\n";


$display=$y;

$select_paid = "<SELECT name='filter_paid' onChange='javacript:document.form1.submit()'>";
$select_paid .= "<OPTION value=''".($filter_paid==""?" SELECTED ": "").">$i_status_all</OPTION>";
$select_paid .= "<OPTION value='1'".($filter_paid=="1"?" SELECTED ": "").">$i_Payment_PresetPaymentItem_PaidCount</OPTION>";
$select_paid .= "<OPTION value='0'".($filter_paid=="0"?" SELECTED ": "").">$i_Payment_PresetPaymentItem_UnpaidCount</OPTION>";
$select_paid .= "</SELECT>";

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
$toolbar2 = "<a class=iconLink href=\"javascript:exportPage(document.form1,'personal_report_export.php?ItemID=$ItemID&filter_paid=$filter_paid')\">".exportIcon()."$button_export</a>";

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_StatisticsReport,'../index.php',$i_Payment_Menu_Report_PresetItem,'index.php',$i_Payment_PresetPaymentItem_Personal,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<script language="javascript">
        function openPrintPage()
	{
         newWindow("personal_report_print.php?ItemID=<?=$ItemID?>&filter_paid=<?=$filter_paid?>",8);
	}
	function exportPage(obj,url){
		old_url = obj.action;
		obj.action = url;
		obj.submit();
		obj.action = old_url;
	}
</script>

<?php 
$lb = new libdbtable("","","");
?>
<?=$x?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
<tr><td><?= $i_Payment_Note_StudentRemoved ?></td></tr>
</table>
<form name=form1 action='' method='GET'>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?=$lb->displayFunctionbar("-", "-", "$toolbar",$select_paid)?></td></tr>
<tr><td class=admin_bg_menu><?=$lb->displayFunctionbar("-", "-", "$toolbar2","")?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?=$display?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacin=0 align=center>
<tr><td align=right>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table> <BR><BR>
<input type=hidden name=ItemID value='<?=$ItemID?>'>
</form>
<?php
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
