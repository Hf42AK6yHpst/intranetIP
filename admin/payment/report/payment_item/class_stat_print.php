<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");

intranet_opendb();

$li = new libpayment();



# Get Item Name , Category Name, Start Date , End Date
$sql=" SELECT a.Name,b.Name,a.StartDate,a.EndDate FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $li->returnArray($sql,4);
list($item_name,$cat_name,$start_date,$end_date) = $temp[0];

$x="<table width=90% border=0 cellpadding=2 cellspacing=0 align='center'>";
$x.="<Tr><td class='$css_title'>$i_Payment_PresetPaymentItem_ClassStat</td></tr>";
$x.="<tr><td><table border=0 width=60%>";
$x.="<tr class='$css_text'><td align=right class='$css_text'><B>$i_Payment_Field_PaymentItem:</b></td><td>$item_name</td></tr>";
$x.="<tr class='$css_text'><td align=right class='$css_text'><b>$i_Payment_Field_PaymentCategory:</b></td><td>$cat_name</td></tr>";
$x.="<tr class='$css_text'><td align=right ><b>$i_Payment_PresetPaymentItem_PaymentPeriod:</b></td><td>$start_date $i_To $end_date</td></tr>";
$x.="</table></td></tr></table><br>\n\n";



/*	
$sql=" SELECT a.Amount, a.RecordStatus, b.ClassName FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a 
		LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE a.ItemID='$ItemID' AND b.ClassName IS NOT NULL  ORDER BY b.ClassName";
*/

$sql=" SELECT a.Amount, IF(a.SubsidyUnitID IS NOT NULL AND a.SubsidyUnitID <>'' , IF(a.SubsidyAmount>0,a.SubsidyAmount,0),0) AS SubAmount,
				a.RecordStatus, 
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, c.ClassName,b.ClassName) 
		FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID = c.UserID)
		WHERE a.ItemID='$ItemID'  ORDER BY b.ClassName,c.ClassName";
	

$temp = $li->returnArray($sql,4);


$y="<table width=90% border=0  cellpadding=1 cellspacing=0 align='center' class='$css_table'>";
$y.="<Tr class='$css_table_title'>";
$y.="<td class='$css_table_title'>$i_ClassName</td>"; 
$y.="<td class='$css_table_title'>$i_Payment_Field_TotalPaidCount</td>"; 
$y.="<td class='$css_table_title'>$i_Payment_PresetPaymentItem_PaidStudentCount</td>"; 
$y.="<td class='$css_table_title'>$i_Payment_PresetPaymentItem_UnpaidStudentCount</td>";  
$y.="<td class='$css_table_title'>$i_Payment_PresetPaymentItem_PaidAmount</td>"; 
$y.="<td class='$css_table_title'>$i_Payment_PresetPaymentItem_UnpaidAmount</td>"; 
$y.="<td class='$css_table_title'>$i_Payment_Subsidy_Amount</td>";
$y.="<td class='$css_table_title'>$i_Payment_PresetPaymentItem_ClassStat_Subsidy_Plus_Paid</td>";
$y.="</tr>";

if(sizeof($temp)>0){
	for($i=0;$i<sizeof($temp);$i++){
		list($amount,$sub_amount,$isPaid,$class_name) = $temp[$i];
		$result["$class_name"]['total']++;
		if($isPaid){
			$result["$class_name"]['paidcount']++;
			$result["$class_name"]['paid']+=$amount+0;
		}else{
			$result["$class_name"]['unpaidcount']++;
			$result["$class_name"]['unpaid']+=$amount+0;
		}
		$result["$class_name"]['subsidy'] +=$sub_amount+0;
	}
	$j=0;
	foreach($result as $class => $values){
		
		$total = $values['total'];
		$paid_count = $values['paidcount'];
		$unpaid_count = $values['unpaidcount'];
		$paid = $values['paid'];
		$unpaid=$values['unpaid'];
		$sub_amount = $values['subsidy'];		

		$overall_total +=$total+0;
		$overall_paidcount+=$paid_count;
		$overall_unpaidcount+=$unpaid_count;
		$overall_paid +=$paid;
		$overall_unpaid+=$unpaid;
		$overall_subsidy+=$sub_amount;
		$overall_paid_subsidy+=$sub_amount+$paid;
						
		//$css = $j%2==0?"tableContent":"tableContent2";
		$css = $j%2==0?$css_table_content:$css_table_content."2";

		$y.="<Tr>";
		$y.="<td class='$css'>$class</td>";
		$y.="<td class='$css'>$total</td>";
		$y.="<td class='$css'>".($paid_count==""?0:$paid_count)."</td>";
		$y.="<td class='$css'>".($unpaid_count==""?0:$unpaid_count)."</td>";
		$y.="<td class='$css'>".$li->getWebDisplayAmountFormat($paid)."</td>";
		$y.="<td class='$css'>".$li->getWebDisplayAmountFormat($unpaid)."</td>";
		$y.="<td class='$css'>".$li->getWebDisplayAmountFormat($sub_amount)."</td>";
		$y.="<td class='$css'>".$li->getWebDisplayAmountFormat($sub_amount+$paid)."</td>";		
		$y.="</tr>";
		$j++;
	}
	$y.="<tr><td class='$css_table_content' colspan='8'><img src='$image_path/space.gif' border=0 height=1></td></tr>";
	$y.="<tr><td class='$css_table_title'>$i_Payment_SchoolAccount_Total</td><td class='$css_table_content'>$overall_total</td><td class='$css_table_content'>$overall_paidcount</td><td class='$css_table_content'>$overall_unpaidcount</td><td class='$css_table_content'>".$li->getWebDisplayAmountFormat($overall_paid)."</td><td class='$css_table_content'>".$li->getWebDisplayAmountFormat($overall_unpaid)."</td><td class='$css_table_content'>".$li->getWebDisplayAmountFormat($overall_subsidy)."</td><td class='$css_table_content'>".$li->getWebDisplayAmountFormat($overall_paid_subsidy)."</td></tr>";
	
}else{ # no record
	$y.="<tr><td colspan=8 align=center class='$css_table_content' height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}


$y.="</table>\n\n";

//echo $y;

$display=$y;


?>

<?php 
echo $x;
?>

<?=$display?>
<table width=90% border=0 cellpadding=0 cellspacin=0 align=center>
<tr><td align=right class='<?=$css_text?>'>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table>
<?php
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>
