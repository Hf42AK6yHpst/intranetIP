<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

if($ItemID=="") header("Location: index.php");

include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$li = new libpayment();
$lb = new libdbtable("","","");


# Get Item Name , Category Name, Start Date , End Date
$sql=" SELECT a.Name,b.Name,a.StartDate,a.EndDate FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $li->returnArray($sql,4);
list($item_name,$cat_name,$start_date,$end_date) = $temp[0];

$x="<table width=560 border=0 cellpadding=2 cellspacing=0 align='center'><Tr><td>";
$x.="<table border=0 width=60%>";
$x.="<tr><td align=right>$i_Payment_Field_PaymentItem:</td><td>$item_name</td></tr>";
$x.="<tr><td align=right>$i_Payment_Field_PaymentCategory:</td><td>$cat_name</td></tr>";
$x.="<tr><td align=right>$i_Payment_PresetPaymentItem_PaymentPeriod:</td><td>$start_date $i_To $end_date</td></tr>";
$x.="</table></td></tr></table><br>\n\n";


/*
$sql=" SELECT a.Amount, a.RecordStatus, b.ClassName FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a 
		LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE a.ItemID='$ItemID' AND b.ClassName IS NOT NULL  ORDER BY b.ClassName";
*/

$sql=" SELECT a.Amount, IF(a.SubsidyUnitID IS NOT NULL AND a.SubsidyUnitID <>'' , IF(a.SubsidyAmount>0,a.SubsidyAmount,0),0) AS SubAmount,
				a.RecordStatus, 
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, c.ClassName,b.ClassName) 
		FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID = c.UserID)
		WHERE a.ItemID='$ItemID'  ORDER BY b.ClassName,c.ClassName";
	

$temp = $li->returnArray($sql,4);

$y="<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
$y.="<Tr class=tableTitle>";
$y.="<td>$i_ClassName</td>"; 
$y.="<td>$i_Payment_Field_TotalPaidCount</td>"; 
$y.="<td>$i_Payment_PresetPaymentItem_PaidStudentCount</td>"; 
$y.="<td>$i_Payment_PresetPaymentItem_UnpaidStudentCount</td>"; 
$y.="<td>$i_Payment_PresetPaymentItem_PaidAmount</td>"; 
$y.="<td>$i_Payment_PresetPaymentItem_UnpaidAmount</td>"; 
$y.="<td>$i_Payment_Subsidy_Amount</td>";
$y.="<td>$i_Payment_PresetPaymentItem_ClassStat_Subsidy_Plus_Paid</td>";
$y.="</tr>";

/*
$sql ="SELECT ClassName FROM INTRANET_CLASS WHERE RecordStatus=1 ORDER BY ClassName";
$class_list = $li->returnVector($sql);
for($i=0;$i<sizeof($class_list);$i++){
	$class_name = $class_list[$i];
	$result["$class_name"]['total']=0;
	$result["$class_name"]['paidcount']=0;
	$result["$class_name"]['paid']=0;
	$result["$class_name"]['unpaidcount']=0;
	$result["$class_name"]['unpaid']=0;
}
*/

if(sizeof($temp)>0){
	for($i=0;$i<sizeof($temp);$i++){
		list($amount,$sub_amount,$isPaid,$class_name) = $temp[$i];
		$result["$class_name"]['total']++;
		if($isPaid){
			$result["$class_name"]['paidcount']++;
			$result["$class_name"]['paid']+=$amount+0;
		}else{
			$result["$class_name"]['unpaidcount']++;
			$result["$class_name"]['unpaid']+=$amount+0;
		}
		$result["$class_name"]['subsidy'] +=$sub_amount+0;
	}
	
	$overall_total =0;
	$overall_paid  =0;
	$overall_paidcount=0;
	$overall_unpaid =0;
	$overall_unpaidcount=0;
	$overall_subsidy=0;
	$overall_paid_subsidy=0;
	
	$j=0;
	foreach($result as $class => $values){
		
		$total = $values['total'];
		$paid_count = $values['paidcount'];
		$unpaid_count = $values['unpaidcount'];
		$paid = $values['paid'];
		$unpaid=$values['unpaid'];
		$sub_amount = $values['subsidy'];
		
		$overall_total +=$total+0;
		$overall_paidcount+=$paid_count;
		$overall_unpaidcount+=$unpaid_count;
		$overall_paid +=$paid;
		$overall_unpaid+=$unpaid;
		$overall_subsidy+=$sub_amount;
		$overall_paid_subsidy+=$sub_amount+$paid;
		
		$css = $j%2==0?"tableContent":"tableContent2";
		$y.="<Tr>";
		$y.="<td class='$css'>$class</td>";
		$y.="<td class='$css'>$total</td>";
		$y.="<td class='$css'>".($paid_count==""?0:$paid_count)."</td>";
		$y.="<td class='$css'>".($unpaid_count==""?0:$unpaid_count)."</td>";
		$y.="<td class='$css'>".$li->getWebDisplayAmountFormat($paid)."</td>";
		$y.="<td class='$css'>".$li->getWebDisplayAmountFormat($unpaid)."</td>";
		$y.="<td class='$css'>".$li->getWebDisplayAmountFormat($sub_amount)."</td>";
		$y.="<td class='$css'>".$li->getWebDisplayAmountFormat($sub_amount+$paid)."</td>";
		$y.="</tr>";
		$j++;
	}
	$y.="<tr class=tableTitle2><td>$i_Payment_SchoolAccount_Total</td><td>$overall_total</td><td>$overall_paidcount</td><td>$overall_unpaidcount</td><td>".$li->getWebDisplayAmountFormat($overall_paid)."</td><td>".$li->getWebDisplayAmountFormat($overall_unpaid)."</td><td>".$li->getWebDisplayAmountFormat($overall_subsidy)."</td><td>".$li->getWebDisplayAmountFormat($overall_paid_subsidy)."</td></tr>";
	
}else{ # no record
	$y.="<tr><td colspan=8 align=center class=tableContent height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}


$y.="</table>\n\n";

//echo $y;

$display=$y;

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
$toolbar2 = "<a class=iconLink href='class_stat_export.php?ItemID=$ItemID'>".exportIcon()."$button_export</a>";

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_StatisticsReport,'../index.php',$i_Payment_Menu_Report_PresetItem,'index.php',$i_Payment_PresetPaymentItem_ClassStat,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<script language="javascript">
        function openPrintPage()
	{
         newWindow("class_stat_print.php?ItemID=<?=$ItemID?>",8);
	}
</script>
<?php 
echo $x;
?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?=$lb->displayFunctionbar("-", "-", "$toolbar","")?></td></tr>
<tr><td class=admin_bg_menu><?=$lb->displayFunctionbar("-", "-", "$toolbar2","")?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?=$display?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table><table width=560 border=0 cellpadding=0 cellspacin=0 align=center>
<tr><td align=right>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table> <BR><BR>

<?php
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
