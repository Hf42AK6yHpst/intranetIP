<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();

$li = new libpayment();
$lb = new libdbtable("","","");

$x="<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
$x.="<Tr class=tableTitle>";
$x.="<td width=15%>$i_Payment_Field_PaymentItem</td>";
$x.="<td width=15%>$i_Payment_Field_PaymentCategory</td>";
$x.="<td width=12%>$i_general_startdate</td>";
$x.="<td width=12%>$i_general_enddate</td>";
$x.="<td width=10%>$i_general_status</td>";
$x.="<td>$i_Payment_Field_TotalPaidCount</td>";
$x.="<td>$i_Payment_PresetPaymentItem_PaidStudentCount ($i_Payment_PresetPaymentItem_PaidAmount)</td>";
$x.="<td>$i_Payment_PresetPaymentItem_UnpaidStudentCount ($i_Payment_PresetPaymentItem_UnpaidAmount)</td>";
$x.="<td>&nbsp;</td>";
//$x.="<td>$i_Payment_PresetPaymentItem_UnpaidAmount</td>";

$x.="</tr>";




# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
	$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
	$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$today = date('Y-m-d');
	
$date_cond =" DATE_FORMAT(a.StartDate,'%Y-%m-%d')<='$ToDate' AND DATE_FORMAT(a.EndDate,'%Y-%m-%d')>='$FromDate'";		

$cat_cond = $CatID==""?"":" AND a.CatID='$CatID' ";



$sql="SELECT a.ItemID FROM PAYMENT_PAYMENT_ITEM AS a WHERE $date_cond $cat_cond AND a.Name LIKE '%$keyword%' ";

$temp = $li->returnVector($sql);
if(sizeof($temp)>0){
	$item_list = implode(",",$temp);
	$sql="SELECT a.ItemID, 
				a.Name,
				b.Name,
				a.StartDate,
				a.EndDate,
				IF('$today'<DATE_FORMAT(a.StartDate,'%Y-%m-%d'),'$i_Payment_PaymentStatus_NotStarted',
				IF('$today'>DATE_FORMAT(a.EndDate,'%Y-%m-%d'),'$i_Payment_PaymentStatus_Ended','$i_Payment_PresetPaymentItem_Progress')) AS Status
				FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID)
				WHERE a.ItemID IN($item_list) ORDER BY a.EndDate DESC";
	$temp = $li->returnArray($sql,6);
	

	for($i=0;$i<sizeof($temp);$i++){
		list($item_id,$item_name,$cat_name,$start_date,$end_date,$status)=$temp[$i];
		$result[$item_id]['name']		= $item_name;
		$result[$item_id]['catname']	= $cat_name;
		$result[$item_id]['startdate']	= $start_date;
		$result[$item_id]['enddate']	= $end_date;
		$result[$item_id]['status']		= $status;
		$result[$item_id]['paid']	= 0;
		$result[$item_id]['unpaid']= 0;
		$result[$item_id]['paidcount']	= 0;
		$result[$item_id]['unpaidcount']= 0;
		
		
	}
	$sql="SELECT b.ItemID,
				b.RecordStatus,
				b.Amount FROM PAYMENT_PAYMENT_ITEMSTUDENT AS b WHERE b.ItemID IN ($item_list) ORDER BY b.ItemID,b.RecordStatus";	
	$temp = $li->returnArray($sql,3);
	
	for($i=0;$i<sizeof($temp);$i++){
		list($item_id,$record_status,$amount) = $temp[$i];
		if($record_status==1){ # Paid
			$result[$item_id]['paid']+=($amount>0?$amount:0);
			$result[$item_id]['paidcount']++; 
		}else{
			$result[$item_id]['unpaid']+=($amount>0?$amount:0);
			$result[$item_id]['unpaidcount']++; 
		}
	}
	
	if(sizeof($result)>0){
		$j=0;
		foreach($result as $item_id =>$values){
			$css = $j%2==0?"tableContent":"tableContent2";
			$j++;
			$item_name = $values['name'];
			$cat_name  = $values['catname'];
			$start_date= $values['startdate'];
			$end_date  = $values['enddate'];
			$status    = $values['status'];
			$paid	   = $values['paid'];
			$unpaid	   = $values['unpaid'];
			$paidcount = $values['paidcount'];
			$unpaidcount= $values['unpaidcount'];
			$total  = $paidcount + $unpaidcount;
			
			$x.="<tr>";
			$x.="<td class='$css'>$item_name</td>";
			$x.="<td class='$css'>$cat_name</td>";
			$x.="<td class='$css'>$start_date</td>";
			$x.="<td class='$css'>$end_date</td>";
			$x.="<td class='$css'>$status</td>";
			$x.="<td class='$css'>$total</td>";
			$x.="<td class='$css'>$paidcount (".$li->getWebDisplayAmountFormat($paid).")</td>";
			$x.="<td class='$css'>$unpaidcount (".$li->getWebDisplayAmountFormat($unpaid).")</td>";
			$x.="<Td class='$css' nowrap><a href='class_stat.php?ItemID=$item_id' class='button_link' title='$i_Payment_PresetPaymentItem_ClassStat'><img src='/images/admin/class_stat.gif' border=0></a>&nbsp;<a href='personal_report.php?ItemID=$item_id' class='button_link' title='$i_Payment_PresetPaymentItem_Personal'><img src='/images/admin/student_rec.gif' border=0></a></td>";
			$x.="</tr>";
		}
	}
	
	
}else{ # no record
	$x.="<tr><td colspan=9 align=center class=tableContent2 height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}


$x.="</table>";

$display=$x;

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
$toolbar2 = "<a class=iconLink href=javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate')>".exportIcon()."$button_export</a>";


$cats = $li->returnPaymentCats();
$select_cat = getSelectByArray($cats,"name=CatID ",$CatID,1);


echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_StatisticsReport,'../index.php',$i_Payment_Menu_Report_PresetItem,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
        var css_array = new Array;
        css_array[0] = "dynCalendar_free";
        css_array[1] = "dynCalendar_half";
        css_array[2] = "dynCalendar_full";
        var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
                  // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                                           document.forms['form1'].FromDate.value = dateValue;

          }
          function calendarCallback2(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                                           document.forms['form1'].ToDate.value = dateValue;

          }
        function checkForm(formObj){
                if(formObj==null)return false;
                        fromV = formObj.FromDate;
                        toV= formObj.ToDate;
                        if(!checkDate(fromV)){
                                        //formObj.FromDate.focus();
                                        return false;
                        }
                        else if(!checkDate(toV)){
                                                //formObj.ToDate.focus();
                                                return false;
                        }
                                return true;
        }
        function checkDate(obj){
                         if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
                        return true;
        }
        function submitForm(obj){
                if(checkForm(obj))
                        obj.submit();
        }
        function openPrintPage()
	{
         newWindow("print.php?FromDate=<?=$FromDate?>&ToDate=<?=$ToDate?>&CatID=<?=$CatID?>&keyword=<?=$keyword?>",8);
	}
		function exportPage(obj,url){
			old_url = obj.action;
	        obj.action=url;
	        obj.submit();
	        obj.action = old_url;
	        
	}
</script>
<form name="form1" method="get" action=''>
<!-- date range -->
<table border=0 width=560 align=center>
<tr><td align=right><?=$i_Payment_Field_PaymentCategory?>:</td><Td><?=$select_cat?></td></tr>
<tr><td align=right><?=$i_Payment_Field_PaymentItem?>:</td><td><input type=text name=keyword value='<?=$keyword?>'></td></tr>
	<tr>
		<td nowrap class=tableContent align=right>
		<?=$i_Profile_From?>:</td><td>
			<input type=text name=FromDate value="<?=$FromDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
					//-->
				</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
		<tr><td align=right>
		 	<?=$i_Profile_To?>:</td><td>
		 	<input type=text name=ToDate value="<?=$ToDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal2 = new dynCalendar('startCal2', 'calendarCallback2', '/templates/calendar/images/');
					//-->
				</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>&nbsp;
 <a href='javascript:submitForm(document.form1)'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0' align='absmiddle'></a></td></tr>
<tr><td colspan='2'><hr size=1 class="hr_sub_separator"></td></tr>
</table>

</form>
<?=$y?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?=$lb->displayFunctionbar("-", "-", "$toolbar","")?></td></tr>
<tr><td class=admin_bg_menu><?=$lb->displayFunctionbar("-", "-", "$toolbar2","")?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?=$display?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacin=0 align=center>
<tr><td align=right>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table>
<BR><BR>

<?php
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
