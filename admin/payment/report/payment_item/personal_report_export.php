<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

if($ItemID=="") return;

intranet_opendb();

$lexport = new libexporttext();

$lpayment = new libpayment();

$paid_cond ="";

$namefield = getNameFieldByLang("b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield="c.ChineseName";
}else  $archive_namefield ="c.EnglishName";

$sql=" SELECT a.StudentID, 
				a.Amount, 
				a.SubsidyUnitID,
				IF(a.SubsidyUnitID IS NOT NULL AND a.SubsidyUnitID <>'' , IF(a.SubsidyAmount>0,a.SubsidyAmount,0),0) AS SubAmount, 
				IF(a.SubsidyUnitID IS NOT NULL,d.UnitName,''),				
				a.RecordStatus, 
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, $archive_namefield, $namefield), 
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, c.ClassName, b.ClassName),
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, c.ClassNumber,b.ClassNumber) 
		FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a 
		LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
		LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID = c.UserID)
		LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT AS d ON (a.SubsidyUnitID = d.UnitID)		
		WHERE a.ItemID='$ItemID' $paid_cond ORDER BY b.ClassName,b.ClassNumber,c.ClassName,c.ClassNumber";


/*
$sql=" SELECT a.StudentID, a.Amount, a.RecordStatus, $namefield, b.ClassName,b.ClassNumber FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a 
		LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE a.ItemID='$ItemID' $paid_cond ORDER BY b.ClassName,b.ClassNumber";
*/
		
$temp = $lpayment->returnArray($sql,9);

$y="\"$i_ClassName\","; 
$y.="\"$i_ClassNumber\","; 
$y.="\"$i_UserName\","; 
$y.="\"$i_Payment_Field_Amount\","; 
$y.="\"$i_Payment_Subsidy_Amount\","; 
$y.="\"$i_Payment_Subsidy_Unit\","; 
$y.="\"$i_general_status\""; 
$y.="\n";

$exportColumn = array($i_ClassName, $i_ClassNumber, $i_UserName, $i_Payment_Field_Amount, $i_Payment_Subsidy_Amount,$i_Payment_Subsidy_Unit,$i_general_status);

$paidcount =0;
$unpaidcount =0;
$paid=0;
$unpaid=0;
$total_sub_amount =0;
$noRecord=false;
if(sizeof($temp)>0){

	for($i=0;$i<sizeof($temp);$i++){
		list($sid,$amount,$sub_unit_id,$sub_amount,$sub_unit_name,$isPaid,$sname,$class_name,$class_num) = $temp[$i];
		$total_sub_amount +=$sub_amount+0;			
		if($isPaid){
			$paidcount++;
			$paid+=$amount+0;
			
			if($filter_paid=="" || $filter_paid==1){
				$result[$sid]['class']=$class_name;
				$result[$sid]['classnumber']=$class_num;
				$result[$sid]['name']=$sname;
				$result[$sid]['amount']=$amount+0;
				$result[$sid]['status'] = $i_Payment_PresetPaymentItem_PaidCount;
				$result[$sid]['subsidy_unit_id']=$sub_unit_id;
				$result[$sid]['subsidy']=$sub_amount;
				$result[$sid]['subsidy_unit']=$sub_unit_name;
				
			}
		}else{
			$unpaidcount++;
			$unpaid+=$amount+0;
			if($filter_paid!=1){
				$result[$sid]['class']=$class_name;
				$result[$sid]['classnumber']=$class_num;
				$result[$sid]['name']=$sname;
				$result[$sid]['amount']=$amount+0;
				$result[$sid]['status'] = $i_Payment_PresetPaymentItem_UnpaidCount;
				$result[$sid]['subsidy_unit_id']=$sub_unit_id;
				$result[$sid]['subsidy']=$sub_amount;
				$result[$sid]['subsidy_unit']=$sub_unit_name;
				
			}
		}
	}
	if(sizeof($result)>0){
		foreach($result as $sid => $values){
			$r_name 		= $values['name'];
			$r_class 		= $values['class'];
			$r_classnum	= $values['classnumber'];
			$r_amount 	= $values['amount'];
			$r_status 	= $values['status'];
			$r_subsidy  = $values['subsidy'];
			$r_subsidy_name=$values['subsidy_unit'];
			$r_subsidy_id = $values['subsidy_unit_id'];					
			
			$y.="\"$r_class\",";
			$y.="\"$r_classnum\",";
			$y.="\"$r_name\",";
			$y.="\"".$lpayment->getExportAmountFormat($r_amount)."\",";
			$y.="\"".($r_subsidy_id==""?"":$lpayment->getExportAmountFormat($r_subsidy))."\",";
			$y.="\"$r_subsidy_name\",";
			$y.="\"$r_status\"";
			$y.="\n";
			
			$rows[] = array($r_class, $r_classnum, $r_name, $lpayment->getExportAmountFormat($r_amount), ($r_subsidy_id==""?"":$lpayment->getExportAmountFormat($r_subsidy)),$r_subsidy_name,$r_status);
			
		}
	}else{ # no record
		$noRecord = true;
	}
}else $noRecord = true;
if($noRecord){
		$y.="\"$i_no_record_exists_msg\"\n";
		$rows[] = $i_no_record_exists_msg;
}


# Get Item Name , Category Name, Start Date , End Date
$sql=" SELECT a.Name,b.Name,a.StartDate,a.EndDate FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $lpayment->returnArray($sql,4);
list($item_name,$cat_name,$start_date,$end_date) = $temp[0];
$x="\"$i_Payment_PresetPaymentItem_Personal\"\n";
$x.="\"$i_Payment_Field_PaymentItem\",\"$item_name\"\n";
$x.="\"$i_Payment_Field_PaymentCategory\",\"$cat_name\"\n";
$x.="\"$i_Payment_PresetPaymentItem_PaymentPeriod\",\"$start_date $i_To $end_date\"\n";
$x.="\"$i_Payment_PresetPaymentItem_PaidAmount\",\"".$lpayment->getExportAmountFormat($paid)."\",\"$i_Payment_PresetPaymentItem_PaidStudentCount\",\"$paidcount\"\n";
$x.="\"$i_Payment_PresetPaymentItem_UnpaidAmount\",\"".$lpayment->getExportAmountFormat($unpaid)."\",\"$i_Payment_PresetPaymentItem_UnpaidStudentCount\",\"$unpaidcount\"\n";
$x.="\"$i_Payment_Subsidy_Total_Subsidy_Amount\",\"".$lpayment->getExportAmountFormat($total_sub_amount)."\"\n";


$preheader=$i_Payment_PresetPaymentItem_Personal."\r\n";
$preheader.=$i_Payment_Field_PaymentItem."\t".$item_name."\r\n";
$preheader.=$i_Payment_Field_PaymentCategory."\t".$cat_name."\r\n";
$preheader.=$i_Payment_PresetPaymentItem_PaymentPeriod."\t".$start_date." ".$i_To." ".$end_date."\r\n";
$preheader.=$i_Payment_PresetPaymentItem_PaidAmount."\t".$lpayment->getExportAmountFormat($paid)."\t".$i_Payment_PresetPaymentItem_PaidStudentCount."\t".$paidcount."\r\n";
$preheader.=$i_Payment_PresetPaymentItem_UnpaidAmount."\t".$lpayment->getExportAmountFormat($unpaid)."\t".$i_Payment_PresetPaymentItem_UnpaidStudentCount."\t".$unpaidcount."\r\n";
$preheader.=$i_Payment_Subsidy_Total_Subsidy_Amount."\t".$lpayment->getExportAmountFormat($total_sub_amount)."\r\n";

$display=$x.$y;

$display.="\"$i_general_report_creation_time\",\"".date('Y-m-d H:i:s')."\"\n";
$rows[] = array($i_general_report_creation_time, date('Y-m-d H:i:s'));

$filename="personal_report.csv";

//output2browser($display,$filename);

if($g_encoding_unicode){
	$export_content = $preheader.$lexport->GET_EXPORT_TXT($rows, $exportColumn);
	$lexport->EXPORT_FILE($filename, $export_content);
}
else output2browser($display,$filename);

intranet_closedb();
?>
