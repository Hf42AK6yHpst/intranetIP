<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lpayment = new libpayment();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
if ($field == ""){
	 $field = 8;
}
	 

$namefield = getNameFieldByLang("b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
        $archive_namefield="c.ChineseName";
}else  $archive_namefield ="c.EnglishName";
# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

        # date range
        $today_ts = strtotime(date('Y-m-d'));
        if($FromDate=="")
                $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
        if($ToDate=="")
                $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$search_by=$search_by==""?1:$search_by;                
                
if($search_by!=1)                
	$date_cond = " AND DATE_FORMAT(a.DateInput,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(a.DateInput,'%Y-%m-%d')<='$ToDate' ";
else
   $date_cond = " AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='$ToDate' ";

switch($user_type){
        case 2: $user_cond = " AND (b.RecordType=2 OR c.RecordType=2)"; break;
        case 1: $user_cond = " AND (b.RecordType=1 OR (b.RecordType IS NULL AND c.RecordType IS NULL))"; break;
        //case 1: $user_cond = " AND (b.RecordType=1 )"; break;

        default: $user_cond = "";break;
}

$sql  = "SELECT

               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'<font color=red>*</font>',$namefield)),
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)),
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)),

               ".$lpayment->getWebDisplayAmountFormatDB("a.Amount")." AS Amount,
               CASE a.RecordType
                    WHEN 1 THEN '$i_Payment_Credit_TypePPS'
                    WHEN 2 THEN '$i_Payment_Credit_TypeCashDeposit'
                    WHEN 3 THEN '$i_Payment_Credit_TypeAddvalueMachine'
                    ELSE '$i_Payment_Credit_TypeUnknown' END,
               a.RefCode, a.AdminInCharge, DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
               DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i'),
               CONCAT('<input type=checkbox name=TransactionID[] value=', a.TransactionID ,'>')
         FROM
             PAYMENT_CREDIT_TRANSACTION as a LEFT OUTER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID=c.UserID )

         WHERE
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               a.RefCode LIKE '%$keyword%' OR
               a.AdminInCharge LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%'
              )
             $date_cond
             $user_cond
                ";
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("b.EnglishName,c.EnglishName","b.ClassName,c.ClassName","b.ClassNumber,c.ClassNumber","Amount","a.RecordType","a.RefCode","a.AdminInCharge","a.TransactionTime","a.DateInput");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0);
$li->IsColOff = 2;


// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Payment_Field_Username)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Payment_Field_CreditAmount)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Payment_Credit_Method)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Payment_Field_RefCode)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Payment_Field_AdminInCharge)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Field_PostTime)."</td>\n";
#$li->column_list .= "<td width=1 class=tableTitle>".$li->check("TransactionID[]")."</td>\n";


$select_user = "<SELECT name='user_type' onChange='submitForm(this.form)'>";
$select_user.= "<OPTION value='2'".($user_type==2?"SELECTED":"").">$i_identity_student</OPTION>";
$select_user.= "<OPTION value='1' ".($user_type==1?"SELECTED":"").">$i_identity_teachstaff</OPTION>";
$select_user.= "<OPTION value='' ".($user_type==""?"SELECTED":"").">$i_Payment_All</OPTION>";
$select_user.= "</SELECT>&nbsp;&nbsp;";

$toolbar = "<a class=iconLink href=javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate&search_by=$search_by')>".exportIcon()."$button_export</a>";
//$toolbar .= "<a class=iconLink href=javascript:checkGet(document.form1,'pps_index.php')>".exportIcon()."$button_export_pps</a>";
#$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'TerminalUserID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
#$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'TerminalUserID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";

$searchbar=$select_user;

$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_DataBrowsing,'../',$i_Payment_Menu_Browse_CreditTransaction,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);


# Calculate sum
$sum_sql  = "SELECT
               SUM(a.Amount), COUNT(a.Amount)
         FROM
             PAYMENT_CREDIT_TRANSACTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
             WHERE
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               a.RefCode LIKE '%$keyword%' OR
               a.AdminInCharge LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%'

              )
              $date_cond
              $user_cond
              ";
$ldb = new libdb();
$temp = $ldb->returnArray($sum_sql,2);
list($total_amount, $total_count) = $temp[0];
$total_amount = $lpayment->getWebDisplayAmountFormat($total_amount+0);
$total_count += 0;

$infobar = "<font color=green><u><b>$i_Payment_ItemSummary</b></u></font><br>\n";
$infobar .= "$i_Payment_TotalCountTransaction: $total_count<br>\n";
$infobar .= "$i_Payment_ItemTotal: $total_amount<br>";

?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
        var css_array = new Array;
        css_array[0] = "dynCalendar_free";
        css_array[1] = "dynCalendar_half";
        css_array[2] = "dynCalendar_full";
        var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
                  // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                                           document.forms['form1'].FromDate.value = dateValue;

          }
          function calendarCallback2(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                                           document.forms['form1'].ToDate.value = dateValue;

          }
        function checkForm(formObj){
                if(formObj==null)return false;
                        fromV = formObj.FromDate;
                        toV= formObj.ToDate;
                        if(!checkDate(fromV)){
                                        //formObj.FromDate.focus();
                                        return false;
                        }
                        else if(!checkDate(toV)){
                                                //formObj.ToDate.focus();
                                                return false;
                        }
                                return true;
        }
        function checkDate(obj){
                         if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
                        return true;
        }
        function submitForm(obj){
                if(checkForm(obj))
                        obj.submit();
        }
        function exportPage(obj,url){
                        old_url = obj.action;
                        old_method= obj.method;
                obj.action=url;
                obj.method = "get";
                obj.submit();
                obj.action = old_url;
                obj.method = old_method;

        }
</script>
<form name="form1" method="get" action='index.php'>
<!-- date range -->
<table border=0 width=560 align=center>
 <Tr><td></td><Td class='tableContent'><input type='radio' name='search_by' value='0'  <?=($search_by!=1?"checked":"")?>><?=$i_Payment_Search_By_PostTime?>&nbsp;&nbsp;<input type='radio' name='search_by' value='1' <?=($search_by==1?"checked":"")?>><?=$i_Payment_Search_By_TransactionTime?></td></tr>
 <tr><td nowrap class=tableContent align='right'><?=$i_Payment_Menu_Browse_CreditTransaction_DateRange?>:</td>
<Td><input type=text name=FromDate value="<?=$FromDate?>" size=10>
    <script language="JavaScript" type="text/javascript">
                                        <!--
                                                startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
                                        //-->
                                </script>&nbsp;
                         <?=$i_Profile_To?>
                         <input type=text name=ToDate value="<?=$ToDate?>" size=10>
                                <script language="JavaScript" type="text/javascript">
                                        <!--
                                                startCal2 = new dynCalendar('startCal2', 'calendarCallback2', '/templates/calendar/images/');
                                        //-->
                                </script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>&nbsp;&nbsp;<a href='javascript:submitForm(document.form1)'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0' align='absmiddle'></a>
 </td></tr>
<tr><td colspan='2'><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<!-- summary -->
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $infobar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
<tr><td><?= $i_Payment_Note_StudentRemoved ?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
