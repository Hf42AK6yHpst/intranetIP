<?

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lexport = new libexporttext();
$li = new libdb();
$lpayment = new libpayment();

$order = ($order == 1) ? 1 : 0;
if ($field == "" || $field >2) $field = 0;
$field += 0;

$status_cond = "";

if($status1==1 || $status2==1){
	$tmp = array();
  if($status1==1){
	$tmp[] = 0;
	$tmp[] = 1;
	$tmp[] = 2;
  }
  if($status2==1){
	  $tmp[] = 3;
  }
  $status_cond = " AND b.RecordStatus IN (".implode(",",$tmp).") ";
}


$namefield = getNameFieldByLang("b.");

if( ($status1==1 || $status2==1 ) && $status3==1){ 
	# ALL
	$class_name_field ="IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('*',c.ClassName),b.ClassName)";
	$class_num_field = "IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,b.ClassNumber)";
	$user_id_cond=" AND (b.UserID IS NOT NULL OR c.UserID IS NOT NULL)";
	
}else if($status1==1 || $status2==1){ 
	# Students with status approve,pending,suspended, left
	$class_name_field = "b.ClassName";
	$class_num_field ="b.ClassNumber";
	$user_id_cond = " AND b.UserID IS NOT NULL ";
	
}else{ 
	#  removed student
	$class_name_field = "CONCAT('*',c.ClassName)";
	$class_num_field = "c.ClassNumber";
	$user_id_cond = " AND c.UserID IS NOT NULL ";
}

if($search_by==1){ ## use Transaction Time
	$sql  = "SELECT
	               $class_name_field AS ClassName,
	               $class_num_field AS ClassNumber,
	               ".$lpayment->getExportAmountFormatDB("SUM(IF(a.PPSType=0,".LIBPAYMENT_PPS_CHARGE.",".LIBPAYMENT_PPS_CHARGE_COUNTER_BILL."))")." AS TotalAmount
	         FROM
	             PAYMENT_CREDIT_TRANSACTION as a LEFT OUTER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID $status_cond )
	             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID= c.UserID
	         WHERE
	                         a.RecordType = 1  
	                         $user_id_cond
	                         AND a.TransactionTime >= '$start' 
	                         AND a.TransactionTime <= '$end 23:59:59'
	         GROUP BY a.StudentID ORDER BY $class_name_field, $class_num_field
	";
}else{ ## Use Post Time

	$sql  = "SELECT
	               $class_name_field AS ClassName,
	               $class_num_field AS ClassNumber,
	               ".$lpayment->getExportAmountFormatDB("SUM(a.Amount)")." AS TotalAmount
	         FROM
	             PAYMENT_OVERALL_TRANSACTION_LOG AS a LEFT OUTER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID $status_cond )
	             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID= c.UserID
	         WHERE
	                         a.TransactionType = 8  
	                         $user_id_cond
	                         AND a.TransactionTime >= '$start' 
	                         AND a.TransactionTime <= '$end 23:59:59'
	         GROUP BY a.StudentID ORDER BY $class_name_field, $class_num_field
	";
}
//echo $sql;
/*
$sql  = "SELECT
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassName,b.ClassName),
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,b.ClassNumber),
               COUNT(*) AS RecordCount
         FROM
             PAYMENT_CREDIT_TRANSACTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID= c.UserID
         WHERE
                         a.RecordType = 1 AND
                         a.TransactionTime >= '$start' AND
                         a.TransactionTime <= '$end 23:59:59' AND
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               a.RefCode LIKE '%$keyword%' OR
               a.AdminInCharge LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' 

              )
         GROUP BY a.StudentID
";
*/

$result = $li->returnArray($sql,3);
$x = "Class Name,Class Number,Amount\n";
$utf_x = "Class Name \tClass Number \tAmount\r\n";
for ($i=0; $i<sizeof($result); $i++)
{
     list($class,$classnumber,$amount) = $result[$i];
  
     $x .= "\"$class\",\"$classnumber\",\"".$amount."\"\n";
     $utf_x .= "$class \t$classnumber \t".$amount."\r\n";
}

// Output the file to user browser
$filename = "transaction_cost.csv";

if ($g_encoding_unicode) {
	$lexport->EXPORT_FILE($filename, $utf_x);
} else {
	header("Content-type: application/octet-stream");
	header("Content-Length: ".strlen($x) );
	header("Content-Disposition: attachment; filename=\"".$filename."\"");
	
	echo $x;
}


intranet_closedb();
?>
