<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.submit()",$ClassName);

?>

<form name="form1" method="POST" action="">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_DataBrowsing,'../',$i_Payment_Menu_Browse_MissedPPSBill,'index.php',$i_Payment_action_handle,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_UserClassName; ?>:</td><td><?=$select_class?></td></tr>
<? if ($ClassName != "") {
	$temp = $lclass->getStudentNameListWClassNumberByClassName($ClassName);
	$select_student = getSelectByArray($temp,"name=TargetStudentID",0,0,1);
//$select_student = $lclass->getStudentSelectByClass($ClassName,"name=TargetStudentID",1,1);
?>
<tr><td align=right nowrap><?php echo $i_UserStudentName; ?>:</td><td><?=$select_student?></td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" onClick="JavaScript: AlertPost(document.form1, 'edit_update.php', '<?=$i_Payment_UpdateRecordConfirm?>')"></td></tr>
<? } ?>
</table>
<input type=hidden name=RecordID value="<?=$RecordID?>">
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
