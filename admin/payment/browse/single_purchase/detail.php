<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libpayment.php");

include_once("../../../../includes/libaccount.php");
if ($item == "")
{
    header("Location: index.php");
    exit();
}
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lpayment = new libpayment();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 4;

$namefield = getNameFieldByLang("b.");
if ($start == "" || $end == "") # Default current month
{
    $start = date("Y-m-d",mktime(0,0,0,date('m'),1,date('Y')));
    $end = date("Y-m-d",mktime(0,0,0,date('m')+1,-1,date('Y')));
}

$start_ts = strtotime($start);
$end_ts = strtotime($end) + 60*60*24 - 1;
if ($ClassName != "")
{
    $conds .= "AND (b.ClassName = '$ClassName' OR c.ClassName='$ClassName')";
}

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "c.ChineseName";
}else $archive_namefield = "c.EnglishName";

switch($user_type){
	case 1: $user_cond = " AND (b.RecordType=1 OR (b.RecordType IS NULL AND c.RecordType IS NULL))"; break;
	case 2: $user_cond = " AND (b.RecordType=2 OR c.RecordType=2)"; break;
	default : $user_cond = ""; break;
}

$sql  = "SELECT
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'<font color=red>*</font>',$namefield)),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)),
                IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.CardID,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.CardID)),
               ".$lpayment->getWebDisplayAmountFormatDB("a.Amount")." AS Amount,
               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i')
         FROM
             PAYMENT_OVERALL_TRANSACTION_LOG as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
         WHERE
              a.TransactionType = 3 AND a.Details = '$item'
              AND UNIX_TIMESTAMP(TransactionTime) BETWEEN $start_ts AND $end_ts
              $conds
              $user_cond
              AND ( 
              if(b.UserID IS NOT NULL,$namefield LIKE '%$keyword%',IF(c.UserID IS NOT NULL,$archive_namefield LIKE '%$keyword%','$keyword'=''))
              )              

                ";
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("b.EnglishName,c.EnglishName","b.ClassName,c.ClassName","b.ClassNumber,c.ClassNumber","b.CardID,c.CardID","a.Amount","a.TransactionTime");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;



// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_Payment_Field_Username)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_SmartCard_CardID)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Field_Amount)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";


$select_user = "<SELECT name='user_type' onChange='filterByUser(this.form)'>";
$select_user.= "<OPTION value='2'".($user_type==2?"SELECTED":"").">$i_Payment_Students</OPTION>";
$select_user.= "<OPTION value='1' ".($user_type==1?"SELECTED":"").">$i_general_Teacher</OPTION>";
$select_user.= "<OPTION value='' ".($user_type==""?"SELECTED":"").">-- $i_Payment_All --</OPTION>";
$select_user.= "</SELECT>&nbsp;&nbsp;";

$lclass = new libclass();
$temp = $button_select;
$button_select = $i_status_all;
$select_class = $lclass->getSelectClass("name=ClassName onChange=''".($user_type==2?"":"style='visibility=hidden' disabled=true"),$ClassName);
$button_select = $temp;

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
#$searchbar = $select_class;
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$toolbar2 = "<table border=0 widith=60% cellpadding=1 cellspacing=5>
	<tr><td align=right>$i_From:</td><td><input type=text name=start value='$start' size=10> (YYYY-MM-DD)</td></tr>
	<tr><td align=right>$i_To:</td><td><input type=text name=end value='$end' size=10> (YYYY-MM-DD)</td></tr>
	<tr><td align=right>$i_Payment_UserType:</td><td>$select_user $select_class</td></tr>
	<tr><td></td><Td ><a href='javascript:document.form1.submit()'><img src='/images/admin/button/s_btn_find_$intranet_session_language.gif' align='absmiddle' border=0></a></td></tr>
</table>";

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_DataBrowsing,'../',$i_Payment_Menu_Browse_PurchasingRecords,'index.php',$item,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<SCRIPT LANGUAGE=Javascript>
function openPrintPage()
{
         newWindow('detail_print.php?user_type=<?=$user_type?>&item=<?=$item?>&ClassName=<?=$ClassName?>&start=<?=$start?>&end=<?=$end?>&keyword=<?=$keyword?>&field=<?=$field?>&order=<?=$order?>',6);
}
function filterByUser(formObj){
	
	if(formObj==null) return;
	
	classNameObj = formObj.ClassName;
	userTypeObj = formObj.user_type;
	if(classNameObj==null || userTypeObj ==null) return;
	v = userTypeObj.options[userTypeObj.selectedIndex].value;
	if(v!=2){
		classNameObj.options[0].selected = true;
		classNameObj.disabled = true;
		classNameObj.style.visibility = 'hidden';
	}else{
		classNameObj.disabled = false;
		classNameObj.style.visibility = 'visible';

	}
}


</SCRIPT>
<form name="form1" method="get">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=left><?=$toolbar2?></td></tr>
<tr><td colspan=2><hr size=1 class="hr_sub_separator"><br>&nbsp;</td></tr>
<tr><td><?= $i_Payment_Note_StudentRemoved ?><br>&nbsp;</td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=item value="<?=$item?>">
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
