<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
$i_title = "$i_Payment_Title_Overall_SinglePurchasing - $item";
include_once("../../../../templates/fileheader.php");
intranet_opendb();





$lpayment = new libpayment();
if ($start == "" || $end == "") # Default current month
{
    $start = date("Y-m-d",mktime(0,0,0,date('m'),1,date('Y')));
    $end = date("Y-m-d",mktime(0,0,0,date('m')+1,-1,date('Y')));
}
$start_ts = strtotime($start);
$end_ts = strtotime($end) + 60*60*24 - 1;

if ($ClassName != "")
{
    $conds .= "AND (b.ClassName = '$ClassName' OR c.ClassName ='$ClassName')";
    $class_line = "<tr><td align=left>$i_ClassName:</td><td>$ClassName</td></tr>\n";
}
$namefield = getNameFieldByLang("b.");

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "c.ChineseName";
}else $archive_namefield = "c.EnglishName";
   
switch($user_type){
	case 1: $user_cond = " AND (b.RecordType=1 OR (b.RecordType IS NULL AND c.RecordType IS NULL))"; break;
	case 2: $user_cond = " AND (b.RecordType=2 OR c.RecordType=2)"; break;
	default : $user_cond = ""; break;
}

/*      
$sql  = "SELECT
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),$archive_namefield,$namefield),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassName,b.ClassName),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,b.ClassNumber),
               a.Amount,
               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i')
         FROM
             PAYMENT_OVERALL_TRANSACTION_LOG as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
         WHERE
              a.TransactionType = 3 AND a.Details = '$item'
              AND UNIX_TIMESTAMP(TransactionTime) BETWEEN $start_ts AND $end_ts
              $conds
              $user_cond 
              AND ($archive_namefield LIKE '%$keyword%' OR $namefield LIKE '%$keyword%')
                ";
 */
    $sql  = "SELECT
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=black>*</font><i>',$archive_namefield,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'<font color=black>*</font>',$namefield)),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)),
                IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.CardID,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.CardID)),
               ".$lpayment->getWebDisplayAmountFormatDB("a.Amount")." AS Amount,
               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i')
         FROM
             PAYMENT_OVERALL_TRANSACTION_LOG as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
         WHERE
              a.TransactionType = 3 AND a.Details = '$item'
              AND UNIX_TIMESTAMP(TransactionTime) BETWEEN $start_ts AND $end_ts
              $conds
              $user_cond
              AND ( 
              if(b.UserID IS NOT NULL,$namefield LIKE '%$keyword%',IF(c.UserID IS NOT NULL,$archive_namefield LIKE '%$keyword%','$keyword'=''))
              )              
                ";
//  AND ($archive_namefield LIKE '%$keyword%' OR $namefield LIKE '%$keyword%')

$field_array = array("b.EnglishName","b.ClassName","b.ClassNumber","b.CardID","a.Amount","a.TransactionTime");
$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";


$info = $lpayment->returnArray($sql,6);
$x = "";
for ($i=0; $i<sizeof($info); $i++)
{
     list($studentname, $class, $classnum,$card_id, $amount, $trans_time) = $info[$i];
     //$amount = "$ ".number_format($amount,1);
     //$css = ($i%2? "":"2");
     $css =$i%2==0?$css_table_content:$css_table_content."2";     
     

     $x .= "<tr class='$css'>\n";
   #  $x .= "<td>".($i+1)."</td>\n";
     $x .= "<td class='$css'>$studentname&nbsp;</td>\n";
     $x .= "<td class='$css'>$class&nbsp;</td>\n";
     $x .= "<td class='$css'>$classnum&nbsp;</td>\n";
     $x .= "<td class='$css'>$card_id&nbsp;</td>\n";
     $x .= "<td class='$css'>$amount &nbsp;</td>\n";
     $x .= "<td class='$css'>$trans_time&nbsp;</td>\n";
     #$x .= "<td>&nbsp;</td>\n";
     $x .= "</tr>\n";
}
if(sizeof($info)<=0){
	$x.="<tr class='$css_table_content'><td colspan=6 align=center  class='$css_table_content' height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}
$display = "<table width=100% border=0  cellpadding=2 cellspacing=0 align=center class='$css_table'>";
$display .= "<tr class='$css_table_title'><td class='$css_table_title'>$i_Payment_Field_Username</td><td class='$css_table_title'>$i_UserClassName</td><td class='$css_table_title'>$i_UserClassNumber</td>
<td class='$css_table_title'>$i_SmartCard_CardID</td><td class='$css_table_title'>$i_Payment_Field_Amount</td><td class='$css_table_title'>$i_Payment_Field_TransactionTime</td></tr>\n";
$display .= $x;
$display .= "</table>";

?>
<table width=95% align=center border=0 cellspacing=1 cellpadding=1>
<tr><td align=left class='<?=$css_text?>'><b><?=$i_Payment_Field_SinglePurchase_Unit?>:</b></td><td class='<?=$css_text?>'><?=$item?></td></tr>
<tr><td align=left class='<?=$css_text?>'><b><?=$i_From?>:</b></td><td class='<?=$css_text?>'><?=$start?></td></tr>
<tr><td align=left class='<?=$css_text?>'><b><?=$i_To?>:</b></td><td class='<?=$css_text?>'><?=$end?></td></tr>
<?=$class_line?>
<tr><td class='<?=$css_text?>' colspan='2'><BR><?= $i_Payment_Note_StudentRemoved2 ?></td></tr>
<tr><td colspan=2><?=$display?></td></tr>
</table>
<?

include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>
