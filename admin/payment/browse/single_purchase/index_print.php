<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
$i_title = "$i_Payment_Title_Overall_SinglePurchasing";
include_once("../../../../templates/fileheader.php");
intranet_opendb();

$lpayment = new libpayment();
if ($start == "" || $end == "") # Default current month
{
    $start = date("Y-m-d",mktime(0,0,0,date('m'),1,date('Y')));
    $end = date("Y-m-d",mktime(0,0,0,date('m')+1,-1,date('Y')));
}

$info = $lpayment->returnSinglePurchaseStatsByUserType($start,$end,$user_type);
$x = "";
for ($i=0; $i<sizeof($info); $i++)
{
     list($item, $sum, $count) = $info[$i];
     $sum = $lpayment->getWebDisplayAmountFormat($sum);
     //$css = ($i%2? "":"2");
     $css =$i%2==0?$css_table_content:$css_table_content."2";

     $x .= "<tr class='$css'>\n";
     $x .= "<td class='$css'>$item&nbsp;</td>\n";
     $x .= "<td class='$css'>$sum&nbsp;</td>\n";
     $x .= "<td class='$css'>$count&nbsp;</td>\n";
     //$x .= "<td>&nbsp;</td>\n";
     $x .= "</tr>\n";
}
if(sizeof($info)<=0)
	$x.="<tr class='$css_table_content'><td colspan=3 align=center  class='$css_table_content' height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
$display = "<table width=100% border=0  cellpadding=2 cellspacing=0 align=center class='$css_table'>";
$display .= "<tr class='$css_table_title'><td class='$css_table_title'>$i_Payment_Field_SinglePurchase_Unit</td><td class='$css_table_title'>$i_Payment_Field_SinglePurchase_TotalAmount</td><td class='$css_table_title'>$i_Payment_Field_SinglePurchase_Count</td></tr>\n";
$display .= $x;
$display .= "</table>";
/*
<tr><td align=right><=$i_Payment_Field_SinglePurchase_Unit>:</td><td><=$item></td></tr>
*/
?>
<table width=95% align=center border=0 cellspacing=1 cellpadding=1>
<tr><td align=left class='<?=$css_title?>'><b><?=$i_Payment_Menu_Browse_PurchasingRecords?></b></td></tr>
</table>
<table width=95% align=center border=0 cellspacing=1 cellpadding=1>
<tr><td align=left class='<?=$css_text?>'><b><?=$i_Profile_From?>:</b></td><td class='<?=$css_text?>'><?=$start?></td></tr>
<tr><td align=left class='<?=$css_text?>'><B><?=$i_Profile_To?>:</b></td><td class='<?=$css_text?>'><?=$end?></td></tr>
<tr><td colspan=2><?=$display?></td></tr>
</table>
<?

include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>
