<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lpayment = new libpayment();
$lclass = new libclass();

$usertype = array(
array(1,$i_identity_teachstaff),
array(2,$i_identity_student));

$teacherstatus = array(
array(1,$i_status_suspended),
array(2,$i_status_approved),
array(3,$i_status_pendinguser));

$studentstatus = array(
array(1,$i_status_suspended),
array(2,$i_status_approved),
array(3,$i_status_pendinguser),
array(4,$i_status_graduate));

$format_array = array(
array(0,"Web"),
array(1,"CSV"));

$UserType_Selection = getSelectByArray($usertype,"name=UserType onChange=\"this.form.submit();\"",$UserType,1,0,0);
//$UserStatus_Selection = getSelectByArray($userstatus,"name=UserStatus onChange=\"this.form.submit();\"",$UserStatus);

if($UserType==2)
{
	$class_list = $lclass->getClassList();
	$Class_Selection = "<tr><td align=right>$i_ClassName:</td>";
	$Class_Selection .= "<td>".getSelectByArray($class_list,'name=TargetClass',0,1,0,0)."</td></tr>";
}


//$UserType_Selection = "<input type=checkbox name=UserType[] value=1>$i_identity_teachstaff <input type=checkbox name=UserType[] value=2>$i_identity_student";
if($UserType==1)
	$UserStatus_Selection = "<input type=checkbox name=UserStatus[] value=0>$i_status_suspended <input type=checkbox name=UserStatus[] value=1>$i_status_approved <input type=checkbox name=UserStatus[] value=2>$i_status_pendinguser";
else
	$UserStatus_Selection = "<input type=checkbox name=UserStatus[] value=0>$i_status_suspended <input type=checkbox name=UserStatus[] value=1>$i_status_approved <input type=checkbox name=UserStatus[] value=2>$i_status_pendinguser <input type=checkbox name=UserStatus[] value=3>$i_status_graduate";
$File_Type = getSelectByArray($format_array,"name=FileFormat",0,0,1);

?>

<SCRIPT Language="JavaScript">
function checkForm(obj)
{
	var obj1 = obj["UserType[]"];
	var obj2 = obj["UserStatus[]"];
	var checking1 = 0;
	var checking2 = 0;
	
	for(i=0; i<obj1.length; i++)
		if(obj1[i].checked)
			checking1 = 1;
	
	for(j=0; j<obj2.length; j++)
		if(obj2[j].checked)
			checking2 = 1;
	
	if(checking1 == 1)
	{
		if(checking2 == 1)
		{
			obj.target = "_blank";
			if(obj.FileFormat.value==0)
			{
				obj.action='web_print.php';
			}
			else
			{
				obj.action='csv_print.php';
			}
		}
		else
		{
			alert("<?=$i_Payment_PrintPage_Browse_UserStatusWarning?>");
			return false;
		}
	}
	else
	{
		alert("<?=$i_Payment_PrintPage_Browse_UserIdentityWarning?>");
		return false;
	}
}
</SCRIPT>

<?=displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_DataBrowsing,'../',$i_Payment_Menu_PrintPage,'')?>
<?=displayTag("head_payment_$intranet_session_language.gif", $msg)?>

<form name=form1 action="" method="post" onSubmit="return checkForm(this);">
<table width=340 border=0 align=center>
<tr><td align=right><?=$i_identity?>:</td><td><?=$UserType_Selection?></td></tr>
<tr><td align=right></td><td><?=$Class_Selection?></td></tr>
<tr><td align=right><?=$i_general_status?>:</td><td><?=$UserStatus_Selection?></td></tr>
<tr><td align=right><?=$i_general_Format?>:</td><td><?=$File_Type?></td></tr>
</table>

<table width=560 border=0 align=center>
<tr><td height=10px></td></tr>
<tr><td colspan=2><hr size=1></td></tr>
</table>

<table width=340 border=0 align=center>
<tr><td colspan=2 align=right><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"><?= " ". btnReset() ?></td></tr>
</table>

</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>