<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();

$lpayment = new libpayment();

if(sizeof($UserType)>0)
	$temp_UserType = implode(",",$UserType);
if(sizeof($UserStatus)>0)
	$temp_UserStatus = implode(",",$UserStatus);

if($temp_UserStatus!="")
	$cond = " AND RecordStatus IN ($temp_UserStatus)";
else
	$cond = " ";
	
$sql = "SELECT * FROM INTRANET_USER WHERE RecordType IN ($temp_UserType) $cond";

$target_user = $lpayment->returnVector($sql);

if(sizeof($target_user)>0)
{
	$user_list = implode(",",$target_user);
	
	$namefield = getNameFieldWithClassNumberByLang("a.");
	
	$sql = "SELECT 
					a.UserID, $namefield, IF(b.PPSAccountNo IS NULL, ' -- ', b.PPSAccountNo), CONCAT('$',IF(b.Balance>=0,FORMAT(b.Balance,1),'0.0') ), if(b.LastUpdated IS NULL, ' -- ', b.LastUpdated)
			FROM 
					INTRANET_USER AS a LEFT OUTER JOIN 
					PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
			WHERE
					a.UserID IN ($user_list)
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName";

	$payment_account_result = $lpayment->returnArray($sql, 5);
	
	if(sizeof($payment_account_result)>0)
	{
		$lineRemain = $defaultNumOfLine - $page_header_linefeed;
		
		for($i=0; $i<sizeof($payment_account_result); $i++)
		{
			list($u_id, $name, $pps, $bal, $last_modified) = $payment_account_result[$i];

			$sql2  = "SELECT
	               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
	               IF(a.TransactionType=1,DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),' -- '),
	               CASE a.TransactionType
	                    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
	                    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
	                    WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
	                    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
	                    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
	                    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
	                    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
	                    ELSE '$i_Payment_TransactionType_Other' END,
	               IF(a.TransactionType IN (1,5,6),CONCAT('$',FORMAT(a.Amount,1)),' -- '),
	               IF(a.TransactionType IN (2,3,4,7),CONCAT('$',FORMAT(a.Amount,1)),' -- '),
	               a.Details, CONCAT('$',IF(a.BalanceAfter>=0,FORMAT(a.BalanceAfter,1),'0.0') ),
	               IF(a.RefCode IS NULL, ' -- ', a.RefCode)
	         FROM
	               PAYMENT_OVERALL_TRANSACTION_LOG as a
	               LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as b ON a.RelatedTransactionID = b.TransactionID
	         WHERE
	               a.StudentID =$u_id";

			$file_content .= "\"$i_Payment_Field_Username:\",\"$name\"\n";
			$file_content .= "\"$i_Payment_Field_Balance:\",\"$bal\"\n";
			$file_content .= "\"$i_Payment_Field_LastUpdated:\",\"$last_modified\"\n";
			$file_content .= "\"$i_Payment_Field_PPSAccountNo:\",\"$pps\"\n";
			$file_content .= "\"$i_Payment_Field_TransactionTime\",\"$i_Payment_Field_TransactionFileTime\",\"$i_Payment_Field_TransactionType\",";
			$file_content .= "\"$i_Payment_TransactionType_Credit\",\"$i_Payment_TransactionType_Debit\",\"$i_Payment_Field_TransactionDetail\",";
			$file_content .= "\"$i_Payment_Field_BalanceAfterTransaction\",\"$i_Payment_Field_RefCode\"\n";
			       
			$payment_detail_result = $lpayment->returnArray($sql2,7);
			
			if(sizeof($payment_detail_result)>0)
			{
				for ($z=0; $z<sizeof($payment_detail_result); $z++)
				{
		            list($transaction_time, $credit_transaction_time, $transaction_type, $credit_amount, $debit_amount, $transaction_detail, $balance_after, $ref_no) = $payment_detail_result[$z];
				}
				
				for($j=0; $j<sizeof($payment_detail_result); $j++)
				{
					list($transaction_time, $credit_transaction_time, $transaction_type, $credit_amount, $debit_amount, $transaction_detail, $balance_after, $ref_no) = $payment_detail_result[$j];
					$file_content .= "\"$transaction_time\",\"$credit_transaction_time\",\"$transaction_type\",\"$credit_amount\",";
					$file_content .= "\"$debit_amount\",\"$transaction_detail\",\"$balance_after\",\"$ref_no\"\n";
				}
			}
			if(sizeof($payment_detail_result)==0)
			{
				$file_content .= "\"$i_no_record_exists_msg\"\n";
			}
			$file_content .= "\n";
			$file_content .= "\n";
		}
	}
}
?>
<?
if(sizeof($UserType)==1)
{
	for($i=0; $i<sizeof($UserType); $i++)
	{
		$temp = $UserType[$i];
		if($temp == 1)
			$filename = $i_identity_teachstaff;
		if($temp == 2)
			$filename = $i_identity_student;
	}
}
if(sizeof($UserType)==2)
{
	$filename = "All_Users";	
}
/*
if(sizeof($UserStatus)>0)
{
	for($i=0; $i<sizeof($UserStatus); $i++)
	{
		$temp = $UserStatus[$i];
		if($temp == 0)
			$status .= $i_status_suspended;
		if($temp == 1)
			$status .= $i_status_approved;
		if($temp == 2)
			$status .= $i_status_pendinguser;
	}
}
*/
output2browser($file_content,"Detail_Transaction_Log($filename)".date('Y-m-d').".csv");

?>