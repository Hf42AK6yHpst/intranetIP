<?php
// using kenneth chung
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");
intranet_opendb();

$lpayment = new libpayment();

if(!empty($StartDate) && !empty($EndDate))
{
	$date_range_cond = " AND (a.TransactionTime BETWEEN '$StartDate' AND '$EndDate') ";
}
if(!empty($StartDate) && empty($EndDate))
{
	$date_range_cond = " AND (a.TransactionTime >= '$StartDate') ";
}
if(empty($StartDate) && !empty($EndDate))
{
	$date_range_cond = " AND (a.TransactionTime <= '$EndDate') ";	
}

$li = new libdb();

if($RecordStatus != 4){
	$namefield = getNameFieldWithClassNumberByLang("a.");
	
	$sql = "SELECT 
					a.UserID, $namefield, a.UserLogin,IF(b.PPSAccountNo IS NULL, ' -- ', b.PPSAccountNo), IF(b.Balance>=0,".$lpayment->getWebDisplayAmountFormatDB("b.Balance").",".$lpayment->getWebDisplayAmountFormatDB("0")."), if(b.LastUpdated IS NULL, ' -- ', b.LastUpdated)
			FROM 
					INTRANET_USER AS a LEFT OUTER JOIN 
					PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
			WHERE
					a.UserID ='$StudentID'
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName";
} else {
	$username_field = getNameFieldByLang2("a.");
	$namefield = "CONCAT($username_field,IF($prefix"."ClassNumber IS NULL OR $prefix"."ClassNumber = '','',CONCAT(' (',$prefix"."ClassName,'-',$prefix"."ClassNumber,')')))";
	
	$sql = "SELECT 
					a.UserID, $namefield, a.UserLogin,IF(b.PPSAccountNo IS NULL, ' -- ', b.PPSAccountNo), IF(b.Balance>=0,".$lpayment->getWebDisplayAmountFormatDB("b.Balance").",".$lpayment->getWebDisplayAmountFormatDB("0")."), if(b.LastUpdated IS NULL, ' -- ', b.LastUpdated)
			FROM 
					INTRANET_ARCHIVE_USER AS a LEFT OUTER JOIN 
					PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
			WHERE
					a.UserID ='$StudentID'
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName";
}

	$payment_account_result = $lpayment->returnArray($sql, 6);
	list($u_id, $name, $login,$pps, $bal, $last_modified)=$payment_account_result[0];
    $table_header = "<table width=95% border=0 cellpadding=0 cellspacing=0>";
	$table_header .= "<tr><td class='$css_text'><b>$i_Payment_Field_Username : </b>$name</td><td class='$css_text'><b>$i_UserLogin : </b>$login</td></tr>";
	$table_header .= "<tr><td class='$css_text'><B>$i_Payment_Field_Balance : </b>$bal</td><td class='$css_text'><b>$i_Payment_Field_LastUpdated : </b>$last_modified</td></tr>";
	$table_header .= "<tr><td class='$css_text'><B>$i_Payment_Field_PPSAccountNo : </b>$pps</td><td class='$css_text'><B>$i_Payment_Report_Print_Date : </b>".date('Y-m-d H:i:s')."</td></tr>";	
	$table_header .="</table>";

$sql  = "SELECT
               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
               IF(a.TransactionType=1,DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),'--'),
               CASE a.TransactionType
                    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
                    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
                    WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
                    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
                    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
                    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
                    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
                    WHEN 8 THEn '$i_Payment_PPS_Charge'
                    ELSE '$i_Payment_TransactionType_Other' END,
               IF(a.TransactionType IN (1,5,6),".$lpayment->getWebDisplayAmountFormatDB("a.Amount").",'&nbsp;'),
               IF(a.TransactionType IN (2,3,4,7,8,9,10),".$lpayment->getWebDisplayAmountFormatDB("a.Amount").",'&nbsp;'),
               a.Details, IF(a.BalanceAfter>=0,".$lpayment->getWebDisplayAmountFormatDB("a.BalanceAfter").",".$lpayment->getWebDisplayAmountFormatDB("0")." ),
               a.RefCode
         FROM
             PAYMENT_OVERALL_TRANSACTION_LOG as a
               LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as b ON a.RelatedTransactionID = b.TransactionID
         WHERE
              a.StudentID = $StudentID AND
              (
               a.Details LIKE '%$keyword%'
              )
              $date_range_cond
              ORDER BY a.TransactionTime, a.LogID
                ";

$result = $li->returnArray($sql,8);

$display = "<table width=95% border=0  cellpadding=2 cellspacing=0 class='$css_table'>";
$display .= "<tr class='$css_table_title'>
<td width=15% class='$css_table_title'>$i_Payment_Field_TransactionTime</td>
<td width=15% class='$css_table_title'>$i_Payment_Field_TransactionFileTime</td>
<td width=10% class='$css_table_title'>$i_Payment_Field_TransactionType</td>
<td width=10% class='$css_table_title'>$i_Payment_TransactionType_Credit</td>
<td width=10% class='$css_table_title'>$i_Payment_TransactionType_Debit</td>
<td width=15% class='$css_table_title'>$i_Payment_Field_TransactionDetail</td>
<td width=15% class='$css_table_title'>$i_Payment_Field_BalanceAfterTransaction</td>
<td width=10% class='$css_table_title'>$i_Payment_Field_RefCode</td></tr>\n";
for ($i=0; $i<sizeof($result); $i++)
{
     //$css = ($i%2? "":"2");
          	$css =$i%2==0?$css_table_content:$css_table_content."2";
     list($transaction_time, $addv_file_time, $transaction_type, $credit, $debit, $details, $balance, $ref_code) = $result[$i];
     if ($ref_code == "") $ref_code = "--";
     $display .= "<tr class='$css'><td class='$css'>$transaction_time</td><td class='$css'>$addv_file_time</td><td class='$css'>$transaction_type</td><td class='$css'>$credit</td><td class='$css'>$debit</td><td class='$css'>$details</td><td class='$css'>$balance</td><td class='$css'>$ref_code</td></tr>\n";

}
if(sizeof($result)<=0){
	$display.="<tr class='$css_table_content'><td colspan=8 align=center  class='$css_table_content' height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}
$display .= "</table>";


echo $table_header;
echo "<BR>";
echo $display;
include_once("../../../../templates/filefooter.php");
intranet_closedb();

?>
