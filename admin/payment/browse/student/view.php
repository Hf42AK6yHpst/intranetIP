<?php
// using kenneth chung
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libuser.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

if ($StudentID == "")
{
    header("Location: index.php");
    exit();
}

include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lpayment = new libpayment();
$lu = new libuser($StudentID);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

if(!empty($TargetStartDate) && !empty($TargetEndDate))
{
	$start_day = substr($TargetStartDate,8,2);
	$start_month = substr($TargetStartDate,5,2);
	$start_year = substr($TargetStartDate,0,4);
	$StartDate = date("Y-m-d H:i:s",mktime(0,0,0,$start_month,$start_day,$start_year));
	
	$end_day = substr($TargetEndDate,8,2);
	$end_month = substr($TargetEndDate,5,2);
	$end_year = substr($TargetEndDate,0,4);
	$EndDate = date("Y-m-d H:i:s",mktime(23,59,59,$end_month,$end_day,$end_year));
	
	$date_range_cond = " AND (a.TransactionTime BETWEEN '$StartDate' AND '$EndDate') ";
}
if(!empty($TargetStartDate) && empty($TargetEndDate))
{
	$start_day = substr($TargetStartDate,8,2);
	$start_month = substr($TargetStartDate,5,2);
	$start_year = substr($TargetStartDate,0,4);
	$StartDate = date("Y-m-d H:i:s",mktime(0,0,0,$start_month,$start_day,$start_year));
	
	$date_range_cond = " AND (a.TransactionTime >= '$StartDate') ";
}
if(empty($TargetStartDate) && !empty($TargetEndDate))
{
	$end_day = substr($TargetEndDate,8,2);
	$end_month = substr($TargetEndDate,5,2);
	$end_year = substr($TargetEndDate,0,4);
	$EndDate = date("Y-m-d H:i:s",mktime(23,59,59,$end_month,$end_day,$end_year));
	
	$date_range_cond = " AND (a.TransactionTime <= '$EndDate') ";
}

$sql  = "SELECT
               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
               IF(a.TransactionType=1,DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),'--'),
               CASE a.TransactionType
                    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
                    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
                    WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
                    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
                    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
                    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
                    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
                    WHEN 8 THEn '$i_Payment_PPS_Charge'
                    ELSE '$i_Payment_TransactionType_Other' END,
               IF(a.TransactionType IN (1,5,6),".$lpayment->getWebDisplayAmountFormatDB("a.Amount").",'&nbsp;'),
               IF(a.TransactionType IN (2,3,4,7,8,9,10),".$lpayment->getWebDisplayAmountFormatDB("a.Amount").",'&nbsp;'),
               a.Details, IF(a.BalanceAfter>=0,".$lpayment->getWebDisplayAmountFormatDB("a.BalanceAfter").",".$lpayment->getWebDisplayAmountFormatDB("0")." ),
               a.RefCode
         FROM
             PAYMENT_OVERALL_TRANSACTION_LOG as a
               LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as b ON a.RelatedTransactionID = b.TransactionID
         WHERE
              a.StudentID = $StudentID AND
              (
               a.Details LIKE '%$keyword%'
              )
              $date_range_cond
                ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.TransactionTime");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+8;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0);
$li->IsColOff = 2;
$li->fieldorder2 = " , a.LogID " . (($li->order==0) ? " DESC" : " ASC");

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$i_Payment_Field_TransactionFileTime."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$i_Payment_Field_TransactionType."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$i_Payment_TransactionType_Credit."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$i_Payment_TransactionType_Debit."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$i_Payment_Field_TransactionDetail."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$i_Payment_Field_BalanceAfterTransaction."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$i_Payment_Field_RefCode."</td>\n";
#$li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentID[]")."</td>\n";

#$toolbar = "<a class=iconLink href=javascript:checkNew('new.php')>".newIcon()."$button_new</a>";
#$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'TerminalUserID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
#$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'TerminalUserID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

if($RecordStatus != 4) {
	echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_DataBrowsing,'../',$i_Payment_Menu_Browse_StudentBalance,'index.php',$i_Payment_Menu_Browse_TransactionRecord." [".$lu->UserNameClassNumber()."]",'');
}else{
	$arr_archive_student = $lu->ReturnArchiveStudentInfo($StudentID);
	if(sizeof($arr_archive_student)>0){
		$user_name = $arr_archive_student[0];
		$user_login = $arr_archive_student[1]; 
		$class_name = $arr_archive_student[2];
		$class_num = $arr_archive_student[3]; 
		$year_of_left = $arr_archive_student[4];
	}
	
	echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_DataBrowsing,'../',$i_Payment_Menu_Browse_StudentBalance,'index.php',$i_Payment_Menu_Browse_TransactionRecord." [".$user_name." (".$class_name."-".$class_num.")"."]",'');
}

echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<SCRIPT LANGUAGE=Javascript>
function openPrintPage()
{
         newWindow('view_print.php?StudentID=<?=$StudentID?>&keyword=<?=$keyword?>&StartDate=<?=$StartDate;?>&EndDate=<?=$EndDate?>&RecordStatus=<?=$RecordStatus?>',10);
}
function checkForm()
{
	var obj = document.form1;
	var err = 0;
	var start_date_isset = 0;
	var end_date_isset = 0;
	var comp_date = 0;
	
	if(obj.TargetStartDate.value != '')
	{
		start_date_isset = 1;
		if(check_date(obj.TargetStartDate,"<?=$i_Payment_ViewStudentAccount_InvalidStartDate;?>") == false)
		{
			obj.TargetStartDate.value = '';
			obj.TargetEndDate.value = '';
			obj.TargetStartDate.focus();
			return false;
		}
	}
	if(obj.TargetEndDate.value != '')
	{
		end_date_isset = 1;
		if(check_date(obj.TargetEndDate,"<?=$i_Payment_ViewStudentAccount_InvalidEndDate;?>") == false)
		{
			obj.TargetStartDate.value = '';
			obj.TargetEndDate.value = '';
			obj.TargetEndDate.focus();
			return false;
		}
	}
	
	if((start_date_isset == 1) && (end_date_isset == 1))
	{
		if(compareDate(obj.TargetEndDate.value,obj.TargetStartDate.value) != 1)
		{
			alert("<?=$i_Payment_ViewStudentAccount_InvalidDateRange;?>");
			obj.TargetStartDate.value = '';
			obj.TargetEndDate.value = '';
			obj.TargetStartDate.focus();
			return false;
		}
	}
	return true;
}
</SCRIPT>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
 <script LANGUAGE="javascript">
	 var css_array = new Array;
	 css_array[0] = "dynCalendar_free";
	 css_array[1] = "dynCalendar_half";
	 css_array[2] = "dynCalendar_full";
	 var date_array = new Array;
 </script>

 <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
 <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
 <script type="text/javascript">
 <!--
	  // Calendar callback. When a date is clicked on the calendar
	  // this function is called so you can do as you want with it
	  function startCalendarCallback(date, month, year)
	  {
			   if (String(month).length == 1) {
				   month = '0' + month;
			   }

			   if (String(date).length == 1) {
				   date = '0' + date;
			   }
			   dateValue =year + '-' + month + '-' + date;
			   document.forms['form1'].TargetStartDate.value = dateValue;
	  }
	  function endCalendarCallback(date, month, year)
	  {
			   if (String(month).length == 1) {
				   month = '0' + month;
			   }

			   if (String(date).length == 1) {
				   date = '0' + date;
			   }
			   dateValue =year + '-' + month + '-' + date;
			   document.forms['form1'].TargetEndDate.value = dateValue;
	  }
 // -->
 </script>
<form name="form1" method="get" onSubmit="return checkForm();">

<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr><td align=right><?php echo $i_general_startdate; ?>:</td><td>
	<input type=text name=TargetStartDate value='<?=$TargetStartDate;?>' size=10><script language="JavaScript" type="text/javascript">
	    <!--
	         startCal = new dynCalendar('startCal', 'startCalendarCallback', '/templates/calendar/images/');
	    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
	</td></tr>
	<tr><td align=right><?php echo $i_general_enddate; ?>:</td><td>
	<input type=text name=TargetEndDate value='<?=$TargetEndDate;?>' size=10><script language="JavaScript" type="text/javascript">
	    <!--
	         endCal = new dynCalendar('endCal', 'endCalendarCallback', '/templates/calendar/images/');
	    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
	</td><td align="left"><?= btnSubmit() ." ". btnReset() ?></td></tr>
	<tr><td colspan="3"><hr size=1 class="hr_sub_separator"></td></tr>
</table>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=StudentID value="<?=$StudentID?>">
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
