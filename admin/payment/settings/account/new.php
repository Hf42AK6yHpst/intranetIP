<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$li = new libdb();

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_TerminalAccount,'index.php',$button_new,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);

?>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
     if(!check_text(obj.Username, "<?php echo $i_alert_pleasefillin.$i_Payment_Field_Username; ?>.")) return false;
     if(!check_text(obj.Password, "<?php echo $i_alert_pleasefillin.$i_UserPassword; ?>.")) return false;
     if (obj.Password.value != obj.RePassword.value)
     {
         alert("<?=$i_frontpage_myinfo_password_mismatch?>");
         return false;
     }
     if (!obj.paymentAllowed.checked && !obj.purchaseAllowed.checked)
     {
          alert("<?=$i_Payment_alert_selectOneFunction?>");
          return false;
     }

}
</SCRIPT>
<form name=form1 action="new_update.php" method=POST ONSUBMIT="return checkform(this)">
<table width=90% border=0 align=center>
<tr><td><?=$i_Payment_Field_Username?></td><td><input type=text name=Username size=10 maxlength=10></td></tr>
<tr><td><?=$i_UserPassword?></td><td><input type=password name=Password size=10 maxlength=10></td></tr>
<tr><td><?=$i_frontpage_myinfo_password_retype?></td><td><input type=password name=RePassword size=10 maxlength=10></td></tr>
<tr><td><?=$i_Payment_Field_Function?></td><td>
<input type=checkbox name=paymentAllowed value=1> <?=$i_Payment_Field_PaymentAllowed?>
<input type=checkbox name=purchaseAllowed value=1> <?=$i_Payment_Field_PurchaseAllowed?>
</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
</form>
<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
