<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$TerminalUserID = (is_array($TerminalUserID)? $TerminalUserID[0]:$TerminalUserID);

$li = new libdb();
$sql = "SELECT Username, PaymentAllowed, PurchaseAllowed FROM PAYMENT_TERMINAL_USER WHERE TerminalUserID = $TerminalUserID";
$temp = $li->returnArray($sql,3);
list($Username, $payment, $purchase) = $temp[0];


include_once("../../../../templates/fileheader.php");
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_TerminalAccount,'index.php',$button_edit,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
     if(!check_text(obj.Username, "<?php echo $i_alert_pleasefillin.$i_Payment_Field_Username; ?>.")) return false;
     if (!obj.paymentAllowed.checked && !obj.purchaseAllowed.checked)
     {
          alert("<?=$i_Payment_alert_selectOneFunction?>");
          return false;
     }
     if (obj.needChangePwd.checked)
     {
         if(!check_text(obj.Password, "<?php echo $i_alert_pleasefillin.$i_UserPassword; ?>.")) return false;
         if (obj.Password.value != obj.RePassword.value)
         {
             alert("<?=$i_frontpage_myinfo_password_mismatch?>");
             return false;
         }
     }

}
function change(obj)
{
         obj.form.Password.disabled = !obj.checked;
         obj.form.RePassword.disabled = !obj.checked;
}
</SCRIPT>
<form name=form1 action="edit_update.php" method=POST ONSUBMIT="return checkform(this)">
<table width=90% border=0 align=center>
<tr><td><?=$i_Payment_Field_Username?></td><td><input type=text name=Username size=10 maxlength=10 value="<?=$Username?>"></td></tr>
<tr><td><?=$i_Payment_Field_ChangePassword?></td><td><input type=checkbox name=needChangePwd ONCHANGE="change(this)"></td></tr>
<tr><td><?=$i_UserPassword?></td><td><input type=password name=Password size=10 maxlength=10 DISABLED></td></tr>
<tr><td><?=$i_frontpage_myinfo_password_retype?></td><td><input type=password name=RePassword size=10 maxlength=10 DISABLED></td></tr>
<tr><td><?=$i_Payment_Field_Function?></td><td>
<input type=checkbox name=paymentAllowed value=1 <?=($payment==1?"CHECKED":"")?>> <?=$i_Payment_Field_PaymentAllowed?>
<input type=checkbox name=purchaseAllowed value=1 <?=($purchase==1?"CHECKED":"")?>> <?=$i_Payment_Field_PurchaseAllowed?>
</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
<input type=hidden name=TerminalUserID value="<?=$TerminalUserID?>">
</form>
<?
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>
