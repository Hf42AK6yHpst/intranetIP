<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();


$lexport = new libexporttext();

$lpayment = new libpayment();


if(is_array($UnitID))
	$UnitID = $UnitID[0];

// get summary	
$sql="SELECT SUM(a.SubsidyAmount) FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a WHERE a.SubsidyUnitID ='$UnitID' ";
$temp = $lpayment->returnVector($sql);
$unit_used_amount = $lpayment->getExportAmountFormat($temp[0]);
	
	
$sql="SELECT UnitName,TotalAmount FROM PAYMENT_SUBSIDY_UNIT WHERE UnitID = '$UnitID'";
$temp = $lpayment->returnArray($sql,2);
list($unit_name,$unit_total_amount) = $temp[0];

$unit_left_amount = $unit_total_amount - $unit_used_amount;
$unit_left_amount = $unit_left_amount > 0 ?$unit_left_amount:0;
$unit_left_amount = $lpayment->getExportAmountFormat($unit_left_amount);
$unit_total_amount = $lpayment->getExportAmountFormat($unit_total_amount);
//$unit_total_amount = number_format($unit_total_amount,1);
//$unit_used_amount  = number_format($unit_used_amount,1);
//$unit_left_amount  = number_format($unit_left_amount,1);

$sql="
	SELECT 
		COUNT(*), ".$lpayment->getExportAmountFormatDB("SUM(a.SubsidyAmount)")."
	FROM 
		PAYMENT_PAYMENT_ITEMSTUDENT AS a LEFT OUTER JOIN
		PAYMENT_PAYMENT_ITEM AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
		INTRANET_USER AS c ON (a.StudentID = c.UserID) LEFT OUTER JOIN
		INTRANET_ARCHIVE_USER AS d ON (a.StudentID = d.UserID)
	WHERE a.SubsidyUnitID ='$UnitID'  AND 
              (
              a.SubsidyAmount LIKE '%$keyword%' OR
              a.SubsidyPICAdmin LIKE '%$keyword%' OR
              a.DateModified LIKE '%$keyword%' OR
              c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.EnglishName LIKE '%$keyword%' OR
               d.ChineseName LIKE '%$keyword%' OR
               d.ClassName LIKE '%$keyword%' OR
               d.ClassNumber LIKE '%$keyword%' OR
               b.Name LIKE '%$keyword%'

              )	
	";
	
$temp = $lpayment->returnArray($sql,2);
$no_of_students = $temp[0][0];	              
$search_amount = $lpayment->getExportAmountFormat($temp[0][1]);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;


if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "d.ChineseName";
}else $archive_namefield = "d.EnglishName";

$namefield = getNameFieldWithClassNumberByLang("c.");

$sql="
	SELECT 
		IF((c.UserID IS NULL AND d.UserID IS NOT NULL),CONCAT('*',$archive_namefield), $namefield),
		b.Name,".$lpayment->getExportAmountFormatDB("a.SubsidyAmount").",a.SubsidyPICAdmin,a.DateModified 
	FROM 
		PAYMENT_PAYMENT_ITEMSTUDENT AS a LEFT OUTER JOIN
		PAYMENT_PAYMENT_ITEM AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
		INTRANET_USER AS c ON (a.StudentID = c.UserID) LEFT OUTER JOIN
		INTRANET_ARCHIVE_USER AS d ON (a.StudentID = d.UserID)
	WHERE a.SubsidyUnitID ='$UnitID'  AND 
              (
              a.SubsidyAmount LIKE '%$keyword%' OR
              a.SubsidyPICAdmin LIKE '%$keyword%' OR
              a.DateModified LIKE '%$keyword%' OR
              c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.EnglishName LIKE '%$keyword%' OR
               d.ChineseName LIKE '%$keyword%' OR
               d.ClassName LIKE '%$keyword%' OR
               d.ClassNumber LIKE '%$keyword%' OR
               b.Name LIKE '%$keyword%'

              )	
	";            
# TABLE INFO

$field_array = array("$namefield","b.Name","a.SubsidyAmount","a.SubsidyPICAdmin","a.DateModified");

$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();

$temp = $li->returnArray($sql,5);



$x="\"$i_Payment_Subsidy_Unit\",\"$unit_name\"\n";
$x.="\"$i_Payment_Subsidy_Total_Amount\",\"$unit_total_amount\"\n";
$x.="\"$i_Payment_Subsidy_Used_Amount\",\"$unit_used_amount\"\n";
$x.="\"$i_Payment_Subsidy_Left_Amount\",\"$unit_left_amount\"\n";
$x.="\"$i_Payment_Subsidy_Search_Amount\",\"$search_amount ($no_of_students $i_Payment_Students)\"\n";



$utf_x=$i_Payment_Subsidy_Unit."\t";
$utf_x.=$i_Payment_Subsidy_Total_Amount."\t\r\n";

$utf_x=$i_Payment_Subsidy_Unit."\t".$unit_name."\r\n";
$utf_x.=$i_Payment_Subsidy_Total_Amount."\t".$unit_total_amount."\r\n";
$utf_x.=$i_Payment_Subsidy_Used_Amount."\t".$unit_used_amount."\r\n";
$utf_x.=$i_Payment_Subsidy_Left_Amount."\t".$unit_left_amount."\r\n";
$utf_x.=$i_Payment_Subsidy_Search_Amount."\t"."$search_amount ($no_of_students $i_Payment_Students)"."\r\n";


$x.= "\"$i_UserStudentName\",";
$x.= "\"$i_Payment_Field_PaymentItem\",";
$x.= "\"$i_Payment_Subsidy_Amount\",";
$x.= "\"$i_Payment_Subsidy_Unit_Admin\",";
$x.= "\"$i_Payment_Subsidy_Unit_UpdateTime\"\n";

$utf_x.= $i_UserStudentName."\t";
$utf_x.= $i_Payment_Field_PaymentItem."\t";
$utf_x.= $i_Payment_Subsidy_Amount."\t";
$utf_x.= $i_Payment_Subsidy_Unit_Admin."\t";
$utf_x.= $i_Payment_Subsidy_Unit_UpdateTime."\r\n";


if(sizeof($temp)<=0){
        $x.="\"$i_no_record_exists_msg\"\n";
        $utf_x.=$i_no_record_exists_msg."\r\n";
}else{
	for($i=0;$i<sizeof($temp);$i++){
		list($student_name,$item_name,$sub_amount,$last_modified,$last_modified_date)=$temp[$i];
		$x.="\"$student_name\",\"$item_name\",\"$sub_amount\",\"$last_modified\",\"$last_modified_date\"\n";
		$utf_x.=$student_name."\t".$item_name."\t".$sub_amount."\t".$last_modified."\t".$last_modified_date."\r\n";

	}	
	$x.="\n\"".$i_Payment_Note_StudentRemoved2."\"\n";
	$utf_x.="\r\n".$i_Payment_Note_StudentRemoved2."\r\n";
}

$content = $x;

$utf_content=$utf_x;


intranet_closedb();

$filename = "subsidy_unit_studentlist.csv";
if ($g_encoding_unicode) {
        $lexport->EXPORT_FILE($filename, $utf_content);
} else {
        output2browser($content,$filename);
}


?>
