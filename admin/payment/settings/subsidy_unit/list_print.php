<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");
intranet_opendb();


$lpayment = new libpayment();


if(is_array($UnitID))
	$UnitID = $UnitID[0];

// get summary	
$sql="SELECT SUM(a.SubsidyAmount) FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a WHERE a.SubsidyUnitID ='$UnitID' ";
$temp = $lpayment->returnVector($sql);
$unit_used_amount = $temp[0];
	
	
$sql="SELECT UnitName,TotalAmount FROM PAYMENT_SUBSIDY_UNIT WHERE UnitID = '$UnitID'";
$temp = $lpayment->returnArray($sql,2);
list($unit_name,$unit_total_amount) = $temp[0];

$unit_left_amount = $unit_total_amount - $unit_used_amount;
$unit_left_amount = $unit_left_amount > 0 ?$unit_left_amount:0;
$unit_total_amount = $lpayment->getWebDisplayAmountFormat($unit_total_amount);
$unit_used_amount  = $lpayment->getWebDisplayAmountFormat($unit_used_amount);
$unit_left_amount  =  $lpayment->getWebDisplayAmountFormat($unit_left_amount);

$sql="
	SELECT 
		COUNT(*), ".$lpayment->getWebDisplayAmountFormatDB("SUM(a.SubsidyAmount)")."
	FROM 
		PAYMENT_PAYMENT_ITEMSTUDENT AS a LEFT OUTER JOIN
		PAYMENT_PAYMENT_ITEM AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
		INTRANET_USER AS c ON (a.StudentID = c.UserID) LEFT OUTER JOIN
		INTRANET_ARCHIVE_USER AS d ON (a.StudentID = d.UserID)
	WHERE a.SubsidyUnitID ='$UnitID'  AND 
              (
              a.SubsidyAmount LIKE '%$keyword%' OR
              a.SubsidyPICAdmin LIKE '%$keyword%' OR
              a.DateModified LIKE '%$keyword%' OR
              c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.EnglishName LIKE '%$keyword%' OR
               d.ChineseName LIKE '%$keyword%' OR
               d.ClassName LIKE '%$keyword%' OR
               d.ClassNumber LIKE '%$keyword%' OR
               b.Name LIKE '%$keyword%'

              )	
	";
	
$temp = $lpayment->returnArray($sql,2);
$no_of_students = $temp[0][0];	              
$search_amount = $temp[0][1];

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;


if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "d.ChineseName";
}else $archive_namefield = "d.EnglishName";

$namefield = getNameFieldWithClassNumberByLang("c.");

$sql="
	SELECT 
		IF((c.UserID IS NULL AND d.UserID IS NOT NULL),CONCAT('<font color=black>*</font><i>',$archive_namefield,'</i>'), $namefield),
		b.Name,".$lpayment->getWebDisplayAmountFormatDB("a.SubsidyAmount").",a.SubsidyPICAdmin,a.DateModified 
	FROM 
		PAYMENT_PAYMENT_ITEMSTUDENT AS a LEFT OUTER JOIN
		PAYMENT_PAYMENT_ITEM AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
		INTRANET_USER AS c ON (a.StudentID = c.UserID) LEFT OUTER JOIN
		INTRANET_ARCHIVE_USER AS d ON (a.StudentID = d.UserID)
	WHERE a.SubsidyUnitID ='$UnitID'  AND 
              (
              a.SubsidyAmount LIKE '%$keyword%' OR
              a.SubsidyPICAdmin LIKE '%$keyword%' OR
              a.DateModified LIKE '%$keyword%' OR
              c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.EnglishName LIKE '%$keyword%' OR
               d.ChineseName LIKE '%$keyword%' OR
               d.ClassName LIKE '%$keyword%' OR
               d.ClassNumber LIKE '%$keyword%' OR
               b.Name LIKE '%$keyword%'

              )	
	";            
# TABLE INFO
$field_array = array("$namefield","b.Name","a.SubsidyAmount","a.SubsidyPICAdmin","a.DateModified");
$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();

$temp = $li->returnArray($sql,5);


$x="<table width=90% border=0  cellpadding=2 cellspacing=0 align='center'  class='$css_table'>";
$x.="<Tr class='$css_table_title'>";
$x.= "<td width=1 class='$css_table_title'>#</td>\n";
$x.= "<td width=25% class='$css_table_title'>$i_UserStudentName</td>\n";
$x.= "<td width=25% class='$css_table_title'>$i_Payment_Field_PaymentItem</td>\n";
$x.= "<td width=15% class='$css_table_title'>$i_Payment_Subsidy_Amount</td>\n";
$x.= "<td width=15% class='$css_table_title'>$i_Payment_Subsidy_Unit_Admin</td>\n";
$x.= "<td width=20% class='$css_table_title'>$i_Payment_Subsidy_Unit_UpdateTime</td>\n";
$x.="</tr>";
for($i=0;$i<sizeof($temp);$i++){
	list($student_name,$item_name,$sub_amount,$last_modified,$last_modified_date)=$temp[$i];
	//$css =$i%2==0?"tableContent":"tableContent2";
	$css =$i%2==0?$css_table_content:$css_table_content."2";	
	$x.="<tr class='$css'>";
	$x.="<td class='$css'>".($i+1)."</td>";
	$x.="<td class='$css'>$student_name</td><td class='$css'>$item_name</td><td class='$css'>$sub_amount</td><td class='$css'>$last_modified</td><td class='$css'>$last_modified_date</td>";
	$x.="</tr>";
}
if(sizeof($temp)<=0){
        $x.="<tr><td colspan=6 align=center  class='$css_table_content' height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}


$x.="</table>";

$display=$x;

include_once("../../../../templates/fileheader.php");
//echo displayNavTitle($i_Payment_Menu_Report_SchoolAccount,'');
?>
<table width=90% border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td width=20% class='<?=$css_text?>'><B><?=$i_Payment_Subsidy_Unit?> :</b> <?=$unit_name?></td></tr>
<tr><td width=20% class='<?=$css_text?>'><B><?=$i_Payment_Subsidy_Total_Amount?> :</b> <?=$unit_total_amount?></td></tr>
<tr><td width=20% class='<?=$css_text?>'><B><?=$i_Payment_Subsidy_Used_Amount?> :</b> <?=$unit_used_amount?></td></tr>
<tr><td width=20% class='<?=$css_text?>'><B><?=$i_Payment_Subsidy_Left_Amount?> :</b> <?=$unit_left_amount?></td></tr>
<tr><td width=20% class='<?=$css_text?>'><B><?=$i_Payment_Subsidy_Search_Amount?> :</b> <?=("$search_amount ($no_of_students $i_Payment_Students)")?></td></tr>
<tr><td colspan='2' class='<?=$css_text?>'><BR><?= $i_Payment_Note_StudentRemoved2 ?></td></tr>
</table>
<?=$display?>
<BR>
<table border=0 width=90% align=center>
<tr><td align=center><a href='javascript:window.print()'><img src='/images/admin/button/s_btn_print_<?=$intranet_session_language?>.gif' border=0 align='absmiddle'></a></td></tr>
</table>
<BR><BR>
<?php
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>
