<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lpayment = new libpayment();


if($UnitID=="" || sizeof($UnitID)<=0){
	header("Location: index.php");
	exit();
}

if(is_array($UnitID))
	$UnitID = $UnitID[0];

// get summary	
$sql="SELECT SUM(a.SubsidyAmount) FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a WHERE a.SubsidyUnitID ='$UnitID' ";
$temp = $lpayment->returnVector($sql);
$unit_used_amount = $temp[0];
	
	
$sql="SELECT UnitName,TotalAmount FROM PAYMENT_SUBSIDY_UNIT WHERE UnitID = '$UnitID'";
$temp = $lpayment->returnArray($sql,2);
list($unit_name,$unit_total_amount) = $temp[0];

$unit_left_amount = $unit_total_amount - $unit_used_amount;
$unit_left_amount = $unit_left_amount > 0 ?$unit_left_amount:0;
$unit_total_amount = $lpayment->getWebDisplayAmountFormat($unit_total_amount);
$unit_used_amount  = $lpayment->getWebDisplayAmountFormat($unit_used_amount);
$unit_left_amount  =  $lpayment->getWebDisplayAmountFormat($unit_left_amount);






$sql="
	SELECT 
		COUNT(*), ".$lpayment->getWebDisplayAmountFormatDB("SUM(a.SubsidyAmount)")."
	FROM 
		PAYMENT_PAYMENT_ITEMSTUDENT AS a LEFT OUTER JOIN
		PAYMENT_PAYMENT_ITEM AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
		INTRANET_USER AS c ON (a.StudentID = c.UserID) LEFT OUTER JOIN
		INTRANET_ARCHIVE_USER AS d ON (a.StudentID = d.UserID)
	WHERE a.SubsidyUnitID ='$UnitID'  AND 
              (
              a.SubsidyAmount LIKE '%$keyword%' OR
              a.SubsidyPICAdmin LIKE '%$keyword%' OR
              a.DateModified LIKE '%$keyword%' OR
              c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.EnglishName LIKE '%$keyword%' OR
               d.ChineseName LIKE '%$keyword%' OR
               d.ClassName LIKE '%$keyword%' OR
               d.ClassNumber LIKE '%$keyword%' OR
               b.Name LIKE '%$keyword%'

              )	
	";
	
$temp = $lpayment->returnArray($sql,2);
$no_of_students = $temp[0][0];	              
$search_amount = $temp[0][1];


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;


if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "d.ChineseName";
}else $archive_namefield = "d.EnglishName";

$namefield = getNameFieldWithClassNumberByLang("c.");

$sql="
	SELECT 
		IF((c.UserID IS NULL AND d.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), $namefield),
		b.Name,".$lpayment->getWebDisplayAmountFormatDB("a.SubsidyAmount").",a.SubsidyPICAdmin,a.DateModified 
	FROM 
		PAYMENT_PAYMENT_ITEMSTUDENT AS a LEFT OUTER JOIN
		PAYMENT_PAYMENT_ITEM AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
		INTRANET_USER AS c ON (a.StudentID = c.UserID) LEFT OUTER JOIN
		INTRANET_ARCHIVE_USER AS d ON (a.StudentID = d.UserID)
	WHERE a.SubsidyUnitID ='$UnitID'  AND 
              (
              a.SubsidyAmount LIKE '%$keyword%' OR
              a.SubsidyPICAdmin LIKE '%$keyword%' OR
              a.DateModified LIKE '%$keyword%' OR
              c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.EnglishName LIKE '%$keyword%' OR
               d.ChineseName LIKE '%$keyword%' OR
               d.ClassName LIKE '%$keyword%' OR
               d.ClassNumber LIKE '%$keyword%' OR
               b.Name LIKE '%$keyword%'

              )	
	";

                
# TABLE INFO

$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("$namefield","b.Name","a.SubsidyAmount","a.SubsidyPICAdmin","a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $i_Payment_Field_PaymentItem)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Subsidy_Amount)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Subsidy_Unit_Admin)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_Payment_Subsidy_Unit_UpdateTime)."</td>\n";



$toolbar= "<a class=iconLink href=javascript:openPrintPage(document.form1,'list_print.php')>".printIcon()."$i_PrinterFriendlyPage</a>";
$toolbar.= "<a class=iconLink href=javascript:exportPage(document.form1,'list_export.php')>".exportIcon()."$button_export</a>\n".toolBarSpacer();


$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";


## echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentItem,'');
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Subsidy_Setting,'index.php',$unit_name,'');

echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<script language='javascript'>
function openPrintPage(obj,url){
	old_url = obj.action;
	old_target=obj.target;
	obj.action=url;
	obj.target='_blank';
	obj.submit();
	
	obj.action = old_url;
	obj.target = old_target;
}
function exportPage(obj,url){
        old_url = obj.action;
        obj.action=url;
        obj.submit();
        obj.action = old_url;

}
</script>
<form name="form1" method="get">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td colspan='2'><font color=green><u><b><?=$i_Payment_ItemSummary?></b></u></font></td></tr>
<tr><td width=20%><b><?=$i_Payment_Subsidy_Unit?> :</b> <?=$unit_name?></td></tr>
<tr><td width=20%><b><?=$i_Payment_Subsidy_Total_Amount?> :</b> <?=$unit_total_amount?></td></tr>
<tr><td width=20%><B><?=$i_Payment_Subsidy_Used_Amount?> :</b> <?=$unit_used_amount?></td></tr>
<tr><td width=20%><B><?=$i_Payment_Subsidy_Left_Amount?> :</b> <?=$unit_left_amount?></td></tr>
<tr><td width=20%><B><?=$i_Payment_Subsidy_Search_Amount?> :</b> <?=("$search_amount ($no_of_students $i_Payment_Students)")?></td></tr>
<tr><td colspan='2'><hr size=1 class="hr_sub_separator"><BR>&nbsp;</td></tr>
<tr><td colspan='2'><?= $i_Payment_Note_StudentRemoved ?></td></tr>
</table><BR>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar2", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=UnitID value='<?=$UnitID?>'>
</form>

<?
include_once("../../../../templates/adminfooter.php");


intranet_closedb();
?>
