<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();


$lpayment = new libpayment();


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;

if($status=="")
	$status=1;

if($status=="-1"){
	$conds="";
}
else if($status==0){
	$conds = " AND a.RecordStatus=0 ";
}
else if($status==1){
	$conds = " AND a.RecordStatus=1";
}



$sql="SELECT CONCAT('<a href=list.php?UnitID[]=',a.UnitID,'>',a.UnitName,'</a>'),
	".$lpayment->getWebDisplayAmountFormatDB("SUM(b.SubsidyAmount)")." AS abc,
	IF(a.TotalAmount IS NULL,".$lpayment->getWebDisplayAmountFormatDB("0").",".$lpayment->getWebDisplayAmountFormatDB("a.TotalAmount")."),
	CONCAT('<input type=checkbox name=UnitID[] value=', a.UnitID ,'>')
	FROM PAYMENT_SUBSIDY_UNIT AS a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b ON (a.UnitID = b.SubsidyUnitID)
	WHERE (a.UnitName LIKE '%$keyword%' OR
               a.TotalAmount LIKE '%$keyword%' OR
               a.UnitCode LIKE '%$keyword%'
    )
    $conds
    GROUP BY a.UnitID
";

              
                
# TABLE INFO

$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.UnitName","abc","a.TotalAmount");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=40% class=tableTitle>".$li->column($pos++, $i_Payment_Subsidy_Unit)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_Payment_Subsidy_Used_Amount)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_Payment_Subsidy_Total_Amount)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("UnitID[]")."</td>\n";

$select_status = "<SELECT name=status onChange=this.form.submit()>\n";
$select_status .= "<OPTION value='-1' ".($status=="-1"? "SELECTED":"").">$i_status_all</OPTION>\n";
$select_status .= "<OPTION value=1 ".($status==1? "SELECTED":"").">$i_general_active</OPTION>\n";
$select_status .= "<OPTION value=0 ".($status==0? "SELECTED":"").">$i_general_inactive</OPTION>\n";
$select_status .= "</SELECT>\n";



$toolbar = "<a class=iconLink href=javascript:checkNew('new.php')>".newIcon()."$button_new</a>";
$toolbar2= "<a class=iconLink href=javascript:openPrintPage(document.form1,'print.php')>".printIcon()."$i_PrinterFriendlyPage</a>";
$toolbar2.= "<a class=iconLink href=javascript:exportPage(document.form1,'export.php')>".exportIcon()."$button_export</a>\n".toolBarSpacer();


$functionbar .="$select_status&nbsp;";
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'UnitID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
//$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'UnitID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";


## echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentItem,'');
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Subsidy_Setting,'');

echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<script language='javascript'>
function openPrintPage(obj,url){
	old_url = obj.action;
	old_target=obj.target;
	obj.action=url;
	obj.target='_blank';
	obj.submit();
	
	obj.action = old_url;
	obj.target = old_target;
}
function exportPage(obj,url){
        old_url = obj.action;
        obj.action=url;
        obj.submit();
        obj.action = old_url;

}
</script>
<form name="form1" method="get">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar2", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?
include_once("../../../../templates/adminfooter.php");

# remove the temp table
$temp_table_sql = "DROP TABLE TEMP_PAYMENT_ITEM_SUMMARY";
$lpayment->db_db_query($temp_table_sql);

intranet_closedb();
?>
