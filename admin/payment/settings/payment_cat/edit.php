<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$CatID = (is_array($CatID)? $CatID[0]: $CatID);

$lpayment = new libpayment();
$info = $lpayment->returnPaymentCatInfo($CatID);
list($name,$displayOrder,$desp) = $info;

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentCategory,'index.php',$button_edit,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);

?>
<form name=form1 action="edit_update.php" method=POST>
<table width=90% border=0 align=center>
<tr><td><?=$i_Payment_Field_PaymentCategory?></td><td><input type=text name=CatName size=50 maxlength=255 value='<?=$name?>'></td></tr>
<tr><td><?=$i_Payment_Field_DisplayOrder?></td><td><input type=text name=DisplayOrder size=10 maxlength=10 value="<?=$displayOrder?>"></td></tr>
<tr><td><?=$i_general_description?></td><td><TEXTAREA rows=5 cols=50 name=Description><?=$desp?></TEXTAREA></td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
<input type=hidden name=CatID value="<?=$CatID?>">
</form>
<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
