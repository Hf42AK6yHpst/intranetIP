<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

$li = new libpayment();

$pps_charge_from_file = trim(get_file_content("$intranet_root/file/pps_charge_deduction.txt"));

$temp =  explode("\n",$pps_charge_from_file);
$enable = $temp[0];
$IEPS = $temp[1];
$COUNTER = $temp[2];


$interval  = 0.1 ;

### IEPS ###
$select_ieps ="<SELECT name='IEPS' ".($enable==1?"":" DISABLED ").">";
$i=2;

while($i<=4 || abs($i-4)< 0.001){
	$diff = abs($IEPS - $i);
	$selected = $IEPS!="" && $diff < 0.001 ? " SELECTED ":"";
	$select_ieps.="<OPTION VALUE='$i' $selected>".$li->getWebDisplayAmountFormat($i)."</OPTION>";
	$i+=$interval;

}
$select_ieps.="</SELECT>";

### COUNTER ####
$select_counter ="<SELECT name='COUNTER' ".($enable==1?"":" DISABLED ").">";
$j=3;
while($j<=6  || abs($i-6)< 0.001){
	$diff2 = abs($COUNTER - $j);
	$selected2 = $COUNTER!="" && $diff2< 0.001 ? " SELECTED ":"";
	$select_counter.="<OPTION VALUE='$j' $selected2>".$li->getWebDisplayAmountFormat($j)."</OPTION>";
	$j+=$interval;
}
$select_counter.="</SELECT>";

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PPS_Setting,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>
<script language='javascript'>
function setCharge(flag){
	ieps = document.getElementsByName('IEPS');
	counter = document.getElementsByName('COUNTER');
	
	if(ieps==null || counter == null) {
		return;
	}
	ieps[0].disabled = !flag;
	counter[0].disabled = !flag;
}
function resetForm(){
	f = document.form1;
	e = document.form1.ChargeDeduction;
	if(f ==null || e==null) return;
	f.reset();
	setCharge(e.checked);
}
</script>
<form name=form1 method=POST action=update.php>
<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td colspan='2'><b><u><?php echo $i_Payment_PPS_Charge; ?>:</u></b></td><td><input valign=absmiddle type=checkbox name=ChargeDeduction value=1 <?=($enable==1?"CHECKED":"")?> onClick='setCharge(this.checked)'></td></tr>
<tr><td width='10'></td><td><?=$i_Payment_Import_PPS_FileType_Normal?>:</td><td><?=$select_ieps?></td></tr>
<tr><td></td><td><?=$i_Payment_Import_PPS_FileType_CounterBill?>:</td><td><?=$select_counter?></td></tr>
</table>
<br>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit()?>
&nbsp;<a href='javascript:resetForm()'><img src='<?=$image_path?>/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif' border='0'></a>
 <a href="../index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>


</form>
<?
include_once("../../../../templates/adminfooter.php");
?>