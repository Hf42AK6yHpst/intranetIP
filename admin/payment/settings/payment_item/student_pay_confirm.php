<?
// page edit by: Kenneth Chung
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libpayment.php");
intranet_opendb();

@set_time_limit(0);

if(is_array($PaymentID))
	$list = implode(",",$PaymentID);
	
if($list!=""){

$lpayment = new libpayment();
$itemName = $lpayment->returnPaymentItemName($ItemID);


$sql = "LOCK TABLES
             PAYMENT_PAYMENT_ITEMSTUDENT AS a READ,
             PAYMENT_ACCOUNT AS c READ,
             PAYMENT_PAYMENT_ITEMSTUDENT WRITE,
             PAYMENT_ACCOUNT WRITE,
             PAYMENT_OVERALL_TRANSACTION_LOG WRITE
             ";
$lpayment->db_db_query($sql);
$sql = "SELECT a.StudentID, a.Amount, c.Balance,a.PaymentID
               FROM PAYMENT_PAYMENT_ITEMSTUDENT as a
                    LEFT OUTER JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID
               WHERE a.RecordStatus = 0 AND a.PaymentID IN ($list)";
$result = $lpayment->returnArray($sql,4);

$result2 = array();

$allEnough = true;
for ($i=0; $i<sizeof($result); $i++)
{
     list ($StudentID, $amount, $balance, $paymentID) = $result[$i];
     $balanceAfter = $balance - $amount;
     
     if (abs($balanceAfter) < 0.001)    # Smaller than 0.1 cent
     {
         $balanceAfter = 0;
     }          
     if ($balanceAfter < 0)
     {
         $allEnough = false;
         //break;
         continue;
     }else{
	     $result2[] = $result[$i];
     }
}
//if ($allEnough)
//{
    # Proceed payment here
    $itemName = addslashes($itemName);

    for ($i=0; $i<sizeof($result2); $i++)
    {
         list ($StudentID, $amount, $balance, $paymentID) = $result[$i];
         $balanceAfter = $balance - $amount;

         # 1. Set Payment Record Status to PAID
         $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET RecordStatus = 1,PaidTime = NOW(), ProcessingTerminalUser=NULL,ProcessingTerminalIP=NULL, ProcessingAdminUser='$PHP_AUTH_USER',DateModified=NOW() WHERE PaymentID = $paymentID";
         $lpayment->db_db_query($sql);
         
         # 2. Deduct Balance in PAYMENT_ACCOUNT
         /*
         $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance - $amount,
                                        LastUpdateByAdmin = '$PHP_AUTH_USER',
                                        LastUpdateByTerminal = NULL, LastUpdated = NOW() WHERE StudentID = $StudentID";
         */
         $balance_to_db = round($balanceAfter,2);
         
          $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = '$balance_to_db',
                                LastUpdateByAdmin = '$PHP_AUTH_USER',
                                LastUpdateByTerminal = NULL, LastUpdated = NOW() WHERE StudentID = $StudentID";
                       
         $lpayment->db_db_query($sql);
         
         # 3. Insert Transaction Record
         $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
                 (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details)
                 VALUES
                 ($StudentID, 2,'$amount','$paymentID','$balance_to_db',NOW(),'$itemName')";
         $lpayment->db_db_query($sql);
         $logID = $lpayment->db_insert_id();
         $sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RefCode = CONCAT('PAY',LogID) WHERE LogID = $logID";
         $lpayment->db_db_query($sql);
    }
    $pay_msg = 1;
//}
//else
//{
//   $pay_msg = 2;
//}

$sql = "UNLOCK TABLES";
$lpayment->db_db_query($sql);
}
header ("Location: list.php?ItemID=$ItemID&ClassName=$ClassName&pageNo=$pageNo&order=$order&field=$field&pay=$pay_msg");
intranet_closedb();
?>
