<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$limport = new libimporttext();

$li = new libdb();
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;


$lpayment = new libpayment();
$itemName = $lpayment->returnPaymentItemName($ItemID);


$format = $format==""?1:$format;

$file_format1 = array("Class Name","Class Number","Amount");
$file_format2 = array("User Login","Amount");


if($format==1)
        $file_format = $file_format1;
if($format==2)
        $file_format = $file_format2;

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php?ItemID=$ItemID&failed=2");
        exit();
}  else {
        //$ext = strtoupper($lo->file_ext($filename));
        //if($ext == ".CSV")
        if($limport->CHECK_FILE_EXT($filename))
        {
           # read file into array
           # return 0 if fail, return csv array if success
           $data = $limport->GET_IMPORT_TXT($filepath);
			$col_name = array_shift($data);		# drop the title bar
		 
           #Check file format
           $format_wrong = false;

           for ($i=0; $i<sizeof($file_format); $i++)
           {
                if ($col_name[$i]!=$file_format[$i])
                {
                    $format_wrong = true;
                    break;
                }
           }
           if ($format_wrong)
           {
               header ("Location: import.php?ItemID=$ItemID&failed=2");
               exit();
           }
			
        # Get Existing non-paid StudentID
        $sql = "SELECT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = $ItemID AND RecordStatus = 0";
        $unpaid = $li->returnVector($sql);

        # Get Existing paid StudentID
        $sql = "SELECT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = $ItemID AND RecordStatus = 1";
        $paid = $li->returnVector($sql);
        
        $duplicated_list = array();
                   
                 # build student array
               $t_sql ="SELECT UserID,UserLogin,ClassName,ClassNumber FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN(0,1,2)";
                  $t_result = $li->returnArray($t_sql,4);
                  for($i=0;$i<sizeof($t_result);$i++){
                          list($u_id,$u_login,$u_class,$u_classnum) = $t_result[$i];
                          if($format==1){
                                  $students[$u_class][$u_classnum]['uid']=$u_id;
                                  $students[$u_class][$u_classnum]['login']=$u_login;

                       }
                          else if($format==2){
                                  $students[$u_login]['uid'] = $u_id;
                                  $students[$u_login]['class'] = $u_class;
                                  $students[$u_login]['classnumber']=$u_classnum;

                       }
               }
               
        // checking of duplicated entries
        for($i=0;$i<sizeof($data);$i++){
	        if(trim(implode("",$data[$i]))=="") continue;
	        if($format==1){
		        list($class,$classnum,$amount)=$data[$i];
		        $class = trim($class);
		        $classnum = trim($classnum);
		        $duplicated_list[$class.":".$classnum]+=1+0;
		    }
		    else if($format ==2){
			    list($user_login,$amount) = $data[$i];
			    $duplicated_list[$user_login]+=1+0;
			}
	    }       
   
               
	$sql_data = array();
	$action_list = array();
           $error1 = false; // No matched Student
		   $error2 = false; // Invalid Amount
		   $error3 = false; // Duplicated entries
		   
           # Update TEMP_CASH_DEPOSIT
           $values = "";
           $delim = "";

           for ($i=0; $i<sizeof($data); $i++)
           {

				$warning_msg = array();
                # check for empty line
                    $test = trim(implode("",$data[$i]));
                    if($test=="") continue;

                    if($format==1){
                        list($class, $classnum, $amount) = $data[$i];
                           $class = trim($class);
                        $classnum = trim($classnum);
                        if($students[$class][$classnum]['uid']==""){ # no matched student
                            $error1 = true;
                            //$warning1="<font color=red>*</font>";
                            //$warning1 = $i_Payment_Import_NoMatch_Entry;
                            $warning_msg[] = $i_Payment_Import_NoMatch_Entry;

                        }
                        
                        $target_user_id = $students[$class][$classnum]['uid'];
                        
                        if(!is_numeric($amount) || $amount<0){ # invalid amount
	                        $error2 = true;
	                        //$warning2="<font color=red>**</font>";
	                        $warning_msg[]=$i_Payment_Import_InvalidAmount;
	                    }
	                    if($duplicated_list[$class.":".$classnum]>1){
		                    $error3 = true;
		                    $warning_msg[]=$i_Payment_Import_DuplicatedStudent;
		                }
	                    
	                    //if($error2 || $error1){
		                    $str_amount = $error2?"$".$amount:$lpayment->getWebDisplayAmountFormat($amount);
                    		//$warning_msg = "<font color=red>".implode("<BR>",$warning_msg)."</font>&nbsp;";
                    		$warning_message = "<table border=0>";
                    		for($x=0;$x<sizeof($warning_msg);$x++){
	                    		$warning_message.="<tr><td><font color=red>-</font></td><td><font color=red>".$warning_msg[$x]."</font></td></tr>";
	                    	}
	                    	if(sizeof($warning_msg)<=0)
	                    		$warning_message.="<tr><td colspan='2'>&nbsp;</td></tr>";	                    	
	                    	$warning_message.="</table>";
                        	//$error_entries[] = array("&nbsp;&nbsp;<font color=red><i>$warning1</i></font>","<i>$class</i>","<i>$classnum</i>","","$str_amount $warning2");
                        	$error_entries[] = array("&nbsp;&nbsp;","<i>$class</i>","<i>$classnum</i>","",$str_amount,$warning_message);

                        //}


                }else if($format==2){
                           list($user_login, $amount) = $data[$i];
                              if(!is_numeric($amount) || $amount<0){ # invalid amount
                                       $error2 = true;
	                        			//$warning2="<font color=red>**</font>";
                            			$warning_msg[]=$i_Payment_Import_InvalidAmount;
	                              
	                          }

                          if($students[$user_login]['uid']==""){ # no matched student
                                  $error1 = true;
            						//$warning1="<font color=red>*</font>";
            						$warning_msg[]=$i_Payment_Import_NoMatch_Entry2;
                          }

                        else if($user_login!=""){
                                   $class = $students[$user_login]['class'];
                                   $classnum = $students[$user_login]['classnumber'];
                           }
                           $target_user_id = $students[$user_login]['uid'];
                       if($duplicated_list[$user_login]>1){
		                    $error3 = true;
		                    $warning_msg[]=$i_Payment_Import_DuplicatedStudent;
		                }
   	                    //if($error2 || $error1){
                   		     $str_amount = $error2?"$".$amount:$lpayment->getWebDisplayAmountFormat($amount);
                    					//$warning_msg = "<font color=red>".implode("<BR>",$warning_msg)."</font>&nbsp;";
                  			$warning_message = "<table border=0>";
                    		for($x=0;$x<sizeof($warning_msg);$x++){
	                    		$warning_message.="<tr><td><font color=red>-</font></td><td><font color=red>".$warning_msg[$x]."</font></td></tr>";
	                    	}
	                    	if(sizeof($warning_msg)<=0)
	                    		$warning_message.="<tr><td colspan='2'>&nbsp;</td></tr>";
	                    	$warning_message.="</table>";

		                    		//$error_entries[] = array("&nbsp;&nbsp;<font color=red><i>$warning1</i></font>","","","<i>$user_login</i>","$str_amount $warning2");
                            			$error_entries[] = array("&nbsp;&nbsp;","","","<i>$user_login</i>",$str_amount,$warning_message);

                        //}

                           

                }
                
                if (!$error1 && !$error2 && !$error3 && $class != "" && $classnum != "" && $amount > 0){
	                	$sql_data[$class.":".$classnum] = "('$class','$classnum','$amount')";
	                	if($target_user_id!=""){
		                	if(in_array($target_user_id,$unpaid))
	                			$action_list[$class.":".$classnum] =$i_Payment_Menu_Settings_PaymentItem_Import_Action_Update;
	                		else if(in_array($target_user_id,$paid))
	                			$action_list[$class.":".$classnum] =$i_Payment_Menu_Settings_PaymentItem_Import_Action_NoChange;
	                		else
	                			$action_list[$class.":".$classnum] =$i_Payment_Menu_Settings_PaymentItem_Import_Action_New;

	                	}
                          //$values .= "$delim ('$class','$classnum','$amount')";
                           //$delim = ",";
                }

           }
           if(!$error1 && !$error2 && !$error3){
           				/* Created in import.php
                                $sql = "CREATE TABLE IF NOT EXISTS TEMP_PAYMENT_ITEM_IMPORT (
                                 ClassName varchar(20),
                                 ClassNumber varchar(20),
                                 Amount float
                                )";
                                $li->db_db_query($sql);
                        */
                                $values = implode(",",$sql_data);
                              $sql = "INSERT INTO TEMP_PAYMENT_ITEM_IMPORT (ClassName,ClassNumber,Amount) VALUES $values";
                   $li->db_db_query($sql);
                }
        }
        else
        {
            header ("Location: import.php?ItemID=$ItemID&failed=1");
            exit();
        }
}

$error_exists = $error1 || $error2 || $error3 ? true:false;

if(!$error_exists){
        $namefield = getNameFieldByLang("b.");

        $sql = "SELECT $namefield, a.ClassName, a.ClassNumber, b.UserLogin, a.Amount
        FROM TEMP_PAYMENT_ITEM_IMPORT as a LEFT OUTER JOIN INTRANET_USER as b
             ON a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2)
     ";
     $result = $li->returnArray($sql,5);
}
else{
         $result = $error_entries;

}

$display = "<tr>
<td class=tableTitle width=130>$i_UserStudentName</td>
<td class=tableTitle width=60>$i_UserClassName</td>
<td class=tableTitle width=60>$i_UserClassNumber</td>
<td class=tableTitle width=60>$i_UserLogin</td>
<td class=tableTitle width=80>$i_Payment_Field_Amount</td>";
if(!$error_exists)
	$display.="<td class=tableTitle width=80>$i_Payment_Menu_Settings_PaymentItem_Import_Action</td>";
else $display.="<td class=tableTitle width=80>$i_Payment_Import_PPSLink_Result_ErrorMsg_Reason</td>";
$display.="</tr>\n";

for ($i=0; $i<sizeof($result); $i++)
{
     list($name,$class,$classnum,$login,$amount,$reason) = $result[$i];
     $css = ($i%2? "":"2");
     $str_amount = $error_exists? $amount:$lpayment->getWebDisplayAmountFormat($amount);
     if(!$error_exists)
     	$reason = "<font color=green>".$action_list[$class.":".$classnum]."</font>";
     $display .= "<tr class=tableContent$css><td>$name&nbsp;</td><td>$class&nbsp;</td><td>".$classnum."&nbsp;</td><td>$login&nbsp;</td><td>$str_amount</td><Td>$reason</td>";
     	$display.="</tr>\n";
}
include_once("../../../../templates/adminheader_setting.php");

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$itemName,'javascript:history.back()',$button_import,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);

?>

<form name="form1" method="GET" action="import_update.php">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center"><tr><td>
<?php 
if($error_exists){
	echo "<Table width=560 border=0>";
	echo "<tr><td colspan='2'>$i_Payment_Import_Error</td></tr>";
	/*
	if($error1)
		echo "<tr><td><font color=red>*</font>".($format==1?$i_Payment_Import_NoMatch_Entry:$i_Payment_Import_NoMatch_Entry2)."</td></tr>";
	//echo ($error1?($format==1?$i_Payment_Import_NoMatch_Entry:$i_Payment_Import_NoMatch_Entry2):"");
	//echo "<BR>";
	if($error2)
		echo "<tr><td><font color=red>**</font>$i_Payment_Import_InvalidAmount</td></tr>";
	*/
	echo "</table>";
}else{
	echo $i_Payment_Import_Confirm;
}
?>
<br><Br>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<?=$display?>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<table width=560 cellspacing=0 cellpadding=0 cellspacing=0 cellpadding=10 align=center>
<tr><td align='right'><hr size=1></td></tr>
<tr><td align="right">
        <?php if(!$error_exists){?>
<input type="image" alt="<?=$button_confirm?>" src="/images/admin/button/s_btn_confirm_<?=$intranet_session_language?>.gif" border='0'>
<?php } ?>
<a href='import.php?ItemID=<?=$ItemID?>&clear=1'><img alt="<?=$button_cancel?>" border=0 src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=confirm value=1>
<input type=hidden name=ItemID value="<?=$ItemID?>">
</form>

<?
intranet_closedb();
include_once("../../../../templates/adminfooter.php");
?>
