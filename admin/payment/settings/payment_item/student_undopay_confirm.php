<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$list = implode(",",$PaymentID);
$lpayment = new libpayment();
$itemName = $lpayment->returnPaymentItemName($ItemID);

$itemName = addslashes($itemName);

$sql = "LOCK TABLES
             PAYMENT_PAYMENT_ITEMSTUDENT AS a READ,
             PAYMENT_ACCOUNT AS c READ,
             PAYMENT_PAYMENT_ITEMSTUDENT WRITE,
             PAYMENT_ACCOUNT WRITE,
             PAYMENT_OVERALL_TRANSACTION_LOG WRITE
             ";
$lpayment->db_db_query($sql);
$sql = "SELECT a.StudentID, a.Amount, c.Balance,a.PaymentID
               FROM PAYMENT_PAYMENT_ITEMSTUDENT as a
                    LEFT OUTER JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID
               WHERE a.RecordStatus = 1 AND a.PaymentID IN ($list)";
$result = $lpayment->returnArray($sql,4);

# Proceed undo here

for ($i=0; $i<sizeof($result); $i++)
{
     list ($StudentID, $amount, $balance, $paymentID) = $result[$i];
     $balanceAfter = $balance + $amount;
     
     # 1. Set Payment Record Status to UNPAID
     $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET RecordStatus = 0,PaidTime = NULL, ProcessingTerminalUser=NULL,ProcessingTerminalIP=NULL, ProcessingAdminUser='$PHP_AUTH_USER',DateModified=NOW()  WHERE PaymentID = $paymentID";
     $lpayment->db_db_query($sql);

     # 2. Update Account Balance
     /*
     $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount,
                    LastUpdateByAdmin = '$PHP_AUTH_USER',
                    LastUpdateByTerminal = NULL, LastUpdated = NOW() WHERE StudentID = $StudentID";
     */
      $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = '$balanceAfter',
                LastUpdateByAdmin = '$PHP_AUTH_USER',
                LastUpdateByTerminal = NULL, LastUpdated = NOW() WHERE StudentID = $StudentID";

     $lpayment->db_db_query($sql);
     # 3. Insert Transaction Record

     $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
             (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details)
             VALUES
             ($StudentID, 6,'$amount','$paymentID','$balanceAfter',NOW(),'$i_Payment_action_cancel_payment $itemName')";
     $lpayment->db_db_query($sql);
     $logID = $lpayment->db_insert_id();
     $sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RefCode = CONCAT('PAY',LogID) WHERE LogID = $logID";
     $lpayment->db_db_query($sql);

}

$pay_msg = 3;

$sql = "UNLOCK TABLES";
$lpayment->db_db_query($sql);

header ("Location: list.php?ItemID=$ItemID&ClassName=$ClassName&pageNo=$pageNo&order=$order&field=$field&pay=$pay_msg");
intranet_closedb();
?>
