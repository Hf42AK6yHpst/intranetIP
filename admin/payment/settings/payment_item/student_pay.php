<?php
// page edit by: Kenneth Chung
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lpayment = new libpayment();

$BulkPayment = $_REQUEST['BulkPayment'];
$ItemID = $_REQUEST['ItemID'];

# Get List
$list = implode(",",$PaymentID);
$namefield = getNameFieldByLang("b.");
$sql = "SELECT a.PaymentID, $namefield, b.ClassName, b.ClassNumber, a.Amount, c.Balance
               FROM PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus  IN (0,1,2)
                    LEFT OUTER JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID
               WHERE a.RecordStatus = 0 AND a.PaymentID IN ($list) AND b.UserID IS NOT NULL
               ORDER BY b.ClassName, b.ClassNumber, b.EnglishName";
$result = $lpayment->returnArray($sql,6);
if (sizeof($result)==0)
{
    header("Location: list.php?ItemID=$ItemID");
    exit();
}
$allEnough = true;
$x = "";
for ($i=0; $i<sizeof($result); $i++)
{
     list ($PaymentID, $name, $class, $classnum, $amount, $balance) = $result[$i];
     $balanceAfter = $balance - $amount;
     if (abs($balanceAfter) < 0.001)    # Smaller than 0.1 cent
     {
         $balanceAfter = 0;
     }
     if ($balanceAfter < 0)
     {
         $allEnough = false;
         $balanceAfter = $lpayment->getWebDisplayAmountFormat($balanceAfter);
         $balanceAfter = "<font color=red>$balanceAfter</font>";
     }
     else
     {
         $balanceAfter = $lpayment->getWebDisplayAmountFormat($balanceAfter);
         $x .= "<input type=hidden name=PaymentID[] value='$PaymentID'>\n";
     }

     $css = ($i%2? "":"2");
     $x .= "<tr class=tableContent$css>\n";
     $x .= "<td>$name</td>\n";
     $x .= "<td>$class</td>\n";
     $x .= "<td>$classnum</td>\n";
     $x .= "<td>".$lpayment->getWebDisplayAmountFormat($amount)."</td>\n";
     $x .= "<td>".$lpayment->getWebDisplayAmountFormat($balance)."</td>\n";
     $x .= "<td>$balanceAfter</td>\n";
     $x .= "</tr>\n";
}

$itemName = $lpayment->returnPaymentItemName($ItemID);
include_once("../../../../templates/adminheader_setting.php");

?>
<? //displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$itemName,'javascript:history.back()',$i_Payment_action_batchpay,'') 
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$itemName,'javascript:history.back()',$i_Payment_action_batchpay,'');

?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>
<form name="form1" action="student_pay_confirm.php" method=POST>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<?=$i_Payment_PaymentItem_Pay_Warning?>
</td></tr>
</table><BR>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
<tr><td>
<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>
<tr>
<td class=tableTitle><?=$i_UserStudentName?></td>
<td class=tableTitle><?=$i_UserClassName?></td>
<td class=tableTitle><?=$i_UserClassNumber?></td>
<td class=tableTitle><?=$i_Payment_Field_Amount?></td>
<td class=tableTitle><?=$i_Payment_Field_CurrentBalance?></td>
<td class=tableTitle><?=$i_Payment_Field_BalanceAfterPay?></td>
</tr>
<?=$x?>
</table>
</td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?php 
	// if ($allEnough) { 
?>
<input type="image" src="/images/admin/button/s_btn_proceed_pay_<?=$intranet_session_language?>.gif" border='0' alt='<?=$i_Payment_action_proceed_payment?>'>
<?php
	// }  
if ($BulkPayment === "1") {
?>
	<a href="list.php?ItemID=<?=$ItemID?>">
<?
}
else {
?>
	<a href="javascript:history.back()">
<?
}
?>
<img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=ClassName value="<?=$ClassName?>">
<input type=hidden name=ItemID value="<?=$ItemID?>">
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
