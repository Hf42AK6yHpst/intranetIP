<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();

$lidb = new libdb();
$lclass = new libclass();
$lpayment = new libpayment();

#student - Array contains the students selected
$student_list = implode(",", $student);


### Step 1 - Double Check The RecordType = 2 AND RecordStatus IN (0,1,2) ### 
$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) AND UserID IN ($student_list)";

$temp_passed_student = $lclass->returnVector($sql);

if (sizeof($temp_passed_student) == sizeof($student))
{
	$final_list = implode(",", $student);
	
	### Step 2 - Double Check PAYMENT_PAYMENT_ITEMSTUDENT ###
	$sql = "SELECT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = $ItemID AND StudentID IN ($final_list)";
	
	$checking = $lclass->returnVector($sql);
	
	if (sizeof($checking) == 0)
	{		
		for ($i=0; $i<sizeof($student); $i++)
		{
			$sql = " 
					INSERT INTO 
								PAYMENT_PAYMENT_ITEMSTUDENT 
								(ItemID, StudentID, Amount, RecordType, RecordStatus, ProcessingTerminalUser, ProcessingAdminUser, ProcessingTerminalIP, DateInput, DateModified) 
					VALUES 
								($ItemID, $student[$i], $amount, 0, 0, NULL, '$PHP_AUTH_USER', NULL, NOW(), NOW())
					";
			
			$lidb->db_db_query($sql);
			$x = 1;
		}
	}
	else
	{
		$x = 12;
	}
}
else
{
	$x = 12;
}


intranet_closedb();
header("Location: list.php?ItemID=$ItemID&msg=$x");

?>