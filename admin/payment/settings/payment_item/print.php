<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");

intranet_opendb();

$lpayment = new libpayment();

//$itemStatus = $lpayment->returnPaymentItemStatus($ItemID);
//$itemName = $lpayment->returnPaymentItemName($ItemID);
$sql="SELECT a.Name,b.Name,a.DefaultAmount,a.StartDate,a.EndDate,a.RecordStatus FROM PAYMENT_PAYMENT_ITEM as a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $lpayment->returnArray($sql,6);
list($itemName,$catName,$defaultAmount,$startDate,$endDate,$itemStatus)=$temp[0];
$defaultAmount = $lpayment->getWebDisplayAmountFormat($defaultAmount);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;


$student_status_cond="";
if($StudentStatus==1){ ## Removed Student
	$student_status_cond =" AND c.UserID IS NOT NULL ";
}else if($StudentStatus==2){ ## Not Removed Student
	$student_status_cond = " AND b.UserID IS NOT NULL ";
}
else{ ## All Student
	$student_status_cond ="";
}

$conds = "";
if ($RecordStatus != "")
{
    $conds .= " AND a.RecordStatus = $RecordStatus";
}
if ($ClassName != "")
{
    $conds .= " AND (b.Classname = '$ClassName' OR c.Classname='$ClassName')";
}
$namefield = getNameFieldWithClassNumberByLang("b.");

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "c.ChineseName";
}else $archive_namefield = "c.EnglishName";

$last_updated_field="IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,''))";



$sql  = "SELECT
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=black>*</font><i>',$archive_namefield,'</i>'), $namefield),
                ".$lpayment->getWebDisplayAmountFormatDB("a.Amount")." AS Amount,
                IF(a.SubsidyAmount IS NULL,".$lpayment->getWebDisplayAmountFormatDB("0").",".$lpayment->getWebDisplayAmountFormatDB("a.SubsidyAmount").") AS SubsidyAmount,
                IF(d.UnitName IS NULL,'&nbsp;',d.UnitName),
               IF(a.RecordStatus = 1,'$i_Payment_PaymentStatus_Paid','$i_Payment_PaymentStatus_Unpaid'),
			   IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,
			   		IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,'&nbsp;')),
               IF(a.PaidTime IS NULL,'&nbsp;',DATE_FORMAT(a.PaidTime,'%Y-%m-%d %H:%i:%s'))
         FROM
             PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT as d ON (a.SubsidyUnitID=d.UnitID)
         WHERE
              a.ItemID = $ItemID AND
              (b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.UnitName LIKE '%$keyword%'

              )
              $conds $student_status_cond
                ";



$ldb = new libdb();
# Grab Class list
$class_sql = "SELECT DISTINCT b.ClassName FROM PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
			  WHERE
					a.ItemID = $ItemID AND
					b.ClassName IS NOT NULL
			  ORDER BY b.ClassName";
$classes = $ldb->returnVector($class_sql);
$select_class = getSelectByValue($classes,"name=ClassName onChange=this.form.submit()",$ClassName,1);

# Grab Summary
$summary_sql = "SELECT a.Amount, a.SubsidyAmount,a.SubsidyUnitID,a.RecordStatus FROM
             PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER as c ON a.StudentID = c.UserID LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT as d ON (a.SubsidyUnitID=d.UnitID)
         WHERE
              a.ItemID = $ItemID AND
               (b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.UnitName LIKE '%$keyword%'

              )
              
              $conds $student_status_cond
 ";
$count_paid = 0;
$count_unpaid = 0;
$sum_paid = 0;
$sum_unpaid = 0;
$sum_subsidy_total=0;
$count_subsidy_total=0;
$temp = $ldb->returnArray($summary_sql,4);
for ($i=0; $i<sizeof($temp); $i++)
{
     list($amount,$sub_amount,$sub_unit_id,$status) = $temp[$i];
     if ($status == 1)
     {
         $count_paid++;
         $sum_paid += $amount;
     }
     else
     {
         $count_unpaid++;
         $sum_unpaid += $amount;
     }
     if($sub_unit_id>0){
       	$count_subsidy_total++;
	 }
     if($sub_amount>0){
     	$sum_subsidy_total+=$sub_amount;
     }
}
$sum_total = $sum_paid + $sum_unpaid;
$count_total = $count_paid + $count_unpaid;
$sum_paid = $lpayment->getWebDisplayAmountFormat($sum_paid);
$sum_unpaid = $lpayment->getWebDisplayAmountFormat($sum_unpaid);
$sum_total = $lpayment->getWebDisplayAmountFormat($sum_total);
$sum_subsidy_total = $lpayment->getWebDisplayAmountFormat($sum_subsidy_total);

$infobar="";
//$infobar = "<font color=green><u><b>$i_Payment_ItemSummary</b></u></font><br>\n";
//$infobar .= "<b>$i_Payment_Field_PaymentItem:</b> $itemName<br>\n";
$infobar .= "<span class='$css_title'><b>$itemName</B></span><br><BR>\n";

$infobar .= "<b>$i_Payment_Field_PaymentCategory:</b> $catName<br>\n";
$infobar .= "<b>$i_Payment_PresetPaymentItem_PaymentPeriod:</b> $startDate $i_Profile_To $endDate<br>\n";
$infobar .= "<b>$i_Payment_Field_PaymentDefaultAmount:</b> $defaultAmount<br>\n";
$infobar .= "<b>$i_Payment_ItemPaid:</b> $sum_paid ($count_paid $i_Payment_Students)<br>\n";
$infobar .= "<b>$i_Payment_ItemUnpaid:</b> $sum_unpaid ($count_unpaid $i_Payment_Students)<br>\n";
$infobar .= "<B>$i_Payment_Field_Total_Chargeable_Amount:</b> $sum_total ($count_total $i_Payment_Students)<br>";
$infobar .= "<B>$i_Payment_Subsidy_Total_Subsidy_Amount:</b> $sum_subsidy_total ($count_subsidy_total $i_Payment_Students)<br>";

# TABLE INFO
/*
$li = new libdbtable($field, $order, $pageNo);
//$li->field_array = array("b.ClassName","b.ClassNumber","b.EnglishName","a.Amount","a.SubsidyAmount","d.UnitName","a.RecordStatus","$last_updated_field","a.PaidTime");
$li->field_array = array("IF(b.UserID IS NULL,c.EnglishName,b.EnglishName)","a.Amount","a.SubsidyAmount","d.UnitName","a.RecordStatus","$last_updated_field","a.PaidTime");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;
*/


$field_array = array("IF(b.UserID IS NULL,c.EnglishName,b.EnglishName)","a.Amount","a.SubsidyAmount","d.UnitName","a.RecordStatus","$last_updated_field","a.PaidTime");
$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();


$temp = $li->returnArray($sql, 7);


$x="<table width=95% border=0 cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
$x.="<Tr>";
$x.="<td width=1 class='$css_table_title'>#</td>\n";
$x.="<td width=20% class='$css_table_title'>".$i_UserStudentName."</td>\n";
$x.="<td width=15% class='$css_table_title'>".$i_Payment_Field_Chargeable_Amount."</td>\n";
$x.="<td width=15% class='$css_table_title'>".$i_Payment_Subsidy_Amount."</td>\n";
$x.="<td width=15% class='$css_table_title'>".$i_Payment_Subsidy_Unit."</td>\n";
$x.="<td width=10% class='$css_table_title'>".$i_general_status."</td>\n";
$x.="<td width=10% class='$css_table_title'>".$i_general_last_modified_by."</td>\n";
$x.= "<td width=15% class='$css_table_title'>".$i_Payment_Field_TransactionTime."</td>\n";
$x.="</tr>";

for($i=0;$i<sizeof($temp);$i++){
	//$css = $i%2==0?"tableContent":"tableContent2";
	$css =$i%2==0?$css_table_content:$css_table_content."2";	
	list($name,$amount,$sub_amount,$sub_unit,$paid_status,$admin,$paid_time)=$temp[$i];
	$x.="<tr class='$css'>";
	$x.="<Td class='$css'>".($i+1)."</td>";
	$x.="<td class='$css'>$name&nbsp;</td>";
	$x.="<td class='$css'>$amount</td>";
	$x.="<td class='$css'>$sub_amount</td>";
	$x.="<td class='$css'>$sub_unit</td>";
	$x.="<td class='$css'>$paid_status</td>";
	$x.="<td class='$css'>$admin</td>";
	$x.="<td class='$css'>$paid_time</td>";
	$x.="</tr>";
}
if(sizeof($temp)<=0){
	$x.="<tr><td colspan=8 align=center class='$css_table_content' height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}



?>
<table width=95% border=0 cellpadding=0 cellspacing=0 align="center">
<tr class='<?=$css_text?>'><td><?= $infobar ?></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td class='<?=$css_text?>'><?= $i_Payment_Note_StudentRemoved2 ?></td></tr>
</table>

<?php echo $x; ?>


<?php
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>
