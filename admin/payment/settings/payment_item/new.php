<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lpayment = new libpayment();

if ($CatID == "")
{
$cats = $lpayment->returnPaymentCats();
$select_cat = getSelectByArray($cats,"name=CatID onChange=this.form.submit()",$CatID);

## echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentCategory,'index.php',$button_new,'');
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$button_new,'');

echo displayTag("head_payment_$intranet_session_language.gif", $msg);

?>

<form name=form1 action="" method=GET>
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo "$i_Payment_Field_PaymentCategory"; ?>:</td><td>
<?=$select_cat?>
</td></tr>
</table>
</form>

<?
}
else
{

$nextOrder = $lpayment->getPaymentItemNextDisplayOrder($CatID);

$lclass = new libclass();
$classes = $lclass->getClassList();
$currLvl = $classes[0][2];
$classList = "";

for ($i=0; $i<sizeof($classes); $i++)
{
     list($cid, $cname, $clvl) = $classes[$i];
     if ($currLvl != $clvl)
     {
         $currLvl = $clvl;
         $classList .= "<br>\n";
     }
     $classList .= "<input type=checkbox name=ClassID[] value=$cid> $cname &nbsp;&nbsp;";
}

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$button_new,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);

?>
<script language='javascript'>
function checkform(obj){
        if(obj==null) return false;
        objName = obj.ItemName;
        objStartDate = obj.StartDate;
        objEndDate = obj.EndDate;
        objDispOrder = obj.DisplayOrder;
        objPriority = obj.PayPriority;
        objAmount = obj.Amount;
        if(!check_text(objName,'<?=$i_Payment_Warning_Enter_PaymentItemName?>')) return false;
        if(!check_date(objStartDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) return false;
        if(!check_date(objEndDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) return false;
        if(!check_numeric(objDispOrder,'','<?=$i_Payment_Warning_Invalid_DisplayOrder?>')) return false;
        if(!check_numeric(objPriority,'','<?=$i_Payment_Warning_Invalid_PayPriority?>')) return false;
        //if(!check_text(objAmount,'<?=$i_Payment_Warning_Enter_DefaultAmount?>')) return false;
        if(!check_numeric(objAmount,'','<?=$i_Payment_Warning_InvalidAmount?>')) return false;
        if (compareDate(objStartDate.value, objEndDate.value) > 0)
        {
            alert ("<?php echo $i_con_msg_date_startend_wrong_alert; ?>"); return false;
        }

        return true;

}
</script>
<form name=form1 action="new_update.php" method=POST onSubmit='return checkform(this)'>
<table width=90% border=0 align=center>
<tr><td align=right nowrap><?=$i_Payment_Field_PaymentCategory?>:</td><td><?=$lpayment->returnPaymentCatName($CatID)?></td></tr>
<tr><td align=right nowrap><?=$i_Payment_Field_PaymentItem?>:</td><td><input type=text name=ItemName size=50 MAXLENGTH=255></td></tr>
<tr><td align=right nowrap><?=$i_general_startdate?>:</td><td><input type=text name=StartDate size=10 MAXLENGTH=10 value="<?=date('Y-m-d')?>"></td></tr>
<tr><td align=right nowrap><?=$i_general_enddate?>:</td><td><input type=text name=EndDate size=10 MAXLENGTH=10 value="<?=date('Y-m-d', time()+7*24*60*60)?>"></td></tr>
<tr><td align=right nowrap><?=$i_Payment_Field_DisplayOrder?>:</td><td><input type=text name=DisplayOrder size=10 maxlength=10 value="<?=$nextOrder?>"></td></tr>
<tr><td align=right nowrap><?=$i_Payment_Field_PayPriority?>:</td><td><input type=text name=PayPriority size=10 maxlength=10 value="<?=0?>">(<?=$i_Payment_Note_Priority?>)</td></tr>
<tr><td align=right nowrap><?=$i_general_description?>:</td><td><TEXTAREA rows=5 cols=50 name=Description></TEXTAREA></td></tr>
<tr><td align=right nowrap><?=$i_Payment_Field_PaymentDefaultAmount?>:</td><td><input type=text name=Amount size=10 maxlength=10></td></tr>
<tr><td align=right nowrap><?=$i_Payment_ClassInvolved?>:</td><td><?=$classList?><br>
(<?=$i_Payment_NoClass?>)
</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
<input type=hidden name=CatID value="<?=$CatID?>">
</form>
<?
}
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
