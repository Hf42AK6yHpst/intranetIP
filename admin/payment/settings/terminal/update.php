<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

write_file_content($IPList,"$intranet_root/file/payment_ip.txt");
$st_expiry = trim($st_expiry);
if ($st_expiry=="" || !is_numeric($st_expiry))
{
    $st_expiry = 60;
}
write_file_content($st_expiry,"$intranet_root/file/payment_expiry.txt");

$auth_content = "$payment_auth\n$purchase_auth";
write_file_content($auth_content,"$intranet_root/file/payment_auth.txt");

header("Location: index.php?msg=2");
?>