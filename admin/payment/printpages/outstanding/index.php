<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();
$button_select = $i_status_all;
$select_class = $lclass->getSelectClass("name=ClassName onChange=changeSelection(this.form)",$ClassName);

$format_array = array(
                      array(0,"Web"),
                      array(1,"CSV"),
                      array(2,"$i_Payment_PrintPage_PaymentLetter"));
$select_format = getSelectByArray($format_array, "name=format onChange='changeSelection(this.form)'",0,0,1);


?>
<script language='javascript'>
function changeSelection(formObj){
		if(formObj==null) return;
		objClassName = formObj.ClassName;
		objFormat = formObj.format;
        checkObj = formObj.ExcludeEmpty;
        if(objClassName ==null || objFormat==null || checkObj==null)return;
        vClass = objClassName.options[objClassName.selectedIndex].value;
   		vFormat = objFormat.options[objFormat.selectedIndex].value;
        if(vClass=='' && vFormat != 2) 
        	checkObj.disabled=false;
        else {
                checkObj.checked = false;
                checkObj.disabled=true;
        }
}
function checkform(obj)
{
         if (obj.format.value == 0 || obj.format.value==2)
         {
             obj.target = "_blank";
             if(obj.format.value==2){
	             obj.action='print_letter.php';
	         }else{
		         obj.action='print.php';
		     }
         }
         else
         {
             obj.target = "";
         }
         return true;
}
</script>
<form name="form1" method="POST" action="print.php" ONSUBMIT="return checkform(this)">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_PrintPage,'../',$i_Payment_Menu_PrintPage_Outstanding,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_UserClassName; ?>:</td><td><?=$select_class?></td></tr>
<tr><td align=right nowrap><?=$i_general_Format?>:</td><td><?=$select_format?><br>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td><input type=checkbox name=ExcludeEmpty value=1><?=$i_Payment_Exclude?><br>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td><input type=checkbox name=IncludeNonOverDue value=1><?=$i_Payment_IncludeNonOverDue?><br>&nbsp;</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>

</table>
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
