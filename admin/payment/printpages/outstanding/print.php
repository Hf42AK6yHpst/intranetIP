<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../includes/libexporttext.php");


if($format!=1)
        include_once("../../../../templates/fileheader.php");
intranet_opendb();


$lexport = new libexporttext();

$lpayment = new libpayment();

$ExcludeEmpty = (isset($_REQUEST['ExcludeEmpty']) && $_REQUEST['ExcludeEmpty'] == 1) ? 1 : 0;
$page_breaker = "<P CLASS='breakhere'>";
//$namefield = getNameFieldWithClassNumberByLang("c.");
$namefield = getNameFieldByLang2("c.");

# Get Account Balance
$student_balance = $lpayment->getBalanceInAssoc();

if($IncludeNonOverDue!=1){
	$cond = " AND b.EndDate < CURDATE() ";
}else{
	$cond = " AND b.StartDate <=CURDATE()";
}

$sql = "SELECT
                                $namefield,
                                c.ClassName,
                                c.ClassNumber,
                                b.Name,
                                a.Amount,
                                c.ClassName,
                                a.StudentID,
                                d.PPSAccountNo,
                                DATE_FORMAT(b.EndDate,'%Y-%m-%d')
                        FROM
                                PAYMENT_PAYMENT_ITEMSTUDENT AS a
                                LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM AS b ON a.ItemID = b.ItemID
                                LEFT OUTER JOIN INTRANET_USER AS c ON a.StudentID = c.UserID
                                LEFT OUTER JOIN PAYMENT_ACCOUNT as d ON c.UserID = d.StudentID
                        WHERE
                                a.RecordStatus <> 1
                                $cond ";
$sql .= (!empty($_REQUEST['ClassName'])) ? " AND c.ClassName = '".$_REQUEST['ClassName']."'" : "";
$sql .= " ORDER BY c.ClassName, c.ClassNumber";
$data = $lpayment->returnArray($sql, 9);
for($i=0; $i<sizeof($data); $i++)
{
        list($sname, $sclassname, $sclassnumber, $itemname, $amount, $classname, $sid, $s_pps_no,$due_date) = $data[$i];
        $s_pps_no = trim($s_pps_no);
        $s_pps_no = (($s_pps_no=="")?"--":$s_pps_no);
        $TotalRecord[$classname] += 1;
        //debug($TotalAmount[$sid]);
        $TotalAmount[$sid] += $amount;
        //debug($TotalAmount[$sid]);
        //exit;
        $Payment[$classname][] = array($sname, $sclassname, $sclassnumber, $itemname, $amount, $sid, $s_pps_no,$due_date);
}

if (empty($_REQUEST['ClassName']))
{
        $sql = "SELECT ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
        $classes = $lpayment->returnVector($sql);
}
else
{
        $classes = array($_REQUEST['ClassName']);
}

$display = "";

for ($i=0; $i<sizeof($classes); $i++)
{
        $classname = $classes[$i];
        $x = "";
                $csv = "";
        if ($TotalRecord[$classname] > 0 || $ExcludeEmpty == 0)
        {
                $x .= "<p><b><font size=+1>".$classname."</font></b></p>\n";
                $x .= "<table width=95% border=0 cellpadding=2 cellspacing=2>\n";
                $x .= "<tr>\n";
                        $x .= "<td>$i_UserStudentName</td>";
                        $x .= "<td>$i_UserClassName</td>";
                        $x .= "<td>$i_UserClassNumber</td>";
                        $x .= "<td>$i_Payment_Field_PaymentItem</td>";
                        $x .= "<td>$i_Payment_PrintPage_Outstanding_DueDate</td>";
                        $x .= "<td>$i_Payment_PrintPage_Outstanding_Total</td>";
                        $x .= "<td>$i_Payment_PrintPage_Outstanding_AccountBalance</td>";
                        $x .= "<td>$i_Payment_PrintPage_Outstanding_LeftAmount</td>";
                        $x .= "<td>$i_Payment_Field_PPSAccountNo</td>";
                $x .= "</tr>\n";

                $csv.="\"$classname\"\n";
                $csv.="\"$i_UserStudentName\",\"$i_UserClassName\",\"$i_UserClassNumber\",\"$i_Payment_Field_PaymentItem\",\"$i_Payment_PrintPage_Outstanding_DueDate\",\"$i_Payment_PrintPage_Outstanding_Total\",";
                $csv.="\"$i_Payment_PrintPage_Outstanding_AccountBalance\",\"$i_Payment_PrintPage_Outstanding_LeftAmount\",\"$i_Payment_Field_PPSAccountNo\"\n";

        }

        if ($TotalRecord[$classname] == 0 && $ExcludeEmpty == 0)
        {
                $x .= "<tr><td colspan=9><hr></td></tr>\n";
                $x .= "<tr><td>$i_no_record_exists_msg</td></tr>\n";

                $csv.="\"$i_no_record_exists_msg\"\n";
        }
        else
        {
                $payment_record = $Payment[$classname];
                $curr_student = "";
                for($j=0; $j<sizeof($payment_record); $j++)
                {
                        list($StudentName, $ClassName, $ClassNumber, $ItemName, $Amount, $StudentID, $s_pps_no,$due_date) = $payment_record[$j];

                        $x .= ($curr_student == $StudentID) ? "" : "<tr><td colspan=9><hr></td></tr>\n";
                        $x .= "<tr>\n";
                                $x .= "<td>";
                                                                if($curr_student == $StudentID) {
                                                                        $x.="&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                                                                        $csv.="\"\",\"\",\"\",";
                                                                }
                                                                else{
                                                                         $x.=$StudentName."</td><td>".$ClassName."</td><td>".$ClassNumber."</td>";
                                                                         $csv.="\"$StudentName\",\"$ClassName\",\"$ClassNumber\",";
                                                                }
                                       
                                $x .= "<td>".$ItemName."(".$lpayment->getWebDisplayAmountFormat($Amount).")</td>";
                                $x .= "<td nowrap>".$due_date."</td>";
                                
                                $csv.="\"".$ItemName."(".$lpayment->getExportAmountFormat($Amount).")\",";
                                $csv.="\"".$due_date."\",";
                                if ($curr_student == $StudentID)
                                {
                                    $x .= "<td>&nbsp;</td>";
                                    $csv.="\"\",";
                                }
                                else
                                {
                                    $t_balance = $student_balance[$StudentID] + 0;
                                    $t_diff = $TotalAmount[$StudentID] - $t_balance;
                                    if (abs($t_diff) < 0.001)    # Smaller than 0.1 cent
                                    {
                                        $t_diff = 0;
                                    }
                                    $str_diff = ($t_diff > 0)? "<font color=red>-".$lpayment->getWebDisplayAmountFormat($t_diff)."</font>":"--";
                                        $csv_str_diff = ($t_diff > 0)? "-".$lpayment->getExportAmountFormat($t_diff):"--";


                                    $x .= "<td>".$lpayment->getWebDisplayAmountFormat($TotalAmount[$StudentID])."</td>";
                                    $x .= "<td>".$lpayment->getWebDisplayAmountFormat($t_balance)."</td>";
                                    $x .= "<td>$str_diff</td>";
                                    $x .= "<td>$s_pps_no</td>";

                                    $csv .= "\"".$lpayment->getExportAmountFormat($TotalAmount[$StudentID])."\",";
                                    $csv .= "\"".$lpayment->getExportAmountFormat($t_balance)."\",";
                                    $csv .= "\"$csv_str_diff\",";
                                    $csv .= "\"$s_pps_no\"";
                                }

                        $x .= "</tr>\n";
                        $csv.="\n";
                        $curr_student = $StudentID;
                        //$x .= ($curr_student == $StudentID) ? "" : "<tr>\n";
                }
                //echo $x;
                //exit;
        }
        if ($TotalRecord[$classname] > 0 || $ExcludeEmpty == 0)
        {
                $x .= "</table>\n";
                $x .= $page_breaker;
                $csv.="\n\n";
        }
                if($format!=1)
                $display .= $x;
        else $display .=$csv;
}
if($format!=1)
        echo $display;
else {
	$filename = "outstanding".date('Y-m-d').".csv";
	if($g_encoding_unicode){
		$display = str_replace("\"","",$display);
		$display = str_replace(",","\t",$display);
		$display = str_replace("\n","\r\n",$display);
		$lexport->EXPORT_FILE($filename, $display);
	}
	else output2browser($display,$filename);
}
?>
<? if($format!=1){?>
<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE>
<?}?>
<?php
intranet_closedb();
if($format!=1)
        include_once("../../../../templates/filefooter.php");
?>
