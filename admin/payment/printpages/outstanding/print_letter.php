<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");

intranet_opendb();


# get school data
$school_data = split("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];
$school_org = $school_data[1];

# get payment letter header & footer
$letter_head_file = "$intranet_root/file/pay_out_letter_head.txt";
$letter_head2_file = "$intranet_root/file/pay_out_letter_head2.txt";
$letter_foot_file = "$intranet_root/file/pay_out_letter_foot.txt";
$letter_signature_option_file = "$intranet_root/file/pay_out_letter_signature_option.txt";

$letter_head = get_file_content($letter_head_file);
$letter_head2 = get_file_content($letter_head2_file);
$letter_foot = get_file_content($letter_foot_file);
$letter_signature_option = get_file_content($letter_signature_option_file);

$letter_head = nl2br(str_replace(" ","&nbsp;",stripslashes($letter_head)));
$letter_head2 = nl2br(str_replace(" ","&nbsp;",stripslashes($letter_head2)));
$letter_foot = nl2br(str_replace(" ","&nbsp;",stripslashes($letter_foot)));
$letter_signature_option = trim($letter_signature_option);



$lpayment = new libpayment();

$ExcludeEmpty = (isset($_REQUEST['ExcludeEmpty']) && $_REQUEST['ExcludeEmpty'] == 1) ? 1 : 0;
$page_breaker = "<P CLASS='breakhere'>";
$namefield = getNameFieldWithClassNumberByLang("c.");

# Get Account Balance
$student_balance = $lpayment->getBalanceInAssoc();

if($IncludeNonOverDue!=1){
	$cond = " AND b.EndDate < CURDATE() ";
}else{
	$cond = " AND b.StartDate <=CURDATE()";
}

$sql = "SELECT
                                $namefield,
                                b.Name,
                                a.Amount,
                                c.ClassName,
                                a.StudentID,
                                d.PPSAccountNo,
                                DATE_FORMAT(b.StartDate,'%Y/%m/%d'),
                                DATE_FORMAT(b.EndDate,'%Y/%m/%d')
                        FROM
                                PAYMENT_PAYMENT_ITEMSTUDENT AS a
                                LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM AS b ON a.ItemID = b.ItemID
                                LEFT OUTER JOIN INTRANET_USER AS c ON a.StudentID = c.UserID
                                LEFT OUTER JOIN PAYMENT_ACCOUNT as d ON c.UserID = d.StudentID
                        WHERE
                                a.RecordStatus <> 1
                                $cond ";
$sql .= (!empty($_REQUEST['ClassName'])) ? " AND c.ClassName = '".$_REQUEST['ClassName']."'" : "";
$sql .= " ORDER BY c.ClassName, c.ClassNumber";
$data = $lpayment->returnArray($sql, 8);
for($i=0; $i<sizeof($data); $i++)
{
        list($sname, $itemname, $amount, $classname, $sid, $s_pps_no,$start_date,$due_date) = $data[$i];
        $s_pps_no = trim($s_pps_no);
        $s_pps_no = (($s_pps_no=="")?"--":$s_pps_no);
        $TotalRecord[$classname] += 1;
        //debug($TotalAmount[$sid]);
        $TotalAmount[$sid] += $amount;
        //debug($TotalAmount[$sid]);
        //exit;
        $Payment[$classname][] = array($sname, $itemname, $amount, $sid, $s_pps_no,$start_date,$due_date);
}

if (empty($_REQUEST['ClassName']))
{
        $sql = "SELECT ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
        $classes = $lpayment->returnVector($sql);
}
else
{
        $classes = array($_REQUEST['ClassName']);
}


function printLine($num){
	for($i=0;$i<$num;$i++){
		$x.="&nbsp;";
	}
	$x = "<u>$x</u>";
	return $x;
}

# header table
$header_table = "<table width=100% border=0 cellspacing=0 cellpadding=0><tr><td>\n";
$header_table.="<br><div align=center><font size=+2>$school_name</font></div><br>";
$header_table.="<div align=center><font size=+2>$letter_head</font></div><br>";
$header_table.="</td></tr>";
$header_table.="<tr><td><div>$letter_head2</div></td></tr>";
$header_table.="</table><Br>";

# footer table
$footer_table = "<br><table width=100% border=0 cellspacing=0 cellpadding=0><tr><td>\n";
$footer_table.="<br><div align=left>$letter_foot</div><br></td></tr>";
$footer_table.="<tr><td>";
if (!$letter_signature_option)
{
$footer_table.="<table align=right border=0 cellspacing=2 cellpadding=0>";
$footer_table.="<tr><Td align=right nowrap>$i_Payment_PrintPage_PaymentLetter_ParentSign : ".printLine(40)."<br>&nbsp;</td></tr>";
$footer_table.="<tr><Td align=right nowrap>$i_Payment_PrintPage_PaymentLetter_Date : ".printLine(40)."<br></td></tr>";
$footer_table.="</table><br>";
}

$footer_table.="</td></tr></table><Br><br>";


$display = "";
$display .="<table width=100% border=0 cellspacing=0 cellpadding=0><tr><td>\n";
for ($i=0; $i<sizeof($classes); $i++){
	$classname = $classes[$i];
	if($TotalRecord[$classname] <= 0)
		continue;
    $payment_record = $Payment[$classname];
    $curr_student = "";
    
    for($j=0; $j<sizeof($payment_record); $j++){
       list($StudentName, $ItemName, $Amount, $StudentID, $s_pps_no,$start_date,$due_date) = $payment_record[$j];
       if($curr_student!=$StudentID){
		   if($curr_student!=""){
			   		# add item list table
			   		$item_table.="<tr><td align=right colspan='3'>$i_Payment_PrintPage_Outstanding_Total :&nbsp;</td><td align=right>".$lpayment->getWebDisplayAmountFormat($TotalAmount[$curr_student])."</td></tr>";
			   		$item_table.="</table><Br>";
			   		
			   		$content.=$item_table;
			   		
			   		# add amount due table		
			   		$t_balance = $student_balance[$curr_student] + 0;
                    $t_diff = $TotalAmount[$curr_student] - $t_balance;
                    if (abs($t_diff) < 0.001)    # Smaller than 0.1 cent
                    {
                        $t_diff = 0;
                    }
                    $diff_str= $t_diff>0?$lpayment->getWebDisplayAmountFormat(abs($t_diff)):"--";
				    $amount_table = "<table border=1 width=80% cellspacing=0 cellpadding=0 bordercolor=#000000>";
				    $amount_table.="<tr><Td>$i_Payment_PrintPage_Outstanding_LeftAmount:</td><td width=20%>$diff_str</td></tr>";
				    $amount_table.="</table>";
					$content .=$amount_table;

			   		# add footer table
       				$content.=$footer_table.$page_breaker;
       	   }
	       
	       $content.=$header_table; # add header table
	       
       	   # add account summary table
		   $ac_table = "<table border=1 width=100% cellspacing=0 cellpadding=0 bordercolor=#000000>";
		   $ac_table.= "<tr><Td width=20%>$i_UserStudentName:</td><td>$StudentName</td><td>$i_Payment_Field_PPSAccountNo:</td><td width=20%>$s_pps_no</td></tr>";
	   	   $ac_table.= "<tr><Td width=20%>$i_Payment_PrintPage_PaymentLetter_Date:</td><td>".date('Y/m/d')."</td><td>$i_Payment_PrintPage_Outstanding_AccountBalance:</td><td width=20%>".$lpayment->getWebDisplayAmountFormat($student_balance[$StudentID])."</td></tr>";
		   $ac_table.= "</table><br>";
		   
		   $content.=$ac_table;
		   
		   $item_table = "<table border=1 cellspacing=0 cellpadding=0 bordercolor=#000000 width=100%>";
		   $item_table .= "<tr><td>$i_Payment_PrintPage_PaymentLetter_UnpaidItem</td><td align=center width=15%>$i_Payment_PrintPage_PaymentLetter_Date ($i_From)</td><td align=center width=15%>$i_Payment_PrintPage_PaymentLetter_Date ($i_To)</td><td align=center width=15%>$i_Payment_Field_Amount</td></tr>";

	   }
	   
	   
   	   # build item list table
	   $item_table .= "<tr><td>$ItemName</td><Td align=center>$start_date</td><td align=center>$due_date</td><Td align=right>".$lpayment->getWebDisplayAmountFormat($Amount)."</td></tr>";
	   $curr_student = $StudentID;
	}
	
	if($curr_student!=""){
   		# add item list table
   		$item_table.="<tr><td align=right colspan='3'>$i_Payment_PrintPage_Outstanding_Total :&nbsp;</td><td align=right>".$lpayment->getWebDisplayAmountFormat($TotalAmount[$curr_student])."</td></tr>";
   		$item_table.="</table><Br>";
   		
   		$content.=$item_table;
   		
   		# add amount due table		
   		$t_balance = $student_balance[$curr_student] + 0;
        $t_diff = $TotalAmount[$curr_student] - $t_balance;
        if (abs($t_diff) < 0.001)    # Smaller than 0.1 cent
        {
            $t_diff = 0;
        }
        $diff_str= $t_diff>0? $lpayment->getWebDisplayAmountFormat(abs($t_diff)):"--";
	    $amount_table = "<table border=1 width=100% cellspacing=0 cellpadding=0 bordercolor=#000000>";
	    $amount_table.="<tr><Td>$i_Payment_PrintPage_Outstanding_LeftAmount:</td><td width=20%>$diff_str</td></tr>";
	    $amount_table.="</table>";
		$content .=$amount_table;

   		# add footer table
		$content.=$footer_table.$page_breaker;
   }
}




$display.=$content;
$display .="</td></tr></table>";

?>

<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
     td {vertical-align:top}
</STYLE>
<?=$display?>
<?php
intranet_closedb();
include_once("../../../../templates/filefooter.php");
?>
