<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();

$li = new libdb();

if(sizeof($record_id) > 0)
{
	for($i=0; $i<sizeof($record_id); $i++)
	{
		list($r_id) = $record_id[$i];
		$sql = "DELETE FROM PAYMENT_PRINTING_PACKAGE WHERE RecordID = $r_id";
		$li->db_db_query($sql);
	}
}

Header("Location: index.php?msg=3");
intranet_closedb();
?>