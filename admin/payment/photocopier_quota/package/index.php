<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$li = new libdb();
$lpayment = new libpayment();

$toolbar = "<a class=iconLink href=javascript:checkNew('insert.php')>".newIcon()."$button_new</a>";
$toolbar2 = "<a class=iconLink href=javascript:checkEdit(document.form1,'record_id[]','edit.php')>"."<img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$toolbar3 = "<a class=iconLink href=\"javascript:checkRemove(document.form1,'record_id[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";

$table_content = "<tr class=tableTitle><td>#</td>";
$table_content .= "<td>$i_Payment_Menu_PhotoCopier_TitleName</td>";
$table_content .= "<td>$i_Payment_Field_Quota</td>";
$table_content .= "<td>$i_Payment_Field_Amount</td>";
$table_content .= "<td><input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'record_id[]'):setChecked(0,this.form,'record_id[]')>";


$sql = "SELECT
				RecordID, NumOfQuota, ".$lpayment->getWebDisplayAmountFormatDB("Price")." AS Amount, PurchasingItemName, RecordType 
		FROM
				PAYMENT_PRINTING_PACKAGE
		ORDER BY
				NumOfQuota, Price
		";

$temp = $li->ReturnArray($sql,5);

if(sizeof($temp) > 0)
{
	for($i=0; $i<sizeof($temp); $i++)
	{
		$j++;
		list($record_id, $num_of_quota, $price, $item_name, $record_type) = $temp[$i];
		$table_content .= "<tr><td>$j</td>\n";
		$table_content .= "<td>$item_name</td>\n";
		$table_content .= "<td>$num_of_quota</td>\n";
		$table_content .= "<td>$price</td>\n";
		$table_content .= "<td><input type=checkbox name=record_id[] value=$record_id></td></tr>";
	}
}
else
{
	$table_content .= "<tr><td colspan=4 align=center>$i_no_record_exists_msg</td></tr>\n";
}

?>

<SCRIPT Language="JavaScript">
function checkEdit(obj,element,page){
        if(countChecked(obj,element)>=1) {
                obj.action=page;
                obj.submit();
        } else {
                alert(globalAlertMsg1);
        }
}
function checkRemove(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(globalAlertMsg3)){	            
                obj.action=page;                
                obj.method="POST";
                obj.submit();				             
                }
        }
}
</SCRIPT>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_PhotoCopierQuotaSetting,'../',$i_Payment_Menu_PhotoCopier_Package_Setting,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="" method=POST>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><table width=560 border=0 cellspacing=0 cellpadding=0>
<tr><td class=admin_bg_menu><?=$toolbar?></td><td class=admin_bg_menu align=right><?=$toolbar2.$toolbar3?></td></tr>
</table>
<tr><td>
<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<?=$table_content?>
</table>
<tr></td>
<tr><td>
<table width=100% border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</tr></td>
</table>
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
