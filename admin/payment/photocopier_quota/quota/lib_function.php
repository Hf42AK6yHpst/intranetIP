<?
function update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($total_quota,$recordType,$condition,&$objDB)

{
	/*	recordType	1 - Add
					2 - Reset
					3 - Quota deduction from Copier
					4 - Purchase
	*/
	$sql  = "INSERT INTO PAYMENT_PRINTING_QUOTA_CHANGE_LOG ";
	$sql .= "(UserID,QuotaChange,QuotaAfter,RecordType,DateInput,DateModified) ";
	$sql .= "select ";
	if($recordType == 2)
	{	
		$sql .= "userID,0,".$total_quota.",".$recordType.",Now(),Now() ";
	}
	else
	{
		$sql .= "userID,".$total_quota.",TotalQuota + ".$total_quota.",".$recordType.",Now(),Now() ";
	}
	$sql .= "from ";
	$sql .= "PAYMENT_PRINTING_QUOTA ";
	$sql .= $condition;
//	echo "sql [".$sql."]<br>";
	$objDB->db_db_query($sql);
}
?>
