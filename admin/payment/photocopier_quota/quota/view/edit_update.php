<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lidb = new libdb();

if(is_array($user_id))
{
	$targetUser = implode(",",$user_id);
	
	$sql = "SELECT UserID FROM PAYMENT_PRINTING_QUOTA WHERE UserID IN ($targetUser)";
	$record_exist = $lidb->returnVector($sql);
	$record_not_exist = array_diff($user_id, $record_exist);
		
	if(sizeof($record_exist) > 0)
	{
		foreach($record_exist AS $key => $UserID)
		{	
			$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = ". ${"total_quota_$UserID"} .", DateModified = NOW() WHERE UserID = $UserID";
			$result = $lidb->db_db_query($sql);
			if($result == 1)
				$x = 2;
			else
				$x = 12;
		}
	}
	if(sizeof($record_not_exist) > 0)
	{
		foreach($record_not_exist AS $key => $UserID)
		{
			$sql = "INSERT INTO PAYMENT_PRINTING_QUOTA VALUES ($UserID, ". ${"total_quota_$UserID"} .", 0, '', '', NOW(), NOW())";
			$result = $lidb->db_db_query($sql);
			if($result == 1)
				$x = 2;
			else
				$x = 12;
		}
	}
}
else
{
	$x=12;
}

header("Location: index.php?msg=".$x);
intranet_closedb();

?>