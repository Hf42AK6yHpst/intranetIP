<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");

$lidb = new libdb();
intranet_opendb();
$sql = "select 
			PaperType,  NumberOfPaper
		from 
			PAYMENT_COPIER_TRANSACTION_DETAIL 
		where 
			transactionLogID = '$transactionID'
		order by 
			PaperType
		";
$temp = $lidb->returnArray($sql,2);
//$layer_content = "<table border=\"1\" width=\"300\" bordercolorlight=\"#FBFDEA\" bordercolordark=\"#B3B692\" cellpadding=\"2\" cellspacing=\"0\" bgcolor=\"#FBFDEA\">";
$layer_content = "<table border=\"1\" width=\"300\" bordercolorlight=\"#c5e2f0\" bordercolordark=\"#6298b3\" cellpadding=\"2\" cellspacing=\"0\" bgcolor=\"#fbfdea\">";
$layer_content .= "<tr bgcolor=\"#d8edf7\"><td colspan=\"3\" align=right><input type=button value=' X ' onClick=hideMenu('ToolMenu2')></td></tr>";
$layer_content .= "<tr bgcolor=\"#b1d8eb\"><td width=1>#</td><td width=50%>$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type</td><td width=50%>$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Quota_Purchase_Quantity</td></tr>";

if (sizeof($temp)==0)
	$layer_content .= "<tr><td colspan=3 align=center>$i_SmartCard_Payment_PhotoCopier_Details_Transaction_No_Record</td></tr>";
else
{
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($paperType, $noOfPaper) = $temp[$i];
		$no = $i+1;
		$displayPaperType = "";
		switch($paperType)
		{
			case a4_bw:
				$displayPaperType = $i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_a4_bw;
				break;
			case nona4_bw:
				$displayPaperType = $i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_non_a4_bw;
				break;
			case a4_color:
				$displayPaperType = $i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_a4_color;
				break;
			case nona4_color:
				$displayPaperType = $i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_non_a4_color;
				break;
		}

		$layer_content .= "<tr><td>$no</td><td>$displayPaperType</td><td>$noOfPaper</td></tr>";
	}
}
$layer_content .= "<tr bgcolor=\"#d8edf7\"><td colspan=\"3\" align=right>&nbsp;</td></tr>";
$layer_content .= "</table>";
$response = iconv("Big5","UTF-8",$layer_content);
echo $response;
intranet_closedb();
?>