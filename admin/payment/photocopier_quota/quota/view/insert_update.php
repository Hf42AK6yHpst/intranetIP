<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libdbtable.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libpayment.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();


$lclass = new libclass();
$lidb = new libdb();

$list = implode(",", $student);

$sql = "SELECT UserID FROM PAYMENT_PRINTING_QUOTA WHERE UserID IN ($list)";
$result = $lclass->returnArray($sql, 1);

if(sizeof($result) == 0)
{
	for($i=0; $i<sizeof($student); $i++)
	{
		$sql = "INSERT INTO PAYMENT_PRINTING_QUOTA VALUES ($student[$i], $total_quota, 0, '', '', NOW(), NOW())";
		$lidb->db_db_query($sql);
	}
}
$x=1;


header("Location: index.php?msg=".$x);
intranet_closedb();
?>