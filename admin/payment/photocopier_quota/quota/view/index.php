<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

$lidb = new libdb();
$lclass = new libclass();

### User Type Selection ###
$usertype = array(
array(1,$i_identity_teachstaff),
array(2,$i_identity_student),
);
$select_userType = getSelectByArray($usertype,"name=targetUserType onChange=\"generalFormSubmitCheck(this.form)\"",$targetUserType,1);

### Teacher Selection ###
$namefield = getNameFieldWithClassNumberByLang("a.");
$sql = "SELECT a.UserID, $namefield FROM INTRANET_USER AS a WHERE a.RecordType = 1 AND a.RecordStatus=1 ORDER BY a.EnglishName";
$temp = $lidb->returnArray($sql,2);
$select_teacher = getSelectByArray($temp,"name=targetTeacher",$targetTeacher,1);

### Class & Student Selection ###
$select_class = $lclass->getSelectClass("name=targetClass onChange=\"changeClass(this.form)\"",$targetClass);
if ($targetClass != "")
{
    //$select_students = $lclass->getStudentSelectByClass($targetClass,"name=targetStudent",$targetStudent);
    $student_list = $lclass->returnStudentListByClass($targetClass);
    $select_students = getSelectByArray($student_list,"name=targetStudent",$targetStudent,1);
}


if ($keyword!="")
{
	$search_cond = "AND (a.EnglishName LIKE '%$keyword%' OR
               			 a.ChineseName LIKE '%$keyword%' OR
               			 a.ClassName LIKE '%$keyword%' OR
               			 a.ClassNumber LIKE '%$keyword%'
               			 ) ";
}
else
{
	$search_cond = " ";	
}

### Get Data From Database For All Users ###
if($targetUserType == "" && $flag == 3)
{
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT a.UserID, $namefield, b.TotalQuota, b.UsedQuota FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_PRINTING_QUOTA AS b ON (a.UserID = b.UserID) WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) $search_cond OR a.RecordType = 1 AND a.RecordStatus IN (0,1,2) $search_cond ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	$result = $lidb->returnArray($sql, 4);
}

### Get Data From Database For Teachers ###
if($targetUserType == 1 && $flag == 3)
{
	if($targetTeacher == "")
	{
		$cond = " a.RecordType = 1 AND ";
	}
	else
	{
		$cond = " a.RecordType = 1 AND a.UserID = $targetTeacher AND ";
	}
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT a.UserID, $namefield, b.TotalQuota, b.UsedQuota FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_PRINTING_QUOTA AS b ON (a.UserID = b.UserID) WHERE $cond a.RecordStatus IN (0,1,2) $search_cond ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	$result = $lidb->returnArray($sql, 4);
}

### Get Data From Database For Students ###
if($targetUserType == 2 && $flag == 3)
{
	$namefield = getNameFieldWithClassNumberByLang("a.");
	if(($targetClass != "") && ($targetStudent == ""))
	{
		$cond = " a.RecordType = 2 AND a.ClassName = '$targetClass' AND ";
		$sql = "SELECT a.UserID, $namefield, b.TotalQuota, b.UsedQuota FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_PRINTING_QUOTA AS b ON (a.UserID = b.UserID) WHERE $cond a.RecordStatus IN (0,1,2) $search_cond ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	}
	if(($targetClass != "") && ($targetStudent != ""))
	{
		$cond = " a.RecordType = 2 AND a.ClassName = '$targetClass' AND a.UserID = $targetStudent AND ";
		$sql = "SELECT a.UserID, $namefield, b.TotalQuota, b.UsedQuota FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_PRINTING_QUOTA AS b ON (a.UserID = b.UserID) WHERE $cond a.RecordStatus IN (0,1,2) $search_cond ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	}
	$result = $lidb->returnArray($sql, 4);
}
	
$table_content = "";

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list($user_id, $user_name, $total_quota, $used_quota) = $result[$i];
		$table_content .= "<tr><td><a href=\"view.php?StudentID=".$user_id."\">$user_name</a></td>";
		if($total_quota != "")
		{
			$table_content .= "<td>$total_quota</td>";
			$table_content .= "<td>$used_quota</td>";
			$table_content .= "<td><input type=checkbox name=user_id[] value=$user_id></td></tr>";
		}
		else
		{
			$table_content .= "<td>0</td>";
			$table_content .= "<td>0</td>";
			$table_content .= "<td><input type=checkbox name=user_id[] value=$user_id></td></tr>";
		}
	}
}

if(sizeof($result) <= 0)
{
	$table_content .= "<tr><td colspan=4 align=center>$i_no_record_exists_msg</td></tr>";
}

//$toolbar1 = "<a class=iconLink href=javascript:checkNew('insert.php')>".newIcon()."$button_new</a>";
$toolbar2 = "<a class=iconLink href=javascript:checkEdit(document.form1,'user_id[]','edit.php')>"."<img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
//$toolbar3 = "<a class=iconLink href=javascript:checkAlert(document.form1,'user_id[]','reset.php','$i_SmartCard_Payment_PhotoCopier_ResetQuota')>"."<img src='/images/admin/button/t_btn_reset_$intranet_session_language.gif' border='0' align='absmiddle'></a>";

$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" >\n";
$searchbar .= "<a href='javascript:document.form1.flag.value=3; document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

?>

<SCRIPT Language="JavaScript">
function checkEdit(obj,element,page){
        if(countChecked(obj,element)>=1) {
                obj.action=page;
                obj.submit();
        } else {
                alert(globalAlertMsg2);
        }
}
function generalFormSubmitCheck(obj)
{
		 if (obj.targetUserType.selectedIndex == 2){
				obj.flag.value = 2;		 
			}
         obj.submit();
}
function changeClass(obj)
{
         obj.flag.value = 2;
         if (obj.targetStudent != undefined)
             obj.targetStudent.selectedIndex = 0;
         generalFormSubmitCheck(obj);
}
function checkForm()
{
	if(document.form1.targetUserType.selectedIndex == 2)
	{
		if(document.form1.targetClass.selectedIndex == 0)
		{
			alert ("<?=$i_SmartCard_Payment_PhotoCopier_Class_Select_Instruction?>");
			document.form1.flag.value = 0;
			return false;
		}
	}		
	document.form1.flag.value = 3;
	return true
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../../',$i_Payment_Menu_PhotoCopierQuotaSetting,'../../',$i_Payment_Menu_PhotoCopier_Quota_Management,'../',$button_view,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="" method=POST onsubmit="return checkForm();">
<table width=433 border=0 cellpadding=4 cellspacing=0 align=center>
		<tr>
				<td align=right><?=$i_identity?></td>
				<td><?=$select_userType?></td>
		</tr>
<?
		if($targetUserType == 1)
		{
?>
		<tr>
				<td align=right><?=$i_UserName?></td>
				<td><?=$select_teacher?></td>
		</tr>
<?
		}
		if($targetUserType == 2)
		{
?>	
        <tr>
                <td align=right><?=$i_ClassName?></td>
                <td><?=$select_class?></td>
        </tr>
<?
		if($targetClass != "")
		{
?>
        <tr>
        		<td align=right><?=$i_UserStudentName?></td>
                <td><?=$select_students?></td>
        </tr>
<?
		}
		}
?>
        <tr>
                <td colspan=2 align=right><input type=image src="<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td>
        </tr>
</table>
<input type=hidden name=flag value=0>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
<tr><td height=10px></td></tr>
</table>
<?
if($flag == 3)
{
?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td class=admin_bg_menu align=right><?=$searchbar?></td></tr>
<tr><td class=admin_bg_menu align=right><?=$toolbar2.toolBarSpacer()?></td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
	<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
	<tr class=tableTitle>
		<td><?=$i_UserName?></td>
		<td><?=$i_SmartCard_Payment_PhotoCopier_TotalQuota?></td>
		<td><?=$i_SmartCard_Payment_PhotoCopier_UsedQuota?></td>
		<td><input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'user_id[]'):setChecked(0,this.form,'user_id[]')>
	</tr>
	<?=$table_content?>
	</table>
</td></tr>
<tr><td>
<table width=100% border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</tr></td>
</table>
<?
}
?>
</form>

<?
include_once("../../../../../templates/filefooter.php");
intranet_closedb();
?>