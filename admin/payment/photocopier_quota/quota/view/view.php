<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
$includeOncePath = "../../../../..";
include_once($includeOncePath."/includes/global.php");
include_once($includeOncePath."/includes/libdb.php");
include_once($includeOncePath."/includes/libuser.php");
include_once($includeOncePath."/includes/libfilesystem.php");
include_once($includeOncePath."/includes/libdbtable.php");
include_once($includeOncePath."/includes/libaccount.php");
include_once($includeOncePath."/includes/libpayment.php");
include_once($includeOncePath."/lang/lang.$intranet_session_language.php");
include_once($includeOncePath."/includes/libwordtemplates.php");
include_once($includeOncePath."/templates/adminheader_setting.php");
if ($StudentID == "")
{
    header("Location: index.php");
    exit();
}

intranet_opendb();

$lpayment = new libpayment();
$lu = new libuser($StudentID);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;


	$sql  =  "select a.DateModified, ";
//	$sql .=  "if (a.QuotaChange=0,'--',concat(' ',a.QuotaChange, concat('<a class=\"tran_Details\"onMouseMove=\"overhere()\" href=\'javascript:retrieveTransactionInfo(',a.recordid,')\'><img src=\"$image_path/icons_guardian_info.gif\" border=\'0\' alt=\'$button_select\'></a>'))),  ";
	$sql .=  "if (a.QuotaChange=0,'--',if(a.RecordType=3,concat(' ',a.QuotaChange, concat('<a class=\"tran_Details\"onMouseMove=\"overhere(\'a\')\" href=\'javascript:retrieveTransactionInfo(',a.recordid,')\'><img src=\"$image_path/icon_alt.gif\" border=\'0\' alt=\'$button_select\'></a>')),a.QuotaChange)), ";
	$sql .=  "a.QuotaAfter, ";
	$sql .=  "CASE a.RecordType ";
	$sql .=  "WHEN 1 THEN '".$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Add_By_Admin."' ";
	$sql .=  "WHEN 2 THEN '".$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Reset_By_Admin."' ";
	$sql .=  "WHEN 3 THEN '".$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Copied_Deduction."' ";
	$sql .=  "ELSE '".$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Purchasing."' END ";
	$sql .= "from ";
	$sql .= "PAYMENT_PRINTING_QUOTA_CHANGE_LOG a ";
	$sql .= "where ";
	$sql .= "a.UserID = ".$StudentID." ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.DateModified","a.QuotaChange","a.QuotaAfter","a.RecordType");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_SmartCard_Payment_PhotoCopier_LastestTransactionTime)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_SmartCard_Payment_PhotoCopier_ChangeQuota)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_SmartCard_Payment_PhotoCopier_FinalQuota)."</td>\n";
$li->column_list .= "<td width=29% class=tableTitle>".$li->column($pos++, $i_SmartCard_Payment_PhotoCopier_ChangeRecordType)."</td>\n";

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
//$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
//$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../../',$i_Payment_Menu_PhotoCopierQuotaSetting,'../../',$i_Payment_Menu_PhotoCopier_Quota_Management,'../',$button_view,'index.php',"[".$lu->UserNameClassNumber()."]",'');



echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>

<!--AJAX JS SETUP -->
<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="JavaScript" src="/templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="/templates/ajax_connection.js"></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
         #ToolMenu2{position:absolute; top: 0px; left: 0px; z-index:5; visibility:show;  width: 450px;}
		 .tran_Details{text-align:right;}
</style>
<script language="JavaScript">
isMenu = true;
</script>
<div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>
<div id="ToolMenu2"></div>
<!--AJAX JS END SETUP -->
<SCRIPT LANGUAGE=Javascript>
function openPrintPage()
{
         newWindow('view_print.php?StudentID=<?=$StudentID?>&keyword=<?=$keyword?>',10);
}

// AJAX follow-up
var callback = {
        success: function ( o )
        {
                jChangeContent( "ToolMenu2", o.responseText );
        }
}
// start AJAX
function retrieveTransactionInfo(transactionID)
{
        //FormObject.testing.value = 1;

        obj = document.form2;
//        var myElement = document.getElementById("ToolMenu2");

        showMenu("ToolMenu2","<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>Loading</td></tr></table>");

        YAHOO.util.Connect.setForm(obj);

        var path = "transactionDetails.php?transactionID=" + transactionID;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);

}

</SCRIPT>
<form name="form1" method="get">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?>&nbsp;</td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=StudentID value="<?=$StudentID?>">
</form>

<?
include_once($includeOncePath."/templates/adminfooter.php");
intranet_closedb();
?>
