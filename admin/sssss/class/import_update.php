<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$limport = new libimporttext();

$li = new libclass();
$lo = new libfilesystem();
$filepath = $classfile;
$filename = $classfile_name;

$websams_attendance_export = false;
if($special_feature['websams_attendance_export']){
	$websams_attendance_export = true;
}

if($websams_attendance_export){
	$file_format = array("Class","ClassLevel","WebSamsClassCode","WebSamsClassLevel");
}else{
	$file_format = array("Class","ClassLevel");
}

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php");
} else {
        //$ext = strtoupper($lo->file_ext($filename));
        //if($ext == ".CSV") {
		if($limport->CHECK_FILE_EXT($filename)) {
                # read file into array
                # return 0 if fail, return csv array if success
                //$data = $lo->file_read_csv($filepath);
                $data = $limport->GET_IMPORT_TXT($filepath);
                $title_row = $data[0];
                $formatCorrect = true;
                for ($i=0; $i<sizeof($title_row); $i++)
                {
                     if (trim($title_row[$i])!=$file_format[$i])
                     {
                         $formatCorrect = false;
                         break;
                     }
                }

        }
        if ($formatCorrect)
        {
            array_shift($data);                   # drop the title bar
            # Process import
            $delimiter = "";
            $toValues = "";
            $ldelimiter = "";
            $toLvlValues = "";
            $gdelimiter = "";
            $toGpValues = "";
            for ($i=0; $i<sizeof($data); $i++)
            {
	            if($websams_attendance_export){
                 	list($class,$level,$WebSamsClassCode,$WebSamsClassLevel) = $data[$i];
                }
                else{
                   	list($class,$level) = $data[$i];
                   	$WebSamsClassCode = "";
                   	$WebSamsClassLevel= "";
                }
                 $class = trim($class);
                 $level = trim($level);
                 $WebSamsClassCode = trim($WebSamsClassCode);
                 $WebSamsClassLevel = trim($WebSamsClassLevel);
                 
                 if ($class != "" && $level != "")
                 {
                     $toValues .= "$delimiter ('$class','$level','$WebSamsClassCode')";
                     $delimiter = ",";
                     $toLvlValues .= "$ldelimiter ('$level',1,'$WebSamsClassLevel')";
                     $ldelimiter = ",";
                     $toGpValues .= "$gdelimiter ('$class',3,5,now(),now())";
                     $gdelimiter = ",";
                 }
            }
            # Insert Level First
            $sql = "INSERT IGNORE INTO INTRANET_CLASSLEVEL (LevelName,RecordStatus,WebSAMSLevel) VALUES $toLvlValues";
            $li->db_db_query($sql);
            # Insert Groups
            $sql = "INSERT IGNORE INTO INTRANET_GROUP (Title,RecordType,StorageQuota,DateInput,DateModified) VALUES $toGpValues";
            $li->db_db_query($sql);

            # Create temp table
            $sql = "CREATE TEMPORARY TABLE TEMP_CLASS_IMPORT (
                       Class varchar(255) NOT NULL,
                       Level varchar(255) NOT NULL,
                       WebSAMSClassCode varchar(5) NOT NULL
                    )";
            $li->db_db_query($sql);
            $sql = "INSERT INTO TEMP_CLASS_IMPORT (Class,Level,WebSAMSClassCode) VALUES $toValues";
            $li->db_db_query($sql);

            $sql = "INSERT IGNORE INTO INTRANET_CLASS (ClassName,ClassLevelID,GroupID,RecordStatus,WebSAMSClassCode)
                           SELECT a.Class, b.ClassLevelID, c.GroupID,1,a.WebSAMSClassCode
                                  FROM TEMP_CLASS_IMPORT as a LEFT OUTER JOIN INTRANET_CLASSLEVEL as b
                                       ON a.Level = b.LevelName
                                       LEFT OUTER JOIN INTRANET_GROUP as c ON a.Class = c.Title";
            $li->db_db_query($sql);
            $li->fixClassGroupRelation();
        }
        else
        {
            include_once("../../../templates/adminheader_intranet.php");
            echo displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '../', $i_SettingsSchool_Class, 'javascript:history.back()', $button_import." "."$i_ClassName / $i_ClassLevel", '');
            echo "<blockquote><br>$i_import_invalid_format <br>\n";
            echo "<table width=100 border=1>\n";
            for ($i=0; $i<sizeof($file_format); $i++)
            {
                 echo "<tr><td>".$file_format[$i]."</td></tr>\n";
            }
            echo "</table></blockquote>\n";
                        echo "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                                        <tr><td><hr size=1></td></tr>
                                        <tr><td align='right'><a href='javascript:history.go(-2)'><img src='/images/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0'></a></td></tr></table>";


            //echo $li->queries;
            include_once("../../../templates/adminfooter.php");
        }

}
intranet_closedb();
header("Location: class.php?msg=2");
?>