<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$FormID = array_unique($FormID);
$FormID = array_values($FormID);
$list = implode(",",$FormID);

# Handle student profile evaluation form
$type_evaluation = 0;
$sql = "SELECT FormID FROM INTRANET_FORM WHERE FormID IN ($list) AND RecordType = '$type_evaluation'";
$eva = $li->returnVector($sql);
if (sizeof($eva)!=0)
{
    $eList = implode(",",$eva);
    $sql = "DELETE FROM PROFILE_STUDENT_ASSESSMENT WHERE FormID IN ($eList)";
    $li->db_db_query($sql);
}

# ----------------------------------
# Add other type of forms deletion here
# ----------------------------------


# Delete Form
$sql = "DELETE FROM INTRANET_FORM WHERE FormID IN ($list)";
$li->db_db_query($sql);

$msg = 3;

intranet_closedb();
header("Location: index.php?filter=$filter&status=$status&msg=$msg");
?>