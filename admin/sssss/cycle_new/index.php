<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

switch ($step)
{
        case 2: $xmsg = "<font color=green>$i_CycleNew_Prompt_PreviewGenerated</font>"; break;
        case 3: $xmsg = "<font color=green>$i_CycleNew_Prompt_ProductionMade</font>"; break;
}
?>

<?= displayNavTitle($i_admintitle_sc, '',$i_adminmenu_sc_school_settings,'../', $i_admintitle_sc_cycle, '') ?>
<?= displayTag("head_cycle_$intranet_session_language.gif", $msg) ?>
<script LANGUAGE=Javascript>
function generatePreview()
{
         if (confirm("<?=$i_CycleNew_Alert_GeneratePreview?>"))
         {
             location.href = "preview_generate.php";
         }
}
function makeToProduction()
{
         if (confirm("<?=$i_CycleNew_Alert_MakeToProduction?>"))
         {
             location.href = "makeproduction.php";
         }
}
</script>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300>
<blockquote>

<?= displayOption($i_CycleNew_Menu_DefinePeriods, 'periods/', 1,
                                $i_CycleNew_Menu_GeneratePreview, 'javascript:generatePreview()', 1,
                                $i_CycleNew_Menu_CheckPreview, 'javascript:newWindow("preview_check.php",1)', 1,
                                $i_CycleNew_Menu_MakePreviewToProduction, 'javascript:makeToProduction()',1
                                ) ?>

                                <p><b><?=$i_CycleSpecialNotes?><b></p>
</blockquote>
</td></tr>
</table>
<SCRIPT LANGUAGE=Javascript>
parent.intranet_admin_menu.runStatusWin("");
</SCRIPT>
<?
include_once("../../../templates/adminfooter.php");
?>