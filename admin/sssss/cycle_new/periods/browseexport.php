<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();
$lexport = new libexporttext();

$lc = new libcycleperiods();
$data = $lc->retrieveImportRecords($PeriodStart,$PeriodEnd);
$content = "\"Date\",\"Chi\",\"Eng\",\"Short\"\n";
$utf_content = "Date\tChi\tEng\tShort\r\n";
for ($i=0; $i<sizeof($data); $i++)
{
     list($id,$date,$txtEng,$txtChi,$txtShort) = $data[$i];
     $content .= "\"$date\",\"$txtChi\",\"$txtEng\",\"$txtShort\"\n";
     $utf_content .= "$date\t$txtChi\t$txtEng\t$txtShort\r\n";
}
$content = trim($content);
$utf_content = trim($utf_content);
$filename = "download_$PeriodStart"."to$PeriodEnd.csv";

if (!$g_encoding_unicode) {
	output2browser($content,$filename);
} else {
	$lexport->EXPORT_FILE($filename, $utf_content);
}
?>