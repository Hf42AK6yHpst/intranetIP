<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

?>

<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '') ?>
<?= displayTag("head_school_setting_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300 align=left>
<blockquote>
<?= displayOption($i_SettingsSchool_Class, 'class/class.php', 1,
                                $i_admintitle_fs_homework_subject, 'subject/subject.php', $li_menu->is_access_homework,
                                $i_adminmenu_sc_cycle, 'cycle_new/', $li_menu->is_access_cycle,
                                $i_adminmenu_sc_period, 'rbps/', $li_menu->is_access_rbps,
                                $i_SettingsSchool_Forms, 'form/', $li_menu->is_access_rbps,
                                $i_OrganizationPage_Settings, 'organization/', $li_menu->is_access_rbps,
                                $i_adminmenu_sc_wordtemplates, 'wordtemplates/', $li_menu->is_access_rbps
                                ) ?>
</blockquote>
</td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>