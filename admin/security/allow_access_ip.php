<?php
// 

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libauth.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libgeneralsettings.php");
include_once("../../includes/libinterface.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$linterface = new interface_html();

$la = new libaccount();
$la->is_access_function($PHP_AUTH_USER);
if (!$la->is_access_system_security)
{
	die ("E001 :: Access denied! ");
}

$libau = new libauth();

// get saved IP
$lgs = new libgeneralsettings();
$AllowAccessIP = $lgs->Get_General_Setting("AdminConsole", array("'AllowAccessIP'"));
$AllowAccessIP = $AllowAccessIP['AllowAccessIP'];

?>


<script type="text/javascript" src="/templates/jquery/jquery-1.4.4.min.js"></script>


<form name="form1" action="allow_access_ip_update.php" method="post" >

<?= displayNavTitle($i_admintitle_sa, '', $Lang['SysMgr']['Security']['Title'], 'index.php', $Lang['SysMgr']['Security']['AllowAccessIP'], '') ?>
<?= displayTag("head_system_security_$intranet_session_language.gif", $msg) ?>

<blockquote>


	
	<table class="form_table_v30">
		<tbody>
	   	<tr>
				<td valign="top" nowrap="nowrap" class="tabletext">
					<?=$Lang['Security']['TerminalIPList']?>
				</td>
				<td class="tabletext" width="70%">
					<span class="tabletextremark"><?=$Lang['Security']['TerminalIPInput']?><br />
					<?=$Lang['Security']['TerminalYourAddress'].':'.getRemoteIpAddress()?></span><br />
					<?=$linterface->GET_TEXTAREA("AllowAccessIP", $AllowAccessIP)?>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="formfieldtitle tabletext">
					<br /><?=$Lang['Security']['TerminalIPSettingsDescription']?>
				</td>
			</tr>
	   </tbody>
	</table>
	
	
	



<script language="javascript">
function ToggleEditView(parType)
{
	if (parType==1)
	{
		$("#LayerView").hide();
		$("#LayerEdit").show();
		ToggleDurationRow();
	} else
	{
		$("#LayerEdit").hide();
		$("#LayerView").show();
	}
}

function ToggleDurationRow()
{
	if ($("#login_attempt_limit").val()==-1)
	{
		$("#layer_lock_duration").hide();
	} else
	{
		$("#layer_lock_duration").show();
	}
}
</script>

</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ?>
 <a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>