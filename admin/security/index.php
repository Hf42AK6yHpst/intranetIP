<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");


$la = new libaccount();
$la->is_access_function($PHP_AUTH_USER);
if (!$la->is_access_system_security)
{
	die ("E001 :: Access denied! ");
}

//$Lang['General']['ReturnMessage'][$_GET["xmsg"]]
?>

<?= displayNavTitle($i_admintitle_sa, '', $Lang['SysMgr']['Security']['Title'], '') ?>
<?= displayTag("head_system_security_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300 align=left>
<blockquote>

<p></p>

<?= displayOption($Lang['SysMgr']['Security']['AllowAccessIP'], 'allow_access_ip.php', 1
                            ) ?>
</blockquote>
</td></tr>
</table>


<?php
include_once("../../templates/adminfooter.php");
?>