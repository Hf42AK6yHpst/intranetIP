<?php
include("../includes/global.php");
include("../includes/libdb.php");
include("../includes/libfamily.php");
include("../lang/lang.$intranet_session_language.php");
intranet_opendb();
$lfamily = new libfamily();
#$body_tags = "topmargin=0 leftmargin=0 marginheight=0 marginwidth=0";
include("../templates/fileheader.php");
?>

      <?=$lfamily->displayChildrenList($uid)?>

<?php

include("../templates/filefooter.php");
intranet_closedb();
?>