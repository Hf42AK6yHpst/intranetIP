<?php
# Modification Log:
#	**** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [start] ****
#	Date:	2016-08-03 Ivan [ip.2.5.7.9.1]
#			added CompanyName for macau pui ching (but treat as general improvement)
#	**** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [end] ****
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$student_id = $StudentID[0];

if($student_id=="")
	header("Location: index.php");
	
$li = new libdb();

//print_r($ClassID);
if(sizeof($ClassID)>0)
{
	$class_list = implode(",",$ClassID);
	//echo $class_list;
}

## UPDATE
if($recordids!=""){ 
	$RecordIDs =  explode(",",$recordids);
	for($i=0;$i<sizeof($RecordIDs);$i++){
		$record_id = $RecordIDs[$i];
		
		// deleted
		if(is_array($Delete) && in_array($record_id,$Delete)) continue;
		
		$ch_name = ${'chname_'.$record_id};
		$en_name = ${'enname_'.$record_id};
		$phone   = ${'phone_'.$record_id};
		$emphone = ${'emphone_'.$record_id};

		if($plugin['StudentRegistry']){
			$add_area = ${'add_area_'.$record_id};
			$add_road = ${'add_road_'.$record_id};
			$add_address = ${'add_address_'.$record_id};
			if($add_area == "" || $add_area == "O"){
				$address = "";
			}else{
				$address = $i_StudentRegistry_AreaCode[$add_area]."\n";
			}
			$address .= trim($add_road)."\n";
			$address .= trim($add_address);
		}else{
			$add_area = '';
			$add_road = '';
			$add_address = '';
			$address = ${'address_'.$record_id};
		}

		$relation= ${'relation_'.$record_id};

		$fields  = "ChName ='$ch_name' ";
		$fields .= ",EnName='$en_name'";
		$fields .= ",Phone='$phone'";
		$fields .= ",EmPhone='$emphone'";
		$fields .= ",Address='$address'";
		$fields .= ",Relation='$relation'";

		$fields .= is_array($main) && in_array($record_id,$main)? ", IsMain=1 ": ", IsMain=0 ";

		$fields .= ($plugin['sms'])?(is_array($sms) && in_array($record_id,$sms)?",IsSMS=1":", IsSMS=0"):"";
		$fields .= ",ModifiedDate=NOW()";
		$sql="UPDATE $eclass_db.GUARDIAN_STUDENT SET  $fields WHERE RecordID = '$record_id' ";
		$li->db_db_query($sql);

		$occupation = ${'occupation_'.$record_id};
		$companyName = ${'companyName_'.$record_id};
		$IsLiveTogether = (int)${'liveTogether_'.$record_id};
		$IsEmergencyContact = (int)(is_array($ec) && in_array($record_id,$ec));

		$sql = "SELECT ExtendRecordID FROM $eclass_db.GUARDIAN_STUDENT_EXT_1 WHERE RecordID = '$record_id'";
		$r = $li->returnArray($sql, 9);
		$ExtendRecordID = isset($r[0]['ExtendRecordID']) ? $r[0]['ExtendRecordID'] : '';
		if($ExtendRecordID){
			//update the extend record
			$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT_EXT_1 SET 
			IsLiveTogether = '$IsLiveTogether',IsEmergencyContact = '$IsEmergencyContact',
			Occupation = '$occupation', CompanyName = '$companyName',
			AreaCode = '$add_area', Road = '$add_road', Address = '$add_address',
			ModifiedDate = NOW()
			WHERE ExtendRecordID = '$ExtendRecordID'
			";
		}else{
			//insert the extend record
			$sql = "INSERT INTO $eclass_db.GUARDIAN_STUDENT_EXT_1 (
			UserID, RecordID, IsLiveTogether, IsEmergencyContact, Occupation, CompanyName,";
			$sql .= " AreaCode, Road, Address, InputDate,ModifiedDate
			) VALUES (
			'$student_id','$record_id', '$IsLiveTogether', '$IsEmergencyContact', '$occupation', '$companyName', 
			'$add_area','$add_road', '$add_address', NOW(), NOW()
			)";
		}
		$li->db_db_query($sql);
//echo $sql;
//echo mysql_error();
	}
}

## INSERT
if($newids!=""){
//	$sql=" INSERT INTO $elcass_db.GUARDIAN_STUDENT ";
	$ids = explode(",",$newids);
//	$delim="";
//	$values="";
	for($i=0;$i<sizeof($ids);$i++){
		$id = $ids[$i];
		$ch_name = ${'chname_new_'.$id};
		$en_name = ${'enname_new_'.$id};
		$phone   = ${'phone_new_'.$id};
		$emphone = ${'emphone_new_'.$id};

		if($plugin['StudentRegistry']){
			$add_area = ${'add_area_new_'.$id};
			$add_road = ${'add_road_new_'.$id};
			$add_address = ${'add_address_new_'.$id};
			if($add_area == "" || $add_area == "O"){
				$address = "";
			}else{
				$address = $i_StudentRegistry_AreaCode[$add_area]."\n";
			}
			$address .= trim($add_road)."\n";
			$address .= trim($add_address);
		}else{
			$add_area = '';
			$add_road = '';
			$add_address = '';
			$address = ${'address_new_'.$id};
		}
		$relation= ${'relation_new_'.$id};

		$new_str ="new_$id";
		$IsMain = (int)(is_array($main) && in_array($new_str,$main));
		$IsSMS = (int) ($plugin['sms'] && is_array($sms) && in_array($new_str,$sms));
		$IsEmergencyContact = (int)(is_array($ec) && in_array($new_str,$ec));
		$occupation = ${'occupation_new_'.$id};
		$companyName = ${'companyName_new_'.$id};

		$IsLiveTogether = (int)${'liveTogether_new_'.$id};

/*
//changed to "on the fly"
		$values .=$delim."('$student_id','$ch_name','$en_name','$phone','$emphone','$address','$relation'";
		$values .= is_array($main) && in_array($new_str,$main)? ",1 ": ", 0 ";

		$values .= ($plugin['sms'])?(is_array($sms) && in_array($new_str,$sms)?",1":",0"): ",0";
		$values .= ", NOW(),NOW())";
		$delim=",";
*/
		$sql = "INSERT INTO $eclass_db.GUARDIAN_STUDENT (
			UserID,ChName,EnName,
			Phone,EmPhone,Address,Relation,
			IsMain,IsSMS,InputDate,ModifiedDate
			) VALUES (
			'$student_id','$ch_name','$en_name',
			'$phone','$emphone','$address','$relation',
			'$IsMain', '$IsSMS',NOW(), NOW()
			) ";
		$li->db_db_query($sql);
		$record_id = mysql_insert_id();

		//insert the extend record
		$sql = "INSERT INTO $eclass_db.GUARDIAN_STUDENT_EXT_1 (
		UserID,RecordID, IsLiveTogether, IsEmergencyContact, Occupation, CompanyName,
		AreaCode, Road, Address,
		InputDate,ModifiedDate
		) VALUES (
		'$student_id','$record_id', '$IsLiveTogether', '$IsEmergencyContact', '$occupation', '$companyName',
		'$add_area','$add_road', '$add_address', 
		NOW(), NOW()
		)";
		$li->db_db_query($sql);

	}
//	$sql = "INSERT INTO $eclass_db.GUARDIAN_STUDENT (UserID,ChName,EnName,Phone,EmPhone,Address,Relation,IsMain,IsSMS,InputDate,ModifiedDate) VALUES $values ";
//	$li->db_db_query($sql);
	//echo $sql."<Br>";
}

### Delete
if(sizeof($Delete)>0){
	$deletedids = implode(",",$Delete);
	$sql="DELETE FROM $eclass_db.GUARDIAN_STUDENT WHERE RecordID IN ($deletedids)";
	$li->db_db_query($sql);

	$sql="DELETE FROM $eclass_db.GUARDIAN_STUDENT_EXT_1 WHERE RecordID IN ($deletedids)";
	$li->db_db_query($sql);
}
	
/*
for($i=0;$i<sizeof($ClassID);$i++){
	echo "<input type=hidden name=ClassID[] value='".$ClassID[$i]."'>";
}
*/

intranet_closedb();
header("Location: edit.php?StudentID[]=".$student_id."&ClassList=".$class_list."&keyword=".$keyword."&msg=2");
?>
