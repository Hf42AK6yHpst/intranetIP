<?php

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


$namefield = getNameFieldByLang("a.");
$namefield2= getNameFieldWithClassNumberByLang("a.");
$cond="";
$cond_keyword="";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

/*
echo $ClassList;
if($ClassList!="")
{
	$ClassID = explode(",", $ClassList);
}
*/

$toolbar .= "<a class=iconLink href=javascript:checkGet(document.form1,'export.php')>".exportIcon()."$button_export</a>\n".toolBarSpacer();


$delim="";
for($i=0;$i<sizeof($ClassID);$i++){
	$class_str .= $delim."'".$ClassID[$i]."'";
	$delim=",";
}
$cond = " AND a.ClassName IN ($class_str) ";
if($keyword!=""){
		$cond_keyword=" AND ($namefield2 LIKE '%$keyword%' OR a.UserLogin LIKE '%$keyword%') ";
}





$main_content_field = "IF(b.EnName IS NOT NULL AND b.EnName !='' AND b.ChName IS NOT NULL AND b.ChName !='', CONCAT(b.EnName,' / ',b.ChName), IF(b.EnName IS NOT NULL AND b.EnName!='',b.EnName,b.ChName))";
$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";

$smsphone_field = "IF(d.Phone IS NOT NULL AND d.Phone !='' AND d.EmPhone IS NOT NULL AND d.EmPhone !='', CONCAT(d.Phone,' / ',d.EmPhone),IF(d.Phone IS NOT NULL AND d.Phone!='',d.Phone,d.EmPhone)) ";
$smsphone_field = "IF($smsphone_field='' OR $smsphone_field IS NULL,'-',$smsphone_field)";

/*
$sql="SELECT CONCAT('<a href=\"javascript:editGuardian(',a.UserID,')\">',$namefield,'</a>'),
			a.ClassName,
			a.ClassNumber,
			$main_content_field,
			".($plugin['sms']?"$smsphone_field,":"")."
			CONCAT('<input type=checkbox name=StudentID[] value=',a.UserID,'>')
			FROM INTRANET_USER as a LEFT OUTER JOIN
				$eclass_db.GUARDIAN_STUDENT as b ON (a.UserID=b.UserID AND b.IsMain=1),
				INTRANET_CLASS as c LEFT OUTER JOIN
				$eclass_db.GUARDIAN_STUDENT as d ON (a.UserID=d.UserID AND d.IsSMS=1) 
			WHERE a.RecordType=2 AND a.RecordStatus IN (0,1,2) AND a.ClassName = c.ClassName AND c.RecordStatus=1 
				 $cond
				 $cond_keyword
		";
		*/
$sql="SELECT CONCAT('<a href=\"javascript:editGuardian(',a.UserID,')\">',$namefield,'</a>'),
			a.ClassName,
			a.ClassNumber,
			$main_content_field,
			".($plugin['sms']?"$smsphone_field,":"")."
			CONCAT('<input type=checkbox name=StudentID[] value=',a.UserID,'>')
			FROM INTRANET_USER as a 
				LEFT OUTER JOIN	$eclass_db.GUARDIAN_STUDENT as b ON (a.UserID=b.UserID AND b.IsMain=1)
				LEFT OUTER JOIN	$eclass_db.GUARDIAN_STUDENT as d ON (a.UserID=d.UserID AND d.IsSMS=1) 
			WHERE a.RecordType=2 AND a.RecordStatus IN (0,1,2) 
				 $cond
				 $cond_keyword
		";		
	$li = new libdbtable($field, $order, $pageNo);
	
	
	if($plugin['sms'])
		$li->field_array = array("$namefield","a.ClassName","a.ClassNumber","$main_content_field","$smsphone_field");
	else $li->field_array = array("$namefield","a.ClassName","a.ClassNumber","$main_content_field");
	
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	$li->title = "";
	$li->column_array = array(0,0,0,0,0);
	$li->wrap_array = array(0,0,0,0,0);
	$li->IsColOff = 2;

	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width=5% class=tableTitle>#</td>\n";
	$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
	$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_ClassName)."</td>\n";
	$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_ClassNumber)."</td>\n";
	$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_StudentGuardian_MainContent)."</td>\n";
	if($plugin['sms'])
		$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_StudentGuardian_SMSPhone)."</td>\n";
	$li->column_list .= "<td width=5% class=tableTitle>".$li->check("StudentID[]")."</td>\n";

	$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'StudentID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";

	
if(sizeof($ClassID))	
	$title = implode(",",$ClassID);	
?>
<?= displayNavTitle($i_adminmenu_adm, '', $i_StudentGuardian['MenuInfo'], 'index.php',$button_search,'search.php') ?>
<?= displayTag("head_guardian_$intranet_session_language.gif", $msg) ?>

<script language='javascript'>
function editGuardian(studentid){
	//document.form1.action = 'edit.php?SID='+studentid;
	document.form1.SID.value=studentid;
	document.form1.action='edit.php';
	document.form1.submit();
}
</script>
<form name="form1" method="GET">

	<table  width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td width=100><?=$i_StudentGuardian_Searching_Area?>:</td></tr>
	<tr><td><?=$title?></td></tr>
	<tr><td height=20></td></tr>
	</table>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
	<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
	<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar2", $functionbar); ?></td></tr>
	<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
	</table>
	<?php echo $li->display(); ?>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
	</table>

<br><br>
<?php
for($i=0;$i<sizeof($ClassID);$i++){
	echo "<input type=hidden name=ClassID[] value='".$ClassID[$i]."'>";
}

?>
<input type=hidden name=SID value=''>
<input type=hidden name=keyword value="<?=$keyword?>">
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>
<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>