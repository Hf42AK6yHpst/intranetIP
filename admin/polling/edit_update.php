<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$Question = intranet_htmlspecialchars(trim($Question));
$AnswerA = intranet_htmlspecialchars(trim($AnswerA));
$AnswerB = intranet_htmlspecialchars(trim($AnswerB));
$AnswerC = intranet_htmlspecialchars(trim($AnswerC));
$AnswerD = intranet_htmlspecialchars(trim($AnswerD));
$AnswerE = intranet_htmlspecialchars(trim($AnswerE));
$AnswerF = intranet_htmlspecialchars(trim($AnswerF));
$AnswerG = intranet_htmlspecialchars(trim($AnswerG));
$AnswerH = intranet_htmlspecialchars(trim($AnswerH));
$AnswerI = intranet_htmlspecialchars(trim($AnswerI));
$AnswerJ = intranet_htmlspecialchars(trim($AnswerJ));
$Reference = intranet_htmlspecialchars(trim($Reference));
$Reference = ($Reference=="http://") ? "" : $Reference;
$DateStart = intranet_htmlspecialchars(trim($DateStart));
$DateEnd = intranet_htmlspecialchars(trim($DateEnd));
$DateRelease = intranet_htmlspecialchars(trim($DateRelease));
# Check date
# Start date >= curdate() && Start date <= due date
$start_stamp = strtotime($DateStart);
$due_stamp = strtotime($DateEnd);

if ($li->compareDate($due_stamp,$start_stamp)<0)
{
    $msg = 0;
}
else
{
$DateRelease = ($DateRelease == "" ? $DateEnd : $DateRelease);
$sql = "UPDATE INTRANET_POLLING SET
               Question = '$Question',
               AnswerA = '$AnswerA',
               AnswerB = '$AnswerB',
               AnswerC = '$AnswerC',
               AnswerD = '$AnswerD',
               AnswerE = '$AnswerE',
               AnswerF = '$AnswerF',
               AnswerG = '$AnswerG',
               AnswerH = '$AnswerH',
               AnswerI = '$AnswerI',
               AnswerJ = '$AnswerJ',
               Reference = '$Reference',
               DateStart = '$DateStart',
               DateEnd = '$DateEnd',
               DateRelease = '$DateRelease',
               RecordStatus = '$RecordStatus',
               DateModified = now()
          WHERE
               PollingID = $PollingID";
$li->db_db_query($sql);
$msg = 2;
}
intranet_closedb();
header("Location: index.php?msg=$msg");
?>