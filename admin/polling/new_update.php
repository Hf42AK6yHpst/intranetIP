<?php
include("../../includes/global.php");
include("../../includes/libdb.php");

intranet_opendb();

$li = new libdb();

$Question = intranet_htmlspecialchars(trim($Question));
$AnswerA = intranet_htmlspecialchars(trim($AnswerA));
$AnswerB = intranet_htmlspecialchars(trim($AnswerB));
$AnswerC = intranet_htmlspecialchars(trim($AnswerC));
$AnswerD = intranet_htmlspecialchars(trim($AnswerD));
$AnswerE = intranet_htmlspecialchars(trim($AnswerE));
$AnswerF = intranet_htmlspecialchars(trim($AnswerF));
$AnswerG = intranet_htmlspecialchars(trim($AnswerG));
$AnswerH = intranet_htmlspecialchars(trim($AnswerH));
$AnswerI = intranet_htmlspecialchars(trim($AnswerI));
$AnswerJ = intranet_htmlspecialchars(trim($AnswerJ));
$Reference = intranet_htmlspecialchars(trim($Reference));
$Reference = ($Reference=="http://") ? "" : $Reference;
$DateStart = intranet_htmlspecialchars(trim($DateStart));
$DateEnd = intranet_htmlspecialchars(trim($DateEnd));
$DateRelease = intranet_htmlspecialchars(trim($DateRelease));

# Check date
# Start date >= curdate() && Start date <= due date
$start_stamp = strtotime($DateStart);
$due_stamp = strtotime($DateEnd);

$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
if ($li->compareDate($start_stamp,$today) < 0 || $li->compareDate($due_stamp,$start_stamp)<0)
{
    $msg = 0;
}
else
{
    if ($DateRelease == "")
    {
        $DateRelease = $DateEnd;
    }
    $sql = "INSERT INTO INTRANET_POLLING (Question, AnswerA, AnswerB, AnswerC, AnswerD, AnswerE, AnswerF, AnswerG, AnswerH, AnswerI, AnswerJ, Reference, DateStart, DateEnd,DateRelease, RecordStatus, DateInput, DateModified) VALUES ('$Question', '$AnswerA', '$AnswerB', '$AnswerC', '$AnswerD', '$AnswerE', '$AnswerF', '$AnswerG', '$AnswerH', '$AnswerI', '$AnswerJ', '$Reference', '$DateStart', '$DateEnd','$DateRelease', '$RecordStatus', now(), now())";
    $li->db_db_query($sql);
    $PollingID = $li->db_insert_id();
    $msg = 1;
}

intranet_closedb();
header("Location: index.php?msg=$msg");
?>