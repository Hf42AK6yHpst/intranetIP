<?php
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

$SettingList['TeacheriAccountProfileAllowEdit'] = (empty($_REQUEST['teacher_profile_setting'])) ? 0 : $_REQUEST['teacher_profile_setting'];
$GeneralSetting = new libgeneralsettings();

$GeneralSetting->Save_General_Setting('StudentAttendance',$SettingList);
header("Location: teacher_setting.php?msg=2");
?>
