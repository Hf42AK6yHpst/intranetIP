<?php
include_once("../../includes/global.php");

$content = "";

$class_teacher_level += 0;
$subj_teacher_level += 0;
if (!$class_teacher_level)
{
     $class_teacher_level = 0;
     $class_attend = 0;
     $class_merit = 0;
     $class_service = 0;
     $class_activity = 0;
     $class_award = 0;
     $class_assessment = 0;
     $class_file = 0;
}
if (!$subj_teacher_level)
{
     $subj_teacher_level = 0;
     $subj_attend = 0;
     $subj_merit = 0;
     $subj_service = 0;
     $subj_activity = 0;
     $subj_award = 0;
     $subj_assessment = 0;
     $subj_file = 0;
}
if ($class_teacher_level!=0) $class_teacher_level -= 1;
$class_attend += 0;
$class_merit += 0;
$class_service += 0;
$class_activity += 0;
$class_award += 0;
$class_assessment += 0;
$class_file += 0;
if ($subj_teacher_level!=0) $subj_teacher_level -= 1;
$subj_attend += 0;
$subj_merit += 0;
$subj_service += 0;
$subj_activity += 0;
$subj_award += 0;
$subj_assessment += 0;
$subj_file += 0;

$content = "$class_teacher_level$class_attend$class_merit$class_service$class_activity$class_award$class_assessment$class_file\n";
$content .= "$subj_teacher_level$subj_attend$subj_merit$subj_service$subj_activity$subj_award$subj_assessment$subj_file";

$setting_file = "$intranet_root/file/std_profile_acl.txt";
write_file_content($content,$setting_file);
header("Location: teacher.php?msg=2");
?>
