<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();
// $li_menu from adminheader_intranet.php

$lstudentprofile = new libstudentprofile();

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_Profile_settings, 'index.php',$i_Profile_settings_display,'') ?>
<?= displayTag("head_profile_set_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="display_update.php" method=POST>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td>
<blockquote>
<table border=0 cellpadding=5 cellspacing=0>
<tr>
<td class=tableTitle_new style="vertical-align:bottom">&nbsp;</td>
<td class=tableTitle_new style="vertical-align:bottom"><u><?= $i_Profile_Hide_Frontend ?></u></td>
<td class=tableTitle_new style="vertical-align:bottom"><u><?= $i_Profile_Hide_PrintPage ?></u></td>
</tr>
<tr>
<td style="vertical-align:middle"><?=$i_Profile_Attendance?></td>
<td class=td_center_middle><input type=checkbox name=frontend_attendance <?=($lstudentprofile->is_frontend_attendance_hidden? "CHECKED":"")?> value=1></td>
<td class=td_center_middle><input type=checkbox name=printpage_attendance <?=($lstudentprofile->is_printpage_attendance_hidden? "CHECKED":"")?> value=1></td>
</tr>
<tr>
<td style="vertical-align:middle"><?=$i_Profile_Merit?></td>
<td class=td_center_middle><input type=checkbox name=frontend_merit <?=($lstudentprofile->is_frontend_merit_hidden? "CHECKED":"")?> value=1></td>
<td class=td_center_middle><input type=checkbox name=printpage_merit <?=($lstudentprofile->is_printpage_merit_hidden? "CHECKED":"")?> value=1></td>
</tr>
<tr>
<td style="vertical-align:middle"><?=$i_Profile_Service?></td>
<td class=td_center_middle><input type=checkbox name=frontend_service <?=($lstudentprofile->is_frontend_service_hidden? "CHECKED":"")?> value=1></td>
<td class=td_center_middle><input type=checkbox name=printpage_service <?=($lstudentprofile->is_printpage_service_hidden? "CHECKED":"")?> value=1></td>
</tr>
<tr>
<td style="vertical-align:middle"><?=$i_Profile_Activity?></td>
<td class=td_center_middle><input type=checkbox name=frontend_activity <?=($lstudentprofile->is_frontend_activity_hidden? "CHECKED":"")?> value=1></td>
<td class=td_center_middle><input type=checkbox name=printpage_activity <?=($lstudentprofile->is_printpage_activity_hidden? "CHECKED":"")?> value=1></td>
</tr>
<tr>
<td style="vertical-align:middle"><?=$i_Profile_Award?></td>
<td class=td_center_middle><input type=checkbox name=frontend_award <?=($lstudentprofile->is_frontend_award_hidden? "CHECKED":"")?> value=1></td>
<td class=td_center_middle><input type=checkbox name=printpage_award <?=($lstudentprofile->is_printpage_award_hidden? "CHECKED":"")?> value=1></td>
</tr>

</table>
</blockquote>
</td></tr>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>

</table>
</form>
<?
include_once("../../templates/adminfooter.php");

?>
