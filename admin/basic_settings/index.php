<?php

# using: 

###################################################### Change Log ######################################################
# Date		: 20160826 (Carlos) - get website value from /file/email_website.txt
# Date		: 20160718 (Jason) - change split to explode to support php5.4 
# Date		: 20120914 (Yuen) - Introduced Account Lockout Policy
#
###################################################### Change Log ######################################################


include("../../includes/global.php");
include("../../lang/email.php");
include("../../includes/libdb.php");
include("../../includes/libaccount.php");
include("../../includes/libauth.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_setting.php");


$li = new libfilesystem();
$LangID = $li->file_read($intranet_root."/file/language.txt");
$LangID += 0;
if(!file_exists($intranet_root."/file/user_info_settings.txt")){
  $misc = $li->file_read($intranet_root."/file/basic_misc.txt");
  $misca = explode("\n",$misc);
  $email_allowed = $misca[0];
  $title_disabled = $misca[1];
  $home_tel_allowed = $misca[2];
  $email_checked = ($email_allowed==1? "CHECKED":"");
  $title_checked = ($title_disabled==1? "CHECKED":"");
  $home_tel_checked = ($home_tel_allowed==1? "CHECKED":"");
}

$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
if ($imgfile != "")
{
    $badge_checked = "CHECKED";
}

$website = trim(get_file_content("$intranet_root/file/email_website.txt"));

$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];
$school_org = $school_data[1];
$school_phone = $school_data[2];
for($i=3; $i<count($school_data); $i++)
	$school_address .= $school_data[$i]."\n";

/*	
$academic_yr = get_file_content("$intranet_root/file/academic_yr.txt");
if ($academic_yr == "") $academic_yr = date("Y");
*/
/*
$semester_mode = $li->file_read($intranet_root."/file/semester_mode.txt");
if($SemesterMode == ""){
	$SemesterMode = $semester_mode;
}
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
$number = sizeof($semester_data);
$number = ($number > 4 ? $number+2 : 6);
if($SemesterMode == 1 || $SemesterMode == ""){
	$semester_table = "<table width=100% border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=1>\n";
	$semester_table .= "<tr><td align='center' nowrap class='tableTitle_new'><u>$i_SettingsCurrentSemester</u></td><td align='center' class='tableTitle_new'><u>$i_SettingsSemesterList</u></td></tr>\n";
	for ($i=0; $i<$number; $i++)
	{
	     if ($semester_data[$i] != "")
	     {
	         $linedata = split("::",$semester_data[$i]);
	     }
	     else
	     {
	         $linedata = array("","");
	     }
	     if ($linedata[1]==1)
	     {
	         $selected_str = "CHECKED";
	     }
	     else
	     {
	         $selected_str = "";
	     }
	     $semester_table .= "<tr><td align='center'><input type=radio name=currentsem value=$i $selected_str></td><td align='center'><input type=text name=semester[] length=40 value=\"".$linedata[0]."\"></td></tr>\n";
	}
	$semester_table .= "</table>\n";
}
if($SemesterMode == 2){
	$semester_table = "<table width=100% border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=1>\n";
	$semester_table .= "<tr><td align='center' class='tableTitle_new'><u>$i_SettingsSemesterList</u></td><td align='center' class='tableTitle_new'><u>$i_general_startdate</u><br><span class=\"extraInfo\">(YYYY-MM-DD)</span></td><td align='center' class='tableTitle_new'><u>$i_general_enddate</u><br><span class=\"extraInfo\">(YYYY-MM-DD)</span></td></tr>\n";
	for($i=0; $i<$number; $i++)
	{
		if ($semester_data[$i] != "")
	    {
		    $linedata = split("::",$semester_data[$i]);
	    }
	    else
	    {
		    $linedata = array("","");
	    }
		$semester_table .= "<tr>
								<td align='center'><input type=text name=semester[] length=40 value=\"".$linedata[0]."\"</td>
								<td align='center'><input type=text name=start_date[] size=16 maxlength=10 value=\"".$linedata[2]."\"</td>
								<td align='center'><input type=text name=end_date[] size=16 maxlength=10 value=\"".$linedata[3]."\"</td>
							</tr>";
	}
	$semester_table .= "</table>\n";
}
*/

$current_daytype = get_file_content("$intranet_root/file/schooldaytype.txt");
if ($current_daytype != 2 && $current_daytype != 3)
    $current_daytype = 1;

$daySelect = getSelectSchoolDayType("name=DayType",$current_daytype);


$popup_file = $li->file_read($intranet_root."/file/popupdate.txt");
$dates = explode("\n",$popup_file);

$libau = new libauth();
$login_attempt_limit = $libau->LoginAttemptLimit;
$login_lock_duration = $libau->LoginLockDuration;

?>

<script language="javascript">
/*function checkform(obj){
	var objSemester = document.form1.elements['semester[]'];
	var objStartDate = document.form1.elements['start_date[]'];
	var objEndDate = document.form1.elements['end_date[]'];
	var check_step1 = 0;
	var check_passed = 0;
	var valid_row = new Array();
	
	if(!check_text(obj.website, "<?php echo $i_alert_pleasefillin.$i_EmailWebsite; ?>.")){
		return false;
	}else{
		for(i=0; i<objSemester.length; i++){
			if(objSemester[i].value != ""){
				if((objStartDate[i].value != "") || (objEndDate[i].value != "")){
					if((objStartDate[i].value == "") || (objEndDate[i].value == "")){
						
						if(objStartDate[i].value == ""){
							alert("<?=$i_SettingSemesterSetting_Alert1;?>");
							objStartDate[i].focus();
						}else{
							alert("<?=$i_SettingSemesterSetting_Alert2;?>");
							objEndDate[i].focus();
						}
							
						check_step1--;
						break;
						
					}else{
						
						if(check_date(objStartDate[i],"<?=$i_SettingSemesterSetting_Alert3;?>")){
							if(check_date(objEndDate[i],"<?=$i_SettingSemesterSetting_Alert3;?>")){
								valid_row.push(i);
								check_step1++;
							}else{
								break;
								check_step1--;
								return false;
							}
						}else{
							break;
							check_step1--;
							return false;
						}
					}
				}else{
					continue;
				}
			}
		}
		
		var tmp_start_check = new Array();
		var tmp_end_check = new Array();
		if(check_step1 > 0){
			if(valid_row.length > 1){
				for(j=0; j<valid_row.length; j++){
					var row_num = valid_row[j];
					if(j%2==0){
						tmp_end_check.push(objEndDate[valid_row[j]].value);
					}
					if(j%2==1){
						tmp_start_check.push(objStartDate[valid_row[j]].value);
					}
				}
			}
			
			if(tmp_end_check.length == tmp_start_check.length){
				for (x in tmp_end_check){
					if(compareDate(tmp_start_check[x],tmp_end_check[x])){
						check_passed = 1;
					}else{
						check_passed = 0;
						break;
					}
				}
			}
		
			if(check_passed == 1){
				return true;
			}else{
				alert("<?=$i_SettingSemesterSetting_Alert4;?>");
				return false;
			}
		}else{
			return false;
		}
	}
}*/

function checkform(obj){
	var objSemester = document.form1.elements['semester[]'];
	var objStartDate = document.form1.elements['start_date[]'];
	var objEndDate = document.form1.elements['end_date[]'];
	if(!check_text(obj.website, "<?php echo $i_alert_pleasefillin.$i_EmailWebsite; ?>.")){
		return false;
	}else{
	
		var login_attempt_limit = parseInt(document.form1.login_attempt_limit.value);
		if (isNaN(login_attempt_limit))
		{
			alert("<?=$Lang['login']['input_alert']?>");
			document.form1.login_attempt_limit.focus();
			return false;
		}
		var login_lock_duration = parseInt(document.form1.login_lock_duration.value);
		if (isNaN(login_lock_duration))
		{
			alert("<?=$Lang['login']['input_alert']?>");
			document.form1.login_lock_duration.focus();
			return false;
		}

		for(i=0; i<objSemester.length; i++)
		{
			if(objSemester[i].value != "")
			{
				if((objStartDate[i].value == "") || (objEndDate[i].value == ""))
				{
					if(objStartDate[i].value == "")
					{
						alert("<?=$i_SettingSemesterSetting_Alert1;?>");
						objStartDate[i].focus();
						return false;
					}
					if(objEndDate[i].value == "")
					{
							alert("<?=$i_SettingSemesterSetting_Alert2;?>");
							objEndDate[i].focus();
							return false;
					}
				}
				else
				{
					if(check_date(objStartDate[i],"<?=$i_SettingSemesterSetting_Alert3;?>") && check_date(objEndDate[i],"<?=$i_SettingSemesterSetting_Alert3;?>"))
					{
						var vaild = compareDate(objEndDate[i].value,objStartDate[i].value )
						
						if(vaild==1)
						{
							
						}
						else
						{
							alert("<?=$i_SettingSemesterSetting_Alert4;?>");
							return false;
						}
					}
					else
					{
						return false;	
					}
					
				}
			}
		}
		return true;
	}
}

function display_semester_warning()
{
	if(confirm('<?=$semester_change_warning?>'))
	{
		document.form1.action = ''; 
		document.form1.submit();
	}
	else
	{
		if(document.form1.SemesterMode.selectedIndex==1)
			document.form1.SemesterMode.selectedIndex = 0;
		else
			document.form1.SemesterMode.selectedIndex = 1;
	}
}
</script>

<form name="form1" action="update_all.php" method="post" onSubmit="return checkform(this);" enctype="multipart/form-data">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_basic_settings, '') ?>
<?= displayTag("head_basic_setting_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>

<p><br>
<table border=0 cellpadding=5 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_EmailWebsite; ?>:</td>
<td><input class=text type=text name=website size=40 maxlength=50 value="<?php echo $website; ?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_SettingsSchoolName; ?>:</td>
<td><input class=text type=text name=schoolname size=40 maxlength=100 value="<?php echo $school_name; ?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_SettingsOrganization; ?>:</td>
<td><input class=text type=text name=schoolorg size=40 maxlength=100 value="<?php echo $school_org; ?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_SettingsSchoolPhone; ?>:</td>
<td><input class=text type=text name=schoolphone size=20 maxlength=20 value="<?php echo $school_phone; ?>"></td></tr>
<tr><td align=right nowrap><?php echo $i_SettingsSchoolAddress; ?>:</td>
<td><textarea name="schooladdress" cols="50" rows="3"><?=$school_address?></textarea></td></tr>
<? /* ?>
<tr><td align=right nowrap><?php echo $i_SettingsCurrentAcademicYear; ?>:</td>
<td><input class=text type=text name=academic_yr size=10 maxlength=20 value="<?php echo $academic_yr; ?>"></td></tr>

<?
	$arr_semester_mode = array(array(1,"$i_SettingSemesterManualUpdate"),array(2,"$i_SettingSemesterAutoUpdate"));
	$selectSemesterMode = getSelectByArray($arr_semester_mode, " name=\"SemesterMode\" onChange=\"display_semester_warning();\" ",$SemesterMode,0,1);
?>
<tr><td align=right nowrap><?=$i_SettingSemesterMode;?>:</td><td><?=$selectSemesterMode;?></td></tr>
<tr><td align=right nowrap><?php echo $i_SettingsSemester; ?>:</td><td><?=$semester_table?></td></tr>
<? */ ?>
<tr><td align=right nowrap><?php echo $i_SettingsSchoolDayType; ?>:</td><td><?=$daySelect?></td></tr>
</table>

<br>
<hr size=1 class="hr_sub_separator">
<p>
<span class="extraInfo">[<span class=subTitle><?=$i_admintitle_sc_language?></span>]</span>
<div><input type=radio name=LangID value=0 <?php if($LangID==0) echo "CHECKED"; ?>><span class=tableColor><img src=../../images/en.gif border=0 align=absmiddle hspace=5 vspace=5></span></div>
<? if (in_array('b5',$intranet_default_lang_set)) { ?>
<div><input type=radio name=LangID value=1 <?php if($LangID==1) echo "CHECKED"; ?>><span class=tableColor><img src=../../images/b5.gif border=0 align=absmiddle hspace=5 vspace=5></span></div>
<? } ?>
<? if (in_array('gb',$intranet_default_lang_set)) { ?>
<div><input type=radio name=LangID value=2 <?php if($LangID==2) echo "CHECKED"; ?>><span class=tableColor><img src=../../images/gb.gif border=0 align=absmiddle hspace=5 vspace=5></span></div>
<? } ?>
</p>

<hr size=1 class="hr_sub_separator">

<p>
<span class="extraInfo">[<span class=subTitle><?=$i_adminmenu_basic_settings_badge?></span>]</span>
<table border=0 cellpadding=5 cellspacing=0>
<tr><td colspan="2"><input type=checkbox name=use_badge value=1 <?=$badge_checked?>> <?=$i_adminmenu_basic_settings_badge_use?></td></tr>
<tr><td>
<?php
if ($imgfile != "")
{
    echo "<img src='/file/$imgfile' width=120 height=60>\n";
}
?>
</td>
<td><input type=file name=badge_image length=30>
<br><span class="extraInfo"><?=$i_adminmenu_basic_settings_badge_instruction?></span>
</td>
</tr>
</table>
</p>

<!--
<hr size=1 class="hr_sub_separator">

<p>
<span class="extraInfo">[<span class=subTitle><?=$Lang['login']['control']?></span>]</span>
<div><?=$Lang['login']['attempt_limit']?>: <input type=text name="login_attempt_limit" value="<?=$login_attempt_limit?>" size=4 /> <?=$Lang['login']['unit_time']?></div>
<div><?=$Lang['login']['lock_duration']?>: <input type=text name="login_lock_duration" value="<?=$login_lock_duration?>" size=4 /> <?=$Lang['login']['unit_minute']?></div>
<span class="extraInfo">* <?= $Lang['login']['remark'] ?></span>
</p>
-->


<br />
<hr size=1 class="hr_sub_separator">

<p>
<span class="extraInfo">[<span class=subTitle><?=$i_Help_Show?></span>]</span>
<div><?=$i_Help_DateStart?>: <input type=text name=helpstart value='<?=$dates[0]?>' size=10> (YYYY-MM-DD)</div>
<div><?=$i_Help_DateEnd?>: <input type=text name=helpend value='<?=$dates[1]?>' size=10> (YYYY-MM-DD)</div>
</p>

<!--
<hr size=1 class="hr_sub_separator">

<p>
<span class="extraInfo">[<span class=subTitle><?=$i_adminmenu_basic_settings_misc?></span>]</span>
<div><input type=checkbox name=email_allowed value=1 <?=$email_checked?>> <?=$i_adminmenu_basic_settings_email?></div>
<div><input type=checkbox name=title_disabled value=1 <?=$title_checked?>> <?=$i_adminmenu_basic_settings_title?></div>
<div><input type=checkbox name=home_tel_allowed value=1 <?=$home_tel_checked?>> <?=$i_adminmenu_basic_settings_home_tel?></div>
</p>

-->

</blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>
