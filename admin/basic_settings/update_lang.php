<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");

switch ($LangID){
	case 0: $LangID = 0; break;
	case 1: $LangID = 1; break;
	case 2: $LangID = 2; break;
	default: $LangID = 0; break;
}

$li = new libfilesystem();
$li->file_write(htmlspecialchars(trim(stripslashes($LangID))), $intranet_root."/file/language.txt");

header("Location: index.php?msg=2");
?>
