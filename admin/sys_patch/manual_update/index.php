<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

# New Varialbes
$i_adminmenu_sc_eclass_update = "eClass Update";
$i_manual_update = "Manual Update";
$i_auto_update = "Auto Update";

?>


<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_eclass_update, '', $i_manual_update, '') ?>
<?= displayTag("head_group_setting_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300>

Latest IntranetIP Version: intranetI-P-testing.tar.gz.v41.20090209_r5<br>
Current IntranetIP Version: <font color=red>intranetI-P-testing.tar.gz.v41.20081217_r22</font><br>
<br><br>
Latest eClass Version: eclass3-1.tar.gz.v41.20081208_r7<br>
Current eClass Version: <font color=green>eclass3-1.tar.gz.v41.20081208_r7</font><br>
<br><br>
Download latest update now? 
<input type=button value="Confirm" onClick='location.href="download_update.php"'> <-- Admin can update system at any time, no matter the version is latest or not.
<br><br>
Your system have been updated to latest version already. <-- If no new update available

</td></tr>
</table>
<?
include_once("../../../templates/adminfooter.php");

?>