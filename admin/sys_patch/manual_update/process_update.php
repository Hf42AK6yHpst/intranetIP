<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

# New Varialbes
$i_adminmenu_sc_eclass_update = "eClass Update";
$i_manual_update = "Manual Update";
$i_auto_update = "Auto Update";

?>


<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_eclass_update, '', $i_manual_update, '') ?>
<?= displayTag("head_group_setting_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300>

Downloaded latest updates
<br><br>
IntranetIP Version: <br>intranetI-P-testing.tar.gz.v41.20090209_r5 [<font color=green>OK!</font>]<br><br>
eClass Version: <br>eclass3-1.tar.gz.v41.20081208_r7 [<font color=red>Error!</font>]
<br><br>
<b>Important</b>: Please make sure that you have eclass data backup before continue "eClass Update". eClass Update will start after you click "Update Now". 
Please don't turn off or reboot eClass Server until the update process complete.<br><br>
<input type=button value="Update Now" onClick='location.href="update_in_progress.php"'> <- if all downloaded files is "<font color=green>OK!</font>"<br><br>
or<br><br>
<b>Important</b>: Your eclass server do not able to connect Broadlearning Update Server at this moment. 
Please click "Retry" after 10 minutes.<br><br>

<input type=button value="Retry" onClick='location.href="index.php"'> <- if any downloaded files is "<font color=red>Error!</font>"<br><br>

</td></tr>
</table>
<?
include_once("../../../templates/adminfooter.php");

?>