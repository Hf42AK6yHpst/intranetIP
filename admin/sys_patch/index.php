<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

# New Varialbes
$i_adminmenu_sc_eclass_update = "eClass Update";
$i_manual_update = "Manual Update";
$i_auto_update = "Auto Update";
$i_update_report = "Update Report";

?>


<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_eclass_update, '') ?>
<?= displayTag("head_group_setting_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300>
<blockquote>
<?= displayOption($i_manual_update, '/admin/sys_patch/manual_update/', $li_menu->is_access_groupcategory,
				$i_auto_update, '/admin/sys_patch/auto_update/', $li_menu->is_access_role,
				$i_update_report, '/admin/sys_patch/update_report/', $li_menu->is_access_role ) ?>
</blockquote>
</td></tr>
</table>
<?
include_once("../../templates/adminfooter.php");

?>