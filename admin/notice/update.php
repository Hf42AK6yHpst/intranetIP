<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libnotice.php");

$lnotice = new libnotice();
$setting_file = $lnotice->setting_file;

$content = "";
$content .= "$disabled\n";			# Disable E-Notice
$content .= "$fullGroup\n";			# Advanced Control Group
$content .= "$normalGroup\n";		# Normal Control Group
$content .= "$classteacher\n";		# Disable Class teacher right to edit replies for parents.
$content .= "$allallow\n";			# All staff can issue notices.
$content .= "$numDays\n";			# Default no. of days for returning notice.
$content .= "$enableViewAll\n";		# Allow all parents/students to view all notices.

# 20090306 yat
################################################
# about the module notice batch printing
# 1. set group access right 
# 2. remember to update libnotice.php to add this access right
################################################
$content .= "$DisciplineGroup";			# Discipline Notice Batch Printing Group

$li = new libfilesystem();
$li->file_write($content, $setting_file);
header("Location: settings.php?msg=2");
?>