<?php
include_once("../../includes/global.php");

if (!$plugin['webmail'] && !$plugin['personalfile'])
{
     header("/admin/main_setting.php");
     exit();
}

include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

if ($plugin['webmail'] && $webmail_SystemType==1)
{
    $x .= "<li>$i_LinuxAccount_Webmail</li>\n";
}
else if ($plugin['webmail'] && $webmail_SystemType==3)
{
     $x .= "<li>$i_LinuxAccount_Campusmail</li>\n";
}

if ($plugin['personalfile'] && $personalfile_type == 'LOCAL_FTP' && $personalfile_account_management)
{
    $x .= "<li>$i_LinuxAccount_PersonalFile</li>\n";
}

$x = "<ol>\n$x\n</ol>\n";

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_LinuxAccountQuotaSetting, '') ?>
<?= displayTag("head_club_enrollment_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300>
<blockquote>
<?=$i_LinuxAccount_Quota_Description?><br>
<?=$x?>
<?= displayOption($i_LinuxAccount_SetDefaultQuota, 'default.php', 1,
                                $i_LinuxAccount_SetUserQuota, 'user.php', 1,
                                $i_LinuxAccount_DisplayQuota, 'list.php', 1
                                ) ?>
</blockquote>
</td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>