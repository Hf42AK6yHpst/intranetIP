<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_setting.php");

if ($upload ==1)
{
    $upload_msg = $i_sls_library_mapping_file_upload_success;
}
?>
<?=$upload_msg?>

<form action="update.php" method="post" enctype="multipart/form-data" name="form1">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_plugin_sls_library, '') ?>
<?= displayTag("head_sls_library_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<blockquote>
<p><br>
<span class="extraInfo">[<span class=subTitle><?=$i_sls_library_mapping_file?></span>]</span>
<table border=0 cellpadding=5 cellspacing=0>
<tr>
<td><?=$i_sls_library_mapping_file_select?>
<br><input type=file name=slsfile length=60>
</td>
</tr>
<tr>
<td><?=$i_sls_library_mapping_file_instruction?>
</td>
</tr>
<tr>
<td><span class="extraInfo"><?=$i_sls_library_mapping_file_use?></span>
</td>
</tr>
<tr>
<td><a class="functionlink_new" href="/admin/plugconf/slsMap.txt"><?=$i_sls_library_mapping_file_download?></a>
</td>
</tr>
</table>
</blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>