<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");

$li = new libfilesystem();
$file_end = ($type==0?"_timetable.csv":"_timetable_special.csv");
$ext = strtoupper($li->file_ext($userfile_name));

if ($ext != ".CSV")
{
    header ("Location: index.php?GroupID=$GroupID&msg=13");
}
else
{
    if(isset($GroupID)){
       $url = "/file/timetable/g".$GroupID.$file_end;
       if($userfile=="none"){
       } else {
         $li->lfs_copy($userfile, $intranet_root.$url);
       }
    }
    if ($type == 0)
    {
        $li->lfs_remove("/file/timetable/g".$GroupID."_timetable_special.csv");
    }
    header("Location: index.php?GroupID=$GroupID&msg=10");
}
?>