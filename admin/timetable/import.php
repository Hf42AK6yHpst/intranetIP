<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libgrouping.php");
include("../../includes/libresource.php");
include("../../includes/libfilesystem.php");
include("../../includes/libtimetablegroup.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();

$li = new libgrouping();
$GroupID = (isset($GroupID)) ? $GroupID : $li->returnFirstGroup();

?>

<form name="form1" action="import_update.php" method="post" enctype="multipart/form-data">
<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_groupfunction, '/admin/groupfunction/', $i_admintitle_im_timetable, 'javascript:history.back()', $button_import, '') ?>
<?= displayTag("head_group_timetable_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?= $i_Attendance_ImportSelect ?>:</td>
<td><input class=file type=file name=userfile size=25>
<?
if(!$g_encoding_unicode)
		{
		echo "<br />( $i_import_utf_type )";
		}
?>
</td></tr>
<tr><td>&nbsp;</td><td>
<p><input type=radio name=type value="0" checked><?=$i_Timetable_type0?></p>
<input type=radio name=type value="1"><?=$i_Timetable_type1?>
</td></tr>
</table>
</blockquote>
<input type="hidden" name="GroupID" value="<?=$GroupID?>" >

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>