<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgrouping.php");
include_once("../../includes/libresource.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libtimetablegroup.php");
include_once("../../includes/libbatch.php");
include_once("../../includes/libaccount.php");
intranet_opendb();

if(isset($GroupID)){
     $lb = new libbatch();
     $row = $lb->slots;
     $lu = new libtimetablegroup();
     $lu->setGroupID($GroupID);
     $lu->setRow($row);
     $lu->setTimetable($data,$rows,$cols);
}

intranet_closedb();
header("Location: index.php?GroupID=$GroupID&msg=2");
?>