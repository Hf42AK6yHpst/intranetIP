<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libgrouping.php");
include("../../includes/libfilesystem.php");
include("../../includes/libtimetablegroup.php");
include("../../includes/libbatch.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
include("../../includes/libimporttext.php");
intranet_opendb();

$li = new libgrouping();
$limport = new libimporttext();
$GroupID = (isset($GroupID)) ? $GroupID : $li->returnFirstGroup();

$lb = new libbatch();
$slots = $lb->slots;
for ($i=0; $i<sizeof($slots); $i++)
     $row[$i] = $slots[$i][2]."<br>\n".$slots[$i][1];

$lu = new libtimetablegroup();
$lu->setGroupID($GroupID);
$lu->setRow($row);
$lu->setMode(1);

// TABLE FUNCTION BAR
$selection .= "<select name=GroupID onChange=\"this.form.method='get';this.form.action='edit.php';this.form.submit();\">\n";
$selection .= $li->displayGroupsSelection($GroupID);
$selection .= "</select>\n";
?>

<form action="edit_update.php" name="form1" method="post">
<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_groupfunction, '/admin/groupfunction/', $i_admintitle_im_timetable, 'index.php', $button_edit, '') ?>
<?= displayTag("head_group_timetable_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $selection ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td colspan=2 class=admin_bg_menu><?php echo $lu->display(); ?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="35" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="index.php?GroupID=<?=$GroupID?>"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>