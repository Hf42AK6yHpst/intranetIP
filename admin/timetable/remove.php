<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");

$li = new libfilesystem();

if(isset($GroupID)){
     $url = "/file/timetable/g".$GroupID."_timetable.csv";
     $li->lfs_remove($intranet_root.$url);
     $url = "/file/timetable/g".$GroupID."_timetable_special.csv";
     $li->lfs_remove($intranet_root.$url);
}

header("Location: index.php?GroupID=$GroupID&msg=3");
?>