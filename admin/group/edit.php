<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libalbum.php");
include_once("../../includes/liborganization.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

$li = new libgroup($GroupID[0]);
$lgc = new libgroupcategory($li->RecordType);
$lorg = new liborganization();
$la = new libalbum();
$checked = ($li->AnnounceAllowed==1? "CHECKED":"");
$allToolsChecked = ($li->isAccessAllTools()? "CHECKED":"");
$availableTools = $li->getSelectCurrentAvailableFunctions();
$hideChked = ($lorg->isGroupHidden($GroupID[0])? "CHECKED":"");
?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_GroupTitle; ?>.")) return false;
     if(!check_positive_int(obj.Quota,"<?=$i_GroupQuotaIsInt?>")) return false;
}
function allToolsChecked(obj)
{
         var val;
         var i=0;
         len=obj.elements.length;
         if (obj.alltools.checked)
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouptools[]')
                  {
                      obj.elements[i].disabled=true;
                      obj.elements[i].checked=true;
                  }
             }
         }
         else
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouptools[]')
                  {
                      obj.elements[i].disabled=false;
                  }
             }
         }
}
</script>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_group, 'javascript:history.back()', $button_edit, '') ?>
<?= displayTag("head_group_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right><?php echo $i_GroupTitle; ?>:</td><td><input class=text type=text name=Title size=30 maxlength=100 value="<?php echo $li->Title; ?>"></td></tr>
<tr><td align=right><?php echo $i_GroupDescription; ?>:</td><td><textarea name=Description cols=30 rows=5><?php echo $li->Description; ?></textarea></td></tr>
<tr><td align=right><?php echo $i_GroupQuota; ?>:</td><td><input class=text type=text name=Quota size=5 maxlength=5 value="<?=$li->StorageQuota?>"></td></tr>
<tr><td align=right><?php echo $i_AlbumQuota; ?>:</td><td><input class=text type=text name=AlbumQuota size=5 maxlength=5 value="<?=$la->returnStorageQuota($GroupID[0])?>"></td></tr>
<tr><td align=right><?php echo $i_AlbumAccessType; ?>:</td><td><?php echo $la->returnSelectAccessType("name=AllowedAccessType",true,0,$la->returnAlowedAccessType($GroupID[0])); ?></td></tr>

<? if ($special_announce_public_allowed) { ?>
<tr><td align=right><?php echo $i_GroupAnnounceRight; ?>:</td><td><input TYPE=checkbox NAME=AnnounceAllowed VALUE=1 <?=$checked?>></td></tr>
<? } ?>
<tr><td align=right><?php echo $i_OrganizationPage_HideInOrganization; ?>:</td><td><input TYPE=checkbox NAME=hide VALUE=1 <?=$hideChked?>></td></tr>
<tr><td align=right><?php echo $i_GroupRecordType; ?>:</td><td><?php echo (($li->RecordStatus==1 || $li->RecordStatus==3) ? $lgc->CategoryName." <input type=hidden name=RecordType value='".$li->RecordType."'>" : $lgc->returnSelectCategory("name=RecordType",true,0,$li->RecordType) ); ?></td></tr>
<tr><td align=right><?php echo $i_GroupUseAllTools; ?>:</td><td><input TYPE=checkbox NAME=alltools VALUE=1 <?=$allToolsChecked?> onClick="allToolsChecked(this.form)"></td></tr>
<tr><td align=right><?=$i_GroupToolsAllowed?>:</td><td><?=$availableTools?></td></tr>
</table>
</blockquote>
<input type=hidden name=GroupID value="<?php echo $li->GroupID; ?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>