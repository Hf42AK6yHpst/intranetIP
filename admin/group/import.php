<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

$linterface = new interface_html();
$limport = new libimporttext();

$filter = ($filter == 0) ? 1 : $filter;
?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_GroupTitle; ?>.")) return false;
}
</script>

<form name="form1" action="import_update.php" method="post" enctype="multipart/form-data">
<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_group, 'javascript:history.back()', $button_import, '') ?>
<?= displayTag("head_group_$intranet_session_language.gif", $msg) ?>


<blockquote>
<table width=400 border=0 cellpadding=2 cellspacing=1>
<tr><td align=right><?php echo $i_groupimport_msg; ?>:</td><td><input class=file type=file name=userfile size=25><br />
<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?></td></tr>
<tr><td align=right><?php echo $i_groupimport_type; ?>:</td><td><?php echo intranet_group_role($filter,1); ?></td></tr>
<tr><td colspan=2><p>
<span class=extraInfo><?=$i_groupimport_fileformat?>:</span><br><?=$i_groupimport_fileformat2?><br>
<a class=functionlink_new href="<?= GET_CSV("groupsample.csv")?>"><?=$i_groupimport_sample?></a>
</p></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?> 
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>