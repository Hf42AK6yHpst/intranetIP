<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/liborganization.php");
intranet_opendb();

$li = new libdb();

$Title = intranet_htmlspecialchars(trim($Title));
$Description = intranet_htmlspecialchars(trim($Description));
$Quota = intranet_htmlspecialchars(trim($Quota));
if (!is_numeric($Quota) || $Quota < 0) $Quota = 5;

if ($alltools == 1)
{
    $functionAccess = 'NULL';
}
else
{
    # Change the tool rights to integer
    $sum = 0;
    for ($i=0; $i<sizeof($grouptools); $i++)
    {
         $target = intval($grouptools[$i]);
         if ($target == 5)        # Question bank is for academic only
         {
             if ($RecordType == 2)
             {
                 $sum += pow(2,$target);
             }
         }
         else
         {
             $sum += pow(2,$target);
         }
    }
    $functionAccess = $sum;
}

$fieldname = "Title, Description, RecordType, DateInput, DateModified, StorageQuota, FunctionAccess";
$fieldvalue = "'$Title', '$Description', '$RecordType', now(), now(), $Quota, $functionAccess";
if ($AnnounceAllowed == 1)
{
    $fieldname .= ",AnnounceAllowed";
    $fieldvalue .= ",1";
}
$sql = "INSERT INTO INTRANET_GROUP ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);
$GroupID = $li->db_insert_id();

# Match with INTRANET_CLASS
if ($RecordType == 3)
{
    $sql = "UPDATE INTRANET_CLASS SET GroupID = $GroupID WHERE ClassName = '$Title'";
    $li->db_db_query($sql);
}

# INTRANET_ORPAGE_GROUP
$lorg = new liborganization();
if ($hide==1)
{
    $lorg->setGroupHidden($GroupID);
}

intranet_closedb();
header("Location: index.php?filter=$RecordType&msg=1");
?>