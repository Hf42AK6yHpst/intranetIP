<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libgroup.php");
intranet_opendb();

$li = new libdb();

$lgroup = new libgroup($GroupID);
$CalID = $lgroup->CalID;

if(sizeof($ChooseUserID) <> 0){
     $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID) VALUES ";
     for($i=0;$i<sizeof($ChooseUserID);$i++){
          $sql .= ($i==0) ? "" : ",";
          $sql .= "($GroupID, ".$ChooseUserID[$i].", $RoleID)";
          
			##############################################
			## START - add CALENDAR_CALENDAR_VIEWER
			##############################################
			$cal_sql = "
				insert into CALENDAR_CALENDAR_VIEWER 
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
				values 
				($CalID, ".$ChooseUserID[$i].", $GroupID, 'E', 'R', 'FFFFFF', '1')
			";
			$li->db_db_query($cal_sql);
			##############################################
			## END - add CALENDAR_CALENDAR_VIEWER
			##############################################
     }
     $li->db_db_query($sql);
     $li->UpdateRole_UserGroup();
     
     
     
}
intranet_closedb();
?>
<body onLoad="document.form1.submit();opener.window.location=opener.window.location;">
<form name=form1 action="add.php" method="get">
<input type=hidden name=GroupID value="<?php echo $GroupID; ?>">
<input type=hidden name=RoleID value="<?php echo $RoleID; ?>">
</form>
</body>