<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbdump.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lexport = new libexporttext();

#ClassName        ClassNumber        EnglishName        Chinese Name        Post        UserLogin        UserEmail

# Export default format

$sql = "SELECT b.ClassName, b.ClassNumber, b.EnglishName, b.ChineseName,c.Title,b.UserLogin,b.UserEmail
        FROM INTRANET_USERGROUP as a
               LEFT OUTER JOIN INTRANET_USER as b ON a.GroupID = $GroupID AND a.UserID = b.UserID
               LEFT OUTER JOIN INTRANET_ROLE as c ON a.RoleID = c.RoleID
             WHERE a.GroupID = $GroupID
        ";

$li = new libdb();
$members = $li->returnArray($sql,7);

$x = "ClassName,ClassNumber,EnglishName,ChineseName,Role,UserLogin,UserEmail\n";
$exportColumn = array("ClassName","ClassNumber","EnglishName","ChineseName","Role","UserLogin","UserEmail");
/*
for ($i=0; $i<sizeof($members); $i++)
{
     for($j=0; $j<sizeof($members[$i]); $j++)
     {
         $members[$i][$j] = "\"".intranet_undo_htmlspecialchars($members[$i][$j])."\"";
     }
     $x .= implode(",", $members[$i])."\n";
}

$url = "/file/export/eclass-groupuser-".session_id()."-".time().".csv";
$lo = new libfilesystem();
$lo->file_write($x, $intranet_root.$url);
*/

// Output the file to user browser
#$filename = "eclass-groupuser-".session_id()."-".time().".csv";
$filename = "eclass-groupuser.csv";

//output2browser($x, $filename);

$export_content = $lexport->GET_EXPORT_TXT($members, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
#header("Location: $url");
?>