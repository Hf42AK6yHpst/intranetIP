<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libgroup.php");
intranet_opendb();

$li = new libdb();

$lgroup = new libgroup($GroupID);
$CalID = $lgroup->CalID;

if (is_array($UserID) && sizeof($UserID)!=0)
{
    $sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID = $GroupID AND UserID IN (".implode(",", $UserID).")";
    $li->db_db_query($sql);
    
    ##############################################
	## START - delete CALENDAR_CALENDAR_VIEWER
	##############################################
	$cal_sql = "delete from CALENDAR_CALENDAR_VIEWER where CalID=$CalID and UserID IN (".implode(",", $UserID).")";
	$li->db_db_query($cal_sql);
	##############################################
	## END - delete CALENDAR_CALENDAR_VIEWER
	##############################################
}

intranet_closedb();
header("Location: index.php?GroupID=$GroupID&filter=$filter&msg=3");
?>