<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libgroup.php");
include("../../../includes/libgrouping.php");
include("../../../lang/lang.$intranet_session_language.php");
include("../../../templates/fileheader_admin.php");
intranet_opendb();

# retrieve role type

$lo = new libgroup($GroupID);

$row = $lo->returnRoleType();
$RoleOptions .= "<select name=RoleID>\n";
for($i=0; $i<sizeof($row); $i++)
$RoleOptions .= (Isset($RoleID)) ? "<option value=".$row[$i][0]." ".(($row[$i][0]==$RoleID)?"SELECTED":"").">".$row[$i][1]."</option>\n" : "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
$RoleOptions .= "</select>\n";

# retrieve group category

$li = new libgrouping();

if($CatID > 10){
     unset($ChooseGroupID);
     $ChooseGroupID[0] = $CatID-10;
}

$x1  = ($CatID!=0 && $CatID < 10) ? "<select name=CatID onChange=checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()>\n" : "<select name=CatID onChange=this.form.submit()>\n";
$x1 .= "<option value=0></option>\n";
$x1 .= "<option value=3 ".(($CatID==3)?"SELECTED":"").">".$i_GroupRole[3]."</option>\n";
$x1 .= "<option value=1 ".(($CatID==1)?"SELECTED":"").">".$i_GroupRole[1]."</option>\n";
$x1 .= "<option value=2 ".(($CatID==2)?"SELECTED":"").">".$i_GroupRole[2]."</option>\n";
$x1 .= "<option value=4 ".(($CatID==4)?"SELECTED":"").">".$i_GroupRole[4]."</option>\n";
$x1 .= "<option value=5 ".(($CatID==5)?"SELECTED":"").">".$i_GroupRole[5]."</option>\n";
$x1 .= "<option value=6 ".(($CatID==6)?"SELECTED":"").">".$i_GroupRole[6]."</option>\n";
$x1 .= "<option value=0>";
for($i = 0; $i < 20; $i++)
$x1 .= "_";
$x1 .= "</option>\n";
$x1 .= "<option value=0></option>\n";
/*
$row = $li->returnCategoryGroups(0);
for($i=0; $i<sizeof($row); $i++){
     $IdentityCatID = $row[$i][0] + 10;
     $IdentityCatName = $row[$i][1];
     $x1 .= "<option value=$IdentityCatID ".(($IdentityCatID==$CatID)?"SELECTED":"").">$IdentityCatName</option>\n";
}
*/
$x1 .= "<option value=11 ".(($CatID==11)?"SELECTED":"").">$i_identity_teachstaff</option>\n";
$x1 .= "<option value=12 ".(($CatID==12)?"SELECTED":"").">$i_identity_student</option>\n";
$x1 .= "<option value=13 ".(($CatID==13)?"SELECTED":"").">$i_identity_parent</option>\n";

$x1 .= "<option value=0></option>\n";
$x1 .= "</select>";

if($CatID!=0 && $CatID < 10) {
     $row = $li->returnCategoryGroups($CatID);
     $x2  = "<select name=ChooseGroupID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
          $GroupCatID = $row[$i][0];
          $GroupCatName = $row[$i][1];
          if ($GroupCatID != $GroupID)
          {
              $x2 .= "<option value=$GroupCatID";
              for($j=0; $j<sizeof($ChooseGroupID); $j++){
                  $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
              }
              $x2 .= ">$GroupCatName</option>\n";
          }
     }
     $x2 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x2 .= "&nbsp;";
     $x2 .= "</option>\n";
     $x2 .= "</select>\n";
}

if($CatID > 10){
   # Return users with identity chosen
   $selectedUserType = $CatID - 10;
   $row = $li->returnUserForTypeExcludeGroup($selectedUserType,$GroupID);

     $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}
else if(isset($ChooseGroupID)) {
     $row = $li->returnGroupUsersExcludeGroup($ChooseGroupID, $GroupID);
     $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}
?>
<script language="JavaScript1.2">
function add_role(obj){
     role_name = prompt("", "New_Role");
     if(role_name!=null && Trim(role_name)!=""){
          obj.role.value = role_name;
          obj.action = "add_role.php";
          obj.submit();
     }
}
function add_user(obj){
     obj.action = "add_user.php";
     obj.submit();
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

</script>

<form name=form1 action=add.php method=post>
<p class=admin_head><?php echo $i_admintitle_user." ".$button_import; ?></p>
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src=../../../images/admin/popup_top.gif width=422 height=14 border=0></td></tr>
<tr><td class=admin_bg_menu><span class=admin_head><?php echo $lo->displayGroupIcon().$lo->Title; ?></span></td></tr>
<tr><td><img src=../../../images/admin/menu_bottom.gif width=422 height=8 border=0></td></tr>
</table>
<table width=422 border=0 cellpadding=10 cellspacing=0>
<tr><td class=tableContent><?php echo $RoleOptions; ?> <input class=button type=button value="<?php echo $button_new; ?> <?php echo $i_admintitle_role; ?>" onClick=add_role(this.form)></td></tr>
</table>
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td class=tableContent><img src=../../../images/admin/headline.gif width=422 height=16 border=0></td></tr>
</table>
<table width=422 border=0 cellpadding=10 cellspacing=0>
<tr><td class=tableContent>
<p><?php echo $i_frontpage_campusmail_select_category; ?>:
<br><?php echo $x1; ?>
<?php if($CatID!=0 && $CatID < 10) { ?>
<p><?php echo $i_frontpage_campusmail_select_group; ?>:
<br><?php echo $x2; ?>
<input class=submit type=submit value="<?php echo $i_frontpage_campusmail_expand; ?>" onClick=checkOption(this.form.elements["ChooseGroupID[]"]);>
<?php } ?>
<?php if(isset($ChooseGroupID)) { ?>
<p><?php echo $i_frontpage_campusmail_select_user; ?>:
<br><?php echo $x3; ?>
<input class=button type=button value="<?php echo $button_add; ?>" onClick=checkOption(this.form.elements["ChooseUserID[]"]);add_user(this.form)><input class=button type=button onClick="SelectAll(this.form.elements['ChooseUserID[]'])" value="<?=$button_select_all?>">
<?php } ?>
</td>
</tr>
<tr><td align=right><input class=button type=button value="<?php echo $button_close; ?>" onClick=self.close()></td></tr>
</table>
<input type=hidden name=GroupID value="<?php echo $GroupID; ?>">
<input type=hidden name=role>
</form>

<?php
intranet_closedb();
include("../../../templates/filefooter.php");
?>