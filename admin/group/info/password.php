<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libgroup.php");
include("../../../includes/libfilesystem.php");
include("../../../includes/libaccount.php");
include("../../../lang/lang.$intranet_session_language.php");
include("../../../templates/adminheader_intranet.php");
intranet_opendb();

$li = new libgroup($GroupID);
?>

<form name="form1">
<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_group, "../index.php?filter=$filter", $lg->Title, 'javascript:history.back()', $i_frontpage_schoolinfo_groupinfo_group_members, '') ?>
<?= displayTag("head_group_$intranet_session_language.gif", $msg) ?>

<blockquote><blockquote>
<p>
<textarea name=message cols=55 rows=15 wrap=off>
<?php echo $li->displayGroupUsersPassword(); ?>
</textarea>
<p>
</blockquote></blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include("../../../templates/adminfooter.php");
?>