<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libwebsamsattendcode.php");
include_once($PATH_WRT_ROOT."includes/libSimpleXML.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");

echo displayNavTitle($i_adminmenu_adm, '', $i_StudentRegistry['System'], 'index.php',$button_import_xml,'');
echo displayTag("head_registry_$intranet_session_language.gif", $msg);

//error_reporting(E_ALL);

intranet_opendb();

//$limport = new libimporttext();

$page_check = array();
$page_check['EnglishNameUpper'] = true;
$page_check['SP_ValidDate_Not_Null_For_Type_2_3'] = true;
$page_check['CountryText_For_Code_0_Only'] = true;
$page_check['R_AreaText_For_Code_O_Only'] = true;
$page_check['R_AreaText_Not_Null_For_Code_O'] = true;
$page_check['EC_NullOrFillCertain6Fields'] = true;


$li = new libdb();
//$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

$lwebsams_code = new libwebsamsattendcode();
$lwebsams_code->retrieveBasicInfo();
$SchoolID = $lwebsams_code->SchoolID;

//$format = $format==""?1:$format;

//$file_format1 = array("Class Name","Class Number","Amount");
//$file_format2 = array("User Login","Amount");

if($filepath=="none" || $filepath == ""){ # import failed
	header("Location: import_xml.php?failed=2");
	exit();
}
else{
	$lsx = new libSimpleXML();
	$lsx->loadXMLFile($filepath);
	$data = $lsx->getXMLData();
	
	$StudentData = isset($data['SRA'][0]['STUDENT']) ? $data['SRA'][0]['STUDENT'] : array();
	
	$validRecordRows = array();
	$invalidRecordRows = array();
	for($i=0;$i<sizeof($StudentData);$i++)
	{
		$WebSAMSRegNo = isset($StudentData[$i]['STUD_ID'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['STUD_ID'][0]['data'])) : '';
		$Code = isset($StudentData[$i]['CODE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['CODE'][0]['data'])) : '';
		$ChineseName = isset($StudentData[$i]['NAME_C'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['NAME_C'][0]['data'])) : '';
		$EnglishName = isset($StudentData[$i]['NAME_P'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['NAME_P'][0]['data'])) : '';

		if($page_check['EnglishNameUpper'])
			$EnglishName = strtoupper($EnglishName);//may consider mb_strtoupper if encoding confirmed

		$Gender = isset($StudentData[$i]['SEX'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['SEX'][0]['data'])) : '';
		$DateOfBirth = isset($StudentData[$i]['B_DATE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['B_DATE'][0]['data'])) : '';
		$PlaceOfBirthCode = isset($StudentData[$i]['B_PLACE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['B_PLACE'][0]['data'])) : '';
		$ID_Type = isset($StudentData[$i]['ID_TYPE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['ID_TYPE'][0]['data'])) : '';
		$ID_Num = isset($StudentData[$i]['ID_NO'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['ID_NO'][0]['data'])) : '';
		$ID_IssuePlace = isset($StudentData[$i]['I_PLACE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['I_PLACE'][0]['data'])) : '';
		$ID_IssueDate = isset($StudentData[$i]['I_DATE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['I_DATE'][0]['data'])) : '';
		$ID_ValidDate = isset($StudentData[$i]['V_DATE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['V_DATE'][0]['data'])) : '';
		$SP_Type = isset($StudentData[$i]['S6_TYPE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['S6_TYPE'][0]['data'])) : '';
		$SP_IssueDate = isset($StudentData[$i]['S6_IDATE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['S6_IDATE'][0]['data'])) : '';
		$SP_ValidDate = isset($StudentData[$i]['S6_VDATE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['S6_VDATE'][0]['data'])) : '';
		$CountryCode = isset($StudentData[$i]['NATION'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['NATION'][0]['data'])) : '';
		$Province = isset($StudentData[$i]['ORIGIN'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['ORIGIN'][0]['data'])) : '';
		$Tel = isset($StudentData[$i]['TEL'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['TEL'][0]['data'])) : '';
		$R_AreaCode = isset($StudentData[$i]['R_AREA'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['R_AREA'][0]['data'])) : '';
		$R_AreaText = isset($StudentData[$i]['RA_DESC'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['RA_DESC'][0]['data'])) : '';
		$AreaCode = isset($StudentData[$i]['AREA'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['AREA'][0]['data'])) : '';
		$Road = isset($StudentData[$i]['ROAD'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['ROAD'][0]['data'])) : '';
		$Address = isset($StudentData[$i]['ADDRESS'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['ADDRESS'][0]['data'])) : '';
		$FatherName = isset($StudentData[$i]['FATHER'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['FATHER'][0]['data'])) : '';
		$MotherName = isset($StudentData[$i]['MOTHER'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['MOTHER'][0]['data'])) : '';
		$FatherOccupation = isset($StudentData[$i]['F_PROF'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['F_PROF'][0]['data'])) : '';
		$MotherOccupation = isset($StudentData[$i]['M_PROF'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['M_PROF'][0]['data'])) : '';
		$GuardianType = isset($StudentData[$i]['GUARD'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['GUARD'][0]['data'])) : '';
		$GuardianIsLiveTogether = isset($StudentData[$i]['LIVE_SAME'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['LIVE_SAME'][0]['data'])) : '';
		$EmergencyContactName = isset($StudentData[$i]['EC_NAME'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['EC_NAME'][0]['data'])) : '';
		$EmergencyContact_RelationString = isset($StudentData[$i]['EC_REL'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['EC_REL'][0]['data'])) : '';
		$EmergencyContact_Tel = isset($StudentData[$i]['EC_TEL'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['EC_TEL'][0]['data'])) : '';
		$EmergencyContact_AreaCode = isset($StudentData[$i]['EC_AREA'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['EC_AREA'][0]['data'])) : '';
		$EmergencyContact_Road = isset($StudentData[$i]['EC_ROAD'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['EC_ROAD'][0]['data'])) : '';
		$EmergencyContact_Address = isset($StudentData[$i]['EC_ADDRESS'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['EC_ADDRESS'][0]['data'])) : '';
		$SCode = isset($StudentData[$i]['S_CODE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['S_CODE'][0]['data'])) : '';
		$Grade = isset($StudentData[$i]['GRADE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['GRADE'][0]['data'])) : '';
		$WebSAMSClassCode = isset($StudentData[$i]['CLASS'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['CLASS'][0]['data'])) : '';
		$ClassNumber = isset($StudentData[$i]['C_NO'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['C_NO'][0]['data'])) : '';
		$OM_GuardianName = isset($StudentData[$i]['G_NAME'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['G_NAME'][0]['data'])) : '';
		$OM_Guardian_RelationString = isset($StudentData[$i]['G_RELATION'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['G_RELATION'][0]['data'])) : '';
		$OM_Guardian_Occupation = isset($StudentData[$i]['G_PROFESSION'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['G_PROFESSION'][0]['data'])) : '';
		$Guardian_AreaCode = isset($StudentData[$i]['G_AREA'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['G_AREA'][0]['data'])) : '';
		$Guardian_Road = isset($StudentData[$i]['G_ROAD'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['G_ROAD'][0]['data'])) : '';
		$Guardian_Address = isset($StudentData[$i]['G_ADDRESS'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['G_ADDRESS'][0]['data'])) : '';
		$Guardian_Tel = isset($StudentData[$i]['G_TEL'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['G_TEL'][0]['data'])) : '';
		$Guardian_Mobile = isset($StudentData[$i]['GUARDMOBILE'][0]['data']) ? intranet_htmlspecialchars(trim($StudentData[$i]['GUARDMOBILE'][0]['data'])) : '';

		# must update? (expects existing student only at the moment, no insert)
		# identify the user_recordid first, by WebSAMSRegNo/Code
    if($WebSAMSRegNo!=""){
      if(substr($WebSAMSRegNo,0,1)!="#")
        $WebSAMSRegNo = "#".$WebSAMSRegNo;
    	$sql =	"
								SELECT
									UserID,
									EnglishName,
									ChineseName,
									Gender,
									DateOfBirth
								FROM
									INTRANET_USER
								WHERE
									WebSAMSRegNo = '$WebSAMSRegNo' AND
									RecordType=2
							";
      $r = $li->returnArray($sql);
    }

		$UserID = isset($r[0]['UserID']) ? $r[0]['UserID'] : '';
		$ChineseName = $ChineseName != "" ? $ChineseName : $r[0]['ChineseName'];
		$EnglishName = $EnglishName != "" ? $EnglishName : $r[0]['EnglishName'];
		$Gender = $Gender != "" ? $Gender : $r[0]['Gender'];
		$DateOfBirth = $DateOfBirth != "" ? $DateOfBirth : substr($r[0]['DateOfBirth'],0,10);

		$sql =	"
							SELECT
								ic.ClassName
							FROM
								INTRANET_CLASSLEVEL icl
							INNER JOIN
								INTRANET_CLASS ic
							ON
								ic.ClassLevelID = icl.ClassLevelID
							WHERE
								icl.WebSAMSLevel = '$Grade' AND
								ic.WebSAMSClassCode = '$WebSAMSClassCode'
						";
		$r = $li->returnArray($sql);
		$ClassName = isset($r[0]['ClassName']) ? $r[0]['ClassName'] : '';

		$local_result = array();	
		$error1 = $UserID ? false : true;//user found
		$error2 = ($SchoolID != $SCode);
		$error3 = $ClassName ? false : true;//class found
		$local_result['User'] = !$error1;
		$local_result['School'] = !$error2;
		$local_result['Class'] = !$error3;

		$HiddenEntry = '';
		$RecordRow = '';
		if($error1){
			$RecordRow .= '<td nowrap><span style="color:red">';
      $RecordRow .= $WebSAMSRegNo != "" ? $WebSAMSRegNo:"-";
      $RecordRow .= '</span></td>';
		}
		else{
			$RecordRow .= '<td nowrap>'.$WebSAMSRegNo.'&nbsp;</td>';
			$HiddenEntry .= '<textarea name="UserID[]" style="display:none">'.$UserID.'</textarea>';
		}
		$separatingIdx = strpos($Code, '-');
		if($separatingIdx!==FALSE){
			$AuthCodeMain = substr($Code,0,$separatingIdx);
			$AuthCodeCheck = substr($Code, $separatingIdx+1);
		}
		else{
			$AuthCodeMain = $Code;
			$AuthCodeCheck = '';
		}

		$RecordRow .= '<td nowrap>'.$Code.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="AuthCodeMain[]" style="display:none">'.$AuthCodeMain.'</textarea>';
		$HiddenEntry .= '<textarea name="AuthCodeCheck[]" style="display:none">'.$AuthCodeCheck.'</textarea>';
		$RecordRow .= '<td nowrap>'.$ChineseName.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="ChineseName[]" style="display:none">'.$ChineseName.'</textarea>';
		$RecordRow .= '<td nowrap>'.$EnglishName.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="EnglishName[]" style="display:none">'.$EnglishName.'</textarea>';

		# locate the grade/class pair
		if($error2 || $error3){
			if($error2){
				$RecordRow .= '<td nowrap><span style="color:red">'.$SCode.' / '.$Grade.' / '.$WebSAMSClassCode.'</span></td>';
			}
			else{
				$RecordRow .= '<td nowrap>'.$SCode.' / <span style="color:red">'.$Grade.' / '.$WebSAMSClassCode.'</span></td>';
			}
		}
		else{
			$RecordRow .= '<td nowrap>'.$ClassName.'<br><span class="extraInfo">('.$SCode.' / '.$Grade.' / '.$WebSAMSClassCode.')</span></td>';
			$HiddenEntry .= '<textarea name="ClassName[]" style="display:none">'.$ClassName.'</textarea>';
		}
		$RecordRow .= '<td nowrap>'.$ClassNumber.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="ClassNumber[]" style="display:none">'.$ClassNumber.'</textarea>';
		$RecordRow .= '<td nowrap>'.$Gender.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="Gender[]" style="display:none">'.$Gender.'</textarea>';
		$RecordRow .= '<td nowrap>'.$DateOfBirth.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="DateOfBirth[]" style="display:none">'.$DateOfBirth.'</textarea>';
		
		if(isset($i_StudentRegistry_PlaceOfBirth_Code[$PlaceOfBirthCode])){
			$PlaceOfBirthCodeString = $i_StudentRegistry_PlaceOfBirth_Code[$PlaceOfBirthCode];
		}
		else{
			$PlaceOfBirthCodeString = '<span style="color:red">'.$PlaceOfBirthCode.'</span>';
			$local_result['PlaceOfBirthCode'] = false;
		}

		$RecordRow .= '<td nowrap>'.$PlaceOfBirthCodeString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="PlaceOfBirthCode[]" style="display:none">'.$PlaceOfBirthCode.'</textarea>';

		if(isset($i_StudentRegistry_ID_Type_Code[$ID_Type])){
			$ID_TypeString = $i_StudentRegistry_ID_Type_Code[$ID_Type];
		}
		else{
			$ID_TypeString = '<span style="color:red">'.$ID_Type.'</span>';
			$local_result['ID_Type'] = false;
		}
		$RecordRow .= '<td nowrap>'.$ID_TypeString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="ID_Type[]" style="display:none">'.$ID_Type.'</textarea>';

		$RecordRow .= '<td nowrap>'.$ID_Num.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="ID_Num[]" style="display:none">'.$ID_Num.'</textarea>';

		if(isset($i_StudentRegistry_ID_IssuePlace_Code[$ID_IssuePlace])){
			$ID_IssuePlaceString = $i_StudentRegistry_ID_IssuePlace_Code[$ID_IssuePlace];
		}
		else{
			$ID_IssuePlaceString = '<span style="color:red">'.$ID_IssuePlace.'</span>';
			$local_result['ID_IssuePlace'] = false;
		}
		$RecordRow .= '<td nowrap>'.$ID_IssuePlaceString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="ID_IssuePlace[]" style="display:none">'.$ID_IssuePlace.'</textarea>';
		$RecordRow .= '<td nowrap>'.$ID_IssueDate.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="ID_IssueDate[]" style="display:none">'.$ID_IssueDate.'</textarea>';
		$RecordRow .= '<td nowrap>'.$ID_ValidDate.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="ID_ValidDate[]" style="display:none">'.$ID_ValidDate.'</textarea>';

		if(isset($i_StudentRegistry_SP_Type_Code[$SP_Type])){
			$SP_TypeString = $i_StudentRegistry_SP_Type_Code[$SP_Type];
			if($page_check['SP_ValidDate_Not_Null_For_Type_2_3']){
				if(($SP_Type=='2' OR $SP_Type=='3') && $SP_ValidDate==''){
					$SP_ValidDate = '<span style="color:red">-</span>';
					$local_result['SP_ValidDate'] = false;
				}
			}
		}
		else{
			$SP_TypeString = '<span style="color:red">'.$SP_Type.'</span>';
			$local_result['SP_Type'] = false;
		}
		$RecordRow .= '<td nowrap>'.$SP_TypeString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="SP_Type[]" style="display:none">'.$SP_Type.'</textarea>';
		$RecordRow .= '<td nowrap>'.$SP_IssueDate.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="SP_IssueDate[]" style="display:none">'.$SP_IssueDate.'</textarea>';
		$RecordRow .= '<td nowrap>'.$SP_ValidDate.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="SP_ValidDate[]" style="display:none">'.$SP_ValidDate.'</textarea>';

		if(isset($i_StudentRegistry_Country_Code[$CountryCode])){
			$CountryCodeString = $i_StudentRegistry_Country_Code[$CountryCode];
		}
		else{
			$CountryCodeString = '<span style="color:red">'.$CountryCode.'</span>';
			$local_result['CountryCode'] = false;
		}

		$RecordRow .= '<td nowrap>'.$CountryCodeString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="CountryCode[]" style="display:none">'.$CountryCode.'</textarea>';
		$RecordRow .= '<td nowrap>'.$Province.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="Province[]" style="display:none">'.$Province.'</textarea>';
		$RecordRow .= '<td nowrap>'.$Tel.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="HomeTelNo[]" style="display:none">'.$Tel.'</textarea>';

		if(isset($i_StudentRegistry_ResidentialAreaNight_Code[$R_AreaCode])){
			$R_AreaCodeString = $i_StudentRegistry_ResidentialAreaNight_Code[$R_AreaCode];
		}
		else{
			$R_AreaCodeString = '<span style="color:red">'.$R_AreaCode.'</span>';
			$local_result['R_AreaCode'] = false;
		}
		$RecordRow .= '<td nowrap>'.$R_AreaCodeString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="R_AreaCode[]" style="display:none">'.$R_AreaCode.'</textarea>';
		$RecordRow .= '<td nowrap>'.$R_AreaText.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="R_AreaText[]" style="display:none">'.$R_AreaText.'</textarea>';
		
		if(isset($i_StudentRegistry_AreaCode[$AreaCode])){
			$AreaCodeString = $i_StudentRegistry_AreaCode[$AreaCode];
		}
		else{
			$AreaCodeString = '<span style="color:red">'.$AreaCode.'</span>';
			$local_result['AreaCode'] = false;
		}
		$RecordRow .= '<td nowrap>'.$AreaCodeString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="AreaCode[]" style="display:none">'.$AreaCode.'</textarea>';
		$RecordRow .= '<td nowrap>'.$Road.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="Road[]" style="display:none">'.$Road.'</textarea>';
		$RecordRow .= '<td nowrap>'.$Address.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="Address[]" style="display:none">'.$Address.'</textarea>';
		$RecordRow .= '<td nowrap>'.$FatherName.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="FatherName[]" style="display:none">'.$FatherName.'</textarea>';
		$RecordRow .= '<td nowrap>'.$MotherName.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="MotherName[]" style="display:none">'.$MotherName.'</textarea>';
		$RecordRow .= '<td nowrap>'.$FatherOccupation.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="FatherOccupation[]" style="display:none">'.$FatherOccupation.'</textarea>';
		$RecordRow .= '<td nowrap>'.$MotherOccupation.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="MotherOccupation[]" style="display:none">'.$MotherOccupation.'</textarea>';
		if(in_array($GuardianType,array("M", "F", "O"))){
			$GuardianTypeString = $GuardianType=="M" ? $ec_guardian['02'] : 
								($GuardianType=="F" ? $ec_guardian['01'] : 
											$ec_guardian['08']);
		}
		else{
			$GuardianTypeString = '<span style="color:red">'.$GuardianType.'</span>';
			$local_result['GuardianType'] = false;
		}
		$RecordRow .= '<td nowrap>'.$GuardianTypeString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="GuardianType[]" style="display:none">'.$GuardianType.'</textarea>';

		$GuardianIsLiveTogether = (int)$GuardianIsLiveTogether;
		$GuardianIsLiveTogetherString = $GuardianIsLiveTogether ? $i_general_yes : $i_general_no;
		$RecordRow .= '<td nowrap>'.$GuardianIsLiveTogetherString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="GuardianIsLiveTogether[]" style="display:none">'.$GuardianIsLiveTogether.'</textarea>';

		switch ($EmergencyContact_RelationString === 0 ? '' : $EmergencyContact_RelationString)
		{
			case $ec_guardian['01']:
				$EmergencyContact_Relation = '01';
				break;
			case $ec_guardian['02']:
				$EmergencyContact_Relation = '02';
				break;
			case $ec_guardian['03']:
				$EmergencyContact_Relation = '03';
				break;
			case $ec_guardian['04']:
				$EmergencyContact_Relation = '04';
				break;
			case $ec_guardian['05']:
				$EmergencyContact_Relation = '05';
				break;
			case $ec_guardian['06']:
				$EmergencyContact_Relation = '06';
				break;
			case $ec_guardian['07']:
				$EmergencyContact_Relation = '07';
				break;
			case '':
				$EmergencyContact_Relation = '';
				break;
			default:
				if($EmergencyContact_RelationString != $ec_guardian['08']){
					$EmergencyContact_RelationString = '<span style="color:green;">'.$ec_guardian['08'].'</span>';
				}
				$EmergencyContact_Relation = '08';
				break;
		}
		if($page_check['EC_NullOrFillCertain6Fields'] && $EmergencyContactName!==''){
//			if($EmergencyContactName!==''){
//				$local_result['EmergencyContact_Name'] = false;
//				$EmergencyContactName = '<span style="color:red;">-</span>';
//			}
			//relation
			if($EmergencyContact_Relation==''){
				$local_result['EC_Relation'] = false;
				$EmergencyContact_RelationString = '<span style="color:red;">-</span>';
			}
			if($EmergencyContact_Tel==''){
				$local_result['EmergencyContact_Tel'] = false;
				$EmergencyContact_Tel = '<span style="color:red;">-</span>';
			}
			if($EmergencyContact_AreaCode==''){
				$local_result['EmergencyContact_AreaCode'] = false;
//				$EmergencyContact_AreaCode = '<span style="color:red;">-</span>';
				$EmergencyContact_AreaCode = '-';
			}
			if($EmergencyContact_Road==''){
				$local_result['EmergencyContact_Road'] = false;
				$EmergencyContact_Road = '<span style="color:red;">-</span>';
			}
			if($EmergencyContact_Address==''){
				$local_result['EmergencyContact_Address'] = false;
				$EmergencyContact_Address = '<span style="color:red;">-</span>';
			}
		}
		$RecordRow .= '<td nowrap>'.$EmergencyContactName.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="EmergencyContactName[]" style="display:none">'.$EmergencyContactName.'</textarea>';
		$RecordRow .= '<td nowrap>'.$EmergencyContact_RelationString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="EmergencyContact_Relation[]" style="display:none">'.$EmergencyContact_Relation.'</textarea>';
		$RecordRow .= '<td nowrap>'.$EmergencyContact_Tel.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="EmergencyContact_Tel[]" style="display:none">'.$EmergencyContact_Tel.'</textarea>';

		if(isset($i_StudentRegistry_AreaCode[$EmergencyContact_AreaCode])){
			$EmergencyContact_AreaCodeString = $i_StudentRegistry_AreaCode[$EmergencyContact_AreaCode];
		}else{
			$EmergencyContact_AreaCodeString = '<span style="color:red;">'.$EmergencyContact_AreaCode.'</span>';
			$local_result['EmergencyContact_AreaCode'] = false;
		}
		$RecordRow .= '<td nowrap>'.$EmergencyContact_AreaCodeString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="EmergencyContact_AreaCode[]" style="display:none">'.$EmergencyContact_AreaCode.'</textarea>';
		$RecordRow .= '<td nowrap>'.$EmergencyContact_Road.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="EmergencyContact_Road[]" style="display:none">'.$EmergencyContact_Road.'</textarea>';
		$RecordRow .= '<td nowrap>'.$EmergencyContact_Address.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="EmergencyContact_Address[]" style="display:none">'.$EmergencyContact_Address.'</textarea>';
		if($GuardianType=='O'){
			if($OM_GuardianName == ''){
				$OM_GuardianName = '<span style="color:red">-</span>';
				$local_result['OM_GuardianName'] = false;
			}
			if($OM_Guardian_RelationString == ''){
				$OM_Guardian_RelationString = '<span style="color:red">-</span>';
				$local_result['OM_Guardian_RelationString'] = false;
			}
			else{
				switch ($OM_Guardian_RelationString === 0 ? '' : $OM_Guardian_RelationString)
				{
					case $ec_guardian['01']:
						$OM_Guardian_Relation = '01';
						break;
					case $ec_guardian['02']:
						$OM_Guardian_Relation = '02';
						break;
					case $ec_guardian['03']:
						$OM_Guardian_Relation = '03';
						break;
					case $ec_guardian['04']:
						$OM_Guardian_Relation = '04';
						break;
					case $ec_guardian['05']:
						$OM_Guardian_Relation = '05';
						break;
					case $ec_guardian['06']:
						$OM_Guardian_Relation = '06';
						break;
					case $ec_guardian['07']:
						$OM_Guardian_Relation = '07';
						break;
					default:
						if($OM_Guardian_RelationString != $ec_guardian['08']){
							$OM_Guardian_RelationString = '<span style="color:green;">'.$ec_guardian['08'].'</span>';
						}
						$OM_Guardian_Relation = '08';
						break;
				}
			}
			if($OM_Guardian_Occupation == ''){
				$OM_Guardian_Occupation = '<span style="color:red">-</span>';
				$local_result['OM_Guardian_Occupation'] = false;
			}
		}
		$RecordRow .= '<td nowrap>'.$OM_GuardianName.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="OM_GuardianName[]" style="display:none">'.$OM_GuardianName.'</textarea>';
		$RecordRow .= '<td nowrap>'.$OM_Guardian_RelationString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="OM_Guardian_Relation[]" style="display:none">'.$OM_Guardian_Relation.'</textarea>';
		$RecordRow .= '<td nowrap>'.$OM_Guardian_Occupation.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="OM_Guardian_Occupation[]" style="display:none">'.$OM_Guardian_Occupation.'</textarea>';
		if($GuardianIsLiveTogether==0){
			if($Guardian_AreaCode==''){
				//$Guardian_AreaCode = '<span style="color:red">-</span>';
				$Guardian_AreaCode = '-';
				$local_result['Guardian_AreaCode'] = false;
			}
			if($Guardian_Road==''){
				$Guardian_Road = '<span style="color:red">-</span>';
				$local_result['Guardian_Road'] = false;
			}
			if($Guardian_Address==''){
				$Guardian_Address = '<span style="color:red">-</span>';
				$local_result['Guardian_Address'] = false;
			}
			if($Guardian_Tel==''){
				$Guardian_Tel = '<span style="color:red">-</span>';
				$local_result['Guardian_Tel'] = false;
			}
		}
		if(isset($i_StudentRegistry_AreaCode[$Guardian_AreaCode])){
			$Guardian_AreaCodeString = $i_StudentRegistry_AreaCode[$Guardian_AreaCode];
		}
		else{
			if($GuardianIsLiveTogether==1 && $Guardian_AreaCode==''){
				$Guardian_AreaCodeString = '';
			}
			else{
				$Guardian_AreaCodeString = '<span style="color:red;">'.$Guardian_AreaCode.'</span>';
				$local_result['Guardian_AreaCode'] = false;
			}
		}
		$RecordRow .= '<td nowrap>'.$Guardian_AreaCodeString.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="Guardian_AreaCode[]" style="display:none">'.$Guardian_AreaCode.'</textarea>';

		$RecordRow .= '<td nowrap>'.$Guardian_Road.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="Guardian_Road[]" style="display:none">'.$Guardian_Road.'</textarea>';

		$RecordRow .= '<td nowrap>'.$Guardian_Address.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="Guardian_Address[]" style="display:none">'.$Guardian_Address.'</textarea>';

		$RecordRow .= '<td nowrap>'.$Guardian_Tel.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="Guardian_Tel[]" style="display:none">'.$Guardian_Tel.'</textarea>';

		$RecordRow .= '<td nowrap>'.$Guardian_Mobile.'&nbsp;</td>';
		$HiddenEntry .= '<textarea name="Guardian_Mobile[]" style="display:none">'.$Guardian_Mobile.'</textarea>';
//var_dump($local_result);
		if(!in_array(false, $local_result)){
			$validRecordRows[] = $RecordRow.$HiddenEntry;
		}
		else{
			$invalidRecordRows[] = $RecordRow;
		}
	}
	
}

$display =	"
							<tr>
								<td class=tableTitle width=130 nowrap>$i_WebSAMS_Registration_No</td>
								<td class=tableTitle width=60 nowrap>$i_StudentRegistry_AuthCodeMain</td>
								<td class=tableTitle width=60 nowrap>$i_UserChineseName</td>
								<td class=tableTitle width=60 nowrap>$i_UserEnglishName</td>
								<td class=tableTitle width=80 nowrap>$i_UserClassName</td>
								<td class=tableTitle width=80 nowrap>$i_UserClassNumber</td>
								<td class=tableTitle width=80 nowrap>$i_UserGender</td>
								<td class=tableTitle width=80 nowrap>$i_UserDateOfBirth</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_PlaceOfBirth</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ID_Type</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ID_No</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ID_IssuePlace</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ID_IssueDate</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ID_ValidDate</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_SP_Type</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_SP_IssueDate</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_SP_ValidDate</td>
								<td class=tableTitle width=80 nowrap>$i_UserCountry</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_Province</td>
								<td class=tableTitle width=80 nowrap>$i_UserHomeTelNo</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ResidentialArea_Night</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ResidentialArea_Night (".$i_StudentRegistry_ResidentialAreaNight_Code["O"].")</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ResidentialArea</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ResidentialRoad</td>
								<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ResidentialAddress</td>
								<td class=tableTitle width=80 nowrap>".$ec_guardian['01']."<br>$i_UserChineseName</td>
								<td class=tableTitle width=80 nowrap>".$ec_guardian['02']."<br>$i_UserChineseName</td>
								<td class=tableTitle width=80 nowrap>".$ec_guardian['01']."<br>$i_StudentGuardian_Occupation</td>
								<td class=tableTitle width=80 nowrap>".$ec_guardian['02']."<br>$i_StudentGuardian_Occupation</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_StudentGuardian_LiveTogether</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_EmergencyContact<br>$i_UserChineseName</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_EmergencyContact<br>".$ec_iPortfolio['relation']."</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_EmergencyContact<br>$i_StudentGuardian_EMPhone</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_EmergencyContact<br>$i_UserAddress_Area</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_EmergencyContact<br>$i_UserAddress_Road</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_EmergencyContact<br>$i_UserAddress_Address</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_UserChineseName</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>".$ec_iPortfolio['relation']."</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_StudentGuardian_Occupation</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_UserAddress_Area</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_UserAddress_Road</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_UserAddress_Address</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_UserHomeTelNo</td>
								<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_StudentGuardian_EMPhone</td>
							</tr>\n
						";

$cols = 47;
if(sizeof($validRecordRows)>0){
	for ($i=0; $i<sizeof($validRecordRows); $i++){
		$css = ($i%2? "":"2");
		$display .= "<tr class=tableContent$css>";
		$display .= $validRecordRows[$i];
		$display .= "</tr>\n";
	}
	$data_exists = true;
}
else{	
	$data_exists = false;
}

if(sizeof($invalidRecordRows)>0){
	$display .= "<tr><td class=tableTitle colspan=$cols >$i_general_invalid (".sizeof($invalidRecordRows).")</td></tr>";
	for ($i=0; $i<sizeof($invalidRecordRows); $i++){
		$css = ($i%2? "":"2");
		$display .= "<tr class=tableContent$css>";
		$display .= $invalidRecordRows[$i];
		$display .= "</tr>\n";
	}
	$error_exists = true;
}
else{
	$error_exists = false;
}

if($data_exists){
	if($error_exists){
		$expeect_result_msg = $i_StudentRegistry_Import_ValidInvalid_Record.'<br />'.$i_StudentRegistry_Import_Invalid_Reason;
	}
	else{
		$expeect_result_msg = $i_StudentRegistry_Import_Valid_Record;
	}
}
else{
	if($error_exists){
		$expeect_result_msg = $i_StudentRegistry_Import_Invalid_Record.'<br />'.$i_StudentRegistry_Import_Invalid_Reason;
	}
	else{
		$expeect_result_msg = $i_StudentRegistry_Import_No_Record;
	}
}

?>

<form name="form1" method="POST" action="import_update.php">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td><?=$expeect_result_msg ?></td>
	</tr>
</table>

<table width="100%" border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<?=$display?>
</table>

<table width=560 cellspacing=0 cellpadding=0 cellspacing=0 cellpadding=10 align=center>
	<tr>
		<td align='right'><hr size=1 /></td>
	</tr>
	<tr>
		<td align="right">
<?php if($data_exists) { ?>
			<input type="image" alt="<?=$button_confirm?>" src="/images/admin/button/s_btn_confirm_<?=$intranet_session_language?>.gif" border='0'>
<?php } ?>
			<a href='import_xml.php?clear=1'><img alt="<?=$button_cancel?>" border=0 src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border='0' /></a>
		</td>
	</tr>
</table>
</form>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>
