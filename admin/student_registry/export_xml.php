<?php
$PATH_WRT_ROOT = "../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libfilesystem.php");
include($PATH_WRT_ROOT."includes/libaccount.php");
include($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include($PATH_WRT_ROOT."templates/adminheader_intranet.php");

//test
/*

//dev purpose only
include_once($PATH_WRT_ROOT."includes/libSimpleXML.php");
error_reporting(E_ALL);



echo '<pre>';
$file = 'example.xml';
$lsx = new libSimpleXML();
$lsx->loadXMLFile($file);
$xx =$lsx->Data;

//var_dump($xx);
$lsx = new libSimpleXML();

$lsx->Data = $xx;
echo '<textarea cols=120 rows=28>';
echo $lsx->getXML();
echo '</textarea>';

echo '</pre>';
*/

?>

<script language="javascript">

function jEXPORT(){
  if(document.form1.ExportType.value == '1')
    document.form1.action = "export_xml_update.php";
  else
    document.form1.action = "export_xml_confirm.php";
	document.form1.submit();
}

</script>

<?= displayNavTitle($i_admintitle_am, '', $i_StudentRegistry['System'], 'index.php',$button_search, 'javascript:history.back()', $button_export_xml, '') ?>
<?= displayTag("head_registry_$intranet_session_language.gif", $msg) ?>

<form name="form1" action="export_xml_confirm.php" method="post">

<?
$export_types = array();
$export_types[] = array(1, $i_StudentRegistry_ExportFormat_XML_1);
$export_types[] = array(2, $i_StudentRegistry_ExportFormat_XML_2);
$export_types[] = array(3, $i_StudentRegistry_ExportFormat_XML_3);
$export_type_select = getSelectByArray($export_types,' name="ExportType"','',0,1);
?>
<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="right"><?=$i_StudentRegistry_ExportPurpose?></td>
		<td align="right"><?=$export_type_select?></td>
	</tr>
	<tr>
		<td height="22" style="vertical-align:bottom" colspan="2"><hr size=1 /></td>
	</tr>
</table>

<input type=hidden name=pageNo value="<?=$pageNo ?>" />
<input type=hidden name=order value="<?=$order ?>" />
<input type=hidden name=field value="<?=$field ?>" />
<input type=hidden name=keyword value="<?=$keyword ?>" />
<input type=hidden name=ts value="<?=$ts ?>" />

<?php
for($i=0;$i<sizeof($ClassID);$i++){
	echo "<input type=hidden name=ClassID[] value='".$ClassID[$i]."' />";
}
?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td align="right">
			<img src="/images/admin/button/s_btn_export_<?=$intranet_session_language?>.gif" border="0" onclick="jEXPORT();">
			<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
		</td>
	</tr>
</table>
</form>

<?php
include($PATH_WRT_ROOT."templates/adminfooter.php");
?>
