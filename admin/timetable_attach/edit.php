<?php 
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libtimetable.php");
include("../../includes/libgrouping.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();

$li = new libtimetable($TimetableID[0]);
$lo = new libgrouping();
$RecordStatus0 = ($li->RecordStatus==0) ? "CHECKED" : "";
$RecordStatus1 = ($li->RecordStatus==1) ? "CHECKED" : "";
?>

<script language="javascript">
function checkform(obj){
	if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_TimetableTitle; ?>.")) return false;
	checkOptionAll(obj.elements["GroupID[]"]);
	if(obj.elements["GroupID[]"].length==0){ alert(globalAlertMsg15); return false; }
}
function fileAttach(obj){
	url = "../filesystem/attach.php?folderID=0&fieldname=" + obj.name + "&attachment=" + obj.value;
	newWindow(url,1);
}
</script>

<form name=form1 action=edit_update.php method=post onSubmit="return checkform(this);">
<p class=admin_head><?php echo $i_admintitle_im.displayArrow(); ?><a href=javascript:history.back()><?php echo $i_admintitle_im_timetable; ?></a><?php echo displayArrow();?><?php echo $button_edit; ?></p>
<blockquote>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr><td align=right><?php echo $i_TimetableTitle; ?>:</td><td><input class=text type=text name=Title size=30 maxlength=255 value="<?php echo $li->Title; ?>"></td></tr>
<tr><td align=right><?php echo $i_TimetableDescription; ?>:</td><td><textarea name=Description cols=30 rows=5><?php echo $li->Description; ?></textarea></td></tr>
<tr><td align=right><?php echo $i_TimetableURL; ?>:</td><td><input class=text type=text name=URL size=30 maxlength=255 value="<?php echo $li->URL; ?>"> <input class=button type=button value="<?php echo $button_attach; ?>" onClick=fileAttach(this.form.URL)></td></tr>
<tr><td align=right><?php echo $i_TimetableRecordStatus; ?>:</td><td><input type=radio name=RecordStatus value=1 <?php echo $RecordStatus1; ?>> <?php echo $i_status_publish; ?> <input type=radio name=RecordStatus value=0 <?php echo $RecordStatus0; ?>> <?php echo $i_status_pending; ?></td></tr>
<tr><td colspan=2><br></td></tr>
<tr><td colspan=2><?php echo $lo->displayTimetableGroups($li->TimetableID); ?></td></tr>
<tr><td colspan=2><br></td></tr>
<tr><td><br></td><td><input class=submit type=submit value="<?php echo $button_save; ?>"><input class=reset type=reset value="<?php echo $button_reset; ?>"><input class=button type=button value="<?php echo $button_cancel; ?>" onClick=history.back()></td></tr>
</table>
</blockquote>
<input type=hidden name=TimetableID value="<?php echo $li->TimetableID; ?>">
</form>

<?php 
intranet_closedb();
include("../../templates/adminfooter.php"); 
?>
