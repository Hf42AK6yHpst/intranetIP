<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$Title = htmlspecialchars(trim($Title));
$Description = htmlspecialchars(trim($Description));
$URL = htmlspecialchars(trim($URL));
$fieldname = "Title, Description, URL, RecordStatus, DateInput, DateModified";
$fieldvalue = "'$Title', '$Description', '$URL', '$RecordStatus', now(), now()";
$sql = "INSERT INTO INTRANET_TIMETABLE ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);
$TimetableID = $li->db_insert_id();

for($i=0; $i<sizeof($GroupID); $i++){
	$sql = "INSERT INTO INTRANET_GROUPTIMETABLE (GroupID, TimetableID) VALUES (".$GroupID[$i].", $TimetableID)";
	$li->db_db_query($sql);
}

intranet_closedb();
header("Location: index.php?filter=$RecordStatus&msg=1");
?>
