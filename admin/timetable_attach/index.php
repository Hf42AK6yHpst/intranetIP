<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libdbtable.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();

# TABLE SQL
$keyword = trim($keyword);
if($field=="") $field = 2;
switch ($field){
	case 0: $field = 0; break;
	case 1: $field = 1; break;
	case 2: $field = 2; break;
	default: $field = 0; break;
}
if($filter == "") $filter = 1;
$filter = ($filter == 1) ? 1 : 0;
$sql  = "SELECT 
			CONCAT('<a class=tableContent href=edit.php?TimetableID[]=', TimetableID, '>', Title, '</a>'), 
			Description,
			DateModified,
			CONCAT('<input type=checkbox name=TimetableID[] value=', TimetableID ,'>')
		FROM 
			INTRANET_TIMETABLE
		WHERE 
			(Title like '%$keyword%' OR Description like '%$keyword%') AND
			RecordStatus = $filter
		";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("Title", "Description", "DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_timetable;
$li->column_array = array(0,1,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(0, $i_TimetableTitle)."</td>\n";
$li->column_list .= "<td width=40% class=tableTitle>".$li->column(1, $i_TimetableDescription)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(2, $i_TimetableDateModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("TimetableID[]")."</td>\n";

// TABLE FUNCTION BAR 
$toolbar = "<a href=javascript:checkNew('new.php')>".newIcon()."$button_new</a>";
$functionbar  = "<input class=button type=button value='$button_edit' onClick=checkEdit(this.form,'TimetableID[]','edit.php')>";
$functionbar .= "<input class=button type=button value='$button_remove' onClick=checkRemove(this.form,'TimetableID[]','remove.php')>";
$searchbar  = "<select name=filter onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value=0 ".(($filter==0)?"selected":"").">$i_status_pending</option>\n";
$searchbar .= "<option value=1 ".(($filter==1)?"selected":"").">$i_status_publish</option>\n";
$searchbar .= "</select>\n";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<input class=submit type=submit value='$button_search'>\n";
?>

<form name=form1 method="get">
<p class=admin_head><?php echo $i_admintitle_im.displayArrow().$i_admintitle_im_timetable; ?></p>
<table width=560 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src=../../images/admin/timetable/timetable_<?php echo $intranet_session_language; ?>.gif width=170 height=36 border=0></td></tr>
<tr><td class=admin_bg_menu><img src=../../images/space.gif width=1 height=4 border=0></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=../../images/admin/menu_bottom.gif width=560 height=8 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
</form>

<?php 
intranet_closedb();
include("../../templates/adminfooter.php"); 
?>
