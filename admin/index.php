<?php
include("../includes/global.php");
if (is_file("../servermaintenance.php") && !$iServerMaintenance)
{
    header("Location: ../servermaintenance.php");
    exit();
}
include("../lang/lang.$intranet_session_language.php");



session_unregister_intranet("UserID");
session_unregister_intranet("composeFolder_admin");

$width = ($isIE) ? 80 : 83;
//<frame src=../templates/adminmenu/ noresize frameborder=0 border=0 framespacing=0 marginwidth=0 scrolling=no>
// debug_pr($_SESSION);

?>
<html>
<head><title><?php echo $i_admin_title; ?></title></head>
<META http-equiv=Content-Type content='text/html; charset=utf-8'>
<frameset rows="<?php echo $width; ?>,*" frameborder=0 border=0 framespacing=0 marginwidth=0 marginheight=0>
<frame src=menu.php name=intranet_admin_menu noresize frameborder=0 border=0 framespacing=0 marginwidth=0 scrolling=no>
<frame src=main.php name=intranet_admin_main noresize frameborder=0 border=0 framespacing=0 marginwidth=0 scrolling=auto>
</frameset>
</html>