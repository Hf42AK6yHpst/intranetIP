<?php
//using : yat

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libresource.php");
include_once("../../includes/librb.php");
include_once("../../includes/libcycleperiods.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

$lr = new libresource();
$lrb = new librb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     case 4: $field = 4; break;
     default: $field = 0; break;
}
switch ($filter){
     case 0: $filter = 0; break;
     case 1: $filter = 1; break;
     case 2: $filter = 2; break;
     case 3: $filter = 3; break;
     case 4: $filter = 4; break;
     case 5: $filter = 5; break;
     default: $filter = 0; break;
}


# Check need to change status to "Approved" or not (if no pending record)
$sql = "select PeriodicBookingID from INTRANET_PERIODIC_BOOKING where RecordStatus=0";
$result = $lrb->returnVector($sql);
if(!empty($result))
{
	foreach($result as $k=>$pid)
	{
		$lrb->NeedUpdatePeriodicRecordStatus($pid);
	}	
}

$sql  = "SELECT
               DATE_FORMAT(a.BookingStartDate,'%Y-%m-%d'),
               DATE_FORMAT(a.BookingEndDate,'%Y-%m-%d'),
               CONCAT(b.TimeSlotBatchID, ':',a.TimeSlots),
               CONCAT('<b>', b.ResourceCategory, '</b><br>', b.ResourceCode, '<br>' ,b.Title) AS Resource,
               CONCAT('<b>', c.EnglishName,'</b> ', IFNULL(IF(c.ClassName <> '',c.ClassName,''),''), IFNULL(IF(c.ClassNumber = 0,'',c.ClassNumber),''), '<br>', if(a.Remark is NULL,'',concat('<br>', a.Remark))) AS UserName,
               CONCAT(a.RecordType,':',a.TypeValues,':',DATE_FORMAT(a.BookingStartDate,'%Y-%m-%d'),':',DATE_FORMAT(a.BookingEndDate,'%Y-%m-%d')),
               a.TimeApplied,
               CONCAT('<input type=checkbox name=PeriodicBookingID[] value=', a.PeriodicBookingID ,'>')
          FROM
               INTRANET_PERIODIC_BOOKING AS a, INTRANET_RESOURCE AS b, INTRANET_USER AS c
          WHERE
               a.ResourceID = b.ResourceID AND
               a.UserID = c.UserID AND
               a.RecordStatus = $filter
               ";

if ($filter2!='')
    $sql .= " AND b.ResourceCategory = '$filter2'";
if ($filter3!='')
    $sql .= " AND b.ResourceID = '$filter3'";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.BookingStartDate","a.BookingEndDate", "Resource", "UserName","a.TimeApplied");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+4;
$li->title = $i_admintitle_booking;
$li->column_array = array(0,0,3,0,1,4);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(0, $i_BookingStartDate)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(1, $i_BookingEndDate)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle> $i_BookingTimeSlots </td>\n";
$li->column_list .= "<td width=17% class=tableTitle>".$li->column(2, $i_ResourceCategory."/".$i_ResourceCode."/".$i_ResourceTitle)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(3, $i_UserEnglishName."/".$i_UserRemark)."</td>\n";
$li->column_list .= "<td width=8% class=tableTitle> $i_BookingPeriodicType </td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(4, $i_BookingApplied)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("PeriodicBookingID[]")."</td>\n";

// TABLE FUNCTION BAR
// $btn_reject = "<a href=\"javascript:checkRemove(document.form1,'PeriodicBookingID[]','cancel.php')\"><img src='/images/admin/button/t_btn_refuse_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$btn_reject = "<a href=\"javascript:checkAlert(document.form1,'PeriodicBookingID[]','cancel.php','$i_Alert_Reject')\"><img src='/images/admin/button/t_btn_refuse_$intranet_session_language.gif' border='0' align='absmiddle'></a>";

$btn_checkin = "<input class=button type=button value='$button_checkin' onClick=checkRemove(this.form,'PeriodicBookingID[]','checkin.php')>";
$btn_checkout = "<input class=button type=button value='$button_checkout' onClick=checkRemove(this.form,'PeriodicBookingID[]','checkout.php')>";
$btn_approve = "<a href=\"javascript:checkApprove(document.form1,'PeriodicBookingID[]','approve.php')\"><img src='/images/admin/button/t_btn_permit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$btn_remove = "<a href=\"javascript:checkRemove(document.form1,'PeriodicBookingID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";

switch ($filter)
{
        case 0:
             $functionbar = "$btn_reject$btn_approve"; break;
        case 1:
             $functionbar = ""; break;
        case 2:
             $functionbar = ""; break;
        case 3:
             $functionbar = ""; break;
        case 4:
             $functionbar = "$btn_reject"; break;
        case 5:
             $functionbar = ""; break;
        default:
             $functionbar = "";
}
/*
$functionbar .= ($filter==3 || $filter==2 || $filter==1) ? "" : "<input class=button type=button value='$button_cancel' onClick=checkRemove(this.form,'BookingID[]','cancel.php')>";
$functionbar .= ($filter==3 || $filter==2 || $filter==1) ? "" : "<input class=button type=button value='$button_checkin' onClick=checkRemove(this.form,'BookingID[]','checkin.php')>";
$functionbar .= ($filter==3 || $filter==1 || $filter==0) ? "" : "<input class=button type=button value='$button_checkout' onClick=checkRemove(this.form,'BookingID[]','checkout.php')>";
$functionbar .= "<input class=button type=button value='$button_remove' onClick=checkRemove(this.form,'BookingID[]','remove.php')>\n";
*/
$functionbar .= "$btn_remove\n";
$searchbar  = "<select name=filter onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value=0 ".(($filter==0)?"selected":"").">$i_BookingPending</option>\n";
$searchbar .= "<option value=1 ".(($filter==1)?"selected":"").">$i_status_cancel</option>\n";
//$searchbar .= "<option value=2 ".(($filter==2)?"selected":"").">$i_status_checkin</option>\n";
//$searchbar .= "<option value=3 ".(($filter==3)?"selected":"").">$i_status_checkout</option>\n";
$searchbar .= "<option value=4 ".(($filter==4)?"selected":"").">$i_status_reserved</option>\n";
$searchbar .= "<option value=5 ".(($filter==5)?"selected":"").">$i_status_rejected</option>\n";
$searchbar .= "</select>\n";

/*
$filter_date = "<SELECT name=filter4 onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$filter_date .= "<OPTION value=0>$i_Booking_all</option>\n";
$filter_date .= "<OPTION value=1>$i_Booking_future</option>\n";
$filter_date .= "<OPTION value=2>$i_Booking_past</option>\n";
$filter_date .= "</SELECT>\n";
*/
$filter_cat = $lr->getSelectCats("name=filter2 onChange=\"this.form.pageNo.value=1;this.form.filter3.value='';this.form.submit();\"",$filter2);
$filter_item = $lr->getSelectItems("name=filter3 onChange=\"this.form.pageNo.value=1;this.form.filter2.value='';this.form.submit();\"",$filter3);
?>


<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_rm, '', $i_admintitle_rm_per_record, '') ?>
<?= displayTag("head_booking_periodic_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $searchbar.$functionbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-","-","",$filter_cat.$filter_item."&nbsp;"); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>