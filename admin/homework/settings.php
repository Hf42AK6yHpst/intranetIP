<?php
include("../../includes/global.php");
include("../../lang/email.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../includes/libdb.php");
include("../../templates/adminheader_setting.php");
//include("../../includes/libinterface.php");

//$li = new interface_html();

$lf = new libfilesystem();
$fixed = $lf->file_read($intranet_root."/file/homework_startdate.txt");
$prights = $lf->file_read($intranet_root."/file/homework_parent.txt");
$disable = $lf->file_read($intranet_root."/file/homework_disable.txt");
$byteacher = $lf->file_read("$intranet_root/file/homework_byteacher.txt");
$bysubject = $lf->file_read("$intranet_root/file/homework_bysubject.txt");
$nonteaching = $lf->file_read("$intranet_root/file/homework_nonteaching.txt");
$restrict = $lf->file_read("$intranet_root/file/homework_restrict.txt");
$bytaught = $lf->file_read("$intranet_root/file/homework_taught.txt");
$subleader = $lf->file_read("$intranet_root/file/homework_leader.txt");
$allowexport = $lf->file_read("$intranet_root/file/homework_export.txt");
$allow_past = $lf->file_read("$intranet_root/file/homework_past.txt");
$useteachercollecthomework = $lf->file_read("$intranet_root/file/homework_teachercollects.txt");

$dchecked = ($fixed==1? "CHECKED":"");
$pchecked = ($prights==1? "CHECKED":"");
$disablechecked = ($disable==1? "CHECKED":"");
$tchecked = ($byteacher==1?"CHECKED":"");
$schecked = ($bysubject==1?"CHECKED":"");
$nonteachchecked = ($nonteaching==1?"CHECKED":"");
$restrictchecked = ($restrict==1?"CHECKED":"");
$stchecked = ($bytaught==1?"CHECKED":"");
$subleaderchecked = ($subleader==1?"CHECKED":"");
$allowexportchecked = ($allowexport==1?"CHECKED":"");
$allow_pastchecked = ($allow_past==1?"CHECKED":"");
$useteachercollecthomeworkchecked = ($useteachercollecthomework==1?"CHECKED":"");

if ($sys_custom['homeworklist_type'])
{
    $use_homework_type_content = $lf->file_read("$intranet_root/file/homework_usetype.txt");
    $usehomeworktype_checked = ($use_homework_type_content==1?"CHECKED":"");
}

if ($sys_custom['homeworklist_handin'])
{
    $useHomeworkHandin = $lf->file_read("$intranet_root/file/homework_usehandin.txt"); // Temp. Use
    $usehomeworkHandin_checked = ($useHomeworkHandin==1?"CHECKED":"");
}

?>

<script language="javascript">
function checkform(obj){
	
	if(obj.clearall.checked)
	{
		for (var i=0; i < obj.delHomeworkRec.length; i++)
		{
			if (obj.delHomeworkRec[i].checked)
			{
				
	       		if(obj.delHomeworkRec[i].value == "period")
	     		{
	        		if(!check_text(obj.from, "<?=$i_alert_pleaseselect?> <?=$i_general_startdate?>.")) return false;
	        		if(!check_text(obj.to, "<?=$i_alert_pleaseselect?> <?=$i_general_enddate?>.")) return false;
	        		if(!confirm('<?=$i_Homework_admin_clear_all_records_alert?>'))	return false;
	     		}
	     		else
	     		{
		     		if(!confirm('<?=$i_Homework_admin_clear_all_records_alert?>'))	return false;
	     		}
			}
		}
	}

	 
        return true;
}

function setCheckBoxVisible()
{
	var curStyle = document.getElementById('delHomeWorkCB').style.display;
	if(curStyle=='block')
		newStyle = 'none';
	else
		newStyle = 'block';
	document.getElementById('delHomeWorkCB').style.display=newStyle;
}

function setTextEnable(val)
{
	
	var curEnable = document.getElementById('from').disabled;
	
	if(val=='period')
		document.getElementById('delHomeWorkTB').style.display='block';
	else
		document.getElementById('delHomeWorkTB').style.display='none';
		
	if(curEnable)
		newEnable = false;
	else
		newEnable = true;
	document.getElementById('from').disabled=newEnable;
	document.getElementById('to').disabled=newEnable;
}

</script>


<form name="form1" action="update.php" method="post" ONSUBMIT="return checkform(this)">
<?= displayNavTitle($i_admintitle_fs, '', $i_admintitle_fs_homework, '' ) ?>
<?= displayTag("head_homework_$intranet_session_language.gif", $msg) ?>


<blockquote><blockquote>
<table width=400 border=0 bordercolor='#F7F7F9' cellspacing=1 cellpadding=3>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_disable?></td><td align=center><input type=checkbox name=disable value=1 <?=$disablechecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_startdate?></td><td align=center><INPUT type=checkbox name=fixstart value=1 <?=$dchecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_past_day_allowed?></td><td align=center><input type=checkbox name=pastallowed value=1 <?=$allow_pastchecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_parent?></td><td align=center><input type=checkbox name=prights value=1 <?=$pchecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_disable_search_subject?></td><td align=center><input type=checkbox name=disablesubject value=1 <?=$schecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_disable_search_teacher?></td><td align=center><input type=checkbox name=disableteacher value=1 <?=$tchecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_disable_search_taught?></td><td align=center><input type=checkbox name=disabletaught value=1 <?=$stchecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_nonteaching_access?></td><td align=center><input type=checkbox name=nonteach value=1 <?=$nonteachchecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_restrict_access?></td><td align=center><input type=checkbox name=restrict value=1 <?=$restrictchecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_enable_subjectleader?></td><td align=center><input type=checkbox name=subleader value=1 <?=$subleaderchecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_enable_export?></td><td align=center><input type=checkbox name=allowexport value=1 <?=$allowexportchecked?>></td></tr>
<? if ($sys_custom['homeworklist_type']) { ?>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_use_homework_type?> [<a class=functionlink_new href="homeworktype.php"><?="$button_edit $i_Homework_HomeworkType"?></a>]</td><td align=center><input type=checkbox name=usehomeworktype value=1 <?=$usehomeworktype_checked?>></td></tr>
<? } ?>
<? if ($sys_custom['homeworklist_handin']) { ?>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_use_homework_handin?></td><td align=center><input type=checkbox name=usehomeworkhandin value=1 <?=$usehomeworkHandin_checked?>></td></tr>
<? } ?>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_use_teacher_collect_homework?></td><td align=center><input type=checkbox name=useteachercollecthomework value=1 <?=$useteachercollecthomeworkchecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Homework_admin_clear_all_records?></td><td align=center><input type=checkbox name=clearall value=1 onclick="setCheckBoxVisible()"></td></tr>

<tr><td id="delHomeWorkCB" style="display: none; vertical-align:middle" class=tableTitle_new >
<table bordercolor='#F7F7F9' border="0" cellspacing=1 cellpadding=3 width="90%"><tr><td width="50%"><input type="radio" name="delHomeworkRec" value="all" id="delHomeworkRec" onclick="setTextEnable(this.value)" checked><?=$i_Homework_AllRecords?></td>
<td><input type="radio" name="delHomeworkRec" value="period" id="delHomeworkRec" onclick="setTextEnable(this.value)"><?=$eEnrollment['Period']?></td></tr>
<tr id="delHomeWorkTB" style="display: none"><td colspan="2">
	<table bordercolor='#F7F7F9' border="0" cellspacing=1 cellpadding=3 width="90%">
	<tr><td><?=$i_From?></td><td><input type="text" id="from" name="from" disabled> (YYYY-MM-DD)</td></tr>
	<tr><td><?=$i_To?></td><td><input type="text" id="to" name="to" disabled> (YYYY-MM-DD)</td></tr></table>
</td></tr>
</table>
</td><td>&nbsp;</td></tr>


</table>
</blockquote></blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
 <a href="index.php"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>
</form>


<?php
include("../../templates/adminfooter.php");
?>