<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
intranet_opendb();

$lf = new libfilesystem();
if (is_array($target) && sizeof($target)>0)
{
	$newTarget = array();
	for($i=0; $i<sizeof($target); $i++) {
		if(trim($target[$i]) != "" && is_numeric($target[$i])) {
			$newTarget[] = $target[$i];
		}
	}
	$target = $newTarget;

	$target = str_replace("&#160;", "", $target);
	$file_content = implode(",", $target);
}

# Write to file
$li = new libfilesystem();
$location = $intranet_root."/file/inventory";
$li->folder_new($location);
$file = $location."/admin_user.txt";
$success = $li->file_write($file_content, $file);

intranet_closedb();
header("Location: admin_setting.php?msg=2");
?>
