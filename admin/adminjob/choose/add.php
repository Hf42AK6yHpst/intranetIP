<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libgroupcategory.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/fileheader_admin.php");
intranet_opendb();

# retrieve group category

$li = new libgrouping();

if($CatID < 0){
     unset($ChooseGroupID);
     $ChooseGroupID[0] = 0-$CatID;
}
$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();

$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=CatID onChange=checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()>\n" : "<select name=CatID onChange=this.form.submit()>\n";
$x1 .= "<option value=0></option>\n";
for ($i=0; $i<sizeof($cats); $i++)
{
     list($id,$name) = $cats[$i];
     if ($id!=0)
     {
         $x1 .= "<option value=$id ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
     }
}
$x1 .= "<option value=0>";
for($i = 0; $i < 20; $i++)
$x1 .= "_";
$x1 .= "</option>\n";
$x1 .= "<option value=0></option>\n";
$x1 .= "<option value=-1 ".(($CatID==-1)?"SELECTED":"").">$i_identity_teachstaff</option>\n";
$x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
$x1 .= "<option value=-3 ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n";

$x1 .= "<option value=0></option>\n";
$x1 .= "</select>";

if($CatID!=0 && $CatID > 0) {
     $row = $li->returnCategoryGroups($CatID);
     $x2  = "<select name=ChooseGroupID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
          $GroupCatID = $row[$i][0];
          $GroupCatName = $row[$i][1];
          if ($GroupCatID != $GroupID)
          {
              $x2 .= "<option value=$GroupCatID";
              for($j=0; $j<sizeof($ChooseGroupID); $j++){
                  $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
              }
              $x2 .= ">$GroupCatName</option>\n";
          }
     }
     $x2 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x2 .= "&nbsp;";
     $x2 .= "</option>\n";
     $x2 .= "</select>\n";
}

if($CatID < 0){
   # Return users with identity chosen
   $selectedUserType = 0-$CatID;
   $row = $li->returnUserForType($selectedUserType);

     $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}
else if(isset($ChooseGroupID)) {
     $row = $li->returnGroupUsers($ChooseGroupID);
     $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}
?>
<script language="JavaScript1.2">

function import_update(){
        var obj = document.form1;
        checkOption(obj.elements["ChooseUserID[]"]);
        obj.action = "add_user.php";
        obj.submit();
}

function SelectAll()
{
        var obj = document.form1.elements['ChooseUserID[]'];
        for (i=0; i<obj.length; i++)
        {
          obj.options[i].selected = true;
        }
}

function expandGroup()
{
        var obj = document.form1;
        checkOption(obj.elements['ChooseGroupID[]']);
        obj.submit();
}

</script>

<form name=form1 action=add.php method=post>
<?= displayNavTitle($i_AdminJob_Announcement, '', $button_add, '') ?>

<p style="padding-left:20px">
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
<tr><td style="background-image: url(../../../images/admin/pop_bg.gif);" >
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_bar.gif" width=422 height=16 border=0></td></tr>
</table>
<table width=422 border=0 cellpadding=10 cellspacing=0>
<tr><td>
<p><?php echo $i_frontpage_campusmail_select_category; ?>:
<br><?php echo $x1; ?>
<?php if($CatID!=0 && $CatID > 0) { ?>
<p><?php echo $i_frontpage_campusmail_select_group; ?>:
<br><?php echo $x2; ?>
<a href="javascript:expandGroup()"><img src="/images/admin/button/s_btn_expand_<?=$intranet_session_language?>.gif" border="0"></a>
<?php } ?>
<?php if(isset($ChooseGroupID)) { ?>
<p><?php echo $i_frontpage_campusmail_select_user; ?>:
<br><?php echo $x3; ?>
<?
# Different options
if ($type==1)         # Circular Admin Level
{
?>
<br><?=$i_Circular_AdminLevel?> : <input type=radio name=adminlevel value=0 CHECKED> <?=$i_Circular_AdminLevel_Normal?> &nbsp; <input type=radio name=adminlevel value=1> <?=$i_Circular_AdminLevel_Full?>
<?
}
?>
<a href="javascript:import_update()"><img src="/images/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border="0"></a>
<a href="javascript:SelectAll()"><img src="/images/admin/button/s_btn_select_all_<?=$intranet_session_language?>.gif" border="0"></a>
<?php } ?>
</td>
</tr>
</table>

</td></tr>
<tr><td><img src="../../../images/admin/pop_bottom.gif" width=422 height=18 border=0></td></tr>
<tr><td align=center height="40" style="vertical-align:bottom">
<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
<input type=hidden name=type value="<?=$type?>">
</form>

<?php
intranet_closedb();
include_once("../../../templates/filefooter.php");
?>