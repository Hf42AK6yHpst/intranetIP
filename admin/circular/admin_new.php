<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libcircular.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$lcircular = new libcircular();

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_Circular_Settings, 'index.php',$i_Circular_Admin,'javascript:history.back()',$button_new,'') ?>
<?= displayTag("head_circular_set_$intranet_session_language.gif", $msg) ?>
<SCRIPT language=Javascript>

</SCRIPT>
<form name=form1 action=admin_new_update.php method=POST>
<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0 align=center>
<tr><td><?=$i_Circular_Description_SetAdmin?></td></tr>
<tr><td><?=$lcircular->displayNonAdminUserInput()?></td></tr>
<tr><td><br><?=$i_Circular_AdminLevel?> : <br>
<input type=radio name=adminlevel value=0 CHECKED> <?=$i_Circular_AdminLevel_Detail_Normal?> <br>
<input type=radio name=adminlevel value=1> <?=$i_Circular_AdminLevel_Detail_Full?></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="admin.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>


<?php
include_once("../../templates/adminfooter.php");
?>