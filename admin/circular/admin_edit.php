<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libadminjob.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$lu = new libuser($aid);
$ladminjob = new libadminjob();
$adminlevel = $ladminjob->returnAdminLevel($aid,1);

if ($adminlevel == 1)
{
    $strFull = "CHECKED";
    $strNormal = "";
}
else
{
    $strFull = "";
    $strNormal = "CHECKED";
}

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_Circular_Settings, 'index.php',$i_Circular_Admin,'javascript:history.back()',$button_edit,'') ?>
<?= displayTag("head_circular_set_$intranet_session_language.gif", $msg) ?>
<SCRIPT language=Javascript>

</SCRIPT>
<form name=form1 action=admin_edit_update.php method=POST>
<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0 align=center>
<tr><td align=right><?=$i_UserLogin?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?=$i_UserName?>:</td><td><?=$lu->UserNameLang()?></td></tr>
<? if ($lu->ClassName != "") {?>
<tr><td align=right><?=$i_ClassName?>:</td><td><?=$lu->ClassName?></td></tr>
<? } ?>
<? if ($lu->ClassNumber != "") {?>
<tr><td align=right><?=$i_ClassNumber?>:</td><td><?=$lu->ClassNumber?></td></tr>
<? } ?>
<tr><td align=right><?=$i_Circular_AdminLevel?> :</td><td>
<input type=radio name=adminlevel value=0 <?=$strNormal?>> <?=$i_Circular_AdminLevel_Detail_Normal?> <br>
<input type=radio name=adminlevel value=1 <?=$strFull?>> <?=$i_Circular_AdminLevel_Detail_Full?></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="admin.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=targetUserID value="<?=$aid?>">
</form>


<?php
include_once("../../templates/adminfooter.php");
?>