<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libcircular.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$lcircular = new libcircular();

$disableChecked = ($lcircular->disabled==1? "CHECKED":"");
$helpChecked = ($lcircular->isHelpSignAllow==1? "CHECKED":"");
$lateChecked = ($lcircular->isLateSignAllow==1? "CHECKED":"");
$resignChecked = ($lcircular->isResignAllow==1? "CHECKED":"");
$defaultNum = $lcircular->defaultNumDays;
$enableViewAllChecked = ($lcircular->showAllEnabled==1?"CHECKED":"");
?>
<form name="form1" action="update.php" method="post">
<?= displayNavTitle($i_admintitle_fs, '', $i_Circular_Settings, 'index.php',$i_general_BasicSettings,'') ?>
<?= displayTag("head_circular_set_$intranet_session_language.gif", $msg) ?>
<blockquote><blockquote>
<table width=400 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=3>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Circular_Settings_Disable?></td><td align=center><input type=checkbox name=disabled value=1 <?=$disableChecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Circular_Settings_AllowHelpSigning?></td><td align=center><input type=checkbox name=helpsign value=1 <?=$helpChecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Circular_Settings_AllowLateSign?></td><td align=center><input type=checkbox name=late value=1 <?=$lateChecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Circular_Settings_AllowResign?></td><td align=center><input type=checkbox name=resign value=1 <?=$resignChecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Circular_Settings_DefaultDays?></td><td align=center><input type=text name=numDays value=<?=$defaultNum?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Circular_Setting_TeacherCanViewAll?></td><td align=center><input type=checkbox name=enableViewAll value=1 <?=$enableViewAllChecked?>></td></tr>
</table>
</blockquote></blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
 <a href="index.php"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>
</form>


<?php
include("../../templates/adminfooter.php");
?>