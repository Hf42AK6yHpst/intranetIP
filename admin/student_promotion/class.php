<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
if ($ClassID == "")
{
    header ("Location: index.php");
    exit();
}
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


$lsp = new libstudentpromotion();
$ClassName = $lsp->getClassName($ClassID);
/*
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
*/
$sql = "SELECT
              a.ChineseName, a.EnglishName,
              a.Gender,
               a.ClassName, a.ClassNumber, a.UserLogin,
               CONCAT('<input type=text size=6 maxlength=20 name=newClassNum[] value=\"',b.NewClassNumber,'\"><input type=hidden name=ClassStudentID[] value=',b.UserID,'>') ,
               CONCAT('<input type=checkbox name=StudentID[] value=',a.UserID,'>')
        FROM INTRANET_PROMOTION_STATUS as b LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
        WHERE b.NewClass = $ClassID AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND a.UserID IS NOT NULL
        ";
//echo $sql;
# TABLE INFO
if ($order_selection != 2 && $order_selection != 3)
{
    $order_selection = 1;
}
if ($order_selection == 1)         # Pure English Name
{
    $order = 1;
    $field = 0;
}
else if ($order_selection == 2)      # Boys First, English Name
{
     $order = 0;
     $field = 1;
}
else                                   # Girls first, English Name
{
    $order = 1;
    $field = 1;
}

$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.EnglishName","a.Gender");
if ($order_selection == 2 || $order_selection == 3)
    $li->fieldorder2 = ",a.EnglishName ASC";
$li->sql = $sql;
$li->no_col = 9;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>$i_UserChineseName</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>$i_UserEnglishName</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>$i_UserGender</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>$i_StudentPromotion_OldClass</td>\n";
$li->column_list .= "<td width=12% class=tableTitle>$i_StudentPromotion_OldClassNumber</td>\n";
$li->column_list .= "<td width=18% class=tableTitle>$i_UserLogin</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>$i_StudentPromotion_NewClassNumber</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentID[]")."</td>\n";

$select_order = "<SELECT name=order_selection onChange=this.form.submit()>\n";
$select_order .= "<OPTION value=1 ".($order_selection==1?"SELECTED":"").">$i_StudentPromotion_Sorting_Name</OPTION>\n";
$select_order .= "<OPTION value=2 ".($order_selection==2?"SELECTED":"").">$i_StudentPromotion_Sorting_BoysName</OPTION>\n";
$select_order .= "<OPTION value=3 ".($order_selection==3?"SELECTED":"").">$i_StudentPromotion_Sorting_GirlsName</OPTION>\n";
$select_order .= "</SELECT>\n";
#$toolbar = "<a class=iconLink href=\"javascript:add()\">".newIcon()."$button_add</a>";

$functionbar .= "$list_sortby: $select_order";
$functionbar .= "<a href=\"javascript:checkPost(document.form1,'generate.php')\"><img alt='$i_StudentPromotion_GenerateClassNumber' src='/images/admin/button/t_btn_generateclassnumber_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
#$functionbar .= "<a href=\"javascript:alert('generate.php')\"><img alt='$i_StudentPromotion_GenerateClassNumber' src='/images/admin/button/t_btn_generateclassnumber_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$functionbar .= "<a href=\"javascript:checkPost(document.form1,'save.php')\"><img alt='$button_save' src='/images/admin/button/t_btn_save_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'StudentID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";

?>
<SCRIPT LANGUAGE=Javascript>
function add()
{
         newWindow('add.php?ClassID=<?=$ClassID?>',1);
}
globalAlertMsg3 = '<?=$i_StudentPromotion_Alert_RemoveRecord?>';

</SCRIPT>
<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_Menu_EditClassNumber, 'student.php',$ClassName,'') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>

<form name="form1" ACTION="class.php" METHOD="POST">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?=$li->display()?>
<input type=hidden name=field value="<?=$field?>">
<input type=hidden name=order value="<?=$order?>">
<input type=hidden name=pageNo value="<?=$pageNo?>">
<input type=hidden name=ClassID value="<?=$ClassID?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>
