<?php

#####################################
#
#	Date:	2012-07-30	YatWoon
#			update promotion guide link
#
#####################################
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");

switch ($step)
{
        case 1:
             $xmsg = "$i_StudentPromotion_Prompt_PreparationDone"; break;
        case 2:
             $xmsg = "$i_StudentPromotion_Prompt_NewClassEffectiveDone"; break;
        case 3:
             $xmsg = "$i_StudentPromotion_Prompt_DataArchived"; break;
}

?>

<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, '') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>
<script LANGUAGE=Javascript>
function reset()
{
         if (confirm("<?=$i_StudentPromotion_Alert_Reset?>"))
         {
             location.href = "reset.php";
         }
}

function PromotionGuide()
{
	window.open("http://support.broadlearning.com/doc/help/portal/resources/IP_Transition_AG.pdf", "promotio_win");
}
</script>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300>
<blockquote>
<?php 
if($plugin['iPortfolio']==1)
{
	/*$content = displayOption($i_StudentPromotion_Menu_Reset, 'javascript:reset()', 1,
                                $i_StudentPromotion_Menu_ViewEditList, 'student2.php', 1,
                                "$i_StudentPromotion_Menu_EditClassNumber", 'student.php', 1,
                                $i_StudentPromotion_Menu_NewClassEffective, 'effective.php',1,
                                $i_StudentPromotion_Menu_ArchiveLeftStudents, 'archive_left.php',1,
								$i_StudentPromotion_Menu_iPortfolioGroup, 'portfolio_group.php', 1,
								$ip_lang['class_history_management'], 'class_history_years.php', 1
                                );*/
    if($plugin['platform']=='KIS'){
    		$content = displayOption(
							$i_StudentPromotion_Menu_iPortfolioGroup, 'portfolio_group.php', 1,
							$ip_lang['class_history_management'], 'class_history_years.php', 1
							);
    }else{                          
		$content = displayOption(
							$Lang['PromotionGuide'], 'javascript:PromotionGuide();', 1,
							$i_StudentPromotion_Menu_iPortfolioGroup, 'portfolio_group.php', 1,
							$ip_lang['class_history_management'], 'class_history_years.php', 1
							);
    }
}
else
{
	/*$content = displayOption($i_StudentPromotion_Menu_Reset, 'javascript:reset()', 1,
                                $i_StudentPromotion_Menu_ViewEditList, 'student2.php', 1,
                                "$i_StudentPromotion_Menu_EditClassNumber", 'student.php', 1,
                                $i_StudentPromotion_Menu_NewClassEffective, 'effective.php',1,
                                $i_StudentPromotion_Menu_ArchiveLeftStudents, 'archive_left.php',1,
								$ip_lang['class_history_management'], 'class_history_years.php', 1
                                );*/
	$content = displayOption(
							$Lang['PromotionGuide'], 'javascript:PromotionGuide();', 1,
							$ip_lang['class_history_management'], 'class_history_years.php', 1
							);
}
echo $content;
?>
</blockquote>
</td></tr>
</table>

<SCRIPT LANGUAGE=Javascript>
parent.intranet_admin_menu.runStatusWin("");
</SCRIPT>
<?php
include_once("../../templates/adminfooter.php");
?>