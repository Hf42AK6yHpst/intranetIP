<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../includes/libeclass.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");

intranet_opendb();

if ($confirmed != 1)
{
include_once("../../templates/adminheader_intranet.php");

$lsp = new libstudentpromotion();
$assigned = $lsp->returnNumOfAssigned();
$total = $lsp->returnNumOfStudents();
$left = $total - $assigned;

$table_width = ($intranet_session_language=="en"?"300":"200");
?>
<SCRIPT LANGUAGE=Javascript>
function submit2update(){
		document.getElementById('btn_continue').href = 'javascript:void(0)';
        document.form1.action='effective.php';
        parent.intranet_admin_menu.runStatusWin("/admin/pop_processing.php");
        document.form1.submit();
        return;
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_Menu_NewClassEffective, '') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>
<form name="form1" method="GET" action="effective.php" enctype="multipart/form-data">
<blockquote>
<u><strong><?=$i_StudentPromotion_Summary?></strong></u>
<table width=<?=($table_width+100)?> border=1 cellpadding=3 cellspacing=0 align=center >
<tr><td align=right width=<?=$table_width?> nowrap><?php echo $i_StudentPromotion_StudentAssigned; ?>:</td><td width=100><?=$assigned?></td></tr>
<tr><td align=right nowrap><?php echo $i_StudentPromotion_Unset; ?>:</td><td><?=$left?></td></tr>
<tr><td align=right nowrap><?php echo $i_StudentPromotion_AcademicYear; ?>:</td><td><?=getCurrentAcademicYear()?></td></tr>
</table><br>
<?=$i_StudentPromotion_Notes_NewClassEffective?><br><Br>
<?=$i_StudentPromotion_ClickContinueToProceed?>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<a id="btn_continue" href=javascript:submit2update()><image src="<?=$image_path?>/admin/button/s_btn_continue_<?=$intranet_session_language?>.gif" border=0></a>
&nbsp;<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=confirmed value=1>
</form>
<?

}
else
{
    $lsp = new libstudentpromotion();
    # Perform Make New Class Effective
    # Set Academic Year
    $CurrentYear = getCurrentAcademicYear();
    $lsp->setAcademicYear($CurrentYear);
    # Update All Students Class History
    $lsp->updateClassHistory();
    # Update INTRANET_USER to new Class & class number
    $lsp->updateStudentInfoPromotion();
    # Archive Class Groups
    $lsp->archiveGroup();
    # Create New groups
    $lsp->createNewGroups();
    # Build Class-Group Relation
    $lsp->fixClassGroupRelation();
    # Add Students to new Class group
    $lsp->addStuToNewClassGroup();
    # Update INTRANET_USER status -> left for unset students
    $lsp->setUnsetStudentToStatusLeft();


    # Synchronize eClass DB
    $sql = "SELECT UserEmail, Title, EnglishName, ChineseName, FirstName, LastName, UserPassword, ClassName, ClassNumber, UserEmail, Gender FROM INTRANET_USER WHERE RecordType = 2";
    $users = $lsp->returnArray($sql,11);

    $leclass = new libeclass();
    for($i=0; $i<sizeof($users); $i++)
    {
        list($email, $title, $englishname,$chiname, $firstname,$lastname,$password,$class_name,$class_number,$new_email,$gender) = $users[$i];
        $query = $leclass->eClassUserUpdateInfoImport($email, $title, $englishname,$chiname, $firstname,$lastname,"",$class_name,$class_number,"",$gender);
    }
    header ("Location: index.php?step=2");
}

include_once("../../templates/adminfooter.php");

intranet_closedb();
?>
