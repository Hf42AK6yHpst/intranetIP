<?php

//Modify by:

############### Chagne Log Start ####################
#
#	Date:	2010-02-04	YatWoon
#			no need check the class is exists or not -20100204 yatwoon 
#
############### Chagne Log End #################### 

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

#include_once("../../includes/libclass.php");
#include_once("../../includes/libstudentpromotion.php");
intranet_opendb();

$limport = new libimporttext();

$li = new libdb();
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;
$msg = 13;
$duplicateFound = false;

if ($format == 1)
    $format_array = array("UserLogin","Student", "AcademicYear","ClassName","ClassNumber");
else
	$format_array = array("WebSAMSRegNo","Student", "AcademicYear","ClassName","ClassNumber");


if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename)){ 	# import failed
        header("Location: import_history.php");
} else {
		$data = $limport->GET_IMPORT_TXT($filepath);
		$header_row = array_shift($data);		# drop the title bar

        for ($i=0; $i<sizeof($format_array); $i++)
        {
             if ($header_row[$i] != $format_array[$i])
             {	 	             
                 $format_wrong = true;                 
             }
        }
        if ($format_wrong)
        {
            $display .= "<blockquote><br>$i_import_invalid_format <br>\n";
            $display .= "<table width=100 border=1>\n";
            for ($i=0; $i<sizeof($format_array); $i++)
            {
                 $display .= "<tr><td>".$format_array[$i]."</td></tr>\n";
            }
            $display .= "</table></blockquote>\n";
        }
        else
        {

            $sql = "LOCK TABLES
                         INTRANET_USER READ
                         ,PROFILE_CLASS_HISTORY WRITE
                         ,INTRANET_CLASS READ
                         ";
            $li->db_db_query($sql);
  
            
//============================================================================            
            if ($format == 1)           # Use Class and Class number
            {            
	            $fields = "UserID, UserLogin";
     		}else{
	     		$fields = "UserID, WebSAMSRegNo";
	     	}
	      		
	     	
            $sql ="SELECT $fields FROM INTRANET_USER";            
            $user_set = $li->returnArray($sql, 3);
            
            //debug_r($user_set);
            
            foreach($user_set as $key=>$value){
                $aryUID[$value[1]] = $value[0];
                $aryKeyID[$key] = $value[1];
            }
            
            $sql = "SELECT ClassName FROM INTRANET_CLASS";  
            $class_set = $li->returnVector($sql,2);                
         
            $sql = "SELECT UserID, AcademicYear, RecordID FROM PROFILE_CLASS_HISTORY ";                
            $history_set = $li->returnArray($sql);
            
            
            
            for ($i=0; $i<sizeof($data); $i++)
            {                    
                 list($keyLogin,$Student, $AcademicYear,$ClassName,$ClassNumber) = $data[$i];
                
                 if ( $keyLogin != "" && $AcademicYear != "" && $ClassName != "" & $ClassNumber != "")
                 {
	                 
	                 if(($format == 1) ||($format == 2 && $keyLogin[0] == "#")){		                
	                 
	                    # Student and Class Found                        
	                    if(in_array($keyLogin, $aryKeyID)){
	                        /* no need check the class is exists or not -20100204 yatwoon 
		                    if(in_array($ClassName,$class_set)){
			                    */
		                         $recordExists = false;
		                         
		                         $UserID = $aryUID[$keyLogin];
		                         
		                         #check for Duplicated ~ Year + UserID
		                         foreach($history_set as $key=>$value){	
			                         $aryTmpYear = explode('-', $value[1]);
			                         $aryImportYear = explode('-', $AcademicYear);		                         
			                         if(($value[0] == $UserID) && (trim($aryTmpYear[0])==trim($aryImportYear[0]))){
			                         //if($value[0] == $UserID && strpos($value[1], $AcademicYear) !== false){
			                         	$recordExists = true;
			                         	$duplicateFound = true;
			                         	$duplicateRecordID = $value[2];
			                         	break;
		                         	}
			                     }		                         
			                     
	                             if ($recordExists){      		                             
								 	$sql = "UPDATE PROFILE_CLASS_HISTORY SET ClassName = '".trim($ClassName)."',ClassNumber='".trim($ClassNumber)."', DateModified = '".date('Y-m-d h:m:s')."' WHERE UserID = ".$UserID." AND RecordID  = ".$duplicateRecordID;
	                             }else{          
		                             
		                            //Strip away spaces 
		                            if(strrpos($AcademicYear, '-')){
			                            $tmpYr = explode("-", trim($AcademicYear));
			                            if(is_array($tmpYr)){
				                            foreach($tmpYr as $key=>$value){
					                            $tmpYr[$key] = trim($value);
					                        }
					                        $AcademicYear = implode('-', $tmpYr);
				                        }				                        
		                        	}
		                            
	                                $currentTime = date('Y-m-d, H:i:s');
									$sql = "INSERT INTO PROFILE_CLASS_HISTORY (UserID, AcademicYear, ClassName, ClassNumber, DateInput) VALUES ".
											"($UserID, '".trim($AcademicYear)."', '".trim($ClassName)."', '".trim($ClassNumber)."', '".trim($currentTime)."')";
	                             }
	                             
	                             
	                             $li->db_db_query($sql);
	                             $recordValid[$i] = true;
	                         /*}
	                         else{
	                             $recordValid[$i] = false;                             
	                             $reason[$i] = "Class does not exists ($ClassName)";
	                         }*/
	                    }else{
	                        $recordValid[$i] = false;                             
	                        $reason[$i] = "Student not exists ($keyLogin: $Student)";                            
	                    }
                    }else{
	                    $recordValid[$i] = false;                             
	                    $reason[$i] = "Incorrect format for Reg ID, please add '#' in front of numbers: ($keyLogin)";
	                }
                 }
                 else
                 {
                     if ($keyLogin == "" && $AcademicYear == "" && $ClassName == "" & $ClassNumber == ""){
                         break;             # Assume already EOF
                     }
                     else
                     {
                         $recordValid[$i] = false;
                         $reason[$i] = "Some fields missing: ";
                         $delim = "";
                         if ($UserID != ""){
                             if($format==1)
                             	$reason[$i] .= "User Login";
                             else
                             	$reason[$i]	.= "WebSAMSRegNo";
                             $delim = ",";
                         }
                        // if ($AcademicYear != ""){
                             $reason[$i] .= "$delim Academic Year";
                             $delim = ",";
                       ///  }
                       //  if ($ClassName != ""){
                             $reason[$i] .= "$delim Class Name";
                             $delim = ",";
                       //  }
                        // if ($ClassNumber != ""){
                             $reason[$i] .= "$delim Class Number";
                             $delim = ",";
                       //  }
                     }
                 }
            }
            
                      
            
            
            $sql = "UNLOCK TABLES";
            $li->db_db_query($sql);

            # Mark Reason to display
            for ($i=0; $i<sizeof($recordValid); $i++)
            {
                 if (!$recordValid[$i] && $reason[$i]!="")
                 {
                      $display .= "<tr><td>Line ".($i+1)."</td><td>".$reason[$i]."</td></tr>\n";
                 }
            }
            if ($display != "")
            {
                $display = "<table width=90% align=center border=1 cellspacing=0 cellpadding=0>\n$display\n</table>\n";
            }

        }


        if ($display != '')
        {

            include_once("../../templates/adminheader_setting.php");
            echo displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_Menu_ViewEditList, 'javascript:history.back()',$button_import,'');
            echo displayTag("head_studentpromotion_$intranet_session_language.gif", $msg);
            if (!$format_wrong)
            {
                 $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                                <tr><td><hr size=1></td></tr>
                                <tr><td align='right'><a href='javascript:history.go(-2)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";
                 echo "$i_StudentPromotion_ImportFailed<br>";
            }
            else
            {
                 $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                                <tr><td><hr size=1></td></tr>
                                <tr><td align='right'><a href='javascript:history.go(-1)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";
            }
            echo $display;
            include_once("../../templates/adminfooter.php");
            
			intranet_closedb();
        }
        else
        {
	        
	        intranet_closedb();
	        
	        switch($fromPage){		      
		        case "class":
		        	$referer = "class_history_class.php?y=$year";
		        	break;
		        case "student":
		        	$referer = "class_history_student.php?y=$year&c=$class";
		        	break;
		        default:
		        	$referer = "class_history_class.php?y=$year";
		        	break;
		    }
		    
		    ($duplicateFound)? $msg=19:$msg=10;
		    
            header("Location: ".$referer."&msg=".$msg);
        }
}



?>