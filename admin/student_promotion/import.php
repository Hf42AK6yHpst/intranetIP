<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

$linterface = new interface_html();
$limport = new libimporttext();

intranet_opendb();

?>

<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_Menu_ViewEditList, 'javascript:history.back()',$button_import,'') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>
<form name="form1" method="POST" action="import_update.php" enctype="multipart/form-data">
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_select_file; ?>:</td><td><input type=file size=50 name=userfile><br />
<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?></td></tr>
<tr><td align=right nowrap><?=$i_general_Format?>:</td><td>
<input type=radio name=format value=1 CHECKED><?=$i_StudentPromotion_Type_ClassName?> <a class=functionlink href="<?= GET_CSV("format1.csv")?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>  <br>
<input type=radio name=format value=2><?=$i_StudentPromotion_Type_Login?> <a class=functionlink href="<?= GET_CSV("format2.csv")?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>
</td></tr>
</table>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

</form>



<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>