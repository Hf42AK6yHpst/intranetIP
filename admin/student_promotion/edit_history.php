<?php 
//sandy
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../lang/lang.$intranet_session_language.php");

include_once("../../includes/lib.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libmanagehistory.php");

intranet_opendb();
$lmh = new libmanagehistory();
$lu = new libuser();

$x = "";
$js = "";

	
$userId = $lmh->getUserIDByRecordId($chk[0]);
$aryYear = $lmh->doGetAcademicYearByUser($userId);
$data = $lmh->getStudentClassHistory($userId, $optClass);
	

if($data == array() && isset($from)){	
	
	switch($from){		
		case 'duplicate':
			$referer = "handleDuplicate.php?msg=21";
			break;
		case 'problem':				
			$referer = "handleProblematic.php?msg=21";
			break;
		default:
			$referer = "class_history_student.php?msg=20";
			break;
	}
	header("Location:".$referer);
}

include_once("../../templates/adminheader_intranet.php");


$updateCount = sizeof($data);
$uname = $lu->getNameWithClassNumber($userId);
$aryUname = explode("(", $uname);
$name = $aryUname[0];



$x .= "<tr><td align='right' width='150'>".$i_StudentPromotion_mgt['STUDENT'].":</td><td align='left' width='*' colspan='2'>&nbsp;".$name."</td></tr>";
$x .="<tr><td colspan='3' height='10'></td></tr>";
$x .= "<tr><td align='right'></td><td></td><td>".$i_StudentPromotion_mgt['UNCHECKTOREMOVE']."</td></tr>";

#Loop display data 
foreach($data as $key=>$value){
	
	$x .= "<tr><td align='right' width='150'>".$i_StudentPromotion_mgt['ACADEMICYEAR'].":</td><td  width='150'>&nbsp;<input type='text' name='year[$key]' id='year[$key]' value='".$value["AcademicYear"]."' /></td><td  width='*'>";
	$x .= "<input type='checkbox' name='keepRecord[$key]' id='keepRecord[$key]' value='".$value["RecordId"]."' onClick='javascript:changeDisplay($key);' CHECKED />";
	
	if($value['isDuplicate']== "YES")
		$x .= "<font color='red'>*</font>";
	
	$x .= "</td></tr><tr><td align='right'>".$i_StudentPromotion_mgt['CLASS'].":</td><td>&nbsp;";
	$x .= "<input type='text' name='class[$key]' id='class[$key]' value='".$value["ClassName"]."' /></td></tr>";		
	$x .= "<tr><td align='right'>".$i_StudentPromotion_mgt['CLASSNUM'].":</td><td>&nbsp;<input type='text' name='classnum[$key]' id='classnum[$key]' value='".$value["ClassNumber"]."' onKeypress='Javascript:IsNumeric(this.value);' /></td></tr>";
	$x .= "<input type='hidden' name='history[$key]' id='history[$key]' value='".$value["RecordId"]."' />";
	$x .="<tr><td colspan='2' height='10'></td></tr>";		
	
}

$x .= "<tr><td colspan='2'><BR /><font color='red'>".$i_StudentPromotion_mgt['EDIT_NOTE']."</font></td></tr>";


## Build JS 
$js = " var updateCount = $updateCount; ";

## Button Panel
$x .= "<tr><td colspan='2' ><hr /></td></tr>
		<tr><td colspan='2' align='right'>
		<input type='image' src='/images/admin/button/s_btn_save_$intranet_session_language.gif' border='0'>
 		<a href='javascript:resetForm();'><img src='$image_path/admin/button/s_btn_reset_$intranet_session_language.gif' border='0'></a>
 		<a href='javascript:history.back()'><img src='/images/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0'></a></td></tr>
		";

?>


<SCRIPT LANGUAGE=Javascript>

<?=$js?>
var origBg = "";


function validate(){
	var valid = true;
	var errMsg = "";	
	var aryFound = new Array();
	var strDeleteRecordID = "";
	for(i=0; i<updateCount; i++){
		
		if(document.getElementById('keepRecord['+i+']').checked){
			var aryInputYear = (document.getElementById('year['+i+']').value).split('-');
			
			if(aryFound[aryInputYear[0]]){
				document.getElementById('year['+i+']').focus();
				document.getElementById('year['+i+']').select();
				valid = false;					
				errMsg = "<?=$i_StudentPromotion_mgt['YEARDUPLICATE']?>";				
				break;			
			}else{
				aryFound[aryInputYear[0]] = true;
			}			
		}else{
			strDeleteRecordID = strDeleteRecordID!=""? ","+document.getElementById('keepRecord['+i+']').value : document.getElementById('keepRecord['+i+']').value; 
		}
	}	
	
	document.getElementById('strDeleteRecordID').value = strDeleteRecordID;
	
	
	if(valid){
		document.edit.submit();
	}else{
		document.edit.submit = false;
		alert(errMsg);		
	}	
}

function changeDisplay(key){
	
	if(document.getElementById('keepRecord['+key+']').checked){
		document.getElementById('year['+key+']').disabled = false;
		document.getElementById('class['+key+']').disabled = false;
		document.getElementById('classnum['+key+']').disabled = false;
		document.getElementById('year['+key+']').style.backgroundColor = origBg;
		document.getElementById('class['+key+']').style.backgroundColor = origBg;
		document.getElementById('classnum['+key+']').style.backgroundColor = origBg;
	}else{		
		origBg = document.getElementById('classnum['+key+']').style.background;
		document.getElementById('year['+key+']').disabled = true;
		document.getElementById('class['+key+']').disabled = true;
		document.getElementById('classnum['+key+']').disabled = true;
		document.getElementById('year['+key+']').style.backgroundColor = "#EEEEEE";
		document.getElementById('class['+key+']').style.backgroundColor = "#EEEEEE";
		document.getElementById('classnum['+key+']').style.backgroundColor = "#EEEEEE";
	}
}

function resetForm(){
	document.edit.reset()
	
	for(var i=0; i<updateCount; i++){
		document.getElementById('year['+i+']').disabled = false;
		document.getElementById('class['+i+']').disabled = false;
		document.getElementById('classnum['+i+']').disabled = false;
		document.getElementById('year['+i+']').style.backgroundColor = origBg;
		document.getElementById('class['+i+']').style.backgroundColor = origBg;
		document.getElementById('classnum['+i+']').style.backgroundColor = origBg;
	}
}
</SCRIPT>
<?php 

	if(isset($from)){
		
		switch($from){			
			case 'duplicate':
 				echo displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $ip_lang['class_history_management'], 'class_history_years.php', $i_StudentPromotion_mgt['NAV']['PROBLEM'],'handleDuplicate.php?search='.$search, $i_StudentPromotion_mgt['NAV']['EDIT'], '');
 				break;
			case 'problem':			
				echo displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $ip_lang['class_history_management'], 'class_history_years.php', $i_StudentPromotion_mgt['NAV']['PROBLEM'],'handleProblematic.php?search='.$search, $i_StudentPromotion_mgt['NAV']['EDIT'], '');
				break;
			default:
				echo displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $ip_lang['class_history_management'], 'class_history_years.php', $i_StudentPromotion_mgt['NAV']['CLASS'], 'class_history_class.php?y='.$y, $i_StudentPromotion_mgt['NAV']['STUDENT'],'class_history_student.php?y='.$y.'&c='.$c, $i_StudentPromotion_mgt['NAV']['EDIT'], '');
				break;				
		}	 	
	}else{
		displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $ip_lang['class_history_management'], 'class_history_years.php', $i_StudentPromotion_mgt['NAV']['CLASS'], 'class_history_class.php?y='.$y, $i_StudentPromotion_mgt['NAV']['STUDENT'],'class_history_student.php?y='.$y.'&c='.$c, $i_StudentPromotion_mgt['NAV']['EDIT'], '');
	}
	
	
?>
		
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>
<form name="edit" method="POST" action="edit_history_update.php" onSubmit="validate(); return false; ">
<table border="0" cellspacing="5" cellpadding="0" width="560" align="center">
<?=$x?>
<input type="hidden" name="fromYear" value="<?=$optYear?>" />
<input type="hidden" name="fromClass" value="<?=$optClass?>" />
<input type="hidden" name="from" value="<?=$from?>" />
<input type="hidden" name="strDeleteRecordID" id="strDeleteRecordID" value="" />
</table>
</form>
<?php
include_once("../../templates/adminfooter.php");

?>