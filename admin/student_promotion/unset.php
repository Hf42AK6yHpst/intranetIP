<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");
// $li_menu from adminheader_intranet.php
intranet_opendb();

$lsp = new libstudentpromotion();
$students = $lsp->returnUnsetStudentList();

?>
<?=$i_StudentPromotion_Unset_List?>
<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
<tr>
<td class=tableTitle_new width=5>#</td>
<td class=tableTitle_new width=55><?=$i_UserLogin?></td>
<td class=tableTitle_new width=230><?=$i_UserEnglishName?></td>
<td class=tableTitle_new width=130><?=$i_UserChineseName?></td>
<td class=tableTitle_new width=70><?=$i_UserClassName?></td>
<td class=tableTitle_new width=70><?=$i_UserClassNumber?></td>
</tr>
<?
for ($i=0; $i<sizeof($students); $i++)
{
     list ($uid, $login, $engName, $chiName, $class, $classnum) = $students[$i];
?>
<tr>
<td><?=$i+1?></td>
<td><?=$login?></td>
<td><?=$engName?></td>
<td><?=$chiName?></td>
<td><?=$class?></td>
<td><?=$classnum?></td>
</tr>
<?
}
?>
</table>
<?
include_once("../../templates/filefooter.php");

?>