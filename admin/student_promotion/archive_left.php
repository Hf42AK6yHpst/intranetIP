<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../includes/libeclass.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclubsenrol.php");
include_once("../../includes/libform.php");
include_once("../../includes/libwebmail.php");
include_once("../../includes/libftp.php");
include_once("../../includes/libldap.php");
include_once("../../lang/lang.$intranet_session_language.php");

intranet_opendb();

if ($confirmed != 1)
{
include_once("../../templates/adminheader_intranet.php");

$lsp = new libstudentpromotion();
$num_left_students = $lsp->getNumOfLeftStudents();

$table_width = ($intranet_session_language=="en"?"300":"200");
?>
<SCRIPT LANGUAGE=Javascript>
function submit2update(){
        document.form1.action='archive_left.php';
        parent.intranet_admin_menu.runStatusWin("/admin/pop_processing.php");
        document.form1.submit();
        return;
}
</SCRIPT>
<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_Menu_ArchiveLeftStudents, '') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>
<?
#### Check any account has non-zero balance
/*
$sql = "SELECT COUNT(*) FROM PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
               WHERE b.RecordType = 2 AND b.RecordStatus = 3 AND a.Balance >= 0.01";
*/
$sql = "SELECT COUNT(*) FROM PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
			LEFT OUTER JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS c on (c.StudentID = b.UserID)
               WHERE b.RecordType = 2 AND b.RecordStatus = 3 AND (a.Balance >= 0.01 OR c.RecordStatus=0) ";
               
$temp = $lsp->returnVector($sql);
if ($temp[0]>0)
{
    # Stop this action
    /*
    $langfield = getNameFieldByLang("b.");
    
    $sql = "SELECT b.UserLogin, $langfield, b.ClassName, b.ClassNumber, CONCAT('$', FORMAT(a.Balance, 1)) AS Amount
                   FROM PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                   WHERE b.RecordType = 2 AND b.RecordStatus = 3 AND a.Balance >= 0.01";
    
    $result = $lsp->returnArray($sql,5);
    $content = "";
    for ($i=0; $i<sizeof($result); $i++)
    {
         list($t_userLogin,$t_name, $t_className, $t_classNum, $t_balance) = $result[$i];
         $css = ($i%2?"":"2");
         $content .= "<tr class=tableContent$css><td>$t_userLogin</td><td>$t_name</td><td>$t_className</td><td>$t_classNum</td><td>$t_balance</td></tr>\n";
    }
    */
    $langfield = getNameFieldByLang("b.");
    
        $sql = "SELECT b.UserLogin, $langfield, b.ClassName, b.ClassNumber, IF(a.Balance>0.01,CONCAT('<font color=red>$', FORMAT(a.Balance, 1),'</font>'),CONCAT('$', FORMAT(a.Balance, 1))) AS Amount,c.PaymentID,d.Name,CONCAT('$',FORMAT(c.Amount,1))
                   FROM PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                   LEFT OUTER JOIN PAYMENT_PAYMENT_ITEMSTUDENT as c ON (a.StudentID = c.StudentID)
                   LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as d ON (c.ItemID = d.ItemID)
                   WHERE b.RecordType = 2 AND b.RecordStatus = 3 AND (a.Balance >= 0.01 OR c.RecordStatus=0) ";
             
    $result = $lsp->returnArray($sql,8);
    $result2 = array();
    for ($i=0; $i<sizeof($result); $i++)
    {
         list($t_userLogin,$t_name, $t_className, $t_classNum, $t_balance,$payment_id,$item_name,$unpaid_amount) = $result[$i];
	     $result2[$t_userLogin]['name']=$t_name;
	     $result2[$t_userLogin]['class']=$t_className;
	     $result2[$t_userLogin]['classnumber']=$t_classNum;
	     $result2[$t_userLogin]['balance']=$t_balance;
	     if($payment_id!=""){
	     	$result2[$t_userLogin]['items'][]=$item_name." (".$unpaid_amount.")";
	     }
    }
    
    $content = "";

    $j=0;
    foreach($result2 as $user_login => $values){
	    $name = $values['name'];
	    $class= $values['class'];
	    $classnumber = $values['classnumber'];
	    $balance = $values['balance'];
	    if(is_array($values['items'])){
		    $items="<table border=0>";
		  	for($x=0;$x<sizeof($values['items']);$x++){
	    		$items .= "<tr><td><font color=red>-</font></td><td><font color=red>".$values['items'][$x]."</font></td></tr>";
	    	}
	    	$items.="</table>";
		}
	    $j++;
	    $css =$css = ($j%2?"":"2");
        $content .= "<tr class=tableContent$css><td>$user_login</td><td>$name</td><td>$class</td><td>$classnumber</td><td>$balance</td><td>$items</td></tr>\n";

	}    
    
    ?>
<blockquote>
<br>
<span class="extraInfo"><span class=subTitle><?=$i_UserRemoveStop_PaymentBalanceNonZero?></span></span>
<br>
<table width=80% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 >
<!--<tr class=tableTitle><td><?=$i_UserLogin?></td><td><?=$i_UserName?></td><td><?=$i_UserClassName?></td><td><?=$i_UserClassNumber?></td><td><?=$i_Payment_Field_Balance?></td></tr>-->
<tr class=tableTitle><td><?=$i_UserLogin?></td><td><?=$i_UserName?></td><td><?=$i_UserClassName?></td><td><?=$i_UserClassNumber?></td><td><?=$i_Payment_Field_Balance?></td><td><?="$i_Payment_Field_PaymentItem ($i_Payment_Field_Amount)"?></td></tr>

<?=$content?>
</table>
<br><br>
<a class=functionlink_new href="index.php"><?=$button_back?></a>
</blockquote>

    <?
}
else # if ($temp[0]>0)
{

?>
<form name="form1" method="GET" action="archive_left.php" enctype="multipart/form-data">
<blockquote>
<u><strong><?=$i_StudentPromotion_Summary?></strong></u>
<table width=<?=($table_width+100)?> border=1 cellpadding=3 cellspacing=0 align=center >
<tr><td align=right width=<?=$table_width?> nowrap><?php echo $i_StudentPromotion_NumLeftStudents; ?>:</td><td width=100><?=$num_left_students?></td></tr>
<tr><td align=right nowrap><?php echo $i_StudentPromotion_ArchivalYear; ?>:</td><td><?=date('Y')?></td></tr>
</table><br>
<?=$i_StudentPromotion_Notes_ArchiveLeftStudents?><br><Br>
<?=$i_StudentPromotion_ClickContinueToProceed?>
<?
if ($special_feature['alumni'])
{
    echo "<br><br>";
    if ($alumni_system_type==1)
    {
        if ($alumni_GroupID != "")
        {
            $sql = "SELECT GroupID FROM INTRANET_GROUP WHERE GroupID = '".IntegerSafe($alumni_GroupID)."'";
            $temp = $lsp->returnVector($sql);
            if ($temp[0]=="")
            {
                echo $i_StudentPromotion_AutoAlumni_NoGroupExist;
            }
            else
            {
                echo $i_StudentPromotion_AutoAlumni;
            }
        }
        else
        {
            echo $i_StudentPromotion_AutoAlumni_NoGroupSet;
        }
    }
    else
    {
        echo $i_StudentPromotion_NoAutoAlumniAdd;
    }
}
?>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<a href=javascript:submit2update()><image src="<?=$image_path?>/admin/button/s_btn_continue_<?=$intranet_session_language?>.gif" border=0></a>
&nbsp;<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input type=hidden name=confirmed value=1>
</form>
<?
} # if ($temp[0]>0)
}
else # if ($confirmed != 1)
{
    $lsp = new libstudentpromotion();
    $lu = new libuser();
    # Perform Archival of Data
    # Set Academic Year
    $CurrentYear = getCurrentAcademicYear();
    $lsp->setAcademicYear($CurrentYear);

    # Get List of students in status Left
    $students = $lu->returnLeftStudentIDList();
    if (sizeof($students)==0)
    {
        header("Location: index.php?step=3&failed=1");
        exit();
    }
    $list = implode(",",$students);

    # Archive Student Information
    $lsp->archiveStudentsInfo($list);
    # Archive Process of remove users
    $lu->prepareUserRemoval($list);

    # If alumni auto , change record type and add to alumni
    # else remove from INTRANET_USER
    if ($special_feature['alumni'] && $alumni_system_type==1)
    {
        $lu->setToAlumni($list);
    }
    else
    {
        $lu->removeUsers($list);
    }


    header ("Location: index.php?step=3");
} # if ($confirmed != 1)

include_once("../../templates/adminfooter.php");

intranet_closedb();
?>
