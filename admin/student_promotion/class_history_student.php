<?php
#Modify by : 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
include_once("../../includes/libmanagehistory.php");

$classExist = false;

intranet_opendb();

$lmh = new libmanagehistory();

$aryYear = $lmh->getYearList();
$aryClassList = $lmh->getClassListByYear($y,false);
$aryStudentList = $lmh->getStudentListByClass($y,$c);

## [ SEARCH ] - Year
$displayOption .= "<div id='panelYear' style='float:left;'>".$i_StudentPromotion_mgt['ACADEMICYEAR'].": <select name='optYear' id='optYear' onChange='javascript:refreshClassnameOpt();'>";
foreach($aryYear as $key=>$value){
	$displayOption .= "<option value='$value'";
	if(isset($y) && $y == $value)
		$displayOption .= " SELECTED";
	$displayOption .= ">$value</option>";
	
	$arySearchClassList[$value] = $lmh->getClassListByYear($value,false);
}
$displayOption .= "</select ></div>";


## [ SEARCH ] - Class Name
$js = "var classList = new Array(); ";
foreach($arySearchClassList as $key1=>$aryYear){
	$js .= " classnameList = new Array(); ";
	foreach($aryYear[0] as $key2=>$value)
		$js .=  "classnameList[$key2] = '".$value['ClassName']."';";
		
	$js .=  "classList[$key1] = classnameList;";
}

$displayOption .= "<div id='panelClass' style='float:left;'>".$i_StudentPromotion_mgt['CLASS'].": <select name='optClass' id='optClass'>";
if($aryClassList[0] != array()){
	foreach($aryClassList[0] as $key=>$value){
		$displayOption .= "<option value='".$value[1]."'";
		if(isset($c) && $c == $value[1]){
			$displayOption .= " SELECTED";
			$classExist = true;
		}
		$displayOption .= ">".$value[1]."</option>";
	}
}else{
	$displayOption .= "<option value='--'>--</option>";
}
$displayOption.= "</select></div>";
$displayOption.= "<div id='panelBtn' style='float:left;'>&nbsp;<input type='button' name='go' value='".$i_StudentPromotion_mgt['BTNSEARCH']."' onClick='prepareSubmit(\"search\",\"\")' /></div>";


## When coming from [SEARCH - Year + Classname] , class 'XX' in Year '0000' may not exist. 
## So Get student list from the 1st Classname in Selected Year
if(!$classExist){
	if(isset($aryClassList[0][0][1]))
		$aryStudentList = $lmh->getStudentListByClass($y, $aryClassList[0][0][1]);	
}

## Build display Data
$x .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr>
<td class=tableTitle width=100>".$i_StudentPromotion_mgt['CLASSNUM']."</td>
<td class=tableTitle width=370>".$i_StudentPromotion_mgt['STUDENT']."</td>
<td class=tableTitle width=370>".$i_StudentPromotion_mgt['CLASSHISTORY']."</td>
<td class=tableTitle width=20><input type='checkbox' name='chkall' onClick=\"Javascipt:(this.checked)?setChecked(1,document.form1,'chk[]'):setChecked(0,document.form1,'chk[]')\" /></td>
</tr>\n";

if($aryStudentList != array()){
	for ($i=0; $i<sizeof($aryStudentList); $i++)
	{
		
		$css = ($i%2) ? "2" : "";     
		
		$aryStudentList[$i]['classnumber'] = (!empty($aryStudentList[$i]['classnumber']))?str_pad($aryStudentList[$i]['classnumber'], 2, '0', STR_PAD_LEFT):"--";
	     $x .= "<tr>
	     		<td class=tableContent$css>".$aryStudentList[$i]['classnumber']."</td>
	     		<td class=tableContent$css>".$aryStudentList[$i]['name']."&nbsp;</td>
	     		<td class=tableContent$css width=370>".$aryStudentList[$i]['history']."</td>
	     		<td class=tableContent$css align='center'><input type='checkbox' name='chk[]' value='".$aryStudentList[$i]['recordId']."' /></td></tr>";
	}
}else{
	$x .= "<tr><td colspan='4' align='center' style='vertical-align: middle' height='40'>".$i_StudentPromotion_mgt['NORECORD']."</td></tr>";
}
$x .= "</table>\n";


$msg = 1;

###################################################

?>
<SCRIPT LANGUAGE=Javascript>

<?=$js?>
	function prepareSubmit(type, id){	
		switch(type){
			case 'remove':
				if(confirm('<?=$i_StudentPromotion_mgt['CONFIRMDELETE'][0]?>'+id)){					
					location.href= "remove_history.php?from=3&y=<?=$y?>&c=<?=$c?>&id="+id;			
				}
				break;
			case 'search':
				location.href= "class_history_student.php?from=search&y="+document.getElementById('optYear').value+"&c="+document.getElementById('optClass').value;		
				break;	
			efault:
				break;	
		}
	}
	
	function refreshClassnameOpt(){
		
		var selectedOpt = 0;
		var selectedYear = document.getElementById('optYear').value;		
		var listSize = classList[selectedYear].length;		
		
		var panel = document.getElementById('panelClass');
		var oldClassOpt = document.getElementById('optClass');
		var oldSelectedOpt = oldClassOpt.value;
		
		panel.removeChild(oldClassOpt);
		
		var newSelect = document.createElement('Select');
		newSelect.id = 'optClass';
		newSelect.name = 'optClass';
		
		for(var i=0; i<listSize; i++){
			
			newOpt = document.createElement('option');
			newOpt.innerHTML = classList[selectedYear][i];
			newOpt.value = classList[selectedYear][i];
			newSelect.appendChild(newOpt);
			
			if(oldSelectedOpt == classList[selectedYear][i])
				selectedOpt = i;
		}
		
		panel.appendChild(newSelect);
		document.getElementById('optClass').selectedIndex = selectedOpt;
	}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_mgt['NAV']['YEAR'], 'class_history_years.php', $i_StudentPromotion_mgt['NAV']['CLASS'], 'class_history_class.php?y='.$y, $i_StudentPromotion_mgt['NAV']['STUDENT'],'') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>

<form name="form1" post="POST" action="">
<a name="#top" />
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="25"><?=$displayOption ?></td></tr>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu>
<div style='float:left;'><a class=iconLink href="javascript:checkGet(document.form1,'import_history.php?y=<?=$y?>&c=<?=$c?>')"><?=importIcon()."$button_import"?></a><a class=iconLink href="javascript:checkGet(document.form1,'export_history_update.php')"><?=exportIcon()."$button_export"?></a></div>
<div style='float:right;'><a class=iconLink href='javascript:checkEdit(document.form1,"chk[]","edit_history.php")'><img src="<?=$image_path?>/admin/button/t_btn_edit_<?=$intranet_session_language?>.gif" border='none'></a>
<a href='javascript:checkRemove(document.form1, "chk[]", "remove_history.php");'><img src="<?=$image_path?>/admin/button/t_btn_delete_<?=$intranet_session_language?>.gif" border="none" ></a>&nbsp;</div></td>
</tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?=$x?>


<input type="hidden" name="from" value="3" />

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
<tr><td align='right' valign='center' height='30'><BR /><a href="#top"><?=$i_StudentPromotion_mgt['BACKTOTOP']?></a></td></tr>
</table>

<input type="hidden" name="fromPage" value="student" />
</form>
<?php
include_once("../../templates/adminfooter.php");
?>