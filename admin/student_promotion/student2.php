<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");

intranet_opendb();

$lsp = new libstudentpromotion();
$classStatus = build_assoc_array($lsp->getAssignedStatus());
$classSize = build_assoc_array($lsp->returnNumOfStudentsByClass());

$totalNum = $lsp->returnNumOfStudents();
$classes = $lsp->getClassList();

$x .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr>
<td class=tableTitle width=300>$i_StudentPromotion_OldClass</td>
<td class=tableTitle width=260>$i_StudentPromotion_StudentAssigned/$i_StudentPromotion_ClassSize</td>
</tr>\n";
$total = 0;
for ($i=0; $i<sizeof($classes); $i++)
{
     list ($classid,$classname) = $classes[$i];
     $count = $classStatus[$classname] +0;
     $size = $classSize[$classname] + 0;
     $total += $count;
     $css = ($i%2) ? "2" : "";
     $x .= "<tr>
             <td class=tableContent$css><a class=functionlink href=\"class2.php?ClassName=$classname\">$classname</a></td>
             <td class=tableContent$css>$count/$size</td>
            </tr>\n";
}
$count = $totalNum - $total;

$x .= "</table>\n";

?>
<SCRIPT LANGUAGE=Javascript>

</SCRIPT>

<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_Menu_ViewEditList, '') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>

<form name=form1 action=''>
</form>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><a class=iconLink href="javascript:checkGet(document.form1,'import.php')"><?=importIcon()."$button_import"?></a></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?=$x?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<?php
include_once("../../templates/adminfooter.php");
?>