<?php

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");

intranet_opendb();
$li = new libdb();

if (isset($prefer_view) && isset($time_format) && isset($working_hours_start) && 
	isset($working_hours_end) && isset($disable_repeat) && isset($disable_guest)) {
	
	$sql = "UPDATE CALENDAR_CONFIG SET ";
	$sql .= "Value = '$prefer_view' WHERE Setting = 'PreferredView'";
	$li->db_db_query($sql);
	
	$sql = "UPDATE CALENDAR_CONFIG SET ";
	$sql .= "Value = '$time_format' WHERE Setting = 'TimeFormat'";
	$li->db_db_query($sql);
	
	$sql = "UPDATE CALENDAR_CONFIG SET ";
	$sql .= "Value = '$working_hours_start' WHERE Setting = 'WorkingHoursStart'";
	$li->db_db_query($sql);
	
	$sql = "UPDATE CALENDAR_CONFIG SET ";
	$sql .= "Value = '$working_hours_end' WHERE Setting = 'WorkingHoursEnd'";
	$li->db_db_query($sql);
	
	$sql = "UPDATE CALENDAR_CONFIG SET ";
	$sql .= "Value = '$disable_repeat' WHERE Setting = 'DisableRepeat'";
	$li->db_db_query($sql);
	
	$sql = "UPDATE CALENDAR_CONFIG SET ";
	$sql .= "Value = '$disable_guest' WHERE Setting = 'DisableGuest'";
	$li->db_db_query($sql);
	
	intranet_closedb();
	header("Location: settings.php?msg=2");
} else {
	header("Location: settings.php");
}
?>