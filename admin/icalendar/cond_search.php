<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../lang/ical_lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

# Make Temp table
$li = new libdb();
$sql = "CREATE TABLE IF NOT EXISTS TEMP_ICALENDAR_COND_REMOVE (
	ID int(11) NOT NULL,
	PRIMARY KEY (ID)
)";
$li->db_db_query($sql);

$sql = "DELETE FROM TEMP_ICALENDAR_COND_REMOVE";
$li->db_db_query($sql);
?>
<?= displayNavTitle($i_adminmenu_fs, '', $i_Calendar_Admin_Setting, 'index.php', $i_Calendar_New_RemoveBySearch, 'cond_search.php') ?>
<?= displayTag("$img_tag", $msg) ?>
<form name="form1" action="cond_search_result.php" method="post">
<blockquote>
<table width="560" border="0" cellpadding="4" cellspacing="0" align="center">
	<tr>
		<td align="right"><?=$i_Calendar_SearchRemoval_Type?>:</td>
		<td>
			<select name="type">
				<option value="event"><?=$i_Calendar_SearchRemoval_TypeOption1?></option>
				<option value="calendar"><?=$i_Calendar_SearchRemoval_TypeOption2?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td align="right"><?=$i_Calendar_SearchRemoval_Phase?>:</td>
		<td>
			<input type="text" name="keyword" size="40" maxlength="255">
		</td>
	</tr>
</table>
<table width="560" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td height="22" style="vertical-align:bottom"><hr size="1"></td>
	</tr>
	<tr>
		<td align="right">
			<input type="image" src="/images/admin/button/s_btn_find_<?=$intranet_session_language?>.gif" border="0">
	 		<?=btnReset()?>
		</td>
	</tr>
</table>
</blockquote>
<input type="hidden" name="newsearch" value="1">
</form>
<?php
include_once("../../templates/adminfooter.php");
?>