<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../lang/ical_lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_Calendar_Admin_Setting, '') ?>
<?= displayTag("$img_tag", $msg) ?>

<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td class="tableContent" height="300">
			<blockquote>
				<?= displayOption(
					$i_general_BasicSettings, 'settings.php', 1,
					$i_Calendar_ForceRemoval, 'remove.php', 1,
					$i_Calendar_New_RemoveBySearch,'cond_search.php',1
				) ?>
			</blockquote>
		</td>
	</tr>
</table>
<?
include_once("../../templates/adminfooter.php");
?>