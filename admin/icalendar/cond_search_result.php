<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/ical_lang.$intranet_session_language.php");
include_once("../../lang/lang.$intranet_session_language.php");

if(!isset($keyword) || !isset($type)) {
	header("Location: cond_search.php?newsearch=1");
}

include_once("../../templates/adminheader_intranet.php");


intranet_opendb();

# Create Search Result
if ($newsearch == 1) {
    $ldb = new libdb();
    $sql = "DELETE FROM TEMP_ICALENDAR_COND_REMOVE";
    $ldb->db_db_query($sql);
    
    # Search result
    $sql = "INSERT IGNORE INTO TEMP_ICALENDAR_COND_REMOVE (ID) ";
    if ($type == "event") {
		$sql .= "SELECT EventID FROM CALENDAR_EVENT_ENTRY ";
		$sql .= "WHERE (Title like '%$keyword%' OR Description like '%$keyword%')";
	} else if ($type == "calendar") {
		$sql .= "SELECT CalID FROM CALENDAR_CALENDAR ";
		$sql .= "WHERE (Name like '%$keyword%' OR Description like '%$keyword%')";
	}
    $ldb->db_db_query($sql);
}


# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
switch ($field){
	case 0: $field = 0; break;
	case 1: $field = 1; break;
	case 2: $field = 2; break;
	case 3: $field = 3; break;
	default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
$namefield = getNameFieldWithClassNumberByLang("u.");

$li = new libdbtable($field, $order, $pageNo);

if ($type == "event") {
	$sql  = "SELECT m.EventDate, $namefield AS OwnerName, m.Title, m.Description, ";
	$sql .= "CONCAT('<input type=checkbox name=eventID[] value=', m.EventID, '>') ";
	$sql .= "FROM TEMP_ICALENDAR_COND_REMOVE AS r ";
	$sql .= "LEFT OUTER JOIN CALENDAR_EVENT_ENTRY AS m ON m.EventID = r.ID ";
	$sql .= "LEFT OUTER JOIN INTRANET_USER AS u ON m.UserID = u.UserID";
	
	# TABLE INFO
	$li->field_array = array("m.EventDate","OwnerName","m.Title","m.Description");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	$li->title = "";
	$li->column_array = array(0,0,0,0);
	$li->wrap_array = array(0,0,0,0);
	$li->IsColOff = 2;
	
	# TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width='1' class='tableTitle'>#</td>\n";
	$li->column_list .= "<td width='15%' class='tableTitle'>".$li->column($pos++, $iCalendar_NewEvent_EventDate)."</td>\n";
	$li->column_list .= "<td width='25%' class='tableTitle'>".$li->column($pos++, $iCalendar_EditEvent_CreatedBy)."</td>\n";
	$li->column_list .= "<td width='25%' class='tableTitle'>".$li->column($pos++, $iCalendar_NewEvent_Title)."</td>\n";
	$li->column_list .= "<td width='35%' class='tableTitle'>".$li->column($pos++, $iCalendar_NewEvent_Description)."</td>\n";
	$li->column_list .= "<td width='1' class='tableTitle'>".$li->check("eventID[]")."</td>\n";
	
	$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'eventID[]','cond_search_remove.php')\">";
	$functionbar .= "<img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
} else if ($type == "calendar") {
	$sql  = "SELECT $namefield AS OwnerName, m.Name, m.Description, ";
	$sql .= "CONCAT('<input type=checkbox name=calID[] value=', m.CalID, '>') ";
	$sql .= "FROM TEMP_ICALENDAR_COND_REMOVE AS r ";
	$sql .= "LEFT OUTER JOIN CALENDAR_CALENDAR AS m ON m.CalID = r.ID ";
	$sql .= "LEFT OUTER JOIN INTRANET_USER AS u ON m.Owner = u.UserID";
	
	# TABLE INFO
	$li->field_array = array("OwnerName","m.Name","m.Description");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	$li->title = "";
	$li->column_array = array(0,0,0);
	$li->wrap_array = array(0,0,0);
	$li->IsColOff = 2;
	
	# TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width='1' class='tableTitle'>#</td>\n";
	$li->column_list .= "<td width='25%' class='tableTitle'>".$li->column($pos++, $iCalendar_EditEvent_CreatedBy)."</td>\n";
	$li->column_list .= "<td width='35%' class='tableTitle'>".$li->column($pos++, $iCalendar_NewCalendar_Name)."</td>\n";
	$li->column_list .= "<td width='40%' class='tableTitle'>".$li->column($pos++, $iCalendar_NewCalendar_Description)."</td>\n";
	$li->column_list .= "<td width='1' class='tableTitle'>".$li->check("calID[]")."</td>\n";
	$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'calID[]','cond_search_remove.php')\">";
	$functionbar .= "<img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
}

?>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_fs, '', $i_Calendar_Admin_Setting, 'index.php', $i_Calendar_New_RemoveBySearch, 'cond_search.php') ?>
<?= displayTag("$img_tag", $msg) ?>

<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td>
	</tr>
	<tr>
		<td class="admin_bg_menu"><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td>
	</tr>
	<tr>
		<td><img src="../../images/admin/table_head1.gif" width="560" height="7" border="0"></td>
	</tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td>
	</tr>
</table>
<br />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="keyword" value="<?=str_replace('"', "&quot;", stripslashes($keyword))?>" />
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>