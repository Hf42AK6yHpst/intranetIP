<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libteaching.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();
$lu = new libuser($tid);
$name = $lu->EnglishName;
$lteaching = new libteaching();
?>

<script language="javascript">
function checkform(obj){
         return true;
}
</script>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_sc, '', $i_adminmenu_adm_teaching,  'javascript:history.back()', $name, '') ?>
<?= displayTag("head_teaching_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<?=$lteaching->displayTeacherEdit($tid)?>
</blockquote>
</td></tr></table>
<input type=hidden name=tid value="<?=$tid?>">
<p>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</p>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>