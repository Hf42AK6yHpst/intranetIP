<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libteaching.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();
$lu = new libuser($tid);
$name = $lu->EnglishName;
$lteaching = new libteaching();
?>
<script language="javascript">
function checkform(obj){
    return true;
}

function jSELECT_CLASSES(jID)
{
	jCIDList = eval("document.form1.class_id_list"+jID+".value");
	url = "reportcard_add_class.php?ID="+jID+"&CIDList="+jCIDList;
	newWindow(url,15);
}

function jADD_CLASSES(jSID, jCIDList, jCNameList)
{
	document.getElementById("class_list"+jSID).innerHTML = jCNameList;
	var jObj = eval("document.form1.class_id_list"+jSID);
	jObj.value = jCIDList;
}
</script>

<form name="form1" action="reportcard_edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_sc, '', $i_adminmenu_adm_teaching,  'index.php', $i_ReportCard_System_Setting,  'reportcard_index.php', $name, '') ?>
<!--<?= displayTag("head_teaching_$intranet_session_language.gif", $msg) ?>-->
<!-- Temporary heading, a heading image needed -->
<table width='560' border='0' cellspacing='0' cellpadding='0' align='center'>
<tr><td class='13-blue'><?=$i_ReportCard_System_Setting?></td></tr>
<tr><td height='25' align='right'><hr size=1><?=$xmsg?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<?=$lteaching->displayReportCardTeacherEdit($tid)?>
</blockquote>
</td></tr></table>
<input type=hidden name=tid value="<?=$tid?>" />
<input type=hidden name=SelectedClass />
<p>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
<!--<?= btnReset() ?>-->
<a href="javascript:self.location.reload();"><img src='/images/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif' border='0'></a>
&nbsp;<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</p>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>