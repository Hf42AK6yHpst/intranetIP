<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libteaching.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$lteaching = new libteaching();
$toolbar = "<a class=iconLink href=\"import.php\">".importIcon()."$button_import</a>";
?>

<?= displayNavTitle($i_adminmenu_sc, '', $i_adminmenu_adm_teaching, 'index.php', $i_ReportCard_System_Setting, '') ?>
<!--<?= displayTag("head_teaching_$intranet_session_language.gif", $msg) ?>-->
<!-- Temporary heading, a heading image needed -->
<table width='560' border='0' cellspacing='0' cellpadding='0' align='center'>
<tr><td class='13-blue'><?=$i_ReportCard_System_Setting?></td></tr>
<tr><td height='25' align='right'><hr size=1><?=$xmsg?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td colspan="2"><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td colspan="2" class=admin_bg_menu><?php echo $toolbar; ?></td></tr>
<tr><td colspan="2"><?=$lteaching->displayReportCardTeachingTable(0)?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<?php
include_once("../../templates/adminfooter.php");
?>