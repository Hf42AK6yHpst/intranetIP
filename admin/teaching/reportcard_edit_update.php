<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

# Update Subjects
$sql = "DELETE FROM RC_SUBJECT_TEACHER WHERE UserID = $tid";
$li->db_db_query($sql);
for ($i=0; $i<$size; $i++)
{
	$SubjectID = ${"subject$i"};
	if($SubjectID>0)
	{
		$ClassIDList = ${"class_id_list".$i};
		$ClassArray = explode(",", $ClassIDList);
		for($j=0; $j<sizeof($ClassArray); $j++)
		{
			$cid = $ClassArray[$j];
			if($cid>0)
			{
				$sql = "INSERT INTO RC_SUBJECT_TEACHER (UserID, ClassID, SubjectID, DateInput, DateModified)
						 VALUES ($tid, $cid, $SubjectID, now(), now())";
				$li->db_db_query($sql);
			}
		}
	}
}

intranet_closedb();
header("Location: reportcard_index.php?msg=2");

?>