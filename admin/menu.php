<?php
## Using By : 

/********************** Change Log ***********************/
#	Date	:	2017-09-25 [Paul]
#				Do no display the tab "network" if flag disableNetworkFeature is on as cloud client does not require it
#
#	Date	:	2017-01-05 [Villa]
#				Modified header logo depended on the region
#
#	Date	:	2016-01-19 [Carlos]
#				Modified always do logout with /adminlogout.php
#
#	Date	:	2010-10-11 [YatWoon]
#			Fixed: wordings are hardcode and the charset missing to cater GB version.
#
# 	Date	:	2010-07-21 [Kelvin]
#	Details	:	check if client has any special room. if not, hide the special menu
#
#
# 	Date	:	2010-01-27 [Yuen]
#	Details	:	change Question Mark to provide entrance to eClass Community and eClass Version Log
#
/******************* End Of Change Log *******************/


include("../includes/global.php");
include("../lang/lang.$intranet_session_language.php");
$server = $HTTP_SERVER_VARS['HTTP_HOST'];
/*
$logout_alert = ($intranet_session_language=="b5"?"由於 Microsoft Internet Explorer 不再支援一種特別的 URL 格式 (Security Update: 832894)，登出系統時，請關閉所有瀏覽器視窗。詳情請參閱 http://support.microsoft.com/default.aspx?scid=kb%3ben-us%3b834489"
:"As Microsoft Internet Explorer no longer supports a URL syntax (Security Update: 832894), to logout the Admin Concole, please close all IE windows. For details, please go to http://support.microsoft.com/default.aspx?scid=kb%3ben-us%3b834489");

$ns_logout_alert = ($intranet_session_language=="b5"?"請關閉所有瀏覽器視窗以登出":"Please close all browser windows to logout");
*/

$logout_alert = $Lang['logout_alert'];
$ns_logout_alert = $Lang['ns_logout_alert'];

if (is_file("../includes/version.php"))
{
	$JustWantVersionData = true;
	include_once("../includes/version.php");
	$eClassVersion = $versions[0][0];
} else
{
	$eClassVersion = "--";
}
# get the URL of this site
$PageURLArr = explode("/", curPageURL());
if (sizeof($PageURLArr)>1)
{
	$strPath = $PageURLArr[1];
	$eClassURL = $PageURLArr[0] . "//" . substr($strPath, 0, strpos($strPath, "/"));
}
# generate the URL and key based on server time
$TimeNow = time();
if (isset($intranet_version_url) && $intranet_version_url!="")
{
	$version_url = $intranet_version_url;
} else
{
	# by default
	$version_url = "hke-version.eclass.com.hk";
}
$products_path = "http://{$version_url}/?eclasskey=".md5(date("Y-m-d-H", $TimeNow))."&eClassURL=".$eClassURL."&eClassCheck=1";

#check if client has any special room
include_once("../includes/libdb.php");
include_once("../includes/lib.php");
intranet_opendb();
$li = new libdb();
$sql = "SELECT count(*) from ".$eclass_db.".course where roomtype!=0";
$count_obj = $li->returnVector($sql);
$has_special_room = ($count_obj[0])>0?true:false;



### UI preparation for about system
$bubbleMsgBlockSysAbout =<<<sysAbout

	<div class="sub_layer_board" id="sub_layer_sys_about" style="position:absolute;visibility:hidden;z-index:100;width:270px;top:0px;left:500px;">
		<table bgcolor="white" border="0" cellpadding="2" cellspacing="0" width="270px" style="border:2px #2288AA dotted;">
			<tr>
				<td aligh="right"><a href="#" title="{$Lang['Btn']['Close']}" onclick="MM_showHideLayers('sub_layer_sys_about','','hide')"><img src="/images/2009a/addon_tools/close_layer.gif" border="0" style="float:right"/></a></td>
			</tr>
			<tr>
				<td align="center">{$i_eu_current_ver}: <a href="javascript:products()">{$eClassVersion}</a></td>
			</tr>
			<tr>
				<td valign="top" align="center" height="30px"><a href="javascript:support()">{$Lang['Header']['eClassCommunity']}</a></td>
			</tr>
		</table>
	</div>

	

sysAbout;

/*
$isIE = (strpos($HTTP_USER_AGENT, "MSIE")) ? 1 : 0;

if ($isIE)
{
    $logout_link = "$intranet_httppath/adminlogout.php";
}
else
{
    $logout_link = "javascript:alert('$ns_logout_alert');"; #"http://logout:logout@$server/logout/";
#    $logout_link = "http://logout:logout@$server/logout/";
}
*/
$logout_link = "$intranet_httppath/adminlogout.php";

#$logout_link = "javascript:alert('$logout_alert');"; #"http://logout:logout@$server/logout/";
#$logout_link = "javascript:en()";
#$logout_link = "http://$server/logout/";
# Get news from broadlearning.com
$cs_server = $BroadlearningSupportSite;
$cs_link = "/home/support.php";
$US_SystemName=1;           //  0=eClass, 1=Intranet, 2=eClassJunior
$US_SystemType=1;          //     0=Frontend, 1=Backend
$US_URL=$HTTP_HOST.$SCRIPT_NAME;  //the function that he is triggering
$US_Email="";                       // user email address
$US_ClientName=$BroadlearningClientName;     // client name
$US_UserType="";     // user type: Junior: teacher/student...?
$US_Language=$intranet_session_language;
$query_string = "US_SystemName=$US_SystemName&US_SystemType=$US_SystemType&US_URL=$US_URL&US_Email=$US_Email&US_ClientName=$US_ClientName&US_UserType=$US_UserType&US_Language=$US_Language";


if ($BroadlearningCheckNews && $cs_server != "")
{
    $news_link = "$cs_server/lookup/userchecknews.php";
    $news_path = "$news_link?$query_string";
    $fp = @fopen ($news_path, "r");
    $US_Alert = 0;
    if ($fp)
    {
        $US_Alert = @fgets($fp,10);
        $US_Alert = rtrim($US_Alert);
    }
}
else
{
    $US_Alert = 0;
}
if (isset($US_Alert) && $US_Alert>=1 )
{
    $cs_path = "$cs_link?$query_string";
    $cs_icon = "<a href=\"javascript:support()\"><img alt='$i_CustomerSupport' src=$image_path/$image_cs_admin_blink border=0></a>";
}
else
{
    $cs_path = "$cs_link?$query_string";
    $cs_icon = "<a href=\"javascript:support()\"><img alt='$i_CustomerSupport' src=$image_path/$image_cs_admin border=0></a>";
}

$charset = $intranet_session_language == "gb" ? "GB2312":"big5";

// debug_pr($intranet_session_language);
if($intranet_session_language == "en"){
	if(get_client_region()=="zh_MY" || get_client_region()=="zh_CN"||0) {
		$headfile = 'gb';
		$onClick = 'gb()';
	}else{
		$headfile = 'b5';
		$onClick = 'b5()';
	}
}else{
	$headfile = 'en';
	$onClick = 'en()';
}
// $intranet_default_lang_set = array('en','gb');
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$charset?>">
<script language=JavaScript1.2 src=/templates/script.js></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function support()
{
         newWindow("<?=$cs_path?>",10);
}

function products()
{
	newWindow("<?=$products_path?>",10);
}
//-->
</script>
<script language="JavaScript1.2">
function en(){
//         return;
     top.intranet_admin_main.location.href = "/lang.php?lang=en&url=" + top.intranet_admin_main.location;
     location = "/lang.php?lang=en&url=" + location;
}
function b5(){
<?
if(isset($intranet_default_lang_set))
{
	if (!in_array("b5",$intranet_default_lang_set))
	{?>
		return;
<?
	}
}
?>
		top.intranet_admin_main.location.href = "/lang.php?lang=b5&url=" + top.intranet_admin_main.location;

     location = "/lang.php?lang=b5&url=" + location;
}
function gb(){
<?
if(isset($intranet_default_lang_set))
{
	if (!in_array("gb",$intranet_default_lang_set))
	{?>
		return;
<?
	}
}
else
{?>
		return;
<?
}
?>
     top.intranet_admin_main.location.href = "/lang.php?lang=gb&url=" + top.intranet_admin_main.location;
     location = "/lang.php?lang=gb&url=" + location;
}
function exit(){
     top.location.href="/logout.php";
}
function main(){
     top.intranet_admin_main.location.href = "/admin/main.php";
         restoreImages(5);
}
function setting(){
     top.intranet_admin_main.location.href = "/admin/main_setting.php";
         document.MM_sr[0].oSrc = "/images/admin/header/tab_intranetsetting_h_<?=$intranet_session_language?>.gif";
         restoreImages(0);
}
function intranet(){
     top.intranet_admin_main.location.href = "/admin/main_intranet.php";
         document.MM_sr[0].oSrc = "/images/admin/header/tab_intranetmanage_h_<?=$intranet_session_language?>.gif";
         restoreImages(1);
}
function eclass(){
     top.intranet_admin_main.location.href = "/admin/main_eclass.php";
         document.MM_sr[0].oSrc = "/images/admin/header/tab_specialroom_h_<?=$intranet_session_language?>.gif";
         restoreImages(2);
}
function network(){
     top.intranet_admin_main.location.href = "/admin/main_network.php";
         document.MM_sr[0].oSrc = "/images/admin/header/tab_systemmanage_h_<?=$intranet_session_language?>.gif";
         restoreImages(3);
}
function restoreImages(not_this){
        MM_swapImgRestore();
        if (not_this!=0)
        {
                restoreImageThis("imgSetting", "/images/admin/header/tab_intranetsetting_<?=$intranet_session_language?>.gif");
        }
        if (not_this!=1)
        {
                restoreImageThis("imgManage", "/images/admin/header/tab_intranetmanage_<?=$intranet_session_language?>.gif");
        }
        if (not_this!=2)
        {
                restoreImageThis("imgEclass", "/images/admin/header/tab_specialroom_<?=$intranet_session_language?>.gif");
        }
        if (not_this!=3)
        {
                restoreImageThis("imgSystem", "/images/admin/header/tab_systemmanage_<?=$intranet_session_language?>.gif");
        }
}
function restoreImageThis(imgName, newSrc){
        MM_swapImage(imgName,'',newSrc,1);
        document.MM_sr[0].oSrc = newSrc;
}

var statusWin = null;
function runStatusWin(myURL){
        if (myURL!="")
        {
                newWindow(myURL, 18);
                statusWin = newWin;
        } else if (statusWin!=null)
        {
                statusWin.close();
                statusWin = null;
        }

        return;
}

function loadContent(evt,contentType){
	if (contentType == 'sys_about') {
		//bubble_block = document.getElementById("sub_layer_sys_about");
		//document.getElementById("ajaxMsgBlock2").style.display = "block";
	}
}
</script>
<!--<link href="/templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">-->
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('/images/admin/header/tab_intranetsetting_o_<?=$intranet_session_language?>.gif','/images/admin/header/tab_intranetmanage_o_<?=$intranet_session_language?>.gif','/images/admin/header/tab_specialroom_o_<?=$intranet_session_language?>.gif','/images/admin/header/tab_systemmanage_o_<?=$intranet_session_language?>.gif')" bgcolor="#F0FAFA">

<table width="780" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="175" rowspan="2"><a href="javascript:main()"><img src="/images/admin/header/logo.gif" border="0" height="78"></a></td>
    <td width="605"><img src="/images/admin/header/btn_e_g_s_l_<?=$headfile?>.gif" border="0" usemap="#MapMenu"></td>
  </tr>
  <tr>
    <td background="/images/admin/header/cellbg.gif">
    	<a href="javascript:setting()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgSetting','','/images/admin/header/tab_intranetsetting_o_<?=$intranet_session_language?>.gif',1)">
    		<img src="/images/admin/header/tab_intranetsetting_<?=$intranet_session_language?>.gif" name="imgSetting" border="0">
    	</a>
    	<a href="javascript:intranet()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgManage','','/images/admin/header/tab_intranetmanage_o_<?=$intranet_session_language?>.gif',1)">
    		<img src="/images/admin/header/tab_intranetmanage_<?=$intranet_session_language?>.gif" name="imgManage" border="0">
    	</a>
    	<?if ($has_special_room){?>
    	<a href="javascript:eclass()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgEclass','','/images/admin/header/tab_specialroom_o_<?=$intranet_session_language?>.gif',1)">
    		<img src="/images/admin/header/tab_specialroom_<?=$intranet_session_language?>.gif" name="imgEclass" border="0">
    	</a>
    	<?}?>
    	<?if (!$sys_custom['admin']['disableNetworkFeature']){?>
    	<a href="javascript:network()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgSystem','','/images/admin/header/tab_systemmanage_o_<?=$intranet_session_language?>.gif',1)">
    		<img src="/images/admin/header/tab_systemmanage_<?=$intranet_session_language?>.gif" name="imgSystem" border="0">
    	</a>
    	<?}?>
    </td>
  </tr>
  <tr>
    <td colspan="2"><img src="/images/admin/header/menu_separator.gif" border="0"></td>
  </tr>
</table>
<?=$bubbleMsgBlockSysAbout?>
<map name="MapMenu">
        <area shape="circle" coords="510,16,13" href="javascript:<?=$onClick ?>">
        <area shape="circle" coords="546,24,12" href="javascript:MM_showHideLayers('sub_layer_sys_about','','show');loadContent(event,'sys_about')">
        <area alt="<?=$i_general_logout?>" shape="circle" coords="581,40,13" href="<?=$logout_link?>" target="_top">
</map>
</body>
</html>