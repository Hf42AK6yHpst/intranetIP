<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libeclass.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

eclass_opendb();

$limport = new libimporttext();

// YuEn: temporary
if ($MemberType=="")
{
	die ("No member type (S, T or A) selected!");
}

$format_array = array("UserLogin","CourseID","Group");
$li = new libdb();
$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;
if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename)){ 	# import failed
        header("Location: import_member.php?failed=2");
        exit();
} else {
        /*
        $ext = strtoupper($lf->file_ext($filename));
        if($ext == ".CSV") {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $lf->file_read_csv($filepath);
                $toprow = array_shift($data);                   # drop the title bar
        }
        */
        $data = $limport->GET_IMPORT_TXT($filepath);
		$toprow = array_shift($data);		# drop the title bar
		
        for ($i=0; $i<sizeof($format_array); $i++)
        {
             if ($toprow[$i] != $format_array[$i])
             {
                 header("Location: import_member.php?msg=13");
                 exit();
             }
        }
        $lo = new libeclass();

        # tidy up data
        $courses = array();
        for ($i=0; $i<sizeof($data); $i++)
        {
             list($user_login,$course_id,$group_name) = $data[$i];
            // echo "$i: $user_login,$course_id,$group_name<br>";

		if (!is_array($users[$user_login]))
		{
		     $sql = "SELECT UserID, UserEmail, UserPassword, ClassNumber, FirstName, LastName, ClassName, CASE Title WHEN 0 THEN 'MR' WHEN 0 THEN 'MR.' WHEN 1 THEN 'MISS.' WHEN 2 THEN 'MRS.' WHEN 3 THEN 'MS.' WHEN 4 THEN 'DR.' WHEN 5 THEN 'PROF.' END , EnglishName, ChineseName, NickName, Gender FROM INTRANET_USER WHERE UserLogin='$user_login' ";
		     $row = $li->returnArray($sql,12);
		     for($j=0; $j<sizeof($row); $j++)
		     {
		          # if ($lo->max_user <> "" && $j == $lo->ticketUser()) break;
		          $UserID = $row[$j][0];
		          $UserEmail = $row[$j][1];
		          $UserPassword = $row[$j][2];
		          $ClassNumber = $row[$j][3];
		          $FirstName = $row[$j][4];
		          $LastName = $row[$j][5];
		          $ClassName = $row[$j][6];
		          $Title = $row[$j][7];
		          $EngName = $row[$j][8];
		          $ChiName = $row[$j][9];
		          $NickName = $row[$j][10];
		          $Gender = $row[$j][11];
		          $eclassClassNumber = (trim($ClassName) == "") ? $ClassNumber : trim($ClassName)." - ".trim($ClassNumber);
		          /*
		          if ($ClassNumber=="" || $ClassName=="" || stristr($ClassNumber,$ClassName))
		              $eclassClassNumber = $ClassNumber;
		          else
		              $eclassClassNumber = $ClassName ." - ".$ClassNumber;
		          */
		          $users[$user_login] = array($UserEmail, $Title,$FirstName,$LastName, $EngName, $ChiName, $NickName, $MemberType, $UserPassword, $eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info,$group_name);
		     }
		}
		if (!in_array($course_id, $courses))
		{
			# Eric Yip (20090421): check if course_id exists in db
			$lo = new libeclass($course_id);
  		if($lo->course_id == "")
  		{
  			eclass_closedb();
  			header("Location: index.php?msg=14");
  		}

			$courses[] = $course_id;
		}
		$course_user[$course_id][] = $user_login;
        }

        # add records
        for ($i=0; $i<sizeof($courses); $i++)
        {
        	$course_id = $courses[$i];
        	$lo = new libeclass($course_id);
        	        	
        	for ($j=0; $j<sizeof($course_user[$course_id]); $j++)
        	{
        		if ($lo->max_user<>"" && $j==$lo->ticketUser()) break;
        		$user_login = $course_user[$course_id][$j];
        		list($UserEmail,$Title,$FirstName,$LastName, $EngName, $ChiName, $NickName, $MemberType, $UserPassword, $eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info,$ClassName) = $users[$user_login];
        		if ($UserEmail!="" && ($FirstName!="" || $LastName!="" || $EngName!="" || $ChiName!=""))
        		{
		      	$lo->eClassUserAddFullInfo($UserEmail, $Title,$FirstName,$LastName, $EngName, $ChiName, $NickName, $MemberType, $UserPassword, $eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info,$ClassName);
		      }
        	}
        	$lo->eClassUserNumber($lo->course_id);
        }
}
eclass_closedb();

header("Location: index.php?msg=1");
?>