<?php
@SET_TIME_LIMIT(600);
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../lang/email.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libeclass.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_eclass.php");
include_once("$eclass_filepath/system/settings/lang/admin/".substr($lang, 0, strpos($lang, ".")).".language.php");

intranet_opendb();

$li = new libdb();
$li->db = classNamingDB($eclass_db);

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field=="") $field=1;
if($filter=="") $filter=0;
if($range=="") $range=0;

// build temp duration table
$sql = "CREATE TEMPORARY TABLE ec_login_hits (
		course_id int(8),
		hit_total int(8),
		hit_T int(8),
		hit_S int(8),
		hit_A int(8)
		)";
$li->db_db_query($sql);
$sql = "SELECT course_id, RoomType FROM {$eclass_db}.course ";
$row_all = $li->returnArray($sql,2);

$now = time();
$today = date('Y-m-d',$now)." 23:59:59";
if($searchBy==="")
{
	$searchBy = 0;
}
else if($searchBy==1 && $start=="" && $end=="")
{
	$start = date('Y-m-d',$now);
	$end = $start;
}
for ($i=0; $i<sizeof($row_all); $i++)
{
	list($cid, $rtype) = $row_all[$i];
	
	if ($searchBy==1)
	{
		if($start!="" && $end!="" && $start!=$end)
		{
			$sql_range = "AND UNIX_TIMESTAMP(lu.inputdate) BETWEEN UNIX_TIMESTAMP('$start') AND (UNIX_TIMESTAMP('$end')+86400) ";
		}
		else 
		{
			if($start==$end || $end=="")
				$exact_date = $start;
			else if($start=="")
				$exact_date = $end;

			$sql_range = "AND UNIX_TIMESTAMP(lu.inputdate) BETWEEN UNIX_TIMESTAMP('$exact_date') AND (UNIX_TIMESTAMP('$exact_date')+86400) ";
		}
	}
	else
	{
		switch ($range){
		case 0: // today
			$sql_range = ($rtype==0) ? "AND TO_DAYS('".$today."')=TO_DAYS(lu.inputdate)" : "WHERE TO_DAYS('".$today."')=TO_DAYS(ls.inputdate)";
			break;
		case 1: // last week
			$sql_range = ($rtype==0) ? "AND UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(lu.inputdate)<=604800" : "WHERE UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(lu.inputdate)<=604800";
			break;
		case 2: // last 2 weeks
			$sql_range = ($rtype==0) ? "AND UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(lu.inputdate)<=1209600" : "WHERE UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(lu.inputdate)<=1209600";
			break;
		case 3: // last month
			$sql_range = ($rtype==0) ? "AND UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(lu.inputdate)<=2678400" : "WHERE UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(lu.inputdate)<=2678400";
			break;
		case 4: // all
			$sql_range = " ";
			break;
		}
	}

	if($rtype==0)
	{
		$sql = "SELECT count(*), u.memberType from ".classNamingDB($cid).".login_usage as lu LEFT JOIN ".classNamingDB($cid).".login_session AS ls ON ls.login_session_id=lu.login_session_id LEFT JOIN ".classNamingDB($cid).".usermaster AS u ON u.user_id=ls.user_id WHERE (lu.function_name like '%/src/course/contents/index.php' OR lu.function_name like '%/src/student/work_student/index.php'  OR lu.function_name like '%/src/assessment/selftest/index.php' OR lu.function_name like '%/src/assessment/practice/index.php' OR lu.function_name like '%/src/assessment/testroom/index.php' OR lu.function_name like '%/src/student/information/message_box/index.php' OR lu.function_name like '%/src/social_corner/bulletin/index.php' OR lu.function_name like '%/src/social_corner/directory/index.php' OR lu.function_name like '%/src/course/resources/links/help.php' OR lu.function_name like '%/src/course/resources/files/index.php') $sql_range group by u.memberType";
	}
	else
	{
		$sql = "SELECT count(*), u.memberType from ".classNamingDB($cid).".login_session AS ls LEFT JOIN ".classNamingDB($cid).".usermaster AS u ON u.user_id=ls.user_id $sql_range group by u.memberType";
	}
	$row = $li->returnArray($sql, 2);

	$sql_values = array();
	$total_in_course = 0;
	for ($j=0; $j<sizeof($row); $j++)
	{
		list($total, $memberType) = $row[$j];
		$sql_values[$memberType] = $total;
		$total_in_course += $total;
	}
	if (sizeof($sql_values)>0)
	{
		$sql = "INSERT INTO ec_login_hits VALUES ($cid, $total_in_course, ".((int) $sql_values['T']).", ".((int) $sql_values['S']).", ".((int) $sql_values['A']).") ";
	}
	$li->db_db_query($sql);
}

$fieldname  = "if(a.RoomType=0,CONCAT('<a class=tableContentLink href=\"javascript:viewCourse(', a.course_id, ')\">', a.course_name, '</a>'), a.course_name), ";
$fieldname .= "ifnull(h.hit_total, 0), ";
$fieldname .= "ifnull(h.hit_T, 0), ";
$fieldname .= "ifnull(h.hit_S, 0), ";
$fieldname .= "ifnull(h.hit_A, 0) ";
//$sql  = "SELECT DISTINCT $fieldname FROM course AS a ";
//$sql .= "LEFT JOIN ec_login_hits as h ON h.course_id=a.course_id ";
$sql  = "SELECT DISTINCT $fieldname FROM course AS a, ec_login_hits as h ";
$sql .= "WHERE h.course_id=a.course_id AND a.course_name like '%$keyword%' ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.course_name", "h.hit_total", "h.hit_T", "h.hit_S", "h.hit_A");
$li->db = classNamingDB($eclass_db);
$li->sql = $sql;
$li->title = $admin_user;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $i_admintitle_eclass;
$li->column_array = array(0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1% height='20' background='$BackTitle' class=tableTitle>&nbsp;#&nbsp;</td>\n";
$li->column_list .= "<td background='$BackTitle' class=tableTitle>".$li->column(0, $i_eClassCourseName)."</td>\n";
$li->column_list .= "<td background='$BackTitle' class=tableTitle>".$li->column(1, $i_eClassHitsTotal)."</td>\n";
$li->column_list .= "<td background='$BackTitle' class=tableTitle>".$li->column(2, $i_eClassHitsTeacher)."</td>\n";
$li->column_list .= "<td background='$BackTitle' class=tableTitle>".$li->column(3, $i_eClassHitsStudent)."</td>\n";
$li->column_list .= "<td background='$BackTitle' class=tableTitle>".$li->column(4, $i_eClassHitsAssistant)."</td>\n";

if($searchBy==0)
{
	$periodSelectTable  .= "<select name=range onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
	$periodSelectTable .= "<option value=0 ".(($range==0)?"selected":"").">$admin_range1</option>\n";
	$periodSelectTable .= "<option value=1 ".(($range==1)?"selected":"").">$admin_range2</option>\n";
	$periodSelectTable .= "<option value=2 ".(($range==2)?"selected":"").">$admin_range3</option>\n";
	$periodSelectTable .= "<option value=3 ".(($range==3)?"selected":"").">$admin_range4</option>\n";
	$periodSelectTable .= "<option value=4 ".(($range==4)?"selected":"").">$admin_range5</option>\n";
	$periodSelectTable .= "</select> \n";

	$searchDisplay1 = $periodSelectTable;
	$searchDisplay2 = "<a href=\"javascript:changeDateTable(1)\" class='iconLink'>".$i_Profile_SearchByDate."</a>\n";
}
else
{
	$dateSearchTable .= "$i_Profile_From <input type=text name=start size=10 value='$start'> $i_Profile_To <input type=text name=end size=10 value='$end'> <span class=extraInfo>(yyyy-mm-dd) <a href='javascript:document.form1.pageNo.value=1;document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>\n";

	$searchDisplay1 = "<a href=\"javascript:changeDateTable(0)\" class='iconLink'>".$i_Profile_SearchByPeriod."</a>";
	$searchDisplay2 = $dateSearchTable;
}

$searchbar = "<input size=10 type=text name=keyword  maxlength=255 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<script language="javascript">
function viewCourse(id){
	var obj = document.form1;
	obj.course_id.value = id;
	obj.pageNo.value = 1;
	obj.field.value = (obj.field.value>0) ? obj.field.value - 1 : 0;
	obj.action = "participation_course.php";
	obj.submit();
	return;
}

function changeDateTable(type)
{
	var formObj = document.form1;
	formObj.searchBy.value = type;
	formObj.pageNo.value=1;
	formObj.submit();
}
</script>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_Stats, '../', $i_eClass_Admin_stats_participation, '') ?>
<?= displayTag("head_eclassstatisitcs_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td colspan="2"><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu valign="top" height="35">&nbsp;<?=$searchDisplay1?></td><td nowrap="nowrap" class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td colspan="2" class=admin_bg_menu valign="bottom">&nbsp;<?=$searchDisplay2?></td></tr>
<tr><td colspan="2"><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?php echo $li->display(); ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=course_id value="<?=$course_id?>">
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=searchBy value="<?=$searchBy?>"/>

<p></p>
</form>

<?php
intranet_closedb();
include("../../../../templates/adminfooter.php");
?>