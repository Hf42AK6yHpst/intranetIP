<?php
include_once("../../../../includes/global.php");
include_once("../../../../lang/email.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libeclass.php");
//include_once("../../../../includes/libdbtable.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_eclass.php");
include_once("$eclass_filepath/src/includes/php/lib-quota.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass_filepath/system/settings/lang/".$lang);

intranet_opendb();

$lo = new libeclass($course_id);

$sys_db = $eclass_db;
$image_path = $eclass_url_root."/images";
$qo = new quota($course_id);
$fm = new fileManager($course_id, $categoryID, $folderID);
?>


<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_Stats, '../', $i_eClass_Admin_stats_files, './', $lo->course_code ." ". $lo->course_name, '') ?>
<?= displayTag("head_eclassstatisitcs_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu align="center"><img src="<?=$image_path?>/icon/resources/files.gif" border='0' hspace='5' align='absmiddle'><b><span class=indextitle><?=$file_usage?></span></b></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?= $qo->statusStorageDetailIP() ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<p></p>
</form>

<?php
intranet_closedb();
include("../../../../templates/adminfooter.php");
?>