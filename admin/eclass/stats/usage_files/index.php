<?php
@SET_TIME_LIMIT(600);
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../lang/email.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libeclass.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_eclass.php");
include_once("$eclass_filepath/src/includes/php/lib-quota.php");
include_once("$eclass_filepath/system/settings/lang/".$lang);

intranet_opendb();

$qo = new quota($eclass_db, 0);
$qo->calAllCourseFileUsage($eclass_db);

$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";


# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field == "") $field = 3;

$sql = "SELECT
		   CONCAT('<a class=tableContentLink href=\"usage_course.php?course_id=', course_id, '\">', course_name, '</a>'),
		   ifnull(max_storage, '-'),
		   file_no,
		   file_size,
		   (max_storage-file_size) AS storage_left
	  FROM
			{$eclass_db}.course_file_usage
	  WHERE
           (course_name like '%$keyword%')
	  ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("course_name", "max_storage", "file_no", "file_size", "storage_left");
$li->db = $eclass_db;
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $i_admintitle_eclass;
$li->column_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=40% class=tableTitle>".$li->column(0, $i_eClassCourseName)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(1, $i_eClassMaxStorage)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(2, $i_eClassFileNo)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(3, $i_eClassFileSize)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(4, $i_eClassFileSpace)."</td>\n";
?>


<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_Stats, '../', $i_eClass_Admin_stats_files, '') ?>
<?= displayTag("head_eclassstatisitcs_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?php echo $li->display(); ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<p></p>
</form>

<?php
intranet_closedb();
include("../../../../templates/adminfooter.php");
?>