<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libeclass.php");

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

eclass_opendb();

$limport = new libimporttext();

$format_array = array("CourseCode","CourseName","NumUsers","MaxStorage","KLA");
$li = new libdb();
$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename)){ 	# import failed
        header("Location: import_new.php?failed=2");
        exit();
} else {
        /*
		$ext = strtoupper($lf->file_ext($filename));
        if($ext == ".CSV") {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $lf->file_read_csv($filepath);
                $toprow = array_shift($data);                   # drop the title bar
        }*/
        
        $data = $limport->GET_IMPORT_TXT($filepath);
		$toprow = array_shift($data);		# drop the title bar
		
        for ($i=0; $i<sizeof($format_array); $i++)
        {
             if ($toprow[$i] != $format_array[$i])
             {
                 header("Location: import_new.php?msg=13");
                 exit();
             }
        }
        $lo = new libeclass();
        for ($i=0; $i<sizeof($data); $i++)
        {
             list($course_code,$course_name,$max_user,$max_storage,$KLA) = $data[$i];
             $course_id = $lo->eClassAdd($course_code, $course_name, "", $max_user, $max_storage);
             $subj_id = array($KLA);
             $lo->eClassSubjectUpdate($subj_id, $course_id);
        }

}
eclass_closedb();

header("Location: index.php?msg=1");
?>