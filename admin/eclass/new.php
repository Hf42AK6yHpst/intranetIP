<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libeclass.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libhomework.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("eclass_common.php");
eclass_opendb();


# eClass
$lo = new libeclass();
$eclass_quota = $lo->status();

//$lo->getChildrenSpecialCourseMenu(6039);

$lgroup = new libgroup();
$groups = $lgroup->returnGroupsByCategory(3);
$group_select = getSelectByArrayTitle($groups,"name=GroupID",$i_notapplicable);

$lhw = new libhomework();
$subjects = $lhw->returnSubjectTable();
$subject_select = getSelectByArrayTitle($subjects,"name=SubjectID",$i_notapplicable);

$nameSelected = "ELPSelected";

include_once("../../templates/adminheader_eclass.php");
?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.course_code, "<?php echo $i_alert_pleasefillin.$i_eClassCourseCode; ?>.")) return false;
     if(!check_text(obj.course_name, "<?php echo $i_alert_pleasefillin.$i_eClassCourseName; ?>.")) return false;
	if (obj.RoomType.value==3)
	{
		if (obj.survey_date.value!="")
		{
			if(!check_date(obj.survey_date, "<?=$i_invalid_date?>")) return false;
		}
		if (obj.StartDate.value!="")
		{
			if(!check_date(obj.StartDate, "<?=$i_invalid_date?>")) return false;
		}
		if (obj.EndDate.value!="")
		{
			if(!check_date(obj.EndDate, "<?=$i_invalid_date?>")) return false;
		}
		if (obj.Remark.checked && !obj.Remark.disabled)
		{
			alert("<?=$i_ssr_anonymous_warning?>");
		}
	}
}
</script>


<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, 'javascript:history.back()', $button_new." (".$str_RoomType.")", '') ?>
<?= displayTag("head_eclass_$intranet_session_language.gif") ?>

<?php
$user_quota = "30";
$file_quota = "30";
if ($RoomType == 0)
{
    if($lo->license == "" || $lo->ticket()>0)
    {
       $valid = true;
    }
    else $valid = false;
} elseif ($RoomType==3)
{
	$valid = ($lo->ssr_left>0);
} elseif ($RoomType==1)
{
	$valid = ($lo->license_readingrm == 0 || $lo->ticket(1)>0);
} elseif ($RoomType==2)
{
	$valid = true;
} elseif ($RoomType==4)
{
	$valid = true;
	$user_quota = "";
	$file_quota = "";
} else
{
	$valid = false;
}

if ($valid)
{
    $tool_select = "";
    # Check Equation Editor
    if ($lo->license_equation !=0)
    {
        $content = trim(get_file_content($lo->filepath."/files/equation.txt"));
        $equation_class = ($content=="")? array(): explode(",",$content);
        $left = $lo->license_equation - sizeof($equation_class);
        if ($left > 0)
        {
            $tool_select .= "<input type=checkbox name=hasEquation value=1> $i_eClass_Tool_EquationEditor ($i_eClass_Tool_LicenseLeft: $left)";
        }
    }
?>
<table width=500 border=0 cellpadding=2 cellspacing=1>

<?php if ($RoomType==2) {
	$nameSelected = "ELPSelected";
?>
<tr><td align=right><?= $i_eClass_elp_courseware ?>:</td><td><?= $lo->getELPList() ?></td></tr>
<?php } elseif ($RoomType==3) {
	$nameSelected = "SSRSelected";
	$nameExtra = " ".date("Y");
?>
<tr><td align=right><?= $i_eClass_ssr_type ?>:</td><td><?= $lo->getSSRList() ?></td></tr>
<?php } ?>

<tr><td align=right><?php echo $i_eClassCourseCode; ?>:</td><td><input class=text type=text name=course_code size=10 maxlength=10></td></tr>
<tr><td align=right><?php echo $i_eClassCourseName; ?>:</td><td><input class=text type=text name=course_name size=30 maxlength=100></td></tr>
<tr><td align=right><?php echo $i_eClassCourseDesc; ?>:</td><td><textarea name=course_desc cols=30 rows=5></textarea></td></tr>
<tr><td align=right><?php echo $i_eClassNumUsers; ?>:</td><td><input class=text type=text name=max_user size=4 maxlength=5 value="<?=$user_quota?>"></td></tr>
<tr><td align=right><?php echo $i_eClassMaxStorage; ?>:</td><td><input class=text type=text name=max_storage size=4 maxlength=5 value="<?=$file_quota?>"> MB</td></tr>

<?php if ($RoomType==3) {
?>
<tr><td align=right><?= $i_eClass_survey_deadline ?>:</td><td><input class=text name="survey_date" value="" size=10 maxlength=10> <span class="extraInfo">(yyyy-mm-dd)</span></td></tr>
<tr><td align=right><?= $i_CycleSpecialStart ?>:</td><td><input class=text name="StartDate" value="<?=date('Y-m-d')?>" size=10 maxlength=10> <span class="extraInfo">(yyyy-mm-dd)</span></td></tr>
<tr><td align=right><?= $i_CycleSpecialEnd ?>:</td><td><input class=text name="EndDate" value="" onClick="if (this.value=='') {this.value='<?=date("Y-m-d", mktime(0,0,0,date("m")+1,date("d"),date("Y")))?>';}" size=10 maxlength=10> <span class="extraInfo">(yyyy-mm-dd)</span></td></tr>
<tr><td align=right><?= $i_Survey_Anonymous ?>:</td><td><input type="checkbox" name="Remark" value="NAME_BLOCKED" <?=$name_blocked_checked?> ></td></tr>
<?php } ?>

<?php if ($RoomType==4) {
?>
<tr><td align=right><?=$i_eClass_function_access?>:</td><td>
<table border="0" cellpadding="3" cellspacing="0">
<tr><td><input type="checkbox" name="is_function_web" checked ></td><td><input class=text name="function_web_e" value="<?=$i_eClass_portfolio_web_e?>" size=10 maxlength=16><?=$i_eClass_portfolio_lang_e?></td></tr>
<tr><td>&nbsp;</td><td><input class=text name="function_web_c" value="<?=$i_eClass_portfolio_web_c?>" size=10 maxlength=16><?=$i_eClass_portfolio_lang_c?></td></tr>
<tr><td><input type="checkbox" name="is_function_form" checked ></td><td><input class=text name="function_form_e" value="<?=$i_eClass_portfolio_form_e?>" size=10 maxlength=16><?=$i_eClass_portfolio_lang_e?></td></tr>
<tr><td>&nbsp;</td><td><input class=text name="function_form_c" value="<?=$i_eClass_portfolio_form_c?>" size=10 maxlength=16><?=$i_eClass_portfolio_lang_c?></td></tr>
</table>
</td></tr>
<tr><td align=right><?=$i_eClass_parent_access?>:</td><td><input type=checkbox name="is_parent" ></td></tr>
<?php } ?>

<?php if ($RoomType==0) { ?>
<tr><td align=right><?php echo $i_eClass_ClassLink; ?>:</td><td><?=$group_select?></td></tr>
<tr><td align=right><?php echo $i_eClass_SubjectLink; ?>:</td><td><?=$subject_select?><br><?=$i_eClass_ClassSubjectNote?></td></tr>
<? if ($tool_select != "") { ?>
<tr><td align=right><?php echo $i_eClass_Tools; ?>:</td><td><?=$tool_select?></td></tr>
<? } ?>
<tr><td><br></td><td><?php echo $lo->eClassCategory(); ?></td></tr>
<?php } ?>

</table>
<p>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>
</p>
<?php }
else
{
	echo "<blockquote>$i_eClassLicenseFull</blockquote>";
}
?>
<input type="hidden" name="RoomType" value="<?=$RoomType?>">
</form>

<script language="JavaScript">
if (typeof(document.form1.course_name)!="undefined" && typeof(document.form1.<?=$nameSelected?>)!="undefined") { document.form1.course_name.value = document.form1.<?=$nameSelected?>.options[document.form1.<?=$nameSelected?>.selectedIndex].text+"<?=$nameExtra?>"; }
</script>

<?php
eclass_closedb();
include_once("../../templates/adminfooter.php");
?>