<?php
# using: 

/* change log
 * 
 * 2013-02-21 Yuen - fixed the missing of save button for reading room by checking RoomType
 * 
 */

include_once("../../../includes/global.php");
//include_once("../../../includes/libdb.php");
include_once("$eclass_filepath/src/includes/php/lib-db.php");
include_once("../../../includes/libeclass.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("$eclass_filepath/src/includes/php/lib-groups.php");
include_once("../../../lang/lang.$intranet_session_language.php");
eclass_opendb();

# eClass
$lo = new libeclass($course_id);
$RoomType = $lo->getEClassRoomType($course_id);

$right_ids = "1,2,3";

$lg = new libgroups($eclass_prefix."c$course_id");

// reading room
if ($RoomType==0)
{
	header("Location: ../");
}

//echo $lg->teacherAssistantListIn(1, "RIGHTS");
//echo $lg->getIpfTeacherAssistantListIn(1, "RIGHTS",$course_id);

//echo $lg->teacherAssistantListOut(1, "RIGHTS");
//echo $lg->getIpfTeacherAssistantListOut(1, "RIGHTS",$course_id);
include("../../../templates/adminheader_eclass.php");
?>

<script language="javascript">
function checkform(obj)
{
	
		
	var right_ids = "<?=$right_ids?>";
	var tmp_arr = right_ids.split(",");
		
	for (var j=0; j<tmp_arr.length; j++)
	{			
		target_obj = eval("obj.elements['target"+tmp_arr[j]+"[]']");
		if (typeof (target_obj) != "undefined")
		{		
			checkOption(target_obj);
			for(i=0; i<target_obj.length; i++)
			target_obj.options[i].selected = true;
		}
	}
		
	return true;
}
</script>

<form name="form1" method="post" action="teacher_rights_update.php" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, "../index.php?RoomType=$RoomType", $lo->course_code ." ". $lo->course_name, "./?course_id=$course_id", $i_eClass_set_teacher_rights, '') ?>
<?= displayTag("head_eclass_namelist_$intranet_session_language.gif", $msg) ?>

<?php if ($RoomType==4) {?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>&nbsp;</td></tr>
<tr><td>
iPortfolio Admin Right Management is no longer in use.  To perform your settings, go to <strong>School Settings > Role</strong> of the front-end.<br /><br />
學習檔案權限管理已經停用。要進行有關設定，請前往前台 <strong>學校基本設定  > 身份角色</strong>。
<tr><td>&nbsp;</td></tr>
</td></tr>
</table>
<?php
}
//iPF Admin Right setting has moved to School Setting >  Role on 22 Aug 2012

	?>
<!--table width=560 border=0 cellpadding=2 cellspacing=0 align="center">
<tr><td colspan="3" nowrap><hr size=1 class="hr_sub_separator"><span class="extraInfo">[<span class=subTitle><?=$i_eClass_teacher_rights_1?></span>]</span></td></tr>
<tr><td width="30%" align="right">
	<select name="target1[]" class='inputfield' size=7 multiple>
	<?php echo $lg->getIpfTeacherAssistantListIn(1, "RIGHTS",$course_id); ?>

	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
	</select>
	</td>
	<td align=center nowrap width="10%"><br>
	<p><input type='image' src='/images/admin/button/s_btn_addto_<?=$intranet_session_language?>.gif' border='0' onClick='checkOptionTransfer(this.form.elements["source1[]"],this.form.elements["target1[]"]);return false;'></p>
	<p><input type='image' src='/images/admin/button/s_btn_deleteto_<?=$intranet_session_language?>.gif' border='0' onClick='checkOptionTransfer(this.form.elements["target1[]"],this.form.elements["source1[]"]);return false;'></p>
	</td>
	<td width="50%">
	<select class='inputfield' name="source1[]" size=7 multiple>
	<?php echo $lg->getIpfTeacherAssistantListOut(1, "RIGHTS",$course_id); ?>
	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
	</select>
	</td>
</tr></table-->
<?php

?>
<?php
if ($RoomType==1)
{
?>

<table width=500 border=0 cellpadding=2 cellspacing=0 align="center">
<tr><td colspan="3" nowrap><span class="extraInfo">[<span class=subTitle><?=$i_eClass_teacher_rights_1?></span>]</span></td></tr>
<tr><td width="30%" align="right">
	<select name="target1[]" class='inputfield' size=7 multiple>
	<?php echo $lg->teacherAssistantListIn(1, "RIGHTS"); ?>
	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
	</select>
	</td>
	<td align=center nowrap width="10%"><br>
	<p><input type='image' src='/images/admin/button/s_btn_addto_<?=$intranet_session_language?>.gif' border='0' onClick='checkOptionTransfer(this.form.elements["source1[]"],this.form.elements["target1[]"]);return false;'></p>
	<p><input type='image' src='/images/admin/button/s_btn_deleteto_<?=$intranet_session_language?>.gif' border='0' onClick='checkOptionTransfer(this.form.elements["target1[]"],this.form.elements["source1[]"]);return false;'></p>
	</td>
	<td width="50%">
	<select class='inputfield' name="source1[]" size=7 multiple>
	<?php echo $lg->teacherAssistantListOut(1, "RIGHTS"); ?>
	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
	</select>
	</td>
</tr></table>


<table width=560 border=0 cellpadding=2 cellspacing=0 align="center">
<tr><td colspan="3" nowrap><hr size=1 class="hr_sub_separator"><span class="extraInfo">[<span class=subTitle><?=$i_eClass_teacher_rights_2?></span>]</span></td></tr>
<tr><td width="30%" align="right">
	<select name="target2[]" class='inputfield' size=7 multiple>
	<?php echo $lg->teacherAssistantListIn(2, "RIGHTS"); ?>
	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
	</select>
	</td>
	<td align=center nowrap width="10%"><br>
	<p><input type='image' src='/images/admin/button/s_btn_addto_<?=$intranet_session_language?>.gif' border='0' onClick='checkOptionTransfer(this.form.elements["source2[]"],this.form.elements["target2[]"]);return false;'></p>
	<p><input type='image' src='/images/admin/button/s_btn_deleteto_<?=$intranet_session_language?>.gif' border='0' onClick='checkOptionTransfer(this.form.elements["target2[]"],this.form.elements["source2[]"]);return false;'></p>
	</td>
	<td width="50%">
	<select class='inputfield' name="source2[]" size=7 multiple>
	<?php echo $lg->teacherAssistantListOut(2, "RIGHTS"); ?>
	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
	</select>
	</td>
</tr></table>
<?php
}
?>


<?php
if ($sys_custom['audit_student_needs'])
{
?>
<p><br></p>

<table width="560" border="0" cellpadding="2" cellspacing="0" align="center">
<tr>
	<td colspan="3" nowrap>
	<hr size=1 class="hr_sub_separator">
	<span class="extraInfo">[<span class="subTitle"><?=$i_eClass_teacher_rights_3?></span>]</span>
	</td>
</tr>
<tr>
	<td width="30%" align="right">
	<select name="target3[]" class='inputfield' size="7" multiple >
	<?php echo $lg->teacherAssistantListIn(3, "RIGHTS"); ?>
	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
	</select>
	</td>
	<td align=center nowrap width="10%"><br>
	<p><input type='image' src='/images/admin/button/s_btn_addto_<?=$intranet_session_language?>.gif' border='0' onClick='checkOptionTransfer(this.form.elements["source3[]"],this.form.elements["target3[]"]);return false;'></p>
	<p><input type='image' src='/images/admin/button/s_btn_deleteto_<?=$intranet_session_language?>.gif' border='0' onClick='checkOptionTransfer(this.form.elements["target3[]"],this.form.elements["source3[]"]);return false;'></p>
	</td>
	<td width="50%">
	<select class='inputfield' name="source3[]" size=7 multiple>
	<?php echo $lg->teacherAssistantListOut(3, "RIGHTS"); ?>
	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
	</select>
	</td>
</tr>
</table>
<?php
}
$right_ids = explode(',',$right_ids);

?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?php
if ($sys_custom['audit_student_needs'] || $RoomType!=4){
?>
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
<!--<a href="javascript:window.location.reload();"><img src="/images/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif" border="0"></a>-->
<?php
}
?>
<a href="./?course_id=<?=$course_id?>"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<p><br></p>
<input type=hidden name="course_id" value="<?php echo $lo->course_id; ?>">
<input type=hidden name="right_ids" value="<?=implode(',',$right_ids)?>">
</form>

<?php
eclass_closedb();
include("../../../templates/adminfooter.php");
?>