<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libeclass.php");
include_once("$eclass_filepath/src/includes/php/lib-groups.php");
include_once("$eclass_filepath/system/settings/lang/$lang");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader_admin.php");
intranet_opendb();


$lo = new libeclass($course_id);
$lg = new libgroups(classNamingDB($course_id));

$sql="SELECT group_name, group_desc FROM grouping WHERE group_id='$group_id' ";
$row = $lg->returnArray($sql, 4);
$group_name= $row[0][0];
$group_desc = $row[0][1];

?>


<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, '', $lo->course_code ." ". $lo->course_name, "", $i_eClass_eclass_group, '', $namelist_member, '') ?>

<table width="80%" border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr><td class=tableContent2 nowrap valign="top" align="right"><span class=title><?= $namelist_groups_name ?>:&nbsp;</span></td><td width='80%'><?= $group_name ?></td></tr>
<tr><td class=tableContent2 nowrap valign="top" align="right"><span class=title><?= $namelist_groups_desc ?>:&nbsp;</span></td><td><?= $group_desc ?>&nbsp;</td></tr>
<tr><td class=tableContent2 nowrap valign="top" align="right"><span class=title><?= $namelist_member ?>:&nbsp;</span></td><td><?= $lg->memberList($group_id) ?></td></tr>
</table>

<br>
<center>
<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a></center>
<p></p>
</form>


<?php
intranet_closedb();
include("../../../../templates/adminfooter_popup.php");
?>
