<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libeclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("$eclass_filepath/system/settings/lang/$lang");
eclass_opendb();


$li = new libeclass($course_id);
$li->db = classNamingDB($course_id);
/*
$fm = new fileManager($ck_course_id, 7, "");
$fm->isSystem = true;
$fm->permission = "GW";
$fm->maxSize
 = $maxSize
;
*/
$group_name = htmlspecialchars($group_name);
$group_desc = htmlspecialchars($group_desc);

$has_right = ($has_right=="-1") ? 0 : 1;
if($group_id==""){
	$msg=1;
	$sql = "INSERT INTO grouping (group_name, group_desc, has_right, inputdate, modified) values ('$group_name', '$group_desc', '$has_right', now(), now())"; 
	// Insert
	$li->db_db_query($sql);
	$group_id = $li->db_insert_id();

/*
	$group_name = parseFileName($group_name);
	$folderName = "G$group_id"."_".$group_name;
	$folderID = $fm->createFolderDB($folderName, $file_group_folder, "", 7);

	$sql = "INSERT INTO grouping_function (group_id, function_id, function_type) VALUES ('$group_id', '$folderID', 'FM')";
	$li->db_db_query($sql);

	//save folderid in group info
	$sql = "UPDATE grouping SET FolderID='$folderID' WHERE group_id='$group_id'";
	$fm->db_db_query($sql);
*/
}else{
	$msg=2;
	$sql = "UPDATE grouping SET group_name = '$group_name', group_desc = '$group_desc', has_right = '$has_right', modified=now() WHERE group_id=$group_id"; 
	// Update
	$li->db_db_query($sql);

	# Eric Yip (20090514): remove grouping function for iPortfolio
	if($li->getEClassRoomType($course_id) == 4 && !$has_right)
	{
		$sql = "DELETE FROM mgt_grouping_function WHERE group_id=$group_id";
		$li->db_db_query($sql);
	} 

	// update user_group - remove
	$sql = "DELETE FROM user_group WHERE group_id = $group_id";
	$li->db_db_query($sql);

/*
	$sql = "SELECT FolderID FROM grouping WHERE group_id=$group_id";
	$row = $fm->returnArray($sql, 1);
	$FileID = $row[0][0];

	$folderName = "G$group_id"."_".$group_name;
	if ($maxSize=="") $maxSize="NULL";

	$update = " Title='$folderName', SizeMax=$maxSize, DateModified=now() ";
	$sql = "UPDATE eclass_file SET $update WHERE FileID='$FileID' ";
	$li->db_db_query($sql);
*/
}

// update user_group - add
for($i = 0; $i < sizeof($target); $i++){
	$user_id = $target[$i];
	$sql = "INSERT INTO user_group (user_id, group_id) VALUES ($user_id, $group_id)";
	$li->db_db_query($sql);
}

// update group leader(s)
/*
if (Trim($leader)!="") {
	$leader_id = split("\|", $leader);
	for ($i=0; $i<sizeof($leader_id); $i++) {
		$sql = "UPDATE user_group SET leader=1 WHERE user_id='$leader_id[$i]' AND group_id=$group_id";
		$li->db_db_query($sql);
	}
}
*/

eclass_closedb();
header("Location: index.php?msg=$msg&course_id=$course_id");
?>
