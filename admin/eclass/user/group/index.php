<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libeclass.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("$eclass_filepath/system/settings/lang/$lang");
eclass_opendb();

# eClass
$lo = new libeclass($course_id);
$RoomType = $lo->getEClassRoomType($course_id);
$lo->order = $order;
$lo->field = $field;

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if($order=="") $order=1;
if($field=="") $field=0;
/*
$sql = "SELECT CONCAT('<a class=tableContentLink href=\"edit.php?course_id=$course_id&group_id=', g.group_id, '\">', g.group_name, '</a>'), g.group_desc, CONCAT('<a href=\"javascript:newWindow(\'view_member.php?course_id=$course_id&group_id=', g.group_id, '\', 2)\" class=tableContentLink>', count(ug.user_group_id), '</a>'), g.modified, CONCAT('<input type=checkbox name=\"group_id[]\" value=', g.group_id ,'>'), count(ug.user_group_id) AS TotalMember FROM grouping AS g LEFT JOIN user_group AS ug ON ug.group_id=g.group_id ";
$sql .= "WHERE (g.group_name like '%$keyword%' or g.group_desc like '%$keyword%') GROUP BY g.group_id ";

*/
$sql = "SELECT 
			CONCAT('<a class=tableContentLink href=\"edit.php?course_id=$course_id&group_id=', g.group_id, '\">', g.group_name, '</a>'), g.group_desc, 
			CONCAT('<a href=\"javascript:newWindow(\'view_member.php?course_id=$course_id&group_id=', g.group_id, '\', 2)\" class=tableContentLink>', count(um.user_id), '</a>'), ";
$sql .= ($RoomType==4) ? "if(g.has_right=0,'--', CONCAT('<a href=\"../../portfolio/grouping_function_settings.php?course_id=$course_id&group_id=', g.group_id,'\">$namelist_groups_set_rights</a>')), " : "";
$sql .= " g.modified, CONCAT('<input type=checkbox name=\"group_id[]\" value=', g.group_id ,'>'), count(ug.user_group_id) AS TotalMember ";
$sql .= " FROM grouping AS g LEFT JOIN user_group AS ug ON ug.group_id=g.group_id LEFT JOIN usermaster AS um ON (ug.user_id = um.user_id AND um.status IS NULL)";
$sql .= " WHERE (g.group_name like '%$keyword%' or g.group_desc like '%$keyword%') GROUP BY g.group_id ";


# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = ($RoomType==4) ? array("group_name", "group_desc", "TotalMember", "has_right", "modified") : array("group_name", "group_desc", "TotalMember", "modified");
$li->db = classNamingDB($course_id);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $namelist_groups;
$li->column_array = ($RoomType==4) ? array(0,0,0,0,0,0,0,0) : array(0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width='5%' class=tableTitle>#</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(0, $namelist_groups_name)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(1, $namelist_groups_desc)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(2, $namelist_member)."</td>\n";
if($RoomType==4)
{
	$li->column_list .= "<td width=10% class=tableTitle>".$li->column(3, $namelist_groups_admin_right)."</td>\n";
	$li->column_list .= "<td width=15% class=tableTitle>".$li->column(4, $profile_modified)."</td>\n";
}
else
{
	$li->column_list .= "<td width=25% class=tableTitle>".$li->column(3, $profile_modified)."</td>\n";
}
$li->column_list .= "<td width='5%' class=tableTitle>".$li->check("group_id[]")."</td>\n";


$toolbar = "<a class=iconLink href=\"javascript:checkGet(document.form1,'new.php?course_id=$course_id')\">".newIcon()."$button_new</a>\n";

$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'group_id[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'group_id[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";

include("../../../../templates/adminheader_eclass.php");
?>


<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, "../../index.php?RoomType=$RoomType", $lo->course_code ." ". $lo->course_name, "../index.php?course_id=$course_id", $i_eClass_eclass_group, '') ?>
<?= displayTag("head_eclass_namelist_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $selection ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
<?php if ($license_html!="") { ?>
<tr><td><?= $license_html ?></td></tr>
<?php } ?>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=../../../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<p><br></p>
<input type=hidden name=course_id value="<?= $course_id ?>">
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

</form>

<?php
eclass_closedb();
include("../../../../templates/adminfooter.php");
?>
