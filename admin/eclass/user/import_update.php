<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclass40.php");
include_once("../../../lang/lang.$intranet_session_language.php");
eclass_opendb();

$li = new libdb();
$lo = new libeclass($course_id);

$lo->setRoomType($lo->getEClassRoomType($course_id));

if(sizeof($ChooseUserID) <> 0){
     //$sql = "SELECT UserID, UserEmail, UserPassword, ClassNumber, FirstName, LastName, ClassName, CASE Title WHEN 0 THEN 'MR' WHEN 0 THEN 'MR.' WHEN 1 THEN 'MISS.' WHEN 2 THEN 'MRS.' WHEN 3 THEN 'MS.' WHEN 4 THEN 'DR.' WHEN 5 THEN 'PROF.' END , EnglishName, ChineseName, NickName, Gender, Info FROM INTRANET_USER WHERE UserID IN (".implode(",", $ChooseUserID).")";
     $sql = "SELECT UserID, UserEmail, UserPassword, ClassNumber, FirstName, LastName, ClassName,  Title, EnglishName, ChineseName, NickName, Gender, Info FROM INTRANET_USER WHERE UserID IN (".implode(",", $ChooseUserID).")";
     $row = $li->returnArray($sql,13);
     for($i=0; $i<sizeof($row); $i++){
          if($lo->max_user <> "" && $i == $lo->ticketUser()) break;
          $UserID = $row[$i][0];
          $UserEmail = $row[$i][1];
          $UserPassword = $row[$i][2];
          
          // Eric Yip (20100709): (Intranet) class number 0 -> (EClass) ''
          if($MemberType=="S" && $zero_padding['ClassNum'])
          {
            $ClassNumber = str_pad($row[$i][3], 2, "0", STR_PAD_LEFT);
          }
          else
          {
            $ClassNumber = ($row[$i][3]==0) ? "" : $row[$i][3];
          }
          
          $FirstName = $row[$i][4];
          $LastName = $row[$i][5];
          $ClassName = $row[$i][6];
          $Title = $row[$i][7];
          $EngName = $row[$i][8];
          $ChiName = $row[$i][9];
          $NickName = $row[$i][10];
          $Gender = $row[$i][11];
		  $Info = $row[$i][12];
		  $eclassClassNumber = (trim($ClassName) == "") ? $ClassNumber : trim($ClassName)." - ".trim($ClassNumber);
		  /*
          if ($ClassNumber=="" || $ClassName=="" || stristr($ClassNumber,$ClassName))
              $eclassClassNumber = $ClassNumber;
          else
              $eclassClassNumber = $ClassName ." - ".$ClassNumber;
          */    
          #$lo->eClassUserAdd($UserEmail, $Title, $EngName, "", $NickName, $MemberType, $UserPassword, $eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info);
          $lo->eClassUserAddFullInfo($UserEmail, $Title,$FirstName,$LastName, $EngName, $ChiName, $NickName, $MemberType, $UserPassword, $eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info,$ClassName);

//          $lo->eClassUserAdd($UserEmail, $Title, $FirstName, $LastName, $DisplayName, $MemberType, $UserPassword, $eclassClassNumber);
     }
     $lo->eClassUserNumber($lo->course_id);
}
eclass_closedb();
?>
<body onLoad="document.form1.submit();opener.window.location=opener.window.location;">
<form name=form1 action="import.php" method="get">
<input type=hidden name=course_id value="<?php echo $course_id; ?>">
<input type=hidden name=RoomType value=<?=$RoomType?>>
</form>
</body>