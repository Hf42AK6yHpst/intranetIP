<?php
// Modified by: fai
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclass40.php");
eclass_opendb();

$lo = new libeclass($course_id);
$roomType = $lo->getEClassRoomType($course_id);
$tmp_arr = split(",", $right_ids);
for ($i=0; $i<sizeof($tmp_arr); $i++)
{
	$right_id = $tmp_arr[$i];
	$lo->updateEClassUserRights($right_id, ${"target".$right_id},$roomType);
}

eclass_closedb();

header("Location: teacher_rights.php?course_id=$course_id&msg=2");
?>