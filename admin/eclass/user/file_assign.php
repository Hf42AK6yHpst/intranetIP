<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclass.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
eclass_opendb();

# eClass
$lo = new libeclass($course_id);
$lo->order = $order;
$lo->field = $field;
$eclass_quota  = $lo->status();
$eclass_quota .= "- ".$i_eClassNumUsers.": ".$lo->max_user."<br>\n";
$eclass_quota .= "- ".$i_eClassMaxStorage.": ".$lo->max_storage."<br>\n";



$ec_db_name = $lo->db_prefix."c$course_id";
//$name_separator = ($intranet_session_language=="en") ? " " : "";


############## GET TEACHER LIST WHO HAVE FILES ###############
$sql = "SELECT u.user_id, CONCAT(LTRIM(CONCAT(u.lastname, ' ', u.firstname)), if(u.status='deleted', ' ($i_SMS_Deleted)', '')) as t_name ";
$sql .= "FROM ".$ec_db_name.".usermaster as u ";
$sql .= ", ".$ec_db_name.".eclass_file as f ";
$sql .= "WHERE u.memberType='T' ";
$sql .= "AND (u.user_id=f.User_id OR u.user_email=f.UserEmail) ";
$sql .= "GROUP BY u.user_id ORDER BY t_name ";
$row_r = $lo->returnArray($sql, 2);
$teacher_removed_list = "<select name='teacher_removed[]' size=5 multiple>\n";
for ($i=0; $i<sizeof($row_r); $i++)
{
	$teacher_removed_list .= "<option value='".$row_r[$i][0]."'>".$row_r[$i][1]."</option>\n";
}
$teacher_removed_list .= "</select>\n";
if (sizeof($row_r)>0)
{
	// show submit button if deleted teacher existing with files
	$submit_button = "<input type=submit class=submit value='$button_submit'>";
}


######################### GET EXISTING TEACHER LIST #########################
$sql = "SELECT user_id, LTRIM(CONCAT(lastname, ' ', firstname)) as t_name, user_email ";
$sql .= "FROM ".$ec_db_name.".usermaster ";
$sql .= "WHERE memberType='T' AND (status IS NULL OR status='') ";
$sql .= "ORDER BY t_name ";
$row_e = $lo->returnArray($sql, 3);

$sql = "SELECT UserID, EnglishName, ChineseName, UserEmail ";
$sql .= "FROM INTRANET_USER ";
$sql .= "WHERE RecordType = 1 ";
$sql .= "ORDER BY EnglishName ";
$li = new libdb();
$row_i = $li->returnArray($sql, 4);

$teacher_exist_list = "<select name='teacher_benefit'>\n";
$teacher_exist_list .= "<option value=''>&nbsp;-- ".$i_ec_file_exist_teacher." --&nbsp;</option>\n";
$cours_teacher_size = sizeof($row_e);
$exclus_email = array();
for ($i=0; $i<$cours_teacher_size; $i++)
{
	$teacher_exist_list .= "<option value='".$row_e[$i][0]."'>".$row_e[$i][1]."</option>\n";
	$exclus_email[] = trim($row_e[$i][2]);
}
$teacher_exist_list .= "<option value=''>____________________</option>";
$teacher_exist_list .= "<option value=''>&nbsp;-- ".$i_ec_file_not_exist_teacher." --&nbsp;</option>\n";
for ($i=0; $i<sizeof($row_i); $i++)
{
	if (!in_array($row_i[$i][3], $exclus_email))
	{
		$chiname = ($row_i[$i][2]==""? "": "(".$row_i[$i][2].")" );
		$teacher_exist_list .= "<option value='".$row_i[$i][0]."'>".$row_i[$i][1]." $chiname</option>\n";
	}
}

$teacher_exist_list .= "</select>\n";


include("../../../templates/adminheader_eclass.php");
?>

<script language="javascript">
function checkform(obj){
	if (countSelected(obj, "teacher_removed[]")<1)
	{
		alert("<?=$i_ec_file_warning ?>");
		return false;
	}
	if (obj.teacher_benefit.options[obj.teacher_benefit.selectedIndex].value=="")
	{
		alert("<?=$i_ec_file_warning2 ?>");
		return false;
	}
	if (confirm("<?=$i_ec_file_confirm?>"))
	{
		if (obj.teacher_benefit.selectedIndex><?=$cours_teacher_size?>+2)
		{
			obj.is_user_import.value = 1;
			if (confirm("<?=$i_ec_file_confirm2?>"))
			{
				return true;
			} else
			{
				return false;
			}
		} else
		{
			return true;
		}
	} else
	{
		return false;
	}
}

function countSelected(obj, e_name){
	var count = 0;
	for(i=0; i<obj.elements[e_name].length; i++)
	{
		if (obj.elements[e_name].options[i].selected)
		{
			count ++;
		}
	}

	return count;
}
</script>


<form name=form1 method="post" action="file_assign_update.php" onSubmit="return checkform(this)">

<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, "../index.php", $lo->course_code ." ". $lo->course_name, "index.php?course_id=$course_id", $i_ec_file_assign, "") ?>
<?= displayTag("head_eclass_namelist_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>

<br>
<table width=480 border=0 align=center cellpadding=5>
<tr><td width=200><u><?=$i_ec_file_remove_teacher?></u></td><td width=150>&nbsp;</td><td width=150><u><?=$i_ec_file_target_teacher?></u></td></tr>
<tr><td><?=$teacher_removed_list?></td>
<td style="vertical-align:middle"><?=$i_ec_file_assign_to?> &gt;&gt;</td>
<td style="vertical-align:middle"><?=$teacher_exist_list?></td></tr>
<tr><td colspan=3><br>
</td></tr>
</table>

</blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border="0">
<a href="index.php?course_id=<?=$course_id?>"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>

<input type=hidden name=course_id value=<?php echo $lo->course_id; ?>>
<input type=hidden name=order>
<input type=hidden name=field>
<input type='hidden' name='is_user_import' value='0'>
</form>

<?php
eclass_closedb();
include("../../../templates/adminfooter.php");
?>
