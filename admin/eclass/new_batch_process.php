<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libeclass.php");
include_once("../../includes/libfilesystem.php");
include_once("../../lang/lang.$intranet_session_language.php");
eclass_opendb();


function import2eClass($course_id, $user_filter_sql, $MemberType){
	global $intranet_db;
	$sql = "SELECT UserID, UserEmail, UserPassword, ClassNumber, FirstName, LastName, ClassName, CASE Title WHEN 0 THEN 'MR' WHEN 0 THEN 'MR.' WHEN 1 THEN 'MISS.' WHEN 2 THEN 'MRS.' WHEN 3 THEN 'MS.' WHEN 4 THEN 'DR.' WHEN 5 THEN 'PROF.' END , EnglishName, ChineseName, NickName FROM {$intranet_db}.INTRANET_USER WHERE $user_filter_sql ";
	$lo = new libeclass($course_id);
	$row = $lo->returnArray($sql,11);
	for($i=0; $i<sizeof($row); $i++)
	{
		if($lo->max_user <> "" && $i == $lo->ticketUser()) break;
		$UserID = $row[$i][0];
		$UserEmail = $row[$i][1];
		$UserPassword = $row[$i][2];
		$ClassNumber = $row[$i][3];
		$FirstName = $row[$i][4];
		$LastName = $row[$i][5];
		$ClassName = $row[$i][6];
		$Title = $row[$i][7];
		$EngName = $row[$i][8];
		$ChiName = $row[$i][9];
		$NickName = $row[$i][10];
		$eclassClassNumber = (trim($ClassName) == "") ? $ClassNumber : trim($ClassName)." - ".trim($ClassNumber);
		/*
		if ($ClassNumber=="" || $ClassName=="" || stristr($ClassNumber,$ClassName))
		  $eclassClassNumber = $ClassNumber;
		else
		  $eclassClassNumber = $ClassName ." - ".$ClassNumber;
		*/
		$lo->eClassUserAddFullInfo($UserEmail,$Title,$FirstName,$LastName,$EngName,$ChiName, $NickName,$MemberType,$UserPassword,$eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info,$ClassName);
	}
	$lo->eClassUserNumber($lo->course_id);

	return;
}

$arr_tmp = split("_", $course_add[$course_current]);
$ClassID = $arr_tmp[0];
$SubjectID = $arr_tmp[1];
$IsStdImport = $student_import[$course_current];

$li = new libdb();
$sql = "SELECT DISTINCT CONCAT(c.ClassName, ' ', s.SubjectName), c.GroupID, c.ClassName FROM INTRANET_CLASS AS c, INTRANET_SUBJECT AS s where c.ClassID=$ClassID AND s.SubjectID=$SubjectID";
$row_all = $li->returnArray($sql, 3);

if ($course_code_prefix)
{
	$sql = "SELECT count(*) FROM {$eclass_db}.course ";
	$row_ec = $li->returnVector($sql);
	$course_tt = (int) $row_ec[0];
	$course_code_0 = 5 - strlen($course_tt);
	for ($i=0; $i<$course_code_0; $i++)
	{
		$course_code_str .= "0";
	}
}

$ClassName = addslashes($row_all[0][2]);
$GroupID = $row_all[0][1];
$course_code = htmlspecialchars($course_code_prefix).$course_code_str.$course_tt;
$course_name = addslashes($row_all[0][0]);
//$course_desc = htmlspecialchars($course_desc);
$max_user = (int) $max_user;
$max_storage = (int) $max_storage;
if ($max_user==0) $max_user = "NULL";
if ($max_storage==0) $max_storage = "NULL";

$lo = new libeclass();
$lo->setRoomType($RoomType);

$valid = false;
if(($lo->license!="" &&  $lo->ticket()==0) || $course_name=="")
{
   $valid = false;
} else
{
	$valid = true;
}

if ($valid){
    $course_id = $lo->eClassAdd($course_code, $course_name, $course_desc, $max_user, $max_storage);
    $lo->eClassSubjectUpdate($subj_id, $course_id);


	# Update INTRANET_COURSE
	if ($GroupID!="" && $SubjectID!="")
	{
		$sql = "INSERT INTO INTRANET_COURSE (CourseID,ClassGroupID,SubjectID) VALUES ('$course_id','$GroupID','$SubjectID')";
		$li->db_db_query($sql);
	}


	# import teachers
	$sql = "SELECT UserID FROM INTRANET_SUBJECT_TEACHER WHERE ClassID=$ClassID AND SubjectID=$SubjectID ";
	$row_teacher = $li->returnVector($sql);
	if (is_array($row_teacher) && sizeof($row_teacher)>0)
	{
		$user_filter_sql = " UserID IN (".implode(",",$row_teacher).") ";
		import2eClass($course_id, $user_filter_sql, "T");
	}


	# import students
	if($IsStdImport)
	{
		$user_filter_sql = " RecordType=2 AND ClassName='$ClassName' ";
		import2eClass($course_id, $user_filter_sql, "S");
	}
}

# Set Equation Editor Rights
/*
if ($hasEquation == 1)
{
    $content = trim(get_file_content($lo->filepath."/files/equation.txt"));
    $equation_class = ($content=="")? array(): explode(",",$content);
    $equation_class[] = $course_id;
    write_file_content(implode(",",$equation_class),$lo->filepath."/files/equation.txt");
}
*/

eclass_closedb();
?>



<script language="javascript">
parent.fmBatchMain.submitProcess();
</script>