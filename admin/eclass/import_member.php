<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libeclass.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("eclass_common.php");

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

$linterface = new interface_html();
$limport = new libimporttext();

eclass_opendb();

include_once("../../templates/adminheader_eclass.php");
?>

<script language="javascript">
</script>

<?php
if ($MemberType!="")
{
?>

<form name="form1" action="import_member_update.php" method="post" enctype="multipart/form-data">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, 'javascript:history.back()', $i_eClass_batch_import, '') ?>
<?= displayTag("head_eclass_$intranet_session_language.gif") ?>
<blockquote>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr><td align=right nowrap><?php echo $i_select_file; ?>:</td><td><input type=file size=50 name=userfile><br />
<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?></td></tr>
<tr><td align=right nowrap></td><td>
<br><?=$i_eClass_ImportInstruction_batch_member?>
<br><br><a class=functionlink_new href="<?= GET_CSV("batch_member_sample.csv")?>" target=_blank><?=$i_general_clickheredownloadsample?></a>
<br>
</td></tr>
</table>
</blockquote>
<p>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>
</p>
<input type="hidden" name="MemberType" value="<?=$MemberType?>" >
</form>

<?php
} else
{
?>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, 'javascript:history.back()', $i_eClass_batch_import, '') ?>
<?= displayTag("head_eclass_$intranet_session_language.gif") ?>
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo "$button_select $i_identity"; ?>:</td><td>
<SELECT name="MemberType" onChange=this.form.submit()>
<OPTION><?=" -- $button_select -- "?></OPTION>
<OPTION value="S"><?=$i_eClass_group_student?></OPTION>
<OPTION value="T"><?=$i_eClass_group_teacher?></OPTION>
<OPTION value="A"><?=$i_eClass_group_assistant?></OPTION>
</td></tr>
</table>

<?php
}
eclass_closedb();
include_once("../../templates/adminfooter.php");
?>