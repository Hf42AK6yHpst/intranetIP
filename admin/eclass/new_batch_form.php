<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libeclass.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
eclass_opendb();

# eClass
$lo = new libeclass();
$ticket = $lo->ticket();
if($lo->license == "" || $ticket>0)
{
	$valid = true;
} else
{
	$valid = false;
}


$li = new libdb();
$sql = "SELECT c.ClassID, s.SubjectID, CONCAT(c.ClassName, ' ', s.SubjectName) FROM INTRANET_CLASS AS c, INTRANET_SUBJECT AS s where c.RecordStatus=1 AND s.RecordStatus=1 ORDER BY c.ClassName, s.SubjectName";
$row_all = $li->returnArray($sql, 3);
$amount = sizeof($row_all);
for ($i=0; $i<$amount; $i++)
{
	list($ClassID, $SubjectID, $CourseName) = $row_all[$i];
	$td_css = ($i%2) ? "2" : "";
	$course_add = $ClassID."_".$SubjectID;
	$std_import = "std_".$course_add;
	$checked = ($ticket>$i || $lo->license=="") ? "checked" : "";
	$disabled = ($checked) ? "" : "disabled";
	$rows .= "<tr><td class=tableContent$td_css>".($i+1)."&nbsp;</td><td class=tableContent$td_css>".$CourseName."</td><td class=tableContent$td_css><input type='checkbox' name='course_add[]' onClick='triggerCourse(this, this.form)' $checked value='".$course_add."'></td><td class=tableContent$td_css><input type='checkbox' name='std_import[]' $checked $disabled value='".$std_import."'></td></tr>\n";
}
if ($amount>0)
{
	$proposed_html = "<table width=480 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
	$proposed_html .= "<tr><td class=tableTitle width='1'>#</td><td class=tableTitle width='60%'>$i_frontpage_eclass_courses</td><td class=tableTitle width='20%' nowrap><input type='checkbox' onClick=\"switchAllCourse(this, this.form)\">$button_new</td><td class=tableTitle width='20%' nowrap><input type='checkbox' onClick=\"(this.checked)?setChecked2(1,this.form,'std_import[]'):setChecked2(0,this.form,'std_import[]')\">$i_eClassCourseImportStudent</td></tr>\n";
	$proposed_html .= $rows;
	$td_css = ($i%2) ? "2" : "";
	$course_num = ($amount>$ticket && $lo->license!="") ? $ticket : $amount;
	$tick_total = ($lo->license=="") ? "" : "/ $ticket";
	$proposed_html .= "<tr><td class=tableContent$td_css colspan='2' align='right'>$i_eClassLicenseMsg1:</td><td class=tableContent$td_css colspan='2'><input class='buttonText' type='button' name='course_num' value='$course_num'>{$ticket_total}</td></tr>\n";
	$proposed_html .= "</table>\n";
} else
{
	$proposed_html = $i_eClassCourseProposalNone;
}


$str_RoomType = ($RoomType==1?$i_eClass_reading_room:$i_eClass_normal_course);

include_once("../../templates/adminheader_eclass.php");
?>

<style type="text/css">
.buttonText {
	COLOR: black; BACKGROUND-COLOR: white;
	FONT-SIZE: 12px;
	BORDER-RIGHT: white 1px solid; BORDER-TOP: white 1px solid;
	BORDER-LEFT: white 1px solid; BORDER-BOTTOM: white 1px solid;
}
.buttonTextWarn {
	COLOR: black; BACKGROUND-COLOR: white;
	FONT-SIZE: 12px;
	BORDER-RIGHT: red 1px solid; BORDER-TOP: red 1px solid;
	BORDER-LEFT: red 1px solid; BORDER-BOTTOM: red 1px solid;
}
</style>

<script language="javascript">
function checkform(obj){
	var course_total = countChecked(obj, "course_add[]");
	if(course_total<1)
	{
		alert("<?= $i_eClassCourseNoneWarn ?>");
		return false;
	}
<?php
if ($eclass_mck!="")
{
?>
	if(course_total><?= (int) $ticket?>)
	{
		alert("<?= $i_eClassLicenseExceed ?>");
		return false;
	}
<?php
}
?>
	if(confirm("<?= $i_eClassCourseConfirm ?>"))
	{
		return true;
	} else
	{
		return false;
	}
}

function triggerCourse(chkObj, frmObj){
	var cid = chkObj.value;
	if (!chkObj.checked)
	{
		setCheckedDisabled(0, frmObj, "std_import[]", ("std_"+cid), 1);
	} else
	{
		setCheckedDisabled(1, frmObj, "std_import[]", ("std_"+cid), 0);
	}
	updateCourseNumber(frmObj, "");
	return;
}

function setCheckedDisabled(val, obj, element_name, value, isCheckRelated){
	var len=obj.elements.length;
	var i=0;

	for (i=0 ; i<len ; i++) {
		if (obj.elements[i].name==element_name)
		{
			if ((value!="" && obj.elements[i].value==value) || value=="")
			{
				if (isCheckRelated)
				{
					obj.elements[i].checked = val;
				}
				obj.elements[i].disabled = !val;
			}
		}
	}

	return;
}

function updateCourseNumber(frmObj, myValue){
	frmObj.course_num.value = (myValue!="") ? myValue : countChecked(frmObj, "course_add[]");
<?php
if ($eclass_mck!="")
{
?>
	if (frmObj.course_num.value><?= (int) $ticket?>)
	{
		frmObj.course_num.className = "buttonTextWarn";
	} else
	{
		frmObj.course_num.className = "buttonText";
	}
<?php
}
?>
	return;
}

function setChecked2(val, obj, element_name){
	len=obj.elements.length;
	var i=0;
	for(i=0; i<len; i++) {
		if (obj.elements[i].name==element_name && !obj.elements[i].disabled)
		obj.elements[i].checked=val;
	}

	return;
}

function switchAllCourse(chkObj, frmObj){
	if (chkObj.checked)
	{
		setChecked(1,frmObj,'course_add[]');
		setCheckedDisabled(1, frmObj, "std_import[]", "", 0);
	} else
	{
		setChecked(0,frmObj,'course_add[]');
		setCheckedDisabled(0, frmObj, "std_import[]", "", 0);
	}
	updateCourseNumber(frmObj, "");

	return;
}
</script>


<form name="form1" action="new_batch_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, 'javascript:history.back()', $button_quickadd." (".$str_RoomType.")", '') ?>
<?= displayTag("head_eclass_$intranet_session_language.gif") ?>

<?php
if ($valid)
{
?>
<blockquote>
<p><u><?=$i_eClassCourseSettings?>:</u>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr><td align=right width="30%" nowrap><?php echo $i_eClassNumUsers; ?>:</td><td width="70%"><input class=text type=text name=max_user size=5 maxlength=5 value=30></td></tr>
<tr><td align=right width="30%" nowrap><?php echo $i_eClassMaxStorage; ?>:</td><td><input class=text type=text name=max_storage size=5 maxlength=5 value=30> MB</td></tr>
<tr><td align=right width="30%" nowrap><?php echo $i_eClassCourseCodePrefix; ?>:</td><td><input class=text type=text name=course_code_prefix value="C" size=5 maxlength=5> <span class="extraInfo">(<?=$i_eClassCourseCodePrefixGuide?>)</span></td></tr>
</table>
</p>
<br>
<p><u><?=$i_eClassCourseProposal?>:</u>
<?= $proposed_html ?>
<table width=480 border=0 cellpadding=2 cellspacing=1 align=center>
<tr><td><?=$i_eClassCourseProposalNotes?></td></tr>
</table>
</p>
</blockquote>
<p>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>
</p>
<?php }
else
{
    echo "<blockquote>$i_eClassLicenseFull</blockquote>";
}
?>
<input type="hidden" name="RoomType" value="<?=$RoomType?>">
<p></p>
</form>

<?php
eclass_closedb();
include_once("../../templates/adminfooter.php");
?>