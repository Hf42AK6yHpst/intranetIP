<?php
include_once("../../../includes/global.php");
include_once("../../../lang/email.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libaccess.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_eclass.php");

# block illegal access
$la = new libaccess();
$la->retrieveAccessEClass();
$la->retrieveEClassSettings();
$ec_mgt_checked = ($la->isAccessEClassMgt()) ? "checked" : "";
$ec_mgt_course_checked = ($la->isAccessEClassMgtCourse()) ? "checked" : "";
$ec_email_teacher_0_checked = ($la->isAccessEClassEmailTeacher()) ? "checked" : "";
$ec_email_helper_0_checked = ($la->isAccessEClassEmailHelper()) ? "checked" : "";
$ec_email_student_0_checked = ($la->isAccessEClassEmailStudent()) ? "checked" : "";


# get planner color setting
$filecontent = trim(get_file_content("$eclass_filepath/files/planner.txt"));
if ($filecontent!="")
{
	$planner_content = explode("\n",$filecontent);
	for ($i=0; $i<sizeof($planner_content); $i++)
	{
		list($event_color, $event_title) = explode("||", $planner_content[$i]);
		$ec_event_color[] = array(str_replace("\"", "&quot;", trim($event_title)), trim($event_color));
	}
} else
{
	$ec_event_color = array(
					array("", "#FF0000"),
					array("", "#FF6400"),
					array("", "#967D00"),
					array("", "#009B00"),
					array("", "#009BD7"),
					array("", "#0000C8"),
					array("", "#9600D2"),
					array("", "#808080"),
					array("", "#FF00FF")
				);
}
for ($i=0; $i<sizeof($ec_event_color); $i++)
{
	$planner_color_html .= "<tr><td align='center'><font color='".$ec_event_color[$i][1]."'>text ABC</a></td><td align='center'><input type='text' name='planner_type[]' value=\"".$ec_event_color[$i][0]."\"><input type='hidden' name='planner_color[]' value=\"".$ec_event_color[$i][1]."\"></td></tr>\n";
}


$base_dir = "$intranet_root/file/templates/";
if (!is_dir($base_dir))
{
     $lf->folder_new($base_dir);
}
$target_file = "$base_dir"."bulletin_badwords.txt";
$data = get_file_content($target_file);


// For powervoice control
if ($plugin['power_voice'])
{
	$pvoice_target_file = "$intranet_root/file/powervoice.txt";
	$pvoice_data = get_file_content($pvoice_target_file);
	$pvoice_array = unserialize($pvoice_data);
	
	$powervoice_content  = "";
	$powervoice_content .= "<hr size=\"1\" class=\"hr_sub_separator\" >";	

	$powervoice_content .= "<p>";
	$powervoice_content .= "<span class=\"extraInfo\">[<span class=subTitle>".$iPowerVoice['power_voice_setting']."</span>]</span>";

	$powervoice_content .= "<table width='250' border='1' bordercolor='#F7F7F9' cellspacing='1' cellpadding='1'>";
	$bitrate_select = "";
	$bitrate_select .= "<select name='bitrate' >";
	if ($pvoice_array['bitrate'] == "128")
	{
		$bitrate_select .= "<option value=\"128\" selected=\"selected\" >128</option>";
	} else {
		$bitrate_select .= "<option value=\"128\" >128</option>";
	}
	if (($pvoice_array['bitrate'] == "96") || ($pvoice_array['bitrate'] == ""))
	{	
		$bitrate_select .= "<option value=\"96\" selected=\"selected\" >96</option>";
	} else {
		$bitrate_select .= "<option value=\"96\" >96</option>";
	}
	if ($pvoice_array['bitrate'] == "64")
	{		
		$bitrate_select .= "<option value=\"64\" selected=\"selected\" >64</option>";
	} else {
		$bitrate_select .= "<option value=\"64\" >64</option>";
	}
	if ($pvoice_array['bitrate'] == "32")
	{			
		$bitrate_select .= "<option value=\"32\" selected=\"selected\" >32</option>";
	} else {
		$bitrate_select .= "<option value=\"32\" >32</option>";
	}
	$bitrate_select .= "</select>";
	$powervoice_content .= "<tr><td style=\"vertical-align:bottom\" align='left'>".$iPowerVoice['bit_rate']."</td><td style=\"vertical-align:bottom\" align='left'>{$bitrate_select} {$iPowerVoice['bit_rate_unit']}</td></tr>";
	$sample_select = "";
	$sample_select .= "<select name='samplerate' >";
	if ($pvoice_array['sampling_frequency'] == "22050")
	{	
		$sample_select .= "<option value=\"44100\" >44100</option>";
		$sample_select .= "<option value=\"22050\" selected=\"selected\" >22050</option>";				
	} else {
		$sample_select .= "<option value=\"44100\" selected=\"selected\" >44100</option>";
		$sample_select .= "<option value=\"22050\" >22050</option>";		
	}
	$sample_select .= "</select>";		
	$powervoice_content .= "<tr><td style=\"vertical-align:bottom\" align='left'>".$iPowerVoice['sampling_rate']."</td><td style=\"vertical-align:bottom\" align='left'>{$sample_select} {$iPowerVoice['sampling_rate_unit']} </td></tr>";
	$length_select = "";
	$length_select .= "<select name='voice_length' >";	
	if (($pvoice_array['length'] == "60") || ($pvoice_array['length'] == ""))
	{				
		$length_select .= "<option value=\"60\" selected=\"selected\" >60 {$iPowerVoice['sound_length_unit']}</option>";
	} else {
		$length_select .= "<option value=\"60\" >60 {$iPowerVoice['sound_length_unit']}</option>";
	}	
	if ($pvoice_array['length'] == "30")
	{				
		$length_select .= "<option value=\"30\" selected=\"selected\" >30 {$iPowerVoice['sound_length_unit']}</option>";
	} else {
		$length_select .= "<option value=\"30\" >30 {$iPowerVoice['sound_length_unit']}</option>";
	}
	if ($pvoice_array['length'] == "25")	
	{
		$length_select .= "<option value=\"25\" selected=\"selected\" >25 {$iPowerVoice['sound_length_unit']}</option>";
	} else {
		$length_select .= "<option value=\"25\" >25 {$iPowerVoice['sound_length_unit']}</option>";
	}
	if ($pvoice_array['length'] == "20")	
	{	
		$length_select .= "<option value=\"20\" selected=\"selected\" >20 {$iPowerVoice['sound_length_unit']}</option>";
	} else {
		$length_select .= "<option value=\"20\" >20 {$iPowerVoice['sound_length_unit']}</option>";
	}
	if ($pvoice_array['length'] == "15")	
	{		
		$length_select .= "<option value=\"15\" selected=\"selected\" >15 {$iPowerVoice['sound_length_unit']}</option>";	
	} else {
		$length_select .= "<option value=\"15\" >15 {$iPowerVoice['sound_length_unit']}</option>";	
	}
	if ($pvoice_array['length'] == "10")	
	{			
		$length_select .= "<option value=\"10\" selected=\"selected\" >10 {$iPowerVoice['sound_length_unit']}</option>";
	} else {
		$length_select .= "<option value=\"10\" >10 {$iPowerVoice['sound_length_unit']}</option>";
	}
	if ($pvoice_array['length'] == "5")	
	{	
		$length_select .= "<option value=\"5\" selected=\"selected\" >5 {$iPowerVoice['sound_length_unit']}</option>";		
	} else {	
		$length_select .= "<option value=\"5\" >5 {$iPowerVoice['sound_length_unit']}</option>";
	}
	$length_select .= "</select>";			
	$powervoice_content .= "<tr><td style=\"vertical-align:bottom\" align='left' >".$iPowerVoice['sound_length']."</td><td style=\"vertical-align:bottom\" align='left'>{$length_select} </td></tr>";
	$powervoice_content .= "</table>";
	$powervoice_content .= "</p>";
}

?>

<script language="javascript">
function triggerCourse(obj, isCheck){
	if (!isCheck)
	{
		obj.ec_mgt_course.checked = false;
		obj.ec_mgt_course.disabled = true;
	} else
	{
		obj.ec_mgt_course.disabled = false;
	}

	return;
}
</script>

<form name="form1" action="settings_update.php" method="post">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_Settings, '') ?>
<?= displayTag("head_eclasssettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>


<p>
<span class="extraInfo">[<span class=subTitle><?=$i_frontpage_menu_eclass_mgt?></span>]</span>
<div>
<input type=checkbox onClick="triggerCourse(this.form, this.checked)" name="ec_mgt" value=1 <?=$ec_mgt_checked?>> <?=$i_eClass_management_enable?></div>
<div>
&nbsp; &nbsp; &nbsp; <input type=checkbox name="ec_mgt_course" value=1 <?=$ec_mgt_course_checked?>> <?=$i_eClass_teacher_open_course?></div>
</p>

<hr size=1 class="hr_sub_separator">

<p>
<span class="extraInfo">[<span class=subTitle><?=$i_eClass_email_function?></span>]</span>
<div><?=$i_eClass_email_function_enable?></div>
<table width=150 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=1>
<tr><td align='center' nowrap class='tableTitle_new'><u><?= $i_Set_enabled ?></u></td><td align='center' class='tableTitle_new'><u><?=$i_Campusquota_identity?></u></td></tr>
<tr><td align='center'><input type="checkbox" name="ec_email_teacher_disabled" value=1 <?=$ec_email_teacher_0_checked?>></td><td style="vertical-align:bottom" align='center'><?=$i_general_Teacher?></td></tr>
<tr><td align='center'><input type="checkbox" name="ec_email_helper_disabled" value=1 <?=$ec_email_helper_0_checked?>></td><td style="vertical-align:bottom" align='center'><?=$i_eClass_identity_helper?></td></tr>
<tr><td align='center'><input type="checkbox" name="ec_email_student_disabled" value=1 <?=$ec_email_student_0_checked?>></td><td style="vertical-align:bottom" align='center'><?=$i_identity_student?></td></tr>
</table>
</p>

<!-- Eric Yip (20090803): Hide in eClassIP25
<hr size=1 class="hr_sub_separator">
<p>
<span class="extraInfo">[<span class=subTitle><?=$i_eClass_planner_function?></span>]</span>
<div><?=$i_eClass_planner_type_color?></div>
<table width=300 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=1>
<tr><td align='center' nowrap class='tableTitle_new'><u><?= $i_eClass_planner_color ?></u></td><td align='center' class='tableTitle_new'><u><?=$i_eClass_planner_type?></u></td></tr>
<?= $planner_color_html ?>
</table>
</p>
-->

<!-- badword -->
<hr size=1 class="hr_sub_separator">
<p>
<span class="extraInfo">[<span class=subTitle><?=$i_frontpage_schoolinfo_groupinfo_group_bulletin?></span>]</span>
<div><?=$i_eClass_Bulletin_BadWords_Instruction_top?></div>
<textarea name=data COLS=60 ROWS=20>
<?=$data?>
</textarea>
<br><span class="extraInfo"><?=$i_CampusMail_New_BadWords_Instruction_bottom?></span>
</p>
<!-- badword -->

<?=$powervoice_content?>

</blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include("../../../templates/adminfooter.php");
?>