<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libeclass.php");
include("../../includes/libfilesystem.php");
eclass_opendb();

$lo = new libeclass();
$lo->eClassDel($course_id);

# Remove records in INTRANET_COURSE
$li = new libdb();
$list = implode(",",$course_id);
$sql = "DELETE FROM INTRANET_COURSE WHERE CourseID IN ($list)";
$li->db_db_query($sql);
$sql = "DELETE FROM INTRANET_COURSE_HOMEWORK WHERE CourseID IN ($list)";
$li->db_db_query($sql);

# Update Equation Editor License
if ($eclass_equation_mck != "")
{
    $content = trim(get_file_content($lo->filepath."/files/equation.txt"));
    $equation_class = ($content=="")? array(): explode(",",$content);
    $updated_class = array();

    for ($i=0; $i<sizeof($equation_class); $i++)
    {
         $target = $equation_class[$i];
         if (!in_array($target,$course_id))
         {
              $updated_class[] = $target;
         }
    }

    write_file_content(implode(",",$updated_class),$lo->filepath."/files/equation.txt");
}



eclass_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&msg=3&RoomType=$RoomType");
?>