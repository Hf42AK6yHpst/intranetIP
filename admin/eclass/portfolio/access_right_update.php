<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclass.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("$eclass_filepath/src/includes/php/lib-groups.php");
include_once("../../../lang/lang.$intranet_session_language.php");

# include eClass's language file
$tmp_room_type = $ck_room_type;
$ck_room_type = "iPORTFOLIO";
include_once("$eclass_filepath/system/settings/lang/$lang");

eclass_opendb();


# eClass
$lo = new libeclass($course_id);
$RoomType = $lo->getEClassRoomType($course_id);

// maybe set by checking what rights are allowed to be set
$right_ids = "1,2";

$lg = new libgroups($eclass_prefix."c$course_id");


// reading room
if ($RoomType!=4)
{
	header("Location: ../");
}



include_once("common.php");

$access_config = getFormValue($portfolio_functions);

$portfolio_setting_file = "$eclass_filepath/files/portfolio_right.txt";

$li_fm = new fileManager($eclass_db, 0, 0);

$li_fm->writeFile(serialize($access_config), $portfolio_setting_file);

eclass_closedb();

header("Location: access_right.php?course_id=$course_id&msg=2");
?>