<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libeclass.php");
include("../../includes/libfilesystem.php");
eclass_opendb();

$lo = new libeclass();

$sql = "SELECT course_id FROM course WHERE RoomType='$RoomType' ";
$course_id = $lo->returnVector($sql);

# Update Equation Editor License
if ($eclass_equation_mck != "")
{
    $content = trim(get_file_content($lo->filepath."/files/equation.txt"));
    $equation_class = ($content=="")? array(): explode(",",$content);
    $updated_class = array();

    for ($i=0; $i<sizeof($equation_class); $i++)
    {
         $target = $equation_class[$i];
         if (!in_array($target,$course_id))
         {
              $updated_class[] = $target;
         }
    }

    write_file_content(implode(",",$updated_class),$lo->filepath."/files/equation.txt");
}


$lo->eClassDel($course_id);
/*
for ($i=0; $i<sizeof($list); $i++)
{
     $lo->eClassDel($course_id[$i]);
}
*/

# Remove records in INTRANET_COURSE
$li = new libdb();
$sql = "DELETE FROM INTRANET_COURSE";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_COURSE_HOMEWORK";
$li->db_db_query($sql);

eclass_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&msg=3&RoomType=$RoomType");
?>