<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclass.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
intranet_opendb();

/* parameters
1. fid = fileID
OR
1. categoryID
2. fpath = VirPath + Title

optional: $courseID
*/

if ($courseID=="") $courseID=$ck_course_id;

$fm = new fileManager($courseID, $categoryID, $folderID);
//get fileID from given path

$fid_name = ($fid!="") ? $fid : basename($fpath);
if ($fid=="") {
	$fid = $fm->openFirstFile($fpath, $categoryID);
	$fid = $fm->fileID;
}

// permission control
if ($fid=="" || $fid==0) {
	//no file found
	header("Location: help.php?err=15");
	die();
}

$li = new libdb();
$li->db = classNamingDB($courseID);

$sql = "SELECT FileID, Location, User_id, Title, Category, Permission FROM eclass_file ";
if ($fid!="") {
	//get from fileID
	$sql .= " WHERE FileID='$fid' ";
} elseif ($categoryID!="" && $fpath!="") {
	$posit = strrchr($fpath, "/");
	$vpath = substr($fpath, 0, $posit-1);
	$title = substr($fpath, $posit+1, strlen($fpath)-$posit-1);
	$sql .= " WHERE Category='$categoryID' AND VirPath='$vpath' AND Title='$title' ";
}
$row = $li->returnArray($sql, 6);

if (sizeof($row)>0) {
	$FileID = $row[0][0];
	$Location = $row[0][1];
	$User_id = $row[0][2];
	$filename = $row[0][3];
	$categoryID = $row[0][4];
	$Permission = $row[0][5];

	$filepath = $fm->getCategoryRoot($categoryID, $li->db)."/".$Location."/".$filename;

	if (file_exists($filepath)) {
		/*
		//determinate file type
		$fileType = strtoupper(strrchr($filename, "."));

		$isDirectOpenURL = ($fileType==".HTML" || $fileType==".HTM" || $fileType==".PPT" || $fileType==".MP3" ||  $fileType==".MID" || $fileType==".MIDI" || $fileType==".RMI" || $fileType==".ASF" || $fileType==".WM" || $fileType==".WMA" || $fileType==".WMV" || $fileType==".AVI" || $fileType==".MPEG" || $fileType==".MPG" || $fileType==".MP2" || $fileType==".WAV" || $fileType==".AU" || $fileType==".RA" || $fileType==".RM" || $fileType==".RAM" || $fileType==".RMS" || $fileType==".PDF" || $fileType==".SWF");

		if (!$isDirectOpenURL) {
			header("Cache-control: private");		// fix for IE
		}

		if ($fileType==".DOC" || $fileType==".XLS" || $fileType==".CSV") {
			// word/excel
			header("Content-Type: application/x-msword");
		} elseif ($fileType==".JPG" || $fileType==".JPE" || $fileType==".GIF" ||
			$fileType==".PNG" || $fileType==".BMP") {
			// JPEG, JPG, GIF, PNG
			header("Content-Type: image/jpeg");
		} elseif ($fileType==".TXT") {
			// text
			header("Content-Type: text/plain");
		} elseif ($fileType==".SWF_tmp") {
			// flash
			header("Content-Type: application/x-shockwave-flash");
		} elseif ($isDirectOpenURL) {
			// HTML/PPT
		*/
		$url = $eclass_url_root."/".$fm->getCategoryFile($categoryID, $li->db)."/".$Location."/".$filename;
		/*
		} else {
			header('Content-Type: application/octet-stream');
		}
		*/
		$isDirectOpenURL = true;

		if ($isDirectOpenURL)
		{
			header("Location: $url");
		} else {
			$len = filesize($filepath);
			header("Content-Length: $len");
			header("Content-Disposition: inline; filename=".$filename);
			readfile($filepath);
		}
	} else {
		header("Location: help.php?err=15");
		die();
	}
}

intranet_closedb();
?>