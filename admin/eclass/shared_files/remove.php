<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclass.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
intranet_opendb();

$fm = new fileManager($courseID, $categoryID, $folderID);
$params = "courseID=$courseID&categoryID=$categoryID&folderID=$folderID";

for($i=0;$i<sizeof($file_id);$i++){
	$fm->deleteFile($file_id[$i]);
}

$fm->updateFolderSize($folderID);

intranet_closedb();

header("Location: index.php?msg=3&$params&reload=1");
?>
