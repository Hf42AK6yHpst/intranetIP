<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libalbum.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

$lo = new libalbum($album_id);
$lo->db = $intranet_db;
$album_list = $lo->returnAlbumList();

$total_photo = 10;
for ($i=0; $i<$total_photo; $i++)
{
	$photo_input .= ($i>0) ? "<tr><td colspan=3>&nbsp;</td></tr>\n" : "";
	$photo_input .= "<tr><td>".($i+1).".</td>\n";
	$photo_input .= "<td nowrap>".$i_select_file.":</td>\n";
	$photo_input .= "<td><input class=file type=file name='userfile[]' size=43><input type=hidden name=userfile_hidden[]></td>\n";
	$photo_input .= "</tr><tr>\n";
	$photo_input .= "<td>&nbsp;</td>\n";
	$photo_input .= "<td nowrap>".$i_general_description.":</td>\n";
	$photo_input .= "<td><input type='text' name='photo_description[]' value='' size=52 maxlength='255'></td>\n";
	$photo_input .= "</tr>\n";
}

?>
<script language="javascript">
function checkform(obj){

	var len=obj.elements.length;
	var i=0;
	var flag=0;

	for( i=0 ; i<len; i++) 
	{
		if (obj.elements[i].name=="userfile[]" && obj.elements[i].value != "")
			flag = 1;		
	}

	if(flag == 0)
	{
		alert("<?php echo $jr_warning['album_photo_added']; ?>.");
		return false;
	}
	else
		Big5FileUploadHandler(obj);
}

function Big5FileUploadHandler(obj) 
{			
	var len = obj.elements["userfile[]"][0].value;	

	for (var i=0; i<10; i++)
	{		
		if (typeof(obj.elements["userfile[]"][i])!='undefined')
		{				
			if (obj.elements["userfile[]"][i].value != '')
			{		
				var Ary = obj.elements["userfile[]"][i].value.split('\\');	
				obj.elements["userfile_hidden[]"][i].value = Ary[Ary.length-1];				
			}
		}
	}		
	return true;
}
</script>

<form name="form1" method="post" action="photo_update.php" enctype="multipart/form-data" onSubmit="return checkform(this);">
<?= $lo->displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_admintitle_im_announcement, '/admin/photoalbum/', $album_list) ?>

<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
  <?=$photo_input?>  
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type="hidden" name="album_id" value="<?=$album_id?>">

<input type=hidden name=pageNo value="<?php $pageNo; ?>">
<input type=hidden name=order value="<?php $order; ?>">
<input type=hidden name=field value="<?php $field; ?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>