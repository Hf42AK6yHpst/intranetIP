<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libalbum.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$album_id = trim($_POST['album_id']);
$photo_id = trim($_POST['photo_id']);

$li = new libalbum();
$li->updatePhoto($photo_id, $photo_description);


intranet_closedb();
header("Location: list_photo.php?album_id=$album_id&count=$count");
?>