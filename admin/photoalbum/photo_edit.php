<?php

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libalbum.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

$album_id = $_GET['album_id'];
//$album_name = $_GET['album_name'];
$ItemID = $_GET['ItemID'];

$photo_id= $ItemID[0];

$li = new libalbum($album_id);
$li->db = $intranet_db;
$li->photo_limit = array(300, 300);
$li->loadPhoto($photo_id);
$album_list = $li->returnAlbumList();

?>

<script language="javascript">
function viewPhoto(pid){
	newWindow('photo_view.php?album_id=<?=$album_id?>&photo_id='+pid, 16);
	return ;
}
</script>


<form name="form1" method="post" action="photo_edit_update.php" enctype="multipart/form-data" onSubmit="return checkform(this);">
<?= $li->displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_admintitle_im_announcement, '/admin/photoalbum/', $album_list, 'Edit', '') ?>
<blockquote>


<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr>
<td nowrap valign="top"><?=$i_AlbumPhotoFile?>:</td>
<td><?=$li->returnPhotoImg()?></td>
</tr>
<tr>
<td nowrap><?=$i_general_description?>:</td>
<td><input type="text" name="photo_description" value="<?=$li->returnPhotoDescription(1)?>" size=45 maxlength="255"></td>
</tr>
</table>
</blockquote>

<table width=95% border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name="photo_id" value="<?=$photo_id?>">
<input type=hidden name="album_id" value="<?=$album_id?>">
<input type=hidden name="count" value="<?=$count?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>