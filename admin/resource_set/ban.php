<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

$list = get_file_content("$intranet_root/file/resourcebooking_banlist.txt");

?>

<form name="form1" action="ban_update.php" method="post">
<?= displayNavTitle($i_admintitle_fs, '', $i_admintitle_fs_resource_set, 'index.php',$i_BookingBanList,'') ?>
<?= displayTag("head_resources_set_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>

<table border=0 cellpadding=5 cellspacing=0>
<tr><td><?=$i_BookingBanList_Instruction?></td></tr>
<tr><td><textarea name=banlist rows=10 cols=40><?=$list?></textarea></td></tr>
</table>
</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>