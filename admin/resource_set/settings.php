<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_setting.php");

$li = new libfilesystem();
$id_names = array($i_identity_teachstaff,$i_identity_student,$i_identity_parent);
$file_content = trim($li->file_read($intranet_root."/file/resource_set.txt"));
if ($file_content == "")
{
    $access_right = array(1,1,1);
}
else
{
    $content = explode("\n", $file_content);
    $access_right = array($content[0],$content[1],$content[3]);
}
?>

<form name="form1" action="update.php" method="post">
<?= displayNavTitle($i_admintitle_fs, '', $i_admintitle_fs_resource_set, 'index.php',$i_general_BasicSettings,'') ?>
<?= displayTag("head_resources_set_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<td>
<blockquote>
<p><br><br>
<table border=0 cellpadding=5 cellspacing=0>
<tr>
<td class=tableTitle_new><u><?= $i_Campusquota_identity ?></u></td>
<td class=tableTitle_new><u><?=$i_Set_enabled ?></u></td>
</tr>
<?php for($i=0; $i<sizeof($access_right); $i++) {
if ($access_right[$i] == "" || $access_right[$i]==1)
{
    $checked = " CHECKED";
}
else
{
     $checked = "";
}
?>
<tr>
<td style="vertical-align: middle"><?=$id_names[$i]?></td>
<td><input type=checkbox name=allow<?=$i?><?=$checked?> value=<?=1?>></td>
</tr>
<?php } ?>
</table>
</blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>