<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdb.php");
include_once("../../includes/liblslp.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$ls = new lslp();

$license_type = $ls->getLicenseType();
$license_period = $ls->getLicensePeriod();
?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_LSLP['LSLP'], '') ?>
<?= displayTag("head_lslp_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?php if (trim($warning_message) != "") { ?>
<div><?=$warning_message.'<br><br>'?></div>
<?php } ?>
<div><?=$i_LSLP['license_type'].": ".$license_type?></div>
<div><?=$i_LSLP['license_period'].": ".$license_period?></div>
<?php

	echo displayOption($i_LSLP['admin_user_setting'], 'admin_setting.php',1);
	if(!$ls->hasFullLicense())
		echo displayOption($Lang['ModuleLicense']['LicenseManagement'], 'user_setting.php',1);
	
?>	
<!--<?=$display?>-->

</blockquote>
</td></tr>
</table>

<?php
intranet_closedb();

include_once("../../templates/adminfooter.php");
?>