ajaxCaller.shouldDebug = false;
var timeURL = "process_status.php";

window.onload = function() {
	$("defaultTime").onclick = requestDefaultTime;
	// $("customTime").onclick=requestCustomTime;
	requestBothTimes();
	setInterval(requestBothTimes, 2000);
}

function requestBothTimes() {
	requestDefaultTime();
	// requestCustomTime();
}

function requestDefaultTime() {
	ajaxCaller.getPlainText(timeURL, showDefaultTime);
}

function showDefaultTime(text) {
	var defaultTimeLabel = $("defaultTimeLabel");
	var BackupStatus = $("BackupStatus");
	
	defaultTimeLabel.innerHTML = text;

	if (text == "1" && BackupStatus.value == "notdone") {
		setCount(1);
		defaultTimeLabel.innerHTML = "Ready to start";
		 document.form1.manual_backup_start.disabled=false;
	} else if (text == "5") {
		setCount(10);
		defaultTimeLabel.innerHTML = "Backup Process Start";
	     document.form1.manual_backup_start.disabled=true;
	} else if (text == "10") {
		setCount(20);
		defaultTimeLabel.innerHTML = "Checking Backup Space";
		document.form1.manual_backup_start.disabled=true;
	}  else if (text == "15") {
		setCount(30);
		defaultTimeLabel.innerHTML = "Packing System Files";
		document.form1.manual_backup_start.disabled=true;
	}  else if (text == "20") {
		setCount(40);
		defaultTimeLabel.innerHTML = "Packing Database Files";
		document.form1.manual_backup_start.disabled=true;
	}  else if (text == "30") {
		setCount(50);
		defaultTimeLabel.innerHTML = "Verifying File Backup";
		document.form1.manual_backup_start.disabled=true;
	}  else if (text == "32") {
		setCount(60);
		defaultTimeLabel.innerHTML = "Verifying File Backup Pass";
		document.form1.manual_backup_start.disabled=true;
	}  else if (text == "40") {
		setCount(70);
		defaultTimeLabel.innerHTML = "Verifying MySQL Backup";
		document.form1.manual_backup_start.disabled=true;		
	}  else if (text == "42") {
		setCount(80);
		defaultTimeLabel.innerHTML = "Verifying MySQL Backup Pass";
		document.form1.manual_backup_start.disabled=true;
	}  else if (text == "50") {
		setCount(90);
		defaultTimeLabel.innerHTML = "Splitting Files";
		document.form1.manual_backup_start.disabled=true;
		//Finalizing Backup Report step may jump too fast and skipped
		BackupStatus.value = "done";
	}  else if (text == "60") {
		setCount(95);
		defaultTimeLabel.innerHTML = "Finalizing Backup Report";
		BackupStatus.value = "done";
	} else if (text == "1" && BackupStatus.value == "done") {
		defaultTimeLabel.innerHTML = "Backup is done";
		setCount(100);
		document.form1.manual_backup_start.disabled=false;
     } else if (text == "110") {
		defaultTimeLabel.innerHTML = "No space left on devices";
		setCount(1);
		document.form1.manual_backup_start.disabled=false;
     } else if (text == "120") {
 		defaultTimeLabel.innerHTML = "File Backup Verify Fail";
 		setCount(1);
 		document.form1.manual_backup_start.disabled=false;
     } else if (text == "130") {
  		defaultTimeLabel.innerHTML = "MySQL Backup Verify Fail";
  		setCount(1);
  		document.form1.manual_backup_start.disabled=false;
     } 
	else
     {
       defaultTimeLabel.innerHTML = "Error";
     }
     
}// end function

/*
 * function requestCustomTime() { var vars = new Array(); vars["showWeekday"]=0;
 * vars["showTimezone"]=0; vars["showYear"]=1; var callingContext =
 * Math.round(Math.random() * 100); ajaxCaller.get(timeURL, vars,
 * showCustomTime, false, callingContext); }
 *
 * function showCustomTime(text, callingContext) { var customTimeLabel =
 * $("customTimeLabel"); customTimeLabel.innerHTML = text + "." +
 * callingContext; }
 */

