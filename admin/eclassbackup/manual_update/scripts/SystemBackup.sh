#!/bin/sh

#=====================================================================
# Don't make any changes below
#=====================================================================
version="20140616"
outconfigfile="SystemBackupConfig"

#-------------------------
# Assign variables
#-------------------------
function ASSIGN_VARIABLES
{
        timeStamp=`date +%Y%m%d_%H%M`
		timestamp_sql=`date "+%Y-%m-%d %H:%M:%S"`
        backupType="eClass_backup"
        backupMysql="Mysql"
        backupMysqlName=$backupMysql"_"$systemName"_"$timeStamp

		backupFolderName=$backupType"_"$systemName"_"$timeStamp
        backupFileName=$backupFolderName".tar"
        backupFileDestination=$destination_Path"/"$backupFolderName
        backupFileFinalPath=$backupFileDestination"/"$backupFileName
        eclassBackupScriptLog="/tmp/eclassbackupLog"

        backupMysqlFinalPath=$backupFileDestination"/"$backupMysqlName".sql"
		#get current script path
		scriptpath=`/usr/bin/dirname $PRE_EXE_SCRIPT`
}

#-------------------------
# Check if the user is root
#-------------------------
function CHECK_PRIVILEGE
{
        if [ `id -u` -ne 0 ]; then
                echo "You need root privileges to run this script."
                exit
        fi
}

#-------------------------
# Check Parameters
#-------------------------
function CHECK_PARAMETERS
{
        PARAMETERS_FLAG="false"

        if [ -z "$fileDivideSize" ]; then
                echo "fileDivideSize cannot be null"
                PARAMETERS_FLAG="true"
        fi

        if [ -z "$backupBreakDown" ]; then
                echo "backupBreakDown cannot be null"
                PARAMETERS_FLAG="true"
        fi

        if [ -z "$systemName" ]; then
                echo "systemName cannot be null"
                PARAMETERS_FLAG="true"
        fi

        if [ -z "$systemPath" ]; then
                echo "systemPath cannot be null"
                PARAMETERS_FLAG="true"
        fi

        if [ -z "$destination_Path" ]; then
                echo "destination_Path cannot be null"
                PARAMETERS_FLAG="true"
        fi

        if [ -z "$mysql_login" ]; then
                echo "mysql_login cannot be null"
                PARAMETERS_FLAG="true"
        fi

        if [ -z "$mysql_password" ]; then
                echo "mysql_password cannot be null"
                PARAMETERS_FLAG="true"
        fi
		
#	if [ -z "$SMS_CONFIG" ]; then
        if [ -z "$SMS_CONFIG" ] && [ -n "$SMS_CONFIG" ]; then
                echo "SMS CONFIG cannot be null"
                PARAMETERS_FLAG="true"
        fi
		
		if [ -z "$EMAIL_CONFIG" ]; then
                echo "EMAIL CONFIG cannot be null"
                PARAMETERS_FLAG="true"
        fi
		
		if [ -z "$SMS_ALERT" ]; then
                echo "SMS ALERT cannot be null"
                PARAMETERS_FLAG="true"
        fi
		
		if [ -z "$EMAIL_ALERT" ]; then
                echo "EMAIL ALERT cannot be null"
                PARAMETERS_FLAG="true"
        fi
		
		if [ -z "$POST_EXE_SCRIPT" ]; then
                echo "POST_EXE_SCRIPT cannot be null"
                PARAMETERS_FLAG="true"
        fi
		if [ -z "$file_rotate_day" ]; then
                echo "file_rotate_day cannot be null"
                PARAMETERS_FLAG="true"
        fi



}

#------------
# Report function 
#------------

function POST_BACKUP_REPORT
{
		cd $scriptpath
		ERROR_STR=`/usr/bin/php $POST_EXE_SCRIPT $timestamp_sql DONE $TempBackupFileSize`
}

function PRE_BACKUP_REPORT
{
		cd $scriptpath
		#try detect the position of our place
		#cannot execute when script runs  by cronjob
		ERROR_STR=`/usr/bin/php $PRE_EXE_SCRIPT $timestamp_sql BACKUP_START 0`
}

function BACKUP_INTERRUPT_REPORT
{
		cd $scriptpath
		ERROR_STR=`/usr/bin/php $POST_EXE_SCRIPT $timestamp_sql $1 0`
		#kill SystemBackup.sh after report error
		exit 0
}



#------------
# Split files
#------------
function FILE_BREAKDOWN
{
        File_For_Split=$1
        File_Break_Down_Size=$2
        File_Break_Down_Name=$3
        Linux_Restore_Filename=$File_For_Split

        Backup_File_Size=`du -sm $File_For_Split | sed 's/[[:blank:]]/:/g' | cut -d ":" -f1`

        if [ $Backup_File_Size -gt $File_Break_Down_Size ];then
                split -b $File_Break_Down_Size"m" $File_For_Split $File_Break_Down_Name
                Temp_Combine=`ls  $File_Break_Down_Name* -x`
                Temp_Path=`echo -n "cat $Temp_Combine  > $Linux_Restore_Filename "`
                `echo -n $Temp_Path > $Linux_Restore_Filename".sh"`
                chmod 755 $Linux_Restore_Filename".sh"
                rm -f $File_For_Split
        fi
}


#-----
# Help
#-----
function HELP
{
        echo ""
        echo "SYNOPSIS"
        echo "  SystemBackup.sh <File>"
        echo ""
        echo "DESCRIPTION"
        echo "A basic archiving program designed to backup eClass related system or eClass related Database in tar format"
        echo "where"
        echo "[File]=Configuration file for backup"
        echo "[option]"
        echo "  -f, --create-file"
        echo "          Create configuration file"
        echo "  -v, --version"
        echo "          display version of program"
        echo "  -h, --help"
        echo "          help"
        echo ""
        echo "AUTHOR"
        echo "  Written by support@broadlearning.com"
        echo ""
        echo "REPORTING BUGS"
        echo "  Report bugs to <support@broadlearning.com>."
        exit
}

#------------
# Option Menu
#------------
function OPTION_MENU
{
        case $1 in
          -f|--create-file)
                CREATE_CONFIG_FILE;;
          -h|--help)
                HELP;;
          -v|--version)
                echo "Version: $version";;
          *)    BACKUP_PROCESS $1;;
        esac
        exit
}


#----------------
# Backup System
#----------------
function BACKUP_SYSTEM
{
        cd;clear
#       $httpdServicePath stop

        echo
        echo "Backup processing ........."
        echo
        mkdir $backupFileDestination

        tar -cvf $backupFileFinalPath $systemPath

#       $httpdServicePath start
}

#----------------
# Backup System Differential
#----------------
function BACKUP_SYSTEM_DIFF
{
	  cd;clear
	  
	  echo
        echo "Backup processing ........."
        echo
        mkdir $backupFileDestination
       
       tar -N $last_full_backup_day -cvf  $backupFileFinalPath $systemPath
}

#----------------
# Backup Backup Path for Diff
#----------------
function CHANGE_DIFF_PATH
{
	backupFolderName=$backupType"_"$systemName"_"$timeStamp"-diff-"$last_full_backup_day
    backupFileName=$backupFolderName".tar"
    backupFileDestination=$destination_Path"/"$backupFolderName
    backupFileFinalPath=$backupFileDestination"/"$backupFileName
    backupMysqlFinalPath=$backupFileDestination"/"$backupMysqlName".sql"
}

#----------------
# Backup Mysql
#----------------
function BACKUP_MYSQL
{
        echo
        echo -n "Backup Mysql ..."
        #mysqldump -u$1 -p$2 --all-databases --single-transaction   > $backupMysqlFinalPath
        . $scriptpath/../manual_update/scripts/backup_mysql.sh % $2 $backupFileDestination
        backup_mysql_result=$?
}

#----------------
# Verify System Backup
#----------------
function BACKUP_SYSTEM_VERIFY
{
	echo "Verifying File Backup" >> /tmp/eclassbackup
	tar tvf $backupFileFinalPath > /dev/dull 2> /tmp/error_log
	if [  -n "`cat /tmp/error_log`" ]; then
		echo "File Backup Verify Fail" >> /tmp/eclassbackup
		BACKUP_INTERRUPT_REPORT FILE_BACKUP_VERIFY_FAIL
	else
		echo "File Backup Verify Pass" >> /tmp/eclassbackup
	fi
	
	rm -f /tmp/error_log
}

#----------------
# Verify MySQL Backup
#----------------
function BACKUP_MYSQL_VERIFY
{
	echo "Verifying MySQL Backup" >> /tmp/eclassbackup

	
	#rm -f /tmp/error_log
	
	#     tail  "$backupMysqlFinalPath" -n1 |grep "Dump completed" > /dev/null
    #            mysqltailstatus=$?

    #            chmod 600 $backupMysqlFinalPath

                #if [ $mysqltailstatus == 0 ]
                if [ $backup_mysql_result == 0 ]
                then
                        logtime=`date +%d/%m/%Y:%H:%M:%S`
						echo "MySQL Backup Verify Pass" >> /tmp/eclassbackup

                else
          	        echo "MySQL Backup Verify Fail" >> /tmp/eclassbackup
		            BACKUP_INTERRUPT_REPORT MYSQL_BACKUP_VERIFY_FAIL

                fi

}


#----------------
# Backup Folders/Files Rotation
# this function only remove outdated folder named Full_backup_eClassSystem*
#/usr/bin/find $destination_Path -type d -mtime +$file_rotate_day  -name "*eClass_backup*" >> /tmp/eclassbackup 
#----------------
function BACKUP_File_Rotation
{
	# print target deleted file in log 
	/usr/bin/find $destination_Path -type d -mtime +$file_rotate_day  -name "*eClass_backup*" >> /tmp/eclassbackup
	
	#for rotate remove command checking
	#echo $destination_Path >> /tmp/eclassbackup_check
	#echo $file_rotate_day >> /tmp/eclassbackup_check
	
	#delete Backup
	/usr/bin/find $destination_Path -type d -mtime +$file_rotate_day  -name "*eClass_backup*"  -exec rm -Rfv {} \; 
}

#---------

# Check OS
#---------
function CHECK_OS
{
        OsType=`egrep "(Red Hat|Debian|Cobalt)" /etc/issue`

        if [ ! -n "$OsType" ]; then
                mysqlServicPath=/etc/rc.d/init.d/mysqld
                httpdServicePath=/etc/rc.d/init.d/httpd
        fi

        if [  -n "`less /etc/issue | grep "Red Hat"`" ]; then
                mysqlServicPath=/etc/rc.d/init.d/mysqld
                httpdServicePath=/etc/rc.d/init.d/httpd
        fi

        if [  -n "`less /etc/issue | grep "Debian"`" ]; then
                mysqlServicPath=/etc/init.d/mysql
                httpdServicePath=/etc/init.d/apache
        fi

        if [  -n "`less /etc/issue | grep "Cobalt"`" ]; then
                mysqlServicPath=/etc/rc.d/init.d/mysql
                httpdServicePath=/etc/rc.d/init.d/httpd
        fi

        if [  -n "`less /etc/issue | grep "SUSE"`" ]; then
                mysqlServicPath=/etc/init.d/mysql

                if [  -e /etc/init.d/apache ]; then
                        httpdServicePath=/etc/init.d/apache
                elif [  -e /etc/init.d/apache2 ]; then
                        httpdServicePath=/etc/init.d/apache2
                fi
        fi
}

#------------------------
# Split the backuped file
#------------------------
function SPLIT_FILE
{
        BackupedFileSize=`du -s $backupFileFinalPath | sed 's/[[:blank:]]/:/g' | cut -d ":" -f1`
        CurrentPartitionSize=`df "$destination_Path" -P | sed 's/  */#/g' | grep "/" | cut -d"#" -f4`
		echo $BackupedFileSize
		echo $CurrentPartitionSize
		
        if [ "$BackupedFileSize" -le "$CurrentPartitionSize" ]; then
                echo -n "File Splitting ......."
                FILE_BREAKDOWN $backupFileFinalPath $fileDivideSize $backupFileDestination"/"$backupFolderName"_disk_"
                echo "          [ Finish ]"
                echo "Backup process finished !!"
        else
                echo "Backup process finished !!"
                echo "Warning : Backuped file didn't split to multiple files since not enough space."
                echo "`date +%c`: Backuped file didn't split to multiple files since not enough space." >> $eclassBackupScriptLog
                echo
        fi
}

#----------------------
# Create config file
#----------------------
function CREATE_CONFIG_FILE
{
        echo "#!/bin/sh" > $outconfigfile
        echo "" >> $outconfigfile
        echo "# Size of the break down files, default is 650MB" >> $outconfigfile
        echo "fileDivideSize=650" >> $outconfigfile
        echo "" >> $outconfigfile
        echo "# Break down single backup file into multiple files" >> $outconfigfile
        echo "backupBreakDown=\"true\"" >> $outconfigfile
        echo "" >> $outconfigfile
        echo "# File name of the backup" >> $outconfigfile
        echo "systemName=\"eClassSystem\"" >> $outconfigfile
        echo "" >> $outconfigfile
        echo "# Path to be backup" >> $outconfigfile
        echo "systemPath=\"/home/eclass\"" >> $outconfigfile
        echo "" >> $outconfigfile
        echo "# Folder to save the backup" >> $outconfigfile
        echo "destination_Path=\"/home/eclassBackup\"" >> $outconfigfile
        echo "" >> $outconfigfile
        echo "# Mysql login detail" >> $outconfigfile
        echo "mysql_login=" >> $outconfigfile
        echo "mysql_password=" >> $outconfigfile
        echo "Config file created, file name is $outconfigfile"
}

#----------------------
# Check available space
#----------------------
function CHECK_AVAILABLE_SPACE
{
        BackupedFileSize=
        echo -n "Checking storage space........."

        for i in $systemPath
        do
            TempPartitionSize=`du -s $i | sed 's/[[:blank:]]/:/g' | cut -d ":" -f1`
            let "BackupedFileSize = $BackupedFileSize + $TempPartitionSize"
        done

		#[BUG] this df command will hang if NFS services is dead, please check NFS services status here first!!
		#also report to post-process command
		CurrentPartitionSize=`df $destination_Path -P | sed 's/  */#/g' | grep "/" | cut -d"#" -f4`
        let "TempCalculateValue = $BackupedFileSize * 2"

        if [ $CurrentPartitionSize -gt $TempCalculateValue ]
        then :
        else
                echo
                echo "`date +%c`: Not enought space for saving the backup." >> $eclassBackupScriptLog
                echo "Not enough space for saving the backup, try to run this script on another partition."
                echo "Program terminate!!"
				#write log to eClassBackup
				echo "Not enough space" >> /tmp/eclassbackup
				BACKUP_INTERRUPT_REPORT NO_FREE_SPACE
                exit
        fi
}


#----------------------
# Check Backup File Size after Backup
# parameter $1=BackupFile Path
#----------------------
function CHECK_BACKUP_FILE_SIZE
{
        TempBackupFileSize=`du -s $backupFileDestination | sed 's/[[:blank:]]/:/g' | cut -d ":" -f1`
		let "TempBackupFileSize = $TempBackupFileSize / 1024"
}




#----------------------
# Backup process
#----------------------
function BACKUP_PROCESS
{
         		#[BUG] Text Log output required for whole backup process

        # Import configuration files
        . $1

        #Check parameters
        CHECK_PARAMETERS
        ASSIGN_VARIABLES

		#Execute Pre Backup PHP
		#reset eclassbackup log
		echo "Backup Process Start" > /tmp/eclassbackup
		#chmod to 777 for later settings update, this file has to be remove
		chown apache.apache /tmp/eclassbackup

		PRE_BACKUP_REPORT

        if [ ! -e $destination_Path ]; then
                mkdir -p $destination_Path
        fi
        
                if [ "$PARAMETERS_FLAG" == "true" ]; then
                echo "Configuration file incorrect!! Please check."
				echo "Configuration file incorrect!! Please check." >> /tmp/eclassbackup
                exit
        else
				echo "Checking Backup Space" >> /tmp/eclassbackup
                CHECK_AVAILABLE_SPACE
                CHECK_OS
				echo "Packing System Files" >> /tmp/eclassbackup

				#check use diff or full backup for this job




				#case1: if backup_method = full >  BACKUP_SYSTEM
				#case2: if  TODAY minus last_full_backup less than differential_span than use BACKUP_SYSTEM_DIFF
				#case3: if  TODAY - last_full_backup greater than differential_span use BACKUP_SYSTEM
				if [ "$backup_method" == "full" ]; then
					BACKUP_SYSTEM
				     else if [ "$backup_method" == "diff" ]; then
					#get unix time
					TODAY=`date +%s`
					RESULT=$[TODAY-last_full_backup]
                              if [ "$RESULT" -lt "$differential_span" ]; then
						CHANGE_DIFF_PATH
						BACKUP_SYSTEM_DIFF
					     else
						BACKUP_SYSTEM
					     fi
				      fi
                   fi



				echo "Packing Database Files" >> /tmp/eclassbackup
				
				#Check if the mysql_password encrypted
				if echo "$mysql_password" | egrep -q "=" ; then
                                #echo "It should be encrypted password"
                                keysalt=`$eclass_root_folder/admin/eclassupdate/manual_update/scripts/zendid | cut -d \: -f 2|head -1`
				mysql_password=`echo $mysql_password | openssl enc -aes-128-cbc -a -d -salt -pass pass:$keysalt`
				#echo $mysql_password >> /tmp/templog
				fi

				
				BACKUP_MYSQL $mysql_login $mysql_password
				#Verify BackupFile here
				BACKUP_SYSTEM_VERIFY
				BACKUP_MYSQL_VERIFY



                if [ "$backupBreakDown" == "true" ]; then
                        echo $File_For_Split
						echo "Splitting Files" >> /tmp/eclassbackup
                        SPLIT_FILE
                else
                        echo
                        echo "Backup process finished !!"

                fi
				
				
              echo "System backup $backupFileName created in $destination_Path"
				echo "Finalizing Backup Report" >> /tmp/eclassbackup
				CHECK_BACKUP_FILE_SIZE
				#reset destination folder premission to apache owned
				chown -R apache.apache $backupFileDestination

				POST_BACKUP_REPORT
				echo "Backup File Size: $TempBackupFileSize" >> /tmp/eclassbackup


				#Skip Error Messages
				echo "Backup Done" >> /tmp/eclassbackup
				#Backup File Rotation
				BACKUP_File_Rotation


				echo "Backup Done" >> /tmp/eclassbackup
				
     fi

}

#-----
# Main
#-----

#CHECK_PRIVILEGE

case $# in
  0|[2-9]) HELP;;
  1) OPTION_MENU $1;;
esac
