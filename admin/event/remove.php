<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".implode(",", $EventID).")";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".implode(",", $EventID).")";
$li->db_db_query($sql);

intranet_closedb();
header("Location: event.php?TabID=$TabID&filter=$filter&order=$order&field=$field&msg=3");
?>
