<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libevent.php");
include("../../includes/libgrouping.php");
include("../../includes/libfilesystem.php");
include("../../includes/libcycle.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();

$li = new libevent($EventID[0]);
$lo = new libgrouping();
#$lc = new libcycle();
#$is_cycle = ($lc->CycleID >= 1);

$TabID = $li->RecordType;
$RecordStatus0 = ($li->RecordStatus==0) ? "CHECKED" : "";
$RecordStatus1 = ($li->RecordStatus==1) ? "CHECKED" : "";

switch ($TabID){
     case 0: $TabID = 0; $TabTitle = $i_EventTypeSchool; break;
     case 1: $TabID = 1; $TabTitle = $i_EventTypeAcademic; break;
     case 2: $TabID = 2; $TabTitle = $i_EventTypeHoliday; break;
     case 3: $TabID = 3; $TabTitle = $i_EventTypeGroup; break;
     default: $TabID = 0; break;
}
?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.EventDate, "<?php echo $i_alert_pleasefillin.$i_EventDate; ?>.")) return false;
     if(!check_date(obj.EventDate, "<?php echo $i_invalid_date; ?>.")) return false;
     if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_EventTitle; ?>.")) return false;
     <?php if($TabID == 3){ ?>
     checkOptionAll(obj.elements["GroupID[]"]);
     if(obj.elements["GroupID[]"].length==0){ alert(globalAlertMsg15); return false; }
     <?php } ?>
}
</script>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_im_event, 'index.php', $i_EventTypeString[$TabID], 'javascript:history.back()', $button_edit, '') ?>
<?php
switch ($TabID)
{
        case 0: $imgTitle = "head_event_school_$intranet_session_language.gif"; break;
        case 1: $imgTitle = "head_event_teaching_$intranet_session_language.gif"; break;
        case 2: $imgTitle = "head_event_holiday_$intranet_session_language.gif"; break;
        case 3: $imgTitle = "head_event_group_$intranet_session_language.gif"; break;
}
echo displayTag($imgTitle, $msg);
?>

<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td width=100 align=right nowrap><?php echo $i_EventRecordType; ?>:</td><td><?php echo $TabTitle; ?></td></tr>
<tr><td width=100 align=right nowrap><?php echo $i_EventDate; ?>:</td><td><input class=text type=text name=EventDate size=10 maxlength=10 value="<?php echo $li->EventDate; ?>"> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td width=100 align=right nowrap><?php echo $i_EventTitle; ?>:</td><td><input class=text type=text name=Title size=60 maxlength=255 value="<?php echo $li->Title; ?>"></td></tr>
<tr><td width=100 align=right nowrap><?php echo $i_EventVenue; ?>:</td><td><input class=text type=text name=EventVenue size=60 maxlength=100 value="<?php echo $li->EventVenue; ?>"></td></tr>
<tr><td width=100 align=right nowrap><?php echo $i_EventNature; ?>:</td><td><input class=text type=text name=EventNature size=60 maxlength=100 value="<?php echo $li->EventNature; ?>"></td></tr>
<tr><td width=100 align=right nowrap><?php echo $i_EventDescription; ?>:</td><td><textarea name=Description cols=60 rows=10><?php echo $li->Description; ?></textarea></td></tr>
<?php
$skipChecked = ($li->isSkipCycle ==1? "CHECKED":"");
?>
<tr><td width=100 align=right nowrap><?php echo $i_EventSkipCycle; ?>:</td><td><INPUT type=checkbox NAME=EventSkip VALUE=1 <?=$skipChecked?>></td></tr>
<tr><td width=100 align=right nowrap><?php echo $i_EventRecordStatus; ?>:</td><td><input type=radio name=RecordStatus value=1 <?php echo $RecordStatus1; ?>> <?php echo $i_status_publish; ?> <input type=radio name=RecordStatus value=0 <?php echo $RecordStatus0; ?>> <?php echo $i_status_pending; ?></td></tr>
<?php if($TabID == 3){ ?>
<tr><td align="right" nowrap><?=$i_admintitle_group?>:</td>
<td><?php echo $lo->displayEventGroups($li->EventID); ?></td></tr>
<?php } ?>
</table>
</blockquote>
<input type=hidden name=EventID value="<?php echo $li->EventID; ?>">
<input type=hidden name=RecordType value=<?php echo $TabID; ?>>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>