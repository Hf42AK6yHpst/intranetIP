<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libattendance.php");
include_once("../../includes/libmerit.php");
include_once("../../includes/libservice.php");
include_once("../../includes/libactivity.php");
include_once("../../includes/libaward.php");
include_once("../../includes/libassessment.php");
include_once("../../includes/libstudentfiles.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");
// $li_menu from adminheader_intranet.php
intranet_opendb();

# Parameters of page
//Default value of number of line in each page
$defaultNumOfLine = 45;

//current number of line remaining
$lineRemain = $defaultNumOfLine;

//how many line need
$fieldLineNeed = 1;


// get school information (name + badge)
$li = new libfilesystem();
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$schoolbadge = ($imgfile!="") ? "<img src=/file/$imgfile width=120 height=60><br>\n" : "&nbsp;";

$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? $school_data[0] : "&nbsp;";

if ($schoolbadge!="&nbsp;" || $school_name!="&nbsp;")
{
        $school_info = "<table border=0 width=650>\n";
        $school_info .= "<tr><td align=center>$schoolbadge <font size=+0><b>$school_name</b></font></td></tr>\n";
        $school_info .= "</table>\n<br>\n";
        $lineRemain = $lineRemain-5;
}

#######################################
# Customization (Not display school badge)
if($sys_custom['student_profile_report_no_header'] == true)
{
	$school_info = "";
}
#######################################

$li_menu = new libaccount();
$li_menu->is_access_function($PHP_AUTH_USER);

$lstudentprofile = new libstudentprofile();

$lclass = new libclass();
$lu = new libuser($StudentID);

$toolbar = "$i_ClassName: $class <br>$i_PrinterFriendly_StudentName: ".$lu->UserNameClassNumber();
if ($StudentID != "")
{
	$now = time();
	$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
	$yearend = date('Y-m-d',getEndOfAcademicYear($now));
	
	$lattend = new libattendance();
	$lmerit = new libmerit();
	$lact = new libactivity();
	$lservice = new libservice();
	$laward = new libaward();
	$lassess = new libassessment();
	$lfiles = new libstudentfiles();
	
	$year_attend = $lattend->returnYears($StudentID);
	$year_merit = $lmerit->returnYears($StudentID);
	$year_act = $lact->returnYears($StudentID);
	$year_service = $lservice->returnYears($StudentID);
	$year_award = $laward->returnYears($StudentID);
	$year_assess = $lassess->returnYears($StudentID);
	$year_files = $lfiles->returnYears($StudentID);
	
	$currentYear = getCurrentAcademicYear();
	$year_array = array_merge($year_attend,$year_merit,$year_act,$year_service,$year_award,$year_access,$year_files);
	$year_array[] = $currentYear;
	$year_array = array_unique($year_array);
	rsort($year_array);
	
	$year_array = array_values($year_array);
	
	##### global variable called by : 
	#	- libattendance.php
	#	- libmerit.php
	$all_distinct_years = $year_array;
	
	//$year_display = $sys_custom['uccke_student_profile'] ? "" : ($year);
	$year_display = $year;
}

?>

<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE>

<?=$school_info?>

<table width=560 border=0 cellpadding=0 cellspacing=0>
<tr>
	<td>
		<? 
			if ($year_display != "") 
			{
				echo "$i_AwardYear: ".$year_display." $sem <br>";
				$lineRemain--;
			}
	
			echo $toolbar;
			$lineRemain = $lineRemain-2;
		?>
	</td>
</tr>
</table>

<?
if ($StudentID != "") 
{
?>
	<?
	if ($year == "")  
	{
		echo "<br>";
		$lineRemain--;
	
		echo "<span class=body>$i_Profile_ClassHistory</span>";
		$lineRemain--; 
			
		echo $lclass->displayClassHistoryAdmin($StudentID);
		$lineRemain = $lineRemain-2 ; 
	}
?>

<?
# Attendance Record
if ($li_menu->is_access_attendance && !$lstudentprofile->is_printpage_attendance_hidden) 
{ 
	echo "<br>";
	$lineRemain--;
	echo "<span class=body>$i_Profile_Attendance</span>";
	$lineRemain--;

	$x = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>";
	$title_array = array($i_Attendance_Year,$i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave);
	$x .= "<tr>";
	for ($i=0; $i<sizeof($title_array); $i++)
    {
         $x .= "<td class=tableTitle_new align=center>".$title_array[$i]."</td>";
    }
    $x .= "</tr>";
    $lineRemain--;
        
	$result = $lattend->returnStudentRecordByYear($StudentID,$year,$sem);
	if (sizeof($result)==0)
	{
		$x .= "<tr><td colspan=4 algin=center>$i_no_record_exists_msg2</td></tr>\n";
		$lineRemain--;
	}
	else
	{
		
        
        for ($i=0; $i<sizeof($result); $i++)
        {
             $x .= "<tr>\n";
             for ($j=0; $j<sizeof($result[$i]); $j++)
             {
                  $data = $result[$i][$j];
                  $x .= "<td align=center>$data</td>";
             }
             $x .= "</tr>\n";
             $lineRemain--;
        }
	}
	$x .= "</table>";
	echo $x;
}
?>

<?
# Merit/Demerit Record
if ($li_menu->is_access_merit && !$lstudentprofile->is_printpage_merit_hidden) 
{
	echo "<br>";
	$lineRemain--;
	echo "<span class=body>$i_Profile_Merit</span>";
	$lineRemain--;
	
	$result = $lmerit->returnStudentRecordByYear($StudentID,$year,$sem);
	$x = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>";
	
	$title_array = array($i_Profile_Year);
	$title_array = array_merge($title_array,$i_Merit_TypeArray);
	
	global $merit_col_not_display;
	$field_disabled_array = array(0,
								$lstudentprofile->is_merit_disabled,
								$lstudentprofile->is_min_merit_disabled,
                                $lstudentprofile->is_maj_merit_disabled, 
                                $lstudentprofile->is_sup_merit_disabled,
                                $lstudentprofile->is_ult_merit_disabled, 
                                $lstudentprofile->is_black_disabled,
                                $lstudentprofile->is_min_demer_disabled, 
                                $lstudentprofile->is_maj_demer_disabled,
                                $lstudentprofile->is_sup_demer_disabled, 
                                $lstudentprofile->is_ult_demer_disabled
                                );
	# Customarization for UCCKE to hide "homework" and "bad name" in print preview
	# 6: homework column
	# 7: badname column
	
	$field_disabled_array[6] = ($sys_custom['student_profile_report_print_no_homework'])?true:$field_disabled_array[6];
	$field_disabled_array[7] = ($sys_custom['student_profile_report_print_no_badname'])?true:$field_disabled_array[7];
							
    $x .= "<tr>\n";
    for ($i=0; $i<sizeof($title_array); $i++)
    {
         if ($merit_col_not_display[$i]==1) continue;
         if (!$field_disabled_array[$i])
         {
              $x .= "<td class=tableTitle_new align=center>".$title_array[$i]."</td>";
         }
    }
    $x .= "</tr>\n";
    $lineRemain = $lineRemain-2;
    
	if (sizeof($result)==0)
	{
		$x .= "<tr><td colspan=". sizeof($title_array) ." align=center>$i_no_record_exists_msg2</td></tr>\n";
		$lineRemain--;
	}
	else
	{
		for ($i=0; $i<sizeof($result); $i++)
        {
             $x .= "<tr>\n";
             for ($j=0; $j<sizeof($result[$i]); $j++)
             {
                  if ($merit_col_not_display[$j]==1) continue;
                  if (!$field_disabled_array[$j])
                  {
	                  $data = $result[$i][$j];
	                  $x .= "<td align=center>$data</td>";
                  }
             }
             $x .= "</tr>\n";
             $lineRemain--;
        }
	}
	$x .= "</table>";
	echo $x;
	
}
?>

<?
# Service Record
if ($li_menu->is_access_service && !$lstudentprofile->is_printpage_service_hidden) 
{
	echo "<br>";
	$lineRemain--;
	echo "<span class=body>$i_Profile_Service</span>";
	$lineRemain--;
	
	$TableOpen = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
			        <tr>
			          <td width=70 align=center class=tableTitle_new>$i_ServiceYear</td>
			          <td width=110 align=center class=tableTitle_new>$i_ServiceSemester</td>
			          <td width=90 align=center class=tableTitle_new>$i_ServiceDate</td>
			          <td width=110 align=center class=tableTitle_new>$i_ServiceName</td>
			          <td width=90 align=center class=tableTitle_new>$i_ServiceRole</td>
			          <td width=90 align=center class=tableTitle_new>$i_ServicePerformance</td>
			        </tr>\n";
    $lineRemain = $lineRemain-2;
			        
	$result = $lservice->getServiceByStudent($StudentID, $year,$sem);
	if (sizeof($result)==0)
	{
		$x = $TableOpen;
		$x .= "<tr><td colspan=6 align=center>$i_no_record_exists_msg2</td></tr>\n";
		$lineRemain--;
	}
	else
	{
		# set the field width
		$fieldwidth = array();
		$curLine = array();
		$fieldwidth = array(9, 10, 10, 15, 8, 18);
	
		$x = $TableOpen;
		for ($i=0; $i<sizeof($result); $i++)
		{
  			for($j=0;$j<sizeof($fieldwidth);$j++)
 				$curLine[$j] = (ceil((strlen($result[$i][$j]) / $fieldwidth[$j])));
			$thisRow = max($curLine);

			list($thisyear,$thissem,$sDate,$service,$role,$performance) = $result[$i];			
			if($thisRow > $lineRemain)
			{
				$x .= "</table>";
				$lineRemain = $defaultNumOfLine;
				$x .= "<P CLASS='breakhere'>&nbsp;";
				$x .= $TableOpen;
				$lineRemain = $lineRemain -2;
			}
 			
			$x .= "
				<tr>
				<td align=center>$thisyear</td>
				<td align=center>$thissem</td>
				<td align=center>$sDate</td>
				<td align=center>$service</td>
				<td align=center>$role&nbsp;</td>
				<td align=center>$performance&nbsp;</td>
				</tr>\n";
			$lineRemain = $lineRemain - $thisRow;
		}
	}
	
	$x .= "</table>";
	echo $x;
}
?>

<?
# Activity Record
if ($li_menu->is_access_activity && !$lstudentprofile->is_printpage_activity_hidden)
{
	if($lineRemain<=4)
	{
		echo "<P CLASS='breakhere'>";
		$lineRemain = $defaultNumOfLine;
	}
	else	
	{
		echo "<br>";
		$lineRemain--;
	}
	echo "<span class=body>$i_Profile_Activity</span>";
	$lineRemain--;
	
	$TableOpen = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_ServiceYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_ActivitySemester</td>
                      <td width=150 align=center class=tableTitle_new>$i_ActivityName</td>
                      <td width=110 align=center class=tableTitle_new>$i_ActivityRole</td>
                      <td width=120 align=center class=tableTitle_new>$i_ActivityPerformance</td>
                    </tr>\n";
	$result = $lact->getActivityByStudent($StudentID, $year, $sem);
	if (sizeof($result)==0)
	{
		$x = $TableOpen;
		$lineRemain = $lineRemain-2;
		$x .= "<tr><td colspan=5 align=center>$i_no_record_exists_msg2</td></tr>\n";
		$lineRemain--;
	}
	else
	{
		# set the field width
		$fieldwidth = array();
		$curLine = array();
		$fieldwidth = array(9, 10, 22, 10, 18);
		
		$x = $TableOpen;
		$lineRemain = $lineRemain-2;
		for ($i=0; $i<sizeof($result); $i++)
		{
			for($j=0;$j<sizeof($fieldwidth);$j++)
 				$curLine[$j] = (ceil((strlen($result[$i][$j]) / $fieldwidth[$j])));
			$thisRow = max($curLine);

			if($thisRow > $lineRemain)
			{
				$x .= "</table>";
				$lineRemain = $defaultNumOfLine;
				$x .= "<P CLASS='breakhere'>&nbsp;";
				$x .= $TableOpen;
				$lineRemain = $lineRemain -2;
			}
			
			list($thisyear,$thissem,$actName,$role,$performance) = $result[$i];
			$x .= "
				<tr>
				<td align=center>$thisyear</td>
				<td align=center>$thissem</td>
				<td align=center>$actName</td>
				<td align=center>$role&nbsp;</td>
				<td align=center>$performance&nbsp;</td>
				</tr>\n";
			$lineRemain = $lineRemain - $thisRow;
		}
	}
	
	$x .= "</table>";
	echo $x;
}	
?>

<?
# Award Record
if ($li_menu->is_access_award && !$lstudentprofile->is_printpage_award_hidden)
{
	if($lineRemain<=4)
	{
		echo "<P CLASS='breakhere'>";
		$lineRemain = $defaultNumOfLine;
	}
	else	
	{
		echo "<br>";
		$lineRemain--;
	}
	echo "<span class=body>$i_Profile_Award</span>";
	$lineRemain--;
	
	$TableOpen = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_AwardYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_AwardSemester</td>
                      <td width=90 align=center class=tableTitle_new>$i_AwardDate</td>
                      <td width=170 align=center class=tableTitle_new>$i_AwardName</td>
                      <td width=120 align=center class=tableTitle_new>$i_AwardRemark</td>
                    </tr>\n";
	          
	$result = $laward->getAwardByStudent($StudentID, $year,$sem);
	if (sizeof($result)==0)
	{
		$x = $TableOpen;
		$lineRemain = $lineRemain-2;     
		$x .= "<tr><td colspan=5 align=center>$i_no_record_exists_msg2</td></tr>\n";
		$lineRemain = $lineRemain-3;
	}
	else
	{
		# set the field width
		$fieldwidth = array();
		$curLine = array();
		$fieldwidth = array(9, 10, 10, 20, 15);
	
		$x = $TableOpen;
		$lineRemain = $lineRemain-2;     
		for ($i=0; $i<sizeof($result); $i++)
		{
			for($j=0;$j<sizeof($fieldwidth);$j++)
 				$curLine[$j] = (ceil((strlen($result[$i][$j]) / $fieldwidth[$j])));
			$thisRow = max($curLine);

			if($thisRow > $lineRemain)
			{
				$x .= "</table>";
				$lineRemain = $defaultNumOfLine;
				$x .= "<P CLASS='breakhere'>&nbsp;";
				$x .= $TableOpen;
				$lineRemain = $lineRemain -2;
			}
			
			list($thisyear,$thissem,$aDate,$award,$remark) = $result[$i];
	         $x .= "
	          <tr>
	                  <td align=center>$thisyear</td>
	                  <td align=center>$thissem</td>
	                  <td align=center>$aDate</td>
	                  <td align=center>$award</td>
	                  <td align=center>$remark&nbsp;</td>
	                </tr>\n";
			$lineRemain = $lineRemain - $thisRow;
		}
	}
	
	$x .= "</table>";
	echo $x;
}	
?>
<? } ?>	
<?
include_once("../../templates/filefooter.php");

?>
