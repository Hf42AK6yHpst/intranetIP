<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libstudentfiles.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

#$result = $lassessment->getClassAssessmentListByForm($FormID);
$lfiles = new libstudentfiles();
$result = $lfiles->getClassFilesList();
$x = "<table width=560 border=1 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5 cellpadding=2 cellspacing=0>
<tr>
<td width=70% class=tableTitle>$i_UserClassName</td>
<td width=30% class=tableTitle>$i_ReferenceFiles_Qty</td>
</tr>
";

$lvlArray = array();
$lvlFileCount = array();
for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$fileCount,$lvlID) = $result[$i];
     if (!in_array($lvlID,$lvlArray))
     {
          $lvlArray[] = $lvlID;
     }
     for ($j=0; $j<sizeof($lvlArray); $j++)
     {
          if ($lvlArray[$j]==$lvlID)
          {
              $lvlFileCount[$j] += $fileCount;
              break;
          }
     }
     $link = "<a class=functionlink href=classview.php?classid=$id>$name</a>";
     $css = ($i%2) ? "2" : "";

     $x .= "<tr>
             <td class=tableContent$css>$link</td>
             <td class=tableContent$css>$fileCount</td>
            </tr>\n";
}
$x .= "</table>\n";


$lvls = $lfiles->getLevelList();
$y = "<table width=560 border=1 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5 cellpadding=2 cellspacing=0>
<tr>
<td width=70% class=tableTitle>$i_UserClassLevel</td>
<td width=30% class=tableTitle>$i_ReferenceFiles_Qty</td>
</tr>
";
for ($i=0; $i<sizeof($lvlArray); $i++)
{
     $id = $lvlArray[$i];
     $name = $lvls[$id];
     $count = $lvlFileCount[$i];
     $css = ($i%2) ? "2" : "";

     $y .= "<tr>
             <td class=tableContent$css>$name</td>
             <td class=tableContent$css>$count</td>
            </tr>\n";
}
$y .= "</table>\n";


#$functionbar = "$i_Assessment_SelectForm: $selection";
/*
<p class=admin_head><?php echo $i_adminmenu_adm.displayArrow()."<a href=/admin/academic/>$i_adminmenu_adm_academic_record</a>".displayArrow().$i_Form_Assessment; ?></p>
*/
?>
<form name=form1 method=get>
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Files, '') ?>
<?= displayTag("head_referencefile_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
<tr><td class=tableContent>
<?=$x?>
</td></tr>
<tr><td><br><br></td></tr>
<tr><td class=tableContent>
<?=$y?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");

?>