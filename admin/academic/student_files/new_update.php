<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/form_class_manage.php");
intranet_opendb();

$li = new libuser($studentid);
$classname = $li->ClassName;
$classnumber = $li->ClassNumber;
$lf = new libfilesystem();

// $year = intranet_htmlspecialchars(trim($year));
$title = intranet_htmlspecialchars(trim($title));
$filetype = intranet_htmlspecialchars(trim($filetype));
// $semester = intranet_htmlspecialchars(trim($semester));
$description = intranet_htmlspecialchars(trim($description));
$filepath = $userfile;
$filename = $userfile_name;

$academic_year_term = new academic_year_term($YearTermID);
$year = $academic_year_term->YearNameEN;
$semester = $academic_year_term->YearTermNameEN;


# Copy file
$base_path = "$file_path/file/studentfiles/";
if (!is_dir($base_path)) $lf->folder_new($base_path);

$pathsuffix = session_id()."-".time();
$path = "$base_path$pathsuffix";
if (!is_dir($path)) $lf->folder_new($path);

if ($filepath == "none" || $filepath=="")
{}
else
{
    $lf->lfs_copy($filepath, "$path/$filename");
}
$filesize = ceil(filesize($filepath)/1024.0);

$fieldname = "UserID,Year,Semester,Title,Description,FileType,FileName,FileSize,Path,DateInput,DateModified,ClassName,ClassNumber";
$fieldvalue = "'$studentid','$year','$semester','$title','$description','$filetype','$filename','$filesize','$pathsuffix',now(),now(),'$classname','$classnumber'";
$sql = "INSERT INTO PROFILE_STUDENT_FILES ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);
intranet_closedb();
header("Location: studentview.php?studentid=$studentid&classid=$classid&msg=1");
?>