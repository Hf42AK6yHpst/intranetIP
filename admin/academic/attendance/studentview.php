<?php
// page used by : kenneth chung
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libattendance.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");
include_once("../../../includes/libcardstudentattend2.php");


intranet_opendb();

### allow input attendace record through student profile
$li = new libcardstudentattend2();
$disallow_input_attendace_record = $li->disallowInputInProfile();



if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$lu = new libuser($studentid);

$now = time();
//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));


if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}

# TABLE SQL
$keyword = trim($keyword);
if($field=="") $field = 0;
if($order=="") $order = 1;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     case 4: $field = 4; break;
     case 5: $field = 5; break;
     default: $field = 0; break;
}
if ($yrfilter!="")
{
    $conds = " AND Year = '$yrfilter'";
}
else
{
    $conds = "";
}

$sql = "SELECT
              DATE_FORMAT(a.AttendanceDate,'%Y-%m-%d'),
              a.Year,
              CASE a.RecordType
                   WHEN 1 THEN '$i_Profile_Absent'
                   WHEN 2 THEN '$i_Profile_Late'
                   WHEN 3 THEN '$i_Profile_EarlyLeave'
                   ELSE '-' END,
              CASE a.DayType
                   WHEN 1 THEN '$i_DayTypeWholeDay'
                   WHEN 2 THEN '$i_DayTypeAM'
                   WHEN 3 THEN '$i_DayTypePM'
                   ELSE '-' END,
              a.Reason,
              a.DateModified,
              CONCAT('<input type=checkbox name=StudentAttendanceID[] value=', a.StudentAttendanceID ,'>')
        FROM PROFILE_STUDENT_ATTENDANCE as a
        WHERE a.UserID = $studentid
              AND (a.AttendanceDate like '%$keyword%'
                   OR
                   a.Reason like '%$keyword%') $conds
              ";
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.AttendanceDate","a.Year","a.RecordType", "a.DayType","a.Reason","a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(0, $i_Attendance_Date)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(1, $i_Attendance_Year)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(2, $i_Attendance_Type)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(3, $i_Attendance_DayType)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(4, $i_Attendance_Reason)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(5, $i_Attendance_Modified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentAttendanceID[]")."</td>\n";

## allow input attendace record through student profile
if($disallow_input_attendace_record!=1){
	$toolbar = "<a class=iconLink href=\"javascript:checkPost(document.form1,'new.php')\">".newIcon()."$button_new</a>";
}

// TABLE FUNCTION BAR
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'StudentAttendanceID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

## allow input attendace record through student profile
if($disallow_input_attendace_record!=1){
	$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'StudentAttendanceID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
}

$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

$printAnalyze = "<a class=iconLink href='javascript:openAnalysisPage()'>".analysisIcon()."$i_Profile_Attendance_Analysis</a>\n";
$printAnalyze .= "<a class=iconLink href='javascript:openPrintPage()'>".printIcon()."$i_PrinterFriendlyPage</a>\n";
$name = $lu->UserNameLang();

$lattend = new libattendance();
$years = $lattend->returnYears($studentid);
$filterbar = "$button_select $i_Profile_Year: ".getSelectByValue($years,"name=yrfilter onChange='this.form.submit()'",$yrfilter,1);


?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function changeDateType(obj)
{
         switch (obj.datetype.value)
         {
                 case '0':
                      obj.start.value = '<?=$today?>';
                      obj.end.value = '<?=$today?>';
                      break;
                 case '1':
                      obj.start.value = '<?=$weekstart?>';
                      obj.end.value = '<?=$weekend?>';
                      break;
                 case '2':
                      obj.start.value = '<?=$monthstart?>';
                      obj.end.value = '<?=$monthend?>';
                      break;
                 case '3':
                      obj.start.value = '<?=$yearstart?>';
                      obj.end.value = '<?=$yearend?>';
                      break;
         }
         obj.submit();
}
function openAnalysisPage()
{
        newWindow("studentanalysis.php?studentid=<?=$studentid?>&keyword=<?=$keyword?>&yrfilter=<?=$yrfilter?>",4);
}
function openPrintPage()
{
        newWindow("studentprint.php?studentid=<?=$studentid?>&keyword=<?=$keyword?>&yrfilter=<?=$yrfilter?>",4);
}
</SCRIPT>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Attendance, 'index.php', $lu->ClassName, "classview.php?classid=$classid", $name, '') ?>
<?= displayTag("head_attendance_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $filterbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $printAnalyze, $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
<tr><td class=tableContent>
<?=$li->display()?>
</td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=studentid value="<?=$studentid?>">
<input type=hidden name=classid value="<?=$classid?>">
</form>
<?
include_once("../../../templates/adminfooter.php");
?>