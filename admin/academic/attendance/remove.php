<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

if (!is_array($StudentAttendanceID) || sizeof($StudentAttendanceID)==0)
{
    if ($page_from == "detail.php")
    {
        header("Location: detail.php?class=$class&yrfilter=$yrfilter&reason=$reason&datetype=$datetype&date_from=$date_from&date_to=$date_to&msg=3");
    }
    else
    {
        header("Location: studentview.php?classid=$classid&studentid=$studentid&msg=3");
    }
    exit();
}

$list = implode(",",$StudentAttendanceID);



# new codes, added on 2007-07-25 to support accumulative punishment ( Late )
if ($plugin['Discipline'])
{
		include_once("../../../includes/libdiscipline.php");
		$ldiscipline = new libdiscipline();
		
		#Get Current Year and Semester
		$semester = getCurrentSemester();
		$year = getCurrentAcademicYear(); 
		
		$conds2 = "";
		switch ($ldiscipline->late_upgrade_option)
		{
			case 1: 
				$conds2 = "AND Year = '$year' AND Semester = '$semester'";
				break;
			case 2:
				$conds2 = "AND Year = '$year'";
				break;
			default:
		}
		
		$sql = "SELECT DISTINCT UserID, RecordType,AttendanceDate FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID IN ($list) AND RecordType=2";
		$students = $li->returnArray($sql, 3);
		for($i=0;$i<sizeof($students);$i++){
			list($student_id,$attendance_type,$attendance_date) = $students[$i];
			if($attendance_type	!=2) continue;
			if($ldiscipline->isUseAccumulativeLateSetting($attendance_date)){
				
					$temp = $ldiscipline->returnAccumulativePeriodInfoByDate($attendance_date);
					list($period_id,$date_start,$date_end,$target_year,$target_semester) = $temp;
				
					$conds = "AND AttendanceDate>='$date_start' AND AttendanceDate<='$date_end' ";
					 $sql = "SELECT MIN(AttendanceDate) FROM PROFILE_STUDENT_ATTENDANCE WHERE UserID = '$student_id' AND StudentAttendanceID IN ($list) $conds";
					 $temp = $li->returnVector($sql);
					 if ($temp[0]!="")
					 {
						 $target_date = $temp[0];
						 $ldiscipline->resetAccumulativeUpgradeLateToDemerit($student_id,$target_date);
						 $ldiscipline->resetAccumulativeUpgradeLateToDetention($student_id,$target_date);
					 }
					
			}
			else{
				
				 $sql = "SELECT MIN(AttendanceDate) FROM PROFILE_STUDENT_ATTENDANCE WHERE UserID = '$student_id' AND StudentAttendanceID IN ($list) $conds";
				 $temp = $li->returnVector($sql);
				 if ($temp[0]!="")
				 {
					 $target_date = $temp[0];
					 $ldiscipline->resetUpgradeLateToDemerit($student_id,$target_date);
					 $ldiscipline->resetUpgradeLateToDetention($student_id,$target_date);
				 }
					
			}
			
		}
}
$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID IN ($list)";
$li->db_db_query($sql);


if ($plugin['Discipline'])
{
    for ($i=0; $i<sizeof($students); $i++)
    {
         list($student_id, $attendance_type,$attendance_date) = $students[$i];

		 if($attendance_type == 2)
		{
			if($ldiscipline->isUseAccumulativeLateSetting($attendance_date)){
				$ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id , $attendance_date);
				$ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id , $attendance_date);
			}else{
			 	$ldiscipline->calculateUpgradeLateToDemerit($student_id);
			 	$ldiscipline->calculateUpgradeLateToDetention($student_id);
			}
		}
    }
}

### end new codes

$sql= "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE ProfileRecordID IN ($list) ";
$li->db_db_query($sql);




/* old codes
if ($plugin['Discipline'])
{
	
	$ldiscipline = new libdiscipline();

	
			#Get Current Year and Semester
		$semester = getCurrentSemester();
		$year = getCurrentAcademicYear(); 
	
		$conds = "";
		switch ($ldiscipline->late_upgrade_option)
		{
			case 1: 
				$conds = "AND Year = '$year' AND Semester = '$semester'";
				break;
			case 2:
				$conds = "AND Year = '$year'";
				break;
			default:
		}
	
    $sql = "SELECT DISTINCT UserID, RecordType FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID IN ($list) $conds";
    $students = $li->returnArray($sql, 2);

    # Reset Attendance Record UpgradedID and remove the upgraded items
    for ($i=0; $i<sizeof($students); $i++)
    {
		list($student_id, $type) = $students[$i];

         if($type == 2)
		{
			 $sql = "SELECT MIN(AttendanceDate) FROM PROFILE_STUDENT_ATTENDANCE WHERE UserID = '$student_id' AND StudentAttendanceID IN ($list) $conds";
			 $temp = $li->returnVector($sql);
			 if ($temp[0]!="")
			 {
				 $target_date = $temp[0];
				 $ldiscipline->resetUpgradeLateToDemerit($student_id,$target_date);
				 $ldiscipline->resetUpgradeLateToDetention($student_id,$target_date);
			 }
		}
    }
}

$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID IN ($list)";
$li->db_db_query($sql);

if ($plugin['Discipline'])
{
    for ($i=0; $i<sizeof($students); $i++)
    {
         list($student_id, $type) = $students[$i];

		 if($type == 2)
		{
			 $ldiscipline->calculateUpgradeLateToDemerit($student_id);
			 $ldiscipline->calculateUpgradeLateToDetention($student_id);
		}
    }
}
*/


intranet_closedb();
if ($page_from == "detail.php")
{
    header("Location: detail.php?class=$class&yrfilter=$yrfilter&reason=$reason&datetype=$datetype&date_from=$date_from&date_to=$date_to&msg=3");
}
else
{
    header("Location: studentview.php?classid=$classid&studentid=$studentid&msg=3");
}
?>