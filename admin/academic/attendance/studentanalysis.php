<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libstudentprofile.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/fileheader.php");

intranet_opendb();

$lu = new libuser($studentid);
$lstudentprofile = new libstudentprofile();

$now = time();
//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));


if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}


$name = $lu->UserNameLang();

# get stats according to reasons
if ($yrfilter!="")
{
    $conds = " AND Year = '$yrfilter'";
}
else
{
    $conds = "";
}

//$count_sql = ($lstudentprofile->attendance_count_method == 1) ? "IF(DayType=1, COUNT(Reason), COUNT(Reason)*0.5)" : "COUNT(Reason) ";
$count_sql = ($lstudentprofile->attendance_count_method == 1) ? "IF(DayType=1 OR RecordType <> 1, COUNT(*), COUNT(*)*0.5)" : "COUNT(*) ";

$sql = "
SELECT
                Reason,
                RecordType,
                $count_sql
FROM PROFILE_STUDENT_ATTENDANCE
WHERE
                UserID = $studentid
                AND (AttendanceDate like '%$keyword%'
                   OR
                   Reason like '%$keyword%') $conds
GROUP BY
                Reason, RecordType, DayType
ORDER BY
                Reason, RecordType
";
$ldb = new libdb();
$row = $ldb->returnArray($sql, 3);

// build array for final table
$pos_now = 0;
$pre_reason = $row[0][0];
$dataArr[$pos_now][0] = $pre_reason;
$dataArr[$pos_now][1] = 0;
$dataArr[$pos_now][2] = 0;
$dataArr[$pos_now][3] = 0;
for ($i=0; $i<sizeof($row); $i++)
{
        if ($pre_reason!=$row[$i][0])
        {
                $pos_now++;
                $dataArr[$pos_now][0] = $row[$i][0];
                $dataArr[$pos_now][1] = 0;
                $dataArr[$pos_now][2] = 0;
                $dataArr[$pos_now][3] = 0;
                $pre_reason = $row[$i][0];
        }
        $dataArr[$pos_now][$row[$i][1]] += $row[$i][2];
}

function getTotal($colNo){
        global $dataArr;
        $total = 0;
        for ($i=0; $i<sizeof($dataArr); $i++)
                $total += $dataArr[$i][$colNo];
        return $total;
}

$sStats = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$sStats .= "<tr><td class=tableTitle width=1>#</td>\n";
$sStats .= "<td class=tableTitle width=40%>$i_Attendance_Reason</td>\n";
$sStats .= "<td class=tableTitle width=20%>$i_Profile_Absent</td>\n";
$sStats .= "<td class=tableTitle width=20%>$i_Profile_Late</td>\n";
$sStats .= "<td class=tableTitle width=20%>$i_Profile_EarlyLeave</td></tr>\n";
if (sizeof($row)==0)
{
        $sStats .= "<tr><td class=tableContent colspan=5 align=center>$i_no_record_exists_msg</td></tr>\n";
}
else
{
        for ($i=0; $i<sizeof($dataArr); $i++)
        {
                $dataArr[$i][0] = (trim($dataArr[$i][0])!="") ? $dataArr[$i][0] : "&nbsp;";
                $sStats .= "<tr><td class=tableContent>".($i+1)."</td><td class=tableContent>".$dataArr[$i][0]."</td><td class=tableContent>".$dataArr[$i][1]."</td><td class=tableContent>".$dataArr[$i][2]."</td><td class=tableContent>".$dataArr[$i][3]."</td></tr>\n";
        }
        $sStats .= "<tr><td colspan=2 align=right>$list_total:</td>
                <td class=tableContent>".getTotal(1)."</td>
                <td class=tableContent>".getTotal(2)."</td>
                <td class=tableContent>".getTotal(3)."</td>
                </tr>
                ";
}
$sStats .= "</table>\n";

$functionbar = (trim($keyword)!="") ? "&nbsp; $i_Attendance_Reason/$i_Attendance_Date: $keyword" : "";
$functionbar2 = ($yrfilter!="")? "&nbsp; $i_Profile_Year: $yrfilter" :"";
?>

<form>
<p><?=$i_Profile_Attendance_Analysis.displayArrow().$lu->ClassName.displayArrow().$name?>
<br><?=$functionbar?>
<br><?=$functionbar2?>
</p>
<?= $sStats ?>

<br>
<table border=0 width=550>
<tr><td align=center>
<a href="javascript:window.print()"><img src="/images/admin/button/s_btn_print_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
</form>

<?
include_once("../../../templates/filefooter.php");

?>