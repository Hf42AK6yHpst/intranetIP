<?
# using: yat
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$li = new libdb();
$lexport = new libexporttext();

$conds = "";
if ($yrfilter != "")
{
    $conds .= " AND a.Year = '$yrfilter'";
}
if ($class != "")
{
    $conds .= " AND b.ClassName = '$class'";
}
if ($RecordType != "")
{
    $conds .= " AND a.RecordType = '$RecordType'";
}
if ($reason != "")
{
    $conds .= " AND a.Reason = '$reason'";
}
if ($datetype != 0)
{
    $date_field = "";
    if ($datetype == 1)
    {
        $date_field = "a.AttendanceDate";
    }
    else if ($datetype == 2)
    {
         $date_field = "a.DateModified";
    }
    if ($date_field != "")
    {
        if ($date_from != "")
        {
            $conds .= " AND $date_field >= '$date_from'";
        }
        if ($date_to != "")
        {
            $conds .= " AND $date_field <= '$date_to 23:59:59'";
        }
    }
}

$studentnamefield = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT
              a.Year,a.Semester,
              DATE_FORMAT(a.AttendanceDate,'%Y-%m-%d'),
              b.ClassName, b.ClassNumber,
              $studentnamefield,
              CASE a.RecordType
                   WHEN 1 THEN 'A'
                   WHEN 2 THEN 'L'
                   WHEN 3 THEN 'E' END,
              CASE a.DayType
                   WHEN 1 THEN 'WD'
                   WHEN 2 THEN 'AM'
                   WHEN 3 THEN 'PM' END,
              a.Reason
        FROM PROFILE_STUDENT_ATTENDANCE as a
                 LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
        WHERE 	   b.RecordStatus IN (0,1,2) AND
        		   (
                   a.Reason like '%$keyword%'
                   OR b.EnglishName like '%$keyword%'
                   OR b.ChineseName like '%$keyword%'
                   OR b.UserLogin like '%$keyword%'
                   OR b.ClassNumber like '%$keyword%'
                   ) $conds
              ";
              
$file_format = array("Year","Semester","Date","Class","Class Number","Student Name","Type","Time Slot","Reason");

$result = $li->returnArray($sql);

// $x = "\"Year\",\"Semester\",\"Date\",\"Class\",\"Class Number\",\"Student Name\",\"Type\",\"Time Slot\",\"Reason\"\n";
/*
for ($i=0; $i<sizeof($result); $i++)
{
     $delim = "";
     for ($j=0; $j<sizeof($result[$i]); $j++)
     {
          $x .= "$delim\"".$result[$i][$j]."\"";
          $delim = ",";
     }
     $x .= "\n";
}
*/
// Output the file to user browser
$filename = "attendance_data.csv";
$export_content = $lexport->GET_EXPORT_TXT($result, $file_format);
$lexport->EXPORT_FILE($filename, $export_content);

/*
header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($x) );
header("Content-Disposition: attachment; filename=\"".$filename."\"");

echo $x;
*/
intranet_closedb();
?>
