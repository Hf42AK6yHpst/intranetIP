<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libattendance.php");
include_once("../../includes/libmerit.php");
include_once("../../includes/libservice.php");
include_once("../../includes/libactivity.php");
include_once("../../includes/libaward.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");
intranet_opendb();

#######################################
# Customization (Not display last 2 cols) (0 - Show, 1 - Hide)
#
# Column1 - Year, Column2 - Merit, Column3 - Min_Merit, Column4 - Maj_Merit, Column5 - Sup_Merit
# Column6 - Ult_Merit, Column7 - Demerit, Column8 - Min_Demerit, Column9 - Maj_Demerit, Column10 - Sup_Demerit
# Column11 - Ult_Demerit
//$merit_col_not_display = array(0,0,0,0,0,0,0,0,0,1,1);
#######################################

# Parameters of page
//Default value of number of line in each page
$defaultNumOfLine = 45;

//current number of line remaining
$lineRemain = $defaultNumOfLine;

//how many line need
$fieldLineNeed = 1;

# Page layout details
$page_breaker = "<P CLASS='breakhere'>";
// $page_header_linefeed = 6;
// $intermediate_pageheader = "<P CLASS='breakhere'>";
// $intermediate_linefeed = 5;
?>

<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE>

<?
// get school information (name + badge)
$li = new libfilesystem();
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$schoolbadge = ($imgfile!="") ? "<img src=/file/$imgfile width=120 height=60><br>\n" : "&nbsp;";

$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? $school_data[0] : "&nbsp;";

if ($schoolbadge!="&nbsp;" || $school_name!="&nbsp;")
{
	$school_info = "<table border=0 width=650 height=60>\n";
	$school_info .= "<tr><td align=center>$schoolbadge <font size=+0><b>$school_name</b></font></td></tr>\n";
	$school_info .= "</table>\n<br>\n";
}

#######################################
# Customization (Not display school badge)

if($sys_custom['student_profile_report_no_header'] == true)
{
        $school_info = "";
}
#######################################

$li_menu = new libaccount();
$li_menu->is_access_function($PHP_AUTH_USER);

$lstudentprofile = new libstudentprofile();

# settings controling show/hide columns in merit/demerit table
$field_disabled_array = array(0,$lstudentprofile->is_merit_disabled,$lstudentprofile->is_min_merit_disabled,
        $lstudentprofile->is_maj_merit_disabled, $lstudentprofile->is_sup_merit_disabled,
        $lstudentprofile->is_ult_merit_disabled, $lstudentprofile->is_black_disabled,
        $lstudentprofile->is_min_demer_disabled, $lstudentprofile->is_maj_demer_disabled,
        $lstudentprofile->is_sup_demer_disabled, $lstudentprofile->is_ult_demer_disabled);

# Customarization for UCCKE to hide "homework" and "bad name" in print preview
# 6: homework column
# 7: badname column
$field_disabled_array[6] = $sys_custom['student_profile_report_print_no_homework']?true:$field_disabled_array[6];
$field_disabled_array[7] = $sys_custom['student_profile_report_print_no_badname']?true:$field_disabled_array[7];

# Current Academic Year or All
$year_display = ($generalYear==0?getCurrentAcademicYear():"");

$data = array();

# Grep all students information
$lclass = new libclass();
if ($left == 1)
{
    $student_conds = "AND RecordStatus = 3";
}
else
{
    $student_conds = "AND RecordStatus != 3";
}
$name_field = getNameFieldByLang();
if ($class != "")
{
    $sql = "SELECT UserID, $name_field, ClassNumber, ClassName FROM INTRANET_USER WHERE RecordType = 2 $student_conds AND ClassName = '$class' ORDER BY ClassNumber";
    $students = $lclass->returnArray($sql,4);
}
else
{
    $sql = "SELECT UserID, $name_field, ClassNumber, ClassName FROM INTRANET_USER WHERE RecordType = 2 $student_conds ORDER BY ClassName, ClassNumber";
    $students = $lclass->returnArray($sql,4);
}
    $lattend = new libattendance();
    $lmerit = new libmerit();
    $lact = new libactivity();
    $lservice = new libservice();
    $laward = new libaward();

    for ($iter=0; $iter<sizeof($students); $iter++)
    {
         list ($studentID, $studentName, $studentClassnumber, $studentClassName) = $students[$iter];
         if ($class != "") $studentClassName = $class;
         # Attendance data
         if ($li_menu->is_access_attendance && !$lstudentprofile->is_printpage_attendance_hidden)
         {
             $dataAttendance = $lattend->returnStudentRecordByYear($studentID,$year_display,$generalSem);
         }
         # Merit data
         if ($li_menu->is_access_merit && !$lstudentprofile->is_printpage_merit_hidden)
         {
             $dataMerit = $lmerit->returnStudentRecordByYear($studentID,$year_display,$generalSem);
         }
         # Service Data
         if ($li_menu->is_access_service && !$lstudentprofile->is_printpage_service_hidden)
         {
             $dataService = $lservice->getServiceByStudent($studentID,$year_display,$generalSem);
         }
         # Activity Data
         if ($li_menu->is_access_activity && !$lstudentprofile->is_printpage_activity_hidden)
         {
             $dataActivity = $lact->getActivityByStudent($studentID,$year_display,$generalSem);
         }
         # Award Data
         if ($li_menu->is_access_award && !$lstudentprofile->is_printpage_award_hidden)
         {
             $dataAward = $laward->getAwardByStudent($studentID,$year_display,$generalSem);
         }



		######## Start the report printint
         echo "$school_info\n";
         $lineRemain = $lineRemain-5;
         echo "<table width=560 border=0 cellpadding=0 cellspacing=0>\n";
         echo "<tr><td>\n";
         
         if ($year_display != "")
         {
             echo "$i_AwardYear: $year_display $generalSem<br>\n";
             $lineRemain--;
         }
         echo "$i_ClassName: $studentClassName<br>\n$i_PrinterFriendly_StudentName: $studentName ($studentClassName - $studentClassnumber)\n";
         $lineRemain -= 2;
         
         echo "</td></tr></table>\n";
        
         # Class History
		$x = "";
        //if ($year == "")
        if($year_display == "")
        {
	        echo "<br>";
	        $lineRemain--;
            echo "<span class=body>$i_Profile_ClassHistory</span>";
            $lineRemain--; 
            echo $lclass->displayClassHistoryAdmin($studentID);
            $lineRemain -= 2;
        }
        
        # Attendance
        if ($li_menu->is_access_attendance && !$lstudentprofile->is_printpage_attendance_hidden)
        {
			echo "<br>";
			$lineRemain--;
			echo "<span class=body>$i_Profile_Attendance</span>";
			$lineRemain--;
	        
			$x = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>";
			$title_array = array($i_Attendance_Year,$i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave);
			$x .= "<tr>";
			for ($i=0; $i<sizeof($title_array); $i++)
		    {
		         $x .= "<td class=tableTitle_new align=center>".$title_array[$i]."</td>";
		    }
		    $x .= "</tr>";
		    $lineRemain--;
		    
		    if (sizeof($dataAttendance)==0)
            {
				$x .= "<tr><td colspan=4 align=center>$i_no_record_exists_msg2</td></tr>\n";
				$lineRemain--;
            }
            else
            {
	            for ($i=0; $i<sizeof($dataAttendance); $i++)
                {
	                 $x .= "<tr>\n";
	                 for ($j=0; $j<sizeof($dataAttendance[$i]); $j++)
	                 {
                          $data = $dataAttendance[$i][$j];
                          $x .= "<td align=center>$data</td>";
	                 }
	                 $x .= "</tr>\n";
	                 $lineRemain--;
                }
            }
			$x .= "</table>";
			echo $x;
        }
        
        # Merit
        if ($li_menu->is_access_merit && !$lstudentprofile->is_printpage_merit_hidden)
        {
	        echo "<br>";
			$lineRemain--;
			echo "<span class=body>$i_Profile_Merit</span>";
			$lineRemain--;
			
	        $x = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
	        $title_array = array($i_Profile_Year);
	        $title_array = array_merge($title_array,$i_Merit_TypeArray);
	        
	        $x .= "<tr>\n";
	        for ($i=0; $i<sizeof($title_array); $i++)
	        {
		        if ($merit_col_not_display[$i]==1) continue;
	            if (!$field_disabled_array[$i])
	            {
	                 $x .= "<td class=tableTitle_new align=center>".$title_array[$i]."</td>";
	            }
	        }
	        $x .= "</tr>\n";
	        $lineRemain = $lineRemain-2;
	        
	        if (sizeof($dataMerit)==0)
	        {
	                $x .= "<tr><td colspan=". sizeof($title_array) ." align=center>$i_no_record_exists_msg2</td></tr>\n";
					$lineRemain--;
	        }
	        else
	        {
		        for ($i=0; $i<sizeof($dataMerit); $i++)
		        {
		             $x .= "<tr>\n";
		             for ($j=0; $j<sizeof($dataMerit[$i]); $j++)
		             {
		                  if ($merit_col_not_display[$j]==1) continue;
		                  if (!$field_disabled_array[$j])
		                  {
		                       $data = $dataMerit[$i][$j];
		                       $x .= "<td align=center>$data</td>";
		                  }
		             }
		             $x .= "</tr>\n";
		             $lineRemain--;
		        }
	        }
	        $x .= "</table>\n";
	        echo $x;
        }
        
        # Service
        if ($li_menu->is_access_service && !$lstudentprofile->is_printpage_service_hidden)
        {
			echo "<br>";
			$lineRemain--;
			echo "<span class=body>$i_Profile_Service</span>";
			$lineRemain--;
	        	
			$TableOpen = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
		                    <tr>
		                      <td width=70 align=center class=tableTitle_new>$i_ServiceYear</td>
		                      <td width=110 align=center class=tableTitle_new>$i_ServiceSemester</td>
		                      <td width=90 align=center class=tableTitle_new>$i_ServiceDate</td>
		                      <td width=110 align=center class=tableTitle_new>$i_ServiceName</td>
		                      <td width=90 align=center class=tableTitle_new>$i_ServiceRole</td>
		                      <td width=90 align=center class=tableTitle_new>$i_ServicePerformance</td>
		                    </tr>\n";
			$lineRemain = $lineRemain-2;
			
			if (sizeof($dataService)==0)
            {
	            $x = $TableOpen;
                $x .= "<tr><td colspan=6 align=center>$i_no_record_exists_msg2</td></tr>\n";
				$lineRemain--;
            }
            else
            {
	            # set the field width
				$fieldwidth = array();
				$curLine = array();
				$fieldwidth = array(9, 10, 10, 15, 8, 18);
			            
				$x = $TableOpen;
	            for ($i=0; $i<sizeof($dataService); $i++)
                {
	                for($j=0;$j<sizeof($fieldwidth);$j++)
		 				$curLine[$j] = (ceil((strlen($dataService[$i][$j]) / $fieldwidth[$j])));
					$thisRow = max($curLine);
					
					if($thisRow > $lineRemain)
					{
						$x .= "</table>";
						$lineRemain = $defaultNumOfLine;
						$x .= "<P CLASS='breakhere'>&nbsp;";
						$x .= $TableOpen;
						$lineRemain = $lineRemain -2;
					}
			
                     list($year,$sem,$sDate,$service,$role,$performance) = $dataService[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$sDate</td>
                              <td align=center>$service</td>
                              <td align=center>$role&nbsp;</td>
                              <td align=center>$performance&nbsp;</td>
                            </tr>\n";
					$lineRemain = $lineRemain - $thisRow;
                }
            }
			$x .= "</table>";
			echo $x;
        }
        
        # Activity
        if ($li_menu->is_access_activity && !$lstudentprofile->is_printpage_activity_hidden)
        {
	        if($lineRemain<=4)
			{
				echo "<P CLASS='breakhere'>";
				$lineRemain = $defaultNumOfLine;
			}
			else	
			{
				echo "<br>";
				$lineRemain--;
			}
			echo "<span class=body>$i_Profile_Activity</span>";
			$lineRemain--;
	        
	        $TableOpen = "
	                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
	                    <tr>
	                      <td width=70 align=center class=tableTitle_new>$i_ServiceYear</td>
	                      <td width=110 align=center class=tableTitle_new>$i_ActivitySemester</td>
	                      <td width=150 align=center class=tableTitle_new>$i_ActivityName</td>
	                      <td width=110 align=center class=tableTitle_new>$i_ActivityRole</td>
	                      <td width=120 align=center class=tableTitle_new>$i_ActivityPerformance</td>
	                    </tr>\n";
			
			if (sizeof($dataActivity)==0)
            {
	            $x = $TableOpen;
				$lineRemain = $lineRemain-2;
				$x .= "<tr><td colspan=5 align=center>$i_no_record_exists_msg2</td></tr>\n";
				$lineRemain--;
            }
            else
            {
	            # set the field width
				$fieldwidth = array();
				$curLine = array();
				$fieldwidth = array(9, 10, 22, 10, 18);
				
				$x = $TableOpen;
				$lineRemain = $lineRemain-2;
	            
	            for ($i=0; $i<sizeof($dataActivity); $i++)
                {
	                for($j=0;$j<sizeof($fieldwidth);$j++)
		 				$curLine[$j] = (ceil((strlen($dataActivity[$i][$j]) / $fieldwidth[$j])));
					$thisRow = max($curLine);
		
					if($thisRow > $lineRemain)
					{
						$x .= "</table>";
						$lineRemain = $defaultNumOfLine;
						$x .= "<P CLASS='breakhere'>&nbsp;";
						$x .= $TableOpen;
						$lineRemain = $lineRemain -2;
					}
			
                     list($year,$sem,$actName,$role,$performance) = $dataActivity[$i];
                     $x .= "
                      	<tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$actName</td>
                              <td align=center>$role&nbsp;</td>
                              <td align=center>$performance&nbsp;</td>
                            </tr>\n";
					$lineRemain = $lineRemain - $thisRow;
                }
            }
            $x .= "</table>";
			echo $x;
        }
        
        # Award
        if ($li_menu->is_access_award && !$lstudentprofile->is_printpage_award_hidden)
        {
	        if($lineRemain<=4)
			{
				echo "<P CLASS='breakhere'>";
				$lineRemain = $defaultNumOfLine;
			}
			else	
			{
				echo "<br>";
				$lineRemain--;
			}
			echo "<span class=body>$i_Profile_Award</span>";
			$lineRemain--;
	        
	        $TableOpen = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
		                    <tr>
		                      <td width=70 align=center class=tableTitle_new>$i_AwardYear</td>
		                      <td width=110 align=center class=tableTitle_new>$i_AwardSemester</td>
		                      <td width=90 align=center class=tableTitle_new>$i_AwardDate</td>
		                      <td width=170 align=center class=tableTitle_new>$i_AwardName</td>
		                      <td width=120 align=center class=tableTitle_new>$i_AwardRemark</td>
		                    </tr>\n";

			if (sizeof($dataAward)==0)
            {
	            $x = $TableOpen;
				$lineRemain = $lineRemain-2;     
				$x .= "<tr><td colspan=5 align=center>$i_no_record_exists_msg2</td></tr>\n";
				$lineRemain = $lineRemain-3;
            }
            else
            {
	            # set the field width
				$fieldwidth = array();
				$curLine = array();
				$fieldwidth = array(9, 10, 10, 20, 15);
			
				$x = $TableOpen;
				$lineRemain = $lineRemain-2;     
				
				for ($i=0; $i<sizeof($dataAward); $i++)
				{
					for($j=0;$j<sizeof($fieldwidth);$j++)
		 				$curLine[$j] = (ceil((strlen($dataAward[$i][$j]) / $fieldwidth[$j])));
					$thisRow = max($curLine);
		
					if($thisRow > $lineRemain)
					{
						$x .= "</table>";
						$lineRemain = $defaultNumOfLine;
						$x .= "<P CLASS='breakhere'>&nbsp;";
						$x .= $TableOpen;
						$lineRemain = $lineRemain -2;
					}
			
					list($year,$sem,$aDate,$award,$remark) = $dataAward[$i];
					$x .= "
						<tr>
						<td align=center>$year</td>
						<td align=center>$sem</td>
						<td align=center>$aDate</td>
						<td align=center>$award</td>
						<td align=center>$remark&nbsp;</td>
						</tr>\n";
					$lineRemain = $lineRemain - $thisRow;
				}
            }
            $x .= "</table>";
			echo $x;
        }

        if ($iter < sizeof($students)-1)
        {
            echo "$page_breaker";
            $lineRemain = $defaultNumOfLine;
        }
            
    }

intranet_closedb();

include_once("../../templates/filefooter.php");

?>
