<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
intranet_opendb();

$li = new libuser($accessby);
$lstudent = new libuser($studentid);
$classname = $lstudent->ClassName;
$classnumber = $lstudent->ClassNumber;

if ($accessby != "" && $accessby != 0)
{
    $username = $li->getNameForRecord();
}
else $username = "";

//$year = intranet_htmlspecialchars(trim($year));
list($AcademicYearID, $year, $YearTermID, $semester) = getAcademicYearInfoAndTermInfoByDate($assessmentDate);

$fieldname = "UserID, Year, Semester, AssessmentDate, AssessBy, AssessByName, FormID, AnsString, RecordStatus, DateInput,DateModified,ClassName,ClassNumber";
$fieldvalue = "$studentid,'$year','$semester','$assessmentDate','$accessby','$username',$formid,'$aStr',1,now(),now(),'$classname','$classnumber'";
$sql = "INSERT INTO PROFILE_STUDENT_ASSESSMENT ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);
intranet_closedb();
header("Location: studentview.php?studentid=$studentid&msg=1&classid=$classid");
?>