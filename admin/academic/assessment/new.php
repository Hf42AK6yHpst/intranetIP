<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libform.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();
$lu = new libuser($studentid);
$lform = new libform();
$forms = $lform->returnAssessmentForm();
$x = "<tr><td width=50% class=tableTitle_new><u>$i_Form_Name</u></td><td width=50% class=tableTitle_new><u>$i_Form_Description</u></td></tr>\n";
for ($i=0; $i<sizeof($forms); $i++)
{
     list ($id, $name, $desp) = $forms[$i];
     $link = "<a href=new_form.php?formid=$id&studentid=$studentid&classid=$classid>$name</a>";
     $x .= "<tr><td>$link</td><td>$desp</td></tr>\n";
}
$name = $lu->UserNameLang();
?>

<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Assessment, 'index.php', $lu->ClassName, "classview.php?classid=$classid", $name, 'javascript:history.back()', $i_Assessment_SelectForm, '') ?>
<?= displayTag("head_assessment_$intranet_session_language.gif", $msg) ?>


<blockquote>
<p><?=$i_Assessment_PleaseSelectAForm?>:</p>
<table width=500 border=1 bordercolor='#F7F7F9' cellspacing=2 cellpadding=1>
<?=$x?>
</table>
</blockquote>

<?php
include_once("../../../templates/adminfooter.php");
?>