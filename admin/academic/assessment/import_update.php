<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$file_format = array("Date","Class","Class Number","Type","Time Slot","Reason");

$li = new libdb();

$limport = new libimporttext();

$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php");
} else {
        $ext = strtoupper($lo->file_ext($filename));
        if($limport->CHECK_FILE_EXT($filename))
        {
           # read file into array
           # return 0 if fail, return csv array if success
           //$data = $lo->file_read_csv($filepath);
           $data = $limport->GET_IMPORT_TXT($filepath);
           $col_name = array_shift($data);                   # drop the title bar

           #Check file format
           $format_wrong = false;
           for ($i=0; $i<sizeof($file_format); $i++)
           {
                if ($col_name[$i]!=$file_format[$i])
                {
                    $format_wrong = true;
                    break;
                }
           }
           if ($format_wrong)
           {
               header ("Location: import.php?error=1");
               exit();
           }

           $t_delimiter = "";
           $toTempValues = "";
           $today = date('Y-m-d');
           for ($i=0; $i<sizeof($data); $i++)
           {
                list($attenddate,$class,$classnum,$type,$slot,$reason) = $data[$i];
                $attenddate = (($attenddate == "")? $today: $attenddate);
                $class = addslashes($class);
                $classnum = addslashes($classnum);
                $reason = addslashes($reason);
                $toTempValues .= "$t_delimiter ('$attenddate','$class','$classnum','$type','$slot','$reason')";
                $t_delimiter = ",";
           }

           # Create temp table
           $sql = "CREATE TEMPORARY TABLE TEMP_ATTENDANCE_IMPORT (
                          AttendanceDate datetime,
                          ClassName varchar(255),
                          ClassNumber varchar(255),
                          RecordType char(2),
                          Slot char(2),
                          Reason text
                  ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
           $li->db_db_query($sql);
           $fields_name = "AttendanceDate, ClassName, ClassNumber, RecordType, Slot,Reason";
           $sql = "INSERT IGNORE INTO TEMP_ATTENDANCE_IMPORT ($fields_name) VALUES $toTempValues";
           $li->db_db_query($sql);

           $fields_name = "UserID, AttendanceDate, RecordType, DayType, Reason, DateInput, DateModified";
           $sql = "INSERT IGNORE INTO PROFILE_STUDENT_ATTENDANCE ($fields_name)
                          SELECT a.UserID, b.AttendanceDate,
                                 CASE b.RecordType
                                      WHEN 'A' THEN 1
                                      WHEN 'L' THEN 2
                                      WHEN 'E' THEN 3 END,
                                 CASE b.Slot
                                      WHEN 'WD' THEN 1
                                      WHEN 'AM' THEN 2
                                      WHEN 'PM' THEN 3 END,
                                 b.Reason,now(),now()
                                 FROM INTRANET_USER as a, TEMP_ATTENDANCE_IMPORT as b
                                 WHERE a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber
                                 ";
           $li->db_db_query($sql);
           header("Location: index.php?msg=1");
        }
}

intranet_closedb();
?>