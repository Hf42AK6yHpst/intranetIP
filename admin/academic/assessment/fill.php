<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libform.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/fileheader.php");

intranet_opendb();
$lform = new libform($formid);
#$queString = $lform->queString;
#$queString = str_replace('"','&quot;',$queString);
$queString = $lform->getConvertedQuestion();
?>
<script language="javascript" src="/templates/forms/layer.js"></script>
<script language="javascript" src="/templates/forms/form_edit.js"></script>
<form name="ansForm" method="post" action="update.php">
        <input type=hidden name="qStr" value="">
        <input type=hidden name="aStr" value="">
</form>

<p style="padding-left:20px">
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
<tr><td style="background-image: url(../../../images/admin/pop_bg.gif);" align=center>

<table width=400 border=0 cellpadding=10 cellspacing=0>
<tr><td>

<script language="Javascript">
<?=$lform->getWordsInJS()?>

background_image = "/images/layer_bg.gif";

var sheet= new Answersheet();
// attention: MUST replace '"' to '&quot;'
sheet.qString="<?=$queString?>";
//edit submitted application
sheet.aString=window.opener.document.form1.aStr.value;
sheet.mode=1;
sheet.answer=sheet.sheetArr();
//sheet.templates=form_templates;
document.write(editPanel());
</script>
<SCRIPT LANGUAGE=javascript>
function copyback()
{
         finish();
         window.opener.document.form1.aStr.value = document.ansForm.aStr.value;
         self.close();
}
</SCRIPT>
</td></tr>
</table>
</td></tr>
<tr><td><img src="../../../images/admin/pop_bottom.gif" width=422 height=18 border=0></td></tr>
<tr><td align=center height="40" style="vertical-align:bottom">
<a href="javascript:copyback()"><img alt='<?=$button_save?>' src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
<?php
include_once("../../../templates/filefooter.php");
?>