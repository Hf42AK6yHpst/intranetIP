<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libform.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();
$lu = new libuser($studentid);
$lform = new libform($formid);
$formname = $lform->FormName;
$semesterSelect = getSelectSemester("name=semester");
$teacherSelect = $lu->getSelectTeacherStaff("name=accessby");
$name = $lu->UserNameLang();
?>
<script language="javascript">
function checkform(obj){
         if (obj.assessmentDate.value != "")
         {
             if(!check_date(obj.assessmentDate, "<?php echo $i_invalid_date; ?>.")) return false;
         }
         if(!check_text(obj.year, "<?php echo $i_alert_pleasefillin.$i_AssesementYear; ?>.")) return false;
         return true;
}
function openform()
{
         newWindow("fill.php?formid=<?=$formid?>",1);
}
</script>

<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Assessment, 'index.php', $lu->ClassName, "classview.php?classid=$classid", $name, "studentview.php?studentid=$studentid&classid=$classid", $i_Assessment_SelectForm, 'javascript:history.back()', $i_Assessment_FillForm, '') ?>
<?= displayTag("head_assessment_$intranet_session_language.gif", $msg) ?>

<blockquote>
<?=$i_Assessment_PleaseFillAForm?>:<br>
<br>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right><?php echo $i_UserLogin; ?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?php echo $i_UserEnglishName; ?>:</td><td><?=$lu->EnglishName?></td></tr>
<tr><td align=right><?php echo $i_UserChineseName; ?>:</td><td><?=$lu->ChineseName?></td></tr>
<tr><td align=right><?php echo $i_UserClassName; ?>:</td><td><?=$lu->ClassName?></td></tr>
<tr><td align=right><?php echo $i_UserClassNumber; ?>:</td><td><?=$lu->ClassNumber?></td></tr>
<!--
<tr><td align=right><?php echo $i_Assessment_Year; ?>:</td><td><input type=text name=year value='<?=getCurrentAcademicYear()?>'></td></tr>
<tr><td align=right><?php echo $i_Assessment_Semester; ?>:</td><td><?=$semesterSelect?></td></tr>
//-->
<tr><td align=right><?php echo $i_Assessment_Date; ?>:</td><td><input type=text size=10 maxlength=10 name=assessmentDate value='<?=date('Y-m-d')?>'> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td align=right><?php echo $i_Assessment_By; ?>:</td><td><?=$teacherSelect?> (<?=$i_Assessment_UserSelectAdmin?>)</td></tr>
<tr><td align=right>&nbsp;</td><td>
<a href="javascript:openform()"><img src="/images/admin/button/s_btn_show_form_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
</blockquote>
<input type=hidden name="formid" value="<?=$formid?>">
<input type=hidden name="studentid" value="<?=$studentid?>">
<input type=hidden name="qStr" value="">
<input type=hidden name="aStr" value="">
<input type=hidden name=classid value=<?=$classid?>>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>
<?php
include_once("../../../templates/adminfooter.php");
?>