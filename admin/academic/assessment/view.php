<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libassessment.php");
include_once("../../../includes/libform.php");
include_once("../../../includes/libuser.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/fileheader.php");

intranet_opendb();
if ($StudentType=='Archive')
{
    $lu = new libuser();
    $sql = "SELECT EnglishName , ChineseName,UserLogin FROM INTRANET_ARCHIVE_USER WHERE UserID = '$StudentID'";
    $temp = $lu->returnArray($sql,3);
    list($engName, $chiName, $login ) = $temp[0];

    $sql = "SELECT ClassName, ClassNumber, RecordDate, Year, Semester, AssessEngName, AssessChiName, FormContent, FormName FROM PROFILE_ARCHIVE_ASSESSMENT WHERE RecordID = '$RecordID'";
    $form_info = $lu->returnArray($sql,9);
    list($StudentClassName,$StudentClassNumber,$AssessmentDate, $year, $semester, $assessEng, $assessChi, $formContent, $formName) = $form_info[0];
    $StudentName = "$engName ($chiName)";
    $StudentLogin = $login;
    if ($assessEng == "" && $accessChi == "")
    {
        $username = "$i_general_sysadmin";
    }
    else $username = "$assessEng ($assessChi)";
}
else
{
$lassessment = new libassessment($AssessmentID);
$lform = new libform($lassessment->formID);
$queString = $lform->queString;
$queString = $lform->getConvertedString($queString);
$ansString = $lassessment->ansStr;
$ansString = $lform->getConvertedString($ansString);

# Grab form information
$formName = $lform->FormName;
$year = $lassessment->year;
$semester = $lassessment->semester;
$AssessmentDate = $lassessment->AssessmentDate;

# Student information
$lu = new libuser($lassessment->UserID);
$StudentName = $lu->UserNameLang();
$StudentClassName = $lu->ClassName;
$StudentClassNumber = $lu->ClassNumber;
$StudentLogin = $lu->UserLogin;

# Filler information
# Check assess by in system or not
if ($lassessment->assessByUserID != "" && $lassessment->accessByName != "")
{
    $sql = "SELECT UserID FROM INTRANET_USER WHERE UserID = '".$lassessment->assessByUserID."'";
    $result = $lu->returnVector($sql);
    if ($result[0]!=$lassessment->assessByUserID)
    {
        $username = "<br>($i_Assessment_OriginalAssessBy: ".$lassessment->assessByName.")";
    }
    else $username = "";
}
else $username = "";

if ($username == "" && $lassessment->assessByUserID != "")
{
    $lu = new libuser($lassessment->assessByUserID);
    $TeacherName = $lu->UserNameLang();
    $TeacherLogin = $lu->UserLogin;
    $username = "$TeacherName ($i_UserLogin: $TeacherLogin)";
}

}
?>
<script language="javascript" src="/templates/forms/layer.js"></script>
<script language="javascript" src="/templates/forms/form_view.js"></script>
<form name="ansForm" method="post" action="update.php">
        <input type=hidden name="qStr" value="">
        <input type=hidden name="aStr" value="">
</form>
<?= displayNavTitle($formName, '') ?>

<p style="padding-left:20px">
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
<tr><td style="background-image: url(../../../images/admin/pop_bg.gif);" align=center>

<table width=400 border=0 cellpadding=10 cellspacing=0>
<tr><td>
<?="$i_PrinterFriendly_StudentName: $StudentName"?><br>
<?="$i_UserClassName: $StudentClassName"?><br>
<?="$i_UserClassNumber: $StudentClassNumber"?><br>
<?="$i_UserLogin: $StudentLogin"?><br>
</p>
<p><?="$i_Assessment_By: $username"?>
</p>
</td></tr>
</table>

<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_bar.gif" width=422 height=16 border=0></td></tr>
</table>
<table width=400 border=0 cellpadding=10 cellspacing=0>
<tr><td>
<?
if ($StudentType=='Archive')
{
    echo nl2br($formContent);
}
else
{
?>
<script language="Javascript">
var myQue="<?=$queString?>";
var myAns="<?=$ansString?>";

document.write(viewForm(myQue, myAns));
</SCRIPT>
<? } ?>
</td></tr>
</table>
</td></tr>

<tr><td><img src="../../../images/admin/pop_bottom.gif" width=422 height=18 border=0></td></tr>
<tr><td align=center height="40" style="vertical-align:bottom">
<a href="javascript:window.print()"><img alt='<?=$button_print?>' src="/images/admin/button/s_btn_print_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
<?php
include_once("../../../templates/filefooter.php");
?>