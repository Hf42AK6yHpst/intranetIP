<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
if ($plugin['Discipline'])
{
include_once("../../../includes/libdiscipline.php");
}
intranet_opendb();

$li = new libuser($studentid);
$classname = $li->ClassName;
$classnumber = $li->ClassNumber;

$reason = intranet_htmlspecialchars(trim($reason));
$attenddate = intranet_htmlspecialchars(trim($attenddate));
$year = intranet_htmlspecialchars(trim($year));
$semester = intranet_htmlspecialchars(trim($semester));

$fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, Reason, DateInput,DateModified,ClassName,ClassNumber";
$fieldvalue = "'$studentid','$attenddate','$year','$semester','$type','$daytype','$reason',now(),now(),'$classname','$classnumber'";
$sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);

$warning_student_ids = array();

if ($plugin['Discipline'] && $type == 2)
{
                $ldiscipline = new libdiscipline();
                if($ldiscipline->calculateUpgradeLateToDemerit($studentid))
                {
                   $warning_student_ids[] = $studentid;
                }
                $ldiscipline->calculateUpgradeLateToDetention($studentid);
}
intranet_closedb();
if (sizeof($warning_student_ids)!=0)
{
        $body_tags = "onLoad=document.form1.submit()";
        include_once("../../../../templates/fileheader.php");
        ?>
        <form name=form1 action=shownotice_msg_prompt.php method=POST>
        <?
        for ($i=0; $i<sizeof($warning_student_ids); $i++)
        {
             ?>
             <input type=hidden name=WarningStudentID[] value="<?=$warning_student_ids[$i]?>">
             <?
        }
        ?>
        <input type=hidden name=studentid value="<?=$studentid?>">
        <input type=hidden name=classid value="<?=$classid?>">
        </form>
        <?
        include_once("../../../../templates/filefooter.php");

}
else
{
    header("Location: studentview.php?studentid=$studentid&msg=1&classid=$classid");
}
?>