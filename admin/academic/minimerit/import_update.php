<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libminimerit.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_opendb();

$limport = new libimporttext();

$file_format = array("Year","Semester","Date","Class","Class Number","Type","Reason","Remark","PIC Login");

$li = new libdb();
$lo = new libfilesystem();
$lmerit = new libminimerit();
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php");
} else {
        $ext = strtoupper($lo->file_ext($filename));
        if($limport->CHECK_FILE_EXT($filename))
        {
           # read file into array
           # return 0 if fail, return csv array if success
           //$data = $lo->file_read_csv($filepath);
           $data = $limport->GET_IMPORT_TXT($filepath);
           $col_name = array_shift($data);                   # drop the title bar

           #Check file format
           $format_wrong = false;
           for ($i=0; $i<sizeof($file_format); $i++)
           {
                if ($col_name[$i]!=$file_format[$i])
                {
                    $format_wrong = true;
                    break;
                }
           }
           if ($format_wrong)
           {
               header ("Location: import.php?error=1");
               exit();
           }

           $today = date('Y-m-d');
           $currentYear = getCurrentAcademicYear();
           $currentSem = getCurrentSemester();
           for ($i=0; $i<sizeof($data); $i++){

                list($year,$sem,$date,$class,$classnum,$type,$reason,$remark,$pic) = $data[$i];
                $date = (($date == "")? $today: $date);
                $year = ($year == ""? $currentYear : $year);
                $sem = ($sem == ""? $currentSem : $sem);
                $class = trim(addslashes($class));
                $classnum = trim(addslashes($classnum));
                $reason = trim(addslashes($reason));
                $remark = trim(addslashes($remark));
                $pic = trim(addslashes($pic));

				switch($type){
					case 'D1' : $type='-1'; break;
					case 'D2' : $type='-2'; break;
                    case 'D3' : $type='-3'; break;
				}

				if ($class != "" && $classnum != ""){
					$classnumTemp = ($classnum<10)?"0".$classnum:$classnum;
					$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName='$class' AND ClassNumber='$classnumTemp'";
					$userid = $li->returnVector($sql);

					$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 1 AND RecordStatus = 1 AND UserLogin='$pic'";
					$picid = $li->returnVector($sql);

                    $fieldvalue = " $userid[0],'$date','$year','$sem','$type','$reason','$remark', NOW(), NOW() ,'$class','$classnum',$picid[0]";
		            $fields_name = "UserID, MeritDate, Year, Semester, RecordType, Reason,Remark, DateInput, DateModified,ClassName,ClassNumber,PersonInCharge";

					$sql = "INSERT INTO PROFILE_STUDENT_MINI_MERIT ($fields_name) VALUES (".$fieldvalue.")";
					$li->db_db_query($sql);

					# update minor, major
					$lmerit->synAllGradeUp($userid[0]);
				}
			}

           header("Location: index.php?msg=1");
        }
}

intranet_closedb();
?>