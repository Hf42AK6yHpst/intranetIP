<?
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libclass.php");

include_once("../../../includes/libminimerit.php");

include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");

//include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

$lmerit = new libminimerit($id);
$studentid = $lmerit->UserID;
$lu = new libuser($studentid);

$typeSelect = $lmerit->getSelectMeritType("name=type ",$lmerit->RecordType);
$select_sem = getSelectSemester("name=semester",$lmerit->Semester);
$teacherSelect = $lu->getSelectTeacherStaff("name=PersonInCharge",$lmerit->PersonInCharge);

$lword = new libwordtemplates();
$reasonList = $lword->getSelectMerit("name=wordTemp onChange=\"this.form.reason.value=this.value\"", $lmerit->RecordType);
$word_arr_merit = $lword->getWordListMerit();
$word_arr_demerit = $lword->getWordListDemerit();
?>

<script language="javascript">
function checkform(obj){
         if(!check_date(obj.meritdate,"<?="$i_alert_pleasefillin$i_Merit_Date"?>")) return false;
         if(!check_text(obj.year,"<?="$i_alert_pleasefillin$i_Profile_Year"?>")) return false;
         return true;
}
</script>

<form name="form1" action="subdetail_update.php" method="post" onSubmit="return checkform(this);">
<table border=0 cellpadding=4 cellspacing=0>
<tr><td align=right <?=($intranet_session_language=="en"?"":"nowrap")?>><?php echo $i_UserLogin; ?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?php echo $i_UserEnglishName; ?>:</td><td><?=$lu->EnglishName?></td></tr>
<tr><td align=right><?php echo $i_UserChineseName; ?>:</td><td><?=$lu->ChineseName?></td></tr>
<tr><td align=right><?php echo $i_UserClassName; ?>:</td><td><?=$lu->ClassName?></td></tr>
<tr><td align=right><?php echo $i_UserClassNumber; ?>:</td><td><?=$lu->ClassNumber?></td></tr>
<tr><td align=right><?php echo $i_Profile_Year; ?>:</td><td><input type=text name=year value='<?=$lmerit->Year?>'></td></tr>
<tr><td align=right><?php echo $i_Profile_Semester; ?>:</td><td><?=$select_sem?></td></tr>
<tr><td align=right><?php echo $i_Merit_Date; ?>:</td><td><input type=text size=10 maxlength=10 name=meritdate value='<?=$lmerit->MeritDate?>'> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td align=right><?php echo $i_Merit_Type; ?>:</td><td><?=$typeSelect?></td></tr>
<tr><td align=right><?php echo $i_Profile_PersonInCharge; ?>:</td><td><?=$teacherSelect?></td></tr>
<tr><td align=right><?php echo $i_Merit_Reason; ?>:</td><td><input type=text size=30 name=reason value='<?=$lmerit->Reason?>'><?=$reasonList?></td></tr>
<tr><td align=right><?php echo $i_Merit_Remark; ?>:</td><td><textarea name=remark cols=30 rows=5><?=$lmerit->Remark?></textarea></td></tr>
<tr><td colspan=2><hr size=1></td></tr>
<tr><td align=right colspan=2><input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
<a href="javascript:self.close()"><img src='/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif' border='0'></a></td></tr>
</table>

<input type=hidden name=studentid value=<?=$studentid?>>
<input type=hidden name=StudentMeritID value=<?=$id?>>
<input type=hidden name=originalType value=<?=$lmerit->RecordType?>>

</form>
