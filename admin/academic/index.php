<?php 
# using: yat

#################################
#
#	Date:	2011-06-29	YatWoon
#			add remark
#
#################################

include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
// $li_menu from adminheader_intranet.php

?>

<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '') ?>
<?= displayTag("head_academic_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
	<td>
		<?=$Lang['StudentProfile']['Reamrk1']?>
		<br><br>
		<?=$Lang['StudentProfile']['Reamrk2']?>
		<br><br>
	</td>
</tr>
<tr>
<td>
<blockquote>
<?= displayOption(
                        $i_Profile_Attendance, 'attendance/', $li_menu->is_access_attendance,
                                $i_Profile_Merit, 'merit/', $li_menu->is_access_merit,
                                $i_Profile_Service, 'service/', $li_menu->is_access_service,
                                $i_Profile_Activity, 'activity/', $li_menu->is_access_activity,
                                $i_Profile_Award, 'award/', $li_menu->is_access_award,
//                                $i_Profile_Assessment, 'assessment/', $li_menu->is_access_assessment,
//                                $i_Profile_Files,'student_files/',  $li_menu->is_access_student_files,
                                $i_Profile_OverallStudentView, 'studentview.php', 1,
                                $i_Profile_OverallStudentViewLeft, 'studentview_left.php',1,
                                $i_Profile_DataLeftStudent, 'searchArchiveIndex.php',1
                                ) ?>
</blockquote>
</td></tr>
</table>
<?
include_once("../../templates/adminfooter.php");

?>
