<?php

################################################
#
#	Date:	2016-07-20 (Bill)
#			Replace deprecated split() by explode() for PHP 5.4
#
#	Date:	2012-11-23	YatWoon
#			fixed: Failed to display school logo in file name with space [Case#2012-1122-1536-19132]
#
################################################

include_once("../../includes/global.php");

if($sys_custom['student_profile_report_page_break'])
{
	header("Location: studentprint_break.php?StudentID=$StudentID&class=$class&year=$year&sem=$sem");
	exit;
}

include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libattendance.php");
include_once("../../includes/libmerit.php");
include_once("../../includes/libservice.php");
include_once("../../includes/libactivity.php");
include_once("../../includes/libaward.php");
include_once("../../includes/libassessment.php");
include_once("../../includes/libstudentfiles.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");
// $li_menu from adminheader_intranet.php
intranet_opendb();

// get school information (name + badge)
$li = new libfilesystem();
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$schoolbadge = ($imgfile!="") ? "<img src='/file/$imgfile' width=120 height=60><br>\n" : "&nbsp;";

//$school_data = split("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? $school_data[0] : "&nbsp;";

if ($schoolbadge!="&nbsp;" || $school_name!="&nbsp;")
{
        $school_info = "<table border=0 width=650>\n";
        $school_info .= "<tr><td align=center>$schoolbadge <font size=+0><b>$school_name</b></font></td></tr>\n";
        $school_info .= "</table>\n<br>\n";
}

#######################################
# Customization (Not display school badge)

if($sys_custom['student_profile_report_no_header'] == true)
{
	$school_info = "";
}
#######################################

$li_menu = new libaccount();
$li_menu->is_access_function($PHP_AUTH_USER);

$lstudentprofile = new libstudentprofile();

$lclass = new libclass();
$lu = new libuser($StudentID);
$case = 0;
/*
# Selection of class and student
if ($class !="")
{
    $student_selection = "$i_PrinterFriendly_StudentName: ".$lclass->getStudentSelectByClass($class,"name=StudentID onChange=this.form.submit()",$StudentID);
}
$class_selection = "$i_ClassName: ".$lclass->getSelectClass("name=class onChange=\"this.form.submit()\"",$class);
$toolbar = "$class_selection <br> $student_selection";
*/

$toolbar = "$i_ClassName: $class <br>$i_PrinterFriendly_StudentName: ".$lu->UserNameClassNumber();
if ($StudentID != "")
{
$now = time();
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));

$lattend = new libattendance();
$lmerit = new libmerit();
$lact = new libactivity();
$lservice = new libservice();
$laward = new libaward();
$lassess = new libassessment();
$lfiles = new libstudentfiles();

$year_attend = $lattend->returnYears($StudentID);
$year_merit = $lmerit->returnYears($StudentID);
$year_act = $lact->returnYears($StudentID);
$year_service = $lservice->returnYears($StudentID);
$year_award = $laward->returnYears($StudentID);
$year_assess = $lassess->returnYears($StudentID);
$year_files = $lfiles->returnYears($StudentID);


$year_array = array_merge((array)$year_attend,(array)$year_merit,(array)$year_act,(array)$year_service,(array)$year_award,(array)$year_access,(array)$year_files);
if ($comeFrom != 'left') {
	$currentYear = getCurrentAcademicYear();
	$year_array[] = $currentYear;
}
$year_array = array_unique($year_array);
rsort($year_array);

$year_array = array_values($year_array);

##### global variable called by : 
#	- libattendance.php
#	- libmerit.php
$all_distinct_years = $year_array;


//$year_display = ($year==""?getCurrentAcademicYear():$year);
$year_display = ($year);
//$year_display = ($year==""?"&nbsp;":$year);

}



?>
<SCRIPT LANGUAGE=Javascript>
function viewAttend(id)
{
         newWindow("/admin/academic/view_attendance.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function viewMerit(id)
{
         newWindow("/admin/academic/view_merit.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function openPrintPage()
{
         newWindow("printpage.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function viewAttendByYear()
{
         return;
}
function viewMeritByYear()
{
         return;
}
</SCRIPT>
<form name=form1 action="">
<?=$school_info?>
<table width=560 border=0 cellpadding=0 cellspacing=0>
<tr><td>
<?
if ($year_display != "")
echo "$i_AwardYear: ".$year_display." $sem";
?><br>
<?php echo $toolbar;
?></td></tr>
<tr><td><br></td></tr>
<tr>
<td class=tableContent height=300 align=left>
<?php if ($StudentID != "") { ?>
<? if ($year == "") { ?>
<?=$i_Profile_ClassHistory?>
<?=$lclass->displayClassHistoryAdmin($StudentID)?>
<br>
<? } ?>
<? if ($li_menu->is_access_attendance && !$lstudentprofile->is_printpage_attendance_hidden) { ?>
<?=$i_Profile_Attendance?>
<?=$lattend->displayStudentRecordByYearAdmin($StudentID,$year,$sem,true)?>
                        <br>
<br>
<? }
   if ($li_menu->is_access_merit && !$lstudentprofile->is_printpage_merit_hidden) {
?>
<?=$i_Profile_Merit?>
<?=$lmerit->displayStudentRecordByYearAdmin($StudentID,$year,$sem,true)?>
<br>
<? }
   if ($li_menu->is_access_service && !$lstudentprofile->is_printpage_service_hidden) {
?>
<?=$i_Profile_Service?>
<?=$lservice->displayServiceAdmin($StudentID,$year,$sem)?>
<br>
<? }
   if ($li_menu->is_access_activity && !$lstudentprofile->is_printpage_activity_hidden) {
?>
<?=$i_Profile_Activity?>
<?=$lact->displayActivityAdmin($StudentID,$year,$sem)?>
<br>
<? }
   if ($li_menu->is_access_award && !$lstudentprofile->is_printpage_award_hidden) {
?>
<?=$i_Profile_Award?>
<?=$laward->displayAwardAdmin($StudentID,$year,$sem)?>
<? } ?>
<br>
<br>
<?php } ?>
</td></tr>
</table>
<!---
<table border=0 width=550>
<tr><td align=center>
<a href="javascript:window.print()"><img src="/images/admin/button/s_btn_print_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
--->
<?
include_once("../../templates/filefooter.php");

?>
