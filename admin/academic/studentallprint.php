<?php
# 

################################################
#
#	Date:	2016-07-20 	Bill
#			Replace deprecated split() by explode() for PHP 5.4
#
#	Date:	2012-11-23	YatWoon
#			fixed: Failed to display school logo in file name with space [Case#2012-1122-1536-19132]
#
################################################
include_once("../../includes/global.php");

if($sys_custom['student_profile_report_page_break'])
{
	header("Location: studentallprint_break.php?class=$class&generalYear=$generalYear&generalSem=$generalSem");
	exit;
}

include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libattendance.php");
include_once("../../includes/libmerit.php");
include_once("../../includes/libservice.php");
include_once("../../includes/libactivity.php");
include_once("../../includes/libaward.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");
intranet_opendb();

#######################################
# Customization (Not display last 2 cols) (0 - Show, 1 - Hide)
#
# Column1 - Year, Column2 - Merit, Column3 - Min_Merit, Column4 - Maj_Merit, Column5 - Sup_Merit
# Column6 - Ult_Merit, Column7 - Demerit, Column8 - Min_Demerit, Column9 - Maj_Demerit, Column10 - Sup_Demerit
# Column11 - Ult_Demerit
//$merit_col_not_display = array(0,0,0,0,0,0,0,0,0,1,1);
#######################################

#######################################
# Customization - Hide School Info Or Not (false - Show, true - Hide)
//$HideSchoolInfo = true;
#######################################

# Parameters of page
//Default value of number of line in each page
$defaultNumOfLine = 40;

//Default width of a specific field
$defaultFieldWidth1 = 53;

//current number of line remaining
$lineRemain = $defaultNumOfLine;

//how many line need
$fieldLineNeed = 1;

# Page layout details
$page_breaker = "<P CLASS='breakhere'>";
$page_header_linefeed = 6;
$intermediate_pageheader = "<P CLASS='breakhere'>";
$intermediate_linefeed = 5;
?>

<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE>

<?
// get school information (name + badge)
$li = new libfilesystem();
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$schoolbadge = ($imgfile!="") ? "<img src='/file/$imgfile' width=120 height=60><br>\n" : "&nbsp;";

//$school_data = split("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? $school_data[0] : "&nbsp;";

if($HideSchoolInfo) # For Customization Used Only
{
        #######################################
        # Customization (Not display school badge)
        $school_info = "<table border=0 width=650 height=60>\n";
        $school_info .= "<tr><td align=center>&nbsp;</td></tr>\n";
        $school_info .= "</table>\n<br>\n";
        #######################################
}
else
{
        if ($schoolbadge!="&nbsp;" || $school_name!="&nbsp;")
        {
                $school_info = "<table border=0 width=650 height=60>\n";
                $school_info .= "<tr><td align=center>$schoolbadge <font size=+0><b>$school_name</b></font></td></tr>\n";
                $school_info .= "</table>\n<br>\n";
        }
}

#######################################
# Customization (Not display school badge)

if($sys_custom['student_profile_report_no_header'] == true)
{
        $school_info = "";
}
#######################################

$li_menu = new libaccount();
$li_menu->is_access_function($PHP_AUTH_USER);

$lstudentprofile = new libstudentprofile();

# settings controling show/hide columns in merit/demerit table
$field_disabled_array = array(0,$lstudentprofile->is_merit_disabled,$lstudentprofile->is_min_merit_disabled,
        $lstudentprofile->is_maj_merit_disabled, $lstudentprofile->is_sup_merit_disabled,
        $lstudentprofile->is_ult_merit_disabled, $lstudentprofile->is_black_disabled,
        $lstudentprofile->is_min_demer_disabled, $lstudentprofile->is_maj_demer_disabled,
        $lstudentprofile->is_sup_demer_disabled, $lstudentprofile->is_ult_demer_disabled);

# Customarization for UCCKE to hide "homework" and "bad name" in print preview
# 6: homework column
# 7: badname column
$field_disabled_array[6] = $sys_custom['student_profile_report_print_no_homework']?true:$field_disabled_array[6];
$field_disabled_array[7] = $sys_custom['student_profile_report_print_no_badname']?true:$field_disabled_array[7];

# Current Academic Year or All
$year_display = ($generalYear==0?getCurrentAcademicYear():"");

$data = array();

# Grep all students information
$lclass = new libclass();
if ($left == 1)
{
    $student_conds = "AND RecordStatus = 3";
}
else
{
    $student_conds = "AND RecordStatus != 3";
}
$name_field = getNameFieldByLang();
if ($class != "")
{
    $sql = "SELECT UserID, $name_field, ClassNumber, ClassName FROM INTRANET_USER WHERE RecordType = 2 $student_conds AND ClassName = '$class' ORDER BY ClassNumber";
    $students = $lclass->returnArray($sql,4);
}
else
{
    $sql = "SELECT UserID, $name_field, ClassNumber, ClassName FROM INTRANET_USER WHERE RecordType = 2 $student_conds ORDER BY ClassName, ClassNumber";
    $students = $lclass->returnArray($sql,4);
}
    $lattend = new libattendance();
    $lmerit = new libmerit();
    $lact = new libactivity();
    $lservice = new libservice();
    $laward = new libaward();

    for ($iter=0; $iter<sizeof($students); $iter++)
    {
         list ($studentID, $studentName, $studentClassnumber, $studentClassName) = $students[$iter];
         if ($class != "") $studentClassName = $class;
         # Attendance data
         if ($li_menu->is_access_attendance && !$lstudentprofile->is_printpage_attendance_hidden)
         {
             $dataAttendance = $lattend->returnStudentRecordByYear($studentID,$year_display,$generalSem);
         }
         # Merit data
         if ($li_menu->is_access_merit && !$lstudentprofile->is_printpage_merit_hidden)
         {
             $dataMerit = $lmerit->returnStudentRecordByYear($studentID,$year_display,$generalSem);
         }
         # Service Data
         if ($li_menu->is_access_service && !$lstudentprofile->is_printpage_service_hidden)
         {
             $dataService = $lservice->getServiceByStudent($studentID,$year_display,$generalSem);
         }
         # Activity Data
         if ($li_menu->is_access_activity && !$lstudentprofile->is_printpage_activity_hidden)
         {
             $dataActivity = $lact->getActivityByStudent($studentID,$year_display,$generalSem);
         }
         # Award Data
         if ($li_menu->is_access_award && !$lstudentprofile->is_printpage_award_hidden)
         {
             $dataAward = $laward->getAwardByStudent($studentID,$year_display,$generalSem);
         }
         $studentInfo = array($studentID,$studentName,$studentClassnumber,$dataAttendance,$dataMerit,$dataService,$dataActivity,$dataAward);
         #$data[] = $studentInfo;

         # Print page construct
        #$lineRemain=$lineRemain - 5 ;
/*
        if ($iter==0){
                echo "<P CLASS='breakhere'>";
                $lineRemain = $defaultNumOfLine - 5;
        }
        */
        $lineRemain = $defaultNumOfLine - $page_header_linefeed;

         echo "$school_info <br>\n";
         echo "<table width=560 border=0 cellpadding=0 cellspacing=0>\n";
         #echo "<tr><td><br><br></td></tr>\n";
         echo "<tr><td>\n";
         if ($year_display != "")
         {
             echo "$i_AwardYear: $year_display $generalSem<br>\n";
         }
         echo "$i_ClassName: $studentClassName<br>\n$i_PrinterFriendly_StudentName: $studentName ($studentClassName - $studentClassnumber)<br><br>\n";
         echo "</td></tr>\n";
         echo "<tr><td><br></td></tr>
                <tr><td class=tableContent height=300 align=left>\n";
        # $x store text in 1 student
        $x = "";
        if ($year_display == "")
        {
            $x .= "$i_Profile_ClassHistory";
            $classtable = $lclass->displayClassHistoryAdmin($studentID);
            $x .= $classtable;
            $x .= "<br>";
            $lineRemain -= 5;
        }

         # Attendance
        if ($li_menu->is_access_attendance && !$lstudentprofile->is_printpage_attendance_hidden)
        {

                        for ($j=0; $j<sizeof($dataAttendance); $j++){

                list($dataAttendanceYear, $dataAttendanceAbs, $dataAttendanceLate, $dataAttendanceEarly)
                        = $dataAttendance[$j];

                $currentLineNeed = ceil ( strlen($dataAttendanceYear . $dataAttendanceAbs . $dataAttendanceLate . $dataAttendanceEarly ) / $defaultFieldWidth1 );

                if ($fieldLineNeed < $currentLineNeed)
                        $fieldLineNeed = $currentLineNeed;

                        }
                        $lineRemain=$lineRemain - $fieldLineNeed*sizeof($dataAttendance) ;

                        if ($lineRemain <0){
                $x .= "$intermediate_pageheader";
                $lineRemain = $defaultNumOfLine - $fieldLineNeed*sizeof($dataAttendance);
                        }

                        $x .= $i_Profile_Attendance;
                        $x .= "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
                        $title_array = array($i_Attendance_Year,$i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave);
                        $size = sizeof($dataAttendance[0]) + 1;
                        $width = 100/$size;
                        # ================== Changes from here
                        $x .= "<tr>\n";
                        for ($i=0; $i<sizeof($title_array); $i++)
                        {
                                 $x .= "<td class=tableTitle_new>".$title_array[$i]."</td>";
                        }
                        if (sizeof($dataAttendance)==0)
                        {
                                $x .= "<tr><td colspan=4 align=center>$i_no_record_exists_msg</td></tr>\n";
                        }
                        //$x .= "</tr>\n";
                        for ($i=0; $i<sizeof($dataAttendance); $i++)
                        {
                                 $x .= "<tr>\n";
                                 for ($j=0; $j<sizeof($dataAttendance[$i]); $j++)
                                 {
                                          $data = $dataAttendance[$i][$j];
                                          $x .= "<td>$data</td>";
                                 }
                                 $x .= "</tr>\n";
                        }
                        $x .= "</table>\n";

                        # End of attendance

                        $x .= "<br>\n";
                        $lineRemain--;
        }

        # Merit
        if ($li_menu->is_access_merit && !$lstudentprofile->is_printpage_merit_hidden)
        {

        for ($j=0; $j<sizeof($dataMerit); $j++){

                $length = 0;
                for ($pos=0; $pos < sizeof($dataMerit[$j]); $pos++)
                {
                     $length += strlen($dataMerit[$j][$pos]);
                }
                $currentLineNeed = ceil ( $length / $defaultFieldWidth1 );

                if ($fieldLineNeed < $currentLineNeed)
                        $fieldLineNeed = $currentLineNeed;

        }
        $lineRemain=$lineRemain - $fieldLineNeed*sizeof($dataMerit) ;

        if ($lineRemain <0){
                $x .= "$intermediate_pageheader";
                $lineRemain = $defaultNumOfLine - $fieldLineNeed*sizeof($dataMerit);
        }

        $x .= $i_Profile_Merit;
        $x .= "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
        $title_array = array($i_Profile_Year);
        $title_array = array_merge($title_array,$i_Merit_TypeArray);
        $size = sizeof($dataMerit[0]) + 1;
        # $width = 100/$size;

        # ================== Changes from here
        $x .= "<tr>\n";
        for ($i=0; $i<sizeof($title_array); $i++)
        {
	        if ($merit_col_not_display[$i]==1) continue;
            if (!$field_disabled_array[$i])
            {
                 $x .= "<td class=tableTitle_new>".$title_array[$i]."</td>";
                 $column_cnt++;
            }
        }
        $x .= "</tr>\n";
        if (sizeof($dataMerit)==0)
        {
                $x .= "<tr><td colspan=".$column_cnt." align=center>$i_no_record_exists_msg</td></tr>\n";
        }
        for ($i=0; $i<sizeof($dataMerit); $i++)
        {
             $x .= "<tr>\n";
             for ($j=0; $j<sizeof($dataMerit[$i]); $j++)
             {
                  if ($merit_col_not_display[$j]==1) continue;
                  if (!$field_disabled_array[$j])
                  {
                       $data = $dataMerit[$i][$j];
                       $x .= "<td>$data</td>";
                  }
             }
             $x .= "</tr>\n";
        }
        $x .= "</table>\n";

        # End of Merit

        $x .= "<br>\n";
        $lineRemain--;
        }

        # Service
        if ($li_menu->is_access_service && !$lstudentprofile->is_printpage_service_hidden)
        {

        for ($j=0; $j<sizeof($dataService); $j++){
                $length = 0;
                for ($pos=0; $pos < sizeof($dataService[$j]); $pos++)
                {
                     $length += strlen($dataService[$j][$pos]);
                }
                $currentLineNeed = ceil ( $length / $defaultFieldWidth1 );

                if ($fieldLineNeed < $currentLineNeed)
                        $fieldLineNeed = $currentLineNeed;

        }
        $lineRemain=$lineRemain - $fieldLineNeed*sizeof($dataService) ;

        if ($lineRemain <0){
                $x .= "$intermediate_pageheader";
                $lineRemain = $defaultNumOfLine - $fieldLineNeed*sizeof($dataService);
        }

        $x .= $i_Profile_Service;

                $x .= "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_ServiceYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_ServiceSemester</td>
                      <td width=90 align=center class=tableTitle_new>$i_ServiceDate</td>
                      <td width=110 align=center class=tableTitle_new>$i_ServiceName</td>
                      <td width=90 align=center class=tableTitle_new>$i_ServiceRole</td>
                      <td width=90 align=center class=tableTitle_new>$i_ServicePerformance</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($dataService); $i++)
                {
                     list($year,$sem,$sDate,$service,$role,$performance) = $dataService[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$sDate</td>
                              <td align=center>$service</td>
                              <td align=center>$role&nbsp;</td>
                              <td align=center>$performance&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($dataService)==0)
                {
                    $x .= "<tr><td colspan=6 align=center>$i_ServiceNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";


        # End of Service

        $x .= "<br>\n";
        $lineRemain--;
        }

        # Activity
        if ($li_menu->is_access_activity && !$lstudentprofile->is_printpage_activity_hidden)
        {

        for ($j=0; $j<sizeof($dataActivity); $j++){
                $length = 0;
                for ($pos=0; $pos < sizeof($dataActivity[$j]); $pos++)
                {
                     $length += strlen($dataActivity[$j][$pos]);
                }
                $currentLineNeed = ceil ( $length / $defaultFieldWidth1 );

                if ($fieldLineNeed < $currentLineNeed)
                        $fieldLineNeed = $currentLineNeed;

        }
        $lineRemain=$lineRemain - $fieldLineNeed*sizeof($dataActivity) ;

        if ($lineRemain <0){
                $x .= "$intermediate_pageheader";
                $lineRemain = $defaultNumOfLine - $fieldLineNeed*sizeof($dataActivity);
        }
        $x .= $i_Profile_Activity;
                $x .= "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_ActivityYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_ActivitySemester</td>
                      <td width=150 align=center class=tableTitle_new>$i_ActivityName</td>
                      <td width=110 align=center class=tableTitle_new>$i_ActivityRole</td>
                      <td width=120 align=center class=tableTitle_new>$i_ActivityPerformance</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($dataActivity); $i++)
                {
                     list($year,$sem,$actName,$role,$performance) = $dataActivity[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$actName</td>
                              <td align=center>$role&nbsp;</td>
                              <td align=center>$performance&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($dataActivity)==0)
                {
                    $x .= "<tr><td colspan=5 align=center>$i_ActivityNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";

        # End of Activity

        $x .= "<br>\n";
        $lineRemain--;
        }

        # Award
        if ($li_menu->is_access_award && !$lstudentprofile->is_printpage_award_hidden)
        {

        for ($j=0; $j<sizeof($dataAward); $j++){
                $length = 0;
                for ($pos=0; $pos < sizeof($dataAward[$j]); $pos++)
                {
                     $length += strlen($dataAward[$j][$pos]);
                }
                $currentLineNeed = ceil ( $length / $defaultFieldWidth1 );

                if ($fieldLineNeed < $currentLineNeed)
                        $fieldLineNeed = $currentLineNeed;

        }
        $lineRemain=$lineRemain - $fieldLineNeed*sizeof($dataAward) ;

        if ($lineRemain <0){
                $x .= "$intermediate_pageheader";
                $lineRemain = $defaultNumOfLine - $fieldLineNeed*sizeof($dataAward);
        }
        $x .= $i_Profile_Award;
                $x .= "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_AwardYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_AwardSemester</td>
                      <td width=90 align=center class=tableTitle_new>$i_AwardDate</td>
                      <td width=170 align=center class=tableTitle_new>$i_AwardName</td>
                      <td width=120 align=center class=tableTitle_new>$i_AwardRemark</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($dataAward); $i++)
                {
                     list($year,$sem,$aDate,$award,$remark) = $dataAward[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$aDate</td>
                              <td align=center>$award</td>
                              <td align=center>$remark&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($dataAward)==0)
                {
                    $x .= "<tr><td colspan=5 align=center>$i_AwardNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";

        # End of Award
        }
        echo $x;
        echo "</td></tr></table>";
        if ($iter < sizeof($students)-1)
            echo "$page_breaker";
            
    }


#print_r($data);
intranet_closedb();

include_once("../../templates/filefooter.php");

?>
