<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libmerit.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lmerit = new libmerit();

$now = time();
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));

if ($StudentType=="Archive"){	
		$lu = new libuser();
		$toolbar = "$i_Merit_DetailedMeritRecord<BR>\n$i_UserStudentName: ".$lu->ArchiveUserNameClassNumber($StudentID);		
		$x = $lmerit->displayArchiveDetailedRecordByYear($StudentID,$year);
}
else{	
		$lu = new libuser($StudentID);
		$toolbar = "$i_Merit_DetailedMeritRecord<BR>\n$i_UserStudentName: ".$lu->UserNameClassNumber();	
		
		if ($year == "")
		{
		    $x = $lmerit->displayStudentRecord($StudentID,$yearstart,$yearend);
		}
		else
		{
		    $x = $lmerit->displayDetailedRecordByYear($StudentID,$year);
		}
}


include_once("../../templates/fileheader.php");
?>

<form name=form1 method=get>
<?=$toolbar?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10">&nbsp;</td>
    <td width="*">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><?=$x?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<input type=hidden name=type value="<?=$type?>">
</form>

<?php
include_once("../../templates/filefooter.php");
intranet_closedb();
?>