<?

###############################
#
#	Date:	2011-03-07	YatWoon
#			- Improved: display "Whole Year" is semester is empty
#
###############################

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$li = new libdb();
$lexport = new libexporttext();

$conds = "";
if ($yrfilter != "")
{
    $conds .= " AND a.Year = '$yrfilter'";
}
if ($class != "")
{
    $conds .= " AND b.ClassName = '$class'";
}
if ($AwardName != "")
{
    $conds .= " AND UPPER(a.AwardName) like UPPER('%$AwardName%')";
}
if ($AwardRemark != "")
{
    $conds .= " AND UPPER(a.Remark) like UPPER('%$AwardRemark%')";
}
if ($datetype != 0)
{
    $date_field = "";
    if ($datetype == 1)
    {
        $date_field = "a.AwardDate";
    }
    else if ($datetype == 2)
    {
         $date_field = "a.DateModified";
    }
    if ($date_field != "")
    {
        if ($date_from != "")
        {
            $conds .= " AND $date_field >= '$date_from'";
        }
        if ($date_to != "")
        {
            $conds .= " AND $date_field <= '$date_to 23:59:59'";
        }
    }
}

$studentnamefield = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT
			a.Year,
			 IF (a.Semester='','". $Lang['General']['WholeYear'] ."',a.Semester) as Semester,
			a.AwardDate,
			b.ClassName, b.ClassNumber,
			$studentnamefield, 
			a.AwardName,
			a.Remark,
			a.Organization,
			a.SubjectArea
        FROM 
        			PROFILE_STUDENT_AWARD as a
        			INNER JOIN 
        			INTRANET_USER as b
        			ON a.UserID = b.UserID 
        WHERE 
        			(b.ChineseName like '%$keyword%' 
        			 OR b.EnglishName like '%$keyword%' 
        			 OR a.AwardName like '%$keyword%'
               OR a.Year like '%$keyword%'
               OR a.Semester like '%$keyword%'
               OR a.Remark like '%$keyword%'
              ) 
              $conds
              ";

$result = $li->returnArray($sql);
/*
for ($i=0; $i< sizeof($result); $i++) {
	for ($j=0;$j < 7; $j++) {
		$result[$i][$j] = '"'.$result[$i][$j].'"';
	}
}
*/
// $x = "\"StudentName\",\"Year\",\"Semester\",\"AwardDate\",\"AwardName\",\"Remark\",\"DateModified\"\n";
$exportColumn = array("Year", "Semester", "Date","Class","Class Number","StudentName",  "Award Name", "Remark", "Organization","SubjectArea");

/*
for ($i=0; $i<sizeof($result); $i++)
{
     $delim = "";
     for ($j=0; $j<sizeof($result[$i]); $j++)
     {
          $x .= "$delim\"".str_replace('"', '""', intranet_undo_htmlspecialchars($result[$i][$j]))."\"";
          $delim = ",";
     }
     $x .= "\n";
}
*/
// Output the file to user browser
$filename = "award_data.csv";
/*
header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($x) );
header("Content-Disposition: attachment; filename=\"".$filename."\"");

echo $x;
*/

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);


intranet_closedb();
?>
