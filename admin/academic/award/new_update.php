<?php

#########################################
##	Modification Log:
##
##	2015-01-08	Omas
##	- Fixed : Insert student get award 's academic year classname, classnumber into db
##
#########################################
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/form_class_manage.php");
intranet_opendb();

//$form_class = new Year();
$li = new libuser($studentid);

//case #73503
//$classname = $li->ClassName; 
//$classnumber = $li->ClassNumber;

// $year = intranet_htmlspecialchars(trim($year));
$awardDate = intranet_htmlspecialchars(trim($awardDate));
$award = intranet_htmlspecialchars(trim($award));
$remark = intranet_htmlspecialchars(trim($remark));
$organization = intranet_htmlspecialchars(trim($organization));
$subjectarea = intranet_htmlspecialchars(trim($subjectarea));

$academic_year = new academic_year($AcademicYearID);
$year = $academic_year->YearNameEN;
$academic_year_term = new academic_year_term($YearTermID);
$semester = $academic_year_term->YearTermNameEN;
$form_class_manage = new year_class();

# Class of student following AcademicYear instead of current class
$classinfoAry = $li->getStudentInfoByAcademicYear($AcademicYearID);

$classname = $classinfoAry[0]['ClassNameEn'];
$classnumber = $classinfoAry[0]['ClassNumber'];


$awardDate = ($awardDate==""? "NULL":"'$awardDate'");

$fieldname = "UserID, Year, Semester,AwardDate, AwardName,Remark, Organization, SubjectArea, DateInput,DateModified,ClassName,ClassNumber";
$fieldvalue = "'$studentid','$year','$semester',$awardDate,'$award','$remark','$organization','$subjectarea',now(),now(),'$classname','$classnumber'";
$sql = "INSERT INTO PROFILE_STUDENT_AWARD ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: studentview.php?studentid=$studentid&msg=1&classid=$classid");
?>
