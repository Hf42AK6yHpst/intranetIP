<?
###############################
#
#	Date:	2011-03-07	YatWoon
#			- Improved: display "Whole Year" is semester is empty
#
###############################

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$li = new libdb();
$lexport = new libexporttext();

$conds = "";
if ($yrfilter != "")
{
    $conds .= " AND a.Year = '$yrfilter'";
}
if ($role != "")
{
    $conds .= " AND UPPER(a.Role) like UPPER('%$role%')";
}
if ($class != "")
{
    $conds .= " AND b.ClassName = '$class'";
}
if ($performance != "")
{
    $conds .= " AND UPPER(a.Performance) like UPPER('%$performance%')";
}
if ($ServiceName != "")
{
    $conds .= " AND UPPER(a.ServiceName) like UPPER('%$ServiceName%')";
}
if ($datetype != 0)
{
    $date_field = "";
    if ($datetype == 1)
    {
        $date_field = "a.ServiceDate";
    }
    else if ($datetype == 2)
    {
         $date_field = "a.DateModified";
    }
    if ($date_field != "")
    {
        if ($date_from != "")
        {
            $conds .= " AND $date_field >= '$date_from'";
        }
        if ($date_to != "")
        {
            $conds .= " AND $date_field <= '$date_to 23:59:59'";
        }
    }
}
// IF (a.ServiceDate IS NULL,'-',a.ServiceDate),
$studentnamefield = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT
			a.Year,
			 IF (a.Semester='','". $Lang['General']['WholeYear'] ."',a.Semester) as Semester,
			a.ServiceDate,
			b.ClassName, b.ClassNumber,
			$studentnamefield,
			a.ServiceName,
			a.Role,
			a.Remark,
			a.Performance,
			a.Organization
        FROM 
        	PROFILE_STUDENT_SERVICE as a
        	INNER JOIN 
        	INTRANET_USER b
        	ON a.UserID = b.UserID 
        WHERE 
        			(a.ServiceName like '%$keyword%'
                   OR a.Year like '%$keyword%'
                   OR a.Semester like '%$keyword%'
                   OR a.Role like '%$keyword%'
                   OR a.Performance like '%$keyword%'
                   ) $conds
              ";
$result = $li->returnArray($sql);
// for ($i=0; $i< sizeof($result); $i++) {
// 	for ($j=0;$j < 8; $j++) {
// 		$result[$i][$j] = '"'.$result[$i][$j].'"';
// 	}
// }

// $x = "\"StudentName\",\"Year\",\"Semester\",\"ServiceDate\",\"ServiceName\",\"Role\",\"Performance\",\"Organization\"\n";
$exportColumn = array("Year", "Semester", "Date","Class","Class Number","Student Name","Service Name", "Role", "Performance", "Remark", "Organization");

/*
for ($i=0; $i<sizeof($result); $i++)
{
     $delim = "";
     for ($j=0; $j<sizeof($result[$i]); $j++)
     {
          $x .= "$delim\"".str_replace('"', '""', intranet_undo_htmlspecialchars($result[$i][$j]))."\"";
          $delim = ",";
     }
     $x .= "\n";
}
*/
// Output the file to user browser
$filename = "service_data.csv";
/*
header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($x) );
header("Content-Disposition: attachment; filename=\"".$filename."\"");

echo $x;
*/

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);


intranet_closedb();
?>
