<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

?>

<script language="javascript">
function checkform(obj){
     if(countOption(obj.elements["Fields[]"])==0){ alert(globalAlertMsg18); return false; }
}
</script>

<form name="form1" action="export_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Merit, 'index.php', $button_export, '') ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><?php echo $i_export_msg2; ?>:<br>
<select name=Fields[] size=10 multiple>
<option value=merit SELECTED><?= $i_Merit_Short_Merit ?></option>
<option value=minorC SELECTED><?= $i_Merit_Short_MinorCredit ?></option>
<option value=majorC SELECTED><?= $i_Merit_Short_MajorCredit ?></option>
<option value=superC SELECTED><?= $i_Merit_Short_SuperCredit ?></option>
<option value=ultraC SELECTED><?= $i_Merit_Short_UltraCredit ?></option>
<option value=black SELECTED><?= $i_Merit_Short_BlackMark ?></option>
<option value=minorD SELECTED><?= $i_Merit_Short_MinorDemerit ?></option>
<option value=majorD SELECTED><?= $i_Merit_Short_MajorDemerit ?></option>
<option value=superD SELECTED><?= $i_Merit_Short_SuperDemerit ?></option>
<option value=ultraD SELECTED><?= $i_Merit_Short_UltraDemerit ?></option>
<option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option>
</select>
</blockquote>
</td></tr></table>

<input type=hidden name=start value="<?= $start ?>">
<input type=hidden name=end value="<?= $end ?>">
<input type=hidden name=reason value="<?= $reason ?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_export_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?> 
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

</form>

<?php
include("../../../templates/adminfooter.php");
?>