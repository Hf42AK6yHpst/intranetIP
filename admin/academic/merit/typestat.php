<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libmerit.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

$lword = new libwordtemplates();
$meritReasons = $lword->getWordListMerit();

$lvlArray = array();
$lvlCount = array();

$lmerit = new libmerit();

$now = time();
//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));


if ($start == "" || $end == "")
{
    $start = $yearstart;
    $end = $yearend;
}
$reason = trim($reason);
if ($reason != "")
{

$result = $lmerit->getClassMeritListByReason($start,$end,$reason);

$x = "<table width=200 border=1 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5 cellpadding=2 cellspacing=0>
<tr>
<td width=50% class=tableTitle>$i_UserClassName</td>
<td width=50% class=tableTitle>$i_Merit_Count</td>
</tr>
";
$en_reason = urlencode($reason);
for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$count,$lvlID) = $result[$i];
     if (!in_array($lvlID,$lvlArray))
     {
          $lvlArray[] = $lvlID;
     }
     for ($j=0; $j<sizeof($lvlArray); $j++)
     {
          if ($lvlArray[$j]==$lvlID)
          {
              $lvlCount[$j] += $count;
              break;
          }
     }
     $link = "<a class=functionlink href=typestat_classview.php?classid=$id&start=$start&end=$end&reason=$en_reason>$name</a>";
     $x .= "<tr>
             <td class=tableContent>$link</td>
             <td class=tableContent>$count</td>
            </tr>\n";
}
$x .= "</table>\n";


$lvls = $lmerit->getLevelList();
$y = "<table width=200 border=1 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5 cellpadding=2 cellspacing=0>
<tr>
<td width=50% class=tableTitle>$i_UserClassLevel</td>
<td width=50% class=tableTitle>$i_Merit_Count</td>
</tr>
";
for ($i=0; $i<sizeof($lvlArray); $i++)
{
     $id = $lvlArray[$i];
     $name = $lvls[$id];
     $count = $lvlCount[$i];

     $y .= "<tr>
             <td class=tableContent>$name</td>
             <td class=tableContent>$count</td>
            </tr>\n";
}
$y .= "</table>\n";

if ($datetype == "")
{
    $datetype = 3;
}
$datetype += 0;
$selected[$datetype] = "SELECTED";

$classSelect = "<SELECT name=datetype onChange=\"changeDateType(this.form)\">\n";
$classSelect .= "<OPTION value=0 ".$selected[0].">$i_Profile_Today</OPTION>\n";
$classSelect .= "<OPTION value=1 ".$selected[1].">$i_Profile_ThisWeek</OPTION>\n";
$classSelect .= "<OPTION value=2 ".$selected[2].">$i_Profile_ThisMonth</OPTION>\n";
$classSelect .= "<OPTION value=3 ".$selected[3].">$i_Profile_ThisAcademicYear</OPTION>\n";
$classSelect .= "</SELECT><br>\n";
$functionbar = "$classSelect $i_Profile_From <input type=text name=start size=10 value='$start'> $i_Profile_To";
$functionbar .= "<input type=text name=end size=10 value='$end'> (YYYY-MM-DD)<br><input type=submit class=submit value='$button_submit'><br>";
}

$reasonSelect = getSelectByValue($meritReasons,"name=reason onChange=this.form.submit()",$reason);
$functionbar .= "$i_Profile_SelectReason: $reasonSelect";
?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function changeDateType(obj)
{
         switch (obj.datetype.value)
         {
                 case '0':
                      obj.start.value = '<?=$today?>';
                      obj.end.value = '<?=$today?>';
                      break;
                 case '1':
                      obj.start.value = '<?=$weekstart?>';
                      obj.end.value = '<?=$weekend?>';
                      break;
                 case '2':
                      obj.start.value = '<?=$monthstart?>';
                      obj.end.value = '<?=$monthend?>';
                      break;
                 case '3':
                      obj.start.value = '<?=$yearstart?>';
                      obj.end.value = '<?=$yearend?>';
                      break;
         }
         obj.submit();
}
</SCRIPT>
<form name=form1 method=get>
<p class=admin_head><?php echo $i_adminmenu_adm.displayArrow()."<a href=/admin/academic/>$i_adminmenu_adm_academic_record</a>".displayArrow()."<a href=index.php>$i_Profile_Merit</a>".displayArrow().$i_Profile_ByReason; ?></p>
<table width=560 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src=../../../images/admin/group_links/group_links_<?php echo $intranet_session_language; ?>.gif width=170 height=35 border=0></td></tr>
<tr><td class=admin_bg_menu><img src=../../images/space.gif width=1 height=4 border=0></td></tr>
<tr><td class=admin_bg_menu><?php //echo $li->displayFunctionbar("-", "-", "", $functionbar);
echo $functionbar;
?></td></tr>
<tr><td class=admin_bg_menu></td></tr>
<tr><td class=tableContent><?=$x?>&nbsp;</td></tr>
<tr><td class=tableContent><br><br>&nbsp;</td></tr>
<tr><td class=tableContent><?=$y?>&nbsp;</td></tr>
<tr><td><img src=../../../images/admin/menu_bottom.gif width=560 height=8 border=0></td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");

?>