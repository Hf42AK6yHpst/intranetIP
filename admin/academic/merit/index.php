<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libmerit.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libstudentprofile.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

$lvlArray = array();
$lvlMerit = array();
$lvlMinorC = array();
$lvlMajorC = array();
$lvlSuperC = array();
$lvlUltraC = array();
$lvlBlack = array();
$lvlMinorD = array();
$lvlMajorD = array();
$lvlSuperD = array();
$lvlUltraD = array();

$lmerit = new libmerit();
$lword = new libwordtemplates();
$lsp = new libstudentprofile();

$meritReasons = array_merge($lword->getWordListMerit(), $lword->getWordListDemerit());

$now = time();
//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));


if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}

$result = $lmerit->getClassMeritList($start,$end,$reason);

$x = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td class=tableTitle rowspan=2>$i_UserClassName</td>
<td class=tableTitle align=center colspan=".$lsp->col_merit.">$i_Merit_Award</td>
<td class=tableTitle align=center colspan=".$lsp->col_demerit.">$i_Merit_Punishment</td>
</tr>\n";
$x .= "<tr>\n";
if (!$lsp->is_merit_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_Merit</td>\n";
}
if (!$lsp->is_min_merit_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_MinorCredit</td>\n";
}
if (!$lsp->is_maj_merit_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_MajorCredit</td>\n";
}
if (!$lsp->is_sup_merit_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_SuperCredit</td>\n";
}
if (!$lsp->is_ult_merit_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_UltraCredit</td>\n";
}
if (!$lsp->is_black_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_BlackMark</td>\n";
}
if (!$lsp->is_min_demer_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_MinorDemerit</td>\n";
}
if (!$lsp->is_maj_demer_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_MajorDemerit</td>\n";
}
if (!$lsp->is_sup_demer_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_SuperDemerit</td>\n";
}
if (!$lsp->is_ult_demer_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_UltraDemerit</td>\n";
}
$x .= "</tr>\n";

$en_reason = urlencode($reason);

for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$lvlID,$merit,$minorC,$majorC,$superC,$ultraC,$black,$minorD,$majorD,$superD,$ultraD) = $result[$i];
     if (!in_array($lvlID,$lvlArray))
     {
          $lvlArray[] = $lvlID;
     }
     for ($j=0; $j<sizeof($lvlArray); $j++)
     {
          if ($lvlArray[$j]==$lvlID)
          {
              $lvlMerit[$j] += $merit;
              $lvlMinorC[$j] += $minorC;
              $lvlMajorC[$j] += $majorC;
              $lvlSuperC[$j] += $superC;
              $lvlUltraC[$j] += $ultraC;
              $lvlBlack[$j] += $black;
              $lvlMinorD[$j] += $minorD;
              $lvlMajorD[$j] += $majorD;
              $lvlSuperD[$j] += $superD;
              $lvlUltraD[$j] += $ultraD;
              break;
          }
     }
     
     $link = "<a class=functionlink href=classview.php?classid=$id&start=$start&end=$end&reason=$en_reason&datetype=$datetype>$name</a>";
     $css = ($i%2) ? "2" : "";
     $x .= "<tr>\n<td class=tableContent$css>$link</td>\n";

     if (!$lsp->is_merit_disabled)
     {
          $x .= "<td class=tableContent$css>$merit</td>\n";
     }
     if (!$lsp->is_min_merit_disabled)
     {
          $x .= "<td class=tableContent$css>$minorC</td>\n";
     }
     if (!$lsp->is_maj_merit_disabled)
     {
          $x .= "<td class=tableContent$css>$majorC</td>\n";
     }
     if (!$lsp->is_sup_merit_disabled)
     {
          $x .= "<td class=tableContent$css>$superC</td>\n";
     }
     if (!$lsp->is_ult_merit_disabled)
     {
          $x .= "<td class=tableContent$css>$ultraC</td>\n";
     }
     if (!$lsp->is_black_disabled)
     {
          $x .= "<td class=tableContent$css>$black</td>\n";
     }
     if (!$lsp->is_min_demer_disabled)
     {
          $x .= "<td class=tableContent$css>$minorD</td>\n";
     }
     if (!$lsp->is_maj_demer_disabled)
     {
          $x .= "<td class=tableContent$css>$majorD</td>\n";
     }
     if (!$lsp->is_sup_demer_disabled)
     {
          $x .= "<td class=tableContent$css>$superD</td>\n";
     }
     if (!$lsp->is_ult_demer_disabled)
     {
          $x .= "<td class=tableContent$css>$ultraD</td>\n";
     }
     $x .= "</tr>\n";
}
$x .= "</table>\n";


$lvls = $lmerit->getLevelList();
$y = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td class=tableTitle rowspan=2>$i_UserClassLevel</td>
<td class=tableTitle align=center colspan=".$lsp->col_merit.">$i_Merit_Award</td>
<td class=tableTitle align=center colspan=".$lsp->col_demerit.">$i_Merit_Punishment</td>
</tr>
<tr>\n";

if (!$lsp->is_merit_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_Merit</td>\n";
}
if (!$lsp->is_min_merit_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_MinorCredit</td>\n";
}
if (!$lsp->is_maj_merit_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_MajorCredit</td>\n";
}
if (!$lsp->is_sup_merit_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_SuperCredit</td>\n";
}
if (!$lsp->is_ult_merit_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_UltraCredit</td>\n";
}
if (!$lsp->is_black_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_BlackMark</td>\n";
}
if (!$lsp->is_min_demer_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_MinorDemerit</td>\n";
}
if (!$lsp->is_maj_demer_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_MajorDemerit</td>\n";
}
if (!$lsp->is_sup_demer_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_SuperDemerit</td>\n";
}
if (!$lsp->is_ult_demer_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_UltraDemerit</td>\n";
}
$y .= "</tr>";

for ($i=0; $i<sizeof($lvlArray); $i++)
{
     $id = $lvlArray[$i];
     $name = $lvls[$id];
     $merit = $lvlMerit[$i];
     $minorC = $lvlMinorC[$i];
     $majorC = $lvlMajorC[$i];
     $superC = $lvlSuperC[$i];
     $ultraC = $lvlUltraC[$i];
     $black = $lvlBlack[$i];
     $minorD = $lvlMinorD[$i];
     $majorD = $lvlMajorD[$i];
     $superD = $lvlSuperD[$i];
     $ultraD = $lvlUltraD[$i];

         $css = ($i%2) ? "2" : "";
     $y .= "<tr>
             <td class=tableContent$css>$name</td>\n";

if (!$lsp->is_merit_disabled)
{
     $y .= "<td class=tableContent$css>$merit</td>\n";
}
if (!$lsp->is_min_merit_disabled)
{
     $y .= "<td class=tableContent$css>$minorC</td>\n";
}
if (!$lsp->is_maj_merit_disabled)
{
     $y .= "<td class=tableContent$css>$majorC</td>\n";
}
if (!$lsp->is_sup_merit_disabled)
{
     $y .= "<td class=tableContent$css>$superC</td>\n";
}
if (!$lsp->is_ult_merit_disabled)
{
     $y .= "<td class=tableContent$css>$ultraC</td>\n";
}
if (!$lsp->is_black_disabled)
{
     $y .= "<td class=tableContent$css>$black</td>\n";
}
if (!$lsp->is_min_demer_disabled)
{
     $y .= "<td class=tableContent$css>$minorD</td>\n";
}
if (!$lsp->is_maj_demer_disabled)
{
     $y .= "<td class=tableContent$css>$majorD</td>\n";
}
if (!$lsp->is_sup_demer_disabled)
{
     $y .= "<td class=tableContent$css>$superD</td>\n";
}
if (!$lsp->is_ult_demer_disabled)
{
     $y .= "<td class=tableContent$css>$ultraD</td>\n";
}
     $y .= "</tr>\n";
}
$y .= "</table>\n";

$datetype += 0;
$selected[$datetype] = "SELECTED";

$classSelect = "<SELECT name=datetype onChange=\"changeDateType(this.form)\">\n";
$classSelect .= "<OPTION value=0 ".$selected[0].">$i_Profile_Today</OPTION>\n";
$classSelect .= "<OPTION value=1 ".$selected[1].">$i_Profile_ThisWeek</OPTION>\n";
$classSelect .= "<OPTION value=2 ".$selected[2].">$i_Profile_ThisMonth</OPTION>\n";
$classSelect .= "<OPTION value=3 ".$selected[3].">$i_Profile_ThisAcademicYear</OPTION>\n";
$classSelect .= "</SELECT>\n";

$functionbar = "<table border=0>\n";
$functionbar .= "<tr><td nowrap>$i_Profile_SelectSemester:</td><td>$classSelect<br>$i_Profile_From <input type=text name=start size=10 value='$start'> $i_Profile_To ";
$functionbar .= "<input type=text name=end size=10 value='$end'> <span class=extraInfo>(yyyy-mm-dd) </span> <a href='javascript:document.form1.submit()'><img src='/images/admin/button/s_btn_submit_$intranet_session_language.gif' border='0' align='absmiddle'></a></td></tr>";
$reasonSelect = getSelectByValue($meritReasons,"name=reason onChange=this.form.submit()",$reason,1);
$functionbar .= "<tr><td nowrap>$i_Profile_SelectReason:</td><td>$reasonSelect</td></tr>\n";
$functionbar .= "</table>\n";


?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function changeDateType(obj)
{
         switch (obj.datetype.value)
         {
                 case '0':
                      obj.start.value = '<?=$today?>';
                      obj.end.value = '<?=$today?>';
                      break;
                 case '1':
                      obj.start.value = '<?=$weekstart?>';
                      obj.end.value = '<?=$weekend?>';
                      break;
                 case '2':
                      obj.start.value = '<?=$monthstart?>';
                      obj.end.value = '<?=$monthend?>';
                      break;
                 case '3':
                      obj.start.value = '<?=$yearstart?>';
                      obj.end.value = '<?=$yearend?>';
                      break;
         }
         obj.submit();
}
function openPrintPage()
{
        newWindow("indexprint.php?start=<?=$start?>&end=<?=$end?>&reason=<?=$en_reason?>",4);
}
</SCRIPT>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Merit, '') ?>
<?= displayTag("head_merit_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $functionbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu>
<a class=iconLink href="import.php"><?=importIcon().$button_import?></a>
<a class=iconLink href="javascript:checkGet(document.form1,'export.php')"><?=exportIcon().$button_export?></a>
<a class=iconLink href="javascript:openPrintPage()"><?=printIcon().$i_PrinterFriendlyPage?></a>
<a class=iconLink href="detail.php"><?=detailIcon().$i_general_ViewDetailRecords?></a>
</td></tr>
<tr><td class=admin_bg_menu></td></tr>
<tr><td class=tableContent>
<?=$x?>
</td></tr>
<tr><td><br><br></td></tr>
<tr><td class=tableContent>
<?=$y?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");

?>