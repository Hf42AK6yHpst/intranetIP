<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libmerit.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libstudentprofile.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

function returnJSarray($arr_tmp, $js_name){
        if (sizeof($arr_tmp)<1)
                return "var $js_name = new Array();\n";
        $x = "var $js_name = new Array(";
        for ($i=0; $i<sizeof($arr_tmp); $i++)
        {
                $x .= ($i==0) ? "" : ",";
                $x .= "\"".$arr_tmp[$i]."\"";
        }
        $x .= ");\n";

        return $x;
}

$StudentMeritID = (is_array($StudentMeritID)? $StudentMeritID[0]: $StudentMeritID);

$lmerit = new libmerit($StudentMeritID);
$studentid = $lmerit->UserID;
$lu = new libuser($studentid);
$lword = new libwordtemplates();
$lsp = new libstudentprofile();
$reasonList = $lword->getSelectMerit("name=wordTemp onChange=\"this.form.reason.value=this.value\"", $lmerit->RecordType);
$word_arr_merit = $lword->getWordListMerit();
$word_arr_demerit = $lword->getWordListDemerit();
$typeSelect = $lsp->getSelectMeritType("name=type onChange=\"updateWordTemp(this.selectedIndex)\"",$lmerit->RecordType);
$name = $lu->UserNameLang();

$pre_word_pos = ($lmerit->RecordType<0) ? 5 : 0;

$select_sem = getSelectSemester("name=semester",$lmerit->Semester);
$button_select = $i_notapplicable;
$teacherSelect = $lu->getSelectTeacherStaff("name=PersonInCharge",$lmerit->PersonInCharge);

?>

<script language="javascript">
function checkform(obj){
         if(!check_positive_int(obj.qty,"<?=$i_Merit_QtyMustBeInteger?>")) return false;
         if(!check_date(obj.meritdate,"<?="$i_alert_pleasefillin$i_Merit_Date"?>")) return false;
         if(!check_text(obj.year,"<?="$i_alert_pleasefillin$i_Profile_Year"?>")) return false;
         return true;
}

<?=returnJSarray($word_arr_merit, "js_merit")?>
<?=returnJSarray($word_arr_demerit, "js_demerit")?>

var pre_word_pos = <?=$pre_word_pos?>;
function updateWordTemp(selIndex){
                if (js_merit.length==0 && js_demerit.length==0)
                        return;

                if (selIndex><?=$lsp->col_merit-1?> && pre_word_pos<=<?=$lsp->col_merit-1?>)
                {
                        updateList(js_demerit, js_merit);
                }
                else if (selIndex<=<?=$lsp->col_merit-1?> && pre_word_pos><?=$lsp->col_merit-1?>)
                {
                        updateList(js_merit, js_demerit);
                }
                pre_word_pos = selIndex;
                return;
}

function updateList(newArr, oldArr){
                var objTar = document.form1.wordTemp;

                // empty reason field if the reason is from previous merit/derit list
                for (var i=0; i<oldArr.length; i++)
                {
                        if (document.form1.reason.value == oldArr[i])
                        {
                                document.form1.reason.value = "";
                                break;
                        }
                }

                // replace reason list
                objTar.length = 0;
                objTar.options[0] = new Option ("-- <?=$button_select?> --", "");
                for (i=0; i<newArr.length; i++)
                {
                        objTar.options[i+1] = new Option (newArr[i], newArr[i]);
                }

                return;
}
</script>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Merit, 'index.php', $lu->ClassName, "classview.php?classid=$classid", $name, 'javascript:history.back()', $button_edit, '') ?>
<?= displayTag("head_merit_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right <?=($intranet_session_language=="en"?"":"nowrap")?>><?php echo $i_UserLogin; ?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?php echo $i_UserEnglishName; ?>:</td><td><?=$lu->EnglishName?></td></tr>
<tr><td align=right><?php echo $i_UserChineseName; ?>:</td><td><?=$lu->ChineseName?></td></tr>
<tr><td align=right><?php echo $i_UserClassName; ?>:</td><td><?=$lu->ClassName?></td></tr>
<tr><td align=right><?php echo $i_UserClassNumber; ?>:</td><td><?=$lu->ClassNumber?></td></tr>
<!--
<tr><td align=right><?php echo $i_Profile_Year; ?>:</td><td><input type=text name=year value='<?=$lmerit->Year?>'></td></tr>
<tr><td align=right><?php echo $i_Profile_Semester; ?>:</td><td><?=$select_sem?></td></tr>
//-->
<tr><td align=right><?php echo $i_Merit_Date; ?>:</td><td><input type=text size=10 maxlength=10 name=meritdate value='<?=$lmerit->MeritDate?>'> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td align=right><?php echo $i_Merit_Type; ?>:</td><td><?=$typeSelect?></td></tr>
<tr><td align=right><?php echo $i_Merit_Qty; ?>:</td><td><input type=text name=qty size=2 value='<?=$lmerit->Number?>'></td></tr>
<tr><td align=right><?php echo $i_Profile_PersonInCharge; ?>:</td><td><?=$teacherSelect?></td></tr>
<tr><td align=right><?php echo $i_Merit_Reason; ?>:</td><td><input type=text size=30 name=reason value='<?=$lmerit->Reason?>'><?=$reasonList?></td></tr>
<tr><td align=right><?php echo $i_Merit_Remark; ?>:</td><td><textarea name=remark cols=30 rows=5><?=$lmerit->Remark?></textarea></td></tr>
</table>
</blockquote>
<input type=hidden name=studentid value=<?=$studentid?>>
<input type=hidden name=StudentMeritID value=<?=$StudentMeritID?>>
<input type=hidden name=classid value=<?=$classid?>>
<input type=hidden name=page_from value="<?=$page_from?>">
<input type=hidden name=class value="<?=$class?>">
<input type=hidden name=yrfilter value="<?=$yrfilter?>">
<input type=hidden name=pic value="<?=$pic?>">
<input type=hidden name=prev_reason value="<?=$reason?>">
<input type=hidden name=datetype value="<?=$datetype?>">
<input type=hidden name=date_from value="<?=$date_from?>">
<input type=hidden name=date_to value="<?=$date_to?>">
<input type=hidden name=pageNo value="<?=$pageNo?>">
<input type=hidden name=order value="<?=$order?>">
<input type=hidden name=field value="<?=$field?>">


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include_once("../../../templates/adminfooter.php");
?>