<?php

################################################
#
#	Date:	2016-07-20 (Bill)
#			Replace deprecated split() by explode() for PHP 5.4
#
################################################

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libattendance.php");
include_once("../../includes/libmerit.php");
include_once("../../includes/libservice.php");
include_once("../../includes/libactivity.php");
include_once("../../includes/libaward.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");
intranet_opendb();
?>
<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE>
<?

# Parameters of page
//Default value of number of line in each page
$defaultNumOfLine = 40;

//Default width of a specific field
$defaultFieldWidth1 = 53;

//current number of line remaining
$lineRemain = $defaultNumOfLine;

//how many line need
$fieldLineNeed = 1;

// function for getting student ID & name
function getStudentID($YearOfLeft, $selectedClass){
        global $button_select;
                  global $intranet_session_language;
                   $li = new libdb();

                if ( $selectedClass ){
                        $sql = "SELECT UserID, ";
                                                       if ( $intranet_session_language=="en") $sql.= "EnglishName, ClassNumber, ClassName ";
                                                       if ( $intranet_session_language=="b5") $sql.= "ChineseName, ClassNumber, ClassName ";
                        $sql .=        "FROM INTRANET_ARCHIVE_USER
                                                                WHERE YearOfLeft=\"$YearOfLeft\" AND ClassName=\"$selectedClass\"
                                                                ORDER BY ClassName ASC, ClassNumber ASC";
                }
                else{
                        $sql = "SELECT UserID, ";
                                                       if ( $intranet_session_language=="en") $sql.= "EnglishName, ClassNumber, ClassName ";
                                                       if ( $intranet_session_language=="b5") $sql.= "ChineseName, ClassNumber, ClassName ";
                        $sql .=        "FROM INTRANET_ARCHIVE_USER
                                                                WHERE YearOfLeft=\"$YearOfLeft\"
                                                                ORDER BY ClassName ASC, ClassNumber ASC";
                }

             return $li->returnArray($sql, 4);
}

// get school information (name + badge)
$li = new libfilesystem();
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$schoolbadge = ($imgfile!="") ? "<img src=/file/$imgfile width=120 height=60><br>\n" : "&nbsp;";

//$school_data = split("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? $school_data[0] : "&nbsp;";

if ($schoolbadge!="&nbsp;" || $school_name!="&nbsp;")
{
        $school_info = "<table border=0 width=650 height=60>\n";
        $school_info .= "<tr><td align=center>$schoolbadge <font size=+0><b>$school_name</b></font></td></tr>\n";
        $school_info .= "</table>\n<br>\n";
}

# Page layout details
$page_breaker = "<P CLASS='breakhere'>";
$page_header_linefeed = 6;
$intermediate_pageheader = "<P CLASS='breakhere'>";
$intermediate_linefeed = 5;

$li_menu = new libaccount();
$li_menu->is_access_function($PHP_AUTH_USER);
$lstudentprofile = libstudentprofile();
$field_disabled_array = array(0,$lstudentprofile->is_merit_disabled,$lstudentprofile->is_min_merit_disabled,
        $lstudentprofile->is_maj_merit_disabled, $lstudentprofile->is_sup_merit_disabled,
        $lstudentprofile->is_ult_merit_disabled, $lstudentprofile->is_black_disabled,
        $lstudentprofile->is_min_demer_disabled, $lstudentprofile->is_maj_demer_disabled,
        $lstudentprofile->is_sup_demer_disabled, $lstudentprofile->is_ult_demer_disabled);

# display all years
$year_display = "";

$data = array();

# Grep all students information
$lclass = new libclass();

$students = getStudentID($generalYear, $class);

$lattend = new libattendance();
$lmerit = new libmerit();
$lact = new libactivity();
$lservice = new libservice();
$laward = new libaward();

    for ($iter=0; $iter<sizeof($students); $iter++)
    {
         list ($studentID, $studentName, $studentClassnumber, $studentClassName) = $students[$iter];
         if ($class != "") $studentClassName = $class;
         # Attendance data
         if ($li_menu->is_access_attendance && !$lstudentprofile->is_printpage_attendance_hidden)
         {
             $dataAttendance = $lattend->returnArchiveStudentRecordByYear($studentID,"","");
         }
         # Merit data
         if ($li_menu->is_access_merit && !$lstudentprofile->is_printpage_merit_hidden)
         {
             $dataMerit = $lmerit->returnArchiveStudentRecordByYear($studentID,"","");
         }
         # Service Data
         if ($li_menu->is_access_service && !$lstudentprofile->is_printpage_service_hidden)
         {
             $dataService = $lservice->getArchiveServiceByStudent($studentID,"","");
         }
         # Activity Data
         if ($li_menu->is_access_activity && !$lstudentprofile->is_printpage_activity_hidden)
         {
             $dataActivity = $lact->getArchiveActivityByStudent($studentID,"","");
         }
         # Award Data
         if ($li_menu->is_access_award && !$lstudentprofile->is_printpage_award_hidden)
         {
             $dataAward = $laward->getArchiveAwardByStudent($studentID,"","");
         }
         $studentInfo = array($studentID,$studentName,$studentClassnumber,$dataAttendance,$dataMerit,$dataService,$dataActivity,$dataAward);
         #$data[] = $studentInfo;

         # Print page construct
        #$lineRemain=$lineRemain - 5 ;
/*
        if ($iter==0){
                echo "<P CLASS='breakhere'>";
                $lineRemain = $defaultNumOfLine - 5;
        }
        */
        $lineRemain = $defaultNumOfLine - $page_header_linefeed;
         echo "$school_info\n";
         echo "<table width=560 border=0 cellpadding=0 cellspacing=0>\n";
         ###################################
         echo "<tr><td>\n";

         if ($generalYear != "")
         {
             echo "$i_Profile_DataLeftYear: $generalYear $generalSem<br>\n";
         }
         echo "$i_Profile_ClassOfFinalYear: $studentClassName<br>\n$i_PrinterFriendly_StudentName: $studentName ($studentClassName - $studentClassnumber)<br><br>\n";
         echo "</td></tr>\n";
         echo "<tr><td><br></td></tr>
                <tr><td class=tableContent height=300 align=left>\n";

        # $x store text in 1 student
        $x = "";
        $x .= "$i_Profile_ClassHistory";
        $classtable = $lclass->displayLeftClassHistoryAdmin($studentID);
        $x .= $classtable;
        $x .= "<br>";
        $lineRemain -= 5;

         # Attendance
         if ($li_menu->is_access_attendance && !$lstudentprofile->is_printpage_attendance_hidden)
         {

        for ($j=0; $j<sizeof($dataAttendance); $j++){

                list($dataAttendanceYear, $dataAttendanceAbs, $dataAttendanceLate, $dataAttendanceEarly)
                        = $dataAttendance[$j];

                $currentLineNeed = ceil ( strlen($dataAttendanceYear . $dataAttendanceAbs . $dataAttendanceLate . $dataAttendanceEarly ) / $defaultFieldWidth1 );

                if ($fieldLineNeed < $currentLineNeed)
                        $fieldLineNeed = $currentLineNeed;

        }
        $lineRemain=$lineRemain - $fieldLineNeed*sizeof($dataAttendance) ;

        if ($lineRemain <0){
                $x .= "$intermediate_pageheader";
                $lineRemain = $defaultNumOfLine - $fieldLineNeed*sizeof($dataAttendance);
        }
        $x .= $i_Profile_Attendance;
        $x .= "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
        $title_array = array($i_Attendance_Year,$i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave);
        $size = sizeof($dataAttendance[0]) + 1;
        $width = 100/$size;
        # ================== Changes from here
        $x .= "<tr>\n";
        for ($i=0; $i<sizeof($title_array); $i++)
        {
             $x .= "<td class=tableTitle_new>".$title_array[$i]."</td>";
        }
        $x .= "</tr>\n";
        if (sizeof($dataAttendance)==0)
        {
            $x .= "<tr><td colspan=4 align=center>$i_no_record_exists_msg</td></tr>\n";
        }

        for ($i=0; $i<sizeof($dataAttendance); $i++)
        {
             $x .= "<tr>\n";
             for ($j=0; $j<sizeof($dataAttendance[$i]); $j++)
             {
                  $data = $dataAttendance[$i][$j];
                  $x .= "<td>$data</td>";
             }
             $x .= "</tr>\n";
        }
        $x .= "</table>\n";
        # End of attendance

        $x .= "<br>\n";
        $lineRemain--;
        }

        # Merit
        if ($li_menu->is_access_merit && !$lstudentprofile->is_printpage_merit_hidden)
        {

        for ($j=0; $j<sizeof($dataMerit); $j++){
/*
                list($dataMeritYear, $dataAttendanceAbs, $dataAttendanceLate, $dataAttendanceEarly)
                        = $dataAttendance[$j];
*/
                $length = 0;
                for ($pos=0; $pos < sizeof($dataMerit[$j]); $pos++)
                {
                     $length += strlen($dataMerit[$j][$pos]);
                }
                $currentLineNeed = ceil ( $length / $defaultFieldWidth1 );

                if ($fieldLineNeed < $currentLineNeed)
                        $fieldLineNeed = $currentLineNeed;

        }
        $lineRemain=$lineRemain - $fieldLineNeed*sizeof($dataMerit) ;

        if ($lineRemain <0){
                $x .= "$intermediate_pageheader";
                $lineRemain = $defaultNumOfLine - $fieldLineNeed*sizeof($dataMerit);
        }

        $x .= $i_Profile_Merit;
        $x .= "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
        $title_array = array($i_Profile_Year);
        $title_array = array_merge($title_array,$i_Merit_TypeArray);
        $size = sizeof($dataMerit[0]) + 1;
        # $width = 100/$size;

        # ================== Changes from here
        $x .= "<tr>\n";
        for ($i=0; $i<sizeof($title_array); $i++)
        {
             if ($merit_col_not_display[$i]==1) continue;
             if (!$field_disabled_array[$i])
             {
                  $x .= "<td class=tableTitle_new>".$title_array[$i]."</td>";
             }
        }
        $x .= "</tr>\n";
        if (sizeof($dataMerit)==0)
        {
            $x .= "<tr><td colspan=11 align=center>$i_no_record_exists_msg</td></tr>\n";
        }
//        $x .= "</tr>\n";
        for ($i=0; $i<sizeof($dataMerit); $i++)
        {
             $x .= "<tr>\n";
             for ($j=0; $j<sizeof($dataMerit[$i]); $j++)
             {
                  if ($merit_col_not_display[$j]==1) continue;
                  if (!$field_disabled_array[$j])
                  {
                       $data = $dataMerit[$i][$j];
                       $x .= "<td>$data</td>";
                  }
             }
             $x .= "</tr>\n";
        }
        $x .= "</table>\n";

        # End of Merit

        $x .= "<br>\n";
        $lineRemain--;
        }

        # Service
        if ($li_menu->is_access_service && !$lstudentprofile->is_printpage_service_hidden)
        {
        for ($j=0; $j<sizeof($dataService); $j++){
                $length = 0;
                for ($pos=0; $pos < sizeof($dataService[$j]); $pos++)
                {
                     $length += strlen($dataService[$j][$pos]);
                }
                $currentLineNeed = ceil ( $length / $defaultFieldWidth1 );

                if ($fieldLineNeed < $currentLineNeed)
                        $fieldLineNeed = $currentLineNeed;

        }
        $lineRemain=$lineRemain - $fieldLineNeed*sizeof($dataService) ;

        if ($lineRemain <0){
                $x .= "$intermediate_pageheader";
                $lineRemain = $defaultNumOfLine - $fieldLineNeed*sizeof($dataService);
        }
        $x .= $i_Profile_Service;

                $x .= "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_ServiceYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_ServiceSemester</td>
                      <td width=90 align=center class=tableTitle_new>$i_ServiceDate</td>
                      <td width=110 align=center class=tableTitle_new>$i_ServiceName</td>
                      <td width=90 align=center class=tableTitle_new>$i_ServiceRole</td>
                      <td width=90 align=center class=tableTitle_new>$i_ServicePerformance</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($dataService); $i++)
                {
                     list($year,$sem,$sDate,$service,$role,$performance) = $dataService[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$sDate</td>
                              <td align=center>$service</td>
                              <td align=center>$role&nbsp;</td>
                              <td align=center>$performance&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($dataService)==0)
                {
                    $x .= "<tr><td colspan=6 align=center>$i_ServiceNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";


        # End of Service

        $x .= "<br>\n";
        $lineRemain--;
        }

        # Activity
        if ($li_menu->is_access_activity && !$lstudentprofile->is_printpage_activity_hidden)
        {
        for ($j=0; $j<sizeof($dataActivity); $j++){
                $length = 0;
                for ($pos=0; $pos < sizeof($dataActivity[$j]); $pos++)
                {
                     $length += strlen($dataActivity[$j][$pos]);
                }
                $currentLineNeed = ceil ( $length / $defaultFieldWidth1 );

                if ($fieldLineNeed < $currentLineNeed)
                        $fieldLineNeed = $currentLineNeed;

        }
        $lineRemain=$lineRemain - $fieldLineNeed*sizeof($dataActivity) ;

        if ($lineRemain <0){
                $x .= "$intermediate_pageheader";
                $lineRemain = $defaultNumOfLine - $fieldLineNeed*sizeof($dataActivity);
        }

        $x .= $i_Profile_Activity;
                $x .= "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_ActivityYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_ActivitySemester</td>
                      <td width=150 align=center class=tableTitle_new>$i_ActivityName</td>
                      <td width=110 align=center class=tableTitle_new>$i_ActivityRole</td>
                      <td width=120 align=center class=tableTitle_new>$i_ActivityPerformance</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($dataActivity); $i++)
                {
                     list($year,$sem,$actName,$role,$performance) = $dataActivity[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$actName</td>
                              <td align=center>$role&nbsp;</td>
                              <td align=center>$performance&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($dataActivity)==0)
                {
                    $x .= "<tr><td colspan=5 align=center>$i_ActivityNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";

        # End of Activity

        $x .= "<br>\n";
        $lineRemain--;
        }

        # Award
        if ($li_menu->is_access_award && !$lstudentprofile->is_printpage_award_hidden)
        {
        for ($j=0; $j<sizeof($dataAward); $j++){
                $length = 0;
                for ($pos=0; $pos < sizeof($dataAward[$j]); $pos++)
                {
                     $length += strlen($dataAward[$j][$pos]);
                }
                $currentLineNeed = ceil ( $length / $defaultFieldWidth1 );

                if ($fieldLineNeed < $currentLineNeed)
                        $fieldLineNeed = $currentLineNeed;

        }
        $lineRemain=$lineRemain - $fieldLineNeed*sizeof($dataAward) ;

        if ($lineRemain <0){
                $x .= "$intermediate_pageheader";
                $lineRemain = $defaultNumOfLine - $fieldLineNeed*sizeof($dataAward);
        }

        $x .= $i_Profile_Award;
                $x .= "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_AwardYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_AwardSemester</td>
                      <td width=90 align=center class=tableTitle_new>$i_AwardDate</td>
                      <td width=170 align=center class=tableTitle_new>$i_AwardName</td>
                      <td width=120 align=center class=tableTitle_new>$i_AwardRemark</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($dataAward); $i++)
                {
                     list($year,$sem,$aDate,$award,$remark) = $dataAward[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$aDate</td>
                              <td align=center>$award</td>
                              <td align=center>$remark&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($dataAward)==0)
                {
                    $x .= "<tr><td colspan=5 align=center>$i_AwardNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";
        # End of Award
        }

        echo $x;
        echo "</td></tr></table>";
        if ($iter < sizeof($students)-1)
            echo "$page_breaker";
    }


#print_r($data);
intranet_closedb();

include_once("../../templates/filefooter.php");

?>
