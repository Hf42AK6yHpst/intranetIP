<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libmerit.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$lu = new libuser($studentid);

# TABLE SQL
$keyword = trim($keyword);
if($field=="") $field = 0;
if($order=="") $order = 1;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     case 4: $field = 4; break;
     case 5: $field = 5; break;
     case 6: $field = 6; break;
     case 7: $field = 7; break;
     case 8: $field = 8; break;
     default: $field = 0; break;
}
$conds = "";
if ($yrfilter != "")
{
    $conds .= " AND a.Year = '$yrfilter'";
}
/*
if ($semester != "")
{
    $conds .= " AND a.Semester = '$semester'";
}
*/
if ($class != "")
{
    $conds .= " AND b.ClassName = '$class'";
}
if ($pic != "")
{
    $conds .= " AND a.PersonInCharge = '$pic'";
}
if ($RecordType != "")
{
    $conds .= " AND a.RecordType = '$RecordType'";
}
if ($reason != "")
{
    $conds .= " AND a.Reason = '$reason'";
}
if ($datetype != 0)
{
    $date_field = "";
    if ($datetype == 1)
    {
        $date_field = "a.MeritDate";
    }
    else if ($datetype == 2)
    {
         $date_field = "a.DateModified";
    }
    if ($date_field != "")
    {
        if ($date_from != "")
        {
            $conds .= " AND $date_field >= '$date_from'";
        }
        if ($date_to != "")
        {
            $conds .= " AND $date_field <= '$date_to 23:59:59'";
        }
    }
}

$studentnamefield = getNameFieldWithClassNumberByLang("b.");
$namefield = getNameFieldWithLoginByLang("c.");
$sql = "SELECT
              DATE_FORMAT(a.MeritDate,'%Y-%m-%d'),
              $studentnamefield,
              a.Year,a.Semester,
              CASE a.RecordType
                   WHEN 1 THEN '$i_Merit_Merit'
                   WHEN 2 THEN '$i_Merit_MinorCredit_unicode'
                   WHEN 3 THEN '$i_Merit_MajorCredit_unicode'
                   WHEN 4 THEN '$i_Merit_SuperCredit_unicode'
                   WHEN 5 THEN '$i_Merit_UltraCredit_unicode'
                   WHEN -1 THEN '$i_Merit_BlackMark'
                   WHEN -2 THEN '$i_Merit_MinorDemerit'
                   WHEN -3 THEN '$i_Merit_MajorDemerit'
                   WHEN -4 THEN '$i_Merit_SuperDemerit'
                   WHEN -5 THEN '$i_Merit_UltraDemerit'
                   ELSE '-' END,
              a.NumberOfUnit,
              a.Reason,
              IF(a.PersonInCharge IS NULL OR a.PersonInCharge=0,'--',$namefield),
              a.DateModified,
              CONCAT('<input type=checkbox name=StudentMeritID[] value=', a.StudentMeritID ,'>')
        FROM PROFILE_STUDENT_MERIT as a
                 LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                 LEFT OUTER JOIN INTRANET_USER as c ON a.PersonInCharge = c.UserID
        WHERE b.UserID IS NOT NULL AND (a.MeritDate like '%$keyword%'
                   OR a.Reason like '%$keyword%'
                   OR b.EnglishName like '%$keyword%'
                   OR b.ChineseName like '%$keyword%'
                   OR b.UserLogin like '%$keyword%'
                   OR b.ClassNumber like '%$keyword%'
                   ) $conds
              ";
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.MeritDate","b.EnglishName","a.Year","a.Semester","a.RecordType", "a.NumberOfUnit","a.Reason","c.EnglishName","a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Profile_RecordDate)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Profile_Year)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Profile_Semester)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Merit_Type)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Merit_Qty)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Merit_Reason)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Profile_PersonInCharge)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Merit_DateModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentMeritID[]")."</td>\n";

#$toolbar = "<a class=iconLink href=javascript:checkPost(document.form1,'new.php')>".newIcon()."$button_new</a>";
$toolbar = "<a class=iconLink href=javascript:checkGet(document.form1,'detail_export.php')>".exportIcon()."$button_export</a>";

// TABLE FUNCTION BAR
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'StudentMeritID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>\n";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'StudentMeritID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";


#$printAnalyze = "<a class=iconLink href='javascript:openAnalysisPage()'>".analysisIcon()."$i_Profile_Merit_Analysis</a>\n";
#$printAnalyze .= "<a class=iconLink href='javascript:openPrintPage()'>".printIcon()."$i_PrinterFriendlyPage</a>\n";
#$name = $lu->UserNameLang();

$lmerit = new libmerit();
$years = $lmerit->returnYears();
$pics  = $lmerit->returnPICs();

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=class",$class);
$select_year = getSelectByValue($years,"name=yrfilter",$yrfilter,1);
$select_pic = getSelectByArray($pics, "name=pic",$pic,1);

$lword = new libwordtemplates();
$meritReasons = array_merge($lword->getWordListMerit(), $lword->getWordListDemerit());
$reasonSelect = getSelectByValue($meritReasons,"name=reason",$reason,1);

$filterbar = "<table width=100% border=0 cellpadding=1 cellspacing=1>\n";
$filterbar .= "<tr><td width=20% align=right>$i_Profile_Year: </td><td>$select_year<td></tr>\n";
$filterbar .= "<tr><td width=20% align=right>$i_UserClassName: </td><td>$select_class<td></tr>\n";
$filterbar .= "<tr><td width=20% align=right>$i_Profile_PersonInCharge: </td><td>$select_pic<td></tr>\n";
$filterbar .= "<tr><td width=20% align=right>$i_Profile_SelectReason: </td><td>$reasonSelect<td></tr>\n";
$filterbar .= "<tr><td width=20% align=right>$i_Profile_SearchByDate: </td><td>";
$filterbar .= "<input type=radio name=datetype value=0 ".($datetype==0?"CHECKED":"")."> $i_Profile_NotSearchByDate";
$filterbar .= "&nbsp;<input type=radio name=datetype value=1 ".($datetype==1?"CHECKED":"")."> $i_Profile_ByRecordDate";
$filterbar .= "&nbsp;<input type=radio name=datetype value=2 ".($datetype==2?"CHECKED":"")."> $i_Profile_ByLastModified";
$filterbar .= "<br>\n$i_From : <input type=text name=date_from size=10 maxlength=10 value='$date_from'> $i_To : <input type=text name=date_to size=10 maxlength=10 value='$date_to'> (YYYY-MM-DD)<br>\n";
$filterbar .= "<input type=image src=\"$image_path/admin/button/s_btn_find_$intranet_session_language.gif\"><td></tr>";
$filterbar .= "</table>\n";


################################## here
?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function changeDateType(obj)
{
         switch (obj.datetype.value)
         {
                 case '0':
                      obj.start.value = '<?=$today?>';
                      obj.end.value = '<?=$today?>';
                      break;
                 case '1':
                      obj.start.value = '<?=$weekstart?>';
                      obj.end.value = '<?=$weekend?>';
                      break;
                 case '2':
                      obj.start.value = '<?=$monthstart?>';
                      obj.end.value = '<?=$monthend?>';
                      break;
                 case '3':
                      obj.start.value = '<?=$yearstart?>';
                      obj.end.value = '<?=$yearend?>';
                      break;
         }
         obj.submit();
}
function openAnalysisPage()
{
        newWindow("studentanalysis.php?studentid=<?=$studentid?>&keyword=<?=$keyword?>&yrfilter=<?=$yrfilter?>",4);
}
function openPrintPage()
{
        newWindow("studentprint.php?studentid=<?=$studentid?>&keyword=<?=$keyword?>&yrfilter=<?=$yrfilter?>",4);
}
</SCRIPT>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Merit, 'index.php', $i_general_ViewDetailRecords, '') ?>
<?= displayTag("head_merit_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $filterbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="100%" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $printAnalyze, $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width="100%" height=7 border=0></td></tr>
<tr><td class=tableContent>
<?=$li->display()?>
</td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=studentid value="<?=$studentid?>">
<input type=hidden name=classid value="<?=$classid?>">
<input type=hidden name=page_from value="<?="detail.php"?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");

?>