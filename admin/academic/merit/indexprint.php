<?php

################################################
#
#	Date:	2016-07-20 (Bill)
#			Replace deprecated split() by explode() for PHP 5.4
#
#	Date:	2012-11-23	YatWoon
#			fixed: Failed to display school logo in file name with space [Case#2012-1122-1536-19132]
#
################################################

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libmerit.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libstudentprofile.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/fileheader.php");
intranet_opendb();

// get school information (name + badge)
$li = new libfilesystem();
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$schoolbadge = ($imgfile!="") ? "<img src='/file/$imgfile' width=120 height=60><br>\n" : "&nbsp;";

//$school_data = split("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? $school_data[0] : "&nbsp;";

if ($schoolbadge!="&nbsp;" || $school_name!="&nbsp;")
{
        $school_info = "<table border=0 width=650>\n";
        $school_info .= "<tr><td align=center>$schoolbadge <font size=+0><b>$school_name</b></font></td></tr>\n";
        $school_info .= "</table>\n<br>\n";
}

$lvlArray = array();
$lvlMerit = array();
$lvlMinorC = array();
$lvlMajorC = array();
$lvlSuperC = array();
$lvlUltraC = array();
$lvlBlack = array();
$lvlMinorD = array();
$lvlMajorD = array();
$lvlSuperD = array();
$lvlUltraD = array();

$lmerit = new libmerit();
$lword = new libwordtemplates();
$lsp = new libstudentprofile();
$meritReasons = $lword->getWordListMerit();

$now = time();
//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));


if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}

$result = $lmerit->getClassMeritList($start,$end,$reason);

$x = "<table width=95% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td width=20% class=tableTitle rowspan=2>$i_UserClassName</td>
<td class=tableTitle align=center colspan=".$lsp->col_merit.">$i_Merit_Award</td>
<td class=tableTitle align=center colspan=".$lsp->col_demerit.">$i_Merit_Punishment</td>
</tr>\n";

$x .= "<tr>\n";
if (!$lsp->is_merit_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_Short_Merit</td>\n";
}
if (!$lsp->is_min_merit_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_Short_MinorCredit</td>\n";
}
if (!$lsp->is_maj_merit_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_Short_MajorCredit</td>\n";
}
if (!$lsp->is_sup_merit_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_Short_SuperCredit</td>\n";
}
if (!$lsp->is_ult_merit_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_Short_UltraCredit</td>\n";
}
if (!$lsp->is_black_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_Short_BlackMark</td>\n";
}
if (!$lsp->is_min_demer_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_Short_MinorDemerit</td>\n";
}
if (!$lsp->is_maj_demer_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_Short_MajorDemerit</td>\n";
}
if (!$lsp->is_sup_demer_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_Short_SuperDemerit</td>\n";
}
if (!$lsp->is_ult_demer_disabled)
{
     $x .= "<td class=tableTitle>$i_Merit_Short_UltraDemerit</td>\n";
}
$x .= "</tr>\n";

$en_reason = urlencode($reason);

for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$lvlID,$merit,$minorC,$majorC,$superC,$ultraC,$black,$minorD,$majorD,$superD,$ultraD) = $result[$i];
     if (!in_array($lvlID,$lvlArray))
     {
          $lvlArray[] = $lvlID;
     }
     for ($j=0; $j<sizeof($lvlArray); $j++)
     {
          if ($lvlArray[$j]==$lvlID)
          {
              $lvlMerit[$j] += $merit;
              $lvlMinorC[$j] += $minorC;
              $lvlMajorC[$j] += $majorC;
              $lvlSuperC[$j] += $superC;
              $lvlUltraC[$j] += $ultraC;
              $lvlBlack[$j] += $black;
              $lvlMinorD[$j] += $minorD;
              $lvlMajorD[$j] += $majorD;
              $lvlSuperD[$j] += $superD;
              $lvlUltraD[$j] += $ultraD;
              break;
          }
     }
     $link = "$name";
     $x .= "<tr>
             <td class=tableContent>$link</td>\n";
     if (!$lsp->is_merit_disabled)
     {
          $x .= "<td class=tableContent$css>$merit</td>\n";
     }
     if (!$lsp->is_min_merit_disabled)
     {
          $x .= "<td class=tableContent$css>$minorC</td>\n";
     }
     if (!$lsp->is_maj_merit_disabled)
     {
          $x .= "<td class=tableContent$css>$majorC</td>\n";
     }
     if (!$lsp->is_sup_merit_disabled)
     {
          $x .= "<td class=tableContent$css>$superC</td>\n";
     }
     if (!$lsp->is_ult_merit_disabled)
     {
          $x .= "<td class=tableContent$css>$ultraC</td>\n";
     }
     if (!$lsp->is_black_disabled)
     {
          $x .= "<td class=tableContent$css>$black</td>\n";
     }
     if (!$lsp->is_min_demer_disabled)
     {
          $x .= "<td class=tableContent$css>$minorD</td>\n";
     }
     if (!$lsp->is_maj_demer_disabled)
     {
          $x .= "<td class=tableContent$css>$majorD</td>\n";
     }
     if (!$lsp->is_sup_demer_disabled)
     {
          $x .= "<td class=tableContent$css>$superD</td>\n";
     }
     if (!$lsp->is_ult_demer_disabled)
     {
          $x .= "<td class=tableContent$css>$ultraD</td>\n";
     }
     $x .= "</tr>\n";
}
$x .= "</table>\n";


$lvls = $lmerit->getLevelList();
$y = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td width=20% class=tableTitle rowspan=2>$i_UserClassLevel</td>
<td class=tableTitle align=center colspan=".$lsp->col_merit.">$i_Merit_Award</td>
<td class=tableTitle align=center colspan=".$lsp->col_demerit.">$i_Merit_Punishment</td>
</tr>\n";

$y .= "<tr>\n";
if (!$lsp->is_merit_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_Short_Merit</td>\n";
}
if (!$lsp->is_min_merit_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_Short_MinorCredit</td>\n";
}
if (!$lsp->is_maj_merit_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_Short_MajorCredit</td>\n";
}
if (!$lsp->is_sup_merit_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_Short_SuperCredit</td>\n";
}
if (!$lsp->is_ult_merit_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_Short_UltraCredit</td>\n";
}
if (!$lsp->is_black_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_Short_BlackMark</td>\n";
}
if (!$lsp->is_min_demer_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_Short_MinorDemerit</td>\n";
}
if (!$lsp->is_maj_demer_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_Short_MajorDemerit</td>\n";
}
if (!$lsp->is_sup_demer_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_Short_SuperDemerit</td>\n";
}
if (!$lsp->is_ult_demer_disabled)
{
     $y .= "<td class=tableTitle>$i_Merit_Short_UltraDemerit</td>\n";
}
$y .= "</tr>\n";
for ($i=0; $i<sizeof($lvlArray); $i++)
{
     $id = $lvlArray[$i];
     $name = $lvls[$id];
     $merit = $lvlMerit[$i];
     $minorC = $lvlMinorC[$i];
     $majorC = $lvlMajorC[$i];
     $superC = $lvlSuperC[$i];
     $ultraC = $lvlUltraC[$i];
     $black = $lvlBlack[$i];
     $minorD = $lvlMinorD[$i];
     $majorD = $lvlMajorD[$i];
     $superD = $lvlSuperD[$i];
     $ultraD = $lvlUltraD[$i];

     $y .= "<tr>
             <td class=tableContent>$name</td>\n";
     if (!$lsp->is_merit_disabled)
     {
          $y .= "<td class=tableContent$css>$merit</td>\n";
     }
     if (!$lsp->is_min_merit_disabled)
     {
          $y .= "<td class=tableContent$css>$minorC</td>\n";
     }
     if (!$lsp->is_maj_merit_disabled)
     {
          $y .= "<td class=tableContent$css>$majorC</td>\n";
     }
     if (!$lsp->is_sup_merit_disabled)
     {
          $y .= "<td class=tableContent$css>$superC</td>\n";
     }
     if (!$lsp->is_ult_merit_disabled)
     {
          $y .= "<td class=tableContent$css>$ultraC</td>\n";
     }
     if (!$lsp->is_black_disabled)
     {
          $y .= "<td class=tableContent$css>$black</td>\n";
     }
     if (!$lsp->is_min_demer_disabled)
     {
          $y .= "<td class=tableContent$css>$minorD</td>\n";
     }
     if (!$lsp->is_maj_demer_disabled)
     {
          $y .= "<td class=tableContent$css>$majorD</td>\n";
     }
     if (!$lsp->is_sup_demer_disabled)
     {
          $y .= "<td class=tableContent$css>$superD</td>\n";
     }
     if (!$lsp->is_ult_demer_disabled)
     {
          $y .= "<td class=tableContent$css>$ultraD</td>\n";
     }
     $y .= "</tr>\n";
}
$y .= "</table>\n";

$datetype += 0;

$functionbar = "&nbsp; $i_Attendance_Date: $i_Profile_From $start $i_Profile_To $end";
$reasonSelect = ($reason!="") ? $reason : $i_status_all;
$functionbar .= "<br>&nbsp; $i_Attendance_Reason: $reasonSelect";

?>


<form name="form1" method="get">
<?=$school_info?>
<table width=95% border=0 cellpadding=0 cellspacing=0>
<tr><td><font size='+0'><u><?=$i_Profile_Merit ?></u></font></td></tr>
<tr><td><?= $functionbar ?></td></tr>
<tr><td class=tableContent><br><br>
<?=$x?>
</td></tr>
<tr><td><br><br></td></tr>
<tr><td class=tableContent>
<?=$y?>
</td></tr>
</table>

<br>
<table border=0 width=550>
<tr><td align=center>
<input type=image class=printImg src="/images/admin/button/s_btn_print_<?=$intranet_session_language?>.gif" border="0" onclick="window.print(); return false;">
</td></tr>
</table>

</form>

<?
include_once("../../../templates/filefooter.php");
?>