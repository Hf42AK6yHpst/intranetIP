<?php
if (!defined("LIBMERIT_DEFINED"))         // Preprocessor directives
{

 define("LIBMERIT_DEFINED",true);

 class libmerit extends libclass{
       var $StudentMeritID;
       var $UserID;
       var $Year;
       var $MeritDate;
       var $RecordType;
       var $Number;
       var $Reason;
       var $Remark;
       var $type_array;

       function libmerit($StudentMeritID="")
       {
                $this->libclass();
                if ($StudentMeritID != "")
                {
                    $this->StudentMeritID = $StudentMeritID;
                    $this->retrieveRecord($this->StudentMeritID);
                }
                $this->type_array = array(1,2,3,4,5,-1,-2,-3,-4,-5);
       }
       function retrieveRecord($id)
       {
                $fields = "UserID, Year, DATE_FORMAT(MeritDate,'%Y-%m-%d'), RecordType, NumberOfUnit, Reason, Remark";
                $conds = "StudentMeritID = $id";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_MERIT WHERE $conds";
                $result = $this->returnArray($sql,7);
                list(
                $this->UserID, $this->Year, $this->MeritDate,$this->RecordType,
                $this->Number, $this->Reason, $this->Remark) = $result[0];
                return $result;
       }
       function returnDateConditions($start, $end)
       {
                if ($start == "" || $end == "")
                {
                    $date_conds = "";
                }
                else
                {
                    $date_conds = "AND UNIX_TIMESTAMP(MeritDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')";
                }
                return $date_conds;
       }
       function returnStudentRecord($uid, $start, $end)
       {
                $fields = "DATE_FORMAT(MeritDate,'%Y-%m-%d'), RecordType,NumberOfUnit, Reason";
                $conds = "UserID = $uid AND UNIX_TIMESTAMP(MeritDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')";
                $order = "MeritDate DESC";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_MERIT WHERE $conds ORDER BY $order";
                return $this->returnArray($sql,4);
       }
       function getMeritCountByClassName ($class, $start, $end)
       {
                $students = $this->getClassStudentList($class);
                if (sizeof($students)!=0)
                {
                    $student_list = implode(",",$students);
                    $date_conds = " AND UNIX_TIMESTAMP(MeritDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')";

                    $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                            WHERE UserID IN ($student_list) $date_conds AND RecordType > 0";
                    $merit = $this->returnVector($sql);
                    $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                            WHERE UserID IN ($student_list) $date_conds AND RecordType < 0";
                    $demerit = $this->returnVector($sql);
                    $entry = array($merit[0]+0,$demerit[0]+0);
                    return $entry;
                }
                else
                {
                    $entry = array(0,0);
                    return $entry;
                }
       }
       function getClassMeritList ($start, $end)
       {
       /*
                $classList = $this->getClassList();
                if (sizeof($classList)==0) return array();
                for ($i=0; $i<sizeof($classList); $i++)
                {
                     list($id, $name,$lvlID) = $classList[$i];
                     $temp = $this->getMeritCountByClassName($name,$start,$end);
                     $result[] = array($id,$name,$temp[0],$temp[1],$lvlID);
                }
                return $result;
       */
                $dateConds = $this->returnDateConditions($start, $end);
                $records = array();
                $counts = array();
                for ($i=0; $i<sizeof($this->type_array); $i++)
                {
                     $type = $this->type_array[$i];
                     $sql = "SELECT c.ClassID, b.ClassName, COUNT(a.StudentMeritID), c.ClassLevelID
                             FROM PROFILE_STUDENT_MERIT as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                             LEFT OUTER JOIN INTRANET_CLASS as c ON b.ClassName = c.ClassName
                             WHERE a.RecordType = $type $dateConds
                             GROUP BY c.ClassID
                             ";
                     $result[] = $this->returnArray($sql,4);
                     for ($j=0; $j<sizeof($result); $i++)
                     {
                          list ($ClassID,$ClassName,$MeritCount,$LvlID) = $result[$j];
                          $counts[$i][$ClassID] = array($ClassName,$MeritCount,$LvlID);
                     }
                }

                $classList = $this->getClassList();
                $returnResult = array();
                for ($i=0; $i<sizeof($classList); $i++)
                {
                     list($ClassID, $ClassName, $LvlID) = $classList[$i];
                     $returnResult[$i][0] = $ClassID;
                     $returnResult[$i][1] = $ClassName;
                     $returnResult[$i][2] = $LvlID;
                     for ($j=0; $j<sizeof($this->type_array); $j++)
                     {
                          $MeritCount = $counts[$i][$ClassID][1]+0;
                          $returnResult[$i][3+$j] = $MeritCount;
                     }
                }
                return $returnResult;
       }
       function getMeritCountByStudent($studentid,$start="",$end="")
       {
                if ($start == "" || $end == "")
                {
                    $date_conds = "";
                }
                else
                {
                    $date_conds = "AND UNIX_TIMESTAMP(MeritDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')";
                }
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType > 0";
                $merit = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType < 0";
                $demerit = $this->returnVector($sql);
                $entry = array($merit[0]+0,$demerit[0]+0);
                return $entry;
       }
       function getDetailedMeritCountByStudent($studentid,$start="",$end="")
       {
                if ($start == "" || $end == "")
                {
                    $date_conds = "";
                }
                else
                {
                    $date_conds = "AND UNIX_TIMESTAMP(MeritDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')";
                }
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = 1";
                $merit = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = 2";
                $minorC = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = 3";
                $majorC = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = 4";
                $superC = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = 5";
                $ultraC = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = -1";
                $black = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = -2";
                $minorD = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = -3";
                $majorD = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = -4";
                $superD = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = -5";
                $ultraD = $this->returnVector($sql);
                $entry = array(
                $merit[0]+0,
                $minorC[0]+0,
                $majorC[0]+0,
                $superC[0]+0,
                $ultraC[0]+0,
                $black[0]+0,
                $minorD[0]+0,
                $majorD[0]+0,
                $superD[0]+0,
                $ultraD[0]+0
                );
                return $entry;
       }
       function getMeritListByClass($classid,$start,$end)
       {
                $studentList = $this->getClassStudentNameList($classid);
                if (sizeof($studentList)==0) return array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($id, $name, $classnumber) = $studentList[$i];
                     $temp = $this->getMeritCountByStudent($id,$start,$end);
                     $result[] = array($id,$name,$classnumber,$temp[0],$temp[1]);
                }
                return $result;
       }
       function getSelectMeritType ($tags,$selected="")
       {
                global $i_Merit_TypeArray;
                $type = array(1,2,3,4,5,-1,-2,-3,-4,-5);
                $x = "<SELECT $tags>\n";
                for ($i=0; $i<sizeof($type); $i++)
                {
                     $value = $type[$i];
                     $name = $i_Merit_TypeArray[$i];
                     $selected_str = ($value==$selected? "SELECTED":"");
                     $x .= "<OPTION value=$value $selected_str>$name</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }
#                $fields = "MeritDate, RecordType,NumberOfUnit, Reason";
       function displayStudentRecord($studentid,$start,$end)
       {
                global $i_Merit_Date,$i_Merit_Type,$i_Merit_Qty,$i_Merit_Reason;
                global $i_no_record_exists_msg;
                global $i_Merit_Merit,$i_Merit_MinorCredit,$i_Merit_MajorCredit,
                       $i_Merit_SuperCredit,$i_Merit_UltraCredit,$i_Merit_BlackMark,
                       $i_Merit_MinorDemerit,$i_Merit_MajorDemerit,$i_Merit_SuperDemerit,
                       $i_Merit_UltraDemerit;

                $no_msg = $i_no_record_exists_msg;

                $result = $this->returnStudentRecord($studentid,$start,$end);
              $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>
        <tr>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Date</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Type</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Qty</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Reason</td>
        </tr>\n";
              for ($i=0; $i<sizeof($result); $i++)
              {
                   list ($meritDate,$type,$qty,$reason) = $result[$i];
                   switch ($type)
                   {
                           case 1: $typeStr = $i_Merit_Merit; break;
                           case 2: $typeStr = $i_Merit_MinorCredit; break;
                           case 3: $typeStr = $i_Merit_MajorCredit; break;
                           case 4: $typeStr = $i_Merit_SuperCredit; break;
                           case 5: $typeStr = $i_Merit_UltraCredit; break;
                           case -1: $typeStr = $i_Merit_BlackMark; break;
                           case -2: $typeStr = $i_Merit_MinorDemerit; break;
                           case -3: $typeStr = $i_Merit_MajorDemerit; break;
                           case -4: $typeStr = $i_Merit_SuperDemerit; break;
                           case -5: $typeStr = $i_Merit_UltraDemerit; break;
                           default: $typeStr = "Error"; break;
                   }
                   $x .= "<tr>
          <td class=td_center_middle bgcolor=#E8FBFC>$meritDate</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$typeStr</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$qty</td>
          <td class=td_center_middle bgcolor=#E8FBFC>&nbsp;$reason</td>
        </tr>";
              }
              if (sizeof($result)==0)
              {
                  $x .= "<tr><td colspan=4 class=td_center_middle bgcolor=#E8FBFC>".$no_msg."</td></tr>\n";
              }
              $x .= "</table>";
              return $x;

       }

       function returnYears($studentid)
       {
                $sql = "SELECT DISTINCT(Year) FROM PROFILE_STUDENT_MERIT WHERE UserID = '$studentid' ORDER BY Year";
                return $this->returnVector($sql);
       }

       function returnStudentRecordByYear($studentid)
       {
                # Grab information in DB
                for ($i=0; $i<sizeof($this->type_array); $i++)
                {
                     $sql = "SELECT Year, COUNT(StudentMeritID) FROM PROFILE_STUDENT_MERIT
                             WHERE UserID = '$studentid' AND RecordType = '".$this->type_array[$i]."'
                             GROUP BY Year";
                     $data[] = $this->returnArray($sql,2);
                }

                # Build the whole array

                $years = $this->returnYears($studentid);
                $result = array();
                for ($i=0; $i<sizeof($years); $i++)
                {
                     $result[0][$i] = $years[$i];
                }
                for ($i=0; $i<sizeof($this->type_array); $i++)
                {
                     //$result[$i] = array();
                     for ($j=0; $j<sizeof($years); $j++)
                     {
                          $pos = -1;
                          for ($k=0; $k<sizeof($data[$i]); $k++)
                          {
                               if ($data[$i][$k][0] == $years[$j])
                               {
                                   $pos = $k;
                                   break;
                               }
                          }
                          if ($pos == -1)
                          {
                              $count = 0;
                          }
                          else
                          {
                              $count = $data[$i][$k][1];
                          }
                          $result[$i+1][$j] = $count;
                     }
                }
                return $result;

       }


       function displayStudentRecordByYearAdmin($studentid)
       {
                global $i_no_record_exists_msg;
                $result = $this->returnStudentRecordByYear($studentid);
                $x = "<table width=500 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEFDEE bordercolordark=#BEBEBE class=body>\n";
                if (sizeof($result)==0)
                {
                    return "$x<tr><td>$i_no_record_exists_msg</td></tr></table>\n";
                }
                global $i_Profile_Year,$i_Merit_TypeArray;
                $title_array = array($i_Profile_Year);
                $title_array = array_merge($title_array,$i_Merit_TypeArray);
                $size = sizeof($result[0]) + 1;
                //$width = 100/$size;

                for ($i=0; $i<sizeof($title_array); $i++)
                {
                     $x .= "<tr><td width=80>".$title_array[$i]."</td>";
                     for ($j=0; $j<sizeof($result[$i]); $j++)
                     {
                          $data = $result[$i][$j];
                          if ($i == 0)
                          {
                              $data = "<a href=javascript:viewMeritByYear($studentid,$data)>$data</a>";
                          }
                          $x .= "<td>$data</td>";
                     }
                     $x .= "</tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }
       function returnDetailedRecordByYear($StudentID, $year)
       {
                $fields = "DATE_FORMAT(MeritDate,'%Y-%m-%d'), RecordType,NumberOfUnit, Reason,Remark";
                $conds = "UserID = '$StudentID' AND Year = '$year'";
                $order = "MeritDate DESC";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_MERIT WHERE $conds ORDER BY $order";
                return $this->returnArray($sql,5);
       }
       function displayDetailedRecordByYear($StudentID, $year)
       {
                global $i_Merit_Date,$i_Merit_Type,$i_Merit_Qty,$i_Merit_Reason,$i_Merit_Remark;
                global $i_no_record_exists_msg;
                global $i_Merit_Merit,$i_Merit_MinorCredit,$i_Merit_MajorCredit,
                       $i_Merit_SuperCredit,$i_Merit_UltraCredit,$i_Merit_BlackMark,
                       $i_Merit_MinorDemerit,$i_Merit_MajorDemerit,$i_Merit_SuperDemerit,
                       $i_Merit_UltraDemerit;

                $no_msg = $i_no_record_exists_msg;

                $result = $this->returnDetailedRecordByYear($StudentID,$year);
              $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>
        <tr>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Date</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Type</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Qty</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Reason</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Remark</td>
        </tr>\n";
              for ($i=0; $i<sizeof($result); $i++)
              {
                   list ($meritDate,$type,$qty,$reason,$remark) = $result[$i];
                   switch ($type)
                   {
                           case 1: $typeStr = $i_Merit_Merit; break;
                           case 2: $typeStr = $i_Merit_MinorCredit; break;
                           case 3: $typeStr = $i_Merit_MajorCredit; break;
                           case 4: $typeStr = $i_Merit_SuperCredit; break;
                           case 5: $typeStr = $i_Merit_UltraCredit; break;
                           case -1: $typeStr = $i_Merit_BlackMark; break;
                           case -2: $typeStr = $i_Merit_MinorDemerit; break;
                           case -3: $typeStr = $i_Merit_MajorDemerit; break;
                           case -4: $typeStr = $i_Merit_SuperDemerit; break;
                           case -5: $typeStr = $i_Merit_UltraDemerit; break;
                           default: $typeStr = "Error"; break;
                   }
                   $x .= "<tr>
          <td class=td_center_middle bgcolor=#E8FBFC>$meritDate</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$typeStr</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$qty</td>
          <td class=td_center_middle bgcolor=#E8FBFC>&nbsp;$reason</td>
          <td class=td_center_middle bgcolor=#E8FBFC>&nbsp;$remark</td>
        </tr>";
              }
              if (sizeof($result)==0)
              {
                  $x .= "<tr><td colspan=5 class=td_center_middle bgcolor=#E8FBFC>".$no_msg."</td></tr>\n";
              }
              $x .= "</table>";
              return $x;
       }

       function getClassMeritListByReason($start,$end,$reason)
       {
                $date_conds = $this->returnDateConditions($start,$end);
                $sql = "SELECT c.ClassID, b.ClassName, COUNT(a.StudentMeritID), c.ClassLevelID
                        FROM PROFILE_STUDENT_MERIT as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        LEFT OUTER JOIN INTRANET_CLASS as c ON b.ClassName = c.ClassName
                        WHERE Reason = '$reason' $date_conds
                        GROUP BY b.ClassName";
                $result = $this->returnArray($sql,4);
                $counts = array();
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($ClassID,$ClassName,$MeritCount,$LvlID) = $result[$i];
                     $counts[$ClassID] = array($ClassName,$MeritCount,$LvlID);
                }

                $classList = $this->getClassList();
                $returnResult = array();
                for ($i=0; $i<sizeof($classList); $i++)
                {
                     list($ClassID, $ClassName, $LvlID) = $classList[$i];
                     $MeritCount = $counts[$ClassID][1]+0;
                     $returnResult[] = array($ClassID, $ClassName, $MeritCount, $LvlID);
                }
                return $returnResult;
       }
       function getMeritListByClassReason($classid, $reason, $start, $end)
       {
                $date_conds = $this->returnDateConditions($start,$end);
                $class_name = $this->getClassName($classid);
                $name_field = getNameFieldByLang("b.");
                $sql = "SELECT a.UserID, $name_field, b.ClassNumber, COUNT(a.StudentMeritID)
                        FROM PROFILE_STUDENT_MERIT as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        WHERE b.ClassName = '$class_name' AND a.Reason = '$reason' $date_conds
                        GROUP BY a.UserID";
                $result = $this->returnArray($sql,4);
                $counts = array();
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($StudentID,$StudentName,$ClassNumber,$count) = $result[$i];
                     $counts[$StudentID] = array($StudentName,$ClassNumber,$count);
                }

                $studentList = $this->getClassStudentNameList($classid);
                $returnResult = array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($StudentID, $StudentName, $ClassNumber) = $studentList[$i];
                     $MeritCount = $counts[$StudentID][2]+0;
                     $returnResult[] = array($StudentID, $StudentName,$ClassNumber, $MeritCount);
                }
                return $returnResult;
       }

 }


} // End of directives
?>