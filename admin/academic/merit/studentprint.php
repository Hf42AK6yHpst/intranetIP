<?php

################################################
#
#	Date:	2016-07-20 (Bill)
#			Replace deprecated split() by explode() for PHP 5.4
#
#	Date:	2012-11-23	YatWoon
#			fixed: Failed to display school logo in file name with space [Case#2012-1122-1536-19132]
#
################################################

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libstudentprofile.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/fileheader.php");

intranet_opendb();

// get school information (name + badge)
$li = new libfilesystem();
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$schoolbadge = ($imgfile!="") ? "<img src='/file/$imgfile' width=120 height=60><br>\n" : "&nbsp;";

//$school_data = split("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? $school_data[0] : "&nbsp;";

if ($schoolbadge!="&nbsp;" || $school_name!="&nbsp;")
{
        $school_info = "<table border=0 width=650>\n";
        $school_info .= "<tr><td align=center>$schoolbadge <font size=+0><b>$school_name</b></font></td></tr>\n";
        $school_info .= "</table>\n<br>\n";
}

$lu = new libuser($studentid);

$now = time();
//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));


if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}

# TABLE SQL
$keyword = trim($keyword);
if($field=="") $field = 0;
if($order=="") $order = 1;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     case 4: $field = 4; break;
     case 5: $field = 5; break;
     default: $field = 0; break;
}
if ($yrfilter!="")
{
    $conds = " AND Year = '$yrfilter'";
}
else
{
    $conds = "";
}

$sql = "SELECT
              DATE_FORMAT(a.MeritDate,'%Y-%m-%d'),
              ifnull(a.Year, '&nbsp;'),
              CASE a.RecordType
                   WHEN 1 THEN '$i_Merit_Merit'
                   WHEN 2 THEN '$i_Merit_MinorCredit_unicode'
                   WHEN 3 THEN '$i_Merit_MajorCredit_unicode'
                   WHEN 4 THEN '$i_Merit_SuperCredit_unicode'
                   WHEN 5 THEN '$i_Merit_UltraCredit_unicode'
                   WHEN -1 THEN '$i_Merit_BlackMark'
                   WHEN -2 THEN '$i_Merit_MinorDemerit'
                   WHEN -3 THEN '$i_Merit_MajorDemerit'
                   WHEN -4 THEN '$i_Merit_SuperDemerit'
                   WHEN -5 THEN '$i_Merit_UltraDemerit'
                   ELSE '-' END,
              a.NumberOfUnit,
              a.Reason,
              CONCAT(a.Remark,'&nbsp;')
        FROM PROFILE_STUDENT_MERIT as a
        WHERE a.UserID = $studentid
              AND a.RecordType != 0
              AND (a.MeritDate like '%$keyword%'
                   OR
                   a.Reason like '%$keyword%'
                   ) $conds
                ORDER BY
                          MeritDate
              ";
# TABLE INFO
$li = new libdb();
$row = $li->returnArray($sql, 6);

$x = "<table width=95% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr><td class=tableTitle width=1>#</td>\n";
$x .= "<td width=20% class=tableTitle>$i_Merit_Date</td>\n";
$x .= "<td width=10% class=tableTitle>$i_Profile_Year</td>\n";
$x .= "<td width=10% class=tableTitle>$i_Merit_Type</td>\n";
$x .= "<td width=10% class=tableTitle>$i_Merit_Qty</td>\n";
$x .= "<td width=30% class=tableTitle>$i_Merit_Reason</td>\n";
$x .= "<td width=40% class=tableTitle>$i_Merit_Remark</td>\n";
$x .= "</tr>\n";
if (sizeof($row)==0)
{
        $x .= "<tr><td class=tableContent colspan=7 align=center>$i_no_record_exists_msg</td></tr>\n";
}
for ($i=0; $i<sizeof($row); $i++)
{
        $x .= "<tr><td class=tableContent>".($i+1)."</td>\n";
        $x .= "<td class=tableContent>".$row[$i][0]."</td>\n";
        $x .= "<td class=tableContent>".$row[$i][1]."</td>\n";
        $x .= "<td class=tableContent>".$row[$i][2]."</td>\n";
        $x .= "<td class=tableContent>".$row[$i][3]."</td>\n";
        $row[$i][4] = (trim($row[$i][4])!="") ? $row[$i][4] : "&nbsp;";
        $x .= "<td class=tableContent>".$row[$i][4]."</td>\n";
        $x .= "<td class=tableContent>".$row[$i][5]."</td>\n";
        $x .= "</tr>\n";
}
$x .= "</table>\n";

$name = $lu->UserNameLang();


$functionbar = (trim($keyword)!="") ? "&nbsp; $i_Attendance_Reason/$i_Attendance_Date: $keyword" : "";
$functionbar2 = ($yrfilter!="")? "&nbsp; $i_Profile_Year: $yrfilter" :"";

?>
<style type='text/css' media='print'>
 .printImg {display:none;}
</style>

<form name="form1" method="get">
<?=$school_info?>
<table width=95% border=0 cellpadding=0 cellspacing=0>
<tr><td><font size='+0'><u><?=$i_Profile_Merit ?></u></font></td></tr>
<tr><td>&nbsp; <?= $i_UserClassName.": ".$lu->ClassName?></td></tr>
<tr><td>&nbsp; <?= $i_PrinterFriendly_StudentName.": ".$name?></td></tr>
<tr><td><?= $functionbar ?></td></tr>
<tr><td><?= $functionbar2 ?></td></tr>
<tr><td class=tableContent><br><br>
<?=$x?>
</td></tr>
</table>

<br>
<table border=0 width=550>
<tr><td align=center>
<input type=image class=printImg src="/images/admin/button/s_btn_print_<?=$intranet_session_language?>.gif" border="0" onclick="window.print(); return false;">
</td></tr>
</table>

</form>
<?
include_once("../../../templates/filefooter.php");

?>