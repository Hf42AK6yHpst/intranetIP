<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libstudentprofile.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/fileheader.php");

intranet_opendb();

$lu = new libuser($studentid);
$lsp = new libstudentprofile();

$now = time();
//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));


if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}


$name = $lu->UserNameLang();

# get stats according to reasons
if ($yrfilter!="")
{
    $conds = " AND Year = '$yrfilter'";
}
else
{
    $conds = "";
}
$sql = "
SELECT
                Reason,
                RecordType,
                SUM(NumberOfUnit)
FROM PROFILE_STUDENT_MERIT
WHERE
                UserID = $studentid
                AND (MeritDate like '%$keyword%'
                   OR
                   Reason like '%$keyword%') $conds
GROUP BY
                Reason, RecordType
ORDER BY
                Reason, RecordType
";
$ldb = new libdb();
$row = $ldb->returnArray($sql, 3);

// build array for final table
$pos_now = 0;
$pre_reason = $row[0][0];
$dataArr[$pos_now][0] = $pre_reason;
$dataArr[$pos_now][1] = 0;
$dataArr[$pos_now][2] = 0;
$dataArr[$pos_now][3] = 0;
$dataArr[$pos_now][4] = 0;
$dataArr[$pos_now][5] = 0;
$dataArr[$pos_now][6] = 0;
$dataArr[$pos_now][7] = 0;
$dataArr[$pos_now][8] = 0;
$dataArr[$pos_now][9] = 0;
$dataArr[$pos_now][10] = 0;
for ($i=0; $i<sizeof($row); $i++)
{
        if ($pre_reason!=$row[$i][0])
        {
                $pos_now++;
                $dataArr[$pos_now][0] = $row[$i][0];
                $dataArr[$pos_now][1] = 0;
                $dataArr[$pos_now][2] = 0;
                $dataArr[$pos_now][3] = 0;
                $dataArr[$pos_now][4] = 0;
                $dataArr[$pos_now][5] = 0;
                $dataArr[$pos_now][6] = 0;
                $dataArr[$pos_now][7] = 0;
                $dataArr[$pos_now][8] = 0;
                $dataArr[$pos_now][9] = 0;
                $dataArr[$pos_now][10] = 0;
                $pre_reason = $row[$i][0];
        }
        $iIndex = ($row[$i][1]>0) ? $row[$i][1] : -1*$row[$i][1]+5;
        $dataArr[$pos_now][$iIndex] = $row[$i][2];
        //echo $dataArr[$pos_now][$row[$i][1]]."<br>";
}

function getTotal($colNo){
        global $dataArr;
        $total = 0;
        for ($i=0; $i<sizeof($dataArr); $i++)
                $total += $dataArr[$i][$colNo];
        return $total;
}

/*
function convertTotal($in_1, $in_2, $in_3, $in_4, $in_5){
        $convertBy = 3;
        $out_1 = $in_1%$convertBy;
        for ($i=2; $i<5; $i++)
        {
                ${"in_$i"} = floor(${"in_".($i-1)}/$convertBy) + ${"in_$i"};
                ${"out_$i"} = ${"in_$i"}%$convertBy;
        }
        $out_5 = floor($in_4/$convertBy) + $in_5;
        return Array($out_1, $out_2, $out_3, $out_4, $out_5);
}
*/

function convertTotal($type, $in_1, $in_2, $in_3, $in_4, $in_5)
{
	global $ldb, $lsp;
	
	$con = $type==1 ? "MeritType > 0" : "MeritType < 0";
	$sql = "select * from DISCIPLINE_MERIT_TYPE_SETTING where $con order by MeritType";
	$row = $ldb->returnArray($sql);	
	
	$upAry = array();
	foreach($row as $k=>$d)
	{
 		if($d['UpgradeNum'])
			$upAry[$d['UpgradeFromType']] = $d['UpgradeNum'];
	}
	$a = $type==1? "" : "-";
	
  	for ($i=1; $i<=5; $i++)
    {
	    $convertBy = $upAry[$a.$i];
	    
	    # check next level is open or not
	    switch($a.$i)
	    {
		    case 1: $next = !$lsp->is_min_merit_disabled; 
				break;
			case 2: $next = !$lsp->is_maj_merit_disabled; 
				break;
			case 3: $next = !$lsp->is_sup_merit_disabled; 
				break;
			case 4: $next = !$lsp->is_ult_merit_disabled; 
				break;
		    case -1: $next = !$lsp->is_min_demer_disabled; 
				break;
			case -2: $next = !$lsp->is_maj_demer_disabled; 
				break;
			case -3: $next = !$lsp->is_sup_demer_disabled; 
				break;
			case -4: $next = !$lsp->is_ult_demer_disabled; 
				break;
			default:
				$next = false;
				break;
	    }
	    
		if($convertBy)
		{
			if($next)
			{	
				${"in_$i"} = floor(${"in_".($i-1)}/$convertBy) + ${"in_$i"};
		        ${"out_$i"} = ${"in_$i"}%$convertBy;
			}
			else
			{
				${"out_$i"} = floor(${"in_".($i-1)}/$convertBy) + ${"in_$i"};
			}
		}
		else
		{
				${"out_$i"} = ${"in_$i"};
		}
    }
    
    return Array($out_1, $out_2, $out_3, $out_4, $out_5);
}

function buildDescription()
{
	global $ldb, $lsp, $i_Merit_BlackMark, $i_Merit_MinorDemerit, $i_Merit_MajorDemerit, $i_Merit_SuperDemerit, $i_Merit_UltraDemerit, $i_Merit_Merit, $i_Merit_MinorCredit, $i_Merit_MajorCredit, $i_Merit_SuperCredit, $i_Merit_UltraCredit;
	 
	$MeritTitle['-1'] = $i_Merit_BlackMark;
	$MeritTitle['-2'] = $i_Merit_MinorDemerit;
	$MeritTitle['-3'] = $i_Merit_MajorDemerit;
	$MeritTitle['-4'] = $i_Merit_SuperDemerit;
	$MeritTitle['-5'] = $i_Merit_UltraDemerit;
	$MeritTitle['1'] = $i_Merit_Merit;
	$MeritTitle['2'] = $i_Merit_MinorCredit;
	$MeritTitle['3'] = $i_Merit_MajorCredit;
	$MeritTitle['4'] = $i_Merit_SuperCredit;
	$MeritTitle['5'] = $i_Merit_UltraCredit;
	
	# Merit 
	$sql = "select * from DISCIPLINE_MERIT_TYPE_SETTING where MeritType > 0 order by MeritType";
	$row = $ldb->returnArray($sql);
	$temp = array();
	foreach($row as $k=>$d)
	{
		if($d['UpgradeNum'])
		{
			switch($d['MeritType'])
		    {
				case 2: $next = !$lsp->is_min_merit_disabled; 
					break;
				case 3: $next = !$lsp->is_maj_merit_disabled; 
					break;
				case 4: $next = !$lsp->is_sup_merit_disabled; 
					break;
				default:
					$next = false;
					break;
		    }
		    if(!$next)	continue;
			$temp[] = $d['UpgradeNum'] ." ". $MeritTitle[$d['UpgradeFromType']] ."= " . $MeritTitle[$d['MeritType']];
		}
	}
	$x = implode(", " , $temp);
	
	# DeMerit 
	$sql = "select * from DISCIPLINE_MERIT_TYPE_SETTING where MeritType < 0 order by MeritType desc";
	$row = $ldb->returnArray($sql);
	$temp = array();
	foreach($row as $k=>$d)
	{
		if($d['UpgradeNum'])
		{
			switch($d['MeritType'])
		    {
				case -2: $next = !$lsp->is_min_demer_disabled; 
					break;
				case -3: $next = !$lsp->is_maj_demer_disabled; 
					break;
				case -4: $next = !$lsp->is_sup_demer_disabled; 
					break;
				default:
					$next = false;
					break;
		    }
		    if(!$next)	continue;
			$temp[] = $d['UpgradeNum'] ." ". $MeritTitle[$d['UpgradeFromType']] ."= " . $MeritTitle[$d['MeritType']];
		}
	}
	$x .= ($x ? "<br>" : "") . implode(", " , $temp);
	
	return $x;
}

function buildTable($in_1, $desc_1, $in_2, $desc_2, $in_3, $desc_3, $in_4, $desc_4, $in_5, $desc_5, $title){
        if ($in_1==0 && $in_2==0 && $in_3==0 && $in_4==0 && $in_5==0)
                return "";
        $x = "<table border=0 cellpadding=2 cellspacing=0>\n";
        $x .= "<tr><td>$title -&nbsp;</td>\n";
        $counter = 0;
        for ($i=1; $i<6; $i++)
        {
                if (${"in_$i"}>0)
                {
                        $x .= ($counter>0) ? "<td>, &nbsp;".${"in_$i"}." ( ".${"desc_$i"}.")</td>" : "<td>".${"in_$i"}." ( ".${"desc_$i"}.")</td>";
                        $counter ++;
                }
        }
        $x .= "</tr></table>\n";

        return $x;
}

$sMerit = "<table width=95% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td class=tableTitle width=1>#</td>
<td class=tableTitle>$i_Attendance_Reason</td>\n";
if (!$lsp->is_merit_disabled)
{
     $sMerit .= "<td class=tableTitle>$i_Merit_Short_Merit</td>\n";
}
if (!$lsp->is_min_merit_disabled)
{
     $sMerit .= "<td class=tableTitle>$i_Merit_Short_MinorCredit</td>\n";
}
if (!$lsp->is_maj_merit_disabled)
{
     $sMerit .= "<td class=tableTitle>$i_Merit_Short_MajorCredit</td>\n";
}
if (!$lsp->is_sup_merit_disabled)
{
     $sMerit .= "<td class=tableTitle>$i_Merit_Short_SuperCredit</td>\n";
}
if (!$lsp->is_ult_merit_disabled)
{
     $sMerit .= "<td class=tableTitle>$i_Merit_Short_UltraCredit</td>\n";
}
$sMerit .= "</tr>\n";
if (sizeof($row)==0)
{
        $sMerit .= "<tr><td class=tableContent colspan=".($lsp->col_merit+2)." align=center>$i_no_record_exists_msg</td></tr>\n";
}
else
{
        $counter1 = 1;
        for ($i=0; $i<sizeof($dataArr); $i++)
        {
                if ($dataArr[$i][1]>0 || $dataArr[$i][2]>0 || $dataArr[$i][3]> 0 ||
                        $dataArr[$i][4]>0 || $dataArr[$i][5]>0)
                {
                        $dataArr[$i][0] = (trim($dataArr[$i][0])!="") ? $dataArr[$i][0] : "&nbsp;";
                        $sMerit .= "<tr><td class=tableContent>$counter1</td>
                        <td class=tableContent>".$dataArr[$i][0]."</td>
                        ";
                        if (!$lsp->is_merit_disabled)
                        {
                             $sMerit .= "<td class=tableContent>".$dataArr[$i][1]."</td>";
                        }
                        if (!$lsp->is_min_merit_disabled)
                        {
                             $sMerit .= "<td class=tableContent>".$dataArr[$i][2]."</td>";
                        }
                        if (!$lsp->is_maj_merit_disabled)
                        {
                             $sMerit .= "<td class=tableContent>".$dataArr[$i][3]."</td>";
                        }
                        if (!$lsp->is_sup_merit_disabled)
                        {
                             $sMerit .= "<td class=tableContent>".$dataArr[$i][4]."</td>";
                        }
                        if (!$lsp->is_ult_merit_disabled)
                        {
                             $sMerit .= "<td class=tableContent>".$dataArr[$i][5]."</td>";
                        }
                        $sMerit .= "</tr>\n";
                        $counter1++;
                }
        }
        $sMerit .= "<tr><td colspan=2 align=right style='border-right: solid #FFFFFF; border-left:solid #FFFFFF; border-bottom:solid #FFFFFF'>$list_total:</td>\n";
        if (!$lsp->is_merit_disabled)
        {
             $sMerit .= "<td class=tableContent>".getTotal(1)."</td>\n";
        }
        if (!$lsp->is_min_merit_disabled)
        {
             $sMerit .= "<td class=tableContent>".getTotal(2)."</td>\n";
        }
        if (!$lsp->is_maj_merit_disabled)
        {
             $sMerit .= "<td class=tableContent>".getTotal(3)."</td>\n";
        }
        if (!$lsp->is_sup_merit_disabled)
        {
             $sMerit .= "<td class=tableContent>".getTotal(4)."</td>\n";
        }
        if (!$lsp->is_ult_merit_disabled)
        {
             $sMerit .= "<td class=tableContent>".getTotal(5)."</td>\n";
        }
        $sMerit .= "</tr>\n";
}

$sMerit .= "</table>\n";


$sDemerit = "<table width=95% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td class=tableTitle width=1>#</td>
<td class=tableTitle>$i_Attendance_Reason</td>\n";
if (!$lsp->is_black_disabled)
{
     $sDemerit .= "<td class=tableTitle >$i_Merit_Short_BlackMark</td>\n";
}
if (!$lsp->is_min_demer_disabled)
{
     $sDemerit .= "<td class=tableTitle >$i_Merit_Short_MinorDemerit</td>\n";
}
if (!$lsp->is_maj_demer_disabled)
{
     $sDemerit .= "<td class=tableTitle >$i_Merit_Short_MajorDemerit</td>\n";
}
if (!$lsp->is_sup_demer_disabled)
{
     $sDemerit .= "<td class=tableTitle >$i_Merit_Short_SuperDemerit</td>\n";
}
if (!$lsp->is_ult_demer_disabled)
{
     $sDemerit .= "<td class=tableTitle >$i_Merit_Short_UltraDemerit</td>\n";
}
$sDemerit .= "</tr>\n";
if (sizeof($row)==0)
{
        $sDemerit .= "<tr><td class=tableContent colspan=".($lsp->col_demerit+2)." align=center>$i_no_record_exists_msg</td></tr>\n";
}
else
{
        $counter2 = 1;
        for ($i=0; $i<sizeof($dataArr); $i++)
        {
                if ($dataArr[$i][6]>0 || $dataArr[$i][7]>0 || $dataArr[$i][8]> 0 ||
                        $dataArr[$i][9]>0 || $dataArr[$i][10]>0)
                {
                        $dataArr[$i][0] = (trim($dataArr[$i][0])!="") ? $dataArr[$i][0] : "&nbsp;";
                        $sDemerit .= "<tr><td class=tableContent>$counter2</td>
                        <td class=tableContent>".$dataArr[$i][0]."</td>\n";
                        if (!$lsp->is_black_disabled)
                        {
                             $sDemerit .= "<td class=tableContent>".$dataArr[$i][6]."</td>";
                        }
                        if (!$lsp->is_min_demer_disabled)
                        {
                             $sDemerit .= "<td class=tableContent>".$dataArr[$i][7]."</td>";
                        }
                        if (!$lsp->is_maj_demer_disabled)
                        {
                             $sDemerit .= "<td class=tableContent>".$dataArr[$i][8]."</td>";
                        }
                        if (!$lsp->is_sup_demer_disabled)
                        {
                             $sDemerit .= "<td class=tableContent>".$dataArr[$i][9]."</td>";
                        }
                        if (!$lsp->is_ult_demer_disabled)
                        {
                             $sDemerit .= "<td class=tableContent>".$dataArr[$i][10]."</td>";
                        }
                        $sDemerit .= "</tr>\n";
                        $counter2 ++;
                }
        }
        $sDemerit .= "<tr><td colspan=2 align=right style='border-right: solid #FFFFFF; border-left:solid #FFFFFF; border-bottom:solid #FFFFFF'>$list_total:</td>\n";
        if (!$lsp->is_black_disabled)
        {
             $sDemerit .= "<td class=tableContent>".getTotal(6)."</td>";
        }
        if (!$lsp->is_min_demer_disabled)
        {
             $sDemerit .= "<td class=tableContent>".getTotal(7)."</td>";
        }
        if (!$lsp->is_maj_demer_disabled)
        {
             $sDemerit .= "<td class=tableContent>".getTotal(8)."</td>";
        }
        if (!$lsp->is_sup_demer_disabled)
        {
             $sDemerit .= "<td class=tableContent>".getTotal(9)."</td>";
        }
        if (!$lsp->is_ult_demer_disabled)
        {
             $sDemerit .= "<td class=tableContent>".getTotal(10)."</td>";
        }
        $sDemerit .= "</tr>\n";
}

$sDemerit .= "</table>\n";

if ($counter1>1 || $counter2>1)
{
	if($plugin['Discipline'])
	{
		$Merit_arr = convertTotal(1, getTotal(1),getTotal(2),getTotal(3),getTotal(4),getTotal(5));
		$Demerit_arr = convertTotal(-1, getTotal(6),getTotal(7),getTotal(8),getTotal(9),getTotal(10));
		
		$sSummary = "<br>$i_Profile_Summary <table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
                            <tr><td nowrap>$name $i_Profile_Summary_Has : </td><td width=80%>";
		$sSummary .= buildTable($Merit_arr[4],$i_Merit_UltraCredit, $Merit_arr[3],$i_Merit_SuperCredit, $Merit_arr[2],$i_Merit_MajorCredit, $Merit_arr[1],$i_Merit_MinorCredit, $Merit_arr[0],$i_Merit_Merit, $i_Merit_Award);
        $sSummary .= buildTable($Demerit_arr[4],$i_Merit_UltraDemerit, $Demerit_arr[3],$i_Merit_SuperDemerit, $Demerit_arr[2],$i_Merit_MajorDemerit, $Demerit_arr[1],$i_Merit_MinorDemerit, $Demerit_arr[0],$i_Merit_BlackMark, $i_Merit_Punishment);
	    $sSummary .= "</td></tr></table>";
	    
	    # Description of Merit/Demerit
	    $sSummary .= "
			<table width=560 border=0 cellpadding=2 cellspacing=0>
				<tr><td align=right>". buildDescription() ."</td></tr>
			</table>";
	}
	else
	{
		$sSummary = "<br>$i_Profile_Summary <table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
                            <tr><td nowrap>$name $i_Profile_Summary_Has : </td><td width=80%>";
	    $sSummary .= buildTable(getTotal(5),$i_Merit_UltraCredit, getTotal(4),$i_Merit_SuperCredit, getTotal(3),$i_Merit_MajorCredit, getTotal(2),$i_Merit_MinorCredit, getTotal(1),$i_Merit_Merit, $i_Merit_Award);
	    $sSummary .= buildTable(getTotal(10),$i_Merit_UltraDemerit, getTotal(9),$i_Merit_SuperDemerit, getTotal(8),$i_Merit_MajorDemerit, getTotal(7),$i_Merit_MinorDemerit, getTotal(6),$i_Merit_BlackMark, $i_Merit_Punishment);
	    $sSummary .= "</td></tr></table>";
	}
    
}


/*
# ����: 3 �i 1
if ($counter1>1 || $counter2>1)
{
        $Merit_arr = convertTotal(getTotal(1),getTotal(2),getTotal(3),getTotal(4),getTotal(5));
        $Demerit_arr = convertTotal(getTotal(6),getTotal(7),getTotal(8),getTotal(9),getTotal(10));

        $sSummary = "<br>$i_Profile_Summary <table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
                                <tr><td nowrap>$name $i_Profile_Summary_Has : </td><td width=80%>";
        $sSummary .= buildTable($Merit_arr[4],$i_Merit_UltraCredit, $Merit_arr[3],$i_Merit_SuperCredit, $Merit_arr[2],$i_Merit_MajorCredit, $Merit_arr[1],$i_Merit_MinorCredit, $Merit_arr[0],$i_Merit_Merit, $i_Merit_Award);
        $sSummary .= buildTable($Demerit_arr[4],$i_Merit_UltraDemerit, $Demerit_arr[3],$i_Merit_SuperDemerit, $Demerit_arr[2],$i_Merit_MajorDemerit, $Demerit_arr[1],$i_Merit_MinorDemerit, $Demerit_arr[0],$i_Merit_BlackMark, $i_Merit_Punishment);
        $sSummary .= "</td></tr>
                                </table>
                                <table width=560 border=0 cellpadding=2 cellspacing=0>
                                <tr><td align=right>
                                $i_Profile_Summary_Conversion: 3 $i_Profile_Summary_Conversion_To 1
                                </td></tr>
                                </table>
                                ";
}
*/

$functionbar = (trim($keyword)!="") ? "&nbsp; $i_Attendance_Reason/$i_Attendance_Date: $keyword" : "";
$functionbar2 = ($yrfilter!="")? "&nbsp; $i_Profile_Year: $yrfilter" :"";

?>

<form onsubmit="return false;">
<?=$i_Profile_Merit_Analysis.displayArrow().$lu->ClassName.displayArrow().$name?>
<br><?=$functionbar?>
<br><?=$functionbar2?>
<br><br>
<?= $i_Merit_Award . $sMerit ?>
<br><br>
<?= $i_Merit_Punishment . $sDemerit ?>
<br>

<?= $sSummary ?>

<br>
<table border=0 width=550>
<tr><td align=center>
<input type=image class=printImg src="/images/admin/button/s_btn_print_<?=$intranet_session_language?>.gif" border="0" onclick="window.print(); return false;">
</td></tr>
</table>

</form>

<?
include_once("../../../templates/filefooter.php");

?>