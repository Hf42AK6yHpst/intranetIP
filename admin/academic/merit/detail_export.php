<?

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$li = new libdb();
$lexport = new libexporttext();

$conds = "";
if ($yrfilter != "")
{
    $conds .= " AND a.Year = '$yrfilter'";
}
if ($class != "")
{
    $conds .= " AND b.ClassName = '$class'";
}
if ($pic != "")
{
    $conds .= " AND a.PersonInCharge = '$pic'";
}
if ($RecordType != "")
{
    $conds .= " AND a.RecordType = '$RecordType'";
}
if ($reason != "")
{
    $conds .= " AND a.Reason = '$reason'";
}
if ($datetype != 0)
{
    $date_field = "";
    if ($datetype == 1)
    {
        $date_field = "a.MeritDate";
    }
    else if ($datetype == 2)
    {
         $date_field = "a.DateModified";
    }
    if ($date_field != "")
    {
        if ($date_from != "")
        {
            $conds .= " AND $date_field >= '$date_from'";
        }
        if ($date_to != "")
        {
            $conds .= " AND $date_field <= '$date_to 23:59:59'";
        }
    }
}

$studentnamefield = getNameFieldWithClassNumberByLang("b.");
$namefield = getNameFieldWithLoginByLang("c.");
$sql = "SELECT
              DATE_FORMAT(a.MeritDate,'%Y-%m-%d'),
              $studentnamefield,
              a.Year,a.Semester,
              CASE a.RecordType
                   WHEN 1 THEN '$i_Merit_Merit'
                   WHEN 2 THEN '".addslashes($i_Merit_MinorCredit)."'
                   WHEN 3 THEN '".addslashes($i_Merit_MajorCredit)."'
                   WHEN 4 THEN '".addslashes($i_Merit_SuperCredit)."'
                   WHEN 5 THEN '".addslashes($i_Merit_UltraCredit)."'
                   WHEN -1 THEN '$i_Merit_BlackMark'
                   WHEN -2 THEN '$i_Merit_MinorDemerit'
                   WHEN -3 THEN '$i_Merit_MajorDemerit'
                   WHEN -4 THEN '$i_Merit_SuperDemerit'
                   WHEN -5 THEN '$i_Merit_UltraDemerit'
                   ELSE '-' END,
              a.NumberOfUnit,
              a.Reason,
              IF(a.PersonInCharge IS NULL OR a.PersonInCharge=0,'--',$namefield),
              a.DateModified
        FROM PROFILE_STUDENT_MERIT as a
                 LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                 LEFT OUTER JOIN INTRANET_USER as c ON a.PersonInCharge = c.UserID
        WHERE (a.MeritDate like '%$keyword%'
                   OR a.Reason like '%$keyword%'
                   OR b.EnglishName like '%$keyword%'
                   OR b.ChineseName like '%$keyword%'
                   OR b.UserLogin like '%$keyword%'
                   OR b.ClassNumber like '%$keyword%'
                   ) $conds
              ";

$result = $li->returnArray($sql,9);

$x = "RecordDate, StudentName, Year, Semester, Type, NumberOfUnit, Reason, PIC, DateModified\n";
$exportColumn = array("RecordDate", "StudentName", "Year", "Semester", "Type", "NumberOfUnit", "Reason", "PIC", "DateModified");
/*
for ($i=0; $i<sizeof($result); $i++)
{
     $delim = "";
     for ($j=0; $j<sizeof($result[$i]); $j++)
     {
          $x .= "$delim\"".str_replace('"', '""', intranet_undo_htmlspecialchars($result[$i][$j]))."\"";
          $delim = ",";
     }
     $x .= "\n";
}
*/
// Output the file to user browser
$filename = "merit_data.csv";
/*
header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($x) );
header("Content-Disposition: attachment; filename=\"".$filename."\"");

echo $x;
*/

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);


intranet_closedb();
?>
