<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
intranet_opendb();

$li = new libuser($studentid);
$classname = $li->ClassName;
$classnumber = $li->ClassNumber;

$reason = intranet_htmlspecialchars(trim($reason));
$remark = intranet_htmlspecialchars(trim($remark));
$meritdate = intranet_htmlspecialchars(trim($meritdate));
$qty = intranet_htmlspecialchars(trim($qty));
//$year = intranet_htmlspecialchars(trim($year));
//$semester = intranet_htmlspecialchars(trim($semester));

list($AcademicYearID, $year, $YearTermID, $semester) = getAcademicYearInfoAndTermInfoByDate($meritdate);

$fieldname = "UserID, Year ,Semester, MeritDate, RecordType, NumberOfUnit, Reason,Remark, DateInput,DateModified,ClassName,ClassNumber";
$fieldvalue = "'$studentid','$year','$semester','$meritdate','$type','$qty','$reason','$remark',now(),now(),'$classname','$classnumber'";
# PersonInCharge
if (isset($PersonInCharge) && $PersonInCharge!='')
{
     $fieldname .= ",PersonInCharge";
     $fieldvalue .= ",'$PersonInCharge'";
}

$sql = "INSERT INTO PROFILE_STUDENT_MERIT ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: studentview.php?studentid=$studentid&msg=1&classid=$classid");
?>