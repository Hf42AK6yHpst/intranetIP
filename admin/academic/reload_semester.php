<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_opendb();

$libsubject_ui = new subject_class_mapping_ui();

$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
$SelectedYearTermID = stripslashes($_REQUEST['SelectedYearTermID']);
$onChangeSubmitStr = stripslashes($_REQUEST['onChangeSubmitStr']);
$displayAll = stripslashes($_REQUEST['displayAll']);

if($AcademicYearID)
	$returnString = $libsubject_ui->Get_Term_Selection('YearTermID', $AcademicYearID, $SelectedYearTermID, $onChangeSubmitStr, 0, '', $displayAll, $i_status_all);

echo $returnString;

intranet_closedb();
	
?>