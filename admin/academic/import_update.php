<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
if ($plugin['Discipline'])
{
include_once("../../../includes/libdiscipline.php");
}
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$file_format = array("Year","Semester","Date","Class","Class Number","Type","Time Slot","Reason");

$li = new libdb();
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;
$warning_student_ids = array();

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php");
} else {
        $ext = strtoupper($lo->file_ext($filename));
        if($ext == ".CSV")
        {
           # read file into array
           # return 0 if fail, return csv array if success
           $data = $lo->file_read_csv($filepath);
           $col_name = array_shift($data);                   # drop the title bar

           #Check file format
           $format_wrong = false;
           for ($i=0; $i<sizeof($file_format); $i++)
           {
                if ($col_name[$i]!=$file_format[$i])
                {
                    $format_wrong = true;
                    break;
                }
           }
           if ($format_wrong)
           {
               $display .= "<blockquote><br>$i_import_invalid_format <br>\n";
               $display .= "<table width=100 border=1>\n";
               for ($i=0; $i<sizeof($file_format); $i++)
               {
                    $display .= "<tr><td>".$file_format[$i]."</td></tr>\n";
               }
               $display .= "</table></blockquote>\n";
           }
           else
           {

               $t_delimiter = "";
               $toTempValues = "";
               $today = date('Y-m-d');
               $currentYear = getCurrentAcademicYear();
               $currentSem = getCurrentSemester();
               for ($i=0; $i<sizeof($data); $i++)
               {
                    list($year,$sem,$attenddate,$class,$classnum,$type,$slot,$reason) = $data[$i];
                    $attenddate = (($attenddate == "")? $today: $attenddate);
                    $year = ($year == ""? $currentYear : $year);
                    $sem = ($sem == ""? $currentSem : $sem);
                    $class = trim(addslashes($class));
                    $classnum = trim(addslashes($classnum));
                    $reason = trim(addslashes($reason));
                    if ($class != "" && $classnum != "")
                    {
                        $toTempValues .= "$t_delimiter ('$attenddate','$year','$sem','$class','$classnum','$type','$slot','$reason')";
                        $t_delimiter = ",";
                    }
               }

               # Create temp table
               $sql = "CREATE TEMPORARY TABLE TEMP_ATTENDANCE_IMPORT (
                              AttendanceDate datetime,
                              Year char(20),
                              Semester char(20),
                              ClassName varchar(255),
                              ClassNumber varchar(255),
                              RecordType char(2),
                              Slot char(2),
                              Reason text
                              )";
               $li->db_db_query($sql);
               $fields_name = "AttendanceDate, Year,Semester,ClassName, ClassNumber, RecordType, Slot,Reason";
               $sql = "INSERT IGNORE INTO TEMP_ATTENDANCE_IMPORT ($fields_name) VALUES $toTempValues";
               $li->db_db_query($sql);

               if ($plugin['Discipline'])
               {
                   $sql = "SELECT DISCTINCT a.UserID FROM TEMP_ATTENDANCE_IMPORT as b
                                  LEFT OUTER JOIN INTRANET_USER as a ON b.ClassName = a.ClassName
                                             AND b.ClassNumber = a.ClassNumber
                                             AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                                  WHERE a.UserID IS NOT NULL
                                             ";
                   $target_students = $li->returnVector($sql);
               }

               $fields_name = "UserID, AttendanceDate, Year, Semester, RecordType, DayType, Reason, DateInput, DateModified, ClassName, ClassNumber";
               $sql = "INSERT IGNORE INTO PROFILE_STUDENT_ATTENDANCE ($fields_name)
                              SELECT a.UserID, b.AttendanceDate, b.Year, b.Semester,
                                     CASE b.RecordType
                                          WHEN 'A' THEN 1
                                          WHEN 'L' THEN 2
                                          WHEN 'E' THEN 3 END,
                                     CASE b.Slot
                                          WHEN 'WD' THEN 1
                                          WHEN 'AM' THEN 2
                                          WHEN 'PM' THEN 3 END,
                                     b.Reason,now(),now(),
                                     a.ClassName, a.ClassNumber
                                     FROM INTRANET_USER as a, TEMP_ATTENDANCE_IMPORT as b
                                          WHERE a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber
                                          AND a.RecordStatus IN (0,1,2)
                                          AND a.RecordType = 2
                                     ";
               $li->db_db_query($sql);

               if ($plugin['Discipline'])
               {
                   $ldiscipline = new libdiscipline();
                   for ($i=0; $i<sizeof($target_students); $i++)
                   {
                        $student_id = $target_students[$i];
                        if($ldiscipline->calculateUpgradeLateToDemerit($student_id))
                        {
                           $warning_student_ids[] = $student_id;
                        }
                        $ldiscipline->calculateUpgradeLateToDetention($student_id);
                   }
               }


               if (sizeof($warning_student_ids)!=0)
               {
                   $body_tags = "onLoad=document.form1.submit()";
                   include_once("../../../templates/fileheader.php");
                   ?>
                   <form name=form1 action=shownotice_msg_prompt.php method=POST>
                   <?
                   for ($i=0; $i<sizeof($warning_student_ids); $i++)
                   {
                   ?>
                     <input type=hidden name=WarningStudentID[] value="<?=$warning_student_ids[$i]?>">
                   <?
                   }
                   ?>
                   <input type=hidden name=studentid value="<?=$studentid?>">
                   <input type=hidden name=classid value="<?=$classid?>">
                   </form>
                   <?
                   include_once("../../../templates/filefooter.php");
               }
               else
               {
                   header("Location: index.php?msg=1");
               }
           }
           if ($display != '')
           {
               include_once("../../../templates/adminheader_setting.php");
               echo displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Attendance, 'index.php', $button_import, '');
               echo displayTag("head_attendance_$intranet_session_language.gif", $msg);
               if (sizeof($warning_student_ids)!=0)
               {
                   $link_cont = "document.form1.submit()";
               }
               else
               {
                   $link_cont = "history.go(-1)";
               }
               $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                               <tr><td><hr size=1></td></tr>
                               <tr><td align='right'><a href='javascript:$link_cont'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";
               ?>
               <form name=form1 action=shownotice_msg_prompt.php method=POST>
               <?
               for ($i=0; $i<sizeof($warning_student_ids); $i++)
               {
                   ?>
                     <input type=hidden name=WarningStudentID[] value="<?=$warning_student_ids[$i]?>">
                   <?
               }
                   ?>
                   <input type=hidden name=studentid value="<?=$studentid?>">
                   <input type=hidden name=classid value="<?=$classid?>">
                   </form>
                   <?
               echo $display;
               include_once("../../../templates/adminfooter.php");
           }

        }
        else
        {
            header("Location: import.php");
        }


}

intranet_closedb();
?>