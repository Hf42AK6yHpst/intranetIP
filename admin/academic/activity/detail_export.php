<?

###############################
#
# 	Date:	2015-12-07 (Bill)	[2015-1008-1356-44164]
#			Improved: added column for Activity Date
#
#	Date:	2013-03-27	Ivan [2013-0325-0947-16156]
#			- Improved: Added record type in the exported csv file for activity records of student profile (customization)
#
#	Date:	2011-03-07	YatWoon
#			- Improved: display "Whole Year" is semester is empty
#
###############################

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$li = new libdb();
$lexport = new libexporttext();

$conds = "";
if ($yrfilter != "")
{
    $conds .= " AND a.Year = '$yrfilter'";
}
if ($role != "")
{
    $conds .= " AND UPPER(a.Role) like UPPER('%$role%')";
}
if ($class != "")
{
    $conds .= " AND b.ClassName = '$class'";
}
if ($performance != "")
{
    $conds .= " AND UPPER(a.Performance) like UPPER('%$performance%')";
}
if ($ActivityName != "")
{
    $conds .= " AND UPPER(a.ActivityName) like UPPER('%$ActivityName%')";
}
if ($datetype != 0)
{
    $date_field = "";
    if ($datetype == 1)
    {
        $date_field = "a.DateInput";
    }
    else if ($datetype == 2)
    {
         $date_field = "a.DateModified";
    }
    if ($date_field != "")
    {
        if ($date_from != "")
        {
            $conds .= " AND $date_field >= '$date_from'";
        }
        if ($date_to != "")
        {
            $conds .= " AND $date_field <= '$date_to 23:59:59'";
        }
    }
}

if ($special_feature['activity_internal_external']) {
	$recordTypeField = " , IF (a.RecordType = 1, 'Internal', 'External') ";
}

$studentnamefield = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT
			a.Year,
			 IF (a.Semester='','". $Lang['General']['WholeYear'] ."',a.Semester) as Semester,
			a.ActivityDate,
			b.ClassName, b.ClassNumber,
			$studentnamefield, 
			a.ActivityName,
			a.Role,
			a.Performance,
			a.Remark,
			a.Organization
			$recordTypeField
        FROM 
        			PROFILE_STUDENT_ACTIVITY as a
        			INNER JOIN 
        			INTRANET_USER b
        			ON a.UserID = b.UserID
        WHERE 
              (b.ChineseName like '%$keyword%' 
               OR b.EnglishName like '%$keyword%' 
               OR a.ActivityName like '%$keyword%' 
               OR a.Year like '%$keyword%' 
               OR a.Semester like '%$keyword%' 
               OR a.Role like '%$keyword%' 
               OR a.Performance like '%$keyword%' 
              ) 
              $conds
		Order By
			  b.ClassName, b.ClassNumber
        ";

$result = $li->returnArray($sql);
/*
for ($i=0; $i< sizeof($result); $i++) {
	for ($j=0;$j < 7; $j++) {
		$result[$i][$j] = '"'.$result[$i][$j].'"';
	}
}
*/
// $x = "\"StudentName\",\"Year\",\"Semester\",\"ActivityName\",\"Role\",\"Performance\",\"DateModified\"\n";
$exportColumn = array("Year", "Semester","Date","Class","Class Number","StudentName", "Activity Name", "Role", "Performance", "Remark","Organization");
if ($special_feature['activity_internal_external']) {
	$exportColumn[] = 'Type';
}
/*
for ($i=0; $i<sizeof($result); $i++)
{
     $delim = "";
     for ($j=0; $j<sizeof($result[$i]); $j++)
     {
          $x .= "$delim\"".str_replace('"', '""', intranet_undo_htmlspecialchars($result[$i][$j]))."\"";
          $delim = ",";
     }
     $x .= "\n";
}
*/
// Output the file to user browser
$filename = "activity_data.csv";
/*
header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($x) );
header("Content-Disposition: attachment; filename=\"".$filename."\"");

echo $x;
*/

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);


intranet_closedb();
?>
