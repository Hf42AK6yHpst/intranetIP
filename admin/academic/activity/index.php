<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

if ($plugin['eEnrollment'] == true)
{
	$DisplayOptions = displayOption2($i_Activity_ViewStudent, 'classview.php', 1);
}
else
{
	$DisplayOptions = displayOption2(	$i_Activity_ViewStudent, 'classview.php', 1,
										$i_Activity_ArchiveRecord, 'archive.php', 1);
}

?>
<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Activity, '') ?>
<?= displayTag("head_activity_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td class=tableContent>
<br>
<blockquote>
<?= $DisplayOptions ?>
</blockquote>
<br>
</td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");

?>