<?php

// using: 

/*******************************************
 * Date: 	2013-03-15 (Rita)
 * Details:	add redirection variables 
 *******************************************/
 
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$list = implode(",",$StudentActivityID);
$sql = "DELETE FROM PROFILE_STUDENT_ACTIVITY WHERE StudentActivityID IN ($list)";
$li->db_db_query($sql);

intranet_closedb();
if ($page_from){
	header("Location: $page_from?msg=3&yrfilter=$yrfilter&class=$class&ActivityName=$ActivityName&role=$role&datetype=$datetype&date_from=$date_from&date_to=$date_to&performance=$performance");
}else{
	header("Location: studentview.php?classid=$classid&studentid=$studentid&msg=3");
}
?>