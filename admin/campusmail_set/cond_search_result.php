<?php
// editing by 

################ Change Log [Start] #####################
#	Date	:	2019-10-28 [Carlos]
#				Added data column [Receiver Name] for easier classify which user's email.
#
#	Date	:	2018-06-27 [Carlos]
#				Fine tune the timeout limit and search records split range.
#
#	Date	:	2016-04-28 [Carlos]
#				For large amount of campusmail records, improved to do search by splitting record set into 10 parts to avoid memory burst and timeout. 
#
#	Date	:	2011-08-26 [Carlos]
#				Limit keyword search to mail subject only (Other fields are not key index may introduce perfoemance issue)
#				
#	Date	:	2011-06-20 [Carlos]
#				Added search conditions sender and date range 
#
#	Date	:	2010-12-07 [Yuen]
#				Limit keyword search to mail subject only and hide message from the result list
#
################ Change Log [End] #####################
SET_TIME_LIMIT(1*60*60); // 1 hour
ini_set("memory_limit", "1024M");
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");

intranet_opendb();

session_write_close();

if ($special_feature['imail'])
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}


//if($SYS_CONFIG['Mail']['hide_imail'] === true )
//{
//	include_once("../../includes/imap_gamma.php");
//	$IMap = new imap_gamma($skipLogin=1);
//	$IMap->searchEamil($keyword);
//	
//	die;
//}

# Create Search Result
if ($newsearch == 1)
{
    $ldb = new libdb();
    $sql = "DELETE FROM TEMP_CAMPUSMAIL_COND_REMOVE";
    $ldb->db_db_query($sql);
    
    $sql = "SELECT MAX(CampusMailID) FROM INTRANET_CAMPUSMAIL";
	$max_record = $ldb->returnVector($sql);
	$max_campusmailid = $max_record[0];

	if($max_campusmailid > 300000){
		// if more than 300000 records, split it to do search 
		$interval = intval($max_campusmailid / 20);
		$interval = min(100000, $interval);
		for($i=1;$i<=$max_campusmailid;$i+=$interval){
			$sql = "INSERT IGNORE INTO TEMP_CAMPUSMAIL_COND_REMOVE (CampusMailID)
                   		SELECT CampusMailID FROM INTRANET_CAMPUSMAIL
                          WHERE (CampusMailID BETWEEN '$i' AND '".($i+$interval)."') AND (Subject like '%$keyword%')";
        	$ldb->db_db_query($sql);
        	usleep(100);
        	echo " ";
        	flush();
		}
	}else{
	    # Search result
	    $sql = "INSERT IGNORE INTO TEMP_CAMPUSMAIL_COND_REMOVE (CampusMailID)
					SELECT CampusMailID FROM INTRANET_CAMPUSMAIL
						WHERE Subject like '%$keyword%' ";
	    
	    /*
	    $sql = "INSERT IGNORE INTO TEMP_CAMPUSMAIL_COND_REMOVE (CampusMailID)
					SELECT CampusMailID FROM INTRANET_CAMPUSMAIL
						WHERE (Subject like '%$keyword%' OR Message like '%$keyword%')";
		*/
		/*
		$sql = "INSERT IGNORE INTO TEMP_CAMPUSMAIL_COND_REMOVE (CampusMailID)
					(SELECT CampusMailID FROM INTRANET_CAMPUSMAIL as c 
							LEFT JOIN INTRANET_USER as u ON u.UserID = c.SenderID 
						WHERE (c.Subject LIKE '%$keyword%' 
								OR c.SenderEmail LIKE '%$keyword%'
								OR u.EnglishName LIKE '%$keyword%'
								OR u.ChineseName LIKE '%$keyword%') "; 
		$StartDate = trim($StartDate);
		$EndDate = trim($EndDate);
		if($StartDate != ''){
			$start_ts = strtotime("$StartDate 00:00:00");
			$sql .= "AND UNIX_TIMESTAMP(c.DateInput) >= '$start_ts' ";
		}
		if($EndDate != ''){
			$end_ts = strtotime("$EndDate 23:59:59");
			$sql .= "AND UNIX_TIMESTAMP(c.DateInput) <= '$end_ts' ";
		}
		$sql .= " )";
		*/
	    $ldb->db_db_query($sql);
	}
}


# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
$namefield = getNameFieldWithClassNumberByLang("u.");
$namefield2 = getNameFieldWithClassNumberByLang("u2.");
/*
$sql = "SELECT m.DateModified, $namefield as sendername, m.Subject, m.Message,
        CONCAT('<input type=checkbox name=CampusMailID[] value=', m.CampusMailID ,'>')
               FROM TEMP_CAMPUSMAIL_COND_REMOVE as r
                    LEFT OUTER JOIN INTRANET_CAMPUSMAIL as m ON m.CampusMailID = r.CampusMailID
                    LEFT OUTER JOIN INTRANET_USER as u ON m.SenderID = u.UserID";
*/
$sql = "SELECT m.DateModified, IF(m.SenderID IS NULL,m.SenderEmail,$namefield) as SenderName, $namefield2 as ReceiverName, m.Subject,
        CONCAT('<input type=checkbox name=CampusMailID[] value=', m.CampusMailID ,'>')
               FROM TEMP_CAMPUSMAIL_COND_REMOVE as r
                    INNER JOIN INTRANET_CAMPUSMAIL as m ON m.CampusMailID = r.CampusMailID
                    LEFT JOIN INTRANET_USER as u ON m.SenderID = u.UserID 
					LEFT JOIN INTRANET_USER as u2 ON m.UserID=u2.UserID ";


# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
# hide message on 2010-12-07
//$li->field_array = array("m.DateModified","sendername","m.Subject","m.Message");
$li->field_array = array("m.DateModified","SenderName","ReceiverName","m.Subject");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_frontpage_campusmail_date)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_frontpage_campusmail_sender)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_frontpage_campusmail_recipients)."</td>\n";
$li->column_list .= "<td width=44% class=tableTitle>".$li->column($pos++, $i_frontpage_campusmail_subject)."</td>\n";
//$li->column_list .= "<td width=35% class=tableTitle>".$li->column($pos++, $i_frontpage_campusmail_message)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("CampusMailID[]")."</td>\n";

$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'CampusMailID[]','cond_search_remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_fs, '', $mail_set_title, 'index.php',$i_CampusMail_New_RemoveBySearch,'') ?>
<?= displayTag("$img_tag", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type="hidden" name="keyword" value="<?=str_replace('"', "&quot;", stripslashes($keyword))?>" />
<input type="hidden" name="StartDate" value="<?=str_replace('"',"&quot;",stripslashes($StartDate))?>" />
<input type="hidden" name="EndDate" value="<?=str_replace('"',"&quot;",stripslashes($EndDate))?>" />
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>