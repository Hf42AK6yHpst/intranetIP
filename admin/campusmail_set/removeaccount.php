<?php
// editing by 
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libwebmail.php");
include_once("../../includes/libdb.php");

intranet_opendb();

$li = new libdb();
$lwebmail = new libwebmail();

$sql = "SELECT a.UserLogin, b.ACL FROM INTRANET_USER as a
               LEFT OUTER JOIN INTRANET_SYSTEM_ACCESS as b ON a.UserID = b.UserID
               WHERE a.UserID = $uid";
$temp = $li->returnArray($sql,2);
list($userlogin, $acl) = $temp[0];

if ($userlogin == "")
{
    header("Location: index.php");
    exit();
}

if($plugin['imail_gamma']===true){
	include_once("../../includes/imap_gamma.php");
	include_once("../../includes/libaccountmgmt.php");
	$IMap = new imap_gamma(1);
	$laccount = new libaccountmgmt();
	if($sys_custom['iMailPlus']['EmailAliasName']){
		$sql = "SELECT ImapUserLogin FROM INTRANET_USER WHERE UserID='$uid'";
		$imapUserLoginRecord = $li->returnVector($sql);
		$imapUserLogin = trim($imapUserLoginRecord[0]);
		$IMapUserEmail = ($imapUserLogin!=''? $imapUserLogin : strtolower($userlogin))."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	}else{
		$IMapUserEmail = strtolower($userlogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	}
	if($IMap->is_user_exist($IMapUserEmail)){
		if($IMap->delete_account($IMapUserEmail)){
			$laccount->setIMapUserEmail($uid,"");
			$IMap->setIMapUserEmailQuota($uid,0);
			if($sys_custom['iMailPlus']['EmailAliasName']){
				$sql = "UPDATE INTRANET_USER SET ImapUserLogin=NULL WHERE UserID='$uid'";
				$li->db_db_query($sql);	
			}
		}
	}
	if($IMap->IMapCache !== false) $IMap->IMapCache->Close_Connect();
}else{
	$lwebmail->delete_account($userlogin);
	
}

if ($acl == "")
{
    $sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($uid, 0)";
    $li->db_db_query($sql);
}
else
{
    $sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = $uid AND ACL IN (1,3)";
    $li->db_db_query($sql);
}

if ($type==1)       # From Type List
{
    $url = "list_type.php?UserType=$target&ClassName=$ClassName&msg=2";
}
else if ($type==2)    # From Group List
{
    $url = "list_group.php?GroupID=$target&msg=2";
}
else     # From Login name input
{
    $url = "index.php";
}

intranet_closedb();
header("Location: $url");
?>