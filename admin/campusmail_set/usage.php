<?php
// editing by 
/******************************************  Changes ******************************************
 * 2011-09-16 (Carlos): Hide internet mail option if no webmail available
 **********************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libwebmail.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

$li = new libfilesystem();
$lwebmail = new libwebmail();

$file_content = $li->file_read($intranet_root."/file/campusmail_set_usage.txt");
$set_data = explode("\n",$file_content);
$in_disabled = (trim($set_data[0])=="1");
$ex_disabled = (trim($set_data[1])=="1");

$has_webmail = $special_feature['imail'] && $lwebmail->has_webmail && ($lwebmail->type==3);

?>
<form name="form1" action="usage_update.php" method="post">
<?= displayNavTitle($i_admintitle_fs, '', $i_CampusMail_New_iMail_Settings, 'index.php',$i_campusmail_usage_setting,'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<table width=400 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=3>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_CampusMail_New_MailSource_Internal?></td><td align=center>
<input type=radio name="internal_disabled" id="in_en" value="0" <?=(!$in_disabled?"CHECKED":"")?> /><LABEL for="in_en"><?=$i_general_enabled?></LABEL> &nbsp;
<input type=radio name="internal_disabled" id="in_dis" value="1" <?=($in_disabled?"CHECKED":"")?> /><LABEL for="in_dis"><?=$i_general_disabled?></LABEL>
</td></tr>
<?php
if($has_webmail)
{
?>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_CampusMail_New_MailSource_External?></td><td align=center>
<input type=radio name="external_disabled" id="ex_en" value="0" <?=(!$ex_disabled?"CHECKED":"")?> /><LABEL for="ex_en"><?=$i_general_enabled?></LABEL> &nbsp;
<input type=radio name="external_disabled" id="ex_dis" value="1" <?=($ex_disabled?"CHECKED":"")?> /><LABEL for="ex_dis"><?=$i_general_disabled?></LABEL>
</td></tr>
<?php 
}
?>
</table>

</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>