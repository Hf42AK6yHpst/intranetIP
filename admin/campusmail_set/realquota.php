<?php
// editing by 
/******************************************* Change log ****************************************************
 * 2011-10-19 (Carlos): added alumni type
 ***********************************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$file_content = get_file_content($intranet_root."/file/campusmail_set.txt");
$id_names = array($i_identity_teachstaff,$i_identity_student,$i_identity_parent);
if($special_feature['alumni']) $id_names[] = $i_identity_alumni;

if ($file_content == "")
{
    $access_right = array(1,1,1);
    $userquota = array(10,10,10);
    if($special_feature['alumni']){
    	$access_right[] = 1;
    	$userquota[] = 10;
    }
}
else
{
    $content = explode("\n", $file_content);
    unset($line);
    $line[] = explode(":",$content[0]);
    $line[] = explode(":",$content[1]);
    $line[] = explode(":",$content[3]);
    if($special_feature['alumni']) $line[3] = explode(":",$content[2]); // alumni UserType is 4 but saved at index 2 (count from 0)
    $access_right = array($line[0][0],$line[1][0],$line[2][0]);
    if($special_feature['alumni']) $access_right[3] = $line[3][0];
    for ($i=0; $i<sizeof($line); $i++)
    {
         $userquota[] = ( $line[$i][1]!=""? $line[$i][1]:10);
    }
}

// $i_Campusquota_basic_identity
$user_select = "<SELECT name=UserType onChange=\"this.form.type[0].checked=true\">\n";
if ($access_right[0] == 1)
    $user_select .= "<OPTION value=1>$i_identity_teachstaff</OPTION>\n";
if ($access_right[1] == 1)
    $user_select .= "<OPTION value=2>$i_identity_student</OPTION>\n";
if ($access_right[2] == 1)
    $user_select .= "<OPTION value=3>$i_identity_parent</OPTION>\n";
if ($special_feature['alumni'] && $access_right[3] == 1)
    $user_select .= "<OPTION value=4>$i_identity_alumni</OPTION>\n";
$user_select .= "</SELECT>";

$lg = new libgroup();
$AcademicYear = Get_Current_Academic_Year_ID();
$sql = "SELECT a.GroupID,CONCAT('(',b.CategoryName,') ',a.Title)
        FROM INTRANET_GROUP as a LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b ON a.RecordType = b.GroupCategoryID
        WHERE a.RecordType != 0 AND a.AcademicYearID = '$AcademicYear'
        ORDER BY b.GroupCategoryID ASC, a.Title";
$groups = $lg->returnArray($sql,2);
$group_select = getSelectByArray($groups,"name=GroupID onChange=\"this.form.type[1].checked=true\"");

if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;

?>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
         if (obj.type[0].checked) obj.action = 'real_list_type.php';
         else {
	         if(document.form1.GroupID.selectedIndex != 0){
	         	obj.action = 'real_list_group.php';
         	 } else {
	         	alert("<?=$i_Campusmail_MailQuotaSettting_SelectGroupWarning;?>");
	         	obj.action = ''; 
         	 }
		 }
}
</SCRIPT>
<form name="form1" action="" method="get" onSubmit="return checkform(this)">
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$Lang['Gamma']['UserUsageRightsAndQuota'],'quotaindex.php',$i_CampusMail_New_QuotaSettings,'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<span class="extraInfo">[<span class=subTitle><?=$i_general_SelectTarget?></span>]</span>
<br><br>
<input type=radio name=type value=0 CHECKED> <?=$i_LinuxAccount_SelectUserType?> <?=$user_select?><br>
<input type=radio name=type value=1> <?=$i_LinuxAccount_SelectGroup?> <?=$group_select?>
</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>