<?php
// editing by 
/*
 * 2013-12-04 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

## Only For iMail Gamma/Plus
if($plugin['imail_gamma']!==true){
	header("Location: index.php");
	exit();
}

$IMap = new imap_gamma(true);

$quota_alert = $IMap->getQuotaAlertSetting();

?>
<?= displayNavTitle($i_adminmenu_fs, '', $i_CampusMail_New_iMail_Settings, 'index.php',$Lang['Gamma']['QuotaAlertSettings'], '') ?>
<?php
## Return Message
$msgarr = explode("|=|",$msg);
if (count($msgarr)>1)
{
	if($msgarr[0]=="1"){
		$xmsg = "<font color=green>".$msgarr[1]."</font>";
	}else if($msgarr[0]=="0"){
		$xmsg = "<font color=red>".$msgarr[1]."</font>";
	}else
		$xmsg = "";
}else
	$xmsg = "";
?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $xmsg) ?>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" language="JavaScript">
function checkform(formObj)
{
	var percent = parseFloat( $.trim(formObj.QuotaAlert.value) );
	var percent_warn_div = $('div#quota_alert_warning');
	var is_valid = true;
	
	if((percent!=0 && percent == "") || (percent!=0 && isNaN(percent)) || percent < 0.0 || percent > 100.0){
		percent_warn_div.show();
		is_valid = false;
	}else{
		percent_warn_div.hide();
	}
	
	if(is_valid){
		formObj.submit();
	}
}
</script>
<form id="form1" name="form1" action="quota_alert_settings_update.php" method="post" onsubmit="checkform(this);return false;">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td>
			<blockquote>
			<p>
				<table cellspacing="0" cellpadding="5" border="0" width="100%">
				<tbody>
					<tr>
						<td style="vertical-align: middle;width:40%;"><font color="red">*</font><?=$Lang['Gamma']['AlertWhenQuotaOver']?>:</td>
						<td style="text-align:left;width:60%;">
							<input type="text" value="<?=$quota_alert?>" maxlength="10" size="10" id="QuotaAlert" name="QuotaAlert" class="text"> % <?=$Lang['Gamma']['ZeroToDisable']?>
							<div id="quota_alert_warning" style="display:none"><font color="red">*<span><?=$Lang['Gamma']['Warning']['PleaseInputNumber0to100']?></span></font></div>
						</td>
					</tr>
				</tbody>
				</table>
			</p>
			<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
				<tr><td><span class="tabletextrequire"><?=str_replace('*','<font color="red">*</font>',$Lang['General']['RequiredField'])?></span></tr>
			</table>
			</BLOCKQUOTE>
		</td>
	</tr>
	<tr>
		<td height="22" style="vertical-align:bottom"><hr size=1></td>
	</tr>
	<tr>
		<td align="right">
			<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 			<?= btnReset() ?>
		</td>
	</tr>
</table>
</form>
<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>