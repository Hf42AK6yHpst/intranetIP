<?php
#Using By : 
/****************************************************** Change Log ***********************************************
 * 2011-02-08 (Carlos): iMail Gamma - modified to call only one OSAPI Set_Batch_Quota() to update all user quota
 *****************************************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libwebmail.php");
intranet_opendb();

$lwebmail = new libwebmail();
$li = new libdb();

if ($quota != "")
{
	if($plugin['imail_gamma'] === true)
	{
		include_once("../../includes/imap_gamma.php");
		$IMap = new imap_gamma($skipLogin = 1);
		$isAdd = ($force == 0);
		$UserIDSql = implode(",",(array)$UserIDArr);
		/*
		$sql = "SELECT UserID, IMapUserEmail FROM INTRANET_USER WHERE UserID IN ($UserIDSql)";
		$Result = $li->returnArray($sql);
		$EmailArr = BuildMultiKeyAssoc($Result, "UserID", "IMapUserEmail");
		
		foreach((array)$UserIDArr as $thisUserID)
		{
			$IMap->SetTotalQuota($EmailArr[$thisUserID]["IMapUserEmail"],$quota,$thisUserID,$isAdd);
		}
		*/
		$sql = "SELECT 
					u.UserID, u.IMapUserEmail, q.GammaQuota 
				FROM INTRANET_USER as u 
				LEFT JOIN INTRANET_CAMPUSMAIL_USERQUOTA as q ON q.UserID = u.UserID 
				WHERE u.UserID IN ($UserIDSql)";
		$EmailArr = $li->returnArray($sql);
		
		$userIDArr = array();
		$loginArr = array();
		$quotaArr = array();
		for($i=0;$i<sizeof($EmailArr);$i++){
			list($uid,$imap_email,$gamma_quota) = $EmailArr[$i];
			if($isAdd){
				$new_gamma_quota = $gamma_quota + $quota;
			}else{
				$new_gamma_quota = $quota;
			}
			if($gamma_quota != $new_gamma_quota){// update only when quota differs with existing quota
				$userIDArr[] = $uid;
				$loginArr[] = $imap_email;
				$quotaArr[] = $new_gamma_quota;
			}
		}
		
		if(sizeof($loginArr)>0){
			$set_result = $IMap->Set_Batch_Quota($loginArr,$quotaArr);
			$resultArr = explode(",",$set_result);
			for($i=0;$i<sizeof($resultArr);$i++){
				if($resultArr[$i]=="1"){// update quota success, sync DB quota
					$IMap->setIMapUserEmailQuota($userIDArr[$i],$quotaArr[$i]);
				}
			}
		}
	}
	else
	{
		# Retrieve Quota
		$list = $lwebmail->getQuotaTable("iMail");
		for ($i=0; $i<sizeof($list); $i++)
		{
			 list($login,$used,$soft,$hard) = $list[$i];
			 $current_quota[$login] = array($used,$soft);
			 /*
			 echo '<pre>';
			 echo $login.','.$used.','.$soft.','.$hard;
			 print_r($current_quota[$login]);
			 echo '</pre>';
			 */
		}
	
		if ($type == 0)
		{
			$sql = "SELECT UserLogin, UserID FROM INTRANET_USER WHERE RecordType = $UserType ORDER BY ClassName, ClassNumber, EnglishName";
		}
		else if ($type == 1)
		{
			$sql = "SELECT b.UserLogin, b.UserID 
					FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
					WHERE a.GroupID = $GroupID AND b.UserID IS NOT NULL
					ORDER BY b.ClassName, b.ClassNumber, b.EnglishName";
		}
		else
		{
			header("Location: group.php");
			exit();
		}
	
		$users = $li->returnArray($sql);
		// debug_r($lwebmail);
		for ($i=0; $i<sizeof($users); $i++)
		{
			$loginName = strtolower($users[$i]['UserLogin']);
			$uid = $users[$i]['UserID'];
			$loginName .= ($lwebmail->actype == 'postfix') ? '@'.$lwebmail->mailaddr_domain : '';
			
     		if($lwebmail->actype == "postfix"){
     			$user_used = round($current_quota[$loginName][0]/1024/1024,2);
     			$user_quota = round($current_quota[$loginName][1]/1024/1024,2);
     		}else{
     			$user_used = $current_quota[$loginName][0];
     			$user_quota = $current_quota[$loginName][1];
     		}
			
			// echo '<pre>';
			// echo "loginName : ".$loginName;
			// echo '</pre>';
			if (!isset($current_quota[$loginName]))
			{
				// echo "no account"."\n";
			}
			else
			{
				//if ( ($force == 1) || ($user_quota < $quota) )
				if ($force == 1)
				{
					$succeed = $lwebmail->setTotalQuota($loginName,$quota,"iMail");
					//$db_quota_succeed = $lwebmail->setDBTotalQuota($uid,$quota);
				}
				else
				{
					#echo "Set $loginName to $quota (old: $user_quota , not set)\n";
					$new_quota = $user_quota + $quota;			# calculate the new quota
					$succeed = $lwebmail->setTotalQuota($loginName,$new_quota,"iMail");		# update user quota
					//$db_quota_succeed = $lwebmail->setDBTotalQuota($uid,$new_quota);
				}
			}
			 
		}
	}
}
header("Location: group.php?msg=2");
?>