<?php
// editing by 
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
include_once("../../includes/imap_gamma.php");



if($plugin['imail_gamma'] === true){
	$IMap = new imap_gamma();
	$list = $IMap->ban_send_mail_list;
	$gamma_ban_list = $IMap->ban_ext_mail_list;
}
else
{
	$list = get_file_content("$intranet_root/file/campusmail_banlist.txt");
}

if ($special_feature['imail'])
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}

?>

<form name="form1" action="ban_update.php" method="post">
<?= displayNavTitle($i_admintitle_fs, '', $mail_set_title, 'index.php',$Lang['Gamma']['BlockUserToSend'],'') ?>
<?= displayTag("$img_tag", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>

<table border=0 cellpadding=5 cellspacing=0>
<tr><td><?=($plugin['imail_gamma']===true?$Lang['Gamma']['BanInstruction']:$i_campusmail_ban_instruction)?></td></tr>
<tr>
<td>
	<textarea name=banlist rows=10 cols=40><?=$list?></textarea>
	<a class="functionlink_new" href="javascript:newWindow('ban_add.php?fieldname=banlist',9)"><?=$Lang['Gamma']['AddUsers']?></a>
</td>
</tr>
</table>
<?if($plugin['imail_gamma'] === true){?>
<br>
<table border=0 cellpadding=5 cellspacing=0>
<tr><td><?=$Lang['Gamma']['DisallowSendReceive']?></td></tr>
<tr><td>
	<textarea name="DisallowSendReceive" rows=10 cols=40><?=$gamma_ban_list?></textarea>
	<a class="functionlink_new" href="javascript:newWindow('ban_add.php?fieldname=DisallowSendReceive',9)" style="vertical-align:bottom"><?=$Lang['Gamma']['AddUsers']?></a>
</td></tr>
</table>
<?}?>
</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>