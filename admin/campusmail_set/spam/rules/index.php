<?php
// Editing by 
/*
 * 2013-11-28 (Carlos): Created
 */
$PATH_WRT_ROOT = '../../../../';
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

if(!$plugin['imail_gamma']){
	header("Location: ../../index.php");
	intranet_closedb();
	exit;
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 0; break;
}

$order = ($order == 1) ? 1 : 0;

$keyword = stripslashes(trim($keyword));

$type = isset($type) && in_array($type,array('subject','body'))? $type : '';

$li = new libdbtable($field, $order, $pageNo);

$sql = "SELECT 
			Rule, 
			Score,
			IF(type='subject','".$Lang['Gamma']['Subject']."','".$Lang['Gamma']['Message']."') as Type,
			DateModified,
			CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"', RecordID ,'\">') as Checkbox 
		FROM MAIL_AVAS_RULES ";
if($type != '') {
	$sql .= "WHERE Type='$type' ";
}

# TABLE INFO
$li->field_array = array("Rule","Score","Type","DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(18,18,18,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=35% class=tableTitle>".$li->column($pos++, $Lang['Gamma']['Rule'])."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $Lang['Gamma']['Score'])."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $Lang['General']['Type'])."</td>\n";
$li->column_list .= "<td width=35% class=tableTitle>".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("RecordID[]")."</td>\n";

$type_selection = '<select id="type" name="type" onchange="document.form1.submit();">';
$type_selection.= '<option value="" '.($type==''?'selected':'').'>'.$Lang['Btn']['All'].'</option>';
$type_selection.= '<option value="subject" '.($type=='subject'?'selected':'').'>'.$Lang['Gamma']['Subject'].'</option>';
$type_selection.= '<option value="body" '.($type=='body'?'selected':'').'>'.$Lang['Gamma']['Message'].'</option>';
$type_selection.= '</select>';


$toolbar = "<a class=\"iconLink\" href=\"edit_rule.php\")>".newIcon().$Lang['Btn']['New']."</a>\n".toolBarSpacer().toolBarSpacer().$type_selection.toolBarSpacer();
$functionbar = "<a href=\"javascript:checkEdit(document.form1,'RecordID[]','edit_rule.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'RecordID[]','remove_rule.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

## Return Message
$msgarr = explode("|=|",$msg);
if (count($msgarr)>1)
{
	if($msgarr[0]=="1"){
		$xmsg = "<font color=green>".$msgarr[1]."</font>";
	}else if($msgarr[0]=="0"){
		$xmsg = "<font color=red>".$msgarr[1]."</font>";
	}else
		$xmsg = "";
}else
	$xmsg = "";

include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
?>
<?= displayNavTitle($i_adminmenu_fs, '', $i_CampusMail_New_iMail_Settings, '../../',$i_spam['Setting'],'../index.php',$Lang['Gamma']['RuleSettings'],'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $xmsg) ?>
<form id="form1" name="form1" action="" method="post">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td>
	</tr>
	<tr>
		<td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $functionbar); ?></td>
	</tr>
	<tr>
		<td><img src=/images/admin/table_head1.gif width=560 height=7 border=0></td>
	</tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td>
	</tr>
</table>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>
<?php
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
intranet_closedb();
?>