<?php
// editing by 
/*
 * 2013-11-28 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

## Only For iMail Gamma/Plus
if(!$plugin['imail_gamma']){
	header("Location: ../index.php");
	intranet_closedb();
	exit();
}

$li = new libdb();
$sql = "DELETE FROM MAIL_AVAS_RULES WHERE RecordID IN (".implode(",",(array)$RecordID).")";
$success = $li->db_db_query($sql);

// Call API to write rules and scores to spamassasin 
if($success){
	$IMap = new imap_gamma(true);
	$api_subject_result = $IMap->Set_AVAS_Rules('subject');
	$api_body_result = $IMap->Set_AVAS_Rules('body');
}

$msg = $success && $api_subject_result && $api_body_result?$Lang['General']['ReturnMessage']['DeleteSuccess']:$Lang['General']['ReturnMessage']['DeleteUnsuccess'];

intranet_closedb();
header("Location: index.php?msg=".urlencode($msg));
?>