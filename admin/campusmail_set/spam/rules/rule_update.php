<?php
// editing by 
/*
 * 2013-11-28 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

## Only For iMail Gamma/Plus
if(!$plugin['imail_gamma']){
	header("Location: ../index.php");
	intranet_closedb();
	exit();
}

$li = new libdb();
if($RecordID == ""){
	$sql = "INSERT INTO MAIL_AVAS_RULES (Rule,Score,Type,DateInput,DateModified) VALUES ('$Rule','$Score','$Type',NOW(),NOW())";
}else{
	$sql = "UPDATE MAIL_AVAS_RULES SET Rule='$Rule',Score='$Score',Type='$Type',DateModified=NOW() WHERE RecordID='$RecordID'";
}
$success = $li->db_db_query($sql);

// Call API to write rules and scores to spamassasin 
if($success){
	$IMap = new imap_gamma(true);
	$api_subject_result = $IMap->Set_AVAS_Rules("subject");
	$api_body_result = $IMap->Set_AVAS_Rules("body");
	//echo $api_result;
	//intranet_closedb();
	//exit;
}

if(trim($RecordID)==""){
	// New
	$msg = $success && $api_subject_result && $api_body_result?$Lang['General']['ReturnMessage']['AddSuccess']:$Lang['General']['ReturnMessage']['AddUnsuccess'];
}else{
	// Edit
	$msg = $success && $api_result && $api_body_result?$Lang['General']['ReturnMessage']['UpdateSuccess']:$Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

intranet_closedb();
header("Location: index.php?msg=".urlencode($msg));
?>