<?php
// Editing by Carlos
/*
 * 2013-11-27 (Carlos): iMail plus added [Rule Settings]
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libwebmail.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

$lwebmail = new libwebmail();

$mail_server_quota_control = $special_feature['imail'] && $lwebmail->has_webmail && ($lwebmail->type==3);
$has_imail = $special_feature['imail'];

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_CampusMail_New_iMail_Settings, '../',$i_spam['Setting'],'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300>
<blockquote>

<?php 
if($plugin['imail_gamma']) {
	echo displayOption(
	                  $i_spam['BypassSetting'],'bypass.php',1,
	                  $i_spam['WhiteList'],'whitelist.php',1,
	                  $i_spam['BlackList'],'blacklist.php',1,
	                  $Lang['Gamma']['RuleSettings'],'rules/index.php',$sys_custom['iMailPlus']['AVAS_Rule_Settings']
	                                ); 
}else{
	echo displayOption(
	                  $i_spam['BypassSetting'],'bypass.php',1,
	                  $i_spam['WhiteList'],'whitelist.php',1,
	                  $i_spam['BlackList'],'blacklist.php',1
	                                ); 
}
?>
</blockquote>
</td></tr>
</table>

<?php
include_once("../../../templates/adminfooter.php");
?>