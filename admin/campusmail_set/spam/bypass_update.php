<?php
// editing by 
/*
 * 2014-08-26 (Carlos): Cater iMail plus and Webmail api calls
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libwebmail.php");

$lwebmail = new libwebmail();

if($plugin['imail_gamma'])
{
	include("../../../includes/imap_gamma.php");
	$IMap = new imap_gamma(1);
}

### Spam ###
if ($_POST['spam_control']=='N')
{
	//$lwebmail->setSpamControlSetting(0);
	//$lwebmail->turnOffSpamCheck();
    //$lwebmail->changeAllUserPolicy();
    if($plugin['imail_gamma']){
    	$IMap->setSpamControlSetting(0);
    	$IMap->turnOffSpamCheck();
    }else{
    	$lwebmail->setSpamControlSetting(0);
		$lwebmail->turnOffSpamCheck();
    	$lwebmail->changeAllUserPolicy();
    }
}
else
{
    # Default activated
    if($custom_control_level == 1){
	    if($plugin['imail_gamma']){
	    	$IMap->setSpamControlSetting(1);
	   		$IMap->setSpamControlLevel($spam_control_score);
	    	$IMap->turnOnSpamCheck();
	    	$IMap->changeDomainSpamScore($spam_control_score);
	    }else{
	    	$lwebmail->setSpamControlSetting(1);
		    $lwebmail->setSpamControlLevel($spam_control_score);
		    $lwebmail->turnOnSpamCheck();
		    $lwebmail->changeAllUserPolicy();
		    $lwebmail->setCustomeSpamScore($spam_control_score);
		    //if($lwebmail->actype == "postfix"){
		    //	$lwebmail->changeDomainSpamScore($spam_control_score);
		    //}
	    }
    } else {
	    //$lwebmail->setSpamControlSetting(0);
	    //$lwebmail->setSpamControlLevel('');
	    //$lwebmail->turnOnSpamCheck();
	    //$lwebmail->changeAllUserPolicy();
	    if($plugin['imail_gamma']){
	    	$IMap->setSpamControlSetting(0);
	   		$IMap->setSpamControlLevel('');
	    	$IMap->turnOnSpamCheck();
	    	$IMap->changeDomainSpamScore($IMap->readSpamScore());
	    }else{
	    	$lwebmail->setSpamControlSetting(0);
		    $lwebmail->setSpamControlLevel('');
		    $lwebmail->turnOnSpamCheck();
		    $lwebmail->changeAllUserPolicy();
		    //if($lwebmail->actype == "postfix"){
		    //	$lwebmail->changeDomainSpamScore($lwebmail->readSpamScore());
		    //}
	    }
    }
}

### Virus ###
if ($_POST['virus_scan']=='N')
{
    //$lwebmail->turnOffVirusCheck();
    //$lwebmail->changeAllUserPolicy();
    if($plugin['imail_gamma']){
    	$IMap->turnOffVirusCheck();
    }else{
    	$lwebmail->turnOffVirusCheck();
    	$lwebmail->changeAllUserPolicy();
    }
}
else
{
    # Default activated
    //$lwebmail->turnOnVirusCheck();
    //$lwebmail->changeAllUserPolicy();
    if($plugin['imail_gamma']){
    	$IMap->turnOnVirusCheck();
    }else{
    	$lwebmail->turnOnVirusCheck();
    	$lwebmail->changeAllUserPolicy();
    }
}


header("Location: bypass.php?msg=2");
?>
