<?php
/*
 * 2014-09-15 (Carlos): iMail plus use its own osapi call
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libwebmail.php");
include_once("../../../includes/imap_gamma.php");

if($plugin['imail_gamma']){
	$lwebmail = new imap_gamma(1);
}else{
	$lwebmail = new libwebmail();
}

if ($_POST['target']!='')
{
    $success = $lwebmail->addWhiteList($_POST['target']);
}
if ($success)
{
    $msg = "msg=1";
}
else
{
    $msg = "msg=12";
}

header("Location: whitelist.php?$msg");
?>
