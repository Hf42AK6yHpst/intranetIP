<?php
// editing by 
/**************************************** Changes log *********************************************
 * 2014-11-11 (Carlos): Generate a random password for users that do not have encrypted password logged.
 * 2014-07-03 (Carlos): Use new approach to retrieve password if using hash password mechanism
 * 2014-05-07 (Carlos): Added suspend/unsuspend mail account api call
 * 2013-12-16 (Carlos): $sys_custom['iMailPlus']['EmailAliasName'] - if INTRANET_USER.ImapUserLogin is set, use it as email account name
 * 2011-09-30 (Carlos): Cater schools that only use hashed password problem
 */
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libwebmail.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccountmgmt.php");
include_once("../../includes/libpwm.php");
intranet_opendb();

$li = new libdb();
if($plugin['imail_gamma']!==true) $lwebmail = new libwebmail();

$sql = "SELECT u.RecordType, u.UserLogin, u.UserPassword, s.EncPassword ".($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName']?",u.ImapUserLogin":"")."
		FROM INTRANET_USER as u 
		LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as s ON s.UserID = u.UserID 
		WHERE u.UserID = '$uid'";
$temp = $li->returnArray($sql,3);
list ($target_utype, $loginName, $password, $encPassword) = $temp[0];
if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName']){
	$imap_user_login = $temp[0]['ImapUserLogin'];
}
$loginName = strtolower($loginName);
$password = trim($password);
# cater some schools that only have hashed password available, get from reversable password
/*
$encPassword = trim($encPassword);
if($password == "" && $encPassword != ""){
	$password = GetDecryptedPassword($encPassword);
}
*/
if($intranet_authentication_method == 'HASH'){
	$libpwm = new libpwm();
	$rs = $libpwm->getData(array($uid));
	$password = $rs[$uid];
}

if (!in_array($target_utype,$webmail_identity_allowed))
{
     header("Location: user.php?error=2");
     intranet_closedb();
     exit();
}

if($plugin['imail_gamma']===true){
	if($sys_custom['iMailPlus']['EmailAliasName'] && $imap_user_login != ''){
		$EmailLoginName = trim($imap_user_login)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	}else{
		$EmailLoginName = trim($loginName)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	}
}else{
	$EmailLoginName = $loginName."@".$lwebmail->mailaddr_domain;
}

if ($newAccount == 1)        # Create new account
{
    if ($password == "")
    {
        //header("Location: user.php?error=1");
        //exit();
        // generate a random password if password is not available
		$password = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'),0,10);
    }
    if($plugin['imail_gamma']===true){
    	include_once("../../includes/imap_gamma.php");
		$laccount = new libaccountmgmt();
		$IMap = new imap_gamma(1);
		$ban_ext_mail_list = trim($IMap->ban_ext_mail_list);
		$ban_ext_mail_list_arr = explode("\r\n",$ban_ext_mail_list);
		if($sys_custom['iMailPlus']['EmailAliasName'] && $imap_user_login != ''){
			$IMapLoginName = trim($imap_user_login)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		}else{
			$IMapLoginName = trim($loginName)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		}
		$account_exists = $IMap->is_user_exist ($IMapLoginName);
		if(!$account_exists){
			if($IMap->open_account($IMapLoginName, $password)){
				if ($status == 1){
					$Debug['setIMapUserEmail_'.$IMapLoginName] = $laccount->setIMapUserEmail($uid,$IMapLoginName);
				}else{
					$laccount->setIMapUserEmail($uid,"");
				}
				$Debug['SetTotalQuota_'.$IMapLoginName.'_'.$quota] = $IMap->SetTotalQuota($IMapLoginName, $quota, $uid);
			}
		}else{
			//if($quota != $oldQuota){
				$IMap->SetTotalQuota($IMapLoginName, $quota, $uid);
			//}
			if ($status == 1){
				$laccount->setIMapUserEmail($uid,$IMapLoginName);
			}else{
				$laccount->setIMapUserEmail($uid,"");
			}
			if($linked == 1){
				if(!in_array($IMapLoginName,$ban_ext_mail_list_arr))
					$IMap->removeGroupBlockExternal((array)$IMapLoginName);
			}else{
				$IMap->addGroupBlockExternal((array)$IMapLoginName);
			}
		}
		if($IMap->IMapCache !== false) $IMap->IMapCache->Close_Connect();
    }else{
    	$lwebmail->open_account($loginName, $password);
    }
}else{
	if($plugin['imail_gamma']===true){
		if($newAccount != 1){
			include_once("../../includes/imap_gamma.php");
			$laccount = new libaccountmgmt();
			$IMap = new imap_gamma(1);
			$ban_ext_mail_list = trim($IMap->ban_ext_mail_list);
			$ban_ext_mail_list_arr = explode("\r\n",$ban_ext_mail_list);
			if($sys_custom['iMailPlus']['EmailAliasName'] && $imap_user_login != ''){
				$IMapLoginName = trim($imap_user_login)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
			}else{
				$IMapLoginName = trim($loginName)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
			}
			//if($quota != $oldQuota){
				$IMap->SetTotalQuota($IMapLoginName, $quota, $uid);
			//}
			if ($status == 1){
				$laccount->setIMapUserEmail($uid,$IMapLoginName);
			}else{
				$laccount->setIMapUserEmail($uid,"");
			}
			if($linked == 1){
				if(!in_array($IMapLoginName,$ban_ext_mail_list_arr))
					$IMap->removeGroupBlockExternal((array)$IMapLoginName);
			}else{
				$IMap->addGroupBlockExternal((array)$IMapLoginName);
			}
			if($IMap->IMapCache !== false) $IMap->IMapCache->Close_Connect();
		}
	}
}

if($plugin['imail_gamma']!==true){
	if ($newAccount == 1 || ($quota != "" /* && $quota != $oldQuota */) ){ # Same not change
    	$set_quota_success = $lwebmail->setTotalQuota($loginName,$quota,"iMail");
	}
}

//if($plugin['imail_gamma']!==true){
	$sql = "SELECT ACL FROM INTRANET_SYSTEM_ACCESS WHERE UserID = '$uid'";
	$temp = $li->returnVector($sql);
	$acl = $temp[0];
	if ($linked == 1)
	{
	    if ($acl == "")
	    {
	        $sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ('$uid', 1)";
	        $li->db_db_query($sql);
	    }
	    else
	    {
	        if ($acl == 1 || $acl == 3)
	        {
	            # Nothing to do
	
	        }
	        else
	        {
	            $sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL + 1 WHERE UserID = '$uid' AND (ACL NOT IN (1,3))";
	            $li->db_db_query($sql);
	        }
	    }
	    if(!$plugin['imail_gamma']){ // resume webmail
	    	$lwebmail->setUnsuspendUser($EmailLoginName,"iMail",$password);
	    }
	}
	else    # Unlink it
	{
	    if ($acl == "")
	    {
	        $sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ('$uid', 0)";
	        $li->db_db_query($sql);
	    }
	    else
	    {
	        if ($acl == 1 || $acl == 3)
	        {
	            $sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = '$uid' AND (ACL IN (1,3))";
	            $li->db_db_query($sql);
	        }
	        else
	        {
	            # Nothing to do
	        }
	    }
	    if(!$plugin['imail_gamma']){ // suspend webmail
	    	$lwebmail->setSuspendUser($EmailLoginName,"iMail");
	    }
	}
//}

if($plugin['imail_gamma']===true){
	if($status == "1"){ // resume iMail plus 
		$IMap->setUnsuspendUser($EmailLoginName,"iMail",$password);
	}else{ // suspend iMail plus
		$IMap->setSuspendUser($EmailLoginName,"iMail");
	}
}

if ($type==1)       # From Type List
{
    $url = "list_type.php?UserType=$target&ClassName=$ClassName&msg=2";
}
else if ($type==2)    # From Group List
{
    $url = "list_group.php?GroupID=$target&msg=2";
}
else     # From Login name input
{
    $url = "user.php?msg=2";
}

intranet_closedb();
header("Location: $url");
?>