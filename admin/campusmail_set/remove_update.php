<?php
//using : 
/******************************* Changes *******************************************
 * 2018-11-13 (Carlos): Added $sys_custom['iMailPlus']['ForceRemovalExcludeUserID'] to exclude certain users avoid being cleaned emails.
 * 2017-06-14 (Carlos): Apply user status filter to search different status users. 
 * 2017-03-07 (Carlos): Update new mail counter.
 * 2012-09-17 (Carlos): added libwebmail->RemoveRawBodyLogFile() to remove internet mail raw message source log files
 * 2012-08-29 (Carlos): rename UserType (conflict with session variable) to TargetUserType
 * 2012-08-28 (Carlos): split CampusMailID list into 5000 per chunk to avoid memory burst
 * 2012-07-17 (Carlos): Select mails by user type and date range to delete 
 * 2011-06-22 (Carlos): Add checking on attachment to prevent mis-deletion
 ***********************************************************************************/
ini_set("memory_limit","512M");
set_time_limit(86400);
$PATH_WRT_ROOT = "../../"; 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once("../../includes/libwebmail.php");

$target_user_status_param = "";
if(isset($_POST['TargetUserStatus']) && is_array($_POST['TargetUserStatus'])){
	foreach($_POST['TargetUserStatus'] as $key => $val){
		$target_user_status_param .= "&TargetUserStatus[]=".$val;
	}
}
if(!isset($_POST['TargetUserType']) || !isset($_POST['start']) || !isset($_POST['end']) || !isset($_POST['TargetUserStatus'])){
	header("Location: remove.php?TargetUserType=".$TargetUserType.$target_user_status_param."&start=".$start."&end=".$end."&msg=0");
	exit;
}

intranet_opendb();

$li = new libcampusmail();
$lf = new libfilesystem();
$lwebmail = new libwebmail();
$ts_start = strtotime($start);
$ts_end = strtotime($end) + 60*60*24 - 1;

$more_conds = "";
if(isset($sys_custom['iMailPlus']['ForceRemovalExcludeUserID']) && is_array($sys_custom['iMailPlus']['ForceRemovalExcludeUserID']) && count($sys_custom['iMailPlus']['ForceRemovalExcludeUserID'])>0){
	$more_conds .= " AND u.UserID NOT IN (".implode(",",$sys_custom['iMailPlus']['ForceRemovalExcludeUserID']).") ";
}

$sql = "";
if(in_array('1',$TargetUserStatus) || in_array('0',$TargetUserStatus) || in_array('3',$TargetUserStatus))
{
	# select mails in specified date range and by specific user type
	$sql = "SELECT c.CampusMailID as CampusMailID,c.UserID FROM INTRANET_USER as u 
			INNER JOIN INTRANET_CAMPUSMAIL as c ON u.UserID=c.UserID 
			WHERE u.RecordType='".$TargetUserType."' AND u.RecordStatus IN ('".implode("','",$TargetUserStatus)."') AND UNIX_TIMESTAMP(c.DateInput) >= '$ts_start' AND
	        UNIX_TIMESTAMP(c.DateInput) <= '$ts_end' $more_conds ";
}
if(in_array('-1',$TargetUserStatus)){
	if($sql != '') $sql .= " UNION ";
	$sql .= "SELECT c.CampusMailID as CampusMailID,c.UserID FROM INTRANET_ARCHIVE_USER as u 
			INNER JOIN INTRANET_CAMPUSMAIL as c ON u.UserID=c.UserID 
			WHERE u.RecordType='".$TargetUserType."' AND UNIX_TIMESTAMP(c.DateInput) >= '$ts_start' AND
	        UNIX_TIMESTAMP(c.DateInput) <= '$ts_end' $more_conds ";
}
$sql .= " ORDER BY CampusMailID ";
$tmp_result = $li->returnResultSet($sql);

$resultAll = Get_Array_By_Key($tmp_result,'CampusMailID');
$involvedUserIdAry = array_values(array_unique(Get_Array_By_Key($tmp_result,'UserID')));

if (sizeof($resultAll)!=0)
{
	# delete action log  
	if (!is_dir("$intranet_root/file/mailDeleteLog")) {
		mkdir("$intranet_root/file/mailDeleteLog",0777);
	}
	if (!is_dir("$intranet_root/file/mailDeleteLog/adminconsole")) {
		mkdir("$intranet_root/file/mailDeleteLog/adminconsole",0777);
	}
	$maillogPath = $intranet_root.'/file/mailDeleteLog/adminconsole';
	
	$logFilename = 'force_removal_'.date('Y-m-d').'.log';
	
	$logFileHandle = (file_exists($maillogPath.'/'.$logFilename)) ? fopen($maillogPath.'/'.$logFilename,'a') : fopen($maillogPath.'/'.$logFilename,'w');
	if($logFileHandle) {
		$log_content = "Time: ".date("Y-m-d H:i:s")."\n";
		$log_content .= "Client IP: ".$_SERVER['REMOTE_ADDR']."\n";
		$log_content .= "Date range: ".$start." - ".$end."\n";
		$log_content .= "Query: ".$sql."\n";
		$log_content .= "Found CampusMailID: ".implode(",",$resultAll)."\n";
		$log_content .= "Involved UserID: ".implode(",",$involvedUserIdAry)."\n";
		$log_content .= "\n";
		fwrite($logFileHandle, $log_content);
		fclose($logFileHandle);
	}
	# end of log
	
	$resultChunk = array_chunk($resultAll,5000);
	$numOfChunks = count($resultChunk);
	for($k=0;$k<$numOfChunks;$k++)
	{
		$result = $resultChunk[$k];
	    $list = implode(",",$result);
		
		$sql = "SELECT UserID,IF(Deleted=1,-1,UserFolderID) as UserFolderID, COUNT(*) as NewMailCount,Deleted FROM INTRANET_CAMPUSMAIL WHERE CampusMailID IN ($list) AND (RecordStatus IS NULL OR RecordStatus='') GROUP BY UserID,UserFolderID,Deleted";
		$count_records = $li->returnResultSet($sql);
		$count_records_size = count($count_records);
		for($i=0;$i<$count_records_size;$i++)
		{
			$li->UpdateCampusMailNewMailCount($count_records[$i]['UserID'], $count_records[$i]['UserFolderID'], -$count_records[$i]['NewMailCount']);
		}
		
		$sql = "SELECT HeaderFilePath,MessageFilePath FROM INTRANET_RAWCAMPUSMAIL WHERE CampusMailID IN (".$list.")";
		$raw_message_path = $li->returnArray($sql);
		
		# Select internet mails CampusMailID for removing raw meesage log file
		$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE MailType = 2 AND CampusMailID IN (".$list.")"; 
		$to_remove_rawlog_campusmailid = $li->returnVector($sql);
		for($i=0;$i<count($to_remove_rawlog_campusmailid);$i++) {
			$lwebmail->RemoveRawBodyLogFile($to_remove_rawlog_campusmailid[$i]);
		}
		
	    # Attachment Removal
	    $sql = "SELECT DISTINCT Attachment FROM INTRANET_CAMPUSMAIL WHERE CampusMailID IN ($list) AND IsAttachment = 1 AND Attachment IS NOT NULL AND Attachment <> ''";
	    $dirs = $li->returnVector($sql);
	    
	    ## delete internal reply message
	    $sql = "DELETE FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID IN ($list)";
	    #echo "1. $sql\n";
	    $li->db_db_query($sql);
		
		## delete the mail
	    //$sql = "DELETE FROM INTRANET_CAMPUSMAIL WHERE UNIX_TIMESTAMP(DateInput) >= '$ts_start' AND
	    //        UNIX_TIMESTAMP(DateInput) <= '$ts_end'";
	    $sql = "DELETE FROM INTRANET_CAMPUSMAIL WHERE CampusMailID IN (".$list.")";
	            #echo "2. $sql\n";
	    $li->db_db_query($sql);
	    
	    $delim = "";
	    $list_path_string_to_remove = "";
	    for ($i=0; $i<sizeof($dirs); $i++)
	    {
	         if ($dirs[$i] != "")
	         {
	         	 $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE Attachment = '".$dirs[$i]."'";
	         	 $temp = $li->returnVector($sql);
	             if (is_array($temp) && sizeof($temp)==1 && $temp[0]==0 && preg_match('/^u\d+\/.+$/',$dirs[$i])) // make sure the attachment path is subfolder of user folder, not the user folder or any parent folder, which would result in delete all attachments
	             {
	             	$list_path_string_to_remove .= $delim."'".$dirs[$i]."'";
	             	$path = "$file_path/file/mail/".$dirs[$i];
	             	$lf->lfs_remove($path);
	             	$delim = ",";
	             }
	         }
	    }
	    
	    ## delete records in attachment map table
	    if ($list_path_string_to_remove != "")
	    {
	    	$sql = "DELETE FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE AttachmentPath IN ($list_path_string_to_remove) ";
	    	$li->db_db_query($sql);
	    }
	    
	    ## delete internet mail source files
	    for($i=0;$i<count($raw_message_path);$i++)
	    {
	    	$header_path = trim($raw_message_path[$i]['HeaderFilePath']);
	    	$body_path = trim($raw_message_path[$i]['MessageFilePath']);
	    	if($header_path != ''){
	    		$HeaderFullPath = "$file_path/file/mail/".$header_path;
	    		$lf->lfs_remove($HeaderFullPath);
	    	}
	    	if($body_path != ''){
				$MessageFullPath = "$file_path/file/mail/".$body_path;
				$lf->lfs_remove($MessageFullPath);
	    	}
	    }
	    
	    ## Caution; maybe very slow
	    ## Recalculate used quota 
	    $sql = "SELECT u.UserID,IFNULL(SUM(c.AttachmentSize),0) as QuotaUsed 
				FROM INTRANET_USER as u 
				INNER JOIN INTRANET_CAMPUSMAIL as c ON c.UserID=u.UserID 
				WHERE u.UserID IN (".implode(",",$involvedUserIdAry).") AND u.RecordType='".$TargetUserType."' AND c.IsAttachment = 1 AND c.Attachment <> '' 
				GROUP BY u.UserID";
		$UserIDQuotaUsed = $li->returnArray($sql);
		for($i=0;$i<count($UserIDQuotaUsed);$i++){
			$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed='".$UserIDQuotaUsed[$i]['QuotaUsed']."' WHERE UserID='".$UserIDQuotaUsed[$i]['UserID']."'";
			$li->db_db_query($sql);
		}
	}
}

intranet_closedb();
header("Location: remove.php?TargetUserType=".$TargetUserType.$target_user_status_param."&start=".$start."&end=".$end."&msg=1");
?>