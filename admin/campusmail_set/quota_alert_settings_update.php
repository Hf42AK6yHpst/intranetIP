<?php
// editing by 
/*
 * 2013-12-04 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

## Only For iMail Gamma/Plus
if(!$plugin['imail_gamma']){
	header("Location: index.php");
	intranet_closedb();
	exit();
}

$IMap = new imap_gamma(true);

$success = $IMap->setQuotaAlertSetting($QuotaAlert);

$msg = $success?$Lang['General']['ReturnMessage']['UpdateSuccess']:$Lang['General']['ReturnMessage']['UpdateUnsuccess'];

intranet_closedb();
header("Location: quota_alert_settings.php?msg=".urlencode($msg));
?>