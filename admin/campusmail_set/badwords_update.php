<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");

$data = stripslashes($data);
$data = intranet_htmlspecialchars($data);

if($plugin['imail_gamma'] === true)
{
	include_once("../../includes/imap_gamma.php");
	$IMap = new imap_gamma();
	$IMap->setBadWordList($data);
}
else
{
	$lf = new libfilesystem();
	$base_dir = "$intranet_root/file/templates/";
	if (!is_dir($base_dir))
	{
	     $lf->folder_new($base_dir);
	}
	
	$target_file = "$base_dir"."mail_badwords.txt";
	$data = write_file_content($data,$target_file);
}

header("Location: badwords.php?msg=2");
?>