<?php
// editing by 
/********************** Changes ***************************
 * 2020-07-29 (Henry): general removal search for iMail
 * 2011-08-26 (Carlos): Hide date range search condition as DateInput is not key index may introduce slow performance
 * 2011-06-20 (Carlos): Added date range search fields
 **********************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();

// $i_Campusquota_basic_identity
//$filecontent = trim(get_file_content("$intranet_root/file/cm_removal_search.txt"));
//$cond_search_allowed = ($filecontent == 1);
//if (!$cond_search_allowed)
//{
//     header("Location: index.php");
//     exit();
//}
if ($special_feature['imail'])
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}
# Make Temp table
$li = new libdb();
$sql = "CREATE TABLE IF NOT EXISTS TEMP_CAMPUSMAIL_COND_REMOVE (
 CampusMailID int(11) NOT NULL,
 PRIMARY KEY (CampusMailID)
)";
$li->db_db_query($sql);

$sql = "DELETE FROM TEMP_CAMPUSMAIL_COND_REMOVE";
$li->db_db_query($sql);

include_once("../../templates/adminheader_setting.php");
?>
<form name="form1" action="cond_search_result.php" method="get">
<?= displayNavTitle($i_admintitle_fs, '', $mail_set_title, 'index.php',$i_CampusMail_New_RemoveBySearch,'') ?>
<?= displayTag("$img_tag", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0 align="center">
<tr><td align=right><?=$Lang['iMail']['FieldTitle']['SubjectKeywordKeyPhrase']?>:</td><td><input type=text name=keyword size=40></td></tr>
<!--
<tr>
	<td align=right><?=$Lang['SysMgr']['Periods']['FieldTitle']['PeriodRange']?>:</td>
	<td><?=$Lang['General']['From']."&nbsp;<input type='text' id='StartDate' name='StartDate' value='' size='10' maxlength='10' />&nbsp;".$Lang['General']['To']."&nbsp;<input type='text' id='EndDate' name='EndDate' value='' size='10' maxlength='10' />"?><span style='color: gray;'> (YYYY-MM-DD)</span></td>
</tr>
-->
</table>
</BLOCKQUOTE>
<table width=560 border=0 cellpadding=0 cellspacing=0>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_find_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
<input type=hidden name=newsearch value=1>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>