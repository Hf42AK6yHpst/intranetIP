<?php
// editing by 
/******************************* Changes *******************************************
 * 2017-03-07 (Carlos): Update new mail counter.
 * 2017-03-06 (Carlos): Added Attachment path checking to make sure only subfolders would be deleted. 
 * 						Added log to log down deleted campusmailid and attachment. 
 * 2011-06-22 (Carlos): Add checking on attachment to prevent mis-deletion
 ***********************************************************************************/
 ini_set("memory_limit","512M");
set_time_limit(86400);
$PATH_WRT_ROOT = "../../"; 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
intranet_opendb();

$li = new libcampusmail();
$lwebmail = new libwebmail();

if (sizeof($CampusMailID)!=0)
{
    $list = implode(",",$CampusMailID);

	$sql = "SELECT UserID,IF(Deleted=1,-1,UserFolderID) as UserFolderID, COUNT(*) as NewMailCount,Deleted FROM INTRANET_CAMPUSMAIL WHERE CampusMailID IN ($list) AND (RecordStatus IS NULL OR RecordStatus='') GROUP BY UserID,UserFolderID,Deleted";
	$count_records = $li->returnResultSet($sql);
	$count_records_size = count($count_records);
	for($i=0;$i<$count_records_size;$i++)
	{
		$li->UpdateCampusMailNewMailCount($count_records[$i]['UserID'], $count_records[$i]['UserFolderID'], -$count_records[$i]['NewMailCount']);
	}

	$sql = "SELECT UserID FROM INTRANET_CAMPUSMAIL WHERE CampusMailID IN (".$list.")";
	$involvedUserIdAry = array_values(array_unique($li->returnVector($sql)));

	$sql = "SELECT HeaderFilePath,MessageFilePath FROM INTRANET_RAWCAMPUSMAIL WHERE CampusMailID IN (".$list.")";
	$raw_message_path = $li->returnArray($sql);
	
	# Select internet mails CampusMailID for removing raw meesage log file
	$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE MailType = 2 AND CampusMailID IN (".$list.")"; 
	$to_remove_rawlog_campusmailid = $li->returnVector($sql);
	for($i=0;$i<count($to_remove_rawlog_campusmailid);$i++) {
		$lwebmail->RemoveRawBodyLogFile($to_remove_rawlog_campusmailid[$i]);
	}

    # Attachment Removal
    $sql = "SELECT DISTINCT Attachment FROM INTRANET_CAMPUSMAIL WHERE CampusMailID IN ($list) AND IsAttachment = 1 AND Attachment IS NOT NULL AND Attachment <> '' ";
    $dirs = $li->returnVector($sql);

    $lf = new libfilesystem();
    
    # delete action log  
	if (!is_dir("$intranet_root/file/mailDeleteLog")) {
		mkdir("$intranet_root/file/mailDeleteLog",0777);
	}
	if (!is_dir("$intranet_root/file/mailDeleteLog/adminconsole")) {
		mkdir("$intranet_root/file/mailDeleteLog/adminconsole",0777);
	}
	$maillogPath = $intranet_root.'/file/mailDeleteLog/adminconsole';
	
	$logFilename = 'cond_search_remove_'.date('Y-m-d').'.log';
	
	$logFileHandle = (file_exists($maillogPath.'/'.$logFilename)) ? fopen($maillogPath.'/'.$logFilename,'a') : fopen($maillogPath.'/'.$logFilename,'w');
	if($logFileHandle) {
		$log_content = "Time: ".date("Y-m-d H:i:s")."\n";
		$log_content .= "Client IP: ".$_SERVER['REMOTE_ADDR']."\n";
		$log_content .= "CampusMailID: ".$list."\n";
		$log_content .= "Involved UserID: ".implode(",",$involvedUserIdAry)."\n";
		$log_content .= "Attachment: ".implode(',',(array)$dirs)."\n";
		$log_content .= "\n";
		fwrite($logFileHandle, $log_content);
		fclose($logFileHandle);
	}
	# end of log
    
    $sql = "DELETE FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID IN ($list)";
    $li->db_db_query($sql);

    $sql = "DELETE FROM INTRANET_CAMPUSMAIL WHERE CampusMailID IN ($list)";
    $result = $li->db_db_query($sql);
    
    $delim = "";
    $list_path_string_to_remove = "";
    for ($i=0; $i<sizeof($dirs); $i++)
    {
         if ($dirs[$i] != "")
         {
         	 $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE Attachment = '".$dirs[$i]."'";
         	 $temp = $li->returnVector($sql);
             if (is_array($temp) && sizeof($temp)==1 && $temp[0]==0 && preg_match('/^u\d+\/.+$/',$dirs[$i])) // make sure the attachment path is subfolder of user folder, not the user folder or any parent folder, which would result in delete all attachments
             {
             	$list_path_string_to_remove .= $delim."'".$dirs[$i]."'";
             	$path = "$file_path/file/mail/".$dirs[$i];
             	$lf->lfs_remove($path);
             	$delim = ",";
             }
         }
    }
    
     # Remove Database Records
    if ($list_path_string_to_remove != "")
    {
        $sql = "DELETE FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE AttachmentPath IN ($list_path_string_to_remove)";
        $li->db_db_query($sql);
    }
    
    ## delete internet mail source files
    for($i=0;$i<count($raw_message_path);$i++)
    {
    	$header_path = trim($raw_message_path[$i]['HeaderFilePath']);
    	$body_path = trim($raw_message_path[$i]['MessageFilePath']);
    	if($header_path != ''){
    		$HeaderFullPath = "$file_path/file/mail/".$header_path;
    		$lf->lfs_remove($HeaderFullPath);
    	}
    	if($body_path != ''){
			$MessageFullPath = "$file_path/file/mail/".$body_path;
			$lf->lfs_remove($MessageFullPath);
    	}
    }
    
    ## Caution; maybe very slow
    ## Recalculate used quota 
    $sql = "SELECT c.UserID,IFNULL(SUM(c.AttachmentSize),0) as QuotaUsed 
			FROM INTRANET_CAMPUSMAIL as c 
			WHERE c.UserID IN (".implode(",",$involvedUserIdAry).") AND c.IsAttachment = 1 AND c.Attachment <> '' 
			GROUP BY c.UserID";
	$UserIDQuotaUsed = $li->returnArray($sql);
	for($i=0;$i<count($UserIDQuotaUsed);$i++){
		$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed='".$UserIDQuotaUsed[$i]['QuotaUsed']."' WHERE UserID='".$UserIDQuotaUsed[$i]['UserID']."'";
		$li->db_db_query($sql);
	}
}

intranet_closedb();
header("Location: cond_search_result.php?msg=3&newsearch=0&keyword=".str_replace('"', "&quot;", stripslashes($keyword)));
?>