<?php
// editing by : Ronald
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

# $li_menu = new libaccount()
if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;


## Only For iMail v1.2
$has_imail = $special_feature['imail'];
if(!$plugin['imail_gamma']){
	include_once("../../includes/libwebmail.php");
	$lwebmail = new libwebmail(); 
	$mail_server_quota_control = $special_feature['imail'] && $lwebmail->has_webmail && ($lwebmail->type==3);
}

?>
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$Lang['Gamma']['UserUsageRightsAndQuota'],'')?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300>
<blockquote>
<?
/*
	if($plugin['imail_gamma']===true){
		
	}else{
		if($mail_server_quota_control)
			echo $i_LinuxAccount_iMail_Description."<br><br><br>";	
	}
*/
?>
<?
if($plugin['imail_gamma']===true){
	echo displayOption($Lang['Gamma']['MailBoxQuota'],'group.php',1);
	echo displayOption($i_LinuxAccount_DisplayQuota, 'list.php', 1);
}else{
	
 	echo displayOption($i_CampusMail_New_QuotaSettings, 'realquota.php', $has_imail); 	
 	
	if($mail_server_quota_control)
		echo "<BR>".$i_LinuxAccount_iMail_Description."<br>";	
	
 	echo displayOption($i_LinuxAccount_SetDefaultQuota, 'default.php', $mail_server_quota_control,
                  		$i_LinuxAccount_SetUserQuota, 'user.php', $mail_server_quota_control,
                  		$i_LinuxAccount_SetGroupQuota, 'group.php',$mail_server_quota_control,
                  		$i_LinuxAccount_DisplayQuota, 'list.php', $mail_server_quota_control
                  		);
}
?>
</blockquote>
</td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>