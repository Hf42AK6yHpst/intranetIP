<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

$policy = get_file_content("$intranet_root/file/campusmail_policy.txt");
if ($policy == 1)
{
    $policy_auto = " CHECKED";
    $policy_old = "";
}
else
{
    $policy_old = " CHECKED";
    $policy_auto = "";
}
// $i_Campusquota_basic_identity
if ($special_feature['imail'])
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}
?>

<form name="form1" action="policy_update.php" method="post" >
<?= displayNavTitle($i_admintitle_fs, '', $mail_set_title, 'index.php',$i_campusmail_policy,'') ?>
<?= displayTag("$img_tag", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>

<table border=0 cellpadding=5 cellspacing=0>
<tr><td><input type=radio value=0 name=policy<?=$policy_old?>> <?=$i_campusmail_policy_original?></td></tr>
<tr><td><input type=radio value=1 name=policy<?=$policy_auto?>> <?=$i_campusmail_policy_auto?></td></tr>
</table>
</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>