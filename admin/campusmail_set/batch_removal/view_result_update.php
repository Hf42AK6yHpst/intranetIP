<?php
// editing by 
/*
 * 2014-11-05 (Carlos): Do not use session to login email account. Pass email and password to imap constructor. 
 * 2014-09-25 (Carlos): $sys_custom['iMailPlusBatchRemovalRealtimeAction'] - perform actions in realtime
 * 2014-07-03 (Carlos): Use new approach to retrieve password if using hash password mechanism
 */
set_time_limit(60*60);
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libpwm.php");
intranet_opendb();

## Only For iMail Gamma/Plus
if($plugin['imail_gamma']!==true){
	header("Location: ../index.php");
	exit();
}

$sys_custom['iMailPlusBatchRemovalRealtimeAction'] = true;

//$IMap = new imap_gamma($skipLogin = 1);
$li = new libdb();

/* +--------------------------------------------------------------------+
 * | TABLE MAIL_REMOVAL_LOG                                             |
 * +--------------+------------+----------------------------------------+
 * | RecordStatus | RecordType | Meaning                                |
 * +--------------+------------+----------------------------------------+
 * |      0       |     ANY    | MAIL DELETED & LOG SOFT DELETED        |
 * |      1       |     !1     | MAIL TO BE DELETED BUT NOT DELETED YET |
 * |      1       |      1     | MAIL WAS ACTUALLY DELETED              |
 * |      2       |     ANY    | MAIL HIDDEN FOR UNDO OR DELETE         |
 * |      3       |     !1     | MAIL TO BE UNDO/RESTORE                |
 * +--------------+------------+----------------------------------------+
 */

if(sizeof($RecordID)>0)
{
	$RecordIdAry = array();
	for($i=0;$i<count($RecordID);$i++){
		$ary = explode(",",rtrim($RecordID[$i],','));
		$RecordIdAry = array_merge($RecordIdAry,$ary);
	}
	
	if($record_status==1) // From confirmed deleted view list
	{
		switch($action)
		{
			case "delete":
				$sql = "UPDATE MAIL_REMOVAL_LOG SET RecordStatus = 0, DateModified = NOW() WHERE RecordID IN ('".implode("','",$RecordIdAry)."')";
				$result = $li->db_db_query($sql);
				$msg = $result?$Lang['General']['ReturnMessage']['DeleteSuccess']:$Lang['General']['ReturnMessage']['DeleteUnsuccess'];
			break;
		}
	}else // From hidden view list 
	{
		switch($action)
		{
			case "delete":
				$sql = "UPDATE MAIL_REMOVAL_LOG SET RecordStatus = 1, DateModified = NOW() WHERE RecordID IN ('".implode("','",$RecordIdAry)."')";
				$result = $li->db_db_query($sql);
				$msg = $result?$Lang['General']['ReturnMessage']['DeleteSuccess']:$Lang['General']['ReturnMessage']['DeleteUnsuccess'];
				
				if($sys_custom['iMailPlusBatchRemovalRealtimeAction']){
					$sql = "SELECT DISTINCT u.UserID,u.ImapUserEmail,u.UserPassword 
							FROM INTRANET_USER as u 
							INNER JOIN MAIL_REMOVAL_LOG as r ON r.UserEmail=u.ImapUserEmail 
							WHERE r.RecordID IN ('".implode("','",$RecordIdAry)."')";
					$user_list = $li->returnResultSet($sql);
					$user_count = count($user_list);
					if($intranet_authentication_method == 'HASH' || $intranet_authentication_method == 'LDAP'){
						$libpwm = new libpwm();
						$uid_ary = Get_Array_By_Key($user_list,'UserID');
						$uidToPw = $libpwm->getData($uid_ary);
					}
					
					for($i=0;$i<$user_count;$i++){
						$user_id = $user_list[$i]['UserID'];
						$imap_user_email = $user_list[$i]['ImapUserEmail'];
						$user_password = $user_list[$i]['UserPassword'];
						if($intranet_authentication_method == 'HASH' || $intranet_authentication_method == 'LDAP'){
							$user_password = $uidToPw[$user_id];
						}
						if($imap_user_email =='' || $user_password == ''){
							continue;
						}
						//$_SESSION['SSV_EMAIL_LOGIN'] = $imap_user_email;
	    				//$_SESSION['SSV_EMAIL_PASSWORD'] = $user_password;
	    				//$_SESSION['SSV_LOGIN_EMAIL'] = $imap_user_email;
						$IMap = new imap_gamma(false,$imap_user_email,$user_password);
						if($IMap->ConnectionStatus != true){
							//unset($_SESSION['SSV_EMAIL_LOGIN']);
							//unset($_SESSION['SSV_EMAIL_PASSWORD']);
							//unset($_SESSION['SSV_LOGIN_EMAIL']);
							continue;
						}
						$IMap->Batch_Removal_Expunge_Mails($RecordIdAry);
						$IMap->close();
						//unset($_SESSION['SSV_EMAIL_LOGIN']);
						//unset($_SESSION['SSV_EMAIL_PASSWORD']);
						//unset($_SESSION['SSV_LOGIN_EMAIL']);
					}
				}
			break;
			
			case "undo":
				$sql = "UPDATE MAIL_REMOVAL_LOG SET RecordStatus = '3', DateModified = NOW() WHERE RecordID IN ('".implode("','",$RecordIdAry)."')";
				$result = $li->db_db_query($sql);
				$msg = $result?$Lang['General']['ReturnMessage']['UpdateSuccess']:$Lang['General']['ReturnMessage']['UpdateUnsuccess'];
				
				if($sys_custom['iMailPlusBatchRemovalRealtimeAction']){
					$sql = "SELECT DISTINCT u.UserID,u.ImapUserEmail,u.UserPassword 
							FROM INTRANET_USER as u 
							INNER JOIN MAIL_REMOVAL_LOG as r ON r.UserEmail=u.ImapUserEmail 
							WHERE r.RecordID IN ('".implode("','",$RecordIdAry)."')";
					$user_list = $li->returnResultSet($sql);
					$user_count = count($user_list);
					if($intranet_authentication_method == 'HASH' || $intranet_authentication_method == 'LDAP'){
						$libpwm = new libpwm();
						$uid_ary = Get_Array_By_Key($user_list,'UserID');
						$uidToPw = $libpwm->getData($uid_ary);
					}
					
					for($i=0;$i<$user_count;$i++){
						$user_id = $user_list[$i]['UserID'];
						$imap_user_email = $user_list[$i]['ImapUserEmail'];
						$user_password = $user_list[$i]['UserPassword'];
						if($intranet_authentication_method == 'HASH' || $intranet_authentication_method == 'LDAP'){
							$user_password = $uidToPw[$user_id];
						}
						if($imap_user_email =='' || $user_password == ''){
							continue;
						}
						//$_SESSION['SSV_EMAIL_LOGIN'] = $imap_user_email;
	    				//$_SESSION['SSV_EMAIL_PASSWORD'] = $user_password;
	    				//$_SESSION['SSV_LOGIN_EMAIL'] = $imap_user_email;
						$IMap = new imap_gamma(false,$imap_user_email,$user_password);
						if($IMap->ConnectionStatus != true){
							//unset($_SESSION['SSV_EMAIL_LOGIN']);
							//unset($_SESSION['SSV_EMAIL_PASSWORD']);
							//unset($_SESSION['SSV_LOGIN_EMAIL']);
							continue;
						}
						$IMap->Batch_Removal_Restore_Mails($RecordIdAry);
						$IMap->close();
						//unset($_SESSION['SSV_EMAIL_LOGIN']);
						//unset($_SESSION['SSV_EMAIL_PASSWORD']);
						//unset($_SESSION['SSV_LOGIN_EMAIL']);
					}
				}
			break;
			
			###### For emergency recover, e.g. confirm delete wrong mails, not to display in UI ######
			case "recover":
				$sql = "SELECT 
							u.UserID, u.UserLogin, u.ImapUserEmail, u.UserPassword, rl.MailBox, rl.MailUID, ml.RawMessageSource, rl.RecordID  
						FROM MAIL_REMOVAL_LOG as rl 
						INNER JOIN MAIL_REMOVAL_MESSAGE_LOG as ml ON rl.MessageID = ml.MessageID 
						INNER JOIN INTRANET_USER as u ON u.ImapUserEmail = rl.UserEmail  
						WHERE rl.RecordID IN ('".implode("','",$RecordIdAry)."') 
							AND rl.RecordStatus = '1' 
							AND rl.RecordType = '1'  
							AND u.RecordStatus = '1' 
							AND u.ImapUserEmail IS NOT NULL AND u.ImapUserEmail <> ''
						ORDER BY u.UserID, rl.MailBox ";
				$data_list = $li->returnArray($sql);
				$numOfData = sizeof($data_list);
				
				if($intranet_authentication_method == 'HASH' || $intranet_authentication_method == 'LDAP'){
					$libpwm = new libpwm();
					$uid_ary = Get_Array_By_Key($data_list,'UserID');
					$uidToPw = $libpwm->getData($uid_ary);
				}
				
				$curUserLogin = "";
				$curMailBox = "";
				$records = array();
				for($i=0;$i<$numOfData;$i++){
					list($user_id, $user_login, $imap_user_email,$user_password,$mailbox, $mail_uid, $message_source, $record_id) = $data_list[$i];
					if($intranet_authentication_method == 'HASH' || $intranet_authentication_method == 'LDAP'){
						$user_password = $uidToPw[$user_id];
					}
					if($curUserLogin != $imap_user_email)
					{
						//$_SESSION['SSV_EMAIL_LOGIN'] = $imap_user_email;
	    				//$_SESSION['SSV_EMAIL_PASSWORD'] = $user_password;
	    				//$_SESSION['SSV_LOGIN_EMAIL'] = $imap_user_email;
						$IMap = new imap_gamma(false,$imap_user_email,$user_password);
						//$curUserLogin = $_SESSION['SSV_EMAIL_LOGIN'];
						$curUserLogin = $imap_user_email;
					}
					
					if($IMap->Go_To_Folder($mailbox))
					{
						$records[] = imap_append($IMap->inbox, $IMap->getMailServerRef().$mailbox, $message_source);
					}
				}
				$msg = !in_array(false,$records)?$Lang['General']['ReturnMessage']['UpdateSuccess']:$Lang['General']['ReturnMessage']['UpdateUnsuccess'];
			break;
			################################
		}
	}
}

intranet_closedb();
header("Location: view_result.php?record_status=".$record_status."&msg=".urlencode($msg));
?>