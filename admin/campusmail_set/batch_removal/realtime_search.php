<?php
// Editing by 
/*
 * 2015-07-28 (Carlos): Move real time search function from Removal setting list to this page. 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

## Only For iMail Gamma/Plus
if($plugin['imail_gamma']!==true){
	header("Location: ../index.php");
	exit();
}

$linterface = new interface_html();
$lg = new libgroup();
$AcademicYear = Get_Current_Academic_Year_ID();

$sql = "SELECT a.GroupID,CONCAT('(',b.CategoryName,') ',a.Title) as GroupTitle 
        FROM INTRANET_GROUP as a LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b ON a.RecordType = b.GroupCategoryID
        WHERE a.RecordType != 0 AND a.AcademicYearID = '$AcademicYear' 
        ORDER BY b.GroupCategoryID ASC, a.Title";
$groups = $lg->returnArray($sql);
$group_count = count($groups);
//$group_select = getSelectByArray($groups,"name=GroupID onChange=\"this.form.type[1].checked=true\"");

// $i_Campusquota_basic_identity
$select_target = "<select id=\"SelectTarget[]\" name=\"SelectTarget[]\" multiple=\"multiple\" size=\"20\">\n";
if(isset($webmail_identity_allowed) && sizeof($webmail_identity_allowed)>0){
	$select_target .= "<optgroup label=\"".$i_identity."\">";
}
if (in_array(1, $webmail_identity_allowed))
    $select_target .= "<option value=\"T1\">$i_identity_teachstaff</option>\n";
if (in_array(2, $webmail_identity_allowed))
    $select_target .= "<option value=\"T2\">$i_identity_student</option>\n";
if (in_array(3, $webmail_identity_allowed))
    $select_target .= "<option value=\"T3\">$i_identity_parent</option>\n";
if ($special_feature['alumni'] && in_array(4, $webmail_identity_allowed))
    $select_target .= "<option value=\"T4\">$i_identity_alumni</option>\n";

if(isset($webmail_identity_allowed) && sizeof($webmail_identity_allowed)>0){
	$select_target .= "</optgroup>";
}

$select_target .= "<optgroup label=\"".$i_admintitle_group."\">";
for($i=0;$i<$group_count;$i++){
	$select_target .= "<option value=\"G".$groups[$i]['GroupID']."\">".$groups[$i]['GroupTitle']."</option>";
}
$select_target .= "</optgroup>";

$select_target .= "</select>";

$select_all_btn = '<input type="button" name="btn_selectall" id="btn_selectall" value="'.$button_select_all.'" onclick="SelectAll(\'SelectTarget[]\');" />';

if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;


include_once("../../../templates/adminheader_setting.php");
?>
<script src="../../../templates/jquery/jquery-1.3.2.min.js"></script>
<script src="../../../templates/jquery/jquery.blockUI.js" type="text/JavaScript" language="JavaScript"></script>
<script typer="text/javascrip" language="JavaScript">
var userIdAry = [];
var timerId = null;
function PerformSearch()
{
	if(timerId) return;
	
	var select_obj = document.getElementById('SelectTarget[]');
	var selected_identity = [];
	var selected_group = [];
	for(var i=0;i<select_obj.options.length;i++){
		if(select_obj.options[i].selected){
			var v = select_obj.options[i].value.toString();
			var prefix = v.substr(0,1);
			if(prefix=='T')
				selected_identity.push(v.substr(1));
			else if(prefix=='G')
				selected_group.push(v.substr(1));
		}
	}
	if((selected_identity.length + selected_group.length) == 0)
	{
		select_obj.focus();
		alert('<?=$Lang['Group']['warnSelectGroup']?>');
		return;
	}
	
	if(confirm('<?=$Lang['Gamma']['ConfirmMg']['ConfirmDoRealTimeSearch']?>'))
	{
		$.post(
			'ajax_task.php',
			{
				'task':'get_email_user_id',
				'identity[]':selected_identity,
				'group[]':selected_group
			},
			function(data){
				userIdAry = data.split(',');
				DoSearch();
			}
		);
	}
}

function DoSearch()
{
	if(timerId) return;
	<?php if($sys_custom['iMailPlusBatchRemovalRealtimeActionDebug']){ ?>
	if(window.console){
		console.log(userIdAry);
		console.log('Number of user: ' + userIdAry.length);
	}
	<?php } ?>
	var total_user = userIdAry.length;
	var percent_done = 0;
	var delay = 1;
	blockMsg = '<?=$linterface->Get_Ajax_Loading_Image(1).$Lang['General']['Procesesing']?>';
	Block_Element('ContentDiv');
	
	var timerTask = function(){
		if(userIdAry.length == 0){
			percent_done = 100;
			UpdateProgress(percent_done);
			UnBlock_Element('ContentDiv');
			timerId = null;
			return;
		}
		var cur_user_id = userIdAry.shift();
		<?php if($sys_custom['iMailPlusBatchRemovalRealtimeActionDebug']){ ?>
		if(window.console){
			console.log('Number of user left is ' + userIdAry.length);
		}
		<?php } ?>
		UpdateProgress(percent_done);
		$.post(
			'ajax_task.php',
			{
				'task':'collect_mails',
				'TargetUserID':cur_user_id 
			},
			function(data){
				percent_done = Number(((total_user - userIdAry.length) / total_user) * 100).toFixed(2);
				UpdateProgress(percent_done);
				timerId = setTimeout(timerTask,delay);
			}
		);
	};
	timerId = setTimeout(timerTask,delay);
}

function UpdateProgress(percent_done)
{
	var bar = $('#progressbar');
	var barContainer = $('#progressbarContainer');
	var barText = $('#progressbarPercentText');
	var barImage = $('#progressbarPercentImage');
	var statusMsg =$('#statusMsg');
	
	barText.html(percent_done + '%');
	barImage.css('background-position','-'+(240-(240*(percent_done/100)))+'px 0px');
	barImage.attr('alt',percent_done+'%');
	barImage.attr('title',percent_done+'%');
	bar.show();
	barText.show();
	if(percent_done >= 100){
		statusMsg.show();
		
		UnBlock_Element('ContentDiv');
		setTimeout(
			function(){
				timerId = null;
				bar.hide();
				barText.hide();
				statusMsg.hide();
			},
			2000
		);
	}
}

function SelectAll(objId)
{
	var obj = document.getElementById(objId);
	for(var i=0;i<obj.options.length;i++){
		obj.options[i].selected = true;
	}
}

$(document).ready(function(){
	document.getElementById('SelectTarget[]').focus();
});
</script>
<form name="form1" action="" method="post" onsubmit="PerformSearch();return false;">
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, '../index.php',$Lang['Gamma']['BatchRemoval'],'index.php',$Lang['Gamma']['RealTimeSearch'],'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table id="progressbarContainer" width="560" cellspacing="0" cellpadding="0" border="0" align="center" style="margin:0 0 16px 0;">
	<tbody>
		<tr>
			<td align="center">
            	<span id="progressbar" class="progressBar" style="display:none;">
					<img width="240px" id="progressbarPercentImage" src="/images/space.gif" style="width: 240px; height: 18px; background-image: url('/images/progress_bar_green.gif'); padding: 0pt; margin: 0pt; background-position: -0px 0px;" alt="0%" title="0%">
				</span>
				<span id="progressbarPercentText" style="display:none;">0%</span>
				<span id="statusMsg" style="display:none;color:green;"><?=$Lang['Gamma']['Done']?></span>
			</td>
		</tr>
	</tbody>
</table>
<div id="ContentDiv">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<span class="extraInfo">[<span class=subTitle><?=$i_general_SelectTarget?></span>]</span>
<br><br>
<?=$select_target.'&nbsp;'.$select_all_btn?>
</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border='0'>
</td>
</tr>
</table>
</div>
</form>

<?php
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>