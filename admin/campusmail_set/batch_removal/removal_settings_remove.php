<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
intranet_opendb();

## Only For iMail Gamma/Plus
if($plugin['imail_gamma']!==true){
	header("Location: ../index.php");
	exit();
}

$IMap = new imap_gamma($skipLogin = 1);

$success = $IMap->Remove_Batch_Removal_Settings($RecordID);

$msg = $success?$Lang['General']['ReturnMessage']['DeleteSuccess']:$Lang['General']['ReturnMessage']['DeleteUnsuccess'];

intranet_closedb();
header("Location: removal_settings_list.php?msg=".urlencode($msg));
?>