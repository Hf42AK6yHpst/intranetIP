<?php
// editing by : 
/*
 * 2015-07-28 (Carlos): Added [Real time search] menu.
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");

# $li_menu = new libaccount()
if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;


## Only For iMail Gamma/Plus
if($plugin['imail_gamma']!==true){
	header("Location: ../index.php");
	exit();
}

?>
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, '../index.php',$Lang['Gamma']['BatchRemoval'],'')?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300>
<blockquote>
<?
	echo displayOption($Lang['Gamma']['RemovalSettings'],'removal_settings_list.php',1);
	echo displayOption($Lang['Gamma']['RealTimeSearch'],'realtime_search.php',1);
	echo displayOption($Lang['Gamma']['FilteredRemovedMails'], 'view_result.php', 1);
?>
</blockquote>
</td></tr>
</table>

<?
include_once("../../../templates/adminfooter.php");
?>