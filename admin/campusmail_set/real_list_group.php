<?php
#using : 
/*
 * 2017-06-29 (Carlos): Added Quota Used table column and search text input.
 */
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

if ($GroupID == "")
{
	header("Location: index.php");
    exit();
}
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

intranet_opendb();

# Search Group
$lgroup = new libgroup($GroupID);
$groupname = $lgroup->Title;

if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        case 6: $field = 6; break;
        default: $field = 0; break;
}

$order = ($order == 1) ? 1 : 0;
$namefield = getNameFieldByLang("b.");
/*
$sql  = "SELECT
               d.Title,
               CONCAT('<a class=tableContentLink href=real_list_edit.php?UserID[]=',a.UserID,'>',$namefield,'</a>'),
               b.UserLogin,
               b.ClassName, b.ClassNumber,
               c.Quota,
               CONCAT('<input type=checkbox name=UserID[] value=', a.UserID ,'>')
               FROM
                   INTRANET_USERGROUP as a
                   LEFT OUTER JOIN INTRANET_USER AS b ON a.UserID = b.UserID
                LEFT OUTER JOIN
                        INTRANET_CAMPUSMAIL_USERQUOTA AS c ON a.UserID = c.UserID
                LEFT OUTER JOIN INTRANET_ROLE as d ON a.RoleID = d.RoleID
                WHERE
                        a.GroupID = $GroupID AND b.UserID IS NOT NULL AND a.DateInput IS NOT NULL AND a.DateModified IS NOT NULL
                ";
*/
$sql  = "SELECT
			d.Title,
			CONCAT('<a class=tableContentLink href=real_list_edit.php?UserID[]=',a.UserID,'>',$namefield,'</a>'),
			b.UserLogin,
			b.ClassName, b.ClassNumber,
			CONCAT(IF(s.QuotaUsed IS NULL,0,ROUND(s.QuotaUsed/1024.00,2)),'MB') as QuotaUsed,
			CONCAT(IF(c.Quota IS NULL,0,c.Quota),'MB') as Quota,
			CONCAT('<input type=checkbox name=UserID[] value=', a.UserID ,'>')
		FROM 
			INTRANET_USERGROUP as a
			LEFT JOIN INTRANET_USER AS b ON a.UserID = b.UserID
			LEFT JOIN INTRANET_CAMPUSMAIL_USERQUOTA AS c ON a.UserID = c.UserID
			LEFT JOIN INTRANET_CAMPUSMAIL_USED_STORAGE as s ON s.UserID=b.UserID 
			LEFT JOIN INTRANET_ROLE as d ON a.RoleID = d.RoleID
		WHERE
			a.GroupID = '$GroupID' AND b.UserID IS NOT NULL ";
	if($keyword != ''){
		$safe_keyword = $lgroup->Get_Safe_Sql_Like_Query($keyword);
        $sql .= " AND ($namefield like '%$safe_keyword%' OR b.UserLogin like '%$safe_keyword%') ";
	}
$sql .=	" GROUP BY a.GroupID, a.UserID";
// echo '<pre>'.$sql.'</pre>';
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("d.Title","b.EnglishName","b.UserLogin", "b.ClassName", "b.ClassNumber", "QuotaUsed", "Quota");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_group;
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$name_title = ($intranet_session_language=="en"?$i_UserEnglishName:$i_UserChineseName);
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_adminmenu_role)."</td>\n";
$li->column_list .= "<td width=18% class=tableTitle>".$li->column($pos++, $name_title)."</td>\n";
$li->column_list .= "<td width=17% class=tableTitle>".$li->column($pos++, $i_UserLogin)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_LinuxAccount_UsedQuota)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_CampusMail_New_Quota)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("UserID[]")."</td>\n";

$toolbar = "<a class=iconLink href=\"javascript:checkGet(document.form1,'real_list_group_edit.php')\">".newIcon()."$i_CampusMail_New_BatchUpdate</a>";
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'UserID[]','real_list_edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>\n";
$searchbar .= "<input class=\"text\" type=\"text\" name=\"keyword\" size=\"10\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

?>
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$Lang['Gamma']['UserUsageRightsAndQuota'],'quotaindex.php',$i_CampusMail_New_QuotaSettings,'realquota.php',$i_CampusMail_New_ViewQuota_Group." ".$groupname,'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>
<form name="form1" method="get">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=GroupID value="<?=$GroupID?>">
</form>
<?php
include_once("../../templates/adminfooter.php");
?>