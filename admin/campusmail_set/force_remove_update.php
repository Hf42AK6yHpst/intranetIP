<?php
// editing by 
/*
 * 2018-11-13 (Carlos): Added $sys_custom['iMailPlus']['ForceRemovalExcludeUserID'] to exclude certain users avoid being cleaned emails.
 * 2012-08-14 (Carlos): iMail plus - Force removal
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
intranet_opendb();

## Only For iMail Gamma/Plus
if($plugin['imail_gamma']!==true){
	header("Location: index.php");
	exit();
}

$li = new libdb();

$TargetUserType = $_REQUEST['TargetUserType'];
$ActionType = $_REQUEST['ActionType'];
$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];

$more_conds = "";
if(isset($sys_custom['iMailPlus']['ForceRemovalExcludeUserID']) && is_array($sys_custom['iMailPlus']['ForceRemovalExcludeUserID']) && count($sys_custom['iMailPlus']['ForceRemovalExcludeUserID'])>0){
	$more_conds .= " AND u.UserID NOT IN (".implode(",",$sys_custom['iMailPlus']['ForceRemovalExcludeUserID']).") ";
}

$sql = "SELECT u.UserID FROM INTRANET_USER as u 
		WHERE u.RecordType='".$TargetUserType."' $more_conds 
			AND u.ImapUserEmail IS NOT NULL 
			AND u.ImapUserEmail <> '' ";

$users = $li->returnVector($sql);
$numOfUser = count($users);
$numOfSplice = max(10,floor($numOfUser/100));

$var_user_array = "var user_array = [".implode(",",$users)."];\n";
?>
<script src="../../templates/jquery/jquery-1.3.2.min.js" type="text/JavaScript" language="JavaScript"></script>
<script type="text/JavaScript" language="JavaScript">
var TargetUserType = '<?=$TargetUserType?>';
var ActionType = '<?=$ActionType?>';
var StartDate = '<?=$StartDate?>';
var EndDate = '<?=$EndDate?>';
var parentWin = window.parent;
var numOfUser = <?=$numOfUser?>;
var numOfSplice = <?=$numOfSplice?>;
<?=$var_user_array?>

function ajRemoveMails()
{
	var remove_users = user_array.splice(0,numOfSplice);
	if(remove_users.length > 0){
		/*
		$.post(
			'ajax_force_remove.php',
			{
				'TargetUserType':TargetUserType,
				'ActionType':ActionType,
				'StartDate':StartDate,
				'EndDate':EndDate,
				'RemoveUserID[]':remove_users
			},
			function(data){
				var left_count = user_array.length;
				if(left_count > 0){
					parentWin.UpdateProgress(Math.floor(((numOfUser - left_count)/numOfUser) * 100));
					setTimeout(ajRemoveMails,100);
				}else{
					parentWin.UpdateProgress(100);
				}
			}
		);
		*/
		$.ajax({
			url: 'ajax_force_remove.php',
			type: 'POST',
			data: {
				'TargetUserType':TargetUserType,
				'ActionType':ActionType,
				'StartDate':StartDate,
				'EndDate':EndDate,
				'RemoveUserID[]':remove_users
			},
			success: function(data){
				var left_count = user_array.length;
				if(left_count > 0){
					parentWin.UpdateProgress(Math.floor(((numOfUser - left_count)/numOfUser) * 100));
					setTimeout(ajRemoveMails,100);
				}else{
					parentWin.UpdateProgress(100);
				}
			},
        	error: function(jqXHR, textStatus, errorThrown){
        		var left_count = user_array.length;
				if(left_count > 0){
					parentWin.UpdateProgress(Math.floor(((numOfUser - left_count)/numOfUser) * 100));
					setTimeout(ajRemoveMails,100);
				}else{
					parentWin.UpdateProgress(100);
				}
        	}
		});
	}else{
		parentWin.UpdateProgress(100);
	}
}

$(document).ready(function(){
	ajRemoveMails();
});
</script>
<?php
intranet_closedb();
?>