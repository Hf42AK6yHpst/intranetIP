<?php
// editing by 
/************************************************* Change log ****************************************************
 * 2011-10-19 (Carlos): Added alumni access and quota setting
 *****************************************************************************************************************/
include("../../includes/global.php");
include("../../includes/libfilesystem.php");

/*
$content = array("");
for ($i=0; $i<4; $i++)
{
     $allow = ${"allow$i"};
     $allow = ($allow==""? 0:1);
     $content[$i] = "$allow:".${"quota$i"};
}
$file_content = implode("\n",$content);
*/

for ($i=0; $i<4; $i++)
{
        if (${"allow$i"} == "")
        {
                ${"allow$i"} = 0;
        }
        if (${"quota$i"} == "")
        {
                ${"quota$i"} = 0;
        }
}

$li = new libfilesystem();
if($plugin['imail_gamma'] === true )
{
//	$location = $intranet_root."/file/gamma_mail";
//	$li->folder_new($location);
//	$file = $location."/internet_mail_access.txt";
//	$file_content = $enable;
//	$li->file_write($file_content, $file);
	include("../../includes/imap_gamma.php");
	$IMap = new imap_gamma();
	$IMap->setInternetMailAccess($enable);

//	$file_content = "";
//	$file_content .= "$allow0:$quota0\n$allow1:$quota1\n0:0\n$allow2:$quota2";
//	# echo $file_content;
//	$li->file_write($file_content, $intranet_root."/file/campusmail_set.txt");
	$original_quota  = $IMap->default_quota;
	$access_right[] = $allow0;
	$access_right[] = $allow1;
	$access_right[] = $allow2;
	$access_right[] = $allow3;
	$default_quota[] = $allow0!=1?$original_quota[0]:$quota0;
	$default_quota[] = $allow1!=1?$original_quota[1]:$quota1;
	$default_quota[] = $allow2!=1?$original_quota[2]:$quota2;
	$default_quota[] = $allow3!=1?$original_quota[3]:$quota3;
	
	$IMap->setIndentityGroupAccessQuota($access_right,$default_quota);
	
	if($enable==1){
		for($i=0;$i<4;$i++){
			if(${"internal_only$i"}==1){
				$internal_only[] = 1;
			}else{
				$internal_only[] = 0;
			}
		}
		$IMap->setInternalMailOnly($internal_only);
	}
}
else
{
	$file_content = "";
	$file_content .= "$allow0:$quota0\n$allow1:$quota1\n$allow3:$quota3\n$allow2:$quota2";
	# echo $file_content;
	$li->file_write($file_content, $intranet_root."/file/campusmail_set.txt");
}

header("Location: access.php?msg=2");
?>