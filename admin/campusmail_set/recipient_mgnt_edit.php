<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libwebmail.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../lang/lang.$intranet_session_language.php");

if($usertype=="")
	header("Location: index.php");

include_once("../../templates/adminheader_setting.php");

if ($special_feature['imail'])
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}

intranet_opendb();

$db = new libdb();

//$interface =$interface==""?1:$interface;

# Student
if($usertype==2){
	# listing out class levels
	if($ClassLevelID==""){
		$sql="SELECT ClassLevelID, LevelName FROM INTRANET_CLASSLEVEL WHERE RecordStatus IN(1) ORDER BY LevelName";
		$temp = $db->returnArray($sql,2);
		$select_class_level="$i_ClassLevel : <SELECT name=ClassLevel onChange=\"changeClassLevel(this)\">";
		$select_class_level.="<OPTION VALUE=\"\"> -- $button_select --</OPTION>";
		for($i=0;$i<sizeof($temp);$i++){
			list($level_id, $level_name) = $temp[$i];
			$select_class_level.="<OPTION value=\"$level_id\">$level_name</OPTION>";
		}
		$select_class_level.="</SELECT>\n";
		$title = displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$i_campusmail_recipient_selection_control,'recipient_mgnt.php',"$i_identity_student($i_ClassLevel)",'');
		$display_content = $select_class_level;
	}
	# show the current settings of choosen class level
	else {
		$sql ="SELECT b.LevelName,a.RecordID,a.Restricted,a.ToStaffOption,a.ToStudentOption,a.ToParentOption,a.BlockedGroupCat,a.BlockedUserType FROM INTRANET_CLASSLEVEL AS b LEFT OUTER JOIN  INTRANET_IMAIL_RECIPIENT_RESTRICTION AS a ON (a.ClassLevel = b.ClassLevelID AND a.TargetType='$usertype') WHERE b.ClassLevelID='$ClassLevelID'";
		$temp = $db->returnArray($sql,8);
		list($level_name,$recordid,$restricted,$toStaff,$toStudent,$toParent,$blockedGroups,$blockedUserTypes )=$temp[0];
		
		$title = displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$i_campusmail_recipient_selection_control,'recipient_mgnt.php',"$i_identity_student($i_ClassLevel)",'recipient_mgnt_edit.php?usertype=2',$level_name,'');
		
		# use new interface
	//	if($restricted==1){
				# to Staff selection
				$select_to_staff = "<SELECT name='toStaff'>
							<OPTION VALUE=0 ".($toStaff==0?"SELECTED":"").">$i_campusmail_all_teacher_staff</OPTION>
							<OPTION VALUE=1 ".($toStaff==1?"SELECTED":"").">$i_campusmail_teaching_sender_teacher</OPTION>
							<OPTION VALUE=-1 ".($toStaff==-1?"SELECTED":"").">$i_campusmail_no_teacher_staff</OPTION>
							</SELECT>";
				# to Student Selection
				$select_to_student="<SELECT name='toStudent'>
						<OPTION VALUE=0 ".($toStudent==0?"SELECTED":"").">$i_campusmail_all_students</OPTION>
						<OPTION VALUE=2 ".($toStudent==2?"SELECTED":"").">$i_campusmail_only_students_in_same_class_level</OPTION>
						<OPTION VALUE=3 ".($toStudent==3?"SELECTED":"").">$i_campusmail_only_students_in_same_class</OPTION>
						<OPTION VALUE=-1 ".($toStudent==-1?"SELECTED":"").">$i_campusmail_no_students</OPTION>
						</SELECT>";

				# to Parent Selection
				$select_to_parent ="<SELECT name='toParent'>
							<OPTION VALUE=0 ".($toParent==0?"SELECTED":"").">$i_campusmail_all_parents</OPTION>
							<OPTION VALUE=2 ".($toParent==2?"SELECTED":"").">$i_campusmail_only_parents_with_children_in_same_class_level</OPTION>
							<OPTION VALUE=3 ".($toParent==3?"SELECTED":"").">$i_campusmail_only_parents_with_children_in_same_class</OPTION>
							<OPTION VALUE=4 ".($toParent==4?"SELECTED":"").">$i_campusmail_only_own_parents</OPTION>
							<OPTION VALUE=-1 ".($toParent==-1?"SELECTED":"").">$i_campusmail_no_parents</OPTION>
							</SELECT>";
	//	}
		# use old interface
	//	else{

	//	}
	}
}
# Staff
else if($usertype==1){
	$teaching=$teaching==1?$teaching:0;
	$sql="SELECT Restricted,ToStaffOption,ToStudentOption,ToParentOption,BlockedGroupCat,BlockedUserType FROM INTRANET_IMAIL_RECIPIENT_RESTRICTION WHERE TargetType='$usertype' AND Teaching='$teaching'";
	$temp = $db->returnArray($sql,6);
	list($restricted,$toStaff,$toStudent,$toParent,$blockedGroups,$blockedUserTypes)=$temp[0];
	$title_name = $teaching==1?$i_teachingStaff:$i_nonteachingStaff;
	$title = displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$i_campusmail_recipient_selection_control,'recipient_mgnt.php',"$title_name",'');

	# use new interface
//	if($restricted==1){
			# to Staff selection
			$select_to_staff = "<SELECT name='toStaff'>
						<OPTION VALUE=0 ".($toStaff==0?"SELECTED":"").">$i_campusmail_all_teacher_staff</OPTION>
						<OPTION VALUE=-1 ".($toStaff==-1?"SELECTED":"").">$i_campusmail_no_teacher_staff</OPTION>
						</SELECT>";
			# to Student Selection
			$select_to_student="<SELECT name='toStudent'>
						<OPTION VALUE=0 ".($toStudent==0?"SELECTED":"").">$i_campusmail_all_students</OPTION>";
			if($teaching==1)
						$select_to_student.="<OPTION VALUE=1 ".($toStudent==1?"SELECTED":"").">$i_campusmail_only_students_in_taught_class</OPTION>";
			$select_to_student.="<OPTION VALUE=-1 ".($toStudent==-1?"SELECTED":"").">$i_campusmail_no_students</OPTION></SELECT>";

			# to Parent Selection
			$select_to_parent ="<SELECT name='toParent'>
						<OPTION VALUE=0 ".($toParent==0?"SELECTED":"").">$i_campusmail_all_parents</OPTION>";
			if($teaching==1)
					$select_to_parent.="<OPTION VALUE=1 ".($toParent==1?"SELECTED":"").">$i_campusmail_only_parents_with_children_in_taught_class</OPTION>";
			$select_to_parent.="<OPTION VALUE=-1 ".($toParent==-1?"SELECTED":"").">$i_campusmail_no_parents</OPTION>
						</SELECT>";
	//}

	
}
# Parent
else if($usertype==3){
	$sql="SELECT Restricted,ToStaffOption,ToStudentOption,ToParentOption,BlockedGroupCat,BlockedUserType FROM INTRANET_IMAIL_RECIPIENT_RESTRICTION WHERE TargetType='$usertype'";
	$temp = $db->returnArray($sql,6);
	list($restricted,$toStaff,$toStudent,$toParent,$blockedGroups,$blockedUserTypes)=$temp[0];
	$title = displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$i_campusmail_recipient_selection_control,'recipient_mgnt.php',"$i_identity_parent",'');

	# use new interface
	//if($restricted==1){
			# to Staff selection
			$select_to_staff = "<SELECT name='toStaff'>
						<OPTION VALUE=0 ".($toStaff==0?"SELECTED":"").">$i_campusmail_all_teacher_staff</OPTION>
						<OPTION VALUE=1 ".($toStaff==1?"SELECTED":"").">$i_campusmail_teaching_sender_children_teacher</OPTION>
						<OPTION VALUE=-1 ".($toStaff==-1?"SELECTED":"").">$i_campusmail_no_teacher_staff</OPTION>
						</SELECT>";
			# to Student Selection
			$select_to_student="<SELECT name='toStudent'>
						<OPTION VALUE=0 ".($toStudent==0?"SELECTED":"").">$i_campusmail_all_students</OPTION>
						<OPTION VALUE=2 ".($toStudent==2?"SELECTED":"").">$i_campusmail_only_students_in_same_class_level</OPTION>
						<OPTION VALUE=3 ".($toStudent==3?"SELECTED":"").">$i_campusmail_only_students_in_same_class</OPTION>
						<OPTION VALUE=4 ".($toStudent==4?"SELECTED":"").">$i_campusmail_only_sender_own_children</OPTION>
						<OPTION VALUE=-1 ".($toStudent==-1?"SELECTED":"").">$i_campusmail_no_students</OPTION>
						</SELECT>";
					
			# to Parent Selection
			$select_to_parent ="<SELECT name='toParent'>
						<OPTION VALUE=0 ".($toParent==0?"SELECTED":"").">$i_campusmail_all_parents</OPTION>
						<OPTION VALUE=2 ".($toParent==2?"SELECTED":"").">$i_campusmail_only_parents_with_children_in_same_class_level</OPTION>
						<OPTION VALUE=3 ".($toParent==3?"SELECTED":"").">$i_campusmail_only_parents_with_children_in_same_class</OPTION>
						<OPTION VALUE=-1 ".($toParent==-1?"SELECTED":"").">$i_campusmail_no_parents</OPTION>
						</SELECT>";
				
	//}
}

# display content
if(!($usertype==2 && $ClassLevelID=="")){
	$interface = $interface==""?($restricted==""?0:$restricted):$interface;
	$select_interface = "<SELECT name='interface' onChange='changeInterface(this.form)'>
	<OPTION value=0 ".($interface==0?"SELECTED":"").">$i_campusmail_use_old_interface</OPTION>
	<OPTION value=1 ".($interface==1?"SELECTED":"").">$i_campusmail_use_new_interface</OPTION>
	</SELECT>";
	
	# use new interface
	if($interface==1){
			$display_content="<table border=0 cellpadding=4 cellspacing=2>
				<tr><Td align=right>$i_campusmail_select_interface :</td><td>$select_interface<br></td></tr>
				<tr><Td align=right>$i_campusmail_staff_recipients_selection :</td><td>$select_to_staff<br></td></tr>
				<tr><Td align=right>$i_campusmail_student_recipients_selection :</td><td>$select_to_student<Br></td></tr>
				<tr><td align=right>$i_campusmail_parent_recipients_selection :</td><td>$select_to_parent<Br></td></tr>
				<tr><td colspan=2 align=right><a href='javascript:submitForm(document.form1)'><img src=\"/images/admin/button/s_btn_save_".$intranet_session_language.".gif\" border='0'></a>".
		 		btnReset()."</td></tr>
				</table>";
	}
	# use old interface
	else{
				# blocked groups
				$select_group_cat = "<SELECT name='group_cat' class='select_group' multiple>";
				$select_blocked_group_cat="<SELECT name='blocked_group_cat' class='select_group' multiple>";
				$lgroupcat = new libgroupcategory();
				$cats = $lgroupcat->returnAllCat();
				
				$blockedGroupCats = explode(",",$blockedGroups);
				for($k=0;$k<sizeof($cats);$k++){
					list($catid,$cat_name)=$cats[$k];
					if($catid==0) continue;
					if(!in_array($catid,$blockedGroupCats)){
						$select_group_cat .="<OPTION value='$catid'>$cat_name</OPTION>";
					}else{
						$select_blocked_group_cat.="<OPTION value='$catid'>$cat_name</OPTION>";
					}
				}
				$select_group_cat .="</SELECT>";
				$select_blocked_group_cat.="</SELECT>";
				
				# blocked usertype
				$usertypes[]=array(1,$i_campusmail_teaching_staff);
				$usertypes[]=array(2,$i_identity_student);
				$usertypes[]=array(3,$i_identity_parent);
				$usertypes[]=array(5,$i_campusmail_non_teaching_staff);
				
				$blocked_usertypes = explode(",",$blockedUserTypes);
				$select_blocked_usertype="<SELECT name='blocked_usertype' class='select_group'  multiple>";
				$select_usertype="<SELECT name='select_usertype'  class='select_group' multiple>";
				for($k=0;$k<sizeof($usertypes);$k++){
					list($usertypeID,$usertype_name)=$usertypes[$k];
					if(!in_array($usertypeID,$blocked_usertypes)){
						$select_usertype.="<OPTION value='$usertypeID'>$usertype_name</OPTION>";
					}else
						$select_blocked_usertype.="<OPTION value='$usertypeID'>$usertype_name</OPTION>";
				}
				$select_usertype.="</SELECT>";
				$select_blcoked_usertype.="</SELECT>";
				
		# old interface display content
			$display_content="<table border=0 cellpadding=4 cellspacing=2>
				<tr><Td>$i_campusmail_select_interface :</td><td colspan=2>$select_interface<br><br></td></tr>
				<tr><td>$i_campusmail_group_categories:</td><td></td><Td>$i_campusmail_blocked_groups:</td></tr>
				<tr><td>$select_group_cat</td><Td align=center>
					<a href='javascript:moveOptions(document.form1.group_cat,document.form1.blocked_group_cat)'><img src='/images/admin/button/next.gif' border=0></a><br><Br>
					<a href='javascript:moveOptions(document.form1.blocked_group_cat,document.form1.group_cat)'><img src='/images/admin/button/prev.gif' border=0></a>
				</td><td>$select_blocked_group_cat<br><br><Br></td></tr>
				<tr><td>$i_campusmail_usertypes:</td><td></td><Td>$i_campusmail_blocked_usertypes:</td></tr>
				<tr><td>$select_usertype</td><Td align=center>
					<a href='javascript:moveOptions(document.form1.select_usertype,document.form1.blocked_usertype)'><img src='/images/admin/button/next.gif' border=0></a><br><Br>
					<a href='javascript:moveOptions(document.form1.blocked_usertype,document.form1.select_usertype)'><img src='/images/admin/button/prev.gif' border=0></a>
					</td><td>$select_blocked_usertype</td></tr>
				<tr><td colspan=3 align=right><a href='javascript:submitForm(document.form1)'><img src=\"/images/admin/button/s_btn_save_".$intranet_session_language.".gif\" border='0'></a>
		 		</td></tr>
				</table>
				<input type=hidden name='blocked_group_cat_ids' value=''>
				<input type=hidden name='blocked_usertype_ids' value=''>
				";
	}
}
?>

<?=$title?>
<?= displayTag("$img_tag", $msg) ?>
<style type="text/css">
.select_group{width:200px; height=150px;}
</style>
<script language="javascript">
function changeClassLevel(selectObj){
	if(selectObj==null) return;
	index = selectObj.selectedIndex;
	levelID = selectObj.options[index].value;
	if(levelID=='') return;
	formObj = document.form1;
	if(formObj!=null){
		if(formObj.ClassLevelID!=null){
			formObj.ClassLevelID.value=levelID;
		}
		else{
			 return false;
		}
		formObj.action="";
		formObj.submit();
	}
}
function changeInterface(formObj){
	formObj.action="";
	formObj.submit();
}
function submitForm(formObj){
	objBG = formObj.blocked_group_cat;
	objBU = formObj.blocked_usertype;
	objBGID = formObj.blocked_group_cat_ids;
	objBUID = formObj.blocked_usertype_ids;
	
	if(objBG !=null && objBGID!=null){
		str ="";
		for(i=0;i<objBG.options.length;i++){
			str+=objBG.options[i].value+",";
		}
		if(str!="")
			str = str.substring(0,str.length-1);
		objBGID.value = str;
	}
	if(objBU!=null && objBUID!=null){
		str="";
		for(i=0;i<objBU.options.length;i++){
			str+=objBU.options[i].value+",";
		}
		if(str!="")
			str = str.substring(0,str.length-1);
		objBUID.value = str;
	}	
	formObj.submit();
}
function moveOptions(srcObj,dstObj){
	if(srcObj==null || dstObj==null) return;
	for(i=0;i<srcObj.options.length;i++){
		if(srcObj.options[i].selected){
			txt = srcObj.options[i].text;
			val = srcObj.options[i].value;
			
			dstObj.options[dstObj.options.length] = new Option(txt,val);
			srcObj.options[i]=null;
			i--;
		}
			
	}
}

</script>
<form name=form1 action="recipient_mgnt_edit_update.php" method=get>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<Tr><Td align=center>
<?=$display_content?>
</td></tr>
</table>
<input type=hidden name=ClassLevelID value=<?=$ClassLevelID?>>
<input type=hidden name=usertype value=<?=$usertype?>>
<input type=hidden name=teaching value=<?=$teaching?>>
</form>
<?
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>