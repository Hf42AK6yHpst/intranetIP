<?php
// editing by 
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");

$li = new libfilesystem();

if($plugin['imail_gamma'] !== true )
{
	// update policy
	$file_content = $banlist;
	$li->file_write($file_content, $intranet_root."/file/campusmail_banlist.txt");
	
}
else
{
	include_once("../../includes/libdb.php");
	include_once("../../includes/imap_gamma.php");
	intranet_opendb();
	
	$libdb = new libdb();
	$IMap = new imap_gamma($skipLogin = true);
	
	# ban send email list
	$IMap->setSendMailBanList($banlist);
	
//	$file_content = $banlist;
//	$li->file_write($file_content, $intranet_root."/file/campusmail_banlist.txt");
	
	# get and trim exist block list
	//$gamma_ban_list = get_file_content("$intranet_root/file/gamma_mail_banlist.txt");
	$gamma_ban_list = $IMap->ban_ext_mail_list;
	$ExistUserLoginArr = explode("\n",trim($gamma_ban_list)); 
	for($i=0; $i<count($ExistUserLoginArr);$i++)
		$ExistUserLoginArr[$i] = trim($ExistUserLoginArr[$i]);
		
	# trim new block list
	$UserLoginArr = explode("\n",trim($DisallowSendReceive));
	for($i=0; $i<count($UserLoginArr);$i++)
		$UserLoginArr[$i] = trim($UserLoginArr[$i]);		
	 
	$RemoveBlockList = array_diff($ExistUserLoginArr,$UserLoginArr);
	$AddBlockList = array_diff($UserLoginArr,$ExistUserLoginArr);
	$AllUserLogin = array_union($ExistUserLoginArr,$UserLoginArr);
	
	foreach((array)$AllUserLogin as $UserInfo)
		$UserLoginSql[] = "'".trim($UserInfo)."'"; 	

	$sql = "
		SELECT 
			UserLogin,	
			UserID,
			IMapUserEmail 
		FROM 
			INTRANET_USER
		WHERE
			UserLogin IN( ".implode(",",$UserLoginSql).") 
		UNION 
		(
		SELECT 
			SUBSTRING_INDEX(MailBoxName,'@',1) as UserLogin,
			MailBoxID as UserID,
			MailBoxName as IMapUserEmail 
		FROM
			MAIL_SHARED_MAILBOX 
		WHERE 
			SUBSTRING_INDEX(MailBoxName,'@',1) IN (".implode(",",$UserLoginSql).") 
		)
	";
	//debug_pr($sql);
	$result = $libdb->returnArray($sql,2);

	$final_ban_list = array(); 
	$add_key = $del_key = $add_ctr = $del_ctr = 0;
	foreach((array)$result as $UserEmail)
	{
		list($ulogin, $uid, $email)  = $UserEmail;
		if(trim($email)=='') 
		{
			continue;
		}
		
		if(in_array($ulogin,(array)$AddBlockList))
		{
			$AddBlockEmailArr[$add_key][$ulogin] = $email;
			if(++$add_ctr%50==0) // avoid too long url
				$add_key++;
		}
		else if(in_array($ulogin,(array)$RemoveBlockList))
		{
			$RemoveBlockEmailArr[$del_key][$ulogin] = $email;
			
			if(++$del_ctr%50==0) // avoid too long url
				$del_key++;
		}
		else // keep in list
		{
			$final_ban_list[] = $ulogin;
		} 
	}
	$success = array();
	foreach( (array)$AddBlockEmailArr as $AddBlockEmailList)
	{
		if(count($AddBlockEmailList)>0)
		{
			if($IMap->addGroupBlockExternal(array_values($AddBlockEmailList)))
			{
				$final_ban_list = array_merge((array)$final_ban_list,array_keys($AddBlockEmailList));
				$success[]=true;
			}
			else
			{
				$success[]=false;
			}
		}
	}
	
	
	foreach( (array)$RemoveBlockEmailArr as $RemoveBlockEmailList)
	{
		if(count($RemoveBlockEmailList)>0)
		{
			if(!$IMap->removeGroupBlockExternal(array_values($RemoveBlockEmailList)))
			{
				//$final_ban_list = array_merge($final_ban_list,array_keys($RemoveBlockEmailList));
				$success[]=false;
			}
			else
			{
				$success[]=true;
			}
			
		}
	}
	
	$IMap->setExternMailBanList($final_ban_list);
	
//	$file_content = implode("\r\n",(array)$final_ban_list);
//	
//	$li->file_write($file_content, $intranet_root."/file/gamma_mail_banlist.txt");	
	
	if(in_array(false,$success))
	{
		header("Location: ban.php?msg=13");
		exit;	
	}	
}

header("Location: ban.php?msg=2");
?>