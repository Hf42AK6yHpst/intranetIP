<?
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$sql = "SELECT CardID, EnglishName, ChineseName, ClassName, ClassNumber
        FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1
        ORDER BY ClassName, ClassNumber, EnglishName";

$result = $li->returnArray($sql,5);
$x = "CardID, EnglishName, ChineseName, ClassName, Class Number\n";
for ($i=0; $i<sizeof($result); $i++)
{
     list($cardid,$engname,$chiname,$class,$classnumber) = $result[$i];
     $x .= "$cardid###$engname###$chiname###$class###$classnumber\n";
}

// Output the file to user browser
$filename = "studentinfo.csv";
header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($x) );
header("Content-Disposition: attachment; filename=\"".$filename."\"");

echo $x;
intranet_closedb();
?>
