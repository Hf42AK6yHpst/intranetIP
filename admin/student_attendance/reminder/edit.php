<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$ReminderID = (is_array($ReminderID)? $ReminderID[0]:$ReminderID);

$li = new libdb();
$user_field = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT a.StudentID, $user_field, a.DateOfReminder, a.TeacherID, a.Reason FROM CARD_STUDENT_REMINDER as a
        LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus = 1 WHERE ReminderID = $ReminderID";
$result = $li->returnArray($sql,5);
list($studentID, $name, $remindDate, $TeacherID, $Reason) = $result[0];

        $lcard = new libcardstudentattend();
        $lclass = new libclass();

        $namefield = getNameFieldByLang();
        $sql = "SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType = 1 ORDER BY EnglishName";
        $staff = $lclass->returnArray($sql,2);
        $select_teacher = getSelectByArray($staff,"name=TeacherID","$TeacherID",1,1);


echo displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Reminder,'index.php',$button_edit,'');
echo displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg);
?>
<script language="javascript">
</SCRIPT>
<form name=form1 action="edit_update.php" method=POST>
<table width=90% border=0 align=center>
<tr><td><?=$i_UserStudentName?></td><td><?=$name?></td></tr>
<tr><td><?=$i_StudentAttendance_Reminder_Date?></td><td><input type=text name=RemindDate size=10 maxlength=10 value="<?=$remindDate?>">(YYYY-MM-DD)</td></tr>
<tr><td><?=$i_StudentAttendance_Reminder_Teacher?></td><td><?=$select_teacher?></td></tr>
<tr><td><?=$i_StudentAttendance_Reminder_Reason?></td><td><TEXTAREA rows=5 cols=50 name=Reason><?=$Reason?></TEXTAREA></td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
<input type=hidden name=ReminderID value="<?=$ReminderID?>">
</form>
<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>
