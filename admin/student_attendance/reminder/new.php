<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$li = new libdb();


echo displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Reminder,'index.php',$button_new,'');
echo displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg);

if (!isset($ClassName) || $ClassName == "")
{
        $sql = "SELECT DISTINCT ClassName FROM INTRANET_USER WHERE ClassName != '' ORDER BY ClassName";
        $classes = $li->returnVector($sql);
        $select_class = getSelectByValue($classes,"name=ClassName onChange=this.form.submit()");
        $x = "<form name=form1 action='' method=GET>\n";
        $x .= "<table width=90% border=0 align=center><tr><td>\n";
        $x .= "$i_UserParentLink_SelectClass: $select_class";
        $x .= "</td></tr></table></form>\n";
        echo $x;
}
else
{
        $lclass = new libclass();
        $select_class = $lclass->getSelectClass("name=ClassName onChange=\"this.form.action='';this.form.submit()\"",$ClassName);
        $select_student = $lclass->getStudentSelectByClass($ClassName,"name=StudentID");

        $lcard = new libcardstudentattend();


        $namefield = getNameFieldByLang();
        $sql = "SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType = 1 ORDER BY EnglishName";
        $staff = $lclass->returnArray($sql,2);
        $select_teacher = getSelectByArray($staff,"name=TeacherID","",1,1);
?>
<form name=form1 action="new_update.php" method=POST>
<table width=90% border=0 align=center>
<tr><td><?=$i_SmartCard_ClassName?></td><td><?=$select_class?></td></tr>
<tr><td><?=$i_UserStudentName?></td><td><?=$select_student?></td></tr>
<tr><td><?=$i_StudentAttendance_Reminder_Date?></td><td><input type=text name=RemindDate size=10 maxlength=10 value="<?=date('Y-m-d',time()+60*60*24)?>">(YYYY-MM-DD)</td></tr>
<tr><td><?=$i_StudentAttendance_Reminder_Teacher?></td><td><?=$select_teacher?></td></tr>
<tr><td><?=$i_StudentAttendance_Reminder_Reason?></td><td><TEXTAREA rows=5 cols=50 name=Reason></TEXTAREA></td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
</form>
<?
}
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>
