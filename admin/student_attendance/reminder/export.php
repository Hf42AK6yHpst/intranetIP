<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

if ($filter == 1)
{
    $conds .= " AND a.DateOfReminder >= CURDATE()";
}
else if ($filter == 2)
{
     $conds .= " AND a.DateOfReminder < CURDATE()";
}

$sql = "SELECT b.ClassName, b.ClassNumber, a.DateOfReminder, c.UserLogin, a.Reason
        FROM CARD_STUDENT_REMINDER as a
             LEFT OUTER JOIN INTRANET_USER as b
                  ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus = 1
             LEFT OUTER JOIN INTRANET_USER as c
                  ON a.TeacherID = c.UserID AND c.RecordType = 1 AND c.RecordStatus = 1
         WHERE
              b.UserID IS NOT NULL AND c.UserID IS NOT NULL AND
              (b.ChineseName like '%$keyword%'
               OR b.EnglishName like '%$keyword%'
               )
               $conds
                  ";
$result = $li->returnArray($sql,5);
$filecontent = "ClassName,ClassNumber,Date,Teacher,Reason\n";
for ($i=0; $i<sizeof($result); $i++)
{
     list($class,$classnum,$remindDate,$login,$reason) = $result[$i];
     $filecontent .= "\"$class\",\"$classnum\",\"$remindDate\",\"$login\",\"$reason\"\n";
}

// Output the file to user browser
$filename = "reminders.csv";

header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($filecontent));
header("Content-Disposition: attachment; filename=\"".$filename."\"");
echo $filecontent;

intranet_closedb();

?>