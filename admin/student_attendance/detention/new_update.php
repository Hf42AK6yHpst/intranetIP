<?
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();
$Location = intranet_htmlspecialchars($Location);
$Remark = intranet_htmlspecialchars($Remark);
$Reason = intranet_htmlspecialchars($Reason);

$arrivalTimeStr = ($ArrivalTime ==""? "NULL":"'$ArrivalTime'");
$departureTimeStr = ($DepartureTime ==""? "NULL":"'$DepartureTime'");

$sql = "INSERT INTO CARD_STUDENT_DETENTION (StudentID, RecordDate,ArrivalTime,DepartureTime,Location,Reason,Remark,DateInput,DateModified)
        VALUES ('$StudentID','$RecordDate',$arrivalTimeStr,$departureTimeStr,'$Location','$Reason','$Remark',now(),now())";
$li->db_db_query($sql);
header ("Location: index.php?msg=1");
intranet_closedb();
?>