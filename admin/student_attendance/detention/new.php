<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();

echo displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Detention,'index.php',$button_new,'');
echo displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg);

if (!isset($ClassName) || $ClassName == "")
{
        $select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.submit()");
        $x = "<form name=form1 action='' method=GET>\n";
        $x .= "<table width=90% border=0 align=center><tr><td>\n";
        $x .= "$i_UserParentLink_SelectClass: $select_class";
        $x .= "</td></tr></table></form>\n";
        echo $x;
}
else
{
        $select_class = $lclass->getSelectClass("name=ClassName onChange=\"this.form.action='';this.form.submit()\"",$ClassName);
        $select_student = $lclass->getStudentSelectByClass($ClassName,"name=StudentID");

        $currentDate = date('Y-m-d');
        $currentTime = date('H:i:s');

        #$fromWhereTemplate = getSelectByValue($template_fromWhere,"onChange=\"this.form.FromWhere.value=this.value\"");
        #$locationTemplate = getSelectByValue($template_location,"onChange=\"this.form.Location.value=this.value\"");
        #$objectiveTemplate = getSelectByValue($template_objective,"onChange=\"this.form.Objective.value=this.value\"");

        $lcard = new libcardstudentattend();
        $locationTemplate = getSelectByValue($lcard->getWordList(4),"onChange=\"this.form.Location.value=this.value\"");
        $reasonTemplate = getSelectByValue($lcard->getWordList(5),"onChange=\"this.form.Reason.value=this.value\"");
?>
<form name=form1 action="new_update.php" method=POST>
<table width=90% border=0 align=center>
<tr><td><?=$i_SmartCard_ClassName?></td><td><?=$select_class?></td></tr>
<tr><td><?=$i_UserStudentName?></td><td><?=$select_student?></td></tr>
<tr><td><?=$i_SmartCard_DetentionDate?></td><td><input type=text name=RecordDate size=10 maxlength=10 value="<?=$currentDate?>">(YYYY-MM-DD)</td></tr>
<tr><td><?=$i_SmartCard_DetentionArrivalTime?></td><td><input type=text name=ArrivalTime size=10 maxlength=10 value="<?=$currentTime?>">(HH:mm:ss 24-hr)</td></tr>
<tr><td><?=$i_SmartCard_DetentionDepartureTime?></td><td><input type=text name=DepartureTime size=10 maxlength=10>(HH:mm:ss 24-hr)</td></tr>
<tr><td><?=$i_SmartCard_DetentionLocation?></td><td><input type=text name=Location size=50><?=$locationTemplate?></td></tr>
<tr><td><?=$i_SmartCard_DetentionReason?></td><td><input type=text name=Reason size=50><?=$reasonTemplate?></td></tr>
<tr><td><?=$i_SmartCard_Remark?></td><td><TEXTAREA rows=5 cols=50 name=Remark></TEXTAREA></td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");
}
intranet_closedb();
?>
