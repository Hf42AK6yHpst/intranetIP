<?
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();
$li = new libdb();

/*
query string:
$sitename - location
$datatype - reserved
$In - in/out (1/0)
$dlang - en/b5
$cardid - smart card ID
*/

$sitename = intranet_htmlspecialchars($sitename);
$datatype = intranet_htmlspecialchars($datatype);
/*
$sql = "INSERT INTO CARD_STUDENT_LOG (CardID,SiteName,RecordedTime,Type)
        VALUES ('$cardid','$sitename',now(),'$datatype')";
$li = new libdb();
$li->db_db_query($sql);
$record = $li->db_insert_id();

$sql = "SELECT DATE_FORMAT(RecordedTime,'%H:%i:%s') FROM CARD_STUDENT_LOG WHERE LogID = $record";
$loggedTime = $li->returnVector($sql);
*/

$sql = "SELECT UserID, CardID,Name,IntranetLogin,ClassName,ClassNumber FROM CARD_STUDENT_USER WHERE CardID = '$cardid'";
#echo "1. $sql\n";
$user = $li->returnArray($sql,6);
#print_r($user);
list($StudentID,$card,$name,$login,$ClassName,$ClassNumber) = $user[0];

if ($StudentID!= '' && $In==1)
{
    # Check record exist
    $sql = "SELECT DetentionID FROM CARD_STUDENT_DETENTION WHERE StudentID = $StudentID AND RecordDate = CURDATE()";
#echo "1. $sql\n";
    $temp = $li->returnVector($sql);
    #echo " ---- ";
    if (sizeof($temp)==0)     # No existing record
    {
        $sql = "INSERT INTO CARD_STUDENT_DETENTION (StudentID, RecordDate,ArrivalTime,Location,DateInput,DateModified)
                VALUES ($StudentID,CURDATE(),now(),'$sitename',now(),now())";
        $li->db_db_query($sql);
        $DetentionID = $li->db_insert_id();
        $flag = 1;
    }
    else
    {
        $DetentionID = $temp[0];
        $sql = "UPDATE CARD_STUDENT_DETENTION SET ArrivalTime = now(), Location = '$sitename' WHERE DetentionID = $DetentionID AND ArrivalTime IS NULL OR ArrivalTime > now()";
        $li->db_db_query($sql);
        $rowsAffected = $li->db_affected_rows();
        $flag = ($rowsAffected==0? 3:2);
    }
}
else if($StudentID!= '')# out record
{
    # Check record exist
    $sql = "SELECT DetentionID FROM CARD_STUDENT_DETENTION WHERE StudentID = $StudentID AND RecordDate = CURDATE() AND ArrivalTime IS NOT NULL";
    $temp = $li->returnVector($sql);
    if (sizeof($temp)==0)     # No record, error
    {
        $flag = 4;
    }
    else       # Update Record
    {
        $DetentionID = $temp[0];
        $sql = "UPDATE CARD_STUDENT_DETENTION SET DepartureTime = now() WHERE DetentionID = $DetentionID AND ArrivalTime IS NOT NULL";
        $li->db_db_query($sql);
        $rowsAffected = $li->db_affected_rows();
        $flag = ($rowsAffected==0? 6:5);
    }
}

/*
$sql = "SELECT UserID, CardID,Name,IntranetLogin,ClassName,ClassNumber FROM CARD_STUDENT_USER WHERE CardID = '$cardid'";
echo "1. $sql\n";
$user = $li->returnArray($sql,6);
list($id,$card,$name,$login,$ClassName,$ClassNumber) = $user[0];
*/

# Response string
# 1 - create new in record
# 2 - updated in record
# 3 - already in record
# 4 - no existing, out error
# 5 - update out record successful
# 6 - update out record failed (No arrival time recorded)

if ($dlang == "b5")
{
    $fail = "智能咭資料有誤, 請重試或聯絡系統管理員.";
    $i_arrived = " 已到達. 時間為 ";
    $i_arrival_error = " 已經紀錄";
    $i_departure_error = " 並沒有留堂紀錄, 請檢查再試.";
    $i_departure = " 已離開, 時間為 ";
    $i_departure_error_arrival = " 並沒有到達紀錄, 請檢查再試.";
}
else
{
    $fail = "Card Error. Please retry or contact System Administrator.";
    $i_arrived = " arrived at ";
    $i_arrival_error = " already recorded.";
    $i_departure_error = " does not have detention record today. Please check and try again.";
    $i_departure = " leaved at ";
    $i_departure_error_arrival = " does not have arrival record. Please check and try again.";
}
if ($StudentID!="")
{
    if ($ClassName != "" || $ClassNumber != "")
    {
        $class_str = " ($ClassName - $ClassNumber)";
    }
    else $class_str = "";

    if ($DetentionID != "")
    {
        $sql = "SELECT ArrivalTime, DepartureTime FROM CARD_STUDENT_DETENTION WHERE DetentionID = $DetentionID";
        $loggedTime = $li->returnArray($sql,2);

    }

    $nameStr = "$name$class_str";
    $arrivedTime = $loggedTime[0][0];
    $departureTime = $loggedTime[0][1];
    $response = $nameStr;

    switch ($flag)
    {
            case 1:
            case 2:
                 $response .= " $i_arrived ".$arrivedTime;
                 break;
            case 3:
                 $response .= " $i_arrival_error";
                 break;
            case 4:
                 $response .= " $i_departure_error";
                 break;
            case 5:
                 $response .= " $i_departure $departureTime";
                 break;
            case 6:
                 $response .= " $i_departure_error_arrival";
                 break;
    }
    echo $response;

}
else
{
    echo "$fail";
}




intranet_closedb();
#header("Location: index.php");
?>