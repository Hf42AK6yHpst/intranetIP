<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();
$li = new libdb();
$sql = "DROP TABLE TEMP_CARD_STUDENT_LOG";
$li->db_db_query($sql);

?>

<form name="form1" method="POST" action="import_update.php" enctype="multipart/form-data">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_CardLog,'index.php',$button_import,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_select_file; ?>:</td><td><input type=file size=50 name=userfile></td></tr>
<tr><td align=right nowrap><?=$i_general_Format?>:</td><td>
<input type=radio name=format value=1 CHECKED><?=$i_StudentAttendance_ImportFormat_CardID?> <a class=functionlink href=format1.csv target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>  <br>
<input type=radio name=format value=2><?=$i_StudentAttendance_ImportFormat_ClassNumber?> <a class=functionlink href=format2.csv target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>        <br>
<input type=radio name=format value=3><?=$i_StudentAttendance_ImportFormat_From_OfflineReader?><br>
<br><?=$i_StudentAttendance_ImportTimeFormat?>
</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
</form>

<?
include_once("../../../templates/adminfooter.php");
?>
