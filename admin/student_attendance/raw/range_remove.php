<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
intranet_opendb();

$li = new libdb();
$is_export = false;

$del_conds = "WHERE UNIX_TIMESTAMP(RecordedTime) BETWEEN UNIX_TIMESTAMP('$RemoveFrom') AND UNIX_TIMESTAMP('$RemoveTo 23:59:59')";
$get_conds = " AND UNIX_TIMESTAMP(a.RecordedTime) BETWEEN UNIX_TIMESTAMP('$RemoveFrom') AND UNIX_TIMESTAMP('$RemoveTo 23:59:59')";
if ($action_type==1 || $action_type==3)
{
    $sql = "SELECT DATE_FORMAT(a.RecordedTime,'%Y-%m-%d %H:%i:%s'), a.SiteName,
                   b.ClassName, b.ClassNumber
                  FROM CARD_STUDENT_LOG as a
                       LEFT OUTER JOIN INTRANET_USER as b ON a.CardID = b.CardID AND b.RecordType = 2
                  WHERE b.UserID IS NOT NULL AND b.ClassName != ''
                  AND b.ClassNumber != '' $get_conds ORDER BY a.RecordedTime";
    $data = $li->returnArray($sql,4);
    $is_export = true;
}
if ($action_type==2 || $action_type==3)
{
    $sql = "DELETE FROM CARD_STUDENT_LOG $del_conds";
    $li->db_db_query($sql);
}

$content = "\"Time\",\"Site\",\"Class Name\",\"Class Number\"\n";
if ($is_export)
{
    for ($i=0; $i<sizeof($data); $i++)
    {
         list($time, $site, $class, $classnum) = $data[$i];
         $content .= "\"$time\",\"$site\",\"$class\",\"$classnum\"\n";
    }
    $filename = $RemoveFrom."to".$RemoveTo."_classnum.csv";
    output2browser($content,$filename);
}
else
{
    header("Location: index.php?msg=3");
}

intranet_closedb();
?>