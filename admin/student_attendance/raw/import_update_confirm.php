<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
intranet_opendb();


$li = new libdb();

$sql = "INSERT INTO CARD_STUDENT_LOG (CardID,SiteName,RecordedTime)
        SELECT IF(a.CardID IS NOT NULL, a.CardID, b.CardID),a.SiteName,a.RecordedTime
               FROM TEMP_CARD_STUDENT_LOG as a LEFT OUTER JOIN INTRANET_USER as b
                    ON b.RecordType = 2 AND
                    ((a.CardID IS NOT NULL AND a.CardID = b.CardID)
                      OR (a.CardID IS NULL AND a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber)
					  OR (a.UserID IS NOT NULL AND a.UserID = b.UserID)
                    )";
$li->db_db_query($sql);

$sql = "DROP TABLE TEMP_CARD_STUDENT_LOG";
$li->db_db_query($sql);
intranet_closedb();

header("Location: index.php?msg=1");
?>