<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.submit()",$ClassName);

?>

<form name="form1" method="POST" action="">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_CardLog,'index.php',$button_new,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_UserClassName; ?>:</td><td><?=$select_class?></td></tr>
<? if ($ClassName != "") {
$select_student = $lclass->getStudentSelectByClass($ClassName,"name=StudentID");
$lcard = new libcardstudentattend();
$select_site = getSelectByValue($lcard->getWordList(0),"onChange=\"this.form.site.value=this.value\"");
?>
<tr><td align=right nowrap><?php echo $i_UserStudentName; ?>:</td><td><?=$select_student?></td></tr>
<tr><td align=right nowrap><?=$i_SmartCard_DateRecorded?>:</td><td><input type=text name=date size=10 value='<?=date('Y-m-d')?>'>(YYYY-MM-DD)</td></tr>
<tr><td align=right nowrap><?=$i_SmartCard_TimeRecorded?>:</td><td><input type=text name=time size=10 value='<?=date('H:i:s')?>'>(HH:mm:ss - 24 hrs)</td></tr>
<tr><td align=right nowrap><?=$i_SmartCard_Site?>:</td><td><input type=text name=site size=30 ><?=$select_site?></td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" onClick="this.form.action='new_update.php'"></td></tr>
<? } ?>
</table>
</form>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>
