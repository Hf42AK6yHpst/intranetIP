<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$targetClass = $ClassName;
#$lclass = new libclass();
#$select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.submit()",$ClassName);

if ($slot != 2 && $slot != 3) $slot = 1;

if ($slot == 3)
{
    $sql_func = "MAX";
    $time_str = "$i_StudentAttendance_Time_Departure";
    $min_str = "$i_StudentAttendance_MinEarly";
    $RecordType = 2;               # Leave school
}
else
{
    $sql_func = "MIN";
    $time_str = "$i_StudentAttendance_Time_Arrival";
    $min_str = "$i_StudentAttendance_MinLate";
    $RecordType = 1;               # Go to school
}
$li = new libdb();

# Grab records

$sql = "CREATE TEMPORARY TABLE TEMP_STUDENT_REALTIME_RECORD (
 CardID varchar(100), RecordedTime datetime
)";
$li->db_db_query($sql);

# Grab records to temp table
$target = $date;
$targetStart = "$target $start";
$targetEnd = "$target $end";
$targetBound = "$target $bound";
$boundtime = strtotime($targetBound);

$sql = "INSERT INTO TEMP_STUDENT_REALTIME_RECORD
               (CardID,RecordedTime)
        SELECT CardID, $sql_func(RecordedTime) FROM CARD_STUDENT_LOG
               WHERE RecordedTime >= '$targetStart' AND RecordedTime <= '$targetEnd'
               GROUP BY CardID";

$li->db_db_query($sql);
$namefield = getNameFieldByLang("a.");
if ($targetClass != "")
{
    $classConds = " AND a.ClassName = '$targetClass'";
}
$sql = "SELECT a.UserID, $namefield, a.CardID,a.ClassName,a.ClassNumber,
        IF(b.CardID IS NULL,'',UNIX_TIMESTAMP(b.RecordedTime)),
        c.SiteName
        FROM INTRANET_USER as a LEFT OUTER JOIN TEMP_STUDENT_REALTIME_RECORD as b ON a.CardID = b.CardID
             LEFT OUTER JOIN CARD_STUDENT_LOG as c ON a.CardID = c.CardID AND b.RecordedTime = c.RecordedTime
             WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) $classConds 
             ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";

$result = $li->returnArray($sql,7);

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";
$functionbar = "<a href=javascript:submit2update()><img src=\"$image_path/admin/button/t_btn_archive_$intranet_session_language.gif\" border=0 alt='$button_archive1'></a>";
if ($targetClass!="")
{
    $functionbar .= "<a href=special_archive.php?targetClass=$targetClass&slot=$slot&date=$date&start=$start&end=$end&bound=$bound><img src=\"$image_path/admin/button/t_btn_special_archive_$intranet_session_language.gif\" border=0 alt='$i_StudentAttendance_Action_SpecialArchive'></a>";
}
$display = "<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>\n";
$display .= "<tr><td colspan=2><img src='$image_path/admin/table_head0.gif' width='560' height='13' border='0'></td></tr>";
$display .= "<tr><td class=admin_bg_menu>$toolbar</td><td class=admin_bg_menu align=right>$functionbar</td></tr>";
$display .= "<tr><td colspan=2>";


$display .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0><tr class='tableTitle'><td>$i_UserStudentName</td><td>$time_str</td><td>$i_StudentAttendance_Status</td><td>$min_str</td><td>$i_SmartCard_Site</td></tr>\n";

for ($i=0; $i<sizeof($result); $i++)
{
        list ($uid,$name,$card,$ClassName,$ClassNumber,$ts,$site) = $result[$i];
        $timeStr = date("H:i:s",$ts);

        if ($ts == "")
        {
                $status = $i_StudentAttendance_Status_Absent;
                $minLate = "--";
                $background = "#D9F0C2";
                $textcolor = "#3E7608";
                $timeStr = "--";
        }
        else if ($RecordType == 1 && $ts > $boundtime)
        {
                $status = $i_StudentAttendance_Status_Late;
                $minLate = ceil(($ts - $boundtime)/60);
                $background = "#FDC2B4";
                $textcolor = "#C62E2E";
     }
     else if ($RecordType == 2 && $ts < $boundtime)
     {
                $status = $i_StudentAttendance_Status_EarlyLeave;
                $minLate = ceil(($boundtime - $ts)/60);
                $background = "#B4D5FD";
                $textcolor = "#036AD3";
     }
     else
     {
                if ($noOnTime) continue;
                $status = $i_StudentAttendance_Status_OnTime;
                $minLate = "--";
                $background = "#FFFFFF";
                $textcolor = "#000000";
     }
     $class_str = ($ClassName != "" && $ClassNumber != "")? "($ClassName - $ClassNumber)":"";
     $display .= "<tr bgcolor='$background'><td><font color='$textcolor'>$name $class_str</font></td><td><font color='$textcolor'>$timeStr</font></td><td><font color='$textcolor'>$status</font></td><td><font color='$textcolor'>$minLate</font></td><td><font color='$textcolor'>$site&nbsp;</font></td></tr>\n";
}
$display .= "</table>\n";
$display .= "</td></tr></table>\n";
$display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>";
$display .= "<tr><td><img src='/images/admin/table_bottom.gif' width='560' height='16' border='0'></td></tr>";
$display .= "</table><br>\n";

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_ViewTodayRecord,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<form name=form1 action=archive_update.php method=POST>
<?=$display?>
<input type=hidden name=slot value="<?=$slot?>">
<input type=hidden name=date value="<?=$date?>">
<input type=hidden name=start value="<?=$start?>">
<input type=hidden name=end value="<?=$end?>">
<input type=hidden name=bound value="<?=$bound?>">
<input type=hidden name=targetClass value="<?=$targetClass?>">

</form>

<script language="JavaScript" type="text/javascript">
<!--
function openPrintPage()
{
             newWindow("viewlate_updateprint.php?<?php echo $HTTP_SERVER_VARS['QUERY_STRING'] ?>",4);
}
function submit2update(){
        document.form1.action='archive_update.php';
        parent.intranet_admin_menu.runStatusWin("/admin/pop_processing.php");
        document.form1.submit();
        return;
}
-->
</script>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>
