<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libcycle.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();


$lc = new libcycle();
$cycle_days = $lc->CycleNum;

$select_type = "<SELECT name=DayValue>\n";
for ($i=0; $i<sizeof($i_DayType0); $i++)
{
     $select_type .= "<OPTION value='$i' ".($i==6?"SELECTED":"").">".$i_DayType0[$i]."</OPTION>\n";
}
$cycle_name_array = ${"i_DayType".$lc->CycleType};
for ($i=0; $i<$lc->CycleNum; $i++)
{
     $select_type .= "<OPTION value='".($i+7)."'>".$cycle_name_array[$i]."</OPTION>\n";
}
$select_type .= "</SELECT>\n";

$slotname = "";
switch ($SlotID)
{
        case 1: $slotname = $i_StudentAttendance_Slot_AM; break;
        case 2: $slotname = $i_StudentAttendance_Slot_PM; break;
        case 3: $slotname = $i_StudentAttendance_Slot_AfterSchool; break;
}
$setting_file = "$intranet_root/file/attend_st_settings.txt";
$file_content = get_file_content($setting_file);
$lines = explode("\n",$file_content);
for ($i=0; $i<sizeof($lines); $i++)
{
     $settings[$i] = explode(",",$lines[$i]);
}
$SlotStart = $settings[$SlotID-1][1];
$SlotEnd = $settings[$SlotID-1][2];
$SlotBound = $settings[$SlotID-1][3];

?>
<form name="form1" method="POST" action="special_new_update.php">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_TimeSlotSettings,'index.php',$i_StudentAttendance_Slot_Special." ($slotname)",'javascript:history.back()',$button_new,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_StudentAttendance_Slot_Special; ?>:</td><td><?=$select_type?></td></tr>
<tr><td align=right nowrap><?=$i_StudentAttendance_Slot_Start?>:</td><td><input type=text name=SlotStart size=10 value='<?=$SlotStart?>'>(HH:mm:ss - 24 hrs)</td></tr>
<tr><td align=right nowrap><?=$i_StudentAttendance_Slot_End?>:</td><td><input type=text name=SlotEnd size=10 value='<?=$SlotEnd?>'>(HH:mm:ss - 24 hrs)</td></tr>
<tr><td align=right nowrap><?=$i_StudentAttendance_Slot_Boundary?>:</td><td><input type=text name=SlotBound size=10 value='<?=$SlotBound?>'>(HH:mm:ss - 24 hrs)</td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=SlotID value="<?=$SlotID?>">

</form>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>
