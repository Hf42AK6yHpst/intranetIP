<?php
if ($SlotID != 1 && $SlotID != 2 && $SlotID != 3)
{
    header("Location: index.php");
    exit();
}if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libcycle.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

$lc = new libcycle();
$cycle_days = $lc->CycleNum;

$type_field = "
CASE DayValue
";
for ($i=0; $i<sizeof($i_DayType0); $i++)
{
     $type_field .= " WHEN $i THEN '".$i_DayType0[$i]."'";
}
$cycle_name_array = ${"i_DayType".$lc->CycleType};
for ($i=0; $i<$lc->CycleNum; $i++)
{
     $type_field .= " WHEN ".($i+7) ." THEN '".$cycle_name_array[$i]."'";
}
$type_field .= " END";

$sql  = "SELECT
               $type_field,
               SlotStart, SlotEnd, SlotBoundary,
               CONCAT('<input type=checkbox name=SpecialSlotID[] value=', SpecialSlotID ,'>')
         FROM
             CARD_STUDENT_SLOT_SPECIAL
         WHERE
              SlotID = $SlotID
                ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("DayValue","SlotStart","SlotEnd","SlotBoundary");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $i_StudentAttendance_Slot_Special)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $i_StudentAttendance_Slot_Start)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $i_StudentAttendance_Slot_End)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $i_StudentAttendance_Slot_Boundary)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("SpecialSlotID[]")."</td>\n";
$toolbar = "<a class=iconLink href=javascript:checkNew('special_new.php?SlotID=$SlotID')>".newIcon()."$button_new</a>";
$functionbar = "<a href=\"javascript:checkEdit(document.form1,'SpecialSlotID[]','special_edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'SpecialSlotID[]','special_remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";

$slotname = "";
switch ($SlotID)
{
        case 1: $slotname = $i_StudentAttendance_Slot_AM; break;
        case 2: $slotname = $i_StudentAttendance_Slot_PM; break;
        case 3: $slotname = $i_StudentAttendance_Slot_AfterSchool; break;
}

?>
<form name="form1" method="get">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_TimeSlotSettings,'index.php',$i_StudentAttendance_Slot_Special." ($slotname)",'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=SlotID value="<?=$SlotID?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

</form>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>
