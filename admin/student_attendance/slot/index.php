<?php

include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$setting_file = "$intranet_root/file/attend_st_settings.txt";
if (!is_file($setting_file))
{
     $default_content = "1,06:00,09:00,07:40\n1,12:30,14:30,13:30\n1,15:00,18:00,16:00";
     write_file_content($default_content,$setting_file);
}
$file_content = get_file_content($setting_file);
$lines = explode("\n",$file_content);
for ($i=0; $i<sizeof($lines); $i++)
{
     $settings[$i] = explode(",",$lines[$i]);
}

?>

<form name="form1" method="POST" ACTION="slot_update.php">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_TimeSlotSettings,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu></td></tr>
  <tr>
    <td>
      <table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>
        <tr>
          <td><?=$i_StudentAttendance_Slot?></td>
          <td><?=$i_StudentAttendance_Slot_InUse?></td>
          <td><?=$i_StudentAttendance_Type?></td>
          <td><?=$i_StudentAttendance_Slot_Start?></td>
          <td><?=$i_StudentAttendance_Slot_End?></td>
          <td><?=$i_StudentAttendance_Slot_Boundary?></td>
          <td><?=$i_StudentAttendance_Slot_Special?></td>
        </tr>
        <tr>
          <td><?=$i_StudentAttendance_Slot_AM?></td>
          <td><input type=checkbox name=slot1_inuse value=1 <?=($settings[0][0]==1?"CHECKED":"")?>></td>
          <td><?=$i_StudentAttendance_Type_GoToSchool?></td>
          <td><input type=text size=5 maxlength=5 name=slot1_start value="<?=$settings[0][1]?>"></td>
          <td><input type=text size=5 maxlength=5 name=slot1_end value="<?=$settings[0][2]?>"></td>
          <td><input type=text size=5 maxlength=5 name=slot1_boundary value="<?=$settings[0][3]?>"></td>
          <td><a class=functionlink_new href=special.php?SlotID=1><?=$button_edit?></a></td>
        </tr>
        <tr>
          <td><?=$i_StudentAttendance_Slot_PM?></td>
          <td><input type=checkbox name=slot2_inuse value=1 <?=($settings[1][0]==1?"CHECKED":"")?>></td>
          <td><?=$i_StudentAttendance_Type_GoToSchool?></td>
          <td><input type=text size=5 maxlength=5 name=slot2_start value="<?=$settings[1][1]?>"></td>
          <td><input type=text size=5 maxlength=5 name=slot2_end value="<?=$settings[1][2]?>"></td>
          <td><input type=text size=5 maxlength=5 name=slot2_boundary value="<?=$settings[1][3]?>"></td>
          <td><a class=functionlink_new href=special.php?SlotID=1><?=$button_edit?></a></td>
        </tr>
        <tr>
          <td><?=$i_StudentAttendance_Slot_AfterSchool?></td>
          <td><input type=checkbox name=slot3_inuse value=1 <?=($settings[2][0]==1?"CHECKED":"")?>></td>
          <td><?=$i_StudentAttendance_Type_LeaveSchool?></td>
          <td><input type=text size=5 maxlength=5 name=slot3_start value="<?=$settings[2][1]?>"></td>
          <td><input type=text size=5 maxlength=5 name=slot3_end value="<?=$settings[2][2]?>"></td>
          <td><input type=text size=5 maxlength=5 name=slot3_boundary value="<?=$settings[2][3]?>"></td>
          <td><a class=functionlink_new href=special.php?SlotID=1><?=$button_edit?></a></td>
        </tr>
        <tr>
          <td colspan=6 align=right><?=btnSubmit()?><?=btnReset()?></td>
        </tr>
      </table>


</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>
<p><?=$i_StudentAttendance_Slot_SettingsDescription?></p>
</form>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>