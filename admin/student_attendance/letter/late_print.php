<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libclass.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();
$targetClass = $ClassName;

$page_breaker = "<P CLASS='breakhere'>";

$i_title = "$i_StudentAttendance_System ($i_StudentAttendance_ViewTodayRecord)";
include_once("../../../templates/fileheader.php");
?>
<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE>

<?

if ($slot != 2 && $slot != 3) $slot = 1;

if ($slot == 3)
{
    $sql_func = "MAX";
    $time_str = "$i_StudentAttendance_Time_Departure";
    $min_str = "$i_StudentAttendance_MinEarly";
    $RecordType = 2;               # Leave school
}
else
{
    $sql_func = "MIN";
    $time_str = "$i_StudentAttendance_Time_Arrival";
    $min_str = "$i_StudentAttendance_MinLate";
    $RecordType = 1;               # Go to school
}
$li = new libdb();

# Grab records

$sql = "CREATE TEMPORARY TABLE TEMP_STUDENT_REALTIME_RECORD (
 CardID varchar(100), RecordedTime datetime
)";
$li->db_db_query($sql);

# Grab records to temp table
$target = $date;
$targetStart = "$target $start";
$targetEnd = "$target $end";
$targetBound = "$target $bound";
$boundtime = strtotime($targetBound);

$sql = "INSERT INTO TEMP_STUDENT_REALTIME_RECORD
               (CardID,RecordedTime)
        SELECT CardID, $sql_func(RecordedTime) FROM CARD_STUDENT_LOG
               WHERE RecordedTime >= '$targetStart' AND RecordedTime <= '$targetEnd'
               GROUP BY CardID";

$li->db_db_query($sql);
$namefield = getNameFieldByLang("a.");
if ($targetClass != "")
{
    $classConds = " AND a.ClassName = '$targetClass'";
}
$sql = "SELECT a.UserID, $namefield, a.CardID,a.ClassName,a.ClassNumber,
        IF(b.CardID IS NULL,'',UNIX_TIMESTAMP(b.RecordedTime)),
        c.SiteName
        FROM INTRANET_USER as a LEFT OUTER JOIN TEMP_STUDENT_REALTIME_RECORD as b ON a.CardID = b.CardID
             LEFT OUTER JOIN CARD_STUDENT_LOG as c ON a.CardID = c.CardID AND b.RecordedTime = c.RecordedTime
             WHERE a.RecordType = 2 $classConds
             ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
$result = $li->returnArray($sql,7);

$pageheader = "";
$pageheader = "<table width='560' border='0'>";
$pageheader .= "<tr><td colspan='2'><u>$i_StudentAttendance_System ($i_StudentAttendance_ViewTodayRecord)</u></td></tr>";
if ($targetClass != "")
{
    $pageheader .= "<tr><td>$i_UserClassName</td><td>$targetClass</td></tr>";
}
$pageheader .= "<tr><td>$i_StudentAttendance_Slot</td><td>";
switch ($slot)
{
        case 1: $pageheader .= "$i_StudentAttendance_Slot_AM"; break;
        case 2: $pageheader .= "$i_StudentAttendance_Slot_PM"; break;
        case 3: $pageheader .= "$i_StudentAttendance_Slot_AfterSchool"; break;
}
$pageheader .= "</tr>";
$pageheader .= "<tr><td>$i_StudentAttendance_View_Date</td><td>$date</td></tr>";
$pageheader .= "<tr><td>$i_StudentAttendance_Slot_Start</td><td>$start</td></tr>";
$pageheader .= "<tr><td>$i_StudentAttendance_Slot_End</td><td>$end</td></tr>";
$pageheader .= "<tr><td>";
$pageheader .= ($slot==3)? "$i_StudentAttendance_LeaveSchoolTime":"$i_StudentAttendance_ToSchoolTime";
$pageheader .= "</td><td>$bound</td></tr>";
$pageheader .= "</table>";


$pageheader .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
$pageheader .= "<tr class='tableTitle'><td>$i_UserStudentName</td><td>$time_str</td><td>$i_StudentAttendance_Status</td><td>$min_str</td><td>$i_SmartCard_Site</td></tr>\n";

$pagefooter = "</table>";

$display .= $pageheader;
$prevClass = $targetClass;
for ($i=0; $i<sizeof($result); $i++)
{
        list ($uid,$name,$card,$ClassName,$ClassNumber,$ts,$site) = $result[$i];
        if ($i!=0 && $prevClass != $ClassName)
        {
            $display .= "$pagefooter\n$page_breaker\n$pageheader";
            $prevClass = $ClassName;
        }

        $timeStr = date("H:i:s",$ts);

        if ($ts == "")
        {
                $status = $i_StudentAttendance_Status_Absent;
                $minLate = "--";
                $background = "#D9F0C2";
                $textcolor = "#3E7608";
                $timeStr = "--";
        }
        else if ($RecordType == 1 && $ts > $boundtime)
        {
                $status = $i_StudentAttendance_Status_Late;
                $minLate = ceil(($ts - $boundtime)/60);
                $background = "#FDC2B4";
                $textcolor = "#C62E2E";
     }
     else if ($RecordType == 2 && $ts < $boundtime)
     {
                $status = $i_StudentAttendance_Status_EarlyLeave;
                $minLate = ceil(($boundtime - $ts)/60);
                $background = "#B4D5FD";
                $textcolor = "#036AD3";
     }
     else
     {
                if ($noOnTime) continue;

                $status = $i_StudentAttendance_Status_OnTime;
                $minLate = "--";
                $background = "#FFFFFF";
                $textcolor = "#000000";
     }
     $class_str = ($ClassName != "" && $ClassNumber != "")? "($ClassName - $ClassNumber)":"";
     $display .= "<tr bgcolor='$background'><td><font color='$textcolor'>$name $class_str</font></td><td><font color='$textcolor'>$timeStr</font></td><td><font color='$textcolor'>$status</font></td><td><font color='$textcolor'>$minLate</font></td><td><font color='$textcolor'>$site&nbsp;</font></td></tr>\n";
}
$display .= "</table>\n";

//$display .= "<br><table width='560' border='0'><tr><td align='center'><a href='javascript:window.print()'><img src='$image_path/admin/button/s_btn_print_$intranet_session_language.gif' border='0'></a></td></tr></table>";

echo $display;

intranet_closedb();
?>
