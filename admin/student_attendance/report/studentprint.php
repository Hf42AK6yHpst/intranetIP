<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$li = new libdb();
$ls = new libuser($StudentID);

$setting_file = "$intranet_root/file/attend_st_settings.txt";
if (!is_file($setting_file))
{
     $default_content = "1,06:00,09:00,07:40\n1,12:30,14:30,13:30\n1,15:00,18:00,16:00";
     write_file_content($default_content,$setting_file);
}
$file_content = get_file_content($setting_file);
$lines = explode("\n",$file_content);
for ($i=0; $i<sizeof($lines); $i++)
{
     $settings[$i] = explode(",",$lines[$i]);
}

# Preset Date Range to current month
if (!isset($year) || $year == "")
{
     $year = date('Y');
}
if (!isset($month) || $month == "")
{
     $month = date('m');
}

$i_title = $i_StudentAttendance_System.$i_StudentAttendance_Report.'('.$i_StudentAttendance_Report_Student.')';
include_once("../../../templates/fileheader.php");
?>
<table width="560" border="0">
	<tr><td colspan='2'><u><?php echo $i_StudentAttendance_System.$i_StudentAttendance_Report.'('.$i_StudentAttendance_Report_Student.')'?></u></td></tr>
	<tr><td><?php echo $i_SmartCard_ClassName?></td><td><?=$ClassName?></td></tr>
	<tr><td><?php echo $i_UserStudentName?></td><td><?= ($intranet_session_language=="b5")?$ls->ChineseName:$ls->EnglishName ?></td></tr>
	<tr><td><?php echo $i_SmartCard_ReportMonth ?></td><td><?= "$month / $year ($i_general_Month / $i_general_Year)" ?></td></tr>
	<tr><td><?php echo $i_StudentAttendance_Report_SelectSlot?></td>
		<td><? for ($i=0; $i<sizeof($slot); $i++) {
				switch ($slot[$i]) {
					case 1:	echo $i_StudentAttendance_Slot_AM.' '; break;
					case 2:	echo $i_StudentAttendance_Slot_PM.' '; break;
					case 3:	echo $i_StudentAttendance_Slot_AfterSchool.' '; break;
				}
			   }
			?>
		</td>
	</tr>
</table>
<br>
<?
	$i=0;
	$lcard = new libcardstudentattend();
	$results = $lcard->retrieveStudentMonthlyRecord($StudentID, $year, $month, $slot, $order);
	$x = '';

	if (sizeof($results) > 0) {
		foreach ($results AS $key => $result)
		{
			$css = ($i++%2==0? "":"2");
			$x .= "<tr class=tableContent$css><td>$key</td>";

			for ($j=0; $j<sizeof($slot); $j++) {
				if ($result[$slot[$j]][1] === 0)
				{
					$background = "#FFFFFF";
					$textcolor = "#000000";
					$str = $result[$slot[$j]][0];
				}
				elseif ($result[$slot[$j]][1] === 1)
				{
					$background = "#D9F0C2";
					$textcolor = "#3E7608";
					$str = $i_StudentAttendance_Status_Absent;
				}
				elseif ($result[$slot[$j]][1] === 2)
				{
					$background = "#FDC2B4";
					$textcolor = "#C62E2E";
					$str = $result[$slot[$j]][0]." ($i_StudentAttendance_MinLate: ".$result[$slot[$j]][2].")";
				}
				elseif ($result[$slot[$j]][1] === 3)
				{
					$background = "#B4D5FD";
					$textcolor = "#036AD3";
					$str = $result[$slot[$j]][0]." ($i_StudentAttendance_MinEarly: ".$result[$slot[$j]][2].")";
				}
				else
				{
					$background = "#E5D8CA";
					$textcolor = "#797979";
					$str = "$i_StudentAttendance_Report_NoRecord";
				}

				$x .= "<td bgcolor='$background'><font color='$textcolor'>$str</font></td>";
			}

			$x .= "</tr>";
		}
	} else {
		$x .= "<tr><td colspan=".(sizeof($slot)+1)." align=center>$i_no_record_exists_msg</td></tr>";
	}			

	$display = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
	$display .= "<tr class='tableTitle'><td>$i_SmartCard_DateRecorded</td>";
	for ($i=0; $i<sizeof($slot); $i++) {
		if ($slot[$i] == 1)
		{
			$display .= "<td>$i_StudentAttendance_Slot_AM</td>";
		}
		if ($slot[$i] == 2)
		{
			$display .= "<td>$i_StudentAttendance_Slot_PM</td>";
		}
		if ($slot[$i] == 3)
		{
			$display .= "<td>$i_StudentAttendance_Slot_AfterSchool</td>";
		}
	}
	$display .= "</tr>";

	$display .= $x;

	$display .= "</table>\n";

	//$display .= "<br><table width='560' border='0'><tr><td align='center'><a href='javascript:window.print()'><img src='$image_path/admin/button/s_btn_print_$intranet_session_language.gif' border='0'></a></td></tr></table>";


	echo $display;

	intranet_closedb();
	include_once("../../../templates/filefooter.php");
?>