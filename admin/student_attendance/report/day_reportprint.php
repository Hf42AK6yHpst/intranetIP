<?
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$i_title = $i_StudentAttendance_System.$i_StudentAttendance_Report.'('.$i_StudentAttendance_Report_Daily.')';
include_once("../../../templates/fileheader.php");

$lclass = new libclass();
if ($ClassID != "")
    $ClassName = $lclass->getClassName($ClassID);

$lcard = new libcardstudentattend();

$result = $lcard->retrieveDayClassRecord($ClassID,$targetDate,$slot);

$display = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
$display .= "<tr class='tableTitle'><td>$i_UserStudentName</td><td>$i_ClassNameNumber</td>";
for ($i=0; $i<sizeof($slot); $i++)
{
     if ($slot[$i] == 1)
     {
         $display .= "<td>$i_StudentAttendance_Slot_AM</td>";
     }
     if ($slot[$i] == 2)
     {
         $display .= "<td>$i_StudentAttendance_Slot_PM</td>";
     }
     if ($slot[$i] == 3)
     {
         $display .= "<td>$i_StudentAttendance_Slot_AfterSchool</td>";
     }
}

for ($i=0; $i<sizeof($result); $i++)
{
        $css = ($i%2==0? "":"2");

     list ($studentID, $studentName, $className, $classNumber, $time1, $type1, $min1, $time2, $type2, $min2, $time3, $type3, $min3) = $result[$i];
     $display .= "<tr class=tableContent$css><td>$studentName</td><td>($className - $classNumber)</td>";
     for ($j=1; $j<=sizeof($slot); $j++)
     {
          $time = ${"time$j"};
          $type = ${"type$j"};
          $min = ${"min$j"};

          if ($type === "No record")
          {
                                $background = "#E5D8CA";
                                $textcolor = "#797979";
                                $str = "$i_StudentAttendance_Report_NoRecord";
          }
          else if ($type === 0)
          {
                                $background = "#FFFFFF";
                                $textcolor = "#000000";
                                $str = "$time";
          }
          else if ($type === 1)
          {
                                $background = "#D9F0C2";
                                $textcolor = "#3E7608";
                                $str = "$i_StudentAttendance_Status_Absent";
          }
          else if ($type === 2)
          {
                                $background = "#FDC2B4";
                                $textcolor = "#C62E2E";
                                $str = "$time ($i_StudentAttendance_MinLate: $min)";
          }
          else if ($type === 3)
          {
                                $background = "#B4D5FD";
                                $textcolor = "#036AD3";
                                $str = "$time ($i_StudentAttendance_MinEarly: $min)";
          }
          else
          {
                                $background = "#E5D8CA";
                                $textcolor = "#797979";
                                $str = "$i_StudentAttendance_Report_NoRecord";
          }

          $display .= "<td bgcolor='$background'><font color='$textcolor'>$str</font></td>";
     }
     $display .= "</tr>\n";
}
$display .= "</table>\n";

//$display .= "<br><table width='560' border='0'><tr><td align='center'><a href='javascript:window.print()'><img src='$image_path/admin/button/s_btn_print_$intranet_session_language.gif' border='0'></a></td></tr></table>";
?>
<table width=560 border=0 cellpadding=2 cellspacing=1>
<tr><td colspan='2'><u><?= $i_StudentAttendance_System.$i_StudentAttendance_Report.'('.$i_StudentAttendance_Report_Daily.')'?></u></td></tr>
<? if ($ClassName != "") { ?>
<tr><td><?= $i_ClassName ?>:</td><td><?=$ClassName?></td></tr>
<? } ?>
<tr><td><?=$i_StudentAttendance_View_Date?>:</td><td><?php echo $targetDate ?></td></tr>
</table>

<?=$display?>

<? for ($i=0; $i<sizeof($slot); $i++) {
?>
<input type=hidden name=slot[] value="<?=$slot[$i]?>">
<? } ?>
</form>
<?
intranet_closedb();
?>
