<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$OutingID = (is_array($OutingID)? $OutingID[0]:$OutingID);

$li = new libdb();

$user_field = getNameFieldByLang("b.");
$sql = "SELECT a.UserID, a.RecordDate, IF(a.OutTime IS NULL,'',a.OutTime),
               IF(a.BackTime IS NULL,'',a.BackTime), a.Location,a.FromWhere, a.Objective,a.Detail,
               a.PIC,CONCAT($user_field,' (',b.ClassName,'-', b.ClassNumber,')')
        FROM CARD_STUDENT_OUTING as a
        LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
        WHERE a.OutingID = $OutingID";
$result = $li->returnArray($sql,10);
list($uid,$recordDate,$outTime,$backTime,$location,$fromWhere,$objective,$detail,$pic,$name) = $result[0];
        $lcard = new libcardstudentattend();

$fromWhereTemplate = getSelectByValue($lcard->getWordList(1),"onChange=\"this.form.FromWhere.value=this.value\"",$fromWhere);
$locationTemplate = getSelectByValue($lcard->getWordList(2),"onChange=\"this.form.Location.value=this.value\"",$location);
$objectiveTemplate = getSelectByValue($lcard->getWordList(3),"onChange=\"this.form.Objective.value=this.value\"",$objective);
        $namefield = getNameFieldForRecord();
        $sql = "SELECT $namefield FROM INTRANET_USER WHERE RecordType = 1 ORDER BY EnglishName";
        $staff = $lcard->returnVector($sql);

$picTemplate = getSelectByValue($staff,"onChange=\"addPIC(this.form,this.value)\"");

echo displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Outing,'index.php',$button_edit,'');
echo displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg);
?>
<script language="javascript">
function addPIC(obj, pic)
{
         if (obj.PIC.value != '')
         {
             obj.PIC.value += ', ';
         }
         obj.PIC.value += pic;
}
</SCRIPT>
<form name=form1 action="edit_update.php" method=POST>
<table width=90% border=0 align=center>
<tr><td><?=$i_UserStudentName?></td><td><?=$name?></td></tr>
<tr><td><?=$i_SmartCard_StudentOutingDate?></td><td><input type=text name=OutingDate size=10 maxlength=10 value="<?=$recordDate?>">(YYYY-MM-DD)</td></tr>
<tr><td><?=$i_SmartCard_StudentOutingOutTime?></td><td><input type=text name=OutTime size=10 maxlength=10 value="<?=$outTime?>">(HH:mm:ss 24-hr)</td></tr>
<tr><td><?=$i_SmartCard_StudentOutingBackTime?></td><td><input type=text name=BackTime size=10 maxlength=10 value="<?=$backTime?>">(HH:mm:ss 24-hr)</td></tr>
<tr><td><?=$i_SmartCard_StudentOutingPIC?></td><td><input type=text name=PIC size=50 value="<?=$pic?>"><?=$picTemplate?></td></tr>
<tr><td><?=$i_SmartCard_StudentOutingFromWhere?></td><td><input type=text name=FromWhere size=50 value="<?=$fromWhere?>"><?=$fromWhereTemplate?></td></tr>
<tr><td><?=$i_SmartCard_StudentOutingLocation?></td><td><input type=text name=Location size=50 value="<?=$location?>"><?=$locationTemplate?></td></tr>
<tr><td><?=$i_SmartCard_StudentOutingReason?></td><td><input type=text name=Objective size=50 value="<?=$objective?>"><?=$objectiveTemplate?></td></tr>
<tr><td><?=$i_SmartCard_Remark?></td><td><TEXTAREA rows=5 cols=50 name=Remark><?=$detail?></TEXTAREA></td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
<input type=hidden name=OutingID value="<?=$OutingID?>">
</form>
<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>
