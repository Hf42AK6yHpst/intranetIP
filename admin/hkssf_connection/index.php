<?php
// Editing by 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libgeneralsettings.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../includes/eclass_api/libeclass_api.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$libgeneralsettings = new libgeneralsettings();

$hkssf_settings = $libgeneralsettings->Get_General_Setting('HKSSF');
$avaliable = $hkssf_settings['avaliable'];
$access_token = $hkssf_settings['access_token'];
?>
<script src="../../templates/jquery/jquery-1.3.2.min.js" language="JavaScript"></script>
<script language="javascript">
function checkform(obj)
{
	obj.submit();
}
function generate()
{
	$.ajax({
        type: "POST",
        url: "update.php",
        data: $("#token").serialize(),
        success: function(msg)
        {
            if(msg != 0)
			{
            	$('#access_token').text(msg);
            }
            else
			{
            	$('#access_token').text('Failed to generate token, please try agian.');
			}
        }
      });
}
</script>

<form name="token" id="token">
	<input type="hidden" name="task" value="token_generate"> 
</form>

<form name="form1" id="form1" action="update.php" method="post" onsubmit="checkform(this);return false;">
<?= displayNavTitle($Lang['HKSSF']['module'], '') ?>
<?= displayTag("head_eclass_update_$intranet_session_language.gif", $msg) ?>
<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td>
			<blockquote>
			<p><br>
			<table border=0 cellpadding=5 cellspacing=0>
				<tr>
					<td><?=$Lang['eClassAPI']['OpenForUse']?>:</td>
					<td>
						<input type="radio" id="avaliable" name="avaliable" value="1" <?=$avaliable=='1'?'checked="checked"':''?> /><label for="avaliable"><?=$Lang['General']['Yes']?></label>
						&nbsp;
						<input type="radio" id="not_avaliable" name="avaliable" value="0"  <?=$avaliable!='1'?'checked="checked"':''?> /><label for="not_avaliable"><?=$Lang['General']['No']?></label>
					</td>
				</tr>
				<tr class="classTogglerable">
					<td><?=$Lang['HKSSF']['access_token']?>:</td>
					<td>
						<span id="access_token"><?=$access_token?></span>
						<button onclick="generate();return false;">
							<?=$Lang['HKSSF']['refresh']?>
						</button>
						<br/>
						<span class="extraInfo"><?=$Lang['HKSSF']['remark']?></span>
					</td>
				</tr>
			</table>
			</blockquote>
			</p><br>
		</td>
	</tr>
	<tr>
		<td><hr size="1"></td>
	</tr>
	<tr>
		<td align="right">
		<?= btnSubmit() /* ." ". btnReset()*/ ?>
		</td>
	</tr>
</table>
</form>
<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>