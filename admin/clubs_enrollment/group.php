<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclubsenrol.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();
$le = new libclubsenrol();
$quotas = $le->getGroupQuota();
$groups = $le->getGroupsForEnrolSet();

?>

<script language="javascript">

function checkform(obj){
        return true;
}

function checkAll(obj,val)
{
         <?
         for ($i=0; $i<sizeof($groups); $i++)
         {
         ?>
         obj.Allow<?=$i?>.checked=val;
         <?
         }
         ?>
}
</script>



<form name="form1" action="group_update.php" method="post" onSubmit="return checkform(this);" enctype="multipart/form-data">
<?= displayNavTitle($i_adminmenu_fs, '', $i_adminmenu_sc_clubs_enrollment_settings, 'index.php', $i_ClubsEnrollment_QuotaSettings, '') ?>
<?= displayTag("head_club_enrollment_group_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=400 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=1>
<tr>
<td class=tableTitle_new><u><?=$i_ClubsEnrollment_GroupName?></u></td>
<td class=tableTitle_new><u><?=$i_ClubsEnrollment_AllowEnrol?></u><input type=checkbox ONCLICK="checkAll(this.form,this.checked)"></td>
<td class=tableTitle_new><u><?=$i_ClubsEnrollment_Quota?></u></td>
</tr>
<?php
for ($i=0; $i<sizeof($groups); $i++)
{
     $id = $groups[$i][0];
     if ($quotas[$id]=="")
     {
         $chkAllow = "";
         $quota = "";
     }
     else
     {
         $chkAllow = "CHECKED";
         $quota = $quotas[$id];
     }
     ?>
     <tr>
     <td><?=$groups[$i][1]."<input type=hidden name=GroupID$i value=$id>"?></td>
     <td><input type=checkbox name=Allow<?="$i $chkAllow"?> value=1></td>
     <td><input type=text size=2 name=quota<?="$i value=$quota"?>></td>
     </tr>
     <?
}
?>
</table>
</blockquote>
<input type=hidden name=num value=<?=sizeof($groups)?>>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>