<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();
$li = new libdb();

for ($i=0; $i<$num; $i++)
{
     if (isset(${"Allow$i"}) && ${"Allow$i"}==1)
     {
         # Default
         $id = ${"GroupID$i"};
         $quota = ${"quota$i"};
         $AllowGroupList[] = $id;
         $quotaList[] = $quota+0;
     }
     else
     {
     }
}

if (is_array($AllowGroupList) && sizeof($AllowGroupList)!=0)
{
    $list = implode(",",$AllowGroupList);
    $sql = "DELETE FROM INTRANET_ENROL_GROUPINFO WHERE GroupID NOT IN ($list)";
    $li->db_db_query($sql);
    $sql = "SELECT GroupID FROM INTRANET_ENROL_GROUPINFO ORDER BY GroupID";
    $currentList = $li->returnVector($sql);

    for ($i=0; $i<sizeof($AllowGroupList); $i++)
    {
         $id = $AllowGroupList[$i];
         $quota = $quotaList[$i];
         if (sizeof($currentList)!=0 && in_array($id,$currentList))
         {
             $sql = "UPDATE INTRANET_ENROL_GROUPINFO SET Quota = '$quota',DateModified = now() WHERE GroupID = $id";
         }
         else
         {
             $sql = "INSERT INTO INTRANET_ENROL_GROUPINFO (GroupID,Quota,Approved,DateInput,DateModified)
                     VALUES ($id,$quota,0,now(),now())";
         }
         $li->db_db_query($sql);
    }
}
else
{
    $sql = "DELETE FROM INTRANET_ENROL_GROUPINFO";
    $li->db_db_query($sql);
}

header("Location: group.php?msg=2");
?>