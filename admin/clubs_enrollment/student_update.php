<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclubsenrol.php");
intranet_opendb();
$lf = new libfilesystem();
$minmax_file = "$intranet_root/file/clubenroll_minmax.txt";
$content = "";
for ($i=0; $i<$num; $i++)
{
     if (isset(${"Def$i"}) && ${"Def$i"}==1)
     {
         # Default
     }
     else
     {
         $id = ${"LevelID$i"};
         $min = ${"min$i"};
         $max = ${"max$i"};
         $disabled = ${"NotUse$i"};
         if ($disabled != 1 && ($min=="" || $max=="") )
         {
             continue;
         }
         $content .= "$id:::$min:::$max:::$disabled";
         if ($i!=$num-1) $content .= "\n";
     }
}
$lf->file_write($content,$minmax_file);

# Remove applications
$lc = new libclubsenrol();
$lc->removeNotUseEnrollmentApplication();

intranet_closedb();
header("Location: student.php?msg=2");
?>