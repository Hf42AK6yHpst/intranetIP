<?php
include_once("../../includes/global.php");
include_once("../../lang/email.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libclubsenrol.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();
$lf = new libfilesystem();
$le = new libclubsenrol();
$lc = new libclass();
$minmax_array = $le->getMinMax();
$lvls = $lc->getLevelArray();
?>

<script language="javascript">

function checkform(obj){
}
function selectAlt(obj,target)
{
         if (obj.checked)
         {
             target.checked = false;
         }
}

</script>


<form name="form1" action="student_update.php" method="post" onSubmit="return checkform(this);" enctype="multipart/form-data">
<?= displayNavTitle($i_adminmenu_fs, '', $i_adminmenu_sc_clubs_enrollment_settings, 'index.php', $i_ClubsEnrollment_StudentRequirement, '') ?>
<?= displayTag("head_club_enrollment_student_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=400 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=1>
<tr>
<td class=tableTitle_new><u><?=$i_ClassLevelName?></u></td>
<td class=tableTitle_new><u><?=$i_ClubsEnrollment_UseDefault?></u></td>
<td class=tableTitle_new><u><?=$i_ClubsEnrollment_Min?></u></td>
<td class=tableTitle_new><u><?=$i_ClubsEnrollment_Max?></u></td>
<td class=tableTitle_new><u><?=$i_ClubsEnrollment_Disable?></u></td>
</tr>
<?php
for ($i=0; $i<sizeof($lvls); $i++)
{
     $id = $lvls[$i][0];

     if ($minmax_array[$id]===false)
     {
         $chkDef = "";
         $min = "";
         $max = "";
         $disabled = "CHECKED";
     }
     else if (!is_array($minmax_array[$id]))
     {
         $chkDef = "CHECKED";
         $min = "";
         $max = "";
         $disabled = "";
     }
     else
     {
         $chkDef = "";
         $min = $minmax_array[$id][0];
         $max = $minmax_array[$id][1];
         $disabled = "";
     }
     ?>
     <tr>
     <td><?=$lvls[$i][1]."<input type=hidden name=LevelID$i value=$id>"?></td>
     <td><input type=checkbox name=Def<?="$i $chkDef"?> value=1 onClick="selectAlt(this.form.Def<?=$i?>,this.form.NotUse<?=$i?>)"></td>
     <td><input type=text size=2 name=min<?="$i value=$min"?>></td>
     <td><input type=text size=2 name=max<?="$i value=$max"?>></td>
     <td><input type=checkbox name=NotUse<?="$i $disabled"?> value=1 onClick="selectAlt(this.form.NotUse<?=$i?>,this.form.Def<?=$i?>)""></td>
     </tr>
     <?
}
?>
</table>
<br>
<font color=red><?=$i_ClubsEnrollment_Warning_NotUse?></font>
</blockquote>
<input type=hidden name=num value=<?=sizeof($lvls)?>>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

</form>

<?php
include_once("../../templates/adminfooter.php");
?>