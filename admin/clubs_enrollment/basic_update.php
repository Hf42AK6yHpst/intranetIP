<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
$lf = new libfilesystem();

$AppStart = intranet_htmlspecialchars($AppStart);
$AppEnd = intranet_htmlspecialchars($AppEnd);
$GpStart = intranet_htmlspecialchars($GpStart);
$GpEnd = intranet_htmlspecialchars($GpEnd);
$Description = intranet_htmlspecialchars($Description);

$setting_file = "$intranet_root/file/clubenroll_setting.txt";
$desp_file = "$intranet_root/file/clubenroll_desp.txt";
$file_content = $lf->file_read($setting_file);
$lines = split("\n",$file_content);
$lines[0] = $mode;
switch ($mode)
{
        case 0:           # Disabled
             break;
        case 1:           # Simple
             $lines[1] = $AppStart;
             $lines[2] = $AppEnd;
             $lines[3] = $GpStart;
             $lines[4] = $GpEnd;
             $lines[5] = $defaultMin;
             $lines[6] = $defaultMax;
             break;
        case 2:           # Advanced
             $lines[1] = $AppStart;
             $lines[2] = $AppEnd;
             $lines[3] = $GpStart;
             $lines[4] = $GpEnd;
             $lines[5] = $defaultMin;
             $lines[6] = $defaultMax;
             $lines[7] = $tiebreak;
             break;
}

$updatedcontent = implode("\n",$lines);
$lf->file_write($updatedcontent,$setting_file);
$lf->file_write($Description, $desp_file);
if ($clearRecord==1)
{
    intranet_opendb();
    $li = new libdb();
    $sql = "DELETE FROM INTRANET_ENROL_STUDENT";
    $li->db_db_query($sql);
    $sql = "DELETE FROM INTRANET_ENROL_GROUPSTUDENT";
    $li->db_db_query($sql);
    $sql = "UPDATE INTRANET_ENROL_GROUPINFO SET Approved = 0";
    $li->db_db_query($sql);
}
header("Location: basic.php?msg=2");
?>