<?php
include_once("../../includes/global.php");
include_once("../../lang/email.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclubsenrol.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
$li = new libfilesystem();
$lc = new libclubsenrol();
/*
$setting_file = "$file_path/file/clubenroll_setting.txt";
$desp_file = "$file_path/file/clubenroll_desp.txt";
$file_content = $li->file_read($setting_file);
$lines = split("\n",$file_content);
$Description = $li->file_read($desp_file);

$mode = $lines[0];
$AppStart = $lines[1];
$AppEnd = $lines[2];
$GpStart = $lines[3];
$GpEnd = $lines[4];
$defaultMin = $lines[5];
$defaultMax = $lines[6];
$tiebreak = $lines[7];
*/
$mode = $lc->mode;
$AppStart = $lc->AppStart;
$AppEnd = $lc->AppEnd;
$GpStart = $lc->GpStart;
$GpEnd = $lc->GpEnd;
$defaultMin = $lc->defaultMin;
$defaultMax = $lc->defaultMax;
$tiebreak = $lc->tiebreak;
$Description = $lc->Description;

$mode += 0;
$chkDisabled = ($mode==0? "CHECKED":"");
$chkSimple = ($mode==1? "CHECKED":"");
$chkAdvanced = ($mode==2? "CHECKED":"");
$min_numbers = array (0,1,2,3,4,5,6,7,8,9);
$max_numbers = array (array(0,$i_ClubsEnrollment_NoLimit));
for ($i=1; $i<10; $i++)
{
     $max_numbers[] = array($i,$i);
}

$minSelection = getSelectByValue($min_numbers,"name=defaultMin",$defaultMin);
$maxSelection = getSelectByArray($max_numbers,"name=defaultMax",$defaultMax);
$tiebreak += 0;
$chkRan = ($tiebreak==0? "SELECTED":"");
$chkTime = ($tiebreak==1? "SELECTED":"");
$tiebreakerSelection = "<SELECT name=tiebreak>\n";
$tiebreakerSelection.= "<OPTION value=0 $chkRan>$i_ClubsEnrollment_Random</OPTION>\n";
$tiebreakerSelection.= "<OPTION value=1 $chkTime>$i_ClubsEnrollment_AppTime</OPTION>\n";
$tiebreakerSelection.= "</SELECT>\n"
?>

<script language="javascript">
function changemode(obj,value){
         switch (value)
         {
                 case 0:
                      obj.AppStart.disabled = true;
                      obj.AppEnd.disabled = true;
                      obj.GpStart.disabled = true;
                      obj.GpEnd.disabled = true;
                      obj.Description.disabled = true;
                      obj.defaultMin.disabled = true;
                      obj.defaultMax.disabled = true;
                      obj.tiebreak.disabled = true;
                      break;
                 case 1:
                      obj.AppStart.disabled = false;
                      obj.AppEnd.disabled = false;
                      obj.GpStart.disabled = false;
                      obj.GpEnd.disabled = false;
                      obj.Description.disabled = false;
                      obj.defaultMin.disabled = false;
                      obj.defaultMax.disabled = false;
                      obj.tiebreak.disabled = true;
                      break;
                 case 2:
                      obj.AppStart.disabled = false;
                      obj.AppEnd.disabled = false;
                      obj.GpStart.disabled = false;
                      obj.GpEnd.disabled = false;
                      obj.Description.disabled = false;
                      obj.defaultMin.disabled = false;
                      obj.defaultMax.disabled = false;
                      obj.tiebreak.disabled = false;
                      break;


         }
}

function checkform(obj){
         switch (obj.mode.value)
         {
                 case 0:
                      break;
                 case 2:
                      if (obj.defaultMax.value != 0 && obj.defaultMin.value > obj.defaultMax.value)
                      {
                          alert("<?=$i_ClubsEnrollment_DefaultMaxMinWrong?>");
                          return false;
                      }
                 case 1:
                      if (trim(obj.AppStart.value) != '')
                      {
                          if (!check_date(obj.AppStart,"<?php echo $i_invalid_date; ?>")) return false;
                      }
                      if (trim(obj.AppEnd.value) != '')
                      {
                          if (!check_date(obj.AppEnd,"<?php echo $i_invalid_date; ?>")) return false;
                      }
                      if (trim(obj.GpStart.value) != '')
                      {
                          if (!check_date(obj.GpStart,"<?php echo $i_invalid_date; ?>")) return false;
                      }
                      if (trim(obj.GpEnd.value) != '')
                      {
                          if (!check_date(obj.GpEnd,"<?php echo $i_invalid_date; ?>")) return false;
                      }
                      break;
         }
         return true;
}
</script>


<form name="form1" action="basic_update.php" method="post" onSubmit="return checkform(this);" enctype="multipart/form-data">
<?= displayNavTitle($i_adminmenu_fs, '', $i_adminmenu_sc_clubs_enrollment_settings, 'index.php', $i_ClubsEnrollment_BasicSettings, '') ?>
<?= displayTag("head_basic_setting_$intranet_session_language.gif", $msg) ?>
<blockquote>
<table width=450 border=0 cellpadding=4 cellspacing=0 align=center>
<tr><td colspan=2 class=tableTitle_new>
<input type=radio name=mode value=0 onClick=changemode(this.form,0) <?=$chkDisabled?>><?=$i_ClubsEnrollment_Disable?>
<input type=radio name=mode value=1 onClick=changemode(this.form,1) <?=$chkSimple?>><?=$i_ClubsEnrollment_Simple?>
<input type=radio name=mode value=2 onClick=changemode(this.form,2) <?=$chkAdvanced?>><?=$i_ClubsEnrollment_Advanced?>
</td></tr>
<tr><td width=50% align=right><?php echo $i_ClubsEnrollment_AppStart; ?>:</td>
<td><input class=text type=text name=AppStart size=10 maxlength=10 value="<?php echo $AppStart; ?>"> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td width=50% align=right><?php echo $i_ClubsEnrollment_AppEnd; ?>:</td>
<td><input class=text type=text name=AppEnd size=10 maxlength=10 value="<?php echo $AppEnd; ?>"> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td width=50% align=right><?php echo $i_ClubsEnrollment_GpStart; ?>:</td>
<td><input class=text type=text name=GpStart size=10 maxlength=10 value="<?php echo $GpStart; ?>"> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td width=50% align=right><?php echo $i_ClubsEnrollment_GpEnd; ?>:</td>
<td><input class=text type=text name=GpEnd size=10 maxlength=10 value="<?php echo $GpEnd; ?>"> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td width=50% align=right><?php echo $i_ClubsEnrollment_ApplicationDescription; ?>:</td>
<td><textarea name=Description rows=5 cols=40><?php echo $Description; ?></textarea></td></tr>
<tr><td width=50% align=right><?php echo $i_ClubsEnrollment_DefaultMin; ?>:</td>
<td><?=$minSelection?></td></tr>
<tr><td width=50% align=right><?php echo $i_ClubsEnrollment_DefaultMax; ?>:</td>
<td><?=$maxSelection?></td></tr>
<tr><td width=50% align=right><?php echo $i_ClubsEnrollment_TieBreakRule; ?>:</td><td><?=$tiebreakerSelection?></td></tr>
<tr><td width=50% align=right><?php echo $i_ClubsEnrollment_ClearRecord; ?>:</td>
<td><input type=checkbox name=clearRecord value=1></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<SCRIPT LANGUAGE=Javascript>
changemode(document.form1,<?=$mode?>);
</SCRIPT>
<?php
include_once("../../templates/adminfooter.php");
?>