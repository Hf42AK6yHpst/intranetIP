<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libfiletable.php");
include("../../includes/libdb.php");
include("../../includes/libannounce.php");
include("../../lang/lang.$intranet_session_language.php");

$body_tags = "onload=auto()";
include("../../templates/fileheader.php");
intranet_opendb();
$la = new libannounce($aid);
$li = new libfilesystem();

# Set attachment path
if ($la->Attachment == "")
{
    $folder = session_id().".a";
    $sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = '$folder' WHERE AnnouncementID = $aid";
    $la->db_db_query($sql);
}
else
    $folder = $la->Attachment;

$no_file = 5;
$path = "$file_path/file/announcement/$folder".$la->AnnouncementID;

$li->folder_new($path);
for ($i=1; $i<=5; $i++)
{
     $key = "filea$i";
     $loc = ${"filea$i"};
     #$file = ${"filea$i"."_name"};
     $file = stripslashes(${"hidden_userfile_name$i"});

     $des = "$path/$file";
     if ($loc == "none" || $loc == "")
     {} else
     {
        if (strpos($file,"."===false)){
        } else
        {
              $li->lfs_copy($loc, $des);
        }
     }
}

$queryString = "&ea=1&Title=$Title&Description=$Description&AnnouncementDate=$AnnouncementDate&EndDate=$EndDate";
/*
$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
par.location.href = par.location.href + '<?=$queryString?>';

*/
?>
<script language="JavaScript1.2">
par = opener.window;

function auto()
{
        document.autoform.target = opener.name;
        document.autoform.submit();
        self.close();
}

</script>
<form ACTION="edit.php" name=autoform method=post>
<input type=hidden name=ea value=1>
<input type=hidden name=AnnouncementID value="<?php echo $aid; ?>">

<input type=hidden name=Title value="<?=stripslashes($Title)?>">
<input type=hidden name=Description value="<?=stripslashes($Description)?>">
<input type=hidden name=AnnouncementDate value="<?=$AnnouncementDate?>">
<input type=hidden name=EndDate value="<?=$EndDate?>">
</form>
<?php
include("../../templates/filefooter.php");
?>