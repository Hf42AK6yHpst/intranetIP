<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../templates/fileheader_admin.php");
include("../../lang/lang.$intranet_session_language.php");
intranet_opendb();
$no_file = 5;

$Title = intranet_htmlspecialchars(trim(stripslashes($Title)));
$Description = intranet_htmlspecialchars(trim(stripslashes($Description)));
$AnnouncementDate = intranet_htmlspecialchars(trim($AnnouncementDate));
$EndDate = intranet_htmlspecialchars(trim($EndDate));
$aid = $AnnouncementID;

echo generateFileUploadNameHandler("form1","filea1","hidden_userfile_name1"
,"filea2","hidden_userfile_name2"
,"filea3","hidden_userfile_name3"
,"filea4","hidden_userfile_name4"
,"filea5","hidden_userfile_name5"
);
?>

<form name="form1" action="attach_update.php" method="post" enctype="multipart/form-data" onsubmit="return Big5FileUploadHandler();">
<?= displayNavTitle($i_AnnouncementNewAttachment, '') ?>

<p style="padding-left:20px">
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
<tr><td style="background-image: url(../../../images/admin/pop_bg.gif);" >

<br>
<table border=0 cellpadding=5 cellspacing=0 align="center">
<?php for($i=1;$i<=$no_file;$i++){ ?><tr><td align=center><input class=file type=file name="filea<?php echo $i; ?>" size=20>
<input type=hidden name="hidden_userfile_name<?=$i?>">
</td></tr>
<?php } ?>
</table>
<br>

</td></tr>
<tr><td><img src="../../../images/admin/pop_bottom.gif" width=422 height=18 border=0></td></tr>
<tr><td align=center height="40" style="vertical-align:bottom">
<input type="image" src="/images/admin/button/s_btn_upload_<?=$intranet_session_language?>.gif" border="0">
<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
</p>

<input type=hidden name=aid value="<?php echo $aid; ?>">
<input type=hidden name=Title value="<?=$Title?> ">
<input type=hidden name=Description value="<?=$Description?>">
<input type=hidden name=AnnouncementDate value="<?=$AnnouncementDate?>">
<input type=hidden name=EndDate value="<?=$EndDate?>">
</form>

<?php
include("../../templates/filefooter.php");
?>