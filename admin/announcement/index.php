<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
if($filter == "") $filter = 1;
$filter = ($filter == 1) ? 1 : 0;
$user_field = getNameFieldWithClassNumberByLang("b.");
$sql  = "SELECT
                        DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'),
                        DATE_FORMAT(a.EndDate, '%Y-%m-%d'),
                        CONCAT('<a class=tableContentLink href=edit.php?AnnouncementID[]=', a.AnnouncementID, '>', a.Title, '</a>',' <a href=javascript:showRead(',a.AnnouncementID,')><img src=$image_path/icon_viewstatics.gif border=0 alt=\"$i_AnnouncementViewReadStatus\"></a>'),
                        IF (a.OwnerGroupID IS NOT NULL OR a.OwnerGroupID != 0,
                            IF (a.UserID IS NOT NULL OR a.UserID != 0,$user_field,'$i_AnnouncementNoAnnouncer'),
                            '$i_AnnouncementSystemAdmin'),
                        IF (a.OwnerGroupID IS NOT NULL OR a.OwnerGroupID != 0, c.Title, '--'),
                        a.DateModified,
                        CONCAT('<input type=checkbox name=AnnouncementID[] value=', a.AnnouncementID ,'>')
                FROM
                        INTRANET_ANNOUNCEMENT as a LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = a.UserID LEFT OUTER JOIN INTRANET_GROUP as c ON c.GroupID = a.OwnerGroupID
                WHERE
                        (a.Title like '%$keyword%') AND
                        a.RecordStatus = $filter
                ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.AnnouncementDate","a.EndDate", "a.Title","$user_field","c.Title","a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_announcement;
$li->column_array = array(0,0,5,0,0,0);
$li->wrap_array = array(0,0,20,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(0, $i_AnnouncementDate)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(1, $i_AnnouncementEndDate)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(2, $i_AnnouncementTitle)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(3, $i_AnnouncementOwner)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(4, $i_AnnouncementByGroup)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(5, $i_AnnouncementDateModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("AnnouncementID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=\"javascript:checkNew('new.php')\">".newIcon()."$button_new</a>";
$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'AnnouncementID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'AnnouncementID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$searchbar  = "<select name=filter onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value=0 ".(($filter==0)?"selected":"").">$i_status_pending</option>\n";
$searchbar .= "<option value=1 ".(($filter==1)?"selected":"").">$i_status_publish</option>\n";
$searchbar .= "</select>\n";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>
<SCRIPT LANGUAGE=Javascript>
function showRead(id)
{
         newWindow('read.php?AnnouncementID='+id,1);
}
</SCRIPT>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_admintitle_im_announcement, '') ?>
<?= displayTag("head_announcement_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>