<?php
include("../../includes/global.php");
///include("../../includes/libfilesystem.php");
include("../../includes/libdb.php");
include("../../includes/libgrouping.php");
include("../../includes/libemail.php");
include("../../includes/libsendmail.php");
include("../../lang/email.php");
intranet_opendb();

$li = new libgrouping();

$Title = intranet_htmlspecialchars(trim($Title));
$Description = intranet_htmlspecialchars(trim($Description));
$AnnouncementDate = intranet_htmlspecialchars(trim($AnnouncementDate));
$EndDate = intranet_htmlspecialchars(trim($EndDate));

$startStamp = strtotime($AnnouncementDate);
$endStamp = strtotime($EndDate);


if (compareDate($startStamp,$endStamp)>0)   // start > end
{
    $valid = false;
}
else if (compareDate($startStamp,time())<0)      // start < now
{
     $valid = false;
}
else
{
    $valid = true;
}

if ($valid)
{
     # Set attachment
     $folder = session_id()."_a";

     $sql = "INSERT INTO INTRANET_ANNOUNCEMENT (Title, Description, AnnouncementDate, RecordStatus, DateInput, DateModified, EndDate) VALUES ('$Title', '$Description', '$AnnouncementDate', '$RecordStatus', now(), now(),'$EndDate')";
     $li->db_db_query($sql);
     $AnnouncementID = $li->db_insert_id();

     for($i=0; $i<sizeof($GroupID); $i++){
             $sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES (".$GroupID[$i].", $AnnouncementID)";
             $li->db_db_query($sql);
     }

     $path = "$file_path/file/announcement/$folder$AnnouncementID";
     # Copy files
     $lf = new libfilesystem();

     $hasAttachment = false;
	 $attachment_size=$attachment_size==""?0:$attachment_size;	
     for ($i=0; $i<$attachment_size; $i++)
     {
          $key = "filea$i";
          $loc = ${"filea$i"};
          $file = stripslashes(${"hidden_userfile_name$i"});
          #$file = ${"filea$i"."_name"};
          $des = "$path/$file";
          if ($loc == "none" || $loc=="")
          {} 
          else
          {
             if (strpos($file,"."==0)){
             } else
             {
                   if (!$hasAttachment)
                   {
                        //$lf->folder_new("$file_path/file/announcement");
                        $lf->folder_new ($path);
                        $hasAttachment = true;
                   }
                   $lf->lfs_copy($loc, $des);
             }
          }

     }

     # Update attachment in DB
     if ($hasAttachment)
     {
          $sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = '$folder' WHERE AnnouncementID = $AnnouncementID";
          $li->db_db_query($sql);
     }

	if ($plugin['power_voice'])
	{      
        if (($voiceFile!="")  && ($voicePath!=""))
        {	        
			if (!$hasAttachment)
			{
				$lf->folder_new ($path);
				$hasAttachment = true;
			}
			$loc = $voicePath."/".$voiceFile;
			$des = $path."/".$voiceFile;
			$lf->lfs_move($loc, $des);     
						
         	$sql = 	"	
         				UPDATE 
         					INTRANET_ANNOUNCEMENT 
         				SET 
         					VoiceFile = '".addslashes($des)."' 
         				WHERE 
         					AnnouncementID = {$AnnouncementID}
         				";
         	$li->db_db_query($sql);
			
		}
	}
	
     if ($special_announce_public_allowed && $publicdisplay == 1)
     {
         $sql = "UPDATE INTRANET_ANNOUNCEMENT SET RecordType = 1 WHERE AnnouncementID = $AnnouncementID";
         $li->db_db_query($sql);
     }

     # Call sendmail function
     if($email_alert==1){
     $type = (sizeof($GroupID)==0? "School": "Group");
     $mailSubject = announcement_title_20($AnnouncementDate,$type); //stripslashes($AnnouncementDate." ".$Title);
//   $mailBody = announcement_body($Title, $type).email_footer(); //stripslashes($Description).email_footer();
     if (sizeof($GroupID)==0)
     {
         $mailBody = school_announcement_body($AnnouncementDate,$Title).email_footer();
     }
     else
     {
         $Groups = $li->returnGroupNames($GroupID);
         $mailBody = group_announcement_body($AnnouncementDate,$Title,$Groups).email_footer();
     }
//      
     # 20081016 yat woon
	# trim the empty group which contain "&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"
	$tmpGroupID = implode(",", $GroupID);
	$tmpGroupID = str_replace("&#160;","",$tmpGroupID);
	$tmpGroupID = explode(",", $tmpGroupID);
	$GroupID = array_remove_empty($tmpGroupID);
	
     $mailTo = $li->returnUsersEmailGroup($GroupID);
     $lu = new libsendmail();
     $lu->do_sendmail($webmaster, "", $mailTo, "", $mailSubject, $mailBody);
     }

     /*
     if ($sms_alert ==1 && isset($plugin['sms']) && $plugin['sms']){
         $sms_body = "$Title: $Description";
         
         //if (isBig5($sms_body))
        // {
         //    $mode = "BIG5";
         //    $count = 140;
         //}
         //else
        // {
        //     $mode = "ASCII";
        //     $count = 160;
        // }
         $sms_body = substr($sms_body,0,$count);
        
         #$sms_body = urlencode($sms_body);
         $sms_numbers = $li->returnUsersMobileGroup($GroupID);  # Array (id, mobile)
         if ($sms_vendor == "ORANGE")
         {
             include_once("../../includes/liborangesms.php");
             $lsms = new liborangesms();
             for ($i=0; $i<sizeof($sms_numbers); $i++)
             {
                  list ($login,$mobile) = $sms_numbers[$i];
                  $number_array[] = $mobile;
             }
             $records = $lsms->sendSMS($number_array,$sms_body, "");
             $logsent = $lsms->sendLogRecords($records);
             $values = "";
             $delim = "";
             for ($i=0; $i<sizeof($records); $i++)
             {
                  $jobid = $records[$i]->jobid;
                  $mobile = $records[$i]->recipient;
                  $values .= "$delim('$jobid','$mobile','$sms_message',1,now(),now())";
                  $delim = ",";
             }
             $sql = "INSERT IGNORE INTO INTRANET_SMS_LOG (JobID, MobileNumber,Message,RecordType,DateInput,DateModified) VALUES $values";
             $li->db_db_query($sql);
         }
         */
         /*
         $log_file = "$intranet_root/file/sms.log";
         $log_fp = fopen ($log_file,"a");

         for ($i=0; $i<sizeof($sms_numbers); $i++)
         {
              list ($login,$mobile) = $sms_numbers[$i];
              if ($mobile != "")
              {
                  $target_path ="$sms_path?mobileid=$mobile&mode=$mode&userid=$sms_user&pwd=$sms_pwd&msg=$sms_body";
                  $fp = @fopen ($target_path, "r");
                  if (!$fp)
                  {
                       # Server Down
                  }
                  $response = @fgets($fp,1024);
                  $response = rtrim($response);
                  $log_time = date('Y-m-d H:i:s');
                  $line = "$log_time,$login,$mobile,$mode,$response\n";
                  fputs($log_fp,$line);
                  $log_path = "$sms_log_path?mobileid=$mobile&login=$login";
              }
         }
         */
     /* } */

     intranet_closedb();
     header("Location: index.php?filter=$RecordStatus&msg=1");
}
else
{
    header("Location: new.php?t=$Title&d=$Description&ad=$AnnouncementDate&ed=$EndDate");
}
?>
