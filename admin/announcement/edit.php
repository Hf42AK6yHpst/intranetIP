<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libannounce.php");
include_once("../../includes/libgrouping.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libfiletable.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

$AnnouncementID = (is_array($AnnouncementID)? $AnnouncementID[0]:$AnnouncementID);
$li = new libannounce($AnnouncementID);
$lo = new libgrouping();
$number_of_groups = sizeof($lo->returnGroups());
$RecordStatus0 = ($li->RecordStatus==0) ? "CHECKED" : "";
$RecordStatus1 = ($li->RecordStatus==1) ? "CHECKED" : "";



if ($invalid==1)
{
    echo "<p> $i_con_msg_date_wrong </p>\n";
}
if ($ea==1)   // Editing attachment
{
    $cTitle = stripslashes($Title);
    $cDescription = stripslashes($Description);
    $cStartDate = $AnnouncementDate;
    $cEndDate = $EndDate;
}
else
{
    $cTitle = $li->Title;
    $cDescription = $li->Description;
    $cStartDate = $li->AnnouncementDate;
    $cEndDate = $li->EndDate;
}

if ($li->RecordType == 1)
{
    $public_checked = " CHECKED";
}
else
{
    $public_checked = "";
}

// form1.Description.value=form1.Description.value.replace("\n","\\n");
/*
   newWindow('more_attach.php?aid=<?=$li->AnnouncementID?>
   &Title='+form1.Title.value+'&Description='+form1.
   Description.value+'&AnnouncementDate='+form1.AnnouncementDate.value
   +'&EndDate='+form1.EndDate.value,2)

*/

if ($plugin['power_voice'])
{	
	$RecBtn = "<input type='button' name='pv_rec_btn' value='".$iPowerVoice['powervoice']."' onclick='editPVoice()' />";
	$ClearBtn = "<input type='button' name='pv_clear_btn' value='".$button_clear."' onclick='clearPVoice()' />";
	$VoiceClip = " <img src='{$image_path}/{$LAYOUT_SKIN}/icon_voice.gif' border='0' align='absmiddle' /> ";
			
	if ($li->VoiceFile != "")
	{
		$VoiceFileName = basename($li->VoiceFile);
		//$VoiceFileDir = substr($li->VoiceFile,0,strlen($VoiceFileName));		
		$VoiceFileDir = dirname($li->VoiceFile);		
		
		//$VoiceFileLink = "<a href='".str_replace($intranet_root,"",$li->VoiceFile)."' target='_blank' />".$VoiceFileName."</a>";
		$VoiceFileLink = "<a href=\"javascript:listenPVoice('".str_replace($intranet_root,"",$li->VoiceFile)."')\"'  />".$VoiceClip.$VoiceFileName."</a>";
		$CurVoiceFileLink = str_replace($intranet_root,"",$li->VoiceFile);
		
		$HiddenHtml = "<input type=\"hidden\" name=\"voiceFile\" value=\"{$VoiceFileName}\" ><input type=\"hidden\" name=\"voicePath\" value=\"{$VoiceFileDir}\" >";
		$HiddenHtml .= "<input type=\"hidden\" name=\"voiceFileFromWhere\" value=\"\" >";	
		
		$voiceContent .= "<table cellpadding=\"0\" cellspacing=\"0\" >";
		$voiceContent .= "<tr>";
		$voiceContent .= "	<td><span id=\"voiceDiv\" >".$VoiceFileLink."</span>&nbsp;&nbsp;</td>";
		$voiceContent .= "	<td>{$RecBtn}{$HiddenHtml}</td>";
		$voiceContent .= "	<td><span id=\"clearDiv\" >{$ClearBtn}</span></td>";
		$voiceContent .= "</tr>";
		$voiceContent .= "</table>";			
	}
	else
	{
		$HiddenHtml = "<input type=\"hidden\" name=\"voiceFile\" ><input type=\"hidden\" name=\"voicePath\" >";
		$HiddenHtml .= "<input type=\"hidden\" name=\"voiceFileFromWhere\" value=\"1\" >";	
		
		$voiceContent .= "<table cellpadding=\"0\" cellspacing=\"0\" >";
		$voiceContent .= "<tr>";
		$voiceContent .= "	<td><span id=\"voiceDiv\" ></span></td>";
		$voiceContent .= "	<td>{$RecBtn}{$HiddenHtml}</td>";
		$voiceContent .= "	<td><span id=\"clearDiv\" style=\"display:none\" >{$ClearBtn}</span></td>";
		$voiceContent .= "</tr>";
		$voiceContent .= "</table>";	
	}
	
	$PVHtml = "<tr>";
	$PVHtml .= "<td align='right' nowrap='nowrap' width='100'>".$iPowerVoice['sound_recording'].":</td>";
	$PVHtml .= "<td>".$voiceContent."</td>";
	$PVHtml .= "</tr>";
}

?>

<script language="javascript">
function checkform(obj)
{	
        if(!check_text(obj.AnnouncementDate, "<?php echo $i_alert_pleasefillin.$i_AnnouncementDate; ?>.")) return false;
        if(!check_date(obj.AnnouncementDate, "<?php echo $i_invalid_date; ?>.")) return false;
        if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_AnnouncementTitle; ?>.")) return false;
        checkOptionAll(obj.elements["GroupID[]"]);
        Big5FileUploadHandler();
        
        // create a list of files to be deleted
        objDelFiles = document.getElementsByName('file2delete[]');
        files = obj.deleted_files;
        if(objDelFiles==null)return true;
        x="";
        for(i=0;i<objDelFiles.length;i++){
		        if(objDelFiles[i].checked==false){
			 		x+=objDelFiles[i].value+":";       
			    }
	    }
	    files.value = x.substr(0,x.length-1);
	    return true;
}
function Big5FileUploadHandler() {
	 new_attach = parseInt(document.form1.attachment_size.value);
	 for(i=1;i<=new_attach;i++){
		 	objFile = eval('document.form1.filea'+i);
		 	objUserFile = eval('document.form1.hidden_userfile_name'+i);
		 	if(typeof(objFile)!='undefined' && objFile.value!='' && typeof(objUserFile)!='undefined'){
			 	Ary = objFile.value.split('\\');
			 	if(Ary!=null){
				 	objUserFile.value = Ary[Ary.length-1];
				}
			}
	 }
	 return true;
}
function fileAttach(obj){
         aWin=newWindow("",1);
         temp = obj.target;
         obj.target="intranet_popup1";
         obj.action="more_attach.php";
         obj.submit();
         obj.target=temp;
         obj.action="edit_update.php";
}
function setRemoveFile(index,v){
	obj = document.getElementById('a_'+index);
	obj.style.textDecorationLineThrough = !v;
}
function add_field(formObj)
{
	var table = document.getElementById("file_list");
	var new_attachment = parseInt(formObj.attachment_size.value);
	var row = table.insertRow(new_attachment);
	if (document.all)
	{
		var cell = row.insertCell(0);
		new_attachment++;
		x= '<input class="file" type="file" name="filea'+new_attachment+'" size="40">';
		x+='<input type="hidden" name="hidden_userfile_name'+new_attachment+'">';

		cell.innerHTML = x;
		document.form1.attachment_size.value = new_attachment;
	}	
}

<?php
if ($plugin['power_voice'])
{ 
?>
	var NewRecordBefore = false;
	
	function listenPVoice(fileName)
	{		
		newWindow("http://<?=$eclass_httppath?>/src/tool/powervoice/pvoice_listen_now_intranet.php?from_intranet=1&location="+fileName, 18);
	}	
	function listenPVoice2(fileName)
	{		
		newWindow("http://<?=$eclass_httppath?>/src/tool/powervoice/pvoice_listen_now_intranet2.php?from_intranet=1&location="+fileName, 18);
	}	

	function editPVoice()
	{
		var fileName = document.form1.voiceFile.value;		
		
		newWindow("http://<?=$eclass_httppath?>/src/tool/powervoice/index.php?from_intranet=1&fileName="+fileName, 17);
	}	
	
	function clearPVoice(num)
	{		
		var voiceObj = document.getElementById("voiceDiv");
		var clearObj = document.getElementById("clearDiv");
		
		voiceObj.innerHTML = "";
		document.form1.voiceFile.value = "";		
		clearObj.style.display = "none";
	}

	function file_exists()
	{
		alert("<?=$classfiles_alertMsgFile12?>");
	}
	
	function file_saved(fileName,filePath,fileHttpPath,fileEmpty)
	{				
		var voiceObj = document.getElementById("voiceDiv");
		var clearObj = document.getElementById("clearDiv");
				
		if (fileName != "")
		{
			if ((fileHttpPath != undefined) &&  (fileHttpPath != ""))
			{				
				if ((fileEmpty=="1") && (!NewRecordBefore))
				{
					voiceObj.innerHTML = "<a href=\"javascript:listenPVoice('<?=$CurVoiceFileLink?>')\" ><?=$VoiceClip?>"+fileName+"</a>&nbsp;&nbsp;";
					document.form1.is_empty_voice.value = "1";
				}				
				else	
				{
					voiceObj.innerHTML = "<a href=\"javascript:listenPVoice2('"+fileHttpPath+"')\" ><?=$VoiceClip?>"+fileName+"</a>&nbsp;&nbsp;";
					document.form1.voicePath.value = filePath;
					if (fileEmpty=="1") 
						document.form1.is_empty_voice.value = "1";
					else	
						document.form1.is_empty_voice.value = "";
					NewRecordBefore = true;	
				}
			}
			else	
				voiceObj.innerHTML = fileName+"&nbsp;&nbsp;";
			document.form1.voiceFile.value = fileName;			
			document.form1.voiceFileFromWhere.value = 1;
			clearObj.style.display = "block";
		}
	}
		
<?php
} 
?>

</script>

<form name="form1" action="edit_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_admintitle_im_announcement, 'javascript:history.back()', $button_edit, '') ?>
<?= displayTag("head_announcement_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td nowrap width=100 align=right><?php echo $i_AnnouncementDate; ?>:</td><td><input class=text type=text name=AnnouncementDate size=10 maxlength=10 value="<?php echo $cStartDate; ?>"> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td nowrap width=100  align=right><?php echo $i_AnnouncementEndDate; ?>:</td><td><input class=text type=text name=EndDate size=10 maxlength=10 value="<?php echo $cEndDate; ?>"> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td width=100  align=right><?php echo $i_AnnouncementTitle; ?>:</td><td><input class=text type=text name=Title size=30 maxlength=100 value="<?php echo $cTitle; ?>">
</td></tr>
<tr><td width=100 align=right><?php echo $i_AnnouncementDescription; ?>:</td><td><textarea name=Description cols=60 rows=10><?php echo $cDescription; ?></textarea></td></tr>
<?=$PVHtml?>
<tr><td align=right nowrap><?=$i_AnnouncementCurrAttachment?>:</td><td>
<table width=80% border=0 bordercolor='#F7F7F9' cellspacing=1 cellpadding=1>
<tr><td nowrap><? /* $i_AnnouncementDelete */?></td><td><?php /*echo $i_AnnouncementCurrAttachment;*/ ?> </td></tr>
<?php
if ($li->Attachment != "")
    echo $li->displayAttachmentEdit("file2delete[]");
else echo "<tr><td colspan=2>$i_AnnouncementNoAttachment</td></tr>"; ?>
</table>
<table width=80% border=0 id='file_list' cellpadding=0 cellspacing=0>
<tr><td>
<input class="file" type="file" name="filea1" size="40">
<input type="hidden" name="hidden_userfile_name1">
</td></tr>
</table>
<!--
<a href="javascript:add_field(document.form1)"><img src="/images/admin/button/s_btn_more_attachment_<?=$intranet_session_language?>.gif" border="0">
</a>-->
<input type=button value=" + " onClick="add_field(document.form1)">
</td></tr>
<tr><td align=right nowrap><?php echo $i_AnnouncementRecordStatus; ?>:</td><td><input type=radio name=RecordStatus value=1 <?php echo $RecordStatus1; ?>> <?php echo $i_status_publish; ?> <input type=radio name=RecordStatus value=0 <?php echo $RecordStatus0; ?>> <?php echo $i_status_pending; ?></td></tr>
<tr><td width=100 ><br></td><td><input type=checkbox name=email_alert value=1> <?php echo $i_AnnouncementAlert; ?></td></tr>
<?php if ($special_announce_public_allowed) {?>
<tr><td width=100><br></td><td><input type=checkbox name=publicdisplay value=1<?=$public_checked?>> <?php echo $i_AnnouncementPublic; ?></td></tr>
<?php } ?>
<tr><td colspan=2><?=$i_AnnouncementPublicInstruction?> </td></tr>
<tr><td align="right"><?=$i_admintitle_group?>:</td><td><?php echo $lo->displayAnnouncementGroups($li->AnnouncementID); ?></td></tr>
</table>
</blockquote>

<input type=hidden name=AnnouncementID value="<?php echo $li->AnnouncementID; ?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type="hidden" name="attachment_size" value='1' />
<input type="hidden" name="deleted_files" value='' />
<input type="hidden" name="is_empty_voice" value='' />

</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>