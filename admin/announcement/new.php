<?php

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgrouping.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

// special_announce_public_allowed
$lo = new libgrouping();
if ($t!="")
    echo "<p> $i_con_msg_date_wrong </p>\n";

$no_file = 5;
/*
echo generateFileUploadNameHandler("form1","filea1","hidden_userfile_name1"
,"filea2","hidden_userfile_name2"
,"filea3","hidden_userfile_name3"
,"filea4","hidden_userfile_name4"
,"filea5","hidden_userfile_name5"
);
*/
if ($plugin['power_voice'])
{	
	$RecBtn = "<input type='button' name='pv_rec_btn' value='".$iPowerVoice['powervoice']."' onclick='editPVoice()' />";
	$ClearBtn = "<input type='button' name='pv_clear_btn' value='".$button_clear."' onclick='clearPVoice()' />";
	$HiddenHtml = "<input type=\"hidden\" name=\"voiceFile\" ><input type=\"hidden\" name=\"voicePath\" >";
	$HiddenHtml .= "<input type=\"hidden\" name=\"voiceFileFromWhere\" value=\"1\" >";
	$VoiceClip = " <img src='{$image_path}/{$LAYOUT_SKIN}/icon_voice.gif' border='0' align='absmiddle' /> ";
			
	$voiceContent .= "<table cellpadding=\"0\" cellspacing=\"0\" >";
	$voiceContent .= "<tr>";
	$voiceContent .= "	<td><span id=\"voiceDiv\" ></span></td>";
	$voiceContent .= "	<td>{$RecBtn}{$HiddenHtml}</td>";
	$voiceContent .= "	<td><span id=\"clearDiv\" style=\"display:none\" >{$ClearBtn}</span></td>";
	$voiceContent .= "</tr>";
	$voiceContent .= "</table>";	
	
	$PVHtml = "<tr>";
	$PVHtml .= "<td align='right' nowrap='nowrap' width='100'>".$iPowerVoice['sound_recording'].":</td>";
	$PVHtml .= "<td>".$voiceContent."</td>";
	$PVHtml .= "</tr>";		
}

?>

<script language="javascript">
var no_of_upload_file = <? echo $no_file==""?5:$no_file;?>;
function checkform(obj)
{
        if(!check_text(obj.AnnouncementDate, "<?php echo $i_alert_pleasefillin.$i_AnnouncementDate; ?>.")) return false;
        if(!check_date(obj.AnnouncementDate, "<?php echo $i_invalid_date; ?>.")) return false;
        if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_AnnouncementTitle; ?>.")) return false;
        checkOptionAll(obj.elements["GroupID[]"]);

        return Big5FileUploadHandler();
}
function Big5FileUploadHandler() {
	
	 for(i=0;i<=no_of_upload_file;i++)
	 {
		 	objFile = eval('document.form1.filea'+i);
		 	objUserFile = eval('document.form1.hidden_userfile_name'+i);
		 	if(objFile!=null && objFile.value!='' && objUserFile!=null)
		 	{
			 	var Ary = objFile.value.split('\\');
			 	objUserFile.value = Ary[Ary.length-1];
			}
	 }
	 return true;
}

function add_field()
{
	var table = document.getElementById("upload_file_list");
	var row = table.insertRow(no_of_upload_file);
	if (document.all)
	{
		var cell = row.insertCell(0);
		x= '<input class="file" type="file" name="filea'+no_of_upload_file+'" size="40">';
		x+='<input type="hidden" name="hidden_userfile_name'+no_of_upload_file+'">';

		cell.innerHTML = x;
		no_of_upload_file++;
	}
}

<?php
if ($plugin['power_voice'])
{ 
?>
	var NewRecordBefore = false;
	
	function listenPVoice2(fileName)
	{		
		newWindow("http://<?=$eclass_httppath?>/src/tool/powervoice/pvoice_listen_now_intranet2.php?from_intranet=1&location="+fileName, 18);
	}	
	
	function editPVoice()
	{
		var fileName = document.form1.voiceFile.value;		
		
		newWindow("http://<?=$eclass_httppath?>/src/tool/powervoice/index.php?from_intranet=1&fileName="+fileName, 17);
	}	
	
	function clearPVoice(num)
	{		
		var voiceObj = document.getElementById("voiceDiv");
		var clearObj = document.getElementById("clearDiv");
		
		voiceObj.innerHTML = "";
		document.form1.voiceFile.value = "";		
		clearObj.style.display = "none";
	}

	function file_exists()
	{
		alert("<?=$classfiles_alertMsgFile12?>");
	}
	
	function file_saved(fileName,filePath,fileHttpPath,fileEmpty)
	{		
		var voiceObj = document.getElementById("voiceDiv");
		var clearObj = document.getElementById("clearDiv");
		
		if (fileName != "")
		{
			if ((fileHttpPath != undefined) &&  (fileHttpPath != ""))
				voiceObj.innerHTML = "<a href=\"javascript:listenPVoice2('"+fileHttpPath+"')\" ><?=$VoiceClip?>"+fileName+"</a>&nbsp;&nbsp;";
			else				
				voiceObj.innerHTML = fileName+"&nbsp;&nbsp;";
			if (fileEmpty=="1")
			{
				document.form1.is_empty_voice.value = 1;
			}
			else
			{
				NewRecordBefore = true;
				document.form1.is_empty_voice.value = "";
			}
				
			document.form1.voiceFile.value = fileName;
			document.form1.voicePath.value = filePath;
			clearObj.style.display = "block";
		}
	}
		
<?php
} 
?>


</script>

<form name="form1" action="new_update.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_admintitle_im_announcement, 'javascript:history.back()', $button_new, '') ?>
<?= displayTag("head_announcement_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width="500" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align="right" nowrap="nowrap" width="100"><?php echo $i_AnnouncementDate; ?>:</td>
	<td align=left width="400">
	<input class="text" type="text" name="AnnouncementDate" size="10" maxlength="10" value="<?php echo ($ad==""?date("Y-m-d"):$ad); ?>"> 
	<span class="extraInfo">(yyyy-mm-dd)</span>
	</td>
</tr>
<tr>
	<td align="right" nowrap="nowrap" width="100"><?php echo $i_AnnouncementEndDate; ?>:</td>
	<td><input class="text" type="text" name="EndDate" size="10" maxlength="10" value="<?php echo ($ed==""?date("Y-m-d"):$ed); ?>"> 
	<span class="extraInfo">(yyyy-mm-dd)</span>
	</td>
</tr>
<tr>
	<td align="right" nowrap="nowrap" width="100" ><?php echo $i_AnnouncementTitle; ?>:</td>
	<td><input class="text" type="text" name="Title" value="<?=$t?>" size="30" maxlength="100" ></td>
</tr>
<tr>
	<td align="right" nowrap="nowrap" width="100" ><?php echo $i_AnnouncementDescription; ?>:</td>
	<td><textarea name="Description" cols="60" rows="10" ><?=$d?></textarea></td>
</tr>
<?=$PVHtml?>

<tr>
	<td align="right" nowrap="nowrap"  width="100" ><?php echo $i_AnnouncementAttachment; ?>:</td>
	<td>
	<!-- create upload file input box -->
	<table border="0" id="upload_file_list" cellpadding="0" cellspacing="0" >
	<script language="javascript">
	for(i=0;i<no_of_upload_file;i++)
	{
		document.writeln('<tr><td><input class="file" type="file" name="filea'+i+'" size="40">');
	    document.writeln('<input type="hidden" name="hidden_userfile_name'+i+'"></td></tr>');
	}
	</script>
	<? /*
	<?php for($i=1;$i<=$no_file;$i++){ ?><input class=file type=file name="filea<?php echo $i; ?>" size=20><br>
	<input type=hidden name="hidden_userfile_name<?=$i?>">
	<?php } ?>
	<input type=button value="+" onClick="add_field(1)">
	*/
	?>
	</table>
	<input type=button value=" + " onClick="add_field()">
	<!-- end of upload file input box -->
	</td>
</tr>

<tr><td align=right nowrap width=100><?php echo $i_AnnouncementRecordStatus; ?>:</td><td><input type=radio name=RecordStatus value=1 CHECKED> <?php echo $i_status_publish; ?> <input type=radio name=RecordStatus value=0> <?php echo $i_status_pending; ?></td></tr>
<tr><td width=100><br></td><td><input type=checkbox name=email_alert value=1> <?php echo $i_AnnouncementAlert; ?></td></tr>
<?/* if (isset($plugin['sms']) && $plugin['sms']) { ?>
<tr><td width=100><br></td><td><input type=checkbox name=sms_alert value=1> <?php echo $i_AnnouncementAlert_SMS; ?><br>
<?=$i_SMS_Cautions?>
<?php
   echo "<ul>";
   for ($i=1; $i<=4; $i++)
   {
        $stmt = ${"i_SMS_Limitation$i"};
        echo "<li>$stmt</li>\n";
   }
   echo "</ul>";
?>
<br></td></tr>
<?php } */?>
<?php if ($special_announce_public_allowed) {?>
<tr><td width=100><br></td><td><input type=checkbox name=publicdisplay value=1> <?php echo $i_AnnouncementPublic; ?></td></tr>
<?php } ?>
<tr><td colspan=2><?=$i_AnnouncementPublicInstruction?> </tr>
<tr><td align="right"><?=$i_admintitle_group?>:</td>
<td><?php echo $lo->displayAnnouncementGroups(); ?></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type="hidden" name='attachment_size' value='<? echo $no_file==""?5:$no_file;?>'>
<input type="hidden" name="is_empty_voice" value='' />

</form>
<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>