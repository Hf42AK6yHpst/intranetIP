<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/liblunchbox.php");
intranet_opendb();


# Checking
if (!isset($targetDate) || $targetDate == "")
{
    header("Location: index.php");
    exit();
}

if (!is_array($StudentID) || sizeof($StudentID)==0)
{
     header("Location: new.php?targetDate=$targetDate");
     exit();
}

$curr_year = date('Y');
$curr_month = date('m');
$curr_day = date('d');

$targetTS = strtotime($targetDate);
$targetYear = date('Y',$targetTS);
$targetMonth = date('m',$targetTS);
$targetDay = date('d',$targetTS);


$targetPassed = false;
if ($curr_year > $targetYear)
{
    $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month > $targetMonth)
{
     $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month == $targetMonth && $curr_day > $targetDay)
{
     $targetPassed = true;
}

if ($targetPassed)
{
    header("Location: new.php?targetDate=$targetDate");
    exit();
}

$llunchbox = new liblunchbox();


$ticket_table_name = $llunchbox->createMonthlyTicketTable($targetYear, $targetMonth);
$student_list = implode(",",$StudentID);

# Remove old records (Status not taken)
$sql = "DELETE FROM $ticket_table_name WHERE StudentID IN ($student_list) AND RecordStatus = 0 AND DayNumber = $targetDay";
$llunchbox->db_db_query($sql);

# Insert new records
$values = "";
$delim = "";
for ($i=0; $i<sizeof($StudentID); $i++)
{
$t_id = $StudentID[$i];
$values .= "$delim ($t_id, $targetDay, 0,0,now(),now())";
$delim = ",";
}
$sql = "INSERT IGNORE INTO $ticket_table_name (StudentID, DayNumber, RecordType, RecordStatus, DateInput, DateModified)
       VALUES $values";
$llunchbox->db_db_query($sql);


header("Location: index.php?targetDate=$targetDate&msg=2");
intranet_closedb();

?>
