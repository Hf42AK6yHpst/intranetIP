<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/liblunchbox.php");
intranet_opendb();


# Checking
if ($targetYear == "" || $targetMonth == "")
{
    header("Location: index.php");
    exit();
}

if (!is_array($StudentID) || sizeof($StudentID)==0)
{
     header("Location: index.php?targetYear=$targetYear&targetMonth=$targetMonth");
     exit();
}

$curr_year = date('Y');
$curr_month = date('m');

$targetPassed = false;
if ($curr_year > $targetYear)
{
    $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month > $targetMonth)
{
     $targetPassed = true;
}

if ($targetPassed)
{
    header("Location: index.php?targetYear=$targetYear&targetMonth=$targetMonth");
    exit();
}

$llunchbox = new liblunchbox();

$ticket_table_name = $llunchbox->createMonthlyTicketTable($targetYear, $targetMonth);
$student_list = implode(",",$StudentID);

# Remove old records (Status not taken)
$sql = "DELETE FROM $ticket_table_name WHERE StudentID IN ($student_list) AND RecordStatus = 0";
$llunchbox->db_db_query($sql);

header("Location: index.php?targetYear=$targetYear&targetMonth=$targetMonth&msg=3");

?>
