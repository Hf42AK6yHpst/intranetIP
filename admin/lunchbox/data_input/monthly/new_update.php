<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/liblunchbox.php");
intranet_opendb();


# Checking
if ($targetYear == "" || $targetMonth == "")
{
    header("Location: index.php");
    exit();
}

if (!is_array($StudentID) || sizeof($StudentID)==0)
{
     header("Location: new.php?targetYear=$targetYear&targetMonth=$targetMonth");
     exit();
}

$curr_year = date('Y');
$curr_month = date('m');

$targetPassed = false;
if ($curr_year > $targetYear)
{
    $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month > $targetMonth)
{
     $targetPassed = true;
}

if ($targetPassed)
{
    header("Location: new.php?targetYear=$targetYear&targetMonth=$targetMonth");
    exit();
}

$llunchbox = new liblunchbox();


$array_set_days = $llunchbox->getCalendarDates($targetYear, $targetMonth);
if ($array_set_days===false || !is_array($array_set_days))
{
    $hasData = false;
}
else
{
    $hasData = true;
}

if ($hasData)
{
    $ticket_table_name = $llunchbox->createMonthlyTicketTable($targetYear, $targetMonth);
    $student_list = implode(",",$StudentID);

    # Remove old records (Status not taken)
    $sql = "DELETE FROM $ticket_table_name WHERE StudentID IN ($student_list) AND RecordStatus = 0";
    $llunchbox->db_db_query($sql);

    $db_day_string = "";
    $delim = "";
    for ($j=0; $j<sizeof($array_set_days); $j++)
    {
         $db_day_string .= "$delim([StudentID], '".$array_set_days[$j]."', 0, 0, now(), now())";
         $delim = ",";
    }
    # Insert new records
    $values = "";
    $delim = "";
    for ($i=0; $i<sizeof($StudentID); $i++)
    {
         $target_string = str_replace("[StudentID]",$StudentID[$i],$db_day_string);
         $values .= "$delim ".$target_string;
         $delim = ",";
    }
    $sql = "INSERT IGNORE INTO $ticket_table_name (StudentID, DayNumber, RecordType, RecordStatus, DateInput, DateModified)
                   VALUES $values";
    $llunchbox->db_db_query($sql);
}

header("Location: index.php?targetYear=$targetYear&targetMonth=$targetMonth&msg=2");

?>
