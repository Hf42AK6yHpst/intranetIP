<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$curr_year = date('Y');
$curr_month = date('m');

if (isset($targetYear) && isset($targetMonth) && $targetYear != "" && $targetMonth != "")
{
    $targetSelected = true;
    $selectedYear = $targetYear;
    $selectedMonth = $targetMonth;
}
else
{
    $targetSelected = false;
    $selectedYear = $curr_year;
    $selectedMonth = $curr_month;

    # Add 1
    $selectedMonth++;
    if ($selectedMonth == 13)
    {
        $selectedYear++;
        $selectedMonth = 1;
    }
}

# Year Selection
$select_year = "<SELECT name=targetYear>\n";
for ($i=-1; $i<=1; $i++)
{
     $temp_year = $selectedYear + $i;
     $select_year .= "<OPTION value=\"$temp_year\" ".($i==0?"SELECTED":"").">$temp_year</OPTION>\n";
}
$select_year .= "</SELECT>\n";

$select_month = "<SELECT name=targetMonth>\n";
for ($i=0; $i<sizeof($i_general_MonthShortForm); $i++)
{
     $temp_value = $i+1;
     $temp_string = $i_general_MonthShortForm[$i];
     $select_month .= "<OPTION value=\"$temp_value\" ".($temp_value==$selectedMonth?"SELECTED":"").">$temp_string</OPTION>\n";
}
$select_month .= "</SELECT>\n";

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SmartCard_Lunchbox_System, '../../', $i_SmartCard_Lunchbox_Menu_DataInput, '../', $i_SmartCard_Lunchbox_DataInput_Month, '') ?>
<?= displayTag("head_lunchbox_$intranet_session_language.gif", $msg) ?>


<form name="form_change" method="get">
<table width=300 align=center border=0 cellspacing=3 cellpadding=2>
<tr><td align=center><?=$select_year?> <?=$select_month?> <input type=image src="<?=$image_path?>/admin/button/s_btn_continue_<?=$intranet_session_language?>.gif" border=0></td></tr>
</table>
</form>

<? if ($targetSelected) {
$llunchbox = new liblunchbox();
$lc = new libclass();

?>
<hr width=85% align=center>
<div align=center><?=$targetYear?> - <?=$i_general_MonthShortForm[$targetMonth-1]?></div>
<form name=form1 method=GET action="">
<?
$targetPassed = false;
if ($curr_year > $targetYear)
{
    $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month > $targetMonth)
{
     $targetPassed = true;
}

if ($targetPassed)
{
    echo "<div align=center><font color=red size=+1>$i_SmartCard_Lunchbox_Warning_CannotEditPrevious</font></div>";
    $checkbox_string = "disabled";

}
else
{
    $array_set_days = $llunchbox->getCalendarDates($targetYear, $targetMonth);
    if ($array_set_days===false || !is_array($array_set_days))
    {
        $hasData = false;
    }
    else
    {
        $hasData = true;
    }
    if (!$hasData)
    {
         echo "<div align=center><font color=red size=+1>$i_SmartCard_Lunchbox_Warning_MonthlyCalendarNotExist</font></div>";
    }
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;

# SQL stmt
$namefield = getNamefieldByLang("u.");
$ticket_table_name = $llunchbox->createMonthlyTicketTable($targetYear, $targetMonth);

if ($targetClassID != "")
{
    $targetClass = $lc->getClassName($targetClassID);
    $conds = " AND u.ClassName = '$targetClass'";
}

$sql  = "SELECT
               $namefield, u.ClassName, u.ClassNumber, COUNT(t.RecordID) as ticket_num
               ".($targetPassed? "":",CONCAT('<input type=checkbox name=StudentID[] value=', u.UserID,' $checkbox_string>')")."
                FROM
                    $ticket_table_name as t
                    LEFT OUTER JOIN INTRANET_USER as u ON t.StudentID = u.UserID AND u.RecordType = 2
                WHERE
                        (u.EnglishName like '%$keyword%' OR
                         u.ChineseName like '%$keyword%' OR
                         u.UserLogin like '%$keyword%' OR
                         u.ClassName like '%$keyword%' OR
                         u.ClassNumber like '%$keyword%'
                         )
                        $conds
                GROUP BY t.StudentID
                ";



# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("u.EnglishName", "u.ClassName", "u.ClassNumber", "ticket_num");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
if (!$targetPassed)
{
     $li->no_col++;
}
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=40% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_SmartCard_Lunchbox_DataField_NumTickets)."</td>\n";
if (!$targetPassed)
{
     $li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentID[]")."</td>\n";
}

if ($hasData)
{
    $toolbar = "<a class=iconLink href=\"javascript:checkGet(document.form1,'new.php')\">".newIcon()."$i_SmartCard_Lunchbox_Action_AddStudent</a>";
    $toolbar .= "&nbsp;<a class=iconLink href=\"javascript:checkGet(document.form1,'import.php')\">".importIcon()."$button_import</a>";
    $toolbar .= "&nbsp;<a class=iconLink href=\"javascript:checkGet(document.form1,'import_last.php')\">".importIcon()."$i_SmartCard_Lunchbox_Import_From_LastMonth</a>";
}

$classes = $lc->getClassList();
$select_class = getSelectByArray($classes,"name=targetClassID onChange=this.form.submit()",$targetClassID,1);

if (!$targetPassed)
{
     $functionbar .= " <a href=\"javascript:checkRemove(document.form1,'StudentID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
}
$searchbar = $select_class;
$searchbar .= " <input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= " <a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=<?=$image_path?>/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>


<input type=hidden name=targetYear value="<?=$targetYear?>">
<input type=hidden name=targetMonth value="<?=$targetMonth?>">
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<? } ?>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
