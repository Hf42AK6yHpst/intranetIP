<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();


$llunchbox = new liblunchbox();
$success = $llunchbox->copyMonthData($targetYear, $targetMonth, $lastYear, $lastMonth);

intranet_closedb();

header("Location: index.php?targetYear=$targetYear&targetMonth=$targetMonth&msg=3");
?>