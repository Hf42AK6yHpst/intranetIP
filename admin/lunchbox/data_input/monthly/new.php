<?php

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$curr_year = date('Y');
$curr_month = date('m');

if (isset($targetYear) && isset($targetMonth) && $targetYear != "" && $targetMonth != "")
{
}
else
{
    $targetYear = $curr_year;
    $targetMonth = $curr_month;

    # Add 1
    $selectedMonth++;
    if ($selectedMonth == 13)
    {
        $selectedYear++;
        $selectedMonth = 1;
    }
}


if ( ($targetType==1 && isset($targetClass) && $targetClass!="") || ($targetType==2 && isset($targetClassLvl) && $targetClassLvl!="" ) )
{
      $targetSelected = true;
}
else
{
    $targetSelected = false;
}

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SmartCard_Lunchbox_System, '../../', $i_SmartCard_Lunchbox_Menu_DataInput, '../', $i_SmartCard_Lunchbox_DataInput_Month, 'index.php', $i_SmartCard_Lunchbox_Action_AddStudent, '') ?>
<?= displayTag("head_lunchbox_$intranet_session_language.gif", $msg) ?>

<?

$lc = new libclass();
if (!$targetSelected) {

$sql = "SELECT ClassLevelID, LevelName FROM INTRANET_CLASSLEVEL WHERE RecordStatus = 1 ORDER BY LevelName";
$levels = $lc->returnArray($sql,2);
$classes = $lc->getClassList();
$select_level = getSelectByArray($levels, "name=targetClassLvl onChange=targetType[1].checked=true");
$select_class = getSelectByArray($classes,"name=targetClass onChange=targetType[0].checked=true");

?>
<form name=form1 action="" method=GET>
<table width=300 align=center border=0 cellspacing=3 cellpadding=2>
<tr><td align=right><input type=radio name=targetType value=1 CHECKED></td><td><?=$i_ClassName?> : <?=$select_class?></td></tr>
<tr><td align=right><input type=radio name=targetType value=2></td><td><?=$i_ClassLevel?> : <?=$select_level?></td></tr>
<tr><td align=right></td><td><input type=image src="<?=$image_path?>/admin/button/s_btn_continue_<?=$intranet_session_language?>.gif" border=0></td></tr>

</table>

<input type=hidden name=targetYear value="<?=$targetYear?>">
<input type=hidden name=targetMonth value="<?=$targetMonth?>">
</form>
<?

}
else
{

$llunchbox = new liblunchbox();

# Class or Class Level
$class_name_list = "";
if ($targetType==1)
{

    $target_string = $lc->getClassName($targetClass);
    $class_name_list = "'$target_string'";
}
else
{
    $classes = $lc->returnClassListByLevel($targetClassLvl);
    $delim = "";
    $target_string = "";
    for ($i=0; $i<sizeof($classes); $i++)
    {
         list($t_id, $t_name) = $classes[$i];
         $target_string .= "$delim $t_name";
         $class_name_list .= "$delim '$t_name'";
         $delim = ",";
    }

}

?>
<hr width=85% align=center>
<div align=center><?=$targetYear?> - <?=$i_general_MonthShortForm[$targetMonth-1]?><br>
<?=$i_SmartCard_ClassName?> : <?=$target_string?>
<br>
<a class=functionlink_new href=javascript:history.back()><?=$i_StudentAttendance_SelectAnotherClass?></a></div>



<form name=form1 method=POST action="new_update.php">
<?

$targetPassed = false;
if ($curr_year > $targetYear)
{
    $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month > $targetMonth)
{
     $targetPassed = true;
}

if ($targetPassed)
{
    echo "<div align=center><font color=red size=+1>$i_SmartCard_Lunchbox_Warning_CannotEditPrevious</font></div>";
    $checkbox_string = "disabled";

}
else
{
    $array_set_days = $llunchbox->getCalendarDates($targetYear, $targetMonth);
    if ($array_set_days===false || !is_array($array_set_days))
    {
        $hasData = false;
    }
    else
    {
        $hasData = true;
    }
}


if ($hasData)
{
    # Show student list
    # Get student list
    $namefield = getNamefieldByLang("u.");
    $sql  = "SELECT
                   u.UserID, $namefield, u.ClassName, u.ClassNumber
                 FROM
                    INTRANET_USER as u
                WHERE
                     u.RecordType = 2 AND u.ClassName IN ($class_name_list)
                     AND u.RecordStatus IN (0,1,2)
                ORDER BY u.ClassName, u.ClassNumber, u.EnglishName
                ";
    $student_data = $llunchbox->returnArray($sql,4);

    # Get tickets number
    $array_tickets = $llunchbox->getMonthlyTicketsNumAssocArray($targetYear, $targetMonth);
    ?>
<table width=90% align=center border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=1 cellspacing=1>
<tr class=tableTitle>
<td><?=$i_UserStudentName?></td>
<td><?=$i_UserClassName?></td>
<td><?=$i_UserClassNumber?></td>
<td><?=$i_SmartCard_Lunchbox_DataField_NumTickets?></td>
<td><input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'StudentID[]'):setChecked(0,this.form,'StudentID[]')></td></td>
</tr>

<?
$table_display = "";
for ($i=0; $i<sizeof($student_data); $i++)
{
     list($t_id, $t_name, $t_class, $t_class_num) = $student_data[$i];
     $t_ticket_num = $array_tickets[$t_id]+0;
     $css = ($i%2?"":"2");
     $table_display .= "<tr class=tableContent$css>";
     $table_display .= "<td>$t_name</td><td>$t_class</td><td>$t_class_num</td><td>$t_ticket_num</td>
     <td><input type=checkbox name=StudentID[] value=\"$t_id\" ".($t_ticket_num!=0?"CHECKED":"")."></td>
     </tr>\n";
}
echo $table_display;
?>

</table>


    <?

}
else
{
    echo "<div align=center><font color=red size=+1>$i_SmartCard_Lunchbox_Warning_MonthlyCalendarNotExist</font></div>";
}
?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<? if (!$targetPassed) { ?>
<a href=javascript:checkAlert(document.form1, 'StudentID[]', 'new_update.php','<?=$i_SmartCard_Lunchbox_Warning_Save?>')><input type='image' onClick="return confirm('<?=$i_SmartCard_Lunchbox_Warning_Save?>')" src='<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0'></a>

<?=btnReset() ?>
<? } ?>
</td>
</tr>
</table>

<input type=hidden name=targetYear value="<?=$targetYear?>">
<input type=hidden name=targetMonth value="<?=$targetMonth?>">
</form>

<?

} ?>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
