<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$llunch = new liblunchbox();
$lclass = new libclass();

$curr_date = date('Y-m-d');

$curr_year = date('Y');
$curr_month = date('m');


$borderLine = "style='border-top:1px #888888 solid;'";

if (!isset($targetYear) || $targetYear == "")
{
     $targetYear = $curr_year;
}
if (!isset($targetMonth) || $targetMonth == "")
{
     $targetMonth = $curr_month;
}


# Check date
$targetPassed = false;
$isToday = false;
if ($curr_year > $targetYear)
{
    $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month >= $targetMonth)
{
     $targetPassed = true;
}

if (!$targetPassed)
{
     die("Future Report is not available");
}


# Handle classes parameters
$class_conds = "";
$class_string = "";
if (!is_array($targetClass) || sizeof($targetClass)==0)
{
     $isAllClass = true;
}

if (!$isAllClass)
{
     $class_list = implode(",", $targetClass);
     $sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassID IN ($class_list) AND RecordStatus = 1";
     $temp = $llunch->returnVector($sql);
     $class_name_list = "'".implode("','", $temp)."'";
     $class_conds = " AND ClassName IN ($class_name_list)";
     $class_string = implode(", ", $temp);
}
else
{
}

$student_conds = "";
if ($class_conds != "")
{
    $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) $class_conds";
    $target_students = $llunch->returnVector($sql);

    $student_conds = " AND StudentID IN (".implode(",", $target_students).")";
}
else
{
}


$ticket_table_name = $llunch->createMonthlyTicketTable($targetYear, $targetMonth);

# Different Types of report
$report_content = "";
$date_string = "$targetYear - ".$i_general_MonthShortForm[$targetMonth-1];
if ($ReportType == 1)      # Day Breakdown
{
    $report_title = "$i_SmartCard_Lunchbox_System - $i_SmartCard_Lunchbox_Report_Type_DayBreakdown : $date_string";
    $report_content .= "$i_SmartCard_Lunchbox_DateField_Date: $date_string<br>\n";
    if (!$isAllClass)
    {
        $report_content .= "$i_ClassName: $class_string<br>\n";
    }
    $report_content .= "<br>\n";

    # Retrieve Counts
    $sql = "SELECT DayNumber, COUNT(RecordID) FROM $ticket_table_name
                   WHERE RecordStatus = 0 $student_conds
                   GROUP BY DayNumber
                   ORDER BY DayNumber";
    $temp = $llunch->returnArray($sql,2);
    $array_untaken = build_assoc_array($temp);

    $sql = "SELECT DayNumber, COUNT(RecordID) FROM $ticket_table_name
                   WHERE RecordStatus = 1 $student_conds
                   GROUP BY DayNumber
                   ORDER BY DayNumber";
    $temp = $llunch->returnArray($sql,2);
    $array_taken = build_assoc_array($temp);

    # Retrieve Days
    $sql = "SELECT DISTINCT DayNumber FROM $ticket_table_name
                   WHERE 1 $student_conds ORDER BY DayNumber";
    $array_days = $llunch->returnVector($sql);

    # Table Display
    $report_content .= "<table width=80% cellspacing=0 cellpadding=2 border=0 style='border:1px black solid;'>\n";
    $report_content .= "<tr class=tableTitle><td>$i_SmartCard_Lunchbox_DateField_Date</td><td>$i_SmartCard_Lunchbox_Status_NotTaken</td><td>$i_SmartCard_Lunchbox_Status_Taken</td><td>$list_total</td></tr>\n";

    $total_taken = 0;
    $total_untaken = 0;
    for ($i=0; $i<sizeof($array_days); $i++)
    {
         $t_day = $array_days[$i];
         $t_untaken = $array_untaken[$t_day]+0;
         $t_taken = $array_taken[$t_day]+0;
         $t_total = $t_untaken + $t_taken;

         $total_untaken += $t_untaken;
         $total_taken += $t_taken;

         # Display
         $css = ($i%2?"":"2");
         $report_content .= "<tr class=tableContent$css><td $borderLine>$t_day</td><td $borderLine>$t_untaken</td><td $borderLine>$t_taken</td><td $borderLine>$t_total</td></tr>\n";

    }
    $report_content .= "<tr class=tableTitle><td $borderLine>$list_total</td><td $borderLine>$total_untaken</td><td $borderLine>$total_taken</td><td $borderLine>".($total_untaken+$total_taken)."</td></tr>\n";
    $report_content .= "</table>\n";

}
else if ($ReportType == 2)       # Top untaken list
{
     # Taken & untaken list
     $report_content = "";
     if ($targetTop <= 0) $targetTop = 10;

     $report_content .= $i_SmartCard_Lunchbox_Report_Type_TopUntaken;
     $report_title = "$i_SmartCard_Lunchbox_System - $i_SmartCard_Lunchbox_Report_Type_TopUntaken : $date_string";
     $report_content .= "<br><br>\n";

     # Get the last count first
     $sql = "SELECT StudentID, COUNT(RecordID) as untaken_count
                    FROM $ticket_table_name
                    WHERE RecordStatus = 0
                    $student_conds
                    GROUP BY StudentID
                    ORDER BY untaken_count DESC
                    LIMIT 0,$targetTop
                    ";
     $temp = $llunch->returnArray($sql,2);
     if (sizeof($temp)==0)
     {
         $report_content .= "<div class=no_valid_data align=center>$i_SmartCard_Lunchbox_Report_Words_NoUntaken</div>\n";
     }
     else
     {
         # Table Display
         $report_content .= "<table width=80% cellspacing=0 cellpadding=2 border=0 style='border:1px black solid;'>\n";
         $report_content .= "<tr class=tableTitle><td>#</td><td>$i_SmartCard_ClassName</td><td>$i_UserClassNumber</td><td>$i_UserStudentName</td><td>$i_SmartCard_Lunchbox_Report_Field_Untaken_Times</td><td>$i_SmartCard_Lunchbox_Report_Field_Untaken_Days</td></tr>\n";

         $last_count = $temp[sizeof($temp)-1][1];
         $namefield = getNameFieldByLang();
         $sql = "SELECT StudentID, COUNT(RecordID) as untaken_count
                        FROM $ticket_table_name
                        WHERE RecordStatus = 0
                        $student_conds
                        GROUP BY StudentID
                        ORDER BY untaken_count DESC
                    ";

         $array_top_students = $llunch->returnArray($sql);
         $prev_count = 0;
         $pos = 1;
         for ($i=0; $i<sizeof($array_top_students); $i++)
         {
              list($t_student_id , $t_count) = $array_top_students[$i];
              if ($t_count < $last_count) break;
              # Get Student Info
              $sql = "SELECT $namefield, ClassName, ClassNumber
                             FROM INTRANET_USER WHERE UserID = $t_student_id";
              $info = $llunch->returnArray($sql,3);
              list($t_name, $t_class, $t_classnum) = $info[0];

              # Get Days List
              $sql = "SELECT DayNumber FROM $ticket_table_name
                             WHERE RecordStatus = 0 AND StudentID = '$t_student_id'
                             ORDER BY DayNumber";
              $days = $llunch->returnVector($sql);
              $day_list = implode(",", $days);
              $css = ($i%2?"":"2");
              # Position
				$pos = $i+1;
              $report_content .= "<tr class=tableContent$css>
                                    <td $borderLine>$pos</td>
                                    <td $borderLine>$t_class</td>
                                    <td $borderLine>$t_classnum</td>
                                    <td $borderLine>$t_name</td>
                                    <td $borderLine>$t_count</td>
                                    <td $borderLine>$day_list</td>
                                  </tr>\n";

         }

     }

     $report_content .= "</table>\n";

} else if ($ReportType == 4)
{
     $namefield = getNameFieldByLang("b.");

     $report_content = $i_SmartCard_Lunchbox_Report_Type_StudentList."<br />\n";
     $report_content .= "$i_SmartCard_Lunchbox_DateField_Date: $date_string<br>\n";

     $report_content .= "<br /><br />\n";

	$isSplitClass = true;
     if ($isSplitClass)    # Split Class in Display
     {
         if ($isAllClass)
         {
             $classes = $lclass->getClassList();
             unset($targetClass);
             for ($i=0; $i<sizeof($classes); $i++)
             {
                  $targetClass[] = $classes[$i][0];
             }
         }
         $delim = "";
          for ($i=0; $i<sizeof($targetClass); $i++)
          {
	         $TotalTaken = 0;
	         $TotalNotTaken = 0;
          		unset($t_result);
               $t_class_id = $targetClass[$i];
               $t_class_name = $lclass->getClassName($t_class_id);
               $sql = "SELECT $namefield, b.ClassName, b.ClassNumber, a.RecordStatus, a.StudentID
                              FROM $ticket_table_name as a
                                   LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                       WHERE b.ClassName = '$t_class_name'" .
                       " ORDER BY b.ClassName, b.ClassNumber, a.StudentID";
               $row_raw = $llunch->returnArray($sql, 5);

               for ($kk=0; $kk<sizeof($row_raw); $kk++)
               {
	               	$t_result[$row_raw[$kk][4]][0] = $row_raw[$kk][0];
	               	$t_result[$row_raw[$kk][4]][1] = $row_raw[$kk][1];
	               	$t_result[$row_raw[$kk][4]][2] = $row_raw[$kk][2];
	               	if ($row_raw[$kk][3]==1)
	               	{
	               		$t_result[$row_raw[$kk][4]][3] ++;
	               	} else
	               	{
	               		$t_result[$row_raw[$kk][4]][4] ++;
	               	}
               }

               $report_content .= "$delim$i_UserClassName : $t_class_name <br>\n";
               if (sizeof($t_result)==0)
               {
                   $report_content .= "<div class=no_valid_data align=center>$i_SmartCard_Lunchbox_Report_Words_NoTicket</div>\n";
               }
               else
               {
                   $report_content .= "<table width=80% align=center border=0 style='border:1px black solid;' cellspacing=0 cellpadding=2 align=center>\n";
                   $report_content .= "<tr class=tableTitle><td>$i_UserClassNumber</td><td>$i_UserStudentName</td><td>$i_SmartCard_Lunchbox_Status_Taken</td><td>$i_SmartCard_Lunchbox_Status_NotTaken</td></tr>\n";
                   $j_pos = 0;
                   for ($j=0; $j<sizeof($row_raw); $j++)
                   {
                   		if (!$StdFlag[$row_raw[$j][4]])
                   		{
                   			$j_pos ++;
	                        list($tj_name, $tj_class, $tj_classnum, $tj_Taken, $tj_NoTake) = $t_result[$row_raw[$j][4]];
	                        $tj_Taken = (int) $tj_Taken;
	                        $tj_NoTake = (int) $tj_NoTake;
	                        $css = ($j_pos%2?"":"2");
	                       // debug($j, $css);
	                        $report_content .= "<tr class=tableContent$css>";
	                        $report_content .= "<td {$borderLine}>$tj_classnum</td><td {$borderLine}>$tj_name</td>";
	                        $report_content .= "<td {$borderLine}>$tj_Taken</td>";
	                        $report_content .= "<td {$borderLine}>$tj_NoTake</td>";
	                        $report_content .= "</tr>\n";
	                        $StdFlag[$row_raw[$j][4]] = true;
	                        $TotalTaken += $tj_Taken;
	                        $TotalNotTaken += $tj_NoTake;
                   		}
                   }
	               $report_content .= "<tr class=tableContent$css>";
	               $report_content .= "<td {$borderLine}>&nbsp;</td><td {$borderLine} align='right'>{$list_total} &nbsp;</td>";
	               $report_content .= "<td {$borderLine}>$TotalTaken</td>";
	               $report_content .= "<td {$borderLine}>$TotalNotTaken</td>";
	               $report_content .= "</tr>\n";
                   $report_content .= "</table>\n";
               }
               $delim = "<hr width=85%>\n";
          }

     }
}
else
{
    echo "Error. Please select Report Type.";
    die();
}


# Display Report
include("../report_header.php");

echo $report_content;

include("../report_footer.php");

intranet_closedb();
?>
