<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lc = new libclass();
$classList = $lc->getClassList();

$selectedYear = date('Y');
$selectedMonth = date('m');
# Year Selection
$select_year = "<SELECT name=targetYear>\n";
for ($i=-4; $i<=1; $i++)
{
     $temp_year = $selectedYear + $i;
     $select_year .= "<OPTION value=\"$temp_year\" ".($i==0?"SELECTED":"").">$temp_year</OPTION>\n";
}
$select_year .= "</SELECT>\n";

$select_month = "<SELECT name=targetMonth>\n";
for ($i=0; $i<sizeof($i_general_MonthShortForm); $i++)
{
     $temp_value = $i+1;
     $temp_string = $i_general_MonthShortForm[$i];
     $select_month .= "<OPTION value=\"$temp_value\" ".($temp_value==$selectedMonth?"SELECTED":"").">$temp_string</OPTION>\n";
}
$select_month .= "</SELECT>\n";

$select_top = "<SELECT name=targetTop>\n";
for ($i=1; $i<=20; $i++)
{
     $select_top .= "<OPTION value=$i ".($i==10?"SELECTED":"").">$i</OPTION>\n";
}
$select_top .= "</SELECT>\n";

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SmartCard_Lunchbox_System, '../../', $i_SmartCard_Lunchbox_Menu_Report, '../', $i_SmartCard_Lunchbox_Report_Month , '') ?>
<?= displayTag("head_lunchbox_$intranet_session_language.gif", $msg) ?>
 <script type="text/javascript">
 <!--

 // -->

function setAll(isChecked, obj)
{
         element_name = "targetClass[]";
         val = isChecked;
        len=obj.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name==element_name)
                {
                    obj.elements[i].checked=val;
                    obj.elements[i].disabled=val;
                }
        }
}
 </script>

<form name="form1" method="POST" action="report.php" target=_blank>

<table width=500 align=center border=0 cellspacing=3 cellpadding=2>
<tr><td align=right><?=$i_SmartCard_Lunchbox_DateField_Date?>:</td><td><?=$select_year?> <?=$select_month?></td></tr>
<tr><td align=right><?=$i_SmartCard_Lunchbox_Report_Param_Type?></td><td>
<input type=radio name=ReportType value=1 checked="checked" ><?=$i_SmartCard_Lunchbox_Report_Type_DayBreakdown?><br />
<input type=radio name=ReportType value=2><?=$i_SmartCard_Lunchbox_Report_Type_TopUntaken?> <?=$select_top?><br />
<input type=radio name=ReportType value=4 ><?=$i_SmartCard_Lunchbox_Report_Type_StudentList?><br />


<!-- &nbsp;&nbsp;<input type=checkbox name=isSplitClass value=1 disabled><?=$i_SmartCard_Lunchbox_Report_Param_SplitClass?>
--->
</td></tr>
<tr><td align=right><?=$i_SmartCard_Lunchbox_Report_Param_SelectClass?></td><td>
<input type=checkbox name=isAllClass CHECKED onClick="setAll(this.checked, this.form)"><?=$i_SmartCard_Lunchbox_Report_Lang_AllClasses?><br>
<?
$lastLvl = $classList[0][2];
for ($i=0; $i<sizeof($classList); $i++)
{
     list($class_id, $class_name, $class_lvl) = $classList[$i];

     if ($i!=0 && $lastLvl != $class_lvl)
     {
         echo "<br>\n";
         $lastLvl = $class_lvl;
     }
     echo "<input type=checkbox name=targetClass[] CHECKED DISABLED value=$class_id> $class_name &nbsp;";
}
?>
</td>
</tr>

</td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
</td>
</tr>
</table>

</form>



<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
