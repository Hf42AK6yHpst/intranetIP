<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$llunch = new liblunchbox();
$lclass = new libclass();

$curr_date = date('Y-m-d');

$curr_year = date('Y');
$curr_month = date('m');
$curr_day = date('d');


if (!isset($targetDate) || $targetDate == "")
{
     $targetDate = $curr_date;
}

$targetTS = strtotime($targetDate);
$targetYear = date('Y',$targetTS);
$targetMonth = date('m',$targetTS);
$targetDay = date('d',$targetTS);

# Check date
$targetPassed = false;
$isToday = false;
if ($curr_year > $targetYear)
{
    $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month > $targetMonth)
{
     $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month == $targetMonth && $curr_day > $targetDay)
{
     $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month == $targetMonth && $curr_day == $targetDay)
{
     $isToday = true;
}


if (!$targetPassed && !$isToday)
{
     die("Future Report is not available");
}


# Handle classes parameters
$class_conds = "";
$class_string = "";
if (!is_array($targetClass) || sizeof($targetClass)==0)
{
     $isAllClass = true;
}

if (!$isAllClass)
{
     $class_list = implode(",", $targetClass);
     $sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassID IN ($class_list) AND RecordStatus = 1";
     $temp = $llunch->returnVector($sql);
     $class_name_list = "'".implode("','", $temp)."'";
     $class_conds = " AND ClassName IN ($class_name_list)";
     $class_string = implode(", ", $temp);
}
else
{
}

$student_conds = "";
if ($class_conds != "")
{
    $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) $class_conds";
    $target_students = $llunch->returnVector($sql);

    $student_conds = " AND StudentID IN (".implode(",", $target_students).")";
}
else
{
}


$ticket_table_name = $llunch->createMonthlyTicketTable($targetYear, $targetMonth);

# Different Types of report
$report_content = "";
if ($ReportType == 1)      # Summary
{
    $report_content .= "$i_SmartCard_Lunchbox_DateField_Date: $targetDate<br>\n";
    if (!$isAllClass)
    {
        $report_content .= "$i_ClassName: $class_string<br>\n";
    }
    $report_content .= "<br>\n";
    $sql = "SELECT RecordStatus, COUNT(RecordID)
                  FROM $ticket_table_name
                  WHERE DayNumber = '$targetDay' $student_conds AND RecordStatus IN (0,1)
                  GROUP BY RecordStatus";
    $temp = $llunch->returnArray($sql,2);
    $result = build_assoc_array($temp);
    $untaken = $result[0]+0;
    $taken = $result[1]+0;
    $total = $untaken + $taken;
    $report_title = "$i_SmartCard_Lunchbox_System - $i_SmartCard_Lunchbox_Report_Type_Summary : $targetDate";

    $report_content .= "<table width=80% cellspacing=0 cellpadding=2 border=1 align=center>\n";
    $report_content .= "<tr><td>$i_SmartCard_Lunchbox_Status_Taken</td><td>".$taken."</td></tr>\n";
    $report_content .= "<tr><td>$i_SmartCard_Lunchbox_Status_NotTaken</td><td>".$untaken."</td></tr>\n";
    $report_content .= "<tr><td>$list_total</td><td>".$total."</td></tr>\n";
    $report_content .= "</table>\n";

}
else if ($ReportType == 2 || $ReportType == 3)
{
     # Taken & untaken list
     $status = ($ReportType==2?0:1);
     $report_content = "";
     $namefield = getNameFieldByLang("b.");

     if ($status == 0)
     {
         $status_string = $i_SmartCard_Lunchbox_Report_Type_UntakenList;
         $nostudent_string = $i_SmartCard_Lunchbox_Report_Words_NoUntaken;
     }
     else
     {
         $status_string = $i_SmartCard_Lunchbox_Report_Type_TakenList;
         $nostudent_string = $i_SmartCard_Lunchbox_Report_Words_NoTaken;
     }
     $report_title = "$i_SmartCard_Lunchbox_System - $status_string : $targetDate";

     $report_content .= $status_string;
     $report_content .= "<br><br>\n";

     if ($isSplitClass)    # Split Class in Display
     {
         if ($isAllClass)
         {
             $classes = $lclass->getClassList();
             unset($targetClass);
             for ($i=0; $i<sizeof($classes); $i++)
             {
                  $targetClass[] = $classes[$i][0];
             }
         }
         $delim = "";
          for ($i=0; $i<sizeof($targetClass); $i++)
          {
               $t_class_id = $targetClass[$i];
               $t_class_name = $lclass->getClassName($t_class_id);
               $sql = "SELECT $namefield, b.ClassName, b.ClassNumber
                              FROM $ticket_table_name as a
                                   LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                       WHERE a.DayNumber = $targetDay AND a.RecordStatus = '$status'
                             AND b.ClassName = '$t_class_name'";
               $t_result = $llunch->returnArray($sql,3);
               $report_content .= "$delim$i_UserClassName : $t_class_name <br>\n";
               if (sizeof($t_result)==0)
               {
                   $report_content .= "<div class=no_valid_data align=center>$nostudent_string</div>";
               }
               else
               {
                   $report_content .= "<table width=80% align=center border=1 cellspacing=0 cellpadding=2 align=center>\n";
                   $report_content .= "<tr class=tableTitle><td>#</td><td>$i_UserStudentName</td><td>$i_UserClassNumber</td></tr>\n";
                   $pos = 1;
                   for ($j=0; $j<sizeof($t_result); $j++)
                   {
                        list($tj_name, $tj_class, $tj_classnum) = $t_result[$j];
                        $css = ($j%2?"":"2");
                        $report_content .= "<tr class=tableContent$css>";
                        $report_content .= "<td>".$pos++."</td>";
                        $report_content .= "<td>$tj_name</td><td>$tj_classnum</td>";
                        $report_content .= "</tr>\n";
                   }
                   $report_content .= "</table>\n";
               }
               $delim = "<hr width=85%>\n";
          }

     }
     else
     {
          $namefield = getNameFieldByLang("b.");
          $student_conds = str_replace("StudentID","a.StudentID",$student_conds);

          $sql = "SELECT $namefield, b.ClassName, b.ClassNumber
                        FROM $ticket_table_name as a
                             LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                        WHERE a.DayNumber = $targetDay AND a.RecordStatus = '$status'
                        $student_conds
                        ORDER BY b.ClassName, b.ClassNumber
                        ";
          $array_students = $llunch->returnArray($sql,3);
          if (sizeof($array_students)==0)
          {
              $report_content .= "<div class=no_valid_data align=center>$nostudent_string</div>\n";
          }
          else
          {
              $report_content .= "<table width=80% align=center border=1 cellspacing=0 cellpadding=2 align=center>\n";
              $report_content .= "<tr class=tableTitle><td>#</td><td>$i_UserStudentName</td><td>$i_SmartCard_ClassName</td><td>$i_UserClassNumber</td></tr>\n";
              $pos = 1;
              for ($j=0; $j<sizeof($array_students); $j++)
              {
                   list($tj_name, $tj_class, $tj_classnum) = $array_students[$j];
                   $css = ($j%2?"":"2");
                   $report_content .= "<tr class=tableContent$css>";
                   $report_content .= "<td>".$pos++."</td>";
                   $report_content .= "<td>$tj_name</td><td>$tj_class</td><td>$tj_classnum</td>";
                   $report_content .= "</tr>\n";
              }
              $report_content .= "</table>\n";
          }
     }

}
else if ($ReportType == 4)
{
     $report_content = "";
     $namefield = getNameFieldByLang("b.");

     $report_title = "$i_SmartCard_Lunchbox_System - $i_SmartCard_Lunchbox_Report_Type_StudentList : $targetDate";
     $report_content .= $i_SmartCard_Lunchbox_Report_Type_StudentList;
     $report_content .= "<br><br>\n";

     if ($isSplitClass)    # Split Class in Display
     {
         if ($isAllClass)
         {
             $classes = $lclass->getClassList();
             unset($targetClass);
             for ($i=0; $i<sizeof($classes); $i++)
             {
                  $targetClass[] = $classes[$i][0];
             }
         }
         $delim = "";
          for ($i=0; $i<sizeof($targetClass); $i++)
          {
               $t_class_id = $targetClass[$i];
               $t_class_name = $lclass->getClassName($t_class_id);
               $sql = "SELECT $namefield, b.ClassName, b.ClassNumber, a.RecordStatus
                              FROM $ticket_table_name as a
                                   LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                       WHERE a.DayNumber = $targetDay
                             AND b.ClassName = '$t_class_name'";
               $t_result = $llunch->returnArray($sql,4);
               $report_content .= "$delim$i_UserClassName : $t_class_name <br>\n";
               if (sizeof($t_result)==0)
               {
                   $report_content .= "<div class=no_valid_data align=center>$i_SmartCard_Lunchbox_Report_Words_NoTicket</div>\n";
               }
               else
               {
                   $report_content .= "<table width=80% align=center border=1 cellspacing=0 cellpadding=2 align=center>\n";
                   $report_content .= "<tr class=tableTitle><td>#</td><td>$i_UserStudentName</td><td>$i_UserClassNumber</td><td>$i_general_status</td></tr>\n";
                   $pos = 1;
                   for ($j=0; $j<sizeof($t_result); $j++)
                   {
                        list($tj_name, $tj_class, $tj_classnum, $tj_status) = $t_result[$j];
                        $str_status = ($tj_status==1?$i_SmartCard_Lunchbox_Status_Taken:$i_SmartCard_Lunchbox_Status_NotTaken);
                        $css = ($j%2?"":"2");
                        $report_content .= "<tr class=tableContent$css>";
                        $report_content .= "<td>".$pos++."</td>";
                        $report_content .= "<td>$tj_name</td><td>$tj_classnum</td>";
                        $report_content .= "<td>$str_status</td>";
                        $report_content .= "</tr>\n";
                   }
                   $report_content .= "</table>\n";
               }
               $delim = "<hr width=85%>\n";
          }

     }
     else
     {
          $namefield = getNameFieldByLang("b.");
          $student_conds = str_replace("StudentID","a.StudentID",$student_conds);

          $sql = "SELECT $namefield, b.ClassName, b.ClassNumber, a.RecordStatus
                        FROM $ticket_table_name as a
                             LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                        WHERE a.DayNumber = $targetDay
                        $student_conds
                        ORDER BY b.ClassName, b.ClassNumber
                        ";
          $array_students = $llunch->returnArray($sql,4);
          if (sizeof($array_students)==0)
          {
              $report_content .= "<div class=no_valid_data align=center>$i_SmartCard_Lunchbox_Report_Words_NoTicket</div>\n";
          }
          else
          {
              $report_content .= "<table width=80% align=center border=1 cellspacing=0 cellpadding=2>\n";
              $report_content .= "<tr class=tableTitle><td>#</td><td>$i_UserStudentName</td><td>$i_SmartCard_ClassName</td><td>$i_UserClassNumber</td><td>$i_general_status</td></tr>\n";
              $pos = 1;
              for ($j=0; $j<sizeof($array_students); $j++)
              {
                   list($tj_name, $tj_class, $tj_classnum, $tj_status) = $array_students[$j];
                   $str_status = ($tj_status==1?$i_SmartCard_Lunchbox_Status_Taken:$i_SmartCard_Lunchbox_Status_NotTaken);
                   $css = ($j%2?"":"2");
                   $report_content .= "<tr class=tableContent$css>";
                   $report_content .= "<td>".$pos++."</td>";
                   $report_content .= "<td>$tj_name</td><td>$tj_class</td><td>$tj_classnum</td>";
                   $report_content .= "<td>$str_status</td>";
                   $report_content .= "</tr>\n";
              }
              $report_content .= "</table>\n";
          }
     }
}
else
{
    echo "Error. Please select Report Type.";
    die();
}


# Display Report
include("../report_header.php");

echo $report_content;

include("../report_footer.php");

intranet_closedb();
?>
