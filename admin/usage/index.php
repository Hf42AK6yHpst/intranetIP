<?php
# using: yat

######### Change Log [Start] ###########
#
#	Date:	2010-12-20	YatWoon
#			use date range instead of select option
#
######### Change Log [End] ###########

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

$now = time();
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));

$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));

if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if ($field == "") $field = 1;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 1; break;
}
$order = ($order == 1) ? 1 : 0;

/*
if ($range == "") $range = 1;

# Date conds
$date_field = "StartTime";
switch ($range)
{
        case 0: // TODAY
             $date_conds = "WHERE TO_DAYS(now()) = TO_DAYS($date_field)";
             break;
        case 1: // LAST WEEK
             $date_conds = "WHERE UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP($date_field)<=604800";
             break;
        case 2: // LAST 2 WEEKS
             $date_conds = "WHERE UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP($date_field)<=1209600";
             break;
        case 3: // LAST MONTH
             $date_conds = "WHERE UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP($date_field)<=2678400";
             break;
        case 4: // ALL
             $date_conds = "";
             break;
        default:
             $date_conds = "WHERE TO_DAYS(now()) = TO_DAYS($date_field)";
             break;
}
$sql  = "SELECT
               $username_field, a.StartTime, a.DateModified
               ,a.ClientHost
               ,UNIX_TIMESTAMP(a.DateModified) - UNIX_TIMESTAMP(a.StartTime) as duration
         FROM INTRANET_LOGIN_SESSION as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
         $date_conds
         ";
*/        

$username_field = getNameFieldWithLoginClassNumberByLang("b.");
if($identity)
{
	$identity_sql = " and b.RecordType=$identity ";	
}
$sql  = "SELECT
			$username_field, 
			a.StartTime, 
			a.DateModified
			,a.ClientHost
			,UNIX_TIMESTAMP(a.DateModified) - UNIX_TIMESTAMP(a.StartTime) as duration
		FROM 
			INTRANET_LOGIN_SESSION as a 
			LEFT OUTER JOIN INTRANET_USER as b ON (a.UserID = b.UserID)
		where 
			a.StartTime>='$start 00:00:00' and a.DateModified<='$end 23:59:59'
			 $identity_sql
";
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("$username_field","a.StartTime","a.DateModified","a.ClientHost", "duration");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,9);
$li->wrap_array = array(0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(0, "$i_UserName ($i_UserLogin)")."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(1, $i_UsageStartTime)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(2, $i_UsageEndTime)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(3, $i_UsageHost)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(4, $i_UsageDuration)."</td>\n";

// TABLE FUNCTION BAR
/*
$searchbar  = "<select name=range onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value=0 ".(($range==0)?"selected":"").">$i_UsageToday</option>\n";
$searchbar .= "<option value=1 ".(($range==1)?"selected":"").">$i_UsageLastWeek</option>\n";
$searchbar .= "<option value=2 ".(($range==2)?"selected":"").">$i_UsageLast2Week</option>\n";
$searchbar .= "<option value=3 ".(($range==3)?"selected":"").">$i_UsageLastMonth</option>\n";
$searchbar .= "<option value=4 ".(($range==4)?"selected":"").">$i_UsageAll</option>\n";
$searchbar .= "</select>\n";
*/

$datetype += 0;
$selected[$datetype] = "SELECTED";

$classSelect = "<SELECT name=datetype onChange=\"changeDateType(this.form)\">\n";
$classSelect .= "<OPTION value=0 ".$selected[0].">$i_Profile_Today</OPTION>\n";
$classSelect .= "<OPTION value=1 ".$selected[1].">$i_Profile_ThisWeek</OPTION>\n";
$classSelect .= "<OPTION value=2 ".$selected[2].">$i_Profile_ThisMonth</OPTION>\n";
$classSelect .= "<OPTION value=3 ".$selected[3].">$i_Profile_ThisAcademicYear</OPTION>\n";
$classSelect .= "</SELECT> \n";

unset($selected);
$selected[$identity] = "SELECTED";

$identitySelect = "<SELECT name=identity>\n";
$identitySelect .= "<OPTION value=0 ".$selected[0].">$button_select_all</OPTION>\n";
$identitySelect .= "<OPTION value=1 ".$selected[1].">$i_identity_teachstaff</OPTION>\n";
$identitySelect .= "<OPTION value=2 ".$selected[2].">$i_identity_student</OPTION>\n";
$identitySelect .= "<OPTION value=3 ".$selected[3].">$i_identity_parent</OPTION>\n";
$identitySelect .= "</SELECT> \n";

$functionbar = "<table border=0>\n";
$functionbar .= "<tr><td nowrap>$i_Profile_SelectSemester:</td><td>$classSelect<br>$i_Profile_From <input type=text name=start size=10 value='$start'> $i_Profile_To ";
$functionbar .= "<input type=text name=end size=10 value='$end'> <span class=extraInfo>(yyyy-mm-dd) </span>";
$functionbar .= "</td></tr>";
$functionbar .= "<tr>";
$functionbar .= "<td>$i_identity:</td>";
$functionbar .= "<td>".$identitySelect."</td>";
$functionbar .= "</tr>";

$functionbar .= "</table>\n";

$functionbar .= "<center><a href='javascript:document.form1.submit()'><img src='/images/admin/button/s_btn_submit_$intranet_session_language.gif' border='0' align='absmiddle'></a></center>";

$toolbar ="<a class=iconLink href='export.php?start=$start&end=$end&identity=$identity'>".exportIcon().$button_export."</a>";

?>
     <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
     <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_free";
         css_array[1] = "dynCalendar_half";
         css_array[2] = "dynCalendar_full";
         var date_array = new Array;
     </script>

     <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
     <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
     <script type="text/javascript">
     <!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                   if (String(month).length == 1) {
                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }
                   dateValue =year + '-' + month + '-' + date;
                   document.forms['remove'].RemoveFrom.value = dateValue;
          }
          
          function changeDateType(obj)
			{
			         switch (obj.datetype.value)
			         {
			                 case '0':
			                      obj.start.value = '<?=$today?>';
			                      obj.end.value = '<?=$today?>';
			                      break;
			                 case '1':
			                      obj.start.value = '<?=$weekstart?>';
			                      obj.end.value = '<?=$weekend?>';
			                      break;
			                 case '2':
			                      obj.start.value = '<?=$monthstart?>';
			                      obj.end.value = '<?=$monthend?>';
			                      break;
			                 case '3':
			                      obj.start.value = '<?=$yearstart?>';
			                      obj.end.value = '<?=$yearend?>';
			                      break;
			         }
			         obj.submit();
			}

     // -->
     </script>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_am, '', $i_adminmenu_am_usage, '') ?>
<?= displayTag("head_usage_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $functionbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><img src=../../images/space.gif width=1 height=4 border=0></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<form name="remove" method="post" action="remove.php" ONSUBMIT="return checkRemoveForm(this)">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<hr size=1 class="hr_sub_separator">
<blockquote>
<script LANGUAGE=Javascript>
function checkRemoveForm(obj)
{
         if (!check_date(obj.RemoveFrom,"<?php echo $i_invalid_date; ?>")) return false;
         return confirm('<?=$i_Usage_RemoveConfirm?>');
}
</script>
<p><span class="extraInfo">[<span class=subTitle><?=$i_Usage_RemoveRecords?></span>]</span></p>
<?=$i_Usage_Remove1?> <input type=text name=RemoveFrom value='<?=$specialStart?>' size=10><script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span> <?=$i_Usage_Remove2?>
<br>
</blockquote>
</td>
</tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0">
<a href="javascript:document.remove.reset()"><img src='/images/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>