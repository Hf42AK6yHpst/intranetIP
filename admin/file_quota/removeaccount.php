<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libftp.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();
$lftp = new libftp();

$sql = "SELECT a.UserLogin, b.ACL FROM INTRANET_USER as a
               LEFT OUTER JOIN INTRANET_SYSTEM_ACCESS as b ON a.UserID = b.UserID
               WHERE a.UserID = $uid";
$temp = $li->returnArray($sql,2);
list($userlogin, $acl) = $temp[0];

if ($userlogin == "")
{
    header("Location: index.php");
    exit();
}

$lftp->removeAccount($userlogin);
if ($acl == "")
{
    $sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($uid, 0)";
    $li->db_db_query($sql);
}
else
{
    $sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 2 WHERE UserID = $uid AND ACL IN (2,3)";
    $li->db_db_query($sql);
}


if ($type==1)       # From Type List
{
    $url = "list_type.php?UserType=$target&ClassName=$ClassName&msg=2";
}
else if ($type==2)    # From Group List
{
    $url = "list_group.php?GroupID=$target&msg=2";
}
else     # From Login name input
{
    $url = "index.php";
}

intranet_closedb();
header("Location: $url");
?>