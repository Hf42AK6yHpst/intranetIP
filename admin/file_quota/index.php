<?php
include_once("../../includes/global.php");

if (!$plugin['personalfile'])
{
     header("/admin/main_setting.php");
     exit();
}

include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_LinuxAccount_Folder_QuotaSetting, '') ?>
<?= displayTag("head_storagequota_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300>
<blockquote>
<?=$i_LinuxAccount_PersonalFile_Description?><br><br><br>
<?= displayOption($i_LinuxAccount_SetDefaultQuota, 'default.php', 1,
                                $i_LinuxAccount_SetUserQuota, 'user.php', 1,
                                $i_LinuxAccount_SetGroupQuota, 'group.php',1,
                                $i_LinuxAccount_DisplayQuota, 'list.php', 1
                                ) ?>
</blockquote>
</td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>