<?php
// editing by 
/*********************************************** Change log ******************************************************
 * 2013-08-01 (Carlos): set default_socket_timeout to a large value to prevent fopen timeouted 
 * 2011-10-19 (Carlos): Added Alumni
 *****************************************************************************************************************/
ini_set('default_socket_timeout',60*60);
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libftp.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();
# Search Type
$UserType = $_GET['UserType'];
$type_name = $i_identity_array[$UserType];

?>
<?= displayNavTitle($i_admintitle_fs, '', $i_LinuxAccount_Folder_QuotaSetting, 'index.php',$i_LinuxAccount_DisplayQuota,'list.php',$type_name,'') ?>
<?= displayTag("head_storagequota_$intranet_session_language.gif", $msg) ?>
<?
if ($UserType == 1 || $UserType == 3 || $UserType == 4 || $ClassName != "")
{
?>
<SCRIPT language=Javascript>
function removeAccount(id)
{
         if (confirm('<?=$i_Files_alert_RemoveAccount?>'))
         {
             location.href = "removeaccount.php?type=1&target=<?=$UserType?>&ClassName=<?=$ClassName?>&uid="+id;
         }
}
</SCRIPT>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><a class=iconLink href="batch_process.php?type=1&target=<?=$UserType?>&ClassName=<?=$ClassName?>"><img src="<?=$image_path?>/admin/icon_setting.gif" border=0 hspace=1 vspace=0 align=absmiddle><?=$i_LinuxAccount_BatchProcess?></a></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td class=tableTitle><?="$i_UserName ($i_UserLogin)"?></td>
<td class=tableTitle><?=$i_Files_Linked?></td>
<td class=tableTitle><?=$i_LinuxAccount_Quota?> (Mbytes)</td>
<td class=tableTitle><?=$i_LinuxAccount_UsedQuota?> (Mbytes)</td>
</tr>
<?
# Retrieve Quota
$lftp = new libftp();
$list = $lftp->getQuotaTable("iFolder");
for ($i=0; $i<sizeof($list); $i++)
{
     list($login,$used,$soft,$hard) = $list[$i];
     $quota[$login] = array($used,$soft);
}
$name_field = getNameFieldWithClassNumberByLang("a.");
$allowed_type_list = implode(",",$personalfile_identity_allowed);
if ($UserType == 1 || $UserType == 3 || $UserType == 4)
{
    $sql = "SELECT a.UserID, a.UserLogin, $name_field, b.ACL FROM INTRANET_USER as a
                   LEFT OUTER JOIN INTRANET_SYSTEM_ACCESS as b ON a.UserID = b.UserID
                   WHERE a.RecordType = $UserType
                   AND a.RecordType IN ($allowed_type_list)
                   ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
}
else
{
    $sql = "SELECT a.UserID, a.UserLogin, $name_field, b.ACL FROM INTRANET_USER as a
                   LEFT OUTER JOIN INTRANET_SYSTEM_ACCESS as b ON a.UserID = b.UserID
                   WHERE a.RecordType = $UserType AND a.ClassName = '$ClassName'
                   AND a.RecordType IN ($allowed_type_list)
                   ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
}
#echo "1. $sql\n";
$users = $lclass->returnArray($sql,4);
for ($i=0; $i<sizeof($users); $i++)
{
     list($uid, $login, $name, $acl) = $users[$i];
     $login = strtolower($login);
     $hasAccessRight = ($acl==2 || $acl==3);
     $user_used = round($quota[$login][0],2);
     $user_quota = round($quota[$login][1],2);
     if (!isset($quota[$login]))
     {
         $user_quota = "N/A";
     }
     else if ($user_quota == 0)
     {
         $user_quota = "$i_LinuxAccount_NoLimit";
     }
     if (!isset($quota[$login]))
     {
         $user_used = "N/A";
     }
     if ($hasAccessRight)
     {
         $ifolder_linked = "yes";
     }
     else $ifolder_linked = "no";
     $css = ($i%2? "":"2");
     $en_login = urlencode($login);
     $en_ClassName = urlencode($ClassName);
     $edit_link = "<a href=user_set.php?type=1&target=$UserType&ClassName=$en_ClassName&loginName=$en_login>$name ($login) <img src=\"$image_path/edit_icon.gif\" border=0></a>";
     $remove_link = "<a class=functionlink href=\"javascript:removeAccount($uid)\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
     if ($user_quota == "N/A")
     {
         echo "<tr class=tableContent$css><td>$edit_link $remove_link</td><td>$ifolder_linked</td><td colspan=2 align=center>$i_LinuxAccount_NoAccount</td></tr>\n";
     }
     else
     {
         echo "<tr class=tableContent$css><td>$edit_link $remove_link</td><td>$ifolder_linked</td><td>$user_quota</td><td>$user_used</td></tr>\n";
     }
}

?>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</td>
</tr>
</table>
<?
}
else
{
    $select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.submit()");
?>
<form name=form1 action='' method=GET>
<blockquote>
<?=$i_Profile_SelectClass?> <?=$select_class?>
</blockquote>
<input type=hidden name=UserType value="<?=$UserType?>">
</form>
<?
}
?>


<?php
include_once("../../templates/adminfooter.php");
?>