<?php
// editing by 
/**************************************** Changes log *********************************************
 * 2014-11-11 (Carlos): Generate a random password for users that do not have encrypted password logged.
 * 2014-07-03 (Carlos): Use new approach to retrieve password if using hash password mechanism
 * 2014-05-07 (Carlos): Added suspend/unsuspend api call
 * 2011-09-30 (Carlos): Cater schools that only use hashed password problem
 */
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libftp.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libpwm.php");
intranet_opendb();

$li = new libdb();
$lftp = new libftp();
$sql = "SELECT u.RecordType, u.UserLogin, u.UserPassword, s.EncPassword  
		FROM INTRANET_USER as u 
		LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as s ON s.UserID = u.UserID 
		WHERE u.UserID = '$uid'";
$temp = $li->returnArray($sql,4);
list ($target_utype, $loginName, $password, $encPassword) = $temp[0];
$loginName = strtolower($loginName);
$password = trim($password);
/*
$encPassword = trim($encPassword);
if($password == "" && $encPassword != ""){
	$password = GetDecryptedPassword($encPassword);
}
*/
if($intranet_authentication_method == 'HASH'){
	$libpwm = new libpwm();
	$uidToPw = $libpwm->getData(array($uid));
	$password = $uidToPw[$uid];
}

if (!in_array($target_utype,$personalfile_identity_allowed))
{
     header("Location: user.php?error=2");
     intranet_closedb();
     exit();
}

if ($newAccount == 1)        # Create new account
{
    if ($password == "")
    {
        //header("Location: user.php?error=1");
        //exit();
        // generate a random password if password is not available
		$password = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'),0,10);
    }
    $lftp->open_account($loginName, $password);
}

if ($newAccount == 1 || ($quota != "" && $quota != $oldQuota) ) # Same not change
    $quota = $lftp->setTotalQuota($loginName,$quota,"iFolder");

$sql = "SELECT ACL FROM INTRANET_SYSTEM_ACCESS WHERE UserID = $uid";
$temp = $li->returnVector($sql);
$acl = $temp[0];
if ($linked == 1)
{
    if ($acl == "")
    {
        $sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($uid, 2)";
        $li->db_db_query($sql);
    }
    else
    {
        if ($acl == 2 || $acl == 3)
        {
            # Nothing to do

        }
        else
        {
            $sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL + 2 WHERE UserID = $uid AND (ACL NOT IN (2,3))";
            $li->db_db_query($sql);
        }
    }
    
    $lftp->setUnsuspendUser($loginName,"iFolder",$password);
}
else    # Unlink it
{
    if ($acl == "")
    {
        $sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($uid, 0)";
        $li->db_db_query($sql);
    }
    else
    {
        if ($acl == 2 || $acl == 3)
        {
            $sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 2 WHERE UserID = $uid AND (ACL IN (2,3))";
            $li->db_db_query($sql);
        }
        else
        {
            # Nothing to do
        }
    }
    
    $lftp->setSuspendUser($loginName,"iFolder");
}

if ($type==1)       # From Type List
{
    $url = "list_type.php?UserType=$target&ClassName=$ClassName&msg=2";
}
else if ($type==2)    # From Group List
{
    $url = "list_group.php?GroupID=$target&msg=2";
}
else     # From Login name input
{
    $url = "user.php?msg=2";
}

intranet_closedb();
header("Location: $url");
?>