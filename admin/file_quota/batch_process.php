<?php
// editing by 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
intranet_opendb();

$file_content = get_file_content($intranet_root."/file/account_file_quota.txt");
if ($file_content == "")
{
    $userquota = array(10,10,10);
    if($special_feature['alumni']) $userquota[] = 10;
}
else
{
    $userquota = explode("\n", $file_content);
    if($userquota[3] == "") $userquota[3] = 10; 
}
if ($type==1)
{
    $quota = $userquota[$target-1];
}
else
{
    $quota = 10;
}

#$used = $lwebmail->getUsedQuota($loginName);
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function checkform(obj)
{
         if (obj.batchoption.value==1)
         {
             if (obj.quota.value != '0' && !check_int(obj.quota,0,'<?=$i_LinuxAccount_Alert_QuotaMissing?>')) return false;
         }
         return true;
}
</SCRIPT>


<form name="form1" action="batch_process_update.php" method="post" onSubmit="return checkform(this)">
<?= displayNavTitle($i_admintitle_fs, '', $i_LinuxAccount_Folder_QuotaSetting, 'index.php',$i_LinuxAccount_BatchProcess,'') ?>
<?= displayTag("head_storagequota_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<table width=400 border=0>
<tr><td>
<input type=radio name=batchoption value=1 CHECKED> <?=$i_Files_BatchOption_OpenAccount?><input type=text size=5 name=quota value='<?=$quota?>'><br>
<input type=radio name=batchoption value=2 > <?=$i_Files_BatchOption_UnlinkAccount?><br>
<input type=radio name=batchoption value=3 > <?=$i_Files_BatchOption_RemoveAccount?><br>
</td></tr>
<tr><td><br>
<font color=red><?=$i_LinuxAccount_GroupSetNote?></font>
</td></tr>
</table>
</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
<input type=hidden name=type value="<?=$type?>">
<input type=hidden name=target value="<?=$target?>">
<input type=hidden name=ClassName value="<?=$ClassName?>">
</form>

<?php
include_once("../../templates/adminfooter.php");
?>