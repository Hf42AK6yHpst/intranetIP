<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
#include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

#intranet_opendb();

$key = array_keys($i_Sports_AdminConsole_UserType);
for($i=0; $i<sizeof($i_Sports_AdminConsole_UserType); $i++)
{
	if ($i == 0) {
		$userTypes .= "<tr><td align=\"right\">".$i_Sports_AdminConsole_UserTypes.":</td><td><input type=\"radio\" name=\"userType\" value=\"".$key[$i]."\" checked id=\"userType".$key[$i]."\"><label for=\"userType".$key[$i]."\">".$i_Sports_AdminConsole_UserType[$key[$i]]."</label></td></tr>";
	} else {
		$userTypes .= "<tr><td align=\"right\">&nbsp;</td><td><input type=\"radio\" name=\"userType\" value=\"".$key[$i]."\" id=\"userType".$key[$i]."\"><label for=\"userType".$key[$i]."\">".$i_Sports_AdminConsole_UserType[$key[$i]]."</label></td></tr>";
	}
}
?>
<script language="javascript">
function checkform(obj){
     if(!check_text(obj.targetLogin, "<?php echo $i_alert_pleasefillin.$i_UserLogin; ?>.")) return false;
}
</script>
<form name="form1" action="admin_new_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_fs, '', $i_Sports_System, 'index.php',$i_Sports_AdminConsole_AccountAllowedToUse,'admin.php',$button_new,'') ?>
<?= displayTag("head_sports_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right><?php echo $i_UserLogin; ?>:</td><td><input class=text type=text name=targetLogin size=30 maxlength=100></td></tr>
<?php echo $userTypes; ?>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>



<?php
#intranet_closedb();
include_once("../../templates/adminfooter.php");
?>