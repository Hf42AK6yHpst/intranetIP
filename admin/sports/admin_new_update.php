<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libsports.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lsports = new libsports();

$targetLogin = trim($_POST['targetLogin']);
$userType = $_POST['userType'];
if ($lsports->addAdminUser($targetLogin, $userType))
{
    $msg = 1;
}
else
{
    $msg = 12;
}

intranet_closedb();
header("Location: admin.php?msg=$msg");
?>