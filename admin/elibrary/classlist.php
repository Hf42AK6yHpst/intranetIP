<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libteaching.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");
intranet_opendb();

$lt = new libteaching();
$class = $lt->returnClassList();
$x .= "<b>$i_Teaching_ClassList</b> :\n<br />\n";
$x .= "<table border=1 bordercolor='#CCCCCC' cellspacing=0 cellpadding=4 width='150'>\n";
$x .= "<tr><td><b>".$i_ClassName."</b></td></tr>";
for ($i=0; $i<sizeof($class); $i++)
{
     $x .= "<tr><td>".$class[$i][1]."</td></tr>\n";
}
$x .= "</table>\n";
echo $x;
intranet_closedb();
include_once("../../templates/filefooter.php");
?>