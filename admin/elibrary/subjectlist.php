<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libteaching.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");
intranet_opendb();

//$lt = new libteaching();
//$SubjectArray = $lt->returnReportCardSubjectList();
$li = new libdb();

$NameField = ($intranet_session_language=="en") ? "EN_ABBR" : "CH_ABBR";
$sql = "SELECT EN_SNAME, $NameField FROM {$eclass_db}.ASSESSMENT_SUBJECT WHERE CMP_CODEID IS NULL ORDER BY DisplayOrder";
$SubjectArray = $li->returnArray($sql,2);

$x .= "<b>$i_Teaching_SubjectList</b> :\n<br />\n";
$x .= "<table border=1 bordercolor='#CCCCCC' cellspacing=0 cellpadding=4 width='300'>\n";
$x .= "<tr>
		<td><b>".$eReportCard['Code']."</b></td>
		<td><b>".$eReportCard['SubjectName']."</b></td>
	</tr>";
for ($i=0; $i<sizeof($SubjectArray); $i++)
{
	list($SubjectCode, $SubjectDesc) = $SubjectArray[$i];
     $x .= "<tr><td>".$SubjectCode."</td><td>".$SubjectDesc."</td></tr>\n";
}
$x .= "</table>\n";
echo $x;
intranet_closedb();
include_once("../../templates/filefooter.php");
?>