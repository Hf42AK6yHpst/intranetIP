<?php
$PATH_WRT_ROOT = "../../";

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libelibrary.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
include_once($PATH_WRT_ROOT."file/elibrary/elibrary_batch.php");
intranet_opendb();

$li = new libdb();
$libelib = new elibrary();

?>
<?= displayNavTitle($i_adminmenu_fs, '', $i_eLibrary_System, 'index.php', $i_eLibrary_System_Update_Batch, '') ?>
<?= displayTag("head_eLibrary_$intranet_session_language.gif", $msg) ?>

<script language="javascript">
</script>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center"><tr><td>
<?php
echo "<p><b>".$i_eLibrary_System_Current_Batch.":</b></p>";
foreach($elib_batch as $key => $data)
{
	$tmpStatus = $data["value"];
	$tmpDesc = $data["desc_".$intranet_session_language];
	
	if($tmpStatus)
	echo "<p>$tmpDesc</p>";
}
?>
</td></tr></table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<a href="batch_update.php"><img src="/images/admin/button/s_btn_update_<?=$intranet_session_language?>.gif" border="0"></a>
<a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<?php
intranet_closedb();

include("../../templates/adminfooter.php");
?>