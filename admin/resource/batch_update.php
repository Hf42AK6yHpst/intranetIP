<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$sql = "DELETE FROM INTRANET_BOOKING WHERE ResourceID IN (".implode(",", $ResourceID).")";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_GROUPRESOURCE WHERE ResourceID IN (".implode(",", $ResourceID).")";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_RESOURCE WHERE ResourceID IN (".implode(",", $ResourceID).")";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&msg=3");
?>
