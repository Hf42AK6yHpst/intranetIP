<?php 
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libresource.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();

$li = new libresource($ResourceID);
?>

<form action=edit.php method=get>
<p class=admin_head><?php echo $i_admintitle_rm; ?><?php echo displayArrow();?><a href=javascript:history.back()><?php echo $i_admintitle_rm_item; ?></a><?php echo displayArrow();?><?php echo $li->Title; ?></p>
<blockquote>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr><td><?php echo $li->display(); ?></td></tr>
<tr><td><input class=submit type=submit value="<?php echo $button_edit; ?>"></td></tr>
</table>
</blockquote>
<input type=hidden name=ResourceID[] value="<?php echo $li->ResourceID; ?>">
</form>

<?php 
intranet_closedb();
include("../../templates/adminfooter.php"); 
?>