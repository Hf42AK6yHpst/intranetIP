<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libresource.php");
intranet_opendb();

$li = new libresource($ResourceID);
$old_bid = $li->TimeSlotBatchID;

$ResourceCode = intranet_htmlspecialchars(trim($ResourceCode));
$ResourceCategory = intranet_htmlspecialchars(trim($ResourceCategory));
$Title = intranet_htmlspecialchars(trim($Title));
$Description = intranet_htmlspecialchars(trim($Description));
$Remark = intranet_htmlspecialchars(trim($Remark));
$Attachment = intranet_htmlspecialchars(trim($Attachment));
$daysbefore = intranet_htmlspecialchars(trim($daysbefore));
$batchID = intranet_htmlspecialchars(trim($batchID));
$DefaultStatus = intranet_htmlspecialchars(trim($DefaultStatus));
if ($DefaultStatus == 'yes')
    $RecordType = 4;
else $RecordType = 0;


$sql = "UPDATE INTRANET_RESOURCE SET
               ResourceCode = '$ResourceCode',
               ResourceCategory = '$ResourceCategory',
               Title = '$Title',
               Description = '$Description',
               Remark = '$Remark',
               Attachment = '$Attachment',
               RecordStatus = '$RecordStatus',
               RecordType = '$RecordType',
               DaysBefore = '$daysbefore',
               TimeSlotBatchID = '$batchID',
               DateModified = now()
          WHERE
               ResourceID = $ResourceID";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_GROUPRESOURCE WHERE ResourceID = $ResourceID";
$li->db_db_query($sql);

for($i=0; $i<sizeof($GroupID); $i++){
     $sql = "INSERT INTO INTRANET_GROUPRESOURCE (GroupID, ResourceID) VALUES (".$GroupID[$i].", $ResourceID)";
     $li->db_db_query($sql);
}

# Perform cancellation and archival
// debug_pr("$old_bid != $batchID");
if ($ResetBooking == 1 && $old_bid != $batchID)
{
//    # Archival
//    $conds = "a.ResourceID = $ResourceID AND a.BookingDate < CURDATE()";
//    $sql = "INSERT INTO INTRANET_BOOKING_ARCHIVE
//            (BookingDate,TimeSlot,Username,Category,Item,ItemCode,FinalStatus,TimeApplied,LastAction)
//            SELECT a.BookingDate, CONCAT(c.Title,' ',c.TimeRange),
//            CONCAT(b.EnglishName, IF (b.ClassNumber IS NULL OR b.ClassNumber = '', '', CONCAT(' (',b.ClassName,'-',b.ClassNumber,')') ) ),
//            d.ResourceCategory, d.Title, d.ResourceCode,
//            a.RecordStatus, a.TimeApplied, a.LastAction
//            FROM INTRANET_BOOKING_RECORD as a, INTRANET_USER as b, INTRANET_SLOT as c, INTRANET_RESOURCE as d
//            WHERE a.UserID = b.UserID AND c.BatchID = d.TimeSlotBatchID AND a.ResourceID = d.ResourceID AND a.TimeSlot = c.SlotSeq
//            AND $conds";
//    $li->db_db_query($sql);
//
//    # Remove records
//    $cond2 = "ResourceID = $ResourceID";
//    $sql = "DELETE FROM INTRANET_BOOKING_RECORD WHERE $cond2";
//    $li->db_db_query($sql);

	include_once("../../includes/librb.php");
	$librb = new librb();
	$librb->Archive_Booking_Record($ResourceID);

}

intranet_closedb();
header("Location: index.php?filter=$RecordStatus&msg=2");
?>