<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libbatch.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

$lb = new libbatch();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if($field=="") $field = 3;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     default: $field = 3; break;
}
if($filter == "") $filter = 1;
$filter = ($filter == 1) ? 1 : 0;
$sql  = "SELECT
               CONCAT('<a class=tableContentLink href=edit.php?ResourceID[]=', a.ResourceID, '>', a.ResourceCategory, '</a>'),
               CONCAT('<a class=tableContentLink href=edit.php?ResourceID[]=', a.ResourceID, '>', a.ResourceCode, '</a>'),
               CONCAT('<a class=tableContentLink href=edit.php?ResourceID[]=', a.ResourceID, '>', a.Title, '</a>'),
               b.Title,
               a.DateModified,
               CONCAT('<input type=checkbox name=ResourceID[] value=', a.ResourceID ,'>')
          FROM
               INTRANET_RESOURCE as a, INTRANET_SLOT_BATCH as b
          WHERE
               (a.ResourceCode like '%$keyword%' OR a.ResourceCategory like '%$keyword%' OR a.Title like '%$keyword%') AND
               a.RecordStatus = $filter AND (a.TimeSlotBatchID = b.BatchID OR (a.TimeSlotBatchID is NULL and b.RecordStatus =1) )
          ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.ResourceCategory", "a.ResourceCode", "a.Title","b.BatchID", "a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_resource;
$li->column_array = array(0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(0, $i_ResourceCategory)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(1, $i_ResourceCode)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(2, $i_ResourceTitle)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(3, $i_ResourceTimeSlot)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(4, $i_ResourceDateModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("ResourceID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=\"javascript:checkNew('new.php')\">".newIcon()."$button_new</a>";
$toolbar .= toolBarSpacer()."<a class=iconLink  href=\"import.php\">".importIcon()."$button_import</a>\n";
//$toolbar .= toolBarSpacer()."<a href=javascript:checkPost(document.form1,'import.php')>".importIcon()."$button_import</a>\n";

$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'ResourceID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'ResourceID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
/*
$functionbar .= $lb->returnBatchSelect("name=batchID");
$functionbar .= "<input class=button type=button value='$button_remove' onClick=checkUpdate(this.form,'ResourceID[]','batch_update.php')>";
*/
$titlebar  = "<select name=filter onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$titlebar .= "<option value=0 ".(($filter==0)?"selected":"").">$i_status_pending</option>\n";
$titlebar .= "<option value=1 ".(($filter==1)?"selected":"").">$i_status_publish</option>\n";
$titlebar .= "</select>\n";
$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_rm, '', $i_admintitle_rm_item, '') ?>
<?= displayTag("head_resource_list_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $titlebar.$searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<p><br></p>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>