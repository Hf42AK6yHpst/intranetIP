<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/fileheader_admin.php"); 
include("menu.php"); 

$root = returnFolderRoot($folderID);
$title = returnFolderTitle($folderID);
$path = str_replace("..","",$path);
?>

<form name="form1" action="attach_uploadfile.php" method="post" enctype="multipart/form-data">
<?= displayNavTitle($title, 'javascript:history.back()', $button_upload, '') ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td>
<blockquote>
<br><br>
<table border="0" cellpadding="5" cellspacing="0">
<?php for($i=0;$i<$no_file;$i++){ ?><?php echo "<tr><td>".($i+1); ?>: <input class=file type=file name="userfile<?php echo $i; ?>" size=25></td></tr><?php } ?>
</table>
<br><br>
</blockquote>
</td></tr>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_upload_<?=$intranet_session_language?>.gif" border="0">
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td></tr>
</table>

<input type=hidden name=no_file value="<?php echo $no_file; ?>">
<input type=hidden name=path value="<?php echo $path; ?>">
<input type=hidden name=folderID value="<?php echo $folderID; ?>">
<input type=hidden name=attachment value="<?php echo $attachment; ?>">
<input type=hidden name=fieldname value="<?php echo $fieldname; ?>">
</form>

<?php 
include("../../templates/filefooter.php"); 
?>
