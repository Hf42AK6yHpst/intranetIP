<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../templates/fileheader.php");
include("../../lang/lang.$intranet_session_language.php");
intranet_opendb();
$li = new libdb();
//$sql = "SELECT Title, RecordStatus FROM INTRANET_SLOT_BATCH ORDER BY RecordStatus DESC, Title ASC";
$sql = "SELECT a.Title, a.RecordStatus, b.Title, b.TimeRange, b.BatchID
        FROM INTRANET_SLOT_BATCH as a, INTRANET_SLOT as b
        WHERE a.BatchID = b.BatchID
        ORDER BY a.RecordStatus DESC, a.Title ASC, b.SlotSeq ASC";
$result = $li->returnArray($sql,5);
$prevBatch = -1;
echo "<ul>\n";
for ($i=0; $i<sizeof($result); $i++)
{
     list($batchName,$school,$slotname,$slottime,$bid) = $result[$i];
     if ($bid != $prevBatch)
     {
         if ($school == 1)
             $batchName .= ' *';
         if ($prevBatch != -1)
         {
             echo "</ol>\n";
         }
         echo "<li> $batchName <ol>\n";
         $prevBatch = $bid;
     }
?>
<li class=numberlist> <?="$slotname $slottime"?></li>

<?php
}
echo "</ol></ul>\n";
echo "* - $i_ResourcePopScheme\n";
intranet_closedb();
include("../../templates/filefooter.php");
?>