<?php
die();
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
include("menu.php");

$root = returnFolderRoot($folderID);
$title = returnFolderTitle($folderID);
$li = new libfilesystem();
$path = str_replace("..", "", $path);
$path = stripslashes($path);
$newpath = str_replace("..","",$newpath);
$newpath = stripslashes($newpath);
$x = "";
for($i=0;$i<sizeof($filename);$i++){        
        $file = stripslashes($filename[$i]);
        $x .= ($li->lfs_copy("$root/$path/$file", "$root/$newpath")) ? "$button_copy: $file ...... SUCCESS." : "$button_copy: $file ...... FAIL.";
        $x .= "<br>\n";
}
?>

<form name="form1" action="index.php" method="get">
<?= displayNavTitle($i_admintitle_sf, '', $title, 'javascript:history.back()', $button_copy, '') ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td>
<blockquote>
<p><br><br><?php echo $x; ?><br><br>
</blockquote>
</td></tr>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_continue_<?=$intranet_session_language?>.gif" border="0"></td></tr>
</table>

<input type=hidden name=path value="<?php echo $newpath; ?>">
<input type=hidden name=folderID value="<?php echo $folderID; ?>">
</form>

<?php
include("../../templates/adminfooter.php");
?>