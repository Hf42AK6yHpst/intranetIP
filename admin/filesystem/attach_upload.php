<?php
die();
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/fileheader_admin.php"); 
include("menu.php"); 

$root = returnFolderRoot($folderID);
$title = returnFolderTitle($folderID);
$path = str_replace("..","",$path);
?>

<form action="attach_uploadfile.php" method="post" enctype="multipart/form-data">
<p class=admin_head><a href=javascript:history.back()><?php echo $title; ?></a> <?php echo displayArrow();?>  <?php echo $button_upload; ?></p>
<blockquote>
<p><?php for($i=0;$i<$no_file;$i++){ ?><?php echo $i+1; ?>: <input class=file type=file name="userfile<?php echo $i; ?>" size=25><br><?php } ?>
<p><input class=submit type=submit value="<?php echo $button_upload; ?>"><input class=button type=button value="<?php echo $button_cancel; ?>" onClick="history.back()">
</blockquote>
<input type=hidden name=no_file value="<?php echo $no_file; ?>">
<input type=hidden name=path value="<?php echo $path; ?>">
<input type=hidden name=folderID value="<?php echo $folderID; ?>">
<input type=hidden name=attachment value="<?php echo $attachment; ?>">
<input type=hidden name=fieldname value="<?php echo $fieldname; ?>">
</form>

<?php 
include("../../templates/filefooter.php"); 
?>
