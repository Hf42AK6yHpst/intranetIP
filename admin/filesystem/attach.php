<?php
die();
include("../../includes/global.php");
include("../../includes/libfiletable.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/fileheader_admin.php"); 
include("menu.php"); 

$keyword = trim($keyword);
$order = ($order==1) ? 1 : 0;
$field = ($field==1 || $field==2) ? $field : $field = 0;
$root = returnFolderRoot($folderID);
$title = returnFolderTitle($folderID);
$path = str_replace("..","",$path);
$li = new libfiletable($root, $path, $order, $field, $keyword);
$li->checkField2 = returnCheckField($folderID);
$li->radioField = returnRadioField($folderID);
$li->attachment = $attachment;
$toolbar  = "<a href=javascript:fs_newfolder(document.form1,'attach_newfolder.php')>".newFolderIcon()."$button_newfolder</a>\n";
$functionbar .= "<input class=button type=button value='$button_attach' onClick=fs_attachfile(this.form,'filename[]',opener.window.document.form1.$fieldname);>";
$titlebar  = "<input class=button type=button value='$button_upload' onClick=\"fs_fileupload(this.form,'attach_upload.php');\">\n";
$titlebar .= "<input class=text type=text size=1 maxlength=1 name=no_file value=1> $i_FileSystemFiles \n";
$searchbar  = "<input class=text type=text name=keyword size=5 maxlength=20 value=\"".stripslashes($keyword)."\">\n";
$searchbar .= "<input class=submit type=submit value=\"$button_find\">\n";
?>

<form action=attach.php name=form1 method="get">
<p class=admin_head><?php echo $title; ?></p>
<?php echo $li->displayFunctionbar("", $titlebar.$searchbar); ?>
<?php echo $li->displayFunctionbar($toolbar, $functionbar); ?>
<?php echo $li->display(); ?>
<input type=hidden name=folderName value="">
<input type=hidden name=pageNo value="0">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=path value="<?php echo $path; ?>">
<input type=hidden name=folderID value="<?php echo $folderID; ?>">
<input type=hidden name=attachment value="<?php echo $attachment; ?>">
<input type=hidden name=fieldname value="<?php echo $fieldname; ?>">
</form>

<?php 
include("../../templates/filefooter.php"); 
?>
