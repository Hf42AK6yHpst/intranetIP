<?php
die();
include("../../includes/global.php");
include("../../includes/libfiletable.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
include("menu.php");

$keyword = stripslashes(trim($keyword));
$folderID = ($folderID==1) ? 1 : 0;
$order = ($order==1) ? 1 : 0;
$field = ($field==1 || $field==2) ? $field : 0;
$root = returnFolderRoot($folderID);
$title = returnFolderTitle($folderID);
$tab = returnTabName($folderID);
$path = str_replace("..","",$path);
$path = stripslashes($path);
$li = new libfiletable($root, $path, $order, $field, $keyword);
$li->checkField = 1;

$lo = new libfilesystem();
$row = $lo->return_folder($root);
/*
if(sizeof($row) > 0){
     $titlebar  = "<select name=dest onChange=this.form.path.value=this.value;this.form.submit()>\n";
     $titlebar .= "<option>/</option>";
     for($i=sizeof($row)-1; $i>=0; $i--){
          $dest = str_replace($root, "", $row[$i]);
          $titlebar .= "<option value=\"$dest\" ".(($path == $dest) ? "SELECTED" : "").">$dest</option>";
     }
     $titlebar .= "</select>\n";
}
*/
# Not use directory jump
$titlebar = "";

$toolbar  = "<a class='iconLink' href=\"javascript:fs_newfolder(document.form1,'newfolder.php')\">".newIcon()."$button_newfolder</a>\n";
$functionbar .= "<a href=\"javascript:fs_rename(document.form1,'filename[]','rename.php')\"><img src='/images/admin/button/t_btn_rename_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:fs_unzip(document.form1,'filename[]','unzip.php')\"><img src='/images/admin/button/t_btn_unzip_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:fs_copy(document.form1,'filename[]','copy.php')\"><img src='/images/admin/button/t_btn_copy_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:fs_move(document.form1,'filename[]','move.php')\"><img src='/images/admin/button/t_btn_move_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'filename[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;<br>\n";
$searchbar  = "<a href=\"javascript:fs_fileupload(document.form1,'upload.php')\"><img src='/images/admin/button/t_btn_upload_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$searchbar .= " <input class=text type=text size=1 maxlength=1 name=no_file value=1> $i_FileSystemFiles \n";
$searchbar .= "&nbsp;<input class=text type=text name=keyword size=10 maxlength=20 value=\"".$keyword."\">\n";
$searchbar .= "<a href=\"javascript:document.form1.submit()\"><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_sf, '', $title, '') ?>
<?= displayTag("head_filesystem_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar($toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?php echo $li->display(); ?>
<?php echo $li->displayFunctionbar($titlebar, ""); ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=folderID value="<?php echo $folderID; ?>">
<input type=hidden name=folderName value="">
<input type=hidden name=newName value="">
<input type=hidden name=pageNo value="0">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=path value="<?php echo $path; ?>">
</form>

<?php
include("../../templates/adminfooter.php");
?>