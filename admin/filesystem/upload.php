<?php
die();
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
include("menu.php");

$root = returnFolderRoot($folderID);
$title = returnFolderTitle($folderID);
$path = str_replace("..","",$path);
$path = stripslashes($path);

?>

<form name="form1" action="uploadfile.php" method="post" enctype="multipart/form-data" onsubmit="Big5FileUploadHandler();return true"  >
<?= displayNavTitle($i_admintitle_sf, '', $title, 'javascript:history.back()', $button_upload, '') ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td>
<blockquote>
<br><br>
<table border="0" cellpadding="5" cellspacing="0">
<?php 
	for($i=0;$i<$no_file;$i++){ 
?>
		<?php echo "<tr><td>".($i+1); ?>: 
		<input class=file type=file name="userfile<?php echo $i; ?>" size=25></td></tr>
		<input type="hidden" name="userfileHidden<?=$i?>" />
<?php 
		$ParArr[] = "userfile{$i}";
		$ParArr[] = "userfileHidden{$i}";
	} 
	
	echo generateFileUploadNameHandler2("form1",$ParArr);
?>
</table>
<br><br>
</blockquote>
</td></tr>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_upload_<?=$intranet_session_language?>.gif" border="0">
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td></tr>
</table>


<input type=hidden name=no_file value="<?php echo $no_file; ?>">
<input type=hidden name=path value="<?php echo $path; ?>">
<input type=hidden name=folderID value="<?php echo $folderID; ?>">
</form>

<?php
include("../../templates/adminfooter.php");
?>