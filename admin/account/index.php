<?php
# using: yat

#######################################
#
#	Date:	2012-02-21	YatWoon
#			hide "User Account Mgmt" 
#
#######################################

include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_setting.php");

if($User<>""){
        $li_menu->is_access_function($User);
        $is_check_password = ($li_menu->is_access_password) ? " CHECKED" : "";
        $is_check_user = ($li_menu->is_access_user) ? " CHECKED" : "";
        $is_check_group = ($li_menu->is_access_group) ? " CHECKED" : "";
        $is_check_role = ($li_menu->is_access_role) ? " CHECKED" : "";
        $is_check_motd = ($li_menu->is_access_motd) ? " CHECKED" : "";
        $is_check_polling = ($li_menu->is_access_polling) ? " CHECKED" : "";
        $is_check_announcement = ($li_menu->is_access_announcement) ? " CHECKED" : "";
        $is_check_event = ($li_menu->is_access_event) ? " CHECKED" : "";
        $is_check_campusmail = ($li_menu->is_access_campusmail) ? " CHECKED" : "";
        $is_check_timetable = ($li_menu->is_access_timetable) ? " CHECKED" : "";
        $is_check_groupbulletin = ($li_menu->is_access_groupbulletin) ? " CHECKED" : "";
        $is_check_groupfiles = ($li_menu->is_access_groupfiles) ? " CHECKED" : "";
        $is_check_grouplinks = ($li_menu->is_access_grouplinks) ? " CHECKED" : "";
        $is_check_resource = ($li_menu->is_access_resource) ? " CHECKED" : "";
        $is_check_booking = ($li_menu->is_access_booking) ? " CHECKED" : "";
        $is_check_eclass = ($li_menu->is_access_eclass) ? " CHECKED" : "";
        $is_check_booking_periodic = ($li_menu->is_access_booking_periodic) ? " CHECKED" : "";
        $is_check_attendance = ($li_menu->is_access_attendance)? " CHECKED":"";
        $is_check_merit = ($li_menu->is_access_merit)? " CHECKED":"";
        $is_check_service = ($li_menu->is_access_service)? " CHECKED":"";
        $is_check_activity = ($li_menu->is_access_activity)? " CHECKED":"";
        $is_check_award = ($li_menu->is_access_award)? " CHECKED":"";
        $is_check_assessment = ($li_menu->is_access_assessment)? " CHECKED":"";
        $is_check_usage = ($li_menu->is_access_usage)? " CHECKED":"";
        $is_check_teaching = ($li_menu->is_access_teaching)? " CHECKED":"";
        $is_check_student_files = ($li_menu->is_access_student_files)? " CHECKED":"";
        $is_check_enrollment_approval = ($li_menu->is_access_enrollment_approval)? " CHECKED":"";
        $is_check_groupcategory = ($li_menu->is_access_groupcategory)? " CHECKED":"";

        $is_check_campustv = ($li_menu->is_access_campustv)? " CHECKED":"";
        $is_check_sms = ($li_menu->is_access_sms)? " CHECKED":"";

        $is_check_student_attendance = ($li_menu->is_access_student_attendance)? " CHECKED":"";
        $is_check_payment = ($li_menu->is_access_payment)? " CHECKED":"";
                
        $is_check_student_attendance2 = ($li_menu->is_access_student_attendance2)? " CHECKED":"";
        $is_check_guardian = ($li_menu->is_access_guardian)? " CHECKED":"";

		$is_check_websams = ($li_menu->is_access_websams)? " CHECKED":"";

}

# copy .htgroup to SystemHelperList.txt
$lf = new libfilesystem();
$source = $intranet_root ."/file/.htgroup";
$dest = $intranet_root ."/file/SystemHelperList.txt";
$lf->file_copy($source, $dest);
$dest = "/file/SystemHelperList.txt";
?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.AdminLogin, "<?php echo $i_alert_pleasefillin.$i_AdminLogin; ?>.")) return false;
        if(!checkRegEx2(obj.AdminLogin.value,"<?php echo $i_alert_pleasefillin.$i_AdminLogin; ?>")){ obj.AdminLogin.value=""; obj.AdminLogin.focus(); return false; }
        
        var is_in_option = false;
        for(i=0; i<obj.AdminLoginUser.length; i++)
        {
          if(obj.AdminLoginUser.options[i].value == obj.AdminLogin.value)
            is_in_option = true;
        }
        
        if(!is_in_option)
        {
          if(!check_text(obj.AdminPassword, "<?php echo $i_alert_pleasefillin.$i_AdminPassword; ?>.")) return false;
        }

        if(!checkRegEx(obj.AdminPassword.value,"<?php echo $i_alert_pleasefillin.$i_AdminPassword; ?>")){ obj.AdminPassword.value=""; obj.AdminPassword.focus(); return false; }
        if(countChecked(obj,"rights[]")==0) { alert(globalAlertMsg2); return false; }
}

function EditAdminLoginUser(){
                var obj = document.form1;
        if(check_select(obj.AdminLoginUser, globalAlertMsg18, "")){
                i = obj.AdminLoginUser.options[obj.AdminLoginUser.selectedIndex].value;
                self.location.href = "index.php?User=" + i;
        }
}

function RemoveAdminLoginUser(){
                var obj = document.form1;
        if(check_select(obj.AdminLoginUser, globalAlertMsg18, "")){
                if(confirm(globalAlertMsg3)){
                        obj.action = "delete.php";
                        obj.submit();
                }
        }
}
</script>

<form name="form1" action="update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_sa, '', $i_admintitle_sa_helper, '') ?>
<?= displayTag("head_sys_account_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
	<td align="right"><a href="<?=$dest?>" target="_blank" class="functionlink_new"><?=$Lang['SystemHelpter']['SavePageSettingsAsText']?></a></td>
</tr>	
<tr>
<td height=300>
<blockquote>
<p><br>
<p><?php echo $li_menu->list_user(); ?>
<a href="javascript:EditAdminLoginUser()"><img src="/images/admin/button/s_btn_edit_<?=$intranet_session_language?>.gif" border="0" align="absmiddle"></a>
<a href="javascript:RemoveAdminLoginUser()"><img src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0" align="absmiddle"></a></p>
<hr size=1 class="hr_sub_separator">
<p><?php echo $i_AdminLogin; ?><br>
<input class=text type=text name=AdminLogin size=10 maxlength=10 value="<?php echo $User; ?>"> <span class="extraInfo">(0-9a-zA-Z_)</span>
<p><?php echo $i_AdminPassword; ?><br><input class=password type=password name=AdminPassword size=10 maxlength=10> <span class="extraInfo">(0-9a-zA-Z)</span> <span class="extraInfo">(<?=$i_adminmenu_sa_pw_leave_blank?>)</span>
<p>
<table border=0 cellpadding=5 cellspacing=0>
<tr><td colspan=3><span class="extraInfo">[<span class=subTitle><?php echo $i_AdminRights; ?></span>]</span></td></tr>
<tr>
<td width=25% class=tableTitle_new><u><?php echo $i_menu_setting; ?></u></td>
<td width=25% class=tableTitle_new><u><?php echo $i_menu_intranet; ?></u></td>
<td width=25% class=tableTitle_new><u><?php echo $i_admintitle_eclass; ?></u></td> 
</tr>
<tr>
<td>
<input type=checkbox name=rights[] value=password <?php echo $is_check_password; ?>><?php echo $i_adminmenu_sa_password; ?>

<?
if ($plugin['tv'] || $plugin['sms'] || ($plugin['WebSAMS']))
{
?>
<p><span class="extraInfo"><?php echo $i_adminmenu_plugin; ?></span><br>
<? if ($plugin['tv']) {
?>
<input type=checkbox name=rights[] value=campustv <?php echo $is_check_campustv; ?>><?php echo $i_adminmenu_sc_campustv; ?><br>
<? }
if ($plugin['sms']) {
?>
<input type=checkbox name=rights[] value=sms <?php echo $is_check_sms; ?>><?php echo $i_SMS_SMS; ?><br>
<?
}

if ($plugin['WebSAMS']) {
?>
<input type=checkbox name=rights[] value=websams <?php echo $is_check_websams; ?>><?php echo $i_WebSAMS_Integration; ?><br>
<?
}


}
?>

</td>
<td>
<p><span class="extraInfo"><?php echo $i_adminmenu_am; ?></span><br>
<? /* ?>
<input type=checkbox name=rights[] value=user <?php echo $is_check_user; ?>><?php echo $i_adminmenu_am_user; ?><br>
<? */ ?>
<input type=checkbox name=rights[] value=usage <?php echo $is_check_usage; ?>><?php echo $i_adminmenu_am_usage; ?><br>
<p><span class="extraInfo"><?php echo $i_adminmenu_adm; ?></span><br>
<input type=checkbox name=rights[] value=attendance <?php echo $is_check_attendance; ?>><?php echo $i_Profile_Attendance; ?><br>
<input type=checkbox name=rights[] value=merit <?php echo $is_check_merit; ?>><?php echo $i_Profile_Merit; ?><br>
<input type=checkbox name=rights[] value=service <?php echo $is_check_service; ?>><?php echo $i_Profile_Service; ?><br>
<input type=checkbox name=rights[] value=activity <?php echo $is_check_activity; ?>><?php echo $i_Profile_Activity; ?><br>
<input type=checkbox name=rights[] value=award <?php echo $is_check_award; ?>><?php echo $i_Profile_Award; ?><br>
<input type=checkbox name=rights[] value=assessment <?php echo $is_check_assessment; ?>><?php echo $i_Profile_Assessment; ?><br>
<input type=checkbox name=rights[] value=student_files <?php echo $is_check_student_files; ?>><?php echo $i_Profile_Files; ?><br>
<input type=checkbox name=rights[] value=guardian <?php echo $is_check_guardian; ?>><?php echo $i_StudentGuardian['MenuInfo']; ?><br>
<? /* ?><p><span class="extraInfo"><?php echo $i_adminmenu_im; ?></span><br><? */ ?>
<input type=checkbox name=rights[] value=motd <?php echo $is_check_motd; ?>><?php echo $i_adminmenu_im_motd; ?><br>
<p><span class="extraInfo"><?php echo $i_adminmenu_rm; ?></span><br>
<input type=checkbox name=rights[] value=resource <?php echo $is_check_resource; ?>><?php echo $i_adminmenu_rm_item; ?><br>
<input type=checkbox name=rights[] value=booking <?php echo $is_check_booking; ?>><?php echo $i_adminmenu_rm_record; ?><br>
<input type=checkbox name=rights[] value=booking_periodic <?php echo $is_check_booking_periodic; ?>><?php echo $i_adminmenu_rm_record_periodic; ?><br>
</td>
<td><input type=checkbox name=rights[] value=eclass <?php echo $is_check_eclass; ?>><?php echo $i_eClass_Admin_MgmtCenter; ?></td>

</tr>
</table>
</blockquote>
</td>
</tr>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>
