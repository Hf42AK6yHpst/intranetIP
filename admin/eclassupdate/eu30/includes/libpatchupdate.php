<?php

function writePULog($the_string,$status='Patch',$refid='',$sid='')
{
	intranet_opendb();
        $pu = new libpatchupdate();
		$pu->create_patch_history_table($systemType);
        $pu->inserthistory($the_string,$status,$refid,$sid);
}

function writeEULog2($the_string,$status='Debug', $refid='')
{
	intranet_opendb();
        $pu = new libpatchupdate();
        $pu->insert_eu_history($the_string,$status,$refid);
}


class libpatchupdate extends libdb{

function libpatchupdate()
{

	 $this->libdb();
	 	 
}

function create_patch_history_table($systemType) {

if( $systemType == "junior" ) {
$sql = "CREATE TABLE IF NOT EXISTS `ECLASS_PATCH_HISTORY` (
  `HistoryID` int(8) NOT NULL auto_increment,
  `EventTime` datetime default NULL,
  `Events` text,
  `Status` varchar(16) default NULL,
  `ReferenceID` int(11) default NULL,
  `SessionID` varchar(40) default NULL,  
  PRIMARY KEY  (`HistoryID`), KEY `Status` (`Status`), KEY `SessionID` (`SessionID`), KEY `ReferenceID` (`ReferenceID`)
) ";

} else 
{
$sql = "CREATE TABLE IF NOT EXISTS `ECLASS_PATCH_HISTORY` (
  `HistoryID` int(8) NOT NULL auto_increment,
  `EventTime` datetime default NULL,
  `Events` text,
  `Status` varchar(16) default NULL,
  `ReferenceID` int(11) default NULL,
  `SessionID` varchar(40) default NULL,  
  PRIMARY KEY  (`HistoryID`), KEY `Status` (`Status`), KEY `SessionID` (`SessionID`), KEY `ReferenceID` (`ReferenceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

}
$this->db_db_query($sql);
}



function inserthistory($events,$status,$refid,$sid) {
	
	$sql = "INSERT INTO ECLASS_PATCH_HISTORY (EventTime,Events,Status,ReferenceID,SessionID) VALUES (NOW(),'$events','$status','$refid','$sid')";
	//print $sql;
	$this->db_db_query($sql);
}

function eclassdblist($eclass_prefix) {
    $sql = "SHOW DATABASES LIKE '".$eclass_prefix."c%'";	
	return $this->returnArray($sql,1);
}


function intranetdblist($intranet_db,$systemType) {
    $sql = "SHOW DATABASES LIKE '".$intranet_db."_DB_REPORT_CARD_%'";	

	return $this->returnArray($sql,1);
}

function insert_eu_history($events,$status,$refid) {
	
	$sql = "INSERT INTO ECLASS_UPDATE_HISTORY (EventTime,Events,Status,ReferenceID) VALUES (NOW(),'$events','$status','$refid')";
	//print $sql;
	$this->db_db_query($sql);
}


}
?>