<?
//include_once("../../../includes/global.php");
include_once($intranet_root."/includes/global.php");

$eu_version_no = "20100924";
$eclassuser = 'eclass';

/*------------------- Update Server Detail -------------------*/
// eClass update server IP address
$update_server = "eclassupdate.broadlearning.com";


// Download location at eClass update server
$package_folder = "eupdate/epackage";	# Location of tar file
$package_http_root = "http://$update_server/$package_folder";	

// API at eClass update server
$package_api_root = "https://$update_server/api";


// XML at eClass update server
$eclass_package_xml = "https://$update_server/api/packages.php";
$intranet_package_xml = "https://$update_server/api/packages.php";


/*------------------- Import eClass global.php -------------------*/
// Mysql login name and password
$mysql_login = $intranet_db_user;
$mysql_root = $intranet_db_pass;



/*------------------- Eclass update variables -------------------*/
// Output name of eclass backup
$backupEclassFileName=$_SERVER['SERVER_NAME']."_EAU_eclass";


// Output name of MYSQL backup
$backupMysqlFileName=$_SERVER['SERVER_NAME']."_EAU_mysql";

// Mysql location
$mysql_path = "/var/lib/mysql";


// Email to receive the update log
$admministrator_email = "server@broadlearning.com";

$mode = ini_get('zend.ze1_compatibility_mode');
ini_set('zend.ze1_compatibility_mode', '0');

// temp folder in server
$package_download_folder="/tmp/eclassupdate";

if(strpos($intranet_root, "intranet20")) {
        $systemType = "junior";
        $eclass_path = substr($intranet_root,0,strpos($intranet_root,'intranet20')-1);
        $eclass_root = $eclass_path."/eclass30";
	$backup_folder = "/home/juniorBackup";
} else {
        $systemType = "eclass";
        $eclass_path = substr($intranet_root,0,strpos($intranet_root,'intranetIP')-1);
        $eclass_root = $eclass_path."/eclass40";
	$backup_folder = "/home/eclassBackup";
}


?>
