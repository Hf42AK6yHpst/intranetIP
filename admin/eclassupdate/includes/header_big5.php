<style type="text/css"> 

#eu-header{
	width: 100%;
        padding: 0 0 0 0;
        
}

#eu-step-1, #eu-step-2, #eu-step-3, #eu-step-4, #eu-step-5{
	border:solid 1px #DEDEDE; 
	background:#F7FBFF;
	color:#222222;
	padding:2px;
        margin: 0 1px 15px 1px;
	text-align:center;
	float: left;
	font-size: 9pt;
}

#dl-wrapper {
	width:100%;
        margin: 10px 0 0 0;
}

#dl-redownload, #dl-download {
	width:100%;
	float: left;
}

.progress_wrapper {width:800px;border:1px solid #ccc;position:absolute;}
.progress {height:10px;background-color:#000}
.progress_info {background-color:#fff}

/****** Final ******/
#final-wrapper {
        padding: 2px;
        margin: 0 0 10px 0;
        background: #f4f4f4;
        color: #246088;
        border-top: 1px solid #4A8EBC;
        border-bottom: 1px solid #4A8EBC;
}


/****** Extract ******/
#extract-source-wrapper, #extract-mysql-wrapper{
        padding: 2px;
        margin: 0 0 10px 0;
        background: #FFFFFF;
        color: #246088;
        font-family: arial;
        font-size: 9pt;
}


/****** Backup ******/
#system-bl-wrapper, #mysql-dl-wrapper{
        padding: 2px;
        margin: 0 0 10px 0;
        background: #FFFFFF;
        color: #246088;
        
}

/****** Download ******/
#intranet-dl-wrapper, #eclass-dl-wrapper{
        padding: 2px;
        margin: 0 0 10px 0;
        background: #FFFFFF;
        color: #246088;
        
}

/****** Pre checking ******/
#prechecking, #download-wrapper{
        padding: 10px;
        margin: 0 0 10px 0;
        background: #FFFFFF;
        color: #246088;
        border-top: 1px solid #4A8EBC;
        border-bottom: 1px solid #4A8EBC;
}

#prechecking .upgradeinfo{
        padding: 10px;
        margin: 0 0 10px 0;
        background: #A9D4F2;
        color: #246088;
        border-top: 1px solid #4A8EBC;
        border-bottom: 1px solid #4A8EBC;
}


</style>

<?php if ($intranet_session_language == 'b5') {
	?>

	<div id="eu-header">
	<div id="eu-step-1">1. 預備</div>
	<div id="eu-step-2">2. 下載</div>
	<div id="eu-step-3">3. 系統測試</div>
	<div id="eu-step-4">4. 進行更新</div>
	<div id="eu-step-5">5. 完成</div>
    </div>
		
<?php
} else {
	?>
	<div id="eu-header">
	<div id="eu-step-1">1. Preparation</div>
	<div id="eu-step-2">2. Download</div>
	<div id="eu-step-3">3. System Verification</div>
	<div id="eu-step-4">4. Update</div>
	<div id="eu-step-5">5. Finish</div>
    </div>
	
<?php
}
	
?>



<SCRIPT LANGUAGE="javascript">
function showHeader(step){
	if ( step == 1)
		document.getElementById('eu-step-1').style.backgroundColor = "#DCF2B8";
	else if ( step == 2)
		document.getElementById('eu-step-2').style.backgroundColor = "#DCF2B8";
	else if ( step == 3)
		document.getElementById('eu-step-3').style.backgroundColor = "#DCF2B8";
	else if ( step == 4)
		document.getElementById('eu-step-4').style.backgroundColor = "#DCF2B8";
	else 
		document.getElementById('eu-step-5').style.backgroundColor = "#DCF2B8";
}
</SCRIPT>
<BR>
<BR>


