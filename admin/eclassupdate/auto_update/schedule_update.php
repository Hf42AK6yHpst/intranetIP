<?php
//using by 
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclassupdate.php");

$eu = new libeclassupdate();

if ($setschedule == '1' and $enableupdate == '1') { #check form submission
	$correctmin = $eminute * 15;
if ($UpdateMethod=="daily") { # Daily Update
	
	$cronjob = array($correctmin,$ehour,'*','*','*');
	$eu->setcrontab($cronjob, $scode);	
	$check_enableupdate = "checked";
	
} else if ($UpdateMethod=="weekly") {  # Weekly Update
	
	if (isset($edays)) {
    $cronjob = array($correctmin,$ehour,'*','*',implode(",",$edays));
	$eu->setcrontab($cronjob, $scode);	
	$check_enableupdate = "checked";
}
	}else if ($UpdateMethod=="onetime") { # Onetime Update
	
	$currdate = explode("-",$edate);
	
	$cronjob = array($correctmin,$ehour,$currdate[1],$currdate[0],'*');
	
	$eu->setcrontab($cronjob, $scode);	
	
	$check_enableupdate = "checked";	
	} 

	
} else if ($setschedule == '1' and $enableupdate == '' ) { # Disable Update
	$eu->disablecrontab();
	
	}

	
else {
	
$currentmethod = $eu->updatemethod(); # Check current update method

if ($currentmethod == 'unknown' or $currentmethod == 'disabled'){ # 
	$check_enableupdate = "";
	} else {$check_enableupdate = "checked";}

}

# New Varialbes
$i_adminmenu_sc_eclass_update = "eClass Update";
$i_manual_update = "Manual Update";
$i_auto_update = "Auto Update";

$UpdateMethodValues['daily'] = "Daily";
$UpdateMethodValues['weekly'] = "Weekly";
$UpdateMethodValues['onetime'] = "At a specified time";

$UpdateMethod = isset($UpdateMethod)? $UpdateMethod : "daily";
$hour = isset($ehour)?sprintf('%02d',$ehour):"00";
$minute = isset($eminute)?sprintf('%02d',$eminute*15):"00";
if($UpdateMethod == "daily")
{
	$UpdateTime = $hour.":".$minute;
}else if($UpdateMethod == "weekly")
{
	$days=array();
	if(isset($edays))
	{
		for($i=0;$i<sizeof($edays);$i++)
		{
			switch($edays[$i])
			{
				case 0: $days[]="Sunday";
				$sunday_checked = "checked";
						break;
				case 1: $days[]="Monday";
				$monday_checked = "checked";
						break;
				case 2: $days[]="Tuesday";
				$tuesday_checked = "checked";
						break;
				case 3: $days[]="Wednesday";
				$wednesday_checked = "checked";
						break;
				case 4: $days[]="Thursday";
				$thursday_checked = "checked";
						break;
				case 5: $days[]="Friday";
				$friday_checked = "checked";
						break;
				case 6: $days[]="Saturday";
				$saturday_checked = "checked";
						break;
			}
		}
	}
	$day = implode(', ',$days);
	$UpdateTime = $day." ".$hour.":".$minute;
}else if($UpdateMethod == "onetime")
{
	$date = isset($edate)?$edate:date("Y-m-d");
	$UpdateTime = $date." ".$hour.":".$minute;
}



?>


<?= displayNavTitle($i_admintitle_sa, '', $i_adminmenu_sc_eclass_update, '../index.php', $i_auto_update, '') ?>
<?= displayTag("head_eclass_update_$intranet_session_language.gif", $msg) ?>

<script type="text/javascript">
<!--
function go(){
document.location.href = '?UpdateMethod='+document.form1.UpdateMethod.options[document.form1.UpdateMethod.selectedIndex].value
}
//-->
</script>



<form name="form1" id="form1" method="POST" action="<?= $SCRIPT_NAME; ?>" >
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300>
<?php $eu->checkschedule();?><br><br>
Enable Auto Update: <input type=checkbox name="enableupdate" value=1 <?php echo $check_enableupdate ; ?>><br>
<br>
Frequency<br><br>
<select id="UpdateMethod" name="UpdateMethod" onchange="go();" >
<option value="daily" <?if($UpdateMethod == "daily") echo "selected";?> >Daily</option>
<option value="weekly" <?if($UpdateMethod == "weekly") echo "selected";?>>Weekly</option>
<option value="onetime" <?if($UpdateMethod == "onetime") echo "selected";?>>At a specified time</option>
</select>
<br><br>
Schedule settings<br><br>
<?
if($UpdateMethod == "daily")
{
?>

Time: <select name="ehour" id="ehour" onChange='tickTime(this.form)'>
<?
for($i=0;$i<24;$i++)
{
	echo "<option value=".$i;
	if($ehour == $i) echo " selected";
	echo ">".$i."</option>";
}
?>
</select>:<select name="eminute" id="eminute" onChange='tickTime(this.form)'>
<option value=0 <?if($eminute==0) echo "selected" ?>>00</option>
<option value=1 <?if($eminute==1) echo "selected" ?>>15</option>
<option value=2 <?if($eminute==2) echo "selected" ?>>30</option>
<option value=3 <?if($eminute==3) echo "selected" ?>>45</option>
</select>
<br><br>
<input type=button name="updatesubmit" value="Update Schedule" onClick='form1.submit();'>
<input type=hidden name="setschedule" value="1">
<br><br>

<?
}else if($UpdateMethod == "weekly")
{
?>

Day of Week: 
<input type=checkbox name="edays[]" id="esunday" value=0 <?php echo $sunday_checked;?>>Sun</input>
<input type=checkbox name="edays[]" id="emonday" value=1 <?php echo $monday_checked;?>>Mon</input>
<input type=checkbox name="edays[]" id="etuesday" value=2 <?php echo $tuesday_checked;?>>Tue</input>
<input type=checkbox name="edays[]" id="ewednesday" value=3 <?php echo $wednesday_checked;?>>Wed</input>
<input type=checkbox name="edays[]" id="ethursday" value=4 <?php echo $thursday_checked;?>>Thr</input>
<input type=checkbox name="edays[]" id="efriday" value=5 <?php echo $friday_checked;?>>Fri</input>
<input type=checkbox name="edays[]" id="esaturday" value=6 <?php echo $saturday_checked;?>>Sat</input>
<br>
Time: <select name="ehour" id="ehour" onChange='tickTime(this.form)'>
<?	
for($i=0;$i<24;$i++)
{
	echo "<option value=".$i;
	if($ehour == $i) echo " selected";
	echo ">".$i."</option>";
}
?>
</select>:<select name="eminute" id="eminute" onChange='tickTime(this.form)'>
<option value=0 <?if($eminute==0) echo "selected" ?>>00</option>
<option value=1 <?if($eminute==1) echo "selected" ?>>15</option>
<option value=2 <?if($eminute==2) echo "selected" ?>>30</option>
<option value=3 <?if($eminute==3) echo "selected" ?>>45</option>
</select>

<br><br>
<input type=button name="UpdateSubmit" value="Update Schedule" onClick='form1.submit();'>
<input type=hidden name="setschedule" value="1">
<br><br>

<?
}else
{
?>

Date: <input type=text name="edate" id="edate" value=<?php echo $edate;?>></input>(MM-DD)<br>
Time: <select name="ehour" id="ehour">
<?	
for($i=0;$i<24;$i++)
{
	echo "<option value=".$i;
	if($ehour == $i) echo " selected";
	echo ">".$i."</option>";
}
?>
</select>:<select name="eminute" id="eminute">
<option value=0 <?if($eminute==0) echo "selected" ?>>00</option>
<option value=1 <?if($eminute==1) echo "selected" ?>>15</option>
<option value=2 <?if($eminute==2) echo "selected" ?>>30</option>
<option value=3 <?if($eminute==3) echo "selected" ?>>45</option>
</select>

<br><br>
<input type=button name="UpdateSubmit" value="Update Schedule" onClick='form1.submit();'>
<input type=hidden name="setschedule" value="1">
<br><br>

<?
}
?>


</td></tr>
</table>
</from>
<?
include_once("../../../templates/adminfooter.php");

?>