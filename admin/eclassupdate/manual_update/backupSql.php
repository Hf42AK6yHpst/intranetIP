<? 
include("../includes/global.php"); // Import system variables
include("../includes/functions.php"); // Import functions libraries
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclassupdate.php");

intranet_opendb();
$eu = new libeclassupdate();

$eclassdbarray = $eu->eclassdblist($eclass_prefix);
$separator = 'UTF8';

// Create Database list
for ($i=0;$i<sizeof($eclassdbarray);$i++) {
        if (strstr($eclassdbarray[$i][0], $separator) == false)
                $eclassdblist .= " ".$eclassdbarray[$i][0];
}

$dbbackup = $intranet_db." ".$eclass_db." ".trim($eclassdblist);


//Define variables
$progress_counter = 0;
$makeBackup = $_REQUEST["backupPath"] . "/" .  $_REQUEST["backupFileName"];


// Call shell script to backup SQL
if( $systemType == "junior" )
	$command2 = "scripts/update.sh backupsql " . $mysql_login . " " . $mysql_root . " " . $makeBackup . " " . "\"$dbbackup\"";
else
	$command2 = "sudo -u eclass scripts/update.sh backupsql " . $mysql_login . " " . $mysql_root . " " . $makeBackup . " " . "\"$dbbackup\"";

$ps2 = run_in_background("$command2");

while(is_process_running($ps2))
{
	$progress_counter++;

	if ( ($progress_counter % 100) == 0)
		//echo("#");
                echo str_repeat(" ", 4096);
	       	ob_flush(); 
		flush();
	        usleep(5000);
}

$alertmessage = "Backup mysql finish";

if ( $_REQUEST["backupIdentifier"] == "eclassMysql" ){
        echo '<script language="javascript">';
        echo 'location.href = "backup.php?doAction=verify&backupIdentifier=eclassMysql&alertmessage=' . $alertmessage . '";';
        echo '</script>';
}

?>
