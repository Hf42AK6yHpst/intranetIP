#!/bin/bash

# Parameters
TIMESTAMP=`date +%Y%m%d_%H%M`
LOGDIRECTORY="./Log"

#=================
# Check Parameter
#=================

if [ $# -eq 4 ]
then :
else
        echo "Usage: ./backupSql.sh <account name> <password> <backup path>"
        exit
fi

#=================
# MysqlDump
#=================

#mysqldump -u$1 -p$2 --all-databases > $3

mysqldump -u$1 -p$2 --databases $4 > $3

