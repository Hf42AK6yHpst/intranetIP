#!/bin/sh

FILE=$1

SAVE_FLAG=`tail -2 $FILE | grep "saved"`

if [ ! -z "$SAVE_FLAG" ]; then
	RESULT=`tail -2 $FILE | head -n1 | awk '{ print $7 "-" $1}'`
	echo "Finish-"$RESULT
else	
	RESULT=`tail -2 $FILE | head -n1 | awk '{ print $1 "-" $7 "-" $9}'`
	echo "Downloading...-"$RESULT
fi

