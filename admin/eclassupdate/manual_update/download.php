<? 
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../includes/libdb.php");
include("../includes/global.php"); // Import system variables
include("../includes/functions.php"); // Import functions libraries

// Import language files
if( $systemType == "junior" ){
        include_once("../lang/lang.$intranet_session_language.eu_big5.php");
}else{
        include_once("../lang/lang.$intranet_session_language.eu_utf8.php");
}

session_start();

// Get variables 
$doAction = $_REQUEST["doAction"];
#$eclass_package_file_location = "$package_download_folder/$eclass_package_filename";	# path in local disk
$intranet_package_file_location = "$package_download_folder/$intranet_package_filename"; # path in local disk


// Log Variables
$showVars = "[systemType:" . $systemType . "|doAction:" . $doAction . "|eclass_package_file_location:" . $eclass_package_file_location;
$showVars .= "|intranet_package_file_location:" . $intranet_package_file_location . "]";
writeEULog("$showVars");


// Download the source code
if ($doAction == "download") {
include_once("../../../templates/adminheader_setting.php");?>
<?= displayNavTitle($i_admintitle_sa, '', $i_adminmenu_sc_eclass_update, '', $i_manual_update, '') ?>
<?= displayTag("head_eclass_update_$intranet_session_language.gif", $msg) ?>
<table width=590 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300>
<?
if( $systemType == "junior" )
        include("../includes/header_big5.php"); // Include big5 header
else
        include("../includes/header_utf8.php"); // Include utf-8 header


        echo "<script language=\"JavaScript\" type=\"text/javascript\">";
        echo "{ showHeader(2); }";
        echo "</script>";
        echo "<div id=download-wrapper>";

	writeEULog ("eClassUpdate package is downloading to your server.<br>");
        echo $i_eu_download_msg;
	if ((trim($current_intranet_version) != trim($intranet_package_version)) && (trim($current_eclass_version) != trim($eclass_package_version))){
		startIntranetDownload($systemType);
		sleep(2);
               	showDownloadButton();
        } else if ((trim($current_intranet_version) != trim($intranet_package_version)) && (trim($current_eclass_version) == trim($eclass_package_version))){
                startIntranetDownload($systemType);
                showDownloadButton("$i_eu_next_button");
        } else if ((trim($current_intranet_version) == trim($intranet_package_version)) && (trim($current_eclass_version) != trim($eclass_package_version))){
                startEclassDownload();
                showDownloadButton();
       	} else {
		echo "no download";
	}
	
       	echo "<SCRIPT LANGUAGE=javascript>";
	echo 'document.forms["backupsource"].Submit.disabled=true;';
        echo "</SCRIPT>";
}


// Re-download the source code 
if ($doAction == "redownload") {
	include_once("../../../templates/adminheader_setting.php");

if( $systemType == "junior" )
        include("../includes/header_big5.php"); // Include big5 header
else
        include("../includes/header_utf8.php"); // Include utf-8 header


        echo "<script language=\"JavaScript\" type=\"text/javascript\">";
        echo "{ showHeader(2); }";
        echo "</script>";
	echo str_repeat(" ", 4096);
	echo "<BR>";
	Show_and_write_log ("Re-downloading...");

        # Delete the file and re-download 
        passthru("rm -rf ". "$eclass_package_file_location");
        passthru("rm -rf ". "$intranet_package_file_location");

	//sleep(1);
	startIntranetDownload($systemType);
	startEclassDownload();
        showDownloadButton();

       	echo "<SCRIPT LANGUAGE=javascript>";
       	echo 'document.forms["backupsource"].Submit.disabled=true;';
        echo "</SCRIPT>";

	// Check MD5 of the file
        $checkResult = checkFileMD5($package_file_location);
        if ( "$checkResult" == "$package_md5") {
                echo "<SCRIPT LANGUAGE=javascript>";
                echo 'parent.document.backupsource.Submit.disabled=false;';
                echo "</SCRIPT>";
                Show_and_write_log ($i_eu_download_md5_success);
        } else {
                Show_and_write_log ($i_eu_download_md5_fail);

		if( $systemType == "junior" ) {
		        $command = "sudo -u junior scripts/update.sh clearEUfiles " . $package_download_folder;
		        $ps = run_in_background("$command");
		} else {
		        $command = "sudo -u eclass scripts/update.sh clearEUfiles " . $package_download_folder;
		        $ps = run_in_background("$command");
		}
		echo "<BR>$i_eu_check_file";
		echo "<BR>$i_eu_check_reload";
        }

}



if ( $doAction == "verify" ){
	passthru("rm -rf /tmp/eclassupdate/wgetresultintranet.txt");
	passthru("rm -rf /tmp/eclassupdate/wgetresulteclass.txt");
	print "<body bgcolor=#FFFFFF><font face=arial size=-1 color=#246088>";


	// Log Variables
	$showVars = "[doAction:" . $doAction . "|intranet_package_download_complete:" . $intranet_package_download_complete;
	$showVars .= "|eclass_package_download_complete:" . $eclass_package_download_complete . "]";
	writeEULog("$showVars");


	if (($intranet_package_download_complete == "true") && ($eclass_package_download_complete == "true")){
		writeEULog ("Verifing Intranet file...");

		// Get MD5 value of downloaded file
		$checkIntranet = checkFileMD5($intranet_package_file_location);
		writeEULog ("intranet_package_file_location: $intranet_package_file_location");
		writeEULog ("MD5 from Intranet XML: $intranet_package_md5");
		writeEULog ("MD5 of download Intranet file: $checkIntranet");
		writeEULog ("Verifing eclass file...");

		// Get MD5 value of downloaded file
		$checkEclass = checkFileMD5($eclass_package_file_location);
		writeEULog ("MD5 from Eclass XML: $eclass_package_md5");
		writeEULog ("MD5 of download Eclass file: $checkEclass");

		//Check MD5 of file
		if (("$checkIntranet" == "$intranet_package_md5") && ("$checkEclass" == "$eclass_package_md5")){
        	        echo "<SCRIPT LANGUAGE=javascript>";
	                echo 'parent.document.backupsource.Submit.disabled=false;';
       		        echo "</SCRIPT>";
			writeEULog ("[Download completed!!]<br><br> Please click the button to begin the upgrade process. ");
			writeEULog ("Md5 Checking success");
	                Show_and_write_log ($i_eu_download_md5_success);
			
		} else {
			Show_and_write_log ($i_eu_download_md5_fail);
			echo "<BR>$i_eu_check_reload";
		        passthru("$intranet_package_file_location");
		}
	} else if (($intranet_package_download_complete == "true") && ($eclass_package_download_complete != "true")){
                writeEULog ("Verifing Intranet file...");

                // Get MD5 value of downloaded file
                $checkIntranet = checkFileMD5($intranet_package_file_location);
               
                writeEULog ("intranet_package_file_location: $intranet_package_file_location");
                writeEULog ("MD5 from Intranet XML: $intranet_package_md5");
                writeEULog ("MD5 of download Intranet file: $checkIntranet");

                //Check MD5 of file
                if (("$checkIntranet" == "$intranet_package_md5")){
                        echo "<SCRIPT LANGUAGE=javascript>";
                        echo 'parent.document.backupsource.Submit.disabled=false;';
                        echo "</SCRIPT>";
                        echo $i_eu_download_completed."<br><br>".$i_eu_download_next;
                        writeEULog ("[Download completed!!]<br><br> Please click the button to begin the upgrade process. ");
			writeEULog ($i_eu_download_md5_success);
                } else {
                        Show_and_write_log ($i_eu_download_md5_fail);

	                if( $systemType == "junior" ) {
        	                $command = "sudo -u junior scripts/update.sh clearEUfiles " . $package_download_folder;
	               	        $ps = run_in_background("$command");
        	        } else {
                	        $command = "sudo -u eclass scripts/update.sh clearEUfiles " . $package_download_folder;
                        	$ps = run_in_background("$command");
	                }

			echo "<BR>$i_eu_check_file";
			echo "<BR>$i_eu_check_reload";
		        passthru("$intranet_package_file_location");

                }
        } else if (($intranet_package_download_complete != "true") && ($eclass_package_download_complete == "true")){
                writeEULog ("Verifing eclass file...");

                // Get MD5 value of downloaded file
                $checkEclass = checkFileMD5($eclass_package_file_location);
                writeEULog ("MD5 from Eclass XML: $eclass_package_md5");
                writeEULog ("MD5 of download Eclass file: $checkEclass");

                //Check MD5 of file
                if (("$checkEclass" == "$eclass_package_md5")){
                        echo "<SCRIPT LANGUAGE=javascript>";
                        echo 'parent.document.backupsource.Submit.disabled=false;';
                        echo "</SCRIPT>";
			Show_and_write_log ("[Download completed!!]<br><br> Please click the button to begin the upgrade process. ");
			writeEULog ($i_eu_download_md5_success);
                } else {
                        Show_and_write_log ($i_eu_download_md5_fail);
			echo "Trying to redownload files.....";

	                if( $systemType == "junior" ) {
        	                $command = "sudo -u junior scripts/update.sh clearEUfiles " . $package_download_folder;
                	        $ps = run_in_background("$command");
	                } else {
        	                $command = "sudo -u eclass scripts/update.sh clearEUfiles " . $package_download_folder;
	               	        $ps = run_in_background("$command");
        	        }

			echo "<BR>$i_eu_check_file";
			echo "<BR>$i_eu_check_reload";
		        passthru("$intranet_package_file_location");
                }
        } else 
		echo "No Download!!";
}

echo "</div>";

if ($doAction!="verify") { ?>
	</td></tr>
</table>
<?
include_once("../../../templates/adminfooter.php");
}
?>
