<?php
include("../includes/global.php"); // Import system variables
include("../includes/functions.php"); // Import functions libraries
session_start();

if (!checkFileMD5("$package_download_folder/".$intranet_package_filename) == $intranet_package_md5) {
	// Call shell script to download the file
	writeEULog ("scripts/downloadintranet.sh $intranet_package_url $package_download_folder/$intranet_package_filename $package_download_folder/wgetresultintranet.txt &");
	exec("scripts/downloadintranet.sh $intranet_package_url $package_download_folder/$intranet_package_filename $package_download_folder/wgetresultintranet.txt &");
} else {
	exec("echo $package_download_folder/$intranet_package_filename saved > $package_download_folder/wgetresultintranet.txt");
	writeEULog ("$package_download_folder/$intranet_package_filename found in server already.");	
}


?>
