<?
include_once("../../../includes/global.php");
include("../includes/global.php"); // Import system variables
include("../includes/functions.php"); // Import functions libraries


// Define variables
$progress_counter = 0;
session_start();
session_register('MysqlVerifyComplete');


// Call shell script to verify mysql
$exeCommand = "cat /tmp/eclassupdate/mysqlVerify.txt | grep FAIL";
$result = exec($exeCommand);

if ( "$result" != "" ) {
        writeEULog ("Mysql verify fail");
       	$alertmessage = "Mysql verify fail";
        $MysqlVerifyComplete = false;
} else {
       	writeEULog ("Mysql verify success");
        $alertmessage = "Mysql verify success";
        $MysqlVerifyComplete = true;
}

echo '<script language="javascript">';
echo 'location.href = "backup.php?doAction=extract&alertmessage=' . $alertmessage . '";';
echo '</script>';

?>
