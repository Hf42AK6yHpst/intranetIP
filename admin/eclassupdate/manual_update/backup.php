<? 
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../includes/libdb.php");
include("../includes/global.php"); // Import system variables
include("../includes/functions.php"); // Import functions libraries

session_start();
session_register('show_extract_button');

// Define variables
$doAction = $_REQUEST["doAction"];
$backupIdentifier = $_REQUEST["backupIdentifier"];
$alertmessage = $_REQUEST["alertmessage"];
$backupEclassFileName=$backupEclassFileName . "_" . $programStartTime . ".tar.gz";
$backupMysqlFileName=$backupMysqlFileName . "_" . $programStartTime . ".sql";
$skipBackup = $_REQUEST["skipBackup"];

// Import language files
if( $systemType == "junior" ){
        include_once("../lang/lang.$intranet_session_language.eu_big5.php");
}else{
        include_once("../lang/lang.$intranet_session_language.eu_utf8.php");
}


// Log Variables
$showVars = "[systemType:" . $systemType . "|doAction:" . $doAction . "|backupIdentifier:" . $backupIdentifier  . "]";
writeEULog("$showVars");


/* Backup eclass source code /home/eclass/{eclass30,intranetIP} or /home/junior/junior20/{eclass30,intranetIP} */
// Implementation of backup process

if ( $doAction == "backup" )
{

	include_once("../../../templates/adminheader_setting.php");?>
	<?= displayNavTitle($i_admintitle_sa, '', $i_adminmenu_sc_eclass_update, '', $i_manual_update, '') ?>
	<?= displayTag("head_eclass_update_$intranet_session_language.gif", $msg) ?>
	<table width=590 border=0 cellpadding=0 cellspacing=0 align=center>
	<td class=tableContent height=400>
	<?
	if( $systemType == "junior" )
       		include("../includes/header_big5.php"); // Include big5 header
	else 
       		include("../includes/header_utf8.php"); // Include utf-8 header

	echo "<script language=\"JavaScript\" type=\"text/javascript\">";
	echo "{ showHeader(3); }";
	echo "</script>";
        echo "<div id=backup-bl-wrapper>";
	echo "<BR>$i_eu_verify_msg<BR>";

	# Start eclass source backup
	# backupFileName: File name for created backup
	# backupEclassFilePath: Path to be backup
	# backupIdentifier: will return to backuk.php to determine action after backupAction.php

	if (($upgrade_intranet) && ($upgrade_eclass)) 
		$backupEclassFilePath = $intranet_root . " " . $eclass_root;
	else if ($upgrade_intranet) 
		$backupEclassFilePath = $intranet_root . " " . $eclass_root;
	else if ($upgrade_eclass)
                $backupEclassFilePath = $eclass_root;
	else
                $backupEclassFilePath = $intranet_root . " " . $eclass_root;

        echo "<div id=system-bl-wrapper>";
	echo "$i_eu_verifying...<iframe src=\"backupAction.php?backupFileName=$backupEclassFileName&backupFilePath=$backupEclassFilePath&backupIdentifier=eclassSource&backupPath=$backup_folder&systemType=$systemType\" name=\"Download\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height=\"35%\" width=\"100%\" marginheight=\"0\" style=\"overflow-x: hidden; overflow-y: hidden; border-width: 0px;\"></iframe>";
        writeEULog ("Backup eClass System");
        echo "</div>";

	// Start mysql backup
	echo "<div id=mysql-dl-wrapper>";
	echo "<iframe src=\"backupSql.php?backupFileName=$backupMysqlFileName&backupFilePath=$backupMysqlFilePath&backupIdentifier=eclassMysql&backupPath=$backup_folder\" name=\"Download\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height=\"150\" width=\"100%\" marginheight=\"0\" style=\"overflow-x: hidden; overflow-y: hidden; border-width: 0px;\"></iframe>";
        writeEULog ("Backup Mysql");
        echo "</div>";
}


// Verify eclass source backup
if ( $doAction == "verify" )
{
	$show_extract_button = true;
	if ( $backupIdentifier == "eclassSource") {
	        writeEULog ("Verifing Eclass Source ...");
		if( $systemType == "junior" )
			echo "<iframe src=\"backupVerifySource_EJ.php?backupFileName=$backupEclassFileName&backupPath=$backup_folder\" name=\"Download\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height=\"\" width=\"100%\" marginheight=\"0\" style=\"overflow-x: hidden; overflow-y: hidden; border-width: 0px;\"></iframe>";
		else
			echo "<iframe src=\"backupVerifySource_IP.php?backupFileName=$backupEclassFileName&backupPath=$backup_folder\" name=\"Download\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height=\"\" width=\"100%\" marginheight=\"0\" style=\"overflow-x: hidden; overflow-y: hidden; border-width: 0px;\"></iframe>";
	}

	if ( $backupIdentifier == "eclassMysql") {
	        writeEULog ("Verifing Mysql Backup...");
		if( $systemType == "junior" )
			echo "<iframe src=\"backupVerifyMysql_EJ.php?backupFileName=$backupMysqlFileName&backupPath=$backup_folder\" name=\"Download\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height=\"\" width=\"100%\" marginheight=\"0\" style=\"overflow-x: hidden; overflow-y: hidden; border-width: 0px;\"></iframe>";
		else
			echo "<iframe src=\"backupVerifyMysql_IP.php?backupFileName=$backupMysqlFileName&backupPath=$backup_folder\" name=\"Download\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height=\"\" width=\"100%\" marginheight=\"0\" style=\"overflow-x: hidden; overflow-y: hidden; border-width: 0px;\"></iframe>";
	}
}


// Go to extract page
if ( $doAction == "extract" )
{
	if ($skipBackup == true) {
		$upgrade_intranet = true;
		$MysqlVerifyComplete = true;
		$SourceVerifyComplete = true;
		$show_extract_button = true;
	}


	// Log Variables (backup.php)
	$showVars = "[doAction:" . $doAction . "|SourceVerifyComplete:" . $SourceVerifyComplete . "|MysqlVerifyComplete:" . $MysqlVerifyComplete . "]";
	writeEULog("$showVars");

	if ($upgrade_intranet) {
	     if ($MysqlVerifyComplete == false) {
		redirectErrorPage("Mysql verification error");
	     }
		  
	     if ($SourceVerifyComplete == false) {
	      redirectErrorPage("System verification error");
		  
	     }

		if (($SourceVerifyComplete == true) && ($MysqlVerifyComplete == true)) {
		    if ($show_extract_button == true) {
			echo "<font face=arial size=-1>$i_eu_verify_done $i_eu_download_next<br>";
			echo "<form action=extract.php method=post target=intranet_admin_main>";
			echo "<input id=\"submit\" type=\"submit\" Name=\"Submit\" Value=\"$i_eu_next_button!\">";
		        echo "<INPUT TYPE=HIDDEN NAME=\"doAction\" value=\"extractSource\">";
		        echo "</form>";
	            }
			$show_extract_button = false;
		  }
     }
		

}

echo "</div>";

if ($doAction!="verify" and $doAction!="extract" ) { ?>
	</td></tr>
</table>
<?
include_once("../../../templates/adminfooter.php");
}
?>

