<style type="text/css">
.progress_wrapper2 {width:300px;border:0px solid #ccc;position:absolute;}
.progress2 {height:20px;background-color:#000}
.progress_info2 {background-color:#fff; font-size:9pt; font-family:arial;}
</style>

<?
include_once("../../../lang/lang.$intranet_session_language.eu.php");

// Import language files
if( $systemType == "junior" ){
        include_once("../lang/lang.$intranet_session_language.eu_big5.php");
}else{
        include_once("../lang/lang.$intranet_session_language.eu_utf8.php");
}

$dl_status = "Starting...";

// Download file
while ( $dl_status != "Finish" )
{
	$result = shell_exec('scripts/check_intranet_download.sh /tmp/eclassupdate/wgetresultintranet.txt');
	$resultset = explode("-", $result);
	$dl_status = $resultset[0]; 

	if ( empty($resultset[2]) ) 
		$resultset[2] = "0%";


	echo '<div class="progress_wrapper2">';

	if ( $dl_status == "Finish" )
		echo '<div class="progress_info2">'.$i_eu_download_completed.'</div>';
	else {
		echo '<div class="progress_info2">'.$i_eu_download_progress.': ' .  $resultset[2] . '</div>';}
	echo '</div>';

	// Fill up php buffer before flush
	echo str_repeat(" ", 4096);
	ob_flush(); 
	flush();
	usleep(500000);

	if ( $dl_status == "Finish" ){
		$intranet_package_download_complete = true;

	        echo '<script language="javascript">';
        	echo 'location.href = "download.php?doAction=verify&intranet_package_download_complete=true";';
	        echo '</script>';
	}
}

exit(0);

?>
