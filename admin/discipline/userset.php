<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libdiscipline.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();


# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


$ldiscipline = new libdiscipline();
$basic_admin_level = $ldiscipline->getAdminLevelName(0);
$sql ="SELECT LevelName,LevelNum FROM DISCIPLINE_ACCESS_LEVEL ORDER BY LevelNum DESC";
$temp = $ldiscipline->returnArray($sql,2);
$max_admin_level_name = $temp[0][0];
$max_admin_level = $temp[0][1];
$max_admin_level = $max_admin_level==""?0:$max_admin_level;

$namefield = getNamefieldWithLoginByLang("a.");
$sql = "SELECT $namefield,
               IF(b.UserID IS NULL,'$basic_admin_level',IF(b.UserLevel>$max_admin_level,'$max_admin_level_name',c.LevelName)),
               CONCAT('<input type=checkbox name=StaffID[] value=', a.UserID ,'>')
               FROM INTRANET_USER as a
                    LEFT OUTER JOIN DISCIPLINE_USER_ACL as b ON a.UserID = b.UserID
                    LEFT OUTER JOIN DISCIPLINE_ACCESS_LEVEL as c ON b.UserLevel = c.LevelNum
               WHERE a.RecordType = 1 AND a.RecordStatus = 1
               ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.UserLogin", "c.LevelName");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=50% class=tableTitle>".$li->column($pos++, "$i_UserName ($i_UserLogin)")."</td>\n";
$li->column_list .= "<td width=50% class=tableTitle>".$li->column($pos++, $i_Discipline_System_Field_AdminLevel)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("StaffID[]")."</td>\n";



#$lteaching = new libteaching();
#$toolbar = "<a class=iconLink href=\"import.php\">".importIcon()."$button_import</a>";
$admin_levels = $ldiscipline->getAdminLevels();
#$array = build_assoc_array($admin_levels);
$select_admin_level = getSelectByArray($admin_levels,"name=TargetLevel",0,0,1);
$functionbar .= $select_admin_level;
$functionbar .= "<a href=\"javascript:checkAlert(document.form1,'StaffID[]','userset_update.php','$i_Discipline_System_alert_ChangeAdminLevel')\"><img src='/images/admin/button/t_btn_update_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

?>
<form name="form1" method="get">

<?= displayNavTitle($i_adminmenu_fs, '', $i_Discipline_System, 'index.php', $i_Discipline_System_Control_UserACL,'') ?>
<?= displayTag("head_discipline_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>


<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>