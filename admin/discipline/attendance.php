<?php
#############################
# Set rules of combine late
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdiscipline.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$string_type["0"] = $i_Merit_Warning;
$string_type["-1"] = $i_Merit_BlackMark;
$string_type["-2"] = $i_Merit_MinorDemerit;
$string_type["-3"] = $i_Merit_MajorDemerit;
$string_type["-4"] = $i_Merit_SuperDemerit;
$string_type["-5"] = $i_Merit_UltraDemerit;

##### Data retrieve
$ldiscipline = new libdiscipline();
$late_rules = $ldiscipline->getLateUpdateRules();
$detention_rules = $ldiscipline->getDetentionUpdateRules();
//debug_r($detention_rules);
$table_display = "";
$table_display2 = "";
for ($i=0; $i<sizeof($late_rules); $i++)
{
     list($l_nextLate, $l_profileType, $l_profileNum, $l_reasonID, $l_reason) = $late_rules[$i];
     $l_order = $i+1;

     $str_profile_type = $string_type[$l_profileType];
     $css = ($i%2?"":"2");
     $table_display .= "<tr class=tableContent$css><td>$l_order</td><td>$l_nextLate</td><td>$l_profileNum $str_profile_type</td><td>$l_reason</td>";
     /*
     $table_display .= "<td><a href=attendance_rule_edit.php?type=0&rule=$l_order><img src=\"$image_path/icon_edit.gif\" border=0></a>";
     if ($l_order==sizeof($late_rules))
     {
         $table_display .= "<a href=javascript:confirmRemoval('0')><img src=\"$image_path/icon_erase.gif\" border=0></a>";
     }
     $table_display .= "</td>";
     */
     $table_display .= "</tr>\n";
}

for($j=0; $j<sizeof($detention_rules); $j++)
{
         list($d_nextLate, $d_minute, $d_reason) = $detention_rules[$j];
     $d_order = $j+1;

     $css = ($j%2?"":"2");
     $table_display2 .= "<tr class=tableContent$css><td>$d_order</td><td>$d_nextLate</td><td>$d_minute</td><td>$d_reason</td>";
     /*
     $table_display2 .= "<td><a href=attendance_rule_edit.php?type=1&rule=$d_order><img src=\"$image_path/icon_edit.gif\" border=0></a>";
     if ($d_order==sizeof($detention_rules))
     {
          $table_display2 .= "<a href=javascript:confirmRemoval('1')><img src=\"$image_path/icon_erase.gif\" border=0></a>";
     }
     $table_display2 .= "</td>";
     */
     
     $table_display2 .= "</tr>\n";
}

//$toolbar = "<a class=iconLink href=\"attendance_new.php?type=0\">".newIcon()."$i_Discipline_System_LateUpgrade_Rule_AddNew</a>";
//$toolbar2 = "<a class=iconLink href=\"attendance_new.php?type=1\">".newIcon()."$i_Discipline_System_LateUpgrade_Rule_AddNew</a>";

?>
<SCRIPT LANGUAGE=Javascript>
function confirmRemoval(ftype)
{
         if (confirm('<?=$i_Usage_RemoveConfirm?>'))
         {
             eval("location.href = 'attendance_rule_remove.php?type=" + ftype + "'");
         }
}
</SCRIPT>
<?= displayNavTitle($i_adminmenu_fs, '', $i_Discipline_System, 'index.php',$i_Discipline_System_Control_AttendanceSettings,'') ?>
<?= displayTag("head_discipline_$intranet_session_language.gif", $msg) ?>
<?
# Year and Semester criteria
?>
<blockquote>
<p><span class="extraInfo">[<span class=subTitle><?=$i_Discipline_System_Count_Option?></span>]</span></p>

<form name=form_year action=attendance_option_update.php method=POST>
<table width=500 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<input type=radio name=count_option value=1 <?=($ldiscipline->late_upgrade_option==1?"CHECKED":"")?>><?=$i_Discipline_System_Count_Semester?><br>
<input type=radio name=count_option value=2 <?=($ldiscipline->late_upgrade_option==2?"CHECKED":"")?>><?=$i_Discipline_System_Count_Year?><br>
<input type=radio name=count_option value=3 <?=($ldiscipline->late_upgrade_option==3?"CHECKED":"")?>><?=$i_Discipline_System_Count_All?><br>
</td></tr>
<!--
<tr>
<td align=right>
<input type="image" src="<?=$image_path?>/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
</td>
</tr>
-->
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>
</blockquote>
</form>
<?
if ($plugin['Discipline_version']!='Q') {
?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu>
  <table widht=100% border=0 cellspacing=0 cellpadding=0>
    <tr><td><?=$toolbar?></td><td align=right></td></tr>
  </table>
</td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr class=tableTitle><td>#</td><td><?=$i_Discipline_System_LateUpgrade_Next?></td><td><?=$i_Discipline_System_Add_Demerit?></td><td><?=$i_Discipline_System_general_record?></td></tr>
<?=$table_display?>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<br/>
<br/>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu>
  <table widht=100% border=0 cellspacing=0 cellpadding=0>
    <tr><td><?=$toolbar2?></td><td align=right></td></tr>
  </table>
</td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr class=tableTitle><td>#</td><td><?=$i_Discipline_System_LateUpgrade_Next?></td><td><?=$i_Discipline_System_LateUpgrade_Detention_Minutes?></td><td><?=$i_SmartCard_DetentionReason?></td></tr>

<?=$table_display2?>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<? } ?>

<?php
include_once("../../templates/adminfooter.php");
?>
<script language='javascript'>
alert('<?=$iDiscipline['Outdated_Page_Waring']?>');
</script>
