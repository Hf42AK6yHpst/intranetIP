<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

# Clear old records
$sql = "DELETE FROM DISCIPLINE_ACCESS_LEVEL";
$li->db_db_query($sql);

# Put in new records
$values = "";
$delim = "";
for ($i=0; $i<sizeof($InputLevelName); $i++)
{
     if ($i >= $num)
     {
         break;
     }
     if (trim($InputLevelName[$i] != ""))
     {
         $values .= "$delim ($i, '".trim($InputLevelName[$i])."')";
         $delim = ",";
     }
}

$sql = "INSERT IGNORE INTO DISCIPLINE_ACCESS_LEVEL (LevelNum, LevelName) VALUES $values";
$li->db_db_query($sql);

intranet_closedb();
header("Location: levelset.php?msg=2");

?>