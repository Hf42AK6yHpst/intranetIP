<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdiscipline.php");
intranet_opendb();

$ldiscipline = new libdiscipline();
$count_option = $_POST['count_option'];
$ldiscipline->late_upgrade_option = $count_option;
$ldiscipline->writeSettings();

intranet_closedb();
header("Location: attendance.php?msg=2");

?>