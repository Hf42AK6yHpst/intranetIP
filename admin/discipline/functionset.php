<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdiscipline.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();


$ldiscipline = new libdiscipline();

// get maximum available admin level
$sql ="SELECT MAX(LevelNum) FROM DISCIPLINE_ACCESS_LEVEL";
$temp = $ldiscipline->returnVector($sql);
$max_admin_level = $temp[0];
$max_admin_level = $max_admin_level==""?0:$max_admin_level;


$temp = $ldiscipline->retrieveFunctionSettings();
$array_fs = build_assoc_array($temp);


for($i=1;$i<=8;$i++){
        if($array_fs[$i] > $max_admin_level)
                $array_fs[$i]= $max_admin_level;
}
$admin_levels = $ldiscipline->getAdminLevels();


?>

<form name="form1" action="functionset_update.php" method="post">
<?= displayNavTitle($i_adminmenu_fs, '', $i_Discipline_System, 'index.php', $i_Discipline_System_Control_ItemLevel,'') ?>
<?= displayTag("head_discipline_$intranet_session_language.gif", $msg) ?>
<SCRIPT LANGUAGE=Javascript>

</SCRIPT>

<blockquote><blockquote>
<table width=400 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=3>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Merit_Award?></td><td align=center><?=getSelectByArray($admin_levels,"name=Access1",$array_fs[1],0,1)?></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Merit_Punishment?></td><td align=center><?=getSelectByArray($admin_levels,"name=Access2",$array_fs[2],0,1)?></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$iDiscipline['Management_Child_AccumulativePunishment']?></td><td align=center><?=getSelectByArray($admin_levels,"name=Access9",$array_fs[9],0,1)?></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Discipline_System_Approval?></td><td align=center><?=getSelectByArray($admin_levels,"name=Access3",$array_fs[3],0,1)?></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Discipline_System_PunishCounselling?></td><td align=center><?=getSelectByArray($admin_levels,"name=Access4",$array_fs[4],0,1)?></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Discipline_System_Report?></td><td align=center><?=getSelectByArray($admin_levels,"name=Access5",$array_fs[5],0,1)?></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Discipline_System_Stat?></td><td align=center><?=getSelectByArray($admin_levels,"name=Access6",$array_fs[6],0,1)?></td></tr>
<!--
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Discipline_System_SchoolRules?></td><td align=center><?=getSelectByArray($admin_levels,"name=Access7",$array_fs[7],0,1)?></td></tr>
-->
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Discipline_System_Settings?></td><td align=center><?=getSelectByArray($admin_levels,"name=Access8",$array_fs[8],0,1)?></td></tr>
</table>
</blockquote></blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
 <a href="index.php"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>
</form>


<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>
