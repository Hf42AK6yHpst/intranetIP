<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclubsenrol.php");
include_once("../../includes/libclass.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");

intranet_opendb();

$lclub = new libclubsenrol();
$lclass = new libclass();
$sql = "CREATE TEMPORARY TABLE TEMP_ENROL_REQUIRE (
         StudentID int(11),
         Min int(11)
         )";
$lclub->db_db_query($sql);

# Grab min requirement to temp table
$minmax = $lclub->getFullMinMax();
$overallMax = $lclub->defaultMax;
for ($i=0; $i<sizeof($minmax); $i++)
{
     list($id,$min,$max) = $minmax[$i];
     if ($max == "") continue;
     if ($max > $overallMax) $overallMax = $max;

     # Grab classname for this form
     $classes = $lclass->returnClassListByLevel($id);
     $delimiter = "";
     $list = "";
     for ($j=0; $j<sizeof($classes); $j++)
     {
          list($id,$name) = $classes[$j];
          $list .= "$delimiter '$name'";
          $delimiter = ",";
     }
     $sql = "INSERT IGNORE INTO TEMP_ENROL_REQUIRE (StudentID,Min)
             SELECT UserID, $min FROM INTRANET_USER WHERE RecordType = 2 AND ClassName IN ($list) AND ClassName != ''";
     $lclub->db_db_query($sql);
}
if ($overallMax == 0)
{
    $sql = "SELECT MAX(Max) FROM INTRANET_ENROL_STUDENT";
    $result = $lclub->returnVector($sql);
    if ($result[0]!=0)
    {
        $overallMax = $result[0];
    }
}
$sql = "SELECT a.StudentID FROM INTRANET_ENROL_STUDENT as a, TEMP_ENROL_REQUIRE as b
        WHERE a.StudentID = b.StudentID AND a.Approved < b.Min";
$unsatSchool = $lclub->returnVector($sql);
$i = 0;
if ($lclub->tiebreak == 0)         # Tiebreaker
{
    $tiebreak_ord = "RAND()";
}
else  # 1
{
    $tiebreak_ord = "DateModified";
}
while (sizeof($unsatSchool)>0 && $i<$overallMax)
{
       $unsatList = implode(",",$unsatSchool);
       $i++;
       # Retrieve Groups with spaces
       $sql = "SELECT GroupID,Quota,Approved FROM INTRANET_ENROL_GROUPINFO WHERE Quota > Approved OR Quota = 0 OR Quota IS NULL";
       $groups = $lclub->returnArray($sql,3);

       for ($j=0; $j<sizeof($groups); $j++)
       {
            list ($groupID, $groupQuota, $groupApproved) = $groups[$j];
            if ($groupQuota > 0)
            {
                $left = $groupQuota - $groupApproved;
                $limit_str = "LIMIT 0,$left";
            }
            else
            {
                $limit_str = "";
            }
            $sql = "SELECT EnrolGroupID, StudentID FROM INTRANET_ENROL_GROUPSTUDENT
                    WHERE StudentID IN ($unsatList) AND
                         GroupID = $groupID AND Choice = $i AND RecordStatus = 0
                    ORDER BY $tiebreak_ord $limit_str";
            $approvedStudents = $lclub->returnArray($sql,2);
            $delimiter = "";
            $enrolIDList = "";
            $studentIDList = "";
            $numApproved = sizeof($approvedStudents);
            for ($k=0; $k<$numApproved; $k++)
            {
                 list($enrolID,$studentID) = $approvedStudents[$k];
                 $enrolIDList .= "$delimiter $enrolID";
                 $studentIDList .= "$delimiter $studentID";
                 $delimiter = ",";
            }
            $sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 2 WHERE EnrolGroupID IN ($enrolIDList)";
            $lclub->db_db_query($sql);
            $sql = "UPDATE INTRANET_ENROL_STUDENT SET Approved=Approved+1 WHERE StudentID IN ($studentIDList)";
            $lclub->db_db_query($sql);
            $sql = "UPDATE INTRANET_ENROL_GROUPINFO SET Approved = Approved + $numApproved WHERE GroupID = $groupID";
            $lclub->db_db_query($sql);
       }

       # Grab unsatisfied school requirement again
       $sql = "SELECT a.StudentID FROM INTRANET_ENROL_STUDENT as a, TEMP_ENROL_REQUIRE as b
               WHERE a.StudentID = b.StudentID AND a.Approved < b.Min";
       $unsatSchool = $lclub->returnVector($sql);
}
# Now have to match students' own requirement
$sql = "SELECT StudentID FROM INTRANET_ENROL_STUDENT WHERE Approved < Max";
$unsatOwn = $lclub->returnVector($sql);
$i = 0;
while (sizeof($unsatOwn)>0 && $i<$overallMax)
{
       $unsatOwnList = implode(",",$unsatOwn);
       $i++;
       # Retrieve Groups with spaces
       $sql = "SELECT GroupID,Quota,Approved FROM INTRANET_ENROL_GROUPINFO WHERE Quota > Approved OR Quota = 0 OR Quota IS NULL";
       $groups = $lclub->returnArray($sql,3);


       for ($j=0; $j<sizeof($groups); $j++)
       {
            list ($groupID, $groupQuota, $groupApproved) = $groups[$j];
            if ($groupQuota > 0)
            {
                $left = $groupQuota - $groupApproved;
                $limit_str = "LIMIT 0,$left";
            }
            else
            {
                $limit_str = "";
            }
            $sql = "SELECT EnrolGroupID, StudentID FROM INTRANET_ENROL_GROUPSTUDENT
                    WHERE StudentID IN ($unsatOwnList) AND
                         GroupID = $groupID AND Choice = $i AND RecordStatus = 0
                    ORDER BY $tiebreak_ord $limit_str";
            $approvedStudents = $lclub->returnArray($sql,2);
            $delimiter = "";
            $enrolIDList = "";
            $studentIDList = "";
            $numApproved = sizeof($approvedStudents);
            for ($k=0; $k<$numApproved; $k++)
            {
                 list($enrolID,$studentID) = $approvedStudents[$k];
                 $enrolIDList .= "$delimiter $enrolID";
                 $studentIDList .= "$delimiter $studentID";
                 $delimiter = ",";
            }
            $sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 2 WHERE EnrolGroupID IN ($enrolIDList)";
            $lclub->db_db_query($sql);
            $sql = "UPDATE INTRANET_ENROL_STUDENT SET Approved=Approved+1 WHERE StudentID IN ($studentIDList)";
            $lclub->db_db_query($sql);
            $sql = "UPDATE INTRANET_ENROL_GROUPINFO SET Approved = Approved + $numApproved WHERE GroupID = $groupID";
            $lclub->db_db_query($sql);
       }

       # Grab unsatisfied own requirement again
       $sql = "SELECT StudentID FROM INTRANET_ENROL_STUDENT WHERE Approved < Max";
       $unsatOwn = $lclub->returnVector($sql);

}





intranet_closedb();
?>
<p style="padding-left:20px">
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
<tr><td style="background-image: url(../../../images/admin/pop_bg.gif);" align=center>
<?=$i_ClubsEnrollment_LotteryFinished?>
</td></tr>
<tr><td><img src="../../../images/admin/pop_bottom.gif" width=422 height=18 border=0></td></tr>
<tr><td align=center height="40" style="vertical-align:bottom">
<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
<?
include_once("../../templates/filefooter.php");
?>