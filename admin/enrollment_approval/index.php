<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");

?>

<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_enrollment_approval, '') ?>
<?= displayTag("head_enrollment_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300>
<blockquote>
<?= displayOption($i_adminmenu_adm_enrollment_group, 'group.php', 1,
				$Lang['eEnrolment']['StudentEnrolmentStatus'], 'student.php', 1) ?>
</blockquote>
</td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>