<?php
// using :
/*
 * Change Log:
 * 2018-06-14 Frankie SSO for DSI initial
 */

if (!isset($indexVar['libSsoDsi']) ||
    !isset($_SESSION["UserID"]) ||
    $_SESSION["UserID"] <= 0) {
    No_Access_Right_Pop_Up();
    exit;
}
/*
 * get configs
 */
$ssoDsiConfigs = $indexVar['libSsoDsi']->getConfig('all');

/*
 * generate Public key
 */
$eclass_public_key = md5(base64_encode($ssoDsiConfigs["passport_client_id"] . $ssoDsiConfigs['hex_key']));


if (!isset($_GET["force_login"])) { 
    $tokenInfo = $indexVar['libSsoDsi']->getTokenByUserID($_SESSION["UserID"]);
    if ($tokenInfo !== null) {
        $_SESSION["DSI_SSO"]["init"] = $tokenInfo;
        $_SESSION["DSI_SSO"]["count"] = 1;
        header("Location:" . $ssoDsiConfigs['token_login']);
        exit;
    }
}

/*
 * generate Pack Hex key
 */
$pack_key = pack('H*', $ssoDsiConfigs['pack_key']);

/*
 * Encrypt User ID
 */
$user_id = sprintf("%020d", $_SESSION["UserID"]);
$user_id = $indexVar['libSsoDsi']->db_encrypt_decrypt('encrypt', $user_id, $ssoDsiConfigs['hex_key'], false);

$current_url = $ssoDsiConfigs["receive_toekn"];
$current_link = $indexVar['libSsoDsi']->db_encrypt_decrypt('encrypt', $current_url, $ssoDsiConfigs['hex_key'], false);

$encrypted_user_id = md5($eclass_public_key) . base64_encode($user_id . $pack_key) . md5($current_link);
$encrypted_current_link = base64_encode($current_link . $pack_key) . md5($eclass_public_key);
$first_login_token = md5($encrypted_current_link . base64_encode($user_id) . $eclass_public_key);

$default_link = $indexVar['libSsoDsi']->db_encrypt_decrypt('encrypt', $ssoDsiConfigs['auth_link'], $ssoDsiConfigs['hex_key'], false);

$param = array(
    'action' => $ssoDsiConfigs['url'] . "/eclass",
    'fields' => array(
        'ut' => $encrypted_user_id, // Encrypted Current User ID
        'ct' => $encrypted_current_link, // Encrypted Current link
        'ft' => $first_login_token, // Login Verify Token
        'ts' => $current_link, // Current link
        'callback' => $default_link// API Login link
    )
);

header("Location: " . $param["action"]. "?" . http_build_query($param["fields"]));
exit;
?>