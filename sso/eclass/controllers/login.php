<?php
// using : Frankie
/*
 * Change Log:
 * 2018-07-04 Frankie
 *          - SSO from another site for DSI initial
 */

if (!isset($indexVar['libSsoServer']) && $_SESSION["UserID"] > 0) {
    No_Access_Right_Pop_Up();
    exit;
}


$indexVar['libSsoServer']->authorizeAccount($_SESSION["ECLASS_SSO"]["client_credentials"], $_SESSION["UserID"]);
exit;