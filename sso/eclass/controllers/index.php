<?php
// using : Frankie
/*
 * Change Log:
 * 2018-07-04 Frankie
 *          - SSO from another site for DSI initial
 */

if (!isset($indexVar['libSsoServer'])) {
    No_Access_Right_Pop_Up();
    exit;
}
header("HTTP/1.0 404 Not Found");
exit;