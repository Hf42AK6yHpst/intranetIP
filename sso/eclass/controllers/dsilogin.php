<?php
// using : Frankie
/*
 * Change Log:
 * 2019-01-03 Frankie
 *          - SSO Customization login page
 */

if (!isset($indexVar['libSsoServer'])) {
    No_Access_Right_Pop_Up();
    exit;
}
@session_start();
/*** come from SSO ***/
if (isset($_SESSION["ECLASS_SSO"]["client_credentials"]))
{
    // 5 min expired
    $expireAfter = 5;
    $secondsInactive = time() - $_SESSION["ECLASS_SSO"]["client_credentials"]["expiredTime"];
    $expireAfterSeconds = $expireAfter * 60;
    if($secondsInactive <= $expireAfterSeconds)
    {
        $client_credentials = $_SESSION["ECLASS_SSO"]["client_credentials"];
    }
}


$logo = $indexVar['libSsoServer']->isEJ ? './assets/images/ej-logo.png' : './assets/images/ip-logo.png';
$logo_title = $indexVar['libSsoServer']->isEJ ? '' : 'Integrated Platform';

$error_msg = 'Please enter a valid username or password.';

if (isset($_SESSION["ECLASS_SSO"]["client_credentials"]))
{
    if ($indexVar['libSsoServer']->isEJ)
    {
        $SecureToken = '';
    } else {
        include_once("../../includes/SecureToken.php");
        $SecureToken_obj = new SecureToken();
        $SecureToken = $SecureToken_obj->WriteFormToken();
    }
    
    
    $_view = array(
        'page_title' => 'eClass Integrated Platform',
        'login_logo' => $logo,
        'login_title' => $logo_title,
        'prompt_msg' => 'You are going to setup Single Sign-on (SSO) to eClass system on DSI. Please sign in with your eClass account to complete the process.',
        'login_id' => 'Login ID',
        'password' => 'Password',
        'login_button' => 'Login',
        'show_error_msg' => $_GET['err'] == 1 ? true : false,
        'error_msg' => $error_msg,
        'SecureToken' => $SecureToken
    );
    require_once('./views/custom_login.php');
} else {
    $_view = array(
        'page_title' => 'eClass Integrated Platform',
        'login_logo' => $logo,
        'no_permission_msg' => 'You have no permission to access this page.'
    );
    require_once('./views/invalid_page.php');
}

exit;