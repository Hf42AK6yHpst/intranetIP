<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $_view['page_title']; ?></title>
    <link rel="stylesheet" type="text/css" href="./assets/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="./assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" type="text/css" href="./assets/css/login-dsi.css" />
    <link rel="apple-touch-icon" sizes="180x180" href="./assets/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="./assets/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="./assets/images/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="./assets/images/favicon/manifest.json">
    <link rel="mask-icon" href="./assets/images/favicon/safari-pinned-tab.svg" color="#00abb6">
    <meta name="theme-color" content="#ffffff">
  </head>
<body class="login">
  <div id="wrapper">
  	<div class="login-box">
      <span id="logo">
        <img src="<?php echo $_view['login_logo']; ?>" width="150">
      </span>
      <div class="login-form">
          <div class="no-permission"><?php echo $_view['no_permission_msg']; ?></div>
  	  </div>
  	</div>
  </div>

  <div id="bg">
    <!-- <img src="./assets/images/login-bg.jpg"> changed to svg tag because of IE-->
    <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svgBlur">
      <filter id="svgBlurFilter">
        <feGaussianBlur in="SourceGraphic" stdDeviation="5" />
      </filter>
      <image xlink:href="./assets/images/login-bg-ipej.jpg" x="0" y="0" height="100%" width="100%" filter="url(#svgBlurFilter)" preserveAspectRatio="xMinYMax slice"/>
    </svg>
  </div>


</body>
</html>
