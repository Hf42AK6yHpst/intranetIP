<?php
/**
 * Change Log:
 * 2019-05-02 Pun
 *  - Prevent Command-injection
 */
ini_set('zend.ze1_compatibility_mode', '0');

$username = $_POST["username"];
include_once('../../includes/global.php');
include_once('../../includes/libdb.php');
require_once 'saml_util.php';

intranet_auth();
intranet_opendb();

$libdb = new libdb();
session_start();

//$domainName = 'eclass.uccke.edu.hk';
//$domainName = 'psosamldemo.net';

function check_username($username) {
	global $libdb;

	$sql = " Select UserLogin From INTRANET_USER WHERE UserID = '".$_SESSION['UserID']."' ";
	$userlogin = current($libdb->returnVector($sql));
	if($userlogin == $username){
		return true;
	}
	return false;
//	return true;
}

/**
 * Returns a SAML response with various elements filled in.
 * @param string $authenticatedUser The Google Apps username of the
                 authenticated user
 * @param string $notBefore The ISO 8601 formatted date before which the
                 response is invalid
 * @param string $notOnOrAfter The ISO 8601 formatted data after which the
                 response is invalid
 * @param string $rsadsa 'rsa' if the response will be signed with RSA keys,
                 'dsa' for DSA keys
 * @param string $requestID The ID of the request we're responding to
 * @param string $destination The ACS URL that the response is submitted to
 * @return string XML SAML response.
 */
function createSamlResponse($authenticatedUser, $notBefore, $notOnOrAfter,
                            $rsadsa, $requestID, $destination) {
  //global $domainName;
  global $singlesignonservice;

  $samlResponse = file_get_contents('templates/SamlResponseTemplate.xml');
  $samlResponse = str_replace('<USERNAME_STRING>', $authenticatedUser,
	                          $samlResponse);
  $samlResponse = str_replace('<RESPONSE_ID>', samlCreateId(), $samlResponse);
  $samlResponse = str_replace('<ISSUE_INSTANT>', samlGetDateTime(time()),
	                          $samlResponse);
  $samlResponse = str_replace('<AUTHN_INSTANT>', samlGetDateTime(time()),
	                          $samlResponse);
  $samlResponse = str_replace('<NOT_BEFORE>', $notBefore, $samlResponse);
  $samlResponse = str_replace('<NOT_ON_OR_AFTER>', $notOnOrAfter,
	                          $samlResponse);
  $samlResponse = str_replace('<ASSERTION_ID>', samlCreateId(), $samlResponse);
  $samlResponse = str_replace('<RSADSA>', strtolower($rsadsa), $samlResponse);
  $samlResponse = str_replace('<REQUEST_ID>', $requestID, $samlResponse);
  $samlResponse = str_replace('<DESTINATION>', $destination, $samlResponse);
  $samlResponse = str_replace('<ISSUER_DOMAIN>', $singlesignonservice["google"]["domain_name"], $samlResponse);

  return $samlResponse;
}

/**
 * Signs a SAML response with the given private key, and embeds the public key.
 * @param string $responseXmlString
 * @param string $pubKey
 * @param string $privKey
 * @return string
 */
function signResponse($responseXmlString, $pubKey, $privKey) {
  global $error, $intranet_root;

  // generate unique temporary filename
  //$tempFileName = 'temp/saml-response-' . samlCreateId() . '.xml';
  if(!file_exists($intranet_root.'/file/sso_temp/')){
  	mkdir($intranet_root.'/file/sso_temp/', 0777);
  }
  $tempFileName = $intranet_root.'/file/sso_temp/saml-response-' . samlCreateId() . '.xml';
  while (file_exists($tempFileName))
	     $tempFileName = $intranet_root.'/file/sso_temp/saml-response-' . samlCreateId() . '.xml';

  if (!$handle = fopen($tempFileName, 'w')) {
    echo 'Cannot open temporary file (' . $tempFileName . ')';
    exit;
  }

  if (fwrite($handle, $responseXmlString) === FALSE) {
    echo 'Cannot write to temporary file (' . $tempFileName . ')';
    exit;
  }

  fclose($handle);

    $_privKey = OsCommandSafe($privKey);
    $_pubKey = OsCommandSafe($pubKey);
    $_tempFileName = OsCommandSafe($tempFileName);
  if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
    $cmd = 'C:\libs\xmlsec-win32\xmlsec sign --privkey-pem ' . $_privKey .
	     ' --pubkey-der ' . $_pubKey . ' --output ' . $_tempFileName .
	     '.out ' . $_tempFileName;
  } else {
      $cmd = '/usr/bin/xmlsec1 sign --privkey-pem ' . $_privKey .
      ' --pubkey-der ' . $_pubKey . ' --output ' . $_tempFileName .
      '.out ' . $_tempFileName;
  }
  $resp = array();
  exec($cmd, $resp);
  //echo '<pre>';var_dump(array($cmd,$resp, $tempFileName));echo '</pre>';die();
  unlink($tempFileName);

  $xmlResult = @file_get_contents($tempFileName . '.out');
  if (!$xmlResult) {
    $error = 'Unable to sign XML response. Please ensure that xmlsec is ' .
	         'installed, and check your keys.';
    return false;
  } else {
    unlink($tempFileName . '.out');
    return $xmlResult;
  }
}

if ($_GET['SAMLRequest'] != '') {
  $SAMLRequest = $_GET['SAMLRequest'];
  $relayState = $_GET['RelayState'];
  $error = '';

  $requestXmlString = samlDecodeMessage($SAMLRequest);

  if (($requestXmlString == '') || ($requestXmlString === FALSE)) {
    $error = 'Unable to decode SAML Request.';
  } else {
    $samlAttr = getRequestAttributes($requestXmlString);

    $issueInstant = $samlAttr['issueInstant'];
    $acsURL = $samlAttr['acsURL'];
    $providerName = $samlAttr['providerName'];
    $requestID = $samlAttr['requestID'];
  }
} else if($_POST['SAMLRequest'] != '') {

  $samlAction = $_POST['samlAction'];
  $SAMLRequest =$_POST['SAMLRequest'];
  $returnPage = $_POST['returnPage'];
  $username = $_POST['username'];
  $password = $_POST['password'];
  $relayStateURL = $_POST['RelayState'];

  if ($SAMLRequest == '') {
    $error = 'Error: Unspecified SAML parameters.';
  } else if ($samlAction == '') {
    $error = 'Error: Invalid SAML action.';
  } else if ($returnPage != '') {
  $requestXmlString = samlDecodeMessage($SAMLRequest);
    if (($requestXmlString == '')||($requestXmlString === FALSE)) {
      $error = 'Unable to decode SAML Request.';
    } else {
      $samlAttr = getRequestAttributes($requestXmlString);
      $issueInstant = $samlAttr['issueInstant'];
      $acsURL = $samlAttr['acsURL'];
      $providerName = $samlAttr['providerName'];
      $requestID = $samlAttr['requestID'];

      if ($username == '') {
        $error = 'Login Failed: Invalid user.';
      } else {
      	if(!check_username($username)){
        	$error = 'Login Failed: User does not match.';
      	}else{
      		$pubKey = $singlesignonservice["google"]["public_key"];//'keys/plhkspubkey.key';
	        $privKey = $singlesignonservice["google"]["private_key"]; //'keys/googleappsidp.pem';//'keys/lamhonkwongkey.pem';
	        $keyType = 'rsa';

	        $notBefore = samlGetDateTime(strtotime('-5 minutes'));
	        $notOnOrAfter = samlGetDateTime(strtotime('+10 minutes'));

	        // Sign XML containing user name with specified keys
	        $responseXmlString = createSamlResponse($username, $notBefore,
				                                    $notOnOrAfter, $keyType,
				                                    $requestID, $acsURL);
			$samlResponse = signResponse($responseXmlString, $pubKey, $privKey);
      	}
      }
    }
  }
}

include '3rd.php';

?>