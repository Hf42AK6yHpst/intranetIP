<?php
/*
 * Editing by Thomas
 * 
 * Modification Log
 *
 * 2019-08-08 (Thomas) [ip.2.5.10.10.1]
 *      - Show error message when fail to create lesson session
 * 2019-07-15 (Pun) [164510] [ip.2.5.10.10.1]
 *      - Added $_SESSION["FlippedChannelsServerPublicPath"]
 * 2018-04-12 (Thomas) [ip.2.5.9.5.1]
 *      - Fixed the problem of redirecting to incorrect protocol if $web_protocol is set
 * 2016-07-14 (Jason) [ip.2.5.7.7.1]
 * 		- modify the session call method to support php5.4
 * 2016-06-01 (Thomas) [ip.2.5.7.7.1]
 * 		- Support recording user device id if the lesson is in progress
 * 2015-07-24 (Thomas) [ip.2.5.6.1.1]
 * 		- Add code to handle the version string passed from Android PowerLesson App
 * 2015-03-17 (Thomas) [ip.2.5.6.5.1]
 * 		- Added Flipped Channels part for getting the FlippedChannelsServerPath and save in session 
 * 
 * Note: 
 * 		this page is called by iOS Powerlesson APP to login the lesson 
 * 		- combine 3 login process pages 
 * 			1. /intranetIP/login.php
 * 			2. /eclass40/checkpl.php
 * 			3. /eclass40/src/lessonplan/login_lesson.php
 * 		Aim: 
 * 			- optimize the performance of login process via iOS PowerLesson App
 * 
 * 		@Param:
 * 			key				- SessionKey from INTRANET_USER
 * 			user_course_id	- for user_course
 * 			plan_id			- lesson plan id
 * 			lsession_id		- lesson session id 
 * 			AppVersion		- version of PowerLesson App
 * 			$subject_id		- $subject_id		// for EJ50 use 
 */

$ByPassLibPage = true;
include_once("includes/global.php");

include_once($eclass40_filepath.'/system/settings/global.php');

include_once($eclass40_filepath.'/src/includes/php/lib-db.php');
include_once($eclass40_filepath.'/src/includes/php/lib-courseinfo.php');
include_once($eclass40_filepath.'/src/includes/php/lib-record.php');
include_once($eclass40_filepath.'/src/includes/php/lib-user.php');		## FOR retrieving teacher photos on HEADER PANEL
opendb();
#######################################################################################

######## Test Data - by pass ########
if($_REQUEST['role'] == 'T'){
	# jason_t
	$key = '5b37464a56bcd72dd8e172520467a077';
	$user_course_id = 5957;
	$plan_id = 71;
} else if($_REQUEST['role'] == 'S'){
	# power_s1
	$key = '852cd3dcf90bc899ae1025f9ca0129dc';
	$user_course_id = 5545;
	$plan_id = 71;
	$lsession_id = '5aaa87eb751016daae02f2b629032400';
}
######## End of Testing Data ########

########################################################################################################
# Clear the cookies
setcookie("ck_memberType_classB", null, time()-999999, $eclass_url_path."/src/resource/eclass_files/files/", "", 0);
########################################################################################################

## Get Data
$key = (isset($key) && $key != '') ? $key : '';
$user_course_id = (isset($user_course_id) && $user_course_id != '') ? $user_course_id : '';

$plan_id 	= (isset($plan_id) && $plan_id != '') ? $plan_id : '';
$AppVersion = (isset($AppVersion) && $AppVersion != '') ? $AppVersion : '';
$subject_id = (isset($subject_id) && $subject_id != '') ? $subject_id : '';
$DeviceID   = (isset($DeviceID) && $DeviceID != '') ? $DeviceID : '';

if($userBrowser->platform=='Andriod'){
	preg_match_all('/^v([\d.]+)\.a/', $AppVersion, $out, PREG_SET_ORDER);
	if(count($out)>0){
		$AppVersion = $out[0][1];
		
		/* Remap the Version String for Android */
		if($AppVersion=='1.10'){
			$AppVersion = '1.1.0';
		}
		else if($AppVersion=='1.11'){
			$AppVersion = '1.1.1';
		}
		else if($AppVersion=='1.12'){
			$AppVersion = '1.1.2';
		}
	}
}

## Init
$ErrorCodeRemark = array();
$ErrorCodeRemark[101] = 'the SessionKey of this user is no longer valid via Intranet.';
$ErrorCodeRemark[102] = 'the user does not have access right to that course.';


## Use Library 
$li = new libdb();
$li->db = $eclass_db;


 session_start();
#######################################################################################

###### Step 1 ######
##########################################################################################
###### Start of /intranetIP/login.php ####################################################
##########################################################################################

## Check Session Key passed by App
$sql = "SELECT
			iu.UserID, iu.UserLogin, iu.UserEmail
		FROM
			".$intranet_db.".INTRANET_USER AS iu INNER JOIN
			".$eclass_db.".user_course AS uc ON iu.UserEmail = uc.user_email
		WHERE
			iu.SessionKey = '".$key."' AND uc.user_course_id = '".$user_course_id."'";
//$sql = "select UserID, UserLogin, UserEmail from ".$intranet_db.".INTRANET_USER where SessionKey = '".$key."' ";
$row = $li->returnArray($sql);


if(count($row) == 0){
	
	$flag = 0;
	$email = "";
	
	# fail to login 
	$versionCompareResult = powerlessonapp_version_compare($AppVersion, $cfg['PowerLessonApp']['VersionCompare']['FinishLessonWithError']);
	
	if($versionCompareResult=='SAME' || $versionCompareResult=='LATER'){
		$ErrorDomain = 401;
		$ErrorDescription = "Fail to Login PowerLesson";
		
		if(!$key)
			$ErrorCode = 103;
		else if(!$user_course_id)
			$ErrorCode = 104;
		else
			$ErrorCode = 105;
		
		header("Location: ".$app_cfg['PowerLessonApp']['Scheme']['FinishLesson']."://?error=".base64_encode("domain=".$ErrorDomain."&code=".$ErrorCode."&description=".$ErrorDescription));
		die();
	}
} else {
	$intranet_user_id = $row[0]['UserID'];
	
	$flag = 1;
	$email = $row[0]['UserEmail'];
	
//	$ck_intranet_user_id = $intranet_user_id;
	session_register_eclass("ck_intranet_user_id", $intranet_user_id);
	setcookie("ck_user_email_client", $email, time()+86400, "", "", 0);
}

## Register session
session_register_eclass("UserID", $ID);
//session_register("eclass_session_password");

//$UserID = $ID;


session_register_eclass("elcass_server_url", $_SERVER['HTTP_HOST']);
//$elcass_server_url = $_SERVER['HTTP_HOST'];

$ss_intranet_plugin['power_speech'] 		= $plugin['power_speech']; 	
$ss_intranet_plugin['power_speech_expire'] 	= $plugin['power_speech_expire'];

//$ss_intranet_plugin['elib_power_speech'] 		= $plugin['elib_power_speech']; 	
//$ss_intranet_plugin['elib_power_speech_expire'] 	= $plugin['elib_power_speech_expire'];

## Power speech sessions	 
// commented by Yuen - session_register("plugin");		
//session_register("tts_server_url");
//session_register("tts_user");
$_SESSION['tts_server_url'] = $tts_server_url;
$_SESSION['tts_user'] = $tts_user;
$_SESSION['config_school_code'] = $config_school_code;

# 190 Project - Wong Wah San eLearning Project
if(isset($plugin['WWS_eLearningProject']) && $plugin['WWS_eLearningProject'] == true)
{
	$_SESSION['ck_wws_elearning_project'] = $plugin['WWS_eLearningProject'];
}

# PowerLesson
if(isset($plugin['power_lesson']) && $plugin['power_lesson'] == true)
{
	$_SESSION['ck_power_lesson'] = $plugin['power_lesson'];
	$_SESSION['ck_from_pl'] 	 = $from_pl;
}

if(isset($plugin['power_lesson_media_conversion_type']) && is_array($plugin['power_lesson_media_conversion_type']))
{
	$_SESSION['ck_power_lesson_media_conversion_type'] = $plugin['power_lesson_media_conversion_type']; 
}

# PowerBoard
if(isset($plugin['power_board']) && $plugin['power_board'] == true)
{
	$_SESSION['ck_power_board'] = $plugin['power_board']; 
}

# Flipped Channels
if ($plugin['FlippedChannels']) {
	include_once("includes/FlippedChannels/libFlippedChannels.php");
	$lFlippedChannels = new libFlippedChannels();
		
	$centralServerPath = $lFlippedChannels->getCurCentralServerUrl();
	$centralServerPath = ($centralServerPath==null)? '' : $centralServerPath;
	
	//session_register("FlippedChannelsServerPath");
	$_SESSION["FlippedChannelsServerPath"] = $centralServerPath;

    $flippedChannelsServerPublicPath = $lFlippedChannels->getServerPublicPath();
    $flippedChannelsServerPublicPath = ($flippedChannelsServerPublicPath === null) ? '' : $flippedChannelsServerPublicPath;

    //session_register("FlippedChannelsServerPath");
    $_SESSION["FlippedChannelsServerPath"] = $centralServerPath;
    $_SESSION["FlippedChannelsServerPublicPath"] = $flippedChannelsServerPublicPath;
}

session_register_eclass("ss_intranet_plugin", $ss_intranet_plugin);

#Get current school year id. Refer to form_class_manage.php function getCurrentAcademicaYearID(). 
#$_SESSION['CurrentSchoolYearID'] is set in lib.php function Get_Current_Academic_Year_ID()
$sql = "select
			AcademicYearID
		from
			".$intranet_db.".ACADEMIC_YEAR_TERM
		where
			CURDATE() between TermStart and TermEnd";
$schoolYearId = current($li->returnVector($sql));
$_SESSION['CurrentSchoolYearID'] = $schoolYearId;

//debug_r($_SESSION);

##########################################################################################
###### End of /intranetIP/login.php ######################################################
##########################################################################################

###### Step 2 ######
##########################################################################################
###### Start of /eclass40/checkpl.php ####################################################
##########################################################################################
if ($flag != 1)
{
	# failed - because the SessionKey of this user is no longer valid via Intranet (Remark: session time checking is ginored already)
	$ErrorCode = 101;
} else
{
	$li->db = classNamingDB($sys_db);
	
	$sql = "SELECT
				course_id,
				user_id,
				user_email,
				memberType,
				ifnull(scheme_id,1) AS scheme_id,
				font_size,
				language,
				scheme_background
			FROM
				user_course
			WHERE
				(status is null or status = '') AND
				user_course_id = '".$user_course_id."'
				AND user_email = '$email' ";
	$row_result = $li->returnArray($sql);
//	echo $sql;
//	debug_r($row_result);
	
	if (sizeof($row_result)>0)
	{	
		$row = $row_result[0];
		$course_id = $row["course_id"];
		$user_id = $row["user_id"];
		$user_email = $row["user_email"];
		
		$memberType = $row["memberType"];
		
		$scheme_id = ($row["scheme_id"] > 0) ? $row["scheme_id"] : 4;
		$scheme_background = $row['scheme_background'];
		
		## NEW Eclass40
		$font_size = $row['font_size'];
		$user_language  = $row['language'];
		
		# Update eClass course_visitor_count table
		$sql = "INSERT INTO course_count (host, course_id, user_id, inputdate) VALUES ('$REMOTE_ADDR', $course_id, $user_id, now())";
		$li->db_db_query($sql);
		
		# Update last_used in user_course
		$sql = "UPDATE user_course SET lastused = now() WHERE user_course_id=$user_course_id";
		$li->db_db_query($sql);
		
		
		# Get course info
		$sql = "SELECT 
					course_code, course_name , RoomType, SubjectGroupID, 
					language, ReferenceID, rights_student, rights_guest 
				FROM course WHERE course_id = '".$course_id."' ";
		$row_data = $li->returnArray($sql);
		$course_code = $row_data[0]['course_code'];
		$course_name = $row_data[0]['course_name'];
		$course_room_type = $row_data[0]['RoomType'];
		$subjectGroupID = $row_data[0]['SubjectGroupID'];
		$language = $row_data[0]['language'];
		$ReferenceID = $row_data[0]['ReferenceID'];
		$courseStudentRights = $row_data[0]['rights_student'];
		$courseGuestRights = $row_data[0]['rights_guest'];
		
		# Update last_used in usermaster        
		$li->db = classNamingDB($course_id);
		$sql = "UPDATE usermaster SET lastused = now() WHERE user_id = $user_id";
		$li->db_db_query($sql);

		# pre-load access rights for student and guest
		if ($memberType==$cfg["user_course_memberType"]["isStudent"])
		{
			$ck_course_rights = $courseStudentRights;
		} elseif ($memberType==$cfg["user_course_memberType"]["isGuest"])
		{
			$ck_course_rights = $courseGuestRights;
		} else
		{
			$ck_course_rights = "";
		}
		session_register_eclass("ck_course_rights", $ck_course_rights);
		
		// Set session cookies
		session_unregister_eclass("poemsandsongplver");	// 20131128-PAS-PowerLessonIntegration
		$ck_lang = "";
		session_unregister_eclass("ck_lang");
		//$ck_course_id = $course_id;
		session_register_eclass("ck_course_id", $course_id);
		
		//$ck_course_code = $course_code;
		session_register_eclass("ck_course_code", $course_code);
		
		//$ck_course_name = $course_name;
		session_register_eclass("ck_course_name", $course_name);
		
		//$ck_course_room_type = $course_room_type;
		session_register_eclass("ck_course_room_type", $course_room_type);
		
		//$ck_user_id = $user_id;
		session_register_eclass("ck_user_id", $user_id);
		//$ck_org_user_id = "";
		session_register_eclass("ck_org_user_id", "");
		
		//$ck_user_name = $user_name;
		session_register_eclass("ck_user_name", $user_name);
		
		//$ck_header_style = $header_style;
		session_register_eclass("ck_header_style", $header_style);
		
		//$ck_user_course_id = $user_course_id;
		session_register_eclass("ck_user_course_id", $user_course_id);
		
		//$ck_login_session_id = $login_session_id;
		session_register_eclass("ck_login_session_id", $login_session_id);
		
		//$ck_memberType = $memberType;
		session_register_eclass("ck_memberType", $memberType);
		
		# set language
		$ck_default_lang = $language;
		if($ck_default_lang == "chib5" && $g_chinese =="gb"){
			$ck_default_lang = "chigb";
		}
		
		session_register_eclass("ck_default_lang", $ck_default_lang);
		//$ck_scheme_id = $scheme_id;
		session_register_eclass("ck_scheme_id", $scheme_id);
		//$ck_user_email = $user_email;
		session_register_eclass("ck_user_email", $user_email);
		//$ck_roleType = $memberType;
		session_register_eclass("ck_roleType", $memberType);
		//$ck_user_role = "VIEW";
		session_register_eclass("ck_user_role", "VIEW");
		## Eclass40 NEW 	
		//$ck_font_size = $font_size;
		session_register_eclass("ck_font_size", $font_size);
		//$ck_scheme_background = $scheme_background;
		session_register_eclass("ck_scheme_background", $scheme_background);
		
		#### Register eClass40 ##################################################################
		# added on 2011-02-02 
		# Get & Check client current version number of eClass
		if(isset($_SESSION['VERSION']['eClass']) && $_SESSION['VERSION']['eClass'] > 0){
			# by SESSION passed by INTRANET
			$current_eclass_ver = $_SESSION['VERSION']['eClass'];
		} else {
			# by Intranet Database Record if no SESSION is set
			$sql = "SELECT MAX(Version) FROM {$intranet_db}.ECLASS_UPDATE_LOG WHERE Module = 'eClass' ";
			$rs_ec_ver = $li->returnVector($sql);
			$current_eclass_ver = $rs_ec_ver[0];
		}
		if($current_eclass_ver != '' && $current_eclass_ver > 0){
			# get from intranet eclass_update_log records
			$ck_ec_version_no = $current_eclass_ver;
		} else {
			# get from /system/settings/settings.php or global.php
			$ck_ec_version_no = $ec_version_no;
		}
		session_register_eclass("ck_ec_version_no", $ck_ec_version_no);
		#########################################################################################
		
		//$ck_teacher_photo = "";
		session_unregister_eclass("ck_teacher_photo"); // unregister session used in previous classroom
		
		$li_ci = new courseinfo(1);
		
		$li_ci->registerRoomType($ck_course_id);
		
		$li_ci->registerUserRights($ck_user_id, $memberType);
		
		//$ck_plugin_power_lesson = true;
		session_register_eclass("ck_plugin_power_lesson", true);
		
		if(isset($plugin['power_board_html5']) && $plugin['power_board_html5'] == true)
		{
			$ck_power_board_html5 = $plugin['power_board_html5'];
		} else {
			$ck_power_board_html5 = false;
		}
		session_register_eclass("ck_power_board_html5", $ck_power_board_html5);
		
		if($ck_room_type == 'WWS_ELEARNING_PROJECT'){
			# WWS Project would use PowerLesson App too
			//$ck_plugin_wws_elearning_project = $_SESSION['ck_wws_elearning_project'];
			session_register_eclass("ck_plugin_wws_elearning_project", $_SESSION['ck_wws_elearning_project']);
			
			//$ck_wws_subject = $subjectGroupID;
			session_register_eclass("ck_wws_subject", $subjectGroupID);
			
			//$ck_plugin_power_lesson = true;
			session_register_eclass("ck_plugin_power_lesson", true);
			
			//$ck_power_board = true;
			session_register_eclass("ck_power_board", true);
			
			//$ck_power_board_html5 = (strtotime($today) > strtotime($plugin_expiry['power_board_html5'])) ? false : true;
			session_register_eclass("ck_power_board_html5", $ck_power_board_html5);
		} else {
			//$ck_plugin_wws_elearning_project = false;
			session_register_eclass("ck_plugin_wws_elearning_project", false);
		
			//$ck_wws_subject = '';
			session_register_eclass("ck_wws_subject", "");
		}
		
		# perform all the menu access display control in this file
//		include_once($cfg_eclass_lib_path."/module_access_control.php");
		include_once($eclass40_filepath.'/src/includes/php/module_access_control.php');
		
		# Enter to course
		$is_from_app = array();
		$goActiveLesson = false;
		if($lsession_id != ''){
			$is_from_app['PowerLesson'] = true;
			$is_from_app['AppVersion']	= $AppVersion;
			$is_from_app['DeviceID']	= $DeviceID;
			$is_from_appweb['PowerLesson'] = false;
			
			/* Comment out by Thomas on 2013-12-03 because of performance
			$req_url = "src/lessonplan/login_lesson.php?session_id=$r_var";
			$url = "loadpl.php?url=".urlencode($req_url);
			*/
			$goActiveLesson = true;
			//$url = "src/lessonplan/login_lesson.php?session_id=$r_var";
		} else {
			$is_from_app['PowerLesson'] = true;
			$is_from_app['AppVersion']	= $AppVersion;
			$is_from_app['DeviceID']	= $DeviceID;
			$is_from_appweb['PowerLesson'] = false;
			
			/* Comment out by Thomas on 2013-12-03 because of performance
			$req_url = "src/lessonplan/section_view.php?page_mode=viewLesson&plan_id=$r_var";
			$url = "loadpl.php?url=".urlencode($req_url);
			*/
			$url = (isset($web_protocol)? $web_protocol : 'http')."://".$eclass40_httppath."src/lessonplan/section_view.php?page_mode=viewLesson&plan_id=".$plan_id;
		}
		
		/*
		else if($jumpback=="PowerLessonFromAppWeb"){
			$is_from_app['PowerLesson'] = false;
			$is_from_appweb['PowerLesson'] = true;
			
			$url = "src/lessonplan/section_view.php?page_mode=viewLesson&plan_id=".$plan_id;
		}
		else if($jumpback=="PowerLessonFromAppWeb_Happening"){
			$is_from_app['PowerLesson'] = false;
			$is_from_appweb['PowerLesson'] = true;
			
			$goActiveLesson = true;
			//$url = "src/lessonplan/login_lesson.php?session_id=$r_var";
		}
		*/
//		else{
//			$is_from_app['PowerLesson'] = false;
//			$is_from_appweb['PowerLesson'] = false;
//		}
		
		//$ck_is_from_app = $is_from_app;
		session_register_eclass("ck_is_from_app", $is_from_app);
		
		//$ck_is_from_appweb = $is_from_appweb;
		session_register_eclass("ck_is_from_appweb", $is_from_appweb);
	}
	else
	{
		# Failed - because the user does not have access right to that course. 
		$ErrorCode = 102;
	}
} 

##########################################################################################
###### End of /eclass40/checkpl.php ######################################################
##########################################################################################

###### Step 3 ######

if($goActiveLesson == true)
{
##########################################################################################
###### Start of /eclass40/src/lessonplan/login_lesson.php ################################
##########################################################################################

	include_once($eclass40_filepath.'/system/settings/lang/'.$ck_default_lang.'.php');
	include_once($eclass40_filepath.'/src/includes/php/lib-lessonplan.php');
	
	$llp = new lessonplan();
	
	if ($llp->memberType == 'S'){
		$lsession = $llp->std_login_lesson($lsession_id);
		
		$_SESSION['ck_ec_subject_id'] = $lsession[0]['subject_id'];
	}else{
		$llp->db_db_query("SET autocommit=0");
		$llp->db_db_query("START TRANSACTION");
		$lsession = $llp->create_lesson_session($plan_id);

        if(!$lsession){
            die("<br><br><br><center>The support of PowerLesson is ended, please migrate your lesson plan to PowerLesson 2.</center>");
            exit();
        }

        $llp->db_db_query($sql);
		$sql = "select plan_section_id from ".$lsession[0]['scoreboard_table']." 
				where waiting = 1 or is_using = 1";
		$check = $llp->returnVector($sql);
		
		if (empty($check)){
			$lesson = $llp->get_first_lesson('','',$lsession[0]['scoreboard_table']);
			$sql = "update ".$lsession[0]['scoreboard_table']." set waiting = 1 where plan_section_id = '".$lesson['plan_section_id']."'";
			$llp->db_db_query($sql);
		}
		
		if($ck_is_from_app['PowerLesson']){
			$llp->recordUserDeviceIDForLesson($ck_course_id, $lsession[0]['lesson_plan_id'], $lsession[0]['session_id'], $ck_intranet_user_id, $ck_is_from_app['DeviceID']);
		}
		
		$llp->db_db_query("COMMIT");
	}
	
	if (!$ck_plugin_power_lesson){
		die("<br><br><br><center>PowerLesson is not allowed in the site.</center>");
	}
	
	if (empty($lsession)){
		die("<br><br><br><center>No lesson Session is found.</center>");
		exit();
	}

	$_SESSION['lesson_session'] = $lsession[0];
	
	if ($llp->memberType == 'S')
		header('location: '.(isset($web_protocol)? $web_protocol : 'http').'://'.$eclass40_httppath.'src/lessonplan/lesson_student.php');
	else
		header('location: '.(isset($web_protocol)? $web_protocol : 'http').'://'.$eclass40_httppath.'src/lessonplan/lesson_teacher.php');

##########################################################################################
###### End of /eclass40/src/lessonplan/login_lesson.php ##################################
##########################################################################################
} 
else 
{
	//var_dump($url);die();
	# redirect to corresponding pages or show error
	if($url != '')
	{
		header("Location: $url");
	} 
	else 
	{
		echo '<center>';
		echo 'Oops... the system encountered a problem (#'.$ErrorCode.')';
		echo '<input type="button" value="'.$button_back.'" onmouseout="this.className=\'formstartbutton\'" onmouseover="this.className=\'formstartbuttonon\'"  onclick="button_back_click()" class="formstartbutton" >';
		echo '<center>';
		echo '<script>
				function button_back_click(){
					location.href=\''.$cfg['PowerLessonApp']['Scheme']['FinishLesson'].'://\';
				}
			  </script>';
	}
}

closedb();
?>