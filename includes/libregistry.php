<?php
#using by : 

/*
 * Modification Log:
 *  2017-06-29 (Paul): modified UserNewAdd() to log user creation 
 * 	2013-07-17 (Ivan): modified UserAdd(), UserNewAdd() to check maximum UserID from INTRANET_USER and INTRANET_ARCHIVE_USER instead of using auto increment from DB
 * 	2012-06-19 (Ivan): modified functions UserAdd and UserNewAdd to record InputBy and ModifyBy UserID
 */

class libregistry extends libdb {

     function libregistry(){
          $this->libdb();
     }

     function UserAdd($UserEmail, $FirstName, $LastName, $RecordStatus){
     	  include_once("libuser.php");
          $lu = new libuser();
          $nextUserId = $lu->returnNextUserId();
          
     	  $targetUserId = ($_SESSION['UserID']=='')? 'null' : $_SESSION['UserID'];
          //$sql = "INSERT INTO INTRANET_USER (UserEmail, FirstName, LastName, RecordStatus, DateInput, DateModified, InputBy, ModifyBy) VALUES ('$UserEmail', '$FirstName', '$LastName', '$RecordStatus', now(), now(), $targetUserId, $targetUserId)";
          $sql = "INSERT INTO INTRANET_USER (UserID, UserEmail, FirstName, LastName, RecordStatus, DateInput, DateModified, InputBy, ModifyBy) VALUES ('$nextUserId', '$UserEmail', '$FirstName', '$LastName', '$RecordStatus', now(), now(), $targetUserId, $targetUserId)";
          return $this->db_db_query($sql);
     }

     function UserNewAdd($UserLogin , $UserEmail, $EnglishName, $ChineseName, $RecordStatus){
     	  global $sys_custom,$intranet_root;
          if (!$this->validateLogin($UserLogin)){
              return false;
          }
          
          include_once("libuser.php");
          $lu = new libuser();
          $nextUserId = $lu->returnNextUserId();
          
          $targetUserId = ($_SESSION['UserID']=='')? 'null' : $_SESSION['UserID'];
          //$sql = "INSERT INTO INTRANET_USER (UserLogin, UserEmail, EnglishName, ChineseName, RecordStatus, DateInput, DateModified, InputBy, ModifyBy) VALUES ('$UserLogin','$UserEmail', '$EnglishName', '$ChineseName', '$RecordStatus', now(), now(), $targetUserId, $targetUserId)";
          
          if($sys_custom['project']['NCS']){
          	include_once($intranet_root."/includes/libcurdlog.php");
          	$log = new libcurdlog();
          	$log->addlog("User Settings", "CREATE", "INTRANET_USER", "UserID", $nextUserId, "", "UserID: ".$nextUserId, $sql);
          	$logID = $log->db_insert_id();
          }
          $lu->insertAccountLog($nextUserId, 'C');
          $sql = "INSERT INTO INTRANET_USER (UserID, UserLogin, UserEmail, EnglishName, ChineseName, RecordStatus, DateInput, DateModified, InputBy, ModifyBy) VALUES ('$nextUserId', '$UserLogin','$UserEmail', '$EnglishName', '$ChineseName', '$RecordStatus', now(), now(), $targetUserId, $targetUserId)";
                    
          if($sys_custom['project']['NCS']){
          	$log->updateLog($logID, 'query', addslashes($sql));
          	unset($log);
          }

          return $this->db_db_query($sql);
     }

     function UserUpdateEmail($UserID, $UserEmail){
          $sql = "SELECT UserID FROM INTRANET_USER WHERE UserID = $UserID AND UserEmail = '$UserEmail'";
          $isUserEmailChanged = sizeof($this->returnArray($sql,1));
          if($isUserEmailChanged == 1){
               return 0; # Email unchanged
          }else{
               $sql = "SELECT UserID FROM INTRANET_USER WHERE UserID <> $UserID AND UserEmail = '$UserEmail'";
               $isUserEmailExisted = sizeof($this->returnArray($sql,1));
               if($isUserEmailExisted == 1){
                    return 1; # New email which has already existed
               }else{
                    $sql = "UPDATE INTRANET_USER SET UserEmail = '$UserEmail', DateModified = now() WHERE UserID = $UserID";
                    $this->db_db_query($sql);
                    return 2; # New email which can be updated
               }
          }
     }

     function UserChangePassword($UserID, $OldPassword, $NewPassword){
          $sql = "SELECT UserID FROM INTRANET_USER WHERE UserID = $UserID AND UserPassword = '$OldPassword'";
          $isUserPasswordCorrect = sizeof($this->returnArray($sql,1));
          if($isUserPasswordCorrect == 0){
               return 0;
          }else{
               $sql = "UPDATE INTRANET_USER SET UserPassword = '$NewPassword', DateModified = now() WHERE UserID = $UserID";
               $this->db_db_query($sql);
               return 1;
          }
     }

}
?>