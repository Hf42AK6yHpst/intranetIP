<?php
## Using By : yat

###########################################
#
###########################################

if (!defined("LIBSPORTS_CUST_DEFINED"))                     
{

	define("LIBSPORTS_CUST_DEFINED",true);
	
	class libsports_cust extends libsports 
	{
		function libsports_cust ()
		{
			$this->libdb();
		}
		
		#####################################################################
		#	Customization for Sana Rosa [Case#2013-0424-1211-29096]
		#####################################################################
		function Get_Year_List()
		{
			##### Secondary 
			$sql = "select 
								YearID, YearName
							From 
								YEAR 
							Where
								RecordStatus = 1
								and YearName like 'Form%'
							Order By 
								Sequence desc";
			$sResult = $this->returnArray($sql);
			
			##### Primary
			$sql = "select 
								YearID, YearName
							From 
								YEAR 
							Where
								RecordStatus = 1
								and YearName like 'Primary%'
							Order By 
								Sequence desc";
			$pResult = $this->returnArray($sql);
			
			
			$Result = array_merge($sResult, $pResult);
			return $Result;
		}
		
		
		
		
		#####################################################################
		#	[End] Customization for Sana Rosa [Case#2013-0424-1211-29096]
		#####################################################################
		
		
	}
}
?>