<?php
/**
 * ACI class for new linux base 116x controller.
 * Booking flow :
 * Example:
 * 00000000002885AC,001,01,11122013,163200,163300
 *
 * @author Bob Pang <bobpang@broadlearning.com>
 * @version v0.10
 * @link http://www.eclass.com.hk/
 * @copyright Copyright &copy; 2003-2014 BroadLearning Education (Asia) Limited
 */
 
class libdooraccess_aci2
{  
    /**
    * Create new booking record string
    * 1 = Write file successfully!
    * @param array $arr_str two dimensional array of booking records
      e.g
           array(
                array(
                        "card_id"=>"00000000002885AC",
                        "door_id"=>"1",
                        "date"=>"12122013",
                        "start_time"=>"104600",
                        "end_time"=>"104700"
                    ),
                    .
                    .
                    .
            }
       * @return boolean return Write file status.
    */
    function actionCreate($arr_str)
    {
       //sort($arr_str);
       foreach ($arr_str as $v) 
       { 
            $data .= $v['card_id'].",001,".str_pad($v['door_id'],2,0,STR_PAD_LEFT).",".$v['date'].",".$v['start_time'].",".$v['end_time']."\n";         
       }
       
       $this->actionWriteIndividualTxt($data);
       
       return "1";
    }

    /**
    * Write booking record string to "individual.txt" file
    * @param string $str  booking record string
    * @param string $file file to write string
    */
    function actionWriteIndividualTxt($str,$file="/tmp/individual.txt")
    {
        $OF = @fopen($file, 'a') or die("can't open file");
        fwrite($OF,$str);
        fclose($OF);
    }
    
    /**
    * Ftp upload "individual.txt" file to the controller "/pub" folder
    * Return : 
    * 1 = Upload file succesfully!
    * 0 = Upload file fail!
    * 
    * @param integer $ftp_host the controller IP.
    * @param string $ftp_user the controller ftp user name. 
    * @param string $ftp_pass the controller ftp user password.
    * @param string $file the file need to upload to controller, default is "/tmp/individual.txt" file
    * @return boolean return ftp upload "individual.txt" file status.
    */
    function actionUpload($ftp_host,$ftp_user="anonymous",$ftp_pass="",$file="/tmp/individual.txt")
    {
        //Add "END" at the end of the txt file
        $OF = @fopen($file, 'a') or die("can't open file");
        fwrite($OF, "\r\nEND");
        fclose($OF);
        
	$remote_file = '/home/ftp/pub/individual.txt';
        $conn_id = ftp_connect($ftp_host) or die("Couldn't connect to $ftp_host"); ;

        // login with username and password
        $login_result = ftp_login($conn_id, $ftp_user, $ftp_pass);

        // upload a file
        if (ftp_put($conn_id, $remote_file, $file, FTP_ASCII)){
            ftp_close($conn_id);
            @unlink($file);
            return "1";
            
        } else {
            ftp_close($conn_id);
            @unlink($file);
            return "0";
        }
    }
}
?>
