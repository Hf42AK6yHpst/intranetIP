<?php
# using: 

###################################################### Change Log ######################################################
# Date      : 20201106 (Ray)  added CheckAppLoginSession
# Date		: 20190624 (Carlos) modified ChangePassword(), empty UserPassword field.
# Date		: 20190430 (Henry) modified updateRadiusServerPassword for command injection handling
# Date		: 20190220 (Carlos) [ip.2.5.10.2.1] modified WS_Login() for LDAP to adapt another authentication method and exclude broadlearning from LDAP login.
# Date      : 20180918 (Ivan) [ip.2.5.9.10.1] -modified WS_Login() to remove control character in user's eng and chi name
# Date      : 20180508 (Tiffany) [ip.2.5.9.7.1] -modified WS_Login() add ClassAttendanceType, that means 1 am, 2 pm, 3 whole day
# Date      : 20180222 (Ronald) [ip.2.5.9.3.1] -modified ChangePassword() to send push message for ALL app types (instead of only Parent App) after success in changing password
# Date		: 20180103 (Carlos) [ip.2.5.9.1.1] - added CheckPasswordCriteria() for checking password criteria.
# Date		: 20171124 (Bill) [2017-1121-1046-19206] [ip.2.5.9.1.1] - modified WS_Login() to handle HTML entity in Chinese Name for IOS display
# Date		: 20171011 (Ivan) [D128076] [ip.2.5.7.10.1] - modified validate_hashed() to add lowercase UserLogin checking to cater some very early created accounts
# Date		: 20170626 (Tiffany) - Modified WS_Login(), pass department info when under dhl setting.
# Date		: 20161110 (Henry HM) [ip.2.5.7.10.1] - Add WS_Login_Centennial()
# Date		: 20160905 (Pun) [ip.2.5.7.10.1] - modified GetUserIDFromSessionKey(), added expire checking for sessionKey
# Date		: 20160905 (Pun) [ip.2.5.7.10.1] - modified validate_hashed(), validate_plain(), WS_Login(), change to call libdb's updateSessionByUserId() instead of calling sql
# Date		: 20160823 (Ivan) [ip.2.5.7.10.1] - modified WS_Login() to fix missing 2nd parameter for function session_register_intranet()
# Date		: 20160525 (Ivan) [ip.2.5.7.7.1] - modified WS_Login() to fix cannot login student app using demo accounts
# Date		: 20160309 (Roy)	- add getUserYearClassInfo() to get class Chinese & English name for app login
#								  Modified WS_Login(), return class Chinese & English name for app login
# Date		: 20160106 (Carlos) - Modified ChangePassword(), let it change password if old password is same as userlogin even security settings does not allow to change password.
# Date		: 20151214 (Carlos) - Modified UpdateEncryptedPassword($ParamUserID, $ParamPassword) and added updateRadiusServerPassword($username,$password) for updating radius server password. 
# Date		: 20151120 (Yuen)	- Modified validate() to adopt new password (individual for each site/school)
# Date		: 20151119 (Ivan)	- [X88599][ip.2.5.7.1.1] Modified WS_Login() to return UserLogin from database for eClassApp request to support email login
# Date		: 20151116 (Thomas) - Modify WS_Login(), add parameter $fromQRCodeLogin and support login mechanism for $sys_custom['SingleSignOnIP25PL']
#								  Modify WS_QRCodeLogin(), pass 'true' for input parameter - $fromQRCodeLogin of function WS_Login()
# Date		: 20151002 (Qiao) -   Modify WS_Login() and getUserInfo() add TitleEnglish and TitleChinese to result
# Date		: 20150918 (Qiao) -   Modify ChangePassword()
# Date		: 20150710 (Qiao) -   Add ChangePassword()
# Date		: 20150505 (Thomas) - Modified WS_QRCodeLogin(), support LDAP login
# Date		: 20150504 (Carlos) - Modified validate_hashed() and validate_plain(), added customized control for Document Routing to allow suspended users to pass the validation.
# Date		: 20150311 (Thomas) - Modified WS_Login() to support LDAP with different users using different LDAP dn string
# Date		: 20150304 (Ivan) - Modified WS_Login() return empty string instead of null for ClassLevel, ClassName, ClassNumber
# Date		: 20150303 (Thomas) - Modified getUserInfo() to return ClassLevel, ClassName, ClassNumber
#								- Modified WS_Login() to return SchoolCode, ClassLevel, ClassName, ClassNumber
# Date		: 20141020 (Thomas) - Modified WS_Login() to support LDAP
# Date		: 20140911 (Ivan)	- Modified WS_Login() to return empty string instead of null if there are no EnglishName or ChineseName [U67632]
# Date		: 20140808 (Ivan)	- Modified WS_Login() to support demo site account auto mapping for eClassApp login
# Date		: 20140703 (Carlos) - Modified UpdateEncryptedPassword() and GetUserDecryptedPassword() use the new approach to get/set passwords
# Date		: 20140331 (Thomas) - Modified GetSessionKey(), Handle the case of hash password
# Date		: 20140320 (Thomas) - Modified getUserInfo(), add HashedPass field in sql
#								- Modified WS_Login(), add parameter $HashPassword
#								- Modified WS_QRCodeLogin(), Handle the case of hash password
# Date		: 20140120 (Yuen) - removed central password for btw user account (requested by CS)
# Date		: 20131230 (Ivan) - modified WS_Login() to add eClassApp license checking
# Date		: 20130909 (Ivan) [2013-0903-1525-07169] - improved function validate() and validate_hashed() to check hash password directly
# Date		: 20130821 (Thomas) - Added function WS_QRCodeLogin() for web service login through QR Code Login
# Date		: 20130820 (Thomas) - Added function validate_PowerLessonAppQRCodeLogin() to validate User Login through PowerLesson App QR Code Login
# Date		: 20130613 (Thomas) - modified function WS_Login() and getUserInfo(), add field PersonalPhotoLink
# Date		: 20130302 (YatWoon) - update IsConsecutiveUnsuccessfulLogins()
# Date		: 20130308 (yuen) - added getProtentialRiskByPassword() to sort out high risk accounts (i.e. password==login)
# Date		: 20130206 (Yuen) - Use central password for btw user account
# Date		: 20121127 (YatWoon) - update validate(), add status checking for broadlearning account
# Date		: 20121120 (Thomas) - Modified function GetSessionKey(), add special handle case for 'broadlearning' account
# Date		: 20121009 (YatWoon) - Modified function Encrypt(), Decrypt(), add checking to check function_exists(mcrypt_xxxx) [Case#2012-1008-0909-07073]
# Date		: 20121008 (YatWoon) - Modified function validate(), missing check RecordStatus for hash password checking. 
# Date		: 20120918 (Thomas) - Modified function WS_Login(), replace validate_plain() with validate() to cater the case of hash password
#								- Modified function GetSessionKey(), handle the case of hash password

# Date		: 20120914 (Yuen) - completed Account Lockout Policy (settings are available in admin console)
#
# Date		: 20120712 (Yuen) - Introduced failed attempt control to login
#
# Date		: 20110103 (Yuen) - Use central password for broadlearning user account

# Date		: 20110930 (Carlos) - Added UpdateEncryptedPassword() for storing two-way hased password
#
# Date 		: 20091103 (Ronald)
# Details 	: As now we are using UTF8 charset, so in the function validate_plain(), 
#				I have modified the query by adding COLLATE utf8_bin in the query, 
#				so that now the userlogin and userpassword is case sensitive.
###################################################### Change Log ######################################################


class libauth extends libdb {

	const RESET_PASSWD_ENCRYPT_KEY = "sc4IP39fUwU+SOpCEDP6eNNRIW7JKN4lLKV3g3sN";
	const RESET_PASSWD_SALT_SIZE = 16;
	
     var $LoginAttemptLimit;
     var $LoginLockDuration;

     function libauth(){
			global $special_feature, $intranet_root;
			
			$this->libdb();
			
			include_once("{$intranet_root}/includes/libfilesystem.php");
			$li = new libfilesystem();
			
			$login_lock_file = $li->file_read($intranet_root."/file/login_attempt_lock.txt");
			list($login_attempt_limit, $login_lock_duration) = explode("\n", $login_lock_file);

			if ($login_attempt_limit!="" && $login_lock_duration!="")
			{
				$this->LoginAttemptLimit = $login_attempt_limit;
				$this->LoginLockDuration = $login_lock_duration;
			} else
			{
				$this->LoginAttemptLimit = 10;
				$this->LoginLockDuration = 15;
			}
     }
     
     
     function getProtentialRiskByPassword()
     {
			global $intranet_authentication_method, $PATH_WRT_ROOT, $plugin, $SYS_CONFIG, $intranet_root, $intranet_password_salt;
			
			# display to system admin and not using LDAP
			if ($_SESSION['SSV_PRIVILEGE']['schoolsettings']["isAdmin"] && $intranet_authentication_method!="LDAP")
			{
				# select user recordstatus==1 and (LastUsed is null or 3 months ago)
				$sql = "SELECT UserID, UserLogin, RecordType, EnglishName, ChineseName, ClassName, ClassNumber, LastUsed " .
						"FROM INTRANET_USER " .
						"WHERE RecordStatus=1 AND (LastUsed IS NULL OR DATEDIFF(now(), LastUsed)>90) AND DATEDIFF(now(), DateInput)>90  " .
						"		AND MD5(CONCAT(UserLogin,UserLogin,'$intranet_password_salt')) = HashedPass " .
						"ORDER BY RecordType, ClassName, ClassNumber, EnglishName";
				$rows = $this->returnArray($sql);
				
				return $rows;

				/*
				for ($i=0; $i<sizeof($rows); $i++)
				{
					$userObj = $rows[$i];
					$userPassword = $this->GetUserDecryptedPassword($userObj["UserID"]);
					# check if password == loginID
					if ($userPassword!="" && strtoupper($userObj["UserLogin"])==strtoupper($userPassword))
					{
						# record the user
						$riskyUsers[] = $userObj;
					}					
				}
				
				return $riskyUsers;
				*/
			} else
			{
				return null;
			}
     }


     function validate($user, $passwd, $passwd_hash='', $validateonly=0)
     {
              global $intranet_authentication_method, $PATH_WRT_ROOT, $plugin, $SYS_CONFIG, $intranet_root;
              # authenticate for reserved system account
             // if (strtolower($user)=="broadlearning" || strtolower($user)=="btw")
              if (strtolower($user)=="broadlearning" )
              {              	
              		# get the password from central server
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_REFERER, $_SERVER["SERVER_NAME"]);
					@curl_setopt($ch, CURLOPT_URL, "http://eclassupdate.broadlearning.com/api/blpswd.php?ptype=3");
					
					// grab URL and pass it to the browser
					$password_central = trim(curl_exec($ch));
					//$account_name = (strtolower($user)=="broadlearning") ? "ac_broadlearning" : "ac_btw";
					$account_name =  "ac_broadlearning";
				
					if ($password_central!="" && strlen($password_central)>3)
					{
						if (md5("2012allschools".$passwd)==$password_central)
						{
							
							# check account exists
							$sql = "select UserID from INTRANET_USER where UserLogin='$user' and RecordStatus=1";
							$temp = $this->returnVector($sql);
							$login_user_id = $temp[0];
							
							# log the access
							$sql = "UPDATE INTRANET_USER SET LastUsed = now() WHERE UserID = '$login_user_id'";
							$this->db_db_query($sql);
							
							$results = $this->getSessionKeyLastUpdatedFromUserId($login_user_id);
							$session_key = trim($results["SessionKey"]);
							if ($session_key=="")
							{
							    $session_key = $this->generateSessionKey();
							    $this->updateSessionByUserId($session_key, $login_user_id);
							}
							$this->updateSessionLastUpdatedByUserId($login_user_id);
							
							
							if(sizeof($temp)>0)
							{
								$sql = "SELECT Module FROM GENERAL_SETTING WHERE Module='blpwdsyn' AND SettingName='{$account_name}' AND SettingValue='{$password_central}'";
								$previous_record = $this->returnVector($sql);
								
								if (sizeof($previous_record)<=0)
								{
									# to update password for integrated systems
									
									### MAIL
									include_once($PATH_WRT_ROOT."includes/libwebmail.php");
									$lwebmail = new libwebmail();
									if ($lwebmail->has_webmail)
									{
										$lwebmail->change_password($user,$passwd,"iMail");
									}
									
									if($plugin["imail_gamma"]==true)
									{
										include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
										$IMap = new imap_gamma(1);
										$IMapEmail = trim($user)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
										if($IMap->is_user_exist($IMapEmail))
										{
											$IMap->change_password($IMapEmail, $passwd);
										}
									}
									
									### FTP management
									if ($plugin['personalfile'])
									{
										include_once($PATH_WRT_ROOT."includes/libftp.php");
										$lftp = new libftp();
										if ($lftp->isFTP)
										{
											$lftp->changePassword($user,$passwd,"iFolder");
										}
									}
									
									
									# update General Settings - markd the script is executed									
									$sql = "delete from GENERAL_SETTING where Module='blpwdsyn' AND SettingName='{$account_name}'";
									$this->db_db_query($sql);
									
									$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('blpwdsyn', '{$account_name}', '{$password_central}', now())";
									$this->db_db_query($sql);
								}
							}
							
							return $login_user_id;
						} else
						{
							return false;
						}
					}
              }

				# authenticate for normal users
				if ($intranet_authentication_method == "HASH")
				{
					/*
					# handle to use real login ID in order to matach hashed value
					$sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserLogin='".$user."' and RecordStatus=1";
					$row = $this->returnVector($sql);
					
					if ($row[0]!="")
					{
						$user = $row[0];
					}
					else
						return false;
					*/
				    return $this->validate_hashed($user, $passwd, $validateonly, $passwd_hash);
				}
				else
				{
				    return $this->validate_plain($user, $passwd, $validateonly);
				}
     }

     function validate_hashed($user, $passwd, $validateonly=0, $passwd_hash='')
     {
              global $intranet_password_salt, $sys_custom;
              
              $passwd = intranet_undo_htmlspecialchars($passwd);
              
              # allow to use email to login
              if ($passwd_hash != '') {
              	$conds_password = " AND HashedPass = '".$passwd_hash."' ";
              }
              else {
              	//$conds_password = " AND (MD5(CONCAT(UserLogin,'$passwd$intranet_password_salt')) = HashedPass OR MD5(CONCAT('{$user}','$passwd$intranet_password_salt')) = HashedPass) ";
              	$conds_password = " AND (MD5(CONCAT(UserLogin,'$passwd$intranet_password_salt')) = HashedPass OR MD5(CONCAT('{$user}','$passwd$intranet_password_salt')) = HashedPass OR MD5(CONCAT('".strtolower($user)."','$passwd$intranet_password_salt')) = HashedPass) ";
              }
              
              if(isset($sys_custom['DocRouting']['SpecialGroup']) && count($sys_custom['DocRouting']['SpecialGroup'])>0 
              	&& (strpos($_SERVER['REQUEST_URI'],'/home/drs.php')!==false || strpos($_SERVER['REQUEST_URI'],'/home/iaccount/account/login_password_update.php')!==false))
              {
              		$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordStatus IN ('0','1') AND (UserLogin = '$user' OR UserEmail = '$user') $conds_password";
              }else{
              		$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordStatus = '1' AND (UserLogin = '$user' OR UserEmail = '$user') $conds_password";
              }
              $temp = $this->returnVector($sql);

              $target_id = $temp[0];
              if ($target_id == "")
              {
                return false;
              }
              else if($validateonly)
              {
              	return $target_id;
              }
              else
              {
                  $sql = "UPDATE INTRANET_USER SET LastUsed = now() WHERE UserID = '$target_id'";
                  $this->db_db_query($sql);
                  
                  $results = $this->getSessionKeyLastUpdatedFromUserId($target_id);
                  $session_key = trim($results["SessionKey"]);
                  if ($session_key=="")
                  {
                      $session_key = $this->generateSessionKey();
                      $this->updateSessionByUserId($session_key, $target_id);
                  }
				  $this->updateSessionLastUpdatedByUserId($target_id);
                  
                  return $target_id;
              }
     }
	function validate_plain($user, $passwd, $validateonly=0){
		global $sys_custom;
		
		$passwd = intranet_undo_htmlspecialchars($passwd);
		if(isset($sys_custom['DocRouting']['SpecialGroup']) && count($sys_custom['DocRouting']['SpecialGroup'])>0 
			&& (strpos($_SERVER['REQUEST_URI'],'/home/drs.php')!==false || strpos($_SERVER['REQUEST_URI'],'/home/iaccount/account/login_password_update.php')!==false))
		{
			$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordStatus IN ('0','1') AND BINARY UserPassword = '$passwd' AND (UserLogin = '$user' OR UserEmail = '$user')";
		}else{
        	$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordStatus = '1' AND BINARY UserPassword = '$passwd' AND (UserLogin = '$user' OR UserEmail = '$user')";
		}
        //$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordStatus = '1' AND BINARY UserPassword = '$passwd' AND (BINARY UserLogin = '$user' OR BINARY UserEmail = '$user')";
        $row = $this->returnArray($sql,1);

        if(sizeof($row)==0){
			return 0;
		}
		else if($validateonly)
        {
        	return $row[0][0];
        } 
        else 
        {
			$tUserID = $row[0][0];
			
			$sql = "UPDATE INTRANET_USER SET LastUsed = now() WHERE UserID = '$tUserID'";
			$this->db_db_query($sql);
			
			$results = $this->getSessionKeyLastUpdatedFromUserId($tUserID);
			$session_key = trim($results["SessionKey"]);
			if ($session_key=="")
			{
			    $session_key = $this->generateSessionKey();
			    $this->updateSessionByUserId($session_key, $tUserID);
			}
		    $this->updateSessionLastUpdatedByUserId($tUserID);
			
			return $tUserID;
		}
	}
	
	function validate_PowerLessonAppQRCodeLogin($PL_courseID, $PL_lessonID, $PL_lessonSessionID, $PL_userCourseID){
		global $intranet_db, $eclass_db, $eclass_prefix;
		
		$sql = "SELECT
					lp.target_type 
				FROM 
					".$eclass_prefix."c".$PL_courseID.".lesson_session AS ls INNER JOIN
					".$eclass_prefix."c".$PL_courseID.".lesson_plan AS lp ON ls.lesson_plan_id = lp.plan_id 
				WHERE 
					session_id = '$PL_lessonSessionID' AND 
					lesson_plan_id = '$PL_lessonID'";
		$result = $this->returnVector($sql);
		
		if(count($result)){
			$target_type = $result[0];
			
			if($target_type==1){ // Group Lesson Plan
				$sql = "SELECT
							iu.UserID
						FROM
							".$eclass_prefix."c".$PL_courseID.".grouping_function AS gf INNER JOIN
							".$eclass_prefix."c".$PL_courseID.".lesson_plan_user_group AS ug ON gf.function_type = 'PL' AND gf.group_id IS NOT NULL AND gf.function_id = '$PL_lessonID' AND gf.group_id = ug.lesson_group_id INNER JOIN
							".$eclass_prefix."c".$PL_courseID.".usermaster AS u ON u.status IS NULL AND u.memberType !='T' AND ug.user_id = u.user_id INNER JOIN
							".$eclass_db.".user_course AS uc ON uc.user_course_id = '$PL_userCourseID' AND u.user_id = uc.user_id INNER JOIN
							".$intranet_db.".INTRANET_USER AS iu ON uc.user_email = iu.UserEmail
						WHERE
							iu.RecordStatus = 1";
				$result = $this->returnVector($sql);
			}
			else{ // Individual Lesson Plan
				$sql = "SELECT
							iu.UserID 
						FROM
							".$eclass_prefix."c".$PL_courseID.".grouping_function AS gf INNER JOIN
							".$eclass_prefix."c".$PL_courseID.".usermaster AS u ON gf.function_type = 'PL' AND gf.group_id IS NULL AND gf.function_id = '$PL_lessonID' AND u.status IS NULL AND u.memberType !='T' AND gf.user_id = u.user_id INNER JOIN
							".$eclass_db.".user_course AS uc ON uc.user_course_id = '$PL_userCourseID' AND u.user_id = uc.user_id INNER JOIN
							".$intranet_db.".INTRANET_USER AS iu ON uc.user_email = iu.UserEmail
						WHERE
							iu.RecordStatus = 1";
				$result = $this->returnVector($sql);
						
				if(count($result)==0){
					$sql = "SELECT
								iu.UserID 
							FROM
								".$eclass_prefix."c".$PL_courseID.".usermaster AS u INNER JOIN
								".$eclass_db.".user_course AS uc ON u.status IS NULL AND u.memberType !='T' AND uc.user_course_id = '$PL_userCourseID' AND u.user_id = uc.user_id INNER JOIN
								".$intranet_db.".INTRANET_USER AS iu ON uc.user_email = iu.UserEmail
							WHERE
								iu.RecordStatus = 1";
					$result = $this->returnVector($sql);
				}
			}
			
			return count($result)? $result[0] : 0;
		}
		
		return 0;
	}
	
	function check_password($user, $passwd)
	{ 
		global $intranet_authentication_method;
//		debug_pr("$user, $passwd, $intranet_authentication_method");
		if ($intranet_authentication_method == "HASH")
		{
		    return $this->validate_hashed($user, $passwd, 1);
		}
		else
		{
		    return $this->validate_plain($user, $passwd, 1);
		}
		
	}

     function forgetPassword($user){
          $sql = "SELECT UserID, UserLogin, UserPassword, UserEmail, FirstName, LastName FROM INTRANET_USER WHERE RecordStatus = '1' AND (UserLogin = '$user' OR UserEmail = '$user')";
          $row = $this->returnArray($sql,6);
          return (sizeof($row)==0) ? 0 : $row[0];
     }

     function getUserInfo($user)
     {
              $sql = "SELECT
              			UserID, UserLogin, UserPassword, UserEmail, EnglishName,
              			ChineseName, RecordType, PersonalPhotoLink, HashedPass, ClassLevel,
              			ClassName, ClassNumber,
                        TitleEnglish,TitleChinese
              		FROM
              			INTRANET_USER
              		WHERE
              			RecordStatus = 1 AND
              			(UserLogin = '$user' OR UserEmail = '$user')";
              $row = $this->returnArray($sql,9);
              return (sizeof($row)==0) ? 0 : $row[0];
     }
     
     function getUserYearClassInfo($userId) {
     	$sql = "SELECT
					yc.YearClassID as YearClassID, yc.ClassTitleEN as ClassNameEn, yc.ClassTitleB5 as ClassNameCh
				FROM
					YEAR_CLASS as yc
					Inner Join YEAR_CLASS_USER as ycu  on (yc.YearClassID = ycu.YearClassID) 
				WHERE
					ycu.UserID = '$userId'
					AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
		$row = $this->returnArray($sql, 3);
		return (sizeof($row) == 0) ? 0 : $row[0];
     }

     function forgetPassword2($user)
     {
              $sql = "SELECT UserID, UserLogin, UserPassword, UserEmail, EnglishName, ChineseName FROM INTRANET_USER WHERE RecordStatus = '1' AND (UserLogin = '$user' OR UserEmail = '$user')";
              $row = $this->returnArray($sql,6);
              return (sizeof($row)==0) ? 0 : $row[0];
     }
     function getHashedPassword($login, $password)
     {
              global $intranet_password_salt;
              
              //$target = strtolower($login) . $password . $intranet_password_salt;
              
              return md5($login . $password . $intranet_password_salt);
     }
	
	# Login function used for Web Service 
	/*
	function WS_Login($UserName, $Password) {			
		$tUserID = $this->validate_plain($UserName, $Password);
		$UserDetail = $this->getUserInfo($UserName);
		$UserEmail = $UserDetail['UserEmail'];		
		
		if($tUserID) {			
			$SessionKey = $this->GetSessionKey($UserName, $Password);
			$ReturnContent = array('SessionID'=>$SessionKey, 'UserEmail'=>$UserEmail, 'UserID'=>$tUserID);			
		}else{
			$ReturnContent = '401';			
		}
		
		return $ReturnContent;		 
	}
	*/
	
	# Login function used for Web Service
	# Henry Yuen (2010-05-07): select Chinese/ English names as well
	# Henry Yuen (2010-06-15): added checking on user License  
	# Henry Yuen (2010-08-09): added School Name and Organization
	function WS_Login($UserName, $Password, $hasAPIKey=false, $fromEClassApp=false, $HashPassword='', $fromQRCodeLogin=false) {
		global $module_code, $intranet_root, $plugin, $eclassAppConfig, $config_school_code, $sys_custom;
		global $UseLDAP, $intranet_authentication_method, $intranet_password_salt;
		global $ldap_user_type_mode, $ldap_teacher_base_dn, $ldap_student_base_dn, $ldap_parent_base_dn, $ldap_tcp_dns, $ldap_host, $ldap_port;
		
		$byPassLicenseCheck = $hasAPIKey; // NOTE: used to by pass license checking (use for DEV or DEMO)
		//$UserID = $this->validate_plain($UserName, $Password); // Comment out by Thomas on 2012-09-18 to handle both plain password and hash password
		
		$originalUserName = $UserName;
		if (strtolower($UserName) != 'broadlearning' && ((isset($UseLDAP) && $UseLDAP) || $intranet_authentication_method == 'LDAP')){
			include_once("$intranet_root/includes/libldap.php");
			
			if ($ldap_user_type_mode)
			{
				# Get User Type
				$sql = "SELECT UserID, RecordType FROM INTRANET_USER WHERE UserLogin = '$UserName'";
				$temp = $this->returnArray($sql,2);
		
				list($t_id, $t_user_type) = $temp[0];
		
				switch ($t_user_type)
				{
					case 1: $special_ldap_dn = $ldap_teacher_base_dn; break;
					case 2: $special_ldap_dn = $ldap_student_base_dn; break;
					case 3: $special_ldap_dn = $ldap_parent_base_dn; break;
					default: $special_ldap_dn = $ldap_teacher_base_dn;
				}
			}
			
			$lldap = new libldap();
			//$lldap->connect();
			// find all ldap servers with nslookup
			if(isset($ldap_tcp_dns) && $ldap_tcp_dns != ''){
				$tmp_ldap_hosts = $lldap->getLdapHosts($ldap_tcp_dns);
			}else{
				$tmp_ldap_hosts = array(array($ldap_host, $ldap_port));
			}
			$is_ldap_connected = false;
			// connect to one of the ldap servers that is available
			for($i=0;$i<count($tmp_ldap_hosts);$i++){
				$lldap->changeToHost($tmp_ldap_hosts[$i][0], $tmp_ldap_hosts[$i][1]);
				$is_ldap_connected = $lldap->connect();
				if($is_ldap_connected) break;
			}
			
			if ($is_ldap_connected && !( ($lldap->validate_try_all_ou($UserName,$Password) && $lldap->validate($UserName,$Password)) || $lldap->validateDomainUser($UserName,$Password) ) )
			{
				$UserID = 0;
			} else
			{
				$sql = "SELECT UserID, UserEmail, ImapUserEmail FROM INTRANET_USER WHERE UserLogin = '$UserName' and RecordStatus=1";
				$result = $this->returnArray($sql,3);
				$UserID = $result[0][0]+0;
				if ($UserID!=0)
				{
					//$sql = "UPDATE INTRANET_USER SET LastUsed = now(), UserPassword = '$UserPassword' WHERE UserID = '$ID' ";
					# 2010-07-01 [Yuen]
					//$session_key = $this->generateSessionKey();
					//$sql = "UPDATE INTRANET_USER SET LastUsed = now(), SessionKey = '$session_key', SessionLastUpdated = now(), UserPassword = '$Password' WHERE UserID = '$UserID' ";
					//$this->db_db_query($sql);
					
				    # 2016-09-05 [Pun]
                    $sql = "UPDATE INTRANET_USER SET LastUsed = now(), UserPassword = '$Password' WHERE UserID = '$UserID'";
                    $this->db_db_query($sql);
                    
                    $results = $this->getSessionKeyLastUpdatedFromUserId($UserID);
                    $session_key = trim($results["SessionKey"]);
                    if ($session_key=="")
                    {
                        $session_key = $this->generateSessionKey();
                        $this->updateSessionByUserId($session_key, $UserID);
                    }
                    $this->updateSessionLastUpdatedByUserId($UserID);
                  
					
					include_once("$intranet_root/includes/libeclass40.php");
					$leclass = new libeclass();
					$UserEmail = $result[0][1];
					$leclass->eClassUserUpdatePassword($UserEmail,$Password);
			
					/*
					 # Syn mail server
					if ($webmail_not_config_to_ldap)
					{
					include_once("includes/libwebmail.php");
					$lwebmail = new libwebmail();
					$lwebmail->change_password($UserLogin, $UserPassword);
					}
					*/
			
					# Syn mail server
					include_once("$intranet_root/includes/libwebmail.php");
					include_once("$intranet_root/includes/imap_gamma.php");
			
					if($plugin['imail_gamma']){
						$IMap = new imap_gamma(1);
						$imap_user_email = trim($result[0][2]);
						if($imap_user_email != '' && $IMap->is_user_exist($imap_user_email)){
							$IMap->change_password($imap_user_email, $Password);
						}
					}else{
						$lwebmail = new libwebmail();
						if ($lwebmail->has_webmail)
						{
							$lwebmail->change_password($UserName, $Password);
						}
					}
			
					# Syn FTP Server
					include_once("$intranet_root/includes/libftp.php");
					if ($plugin['personalfile'])
					{
						$lftp = new libftp();
						if ($lftp->isFTP)
						{
							$lftp->changePassword($UserName, $Password);
						}
					}
			
					# AeroDrive login variables
					$tmp_username = $UserName;
					session_register_intranet("tmp_username", $UserName);
					$tmp_password = $Password;
					session_register_intranet("tmp_password", $Password);
			
				}
			}
		}
		else if(!$fromQRCodeLogin && $UserName!='broadlearning' && $sys_custom['sahk_login_without_prefix'] && isset($sys_custom['SingleSignOnIP25PL']) && is_array($sys_custom['SingleSignOnIP25PL']) && count($sys_custom['SingleSignOnIP25PL'])>0){
			$HashPassword = $HashPassword? $HashPassword:md5($UserName.$Password.$intranet_password_salt);
			
			for($i=0; $i<count($sys_custom['SingleSignOnIP25PL']); $i++){
				$tmpUserName = $sys_custom['SingleSignOnIP25PL'][$i][1].$UserName;

				$UserID = $this->validate($tmpUserName, $Password, $HashPassword);
				
				if($UserID > 0){
					$UserName = $tmpUserName;
					break;
				}
			}
		}
		else{
			if($HashPassword!==''){
				$UserID = $this->validate($UserName, $Password, $HashPassword);
			}
			else{
				$UserID = $this->validate($UserName, $Password);
			}
		}
		
		//if ($fromEClassApp) {
			if ($sys_custom['demo_site_account_mapping'] && $UserID>0)
		    {
		    	$Map2DemoAccount = false;
		    	if (strlen($UserName)==strpos($UserName, "_t")+2) 
		    	{
		    		$Map2DemoAccount = true;
		    		$demo_account_login = "t";
		    	} elseif (strlen($UserName)==strpos($UserName, "_s")+2)
		    	{
		    		$Map2DemoAccount = true;
		    		$demo_account_login = "s";
		    	} elseif (strlen($UserName)==strpos($UserName, "_p")+2)
		    	{
		    		$Map2DemoAccount = true;
		    		$demo_account_login = "p";
		    	}   	
		    	
		    	if ($Map2DemoAccount)
		    	{
		    		$sql_demo = "SELECT UserID, HashedPass FROM INTRANET_USER where UserLogin='".$demo_account_login."'";
		    		$demo_account = $this->returnResultSet($sql_demo);
		    		$UserID = trim($demo_account[0]['UserID']);
		    		$hashedPwd = trim($demo_account[0]['HashedPass']);
		
		    		# get password by decryption
		    		if ($UserID!="" && $UserID>0)
		    		{
		    			# change to use demo account
		    			$UserName = $demo_account_login;
		    			$Password = $this->GetUserDecryptedPassword($UserID);
		    			
		    			$UserID = $this->validate($UserName, $Password, $hashedPwd);
		    		}
		    	}
		    }
		    else if ($fromEClassApp && $sys_custom['demo_site_account_mapping'] && (strtolower($UserName)=='p'||strtolower($UserName)=='t') && !empty($Password)) {
		    	// for eClass App demo account
		    	$sql = "SELECT UserID FROM INTRANET_USER where UserLogin='".strtolower($UserName)."'";
		    	$demo_account = $this->returnVector($sql);
		    	$UserID = trim($demo_account[0]);
		    }
		//}
		
		$UserDetail = $this->getUserInfo($UserName);		
		$UserEmail = $UserDetail['UserEmail'];		
	
		if($UserID) {
			if ($fromEClassApp) {
				// skip account license checking to improve performance
				$licenseValid = true;
			}
			else {
				$sql = "SELECT ModuleID from INTRANET_MODULE WHERE code = '".$module_code['ischoolbag']."'";
				//echo $sql;
				$rs = $this->returnVector($sql);
				if($rs[0] != ''){
					$ModuleID = $rs[0];				
					$sql = "SELECT ModuleStudentID FROM INTRANET_MODULE_USER WHERE ModuleID='".$ModuleID."' AND UserID='".$UserID."'";
					$rs = $this->returnVector($sql);							
					if($rs[0] != ""){
						$licenseValid = true;							
					}
					else{
						$licenseValid = false;
					}								
				}
				else{
					$licenseValid = false;
				}
			}			

			if($byPassLicenseCheck or $licenseValid){
				if($HashPassword!==''){
					//$SessionKey = $this->GetSessionKey($UserName, $Password, $HashPassword);
					$SessionKey = $this->GetSessionKey($UserDetail['UserLogin'], $Password, $HashPassword);
				}
				else{
					//$SessionKey = $this->GetSessionKey($UserName, $Password);
					$SessionKey = $this->GetSessionKey($UserDetail['UserLogin'], $Password);
				}
				//$ReturnContent = array('SessionID'=>$SessionKey, 'UserEmail'=>$UserEmail, 'UserID'=>$UserID);
	
				$ReturnContent['SessionID'] = $SessionKey;
				$ReturnContent['UserEmail'] = $UserEmail;
				$ReturnContent['UserID'] = $UserID;
				//2014-0910-1713-58200
//				$ReturnContent['ChineseName'] = $UserDetail['ChineseName'];
//				$ReturnContent['EnglishName'] = $UserDetail['EnglishName'];
				$ReturnContent['ChineseName'] = ($UserDetail['ChineseName'])? $UserDetail['ChineseName'] : '';
				$ReturnContent['EnglishName'] = ($UserDetail['EnglishName'])? $UserDetail['EnglishName'] : '';
				
				## 2011-07-14: add UserType
				if($UserDetail['RecordType'] == 1){
					$ReturnContent['UserType'] = 'T';
				}
				elseif($UserDetail['RecordType'] == 2){
					$ReturnContent['UserType'] = 'S';
				}	 	
				elseif($UserDetail['RecordType'] == 3){
					$ReturnContent['UserType'] = 'P';
				}
				
				$ReturnContent['PersonalPhotoLink'] = is_file($intranet_root.$UserDetail['PersonalPhotoLink'])? $UserDetail['PersonalPhotoLink']:"";
				
				$ReturnContent['SchoolCode'] = $config_school_code;
				$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
				$school_name = $school_data[0];
				$school_org = $school_data[1];
				$ReturnContent['SchoolName'] = htmlspecialchars($school_name);
				$ReturnContent['SchoolOrg'] = htmlspecialchars($school_org);
				
				if ($fromEClassApp) {
					if ($sys_custom['demo_site_account_mapping']) {
						// maintain original username => don't use email to login in demo site
						$ReturnContent['UserLogin'] = $originalUserName;
						
						// add dummy sessionId
						$ReturnContent['SessionID'] = 'ff9d4b6a5f32e6744474f51b13ce6a48';
					}
					else {
						// user may use email to login app => return UserLogin in database for the app
						$ReturnContent['UserLogin'] = ($UserDetail['UserLogin'])? $UserDetail['UserLogin'] : $UserName;
					}
					
					// [2017-1121-1046-19206] Handle HTML Entity display in IOS
					$ReturnContent['ChineseName'] = ($ReturnContent['ChineseName'])? html_entity_decode($ReturnContent['ChineseName']) : '';
					
					// X148395 - remove control character in the user name
					include_once($intranet_root."/includes/eClassApp/libeClassApp.php");
					$leClassApp = new libeClassApp();
					$ReturnContent['ChineseName'] = $leClassApp->standardizePushMessageText($ReturnContent['ChineseName']);
					$ReturnContent['EnglishName'] = $leClassApp->standardizePushMessageText($ReturnContent['EnglishName']);
				}
				else {
					$ReturnContent['UserLogin'] = $UserName;
				}
				
				// 20160309 Roy: get user year class name in Chinese and English
				$YearClassInfo = $this->getUserYearClassInfo($UserID);
				if ($fromEClassApp) {
					$ReturnContent['ClassNameCh'] = ($YearClassInfo['ClassNameCh'])? $YearClassInfo['ClassNameCh'] : '';
					$ReturnContent['ClassNameEn'] = ($YearClassInfo['ClassNameEn'])? $YearClassInfo['ClassNameEn'] : '';
				}
				//for add AM PM group info
				include_once("$intranet_root/includes/form_class_manage.php");
// 				$libYear = new Year();		
// 				$YearClassGroupInfo = $libYear->Get_Class_Group_Info($YearClassInfo['YearClassID']);
				$libYearClass = new year_class();
				$YearClassGroupInfo = $libYearClass->Get_Class_Group_Info($YearClassInfo['YearClassID']);
				$ReturnContent['ClassAttendanceType'] = $YearClassGroupInfo['AttendanceType'];//1 am, 2 pm, 3 whole day

//				$ReturnContent['ClassLevel']  = $UserDetail['ClassLevel'];
//				$ReturnContent['ClassName']   = $UserDetail['ClassName'];
//				$ReturnContent['ClassNumber'] = $UserDetail['ClassNumber'];
				$ReturnContent['ClassLevel'] = ($UserDetail['ClassLevel'])? $UserDetail['ClassLevel'] : '';
				$ReturnContent['ClassName'] = ($UserDetail['ClassName'])? $UserDetail['ClassName'] : '';
				$ReturnContent['ClassNumber'] = ($UserDetail['ClassNumber'])? $UserDetail['ClassNumber'] : '';
				
				$ReturnContent['TitleEnglish'] = ($UserDetail['TitleEnglish'])? $UserDetail['TitleEnglish'] : '';
				$ReturnContent['TitleChinese'] = ($UserDetail['TitleChinese'])? $UserDetail['TitleChinese'] : '';
				
				if($sys_custom['DHL']){
					include_once("$intranet_root/includes/DHL/libdhl.php");			
					$libdhl = new libdhl();		
					$userOrganizationInfo = $libdhl->getUserRecords(array('user_id'=>$UserID,'GetOrganizationInfo'=>1,'GetOrganizationField'=>1));
					$ReturnContent['TitleEnglish'] = ($userOrganizationInfo[0]["DivisionNameEng"])? $userOrganizationInfo[0]["DivisionNameEng"] : '';
					$ReturnContent['TitleChinese'] = ($userOrganizationInfo[0]["DivisionNameChi"])? $userOrganizationInfo[0]["DivisionNameChi"] : '';	
					$ReturnContent['ChineseName'] = $ReturnContent['EnglishName'];
					$ReturnContent['UserEmail'] = ($UserDetail['UserEmail'])? $UserDetail['UserEmail'] : '';
				}
				
			}
			else{
				$ReturnContent = '402';
			}		
			//debug_r($row);	
		}else{
			$ReturnContent = '401';			
		}
		
		return $ReturnContent;		 
	}
	
	function WS_Login_Centennial($UserName, $Password='', $hasAPIKey=false, $fromEClassApp=false, $HashPassword='', $fromQRCodeLogin=false) {
		
		global $intranet_password_salt;
		$HashPassword = md5($UserName.$intranet_password_salt);
		return $this->WS_Login($UserName, $Password='', $hasAPIKey, $fromEClassApp, $HashPassword, $fromQRCodeLogin);
	}
	
	function WS_QRCodeLogin($parCourseID, $parLessonID, $parSessionID, $parUserID, $hasAPIKey=false){
		global $intranet_root, $intranet_db, $eclass_db, $intranet_authentication_method, $UseLDAP;
		
		$sql = "SELECT
					iu.UserLogin, uc.user_course_id
				FROM
					".$intranet_db.".INTRANET_USER AS iu INNER JOIN
					".$eclass_db.".user_course AS uc on uc.course_id = '$parCourseID' AND iu.UserEmail = uc.user_email
				WHERE
					iu.UserID = '$parUserID'";
		list($UserName, $parUserCourseID) = current($this->returnArray($sql));
		
		$ID = $this->validate_PowerLessonAppQRCodeLogin($parCourseID, $parLessonID, $parSessionID, $parUserCourseID);
		
		if($ID){
			if((isset($UseLDAP) && $UseLDAP) || $intranet_authentication_method=='LDAP'){
				include_once("$intranet_root/includes/libpwm.php");
				$libpwm = new libpwm();
				$passwordAry = $libpwm->getData($ID);
				
				$Password 	  = $passwordAry[$ID];
				$HashPassword = '';
			}
			else{
				$UserInfo 	  = $this->getUserInfo($UserName);
				$Password 	  = $intranet_authentication_method=="HASH"? '':$UserInfo['UserPassword'];
				$HashPassword = $intranet_authentication_method=="HASH"? $UserInfo['HashedPass']:'';
			}
			
			$ReturnContent = $this->WS_Login($UserName, $Password, $hasAPIKey, false, $HashPassword, true);
		}
		else{
			$ReturnContent = '403';
		}
		
		return $ReturnContent;
	}
	
	# 2011-07-29: webservice logout
	function WS_Logout($UserID){
		$UserID = (int)$UserID;
		
		$sql = "UPDATE INTRANET_USER SET SessionKey = '' WHERE UserID = '$UserID'";
		$this->db_db_query($sql);
		
		$ReturnContent = array();
		$ReturnContent['Logout'] = 1;
		
        return $ReturnContent;
	}
	
	function GetSessionKey($UserName, $Password, $HashPassword='') {
		global $intranet_authentication_method, $intranet_password_salt;
		
		//if(strtolower($UserName)=='broadlearning' || strtolower($UserName)=="btw"){
		if(strtolower($UserName)=='broadlearning' ){
			$sql = "SELECT SessionKey FROM INTRANET_USER WHERE UserLogin = '$UserName'";
		}
		else if ($intranet_authentication_method == "HASH")
		{
			# handle to use real login ID in order to matach hashed value
			$sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserLogin='".$UserName."'";
			$row = $this->returnVector($sql);
			if ($row[0]!="")
			{
				$user = $row[0];
			}
			
			if($HashPassword!==''){
				$sql = "SELECT SessionKey FROM INTRANET_USER WHERE UserLogin = '".$UserName."' AND HashedPass = '".$HashPassword."'";
			}
			else{
				$Password = intranet_undo_htmlspecialchars($Password);
	            $userLower = strtolower($user);
				
				$sql = "SELECT
							SessionKey
						FROM
							INTRANET_USER
						WHERE
							UserLogin = '$user' AND
							(
								MD5('$user$Password$intranet_password_salt') = HashedPass OR
								MD5('$userLower$Password$intranet_password_salt') = HashedPass
							)";
			}
		}
		else{
			$sql = "SELECT SessionKey FROM INTRANET_USER WHERE UserLogin = '".$UserName."' AND UserPassword = '".$Password."' ";
		}
		
		$result = $this->returnArray($sql);
		if(count($result) > 0) {
			$SessionKey = $result[0][0];	
		}
		
		return $SessionKey;
	}
	
	function GetUserIDFromSessionKey($SessionKey) {		
        global $session_expiry_time;
        $limit = $session_expiry_time * 60;
        
		$sql = "SELECT UserID FROM INTRANET_USER WHERE SessionKey = '".$SessionKey."' AND UNIX_TIMESTAMP(SessionLastUpdated)+$limit>=UNIX_TIMESTAMP(now()) ";
		$result = $this->returnArray($sql);
		if(count($result) > 0) {
			$tUserID = $result[0][0];	
		}
		
		return $tUserID;		
	}
	
	function UpdateEncryptedPassword($ParamUserID, $ParamPassword){
		/*
		global $intranet_password_salt;
		
		$EncryptedPass = AES_128_Encrypt($ParamPassword,$intranet_password_salt);
		$sql = "INSERT INTO INTRANET_USER_PERSONAL_SETTINGS(UserID,EncPassword,EncPassword_DateModified) 
				VALUES ('".$ParamUserID."','".$EncryptedPass."',NOW())
				 ON DUPLICATE KEY UPDATE EncPassword = '".$EncryptedPass."', EncPassword_DateModified = NOW()";
		return $this->db_db_query($sql);
		*/
		global $intranet_root, $file_path, $eclass_db, $plugin;
		include_once("libpwm.php");
		
		$libpwm = new libpwm();
		$ary = array();
		$ary[$ParamUserID] = $ParamPassword;
		
		if($plugin['radius_server'])
		{
			global $radius_teacher_nas_id, $radius_student_nas_id, $radius_parent_nas_id, $radius_alumni_nas_id;
			$recordTypeToNasId = array(USERTYPE_STAFF=>$radius_teacher_nas_id, USERTYPE_STUDENT=>$radius_student_nas_id, USERTYPE_PARENT=>$radius_parent_nas_id, USERTYPE_ALUMNI=>$radius_alumni_nas_id);
			$sql = "SELECT UserLogin,RecordType FROM INTRANET_USER WHERE UserID='$ParamUserID'";
			$record = $this->returnResultSet($sql);
			if(count($record)>0)
			{
				$userlogin = $record[0]['UserLogin'];
				$record_type = $record[0]['RecordType'];
				$site_id = $recordTypeToNasId[$record_type];
				$this->updateRadiusServerPassword($userlogin,$ParamPassword,$site_id);
			}
		}
		
		return $libpwm->setData($ary);
	}
	
	function GetUserDecryptedPassword($ParamUserID){		
		/*
		$sql = "SELECT EncPassword FROM INTRANET_USER_PERSONAL_SETTINGS WHERE UserID='".$ParamUserID."' ";
		$row = $this->returnVector($sql);
		
		return GetDecryptedPassword($row[0]);
		*/
		global $intranet_root, $file_path, $eclass_db;
		include_once("libpwm.php");
		$libpwm = new libpwm();
		$ary = array($ParamUserID);
		$rs = $libpwm->getData($ary);
		return $rs[$ParamUserID];
	}
	
	function CreateResetPasswordKey($login) 
	{ 
		$salt = intranet_random_passwd(libauth::RESET_PASSWD_SALT_SIZE);
		
		$info[] = time();
		$info[] = $login;
		$infoStr = serialize($info);
	
		$str = $this->Encrypt($infoStr, $salt).$salt; 
		
		$key = md5(libauth::RESET_PASSWD_ENCRYPT_KEY);
		
	    return $this->Encrypt($str, $key);
	} 	

	function GetInfoFromResetKey($resetKey)
	{
		$key = md5(libauth::RESET_PASSWD_ENCRYPT_KEY);
		
		$saltedstr = $this->Decrypt($resetKey, $key);
		$str = substr($saltedstr, 0, libauth::RESET_PASSWD_SALT_SIZE*-1);
		$salt = substr($saltedstr, libauth::RESET_PASSWD_SALT_SIZE*-1);
		
		$infoArr = unserialize($this->Decrypt($str, $salt));
		
		return $infoArr;
		
	}
	
	function Encrypt($text, $key) 
	{ 
		if (function_exists('mcrypt_encrypt')) 
	    	return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND)))); 
	    else
	    	return trim(base64_encode($text)); 
	} 
	
	function Decrypt($text, $key) 
	{ 
		if (function_exists('mcrypt_decrypt')) 
	 	   return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND))); 
	    else
	    	return trim(base64_decode($text)); 
	}
	
	function HandleFailure($UserLogin, $UserPassword, $SessionKey, $REMOTE_ADDR)
	{
		global $intranet_root;
		
		$AlertBoundary = $this->LoginAttemptLimit;
		
		if ($AlertBoundary!="" && $AlertBoundary>0)
		{

			# for similar attempt within 1 hour
			$sql = "SELECT UserID, UserLogin FROM INTRANET_USER WHERE UserLogin='$UserLogin' ";
			$result = $this->returnResultSet($sql);
			if ($result[0]['UserID']>0 && $result[0]['UserID']!="")
			{
			
				$sql = "SELECT FailureID, AttemptTotal, AttemptTimeFirst FROM INTRANET_LOGIN_FAILURE_LOG WHERE UserLogin='$UserLogin' AND RemoteAddr='$REMOTE_ADDR' AND UNIX_TIMESTAMP(AttemptTimeFirst)+3600>=UNIX_TIMESTAMP(now())";
				$result = $this->returnResultSet($sql);
				if ($result[0]['FailureID']>0 && $result[0]['FailureID']!="")
				{		
					$FailureID = $result[0]['FailureID'];
					$AttemptTotal = $result[0]['AttemptTotal'];
					$AttemptTimeFirst = $result[0]['AttemptTimeFirst'];
					
					# update
					$sql = "UPDATE INTRANET_LOGIN_FAILURE_LOG set AttemptTimeLast=now(), AttemptTotal = AttemptTotal + 1, UserPassword='{$UserPassword}' WHERE FailureID='{$FailureID}' ";
					$this->db_db_query($sql);
					
					# check if exceed the limit, send email to
					if (($AttemptTotal+1)==$AlertBoundary)
					{
						# email to the user and also system admin. 
						//  EmailAlert
						include_once($intranet_root."/includes/libemail.php");
						include_once($intranet_root."/includes/libsendmail.php");
						$libemail = new libsendmail();
						
						$sql = "SELECT AttemptTotal, AttemptTimeLast FROM INTRANET_LOGIN_FAILURE_LOG WHERE UserLogin='$UserLogin' AND RemoteAddr='$REMOTE_ADDR' AND UNIX_TIMESTAMP(AttemptTimeLast)+3600>=UNIX_TIMESTAMP(now())";
						$result = $this->returnResultSet($sql);
						$AttemptTotal = $result[0]['AttemptTotal'];
						$AttemptTimeLast = $result[0]['AttemptTimeLast'];
						
						$email_contents = "<p>Dear Administrator,</p><p>Please be alerted that <b>{$AttemptTotal} consecutive unsuccessful logons</b> using user account <b>\"{$UserLogin}\"</b> attempted by IP <b>{$REMOTE_ADDR}</b> were record from <b>{$AttemptTimeFirst}</b> to {$AttemptTimeLast}.</p><p><i>eClass</i></p>";
						$email_to_admin = trim(get_webmaster());
						if ($email_to_admin!="")
						{
							$result = $libemail->send_email(get_webmaster(), "Consecutive Unsuccessful Login Attempts to eClass", $email_contents, "eClass.Alert.Mail", "", "");
							
							# update
							$sql = "UPDATE INTRANET_LOGIN_FAILURE_LOG set EmailAlert='".addslashes($email_to_admin)."' WHERE FailureID='{$FailureID}' ";
							$this->db_db_query($sql);
						}
						//debug("send email ".get_webmaster()); die();
					}
				} else
				{
					# insert 
					$sql = "INSERT INTO INTRANET_LOGIN_FAILURE_LOG (UserLogin, UserPassword, SessionKey, RemoteAddr, AttemptTotal, AttemptTimeFirst) ";
					$sql .= " VALUES ('$UserLogin', '{$UserPassword}', '{$SessionKey}', '{$REMOTE_ADDR}', 1, now())";
					$this->db_db_query($sql);
				}
			}
		}
		
		# clean-up
		$sql = "DELETE FROM INTRANET_LOGIN_FAILURE_LOG WHERE AttemptTotal<={$AlertBoundary} AND UNIX_TIMESTAMP(AttemptTimeFirst)+86400<UNIX_TIMESTAMP(now()) ";
		$this->db_db_query($sql);
	}
	
	
	function IsConsecutiveUnsuccessfulLogins($UserLogin, $REMOTE_ADDR)
	{
		$AlertBoundary = $this->LoginAttemptLimit;
		$LoginLockDuration = $this->LoginLockDuration * 60;
		
		/*
		if ($AlertBoundary!="" && $AlertBoundary>0)
		{
			# lock for 0.5 hour if failed attempt exceeds the limit
			$sql = "SELECT FailureID, AttemptTotal, AttemptTimeFirst FROM INTRANET_LOGIN_FAILURE_LOG WHERE UserLogin='$UserLogin' AND RemoteAddr='$REMOTE_ADDR' AND UNIX_TIMESTAMP(AttemptTimeLast)+{$LoginLockDuration}>=UNIX_TIMESTAMP(now()) AND AttemptTotal>$AlertBoundary ";
			$result = $this->returnResultSet($sql);
			$IsLock = ($result[0]['FailureID']>0 && $result[0]['FailureID']!="");
		} else
		{
			$IsLock = false;
		}
		*/
		if ($AlertBoundary>0 && $LoginLockDuration>0)
		{
			# lock for 0.5 hour if failed attempt exceeds the limit
			$sql = "SELECT FailureID, AttemptTotal, AttemptTimeFirst FROM INTRANET_LOGIN_FAILURE_LOG WHERE UserLogin='$UserLogin' AND RemoteAddr='$REMOTE_ADDR' AND UNIX_TIMESTAMP(AttemptTimeLast)+{$LoginLockDuration}>=UNIX_TIMESTAMP(now()) AND AttemptTotal>'$AlertBoundary' ";
			$result = $this->returnResultSet($sql);
			$IsLock = ($result[0]['FailureID']>0 && $result[0]['FailureID']!="");
		} else
		{
			$IsLock = false;
		}
		
		return $IsLock; 
	}
	
	function ChangePassword($UserID,$ParUserLogin,$OldPassword,$NewPassword,$ReNewPassword,$FromResetPassword,$parLang='en'){
	   global $intranet_root, $PATH_WRT_ROOT,$intranet_authentication_method,$plugin,$personalfile_type,$intranet_password_salt,$eclass_version,$eclass_filepath, $SYS_CONFIG,$eclassAppConfig,$Lang,$sys_custom;

	   include_once($PATH_WRT_ROOT."includes/libeclass.php");
	   include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	   include_once($PATH_WRT_ROOT."includes/libuser.php");
	   include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
	   include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
	   include_once($PATH_WRT_ROOT."lang/lang.$parLang.php");
	   
	   	$li = new libdb();
        $lc = new libeclass(); 
		$lu = new libuser($UserID, $ParUserLogin);
		$libeClassApp = new libeClassApp();
		
		$failed = !$lu->RetrieveUserInfoSetting("CanUpdate", "Password", $lu->RecordType);
		if(isset($_SESSION['FORCE_CHANGE_PASSWORD'])){
			$failed = false; // must allow to change password
		}
		if($lu->UserLogin == $OldPassword){ # if old password is same as userlogin, must change it
			$failed = false;
		}
		
		# 2. Old Password is correct or not change password
		if (!$failed)
		{
		     if ($OldPassword != "" || $FromResetPassword)
		     {
		//         if ($OldPassword != $lu->UserPassword && !$FromResetPassword)
		
				 if(!$this->check_password($lu->UserLogin, $OldPassword) && !$FromResetPassword)
		         {
		             $failed = true;
		             $signal = "InvalidPassword";
		         } 
		         else
		         {
		             # 3. New Password and password confirm is the same
		             if ($NewPassword != $ReNewPassword)
		             {
		                 $failed = true;
		                 $signal = "InvalidPassword";
		             }
		         }
		     }
		}else{
			$signal = "UpdateUnsuccess";
		}
        
        if($sys_custom['UseStrongPassword'] && !$failed){
        	$SettingArr = $lu->RetrieveUserInfoSettingInArrary(array('CanUpdatePassword_'.$lu->RecordType,'EnablePasswordPolicy_'.$lu->RecordType));
        	$password_change_disabled = !$SettingArr['CanUpdatePassword_'.$lu->RecordType];
        	$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$lu->RecordType];
			if ($PasswordLength<6)
			{
				# at least 6 by default
				$PasswordLength = 6;
			}
        	$check_password_result = $this->CheckPasswordCriteria($NewPassword,$ParUserLogin,$PasswordLength);
        	if(/*$password_change_disabled || */ !in_array(1,$check_password_result)){
        		$failed = true;
        		$signal = "InvalidPassword";
        	}
        }
        
		$fieldname = "";
		$result = array();
		if (!$failed)
		{
		     $fieldname .= " LastModifiedPwd = NOW() ";
		
		     if ($OldPassword != "" || $FromResetPassword)
		     {
		         $TargetPassword = $NewPassword;
		         if ($intranet_authentication_method == "HASH")
		         {
		             //$fieldname .= ", HashedPass = '".MD5(strtolower($lu->UserLogin).$TargetPassword.$intranet_password_salt)."'";
		             $fieldname .= ", HashedPass = '".MD5($lu->UserLogin.$TargetPassword.$intranet_password_salt)."'";
		             $fieldname .= ", UserPassword = NULL ";
		         }
		         else
		         {
		             $fieldname .= ", UserPassword = NULL ";
		         }
		         $_SESSION['eclass_session_password'] = $TargetPassword;
				
		     	 $this->UpdateEncryptedPassword($lu->UserID, $TargetPassword);
				
		         # Webmail
		         $lwebmail = new libwebmail();
		         if($lwebmail->has_webmail && !$plugin['imail_gamma']){
		         	$result['webmail_changepassword'] = $lwebmail->change_password($lu->UserLogin,$TargetPassword,"iMail");
		         }
		        # iMail Gamma 
		        if($plugin['imail_gamma'] && $lu->ImapUserEmail != "" /* && ($_SESSION['SSV_EMAIL_LOGIN']!=''&&$_SESSION['SSV_EMAIL_PASSWORD']!=''&&$_SESSION['SSV_LOGIN_EMAIL']!='')*/)
				{
					$IMap = new imap_gamma(true);
		         	$imap_result = $IMap->change_password($lu->ImapUserEmail,$TargetPassword);
		         	if($imap_result)
		         		$_SESSION['SSV_EMAIL_PASSWORD'] = $TargetPassword;
		         	$result['gamma_changepassword'] = $imap_result;
		        }
		        
		         # FTP management
		         if ($plugin['personalfile'])
		         {
		             if ($personalfile_type == 'FTP' || $personalfile_type == 'LOCAL_FTP' || $personalfile_type == 'REMOTE_FTP')
		             {
		                 include_once($PATH_WRT_ROOT."includes/libftp.php");
		                 $lftp = new libftp();
		                 $result['ftp_changepassword'] = $lftp->changePassword($lu->UserLogin,$NewPassword,"iFolder");
		             }
		         }
		
		         if ($intranet_authentication_method == 'LDAP')
		         {
		             include_once($PATH_WRT_ROOT."includes/libldap.php");
		             $lldap = new libldap();
		             if ($lldap->isPasswordChangeNeeded())
		             {
		//                 $result['ldap_changepassword'] = $lldap->changePassword($lu->UserLogin,$OldPassword,$NewPassword);
							$result['ldap_changepassword'] = $lldap->changePassword($lu->UserLogin,$NewPassword);
		             }
		         }
		     }
		     else
		     {
		         $TargetPassword = "";
		     }
		
		     $sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = ".$lu->UserID;
		     $result['sql_update'] = $li->db_db_query($sql) or die ($sql.mysql_error());
		     
		     $newmail = "";
		     
		     if (!in_array(false,$result))
		     {
		         $lc->eClassUserUpdateInfoIP($lu->UserEmail, $lu->Title, $lu->EnglishName,$lu->ChineseName,$lu->FirstName,$lu->LastName, $lu->NickName, $TargetPassword, $lu->ClassName,$lu->ClassNumber, $newmail,$lu->Gender, $lu->ICQNo,$lu->HomeTelNo,$lu->FaxNo,$lu->DateOfBirth,$lu->Address,$lu->Country,$lu->URL,$lu->Info);
		         $signal = "UpdateSuccess";
		         
		         # Send A Push Notification
		         
		         $app_type = '';
                 switch ($lu->RecordType) {
                     case 1:
                         $app_type = $eclassAppConfig['appType']['Teacher'];
                         break;
                     case 2:
                         $app_type = $eclassAppConfig['appType']['Student'];
                         break;
                     case 3:
                         $app_type = $eclassAppConfig['appType']['Parent'];
                         break;
                 }

                 if($app_type) {
                     if($app_type == $eclassAppConfig['appType']['Parent']) {
                         $individualMessageInfoAry[0]['relatedUserIdAssoAry'][$UserID] = $lu->getChildren();
                     }else{
                         $individualMessageInfoAry[0]['relatedUserIdAssoAry'][$UserID] = array( $lu->UserID );
                     }
//			         $pushmessage_content = $Lang['AppNotifyMessage']['ChangePassword']['Content'][0].date("Y-m-d H:i").$Lang['AppNotifyMessage']['ChangePassword']['Content'][1];
			         $pushmessage_content = $Lang['AppNotifyMessage']['ChangePassword']['Content'];
			         $pushmessage_content = str_replace("<!--DateTime-->",date("Y-m-d H:i"),$pushmessage_content);
			         $libeClassApp->sendPushMessage($individualMessageInfoAry, $Lang['AppNotifyMessage']['ChangePassword']['Title'], $pushmessage_content, '', $recordStatus=0, $app_type,'', '', '', '',$eclassAppConfig['pushMessage']['fromModule']['changePwd']);
                 }

//                  if($lu->RecordType == 3) {

//                      $individualMessageInfoAry[0]['relatedUserIdAssoAry'][$UserID] = $lu->getChildren();
// //			         $pushmessage_content = $Lang['AppNotifyMessage']['ChangePassword']['Content'][0].date("Y-m-d H:i").$Lang['AppNotifyMessage']['ChangePassword']['Content'][1];
//                      $pushmessage_content = $Lang['AppNotifyMessage']['ChangePassword']['Content'];
//                      $pushmessage_content = str_replace("<!--DateTime-->",date("Y-m-d H:i"),$pushmessage_content);
//                      $libeClassApp->sendPushMessage($individualMessageInfoAry, $Lang['AppNotifyMessage']['ChangePassword']['Title'], $pushmessage_content, '', $recordStatus=0, $eclassAppConfig['appType']['Parent'],'', '', '', '',$eclassAppConfig['pushMessage']['fromModule']['changePwd']);
//                  }

		         # Send Ack to admin
		         if($FromResetPassword)
		         {
		         	include_once($PATH_WRT_ROOT."includes/libemail.php");
					include_once($PATH_WRT_ROOT."includes/libsendmail.php");
				    $lsendmail = new libsendmail();
		         	
				    $webmaster = get_webmaster();
		
				    # Send ACK Email
					list($ackMailSubject,$ackMailBody) = $lu->returnEmailNotificationData_HashedPw_ACK($lu->UserEmail, $lu->UserLogin);
					if($webmaster != "" && intranet_validateEmail($webmaster,true)){
						$result2 = $lsendmail->send_email($webmaster, $ackMailSubject,$ackMailBody,"");
					}
		         }
		     }
		     else
		     {
		         $signal = "UpdateUnsuccess";
		     }
		}		
		
		return  $signal ;
	}
	
	function updateRadiusServerPassword($username,$password,$site_id)
	{
		global $intranet_db,$intranet_db_user, $intranet_db_pass, $sys_custom, $plugin, $radius_db_host, $radius_db_name, $radius_db_username, $radius_db_password;
		
		if(!$plugin['radius_server']) return false;
		// Get NT Hash value
		$result = shell_exec(OsCommandSafe("smbencrypt ".'$'."'".str_replace("'",'\\\'',$password)."'"));
		$nt_hash = trim(substr($result, strrpos($result,"\t")));
		
		intranet_closedb();// close the intranet db
		$this->opendb($radius_db_host,$radius_db_username,$radius_db_password,$radius_db_name); // open the radius server db
		
		$sql = "SELECT COUNT(1) FROM ".$radius_db_name.".radcheck WHERE username='$username'";
		$record = $this->returnVector($sql);
		
		if($record[0]==0){
			$sql = "INSERT INTO ".$radius_db_name.".radcheck (username,attribute,op,value,site_id) VALUES ('$username','NT-Password',':=','$nt_hash','$site_id')";
			$success = $this->db_db_query($sql);
		}else{
			$sql = "UPDATE ".$radius_db_name.".radcheck SET attribute='NT-Password',op=':=',value='$nt_hash',site_id='$site_id' WHERE username='$username'";
			$success = $this->db_db_query($sql);
		}
		$this->closedb(); // close the radius server db
		intranet_opendb(); // reopen the intranet db
		$this->db = $intranet_db;
		
		return $success;
	}
	
	function CheckPasswordCriteria($password,$userlogin,$min_length=-1)
	{
		// forbidden characters
		$banned_chars = array('"','\'', ' ', '$', '&', '<', '>', '+', '\\');
		// top 500 bad passwords
		$bad_passwords = array($userlogin,"123","abc",
							"123456","password","12345678","1234","pussy","12345","dragon","qwerty","696969","mustang",
							"letmein","baseball","master","michael","football","shadow","monkey","abc123","pass","fuckme",
							"6969","jordan","harley","ranger","iwantu","jennifer","hunter","fuck","2000","test",
							"batman","trustno1","thomas","tigger","robert","access","love","buster","1234567","soccer",
							"hockey","killer","george","sexy","andrew","charlie","superman","asshole","fuckyou","dallas",
							"jessica","panties","pepper","1111","austin","william","daniel","golfer","summer","heather",
							"hammer","yankees","joshua","maggie","biteme","enter","ashley","thunder","cowboy","silver","richard",
							"fucker","orange","merlin","michelle","corvette","bigdog","cheese","matthew","121212","patrick",
							"martin","freedom","ginger","blowjob","nicole","sparky","yellow","camaro","secret","dick","falcon",
							"taylor","111111","131313","123123","bitch","hello","scooter","please",
							"porsche","guitar","chelsea","black","diamond","nascar","jackson","cameron","654321","computer",
							"amanda","wizard","xxxxxxxx","money","phoenix","mickey","bailey","knight","iceman","tigers",
							"purple","andrea","horny","dakota","aaaaaa","player","sunshine","morgan","starwars","boomer",
							"cowboys","edward","charles","girls","booboo","coffee","xxxxxx","bulldog","ncc1701","rabbit",
							"peanut","john","johnny","gandalf","spanky","winter","brandy","compaq","carlos","tennis","james",
							"mike","brandon","fender","anthony","blowme","ferrari","cookie","chicken","maverick","chicago",
							"joseph","diablo","sexsex","hardcore","666666","willie","welcome","chris","panther","yamaha",
							"justin","banana","driver","marine","angels","fishing","david","maddog","hooters","wilson",
							"butthead","dennis","fucking","captain","bigdick","chester","smokey","xavier","steven","viking",
							"snoopy","blue","eagles","winner","samantha","house","miller","flower","jack",
							"firebird","butter","united","turtle","steelers","tiffany","zxcvbn","tomcat","golf","bond007",
							"bear","tiger","doctor","gateway","gators","angel","junior","thx1138","porno","badboy","debbie",
							"spider","melissa","booger","1212","flyers","fish","porn","matrix","teens","scooby","jason","walter",
							"cumshot","boston","braves","yankee","lover","barney","victor","tucker","princess","mercedes",
							"5150","doggie","zzzzzz","gunner","horney","bubba","2112","fred","johnson","xxxxx","tits","member",
							"boobs","donald","bigdaddy","bronco","penis","voyager","rangers","birdie","trouble","white","topgun",
							"bigtits","bitches","green","super","qazwsx","magic","lakers","rachel","slayer","scott","2222","asdf",
							"video","london","7777","marlboro","srinivas","internet","action","carter","jasper","monster","teresa",
							"jeremy","11111111","bill","crystal","peter","pussies","cock","beer","rocket","theman","oliver",
							"prince","beach","amateur","7777777","muffin","redsox","star","testing","shannon","murphy","frank",
							"hannah","dave","eagle1","11111","mother","nathan","raiders","steve","forever","angela","viper","ou812",
							"jake","lovers","suckit","gregory","buddy","whatever","young","nicholas","lucky","helpme","jackie","monica",
							"midnight","college","baby","cunt","brian","mark","startrek","sierra","leather","232323","4444","beavis",
							"bigcock","happy","sophie","ladies","naughty","giants","booty","blonde","fucked","golden","0","fire",
							"sandra","pookie","packers","einstein","dolphins","0","chevy","winston","warrior","sammy","slut","8675309",
							"zxcvbnm","nipples","power","victoria","asdfgh","vagina","toyota","travis","hotdog","paris","rock","xxxx",
							"extreme","redskins","erotic","dirty","ford","freddy","arsenal","access14","wolf","nipple","iloveyou",
							"alex","florida","eric","legend","movie","success",
							"rosebud","jaguar","great","cool","cooper","1313","scorpio","mountain","madison","987654","brazil",
							"lauren","japan","naked","squirt","stars","apple","alexis","aaaa","bonnie","peaches","jasmine","kevin",
							"matt","qwertyui","danielle","beaver","4321","4128","runner","swimming","dolphin","gordon","casper","stupid",
							"shit","saturn","gemini","apples","august","3333","canada","blazer","cumming","hunting","kitty","rainbow",
							"112233","arthur","cream","calvin","shaved","surfer","samson","kelly","paul","mine","king","racing","5555",
							"eagle","hentai","newyork","little","redwings","smith","sticky","cocacola","animal","broncos","private","skippy",
							"marvin","blondes","enjoy","girl","apollo","parker","qwert","time","sydney","women","voodoo","magnum","juice",
							"abgrtyu","777777","dreams","maxwell","music","rush2112","russia","scorpion","rebecca","tester","mistress",
							"phantom","billy","6666","albert");
		
		$result = array();
		if($min_length != -1 && $min_length > 0){
			$password_length = strlen($password);
			if($password_length < $min_length){
				$result[] = -1; // Password does not fulfil minimum required length.
			}
		}
		
		foreach($banned_chars as $char){
			if(strpos($password,$char) !== false){
				$result[] = -2; // Password contains one or more of the restricted characters " ' space $ & < > + \.
				break;
			}
		}
		
		if(in_array($password,$bad_passwords)){
			$result[] = -3; // Password is too simple.
		}
		
		if(!preg_match('/^.*[a-zA-Z].*$/',$password) || !preg_match('/^.*\d.*$/',$password)){
			$result[] = -4; // Password should contains both alphabets and digits.
		}
		
		if(count($result)==0){
			$result[] = 1; // Valid and OK
		}
		
		return $result;
	}

	function CheckAppLoginSession($SessionID) {
		$sql = "SELECT count(*) as count FROM APP_LOGIN_SESSION WHERE SessionID = '".$SessionID."' AND RecordStatus=1";
		$result = $this->returnArray($sql);
		if(count($result) > 0) {
			if($result[0][0] >= 1) {
				return true;
			}
		}
		return false;
	}
}
?>