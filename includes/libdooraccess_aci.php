<?php 
/**
 * ACI class file.
 * Diagram: 
 * <br>
 * <img src="http://backup.broadlearning.com/aci.jpg">
 * <br>
 * @author Bob Pang <bobpang@broadlearning.com>
 * @version v0.10
 * @link http://www.eclass.com.hk/
 * @copyright Copyright &copy; 2001-2012 BroadLearning Education (Asia) Limited
 */
 class libdooraccess_aci {
    /**
    * @var boolean Checks to see if the connection is active
    */
    var $con = false;
    
    /**
     * @var string the IP of the ACI pc
     */
    var $ip;

    /**
     * @var integer port number of appServ web server in ACI pc
     */
    var $port;
    
    /**
     * @var string the api folder path
     */
    var $api_folder;
  
    /**
     * @var string the url to call aci pc's link
     */
    var $aci_url;
    
    /**
     * @var string the security key to access between eclass server and aci pc
     */
    var $key;
    
    /**
    * Constructor to initialization the connection aci url
    * It will use actionCombineAciUrl() method to create an aci url if port and api_folder are available
    * @param string $ip the IP of the ACI pc
    * @param string $port port number of appServ web server in ACI pc
    * @param string $api_folder the api folder path
    * @param string $key the security key to access between eclass server and aci pc
    */  
    public function libdooraccess_aci($ip,$port="",$api_folder="",$key) {
        $this->ip = $ip;
        $this->port = $port;
        $this->api_folder = $api_folder;
        $this->actionCombineAciUrl();
        $this->key = md5($key.date("YmdH"));
    }
    
    /**
    * Combine a full aci url if port or api_folder are available
    */
    public function actionCombineAciUrl() {
        if(!empty($this->port))
        {
            $this->aci_url = $this->ip.":".$this->port."/";
        }
        else
        {
            $this->aci_url = $this->ip."/";
        }
      
        if(!empty($this->api_folder))
        {
            $this->aci_url .= $this->api_folder."/";
        }
    }
    
    /**
    * Curl access the ACI pc's url and return the code number. 
    * @param string $url the ACI pc access url 
    * @param array $post_param the post param in a array.
    * @return integer the return code number
    */
    public function actionCurlPostUrl($url,$post_param) {
        $c = curl_init($url);
        $arr_data = $post_param;
        curl_setopt($c, CURLOPT_POST, 1);
        curl_setopt($c, CURLOPT_POSTFIELDS, $arr_data);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($c);
        curl_close($c);

        return $result;
    }
    
    /**
    * Add security groups for the card id in ACI pc mysql database.
    * <br>
    * Return : 
    * <ul>
    * <li>num(insert id) = Create record succesfully!</li>
    * <li>0 = Create record fail!</li>
    * </ul>
    * @param string $card_id the card number 
    * @param string $group_id the group list to update
    * @param integer $start_time the start time to open the door [yyyymmddhhmmss]
    * @param integer $end_time the end time to close the door [yyyymmddhhmmss]
    * @return integer the ID generated for an AUTO_INCREMENT column by the previous query (usually INSERT). 
    */
    public function actionCreate($card_id,$group_id,$start_time,$end_time) { 
            $create_data['action']="create";
            $create_data['card_id']=$card_id;
            $create_data['group_id']=$group_id;
            $create_data['start_time']=$start_time;
            $create_data['end_time']=$end_time;
            $create_data['key']=$this->key;
            return $this->actionCurlPostUrl($this->aci_url.'index.php',$create_data);
    }
 
    /**
    * Update security groups for the card id in ACI pc's mysql database by $id.
    * <br>
    * Return : 
    * <ul>
    * <li>1 = Update record succesfully!</li>
    * <li>0 = Update record fail!</li>
    * </ul>
    * @param integer $id the ID generated for an AUTO_INCREMENT column by the previous query (usually INSERT).
    * @param string $card_id the card number 
    * @param string $group_id the group list to update
    * @param integer $start_time the start time to open the door [yyyymmddhhmmss]
    * @param integer $end_time the end time to close the door [yyyymmddhhmmss]
    * @return boolean the return update record status
    */
    public function actionUpdate($id,$card_id,$group_id,$start_time,$end_time) {
            $update_data['action']="update";
            $update_data['id']=$id;
            $update_data['card_id']=$card_id;
            $update_data['group_id']=$group_id;
            $update_data['start_time']=$start_time;
            $update_data['end_time']=$end_time;
            $update_data['key']=$this->key;
            return $this->actionCurlPostUrl($this->aci_url.'index.php',$update_data);
    }
    
    /**
    * Delete record in ACI pc's mysql database by $id.
    * <br>
    * Return : 
    * <ul>
    * <li>1 = Delete record succesfully!</li>
    * <li>0 = Delete record fail!</li>
    * </ul>
    * @param integer $id the ID generated for an AUTO_INCREMENT column by the previous query (usually INSERT).
    * @param string $card_id the card number 
    * @return boolean the return delete record status
    */
    public function actionDelete($id,$card_id='') {
            $delete_data['action']="delete";
            $delete_data['id']=$id;
            $delete_data['card_id']=$card_id;
            $delete_data['key']=$this->key;
            return $this->actionCurlPostUrl($this->aci_url.'index.php',$delete_data);
    }
    
} 
?>
