<?php
// Modifing by : 

## Change Log ##
/*
#
# 2013-07-05 [Charles Ma] - update20130705
# Detail	:	eBook Licence

*/

class elibvideo_install extends libdb
{
	
	var $BookPublishers = array();
		
	function elibvideo_install(){		
		$this->libdb();		
	}	
	
	/////////////////////////////////////////
	/////////////////update20130822 Charles Ma - evideo license
	/////////////////////////////////////////
	
	function retrive_evideo_license_html(){
		$sql = " SELECT SettingID, SchoolCode, SchoolName, Package, StartDate, EndDate FROM ELIB_VIDEO_SETTING ";
		$result = $this->returnArray($sql);
		$html = "";
		for($i = 0; $i < count($result) ; $i++){
			$StartDate = $result[$i]["StartDate"] == "0000-00-00 00:00:00" || $result[$i]["StartDate"] == "NULL" ? "--" : date("Y-m-d",strtotime($result[$i]["StartDate"]));
			$EndDate = $result[$i]["EndDate"] == "0000-00-00 00:00:00" || $result[$i]["EndDate"] == "NULL" ? "--" : date("Y-m-d",strtotime($result[$i]["EndDate"]));
			
			$html .= "<tr><td>".$result[$i]['SchoolCode']."</td><td>".$result[$i]['SchoolName']."</td>" ."</td><td>".$result[$i]['Package']."</td>" .
					"<td>".$StartDate."</td><td>".$EndDate."</td>" .
					"<td style=\"color: #ff0058;\"><a class=\"delete_btn\" style=\"cursor: pointer;\" value=\"".$result[$i]['SettingID']."\">X</a></td></tr>";
		}
		return $html;
	}
	
	function install_evideo_license($SchoolCode, $SchoolName, $StartDate, $EndDate, $Package){
		////////////////////// check if schoolcode
		$SchoolName = addslashes($SchoolName);
		$sql = " SELECT SettingID FROM ELIB_VIDEO_SETTING WHERE SchoolCode = $SchoolCode ";
		$SettingID = current($this->returnVector($sql));
		
		if($SettingID == ""){
			$sql = " INSERT INTO ELIB_VIDEO_SETTING (SchoolCode, SchoolName, StartDate, EndDate, Package) " .
					" Values ('$SchoolCode','$SchoolName','$StartDate','$EndDate', '$Package')  ";
			$result = $this->db_db_query($sql);
			if($result) return " Query insertion completed. ";
			else " Query insertion failed ";
		}else{
			$sql = " UPDATE ELIB_VIDEO_SETTING Set StartDate = '$StartDate' , EndDate = '$EndDate', Package = '$Package' Where SchoolCode = '$SchoolCode'  ";
			$result = $this->db_db_query($sql);
			if($result) return " Query update completed. ";
			else " Query update failed ";
		}
		return " Unexpected END ";
	}
	
	function unstall_evideo_license($QuotationID){
		if($QuotationID){
			$sql = " DELETE FROM ELIB_VIDEO_SETTING WHERE SettingID = '$QuotationID'  ";
			$result = $this->db_db_query($sql);
		}
	}
	
	/////////////////////////////////////////
	/////////////////update20130822 END
	/////////////////////////////////////////
}

?>