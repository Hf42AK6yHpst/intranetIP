<?php
## Using By : 
if (!defined("LIBFTP_DEFINED"))                     // Preprocessor directive
{
define("LIBFTP_DEFINED", true);

include_once("libosaccount.php");
 function parsenext ($s)
        {
                 $k = strpos ($s, " ");
                 $m = array();
                 $m[0] = trim(substr($s, 0, $k));
                 $m[1] = trim(substr($s, $k+1));
                 return $m;
        }


# Module for FTP functions for personal file
# Handle cases for both local and remote
class libftp extends libosaccount {

        var $host;
        var $port;
        var $isAccountManageable;
        var $private_dir;
        var $public_dir;
        var $upload_temp;
        var $download_temp;
        var $host_type;
        var $isFTP;
        var $control_url;
        var $control_param;

        var $connection;        // Connection to the FTP server.

        function libftp()
        {
               $this->libosaccount();
                global $personalfile_ftp_host,$personalfile_ftp_port,$personalfile_account_management,$personalfile_type;
                global $personalfile_ftp_private_dir,$personalfile_ftp_public_dir,$personalfile_ftp_upload_temp,$personalfile_ftp_download_temp;
                global $personalfile_api_port, $personalfile_actype;
                $this->host = $personalfile_ftp_host;
                $this->setRemoteAPIHost($this->host,$personalfile_api_port);
                $this->port = ($personalfile_ftp_port==""? 21 :intval($personalfile_ftp_port));
                $this->isAccountManageable = $personalfile_account_management;
                $this->private_dir = ($personalfile_ftp_private_dir==""? "~" :$personalfile_ftp_private_dir);
                $this->public_dir = ($personalfile_ftp_public_dir==""? "public_html" :$personalfile_ftp_public_dir);
                $this->upload_temp = ($personalfile_ftp_upload_temp==""? "/tmp" :$personalfile_ftp_upload_temp);
                $this->download_temp = ($personalfile_ftp_download_temp==""? "/tmp" :$personalfile_ftp_download_temp);
                if ($personalfile_type == 'FTP')
                {
                     $this->isFTP = true;
                     global $personalfile_ftp_control_url,$personalfile_ftp_control_param;
                     $this->control_url = $personalfile_ftp_control_url;
                     $this->control_param = $personalfile_ftp_control_param;
                     $this->actype = $personalfile_actype;
                }
                else
                {
                    $this->isFTP = false;
                }
        }

        // Attempts to connect to the FTP server.
        // Returns a FTP stream on success or FALSE on error.
        // The connection to the FTP server will also be reset on error.
        function connect($ftp_username,$ftp_passwd)
        {
                # Fill code of Connect FTP Server to $this->host , $this-port
                # return connection successful or not

                // Connects to the FTP server
                $this->connection = @ftp_connect($this->host, $this->port);
                if (!$this->connection) return false;
				
                // Login with username and password
                $ftp_username = strtolower($ftp_username);
                $login_result = @ftp_login($this->connection, $ftp_username, $ftp_passwd);             
                global $personalfile_ftp_pasv_mode;
                if ($personalfile_ftp_pasv_mode)
                {
                    @ftp_pasv($this->connection, $personalfile_ftp_pasv_mode);
                }
                // Verify connection
                if ($this->connection && $login_result)
                {
                        return $this->connection;
                }
                else
                {
                        $this->close();
                        return false;
                }

                return false;
        }

        // Close a FTP connection. This function always return true no matter the
        // operation is succeed or not.
        function close()
        {
                # Fill code of close the connection
                #ftp_close($this->connection);
                @ftp_quit($this->connection);

                return true;
        }

        // Attempt to create an user account.
        // Return TRUE on succeed and FALSE on error.
        function open_account($UserLogin,$UserPassword)
        {
                # Fill code of create new account
                if ($this->isAccountManageable)
                {
                    if ($this->isFTP)
                    {
                        # Standard Remote API calling
                        if ($this->openAccount($UserLogin, $UserPassword,"iFolder"))
                        {
                            # Create public_html for placing public web pages
                            if ($this->connect($UserLogin,$UserPassword))
                            {
                                $this->createWebDirectory();
                                $this->close();
                                return true;
                            }
                        }
                    }
                }

                # If can't open account return false
                return false;
        }

        /*
        # Inherit from libosaccount
        function removeAccount
        function changePassword
        function isAccountExist
        function getTotalQuota
        function getQuotaTable
        function getUsedQuota
        function setTotalQuota
        */

        function createWebDirectory()
        {
                 if (!$this->chdir($this->public_dir))
                 {
                      $this->mkdir($this->public_dir);
                 }
                 else
                 {
                     $this->chdir("..");
                 }
                 $this->chmod($this->public_dir,"",1);
        }

        // Attempt to upload the file from the web server to the FTP server
        // Return TRUE on success, FALSE otherwise
        function upload($source_file,$destination_dir,$filename,$isPublic)
        {
                 # Fill code of file upload
                 # Param:
                 # $source_file : path of the source file
                 # $destination_dir : path of the file to be put
                 # $filename : filename of this file
                 # $isPublic : specify is the file is public. This affect the file permissions.
                 #$filename = addslashes($filename);

                 $upload = @ftp_put($this->connection, $destination_dir."/".$filename, $source_file, FTP_BINARY);
                 $this->chmod($destination_dir,$filename,$isPublic);
                 //$chmod = chmod($this->connection, $isPublic, $destination_file);

                 return $upload;
                 //return ($upload && $chmod);
        }

        // Attempt to download the file specified
        // Return TRUE if the file can be outputed to the client browser, FALSE otherwise.
        function download($target_dir,$filename)
        {
                // Download the file. Local filename is renamed to protect privacy.
                // The temporary path should be hidden from web server.
                $temp_filename = rand();

                $get = @ftp_get($this->connection, $this->download_temp."/".$temp_filename, $target_dir."/".$filename, FTP_BINARY);

                if (!$get) return false;
                
                ### Check user's browser ###
				$useragent = $_SERVER['HTTP_USER_AGENT'];
				if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
				    $browser_version=$matched[1];
				    $browser = 'IE';
				} elseif (preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
				    $browser_version=$matched[1];
				    $browser = 'Opera';
				} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
					$browser_version=$matched[1];
					$browser = 'Firefox';
				} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
					$browser_version=$matched[1];
					$browser = 'Safari';
				} else {
					// browser not recognized!
				    $browser_version = 0;
				    $browser = 'other';
				}
                
                $encoded_filename = urlencode($filename);
                $encoded_filename = str_replace("+", "_", $encoded_filename);
                
				// Output the file to user browser
                header("Pragma: public");
                header("Expires: 0"); // set expiration time
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Content-type: application/octet-stream");
                header("Content-Length: ".filesize($this->download_temp."/".$temp_filename));
                //header("Content-Disposition: attachment; filename=\"".$filename."\"");
                if ($browser == "IE") 
				{
					header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
				} 
				else if ($browser == "Firefox")
				{
					header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
				}
				else 
				{
					header('Content-Disposition: attachment; filename="' . $filename . '"');
				
				}
                readfile($this->download_temp."/".$temp_filename);

                # Remove file
                unlink($this->download_temp."/".$temp_filename);

                return true;
        }

        function download2variable($target_dir,$filename)
        {
                // Download the file. Local filename is renamed to protect privacy.
                // The temporary path should be hidden from web server.
                $temp_filename = rand();
                $get = @ftp_get($this->connection, $this->download_temp."/".$temp_filename, $target_dir."/".$filename, FTP_BINARY);

                if (!$get) return false;

                // Output the file to user browser
                $content = get_file_content($this->download_temp."/".$temp_filename);

                # Remove file
                unlink($this->download_temp."/".$temp_filename);

                return $content;
        }

function ftp_rmAll($dst_dir){
    $ar_files = @ftp_nlist($this->connection, $dst_dir);
    if (is_array($ar_files)){ // makes sure there are files
        for ($i=0;$i<sizeof($ar_files);$i++){ // for each file
        	$st_file = $ar_files[$i];
        	if((substr($st_file,-3) != "/..") && (substr($st_file,-2) != "/."))
        	{
	            #$full_path = $dst_dir . "/" . $st_file;
	            $full_path = $st_file;
	            if ($this->host_type == "Windows")
	            {
	                # Trim part after \
	                $slash_pos = strrpos($full_path, "\\");
	                if ($slash_pos === false )
	                {
	                    # nthg to do
	                }
	                else
	                {
	                    $full_path = substr($full_path,0,$slash_pos);
	                }
	            }
	
	            #echo "Full path: $full_path <br>";
	            if (@ftp_size($this->connection, $full_path) == -1){ // check if it is a directory
	                $this->ftp_rmAll($full_path); // if so, use recursion
	            } else {
	                @ftp_delete($this->connection, $full_path); // if not, delete the file
	            }
	        }
        }
    }
    @ftp_rmdir($this->connection, $dst_dir); // delete empty directories
}

        // Attempt to remove the file specified
        // Returns 0 if fails
        // 1 if file removed, 2 if dir removed
        function remove($target_dir,$filename)
        {
                # Fill code of file removal
                # Param:
                # $target_dir : path of the file to be removed
                # $filename : filename of target file
                //ftp_delete($this->connection, $target_dir."/".$filename);
                $this->ftp_rmAll($target_dir."/".$filename);
        }

        // Attempt to rename the file in the specified directory
        // Returns TRUE on success or FALSE on failure.
        function rename($target_dir,$old_filename,$new_filename)
        {
                # Fill code of file rename
                # Param:
                # $target_dir : path of the file
                # $old_filename : filename of the file to be renamed
                # $new_filename : new filename
                return @ftp_rename($this->connection, $target_dir."/".$old_filename, $target_dir."/".$new_filename);
        }

        // Attempt to move the file speicified
        // Returns TRUE on success or FALSE on failure.
        function move($old_path,$new_path,$filename)
        {
                # Fill code of file rename
                # Param:
                # $old_path : original file path
                # $new_path : new file path
                # $filename : filename to be moved
                $new_path = intranet_undo_htmlspecialchars($new_path);
                return @ftp_rename($this->connection, $old_path."/".$filename, $new_path."/".$filename);
        }

        function parseFileListing($files)
        {
        		$result = array();
                 for($i=0; $i<sizeof($files); $i++)
                 {
                     $str = $files[$i];
                     /*
                     $str = str_replace(" ",";",$str);
                     while (strpos($str,";;")) $str = str_replace(";;",";",$str);
                     $str = explode(";",$str);
                     */
                     unset($file);
                     list ($permissions, $str) = parsenext ($str);
                     list ($number, $str) = parsenext ($str);
                     list ($owner, $str) = parsenext ($str);
                     list ($group, $str) = parsenext ($str);
                     list ($size, $str) = parsenext ($str);
                     list ($timeMonth,$str) = parsenext ($str);
                     list ($timeDay,$str) = parsenext ($str);
                     list ($timeTime,$str) = parsenext ($str);

                     $file["permissions"] = $permissions; #$str[0];
                     $file["number"] = $number; #$str[1];
                     $file["owner"] = $owner; #$str[2];
                     $file["group"] = $group; #$str[3];
                     $file["size"] = $size; #$str[4];
                     $file["timeMonth"] = $timeMonth; #$str[5];
                     $file["timeDay"] = $timeDay; #$str[6];
                     $file["timeTime"] = $timeTime; #$str[7];
                     /*
                     if(strpos($timeTime,":")){
	                     $tmp_hr = substr($timeTime,0,strpos($timeTime,":"));
	                     $tmp_min = substr($timeTime,strpos($timeTime,":")+1);
                     }
                     $file["timeTime"] = date("H:i",mktime($tmp_hr+8,$tmp_min,0,0,0));
                     */

                     #for ($x = 8; $x < sizeof($str); $x++) $file["name"] .= $str[$x];
                     $file["name"] = $str;
                     
                     
                     # for testing
                     /*
                     if (strstr($this->ftp_username, "broadlearningDISABLED"))
                     {
                     	 global $current_dir;
                     	 if ($current_dir!="")
                     	 {
	                     	 $datetime = @ftp_mdtm($this->connection, $current_dir."/".$file["name"]);
	                     	 if ($datetime!=-1)
	                     	 {
		                     	 $fileYear = date("Y", $datetime);
		                     	 $currentYear = date("Y");
		                     	 if ($fileYear==$currentYear)
		                     	 {
		                     	 	$file["timeTime"] = date("G:i", $datetime);
		                     	 } elseif ($fileYear>2000)
		                     	 {
		                     	 	$file["timeTime"] = $fileYear;
		                     	 }
		                     	 $file["timeMonth"] = date("M", $datetime);
		                     	 $file["timeDay"] = date("j", $datetime);
	                     	 }
                     	 }
                     }
                     */
                     
                     
                     $result[] = $file;
                 }
                 if (sizeof($result)>=2)
                 {
                     usort($result,"list_compare_function");
                 }

                 return $result;
        }

        // List the files in the current directory
        function list_file($target_dir)
        {
                # Fill code of file listing
                # Param:
                # $target_dir : path of the directory to be browsed
                # Return:
                # Array of data in the format
                # - Filetype (isFile: 'F' , isDirectory: 'D')
                # - Filesize (in bytes)
                # - Date
                # - Filename

                # Kenneth: I dunno whether there is any more info can be extracted
                # and is there any sorting can be done when listing?
                # or we need to do in php level?

                // "ftp_rawlist" and "ftp_nlist" list the files in a directory.
                // "ftp_rawlist" returns more information (same as 'ls -l') then "ftp_nlist".
                // More control may be available using "ftp_raw" in PHP5.
                global $benchmark;
                $benchmark['start list'] = time();
                $filelist = @ftp_rawlist($this->connection, "$target_dir");
                $benchmark['raw list retrieved'] = time();
                $parsed = $this->parseFileListing($filelist);
                $benchmark['list parsed'] = time();
				$newfilelist = array();
                for ($i=0; $i<sizeof($parsed); $i++)
                {
                		/*
                        // Split the returned file list into array and extract only useful information.
                        $file = preg_split("/[\s,]+/", $filelist[$i], -1);
                        $newfilelist[$i]['Filetype'] = (substr($file[0], 0, 1) == 'd')? 'D':'F';
                        $newfilelist[$i]['Filesize'] = $file[4];
                        $newfilelist[$i]['Date'] = $file[5]." ".$file[6]." ".$file[7];
                        $newfilelist[$i]['Filename'] = $file[8];
                        */
                        $file = $parsed[$i];
                        
                        if((strpos($file["name"],".") > 0) || (strpos($file["name"],".") === false)) 
                        {
							/*
	                        $newfilelist[$i]['Filetype'] = (substr($file["permissions"], 0, 1) == 'd')? 'D':'F';
	                        if ($newfilelist[$i]['Filetype']=='D')
	                        {
	                            $newfilelist[$i]['Filesize'] = "&nbsp;";
	                        }
	                        else
	                        {
	                            $newfilelist[$i]['Filesize'] = $file["size"];
	                        }
	
	                        $newfilelist[$i]['Date'] = $file["timeMonth"]." ".$file["timeDay"]." ".$file["timeTime"];
	                        $newfilelist[$i]['Filename'] = $file["name"];
	                        */
	                        $j++;
	                        $newfilelist[$j-1]['Filetype'] = (substr($file["permissions"], 0, 1) == 'd')? 'D':'F';
	                        if ($newfilelist[$j-1]['Filetype']=='D')
	                        {
	                            $newfilelist[$j-1]['Filesize'] = "&nbsp;";
	                        }
	                        else
	                        {
	                            $newfilelist[$j-1]['Filesize'] = $file["size"];
	                        }
	
	                        $newfilelist[$j-1]['Date'] = $file["timeMonth"]." ".$file["timeDay"]." ".$file["timeTime"];
	                        $newfilelist[$j-1]['Filename'] = $file["name"];
                        }
                }
				
                return $newfilelist;
        }

        function getDirectoryList($target_dir)
        {
                 $rawfilelist = @ftp_rawlist($this->connection, "$target_dir");
                 $filelist = $this->parseFileListing($rawfilelist);
                 if(substr($target_dir,0,2) == "//")
                 	$target_dir = "/..".substr($target_dir,1,strlen($target_dir));
                 	
                 $list_array[] = $target_dir;
                                  
                 for ($i=0; $i<sizeof($filelist); $i++)
                 {
						$file = $filelist[$i];
						$type = (substr($file["permissions"], 0, 1) == 'd')? 'D':'F';
						$filename = $file["name"];

						if ($type == "D" AND substr($filename,0,1) != ".")
						{
							$dir = "$target_dir/$filename";
							$child = $this->getDirectoryList($dir);
							for ($j=0; $j<sizeof($child); $j++)
							{
							   $list_array[] = $child[$j];
							}
						}
                 }
                 
                 return $list_array;
        }

        // Attempt to change the current directory into the one specified.
        // Returns TRUE on success or FALSE on failure.
        function chdir($target_dir)
        {
                //$target_dir = addslashes($target_dir);
                return @ftp_chdir($this->connection, $target_dir);
        }

        // Return the current directory
        // Returns the current directory or FALSE on error.
        function pwd()
        {
                return stripslashes(@ftp_pwd($this->connection));
        }

        // Attempt to make a directory specified in $new_dir in the current directory
        // Returns the newly created directory name on success or FALSE on error.
        function mkdir($new_dir)
        {
        		//$new_dir = iconv("BIG5","UTF8//TRANSLIT",$new_dir);
                return @ftp_mkdir($this->connection, $new_dir);
        }

        // Attempt to remove the directory specified in the current directory
        // Returns TRUE on success or FALSE on failure.
        function rmdir($target_dir)
        {
                return @ftp_rmdir($this->connection, $target_dir);
        }

        // Attempt to change the permission of the file specified.
        // '644' if the file is public, '700' otherwise
        // Returns TRUE on success or FALSE on failure.
        function chmod($target_dir, $target_file, $isPublic)
        {
                # Param:
                # $target_dir : the directory of the filename
                # $filename : the target file
                # $isPublic : 'true' if the file is public, 'false' otherwise
                global $sys_custom;
                
                $target_file = stripslashes($target_file);
                if ($this->host_type == "Windows") return;
                
                if($sys_custom['iFolder_CustomizeFileAccessRight'] != "") {
                	$chmod_cmd = ($isPublic)? "CHMOD 0755 $target_dir"."/"."$target_file":"CHMOD ".$sys_custom['iFolder_CustomizeFileAccessRight']." $target_dir"."/"."$target_file";
                } else {
                	$chmod_cmd = ($isPublic)? "CHMOD 0755 $target_dir"."/"."$target_file":"CHMOD 0700 $target_dir"."/"."$target_file";
                }
                
                //$chmod_cmd = "CHMOD 0755 $target_dir"."/"."$target_file";
                return @ftp_site($this->connection, $chmod_cmd);
        }

     function displayFunctionbar($a, $b){
          return "<table width=100% border=0 cellpadding=1 cellspacing=0><tr><td>$a</td><td align=right>$b</td></tr></table>\n";
     }

		function phpduoCopy($file, $dest, $type, $folder1="", $folder2="")
		/*
		** Copy a file / folder from $file to $dest
		** If it is a directory ($type: D - directory; F - File), copy the whole directory
		** folder1 - local ; folder2 - ftp
		*/
		{	
			global $lu,$UserID,$ck_course_id,$fm,$eclass_session_password;
			
			
			if (!(isset($lu) && $lu->UserID==$UserID))
			{
				$lu = new libuser($UserID);
			}
			$lftp3 = new libftp();	
						
			if ($eclass_session_password != "")
				$conn_id = $lftp3->connect(strtolower(trim($lu->UserLogin)),trim($eclass_session_password));			
			else 	
				$conn_id = $lftp3->connect(strtolower(trim($lu->UserLogin)),trim($lu->user_password));			
					
			if ($type=="F")
			{
				
				//$result = ftp_get($conn_id,$folder1."/".$file,$folder2."/".$file,FTP_BINARY);				
				//echo $conn_id." ".$folder1."/".$file." ".$folder2."/".$file." ".FTP_BINARY." <br>";
				
				// open some file for reading
				$file1 = $folder1."/".$file;
				$fp = fopen($file1, 'w');

				// Initate the download
				$ret = ftp_nb_fget($conn_id, $fp, $folder2."/".$file, FTP_BINARY);
				while ($ret == FTP_MOREDATA) 
				{

   					// Do whatever you want
   					//echo ".";

   					// Continue downloading...
   					$ret = ftp_nb_continue($conn_id);
				}
				if ($ret != FTP_FINISHED) {
   					//echo "There was an error downloading the file...";
   					//exit(1);
				}

				// close filepointer
				fclose($fp);				
			} 
			else if ($type=="D")
			{
					
				$fm->createFolder($folder1."/".$file);
				$curDir = $folder2."/".$file;
				$lftp4 = new libftp();

				if ($eclass_session_password != "")
					$lftp4->connect(strtolower(trim($lu->UserLogin)),trim($eclass_session_password));			
				else 	
					$lftp4->connect(strtolower(trim($lu->UserLogin)),trim($lu->user_password));											
				$lftp4->chdir($curDir);
				$files = $lftp4->list_file($curDir);
	
				for ($i=0; $i<count($files); $i++)
    			{
					$type = $files[$i]['Filetype'];
					$size = $files[$i]['Filesize'];
					$date = $files[$i]['Date'];
					$name = $files[$i]['Filename'];	    			
					
					$result = $this->phpduoCopy($name, $dest, $type, $folder1."/".$file, $folder2."/".$file);
    			}
				
				
			}
			return $result;
		}
		
		
		function ftp_chmod_recursive ($target_dir) {
			global $sys_custom;
			$rawfilelist = @ftp_rawlist($this->connection, "$target_dir");
			
			$filelist = $this->parseFileListing($rawfilelist);
             
			if(substr($target_dir,0,2) == "//")
				$target_dir = "/..".substr($target_dir,1,strlen($target_dir));
             	
			$list_array[] = $target_dir;

			for ($i=0; $i<sizeof($filelist); $i++)
			{
				$file = $filelist[$i];
				
				$type = (substr($file["permissions"], 0, 1) == 'd')? 'D':'F';
				$permission_num = $this->chmodnum(substr($file["permissions"],1,strlen($file["permissions"])));
				$filename = $file["name"];
				if ($type == "D" AND substr($filename,0,1) != ".")
				{
					## Directory 
					$dir = "$target_dir/$filename";
					
					if($sys_custom['iFolder_CustomizeFileAccessRight'] != "") {
						if($permission_num != $sys_custom['iFolder_CustomizeFileAccessRight']){
							$command = "chmod ".$sys_custom['iFolder_CustomizeFileAccessRight']." /".$dir;
							ftp_site($this->connection, $command);
						}
					}else{
						## default permission - 755
						if($permission_num != "755"){
							$command = "chmod 755 /".$dir;
							ftp_site($this->connection, $command);
						}
					}
					
					$child = $this->ftp_chmod_recursive($dir);
					
					for ($j=0; $j<sizeof($child); $j++)
					{
					   $list_array[] = $child[$j];
					}
				}else{
					## File
					$dir = "$target_dir/$filename";
					if($sys_custom['iFolder_CustomizeFileAccessRight'] != "") {
						if($permission_num != $sys_custom['iFolder_CustomizeFileAccessRight']){
							$command = "chmod ".$sys_custom['iFolder_CustomizeFileAccessRight']." /".$dir;
							ftp_site($this->connection, $command);
						}
					}else{
						## default permission - 755
						if($permission_num != "755"){
							$command = "chmod 755 /".$dir;
							ftp_site($this->connection, $command);
						}
					}
				}
			}
		}
		
		function chmodnum($mode) {
			$realmode = "";
			$legal =  array("","w","r","x","-");
			$attarray = preg_split("//",$mode);
			for($i=0;$i<count($attarray);$i++){
				if($key = array_search($attarray[$i],$legal)){
					$realmode .= $legal[$key];
				}
			}
			$mode = str_pad($realmode,9,'-');
			$trans = array('-'=>'0','r'=>'4','w'=>'2','x'=>'1');
			$mode = strtr($mode,$trans);
			$newmode = '';
			$newmode .= $mode[0]+$mode[1]+$mode[2];
			$newmode .= $mode[3]+$mode[4]+$mode[5];
			$newmode .= $mode[6]+$mode[7]+$mode[8];
			return $newmode;
		}
}

        function list_compare_function ($s1,$s2)
        {
                 # Check Directory/File
                 $s1_type = substr($s1["permissions"],0,1);
                 $s2_type = substr($s2["permissions"],0,1);

                 if ($s1_type == "d" && $s2_type != "d")
                 {
                     return -1;
                 }
                 else if ($s1_type != "d" && $s2_type == "d")
                 {
                      return 1;
                 }

                 # filename
                 $s1_filename = strtolower($s1["name"]);
                 $s2_filename = strtolower($s2["name"]);
                 return strcmp($s1_filename, $s2_filename);
                 /*
                 if ($s1_filename > $s2_filename)
                 {
                     return 1;
                 }
                 else return -1;
                 */
        }
        
        


}    # End of directive
?>