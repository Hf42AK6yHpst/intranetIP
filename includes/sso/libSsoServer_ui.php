<?php
// using : Frankie
/*
 * Change Log:
 * 2018-07-04 Frankie SSO from DSI
 * 2018-06-14 Frankie SSO for DSI initial
 */

if (!defined("LIBSSOSERVER_UI_DEFINED"))
{
	define("LIBSSOSERVER_UI_DEFINED", true);
	
	class libSsoServer_ui extends interface_html
	{
		
	    public function libSsoServer_ui($parTemplate='')
	    {
			$template = ($parTemplate=='')? 'default.html' : $parTemplate;
			$this->interface_html($template);
		}
	}
}