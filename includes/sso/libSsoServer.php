<?php
// using : Frankie
/*
 * Change Log:
 * 2018-07-04 Frankie - SSO from DSI
  */

if (!defined("LIBSSOSERVER_DEFINED"))
{
	define("LIBSSOSERVER_DEFINED", true);

	include_once($PATH_WRT_ROOT."includes/sso/libSsoCommon.php");

	class libSsoServer extends libSsoCommon
	{
	    public function __construct()
		{
		    global $junior_mck, $sys_custom, $SsoFromAnotherSite;


		    $this->libdb();

		    if (file_exists(dirname(dirname(__FILE__)) . "/json.php")) {
		        require_once(dirname(dirname(__FILE__)) . "/json.php");
		        $this->json = new JSON_obj();
		    }


		    /*****************************************************/
		    $this->isEJ = (isset($junior_mck) && $junior_mck > 0);
		    $this->config['ssoEnv'] = $SsoFromAnotherSite;

		    $this->tables = array();
		    if (!empty($sys_custom['INTRANET_USER']))
		    {
		        $this->tables['userTable'] = $sys_custom['INTRANET_USER'];
		    }
		    else
		    {
		        $this->tables['userTable'] = 'INTRANET_USER';
		    }

		    $this->tables['oauthClientTable'] = 'INTRANET_SSO_OAUTH_CLIENT';
		    $this->tables['oauthAccessTokensTable'] = 'INTRANET_SSO_OAUTH_ACCESS_TOKENS';
		    $this->tables['oauthRefreshTokensTable'] = 'INTRANET_SSO_OAUTH_REFRESH_TOKENS';


		    // $domainInfo = parse_url();
		    if (isset($_SERVER["HTTPS"]))
		    {
		        $http_host = "https://" . $_SERVER['HTTP_HOST'];
		    } else {
		        $http_host = "http://" . $_SERVER['HTTP_HOST'];
		    }
		    $this->token_key = md5($http_host);

		    $this->config['token']['token_expired'] = 60 * 15; // 15 minuite;
		    $this->config['token']['refresh_expired'] = 60 * 60 * 24 * 30; // 30 days
		    $this->config['token']['access_token_packkey'] = $this->generateRandomString(10);
		    $this->config['token']['refresh_token_packkey'] = $this->generateRandomString(20);

		}

		/**
		 * Set the token on the user.
		 *
		 * @param  string $token
		 *
		 * @return $this
		 */
		public function setToken($token)
		{
		    $this->token = $token;
		}

		/**
		 * Set the refresh token required to obtain a new access token.
		 *
		 * @param  string $refreshToken
		 *
		 * @return $this
		 */
		public function setRefreshToken($refreshToken)
		{
		    $this->refreshToken = $refreshToken;
		}

		/**
		 * Set the number of seconds the access token is valid for.
		 *
		 * @param  int $expiresIn
		 *
		 * @return $this
		 */
		public function setExpiresIn($expiresIn)
		{
		    $this->expiresIn = $expiresIn;
		}

		/**
		 * default Error Response
		 */
		public function defaultFailPage()
		{
		    header("Content-Type:text/html;charset=utf8");
		    No_Access_Right_Pop_Up();
		    exit;
		}

		/**
		 * generate Client Secret
		 *
		 * @param unknown $request
		 * @return unknown
		 */
		public function generateClientSecret($request)
		{
		    return md5(base64_encode(md5($this->token_key . "|" . $request["client_id"] . "|" . $this->getClientRealIp())));
		}


		/**
		 * get Client Credentials By Client ID
		 *
		 * @param string $client_id
		 * @return array|NULL
		 */
		public function getClientCredentialsByClientID($client_id)
		{
		    $sql = "SELECT sso_client_id, client_id, client_secret, redirect_uri FROM " . $this->tables['oauthClientTable'];
		    $sql .= " WHERE client_id like '" . $client_id. "'";
		    $sql .= " limit 1";

		    $result = $this->returnResultSet($sql);

		    if (count($result) > 0) {
		        return $result[0];
		    }
		    return null;
		}

		/**
		 * Create Client Credentials
		 *
		 * @param array $param
		 * @return array
		 */
		public function createClientCredentials($param)
		{
		    $sql = "INSERT INTO " . $this->tables['oauthClientTable'] . " (" . implode(", ", array_keys($param)) . " ) VALUES ('" . implode("', '", array_values($param)) . "') ";
		    return $this->db_db_query($sql);
		}


		/**
		 * Client Credentials Handle
		 *
		 * @param array $request
		 */
		public function clientCredentials($request)
		{
		    $checksum = md5($request['id'] . "=" . $request["client_id"] . "=" . $request['redirect_url']);
		    $param = null;

		    if ($checksum == $request["checksum"] && $this->checkSsoSysConfig($request))
		    {

		        $param = $this->getClientCredentialsByClientID($request["client_id"]);
		        if ($param === null)
		        {
		            $client_param = array(
		                "client_id" => $request["client_id"],
		                "client_secret" => $this->generateClientSecret($request),
		                "redirect_uri" => $request['redirect_url']
		            );
		            if ($this->createClientCredentials($client_param))
		            {
		                $param = $this->getClientCredentialsByClientID($request["client_id"]);
		            }
		        }
		        if ($param !== null) {

		            if ($request['redirect_url'] != $param["redirect_uri"])
		            {
		                $sql = "UPDATE " . $this->tables['oauthClientTable'] . " SET redirect_uri='" . $request['redirect_url'] . "' WHERE client_id='" . $request["client_id"] . "'";
		                $this->db_db_query($sql);
		            }
		            $param["redirect_uri"] = $request['redirect_url'];
		            $param["sso_identity"] = $request['id'];
		            $param["expiredTime"] = time();
		            $param["ip_addr"] = $this->getClientRealIp();
		            $param["time"] = date("Y-m-d H:i:s");

		            $_SESSION["ECLASS_SSO"]["client_credentials"] = $param;

		            header("Location: /sso/eclass/?task=dsilogin");
		            exit;
		        }
		    }
	        $this->defaultFailPage();
		}

		/**
		 * generate Full Token Record
		 *
		 * @param array $client_credentials
		 * @param int $user_id
		 * @return array|null
		 */
		public function generateTokenInfo($client_credentials, $user_id)
		{
		    $current_time = date("Y-m-d H:i:s");

		    $access_token = $this->generateToken('access_token', $user_id, $current_time);
		    $param = array(
		        'sso_client_id' => $client_credentials['sso_client_id'],
		        'client_id' => $client_credentials['client_id'],
		        'access_token' => $access_token,
		        'user_id' => $user_id,
		        'expires' => date("Y-m-d H:i:s", strtotime($current_time) + $this->config['token']['token_expired']),
		        'scope' => '*',
		        'ip_addr' => $client_credentials['ip_addr'],
		        'created_at' => $current_time
		    );

 		    $sql = "SELECT 
                        token_id 
                    FROM " . $this->tables['oauthAccessTokensTable'] . "
                    WHERE 
                        user_id='" . $user_id . "'
                        AND client_id='" . $param["client_id"] . "'
                        AND ip_addr='" . $client_credentials['ip_addr'] . "'
                    LIMIT 1";
		    $result = $this->returnResultSet($sql);
		    if (count($result) > 0) {
		        $token_id = $result[0]['token_id'];
		        $sql = "UPDATE " . $this->tables['oauthAccessTokensTable'] . " SET access_token='" . $access_token . "', expires='" . $param["expires"] . "' WHERE token_id='" . $token_id. "'";
		        $this->db_db_query($sql);
		    } else {
		        $sql = "INSERT INTO " . $this->tables['oauthAccessTokensTable'] . " (" . implode(', ', array_keys($param)) . ") VALUES ('" . implode("', '", array_values($param)) . "')";
		        $this->db_db_query($sql);
		    }

		    $refresh_token = $this->generateToken('refresh_token', $user_id, $current_time);
		    $param = array(
		        'sso_client_id' => $client_credentials['sso_client_id'],
		        'client_id' => $client_credentials['client_id'],
		        'refresh_token' => $refresh_token,
		        'user_id' => $user_id,
		        'expires' => date("Y-m-d H:i:s", strtotime($current_time) + $this->config['token']['refresh_expired']),
		        'ip_addr' => $client_credentials['ip_addr'],
		        'created_at' => $current_time
		    );

 		    $sql = "SELECT 
                        refresh_token_id
                    FROM " . $this->tables['oauthRefreshTokensTable'] . "
                    WHERE
                        user_id='" . $user_id . "' AND
                        client_id='" . $param["client_id"] . "' AND
                        ip_addr='" . $client_credentials['ip_addr'] . "'
                    LIMIT 1";
		    $result = $this->returnResultSet($sql);
		    if (count($result) > 0) {
		        $token_id = $result[0]["refresh_token_id"];
		        $sql = "UPDATE " . $this->tables['oauthRefreshTokensTable'] . " SET refresh_token='" . $refresh_token . "', expires='" . $param["expires"] . "' WHERE refresh_token_id='" . $token_id. "'";
		        $this->db_db_query($sql);
		    } else {
		        $sql = "INSERT INTO " . $this->tables['oauthRefreshTokensTable'] . " (" . implode(', ', array_keys($param)) . ") VALUES ('" . implode("', '", array_values($param)) . "')";
		        $this->db_db_query($sql);
		    }

		    $sso_query = array(
		        'access_token' => $access_token,
		        'expires_in' => date("Y-m-d H:i:s", strtotime($current_time) + $this->config['token']['token_expired']),
		        'client_id' => $client_credentials["client_id"],
		        'client_secret' => $client_credentials["client_secret"],
		        'sso_identity' => $client_credentials['sso_identity'],
		        'token_type' => 'Bearer',
		        'scope' => '*',
		        'refresh_token' => $refresh_token,
		        'callback' => $this->getHttpHost(),
		        'is_curl' => true
		    );
		    return $sso_query;
		}

		/**
		 * check User is valid Teacher account
		 *
		 * @param int $user_id
		 * @return boolean
		 */
		public function checkUserValidById($user_id)
		{
		    $sql = "SELECT
                        UserLogin
                    FROM " . $this->tables['userTable'] . "
                    WHERE UserID='" . $user_id . "'
                    AND RecordStatus='1'
                    AND RecordType='1'
                    LIMIT 1";
		    $result = $this->returnResultSet($sql);
		    if (count($result) > 0)
		    {
		        return true;
		    } else {
		        return false;
		    }
		}

		/**
		 * authorize User Account and Token
		 *
		 * @param array $client_credentials
		 * @param int $user_id
		 */
		public function authorizeAccount($client_credentials, $user_id)
		{
		    $callback = $this->getHttpHost();

		    if ($this->checkUserValidById($user_id))
		    {
		        $sso_query = $this->generateTokenInfo($client_credentials, $user_id);


		        $url = $client_credentials['redirect_uri'];
		        $response = $this->httpRequest($url, $sso_query, 'get');
		        if ($response != true)
		        {
		            unset($sso_query["is_curl"]);
		            if (strpos($url, '?') !== false) {
		                $url .= "&" . http_build_query($sso_query);
		            } else {
		                $url .= "?" . http_build_query($sso_query);
		            }
		            header("Location: " . $url);
		            exit;
		        }
		    }
		    header("Location: " . $callback);
		    exit;
		}

		/**
		 * generate Token string
		 *
		 * @param string $type
		 * @param int $user_id
		 * @param datetime $current_time
		 * @return string|NULL
		 */
		public function generateToken($type = 'access_token', $user_id, $current_time)
		{
		    $sql = "SELECT AES_ENCRYPT('" . $type . "||" . $user_id . "||" . $current_time . "', UNHEX('" . bin2hex($this->config['token'][$type . '_packkey']) . "')) as token";
		    $result = $this->returnResultSet($sql);
		    $token = $result[0]['token'];
		    if (!empty($token))
		    {
		        return bin2hex($result[0]['token']) . "+" . md5($result[0]['token']);
		    }
	        return null;
		}


		/**
		 * valadite token and generate token by refresh token
		 *
		 * @param array $request
		 */
		public function authorizeToken($request)
		{
		    if (!isset($request["ip_addr"]))
		    {
		        $request["ip_addr"] = $this->getClientRealIp();
		    }

		    if (isset($request["access_token"]))
		    {
	            $sql = "SELECT 
                            sso_client_id, user_id, expires
                        FROM 
                            " . $this->tables['oauthAccessTokensTable'] . "
                        WHERE 
                            client_id='" . $request["client_id"] . "'
                            AND access_token='" . $request["access_token"] . "'
                            AND ip_addr LIKE '" . $request['ip_addr'] . "'
                        LIMIT 1";

	            $result = $this->returnResultSet($sql);
	            if (count($result) > 0)
	            {
	                $tokenInfo = $result[0];

	                if ($this->checkUserValidById($tokenInfo["user_id"]))
	                {
    	                $current_time = time();
    	                $expired_time = strtotime($tokenInfo["expires"]);
    	                if ($current_time > $expired_time)
    	                {
    	                    $response = array(
    	                        "token_status" => "EXPIRED"
    	                    );
    	                } else {
    	                    $response = array(
    	                        "token_status" => "VALID"
    	                    );
    	                }
	                } else {
	                    $response = array(
	                        "token_status" => "INVALID_ACCOUNT"
	                    );
	                }
	            } else {
	                $response = array(
	                    "token_status" => "NOT_FOUND"
	                );
	            }
	            echo $this->jsonEncode($response);
		    }
		    else
		    {
		        if (isset($request["refresh_token"]))
		        {

		            if ($this->checkSsoSysConfig($request)) {
		                $query_str = http_build_query(array( "ssoUserLogin" => 'abc', "ssoAuthType" => "SSOLOGIN", "ssoSrvType" => "" ));
		                $key = getEncryptedText($query_str);


		                $sql = "SELECT
                            sso_client_id, user_id, expires
                        FROM
                            " . $this->tables['oauthRefreshTokensTable'] . "
                        WHERE
                            client_id='" . $request["client_id"] . "'
                            AND refresh_token='" . $request["refresh_token"] . "'
                            AND ip_addr like '" . $request["ip_addr"] . "'
                        LIMIT 1";

		                $result = $this->returnResultSet($sql);
		                if (count($result) > 0)
		                {
		                    $tokenInfo = $result[0];
		                    if ($this->checkUserValidById($tokenInfo["user_id"]))
		                    {
		                        $current_time = time();
		                        $expired_time = strtotime($tokenInfo["expires"]);
		                        if ($current_time > $expired_time)
		                        {
		                            $response = array(
		                                "token_status" => "EXPIRED"
		                            );
		                        } else {

		                            $clientIDInfo = $this->getClientCredentialsByClientID($request["client_id"]);
		                            $request['sso_client_id'] = $clientIDInfo['sso_client_id'];

		                            $response = $this->generateTokenInfo($request, $tokenInfo["user_id"]);
		                            $response["token_status"] = "RESET";
		                        }
		                    }
		                } else {
		                    $response = array(
		                        "token_status" => "INVALID_ACCOUNT"
		                    );
		                }
		            } else {
		                $this->defaultFailPage();
		            }
		        } else {
		            $response = array(
		                "token_status" => "NOT_FOUND"
		            );
		        }
		        echo $this->jsonEncode($response);
		    }
		    exit;
		}

		/**
		 * authorize algorithm
		 *
		 */
		public function authorize($request)
		{
		    $request["ip_addr"] = $this->getClientRealIp();
		    $sql = "SELECT
                            sso_client_id, user_id, expires
                        FROM
                            " . $this->tables['oauthAccessTokensTable'] . "
                        WHERE
                            client_id='" . $request["client_id"] . "'
                            AND access_token='" . $request["access_token"] . "'
                            AND ip_addr like '" . $request['ip_addr'] . "'
                        LIMIT 1";
		    $result = $this->returnResultSet($sql);
		    if (count($result) > 0 && $this->checkUserValidById($result[0]["user_id"]))
		    {
		        $current_time = time();
		        $expired_time = strtotime($result[0]["expires"]);
		        if ($current_time <= $expired_time)
		        {

		            $userInfo = $this->getUserInfo($result[0]["user_id"]);
		            $string = "ssoUserLogin=" . $userInfo["UserLogin"] . "&ssoUID=" . $result[0]["user_id"] . "&ssoAuthType=SSOLOGIN";
		            $key = getEncryptedText($string);

		            $_SESSION["ECLASS_SSO"]["client_credentials"] = array(
		                "target_e" => $key,
		                "client_id" => $request["client_id"]
		            );

		            header("Location: /login.php");
		            exit;
		        }
		    }
		    $this->defaultFailPage();
		}

		/**
		 * get User Info
		 *
		 * @param int $user_id
		 * @return array|NULL
		 */
		public function getUserInfo($user_id)
		{
		    $sql = "SELECT
                        UserLogin
                    FROM " . $this->tables['userTable'] . "
                    WHERE UserID='" . $user_id . "'
                    AND RecordStatus='1'
                    AND RecordType='1'
                    LIMIT 1";
		    $result = $this->returnResultSet($sql);
		    if (count($result) > 0)
		    {
		        return $result[0];
		    }
		    return null;
		}
	}
}
