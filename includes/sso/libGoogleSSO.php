<?php
//Using:
/**
 * Change Log:
 *  2019-08-09 - Crabbe
 *  -Updated sync functions to include checking on account status and membersip in google
 *  - added groupMembersLogins()
 *  2019-05-13 Paul
 * 	- Add quote to query having variable not quoted
 *  2018-12-05 Pun [153965] [ip.2.5.10.1.1]
 *  - Modified suspendUserFromEClassToGoogle(), fixed syntax error
 *  2018-10-10 - Crabbe
 *  -Added the MAX_ATTEMPT_GOOGLE_ACTION constant
 *  2018-10-05 - Crabbe
 *  -Added the removeGroupMemberFromEClassToGoogle Function
 *  -Added the removeGroupFromEClassToGoogle Function
 * 2018-09-17 - Crabbe
 *  - Added the syncGroupFromEClassToGoogle function
 *  - Added the syncGroupMemberFromEClassToGoogle function
 *   -Added the activateUserFromEClassToGoogle function
 *   -Added the suspendUserFromEClassToGoogle function
 * 2018-08-30 Pun [ip.2.5.9.10.1]
 *  - Modified syncUserFromEClassToGoogle(), added $chiname
 * 2018-08-27 Pun
 *  - Modified syncGroupForUserFromEClassToGoogle(), fix cannot add user to new group
 */
class libGoogleSSO{
    const MAX_ATTEMPT_GOOGLE_ACTION =  5;
    public function getRemainingQuota($google_api){
        global $ssoservice;

        $quota = $ssoservice["Google"]['api'][$google_api->getConfigIndex()]['quota'];
        $number_of_accounts = $google_api->numberOfAccounts();
        $diff = $quota - $number_of_accounts;
        return (($diff < 0) ? 0 : $diff);
    }

    public function getAllGoogleAPIConfigIndex(){
        global $ssoservice;
        $array_return = array();

        $google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
        foreach((array)$google_apis as $google_api){
            array_push($array_return, $google_api->getConfigIndex());
        }
        return $array_return;
    }

    public function syncUserFromEClassToGoogle($uid, $userlogin, $status, $engname, $password_for_google_account_creation, $gmail_input_array, $chiname=''){
        global $ssoservice;

        if($chiname == ''){
            $chiname = $engname;
        }
        // when on update, no password is passed in
        if($password_for_google_account_creation == ''){
            global $PATH_WRT_ROOT;
            include_once($PATH_WRT_ROOT."includes/libpwm.php");
            $libpwm = new libpwm();

            $password_for_google_account_creation='';
            $rows_password = $libpwm->getData(array($uid));
            foreach((array)$rows_password as $key=>$row_password){
                $password_for_google_account_creation = $row_password;
            }
        }
        $array_error_message = array();

        $google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
        $libSSO_db = new libSSO_db();

        foreach((array)$google_apis as $google_api){
            $array_result = $libSSO_db->isUserHaveSSOIdentifiersAndReturnWithStatus($uid, $google_api);
            $is_account_record_exist = $array_result['is_account_record_exist'];
            $account_status = $array_result['account_status'];

            //check if G Suite account exists
            $is_target_account_exist = false;
            try{
                $account_details = $google_api->getGoogleAccountByEmail($userlogin.'@'.$google_api->getDomain());
                $is_target_account_exist = ($account_details == null ? false : true);
            }catch(Exception $e){
                $is_target_account_exist = false;
            }

            $error_message = '';
            $gmail_array_index = 'gmail_' . $google_api->getConfigIndex();
            $account_identifier = $userlogin . '@' . $google_api->getDomain();
            $google_account_exists_during_creation = false;
            $google_account_deleted = false;
            if(!$is_account_record_exist){
                if($gmail_input_array[$gmail_array_index] == '1'){
                    if($status=='1'){
                        //echo 'db have no records, checked GSuite, active => create with status "a"';
                        try{
                            if(!$is_target_account_exist){
                                $google_api->newGoogleAccountByFirstNameAndLastName($chiname,$engname,$account_identifier,$password=$password_for_google_account_creation);
                            }elseif ($account_details['suspended']){
                                $google_api->reactivateGoogleAccountByEmail($account_identifier);
                            }
                            $libSSO_db->addSSOIdentifier($uid, $account_identifier);
                        }catch(Exception $e){
                            if($google_api->isError($e,'409')){
                                $google_account_exists_during_creation = true;
                                $password = '';
                                try{
                                    $google_api->reactivateGoogleAccountByEmail($account_identifier);
                                    $libSSO_db->addSSOIdentifier($uid, $account_identifier);
                                }catch(Exception $e){
                                    $error_message = $google_api->getErrorMessage($e);
                                }
                            }elseif($google_api->isError($e,'412')){
                                $error_message = $google_api->getErrorMessage($e);
                            }elseif($google_api->isError($e)){
                                $error_message = $google_api->getErrorMessage($e);
                            }
                        }
                    }else{
                        //echo 'db have no records, checked GSuite, inactive => create with status "s"';
                        try{
                            if(!$is_target_account_exist){
                                $google_api->newGoogleAccountByFirstNameAndLastName($chiname,$engname,$account_identifier,$password=$password_for_google_account_creation);
                            }
                        }catch(Exception $e){
                            // echo $e->getMessage();
                            if($google_api->isError($e,'409')){
                                $google_account_exists_during_creation = true;
                                $password = '';
                            }elseif($google_api->isError($e)){
                                $error_message = $google_api->getErrorMessage($e);
                            }
                        }
                        try{
                            $google_api->suspendGoogleAccountByEmail($account_identifier);
                            $libSSO_db->addSSOIdentifier($uid, $account_identifier,'s');
                        }catch(Exception $e){
                            $error_message = $google_api->getErrorMessage($e);
                        }
                    }
                }else{
                    //echo 'db have no records, unchecked GSuite, inactive/active => create with status "n"';
                    $libSSO_db->addSSOIdentifier($uid, $account_identifier,'n');
                }
            }else{
                if($gmail_input_array[$gmail_array_index] == '1'){
                    if($status=='1'){
                        //echo 'db have records, checked GSuite, active => update with status "a"';
                        if($account_status == 'n' || $account_status == 'x'){
                            try{
                                if(!$is_target_account_exist){
                                    $google_api->newGoogleAccountByFirstNameAndLastName($chiname,$engname,$account_identifier,$password=$password_for_google_account_creation);
                                }
                                $libSSO_db->resumeSSOIdentifier($uid, $account_identifier);
                            }catch(Exception $e){
                                // echo $e->getMessage();
                                if($google_api->isError($e,'409')){
                                    $google_account_exists_during_creation = true;
                                    $password = '';
                                    try{
                                        $google_api->reactivateGoogleAccountByEmail($account_identifier);
                                        $libSSO_db->resumeSSOIdentifier($uid, $account_identifier);
                                    }catch(Exception $e){
                                        $error_message = $google_api->getErrorMessage($e);
                                    }
                                }elseif($google_api->isError($e)){
                                    $error_message = $google_api->getErrorMessage($e);
                                }
                            }
                        }elseif($account_status == 's'){
                            try{
                                $google_api->reactivateGoogleAccountByEmail($account_identifier);
                                $libSSO_db->resumeSSOIdentifier($uid, $account_identifier);
                            }catch(Exception $e){
                                $error_message = $google_api->getErrorMessage($e);
                            }
                        }elseif($account_status == 'a'){
                            try{
                                $google_api->reactivateGoogleAccountByEmail($account_identifier);
                            }catch(Exception $e){
                                $error_message = $google_api->getErrorMessage($e);
                            }
                        }
                    }else{
                        //echo 'db have records, checked GSuite, inactive => update with status "s"';
                        try{
                            if(!$is_target_account_exist){
                                $google_api->newGoogleAccountByFirstNameAndLastName($chiname,$engname,$account_identifier,$password=$password_for_google_account_creation);
                            }elseif ($account_details['suspended']){
                                $google_api->reactivateGoogleAccountByEmail($account_identifier);
                            }
                        }catch(Exception $e){
                            // echo $e->getMessage();
                            if($google_api->isError($e,'409')){
                                $google_account_exists_during_creation = true;
                                $password = '';
                            }elseif($google_api->isError($e)){
                                $error_message = $google_api->getErrorMessage($e);
                            }
                        }
                        try{
                            $google_api->suspendGoogleAccountByEmail($account_identifier);
                            $libSSO_db->suspendSSOIdentifier($uid, $account_identifier);
                        }catch(Exception $e){
                            $error_message = $google_api->getErrorMessage($e);
                        }
                    }
                }else{
                    //echo 'db have records, unchecked GSuite, inactive/active => update with status "x"';
                    if($account_status != 'n' && $account_status != 'x'){
                        try{
                            $google_api->deleteGoogleAccountByEmail($account_identifier);
                            $libSSO_db->deleteSSOIdentifier($uid, $account_identifier);
                            $google_account_deleted = true;
                        }catch(Exception $e){
                            $error_message = $google_api->getErrorMessage($e);
                        }
                    }
                }
            }

            $array_error_message[$google_api->getConfigIndex()] = $error_message;
        }

        foreach((array)$array_error_message as $config_index => $error_message){
            if($error_message==''){
                if(!$google_account_deleted){
                    try{
                        $google_api->updateGoogleAccountInformation($account_identifier,$engname,$chiname);
                    }catch(Exception $e){
                    }
                }
            }
        }

        return $array_error_message;
    }

    public function syncGroupForUserFromEClassToGoogle($laccount, $userlogin, $gmail_input_array, $GroupID, $array_original_group_id = array()){
        global $ssoservice;

        $google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
        $libSSO_db = new libSSO_db();
        $CurYearID = Get_Current_Academic_Year_ID();
        $sql1 = 'SELECT `YearNameEN` FROM `ACADEMIC_YEAR` WHERE `AcademicYearID`=\'' . $CurYearID . '\';';
        $CurrYearNum = current($laccount->returnVector($sql1));
        $CurrYear = ' (' . $CurrYearNum . ')';

        foreach((array)$google_apis as $google_api){

            $gmail_array_index = 'gmail_' . $google_api->getConfigIndex();
            if($gmail_input_array[$gmail_array_index] == '1'){
                //add group
                for($i=0; $i<sizeof($GroupID); $i++){

                    $sql = 'SELECT `Title` FROM `INTRANET_GROUP` WHERE `GroupID`=\''.$GroupID[$i].'\';';
                    $rows_group = $laccount->returnArray($sql);
                    if(count($rows_group)>0){
                        $row_group = $rows_group[0];

                        $group_email = 'g'.($GroupID[$i]+288).'@'.$google_api->getDomain();
                        $group_email_original = '';
                        $is_new_email_address = false;
                        $rows_sso_group = $libSSO_db->getSSOGroup($GroupID[$i]);
                        if(count($rows_sso_group)>0){
                            $row_sso_group = $rows_sso_group[0];
                            $group_email_original = $row_sso_group['SSOGroupIdentifier'];
                            if($group_email_original!=$group_email){
                                $is_new_email_address = true;
                            }
                            if (in_array($row_sso_group['GroupID'],array(1,2,3,4))) {
                                if($row_sso_group['SSOGroupTitle']!=$row_group['Title'].$CurrYear){
                                    $is_new_email_address = true;
                                }
                            }
                        }
                        $libSSO_db->addSSOGroupIdentifier($GroupID[$i], $group_email,($row_group['Title'].$CurrYear));
                        try{
                            if($is_new_email_address){
                                $google_api->updateGoogleGroupByEmail($group_email_original,(htmlspecialchars_decode(($row_group['Title'].$CurrYear),ENT_QUOTES)),$group_email);
                            }else{
                                $google_api->newGoogleGroup((htmlspecialchars_decode(($row_group['Title'].$CurrYear),ENT_QUOTES)),$group_email);
                            }
                        }catch(Exception $e){
                            //								echo $e->getMessage();
                        }
                        $group_existing_logins = $this->groupMembersLogins($GroupID[$i]);
                        if ($group_existing_logins==null) {
                            $account_identifier = $userlogin . '@' . $google_api->getDomain();

                            for ($j = 0; $j < libGoogleSSO::MAX_ATTEMPT_GOOGLE_ACTION; $j ++) {  // Wait for create group completed
                                try{
                                    $google_api->addMemberToGroup($group_email,$account_identifier);
                                    break;
                                }catch(Exception $e){
                                    //							echo $e->getMessage();
                                    sleep(1);
                                }
                            }
                        }else {
                            if (!in_array($userlogin,$group_existing_logins)) {
                                $account_identifier = $userlogin . '@' . $google_api->getDomain();

                                for ($j = 0; $j < libGoogleSSO::MAX_ATTEMPT_GOOGLE_ACTION; $j ++) {  // Wait for create group completed
                                    try{
                                        $google_api->addMemberToGroup($group_email,$account_identifier);
                                        break;
                                    }catch(Exception $e){
                                        //							echo $e->getMessage();
                                        sleep(1);
                                    }
                                }
                            }
                        }
                    }
                }

                if(count($array_original_group_id) > 0){
                    //remove group

                    $array_removed_group_id = array_diff($array_original_group_id,(array)$GroupID);
                    foreach((array)$array_removed_group_id as $removed_group_id){
                        $sql = 'SELECT `Title` FROM `INTRANET_GROUP` WHERE `GroupID`=\''.$removed_group_id.'\';';
                        $rows_group = $laccount->returnArray($sql);
                        if(count($rows_group)>0){
                            $row_group = $rows_group[0];
                            $group_email = 'g'.($removed_group_id+288).'@'.$google_api->getDomain();

                            $account_identifier = $userlogin . '@' . $google_api->getDomain();
                            $google_api->removeMemberFromGoogleByEmail($group_email, $account_identifier);
                        }
                    }
                }
            }
        }
    }

    public function syncGroupFromEClassToGoogle($laccount, $GroupIDArr,$currYearID='')
    {
        global $ssoservice;

        $google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
        $libSSO_db = new libSSO_db();
        $CurYearID = Get_Current_Academic_Year_ID();
        $CurYearID = $currYearID?$currYearID:$CurYearID;
        $sql1 = 'SELECT `YearNameEN` FROM `ACADEMIC_YEAR` WHERE `AcademicYearID`=\'' . $CurYearID . '\';';
        $CurrYearNum = current($laccount->returnVector($sql1));
        $CurrYear = ' (' . $CurrYearNum . ')';
        foreach ((array) $google_apis as $google_api) {

            if (! empty($GroupIDArr)) {
                // add group to google and update title
                for ($i = 0; $i < sizeof($GroupIDArr); $i ++) {

                    $sql = 'SELECT `Title` FROM `INTRANET_GROUP` WHERE `GroupID`=\'' . $GroupIDArr[$i] . '\';';
                    $rows_group = $laccount->returnArray($sql);
                    if (count($rows_group) > 0) {
                        $row_group = $rows_group[0];
                        $group_email = 'g' . ($GroupIDArr[$i] + 288) . '@' . $google_api->getDomain();
                        $group_email_original = '';
                        $group_title_original = '';
                        $is_new_email_address = false;
                        $rows_sso_group = $libSSO_db->getSSOGroup($GroupIDArr[$i]);
                        if (count($rows_sso_group) > 0) {
                            $row_sso_group = $rows_sso_group[0];
                            $group_title_original = $row_sso_group['SSOGroupTitle'];
                            $group_email_original = $row_sso_group['SSOGroupIdentifier'];
                            if (($group_email_original != $group_email) || ($group_title_original != ($row_group['Title'] . $CurrYear))) {
                                $is_new_email_address = true;
                            }
                        }
                        $libSSO_db->addSSOGroupIdentifier($GroupIDArr[$i], $group_email, ($row_group['Title'] . $CurrYear)); // edit

                        for ($j = 0; $j < libGoogleSSO::MAX_ATTEMPT_GOOGLE_ACTION; $j ++) { // Prevent Time out
                            try {
                                if ($is_new_email_address) {
                                    $google_api->updateGoogleGroupByEmail($group_email_original, (htmlspecialchars_decode(($row_group['Title'] . $CurrYear), ENT_QUOTES)), $group_email);
                                } else {
                                    $google_api->newGoogleGroup((htmlspecialchars_decode(($row_group['Title'] . $CurrYear), ENT_QUOTES)), $group_email);
                                }
                            } catch (Exception $e) {
                                // echo $e->getMessage();
                            }
                        }
                    }
                }
            }
        }
    }

    public function removeGroupFromEClassToGoogle($GroupIDArr)
    {
        global $ssoservice;

        $google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
        $libSSO_db = new libSSO_db();
        foreach ((array) $google_apis as $google_api) {
            if (count($GroupIDArr) > 0) {
                foreach ((array) $GroupIDArr as $removed_group_id) {
                    $row_group = $libSSO_db->getSSOGroup($removed_group_id);
                    $group_email = 'g' . ($removed_group_id + 288) . '@' . $google_api->getDomain();
                    $groupTitle = $row_group[0]['SSOGroupTitle'];
                    $libSSO_db->deleteSSOGroupIdentifier($removed_group_id, $group_email, $groupTitle);
                    for ($j = 0; $j < libGoogleSSO::MAX_ATTEMPT_GOOGLE_ACTION; $j ++) { // Prevent Time out
                        try {
                            $google_api->deleteGoogleGroupByEmail($group_email);
                            break;
                        } catch (Exception $e) {
                            // echo $e->getMessage();
                            sleep(1);
                        }
                    }
                }
            }
        }
    }

    public function syncGroupMemberFromEClassToGoogle( $userlogin, $GroupID)
    {
        global $ssoservice;

        $google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
        $libSSO_db = new libSSO_db();
        foreach ((array) $google_apis as $google_api) {
            $GroupIDint = (int) $GroupID; // passing from string to int to ensure value is above 0
            if ($GroupIDint > 0) {
                // add group member to existing group
                $group_email = 'g' . ($GroupID + 288) . '@' . $google_api->getDomain();
                $account_identifier = $userlogin . '@' . $google_api->getDomain();
                for ($j = 0; $j < libGoogleSSO::MAX_ATTEMPT_GOOGLE_ACTION; $j ++) { // Prevent Time out
                    try {
                        $google_api->addMemberToGroup($group_email, $account_identifier);
                        break;
                    } catch (Exception $e) {
                        // echo $e->getMessage();
                        sleep(1);
                    }
                }
            }
        }
    }

    public function removeGroupMemberFromEClassToGoogle($userlogin, $removeGroupID)
    {
        global $ssoservice;

        $google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
        $libSSO_db = new libSSO_db();
        foreach ((array) $google_apis as $google_api) {
            $removeGroupIDint = (int) $removeGroupID; // passing from string to int to ensure value is above 0
            if ($removeGroupIDint > 0) {
                // remove Group member from existing group
                $group_email = 'g' . ($removeGroupID + 288) . '@' . $google_api->getDomain();
                $account_identifier = $userlogin . '@' . $google_api->getDomain();
                for ($j = 0; $j < libGoogleSSO::MAX_ATTEMPT_GOOGLE_ACTION; $j ++) { // Prevent Time out
                    try {
                        $google_api->removeMemberFromGoogleByEmail($group_email, $account_identifier);
                        break;
                    } catch (Exception $e) {
                        // echo $e->getMessage();
                        sleep(1);
                    }
                }
            }
        }
    }

    public function suspendUserFromEClassToGoogle($laccount, $userIDArr)
    {
        global $ssoservice;

        $google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
        $libSSO_db = new libSSO_db();
        foreach ((array) $google_apis as $google_api) {
            if (count($userIDArr) > 0) {
                for ($i = 0; $i < sizeof($userIDArr); $i ++) {
                    $sql = 'SELECT `UserLogin` FROM `INTRANET_USER` WHERE `UserID`=\'' . $userIDArr[$i] . '\';';
                    $userlogin = current($laccount->returnVector($sql));
                    for ($j = 0; $j < libGoogleSSO::MAX_ATTEMPT_GOOGLE_ACTION; $j ++) { // Prevent Time out
                        $email_address=$userlogin . '@' . $google_api->getDomain();
                        try {
                            $google_api->suspendGoogleAccountByEmail($email_address);
                            $libSSO_db->suspendSSOIdentifier($userIDArr[$i], $email_address);
                            break;
                        } catch (Exception $e) {
                            // echo $e->getMessage();
                            sleep(1);
                        }
                    }
                }
            }
        }
    }

    public function activateUserFromEClassToGoogle($laccount, $userIDArr)
    {
        global $ssoservice;

        $google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
        $libSSO_db = new libSSO_db();
        foreach ((array) $google_apis as $google_api) {
            if (count($userIDArr) > 0) {   // count() > 0 / !empty()
                for ($i = 0; $i < sizeof($userIDArr); $i ++) {
                    $sql = 'SELECT `UserLogin` FROM `INTRANET_USER` WHERE `UserID`=\'' . $userIDArr[$i] . '\';';
                    $userlogin = current($laccount->returnVector($sql));
                    for ($j = 0; $j < libGoogleSSO::MAX_ATTEMPT_GOOGLE_ACTION; $j ++) {// Prevent Time out
                        $email_address= $userlogin . '@' . $google_api->getDomain();
                        try {
                            $google_api->reactivateGoogleAccountByEmail($email_address);
                            $libSSO_db->resumeSSOIdentifier($userIDArr[$i], $email_address);
                            break;
                        } catch (Exception $e) {
                            // echo $e->getMessage();
                            sleep(1);
                        }
                    }
                }
            }
        }
    }

    public function enabledUserForGoogle($laccount, $userIDArr)
    {
        $userStatus = array();
        if (count($userIDArr) > 0) {
            for ($i = 0; $i < sizeof($userIDArr); $i ++) {
                $sql = "SELECT SSOAccountStatus from INTRANET_SSO_ACCOUNT WHERE UserID = ('" . $userIDArr[$i] . "')";
                $results =current($laccount->returnVector($sql));
                if ($results == 'a') {
                    $userStatus['activeUsers'][] = $userIDArr[$i];;
                } elseif ($results == 's') {
                    $userStatus['suspendedUsers'][] = $userIDArr[$i];
                } elseif ($results == 'n') {
                    $userStatus['unlinkedUsers'][] = $userIDArr[$i];
                } elseif ($results == 'x') {
                    $userStatus['deletedUsers'][] = $userIDArr[$i];
                } elseif ($results == null) {
                    $userStatus['linklessUsers'][] = $userIDArr[$i];
                }
            }
        }
        return $userStatus;
    }

    public function removeUserFromEClassToGoogle($user_id){
        global $ssoservice;

        $google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
        $libSSO_db = new libSSO_db();

        foreach((array)$user_id as $uid){
            try{
                $ssoAccounts = $libSSO_db->getSSOAccount($uid);
                foreach($ssoAccounts as $ssoAccount){
                    foreach((array)$google_apis as $google_api){
                        if(strpos($ssoAccount['SSOAccountIdentifier'],$google_api->getDomain()) !== false){
                            if($ssoAccount['SSOAccountStatus'] != 'x' && $ssoAccount['SSOAccountStatus'] != 'n'){
                                $google_api->deleteGoogleAccountByEmail($ssoAccount['SSOAccountIdentifier']);
                                $libSSO_db->deleteSSOIdentifier($uid, $ssoAccount['SSOAccountIdentifier']);
                            }
                        }
                    }
                }
            }catch(Exception $e){
            }
        }
    }

    public function groupMembersLogins($GroupID)
    {
        global $ssoservice;

        $google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
        $libSSO_db = new libSSO_db();
        foreach ((array) $google_apis as $google_api) {
            $GroupIDint = (int) $GroupID; // passing from string to int to ensure value is above 0
            if ($GroupIDint > 0) {
                // add group member to existing group
                $group_email = 'g' . ($GroupID + 288) . '@' . $google_api->getDomain();
                for ($j = 0; $j < libGoogleSSO::MAX_ATTEMPT_GOOGLE_ACTION; $j ++) { // Prevent Time out
                    try {
                        $results = $google_api->getMembersFromGroupByEmail($group_email);
                        break;
                    } catch (Exception $e) {
                        // echo $e->getMessage();
                        sleep(1);
                    }
                }
            }
            return $results;
        }
    }
}

?>