<?php 
# Using : 
###################################
#       Modification Log
###################################
#   Date:   2019-01-10 [Anna]
#   Modified getMedalListInfoByID(),
#            editMedalListRecord(),
#            addNewMedalListRecord()
#            insertMeadalListRecordTemp() - add WebsiteCode fiedld
#
#
# Date : 2018-06-27 [Philips]
#           added insertEventListRecordTemp()
#           added clearTempEventListRecord()
#           added getEventListRecordTemp()
#           added insertGameListRecordTemp()
#           added clearTempGameListRecord()
#           added getGameListRecordTemp()
#           added getAllSport()
#           added getSport()
#           added addSport()
#           added updateSport()
#           added deleteSport()
#           added getSportID()
#
#
#



include_once($PATH_WRT_ROOT."includes/libdb.php");



class libmedallist extends libdb {
    
    var $minmax;
    var $setting_file;
    var $AcademicYearID;
   
    var $ModuleTitle;		// For General Setting
    var $AllowToEditPreviousYearData;		//allow admin to edit enrolment data (club / activity) of previous years
 
    var $InputBy;
    var $DateInput;
    var $ModifiedBy;
    var $DateModified;
    
    //$Round is for SIS customization
    function libmedallist ($AcademicYearID="", $CategoryID="")
    {
        global $intranet_root,$plugin;
        $this->libdb();
   
//         if (!$plugin['eEnrollment']) return;
        
        $this->Round = $Round;
        
        $this->minmax = "";
        if($AcademicYearID != "")
            $this->AcademicYearID = $AcademicYearID;
     
            $this->ModuleTitle = "MedalList";           
    }
    
    function GET_MODULE_OBJ_ARR()
    {
        global $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $Lang;
        global $image_path, $i_ActivityArchive, $i_SmartCard_TerminalSettings, $plugin, $sys_custom;
        
        global $MedalListMenu;
        global $eEnrollment;
            
            # Get User Access Right
//             $isEnrolAdmin			= $this->IS_ENROL_ADMIN($_SESSION['UserID']);
       
            $genStudentMenu = ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT);
           
                
                # Current Page Information init
            $PageMgtMedalList  = 0;
            $PageSysGameSettings= 0;
            $PageSysEventSettings= 0;
            $PageSysSportSettings= 0;
         
       
            switch ($CurrentPage) {
               
                    
                    // Management
                case "PageMgtMedalList":
                    $PageMgtList   = 1;
                    $PageMgtMedalList = 1;
                    break;

              // setting
                case "PageSysSettingGameSettings":
                    $PageSysGameSettings = 1;
                    $PageSysSetting = 1;
                    break;
                case "PageSysSettingEventSettings":
                    $PageSysEventSettings= 1;
                    $PageSysSetting = 1;
                    break;
                case "PageSysSettingSportSettings":
                    $PageSysSportSettings=1;
                    $PageSysSetting = 1;
                    break;
            }
            
            
//             if ($isClubPIC )
//             {
            $MenuArr["medallist_sys_management"] = array($MedalListMenu['medallist_sys_management'], "", $PageMgtMedalList);
            
            $MenuArr["medallist_sys_management"]["Child"]["medallist"] = array($MedalListMenu['medallist'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MedalList/management/index.php", $PageMgtList);
            
            $MenuArr["medallist_sys_setting"] = array($MedalListMenu['medallist_sys_setting'], "", $PageSysSetting);
           
            $MenuArr["medallist_sys_setting"]["Child"]["game"] = array($MedalListMenu['game'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MedalList/settings/game/index.php", $PageSysGameSettings);
            $MenuArr["medallist_sys_setting"]["Child"]["event"] = array($MedalListMenu['event'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MedalList/settings/event/event.php", $PageSysEventSettings);
            $MenuArr["medallist_sys_setting"]["Child"]["sport"] = array($MedalListMenu['sport'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MedalList/settings/sport/sport.php", $PageSysSportSettings);
//             }
            
            # module information
            $MedalListTitle = $Lang['Header']['Menu']['MedalList'];
            $MODULE_OBJ['title'] = $MedalListTitle;
            //                 $MODULE_OBJ['title'] = $eEnrollmentMenu['title'];
            
            $MODULE_OBJ['title_css'] = "menu_opened";
            $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_enrollment.gif";
          //  $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eService/enrollment/index.php";
            $MODULE_OBJ['menu'] = array();
            $MODULE_OBJ['menu'] = $MenuArr;
            
            return $MODULE_OBJ;
                
    }
    
    function hasAccessRight($ParUserID, $ParPageType='', $ParID='') {
        global $plugin, $PATH_WRT_ROOT, $Lang, $UserID;

        $CanAccessEnrollment = (!$plugin['eEnrollment'] || $this->Has_Trial_Period_Past())? false : true;
        
        $isEnrolAdmin = $this->IS_ENROL_ADMIN($_SESSION['UserID']);
       
        
        $CanAccessPage = true;
        
        if ($ParPageType == 'Admin') {
            $CanAccessPage = $isEnrolAdmin? true : false;
        }
        
        
        if (!$CanAccessEnrollment || !$CanAccessPage) {
            No_Access_Right_Pop_Up();
        }
        
        return true;
    }
    
    function getInUsedGames(){
       // global $plugin, $PATH_WRT_ROOT, $Lang, $UserID;
        $sql  = "SELECT GameID,
                 CONCAT('<a href=\"game_new.php?GameID=', GameID ,'\" class=\"tablelink\">', GameChiName, '</a>') as GameChiName,
                 CONCAT('<a href=\"game_new.php?GameID=', GameID ,'\" class=\"tablelink\">', GameEngName, '</a>') as GameEngName              
         FROM
                SFOC_GAMES_SETTING
         WHERE
                RecordStatus = '1'
        ";
        $ReturnArr = $this->returnResultSet($sql);
        return $ReturnArr;
    }
    
    function getGamesList(){
        // global $plugin, $PATH_WRT_ROOT, $Lang, $UserID;
        $GameName = Get_Lang_Selection('GameChiName','GameEngName');
        $sql  = "SELECT GameID,
                 $GameName
         FROM
                SFOC_GAMES_SETTING
         WHERE
                RecordStatus = '1'
        ";
        $ReturnArr = $this->returnArray($sql);
        return $ReturnArr;
    }

    
    function getGameInfoByID($GameID = ''){
        $sql  = "SELECT GameID,
                 GameChiName, 
                GameEngName
         FROM
                SFOC_GAMES_SETTING
         WHERE
                GameID = '{$GameID}'
        ";
        $ReturnArr = $this->returnResultSet($sql);
        return $ReturnArr[0];
    }
    
    
    function getGameInfoByConds($conds= ''){
        $sql  = "SELECT GameID                
                FROM
                    SFOC_GAMES_SETTING
                WHERE
                    1 $conds
        ";

        $ReturnArr = $this->returnVector($sql);
        return $ReturnArr[0];
    }
    
    function addNewGame($GameArr = ''){
        $GameArr = is_array($GameArr)? $GameArr: (array)$GameArr;
        $sql = "
                INSERT INTO SFOC_GAMES_SETTING 
                    (GameChiName ,
                    GameEngName ,
                    RecordStatus , 
                    InputBy ,
                    InputDate ,
                    ModifiedBy ,
                    ModifiedDate )
                
                Values 
                    ('{$GameArr['GameChiName']}', 
                    '{$GameArr['GameEngName']}', 
                    '1',
                    '{$_SESSION['UserID']}',
                    now(),
                    '{$_SESSION['UserID']}',
                    now()
                    )

        ";   
        
        $Result = $this->db_db_query($sql);
        return $Result;
    }
    function editGame($GameArr = ''){
        
        $GameArr = is_array($GameArr)? $GameArr: (array)$GameArr;
        $GameID = $GameArr['GameID'];
        $SQL  ="UPDATE SFOC_GAMES_SETTING  
                Set 
                    GameChiName = '{$GameArr['GameChiName']}',
                    GameEngName = '{$GameArr['GameEngName']}',
                    ModifiedBy = '{$_SESSION['UserID']}',
                    ModifiedDate = now()
                Where 
                    GameID = '$GameID' ";
        $Result = $this->db_db_query($SQL);
 
        return $Result;
        
    }
    
    function deleteGame($GameID = ''){
        $SQL  ="UPDATE SFOC_GAMES_SETTING
                Set
                    RecordStatus = '0',
                    ModifiedBy = '{$_SESSION['UserID']}',
                    ModifiedDate = now()
                 Where
                    GameID = '$GameID' ";
        $Result = $this->db_db_query($SQL);
        
        return $Result;
    }
    
    function getAllEvents(){
        $SQL  = "SELECT 
                    EventID ,
                    CONCAT('<a href=\"event_new.php?EventID=', EventID ,'\" class=\"tablelink\">', EventChiName, '</a>') as EventChiName,
                    CONCAT('<a href=\"event_new.php?EventID=', EventID ,'\" class=\"tablelink\">', EventEngName, '</a>') as EventEngName
                FROM 
                    SFOC_EVENTS_SETTING ";
        $ReturnArr = $this->returnResultSet($SQL);
        return $ReturnArr;
        
    }
    
    function getEventList($EventID = ''){
        if($EventID != ''){
            $conds = "AND EventID='$EventID'";
        }
        $EventName = Get_Lang_Selection('EventChiName','EventEngName');
        $sql  = "SELECT EventID,
                    $EventName
                 FROM
                    SFOC_EVENTS_SETTING WHERE 1 $conds";
        $ReturnArr = $this->returnArray($sql);
        return $ReturnArr;
        
    }
   
    function getEventIDByCondition($conds = ''){
        $sql  = "SELECT EventID
                 FROM
                    SFOC_EVENTS_SETTING 
                 WHERE 1 $conds";
     
        $ReturnArr = $this->returnVector($sql);
        return $ReturnArr[0];
    }
    
    function getEventInfoByID($EventID = ''){
        $sql  = "SELECT EventID,
                    EventChiName,
                    EventEngName
                 FROM
                    SFOC_EVENTS_SETTING
                 WHERE
                     EventID = '{$EventID}'
        ";
        $ReturnArr = $this->returnResultSet($sql);
        return $ReturnArr[0];    
    }
    
    function addNewEvent($EventArr = '',$getID = ''){
        $EventArr = is_array($EventArr)? $EventArr: (array)$EventArr;
     
        $sql = "
               INSERT INTO SFOC_EVENTS_SETTING
                    (EventChiName ,
                    EventEngName ,
                    InputBy ,
                    InputTime,
                    ModifiedBy ,
                    ModifiedTime)                    
               Values
                    ('{$EventArr['EventChiName']}',
                    '{$EventArr['EventEngName']}',
                    '{$_SESSION['UserID']}',
                    now(),
                    '{$_SESSION['UserID']}',
                    now()
                    )
                    
                    ";
        if($getID == ''){
            return $this->db_db_query($sql);
        }else{
            $this->db_db_query($sql);
            return $this->db_insert_id();	                 
        }
    }
    function editEvent($EventArr = ''){
        
        $EventArr = is_array($EventArr)? $EventArr: (array)$EventArr;
        $EventID = $EventArr['EventID'];
        $SQL  ="UPDATE 
                    SFOC_EVENTS_SETTING
                Set
                    EventChiName = '{$this->Get_Safe_Sql_Like_Query($EventArr['EventChiName'])}',
                    EventEngName = '{$this->Get_Safe_Sql_Like_Query($EventArr['EventEngName'])}',
                    ModifiedBy = '{$_SESSION['UserID']}',
                    ModifiedTime  = now()
                Where
                    EventID = '$EventID' ";
        $Result = $this->db_db_query($SQL);
        
        return $Result;
        
    }
    function deleteEvent($EventID = ''){
        $sql = "DELETE FROM SFOC_EVENTS_SETTING WHERE EventID = '{$EventID}' ";
        $Result = $this->db_db_query($sql);
        return $Result;
    }
    
    
    function getSportsList($SportID=''){
        if($SportID != ''){
            $conds  = "AND SportID = '$SportID'";
        }
        else{
            $conds = '';
        }
        $SportName = Get_Lang_Selection('SportChiName','SportEngName');
        $sql  = "SELECT SportID,
                    $SportName
                 FROM
                    SFOC_SPORTS_TYPE WHERE 1 $conds
                    AND Is_Deleted = 0";
        $ReturnArr = $this->returnArray($sql);
        return $ReturnArr;
    }
    function getSportIDByCondition($conds= ''){
        $sql  = "SELECT SportID        
                 FROM
                    SFOC_SPORTS_TYPE 
                 WHERE 1 $conds";
        $ReturnArr = $this->returnVector($sql);
        return $ReturnArr[0];  
        
    }
    
    function addNewSport($SportArr= ''){
        $SportArr= is_array($SportArr)? $SportArr: (array)$SportArr;
        $sql = "
               INSERT INTO SFOC_SPORTS_TYPE
                    (SportChiName ,
                    SportEngName
                   )
               Values
                  ('".$SportArr['SportChiName']."',
                  '".$SportArr['SportEngName']."'                   
                    ) ";

        $this->db_db_query($sql);       
        $Result = $this->db_insert_id($sql);
        return $Result;        
    }
    function getMedalListRecord(){
        
        $LocationTitle = Get_Lang_Selection('smlr.LocationChi','smlr.LocationEng');
        $GameName = Get_Lang_Selection('sgs.GameChiName','sgs.GameEngName');
        $EventName = Get_Lang_Selection('ses.EventChiName','ses.EventEngName');
        $SportName = Get_Lang_Selection('sst.SportChiName','sst.SportEngName');
        $AthleteName= Get_Lang_Selection('smlr.NameChi','smlr.NameEng');
        $sql = "SELECT 
                    smlr.RecordID,
                    smlr.Year,
                    $LocationTitle as LocationName,
                    $GameName as GameName,
                    $EventName as EventName,
                    $SportName as SportName,
                    $AthleteName as AthleteName                  
                FROM  SFOC_MEDAL_LIST_RECORD AS smlr               
                INNER JOIN SFOC_GAMES_SETTING AS sgs ON (smlr.GameID = sgs.GameID)
                INNER JOIN SFOC_EVENTS_SETTING AS ses ON (smlr.EventID = ses.EventID)
                INNER JOIN SFOC_SPORTS_TYPE AS sst ON (smlr.SportID = sst.SportID)";
          $ReturnArr = $this->returnResultSet($sql);
         
          return $ReturnArr;  
        
    }
    
    function getMedalListInfoByID($RecordID=''){
        

        $sql = "SELECT
                    smlr.RecordID,
                    smlr.Year,
                    smlr.LocationChi,
                    smlr.LocationEng,
                    smlr.GameID,
                    ses.EventID,
                    sst.SportID, 
                    smlr.NameChi,
                    smlr.NameEng,
                    smlr.Medal,
                    smlr.WebsiteCode    

                FROM  SFOC_MEDAL_LIST_RECORD AS smlr
                    INNER JOIN SFOC_GAMES_SETTING AS sgs ON (smlr.GameID = sgs.GameID)
                    INNER JOIN SFOC_EVENTS_SETTING AS ses ON (smlr.EventID = ses.EventID)
                    INNER JOIN SFOC_SPORTS_TYPE AS sst ON (smlr.SportID = sst.SportID)
                WHERE 
                    smlr.RecordID = '$RecordID'";
        $ReturnArr = $this->returnResultSet($sql);
        
        return $ReturnArr[0];
        
    }
    
    function addNewMedalListRecord($RecordArr = ''){
        
        $RecordArr= is_array($RecordArr)? $RecordArr: (array)$RecordArr;  
        
        $sql = "INSERT INTO SFOC_MEDAL_LIST_RECORD
                   (Year,
                    LocationChi ,
                    LocationEng ,
                    GameID,
                    EventID ,
                    SportID,
                    NameChi,
                    NameEng,
                    Medal,
                    InputTime,
                    ModifiedBy,
                    ModifiedTime,
                    WebsiteCode)
                Values
                    ('".$this->Get_Safe_Sql_Query($RecordArr['Year'])."',
                    '".$this->Get_Safe_Sql_Query($RecordArr['LocationChi'])."',
                    '".$this->Get_Safe_Sql_Query($RecordArr['LocationEng'])."',
                    '".$this->Get_Safe_Sql_Query($RecordArr['GameID'])."',
                    '".$this->Get_Safe_Sql_Query($RecordArr['EventID'])."',
                    '".$this->Get_Safe_Sql_Query($RecordArr['SportID'])."',
                    '".$this->Get_Safe_Sql_Query($RecordArr['NameOfAthleteChi'])."', 
                    '".$this->Get_Safe_Sql_Query($RecordArr['NameOfAthleteEng'])."',      
                    '".$this->Get_Safe_Sql_Query($RecordArr['MedalID'])."',                       
                    now(),
                    '{$_SESSION['UserID']}',
                    now(),
                    '".$this->Get_Safe_Sql_Query($RecordArr['WebsiteCode'])."')";
 
        $Result = $this->db_db_query($sql);
     
        return $Result;
        
    }
    
    function editMedalListRecord($RecordArr = ''){
        $RecordArr= is_array($RecordArr)? $RecordArr: (array)$RecordArr;
        $RecordID = $RecordArr['RecordID'];
        $SQL  ="UPDATE
                    SFOC_MEDAL_LIST_RECORD
                Set
                    Year = '{$RecordArr['Year']}',
                    LocationChi = '".$RecordArr['LocationChi']."',
                    LocationEng = '".$RecordArr['LocationEng']."',
                    GameID = '".$RecordArr['GameID']."',
                    EventID = '".$RecordArr['EventID']."',
                    SportID = '".$RecordArr['SportID']."',
                    NameChi = '".$RecordArr['NameOfAthleteChi']."',
                    NameEng = '".$RecordArr['NameOfAthleteEng']."',
                    Medal = '".$RecordArr['MedalID']."',
                    ModifiedBy = '{$_SESSION['UserID']}',
                    ModifiedTime  = now(),
                    WebsiteCode = '".$RecordArr['WebsiteCode']."'
                Where
                    RecordID = '$RecordID' ";
        $Result = $this->db_db_query($SQL);

        return $Result;
    }
    function deleteMedalListRecord($RecordID = ''){
        $sql = "DELETE FROM SFOC_MEDAL_LIST_RECORD WHERE RecordID = '{$RecordID}' ";
        $Result = $this->db_db_query($sql);
        return $Result;
    }
    

    function insertMeadalListRecordTemp($TempValueArr=''){
        $this->clearTempMeadalListRecord();
       
           
        foreach ($TempValueArr as $value)
        {            
            $valueArr[] = $this->Get_Safe_Sql_Query($value);
        }
        
        $valueText = implode(", ", $valueArr);
        
       
        $sql = '
        INSERT INTO TEMP_SFOC_MEDAL_LIST 
            (TempID, Year, LocationChi,LocationEng,GameID,EventID,SportID,NameChi,NameEng,Medal,InputBy,InputTime, WebsiteCode)
        VALUES
            '.$valueText;

        $success= $this->db_db_query($sql);
      
        if ($success)
        {
            $TempID = $this->db_insert_id();
            return $TempID;
        }
        else
        {
            return false;
        }
        
    }
    function clearTempMeadalListRecord(){
        $sql = "DELETE FROM TEMP_SFOC_MEDAL_LIST WHERE InputBy  = '".$_SESSION['UserID']."'  ";
        $result = $this->db_db_query($sql);
        return $result;
    }
    function getMedalListRecordTemp(){
        $sql="SELECT * FROM TEMP_SFOC_MEDAL_LIST WHERE InputBy  = '".$_SESSION['UserID']."' ";
        
        $result = $this->returnArray($sql);
        return $result;
    }
    
    // Philips edit area
    
    function insertEventListRecordTemp($TempValueArr=''){
        $this->clearTempEventListRecord();
        
        
        foreach ($TempValueArr as $value)
        {
            $valueArr[] = $this->Get_Safe_Sql_Query($value);
        }
        
        $valueText = implode(", ", $valueArr);
        
        
        $sql = '
        INSERT INTO TEMP_SFOC_EVENT_LIST
            (TempID,EventChiName, EventEngName,InputBy,InputTime)
        VALUES
            '.$valueText;
        
        $success= $this->db_db_query($sql);
        
        if ($success)
        {
            $TempID = $this->db_insert_id();
            return $TempID;
        }
        else
        {
            return false;
        }
        
    }
    function clearTempEventListRecord(){
        $sql = "DELETE FROM TEMP_SFOC_EVENT_LIST WHERE InputBy  = '".$_SESSION['UserID']."'  ";
        $result = $this->db_db_query($sql);
        return $result;
    }
    function getEventListRecordTemp(){
        $sql="SELECT * FROM TEMP_SFOC_EVENT_LIST WHERE InputBy  = '".$_SESSION['UserID']."' ";
        
        $result = $this->returnArray($sql);
        return $result;
    }
    
    function insertGameListRecordTemp($TempValueArr=''){
        $this->clearTempGameListRecord();
        
        
        foreach ($TempValueArr as $value)
        {
            $valueArr[] = $this->Get_Safe_Sql_Query($value);
        }
        
        $valueText = implode(", ", $valueArr);
        
        
        $sql = '
        INSERT INTO TEMP_SFOC_GAME_LIST
            (TempID,GameChiName, GameEngName,InputBy,InputTime)
        VALUES
            '.$valueText;
        
        $success= $this->db_db_query($sql);
        
        if ($success)
        {
            $TempID = $this->db_insert_id();
            return $TempID;
        }
        else
        {
            return false;
        }
        
    }
    function clearTempGameListRecord(){
        $sql = "DELETE FROM TEMP_SFOC_GAME_LIST WHERE InputBy  = '".$_SESSION['UserID']."'  ";
        $result = $this->db_db_query($sql);
        return $result;
    }
    function getGameListRecordTemp(){
        $sql="SELECT * FROM TEMP_SFOC_GAME_LIST WHERE InputBy  = '".$_SESSION['UserID']."' ";
        
        $result = $this->returnArray($sql);
        return $result;
    }
    
    function insertSportRecordTemp($TempValueArr=''){
        $this->clearTempSportRecord();
    
        foreach ($TempValueArr as $value)
        {
            $valueArr[] = $this->Get_Safe_Sql_Query($value);
        }
        
        $valueText = implode(", ", $valueArr);
        
        
        $sql = '
        INSERT INTO TEMP_SFOC_SPORT_LIST
            (TempID,SportChiName , SportEngName ,SportsIntroductionChi ,SportsIntroductionEng,InputBy,InputTime)
        VALUES
            '.$valueText;
        
        $success= $this->db_db_query($sql);
        
        if ($success)
        {
            $TempID = $this->db_insert_id();
            return $TempID;
        }
        else
        {
            return false;
        }
        
    }
    function clearTempSportRecord(){
        $sql = "DELETE FROM TEMP_SFOC_SPORT_LIST WHERE InputBy  = '".$_SESSION['UserID']."'  ";
        $result = $this->db_db_query($sql);
        return $result;
    }
    function getSportRecordTemp(){
        $sql="SELECT * FROM TEMP_SFOC_SPORT_LIST WHERE InputBy  = '".$_SESSION['UserID']."' ";
        
        $result = $this->returnArray($sql);
        return $result;
    }
    
    function getAllSport($fieldname = '', $deleted = 0){
        $defaultFieldNames = "SportID, SportChiName, SportEngName, IconPath, IntroductionChi, IntroductionEng, RulesChi, EquipmentEng, VenueChi, VenueEng, CourseInfoChi, CourseInfoEng";
        if($fieldname == ''){
            $fieldname = $defaultFieldNames;
        }
        if($deleted!=0){
            $showCond = 'OR Is_Deleted = 1';
        }
        $sql = "SELECT $fieldname
                FROM SFOC_SPORTS_TYPE
                WHERE Is_Deleted = 0
                $showCond
               ";
        $result = $this->returnArray($sql);
        return $result;
    }
    
    function getSport($sportID,$fieldname = ''){
        $defaultFieldNames = "SportID, SportChiName, SportEngName, IconPath, IntroductionChi, IntroductionEng, RulesChi, EquipmentEng, VenueChi, VenueEng, CourseInfoChi, CourseInfoEng";
        if($fieldname == ''){
            $fieldname = $defaultFieldNames;
        }
        $sql = "SELECT $fieldname
                FROM SFOC_SPORTS_TYPE
                WHERE SportID = '$sportID'";
        $result = $this->returnArray($sql);
        return $result;
    }
    
    function getSportID($isDeleted = 0){
        $fieldname = "SportID";
        if($isDeleted!=0){
            $showCond = 'OR Is_Deleted = 1';
        }
        $sql = "SELECT $fieldname
                FROM SFOC_SPORTS_TYPE
                WHERE Is_Deleted = 0
                $showCond";
        $result = $this->returnVector($sql);
        return $result;
    }
    
    function addSport($arr){
        $fieldname = '';
        $values = '';
        foreach($arr as $k => $v){
            $fieldname .= "$k,";
            $values .= "'$v',";
        }
        $fieldname = substr($fieldname, 0, -1);
        $values = substr($values, 0, -1);
        $sql = "INSERT INTO SFOC_SPORTS_TYPE
                ($fieldname)
                VALUES
                ($values)";
        $result = $this->db_db_query($sql);
        return $result;
    }
    
    function updateSport($arr){
        $Sport = $this->getSport($arr['SportID']);
        if(count($Sport)>0){
            $values = '';
            foreach($arr as $k => $v){
                if($k != 'SportID')
                    $values .= "$k = '$v',";
            }
            $values = substr($values, 0, -1);
            $sql = "UPDATE SFOC_SPORTS_TYPE
                    SET
                        $values
                    WHERE SportID = '$arr[SportID]'";
            $result = $this->db_db_query($sql);
            return result;
        } else {
            return false;
        }
    }
    
    function deleteSport($SportID){
        $sql = "UPDATE SFOC_SPORTS_TYPE
                SET Is_Deleted = 1
                WHERE SportID = '$SportID'";
        $result = $this->db_db_query($sql);
        return $result;
    }
    // Philips edit area
    
}

?>