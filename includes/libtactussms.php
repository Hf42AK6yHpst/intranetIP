<?php
if (!defined("LIBTACTUSSMS_DEFINED"))                     // Preprocessor directive
{
	define("LIBTACTUSSMS_DEFINED", true);


	include_once("$intranet_root/includes/libhttpclient.php");
	include_once("$intranet_root/includes/libwebmail.php");
	include_once("$intranet_root/lang/email.php");

	class libtactussms {

		var $service_host;
		var $service_port;
		var $service_action;
		var $data_action;
		var $usage_action;
		var $status_action;
		var $login;
		var $password;
		var $school_code;
		var $simulate_only;
		var $cancel_previous_action;
		var $start_transaction;
		var $end_transaction;
          var $countycode;

		function libtactussms ()
		{
			//assign variable only
			global $sms_tactus_host, $sms_tactus_port, $sms_tactus_username, $sms_tactus_password, $sms_tactus_api_send_request, $sms_tactus_dlrurl, $sms_tactus_simulate_only,$sms_tactus_from,$sms_tactus_countycode,$sms_tactus_usage;
			$this->service_host = $sms_tactus_host;
			$this->service_port = $sms_tactus_port;
			$this->login = $sms_tactus_username;
			$this->password = $sms_tactus_password;
			$this->from = $sms_tactus_from;
			$this->service_action = $sms_tactus_api_send_request;
			$this->dlrurl = $sms_tactus_dlrurl;
			$this->simulate_only = $sms_tactus_simulate_only;
			$this->countrycode = $sms_tactus_countycode;
			$this->usage_action= $sms_tactus_usage;
			
		}//end function  libtactussms ()


	function sms_unicode($message)
		{
			if (utf8_strlen($message)!=strlen($message))
			{
				# unicode or double bytes word exists
				if (function_exists('iconv')) {
				
					$latin = @iconv('UTF-8', 'ISO-8859-1', $message);
					
	    				//if (strcmp($latin, $message)) {
						$arr = @unpack('H*hex', @iconv('UTF-8', 'UCS-2BE', $message));
						return strtoupper($arr['hex']);
					//}
				} else
					die("unable to send SMS due to missing of PHP function - ICONV!");
			} else
			{
				//$latin = @iconv('UTF-8', 'ISO-8859-1', $message);
				return urlencode($message);
			}
			//return $message;
		}//end sms_unicode


		function big5_to_uf8_urlencode_msg($post_message)
		{
          // only use this line if IP20/EJ4 big5 platform
//			$post_message=iconv("BIG5", "UTF-8//IGNORE", $post_message);


			//$post_message=iconv("BIG5", "UTF-8//TRANSLIT", $message);
			//echo 'ASCII<br>'.$post_message.'<br>';
			//$post_message=utf8Encode2($post_message);
			$post_message=$this->sms_unicode($post_message);
			//debug($post_message);
			//die();
			//$post_message=urlencode($post_message);
			return $post_message; 
		}//end big5_to_uf8_urlencode_msg




		function sendSMS($recipient, $message, $message_code="", $delivery_time="", $isReplyMessage)
		{
			global $webmaster;

			# detect for correct coding!!! otherwise, may be wrong encoding or count message into 2 or more
			$service_action_final = (utf8_strlen($message)!=strlen($message)) ? $this->service_action : str_replace("gw-coding=3", "gw-coding=1", $this->service_action);
			
			//$message = stripslashes($message);
			$message = $this->big5_to_uf8_urlencode_msg($message);
			
			//put country code here
			$recipient=str_replace(',',','.$this->countrycode,$recipient);
			$recipient=$this->countrycode.$recipient;
			$post_data = "gw-username=".$this->login."&gw-password=".$this->password."&gw-to=".$recipient;
			$post_data .= "&gw-from=".$this->from."&gw-text=".$message."&gw-dlrurl=".$this->dlrurl;

			
			if ($this->simulate_only)
			{
				global $intranet_root;
				$logpath = "$intranet_root/file/tactus.send";
				$content = get_file_content($logpath);
				write_file_content ($content."\n".$post_data,$logpath);
				return "SIMULATE";
			}//end if
			else  //not simulate
			{
				# Try to connect to service host
				$lhttp = new libhttpclient($this->service_host,$this->service_port);
				if (!$lhttp->connect())
				{    # Exit if failed to connect
					return -99;
				}//eif
					
				//send sms (use post method not working for chinese?)
				/*
				 $lhttp->set_path($this->service_action);
				 $lhttp->post_data = $post_data;
				 $response = $lhttp->send_request();
				*/
				
				//send sms (use get method for chinese)
				//temp code for send
				
				$final_send_sms='http://'.$this->service_host.':'.$this->service_port.$service_action_final.$post_data;
				
				
                     //$contents=file_get_contents($final_send_sms);
                     //$contents=file_get_contents("http://202.76.237.69:11001/direct?gw-coding=3");
                     //$contents=file_get_contents('http://202.76.237.69:11001/direct?gw-coding=3');
                     
                     
                     $curl_handle=curl_init();
                     curl_setopt($curl_handle, CURLOPT_URL,$final_send_sms);
                     curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                     curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                     curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
                     $contents = curl_exec($curl_handle);
                     curl_close($curl_handle);




                     //print_r($contents);
                     //die();
                     
                     /*
                     codes for fopen
				$contents=file_get_contents('http://10.0.3.76/ks.cfg');
				$handle=fopen($final_send_sms,'r');
                    $contents = fread($handle, 8192);
                    $contents='test';
                    */


				# Put in log for debug
				global $intranet_root;
				write_file_content ($final_send_sms,"$intranet_root/file/tactus.send");
				write_file_content ($contents,"$intranet_root/file/tactus.log");


                    //test code in here!!
                    //able to get test code here
                    
                    
                    //count how many recepient here, if only one recepient, use this code
                    if(!strrpos($recipient,','))
                    {
                     //single msg
                     
                     $test_rec1['recipient']=$recipient;
                     $test_rec1['message_code']=$message_code;
                     
                     //get ref id
                     $content_array=explode('=',$contents);
                     $test_rec1['reference_id']=$content_array[2];
                     $records[0]=new tactusSMSRecord($test_rec1);
                     return $records;
                    
                    }
                    else
                    {
                    //generate record from contents
                    //only works if SMS >= 2 recepient
                    //extract output contents to
                    //msg id , #6
                    $content_array=explode('&',$contents);
                    $content_array_msgid=explode('>',$content_array[6]);

                    for($i=0;$i<sizeof($content_array_msgid);$i++)
                    {
					 	if (strpos($content_array_msgid[$i+1],'|')!==false)
					 	{
							$reference_id_array[$i]= substr($content_array_msgid[$i+1],0,strpos($content_array_msgid[$i+1],'|'));
						} else
						{
							$reference_id_array[$i]=substr($content_array_msgid[$i+1],0,36);
						}
                    }//end for
                    
                    //extract all msg code in $message_code
                    $msg_code_arrary=explode(',',$message_code);
                    $recipient_array=explode(',',$recipient);
                    
                    for($i=0;$i<sizeof($msg_code_arrary);$i++)
                    {
                     $test_rec0['recipient']=$recipient_array[$i];
                     $test_rec0['reference_id']=$reference_id_array[$i];
                     $test_rec0['message_code']=$msg_code_arrary[$i];
                     
                     $records[$i]=new tactusSMSRecord($test_rec0);
                    }//end for
                    }//end else
                    //print_r($records);
                    //die();
                    
                    /*
                    $test_rec0['recipient']='rec0';
                    $test_rec0['reference_id']=0;
                    $test_rec0['message_code']=$msg_code_arrary[0];
                    
                    
                    
                    $test_rec1['recipient']='rec1';
                    $test_rec1['reference_id']=1;
                    $test_rec1['message_code']=$msg_code_arrary[1];
                    
                    $records[0]=new tactusSMSRecord($test_rec0);
                    $records[1]=new tactusSMSRecord($test_rec1);
                    */
                    
				return $records;

			}//end else

		}//end function sendSMS

		#recipeintData : list of array(phone,message,message code,delivery time)
		#pre-condition: $isReplyMessage always = 0 for 1 way sms
		#post-condition: return record format (?)


          //this function use for file csv input bulk send only
		function sendBulkSMS($recipientData,$isReplyMessage)
		{
			if($recipientData=="" || sizeof($recipientData)<=0){
				return false;
			}
               // add debug mobile
			if (!$this->simulate_only){

				# Try to connect to service host
				$lhttp = new libhttpclient($this->service_host,$this->service_port);
				if (!$lhttp->connect()){    # Exit if failed to connect
					return -99;
				}
			}
			$records = array();

			for($i=0;$i<sizeof($recipientData);$i++){
				list($recipient, $message, $message_code,$delivery_time) = $recipientData[$i];
				
				# detect for correct coding!!! otherwise, may be wrong encoding or count message into 2 or more
				$service_action_final = (utf8_strlen($message)!=strlen($message)) ? $this->service_action : str_replace("gw-coding=3", "gw-coding=1", $this->service_action);
				
				
				//$message = stripslashes($message);
				$message = $this->big5_to_uf8_urlencode_msg($message);

                    //put country code here
			     $recipient=str_replace(',',','.$this->countrycode,$recipient);
                    $recipient=$this->countrycode.$recipient;
			
				$post_data = "gw-username=".$this->login . "&gw-password=".$this->password."&gw-to=".$recipient;
				$post_data .= "&gw-from=".$this->from."&gw-text=".$message."&gw-dlrurl=".$this->dlrurl;

				
				
				if ($this->simulate_only)
				{
					global $intranet_root;
					$logpath = "$intranet_root/file/tactus.send";
					$content = get_file_content($logpath);
					write_file_content ($content."\n".$post_data,$logpath);

					// return "SIMULATE";
					$records[] = "SIMULATE";
				}
				else
				{
                          $final_send_sms='http://'.$this->service_host.':'.$this->service_port.$service_action_final.$post_data;


                     $curl_handle=curl_init();
                     curl_setopt($curl_handle, CURLOPT_URL,$final_send_sms);
                     curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                     curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                     curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
                     $contents = curl_exec($curl_handle);
                     curl_close($curl_handle);

                     //FIX no reference id on first submit SMS to SMS gateway
                     if (strpos($contents,'msgid')<1)
                     {
                        $s=system("echo '".$contents."====No reference id from gateway====SMS RESEND!!=====\n' >> "."$intranet_root/file/tactus.log",$return_var);
                        //pause 2 seconds
                        sleep(2);
                        $curl_handle=curl_init();
                        curl_setopt($curl_handle, CURLOPT_URL,$final_send_sms);
                        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 10);
                        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
                        $contents = curl_exec($curl_handle);
                        curl_close($curl_handle);
                      }//end if                     

				# Put in log for debug
				global $intranet_root;
				$s=system("echo '".$final_send_sms."=====SMS END===========\n' >> "."$intranet_root/file/tactus.send",$return_var);
				$s=system("echo '".$contents."=====SMS END===========\n' >> "."$intranet_root/file/tactus.log",$return_var);

                     $test_rec1['recipient']=$recipient;
                     $test_rec1['message_code']=$message_code;
		           //get ref id
                     $content_array=explode('=',$contents);
                     //skip next split SMS reference id
                     $reference_id_arr=explode('&',$content_array[2]);
                     $test_rec1['reference_id']=$reference_id_arr[0];

                     //orginal ref id
                     //$test_rec1['reference_id']=$content_array[2];

                     $records[$i]=new tactusSMSRecord($test_rec1);
                      

	

				
			
				}//end else
			}//end for
         
      
			return $records;
		}//end function

			



		//data format from report page call from smsv2/usages2/refresh.php
		//date("Y-m-d H:i:s", mktime(0, 0, 0, $month, 1, $year));
		//return value: total messages send out on this month (local + international)
		function retrieveMessageCount($start, $end)
		{
		global $intranet_root;
		//trim  start and end timestamp
		$startdate=substr($start,0,10);
		$enddate=substr($end,0,10);
		$target_url = $this->usage_action."?school_id=".$this->login."&from_date=".$startdate."&to_date=".$enddate;
		//echo $target_url.'<br>';

		$fp =@fopen($target_url,"r");

		# Server Down
		if (!$fp)
		{
			return 0;
		}
		else
		{
              while (!feof($fp))
               {
               $data .= fread($fp, 8192);
               }
		}
		fclose($fp);
		//print_r($target_url);
          //trim first line result only
          
		$dataarrary=explode("\n",$data);
		$dataarrary[0]=trim($dataarrary[0]);
		return $dataarrary[0];
					
		//list($total_sms_sent,$local_sms_sent,$inti_sms_sent,$fail_local_sms,$fail_inti_sms) = split(",",$data[0]);
			

		//return trim($total_sms_sent);

		return 0;
		}

		


		//pre-condition: reference id array
		//post-condition: return array with status
		//BUG: not finish coding

		/* disable all function not relate send */

function retrieveSMSStatus($RecipientPhoneNumber_Arr,$refid_array)
{

         //echo "libtactus retrieveSMSStatus".sizeof($RecipientPhoneNumber_Arr).sizeof($refid_array);
         //die();

  for($i=0;$i<sizeof($refid_array);$i++)
  {
      $content="http://".$this->service_host.":".$this->service_port.$this->service_action."gw-username=".$this->login."&gw-password=".$this->password."&gw-from=".$this->from."&gw-to=".$this->countrycode.$RecipientPhoneNumber_Arr[$i]."&gw-msgid=".$refid_array[$i];
	 $curl_handle=curl_init();

      curl_setopt($curl_handle, CURLOPT_URL,$content);
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
      $contents = curl_exec($curl_handle);
      curl_close($curl_handle);

      $content_array=explode('&',$contents);
      $gw_dlr_status=explode('=',$content_array[2]);
      $gw_msgid=explode('=',$content_array[3]);

      //dlr is empty (gw-err-msg=msg_id_not_found) if record is outdated, do not write it back to database
      //echo $gw_dlr_status[1];
      //echo $gw_msgid[1];

      $test_rec1['gw_dlr_status']=trim($gw_dlr_status[1]);
      #adjustment for status mapping (20111031 according to spec. 3.5 of Tactus)
	  if ($test_rec1['gw_dlr_status']=="DELIVRD")
	  {
	  	$test_rec1['gw_dlr_status'] = 1;
	  } elseif ($test_rec1['gw_dlr_status']=="UNDELIV" || $test_rec1['gw_dlr_status']=="EXPIRED")
	  {
	  	$test_rec1['gw_dlr_status'] = 2;
	  }
      $test_rec1['gw_msgid']=$gw_msgid[1];


      $response[$i]=new tactusSMSStatus($test_rec1);
   //}

   }//end for
   return $response;

}//end Refresh_SMS_Status



		/*

		function XML2Obj ($xDoc)
		{
		$data = $xDoc;
		$parser = xml_parser_create();
		xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
		xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
		xml_parse_into_struct($parser,$data,$values,$tags);
		xml_parser_free($parser);

		// loop through the structures
		foreach ($tags as $key=>$val)
		{
		if ($key == "item")   # Specified in KanHan Document, each record is inside tag <msg>
		{
		$recordranges = $val;
		// each contiguous pair of array entries are the
		// lower and upper range for each record definition
		for ($i=0; $i < count($recordranges); $i+=2)
		{
		$offset = $recordranges[$i] + 1;
		$len = $recordranges[$i + 1] - $offset;
		$tdb[] = parseRecord(array_slice($values, $offset, $len));
		}
		}
		else
		{
		continue;
		}
		}
		return $tdb;

		}

		*/

		/*
		 function parseRecord($mvalues) {
		 for ($i=0; $i < count($mvalues); $i++)
		 $mol[$mvalues[$i]["tag"]] = $mvalues[$i]["value"];
		 return new ctmSMSRecord($mol);
		 }
		 */

	}//end class
	
# Data Structure only
class tactusSMSRecord
{
      var $recipient;
      var $reference_id;
      var $message_code;

      function tactusSMSRecord($aa)
      {
               foreach ($aa as $k=>$v)
                        $this->$k = $aa[$k];
      }
}//end eclass


class tactusSMSStatus
{
      var $gw_dlr_status;
      var $gw_msgid;

      function tactusSMSStatus($aa)
      {
               foreach ($aa as $k=>$v)
                        $this->$k = $aa[$k];
      }
}//end eclass

}//end define if
?>