<?php
// editing by 

$PowerPortfolioConfig = array();
$PowerPortfolioConfig['moduleCode'] = 'PowerPortfolio';
$PowerPortfolioConfig['eAdminPath'] = 'home/eAdmin/StudentMgmt/PowerPortfolio';
$PowerPortfolioConfig['taskSeparator'] = '.';

$PowerPortfolioConfig['jsFilePath'] = '/templates/2009a/js/PowerPortfolio';
$PowerPortfolioConfig['cssFilePath'] = '/templates/2009a/css/PowerPortfolio';
$PowerPortfolioConfig['imageFilePath'] = '/images/2009a/PowerPortfolio';
$PowerPortfolioConfig['fontFilePath'] = '/templates/2009a/font/PowerPortfolio';

$PowerPortfolioConfig['formLevel'] = array('K1', 'K2', 'K3');
//$PowerPortfolioConfig['subjectCode'] = array('165','JCHI01');
$PowerPortfolioConfig['subjectCode'] = array('S10','S08');
$PowerPortfolioConfig['indicatorsCatCode'] = array('EN','PTH');
//$PowerPortfolioConfig['subjectIndicatorsCodeMap'] = array('165'=>'EN','JCHI01'=>'PTH');
$PowerPortfolioConfig['subjectIndicatorsCodeMap'] = array('S10'=>'EN','S08'=>'PTH');

$PowerPortfolioConfig['awardType'] = array('Generate' => 'G', 'Input' => 'I');

$PowerPortfolioConfig['Levels'] = array(2, 3);
$PowerPortfolioConfig['GradeNum'] = array(3, 4, 5);
$PowerPortfolioConfig['ExamGradeNum'] = array(3, 4, 5);

$PowerPortfolioConfig['CommentMaxLength'] = 200;
?>