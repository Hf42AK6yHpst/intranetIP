<?php
// Using:
/* 
 * Modification log:
 *
 */

if (!defined("LIBPOWERPORTFOLIO_DEFINED"))
{
	# Define variable
	define("LIBPOWERPORTFOLIO_DEFINED", true);
	/*
 	define("TAIWAN_ABILITY", 0);
	define("MACAU_ABILITY", 1);
	define("INPUT_SCORE_INCOMPLETE", 0);
 	define("INPUT_SCORE_COMPLETE", 1);
 	define("STUDENT_NOT_IN_ZONE", 0);
	define("STUDNET_IN_ZONE", 1);
 	define("STUDNET_INCOMPLETE", 2);
	define("STUDENT_COMPLETE", 3);
 	define("TOPIC_CAT_SUBJECT", 1);
	define("TOPIC_CAT_PERSONAL", 2);
 	*/

	class libpowerportfolio extends libdb
	{
		/**
		 * Constructor 
		 */
		function libpowerportfolio($parAcademicYearId='')
		{
			global $PATH_WRT_ROOT, $intranet_root, $PowerPortfolioConfig;
			
			$this->libdb();
			
			$this->ModuleName = $PowerPortfolioConfig['moduleCode'];
			$this->ModuleDocumentRoot = $PATH_WRT_ROOT.$PowerPortfolioConfig['eAdminPath'];

			$this->AcademicYearID = $parAcademicYearId == '' ? $this->Get_Active_AcademicYearID() : $parAcademicYearId;
			$this->schoolYearName = $this->Get_Active_AcademicYearName();
			$this->DBName = $this->Get_Database_Name($this->AcademicYearID);
			
			$this->SchoolCode = $ercKindergartenCustomSchoolName ? $ercKindergartenCustomSchoolName : 'general';
			
			$this->configFilesType = array('summary', 'award');
			$this->configFilesPath = $intranet_root."/home/eAdmin/StudentMgmt/PowerPortfolio/other_info_config/".$this->SchoolCode."/";
			$this->dataFilesPath = $intranet_root."/file/power_portfolio/".$this->schoolYearName."/";
		}
		
		function Get_Module_Obj_Arr()
		{
			global $PATH_WRT_ROOT, $intranet_root, $image_path, $LAYOUT_SKIN, $CurrentPage;
			global $plugin, $special_feature, $PowerPortfolioConfig, $indexVar, $sys_custom;
			global $intranet_session_language, $Lang;
	        
	        
	        # Page - Menu & Function
			$currentPageAry = explode($PowerPortfolioConfig['taskSeparator'], $CurrentPage);
			$pageFunction = $currentPageAry[0];
			$pageMenu = $currentPageAry[0].$PowerPortfolioConfig['taskSeparator'].$currentPageAry[1];
			
			if($this->IS_POWER_PORTFOLIO_ADMIN_USER(true) || $this->IS_KG_CLASS_TEACHER() /*|| $this->IS_KG_ADMIN_GROUP_MEMBER()*/)
			{
			    $role_access_page_arr = array();

			    $isAdmin = $this->IS_POWER_PORTFOLIO_ADMIN_USER(true);
			    $isAdminOrClassTeacher = $this->IS_POWER_PORTFOLIO_ADMIN_USER(true) || $this->IS_KG_CLASS_TEACHER();
                //$isAdminOrClassOrSubjectTeacher = $this->IS_POWER_PORTFOLIO_ADMIN_USER(true) || $this->IS_KG_CLASS_TEACHER() || $this->IS_KG_SUBJECT_TEACHER();

    		    # Management
    			$MenuArr["Management"] = array($Lang['Menu']['AccountMgmt']['Management'], "#", ($pageFunction=='mgmt'));

                /*
                $menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'view_edit_period_setting'.$PowerPortfolioConfig['taskSeparator'].'list';
                $MenuArr["Management"]["Child"]["ViewEditTimeSetting"] = array($Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdmin) {
                    $role_access_page_arr[] = $menuTask;
                }

                $menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'topic_timetable'.$PowerPortfolioConfig['taskSeparator'].'list';
                $MenuArr["Management"]["Child"]["TopicTimetable"] = array($Lang['eReportCardKG']['Management']['TopicTimeTable']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'tool_score'.$PowerPortfolioConfig['taskSeparator'].'list';
    			$MenuArr["Management"]["Child"]["ToolScore"] = array($Lang['eReportCardKG']['Management']['ToolScore']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'subject_topic_score'.$PowerPortfolioConfig['taskSeparator'].'list';
    			$MenuArr["Management"]["Child"]["SubjectIndexScore"] = array($Lang['eReportCardKG']['Management']['SubjectIndexScore']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassOrSubjectTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'language_behavior_score'.$PowerPortfolioConfig['taskSeparator'].'list';
    			$MenuArr["Management"]["Child"]["LanguageBehavior"] = array($Lang['eReportCardKG']['Management']['LanguageBehavior']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassOrSubjectTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'ability_grade_remark'.$PowerPortfolioConfig['taskSeparator'].'list';
    			$MenuArr["Management"]["Child"]["remark"] = array($Lang['eReportCardKG']['Management']['AbilityRemarks']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'ability_remark_selection'.$PowerPortfolioConfig['taskSeparator'].'list';
    			$MenuArr["Management"]["Child"]["selection"] = array($Lang['eReportCardKG']['Management']['AbilityRemarksSelections']['ModuleTitle'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'award_generation'.$PowerPortfolioConfig['taskSeparator'].'index';
    			$MenuArr["Management"]["Child"]["AwardGeneration"] = array($Lang['eReportCardKG']['Management']['AwardGeneration']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'other_info'.$PowerPortfolioConfig['taskSeparator'].'index';
    			$MenuArr["Management"]["Child"]["OtherInfo"] = array($Lang['eReportCardKG']['Management']['OtherInfo']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'generate_reports'.$PowerPortfolioConfig['taskSeparator'].'index';
    			$MenuArr["Management"]["Child"]["GenerateReports"] = array($Lang['eReportCardKG']['Management']['GenerateReports']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'export_performance'.$PowerPortfolioConfig['taskSeparator'].'index';
    			$MenuArr["Management"]["Child"]["ExportPerformance"] = array($Lang['eReportCardKG']['Management']['ExportPerformance']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

                $menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'archive_reports'.$PowerPortfolioConfig['taskSeparator'].'index';
                $MenuArr["Management"]["Child"]["PrintArchiveReport"] = array($Lang['eReportCardKG']['Management']['PrintArchiveReport']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

                $menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'generate_archive_reports'.$PowerPortfolioConfig['taskSeparator'].'index';
                $MenuArr["Management"]["Child"]["ArchiveReport"] = array($Lang['eReportCardKG']['Management']['ArchiveReport']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdmin) {
                    $role_access_page_arr[] = $menuTask;
                }
                */

    			$menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'data_handling'.$PowerPortfolioConfig['taskSeparator'].'transition';
                if($isAdmin) {
    			$MenuArr["Management"]["Child"]["DataTransition"] = array($Lang['PowerPortfolio']['Management']['DataTransition']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                    $role_access_page_arr[] = $menuTask;
                }
                
                $menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'input_score'.$PowerPortfolioConfig['taskSeparator'].'index_class';
                if($isAdminOrClassTeacher) {
                $MenuArr["Management"]["Child"]["InputScore"] = array($Lang['PowerPortfolio']['Management']['InputScore']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                	$role_access_page_arr[] = $menuTask;
                }
                
                $menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'activity'.$PowerPortfolioConfig['taskSeparator'].'index_class';
                if($isAdminOrClassTeacher) {
                $MenuArr["Management"]["Child"]["Activity"] = array($Lang['PowerPortfolio']['Management']['Activity']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                	$role_access_page_arr[] = $menuTask;
                }
                
                if(empty($MenuArr["Management"]["Child"])){
                	unset($MenuArr["Management"]);
                }

                # Reports
                $MenuArr["Reports"] = array($Lang['Menu']['AccountMgmt']['Reports'], "#", ($pageFunction=='reports'));
                $menuTask = 'reports'.$PowerPortfolioConfig['taskSeparator'].'ca_report'.$PowerPortfolioConfig['taskSeparator'].'index';
                if($isAdmin) {
                	$MenuArr["Reports"]["Child"]["CAReport"] = array($Lang['PowerPortfolio']['Reprots']['CAReport']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                	$role_access_page_arr[] = $menuTask;
                }
                
                if(empty($MenuArr["Reports"]["Child"])){
                	unset($MenuArr["Reports"]);
                }

                /*
    			# Tablet Interface
    			$MenuArr["Lesson"] = array($Lang['eReportCardKG']['Management']['Device']['Title'], "#", ($pageFunction=='lesson'));

    			$menuTask = 'lesson'.$PowerPortfolioConfig['taskSeparator'].'lesson_student_view'.$PowerPortfolioConfig['taskSeparator'].'index';
    			$MenuArr["Lesson"]["Child"]["LearningZone"] = array($Lang['eReportCardKG']['Management']['Device']['LearningZone'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'lesson'.$PowerPortfolioConfig['taskSeparator'].'lesson_teacher_view'.$PowerPortfolioConfig['taskSeparator'].'index';
    			$MenuArr["Lesson"]["Child"]["InputScore"] = array($Lang['eReportCardKG']['Management']['Device']['InputScore'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }
    			*/

    		    # Settings
    			$MenuArr["Settings"] = array($Lang['Menu']['AccountMgmt']['Settings'], "#", ($pageFunction=='settings'));

    			$menuTask = 'settings'.$PowerPortfolioConfig['taskSeparator'].'rubrics'.$PowerPortfolioConfig['taskSeparator'].'list';
                if($isAdmin) {
    			$MenuArr["Settings"]["Child"]["Rubrics"] = array($Lang['PowerPortfolio']['Settings']['Rubrics']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'settings'.$PowerPortfolioConfig['taskSeparator'].'topic'.$PowerPortfolioConfig['taskSeparator'].'list';
                if($isAdmin) {
    			$MenuArr["Settings"]["Child"]["Topic"] = array($Lang['PowerPortfolio']['Settings']['Topic']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'settings'.$PowerPortfolioConfig['taskSeparator'].'activity'.$PowerPortfolioConfig['taskSeparator'].'list';
                if($isAdmin) {
    				$MenuArr["Settings"]["Child"]["Activity"] = array($Lang['PowerPortfolio']['Settings']['Activity']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                    $role_access_page_arr[] = $menuTask;
                }


	                $menuTask = 'settings'.$PowerPortfolioConfig['taskSeparator'].'zone'.$PowerPortfolioConfig['taskSeparator'].'list';
                if($isAdmin && $sys_custom['PowerPortfolio']['Settings']['Zone']) {
	                $MenuArr["Settings"]["Child"]["Zone"] = array($Lang['PowerPortfolio']['Settings']['Zone']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
	                    $role_access_page_arr[] = $menuTask;
	                }

	    			$menuTask = 'settings'.$PowerPortfolioConfig['taskSeparator'].'tool'.$PowerPortfolioConfig['taskSeparator'].'list';
                if($isAdmin && $sys_custom['PowerPortfolio']['Settings']['Tool']) {
	    			$MenuArr["Settings"]["Child"]["Tool"] = array($Lang['PowerPortfolio']['Settings']['Tool']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
	                    $role_access_page_arr[] = $menuTask;
	                }

    			$menuTask = 'settings'.$PowerPortfolioConfig['taskSeparator'].'comment'.$PowerPortfolioConfig['taskSeparator'].'list';
                if($isAdmin) {
    				$MenuArr["Settings"]["Child"]["Comment"] = array($Lang['PowerPortfolio']['Settings']['Comment']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'settings'.$PowerPortfolioConfig['taskSeparator'].'report'.$PowerPortfolioConfig['taskSeparator'].'list';
                if($isAdmin) {
    				$MenuArr["Settings"]["Child"]["Report"] = array($Lang['PowerPortfolio']['Settings']['Report']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                    $role_access_page_arr[] = $menuTask;
                }
                
                if(empty($MenuArr["Settings"]["Child"])){
                	unset($MenuArr["Settings"]);
                }
    			/*
    			$menuTask = 'settings'.$PowerPortfolioConfig['taskSeparator'].'langbehavior_item'.$PowerPortfolioConfig['taskSeparator'].'list';
    			$MenuArr["Settings"]["Child"]["LanguageBehaviorItem"] = array($Lang['eReportCardKG']['Setting']['LanguageBehaviorItem']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'settings'.$PowerPortfolioConfig['taskSeparator'].'award_list'.$PowerPortfolioConfig['taskSeparator'].'list';
    			$MenuArr["Settings"]["Child"]["AwardsList"] = array($Lang['eReportCardKG']['Setting']['AwardsList']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdminOrClassTeacher) {
                    $role_access_page_arr[] = $menuTask;
                }

    			$menuTask = 'settings'.$PowerPortfolioConfig['taskSeparator'].'admin_group'.$PowerPortfolioConfig['taskSeparator'].'list';
    			$MenuArr["Settings"]["Child"]["AdminGroup"] = array($Lang['eReportCardKG']['Setting']['AdminGroup']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                if($isAdmin) {
                    $role_access_page_arr[] = $menuTask;
                }

    			// Admin Group Access Right
    			if(!$isAdmin)
    			{
    			    $UserAccess = $this->Get_User_Access_Right($_SESSION['UserID']);
    			    foreach((array)$MenuArr as $cat => $SubMenuArr){
    			        foreach((array)$SubMenuArr['Child'] as $key => $SubMenuItem){
    			            $thisLink = $SubMenuItem[1];
    			            $thisLink = str_replace('?task=', '', $thisLink);
    			            $LinkStructureAry = explode('.', $thisLink);

    			            $thisPath = $LinkStructureAry[0].$PowerPortfolioConfig['taskSeparator'].$LinkStructureAry[1];
    			            if(!in_array($thisPath, (array)$UserAccess) && !in_array($thisLink, (array)$role_access_page_arr)){
    			                unset($MenuArr[$cat]['Child'][$key]);
    			            }
    			        }
                        if(empty($MenuArr[$cat]['Child'])){
                            unset($MenuArr[$cat]);
                        }
    			    }
    			}
			}
			else if($this->IS_KG_SUBJECT_TEACHER())
			{
			    # Management
			    $MenuArr["Management"] = array($Lang['Menu']['AccountMgmt']['Management'], "#", ($pageFunction=='mgmt'));
			    
			    $menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'subject_topic_score'.$PowerPortfolioConfig['taskSeparator'].'list';
			    $MenuArr["Management"]["Child"]["SubjectIndexScore"] = array($Lang['eReportCardKG']['Management']['SubjectIndexScore']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			    
			    $menuTask = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'language_behavior_score'.$PowerPortfolioConfig['taskSeparator'].'list';
			    $MenuArr["Management"]["Child"]["LanguageBehavior"] = array($Lang['eReportCardKG']['Setting']['LanguageBehavior']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			    
			    # Settings
			    $MenuArr["Settings"] = array($Lang['Menu']['AccountMgmt']['Settings'], "#", ($pageFunction=='settings'));
			    
			    $menuTask = 'settings'.$PowerPortfolioConfig['taskSeparator'].'subject_mapping'.$PowerPortfolioConfig['taskSeparator'].'list';
			    $MenuArr["Settings"]["Child"]["subjectMapping"] = array($Lang['eReportCardKG']['Setting']['SubjectMapping']['Title'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                */
			}

			### Module Information
			$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT.$PowerPortfolioConfig['eAdminPath'];
	        $MODULE_OBJ['title'] = $Lang['PowerPortfolio']['Title'];
	        $MODULE_OBJ['title_css'] = "menu_opened";
	        $MODULE_OBJ['logo'] = "{$image_path}/kis/module_icon/icon_powerportfolio.png";
	        $MODULE_OBJ['menu'] = $MenuArr;
	        
	        return $MODULE_OBJ;
		}
		
		function Check_Access_Right($task)
		{
		    global $PowerPortfolioConfig;

            /*
            # PARENT
            // 1. Management > Print Report Card
            if($this->IS_KG_PARENT_ACCESS())
            {
                $taskStructureAry = explode($PowerPortfolioConfig['taskSeparator'], $task);
                if($taskStructureAry[1] != 'generate_reports'){
                    No_Access_Right_Pop_Up();
                    return false;
                }else{
                    return true;
                }
                exit;
            }
            */

		    # ADMIN
            if($this->IS_POWER_PORTFOLIO_ADMIN_USER(true))
            {
                return true;
            }
            else
            {
                return false;
            }
            exit;

            /*
		    # ADMIN GROUP MEMBER
		    if($this->IS_KG_ADMIN_GROUP_MEMBER())
		    {
		        $UserAccess = $this->Get_User_Access_Right($_SESSION['UserID']);
		        $taskStructureAry = explode($PowerPortfolioConfig['taskSeparator'], $task);
		        $taskName = $taskStructureAry[0] . $PowerPortfolioConfig['taskSeparator'] . $taskStructureAry[1];
		        if(in_array($taskName, $UserAccess)){
		            return true;
		            exit;
		        }
		    }

		    # for CLASS TEACHER & SUBJECT TEACHER only
		    if(!$this->IS_KG_CLASS_TEACHER() && !$this->IS_KG_SUBJECT_TEACHER())
		    {
		        No_Access_Right_Pop_Up();
		        return false;
                exit;
		    }

		    # CLASS TEACHER (NOT ALLOW ACCESS)
            // 1. Management > Schedule
            // 2. Management > Data Handling
            // 3. Settings > Admin Group
            if($this->IS_KG_CLASS_TEACHER())
            {
                $taskStructureAry = explode($PowerPortfolioConfig['taskSeparator'], $task);
                if($taskStructureAry[1] == 'view_edit_period_setting' || $taskStructureAry[1] == 'data_handling' || $taskStructureAry[1] == 'admin_group'){
                    No_Access_Right_Pop_Up();
                    return false;
                    exit;
                }
            }

		    # SUBJECT TEACHER
            // 1. Management > Input Score Revision (Subjects' Ability Targets)
            // 2. Management > Language Proficiency and Behavioural Indicators
            // 3. Settings > Subjects' Ability Targets
		    if(!$this->IS_KG_CLASS_TEACHER() && $this->IS_KG_SUBJECT_TEACHER())
            {
                $taskStructureAry = explode($PowerPortfolioConfig['taskSeparator'], $task);
		        if($taskStructureAry[1] != 'subject_topic_score' && $taskStructureAry[1] != 'language_behavior_score' && $taskStructureAry[1] != 'subject_mapping'){
                    No_Access_Right_Pop_Up();
                    return false;
                    exit;
                }
		    }
		    
			return true;
            */
		}
		
		function Get_User_Default_Page()
		{
		    global $PowerPortfolioConfig;

            if($this->IS_POWER_PORTFOLIO_ADMIN_USER(true))
            {
		        return 'settings'.$PowerPortfolioConfig['taskSeparator'].'rubrics'.$PowerPortfolioConfig['taskSeparator'].'list';
		        exit;
            }

		    /*
		    if($this->IS_POWER_PORTFOLIO_ADMIN_USER(true) || $this->IS_KG_CLASS_TEACHER()){
                return 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'topic_timetable'.$PowerPortfolioConfig['taskSeparator'].'list';
		    }else if($this->IS_KG_ADMIN_GROUP_MEMBER()){
		        $UserAccess = $this->Get_User_Access_Right($_SESSION['UserID']);
		        $tail = 'list';
		        if(substr($UserAccess[0],0,6)=='lesson'){
		            $tail = 'index';
		        }
		        if(strpos($UserAccess[0],'other_info') > -1){
		            $tail = 'index';
		        }
		        if(strpos($UserAccess[0],'generate_reports') > -1){
		            $tail = 'index';
		        }
		        if(strpos($UserAccess[0],'export_performance') > -1){
		            $tail = 'index';
		        }
		        if(strpos($UserAccess[0],'data_handling') > -1){
		            $tail = 'transition';
		        }
		        return $UserAccess[0]. $PowerPortfolioConfig['taskSeparator'] . $tail;
		    }else if($this->IS_KG_SUBJECT_TEACHER()){
		        return 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'language_behavior_score'.$PowerPortfolioConfig['taskSeparator'].'list';
		    }else if($_SESSION['UserType']==USERTYPE_PARENT){
		        return 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'generate_reports'.$PowerPortfolioConfig['taskSeparator'].'index';
		    }
		    */
		    
		    No_Access_Right_Pop_Up();
		    return false;
		}
		
		function IS_POWER_PORTFOLIO_ADMIN_USER($isModuleAdminOnly=false)
        {
            /*
		    if(!$isModuleAdminOnly && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-PowerPortfolio"] && $this->IS_POWER_PORTFOLIO_ADMIN_GROUP_MEMBER())
            {
                global $PowerPortfolioConfig, $indexVar;

                $taskStructureAry = explode($PowerPortfolioConfig['taskSeparator'], $indexVar['task']);
                $taskName = $taskStructureAry[0] . $PowerPortfolioConfig['taskSeparator'] . $taskStructureAry[1];

                // has access right > same as module admin
                $UserAccess = $this->Get_User_Access_Right($_SESSION['UserID']);
                if(in_array($taskName, $UserAccess)){
                    return true;
                    exit;
                }
            }
            */

			return $_SESSION["SSV_USER_ACCESS"]["eAdmin-PowerPortfolio"];
		}
		
		function IS_KG_CLASS_TEACHER() {
		    return $_SESSION["SSV_PRIVILEGE"]["power_portfolio"]["is_class_teacher"];
		}
		
		function IS_KG_SUBJECT_TEACHER() {
		    return $_SESSION["SSV_PRIVILEGE"]["power_portfolio"]["is_subject_teacher"];
		}
		
		function IS_KG_PARENT_ACCESS() {
		    return $_SESSION['UserType']==USERTYPE_PARENT && !$this->IS_POWER_PORTFOLIO_ADMIN_USER();
		}
		
		function IS_POWER_PORTFOLIO_ADMIN_GROUP_MEMBER(){
		    $UserAccess = $this->Get_User_Access_Right($_SESSION['UserID']);
		    return !empty($UserAccess);
		}
		
		function IS_INPUT_LOCKED() {
		    return !$this->IS_POWER_PORTFOLIO_ADMIN_USER();
		}
		
		function Get_All_AcademicYearID() {
			$targetNameField = Get_Lang_Selection("YearNameB5", "YearNameEN");
			$sql = "SELECT 
						ay.AcademicYearID,
						ay.".$targetNameField." as AcademicYearName
					FROM
						ACADEMIC_YEAR ay
					INNER JOIN
						ACADEMIC_YEAR_TERM ayt ON (ay.AcademicYearID = ayt.AcademicYearID)
					GROUP BY
						ay.AcademicYearID
					ORDER BY 
						ayt.TermStart ";
			$result = $this->returnResultSet($sql);
			return $result;
		}
		
		function Get_Semester_Seq_Number($SemID, $AcademicYearID = '') {
		    global $intranet_root;
		    include_once ($intranet_root."/includes/form_class_manage.php");
		    
		    if (trim($AcademicYearID) == '') {
		        $AcademicYearID = $this->AcademicYearID;
		    }
	        
	        $ay = new academic_year($AcademicYearID);
	        $TermList = $ay->Get_Term_List();
	        $TermIDList = array_keys($TermList);
	        
	        if (!in_array($SemID, $TermIDList)) {
	            return false;
	        } else {
                return array_search($SemID, $TermIDList) + 1;
	        }
		}
		
		function Get_Active_AcademicYearID() {
		    if(isset($this->AcademicYearID)) {
		        return $this->AcademicYearID;
            }

			global $intranet_root;
			include_once($intranet_root."/includes/libgeneralsettings.php");
			
			$lgs = new libgeneralsettings();
			$SettingsArr = $lgs->Get_General_Setting($this->ModuleName, array("'ActiveAcademicYearID'"));
			$activeYearID = $SettingsArr['ActiveAcademicYearID'];
			return $activeYearID;
		}
		
		function Get_Active_AcademicYearName() {
			global $intranet_root;
			include_once($intranet_root."/includes/form_class_manage.php");
			
			$ObjAcademicYear = new academic_year($this->AcademicYearID);
			$activeYearName = $ObjAcademicYear->Get_Academic_Year_Name();
			return $activeYearName;
		}
		
		function Update_Active_AcademicYearID($AcademicYearID) {
			global $intranet_root;
			include_once($intranet_root."/includes/libgeneralsettings.php");
			
			$lgs = new libgeneralsettings();
			$SettingsArr['ActiveAcademicYearID'] = $AcademicYearID;
			$success = $lgs->Save_General_Setting($this->ModuleName, $SettingsArr);
			return $success;
		}
		
		function TRUNCATE_POWER_PORTFOLIO_TABLE($db, $tables) {
			$success = array();
			for($i=0; $i<sizeof($tables); $i++) {
				$sql = "TRUNCATE TABLE $db.".$tables[$i];
				$success[] = $this->db_db_query($sql);
			}
			return !in_array(false, $success);
		}
		
		function UPDATE_POWER_PORTFOLIO_TABLE_FIELDS($db, $AcademicYearID, $tablesToUpdateAry) {
		    $mappingDataAry = array();
		    $mappingDataAry['YearClassID'] = $this->Build_Active_Year_Class_Mapping($AcademicYearID);
		    $mappingDataAry['YearTermID']  = $this->Build_Active_Year_Term_Mapping($AcademicYearID);
		    $mappingDataAry['TermID']      = $mappingDataAry['YearTermID'];
		    
		    $success = array();
		    for($i=0; $i<sizeof($tablesToUpdateAry); $i++)
		    {
		        list($table, $field) = $tablesToUpdateAry[$i];
		        if($table != "" && $field != "" && !empty($mappingDataAry[$field])) {
		            $targetMappingAry = $mappingDataAry[$field];
		            foreach((array)$targetMappingAry as $oldTargetID => $newTargetID) {
		                if($oldTargetID > 0 && $newTargetID > 0) {
		                    $sql = "UPDATE $db.$table SET $field = '$newTargetID' WHERE $field = '$oldTargetID'";
		                    $success[] = $this->db_db_query($sql);
		                }
		            }
		        }
		    }
		    return !in_array(false, $success);
		}
		
		function Build_Active_Year_Class_Mapping($newAcademicYearID, $oldAcademicYearID='') {
		    $oldYearClassAry = $this->Get_All_KG_Class($oldAcademicYearID);
		    $oldYearClassAry = BuildMultiKeyAssoc($oldYearClassAry, "YearClassID", "ClassTitleEN", 1);
		    $newYearClassAry = $this->Get_All_KG_Class($newAcademicYearID);
		    $newYearClassAry = BuildMultiKeyAssoc($newYearClassAry, "ClassTitleEN", "YearClassID", 1);
		    
		    $yearClassMapAry = array();
		    foreach($oldYearClassAry as $oldYearClassID => $classTitle) {
		        if(isset($newYearClassAry[$classTitle])) {
		            $yearClassMapAry[$oldYearClassID] = $newYearClassAry[$classTitle];
		        }
		    }
		    return $yearClassMapAry;
		}
		
		function Build_Active_Year_Term_Mapping($newAcademicYearID, $oldAcademicYearID='') {
		    $oldAcademicYearID = $oldAcademicYearID? $oldAcademicYearID : $this->AcademicYearID;
		    $oldYearTermAry = getSemesters($oldAcademicYearID, 0);
		    $newYearTermAry = getSemesters($newAcademicYearID, 0);
		    
		    $yearTermMapAry = array();
		    foreach($oldYearTermAry as $termOrder => $oldTermInfo) {
		        if(isset($newYearTermAry[$termOrder])) {
		            $yearTermMapAry[$oldTermInfo['YearTermID']] = $newYearTermAry[$termOrder]['YearTermID'];
		        }
		    }
		    return $yearTermMapAry;
		}
		
		function Add_Active_AcademicYearID($AcademicYearID)
		{
			global $intranet_root, $sys_custom, $indexVar;
			global $intranet_db, $intranet_db_user, $intranet_db_pass;
			
			$OldDBName = $this->DBName;
			$OldSchoolYearName = $this->Get_Active_AcademicYearName();
			
			# Step 1: Create new Database
			$AcademicYearID = IntegerSafe($AcademicYearID);
			$NewDBName = $intranet_db."_DB_POWER_PORTFOLIO_".$AcademicYearID;
			$sql = "CREATE DATABASE $NewDBName";
			$successArr['Create_Database'] = $this->db_db_query($sql);
			if ($successArr['Create_Database'])
			{
				# Step 2: Execute commands (mysqldump & mysql) > Duplicate entire Database
				$SqlHostPara = "";
				if ($sys_custom['MySQL_Server_Host']) {
					$SqlHostPara = "-h ".$sys_custom['MySQL_Server_Host'];
				}
				$dumpCommand = "mysqldump $SqlHostPara -u $intranet_db_user --password=$intranet_db_pass $OldDBName | mysql $SqlHostPara -u $intranet_db_user --password=$intranet_db_pass $NewDBName";
				exec($dumpCommand);
				
				# Step 3: Truncate all data from these tables, others are preserved so they don't need to set again
                /*
				$tablesToTruncate = array(
                        				    "RC_AWARD_STUDENT_RECORD",
                        				    "RC_AWARD_GENERATED_STUDENT_RECORD",
                        				    "RC_CLASS_COMMENT_PROGRESS",
                        				    "RC_EXTRA_SUBJECT_INFO",
                        				    "RC_LOG",
                        				    "RC_MANUAL_ADJUSTMENT",
                        				    "RC_MARKSHEET_COMMENT",
                        				    "RC_MARKSHEET_FEEDBACK",
                        				    "RC_MARKSHEET_OVERALL_SCORE",
                        				    "RC_MARKSHEET_SCORE",
                        				    "RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD",
                        				    "RC_MARKSHEET_SUBMISSION_PROGRESS",
                        				    "RC_MARKSHEET_TOPIC_SCORE",
                        				    "RC_MARKSHEET_VERIFICATION_PROGRESS",
                        				    "RC_OTHER_STUDENT_INFO",
                        				    "RC_PERSONAL_CHARACTERISTICS_DATA",
                        				    "RC_REPORT_CARD_ARCHIVE",
                        				    "RC_REPORT_RESULT",
                        				    "RC_REPORT_RESULT_ARCHIVE",
                        				    "RC_REPORT_RESULT_FULLMARK",
                        				    "RC_REPORT_RESULT_SCORE",
                        				    "RC_REPORT_RESULT_SCORE_ARCHIVE",
                        				    "RC_REPORT_STUDENT_EXTRA_INFO",
                        				    "RC_STUDENT_ACADEMIC_PROGRESS",
                        				    "RC_SUB_MARKSHEET_SCORE",
				                            "RC_OTHER_INFO_STUDENT_RECORD",         // KG related DB Tables
											"RC_STUDENT_SCORE",
                        				    "RC_SUBJECT_TOPIC_SCORE",
                        				    "RC_TOPIC_SCORE",
											"STUDENT_ACCESS_LOG",
											"RC_RUBRIC_SETTING",
											"RC_RUBRIC_SETTING_ITEM"
				);
                */
                $tablesToTruncate = array();
				$successArr['Truncate_Table'] = $this->TRUNCATE_POWER_PORTFOLIO_TABLE($NewDBName, $tablesToTruncate);
				
				# Step 4: Update fields for these tables, ensure those settings can use in new school year
                /*
				$tablesToUpdate = array(
                        				    array("RC_ABILITY_GRADE_REMARKS_CUSTOM", "YearTermID"),
                                            array("RC_ABILITY_GRADE_REMARKS_TARGET_CAT", "YearTermID"),
                        				    array("RC_SUBJECT", "TermID"),
                                            array("RC_TOPIC", "TermID"),
                        				    array("RC_TIMETABLE_CLASS_ZONE_QUOTA_MAPPING", "YearClassID")
				);
                */
                $tablesToUpdate = array();
				$successArr['Update_Table'] = $this->UPDATE_POWER_PORTFOLIO_TABLE_FIELDS($NewDBName, $AcademicYearID, $tablesToUpdate);
				
				# Step 5: Set these DateTime fields to NULL so all Timetables are reset to inactive state
                /*
				$sql = "UPDATE $NewDBName.RC_TOPICTIMETABLE SET StartDate = NULL, EndDate = NULL";
				$successArr['Reset_Dates'] = $this->db_db_query($sql);
				*/

				# Step 6: Set active Database to newly created Database
				$successArr['Update_ActiveYear'] = $this->Update_Active_AcademicYearID($AcademicYearID);
				
				# Step 7: Copy file to new folder
				$NewSchoolYearName = getAYNameByAyId($AcademicYearID);
				$newDBFolderPathPrefix = "/file/power_portfolio/".$NewSchoolYearName;
				$newDBFolderFullPath = $intranet_root.$newDBFolderPathPrefix;
				if (!file_exists($newDBFolderFullPath)) {
					$successArr['Create_Template_File_Folder'] = $indexVar['libfilesystem']->folder_new($newDBFolderFullPath);
				}
				$oldDBFolderPathPrefix = "/file/power_portfolio/".$OldSchoolYearName."/images";
				$oldDBFolderFullPath = $intranet_root.$oldDBFolderPathPrefix;
				if(file_exists($oldDBFolderFullPath)) {
					$successArr['Copy_Folder'] = $indexVar['libfilesystem']->folder_copy($oldDBFolderFullPath, $newDBFolderFullPath);
				}
			}
		}
		
		function Get_Database_Name($year) {
			global $intranet_db;
			$prefix = $intranet_db."_DB_POWER_PORTFOLIO_";
			$thisdbName = $prefix.$year;
			return $thisdbName;
		}
		
		function Get_Table_Name($Table) {
			$thisTableName = $this->DBName.'.'.$Table;
			return $thisTableName;
		}
		
		function Get_Form_Topic_Record($TopicID='', $YearID='', $filterByClassTeacher=false) {
			if($TopicID){
				$TopicIDFilter = " AND ft.TopicID = '$TopicID' ";
			}
			if($YearID){
				$YearIDFilter = " AND ft.YearID = '$YearID' ";
			}
            if($filterByClassTeacher) {
                $YearIDArr = Get_Array_By_Key($this->Get_Teaching_Level($_SESSION['UserID']), 'YearID');
                $classTeacherYearFilter = " AND ft.YearID IN ('".implode("','", (array)$YearIDArr)."') ";
            }

			$tableName = $this->Get_Table_Name("RC_FORM_TOPICS");
			//$tableName2 = $this->Get_Table_Name('RC_ABILITY_INDEX_CATEGORY');
			
			$sql = "SELECT 
						ft.*, y.YearName
					FROM
						$tableName ft
					INNER JOIN YEAR y 
						ON ft.YearID = y.YearID
					WHERE 
						ft.IsDeleted = '0'
						$TopicIDFilter
						$YearIDFilter
						$classTeacherYearFilter ";
			$result = $this->returnResultSet($sql);
			return $result;
		}
		
		function Update_Form_Topic_Record($TopicID='', $TopicCode, $TopicB5, $TopicEN, $remarks, $YearID, $LearningZone=array()) {
			$userID = $_SESSION['UserID'];
			$tableName = $this->Get_Table_Name("RC_FORM_TOPICS");
			
			// Update Topics
			if($TopicID){
				$sql = "UPDATE
							$tableName
						SET 
							TopicCode = '$TopicCode', TopicNameB5 = '$TopicB5', TopicNameEN = '$TopicEN', Remark = '$remarks', YearID = '$YearID', InputBy = '$userID', DateInput = now(), ModifiedBy = '$userID', DateModified = now()
						WHERE
							TopicID = '$TopicID'";
			}
			// Insert new Topics
			else{
				$sql = "INSERT INTO $tableName
							(TopicCode , TopicNameB5, TopicNameEN, Remark, IsDeleted, YearID, InputBy, DateInput, ModifiedBy, DateModified)
						VALUES
							('$TopicCode', '$TopicB5', '$TopicEN', '$remarks', '0', '$YearID', '$userID', now(), '$userID', now())";
			}
			return $this->db_db_query($sql);
		}
		
		function Delete_Form_Topic_Record($TopicIDAry){
			$userID = $_SESSION['UserID'];
			$tableName = $this->Get_Table_Name("RC_FORM_TOPICS");
			
			// Soft delete Topics
			$sql = "UPDATE
						$tableName
					SET 
						IsDeleted = '1', ModifiedBy = '$userID', DateModified = now()
					WHERE
						TopicID IN ('".implode("', '", (array)$TopicIDAry)."')";
			return $this->db_db_query($sql);
		}
		
		function Get_Learning_Zone_Record($ZoneID='', $YearID='', $withRelatedTopics=false, $filterByClassTeacher=false) {
			if($ZoneID){
			    if(!is_array($ZoneID)) {
			        $ZoneIDFilter = " AND lz.ZoneID = '$ZoneID' ";
			    } else {
			        $ZoneIDFilter = " AND lz.ZoneID IN ('".implode("', '", (array)$ZoneID)."') ";
			    }
			}
			if($YearID){
				$YearIDFilter = " AND lz.YearID = '$YearID' ";
			}
            if($filterByClassTeacher) {
                $YearIDArr = Get_Array_By_Key($this->Get_Teaching_Level($_SESSION['UserID']), 'YearID');
                $classTeacherYearFilter = " AND lz.YearID IN ('".implode("','", (array)$YearIDArr)."') ";
            }
			$tableName = $this->Get_Table_Name("RC_LEARNING_ZONE");
			//$tableName2 = $this->Get_Table_Name('RC_ABILITY_INDEX_CATEGORY');
			
			$sql = "SELECT 
						lz.*, y.YearName
					FROM
						$tableName lz
					INNER JOIN YEAR y 
						ON lz.YearID = y.YearID
					WHERE 
						lz.IsDeleted = '0'
						$ZoneIDFilter
						$YearIDFilter 
						$classTeacherYearFilter ";
			$result = $this->returnResultSet($sql);
			return $result;
		}
		
		function Update_Learning_Zone_Record($ZoneID='', $ZoneCode, $ZoneB5, $ZoneEN, $ZoneQuota, $pictureType, $remarks, $YearID, $StudyTools=array()) {
			$userID = $_SESSION['UserID'];
			$tableName = $this->Get_Table_Name("RC_LEARNING_ZONE");
			
			// Update Learning Zone
			if($ZoneID){
				$sql = "UPDATE
							$tableName
						SET 
							ZoneCode = '$ZoneCode', ZoneNameB5 = '$ZoneB5', ZoneNameEN = '$ZoneEN', ZoneQuota = '$ZoneQuota', PictureType = '$pictureType', Remark = '$remarks', YearID = '$YearID', InputBy = '$userID', DateInput = now(), ModifiedBy = '$userID', DateModified = now()
						WHERE
							ZoneID = '$ZoneID'";
			}
			// Insert new Learning Zone
			else{
				$sql = "INSERT INTO $tableName
							(ZoneCode , ZoneNameB5, ZoneNameEN, ZoneQuota, PictureType, Remark, IsDeleted, YearID, InputBy, DateInput, ModifiedBy, DateModified)
						VALUES
							('$ZoneCode', '$ZoneB5', '$ZoneEN', '$ZoneQuota', '$pictureType', '$remarks', '0', '$YearID', '$userID', now(), '$userID', now())";
			}
			return $this->db_db_query($sql);
		}
		
		function Delete_Learning_Zone_Record($ZoneIDAry){
			$userID = $_SESSION['UserID'];
			$tableName = $this->Get_Table_Name("RC_LEARNING_ZONE");
			
			// Soft Delete Learning Zone
			$sql = "UPDATE
						$tableName
					SET 
						IsDeleted = '1', ModifiedBy = '$userID', DateModified = now()
					WHERE
						ZoneID IN ('".implode("', '", (array)$ZoneIDAry)."')";
			return $this->db_db_query($sql);
		}
		
		function Update_Equipment_Record($CodeID='', $Code, $EN_Name, $CH_Name, $YearID, $PhotoPath='', $Remarks='', $ToolCatID='', $ZoneID='', $Chapter = '')
		{
			global $indexVar;
			
			$userID = $_SESSION['UserID'];
			$tableName = $this->Get_Table_Name('RC_EQUIPMENT');
			
			// Update Equipment
			if($CodeID)
			{
				$sql = "UPDATE
							$tableName
						SET 
							/*Code = '$Code',*/ EN_Name = '$EN_Name', CH_Name = '$CH_Name', YearID = '$YearID', 
							PhotoPath = '$PhotoPath', Remarks = '$Remarks', CategoryID = '$ToolCatID', ZoneID = '$ZoneID', Chapter = '$Chapter', DateModified = NOW(), ModifiedBy = '$userID'
						WHERE
							CodeID = '$CodeID' ";
			}
			// Insert Equipment
			else
			{
				$sql = "INSERT INTO $tableName
							(/*Code,*/ EN_Name, CH_Name, YearID, PhotoPath, Remarks, CategoryID, ZoneID, Chapter, DateInput, InputBy, DateModified, ModifiedBy, IsDeleted)
						VALUES
							(/*'$Code',*/ '$EN_Name', '$CH_Name', '$YearID', '$PhotoPath', '$Remarks', '$ToolCatID', '$ZoneID', '$Chapter', NOW(), '$userID', NOW(), '$userID', 0) ";
			}
			$this->db_db_query($sql);
			
			// Handling for Insert Equipment
			if(!$CodeID)
			{
				// Get Equipment Code ID
// 				$sql = "SELECT CodeID FROM $tableName
// 							WHERE Code = '$Code' AND EN_Name = '$EN_Name' AND CH_Name = '$CH_Name' AND YearID = '$YearID' AND ZoneID = '$ZoneID' AND PhotoPath = '$PhotoPath' AND Remarks = '$Remarks'
// 							ORDER BY CodeID DESC ";
// 				$temp = $this->returnResultSet($sql);
// 				$CodeID = $temp[0]["CodeID"];
// 				if($CodeID > 0 && $PhotoPath != "") {
			    
			    // Update Code and Photo Path
			    $CodeID = $this->db_insert_id();
			    if($CodeID > 0)
			    {
			        $Code = $CodeID;
			        while (strlen($Code) < 4) {
			            $Code = '0' . $Code;
			        }
			        
			        $updatePathSQL = "";
			        if($PhotoPath != "") {
    					$file_ext = $indexVar["libfilesystem"]->file_ext($PhotoPath);
    					$PhotoPath = basename($CodeID).$file_ext;
    					$updatePathSQL = " , PhotoPath = '$PhotoPath' ";
			        }
			        
					$sql = "UPDATE
								$tableName
							SET 
								Code = '$Code' $updatePathSQL
							WHERE 
								CodeID = '$CodeID' ";
					$this->db_db_query($sql);
				}
			}
			
			return $CodeID;
		}
		
		function Updata_Equipment_Cata_Mapping($CodeID,$AbilityID){
			$userID = $_SESSION['UserID'];
			$Equipment_Table = $this->Get_Table_Name('RC_EQUIPMENT');
			$Mapping_Table = $this->Get_Table_Name("RC_EQUIPMENT_CATA_MAPPING");
			$insertRecord = "";
			$dimeter = "";
			
			// Delete Existing Records
			$sql = "UPDATE
						$Mapping_Table
					SET
						IsDeleted = 1
					WHERE
						CodeID = '$CodeID' ";
			$this->db_db_query($sql);
			
			// Insert new Matching
			foreach ((array)$AbilityID as $_AbilityID){
				$insertRecord .= $dimeter."('$CodeID','$_AbilityID', now(), '$userID', now(), '$userID', 0)";
				$dimeter = ",";
			}
			if($insertRecord){
				$sql = "INSERT INTO $Mapping_Table
							(CodeID, AbilityCatID , DateInput, InputBy, DateModified, ModifiedBy, IsDeleted)
						VALUES
							$insertRecord ";
				$this->db_db_query($sql);
			}
			return;
		}
		
		function Get_Equipment_Record($YearID='', $CodeID='', $CatID='', $CodeLike='', $ZoneID='', $keyword='', $Chapter = '', $filterByClassTeacher=false){
			if($YearID) {
				$YearIDFilter = " AND re.YearID = '$YearID' ";
			}
			if($CodeID) {
				$CodeIDFilter = " AND re.CodeID IN ('".implode("', '", (array)$CodeID)."')";
			}
			if($CatID) {
				$CatIDFilter = " AND re.CategoryID = '$CatID' ";
			}
			if($CodeLike) {
			    $CodeLikeFilter = " AND re.Code LIKE '%".$CodeLike."%'";
			}
			if($ZoneID) {
			    $ZoneIDFilter = " AND re.ZoneID = '$ZoneID' ";
			}
			if($keyword != '') {
    			$tableName2 = $this->Get_Table_Name('RC_ABILITY_INDEX_CATEGORY');
    			$tableName3 = $this->Get_Table_Name('RC_EQUIPMENT_CATA_MAPPING');
    			
    			$sql = "SELECT
                            DISTINCT(eqc.CodeID)
                        FROM
                            $tableName2 as aic
                            INNER JOIN $tableName3 as eqc ON (aic.CatID = eqc.AbilityCatID AND eqc.IsDeleted = 0)
                        WHERE
                            aic.Type = 0 AND aic.Level = 3 AND 
                            (aic.Code LIKE '%".$keyword."%' OR aic.Name LIKE '%".$keyword."%') ";
                $codeIDAry = $this->returnVector($sql);
                
                $codeIDCond = "";
                if(!empty($codeIDAry)) {
                    $codeIDCond = " OR re.CodeID IN ('".implode("', '", (array)$codeIDAry)."') ";
                }
                
			    $keywordFilter = " AND (re.Code LIKE '%".$keyword."%' OR re.EN_Name LIKE '%".$keyword."%' OR re.CH_Name LIKE '%".$keyword."%' $codeIDCond) ";
			}
			if($Chapter != ''){
				$ChapterFilter = " AND re.Chapter = '$Chapter' ";
			}
            if($filterByClassTeacher) {
                $YearIDArr = Get_Array_By_Key($this->Get_Teaching_Level($_SESSION['UserID']), 'YearID');
                $classTeacherYearFilter = " AND re.YearID IN ('".implode("','", (array)$YearIDArr)."') ";
            }
			
			$tableName = $this->Get_Table_Name('RC_EQUIPMENT');
// 			$tableName2 = $this->Get_Table_Name('RC_ABILITY_INDEX_CATEGORY');
			
			$sql = "SELECT 
						re.*, y.YearName 
					FROM $tableName re
						INNER JOIN YEAR y ON re.YearID = y.YearID
					WHERE 
						re.IsDeleted = '0'
						$YearIDFilter
						$CodeIDFilter
						$CatIDFilter
						$CodeLikeFilter
						$ZoneIDFilter
			            $keywordFilter
						$ChapterFilter
						$classTeacherYearFilter ";
			$result = $this->returnResultSet($sql);
			return $result;
		}
		
		function Delete_Equipment_Record($CodeID)
		{
			$userID = $_SESSION['UserID'];
			$tableName = $this->Get_Table_Name('RC_EQUIPMENT');
			
			$sql = "UPDATE
						$tableName
					SET 
						IsDeleted = '1', DateModified = now(), ModifiedBy = $userID
					WHERE
						CodeID = ('$CodeID') ";
			$result = $this->db_db_query($sql);
			
			if($result)
			{
				$categoryMapTable = $this->Get_Table_Name('RC_EQUIPMENT_CATA_MAPPING');
				$timetableMapTable = $this->Get_Table_Name('RC_TIMETABLE_TOOL_MAPPING');
				
				$sql = "UPDATE
							$categoryMapTable
						SET 
							IsDeleted = '1', DateModified = now(), ModifiedBy = $userID
						WHERE
							CodeID = ('$CodeID') AND IsDeleted = 0 ";
				$this->db_db_query($sql);
				
				$sql = "UPDATE
							$timetableMapTable
						SET 
							IsDeleted = '1', DateModified = now(), ModifiedBy = $userID
						WHERE
							ToolCodeID = ('$CodeID') AND IsDeleted = 0 ";
				$this->db_db_query($sql);
			}
			
			return $result;
		}
		
		function Update_Equipment_Category($CatID='', $Code, $EN_Name, $CH_Name, $YearID='', $Remarks=''){
			$userID = $_SESSION['UserID'];
			$tableName = $this->Get_Table_Name('RC_EQUIPMENT_CATEGORY');
			
			// Update Category
			if($CatID) {
				$sql = "UPDATE $tableName
							SET Code = '$Code', EN_Name = '$EN_Name', CH_Name = '$CH_Name', YearID = '$YearID', Remarks = '$Remarks', DateModified = NOW(), ModifiedBy = '$userID'
							WHERE CatID = '$CatID' ";
			}
			// Insert Category
			else{
				$sql = "INSERT INTO $tableName
							(Code, EN_Name, CH_Name, YearID, Remarks, DateInput, InputBy, DateModified, ModifiedBy, IsDeleted)
						VALUES 
							('$Code', '$EN_Name', '$CH_Name', '$YearID', '$Remarks', NOW(), '$userID', NOW(), '$userID', 0) ";
			}
			$this->db_db_query($sql);
			
			// if insert, get current CatID
			if(!$CatID) {
				$sql = "SELECT CatID FROM $tableName
						WHERE
							Code = '$Code' AND EN_Name = '$EN_Name' AND CH_Name = '$CH_Name' AND YearID = '$YearID' AND Remarks = '$Remarks'
						ORDER BY CatID desc ";
				$temp = $this->returnResultSet($sql);
				$CatID = $temp[0]['CatID'];
			}
			return $CatID;
		}
		
		function Get_Equipment_Category($CatID='', $CatCode='') {
			$CatFilter = "";
			if($CatID) {
				$CatFilter .= " AND CatID = '$CatID' ";
			}
			if($CatCode) {
				$CatFilter .= " AND Code = '$CatCode' ";
			}
			
			$tableName = $this->Get_Table_Name('RC_EQUIPMENT_CATEGORY');
			
			$sql = "SELECT * FROM $tableName WHERE IsDeleted = '0' $CatFilter ORDER BY Code ";
			$result = $this->returnResultSet($sql);
			return $result;
		}
		
		function Delete_Equipment_Category($CatID) {
			$userID = $_SESSION['UserID'];
			$tableName = $this->Get_Table_Name('RC_EQUIPMENT_CATEGORY');
			
			$sql = "UPDATE $tableName
						SET IsDeleted = '1', DateModified = NOW(), ModifiedBy = '$userID'
						WHERE CatID = '$CatID'";
			return $this->db_db_query($sql);
		}
		
		function Get_Macau_Category($returnAssocArr=true){
		    $tableName = $this->Get_Table_Name('RC_ABILITY_INDEX_CATEGORY');
		    
		    $sql = "SELECT
                        CatID, Code, Name
                    FROM
                        $tableName
                    WHERE
                        Type = '1' AND Level = '0' ";
		    $result = $this->returnResultSet($sql);
		    
		    if($returnAssocArr) {
		        $result = BuildMultiKeyAssoc($result, 'Code', 'Name', 1, 0);
		    }
		    return $result;
		}
		
		function Get_Taiwan_Category($YearName='',$Cata=''){
			$tableName = $this->Get_Table_Name('RC_ABILITY_INDEX_CATEGORY');
			$Cata_Level['K1'] = "小";
			$Cata_Level['K2'] = "中";
			$Cata_Level['K3'] = "大";
			if($YearName){
				$cond = " AND Code LIKE '%$Cata_Level[$YearName]%'";
			}
			if($Cata){
				$cond .= " AND Code LIKE '%$Cata%' ";
			}
			
			$sql = "SELECT 
						CatID, Code, Name
					FROM
						$tableName
					WHERE 
						Type = '0'
						$cond ";
			return $this->returnResultSet($sql);
		}
		
		function Get_Taiwan_Category_Fm_Mapping($CodeID){
			$CodeID = implode(',',(array)$CodeID);
			$tableName = $this->Get_Table_Name('RC_ABILITY_INDEX_CATEGORY');
			$Mapping_Table = $this->Get_Table_Name("RC_EQUIPMENT_CATA_MAPPING");
			$sql = "
				SELECT
					raic.CatID, raic.Code, raic.Name, recm.CodeID
				FROM
					$tableName raic
				INNER JOIN
					$Mapping_Table recm
						ON recm.AbilityCatID = raic.CatID
				WHERE
					raic.Type = '0'
					AND recm.CodeID IN ($CodeID)
					AND recm.IsDeleted = '0' ";
			return $this->returnResultSet($sql);
		}
		
		function Update_Index_Details($IndexID, $IndexType, $IndexName) {
			if(empty($IndexID) || empty($IndexName)) {
				return false;
			}
			
			$tableName = $this->Get_Table_Name('RC_ABILITY_INDEX_CATEGORY');
			$sql = "UPDATE $tableName SET Name = '$IndexName', DateModified = NOW()
					WHERE CatID = '$IndexID' AND Type = '$IndexType'";
			return $this->db_db_query($sql);
		}
		
		function Get_All_KG_Form()
		{
		    $sql = "SELECT
						YearID,
                        YearName
					FROM
						YEAR
					ORDER BY
						Sequence";
		    return $this->returnArray($sql);
		}
		
		function Get_All_KG_Class($academicYearID='')
		{
		    $academicYearID = $academicYearID? $academicYearID : $this->AcademicYearID;
		    
			$sql = "SELECT
						yc.YearClassID,
						yc.ClassTitleEN,
						yc.ClassTitleB5,
						yc.YearID
					FROM
						YEAR_CLASS as yc
						INNER JOIN YEAR as y ON (yc.YearID = y.YearID)
					WHERE
						yc.AcademicYearID = '".$academicYearID."'
					ORDER BY
						y.Sequence, yc.Sequence";
			return $this->returnArray($sql);
		}
		
		function Get_KG_Form_Subject($YearID)
		{
		    $subjectArr = array();
		    if($YearID != '')
		    {
		        global $intranet_root;
		        include_once($intranet_root."/includes/subject_class_mapping.php");
		        
		        $sbj = new subject();
		        $subjectArr = $sbj->Get_Subject_By_Form($YearID);
		        
    		    if(!$this->IS_POWER_PORTFOLIO_ADMIN_USER() && !$this->IS_KG_CLASS_TEACHER() && $this->IS_KG_SUBJECT_TEACHER()) {
    		        $teacherSubjectIDArr = $this->Get_Teaching_Subject_Group($_SESSION['UserID'], true, $YearID);
    		        $teacherSubjectIDArr = Get_Array_By_Key($teacherSubjectIDArr, 'RecordID');
    		        foreach ($subjectArr as $index => $thisSubject) {
    		            if(!in_array($thisSubject['RecordID'], $teacherSubjectIDArr)) {
    		                unset($subjectArr[$index]);
    		            }
    		        }
    		        $subjectArr = array_values($subjectArr);
    		    }
		    }
		    return $subjectArr;
		}
		
		function Get_KG_Form_Subject_Group($YearID, $SubjectID, $YearTermID='')
		{
		    if($YearID != '') {
		        global $intranet_root;
		        include_once($intranet_root."/includes/subject_class_mapping.php");
		        
		        $sbj = new subject();
		        return $sbj->Get_Subject_Group_List($YearTermID, $YearID, '', '', 0, '', $SubjectID);
		    }
		}
		
		function Get_KG_Subject_From_Code($CodeAry)
		{
		    $sql = "SELECT DISTINCT(RecordID) FROM ASSESSMENT_SUBJECT
                        WHERE CODEID IN ('".implode("', '", (array)$CodeAry)."') ";
		    return $this->returnVector($sql);
		}

		function Get_Teaching_Level($parTeacherId)
        {
            $sql = "SELECT
						y.YearID,
						y.YearName
					FROM
						YEAR_CLASS_TEACHER as yct
						INNER JOIN YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."')
						INNER JOIN YEAR as y ON (yc.YearID = y.YearID)
					WHERE
						yct.UserID = '$parTeacherId'
                    GROUP BY
                        y.YearID
                    ORDER BY
                        y.Sequence";
            return $this->returnArray($sql);
        }
		
		function Get_Teaching_Class($parTeacherId)
		{
			$sql = "SELECT
						yc.YearClassID,
						yc.ClassTitleEN,
						yc.ClassTitleB5,
						yc.YearID
					FROM
						YEAR_CLASS_TEACHER as yct
						INNER JOIN YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."')
					WHERE
						yct.UserID = '$parTeacherId'";
			return $this->returnArray($sql);
		}
		
		function Get_Teaching_Subject_Group($parTeacherId, $notUseConfig=false, $yearID='', $subjectID = '')
		{
		    global $PowerPortfolioConfig;
		    
		    $sql = "SELECT
                        DISTINCT(stc.SubjectGroupID),
                        s.RecordID,
                        s.CODEID
	                FROM
		                SUBJECT_TERM_CLASS_TEACHER as stct
		                INNER JOIN SUBJECT_TERM_CLASS as stc ON (stct.SubjectGroupID = stc.SubjectGroupID AND stct.UserID = '$parTeacherId')
		                INNER JOIN SUBJECT_TERM as st ON (stc.SubjectGroupID = st.SubjectGroupID)
		                INNER JOIN ACADEMIC_YEAR_TERM as ayt ON (st.YearTermID = ayt.YearTermID AND ayt.AcademicYearID = '".$this->Get_Active_AcademicYearID()."')
                        INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION as stcy ON (stcy.SubjectGroupID = stc.SubjectGroupID)
					    INNER JOIN YEAR as y ON (stcy.YearID = y.YearID)
                        INNER JOIN ASSESSMENT_SUBJECT as s ON (st.SubjectID = s.RecordID AND (CMP_CODEID IS NULL OR CMP_CODEID = ''))
                    WHERE
    					1";
		    $sql .= $yearID == '' ? "" : " AND y.YearID = '".$yearID."' ";
		    $sql .= $subjectID == '' ? "" : " AND s.RecordID = '".$subjectID."' ";
		    $sql .= $notUseConfig? "" : " AND s.CODEID IN ('".implode("', '", (array)$PowerPortfolioConfig['subjectCode'])."')";
		    return $this->returnArray($sql);
		}
		
		function Get_Teaching_Subject_Group_Related_Class($parTeacherId, $notUseConfig=false, $subjectID = '')
		{
		    $related_subject_groups = $this->Get_Teaching_Subject_Group($parTeacherId, $notUseConfig, $yearID='', $subjectID);
		    $related_subject_group_ids = Get_Array_By_Key($related_subject_groups, 'SubjectGroupID');
		    $related_subect_group_student_ids = $this->Get_Student_In_Subject_Group_Of_Subject('', '', '', $related_subject_group_ids, $notUseConfig);
		    $related_subejct_group_classes = $this->Get_Student_Linked_Class($related_subect_group_student_ids);
		    return $related_subejct_group_classes;
		}
		
		function Return_Class_Teacher_Class($ParUserID, $ClassLevelID='', $ClassName='', $YearClassID='') {
			$con = $ClassLevelID != "" ? " and y.YearID = '$ClassLevelID' " : "";
			$con .= $ClassName != "" ? " and yc.ClassTitleEn = '$ClassName' " : "";
			$con .= $YearClassID != "" ? " and yc.YearClassID = '$YearClassID' " : "";
			
		 	$sql = "SELECT
		 					yc.YearClassID as ClassID,
		 					yc.ClassTitleEn as ClassName,
		 					y.YearID as ClassLevelID
		 			FROM
		 					YEAR_CLASS_TEACHER as yct
		 					INNER JOIN YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID AND yct.UserID = '$ParUserID' AND yc.AcademicYearID = '".$this->AcademicYearID."')
		 					INNER JOIN YEAR as y ON (yc.YearID = y.YearID)
		 			WHERE
		 					1
		 					$con";
			return $this->returnArray($sql);
		}
		
		function GetAllClassesInTimeTable($TimeTableID="", $onlyCurrentTimeTable=false)
		{
			// SQL Table & Condition
			$TimeTable_table = $this->Get_Table_Name('RC_TOPICTIMETABLE');
			$TimeTableIDCond = '';
			$TimeTableDateRangeCond = '';
			if($TimeTableID) {
				$TimeTableIDCond = " AND TopicTimeTableID = '$TimeTableID'";
			}
			if($onlyCurrentTimeTable) {
				$today = date("Y-m-d H:i:s");
				$TimeTableDateRangeCond = " AND StartDate <= '$today' AND EndDate >= '$today' ";
			}
			
			// Get Class Level with TimeTable
			$sql = "SELECT
						DISTINCT YearID
					FROM
						$TimeTable_table
					WHERE 
						IsDeleted = 0
						$TimeTableIDCond
						$TimeTableDateRangeCond ";
			$LevelIDAry = $this->returnVector($sql);
			
			// Skip if $LevelIDAry is empty
			if(empty($LevelIDAry)) {
				return array();
			}
			
			// Get Classes in those Class Levels
			$sql = "SELECT
						YearClassID,
						ClassTitleEN,
						ClassTitleB5,
						YearID,
		 				YearClassID as ClassID,
		 				ClassTitleEn as ClassName,
		 				YearID as ClassLevelID
					FROM
						YEAR_CLASS
					WHERE
						AcademicYearID = '".$this->AcademicYearID."' AND 
						YearID IN ('".implode("', '", $LevelIDAry)."')
					ORDER BY
						ClassTitleEN";
			return $this->returnArray($sql);
		}
		
		function Get_Student_By_Class($ParClassID, $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=0, $ParLevelID='')
		{
			$cond = "";
			if (!is_array($ParStudentIDList) && $ParStudentIDList != "") {
				$ParStudentIDList = trim($ParStudentIDList);
				$ParStudentIDList = explode(",", $ParStudentIDList);
			}
			if(is_array($ParStudentIDList) && !empty($ParStudentIDList)) {
				$cond = " AND u.UserID IN ('".implode("', '", (array)$ParStudentIDList)."') ";
			}
			
			$classlevel_cond = " yc.YearClassID IN ('".implode("', '", (array)$ParClassID)."') ";
			if($ParLevelID != '')
			{
			    if($ParClassID == '') {
			        $classlevel_cond = ' 1 ';
			    }
                $classlevel_cond .= " AND yc.YearID = '$ParLevelID' ";
			}
			
			if ($withClassNumber == 1) {
				$NameField = getNameFieldWithClassNumberByLang('u.');
				$ArchiveNameField = getNameFieldWithClassNumberByLang('au.');
			}
			else {
				if ($isShowBothLangs)
				{
					$NameField = "	CONCAT(	TRIM(u.EnglishName), 
											If (
												u.ChineseName Is Not Null Or u.ChineseName != '',
												Concat(' (', TRIM(u.ChineseName), ')'),
												''
											)
									)
								";
					$ArchiveNameField = "	CONCAT(	TRIM(au.EnglishName), 
													If (
														au.ChineseName Is Not Null Or au.ChineseName != '',
														Concat(' (', TRIM(au.ChineseName), ')'),
														''
													)
											)
										";
				}
				else
				{
					$NameField = getNameFieldByLang2("u.");
					$ArchiveNameField = getNameFieldByLang2("au.");
				}
			}
			
			if ($isShowStyle==0) {
				$starHTML = '*';
			}
			else {
				$starHTML = '<font style="color:red;">*</font>';
			}
				
			$sql = "SELECT 
							DISTINCT(ycu.UserID),
							CASE 
								WHEN au.UserID IS NOT NULL then au.WebSAMSRegNo
								ELSE u.WebSAMSRegNo 
							END as WebSAMSRegNo,
							ycu.ClassNumber as ClassNumber,
							CASE 
								WHEN au.UserID IS NOT NULL then CONCAT('$starHTML',".$ArchiveNameField.") 
								WHEN u.RecordStatus = 3  THEN CONCAT('$starHTML',".$NameField.") 
								ELSE ".$NameField." 
							END as StudentName,
							yc.ClassTitleEN as ClassName,
							CASE 
								WHEN au.UserID IS NOT NULL then au.EnglishName
								ELSE u.EnglishName 
							END as StudentNameEn,
							CASE 
								WHEN au.UserID IS NOT NULL then au.ChineseName
								ELSE u.ChineseName 
							END as StudentNameCh,
							yc.ClassTitleEN as ClassTitleEn,
							yc.ClassTitleB5 as ClassTitleCh,
							CASE 
								WHEN au.UserID IS NOT NULL then au.WebSAMSRegNo
								ELSE u.WebSAMSRegNo 
							END as WebSAMSRegNo,
							CASE 
								WHEN au.UserID IS NOT NULL then au.UserLogin
								ELSE u.UserLogin 
							END as UserLogin,
							yc.YearClassID
					FROM 
							YEAR_CLASS_USER as ycu
							INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."')
							LEFT JOIN INTRANET_USER as u ON (ycu.UserID = u.UserID)
							Left Join INTRANET_ARCHIVE_USER as au ON (ycu.UserID = au.UserID) 
					WHERE 
					        $classlevel_cond
							$cond
					ORDER BY 
							yc.Sequence,
							ycu.ClassNumber 
					";
			$row_student = $this->returnArray($sql);
			
			$ReturnArr = array();
			if ($ReturnAsso==1) {
				foreach ($row_student as $key => $thisStudentInfoArr) {
					$thisStudentID = $thisStudentInfoArr['UserID'];
					$ReturnArr[$thisStudentID] = $thisStudentInfoArr;
				}
			}
			else {
				$ReturnArr = $row_student;
			}
			return $ReturnArr;
		}
		
		function Get_Student_In_Subject_Group_Of_Subject($StudentID='', $YearTermID='', $SubjectID='', $SubjectGroupID='', $notUseConfig=false)
		{
		    global $PowerPortfolioConfig;
		    
		    $conds_studentID = '';
		    if($StudentID != '') {
		        $conds_studentID = " AND stcu.UserID IN ('".implode("','", (array)$StudentID)."') ";
		    }
		    
		    $cond_yearTermID = '';
		    if($YearTermID != '') {
		        $cond_yearTermID = " AND st.YearTermID IN ('".implode("','", (array)$YearTermID)."') ";
		    }
		    else {
		        $YearTermID = getSemesters($this->Get_Active_AcademicYearID());
		        $YearTermID = array_keys((array)$YearTermID);
		        $cond_yearTermID = " AND st.YearTermID IN ('".implode("','", (array)$YearTermID)."') ";
		    }
		    
		    $conds_subjectID = '';
		    if($SubjectID != ''){
		        $conds_subjectID = " AND st.SubjectID IN ('".implode("','", (array)$SubjectID)."')' ";
	        }
	        
	        $cond_subectGroupID = '';
	        if($SubjectGroupID != ''){
	            $cond_subectGroupID= " AND st.SubjectGroupID IN ('".implode("','", (array)$SubjectGroupID)."') ";
	        }
	        
	        $cond_subjectCode = "";
	        if(!$notUseConfig) {
	            $cond_subjectCode .= " AND s.CODEID IN ('".implode("', '", (array)$PowerPortfolioConfig['subjectCode'])."') ";
	        }
	        
	        $sql = "SELECT
					   stcu.UserID
                    FROM
					   SUBJECT_TERM as st
					   INNER JOIN SUBJECT_TERM_CLASS_USER as stcu ON (stcu.SubjectGroupID = st.SubjectGroupID)
	                   INNER JOIN ASSESSMENT_SUBJECT as s ON (st.SubjectID = s.RecordID AND (s.CMP_CODEID IS NULL OR s.CMP_CODEID = ''))
			        WHERE
			           1
                       $cond_subjectCode
			           $conds_studentID
			           $cond_yearTermID
        			   $conds_subjectID
	                   $cond_subectGroupID ";
			$student_ids = $this->returnVector($sql);
			return $student_ids;
		}
		
		function Get_Student_Linked_Class($StudentID)
		{
		    $sql = "SELECT
                        DISTINCT(yc.YearClassID),
						yc.ClassTitleEN,
						yc.ClassTitleB5,
						yc.YearID
		            FROM
                        YEAR_CLASS_USER as ycu
		            INNER JOIN
                        YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND ycu.UserID IN ('".implode("','", (array)$StudentID)."') AND yc.AcademicYearID = '".$this->Get_Active_AcademicYearID()."')
 					INNER JOIN
                        YEAR as y ON (yc.YearID = y.YearID) ";
		    $class_ary = $this->returnArray($sql);
		    return $class_ary;
		}
		
		function UpdateTimeTable($TimeTableID='', $TimeTableCode, $CH_Name, $EN_Name, $YearID, $DateStart, $DateEnd){
			$TimeTable_table =  $this->Get_Table_Name('RC_TOPICTIMETABLE');
			$userID = $_SESSION['UserID'];
			
			// Update
			if($TimeTableID){
				$sql = "UPDATE 
							$TimeTable_table
						SET
							TimeTableCode = '$TimeTableCode', EN_Name = '$EN_Name', CH_Name = '$CH_Name', YearID = '$YearID', StartDate = '$DateStart', EndDate = '$DateEnd', DateModified = now(), ModifiedBy = $userID
						WHERE
							TopicTimeTableID = $TimeTableID ";
			}
			// Insert new record
			else{
				$sql = "INSERT INTO $TimeTable_table
							(TimeTableCode, EN_Name, CH_Name, YearID, StartDate, EndDate, DateInput, InputBy, DateModified, ModifiedBy, IsDeleted ) 
						VALUES
							('$TimeTableCode', '$EN_Name', '$CH_Name', $YearID, '$DateStart', '$DateEnd', now(), $userID, now(), $userID, 0) ";
			}
			$this->db_db_query($sql);
			
			// Get latest $TimeTableID
			$sql = "SELECT 
						TopicTimeTableID 
					FROM 
						$TimeTable_table
					WHERE
						TimeTableCode = '$TimeTableCode'
					AND
						ModifiedBy = $userID
					ORDER BY 
						DateModified DESC LIMIT 1 ";
			return $this->returnResultSet($sql);
		}
		
		function DeleteTimeTable($TimeTableID){
			$TimeTable_table =  $this->Get_Table_Name('RC_TOPICTIMETABLE');
			$userID = $_SESSION['UserID'];
			$sql = "UPDATE
						$TimeTable_table
					SET 
						DateModified = now(), ModifiedBy = $userID, isDeleted = 1
					WHERE
						TopicTimeTableID = $TimeTableID ";
			return $this->db_db_query($sql);
		}
		
		function UpdateTimeTopicMapping($TimeTableID, $topic){
			$TimeTable_Topic_Mapping =  $this->Get_Table_Name('RC_TIMETABLE_TOPIC_MAPPING');
			$userID = $_SESSION['UserID'];
			
			// Clear All the Data
			$this->DeleteTimeTopicMapping($TimeTableID);
// 			$sql = "
// 					UPDATE
// 						$TimeTable_Topic_Mapping
// 					SET 
// 						IsDeleted = 1, DateModified = now(), ModifiedBy = $userID
// 					WHERE
// 						TopicTimeTableID = '$TimeTableID'
// 					";
// 			$this->db_db_query($sql);

			// Insert Data Again
			$sql = "INSERT INTO $TimeTable_Topic_Mapping
						(TopicTimeTableID, TopicID, DateInput, InputBy, DateModified, ModifiedBy, IsDeleted)
					VALUES
						('$TimeTableID', '$topic', now(), $userID, now(), $userID, 0) ";
			return $this->db_db_query($sql);
		}
		
		function DeleteTimeTopicMapping($TimeTableID){
			$TimeTable_Topic_Mapping =  $this->Get_Table_Name('RC_TIMETABLE_TOPIC_MAPPING');
			$userID = $_SESSION['UserID'];
			
			$sql = "UPDATE
						$TimeTable_Topic_Mapping
					SET 
						IsDeleted = 1, DateModified = now(), ModifiedBy = $userID
					WHERE
						TopicTimeTableID = '$TimeTableID'
					AND 
						IsDeleted = 0 ";
			return $this->db_db_query($sql);
		}
		
		function UpdateTimeZoneMapping($TimeTableID, $topic, $ZoneID, $ZoneQuota=array())
		{
		    $TimeTable_Zone_Mapping =  $this->Get_Table_Name('RC_TIMETABLE_ZONE_MAPPING');
		    $ClassZoneQuota_table = $this->Get_Table_Name('RC_TIMETABLE_CLASS_ZONE_QUOTA_MAPPING');
			$userID = $_SESSION['UserID'];
			
			// Clear All the Data
			$this->DeleteTimeZoneMapping($TimeTableID);
			$this->DeleteClassZoneQuotaMapping($TimeTableID, $ZoneQuota);
			
			// Insert Data again
			$insert = '';
			$demeter = '';
			foreach((array)$ZoneID as $_ZoneID)
			{
				$thisZoneQuota = $ZoneQuota[$_ZoneID][0];
				$thisZoneQuotaSQL = $thisZoneQuota ? "'$thisZoneQuota'" : "NULL";
				$insert .= $demeter."('$TimeTableID', '$topic', '$_ZoneID', $thisZoneQuotaSQL, NOW(), $userID, NOW(), $userID, 0)";
				$demeter = ',';
			}
			
			$sql = "INSERT INTO $TimeTable_Zone_Mapping
						(TopicTimeTableID, TopicID, ZoneID, ZoneQuota, DateInput, InputBy, DateModified, ModifiedBy, IsDeleted)
					VALUES
						$insert ";
			$success = $this->db_db_query($sql);
			
			// Insert Zone Class Quota
			if($success)
			{
			    $insert = '';
			    $demeter = '';
			    foreach((array)$ZoneQuota as $_ZoneID => $thisZoneQuota)
			    {
			        foreach((array)$thisZoneQuota as $_YearClassID => $thisZoneClassQuota)
			        {
			            if($_YearClassID == 0) {
			                continue;
			            }
			            
			            $thisZoneClassQuotaSQL = $thisZoneClassQuota ? "'$thisZoneClassQuota'" : "NULL";
			            $insert .= $demeter."('$TimeTableID', '$_ZoneID', '$_YearClassID', $thisZoneClassQuotaSQL, NOW(), $userID, NOW(), $userID, 0)";
			            $demeter = ', ';
			        }
			    }
			    
			    if($insert != '')
			    {
    			    $sql = " INSERT INTO $ClassZoneQuota_table
    			                 (TopicTimeTableID, ZoneID, YearClassID, ClassZoneQuota, DateInput, InputBy, DateModified, ModifiedBy, IsDeleted)
    			             VALUES
    			                 $insert ";
                    $this->db_db_query($sql);
			    }
			}
			
			return $success;
		}
		
		function UpdateTimeZoneQuota($TimeTableID, $topic, $ZoneID, $ZoneQuotaNum)
		{
			if(empty($TimeTableID) || empty($topic) || empty($ZoneID)) {
				return false;
			}
			
			$TimeTable_Zone_Mapping = $this->Get_Table_Name('RC_TIMETABLE_ZONE_MAPPING');
			$ClassZoneQuota_table = $this->Get_Table_Name('RC_TIMETABLE_CLASS_ZONE_QUOTA_MAPPING');
			$userID = $_SESSION['UserID'];
			
			$thisZoneQuotaNum = $ZoneQuotaNum[0];
			$thisZoneQuotaSQL = $thisZoneQuotaNum ? "'$thisZoneQuotaNum'" : "NULL";
			$sql = "UPDATE $TimeTable_Zone_Mapping 
						SET ZoneQuota = $thisZoneQuotaSQL, DateModified = NOW(), ModifiedBy = '$userID'
					WHERE
					   TopicTimeTableID = '$TimeTableID' AND TopicID = '$topic' AND ZoneID = '$ZoneID' AND IsDeleted = 0";
			$success = $this->db_db_query($sql);
			
			// Insert Zone Class Quota
			if($success)
			{
			    if(!empty($ZoneQuotaNum))
			    {
			        foreach((array)$ZoneQuotaNum as $YearClassID => $thisZoneClassQuota)
			        {
			            if($YearClassID == 0) {
			                continue;
			            }
			            
			            $thisZoneClassQuotaSQL = $thisZoneClassQuota ? "'$thisZoneClassQuota'" : "NULL";
			            
			            $sql = "SELECT
                                    MappingID
                                FROM
                                    $ClassZoneQuota_table
			                    WHERE
                                    TopicTimeTableID = '$TimeTableID' AND ZoneID = '$ZoneID' AND YearClassID = '$YearClassID' AND IsDeleted = 0";
                        $mappingID = $this->returnVector($sql);
                        $mappingID = $mappingID[0];
			            
			            if($mappingID)
			            {
			                $sql = "UPDATE $ClassZoneQuota_table
                                        SET ClassZoneQuota = $thisZoneClassQuotaSQL, DateModified = NOW(), ModifiedBy = '$userID'
        			                WHERE
        			                    TopicTimeTableID = '$TimeTableID' AND ZoneID = '$ZoneID' AND YearClassID = '$YearClassID' AND IsDeleted = 0";
			                $this->db_db_query($sql);
			            }
			            else
			            {
			                $sql = " INSERT INTO $ClassZoneQuota_table
			                             (TopicTimeTableID, ZoneID, YearClassID, ClassZoneQuota, DateInput, InputBy, DateModified, ModifiedBy, IsDeleted)
			                         VALUES
			                             ('$TimeTableID', '$ZoneID', '$YearClassID', $thisZoneClassQuotaSQL, NOW(), $userID, NOW(), $userID, 0)";
			                $this->db_db_query($sql);
			            }
			        }
			    }
			}
			
			return $success;
		}
		
		function DeleteTimeZoneMapping($TimeTableID)
		{
			$TimeTable_Zone_Mapping =  $this->Get_Table_Name('RC_TIMETABLE_ZONE_MAPPING');
			$userID = $_SESSION['UserID'];
			
			$sql = "UPDATE
						$TimeTable_Zone_Mapping
					SET
						IsDeleted = 1, DateModified = NOW(), ModifiedBy = $userID
					WHERE
						TopicTimeTableID = '$TimeTableID'
					AND 
						IsDeleted = 0 ";
			return $this->db_db_query($sql);
		}
		
		function DeleteClassZoneQuotaMapping($TimeTableID, $ZoneQuota)
		{
		    $ClassZoneQuota_table = $this->Get_Table_Name('RC_TIMETABLE_CLASS_ZONE_QUOTA_MAPPING');
		    $userID = $_SESSION['UserID'];
		    
		    foreach((array)$ZoneQuota as $thisZoneID => $thisZoneQuota)
		    {
		        foreach((array)$thisZoneQuota as $thisYearClassID => $thisZoneClassQuota)
		        {
		            if($thisYearClassID == 0) {
		                continue;
		            }
		            
        		    $sql = "UPDATE
                                $ClassZoneQuota_table
                		    SET
                    		    IsDeleted = 1, DateModified = NOW(), ModifiedBy = $userID
                		    WHERE
                    		    TopicTimeTableID = '$TimeTableID' AND ZoneID = '$thisZoneID' AND YearClassID = '$thisYearClassID' AND IsDeleted = 0 ";
                    $this->db_db_query($sql);
		        }
		    }
		    //return $this->db_db_query($sql);
		}
		
		function UpdateTimeToolMapping($TimeTableID, $topic, $ZoneID, $toolData_Timetable, $ClearAll=1)
		{
			$TimeTable_Tool_Mapping =  $this->Get_Table_Name('RC_TIMETABLE_TOOL_MAPPING ');
			$userID = $_SESSION['UserID'];
			
			// Clear All the Data
			if($ClearAll){
				$this->DeleteTimeToolMapping($TimeTableID);
			}
			else{
				$this->DeleteTimeToolMapping($TimeTableID ,$ZoneID);
			}
// 			$sql = "
// 					UPDATE
// 						$TimeTable_Tool_Mapping
// 					SET
// 						IsDeleted = 1, DateModified = now(), ModifiedBy = $userID
// 					WHERE
// 						TopicTimeTableID = '$TimeTableID'
// 					";
// 			$this->db_db_query($sql);
			
			// Insert Data again
			$insert = '';
			$demeter = '';
			foreach((array)$ZoneID as $_zoneID) {
				foreach ((array)$toolData_Timetable[$_zoneID] as $_tool) {
					if($_tool > 0) {
						$insert .= $demeter."('$TimeTableID', '$topic', '$_zoneID', '$_tool', NOW(), '$userID', NOW(), '$userID', 0)";
						$demeter = ',';
					}
				}
			}
			
			if($insert == "") {
				return false;
			}
			else {
				$sql = "INSERT INTO $TimeTable_Tool_Mapping
							(TopicTimeTableID, TopicID, ZoneID, ToolCodeID, DateInput, InputBy, DateModified, ModifiedBy, IsDeleted)
						VALUES
							$insert ";
				return $this->db_db_query($sql);
			}
		}
		
		function DeleteTimeToolMapping($TimeTableID, $ZoneID='', $ToolID=''){
			$TimeTable_Tool_Mapping =  $this->Get_Table_Name('RC_TIMETABLE_TOOL_MAPPING ');
			$userID = $_SESSION['UserID'];
			if($ZoneID){
				$ZoneID_Cond = " AND ZoneID = $ZoneID ";
			}
			else{
				$ZoneID_Cond = "";
			}
			if($ToolID){
				$ToolID_Cond = " AND ToolCodeID = $ToolID ";
			}
			else{
				$ToolID_Cond = "";
			}
			
			// Clear All the Data
			$sql = "UPDATE
						$TimeTable_Tool_Mapping
					SET
						IsDeleted = 1, DateModified = now(), ModifiedBy = $userID
					WHERE
						TopicTimeTableID = '$TimeTableID'
						$ZoneID_Cond
						$ToolID_Cond
						AND IsDeleted = 0 ";
			$this->db_db_query($sql);
		}
		
		function GetTimeTable($YearID="", $TimeTableID="", $onlyCurrentTimeTable=false, $SemesterID="", $filterByClassTeacher=false)
		{
		    global $sortByZone;
		    
			$TimeTable_table = $this->Get_Table_Name('RC_TOPICTIMETABLE');
			$TimeTable_Topic_Mapping = $this->Get_Table_Name('RC_TIMETABLE_TOPIC_MAPPING');
			$TimeTable_Zone_Mapping = $this->Get_Table_Name('RC_TIMETABLE_ZONE_MAPPING');
			$TimeTable_Tool_Mapping = $this->Get_Table_Name('RC_TIMETABLE_TOOL_MAPPING ');
			$FormTopics_Table = $this->Get_Table_Name('RC_FORM_TOPICS');
			$LearningZone_Table = $this->Get_Table_Name('RC_LEARNING_ZONE');
			
			$YearCond = "";
			$TimeTableIDCond = "";
			$TimeTableDateRangeCond = "";
			$SemesterCond = "";
            $classTeacherYearCond = "";
			if($YearID) {
				$YearCond = " AND tt.YearID = {$YearID} ";
			}
			if($TimeTableID) {
				$TimeTableIDCond = " AND tt.TopicTimeTableID = $TimeTableID";
			}
			if($onlyCurrentTimeTable) {
				$today = date("Y-m-d H:i:s");
				$TimeTableDateRangeCond = " AND tt.StartDate <= '$today' AND tt.EndDate >= '$today' ";
			}
			if($SemesterID) {
				$allSemAry = getSemesters($this->AcademicYearID, 0);
				foreach($allSemAry as $thisSemInfo)
				{
					if($SemesterID == $thisSemInfo["YearTermID"]) {
						$SemesterCond = " AND ((tt.StartDate BETWEEN '".$thisSemInfo["TermStart"]."' AND '".$thisSemInfo["TermEnd"]."') OR (tt.EndDate BETWEEN '".$thisSemInfo["TermStart"]."' AND '".$thisSemInfo["TermEnd"]."')) ";
					}
				}
			}
			if($filterByClassTeacher) {
                $YearIDArr = Get_Array_By_Key($this->Get_Teaching_Level($_SESSION['UserID']), 'YearID');
                $classTeacherYearCond = " AND tt.YearID IN ('".implode("','", (array)$YearIDArr)."') ";
            }
			
			$zoneSortSql = $sortByZone ? ' ,lzt.ZoneCode ' : '';
			
			$sql = "SELECT 
						DISTINCT tt.*, y.YearName, ttm.TopicID, tttm.ToolCodeID, tzm.ZoneID, tzm.ZoneQuota, ft.TopicNameEN, ft.TopicNameB5, lzt.ZoneNameB5, lzt.ZoneNameEN, lzt.ZoneCode
					FROM
						$TimeTable_table tt
					INNER JOIN
						YEAR y
							ON tt.YearID = y.YearID
					INNER JOIN
						$TimeTable_Topic_Mapping ttm
							ON ttm.TopicTimeTableID  = tt.TopicTimeTableID and ttm.IsDeleted = 0
					INNER JOIN 
						$FormTopics_Table ft
							ON ttm.TopicID = ft.TopicID and ft.IsDeleted = 0
					LEFT JOIN
						$TimeTable_Zone_Mapping tzm
							ON tzm.TopicTimeTableID = tt.TopicTimeTableID and ttm.TopicID = tzm.TopicID and tzm.IsDeleted = 0
					LEFT JOIN
						$LearningZone_Table lzt
							ON lzt.ZoneID = tzm.ZoneID
					LEFT JOIN 
						$TimeTable_Tool_Mapping tttm
							ON tt.TopicTimeTableID = tttm.TopicTimeTableID and tzm.ZoneID = tttm.ZoneID and tttm.IsDeleted = 0
					WHERE 
						tt.IsDeleted = 0
						$YearCond
						$TimeTableIDCond
						$TimeTableDateRangeCond
						$SemesterCond 
						$classTeacherYearCond
					ORDER BY
						y.Sequence, tt.StartDate $zoneSortSql, tttm.ToolCodeID";
			return $this->returnResultSet($sql);
		}
		
		function GetZoneFromMapping($TimeTableID,$topic)
		{
			$TimeTable_Zone_Mapping =  $this->Get_Table_Name('RC_TIMETABLE_ZONE_MAPPING');
			$LearningZone = $this->Get_Table_Name("RC_LEARNING_ZONE");
			
			$sql = "SELECT 
						* 
					FROM
						$TimeTable_Zone_Mapping ttzm
					INNER JOIN
						$LearningZone lz 
							ON lz.ZoneID = ttzm.ZoneID
					WHERE
						ttzm.TopicTimeTableID = $TimeTableID
						AND ttzm.IsDeleted = 0 ";
		}
		
		function GetLearningToolFromMapping($TimeTableID)
		{
			$TimeTable_Tool_Mapping =  $this->Get_Table_Name('RC_TIMETABLE_TOOL_MAPPING');
			$LearningTool = $this->Get_Table_Name('RC_EQUIPMENT');
			
			$sql = "SELECT 
						*
					FROM
						$TimeTable_Tool_Mapping tttm
					INNER JOIN
						$LearningTool lt
							ON lt.CodeID = tttm.ToolCodeID	and lt.IsDeleted = 0
					WHERE
						TopicTimeTableID = $TimeTableID	and tttm.IsDeleted = 0 ";
		}
		
		function GetClassZoneQuotaFromMapping($TimeTableID, $ZoneID='', $YearClassID='')
		{
		    $ClassZoneQuota_table = $this->Get_Table_Name('RC_TIMETABLE_CLASS_ZONE_QUOTA_MAPPING');
		    
		    $ZoneIDCond = "";
		    $YearClassIDCond = "";
		    if($ZoneID) {
		        $ZoneIDCond = " AND ZoneID = '$ZoneID' ";
		    }
		    if($YearClassID) {
		        $YearClassIDCond = " AND YearClassID = '$YearClassID'";
		    }
		    
		    $sql = "SELECT
                        ZoneID, YearClassID, ClassZoneQuota
        		    FROM
                        $ClassZoneQuota_table
        		    WHERE
                        TopicTimeTableID = '$TimeTableID'
                        $ZoneIDCond
                        $YearClassIDCond
                        AND IsDeleted = 0";
            return $this->returnResultSet($sql);
		}
		
		function Get_Student_Display_Photo($thisStudentInfo, $defaultWidth="110")
		{
			global $intranet_root;
			
			// Student Info
			$this_studentid = $thisStudentInfo["UserID"];
			$this_studentUserLogin = $thisStudentInfo["UserLogin"];
			
			// Get all path for checking
			$user_login_path = $intranet_root."/file/user_photo/".$this_studentUserLogin.".jpg";
			$uid_path1 = $intranet_root."/file/photo/personal/". $this_studentid.".jpg";
			$uid_path2 = $intranet_root."/file/photo/personal/". $this_studentid.".JPG";
			
			// Student Photo (UserLogin)
			if(file_exists($user_login_path)) {
				$this_studentPhoto = "<img src='/file/user_photo/".$this_studentUserLogin.".jpg' width='".$defaultWidth."px'>";
			}
			// Personal Photo (UserID)
			else if(file_exists($uid_path1)) {
				$this_studentPhoto = "<img src='/file/photo/personal/p".$this_studentid.".jpg' width='".$defaultWidth."px'>";
			}
			else if(file_exists($uid_path2)) {
				$this_studentPhoto = "<img src='/file/photo/personal/p".$this_studentid.".jpg' width='".$defaultWidth."px'>";
			}
			// Default Photo
			else {
				$this_studentPhoto = "<img src='/images/myaccount_personalinfo/samplephoto.gif' width='".$defaultWidth."px'>";
			}
			
			return $this_studentPhoto;
		}
		
		function Get_Available_Student($StudentAry, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID, $isMarked="all", $toolStatus="all")
		{
			// Marked Student Filtering
			if($isMarked=="all") {
				$StudentIDArr = $StudentAry;
			}
			else {
				// Get Marked Student List
				$markedStudentAry = $this->GET_STUDENT_SCORE($StudentAry, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID);
				$markedStudentIDAry = array_keys((array)$markedStudentAry);
				
				if($isMarked==1) {
					// Marked Student only
					$StudentIDArr = $markedStudentIDAry;
				}
				else {
					// Student without Marking
					$StudentIDArr = array_diff($StudentAry, $markedStudentIDAry);
				}
			}
			
			// Zone Status Filtering
			if($toolStatus=="all") {
				$StudentIDArr = $StudentIDArr;
			}
			else {
				// Student in zone
				if($toolStatus==1) {
					$inZoneStudent = $this->GET_STUDENT_IN_ZONE($StudentIDArr, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID, "", $isUsingTools="1");
					$inZoneStudent = array_keys((array)$inZoneStudent);
					$StudentIDArr = array_intersect($StudentIDArr, $inZoneStudent);
				}
				// Student not using tools in zone before
				else if($toolStatus==2) {
					$inZoneStudent = $this->GET_STUDENT_IN_ZONE($StudentIDArr, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID, "", $isUsingTools="all");
					$inZoneStudent = array_keys((array)$inZoneStudent);
					$StudentIDArr = array_diff($StudentIDArr, $inZoneStudent);
				}
				// Student completed tools in zone before
				else if($toolStatus==3) {
					$inZoneStudent = $this->GET_STUDENT_IN_ZONE($StudentIDArr, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID, "", $isUsingTools="0");
					$inZoneStudent = array_keys((array)$inZoneStudent);
					$StudentIDArr = array_intersect($StudentIDArr, $inZoneStudent);
				}
			}
			
			return $StudentIDArr;
		}
		
		function getTopics($TopicID="", $TopicCatID="", $YearID="", $BuildAssocArr=false, $TermID="", $filterByClassTeacher=false, $excludeSubjectTeacher=false)
		{
		    global $PowerPortfolioConfig, $Lang;

			// Condition
			$TopicIDCond = !empty($TopicID) ? " AND topic.TopicID IN ('".implode("', '", (array)$TopicID)."') " : "";
			$TopicCatIDCond = $TopicCatID == "" ? "" : " AND topic.CatID = '".$TopicCatID."' ";
			$YearIDCond = $YearID == "" ? "" : " AND topic.YearID IN ('0', '".$YearID."') ";
			$TermIDCond = $TermID == "" ? "" : " AND topic.TermID IN ('0', '".$TermID."') ";
			
			$TopicCatTypeCond = "";
			// Normal Subject Teacher filtering
			if(!$this->IS_POWER_PORTFOLIO_ADMIN_USER() && $this->IS_KG_SUBJECT_TEACHER() && !$excludeSubjectTeacher)
			{
			    $related_subject_groups = $this->Get_Teaching_Subject_Group($_SESSION['UserID']);
			    $related_subject_codes = Get_Array_By_Key($related_subject_groups, 'CODEID');
			    
			    $related_indicator_codes = array();
			    foreach($related_subject_codes as $this_subject_code){
			        $related_indicator_codes[] = $PowerPortfolioConfig['subjectIndicatorsCodeMap'][$this_subject_code];
			    }
			    $TopicCatTypeCond .= " AND topic_cat.Code IN ('".implode("', '", (array)$related_indicator_codes)."') ";
			    $TopicCatTypeCond .= " AND topic_cat.Code IN ('".implode("', '", (array)$PowerPortfolioConfig['indicatorsCatCode'])."') ";
			}
			// Class Teacher filtering > normally used by teacher with teaching classess & subjects
			if($filterByClassTeacher)
            {
                $YearIDArr = Get_Array_By_Key($this->Get_Teaching_Level($_SESSION['UserID']), 'YearID');
                $classTeacherYearCond = " AND (y.YearID IN ('".implode("','", (array)$YearIDArr)."') OR y.YearID IS NULL) ";
            }
			
			// Table and Fields
			$table = $this->DBName.".RC_TOPIC";
			$table2 = $this->DBName.".RC_TOPIC_CATEGORY";
			$fields = " topic.TopicID, topic.CatID, topic.YearID, topic.TermID, topic.Code, topic.NameEn, topic.NameCh, topic.DisplayOrder, topic.DateInput, topic.InputBy, topic.DateModified, topic.LastModifiedBy, ";
			$fields2 = " topic_cat.CatType, topic_cat.NameEn as TopicCatNameEn, topic_cat.NameCh as TopicCatNameCh, ";
			
			$sql = "SELECT 
						$fields 
						$fields2 
						IF(y.YearID IS NULL, 'K1 - K3', y.YearName) as YearName,
						IF(y.YearID IS NULL, 999, y.Sequence) as yearSeq,
						topic_cat.Code as TopicCatCode
					FROM 
						$table as topic
					INNER JOIN
						$table2 as topic_cat ON (topic.CatID = topic_cat.TopicCatID AND topic_cat.IsDeleted = '0')
					LEFT JOIN 
						YEAR y ON (topic.YearID = y.YearID)
					WHERE 
						topic.isDeleted = '0' 
						$TopicIDCond 
						$TopicCatIDCond 
                        $TopicCatTypeCond
						$YearIDCond
						$TermIDCond
						$classTeacherYearCond
					ORDER BY
						yearSeq, topic_cat.CatType, topic_cat.DisplayOrder, topic.DisplayOrder ";
			$result = $this->returnArray($sql);

			// Get Semester
			$sem_list = getSemesters($this->AcademicYearID);
			
			// Build Topic array
			for($i=0; $i<sizeof($result); $i++)
			{
				$thisCatType = $result[$i]["CatType"];
				$thisCatTypeName = $Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category".$thisCatType];
				$thisTopicCatName = Get_Lang_Selection($result[$i]["TopicCatNameCh"], $result[$i]["TopicCatNameEn"]);
				
				$result[$i]["CatTypeName"] = $thisCatTypeName;
				$result[$i]["TopicCatTypeName"] = $thisTopicCatName." (".$thisCatTypeName.")";
				
				$thisTopicTermID = $result[$i]["TermID"];
				$thisTopicTermName = $thisTopicTermID ? $sem_list[$thisTopicTermID] : $Lang["General"]["WholeYear"];
				
				$result[$i]["TopicTermName"] = $thisTopicTermName;
			}
			
			// Build assoc array
			if($BuildAssocArr)
			{
				$result = BuildMultiKeyAssoc((array)$result, "TopicID");
			}
			
			return $result;
		}
		
		function getTopicLastDisplayOrder($TopicCatID, $YearID)
		{
			$yearCond = " AND YearID = '".$YearID."' ";
			if($YearID == 0) {
				$yearCond = "";
			}
			
			$table = $this->DBName.".RC_TOPIC";
			$sql = "SELECT Max(DisplayOrder) as MaxDisplayOrder FROM $table WHERE IsDeleted = 0 AND CatID = '".$TopicCatID."' ".$yearCond;
			$resultAry = $this->returnArray($sql);
			return $resultAry[0]["MaxDisplayOrder"];
		}
		
		function updateTopic($parTopicId="", $dataAry, $updateLastModified=true)
		{
			if (!is_array($dataAry) || count($dataAry) == 0) {
				return false;
			}
			
			$table = $this->DBName.".RC_TOPIC";
			
			// Update Topic
			if($parTopicId > 0)
			{
				# Build field update values string
				$valueFieldAry = array();
				foreach ($dataAry as $field => $value) {
					$valueFieldAry[] = " $field = '".$this->Get_Safe_Sql_Query($value)."' ";
				}
				if ($updateLastModified) {
					$valueFieldAry[] = " DateModified = NOW() ";
					$valueFieldAry[] = " LastModifiedBy = '".$_SESSION['UserID']."' ";
				}
				$valueFieldText .= implode(',', $valueFieldAry);
				
				$sql = "UPDATE $table SET $valueFieldText WHERE TopicID = '$parTopicId'";
				$success = $this->db_db_query($sql);
			}
			// Insert Topic
			else
			{
				# Build field insert values string
				$fieldAry = array();
				$valueAry = array();
				foreach ($dataAry as $field => $value) {
					$fieldAry[] = " $field ";
					$valueAry[] = " '".$this->Get_Safe_Sql_Query($value)."' ";
				}
				if ($updateLastModified) {
					$fieldAry[] = " DateInput ";
					$fieldAry[] = " InputBy ";
					$fieldAry[] = " DateModified ";
					$fieldAry[] = " LastModifiedBy ";
					$valueAry[] = " NOW() ";
					$valueAry[] = " '".$_SESSION['UserID']."' ";
					$valueAry[] = " NOW() ";
					$valueAry[] = " '".$_SESSION['UserID']."' ";
				}
				$fieldText .= implode(",", $fieldAry);
				$valueText .= implode(",", $valueAry);
				
				$sql = "INSERT INTO $table ($fieldText) VALUES ($valueText) ";
				$success = $this->db_db_query($sql);
				if($success) {
					$parTopicId = $this->db_insert_id();
				}
			}
			
			return ($success)? $parTopicId : -1;
		}
		
		function deleteTopics($TopicIdAry)
		{
			$table = $this->DBName.".RC_TOPIC";
			$sql = "UPDATE $table SET IsDeleted = 1 WHERE TopicID In ('".implode("', '", (array)$TopicIdAry)."')";
			return $this->db_db_query($sql);
		}
		
		function getTopicCategory($CatType="", $TopicCatID="", $BuildAssocArr=false)
		{
			global $Lang;
			
			// Condition
			$CatTypeCond = $CatType == "" ? "" : " AND CatType = '".$CatType."' ";
			$TopicCatCond = !empty($TopicCatID) ? " AND TopicCatID IN ('".implode("', '", (array)$TopicCatID)."') " : "";
			
			// Table and Fields
			$table = $this->DBName.".RC_TOPIC_CATEGORY";
			$fields = "TopicCatID, CatType, Code, NameEn, NameCh, DisplayOrder, DateInput, InputBy, DateModified, LastModifiedBy";
			
			$sql = "SELECT 
						$fields 
					FROM 
						$table 
					WHERE 
						isDeleted != '1' 
						$CatTypeCond 
						$TopicCatCond 
					ORDER BY
						CatType, DisplayOrder ";
			$result = $this->returnArray($sql);
			
			// Build Topic Category array
			for($i=0; $i<sizeof($result); $i++)
			{
				$thisCatType = $result[$i]["CatType"];
				$result[$i]["CatTypeName"] = $Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category".$thisCatType];
			}
			// Build assoc array
			if($BuildAssocArr)
			{
				$result = BuildMultiKeyAssoc((array)$result, "TopicCatID");
			}
			
			return $result;
		}
		
		function getTopicCategoryLastDisplayOrder($CatType)
		{
			$table = $this->DBName.".RC_TOPIC_CATEGORY";	
			$sql = "SELECT Max(DisplayOrder) as MaxDisplayOrder FROM $table WHERE IsDeleted = 0 AND CatType = '".$CatType."'";
			$resultAry = $this->returnArray($sql);
			return $resultAry[0]["MaxDisplayOrder"];
		}
		
		function updateTopicCategory($parTopicCatId="", $dataAry, $updateLastModified=true)
		{
			if (!is_array($dataAry) || count($dataAry) == 0) {
				return false;
			}
			
			$table = $this->DBName.".RC_TOPIC_CATEGORY";
			
			// Update Category
			if($parTopicCatId > 0)
			{
				# Build field update values string
				$valueFieldAry = array();
				foreach ($dataAry as $field => $value) {
					$valueFieldAry[] = " $field = '".$this->Get_Safe_Sql_Query($value)."' ";
				}
				if ($updateLastModified) {
					$valueFieldAry[] = " DateModified = NOW() ";
					$valueFieldAry[] = " LastModifiedBy = '".$_SESSION['UserID']."' ";
				}
				$valueFieldText .= implode(',', $valueFieldAry);
				
				$sql = "UPDATE $table SET $valueFieldText WHERE TopicCatID = '$parTopicCatId'";
				$success = $this->db_db_query($sql);
			}
			// Insert Category
			else
			{
				# Build field insert values string
				$fieldAry = array();
				$valueAry = array();
				foreach ($dataAry as $field => $value) {
					$fieldAry[] = " $field ";
					$valueAry[] = " '".$this->Get_Safe_Sql_Query($value)."' ";
				}
				if ($updateLastModified) {
					$fieldAry[] = " DateInput ";
					$fieldAry[] = " InputBy ";
					$fieldAry[] = " DateModified ";
					$fieldAry[] = " LastModifiedBy ";
					$valueAry[] = " NOW() ";
					$valueAry[] = " '".$_SESSION['UserID']."' ";
					$valueAry[] = " NOW() ";
					$valueAry[] = " '".$_SESSION['UserID']."' ";
				}
				$fieldText .= implode(",", $fieldAry);
				$valueText .= implode(",", $valueAry);
				
				$sql = "INSERT INTO $table ($fieldText) VALUES ($valueText) ";
				$success = $this->db_db_query($sql);
				if($success) {
					$parTopicCatId = $this->db_insert_id();
				}
			}
			
			return ($success)? $parTopicCatId : -1;
		}
		
		function deleteTopicCategory($TopicCatIdAry)
		{
			$table = $this->DBName.".RC_TOPIC_CATEGORY";
			$sql = "UPDATE $table 
                        SET IsDeleted = 1, LastModifiedBy = '".$_SESSION['UserID']."', DateModified = NOW()
                    WHERE TopicCatID IN ('".implode("', '", (array)$TopicCatIdAry)."')";
			return $this->db_db_query($sql);
		}
		
		function Get_Macau_Taiwan_Category_Mapping($TWCatIds, $targetType)
		{
			$Ability_Cat_Index_Mapping = $this->Get_Table_Name('RC_ABILITY_CATEGORY_INDEX_MAPPING');
			$Ability_Index_Table = $this->Get_Table_Name('RC_ABILITY_INDEX_CATEGORY');
			
			$sql = "SELECT
						aitw.Code as TWItemCode,
						aitw.Name as TWItemName,
						acim.TWItemID,
						aimo.Code as MOItemCode,
						aimo.Name as MOItemName,
						acim.MOItemID
					FROM
						".$Ability_Cat_Index_Mapping." acim
					INNER JOIN 
						".$Ability_Index_Table." aitw ON (acim.TWItemID = aitw.CatID)
					INNER JOIN 
						".$Ability_Index_Table." aimo ON (acim.MOItemID = aimo.CatID)
					Where 
						acim.".$targetType."ItemID IN ('".implode("', '", $TWCatIds)."')";
			$resultAry = $this->returnArray($sql);
			return $resultAry;
		}
		
		function Get_Taiwan_Topic_Comment()
		{
			$Grade_Remarks_Table = $this->Get_Table_Name("RC_ABILITY_GRADE_REMARKS");
			$Grading_Range_Table = $this->Get_Table_Name("RC_ABILITY_GRADING_RANGE"); 
			
			$sql = "SELECT
						grs.CatID,
						gr.UpperLimit,
						grs.Remarks
					FROM
						".$Grade_Remarks_Table." grs
					INNER JOIN
						".$Grading_Range_Table." gr ON (grs.GradingRangeID = gr.GradingRangeID)";
			$resultAry = $this->returnArray($sql);
			$resultAry = BuildMultiKeyAssoc($resultAry, array("CatID", "UpperLimit"), "Remarks", 1);
			return $resultAry;
		}
		
		function GetStudentScoreSummary($StudentIDArr, $YearClassId, $TermId, $returnTopicScoreComment=false)
		{
			$dataAry = array();
			$dataCodeAry = array();
			$ScoreAry = array();
			$CodeScoreAry = array();
			
			$ToolCodeMap = array();
			$CodeMapping = array();
			
			// Student Class Level
			$allClassAry = $this->Get_All_KG_Class();
			$allClassAry = BuildMultiKeyAssoc($allClassAry, "YearClassID", "YearID", 1);
			$targetLevelId = $allClassAry[$YearClassId];
			// Stundet Topic Score Comment
			if($returnTopicScoreComment) {
			    $taiwanTopicCommentAry = $this->Get_Ability_Remark_Comment($targetLevelId, $TermId);
			}
			
			// Build Tools and Topics Mapping [Score - Teaching Tools]
			$TimeTablesAry = $this->GetTimeTable($targetLevelId, "", false, $TermId);
			$TimeTablesToolAry = Get_Array_By_Key($TimeTablesAry, "ToolCodeID");
			$TimeTablesToolAry = array_values(array_unique($TimeTablesToolAry));
			foreach($TimeTablesToolAry as $thisToolIds)
			{
				// Get related Twiwan Topics
				$thisToolTWCatAry = $this->Get_Taiwan_Category_Fm_Mapping($thisToolIds);
				$thisToolTWCatAry = Get_Array_By_Key($thisToolTWCatAry, "CatID");
				
				// Get related Macau Topics
				$thisToolCatAry = $this->Get_Macau_Taiwan_Category_Mapping($thisToolTWCatAry, "TW");
				foreach($thisToolCatAry as $thisToolCatInfo)
				{
					// Map - Teaching Tools & Macau Topic Category
					$thisMOCodeCat = $thisToolCatInfo["MOItemCode"];
					$thisMOCodeCat = substr($thisMOCodeCat, 0, 1);
					$ToolCodeMap[$thisToolIds][$thisMOCodeCat] += 0;
					$ToolCodeMap[$thisToolIds][$thisMOCodeCat]++;
					
					// Map - Teaching Tools & Macau Topic Category & Taiwan Topics
					if($returnTopicScoreComment)
					{
						$thisTWCode = $thisToolCatInfo["TWItemCode"];
						$CodeMapping[$thisToolIds][$thisMOCodeCat][$thisTWCode]["ItemID"] = $thisToolCatInfo["TWItemID"];
						$CodeMapping[$thisToolIds][$thisMOCodeCat][$thisTWCode]["TypeCount"] += 0;
						$CodeMapping[$thisToolIds][$thisMOCodeCat][$thisTWCode]["TypeCount"]++;
					}
				}
			}
			
			// Get Topics Student Score
			$TimeTablesAry = BuildMultiKeyAssoc($TimeTablesAry, array("TopicTimeTableID", "ToolCodeID"), "ToolCodeID", 1);
			foreach($TimeTablesAry as $thisTimeTableId => $thisTopicIDAry)
			{
				$thisTopicIDAry = array_values($thisTopicIDAry);
				$thisTopicScoreAry = $this->GetAllStudentScore($StudentIDArr, $targetLevelId, $thisTimeTableId, $thisTopicIDAry);
				$thisTopicScoreAry = BuildMultiKeyAssoc($thisTopicScoreAry, array("StudentID", "ToolID", "InputScoreID"), "Score", 1);
				foreach((array)$thisTopicScoreAry as $thisStudentId => $thisStudentScoreAry)
				{
					foreach((array)$thisStudentScoreAry as $thisToolIds => $thisStudentTopicScoreAry)
					{
						$topicScoreCount = count((array)$thisStudentTopicScoreAry);
						if($topicScoreCount > 0)
						{
							$thisStudentTopicScore = array_sum((array)$thisStudentTopicScoreAry) / $topicScoreCount;
							
							// loop Macau Topic Category
							$thisTopicRelatedMOCat = $ToolCodeMap[$thisToolIds];
							foreach((array)$thisTopicRelatedMOCat as $thisMOCatType => $thisMOCatRatio)
							{
								$ScoreAry[$thisStudentId][$thisMOCatType]["Score"] += 0;
								$ScoreAry[$thisStudentId][$thisMOCatType]["Score"] += $thisMOCatRatio * $thisStudentTopicScore;
								$ScoreAry[$thisStudentId][$thisMOCatType]["Unit"] += 0;
								$ScoreAry[$thisStudentId][$thisMOCatType]["Unit"] += $thisMOCatRatio;
								
								// loop Taiwan Topic
								if($returnTopicScoreComment)
								{
									$thisTWCodeSettings = $CodeMapping[$thisToolIds][$thisMOCatType];
									foreach((array)$thisTWCodeSettings as $thisTWCode => $thisTWCodeInfo)
									{
										$CodeScoreAry[$thisStudentId][$thisMOCatType][$thisTWCode]["ItemID"] = $thisTWCodeInfo["ItemID"];
										$CodeScoreAry[$thisStudentId][$thisMOCatType][$thisTWCode]["Score"] += 0;
										$CodeScoreAry[$thisStudentId][$thisMOCatType][$thisTWCode]["Score"] += $thisTWCodeInfo["TypeCount"] * $thisStudentTopicScore;
										$CodeScoreAry[$thisStudentId][$thisMOCatType][$thisTWCode]["Unit"] += 0;
										$CodeScoreAry[$thisStudentId][$thisMOCatType][$thisTWCode]["Unit"] += $thisTWCodeInfo["TypeCount"];
									}
								}
							}
						}
					}
				}
			}
			$TimeTablesIDAry = array_keys($TimeTablesAry);
			
// 			debug_pr($CodeScoreAry);
			// Build Topics Mapping [Score - Subjects]
			$validTWItemIDsAry = array();
			$validSubjectMappingAry = array();
			$validItemCategoryMappingAry = array();
			$allSubjectMappingAry = $this->getAllSubjectMapping(true, '', $targetLevelId);
			foreach($allSubjectMappingAry as $thisSubjectMapping) {
			    if($thisSubjectMapping['isTerm'] == 1) {
			        if(!$thisSubjectMapping['TermID'] || $thisSubjectMapping['TermID'] != $TermId) {
			            continue;
			        }
			    } else {
			        if(!$thisSubjectMapping['TopicID'] || !in_array($thisSubjectMapping['TopicID'], (array)$TimeTablesIDAry)) {
			            continue;
			        }
			    }
			    
			    $validSubjectMappingAry[$thisSubjectMapping['CodeID']] = true;
			    foreach((array)$thisSubjectMapping['CataArr'] as $thisSubjectCata) {
			        $validTWItemMappingAry[] = $thisSubjectCata['AbilityCatID'];
			    }
			}
			
			// Map - Taiwan & Macau Item
			$validTWItemIDsAry = array_values(array_unique(array_filter((array)$validTWItemMappingAry)));
			if(!empty($validTWItemIDsAry)) {
			    $thisItemCatAry = $this->Get_Macau_Taiwan_Category_Mapping($validTWItemIDsAry, "TW");
			    foreach($thisItemCatAry as $thisItemlCatInfo) {
			        $validItemCategoryMappingAry[$thisItemlCatInfo['TWItemID']][] = array(substr($thisItemlCatInfo["MOItemCode"], 0, 1), $thisItemlCatInfo["TWItemCode"]);
			    }
			}
			
			// Get Subject Topics Student Score
			$allStudentSubjectScoreAry = $this->GET_STUDENT_SUBJECT_TOPIC_SCORE($StudentIDArr, "", $validTWItemIDsAry, "", $YearClassId, $targetLevelId);
			foreach((array)$allStudentSubjectScoreAry as $thisStudentID => $thisStudentSubjectScoreAry)
			{
			    foreach((array)$thisStudentSubjectScoreAry as $thisCodeID => $thisCodeScoreAry)
			    {
			        if(!isset($validSubjectMappingAry[$thisCodeID])) {
			            continue;
			        }
			        
			        foreach((array)$thisCodeScoreAry as $thisTWItemID => $thisScoreAry)
			        {
			            if(!in_array($thisTWItemID, (array)$validTWItemIDsAry) || $thisScoreAry['Score'] <= 0) {
			                continue;
			            }
			            
			            foreach((array)$validItemCategoryMappingAry[$thisTWItemID] as $thisCategoryMapping)
			            {
			                list($thisMOCatType, $thisTWCode) = $thisCategoryMapping;
			                
			                $ScoreAry[$thisStudentID][$thisMOCatType]["Score"] += 0;
			                $ScoreAry[$thisStudentID][$thisMOCatType]["Score"] += $thisScoreAry['Score'];
			                $ScoreAry[$thisStudentID][$thisMOCatType]["Unit"] += 0;
			                $ScoreAry[$thisStudentID][$thisMOCatType]["Unit"]++;
			                
			                // loop Taiwan Topic
			                if($returnTopicScoreComment)
			                {
			                    $CodeScoreAry[$thisStudentID][$thisMOCatType][$thisTWCode]["ItemID"] = $thisTWItemID;
			                    $CodeScoreAry[$thisStudentID][$thisMOCatType][$thisTWCode]["Score"] += 0;
			                    $CodeScoreAry[$thisStudentID][$thisMOCatType][$thisTWCode]["Score"] += $thisScoreAry['Score'];
			                    $CodeScoreAry[$thisStudentID][$thisMOCatType][$thisTWCode]["Unit"] += 0;
			                    $CodeScoreAry[$thisStudentID][$thisMOCatType][$thisTWCode]["Unit"]++;
			                }
			            }
			        }
			    }
			}
//             debug_pr($ScoreAry);
//             debug_pr($CodeScoreAry);
			
			// Get Topics Score Summary
			foreach($ScoreAry as $thisStudentId => $thisStudentScoreAry)
			{
				foreach((array)$thisStudentScoreAry as $thisMOCatType => $thisStudentMOCatInfo)
				{
					if($thisStudentMOCatInfo["Unit"] > 0)
					{
						// Topic Score (Macau Topic Category)
						$thisMOCatScore = $thisStudentMOCatInfo["Score"] / $thisStudentMOCatInfo["Unit"];
						$dataAry[$thisStudentId][$thisMOCatType] = my_round($thisMOCatScore, 2);
						
						// Topic Score (Taiwan Topic)
						$thisStudentTWCatInfo = $CodeScoreAry[$thisStudentId][$thisMOCatType];
						if($returnTopicScoreComment && !empty($thisStudentTWCatInfo))
						{
							ksort($thisStudentTWCatInfo);
							foreach((array)$thisStudentTWCatInfo as $thisTWCode => $thisTWTypeInfo)
							{
								if($thisTWTypeInfo["Unit"] > 0)
								{
									$thisTWCatScore = $thisTWTypeInfo["Score"] / $thisTWTypeInfo["Unit"];
									$thisTWCatScore = my_round($thisTWCatScore, 2);
									$dataCodeAry[$thisStudentId][$thisMOCatType][$thisTWCode]["Score"] = $thisTWCatScore;
									
									$thisTWCommentAry = $taiwanTopicCommentAry[$thisTWTypeInfo["ItemID"]];
									foreach((array)$thisTWCommentAry as $thisUpperLimit => $thisTWComment)
									{
										if($thisTWCatScore <= $thisUpperLimit) {
											$dataCodeAry[$thisStudentId][$thisMOCatType][$thisTWCode]["Remarks"] = $thisTWComment;
											break;
										}
									}
								}
							}
						}
					}
				} 
			}
// 			debug_pr($dataAry);
// 			debug_pr($dataCodeAry);die();

			return array($dataAry, $dataCodeAry);
		}
		
		/*
		function GetAllStudentScore($StudentIDArr, $ClassLevelID, $TimeTableID, $ToolIDArr)
		{
			// Table and Fields
			$table = $this->DBName.".RC_STUDENT_SCORE";
			$fields = "InputScoreID, StudentID, TimeTableID, ZoneID, ToolID, Score, ScoreGrade, InputBy, DateInput, ModifiedBy, DateModified";
			
			$sql  = "SELECT 
						$fields 
					FROM 
						$table 
					WHERE 
						StudentID IN ('".implode("','", (array)$StudentIDArr)."') AND 
						ClassLevelID = '".$ClassLevelID."' AND 
						TimeTableID = '".$TimeTableID."' AND 
						ToolID IN ('".implode("','", (array)$ToolIDArr)."') AND 
						isDeleted != '1' ";
			return $this->returnArray($sql);
		}
		function GET_STUDENT_SCORE($StudentIDArr, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID, $ToolID="")
		{
			// Condition
			$toolCond = $ToolID==""? "" : "  AND ToolID = '$ToolID' ";
			
			// Table and Fields
			$table = $this->DBName.".RC_STUDENT_SCORE";
			$fields = "InputScoreID, StudentID, ClassLevelID, TimeTableID, TopicID, ZoneID, ToolID, Score, ScoreGrade, InputBy, DateInput, ModifiedBy, DateModified";
			
			$sql  = "SELECT 
						$fields 
					FROM 
						$table 
					WHERE 
						StudentID IN ('".implode("','", (array)$StudentIDArr)."') AND 
						ClassLevelID = '$ClassLevelID' AND 
						TimeTableID = '$TimeTableID' AND 
						TopicID = '$TopicID' AND 
						ZoneID = '$ZoneID'
						$toolCond AND 
						isDeleted != '1' ";
			$result = $this->returnArray($sql);
			
			// Build score data
			$returnArr = array();
			for($i=0; $i<sizeof($result); $i++)
			{
				$thisStudentID = $result[$i]["StudentID"];
				$returnArr[$thisStudentID][] = array(
													"InputScoreID" =>  $result[$i]["InputScoreID"],
													"StudentID" =>  $result[$i]["StudentID"],
													"ClassLevelID" => $result[$i]["ClassLeveID"],
													"TimeTableID" =>  $result[$i]["TimeTableID"],
													"TopicID" =>  $result[$i]["TopicID"],
													"ZoneID" =>  $result[$i]["ZoneID"],
													"ToolID" =>  $result[$i]["ToolID"],
													"Score" =>  $result[$i]["Score"],
													"ScoreGrade" =>  $result[$i]["ScoreGrade"],
													"InputBy" =>  $result[$i]["InputBy"],
													"DateInput" =>  $result[$i]["DateInput"],
													"ModifiedBy" =>  $result[$i]["ModifiedBy"],
													"DateModified" =>  $result[$i]["DateModified"],
												);
			}
			
			return $returnArr;
		}
		
		# INSERT scores without raw data
		function INSERT_STUDENT_SCORE($StudentIDArr, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID, $ToolID, $ModifiedUserID) {
			global $UserID;
			
			// Get student list
			$existStudentID = array_keys($this->GET_STUDENT_SCORE($StudentIDArr, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID, $ToolID));
			$StudentIDArr = array_diff($StudentIDArr, $existStudentID);
			sort($StudentIDArr);
			if (sizeof($StudentIDArr) > 0) {
				$table = $this->DBName.".RC_STUDENT_SCORE";
				$fields = "StudentID, ClassLevelID, TimeTableID, TopicID, ZoneID, ToolID, InputBy, DateInput, ModifiedBy, DateModified";
				$sql = "INSERT INTO $table ($fields) VALUES ";
				for($i=0; $i<sizeof($StudentIDArr); $i++) {
					$entries[] = "(".$StudentIDArr[$i].", '$ClassLevelID', '$TimeTableID', '$TopicID', '$ZoneID', '$ToolID', '$UserID', NOW(), '$UserID', NOW())";
				}
				$sql .= implode(",", $entries);
				
				$success = $this->db_db_query($sql);
				return $success;
			}
			else {
				return true;
			}
		}
		# INSERT scores
		function INSERT_STUDENT_SCORE($ScoreArr)
		{
			global $UserID;
			
			// Table and Fields
			$table = $this->DBName.".RC_STUDENT_SCORE";
			$fields = "StudentID, ClassLevelID, TimeTableID, TopicID, ZoneID, ToolID, Score, isFromMgmt, InputBy, DateInput, ModifiedBy, DateModified";
			$sql = "INSERT INTO $table ($fields) VALUES ";
			
			// loop scores
			$entries = array();
			for($i=0; $i<sizeof($ScoreArr); $i++) {
				$thisScore = $ScoreArr[$i];
				$entries[] = "('".$thisScore["StudentID"]."', '".$thisScore["ClassLevelID"]."', '".$thisScore["TimeTableID"]."', '".$thisScore["TopicID"]."', '".$thisScore["ZoneID"]."', '".$thisScore["ToolID"]."', '".$thisScore["Score"]."', '".$thisScore["isFromMgmt"]."', '$UserID', NOW(), '$UserID', NOW())";
			}
			
			// Insert
			$success = false;
			if(count($entries) > 0) {
				$sql .= implode(", ", $entries);
				$success = $this->db_db_query($sql);
			}
			
			return $success;
		}
		
		# UPDATE scores
		function UPDATE_STUDENT_SCORE($ScoreArr)
		{
			// Table and Fields
			$table = $this->DBName.".RC_STUDENT_SCORE";
			$sqlUpdate = "UPDATE $table SET ";
			for($i=0; $i<sizeof($ScoreArr); $i++)
			{
				$sql = "";
				$sql .= "Score = '".$ScoreArr[$i]["Score"]."', ";
				$sql .= "ScoreGrade = '".$ScoreArr[$i]["ScoreGrade"]."', ";
				$sql .= "ModifiedBy = '".$ScoreArr[$i]["ModifiedBy"]."', ";
				$sql .= "DateModified = NOW() ";
				$sql .= "WHERE ";
				if (isset($ScoreArr[$i]["InputScoreID"]) &&  $ScoreArr[$i]["InputScoreID"] != "") {
					$sql .= "InputScoreID = '".$ScoreArr[$i]["InputScoreID"]."'";
				}
				else {
					$sql .= "StudentID = '".$ScoreArr[$i]["StudentID"]."' AND ";
					$sql .= "ClassLevelID = '".$ScoreArr[$i]["ClassLevelID"]."' AND ";
					$sql .= "TimeTableID = '".$ScoreArr[$i]["TimeTableID"]."' AND ";
					$sql .= "TopicID = '".$ScoreArr[$i]["TopicID"]."' AND ";
					$sql .= "ZoneID = '".$ScoreArr[$i]["ZoneID"]."' AND ";
					$sql .= "ToolID = '".$ScoreArr[$i]["ToolID"]."'";
				}
				$sql = $sqlUpdate.$sql;
				
				$success[] = $this->db_db_query($sql);
			}
			return !in_array(false, $success);
		}
		
		# REMOVE scores
		function DELETE_STUDENT_SCORE($StudentScoreID)
		{
			$table = $this->DBName.".RC_STUDENT_SCORE";
			$sql = "UPDATE $table SET isDeleted = '1' WHERE InputScoreID = '$StudentScoreID'";
			$success = $this->db_db_query($sql);
			return $success;
		}
		*/
		function GET_STUDENT_TOPIC_SCORE($StudentIDArr, $ClassLevelID="", $TopicID="", $TopicCatID="", $TermID="")
		{
			// Condition
			$termCond  = $TermID == "" ? "" : " TermID = '".$TermID."' AND ";
			$levelCond = $ClassLevelID == "" ? "" : " ClassLevelID = '".$ClassLevelID."' AND ";
			$topicCond = empty($TopicID) ? "" : " TopicID IN ('".implode("','", (array)$TopicID)."') AND ";
			
			// Condition - Get Topics from Category
			$topicCatCond = "";
			if(empty($TopicID) && $TopicCatID > 0) {
				// $thisTopicList = $this->getTopics("", $TopicCatID, $ClassLevelID);
                $thisTopicList = $this->getTopics("", $TopicCatID, $ClassLevelID, false, "", false, $excludeSubjectTeacher=true);
                $thisTopicList = Get_Array_By_Key((array)$thisTopicList, "TopicID");
				$topicCatCond = " TopicID IN ('".implode("','", (array)$thisTopicList)."') AND ";
			}
			
			$table = $this->DBName.".RC_TOPIC_SCORE";
			$fields = "TopicScoreID, StudentID, ClassLevelID, TopicID, TermID, Score, ScoreGrade, InputBy, DateInput, ModifiedBy, DateModified";
			$sql  = "SELECT 
						$fields 
					FROM 
						$table 
					WHERE 
						StudentID IN ('".implode("','", (array)$StudentIDArr)."') AND
						$termCond
						$levelCond
						$topicCond
						$topicCatCond
						isDeleted = '0' ";
			$result = $this->returnArray($sql);
			
			// Build score data
			$returnArr = array();
			for($i=0; $i<sizeof($result); $i++)
			{
				$thisTopicID = $result[$i]["TopicID"];
				$thisStudentID = $result[$i]["StudentID"];
				$returnArr[$thisStudentID][$thisTopicID] = $result[$i];
			}
			
			return $returnArr;
		}
		
		# INSERT scores
		function INSERT_STUDENT_TOPIC_SCORE($ScoreArr)
		{
			global $UserID;
			
			// Define Table and Fields
			$table = $this->DBName.".RC_TOPIC_SCORE";
			$fields = "StudentID, ClassLevelID, TopicID, TermID, Score, InputBy, DateInput, ModifiedBy, DateModified";
			$sql = "INSERT INTO $table ($fields) VALUES ";
			
			$entries = array();
			for($i=0; $i<sizeof($ScoreArr); $i++) {
				$thisScore = $ScoreArr[$i];
				$entries[] = "('".$thisScore["StudentID"]."', '".$thisScore["ClassLevelID"]."', '".$thisScore["TopicID"]."', '".$thisScore["TermID"]."', '".$thisScore["Score"]."', '$UserID', NOW(), '$UserID', NOW())";
			}
			
			// Insert
			$success = false;
			if(count($entries) > 0) {
				$sql .= implode(", ", $entries);
				$success = $this->db_db_query($sql);
			}
			
			return $success;
		}
		
		# UPDATE scores
		function UPDATE_STUDENT_TOPIC_SCORE($ScoreArr)
		{
			$table = $this->DBName.".RC_TOPIC_SCORE";
			$sqlUpdate = "UPDATE $table SET ";
			for($i=0; $i<sizeof($ScoreArr); $i++)
			{
				$sql = "";
				$sql .= "Score = '".$ScoreArr[$i]["Score"]."', ";
				//$sql .= "ScoreGrade = '".$ScoreArr[$i]["ScoreGrade"]."', ";
				$sql .= "ModifiedBy = '".$ScoreArr[$i]["ModifiedBy"]."', ";
				$sql .= "DateModified = NOW() ";
				
				$sql .= "WHERE ";
				if (isset($ScoreArr[$i]["TopicScoreID"]) &&  $ScoreArr[$i]["TopicScoreID"] != "") {
					$sql .= "TopicScoreID = '".$ScoreArr[$i]["TopicScoreID"]."'";
				}
				else {
					$sql .= "StudentID = '".$ScoreArr[$i]["StudentID"]."' AND ";
					$sql .= "ClassLevelID = '".$ScoreArr[$i]["ClassLevelID"]."' AND ";
					$sql .= "TopicID = '".$ScoreArr[$i]["TopicID"]."' AND ";
					$sql .= "TermID = '".$ScoreArr[$i]["TermID"]."'";
				}
				$sql = $sqlUpdate.$sql;
				
				$success[] = $this->db_db_query($sql);
			}
			return !in_array(false, $success);
		}
		
		# REMOVE scores
//		function DELETE_STUDENT_TOPIC_SCORE($StudentScoreID) {
//			$table = $this->DBName.".RC_TOPIC_SCORE";
//			$sql = "UPDATE $table SET isDeleted = '1' WHERE InputScoreID = '$StudentScoreID'";
//			$success = $this->db_db_query($sql);
//			return $success;
//		}
		
		function INSERT_STUDENT_ENTRY_LOG($LogInfoArr)
		{
			global $UserID;
			
			if (sizeof($LogInfoArr) > 0)
			{
				// Insert Log
				$table = $this->DBName.".STUDENT_ACCESS_LOG";
				$fields = "StudentID, ClassLevelID, TimeTableID, TopicID, ZoneID, StartTime, DateInput, DateModified";
				$sql = "INSERT INTO $table ($fields) VALUES ";
				for($i=0; $i<sizeof($LogInfoArr); $i++) {
					$entries[] = "('".$LogInfoArr[$i]["StudentID"]."', '".$LogInfoArr[$i]["ClassLevelID"]."', '".$LogInfoArr[$i]["TimeTableID"]."', '".$LogInfoArr[$i]["TopicID"]."', '".$LogInfoArr[$i]["ZoneID"]."', NOW(), NOW(), NOW())";
				}
				$sql .= implode(",", $entries);
				
				$success = $this->db_db_query($sql);
				return $success;
			}
		}
		
		function INSERT_STUDENT_LEAVE_LOG($LogInfoArr)
		{
			global $UserID;
			
			if (sizeof($LogInfoArr) > 0)
			{
				$table = $this->DBName.".STUDENT_ACCESS_LOG";
				
				// Set Other Access Exit
				for($i=0; $i<sizeof($LogInfoArr); $i++)
				{
					$sql = "Update 
								$table 
							SET 
								ToolID = '".$LogInfoArr[$i]["ToolID"]."', EndTime = NOW(), DateModified = NOW()
							WHERE 
								StudentID = '".$LogInfoArr[$i]["StudentID"]."' AND 
								ClassLevelID = '".$LogInfoArr[$i]["ClassLevelID"]."' AND 
								TimeTableID = '".$LogInfoArr[$i]["TimeTableID"]."' AND 
								TopicID = '".$LogInfoArr[$i]["TopicID"]."' AND 
								ZoneID = '".$LogInfoArr[$i]["ZoneID"]."' AND 
								ToolID IS NULL AND 
								EndTime IS NULL";
					$success[] = $this->db_db_query($sql);
				}
				
				return $success;
			}
		}
		
		function GET_STUDENT_IN_ZONE($StudentIDArr, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID, $ToolID, $isUsingTools="all")
		{
			$toolCond = $ToolID == "" ? "" : " AND ToolID = '$ToolID' ";
			$statusCond = $isUsingTools == "all" ? "" : ($isUsingTools == 1 ? " AND StartTime LIKE '".(date('Y-m-d'))."%' AND EndTime IS NULL " : " AND EndTime IS NOT NULL ");
			
			$table = $this->DBName.".STUDENT_ACCESS_LOG";
			$fields = "AccessLogID, StudentID, ClassLevelID, TimeTableID, TopicID, ZoneID, ToolID, StartTime, EndTime, DateInput, DateModified";
			
			$sql  = "SELECT 
						$fields 
					FROM 
						$table 
					WHERE 
						StudentID IN ('".implode("', '", (array)$StudentIDArr)."') AND 
						ClassLevelID = '$ClassLevelID' AND 
						TimeTableID = '$TimeTableID' AND 
						TopicID = '$TopicID' AND 
						ZoneID = '$ZoneID'
						$toolCond
						$statusCond";
			$result = $this->returnArray($sql);
			
			// Build score data
			$returnArr = array();
			for($i=0; $i<sizeof($result); $i++) {
				$thisStudentID = $result[$i]["StudentID"];
				$returnArr[$thisStudentID][] = array(
													"AccessLogID" =>  $result[$i]["AccessLogID"],
													"StudentID" =>  $result[$i]["StudentID"],
													"ClassLevelID" => $result[$i]["ClassLeveID"],
													"TimeTableID" =>  $result[$i]["TimeTableID"],
													"TopicID" =>  $result[$i]["TopicID"],
													"ZoneID" =>  $result[$i]["ZoneID"],
													"ToolID" =>  $result[$i]["ToolID"],
													"StartTime" =>  $result[$i]["StartTime"],
													"EndTime" =>  $result[$i]["EndTime"],
													"DateInput" =>  $result[$i]["DateInput"],
													"DateModified" =>  $result[$i]["DateModified"],
												);
			}
			
			return $returnArr;
		}
		
		function checkStudentInAnotherZone($StudentID, $ZoneID)
		{		    
		    $sql  = " SELECT ZoneID FROM ".$this->DBName.".STUDENT_ACCESS_LOG
		              WHERE
		                  StudentID = '$StudentID' AND
                          ZoneID > 0 AND ZoneID != '$ZoneID' AND
                          StartTime LIKE '".(date('Y-m-d'))."%' AND EndTime IS NULL";
		    return $this->returnVector($sql);
		}
		
		function CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($Mark, $NoOfDecimal)
		{
			$returnVal = '';
			
			if($Mark != "" && $NoOfDecimal > 0){
				$len = strlen($Mark);
				$pos = strpos($Mark, ".");
				if(!$pos){		// Input Value is integer
					$returnVal = str_pad($Mark.".", $len+1+$NoOfDecimal, "0", STR_PAD_RIGHT);
				}
				else {			// Input Value is in decimal places
					$returnVal = str_pad($Mark, $len+($NoOfDecimal-($len-($pos+1))), "0", STR_PAD_RIGHT);
				}
			}
			else {
				$returnVal = $Mark;
			}
			return $returnVal;
		}
		
		// ############### Other Info ################
		// Check available config files
		function getOtherInfoType()
		{
		    $configFilesPath = $this->configFilesPath;
		    $suffix = "_config.csv";
		    
		    $returnArr = array();
		    $configFilesType = $this->configFilesType;
            for($i = 0; $i < sizeof($configFilesType); $i++)
            {
                $filename = $configFilesPath.$configFilesType[$i].$suffix;
                if(file_exists($filename)) {
                    $returnArr[] = $configFilesType[$i];
                }
            }
            return $returnArr;
		}
		
		// Create tabs on Other Info upload page
		function getOtherInfoTabObjArr($UploadType)
		{
		    global $Lang, $PATH_WRT_ROOT;
		    
		    $TAGS_OBJ = array();
		    $otherInfoTypeAry = $this->getOtherInfoType();
		    for ($i = 0; $i < sizeof($otherInfoTypeAry); $i++)
		    {
		        $thisOtherInfoType = $otherInfoTypeAry[$i];
		        $thisTitleDisplay = $Lang['eReportCardKG']['Management']['OtherInfoArr'][$thisOtherInfoType];
		        //$thisURL = $thisOtherInfoType;
		        
		        $thisSelected = ($thisOtherInfoType == $UploadType) ? true : false;
		        $thisSelected = ($i == 0 && !isset($UploadType)) ? true : $thisSelected;
		        
		        $TAGS_OBJ[] = array($thisTitleDisplay, $thisOtherInfoType, $thisSelected);
		    }
		    return $TAGS_OBJ;
		}
		
		// Get array containing upload types
		function getOtherInfoConfig($UploadType)
		{
		    global $intranet_root, $PATH_WRT_ROOT;
		    
		    $UploadType = $UploadType == '' ? 'summary' : $UploadType;
		    $configFilesType = $this->getOtherInfoType();
		    if (!in_array($UploadType, $configFilesType)) {
		        return false;
		    }
		    
		    $configFilesPath = $this->configFilesPath;
		    $suffix = "_config.csv";
		    $fileName = $configFilesPath.$UploadType.$suffix;
		    if (file_exists($fileName))
		    {
		        include_once($PATH_WRT_ROOT."includes/libimporttext.php");
		        $limport = new libimporttext();
		        
		        $data = $limport->GET_IMPORT_TXT($fileName);
		        $header = array_shift($data);
		        if (sizeof($data) > 0) {
		            $infoData = array();
		            for($i = 0; $i < sizeof($data); $i++) {
		                for($j = 0; $j < sizeof($header); $j++) {
		                    $infoData[$i][$header[$j]] = $data[$i][$j];
		                }
		            }
		            return $infoData;
		        }
		        else {
		            return false;
		        }
		    }
		    else
		    {
		        return false;
		    }
		}
		
		function getReportOtherInfoData($TermID, $UploadType, $ClassID = '', $StudentID = '', $YearID = '', $ArrayWithType = 0)
		{
		    global $PATH_WRT_ROOT;
		    
		    $csvType = $this->getOtherInfoType();
		    if (!empty($UploadType) && !is_array($UploadType)) {
		        $UploadType = array($UploadType);
		    }
		    
            // Get Report Student
		    $StudentArr = array();
            if($ClassID == '' && $StudentID == '') {
                $StudentArr = $this->Get_Student_By_Class('', '', 0, 0, 0, 0, $YearID);
            } else {
                if($ClassID) {
                    $StudentArr = $this->Get_Student_By_Class($ClassID);
                } else {
                    $StudentArr[] = array("UserID" => $StudentID);
                }
            }
            $StudentIDAry = Get_Array_By_Key((array)$StudentArr, "UserID");
            
            include_once ($PATH_WRT_ROOT."includes/libuser.php");
            $lu = new libuser('', '', $StudentIDAry);
            
            $returnAry = array();
            foreach ((array)$StudentArr as $StudentInfo)
            {
                $StudentID = $StudentInfo['UserID'];
                if ($StudentID) {
                    $lu->LoadUserData($StudentID);
                    if (!$ClassID) {
                        $ClassInfo = $this->Get_Student_Class_ClassLevel_Info($StudentID);
                        $thisClassID = $ClassInfo[0]['ClassID'];
                    } else {
                        $thisClassID = $ClassID;
                    }
                }
                
                // loop CSV Type
                $ary = array();
                foreach ((array)$csvType as $k => $Type)
                {
                    if (!empty($UploadType) && !in_array($Type, $UploadType)) {
                        continue;
                    }
                    
                    // Get CSV data
                    $csvData = $this->getOtherInfoData($Type, $TermID, $thisClassID, $YearID);
                    
                    // loop CSV Type Config
                    $config = $this->getOtherInfoConfig($Type);
                    $ConfigTitleArr = Get_Array_By_Key($config, "EnglishTitle");
                    foreach ((array)$ConfigTitleArr as $key)
                    {
                        $key = trim($key);
                        if (strtoupper($key) == "REGNO") {
                            continue;
                        }
                        $val = $csvData[$StudentID][$key];
                        
                        // Separate string and non-string handling
                        $isCSVDataValid = (is_string($val) && trim($val) != '') || (!is_string($val) && !empty($val));
                        if ($isCSVDataValid)
                        {
                            if ($ArrayWithType == 1) {
                                $ary[$TermID][$Type][$key] = $val;
                            } else {
                                $ary[$TermID][$key] = $val;
                            }
                        }
                        
                        if ($TermID != 0)
                        {
                            if ($ArrayWithType == 1) {
                                if (is_numeric($ary[$TermID][$Type][$key])) {
                                    $ary[0][$Type][$key] += $ary[$TermID][$Type][$key];
                                }
                            } else {
                                if (is_numeric($ary[$TermID][$key])) {
                                    $ary[0][$key] += $ary[$TermID][$key];
                                }
                            }
                        }
                    }
                }
                
                $returnAry[$StudentID] = $ary;
            }
            
            return $returnAry;
		}
		
		function getOtherInfoData($UploadType, $TermID, $ClassID, $ClassLevelID = '')
		{
		    $configFilesType = $this->getOtherInfoType();
		    if (!in_array($UploadType, $configFilesType)) {
		        return false;
		    }
		    
		    $ClassLevelID = $ClassLevelID ? $ClassLevelID : '';
		    $StudentList = $this->Get_Student_By_Class($ClassID, '', 0, 0, 0, 0, $ClassLevelID);
		    $StudentIDArr = Get_Array_By_Key($StudentList, "UserID");
		    $otherInfoData = $this->getStudentOtherInfoData($UploadType, $TermID, $ClassID, $StudentIDArr, '', $ClassLevelID);
		    
		    $returnArr = array();
		    $dataSize = sizeof($otherInfoData);
		    for ($i = 0; $i < $dataSize; $i++)
		    {
		        $varArr = explode("\n", $otherInfoData[$i]['Information']);
		        $ItemCode = trim($otherInfoData[$i]['ItemCode']);
		        if (count($varArr) > 1) {
		            $returnArr[$otherInfoData[$i]['StudentID']][$ItemCode] = array_trim($varArr);
		        }
	            else {
	                $returnArr[$otherInfoData[$i]['StudentID']][$ItemCode] = trim($varArr[0]);
	            }
		    }
		    
		    return $returnArr;
		}
		
		function getStudentOtherInfoData($UploadType = '', $TermID = '', $ClassID = '', $StudentID = '', $ItemCode = '', $YearID = '')
		{
		    if (trim($UploadType) != '') {
		        $cond_UploadType = " AND oisr.UploadType IN ('".implode("','", (array)$UploadType)."') ";
		    }
		    if (trim($TermID) != '') {
		        $cond_TermID = " AND oisr.TermID IN ('".implode("','", (array)$TermID)."') ";
		    }
		    if (trim($ClassID) != '') {
		        $cond_ClassID = " AND oisr.ClassID IN ('".implode("','", (array)$ClassID)."') ";
		    }
		    if (!empty($StudentID)) {
                $cond_StudentID = " AND oisr.StudentID IN ('".implode("','", (array)$StudentID)."') ";
            }
            if (trim($ItemCode) != '') {
                $cond_ItemCode = " AND oisr.ItemCode IN ('".implode("','", (array)$ItemCode)."') ";
            }
            if (trim($YearID) != '') {
                $cond_YearID = " AND yc.YearID IN ('".implode("','", (array)$YearID)."') ";
                $LEFT_JOIN_YEAR_CLASS = "LEFT JOIN YEAR_CLASS yc ON yc.YearClassID = oisr.ClassID";
                $OrderBy = " ORDER BY yc.Sequence ";
            }
            else {
                $LEFT_JOIN_YEAR_CLASS = "LEFT JOIN YEAR_CLASS yc ON yc.YearClassID = oisr.ClassID";
                $LEFT_JOIN_YEAR = "LEFT JOIN YEAR y ON y.YearID = yc.YearID";
                $OrderBy = " ORDER BY y.Sequence, yc.Sequence ";
            }
            
            $RC_OTHER_INFO_STUDENT_RECORD = $this->DBName.".RC_OTHER_INFO_STUDENT_RECORD";
            $sql = "SELECT
                        oisr.TermID,
                        oisr.UploadType,
                        oisr.StudentID,
                        oisr.ItemCode,
                        oisr.Information
                    FROM
                        $RC_OTHER_INFO_STUDENT_RECORD oisr
                        $LEFT_JOIN_YEAR_CLASS
                        $LEFT_JOIN_YEAR
                    WHERE
                        1
                        $cond_UploadType
                        $cond_TermID
                        $cond_ClassID
                        $cond_StudentID
                        $cond_ItemCode
                        $cond_YearID
                    $OrderBy ";
            return $this->returnArray($sql);
		}
		
		function getOtherInfoClassLastModified($UploadType, $TermID, $ClassID = '')
		{
		    global $intranet_db;
		    
		    $UploadType = $UploadType == '' ? 'summary' : $UploadType;
		    
		    if ($ClassID != '' && !is_array($ClassID)) {
		        $ClassIDAry = array($ClassID);
		    }
		    else if (is_array($ClassID)) {
		        $ClassIDAry = $ClassID;
		    }
		    if (count($ClassIDAry) > 0) {
		        $cond_ClassID = " AND oisr.ClassID IN ('".implode("', '", (array)$ClassIDAry)."') ";
		    }
		    
		    $NameField = getNameFieldByLang("iu.");
	        
	        $RC_OTHER_INFO_STUDENT_RECORD = $this->DBName . ".RC_OTHER_INFO_STUDENT_RECORD";
	        $sql = "SELECT
                        oisr.ClassID,
                        $NameField NameField,
                        oisr.DateInput
                    FROM
                        $RC_OTHER_INFO_STUDENT_RECORD oisr
                        LEFT JOIN $intranet_db.INTRANET_USER iu ON iu.UserID = oisr.InputBy
                    WHERE
                        oisr.UploadType  = '$UploadType' AND 
                        oisr.TermID = '$TermID'
                        $cond_ClassID
                    GROUP BY
                        oisr.ClassID ";
	        return $this->returnArray($sql);
		}
		
		function updateStudentOtherInfoData($TermID, $UploadType, $OtherInfoArr, $YearID = '', $YearClassID = '')
		{
		    if (trim($YearID) != '')
		    {
		        $ClassID = array();
		        $ClassArr = $this->GetAllClassesInTimeTable();
		        foreach($ClassArr as $thisClassInfo)
		        {
		            if($thisClassInfo['ClassLevelID'] == $YearID)
		            {
		                $ClassID[] = $thisClassInfo['YearClassID'];
		            }
		        }
		    }
		    else if ($YearClassID != '') {
		        $ClassID = $YearClassID;
		    }
		    else {
		        $ClassID = array_keys($OtherInfoArr);
		    }
		    
		    $UploadType = $UploadType == '' ? 'summary' : $UploadType;
		    
		    $success["Delete"] = $this->deleteStudentOtherInfoData($TermID, $UploadType, $ClassID);
		    $success["Insert"] = $this->insertStudentOtherInfoData($TermID, $UploadType, $OtherInfoArr);
		    
		    return !in_array(false, (array)$success);
		}
		
		function deleteStudentOtherInfoData($TermID, $UploadType, $ClassID)
		{
		    $RC_OTHER_INFO_STUDENT_RECORD = $this->DBName.".RC_OTHER_INFO_STUDENT_RECORD";
		    $sql = "DELETE FROM
		                $RC_OTHER_INFO_STUDENT_RECORD
	                WHERE
                        TermID = '$TermID' AND 
                        UploadType = '".$this->Get_Safe_Sql_Query($UploadType)."' AND 
                        ClassID IN ('".implode("', '", (array)$ClassID)."') ";
		    return $this->db_db_query($sql);
		}
		
		function insertStudentOtherInfoData($TermID, $UploadType, $ClassOtherInfoArr)
		{
		    $i = 0;
		    foreach ((array)$ClassOtherInfoArr as $ClassID => $OtherInfoArr)
		    {
		        foreach ((array)$OtherInfoArr as $StudentID => $StudentOtherInfo)
		        {
		            foreach ((array)$StudentOtherInfo as $ItemCode => $OtherInfoVal)
		            {
		                if ($OtherInfoVal != '')
		                {
		                    $OtherInfoVal = str_replace('<!--linebreakhere-->', "\r\n", $OtherInfoVal);
		                    $RecordSqlGroupArr[$i][] = "('$TermID', '$UploadType', '$StudentID', '".$this->Get_Safe_Sql_Query($ItemCode)."', '".$this->Get_Safe_Sql_Query($OtherInfoVal)."', '$ClassID', NOW(), '".$_SESSION['UserID']."')";
		                    
		                    if (++$ctr % 100 == 0) {
		                        $i++;
		                    }
		                }
		            }
		        }
		    }
		    
		    if (count($RecordSqlGroupArr) > 0)
		    {
		        foreach ($RecordSqlGroupArr as $RecordSqlArr)
		        {
		            $RecordSql = implode(",\n", $RecordSqlArr);
		            
		            $RC_OTHER_INFO_STUDENT_RECORD = $this->DBName.".RC_OTHER_INFO_STUDENT_RECORD";
		            $sql = "INSERT INTO
                                $RC_OTHER_INFO_STUDENT_RECORD
                                (TermID, UploadType, StudentID, ItemCode, Information, ClassID, DateInput, InputBy)
		                    VALUES
                                $RecordSql ";
		            $success[] = $this->db_db_query($sql);
		        }
		        
		        return !in_array(false, $success);
		    }
		    else
		    {
		        return true;
		    }
		}
		
		// Ability Remark functions
		function Get_Ability_Remark_Comment($YearID, $TermID)
		{
		    $Grade_Remarks_Table = $this->Get_Table_Name("RC_ABILITY_GRADE_REMARKS");
		    $Grade_RemarksAll_Table = $this->Get_Table_Name("RC_ABILITY_GRADE_REMARKS_ALL");
		    $Grade_RemarksCustom_Table = $this->Get_Table_Name("RC_ABILITY_GRADE_REMARKS_CUSTOM");
		    $Grading_Range_Table = $this->Get_Table_Name("RC_ABILITY_GRADING_RANGE");
		    
		    $sql = "SELECT
						grs.CatID,
						gr.UpperLimit,
						IF(grc.Remarks IS NULL, IF(gra.Remarks IS NULL, grs.Remarks, gra.Remarks), grc.Remarks) as Remarks
					FROM
						".$Grade_Remarks_Table." grs
					INNER JOIN
						".$Grading_Range_Table." gr ON (grs.GradingRangeID = gr.GradingRangeID)
                    LEFT OUTER JOIN
                        ".$Grade_RemarksAll_Table." gra ON (gra.AbilityRemarkID = grs.AbilityRemarkID)
                    LEFT OUTER JOIN
                        ".$Grade_RemarksCustom_Table." grc ON (grc.AbilityRemarkID = grs.AbilityRemarkID
                                                                 AND grc.YearID = '$YearID' AND grc.YearTermID = '$TermID')";
		    $resultAry = $this->returnArray($sql);
		    
		    $resultAry = BuildMultiKeyAssoc($resultAry, array("CatID", "UpperLimit"), "Remarks", 1);
		    return $resultAry;
		}
		
		function getAbilityOriRemark($IndexID='', $CatID='', $RangeID='')
		{
		    $cond = ' AND ';
		    if($IndexID != '') {
		        $cond .= " AbilityRemarkID = '$IndexID' ";
		    }
		    else if($CatID != '' && $RangeID != '') {
		        $cond .= " CatID = '$CatID' AND GradingRangeID  = '$RangeID' ";
		    }
		    else {
		        $cond .= " 0 ";
		    }
		    
		    $sql = "SELECT
                        AbilityRemarkID,
                        CatID,
                        GradingRangeID,
                        Remarks,
                        DateInput,
                        InputBy,
                        DateModified,
                        ModifiedBy
                    FROM
                        $this->DBName.RC_ABILITY_GRADE_REMARKS
                    WHERE
                        1
                        $cond";
            $result = $this->returnArray($sql);
            return $result;
		}
		
		function getAbilityRemarkAllSingle($IndexID)
		{
		    $sql = "SELECT 
                        AbilityRemarkID,
                        CatID,
                        GradingRangeID,
                        Remarks,
                        DateInput,
                        InputBy,
                        DateModified,
                        ModifiedBy
                    FROM
                        $this->DBName.RC_ABILITY_GRADE_REMARKS_ALL
                    WHERE
                        AbilityRemarkID = '$IndexID'";
		    $result = $this->returnArray($sql);
		    return $result;
		}
		
		function getAbilityRemarkCustomSingle($IndexID, $YearID, $TermID)
		{
		    $sql = "SELECT 
                        CustomRemarkID,
                        AbilityRemarkID,
                        CatID,
                        GradingRangeID,
                        Remarks,
                        DateInput,
                        InputBy,
                        DateModified,
                        ModifiedBy,
                        YearID,
                        YearTermID
                    FROM
                        $this->DBName.RC_ABILITY_GRADE_REMARKS_CUSTOM
                    WHERE
                        AbilityRemarkID = '$IndexID' AND 
                        YearID = '$YearID' AND 
                        YearTermID = '$TermID'";
		    $result = $this->returnArray($sql);
		    return $result;
		}
		
		function getAbilityRemarkTargetMOCat($AbilityID='', $TermID='', $YearID='', $TargetMO='')
		{
		    $cond = "";
		    if(is_array($AbilityID) || (!is_array($AbilityID) && $AbilityID != '')) {
		        $cond .= " AND agrtc.CatID IN ('".implode("', '", (array)$AbilityID)."') ";
		    }
		    if(is_array($TermID) || (!is_array($TermID) && $TermID != '')) {
		        $cond .= " AND agrtc.YearTermID IN ('".implode("', '", (array)$TermID)."') ";
		    }
		    if(is_array($YearID) || (!is_array($YearID) && $YearID != '')) {
		        $cond .= " AND agrtc.ClassLevelID IN ('".implode("', '", (array)$YearID)."') ";
		    }
		    if(is_array($TargetMO) || (!is_array($TargetMO) && $TargetMO != '')) {
		        $cond .= " AND agrtc.TargetMOCat IN ('".implode("', '", (array)$TargetMO)."') ";
		    }
		    
		    $sql = "SELECT
                        agrtc.CatID,
                        aic.Code,
                        agrtc.YearTermID,
                        agrtc.ClassLevelID,
                        agrtc.TargetMOCat 
                    FROM
                        $this->DBName.RC_ABILITY_GRADE_REMARKS_TARGET_CAT agrtc
                        INNER JOIN $this->DBName.RC_ABILITY_INDEX_CATEGORY aic ON (agrtc.CatID = aic.CatID)
                    WHERE
                        1
                        $cond ";
                        $result = $this->returnArray($sql);
            return $result;
		}
		
		function getYearName($YearID)
		{
		    $sql = "SELECT YearName FROM YEAR
                    WHERE YearID = '$YearID'";
		    $result = $this->returnArray($sql);
		    return $result[0]['YearName'];
		}
		
		function updateAbilityOriRemark($IndexCatID, $GradingRangeID, $IndexRemark = '')
		{
		    $targetTable = $this->DBName.".RC_ABILITY_GRADE_REMARKS";
		    
		    // Create if not exist
		    $newID = '';
		    $isRemarkExist = count($this->getAbilityOriRemark('', $IndexCatID, $GradingRangeID)) > 0;
	        if(!$isRemarkExist)
	        {
	            $sql = "INSERT INTO $targetTable (CatID, GradingRangeID, Remarks, DateInput, InputBy, DateModified, ModifiedBy)
                        VALUES 
				        ('$IndexCatID', '$GradingRangeID', '$IndexRemark', NOW(), '".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."')";
	            $this->db_db_query($sql);
	            $newID = $this->db_insert_id();
	        }
	        
	        // Update
	        $sql = "UPDATE
	                   $targetTable
	               SET
	                   Remarks = '$IndexRemark',
	                   DateModified = NOW(),
	                   ModifiedBy = '".$_SESSION['UserID']."'
	               WHERE
	                   CatID = '$IndexCatID' AND GradingRangeID = '$GradingRangeID' ";
	        $this->db_db_query($sql);
	        
	        return $newID;
		}
		
		function updateAbilityRemarkAll($IndexID, $IndexRemark = '')
		{
		    $targetTable = $this->DBName.".RC_ABILITY_GRADE_REMARKS_ALL";
		    
		    $isRemarkExist = count($this->getAbilityRemarkAllSingle($IndexID)) > 0;
		    $isRecordToBeDeleted = trim($IndexRemark) == '';
		    if($isRecordToBeDeleted)
		    {
		        // Delete if exist
		        if($isRemarkExist)
		        {
		            $sql = "DELETE FROM $targetTable WHERE AbilityRemarkID = '$IndexID'";
		            $success = $this->db_db_query($sql);
		        }
		    }
		    else
		    {
		        // Create if not exist
		        if(!$isRemarkExist)
		        {
    		        $sql = "INSERT INTO $targetTable
                                (SELECT * FROM $this->DBName.RC_ABILITY_GRADE_REMARKS
                                WHERE AbilityRemarkID = '$IndexID')";
                    $this->db_db_query($sql);
    		    }
    		    
    		    // Update
    		    $sql = "UPDATE
                            $targetTable
                        SET
                            Remarks = '$IndexRemark',
                            DateModified = NOW(),
                            ModifiedBy = '".$_SESSION['UserID']."'
                        WHERE
                            AbilityRemarkID = '$IndexID'";
    		    $success = $this->db_db_query($sql);
		    }
		    
		    return $success;
		}
		
		function updateAbilityRemarkCustom($IndexID, $IndexRemark = '', $YearID, $TermID)
		{
		    $targetTable = $this->DBName.".RC_ABILITY_GRADE_REMARKS_CUSTOM";
		    
		    $isRemarkExist = count($this->getAbilityRemarkCustomSingle($IndexID, $YearID, $TermID)) > 0;
		    $isRecordToBeDeleted = trim($IndexRemark) == '';
		    if($isRecordToBeDeleted)
		    {
		        // Delete if exist
		        if($isRemarkExist)
		        {
		            $sql = "DELETE FROM $targetTable WHERE AbilityRemarkID = '$IndexID' AND YearID = '$YearID' AND YearTermID = '$TermID' ";
		            $success = $this->db_db_query($sql);
		        }
		    }
		    else
		    {
    		    // Create if not exist
    		    if(!$isRemarkExist)
    		    {
    		        $fieldname = "AbilityRemarkID, CatID, GradingRangeID, Remarks, DateInput, InputBy, DateModified, ModifiedBy ";
    		        $sql = "INSERT INTO $targetTable
                                ($fieldname, YearID, YearTermID)
                                SELECT
                                    $fieldname, '$YearID', '$TermID'
                                FROM
                                    $this->DBName.RC_ABILITY_GRADE_REMARKS
                                WHERE
                                    AbilityRemarkID = '$IndexID'";
    		        $this->db_db_query($sql);
    		    }
    		    
    		    // Update
    		    $sql = "UPDATE
                            $targetTable
                        SET
                            Remarks = '$IndexRemark',
                            DateModified = NOW(),
                            ModifiedBy = '".$_SESSION['UserID']."'
                        WHERE
                            AbilityRemarkID = '$IndexID' AND 
    		                YearID = '$YearID' AND 
                            YearTermID = '$TermID'";
    		    $success = $this->db_db_query($sql);
		    }
		    
		    return $success;
		}
		
		function resetAllAbilityRemarks($IndexID)
		{
            $sql = "DELETE FROM $this->DBName.RC_ABILITY_GRADE_REMARKS_ALL WHERE AbilityRemarkID = '$IndexID'";
            $success = $this->db_db_query($sql);
            
            $sql = "DELETE FROM $this->DBName.RC_ABILITY_GRADE_REMARKS_CUSTOM WHERE AbilityRemarkID = '$IndexID'";
            $success = $this->db_db_query($sql);
		}
		
		function getFormReportAbilityCode($YearID, $TermID)
		{
		    $allSemAry = getSemesters($this->AcademicYearID, 0);
		    foreach($allSemAry as $thisSemInfo) {
		        if($TermID == $thisSemInfo["YearTermID"]) {
		            $SemesterCond = " AND ((StartDate BETWEEN '".$thisSemInfo["TermStart"]."' AND '".$thisSemInfo["TermEnd"]."') OR (EndDate BETWEEN '".$thisSemInfo["TermStart"]."' AND '".$thisSemInfo["TermEnd"]."')) ";
		        }
		    }
		    
		    $sql = "SELECT TopicTimeTableID FROM $this->DBName.RC_TOPICTIMETABLE WHERE IsDeleted = 0 AND YearID = '$YearID' $SemesterCond";
		    $tttid = $this->returnArray($sql);
		    
		    $idAry = array();
		    foreach($tttid as $tid){
		        $idAry[] = $tid['TopicTimeTableID'];
		    }
		    $idAry = implode(',', $idAry);
		    
		    $sql = "SELECT TWItemCode
                    FROM $this->DBName.RC_ABILITY_CATEGORY_INDEX_MAPPING raci
                        INNER JOIN $this->DBName.RC_EQUIPMENT_CATA_MAPPING recm ON (recm.AbilityCatID = raci.TWItemID AND recm.IsDeleted = 0)
                        INNER JOIN $this->DBName.RC_TIMETABLE_TOOL_MAPPING rttm ON (rttm.ToolCodeID = recm.CodeID AND rttm.IsDeleted = 0)
                    WHERE rttm.TopicTimeTableID IN ($idAry)";
		    $result = $this->returnArray($sql);
		    
		    $resultArr = array();
		    foreach($result as $rs){
		        $resultArr[] = "'" . $rs[0] . "'";
		    }
		    return $resultArr;
		}
		// Ability Remark functions
		
		// Subject Mapping Function
		function getAllSubjectMapping($isDeleted = true, $CodeID = '', $YearID = '', $SubjectID = '', $orderArr = array(), $filterByClassTeacher=false)
		{
            if($filterByClassTeacher) {
                $YearIDArr = Get_Array_By_Key($this->Get_Teaching_Level($_SESSION['UserID']), 'YearID');
                $classTeacherYearCond = " AND rcSub.YearID IN ('".implode("','", (array)$YearIDArr)."') ";
            }

		    ## Query Definition
		    $subjectDES = Get_Lang_Selection('CH_DES', 'EN_DES');
		    $colArr = "rcSub.CodeID, rcSub.SubjectID, rcSub.YearID, rcSub.isTerm, rcSub.TermID, rcSub.TopicID, Sub.$subjectDES as Name";
		    $tableName = $this->DBName.".RC_SUBJECT rcSub";
		    
		    ## Table Join
		    $subjectTable = "ASSESSMENT_SUBJECT Sub";
		    $subjectTableCond = "ON rcSub.SubjectID = Sub.RecordID";
		    
		    ## Condition
		    $deleteCondition = ($isDeleted) ? "isDeleted = 0" : "1";                          // Active or Deleted 
		    $idCondition = ($CodeID == '') ? "" : "AND rcSub.CodeID = '$CodeID'";             // Single or All
		    $yearCondition = ($YearID == '') ? "" : "AND rcSub.YearID = '$YearID'";           // Year Filtering
		    $subjectCondition = (empty($SubjectID)) ? "" : " AND rcSub.SubjectID IN ('".implode("', '", (array)$SubjectID)."') ";
		    
		    ## Ordering
		    if(empty($orderArr)) {
		        $orderArr['sortBy'] = 'Name';
		        $orderArr['sortOrder'] = 'desc';
		    }
		    if($orderArr['sortBy']=='Name') {
		        $orderCond = "$orderArr[sortBy] $orderArr[sortOrder]";
		    } else if($orderArr['sortBy']=='TT') {
		        // TT
		        $orderCond = "rcSub.TermID $orderArr[sortOrder]";
		        $orderCond .= ",rcSub.TopicID $orderArr[sortOrder]";
		    }
		    
		    ## Statement Initialization
		    $sql = "SELECT $colArr
		            FROM
                        $tableName
                        INNER JOIN $subjectTable $subjectTableCond
                    Where
                        $deleteCondition
                        $idCondition
                        $yearCondition
                        $subjectCondition
                        $classTeacherYearCond
                    Order By
                        $orderCond";
		    
            ## Get Query Result Array
		    $result = $this->returnArray($sql);
		    //die();
		    
		    ## Get Topic / Term title by foreach Loop
		    foreach($result as $x => $rs) {
		        ## Collect AbilityCat
		        $result[$x]['CataArr'] = $this->getSubjectMappingCataByCode($rs['CodeID']);
		        if($rs['isTerm'] == 1) {
		            ## Term condition
		            $terms = getSemesters($this->Get_Active_AcademicYearID());
		            $result[$x]['TT'] = Get_Lang_Selection('學期', 'Term') . ' : ' . $terms[$rs['TermID']];
		        } else {
		            ## Topic condition
		            $topics = $this->getYearTopicByYear($rs['YearID']);
		            foreach($topics as $ts) {
		                if($ts['TopicTimeTableID'] == $rs['TopicID']) {
		                    $result[$x]['TT'] = $ts[Get_Lang_Selection('CH_Name', 'EN_Name')];
		                }
		            }
		        }
		    }
		    return $result;
		}
		
		function getFormReportSubjectAbilityCode($YearID, $TermID)
		{
		    $allSemAry = getSemesters($this->AcademicYearID, 0);
		    foreach($allSemAry as $thisSemInfo) {
		        if($TermID == $thisSemInfo["YearTermID"]) {
		            $SemesterCond = " AND ((StartDate BETWEEN '".$thisSemInfo["TermStart"]."' AND '".$thisSemInfo["TermEnd"]."') OR (EndDate BETWEEN '".$thisSemInfo["TermStart"]."' AND '".$thisSemInfo["TermEnd"]."')) ";
		        }
		    }
		    
		    $sql = "SELECT TopicTimeTableID FROM $this->DBName.RC_TOPICTIMETABLE WHERE IsDeleted = 0 AND YearID = '$YearID' $SemesterCond";
		    $tttid = $this->returnArray($sql);
		    
		    $topicCond = '';
		    if(count($tttid) > 0)
		    {
    		    $idAry = array();
    		    foreach($tttid as $tid) {
    		        $idAry[] = $tid['TopicTimeTableID'];
    		    }
    		    $idAry = implode("', '", $idAry);
    		    $topicCond = " rcSub.TopicID IN ('$idAry') OR  ";
		    }
		    
		    $sql = "SELECT
                        raic.Code
                    FROM
                        $this->DBName.RC_SUBJECT rcSub
                        INNER JOIN $this->DBName.RC_SUBJECT_CATA_MAPPING rscm ON (rscm.CodeID = rcSub.CodeID AND rscm.IsDeleted = 0)
		                INNER JOIN $this->DBName.RC_ABILITY_INDEX_CATEGORY raic ON (raic.CatID = rscm.AbilityCatID)
		            WHERE
                        rcSub.YearID = '$YearID' AND
                        ($topicCond rcSub.TermID = '$TermID')";
		    $result = $this->returnArray($sql);
		    
		    $resultArr = array();
		    foreach($result as $rs){
		        $resultArr[] = "'" . $rs[0] . "'";
		    }
		    return $resultArr;
		}
		
		function getSubjectMapping($CodeID){
		    ## Single Query
		    $result = $this->getAllSubjectMapping(false, $CodeID);
		    if(count($result) > 0) {
		        $result = $result[0];
		    }
		    return $result;
		}
		
		function insertSubjectMapping($ParamArr){
		    ## Column and Value Definition
		    $cols = array("YearID", "SubjectID", "TermID", "TopicID", "isTerm");
		    $colArr = "";
		    $valueArr = "";
		    foreach($cols as $cs) {
		        if($ParamArr[$cs]) {
		            $colArr .= " $cs, ";
		            $valueArr .= " '$ParamArr[$cs]', ";
		        }
		    }
		    $colArr .= " DateInput, InputBy, isDeleted ";
		    $valueArr .= " NOW(), '$_SESSION[UserID]', 0 ";
		    $tableName = $this->DBName.".RC_SUBJECT";
		    
		    ## Statement Initialization
		    $sql = "INSERT INTO $tableName
		    ($colArr) VALUES
		    ($valueArr)";
		    $result = $this->db_db_query($sql);
		    if($result && $ParamArr['CataArr'][0] != '') {
		        ## Insert AbilityCat
		        $cataCount = 0;
		        $CodeID = $this->db_insert_id();
		        foreach($ParamArr['CataArr'] as $ca) {
		            if($this->insertSubjectMappingCata($CodeID, $ca)) {
		                $cataCount++;
		            }
		        }
		        //$result['cataCount'] = $cataCount;
		    }
		    return $result;
		}
		
		function updateSubjectMapping($ParamArr){
		    ## Check existance
		    $obj = $this->getSubjectMapping($ParamArr['CodeID']);
		    
		    if(!empty($obj)) {
		        ## Column Definition
		        $cols = array("YearID", "SubjectID", "TermID", "TopicID", "isTerm");
		        
		        ## Value Definition
		        $content = "";
		        foreach($cols as $cs) {
		            $content .= " $cs = '$ParamArr[$cs]', ";
		        }
		        $content .= " DateModified = NOW(), ";
		        $content .= " ModifiedBy = '".$_SESSION['UserID']."' ";
		        
		        ## Define target
		        $cond = " CodeID = '$ParamArr[CodeID]' ";
		        
		        ## Statement initialization
		        $tableName = $this->DBName.".RC_SUBJECT";
		        $sql = "UPDATE $tableName
		                SET $content
		                WHERE $cond";
		        
		        ## Get Result
		        $result = array();
		        $result['update'] = $this->db_db_query($sql);
		        if($result && $ParamArr['CataArr'][0] != '') {
		            ## Update Ability Cat Mapping
		            $rc = $this->updateSubjectMappingCata($ParamArr['CodeID'], $ParamArr['CataArr']);
		            $result['CataArr'] = $rc;
		        }
		        return $result;
		    } else {
		        return false;
		    }
		}
		
		function deleteSubjectMapping($CodeID) {
		    ## Check existance
		    $obj = $this->getSubjectMapping($CodeID);
		    if(!empty($obj)) {
		        ## Statement Initialization
		        $tableName = $this->DBName.".RC_SUBJECT";
		        $sql = "UPDATE $tableName
		                SET isDeleted = 1, DateModified = NOW(), ModifiedBy = '".$_SESSION['UserID']."'
                        WHERE CodeID = '$CodeID'";
		        $result = $this->db_db_query($sql);
		        return $result;
		    } else {
		        return false;
		    }
		}
		
		function getSubjectMappingCataByCode($CodeID, $isDeleted = false){
		    ## Column Definition
		    $colArr = "MappingID, CodeID, AbilityCatID";
		    $tableName = $this->DBName.".RC_SUBJECT_CATA_MAPPING";
		    ## Show Deleted
		    $deleteCond = ($isDeleted) ? '1' : 'isDeleted = 0';
		    ## Statement Initialization
		    $sql = "SELECT $colArr
		            FROM
                        $tableName
		            Where
                        $deleteCond
                        AND CodeID = '$CodeID'
                    Order By
                        AbilityCatID";
		    ## Get Query Result Array
		    $result = $this->returnArray($sql);
		    return $result;
		}
		
		function getSubjectMappingCata($CodeID, $SubjectID, $isDeleted = false){
		    ## Column Definition
		    $colArr = "MappingID, CodeID, AbilityCatID";
		    $tableName = $this->DBName.".RC_SUBJECT_CATA_MAPPING";
		    ## Statement Initialization
		    $sql = "SELECT $colArr
		            FROM
                        $tableName
		            Where
                        $deleteCond
                        AND CodeID = '$CodeID'
                    Order By
                        AbilityCatID";
		    ## Get Query Result Set
            $result = $this->returnArray($sql);
            return $result;
		}
		
		function insertSubjectMappingCata($CodeID, $Cata){
		    ## Statement Initialization
		    $tableName = $this->DBName.".RC_SUBJECT_CATA_MAPPING";
		    $cols = ' CodeID, AbilityCatID ';
		    $value = " '$CodeID', '$Cata' ";
		    $cols .= " ,DateInput, InputBy, isDeleted ";
		    $value .= " , NOW(), ".$_SESSION['UserID'].", 0 ";
		    $sql = "INSERT INTO $tableName
        		    ($cols) VALUES
        		    ($value)";
		    $result = $this->db_db_query($sql);
		    //$result['MappingID'] = $this->db_insert_id();
		    return $result;
		}
		
		function updateSubjectMappingCata($CodeID, $CataArr){
		    $result = array();
		    $result['Delete'] = array();
		    $result['Insert'] = array();
		    
		    ## Get Original Data Set
		    $subMapArr = $this->getSubjectMappingCataByCode($CodeID);
		    
		    ## Array Comparison
		    $smArr = array();
		    $insertArr = array();
		    $delArr = array();
		    foreach($subMapArr as $sm) {
		        if(!in_array($sm['AbilityCatID'], $CataArr)) {
		            $delArr[] = $sm['AbilityCatID'];
		        } else {
		            $smArr[] = $sm['AbilityCatID'];
		        }
		    }
		    foreach($CataArr as $cata){
		        if(!in_array($cata, $smArr)){
		            $insertArr[] = $cata;
		        }
		    }
		    //debug_pr($delArr);
		    //debug_pr($insertArr);
		    //die();
		    
		    ## Delete
		    foreach($delArr as $del) {
		        $result['Delete'][] = $this->deleteSubjectMappingCata($CodeID, $del);
		    }
		    ## Insert
		    foreach($insertArr as $insert) {
		        $result['Insert'][] = $this->insertSubjectMappingCata($CodeID, $insert);
		    }
		    return $result;
		}
		
		function deleteSubjectMappingCataByID($MappingID){
		    ## Statement Initialization
		    $tableName = $this->DBName.".RC_SUBJECT_CATA_MAPPING";
		    $sql = "DELETE FROM $tableName
                    WHERE MappingID = '$MappingID'";
		    $result = $this->db_db_query($sql);
		    return $result;
		}
		
		function deleteSubjectMappingCata($CodeID, $TopicID){
		    ## Statement Initialization
		    $tableName = $this->DBName.".RC_SUBJECT_CATA_MAPPING";
		    $sql = "DELETE FROM $tableName
                    WHERE CodeID = '$CodeID' AND AbilityCatID = '$TopicID'";
		    $result = $this->db_db_query($sql);
		    return $result;
		}
		
		function getYearTopicByYear($YearID){
		    ## Column Definition
		    $cols = "TopicTimeTableID, TimeTableCode, EN_Name, CH_Name, YearID, StartDate, EndDate";
		    $tableName = $this->DBName.".RC_TOPICTIMETABLE";
		    $sql = "SELECT $cols
		            FROM $tableName
		            WHERE YearID = '$YearID'";
		    ## Get Query Result Array
		    $result = $this->returnArray($sql);
		    return $result;
		}
		
		function checkSubjectTT($type, $ttID, $SubjectID, $YearID, $CodeID){
		    $tableName = $this->DBName.".RC_SUBJECT";
		    $sql = "SELECT CodeID FROM $tableName WHERE 1 ";
		    
		    ## Exclude Current Topic
		    if($CodeID) {
		        $sql .= " AND CodeID != '$CodeID' ";
		    }
		    
		    if($type == 'topic') {
		        ## Topic Filter Condition
		        ## Topic In Use
		        $cond1 = "  AND isTerm = '0'
                            AND YearID = '$YearID'
                            AND SubjectID = '$SubjectID'
                            AND TopicID = '$ttID'
                            AND isDeleted = 0 ";
		        $sql1 = $sql.$cond1;
		        $result1 = count($this->returnArray($sql1));
		        
		        ## Term Used
		        $cond2 = "  AND isTerm = '1'
                            AND YearID = '$YearID'
                            AND SubjectID = '$SubjectID'
                            AND isDeleted = 0 ";
		        $sql2 = $sql.$cond2;
		        $result2 = count($this->returnArray($sql2));
		        
		        if($result1) {
		            $errorType = 'TopicUsed';
		        } else if($result2) {
		            $errorType = 'TermInUse';
		        }
		    } else if($type=='term') {
		        ## Term Filter Condition
		        ## Term In Use
		        $cond1 = "   AND isTerm = '1'
                            AND YearID = '$YearID'
                            AND SubjectID = '$SubjectID'
                            AND TermID = '$ttID'
                            AND isDeleted = 0 ";
		        $sql1 = $sql.$cond1;
		        $result1 = count($this->returnArray($sql1));
		        
		        ## Topic Used
		        $cond2 = "  AND isTerm = '0'
                            AND YearID = '$YearID'
                            AND SubjectID = '$SubjectID'
                            AND isDeleted = 0 ";
		        $sql2 = $sql.$cond2;
		        $result2 = count($this->returnArray($sql2));
		        
		        if($result1) {
		            $errorType = 'TermUsed';
		        } else if($result2) {
		            $errorType = 'TopicInUse';
		        }
		    }
		    return $errorType;
		}
		
		function GET_STUDENT_SUBJECT_TOPIC_SCORE($StudentIDArr, $CodeID="", $TWItemID="", $SubjectID="", $ClassID="", $ClassLevelID="")
		{
		    // Condition
		    $CodeCond = $CodeID == "" ? "" : " CodeID = '".$CodeID."' AND ";
		    $TWItemCond = empty($TWItemID)? "" : " TWItemID IN ('".implode("','", (array)$TWItemID)."') AND ";
		    $SubjectCond = empty($SubjectID)? "" : " SubjectID IN ('".implode("','", (array)$SubjectID)."') AND ";
		    $ClassCond = empty($ClassID)? "" : " ClassID IN ('".implode("','", (array)$ClassID)."') AND ";
		    $ClassLevelCond = empty($ClassLevelID)? "" : " ClassLevelID IN ('".implode("','", (array)$ClassLevelID)."') AND ";
		    
		    // Condition - Get Topics from Category
// 		    $topicCatCond = "";
// 		    if(empty($TopicID) && $TopicCatID > 0) {
// 		        $thisTopicList = $this->getTopics("", $TopicCatID, $ClassLevelID);
// 		        $thisTopicList = Get_Array_By_Key((array)$thisTopicList, "TopicID");
// 		        $topicCatCond = " TopicID IN ('".implode("','", (array)$thisTopicList)."') AND ";
// 		    }
// 		    if($CodeID == "" && empty($SubjectID)) {
// 		        return array();
// 		    }
		    
		    $table = $this->DBName.".RC_SUBJECT_TOPIC_SCORE";
		    $fields = "TopicScoreID, StudentID, CodeID, ClassLevelID, ClassID, SubjectID, TWItemID, Score, ScoreGrade, InputBy, DateInput, ModifiedBy, DateModified";
		    $sql  = "SELECT
            		    $fields
        		    FROM
            		    $table
        		    WHERE
            		    StudentID IN ('".implode("','", (array)$StudentIDArr)."') AND
            		    $CodeCond
            		    $TWItemCond
            		    $SubjectCond
            		    $ClassCond
            		    $ClassLevelCond
            		    isDeleted = '0' ";
		    $result = $this->returnArray($sql);
		    
		    // Build score data
		    $returnArr = array();
		    for($i=0; $i<sizeof($result); $i++)
		    {
		        $thisCodeID = $result[$i]["CodeID"];
		        $thisItemID = $result[$i]["TWItemID"];
		        $thisStudentID = $result[$i]["StudentID"];
		        $returnArr[$thisStudentID][$thisCodeID][$thisItemID] = $result[$i];
		    }
		    
		    return $returnArr;
		}
		
		# INSERT scores
		function INSERT_STUDENT_SUBJECT_TOPIC_SCORE($ScoreArr)
		{
		    global $UserID;
		    
		    // Define Table and Fields
		    $table = $this->DBName.".RC_SUBJECT_TOPIC_SCORE";
		    $fields = "StudentID, CodeID, ClassLevelID, ClassID, SubjectID, TWItemID, Score, InputBy, DateInput, ModifiedBy, DateModified";
		    $sql = "INSERT INTO $table ($fields) VALUES ";
		    
		    $entries = array();
		    for($i=0; $i<sizeof($ScoreArr); $i++) {
		        $thisScore = $ScoreArr[$i];
		        $entries[] = "('".$thisScore["StudentID"]."', '".$thisScore["CodeID"]."', '".$thisScore["ClassLevelID"]."', '".$thisScore["ClassID"]."', '".$thisScore["SubjectID"]."', '".$thisScore["TWItemID"]."', '".$thisScore["Score"]."', '$UserID', NOW(), '$UserID', NOW())";
		    }
		    
		    // Insert
		    $success = false;
		    if(count($entries) > 0) {
		        $sql .= implode(", ", $entries);
		        $success = $this->db_db_query($sql);
		    }
		    
		    return $success;
		}
		
		# UPDATE scores
		function UPDATE_STUDENT_SUBJECT_TOPIC_SCORE($ScoreArr)
		{
		    $table = $this->DBName.".RC_SUBJECT_TOPIC_SCORE";
		    $sqlUpdate = "UPDATE $table SET ";
		    for($i=0; $i<sizeof($ScoreArr); $i++)
		    {
		        $sql = "";
		        $sql .= "Score = '".$ScoreArr[$i]["Score"]."', ";
		        //$sql .= "ScoreGrade = '".$ScoreArr[$i]["ScoreGrade"]."', ";
		        $sql .= "ModifiedBy = '".$ScoreArr[$i]["ModifiedBy"]."', ";
		        $sql .= "DateModified = NOW() ";
		        
		        $sql .= "WHERE ";
		        if (isset($ScoreArr[$i]["TopicScoreID"]) &&  $ScoreArr[$i]["TopicScoreID"] != "") {
		            $sql .= "TopicScoreID = '".$ScoreArr[$i]["TopicScoreID"]."' ";
		        }
		        else {
		            $sql .= "StudentID = '".$ScoreArr[$i]["StudentID"]."' AND ";
		            $sql .= "TWItemID = '".$ScoreArr[$i]["TWItemID"]."' AND ";
		            if (isset($ScoreArr[$i]["CodeID"]) &&  $ScoreArr[$i]["CodeID"] != "") {
		                $sql .= "CodeID = '".$ScoreArr[$i]["CodeID"]."'";
		            } else {
    		            $sql .= "ClassID = '".$ScoreArr[$i]["ClassID"]."' AND ";
    		            $sql .= "SubjectID = '".$ScoreArr[$i]["SubjectID"]."' ";
		            }
		        }
		        $sql = $sqlUpdate.$sql;
		        
		        $success[] = $this->db_db_query($sql);
		    }
		    return !in_array(false, $success);
		}
		// Subject Mapping Function
		
		// Ability Remark Selection
		// RC_ABILITY_GRADE_REMARKS_TARGET_CAT
// 		function find_ABILITY_INDEX_CATEGORY_TAIWAN_ARRAY($arr)
// 		{
// 		    $resultArr = array();
// 		    if(!empty($arr))
// 		    {
// 		        $table = $this->DBName.".RC_ABILITY_INDEX_CATEGORY";
// 		        $cols = "Name, Code AS TWItemCode";
// 		        $conds = "Code IN ('" . implode("','", (array)$arr) ."')";
// 		        $sql = "SELECT
//                             $cols
//                         FROM
//                             $table
//                         WHERE
//                             $conds ";
// 		        $resultArr = $this->returnArray($sql);
// 		    }
//		    
// 		    return $resultArr;
// 		}
		
		function view_ABILITY_REMARK_SELECTION($TermID, $YearID, $abilityCode, $searchTWCat, $searchTWLevel)
		{
		    $fieldorder = "ORDER BY aitw.Code asc";
		    $cols = "aitw.CatID as CatID,
    				aitw.Code as TWItemCode,
                    IF(ragrtc.RecordID IS NULL, -1, ragrtc.RecordID) as RecordID,
                    IF(ragrtc.TargetMOCat IS NULL, -1, ragrtc.TargetMOCat) as TargetMOCat,
                    aim.MOItemCode as MOItemCode,
                    ragrtc.DateModified,
                    ragrtc.ModifiedBy ";
		    $tables = $this->DBName.".RC_ABILITY_INDEX_CATEGORY aitw
                    LEFT OUTER JOIN ".$this->DBName.".RC_ABILITY_GRADE_REMARKS_TARGET_CAT ragrtc
                        ON aitw.CatID = ragrtc.CatID AND ragrtc.YearTermID = '$TermID' AND ragrtc.ClassLevelID = '$YearID'
                    INNER JOIN ".$this->DBName.".RC_ABILITY_CATEGORY_INDEX_MAPPING aim
    				    ON (aim.TWItemID = aitw.CatID) ";
		    $conds = "IF('$TermID' = '', 0, 1)
                    ".((!empty($abilityCode))? "AND aitw.Code IN ($abilityCode)" : " AND 0")
                    .(trim($searchTWCat) != "" ? " AND aitw.Code LIKE '%$searchTWCat%' " : "")
                    .(trim($searchTWLevel) != "" ? " AND aitw.Code LIKE '%$searchTWLevel%'" : "");
		    
		    $sql = "SELECT
                        $cols
                    FROM
        				$tables
        			Where
        			    $conds
    			    $fieldorder ";
            $resultArr = $this->returnArray($sql, 0);
            return $resultArr;
		}
		
		function find_ABILITY_REMARK_SELECTION($ClassLvlID, $TermID, $CatID = '')
		{
		    $table = $this->DBName.".RC_ABILITY_GRADE_REMARKS_TARGET_CAT";
		    $cols = "RecordID, CatID, TargetMOCat";
		    $conds = "YearTermID = '$TermID' ";
		    $conds .= "AND ClassLevelID = '$ClassLvlID' ";
		    if($CatID != '') {
		        $conds .= "AND CatID = '$CatID'";
		    }
		    
		    $sql = "SELECT 
		                $cols
		            FROM 
		                $table
		            WHERE
                        $conds ";
            $result = $this->returnArray($sql);
            return $result;
		}
		
		function update_ABILITY_REMARK_SELECTION($ClassLvlID, $TermID, $selectionAry)
		{
		    $insertAry = array();
		    $updateAry = array();
		    foreach($selectionAry as $CatID => $selection)
		    {
		        // check existence
		        $isExist = $this->find_ABILITY_REMARK_SELECTION($ClassLvlID, $TermID, $CatID);
		        if($isExist) {
		            $updateAry[] = array("CatID" => $CatID, "Target" => $selection);
		        } else {
		            $insertAry[] = array("CatID" => $CatID, "Target" => $selection);
		        }
		    }
		    
		    // insert process
		    $insertResult = 0;
		    if(!empty($insertAry)) {
		        $insertResult = $this->insert_ABILITY_REMARK_SELECTION($ClassLvlID, $TermID, $insertAry);
		    } else {
		        // do nothing
		    }
		    
		    // update process
		    $updateResult = array();
		    if(!empty($updateAry)) {
		        foreach($updateAry as $ua) {
		            $updateResult[] = $this->modify_ABILITY_REMARK_SELECTION($ClassLvlID, $TermID, $ua['CatID'], $ua['Target']);
		        }
		    } else {
		        // do nothing
		    }
		    
		    return array(
		        "insert" => $insertResult,
		        "update" => $updateResult
		    );
		}
		
		function modify_ABILITY_REMARK_SELECTION($ClassLvlID, $TermID, $CatID, $target)
		{
		    $table = $this->DBName.".RC_ABILITY_GRADE_REMARKS_TARGET_CAT";
		    $query = "TargetMOCat = '$target',
                      DateModified = NOW(),
                      ModifiedBy = '".$_SESSION['UserID']."'";
		    $conds = "CatID = '$CatID'
                      AND YearTermID = '$TermID'
                      AND ClassLevelID = '$ClassLvlID'";
		    $sql = "UPDATE 
		              $table
		            SET
                      $query
		            WHERE
                      $conds";
		    $result = $this->db_db_query($sql);
		    return $result;
		}
		
		function insert_ABILITY_REMARK_SELECTION($ClassLvlID, $TermID, $targetAry)
		{
		    $table = $this->DBName.".RC_ABILITY_GRADE_REMARKS_TARGET_CAT";
		    $cols = "CatID, 
                     YearTermID,
                     ClassLevelID,
                     TargetMOCat,
                     isDeleted,
                     DateInput,
                     InputBy,
                     DateModified,
                     ModifiedBy";
		    
		    $valuesArr = array();
		    foreach((array)$targetAry as $target)
		    {
		        $valuesArr[] = "(
                                 '".$target['CatID']."',
                                 '$TermID',
                                 '$ClassLvlID',
                                 '".$target['Target']."',
                                 '0',
                                 NOW(),
                                 '".$_SESSION['UserID']."',
                                 NOW(),
                                 '".$_SESSION['UserID']."'
                                 )";
		    }
		    //$values = substr($values, 0, strlen($values)-1);
		    $values = implode(',', $valuesArr);
		    
		    if($values != '') {
    		    $sql = "INSERT INTO $table ($cols) VALUES $values";
    		    $result = $this->db_db_query($sql);
		    } else {
		        $result = false;
		    }
		    
		    return $result;
		}
		
		### TIME PERIOD SETTING
		# MARK EDIT
		function find_INPUT_SCORE_PERIOD_SETTING($YearTermID = '')
		{
		    $table = $this->DBName.'.RC_INPUT_SCORE_PERIOD_SETTING';
		    $cols = "SettingID,
                     YearTermID,
                     StartDate,
                     EndDate,
                     DateInput,
                     InputBy,
                     DateModified,
                     ModifiedBy";
		    $conds = "";
		    if($YearTermID != '') {
		        $conds = "WHERE YearTermID = '$YearTermID'";
		    }
		    
		    $sql = "SELECT
                        $cols
                    FROM
                        $table
		            $conds";
		    $result = $this->returnArray($sql);
		    return $result;
		}
		
		function insert_INPUT_SCORE_PERIOD_SETTING($YearTermID, $startDate, $endDate)
		{
		    $table = $this->DBName.'.RC_INPUT_SCORE_PERIOD_SETTING';
		    $cols = "YearTermID,
                     StartDate,
                     EndDate,
                     DateInput,
                     InputBy,
                     DateModified,
                     ModifiedBy";
		    $value = "'$YearTermID',
                      '$startDate',
                      '$endDate',
                      NOW(),
                      '".$_SESSION['UserID']."',
                      NOW(),
                      '".$_SESSION['UserID']."'";
		    
		    $sql = "INSERT INTO $table ($cols) VALUES ($value)";
		    $result = $this->db_db_query($sql);
		    return $result;
		}
		
		function edit_INPUT_SCORE_PERIOD_SETTING($YearTermID, $startDate, $endDate)
		{
		    $table = $this->DBName.'.RC_INPUT_SCORE_PERIOD_SETTING';
		    $conds = "YearTermID = '$YearTermID'";
		    $sql = "UPDATE
                        $table
		            SET
                        StartDate = '$startDate',
                        EndDate = '$endDate',
                        DateModified = NOW(),
                        ModifiedBy = '".$_SESSION['UserID']."'
                    WHERE
                        $conds";
		    $result = $this->db_db_query($sql);
		    //debug_pr($sql);die();
		    return $result;
		}
		
		function delete_INPUT_SCORE_PERIOD_SETTING($YearTermID)
		{
		    $table = $this->DBName.'.RC_INPUT_SCORE_PERIOD_SETTING';
		    $sql = "DELETE FROM
                        $table
                    WHERE
                        YearTermID = '$YearTermID' ";
		    $result = $this->db_db_query($sql);
		    return $result;
		}
		
		function isInputScorePeriod($TermID = '')
		{
		    // Admin > always can edit
		    if($this->IS_POWER_PORTFOLIO_ADMIN_USER()) {
		        return true;
		    }
		    
		    // Current Term without settings > Can EDIT
		    $isCurrentYear = $this->AcademicYearID == Get_Current_Academic_Year_ID();
		    if($isCurrentYear)
		    {
    		    if(!is_array($TermID) && $TermID != '')
    		    {
        		    $isCurrentSemester = $TermID == getCurrentSemesterID();
        		    if($isCurrentSemester) {
        		        $periodSettingAry = $this->find_INPUT_SCORE_PERIOD_SETTING($TermID);
        		        if(empty($periodSettingAry)) {
        		            return true;
        		        }
        		    }
    		    }
    		    else if(is_array($TermID))
    		    {
    		        foreach($TermID as $thisTermID)
    		        {
    		            $isCurrentSemester = $thisTermID == getCurrentSemesterID();
        		        if($isCurrentSemester) {
        		            $periodSettingAry = $this->find_INPUT_SCORE_PERIOD_SETTING($thisTermID);
        		            if(empty($periodSettingAry)) {
        		                return true;
        		            }
        		        }
    		        }
    		    }
		    }
		    
		    $table = $this->DBName.'.RC_INPUT_SCORE_PERIOD_SETTING';
		    $cols = "SettingID,
                     YearTermID,
                     StartDate,
                     EndDate,
                     DateInput,
                     InputBy,
                     DateModified,
                     ModifiedBy";
		    $conds = "";
		    if((!is_array($TermID) && $TermID != '') || is_array($TermID)) {
		        $conds .= "AND YearTermID IN ('".implode("', '", (array)$TermID)."') ";
		    }
		    $conds .= "AND ( NOW() BETWEEN StartDate AND EndDate )";
		    
		    $sql = "SELECT
                        $cols
                    FROM
                        $table
		            WHERE 1
                        $conds";
		    $result = $this->returnArray($sql);
		    return !empty($result);
		}
		
		function Get_KG_Award_List($awardID='', $awardType='', $excludeAwardID = '')
		{
		    $table = $this->DBName.'.RC_AWARD';
		    $cols = "AwardID,
                    AwardCode,
                    AwardNameEn,
                    AwardNameCh,
                    AwardType,
                    DisplayOrder,
                    InputDate,
                    InputBy,
                    LastModifiedDate,
                    LastModifiedBy";
		    $conds = "";
		    if($awardID != '') {
		        $conds .= " AND AwardID = '$awardID' ";
		    }
		    if($awardType != '') {
		        $conds .= " AND AwardType = '$awardType' ";
		    }
		    if(!empty($excludeAwardID)) {
		        $conds .= " AND AwardID NOT IN ('".implode("','", (array)$excludeAwardID)."') ";
		    }
		    
		    $sql = "SELECT $cols FROM $table WHERE 1 $conds";
		    return  $this->returnArray($sql);
		}
		
		function Insert_KG_Award($dataAry)
		{
		    $table = $this->DBName.'.RC_AWARD';
		    
		    $cols = array();
		    $values = array();
		    foreach((array)$dataAry as $col => $val) {
		        $cols[] = $col;
		        $values[] = "'$val'";
		    }
		    $cols = array_merge($cols, array('InputDate', 'InputBy', 'LastModifiedDate', 'LastModifiedBy'));
		    $values = array_merge($values, array('NOW()', "'".$_SESSION['UserID']."'", 'NOW()', "'".$_SESSION['UserID']."'"));
		    $cols = implode(',', (array)$cols);
		    $values = implode(',', (array)$values);
		    
		    $sql = "INSERT INTO $table ($cols) VALUES ($values)";
		    return $this->db_db_query($sql);
		}
		
		function Edit_KG_Award($AwardID, $dataAry)
		{
		    $table = $this->DBName.'.RC_AWARD';
		    
		    $values = array();
		    foreach((array)$dataAry as $col => $val) {
		        $values[] = " $col = '$val' ";
		    }
		    $values = array_merge($values, array(' LastModifiedDate = NOW() ', " LastModifiedBy = '".$_SESSION['UserID']."' "));
		    $values = implode(',', (array)$values);
		    
		    $sql = "UPDATE $table SET $values WHERE AwardID = '$AwardID' ";
		    return $this->db_db_query($sql);
		}
		
		function Delete_KG_Award($AwardID)
		{
		    $table = $this->DBName.'.RC_AWARD';
		    $sql = "DELETE FROM $table WHERE AwardID = '$AwardID' ";
		    return $this->db_db_query($sql);
		}
		
		function Get_Student_Award_List($studentIDArr='', $awardIDArr='', $awardType='')
		{
		    $table = $this->DBName.'.RC_AWARD_GENERATED_STUDENT_RECORD as adsr';
		    $cols = "adsr.RecordID,
                    adsr.StudentID,
                    ad.AwardID,
                    ad.AwardCode,
                    ad.AwardNameEn,
                    ad.AwardNameCh,
                    ad.AwardType,
                    ad.DisplayOrder";
		    $conds = "";
		    if(!empty($studentIDArr)) {
		        $conds .= " AND adsr.StudentID IN ('".implode("','", (array)$studentIDArr)."') ";
		    }
		    if(!empty($awardIDArr)) {
		        $conds .= " AND ad.AwardID IN ('".implode("','", (array)$awardIDArr)."') ";
		    }
		    if($awardType != '') {
		        $conds .= " AND ad.AwardType = '$awardType' ";
		    }
		    
		    $join_table = $this->DBName.'.RC_AWARD as ad ';
		    $join_conds = " adsr.AwardID = ad.AwardID ";
		    
		    $sql = "SELECT $cols FROM $table INNER JOIN $join_table ON $join_conds WHERE 1 $conds";
		    return $this->returnArray($sql);
		}
		
		function Insert_Student_Award($dataAry)
		{
		    $studentID = $dataAry['StudentID'];
		    $awardID = $dataAry['AwardID'];
		    if($studentID && $awardID)
		    {
		        $existingRecord = $this->Get_Student_Award_List(array($studentID), array($awardID));
		        if(!empty($existingRecord)) {
		            return true;
		        }
		    }
		    
		    $table = $this->DBName.'.RC_AWARD_GENERATED_STUDENT_RECORD';
		    
		    $cols = array();
		    $values = array();
		    foreach((array)$dataAry as $col => $val) {
		        $cols[] = $col;
		        $values[] = "'$val'";
		    }
		    $cols = array_merge($cols, array('InputDate', 'InputBy'));
		    $values = array_merge($values, array('NOW()', "'".$_SESSION['UserID']."'"));
		    $cols = implode(',', (array)$cols);
		    $values = implode(',', (array)$values);
		    
		    $sql = "INSERT INTO $table ($cols) VALUES ($values)";
		    return $this->db_db_query($sql);
		}
		
		function Delete_Student_Award($RecordIDArr='', $studentIDArr='', $awardIDArr='')
		{
		    $table = $this->DBName.'.RC_AWARD_GENERATED_STUDENT_RECORD';
		    $conds = "";
		    if(!empty($RecordIDArr)) {
		        $conds .= " AND RecordID IN ('".implode("','", (array)$RecordIDArr)."') ";
		    }
		    if(!empty($studentIDArr)) {
		        $conds .= " AND StudentID IN ('".implode("','", (array)$studentIDArr)."') ";
		    }
		    if(!empty($awardIDArr)) {
		        $conds .= " AND AwardID IN ('".implode("','", (array)$awardIDArr)."') ";
		    }
		    
		    $sql = "DELETE FROM $table WHERE 1 $conds ";
		    return $this->db_db_query($sql);
		}
		
		function Get_Award_Last_Generated_Date()
		{
		    global $PowerPortfolioConfig;
		    
		    $table = $this->DBName.'.RC_AWARD_GENERATED_STUDENT_RECORD as adsr';
		    $cols = "adsr.InputDate";
		    $conds .= " AND ad.AwardType = '".$PowerPortfolioConfig['awardType']['Generate']."' ";
		    
		    $join_table = $this->DBName.'.RC_AWARD as ad ';
		    $join_conds = " adsr.AwardID = ad.AwardID ";
		    
		    $sql = "SELECT $cols FROM $table INNER JOIN $join_table ON $join_conds WHERE 1 $conds";
		    $result = $this->returnVector($sql);
		    return $result[0];
		}
		
		function Generate_Student_Award($targetClassID='')
		{
		    global $indexVar, $PowerPortfolioConfig, $Lang;
		    
		    # Get Generated Awards
		    $GeneratedAwardIDArr = $indexVar["libreportcard"]->Get_KG_Award_List($awardID='', $awardType=$PowerPortfolioConfig['awardType']['Generate'], $excludeAwardID = '');
		    $GeneratedAwardIDArr = BuildMultiKeyAssoc((array)$GeneratedAwardIDArr, 'DisplayOrder', 'AwardID', 1, 0);
		    
// 		    ### Delete Old Generated Award
// 		    $indexVar["libreportcard"]->Delete_Student_Award($RecordIDArr='', $studentIDArr='', $GeneratedAwardIDArr);
		    
		    # Get Macau Category
		    $MOTopicInfo = $indexVar["libreportcard"]->Get_Macau_Category($returnAssocArr=true);
		    $MOTopicType = array_values($MOTopicInfo);
		    $MOTopicTypeCode = array_keys($MOTopicInfo);
		    if(empty($MOTopicType)) {
		        $MOTopicType = array('健康與體育', '語言', '個人、社交與人文', '數學與科學', '藝術');
		    }
		    if(empty($MOTopicTypeCode)) {
		        $MOTopicTypeCode = array('A', 'B', 'C', 'D', 'E');
		    }
		    
		    # Get All Terms
		    $allYearTermAry = getSemesters($indexVar["libreportcard"]->Get_Active_AcademicYearID(), $ReturnAsso=0, 'b5');
		    foreach($allYearTermAry as $thisTermIndex => $thisTermData) {
		        if($thisTermIndex >= 3) {
		            unset($allYearTermAry[$thisTermIndex]);
		        }
		    }
		    $allYearTermAry = Get_Array_By_Key($allYearTermAry, 'YearTermID');
		    
		    # Get All KG Classes
		    $allKGClassArr = $indexVar["libreportcard"]->Get_All_KG_Class();
		    $allKGClassArr = BuildMultiKeyAssoc((array)$allKGClassArr, array('YearID', 'YearClassID'));
		    
		    // loop KG Levels
		    foreach((array)$allKGClassArr as $thisYearID => $thisYearClassArr)
		    {
		        // loop KG Classes
		        $thisYearClassIDArr = array_keys($thisYearClassArr);
    		    foreach((array)$thisYearClassIDArr as $thisYearClassID)
    		    {
    		        // SKIP > if not matched target class
    		        if($targetClassID != '' && $targetClassID > 0 && $thisYearClassID != $targetClassID) {
    		            continue;
    		        }
    		        
    		        // Get Class Students
    		        $thisClassStudentIDArr = $indexVar["libreportcard"]->Get_Student_By_Class($thisYearClassID);
    		        $thisClassStudentIDArr = Get_Array_By_Key((array)$thisClassStudentIDArr, 'UserID');
    		        
    		        // Delete Old Generated Award of Class Students
    		        $indexVar["libreportcard"]->Delete_Student_Award($RecordIDArr='', $thisClassStudentIDArr, $GeneratedAwardIDArr);
    		        
    		        // loop Year Term
    		        $TopicsArr = array();
    		        $TopicScoreArr = array();
    		        $SummaryScoreArr = array();
    		        $StudentScoreArr = array();
    		        foreach((array)$allYearTermAry as $thisTermIndex => $thisYearTermID)
        		    {
        		        # Student Macau Category Score Summary
        		        $StudentSummaryScore = $indexVar["libreportcard"]->GetStudentScoreSummary($thisClassStudentIDArr, $thisYearClassID, $thisYearTermID);
        		        $StudentSummaryScore = $StudentSummaryScore[0];
        		        $SummaryScoreArr[$thisYearTermID] = $StudentSummaryScore;
        		        
        		        # Student Topics
        		        $YearTermTopics = $indexVar["libreportcard"]->getTopics("", "", $thisYearID, true, $thisYearTermID);
        		        $YearTermTopics = BuildMultiKeyAssoc($YearTermTopics, array("CatTypeName", "TopicCatNameCh", "TopicID"));
        		        $TopicsArr[$thisYearTermID] = $YearTermTopics;
        		        
        		        # Student Topic Score
        		        $StudentTopicScore = $indexVar["libreportcard"]->GET_STUDENT_TOPIC_SCORE($thisClassStudentIDArr, $thisYearID, "", "", $thisYearTermID);
        		        $TopicScoreArr[$thisYearTermID] = $StudentTopicScore;
    		        }
    		        
    		        // loop KG Class Students
    		        for($i=0; $i<sizeof($thisClassStudentIDArr); $i++)
    		        {
    		            # Student Info
    		            $StudentID = $thisClassStudentIDArr[$i];
    		            $CategoryScore = 0;
    		            $BehaviorScore = 0;
                        $BehaviorScoreCount = 0;
    		            $CategoryTypeScoreArr = array();
    		            foreach((array)$MOTopicTypeCode as $MOTypeCode) {
    		                $CategoryTypeScoreArr[$MOTypeCode] = 0;
    		            }
    		            
    		            // loop Year Term
    		            foreach((array)$allYearTermAry as $thisTermIndex => $thisYearTermID)
    		            {
    		                $StudentSummaryScore = $SummaryScoreArr[$thisYearTermID];
    		                $YearTermTopics = $TopicsArr[$thisYearTermID];
    		                $StudentTopicScore = $TopicScoreArr[$thisYearTermID];
    		                
    		                foreach((array)$MOTopicTypeCode as $MOTypeCode)
    		                {
    		                    $MOCategoryScore = $StudentSummaryScore[$StudentID][$MOTypeCode];
    		                    $MOCategoryScore = $MOCategoryScore ? $MOCategoryScore : 0;
    		                    $CategoryScore += $MOCategoryScore;
    		                    
                                $CategoryTypeScoreArr[$MOTypeCode] += $MOCategoryScore;
    		                }
    		                
    		                $LangTopicArr = $YearTermTopics[$Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category1"]];
    		                $LangTopicCount = count((array)$LangTopicArr);
    		                if($LangTopicCount > 0)
    		                {
    		                    foreach((array)$LangTopicArr as $thisTopicCatType => $thisLangTopicArr)
    		                    {
    		                        $thisTopicScoreTotal = 0;
    		                        $thisTopicCount = count((array)$thisLangTopicArr);
    		                        if($thisTopicCount > 0)
    		                        {
    		                            $thisValidTopicCount = 0;
    		                            foreach((array)$thisLangTopicArr as $thisTopicID => $thisTopicInfo)
    		                            {
    		                                $thisTopicScore = $StudentTopicScore[$StudentID][$thisTopicID]["Score"];
    		                                if($thisTopicScore > 0) {
                                                $thisTopicScore = $thisTopicScore ? $thisTopicScore : 0;
                                                $thisTopicScoreTotal += $thisTopicScore;
                                                $thisValidTopicCount++;
                                            }
    		                            }

                                        if($thisValidTopicCount > 0) {
                                            $thisTopicScoreTotal = $thisTopicScoreTotal / $thisValidTopicCount;
                                            $thisTopicScoreTotal = my_round($thisTopicScoreTotal, 2);
                                            $CategoryScore += $thisTopicScoreTotal;
                                        }
    		                        }
    		                    }
    		                }
    		                
    		                $BehaviorTopicArr = $YearTermTopics[$Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category2"]];
    		                $BehaviorTopicCount = count((array)$BehaviorTopicArr);
    		                if($BehaviorTopicCount > 0)
    		                {
    		                    foreach((array)$BehaviorTopicArr as $thisTopicCatType => $thisBehaviorTopicArr)
    		                    {
                                    $thisTopicScoreTotal = 0;
    		                        $thisTopicCount = count((array)$thisBehaviorTopicArr);
    		                        if($thisTopicCount > 0)
    		                        {
                                        $thisValidTopicCount = 0;
    		                            foreach((array)$thisBehaviorTopicArr as $thisTopicID => $thisTopicInfo)
    		                            {
    		                                $thisTopicScore = $StudentTopicScore[$StudentID][$thisTopicID]["Score"];
                                            if($thisTopicScore > 0) {
                                                $thisTopicScore = $thisTopicScore ? $thisTopicScore : 0;
                                                $thisTopicScoreTotal += $thisTopicScore;
                                                $thisValidTopicCount++;
                                            }
    		                            }

                                        if($thisValidTopicCount > 0)
                                        {
                                            $thisTopicScoreTotal = $thisTopicScoreTotal / $thisValidTopicCount;
                                            $thisTopicScoreTotal = my_round($thisTopicScoreTotal, 2);
                                            $BehaviorScore += $thisTopicScoreTotal;
                                            $BehaviorScoreCount++;
                                        }
    		                        }
    		                    }
    		                }
    		            }

    		            $BehaviorAverageScore = 0;
                        if($BehaviorScoreCount > 0) {
                            $BehaviorAverageScore = $BehaviorScore / $BehaviorScoreCount;
                            if($BehaviorAverageScore != 0) {
                                $BehaviorAverageScore = my_round($BehaviorAverageScore, 4);
                            }
                        }
    		            
    		            $HighestCategoryArr = array();
    		            $HighestCategoryScore = 0;
    		            if(!empty($CategoryTypeScoreArr)) {
    		                $HighestCategoryArr = array_keys($CategoryTypeScoreArr, max($CategoryTypeScoreArr));
    		                $HighestCategoryScore = max($CategoryTypeScoreArr) / count((array)$allYearTermAry);
    		                
    		                if($HighestCategoryScore == 0) {
    		                    $HighestCategoryArr = array();
    		                } else {
    		                    $HighestCategoryScore = my_round($HighestCategoryScore, 4);
    		                }
    		            }
    		            
    		            $StudentScoreArr[$StudentID] = array('CategoryScore' => $CategoryScore, 'HighestCategory' => (array)$HighestCategoryArr, 'HighestCategoryScore' => $HighestCategoryScore,
    		                                                              'BehaviorScore' => $BehaviorScore, 'BehaviorAverage' => $BehaviorAverageScore, 'StudentID' => $StudentID);
    		        }
    		        
    		        if(!empty($StudentScoreArr))
    		        {
    		            $AwardAStudentIDArr = array();
    		            $AwardBStudentIDArr = array();
    		            
    		            # Award Type A
    		            $StudentScoreForAwardA = $StudentScoreArr;
    		            sortByColumn2($StudentScoreForAwardA, 'CategoryScore', 1);
    		            foreach((array)$StudentScoreForAwardA as $thisStudentScore)
    		            {
    		                if($thisStudentScore['CategoryScore'] > 0 && $thisStudentScore['BehaviorScore'] > 0 && $thisStudentScore['BehaviorAverage'] >= 4)
    		                {
    		                    $AwardAStudentIDArr[] = $thisStudentScore['StudentID'];
    		                    
    		                    // Max: 6 Student
    		                    if(count((array)$AwardAStudentIDArr) >= 6) {
    		                        break;
    		                    }
    		                }
    		            }
    		            $thisAwardIndex = 1;
    		            if(count((array)$AwardAStudentIDArr) > 0 && $GeneratedAwardIDArr[$thisAwardIndex] > 0)
    		            {
    		                foreach((array)$AwardAStudentIDArr as $StudentID)
    		                {
    		                    $dataAry = array();
    		                    $dataAry['AwardID'] = $GeneratedAwardIDArr[$thisAwardIndex];
    		                    $dataAry['StudentID'] = $StudentID;
    		                    
    		                    $indexVar["libreportcard"]->Insert_Student_Award($dataAry);
    		                }
    		            }
    		            
    		            # Award Type B
    		            $StudentScoreForAwardB = $StudentScoreArr;
    		            sortByColumn2($StudentScoreForAwardB, 'BehaviorScore', 1);
    		            foreach((array)$StudentScoreForAwardB as $thisStudentScore)
    		            {
    		                if($thisStudentScore['BehaviorScore'] > 0)
    		                {
    		                    $AwardBStudentIDArr[] = $thisStudentScore['StudentID'];
    		                    
    		                    // Max: 3 Student
    		                    if(count((array)$AwardBStudentIDArr) >= 3) {
    		                        break;
    		                    }
    		                }
    		            }
    		            $thisAwardIndex = 2;
    		            if(count((array)$AwardBStudentIDArr) > 0 && $GeneratedAwardIDArr[$thisAwardIndex] > 0)
    		            {
    		                foreach((array)$AwardBStudentIDArr as $StudentID)
    		                {
    		                    $dataAry = array();
    		                    $dataAry['AwardID'] = $GeneratedAwardIDArr[$thisAwardIndex];
    		                    $dataAry['StudentID'] = $StudentID;
    		                    
    		                    $indexVar["libreportcard"]->Insert_Student_Award($dataAry);
    		                }
    		            }
    		            
    		            # Award Type C
    		            foreach((array)$StudentScoreArr as $thisStudentID => $thisStudentScore)
    		            {
    		                if(!in_array($thisStudentID, (array)$AwardAStudentIDArr))
    		                {
    		                    $HighestCategoryArr = $thisStudentScore['HighestCategory'];
    		                    $HighestCategoryScore = $thisStudentScore['HighestCategoryScore'];
    		                    if(!empty($HighestCategoryArr) && $HighestCategoryScore >= 3.5 && $thisStudentScore['BehaviorAverage'] >= 3.5)
    		                    {
        		                    foreach((array)$HighestCategoryArr as $thisAwardCategory)
        		                    {
        		                        $thisAwardIndex = array_keys($MOTopicTypeCode, $thisAwardCategory);
        		                        $thisAwardIndex = $thisAwardIndex[0];
        		                        if($HighestCategoryScore >= 4.5) {
        		                            $thisAwardIndex = $thisAwardIndex + 3;
        		                        } else {
        		                            $thisAwardIndex = $thisAwardIndex + 8;
        		                        }
        		                        
        		                        if($GeneratedAwardIDArr[$thisAwardIndex] > 0)
        		                        {
            		                        $dataAry = array();
            		                        $dataAry['AwardID'] = $GeneratedAwardIDArr[$thisAwardIndex];
            		                        $dataAry['StudentID'] = $thisStudentID;
            		                        
            		                        $indexVar["libreportcard"]->Insert_Student_Award($dataAry);
        		                        }
        		                    }
    		                    }
    		                }
    		            }
    		        }
    		    }
		    }
		}
		
		# PARENT VIEW
		function find_VIEW_REPORT_PERIOD_SETTING($YearTermID = '')
		{
		    $table = $this->DBName.'.RC_VIEW_REPORT_PERIOD_SETTING';
		    $cols = "SettingID,
                     YearTermID,
                     StartDate,
                     EndDate,
                     DateInput,
                     InputBy,
                     DateModified,
                     ModifiedBy";
		    $conds = "";
		    if($YearTermID != '') {
		        $conds .= "WHERE YearTermID = '$YearTermID'";
		    }
		    
		    $sql = "SELECT
                        $cols
                    FROM
                        $table
		            $conds";
            $result = $this->returnArray($sql);
            return $result;
		}
		
		function insert_VIEW_REPORT_PERIOD_SETTING($YearTermID, $startDate, $endDate)
		{
		    $table = $this->DBName.'.RC_VIEW_REPORT_PERIOD_SETTING';
		    $cols = "YearTermID,
                     StartDate,
                     EndDate,
                     DateInput,
                     InputBy,
                     DateModified,
                     ModifiedBy";
		    $value = "'$YearTermID',
                      '$startDate',
                      '$endDate',
                      NOW(),
                      '".$_SESSION['UserID']."',
                      NOW(),
                      '".$_SESSION['UserID']."'";
		    
		    $sql = "INSERT INTO $table ($cols) VALUES ($value)";
		    $result = $this->db_db_query($sql);
		    return $result;
		}
		
		function edit_VIEW_REPORT_PERIOD_SETTING($YearTermID, $startDate, $endDate)
		{
		    $table = $this->DBName.'.RC_VIEW_REPORT_PERIOD_SETTING';
		    $conds = "YearTermID = '$YearTermID'";
		    $sql = "UPDATE
                        $table
		            SET
                        StartDate = '$startDate',
                        EndDate = '$endDate',
                        DateModified = NOW(),
                        ModifiedBy = '".$_SESSION['UserID']."'
                    WHERE
                        $conds";
            $result = $this->db_db_query($sql);
            return $result;
		}
		
		function delete_VIEW_REPORT_PERIOD_SETTING()
		{
		    $table = $this->DBName.'.RC_VIEW_REPORT_PERIOD_SETTING';
		    $sql = "DELETE FROM
                        $table
                    WHERE
                        YearTermID = '$YearTermID' ";
            $result = $this->db_db_query($sql);
            return $result;
		}
		
		function isViewReportPeriod($TermID='')
		{
		    // Admin > Can EDIT
// 		    if($this->IS_POWER_PORTFOLIO_ADMIN_USER()) {
// 		        return true;
// 		    }
//		    
//		    // Current Term without settings > Can EDIT
// 		    $isCurrentYear = $this->AcademicYearID == Get_Current_Academic_Year_ID();
// 		    $isCurrentSemester = $isCurrentYear && $TermID == getCurrentSemesterID();
// 		    if($isCurrentSemester) {
// 		        $periodSettingAry = $this->find_VIEW_REPORT_PERIOD_SETTING($TermID);
// 		        if(empty($periodSettingAry)) {
// 		            return true;
// 		        }
// 		    }
		    
		    $table = $this->DBName.'.RC_VIEW_REPORT_PERIOD_SETTING';
		    $cols = "SettingID,
                     YearTermID,
                     StartDate,
                     EndDate,
                     DateInput,
                     InputBy,
                     DateModified,
                     ModifiedBy";
		    $conds = "";
		    if($TermID != '') {
		        $conds .= "AND YearTermID = '$TermID' ";
		    }
		    $conds .= "AND ( NOW() BETWEEN StartDate AND EndDate ) ";
		    $sql = "SELECT
                        $cols
                    FROM
                        $table
		            WHERE
                        1
                        $conds";
            $result = $this->returnArray($sql);
            return !empty($result);
		}
		// TIME PERIOD SETTING
		
		// ADMIN GROUP
		function Get_Admin_Group_DBTable_Sql($Keyword = '')
        {
		    $RC_ADMIN_GROUP = $this->DBName.'.RC_ADMIN_GROUP';
		    $RC_ADMIN_GROUP_USER = $this->DBName.'.RC_ADMIN_GROUP_USER';
		    $GroupNameField = Get_Lang_Selection('ag.AdminGroupNameCh', 'ag.AdminGroupNameEn');

            $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 1 AND RecordStatus = 1";
            $existingUserIdAry = $this->returnVector($sql);
		    
		    $conds_Keyword = '';
		    if ($Keyword != '') {
		        $conds_Keyword = " AND (ag.AdminGroupCode LIKE '%$Keyword%' OR $GroupNameField LIKE '%$Keyword%') ";
            }
	        
	        $cols = "ag.AdminGroupCode,
    				CONCAT('<a class=\"tablelink\" href=\"?task=settings.admin_group.setting&AdminGroupIDArr=', ag.AdminGroupID, '\">', $GroupNameField, '</a>') as GroupInfoLink,
    				CONCAT('<a class=\"tablelink\" href=\"?task=settings.admin_group.member&clearCoo=1&AdminGroupID=', ag.AdminGroupID, '\">', Count(agu.UserID), '</a>') as MemberListLink,
    				ag.DateModified,
    				CONCAT('<input type=\"checkbox\" name=\"AdminGroupIDArr[]\" value=\"', ag.AdminGroupID, '\">') as CheckBox";
	        
	        $sql = "SELECT
                        $cols
                    FROM
                        $RC_ADMIN_GROUP as ag
                        LEFT OUTER JOIN $RC_ADMIN_GROUP_USER as agu ON (ag.AdminGroupID = agu.AdminGroupID AND agu.UserID IN ('".implode("', '", (array)$existingUserIdAry)."'))
                    WHERE
                        ag.RecordStatus = 1
                        $conds_Keyword
                    GROUP BY
                        ag.AdminGroupID ";
			return $sql;
		}
		
		function Get_Admin_Group_Info($AdminGroupID='')
		{
		    if ($AdminGroupID != '') {
                $conds_AdminGroupID = " AND AdminGroupID = '".$AdminGroupID."' ";
            }

            $RC_ADMIN_GROUP = $this->DBName.'.RC_ADMIN_GROUP';
            $cols = "AdminGroupID,
                     AdminGroupCode,
                     AdminGroupNameEn,
                     AdminGroupNameCh";

            $sql = "SELECT
                        $cols
                    FROM
                        $RC_ADMIN_GROUP
                    WHERE
                        RecordStatus = 1
                        $conds_AdminGroupID ";
            return $this->returnArray($sql);
		}
		
		function Get_Admin_Group_Member_List_DBTable_Sql($AdminGroupID, $Keyword='')
		{
		    $RC_ADMIN_GROUP_USER = $this->DBName.'.RC_ADMIN_GROUP_USER';
		    
		    $conds_Keyword = '';
		    if ($Keyword != '') {
		        $conds_Keyword = " AND ( iu.EnglishName LIKE '%$Keyword%' OR iu.ChineseName LIKE '%$Keyword%' ) ";
            }

	        $cols = "iu.EnglishName,
                    iu.ChineseName,
                    CONCAT('<input type=\"checkbox\" name=\"UserIDArr[]\" value=\"', iu.UserID, '\">') as CheckBox ";

            $sql = "SELECT
                        $cols
                    FROM
                        $RC_ADMIN_GROUP_USER as agu
                        INNER JOIN INTRANET_USER as iu ON (agu.UserID = iu.UserID)
                    WHERE
                        agu.AdminGroupID = '".$AdminGroupID."'
                        AND iu.RecordType = 1
                        AND iu.RecordStatus = 1
                        $conds_Keyword ";
            return $sql;
		}
		
		function Get_Admin_Group_Member($AdminGroupID)
		{
		    $RC_ADMIN_GROUP_USER = $this->DBName.'.RC_ADMIN_GROUP_USER ragu';
		    $INTRANET_USER = "INTRANET_USER iu";

		    $join1 = "ON iu.UserID = ragu.userID";
		    $cols = "ragu.UserID, iu.UserLogin";

		    $sql = "SELECT
                            $cols
                    FROM
                            $RC_ADMIN_GROUP_USER
                            INNER JOIN $INTRANET_USER $join1
                    WHERE
                            ragu.AdminGroupID = '".$AdminGroupID."' ";
			return $this->returnArray($sql);
		}
		
		function Get_Admin_Group_Access_Right($AdminGroupIDArr)
		{
		    $RC_ADMIN_GROUP_RIGHT = $this->DBName.'.RC_ADMIN_GROUP_RIGHT';
		    $cols = "AdminGroupRightName";
		    $sql = "SELECT
                            $cols
                    FROM
                            $RC_ADMIN_GROUP_RIGHT
                    WHERE
                            AdminGroupID IN (".implode(',', (array)$AdminGroupIDArr).") ";
            return $this->returnArray($sql);
		}
		
		function Is_Admin_Group_Code_Valid($Value, $ExcludeID='')
		{
		    $Value = $this->Get_Safe_Sql_Query(trim($Value));
		    
		    $conds_ExcludeID = '';
		    if ($ExcludeID != '') {
                $conds_ExcludeID = " AND AdminGroupID != '$ExcludeID' ";
            }
		        
		    $cols = "AdminGroupID";
	        $RC_ADMIN_GROUP = $this->DBName.'.RC_ADMIN_GROUP';

	        $sql = "SELECT
    					$cols
        			FROM
    					$RC_ADMIN_GROUP
        			WHERE
    					AdminGroupCode = '$Value'
    					AND RecordStatus = 1
    					$conds_ExcludeID ";
			$ResultArr = $this->returnArray($sql);
			return (count($ResultArr) > 0)? false : true;
		}
		
		function Add_Admin_Group($DataArr)
		{
                if (!is_array($DataArr) || count($DataArr) == 0) {
                    return false;
                }

		        # Set field and value string
		        $fieldArr = array();
		        $valueArr = array();
		        foreach ($DataArr as $field => $value)
		        {
		            $fieldArr[] = $field;
		            $valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
		        }
		        
		        ## Set others fields
		        # DateInput
		        $fieldArr[] = 'DateInput';
		        $valueArr[] = 'now()';
		        # InputBy
		        $fieldArr[] = 'InputBy';
		        $valueArr[] = "'".$_SESSION['UserID']."'";
		        # DateModified
		        $fieldArr[] = 'DateModified';
		        $valueArr[] = 'now()';
		        # LastModifiedBy
		        $fieldArr[] = 'LastModifiedBy';
		        $valueArr[] = "'".$_SESSION['UserID']."'";
		        $fieldText = implode(", ", $fieldArr);
		        $valueText = implode(", ", $valueArr);
		        
		        # Insert Record
		        $this->Start_Trans();
		        
		        $RC_ADMIN_GROUP = $this->DBName.'.RC_ADMIN_GROUP';
		        $sql = "INSERT INTO $RC_ADMIN_GROUP ($fieldText) VALUES ($valueText)";
		        $success = $this->db_db_query($sql);
		        if ($success == false)
		        {
		            $this->RollBack_Trans();
		            return 0;
		        }
		        else
		        {
		            $insertedID = $this->db_insert_id();
		            $this->Commit_Trans();
		            return $insertedID;
		        }
		}
		
		function Add_Admin_Group_Member($AdminGroupID, $UserIDArr)
		{
		    $numOfUser = count($UserIDArr);
		    if ($numOfUser > 0)
		    {
		        $InsertValueArr = array();
		        foreach($UserIDArr as $thisUserID)
		        {
		            //$thisUserID = $UserIDArr[$i];
		            $InsertValueArr[] = "('".$AdminGroupID."', '".$thisUserID."', '".$_SESSION['UserID']."', '".$_SESSION['UserID']."', now(), now())";
		        }
		        
		        $insertValueList = implode(', ', $InsertValueArr);
		        $RC_ADMIN_GROUP = $this->DBName.'.RC_ADMIN_GROUP_USER';
		        
		        $sql = "INSERT INTO $RC_ADMIN_GROUP
						  (AdminGroupID, UserID, InputBy, LastModifiedBy, DateInput, DateModified)
					    VALUES
						  $insertValueList
					   ";
				$success = $this->db_db_query($sql);
		    }
		    
		    return $success;
		}
		
		function Delete_Admin_Group_Member($AdminGroupID, $UserIDArr)
        {
                if (count($UserIDArr) == 0) {
                    return false;
                }

		        $UserIDList = implode(', ', $UserIDArr);
		        $RC_ADMIN_GROUP = $this->DBName.'.RC_ADMIN_GROUP_USER';
		        
		        $sql = "DELETE FROM
    					    $RC_ADMIN_GROUP
        				WHERE
        					AdminGroupID = '".$AdminGroupID."'
        					AND UserID IN ($UserIDList) ";
                return $this->db_db_query($sql);
		}
		
		function Update_Admin_Group_Member($AdminGroupID, $UserIDArr)
        {
		    $MemberList = array();
            $MemberArr = $this->Get_Admin_Group_Member($AdminGroupID);
		    foreach($MemberArr as $member){
		        $MemberList[] = $member['UserID'];
		    }

		    $numOfID = count($UserIDArr);
		    for($i=0;$i<$numOfID;$i++){
		        if(is_int($index = array_search($UserIDArr[$i], $MemberList))){
		            unset($MemberList[$index]);
		            unset($UserIDArr[$i]);
		        }
		    }

		    $result = array();
		    if(count($UserIDArr) > 0){
		        // Add Member
		        $result['add'] = $this->Add_Admin_Group_Member($AdminGroupID, $UserIDArr);
		    } else {
		        $result['add'] = 'empty';
		    }
		    if(count($MemberList) > 0){
		        // Delete Member
		        $result['delete'] = $this->Delete_Admin_Group_Member($AdminGroupID, $MemberList);
		    } else {
		        $result['delete'] = 'empty';
		    }

		    return !array_search(false, $result);
		}
		
		function Update_Admin_Group($AdminGroupID, $DataArr, $UpdateLastModified=1)
		{
                if (!is_array($DataArr) || count($DataArr) == 0) {
                    return false;
                }
		        
		        # Build field update values string
		        $valueFieldArr = array();
		        foreach ($DataArr as $field => $value)
		        {
		            $valueFieldArr[] = " $field = '".$this->Get_Safe_Sql_Query($value)."' ";
		        }
		        
		        if ($UpdateLastModified==1)
		        {
		            $valueFieldArr[] = " DateModified = now() ";
		            $valueFieldArr[] = " LastModifiedBy = '".$_SESSION['UserID']."' ";
		        }
		        $valueFieldText .= implode(',', $valueFieldArr);
		        
		        $RC_ADMIN_GROUP = $this->DBName.'.RC_ADMIN_GROUP';

		        $sql = "UPDATE $RC_ADMIN_GROUP SET $valueFieldText WHERE AdminGroupID = '$AdminGroupID'";
                return $this->db_db_query($sql);
		}
		
		function Update_Admin_Group_Access_Right($AdminGroupID, $AccessRightArr)
		{
		    $SuccessArr = array();
		    $this->Start_Trans();
		    
		    $RC_ADMIN_GROUP_RIGHT = $this->DBName.'.RC_ADMIN_GROUP_RIGHT';
		    $sql = "DELETE FROM $RC_ADMIN_GROUP_RIGHT WHERE AdminGroupID = '".$AdminGroupID."' ";
		    $SuccessArr['DeleteOldAccessRight'] = $this->db_db_query($sql);
		    
		    $numOfAccessRight = count((array)$AccessRightArr);
		    if (is_array($AccessRightArr) && $numOfAccessRight > 0) {
		        $InsertArr = array();
		        for ($i=0; $i<$numOfAccessRight; $i++) {
		            $thisAccessRight = $AccessRightArr[$i];
		            $InsertArr[] = " ('".$AdminGroupID."', '".$this->Get_Safe_Sql_Query($thisAccessRight)."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
		        }
		        
		        $sql = "INSERT INTO $RC_ADMIN_GROUP_RIGHT
						    (AdminGroupID, AdminGroupRightName, DateInput, InputBy, DateModified, LastModifiedBy)
                        VALUES
                            ".implode(',', $InsertArr);
		        $SuccessArr['InsertNewAccessRight'] = $this->db_db_query($sql);
		    }
		    
		    if (!in_array(false, $SuccessArr)) {
		        $this->Commit_Trans();
		        return true;
		    }
		    else {
		        $this->RollBack_Trans();
		        return false;
		    }
		}
		
		function Delete_Admin_Group($AdminGroupIDArr)
		{
		    $AdminGroupIDArr = (array)$AdminGroupIDArr;
		    $numOfAdminGroup = count($AdminGroupIDArr);
		    
		    $DataArr = array();
		    $DataArr['RecordStatus'] = 0;
		    for ($i=0; $i<$numOfAdminGroup; $i++)
		    {
		        $thisAdminGroupID = $AdminGroupIDArr[$i];
		        $SuccessArr['Delete_Topic'][$thisAdminGroupID] = $this->Update_Admin_Group($thisAdminGroupID, $DataArr);
		    }
		    
		    return !in_array(false, $SuccessArr);
		}
		
		function Get_User_Access_Right($ParUserID)
        {
		    $RC_ADMIN_GROUP = $this->DBName.'.RC_ADMIN_GROUP';
		    $RC_ADMIN_GROUP_USER = $this->DBName.'.RC_ADMIN_GROUP_USER';
		    $RC_ADMIN_GROUP_RIGHT = $this->DBName.'.RC_ADMIN_GROUP_RIGHT';
		    
		    $sql = "SELECT
							agr.AdminGroupRightName
					FROM
							$RC_ADMIN_GROUP_USER as agu
							INNER JOIN $RC_ADMIN_GROUP as ag ON (ag.AdminGroupID = agu.AdminGroupID)
							INNER JOIN $RC_ADMIN_GROUP_RIGHT as agr ON (ag.AdminGroupID = agr.AdminGroupID)
					WHERE
							agu.UserID = '".$ParUserID."'
							AND ag.RecordStatus = 1
                    ORDER BY AdminGroupRightName DESC ";
			return $this->returnVector($sql);
		}
		// ADMIN GROUP
		
		function Get_Chapter_List($YearID = '', $ZoneID = '', $filterByClassTeacher=false){
			$RC_EQUIPMENT = $this->DBName. '.RC_EQUIPMENT';
			$conds = "isDeleted = '0' ";
			if($YearID != ''){
				$conds .= " AND YearID = '$YearID' ";
			}
			if($ZoneID != ''){
				$conds .= " AND ZoneID = '$ZoneID' ";
			}
            if($filterByClassTeacher) {
                $YearIDArr = Get_Array_By_Key($this->Get_Teaching_Level($_SESSION['UserID']), 'YearID');
                $conds .= " AND YearID IN ('".implode("','", (array)$YearIDArr)."') ";
            }
			$sql = "SELECT DISTINCT Chapter FROM $RC_EQUIPMENT WHERE $conds";
			return $this->returnVector($sql);
		}

        function GET_ARCHIVE_REPORT_RELATED_LIST($yearClassID='', $termID='', $notGroupBy=false)
        {
            $RC_ARCHIVE_REPORT_CARD = $this->DBName.'.RC_ARCHIVE_REPORT_CARD';

            $conds = '';
            if($yearClassID != ''){
                $conds .= " AND YearClassID = '$yearClassID' ";
            }
            if($termID != ''){
                $conds .= " AND YearTermID = '$termID' ";
            }
            $groupBy = " GROUP BY YearID, YearClassID, YearTermID ";
            if($notGroupBy) {
                $groupBy = "";
            }

            $sql = "SELECT DISTINCT(StudentID), YearClassID, YearTermID FROM $RC_ARCHIVE_REPORT_CARD 
                      WHERE 1 $conds 
                      $groupBy ";
            return $this->returnArray($sql);
        }

		function GET_LASTEST_ARCHIVE_REPORT_INFO($yearID, $yearClassID, $termID, $studentID='')
        {
            $RC_ARCHIVE_REPORT_CARD = $this->DBName.'.RC_ARCHIVE_REPORT_CARD';

            $conds = '';
            if($yearID != ''){
                $conds .= " AND YearID = '$yearID' ";
            }
            if($yearClassID != ''){
                $conds .= " AND YearClassID = '$yearClassID' ";
            }
            if($termID != ''){
                $conds .= " AND YearTermID = '$termID' ";
            }
            if($studentID != ''){
                $conds .= " AND StudentID = '$studentID' ";
            }
            $sql = "SELECT DateInput FROM $RC_ARCHIVE_REPORT_CARD 
                      WHERE 1 $conds 
                      GROUP BY YearID, YearClassID, YearTermID 
                      ORDER BY DateInput DESC
                      LIMIT 1";
            return $this->returnVector($sql);
        }

        function UPDATE_ARCHIVE_STUDENT_REPORT($yearID, $yearClassID, $termID, $studentID)
        {
            $InsertArr = array();
            $InsertArr[] = " ('".$yearID."', '".$yearClassID."', '".$termID."', '".$studentID."', NOW(), '".$_SESSION['UserID']."') ";

            $RC_ARCHIVE_REPORT_CARD = $this->DBName.'.RC_ARCHIVE_REPORT_CARD';
            $sql = "INSERT INTO $RC_ARCHIVE_REPORT_CARD
						(YearID, YearClassID, YearTermID, StudentID, DateInput, InputBy)
					VALUES
						".implode(',', $InsertArr);
            return $this->db_db_query($sql);
        }

        function getEncryptedTextWithNoTimeChecking($plainText='', $key='bXwKs7S93J2') {
            global $intranet_root;

            include_once($intranet_root."/includes/liburlparahandler.php");

            $lurlparahandler = new liburlparahandler(liburlparahandler::actionEncrypt, $plainText, $key);
            return $lurlparahandler->getParaEncrypted();
        }

        //function getDecryptedTextWithNoTimeChecking($encryptedText='', $key='bXwKs7S93J2') {
        //    global $intranet_root;
        //    include_once($intranet_root."/includes/liburlparahandler.php");
        //
        //    $lurlparahandler = new liburlparahandler(liburlparahandler::actionDecrypt, $encryptedText, $key);
        //    $decrypted = $lurlparahandler->getParaDecrypted();
        //
        //    return $decrypted;
        //}
        
        #### Power Portfolio
        function Get_Rubric_Setting($settingID='', $yearID = ''){
        	$table = $this->DBName.'.RC_RUBRIC_SETTING';
        	$cols = "RubricSettingID, Name, YearID, LevelNum, GradeNum, ExamGradeNum, DateModified";
        	$conds = '1';
        	if($settingID != ''){
        		$conds .= " AND RubricSettingID = '$settingID' ";
        	} 
        	if($yearID != ''){
        		$conds .= " AND YearID = '$yearID' ";
        	}
        	$sql = "SELECT $cols FROM $table WHERE $conds";
        	$result = $this->returnArray($sql);
        	return $result;
        }
        function Insert_Rubric_Setting($dataAry = array()){
        	if(empty($dataAry)) return 0;
        	$table = $this->DBName.'.RC_RUBRIC_SETTING';
        	$colAry = array('Name', 'YearID', 'LevelNum', 'GradeNum','ExamGradeNum','InputBy', 'DateInput', 'ModifiedBy', 'DateModified');
        	$vals = array();
        	foreach($colAry as $col){
        		switch($col){
//         			case 'RubricsSettingID':
// 	        			$_thisVal[] = "'$settingID'";
// 	        			break;
        			case 'InputBy':
        			case 'ModifiedBy':
        				$_thisVal[] = "'{$_SESSION['UserID']}'";
        				break;
        			case 'DateInput';
        			case 'DateModified';
        				$_thisVal[] = 'NOW()';
        				break;
        			default:
	        			$_thisVal[] = "'$dataAry[$col]'";
	        			break;
        		}
        	}
        	$vals = implode(',', $_thisVal);
        	$values = '('.$vals.')';
        	$cols = implode(',', $colAry);
        	$sql = "INSERT INTO $table ($cols) VALUES $values";
        	$result = $this->db_db_query($sql);
        	return $this->db_insert_id();
        }
        function Update_Rubric_Setting($settingID, $dataAry = array()){
        	if(empty($dataAry)) return 0;
        	$table = $this->DBName.'.RC_RUBRIC_SETTING';
        	$colAry = array('Name', 'YearID', 'LevelNum', 'GradeNum','ExamGradeNum','ModifiedBy', 'DateModified');
        	$conds = "RubricSettingID = '$settingID' ";
        	$vals = array();
        	foreach($colAry as $col){
        		switch($col){
        			case 'ModifiedBy':
        				$vals[] = "$col = '{$_SESSION['UserID']}'";
        				break;
        			case 'DateModified';
	        			$vals[] = "$col = NOW()";
	        			break;
        			default:
        				$vals[] = "$col = '$dataAry[$col]'";
        				break;
        		}
        	}
        	$colVals = implode(',', $vals);
        	$sql = "UPDATE $table SET $colVals WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        function Delete_Rubric_Setting($settingID){
        	$result['Items'] = $this->Delete_Rubric_Setting_Item($settingID);
        	$table = $this->DBName.'.RC_RUBRIC_SETTING';
        	$conds = "RubricSettingID = '$settingID' ";
        	$sql = "DELETE FROM $table WHERE $conds";
        	$result['Setting'] = $this->db_db_query($sql);
        	return $result;
        }
        function Get_Rubric_Setting_Item($settingID, $type=''){
        	$table = $this->DBName.'.RC_RUBRIC_SETTING_ITEM';
        	$cols = "ItemID, RubricSettingID, Type, Level, Content";
        	$conds = " RubricSettingID = '$settingID'";
        	if($type!=''){
        		$conds .= " AND Type = '$type' ";
        	}
        	$sql = "SELECT $cols FROM $table WHERE $conds";
        	$result = $this->returnArray($sql);
        	return $result;
        }
        function Insert_Rubric_Setting_Item($settingID, $dataAry = array()){
        	if(empty($dataAry)) return 0;
        	$table = $this->DBName.'.RC_RUBRIC_SETTING_ITEM';
        	$colAry = array('RubricSettingID', 'Type', 'Level', 'Content');
        	$vals = array();
        	foreach($dataAry as $data){
        		$_thisVal = array();
        		foreach($colAry as $col){
        			if($col == 'RubricSettingID'){
        				$_thisVal[] = "'$settingID'";
        			} else {
        				$_thisVal[] = "'$data[$col]'";
        			}
        		}
        		$vals[] = implode(',', $_thisVal);
        	}
        	$values = '('.implode('),(',$vals).')';
        	$cols = implode(',', $colAry);
        	$sql = "INSERT INTO $table ($cols) VALUES $values";
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        function Delete_Rubric_Setting_Item($settingID, $type=''){
        	$table = $this->DBName.'.RC_RUBRIC_SETTING_ITEM';
        	$conds = " RubricSettingID = '$settingID'";
        	if($type!=''){
        		$conds .= " AND Type = '$type' ";
        	}
        	$sql = "DELETE FROM $table WHERE $conds";
        	$result = $this->db_db_query($sql);
			return $result;
        }
        function Get_Rubric_Ability_Index_Item($settingID){
        	$table = $this->DBName . '.RC_ABILITY_INDEX_ITEM';
        	$col = 'RubricSettingID,Code,Name,Type,Level,UpperCat,DateModified';
        	$conds = "RubricSettingID = '$settingID'";
        	$order = "Code ASC";
        	$sql = "SELECT $col FROM $table WHERE $conds ORDER BY $order";
        	$result = $this->returnArray($sql);
        	return $result;
        }
        function Upsert_Rubric_Ability_Index_Item($settingID, $dataAry = array()){
        	if(empty($dataAry)) return 0;
        	$table = $this->DBName . '.RC_ABILITY_INDEX_ITEM';
        	$colAry = array('Code', 'Name', 'Type', 'Level', 'UpperCat', 'DateModified');
        	$result = array('update'=>0,'insert'=>0);
        	foreach($dataAry as $data){
//         		if($data['ItemID']!=''){
//         			// update
//         			$conds = "ItemID = '{$data['ItemID']}'";
//         			$colVal = array();
//         			foreach($colAry as $col){
//         				switch($col){
//         					case 'Type':
//         					case 'Level':
//         						$colVal[] = "$col = ''";
//         						break;
//         					case 'UpperCat':
//         						$colVal[] = ($data[$col]=='')?"$col = NULL":"$col = '{$data[$col]}'";
//         						break;
//         					case 'DateModified':
//         						$colVal[] = "$col = NOW()";
//         						break;
//         					default:
//         						$colVal[] = "$col = '{$data[$col]}'";
//         						$colVal;
//         				}
//         			}
//         			$colValues = implode(',', $colVal);
//         			$sql = "UPDATE $table SET $colValues WHERE $conds";
//         			$result['update'] += $this->db_db_query($sql);
//         		} else {
        			// insert
        			$cols = 'RubricSettingID, DateInput,'.implode(',',$colAry);
        			$val = array();
        			$val[] = "'{$settingID}'";
        			$val[] = "NOW()";
        			foreach($colAry as $col){
        				switch($col){
        					case 'Type':
        						$val[] = "''";
        						break;
        					case 'UpperCat':
        						$val[] = ($data[$col]=='')?"NULL":"'{$data[$col]}'";
        						break;
        					case 'DateModified':
        						$val[] = 'NOW()';
        						break;
        					default:
        						$val[] = "'{$data[$col]}'";
        						break;
        				}
        			}
        			$values = '('.implode(',',$val).')';
        			$sql = "INSERT INTO $table ($cols) VALUES $values";
        			$result['insert'] += $this->db_db_query($sql);
        			$itemID = $this->db_insert_id();
//         			$this->Insert_Rubric_Ability_Index_Mapping($settingID, $itemID);
//         		}
        	}
        	return $result;
        }
        function Delete_Ability_Index_Item($settingID = '',$ItemID = ''){
        	$table = $this->DBName . '.RC_ABILITY_INDEX_ITEM';
        	$conds = "1";
        	if($settingID != ''){
        		$conds .= " AND RubricSettingID = '$settingID'";
        	}
        	if($ItemID != ''){
        		$conds = " AND ItemID = '$ItemID'";
        	}
        	$sql = "DELETE FROM $table WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }

        function Get_Rubric_Ability_Index_Mapping($settingID)
        {
        	$table = $this->DBName . '.RC_ABILITY_INDEX_MAPPING';
        	$joinTalbe = $this->DBName . '.RC_ABILITY_INDEX_ITEM';

        	$cols = "raii.ItemID, raii.Code, raii.Name, raii.UpperCat, raii.DateModified";
        	$conds = " raim.RubricSettingID = '$settingID' ";
        	$order = " raii.Code ASC";

        	$sql = "SELECT $cols
        			FROM $table raim
        			INNER JOIN $table raii
        			ON $joinConds
        			WHERE $conds
        			ORDER BY $order";
        	$result = $this->returnArray($sql);
        	return $result;
        }

        function Insert_Rubric_Ability_Index_Mapping($settingID, $itemID)
        {
        	$table = $this->DBName . '.RC_ABILITY_INDEX_MAPPING';

        	$cols = "RubricSettingID, ItemID, DateInput, DateModified";
        	$vals = "'$settingID', '$itemID', NOW(), NOW()";

        	$sql = "INSERT INTO $table ($cols) VALUES ($vals)";
        	$result = $this->db_db_query($sql);
        	return $result;
        }

        function Delete_Rubric_Ability_Index_Mapping($settingID, $itemID = '')
        {
        	$table = $this->DBName . '.RC_ABILITY_INDEX_MAPPING';

        	$conds = " RubricSettingID = '$settingID' ";
        	if($itemID != ''){
        		$conds .= " AND ItemID = '$itemID'";
        	}

        	$sql = "DELETE FROM $table WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }

        function Get_Rubric_Topic_Setting($settingID)
        {
        	$table = $this->DBName . '.RC_TOPIC_SETTING';

        	$cols = "TopicSettingID, Name, YearID, RubricSettingID, ExamPeriodStart,
					 ExamPeriodEnd, MarkPeriodStart, MarkPeriodEnd, RubricList, DateModified";
        	$conds = "TopicSettingID = '$settingID'";

        	$sql = "SELECT $cols FROM $table WHERE $conds";
        	$result = $this->returnArray($sql);
        	return $result;
        }

        function Get_Rubric_Topics($settingID='', $yearID='', $rubricID='', $withinExamPeriod=false, $withinMarkPeriod=false)
        {
            $table = $this->DBName . '.RC_TOPIC_SETTING';

            $cols = "TopicSettingID, Name, YearID, RubricSettingID, ExamPeriodStart,
					 ExamPeriodEnd, MarkPeriodStart, MarkPeriodEnd, RubricList, DateModified";
            $conds = " 1 ";
            if($settingID) {
                $conds .= " AND TopicSettingID = '$settingID' ";
            }
            if($yearID) {
                $conds .= " AND YearID = '$yearID' ";
            }
            if($rubricID) {
                $conds .= " AND RubricSettingID = '$rubricID' ";
            }
            if($withinExamPeriod) {
                $today = date('Y-m-d');
                $conds .= " AND ExamPeriodStart <= '$today' ";
                $conds .= " AND ExamPeriodEnd >= '$today' ";
            }
            if($withinMarkPeriod) {
                $today = date('Y-m-d');
                $conds .= " AND MarkPeriodStart <= '$today' ";
                $conds .= " AND MarkPeriodEnd >= '$today' ";
            }

            $sql = "SELECT $cols FROM $table WHERE $conds";
            $result = $this->returnArray($sql);
            return $result;
        }

        function Insert_Rubric_Topic_Setting($dataAry = array())
        {
        	if(empty($dataAry)) return 0;

        	$table = $this->DBName . '.RC_TOPIC_SETTING';

        	$colAry = array("Name", "YearID", "RubricSettingID", "ExamPeriodStart",
					 "ExamPeriodEnd", "MarkPeriodStart", "MarkPeriodEnd", "RubricList", 
        			 "DateInput", "InputBy", "DateModified", "ModifiedBy");
        	$val = array();
        	foreach($colAry as $col){
        		switch($col){
        			case 'DateInput':
        			case 'DateModified':
        				$val[] = 'NOW()';
        				break;
        			case 'InputBy':
        			case 'ModifiedBy':
        				$val[] = "'{$_SESSION['UserID']}'";
        				break;
        			default:
        				$val[] = "'{$dataAry[$col]}'";
        				break;
        		}
        	}
        	$cols = implode(',',$colAry);
        	$values = '('.implode(',',$val).')';

        	$sql = "INSERT INTO $table ($cols) VALUES $values";
        	$result = $this->db_db_query($sql);
        	return $this->db_insert_id();
        }

        function Update_Rubric_Topic_Setting($settingID, $dataAry = array())
        {
        	if(empty($dataAry)) return 0;

        	$table = $this->DBName.'.RC_TOPIC_SETTING';

        	$colAry = array("Name", "YearID", "RubricSettingID", "ExamPeriodStart",
        			"ExamPeriodEnd", "MarkPeriodStart", "MarkPeriodEnd", "RubricList",
        			"DateModified", "ModifiedBy");
        	$conds = "TopicSettingID = '$settingID' ";

        	$vals = array();
        	foreach($colAry as $col){
        		switch($col){
        			case 'ModifiedBy':
        				$vals[] = "$col = '{$_SESSION['UserID']}'";
        				break;
        			case 'DateModified';
        			$vals[] = "$col = NOW()";
        			break;
        			default:
        				$vals[] = "$col = '{$dataAry[$col]}'";
        				break;
        		}
        	}
        	$colVals = implode(',', $vals);

        	$sql = "UPDATE $table SET $colVals WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }

        function Delete_Rubric_Topic_Setting($settingID)
        {
        	$table = $this->DBName.'.RC_TOPIC_SETTING';

        	$conds = "RubricSettingID = '$settingID' ";

        	$sql = "DELETE FROM $table WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        
        function Get_Activity_Setting($activitySettingID)
        {
        	$table = $this->DBName.'.RC_ACTIVITY_SETTING';

        	$cols = "ReportName,ApplyTopic,ActivityTitle,
        			ActivityName,YearID,ActivityDate,ActivityTime,
        			TopicSettingID, RubricCode,Location,ContentType,
        			DateInput,InputBy,DateModified,ModifiedBy";
        	$conds = "ActivitySettingID = '$activitySettingID' ";

        	$sql = "SELECT $cols FROM $table WHERE $conds";
        	$result = $this->returnArray($sql);
        	return $result;
        }

        function Get_Activities($activityID='', $yearID='', $topicIDArr='')
        {
            $table = $this->DBName . '.RC_ACTIVITY_SETTING';

            $cols = "ActivitySettingID,ReportName,ApplyTopic,ActivityTitle,
        			ActivityName,YearID,ActivityDate,ActivityTime,
        			TopicSettingID, RubricCode,Location,ContentType,
        			DateInput,InputBy,DateModified,ModifiedBy";
            $conds = " 1 ";
            if($activityID) {
                $conds .= " AND ActivitySettingID = '$activityID' ";
            }
            if($yearID) {
                $conds .= " AND YearID = '$yearID' ";
            }
            if(!empty($topicIDArr)) {
                $conds .= " AND TopicSettingID IN ('".implode("','", $topicIDArr)."') ";
            }

            $sql = "SELECT $cols FROM $table WHERE $conds";
            $result = $this->returnArray($sql);
            return $result;
        }

        function Insert_Activity_Setting($dataAry = array())
        {
        	if(empty($dataAry)) return 0;

        	$table = $this->DBName.'.RC_ACTIVITY_SETTING';

        	$colAry = array('ReportName','ApplyTopic','ActivityTitle',
        			'ActivityName','YearID','ActivityDate','ActivityTime',
        			'TopicSettingID', 'RubricCode', 'Location','ContentType',
        			"DateInput", "InputBy", "DateModified", "ModifiedBy"
        	);
        	foreach($colAry as $col){
        		switch($col){
        			case 'DateInput':
        			case 'DateModified':
        				$val[] = 'NOW()';
        				break;
        			case 'InputBy':
        			case 'ModifiedBy':
        				$val[] = "'{$_SESSION['UserID']}'";
        				break;
        			default:
        				$val[] = "'{$dataAry[$col]}'";
        				break;
        		}
        	}
        	$cols = implode(',',$colAry);
        	$values = '('.implode(',',$val).')';

        	$sql = "INSERT INTO $table ($cols) VALUES $values";
        	$result = $this->db_db_query($sql);
        	return $this->db_insert_id();
        }
        
        function Update_Activity_Setting($activitySettingID, $dataAry = array())
        {
        	if(empty($dataAry)) return 0;

        	$table = $this->DBName.'.RC_ACTIVITY_SETTING';

        	$colAry = array('ReportName','ApplyTopic','ActivityTitle',
        			'ActivityName','YearID','ActivityDate','ActivityTime',
        			'TopicSettingID', 'RubricCode','Location','ContentType',
        			"DateModified", "ModifiedBy"
        	);
        	$conds = "ActivitySettingID = '$activitySettingID' ";

        	$vals = array();
        	foreach($colAry as $col){
        		switch($col){
        			case 'ModifiedBy':
        				$vals[] = "$col = '{$_SESSION['UserID']}'";
        				break;
        			case 'DateModified';
        			$vals[] = "$col = NOW()";
        			break;
        			default:
        				$vals[] = "$col = '{$dataAry[$col]}'";
        				break;
        		}
        	}
        	$colVals = implode(',', $vals);

        	$sql = "UPDATE $table SET $colVals WHERE $conds";
        	$result = $this->db_db_query($sql);
        }

        function Delete_Activity_Setting($activitySettingID)
        {
        	$table = $this->DBName.'.RC_ACTIVITY_SETTING';

        	$conds = "ActivitySettingID = '$activitySettingID' ";

        	$sql = "DELETE FROM $table WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }

        function Get_Zone_Setting($zoneID)
        {
        	$table = $this->DBName.'.RC_ZONE';

        	$cols = 'Name, Quota, PictureType, TopicSettingID,
        			InputBy, DateInput, ModifiedBy, DateModified';
        	$conds = "ZoneID = '$zoneID' ";

        	$sql = "SELECT $cols FROM $table WHERE $conds";
        	$result = $this->returnArray($sql);
        	return $result;
        }

        function Insert_Zone_Setting($dataAry = array())
        {
        	if(empty($dataAry)) return 0;

        	$table = $this->DBName.'.RC_ZONE';

        	$colAry = array(
        			'Name', 'Quota', 'PictureType', 'TopicSettingID',
        			'InputBy', 'DateInput', 'ModifiedBy', 'DateModified'
        	);
        	foreach($colAry as $col){
        		switch($col){
        			case 'DateInput':
        			case 'DateModified':
        				$val[] = 'NOW()';
        				break;
        			case 'InputBy':
        			case 'ModifiedBy':
        				$val[] = "'{$_SESSION['UserID']}'";
        				break;
        			default:
        				$val[] = "'{$dataAry[$col]}'";
        				break;
        		}
        	}
        	$cols = implode(',',$colAry);
        	$values = '('.implode(',',$val).')';

        	$sql = "INSERT INTO $table ($cols) VALUES $values";
        	$result = $this->db_db_query($sql);
        	return $this->db_insert_id();
        }
        
        function Update_Zone_Setting($zoneID, $dataAry = array())
        {
        	if(empty($dataAry)) return 0;

        	$table = $this->DBName.'.RC_ZONE';
        	$colAry = array(
        		'Name', 'Quota', 'PictureType', 'TopicSettingID',
        		'ModifiedBy', 'DateModified'
        	);
        	$conds = "ZoneID = '$zoneID' ";

        	$vals = array();
        	foreach($colAry as $col){
        		switch($col){
        			case 'ModifiedBy':
        				$vals[] = "$col = '{$_SESSION['UserID']}'";
        				break;
        			case 'DateModified';
        			$vals[] = "$col = NOW()";
        			break;
        			default:
        				$vals[] = "$col = '{$dataAry[$col]}'";
        				break;
        		}
        	}
        	$colVals = implode(',', $vals);

        	$sql = "UPDATE $table SET $colVals WHERE $conds";
        	$result = $this->db_db_query($sql);
        }

        function Delete_Zone_Setting($zoneID)
        {
        	$table = $this->DBName.'.RC_ZONE';

        	$conds = "ZoneID = '$zoneID' ";

        	$sql = "DELETE FROM $table WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }

        function Get_Tool_Setting($toolID)
        {
        	$table = $this->DBName.'.RC_TOOL';

        	$cols = 'Name, TopicSettingID, PhotoSRC, YearID, ZoneID, RubricCode,
        			InputBy, DateInput, ModifiedBy, DateModified';
        	$conds = "ToolID = '$toolID' ";

        	$sql = "SELECT $cols FROM $table WHERE $conds";
        	$result = $this->returnArray($sql);
        	return $result;
        }

        function Insert_Tool_Setting($dataAry = array())
        {
        	if(empty($dataAry)) return 0;

        	$table = $this->DBName.'.RC_TOOL';

        	$colAry = array(
        			'Name', 'TopicSettingID', 'PhotoSRC', 'YearID', 'ZoneID', 'RubricCode',
        			'InputBy', 'DateInput', 'ModifiedBy', 'DateModified'
        	);
        	foreach($colAry as $col){
        		switch($col){
        			case 'DateInput':
        			case 'DateModified':
        				$val[] = 'NOW()';
        				break;
        			case 'InputBy':
        			case 'ModifiedBy':
        				$val[] = "'{$_SESSION['UserID']}'";
        				break;
        			default:
        				$val[] = "'{$dataAry[$col]}'";
        				break;
        		}
        	}
        	$cols = implode(',',$colAry);
        	$values = '('.implode(',',$val).')';

        	$sql = "INSERT INTO $table ($cols) VALUES $values";
        	$result = $this->db_db_query($sql);
        	return $this->db_insert_id();
        }
        
        function Update_Tool_Setting($toolID, $dataAry = array())
        {
        	if(empty($dataAry)) return 0;

        	$table = $this->DBName.'.RC_TOOL';

        	$colAry = array(
        			'Name', 'TopicSettingID', 'PhotoSRC', 'YearID', 'ZoneID', 'RubricCode',
        			'ModifiedBy', 'DateModified'
        	);
        	$conds = "ToolID = '$toolID' ";
        	$vals = array();
        	foreach($colAry as $col){
        		switch($col){
        			case 'ModifiedBy':
        				$vals[] = "$col = '{$_SESSION['UserID']}'";
        				break;
        			case 'DateModified';
        			$vals[] = "$col = NOW()";
        			break;
        			default:
        				$vals[] = "$col = '{$dataAry[$col]}'";
        				break;
        		}
        	}
        	$colVals = implode(',', $vals);

        	$sql = "UPDATE $table SET $colVals WHERE $conds";
        	$result = $this->db_db_query($sql);
        }

        function Delete_Tool_Setting($toolID)
        {
        	$table = $this->DBName.'.RC_TOOL';

        	$conds = "ToolID = '$toolID' ";

        	$sql = "DELETE FROM $table WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }

        function Get_Report_Setting($ReportID = '')
        {
            $table = $this->DBName.'.RC_REPORT';

            $cols = 'ReportID, Name, YearID, StartDate, EndDate, 
                    CoverIncludeInfo, CoverTitle, CoverSubTitle, CAReportTitle, TopicID, ActivityID, 
                    TermAssessmentIncluded, TermAssessmentTitle, TermCommentIncluded, TermCommentTitle, PageLastPageIncludeInfo,
        			InputBy, DateInput, ModifiedBy, DateModified';
            if($ReportID != ''){
            $conds = " AND ReportID = '$ReportID' ";
            }

            $sql = "SELECT $cols FROM $table WHERE 1 $conds";
            $result = $this->returnArray($sql);
            return $result;
        }

        function Insert_Report_Setting($dataAry = array())
        {
            if(empty($dataAry)) return 0;

            $table = $this->DBName.'.RC_REPORT';

            $colAry = array(
                'Name', 'YearID', 'StartDate', 'EndDate',
                'CoverIncludeInfo', 'CoverTitle', 'CoverSubTitle', 'CAReportTitle', 'TopicID', 'ActivityID',
                'TermAssessmentIncluded', 'TermAssessmentTitle', 'TermCommentIncluded', 'TermCommentTitle', 'PageLastPageIncludeInfo',
                'InputBy', 'DateInput', 'ModifiedBy', 'DateModified'
            );
            foreach($colAry as $col){
                switch($col){
                    case 'DateInput':
                    case 'DateModified':
                        $val[] = 'NOW()';
                        break;
                    case 'InputBy':
                    case 'ModifiedBy':
                        $val[] = "'{$_SESSION['UserID']}'";
                        break;
                    default:
                        $val[] = "'{$dataAry[$col]}'";
                        break;
                }
            }
            $cols = implode(',',$colAry);
            $values = '('.implode(',',$val).')';

            $sql = "INSERT INTO $table ($cols) VALUES $values";
            $result = $this->db_db_query($sql);
            return $this->db_insert_id();
        }

        function Update_Report_Setting($ReportID, $dataAry = array())
        {
            if(empty($dataAry)) return 0;

            $table = $this->DBName.'.RC_REPORT';

            $colAry = array(
                'Name', 'YearID', 'StartDate', 'EndDate',
                'CoverIncludeInfo', 'CoverTitle', 'CoverSubTitle', 'CAReportTitle', 'TopicID', 'ActivityID',
                'TermAssessmentIncluded', 'TermAssessmentTitle', 'TermCommentIncluded', 'TermCommentTitle', 'PageLastPageIncludeInfo',
                'ModifiedBy', 'DateModified'
            );
            $conds = "ReportID = '$ReportID' ";

            $vals = array();
            foreach($colAry as $col){
                switch($col){
                    case 'ModifiedBy':
                        $vals[] = "$col = '{$_SESSION['UserID']}'";
                        break;
                    case 'DateModified';
                        $vals[] = "$col = NOW()";
                        break;
                    default:
                        $vals[] = "$col = '{$dataAry[$col]}'";
                        break;
                }
            }
            $colVals = implode(',', $vals);

            $sql = "UPDATE $table SET $colVals WHERE $conds";
            $result = $this->db_db_query($sql);
        }

        function Delete_Report_Setting($ReportID)
        {
            $table = $this->DBName.'.RC_REPORT';

            $conds = "ReportID = '$ReportID' ";

            $sql = "DELETE FROM $table WHERE $conds";
            $result = $this->db_db_query($sql);

            return $result;
        }
        
        function Get_Comment($CommentID='', $CatID='', $showComment = true, $showCat = false){
        	$table = $this->DBName.'.RC_COMMENT';
        	$cols = "CommentID, Content, CatID, InputBy, DateInput, ModifiedBy, DateModified";
        	$conds = " 1 ";
        	if(!$showComment){
        		$conds .= " AND CatID IS NULL";
        	}
        	if(!$showCat){
        		$conds .= " AND CatID IS NOT NULL";
        	}
        	if($CommentID!=''){
        		$conds .= " AND CommentID = ('{$CommentID}')";
        	}
        	if($CatID!=''){
        		$conds .= " AND CatID = ('{$CatID}')";
        	}
        	$sql = "SELECT $cols FROM $table WHERE $conds";
//         	debug_pr($sql);
        	$result  = $this->returnArray($sql);
        	return $result;
        }
        function Insert_Comment($CatID, $dataAry = array()){
        	if(empty($dataAry)) return 0;
        	$table = $this->DBName.'.RC_COMMENT';
        	
        	$colAry = array(
        			'CatID','Content','InputBy', 'DateInput', 'ModifiedBy', 'DateModified'
        	);
        	$val = array();
        	foreach($colAry as $col){
        		switch($col){
        			case 'CatID':
        				$val[] = "'{$CatID}'";
        				break;
        			case 'DateInput':
        			case 'DateModified':
        				$val[] = 'NOW()';
        				break;
        			case 'InputBy':
        			case 'ModifiedBy':
        				$val[] = "'{$_SESSION['UserID']}'";
        				break;
        			default:
        				$val[] = "'{$dataAry[$col]}'";
        				break;
        		}
        	}
        	$cols = implode(',',$colAry);
        	$values = '('.implode(',',$val).')';
        	
        	$sql = "INSERT INTO $table ($cols) VALUES $values";
        	$result = $this->db_db_query($sql);
        	return $this->db_insert_id();
        }
        function Insert_Comment_Cat($dataAry = array()){
        	if(empty($dataAry)) return 0;
        	$table = $this->DBName.'.RC_COMMENT';
        	
        	$colAry = array(
        			'CatID','Content','InputBy', 'DateInput', 'ModifiedBy', 'DateModified'
        	);
        	$val = array();
        	foreach($colAry as $col){
        		switch($col){
        			case 'CatID':
        				$val[] = "NULL";
        				break;
        			case 'DateInput':
        			case 'DateModified':
        				$val[] = 'NOW()';
        				break;
        			case 'InputBy':
        			case 'ModifiedBy':
        				$val[] = "'{$_SESSION['UserID']}'";
        				break;
        			default:
        				$val[] = "'{$dataAry[$col]}'";
        				break;
        		}
        	}
        	$cols = implode(',',$colAry);
        	$values = '('.implode(',',$val).')';
        	
        	$sql = "INSERT INTO $table ($cols) VALUES $values";
        	$result = $this->db_db_query($sql);
        	return $this->db_insert_id();
        }
        function Update_Comment($CommentID = '', $dataAry = array()){
        	if(empty($dataAry)) return 0;
        	
        	$table = $this->DBName.'.RC_COMMENT';
        	$colAry = array('Content','ModifiedBy', 'DateModified');
        	$conds = "CommentID = '$CommentID' ";
        	
        	$vals = array();
        	foreach($colAry as $col){
        		switch($col){
        			case 'ModifiedBy':
        				$vals[] = "$col = '{$_SESSION['UserID']}'";
        				break;
        			case 'DateModified';
        			$vals[] = "$col = NOW()";
        			break;
        			default:
        				$vals[] = "$col = '{$dataAry[$col]}'";
        				break;
        		}
        	}
        	$colVals = implode(',', $vals);
        	
        	$sql = "UPDATE $table SET $colVals WHERE $conds";
        	$result = $this->db_db_query($sql);
        }
        function Update_Comment_Cat($CatID = '', $dataAry = array()){
        	if(empty($dataAry)) return 0;
        	
        	$table = $this->DBName.'.RC_COMMENT';
        	$colAry = array('Content','ModifiedBy', 'DateModified');
        	$conds = "CommentID = '$CatID' ";
        	
        	$vals = array();
        	foreach($colAry as $col){
        		switch($col){
        			case 'ModifiedBy':
        				$vals[] = "$col = '{$_SESSION['UserID']}'";
        				break;
        			case 'DateModified';
        			$vals[] = "$col = NOW()";
        			break;
        			default:
        				$vals[] = "$col = '{$dataAry[$col]}'";
        				break;
        		}
        	}
        	$colVals = implode(',', $vals);
        	
        	$sql = "UPDATE $table SET $colVals WHERE $conds";
        	$result = $this->db_db_query($sql);
        }
        function Delete_Comment($CommentID){
        	$table = $this->DBName.'.RC_COMMENT';
        	$conds = "CommentID = '$CommentID'";
        	$sql = "DELETE FROM $table WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        function Delete_Comment_Cat($CatID){
        	$table = $this->DBName.'.RC_COMMENT';
        	$conds = "CommentID = '$CatID' OR CatID = 'CatID'";
        	$sql = "DELETE FROM $table WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        // Input Score Start
        function Get_Student_Score($StudentIDAry = array(), $TopicSettingIDAry = array(), $RubricCodeAry = array()){
        	$table = $this->DBName.'.RC_STUDENT_SCORE';
        	$cols = "InputScoreID, StudentID, TopicSettingID, RubricCode, isNA, Score";
        	$conds = "1";
        	if(!empty($StudentIDAry)){
        		$conds .= " AND StudentID IN ('" . implode("','", $StudentIDAry) . "') ";
        	}
        	if(!empty($RubricCodeAry)){
        		$conds .= " AND RubricCode IN ('" . implode("','", $RubricCodeAry) . "') ";
        	}
        	if(!empty($StudentIDAry)){
        		$conds .= " AND TopicSettingID IN ('" . implode("','", $TopicSettingIDAry) . "') ";
        	}
        	$sql = "SELECT $cols FROM $table WHERE $conds ORDER BY StudentID";
//         	debug_pr($sql);
        	$result = $this->returnArray($sql);
        	return $result;
        }
        
        function Insert_Student_Score($dataAry = array()){
        	if(empty($dataAry)){
        		return false;
        	}
        	$colAry = array('StudentID', 'TopicSettingID', 'RubricCode', 'isNA', 'Score', 'InputBy', 'DateInput', 'ModifiedBy', 'DateModified');
        	$cols = implode(',', $colAry);
        	$table = $this->DBName.'.RC_STUDENT_SCORE';
        	$valAry = array();
        	foreach($dataAry as $row){
        		$_thisValAry = array();
        		foreach($colAry as $col){
        			switch($col){
        				case 'InputBy':
        				case 'ModifiedBy':
        					$_thisValAry[] = "'{$_SESSION['UserID']}'";
        					break;
        				case 'DateInput':
        				case 'DateModified':
        					$_thisValAry[] = "NOW()";
        					break;
        				default:
        					$_thisValAry[] = "'{$row[$col]}'";
        					break;
        			}
        		}
        		$valAry[] = '('.implode(",", $_thisValAry) . ')';
        	}
        	$vals = implode(",",$valAry);
        	$sql = "INSERT INTO $table ($cols) VALUES $vals";
//         	debug_pr($sql);die();
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        function Delete_Student_Score($topicSettingID, $studentIDAry = array(), $rubricCodeAry = array()){
        	if(empty($studentIDAry)){
        		return false;
        	}
        	$conds = " TopicSettingID = '{$topicSettingID}' ";
        	$conds .= " AND StudentID IN ('" . implode("','",$studentIDAry) . "') ";
        	if(!empty($rubricCodeAry)){
        		$conds .= " AND RubricCode IN ('" . implode("','", $rubricCodeAry) . "') ";
        	}
        	$table = $this->DBName.'.RC_STUDENT_SCORE';
        	$sql = "DELETE FROM $table WHERE $conds";
//         	debug_pr($sql);die();
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        function Check_Class_Student_Score_Status($topicSettingID, $YearClassID, $RubricCode = ''){
        	$studentAry = $this->Get_Student_By_Class($YearClassID);
        	$studentTotal = sizeof($studentAry);
        	
        	$table = $this->DBName.'.RC_STUDENT_SCORE';
        	if($RubricCode != ''){
        		$rubricCond = " AND ss.RubricCode = '{$RubricCode}' ";
        		$rubricTotal = 1;
        	} else {
        		$TopicSettingInfo = $this->Get_Rubric_Topic_Setting($topicSettingID);
        		$TopicSettingInfo = $TopicSettingInfo[0];
        		$rubricSettingID = $TopicSettingInfo['RubricSettingID'];
        		$rubricIndexValueAry = $this->Get_Rubric_Ability_Index_Item($rubricSettingID);
        		$valueList = $TopicSettingInfo['RubricList'];
        		$valueListAry = explode(',', $valueList);
        		$rubricTotal = sizeof($valueListAry);
        	}
        	$sql = "SELECT COUNT(*) AS total FROM $table ss 
				INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu
					ON ss.StudentID = ycu.UserID 
				INNER JOIN {$intranet_db}.YEAR_CLASS yc
					ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '{$this->AcademicYearID}'
				WHERE ycu.YearClassID = '{$YearClassID}'  $rubricCond AND
				ss.TopicSettingID = '{$topicSettingID}' AND
				((isNA = '1' AND Score = '0') OR (Score <> '0' AND isNA = '0'))";
//         	debug_pr($sql);die();
        	$result = $this->returnArray($sql);
        	if(!empty($result)){
        		return $result[0]['total'] == ($studentTotal * $rubricTotal);
        	} else {
        		return false;
        	}
        }
        function Check_Student_Score_Status($topicSettingID, $StudentID){
        	$TopicSettingInfo = $this->Get_Rubric_Topic_Setting($topicSettingID);
        	$TopicSettingInfo = $TopicSettingInfo[0];
        	$rubricSettingID = $TopicSettingInfo['RubricSettingID'];
        	$rubricIndexValueAry = $this->Get_Rubric_Ability_Index_Item($rubricSettingID);
        	$valueList = $TopicSettingInfo['RubricList'];
        	$valueListAry = explode(',', $valueList);
        	$rubricTotal = sizeof($valueListAry);
        	
        	$table = $this->DBName.'.RC_STUDENT_SCORE';
        	$sql = "SELECT COUNT(*) AS total FROM $table ss
        	WHERE ss.StudentID = '{$StudentID}' AND
        	ss.RubricCode IN ('". implode("','", $valueListAry) . "') AND
			ss.TopicSettingID = '{$topicSettingID}' AND
        	((isNA = '1' AND Score = '0') OR (Score <> '0' AND isNA = '0'))";
//         	debug_pr($sql);die();
        	$result = $this->returnArray($sql);
        	if(!empty($result)){
        		return $result[0]['total'] == $rubricTotal;
        	} else {
        		return false;
        	}
        }
        // Input Score End
        // Activity Record Start
        function Get_Activity_Record($ActivitySettingID = '',$YearClassID = '', $StudentIDAry = array()){
        	$table = $this->DBName.'.RC_ACTIVITY';
        	$conds = "";
        	if($ActivitySettingID != ''){
        		$conds .= " AND ra.ActivitySettingID = '{$ActivitySettingID}' ";
        	}
        	if($YearClassID != ''){
        		$joinTableYC = " INNER JOIN YEAR_CLASS_USER ycu ON ra.StudentID = ycu.UserID 
								 INNER JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '{$this->AcademicYearID}' ";
        		$conds .= " AND yc.YearClassID = '{$YearClassID}' ";
        	}
        	if(!empty($StudentIDAry)){
        		$conds .= " AND ra.StudentID IN ('". implode("','", $StudentIDAry) . "') ";
        	}
        	$sql = "SELECT ra.RecordID, ra.StudentID, ra.ActivitySettingID, ra.ActivityDate, ra.Duration, ra.Location, ra.imgSrc, ra.Description
					FROM $table ra
					$joinTableYC
        			WHERE 1 $conds";
        	$result = $this->returnArray($sql);
//         	debug_pr($sql);die();
        	return $result;
        }
        function Insert_Activity_Record($dataAry = array()){
        	if(empty($dataAry)) return false;
        	$table = $this->DBName.'.RC_ACTIVITY';
        	$colAry = array('StudentID', 'ActivitySettingID', 'ActivityDate', 'Duration', 'Location', 'imgSrc', 'Description', 'InputBy', 'DateInput', 'ModifiedBy', 'DateModified');
        	$cols = implode(',', $colAry);
        	$valAry = array();
        	foreach($dataAry as $row){
        		$_thisValAry = array();
        		foreach($colAry as $col){
        			switch($col){
        				case 'InputBy':
        				case 'ModifiedBy':
        					$_thisValAry[] = "'{$_SESSION['UserID']}'";
        					break;
        				case 'DateInput':
        				case 'DateModified':
        					$_thisValAry[] = "NOW()";
        					break;
        				case 'Location':
        				case 'Duration':
        				case 'imgSrc':
        					$_thisValAry[] = $row[$col] ? "'{$row[$col]}'" : "NULL";
        					break;
        				default:
        					$_thisValAry[] = "'{$row[$col]}'";
        					break;
        			}
        		}
        		$valAry[] = '('.implode(",", $_thisValAry) . ')';
        	}
        	$vals = implode(",",$valAry);
        	$sql = "INSERT INTO $table ($cols) VALUES $vals";
//         	        	debug_pr($sql);die();
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        function Delete_Activity_Record($ActivitySettingID = '',$StudentIDAry = array()){
        	$table = $this->DBName.'.RC_ACTIVITY';
        	$conds = "";
        	if($ActivitySettingID != ''){
        		$conds .= " AND ActivitySettingID = '{$ActivitySettingID}' ";
        	}
        	if(!empty($StudentIDAry)){
        		$conds .= " AND StudentID IN ('". implode("','", $StudentIDAry) . "') ";
        	}
        	$sql = "DELETE 
		        	FROM $table 
		        	WHERE 1 $conds";
//         	debug_pr($sql);die();
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        // Activity Record End
	}// end of class
}
?>