<?php
// editing by 

/*****************************************************************************
 * Modification Log:
 *****************************************************************************/

if (!defined("LIBPOWERPORTFOLIO_UI_DEFINED"))
{
	define("LIBPOWERPORTFOLIO_UI_DEFINED", true);
	
	class libpowerportfolio_ui extends interface_html
	{	
		function libpowerportfolio_ui($parTemplate='') {
			$template = ($parTemplate=='')? 'default.html' : $parTemplate;
			$this->interface_html($template);
		}
		
		function Echo_Module_Layout_Start($parReturnMsg='', $parForPopup=false) {
			global $indexVar, $CurrentPage, $MODULE_OBJ, $TAGS_OBJ, $CurrentPageArr, $PATH_WRT_ROOT, $intranet_session_language;
			
			$MODULE_OBJ = $indexVar['libpowerportfolio']->Get_Module_Obj_Arr();
			
			if ($parForPopup) {
				$this->interface_html('popup.html');
			}
			$this->LAYOUT_START($parReturnMsg);
			
			echo '<!-- task: '.$indexVar['taskScript'].'-->';
			echo '<!-- template: '.$indexVar['templateScript'].'-->';
		}
		
		function Echo_Module_Layout_Stop() {
			$this->LAYOUT_STOP();
		}
		
		function Echo_Device_Layout_Start($type) {
			global $PowerPortfolioConfig;
			
			// Html Header
echo "<!DOCTYPE html>
<html lang='en'>
<head>
	<meta charset='utf-8'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge'>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name='description' content=''>
	<meta name='author' content=''>
	
	<title>澳門培正幼稚園系統</title>
	
	<!-- Bootstrap core CSS -->
	<link href='".$PowerPortfolioConfig['cssFilePath']."/bootstrap.min.css' rel='stylesheet'>
	<!-- Bootstrap theme -->
	<link href='".$PowerPortfolioConfig['cssFilePath']."/bootstrap-theme.min.css' rel='stylesheet'>
	
	<!-- Custom styles for this template -->
	<link href='".$PowerPortfolioConfig['cssFilePath']."/style.css?t=".time()."' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Didact+Gothic' rel='stylesheet' type='text/css'>
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src='https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js'></script>
		<script src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js'></script>
	<![endif]-->
				
	<!-- Bootstrap core JavaScript -->
	<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'></script>
	<script type='text/javascript' src='".$PowerPortfolioConfig['jsFilePath']."/bootstrap.min.js'></script>
</head>
			
<body ontouchstart=''>";

			// Teacher View Background
if($type == "teacher")
{
echo "

<div class='bgcloud teacher'>
	<div class='cloud_con size-5' style='left:-5%; top:10px'><img src='".$PowerPortfolioConfig['imageFilePath']."/teacher_cloud_1.png'/></div>
	<div class='cloud_con size-5' style='left:30%; top:5px'><img src='".$PowerPortfolioConfig['imageFilePath']."/teacher_cloud_1.png'/></div>
	<div class='cloud_con size-6' style='left:75%; top:70px'><img src='".$PowerPortfolioConfig['imageFilePath']."/teacher_cloud_1.png'/></div>
	<div class='cloud_con size-X' style='left:-10%; top:inherit; bottom:-12vh'><img src='".$PowerPortfolioConfig['imageFilePath']."/teacher_cloud_2.png'/></div>
</div>

";
}
			// Student View Background
else if($type == "student")
{
echo "

<div class='bgcloud student'>
	<div class='cloud_con size-1' style='left:8%; top:20px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
	<div class='cloud_con size-1' style='left:-10%; top:100px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
	<div class='cloud_con size-2' style='left:50%; top:50px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
	<div class='cloud_con size-2' style='left:95%; top:190px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
	<div class='cloud_con size-3' style='left:85%; top:10px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
	<div class='cloud_con size-3' style='left:72%; top:140px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
	<div class='cloud_con size-3' style='left:45%; top:385px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
	<div class='cloud_con size-3' style='left:70%; top:490px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
	<div class='cloud_con size-4' style='left:15%; top:200px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
	<div class='cloud_con size-4' style='left:86%; top:310px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
	<div class='cloud_con size-4' style='left:2%; top:430px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
	<div class='cloud_con size-5' style='left:-20%; top:450px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
	<div class='cloud_con size-5' style='left:95%; top:450px'><img src='".$PowerPortfolioConfig['imageFilePath']."/student_cloud.png'/></div>
</div>

";
}

echo "<div class='container' role='main'>

";
		}
		
		function Echo_Device_Layout_Stop() {
			echo "</div> <!-- /container -->

</body>

</html>";
		}
		
		function Get_Taiwan_Category_Selection($selectTWCat,$Onchage='document.form1.submit()') {
			global $Lang;
			
			$taiwanCatType = array();
			$taiwanCatType[] = array("身", "身");
			$taiwanCatType[] = array("認", "認");
			$taiwanCatType[] = array("語", "語");
			$taiwanCatType[] = array("社", "社");
			$taiwanCatType[] = array("情", "情");
			$taiwanCatType[] = array("美", "美");
			return getSelectByArray($taiwanCatType," id= \"selectTWCat\" name=\"selectTWCat\" onChange=\"$Onchage\"", $selectTWCat, 0, 0, $Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllTaiwanCat']);
		}
		
		function Get_Taiwan_ClassLevel_Selection($selectTWLevel) {
			global $Lang;
			
			$taiwanLevelType = array();
			$taiwanLevelType[] = array("小", "小");
			$taiwanLevelType[] = array("中", "中");
			$taiwanLevelType[] = array("大", "大");
			return getSelectByArray($taiwanLevelType," name=\"selectTWLevel\" onChange=\"document.form1.submit()\"", $selectTWLevel, 0, 0, $Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllTaiwanLevel']);
		}
		
		function Get_Macau_Category_Selection($selectMOCat) {
			global $Lang;
			
			$macauCatType = array();
			$macauCatType[] = array("A", "A");
			$macauCatType[] = array("B", "B");
			$macauCatType[] = array("C", "C");
			$macauCatType[] = array("D", "D");
			$macauCatType[] = array("E", "E");
			return getSelectByArray($macauCatType," name=\"selectMOCat\" onChange=\"document.form1.submit()\"", $selectMOCat, 0, 0, $Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllMacauCat']);
		}
		
		function Get_Year_Selection($selectedYear="", $onChange="document.form1.submit()", $allKGOptions=false, $noFirstTitle=false, $firstTitleType=0, $filterByClassTeacherWithSubjects=false, $defaultSelectFirstOpt=false)
		{
			global $indexVar, $UserID;

			// Non-admin filtering - class level
			$yearIdCond = '';
			/*
            if(!$indexVar["libreportcard"]->IS_POWER_PORTFOLIO_ADMIN_USER())
            {
                $yearIDArr = array();
                if($indexVar["libreportcard"]->IS_KG_CLASS_TEACHER()) {
                    $yearIDArr = array_merge($yearIDArr, Get_Array_By_Key($indexVar["libreportcard"]->Get_Teaching_Level($UserID), 'YearID'));
                }
                // Get teaching subject classes
                if($filterByClassTeacherWithSubjects && $indexVar["libreportcard"]->IS_KG_SUBJECT_TEACHER()) {
                    $yearIDArr = array_merge($yearIDArr, Get_Array_By_Key($indexVar["libreportcard"]->Get_Teaching_Subject_Group_Related_Class($_SESSION["UserID"], true), 'YearID'));
                }
                $yearIdCond = " AND YearID IN ('".implode("', '", (array)$yearIDArr)."') ";
            }
            */
            //$YearNameArr = array("K1", "K2", "K3");
            //$YearNameArr_Sql = implode("','", $YearNameArr);
			$sql = "SELECT 
						YearID, YearName
					FROM 
						YEAR 
					WHERE 
						1
						$yearIdCond
					ORDER BY
						YearName ";
			$temp = $indexVar["libpowerportfolio"]->returnResultSet($sql);

			$YearArr = array();
			foreach ($temp as $_tempKey => $_temp) {
			    if($defaultSelectFirstOpt && $selectedYear == '') {
                    $selectedYear = $_temp["YearID"];
                }
			    $YearArr[$_tempKey] = array($_temp["YearID"], $_temp["YearName"]);
			}
			if($allKGOptions) {
			    $YearArr[] = array("0", "K1 - K3");
			}
			return getSelectByArray($YearArr, " name='YearID' id='YearID' onChange='$onChange' ", $selectedYear, $firstTitleType, $noFirstTitle);
		}

        function Get_DB_Year_Selection($selectedYearID="", $onChange="document.form1.submit()", $noFirstTitle=false)
        {
            global $indexVar, $intranet_db;

            # All ReportCard DB
            $sql = "SHOW DATABASES LIKE '".$intranet_db."_DB_POWER_PORTFOLIO_%'";
            $DBYearList = $indexVar['libpowerportfolio']->returnVector($sql);

            # Year Selection
            $yearList = array();
            $allYearList = $indexVar['libpowerportfolio']->Get_All_AcademicYearID();
            foreach($allYearList as $thisYearInfo)
            {
                $thisYearID = $thisYearInfo["AcademicYearID"];
                $thisYearName = $thisYearInfo["AcademicYearName"];
                if(in_array($intranet_db."_DB_POWER_PORTFOLIO_".$thisYearID, $DBYearList)) {
                    $yearList[] = array($thisYearID, $thisYearName);
                }
            }

            return getSelectByArray($yearList, " name='DBYearID' id='DBYearID' onChange='$onChange' ", $selectedYearID, $noFirstTitle);
        }
		
		function Get_Semester_Selection($selectedSemester="", $onChange="document.form1.submit()", $allYearOption=false, $noFirstTitle=false, $firstTitleType=0, $selectionID='', $filterIDArr=array())
		{
			global $Lang, $indexVar;
			
			$dataAry = array();
			$YearTermAry = getSemesters($indexVar["libpowerportfolio"]->Get_Active_AcademicYearID());
			foreach ($YearTermAry as $thisTermId => $thisTermName) {
                if(!empty($filterIDArr) && !in_array($thisTermId, (array)$filterIDArr)){
                    continue;
                }
				$dataAry[] = array($thisTermId, $thisTermName);
			}
			if ($allYearOption) {
				$dataAry[] = array("0", $Lang["General"]["WholeYear"]);
			}
			
			$selectionID = $selectionID? $selectionID : 'TermID';
			return getSelectByArray($dataAry, " name='$selectionID' id='$selectionID' onChange='$onChange' ", $selectedSemester, $firstTitleType, $noFirstTitle);
		}
		
		function Get_Class_Selection($selectedClass='', $onChange='document.form1.submit()', $withYearOptGroup=false, $noFirstTitle=false, $selectYearID='', $selectionName='ClassID', $adminFilterByYearID=false, $filterIDArr=array())
		{
			global $indexVar, $UserID, $Lang, $button_select;
			
			//$yearNameCond = implode("', '", array('K1', 'K2', 'K3'));
			$yearIdCond = $selectYearID ? " AND YearID = '$selectYearID' " : "";
			
			//$sql = "SELECT YearID, YearName FROM YEAR WHERE YearName IN ('".$yearNameCond."') ".$yearIdCond." ORDER BY YearName ";
            $sql = "SELECT YearID, YearName FROM YEAR WHERE 1 ".$yearIdCond." ORDER BY YearName ";
			$result = $indexVar["libpowerportfolio"]->returnArray($sql);
			$yearIdAry = Get_Array_By_Key($result, "YearID");
			$yearIdNameMapAry = BuildMultiKeyAssoc($result, "YearID", "YearName", 1);
			$ClassAry = array();
			$classNameField = "".Get_Lang_Selection('ClassTitleB5','ClassTitleEN');
			if(!empty($filterIDArr)){
				$classFilterCond = " AND YearClassID IN ('" .implode("','",$filterIDArr) . "')";
			}
			$sql = "SELECT YearClassID, $classNameField AS ClassName, YearID
					FROM YEAR_CLASS
					WHERE YearID IN('".implode("','",$yearIdAry)."') 
					AND AcademicYearID = '{$indexVar['libpowerportfolio']->Get_Active_AcademicYearID()}'
					$classFilterCond";
			$result = $indexVar["libpowerportfolio"]->returnArray($sql);
			foreach($result as $classInfo){
				$ClassAry[] = array($classInfo['YearClassID'], $classInfo['ClassName'], $yearIdNameMapAry[$classInfo['YearID']]);
			}
			
			
// 			# Admin can access all classes - Get Target Class Info
// 			$ClassAry = array();
// 			if($indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_ADMIN_USER()) {
// 			    $allClassAry = $indexVar["libpowerportfolio"]->GetAllClassesInTimeTable();
// 			    foreach($allClassAry as $thisClassData) {
// 			        if(!$adminFilterByYearID || ($adminFilterByYearID && in_array($thisClassData["ClassLevelID"], $yearIdAry))) {
//                         if(!empty($filterIDArr) && !in_array($thisClassData["ClassID"], (array)$filterIDArr)){
//                             continue;
//                         }
// 				        $ClassAry[] = array($thisClassData["ClassID"], $thisClassData["ClassName"], $yearIdNameMapAry[$thisClassData["ClassLevelID"]]);
//                     }
// 				}
// 			}
// 			# Teaching Classes
// 			else {
// 				$allTeachingClassAry = $indexVar["libpowerportfolio"]->Return_Class_Teacher_Class($UserID);
// 				foreach($allTeachingClassAry as $thisClassData) {
// 					if(in_array($thisClassData["ClassLevelID"], $yearIdAry)) {
//                         if(!empty($filterIDArr) && !in_array($thisClassData["ClassID"], (array)$filterIDArr)){
//                             continue;
//                         }
// 					    $ClassAry[] = array($thisClassData["ClassID"], $thisClassData["ClassName"], $yearIdNameMapAry[$thisClassData["ClassLevelID"]]);
// 					}
// 				}
// 			}
			
			$selectTags = " id='".$selectionName."' name='".$selectionName."' onChange='".$onChange."' ";
			if($withYearOptGroup) {
			    $classSelection = $this->GET_SELECTION_BOX_WITH_OPTGROUP($ClassAry, $selectTags, "-- $button_select --", $selectedClass);   
			} else {
			    $classSelection = getSelectByArray($ClassAry, $selectTags, $selectedClass, 0, $noFirstTitle);
			}
			
			return $classSelection;
		}
		
		// $selectedClass='', $onChange='document.form1.submit()', $withYearOptGroup=false, $noFirstTitle=false, $selectYearID=''
		function Get_Timetable_Selection($selectedTimetableID='', $onChange='document.form1.submit()', $noFirstTitle=false, $selectYearID='')
		{
		    global $indexVar, $UserID, $Lang, $button_select;
		    
		    # Get TimeTable Info
		    $TimeTableAry = $indexVar["libpowerportfolio"]->GetTimeTable($selectYearID);
		    
		    # Build TimeTable Mapping
		    $TimeTableMapping = array();
		    foreach ($TimeTableAry as $thisAryData) {
		        $TimeTableMapping[$thisAryData["TopicTimeTableID"]][] = $thisAryData;
		    }
		    if($selectedTimetableID != '' && !empty($TimeTableMapping)) {
		        $_timeTableIdAry = array_keys((array)$TimeTableMapping);
		        if(!in_array($selectedTimetableID, $_timeTableIdAry)) {
		            return '';
		        }
		    }
		    
		    // loop Mapping
		    $TimeTableData = array();
		    $tempTimeTableID = '';
		    foreach($TimeTableMapping as $thisTimeTableID => $thisTimeTableInfo)
		    {
		        $thisTimeTableName = Get_Lang_Selection($thisTimeTableInfo[0]["CH_Name"], $thisTimeTableInfo[0]["EN_Name"]);
		        $thisTimeTableDateRange = $thisTimeTableInfo[0]["StartDate"]." - ".$thisTimeTableInfo[0]["EndDate"];
		        $thisTimeTableName .= " (".$thisTimeTableDateRange.")";
		        $TimeTableData[] = array($thisTimeTableID, $thisTimeTableName, $thisTimeTableDateRange);
		        
		        if($selectedTimetableID == '')
		        {
		            $currentDateTS = time();
		            $StartDateTS = strtotime($thisTimeTableInfo[0]["StartDate"]);
		            $EndDateTS = strtotime($thisTimeTableInfo[0]["EndDate"]);
		            $getDefaultTimeTable = ($currentDateTS >= $StartDateTS) && ($currentDateTS <= $EndDateTS);
    		        
    		        // for selected TimeTable
		            if($tempTimeTableID == '' || $getDefaultTimeTable) {
//     		            $thisTopicName = $Lang["eReportCardKG"]["Management"]["ToolScore"]["TitleName"]." :&nbsp;&nbsp;".Get_Lang_Selection($thisTimeTableInfo[0]["TopicNameB5"], $thisTimeTableInfo[0]["TopicNameEN"]);
		                $tempTimeTableID = $thisTimeTableID;
    		        }
		        }
		    }
		    
		    # TimeTable Selection
		    if(empty($TimeTableData)) {
		        return '';
		    } else {
		        sortByColumn2($TimeTableData, "2");
		        return getSelectByArray($TimeTableData," id= \"TimeTableID\" name=\"TimeTableID\" onChange=\"".$onChange."\"", ($selectedTimetableID? $selectedTimetableID : $tempTimeTableID), 0, $noFirstTitle);
		    }
		}
		
		function Get_Rubric_Setting_Selection($selectSetting='', $Onchange='document.form1.submit()', $yearID = ''){
			global $Lang, $indexVar, $PowerPortfolioConfig;
			$settingAry = $indexVar['libpowerportfolio']->Get_Rubric_Setting('', $yearID);
			$RubricSettingArr = array();
			foreach($settingAry as $setting){
				$RubricSettingArr[] = array($setting['RubricSettingID'], $setting['Name']);
			}
			return getSelectByArray($RubricSettingArr, "name='RubricSettingID' id='RubricSettingID' onChange='$Onchange'", $selectSetting, 0, 0);
		}

        function Get_Level_Selection($selectLevel='', $Onchange='document.form1.submit()'){
            global $Lang, $indexVar, $PowerPortfolioConfig;

            $LevelArr = array();
            foreach ($PowerPortfolioConfig['Levels'] as $currentLevel) {
                $LevelArr[] = array($currentLevel, str_replace('<!--level_num-->', $currentLevel, $Lang['PowerPortfolio']['General']['Level']));
            }
            return getSelectByArray($LevelArr," name='Levels' id='Levels' onChange='$Onchange'" , $selectLevel, 0, 0);
        }

        function Get_Grade_Num_Selection($selectGradeNum='', $Onchange='document.form1.submit()'){
            global $Lang, $indexVar, $PowerPortfolioConfig;

            $GradeNumArr = array();
            foreach ($PowerPortfolioConfig['GradeNum'] as $currentGradeNum) {
                $GradeNumArr[] = array($currentGradeNum, $currentGradeNum);
            }
            return getSelectByArray($GradeNumArr," name='GradeNum' id='GradeNum' onChange='$Onchange'" , $selectGradeNum, 0, 0);
        }

        function Get_Grade_Details_Div($selectGradeNum='', $settingID='')
        {
            global $Lang, $indexVar, $PowerPortfolioConfig, $image_path, $LAYOUT_SKIN;
            if($settingID!=''){
            	$contentAry = $indexVar['libpowerportfolio']->Get_Rubric_Setting_Item($settingID, 'GradeNumContent');
            	if(!empty($contentAry)) $contentAry= BuildMultiKeyAssoc($contentAry, 'Level');
            	$colorAry = $indexVar['libpowerportfolio']->Get_Rubric_Setting_Item($settingID, 'GradeNumColor');
            	if(!empty($colorAry)) $colorAry = BuildMultiKeyAssoc($colorAry, 'Level');
            } else {
            	$colorAry = array(
            			array('Content' => '#58a7fd'),
            			array('Content' => '#14bfbe'),
            			array('Content' => '#61c11b'),
            			array('Content' => '#feab5c'),
            			array('Content' => '#fe555a'),
            			array('Content' => '')
            	);
            	$colorAry = array_reverse($colorAry);
            }
            $divHtml = '<table>';

            //$GradeNumArr = array();
            for($i = (int)$selectGradeNum; $i > 0; $i--) {
            	if($i==$selectGradeNum){
                	$rangeDisplay = '('.$Lang['PowerPortfolio']['General']['Max'].')';
            	}else if($i == 1) {
                    $rangeDisplay = '('.$Lang['PowerPortfolio']['General']['Min'].')';
                } else{
                	$rangeDisplay = '';
                }
                $divHtml .= '<tr id="GradeRow_'.$i.'" style="" class="GradeRow"><td>'.$i.'</td><td style="" class="grade_range">'.$rangeDisplay.'</td>
                                <td>
                                    <input type="text" maxlength="25" class="textboxnum requiredField" name="GradeNumContent['.$i.']" id="GradeNumContent_'.$i.'" value="'.$contentAry[$i]['Content'].'">
                                    <br/>
                                    <div style="display:none;" class="warnMsgDiv" id="GradeNumContent_'.$i.'_Warn">
                                        <span class="tabletextrequire">*'.$Lang["PowerPortfolio"]["Setting"]["InputWarning"].'</span>
                                    </div>
                                </td>
                                <td>	
                                	<input type="text" class="colorPickerBox" name="GradeNumColor['.$i.']" id="GradeNumColor_'.$i.'" value="'.$colorAry[$i]['Content'].'"/>
								</td>
                                </tr>';
            }
            $divHtml .= '</table>';

            return $divHtml;
        }

        function Get_Exam_Grade_Num_Selection($selectExamGradeNum='', $Onchange='document.form1.submit()'){
            global $Lang, $indexVar, $PowerPortfolioConfig;

            $GradeNumArr = array();
            foreach ($PowerPortfolioConfig['ExamGradeNum'] as $currentGradeNum) {
                $GradeNumArr[] = array($currentGradeNum, $currentGradeNum);
            }
            return getSelectByArray($GradeNumArr," name='ExamGradeNum' id='ExamGradeNum' onChange='$Onchange'" , $selectExamGradeNum, 0, 0);
        }

        function Get_Exam_Grade_Details_Div($selectExamGradeNum='',$settingID = '')
        {
            global $Lang, $indexVar, $PowerPortfolioConfig, $image_path, $LAYOUT_SKIN;

            if($settingID!=''){
            	$contentAry = $indexVar['libpowerportfolio']->Get_Rubric_Setting_Item($settingID, 'ExamGradeNumContent');
            	if(!empty($contentAry)) $contentAry= BuildMultiKeyAssoc($contentAry, 'Level');
            	$colorAry = $indexVar['libpowerportfolio']->Get_Rubric_Setting_Item($settingID, 'ExamGradeNumColor');
            	if(!empty($colorAry)) $colorAry = BuildMultiKeyAssoc($colorAry, 'Level');
            } else {
            	$colorAry = array(
            			array('Content' => '#58a7fd'),
            			array('Content' => '#14bfbe'),
            			array('Content' => '#61c11b'),
            			array('Content' => '#feab5c'),
            			array('Content' => '#fe555a'),
            			array('Content' => '')
            	);
            	$colorAry = array_reverse($colorAry);
            }
            $divHtml = '<table>';
            for($i = (int)$selectExamGradeNum; $i > 0; $i--) {
            	if($i==$selectExamGradeNum){
            		$rangeDisplay = '('.$Lang['PowerPortfolio']['General']['Max'].')';
            	}else if($i == 1) {
            		$rangeDisplay = '('.$Lang['PowerPortfolio']['General']['Min'].')';
            	} else{
            		$rangeDisplay = '';
            	}
            	$divHtml .= '<tr id="ExamGradeRow_'.$i.'" style="" class="ExamGradeRow"><td>'.$i.'</td><td style="" class="grade_range">'.$rangeDisplay.'</td>
                                <td>
                                    <input type="text" maxlength="25" class="textboxnum requiredField" name="ExamGradeNumContent['.$i.']" id="ExamGradeNumContent_'.$i.'" value="'.$contentAry[$i]['Content'].'">
                                    <br/>
                                    <div style="display:none;" class="warnMsgDiv" id="ExamGradeNumContent_'.$i.'_Warn">
                                        <span class="tabletextrequire">*'.$Lang["PowerPortfolio"]["Setting"]["InputWarning"].'</span>
                                    </div>
                                </td>
                                <td>
                                	<input type="text" class="colorPickerBox" name="ExamGradeNumColor['.$i.']" id="ExamGradeNumColor_'.$i.'" value="'.$colorAry[$i]['Content'].'"/>
								</td>
                                </tr>';
            }
/*
            //$GradeNumArr = array();
            for($i = 8; $i > 0; $i--) {
                $rangeDisplay = $Lang['PowerPortfolio']['General']['Max'];
                //$rangeDisplayStyle = 'display:none;';
                if($i == 1) {
                    $rangeDisplay = $Lang['PowerPortfolio']['General']['Min'];
                    $rangeDisplayStyle = '';
                }
                $divHtml .= '<tr id="ExamGradeRow_'.$i.'" style="display:none;" class="ExamGradeRow">
                                <td>'.$Lang['PowerPortfolio']['General']['MaxAverage'].':</td>
                                <td style="'.$rangeDisplayStyle.'">
                                <input type="text" maxlength="3" class="textboxnum requiredField" name="Code" id="Code" value=""></td>
                                <td>'.$Lang['PowerPortfolio']['General']['GradingDetails'].':</td>
                                <td><input type="text" maxlength="25" class="textboxnum requiredField" name="Code" id="Code" value=""></td>
                                <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<!--<td bgcolor="#000000" id="color_'.$i.'"><a href="#"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ereportcard/icon_color.gif" width="20" height="20" border="0" onMouseOver="ChooseColor(this, \''.$i.'\')" onMouseOut="MM_showHideLayers(\'choosecolor'.$i.'\',\'\',\'hide\')"></a></td>-->
								</tr>
								</table>
</td>
                                </tr>';
            }
            $divHtml .= '</table>';
*/
            return $divHtml;
        }
		
		function Get_Tool_Cat_Selection($selectCat='', $Onchange='document.form1.submit()'){
			global $Lang, $indexVar;
			
			$CatArr = array();
			$allCats = $indexVar['libpowerportfolio']->Get_Equipment_Category();
			foreach ($allCats as $currentCat) {
				$currentCatName = Get_Lang_Selection($currentCat['CH_Name'], $currentCat['EN_Name']);
				$CatArr[] = array($currentCat['CatID'], '['.$currentCat['Code'].'] '.$currentCatName);
			}
			return getSelectByArray($CatArr," name='ToolCatID' id='ToolCatID' onChange='$Onchange'" , $selectCat, 0, 0);
		}
		
// 		function Get_Zone_Selection($selectZone='', $YearID='', $onChange='document.form1.submit()', $hasEmptyValue=false, $filterByClassTeacher=false) {
// 		    global $Lang, $indexVar;
		    
// 		    $ZoneArr = array();
// 		    $allZone = $indexVar['libpowerportfolio']->Get_Learning_Zone_Record('', $YearID, false, $filterByClassTeacher);
// 		    foreach ($allZone as $currentZone) {
// 		        $currentZoneName = Get_Lang_Selection($currentZone['ZoneNameB5'], $currentZone['ZoneNameEN']);
// 		        $ZoneArr[] = array($currentZone['ZoneID'], $currentZoneName);
// 		    }
// 		    return getSelectByArray($ZoneArr," name='ZoneID' id='ZoneID' onChange='$onChange'" , $selectZone, 0, 0);
// 		}

		function Get_Rubrics_Select_Div($rubricsID)
        {
            global $Lang, $indexVar;

            $html = '

	<br><br><br>
	<p>Select Rubics - 主題設定</p>
	<!-- Select Rubics -->
	<table class="table table-multi-levels">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th class="c-selected text-right">已選：50/50</th>
				<th class="c-icon"><a href="#" class="btn-select all selected"></a></th> <!-- add .selected if selected -->
				<th class="c-icon"><a href="#" class="btn-table-expand"></a></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<span class="indent"></span>
					<div class="text">1 分類一</div>
				</td>
				<td class="text-right">30/30</td>
				<td class="c-icon">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-table-expand"></a></td>
			</tr>
			<tr class="level-1 collapsible show">
				<td>
					<span class="indent"></span>
					<div class="text">1.1 子分類一</div>
				</td>
				<td class="text-right">&nbsp;</td>
				<td class="c-icon">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-table-expand"></a></td>
			</tr>
			<tr class="level-2 collapsible show">
				<td>
					<span class="indent"></span>
					<div class="text">1.1.1 項目一</div>
				</td>
				<td class="text-right">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-select selected"></a></td> <!-- add .selected if selected -->
				<td class="c-icon">&nbsp;</td>
			</tr>
			<tr class="level-2 collapsible show">
				<td>
					<span class="indent"></span>
					<div class="text">1.1.2 項目二</div>
				</td>
				<td class="text-right">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-select"></a></td>
				<td class="c-icon">&nbsp;</td>
			</tr>
			<tr class="level-1 collapsible show">
				<td>
					<span class="indent"></span>
					<div class="text">1.2 子分類二</div>
				</td>
				<td class="text-right">&nbsp;</td>
				<td class="c-icon">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-table-expand"></a></td>
			</tr>
			<tr class="level-2 collapsible show">
				<td>
					<span class="indent"></span>
					<div class="text">1.2.1 項目一</div>
				</td>
				<td class="text-right">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-select"></a></td>
				<td class="c-icon">&nbsp;</td>
			</tr>
			<tr class="level-2 collapsible show">
				<td>
					<span class="indent"></span>
					<div class="text">1.2.2 項目二</div>
				</td>
				<td class="text-right">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-select"></a></td>
				<td class="c-icon">&nbsp;</td>
			</tr>
		</tbody>
		<tbody>
			<tr>
				<td>
					<span class="indent"></span>
					<div class="text">2 分類二</div>
				</td>
				<td class="text-right">20/20</td>
				<td class="c-icon">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-table-expand"></a></td>
			</tr>
			<tr class="level-1 collapsible show" style="">
				<td>
					<span class="indent"></span>
					<div class="text">2.1 子分類一</div>
				</td>
				<td class="text-right">&nbsp;</td>
				<td class="c-icon">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-table-expand"></a></td>
			</tr>
			<tr class="level-2 collapsible show">
				<td>
					<span class="indent"></span>
					<div class="text">2.1.1 項目一</div>
				</td>
				<td class="text-right">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-select"></a></td>
				<td class="c-icon">&nbsp;</td>
			</tr>
			<tr class="level-2 collapsible show">
				<td>
					<span class="indent"></span>
					<div class="text">2.1.2 項目二</div>
				</td>
				<td class="text-right">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-select"></a></td>
				<td class="c-icon">&nbsp;</td>
			</tr>
			<tr class="level-1 collapsible show" style="">
				<td>
					<span class="indent"></span>
					<div class="text">2.2 子分類二</div>
				</td>
				<td class="text-right">&nbsp;</td>
				<td class="c-icon">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-table-expand"></a></td>
			</tr>
			<tr class="level-2 collapsible show">
				<td>
					<span class="indent"></span>
					<div class="text">2.2.1 項目一</div>
				</td>
				<td class="text-right">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-select"></a></td>
				<td class="c-icon">&nbsp;</td>
			</tr>
			<tr class="level-2 collapsible show">
				<td>
					<span class="indent"></span>
					<div class="text">2.2.2 項目二</div>
				</td>
				<td class="text-right">&nbsp;</td>
				<td class="c-icon"><a href="#" class="btn-select"></a></td>
				<td class="c-icon">&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<div class="table-multi-levels legend">
		<span class="btn-select all selected"></span>已全選所有項目
		<span class="btn-select selected"></span>已選項目
	</div>
	<!-- /Select Rubics -->';

            return $html;
        }
		
		function Get_No_Remarks_Index_Selection($gradingRange, $curCatID='', $onChange='document.form1.submit()')
		{
		    global $Lang, $indexVar;
		    
		    $cond = '';
		    if($curCatID) {
		        $cond = " AND CatID != '$curCatID' "; 
		    }
		    
		    // Get Ability (specific Grading) with Remarks
		    $sql = "SELECT CatID FROM ".$indexVar['thisDbName'].".RC_ABILITY_GRADE_REMARKS
                    WHERE GradingRangeID = '$gradingRange' ".$cond;
		    $existIndexIDAry = $indexVar['libpowerportfolio']->returnVector($sql);
		    
		    // Get Ability not using
		    $sql = "SELECT
                        CatID, CONCAT('[', Code, '] ', Name) as CatNameDisplay
                    FROM
                        ".$indexVar['thisDbName'].".RC_ABILITY_INDEX_CATEGORY
                    WHERE
                        CatID NOT IN ('".implode("', '", $existIndexIDAry)."') AND
                        Type = '0' AND Level = '3'
                    ORDER BY
                        CatNameDisplay";
		    $targetIndexAry = $indexVar['libpowerportfolio']->returnArray($sql);
		    
		    if(count($targetIndexAry) > 0) {
                return getSelectByArray($targetIndexAry," name='abilityIndexID' id='abilityIndexID' onChange='$onChange'" , $curCatID, 0, 1);
		    } else {
		        return '';
		    }
		}
		
		function Get_Device_Selection_Box($dataAry, $selectionTitle, $selectionType, $defaultValue, $onChange="") {
			// Build Selection Content (list)
			$listContent = "";
			foreach((array)$dataAry as $currentData) {
				$listContent .= "				<li id='".$selectionType."_".$currentData[0]."'><a onclick='updateSelection(\"".$currentData[0]."\", \"".$selectionType."_\", this); ".$onChange."' href='javascript:void(0);'>".$currentData[1]."</a></li>\r\n";
			}
			
			// Build Selection
			$x = "	<span class='dropdown-con'>
						".$selectionTitle."
						<span class='dropdown'>
							<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>
								<span id='".$selectionType."_title'>".$defaultValue."</span>
								<span class='glyphicon glyphicon-triangle-bottom'></span>
							</a>
							<ul class='dropdown-menu'>
								".$listContent."
							</ul>
						</span>
					</span>";
			return $x;
		}
		
		function Get_Device_Button($type, $href="", $onclick="") {
			global $PowerPortfolioConfig;
			
			$href = $href==""? " href='javascript:void(0);' " : " href='".$href."' ";
			$onclick = $onclick==""? "" : " onclick='".$onclick."' ";
			
			$x = "<a ".$href." ".$onclick." class='g-btn'><img src='".$PowerPortfolioConfig['imageFilePath']."/btn_".$type.".png'/></a>";
			return $x;
		}
		
		function Get_Device_Title_Info($ZoneName="", $TopicName="", $ClassName="", $LessonDate="") {
			$LessonDate = $LessonDate? $LessonDate : date("Y-m-d");
			
			$x = "	<div class='base-green'>
						<div class='head-left'><div class='pin'></div></div><div class='body'>
							<div class='group-name'>
								<div>".$ClassName."</div>
							</div>
							<div class='title-set'>
								<div class='fix-height-con'>
									<div class='up'>".$ZoneName."</div>
									<div class='down'>".$TopicName."</div>
								</div>
							</div>
							<div class='date'>".$LessonDate."</div>
						</div><div class='head-right'><div class='pin'></div></div>
					</div>";
			return $x;
		}
		
		function Get_Device_Class_Selection_Box($UserClassList, $onChange="") {
			global $Lang;
			
			$x = "";
			$SelectTitle = $Lang['eReportCardKG']['Management']['Device']['Class'];
			$SelectType = "class";
			
			$UserWithClasses = count((array)$UserClassList) > 0;
			if($UserWithClasses) {
				$x = $this->Get_Device_Selection_Box($UserClassList, $SelectTitle, $SelectType, $UserClassList[0]["ClassTitleEN"], $onChange);
			}
			else {
				$x = $this->Get_Device_Selection_Box("", $SelectTitle, $SelectType, $Lang['eReportCardKG']['Management']['Device']['NoClasses'], $onChange);
			}
			
			return $x;
		}
		
		function Get_Device_Topic_Selection_Box($UserClassTopicList, $onChange="") {
			global $Lang;
			
			$x = "";
			$SelectTitle = $Lang['eReportCardKG']['Management']['Device']['Topic'];
			$SelectType = "topic";
			
			// loop Available Topics
			$UserClassWithTopic = count((array)$UserClassTopicList) > 0;
			if($UserClassWithTopic) {
				$UserClassAvailableTopics = array();
				foreach($UserClassTopicList as $thisAvailableTopics) {
					$UserClassAvailableTopics[] = array($thisAvailableTopics["TopicID"], $thisAvailableTopics["TopicNameB5"]);
				}
				$x = $this->Get_Device_Selection_Box($UserClassAvailableTopics, $SelectTitle, $SelectType, $UserClassAvailableTopics[0][1], $onChange);
			}
			else {
				$x = $this->Get_Device_Selection_Box("", $SelectTitle, $SelectType, $Lang['eReportCardKG']['Management']['Device']['NoTopics'], $onChange);
			}
			
			return $x;
		}
		
		function Get_Device_Zone_Selection_Area($ClassTopicZoneList, $ClassLevelID) {
			global $PowerPortfolioConfig, $indexVar, $Lang;
			
			$x = "";
			
			$ClassTopicWithZone = count((array)$ClassTopicZoneList) > 0;
			if($ClassTopicWithZone)
			{
				# Get all Teaching Tools
				$allTeachingTools = $indexVar['libpowerportfolio']->GetTimeTable($ClassLevelID, "", 1);
				$allTeachingTools = BuildMultiKeyAssoc((array)$allTeachingTools, array("TopicID", "ZoneID", "ToolCodeID"));
				
				// loop Available Zone
				foreach($ClassTopicZoneList as $thisClassTopicZone)
				{
					$thisZoneInfo = $indexVar['libpowerportfolio']->Get_Learning_Zone_Record($thisClassTopicZone["ZoneID"], $ClassLevelID);
					$thisTopicZoneName = $thisClassTopicZone["ZoneNameB5"];
					$thisZonePictureType = $thisZoneInfo[0]["PictureType"];
					$thisZonePicutureName = "btn_subj_".$thisZonePictureType.".png";
					
					# Check any Teaching Tools in Learning Zone
					$thisTeachingTools = $allTeachingTools[$thisClassTopicZone["TopicID"]][$thisClassTopicZone["ZoneID"]];
					$withTeachingTool = !empty($thisTeachingTools);
					if($withTeachingTool) {
						$withTeachingTool = false;
						foreach($thisTeachingTools as $thisToolCodeID => $currentTeacherTool) {
							if(!empty($thisToolCodeID)) {
								$withTeachingTool = true;
								break;
							}
						}
					}
					$onClickAction = $withTeachingTool? "selectZone(\"".$thisClassTopicZone["ZoneID"]."\")" : "alert(\"".$Lang['eReportCardKG']['Management']['TopicTimeTable']['NoLearningTool']."\")";
					
					$x .= "<a onclick='$onClickAction' href='javascript:void(0);' class='subject-button'>";
						$x .= "<img src='".$PowerPortfolioConfig['imageFilePath']."/".$thisZonePicutureName."'/><br>".$thisTopicZoneName;
					$x .= "</a>";
				}
			}
			
			return $x;
		}
		
		function Get_Class_Student_Selection_Area($ClassStudentList, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID, $InputStatus="all", $EntryStatus="all", $specifyPhotoSize="", $specifyPhotoLeftLocation="", $ZoneQuota=0) {
			global $indexVar, $Lang;
			
			$x = "";
			
			// Get students under different status
			$ClassStudentIDAry = Get_Array_By_Key((array)$ClassStudentList, "UserID");
			if($ZoneQuota==0) {
				// Valid Student
				$ValidStudentList = $indexVar['libpowerportfolio']->Get_Available_Student($ClassStudentIDAry, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID, $InputStatus, $EntryStatus);
				
				// Marked Student
				$MarkedStudentList = $indexVar['libpowerportfolio']->Get_Available_Student($ClassStudentIDAry, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID, 1);
			}
			else {
				// In Zone Student
				$StudentInZoneAry = $indexVar['libpowerportfolio']->GET_STUDENT_IN_ZONE((array)$ClassStudentIDAry, $ClassLevelID, $TimeTableID, $TopicID, $ZoneID, "", $isUsingTools="1");
				$StudentInZoneAry = array_keys((array)$StudentInZoneAry);

				// Get Remaining Quota
				$usedQuota = count((array)$StudentInZoneAry);
				$withQuotaRemain = ($ZoneQuota - $usedQuota) > 0;
			}
			
			// loop students
			$thisStudentCount = count((array)$ClassStudentList);
			for($i=0; $i<$thisStudentCount; $i++)
			{
				// Student Info
				$this_studentid = $ClassStudentList[$i]["UserID"];
				$this_studentName = $ClassStudentList[$i]["StudentName"];
				$this_studentUserLogin = $ClassStudentList[$i]["UserLogin"];
				
				if($ZoneQuota==0) {
					// skip if not available
					if(!in_array($this_studentid, $ValidStudentList))	continue;
					
					// Check if student marked
					$isStudentMarked = in_array($this_studentid, $MarkedStudentList)? "photo_up" : "photo_up_teacher";
					
					// Set on click function
					$onClickFunction = "selectStudent(\"$this_studentid\")";
				}
				else {
					// Check if student in zone
					$isStudentMarked = in_array($this_studentid, $StudentInZoneAry)? "photo_up" : "photo_up_teacher";
					
					// Set on click function
					$onClickFunction = (!$withQuotaRemain && !in_array($this_studentid, $StudentInZoneAry))? "alert(\"".$Lang['eReportCardKG']['Management']['Device']['NoQuota']."\")" : 
					                       (in_array($this_studentid, $StudentInZoneAry)? "enterZone(\"$this_studentid\")" : "selectStudent(\"$this_studentid\", \"$ZoneID\")");
				}
				
				// Get Student Photo
				$specifyPhotoSize = $specifyPhotoSize? $specifyPhotoSize : "";
				$this_studentPhoto = $indexVar['libpowerportfolio']->Get_Student_Display_Photo($ClassStudentList[$i], $specifyPhotoSize);
				
				// Get Student Photo Area
				$x .= $this->Get_Class_Student_Photo($this_studentid, $this_studentName, $this_studentPhoto, $isStudentMarked, $specifyPhotoLeftLocation, $onClickFunction);
			}
			
			return $x;
		}
		
		function Get_Class_Student_Photo($StudentID, $StudentName, $StudentPhoto, $PhotoStyle="photo_up_teacher", $specifyPhotoLeftLocation="", $onClick="") {
			global $PowerPortfolioConfig;
			
			$specifyPhotoLeftLocation = $specifyPhotoLeftLocation? $specifyPhotoLeftLocation : "left: 19px";
			$x = "";
			$x .= "<li class='photo-con'>\r\n";
			$x .= "		<a onclick='".$onClick."' href='javascript:void(0);'>\r\n";
			$x .= "			<div class='bg'><img src='".$PowerPortfolioConfig['imageFilePath']."/photo_bg.png' /></div>\r\n";
			$x .= "			<div class='photo' style='".$specifyPhotoLeftLocation."'>".$StudentPhoto."</div>\r\n";
			$x .= "			<div class='bg-up'><img src='".$PowerPortfolioConfig['imageFilePath']."/".$PhotoStyle.".png'/></div>\r\n";
			$x .= "			<div class='ss-name'>".$StudentName."</div>\r\n";
			$x .= "		</a>\r\n";
			$x .= "</li>\r\n";
			
			return $x;
		}
		
		function Get_Topic_Cat_Select($targetTopicCatId="", $Onchange="document.form1.submit()")
		{
			global $indexVar, $Lang, $button_select;
			
			$topicCatArr = array();
			$topicCategoryAry = $indexVar["libpowerportfolio"]->getTopicCategory();
			foreach($topicCategoryAry as $thisTopicCategory) {
				$thisTopicCatArr = array();
				$thisTopicCatArr[] = $thisTopicCategory["TopicCatID"];
				$thisTopicCatArr[] = Get_Lang_Selection($thisTopicCategory["NameCh"], $thisTopicCategory["NameEn"]);
				$thisTopicCatArr[] = $thisTopicCategory["CatTypeName"];
				$topicCatArr[] = $thisTopicCatArr;
			}
			
			return $this->GET_SELECTION_BOX_WITH_OPTGROUP($topicCatArr, " name='TopicCatID' id='TopicCatID' onChange='$Onchange' ", "-- $button_select --", $targetTopicCatId);
		}
		
		function Get_Topic_DB_Table($topicCatId="", $yearId="", $filterByClassTeacher=false)
		{
			global $indexVar, $Lang;
			
			$x = "";
			$x .= "<table id='DataTable' class='common_table_list_v30'>"."\n";
				$x .= "<thead>"."\n";
					$x .= "<tr>"."\n";
						$x .= "<th style='width:3%;'>#</th>"."\n";
						$x .= "<th style='width:10%;'>".$Lang["General"]["Code"]."</th>"."\n";
						$x .= "<th style='width:15%;'>".$Lang["General"]["Type"]."</th>"."\n";
						$x .= "<th style='width:40%;'>".$Lang["General"]["Name"]."</th>"."\n";
						$x .= "<th style='width:10%;'>".$Lang["eReportCardKG"]["Setting"]["ApplyForm"]."</th>"."\n";
						$x .= "<th style='width:15%;'>".$Lang["General"]["Term"]."</th>"."\n";
						$x .= "<th style='width:5%;'>&nbsp;</th>"."\n";
						$x .= "<th style='width:2%;'><input type='checkbox' onclick='(this.checked) ? setChecked(1,this.form,\"topicCatIdAry[]\") : setChecked(0,this.form,\"topicCatIdAry[]\")' name='checkmaster'></th>"."\n";
					$x .= "<tr>"."\n";
				$x .= "</thead>"."\n";
				
				$x .= "<tbody>"."\n";
				
				// Get Topics
				$topicAry = $indexVar["libpowerportfolio"]->getTopics("", $topicCatId, $yearId, false, "", $filterByClassTeacher);
                if(!$indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_ADMIN_USER() && $indexVar["libpowerportfolio"]->IS_KG_CLASS_TEACHER() && $indexVar["libpowerportfolio"]->IS_KG_SUBJECT_TEACHER()) {
                    /*
                     * - Class Teacher & Subject Teacher : get topics related to teaching classes only
                     */
                    $topicInfoAry2 = $indexVar["libpowerportfolio"]->getTopics("", "", $yearId, false, "", true, true);
                    $topicAry = array_merge($topicAry, $topicInfoAry2);
                }
				$numOfTopic = count($topicAry);
				if ($numOfTopic == 0) {
					$x .= "<tr><td colspan='100%' style='text-align:center;'>".	$Lang["General"]["NoRecordAtThisMoment"]."</td></tr>"."\n";
				}
				else {
					for ($i=0; $i<$numOfTopic; $i++)
					{
						$_topicId = $topicAry[$i]["TopicID"];
						$_topicCode = $topicAry[$i]["Code"];
						$_topicCatTypeName = $topicAry[$i]["TopicCatTypeName"];
						$_topicName = Get_Lang_Selection($topicAry[$i]["NameCh"], $topicAry[$i]["NameEn"]);
						$_topicYearName = $topicAry[$i]["YearName"];
						$_topicTermName = $topicAry[$i]["TopicTermName"];
						
						$x .= "<tr id='tr_".$_topicId."'>"."\n";
							$x .= "<td><span class='rowNumSpan'>".($i + 1)."</td>"."\n";
							$x .= "<td>".$_topicCode."</td>"."\n";
							$x .= "<td>".$_topicCatTypeName."</td>"."\n";
							$x .= "<td>".$_topicName."</td>"."\n";
							$x .= "<td>".$_topicYearName."</td>"."\n";
							$x .= "<td>".$_topicTermName."</td>"."\n";
							$x .= "<td class='Dragable'>"."\n";
								$x .= $this->GET_LNK_MOVE("#", $Lang["Btn"]["Move"])."\n";
							$x .= "</td>"."\n";
							$x .= "<td>"."\n";
								$x .= "<input type='checkbox' class='ScaleChk' name='topicIdAry[]' value='".$_topicId."'>"."\n";
							$x .= "</td>"."\n";
						$x .= "</tr>"."\n";
					}
				}
				$x .= "</tbody>"."\n";
			$x .= "</table>"."\n";
			
			return $x;
		}
		
		function Get_Topic_Cat_DB_Table($catId="")
		{
			global $indexVar, $Lang;
			
			$x = "";
			$x .= "<table id='DataTable' class='common_table_list_v30'>"."\n";
				$x .= "<thead>"."\n";
					$x .= "<tr>"."\n";
						$x .= "<th style='width:3%;'>#</th>"."\n";
						$x .= "<th style='width:10%;'>".$Lang["General"]["Code"]."</th>"."\n";
						$x .= "<th style='width:15%;'>".$Lang["General"]["Type"]."</th>"."\n";
						$x .= "<th style='width:50%;'>".$Lang["General"]["Name"]."</th>"."\n";
						$x .= "<th style='width:5%;'>&nbsp;</th>"."\n";
						$x .= "<th style='width:2%;'><input type='checkbox' onclick='(this.checked) ? setChecked(1,this.form,\"topicCatIdAry[]\") : setChecked(0,this.form,\"topicCatIdAry[]\")' name='checkmaster'></th>"."\n";
					$x .= "<tr>"."\n";
				$x .= "</thead>"."\n";
				
				$x .= "<tbody>"."\n";
				
				// Get Topic Categories
				$topicCategoryAry = $indexVar['libpowerportfolio']->getTopicCategory($catId);
				$numOfTopicCategory = count($topicCategoryAry);
				if ($numOfTopicCategory == 0) {
					$x .= "<tr><td colspan='100%' style='text-align:center;'>".	$Lang["General"]["NoRecordAtThisMoment"]."</td></tr>"."\n";
				}
				else {
					for ($i=0; $i<$numOfTopicCategory; $i++)
					{
						$_topicCatId = $topicCategoryAry[$i]["TopicCatID"];
						$_topicCatCode = $topicCategoryAry[$i]["Code"];
						$_topicCatTypeName = $topicCategoryAry[$i]["CatTypeName"];
						$_topicCatName = Get_Lang_Selection($topicCategoryAry[$i]["NameCh"], $topicCategoryAry[$i]["NameEn"]);
						
						$x .= "<tr id='tr_".$_topicCatId."'>"."\n";
							$x .= "<td><span class='rowNumSpan'>".($i + 1)."</td>"."\n";
							$x .= "<td>".$_topicCatCode."</td>"."\n";
							$x .= "<td>".$_topicCatTypeName."</td>"."\n";
							$x .= "<td>".$_topicCatName."</td>"."\n";
							$x .= "<td class='Dragable'>"."\n";
								$x .= $this->GET_LNK_MOVE("#", $Lang["Btn"]["Move"])."\n";
							$x .= "</td>"."\n";
							$x .= "<td>"."\n";
								$x .= "<input type='checkbox' class='ScaleChk' name='topicCatIdAry[]' value='".$_topicCatId."'>"."\n";
							$x .= "</td>"."\n";
						$x .= "</tr>"."\n";
					}
				}
				$x .= "</tbody>"."\n";
			$x .= "</table>"."\n";
			
			return $x;
		}
		
		function Get_Topic_Score_Table($yearId="", $termId="")
		{
			global $indexVar, $PowerPortfolioConfig, $Lang;
			
			# Get related Classes
			// Admin > all classes
			if($indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_ADMIN_USER()){
				$relatedClassAry = $indexVar["libpowerportfolio"]->Get_All_KG_Class();
			}
            // Non-admin > related classes only
            else {
                $relatedClassAry = array();
                if($indexVar["libpowerportfolio"]->IS_KG_CLASS_TEACHER()) {
                    $teachingClassAry = $indexVar["libpowerportfolio"]->Get_Teaching_Class($_SESSION["UserID"]);
                    $relatedClassAry = array_merge($relatedClassAry, $teachingClassAry);
                }
                if($indexVar["libpowerportfolio"]->IS_KG_SUBJECT_TEACHER()) {
                    $subjectRelatedClassAry = $indexVar["libpowerportfolio"]->Get_Teaching_Subject_Group_Related_Class($_SESSION["UserID"]);
                    $relatedClassAry = array_merge($relatedClassAry, $subjectRelatedClassAry);
                }
                $teachingClassIDArr = Get_Array_By_Key((array)$teachingClassAry, "YearClassID");
                //$subjectRelatedClassIDArr = Get_Array_By_Key((array)$subjectRelatedClassAry, "YearClassID");
            }
			$relatedClassAry = BuildMultiKeyAssoc((array)$relatedClassAry, array("YearID", "YearClassID"));

			if($yearId > 0)
			{
				$tempClassAry = $relatedClassAry[$yearId];
				
				$relatedClassAry = array();
				$relatedClassAry[$yearId] = $tempClassAry;
			}
			$numOfRelatedClass = count($relatedClassAry);

            // subject code mapping
            $subjectCodeTypeMapping = array_flip($PowerPortfolioConfig['subjectIndicatorsCodeMap']);

            // special checking for subject teachers
            $subjectTeacherClassAry = array();
            if(!$indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_ADMIN_USER() && $indexVar["libpowerportfolio"]->IS_KG_SUBJECT_TEACHER())
            {
                // subject teacher groups
                $subjectTeacherGroupAry = $indexVar["libpowerportfolio"]->Get_Teaching_Subject_Group($_SESSION['UserID']);
                $subjectTeacherGroupAry = BuildMultiKeyAssoc($subjectTeacherGroupAry, 'RecordID', 'CODEID', 1);

                // loop related classes
                foreach((array)$subjectRelatedClassAry as $thisRelatedClass)
                {
                    $thisRelatedYearID = $thisRelatedClass['YearID'];
                    $thisRelatedYearClassID = $thisRelatedClass['YearClassID'];

                    $thisRelatedClassSubjectAry = $indexVar["libpowerportfolio"]->Get_KG_Form_Subject($thisRelatedYearID);
                    $thisRelatedClassSubjectAry = BuildMultiKeyAssoc($thisRelatedClassSubjectAry, 'RecordID');
                    foreach((array)$thisRelatedClassSubjectAry as $thisSubjectID => $thisSubjectInfo)
                    {
                        $thisSubjectClass = $indexVar["libpowerportfolio"]->Get_Teaching_Subject_Group_Related_Class($_SESSION["UserID"], true, $thisSubjectID);
                        if(!empty($thisSubjectClass)) {
                            $subjectTeacherClassAry[$thisRelatedYearClassID][$subjectTeacherGroupAry[$thisSubjectID]] = Get_Array_By_Key($thisSubjectClass, 'YearClassID');
                        }
                    }
                }
            }

            # Get Semesters
			$yearTermAry = getSemesters($indexVar["libpowerportfolio"]->Get_Active_AcademicYearID());
			if($termId > 0)
			{
				$tempTermName = $yearTermAry[$termId];
				
				$yearTermAry = array();
				$yearTermAry[$termId] = $tempTermName;
			}
			$numOfYearTerm = count($relatedClassAry);
			
			# Get Topics
            /*
             * - Admin / Class Teacher : get all form topics
             * - Subject Teacher : get topics related to teaching subjects only
             */
			$topicInfoAry = $indexVar["libpowerportfolio"]->getTopics("", "", $yearId);
            if(!$indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_ADMIN_USER() && $indexVar["libpowerportfolio"]->IS_KG_CLASS_TEACHER() && $indexVar["libpowerportfolio"]->IS_KG_SUBJECT_TEACHER()) {
                /*
                 * - Class Teacher & Subject Teacher : get topics related to teaching classes only
                 */
                $topicInfoAry2 = $indexVar["libpowerportfolio"]->getTopics("", "", $yearId, false, "", true, true);
                $topicInfoAry = array_merge($topicInfoAry, $topicInfoAry2);
            }
			$topicInfoAry = BuildMultiKeyAssoc((array)$topicInfoAry, array("YearID", "CatID"));
			$numOfTopics = count($topicInfoAry);

			$x = "";
			$x .= "<table id='DataTable' class='common_table_list_v30'>"."\n";
				$x .= "<thead>"."\n";
					$x .= "<tr>"."\n";
						$x .= "<th style='width:2%;'>#</th>"."\n";
						$x .= "<th style='width:14%;'>".$Lang["General"]["Class"]."</th>"."\n";
						$x .= "<th style='width:14%;'>".$Lang["General"]["Term"]."</th>"."\n";
						$x .= "<th style='width:60%;'>".$Lang["eReportCardKG"]["Management"]["LanguageBehavior"]["Title"]."</th>"."\n";
						$x .= "<th style='width:10%;'>&nbsp;</th>"."\n";
					$x .= "<tr>"."\n";
				$x .= "</thead>"."\n";
				
				$x .= "<tbody>"."\n";
				
				if ($numOfRelatedClass == 0 || $numOfYearTerm == 0 || $numOfTopics == 0)
				{
					$x .= "<tr><td colspan='100%' style='text-align:center;'>".	$Lang["General"]["NoRecordAtThisMoment"]."</td></tr>"."\n";
				}
				else
				{
					$row_count = 1;
					foreach($relatedClassAry as $_yearId => $relatedFormClassAry)
					{
						$thisFormTopicAry = $topicInfoAry[$_yearId];
						if(!empty($topicInfoAry[0]))
						{
							if(empty($thisFormTopicAry)) {
								$thisFormTopicAry = $topicInfoAry[0];
							}
							else {
								$thisFormTopicAry = $thisFormTopicAry + $topicInfoAry[0];
							}
						}

						$numOfFormTopics = count($thisFormTopicAry);
						if($numOfFormTopics > 0)
						{
							foreach((array)$relatedFormClassAry as $_classId => $thisClassInfo)
							{
								foreach($thisFormTopicAry as $_topicCatId => $thisTopicCatInfo)
								{
                                    // skip not related classes
                                    if(!$indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_ADMIN_USER())
                                    {
                                        $_topicSubjectCode = $subjectCodeTypeMapping[$thisTopicCatInfo["TopicCatCode"]];

                                        if($indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_CLASS_TEACHER() && in_array($_classId, (array)$teachingClassIDArr)) {
                                            // do nothing
                                        }
                                        else if ($indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_SUBJECT_TEACHER() && $_topicSubjectCode != '' && in_array($_classId, (array)$subjectTeacherClassAry[$_classId][$_topicSubjectCode])) {
                                            // do nothing
                                        }
                                        else {
                                            continue;
                                        }
                                    }

									foreach($yearTermAry as $_termId => $_termName)
									{
										$_className = Get_Lang_Selection($thisClassInfo["ClassTitleB5"], $thisClassInfo["ClassTitleEN"]);
										$_topicCatName = $thisTopicCatInfo["TopicCatTypeName"];
										
										$x .= "<tr>"."\n";
											$x .= "<td><span class='rowNumSpan'>".$row_count."</td>"."\n";
											$x .= "<td>".$_className."</td>"."\n";
											$x .= "<td>".$_termName."</td>"."\n";
											$x .= "<td>".$_topicCatName."</td>"."\n";
											$x .= "<td>"."\n";
												$x .= "<a href='javascript:goEdit(\"$_classId\", \"$_termId\", \"$_topicCatId\")'>";
													$x .= "<img src='/images/2009a/icon_edit_b.gif' width='20' height='20' border='0' title='Edit'>";
												$x .= "</a>";
											$x .= "</td>"."\n";
										$x .= "</tr>"."\n";
										
										$row_count++;
									}
								}
							}
						}
					}
				}
				$x .= "</tbody>"."\n";
			$x .= "</table>"."\n";
			
			return $x;
		}
		
		function Get_Form_Subject_List_Table($YearID, $SubjectID, $YearTermID='', $TimeTableID='')
		{
		    global $indexVar, $Lang;
		    
		    # Get related Classes
		    // Admin > all classes
		    if($indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_ADMIN_USER()) {
		        $relatedClassAry = $indexVar["libpowerportfolio"]->Get_All_KG_Class();
		    }
            // Non-admin > related classes only
		    else {
                $relatedClassAry = array();
		        if($indexVar["libpowerportfolio"]->IS_KG_CLASS_TEACHER()) {
                    $teachingClassAry = $indexVar["libpowerportfolio"]->Get_Teaching_Class($_SESSION["UserID"]);
                    $relatedClassAry = array_merge($relatedClassAry, $teachingClassAry);
                }
                if($indexVar["libpowerportfolio"]->IS_KG_SUBJECT_TEACHER()) {
                    $subjectRelatedClassAry = $indexVar["libpowerportfolio"]->Get_Teaching_Subject_Group_Related_Class($_SESSION["UserID"], true);
                    $relatedClassAry = array_merge($relatedClassAry, $subjectRelatedClassAry);
                }
                $teachingClassIDArr = Get_Array_By_Key((array)$teachingClassAry, "YearClassID");
                $subjectRelatedClassIDArr = BuildMultiKeyAssoc((array)$subjectRelatedClassAry, array("YearID", "YearClassID"));
            }
		    $relatedClassAry = BuildMultiKeyAssoc((array)$relatedClassAry, array("YearID", "YearClassID"));

		    if($YearID > 0) {
		        $tempClassAry = $relatedClassAry[$YearID];
		        
		        $relatedClassAry = array();
		        $relatedClassAry[$YearID] = $tempClassAry;
		    }
		    $numOfRelatedClass = count($relatedClassAry);
		    
		    # Get Subjects
		    $subjectAry = array();
		    $subjectTeacherClassAry = array();
	        foreach($relatedClassAry as $thisYearID => $thisYearClassAry) {
	            $thisFormSubjectAry = $indexVar["libpowerportfolio"]->Get_KG_Form_Subject($thisYearID);
	            $thisFormSubjectAry = BuildMultiKeyAssoc($thisFormSubjectAry, 'RecordID');
	            $subjectAry[$thisYearID] = $thisFormSubjectAry;
	            
	            if($SubjectID > 0 && in_array($SubjectID, array_keys($thisFormSubjectAry))) {
	                $tempSubjectAry = $thisFormSubjectAry[$SubjectID];
	                
	                $subjectAry[$thisYearID] = array();
	                $subjectAry[$thisYearID][$SubjectID]= $tempSubjectAry;
	            }

	            // special checking for subject teachers
	            if(!$indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_ADMIN_USER() && $indexVar["libpowerportfolio"]->IS_KG_SUBJECT_TEACHER()) {
                    foreach((array)$subjectAry[$thisYearID] as $thisSubjectID => $thisSubjectInfo) {
                        $thisSubjectClass = $indexVar["libpowerportfolio"]->Get_Teaching_Subject_Group_Related_Class($_SESSION["UserID"], true, $thisSubjectID);
                        $subjectTeacherClassAry[$thisYearID][$thisSubjectID] = Get_Array_By_Key($thisSubjectClass, 'YearClassID');
                    }
	            }
	        }
		    $numOfFormSubjects = count($subjectAry);

		    $x = "";
		    $x .= "<table id='DataTable' class='common_table_list_v30'>"."\n";
		    $x .= "<thead>"."\n";
		    $x .= "<tr>"."\n";
		    $x .= "<th style='width:2%;'>#</th>"."\n";
                $x .= "<th style='width:15%;'>".$Lang["General"]["Class"]."</th>"."\n";
    		    $x .= "<th style='width:20%;'>".$Lang['eReportCardKG']['Management']['SubjectIndexScore']['Subject']."</th>"."\n";
    		    $x .= "<th style='width:53%;'>".$Lang['eReportCardKG']['Management']['SubjectIndexScore']['TermTimeTable']."</th>"."\n";
    		    $x .= "<th style='width:10%;'>&nbsp;</th>"."\n";
		    $x .= "<tr>"."\n";
		    $x .= "</thead>"."\n";
		    
		    $x .= "<tbody>"."\n";
		    
		    $row_count = 1;
	        if ($numOfRelatedClass > 0 && $numOfFormSubjects > 0)
	        {
		        foreach($relatedClassAry as $yearID => $relatedFormClassAry)
		        {
		            $currentFormSubjectMap = $indexVar["libpowerportfolio"]->getAllSubjectMapping($isDeleted=true, $CodeID='', $yearID, array_keys($subjectAry[$yearID]));
		            $numOfFormSubjectTopics = count($currentFormSubjectMap);
		            if($numOfFormSubjectTopics > 0)
		            {
		                foreach((array)$relatedFormClassAry as $yearClassID => $thisClassInfo)
		                {
    		                foreach((array)$currentFormSubjectMap as $thisFormSubjectMap)
    		                {
                                // skip not related classes
    		                    if(!$indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_ADMIN_USER())
                                {
                                    if($indexVar["libpowerportfolio"]->IS_KG_CLASS_TEACHER() && in_array($yearClassID, (array)$teachingClassIDArr)) {
                                        // do nothing
                                    }
                                    else if ($indexVar["libpowerportfolio"]->IS_KG_SUBJECT_TEACHER() && in_array($yearClassID, $subjectTeacherClassAry[$yearID][$thisFormSubjectMap['SubjectID']])) {
                                        // do nothing
    		                        }
    		                        else {
                                        continue;
                                    }
    		                    }
    		                    
    		                    $isUseTermSettings = $thisFormSubjectMap['isTerm'] == 1;
    		                    if($isUseTermSettings && $YearTermID > 0) {
    		                        if($thisFormSubjectMap['TermID'] != $YearTermID) {
    		                            continue;
    		                        }
    		                    } else if (!$isUseTermSettings && $TimeTableID > 0) {
    		                        if($thisFormSubjectMap['TopicID'] != $TimeTableID) {
    		                            continue;
    		                        }
    		                    }
    		                    
	                            $x .= "<tr>"."\n";
	                            $x .= "<td><span class='rowNumSpan'>".$row_count."</td>"."\n";
	                                $x .= "<td>".Get_Lang_Selection($thisClassInfo["ClassTitleB5"], $thisClassInfo["ClassTitleEN"])."</td>"."\n";
		                            $x .= "<td>".$thisFormSubjectMap['Name']."</td>"."\n";
		                            $x .= "<td>".$thisFormSubjectMap['TT']."</td>"."\n";
		                            $x .= "<td>"."\n";
		                                $x .= "<a href='javascript:goEdit(\"".$yearClassID."\", \"".$thisFormSubjectMap['SubjectID']."\", \"".$thisFormSubjectMap['CodeID']."\")'>";
                                            $x .= "<img src='/images/2009a/icon_edit_b.gif' width='20' height='20' border='0' title='Edit'>";
    		                            $x .= "</a>";
		                            $x .= "</td>"."\n";
	                            $x .= "</tr>"."\n";
	                            
	                            $row_count++;
		                    }
		                }
		            }
		        }
		    }
		    if($row_count == 1) {
		        $x .= "<tr><td colspan='100%' style='text-align:center;'>".	$Lang["General"]["NoRecordAtThisMoment"]."</td></tr>"."\n";
		    }
		    
		    $x .= "</tbody>"."\n";
		    $x .= "</table>"."\n";
		    
		    return $x;
		}
		
		function Get_Other_Info_Class_Table($UploadType, $TermID, $YearID = '')
		{
		    global $indexVar, $UserID, $Lang;
		    
		    # Get all Class
		    $allClassInfoArr = $indexVar['libpowerportfolio']->Get_All_KG_Class();
		    $allClassInfoArr = BuildMultiKeyAssoc($allClassInfoArr, array('YearClassID'));
		    
		    # Admin can access all classes - Get Target Class Info
		    $ClassInfoArr = array();
		    if($indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_ADMIN_USER())
		    {
		        $allClassesAry = $indexVar["libpowerportfolio"]->GetAllClassesInTimeTable();
		        foreach($allClassesAry as $thisClassData)
		        {
		            $thisClassInfo = $allClassInfoArr[$thisClassData["ClassID"]];
		            if($thisClassInfo)
		            {
		                $ClassInfoArr[] = $thisClassInfo;
		            }
		        }
		    }
		    # Teaching Classes
		    else
		    {
		        $allTeachingClasses = $indexVar["libpowerportfolio"]->Return_Class_Teacher_Class($UserID);
		        foreach($allTeachingClasses as $thisClassData)
		        {
		            $thisClassInfo = $allClassInfoArr[$thisClassData["ClassID"]];
		            if($thisClassInfo)
		            {
		                $ClassInfoArr[] = $thisClassInfo;
		            }
		        }
		    }
		    
		    $ClassArr = BuildMultiKeyAssoc($ClassInfoArr, array('YearID', 'YearClassID'));
		    $ClassIDArr = BuildMultiKeyAssoc($ClassInfoArr, array('YearClassID'), array("YearClassID"), 1, 0);
		    
		    $FormInfoArr = $indexVar['libpowerportfolio']->Get_All_KG_Form();
		    $FormArr = BuildMultiKeyAssoc($FormInfoArr, array('YearID'));
		    
		    $LastModifiedInfo = $indexVar['libpowerportfolio']->getOtherInfoClassLastModified($UploadType, $TermID, $ClassIDArr);
		    $LastModifiedInfo = BuildMultiKeyAssoc($LastModifiedInfo, "ClassID");
		    
		    $x = '';
		    $x .= '<table id="OtherInfoTable" class="common_table_list_v30">'."\n";
    		    $x .= '<col align="left" style="width: 20%;">'."\n";
    		    $x .= '<col align="left">'."\n";
    		    $x .= '<col align="left">'."\n";
    		    $x .= '<col align="left">'."\n";
    		    $x .= '<col align="left" style="width: 100px;">'."\n";
                
    		    // Table Header
    		    $x .= '<thead>'."\n";
        		    $x .= '<tr>'."\n";
                        $x .= '<th>'.$Lang['General']['Form'].'</th>'."\n";
        		        $x .= '<th colspan="4" class="sub_row_top">'.$Lang['General']['Class'].'</th>'."\n";
        		    $x .= '</tr>'."\n";
        		    $x .= '<tr>'."\n";
            		    $x .= '<th>'.$Lang['General']['Name'].'</th>'."\n";
            		    $x .= '<th class="sub_row_top">'.$Lang['General']['Name'].'</th>'."\n";
            		    $x .= '<th class="sub_row_top">'.$Lang['General']['LastModified'].'</th>'."\n";
            		    $x .= '<th class="sub_row_top">'.$Lang['General']['LastModifiedBy'].'</th>'."\n";
            		    $x .= '<th class="sub_row_top">&nbsp;</th>'."\n";
        		    $x .= '</tr>'."\n";
    		    $x .= '</thead>'."\n";
                
                // Table Content
                $x .= '<tbody>'."\n";
                
    		    // loop Form
    		    foreach ((array)$ClassArr as $ClassLevelID => $ClassInfoArr)
    		    {
    		        $FormLevelArr = $FormArr[$ClassLevelID];
    		        if (!$FormLevelArr) {
    		            continue;
    		        }
    		        
    		        $YearID = trim($YearID);
    		        if ($YearID != '' && $YearID != $FormLevelArr['YearID']) {
    	                continue;
    	            }
    	            
    	            $NumOfClass = count($ClassInfoArr) + 1;
    	            $x .= '<tr>'."\n";
    	               $x .= '<td rowspan="'.$NumOfClass.'">'.$FormLevelArr['YearName'].'</td>'."\n";
    	            $x .= '</tr>'."\n";
    	            
    	            // loop Class
    	            foreach ((array)$ClassInfoArr as $thisClassID => $thisClassInfo)
    	            {
    	                $LastModifiedDate = Get_String_Display($LastModifiedInfo[$thisClassID]['DateInput']);
    	                $LastModifiedBy = Get_String_Display($LastModifiedInfo[$thisClassID]['NameField']);
    	                
    	                $x .= '<tr class="sub_row">'."\n";
    	                    $x .= '<td>'.Get_Lang_Selection($thisClassInfo['ClassTitleB5'], $thisClassInfo['ClassTitleEN']).'</td>'."\n";
        	                $x .= '<td>'.$LastModifiedDate.'</td>'."\n";
        	                $x .= '<td>'.$LastModifiedBy.'</td>'."\n";
        	                $x .= '<td>'."\n";
        	                   $x .= $this->Get_Action_Lnk('', 'Edit Other Info', "goEditOtherInfo(event, '".$thisClassID."')", 'edit_dim');
    	                   $x .= '</td>'."\n";
    	                $x .= '</tr>'."\n";
    	            }
    		    }
    		    
                $x .= '</tbody>'."\n";
		    $x .= '</table>'."\n"."\n";
		    
		    return $x;
		}
		
		function Get_Other_Info_Data_Table($UploadType, $TermID, $ClassID, $isInputLocked=false)
		{
		    global $intranet_root, $indexVar, $Lang;
		    
		    // Get Other Info Config
		    $OtherInfoConfig = $indexVar['libpowerportfolio']->getOtherInfoConfig($UploadType);
		    $NumOfConfig = sizeof($OtherInfoConfig);
		    
		    // Get Student By Class
		    $StudentArr = $indexVar['libpowerportfolio']->Get_Student_By_Class($ClassID);
		    
		    // Get Other Info Data
		    $OtherInfoData = $indexVar['libpowerportfolio']->getStudentOtherInfoData($UploadType, $TermID, $ClassID);
		    $OtherInfoData = BuildMultiKeyAssoc($OtherInfoData, array("StudentID", "ItemCode"), "Information", 1);
		    
		    $x .= '<table id="OtherInfoTable" class="common_table_list_v30 edit_table_list_v30">'."\n";
    		    $x .= '<col align="left">'."\n";
    		    $x .= '<col align="left">'."\n";
    		    $x .= '<col align="left">'."\n";
    		    for ($i = 5; $i < $NumOfConfig; $i++)
    		    {
    		        $x .= '<col align="left" width="'.ceil(70 / ($NumOfConfig - 5)).'%">'."\n";
    		    }
		        
    		    // Table Hedaer
    		    $x .= '<thead>'."\n";
        		    $x .= '<tr>'."\n";
            		    $x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</th>'."\n";
            		    $x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</th>'."\n";
            		    $x .= '<th>'.$eReportCard['Student'].'</th>'."\n";
            		    for ($i = 5; $i < $NumOfConfig; $i++)
            		    {
            		        $NameCh = str_replace(array("(", ")"), "", $OtherInfoConfig[$i]['ChineseTitle']);
            		        $NameEn = $OtherInfoConfig[$i]['EnglishTitle'];
        		            $x .= '<th>'.Get_Lang_Selection($NameCh, $NameEn).'</th>'."\n";
                        }
                    $x .= '</tr>'."\n";
                $x .= '</thead>'."\n";
		        
                // Table Content
    		    $x .= '<tbody>'."\n";
    		    
    		    // loop Student
    		    $NumOfStudent = sizeof($StudentArr);
    		    for ($i = 0; $i < $NumOfStudent; $i++)
    		    {
    		        $thisStudentID = $StudentArr[$i]['UserID'];
    		        
    		        $x .= '<tr>'."\n";
        		        $x .= '<td>'.$StudentArr[$i]['ClassName'].'</td>'."\n";
        		        $x .= '<td>'.$StudentArr[$i]['ClassNumber'].'</td>'."\n";
        		        $x .= '<td>'.$StudentArr[$i]['StudentName'].'</td>'."\n";
        		        
        		        // loop Config
        		        for ($j = 5; $j < $NumOfConfig; $j++)
        		        {
        		            $Code = $OtherInfoConfig[$j]['EnglishTitle'];
        		            $ID = 'mark['.$i.']['.($j - 5).']';
        		            $Name = 'OtherInfoArr['.$ClassID.']['.$thisStudentID.']['.$Code.']';
        		            $Value = $OtherInfoData[$thisStudentID][$Code];
        		            $inputPar['maxlength'] = $OtherInfoConfig[$j]['Length'];
        		            $isDisabled = $isInputLocked? ' disabled ' : '';
        		            if($isDisabled) {
        		                $inputPar['disabled'] = $isDisabled;
        		            }
        		            
        		            $x .= '<td>'."\n";
        		            
        		            // Input Type: Number
        		            if ($OtherInfoConfig[$j]['Type'] == "num") {
        		                $x .= $this->GET_TEXTBOX_NUMBER($ID, $Name, $Value, $OtherInfoConfig[$j]['Type'], $inputPar);
        		                $x .= $this->Get_Form_Warning_Msg("WarnDiv".$ID, $Lang['eReportCardKG']['Management']['OtherInfoWarningArr']['InputNumber'], "WarnMsgDiv");
        		            }
        		            // Input Type: String
        		            else if ($OtherInfoConfig[$j]['Type'] == "str") {
        		                $x .= $this->GET_TEXTAREA($Name, "\n".$Value, '', 5, NULL, NULL, "style='width:99%' ".$isDisabled, $OtherInfoConfig[$j]['Type']." remarkTextInput_".$j, $ID, $OtherInfoConfig[$j]['Length']);
        		            }
        		            // Input Type: Character
        		            else if ($OtherInfoConfig[$j]['Type'] == "char") {
        		                $x .= $this->GET_TEXTBOX($ID, $Name, $Value, $OtherInfoConfig[$j]['Type'], $inputPar);
        		            }
        		            // Input Type: Grade
        		            else {
        		                $x .= $this->GET_TEXTBOX($ID, $Name, $Value, $OtherInfoConfig[$j]['Type'], $inputPar);
        		                $x .= $this->Get_Form_Warning_Msg("WarnDiv".$ID, $Lang['eReportCardKG']['Management']['OtherInfoWarningArr']['InputGrade'], "WarnMsgDiv");
        		            }
        		            
        		            $x .= '</td>'."\n";
        		        }
    		        $x .= '</tr>'."\n";
    		    }
    		    $x .= '</tbody>' . "\n";
		    $x .= '</table>' . "\n";
		    
		    return $x;
		}
		
		function Edit_Award_Student_Table($ViewMode, $targetID, $filterByClassTeacher=false)
		{
		    global $indexVar, $Lang;
		    
// 		    $AwardInfoArr = $lreportcard_award->Get_Award_Info($AwardID);
// 		    $AwardType = $AwardInfoArr['AwardType'];
		    
		    ### Get the Display Student
		    $StudentIDArr = array();
		    $IncludeStudentIDArr = array();
		    if ($ViewMode == 'CLASS')
		    {
		        ### Get Form Student Info
		        $StudentInfoAssoArr = $indexVar['libpowerportfolio']->Get_Student_By_Class($targetID, "", 0, 0, 0, $ReturnAsso=1);
		        $StudentIDArr = array_keys($StudentInfoAssoArr);
		        if(empty($StudentIDArr)) {
		            $StudentIDArr = array('');
		        }
		        $IncludeStudentIDArr = $StudentIDArr;
		        
		        $NoRecordWarning = $Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['NoStudentInClass'];
		    }
		    else if ($ViewMode == 'AWARD')
		    {
		        ### Get Award Student
		        $StudentAwardInfoArr = $indexVar['libpowerportfolio']->Get_Student_Award_List('', array($targetID), $awardType='');
		        $StudentIDArr = Get_Array_By_Key($StudentAwardInfoArr, 'StudentID');
		        if(empty($StudentIDArr)) {
		            $StudentIDArr = array('');
		        }
		        
		        ### Get all KG Class
		        $allClassInfoArr = $indexVar['libpowerportfolio']->Get_All_KG_Class();
                if($filterByClassTeacher) {
                    $allClassInfoArr = $indexVar["libpowerportfolio"]->Get_Teaching_Class($_SESSION['UserID']);
                }
		        $allClassIDArr = Get_Array_By_Key($allClassInfoArr, 'YearClassID');
		        
		        ### Get Form Student Info
		        $StudentInfoAssoArr = array();
		        foreach((array)$allClassIDArr as $thisKGClassID)
		        {
		            $ClassStudentInfoAssoArr = $indexVar['libpowerportfolio']->Get_Student_By_Class(array($thisKGClassID), $StudentIDArr, 0, 0, 0, $ReturnAsso=1);
		            if(!empty($ClassStudentInfoAssoArr)) {
		                $StudentInfoAssoArr = $StudentInfoAssoArr + $ClassStudentInfoAssoArr;
    		        }
		        }
		        $StudentIDArr = array_keys($StudentInfoAssoArr);
		        $IncludeStudentIDArr = $StudentIDArr;
		        
		        $NoRecordWarning = $Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['NoAwardedStudentSettings'];
		    }
		    $numOfTotalStudent = count($StudentIDArr);
		    $numOfDisplayStudent = count($IncludeStudentIDArr);
		    
		    ### Get Display Student Award Info
		    // $StudentAwardInfoAssoArr[$StudentID][$AwardID][$SubjectID][Key] = Value;
		    $StudentAwardInfoAssoArr = array();
		    if($ViewMode == 'CLASS' && !empty($StudentIDArr))
		    {
    		    $StudentAwardInfoAssoArr = $indexVar['libpowerportfolio']->Get_Student_Award_List($StudentIDArr);
    		    $StudentAwardInfoAssoArr = BuildMultiKeyAssoc($StudentAwardInfoAssoArr, array('StudentID', 'AwardID'));
		    }
		    else if($ViewMode == 'AWARD')
		    {
		        $StudentAwardInfoAssoArr = BuildMultiKeyAssoc($StudentAwardInfoArr, array('StudentID', 'AwardID'));
		    }
		    
		    $x = '';
		    $x .= '<table id="AwardStudentTable" class="common_table_list_v30 edit_table_list_v30">' . "\n";
		    $x .= '<col style="width:5%;">' . "\n";
		    $x .= '<col style="width:10%;">' . "\n";
		    $x .= '<col style="width:10%;">' . "\n";
		    $x .= '<col style="width:25%;">' . "\n";
		    if ($ViewMode == 'CLASS') {
		        $x .= '<col style="width:50%;">' . "\n";
		    } else if ($ViewMode == 'AWARD') {
		        $x .= '<col style="width:3%;">' . "\n";
		        $x .= '<col style="width:3%;">' . "\n";
		    }
		    
		    $x .= '<thead>' . "\n";
		    $x .= '<tr>' . "\n";
// 		    if ($ViewMode == 'AWARD') {
// 		        $x .= '<th>' . $eReportCard['GeneralArr']['Ranking'] . '</th>' . "\n";
// 		    } else if ($ViewMode == 'CLASS') {
// 		        $x .= '<th>#</th>' . "\n";
// 		    }
		    $x .= '<th>#</th>' . "\n";
		    $x .= '<th>' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</th>' . "\n";
		    $x .= '<th>' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</th>' . "\n";
		    $x .= '<th>' . $Lang['SysMgr']['FormClassMapping']['StudentName'] . '</th>' . "\n";
		    if ($ViewMode == 'CLASS') {
		        $x .= '<th>' . $Lang['eReportCardKG']['Management']['AwardGeneration']['Award'] . '</th>' . "\n";
		    } else if ($ViewMode == 'AWARD') {
		        $x .= '<th>&nbsp;</th>' . "\n";
		    }
		    $x .= '</tr>' . "\n";
		    $x .= '</thead>' . "\n";
		    
		    $x .= '<tbody>' . "\n";
		    if ($numOfDisplayStudent == 0)
		    {
		        $x .= '<tr><td colspan="100%" style="text-align:center;">' . $NoRecordWarning . '</td></tr>' . "\n";
		    }
		    else
		    {
		        $StudentDisplayCount = 0;
		        for ($i=0; $i<$numOfTotalStudent; $i++)
		        {
		            $thisStudentID = $StudentIDArr[$i];
		            if (!in_array($thisStudentID, $IncludeStudentIDArr)) {
		                continue;
		            }
		            
		            $thisStudentInfoArr = $StudentInfoAssoArr[$thisStudentID];
		            $thisClassName = Get_Lang_Selection($thisStudentInfoArr['ClassTitleCh'], $thisStudentInfoArr['ClassTitleEn']);
		            $thisClassNumber = $thisStudentInfoArr['ClassNumber'];
		            $thisStudentName = $thisStudentInfoArr['StudentName'];
		            
// 		            if ($ViewMode == 'CLASS') {
// 		                $thisNumberDisplay = ++ $StudentDisplayCount;
// 		            } else if ($ViewMode == 'AWARD') {
// 		                $thisRecordID = $StudentAwardInfoAssoArr[$thisStudentID][$AwardID]['RecordID'];
// 		                $thisNumberDisplay = $StudentAwardInfoAssoArr[$thisStudentID][$AwardID]['AwardRank'];
// 		            }
		            $thisNumberDisplay = ++ $StudentDisplayCount;
		            
		            $x .= '<tr id="' . $thisRecordID . '">' . "\n";
		            $x .= '<td><span class="RankingSpan">' . $thisNumberDisplay . '</span></td>' . "\n";
		            $x .= '<td>' . $thisClassName . '</td>' . "\n";
		            $x .= '<td>' . $thisClassNumber . '</td>' . "\n";
		            $x .= '<td>' . $thisStudentName . '</td>' . "\n";
		            
		            if ($ViewMode == 'CLASS')
		            {
		                $x .= '<td>' . "\n";
                        $x .= '<div id="StudentAwardDiv_' . $thisStudentID . '">' . "\n";
		                foreach ((array) $StudentAwardInfoAssoArr[$thisStudentID] as $thisAwardID => $thisAwardInfoArr)
		                {
		                    $thisRecordID = $thisAwardInfoArr['RecordID'];
		                    $thisAwardName = Get_Lang_Selection($thisAwardInfoArr['AwardNameCh'], $thisAwardInfoArr['AwardNameEn']);
		                    $thisDeleteButton = $this->GET_LNK_DELETE("javascript:void(0);", $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['DeleteAward'], "js_Delete_Award('".$thisStudentID."', '".$thisRecordID."')", $ParClass = "", $WithSpan = 1);
		                    
	                        $x .= '<div id="GeneratedAwardDiv_' . $thisRecordID . '">' . "\n";
		                        $x .= '<span style="float:left;">' . $thisAwardName . '&nbsp;&nbsp;</span>' . $thisDeleteButton . "\n";
		                        $x .= '<br style="clear:both;" />' . "\n";
	                        $x .= '</div>' . "\n";
		                }
                        
	                    $x .= '<span class="table_row_tool row_content_tool">' . "\n";
	                        $x .= $this->Get_Thickbox_Link(450, 750, $ExtraClass = 'add_dim', $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AddAward'], "js_Show_Add_Student_Award_Layer('".$thisStudentID."');", $InlineID = "FakeLayer", $Content = "", $LinkID = '');
	                    $x .= '</span>' . "\n";
		                
		                $x .= '</div>' . "\n";
		                $x .= '</td>' . "\n";
		            }
		            else if ($ViewMode == 'AWARD')
		            {
		                $thisRecordID = $StudentAwardInfoAssoArr[$thisStudentID][$targetID]['RecordID'];
		                
		                // Remove Icon
		                $x .= '<td align="right">' . "\n";
		                $x .= $this->GET_LNK_DELETE("javascript:void(0);", $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['DeleteAward'], "js_Delete_Award('".$thisStudentID."', '".$thisRecordID."')", $ParClass = "", $WithSpan = 1);
		                $x .= '</td>' . "\n";
		            }
		            $x .= '</tr>' . "\n";
		        }
		    }
		    
		    if ($ViewMode == 'AWARD')
		    {
		        $x .= '<tr>' . "\n";
		        $x .= '<td colspan="100%">' . "\n";
        	        $x .= '<span class="table_row_tool row_content_tool" style="float:right;">' . "\n";
    	           $x .= $this->Get_Thickbox_Link(450, 750, $ExtraClass = 'add', $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AddStudent'], "js_Show_Add_Award_Student_Layer('".$targetID."');", $InlineID = "FakeLayer", $Content = "", $LinkID = '');
        	        $x .= '</span>' . "\n";
		        $x .= '</td>' . "\n";
		        $x .= '</tr>' . "\n";
		    }
		    
		    $x .= '</tbody>' . "\n";
		    $x .= '</table>' . "\n";
		    
		    $x .= '<input type="hidden" id="AwardedStudentIDList" name="AwardedStudentIDList" value="' . implode(',', (array)$StudentIDArr) . '" />' . "\n";
		    
		    return $x;
		}
		
		public function Get_Add_Student_Award_Layer_UI($StudentID, $ClassID)
		{
		    $x = "";
		    $x .= '<div id="debugArea"></div>';
		    $x .= '<div class="edit_pop_board" style="height:410px;">';
    		    $x .= $this->Get_Thickbox_Return_Message_Layer();
    		    $x .= '<div id="LevelSettingLayer" class="edit_pop_board_write" style="height:370px;">';
        		    $x .= $this->Get_Add_Student_Award_Layer_Table($StudentID, $ClassID);
    		    $x .= '</div>';
		    $x .= '</div>';
		    
		    return $x;
		}
		
		private function Get_Add_Student_Award_Layer_Table($StudentID, $ClassID)
		{
		    global $indexVar, $Lang;
		    
		    ### Get Display Student Info
		    $StudentInfoArr = $indexVar['libpowerportfolio']->Get_Student_By_Class($ClassID, array($StudentID));
		    $ClassName = Get_Lang_Selection($StudentInfoArr[0]['ClassNameCh'], $StudentInfoArr[0]['ClassName']);
		    $ClassNumber = $StudentInfoArr[0]['ClassNumber'];
		    $StudentName = $StudentInfoArr[0]['ClassName'];
		    
		    ### Get Student Award Info
		    // $StudentAwardInfoAssoArr[$StudentID][$AwardID][$SubjectID][Key] = Value;
		    $SubjectAwardArr = $indexVar['libpowerportfolio']->Get_Student_Award_List(array($StudentID));
		    $SubjectAwardArr = BuildMultiKeyAssoc($SubjectAwardArr, array('StudentID', 'AwardID'));
		    
		    $ExcludeAwardIDArr = array();
		    foreach((array)$SubjectAwardArr[$StudentID] as $thisStudentAward) {
		        $thisAwardID = $thisStudentAward['AwardID'];
		        $thisAwardType = $thisStudentAward['AwardType'];
		        
// 		        if ($thisAwardType == 'OVERALL') {
// 		            $ExcludeAwardIDArr[] = $thisAwardID;
// 		        }
	            $ExcludeAwardIDArr[] = $thisAwardID;
		    }
		    
		    $x = '';
		    $x .= '<table class="form_table_v30">' . "\n";
		    // Class Name
		    $x .= '<tr>' . "\n";
		    $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</td>' . "\n";
		    $x .= '<td>' . $ClassName . '</td>' . "\n";
		    $x .= '</tr>' . "\n";
		    
		    // Class No.
		    $x .= '<tr>' . "\n";
		    $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</td>' . "\n";
		    $x .= '<td>' . $ClassNumber . '</td>' . "\n";
		    $x .= '</tr>' . "\n";
		    
		    // Student Name
		    $x .= '<tr>' . "\n";
		    $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['StudentName'] . '</td>' . "\n";
		    $x .= '<td>' . $StudentName . '</td>' . "\n";
		    $x .= '</tr>' . "\n";
		    
		    // Award
		    $x .= '<tr>' . "\n";
		    $x .= '<td class="field_title">' . $Lang['eReportCardKG']['Management']['AwardGeneration']['StudentAward'] . '</td>' . "\n";
		    $x .= '<td>' . "\n";
		    $x .= $this->Get_Award_Selection('AwardID', $SelectedValue = '', $ExcludeAwardIDArr, $TargetAwardType = '', $OnChange = 'js_Changed_Award_Selection();', $WithAwardType = 1);
		    $x .= $this->Get_Form_Warning_Msg('AwardEmptyWarningDiv', $Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['NoAward'], $Class = 'WarningDiv');
		    $x .= '</td>' . "\n";
		    $x .= '</tr>' . "\n";
		    $x .= '</table>' . "\n";
		    
		    $x .= '<br />';
		    $x .= '<div class="edit_bottom_v30">';
		    $x .= $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Add_Student_Award();") . "\n";
		    $x .= $this->GET_ACTION_BTN($Lang['Btn']['Submit&AddMore'], "button", "js_Add_Student_Award(1);") . "\n";
		    $x .= $this->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();") . "\n";
		    $x .= '</div>';
		    
		    $x .= '<input type="hidden" id="StudentID" name="StudentID" value="' . $StudentID . '">' . "\n";
		    
		    return $x;
		}
		
        function Get_Award_Selection($ID_Name, $SelectedValue, $ExcludeAwardIDArr = '', $TargetAwardType = '', $OnChange = '', $WithAwardType = 1, $isMultiple = 0)
		{
		    global $indexVar, $PowerPortfolioConfig;
		    
// 		    global $lreportcard_award, $PATH_WRT_ROOT;
// 		    if ($lreportcard_award == null) {
// 		        include_once ($PATH_WRT_ROOT . 'includes/libreportcard2008_award.php');
// 		        $lreportcard_award = new libreportcard_award();
// 		    }
		    
		    $SelectArr = array();
		    $AwardInfoArr = $indexVar['libpowerportfolio']->Get_KG_Award_List($awardID='', $awardType=$PowerPortfolioConfig['awardType']['Input'], $ExcludeAwardIDArr);//debug_pr($AwardInfoArr);die();
		    foreach ((array) $AwardInfoArr as $thisAwardInfoArr)
		    {
		        $thisAwardValid = true;
		        $thisAwardID = $thisAwardInfoArr['AwardID'];
		        
		        if ($ExcludeAwardIDArr != '' && in_array($thisAwardID, (array) $ExcludeAwardIDArr)) {
		            $thisAwardValid = false;
		        }
		        
// 		        if ($ShowHaveRankingAwardOnly) {
// 		            foreach ((array) $thisAwardInfoArr['CriteriaInfo'] as $thisCriteriaID => $thisCriteriaInfoArr) {
// 		                if ($thisCriteriaInfoArr['CriteriaType'] == 'PERSONAL_CHAR') {
// 		                    // Awards which has Personal Char as Criteria has no Ranking and therefore exclude from the Award Selection for some cases
// 		                    $thisAwardValid = false;
// 		                    break;
// 		                }
// 		            }
// 		        }
		        
		        $thisAwardType = $thisAwardInfoArr['AwardType'];
		        if ($TargetAwardType != '' && $thisAwardType != $TargetAwardType) {
		            $thisAwardValid = false;
		        }
		        
		        if ($thisAwardValid) {
		            $SelectArr[$thisAwardID] = Get_Lang_Selection($thisAwardInfoArr['AwardNameCh'], $thisAwardInfoArr['AwardNameEn']);
		        }
		    }
		    
		    if ($OnChange != '') {
		        $onchange = ' onchange="' . $OnChange . '" ';
		    }
		    
		    if ($isMultiple) {
		        $multipleTag = ' multiple size="10" ';
		    }
		    
		    $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $multipleTag;
		    return getSelectByAssoArray($SelectArr, $selectionTags, $SelectedValue, $all = 0, $noFirst = 1);
		}
		
		function Get_Add_Award_Student_Layer_UI($AwardID, $ClassID)
		{
		    $x = "";
		    $x .= '<div id="debugArea"></div>';
		    $x .= '<div class="edit_pop_board" style="height:410px;">';
    		    $x .= $this->Get_Thickbox_Return_Message_Layer();
    		    $x .= '<div id="LevelSettingLayer" class="edit_pop_board_write" style="height:370px;">';
                    $x .= $this->Get_Add_Award_Student_Layer_Table($AwardID, $ClassID);
    		    $x .= '</div>';
		    $x .= '</div>';
		    
		    return $x;
		}
		
		public function Get_Add_Award_Student_Layer_Table($AwardID, $ClassID)
		{
		    global $indexVar, $Lang;
		    
// 		    $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
// 		    $ClassLevelID = $ReportInfoArr['ClassLevelID'];
		    
		    ### Get Student Award Info
		    $AwardInfoArr = $indexVar['libpowerportfolio']->Get_KG_Award_List($AwardID);
		    $AwardName = Get_Lang_Selection($AwardInfoArr[0]['AwardNameCh'], $AwardInfoArr[0]['AwardNameEn']);
		    $AwardType = $AwardInfoArr[0]['AwardType'];
		    
		    $x = '';
		    $x .= '<table class="form_table_v30">' . "\n";
		    // Award
		    $x .= '<tr>' . "\n";
		    $x .= '<td class="field_title">' . $Lang['eReportCardKG']['Management']['AwardGeneration']['StudentAward'] . '</td>' . "\n";
		    $x .= '<td>' . $AwardName . '</td>' . "\n";
		    $x .= '</tr>' . "\n";
		    
		    // Class
		    $x .= '<tr>' . "\n";
		    $x .= '<td class="field_title">' . $Lang['General']['Class'] . '</td>' . "\n";
		    $x .= '<td>' . "\n";
		    $x .= $this->Get_Class_Selection($selectedClass=$ClassID, $onChange='js_Changed_Class_Selection(this.value);', $withYearOptGroup=false, $noFirstTitle=false, $selectYearID='', $selectionName='ClassIDArr');
		    $x .= $this->Get_Form_Warning_Msg('ClassEmptyWarningDiv', $Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['NoClass'], $Class = 'WarningDiv');
		    $x .= '</td>' . "\n";
		    $x .= '</tr>' . "\n";
		    
		    // Student
		    $x .= '<tr id="StudentSelTr" style="display:none;">' . "\n";
		    $x .= '<td class="field_title">' . $Lang['SysMgr']['SubjectClassMapping']['ClassStudent'] . '</td>' . "\n";
		    $x .= '<td>' . "\n";
		    $x .= '<div id="StudentSelDiv"></div>' . "\n";
		    $x .= $this->Get_Form_Warning_Msg('StudentEmptyWarningDiv', $Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['AtLeastOneStudent'], $Class = 'WarningDiv');
		    $x .= '</td>' . "\n";
		    $x .= '</tr>' . "\n";
		    $x .= '</table>' . "\n";
		    
		    $x .= '<br />';
		    $x .= '<div class="edit_bottom_v30">';
		    $x .= $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Add_Award_Student();") . "\n";
		    $x .= $this->GET_ACTION_BTN($Lang['Btn']['Submit&AddMore'], "button", "js_Add_Award_Student(1);") . "\n";
		    $x .= $this->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();") . "\n";
		    $x .= '</div>';
		    
		    $x .= '<input type="hidden" id="AwardID" name="AwardID" value="' . $AwardID . '">' . "\n";
		    
		    return $x;
		}
		
		function Get_Report_Page_Header($yearSemInfo, $isAwardPage=false)
		{
		    $thisYear = $yearSemInfo["YearName"];
		    $nextYear = $thisYear + 1;
		    $yearDisplay = $thisYear.' - '.$nextYear;
		    
			$x = "	<div>
					<table class='tbl_pageHead'>
						<tbody>
							<tr class='control'>
								<td style='width: 35.3mm;' />
								<td style='width: 18mm;' />
								<td style='width: 123.9mm;' />
								<td style='width: 23.2mm;' />
								<td style='width: 9.6mm;' />
							</tr>
							<tr>
								<td class='year'><span>".$yearDisplay."</span></td>";
			if($isAwardPage)
			{
			    $x .= "	        <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td> ";
			}
			else
			{
			    $x .= "	        <td class='semester'><span>".$yearSemInfo["SemesterName"]."</span></td>
                                <td>&nbsp;</td>
								<td class='pagination' style=''><span>P.{PAGENO}/{nbpg}</span></td>";
			}
			$x .= "	            <td>&nbsp;</td>
							</tr>
						</tbody>
					</table>
					</div>";
			return $x;
		}
		
		function Get_Report_Cover_Page($studentInfo, $yearSemInfo, $otherInfoData)
		{
		    global $indexVar;
		    
			$thisYear = $yearSemInfo["YearName"];
		    $nextYear = $thisYear + 1;
		    $yearDisplay = $thisYear.' - '.$nextYear;
			
			$x = "	<div class='cover'>
						<table style='width: 210mm'>
							<tr>
								<td colspan='3' style='height: 25mm;'>&nbsp;</td>
							</tr>
							<tr>
								<td class='school_logo' colspan='3' style='height: 65mm; text-align: center;'><img src='asset/images/school_logo.png' alt='school_logo' style='height: 65mm; width: auto;' /></td>
							</tr>
						</table>
						
						<table class='tbl_stdInfo'>
			    			<tbody>
								<tr class='control'>
									<td style='width: 99.5mm' />
									<td style='width: 45.5mm;' />
									<td style='width: 65mm;' />
								</tr>
								<tr>
									<td colspan='3' style='height: 22mm;'>&nbsp;</td>
								</tr>
								<tr>
									<td class='year' colspan='3'><span>".$yearDisplay."</span></td>
								</tr>
								<tr>
									<td class='semester' colspan='3'><span>".$yearSemInfo["SemesterName"]."</span></td>
								</tr>
								<tr>
									<td style='height: 1.5mm;' colspan='3'>&nbsp;</td>
								</tr>
								<tr>
									<!--<td class='std_photo' rowspan='4' style='width: 53.5mm'><img class='stud_photo' src='asset/images/photo.png' alt='photo' /></td>-->
                                    <td class='std_photo' rowspan='4' style='width: 53.5mm' align='center'>".$indexVar['libpowerportfolio']->Get_Student_Display_Photo($studentInfo, "200")."</td>
									<td class='std_name' colspan='2' style='height: 9.5mm;'><span>".($studentInfo["StudentNameCh"]? $studentInfo["StudentNameCh"] : '&nbsp;')."</span></td>
								</tr>
								<tr>
									<td class='std_class'><span>".$studentInfo["ClassTitleCh"]."</span></td>
									<td class='std_classNo'><span>".$studentInfo["ClassNumber"]."</span></td>
								</tr>
								<tr>
									<td class='std_height'><span>".$otherInfoData['Height'].(($otherInfoData['Height'] != '' && strlen($otherInfoData['Height']) > 3)? "&nbsp;&nbsp;&nbsp;" : "")."</span></td>
									<td class='std_weight'><span>".$otherInfoData['Weight'].(($otherInfoData['Weight'] != '' && strlen($otherInfoData['Weight']) > 3)? "&nbsp;&nbsp;&nbsp;" : "")."</span></td>
								</tr>
								<tr>
									<td class='std_abs'><span>".$otherInfoData['Absent Lessons']."</span></td>
									<td class='std_late'><span>".$otherInfoData['Time Lates']."</span></td>
								</tr>
								<tr>
									<td colspan='3' style='height: 76.8mm'></td>
								</tr>
							</tbody>
						</table>
					</div>";
			return $x;
		}
		
		function Get_Report_Ability_Index_Result($studentInfo, $ResultSummaryImage, $TopicResultScoreAry, $yearSemInfo)
		{
		    global $indexVar;
		
			$x = "	<div class='inner1' style='top: 891mm;'>
						<div class='filler' style='width:100%; height: 27.6mm'></div>
						<table class='tbl_myReport'>
							<tbody>
								<tr>
									<td class='myReportTitle'>".$studentInfo["StudentNameCh"]."同學的學習成績</td>
								</tr>
								<tr>
									<td class='myReport'>".$ResultSummaryImage."</td>
								</tr>
							</tbody>
						</table>";
			
			$page1Line = 23;
			$page2Line = 45;
			$pageLineTotal = $page1Line;
			
			// Get Ability Target Macau Category (Term based)
			$abilityTargetMOCat = $indexVar["libpowerportfolio"]->getAbilityRemarkTargetMOCat($AbilityID='', $yearSemInfo['TermID']);
			$abilityTargetMOCat = BuildMultiKeyAssoc($abilityTargetMOCat, "Code", "TargetMOCat", 1);
			
			$topicMOType = $indexVar["libpowerportfolio"]->Get_Macau_Category($returnAssocArr=true);
			if(empty($topicMOType)) {
                $topicMOType = array("A" => "健康與體育", "B" => "語言", "C" => "個人、社交與人文", "D" => "數學與科學", "E" => "藝術");
			}
			foreach($topicMOType as $thisTopicMOType => $thisTopicMOTypeName)
			{
			    $thisSectionLine = 5;
			    $thisTopicTWCommentAry = $TopicResultScoreAry[$thisTopicMOType];
			    if(!empty($thisTopicTWCommentAry))
			    {
			        // hide not matched score comment
			        foreach((array)$thisTopicTWCommentAry as $thisTopicName => $thisTWTopicScoreComment) {
			            if(isset($abilityTargetMOCat[$thisTopicName]) && $abilityTargetMOCat[$thisTopicName] != -1 && $abilityTargetMOCat[$thisTopicName] != $thisTopicMOType) {
			                unset($thisTopicTWCommentAry[$thisTopicName]);
			            }
			        }
			    }
			    
			    $SectionIndex = 0;
			    $CommentSectionAry = array();
				if(!empty($thisTopicTWCommentAry))
				{
					foreach((array)$thisTopicTWCommentAry as $thisTWTopicScoreComment)
					{
					    if($thisTWTopicScoreComment["Remarks"] == '') {
					        continue;
					    }
					    
					    $thisCommentLen = mb_strlen($thisTWTopicScoreComment["Remarks"]);
						$thisSectionLine++;
                        
						//$thisCommentLen -= 37;
						$thisCommentLen -= 43;
						if($thisCommentLen > 0) {
						    $thisSectionLine += ceil($thisCommentLen / 43);
						}
						
						// Page max: 38 lines
						$SectionIndex = floor(($thisSectionLine - 5) / 39);
						$CommentSectionAry[$SectionIndex][] = $thisTWTopicScoreComment["Remarks"];
					}
				}
				else
				{
				    $thisSectionLine += 2;
				    $CommentSectionAry[$SectionIndex][] = '';
				}
				$thisCommentLine += $thisSectionLine;
				
				$isBeakPageFirstTable = false;
				if($thisCommentLine > $pageLineTotal)
				{
					$x .= "	</div>
							<pagebreak />
							
							<div class='inner1' style='top: 891mm;'>
								<div class='filler' style='width:100%; height: 27.6mm'></div>
								<table class='tbl_myReport_small'>
    								<tbody>
    								    <tr><td class='myReportTitle'>".$studentInfo["StudentNameCh"]."同學的學習成績</td></tr>
    								</tbody>
								</table>";
					
					$pageLineTotal = $page2Line;
					//$thisCommentLine = $thisSectionLine + 5;
					$thisCommentLine = $thisSectionLine + 2;
					$isBeakPageFirstTable = true;
				}
				
				foreach((array)$CommentSectionAry as $thisIndex => $thisCommentSectionAry)
				{
				$tableTopPadding = $isBeakPageFirstTable? '0mm' : '5mm' ;
				$x .= "		<table class='tbl_reportDetails' autosize='1' style='padding-top: ".$tableTopPadding."'>
								<tbody>
									<tr class='control'>
										<td style='width: 10mm'>&nbsp;</td>
										<td style='width: 169mm'>&nbsp;</td>
									</tr>
									<tr>
										<td class='reportDetails_img'><img src='asset/images/title_bullet.png' style='height: 8.5mm; width: 8.5mm;'></td>
										<td class='reportDetails_title'><span>".$thisTopicMOTypeName."</span></td>
									</tr>
									<tr>
										<td class='reportDetails_detail' colspan='2' style='height: 12.5mm;'><p>";
				foreach((array)$thisCommentSectionAry as $thisComment)
				{
				    if($thisComment != '') {
                        $x .= 				  "- ".$thisComment."<br/>";
				    }
				}
				$x .= "					</p></td>
									</tr>
									<tr class='reportDetails_bg'><td colspan='2'></td></tr>
								</tbody>
							</table>";
				
				// Page break for remaining comment remarks
				if($thisIndex < $SectionIndex)
				{
				    $x .= "	</div>
							<pagebreak />
				            
							<div class='inner1' style='top: 891mm;'>
								<div class='filler' style='width:100%; height: 27.6mm'></div>
								<table class='tbl_myReport_small'>
    								<tbody>
    								    <tr><td class='myReportTitle'>".$studentInfo["StudentNameCh"]."同學的學習成績</td></tr>
    								</tbody>
								</table>";
				    
				    // already display 38 lines (max)
				    $thisCommentLine = $thisSectionLine - 38;
				    $isBeakPageFirstTable = true;
				}
				}
			}
			$x .= "	</div>
					<pagebreak />";
			
			return $x;
		}
		
		function Get_Report_Indicator_Score_Result($studentInfo, $topicAry, $studentTopicScoreAry)
		{
			global $indexVar, $Lang;
			global $SemesterNumber;
			
//			$totalSize = 49.95;
//			$normalSize = 1.75;
//			$extraSize = 1.05;
//			$headerSize = 5.8;
//			$breakExtraSize = 2.85;
//			$currentSize = 0;
// 			$totalSize = 30.25;
// 			$normalSize = 1;
// 			$extraSize = 0.622;
// 			$headerSize = 5;
// 			$footerLimit = 30.25 - 1.622;
			$totalSize = 30.835;
			$normalSize = 1;
			$extraSize = 0.568;
			$topicSize = 1.324;
			$headerSize = 4.867;
			$footerLimit = 30.835- 1.568;
			$finalPageLimit = $footerLimit - 3;
			$currentSize = 0;

            $activeAcademicYearId = $indexVar["libpowerportfolio"]->Get_Active_AcademicYearID();
            //$curAcademicYearId = Get_Current_Academic_Year_ID();
            $specificAcademicYearId = 20;
			
			$pageTableHeader = "<div class='filler' style='width:100%; height: 27.6mm'></div>
        						<table class='tbl_myReport_small'>
        							<tbody>
        								<tr><td class='myReportTitle'>".$studentInfo["StudentNameCh"]."同學的語文能力</td></tr>
        							</tbody>
        						</table>";
			
			$x = "	<div class='inner' style='top: 891mm;'>
                        ".$pageTableHeader."
						<div class='language_bg'>";
			$langTopicsAry = $topicAry[$Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category1"]];
			
			$langTopicsCount = count((array)$langTopicsAry);
			if($langTopicsCount > 0)
			{
			    $isFirstLangTable = true;
				foreach($langTopicsAry as $thisTopicCatType => $thisLangTopicsAry)
				{
				    if($activeAcademicYearId && $specificAcademicYearId && $activeAcademicYearId == $specificAcademicYearId && $SemesterNumber == 3)
                    {
                        if($thisTopicCatType == "普通話") {
                            continue;
                        }
                    }

					$thisLangTopicsCount = count((array)$thisLangTopicsAry);
					if($thisLangTopicsCount > 0)
					{
						$thisLangType = $thisTopicCatType == "英文" ? "title_eng" : "title_pth";
						
// 						$currentSize += $headerSize;
// 						if($currentSize > $headerSize)
// 						{
// 							$isNeedLineBreak = $isNeedLineBreak || $currentSize >= $footerLimit;
// 							if($isNeedLineBreak)
                            if(!$isFirstLangTable)
							{
								$x .= "				</tbody>
												</table>
										</div></div>
										
										<pagebreak />
										<div class='inner' style='top: 891mm;'>
											<!--<div class='filler' style='width:100%; height: 40.9mm'></div>-->
                    						".$pageTableHeader."
											<div class='language_bg'>";
								
// 								$currentSize = $headerSize;
// 								$isNeedLineBreak = false;
							}
							$isFirstLangTable = false;
							$currentSize = $headerSize;
// 						}
						
						$x .= "	<table class='tbl_language_top'>
									<tbody>
										<tr class='control'>
											<td style='width: 128mm;'>&nbsp;</td>
											<td style='width: 50mm;'>&nbsp;</td>
										</tr>
										<tr>
											<td class='language_subtitle'><img class='language_subtitle' src='asset/images/".$thisLangType.".png' alt='".$thisLangType."' /></td>
											<td class='rate_index'><img class='rate_index' src='asset/images/index.jpg' alt='index' /></td>
										</tr>
									</tbody>
								</table>
								<table class='tbl_language_body'>
									<tbody>
										<tr class='control'>
											<td style='width: 10mm;'>&nbsp;</td>
											<td style='width: 108mm;'>&nbsp;</td>
											<td style='width: 10mm;'>&nbsp;</td>
											<td style='width: 10mm;'>&nbsp;</td>
											<td style='width: 10mm;'>&nbsp;</td>
											<td style='width: 10mm;'>&nbsp;</td>
											<td style='width: 10mm;'>&nbsp;</td>
										</tr>";
						
						//$topicCount = 1;
						$topicCount = 0;
						$topicCountDisplay = 1;
						$previousTopicPrefix = '';
						$thisTopicPrefixDisplayLen = 0;
						foreach((array)$thisLangTopicsAry as $thisTopicID => $thisTopicInfo)
						{
						    $thisTopicNameCh = $thisTopicInfo["NameCh"];
						    $thisTopicNameCh = str_replace('（ ', '（', $thisTopicNameCh);
						    $thisTopicNameCh = str_replace(' ）', '）', $thisTopicNameCh);
						    $thisTopicPieces = explode('：', $thisTopicNameCh);
						    $thisTopicPrefix = trim($thisTopicPieces[0]);
						    if($thisTopicPrefix != $previousTopicPrefix)
						    {
						        $thisTopicPrefixDisplayLen = 0;
    						    if ($thisTopicPrefix != $thisTopicNameCh && mb_strlen($thisTopicPrefix) <= 4)
    						    {
    						        $x .= "	<tr class='item'>
    											<td class='itemName' colspan='7'><span class='title'>".$thisTopicPrefix."</span></td>
    										</tr>";
    						        //$topicCount++;
    						        $topicCountDisplay = 1;
    						        //$currentSize += $normalSize;
    						        //$currentSize += ($extraSize / 2);
    						        $currentSize += $topicSize;
    						        $previousTopicPrefix = $thisTopicPrefix;
    						        
    						        # Handle prefix display length
    						        $prefixBypeLen = strlen($thisTopicPrefix);		        // byte count
    						        $prefixWordLen = mb_strlen($thisTopicPrefix);           // word count
    						        $prefixLenDiff = (($prefixWordLen * 3) - $prefixBypeLen) / 2;
    						        
    						        $prefixDisplayLen = 0;
    						        if($prefixWordLen == $prefixBypeLen) {					// English only
    						            $prefixDisplayLen = $prefixBypeLen;
    						        }
    						        else if (($prefixWordLen * 3) == $prefixBypeLen) {		// Chinese only
    						            $prefixDisplayLen = $prefixWordLen * 1.84;
    						        }
    						        else if (($prefixWordLen * 3) > $prefixBypeLen) {			// English + Chinese
    						            $prefixDisplayLen = $prefixLenDiff + (($prefixWordLen - $prefixLenDiff) * 1.84);
    						        }
    						        $thisTopicPrefixDisplayLen = $prefixDisplayLen;
    						    }
						    }
						    
							$thisTopicScore = $studentTopicScoreAry[$thisTopicID]["Score"];
							$isScore1 = $thisTopicScore == "1" ? " chosen" : "";
							$isScore2 = $thisTopicScore == "2" ? " chosen" : "";
							$isScore3 = $thisTopicScore == "3" ? " chosen" : "";
							$isScore4 = $thisTopicScore == "4" ? " chosen" : "";
							$isScore5 = $thisTopicScore == "5" ? " chosen" : "";
							
							if ($thisTopicPrefix != $thisTopicNameCh && mb_strlen($thisTopicPrefix) <= 4) {
							    $thisTopicDisplay = str_replace($thisTopicPrefix.'：', '', $thisTopicNameCh);
							    if($thisTopicPrefixDisplayLen > 0) {
							        $thisTopicInfo["DisplayLen"] = $thisTopicInfo["DisplayLen"] - $thisTopicPrefixDisplayLen;
							    }
							}
							else {
							    $thisTopicDisplay = $thisTopicNameCh;
							}
							
							$x .= "		<tr class='item'>
											<td class='num'><span>".$topicCountDisplay."</span></td>
											<td class='itemName'><span>".$thisTopicDisplay."</span></td>
											<td class='indexNo ".$isScore1."'><span class='".$isScore1."'>1</span></td>
											<td class='indexNo ".$isScore2."'><span class='".$isScore2."'>2</span></td>
											<td class='indexNo ".$isScore3."'><span class='".$isScore3."'>3</span></td>
											<td class='indexNo ".$isScore4."'><span class='".$isScore4."'>4</span></td>
											<td class='indexNo ".$isScore5."'><span class='".$isScore5."'>5</span></td>
										</tr>";
							$topicCount++;
							$topicCountDisplay++;
							
							$currentSize += $normalSize;
							$thisTopicDisplaySize = $thisTopicInfo["DisplayLen"];
							if($thisTopicDisplaySize > 53) {
								$currentSize += $extraSize;
							}
							
							$isNeedLineBreak = $currentSize >= $footerLimit;
							if($isNeedLineBreak && $thisLangTopicsCount > $topicCount)
							{
								$x .= "				</tbody>
												</table>
										</div></div>
										
										<pagebreak />
										<div class='inner' style='top: 891mm;'>
                                            ".$pageTableHeader."
											<div class='language_bg'>
												<table class='tbl_language_top'>
													<tbody>
														<tr class='control'>
															<td style='width: 128mm;'>&nbsp;</td>
															<td style='width: 50mm;'>&nbsp;</td>
														</tr>
														<tr>
															<td class='language_subtitle'>&nbsp;</td>
															<td class='rate_index'><img class='rate_index' src='asset/images/index.jpg' alt='index' /></td>
														</tr>
													</tbody>
												</table>
												<table class='tbl_language_body'>
													<tbody>
														<tr class='control'>
															<td style='width: 10mm;'>&nbsp;</td>
															<td style='width: 108mm;'>&nbsp;</td>
															<td style='width: 10mm;'>&nbsp;</td>
															<td style='width: 10mm;'>&nbsp;</td>
															<td style='width: 10mm;'>&nbsp;</td>
															<td style='width: 10mm;'>&nbsp;</td>
															<td style='width: 10mm;'>&nbsp;</td>
														</tr>";
								
								$currentSize = $headerSize;
// 								$isNeedLineBreak = false;
							}
						}	
						$x .= "		</tbody>
								</table>";
					}
				}
			}
			$isNeedLineBreak = false;
			
			$x .= "	</div></div>";
			
			$pageTableHeader = "<div class='filler' style='width:100%; height: 27.6mm'></div>
        						<table class='tbl_myReport_small'>
        							<tbody>
        								<tr><td class='myReportTitle'>".$studentInfo["StudentNameCh"]."同學的行為指標</td></tr>
        							</tbody>
        						</table>";
			$x .= "	<pagebreak />
					<div class='inner' style='top: 891mm;'>
                        ".$pageTableHeader."
						<div class='learningIndex_bg'>";
			
			$indicatTopicsAry = $topicAry[$Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category2"]];
			$indicatTopicsCount = count((array)$indicatTopicsAry);
			if($indicatTopicsCount > 0)
			{
				$currentSize = 0;
				$indicatorCount = 0;
				foreach($indicatTopicsAry as $thisTopicCatType => $thisIndicatTopicsAry)
				{
					$thisIndicatTopicsCount = count((array)$thisIndicatTopicsAry);
					if($thisIndicatTopicsCount > 0)
					{
						$thisIndicatType = $thisTopicCatType == "英文" ? "title_eng" : "title_pth";
						
						$currentSize += $headerSize;
						if($currentSize > $headerSize)
						{
							$isNeedLineBreak = $isNeedLineBreak || $currentSize >= $footerLimit;
							if($isNeedLineBreak)
							{
								$x .= "				</tbody>
												</table>
										</div></div>
										
										<pagebreak />
										<div class='inner' style='top: 891mm;'>
											<!--<div class='filler' style='width:100%; height: 40.9mm'></div>-->
                                            ".$pageTableHeader."
											<div class='learningIndex_bg'>";
								
								$currentSize = $headerSize;
								$isNeedLineBreak = false;
							}
						}
						
						$x .= "	<table class='tbl_learningIndex'>
									<tbody>
										<tr class='control'>
											<td style='width: 116mm;'>&nbsp;</td>
											<td style='width: 50mm;'>&nbsp;</td>
										</tr>
										<tr>
											<td class='learningIndex_subtitle'><span class='titleIndex'>".chr(ord("A") + $indicatorCount).". </span><span  class='title'>".$thisTopicCatType."</span></td>
											<td class='rate_index'><img class='rate_index_li' src='asset/images/index.png' alt='index /'></td>
										</tr>
									</tbody>
								</table>
								<table class='tbl_learningIndex'>
									<tbody>
										<tr class='control'>
											<td style='width: 10mm;'>&nbsp;</td>
											<td style='width: 108mm;'>&nbsp;</td>
											<td style='width: 10.2mm;'>&nbsp;</td>
											<td style='width: 10.2mm;'>&nbsp;</td>
											<td style='width: 10.2mm;'>&nbsp;</td>
											<td style='width: 10.2mm;'>&nbsp;</td>
											<td style='width: 10.2mm;'>&nbsp;</td>
										</tr>";
						
						//$topicCount = 1;
						$topicCount = 0;
						foreach((array)$thisIndicatTopicsAry as $thisTopicID => $thisTopicInfo)
						{
							$thisTopicScore = $studentTopicScoreAry[$thisTopicID]["Score"];
							$isScore1 = $thisTopicScore == "1" ? " chosen" : "";
							$isScore2 = $thisTopicScore == "2" ? " chosen" : "";
							$isScore3 = $thisTopicScore == "3" ? " chosen" : "";
							$isScore4 = $thisTopicScore == "4" ? " chosen" : "";
							$isScore5 = $thisTopicScore == "5" ? " chosen" : "";
							
							$x .= "		<tr class='item'>
											<td class='num'><span>".($topicCount + 1)."</span></td>
											<td class='itemName'><span>".$thisTopicInfo["NameCh"]."</span></td>
											<td class='indexNo ".$isScore1."'><span class='".$isScore1."'>1</span></td>
											<td class='indexNo ".$isScore2."'><span class='".$isScore2."'>2</span></td>
											<td class='indexNo ".$isScore3."'><span class='".$isScore3."'>3</span></td>
											<td class='indexNo ".$isScore4."'><span class='".$isScore4."'>4</span></td>
											<td class='indexNo ".$isScore5."'><span class='".$isScore5."'>5</span></td>
										</tr>";
							$topicCount++;
							
							$currentSize += $normalSize;
							$thisTopicDisplaySize = $thisTopicInfo["DisplayLen"];
							if($thisTopicDisplaySize > 53) {
								$currentSize += $extraSize;
							}
							
							$isNeedLineBreak = $currentSize >= $footerLimit;
							if($isNeedLineBreak && $thisIndicatTopicsCount > $topicCount)
							{
								$x .= "				</tbody>
												</table>
										</div></div>
										
										<pagebreak />
										<div class='inner' style='top: 891mm;'>
											<!--<div class='filler' style='width:100%; height: 40.9mm'></div>-->
                                            ".$pageTableHeader."
											<div class='learningIndex_bg'>
												<table class='tbl_learningIndex'>
													<tbody>
														<tr class='control'>
															<td style='width: 116mm;'>&nbsp;</td>
															<td style='width: 50mm;'>&nbsp;</td>
														</tr>
														<tr>
															<td class='learningIndex_subtitle'>&nbsp;</td>
															<td class='rate_index'><img class='rate_index_li' src='asset/images/index.png' alt='index /'></td>
														</tr>
													</tbody>
												</table>
												<table class='tbl_learningIndex'>
													<tbody>
														<tr class='control'>
															<td style='width: 10mm;'>&nbsp;</td>
															<td style='width: 108mm;'>&nbsp;</td>
															<td style='width: 10.2mm;'>&nbsp;</td>
															<td style='width: 10.2mm;'>&nbsp;</td>
															<td style='width: 10.2mm;'>&nbsp;</td>
															<td style='width: 10.2mm;'>&nbsp;</td>
															<td style='width: 10.2mm;'>&nbsp;</td>
														</tr>";
								
								$currentSize = $headerSize;
								$isNeedLineBreak = false;
							}
							$isNeedFooterPage = $currentSize >= $finalPageLimit;
						}
						$x .= "		</tbody>
								</table>";
					}
					$indicatorCount++;
				}
			}
			$x .= "	</div></div>";
			
			if($isNeedFooterPage) {
			    $x .= "
					<pagebreak />
					<div class='inner' style='top: 891mm;'>
						".$pageTableHeader."
						<div class='language_bg'>
						</div>
                    </div>";
			}
			
			return $x;
		}
		
		function Get_Report_Student_Award($studentInfo, $awardAry)
		{
		    $totalSize = 30.25;
		    $normalSize = 1;
		    $extraSize = 0.622;
		    //$headerSize = 5;
		    $headerSize = 4;
		    $footerLimit = 31.25 - 1.622;
		    $finalPageLimit = $footerLimit - 3;
		    $currentSize = 0;
		    
		    $pageTableHeader = "<div class='filler' style='width:100%; height: 27.6mm'></div>
        						<table class='tbl_myReport_small'>
        							<tbody>
        								<tr><td class='myReportTitle'>".$studentInfo["StudentNameCh"]."同學的獎項</td></tr>
        							</tbody>
        						</table>";
		    $x = "	<div class='inner' style='top: 891mm;'>
						".$pageTableHeader."
						<div class='award_bg'>
							<table class='tbl_language_top' style='height: 8mm'>
								<tbody>
									<tr>
										<td style='width: 100%; height: 8mm;'>&nbsp;</td>
									</tr>
								</tbody>
							</table>";
		    
		    $allAwardsCount = count($awardAry);
		    if($allAwardsCount > 0)
		    {
		        $x .= "     <table class='tbl_language_body'>
                            <tbody>
								<tr class='control'>
									<td style='width: 10mm;'>&nbsp;</td>
									<td style='width: 158mm;'>&nbsp;</td>
								</tr>";
	        
		        $awardCount = 1;
		        $currentSize += $headerSize;
		        foreach((array)$awardAry as $awardInfo)
		        {
                    $x .= "		<tr class='item'>
                                    <td class='num'><span>".$awardCount."</span></td>
									<td class='itemName'><span style='padding: 1.3mm 2.5mm;'>".$awardInfo["DisplayStr"]."</span></td>
                                </tr>";
                    $awardCount++;
                    
                    $currentSize += $normalSize;
                    $thisAwardDisplaySize = $awardInfo["DisplayLen"];
                    if($thisAwardDisplaySize > 53) {
                        $currentSize += $extraSize;
                    }
                    
                    $isNeedLineBreak = $currentSize >= $footerLimit;
                    if($isNeedLineBreak && $allAwardsCount > $awardCount)
                    {
                        $x .= "
                            </tbody>
							</table>
						</div>
                    </div>
                    
					<pagebreak />
					<div class='inner' style='top: 891mm;'>
						".$pageTableHeader."
						<div class='language_bg'>
							<table class='tbl_language_top' style='height: 17.5mm'>
								<tbody>
									<tr>
										<td style='width: 100%; height: 17.5mm;'>&nbsp;</td>
									</tr>
								</tbody>
							</table>
                            <table class='tbl_language_body'>
                            <tbody>
								<tr class='control'>
									<td style='width: 10mm;'>&nbsp;</td>
									<td style='width: 158mm;'>&nbsp;</td>
								</tr>";
                        
                        $currentSize = $headerSize;
                        $isNeedLineBreak = false;
                    }
                    $isNeedFooterPage = $currentSize >= $finalPageLimit;
                }
                
                $x .= "		</tbody>
						    </table>
                </div></div>";
		    }
		    
		    if($isNeedFooterPage) {
		        $x .= "
					<pagebreak />
					<div class='inner' style='top: 891mm;'>
						".$pageTableHeader."
						<div class='language_bg'>
						</div>
                    </div>";
		    }
		    
		    return $x;
		}
		
		function Get_Report_Student_Award_2($yearSemInfo, $studentInfo, $awardAry)
		{
		    global $intranet_root, $PowerPortfolioConfig;
		    
		    $awardDisplayImg = array();
		    $awardDisplayStr = array();
		    if(!empty($awardAry))
		    {
		        SortByColumn2($awardAry, 'AwardCode');
		        foreach((array)$awardAry as $thisAwardInfo)
		        {
		            $awardCode = $thisAwardInfo['AwardCode'];
		            $awardImageFilePath = $intranet_root."/".$PowerPortfolioConfig['eAdminPath']."/asset/images/awards/".$awardCode.".png";
		            if(isImage($awardImageFilePath))
		            {
		                // apply award image for 品學兼優獎 & 全年學業成績最優獎
		                //$awardImageWidth = ($awardCode == 'Z004' || $awardCode == 'Z011')? '40%' : '29%';
                        $awardImageWidth = ($awardCode == 'Z001' || $awardCode == 'Z002' || $awardCode == 'Z003' || $awardCode == 'Z004' || $awardCode == 'Z011' || $awardCode == 'M017') ? '40%' : '29%';
		                $awardDisplayImg[$awardImageWidth][] = "<img src='".$awardImageFilePath."' width='".$awardImageWidth."'>";
		            }
		            else
		            {
		                $awardDisplayStr[] = $thisAwardInfo['AwardNameCh'];
		            }
		        }
		    }
		    
		    $pageTableHeader = "<div class='filler' style='width:100%; height: 27.6mm'></div>
        						<table class='tbl_myReport_small'>
        							<tbody>
        								<tr><td class='myReportTitle'>".$yearSemInfo["YearName"]."年度".$studentInfo["StudentNameCh"]."同學的獎項</td></tr>
        							</tbody>
        						</table>";
		    
		    $x .= "
				<div class='inner' style='top: 891mm;'>
					".$pageTableHeader."
					<div class='award_bg2'>
                        <table class='tbl_learningIndex'>
                            <tbody>
                                <tr class='control'>
                                    <td style='width: 116mm;'>&nbsp;</td>
                                    <td style='width: 49mm;'>&nbsp;</td>
                                </tr> ";
		    
		    if(!empty($awardDisplayStr))
		    {
		        $awardDisplay = implode('<br/>', $awardDisplayStr);
		        $x .= "         <tr style='width: 100%;'>
                                    <td style='height:8mm;' colspan='2'>&nbsp;</td>
                                </tr>
                                <tr style='width: 100%;'>
                                    <td style='height: 20mm; font-size: 28px; text-align: center; vertical-align: top;' colspan='2'>".$awardDisplay."</td>
                                </tr>";
            }
            else
            {
		        //$x .= "         <tr class='header'><td class='award_title_space'>&nbsp;</td></tr>";
            }
            
            $x .= "         </tbody>
                        </table>";
            
            $awardDivHeight = 210;
            if(!empty($awardDisplayStr)) {
                $awardDivHeight = 181;
            }
            
            $x .= "     <table class='tbl_learningIndex'>
        					<tbody>
        						<tr style='height: 210mm; width: 100%;'>
        							<td class='awardList' style='height: ".$awardDivHeight."mm; width: 200mm; text-align: center; padding-left:10mm;'>";
            
//         $x .= "<img src='".$intranet_root."/".$PowerPortfolioConfig['eAdminPath']."/asset/images/awards/Z004.png"."' width='40%'>";
//         $x .= "<img src='".$intranet_root."/".$PowerPortfolioConfig['eAdminPath']."/asset/images/awards/Z011.png"."' width='40%'>";
//         $x .= "<img src='".$intranet_root."/".$PowerPortfolioConfig['eAdminPath']."/asset/images/awards/Z151.png"."' width='29%'>";
//         $x .= "<img src='".$intranet_root."/".$PowerPortfolioConfig['eAdminPath']."/asset/images/awards/Z201.png"."' width='29%'>";
//         $x .= "<img src='".$intranet_root."/".$PowerPortfolioConfig['eAdminPath']."/asset/images/awards/Z203.png"."' width='29%'>";
		    foreach((array)$awardDisplayImg as $awardTypeImages) {
                foreach((array)$awardTypeImages as $thisAwardImage) {
                    $x .= $thisAwardImage;
                }
            }
			$x .= "                 </td>
                                </tr>
        					</tbody>
                        </table>
                    </div>
                </div>";
		    
		    return $x;
		}
		
		function returnSignatureTable($ClassTeacherName)
		{
		    global $PowerPortfolioConfig;
		    
		    $x = "  <table class='signature_table'>
                        <tr class='control'>
							<td style='width: 5.5mm;'>&nbsp;</td>
							<td style='width: 40.5mm;'>&nbsp;</td>
							<td style='width: 42.5mm;'>&nbsp;</td>
							<td style='width: 68mm;'>&nbsp;</td>
							<td style='width: 13.5mm;'>&nbsp;</td>
						</tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class='SchoolCrop'><img width='37.5mm' src='/".$PowerPortfolioConfig['eAdminPath']."/asset/images/school_chop.png'></td>
                            <td class='signature_cell'>
                		        <table cellspacing='0' cellpadding='0' border='0'>
                    		        <tr><td>校長 Principal</td></tr>
                    		        <tr><td>&nbsp;</td></tr>
                    		        <tr><td><img width='25mm' src='/".$PowerPortfolioConfig['eAdminPath']."/asset/images/escola_pui_ching_signature.bmp'></td></tr>
                		        </table>
            		        </td>
                		    <td class='signature_cell'>
                		        <table cellspacing='0' cellpadding='0' border='0'>
                    		        <tr><td>副校長兼總務主任: 郭敬文</td></tr>
                    		        <tr><td>副校長: 陳敬濂</td></tr>
                    		        <tr><td>德育主任: 梁永棠</td></tr>
                    		        <tr><td>教務主任: 楊珮欣</td></tr>
                    		        <tr><td>幼稚園主任: 劉玉玲</td></tr>
                    		        <tr><td>班主任: ".$ClassTeacherName."</td></tr>
                    		        <tr><td>&nbsp;</td></tr>
                		        </table>
            		        </td>
                            <td>&nbsp;</td>
						</tr>
                        <tr>
                            <td colspan='5' style='font-size: 11px; padding-left: 8mm; padding-top: 1.5mm;' align='left'>成績報告表加蓋校方印鑑方為有效。Report cards without the school chop are invalid.<br/>www.puiching.edu.mo</td>
						</tr>
                    </table>";
		    return $x;
		}
		
		function Get_Subject_Selection($YearID = '',$onChange = '', $selectSubject, $noFirstTitle=false, $firstTitleType=0, $excludeBehaveLang=false)
		{
// 		    global $intranet_root;
// 		    include_once($intranet_root."/includes/subject_class_mapping.php");
// 		    $sbj = new subject();
// 		    $Subjects = $sbj->Get_Subject_By_Form($YearID);

		    global $indexVar, $PowerPortfolioConfig;
		    $Subjects = $indexVar['libpowerportfolio']->Get_KG_Form_Subject($YearID);

		    $excludeSubjectIDArr = array();
		    if($excludeBehaveLang) {
		        $excludeSubjectIDArr = $indexVar['libpowerportfolio']->Get_KG_Subject_From_Code($PowerPortfolioConfig['subjectCode']);
		    }

		    $SubjectArr = array();
		    $SubjectName = Get_Lang_Selection('CH_DES', 'EN_DES');
		    if($Subjects!=null){
		        foreach($Subjects as $k => $v){
		            if(in_array($v['RecordID'], (array)$excludeSubjectIDArr)) {
		                continue;
		            }
		            $SubjectArr[$k] = array($v['RecordID'], $v[$SubjectName]);
		        }
		        $SubjectArr = array_values($SubjectArr);
		    }

		    $selection = getSelectByArray($SubjectArr,"id='subjectID' name='subjectID' onchange='$onChange' required", $selectSubject, $firstTitleType, $noFirstTitle);
		    return $selection;
		}
		
		function Get_Topic_Selection($YearID = '',$onChange= 'return false;', $selectTopic){
		    global $indexVar;
		    $topics = $indexVar['libpowerportfolio']->getYearTopicByYear($YearID);
		    $name = Get_Lang_Selection('CH_Name','EN_Name');
		    $topicArr = array();
		    if(!empty($topics)){
		        foreach($topics as $k => $v){
		            $topicArr[$k] = array($v['TopicTimeTableID'], $v[$name]);
		        }
		    }
		    $selection = getSelectByArray($topicArr, "id='topicID' name='topicID' onchange='$onChange' required", $selectTopic, 0, 0);
		    return $selection;
		    //$cols = "TopicTimeTableID, TimeTableCode, EN_Name, CH_Name, YearID, StartDate, EndDate";
		}

		function Get_Chapter_Selection($selectedChapter = '', $onChange, $YearID = '', $ZoneID = '', $filterByClassTeacher=false){
			global $indexVar;
			$chapterList = $indexVar['libpowerportfolio']->Get_Chapter_List($YearID, $ZoneID, $filterByClassTeacher);
			$selectArr = array();
			//$selectArr[] = array('',$Lang['General']['All']);
			if(!empty($chapterList)){
                $chapterList = Array_Trim($chapterList);
				foreach($chapterList as $chapter){
					if($chapter != "") $selectArr[] = array($chapter, $chapter);
				}
			}
			$selection = getSelectByArray($selectArr, "id='chapter' name='chapter' onchange='$onChange'", $selectedChapter, 0, 0);
			return $selection;
		}
		
		function Get_Rubric_Index_Setting_Table($settingID = '', $isTopicSetting = false, $topicSettingID = '', $maxLevel = ''){
			global $indexVar, $Lang, $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT.'includes/json.php');
			$jsonObj = new JSON_obj();
			
			if($isTopicSetting && $maxLevel == ''){
				$rubricSettingInfo = $indexVar['libpowerportfolio']->Get_Rubric_Setting($settingID);
				$maxLevel = $rubricSettingInfo[0]['LevelNum'];
			}
			
			$tableHtml = '<table class="table table-multi-levels">';
			
			if(!$isTopicSetting){
				$tableHead = <<<EOD
				<thead>
			        <tr class="master_row">
			            <th>&nbsp;</th>
			            <th class="c-icon">&nbsp;</th>
						<th class="c-icon">&nbsp;</th>
			            <th class="c-icon"><a href="#" class="btn-table-expand master"></a></th>
			        </tr>
		        </thead>
EOD;
			} else {
				$tableHead = <<<EOD
				<thead>
			        <tr class="master_row">
			            <th>&nbsp;</th>
			            <th class="c-selected text-right">{$Lang['PowerPortfolio']['Settings']['Topic']['Selected']}<span id="RubricTotal"></span></th>
						<th class="c-icon"><a href="javascript:toggleSelectAll()" class="btn-select all selected"></a></th>
			            <th class="c-icon"><a href="#" class="btn-table-expand master"></a></th>
			        </tr>
		        </thead>
EOD;
			}
			$tableHtml .= $tableHead;
			$tableHtml .= '<tbody>';
			if($settingID==''){
				$tableHtml .= $this->Get_Rubric_Index_Setting_Row(0, 1, 1, '1', $Lang['PowerPortfolio']['Settings']['Rubrics']['PleaseInputName'], true, false, $maxLevel);
			} else {
				$rubricIndexValueAry = $indexVar['libpowerportfolio']->Get_Rubric_Ability_Index_Item($settingID);
				$tempAry = array(null);
				foreach($rubricIndexValueAry as $value){
					$code = explode('.', $value['Code']);
					$tableHtml .= $this->Get_Rubric_Index_Setting_Row($value['Level'], $code[0],$code[1],$value['Code'], $value['Name'], false, $isTopicSetting, $maxLevel);
					switch(sizeof($code)){
						case '1':
							$tempAry[$code[0]] = array('value'=>$value['Name'], 'code'=> $value['Code'], 'child' => array(null));
							break;
						case '2':
							$tempAry[$code[0]]['child'][$code[1]] = array('value'=>$value['Name'], 'code'=> $value['Code'], 'child' => array(null));
							break;
						case '3':
							$tempAry[$code[0]]['child'][$code[1]]['child'][$code[2]] = array('value'=>$value['Name'], 'code'=> $value['Code'], 'child' => array(null));
							break;
					}
				}
			}
			$tableHtml .= '</tbody>';
			
			$hiddenHtml = "";
			
			$hiddenScript = "<script type='text/javascript'>";
			$hiddenScript .= "RubricIndexValue = [];";
			if($settingID==''){
				$hiddenScript .= "RubricIndexValue[1] = {'value': '', 'child': [null], 'code': '1'};";
			} else if(!$isTopicSetting) {
				$RubricIndexValueJSON = $jsonObj->encode($tempAry);
				$hiddenScript .= "let TempJSON = '$RubricIndexValueJSON';";
				$hiddenScript .= 'RubricIndexValue = JSON.parse(TempJSON);';
			} else {
				if($topicSettingID!=''){
					$topicSetting = $indexVar['libpowerportfolio']->Get_Rubric_Topic_Setting($topicSettingID);
// 					debug_pr($topicSetting);
					if($topicSetting[0]['RubricSettingID'] == $settingID){
						$tempAry = array();
						$valueList = $topicSetting[0]['RubricList'];
						$valueListAry = explode(',', $valueList);
						foreach($valueListAry as $val){
							if($val=='')continue;
							$hiddenScript .= "RubricIndexValue['{$val}'] = true;";
						}
					}
				}
			}
			$hiddenScript .= "</script>";
			
			return $tableHtml . $hiddenHtml . $hiddenScript;
		}
		
		function Get_Rubric_Index_Setting_Row($level, $maj, $sub, $code, $content, $isNew=false, $isTopicSetting = false, $maxLevel = ''){
			global $indexVar, $Lang;
			
			switch($level){
				case 0:
					$levelClass = '';
					$expandClass = 'btn-table-expand maj_row'.$maj;
					break;
				case 1:
					$levelClass = 'level-1 collapsible show maj_row'.$maj;
					$expandClass = 'btn-table-expand maj_row'.$maj.' sub_row'.$sub;
					break;
				case 2:
					$levelClass = 'level-2 collapsible show maj_row'.$maj.' sub_row'.$sub;
					$expandClass = '';
					break;
				default:
					break;
			}
			if($isNew && $content == ''){
				$content = $Lang['PowerPortfolio']['Settings']['Rubrics']['PleaseInputName'];
			}
			$expandHtml = '<a href="#" class="' . $expandClass .'"></a>';
			
			if(!$isTopicSetting){
				$html = <<<EOD
				<tr class="{$levelClass}">
		            <td>
		                <span class="indent"></span>
		                <div class="text" alt-code="{$code}">{$code} {$content}</div>
		            </td>
		            <td class="c-icon"><a onclick="editRubricIndex(this)" class="btn-edit"><i class="fas fa-pen"></i></a></td>
		           	<td class="c-icon"><a onclick="deleteRubricIndex(this)" class="btn-delete"><i class="fas fa-trash"></i></a></td>
					<td class="c-icon">{$expandHtml}</td>
	        	</tr>

EOD;
			} else {
				if($level==0){
					$textRightID = 'id="RubricSelected_$maj"';
				} else {
					$textRightID = '';
				}
				if($level==2 || $level == ($maxLevel - 1)){
					$selectedBox = '<a onclick="toggleSelected(this)" class="btn-select"></a>';
				} else {
					$selectedBox = '';
				}
				$html = <<<EOD
				<tr class="{$levelClass}">
		            <td>
		                <span class="indent"></span>
		                <div class="text" alt-code="{$code}">{$code} {$content}</div>
		            </td>
		            <td class="text-right" {$textRightID}></td>
		           	<td class="c-icon">{$selectedBox}</td>
					<td class="c-icon">{$expandHtml}</td>
	        	</tr>
	        	
EOD;
			}
			if($isNew){
				if($level==2 && $maxLevel==3){
					$html .= $this->Get_Rubric_Index_Setting_AddRow(2,$maj,$sub);
				} else if($level==1){
					if($maxLevel==3) $html .= $this->Get_Rubric_Index_Setting_AddRow(2,$maj,$sub);
					$html .= $this->Get_Rubric_Index_Setting_AddRow(1,$maj,$sub+1);
				} else if($level==0){
					$html .= $this->Get_Rubric_Index_Setting_AddRow(1,$maj,$sub);
					$html .= $this->Get_Rubric_Index_Setting_AddRow(0,$maj+1,$sub);
				}
			}
			return $html;
		}
		
		function Get_Rubric_Index_Setting_AddRow($level, $maj, $sub){
			global $indexVar, $Lang;
			
			switch($level){
				case 0:
					$levelClass = '';
					$title = $Lang['PowerPortfolio']['Settings']['Rubrics']['AddCat'];
					break;
				case 1:
					$levelClass = 'level-1 collapsible show maj_row'.$maj;
					$title = $Lang['PowerPortfolio']['Settings']['Rubrics']['AddSubCat'];
					break;
				case 2:
					$levelClass = 'level-2 collapsible show maj_row'.$maj.' sub_row'.$sub;
					$title = $Lang['PowerPortfolio']['Settings']['Rubrics']['AddItem'];
					break;
				default:
					break;
			}
			
			$html = <<<EOD
			<tr class="{$levelClass}">
			<td><span class="indent"></span> <a onclick="newRubricIndex(this)" alt-level="{$level}_{$maj}_{$sub}" class="btn-add-item"><i class="fas fa-plus"></i> {$title}</a></td>
	            <td class="c-icon">&nbsp;</td>
				<td class="c-icon">&nbsp;</td>
	            <td class="c-icon">&nbsp;</td>
	        </tr>
EOD;
			
			return $html;
		}
		function Get_Rubric_Index_Edit_Box($code, $value){
			global $indexVar, $Lang;
			
			$html = '<table class="form_table_v30">';
			$html .= "<td class='field_title'></td>";
			$html .= "<td><input type='text' id='thickBox_Text' value='$value' placeholder='".$Lang['PowerPortfolio']['Settings']['Rubrics']['PleaseInputName']."'/></td>";
			$html .= '</table>';
			$html .= '<div class="edit_bottom_v30">';
			$html .= '<p class="spacer"></p>';
			$html .= $this->GET_BTN($Lang['Btn']['Submit'], 'button', 'onchangeRubricIndex(\''.$code .'\')');
			$html .= '&nbsp;';
			$html .= $this->GET_BTN($Lang['Btn']['Cancel'], 'button', 'js_Hide_ThickBox();');
			$html .= '<p class="spacer"></p>';
			$html .= '</div>';
			
			return $html;
		}
		function Get_RubricIndex_Selection($selectedCode, $topicSettingID,$upperCode='', $level=0, $propName='RubricCode',$onChange="document.form1.submit()", $noFirstTitle=false, $firstTitleType=0){
			global $indexVar;
			
			$topicSettingInfo = $indexVar['libpowerportfolio']->Get_Rubric_Topic_Setting($topicSettingID);
			if(!empty($topicSettingInfo)){
				$rubricSettingID = $topicSettingInfo[0]['RubricSettingID'];
				$rubricSettingInfo = $indexVar['libpowerportfolio']->Get_Rubric_Setting($rubricSettingID);
				$_maxLevel = $rubricSettingInfo[0]['LevelNum'];
				if($level > $_maxLevel-1) return '';
				if($level == $_maxLevel-1) $lastLevelClass = 'bottomRubric';
			}
			
			$table1 = $indexVar['thisDbName'] .'.RC_TOPIC_SETTING';
			$table2 = $indexVar['thisDbName'] .'.RC_ABILITY_INDEX_ITEM';
			$conds = " aii.Level = '$level' AND ts.TopicSettingID = '$topicSettingID' ";
			if($upperCode != ''){
				$conds .= " AND aii.UpperCat = '{$upperCode}' ";
			}
			if($level==2){
				$conds .= " AND FIND_IN_SET(aii.Code, ts.RubricList) ";
			}
			$order = " aii.Code ASC";
			$cols = "aii.Code, CONCAT(aii.Code, ' ', aii.Name) AS Name";
			$sql = "SELECT $cols FROM $table1 ts INNER JOIN $table2 aii ON ts.RubricSettingID = aii.RubricSettingID WHERE $conds ORDER BY $order";
			$codeAry = $indexVar['libpowerportfolio']->returnArray($sql);

			if(empty($codeAry)) {
			    return '';
            }
			return getSelectByArray($codeAry, " name='{$propName}[{$level}]' id='{$propName}_{$level}' class='{$lastLevelClass}' onChange='$onChange' ", $selectedCode, $firstTitleType, $noFirstTitle);
		}
		function Get_TopicSetting_Selection($selectedTopic="", $onChange="document.form1.submit()", $YearID = '', $noFirstTitle=false, $firstTitleType=0)
		{
			global $indexVar;

			$table = $indexVar['thisDbName'] .'.RC_TOPIC_SETTING';
			$cols = 'TopicSettingID, Name';
			$conds = "";
			if($YearID!= ''){
				$conds .= " AND YearID = '{$YearID}' ";
			}
			$sql = "SELECT $cols FROM $table WHERE 1 $conds";
			$topicArr = $indexVar['libpowerportfolio']->returnArray($sql);
			return getSelectByArray($topicArr, " name='TopicSettingID' id='TopicSettingID' onChange='$onChange' ", $selectedTopic, $firstTitleType, $noFirstTitle);
		}

		function Get_TopicSetting_ContentType_Selection($selectedType="", $onChange="document.form1.submit()")
		{
			global $indexVar, $Lang;

			$typeArr = $Lang['PowerPortfolio']['Settings']['Activity']['TypeArr'];

			$html = '';

			foreach($typeArr as $type => $typeName){
				$checked = $selectedType == $type ? 'checked' : '';
				$html .= "<input type='radio' id='ContentType_{$type}' name='ContentType' value='{$type}' onChange='{$onChange}' {$checked} />";
				$html .= "&nbsp;<label for='ContentType_{$type}'>{$typeName}</label>&nbsp;";
			}

			return $html;
		}

		function Get_Zone_Selection($selectedZone="", $onChange="document.form1.submit()",$topicSettingID='', $noFirstTitle=false, $firstTitleType=0)
        {
			global $indexVar;
			$table = $indexVar['thisDbName'] .'.RC_ZONE';
			$cols = " ZoneID, Name ";
			$conds = '1';
			if($topicSettingID!=''){
				$conds .= " AND TopicSettingID = '$topicSettingID' ";
			}
			$sql = "SELECT $cols FROM $table WHERE $conds";
			$zoneAry = $indexVar['libpowerportfolio']->returnArray($sql);

			return getSelectByArray($zoneAry, " name='ZoneID' id='ZoneID' onChange='$onChange' ", $selectedZone, $firstTitleType, $noFirstTitle);
		}

		function Get_Comment_Edit_Box($commentID='', $isCat=false)
        {
			global $indexVar, $Lang, $PowerPortfolioConfig;
			
			if($commentID!=''){
				$commentAry = $indexVar['libpowerportfolio']->Get_Comment($commentID, '', $showComment=true, $showCat=true);
				if(!empty($commentAry)) $comment = $commentAry[0];
				$hiddenHtml = '<input type="hidden" name="CommentID" id="CommentID" value="'.$comment['CommentID'].'" />';
			}
			$commentLen = $PowerPortfolioConfig['CommentMaxLength'];

			$html = '<form id="thickbox_form" name="thickbox_form">';
			$html .= '<table class="form_table_v30">';
			$html .= "<td class='field_title'></td>";
			if($isCat){
				$html .= "<td><input type='text' id='CommentContent' name='CommentContent' value='{$comment['Content']}' /></td>";
			} else {
				$html .= "<td><textarea id='CommentContent' name='CommentContent' cols='80' rows='4'
                                 wrap='virtual' 
                                 onkeyup='limitText(this, ".$commentLen.", 0, 1, \"alertCommentMaximum()\");' 
                                 onpaste='setTimeout(function() { limitText(document.getElementById(\"CommentContent\"), ".$commentLen.", 0, 1, \"alertCommentMaximum()\"); }, 10);'
                                 ondrop='setTimeout(function() { limitText(document.getElementById(\"CommentContent\"), ".$commentLen.", 0, 1, \"alertCommentMaximum()\"); }, 10);' 
                                 onfocus='this.rows=4;'
                            >{$comment['Content']}</textarea>";
			}
			$html .= '</table>';
			$html .= $hiddenHtml;
			$html .= '<div class="edit_bottom_v30">';
			$html .= '<p class="spacer"></p>';
			$html .= $this->GET_BTN($Lang['Btn']['Submit'], 'button', 'updateComment()');
			$html .= '&nbsp;';
			$html .= $this->GET_BTN($Lang['Btn']['Cancel'], 'button', 'js_Hide_ThickBox();');
			$html .= '<p class="spacer"></p>';
			$html .= '</div>';
			$html .= '</form>';
			
			return $html;
		}
		
		function Get_Report_Selection($tags='', $Onchange='document.form1.submit()'){
			global $Lang, $indexVar, $PowerPortfolioConfig;
			
			$reportSettingAry = $indexVar['libpowerportfolio']->Get_Report_Setting();
			
			$ReportArr = array();
			foreach($reportSettingAry as $rs){
				$ReportArr[] = array($rs['ReportID'], $rs['Name']);
			}
			return getSelectByArray($ReportArr," $tags onChange='$Onchange'" , $selectLevel, 0, 0);
		}
		
		function Get_Report_Rubric_Result($studentInfo, $ResultSummaryImage, $TopicResultScoreAry, $yearSemInfo)
		{
			global $indexVar;
			
			$x = "	<div class='inner1' style='top: 891mm;'>
						<div class='filler' style='width:100%; height: 27.6mm'></div>
						<table class='tbl_myReport'>
							<tbody>
								<tr>
									<td class='myReportTitle'>".$studentInfo["StudentNameCh"]."同學的學習成績</td>
								</tr>
								<tr>
									<td class='myReport'>".$ResultSummaryImage."</td>
								</tr>
							</tbody>
						</table>";
			
			$page1Line = 23;
			$page2Line = 45;
			$pageLineTotal = $page1Line;
			$x .= "	</div>";
			
			return $x;
		}
	}
}
?>