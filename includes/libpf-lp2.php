<?
//editing : Crystal
/**
* LPv2 Main Library
* 2020-10-22 Crystal [ip.2.5.11.11.1]
*       - hide all facebook like function
* 2020-06-22 Crystal [ip.2.5.11.7.1]
*       - modified function getPortfolioUsers() to sort classNumber within className sortBy
* 2019-06-14 Pun [ip.2.5.10.6.1]
*      - Modified replaceFilePaths(), fixed cannot export PowerVoice for CD Burn
* 2019-05-08 Pun [160276] [ip.2.5.10.6.1]
*      - Fix hardcoded http
* 2019-02-01 Pun [137529] [ip.2.5.10.2.1]
*       - modified function copyPortfolioElements(), fixed cannot copy profolio, missing added slashes
*       - modified function createPortfolio(), fixed cannot create portfolio without choosing Year
*
* 2018-09-02 Siuwan [ip.2.5.9.9.1]
*       - modified function getPortfolioUsers() to add sortBy and publish status logic
*
* 2018-04-24 Pun [ip.2.5.9.5.1]
* 		- modified pullPortfolioPrototypeNewElements(), fixed when create sub-page will load the start/end date of parent-page
*
* 2016-12-09 Ronald
* 		- modified function getUserPortfolios() add new parameter $conds for extra conditions.
*
* 2016-12-02 Ronald
* 		- modified function updatePortfolioInfo() and createPortfolio(), add new column FormID for KIS.
*
* 2016-04-01 Siuwan [ip.2.5.7.4.1.0]
* 		- modified function getPublishedPortfolioUserClassListByPortfolioId(), check academic year and use ClassTitle instead of ClassName
*
* 2015-11-11 Siuwan [ip.2.5.7.1.1.0] (Case#U80923)
* 		- added function getElementByPrototypeId(), getPublishedPortfolioUserClassListByPortfolioId(), getPublishedPortfolioUserListByClassId() for printing student LP
*
* 2014-07-24 Siuwan
* 		- added updateResetLog() and modified syncElement() to update reset log if $reset = 1
* 		- included in deploy ip.2.5.5.8.1.0
*
* 2014-02-06 Siuwan
* 		- modified getFriendPortfolios() add parameter $friend_intranet_ids to input friend UserID for filtering result
*
*
* please note that both EC and IP will include this
*
* @author Mick Chiu
* @since 2012-03-27
*
*/

$eclass40_filepath = $eclass40_filepath? $eclass40_filepath: $eclass_root;

include_once 'libpf-lp.php';
class libpf_lp2 extends libpf_lp{

	public static $themes = array('01','02','03','04','05','06','07','08','09','10','11','12','13','14','15');

	public static $old_themes 		= array('01','02','03','04','05','06');
	public static $judge_group_id 	= 32;
	public static $is_competition 	= false;
	public static $is_stem_learning_scheme 	= false;
	public static $is_admin 		= false;
	public $course_id, $user_id, $user_intranet_id, $web_portfolio_id, $current_user_id, $current_user_intranet_id;
	private $lp_version, $theme, $portfolio_info, $is_portfolio_member, $user_folder_name, $is_judge;

	public static function isFromKIS(){
		return ($_SESSION["platform"]=="KIS");
	}

	public static function getPortfolioCourseId(){

		global $eclass_db;

		$libdb = new libdb();

		$libdb->db = $eclass_db;

		$sql = "Select course_id from course where RoomType=4 Limit 1";

		return current($libdb->returnVector($sql));

	}
	/**
	 * writing portfolio course id to session
	 * not recommended
	 */
	public static function registerCourseId(){

		global $eclass_db, $ck_course_id, $ck_user_id, $UserID;

		$ck_course_id 	= self::getPortfolioCourseId();
		$ck_user_id 	= $UserID;

		session_register("ck_course_id");
		session_register("ck_user_id");

	}
	public static function encryptPortfolioKey($portfolio_id, $user_intranet_id){

		return base64_encode($portfolio_id.'join'.$user_intranet_id);

	}
	public static function decryptPortfolioKey($key){

		$pair = explode('join', base64_decode($key));
		return $pair;

	}
	public static function getPortfolioUrl($portfolio){

		global $intranet_rel_path, $eclass_url_path, $eclass40_httppath;

		$HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';
		if ($eclass40_httppath){
		    return $HTTP_SSL.$eclass40_httppath.'src/iportfolio/?portfolio='.$portfolio;
		}else{
		    return $HTTP_SSL.str_replace(array('http://','https://','/'),'',$intranet_rel_path).'/eclass40/src/iportfolio/?portfolio='.$portfolio;
		}

	}
	public static function getDaysWord($date_string){

		global $langpf_lp2, $request_ts;

		if (!$date_string) return '--';

		$ts_diff = ($request_ts?$request_ts:time())-strtotime($date_string);

		if ($ts_diff > 86400){

			$day = floor($ts_diff/86400);

			return $day.' '.($day==1? $langpf_lp2['admin']['item']["daysago"]: $langpf_lp2['admin']['item']["daysago"]);
		}else if ($ts_diff > 3600){
			return floor($ts_diff/3600).' '.$langpf_lp2['admin']['item']["hoursago"];
		}else if ($ts_diff > 60){
			return floor($ts_diff/60).' '.$langpf_lp2['admin']['item']["minsago"];
		}else{
			return $langpf_lp2['admin']['item']["lessthan1minago"];
		}


	}
	public static function createPortfolio($data){

		global $eclass_prefix;

		$libdb = new libdb();

		$libdb->db = $eclass_prefix."c".self::getPortfolioCourseId();

		if(self::isFromKIS()){
			$sql  = "INSERT INTO web_portfolio (title, instruction, status, starttime, deadline, inputdate, modified, folder_size ,created_by, version, form_id)
		values ('".$data['title']."', '".$data['instruction']."', '".$data['status']."', ".$data['starttime'].", ".$data['endtime'].", now(), now(), ".$data['sizeMax'].",'".$data['user_intranet_id']."', 1, '".$data['form_id']."')";

		}else{
			$sql  = "INSERT INTO web_portfolio (title, instruction, status, starttime, deadline, inputdate, modified, folder_size ,created_by, version)
		values ('".$data['title']."', '".$data['instruction']."', '".$data['status']."', ".$data['starttime'].", ".$data['endtime'].", now(), now(), ".$data['sizeMax'].",'".$data['user_intranet_id']."', 1)";

		}

		$libdb->db_db_query($sql);echo mysql_error();

		$portfolio_id = $libdb->db_insert_id();

		return new libpf_lp2($data['user_intranet_id'], $portfolio_id, $data['user_intranet_id'], 'prototype');
	}
	/******** Constuctor ********/
	public function libpf_lp2($user_intranet_id=0, $portfolio_id=0, $current_user_intranet_id=0, $mode=''){

		global $eclass_prefix, $eclass_filepath, $ck_memberType, $eclass_db, $sys_custom, $ck_is_iPortfolioSuperAdmin;

		$this->course_id 	= self::getPortfolioCourseId();
		$this->db 		= $eclass_prefix."c".$this->course_id;
		$this->user_intranet_id = $user_intranet_id?$user_intranet_id:$current_user_intranet_id;
		$this->user_id 		= $this->IP_USER_ID_TO_EC_USER_ID($user_intranet_id);
		$this->mode 		= $mode;
		$this->web_portfolio_id = $portfolio_id;
		$this->template_prefix = 'LP';
		$this->template_file_path = $this->db.'/portfolio/templates/';
		$this->student_file_path = $this->db.'/portfolio/';
		$this->template_delimiter = '|=|';
		libpf_lp2::$is_competition = $sys_custom['lp_competition']==true;
		libpf_lp2::$is_admin = $ck_is_iPortfolioSuperAdmin==true;

	    libpf_lp2::$is_stem_learning_scheme = ($sys_custom['lp_stem_learning_scheme'] || $_SESSION['SSV_PRIVILEGE']['sys_custom']['lp_stem_learning_scheme']);

		if($sys_custom['enable_hk5skg_template']){
			libpf_lp2::$themes[] = 16;
		}
		if ($current_user_intranet_id){

			$this->current_user_intranet_id 	= $current_user_intranet_id;
			$this->current_user_id 		= $this->IP_USER_ID_TO_EC_USER_ID($current_user_intranet_id);
			//$this->memberType 			= $ck_memberType = current($this->returnVector("select memberType from usermaster where user_id='".$this->current_user_id."'"));
			$this->memberType 			= current($this->returnVector("select memberType from usermaster where user_id='".$this->current_user_id."'"));
			if($this->memberType != 'T' && $this->mode == 'draft' && $this->user_intranet_id != $this->current_user_intranet_id){ //if not teacher + draft + intranet_id not match, update user_intranet_id
				$this->user_intranet_id = $this->current_user_intranet_id;
				$this->user_id 		= $this->IP_USER_ID_TO_EC_USER_ID($user_intranet_id);
			}
			$this->is_portfolio_member 		= $this->isPortfolioGroupMember();
		}

		/**** User Permission START ****/

	    if(self::$is_stem_learning_scheme){
	        if(self::$is_admin){
	            $this->user_permission = 0;
	        }else if ($this->memberType=='T'){//non-admin teacher
	            $this->is_judge = true;
	            $this->user_permission = 1;
	        }else if ($this->is_portfolio_member){//teacher not in competition or member in same group
	            $this->user_permission = $this->current_user_id==$this->user_id? 0: 999;//owner or other users in same group
	        }else{
	            $this->user_permission = 999; //not in the same group
	        }
	    }else{
			$this->is_judge = self::$is_competition && in_array(self::$judge_group_id, $this->getUserPortfolioGroups());

	        if($this->is_parent()){ //owner permission for student's parent
	            $this->user_permission = 0;
	        }else if ($this->is_portfolio_member || ($this->memberType=='T' && !self::$is_competition) || self::$is_admin){//teacher not in competition or member in same group
	            $this->user_permission = $this->memberType=='T' || $this->current_user_id==$this->user_id? 0: 1;//owner or teacher vs other users in same group
	        }else{
	            $this->user_permission = 2; //not in the same group
	        }

	        if (self::$is_competition && $this->is_judge) $this->user_permission = 1;

    		if (!$current_user_intranet_id){
    			$this->user_permission = 3;//not logged in
    		}
	    }
		/**** User Permission END ****/


		$this->setPortfolioMode($mode);

		$this->portfolio_info = $this->loadPortfolioInfo();

		if(function_exists("get_client_region")) $this->thisRegion = get_client_region();

	}
	private function copyPortfolioElements($id=-1, $new_parent_id='null'){

		$parent_element = $this->getElement($id);
		$parent_element = array_map('addslashes', $parent_element);

		$sql = "insert into portfolio_prototype (
		    user_id	, web_portfolio_id	, parent_id	,
		    ordering	, title			, description	,
		    url		, content		, type		,
		    status	, starttime		, endtime	,
		    inputdate, modified
		)values (
		    ".$this->current_user_id."		, ".$this->new_portfolio_id."		, $new_parent_id			,
		    '".$parent_element['ordering']."'	, '".$parent_element['title']."'	, '".$parent_element['description']."'	,
		    '".$parent_element['url']."'	, '".$parent_element['content']."'	, '".$parent_element['type']."'		,
		    '".$parent_element['status']."'	, '".$parent_element['starttime']."'	, '".$parent_element['endtime']."'	,
		    now(), now()
		)";

		$this->db_db_query($sql);

		$new_element_id = $id==-1?-1:mysql_insert_id();

		$old_children = $this->getElementChildren($id);

		foreach ($old_children as $old_child){
			$this->copyPortfolioElements($old_child['id'], $new_element_id);
		}

		return $this;

	}
	public function clonePortfolio($data){

		$this->setPortfolioMode('prototype');

		$liblp2 = self::createPortfolio($data);

		$this->new_portfolio_id = mysql_insert_id();

		$this->copyPortfolioElements()->setPortfolioMode($this->mode);

		return $liblp2;

	}
	public function setPortfolioMode($mode){

		$this->portfolio_table = ' portfolio_'.$mode;
		$this->portfolio_cond  = " web_portfolio_id = '".$this->web_portfolio_id."'";
		$this->portfolio_cond .= $mode == 'prototype'? " " : " and user_id = '".$this->user_id."'";

		return $this;

	}
	private function loadPortfolioInfo(){

		$within_time = "(starttime<now() or starttime=0 or starttime is null)
		and (deadline>now() or deadline=0 or deadline is null)";
		if(self::isFromKIS()){
			$sql = "Select title, instruction, in_used, status, folder_size, version, starttime, deadline,form_id, if ($within_time, 1,0) as within_time
			from web_portfolio where web_portfolio_id = '".$this->web_portfolio_id."'";
		}else{
			$sql = "Select title, instruction, in_used, status, folder_size, version, starttime, deadline, if ($within_time, 1,0) as within_time
			from web_portfolio where web_portfolio_id = '".$this->web_portfolio_id."'";
		}

		return current($this->returnArray($sql));

	}
	/******** User/Portfolio Info functions ********/
	public function getUserFolder(){
		global $ck_memberType,$ck_user_id;
		if (!$this->user_id || !$this->web_portfolio_id) return array('',-1);

		$fm = new fileManager($this->course_id, 7, "");

		if($ck_memberType=='T'&&$this->user_id!=$ck_user_id){
			$sql = "SELECT user_email,memberType FROM usermaster WHERE user_id = '".$this->user_id."'";
			$result = current($this->returnArray($sql));
			$fm->memberType = $result['memberType'];
			$fm->user_email = $result['user_email'];
		}else{
			$fm->memberType = $this->memberType;
		}
		$fm->user_id	= $this->user_id;
		$fm->permission = $_SESSION["platform"]=="KIS"?'GWAW':'GR';

		if ($this->memberType!='T') $fm->maxSize = $this->portfolio_info['folder_size'];

		$description = $this->portfolio_info['title'];
		$folder_name = 'student_u'.$this->user_id.'_wp'.$this->web_portfolio_id;

		return array($folder_name, $fm->createFolderDB($folder_name, $description, "", 7));

	}
	public function getUserInfo($intranet_user_id=false){

		global $intranet_db, $image_path, $LAYOUT_SKIN, $intranet_session_language, $intranet_root, $intranet_rel_path, $ck_current_academic_year_id;

		if (!$intranet_user_id) $intranet_user_id = $this->user_intranet_id;//teacher view

		$sql = "select UserLogin, ".getNameFieldByLang2("",$intranet_session_language)." as name, ClassName, ClassNumber, PhotoLink, PersonalPhotoLink
		from {$intranet_db}.INTRANET_USER
		where UserID = '".$intranet_user_id."'";

		$user = current($this->returnArray($sql));

		if (file_exists($intranet_root."/file/user_photo/".$user['UserLogin'].".jpg")){
			$photo_link = "/file/user_photo/".$user['UserLogin'].".jpg";
		}else if ($user['PhotoLink']&&file_exists($intranet_root.$user['PhotoLink'])){
			$photo_link = $user['PhotoLink'];
		}else if ($user['PersonalPhotoLink']&&file_exists($intranet_root.$user['PersonalPhotoLink'])){
			$photo_link = $user['PersonalPhotoLink'];
		}else{
			$photo_link = "/images/2009a/iPortfolio/no_photo.jpg";
		}

		//$user['name'] 	= $intranet_session_language=="b5"?$user['ChineseName']:$user['EnglishName'];
		$user['class'] 	= $user['ClassName']? $user['ClassName'].' - '.$user['ClassNumber'] : '--';
		$user['photo'] 	= $intranet_rel_path.$photo_link;
		$user['school'] = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
		$user['is_judge'] = $this->is_judge;

		if(self::isFromKIS() && $this->memberType=='T'){
			if($_SESSION["SSV_USER_ACCESS"]["other-iPortfolio"]){
				$user['KIS_teacher_type'] = 'admin';
			}else {
				$sql = "SELECT count(*)
				FROM
				{$intranet_db}.ASSESSMENT_ACCESS_RIGHT AAR
				WHERE
				AccessType = '0'
				AND SubjectID IS NULL
				AND AAR.UserID = '{$intranet_user_id}'";

				if(current($this->returnVector($sql))>0){
					$user['KIS_teacher_type'] = 'CoursePanel';
				}else{
					$sql = "SELECT YearID
					FROM
					{$intranet_db}.ASSESSMENT_ACCESS_RIGHT AAR
					WHERE
					AccessType = '1'
					AND SubjectID IS NULL
					AND AAR.UserID = '{$intranet_user_id}'";

					$formIdAry = $this->returnArray($sql);

					$user['KIS_specific_form'] = $formIdAry;

					$sql = "Select  a.YearID, a.YearClassID, a.ClassTitleEN
					From
					{$intranet_db}.YEAR_CLASS a
					INNER JOIN
					{$intranet_db}.YEAR_CLASS_TEACHER c
					ON
					a.YearClassID = c.YearClassID
					WHERE a. AcademicYearID = '$ck_current_academic_year_id' AND c.UserID = '{$intranet_user_id}'
					";
					$classDetail = $this->returnArray($sql);

					if(sizeof($formIdAry) > 0 && sizeof($classDetail) > 0){
						$user['KIS_teacher_type'] = 'FormClassTeacher';
						$user ['KIS_class_info'] = $classDetail;
						$user ['KIS_specific_form'] = $formIdAry;
					}else{
						if(sizeof($formIdAry) > 0){
							$user['KIS_teacher_type'] = 'FormTeacher';
							$user ['KIS_specific_form'] = $formIdAry;
						}else if(sizeof($classDetail) > 0){
							$user['KIS_teacher_type'] = 'ClassTeacher';
							$user ['KIS_class_info'] = $classDetail;

						}else{
							$user['KIS_teacher_type'] = 'NormalTeacher';
						}
					}

				}
			}
		}

		return $user;

	}

	public function getUserFriends(){

		$sql = "SELECT FriendID
		FROM user_friend
		WHERE UserID = '".$this->current_user_intranet_id."'";

		$friend_intranet_ids = $this->returnVector($sql);

		$friends=array();
		foreach ($friend_intranet_ids as $friend_intranet_id){
			$friend=$this->getUserInfo($friend_intranet_id);
			$friend['friend_id'] = $friend_intranet_id;
			$friends[]=$friend;
		}

		return $friends;
	}
	public function createFriends($friend_intranet_ids){
		// old functions
		$this->DELETE_STUDENT_FRIEND($this->current_user_intranet_id);
		$this->ADD_STUDENT_FRIEND($this->current_user_intranet_id, $friend_intranet_ids);

	}
	private function isPortfolioGroupMember(){

		$portfolio_groups = $this->getPortfolioGroupUsers();

		if ($portfolio_groups){ //has group specified , check if current user is in the group

			$common_groups = array_intersect($portfolio_groups, $this->getUserPortfolioGroups());

			return self::$is_competition? sizeof($common_groups):(sizeof($common_groups) || $this->memberType=='T');
			//if not competition, all teachers can edit profolio
		}else{

			return !self::$is_competition;//not competition and no group, all students editable
		}

	}
	private function getPortfolioGroupUsers(){

		$sql = "select group_id from grouping_function where function_type = 'PORTw' AND function_id = '".$this->web_portfolio_id."'";
		return $this->returnVector($sql);

	}
	public function getUserPortfolioGroups(){
		if(!self::$is_admin && (self::$is_competition || self::$is_stem_learning_scheme)){
			$sql = "
			SELECT
				ug.group_id
			FROM
				user_group ug
			INNER JOIN
				grouping g
			ON
				ug.group_id = g.group_id
			WHERE
				ug.user_id='".$this->current_user_id."'
			AND
				g.has_right = '".IPF_CFG_GROUP_SETTING_STD."'
		";
		}else{
			$sql = "select group_id from user_group where user_id='".$this->current_user_id."'";
		}
		return $this->returnVector($sql);

	}
	public function getAllPortfolioGroups($exclude_no_publish = false){

		$exclude = $exclude_no_publish? "inner join user_group u on u.group_id = g.group_id inner join portfolio_publish p on p.user_id = u.user_id and p.ordering <> -1": "";

		$sql = "select g.group_id, g.group_name from grouping g
		$exclude
		where g.has_right=0 group by g.group_id order by g.group_name";

		return $this->returnArray($sql);

	}
	private function removeAllPortfolioGroups(){

		$sql = "DELETE FROM grouping_function
		WHERE function_type = 'PORTw' AND function_id = '".$this->web_portfolio_id."'";

		$this->db_db_query($sql);

		return mysql_affected_rows()>0;

	}
	public function createPortfolioGroups($groups){

		$this->removeAllPortfolioGroups();
		$groups = (self::$is_competition&&!self::$is_admin)? $this->getUserPortfolioGroups():$groups;

		foreach ((array)$groups as $group){
			$sql = "INSERT INTO grouping_function (group_id, function_id, function_type)
			VALUES ($group, ".$this->web_portfolio_id.", 'PORTw')";

			$this->db_db_query($sql);
		}

		return mysql_insert_id();

	}
	public function getClasses(){

		$activated_classes = array_keys((array)$this->GET_CLASS_STUDENT_ACTIVATED());
		return $activated_classes;

	}
	public function getClassStudents($class='',$keyword='', $exclude=array()){

		global $intranet_db, $eclass_db,$ck_current_academic_year_id;

		$exclude[]=$this->current_user_intranet_id;
		$exclude_str = implode(',',$exclude);


		$sql = "SELECT DISTINCT ycu.UserID
		FROM {$intranet_db}.INTRANET_USER AS iu INNER JOIN
		{$intranet_db}.YEAR_CLASS_USER AS ycu ON iu.UserID = ycu.UserID
		INNER JOIN {$intranet_db}.YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID
		INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps ON ps.UserID = iu.UserID
		WHERE yc.AcademicYearID = '$ck_current_academic_year_id'
		AND (yc.ClassTitleEN like '%$class%' OR yc.ClassTitleB5 like '%$class%')
		AND iu.UserID NOT IN ($exclude_str) AND iu.RecordType = '2' AND iu.RecordStatus = '1'
		/*AND (iu.UserLogin like '%$keyword%' OR iu.EnglishName like '%$keyword%' OR iu.ChineseName like '%$keyword%')*/
		AND( iu.EnglishName like '%$keyword%'
		OR iu.ChineseName like BINARY '%$keyword%'
		OR iu.ClassName like '%$keyword%'
		OR CONCAT(LCASE(iu.ClassName),iu.ClassNumber,' ',LCASE(iu.EnglishName)) LIKE '%".strtolower($keyword)."%'
				OR CONCAT(LCASE(iu.ClassName),iu.ClassNumber,' ',iu.ChineseName) LIKE BINARY '%".strtolower($keyword)."%'
			)
		    AND ps.IsSuspend = 0 ORDER BY iu.ClassNumber ASC";

		$user_ids = $this->returnVector($sql);

		$users=array();

		foreach ($user_ids as $user_id){
			$user=$this->getUserInfo($user_id);
			$user['user_id'] = $user_id;
			$users[]=$user;
		}

		return $users;

	}
	public function getTemplates($creator_user_id=false){

		if (self::$is_competition){
			$user_group_only = "inner join user_group g on f.User_id = g.user_id
		inner join grouping_function gf on gf.group_id = g.group_id and function_type = 'PORTw' and function_id = '".$this->web_portfolio_id."'";
		}

		$sql = "SELECT distinct(f.FileID) as template_id, left(f.Title,length(f.Title)-5) as title, f.Description as description
		FROM eclass_file f
		$user_group_only
		WHERE f.IsSuspend <> 1
		AND f.Category='0'
		AND f.isDir=0
		AND f.Title LIKE '%.html%'
		AND f.VirPath='/TEMPLATES/'
		ORDER BY f.Title ASC ";

		$templates = $this->returnArray($sql);

		foreach ($templates as $i=>$template){
			$templates[$i]['url'] = "/eclass40/files/".$this->db."/notes/".$template['Location']."/".$template['Title'];

		}
		return $templates;

	}
	private function getTemplateParentDir(){
		$sql = "SELECT FileID FROM eclass_file
			WHERE title='TEMPLATES' AND VirPath IS NULL AND IsDir=1 AND Category=0";

		$folder_id = current($this->returnVector($sql));echo mysql_error();
		if (!$folder_id) {

			$fm = new fileManager($this->course_id, 0, "");

			$fm->memberType = $this->memberType;
			$fm->permission = "AW";

			$folder_id = $fm->createFolderDB('TEMPLATES', "", "", 0);

		}

		return $folder_id;
	}
	private function getTemplatePath($template_id){

		global $eclass_root;

		$sql = "SELECT Location, Title, Category FROM eclass_file WHERE FileID='$template_id' ";

		$template = current($this->returnArray($sql));
		if(!file_exists("$eclass_root/files/".$this->db."/notes/".$template['Location'])){
			$fm = new fileManager($this->course_id, 0, $template['ParentDirID']);
			$fm->createFolder("$eclass_root/files/".$this->db."/notes/".$template['Location']);
		}
		return "$eclass_root/files/".$this->db."/notes/".$template['Location']."/".$template['Title'];

	}
	public function createTemplate($data){

		$folder_id 	= $this->getTemplateParentDir();//get fileID for template folder

		$sql = "select Location from eclass_file where FileID = '".$folder_id."'";

		$location 	= current($this->returnVector($sql)).'/TEMPLATES';
		$length 	= strlen($data['content']);
		$title		= $data['title'].'.html';

		$sql = "select count(*) from eclass_file
		where Title = '$title' and ParentDirID = '".$folder_id."'";

		if (current($this->returnVector($sql))>0) return -1;//same template name exists

		$sql = "insert into eclass_file (
		User_id, 	UserName, 	UserEmail, 	memberType,
		Title, 	Location, 	VirPath, 	Category,
		Size, 	ParentDirID, 	IsDir, 		LastModifiedBy,
		Permission, DateInput,	DateModified
		)select
		u.user_id, 		u.firstname as UserName, 	u.user_email,	u.memberType,
		'$title',		'$location',			'/TEMPLATES/',	0 as Category,
		'$length' as Size, 	'$folder_id' as ParentDirID, 	0 as IsDir, 	u.firstname as LastModifiedBy,
		'AR',		now(),				now()
		from usermaster u
		where u.user_id = '".$this->current_user_id."'";

		$this->db_db_query($sql);

		$template_id = mysql_insert_id();

		$this->updateTemplate($template_id, $data);

		return mysql_insert_id();

	}
	public function updateTemplate($template_id, $data){

		$fm = new fileManager($this->course_id, 0, $folder_id);
		$fm->writeFile($data['content'], $this->getTemplatePath($template_id));

		return $template_id;
	}
	public function getTemplateContent($template_id){

		global $eclass40_filepath;

		$template_path = $this->getTemplatePath($template_id);

		if (file_exists($template_path)){
			//strips style and scripts
			$template_content = file_get_contents($template_path);
			$template_content = preg_replace("/((<[\s\/]*script\b[^>]*>)([^>]*)(<\/script>))/i",'',$template_content);
			$template_content = preg_replace("/((<[\s\/]*style\b[^>]*>)([^>]*)(<\/style>))/i",'',$template_content);
			$template_content = preg_replace("/(<[\s\/]*link\b[^>]*>)/i",'',$template_content);

		}

		return $template_content;

	}
	public function is_parent(){
		global $ck_memberType,$ck_current_children_id;
		return $ck_memberType=='P'&&$this->user_intranet_id==$ck_current_children_id;
	}
	/*** Portfolio Settings Data ***/
	public function isPortfolioReadable($user_permission=false){
		if (!$user_permission) $user_permission = $this->user_permission;

		switch($this->mode){

			case 'draft': //only member students and portfolio is within time,  or teacher member
				return $this->user_permission == 0;//only teacher with permissions
			case 'prototype':
				return $this->memberType=='T' && $this->user_permission == 0;//only teacher member
			case 'publish':
				if($this->is_parent()){ //owner's parent can view its LP
					return true;
				}else{
					$sql = "select count(*) from portfolio_publish
					where status>='".$user_permission."' and ordering <> -1 and ".$this->portfolio_cond;
					return current($this->returnVector($sql));//check if published with enough permission
				}
			default:
				return false;
		}

	}
	public function isPortfolioEditable(){

		switch($this->mode){

			case 'draft': //only member students and portfolio is within time,  or teacher member
				return $this->memberType=='T'? $this->is_portfolio_member : $this->user_permission == 0 && $this->portfolio_info['within_time'] && $this->is_portfolio_member;
			case 'prototype':
				return $this->memberType=='T' && $this->is_portfolio_member;//only teacher member
			default:
				return false;

		}
	}
	public function getPortfolioInfo(){

		return $this->portfolio_info;

	}
	public function updatePortfolioInfo($data){

		if(self::isFromKIS()){
			$sql = "UPDATE web_portfolio SET
			title = '".$data['title']."',
			instruction = '".$data['instruction']."',
			status = '".$data['status']."',
			starttime = ".$data['starttime'].",
			deadline = ".$data['endtime'].",
			folder_size = ".$data['sizeMax'].",
			form_id = ".$data['form_id'].",
			modified = now()
			WHERE web_portfolio_id = '".$this->web_portfolio_id."'";
		}else{
			$sql = "UPDATE web_portfolio SET
			title = '".$data['title']."',
			instruction = '".$data['instruction']."',
			status = '".$data['status']."',
			starttime = ".$data['starttime'].",
			deadline = ".$data['endtime'].",
			folder_size = ".$data['sizeMax'].",
			modified = now()
			WHERE web_portfolio_id = '".$this->web_portfolio_id."'";
		}

		$this->db_db_query($sql);

		//Handle existing webportfolio folder size
		$title = 'student_u%_wp'.$this->web_portfolio_id;

		$sql = "UPDATE eclass_file SET SizeMax='".$data['sizeMax']."' WHERE Title like '".$data['title']."'";

		$this->db_db_query($sql);
	}
	public function getPortfolioConfig(){

		global $eclass_root;

		$sql = "select title as template_selected, description as banner_status, url as banner_url, content, readflag
		from ".$this->portfolio_table." where ".$this->portfolio_cond." and ordering=-1 and status>=0";

		$config = current($this->returnArray($sql));
		if (!$config){

			$config = array('template_selected'=>'01', 'banner_status'=>'1', 'banner_url'=>'', 'content'=>'1');//defaults
			$sql = "insert into ".$this->portfolio_table."(ordering, user_id, web_portfolio_id, title, description, content, status, readflag, inputdate, modified)
		    values (-1, ".$this->user_id.", ".$this->web_portfolio_id.", '01', '2', '1', 2, ';".$this->current_user_intranet_id.";', now(), now())";
			$this->db_db_query($sql);

		}

		$sql = "select notes_published from user_config where ".$this->portfolio_cond." order by notes_published desc limit 1";

		$config['published'] = current($this->returnVector($sql));
		$config[$this->mode=='prototype'?'custom_root':'allow_like'] = $config['content'];
		$LPSetting = $this->getLPConfigSetting();
		$config['allow_like'] = $LPSetting["disable_facebook"]?0:$config['allow_like'];
		//$config['user_folder_id'] = $this->getUserFolder();
		//$config['user_folder_name'] = $this->user_folder_name;

		return $config;
	}
	public function updatePortfolioConfig($data){

		$sql = "update ".$this->portfolio_table." set
		title='".$data['template_selected']."',
		description = '".$data['banner_status']."',
		content = '".$data['custom_root']."',
		url = '".$data['banner_url']."',
		modified=now()
		where ".$this->portfolio_cond." and ordering=-1 and status>=0";

		$this->db_db_query($sql);

		return mysql_affected_rows()>0;

	}
	public function isUserInReadflag($readflag, $user_intranet_id=false){

		if (!$user_intranet_id) $user_intranet_id = $this->current_user_intranet_id;

		$flag = ';'.$user_intranet_id.';';

		return substr_count($readflag,$flag)>0;

	}
	public function setReadflag($mark_read=true){

		if ($mark_read){

			$sql = "select readflag
		    from ".$this->portfolio_table." where ".$this->portfolio_cond." and ordering=-1 and status>=0";

			$readflag = current($this->returnVector($sql));

			if (!$this->isUserinReadflag($readflag)){
				$readflag.=';'.$this->current_user_intranet_id.';';
			}

		}else{
			$readflag = ';'.$this->current_user_intranet_id.';';
		}

		$sql = "update ".$this->portfolio_table." set
		readflag='$readflag', modified = now()
		where ".$this->portfolio_cond." and ordering=-1 and status>=0";

		$this->db_db_query($sql);
	}

	/****** Portfolio Elements ******/

	public function hasSuperPermission($id, $is_weblog=false){

		if ($this->mode=='prototype') return 2;//user is teacher

		if ($id=='banner'){//banner_status, i.e. can change banner or not

			$sql = "select if(p.description<2,0,1) from portfolio_draft d
		    inner join portfolio_prototype p on p.portfolio_prototype_id = d.portfolio_prototype_id
		    where p.ordering='-1'
			and d.web_portfolio_id = '".$this->web_portfolio_id."' and d.user_id = '".$this->user_id."'";

		}else if ($id==-1){//custom

			$sql = "select p.content from portfolio_draft d
		    inner join portfolio_prototype p on p.portfolio_prototype_id = d.portfolio_prototype_id
		    where p.ordering='-1'
			and d.web_portfolio_id = '".$this->web_portfolio_id."' and d.user_id = '".$this->user_id."'";

		}else{


			$sql = "select count(*) from portfolio_draft d
		    left join portfolio_prototype p on p.portfolio_prototype_id = d.portfolio_prototype_id
		    where (p.status >= ".($is_weblog?"1":"2")." or d.portfolio_prototype_id is null) /* its referring prototype has custom permission, or it is customized element (reference is null) */
			and (d.starttime < now() or d.starttime=0 or d.starttime is null)
			and (d.endtime > now() or d.endtime=0 or d.endtime is null)
			and d.portfolio_draft_id='".$id."' and d.web_portfolio_id = '".$this->web_portfolio_id."' and d.user_id = '".$this->user_id."'";

		}

		return current($this->returnVector($sql));//student allowed to change
	}

	public function updateElement($id, $data){

		foreach($data as $key=>$value){//serialize data
			$fields.=$key."='".$value."',";
		}
		$sql = "update ".$this->portfolio_table." set
		$fields
		modified=now()
		where ".$this->portfolio_table."_id = '".$id."' $within_time and ".$this->portfolio_cond;
		$this->db_db_query($sql);

		if (mysql_affected_rows()<=0) return false;

		if($this->mode=='prototype'){//Update student draft starttime and endtime
			$sql = "UPDATE portfolio_draft SET starttime = '".$data['starttime']."',  endtime = '".$data['endtime']."' WHERE portfolio_prototype_id = '".$id."'";
			$this->db_db_query($sql);
		}

		$this->setReadflag(false);

		return $id;

	}

	public function createElement($parent_id=-1,$data){

		foreach ($data as $key=>$value){//serialize data

			$fields .= $key.",";
			$values .= "'".$value."',";

		}

		$sql = "select max(ordering) from ".$this->portfolio_table." where parent_id=$parent_id and ".$this->portfolio_cond."
		group by parent_id";

		$ordering = current($this->returnVector($sql))+1;


		$sql = "insert into ".$this->portfolio_table."
		(parent_id, ordering, $fields web_portfolio_id, user_id, inputdate, modified)
		values
		($parent_id,  $ordering, $values ".$this->web_portfolio_id.", ".$this->user_id.", now(), now())";

		$this->db_db_query($sql) ;
		$id = mysql_insert_id();
		$this->setReadflag(false);

		return $id;
	}
	public function getElementByPrototypeId($id, $is_removed=false){

		$status = $is_removed?-1:$this->user_permission;

		$cond = $id==-1?"ordering = -1 ":"portfolio_prototype_id = '".$id."'";

		//no limit time if the time is not set
		$within_time = "(starttime<now() or starttime=0 or starttime is null)
		and (endtime>now() or endtime=0 or endtime is null)";

		$sql = "select ".$this->portfolio_table."_id as id, parent_id, ordering, title, status, modified, description, content, url, type, if(starttime=0,'',starttime) as starttime, if(endtime=0,'',endtime) as endtime, if($within_time, 1, 0) as within_time
		from ".$this->portfolio_table."
		where $cond and ".$this->portfolio_cond." and status >= $status
		limit 1";
		return current($this->returnArray($sql));
	}
	public function getElement($id, $is_removed=false){

		$status = $is_removed?-1:$this->user_permission;

		$cond = $id==-1?"ordering = -1 ":$this->portfolio_table."_id = '".$id."'";

		//no limit time if the time is not set
		$within_time = "(starttime<now() or starttime=0 or starttime is null)
		and (endtime>now() or endtime=0 or endtime is null)";

		$sql = "select ".$this->portfolio_table."_id as id, parent_id, ordering, title, status, modified, description, content, url, type, if(starttime=0,'',starttime) as starttime, if(endtime=0,'',endtime) as endtime, if($within_time, 1, 0) as within_time
		from ".$this->portfolio_table."
		where $cond and ".$this->portfolio_cond." and status >= $status
		limit 1";
		return current($this->returnArray($sql));
	}

	public function removeElement($id){

		$children = $this->getElementChildren($id);

		foreach ($children as $child){
			$this->removeElement($child["id"]);
		}

		$sql = "update ".$this->portfolio_table." set status=-1, modified=now() where
		".$this->portfolio_table."_id='".$id."'
		and ".$this->portfolio_cond;

		$this->db_db_query($sql);

		$this->setReadflag(false);

		return mysql_affected_rows()>0;
	}

	public function getElementChildren($id=-1, $is_weblog_item=false){

		$sql = "select ".$this->portfolio_table."_id as id, title, status, type
		from ".$this->portfolio_table."
		where ".$this->portfolio_cond."
		and ".($is_weblog_item?" type = 'wbitem'":"(type <> 'wbitem' or type is null)" )."
		and parent_id='".$id."' and status >= ".$this->user_permission."
		order by ".($is_weblog_item?'url desc':'ordering asc');

		return $this->returnArray($sql);
	}

	public function reorderElements($parent_id, $ids){

		if (!$ids) $ids=array();

		foreach (array_values($ids) as $i=>$id){

			$num = $i+1;
			$sql = "update ".$this->portfolio_table." set ordering=$num, parent_id = '".$parent_id."'
		    where ".$this->portfolio_table."_id='".$id."'
		    and ".$this->portfolio_cond;//no need to update modified
			$this->db_db_query($sql);

		}

		$this->setReadflag(false);
		return true;
	}
	public function isElementUpdated($id){

		$sql = "select p.status
		from portfolio_draft d
		inner join portfolio_prototype p on p.portfolio_prototype_id = d.portfolio_prototype_id
		where p.modified > d.inputdate and d.portfolio_draft_id = '".$id."' AND (p.status != -1 AND p.content != d.content OR p.status = -1)
		";
		return current($this->returnVector($sql));
	}

	public function syncElement($id,$reset=false){
		$sql = "update portfolio_draft d
		inner join portfolio_prototype p on p.portfolio_prototype_id = d.portfolio_prototype_id
		set
		d.title = p.title,
		d.description = p.description,
		d.url = p.url,
		d.content = p.content,
		d.type = p.type,
		d.starttime = p.starttime,
		d.endtime = p.endtime,
		d.inputdate = now(),
		d.modified = now()
		where d.portfolio_draft_id = '".$id."'";
		$sql .= (!$reset)? " and p.modified > d.inputdate":"";
		$this->syncLPFile();
		if($reset){
			$this->updateResetLog($id);
		}
		return $this->db_db_query($sql);

	}
	public function updateResetLog($portfolio_draft_id){
		global $ck_user_id;
		$sql = "INSERT INTO portfolio_reset_log (
			     portfolio_draft_id,parent_id,ordering,portfolio_prototype_id,
			     web_portfolio_id,user_id,title,description,url,content,readflag,
				 status,type,starttime,endtime,inputdate,modified,reset_by,reset_date)
				SELECT
				 portfolio_draft_id,parent_id,ordering,portfolio_prototype_id,
			     web_portfolio_id,user_id,title,description,url,content,readflag,
				 status,type,starttime,endtime,inputdate,modified,'".$ck_user_id."',NOW()
				FROM
					portfolio_draft
				WHERE
					portfolio_draft_id = '".$portfolio_draft_id."'
		";
		$this->db_db_query($sql);
	}
	public function getRemovedElements(){

		$sql = "select ".$this->portfolio_table."_id as id, title, modified, description, content, url , type
		from ".$this->portfolio_table."
		where ".$this->portfolio_cond." and status = -1 and ordering <> -1 and (type<>'weblog' or type is null) order by modified desc";

		return $this->returnArray($sql);
	}

	public function getAllElements($id=-1, $elements=array(), $level=-1){

		$level++;

		$weblogs  	= $this->getElementChildren($id, true);
		$children 	= $this->getElementChildren($id);

		foreach($weblogs as $weblog){

			$weblog['level'] = $level;

			$elements[] = $weblog;

		}

		foreach($children as $child){

			$child['level'] = $level;

			$elements[] = $child;
			$elements = $this->getAllElements($child['id'], $elements, $level);
		}

		return $elements;

	}

	/****** Portfolios Listing ******/
	public function getAllPublishedPortfolios($keyword='',$group_id=false, $offset, $amount){

		global $intranet_db, $sys_custom;

		if($sys_custom['lp_stem_learning_scheme']){
		    $statusSQL = ' AND pc.status > 0'; // Status public
		}

		if (is_array($group_id) && sizeof($group_id)>0)
		{
			$group 		=  " and g.group_id in (".implode(", ", $group_id).")";
		} else
		{
			$group 		= $group_id? " and g.group_id = '$group_id'":' ';
		}
		$within_time 	= "(p.starttime < now() or p.starttime = 0 or p.starttime is null) and (p.deadline>now() or p.deadline=0 or p.deadline is null)";

		$sql = "select SQL_CALC_FOUND_ROWS c.user_id, g.group_id, gp.group_name, iu.UserID as user_intranet_id, iu.EnglishName as name, iu.ClassName as class, p.web_portfolio_id, p.title, p.version, p.starttime, p.deadline, if($within_time, 1,0) as within_time, count(distinct(r.review_comment_id)) as comments_count,
		coalesce(pc.title, c.template_selected,'01') as theme, c.notes_published as published, pc.readflag as readflag

		from web_portfolio p
		inner join user_config c on p.web_portfolio_id = c.web_portfolio_id and c.notes_published is not null
		inner join portfolio_publish pc on pc.web_portfolio_id = p.web_portfolio_id and c.user_id = pc.user_id and pc.ordering = -1
		inner join usermaster u on c.user_id = u.user_id and u.memberType = 'S' AND (u.status IS NULL OR u.status <> 'deleted')
		inner join user_group g on g.user_id = c.user_id
		inner join grouping gp on g.group_id = gp.group_id
		inner join {$intranet_db}.INTRANET_USER iu on iu.UserEmail = u.user_email
		left join review_comment r on p.web_portfolio_id = r.web_portfolio_id and r.iPortfolio_user_id=c.user_id
		where p.status = 1
		and (iu.EnglishName like '%$keyword%' or iu.ChineseName like '%$keyword%' or iu.ClassName like '%$keyword%') $group $statusSQL
		group by u.user_id, p.web_portfolio_id
		order by published desc";

		if ($amount != 'all') $sql.=" limit $offset, $amount";
		$items = $this->returnArray($sql);echo mysql_error();
		$total = current($this->returnVector('select found_rows()'));

		return array($total, $items);
	}


	public function getAllPortfolios($keyword='', $sortby='modified', $order='desc', $offset, $amount){

		global $intranet_db;

		$sql = "SELECT count(distinct u.user_id)
		FROM usermaster u
		inner join {$intranet_db}.INTRANET_USER iu on iu.UserEmail = u.user_email
		WHERE u.memberType = 'S' AND (u.status IS NULL OR u.status <> 'deleted')";

		$total_students = current($this->returnVector($sql));
		if(
		    !self::$is_admin &&
		    (
		        self::$is_competition ||
		        self::$is_stem_learning_scheme
	        )
	    ){
		    $groups = "and g.group_id in (".implode(',',$this->getUserPortfolioGroups()).")";
		}else{
		    $groups = '';
		}

		$conds = '';
		if($_SESSION["platform"]=="KIS"){
			$userInfo = self::getUserInfo();
			switch ($userInfo['KIS_teacher_type']){

				case 'FormTeacher':
					$form = array();
					foreach($userInfo["KIS_specific_form"] as $key){
						$form[] = $key["YearID"];
					}
					$conds = " and p.form_id in (".implode(',',$form).")";
					break;
				case 'ClassTeacher':
					$yearClass = array();
					foreach($userInfo["KIS_class_info"] as $key){
						$yearClass[] = $key["YearClassID"];
					}

					$sql = "SELECT DISTINCT g.group_id

					from web_portfolio p
					inner join grouping_function f
					on f.function_id = p.web_portfolio_id and f.function_type = 'PORTw'
					inner join user_group g
					on g.group_id = f.group_id
					inner join usermaster u
					on g.user_id = u.user_id and u.memberType = 'S' AND (u.status IS NULL OR u.status <> 'deleted')
					inner join
					{$intranet_db}.INTRANET_USER iu
					on iu.UserEmail = u.user_email
					inner join {$intranet_db}.YEAR_CLASS_USER ycu
					ON ycu.UserID = iu.UserID
					inner join {$intranet_db}.YEAR_CLASS yc
					ON ycu.YearClassID = yc.YearClassID  AND yc.YearClassID in (".implode(',',$yearClass).")";

					$relatedGroupID = $this->returnVector($sql);

					$conds = " and g.group_id in (".implode(',',$relatedGroupID).")";
					break;

				case 'FormClassTeacher':
					$form = array();
					foreach($userInfo["KIS_specific_form"] as $key){
						$form[] = $key["YearID"];
					}
					$conds = " and (p.form_id in (".implode(',',$form).")";

					$yearClass = array();
					foreach($userInfo["KIS_class_info"] as $key){
						$yearClass[] = $key["YearClassID"];
					}

					$sql = "SELECT DISTINCT g.group_id

					from web_portfolio p
					inner join grouping_function f
					on f.function_id = p.web_portfolio_id and f.function_type = 'PORTw'
					inner join user_group g
					on g.group_id = f.group_id
					inner join usermaster u
					on g.user_id = u.user_id and u.memberType = 'S' AND (u.status IS NULL OR u.status <> 'deleted')
					inner join
					{$intranet_db}.INTRANET_USER iu
					on iu.UserEmail = u.user_email
					inner join {$intranet_db}.YEAR_CLASS_USER ycu
					ON ycu.UserID = iu.UserID
					inner join {$intranet_db}.YEAR_CLASS yc
					ON ycu.YearClassID = yc.YearClassID  AND yc.YearClassID in (".implode(',',$yearClass).")";

					$relatedGroupID = $this->returnVector($sql);

					$conds .= " or g.group_id in (".implode(',',$relatedGroupID)."))";

					break;

				case 'NormalTeacher':
					$conds .= " and p.web_portfolio_id is NULL";
					break;
			}
		}

		if(self::$is_stem_learning_scheme && !self::$is_admin){
		    $conds .= " and p.status = 1";
		}

		$sql = "select SQL_CALC_FOUND_ROWS p.title, p.web_portfolio_id, p.version, p.starttime, p.deadline, p.status, coalesce(pc.title, '01') as theme,
		greatest(ifnull(pc.modified,0),p.modified) as modified, datediff(curdate(),greatest(ifnull(pc.modified,0),p.modified)) as modified_days,
		if(max(g.group_id) is not null, count(distinct iu.UserID),$total_students) as student_count
		from web_portfolio p
		left join grouping_function f on f.function_id = p.web_portfolio_id and f.function_type = 'PORTw'
		left join user_group g on g.group_id = f.group_id
		left join usermaster u on g.user_id = u.user_id and u.memberType = 'S' AND (u.status IS NULL OR u.status <> 'deleted')
		left join {$intranet_db}.INTRANET_USER iu on iu.UserEmail = u.user_email
		left join portfolio_prototype pc on pc.web_portfolio_id = p.web_portfolio_id and pc.ordering = -1 /* drafting theme */
		where p.title like '%$keyword%' $groups $conds
		group by p.web_portfolio_id
		order by $sortby $order, p.inputdate desc";

		if ($amount != 'all') $sql.=" limit $offset, $amount";

		$items = $this->returnArray($sql);
		$total = current($this->returnVector('select found_rows()'));

		return array($total, $items);

	}
	public function getPublishedPortfolioUserClassListByPortfolioId(){

		global $intranet_db,$ck_current_academic_year_id;

		$sql = "select count(f.function_id)
		from web_portfolio p
		inner join grouping_function f on f.function_id = p.web_portfolio_id and f.function_type = 'PORTw'
		where p.web_portfolio_id = '".$this->web_portfolio_id."'";

		$total_students = current($this->returnVector($sql));
		$sql = "select distinct(iu.UserID) as UserID
		from web_portfolio p ";

		if ($total_students){

			$sql .= "inner join grouping_function f on f.function_id = p.web_portfolio_id and f.function_type = 'PORTw'
		    inner join user_group g on g.group_id = f.group_id
		    inner join usermaster u on g.user_id = u.user_id and u.memberType = 'S' AND (u.status IS NULL OR u.status <> 'deleted') ";

			$groups = (self::$is_competition&&!self::$is_admin)?"and g.group_id in (".implode(',',$this->getUserPortfolioGroups()).")":'';

		}else{
			$sql .= "cross join usermaster u on u.memberType = 'S' AND (u.status IS NULL OR u.status <> 'deleted') ";
		}

		$sql .= "
		INNER JOIN	{$intranet_db}.INTRANET_USER AS iu ON iu.UserEmail = u.user_email
		where p.status = 1 $groups
		and p.web_portfolio_id =".$this->web_portfolio_id." group by u.user_id";
		$UserIDs = $this->returnVector($sql);

		# Get data from db
		if ($_SESSION['intranet_session_language'] == 'en')
			$titleField = 'yc.ClassTitleEN';
			else
				$titleField = 'yc.ClassTitleB5';
				$sql =	"
				SELECT
				yc.YearClassID as ClassID,
				$titleField as ClassTitle
				FROM
				{$intranet_db}.INTRANET_USER AS iu
				INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
				ON ycu.UserID = iu.UserID
				INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
				ON ycu.YearClassID = yc.YearClassID
				WHERE
				iu.UserID IN ('".implode("','",$UserIDs)."') AND
				iu.RecordType = '2' AND
				iu.RecordStatus = '1' AND
				yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
			GROUP BY
				ClassTitle
		";
				$data = array();
				$data['classes'] = $this->returnArray($sql);
				return $data;

	}

	public function getPublishedPortfolioUserListByClassId($ClassId){
		global $intranet_db,$intranet_session_language;
		if(empty($ClassId)) return false;

		$sql = "select count(f.function_id)
		from web_portfolio p
		inner join grouping_function f on f.function_id = p.web_portfolio_id and f.function_type = 'PORTw'
		where p.web_portfolio_id = '".$this->web_portfolio_id."'";

		$total_students = current($this->returnVector($sql));
		$sql = "select distinct(iu.UserID) as UserID, ".getNameFieldByLang2("iu.",$intranet_session_language)." as name
			from web_portfolio p ";

		if ($total_students){

			$sql .= "inner join grouping_function f on f.function_id = p.web_portfolio_id and f.function_type = 'PORTw'
			    inner join user_group g on g.group_id = f.group_id
			    inner join usermaster u on g.user_id = u.user_id and u.memberType = 'S' AND (u.status IS NULL OR u.status <> 'deleted') ";

			$groups = (self::$is_competition&&!self::$is_admin)?"and g.group_id in (".implode(',',$this->getUserPortfolioGroups()).")":'';

		}else{
			$sql .= "cross join usermaster u on u.memberType = 'S' AND (u.status IS NULL OR u.status <> 'deleted') ";
		}

		$sql .= "
		INNER JOIN	{$intranet_db}.INTRANET_USER AS iu ON iu.UserEmail = u.user_email
		INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu ON ycu.UserID = iu.UserID
		where p.status = 1 $groups
		and p.web_portfolio_id =".$this->web_portfolio_id." AND ycu.YearClassID = '".$ClassId."' group by u.user_id
			ORDER BY ".getClassNumberForOrderBy("iu.")."
		";
		return $this->returnArray($sql);
	}
	public function getPortfolioUsers($keyword='', $publishStatus='all', $offset, $amount, $sortBy = null, $order = 'asc'){

		global $intranet_db;

		/*
		 * Three statuses defined in iportfolo_lang - all / published / notPublished
		 */
		switch($publishStatus){
		    case "published":
		        $published 	= "and c.notes_published is not null";
		        break;
		    case "notPublished":
		        $published 	= "and c.notes_published is  null";
		        break;
		    default:
		        $published 	= '';

		}

		$keyword	= $keyword? "and (iu.EnglishName like '%$keyword%' or iu.ChineseName like '%$keyword%' or iu.ClassName like '%$keyword%')":"";
		$within_time 	= "(p.starttime<now() or p.starttime=0 or p.starttime is null) and (p.deadline>now() or p.deadline=0 or p.deadline is null)";

		$sql = "select count(f.function_id)
		from web_portfolio p
		inner join grouping_function f on f.function_id = p.web_portfolio_id and f.function_type = 'PORTw'
		where p.web_portfolio_id = '".$this->web_portfolio_id."'";

		$total_students = current($this->returnVector($sql));

		if (!$this->portfolio_info['version']){

			$columns = "coalesce(c.template_selected, '01') as theme, 0 modified ";

		}else{

			$columns = "coalesce(pc.title, c.template_selected, pp.title,'01') as theme, pc.readflag as readflag, pc.modified as modified ";
			$sub_sql = "
		left join portfolio_draft pc on pc.user_id = u.user_id and pc.web_portfolio_id = p.web_portfolio_id and pc.ordering = -1 and pc.status >= 0/* drafting theme */
		left join portfolio_prototype pp on pp.web_portfolio_id = p.web_portfolio_id and pp.ordering = -1 /* prototype theme */";
		}

		$sql = "select SQL_CALC_FOUND_ROWS distinct(u.user_id) as user_id, iu.EnglishName as name, iu.ClassName as class, p.web_portfolio_id, p.title, p.version, if($within_time, 1,0) as within_time, count(distinct(r.review_comment_id)) as comments_count, $columns, c.notes_published as published
		from web_portfolio p ";

		if ($total_students || self::$is_competition || self::$is_stem_learning_scheme ){

			$sql .= "inner join grouping_function f on f.function_id = p.web_portfolio_id and f.function_type = 'PORTw'
		    inner join user_group g on g.group_id = f.group_id
		    inner join usermaster u on g.user_id = u.user_id and u.memberType = 'S' AND (u.status IS NULL OR u.status <> 'deleted') ";

			if(
			    !self::$is_admin &&
			    (
			        self::$is_competition ||
			        self::$is_stem_learning_scheme
		        )
		    ){
			    $groups = "and g.group_id in (".implode(',',$this->getUserPortfolioGroups()).")";
			}else{
			    $groups = '';
			}

		}else{
			$sql .= "cross join usermaster u on u.memberType = 'S' AND (u.status IS NULL OR u.status <> 'deleted') ";
		}

		$sql .= "left join user_config c on p.web_portfolio_id = c.web_portfolio_id and u.user_id = c.user_id
		inner join {$intranet_db}.INTRANET_USER iu on iu.UserEmail = u.user_email
		left join review_comment r on p.web_portfolio_id = r.web_portfolio_id and r.iPortfolio_user_id=u.user_id
		$sub_sql
		where p.status = 1 $published $groups $keyword
		and p.web_portfolio_id =".$this->web_portfolio_id." group by u.user_id";

		/* Sorting */
		$sortBySql = '';
	    if($sortBy === 'notes_published'){
	       $sortBySql .= 'c.notes_published ' . $order . ', ';
	    }else if($sortBy === 'className'){
	        $sortBySql .= 'iu.ClassName ' . $order . ', ';
            $sortBySql .= 'iu.ClassNumber ' . $order . ', ';
	        $sortBySql .= 'c.notes_published desc, ';
	    }else{
	        $sortBySql .= 'c.notes_published desc, ';
	    }
	    $sortBySql = ' order by ' . $sortBySql . ' modified desc, p.inputdate desc, iu.EnglishName asc';

	    $sql .= $sortBySql;

		if ($amount != 'all') $sql.=" limit $offset, $amount";

		$items = $this->returnArray($sql);
		$total = current($this->returnVector('select found_rows()'));

		return array($total, $items);
	}

	public function getUserPortfolios($keyword='', $offset, $amount, $conds = ''){

		$within_time = "(p.starttime<now() or p.starttime=0 or p.starttime is null)
		and (p.deadline>now() or p.deadline=0 or p.deadline is null)";

		$keyword = $keyword? " and p.title like '%$keyword%' ":'';
		$LPSetting = $this->getLPConfigSetting();
		$disable_facebook = $LPSetting['disable_facebook'];
		$allow_like_sql = $disable_facebook?"0 as allow_like,":"pc.content as allow_like,";
		if(self::isFromKIS()){
			$sql = "select SQL_CALC_FOUND_ROWS p.title, p.web_portfolio_id, p.version, ".$allow_like_sql." if($within_time, 1,0) as within_time, count(distinct(r.review_comment_id)) as comments_count, coalesce(pc.title, c.template_selected,'01') as theme, c.notes_published as published, coalesce(pc.modified,max(n.modified)) as modified, p.deadline
			from web_portfolio p ";
		}else{
			$sql = "select SQL_CALC_FOUND_ROWS p.title, p.web_portfolio_id, p.version, ".$allow_like_sql." if($within_time, 1,0) as within_time, count(distinct(r.review_comment_id)) as comments_count, coalesce(pc.title, c.template_selected,'01') as theme, c.notes_published as published, coalesce(pc.modified,max(n.modified)) as modified
			from web_portfolio p ";
		}
		$sql .= "left join user_config c on p.web_portfolio_id = c.web_portfolio_id and c.user_id = ".$this->current_user_id."
		left join notes_student n on n.user_id = c.user_id and n.web_portfolio_id = p.web_portfolio_id
		left join grouping_function f on p.web_portfolio_id = f.function_id and f.function_type = 'PORTw'
		left join user_group as g on f.group_id = g.group_id
		left join review_comment r on p.web_portfolio_id = r.web_portfolio_id and r.iPortfolio_user_id=c.user_id
		left join portfolio_draft pc on pc.user_id = c.user_id and pc.web_portfolio_id = p.web_portfolio_id and pc.ordering = -1 /* drafting theme */
		where p.status = 1 $conds
		and ((f.function_id is not null and g.user_id = ".$this->current_user_id.")
		    ".(self::$is_competition?"":"or f.function_id is null")."
		    )

		    $keyword
		    group by p.web_portfolio_id
		    order by published desc,  modified desc, p.inputdate desc";

		    if ($amount != 'all') $sql.=" limit $offset, $amount";

		    $items = $this->returnArray($sql);
		    $total = current($this->returnVector('select found_rows()'));

		    return array($total, $items);

	}

	public function getFriendPortfolios($offset, $amount, $friend_intranet_ids=''){
		global $eclass_db, $intranet_db;
		$LPSetting = $this->getLPConfigSetting();
		$disable_facebook = $LPSetting['disable_facebook'];
		$allow_like_sql = $disable_facebook?"0 as allow_like":"pc.content as allow_like";
		$sql = "select YearClassID from {$intranet_db}.YEAR_CLASS_USER where UserID='".$this->current_user_intranet_id."'";
		$class_id = $this->returnVector($sql);

		$sql = "SELECT UserID FROM user_friend WHERE FriendID = '".$this->current_user_intranet_id."'";
		$friend_ids = $this->returnVector($sql);
		$frd_cond = "";
		if(count($friend_ids)>0){
			$frd_cond .= " or s.UserID IN ('".implode("','",$friend_ids)."') ";
		}
		$sql = "select SQL_CALC_FOUND_ROWS c.user_id, p.title, p.web_portfolio_id, p.version, s.CourseUserID, coalesce(c.template_selected,'01') as theme, ".$allow_like_sql."
		from web_portfolio p
		inner join user_config c on p.web_portfolio_id = c.web_portfolio_id and c.allow_review >= 1 AND c.user_id<>".$this->current_user_id."
		left join notes_student n on n.user_id = c.user_id and n.web_portfolio_id = p.web_portfolio_id
		inner join {$eclass_db}.PORTFOLIO_STUDENT s on s.CourseUserID = c.user_id
		left join portfolio_publish pc on pc.user_id = c.user_id and pc.web_portfolio_id = p.web_portfolio_id and pc.ordering = -1
		inner join {$intranet_db}.INTRANET_USER u on s.UserID = u.UserID
		inner join {$intranet_db}.YEAR_CLASS_USER ycu on u.UserID = ycu.UserID
		inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
		where
		s.UserKey IS NOT NULL AND
		s.IsSuspend = 0 AND
		(
		yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' and yc.YearClassID in (".implode(",",$class_id).")

		    ".(self::$is_competition?"":$frd_cond)."
		)
		".(!empty($friend_intranet_ids)?" AND u.UserID in (".$friend_intranet_ids.")":"")."
		group by p.web_portfolio_id, c.user_id
		order by c.notes_published desc";

		//	$sql = "select SQL_CALC_FOUND_ROWS c.user_id, p.title, p.web_portfolio_id, p.version, s.CourseUserID, coalesce(c.template_selected,'01') as theme, ".$allow_like_sql."
		//		from web_portfolio p
		//		inner join user_config c on p.web_portfolio_id = c.web_portfolio_id and c.allow_review >= 1 AND c.user_id<>".$this->current_user_id."
		//		left join notes_student n on n.user_id = c.user_id and n.web_portfolio_id = p.web_portfolio_id
		//		inner join {$eclass_db}.PORTFOLIO_STUDENT s on s.CourseUserID = c.user_id
		//		left join portfolio_publish pc on pc.user_id = c.user_id and pc.web_portfolio_id = p.web_portfolio_id and pc.ordering = -1
		//		left join user_friend f on s.UserID = f.UserID
		//		inner join {$intranet_db}.INTRANET_USER u on s.UserID = u.UserID
		//		inner join {$intranet_db}.YEAR_CLASS_USER ycu on u.UserID = ycu.UserID
		//		inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
		//		where
		//		s.UserKey IS NOT NULL AND
		//		s.IsSuspend = 0 AND
		//		(
				//		    ".(self::$is_competition?'':"f.FriendID = ".$this->current_user_intranet_id." /* user's friend */ or")."
		//		    ( /* same year class as user */
				//		    yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
		//		    and yc.YearClassID in (".implode(',',$class_id).")
		//		    )
		//		)
		//
		//		".(!empty($friend_intranet_ids)?" AND u.UserID in (".$friend_intranet_ids.")":"")."
		//		group by p.web_portfolio_id, c.user_id
		//		order by c.notes_published desc";

				if ($amount != 'all') $sql.=" limit $offset, $amount";

				$items = $this->returnArray($sql);
				$total = current($this->returnVector('select found_rows()'));

				return array($total, $items);
	}

	public function removePortfolios(){

		$portfolio_ids = implode(',',(array)$this->web_portfolio_id);

		$sql = "delete w, c from web_portfolio w
		left join user_config c on w.web_portfolio_id = c.web_portfolio_id
		where w.web_portfolio_id in ($portfolio_ids)";

		$this->db_db_query($sql);

		return mysql_affected_rows()>0;
	}

	public function setPortfolioStatuses($status){

		$portfolio_ids = implode(',',(array)$this->web_portfolio_id);

		$sql = "update web_portfolio
		set status = $status
		where web_portfolio_id in ($portfolio_ids)";

		$this->db_db_query($sql);

		return mysql_affected_rows()>0;
	}

	/****** Portfolio Migration ******/
	public function isDraftExist(){

		$sql = "select count(*) from portfolio_draft
		where ordering <> -1 and ".$this->portfolio_cond;

		return current($this->returnVector($sql));
	}

	public function hasPortfolioPrototypeNewElements(){
		$sql = "select count(*)
		from portfolio_prototype p
		left join portfolio_draft d on p.portfolio_prototype_id = d.portfolio_prototype_id and d.user_id = '".$this->user_id."'
		where p.status > 0 and p.web_portfolio_id = ".$this->web_portfolio_id."
		group by p.portfolio_prototype_id having max(d.status)<0 /* all drafts are removed */ or max(d.status)  /* no existing draft, */ is null";

		return current($this->returnVector($sql));
	}

	public function pullPortfolioPrototypeNewElements(){//runs when a student opens the draft
		$result = array();

		// first, backup tree structure
		$sql = "update portfolio_draft d1
		inner join portfolio_draft d2
		on d1.parent_id = d2.portfolio_draft_id and d1.web_portfolio_id = d2.web_portfolio_id and d1.user_id = d2.user_id and d2.status>=0
		set d1.parent_id = d2.portfolio_prototype_id
		where d1.portfolio_prototype_id is not null /* do not change customized sections */ and d1.web_portfolio_id = '".$this->web_portfolio_id."' and d1.user_id = '".$this->user_id."'";

		$this->db_db_query($sql);

		// third, insert new prototypes to drafts
		$sql = "insert into portfolio_draft (portfolio_prototype_id, user_id, web_portfolio_id, parent_id, ordering, title, description, url, content, type, status, starttime, endtime, inputdate, modified)
		select p.portfolio_prototype_id, '".$this->user_id."', p.web_portfolio_id, p.parent_id, p.ordering, p.title, p.description, p.url, p.content, p.type, 2, p.starttime, p.endtime, now(), now()
		from portfolio_prototype p
		left join portfolio_draft d on p.portfolio_prototype_id = d.portfolio_prototype_id and d.user_id = '".$this->user_id."'
		where p.status > 0 and p.web_portfolio_id = '".$this->web_portfolio_id."'
		group by p.portfolio_prototype_id having max(d.status)<0 /* all drafts are removed */ or max(d.status)  /* no existing draft, */ is null";

		$this->db_db_query($sql);

		$result['created'] = mysql_affected_rows();

		// fourth, relink the tree structure of the new elements
		/*
		 * Commen by Pun, fix the sub-page will reload parent-page's start/end date while create draft.
		$sql = "update portfolio_draft d1
		inner join portfolio_draft d2
		on d1.parent_id = d2.portfolio_prototype_id and d1.web_portfolio_id = d2.web_portfolio_id and d1.user_id = d2.user_id and d2.status>=0
		set
		d1.parent_id = d2.portfolio_draft_id,
		d1.starttime = d2.starttime,
		d1.endtime = d2.endtime
		where d1.web_portfolio_id = '".$this->web_portfolio_id."' and d1.user_id = '".$this->user_id."'";*/
		$sql = "update portfolio_draft d1
		inner join portfolio_draft d2
		on d1.parent_id = d2.portfolio_prototype_id and d1.web_portfolio_id = d2.web_portfolio_id and d1.user_id = d2.user_id and d2.status>=0
		set
		d1.parent_id = d2.portfolio_draft_id
		where d1.web_portfolio_id = '".$this->web_portfolio_id."' and d1.user_id = '".$this->user_id."'";

		$this->db_db_query($sql);

		$sql = "select count(*) from user_config where user_id = '".$this->user_id."' and  web_portfolio_id = '".$this->web_portfolio_id."'";

		$config_exists = current($this->returnVector($sql));

		if (!$config_exists){

			$sql = "insert into user_config (user_id, web_portfolio_id, inputdate)
		    values ('".$this->user_id."', '".$this->web_portfolio_id."', now())";

			$this->db_db_query($sql);

		}
		//fifth, copy LP file if type = file
		$this->syncLPFile();


		$this->setReadflag(false);
		return $result;
	}
	public function syncLPFile(){
		global $eclass40_filepath;
		$sql = "SELECT
				pp.portfolio_prototype_id,
				pd.portfolio_draft_id,
				pp.content
			FROM
				portfolio_draft pd
			INNER JOIN
				portfolio_prototype pp on pd.web_portfolio_id = pp.web_portfolio_id
			AND
				pp.type = 'file'
			AND
				pd.portfolio_prototype_id = pp.portfolio_prototype_id
			WHERE
				pd.user_id = '".$this->user_id."' and pp.web_portfolio_id = '".$this->web_portfolio_id."'
		";
		$lp_arr = $this->returnArray($sql);

		for($p=0;$p<count($lp_arr);$p++){
			list($prototype_id,$draft_id,$content) = $lp_arr[$p];
			if(!empty($content)){
				//included in src/iportfolio/index.php
				$fm = new fileManager($this->course_id,'','');
				$fs = new phpduoFileSystem();
				$lp_file_path = $fm->file_path.'/'.$this->template_file_path;
				$file_list = explode($this->template_delimiter,$content);
				$file_count = count($file_list);
				$file_arr = array();
				for($f=0;$f<$file_count;$f++){
					list($_thisTimeStamp,$_thisFile) = explode('/',$file_list[$f]);
					$_fromPath = $lp_file_path.'/'.$this->template_prefix.'_'.$prototype_id.'/'.$file_list[$f];
					$_studentFolder = $this->getUserFolder();
					$_toPath = $fm->file_path.'/'.$this->student_file_path.'/'.$_studentFolder[0].'/'.$_thisTimeStamp;

					if(file_exists($_fromPath))
					{
						if(strpos($_thisFile, ".")!=0){
							$fm->createFolder(stripslashes($_toPath));
							$fs->phpduoCopy($_fromPath, $_toPath.'/'.$_thisFile);
							$file_arr[] = $_thisTimeStamp.'/'.$_thisFile;
						}
					}
				}
				/*$content = implode($this->template_delimiter,$file_arr);
				 $this->updateElement($draft_id, array('content'=>$content));*/
			}
		}
	}
	public function publishPortfolioDraft($force_status, $allow_like=1){

		$result = array();
		// first, remove all elements from publish
		$sql = "delete from portfolio_publish
		where web_portfolio_id = '".$this->web_portfolio_id."' and user_id = '".$this->user_id."'";

		$this->db_db_query($sql);

		$result['removed'] = mysql_affected_rows();
		$setToAll = !is_array($force_status);
		// second, insert drafts to publish
		$sql = "select portfolio_draft_id as id, ordering
	from portfolio_draft where status >=0 and web_portfolio_id = '".$this->web_portfolio_id."' and user_id = '".$this->user_id."'";

		$elements = $this->returnArray($sql);
		if($setToAll){
			$status = $force_status;
		}else{
			$statusAry = array_unique(array_values($force_status));
			$status = count($statusAry)>1?2:$statusAry[0];
		}
		foreach ($elements as $element){

			extract($element);

			if ($ordering==-1){
				$content 	= "'$allow_like'";
				$status 	= isset($status)? $status: '2';
			}else{
				$content 	= "content";
				$status = $setToAll?$force_status:($force_status[$element['id']]>=0 && $force_status[$element['id']]<=3 && isset($force_status[$element['id']])? "'".$force_status[$element['id']]."'" : $status);
			}
			$sql = "insert into portfolio_publish (user_id, web_portfolio_id, parent_id, ordering, portfolio_draft_id, title, description, url, content, status, type, readflag, inputdate, modified)
		    select '".$this->user_id."', '".$this->web_portfolio_id."', parent_id, ordering, $id, title, description, url, $content, $status, type, readflag, now(), now()
		    from portfolio_draft where portfolio_draft_id = $id";

			$this->db_db_query($sql);echo mysql_error();

			$sql = "update portfolio_draft set status = $status,content=$content,modified=NOW() where portfolio_draft_id = '".$id."'";//also save status to draft

			$this->db_db_query($sql);

		}

		// fourth, relink the tree structure of the new elements
		$sql = "update portfolio_publish d1
		inner join portfolio_publish d2
		on d1.parent_id = d2.portfolio_draft_id and d1.web_portfolio_id = d2.web_portfolio_id and d1.user_id = d2.user_id and d2.status>=0
		set d1.parent_id = d2.portfolio_publish_id
		where d1.web_portfolio_id = '".$this->web_portfolio_id."' and d1.user_id = '".$this->user_id."'";

		$this->db_db_query($sql);

		// fifth, copy config to user_config
		$sql = "update user_config c
		inner join portfolio_publish d on c.user_id = d.user_id and c.web_portfolio_id = d.web_portfolio_id
		set
		c.template_selected = d.title,
		c.notes_published = now(),
		c.modified = now(),
		c.allow_review = d.status
		where d.ordering = -1 and c.user_id = '".$this->user_id."' and c.web_portfolio_id = '".$this->web_portfolio_id."'";

		$this->db_db_query($sql);


		return $result;
	}

	/************ Export functions **************/
	private function replaceFilePaths($content, $id=''){

		global $eclass40_filepath, $eclass40_httppath, $cfg_eclass_lib_path;

		include_once($eclass40_filepath.'/src/includes/php/lib-filemanager.php');

		preg_match_all("`([^'\"]*/eclass40)(/files/[^'\"]+)`ui", $content, $attachment_matches);

		preg_match_all("`<div[^>]+id=\"fck_pv_container[^\"]+\"[^>]*>[^<]*<div[^>]+id=\"pv_name_[^\"]*\">[^<]*</div>[^<]*<div[^>]+fck_filepath=\"([^\"]+)\"[^>]*>[^<]+</div>[^<]*</div>`"
				, $content, $pv_matches);

		if (is_dir($this->export_dir)){
			$this->fs->folder_new($this->export_dir.'/attachments');
			$attachmentAry = array_unique($attachment_matches[2]);
			foreach ($attachmentAry as $i=>$attachment){

				$attachment_decoded = urldecode($attachment);
				$file_name = $id.'_'.$i.strrchr($attachment_decoded,'.');
				$this->fs->item_copy($eclass40_filepath.$attachment_decoded, $this->export_dir.'/attachments/'.$file_name);
				$content = str_replace($attachment_matches[1][$i].$attachment, 'attachments/'.$file_name, $content);

			}

			foreach ($pv_matches[1] as $i=>$pv){

				$file_name = substr(strrchr($pv,'/'),1);
				$file_name_converted = $id.'_pv_'.$i.'.mp3';

				$sql = "SELECT Location, Title FROM eclass_file WHERE Title='".addslashes($file_name)."' AND Category=7";

				$pv_file = current($this->returnArray($sql));

				$file_dir = $eclass40_filepath.'/files/'.$this->db.'/group/'.$pv_file['Location'].'/';//public category
				$this->fs->item_copy($file_dir.$file_name, $this->export_dir.'/attachments/'.$file_name_converted);

				$link = '<a href="attachments/'.$file_name_converted.'">'.$file_name.'</a>';
				$audio = '<audio controls="">
                          <source src="attachments/'.$file_name_converted.'" type="audio/mpeg">
                          <a href="attachments/'.$file_name_converted.'">'.$file_name.'</a>
                        </audio>';
				$content = str_replace($pv_matches[0][$i], $audio, $content);

			}

		}else{
		    $HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';
			foreach ($attachment_matches[1] as $attachment){
			    $content = str_replace('/eclass40'.$attachment, $HTTP_SSL.$eclass40_httppath.$attachment, $content);
			}
			foreach ($pv_matches[1] as $i=>$pv){

				$file_name = substr(addslashes($pv),1);

				$sql = "SELECT Location, Title FROM eclass_file WHERE Title='$file_name' AND Category=7";

				$pv_file = current($this->returnArray($sql));

				$file_url = $HTTP_SSL.$eclass40_httppath.'/files/'.$this->db.'/public/'.$pv_file['Location'].'/';//public category

				$content = str_replace($pv_matches[0][$i], $file_url.$file_name, $content);

			}
		}

		return $content;
	}
	public function getElementsForExport($id=-1, $elements=array()){

		if ($id>-1){

			$element = $this->getElement($id);
			if($element['type']=='file'){
				$element['content'] = $this->getFileUploadContent($element['content'],'publish');
			}
			$element['content'] = $this->replaceFilePaths($element['content'], $id);
			$element['weblogs'] = array();
			if($element['type']=='weblog'){
				$weblogs = $this->getElementChildren($id,true);
				foreach($weblogs as $weblog){

					$weblog_element = $this->getElement($weblog['id']);
					$weblog_element['content'] = $this->replaceFilePaths($weblog_element['content'], $weblog['id']);

					$element['weblogs'][] = $weblog_element;
				}
				$element['content'] = "";
			}
			$elements[$id]=$element;

		}

		$children = $this->getElementChildren($id);

		foreach($children as &$child){
			list($child['children'], $elements) = $this->getElementsForExport($child['id'], $elements);
		}

		return array($children, $elements);//tree stucture vs list structure

	}
	public function exportPortfolio($export_dir, $common_files_root='../'){

		global $langpf_lp2, $intranet_root;

		$this->setPortfolioMode('publish');
		$this->export_dir = $export_dir;
		$this->fs = new libfilesystem();
		$this->fs->folder_remove_recursive($this->export_dir);
		$this->fs->folder_new($this->export_dir);

		$data['info'] 			= $this->getPortfolioInfo();
		$data['config'] 		= $this->getPortfolioConfig();
		$data['config']['banner_url'] 	= $this->replaceFilePaths($data['config']['banner_url']);
		$data['student'] 		= $this->getUserInfo();

		list($data['sections'], $data['elements']) = $this->getElementsForExport();//structure and content

		ob_start();

		include($intranet_root.'/home/portfolio/learning_portfolio_v2/templates/export_html.php');

		$content = ob_get_clean();
		$this->fs->file_write($content, $this->export_dir.'/index_main.html');

		ob_end_clean();
		ob_start();

		include($intranet_root.'/home/portfolio/learning_portfolio_v2/templates/export_doc.php');

		$content = ob_get_clean();
		$this->fs->file_write($content, $this->export_dir.'/'.iconv("utf8","big5-hkscs//IGNORE",$data['student']['name']."_".$data['info']['title'].".doc"));

		ob_end_clean();
		$this->setPortfolioMode($this->mode);

	}

	public function exportPortfolioSharedFiles($path){

		global $eclass40_filepath;
		//copy common theme, jquery
		$libfs = new libfilesystem();

		$common_file_dir = $path.'/common_files';

		$libfs->createFolder($common_file_dir);
		$libfs->createFolder($path.'/images');
		$libfs->item_copy($eclass40_filepath.'/css/ipf_lp_common.css', $common_file_dir);
		$libfs->item_copy($eclass40_filepath.'/js/jquery-1.8.2.min.js', $common_file_dir);
		$libfs->item_copy($eclass40_filepath.'/images/iPortfolio/LP/logo_eclass_footer.gif', $path.'/images');
		$libfs->folder_copy($eclass40_filepath .'/src/iportfolio/themes', $common_file_dir);

	}
	public function exportPortfolios($path, $students, $input_student_array=''){

		global $eclass_prefix, $eclass_filepath, $ck_memberType, $intranet_root, $eclass40_filepath;


		$selected_students 	= $input_student_array? array_keys($input_student_array, 1):array();

		//implant new version Learning Portfolios to the folder
		$exported_students = 0;

		foreach ($students as $student){

			if ($input_student_array && !in_array($student['CourseUserID'], $selected_students) && !in_array($student['UserID'], $selected_students)) continue;

			$sql = "select c.web_portfolio_id as id, c.template_selected as theme from web_portfolio w
		    inner join user_config c on w.web_portfolio_id = c.web_portfolio_id
		    where c.user_id = ".$student['CourseUserID']."
		    and w.version = 1 and c.notes_published is not null";//get new version

			$this->db = $eclass_prefix."c".self::getPortfolioCourseId();

			$portfolios = $this->returnArray($sql);

			$student_path = $_SESSION['ck_memberType']=='T'?$path."/".str_replace("#", "", $student['WebSAMSRegNo'])."_".str_replace(" ", "_", $this->filename_safe($student[4])):$path;

			foreach ($portfolios as $portfolio){

				$liblp2 = new libpf_lp2($student['UserID'], $portfolio['id'], $student['UserID'], 'publish'); //get portfolio using owner permission
				//debug_r($student_path."/student_u".$student['CourseUserID']."_wp".$portfolio['id']);
				$liblp2->exportPortfolio($student_path."/student_u".$student['CourseUserID']."_wp".$portfolio['id'], $_SESSION['ck_memberType']=='T'?'../../':'../');

			}
			$exported_students++;

		}

		if ($exported_students) $this->exportPortfolioSharedFiles($path);
		return $path;
	}
	function getLPConfigSetting(){
		global $eclass_root,$intranet_root;
		// load settings
		include_once($intranet_root."/includes/libfilesystem.php");
		$li_fs = new libfilesystem();
		$lp_config_file = "$eclass_root/files/lp_config.txt";
		$config_array = unserialize($li_fs->file_read($lp_config_file));
		/* 20201022 disable all facebook like function */
        $config_array['disable_facebook'] = 1;
//		$config_array['disable_facebook'] = ($this->thisRegion=='zh_CN')?1:($config_array['disable_facebook']==1);

		return $config_array;

	}
	function getFileUploadContent($data){
		global $eclass40_filepath;
		$content = '';
		$file_list = explode($this->template_delimiter,$data);
		for($i=0;$i<count($file_list);$i++){
			list($_timestamp,$_filename) = explode('/',$file_list[$i]);
			$_studentFolder  = 'student_u'.$this->user_id.'_wp'.$this->web_portfolio_id;;
			$_templateFolder = $this->student_file_path.$_studentFolder.'/';

			$_filepath = '/eclass40/files/'.$_templateFolder.$_timestamp.'/'.$_filename;
			$lp_file_path = $eclass40_filepath.'/files/'.$_templateFolder.$_timestamp.'/'.$_filename;

			if(file_exists($lp_file_path)){
				$content .= '<a href="'.$_filepath.'" target="_blank">'.$_filename.'</a><br/><br/>';
				$content .= '<IFRAME src="'.$_filepath.'" scrolling="auto"  width="100%" height="500"></IFRAME><br/>';

			}
		}
		return $content;
	}
}

libpf_lp2::$is_competition = $sys_custom['lp_competition'];
?>