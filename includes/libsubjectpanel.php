<?php
/** [Modification Log] Modifying By: ivan
 * ******************************************
 *  
 * *******************************************
 */ 
if (!defined("LIBSUBJECTPANEL_DEFINED"))                // Preprocessor directives
{
class libsubjectpanel extends libdb {
	/*
	 * AccessType:
	 * 		0: Student Data Analysis System Admin
	 * 		1: Subject Panel
	 * 
	 */
	
	function libsubjectpanel() {
		$this->libdb();
		$this->ModuleName = 'SubjectPanel';
	}
	
	function getSubjectPanelListSql($parKeyword, $parSubjectId) {
		$userNameField = Get_Lang_Selection('IU.ChineseName', 'IU.EnglishName');
		$subjectField = Get_Lang_Selection('ASS.CH_DES', 'ASS.EN_DES');

		if ($parKeyword !== '' && $parKeyword !== null) {
			$condsKeyword = " And (
									".$userNameField." Like '%".$this->Get_Safe_Sql_Like_Query($parKeyword)."%'
									Or ".$subjectField." Like '%".$this->Get_Safe_Sql_Like_Query($parKeyword)."%'
									Or Y.YearName Like '%".$this->Get_Safe_Sql_Like_Query($parKeyword)."%'
								  ) 
							";
		}
		
		if ($parSubjectId !== '') {
			$condsSubjectId = " And ASS.RecordID = '".$parSubjectId."' ";
		}
		
		$sql = "SELECT 
				".$userNameField." as TeacherName,
				".$subjectField." as SubjectName,
				GROUP_CONCAT(DISTINCT Y.YearName SEPARATOR ',<br />') AS YearName,
				CONCAT('<input type=\"checkbox\" id=\"recordIdAryChk_Global\" name=\"recordIdAry[]\" value=\"', GROUP_CONCAT(AAR.RecordID SEPARATOR ',') , '\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\">')
			FROM 
				ASSESSMENT_ACCESS_RIGHT AAR
				INNER JOIN INTRANET_USER IU ON (AAR.UserID = IU.UserID)
				INNER JOIN ASSESSMENT_SUBJECT ASS ON (AAR.SubjectID = ASS.RecordID)
				INNER JOIN YEAR Y ON (AAR.YearID = Y.YearID)
			WHERE
				AAR.AccessType = '1'
				$condsKeyword
				$condsSubjectId
			GROUP BY
				AAR.UserID,
				AAR.SubjectID
			";
			
		return $sql;
	}
	
	function deleteSubjectPanel($recordIdList) {
		if ($recordIdList == '') {
			return false;
		}
		else {
			$sql = "SELECT * FROM ASSESSMENT_ACCESS_RIGHT WHERE RecordID IN ($recordIdList)";
			$dataAry = $this->returnResultSet($sql);
			$numOfData = count($dataAry);
			
			$sql = "DELETE FROM ASSESSMENT_ACCESS_RIGHT WHERE RecordID IN ($recordIdList)";
			$success = $this->db_db_query($sql);
			
			if ($success) {
				# insert delete log
				global $PATH_WRT_ROOT;
				include_once($PATH_WRT_ROOT.'includes/liblog.php');
				$liblog = new liblog();
				for ($i=0; $i<$numOfData; $i++) {
					$_recordId = $dataAry[$i]['RecordID'];
					
					$_tmpAry = array();
					$_tmpAry = $dataAry[$i];
					$successAry['Log_Delete'] = $liblog->INSERT_LOG($this->ModuleName, 'DeleteSubjectPanel', $liblog->BUILD_DETAIL($_tmpAry), 'ASSESSMENT_ACCESS_RIGHT', $_recordId);
				}
			}
			
			
			return $success; 
		}
	}
	
	function addSubjectPanel($parSubjectId, $parFormIdAry, $parTeacherIdAry) {
		#### Get Old User START ####
		$selectedUserList = implode("','", $parTeacherIdAry);
		$sql = "SELECT
					UserID, YearID
				FROM
					ASSESSMENT_ACCESS_RIGHT
				WHERE
					AccessType = '1'
					AND SubjectID = '".$parSubjectId."'
				";
		$rs = $this->returnResultSet($sql);
		
		$oldUser = array();
		foreach($rs as $r){
			$oldUser[ $r['UserID'] ][ $r['YearID'] ] = 1;
		}
		#### Get Old User END ####
		
		
		#### Add User START ####
		$successAry = array();
		foreach($parTeacherIdAry as $_uid){
			foreach($parFormIdAry as $__formId){
				if(!isset($oldUser[$_uid][$__formId])) {
					$sql = "INSERT INTO ASSESSMENT_ACCESS_RIGHT (
								UserID, 
								AccessType, 
								SubjectID,
								YearID,
								InputDate, 
								CreatedBy
							) VALUES (
								'".$_uid."',
								'1',
								'".$parSubjectId."',
								'".$__formId."',
								CURRENT_TIMESTAMP,
								'".$_SESSION['UserID']."'
							)";
					$successAry[] = $this->db_db_query($sql);
				}
			}
		}
		#### Add User END ####
		
		return !in_array(false, (array)$successAry);
	}
}
} // End of directives
?>