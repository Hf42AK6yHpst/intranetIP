<?
/*
 * Editing by
 *
 * Modification Log:
 *
 * 2020-10-19 (Cameron)
 *  - add filter Publish=1 to getMostActiveUsers(), getUserReviews(), getUserReviews2(), getBookDetail(), getReadingProgress(), getUserNotes(), getUserFavourites(), getFriendNotification(),
 *      getMyFriendeBookViewed(), getMyFriendRankingReadingeBook(), getSharingBooks(), getFriendNotificationCount()  [case #Y190573]
 *  - modify getFriendNotification() to use inner join
 *
 * 2019-07-04 (Cameron)
 *  - retrieve number of book read (hits) chi and eng in getPublicClassSummary() [case #A163164]
 *
 * 2019-06-27 (Cameron)
 *  - add advanced search field "introduction" to getBookList()
 *
 * 2019-05-14 (Paul)
 *  - security fix on queries missing quote
 *
 * 2019-04-08 (Cameron)
 *  - fix no active eBook case in getBookCatrgories() [case #G158645]
 *
 * 2019-03-26 (Cameron)
 *  - don't count expired eBook in getBookCatrgories() [case #G158645]
 *    (has filter out expired eBooks in 2015-12-08, why it was ever changed? now filter out again)
 *
 * 2018-12-05 (Paul)
 * 	- Modified getBookCatrgories() to prevent picking up ebooks only under quotation as customized books have no quotation
 *
 * 2018-12-03 (Cameron)
 *  - modify getBookList(), getBestRateOrMostHitBooks(), getNewBooks() so that they won't show expired books [case #C152289]
 *
 * 2018-09-26 (Cameron)
 *  - check if $this->book_id > 0 in getBookReviews() sql [case #W139304]
 *
 * 2018-09-21 (Cameron)
 *  - modify addOrRemoveReservation(), pass $checkAvailableBookReservation to canReserve() function
 *
 * 2018-09-05 (Cameron)
 *  - allow user to reserve book in addOrRemoveReservation even if the book is available to pick up [case #A135894]
 *
 * 2018-06-21 (Cameron)
 *  - add parameter $withTotal to getUserLoanBooks() and getUserLoanBooksHistory() [case #Y140487]
 *
 * 2018-05-15 (Cameron)
 * - handle sortBy=bookID in getBookList() [case #Y137702]
 *
 * 2018-02-07 (Cameron)
 * - Fix bug: if there's no physical book category, should retrieve empty as Book Category Type in getBookCatrgories() [case #W135416]
 *
 * 2018-01-30 (Cameron)
 * - modify getUserLoanBooks(), getUserPenalty(), getUserLostBooks() to retrieve ItemSeriesNum if it's not empty and not 1 [case #L130405]
 *
 * 2017-12-21 (Henry)
 * - modified renewBook() for disallow user renew book which is overdue [case #R133149]
 *
 * 2017-12-12 (Cameron)
 * - add function getBookCategoryType() [case #F130026]
 * - modify getBookList() for search from book category link, add book_category_type
 * - modify getBookCatrgories() to cater for book category type
 *
 * 2017-12-07 (Cameron)
 * - apply combination of single word in keyword search field method to following fields in getBookList():
 * Title, SubTitle, Author, Publisher, TagsName and SeriesEditor
 *
 * 2017-11-27 (Cameron)
 * - add function getWords() <- move it to lib.php (2017-12-07)
 * - modify getBookList() to allow simple keyword search to get result of any combination of single word in keyword search field [case #F129849]
 *
 * 2017-10-16 (Cameron)
 * - add function getPendingFriends(), getSearchPendingFriend()
 *
 * 2017-10-11 (Cameron)
 * - fix: also delete notification of friend request / accept friend request in removeFriend() so that user can make request again
 *
 * 2017-10-03 (Cameron)
 * - fix: modify getUserPermission() to let user be able to access 'Admin Settings' button if he/she has statistics access right [case #T126695]
 *
 * 2017-09-29 (Cameron)
 * - modify getMostBorrowUsers to include staff in my friends ranking
 *
 * 2017-09-28 (Cameron)
 * - add function getMyFriendExcludeUser(), getTeacher()
 * - add parameter $className to getMyClassLevelClassList()
 *
 * 2017-09-27 (Cameron)
 * - add function getClassAndClassLevelID() and getSearchToAddFriends()
 *
 * 2017-05-18 (Cameron)
 * - void change on 2017-04-07
 *
 * 2017-05-12 (Cameron)
 * - fix Chinese character problem in getMyClassLevelClassList() for ej
 *
 * 2017-03-28 ~ 2017-04-24 (Cameron)
 * - add my friend functions
 *
 * 2017-04-13 (Cameron)
 * - add parameter $cond to getMostBorrowUsers()
 * - add parameter $userCond to getMostActiveUsers()
 *
 * 2017-04-12 (Cameron)
 * - fix bug in getNewBooks(): should use AccountDate to compare for physical books in $dateRangeCond
 *
 * 2017-04-07 (Cameron)
 * - modify getBookList() to handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357] - void for ej (2017-05-18)
 *
 * 2017-04-03 (Cameron)
 * - trim(SubCategory) in getBookList() [case #C115368]
 *
 * 2017-03-03 (Cameron)
 * - modify getUserPermission(), add ip checking for circulation management
 *
 * 2017-02-09 (Cameron)
 * - modify getBookCatrgories() and getBookSubCategories(), allow BookForm = null to display for eBook
 * - remark: getBookReviews(): book review don't support <> as it uses strip_tags, the concern is
 * we don't know if there any client already input html text
 *
 * 2017-01-25 (Cameron)
 * - change order by from DateModified to AccountDate first, then DateModified in getNewBooks()
 *
 * 2017-01-11 (Cameron)
 * - add function getPhotoSetting(), modify getUserInfo() to use photo based on system setting
 * - modify getUserPermission(): don't allow user to view 'Admin Settings' button if he/she only has 'Portal Settings' right
 *
 * 2016-11-16 (Cameron)
 * - add parameter $classNameWithBracket to getMostActiveUsers(), getUserInfo(), getMostUsefulReviews(), getMostBorrowUsers()
 *
 * 2016-11-11 (Cameron)
 * - add $permission['portal_setting'] in getUserPermission()
 *
 * 2016-11-02 (Cameron)
 * - fix bug: the hyper link should stop when encounter space characters(space, tab, new line, form feed) in getPortalNotices()
 * [case #U105884]
 * 2016-10-24 (Cameron)
 * - add parameter $dateRange to getRecommendBooks(), getNewBooks()
 * - fix bug: the start date filter should be deduct one rather than add one in getDayRangeCondition()
 *
 * 2016-10-20 (Cameron)
 * - add parameter $ckeditor to getPortalNotices()
 *
 * 2016-10-17 (Cameron)
 * - add parameter $book_type to getBookCatrgories()
 *
 * 2016-10-13 (Cameron)
 * - change sorting order by Priority, DateStart, Title in getPortalNotices()
 *
 * 2016-09-28 (Cameron)
 * - add date range filter to getTotalPortalNotices() and getPortalNotices()
 *
 * 2016-09-26 (Cameron)
 * - add parameter $offset, $newLimit to getNewBooks()
 *
 * 2016-09-23 (Cameron)
 * - modify getRecommendBooks(): get recommend book records if shuffle is true, then apply shuffle to the result,
 * (previous version retrieve 1000 records only)
 * 2016-09-20 (Cameron)
 * - add parameter $book_id in addOrEditRecommend()
 *
 * 2016-08-05 (Cameron)
 * - add parameter $with_total to function getAllReviewsRecord(), getPublicClassSummary(), getPublicStudentSummary(),
 * getPublicStudentParticularSummary(), getReadingProgress(), getUserNotes()
 *
 * 2016-07-20 (Cameron)
 * - add parameter bookType to getUserReviews()
 *
 * 2016-07-08 (Cameron)
 * - add parameter userType to getMostActiveUsers()
 * - add function getBookLanguages()
 *
 * 2016-06-29 (Cameron)
 * - add function getBookDetailWithoutFormat()
 * - add function getSubStrings()
 *
 * 2016-06-28 (Cameron)
 * - modifiy return value in addOrEditRecommend()
 * - add search by class_level_id in getBookList()
 * - add function getSimilarBooks()
 *
 * 2016-06-27 (Cameron)
 * - add parameter $ReviewID to getBookReviews()
 *
 * 2016-06-24 (Cameron)
 * - add function getTotalBookReviews()
 *
 * 2016-06-22 (Cameron)
 * - add function getBookSubCategories()
 *
 * 2016-06-21 (Cameron)
 * - add search by publihser in getBookList()
 *
 * 2016-06-13 (Cameron)
 * - add function addFavourite() and removeFavourite()
 *
 * 2016-06-07 (Cameron)
 * - add function getLibraryCalendarByWeek() for used in eLib+2 portal
 *
 * 2016-06-06 (Cameron)
 * - add function getTotalPortalNotices()
 * - move code fragment to function copyOldDataFromElibBookPage()
 *
 * 2016-05-27 (Cameron)
 * - set $userPhotoField in getUserInfo() if it's not initialized
 * - add parameter bookType(ebook/physical) in getMostActiveUsers(), getMostUsefulReviews()
 *
 * 2016-05-26 (Cameron)
 * - set $limit=3 as default value in getBestRateOrMostHitBooks()
 * - add private member $accumulated_days
 * - add function set_accumulated_days()
 * - add 'specific' range in getDayRangeCondition()
 * - add parameter bookType(ebook/physical) in getBestRateBooks(), getMostHitBooks(), getBestRateOrMostHitBooks()
 *
 * 2016-05-25 (Cameron)
 * - add parameter bookType(ebook/physical) in getNewBooks(), to support new eLib+ portal
 * - use $physical_book_url instead of url in getBooksArrayByFormat() to distinguish it from ebook [case #P94419]
 *
 * 2016-05-20 (Cameron)
 * - add parameter bookType(ebook/physical) in getRecommendBooks(), to support new eLib+ portal
 *
 * 2016-04-20 (Cameron)
 * - retrieve url for physical book in getBooksArrayByFormat() [revised on 2016-05-25]
 *
 * 2016-02-15 (Henry)
 * - modified getBookList() to fix the total number of book problem
 *
 * 2016-01-21 Paul
 * - add Customisation for internal site to display books without quotations
 * - modified getBookCatrgories() for better performance
 *
 * 2015-12-16 (Paul)
 * - modify getAllReviewsRecord(), getReadingProgress() and getUserNotes() to return one more parameter to check if shown a book as expired or not
 *
 * 2015-12-09 (Paul)
 * - fix standard violation on getBookDetail() for PHP5.4+
 *
 * 2015-12-08 (Paul)
 * - modified getBookImage(), getBooksArrayByFormat(), getNewBooks(), getMostHitBooks(), getBestRateOrMostHitBooks(), getMostLoanBooks(), getBestRateBooks(), getBookList() to prevent expired book from being accessed through portal. Only information and book reviews are allowed to be seen
 * - modified getBookCatrgories() to prevent counting expired books
 *
 * 2015-12-02 (Cameron)
 * - fix bug on addOrRemoveReservation() for sending email (do_email)
 *
 * 2015-11-26 (Cameron)
 * - disable getting first 250 chars only if books description exceed in getBookDetail()
 *
 * 2015-11-25 (Cameron)
 * - fix bug on getBookList(), set $total to number of record returned from $returnArray
 *
 * 2015-10-26 (Cameron)
 * - change order by Category to Count(SubCategory) in getBookCatrgories()
 *
 * 2015-10-26 (Henry)
 * - modified IsRenewalAllowed() to add reserved checking
 *
 * 2015-10-23 (Cameron)
 * - add parameter $limit to getBookCatrgories() to load less categories
 *
 * 2015-10-05 (Cameron)
 * - fix bug on search for string like A&<>'"\B in getBookList(), support comparing unconverted string A&<>'"\B ( created by imoport)
 * and converted string A&amp;'&quot;&lt;&gt;\B (created by add / edit)
 *
 * 2015-09-22 (Cameron)
 * - apply special_sql_str() to getBookList() function so that it can search special characters
 *
 * 2015-09-16 (Cameron)
 * - fix bug on handling special characters in getBookList()
 *
 * 2015-08-03 (Cameron)
 * - retrieve more fields (isbn, no_of_page, subject) from getBooksArrayByFormat()
 *
 * 2015-07-30 (Cameron)
 * - apply $amount to sql in getPortalNotices()
 *
 * 2015-07-16 (Cameron)
 * - fix bug on showing rule in front end. (case ref. # 2015-0713-1520-21170) should check sizeof($annoucement)>0 or not in getPortalNotices()
 *
 * 2015-06-17 (Henry)
 * - fix bug on sorting the best rate/most hit book
 *
 * 2015-06-09 (Henry)
 * - add permission for the circulation access
 *
 * 2015-05-04 (Charles Ma) [ip.2.5.6.5.1]
 * - P77849
 *
 * 2015-04-28 (Cameron)
 * - add parameters $searchFromDate and $searchToDate to getAllReviewsRecord() [case 2015-0326-1635-30054]
 * - add function getAllReviewsDetails()
 *
 * 2015-03-25 (Charles Ma) - [ip.2.5.6.5.1]
 * - modified getPortalNotices() to fix returning of news [Case#P76456]
 *
 * 2015-02-16 (Henry)
 * - modified addOrRemoveReservation() to fix bug for wrong reservation status update [Case#R75085]
 *
 * 2015-02-11 (Henry)
 * - modified getBookList() to support keyword search for subtitle of physical book
 *
 * 2015-02-04 (Henry)
 * - modified getBooksArrayByFormat()
 *
 * 2014-12-10 (Henry)
 * - modified getPublicClassSummary() and getPublicStudentSummary() to fix the bug which show the Alumni information
 *
 * 2014-12-05 (Yuen)
 * - modified addOrRemoveReservation() to fix the bug which disallow cancelling of reservation
 *
 * 2014-10-07 (Charles) 20141007-P64683
 * - modified setPortalNotice() and getPortalNotices()
 *
 * 2014-10-03 (Henry)
 * - modified getBookList() to filter out the book which has 0 no. of copy
 *
 * 2014-09-11 (Henry)
 * - modified getBookCatrgories() to show the physical book tab
 *
 * 2014-06-25 (Charles Ma)
 * - // 20140625-getGroupEndDategetPatronPeriod
 * - function renewBook() add global $User to fulfill $timeManager->findOpenDate > getPatronPeriod condition
 * - Case Number : E63107
 *
 * 2014-05-30 (Henry)
 * - modified to allow Admin have circulation permission by default
 *
 * 2013-12-17 (Yuen)
 * - modified function renewBook() to fix incorrect return date determined
 *
 * 2013-10-31 (Yuen)
 * - improved function getBookTags() to limit the number of tags or otherwise the portal page fails to show
 *
 * 2013-10-23 (Charles Ma)
 * - improve function getMostBorrowUsers for showing current academic year only
 *
 * 2013-10-22 (Henry)
 * - modified addOrRemoveReservation()
 *
 *
 * 2013-10-10 (Charles Ma)
 * - improve my notes function to include html5 book
 * -getUserNotes
 *
 * 2013-10-07 (Henry)
 * - modified addOrRemoveReservation()
 *
 * 2013-09-30 (Charles Ma)
 * - add several function to support stat page
 * -getAllReviewsRecord
 * -getPublicClassSummary
 * -getPublicStudentSummary
 * -getPublicStudentParticularSummary
 * -retrieve_filter
 *
 * 2013-09-03 (Siuwan)
 * - edit getBooksArrayByFormat() to get book call number
 * - edit getBookList() to add book call number in keyword condition
 * 2013-08-28 (Mick)
 * - edit getUserLimitations() to fix the problem of not able to reserve books when the circulation type does not set for particular group
 * instead, it would use the default setting of that group
 * 2013-08-27 (Jason)
 * - edit getBooksArrayByFormat() to add reserved_count no of copy available
 * - add getPhysicalBookReservedCount() to get the reserved number of books
 * 2013-08-23 (Jason)
 * - edit getBookList() to search Series and ACNO field during searching
 */
include_once ("$intranet_root/includes/libelibrary_install.php");
include_once ("$intranet_root/includes/liblibrarymgmt.php");
include_once ("$intranet_root/includes/libelibrary.php");

class elibrary_plus extends elibrary
{

    private $book_id;

    private $physical_book_id;

    public $physical_book_init_value;

    public $physical_book_type_code;

    private $user_id;

    private $book_formats = "";

    private $libmsDB;

    private $range;

    private $accumulated_days;

    public static function getBookListView($books, $showrank = false)
    {
        global $eLib_plus, $eLib;
        ob_start();
        ob_flush();
        include ('templates/book_list.php');

        $book_list = ob_get_clean();

        ob_end_flush();

        return $book_list;
    }

    public static function getDaysWord($date_string)
    {
        global $eLib_plus, $request_ts;

        $ts_diff = $request_ts - strtotime($date_string);

        if ($ts_diff > 86400) {
            return floor($ts_diff / 86400) . ' ' . $eLib_plus["html"]["daysago"];
        } else if ($ts_diff > 3600) {
            return floor($ts_diff / 3600) . ' ' . $eLib_plus["html"]["hoursago"];
        } else if ($ts_diff > 60) {
            return floor($ts_diff / 60) . ' ' . $eLib_plus["html"]["minsago"];
        } else {
            return $eLib_plus["html"]["lessthan1minago"];
        }
    }

    public static function getEllipsisString($str, $limit)
    {
        return mb_strlen($str) > $limit ? mb_substr($str, 0, $limit, 'utf8') . '...' : $str;
    }

    public static function getUserPermission($user_id)
    {
        $libms = new liblms();
        $libms->get_current_user_right();
        $libms->get_class_mgt_user_right();

        $libelibrary = new elibrary();

        if (! isset($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])) {
            $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] = $libelibrary->IS_ADMIN_USER($user_id);
        }

        $permission['admin'] = $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'];
        $permission['circulation'] = (in_array(true, (array) $_SESSION['LIBMS']['admin']['current_right']['circulation management']) || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || in_array(true, (array) $_SESSION['LIBMS']['admin']['class_mgt_right']['circulation management'])) && $_SESSION['LIBMS']['admin']['check_ip_for_circulation'];
        $permission['setting'] = $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || (in_array(true, (array) $_SESSION['LIBMS']['admin']['current_right']['admin']) && ! (count($_SESSION['LIBMS']['admin']['current_right']['admin']) == 1 && implode('', array_keys((array) $_SESSION['LIBMS']['admin']['current_right']['admin'])) == 'portal settings')) || in_array(true, (array) $_SESSION['LIBMS']['admin']['current_right']['reports']) || in_array(true, (array) $_SESSION['LIBMS']['admin']['current_right']['statistics']);
        $permission['reserve'] = $_SESSION['LIBMS']['admin']['current_right']['service']['reserve'] && $libms->IsSystemOpenForLoan();
        $permission['renew'] = $_SESSION['LIBMS']['admin']['current_right']['service']['renew'];
        $permission['elibplus_opened'] = ! $libms->get_system_setting('global_lockout') || $permission['admin'] || $permission['circulation'] || $permission['setting'];
        $permission['portal_setting'] = $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || $_SESSION['LIBMS']['admin']['current_right']['admin']['portal settings'];

        return $permission;
    }

    public static function sendReservationEmail($user_id, $book_title, $newExpiryDate = 0)
    {
        global $intranet_session_language, $intranet_root, $Lang;

        include_once ($intranet_root . '/home/library_sys/lib/liblibrarymgmt.php');
        include_once ($intranet_root . "/lang/libms.lang.$intranet_session_language.php");
        include_once ("libemail.php");
        include_once ("libsendmail.php");
        include_once ("libwebmail.php");

        global $Lang;

        $lwebmail = new libwebmail();
        $mailSubject = $Lang["libms"]["report"]["overdue_email"]["Subject"];
        if ($newExpiryDate == 0) {
            $lwebmail->sendModuleMail((array) $user_id, $Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['subject'], sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body'], $book_title), 1, '', 'User');
        } else {
            $lwebmail->sendModuleMail((array) $user_id, $Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['subject'], sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body_with_due_date'], $book_title, $newExpiryDate), 1, '', 'User');
        }
    }

    public function elibrary_plus($book_id = 0, $user_id = 0, $book_type = "type_all", $range = 'accumulated')
    { // note that using user = affected user
        global $intranet_session_language, $eclass_prefix, $junior_mck;

        $this->elibrary();

        $this->libmsDB = $eclass_prefix . "eClass_LIBMS";
        $this->physical_book_init_value = 10000000;
        $this->physical_book_type_code = "physical";
        $this->book_id = $book_id;
        $this->physical_book_id = $book_id - $this->physical_book_init_value;
        $this->user_id = $user_id;
        $this->range = $range;
        $this->convert_utf8 = $junior_mck;
        $this->notifcation_settings = array(
            'reserve' => $this->getNotificationSettings('reserve_email'),
            'overdue' => $this->getNotificationSettings('overdue_email')
        );

        switch ($book_type) {

            case "type_physical":
                $this->book_formats = "AND BookFormat = '" . $this->physical_book_type_code . "' ";
                break;

            case "type_pc":
                $this->book_formats = "AND (BookFormat = 'ePub2' OR BookFormat IS NULL)";
                break;

            case "type_ipad":
                $this->book_formats = "AND BookFormat = 'ePub2' ";
                break;
        }
    }

    public function getRecommendedClassLevels()
    {
        $recommends = $this->getBookRecommend(array(
            "BookID" => $this->book_id
        ));

        if (empty($recommends)) {

            $returnArray = array(
                'content' => '',
                'isEdit' => false
            );

            $recommended_classes = array();
        } else {

            $returnArray = array(
                'content' => $recommends[0]['Description'],
                'isEdit' => 'true'
            );

            $recommended_classes = explode('|', $recommends[0]['RecommendGroup']);
        }

        if ($this->convert_utf8) {

            $this->dbConnectionLatin1();

            $sql = "select ClassLevelID as class_level_id ,LevelName as class_level_name from INTRANET_CLASSLEVEL order by LevelName";
            $class_levels = $this->returnArray($sql);

            foreach ($class_levels as &$class_level) {

                $class_level['class_level_name'] = convert2Unicode($class_level['class_level_name']);
                $class_level['class_level_recommended'] = in_array($class_level['class_level_id'], $recommended_classes);
            }

            $this->dbConnectionUTF8();
        } else {

            $sql = "select YearID as class_level_id, YearName as class_level_name from YEAR order by Sequence";
            $class_levels = $this->returnArray($sql);

            foreach ($class_levels as &$class_level) {
                $class_level['class_level_recommended'] = in_array($class_level['class_level_id'], $recommended_classes);
            }
        }
        $returnArray['class_levels'] = $class_levels;

        return $returnArray;
    }

    public function getLibraryOpenEndDates()
    {
        return array(
            current($this->returnVector("SELECT value FROM " . $this->libmsDB . ".LIBMS_SYSTEM_SETTING
		WHERE name='system_open_date'")),

            current($this->returnVector("SELECT value FROM " . $this->libmsDB . ".LIBMS_SYSTEM_SETTING
		WHERE name='system_end_date'"))

        );
    }

    public function getLibraryOpeningHours()
    {
        $sql = "select Weekday, OpenTime, CloseTime, Open from " . $this->libmsDB . ".LIBMS_OPEN_TIME order by Weekday";

        return $this->returnArray($sql);
    }

    public function getLibraryHolidays()
    {
        $sql = "SELECT Event as event, UNIX_TIMESTAMP(DateFrom) as start, UNIX_TIMESTAMP(DateEnd+INTERVAL 1 DAY) as end
		FROM " . $this->libmsDB . ".LIBMS_HOLIDAY ORDER By DateFrom";

        return $this->returnArray($sql);
    }

    public function getLibrarySpecialOpenDays($this_week = false)
    {
        $sql = "SELECT UNIX_TIMESTAMP(DateFrom) as start, UNIX_TIMESTAMP(DateEnd+INTERVAL 1 DAY) as end, OpenTime, CloseTime, Open
          	FROM " . $this->libmsDB . ".LIBMS_OPEN_TIME_SPECIAL ORDER By DateFrom";

        return $this->returnArray($sql);
    }

    private function copyOldDataFromElibBookPage()
    {
        $sql = " select p.BookPageID as id, p.Content as content, p.ContentXML as title,
			    p.PageType as type, p.DateModified as modified
			from INTRANET_ELIB_BOOK_PAGE p		
			where p.BookID= -1  
			order by p.DateModified desc ";

        $old_data_arr = $this->returnArray($sql);
        if (sizeof($old_data_arr) > 0) {

            $insert_sql = "( '" . $old_data_arr[0]["id"] . "','" . $old_data_arr[0]["content"] . "','" . $old_data_arr[0]["type"] . "','" . $old_data_arr[0]["modified"] . "','" . $old_data_arr[0]["title"] . "'" . " )";

            for ($i = 1; $i < sizeof($old_data_arr); $i ++) {
                $insert_sql .= ", ( '" . $old_data_arr[$i]["id"] . "','" . $old_data_arr[$i]["content"] . "','" . $old_data_arr[$i]["type"] . "','" . $old_data_arr[$i]["modified"] . "','" . $old_data_arr[$i]["title"] . "'" . " )";
            }

            $sql = " insert into INTRANET_ELIB_BOOK_ANNOUNCEMENT
				(AnnouncementID, Content, AnnouncementType, DateModified, ContentXML)
			    values
				$insert_sql  on duplicate key update " . " Content = VALUES(Content), AnnouncementType = VALUES(AnnouncementType)," . " DateModified = VALUES(DateModified), ContentXML = VALUES(ContentXML) ";
            $success = $this->db_db_query($sql);
            if ($success) {
                $sql = " UPDATE INTRANET_ELIB_BOOK_PAGE Set BookID = '-2' Where BookID = '-1'  ";
                $this->db_db_query($sql);
            }
        }
    }

    public function getPortalNotices($type, $id = false, $offset = 0, $amount = 10, $is_link_allow = false, $ckeditor = false)
    {
        global $PATH_WRT_ROOT, $permission;

        // $sql = "select p.BookPageID as id, p.Content as content, p.ContentXML as title,
        // p.PageType as type, p.DateModified as modified,
        // m.FilePath as attachment, substring_index(m.FilePath, '/', -1) as attachment_name
        // from INTRANET_ELIB_BOOK_PAGE p
        // left join INTRANET_ELIB_BOOK_PAGEMEDIA m on p.BookPageID = m.Page and m.BookID = -1
        // where p.BookID= -1 ".($type?"and p.PageType = '$type'":"")." ".($id?"and p.BookPageID=$id":"")."
        // order by p.DateModified desc
        // ".(isset($offset)&&$amount&&!$id?"limit $offset, $amount":"");

        // Check if the old data is copied when start
        $this->copyOldDataFromElibBookPage();

        $cond = "";
        if (! $permission['admin']) {
            $cond .= " and (((p.DateStart is null or p.DateStart='0000-00-00 00:00:00') and (p.DateEnd is null or p.DateEnd='0000-00-00 00:00:00')) or 
						((p.DateStart is null or p.DateStart='0000-00-00 00:00:00') and p.DateEnd>=CURDATE()) or 
						((p.DateEnd is null or p.DateEnd='0000-00-00 00:00:00') and p.DateStart<=CURDATE()) or 
						(p.DateStart<=CURDATE() and p.DateEnd>=CURDATE()))";
        }

        // Charles Ma 20131209-elibplus-announcement
        // 20141007-P64683
        $sql = "select p.AnnouncementID as id, p.Content as content, p.ContentXML as title,
			    p.AnnouncementType as type, p.DateModified as modified, p.Priority as priority, p.DateStart as date_start, p.DateEnd as date_end,
			    m.FilePath as attachment, substring_index(m.FilePath, '/', -1) as attachment_name
			from INTRANET_ELIB_BOOK_ANNOUNCEMENT  p
			left join INTRANET_ELIB_BOOK_PAGEMEDIA m on p.AnnouncementID = m.Page
			where 1 " . ($type ? "and p.AnnouncementType = '$type'" : "") . " " . ($id ? "and p.AnnouncementID='$id'" : "") . $cond . "
			order by p.Priority desc, p.DateStart desc, p.ContentXML 
			" . (isset($offset) && $amount && ! $id ? "limit $offset, " . ($amount ? $amount : 10) : ""); // "limit $offset, $amount"
        $result = $this->returnResultSet($sql);
        $annoucement = array();

        $current_date = getdate();

        foreach ($result as $key => $value) {
            $nrAnnouncement = sizeof($annoucement);
            if (($nrAnnouncement >= $amount) && ($nrAnnouncement != 0)) { // $amount - max number of record to show, treated as 0 if not set
                break;
            }
            if (($value["date_start"] == "" && $value["date_end"] == "") || ((strtotime($value["date_start"]) == "943891200" || $value["date_start"] == "") && strtotime($value["date_end"]) + 86400 >= $current_date[0]) || (strtotime($value["date_start"]) <= $current_date[0] && (strtotime($value["date_end"]) == "943891200" || $value["date_end"] == "")) || (strtotime($value["date_start"]) <= $current_date[0] && strtotime($value["date_end"]) + 86400 >= $current_date[0])) {
                $annoucement[$key] = $value;
            } else {
                if ($permission['admin']) {
                    $annoucement[$key] = $value;
                    if (strtotime($value["date_start"]) > $current_date[0]) {
                        $annoucement[$key]["not_started"] = true;
                    } else {
                        $annoucement[$key]["expired"] = true;
                    }
                }
            }
            if ($annoucement[$key]["content"] != "") {
                if ($ckeditor) { // force to open in new window
                    $annoucement[$key]["content"] = str_replace("<a href=", "<a target=\"_blank\" href=", $annoucement[$key]["content"]);
                } else {
                    $annoucement[$key]["content"] = htmlspecialchars($annoucement[$key]["content"]);
                    if ($is_link_allow) {
                        $annoucement[$key]["content"] = preg_replace('`(http[s]?:\/\/)([^ \t\r\n\v\f]*)`', '<a class="systemGen" target="_blank" href="$1$2">$1$2</a>', $annoucement[$key]["content"]);
                    }
                }
            }
        }

        return $annoucement;
    }

    public function setPortalNotice($type, $note_id, $data, $replace_attachment)
    { // 20131209-elibplus-announcement
        global $PATH_WRT_ROOT;

        extract($data);

        if ($date_start == "")
            $date_start = "NULL";

        if ($date_end == "")
            $date_end = "NULL";

        if ($note_id) { // 20141007-P64683
            $sql = "update INTRANET_ELIB_BOOK_ANNOUNCEMENT set
			Content = '$content', ContentXML = '$title', Priority = '$priority', DateStart = '$date_start', DateEnd = '$date_end'
		    where AnnouncementID = '$note_id' and AnnouncementType = '$type' ";

            $this->db_db_query($sql);
        } else {
            $sql = "insert into INTRANET_ELIB_BOOK_ANNOUNCEMENT
			(Content, AnnouncementType, DateModified, ContentXML,Priority,DateStart,DateEnd)
		    values
			('$content', '$type', now(), '$title','$priority','$date_start','$date_end')";

            $this->db_db_query($sql);

            $note_id = mysql_insert_id();
        }

        if ($attachment) {

            if ($replace_attachment) {

                $sql = "update INTRANET_ELIB_BOOK_PAGEMEDIA set
			FilePath = '$attachment'
			where Page = '$note_id' and BookID= -1";
                $this->db_db_query($sql);
            } else {

                $sql = "insert into INTRANET_ELIB_BOOK_PAGEMEDIA (BookID, Page, FilePath, DateModified)
			values (-1, '$note_id', '$attachment', now())";
                $this->db_db_query($sql);
            }
        }

        return $note_id;
    }

    public function removePortalNotice($note_id)
    { // 20131209-elibplus-announcement
        global $PATH_WRT_ROOT;

        // $sql = "delete
        // from INTRANET_ELIB_BOOK_PAGE
        // where BookPageID=$note_id and BookID= -1";

        $sql = "delete 
		from INTRANET_ELIB_BOOK_ANNOUNCEMENT
		where AnnouncementID=$note_id ";

        $this->db_db_query($sql);

        return mysql_affected_rows() + $this->removePortalNoticeAttachments($note_id);
    }

    public function removePortalNoticeAttachments($note_id)
    { // 20131209-elibplus-announcement
        $sql = "delete
		from INTRANET_ELIB_BOOK_PAGEMEDIA
		where Page='$note_id' and BookID = -1";

        $this->db_db_query($sql);

        return mysql_affected_rows();
    }

    public function getUserTheme()
    {
        $sql = "select Content from INTRANET_ELIB_USER_MYNOTES where UserID = '" . $this->user_id . "' and BookID = -1 and NoteType = 'Theme' and Category = 'background_theme'";

        $background_theme = current($this->returnVector($sql));

        if (! $background_theme) {

            $date = getdate();

            $background_theme = $date['mon'] == 12 && $date['mday'] >= 20 && $date['mday'] <= 31 ? 'theme_xmas_2012' : 'elib_bg';

            $sql = "insert into INTRANET_ELIB_USER_MYNOTES (UserID, BookID, Content, NoteType, Category) values ('" . $this->user_id . "', -1, '$background_theme', 'Theme', 'background_theme')";
            $this->db_db_query($sql);
        }

        $sql = "select Content from INTRANET_ELIB_USER_MYNOTES where UserID = '" . $this->user_id . "' and BookID = -1 and NoteType = 'Theme' and Category = 'bookshelf_theme'";

        $bookshelf_theme = current($this->returnVector($sql));

        if (! $bookshelf_theme) {

            $bookshelf_theme = 'bookshelf_type_01';

            $sql = "insert into INTRANET_ELIB_USER_MYNOTES (UserID, BookID, Content, NoteType, Category) values ('" . $this->user_id . "', -1, '$bookshelf_theme', 'Theme', 'bookshelf_theme')";
            $this->db_db_query($sql);
        }

        return array(
            'background_theme' => $background_theme,
            'bookshelf_theme' => $bookshelf_theme
        );
    }

    public function setUserTheme($background_theme, $bookshelf_theme)
    {
        $date = getdate();

        $sql = "update INTRANET_ELIB_USER_MYNOTES set Content = '$background_theme' where UserID = '" . $this->user_id . "' and BookID = -1 and NoteType = 'Theme' and Category = 'background_theme'";
        $this->db_db_query($sql);
        echo mysql_error();

        $sql = "update INTRANET_ELIB_USER_MYNOTES set Content = '$bookshelf_theme' where UserID = '" . $this->user_id . "' and BookID = -1 and NoteType = 'Theme' and Category = 'bookshelf_theme'";
        $this->db_db_query($sql);
        echo mysql_error();

        return;
    }

    /*
     * $bookType: ebook -- BookID < 10000000
     * physical -- BookID >= 10000000
     * $limit -- offset
     * $amount -- number of record to retrieve
     * $dateRange -- 0 - accumulated, 1 - this week
     */
    public function getRecommendBooks($limit, $amount = 0, $shuffle = false, $bookType = '', $dateRange = 0)
    { // 2015-05-04 Charles Ma - P77849
        if ($shuffle) {
            list ($total, $books) = $this->GET_RECOMMEND_BOOK(- 1, $bookType, 0, true, $dateRange); // unlimit, with_total
            shuffle($books);
        } else {
            list ($total, $books) = $this->GET_RECOMMEND_BOOK($amount, $bookType, $limit, true, $dateRange);
        }

        $returnArray = array();
        // $total = sizeof($books);
        $i = 0;

        foreach ($books as $book) {
            $i ++;
            if ($shuffle && ($i < $limit + 1 || $i > $limit + $amount)) {
                continue;
            }
            $sql = "SELECT a.*, ifnull(round(avg(Rating),1),'--') as rating
	                    FROM INTRANET_ELIB_BOOK a
	                    LEFT JOIN INTRANET_ELIB_BOOK_REVIEW b on a.BookID=b.BookID
	                    WHERE a.BookID='" . $book['BookID'] . "' " . $this->book_formats . "
	                    GROUP BY a.BookID"; // get all detaild from id

            $detail = current($this->returnArray($sql));
            if ($detail) {

                $entry = $this->getBooksArrayByFormat($detail);
                $entry['reason'] = $book['Description'];
                $entry['rating'] = $detail['rating'];

                $returnArray[] = $entry;
            }
        }

        return array(
            $total,
            $returnArray
        );
    }

    /*
     * $bookType: ebook -- BookID < 10000000
     * physical -- BookID >= 10000000
     * $newLimit: number of books that are defined as "new"
     */
    public function getNewBooks($limit = 5, $bookType = '', $offset = 0, $newLimit = 10, $dateRange = 0)
    {
        // 20140414-ajax-improve
        // $sql = "SELECT SQL_CALC_FOUND_ROWS a.*, ifnull(round(avg(Rating),1),'--') as rating
        // FROM INTRANET_ELIB_BOOK a
        // LEFT JOIN INTRANET_ELIB_BOOK_REVIEW b on a.BookID=b.BookID
        // WHERE a.Publish=1 ".$this->book_formats."
        // GROUP BY a.BookID
        // ORDER BY a.DateModified DESC
        // LIMIT $limit";
        $expiredBook = $this->getExpiredBookIDArr();
//         $exCond_a = (! strpos($_GET['action'], 'Portal')) ? '' : ' AND a.BookID NOT IN (' . $expiredBook . ') ';
//         $exCond_b = (! strpos($_GET['action'], 'Portal')) ? '' : ' AND b.BookID NOT IN (' . $expiredBook . ') ';
        if (! empty($expiredBook)) {
            $exCond_a = ' AND a.BookID NOT IN (' . $expiredBook . ') ';
            $exCond_b = ' AND b.BookID NOT IN (' . $expiredBook . ') ';
        } else {
            $exCond_a = '';
            $exCond_b = '';
        }
        $dateRangeCond = "";

        if ($bookType == $this->physical_book_type_code) {
            $bookTypeCond = " AND a.BookID>=" . $this->physical_book_init_value;
            $orderBy = "a.AccountDate DESC, a.DateModified DESC";
            if ($dateRange == 1) {
                $dateRangeCond = " AND a.AccountDate between (CURDATE()-INTERVAL DAYOFWEEK(CURDATE())-1 DAY) and NOW() ";
            }
        } else if ($bookType == 'ebook') {
            $bookTypeCond = " AND a.BookID<" . $this->physical_book_init_value;
            $orderBy = "a.DateModified DESC";
            if ($dateRange == 1) {
                $dateRangeCond = " AND a.DateModified between (CURDATE()-INTERVAL DAYOFWEEK(CURDATE())-1 DAY) and NOW() ";
            }
        } else {
            $bookTypeCond = "";
            $orderBy = "IFNULL(a.AccountDate, a.DateModified) DESC";
            if ($dateRange == 1) {
                $dateRangeCond = " AND IFNULL(a.AccountDate, a.DateModified) between (CURDATE()-INTERVAL DAYOFWEEK(CURDATE())-1 DAY) and NOW() ";
            }
        }

        $sql = "SELECT count(*) as cnt 
                FROM INTRANET_ELIB_BOOK a               
                WHERE a.Publish=1 " . $this->book_formats . " " . $exCond_a . $bookTypeCond . $dateRangeCond;

        $cnt_array = $this->returnResultSet($sql);
        $total_new = 0;
        if (count($cnt_array) > 0) {
            $total_new = $cnt_array[0]['cnt'];
            if ($total_new > $newLimit) {
                $total_new = $newLimit;
            }
        }

        if ($offset > $newLimit) { // out of range
            $valid_range = false;
        } else if ($offset + $limit > $newLimit) {
            $limit = $newLimit - $offset;
            $valid_range = true;
        } else {
            $valid_range = true;
        }

        if ($valid_range) {
            $sql = "SELECT * 
	                FROM INTRANET_ELIB_BOOK a               
	                WHERE a.Publish=1 " . $this->book_formats . " " . $exCond_a . $bookTypeCond . $dateRangeCond . " 
	                ORDER BY $orderBy
	                LIMIT $offset, $limit";
            $books = $this->returnArray($sql);
        } else {
            $books = array();
        }

        $book_id_arr = array();
        $rating_arr = array();

        foreach ($books as $book) {
            array_push($book_id_arr, $book["BookID"]);
        }

        if (sizeof($book_id_arr) > 0) {
            $sql = "SELECT b.BookID, ifnull(round(avg(Rating),1),'--') as rating
	                FROM INTRANET_ELIB_BOOK_REVIEW b 
	                WHERE b.BookID IN ('" . implode("','", $book_id_arr) . "')
	                $exCond_b
	                GROUP BY b.BookID";

            $ratings = $this->returnArray($sql);

            foreach ($ratings as $rating) {
                $rating_arr[$rating["BookID"]] = array();
                $rating_arr[$rating["BookID"]]["rating"] = $rating["rating"];
            }
        }

        $total = sizeof($books);

        $returnArray = array();

        foreach ($books as $book) {
            $entry = $this->getBooksArrayByFormat($book);
            // $entry['rating'] = $book['rating'];
            $entry['rating'] = $rating_arr[$book["BookID"]]['rating'];
            $returnArray[] = $entry;
        }

        // $total is current round total, $total_new is all rounds total
        return array(
            $total,
            $returnArray,
            $total_new
        );
    }

    public function getBookList($options, $offset = 0, $amount = 30, $sortby = 'random', $order = 'asc')
    {
        global $ck_eLib_encryptsalt;

        if ($sortby == 'random' || ! $sortby) {
            $sortby = "encrypt(concat(a.BookID, title), '$ck_eLib_encryptsalt')";
        } else if ($sortby == 'bookID') {
            $sortby = 'a.BookID';
        }

        foreach ((array) $options as $k => $v) {
            $options[$k] = trim($v);
        }

        $expiredBook = $this->getExpiredBookIDArr();
        if (! empty($expiredBook)) {
            $exCond_a = ' AND a.BookID NOT IN (' . $expiredBook. ') ';
        } else {
            $exCond_a = '';
        }

        $search = "";

        $unconverted_keyword = mysql_real_escape_string(str_replace("\\", "\\\\", $options['keyword'])); // A&<>'"\B ==> A&<>\'\"\\\\B
        $converted_keyword = special_sql_str($options['keyword']); // A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B

        $unconverted_category = mysql_real_escape_string(str_replace("\\", "\\\\", $options['category']));
        $converted_category = mysql_real_escape_string(intranet_htmlspecialchars(intranet_undo_htmlspecialchars($options['category'])));

        $unconverted_subcategory = mysql_real_escape_string(str_replace("\\", "\\\\", $options['subcategory']));
        $converted_subcategory = mysql_real_escape_string(intranet_htmlspecialchars(intranet_undo_htmlspecialchars($options['subcategory'])));

        $unconverted_title = mysql_real_escape_string(str_replace("\\", "\\\\", $options['title']));
        $converted_title = special_sql_str($options['title']);

        $unconverted_subtitle = mysql_real_escape_string(str_replace("\\", "\\\\", $options['subtitle']));
        $converted_subtitle = special_sql_str($options['subtitle']);

        $unconverted_author = mysql_real_escape_string(str_replace("\\", "\\\\", $options['author']));
        $converted_author = special_sql_str($options['author']);

        $unconverted_subject = mysql_real_escape_string(str_replace("\\", "\\\\", $options['subject']));
        $converted_subject = special_sql_str($options['subject']);

        $unconverted_publisher = mysql_real_escape_string(str_replace("\\", "\\\\", $options['publisher']));
        $converted_publisher = special_sql_str($options['publisher']);

        $unconverted_isbn = mysql_real_escape_string(str_replace("\\", "\\\\", $options['isbn']));
        $converted_isbn = special_sql_str($options['isbn']);

        $unconverted_call_number = mysql_real_escape_string(str_replace("\\", "\\\\", $options['call_number']));
        $converted_call_number = special_sql_str($options['call_number']);

        $unconverted_level = mysql_real_escape_string(str_replace("\\", "\\\\", $options['level']));
        $converted_level = intranet_htmlspecialchars(intranet_undo_htmlspecialchars($options['level']));

        $unconverted_tag_id = mysql_real_escape_string(str_replace("\\", "\\\\", $options['tag_id']));
        $converted_tag_id = intranet_htmlspecialchars(intranet_undo_htmlspecialchars($options['tag_id']));

        $unconverted_introduction = mysql_real_escape_string(str_replace("\\", "\\\\", $options['introduction']));
        $converted_introduction = special_sql_str($options['introduction']);

        $converted_category_keyword = str_replace("\\", "\\\\\\\\", intranet_htmlspecialchars($options['keyword']));

        if ($options['language']) {
            if ($options['language'] == 'others_1') {
                $search .= " AND (a.BookCategoryType = '1' OR a.BookCategoryType IS NULL OR a.BookCategoryType='')";
            } else if ($options['language'] == 'others_2') {
                $search .= " AND a.BookCategoryType = '2'";
            } else {
                $search .= " AND Language = '" . $options['language'] . "'";
            }
        }

        if ($options['book_category_type']) {
            if ($options['book_category_type'] == '1') {
                $search .= " AND (a.BookCategoryType = '1' OR a.BookCategoryType IS NULL OR a.BookCategoryType='')";
            } else {
                $search .= " AND a.BookCategoryType = '" . $options['book_category_type'] . "'";
            }
        }

        if ($options['category'])
            $search .= " AND (Category = '" . $unconverted_category . "' OR Category = '" . $converted_category . "')";
        if ($options['subcategory'])
            $search .= " AND (trim(SubCategory) = '" . $unconverted_subcategory . "' OR trim(SubCategory) = '" . $converted_subcategory . "')";
        if ($options['keyword']) {
            if ($unconverted_keyword == $converted_keyword) {
                $words = getWords($unconverted_keyword);
                $search .= " AND (";
                $search .= "(Title like '%" . implode("%' AND Title LIKE'%", $words) . "%') OR ";
                $search .= "(SubTitle like '%" . implode("%' AND SubTitle LIKE'%", $words) . "%') OR ";
                $search .= "(Author like '%" . implode("%' AND Author LIKE'%", $words) . "%') OR ";
                $search .= "(Publisher like '%" . implode("%' AND Publisher LIKE'%", $words) . "%') OR ";
                $search .= "Category like '%" . $unconverted_keyword . "%' OR ";
                $search .= "trim(SubCategory) like '%" . $unconverted_keyword . "%' OR ";
                $search .= "(TagName like '%" . implode("%' AND TagName LIKE'%", $words) . "%') OR ";
                $search .= "(a.SeriesEditor like '%" . implode("%' AND a.SeriesEditor LIKE'%", $words) . "%') OR ";
                $search .= "a.CallNumber like '%" . $unconverted_keyword . "%' OR ";
                $search .= "CONCAT('|=|', a.ACNOList, '|=|') like '%|=|" . $unconverted_keyword . "|=|%'";
                $search .= ")";
            } else {
                $words_1 = getWords($unconverted_keyword);
                $words_2 = getWords($converted_keyword);

                $search .= " AND (";
                $search .= "(Title like '%" . implode("%' AND Title LIKE'%", $words_1) . "%') OR (Title like '%" . implode("%' AND Title LIKE'%", $words_2) . "%') OR ";
                $search .= "(SubTitle like '%" . implode("%' AND SubTitle LIKE'%", $words_1) . "%') OR (SubTitle like '%" . implode("%' AND SubTitle LIKE'%", $words_2) . "%') OR ";
                $search .= "(Author like '%" . implode("%' AND Author LIKE'%", $words_1) . "%') OR (Author like '%" . implode("%' AND Author LIKE'%", $words_2) . "%') OR ";
                $search .= "(Publisher like '%" . implode("%' AND Publisher LIKE'%", $words_1) . "%') OR (Publisher like '%" . implode("%' AND Publisher LIKE'%", $words_2) . "%') OR ";
                $search .= "Category like '%" . $unconverted_keyword . "%' OR Category like '%" . $converted_category_keyword . "%' OR ";
                $search .= "trim(SubCategory) like '%" . $unconverted_keyword . "%' OR trim(SubCategory) like '%" . $converted_category_keyword . "%' OR ";
                $search .= "(TagName like '%" . implode("%' AND TagName LIKE'%", $words_1) . "%') OR (TagName like '%" . implode("%' AND TagName LIKE'%", $words_2) . "%') OR ";
                $search .= "(a.SeriesEditor like '%" . implode("%' AND a.SeriesEditor LIKE'%", $words_1) . "%') OR (a.SeriesEditor like '%" . implode("%' AND a.SeriesEditor LIKE'%", $words_2) . "%') OR ";
                $search .= "a.CallNumber like '%" . $unconverted_keyword . "%' OR a.CallNumber like '%" . $converted_keyword . "%' OR ";
                $search .= "CONCAT('|=|', a.ACNOList, '|=|') like '%|=|" . $unconverted_keyword . "|=|%'";
                $search .= ")";
            }
        }
        if ($options['tag_id'])
            $search .= " AND (d.TagID='" . $unconverted_tag_id . "' OR d.TagID='" . $converted_tag_id . "')";
        if ($options['title'])
            $search .= " AND (a.Title like '%" . $unconverted_title . "%' OR a.Title like '%" . $converted_title . "%')";
        if ($options['subtitle'])
            $search .= " AND (a.SubTitle like '%" . $unconverted_subtitle . "%' OR a.SubTitle like '%" . $converted_subtitle . "%')";
        if ($options['author'])
            $search .= " AND (a.Author like '%" . $unconverted_author . "%' OR a.Author like '%" . $converted_author . "%')";
        if ($options['subject'])
            $search .= " AND (a.RelevantSubject like '%" . $unconverted_subject . "%' OR a.RelevantSubject like '%" . $converted_subject . "%')";
        if ($options['publisher'])
            $search .= " AND (a.Publisher like '%" . $unconverted_publisher . "%' OR a.Publisher like '%" . $converted_publisher . "%')";
        if ($options['isbn'])
            $search .= " AND (a.ISBN like '%" . $unconverted_isbn . "%' OR a.ISBN like '%" . $converted_isbn . "%')";
        if ($options['call_number'])
            $search .= " AND (a.CallNumber like '%" . $unconverted_call_number . "%' OR a.CallNumber like '%" . $converted_call_number . "%')";
        if ($options['level'])
            $search .= " AND (a.Level like '%" . $unconverted_level . "%' OR a.Level like '%" . $converted_level . "%')";
        if ($options['introduction'])
            $search .= " AND (a.Preface like '%" . $unconverted_introduction . "%' OR a.Preface like '%" . $converted_introduction . "%')";

        $extra_join = "";
        if ($options['class_level_id']) {
            $search .= " AND (FIND_IN_SET('" . $options['class_level_id'] . "',replace(r.RecommendGroup,'|',','))> 0)";
            $extra_join .= " LEFT JOIN INTRANET_ELIB_BOOK_RECOMMEND r ON r.BookID=a.BookID ";
        }

        $limit = $offset . ',' . $amount;

        $sql = "SELECT SQL_CALC_FOUND_ROWS a.*, ifnull(round(avg(Rating),1),'--') as rating
                FROM INTRANET_ELIB_TAG c 
                INNER JOIN INTRANET_ELIB_BOOK_TAG d on d.TagID=c.TagID
                RIGHT JOIN INTRANET_ELIB_BOOK a on a.BookID=d.BookID 
                LEFT JOIN INTRANET_ELIB_BOOK_REVIEW b on a.BookID=b.BookID
				$extra_join
                WHERE 1 $search
                AND Publish = 1
                " . $this->book_formats . "
                $exCond_a
                GROUP BY a.BookID
                ORDER BY $sortby $order 
                LIMIT $limit";

        if ($this->convert_utf8)
            $this->dbConnectionUTF8();
        $books = $this->returnArray($sql);
        $total = current($this->returnVector('select found_rows()'));
        // debug_pr($sql);
        $returnArray = array();
        foreach ($books as $i => $book) {
            $entry = $this->getBooksArrayByFormat($book);
            $entry['rating'] = $book['rating'];
            if ($entry['type'] != 'physical' || $entry['copy_count'] > 0)
                $returnArray[] = $entry;
        }

        return array(
            $total,
            $returnArray
        );
    }

    public function getBookDetail($ParArr = "")
    {
        $sql = "SELECT *
                FROM 
                INTRANET_ELIB_BOOK 
                WHERE BookID = '" . $this->book_id."' and Publish=1";

        $books = $this->returnArray($sql);

        $returnArray = $this->getBooksArrayByFormat($books[0]);

        $sql = "SELECT t.TagName as tag_name, t.TagID as tag_id FROM INTRANET_ELIB_TAG t, INTRANET_ELIB_BOOK_TAG bt WHERE bt.BookID='" . $this->book_id . "' AND t.TagID=bt.TagID";

        $returnArray['tags'] = $this->returnArray($sql);

        // if(mb_strlen($returnArray['description'],"UTF-8") > 250 ){
        // $returnArray['description'] = mb_substr($returnArray['description'], 0,250,"UTF-8")."...\"";
        // }

        return $returnArray;
    }

    // added to check if the book is borrowed or not (Henry)
    private function getPhysicalBookLoanCount($book_id)
    {
        $sql = "SELECT count(*)
                    from " . $this->libmsDB . ".LIBMS_BORROW_LOG a
                    WHERE BookID='" . ($book_id - $this->physical_book_init_value)."'";

        return current($this->returnVector($sql));
    }

    // function added to check the total Loan book (Henry)
    private function getPhysicalBookCurrentLoanCount($book_id)
    {
        $sql = "SELECT count(*)
                    from " . $this->libmsDB . ".LIBMS_BORROW_LOG a
                    WHERE BookID='" . ($book_id - $this->physical_book_init_value) . "' AND RecordStatus = 'BORROWED'";

        return current($this->returnVector($sql));
    }

    // added the check RecordStatus = 'READY' if the book is borrowed or not (Henry)
    private function getPhysicalBookReservedCount($book_id)
    {
        $sql = "SELECT count(*)
                from " . $this->libmsDB . ".LIBMS_RESERVATION_LOG 
                WHERE BookID = '" . ($book_id - $this->physical_book_init_value) . "' and (RecordStatus = 'WAITING' OR RecordStatus = 'READY')";
        return current($this->returnVector($sql));
    }

    private function getBooksArrayByFormat($book)
    {
        global $eLib_plus;

        $elib_install = new elibrary_install();
        $isbns = array();

        $returnArray = array();

        if ($book['BookFormat'] == $this->physical_book_type_code) {

            if ($this->convert_utf8)
                $this->dbConnectionLatin1();

            $physicalBook = $this->GET_PHYSICAL_BOOK_INFO($book['BookID']);

            if ($this->convert_utf8)
                $this->dbConnectionUTF8();

            $authors = array();
            if ($physicalBook['ResponsibilityBy1'])
                $authors[] = $physicalBook['ResponsibilityBy1'];
            if ($physicalBook['ResponsibilityBy2'])
                $authors[] = $physicalBook['ResponsibilityBy2'];
            if ($physicalBook['ResponsibilityBy3'])
                $authors[] = $physicalBook['ResponsibilityBy3'];

            $title = $physicalBook['BookTitle'];
            $sub_title = $physicalBook['BookSubTitle'];
            $publisher = $physicalBook['Publisher'];
            $publisher_place = $physicalBook['PublishPlace'];
            $image = $physicalBook['CoverImage'];
            $status = $physicalBook['OpenBorrow'];
            $description = $physicalBook['Introduction'];
            $language = $physicalBook['Language'];
            $edition = $physicalBook['Edition'];
            $publish_year = $physicalBook['PublishYear'];
            $location = $physicalBook['Location'];
            $no_of_copy = $physicalBook['NoOfCopy'];
            $available_item_list = $physicalBook['AvailableItem'];
            $borrowed_item_list = $physicalBook['BorrowedItem'];

            $no_of_copy_available = $physicalBook['NoOfCopyAvailable'];
            $loan_count = $this->getPhysicalBookLoanCount($book['BookID']);
            // Henry added
            $current_loan_count = $this->getPhysicalBookCurrentLoanCount($book['BookID']);
            $reserved_count = $this->getPhysicalBookReservedCount($book['BookID']);
            $type = 'physical';
            $callnumber = $this->GetCallNumber($physicalBook['CallNum'], $physicalBook['CallNum2']);
            if ($this->isBorrowedByUser($book['BookID'] - $this->physical_book_init_value)) {
                $user_status = 'borrowed';
            } else if ($this->isReservedByUser($book['BookID'] - $this->physical_book_init_value)) {
                $user_status = 'reserved';
            } else {
                $user_status = '';
            }

            if ($physicalBook['ISBN'])
                $isbns[] = $physicalBook['ISBN'];
            if ($physicalBook['ISBN2'])
                $isbns[] = $physicalBook['ISBN2'];
            $no_of_page = $physicalBook['NoOfPage'];
            $subject = $physicalBook['Subject'];
            $physical_book_url = $physicalBook['URL'];
        } else {
            $title = $book['Title'];
            $authors = array(
                $book['Author']
            );
            $publisher = $book['Publisher'];
            $image = $this->getBookImage($book['BookID']);
            $description = $book['Preface'];
            $type = ($elib_install->check_if_readable($book['BookID'])) ? ($book['BookFormat'] == BOOK_FORMAT_EPUB2 ? 'html5' : 'flash') : 'expired';
            $url = $elib_install->get_book_reader($book['BookID'], $book['BookFormat'], $book['IsTLF'], $this->user_id);
            $language = $eLib_plus["html"][$book['language']];
            $hit_rate = (int) $book['HitRate'];
            $callnumber = $book['CallNumber'];
            if($book['ISBN']){
                $isbns[] = $book['ISBN'];
            }
            $no_of_page = '';
            $subject = $book['RelevantSubject'];
            $physical_book_url = '';
        }

        $returnArray = array(

            'id' => $book['BookID'],
            'title' => $title,
            'sub_title' => $sub_title,
            'authors' => $authors,
            'author' => implode(', ', $authors),
            'publisher' => $publisher,
            'image' => $image,
            'url' => $url,
            'status' => $status,
            'description' => nl2br($description),
            'location' => $location,
            'hit_rate' => $hit_rate,
            // Siuwan Added
            'publish_year' => $publish_year,
            'publisher_place' => $publisher_place,
            'available_item_list' => (array) $available_item_list,
            'borrowed_item_list' => (array) $borrowed_item_list,

            // Henry Added
            'current_loan_count' => $current_loan_count,
            'loan_count' => $loan_count,
            'category' => $book['Category'],
            'subcategory' => $book['SubCategory'],
            'level' => $book['Level'],
            'language' => $language,
            'edition' => $edition,
            'type' => $type,
            'user_status' => $user_status,
            'available_count' => $no_of_copy_available,
            'copy_count' => $no_of_copy,
            'reserved_count' => $reserved_count,
            'callnumber' => $callnumber,
            'isbns' => $isbns,
            'no_of_page' => $no_of_page,
            'subject' => $subject,
            'physical_book_url' => $physical_book_url,
            'book_type' => ($book['BookID'] > $this->physical_book_init_value) ? 'pbook' : 'ebook'
        );

        return $returnArray;
    }

    private function getBookImage($book_id)
    {
        global $PATH_WRT_ROOT;
        $expiredBook = $this->getExpiredBookIDArr(true);
        if (in_array($book_id, $expiredBook)) {
            $image = null;
        } else {
            $image = '/file/elibrary/content/' . $book_id . '/image/cover.jpg';
        }
        return file_exists($PATH_WRT_ROOT . $image) ? $image : null;
    }

    public function getBookStat()
    {
        $sql = "SELECT ifnull(round(avg(Rating),1),'--') as rating, count(distinct(ReviewID)) as count FROM INTRANET_ELIB_BOOK_REVIEW WHERE BookID = '" . $this->book_id . "' ";
        $reviews = $this->returnArray($sql, 5);

        $returnArray['rating'] = $reviews[0]['rating'];
        $returnArray['review_count'] = $reviews[0]['count'];

        $sql = "SELECT HitRate as hit_count, BookFormat FROM INTRANET_ELIB_BOOK WHERE BookID = '" . $this->book_id."'";
        $book = $this->returnArray($sql);

        $sql = "SELECT COUNT(*) FROM INTRANET_ELIB_BOOK_HISTORY WHERE BookID='" . $this->book_id."'";
        $rows = $this->returnVector($sql);

        $book[0]['hit_count'] = $rows[0];

        if ($book[0]['BookFormat'] == $this->physical_book_type_code) {

            $returnArray['loan_count'] = $this->getPhysicalBookLoanCount($this->book_id);
            $returnArray['status'] = current($this->returnVector("SELECT OpenReservation from " . $this->libmsDB . ".LIBMS_BOOK WHERE BookID='" . $this->physical_book_id."'"));
        } else {
            $returnArray['hit_rate'] = $book[0]['hit_count'];
        }

        $returnArray['is_reserved'] = $this->isReservedByUser();
        $returnArray['is_favourite'] = $this->isFavourite();

        return $returnArray;
    }

    public function isFavourite()
    {
        return $this->IS_MY_FAVOURITE_BOOK(array(
            "BookID" => $this->book_id,
            "currUserID" => $this->user_id
        ));
    }

    // Added the condition RecordStatus <> "BORROWED"
    public function isReservedByUser($physical_book_id = false)
    {
        if (! $physical_book_id)
            $physical_book_id = $this->physical_book_id;

        // $sql = "SELECT count(*) from ".$this->libmsDB.".LIBMS_RESERVATION_LOG
        // where BookID=$physical_book_id AND UserID=".$this->user_id." AND RecordStatus <> 'BORROWED'";
        $sql = "SELECT count(*) from " . $this->libmsDB . ".LIBMS_RESERVATION_LOG
                where BookID='$physical_book_id' AND UserID='" . $this->user_id . "' AND (RecordStatus = 'READY' OR RecordStatus = 'WAITING')";

        return current($this->returnVector($sql));
    }

    public function isBorrowedByUser($physical_book_id = false)
    {
        if (! $physical_book_id)
            $physical_book_id = $this->physical_book_id;

        $sql = "SELECT count(*) from " . $this->libmsDB . ".LIBMS_BORROW_LOG
                where BookID='$physical_book_id' AND RecordStatus = 'BORROWED' AND UserID='" . $this->user_id."'";

        return current($this->returnVector($sql));
    }

    public function getBookReviews($offset = 0, $amount = 10, $ReviewID = '')
    {
        $sql = "SELECT a.ReviewID as id, a.UserID as user_id, a.Rating as rating, a.Content as content,
                a.DateModified as date,
                count(c.ReviewCommentID) as likes
                FROM  INTRANET_ELIB_BOOK_REVIEW a
                left join INTRANET_ELIB_BOOK_REVIEW_HELPFUL c on a.ReviewID=c.ReviewID AND c.Choose=1
                WHERE ".($this->book_id ? "a.BookID = '" . $this->book_id."'" : "1") . ($ReviewID ? " AND a.ReviewID='{$ReviewID}'" : "") . "
                GROUP BY a.ReviewID
                ORDER BY a.ReviewID DESC
		LIMIT $offset, $amount
		";

        $reviews = $this->returnArray($sql);

        foreach ($reviews as &$review) {

            $review['content'] = strip_tags($review['content']);

            list ($review['name'], $review['class'], $review['image']) = $this->getUserInfo($review['user_id']);
        }

        return $reviews;
    }

    public function removeReviews($review_id)
    {
        return $this->REMOVE_BOOK_REVIEW(false, $review_id);
    }

    // 2013-11-14 Charles Ma 2013-11-14-deleteallbookcomment
    public function removeAllReviews($book_id)
    {
        return $this->REMOVE_BOOK_REVIEW($book_id, false);
    }

    public function addReview($content, $rate)
    {
        return $this->addBookReview(array(
            'Rating' => $rate,
            "BookID" => $this->book_id,
            "Content" => $content
        ));
    }

    public function addOrRemoveReservation()
    {
        global $intranet_root, $intranet_session_language, $Lang, $UserID, $eLib_plus;
        include_once ($intranet_root . "/home/library_sys/management/circulation/TimeManager.php");
        include_once ($intranet_root . "/home/library_sys/management/circulation/User.php");
        include_once ($intranet_root . "/home/library_sys/management/circulation/RightRuleManager.php");

        $libms = new liblms();
        $User = new User($UserID, $libms);

        $MSG = "";

        // check if the book is reserved by the user or not
        $sql = "SELECT ReservationID, BookUniqueID, BookID FROM " . $this->libmsDB . ".LIBMS_RESERVATION_LOG
                WHERE BookID='" . $this->physical_book_id . "' AND UserID='" . $this->user_id . "' AND RecordStatus<>'DELETE'";
        $reservationAry = $this->returnResultSet($sql);
        if (count($reservationAry)) {
            $_reservationID = array();
            foreach((array)$reservationAry as $_reservationAry) {
                $_reservationID[] = $_reservationAry['ReservationID'];
            }
            $notifyNextUser = $this->notifcation_settings['reserve'];
            $result = $libms->REMOVE_RESERVATION($_reservationID, $notifyNextUser);
            return $result ? 0 : 1;
        }
        else {
            $canReserve = $User->rightRuleManager->canReserve($this->physical_book_id, $MSG, $pendingBookID='', $checkAvailableBookReservation=true);
            if (!$canReserve) { // get fail reason
                switch ($MSG) {
                    case $Lang["libms"]["CirculationManagement"]["msg"]["Hold_Book"]:
                        $ret = -5;
                        break;
                    case $eLib_plus["html"]["cannothold_book_available"]:
                        $ret = -4;
                        break;
                    case $Lang["libms"]["CirculationManagement"]["msg"]["cannot_reserve"]:
                    case $Lang["libms"]["CirculationManagement"]["msg"]["cannot_reserve_no_copy"]:
                        $ret = -3;
                        break;
                    case $Lang["libms"]["CirculationManagement"]["msg"]["over_reserve_limit"]:
                        $ret = -2;
                        break;
                    case $Lang["libms"]["CirculationManagement"]["msg"]["exceeds_reserve"]:
                        $ret = -6;
                        break;
                    default:
                        $ret = -7;
                        break;
                }
                return $ret;
            }
            else {
                $result = $User->reserve_book($this->physical_book_id);
                return $result;
            }
        }


//         // check the book is ready or not (Henry added[20150213])
//         $sql = "SELECT COUNT(*) FROM " . $this->libmsDB . ".LIBMS_RESERVATION_LOG
//                 where BookID=" . $this->physical_book_id . ' AND UserID=' . $this->user_id . ' AND RecordStatus = \'READY\'';
//         $hasReadyBook = current($this->returnVector($sql));

//         // Added the cond AND (RecordStatus = \'READY\' OR RecordStatus = \'WAITING\' (Henry)
//         $sql = "DELETE FROM " . $this->libmsDB . ".LIBMS_RESERVATION_LOG
//                 where BookID=" . $this->physical_book_id . ' AND UserID=' . $this->user_id . ' AND (RecordStatus = \'READY\' OR RecordStatus = \'WAITING\')';
//         $this->db_db_query($sql);
//         $is_removed = $this->db_affected_rows();

//         $canReserve = $User->rightRuleManager->canReserve($this->physical_book_id, $MSG);

//         if (! $canReserve && ! $is_removed) {
//             return - 3;
//         }

//         $sql = "SELECT b.BookTitle, b.NoOfCopyAvailable-count(a.ReservationID) from " . $this->libmsDB . ".LIBMS_BOOK b
//                 LEFT JOIN " . $this->libmsDB . ".LIBMS_RESERVATION_LOG a on a.BookID=b.BookID
//                 where b.BookID=" . $this->physical_book_id . "
//                 GROUP BY b.BookID"; // see if any no of reservation > no of book copies

//         list ($book_title, $remain_copy) = current($this->returnArray($sql));

//         if (! $is_removed) { // add reservation

//             $physicalBook = $this->GET_PHYSICAL_BOOK_INFO($this->book_id);

//             // disable reserve when the user have borrowed this book (Henry)
//             /*
//              * should be catered by canReserve()
//              * foreach($this->getUserLoanBooksCurrent() as $loanBook){
//              * if($loanBook['id'] == $this->book_id){
//              * return -1;
//              * }
//              * }
//              */
//             /*
//              * should be catered by canReserve()
//              * if($this->getUserReserveQuota($physicalBook['CirculationTypeCode']) <= sizeof($this->getUserReservedBooks())) return -2;
//              */
//             if (! $physicalBook['OpenReservation'])
//                 return - 3;
//             // disable reserve when there is at least 1 book in the lidrary (Henry)
// //             if (($physicalBook['NoOfCopyAvailable'] - $this->getPhysicalBookReservedCount($this->book_id)) > 0)
// //                 return - 4;
//             // disable reserve when there is no book copy (Henry)
//             if ($physicalBook['NoOfCopy'] == 0)
//                 return - 5; // Henry:2013/10/22
//                                                               // $this->getUserLoanBooksCurrent();
//             $status = ($remain_copy > 0) ? 'READY' : 'WAITING'; // get next reservation status
//             $notify_user = ($remain_copy > 0) ? $this->user_id : false;

//             $sql = "INSERT INTO " . $this->libmsDB . ".LIBMS_RESERVATION_LOG
//                     (BookID, UserID, ReserveTime, RecordStatus)
//                     VALUES
//                     ('" . $this->physical_book_id . "', '" . $this->user_id . "', now(), '$status')";
//             $this->db_db_query($sql);
//         }else/* if ($remain_copy>=0)*/{ // update queuing reservation
                                        // To set the book status is normal when no other user reserved and borrowed (Henry)
//             $sql = "SELECT count(*) FROM " . $this->libmsDB . ".LIBMS_RESERVATION_LOG
// 			    WHERE BookID = '" . $this->physical_book_id . "' AND (RecordStatus='WAITING')"; // 20130916 //20131008 remove ready cond
//             $temp = current($this->returnVector($sql));     // check if there's reserved record of the book in reserve log
//             $sql = "Select count(*) From " . $this->libmsDB . ".LIBMS_BOOK_UNIQUE where BookID = '" . $this->physical_book_id . "' AND RecordStatus='RESERVED'";
//             $temp1 = current($this->returnVector($sql));    // check if there's reserved record of the book in book unique
//             if ($temp == 0 && $temp1 > 0) {
//                 $sql = "UPDATE " . $this->libmsDB . ".LIBMS_BOOK_UNIQUE SET
// 			    RecordStatus = 'NORMAL', DateModified = now()
// 			    where BookID = " . $this->physical_book_id . " AND RecordStatus = 'RESERVED' LIMIT 1"; // Henry 20131007
//                 $this->db_db_query($sql);
//                 // Henry 20131007
//                 // $sql = "UPDATE ".$this->libmsDB.".LIBMS_BOOK Set NoOfCopyAvailable = (NoOfCopyAvailable + 1) WHERE BookID = ".$this->physical_book_id;
//                 // $this->db_db_query($sql);
//             }

//             // cancel reservation of the book by user while the book is available to pick up (READY), therefore, should transfer the
//             // READY status to the first user in waiting list
//             if ($hasReadyBook > 0) { // henry added [20150213]
//                 $sql = "SELECT UserID, ReservationID FROM " . $this->libmsDB . ".LIBMS_RESERVATION_LOG
// 				    WHERE BookID = '" . $this->physical_book_id . "' AND RecordStatus='WAITING'
// 				    ORDER BY ReserveTime ASC LIMIT 1";

//                 list ($notify_user, $reservation_id) = current($this->returnArray($sql));
//                 // should update the expire date (Henry modified)
//                 $sql = "SELECT `Value` FROM " . $this->libmsDB . ".`LIBMS_SYSTEM_SETTING` WHERE name='max_book_reserved_limit'";
//                 $result = $this->returnArray($sql);
//                 $timeManager = new TimeManager(new liblms());
//                 if (empty($result)) {
//                     $day_limit = 0;
//                 } else
//                     $day_limit = $result[0][0];
//                 if ($day_limit > 0)
//                     $newExpiryDate = date('Y-m-d', $timeManager->findOpenDate(strtotime('+' . $day_limit . ' day')));
//                 else
//                     $newExpiryDate = "0000-00-00";
//                 // $sql = "UPDATE ".$this->libmsDB.".LIBMS_RESERVATION_LOG SET
//                 // RecordStatus = 'READY', DateModified = now()
//                 // WHERE ReservationID = $reservation_id";
//                 $sql = "UPDATE " . $this->libmsDB . ".LIBMS_RESERVATION_LOG SET
// 				    RecordStatus = 'READY', DateModified = now(), ExpiryDate = '" . $newExpiryDate . "'
// 				    WHERE ReservationID = $reservation_id";

//                 $this->db_db_query($sql);
//             }
//         }

//         if ($notify_user && $this->notifcation_settings['reserve']) { // off temporary
//                                                                      // elibrary_plus::sendReservationEmail(array($notify_user), $book_title, $newExpiryDate);
//             if ($newExpiryDate == 0) { // Henry Modified 20130916
//                 do_email(array(
//                     $notify_user
//                 ), $Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['subject'], sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body'], $book_title));
//             } else {
//                 do_email(array(
//                     $notify_user
//                 ), $Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['subject'], sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body_with_due_date'], $book_title, $newExpiryDate));
//             }
//         }

//         return $is_removed ? 0 : 1;
    }

    public function addOrRemoveFavourite()
    {
        $sql = "DELETE FROM INTRANET_ELIB_BOOK_MY_FAVOURITES
		WHERE BookID='" . $this->book_id . "' AND UserID='" . $this->user_id."'";

        $this->db_db_query($sql);

        if ($this->db_affected_rows() == 0) {

            $sql = "INSERT INTO INTRANET_ELIB_BOOK_MY_FAVOURITES
		    (BookID, UserID, DateModified)
                    VALUES
		    ('" . $this->book_id . "', '" . $this->user_id . "', now())";

            $this->db_db_query($sql);

            return true;
        }

        return false;
    }

    public function addFavourite()
    {
        $sql = "INSERT IGNORE INTO INTRANET_ELIB_BOOK_MY_FAVOURITES
		    (BookID, UserID, DateModified)
                    VALUES
		    ('" . $this->book_id . "', '" . $this->user_id . "', now())";
        $ret = $this->db_db_query($sql);
        return $ret;
    }

    public function removeFavourite()
    {
        $sql = "DELETE FROM INTRANET_ELIB_BOOK_MY_FAVOURITES
			WHERE BookID='" . $this->book_id . "' AND UserID='" . $this->user_id."'";
        $ret = $this->db_db_query($sql);
        return $ret;
    }

    public function likeOrUnlikeReview($reviewID)
    {
        $sql = "DELETE FROM INTRANET_ELIB_BOOK_REVIEW_HELPFUL
		WHERE BookID='" . $this->book_id . "' AND UserID='" . $this->user_id . "' and ReviewID='$reviewID'";

        $this->db_db_query($sql);

        if ($this->db_affected_rows() == 0) {

            $this->addBookReviewHelpful(array(
                'ReviewID' => $reviewID,
                "BookID" => $this->book_id,
                "Choose" => 1
            ));

            return true;
        }

        return false;
    }

    public function addOrEditRecommend($class_level_ids, $content, $mode, $book_id = '')
    { // 20140326-deletebookrecommend
        $class_level_ids = $class_level_ids == NULL ? array() : $class_level_ids;
        $parArr = array(
            'Description' => htmlspecialchars($content),
            'ClassLevelID' => implode('|', $class_level_ids),
            'currUserID' => $this->user_id,
            'BookID' => ($book_id ? $book_id : $this->book_id)
        );

        $rows = $this->editBookRecommend($parArr);

        if ($this->db_affected_rows() == 0 && $class_level_ids != array()) {
            $ret = $this->addBookRecommend($parArr);
            return $ret;
        } else if ($rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function IsRenewalAllowed($borrow_id)
    {
        $sql1 = "SELECT count(*) from " . $this->libmsDB . ".LIBMS_RESERVATION_LOG b INNER JOIN " . $this->libmsDB . ".LIBMS_BORROW_LOG l ON l.BookID = b.BookID WHERE BorrowLogID = '$borrow_id' AND (b.RecordStatus = 'WAITING')";
        $temp = current($this->returnVector($sql1));
        if ($temp > 0) {
            return - 4;
        }

        $sql = "SELECT b.CirculationTypeCode, l.UniqueID, l.RenewalTime from " . $this->libmsDB . ".LIBMS_BORROW_LOG l
			LEFT JOIN " . $this->libmsDB . ".LIBMS_BOOK b ON l.BookID = b.BookID
			WHERE l.BorrowLogID='{$borrow_id}' ";
        $rows = current($this->returnArray($sql));
        list ($circulation_code, $bookUniqieID, $RenewalTime) = $rows;
        if (sizeof($rows) <= 0) {
            return 0;
        }

        $renew_quota = $this->getUserRenewQuota($circulation_code);
        $return_duration = $this->getUserReturnDuration($circulation_code);

        if ($renew_quota == 0 || $RenewalTime >= $renew_quota) {
            // exceeds renewal time
            return - 3;
        } else {
            return 1;
        }
    }

    // Modified to disallow user renew the book whe it is reserved (Henry)
    public function renewBook($borrow_id)
    {
        global $intranet_root, $intranet_session_language, $UserID, $User; // 20140625-getGroupEndDategetPatronPeriod
        include_once ("$intranet_root/home/library_sys/management/circulation/TimeManager.php");

        $sql1 = "SELECT count(*) from " . $this->libmsDB . ".LIBMS_RESERVATION_LOG b INNER JOIN " . $this->libmsDB . ".LIBMS_BORROW_LOG l ON l.BookID = b.BookID WHERE BorrowLogID = '$borrow_id' AND (b.RecordStatus = 'WAITING')";
        $temp = current($this->returnVector($sql1));
        if ($temp > 0) {
            return 0;
        }

        $sql = "SELECT b.CirculationTypeCode, l.UniqueID, l.RenewalTime, l.DueDate from " . $this->libmsDB . ".LIBMS_BORROW_LOG l
			LEFT JOIN " . $this->libmsDB . ".LIBMS_BOOK b ON l.BookID = b.BookID
			WHERE l.BorrowLogID='{$borrow_id}' ";
        $rows = current($this->returnArray($sql));
        list ($circulation_code, $bookUniqieID, $RenewalTime, $dueDate) = $rows;
        if (sizeof($rows) <= 0 || $dueDate != "" && date("Y-m-d") > $dueDate) {
            return 0;
        }

        $renew_quota = $this->getUserRenewQuota($circulation_code);
        $return_duration = $this->getUserReturnDuration($circulation_code);
        $libms = new liblms();

        if ($renew_quota == 0 || $RenewalTime >= $renew_quota) {
            // exceeds renewal time
            return - 3;
        }
        /*
         * $timeManager = new TimeManager($libms);
         *
         * $strToday = date("Y-m-d");
         * if ($libms->get_system_setting("circulation_duedate_method"))
         * {
         * $dueDate = $timeManager->findOpenDateSchoolDays(strtotime($strToday), $return_duration);
         * } else
         * {
         * $dueDate = $timeManager->findOpenDate(strtotime("{$strToday} + {$return_duration} days"));
         * }
         *
         * $dueDate = date('Y-m-d', $dueDate);
         */

        /*
         * $sql = "UPDATE ".$this->libmsDB.".LIBMS_BORROW_LOG
         * SET
         * DueDate='{$dueDate}',
         * RenewalTime=RenewalTime+1,
         * DateModified=NOW(),
         * LastModifiedBy=".$this->user_id."
         * WHERE
         * BorrowLogID = $borrow_id AND
         * RenewalTime < $renew_quota AND
         * RecordStatus = 'BORROWED'
         * ";
         * $this->db_db_query($sql);
         * return $this->db_affected_rows();
         */

        $User = new User($UserID, $libms);

        if (! empty($bookUniqieID)) {
            $result = $User->renew_book($bookUniqieID, true);
        }

        return $result;
    }

    public function getBookTags()
    {
        $sql = "SELECT t.TagName as title, t.TagID as tag_id, COUNT(bt.TagID) AS count
                FROM INTRANET_ELIB_TAG t
                LEFT JOIN INTRANET_ELIB_BOOK_TAG bt ON t.TagID=bt.TagID
                LEFT JOIN INTRANET_ELIB_BOOK AS b ON bt.BookID=b.BookID
                WHERE Publish = 1 " . $this->book_formats . " AND TRIM(t.TagName) <> '' AND t.TagName IS NOT NULL
                GROUP BY t.TagID ORDER BY count DESC LIMIT 500";

        if ($this->convert_utf8)
            $this->dbConnectionUTF8();

        $Results = $this->returnArray($sql);
        return $Results;
    }

    public function getBookLevels()
    {
        $sql = "SELECT Distinct Level FROM INTRANET_ELIB_BOOK where Level is not null and Level <> '' ORDER BY Level ";

        return $this->returnVector($sql);
    }

    public function getBookCategoryType()
    {
        $sql = "SELECT 
						DISTINCT BookCategoryType 
				FROM 
						INTRANET_ELIB_BOOK 
				WHERE 
						BookID>" . $this->physical_book_init_value . " 
					AND Publish=1
					AND BookCategoryType IS NOT NULL
 					AND BookCategoryType <> ''
				ORDER BY BookCategoryType";
        $rs = $this->returnResultSet($sql);
        $categoryTypeAry = array();
        $bookCategoryTypeAry = array();
        if (count($rs)) {
            foreach ((array) $rs as $r) {
                $categoryTypeAry[] = $r['BookCategoryType'];
            }

            $sql = "SELECT 	id, 
							value 
					FROM 
							" . $this->libmsDB . ".LIBMS_BOOK_CATEGORY_SETTING
					WHERE
							id IN ('" . implode("','", $categoryTypeAry) . "') 
					ORDER BY id";
            $crs = $this->returnResultSet($sql);
            if (count($crs)) {
                $allEmpty = true;
                foreach ($crs as $v) {
                    if ($v['value']) {
                        $allEmpty = false;
                        break;
                    }
                }
                if ($allEmpty) {
                    $bookCategoryTypeAry = array(
                        '1' => 'LC',
                        '2' => 'Dewey'
                    ); // default name if not set in system
                } else {
                    $bookCategoryTypeAry = BuildMultiKeyAssoc($crs, 'id', 'value', $SingleValue = 1, $BuildNumericArray = 0);
                }
            }
        }

        return $bookCategoryTypeAry;
    }

    public function getBookCatrgories($limit = 0, $book_type = 'type_all')
    {
        global $sys_custom;
        // $expireBook = $this->getExpiredBookIDArr();
        // $exCond_a = ' AND BookID NOT IN ('.$expireBook.') ';
        // $cond = 'AND isEnable="1" ';

        $activeBookArr = $this->getActiveBookID();
        if (! empty($activeBookArr)) {
            $activeBookList = implode("','", $activeBookArr);
            $exCond_a = "AND b.BookID IN ('" . $activeBookList . "')";
//			$exCond_a = "";
        } else if ($sys_custom['ebook']['disableRemovalEbook']) {
            $exCond_a = "AND (b.BookFormat <> 'physical' OR b.BookFormat IS NULL)";
        } else {
            $exCond_a = "AND 1=0";      // all expire, show no ebooks
        }

        $extra_field = "";
        $bookCategoryType = $this->getBookCategoryType();
        if (count($bookCategoryType)) {
            $extra_field .= ", CASE BookCategoryType ";
            foreach ((array) $bookCategoryType as $cat_type_id => $cat_type_name) {
                $extra_field .= "WHEN '" . $cat_type_id . "' THEN '" . $cat_type_name . "' ";
            }
            $extra_field .= "ELSE '" . $bookCategoryType['1'] . "' "; // NULL or empty are regarded as BookCategoryType1
            $extra_field .= "END AS BookCategoryType";
        } else {
            $extra_field .= ",'' AS BookCategoryType";
        }

        $p_sql = "SELECT Language, Category, SubCategory, Count(SubCategory) as count_subcategory, BookFormat
					{$extra_field}
				FROM INTRANET_ELIB_BOOK
		          WHERE Publish = 1    AND BookFormat='physical'
				GROUP BY category, SubCategory, Language
				HAVING category <> '' or SubCategory <> ''
				ORDER BY count_subcategory desc, Category";

        if ($book_type == 'type_physical') {
            $sql = $p_sql;
        } else {
            $sql = " 
				(SELECT b.Language, b.Category, b.SubCategory, Count(b.SubCategory) as count_subcategory, b.BookFormat,
					'eBook' AS BookCategoryType
				FROM INTRANET_ELIB_BOOK b 
		         WHERE Publish = 1 $exCond_a
				GROUP BY category, SubCategory, Language
				HAVING category <> '' or SubCategory <> ''
				ORDER BY count_subcategory desc, Category)
				UNION
				({$p_sql})";
        }
        if ($limit) {
            $sql .= " LIMIT $limit";
        }

        if ($this->convert_utf8)
            $this->dbConnectionUTF8();

        $subcategories = $this->returnArray($sql);

        $returnArray = array(

            'chi' => array(

                'count' => 0,
                'categories' => array()

            ),
            'eng' => array(

                'count' => 0,
                'categories' => array()

            )

        );

        foreach ($subcategories as $subcategory) {

            $l = $subcategory['Language'];
            $c = $subcategory['Category'];
            $f = $subcategory['BookFormat'];
            $bct = $subcategory['BookCategoryType'];

            if ($l != 'chi' && $l != 'eng' || $f == 'physical') {
                // $l='others';
                $l = $bct;
            }

            // if($l=='others'){
            if ($bct !== 'eBook') {

                $c_array = explode(' - ', $c);

                if (! $c_array[1])
                    continue;

                if (empty($returnArray[$l]['categories'][$c])) {

                    $returnArray[$l]['categories'][$c] = array(

                        'title' => $c,
                        'count' => 0,
                        'subcategories' => array()
                    );
                }
            } else {

                if (empty($returnArray[$l]['categories'][$c])) {

                    $returnArray[$l]['categories'][$c] = array(

                        'title' => $c,
                        'count' => 0,
                        'subcategories' => array()

                    );
                }

                $returnArray[$l]['categories'][$c]['subcategories'][] = array(

                    'title' => $subcategory['SubCategory'],
                    'count' => $subcategory['count_subcategory']

                );
            }

            $returnArray[$l]['categories'][$c]['count'] += $subcategory['count_subcategory'];
            $returnArray[$l]['count'] += $subcategory['count_subcategory'];
        }

        return $returnArray;
    }

    public function getUserNotes($offset = 0, $amount = 20, $with_total = false)
    {
        $elib_install = new elibrary_install();
        // $sql = "SELECT
        // a.*,
        // b.NoteType as type,b.NoteID as note_id,b.Category as category, b.Content as content, b.DateModified as last_modified
        // FROM
        // INTRANET_ELIB_BOOK a
        // INNER JOIN INTRANET_ELIB_USER_MYNOTES b
        // ON b.UserID = ".$this->user_id."
        // WHERE a.BookID = b.BookID
        // AND a.Publish = 1
        // ORDER by last_modified desc
        // LIMIT $offset, $amount
        // ";
        //
        // $books = $this->returnArray($sql);

        $currUserID = $this->user_id;

        $tmp_table_note = "INTRANET_ELIB_TEMP_NOTE_" . $currUserID;
        $sql = "CREATE TEMPORARY TABLE " . $tmp_table_note . "
				(
					BookID int(11) ,
					Title text,
					Content text,
					Category varchar(100) , 
					Date datetime,
					BookFormat varchar(16),
					IsTLF char(2) 
				)  CHARSET=utf8 ";

        $this->db_db_query($sql);

        $sql = "
				SELECT 
				a.BookID, 
				a.Title as Title, 
				CONCAT('<a href=\"#\" class=\"tablelink\" onClick=\"ViewNotesContent(',a.BookID,',\'',b.NoteType,'\',\'',b.NoteID,'\');\">',b.Content, '</a>') as Content,
				b.Category, b.DateModified as Date,
				a.BookFormat,
				a.IsTLF
				FROM
				INTRANET_ELIB_BOOK a, INTRANET_ELIB_USER_MYNOTES b
				WHERE
				b.UserID = '{$currUserID}'
				AND a.BookID = b.BookID
				AND a.Publish = 1			
				";

        $new_sql = "INSERT INTO " . $tmp_table_note . " (BookID,Title,Content,
					Category,Date,BookFormat,IsTLF) ($sql)";

        $this->db_db_query($new_sql);
        // /////////////////////////////////////////////

        $new_sql = " SELECT     a.BookID,
                                a.Title AS Title, 
                                b.Highlight AS Content," .
                                " '-', 
                                DATE_FORMAT(b.DateModified,'%Y-%m-%d %H:%i:%s') AS Date, " .
                                " a.BookFormat, 
                                a.IsTLF, 
                                c.ImageBook " .
                    " FROM 
                                INTRANET_ELIB_BOOK AS a 
                                INNER JOIN INTRANET_EBOOK_HIGHLIGHT_NOTES AS b ON a.BookID = b.BookID " .
                                " INNER JOIN INTRANET_EBOOK_BOOK AS c ON a.BookID = c.BookID " .
                    " WHERE b.UserID = '$currUserID' 
                            AND a.Publish=1";

        $highlight_arr = $this->returnArray($new_sql);

        $new_sql = "";

        foreach ($highlight_arr as $key => $value) {
            $temp_arr = explode("notes", $value["Content"]);
            $temp_arr = explode("page", $temp_arr[1]);
            $temp_arr = explode("\\\",\\\"", $temp_arr[0]);
            $temp_arr = explode("\\\":\\\"", $temp_arr[0]);
            $note = trim($temp_arr[1]);

            $temp_arr = explode("page", $value["Content"]);
            $temp_arr = explode("dateModify", $temp_arr[1]);
            $temp_arr = explode(",\\\"", $temp_arr[0]);
            $temp_arr = explode("\\\":", $temp_arr[0]);
            $page_number = $temp_arr[1];

            if ($note != "") {
                $note = urldecode($note);
                // $page_number_1 = $libebookreader->Get_PageNumber_Mapping_By_PageMode($value["BookID"],1,$page_number,$value["ImageBook"]);

                $note = "<a href=\"#\" class=\"tablelink\" onClick=\"ViewNotesContent_htmlbook(\'" . $value["BookID"] . "\',\'" . $value["Title"] . "\',\'\',\'$page_number_1\',\'$page_number\' );\">$note</a>";

                if ($new_sql == "")
                    $new_sql .= " (' " . $value["BookID"] . " ','" . $value["Title"] . "','" . $note . "','" . $value["Category"] . "'," . "'" . $value["Date"] . "','" . $value["BookFormat"] . "','" . $value["IsTLF"] . "') ";
                else
                    $new_sql .= ", (' " . $value["BookID"] . " ','" . $value["Title"] . "','" . $note . "','" . $value["Category"] . "'," . "'" . $value["Date"] . "','" . $value["BookFormat"] . "','" . $value["IsTLF"] . "') ";
            }
        }
        $new_sql = "INSERT INTO " . $tmp_table_note . " (BookID,Title,Content,
					Category,Date,BookFormat,IsTLF) VALUES $new_sql";

        $this->db_db_query($new_sql);

        // /////////////////////////////////////////////
        $new_sql = " SELECT 
				a.BookID, 
				a.Title as Title, 
		        CONCAT('<a href=\"#\" class=\"tablelink\" onClick=\"ViewNotesContent_htmlbook(',a.BookID,',\'',a.Title,'\',\'',b.RecordID,'\',\'',b.PageNum1PageMode,'\',\'',b.PageNum2PageMode,'\'    );\">',b.Notes, '</a>') as Content,
				'-', 
				b.DateModified as Date,
				a.BookFormat,
				a.IsTLF
				FROM
				INTRANET_ELIB_BOOK AS a INNER JOIN INTRANET_EBOOK_PAGE_NOTES AS b ON a.BookID = b.BookID
				WHERE b.UserID = '$currUserID'  
				      AND a.Publish=1";

        $new_sql = "INSERT INTO " . $tmp_table_note . " (BookID,Title,Content,
					Category,Date,BookFormat,IsTLF) ($new_sql)";

        $this->db_db_query($new_sql);
        $new_sql = " SELECT SQL_CALC_FOUND_ROWS * FROM " . $tmp_table_note . " WHERE 1 ORDER BY Date DESC LIMIT $offset, $amount ";

        $books = $this->returnArray($new_sql);
        $total = current($this->returnVector('select found_rows()'));

        $returnArray = array();

        foreach ($books as $book) {

            $entry = "";
            $entry['title'] = $book['Title'];
            $entry['id'] = $book['BookID'];
            $entry['book_id'] = $book['BookID'];
            $entry['type'] = '';
            $entry['note_id'] = $book['note_id'];
            $entry['category'] = $book['Category'];
            $entry['content'] = $book['Content'];
            $entry['note_url'] = $book['Content'];
            $entry['url'] = "newWindow('/home/eLearning/ebook_reader/" . $book['BookID'] . "/reader/', 31)";
            $entry['last_modified'] = $book['Date'];
            $entry['readable'] = $elib_install->check_if_readable($book['BookID']);

            $returnArray[] = $entry;
        }

        // $returnArray=array();
        //
        // foreach($books as $book){
        //
        // $entry = $this->getBooksArrayByFormat($book);
        // $entry['book_id'] = $entry['id'];
        // $entry['type'] = $book['type'];
        // $entry['note_id'] = $book['note_id'];
        // $entry['category'] = $book['category'];
        // $entry['content'] = $book['content'];
        // $entry['note_url'] = str_replace('?BookID='.$entry['id'],'?BookID='.$entry['id']."&NoteType=".$book['type']."&NoteID=".$book['note_id'],$entry['url']);
        // $entry['last_modified'] = $book['last_modified'];
        //
        // $returnArray[] = $entry;
        //
        // }

        return $with_total ? array(
            $total,
            $returnArray
        ) : $returnArray;
    }

    public function getUserInfo($user_id = false, $classNameWithBracket = true)
    {
        global $eLib_plus, $userPhotoField, $intranet_session_language;

        $userNameField = $intranet_session_language == 'en' ? 'EnglishName' : 'ChineseName';
        if (! $userPhotoField) {
            // global $intranet_root;
            // include_once("$intranet_root/home/eLearning/elibplus/config.php");
            $photoType = $this->getPhotoSetting();
            $userPhotoField = $photoType == 1 ? 'PhotoLink' : 'PersonalPhotoLink';
        }
        $userPhotoField = $userPhotoField ? $userPhotoField : 'PersonalPhotoLink';

        if (! $user_id)
            $user_id = $this->user_id;
        $class = $classNameWithBracket ? "concat('(',ClassName,'-',ClassNumber,')')" : "concat(ClassName,'-',ClassNumber)";
        $sql = "SELECT 
                " . $userNameField . " as name,
                " . $class . " as class,
                " . $userPhotoField . " as image
                FROM INTRANET_USER
                WHERE UserID = '" . $user_id."'";

        if ($this->convert_utf8) {

            $this->dbConnectionLatin1();

            $user = current($this->returnArray($sql));

            $this->dbConnectionUTF8();

            $returnArray = array(

                empty($user['name']) ? $eLib_plus["html"]["anonymous"] : convert2Unicode($user['name']),
                empty($user['class']) ? '' : convert2Unicode($user['class']),
                empty($user['image']) ? '/images/myaccount_personalinfo/samplephoto.gif' : convert2Unicode($user['image'])

            );
        } else {

            $user = current($this->returnArray($sql));

            $returnArray = array(

                empty($user['name']) ? $eLib_plus["html"]["anonymous"] : $user['name'],
                empty($user['class']) ? '' : $user['class'],
                empty($user['image']) ? '/images/myaccount_personalinfo/samplephoto.gif' : $user['image']

            );
        }

        return $returnArray;
    }

    public function getUserFavourites()
    {
        $sql = "SELECT c.*
                FROM INTRANET_ELIB_BOOK c
                INNER JOIN INTRANET_ELIB_BOOK_MY_FAVOURITES a on a.BookID=c.BookID and a.UserID='" . $this->user_id."' and c.Publish=1";

        $books = $this->returnArray($sql);

        $returnArray = array();

        foreach ($books as $book) {
            $returnArray[] = $this->getBooksArrayByFormat($book);
        }

        return $returnArray;
    }

    /*
     * $bookType: ebook -- BookID < 10000000
     * physical -- BookID >= 10000000
     */
    public function getUserReviews($user_id = false, $class_name = false, $offset = 0, $amount = 10, $bookType = '')
    {
        if (! $user_id) {
            if (! $class_name) {
                $filter = "b.UserID = '" . $this->user_id."'";
            } else {
                $filter = "b.ClassName = '" . $class_name . "'";
            }
        } else {
            $filter = "b.UserID = '" . $user_id."'";
        }

        if ($bookType == $this->physical_book_type_code) {
            $bookTypeCond = "AND c.BookID>=" . $this->physical_book_init_value;
        } else if ($bookType == 'ebook') {
            $bookTypeCond = "AND c.BookID<" . $this->physical_book_init_value;
        } else {
            $bookTypeCond = "";
        }

        $day_a = $this->getDayRangeCondition("a.DateModified");

        $sql = "SELECT SQL_CALC_FOUND_ROWS
		a.UserID as user_id,
                a.Rating as rating, a.Content as content,
                a.DateModified as date,
                c.*,
                count(d.ReviewCommentID) as likes,
                a.ReviewID as review_id
                FROM  INTRANET_ELIB_BOOK_REVIEW a
                inner join INTRANET_USER b on a.UserID = b.UserID
                inner join INTRANET_ELIB_BOOK c on a.BookID=c.BookID and c.Publish=1
                left join INTRANET_ELIB_BOOK_REVIEW_HELPFUL d on a.ReviewID=d.ReviewID AND d.Choose=1
                
                WHERE $filter $day_a $bookTypeCond
                GROUP BY a.ReviewID
                ORDER BY a.ReviewID DESC
		LIMIT $offset, $amount";

        $books = $this->returnArray($sql);

        $total = current($this->returnVector('select found_rows()'));

        $returnArray = array();

        foreach ($books as $book) {

            $entry = $this->getBooksArrayByFormat($book);

            $entry['book_id'] = $entry['id'];
            $entry['review_id'] = $book['review_id'];
            $entry['user_id'] = $book['user_id'];

            $entry['rating'] = $book['rating'];
            $entry['content'] = strip_tags($book['content']);
            $entry['date'] = $book['date'];
            $entry['likes'] = $book['likes'];

            list ($entry['name'], $entry['class'], $entry['user_image']) = $this->getUserInfo($entry['user_id']);

            $returnArray[] = $entry;
        }

        return array(
            $total,
            $returnArray
        );
    }

    private function getDayRangeCondition($field)
    {
        switch ($this->range) {

            case "thisweek":

                // return "AND $field between (CURDATE()-INTERVAL DAYOFWEEK(CURDATE())+1 DAY) and NOW()";
                return "AND $field between (CURDATE()-INTERVAL DAYOFWEEK(CURDATE())-1 DAY) and NOW()";

            case "thismonth":

                // return "AND $field between (CURDATE()-INTERVAL DAYOFMONTH(CURDATE())+1 DAY) and NOW()";
                return "AND $field between (CURDATE()-INTERVAL DAYOFMONTH(CURDATE())-1 DAY) and NOW()";

            case "thisyear":

                // return "AND $field between (CURDATE()-INTERVAL DAYOFYEAR(CURDATE())+1 DAY) and NOW()";
                return "AND $field between (CURDATE()-INTERVAL DAYOFYEAR(CURDATE())-1 DAY) and NOW()";

                break;

            case "specific":
                $this->accumulated_days = $this->accumulated_days ? $this->accumulated_days : 60;

                return "AND $field between (CURDATE()-INTERVAL " . $this->accumulated_days . " DAY) and NOW()";

                break;

            case "accumulated": // 2013-11-14-filterranking

                if ($_SESSION["eLibplus"]["ranking"]["sd"] != '' && $_SESSION["eLibplus"]["ranking"]["ed"] != '') {
                    $StartDate = $_SESSION["eLibplus"]["ranking"]["sd"];
                    $EndDate = $_SESSION["eLibplus"]["ranking"]["ed"];

                    return "AND $field between '$StartDate' and '$EndDate'";
                } else if ($_SESSION["eLibplus"]["ranking"]["ay"] != '') {
                    $dateRange = getPeriodOfAcademicYear($_SESSION["eLibplus"]["ranking"]["ay"]);

                    $StartDate = $dateRange["StartDate"];
                    $EndDate = $dateRange["EndDate"];

                    return "AND $field between '$StartDate' and '$EndDate'";
                } else if ($_SESSION["CurrentSchoolYearInfo"]["AcademicYearID"] != '') {
                    $period = getPeriodOfAcademicYear($_SESSION["CurrentSchoolYearInfo"]["AcademicYearID"]);

                    $StartDate = $period["StartDate"];
                    $EndDate = $period["EndDate"];

                    return "AND $field between '$StartDate' and '$EndDate'";
                }

                break;
            default:

                return "";
        }
    }

    /*
     * $bookType: ebook -- BookID < 10000000
     * physical -- BookID >= 10000000
     */
    private function getBestRateOrMostHitBooks($having, $priority_arr, $limit = 3, $bookType = '')
    { // basically 2 queries are same except order
                                                                                                // 20140414-ajax-improve
        $elib_install = new elibrary_install();

        $day_b = $this->getDayRangeCondition("b.DateModified");
        $day_c = $this->getDayRangeCondition("c.DateModified");
        $expiredBook = $this->getExpiredBookIDArr();
        if (! empty($expiredBook)) {
//             $exCond_b = (! strpos($_GET['action'], 'Portal')) ? '' : ' AND b.BookID NOT IN (' . $expiredBook. ') ';
//             $exCond_c = (! strpos($_GET['action'], 'Portal')) ? '' : ' AND c.BookID NOT IN (' . $expiredBook. ') ';
            $exCond_b = ' AND b.BookID NOT IN (' . $expiredBook . ') ';
            $exCond_c = ' AND c.BookID NOT IN (' . $expiredBook . ') ';
        } else {
            $exCond_b = '';
            $exCond_c = '';
        }

        if ($bookType == $this->physical_book_type_code) {
            $bookTypeCond_b = " AND b.BookID>=" . $this->physical_book_init_value;
            $bookTypeCond_c = " AND c.BookID>=" . $this->physical_book_init_value;
        } else if ($bookType == 'ebook') {
            $bookTypeCond_b = " AND b.BookID<" . $this->physical_book_init_value;
            $bookTypeCond_c = " AND c.BookID<" . $this->physical_book_init_value;
        } else {
            $bookTypeCond_b = "";
            $bookTypeCond_c = "";
        }

        // $sql = "SELECT a.*, ifnull(round(avg(b.Rating),1),'--') as rating, count(distinct(b.ReviewID)) as review_count, count(distinct(c.HistoryID)) as hit_rate
        // FROM INTRANET_ELIB_BOOK a
        // LEFT JOIN INTRANET_ELIB_BOOK_REVIEW b on a.BookID=b.BookID $day_b
        // LEFT JOIN INTRANET_ELIB_BOOK_HISTORY c on a.BookID=c.BookID $day_c
        // WHERE a.Publish=1 ".$this->book_formats."
        // GROUP BY a.BookID
        // $options";
        $book_id_arr = array();
        $rating_review_count_arr = array();
        $hit_rate_arr = array();

        if ($having == "review_count") {
            $sql = " SELECT b.BookID, ifnull(round(avg(b.Rating),1),'--') as rating, count(distinct(b.ReviewID)) as review_count " . " FROM INTRANET_ELIB_BOOK_REVIEW b WHERE 1 $day_b $exCond_b $bookTypeCond_b" . " GROUP BY b.BookID ";

            $rating_review_counts = $this->returnArray($sql);
            foreach ($rating_review_counts as $rating_review_count) {
                if ($rating_review_count["review_count"] > 0) {
                    array_push($book_id_arr, $rating_review_count["BookID"]);
                }
                $rating_review_count_arr[$rating_review_count["BookID"]] = array();
                $rating_review_count_arr[$rating_review_count["BookID"]]["rating"] = $rating_review_count["rating"];
                $rating_review_count_arr[$rating_review_count["BookID"]]["review_count"] = $rating_review_count["review_count"];
            }

            $sql = " SELECT c.BookID, count(distinct(c.HistoryID)) as hit_rate " . " FROM INTRANET_ELIB_BOOK_HISTORY c WHERE 1 $day_c $exCond_c $bookTypeCond_c" . " GROUP BY c.BookID ";
            $hit_rates = $this->returnArray($sql);
            foreach ($hit_rates as $hit_rate) {
                $hit_rate_arr[$hit_rate["BookID"]] = array();
                $hit_rate_arr[$hit_rate["BookID"]]["hit_rate"] = $hit_rate["hit_rate"];
            }
        } else if ($having == "hit_rate") {
            $sql = " SELECT b.BookID, ifnull(round(avg(b.Rating),1),'--') as rating, count(distinct(b.ReviewID)) as review_count " . " FROM INTRANET_ELIB_BOOK_REVIEW b WHERE 1 $day_b $exCond_b $bookTypeCond_b" . " GROUP BY b.BookID ";

            $rating_review_counts = $this->returnArray($sql);

            foreach ($rating_review_counts as $rating_review_count) {
                $rating_review_count_arr[$rating_review_count["BookID"]] = array();
                $rating_review_count_arr[$rating_review_count["BookID"]]["rating"] = $rating_review_count["rating"];
                $rating_review_count_arr[$rating_review_count["BookID"]]["review_count"] = $rating_review_count["review_count"];
            }

            $sql = " SELECT c.BookID, count(distinct(c.HistoryID)) as hit_rate " . " FROM INTRANET_ELIB_BOOK_HISTORY c WHERE 1 $day_c $exCond_c $bookTypeCond_c" . " GROUP BY c.BookID ";
            $hit_rates = $this->returnArray($sql);

            foreach ($hit_rates as $hit_rate) {
                if ($hit_rate["hit_rate"] > 0) {
                    array_push($book_id_arr, $hit_rate["BookID"]);
                }
                $hit_rate_arr[$hit_rate["BookID"]] = array();
                $hit_rate_arr[$hit_rate["BookID"]]["hit_rate"] = $hit_rate["hit_rate"];
            }
        }

        $sql = "SELECT *
	                FROM INTRANET_ELIB_BOOK a 
	                WHERE a.Publish=1 " . $this->book_formats . " 
	                AND a.BookID IN ('" . implode("','", $book_id_arr) . "') ";

        $books = $this->returnArray($sql);

        $returnArray = array();

        for ($i = 0; $i < sizeof($books); $i ++) {
            $books[$i]['rating'] = $rating_review_count_arr[$books[$i]["BookID"]]['rating'];
            $books[$i]['review_count'] = $rating_review_count_arr[$books[$i]["BookID"]]['review_count'];
            $books[$i]['hit_rate'] = $hit_rate_arr[$books[$i]["BookID"]]['hit_rate'];
        }

        if ($having == "review_count") {
            usort($books, array(
                'elibrary_plus',
                'getBestRatesort'
            ));
        } else if ($having == "hit_rate") {
            usort($books, array(
                'elibrary_plus',
                'getMostHitsort'
            ));
        }
        $books = array_slice($books, 0, $limit);

        foreach ($books as $book) {

            $entry = $this->getBooksArrayByFormat($book);
            $entry['rating'] = $rating_review_count_arr[$book["BookID"]]['rating'];
            $entry['review_count'] = $rating_review_count_arr[$book["BookID"]]['review_count'];

            if ($book['BookFormat'] != $this->physical_book_type_code) {

                $entry['hit_rate'] = $hit_rate_arr[$book["BookID"]]['hit_rate'];
                $entry['url'] = $elib_install->get_book_reader($book['BookID'], $book['BookFormat'], $book['IsTLF'], $this->user_id);
            }

            $returnArray[] = $entry;
        }

        return $returnArray;
    }

    private static function getBestRatesort($a, $b)
    {
        if ($a["rating"] == $b["rating"]) {
            if ($a["review_count"] == $b["review_count"]) {
                return $b["hit_rate"] - $a["hit_rate"];
            } else {
                return $b["review_count"] - $a["review_count"];
            }
        }
        if ($a["rating"] < $b["rating"]) {
            return true;
        }
        return false;
    }

    private static function getMostHitsort($a, $b)
    {
        if ($a["hit_rate"] < $b["hit_rate"]) {
            return true;
        }
        return false;
    }

    /*
     * $bookType: ebook -- BookID < 10000000
     * physical -- BookID >= 10000000
     */
    public function getBestRateBooks($limit = 3, $bookType = '')
    {

        // return $this->getBestRateOrMostHitBooks("HAVING review_count>0 ORDER BY rating DESC, review_count DESC, hit_rate DESC LIMIT $limit");
        return $this->getBestRateOrMostHitBooks("review_count", array(
            "rating",
            "review_count",
            "hit_rate"
        ), $limit, $bookType);
    }

    /*
     * $bookType: ebook -- BookID < 10000000
     * physical -- BookID >= 10000000
     */
    public function getMostHitBooks($limit = 3, $bookType = '')
    {

        // return $this->getBestRateOrMostHitBooks("HAVING hit_rate>0 ORDER BY hit_rate DESC, rating DESC, review_count DESC LIMIT $limit");
        return $this->getBestRateOrMostHitBooks("hit_rate", array(
            "hit_rate",
            "rating",
            "review_count"
        ), $limit, $bookType);
    }

    public function getMostLoanBooks($limit = 10)
    {
        $day_a = $this->getDayRangeCondition("a.BorrowTime");
        $day_c = $this->getDayRangeCondition("c.DateModified");

        $expiredBook = $this->getExpiredBookIDArr();
        if (! empty($expiredBook)) {
            $exCond_a = (! strpos($_GET['action'], 'Portal')) ? '' : ' AND a.BookID NOT IN (' . $expiredBook . ') ';
            $exCond_c = (! strpos($_GET['action'], 'Portal')) ? '' : ' AND c.BookID NOT IN (' . $expiredBook . ') ';
        } else {
            $exCond_a = '';
            $exCond_c = '';
        }

        $book_id_arr = array();
        $review_count_rating_arr = array();

        // $sql = "SELECT a.BookID+".$this->physical_book_init_value." as BookID,
        // COUNT(DISTINCT(a.BorrowLogID)) as loan_count,
        // COUNT(DISTINCT(c.ReviewID)) as review_count,
        // ifnull(round(avg(c.Rating),1),'--') as rating
        // from ".$this->libmsDB.".LIBMS_BORROW_LOG a
        // INNER JOIN ".$this->libmsDB.".LIBMS_BOOK b ON a.BookID = b.BookID $day_a
        // LEFT JOIN INTRANET_ELIB_BOOK_REVIEW c on a.BookID+".$this->physical_book_init_value."=c.BookID $day_c
        // GROUP BY a.BookID
        // HAVING loan_count>0
        // ORDER BY loan_count DESC, rating DESC
        // LIMIT $limit";

        $sql = "SELECT a.BookID+" . $this->physical_book_init_value . " as BookID,
	                COUNT(DISTINCT(a.BorrowLogID)) as loan_count
	                from " . $this->libmsDB . ".LIBMS_BORROW_LOG a 
	                WHERE 1 $day_a
	                $exCond_a
					GROUP BY a.BookID
	                HAVING loan_count>0 
	                ORDER BY loan_count DESC
	                LIMIT $limit";

        $books = $this->returnArray($sql);

        foreach ($books as $book) {
            array_push($book_id_arr, $book["BookID"]);
        }

        if (sizeof($book_id_arr)) {
            $sql = " SELECT c.BookID+" . $this->physical_book_init_value . " as BookID, " . " COUNT(DISTINCT(c.ReviewID)) as review_count," . " ifnull(round(avg(c.Rating),1),'--') as rating " . " FROM INTRANET_ELIB_BOOK_REVIEW c WHERE c.BookID IN('" . implode("','", $book_id_arr) . "') $day_c $exCond_c";

            $review_count_ratings = $this->returnArray($sql);

            foreach ($review_count_ratings as $review_count_rating) {
                $review_count_rating_arr[$review_count_rating["BookID"]] = array();
                $review_count_rating_arr[$review_count_rating["BookID"]]["review_count"] = $review_count_rating["review_count"];
                $review_count_rating_arr[$review_count_rating["BookID"]]["rating"] = $review_count_rating["rating"];
            }
        }

        $returnArray = array();

        foreach ($books as &$book) {

            $book['BookFormat'] = $this->physical_book_type_code;

            $entry = $this->getBooksArrayByFormat($book); // forced BookFormat so that to get physical book detail
            $entry['loan_count'] = $book['loan_count'];
            $entry['review_count'] = $review_count_rating_arr[$book["BookID"]]['review_count'];
            $entry['rating'] = $review_count_rating_arr[$book["BookID"]]['rating'];

            $returnArray[] = $entry;
        }

        return $returnArray;
    }

    /*
     * $bookType: ebook -- BookID < 10000000
     * physical -- BookID >= 10000000
     */
    public function getMostActiveUsers($limit = 10, $bookType = '', $userType = '', $classNameWithBracket = true, $userCond = '')
    {
        $day = $this->getDayRangeCondition("a.DateModified");

        if ($bookType == $this->physical_book_type_code) {
            $bookTypeCond = "AND a.BookID>=" . $this->physical_book_init_value;
        } else if ($bookType == 'ebook') {
            $bookTypeCond = "AND a.BookID<" . $this->physical_book_init_value;
        } else {
            $bookTypeCond = "";
        }

        if ($userType) {
            $userTypeCond = " AND b.RecordType='" . $userType . "'";
        } else {
            $userTypeCond = "";
        }

        $sql = "SELECT 
		a.UserID as user_id,
		count(distinct(a.ReviewID)) as review_count
		FROM INTRANET_USER b
                INNER JOIN INTRANET_ELIB_BOOK_REVIEW a on a.UserID = b.UserID $day
                INNER JOIN INTRANET_ELIB_BOOK c on c.BookID=a.BookID
        WHERE c.Publish=1           
		" . $bookTypeCond . $userTypeCond . $userCond . " 
		GROUP BY 
		a.UserID 
		ORDER BY 
		review_count desc
                
		Limit $limit"; // ensure INTRANET_ELIB_BOOK has book

        $users = $this->returnArray($sql);

        foreach ($users as &$user) {
            list ($user['name'], $user['class'], $user['image']) = $this->getUserInfo($user['user_id'], $classNameWithBracket);
        }

        return $users;
    }

    public function getMostActiveUsersWithReviews($limit = 10)
    {
        $users = $this->getMostActiveUsers($limit);

        foreach ($users as &$user) {
            $user['reviews'] = $this->getUserReviews($user['user_id']);
        }
        return $users;
    }

    public function getMostActiveClasses($limit = 10)
    {
        $day = $this->getDayRangeCondition("a.DateModified");

        $sql = "SELECT 
		b.ClassName as class,
		count(a.ReviewID) as review_count
		FROM 
		INTRANET_ELIB_BOOK_REVIEW a
		INNER JOIN INTRANET_USER b ON a.UserID = b.UserID
                INNER JOIN INTRANET_ELIB_BOOK c on a.BookID=c.BookID
		WHERE b.ClassName IS NOT NULL AND b.ClassName<>'' $day
		GROUP BY 
		b.ClassName
		ORDER BY 
		review_count desc
		Limit $limit"; // ensure INTRANET_ELIB_BOOK has book

        if ($this->convert_utf8) {

            $this->dbConnectionLatin1();

            $classes = $this->returnArray($sql);

            foreach ($classes as &$class) {
                $class['class'] = convert2Unicode($class['class']);
            }

            $this->dbConnectionUTF8();

            $classes_raw = $this->returnArray($sql); // get an unconverted classname for getting reviews

            foreach ($classes_raw as $i => &$class_raw) {
                $classes[$i][0] = $class_raw['class'];
            }
        } else {
            $classes = $this->returnArray($sql);
        }

        return $classes;
    }

    public function getMostActiveClassesWithReviews($limit = 10)
    {
        $classes = $this->getMostActiveClasses($limit);

        foreach ($classes as &$class) {
            $class['reviews'] = $this->getUserReviews(false, $class[0]);
        }

        return $classes;
    }

    /*
     * UserType - default is student, can be staff & student (in my friends ranking)
     */
    public function getMostBorrowUsers($limit = 10, $classNameWithBracket = true, $cond = '', $userType = 'S')
    {

        // 2013-10-23 Charles Ma
        // $period = getPeriodOfAcademicYear($_SESSION["CurrentSchoolYearInfo"]["AcademicYearID"]);
        // $day = " WHERE a.BorrowTime >= '".$period["StartDate"]."' AND a.BorrowTime <= '".$period["EndDate"]."'";
        $day = $this->getDayRangeCondition("a.BorrowTime");
        $userTypeCond = $userType ? "AND u.UserType = '$userType'" : "";

        $sql = "SELECT a.UserID as user_id, 
                count(a.BookID) as loan_count 
                from " . $this->libmsDB . ".LIBMS_BORROW_LOG a
		INNER JOIN " . $this->libmsDB . ".LIBMS_USER u ON a.UserID = u.UserID {$userTypeCond}
                INNER JOIN INTRANET_USER b ON a.UserID=b.UserID 
                $day 
		WHERE 1=1 $cond
                GROUP BY a.UserID 
                ORDER BY loan_count DESC
		LIMIT $limit";
        // 2013-10-23 Charles Ma END

        $users = $this->returnArray($sql);

        foreach ($users as &$user) {
            list ($user['name'], $user['class'], $user['image']) = $this->getUserInfo($user['user_id'], $classNameWithBracket);
        }

        return $users;
    }

    /*
     * $bookType: ebook -- BookID < 10000000
     * physical -- BookID >= 10000000
     */
    public function getMostUsefulReviews($limit = 10, $bookType = '', $classNameWithBracket = true)
    {
        $day = $this->getDayRangeCondition("d.DateModified");

        if ($bookType == $this->physical_book_type_code) {
            $bookTypeCond = " AND a.BookID>=" . $this->physical_book_init_value;
        } else if ($bookType == 'ebook') {
            $bookTypeCond = " AND a.BookID<" . $this->physical_book_init_value;
        } else {
            $bookTypeCond = "";
        }

        $sql = "SELECT 
		c.*,
                a.ReviewID as review_id,
                d.UserID as user_id,
                d.Content as content,
                d.DateModified as date,
		count(a.ReviewCommentID) likes,
                d.Rating as rating
		FROM INTRANET_ELIB_BOOK_REVIEW_HELPFUL a
                LEFT OUTER JOIN INTRANET_ELIB_BOOK_REVIEW d ON a.ReviewID = d.ReviewID
		LEFT JOIN INTRANET_USER b ON d.UserID = b.UserID
		INNER JOIN INTRANET_ELIB_BOOK c ON c.BookID = a.BookID
		WHERE a.Choose = 1
		AND c.Publish = 1
                $day {$bookTypeCond} 
		GROUP BY a.ReviewID
		ORDER BY likes desc, rating desc
		Limit $limit";

        $books = $this->returnArray($sql);

        $returnArray = array();

        foreach ($books as $book) {

            $entry = $this->getBooksArrayByFormat($book);
            $entry['book_id'] = $entry['id'];
            $entry['review_id'] = $book['review_id'];
            $entry['user_id'] = $book['user_id'];
            $entry['content'] = strip_tags($book['content']);
            $entry['date'] = $book['date'];
            $entry['likes'] = $book['likes'];
            $entry['rating'] = $book['rating'];

            list ($entry['name'], $entry['class'], $entry['user_image']) = $this->getUserInfo($book['user_id'], $classNameWithBracket);

            $returnArray[] = $entry;
        }

        return $returnArray;
    }

    public function getReadingProgress($offset = 0, $amount = 20, $with_total = false)
    {
        $elib_install = new elibrary_install();

        $sql = "SELECT SQL_CALC_FOUND_ROWS a.BookID as id, a.Title as title, a.Author as author, a.IsTLF, a.BookFormat, b.Percentage as percentage, max(c.DateModified) as last_accessed
                FROM INTRANET_ELIB_BOOK a
                RIGHT JOIN INTRANET_ELIB_BOOK_HISTORY c on a.BookID=c.BookID AND a.Publish=1
                LEFT JOIN INTRANET_ELIB_USER_PROGRESS b on b.BookID=c.BookID and  b.UserID=c.UserID
                WHERE a.BookFormat<>'" . $this->physical_book_type_code . "' AND a.BookID IS NOT NULL AND c.UserID='" . $this->user_id . "'
                GROUP BY id
                ORDER BY last_accessed desc
                LIMIT $offset, $amount";

        $books = $this->returnArray($sql);
        $total = current($this->returnVector('select found_rows()'));

        foreach ($books as &$book) {
            $book['url'] = $elib_install->get_book_reader($book['id'], $book['BookFormat'], $book['IsTLF'], $this->user_id);
            $book['readable'] = $elib_install->check_if_readable($book['id']);
        }

        return $with_total ? array(
            $total,
            $books
        ) : $books;
    }

    // should also include the number of book reserved and available
    public function getNotificationCount()
    {
        return sizeof($this->getUserLoanBooksCurrent()) + sizeof($this->getUserReservedBooks()) + sizeof($this->getUserPenaltyCurrent());
    }

    private function getUserLostBooks($dateorder, $offset, $amount)
    { // 20140509-lost-book-list
        global $intranet_session_language, $intranet_root, $Lang, $eLib;

        $name = $Lang['libms']['SQL']['UserNameFeild'];

        $sql = " SELECT bu.BookID+" . $this->physical_book_init_value . " as id, bu.`DateModified` as LostDate , IF(blu.`$name` is null or blu.`$name` = '', ' -- ', blu.`$name`) as UserName , " . " bu.ACNO, IF((b.`CallNum`='' or b.`CallNum` is null) and (b.`CallNum2`='' or b.`CallNum2` is null), ' -- ', " . " CONCAT_WS( ' ', b.`CallNum` , b.`CallNum2` )) AS CallNum, " . " CONCAT(b.`BookTitle`, IF(bu.`ItemSeriesNum`<>'' AND bu.`ItemSeriesNum`<>'1' AND bu.`ItemSeriesNum` IS NOT NULL,CONCAT(' ',bu.`ItemSeriesNum`),'')) as title," . " IF(b.`ResponsibilityBy1` = '' or b.`ResponsibilityBy1` is null, ' -- ', b.`ResponsibilityBy1`) as `ResponsibilityBy1`," . " IF(bu.PurchasePrice is null, ' -- ', bu.PurchasePrice) as PurchasePrice  FROM " . $this->libmsDB . ".LIBMS_BOOK_UNIQUE bu " . " JOIN " . $this->libmsDB . ".LIBMS_BOOK b ON b.`BookID` = bu.`BookID` " . " INNER JOIN " . " ( SELECT bl.* , u.`EnglishName`, u.`ChineseName`, u.`UserLogin`, u.`BarCode`, u.`UserEmail` " . " FROM " . $this->libmsDB . ".LIBMS_BORROW_LOG bl JOIN " . $this->libmsDB . ".LIBMS_USER u ON bl.`UserID` = u.`UserID` " . " WHERE bl.`RecordStatus` LIKE 'LOST' AND u.UserID='" . $this->user_id . "' ORDER BY bl.`DateModified` $dateorder ) blu " . " ON blu.`UniqueID` = bu.`UniqueID` WHERE bu.`RecordStatus` LIKE 'LOST' ";

        return $this->returnArray($sql);
    }

    public function getUserLostBooksHistory($offset = 0, $amount = 50)
    { // 20140509-lost-book-list
        return $this->getUserLostBooks('desc', $offset, $amount);
    }

    private function getUserLoanBooks($status, $dateorder, $offset, $amount, $isgroupby = true, $withTotal=false)
    { // 20140414-elibrary-borrow-record
        global $intranet_root, $intranet_session_language;
        include_once ("$intranet_root/home/library_sys/management/circulation/TimeManager.php");
        $libms_tm = new TimeManager(new liblms());
        // modified to support borrow more than one copy of book (Henry)

        if ($status != "") {
            $status_sql = "and a.RecordStatus='$status'";
        }

        if ($isgroupby) {
            $count_total_sql = " count(*) total, ";
            $group_by_con_sql = " group by c.UniqueID ";    // BorrowLog dependency fields are non-sense in such case: BorrowLogID, ReturnedTime, Borrowtime, DueDate ...
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS $count_total_sql
		CONCAT(b.BookTitle, IF(c.ItemSeriesNum<>'' AND c.ItemSeriesNum<>'1' AND c.ItemSeriesNum IS NOT NULL,CONCAT(' ',c.ItemSeriesNum),''), ' [',c.BarCode,'] ') as title,
                a.BookID+" . $this->physical_book_init_value . " as id,
                a.BorrowLogID as borrow_id,
                a.ReturnedTime as return_date,
                a.BorrowTime as borrow_date,
                a.DueDate as due_date,
                if (CURDATE() > a.DueDate, 1, 0) as is_overdue,
		UNIX_TIMESTAMP(CURDATE()) as current_date_ts,
		UNIX_TIMESTAMP(a.DueDate) as due_date_ts,
                a.RenewalTime as renew_count,
                a.UniqueID
                from " . $this->libmsDB . ".LIBMS_BORROW_LOG a
                JOIN " . $this->libmsDB . ".LIBMS_BOOK b ON a.BookID=b.BookID
                JOIN " . $this->libmsDB . ".LIBMS_BOOK_UNIQUE c ON a.UniqueID=c.UniqueID		
                where a.UserID='" . $this->user_id . "' $status_sql
                $group_by_con_sql
		ORDER BY borrow_date $dateorder, title asc
		
		LIMIT $offset, $amount";
        // debug_r($sql);
        // $sql = "SELECT count(*) total,
        // b.BookTitle as title,
        // a.BookID+".$this->physical_book_init_value." as id,
        // a.BorrowLogID as borrow_id,
        // a.ReturnedTime as return_date,
        // a.BorrowTime as borrow_date,
        // a.DueDate as due_date,
        // if (CURDATE() > a.DueDate, 1, 0) as is_overdue,
        // UNIX_TIMESTAMP(CURDATE()) as current_date_ts,
        // UNIX_TIMESTAMP(a.DueDate) as due_date_ts,
        // a.RenewalTime as renew_count,
        // a.UniqueID
        // from ".$this->libmsDB.".LIBMS_BORROW_LOG a
        // JOIN ".$this->libmsDB.".LIBMS_BOOK b ON a.BookID=b.BookID
        // where a.UserID=".$this->user_id." and a.RecordStatus='$status'
        // group by a.BookID
        // ORDER BY borrow_date $dateorder, title asc
        //
        // LIMIT $offset, $amount";

        if ($this->convert_utf8) {

            $this->dbConnectionLatin1();

            $books = $this->returnArray($sql);

            $this->dbConnectionUTF8();
        } else {

            $books = $this->returnArray($sql);
        }

        $total = current($this->returnVector('select found_rows()'));

        foreach ($books as $i => $book) {

            if ($book['is_overdue']) {
                // $books[$i]['overdue_days'] = $libms_tm->dayForPenalty($book['due_date_ts'], $book['current_date_ts']); performance problem!!!
                $books[$i]['overdue_days'] = 1;
            }
            $books[$i]['return_date'] = $book['return_date'] == "0000-00-00 00:00:00" ? "--" : $book['return_date'];
            $books[$i]['borrow_date'] = $book['borrow_date'] == "0000-00-00 00:00:00" ? "--" : $book['borrow_date'];
        }

        return $withTotal ? array(
            $total,
            $books
        ) : $books;
    }

    public function getUserLoanBooksCurrent($offset = 0, $amount = 20)
    {
        return $this->getUserLoanBooks('BORROWED', 'asc', $offset, $amount);
    }

    public function getUserLoanBooksHistory($offset = 0, $amount = 50, $withTotal = false)
    { // 20140414-elibrary-borrow-record
        return $this->getUserLoanBooks('', 'desc', $offset, $amount, false, $withTotal);
    }

    public function getUserLoanBooksOverDue()
    {

        // problem! user has to return book and then over due will be generated; thus, the result should be wrong!!
        $loan_items = $this->getUserLoanBooksCurrent();

        $overdue_items = array();

        foreach ($loan_items as $loan_item) {
            if ($loan_item['overdue_days'] > 0) {
                $overdue_items[] = $loan_item;
            }
        }

        return $overdue_items;
    }

    private function getUserPenalty($status, $dateorder)
    {
        $sql = "SELECT CONCAT(b.BookTitle, IF(bu.ItemSeriesNum<>'' AND bu.ItemSeriesNum<>'1' AND bu.ItemSeriesNum IS NOT NULL,CONCAT(' ',bu.ItemSeriesNum),'')) as title,        
                    a.BookID+" . $this->physical_book_init_value . " as id,
                    a.DueDate as due_date,
                    if(a.ReturnedTime=0,'--',a.ReturnedTime) as return_date,
                    c.Payment as penalty_total,
                    c.Payment-IFNULL(sum(d.Amount),0) as penalty_owing,
                    IFNULL(c.DaysCount, 'LOST') as penalty_reason
                from " . $this->libmsDB . ".LIBMS_OVERDUE_LOG c
                LEFT JOIN " . $this->libmsDB . ".LIBMS_TRANSACTION d ON c.OverDueLogID=d.OverDueLogID
                INNER JOIN " . $this->libmsDB . ".LIBMS_BORROW_LOG a ON c.BorrowLogID=a.BorrowLogID
                INNER JOIN " . $this->libmsDB . ".LIBMS_BOOK b ON a.BookID=b.BookID
				INNER JOIN " . $this->libmsDB . ".LIBMS_BOOK_UNIQUE bu ON bu.UniqueID=a.UniqueID
                where a.UserID='" . $this->user_id . "' and c.RecordStatus='$status' 
                GROUP BY c.OverDueLogID
                ORDER BY return_date $dateorder, title asc";

        if ($this->convert_utf8) {

            $this->dbConnectionLatin1();

            $books = $this->returnArray($sql);

            $this->dbConnectionUTF8();
        } else {
            $books = $this->returnArray($sql);
        }

        return $books;
    }

    public function getUserPenaltyCurrent()
    {
        return $this->getUserPenalty('OUTSTANDING', 'asc');
    }

    public function getUserPenaltyHistory()
    {
        return $this->getUserPenalty('SETTLED', 'desc');
    }

    // Modified the cond a.RecordStatus (Henry)
    public function getUserReservedBooks($status = false)
    {

        // $sql = "SELECT b.BookTitle as title, a.BookID+".$this->physical_book_init_value." as id, a.ReserveTime as reserve_date, a.RecordStatus as status
        // from ".$this->libmsDB.".LIBMS_RESERVATION_LOG a
        // INNER JOIN ".$this->libmsDB.".LIBMS_BOOK b ON a.BookID=b.BookID
        // where a.UserID=".$this->user_id." ".($status?"and a.RecordStatus = '$status'":"")."
        // ORDER BY status asc, reserve_date asc";
        $sql = "SELECT b.BookTitle as title, a.BookID+" . $this->physical_book_init_value . " as id, a.ReserveTime as reserve_date, a.RecordStatus as status
                from " . $this->libmsDB . ".LIBMS_RESERVATION_LOG a
                INNER JOIN " . $this->libmsDB . ".LIBMS_BOOK b ON a.BookID=b.BookID
                where a.UserID='" . $this->user_id . "' and (a.RecordStatus = 'WAITING' OR a.RecordStatus = 'READY') 
                ORDER BY status asc, reserve_date asc";
        $books = $this->returnArray($sql);

        if ($this->convert_utf8) {

            $this->dbConnectionLatin1();

            $books = $this->returnArray($sql);

            $this->dbConnectionUTF8();
        } else {
            $books = $this->returnArray($sql);
        }

        return $books;
    }

    private function getUserLimitations($fields, $circulation_code)
    {
        if (! $circulation_code)
            $circulation_code = '-1Def';

        $sql = "SELECT $fields
		    FROM " . $this->libmsDB . ".LIBMS_GROUP_CIRCULATION a
		    INNER JOIN " . $this->libmsDB . ".LIBMS_GROUP_USER b ON a.GroupID=b.GroupID
		    WHERE (a.CirculationTypeCode = '$circulation_code' OR a.CirculationTypeCode = '-1Def')
			AND b.UserID='" . $this->user_id . "'
		    ORDER BY IF(CirculationTypeCode= '-1Def', 1 ,0)";

        return current($this->returnVector($sql));
    }

    public function getUserLoanQuota($circulation_code)
    {
        return $this->getUserLimitations("LimitBorrow", $circulation_code);
    }

    public function getUserReserveQuota($circulation_code)
    {
        return $this->getUserLimitations("LimitReserve", $circulation_code);
    }

    public function getUserRenewQuota($circulation_code)
    {
        return $this->getUserLimitations("LimitRenew", $circulation_code);
    }

    public function getUserReturnDuration($circulation_code)
    {
        return $this->getUserLimitations("ReturnDuration", $circulation_code);
    }

    private function getNotificationSettings($type)
    {
        $sql = "SELECT enable FROM " . $this->libmsDB . ".LIBMS_NOTIFY_SETTING
		WHERE name = '$type'";

        return current($this->returnVector($sql));
    }

    // Charles 2013-09-30
    public function getAllReviewsRecord($offset = 0, $amount = 20, $order_sql = 'last_submitted', $searchFromDate = '', $searchToDate = '', $with_total = false)
    { // 2013-09-27 Charles Ma
        $elib_install = new elibrary_install();

        $dateFilter = '';
        $dateFilter .= ($searchFromDate) ? " AND b.DateModified >= '$searchFromDate' " : "";
        $dateFilter .= ($searchToDate) ? " AND b.DateModified <= '" . $searchToDate . " 23:59:59" . "' " : "";

        $sql = "SELECT 
						SQL_CALC_FOUND_ROWS
						a.BookID as id, 
						a.Title	as title, 
						a.Author as author, 
						MAX(b.DateModified) as last_submitted,
						Count(b.BookID) as num_review,
						a.BookFormat as BookFormat,
						a.IsTLF as IsTLF
					FROM
						INTRANET_ELIB_BOOK a, INTRANET_ELIB_BOOK_REVIEW b 
					INNER JOIN 
						INTRANET_USER as u
					ON 
						b.UserID = u.UserID
					WHERE
						a.BookID = b.BookID		
					AND 
						a.Publish = 1
						$dateFilter
					GROUP BY
						b.BookID
					ORDER BY
						$order_sql	
	                LIMIT $offset, $amount";

        $books = $this->returnArray($sql);

        $total = current($this->returnVector('select found_rows()'));

        foreach ($books as &$book) {
            $book['url'] = $elib_install->get_book_reader($book['id'], $book['BookFormat'], $book['IsTLF'], $this->user_id);
            $book['readable'] = $elib_install->check_if_readable($book['id']);
        }

        return $with_total ? array(
            $total,
            $books
        ) : $books;
    }

    public function getPublicClassSummary($offset = 0, $amount = 20, $FromDate = '', $ToDate = '', $FromMonth = '', $ToMonth = '', $FromYear = '', $ToYear = '', $order_sql, $with_total = false)
    {
        $con = "";

        $con_field = 'count(r.BookID)';
        if (! empty($FromMonth)) {
            $FromDate = date('Y-m-d', mktime(0, 0, 0, $FromMonth, 1, $FromYear));
            $ToDate = date('Y-m-d', mktime(0, 0, 0, $ToMonth + 1, 1, $ToYear));
            $con = "AND r.DateModified BETWEEN '$FromDate' AND '$ToDate' ";
        } else {
            $con = '';
        }

        // TEMPORARY

        $tmp_table = "INTRANET_ELIB_TEMP_NUM_" . $this->user_id;
        $sql = "CREATE TEMPORARY TABLE " . $tmp_table . "
				(
					class_name varchar(255),
					count_history int(10),
					count_history_chi int(10),
					count_history_eng int(10),
					count_review int(10),
					num_student int(4)
				)";
        $this->db_db_query($sql);

        $sql = "INSERT INTO " . $tmp_table . " (class_name,num_student) (select distinct ClassName,count(*) FROM INTRANET_USER where ClassName!='' and ClassName IS NOT NULL and RecordType <> 4 group by ClassName)";
        $this->db_db_query($sql);

        $sql = "select u.ClassName, count(ReviewID) as count_rec from 
				INTRANET_USER as u 
				inner join INTRANET_ELIB_BOOK_REVIEW as r on r.UserID = u.UserID
				inner join INTRANET_ELIB_BOOK as b on b.BookID = r.BookID
				where b.publish=1 and u.ClassName!='' and u.ClassName IS NOT NULL and u.RecordType <> 4 
				$con
				group by u.ClassName";
        $review_obj = $this->returnArray($sql);

        for ($a = 0; $a < sizeof($review_obj); $a ++) {
            $sql = "UPDATE " . $tmp_table . " SET count_review = '" . $review_obj[$a]['count_rec'] . "' WHERE class_name = '" . $review_obj[$a]['ClassName'] . "'";
            $this->db_db_query($sql);
        }

        $sql = "select u.ClassName, b.Language, count(HistoryID) as count_rec from 
				INTRANET_USER as u 
				inner join INTRANET_ELIB_BOOK_HISTORY as r on r.UserID = u.UserID
				inner join INTRANET_ELIB_BOOK as b on b.BookID = r.BookID
				where b.publish=1 and u.ClassName!='' and u.ClassName IS NOT NULL and u.RecordType <> 4
				$con
				group by u.ClassName, b.Language";
        $history_obj = $this->returnArray($sql);
        $totalHitByClassAry = array();
        for ($a = 0, $aMax=count($history_obj); $a < $aMax; $a ++) {
            $language = $history_obj[$a]['Language'];
            $className = $history_obj[$a]['ClassName'];
            $nrHit = $history_obj[$a]['count_rec'];
            $sql = "UPDATE " . $tmp_table . " SET count_history_".$language." = '" . $nrHit . "' WHERE class_name = '" . $className . "'";
            $this->db_db_query($sql);
            $totalHitByClassAry[$className] += $nrHit;
        }
        foreach((array) $totalHitByClassAry as $_className=>$_totalHitAry) {
            $sql = "UPDATE " . $tmp_table . " SET count_history = '" . $_totalHitAry . "' WHERE class_name = '" . $_className . "'";
            $this->db_db_query($sql);
        }
        $sql = "SELECT
					SQL_CALC_FOUND_ROWS
					concat('<a class=\"class_tab\" href=\"#PublicClassSummary#ClassName',class_name,'\">',class_name,'</a>') as class_link,
					num_student as num_student,
					if(sum(count_history)>0,sum(count_history),0) as NumBookRead,
					if(sum(count_history_chi)>0,sum(count_history_chi),0) as NumChiBookRead,
					if(sum(count_history_eng)>0,sum(count_history_eng),0) as NumEngBookRead,
					if(round(count_history/num_student)>0,round(count_history/num_student),0) as read_average,
					if(sum(count_review)>0,sum(count_review),0) as NumBookReview,
					if(round(count_review/num_student)>0,round(count_review/num_student),0) as review_average, class_name			
				FROM
					" . $tmp_table . "
				group by class_name ORDER BY $order_sql LIMIT $offset, $amount";
        $books = $this->returnArray($sql);

        $total = current($this->returnVector('select found_rows()'));

        return $with_total ? array(
            $total,
            $books
        ) : $books;
    }

    public function getPublicStudentSummary($offset = 0, $amount = 20, $ClassName = '', $FromDate = '', $ToDate = '', $FromMonth = '', $ToMonth = '', $FromYear = '', $ToYear = '', $order_sql, $with_total = false)
    {
        $lang = $_SESSION["intranet_session_language"];

        $con = " a.ClassName";
        $currUserID = $this->user_id;

        if (! empty($FromMonth)) {
            $FromDate = date('Y-m-d', mktime(0, 0, 0, $FromMonth, 1, $FromYear));
            $ToDate = date('Y-m-d', mktime(0, 0, 0, $ToMonth + 1, 1, $ToYear));
            $search_con = "AND b.DateModified BETWEEN '$FromDate' AND '$ToDate' ";
        } else {
            $search_con = "";
        }

        $tsql1 = "
				CREATE TEMPORARY TABLE INTRANET_ELIB_TEMP_NUM_REVIEW_{$currUserID}
				SELECT b.UserID, Count(DISTINCT b.ReviewID) as NumBookReview
		                FROM INTRANET_ELIB_BOOK a
		                INNER JOIN INTRANET_ELIB_BOOK_REVIEW b
		                ON a.BookID = b.BookID $search_con
		                INNER JOIN INTRANET_USER c
				ON b.UserID = c.UserID
		                WHERE a.Publish = 1 
				GROUP BY b.UserID
				";
        $tsql2 = "
				CREATE TEMPORARY TABLE INTRANET_ELIB_TEMP_NUM_READ_{$currUserID}
				SELECT b.UserID, Count(DISTINCT b.HistoryID) as NumBookRead, max(b.DateModified) as DateModified
				FROM INTRANET_ELIB_BOOK a
		                INNER JOIN INTRANET_ELIB_BOOK_HISTORY b
		                ON a.BookID = b.BookID $search_con
		                INNER JOIN INTRANET_USER c
				ON b.UserID = c.UserID
		                WHERE a.Publish = 1 
				GROUP BY b.UserID
				";
        $tsql2b = "
				CREATE TEMPORARY TABLE INTRANET_ELIB_TEMP_NUM_BOOK_{$currUserID}
				SELECT b.UserID, Count(DISTINCT b.BookID) as NumBooks, max(b.DateModified) as DateModified
				FROM INTRANET_ELIB_BOOK a
		                INNER JOIN INTRANET_ELIB_BOOK_HISTORY b
		                ON a.BookID = b.BookID $search_con
		                INNER JOIN INTRANET_USER c
				ON b.UserID = c.UserID
		                WHERE a.Publish = 1 
				GROUP BY b.UserID
				";
        $tsql3 = "
				CREATE TEMPORARY TABLE INTRANET_ELIB_TEMP_NUM_COMPLETE_{$currUserID}
				SELECT b.UserID, Count(DISTINCT b.BookID) as NumBookComplete
		                FROM INTRANET_ELIB_BOOK a
		                INNER JOIN INTRANET_ELIB_USER_PROGRESS b
		                ON a.BookID = b.BookID AND b.Percentage = 100
		                INNER JOIN INTRANET_USER c
				ON b.UserID = c.UserID
		                WHERE a.Publish = 1
				GROUP BY b.UserID
				";

        $this->db_db_query($tsql1);
        $this->db_db_query($tsql2);
        $this->db_db_query($tsql2b);
        $this->db_db_query($tsql3);

        // $selName1 = "CONCAT('<a class=\"student_tab\" href=\"?page=stats#PublicClassSummary#YearClassID', y.YearClassID, '#StudentID', a.UserID ,'#ClassName', a.ClassName,'\">' , if((a.ChineseName IS NOT NULL AND a.ChineseName != ''), a.ChineseName , a.EnglishName), '</a>') as user_link,a.ChineseName as student_name";
        // $selName2 = "CONCAT('<a class=\"student_tab\" href=\"?page=stats#PublicClassSummary#YearClassID', y.YearClassID, '#StudentID', a.UserID ,'#ClassName', a.ClassName,'\">' , if((a.EnglishName IS NOT NULL AND a.EnglishName != ''), a.EnglishName, a.ChineseName), '</a>') as user_link,a.EnglishName as student_name";

        $selName1 = " if((a.ChineseName IS NOT NULL AND a.ChineseName != ''), ChineseName , EnglishName) as user_link,a.ChineseName as student_name";
        $selName2 = " if((a.EnglishName IS NOT NULL AND a.EnglishName != ''), EnglishName, ChineseName) as user_link,a.EnglishName as student_name";

        $selName = ($lang == "b5") ? $selName1 : $selName2;
        $selClass = ($ClassName == - 1 || $ClassName == "") ? "" : "AND a.ClassName='" . $ClassName . "'";

        // # SQL Query

        $sql = "
				SELECT
					SQL_CALC_FOUND_ROWS
					a.UserID AS user_id,
					a.ClassName AS class_name,
					a.ClassNumber AS class_number,
					$selName,
					if((nb.NumBooks IS NOT NULL),nb.NumBooks, 0) AS NumBooks,
					if((b.NumBookRead IS NOT NULL),b.NumBookRead, 0) AS NumBookRead,
					if((c.NumBookReview IS NOT NULL),c.NumBookReview, 0) AS NumBookReview,
					if((p.NumBookComplete IS NOT NULL), p.NumBookComplete, 0) AS NumBookComplete,
					y.YearClassID AS YearClassID
				FROM INTRANET_USER a 
				LEFT JOIN INTRANET_ELIB_TEMP_NUM_REVIEW_{$currUserID} c ON a.UserID = c.UserID
		                " . ($search ? 'INNER' : 'LEFT') . " JOIN INTRANET_ELIB_TEMP_NUM_READ_{$currUserID} b
				ON a.UserID = b.UserID
				LEFT JOIN INTRANET_ELIB_TEMP_NUM_COMPLETE_{$currUserID} as p
				ON a.UserID = p.UserID
				LEFT JOIN INTRANET_ELIB_TEMP_NUM_BOOK_{$currUserID} as nb
				ON a.UserID = nb.UserID
				LEFT JOIN YEAR_CLASS as y
				ON a.ClassName = y.ClassTitleEN
			 	WHERE 1 $selClass
			 	AND (a.ClassName IS NOT NULL)
			 	AND !(a.ClassName = '')
			 	AND a.RecordType <> 4
			 	GROUP BY a.UserID
				ORDER BY
				$order_sql LIMIT $offset, $amount				
				"; // AND y.AcademicYearID = ".Get_Current_Academic_Year_ID()."

        $books = $this->returnArray($sql);
        $total = current($this->returnVector('select found_rows()'));
        foreach ($books as &$book) {

            $book['user_link'] = "<a class=\"student_tab\" href=\"?page=stats#PublicClassSummary#YearClassID" . $book['YearClassID'] . "#StudentID" . $book['user_id'] . "#ClassName" . $book['class_name'] . "\">" . $book['user_link'] . "</a>";
            if ($book['NumBookRead'] != 0)
                $book['NumBookRead'] = "<a class=\"student_tab\" href=\"?page=stats#PublicClassSummary#YearClassID" . $book['YearClassID'] . "#StudentID" . $book['user_id'] . "#ClassName" . $book['class_name'] . "\">" . $book['NumBookRead'] . "</a>";
            if ($book['NumBookReview'] != 0)
                $book['NumBookReview'] = "<a class=\"review_tab\" href=\"?page=stats#PublicClassSummary#YearClassID" . $book['YearClassID'] . "#StudentID" . $book['user_id'] . "#ClassName" . $book['class_name'] . "\">" . $book['NumBookReview'] . "</a>";
        }

        return $with_total ? array(
            $total,
            $books
        ) : $books;
    }

    public function getPublicStudentParticularSummary($offset = 0, $amount = 20, $currUserID = 0, $ClassName = '', $FromDate = '', $ToDate = '', $FromMonth = '', $ToMonth = '', $FromYear = '', $ToYear = '', $order_sql, $with_total = false)
    {
        $lang = $_SESSION["intranet_session_language"];
        $con = " a.ClassName";
        $elib_install = new elibrary_install();

        if (! empty($FromMonth)) {
            $FromDate = date('Y-m-d', mktime(0, 0, 0, $FromMonth, 1, $FromYear));
            $ToDate = date('Y-m-d', mktime(0, 0, 0, $ToMonth + 1, 1, $ToYear));
            $search_con = "AND last_accessed BETWEEN '$FromDate' AND '$ToDate' ";
        } else {
            $search_con = "";
        }

        $sql = "
			SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT
				b.BookID AS id,	
				b.Title as title,
				if(a.Author !='' , a.Author , '--') as author,
				count(h.BookID) as read_times, 
				MAX(h.DateModified) as last_accessed,
				IF(p.Percentage IS NULL, '--', CONCAT(p.Percentage, '%')) AS percentage, p.Percentage as ppercentage,
				b.BookFormat,
				b.IsTLF
			FROM
				INTRANET_ELIB_BOOK_HISTORY as h
			INNER JOIN
				INTRANET_ELIB_BOOK as b
			 ON 
				h.BookID = b.BookID
			LEFT JOIN 
				INTRANET_ELIB_BOOK_AUTHOR as a
			ON
				b.AuthorID = a.AuthorID
			LEFT JOIN 
				INTRANET_ELIB_USER_PROGRESS as p
			ON 
				b.BookID = p.BookID
		 	WHERE 
		 		b.Publish = 1 AND
		 		h.UserID = '" . $currUserID . "' 
		 	GROUP BY
		 		b.BookID ORDER BY $order_sql ) AS tt WHERE 1 $search_con LIMIT $offset, $amount
		";

        $books = $this->returnArray($sql);
        $total = current($this->returnVector('select found_rows()'));

        foreach ($books as &$book) {
            $book['url'] = $elib_install->get_book_reader($book['id'], $book['BookFormat'], $book['IsTLF'], $this->user_id);
            $book['readable'] = $elib_install->check_if_readable($book['id']);
        }

        return $with_total ? array(
            $total,
            $books
        ) : $books;
    }

    function retrieve_filter($FromDate = '', $ToDate = '', $FromMonth = '', $ToMonth = '', $FromYear = '', $ToYear = '')
    {
        global $intranet_session_language, $intranet_root, $Lang, $eLib;

        include_once ("$intranet_root/includes/form_class_manage.php");
        include_once ("$intranet_root/includes/libclass.php");
        $fcm = new form_class_manage();
        $libclass = new libclass();

        // debug_r($fcm->Get_Class_List_By_Academic_Year($AcademicYearID));
        $ClassSelection = $libclass->getSelectClass("name='ClassName' id='ClassName_Search'", "");
        // $ClassSelection = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($ClassNameArr, "name='YearClassID' id='ClassName' onChange='changeClassName(this);'", "", $YearClassID);

        include_once ("$intranet_root/includes/libinterface.php");
        $linterface = new interface_html("default3.html");

        $ThisYear = Date("Y") * 1;
        $aryMonth = array(
            array(
                '',
                '--'
            ),
            array(
                '01',
                1
            ),
            array(
                '02',
                2
            ),
            array(
                '03',
                3
            ),
            array(
                '04',
                4
            ),
            array(
                '05',
                5
            ),
            array(
                '06',
                6
            ),
            array(
                '07',
                7
            ),
            array(
                '08',
                8
            ),
            array(
                '09',
                9
            ),
            array(
                10,
                10
            ),
            array(
                11,
                11
            ),
            array(
                12,
                12
            )
        );
        $aryYear = array(
            array(
                '',
                '--'
            ),
            array(
                $ThisYear,
                $ThisYear
            ),
            array(
                $ThisYear - 1,
                $ThisYear - 1
            ),
            array(
                $ThisYear - 2,
                $ThisYear - 2
            ),
            array(
                $ThisYear - 3,
                $ThisYear - 3
            ),
            array(
                $ThisYear - 4,
                $ThisYear - 4
            ),
            array(
                $ThisYear - 5,
                $ThisYear - 5
            )
        );
        $ThisMonth = Date("m") * 1;

        $FromMonthSelection = $linterface->GET_SELECTION_BOX($aryMonth, "name='FromMonth' id='FromMonth_Search'", "", $ThisMonth);
        $FromYearSelection = $linterface->GET_SELECTION_BOX($aryYear, "name='FromYear' id='FromYear_Search'", "", $ThisYear);
        $ToMonthSelection = $linterface->GET_SELECTION_BOX($aryMonth, "name='ToMonth' id='ToMonth_Search'", "", $ThisMonth);
        $ToYearSelection = $linterface->GET_SELECTION_BOX($aryYear, "name='ToYear' id='ToYear_Search'", "", $ThisYear);

        $classStr = $eLib["html"]["class"] . " : ";

        $SelectionTable = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
        // $SelectionTable .= "<tr><td><span class=\"tabletext\">".$classStr." ".$ClassSelection."</span></td></tr>";
        $SelectionTable .= '<tr><td>';
        $SelectionTable .= '<input type="hidden" id="Order_Search""/><input type="hidden" id="Sort_Search""/><span class="tabletext">' . $classStr . ' ' . $ClassSelection . '</span>';
        $SelectionTable .= '<tr><td><span class="tabletext">' . $Lang['Header']['Menu']['Period'] . ':' . $FromMonthSelection . $FromYearSelection . $Lang['SysMgr']['Homework']['To'] . $ToMonthSelection . $ToYearSelection . '<input type="button" name="search" onClick="applySearch_Search();" value="' . $Lang['Btn']['Apply'] . '" /></span></td></tr>';

        $SelectionTable .= "</table>";
        return $SelectionTable;
    }

    // 2013-11-14 Charles Ma 2013-11-14-filterranking
    // This function is to store the filter option in session - home\eLearning\elibplus\index.php
    function set_filter_ranking($sd = '', $ed = '', $ay = '')
    {
        $_SESSION["eLibplus"]["ranking"]["sd"] = $sd;
        $_SESSION["eLibplus"]["ranking"]["ed"] = $ed;
        $_SESSION["eLibplus"]["ranking"]["ay"] = $ay;
    }

    // 2013-11-14 Charles Ma 2013-11-14-filterranking
    // This function is to create the filter for ranking page in elib_plus - home\eLearning\elibplus\ajax.php
    function retrieve_filter_ranking($keyword = "#BestRateBooks")
    {
        global $intranet_session_language, $intranet_root, $Lang, $eLib;

        include_once ("$intranet_root/includes/form_class_manage.php");
        include_once ("$intranet_root/includes/libclass.php");
        $fcm = new form_class_manage();
        $libclass = new libclass();

        // debug_r($fcm->Get_Class_List_By_Academic_Year($AcademicYearID));
        $ClassSelection = $libclass->getSelectClass("name='ClassName' id='ClassName_Search'", "");
        // $ClassSelection = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($ClassNameArr, "name='YearClassID' id='ClassName' onChange='changeClassName(this);'", "", $YearClassID);

        include_once ("$intranet_root/includes/libinterface.php");
        $linterface = new interface_html("default3.html");

        $AcademicYear = $_SESSION["eLibplus"]["ranking"]["ay"] == '' ? $_SESSION["CurrentSchoolYearInfo"]["AcademicYearID"] : $_SESSION["eLibplus"]["ranking"]["ay"];
        $academic_year_html = getSelectAcademicYear("academicYear", ' onchange="applySearch_Search(\'academicyear\');" ', 0, 0, $AcademicYear);

        // academic year

        $dateRange = getPeriodOfAcademicYear($_SESSION["CurrentSchoolYearInfo"]["AcademicYearID"]);

        $StartDate = $_SESSION["eLibplus"]["ranking"]["sd"] == '' ? $dateRange["StartDate"] : $_SESSION["eLibplus"]["ranking"]["sd"];
        $EndDate = $_SESSION["eLibplus"]["ranking"]["ed"] == '' ? $dateRange["EndDate"] : $_SESSION["eLibplus"]["ranking"]["ed"];

        // date ranger

        $SelectionTable = '';

        $SelectionTable .= '<script>
				$(document).ready(function(){
				    $(\'.toggle_tool a\').unbind("click tap").bind("click tap", function(){
				        var target=$($(this).attr(\'href\'));
				        
				        target.fadeIn().siblings().hide();
				        $(this).parent().addClass(\'selected\').siblings().removeClass(\'selected\');
				        
				        return false;
				    });
				});
								
				var applySearch_Search = function (key){
				    switch(key){
				    	case \'date\':
				    		window.location.href = "./?page=ranking&range=accumulated&sd=" + $(\'#startDate\').val() + "&ed="+ $(\'#endDate\').val() + "' . $keyword . '";
				    	break;
				    	case \'academicyear\':
				    		window.location.href = "./?page=ranking&range=accumulated&ay="+ $(\'#academicYear\').val() + "' . $keyword . '";
				    	break;
				    }
				}			
			</script>';

        $SelectionTable .= "<table style=\"padding-left: 19px; display: block;\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
        $SelectionTable .= '<tr><td><span class="tabletext">' . $Lang['libms']['report']['SchoolYear'] . ':' . $academic_year_html . '</span></td></tr><br>';
        $SelectionTable .= '<tr><td><span class="tabletext">' . $Lang['Header']['Menu']['Period'] . ': ' . $linterface->GET_DATE_PICKER('startDate', date('Y-m-d', strtotime($StartDate))) . $Lang['SysMgr']['Homework']['To'] . ': ' . $linterface->GET_DATE_PICKER('endDate', date('Y-m-d', strtotime($EndDate))) . '<input type="button" name="search" onclick="applySearch_Search(\'date\');" value="' . $Lang['Btn']['Apply'] . '" /></span></td></tr>';

        $SelectionTable .= "</table>";
        return $SelectionTable;
    }

    public function getUserReviews2($user_id = false, $class_name = false, $offset = 0, $amount = 10, $FromMonth, $ToMonth, $FromYear, $ToYear)
    {
        if (! $user_id) {
            if (! $class_name) {
                $filter = "b.UserID = '" . $this->user_id."'";
            } else {
                $filter = "b.ClassName = '" . $class_name . "'";
            }
        } else {
            $filter = "b.UserID = '" . $user_id."'";
        }

        $day_a = $this->getDayRangeCondition("a.DateModified");

        if (! empty($FromMonth)) {
            $FromDate = date('Y-m-d', mktime(0, 0, 0, $FromMonth, 1, $FromYear));
            $ToDate = date('Y-m-d', mktime(0, 0, 0, $ToMonth + 1, 1, $ToYear));
            $search_con = "AND a.DateModified BETWEEN '$FromDate' AND '$ToDate' ";
        } else {
            $search_con = "";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS
		a.UserID as user_id,
                a.Rating as rating, a.Content as content,
                a.DateModified as date,
                c.*,
                count(d.ReviewCommentID) as likes,
                a.ReviewID as review_id
                FROM  INTRANET_ELIB_BOOK_REVIEW a
                inner join INTRANET_USER b on a.UserID = b.UserID
                inner join INTRANET_ELIB_BOOK c on a.BookID=c.BookID and c.Publish=1
                left join INTRANET_ELIB_BOOK_REVIEW_HELPFUL d on a.ReviewID=d.ReviewID AND d.Choose=1
                
                WHERE $filter $day_a $search_con
                GROUP BY a.ReviewID
                ORDER BY a.ReviewID DESC
		LIMIT $offset, $amount";

        $books = $this->returnArray($sql);

        $total = current($this->returnVector('select found_rows()'));

        $returnArray = array();

        foreach ($books as $book) {

            $entry = $this->getBooksArrayByFormat($book);

            $entry['book_id'] = $entry['id'];
            $entry['review_id'] = $book['review_id'];
            $entry['user_id'] = $book['user_id'];

            $entry['rating'] = $book['rating'];
            $entry['content'] = strip_tags($book['content']);
            $entry['date'] = $book['date'];
            $entry['likes'] = $book['likes'];

            list ($entry['name'], $entry['class'], $entry['user_image']) = $this->getUserInfo($entry['user_id']);

            $returnArray[] = $entry;
        }

        return array(
            $total,
            $returnArray
        );
    }

    /*
     * 2015-04-28: get all books review details for export
     */
    public function getAllReviewsDetails($offset = 0, $amount = 9999, $order_sql = 'b.Title, u.EnglishName', $searchFromDate = '', $searchToDate = '')
    {
        $dateFilter = '';
        $dateFilter .= ($searchFromDate) ? " AND r.DateModified >= '$searchFromDate' " : "";
        $dateFilter .= ($searchToDate) ? " AND b.DateModified <= '" . $searchToDate . " 23:59:59" . "' " : "";

        $sql = "select
					  b.Title,
					  case u.RecordType 
					    when 1 then 'Staff'
					    when 2 then 'Student'
					    when 3 then 'Parent'
					    else 'Alumni'
					  end as 'Identity',
					  u.UserLogin,
					  u.EnglishName,
					  u.ChineseName,
					  if (u.ClassName='0','',u.ClassName) as ClassName,
					  u.ClassNumber,
					  r.Rating,
					  r.Content,
					  r.DateModified
				from
					  INTRANET_ELIB_BOOK_REVIEW r
				inner join 
					  INTRANET_USER u on u.UserID = r.UserID
				inner join 
					  INTRANET_ELIB_BOOK b on b.BookID=r.BookID
				where 
					  b.Publish = 1
					  $dateFilter
				order by 
					  $order_sql					  
				LIMIT $offset, $amount";

        $books = $this->returnArray($sql);

        return $books;
    }
 // end function getAllReviewsDetails
    public function set_accumulated_days($days)
    {
        $this->accumulated_days = $days;
    }

    // public function getMostActiveReaders($limit=10){
    //
    // $day_b = $this->getDayRangeCondition("c.DateModified");
    // $expireBook = $this->getExpiredBookIDArr();
    // if(!empty($expireBook)){
    // $exCond_b = (!strpos($_GET['action'], 'Portal'))?'':' AND b.BookID NOT IN ('.$expireBook.') ';
    // }else{
    // $exCond_b ='';
    // }
    //
    // $bookTypeCond_b = " AND b.BookID<".$this->physical_book_init_value;
    //
    //
    // $sql = "SELECT u.UserID as user_id,
    // count(distinct(c.HistoryID)) as hit_rate
    // FROM INTRANET_USER u
    // INNER JOIN INTRANET_ELIB_BOOK_HISTORY c ON u.UserID = c.UserID
    // INNER JOIN INTRANET_ELIB_BOOK b ON b.BookID=c.BookID
    // WHERE u.RecordType = '2'
    // $day_b $exCond_b $bookTypeCond_b
    // GROUP BY u.UserID
    // ORDER BY hit_rate DESC
    // LIMIT $limit";
    //
    // $users=$this->returnArray($sql);
    //
    // foreach ($users as &$user){
    // list($user['name'],$user['class'],$user['image'])=$this->getUserInfo($user['user_id']);
    // }
    //
    // return $users;
    // }
    public function getTotalPortalNotices($type)
    {
        global $permission;
        // Check if the old data is copied when start
        $this->copyOldDataFromElibBookPage();
        $cond = "";
        if ($type) {
            $cond .= " and p.AnnouncementType = '$type'";
        }
        if (! $permission['admin']) {
            $cond .= " and (((p.DateStart is null or p.DateStart='0000-00-00 00:00:00') and (p.DateEnd is null or p.DateEnd='0000-00-00 00:00:00')) or 
						((p.DateStart is null or p.DateStart='0000-00-00 00:00:00') and p.DateEnd>=CURDATE()) or 
						((p.DateEnd is null or p.DateEnd='0000-00-00 00:00:00') and p.DateStart<=CURDATE()) or 
						(p.DateStart<=CURDATE() and p.DateEnd>=CURDATE()))";
        }
        $sql = "select count(p.AnnouncementID) as Total
			from INTRANET_ELIB_BOOK_ANNOUNCEMENT  p			
			where 1 " . $cond;
        $result = $this->returnResultSet($sql);
        $total = count($result) ? $result[0]['Total'] : 0;

        return $total;
    }

    /*
     * $timestamp --> the starting timestamp for counting a week
     */
    public function getLibraryCalendarByWeek($timestamp)
    {
        global $eLib_plus;
        $opening_days = $this->getLibraryOpeningHours();
        $holidays = $this->getLibraryHolidays();
        $special_days = $this->getLibrarySpecialOpenDays();

        $ret = array();
        for ($i = 0; $i < 7; $i ++, $timestamp += 86400) {
            $date = date('Y-m-d', $timestamp);
            $weekday = date('l', $timestamp);
            $description = '';

            foreach ((array) $opening_days as $opening_day) {
                if ($weekday == $opening_day['Weekday']) {
                    $is_open = $opening_day['Open'];
                    $open = $opening_day['OpenTime'];
                    $close = $opening_day['CloseTime'];
                }
            }
            foreach ((array) $holidays as $holiday) {
                if ($timestamp >= $holiday['start'] && $timestamp < $holiday['end']) {
                    $is_open = 0;
                    $description = $holiday['event'];
                }
            }
            foreach ((array) $special_days as $special_day) {
                if ($timestamp >= $special_day['start'] && $timestamp < $special_day['end']) {
                    $is_open = $special_day['Open'];
                    $open = $special_day['OpenTime'];
                    $close = $special_day['CloseTime'];
                    $description = $eLib_plus["html"]["specialtime"];
                }
            }

            $open_timestamp = strtotime($date . ' ' . $open);
            $close_timestamp = strtotime($date . ' ' . $close);

            $ret[] = array(
                'weekday' => $eLib_plus["html"]["weekday"][$weekday],
                'open' => $is_open ? date('g:i a', $open_timestamp) : 0,
                'close' => $is_open ? date('g:i a', $close_timestamp) : 0,
                'is_open' => $is_open,
                'month' => date('M', $timestamp),
                'day' => date('j', $timestamp),
                'description' => $description,
                'date' => $date,
                'open_timestamp' => $open_timestamp,
                'close_timestamp' => $close_timestamp
            );
        }
        return $ret;
    }
 // end getLibraryCalendarByWeek

    // $para in this format: "$language,$category"
    public function getBookSubCategories($para)
    {
        global $sys_custom;

        if ($para) {
            list ($language, $category) = explode(',', $para);
            $unconverted_category = mysql_real_escape_string(str_replace("\\", "\\\\", $category));
            $converted_category = mysql_real_escape_string(intranet_htmlspecialchars(intranet_undo_htmlspecialchars($category)));

            $activeBookArr = $this->getActiveBookID();
            if (! empty($activeBookArr)) {
                $activeBookList = implode(",", $activeBookArr);
                $exCond_a = "AND b.BookID IN (" . $activeBookList . ")";
            } else if ($sys_custom['ebook']['disableRemovalEbook']) {
                $exCond_a = "AND (b.BookFormat <> 'physical' OR b.BookFormat IS NULL)";
            } else {
                $exCond_a = "";
            }
            $sql = " 
					(SELECT b.SubCategory
					FROM INTRANET_ELIB_BOOK b 
			         WHERE Publish = 1 
							AND b.Language='" . $language . "' 
							AND (b.Category='" . $unconverted_category . "' OR b.Category='" . $converted_category . "') $exCond_a
					GROUP BY SubCategory
					HAVING SubCategory <> ''
					)
					UNION
					(SELECT SubCategory
					FROM INTRANET_ELIB_BOOK
			          WHERE Publish = 1 AND BookFormat='physical'
							AND Language='" . $language . "' 
							AND (Category='" . $unconverted_category . "' OR Category='" . $converted_category . "')
					GROUP BY SubCategory
					HAVING SubCategory <> ''
					) ORDER BY SubCategory";

            if ($this->convert_utf8)
                $this->dbConnectionUTF8();

            $subcategories = $this->returnResultSet($sql);

            return $subcategories;
        } else {
            return false;
        }
    }
 // end getBookSubCategories
    public function getTotalBookReviews()
    {
        $sql = "SELECT count(ReviewID) as Total
	                FROM  INTRANET_ELIB_BOOK_REVIEW a
	                WHERE a.BookID = '" . $this->book_id . "'";
        $result = $this->returnResultSet($sql);
        $total = count($result) ? $result[0]['Total'] : 0;
        return $total;
    }

    public function getBookDetailWithoutFormat($book_id = "")
    {
        $book_id = $book_id ? $book_id : $this->book_id;
        $sql = "SELECT *
	                FROM 
	                INTRANET_ELIB_BOOK 
	                WHERE BookID = '" . $book_id . "'";
        $rs = current($this->returnResultSet($sql));
        return $rs;
    }

    /*
     * Purpose: use for similar search string
     * @param: $str -- given string
     * return partial string (floor) in array
     * e.g. given sting = "Hong Kong Science Technology Application"
     * return = array {"Hong Kong",
     * "Kong Science",
     * "Science Technology",
     * "Technology Application"}
     */
    public function getSubStrings($string)
    {
        $ret = array();
        if ($string) {
            preg_match_all('/(\w+)|(.)/u', $string, $matches);
            $words = $matches[0];
            $nrWords = count($words);
            $divisor = $nrWords > 8 ? 3 : 2;
            $size = floor($nrWords / $divisor);
            if ($size > 0) {
                for ($i = 0, $iMax = $nrWords - $size; $i <= $iMax; $i ++) {
                    $subString = "";
                    for ($j = $i; $j < $i + $size; $j ++) {
                        $subString .= $words[$j];
                    }
                    $ret[] = $subString;
                }
            } else {
                $ret[] = $string;
            }
        }
        return $ret;
    }

    /*
     * @para: $percentage_1 -- criteria to get similar book title in target pool
     * $percentage_2 -- criteria to get top 4 most similar titles
     * algorithm: 1. get books and put them in target pool by having same value in any of these fields:
     * (1) category
     * (2) author
     * (3) tags
     * 2. get books and put them in target pool for title similarity >= $percentage_1
     * 3. scan each books in target pool,
     * if title similarity > $percentage_2, put it in $top_similar array
     * 4. if $top_similar array is empty, sort target pool by similarity on desceding,
     * pick out at most the first 4 ( or total number if less than 4) and put them in $top_similar array
     * 5. randomly pick out at most 4 in $top_similar array, then put them in $ret array
     * 5. get six books randomly from target pool except the four books got in previous step
     *
     * return selected ten books, first 4 is the top most similar books compared to current book title
     * the rest are picked randomly from the pool
     *
     */
    public function getSimilarBooks($percentage_1, $percentage_2)
    {
        $ret = array();
        $percent = 0;
        $top_most_similar_to_show = 4;
        $total_similar_to_show = 10;

        $current_book = $this->getBookDetail();

        if ($current_book) {
            $title = $current_book['title'];
            $current_book_id = $current_book['id'];

            if ($current_book['Category']) {
                $unconverted_category = mysql_real_escape_string(str_replace("\\", "\\\\", $current_book['Category'])); // A&<>'"\B ==> A&<>\'\"\\\\B
                $converted_category = special_sql_str($current_book['Category']); // A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B

                if ($unconverted_category == $converted_category) {
                    $category_filter = " OR a.Category='" . $unconverted_category . "'";
                } else {
                    $category_filter = " OR a.Category='" . $unconverted_category . "' OR a.Category='" . $converted_category . "'";
                }
            } else {
                $category_filter = "";
            }

            if ($current_book['authors']) {
                $author_array = array();
                foreach ((array) $current_book['authors'] as $k => $v) {
                    $unconverted_author = mysql_real_escape_string(str_replace("\\", "\\\\", $v));
                    $converted_author = special_sql_str($v);
                    $author_array[] = $unconverted_author;
                    if ($unconverted_author != $converted_author) {
                        $author_array[] = $converted_author;
                    }
                }
                if (count($author_array)) {
                    $authors = implode("','", $author_array);
                    $author_filter = " OR a.Author IN ('" . $authors . "')";
                } else {
                    $author_filter = "";
                }
            } else {
                $author_filter = "";
            }

            if ($current_book['tags']) {
                foreach ((array) $current_book['tags'] as $tag) {
                    $tags[] = $tag['tag_id'];
                }
                $tags_filter = " OR c.TagID in ('" . implode("','", $tags) . "')";
            } else {
                $tags_filter = "";
            }
            $book_format_filter = ($this->book_formats) ? $this->book_formats : '';
            $filter = $category_filter . $author_filter . $tags_filter;
            if ($filter != "") {
                $filter = substr($filter, 4);
                $filter = " AND (" . $filter . ")";
            }
            // # filter by category or author or tags
            $sql = "SELECT a.BookID, a.Title
		                FROM INTRANET_ELIB_TAG c 
		                INNER JOIN INTRANET_ELIB_BOOK_TAG d on d.TagID=c.TagID
		                RIGHT JOIN INTRANET_ELIB_BOOK a on a.BookID=d.BookID 
		                WHERE a.Publish = 1 AND a.BookID<>'" . $current_book_id . "' " . $book_format_filter . $filter . " GROUP BY a.BookID";

            $result_1 = $this->returnResultSet($sql);
            $book_id_filter = '';
            if (count($result_1) > 0) {
                foreach ((array) $result_1 as $r) {
                    $book_id[] = $r['BookID'];
                }
                $book_id_filter = " AND a.BookID<>'" . $current_book_id . "' AND a.BookID NOT IN ('" . implode("','", $book_id) . "')";
            }

            // # filter by title with similarity >= $percentage_1
            $title_substring = $this->getSubStrings($title);
            $filter_title = "";
            if (count($title_substring) > 0) {
                foreach ($title_substring as $w) {
                    $w = trim($w);
                    $w = mysql_real_escape_string(str_replace("\\", "\\\\", $w));
                    if ($w) {
                        $title_filter .= " OR a.Title LIKE '%" . $w . "%'";
                        ; // filter out blank words
                    }
                }
            }
            if ($title_filter) {
                $title_filter = substr($title_filter, 4);
                $title_filter = " AND (" . $title_filter . ")";
            }

            $sql = "SELECT SQL_CALC_FOUND_ROWS a.BookID, a.Title
		                FROM INTRANET_ELIB_BOOK a 
		                WHERE a.Publish = 1" . $title_filter . $book_id_filter . " " . $book_format_filter . " GROUP BY a.BookID LIMIT 2000";
            $result_2 = $this->returnResultSet($sql);

            $total = current($this->returnVector('select found_rows()'));

            $result_3 = array();
            foreach ((array) $result_2 as $r) {
                similar_text($r['Title'], $title, $percent);
                $percent = round($percent, 0);
                if ($percent >= $percentage_1) {
                    $result_3[] = $r;
                }
            }

            $result = array_merge($result_1, $result_3);

            // # get top most 4 similar title
            $top_similar = array();
            foreach ((array) $result as $k => $book) {
                similar_text($book['Title'], $title, $percent);
                $percent = round($percent, 0);
                $similarity[$k] = $percent;
                if ($percent > $percentage_2) {
                    $book['Similarity'] = $percent;
                    $top_similar[] = $book;
                }
            }

            if (count($top_similar) == 0) {
                if (count($similarity) > 0) {
                    array_multisort($similarity, SORT_DESC, SORT_NUMERIC, $result);
                    $nr_result = count($result);
                    $iMax = $nr_result > $top_most_similar_to_show ? $top_most_similar_to_show : $nr_result;
                    for ($i = 0; $i < $iMax; $i ++) {
                        $top_similar[] = $result[$i];
                    }
                }
            }
            $top_similar = BuildMultiKeyAssoc($top_similar, 'BookID');

            $result = BuildMultiKeyAssoc($result, 'BookID');

            $keys = array_keys($top_similar);
            shuffle($keys);

            $nr_top_similar = count($keys);
            $max_top_similar = $nr_top_similar > $top_most_similar_to_show ? $top_most_similar_to_show : $nr_top_similar;
            for ($i = 0; $i < $max_top_similar; $i ++) {
                $idx = $keys[$i];

                $book = $this->getBookDetailWithoutFormat($top_similar[$idx]['BookID']);
                $book['Similarity'] = $top_similar[$idx]['Similarity'];

                $ret[] = $this->getBooksArrayByFormat($book);
                unset($result[$top_similar[$idx]['BookID']]);
            }

            // # remain similar title
            $keys = array_keys($result);
            shuffle($keys);
            $nr_remain_similar = count($keys);
            if ($nr_remain_similar <= $total_similar_to_show - $top_most_similar_to_show) {
                $max_remain_similar = $nr_remain_similar;
            } else {
                if ($nr_remain_similar + $max_top_similar <= $total_similar_to_show) {
                    $max_remain_similar = $nr_remain_similar;
                } else {
                    $max_remain_similar = $total_similar_to_show - $max_top_similar;
                }
            }

            for ($i = 0; $i < $max_remain_similar; $i ++) {
                $idx = $keys[$i];
                $book = $this->getBookDetailWithoutFormat($result[$idx]['BookID']);
                $book['Similarity'] = $result[$idx]['Similarity'];
                $ret[] = $this->getBooksArrayByFormat($book);
            }
        }

        return $ret;
    }
 // end getSimilarBooks
    function getBookLanguages()
    {
        $sql = "SELECT DISTINCT Language FROM INTRANET_ELIB_BOOK WHERE Language IS NOT NULL AND Language<>'' ORDER BY Language";

        if ($this->convert_utf8)
            $this->dbConnectionUTF8();
        $rs = $this->returnResultSet($sql);
        return $rs;
    }

    function getPhotoSetting()
    {
        $sql = "SELECT value FROM " . $this->libmsDB . ".LIBMS_SYSTEM_SETTING WHERE name='preference_photo_show'";
        $rs = $this->returnResultSet($sql);
        return count($rs) ? $rs[0]['value'] : 0;
    }

    // ##################################
    // # start of my friend functions

    // exclude ignore notifications
    public function getFriendNotification($recipientID = '', $senderID = '', $notificationID = '', $event = array(), $status = array())
    {
        $sql = "SELECT s.BookID, s.Comments, n.* FROM INTRANET_ELIB_FRIEND_NOTIFICATION n
				        INNER JOIN INTRANET_ELIB_FRIEND_SHARE_BOOK s ON s.ShareID=n.ShareID
				        INNER JOIN INTRANET_ELIB_BOOK b ON b.BookID=s.BookID and b.Publish=1  
				WHERE 
				        n.Status<>'-1'";

        if ($recipientID) {
            $sql .= " AND n.RecipientID='" . $recipientID . "'";
        }
        if ($senderID) {
            $sql .= " AND n.SenderID='" . $senderID . "'";
        }
        if ($notificationID) {
            $sql .= " AND n.NotificationID='" . $notificationID . "'";
        }
        if (count($event)) {
            $sql .= " AND n.Event IN ('" . implode("','", (array) $event) . "')";
        }
        if (count($status)) {
            $sql .= " AND n.Status IN ('" . implode("','", (array) $status) . "')";
        }
        $sql .= " ORDER BY n.IssuedOn DESC";

        if ($this->convert_utf8)
            $this->dbConnectionUTF8();
        $rs = $this->returnResultSet($sql);

        // get sender info
        foreach ((array) $rs as $k => $v) {
            list ($rs[$k]['name'], $rs[$k]['class'], $rs[$k]['user_image']) = $this->getUserInfo($v['SenderID']);
        }

        return $rs;
    }

    // count new notification from friends
    public function getFriendNotificationCount($userID = '')
    {
        if (! $userID) {
            $userID = $this->user_id;
        }
        $sql = "SELECT 
                        COUNT(1) AS CNT 
                FROM 
                        INTRANET_ELIB_FRIEND_NOTIFICATION 
                WHERE 
                        RecipientID='" . $userID . "' 
                        AND Status='1'
                        AND (ShareID NOT IN (
                            SELECT 
                                s.ShareID 
                            FROM
                                INTRANET_ELIB_FRIEND_NOTIFICATION n
                                INNER JOIN INTRANET_ELIB_FRIEND_SHARE_BOOK s ON s.ShareID=n.ShareID
                                INNER JOIN INTRANET_ELIB_BOOK b ON b.BookID=s.BookID AND b.Publish=0
                            WHERE 
                                n.RecipientID='" . $userID . "') 
                            OR ShareID IS NULL)";
        $rs = $this->returnResultSet($sql);
        if (count($rs)) {
            $rs = current($rs);
            $rs = $rs['CNT'];
        } else {
            $rs = 0;
        }
        return $rs ? $rs : 0;
    }

    // get my friend ID list, return 1D array, $userID is my UserID
    public function getMyFriendIDs($userID = '')
    {
        if (! $userID) {
            $userID = $this->user_id;
        }
        $sql = "SELECT FriendID FROM INTRANET_ELIB_FRIEND_FRIENDS WHERE UserID='" . $userID . "'";
        $sql .= " UNION ";
        $sql .= "SELECT UserID as FriendID FROM INTRANET_ELIB_FRIEND_FRIENDS WHERE FriendID='" . $userID . "'";
        $rs = $this->returnVector($sql);

        return (count($rs) > 0) ? $rs : array();
    }

    public function getMyFriends($userID = '', $classNameWithBracket = false)
    {
        global $intranet_session_language;

        $myFriendIDs = $this->getMyFriendIDs($userID);
        $userNameField = $intranet_session_language == 'en' ? 'EnglishName' : 'ChineseName';

        $sql = "SELECT UserID as user_id, $userNameField FROM INTRANET_USER WHERE UserID IN ('" . implode("','", (array) $myFriendIDs) . "') ORDER BY $userNameField";
        if ($this->convert_utf8) {
            $this->dbConnectionLatin1();
            $users = $this->returnResultSet($sql);
            $this->dbConnectionUTF8();
        } else {
            $users = $this->returnResultSet($sql);
        }

        if (count($users)) {
            foreach ($users as &$user) {
                list ($user['name'], $user['class'], $user['image']) = $this->getUserInfo($user['user_id'], $classNameWithBracket);
            }
        }

        return $users;
    }

    // get pending friend list (waiting for friend accept)
    public function getPendingFriends($notificationID = '', $classNameWithBracket = false)
    {
        global $intranet_session_language;

        $userNameField = $intranet_session_language == 'en' ? 'u.EnglishName' : 'u.ChineseName';
        $cond = '';
        if ($notificationID) {
            $cond .= " AND n.NotificationID='" . $notificationID . "'";
        }
        $sql = "SELECT 
							n.NotificationID, 
							u.UserID as user_id, 
							$userNameField 
				FROM 
							INTRANET_USER u
				INNER JOIN 
							INTRANET_ELIB_FRIEND_NOTIFICATION n ON n.RecipientID=u.UserID
				WHERE 
							n.SenderID='" . $this->user_id . "'
				AND			n.Status=1
				AND			n.Event=0
				{$cond}
				ORDER BY $userNameField";

        if ($this->convert_utf8) {
            $this->dbConnectionLatin1();
            $users = $this->returnResultSet($sql);
            $this->dbConnectionUTF8();
        } else {
            $users = $this->returnResultSet($sql);
        }

        if (count($users)) {
            foreach ($users as &$user) {
                list ($user['name'], $user['class'], $user['image']) = $this->getUserInfo($user['user_id'], $classNameWithBracket);
            }
        }

        return $users;
    }

    // get record up to two weeks before
    public function getFriendActivities($myUserID = '', $cond = '')
    {
        if (empty($cond)) {
            $cond_a = " AND r.BorrowTime>=DATE_ADD(CURDATE(),interval -2 WEEK)";
            $cond_b = " AND r.DateModified>=DATE_ADD(CURDATE(),interval -2 WEEK)";
        }
        $myFriendIDs = $this->getMyFriendIDs($myUserID);

        $sql = "(SELECT 	'' as review_id,
						r.UserID as user_id,
						'' as rating,
						'' as content,
						r.BorrowTime as activity_date,
						'' as likes,
						b.Author,
						b.BookFormat,
						b.BookID,
						b.CallNumber,
						b.Category,
						b.HitRate,
						b.ISBN,
						b.Language,
						b.Level,
						b.Preface,
						b.Publisher,
						b.RelevantSubject,
						b.SubCategory,
						b.Title,
						'borrow' as activity
                FROM  
						" . $this->libmsDB . ".LIBMS_BORROW_LOG r
				LEFT JOIN
						INTRANET_ELIB_BOOK b ON b.BookID=r.BookID + " . $this->physical_book_init_value . " AND b.Publish = '1' 
                WHERE 
						r.UserID IN ('" . implode("','", (array) $myFriendIDs) . "')
				AND		r.RecordStatus='BORROWED'" . $cond_a . ")
				UNION
				(SELECT 	r.ReviewID as review_id, 
							r.UserID as user_id, 
							r.Rating as rating, 
							r.Content as content,
	                		r.DateModified as activity_date,
	                		count(c.ReviewCommentID) as likes,
							b.Author,
							b.BookFormat,
							b.BookID,
							b.CallNumber,
							b.Category,
							b.HitRate,
							b.ISBN,
							b.Language,
							b.Level,
							b.Preface,
							b.Publisher,
							b.RelevantSubject,
							b.SubCategory,
							b.Title,
							'review' as activity
	            FROM  
							INTRANET_ELIB_BOOK_REVIEW r
	            LEFT JOIN 
							INTRANET_ELIB_BOOK_REVIEW_HELPFUL c on r.ReviewID=c.ReviewID AND c.Choose=1
				INNER JOIN 
							INTRANET_ELIB_BOOK b ON b.BookID=r.BookID
	            WHERE 
							r.UserID IN ('" . implode("','", (array) $myFriendIDs) . "')" . $cond_b . "
				AND 	b.Publish = '1'
	            GROUP BY r.ReviewID)
	            ORDER BY activity_date DESC ";

        if ($this->convert_utf8) {
            $this->dbConnectionUTF8();
        }
        $books = $this->returnResultSet($sql);

        $returnArray = array();
        foreach ((array) $books as $book) {
            if ($book['BookID']) {
                $entry = $this->getBooksArrayByFormat($book);
                $entry['book_id'] = $entry['id'];
                $entry['review_id'] = $book['review_id'];
                $entry['user_id'] = $book['user_id'];
                $entry['content'] = strip_tags($book['content']);
                $entry['activity_date'] = $book['activity_date'];
                $entry['likes'] = $book['likes'];
                $entry['rating'] = $book['rating'];
                $entry['activity'] = $book['activity'];
                list ($entry['name'], $entry['class'], $entry['user_image']) = $this->getUserInfo($book['user_id']);
                $returnArray[] = $entry;
            }
        }

        return $returnArray;
    }

    // get class name list which classlevel is the same as LoginUser's
    public function getMyClassLevelClassList($userID, $className = '')
    {
        // retrieve two fields for building selection box
        if ($this->convert_utf8) { // ej
            $cond = $className ? "AND ic.ClassName='" . $className . "'" : '';
            $sql = "SELECT 
							ic.ClassName 
					FROM 	
							INTRANET_CLASS ic 
					INNER JOIN ( 
								SELECT 
										icl.ClassLevelID
								FROM 	
										INTRANET_CLASS ic
								INNER JOIN
										INTRANET_USER u ON u.ClassName=ic.ClassName 
								INNER JOIN 
										INTRANET_CLASSLEVEL icl ON icl.ClassLevelID = ic.ClassLevelID 
								WHERE 
										ic.RecordStatus = 1 
								AND 	icl.RecordStatus = 1 
								AND 	u.UserID='" . $userID . "') icl
							on icl.ClassLevelID=ic.ClassLevelID					
					WHERE 
							ic.RecordStatus = 1 
							{$cond}
					ORDER BY ic.ClassName";
            $this->dbConnectionLatin1();
            $rs = $this->returnResultSet($sql);

            if (count($rs)) {
                foreach ((array) $rs as $k => $v) {
                    $className = convert2unicode($v['ClassName'], true);
                    $rs[$k]['ClassName'] = $className;
                    $rs[$k]['DispClassName'] = $className;
                    $rs[$k][0] = $className;
                    $rs[$k][1] = $className;
                }
            }
            $this->dbConnectionUTF8();
        } else { // ip
            $AcademicYearID = Get_Current_Academic_Year_ID();
            $cond = $className ? "AND yc.ClassTitleEN='" . $className . "'" : '';
            $dispClassName = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
            $sql = "SELECT
						  	yc.ClassTitleEN as ClassName,
						  	{$dispClassName} as DispClassName
					FROM
							YEAR_CLASS yc
					INNER JOIN
							(
								SELECT 
										y.YearID
								FROM 	
										YEAR y
								INNER JOIN 
										YEAR_CLASS yc on yc.YearID=y.YearID AND yc.AcademicYearID='" . $AcademicYearID . "'
								INNER JOIN
										YEAR_CLASS_USER ycu on ycu.YearClassID=yc.YearClassID
								INNER JOIN
										INTRANET_USER u on u.UserID=ycu.UserID
								WHERE u.UserID='" . $userID . "'
							) y on y.YearID=yc.YearID
					WHERE
							yc.AcademicYearID='" . $AcademicYearID . "'
							{$cond}	
					ORDER BY 1";
            $rs = $this->returnArray($sql); // need to return numeric array as it'll be used by list method to in building selection box
        }
        return $rs;
    }

    public function getMyFriendRankingReadingeBook($limit = 5)
    {
        $users = array();
        $dateRange = $this->getDayRangeCondition("DateModified");
        $myFriendIDs = $this->getMyFriendIDs();
        if (count($myFriendIDs)) {
            $sql = "SELECT 
							h.UserID AS user_id, 
							COUNT(DISTINCT h.BookID) AS book_count 
					FROM 
							INTRANET_ELIB_BOOK_HISTORY h
							INNER JOIN INTRANET_ELIB_BOOK b ON b.BookID=h.BookID  
					WHERE 
							h.UserID in ('" . implode("','", (array) $myFriendIDs) . "') AND b.Publish=1 " . $dateRange . " 
					GROUP BY
							h.UserID 
					ORDER BY 
							book_count DESC
					LIMIT $limit";
            $users = $this->returnResultSet($sql);

            foreach ($users as &$user) {
                list ($user['name'], $user['class'], $user['image']) = $this->getUserInfo($user['user_id'], true);
            }
        }
        return $users;
    }

    public function addFriendRequest($studentID)
    {
        $sql = "INSERT IGNORE INTO INTRANET_ELIB_FRIEND_NOTIFICATION
		    	(SenderID, RecipientID, Status, Event, IssuedOn, DateModified, LastModifiedBy)
                    VALUES
		    	('" . $this->user_id . "', '" . $studentID . "', '1', '0', now(), now(),'" . $this->user_id . "')";
        $ret = $this->db_db_query($sql);
        $notificationID = $this->db_insert_id();
        return array(
            $ret,
            $notificationID
        );
    }

    public function addFriend($data)
    {
        $sql = "INSERT IGNORE INTO INTRANET_ELIB_FRIEND_FRIENDS
		    	(UserID, FriendID, DateModified, LastModifiedBy)
                    VALUES
		    	('" . $data['UserID'] . "', '" . $data['FriendID'] . "', now(),'" . $this->user_id . "')";
        $ret = $this->db_db_query($sql);
        return $ret;
    }

    public function acceptFriendRequest($notificationID)
    {
        $ret = array();
        if ($notificationID) {
            $notification = $this->getFriendNotification('', '', $notificationID);
            $notification = current($notification);
            $data['UserID'] = $notification['RecipientID'];
            $data['FriendID'] = $notification['SenderID'];

            $this->Start_Trans();
            // 1. add friend
            $ret[] = $this->addFriend($data);

            // 2. add notification to inform sender
            $sql = "INSERT IGNORE INTO INTRANET_ELIB_FRIEND_NOTIFICATION
			    	(SenderID, RecipientID, Status, Event, IssuedOn, DateModified, LastModifiedBy)
	                    VALUES
			    	('" . $data['UserID'] . "', '" . $data['FriendID'] . "', '1', '1', now(), now(),'" . $this->user_id . "')";
            $ret[] = $this->db_db_query($sql);

            // 3. delete old notification
            $sql = "DELETE FROM INTRANET_ELIB_FRIEND_NOTIFICATION WHERE NotificationID='" . $notificationID . "'";
            $ret[] = $this->db_db_query($sql);

            if (! in_array(false, $ret)) {
                $this->Commit_Trans();
                return true;
            } else {
                $this->RollBack_Trans();
                return false;
            }
        } else {
            return false;
        }
    }

    public function ignoreFriendRequest($notificationID)
    {
        $sql = "UPDATE INTRANET_ELIB_FRIEND_NOTIFICATION SET Status=-1, DateModified=now(), LastModifiedBy='" . $this->user_id . "' WHERE NotificationID='" . $notificationID . "'";
        $ret = $this->db_db_query($sql);
        return $ret;
    }

    // for new record only
    public function updateRecipientView()
    {
        $ret = array();
        // update view count
        $sql = "UPDATE INTRANET_ELIB_FRIEND_NOTIFICATION SET RecipientView=RecipientView+1, DateModified=now(), LastModifiedBy='" . $this->user_id . "' WHERE RecipientID='" . $this->user_id . "' AND Status='1'";
        $ret[] = $this->db_db_query($sql);

        // update status to old if not first view
        $sql = "UPDATE INTRANET_ELIB_FRIEND_NOTIFICATION SET Status='0', DateModified=now(), LastModifiedBy='" . $this->user_id . "' WHERE RecipientID='" . $this->user_id . "' AND Status='1' AND RecipientView > 1";
        $ret[] = $this->db_db_query($sql);

        return $ret;
    }

    public function removeNotification($notificationID)
    {
        $sql = "DELETE FROM INTRANET_ELIB_FRIEND_NOTIFICATION WHERE NotificationID='" . $notificationID . "'";
        $ret = $this->db_db_query($sql);
        return $ret;
    }

    public function shareBook($data)
    {
        $ret = array();

        if ($data['book_id'] && ! empty($data['selected_friend_ids'])) {
            $this->Start_Trans();

            // 1. add share book
            $sql = "INSERT IGNORE INTO INTRANET_ELIB_FRIEND_SHARE_BOOK
			    	(UserID, BookID, Comments, DateModified, LastModifiedBy)
	                    VALUES
			    	('" . $this->user_id . "', '" . $data['book_id'] . "'," . $this->pack_value($data['Comments'], 'str') . ", now(), '" . $this->user_id . "')";
            $res = $this->db_db_query($sql);

            $ret[] = $res;
            if ($res) {
                $shareID = $this->db_insert_id();

                // 2. add shared to
                $sql = "INSERT IGNORE INTO INTRANET_ELIB_FRIEND_SHARE_TO (ShareID, FriendID) VALUES ";
                $sqlAry = array();
                $sql2Ary = array();
                foreach ((array) $data['selected_friend_ids'] as $friend_id) {
                    $sqlAry[] = "('" . $shareID . "', '" . $friend_id . "')";
                    $sql2Ary[] = "('" . $this->user_id . "', '" . $friend_id . "', '1', '3', '" . $shareID . "', now(), now(),'" . $this->user_id . "')";
                }

                if (count($sqlAry)) {
                    $sql .= implode(",", $sqlAry);
                }
                $ret[] = $this->db_db_query($sql);

                // 3. add notification
                $sql = "INSERT IGNORE INTO INTRANET_ELIB_FRIEND_NOTIFICATION
				    	(SenderID, RecipientID, Status, Event, ShareID, IssuedOn, DateModified, LastModifiedBy) VALUES ";
                if (count($sql2Ary)) {
                    $sql .= implode(",", $sql2Ary);
                }
                $ret[] = $this->db_db_query($sql);
            }

            if (! in_array(false, $ret)) {
                $this->Commit_Trans();
                return true;
            } else {
                $this->RollBack_Trans();
                return false;
            }
        } else {
            return false;
        }
    }

    /*
     * no DateRange restriction
     * $parameter = array('FriendID'=>'','sortby'=>'', 'order'=>'', 'offset'=>'', 'limit'=>'')
     */
    public function getMyFriendeBookViewed($parameter = array())
    {

        // $dateRange = $this->getDayRangeCondition("h.DateModified");
        $dateRange = '';
        $sql = "SELECT 	SQL_CALC_FOUND_ROWS
						b.*,
						MAX(h.DateModified) AS reading_date
				FROM 
						INTRANET_ELIB_BOOK_HISTORY h
				INNER JOIN
						INTRANET_ELIB_BOOK b ON b.BookID=h.BookID 
				WHERE 
						h.UserID='" . $parameter['FriendID'] . "' AND b.Publish=1 " . $dateRange . " 
				GROUP BY
						h.BookID 
				ORDER BY " . $parameter['sortby'] . ' ' . $parameter['order'] . "
				LIMIT " . $parameter['offset'] . ', ' . $parameter['limit'];

        if ($this->convert_utf8) {
            $this->dbConnectionUTF8();
        }
        $books = $this->returnResultSet($sql);
        $total = current($this->returnVector('select found_rows()'));

        $returnArray = array();
        foreach ($books as $book) {
            $entry = $this->getBooksArrayByFormat($book);
            $entry['reading_date'] = $book['reading_date'];
            $returnArray[] = $entry;
        }
        return array(
            $total,
            $returnArray
        );
    }

    /*
     * no DateRange restriction
     * including deleted book which status is normal when borrowed
     * $parameter = array('FriendID'=>'','sortby'=>'', 'order'=>'', 'offset'=>'', 'limit'=>'')
     */
    public function getMyFriendBooksBorrowed($parameter = array())
    {

        // $dateRange = $this->getDayRangeCondition("a.BorrowTime");
        $dateRange = '';
        $sql = "SELECT 	SQL_CALC_FOUND_ROWS
						b.BookID,
						b.Category,
						b.SubCategory,
						b.Level,
						'physical' AS BookFormat,
						MAX(a.BorrowTime) as BorrowTime,
						a.BookID AS LIBMS_BookID 
				FROM 
						" . $this->libmsDB . ".LIBMS_BORROW_LOG a
				LEFT JOIN
						INTRANET_ELIB_BOOK b ON b.BookID=a.BookID + " . $this->physical_book_init_value . " 
				WHERE 
						a.UserID='" . $parameter['FriendID'] . "' " . $dateRange . "
				GROUP BY b.BookID 
				ORDER BY " . $parameter['sortby'] . ' ' . $parameter['order'] . "
				LIMIT " . $parameter['offset'] . ', ' . $parameter['limit'];

        if ($this->convert_utf8) {
            $this->dbConnectionUTF8();
        }
        $books = $this->returnResultSet($sql);
        $total = current($this->returnVector('select found_rows()'));

        $returnArray = array();
        foreach ($books as $book) {
            if ($book['BookID']) {
                $entry = $this->getBooksArrayByFormat($book);
                $returnArray[] = $entry;
            } else {
                $sql = "SELECT BookTitle as title, ResponsibilityBy1 AS author, Publisher AS publisher FROM " . $this->libmsDB . ".LIBMS_BOOK WHERE BookID='" . $book['LIBMS_BookID'] . "'";
                if ($this->convert_utf8) {
                    $this->dbConnectionUTF8();
                }
                $libms_book = $this->returnResultSet($sql);
                $libms_book = current($libms_book);
                $libms_book['id'] = '';
                $returnArray[] = $libms_book;
            }
        }
        return array(
            $total,
            $returnArray
        );
    }

    /*
     * no DateRange restriction
     * $parameter = array('FriendID'=>'','sortby'=>'', 'order'=>'', 'offset'=>'', 'limit'=>'')
     * sharing is bi-direction
     */
    public function getSharingBooks($parameter = array())
    {
        // $dateRange = $this->getDayRangeCondition("s.DateModified");
        $dateRange = '';
        $sql = "SELECT 	SQL_CALC_FOUND_ROWS
						b.BookID,
						b.Title,
						b.Author,
						b.Publisher,
						b.Category,
						b.SubCategory,
						b.Level,
						b.BookFormat,
						a.Direction,
						a.DateModified,
						a.Comments
				FROM 	INTRANET_ELIB_BOOK b
				INNER JOIN (
						SELECT  s.BookID,
								'ToMe' AS Direction,
								s.DateModified,
								s.Comments
						FROM 
								INTRANET_ELIB_FRIEND_SHARE_BOOK s
						INNER JOIN
								INTRANET_ELIB_FRIEND_SHARE_TO t ON t.ShareID=s.ShareID
						WHERE 
								s.UserID='" . $parameter['FriendID'] . "' " . $dateRange . "
						AND		t.FriendID='" . $this->user_id . "' 
					UNION
						SELECT  s.BookID,
								'ByMe' AS Direction,
								s.DateModified,
								s.Comments
						FROM 
								INTRANET_ELIB_FRIEND_SHARE_BOOK s
						INNER JOIN
								INTRANET_ELIB_FRIEND_SHARE_TO t ON t.ShareID=s.ShareID
						WHERE 
								s.UserID='" . $this->user_id . "' " . $dateRange . "
						AND		t.FriendID='" . $parameter['FriendID'] . "' 
					
				) a ON a.BookID=b.BookID
				WHERE b.Publish=1 
				ORDER BY " . $parameter['sortby'] . ' ' . $parameter['order'] . "
				LIMIT " . $parameter['offset'] . ', ' . $parameter['limit'];

        $books = $this->returnResultSet($sql);
        $total = current($this->returnVector('select found_rows()'));

        $returnArray = array();
        foreach ($books as $book) {
            $entry = $this->getBooksArrayByFormat($book);
            $entry['Direction'] = $book['Direction'];
            $entry['DateModified'] = $book['DateModified'];
            $entry['Comments'] = $book['Comments'];
            $returnArray[] = $entry;
        }
        return array(
            $total,
            $returnArray
        );
    }

    // delete relation and notification of request / accept friend request, keep other data such as shared book info
    public function removeFriend($friendID)
    {
        $ret = array();
        if ($friendID) {
            $this->Start_Trans();
            $sql = "DELETE FROM INTRANET_ELIB_FRIEND_FRIENDS WHERE (FriendID='" . $friendID . "' AND UserID='" . $this->user_id . "') OR (UserID='" . $friendID . "' AND FriendID='" . $this->user_id . "')";
            $ret[] = $this->db_db_query($sql);

            $sql = "DELETE FROM INTRANET_ELIB_FRIEND_NOTIFICATION WHERE 
						((SenderID='" . $this->user_id . "' AND RecipientID='" . $friendID . "') OR 
						(RecipientID='" . $this->user_id . "' AND SenderID='" . $friendID . "'))
					AND Event IN (0,1) 
					AND Status<>-1";
            $ret[] = $this->db_db_query($sql);

            if (! in_array(false, $ret)) {
                $this->Commit_Trans();
                return true;
            } else {
                $this->RollBack_Trans();
                return false;
            }
        } else {
            return false;
        }
    }

    // by $bookID shared by me
    public function getSharedByMeOfABook($bookID)
    {
        $sql = "SELECT 	s.ShareID,
						s.Comments,
						s.DateModified,
						GROUP_CONCAT(DISTINCT t.FriendID ORDER BY FriendID SEPARATOR ',') AS FriendIDs
				FROM  
						INTRANET_ELIB_FRIEND_SHARE_BOOK s
				INNER JOIN 
						INTRANET_ELIB_FRIEND_SHARE_TO t ON t.ShareID=s.ShareID
				WHERE 
						s.UserID='" . $this->user_id . "'
				AND		s.BookID='" . $bookID . "'
				GROUP BY s.ShareID
				ORDER BY s.DateModified DESC";

        $sharings = $this->returnResultSet($sql);

        $classNameWithBracket = true;
        $returnArray = array();
        foreach ($sharings as $k => $sharing) {
            $friendIDs_array = explode(",", $sharing['FriendIDs']);
            foreach ((array) $friendIDs_array as $friend_id) {
                $user = array();
                list ($user['name'], $user['class'], $user['image']) = $this->getUserInfo($friend_id, $classNameWithBracket);
                $sharings[$k]['friends'][$friend_id] = $user['name'] . ' ' . $user['class'];
            }
        }
        return $sharings;
    }

    // get sharing from my friends for specific book
    public function getSharedToMeOfABook($bookID, $limit = '')
    {
        $users = array();

        $myFriendIDs = $this->getMyFriendIDs();
        if ($bookID) {
            $sql = "SELECT 
							s.ShareID,
							s.UserID as user_id,
							s.BookID,
							s.Comments,
							s.DateModified  
					FROM 
							INTRANET_ELIB_FRIEND_SHARE_BOOK s
					INNER JOIN
							INTRANET_ELIB_FRIEND_SHARE_TO t ON t.ShareID=s.ShareID
					WHERE 
							s.BookID='" . $bookID . "'
					AND 	s.UserID IN ('" . implode("','", (array) $myFriendIDs) . "')
					AND		t.FriendID='" . $this->user_id . "'
					ORDER BY s.DateModified DESC";
            if ($limit) {
                $sql .= " LIMIT " . $limit;
            }

            $users = $this->returnResultSet($sql);
            if (count($users)) {
                $classNameWithBracket = true;
                foreach ($users as &$user) {
                    list ($user['name'], $user['class'], $user['image']) = $this->getUserInfo($user['user_id'], $classNameWithBracket);
                }
            }
        }
        return $users;
    }

    // delete shared to me item
    public function removeSharedToMe($shareID)
    {
        $sql = "DELETE FROM INTRANET_ELIB_FRIEND_SHARE_TO WHERE ShareID='" . $shareID . "' AND FriendID='" . $this->user_id . "'";

        $ret = $this->db_db_query($sql);
        return $ret;
    }

    public function getMostBorrowFriends($limit = 10, $classNameWithBracket = true)
    {
        $myFriendIDs = $this->getMyFriendIDs();
        $cond = " AND b.UserID in ('" . implode("','", (array) $myFriendIDs) . "')";

        $day = $this->getDayRangeCondition("a.BorrowTime");

        $sql = "SELECT a.UserID as user_id, 
                COUNT(DISTINCT a.BookID) AS loan_count 
                FROM " . $this->libmsDB . ".LIBMS_BORROW_LOG a
		INNER JOIN " . $this->libmsDB . ".LIBMS_USER u ON a.UserID = u.UserID AND u.UserType = 'S'
                INNER JOIN INTRANET_USER b ON a.UserID=b.UserID 
                $day 
		WHERE 1=1 $cond
                GROUP BY a.UserID 
                ORDER BY loan_count DESC
		LIMIT $limit";

        $users = $this->returnArray($sql);

        foreach ($users as &$user) {
            list ($user['name'], $user['class'], $user['image']) = $this->getUserInfo($user['user_id'], $classNameWithBracket);
        }

        return $users;
    }

    // get ClassName (ej), YearClassID (ip) and ClassLevelID (ej), YearID (ip) of the student
    public function getClassAndClassLevelID()
    {
        $rs = array();
        if ($this->convert_utf8) { // ej
            $sql = "SELECT 
							ic.ClassName,
							icl.ClassLevelID
					FROM 	
							INTRANET_CLASS ic
					INNER JOIN
							INTRANET_USER u ON u.ClassName=ic.ClassName 
					INNER JOIN 
							INTRANET_CLASSLEVEL icl ON icl.ClassLevelID = ic.ClassLevelID 
					WHERE 
							ic.RecordStatus = 1 
					AND 	icl.RecordStatus = 1 
					AND 	u.UserID='" . $this->user_id . "'";
            $this->dbConnectionLatin1();
            $rs = $this->returnResultSet($sql);
            // if (count($rs)) {
            // foreach((array)$rs as $k=>$v) {
            // $className = convert2unicode($v['ClassName'],true);
            // $rs[$k]['ClassName'] = $className;
            // }
            // }
            $this->dbConnectionUTF8();
        } else {
            $AcademicYearID = Get_Current_Academic_Year_ID();
            $sql = "SELECT 
							yc.YearClassID,
							y.YearID
					FROM 	
							YEAR y
					INNER JOIN 
							YEAR_CLASS yc on yc.YearID=y.YearID AND yc.AcademicYearID='" . $AcademicYearID . "'
					INNER JOIN
							YEAR_CLASS_USER ycu on ycu.YearClassID=yc.YearClassID
					INNER JOIN
							INTRANET_USER u on u.UserID=ycu.UserID
					WHERE u.UserID='" . $this->user_id . "'";
            $rs = $this->returnResultSet($sql);
        }
        return $rs;
    }

    /*
     * available user: (1) not in notification list (exclude status=ignore)
     * (2) not in friend list
     * (3) exlcude myself
     */
    public function getMyFriendExcludeUser()
    {
        $excludeUser = array();
        $mySendingList = $this->getFriendNotification('', $_SESSION['UserID'], '', array(
            0,
            1
        )); // including request & accept, but not ignore
        if (count($mySendingList) > 0) {
            foreach ((array) $mySendingList as $v) {
                $excludeUser[] = $v['RecipientID']; // do not show that's sent request before
            }
        }

        $myReceivingList = $this->getFriendNotification($_SESSION['UserID'], '', '', array(
            0,
            1
        )); // including request & accept, but not ignore
        if (count($myReceivingList) > 0) {
            foreach ((array) $myReceivingList as $v) {
                $excludeUser[] = $v['SenderID']; // do not show that's sent request before
            }
        }

        $excludeUser[] = $_SESSION['UserID']; // exclude myself
        $myFriendIDs = $this->getMyFriendIDs();

        $excludeUser = array_unique(array_merge($excludeUser, $myFriendIDs));
        return $excludeUser;
    }

    public function getSearchPendingFriend($keyword, $classNameWithBracket = false)
    {
        $ukw = mysql_real_escape_string(str_replace("\\", "\\\\", $keyword)); // A&<>'"\B ==> A&<>\'\"\\\\B
        $ckw = intranet_htmlspecialchars($keyword);
        if ($ukw == $ckw) {
            $cond .= " AND (u.UserLogin LIKE '%" . $ukw . "%' OR u.EnglishName LIKE '%" . $ukw . "%' OR u.ChineseName LIKE '%" . $ukw . "%')";
        } else {
            $cond .= " AND (u.UserLogin LIKE '%" . $ukw . "%' OR u.UserLogin LIKE '%" . $ckw . "%'";
            $cond .= " OR u.EnglishName LIKE '%" . $ukw . "%' OR u.EnglishName LIKE '%" . $ckw . "%'";
            $cond .= " OR u.ChineseName LIKE '%" . $ukw . "%' OR u.ChineseName LIKE '%" . $ckw . "%')";
        }

        $userNameField = Get_Lang_Selection('u.ChineseName', 'u.EnglishName');

        if ($this->convert_utf8) { // ej
            $sql = "SELECT	
								n.NotificationID,
								u.UserID as user_id,
								u.EnglishName AS OrderName,
								{$userNameField}
					FROM 	
								INTRANET_USER_UTF8 u
					INNER JOIN 	
								INTRANET_ELIB_FRIEND_NOTIFICATION n ON n.RecipientID=u.UserID 
					WHERE 
								u.RecordStatus='1'
					AND			n.SenderID='" . $this->user_id . "'
					AND			n.Status=1
					AND			n.Event=0
						{$cond} ";
            $sql .= " ORDER BY OrderName";
            $this->dbConnectionLatin1();
            $users = $this->returnResultSet($sql);
            $this->dbConnectionUTF8();
        } else {
            $sql = "SELECT	
								n.NotificationID,
								u.UserID as user_id,
								u.EnglishName AS OrderName,
								{$userNameField}
					FROM 	
								INTRANET_USER u
					INNER JOIN 	
								INTRANET_ELIB_FRIEND_NOTIFICATION n ON n.RecipientID=u.UserID 
					WHERE 
								u.RecordStatus='1'
					AND			n.SenderID='" . $this->user_id . "'
					AND			n.Status=1
					AND			n.Event=0
						{$cond} ";
            $sql .= " ORDER BY OrderName";
            $users = $this->returnResultSet($sql);
        }

        if (count($users)) {
            foreach ($users as &$user) {
                list ($user['name'], $user['class'], $user['image']) = $this->getUserInfo($user['user_id'], $classNameWithBracket);
            }
        }

        return $users;
    }

    // allow to search student & teacher
    public function getSearchUser($keyword, $scope, $action = 'add', $classNameWithBracket = false)
    {
        $join_sql = "";
        $cond = "";
        $studentCond = "";
        $lookupStudent = true;
        $users = array();

        if (empty($keyword)) {
            return array(); // terminate if no keyword
        }

        if ($action == 'add') {
            $excludeUser = $this->getMyFriendExcludeUser();
            $includeUser = array();
        } else {
            $excludeUser = array(
                $_SESSION['UserID']
            );
            $includeUser = $this->getMyFriendIDs($_SESSION['UserID']);
            if (count($includeUser) == 0) {
                return array(); // terminate if no friends
            }
        }

        if (count($excludeUser)) {
            $cond .= " AND u.UserID NOT IN ('" . implode("','", $excludeUser) . "')";
        }
        if (count($includeUser)) {
            $cond .= " AND u.UserID IN ('" . implode("','", $includeUser) . "')";
        }

        if ($scope == 0 || $scope == 1) { // class or class level
            $classInfo = $this->getClassAndClassLevelID();
        }

        $ukw = mysql_real_escape_string(str_replace("\\", "\\\\", $keyword)); // A&<>'"\B ==> A&<>\'\"\\\\B
                                                                            // $ckw = special_sql_str($keyword); // A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B
        $ckw = intranet_htmlspecialchars($keyword);
        if ($ukw == $ckw) {
            $cond .= " AND (u.UserLogin LIKE '%" . $ukw . "%' OR u.EnglishName LIKE '%" . $ukw . "%' OR u.ChineseName LIKE '%" . $ukw . "%')";
        } else {
            $cond .= " AND (u.UserLogin LIKE '%" . $ukw . "%' OR u.UserLogin LIKE '%" . $ckw . "%'";
            $cond .= " OR u.EnglishName LIKE '%" . $ukw . "%' OR u.EnglishName LIKE '%" . $ckw . "%'";
            $cond .= " OR u.ChineseName LIKE '%" . $ukw . "%' OR u.ChineseName LIKE '%" . $ckw . "%')";
        }

        $userNameField = Get_Lang_Selection('u.ChineseName', 'u.EnglishName');

        if ($this->convert_utf8) { // ej
            if ($scope == 0) {
                if ($classInfo[0]['ClassName']) {
                    $studentCond .= " AND u.ClassName='" . $classInfo[0]['ClassName'] . "'";
                } else {
                    $lookupStudent = false;
                }
            } else if ($scope == 1) {
                $join_sql .= " INNER JOIN INTRANET_USER u2 on u2.UserID=u.UserID";
                $join_sql .= " INNER JOIN INTRANET_CLASS ic on ic.ClassName=u2.ClassName";
                $join_sql .= " INNER JOIN INTRANET_CLASSLEVEL icl on icl.ClassLevelID = ic.ClassLevelID";
                if ($classInfo[0]['ClassLevelID']) {
                    $studentCond .= " AND icl.ClassLevelID='" . $classInfo[0]['ClassLevelID'] . "'";
                    $studentCond .= " AND ic.RecordStatus=1";
                    $studentCond .= " AND icl.RecordStatus=1";
                } else {
                    $lookupStudent = false;
                }
            }

            $sql = "SELECT	
								u.UserID as user_id,
								u.EnglishName AS OrderName,
								{$userNameField}
					FROM 	
								INTRANET_USER_UTF8 u
					WHERE 
								u.RecordStatus='1'
					AND
								u.RecordType='1'
						{$cond} ";

            if ($lookupStudent) {
                $sql .= " UNION 
					SELECT	
								u.UserID as user_id,
								u.EnglishName AS OrderName,
								{$userNameField}
					FROM 	
								INTRANET_USER_UTF8 u
						{$join_sql}
					WHERE 
								u.RecordStatus='1'
					AND
								u.RecordType='2'
						{$cond}
						{$studentCond} ";
            }

            $sql .= " ORDER BY OrderName";
            $this->dbConnectionLatin1();
            $users = $this->returnResultSet($sql);
            $this->dbConnectionUTF8();
        } else { // ip
            $AcademicYearID = Get_Current_Academic_Year_ID();
            if ($scope == 0) { // same class
                $join_sql .= " INNER JOIN YEAR_CLASS_USER ycu on ycu.UserID=u.UserID ";
                $join_sql .= " INNER JOIN YEAR_CLASS yc on yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $AcademicYearID . "'";
                if ($classInfo[0]['YearClassID']) {
                    $studentCond .= " AND yc.YearClassID='" . $classInfo[0]['YearClassID'] . "'";
                } else {
                    $lookupStudent = false;
                }
            } else if ($scope == 1) { // same class level
                $join_sql .= " INNER JOIN YEAR_CLASS_USER ycu on ycu.UserID=u.UserID ";
                $join_sql .= " INNER JOIN YEAR_CLASS yc on yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $AcademicYearID . "'";
                if ($classInfo[0]['YearID']) {
                    $studentCond .= " AND yc.YearID='" . $classInfo[0]['YearID'] . "'";
                } else {
                    $lookupStudent = false;
                }
            }
            $sql = "SELECT	
								u.UserID as user_id,
								u.EnglishName AS OrderName,
								{$userNameField}
					FROM 	
								INTRANET_USER u
					WHERE 
								u.RecordStatus='1'
					AND
								u.RecordType='1'
						{$cond} ";

            if ($lookupStudent) {
                $sql .= " UNION 
					SELECT	
								u.UserID as user_id,
								u.EnglishName AS OrderName,
								{$userNameField}
					FROM 	
								INTRANET_USER u
						{$join_sql}
					WHERE 
								u.RecordStatus='1'
					AND
								u.RecordType='2'
						{$cond}
						{$studentCond} ";
            }

            $sql .= " ORDER BY OrderName";
            $users = $this->returnResultSet($sql);
        }

        if (count($users)) {
            foreach ($users as &$user) {
                list ($user['name'], $user['class'], $user['image']) = $this->getUserInfo($user['user_id'], $classNameWithBracket);
            }
        }

        return $users;
    }

    public function getTeacher($teacherType = "-1", $conds = "")
    {
        if ($teacherType == 1)
            $conds .= " AND Teaching=1";
        else if ($teacherType == 0)
            $conds .= " AND (Teaching IS NULL OR Teaching=0 OR Teaching='')";

        $list = array();
        $name_field = getNameFieldByLang();
        $sql = "SELECT UserID, $name_field as Name FROM INTRANET_USER WHERE RecordType=1 AND RecordStatus='1' $conds ORDER BY EnglishName";
        if ($this->convert_utf8) { // ej
            $this->dbConnectionLatin1();
            $rs = $this->returnArray($sql);

            foreach ((array) $rs as $k => $v) {
                $staffName = convert2unicode($v['Name'], true);
                $rs[$k]['Name'] = $staffName;
                $rs[$k][1] = $staffName;
            }

            $this->dbConnectionUTF8();
        } else {
            $rs = $this->returnArray($sql);
        }
        return $rs;
    }

    /*
     * check if pass in friend is really my friend
     */
    public function isMyFriend($friendID)
    {
        $userID = $this->user_id;

        $sql = "SELECT FriendID FROM INTRANET_ELIB_FRIEND_FRIENDS WHERE UserID='" . $userID . "' AND FriendID='" . $friendID . "'";
        $sql .= " UNION ";
        $sql .= "SELECT UserID as FriendID FROM INTRANET_ELIB_FRIEND_FRIENDS WHERE FriendID='" . $userID . "' AND UserID='" . $friendID . "'";
        $rs = $this->returnVector($sql);

        return (count($rs) > 0) ? true : false;
    }

    // # end of my friend functions
    // ##################################

    // Charles 2013-09-30 END
}
?>