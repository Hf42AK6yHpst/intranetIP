<?php 

## Editing by  
/*
#############
#	Date : 2017-03-16 Villa
#		- modified getFundingSourceTableData() - add field numOfusingFunding
#	Date : 2017-03-15 Villa
#		- modified getFundingSourceTableData() - add fitlering Related
#	Date : 2017-03-03 Villa
#		- add getFundingSourceTableData() - return FundingSourceTableData
#		- add updateFundingCategory() - update Funding Category Record supporting Insert/ Edit
#		- add updateFunding() - update Funding Record supporting Insert/ Edit
#		- add updateFundingSoureRelation() - update funding Category Relation supporting insert record
#		- add deleteFunding() - delete Funding Record by selected Funding
#		- add deleteFundingCategory - delete Funding Category Record by selected category
#		- add deleteFundingCategoryRelation - delete all the Funding Category related to selected FundingID or CategoryID
#	Date : 2016-11-23 Omas
#		- modified getDataFromCSV(), getHeadingFromCSV() for EJ.
#	Date : 2016-10-06 Villa
#		- Add isRuleValidForOtherApplication() -> Check is the Rule Type 'Other' is exist in the db
#	Date : 2016-09-19 Omas
#		- Modified saveSupplierType()
#
############
 */


include_once($intranet_root."/includes/ePCM/libPCM_db.php");
class libPCM_db_setting extends libPCM_db {

	public function __construct(){
		parent::__construct();
	}

	# Group Financial Item
	public function insertGroupFinancialItem($saveToDbAry){
		$tableName = "INTRANET_PCM_GROUP_FINANCIAL_ITEM";
		$fieldNameArr = array("Code", "GroupID", "ItemName", "ItemNameChi");
		
		if($saveToDbAry['itemName']==''){
			$saveToDbAry['itemName']=$saveToDbAry['itemNameChi'];
		}
		if($saveToDbAry['itemNameChi']==''){
			$saveToDbAry['itemNameChi']=$saveToDbAry['itemName'];
		}
		
		$multiRowValueArr = array();
		$tempArr[0] = $saveToDbAry['code'];
		$tempArr[1] = $saveToDbAry['groupID'];
		$tempArr[2] = $saveToDbAry['itemName'];
		$tempArr[3] = $saveToDbAry['itemNameChi'];
		$multiRowValueArr[] = $tempArr;
		
		$result = $this->insertData($tableName, $fieldNameArr, $multiRowValueArr);
		
		return $result;
	}
	
	public function updateGroupFinancialItem($saveToDbAry){
		$tableName = "INTRANET_PCM_GROUP_FINANCIAL_ITEM";
		$fieldNameArr = array("Code", "ItemName", "ItemNameChi");
		
		if($saveToDbAry['itemName']==''){
			$saveToDbAry['itemName']=$saveToDbAry['itemNameChi'];
		}
		if($saveToDbAry['itemNameChi']==''){
			$saveToDbAry['itemNameChi']=$saveToDbAry['itemName'];
		}

		$multiRowValueArr = array();
		$tempArr[0] = $saveToDbAry['code'];
		$tempArr[1] = $saveToDbAry['itemName'];
		$tempArr[2] = $saveToDbAry['itemNameChi'];
		$multiRowValueArr[] = $tempArr;
		
		$targetID = $saveToDbAry['targetID'];
		$conditions = " ItemID = '$targetID' ";
		$result = $this->updateData($tableName, $fieldNameArr, $multiRowValueArr, $conditions);
		
		return $result;
	}

	public function deleteFinancialGroupItem($saveToDbAry){
		
		$tableName = "INTRANET_PCM_GROUP_FINANCIAL_ITEM";
		$primaryKey = "ItemID";
		$itemIDArr = $saveToDbAry['TargetArr'];
		
		$result = $this->deleteData($tableName, $primaryKey, $itemIDArr);
		return $result;
	}
	
	//Subject Group
	public function saveGroup($groupID='',$code,$groupName,$groupNameChi=''){
		$ParUserID = $_SESSION["UserID"];
		
		if($groupName==''){
			$groupName=$groupNameChi;
		}
		if($groupNameChi==''){
			$groupNameChi=$groupName;
		}
		
		if($groupID==''){
			//Insert New
			$table_name = 'INTRANET_PCM_GROUP';
			$array_field_name = array('Code','GroupName','GroupNameChi');
			$rows=array(
				array($code,$groupName,$groupNameChi),
			);
			
			$success = $this->insertData($table_name, $array_field_name, $rows);
		}else{
			//Update
			$table_name = 'INTRANET_PCM_GROUP';
			$array_field_name = array('Code','GroupName','GroupNameChi');
			$rows=array(
				array($code,$groupName,$groupNameChi),
			);
			$conditions = ' `GroupID` = \''.$groupID.'\'';
			
			$success = $this->updateData($table_name, $array_field_name, $rows,$conditions);
		}
		
		$newGroupID = ($success)? $this->db_insert_id():'';
		return $newGroupID;
	}
	public function deleteGroup($groupIDAry){
		$table_name = 'INTRANET_PCM_GROUP';
		$primary_key = 'GroupID';
		$array_id = (array)$groupIDAry;
		$this->deleteData($table_name, $primary_key, $array_id);
	}

	// Category
	public function saveCategory($catID='',$code,$catName,$catNameChi=''){
		$ParUserID = $_SESSION["UserID"];
	
		if($catName==''){
			$catName=$catNameChi;
		}
		if($catNameChi==''){
			$catNameChi=$catName;
		}
		
		if($catID==''){
			//Insert New
			$table_name = 'INTRANET_PCM_CATEGORY';
			$array_field_name = array('Code','CategoryName','CategoryNameChi');
			$rows=array(
				array($code,$catName,$catNameChi),
			);
			
			$this->insertData($table_name, $array_field_name, $rows);
		}else{
			//Update
			$table_name = 'INTRANET_PCM_CATEGORY';
			$array_field_name = array('Code','CategoryName','CategoryNameChi');
			$rows=array(
				array($code,$catName,$catNameChi),
			);
			$conditions = ' `CategoryID` = \''.$catID.'\'';
			
			$this->updateData($table_name, $array_field_name, $rows,$conditions);
		}
	}
	public function deleteCategory($catIDAry){		
		$table_name = 'INTRANET_PCM_CATEGORY';
		$primary_key = 'CategoryID';
		$array_id = (array)$catIDAry;
		
		$this->deleteData($table_name, $primary_key, $array_id);
	}
	
	#### ROLE
	public function saveRole($roleID='',$roleName,$isPrincipal,$forCaseApproval,$forQuotationApproval,$forTenderAuditing,$forTenderApproval,$roleNameChi=''){
		$ParUserID = $_SESSION["UserID"];
		
		if($roleName==''){
			$roleName=$roleNameChi;
		}
		if($roleNameChi==''){
			$roleNameChi=$roleName;
		}
		
		if($roleID==''){
			//Insert New
			$table_name = 'INTRANET_PCM_ROLE';
			$array_field_name = array('RoleName','RoleNameChi','IsPrincipal','ForCaseApproval','ForQuotationApproval','ForTenderAuditing','ForTenderApproval');
			$rows=array(
				array($roleName,$roleNameChi,$isPrincipal,$forCaseApproval,$forQuotationApproval,$forTenderAuditing,$forTenderApproval),
			);
			
			$this->insertData($table_name, $array_field_name, $rows);
		}else{
			//Update
			$table_name = 'INTRANET_PCM_ROLE';
			$array_field_name = array('RoleName','RoleNameChi','IsPrincipal','ForCaseApproval','ForQuotationApproval','ForTenderAuditing','ForTenderApproval');
			$rows=array(
				array($roleName,$roleNameChi,$isPrincipal,$forCaseApproval,$forQuotationApproval,$forTenderAuditing,$forTenderApproval),
			);
			$conditions = ' `RoleID` = \''.$roleID.'\'';
			
			$this->updateData($table_name, $array_field_name, $rows,$conditions);
		}
	}
	public function deleteRole($roleIDAry){
		$table_name = 'INTRANET_PCM_ROLE';
		$primary_key = 'RoleID';
		$array_id = (array)$roleIDAry;
		
		$this->deleteData($table_name, $primary_key, $array_id);
	}

	public function copyRoleMemberFromYear($fromYear,$toYear){
        $sql='INSERT INTO INTRANET_PCM_ROLE_USER (UserID,RoleID,AcademicYearID,IsActive,DateInput,InputBy,IsDeleted,DeletedBy)
              SELECT UserID,RoleID,"'.$toYear.'",IsActive,DateInput,InputBy,IsDeleted,DeletedBy 
              FROM INTRANET_PCM_ROLE_USER
              WHERE AcademicYearID="'.$fromYear.'"';
        $this->db_db_query($sql);
    }

	#### supplier
	
	public function saveSupplier($value_array, $contact_array) {
		$supplier_id = $value_array['SupplierID'];
		$SupplierID = $supplier_id;
		//$Code = $value_array['Code'];
		$SupplierName = $value_array['SupplierName'];
		$SupplierNameChi = $value_array['SupplierNameChi'];
		$Website = $value_array['Website'];
		$IsActive = $value_array['IsActive'];
		$Description = $value_array['Description'];
		$Phone = $value_array['Phone'];
		$Fax = $value_array['Fax'];
		$Email = $value_array['Email'];
		$Type = $value_array['Type'];
		
		if($SupplierName==''){
			$SupplierName=$SupplierNameChi;
		}
		if($SupplierNameChi==''){
			$SupplierNameChi=$SupplierName;
		}
		
		if ($supplier_id == '') {
			//Insert New
			$table_name = 'INTRANET_PCM_SUPPLIER';
			$array_field_name = array('SupplierName','SupplierNameChi','Website','IsActive','Description','Phone','Fax','Email');
			$rows=array(
				array($SupplierName,$SupplierNameChi,$Website,$IsActive,$Description,$Phone,$Fax,$Email),
			);
			
			$this->insertData($table_name, $array_field_name, $rows);

			$supplier_id = $this->db_insert_id();

			$sql = 'INSERT INTO `INTRANET_PCM_SUPPLIER_TYPE_MAPPING`(`TypeID`,`SupplierID`,`DateInput`,`InputBy`) ' .
			'VALUES(\''.$this->Get_Safe_Sql_Query($Type).'\',\''.$this->Get_Safe_Sql_Query($supplier_id).'\',NOW(),\''.$this->Get_Safe_Sql_Query($UserID).'\');';
			$this->db_db_query($sql);

			if (count($contact_array) > 0) {
				foreach ($contact_array as $contact) {
					$table_name = 'INTRANET_PCM_SUPPLIER_CONTACT';
					$array_field_name = array('SupplierID','ContactName','Position','Phone','Email','Description');
					$rows=array(
						array($supplier_id,$contact['cContactName'],$contact['cPosition'],$contact['cPhone'],$contact['cEmail'],$contact['cDescription']),
					);
					
					$this->insertData($table_name, $array_field_name, $rows);
				}
			}

		} else {
			//Update
			$table_name = 'INTRANET_PCM_SUPPLIER';
			$array_field_name = array('SupplierID','SupplierName','SupplierNameChi','Website','IsActive','Description','Phone','Fax','Email');
			$rows=array(
				array($SupplierID,$SupplierName,$SupplierNameChi,$Website,$IsActive,$Description,$Phone,$Fax,$Email),
			);
			$conditions = ' `SupplierID` = \''.$this->Get_Safe_Sql_Query($SupplierID).'\'';
			
			$this->updateData($table_name, $array_field_name, $rows,$conditions);

			$sql = 'UPDATE `INTRANET_PCM_SUPPLIER_TYPE_MAPPING` SET `TypeID`=\'' . $this->Get_Safe_Sql_Query($value_array['Type']) . '\' WHERE `SupplierID`=\'' . $this->Get_Safe_Sql_Query($SupplierID) . '\'';
			$this->db_db_query($sql);


			$sql = 'UPDATE `INTRANET_PCM_SUPPLIER_CONTACT` SET `IsDeleted`=\'1\' WHERE `SupplierID`=\'' . $this->Get_Safe_Sql_Query($SupplierID) . '\';';
			$this->db_db_query($sql);
			foreach ((array)$contact_array as $contact) {
				if($contact['cContactID']!=''){
					$sql = 'UPDATE `INTRANET_PCM_SUPPLIER_CONTACT` SET `ContactName`=\'' . $this->Get_Safe_Sql_Query($contact['cContactName']) . '\',`Position`=\'' . $this->Get_Safe_Sql_Query($contact['cPosition']) . '\',`Phone`=\'' . $this->Get_Safe_Sql_Query($contact['cPhone']) . '\',`Email`=\'' . $this->Get_Safe_Sql_Query($contact['cEmail']) . '\',`Description`=\'' . $this->Get_Safe_Sql_Query($contact['cDescription']) . '\',`IsDeleted`=\'0\' WHERE `ContactID`=\'' . $this->Get_Safe_Sql_Query($contact['cContactID']) . '\';';
					$this->db_db_query($sql);
				}else{
					$table_name = 'INTRANET_PCM_SUPPLIER_CONTACT';
					$array_field_name = array('SupplierID','ContactName','Position','Phone','Email','Description');
					$rows=array(
						array($supplier_id,$contact['cContactName'],$contact['cPosition'],$contact['cPhone'],$contact['cEmail'],$contact['cDescription']),
					);
					
					$this->insertData($table_name, $array_field_name, $rows);
				}
			}
		}
	}

	public function deleteSupplier($supplierIDAry) {
		$table_name = 'INTRANET_PCM_SUPPLIER';
		$primary_key = 'SupplierID';
		$array_id = (array)$supplierIDAry;
		
		$this->deleteData($table_name, $primary_key, $array_id);
	}
	
	public function setActiveSupplier($supplierIDAry,$active=true){
		$table_name = 'INTRANET_PCM_SUPPLIER';
		$array_field_name=array('IsActive');
		$rows=array(
			array($active?1:0),
		);
		$sql_supplier_id = implode("','", $supplierIDAry);
		$conditions=' `SupplierID` in (\''.$sql_supplier_id.'\')';
		
		$this->updateData($table_name, $array_field_name, $rows,$conditions);
	}
	
	public function activateSupplier($supplierIDAry){
		$this->setActiveSupplier($supplierIDAry,true);
	}
	
	public function inactivateSupplier($supplierIDAry){
		$this->setActiveSupplier($supplierIDAry,false);
	}
	
	public function saveSupplierType($record_array){
		
		if($record_array['TypeName']==''){
			$record_array['TypeName']=$record_array['TypeNameChi'];
		}
		if($record_array['TypeNameChi']==''){
			$record_array['TypeNameChi']=$record_array['TypeName'];
		}
		if($record_array['GroupMappingID'] == '' ){
			$record_array['GroupMappingID'] = 0;
		}
		if($record_array['TypeID']==''){
			//Insert New
			$table_name = 'INTRANET_PCM_SUPPLY_TYPE';
			$array_field_name = array('TypeName','TypeNameChi', 'GroupMappingID');
			$rows=array(
				array($record_array['TypeName'],$record_array['TypeNameChi'], $record_array['GroupMappingID']),
			);
			
			$this->insertData($table_name, $array_field_name, $rows);
		}else{
			//Update
			$table_name = 'INTRANET_PCM_SUPPLY_TYPE';
			$array_field_name = array('TypeID','TypeName','TypeNameChi');
			$rows=array(
				array($record_array['TypeID'],$record_array['TypeName'],$record_array['TypeNameChi']),
			);
			$conditions = ' `TypeID` = \''.$record_array['TypeID'].'\'';
			
			$this->updateData($table_name, $array_field_name, $rows,$conditions);
		}
	}
	public function insertSupplierType($multiRowValueArr){
		$fieldNameArr = array('TypeName','TypeNameChi');
		$this->insertData($tableName='INTRANET_PCM_SUPPLY_TYPE',$fieldNameArr,$multiRowValueArr,$excludeCommonField=false);
	}
	public function insertSupplier(){
		
	}

	public function deleteSupplierType($supplierTypeIDAry) {
		$table_name = 'INTRANET_PCM_SUPPLY_TYPE';
		$primary_key = 'TypeID';
		$array_id = (array)$supplierTypeIDAry;
		
		$this->deleteData($table_name, $primary_key, $array_id);
	}

	#### Group > Budget

	public function saveGroupBudget($rows_budget) {

		foreach ((array) $rows_budget as $row_budget) {
			if(isset($row_budget['Budget'])){
				$row_budget['Budget']=str_replace(',','',$row_budget['Budget']);
			}
			if ($row_budget['GroupBudgetID'] == '') {
				unset ($row_budget['GroupBudgetID']);
				$this->insertData('INTRANET_PCM_GROUP_BUDGET', array_keys($row_budget), array (
					array_values($row_budget)
				));
			} else {
				$this->updateData('INTRANET_PCM_GROUP_BUDGET', array_keys($row_budget), array (
					array_values($row_budget)
				), '`GroupBudgetID`=\'' . $row_budget['GroupBudgetID'] . '\';');
			}
		}
	}

	public function deleteGroupBudget($array_id) {
		$this->deleteData('INTRANET_PCM_GROUP_BUDGET', 'GroupBudgetID', $array_id);
	}

	#### Group > Financial Item > Budget

	public function saveGroupFinancialItemBudget($rows_budget) {

		foreach ((array) $rows_budget as $row_budget) {
			if(isset($row_budget['Budget'])){
				$row_budget['Budget']=str_replace(',','',$row_budget['Budget']);
			}
			if ($row_budget['ItemBudgetID'] == '') {
				unset ($row_budget['ItemBudgetID']);
				$this->insertData('INTRANET_PCM_FINANCIAL_ITEM_BUDGET', array_keys($row_budget), array (
					array_values($row_budget)
				));
			} else {
				$this->updateData('INTRANET_PCM_FINANCIAL_ITEM_BUDGET', array_keys($row_budget), array (
					array_values($row_budget)
				), '`ItemBudgetID`=\'' . $row_budget['ItemBudgetID'] . '\';');
			}
		}
	}
	
	public function deleteGroupFinancialItemBudget($array_id) {
		$this->deleteData('INTRANET_PCM_FINANCIAL_ITEM_BUDGET', 'ItemBudgetID', $array_id);
	}
	public function getInvalidAryOfRule($ruleInfo){
		//Assume asc sorted
		$invalidAry = array(false,false,false,false);
		//Rule 1: Start from zero
		if($ruleInfo[0]['FloorPrice']!=='0'){
			$invalidAry[0] = true;
		}
		
		//Rule 2: End from 'Blanks'
		$countOfRule = count($ruleInfo);
		if($ruleInfo[$countOfRule-1]['CeillingPrice']!==NULL){
			$invalidAry[1] = true;
		}
		
		
		
		//Rule 3: Any price must only belongs to one rule
		//$ruleInfo[n]['CeillingPrice']+0.01==$ruleInfo[n+1]['FloorPrice']
		for($i = 0; $i<$countOfRule;$i++){
			
			if($i!=$countOfRule-1){	//except for last rule
				
				//Rule 4: No Unlimited celling for non-last rule
				if($ruleInfo[$i]['CeillingPrice']==NULL){
					$invalidAry[3]['RuleID'][]=$ruleInfo[$i]['RuleID'];
				}else {
					if(($ruleInfo[$i]['CeillingPrice']+1)!=$ruleInfo[$i+1]['FloorPrice']){
						$invalidAry[2]['RuleID'][] = $ruleInfo[$i]['RuleID'];
					}
					if(($ruleInfo[$i]['CeillingPrice']+1)>$ruleInfo[$i+1]['FloorPrice']){
						$invalidAry[2]['Overlapped'][] = array('start'=>$ruleInfo[$i+1]['FloorPrice'],'end'=>$ruleInfo[$i]['CeillingPrice']);
					}
					if(($ruleInfo[$i]['CeillingPrice']+1)<$ruleInfo[$i+1]['FloorPrice']){
						$invalidAry[2]['Missed'][] = array('start'=>$ruleInfo[$i]['CeillingPrice'],'end'=>$ruleInfo[$i+1]['FloorPrice']);
					}
				}
			}
		}
		
		
		return $invalidAry;
	}
	public function isRuleValid(){
		$ruleInfo = $this->getRuleTemplateInfo();
		$valid = true;
		if($this->isRuleEmpty($ruleInfo)){
			$valid = false;
		}
		$invalidAry = $this->getInvalidAryOfRule($ruleInfo);
		
		foreach($invalidAry as $_isInvalid){
			if($_isInvalid){
				$valid = false;
			}
		}
		return $valid;
	}
	
	public function isRuleValidForOtherApplication(){
		$ruleInfo = $this->getRuleOInfo();
		$valid = true;
		if($this->isRuleEmpty($ruleInfo)){
			$valid = false;
		}
		return $valid;
	}
	public function isRuleEmpty($ruleInfo){
		if(empty($ruleInfo)){
			return true;
		}else{
			return false;
		}
	}

	public function getPrincipalUserIDAry(){
		
		$ruleInfoAry = $this->getRoleInfo($roleIDAry='', $ForCaseApproval='',$ForQuotationApproval='',$ForTenderAuditing='',$ForTenderApproval='',$isDeleted='0',$isPrincipal='1');
		
		$principalRuleID = $ruleInfoAry[0]['RoleID'];
		$principalRoleUserAry = $this->getRoleUser($principalRuleID,$academicYearID='');
		//$principalUserIDAry = Get_Array_By_Key($principalRoleUserAry, "UserID");
		return $principalRoleUserAry;
	}
	public function importInvGroup($eInvGroupAry,$needAddMember,$needSetGroupAdmin){
		foreach($eInvGroupAry as &$_group){
			$_group['ePCMgroupID'] = $this->saveGroup('',$_group['Code'],$_group['NameEng'],$_group['NameChi']);
				
			if($needAddMember){
				$_userIDAry = Get_Array_By_Key($_group['UserAry'],'UserID');
				$this->insertGroupMember($_userIDAry,$_group['ePCMgroupID']);
				
				if($needSetGroupAdmin){
					$_groupHeadUserIDAry = BuildMultiKeyAssoc($_group['UserAry'],array('RecordType'),array(),0,1);
					$_groupHeadUserIDAry = Get_Array_By_Key($_groupHeadUserIDAry[1],'UserID');
					
					
					$array_field_name = array('isGroupHead');
					$rows=array(
							array(1),
					);
					foreach($_groupHeadUserIDAry as $_groupHeadUser){
						$conditions = " GroupID = '".$_group['ePCMgroupID']."' AND  UserID ='$_groupHeadUser'";
						$this->updateData('INTRANET_PCM_GROUP_MEMBER', $array_field_name, $rows,$conditions,true);
					}
				}
			}
		}
	}
	
	public function importCsvFileExtChecking($userfile){
		global $intranet_root;
		include_once($intranet_root."/includes/libimporttext.php");
		$limport = new libimporttext();
		return $limport->CHECK_FILE_EXT($userfile['name']);
	}
	public function getHeadingFromCSV($userfile){
		global $intranet_root, $_PAGE;
		include_once($intranet_root."/includes/libimporttext.php");
		$limport = new libimporttext();
		if($_PAGE['libPCM']->isEJ()){
			$data = $limport->GET_IMPORT_TXT_UNICODE($userfile['tmp_name']);
		}else{
			$data = $limport->GET_IMPORT_TXT($userfile['tmp_name']);
		}
		return array_shift($data);
	}
	public function importCsvHeadingChecking($heading_user_entered,$heading_checking_array){
		global $intranet_root;
		//heading checking
		$passHeadingCheck = true;
		//number of heading checking 
		if(count($heading_user_entered)!=count($heading_checking_array)){
			$passHeadingCheck = false;
		}
		foreach($heading_user_entered as $_key => $_header){
			if(trim($_header)!=$heading_checking_array[$_key]){
				$passHeadingCheck = false;
			}
		}
		return $passHeadingCheck;
	}
	public function getDataFromCSV($userfile,$excludeHeading=1){
		global $intranet_root,$UserID, $_PAGE;
		include_once($intranet_root."/includes/libimporttext.php");
		$limport = new libimporttext();
		if($_PAGE['libPCM']->isEJ()){
			$data = $limport->GET_IMPORT_TXT_UNICODE($userfile['tmp_name']);
		}else{
			$data = $limport->GET_IMPORT_TXT($userfile['tmp_name']);
		}
		if($excludeHeading){
			for($i=0;$i<$excludeHeading;$i++ ){
				array_shift($data);
			}
		}
		return $data;
	}
	public function importSupplier($data){
		global $intranet_root,$UserID;
		include_once($intranet_root."/includes/libimporttext.php");
		$limport = new libimporttext();
		$error = array();
		
		//Get all valid type id
		$supplyTypeInfo = $this->getSupplyTypeInfo($isDeleted=0,$typeID='');
		$supplyTypeIDAry = Get_Array_By_Key($supplyTypeInfo,'TypeID');
		
		//Clear Temp table record
		$this->permanentDeleteDate('TEMP_INTRANET_PCM_SUPPLIER', 'InputBy', $UserID);
		$fieldNameArr = array('SupplierName','SupplierNameChi','Website','Description','Phone','Fax','Email','TypeID','InputBy');
		$multiRowValueArr = array();
		foreach($data as $_row => $_data){
			$multiRowValueArr[] = array($_data[1],$_data[0],$_data[2],$_data[3],$_data[4],$_data[5],$_data[6],$_data[7],$UserID);
			if(trim($_data[0])==''){
				$error[] = array('EMPTY_SUPPLIER_NAME_CHI',$_row);
			}
			if(trim($_data[1])==''){
				$error[] = array('EMPTY_SUPPLIER_NAME_ENG',$_row);
			}
			if(trim($_data[4])==''){
				$error[] = array('EMPTY_PHONE',$_row);
			}
			if(trim($_data[7])==''){
				$error[] = array('EMPTY_TYPE',$_row);
			}
			//Check typeID is valid
			if(!(in_array(intval(trim($_data[7])),$supplyTypeIDAry))){
				//not exist in typeID
				$error[] = array('INVALID_TYPE',$_row);
			}
		}
		$this->insertData('TEMP_INTRANET_PCM_SUPPLIER', $fieldNameArr, $multiRowValueArr, $excludeCommonField=true);
		//Checking
		//Template only
		if(false){
			$error[] = 'UNKNOWN';
		}
		return $error;
	}
	public function importSupplierType($data){
		global $intranet_root,$UserID;
		include_once($intranet_root."/includes/libimporttext.php");
		
		$error = array();
		
		//Clear Temp table record
		$this->permanentDeleteDate('TEMP_INTRANET_PCM_SUPPLY_TYPE', 'InputBy', $UserID);
		
		$limport = new libimporttext();
		$fieldNameArr = array('TypeName','TypeNameChi','InputBy');
		$multiRowValueArr = array();
		
		foreach($data as $_row => $_data){
			$multiRowValueArr[] = array($_data[1],$_data[0],$UserID);
			if(trim($_data[0])==''){
				$error[] = array('EMPTY_TYPE_NAME_CHI',$_row);
			}
			if(trim($_data[1])==''){
				$error[] = array('EMPTY_TYPE_NAME_ENG',$_row);
			}
		}
		$this->insertData('TEMP_INTRANET_PCM_SUPPLY_TYPE', $fieldNameArr, $multiRowValueArr, $excludeCommonField=true);
		
		//Checking
		//Template only
		if(false){
			$error[] = 'UNKNOWN';
		}
		return $error;
	}
	public function getFundingSourceTableData($RecordStatus=''){
		$FundingRawData = $this->getFundingRawData('',$RecordStatus);
		$FundingCaseRawData = $this->getFundingCaseRelationRawData();
		$FundingCategoryRawData = $this->getFundingCategoryRawData();
		$FundingCategoryMappingRawData = $this->getFundingCategoryRelationRawData();
		if($FundingCategoryRawData){
			foreach ($FundingCategoryRawData as $_CategoryRaw ){
				//Category Data
				$Data[$_CategoryRaw['FundingCategoryID']]['CategoryID'] = $_CategoryRaw['FundingCategoryID'];
				$Data[$_CategoryRaw['FundingCategoryID']]['FundingCategoryNameEN'] = $_CategoryRaw['FundingCategoryNameEN'];
				$Data[$_CategoryRaw['FundingCategoryID']]['FundingCategoryNameCHI'] = $_CategoryRaw['FundingCategoryNameCHI'];
				foreach ((array)$FundingCategoryMappingRawData[$_CategoryRaw['FundingCategoryID']] as $_FundongID){
					//Funding Data
					if($_FundongID&&$FundingRawData[$_FundongID]){
						$Data[$_CategoryRaw['FundingCategoryID']]['Funding'][$_FundongID] = $FundingRawData[$_FundongID];
						//initalize Budget Remain
						$BudgetRemaim = $Data[$_CategoryRaw['FundingCategoryID']]['Funding'][$_FundongID]['Budget']; 
						$NumOfUsingFunding = 0;
						if($FundingCaseRawData[$_FundongID]){
							foreach ((array)$FundingCaseRawData[$_FundongID] as $_CaseID){
								$Data[$_CategoryRaw['FundingCategoryID']]['Funding'][$_FundongID]['Case'][$_CaseID['CaseID']] ['CaseID'] = $_CaseID['CaseID'];
								$Data[$_CategoryRaw['FundingCategoryID']]['Funding'][$_FundongID]['Case'][$_CaseID['CaseID']] ['RecordStatus'] = $_CaseID['RecordStatus'];
								$Data[$_CategoryRaw['FundingCategoryID']]['Funding'][$_FundongID]['Case'][$_CaseID['CaseID']] ['Amount'] =  $_CaseID['Amount'];
								if( $_CaseID['RecordStatus'] == '0' || $_CaseID['RecordStatus'] == '1'){ //Case Approve or Pending Only
									$BudgetRemaim = $BudgetRemaim - $_CaseID['Amount'];
									$NumOfUsingFunding++;
								}
							}
						}
						$Data[$_CategoryRaw['FundingCategoryID']]['Funding'][$_FundongID]['BudgetRemain'] = number_format($BudgetRemaim,2,'.','');
						$Data[$_CategoryRaw['FundingCategoryID']]['Funding'][$_FundongID]['NumOfUsingFunding'] = $NumOfUsingFunding;
					}
				}
			}
		}
		//Data -> CategoryID -> CategoryData/ Funding -> FundingID -> FundingData/ Case -> CaseID -> CaseBudget 
		return $Data;
	}
	public function updateFundingCategory($CategoryID='',$CategoryNameEN,$CategoryNameCHI){
		$tablename = "INTRANET_PCM_FUNDING_CATEGORY";
		if($CategoryID){ // update fundingCategory
			$fieldName = array('FundingCategoryNameEN','FundingCategoryNameCHI');
			$multiRowValueArr = array();
			$tempArr[0] = $CategoryNameEN;
			$tempArr[1] = $CategoryNameCHI;
			$multiRowValueArr[] = $tempArr;
			$cond = " FundingCategoryID = $CategoryID ";
			$this->updateData($tablename,$fieldName,$multiRowValueArr,$cond);
		}else{ // add new fundingCategory
			$DisplayOrder = $this->getFundingCategoryLastestDisplayOrder();
			$DisplayOrder = $DisplayOrder +1;
			$fieldName = array('FundingCategoryNameEN','FundingCategoryNameCHI','DisplayOrder');
			$multiRowValueArr = array();
			$tempArr[0] = $CategoryNameEN;
			$tempArr[1] = $CategoryNameCHI;
			$tempArr[2] = $DisplayOrder;
			$multiRowValueArr[] = $tempArr;
			$this->insertData($tablename,$fieldName,$multiRowValueArr);
		}
	}
	public function updateFunding($FundingID,$FundingNameCHI,$FundingNameEN,$Budget,$RecordStatus,$DeadLineDate,$DisplayOrder,$CategoryID){
		
		$tablename = "INTRANET_PCM_FUNDING_SOURCE";
		if($FundingID){//update the funding record
			$fieldName = array('FundingNameCHI','FundingNameEN','Budget','DeadLineDate','RecordStatus','DisplayOrder');
			$multiRowValueArr = array();
			$tempArr[0] = $FundingNameCHI;
			$tempArr[1] = $FundingNameEN;
			$tempArr[2] = $Budget;
			$tempArr[3] = $DeadLineDate;
			$tempArr[4] = $RecordStatus;
			$tempArr[5] = $DisplayOrder;
			$multiRowValueArr[] = $tempArr;
			$cond = " FundingID = $FundingID ";
			$this->updateData($tablename,$fieldName,$multiRowValueArr,$cond);
			
		}else{ //add new funding record
			$DisplayOrder = $this->getFundingLastestDisplayOrder();
			$DisplayOrder = $DisplayOrder +1;
			$fieldName = array('FundingID ','FundingNameCHI','FundingNameEN','Budget','DeadLineDate','RecordStatus','DisplayOrder', 'FundingCategoryID');
			$multiRowValueArr = array();
			$tempArr[0] = $FundingID;
			$tempArr[1] = $FundingNameCHI;
			$tempArr[2] = $FundingNameEN;
			$tempArr[3] = $Budget;
			$tempArr[4] = $DeadLineDate;
			$tempArr[5] = $RecordStatus;
			$tempArr[6] = $DisplayOrder;
			$tempArr[7] = $CategoryID;
			$multiRowValueArr[] = $tempArr;
			$this->insertData($tablename,$fieldName,$multiRowValueArr);
		}
		//return lastest ID 
		return $this->getFundingLastestFundingID();
	}
	
	public function deleteFunding($FundingID){
		//delete Funding Record
		$tablename = "INTRANET_PCM_FUNDING_SOURCE";
		$keyName = "FundingID";
		$keyValArray = "$FundingID";
		return $this->deleteData($tablename, $keyName, $keyValArray);
	}
	public function deleteFundingCategory($CategoryID){
		//delete Funding Category
		$tablename = "INTRANET_PCM_FUNDING_CATEGORY";
		$keyName = "FundingCategoryID";
		$keyValArray = "$CategoryID";
		return $this->deleteData($tablename, $keyName, $keyValArray);
	}
}
?>