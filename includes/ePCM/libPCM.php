<?php
// using by :
/*
 * Date: 2017-04-05 Villa
 * - Add enableFundingSource
 * Date: 2017-03-01 Villa
 * - #X100660 add isSMC() 
 * - modified Get_Left_Menu_Structure_Array() show if SMC
 * Date: 2016-11-09	Villa
 * - modified Get_Left_Menu_Structure_Array()
 * - modified GET_MODULE_OBJ_ARR() -routing to index.php- E108171
 * Date: 2016-09-28 Omas
 * - added isEJ()
 * Date: 2016-09-19	OMas
 * - add enableGroupSupplierType() - for $sys_custom['ePCM']['enableGroupSupplierType']
 * Date: 2016-08-26	OMas
 * -add GetRoleSequence(), canUseAppNotification()
 * 
 * Date: 2016-06-30	Kenneth
 * - add getPasswordGeneration()
 * - modified 
 *
 */
class libPCM {
	protected $ModuleDocumentRoot;
	protected $currentPageName;
	
	public function __construct(){
		global $plugin, $PATH_WRT_ROOT;
		if(!$plugin['ePCM']){
			
		}
		$this->ModuleDocumentRoot = $PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/eProcurement/index.php?p=';
		$this->currentPageName = '';
	}
	
	public function isEJ(){
		global $junior_mck;
		return isset($junior_mck);
	}
	
	########################################
	#	Start: EJ Only
	########################################
	public function GET_ADMIN_USER(){
		
		global $intranet_root, $junior_mck;
		if(isset($junior_mck)){
			include_once($intranet_root."/includes/libfilesystem.php");
			$lf = new libfilesystem();
			$AdminUser = trim($lf->file_read($intranet_root."/file/ePCM/admin_user.txt"));
			return $AdminUser;
		}else{
			// EJ function IP return 0
			return 0;
		}
	}
	########################################
	#	End: EJ Only
	########################################
	
	public function isAdmin(){
	    return $_SESSION['ePCM']['isAdmin'];
	}
	
	public function isSMC(){
		global $intranet_root, $ePCMcfg;
		include_once $intranet_root.'/includes/ePCM/libPCM_db.php';
		$db = new libPCM_db();
		$sql = "SELECT 
				Count(*) as Count 
				FROM ".$ePCMcfg['INTRANET_USER']." iu
				INNER JOIN INTRANET_PCM_ROLE_USER ipru ON iu.UserID = ipru.UserID and ipru.IsDeleted = '0'
				WHERE iu.RecordType = 5 AND iu.RecordStatus = 1 AND iu.UserID = '".$_SESSION['UserID']."' 
				ORDER BY EnglishName, ChineseName";
		$rs = $db->returnResultSet($sql);
		if($rs[0]['Count']==1){
			$isSMC = true;
		}else{
			$isSMC = false;
		}
		return $isSMC;
	}
	
	public function isGroupHead($parUserID){
	    global $intranet_root;
	    
	    if(isset($_SESSION['ePCM']['isGroupHead'])){
    	    return $_SESSION['ePCM']['isGroupHead'];
	    }
	    else{
	        include_once $intranet_root.'/includes/ePCM/libPCM_db_setting.php';
	        $db = new libPCM_db_setting();
	        $result = $db->getGroupHeadByUserID($parUserID);
	        if(count($result) > 0){
    	        $_SESSION['ePCM']['isGroupHead'] = 1;
	        }
	        else{
	            unset($_SESSION['ePCM']['isGroupHead']);
	        }
	        return $_SESSION['ePCM']['isGroupHead'];
	    }
	}
	
	public function GetRoleSequence(){
		return array('G','V','R');
	}
	
	public function GET_TAG_OBJ_ARR(){
		global $_PAGE, $Lang;
	
		$CurrentPage = $_PAGE['CurrentSection'].'_'.$_PAGE['CurrentPage'];
		$moduleRoot = $this->ModuleDocumentRoot;
		
		$TAGS_OBJ = array();
		switch($CurrentPage){
			// Mgmt
			
			// Report
			case 'Report_R1':
				$curTab = $_PAGE['curTab'];
				$TAGS_OBJ[] = array('Form', $moduleRoot.'report.r1.page1', $curTab == 'page1');
				$TAGS_OBJ[] = array('Page Sample 2', $moduleRoot.'report.r1.r2.3', $curTab == 'page2');
				$TAGS_OBJ[] = array('Page Sample 3', $moduleRoot.'report.r1.page3', $curTab == 'page3');
				$TAGS_OBJ[] = array('ThickBox', 'javascript:document.getElementById(\'dynSizeThickboxLink\').click();', $curTab == 'page4');
				break;
			// Setting
			case 'Settings_Security':
				$curTab = $_PAGE['curTab'];
				$TAGS_OBJ[] = array($Lang['ePCM']['SettingsArr']['SecurityArr']['AuthenticationArr']['TabTitle'], $moduleRoot.'setting.security.authentication', $curTab=='authentication');
				$TAGS_OBJ[] = array($Lang['ePCM']['SettingsArr']['SecurityArr']['NetworkArr']['TabTitle'],  $moduleRoot.'setting.security.network', $curTab=='network');
				break;
			case 'Settings_Supplier':
				$curTab = $_PAGE['curTab'];
				$TAGS_OBJ[] = array($Lang['ePCM']['SettingsArr']['SupplierArr']['MenuTitle'],  $moduleRoot.'setting.supplier', $curTab=='supplier');
				$TAGS_OBJ[] = array($Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TabTitle'], $moduleRoot.'setting.supplierType', $curTab=='supplierType');
				break;
			case 'Management_FinishedView':
				$curTab = $_PAGE['curTab'];
				$TAGS_OBJ[] = array($Lang['ePCM']['ManagementArr']['FinishedViewArr']['Tab']['Finished'], $moduleRoot.'mgmt.finished.completed', $curTab=='completed');
				$TAGS_OBJ[] = array($Lang['ePCM']['ManagementArr']['FinishedViewArr']['Tab']['Terminated'],  $moduleRoot.'mgmt.finished.terminated', $curTab=='terminated');
				
				break;
// 			case 'Management_View':
// 				$curTab = $_PAGE['curTab'];
// 				$TAGS_OBJ[] = array($Lang['ePCM']['ManagementArr']['ViewArr']['MenuTitle'], $moduleRoot.'mgmt.view', $curTab=='view');
// 				$TAGS_OBJ[] = array($Lang['ePCM']['ManagementArr']['FinishedViewArr']['Tab']['Finished'], $moduleRoot.'mgmt.finished.completed', $curTab=='completed');
// 				break;
			default:
				$TAGS_OBJ[] = array($this->currentPageName);
				break;
		}
		
		return $TAGS_OBJ;
	}
	
	function Get_Left_Menu_Structure_Array($CheckAccessRight=1)
	{
		global $Lang, $PATH_WRT_ROOT, $intranet_root;
		
		$MenuArr = array();
		include_once $intranet_root.'/includes/ePCM/libPCM_leftmenu.php';
		
		$IsAdmin = $this->isAdmin();
		if ($CheckAccessRight == 1 && $IsAdmin == false)
		{
// 			$PageAccessRightArr = $this->Get_User_Page_Access_Right($_SESSION['UserID'], $FromSession=1);
            $ePCM_USER_ACCESS = 1;
            $ePCM_GROUPHEAD_ACCESS = 2;
		    $PageAccessRightArr = array();
		    $PageAccessRightArr['Management'] = $ePCM_USER_ACCESS;
		    $PageAccessRightArr['Report'] = $this->isSMC();
// 		    $PageAccessRightArr['Settings']['Group'] = $ePCM_GROUPHEAD_ACCESS;
			
		    $AccessibleMenuArr = array();
			foreach((array)$MenuArr as $thisMenu => $thisSubMenuArr)
			{
				foreach((array)$thisSubMenuArr as $thisSubMenu => $thisSubMenuInfoArr)
				{
// 					$thisPageCode = $thisMenu.'_'.$thisSubMenu;
					if($PageAccessRightArr[$thisMenu] == $ePCM_USER_ACCESS){
					    $AccessibleMenuArr[$thisMenu][$thisSubMenu] = $MenuArr[$thisMenu][$thisSubMenu];
					}
					else if(is_array($PageAccessRightArr[$thisMenu])){
					    if ($PageAccessRightArr[$thisMenu][$thisSubMenu] == $ePCM_USER_ACCESS){
					       $AccessibleMenuArr[$thisMenu][$thisSubMenu] = $MenuArr[$thisMenu][$thisSubMenu];
					    }
					    else if($PageAccessRightArr[$thisMenu][$thisSubMenu] == $ePCM_GROUPHEAD_ACCESS){
					        if($this->isGroupHead($_SESSION['UserID'])){
    					        $AccessibleMenuArr[$thisMenu][$thisSubMenu] = $MenuArr[$thisMenu][$thisSubMenu];
					        }
					    }
					}
				}
			}
				
			return $AccessibleMenuArr;
		}
		else
		{
			return $MenuArr;
		}
	}	
	
	public function GET_MODULE_OBJ_ARR(){
		
		global $_PAGE, $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$CurrentPage = $_PAGE['CurrentSection'].'_'.$_PAGE['CurrentPage'];
		$moduleRoot = $this->ModuleDocumentRoot;
	
		$tempCurrentPageArr = explode('_', $CurrentPage);
		$CurrentMenu = $tempCurrentPageArr[0];
		$MenuInfoArr = $this->Get_Left_Menu_Structure_Array($CheckAccessRight=1);
		$MenuArr = array();
		$CurrentPageArr['ePCM'] = 1;

// 		$IsFirstAccessiableMenu = true;
		foreach ((array)$MenuInfoArr as $thisMenu => $thisSubMenuArr)
		{
			$thisIsCurrentMenu = ($CurrentMenu == $thisMenu)? true : false;
			$MenuArr[$thisMenu] = array($Lang['ePCM'][$thisMenu.'Arr']['MenuTitle'], "", $thisIsCurrentMenu);

			foreach ((array)$thisSubMenuArr as $thisSubMenu => $thisSubMenuInfoArr)
			{
				$thisPageCode = $thisMenu.'_'.$thisSubMenu;
				$thisIsCurrentSubMenu = ($CurrentPage == $thisPageCode)? true : false;
				$MenuArr[$thisMenu]["Child"][$thisSubMenu] = array($thisSubMenuInfoArr['Title'], $moduleRoot.$thisSubMenuInfoArr['URL'], $thisIsCurrentSubMenu);
				if($thisIsCurrentSubMenu){
					$this->currentPageName = $thisSubMenuInfoArr['Title'];
				}
		
// 				if ($IsFirstAccessiableMenu == true)
// 				{
// 					$MODULE_OBJ['root_path'] = $thisSubMenuInfoArr['URL'];
// 					$IsFirstAccessiableMenu = false;
// 				}
			}
		}
		
		if(!isset($_SESSION['ePCM']['session_password'])){
			$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ePCM'];
			$MODULE_OBJ['title_css'] = "menu_small";
			$MODULE_OBJ['CustomLogo'] = $this->printLeftMenu();
		}else{
			$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ePCM'];
			$MODULE_OBJ['title_css'] = "menu_opened";
			$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_eProcurement.png";
			$MODULE_OBJ['root_path'] = "index.php";
			$MODULE_OBJ['menu'] = array();
			$MODULE_OBJ['menu'] = $MenuArr;
		}

        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass eProcurement";'."\n";
        $js.= '</script>'."\n";
        $MODULE_OBJ['title'] .= $js;

		return $MODULE_OBJ;
	}
	
	function printLeftMenu() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $CurrentPageArr;
	
		$img = $PATH_WRT_ROOT . "images/{$LAYOUT_SKIN}/leftmenu/icon_eProcurement.png";
		$x .= '<span><div id="Reading_Scheme_logo">';
		$x .= "<a href='" . $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eProcurement/'><img border=\"0\" width=\"110\" height=\"100\" src=\"$img\" /></a>";
		$x .= '</div></span>';
	
		return $x;
	}
	
	function checkCurrentIP($ip_list){
		$ip_allowed = false;
	
		// check current IP within allowed IP addresses
		$allowed_IPs = trim($ip_list);
		$ip_addresses = explode("\n", $allowed_IPs);
		for($i=0; $i<count($ip_addresses); $i++) {
			$ip_addresses[$i] = trim($ip_addresses[$i]);
			if(testip($ip_addresses[$i], $_SERVER['REMOTE_ADDR'])) {
				$ip_allowed = true;
			}
		}
		if(in_array('0.0.0.0',$ip_addresses) || $allowed_IPs=='') {
			$ip_allowed = true;
		}
	
		return $ip_allowed;
	}
	
	### Mail
	
	public function sendMail($subject,$message,$from,$receiver_to,$receiver_cc,$receiver_bcc,$attachment_path,$IsImportant="",$mail_return_path="",$reply_address="",$isMulti=null,$nl2br=1,$showSenderAsAdmin=false){
		global $intranet_root;
		
		include_once $intranet_root.'/includes/libwebmail.php';
		$mail=new libwebmail();
		return $mail->sendMail($subject,$message,$from,(array)$receiver_to,(array)$receiver_cc,(array)$receiver_bcc,$attachment_path,$IsImportant,$mail_return_path,$reply_address,$isMulti,$nl2br,$showSenderAsAdmin);
	}
	
	public function getSenderEmail($user_id, $SendAsAdmin=false){
		global $intranet_root;
		
		include_once $intranet_root.'/includes/libmessagecenter.php';
		$mail_center=new libmessagecenter();
		return $mail_center->getSenderEmail($SendAsAdmin, $user_id);
	}
	
	# $object = 'SUPPLIER_CONTACT' or 'SUPPLIER' or 'ALL_SUPPLIER_CONTACT'
	# $record_id = SupplierID (when $object = 'SUPPLIER' or 'ALL_SUPPLIER_CONTACT') or ContactID (when $object = 'SUPPLIER_CONTACT')
	# $subject = Email Subject
	# $message = Email Boby
	# $addition_to = Extra email address that placed at TO
	# $cc = CC array
	# $bcc = BCC array
	# $attachement_path = Path to attachment folder
	# $from = From address
	public function sendEmailToSupplier($object,$record_id,$subject='',$message='',$addition_to='',$cc='',$bcc='',$attachment_path='',$from=''){
		global $intranet_root;
		
		if(!$object){
			return;
		}
		
		//get email addresses
		include_once $intranet_root.'/includes/ePCM/libPCM_db_setting.php';
		$db=new libPCM_db_setting();
		
		$rows_supplier_contact=array();
		$array_supplier_email=array();
		if($object=='SUPPLIER_CONTACT'){
			$rows_supplier_contact=$db->getSupplierContactInfo($record_id);
		}else if($object=='SUPPLIER' || $object=='ALL_SUPPLIER_CONTACT'){
			$rows_supplier=$db->getSupplierInfo($record_id);
			foreach((array)$rows_supplier['MAIN'] as $supplier){
				if($supplier['Email']!=''){
					array_push($array_supplier_email,$supplier['Email']);
				}
			}
			if($object=='ALL_SUPPLIER_CONTACT'){
				$rows_supplier_contact=$rows_supplier['CONTACT'];
			}
		}
		
		foreach((array)$rows_supplier_contact as $contact){
			if($contact['Email']!=''){
				array_push($array_supplier_email,$contact['Email']);
			}
		}
		
		foreach((array)$addition_to as $to){
			if($to!=''){
				array_push($array_supplier_email,$to);
			}
		}
		
		//send out emails
		if(count($array_supplier_email)>0){
			$result1=$this->sendMail($subject,$message,$from,$array_supplier_email,$cc,$bcc,$attachment_path);
			if($result1){
				$result2=$this->sendModuleMail(array($_SESSION['UserID']),$subject,$message,$UseAdminEmailAsSender=1,$attachment_path);
			}else{
				$result2=false;
			}
			return $result1 && $result2;
		}else{
			return false;
		}
	}
	
	public function sendEmailToSupplierByQuotation($object,$quotation_id,$token_id='',$subject='',$message='',$addition_to='',$cc='',$bcc='',$attachment_path='',$from=''){
		global $intranet_root,$Lang;
		if(!$object){
			return 'FAILED';
		}
		
		include_once $intranet_root.'/includes/ePCM/libPCM_db_setting.php';
		$db=new libPCM_db_setting();
		
		$rows_quotation=$db->getCaseQuotation('',$quotation_id);
		$row_quotation=$rows_quotation[0];
		$supplier_id=$row_quotation['SupplierID'];
		
		//-------------prepare message---------------
		$str_token_identifier='<!--TOKEN_URL_START-->';
		$str_token_identifier2='<!--TOKEN_URL_END-->';
		$str_token_identifier3='<!--TOKEN_URL-->';
		$str_token_identifier4='<!--TOKEN_END_DATE-->';
		$str_token_identifier5='<!--API_PASSWORD-->';
		
		$actual_link = 'http://'.$_SERVER['HTTP_HOST'].str_replace(array('/home/eAdmin/ResourcesMgmt/eProcurement/index.php','?'.$_SERVER['QUERY_STRING']),'',$_SERVER['REQUEST_URI']);
		$str_token=$db->getToken($quotation_id,$token_id);
		
		//2016-06-30	random generated password for downloading attachment
		$pwd = $this->getPasswordGeneration($str_token);
		$str_pw = $Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Password'].$pwd;
		
		$str_url=$actual_link.'/api/ePCM/acknowledge.php?t='.$str_token;
		$url_token='<a href="'.$str_url.'">';
		$url_token2='</a>';
		
		//Get token enddate 
		$token_end_date = $db->getTokenEndDate($str_token);
		if($token_end_date){
			
		}
		$message=str_replace($str_token_identifier,$url_token,$message);
		$message=str_replace($str_token_identifier2,$url_token2,$message);
		$message=str_replace($str_token_identifier3,$str_url,$message);
		$message=str_replace($str_token_identifier5,$str_pw,$message);
		
		return $this->sendEmailToSupplier($object,$supplier_id,$subject,$message,$addition_to,$cc,$bcc,$attachment_path,$from)?'SENT':'FAILED';
	}
	
	
	/*
	 * @param string $SendTo : default 'User' - to particular user (student, parent, teacher) 
	 * 						   'CCParent' - send to student and cc to parent
	 * 						   'ParentOnly' - send to parent of the student
	 */
	public function sendModuleMail($ToArray,$Subject,$Message,$UseAdminEmailAsSender=1,$AttachmentPath='',$SendTo='User',$addDoNotReply=false,$showSenderAsAdmin=false){
		global $PATH_WRT_ROOT, $intranet_root;
		include_once($PATH_WRT_ROOT."includes/libwebmail.php");
		$lwebmail = new libwebmail();
		if($this->isEJ()){
			$Subject = iconv('UTF8', 'BIG5-HKSCS', $Subject);
			$Message = iconv('UTF8', 'BIG5-HKSCS', $Message);
		}
		return $lwebmail -> sendModuleMail($ToArray,$Subject,$Message,$UseAdminEmailAsSender,$AttachmentPath,$SendTo,$addDoNotReply,$showSenderAsAdmin);
	}
	
	function sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic='', $recordStatus=1, $appType='T', $sendTimeMode='', $sendTimeString='', $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule='', $moduleRecordID='', $createRecordOnly=false) {
		global $PATH_WRT_ROOT,$intranet_root, $config_school_code, $sys_custom, $eclassAppConfig,$forceUseClass;
		include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
		$libeClassApp = new libeClassApp();
        if($this->isEJ()){
            $messageTitle = convert2unicode($messageTitle, $isAbsOn="", $direction=0);
            $messageContent = convert2unicode($messageContent, $isAbsOn="", $direction=0);
        }
			return $libeClassApp-> sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus, $appType, $sendTimeMode, $sendTimeString, $parNotifyMessageId, $parNotifyMessageTargetIdAry, $fromModule, $moduleRecordID, $createRecordOnly);
//		}
	}
	private function getPasswordGeneration($token){
		global $intranet_root;
		include_once $intranet_root.'/includes/ePCM/libPCM_db_setting.php';
		$db=new libPCM_db_setting();
		
		$tokenInfo = $db->getTokenInfo($token,$quotationID='');
		if(!empty($tokenInfo['Pw'])){
			return $tokenInfo['Pw'];
		}
		// no 0,o,O / l,1 => prevent hard to read
		$length = 8;
		$chars = "abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ23456789";
		$pwd = substr(str_shuffle($chars),0,$length);
		
		
		$db->updateTokenPassword($token,$pwd);
		return $pwd;
	}
	public function canUseAppNotification(){
		global $plugin, $setting;
		return isset($plugin['eClassTeacherApp']) && $plugin['eClassTeacherApp']===true && $setting['eClassAppNotification']=='1';
	}
	
	
	####################
	#	Flag related   #
	####################
	
	public function enableGroupSupplierType(){
		global $sys_custom;
		return $sys_custom['ePCM']['enableGroupSupplierType'];
	}
	
	public function enableFundingSource(){
		global $sys_custom;
		return $sys_custom['ePCM']['enableFundingSource'];
	}
}
?>