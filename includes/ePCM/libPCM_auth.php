<?php
// using by :

/**
 * Date: 2017-03-01 Villa
 * - #X100660 add isAdminOrSMC
 * 
 * Date: 2016-08-29	Omas
 * - modified checkInputResultRight()
 * 
 */

class libPCM_auth
{
	public function __construct()
	{
		
	}
	
	private function extractValueFromPAGE($valuePostionInPAGE, $returnArr=true){
	    global $_PAGE;
	    
	    $valArr = array();
	    if(is_array($valuePostionInPAGE)){
            foreach((array)$valuePostionInPAGE as $_valuePostionInPAGE){
                $result = $this->extractValueFromPAGE($_valuePostionInPAGE, false);
                $valArr[] = $result;
            }
            return $valArr;
	    }
	    else{
    	    $positionArr = explode('_', $valuePostionInPAGE);
    	    $php = '$tempValue = $_PAGE';
    	    foreach((array)$positionArr as $_arrKey){
    	        $php .= '['.$_arrKey.']';
    	    }
    	    $php .= ';';
    	    eval($php);
    	    if(!$returnArr){
    	        return $tempValue;
    	    }
    	    else{
    	        $valArr[] = $tempValue;
    	        return $valArr;
    	    }
	    }
	}
	
	public function callAuthByMapping($authRules)
	{
	    global $_PAGE;
	    $callable = $authRules['function'];
	    $valuePostionInPAGE = $authRules['authValue'];
		if(!$this->isAdmin()){
	        if($callable != ''){
	            if($valuePostionInPAGE != ''){
	    	       $valueArrForCallable = $this->extractValueFromPAGE($valuePostionInPAGE);
	            }
		       
		       if(method_exists($this, $callable)){
		           $php = '$tempResult = $this->$callable('.implode(',', (array)$valueArrForCallable).');
								  $debugResult[$value][$rule] = $tempResult;';
		           eval($php);
		           $result = $tempResult;
		       }
		       else{
		           debug_pr('function: '.$callable.' is not defined');
		       }
	        }
		    else{
	// 	        debug_pr('no auth rules is set return true now');
		        $result = false;
		    }
		}
		else{
			$result = true;
		}
	    return $result;
	}
	
	public function isAdmin(){
		global $_PAGE;
		$libPCM = $_PAGE['Controller']->requestObject('libPCM','includes/ePCM/libPCM.php');
		return $libPCM->isAdmin();
	}

	
	public function allUser(){
	    return true;
	}
	
	public function isAdminOrSMC(){
		global $_PAGE;
		$libPCM = $_PAGE['Controller']->requestObject('libPCM','includes/ePCM/libPCM.php');
		$isAdmin = $libPCM->isAdmin();
		$isSMC = $libPCM->isSMC();
		if($isSMC||$isAdmin){
			$accessRight = true;
		}else{
			$accessRight = false;
		}
		return $accessRight;
	}
	
	public function checkViewFilePermission($attachmentID){
		global $_PAGE;
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$fileInfo = $db->getFileById($attachmentID);
		$caseID = $fileInfo[0]['CaseID'];
		if($caseID >0){
			$result = $this->checkCaseGroupMember($caseID);
		}
		else{
			$result = true;
		}
		return $result;
	}
	
	public function checkModifyFilePermission($attachmentID){
		global $_PAGE;
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$fileInfo = $db->getFileById($attachmentID);
		$caseID = $fileInfo[0]['CaseID'];
		if($caseID != -1){
			$result = $this->caseOwner($caseID);
		}
		else{
			$result = $this->isAdmin();
		}
		return $result;
	}
	
	public function checkCaseGroupMember($caseID){
        global $_PAGE, $sys_custom;
        $canAccess = false;
        $db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
        $caseInfoAry =  $db->getCaseInfo($caseID,'','','');
        $groupID = $caseInfoAry[0]['ApplicantGroupID'];
        $groupMember = $db->getGroupMemberInfo($groupID);
        $UserIDAry = Get_Array_By_Key($groupMember, "UserID");
        if($sys_custom['ePCM']['extraApprovalFlow']||$sys_custom['ePCM_skipCaseApproval']){
            $custCaseApproval = $db->getCustCaseApproval($caseID);
            $custUserIDAry = Get_Array_By_Key($custCaseApproval, "ApproverUserID");
        }
        if(in_array($_SESSION['UserID'],(array)$UserIDAry)||in_array($_SESSION['UserID'],(array)$custUserIDAry)){
            $canAccess = true;
        }
        return $canAccess;
	}
	
	public function checkCaseAccessRight($caseID){
		global $_PAGE;
		
		$canAccess = false;
		if($this->caseOwner($caseID)){
// 			debug_pr('Why you can see the case? '.'owner / admin');
			$canAccess = true;
		}else if($this->checkCaseGroupMember($caseID)){
// 			debug_pr('Why you can see the case? '.'your group');
			$canAccess = true;
		}else if($this->checkCaseApprovalRight($caseID)){
// 			debug_pr('Why you can see the case? '.'you have to approve case');
			$canAccess = true;
		}else if ($this->checkSendQuotationInvitationRight($caseID)){
// 			debug_pr('Why you can see the case? '.'you have to send invitation');
			$canAccess = true;
		}else if($this->checkInputQuotationResponseRight($caseID)){
// 			debug_pr('Why you can see the case? '.'you need to input quotation result');
			$canAccess = true;
		}else if($this->checkInputResultRight($caseID)){
// 			debug_pr('Why you can see the case? '.'you need to input case result');
			$canAccess = true;
		}
		return $canAccess;
	}
	
	public function caseOwner($caseID){
		global $_PAGE,$_SESSION;
		$canAccess = false;
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		//1. Get case owner
		$caseInfoAry =  $db->getCaseInfo($caseID,'','','');
		$ownerUserID = $caseInfoAry[0]['ApplicantUserID'];
		//2. is Admin?
		if($_SESSION['UserID']==$ownerUserID||$this->isAdmin()){
			$canAccess = true;
		}
// 		return false;
		return $canAccess;
	}
	public function checkCaseApprovalRight($caseID,$approverUserID=''){
		global $_PAGE,$_SESSION;
		/**
		 * if $approverUserID = '' => for any approver
		 */
		$canAccess = false;
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		//1. Get case approvaler
		$caseApproval = $db->getCaseApproval($caseID,$approverUserID);
		$approverUserIDAry = Get_Array_By_Key($caseApproval, "ApproverUserID");
		if(in_array($_SESSION['UserID'],$approverUserIDAry)){
			$canAccess = true;
		}
		if($this->isAdmin()){
			$canAccess = true;
		}
		return $canAccess;
	}
	public function checkCustCaseApprovalRight($caseID,$approverUserID=''){
		global $_PAGE,$_SESSION;
		/**
		 * if $approverUserID = '' => for any approver
		 */
		// 		debug_pr($caseID);
		$canAccess = false;
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		//1. Get case approvaler
		$caseApproval = $db->getCustCaseApproval($caseID,$approverUserID);
		$approverUserIDAry = Get_Array_By_Key($caseApproval, "ApproverUserID");
		if(in_array($_SESSION['UserID'],$approverUserIDAry)){
			$canAccess = true;
		}
		if($this->isAdmin()){
			$canAccess = true;
		}
		return $canAccess;
	}
	public function checkSendQuotationInvitationRight($caseID){
//  		debug_pr($caseID);
		return $this->caseOwner($caseID);
	}
	public function checkInputQuotationResponseRight($caseID){
		global $_PAGE,$_SESSION;
// 		debug_pr($caseID);
		/**
		 * Steps : check type of case
		 * 		- If type = 'Q' => caseOwner
		 * 		- If type = 'T' => Find from Rules
		 */
		$canAccess = false;
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		//1. Get case quotation type 
		$caseInfoAry =  $db->getCaseInfo($caseID,'','','');
		$quotationType = $caseInfoAry[0]['QuotationType'];
		
		switch ($quotationType){
			case 'V';
			case 'Q';
				$canAccess = $this->caseOwner($caseID); //included is Admin
			break;
			case 'T';
				$tenderAuditingRoleID = $caseInfoAry[0]['TenderAuditingRoleID'];
				$roleUser = $db->getRoleUser($tenderAuditingRoleID,$academicYearID=''); //assume using current academic year
				$UserIDAry = Get_Array_By_Key($roleUser, "UserID");
				if(in_array($_SESSION['UserID'],(array)$UserIDAry)){
					$canAccess = true;
				}
				if($this->isAdmin()){
					$canAccess = true;
				}
			break;
		}
		return $canAccess;
	}
	public function checkInputResultRight($caseID){
		global $_PAGE,$_SESSION;
// 		debug_pr($caseID);
		/**
		 * Steps : check type of case
		 * 		- If type = 'Q' => caseOwner
		 * 		- If type = 'T' => Find from Rules
		 */
		$canAccess = false;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		//1. Get case quotation type 
		$caseInfoAry =  $db->getCaseInfo($caseID,'','','');
		$quotationType = $caseInfoAry[0]['QuotationType'];
		
		switch ($quotationType){
			case 'V';
			case 'Q';
				//$canAccess = $this->caseOwner($caseID); //included is Admin
				$quotationApprovalRoleID = $caseInfoAry[0]['QuotationApprovalRoleID'];
				if($quotationApprovalRoleID > 0){
					$roleUser = $db->getRoleUser($quotationApprovalRoleID,$academicYearID=''); //assume using current academic year
					$UserIDAry = Get_Array_By_Key($roleUser, "UserID");
				}else if ($quotationApprovalRoleID == -1){
					// -1 = grouphead 
					$groupHeadAry = $db->getGroupHead($caseInfoAry[0]['ApplicantGroupID']);
					$UserIDAry = Get_Array_By_Key($groupHeadAry,'UserID');
				}else if ($quotationApprovalRoleID == -999){
					// -999 = case Owner
					$canAccess = $this->caseOwner($caseID);
				}
				
				if(in_array($_SESSION['UserID'],(array)$UserIDAry)){
					$canAccess = true;
				}
				if($this->isAdmin()){
					$canAccess = true;
				}
			break;
			case 'T';
				$tenderApprovalRoleID = $caseInfoAry[0]['TenderApprovalRoleID'];
				$roleUser = $db->getRoleUser($tenderApprovalRoleID,$academicYearID=''); //assume using current academic year
				$UserIDAry = Get_Array_By_Key($roleUser, "UserID");
				if(in_array($_SESSION['UserID'],(array)$UserIDAry)){
					$canAccess = true;
				}
				if($this->isAdmin()){
					$canAccess = true;
				}
			break;
			case 'N';
				$canAccess = $this->caseOwner($caseID);
			break;
		}
		return $canAccess;
	}
}

?>