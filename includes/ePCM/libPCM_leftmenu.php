<?php
//2016-11-02	Villa get the general setting
global $_PAGE;
$MenuArr['Management']['View']['Title'] = $Lang['ePCM']['ManagementArr']['ViewArr']['MenuTitle'];
$MenuArr['Management']['View']['URL'] = 'mgmt.view';
$MenuArr['Management']['FinishedView']['Title'] = $Lang['ePCM']['ManagementArr']['FinishedViewArr']['MenuTitle'];
$MenuArr['Management']['FinishedView']['URL'] = 'mgmt.finished';
//2016-09-29 Villa add other application
//2016-10-07 Villa hide the other application if the user is not Admin
//2016-10-30 Omas show other application
//2016-11-02 Villa hide the Other Application if the user set to be
// if($_SESSION['ePCM']['isAdmin']){
if($_SESSION['ePCM']['Setting']['General']['MiscExpenditure']){
// if($setting['MiscExpenditure'] ){
$MenuArr['Management']['OthersView']['Title'] = $Lang['ePCM']['ManagementArr']['FinishedViewArr']['OtherApplication'];
$MenuArr['Management']['OthersView']['URL'] = 'mgmt.otherfinished';
}
// }

$MenuArr['Report']['PCMRecordSummary']['Title'] = $Lang['ePCM']['ReportArr']['PCMRecordSummaryArr']['MenuTitle'];
$MenuArr['Report']['PCMRecordSummary']['URL'] = 'report.summary.pcmrs';
$MenuArr['Report']['FileCodeSummary']['Title'] = $Lang['ePCM']['ReportArr']['FileCodeSummaryArr']['MenuTitle'];
$MenuArr['Report']['FileCodeSummary']['URL'] = 'report.summary.fcs';
$MenuArr['Report']['BudgetExpensesReport']['Title'] = $Lang['ePCM']['ReportArr']['BudgetExpensesReportArr']['MenuTitle'];
$MenuArr['Report']['BudgetExpensesReport']['URL'] = 'report.summary.ber';

$MenuArr['Settings']['General']['Title'] = $Lang['ePCM']['SettingsArr']['GeneralArr']['MenuTitle'] ;
$MenuArr['Settings']['General']['URL'] = 'setting.general';
$MenuArr['Settings']['Role']['Title'] = $Lang['ePCM']['SettingsArr']['RoleArr']['MenuTitle'] ;
$MenuArr['Settings']['Role']['URL'] = 'setting.role';
$MenuArr['Settings']['Rule']['Title'] = $Lang['ePCM']['SettingsArr']['RuleArr']['MenuTitle'] ;
$MenuArr['Settings']['Rule']['URL'] = 'setting.rule';
$MenuArr['Settings']['Category']['Title'] = $Lang['ePCM']['SettingsArr']['CategoryArr']['MenuTitle'] ;
$MenuArr['Settings']['Category']['URL'] = 'setting.category';
if($_PAGE['libPCM']->enableFundingSource()){
$MenuArr['Settings']['Funding']['Title'] = $Lang['ePCM']['SettingsArr']['FundingSource']['MenuTitle'] ;
$MenuArr['Settings']['Funding']['URL'] = 'setting.funding';
}
$MenuArr['Settings']['Group']['Title'] = $Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle'] ;
$MenuArr['Settings']['Group']['URL'] = 'setting.group';
$MenuArr['Settings']['Supplier']['Title'] = $Lang['ePCM']['SettingsArr']['SupplierArr']['MenuTitle'] ;
$MenuArr['Settings']['Supplier']['URL'] = 'setting.supplier';
$MenuArr['Settings']['Security']['Title'] = $Lang['ePCM']['SettingsArr']['SecurityArr']['MenuTitle'] ;
$MenuArr['Settings']['Security']['URL'] = 'setting.security';
$MenuArr['Settings']['SampleDocument']['Title'] = $Lang['ePCM']['SettingsArr']['SampleDocumentArr']['MenuTitle'] ;
$MenuArr['Settings']['SampleDocument']['URL'] = 'setting.template';
?>