<?php 
// using by 
/**
 * Date:	2019-06-03 Henry
 * - add updateCasePrincipalApprove() - update approve status by principal
 * Date:	2017-04-18 Villa
 * - add updateCaseSkipBudget() - update caseinfo and skip budget only
 * - add deleteFundingCaseRelation - delete fundingCase Relation bu CaseID and FundingID
 * 
 * Date:	2017-04-13 Villa
 * - modified updateFundingCaseRelation() - fix bug
 * Date:	2017-03-07 Villa
 * - modified getFundingCaseArr - retrun Funding detail with case
 * 
 * Date:	2017-03-06 Villa
 * - modified updateFundingCaseRelation() - Main insert the Funding Case Relation Record
 * - modified updateFundingCaseRelationByCase - update all the Record Related by Case
 * 
 * Date:	2017-03-01 Villa
 * - modified getUsersGroup() - let SMC have Admin access right
 * 
 * Date:	2016-11-09 Villa
 * - modified getViewableCaseInfo() - add paras academic for filtering academic year
 * 
 * Date:	2016-10-07 Villa
 * - modified getSuitableRuleTemplate() - update the checking with the budget updated to float type
 * 
 * Date:	2016-10-05 Villa
 * - added deleteResult() - update Result 'isDeteled' to 0 for other application
 * 
 * Date:	2016-10-04 Villa
 * - added insertNewCaseWithTime() - insert the record to PCM_case with time
 * - added updateCaseForOtherApp() - update the record to INTRANET_PCM_CASE
 * - added insertDataWithCustDateInput() - insert the record to PCM_case with time
 * - added updateResultFromCaseID() - update the result table with a selected CaseID
 * 
 * Date: 	2016-09-19 Omas
 * - added getSupplierTypeInfoAssoArrForDropDownList()
 * 
 * Date: 	2016-09-09 Villa
 * - modified insertResult(), updateResult() - add funding source items
 * 
 * Date:	2016-08-26	Omas
 * - added smartInsertCaseRemarks(), smartDeleteCaseRemarks(), sortingApproverForDisplayByRoleType(), checkCurrentApprovalStage(), sendNotificationToApprovalRole()
 * - modified duplicateTemplateRule(), sendCaseApproverInvitation()
 * 
 * Date:	2016-08-16	Kenneth
 * - modified duplicateTemplateRule(), add NeedVicePrincipalApprove
 * 
 * Date: 	2016-08-11	Kenneth
 * - modified getViewableCaseInfo(), set currentStage of completed case = 10
 * 
 * Date:	2016-07-15	Kenneth
 * - modified getViewableCaseInfo(), cancel finished case keep one day logic
 * 
 * Date:	2016-07-07	Kenneth
 * - modified sendCaseApproverInvitation(), get role name and gorup name
 * 
 * Date:	2016-07-05	Kenneth
 * - added updateCaseCodeAndName()
 * - modified getViewableCaseInfo(), add search by quotation type
 * 
 * Date:	2016-06-29	Kenneth
 * 	- modified getValidQuotation(), skip stage 4 for vebal quotation
 */
include_once($intranet_root."/includes/ePCM/libPCM_db.php");
class libPCM_db_mgmt extends libPCM_db
{

    public function __construct()
    {
        parent::__construct();
    }

    private function isAdmin()
    {
        global $_PAGE, $intranet_root;
        include_once($intranet_root . "/includes/ePCM/libPCM.php");
        //$libPCM = $_PAGE['Controller']->requestObject('libPCM','includes/ePCM/libPCM.php');
        $libPCM = new libPCM();
        return $libPCM->isAdmin();
    }

    private function isSMC()
    {
        global $_PAGE, $intranet_root;
        include_once($intranet_root . "/includes/ePCM/libPCM.php");
        //$libPCM = $_PAGE['Controller']->requestObject('libPCM','includes/ePCM/libPCM.php');
        $libPCM = new libPCM();
        return $libPCM->isSMC();
    }


    ## Get User's Group(s) --- if isAdmin() get all groups
    public function getUsersGroup()
    {
        global $UserID, $sys_custom;
        if ($this->isAdmin()) {
            $groupAry = $this->getGroupInfo(array());
        } else {
            if ($this->isSMC() && $sys_custom['ePCM']['SMC']) {
                $groupAry = $this->getGroupInfo(array());
            } else {
                $groupAry = $this->getGroupInfoByMemberUserID($UserID);
            }
        }
        return $groupAry;
    }

    public function insertNewCase($multiRowValueArr)
    {
        $fieldNameArr = array(
            'CaseName',
            'CategoryID',
            'Description',
            'ItemID',
            'AcademicYearID',
            'Budget',
            'ApplicantType',
            'ApplicantUserID',
            'ApplicantGroupID',
            'CurrentStage',
            'RuleID'
        );
        return $this->insertData($tableName = 'INTRANET_PCM_CASE', $fieldNameArr, $multiRowValueArr);
    }

    public function insertNewCaseWithTime($multiRowValueArr)
    { //2016-10-04	Villa
        $fieldNameArr = array(
            'CaseName',
            'CategoryID',
            'Description',
            'ItemID',
            'AcademicYearID',
            'Budget',
            'ApplicantType',
            'ApplicantUserID',
            'ApplicantGroupID',
            'CurrentStage',
            'RuleID',
            'DateInput'
        );
        return $this->insertDataWithCustDateInput($tableName = 'INTRANET_PCM_CASE', $fieldNameArr, $multiRowValueArr);
    }

    public function updateCaseForOtherApp($multiRowValueArr, $caseID)
    {    //2016-10-04	Villa
        $fieldNameArr = array(
            'CaseName',
            'CategoryID',
            'Description',
            'ItemID',
            'AcademicYearID',
            'Budget',
            'ApplicantType',
            'ApplicantUserID',
            'ApplicantGroupID',
            'CurrentStage',
            'DateInput'
        );
        $cond = "caseID = $caseID";
        return $this->updateData($tableName = 'INTRANET_PCM_CASE', $fieldNameArr, $multiRowValueArr, $cond);
    }

    public function insertNewCaseWithCode($multiRowValueArr)
    {
        $fieldNameArr = array(
            'Code',
            'CaseName',
            'CategoryID',
            'Description',
            'ItemID',
            'AcademicYearID',
            'Budget',
            'ApplicantType',
            'ApplicantUserID',
            'ApplicantGroupID',
            'CurrentStage',
            'RuleID'
        );
        return $this->insertData($tableName = 'INTRANET_PCM_CASE', $fieldNameArr, $multiRowValueArr);
    }

    public function updateCase($multiRowValueArr, $caseID)
    {
        $changefieldNameArr = array(
            'CaseName',
            'CategoryID',
            'Description',
            'ItemID',
            'AcademicYearID',
            'Budget',
            'ApplicantType',
            'ApplicantUserID',
            'ApplicantGroupID',
            'CurrentStage',
            'RuleID'
        );
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateCaseSkipBudget($multiRowValueArr, $caseID)
    {
        $changefieldNameArr = array(
            'CaseName',
            'CategoryID',
            'Description',
            'ItemID',
            'AcademicYearID',
            'ApplicantType',
            'ApplicantUserID',
            'ApplicantGroupID'
        );
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function smartInsertCaseRemarks($caseID, $stageID, $remarks)
    {

        $existRemarks = $this->getCaseRemarks($caseID, $stageID);
        if (count($existRemarks) > 0) {
            // already exist please update
            $changefieldNameArr = array('Remark');
            $multiRowValueArr = array(array($remarks));
            $conditions = " CaseID = '" . $caseID . "' AND StageID = '" . $stageID . "' ";
            $this->updateData('INTRANET_PCM_CASE_REMARK', $changefieldNameArr, $multiRowValueArr, $conditions);
        } else {
            // insert new
            $fieldNameArr = array('CaseID', 'StageID', 'Remark');
            $multiRowValueArr = array(array($caseID, $stageID, $remarks));
            $result = $this->insertData($tableName = 'INTRANET_PCM_CASE_REMARK', $fieldNameArr, $multiRowValueArr);
        }
        return $result;
    }

    public function smartDeleteCaseRemarks($caseID, $stageID)
    {

        $existingRemarks = $this->getCaseRemarks($caseID, $stageID);
        if (count($existingRemarks) > 0) {
            $this->deleteData('INTRANET_PCM_CASE_REMARK', 'RemarkID', $existingRemarks[0]['RemarkID']);
        } else {
            // do noth
        }
    }

    public function insertCaseRemarks($caseID, $stageID, $remarks)
    {
        $fieldNameArr = array('CaseID', 'StageID', 'Remark');
        $multiRowValueArr = array(array($caseID, $stageID, $remarks));
        return $this->insertData($tableName = 'INTRANET_PCM_CASE_REMARK', $fieldNameArr, $multiRowValueArr);
    }

    public function updateCaseCode($caseID, $code)
    {
        $changefieldNameArr = array('Code');
        $multiRowValueArr = array(array($code));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateCaseApproval($caseID, $approverUserID, $recordStatus, $feedback, $approverRoleID)
    {
        $changefieldNameArr = array('RecordStatus', 'Feedback');
        $multiRowValueArr = array(array($recordStatus, $feedback));
        $conditions = " CaseID = '$caseID' AND ApproverUserID = '$approverUserID' AND ApprovalRoleID = '$approverRoleID' ";
        return $this->updateData('INTRANET_PCM_CASE_APPROVAL', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateCustCaseApproval($caseID, $approverUserID, $recordStatus, $feedback, $approverRoleID)
    {
        $changefieldNameArr = array('RecordStatus', 'Feedback');
        $multiRowValueArr = array(array($recordStatus, $feedback));
        $conditions = " CaseID = '$caseID' AND ApproverUserID = '$approverUserID' AND ApprovalRoleID = '$approverRoleID' ";
        return $this->updateData('INTRANET_PCM_CUST_CASE_APPROVAL', $changefieldNameArr, $multiRowValueArr,
            $conditions);
    }

    public function insertResult(
        $caseID,
        $supplierID,
        $dealPrice,
        $description,
        $dealDate
//        $fundingId,
//        $fundingId2,
//        $itemUnitPrice1,
//        $itemUnitPrice2
    ) {
        $fieldNameArr = array(
            'CaseID',
            'SupplierID',
            'DealPrice',
            'Description',
            'DealDate'
//            'FundingSource1',
//            'FundingSource2',
//            'UnitPrice1',
//            'UnitPrice2'
        );
        $multiRowValueArr = array(
            array(
                $caseID,
                $supplierID,
                $dealPrice,
                $description,
                $dealDate
//                $fundingId,
//                $fundingId2,
//                $itemUnitPrice1,
//                $itemUnitPrice2
            )
        );
        return $this->insertData($tableName = 'INTRANET_PCM_RESULT', $fieldNameArr, $multiRowValueArr);
    }

    public function updateResult(
        $resultID,
        $caseID,
        $supplierID,
        $dealPrice,
        $description,
        $dealDate
//        $fundingId,
//        $fundingId2,
//        $itemUnitPrice1,
//        $itemUnitPrice2
    ) {
        $changefieldNameArr = array(
            'CaseID',
            'SupplierID',
            'DealPrice',
            'Description',
            'DealDate'
//            'FundingSource1',
//            'FundingSource2',
//            'UnitPrice1',
//            'UnitPrice2'
        );
        $multiRowValueArr = array(
            array(
                $caseID,
                $supplierID,
                $dealPrice,
                $description,
                $dealDate
//                $fundingId,
//                $fundingId2,
//                $itemUnitPrice1,
//                $itemUnitPrice2
            )
        );
        $conditions = " ResultID = '$resultID' ";
        return $this->updateData('INTRANET_PCM_RESULT', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateResultFromCaseID(
        $caseID,
        $supplierID,
        $dealPrice,
        $description,
        $dealDate
//        $fundingId,
//        $fundingId2,
//        $itemUnitPrice1,
//        $itemUnitPrice2
    ) {
        $changefieldNameArr = array(
            'CaseID',
            'SupplierID',
            'DealPrice',
            'Description',
            'DealDate',
            'FundingSource1',
            'FundingSource2',
            'UnitPrice1',
            'UnitPrice2'
        );
        $multiRowValueArr = array(
            array(
                $caseID,
                $supplierID,
                $dealPrice,
                $description,
                $dealDate
//                $fundingId,
//                $fundingId2,
//                $itemUnitPrice1,
//                $itemUnitPrice2
            )
        );
        $conditions = " CaseID = '$caseID' ";
        return $this->updateData('INTRANET_PCM_RESULT', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function isCaseRejected($caseID = '', $caseApprovalAry = '')
    {
        //Either by caseID / caseApprovalAry
        if ($caseApprovalAry == '') {
            $allApprovalAry = $this->getCaseApproval($caseID);
        } else {
            $allApprovalAry = $caseApprovalAry;
        }
        $isRejected = false;
        foreach ($allApprovalAry as $_approvalInfo) {
            if ($_approvalInfo['RecordStatus'] == '-1') {
                $isRejected = true;
            }
        }
        return $isRejected;
    }

    public function isCustCaseRejected($caseID = '', $caseApprovalAry = '')
    {
        //Either by caseID / caseApprovalAry
        if ($caseApprovalAry === '') {
            $allApprovalAry = $this->getCustCaseApproval($caseID);
        } else {
            $allApprovalAry = $caseApprovalAry;
        }
        $isRejected = false;
        foreach ($allApprovalAry as $_approvalInfo) {
            if ($_approvalInfo['RecordStatus'] == '-1') {
                $isRejected = true;
            }
        }
        return $isRejected;
    }

    public function updateCaseCurrentStage($caseID, $stage)
    {
        $changefieldNameArr = array('CurrentStage');
        $multiRowValueArr = array(array($stage));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }
    
    public function updateCasePrincipalApprove($caseID, $Approve)
    {
        $changefieldNameArr = array('IsPrincipalApproved');
        $multiRowValueArr = array(array($Approve));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateTenderOpeningDates($caseID, $startDate, $endDate)
    {
        $changefieldNameArr = array('TenderOpeningStartDate', 'TenderOpeningEndDate');
        $multiRowValueArr = array(array($startDate, $endDate));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateTenderApprovalDates($caseID, $startDate, $endDate)
    {
        $changefieldNameArr = array('TenderApprovalStartDate', 'TenderApprovalEndDate');
        $multiRowValueArr = array(array($startDate, $endDate));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateQuotationDate($quotationID, $quoteDate, $remarks)
    {
        $changefieldNameArr = array('QuoteDate', 'Remarks');
        $multiRowValueArr = array(array($quoteDate, $remarks));
        $conditions = " QuotationID = '$quotationID'";
        return $this->updateData('INTRANET_PCM_CASE_QUOTATION', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function getSuitableRuleTemplate($budget)
    {
        $allRuleTemplate = $this->getRuleTemplateInfo();
//Date 2016-10-07	Villa update the checking with the update of budget from int to float
        $budget = ceil($budget);
        foreach ($allRuleTemplate as $ruleTemplate) {
            if ($ruleTemplate['FloorPrice'] <= $budget && $ruleTemplate['CeillingPrice'] >= $budget) {
                return $ruleTemplate;
            } else {
                if ($ruleTemplate['FloorPrice'] <= $budget && $ruleTemplate['CeillingPrice'] == '') {
                    // Budget in rule without upper limit
                    return $ruleTemplate;
                }
            }
        }
    }

    public function duplicateTemplateRule($ruleID)
    {
        $ruleTemplate = $this->getRuleTemplateInfo($ruleID);

        $fieldNameArr = array(
            'Code',
            'FloorPrice',
            'CeillingPrice',
            'ApprovalRoleID',
            'NeedGroupHeadApprove',
            'NeedVicePrincipalApprove',
            'QuotationType',
            'MinQuotation',
            'QuotationApprovalRoleID',
            'TenderAuditingRoleID',
            'TenderApprovalRoleID',
            'IsTemplate',
            'ApprovalMode',
            'CaseApprovalRule',
            'CustCaseApprovalRule',
            'CustApprovalMode'
        );
        $multiRowValueArr = array(
            array(
                $ruleTemplate[0]['Code'],
                $ruleTemplate[0]['FloorPrice'],
                $ruleTemplate[0]['CeillingPrice'],
                $ruleTemplate[0]['ApprovalRoleID'],
                $ruleTemplate[0]['NeedGroupHeadApprove'],
                $ruleTemplate[0]['NeedVicePrincipalApprove'],
                $ruleTemplate[0]['QuotationType'],
                $ruleTemplate[0]['MinQuotation'],
                $ruleTemplate[0]['QuotationApprovalRoleID'],
                $ruleTemplate[0]['TenderAuditingRoleID'],
                $ruleTemplate[0]['TenderApprovalRoleID'],
                0,
                $ruleTemplate[0]['ApprovalMode'],
                $ruleTemplate[0]['CaseApprovalRule'],
                $ruleTemplate[0]['CustCaseApprovalRule'],
                $ruleTemplate[0]['CustApprovalMode']
            )
        );
        $success = $this->insertData('INTRANET_PCM_RULE', $fieldNameArr, $multiRowValueArr);
        $newRuleID = ($success) ? $this->db_insert_id() : '';
        return $newRuleID;
    }

    public function getGroupHead($groupID)
    {
        $allMemberInfo = $this->getGroupMemberInfo($groupID);
        $groupHeadAry = array();
        foreach ($allMemberInfo as $_member) {
            if ($_member['IsGroupHead']) {
                $groupHeadAry[] = $_member;
            }
        }
        return $groupHeadAry;
    }

    public function insertCaseApproval($caseID, $userIDAry, $approvalType, $roleId, $sequenceNo)
    {
        //$approvalType => G: Group; R: Role
        $fieldNameArr = array(
            'CaseID',
            'ApproverUserID',
            'RecordStatus',
            'ApprovalType',
            'ApprovalRoleID',
            'ApprovalSequence'
        );
        $multiRowValueArr = array();
        foreach ((array)$userIDAry as $_userID) {
            $multiRowValueArr[] = array($caseID, $_userID, 0, $approvalType, $roleId, $sequenceNo);
        }
        return $this->insertData('INTRANET_PCM_CASE_APPROVAL', $fieldNameArr, $multiRowValueArr);
    }

    public function insertCustCaseApproval($caseID, $userIDAry, $approvalType, $roleId, $sequenceNo)
    {
        //$approvalType => G: Group; R: Role
        $fieldNameArr = array(
            'CaseID',
            'ApproverUserID',
            'RecordStatus',
            'ApprovalType',
            'ApprovalRoleID',
            'ApprovalSequence'
        );
        $multiRowValueArr = array();
        foreach ((array)$userIDAry as $_userID) {
            $multiRowValueArr[] = array($caseID, $_userID, 0, $approvalType, $roleId, $sequenceNo);
        }
        return $this->insertData('INTRANET_PCM_CUST_CASE_APPROVAL ', $fieldNameArr, $multiRowValueArr);
    }
// 	public function getViewableCaseInfo($keyword=''){
// 		global $UserID,$_PAGE;

// 		$libPCM_auth = $_PAGE['Controller']->requestObject('libPCM_auth','includes/ePCM/libPCM_auth.php');

// 		$caseInfoAry = $this->getCaseInfo('','',$keyword);
// 		$myGroupsAry = $this->getGroupInfoByMemberUserID(); //Get users groups
// 		$myGroupsAry = Get_Array_By_Key($myGroupsAry,'GroupID');
// // 		debug_pr($myGroupsAry);
// 		$today = date('Y-m-d',strtotime(date('Y-m-d').'+0 days'));
// //		debug_pr($myGroupsAry);
// 		foreach($caseInfoAry as $key => $_case){
// 			//Logic 1: if case is draft(stage=0) => only owner is viewable
// 			if($_case['CurrentStage']==0){
// 				if($_case['ApplicantUserID']!=$UserID){
// 					unset($caseInfoAry[$key]);
// 				}
// 			}
// 			//Logic 2: if case is public (stage>0) AND user not belongs to (a.you & b.your group & c.approver)
// 			if($libPCM_auth->checkCaseAccessRight($_case['CaseID'])){ //a. you
// // 					debug_pr($_case);
// 			}else{
// 				unset($caseInfoAry[$key]);
// 			}
// 			//Unset finished Case where dateModified is not today
// 			if($_case['CurrentStage']==5){
// 				// is finished
// 				if(strtotime($_case['DateModified'])<strtotime($today)){
// 					unset($caseInfoAry[$key]);
// 				}
// 			}

// 		}
// 		return $caseInfoAry;
// 	}
    public function getViewableCaseInfo(
        $keyword = '',
        $currentStage = '',
        $quotationType = '',
        $academicYear = '',
        $endorsementStatus = ''
    ) {
        global $UserID, $sys_custom;
        $caseInfoAry = $this->getCaseInfo('', '', $keyword, 0, $currentStage, $quotationType);
		if($currentStage == 6){
            $caseInfoAry2 = $this->getCaseInfo('', '', $keyword, 0, 5, $quotationType);
            $caseInfoAry = array_merge($caseInfoAry,$caseInfoAry2);
        }
        $caseInfoAry = BuildMultiKeyAssoc($caseInfoAry, 'CaseID');
        $caseUserRoleAry = $this->getCaseUserRole();
        $caseUserRoleAry = BuildMultiKeyAssoc($caseUserRoleAry, 'CaseID');

// 		$today = date('Y-m-d',strtotime(date('Y-m-d').'+0 days'));
        foreach ($caseInfoAry as $_caseID => $_case) {
            //Logic 1: if case is draft(stage=0) => only owner is viewable
            if ($_case['CurrentStage'] == 0) {
                if ($_case['ApplicantUserID'] != $UserID) {
                    unset($caseInfoAry[$_caseID]);
                }
            }
            //Logic 2: if case is public (stage>0) AND user not belongs to (a.you & b.your group & c.approver)
            //skip this logic if isAdmin
// 			debug_pr($caseUserRoleAry[34]);
            if (!$this->isAdmin()) {
                if ($caseUserRoleAry[$_caseID]['isApplicant'] || $caseUserRoleAry[$_caseID]['isGroupMember'] || $caseUserRoleAry[$_caseID]['isApprover'] || $caseUserRoleAry[$_caseID]['isTenderAuditing'] || $caseUserRoleAry[$_caseID]['isTenderApproval'] || $caseUserRoleAry[$_caseID]['isQuotationApproval'] || $caseUserRoleAry[$_caseID]['isCustApprover']) {
                    // do nothing
                } else {
                    unset($caseInfoAry[$_caseID]);
                }
            }
            //Unset finished Case where dateModified is not today
            if ($_case['CurrentStage'] == 10) {
                // is finished
// 				if(strtotime($_case['DateModified'])<strtotime($today)){
// 					unset($caseInfoAry[$_caseID]);
// 				}
                ######	20160715	Cancel logic of keep finished case for one day in case view
                unset($caseInfoAry[$_caseID]);
            }

            if ($academicYear != '') {
                if ($academicYear == $_case['AcademicYearID']) {
                    //do nothing
                } else {
                    unset($caseInfoAry[$_caseID]);
                }
            }

            if ($sys_custom['ePCM_skipCaseApproval'] || $sys_custom['ePCM']['extraApprovalFlow']) {
                if ($endorsementStatus != '') {
//                    debug_pr($this->getCaseApprovalStatusToCase($_caseID,$UserID,$isEndorsement=true));debug_pr('$endorsementStatus'.$endorsementStatus);//die();
                    $CaseEndorsementStatus = $this->getCaseApprovalStatusToCase($_caseID, $UserID,$isEndorsement = true);
                    if ($endorsementStatus !=
                        $CaseEndorsementStatus ) {
                        unset($caseInfoAry[$_caseID]);
                    }
                }
            }
        }


        return $caseInfoAry;
    }

    public function sendCaseApproverInvitation($caseID)
    {
        global $_PAGE;
        $json = $_PAGE['Controller']->requestObject('JSON_obj', 'includes/json.php');

        $caseInfoAry = $this->getCaseInfo($caseID, '', '', 0);
        $approvalRuleArr = $json->decode($caseInfoAry[0]['CaseApprovalRule']);

        $sequenceNo = 1;
        $involvedRoleIdArr = array();
        $groupHeadAry = array();
        $roleUserAry = array();
        $approvalUsersInfo = array();
        foreach ((array)$approvalRuleArr as $_roleId => $_approveNum) {
            if ($_roleId == -1) {
                // group head
                $groupHeadAry = $this->getGroupHead($caseInfoAry[0]['ApplicantGroupID']);
                $groupHeadUserIDAry = Get_Array_By_Key($groupHeadAry, 'UserID');
                $this->insertCaseApproval($caseID, $groupHeadUserIDAry, '', $_roleId, $sequenceNo);
            } else {
                // other role
                $roleUserAry = $this->getRoleUser($_roleId);
                $approvalUsersInfo = array_merge($approvalUsersInfo, $roleUserAry);
                $roleUserIDAry = Get_Array_By_Key($roleUserAry, 'UserID');
                $this->insertCaseApproval($caseID, $roleUserIDAry, '', $_roleId, $sequenceNo);
                $involvedRoleIdArr[] = $_roleId;
            }
            if ($caseInfoAry[0]['ApprovalMode'] == 'S') {
                $sequenceNo++;
            }
        }

        //Get Group Name & Role Name
        $groupName = $caseInfoAry[0][Get_Lang_Selection('GroupNameChi', 'GroupName')];
        $roleID = $caseInfoAry[0]['ApprovalRoleID'];
        $ruleInfo = BuildMultiKeyAssoc($this->getRoleInfo($involvedRoleIdArr), 'RoleID');

        $_PAGE['views_data']['caseID'] = $caseID;
        $_PAGE['views_data']['code'] = $caseInfoAry[0]['Code'];
        $_PAGE['views_data']['approvalGroupHeadAry'] = $groupHeadAry;
        $_PAGE['views_data']['approvalRoleUserAry'] = $approvalUsersInfo;
        $_PAGE['views_data']['roleInfo'] = $ruleInfo;
        $_PAGE['views_data']['approvalRule'] = $approvalRuleArr;

    }

    public function sendCustCaseApproverInvitation($caseID)
    {
        global $_PAGE;
        $json = $_PAGE['Controller']->requestObject('JSON_obj', 'includes/json.php');

        $caseInfoAry = $this->getCaseInfo($caseID, '', '', 0);

        // if not set cust rule follow normal approval rules
        if (!empty($caseInfoAry[0]['CustCaseApprovalRule'])) {
            $approvalRuleArr = $json->decode($caseInfoAry[0]['CustCaseApprovalRule']);
        } else {
            $approvalRuleArr = $json->decode($caseInfoAry[0]['CaseApprovalRule']);
        }

        $sequenceNo = 1;
        $involvedRoleIdArr = array();
        $groupHeadAry = array();
        $roleUserAry = array();
        $approvalUsersInfo = array();
        foreach ((array)$approvalRuleArr as $_roleId => $_approveNum) {
            if ($_roleId == -1) {
                // group head
                $groupHeadAry = $this->getGroupHead($caseInfoAry[0]['ApplicantGroupID']);
                $groupHeadUserIDAry = Get_Array_By_Key($groupHeadAry, 'UserID');
                $this->insertCustCaseApproval($caseID, $groupHeadUserIDAry, '', $_roleId, $sequenceNo);
            } else {
                // other role
                $roleUserAry = $this->getRoleUser($_roleId);
                $approvalUsersInfo = array_merge($approvalUsersInfo, $roleUserAry);
                $roleUserIDAry = Get_Array_By_Key($roleUserAry, 'UserID');
                $this->insertCustCaseApproval($caseID, $roleUserIDAry, '', $_roleId, $sequenceNo);
                $involvedRoleIdArr[] = $_roleId;
            }
            if ($caseInfoAry[0]['CustApprovalMode'] == 'S') {
                $sequenceNo++;
            }
        }

        //Get Group Name & Role Name
        $groupName = $caseInfoAry[0][Get_Lang_Selection('GroupNameChi', 'GroupName')];
        $roleID = $caseInfoAry[0]['ApprovalRoleID'];
        $ruleInfo = BuildMultiKeyAssoc($this->getRoleInfo($involvedRoleIdArr), 'RoleID');
    }

    public function getCaseQuotationType($caseID)
    {
        $caseInfoAry = $this->getCaseInfo($caseID);
        $quotationType = $caseInfoAry[0]['QuotationType'];
        return $quotationType;
    }

    public function checkIsCaseFinishedAllStage($caseID)
    {
        /**
         * Rule 1 : if QuotationType N && currentStage = 2 => finished
         */
        $caseInfoAry = $this->getCaseInfo($caseID);
        $quotationType = $caseInfoAry[0]['QuotationType'];
        $currentStage = $caseInfoAry[0]['CurrentStage'];

        //Rule 1
        if ($quotationType == 'N') {    //No quotation needed
            if ($currentStage == 2) {
                return true;
            }
        }
        return false;
    }

    public function updateCaseApprovalDate($caseID)
    {
        $changefieldNameArr = array('CaseApprovedDate');
        $timeNow = date("Y-m-d H:i:s");
        $multiRowValueArr = array(array($timeNow));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function setQuotationDates($caseID)
    {
        $timeNow = date("Y-m-d H:i:s");
        $endDate = date('Y-m-d', strtotime('+28 day', strtotime($timeNow)));
        $changefieldNameArr = array('QuotationStartDate', 'QuotationEndDate');
        $multiRowValueArr = array(array($timeNow, $endDate));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function insertQuotation($caseID, $supplierIDAry)
    {
        //New quotation record
        $fieldNameArr = array('CaseID', 'SupplierID', 'ReplyStatus');
        $multiRowValueArr = array();
        foreach ($supplierIDAry as $_supplierID) {
            $multiRowValueArr[] = array($caseID, $_supplierID, 0);
        }
        return $this->insertData('INTRANET_PCM_CASE_QUOTATION', $fieldNameArr, $multiRowValueArr);
    }

    public function getValidQuotation($caseID, $includeNonResponse = false)
    {
        // quoteDate != ''
        $quotations = $this->getCaseQuotation($caseID);
        $validQuotations = array();
        foreach ($quotations as $_quotation) {
            if ($includeNonResponse) {
                $validQuotations[] = $_quotation;
            } else {
                if ($_quotation['QuoteDate'] != '') {
                    $validQuotations[] = $_quotation;
                }
            }
        }
        return $validQuotations;
    }

    public function updateQuoteReplyStatus($quotationID, $replyStatus, $firstTimeUpdate=false)
    {

        $timeNow = date("Y-m-d H:i:s");
        if ($replyStatus == 0) {
            $timeNow = null;
        }
        if($firstTimeUpdate){
            $changefieldNameArr = array('ReplyStatus', 'ReplyDate', 'LastReplyDate', 'InputReplyDate');
            $multiRowValueArr = array(array($replyStatus, $timeNow, $timeNow, $timeNow));
        }else{
            $changefieldNameArr = array('ReplyStatus', 'ReplyDate', 'LastReplyDate');
            $multiRowValueArr = array(array($replyStatus, $timeNow, $timeNow));
        }

        $conditions = " QuotationID = '$quotationID'";
        return $this->updateData('INTRANET_PCM_CASE_QUOTATION', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateQuoteInputReplyDate($quotationID, $inputReplyDate)
    {
        (!empty($inputReplyDate)||$inputReplyDate!='')?$inputReplyDate:'';
        $changefieldNameArr = array('ReplyDate');
        $multiRowValueArr = array(array($inputReplyDate));

        $conditions = " QuotationID = '$quotationID'";
        return $this->updateData('INTRANET_PCM_CASE_QUOTATION', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateQuotationEndDate($caseID, $date)
    {
        $changefieldNameArr = array('QuotationEndDate');
        $multiRowValueArr = array(array($date));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateTenderOpeningStartDate($caseID, $date)
    {
        $changefieldNameArr = array('TenderOpeningStartDate');
        $multiRowValueArr = array(array($date));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateTenderOpeningEndDate($caseID, $date)
    {
        $changefieldNameArr = array('TenderOpeningEndDate');
        $multiRowValueArr = array(array($date));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateTenderApprovalStartDate($caseID, $date)
    {
        $changefieldNameArr = array('TenderApprovalStartDate');
        $multiRowValueArr = array(array($date));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateTenderApprovalEndDate($caseID, $date)
    {
        $changefieldNameArr = array('TenderApprovalEndDate');
        $multiRowValueArr = array(array($date));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function updateContactMethod($quotationID, $contactMethod)
    {
        $changefieldNameArr = array('ContactMethod');
        $multiRowValueArr = array(array($contactMethod));
        $conditions = " QuotationID = '$quotationID'";
        return $this->updateData('INTRANET_PCM_CASE_QUOTATION', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function saveNotificationSchedule($caseID, $stageID, $scheduleDateTime, $messageTitle, $messageContent)
    {
        //check is data exist
        $returnSet = $this->getPushNotificationSchedule($caseID, $stageID);
        if (empty($returnSet)) {
            //insert
            $fieldNameArr = array(
                'CaseID',
                'StageID',
                'MessageTitle',
                'MessageContent',
                'ScheduledDate',
                'RecordStatus'
            );
            $multiRowValueArr = array(array($caseID, $stageID, $messageTitle, $messageContent, $scheduleDateTime, 1));
            return $this->insertData($tableName = 'INTRANET_PCM_NOTIFICATION_SCHEDULE', $fieldNameArr,
                $multiRowValueArr);
        } else {
            //update
            $changefieldNameArr = array('MessageTitle', 'MessageContent', 'ScheduledDate', 'RecordStatus');
            $multiRowValueArr = array(array($messageTitle, $messageContent, $scheduleDateTime, 1));
            $conditions = " CaseID = '$caseID' AND StageID='$stageID' ";
            return $this->updateData('INTRANET_PCM_NOTIFICATION_SCHEDULE', $changefieldNameArr, $multiRowValueArr,
                $conditions);
        }
    }

    public function writeCaseHistory($oldCaseID, $newCaseID)
    {
        /**
         * 1. Check perious caseID if any
         * 2. Check lastest version
         */
        $originalCaseID = $this->getOriginalCaseID($oldCaseID);
        $version = $this->getLastestVersion($originalCaseID);

        if ($version == '') {
            $version = 1;
        } else {
            if ($version >= 1) {
                // have previous case => delete old case

                $version++;
            }
        }
        if ($newCaseID != $oldCaseID) {
            $this->deleteCase($oldCaseID, $reason = "Replaced by CaseID = $newCaseID (auto gernated by systems)");
        }
        $fieldNameArr = array('OriginalCaseID', 'CaseID', 'Version');
        $multiRowValueArr = array(array($originalCaseID, $newCaseID, $version));

        $success = $this->insertData($tableName = 'INTRANET_PCM_CASE_HISTORY', $fieldNameArr, $multiRowValueArr);
        return $success;
    }

    // 2016-10-05	Villa Detele record in 	INTRANET_PCM_RESULT
    public function deleteResult($caseID)
    {
// 		$changefieldNameArr = array('IsDeleted');
// 		$multiRowValueArr = array(array(1));
// 		$conditions = " CaseID = '$caseID'";
        $primaryKeyName = 'CaseID';
        $primaryKeyValArr = $caseID;
        return $this->deleteData('INTRANET_PCM_RESULT ', $primaryKeyName, $primaryKeyValArr);

    }

    public function deleteCase($caseID, $reason)
    {
        $changefieldNameArr = array('IsDeleted', 'DeleteReason');
        $multiRowValueArr = array(array(1, $reason));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function undoDeleteCase($caseID)
    {
        $changefieldNameArr = array('IsDeleted');
        $multiRowValueArr = array(array(0));
        $conditions = " CaseID = '$caseID'";
        return $this->updateData('INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions, true);
    }

    public function duplicateRuleFile($templateRuleID, $newRuleID, $caseID){
        $ruleTemplateFiles = $this->getCaseAttachment($CaseID= -1, 0, $templateRuleID);
        $fieldNameArr = array('AttachmentID', 'StageID', 'CaseID', 'Type', 'RuleID');
        $multiRowValueArr = array();
        foreach($ruleTemplateFiles as $fileInfoAry){
            $fileValueAry = array(
                $fileInfoAry['AttachmentID'],
                $fileInfoAry['StageID'],
                $caseID,
                $fileInfoAry['Type'],
                $newRuleID
                );
            array_push($multiRowValueArr, $fileValueAry);
        }
            $success = $this->insertData(' INTRANET_PCM_CASE_ATTACHMENT', $fieldNameArr, $multiRowValueArr);
        return $success;
    }

    public function insertFileToCase($AttachmentIDArr, $CaseID, $stageID = '0', $type = '0', $ruleID='')
    {

        $result = $this->getFileById($AttachmentIDArr, " AND CaseID ='0' ");
        $newFileIDArr = Get_Array_By_Key($result, 'AttachmentID');
        if (!empty($newFileIDArr)) {
            $fieldNameArr = array('AttachmentID', 'StageID', 'CaseID', 'Type', 'RuleID');
            $multiRowValueArr = array();
            foreach ($newFileIDArr as $_ID) {
                $multiRowValueArr[] = array($_ID, $stageID, $CaseID, $type, $ruleID);
            }
        }
        $this->insertData('INTRANET_PCM_CASE_ATTACHMENT', $fieldNameArr, $multiRowValueArr,
            $excludeCommonField = false);

        // update INTRANET_PCM_ATTACHMENT
        $changefieldNameArr = array('CaseID');
        // new session token => caseID-stageID
        $multiRowValueArr = array();
        $multiRowValueArr[] = array($CaseID);
        $conditions = " AttachmentID IN ('" . implode("','", (array)$AttachmentIDArr) . "') ";
        return $this->updateData('INTRANET_PCM_ATTACHMENT', $changefieldNameArr, $multiRowValueArr, $conditions,
            $excludeCommonField = true);
    }

    public function insertFileToQuotation($AttachmentIDArr, $CaseID, $QuoID)
    {
        $result = $this->getFileById($AttachmentIDArr, " AND CaseID ='0' ");
        $newFileIDArr = Get_Array_By_Key($result, 'AttachmentID');
        if (!empty($newFileIDArr)) {
            $fieldNameArr = array('AttachmentID', 'QuotationID');
            $multiRowValueArr = array();
            foreach ((array)$newFileIDArr as $_ID) {
                $multiRowValueArr[] = array($_ID, $QuoID);
            }
        }
        $this->insertData('INTRANET_PCM_QUOTATION_ATTACHMENT', $fieldNameArr, $multiRowValueArr,
            $excludeCommonField = false);

        // update INTRANET_PCM_ATTACHMENT
        $changefieldNameArr = array('CaseID');
        // new session token => caseID-stageID
        $multiRowValueArr = array();
        $multiRowValueArr[] = array($CaseID);
        $conditions = " AttachmentID IN ('" . implode("','", (array)$AttachmentIDArr) . "') ";
        return $this->updateData('INTRANET_PCM_ATTACHMENT', $changefieldNameArr, $multiRowValueArr, $conditions,
            $excludeCommonField = true);
    }

    public function deleteQuotation($quotationID)
    {
        $changefieldNameArr = array('IsDeleted');
        $multiRowValueArr = array(array(1));
        $conditions = " QuotationID = '$quotationID'";
        return $this->updateData('INTRANET_PCM_CASE_QUOTATION', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function insertPriceItem($caseID, $newItemNameAry)
    {
        $fieldNameArr = array('CaseID', 'PriceItemName');
        $multiRowValueArr = array();
        foreach ((array)$newItemNameAry as $_newItemName) {
            $_rowValueAry = array($caseID, $_newItemName);
            $multiRowValueArr[] = $_rowValueAry;
        }

        $success = $this->insertData('INTRANET_PCM_CASE_PRICEITEM', $fieldNameArr, $multiRowValueArr);
        $newID = ($success) ? $this->db_insert_id() : '';

        $returnIdAry = array();
        $counter = 0;
        foreach ((array)$newItemNameAry as $_newItemName) {
            $returnIdAry[] = $newID + $counter++;
        }
        return $returnIdAry;
    }

    public function deletePriceItem($priceItemID)
    {
        $changefieldNameArr = array('IsDeleted');
        $multiRowValueArr = array(array(1));
        $conditions = " PriceItemID = '$priceItemID'";
        return $this->updateData('INTRANET_PCM_CASE_PRICEITEM', $changefieldNameArr, $multiRowValueArr, $conditions);
    }

    public function insertQuotationPriceItem($priceItemID, $quotationID, $price)
    {
        $fieldNameArr = array('PriceItemID', 'QuotationID', 'Price');
        if ($price === '') {
            $price = 'NULL';
        } else {
            $price = "'" . $price . "'";
        }
        $sql = "INSERT INTO INTRANET_PCM_PRICEITEM_QUOTATION (PriceItemID,QuotationID,Price) VALUES ('$priceItemID','$quotationID',$price)";
        return $this->db_db_query($sql);
    }

    public function updateQuotationPriceItem($quotationID, $priceItemID, $price)
    {
        if ($price === '') {
            $price = 'NULL';
        }
        $conditions = " QuotationID = '$quotationID' AND PriceItemID ='$priceItemID' ";
        $sql = "UPDATE INTRANET_PCM_PRICEITEM_QUOTATION SET Price = $price WHERE $conditions";
        return $this->db_db_query($sql);
    }

    public function updateCaseCodeAndName($caseID, $code, $name)
    {
        $changefieldNameArr = array('Code', 'CaseName');
        $multiRowValueArr = array(array($code, $name));
        $conditions = "CaseID = '$caseID'";
        return $this->updateData($tableName = 'INTRANET_PCM_CASE', $changefieldNameArr, $multiRowValueArr, $conditions,
            $excludeCommonField = false);
    }

    public function sortingApproverForDisplayByRoleType($caseApprovalAry)
    {
        global $_PAGE;
        $caseApprovalAssoc = BuildMultiKeyAssoc($caseApprovalAry, 'ApprovalType', array(), 0, 1);
        $checkingSequence = $_PAGE['libPCM']->GetRoleSequence();
        $rearrangedArr = array();
        foreach ((array)$checkingSequence as $_roleType) {
            foreach ((array)$caseApprovalAssoc[$_roleType] as $_approverArr) {
                $rearrangedArr[] = $_approverArr;
            }
        }
        return $rearrangedArr;
    }

    public function checkCurrentApprovalSequence($ruleArr, $approvalArr)
    {

        $approvalStatusAssoc = BuildMultiKeyAssoc($approvalArr, 'ApprovalRoleID', array(), 0, 1);

        $result = 0;
        $done = true;
        foreach ($ruleArr as $_roleId => $_approvalNoRequire) {

            if (!empty($approvalStatusAssoc[$_roleId])) {
                $_thisRoleApproved = 0;
                $numOfPeopleInThisRole = count($approvalStatusAssoc[$_roleId]);
                foreach ($approvalStatusAssoc[$_roleId] as $__approvalInfo) {
                    $result = $__approvalInfo['ApprovalSequence'];
                    if ($__approvalInfo['RecordStatus'] == 1) {
                        $_thisRoleApproved++;
                    }
                }

                // if $_approvalNoRequire = 0 . need all people approve to be ok
                if ($_approvalNoRequire == 0) {
                    $_approvalNoRequire = $numOfPeopleInThisRole;
                }

                if ($_thisRoleApproved >= $_approvalNoRequire) {
                    // continue = ok, proceed to next role to approve
                    continue;
                } else {
                    // if role no of people last than the requirement
                    // all people in that role approve => ok
                    if ($numOfPeopleInThisRole < $_approvalNoRequire && $numOfPeopleInThisRole == $_thisRoleApproved) {
                        continue;
                    } else {
                        $done = false;
                        break;
                    }
                }
            }
        }
        if ($done) {
            $result++;
        }
        return $result;
    }

    public function sendNotificationToApprovalRole($sequenceNo, $caseID, $caseCode)
    {

        global $_PAGE, $Lang;
        $approverArr = BuildMultiKeyAssoc($this->getCaseApproval($caseID), 'ApprovalSequence', array(), 0, 1);

        $db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');

        $caseInfo = $db->getCaseInfo($caseID);
        $caseAry = $caseInfo[0];
        $caseName = $caseAry['CaseName'];

        $result = array();
        foreach ((array)$sequenceNo as $_type) {
            $sendToUserIDArr = Get_Array_By_Key($approverArr[$_type], 'ApproverUserID');
            if (!empty($sendToUserIDArr)) {
                $ToArray = $sendToUserIDArr;
                $individualMessageInfoAry = array();
                foreach ($ToArray as $to) {
                    $Subject = str_replace('<!--CASE_CODE-->',$caseCode,$Lang['ePCM']['Mgmt']['Case']['PushMessage']['Title']);
                    $Message = str_replace('<!--CASE_NAME-->',$caseName,str_replace('<!--CASE_CODE-->',$caseCode,$Lang['ePCM']['Mgmt']['Case']['PushMessage']['Content']));
                    $info_array = array(
                        'relatedUserIdAssoAry' => array($to => $to),
                    );
                    array_push($individualMessageInfoAry, $info_array);
                }

                $result[] = $_PAGE['libPCM']->sendPushMessage($individualMessageInfoAry, $Subject, $Message);
            }
        }
        return $result;
    }

    public function getSupplierTypeInfoAssoArrForDropDownList($excludeEmptySupplierType=false)
    {
        global $_PAGE, $Lang;

        if (!$_PAGE['libPCM']->isAdmin()) {
            $thisUserGroupIds = Get_Array_By_Key($this->getGroupInfoByMemberUserID(), 'GroupID');
        }

        $supplierTypeInfoAry = $this->getSupplyTypeInfo($isDeleted=0,$typeID='',$excludeEmptySupplierType);
        foreach ($supplierTypeInfoAry as $_suppliertype) {
            if ($_PAGE['libPCM']->enableGroupSupplierType()) {
                // flag enable feature: group created supplier type
                $relatedGroupID = $_suppliertype['GroupMappingID'];
                if ($relatedGroupID > 0) {
                    // group created type
                    $relatedGroupInfo = $this->getGroupInfo($relatedGroupID);
                    $relatedGroupInfo = $relatedGroupInfo[0];
                    $groupName = $Lang['ePCM']['Mgmt']['Case']['Group'] . ' (' . Get_Lang_Selection($relatedGroupInfo['GroupNameChi'],
                            $relatedGroupInfo['GroupName']) . ') ' . $Lang['ePCM']['Mgmt']['Case']['Supplier']['GroupType'];

                } else {
                    // school created type
                    $groupName = $Lang['ePCM']['Mgmt']['Case']['Supplier']['SchoolType'];
                }

                if ($relatedGroupID == 0 || $_PAGE['libPCM']->isAdmin()) {
                    $supplierTypeAry[$groupName][$_suppliertype['TypeID']] = Get_Lang_Selection($_suppliertype['TypeNameChi'],
                        $_suppliertype['TypeName']);
                } else {
                    if (in_array($relatedGroupID, (array)$thisUserGroupIds)) {
                        $supplierTypeAry[$groupName][$_suppliertype['TypeID']] = Get_Lang_Selection($_suppliertype['TypeNameChi'],
                            $_suppliertype['TypeName']);
                    }
                }
            } else {
                // normal case
                $supplierTypeAry[$_suppliertype['TypeID']] = Get_Lang_Selection($_suppliertype['TypeNameChi'],
                    $_suppliertype['TypeName']);
            }
        }

        return $supplierTypeAry;
    }

    public function updateFundingCaseRelation($CaseID, $FundingID, $FundingBudgetUse, $RecordStatus)
    {
        global $_PAGE, $Lang;
        $check = $this->getFundingCaseRelationRawData($CaseID, $FundingID);
        $tablename = "INTRANET_PCM_FUNDING_SOURCE_CASE_RELATION";
        if ($check) {//update
            //skip the insert
            $fieldName = array('CaseID', 'FundingID', 'Amount', 'RecordStatus');
            $multiRowValueArr = array();
            $tempArr[0] = $CaseID;
            $tempArr[1] = $FundingID;
            $tempArr[2] = $FundingBudgetUse;
            $tempArr[3] = $RecordStatus;
            $multiRowValueArr[] = $tempArr;
// 			$cond = " CaseID = $CaseID AND FundingID = $FundingID ";
            $cond = " FundingSourceCaseMappingID = '" . $check[$FundingID][0]['FundingSourceCaseMappingID'] . "'";
            $this->updateData($tablename, $fieldName, $multiRowValueArr, $cond);
        } else {//insert
            $fieldName = array('CaseID', 'FundingID', 'Amount', 'RecordStatus');
            $multiRowValueArr = array();
            $tempArr[0] = $CaseID;
            $tempArr[1] = $FundingID;
            $tempArr[2] = $FundingBudgetUse;
            $tempArr[3] = $RecordStatus;
            $multiRowValueArr[] = $tempArr;
            $this->insertData($tablename, $fieldName, $multiRowValueArr);
        }

    }

    public function updateFundingCaseRelationByCase($CaseID, $RecordStatus)
    {
        global $_PAGE, $Lang;
        $tablename = "INTRANET_PCM_FUNDING_SOURCE_CASE_RELATION";
        $fieldName = array('RecordStatus');
        $multiRowValueArr = array();
        $tempArr[0] = $RecordStatus;
        $multiRowValueArr[] = $tempArr;
        $cond = " CaseID = " . $CaseID;
        $this->updateData($tablename, $fieldName, $multiRowValueArr, $cond);
    }

    public function getFundingCaseArr($FundingID = '', $CaseID = '')
    {
        global $_PAGE, $Lang;
        $FundingRawData = $this->getFundingRawData($FundingID);
        $FundingCaseRawData = $this->getFundingCaseRelationRawData();
        foreach ((array)$FundingRawData as $FundingID => $_Funding) {
            $FundingData[$FundingID] = $_Funding;
            $BudgetRemain = $_Funding['Budget'];
            foreach ((array)$FundingCaseRawData[$FundingID] as $_Case) {
                $FundingData[$FundingID]['Case'][] = $_Case;
                if ($_Case['RecordStatus'] == '0' || $_Case['RecordStatus'] == '1') {
                    $BudgetRemain = $BudgetRemain - $_Case['Amount'];
                }
            }
            $FundingData[$FundingID]['BugetRemain'] = number_format($BudgetRemain, 2, '.', '');
        }
        return $FundingData;
    }

    public function deleteFundingCaseRelation($CaseID, $FundingID)
    {
        global $_PAGE;
        //delete FundingCaseRelation
        $check = $this->getFundingCaseRelationRawData($CaseID, $FundingID);
        $FundingSourceCaseMappingID = $check[$FundingID][0]['FundingSourceCaseMappingID'];

        $tablename = "INTRANET_PCM_FUNDING_SOURCE_CASE_RELATION";
        $keyName = "FundingSourceCaseMappingID";
        $keyValArray = $FundingSourceCaseMappingID;

        return $this->deleteData($tablename, $keyName, $keyValArray);

    }

    public function checkIfExceedBudget($groupID, $itemID, $budget)
    {
        $groupInfo = $this->getGroupBudget($groupID);
        $groupBudget = $groupInfo[0]['Budget'];
        $usedAmount_group = $this->getGroupSpendingUsed($groupID);
        $groupBudgetLeft = $groupBudget - $usedAmount_group;

        //Get Finiancial Budget
        $financialBudget = $this->getFinancialItemBudget($itemID);
        $usedAmount_item = $this->getItemSpendingUsed($itemID);
        $itemBudgetLeft = $financialBudget - $usedAmount_item;

        //Get Setting -> exceed_budget_percentage
        $settings = $_SESSION['ePCM']['Setting']['General'];
        $exceed_budget_percentage = $settings['exceed_budget_percentage'];
        $exceed_budget_percentage = floatval($exceed_budget_percentage);

        $over_budget_group = false;
        $over_budget_item = false;
        if ($groupBudgetLeft * (1 + $exceed_budget_percentage) < $budget) {
            $over_budget_group = true;
        }
        if ($itemBudgetLeft * (1 + $exceed_budget_percentage) < $budget) {
            $over_budget_item = true;
        }

        if ($over_budget_group && $over_budget_item) {
            $result = 'GROUP:ITEM';
        } else {
            if ($over_budget_group) {
                $result = 'GROUP';
            } else {
                if ($over_budget_item) {
                    $result = 'ITEM';
                } else {
                    $result = 'OK';
                }
            }
        }
        return $result;
    }

    public function getCaseCurrentApprovalSequence($CaseID, $extraApprovalFlow=false){
        global $_PAGE, $intranet_root;
        $caseInfoAry = $this->getCaseInfo($CaseID,'','','');
        $thisRule = $this->getRuleInfo($caseInfoAry[0]['RuleID']);
        $thisRule = $thisRule[0];
//        $json = $_PAGE['Controller']->requestObject('JSON_obj',$intranet_root.'/includes/json.php');
//        $approvalRule = $json->decode($thisRule['CaseApprovalRule']);
        include_once($intranet_root.'/includes/json.php');
        $jsonObj = new JSON_obj();
        $approvalRule = $jsonObj->decode($thisRule['CaseApprovalRule']);

        $approvalSorting = (($caseInfoAry[0]['ApprovalMode'] == 'T')? 'ApprovalRoleID asc' : 'ApprovalSequence asc').", ApproverName asc";
        $extraApprovalFlow? $caseApproval = $this->getCustCaseApproval($CaseID,'','',$approvalSorting): $caseApproval = $this->getCaseApproval($CaseID,'','',$approvalSorting);
        $CurrentApprovalSequence = $this->checkCurrentApprovalSequence($approvalRule, $caseApproval);
        return $CurrentApprovalSequence;
    }

    public function getCaseApprovalStatusToCase($CaseID, $UserID, $isEndorsement = '')
    {
        global $sys_custom, $_PAGE;
        $libPCM_auth = $_PAGE['Controller']->requestObject('libPCM_auth','includes/ePCM/libPCM_auth.php');

        if ($isEndorsement != '' && $sys_custom['ePCM']['extraApprovalFlow']) {
            $CaseCurrentApprovalSequence = $this->getCaseCurrentApprovalSequence($CaseID, true);
            $approvalInfoAry = $this->getCustCaseApproval($CaseID, $UserID);
            if (!($libPCM_auth->checkCustCaseApprovalRight($CaseID,$UserID)) || $approvalInfoAry[0]['ApprovalSequence']>$CaseCurrentApprovalSequence){
                return;
            }
            return $approvalInfoAry[0]['RecordStatus'];
        } else {
            $CaseCurrentApprovalSequence = $this->getCaseCurrentApprovalSequence($CaseID);
            $approvalInfoAry = $this->getCaseApproval($CaseID, $UserID);
            if (!($libPCM_auth->checkCaseApprovalRight($CaseID,$UserID)) || $approvalInfoAry[0]['ApprovalSequence']>$CaseCurrentApprovalSequence){
                return;
            }
            return $approvalInfoAry[0]['RecordStatus'];
        }
    }

    public function getUserNumberOfApproval($academicYearID)
    {
        global $UserID, $intranet_root;

        $sql = "select 
                    DISTINCT 
                    ica.CaseID,
                    ica.ApprovalSequence                                       
                FROM
                    INTRANET_PCM_CASE_APPROVAL as ica
                    LEFT JOIN INTRANET_PCM_CASE as ipc on (ipc.CaseID = ica.CaseID)                    
                WHERE 
                    ica.ApproverUserID = ".$UserID."  
                    AND ica.RecordStatus =0
                    AND ipc.IsDeleted = 0
                    AND (ipc.CurrentStage = 1 OR ipc.CurrentStage = 5)
                    AND ipc.AcademicYearID = ".$academicYearID."
          ";
        $ApprovalCaseAry = $this->returnResultSet($sql);

        $numCase = 0;
        foreach($ApprovalCaseAry as $ApprovalCaseInfoAry){
            $caseID = $ApprovalCaseInfoAry['CaseID'];
            $userCurrentApprovalSequence = $ApprovalCaseInfoAry['ApprovalSequence'];
            $CaseCurrentApprovalSequence = $this->getCaseCurrentApprovalSequence($caseID);
            $caseRejected = $this->isCaseRejected($caseID);
            if(($CaseCurrentApprovalSequence == $userCurrentApprovalSequence) && !$caseRejected)$numCase++;
        }
//        $numCase = count($ApprovalCaseAry);
        return $numCase;
    }

}
?>