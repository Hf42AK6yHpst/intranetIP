<?php
// using by : 

/**
 *  date: 2017-04-13 Villa #E114398 
 *  - Modified Get_Case_Detail_UI() - Add Print Btn
 *  - Modified getFundingByCategorySelectionBox - support Edit Mode
 *  
 *  date: 2017-04-05 Villa
 *  - Modified getFundingByCategorySelectionBox - add width
 *  - Modified Get_Case_Detail_UI - add flag to control ON/OFF funding source
 *  
 *  date: 2017-03-16 Villa
 *  - Modified getFundingByCategorySelectionBox - change wording
 *  
 * 	date: 2017-03-15 Villa
 *  - Add getFundingFilter() - filter Record Status
 *  
 *  date: 2017-03-10 Villa
 *  - Modified getFundingByCategorySelectionBox() - fitler out the funding which is not active/ hide the category with no active funding 
 *  
 *  date: 2017-03-07 Villa #K113812
 *  - Modified Get_Case_Detail_UI - add funding source
 *  
 *  date: 2017-03-06 Villa
 *  - K113812 add getFundingByCategorySelectionBox - Generate SelectionBox
 *  
 *  date: 2017-03-01 Villa
 *  - X100660 modified Get_User_Selection() - show SMC selection
 *  
 *  date: 2016-10-12 Villa
 *  - modified Get_Case_Detail_UI() - fix Reason cannot be displayed 
 *  
 *  date: 2016-10-07 Villa
 *  - modified Get_Case_Detail_UI() - allow display budget in float
 *  
 *  date: 2016-09-27 Omas 
 *  - modified Get_Case_Detail_UI() - fix 1970-01-01 problem
 *  
 *  Date: 2016-09-19 Omas
 *  - modified Get_Case_Detail_UI() - now admin can edit Price Comparison Table any time
 *  
 *  Date: 2016-09-12 Villa
 *  - modified Get_Case_Detail_UI() - add $fundingList_arr into casearr
 *  - modified Get_Case_Detail_UI() - display selected funding source and its unit price
 * 
 * Date: 2016-09-06 Omas
 * - modified Get_Case_Detail_UI() - remarks(reasons) only show when supplier number not enough
 *  
 * Date: 2016-08-29	Omas
 * - modified getRulesTable(), Get_Case_Detail_UI()
 * 
 * Date: 2016-08-24	Kenneth
 * - modified Get_Case_Detail_UI(), hide edit price item list for verbal quoatation
 * - modified Get_Case_Detail_UI(), Add flag, all Case approval changes to endorsment
 * - modified Get_Case_Summary_UI(), Add flag, all Case approval changes to endorsment
 * 
 * Date: 2016-07-15	Kenneth
 * - modified Get_Case_Detail_UI(), Lang fix for supplier
 * 
 * Date: 2016-07-06	Kenneth
 * - modified Get_Case_Detail_UI(), add email preview function for adding supplier
 * - modified Get_Case_Detail_UI(), shows related users of the case
 * 
 * Date: 2016-07-04	Kenneth
 * - modified Get_Case_Detail_UI(), add edit Case code and name function
 * 
 * Date:	2016-06-29	Kenneth
 * 	- modified Get_Case_Detail_UI() and Get_Case_Summary_UI(), skip stage 4 for vebal quotation
 */

include_once($intranet_root."/includes/libinterface.php");
include_once($intranet_root."/includes/".$ePCMcfg['ROLE_MANAGE']);
include_once($intranet_root."/includes/form_class_manage.php");
include_once($intranet_root."/includes/libuser.php");
include_once ($intranet_root."/includes/libgroup.php");

class libPCM_ui extends interface_html {
	
	public function __construct(){
		
		if($_SESSION['ePCM']['session_password']){
			parent::__construct();
		}
		else{
			parent::__construct("digital_archive_default.html");
		}
	}
	
	public function getRuleInstructionBox(){
		global $Lang;
        $contentMsg = '';
		$contentMsg .= '<ol>';
		$contentMsg .= '<li>'.$Lang['ePCM']['Setting']['Rule']['Warning']['Instruction'][0].'</li>';
		$contentMsg .= '<li>'.$Lang['ePCM']['Setting']['Rule']['Warning']['Instruction'][1].'</li>';
		$contentMsg .= '<li>'.$Lang['ePCM']['Setting']['Rule']['Warning']['Instruction'][2].'</li>';
		$contentMsg .= '</ol>';
		return $this->Get_Warning_Message_Box($Lang['General']['Instruction'], $contentMsg);
	}
	public function getRuleInvalidWarningBox($invalidAry=array(true,true,true,true)){
		global $Lang;
		if($invalidAry==array(false,false,false,false)){
			return '';
		}
		$contentMsg = $Lang['ePCM']['Setting']['Rule']['Warning']['SoemthingWrong'];
		$contentMsg .= '<ol>';
		$contentMsg .= ($invalidAry[0])?'<li>'.$Lang['ePCM']['Setting']['Rule']['Warning']['Instruction'][0].'</li>':'';
		$contentMsg .= ($invalidAry[1])?'<li>'.$Lang['ePCM']['Setting']['Rule']['Warning']['Instruction'][1].'</li>':'';
		if($invalidAry[2]){
// 			$contentMsg .= '<li>'.$Lang['ePCM']['Setting']['Rule']['Warning']['Instruction'][2].'</li>';
			if(!empty($invalidAry[2]['Missed'])){
				$contentMsg .= '<li>'.$Lang['ePCM']['Setting']['Rule']['Warning']['PriceBetween'].'(';
				$countOfMissed = count($invalidAry[2]['Missed']);
				foreach($invalidAry[2]['Missed'] as $i => $_missed){
					$contentMsg .= $this->displayMoneyFormat(($_missed['start']+1),0) .' - '.$this->displayMoneyFormat($_missed['end'],0);
					if($i!=$countOfMissed-1){
						$contentMsg .= ', ';
					}
				}
				$contentMsg .= ')'.$Lang['ePCM']['Setting']['Rule']['Warning']['PriceMissing'].'</li>';
			}
			
			if(!empty($invalidAry[2]['Overlapped'])){
				$contentMsg .= '<li>'.$Lang['ePCM']['Setting']['Rule']['Warning']['PriceBetween'].'(';
				$countOfMOverlapped = count($invalidAry[2]['Overlapped']);
				foreach($invalidAry[2]['Overlapped'] as $i => $_overlapped){
					$contentMsg .= $this->displayMoneyFormat(($_overlapped['start']),0) .' - '.$this->displayMoneyFormat($_overlapped['end'],0);
					if($i!=$countOfMOverlapped-1){
						$contentMsg .= ', ';
					}
				}
				$contentMsg .= ')'.$Lang['ePCM']['Setting']['Rule']['Warning']['PriceOverlapping'].'</li>';
			}
			
		}
		$contentMsg .= ($invalidAry[3])?'<li>'.$Lang['ePCM']['Setting']['Rule']['UnlimitedCeillingInNonLastRule'].'</li>':'';
		$contentMsg .= '</ol>';
		return $this->GET_WARNING_TABLE($err_msg='', $DeleteItemName='', $DeleteButtonName='', $contentMsg);
	}
	public function getStopNewApplicationWarningBox(){
		global $Lang;
		
		$contentMsg = $Lang['ePCM']['Mgmt']['Case']['Warning']['StopNewApplication'];
		$contentMsg .= '<ol>';
		$contentMsg .= '<li>'.$Lang['ePCM']['Mgmt']['Case']['Warning']['Reason']['RulesInvalid'].'</li>';
		$contentMsg .= '</ol>';
		return $this->GET_WARNING_TABLE($err_msg='', $DeleteItemName='', $DeleteButtonName='', $contentMsg);
	}
	public function getSupplierInfo($supplierInfo){
		global $Lang;
        $html = '';
		$html .= '<h3>'.$supplierInfo['SupplierName'].'</h3>';
		$html .= '<table>';
			$html .= '<tr>';
				$html .= '<td>'.$Lang['ePCM']['SettingsArr']['SupplierArr']['Type'].'</td>';
				$html .= '<td>:</td>';
				$html .= '<td>'.$supplierInfo['TypeName'].'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>'.$Lang['ePCM']['SettingsArr']['SupplierArr']['Website'].'</td>';
				$html .= '<td>:</td>';
				$html .= '<td><a href="'.$supplierInfo['Website'].'" target="_blank" >'.$supplierInfo['Website'].'</a></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>'.$Lang['ePCM']['SettingsArr']['SupplierArr']['Email'].'</td>';
				$html .= '<td>:</td>';
				$html .= '<td><a href="mailto:'.$supplierInfo['Email'].'" target="_blank" >'.$supplierInfo['Email'].'</a></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>'.$Lang['ePCM']['SettingsArr']['SupplierArr']['Phone'].'</td>';
				$html .= '<td>:</td>';
				$html .= '<td>'.$supplierInfo['Phone'].'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>'.$Lang['ePCM']['SettingsArr']['SupplierArr']['Fax'].'</td>';
				$html .= '<td>:</td>';
				$html .= '<td>'.$supplierInfo['Fax'].'</td>';
			$html .= '</tr>';
		$html .= '</table>';
		echo $html;
	}
	public function getTenderOpeningDatesForm($startDate,$endDate){
		global $Lang;
		if($startDate==''&&$endDate==''){
			$today = date('Y-m-d');
			$startDate = $today;
			$endDate = date('Y-m-d',strtotime($today.'+7 days'));
		}
        $html = '';
		$html .= '<table class="form_table_v30">';
			$html .= '<tr>';
				$html .= '<td class="field_title">'.$Lang['ePCM']['Mgmt']['Case']['TenderOpeningStartDate'].'</td>';
				$html .= '<td>'.$this->GET_DATE_PICKER('datePickerStart', $startDate).'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td class="field_title">'.$Lang['ePCM']['Mgmt']['Case']['TenderOpeningEndDate'].'</td>';
				$html .= '<td>'.$this->GET_DATE_PICKER('datePickerEnd', $endDate).'</td>';
			$html .= '</tr>';
		$html .= '</table>';
		$html .= '<div id="editBottomDiv" class="edit_bottom_v30">';
			$html .= '<p class="spacer"></p>';
			$html .= $this->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', $ParOnClick="submitTenderOpendingDates();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
			$html .= $this->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
			$html .= '<p class="spacer"></p>';
		$html .= '</div>';
		$html .= '<div id="ajax_set_tender_opening_dates"></div>';
		return $html;
	}
	public function getTenderApprovalDatesForm($startDate,$endDate){
		global $Lang;
		if($startDate==''&&$endDate==''){
			$today = date('Y-m-d');
			$startDate = $today;
			$endDate = date('Y-m-d',strtotime($today.'+7 days'));
		}
        $html = '';
		$html .= '<table class="form_table_v30">';
			$html .= '<tr>';
				$html .= '<td class="field_title">'.$Lang['ePCM']['Mgmt']['Case']['TenderApprovalStartDate'].'</td>';
				$html .= '<td>'.$this->GET_DATE_PICKER('datePickerStart', $startDate).'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td class="field_title">'.$Lang['ePCM']['Mgmt']['Case']['TenderApprovalEndDate'].'</td>';
				$html .= '<td>'.$this->GET_DATE_PICKER('datePickerEnd', $endDate).'</td>';
			$html .= '</tr>';
		$html .= '</table>';
		$html .= '<div id="editBottomDiv" class="edit_bottom_v30">';
			$html .= '<p class="spacer"></p>';
			$html .= $this->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', $ParOnClick="submitTenderApprovalDates();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
			$html .= $this->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
			$html .= '<p class="spacer"></p>';
		$html .= '</div>';
		$html .= '<div id="ajax_set_tender_approval_dates"></div>';
		return $html;
	}
	public function getFormTableHTML($tableContentArr){
		$html = '';
		$html .= '<table class="form_table_v30">';
		foreach((array)$tableContentArr as $key => $contentArr){
			$html .= '<tr>
						<td class="field_title">'.$contentArr['title'].'</td>
						<td>'.$contentArr['content'].'</td>
					</tr>';
		}
		$html .= '</table>';
		return $html;
	}
	
	public function getDbTable($sql, $columns, $field, $order, $pageNo, $page_size, $checkBox=true, $columnCustArr=''){
		
		global $intranet_root, $Lang, $_PAGE;
		
		if(count($columns)>0 && $sql != ''){
			include_once ($intranet_root . "/includes/libdbtable.php");
			include_once ($intranet_root . "/includes/libdbtable2007a.php");
			$li = new libdbtable2007($field, $order, $pageNo);
	
			
			$li->field_array = Get_Array_By_Key($columns,'0');
			if(is_array($columnCustArr)){
				// $columnCustArr = array(1,2,3,4,21)
				$li->column_array = $columnCustArr;
			}
			$li->sql = $sql;
			$li->no_col = sizeof($li->field_array) + 2;
			if(!$checkBox){
				$li->no_col --;
			}
			$li->IsColOff = "IP25_table";
			$li->page_size = $page_size;
			
			$pos = 0;
			$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
			foreach($columns as $_colInfo){
				$_fieldLang = $_colInfo[1];
				$_width = $_colInfo[2];
				$li->column_list .= "<th width='$_width' >".$li->column($pos++, $_fieldLang)."</th>\n";
			}
			if($checkBox){
				$li->column_list .= "<th width='1'>".$li->check("targetIdAry[]")."</th>\n";			
			}
		}
		return $li->display();
	}
	public function getTable($headingAry,$contentAry){
// 		$headingAry = array('1','2','3');
// 		$contentAry = array(array('a','b','c'),array('a','b','c'));
		global $Lang;
		$x = '';
		$x = '<form id="form1">';
		$x .= '<table class="common_table_list_v30">';
			$x .= '<tr class="tabletop">';
			foreach($headingAry as $_heading){
				$x .= '<th>'.$_heading.'</th>';
			}
			$x .= '</tr>';
			if(!empty($contentAry)){
				foreach($contentAry as $_row){
					$x .= '<tr>';
						foreach($_row as $_content){
							$x .= '<td>'.$_content.'</td>';
						}
					$x .= '</tr>';
				}
			}
			else{
				$x .= '<tr>';
				$x .= '<td class="tableContent" align="center" colspan="'.count($headingAry).'">'.$Lang['ePCM']['General']['NoRecord'].'</td>';
				$x .= '</tr>';
			}
		$x .= '</table>';
		$x .= '</form>';
		return $x;	
	}
	public function getRulesTable($ruleAry,$invalidAry=''){
		global $_PAGE,$Lang,$sys_custom;
		$json = $_PAGE['Controller']->requestObject('JSON_obj','includes/json.php');
		
		$headingAry = array();
		$headingAry[] = $Lang['General']['Code'];
		$headingAry[] = $Lang['ePCM']['Setting']['Rule']['FloorPrice'];
		$headingAry[] = $Lang['ePCM']['Setting']['Rule']['CeillingPrice'];
		if($sys_custom['ePCM']['extraApprovalFlow']){
			$headingAry[] = $Lang['ePCM']['Setting']['Rule']['CaseApproval'];
			$headingAry[] = $Lang['ePCM']['Mgmt']['Case']['Endorsement'];
		}else{
			$headingAry[] = $Lang['ePCM']['Setting']['Rule']['CaseApproval'] .' / '.$Lang['ePCM']['Mgmt']['Case']['Endorsement'];
		}
		$headingAry[] = $Lang['ePCM']['Setting']['Rule']['QuotationType'];
		$headingAry[] = $Lang['ePCM']['Setting']['Rule']['QuotationApproval'];
		$headingAry[] = $Lang['ePCM']['Setting']['Rule']['TenderAuditing'];
		$headingAry[] = $Lang['ePCM']['Setting']['Rule']['TenderApproval'];
		$headingAry[] = $Lang['ePCM']['Setting']['Rule']['MinInvitation'];
		//$headingAry[] = $Lang['ePCM']['Setting']['Rule']['NumTemplateFile'];
		$headingAry[] = $this->Get_Checkbox($ID='', $Name='checkmaster', $Value='', $isChecked=0, $Class='', $Display='', $Onclick="(this.checked)?setChecked(1,this.form,'targetIdAry[]'):setChecked(0,this.form,'targetIdAry[]')", $Disabled='');
		
		$quotationTypeDataAry = array(array('N',$Lang['ePCM']['Setting']['Rule']['NoNeed']),array('V',$Lang['ePCM']['Setting']['Rule']['Verbal']),array('Q',$Lang['ePCM']['Setting']['Rule']['Quotation']),array('T',$Lang['ePCM']['Setting']['Rule']['Tender']));

		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		
		// Get all role info -> for getting ro
		$roleInfoAry = $db->getRoleInfo($roleIDAry='', $ForCaseApproval='',$ForQuotationApproval='',$ForTenderAuditing='',$ForTenderApproval='',$isDeleted='0',$isPrincipal='');
		$roleInfoAry = BuildMultiKeyAssoc($roleInfoAry,'RoleID');

		$colWidthArr = array();
		$colWidthArr = array(5,13,13,19,10,10,10,10,10);
		if($sys_custom['ePCM']['extraApprovalFlow']){
			$colWidthArr = array(4,10,10,18,18,8,8,8,8,8);
		}
		$x = '';
		$x .= '<table class="common_table_list_v30">';
			$x .= '<colgroup>';
			foreach($colWidthArr as $_width){
				$x .= '<col width="'.$_width.'%" />';
			}
			$x .= '</colgroup>';
			$x .= '<tr class="tabletop">';
				foreach($headingAry as $_heading){
					$x .= '<th>'.$_heading.'</th>';
				}
			$x .= '</tr>';
			$counter = 1;
			$countOfRule = count($ruleAry);
			foreach($ruleAry as $_ruleRow){
				
				$groupHead = ($_ruleRow['NeedGroupHeadApprove'])?$Lang['ePCM']['Group']['GroupHead']:'';
				
				$vicePrincipal = ($_ruleRow['NeedVicePrincipalApprove'])?$Lang['ePCM']['General']['VicePrincipal']:'';
				if($vicePrincipal){
					if($groupHead){
						$vicePrincipal = $Lang['ePCM']['Setting']['Rule']['And'].$vicePrincipal;
					}
				}
				
				// approval mode remarks [start]
				$langArppovalMode = ($_ruleRow['ApprovalMode'] == 'S'? $Lang['ePCM']['Setting']['Rule']['ApprovalSeperately'] : $Lang['ePCM']['Setting']['Rule']['ApprovalTogether'] );
				$displayApprovalMode = '<br><span class="tabletextremark">('.$langArppovalMode.')</span>';
				
				$approvalRuleArr = $json->decode($_ruleRow['CaseApprovalRule']);
				$displayApprovalRuleArr = array();
				if(!empty($approvalRuleArr)){
					$num = 0;
					foreach($approvalRuleArr as $_roleId => $_approvalNum){
						$num++;	
						if($_roleId > 0){
							$_displayRole = Get_Lang_Selection($roleInfoAry[$_roleId]['RoleNameChi'], $roleInfoAry[$_roleId]['RoleName']);
						}else{
							$_displayRole = $Lang['ePCM']['Group']['GroupHead'];
						}
						if($_approvalNum > 0){
							$_approvalLang = str_replace('<!--n-->',$_approvalNum, $Lang['ePCM']['Setting']['Rule']['AtLeast']);
						}else{
							$_approvalLang = $Lang['ePCM']['Setting']['Rule']['All'];
						}
						if($_ruleRow['ApprovalMode'] == 'S'){
							$displayApprovalRuleArr[] = $num.'. '.$_approvalLang.$_displayRole;
						}else{
							$displayApprovalRuleArr[] = $_approvalLang.$_displayRole;
						}
					}
				}
				// approval mode remarks [end]

				// cust approval [start]
				if($sys_custom['ePCM']['extraApprovalFlow']){
					$langCustArppovalMode = ($_ruleRow['CustApprovalMode'] == 'S'? $Lang['ePCM']['Setting']['Rule']['ApprovalSeperately'] : $Lang['ePCM']['Setting']['Rule']['ApprovalTogether'] );
					$displayCustApprovalMode = '<br><span class="tabletextremark">('.$langCustArppovalMode.')</span>';
					
					$custApprovalRuleArr = $json->decode($_ruleRow['CustCaseApprovalRule']);
					$displayCustApprovalRuleArr = array();
					if(!empty($custApprovalRuleArr)){
						$num = 0;
						foreach($custApprovalRuleArr as $_roleId => $_approvalNum){
							$num++;	
							if($_roleId > 0){
								$_displayRole = Get_Lang_Selection($roleInfoAry[$_roleId]['RoleNameChi'], $roleInfoAry[$_roleId]['RoleName']);
							}else{
								$_displayRole = $Lang['ePCM']['Group']['GroupHead'];
							}
							if($_approvalNum > 0){
								$_approvalLang = str_replace('<!--n-->',$_approvalNum, $Lang['ePCM']['Setting']['Rule']['AtLeast']);
							}else{
								$_approvalLang = $Lang['ePCM']['Setting']['Rule']['All'];
							}
							if($_ruleRow['CustApprovalMode'] == 'S'){
								$displayCustApprovalRuleArr[] = $num.'. '.$_approvalLang.$_displayRole;
							}else{
								$displayCustApprovalRuleArr[] = $_approvalLang.$_displayRole;
							}
						}
					}
				}
				// cust approval [end]
				
				foreach($quotationTypeDataAry as $quotationTypeData){
					if($_ruleRow['QuotationType']==$quotationTypeData[0]){
						$_quotationTypeText=$quotationTypeData[1];
					}
				}
				if($_ruleRow['QuotationType']=='Q'||$_ruleRow['QuotationType']=='V'){
					if($_ruleRow['QuotationApprovalRoleID'] == -1){
						$_quotationApprovalText = $Lang['ePCM']['Setting']['Rule']['GroupHead'];
					}else if($_ruleRow['QuotationApprovalRoleID'] == -999){
						$_quotationApprovalText = $Lang['ePCM']['Mgmt']['Case']['Applicant'];
					}else{
						$_quotationApprovalText = $roleInfoAry[$_ruleRow['QuotationApprovalRoleID']][Get_Lang_Selection('RoleNameChi','RoleName')];
					}
				}else{
					$_quotationApprovalText = $Lang['General']['EmptySymbol'];
				}
				if($_ruleRow['QuotationType']=='T'){
					$_tenderAuditingText = $roleInfoAry[$_ruleRow['TenderAuditingRoleID']][Get_Lang_Selection('RoleNameChi','RoleName')];
					$_tenderApprovalText = $roleInfoAry[$_ruleRow['TenderApprovalRoleID']][Get_Lang_Selection('RoleNameChi','RoleName')];
				}else{
					$_tenderAuditingText = $Lang['General']['EmptySymbol'];
					$_tenderApprovalText = $Lang['General']['EmptySymbol'];
				}
				if($_ruleRow['QuotationType']=='N'){
					$_minQuotationText = $Lang['General']['EmptySymbol'];
				}else{
					$_minQuotationText = $_ruleRow['MinQuotation'];
				}
				$and = '';
				if($_ruleRow['NeedGroupHeadApprove']&&$_ruleRow['ApprovalRoleID']||$vicePrincipal){
					$and = $Lang['ePCM']['Setting']['Rule']['And'];
				}
				
				//for error showing
				//1
				$FloorErrorClass='';
				if($counter==1){
					if($invalidAry[0]==1){
						$FloorErrorClass = 'class="error"';
					}
				}
				//2
				$CeillingErrorClass='';
				if($counter==$countOfRule){
					if($invalidAry[1]==1){
						$CeillingErrorClass = 'class="error"';
					}
				}
				//3
				if($isNextRowFloorError){	//Previous have price duplication error
					$FloorErrorClass = 'class="error"';
					$isNextRowFloorError = false; //reset
				}
				if(in_array($_ruleRow['RuleID'],(array)$invalidAry[2]['RuleID'])){
					$CeillingErrorClass = 'class="error"';
					$isNextRowFloorError = true; //Error show in next rule
				}
				//4
				if(in_array($_ruleRow['RuleID'],(array)$invalidAry[3]['RuleID'])){
					$CeillingErrorClass = 'class="error"';
				}
				
				
				$x .= '<tr>';
					$x .= '<td>'.$_ruleRow['Code'].'</td>';
					$x .= '<td '.$FloorErrorClass.'>'.$this->displayMoneyFormat($_ruleRow['FloorPrice'],0).'</td>';
					$x .= '<td '.$CeillingErrorClass.'>';
						if($_ruleRow['CeillingPrice']===NULL){
							$x .= '>'.$this->displayMoneyFormat($_ruleRow['FloorPrice'],0);
						}else{
							$x .= $this->displayMoneyFormat($_ruleRow['CeillingPrice'],0);
						}
					$x .= '</td>';
// 					$displayand = '';
// 					if( ($groupHead != '' || $vicePrincipal != '') && $_ruleRow['ApprovalRoleID'] > 0){
// 						$displayand = $and;
// 					}
					//$x .= '<td>' .$groupHead.$vicePrincipal.$displayand.$roleInfoAry[$_ruleRow['ApprovalRoleID']][Get_Lang_Selection('RoleNameChi','RoleName')].$displayApprovalMode.'</td>';
					if($sys_custom['ePCM']['extraApprovalFlow']){
						if($_ruleRow['QuotationType'] == 'N'){
							$x .= '<td>'.Get_String_Display('').'</td>';
							$x .= '<td>'.implode('<br>',$displayApprovalRuleArr).$displayApprovalMode.'</td>';
						}else{
							$x .= '<td>'.implode('<br>',$displayApprovalRuleArr).$displayApprovalMode.'</td>';
							if(!empty($displayCustApprovalRuleArr)){
								$x .= '<td>'.implode('<br>',$displayCustApprovalRuleArr).$displayCustApprovalMode.'</td>';
							}else{
								$x .= '<td>'.Get_String_Display('').'</td>';
							}
						}
					}else{
						$x .= '<td>'.implode('<br>',$displayApprovalRuleArr).$displayApprovalMode.'</td>';
					}
                    $_NumTemplateFile = $_ruleRow[numAttach];
					$x .= '<td>'.$_quotationTypeText.'</td>';
					$x .= '<td>'.$_quotationApprovalText.'</td>';
					$x .= '<td>'.$_tenderAuditingText.'</td>';
					$x .= '<td>'.$_tenderApprovalText.'</td>';
					$x .= '<td>'.$_minQuotationText.'</td>';
                   // $x .= '<td>'.$_NumTemplateFile.'</td>';
					$x .= '<td>'."<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"".$_ruleRow['RuleID']."\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />".'</td>';
				$x .= '</tr>';
				$counter++;
			}

		$x .= '</table>';
		return $x;
	}
	
	public function getSelectAcademicYear($objName, $tag='', $noFirst=0, $noPastYear=0, $targetYearID='', $displayAll=0, $pastAndCurrentYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0, $excludeYearIDArr=array()){
		global $junior_mck;
		if(isset($junior_mck)){
			return getSelectAcademicYear($objName, $tag, $noFirst, $noPastYear, $targetYearID, $displayAll, $pastAndCurrentYearOnly, $OrderBySequence, $excludeCurrentYear, $excludeYearIDArr,$isUnicode=true);
		}else{
			return getSelectAcademicYear($objName, $tag, $noFirst, $noPastYear, $targetYearID, $displayAll, $pastAndCurrentYearOnly, $OrderBySequence, $excludeCurrentYear, $excludeYearIDArr);
		}
	}
	
	public function getFilter($idArray,$showTextAry,$ParSelectedStr, $ParSelected, $ParOnChange, $ParSelLang, $noFirst=0,$CategoryTypeID=0,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='sel'){

        $ReturnStr = "";
		$ReturnStr .= "<select id=\"".$DeflautName."\" name=\"$DeflautName"."\"".($IsMultiple?"[]":"")."\" ".($ParOnChange!=''?"onChange=\"$ParOnChange\"":"")." ".($IsMultiple?"multiple=\"multiple\" size=\"10\" ":"").">";
				if(!$noFirst && !$IsMultiple) {
					$ReturnStr .= "<option value=\"\"> - ".$ParSelLang." - </option>";
				}
				
				$numOfRow = count($showTextAry);
				for ($i = 0; $i < $numOfRow; $i++) {
					$_id = $idArray[$i];
					$_showText = $showTextAry[$i];
					
					if($IsMultiple){
						$_isOptionSelected = false;
						if ($ParDefaultSelectedAll) {
							$_isOptionSelected = true;
						}
						else if($ParSelected && is_array($ParSelectedStr)){
							$_isOptionSelected = in_array($_id,$ParSelectedStr)? true : false;
						}else if($ParSelected && $ParSelectedStr == $_id){
							$_isOptionSelected = true;
						}
						
						$selected = "";
						if ($_isOptionSelected) {
							$selected = ' selected ';
						}
					}else{
						(($ParSelected)&&($ParSelectedStr == $_id)) ? $selected = " selected " : $selected = "";
					}
					//($ParSelectedStr == $ReturnArr[$i][0]) ? $selected = " selected " : $selected = "";
					$ReturnStr .= "<option $selected value=\"".$_id."\">".$_showText."</option>";
				}
			$ReturnStr .= "</select>";
			
		return $ReturnStr;
	}
	function Get_User_Selection($groupCat,$AddUserID='',$multiple=true) {
		global $Lang,$_PAGE,$PATH_WRT_ROOT;
		
		$groupType = substr($groupCat,0,1);
		$groupID = substr($groupCat,1);

		switch ($groupType){
			case 'I':
				if($groupID==1){
					$teaching = 1;
				}else if($groupID==3){
					$teaching = 0;
				}
				$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
				$teachingStaff = $db -> getStaffUserInfo(1,$AddUserID);
				
				$nonTeachingStaff = $db -> getStaffUserInfo(0,$AddUserID);
				if($teaching){
					$UserList = $teachingStaff;
				}else{
					$UserList = $nonTeachingStaff;
				}
				break;
			case 'G':
				$groupObj = new libgroup($groupID);
				$UserList = ($groupObj->returnGroupUser());
				break;
			case 'P':
                $db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
                $parentAry = $db -> getParentUserInfo($AddUserID);
				$UserList = $parentAry;
				break;
			case 'S':
				$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
				$SMCAry = $db-> getSMCUserInfo($AddUserID);
				$UserList = $SMCAry;
				break;
		}
		if ($AddUserID== "")
			$AddUserID = array();
		
		if($multiple){
			$isMultiple = 'multiple = "true" ';
		}else{
			$isMultiple = '';
		}
		
		$x .= '<select name="AvalUserList[]" id="AvalUserList[]" size="10" style="width:99%" '.$isMultiple.'>';
		
		for ($i=0; $i< sizeof($UserList); $i++) {
			$x .= '<option value="'.$UserList[$i]['UserID'].'">';
			$x .= Get_Lang_Selection($UserList[$i]['ChineseName'],$UserList[$i]['EnglishName']);
			$x .= '</option>';
		}
		$x .= '</select>';

		return $x;
	}
	function Get_Case_Summary_UI($caseAry=''){
		global $PATH_WRT_ROOT,$Lang,$LAYOUT_SKIN,$_PAGE,$sys_custom, $ePCMcfg;

		if($caseAry===''){
			$x = '';
			$x .= '<div class="table_board">';
			$x .= '<div class="content_box yellow">';
			$x .= '<div class="content_box" style=" text-align: center; padding:50px"> ';
				$x.= $Lang['General']['NoRecordAtThisMoment'];
			$x .= '</div>';
			$x .= '</div>';
			$x .= '</div>';
			
			return $x;
		}
		
		$caseID = $caseAry['CaseID'];
		$mgmtObj = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$isCaseRejected = $mgmtObj->isCaseRejected($caseID);
        $isCustCaseRejected = $mgmtObj->isCustCaseRejected($caseID);
		$code = $caseAry['Code'];
		$caseName = $caseAry['CaseName'];
		$dateInput = $caseAry['DateInput'];
		$dateModified = $caseAry['DateModified'];
		$description = $caseAry['Description'];
		$budget = $caseAry['Budget'];
		$applicantType = $caseAry['ApplicantType']; // I => individual / G => Group
		$applicantUserID = $caseAry['ApplicantUserID'];
		$applicantUserName = $caseAry['ApplicantUserName'];
		$applicantGroupID = $caseAry['ApplicantGroupID'];
		$applicantGroupName = $caseAry['GroupName'];
		$applicantGroupNameChi = $caseAry['GroupNameChi'];
		$quotationType = $caseAry['QuotationType'];
		$currentStage = $caseAry['CurrentStage'];
		$caseApprovedDate = $caseAry['CaseApprovedDate'];
		
		
		$quotationStartDate = $caseAry['QuotationStartDate'];
		$quotationEndDate = $caseAry['QuotationEndDate'];
		$quotationApprovalStartDate = $caseAry['QuotationApprovalStartDate'];
		$quotationApprovalEndDate = $caseAry['QuotationApprovalEndDate'];
		
		$tenderOpeningStartDate = $caseAry['TenderOpeningStartDate'];
		$tenderOpeningEndDate = $caseAry['TenderOpeningEndDate'];
		
		$tenderApprovalStartDate = $caseAry['TenderApprovalStartDate'];
		$tenderApprovalEndDate = $caseAry['TenderApprovalEndDate'];
		
		// const
		$case_approved_status_wordings = array(
			'-1'=>$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Rejected'],
			'0'=>$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Pending'],
			'1'=>$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approved']
		);
		if(intval($currentStage)===0){
			$isDraft = 1;
		}else if (intval($currentStage)==1){
			$case_approval_status = 0; //????
		}else if(intval($currentStage)==2){
			$case_approval_status = 1;
		}
		
		if ($isCaseRejected)  $case_approval_status = -1;
		
		$stage1 = array(
			'title'=>$Lang['ePCM']['Mgmt']['Case']['CaseApplication'],
			'startDate'=> $dateInput,
			'endDate'=> $caseApprovedDate,
			'image'=> '/images/'.$LAYOUT_SKIN.'/iPortfolio/scheme/icon_admin.gif',
			'status'=> $case_approved_status_wordings[$case_approval_status], // should be calulated????
			'stageID'=>1
		);
		
		$stage2 = array(
			'title'=>$Lang['ePCM']['Mgmt']['Case']['Tender'],
			'startDate'=>$quotationStartDate,
			'endDate'=>$quotationEndDate,
			'image'=> '/images/'.$LAYOUT_SKIN.'/iPortfolio/scheme/icon_staff.gif',
			'stageID'=>2
		);
		
		$stage3 = array(
			'title'=>$Lang['ePCM']['Mgmt']['Case']['TenderOpening'],
			'startDate'=>$tenderOpeningStartDate,
			'endDate'=>$tenderOpeningEndDate,
			'image'=> '/images/'.$LAYOUT_SKIN.'/iPortfolio/scheme/icon_admin.gif',
			'stageID'=>3
		);
		
		$stage4 = array(
			'title'=>$Lang['ePCM']['Mgmt']['Case']['TenderApproval'],
			'startDate'=>$tenderApprovalStartDate,
			'endDate'=>$tenderApprovalEndDate,
			'image'=> '/images/'.$LAYOUT_SKIN.'/iPortfolio/scheme/icon_staff.gif',
			'stageID'=>4
		);
		
		$stage5 = array(
			'title'=> ($quotationType=='Q')?$Lang['ePCM']['Mgmt']['Case']['Quotation']:$Lang['ePCM']['Setting']['Rule']['Verbal'],
			'startDate'=>$quotationStartDate,
			'endDate'=>$quotationEndDate,
			'image'=> '/images/'.$LAYOUT_SKIN.'/iPortfolio/scheme/icon_staff.gif',
			'stageID'=>2
		);
		
		$stage6 = array(
			'title'=>$Lang['ePCM']['Mgmt']['Case']['QuotationApproval'],
			'startDate'=>$quotationApprovalStartDate,
			'endDate'=>$quotationApprovalEndDate,
			'image'=> '/images/'.$LAYOUT_SKIN.'/iPortfolio/scheme/icon_staff.gif',
			'stageID'=>4
		);
		$stage7 = array(
			'title'=>$Lang['ePCM']['Mgmt']['Case']['InputResult'],
			'startDate'=>'',
			'endDate'=>'',
			'image'=> '/images/'.$LAYOUT_SKIN.'/iPortfolio/scheme/icon_staff.gif',
			'stageID'=>4
		);
		$stage8 = array(
			//for 'Q'
			'title'=>$Lang['ePCM']['Mgmt']['Case']['UploadQuoation'],
			'startDate'=>'',
			'endDate'=>'',
			'image'=> '/images/'.$LAYOUT_SKIN.'/iPortfolio/scheme/icon_staff.gif',
			'stageID'=>3
		);
		$stage9 = array(
			//for 'N' = > Endorsement
				'title'=>$Lang['ePCM']['Mgmt']['Case']['Endorsement'],
				'startDate'=> $dateInput,
				'endDate'=> $caseApprovedDate,
				'image'=> '/images/'.$LAYOUT_SKIN.'/iPortfolio/scheme/icon_admin.gif',
				//'status'=> $case_approved_status_wordings[$case_approval_status], // should be calulated????
				'stageID'=>5
		);
		switch ($quotationType){
			case 'N':
				$stageInfoArray = array($stage7,$stage9);
			break;
			case 'V':
				//2016-06-29
				if($sys_custom['ePCM_skipCaseApproval']){
					$stageInfoArray = array($stage5,$stage6,$stage9);
				}else{
					$stageInfoArray = array($stage1,$stage5,$stage6);
				}
				break;
			case 'Q':
				if($sys_custom['ePCM_skipCaseApproval']){
					$stageInfoArray = array($stage5,$stage8,$stage6,$stage9);
				}else{
					$stageInfoArray = array($stage1,$stage5,$stage8,$stage6);
				}
			break;
			case 'T':
				if($sys_custom['ePCM_skipCaseApproval']){
					$stageInfoArray = array($stage2,$stage3,$stage4,$stage9);
				}else{
					$stageInfoArray = array($stage1,$stage2,$stage3,$stage4);
				}
			break;
		}
		
		
		######## const will be used
		$timeNow = date("Y-m-d H:i:s");
		switch($applicantType){
			case 'I':
			$applicant = $applicantUserName;
			break;
			case 'G':
			$applicant = Get_Lang_Selection($applicantGroupNameChi,$applicantGroupName);
			break;
		}
		
		$finish_status = array(
			'0'=>$Lang['ePCM']['Mgmt']['Case']['NotFinished'],
			'1'=>$Lang['ePCM']['Mgmt']['Case']['Finished'],
			'2'=>$Lang['ePCM']['Mgmt']['Case']['Status']['Reject']
		);

		
		$x ='';
		$x .= '<div class="table_board">';
			$x .= '<div class="content_box yellow">';
				$x .= '<span class="box_title pull_left">';
				$x .= '<a href="index.php?p=mgmt.caseDetials.'.$caseID.'" >';
				$x .= ($code!=='')?'['.$code.'] ':'[DRAFT] ';
				$x .= $caseName.'</span> ';
				$x .= '</a>';
				$x .= '<span class="pull_right">'.$Lang['ePCM']['Mgmt']['Case']['LastUpdate'].$Lang['_symbol']['colon'].$dateModified.'</span>';
					$x .= '<div class="content_box"> ';
						$x .= '<span>';
							$x .= $Lang['ePCM']['Mgmt']['Case']['Purpose'].$Lang['PCM']['Symbol']['Colon'].$description;
							$x .= '<br>';
							$x .= $Lang['ePCM']['Mgmt']['Case']['Budget'].$Lang['PCM']['Symbol']['Colon'].$this->displayMoneyFormat($budget);
							$x .= '<br>';
							$x .= $Lang['ePCM']['Mgmt']['Case']['Applicant'].$Lang['PCM']['Symbol']['Colon'].$applicant;
						$x .= '</span>';

							$x .= '<ul class="eproc_flow">';
								$isFinished = 1;
								$stageInfoArray = (array)$stageInfoArray;
								
								foreach($stageInfoArray as $stageInfo){
									if($skip)continue;
									
									$today = strtotime($timeNow);
									$endDate = strtotime($stageInfo['endDate']);
									$startDate = strtotime($stageInfo['startDate']);
									if($isDraft){
										$timeStatus = '';
									}
									else if(intval($currentStage)==$stageInfo['stageID']){
										
										$timeStatus = 'current';
										if($isCaseRejected){
											$timeStatus .= ' rejected';
										}
									}else if(intval($currentStage)>$stageInfo['stageID']){
										$timeStatus = 'past';
									}else{
										$timeStatus = '';
									}
//									else if($startDate==''){
//										$timeStatus = '';
//									}else if($endDate==''){
//										$timeStatus = 'current';
//									}else if($endDate<$today&&$startDate<$today){
//										$timeStatus = 'past';
//									}else if($endDate>$today&&$startDate<$today){
//										$timeStatus = 'current';
//									}else{
//										$timeStatus = '';
//									}
									if($timeStatus!='past'||$sys_custom['ePCM']['extraApprovalFlow']||$sys_custom['ePCM_skipCaseApproval']){
										$isFinished = 0;
									}
									$x .= '<li class="'.$timeStatus.'">';
										$x .= '<div class="steps_box">';
											$x.= '<span>'.$stageInfo['title'].'</span>';
											$x .= '<a href="#" class="icon_edit"></a><br />';
											$x.= '<br>';
											$x.= '<img src="'.$PATH_WRT_ROOT.$stageInfo['image'].'" width="30" height="28">';
											if($stageInfo['status']!=''){
												$x.= '<div class="status">';
													$x.= '<span>'.$stageInfo['status'].'</span>';
												$x .= '</div>';
											}
										$x.= '</div>';
										$x.= '<div class="arrow_box">';
											$x .= '<div class="arrow"></div>';
											$x .= '<span class="arrow_date pull_left">';
											$x .= ($startDate!='')?date('d M',$startDate):'';
											$x .= '</span>';
											$x .= '<span class="arrow_date pull_right">';
											$x .= ($endDate!='')?date('d M',$endDate):'';
											$x .='</span>';
										
									$x .= '</li>';
									
									if($timeStatus=='current rejected'){
										$skip = true;
									}
								}
								// finish / unfinish div
                                if(!($sys_custom['ePCM']['extraApprovalFlow']||$sys_custom['ePCM_skipCaseApproval'])){$isCustCaseRejected=false;}
								$finish = ($isCustCaseRejected)?'':(($isFinished)?'finish':'');
                                $finish_status_ui = ($isCustCaseRejected)?$finish_status[2]:$finish_status[$isFinished];
								$x .= '<li class="'.$finish.'">';
									$x .= '<div class="steps_box">';
										$x .= '<span>'.$finish_status_ui.'</span>';
										$x .= '<br>';
										$x .= '<div class="circle" '.(($isCustCaseRejected)?'style="background: #f26868;"':'').'>';
											$x .= '<img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/iPortfolio/icon_scheme_off.gif" width="25" height="25">';
										$x .= '</div>';
									$x .= '</div>';
									$x.= '<div class="arrow_box">';
										$x .= '<div class="arrow" '.(($isCustCaseRejected)?'style="background: #f26868;"':'').'></div>';
									$x .= '</div>';	
								$x .= '</li>';
							$x .= '</ul>';

					$x .= '</div>';
			$x .= '</div>';
		$x .= '</div>';
		
		return $x;
	}
	public function Get_Case_Detail_UI($caseAry,$gs){
		global $PATH_WRT_ROOT,$Lang,$LAYOUT_SKIN,$_PAGE,$sys_custom, $ePCMcfg;

		$libPCM_auth = $_PAGE['Controller']->requestObject('libPCM_auth','includes/ePCM/libPCM_auth.php');
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
        $linterface = $_PAGE['libPCM_ui'];

		// Get all role info -> for getting ro
		$roleInfoAry = $db->getRoleInfo($roleIDAry='', $ForCaseApproval='',$ForQuotationApproval='',$ForTenderAuditing='',$ForTenderApproval='',$isDeleted='0',$isPrincipal='');
		$roleInfoAry = BuildMultiKeyAssoc($roleInfoAry,'RoleID');
		
		$emailNotificationEnable = $gs['emailNotification'];
		$eClassAppNotificationEnable = $gs['eClassAppNotification'];

		$exceedBudget = $caseAry['exceedBudget'];
		$caseID = $caseAry['CaseID'];
		$code = $caseAry['Code'];
		$caseName = $caseAry['CaseName'];
		$categoryID = $caseAry['CategoryID'];
		$categoryName = $caseAry['CategoryName'];
		$categoryNameChi = $caseAry['CategoryNameChi'];
		$description = $caseAry['Description'];
		$groupName = $caseAry['GroupName'];
		$groupNameChi = $caseAry['GroupNameChi'];
		$financialItemName = $caseAry['ItemName'];
		$financialItemNameChi = $caseAry['ItemNameChi'];
		$itemID = $caseAry['ItemID'];
		$budget = $caseAry['Budget'];
		$applicantType = $caseAry['ApplicantType'];
		$applicantUserID = $caseAry['ApplicantUserID'];
		$applicantUserName = $caseAry['ApplicantUserName'];
		$applicantGroupName = $caseAry['GroupName'];
		$applicantGroupNameChi = $caseAry['GroupNameChi'];
		$applicantGroupID = $caseAry['ApplicantGroupID'];
		$applicant = ($applicantType=='I')?$applicantUserName:Get_Lang_Selection($applicantGroupNameChi,$applicantGroupName);
		$dateInput = $caseAry['DateInput'];
		$quotationType = $caseAry['QuotationType'];
		$quotationEndDate = $caseAry['QuotationEndDate'];
		$quotationNotificationDaysBefore = 3; //Temp
		$currentStage = $caseAry['CurrentStage'];
		$docInfoArray = $caseAry['CaseAttachment'];
		$decDocInfoArray = $caseAry['DeclarationAttachment'];
		$invDocInfoArray = $caseAry['InvitationAttachment'];
		$finalDocument = $caseAry['FinalDocument'];
		$caseApprovalAry = $caseAry['CaseApproval'];
		$custCaseApprovalAry = $caseAry['CustCaseApproval'];
		$isCaseRejected = $caseAry['isCaseRejected'];
		$isCustCaseRejected = $caseAry['isCustCaseRejected'];
//		$quotationInvitationDate = $caseAry['QuotationInvitationDate'];	//How come???
		$quotationInfoAry = $caseAry['Quotation'];
		$quoFilesAssocArr =  $caseAry['QuoAttachment'];
		$tenderOpeningStartDate = $caseAry['TenderOpeningStartDate'];
		$tenderOpeningEndDate = $caseAry['TenderOpeningEndDate'];
		$tenderApprovalStartDate = $caseAry['TenderApprovalStartDate'];
		$tenderApprovalEndDate = $caseAry['TenderApprovalEndDate'];
		$minQuotation = $caseAry['MinQuotation'];
		$remarks = $caseAry['Remarks'];
		$result = $caseAry['Result'][0];
		$versions = $caseAry['Version'];
		$isDeleted = $caseAry['IsDeleted'];
		$deleteReason = $caseAry['DeleteReason'];
		$notiSchedules = $caseAry['NotiSchedule'];
		$notiUsers = $caseAry['NotiUsers'];
		$log = $caseAry['Log'];
		$quotationApprovalRole = $caseAry['QuotationApprovalRole'];
		$tenderAuditingRole = $caseAry['TenderAuditingRole'];
		$tenderApprovalRole = $caseAry['TenderApprovalRole'];
		$priceComparisonInfo = $caseAry['priceComparisonInfo'];
		$fundingList_arr = $caseAry['fundingList_arr'];
		$FundingSourceArr = $caseAry['FundingSourceArr'];
		// 2016-10-12	Villa fix the "Reason cannot be display" problem
		$stageID = Get_Array_By_Key($remarks, "StageID");
		$remarks = BuildMultiKeyAssoc($remarks, "StageID");
		//2018-23-08    Isaac get ruleID for export EDB
        $ruleID =  $caseAry['RuleID'];

		$notiSchedules =  BuildMultiKeyAssoc($notiSchedules, array("StageID"));
		$notiUsers = BuildMultiKeyAssoc($notiUsers, array("StageID"));

		//Const
		$case_approval_status = array(
			'0'=>$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Pending'],
			'1'=>'<span class="accept">'.$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approved'].'</span>',
			'-1'=>'<span class="reject">'.$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Rejected'].'</span>'
		);
		$quotation_status = array(
			'-1'=>'<span class="reject" value="-1">'.$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Rejected'].'</span>',
			'0'=>'<span class="grey_text" value="0" >'.$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Pending'].'</span>',
			'1'=>'<span class="accept" value="1">'.$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Accepted'].'</span>'
		);
		$quotation_status_icon_html = array(
			'-1'=>'<span class="icon_reject"></span>',
			'0'=>'<span class="icon_pending"></span>',
			'1'=>'<span class="icon_accept"></span>'
		);
		$quotation_contact_method = array(
            'R'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['RegisteredLetter'],
			'E'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Email'],
			'P'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Phone'],
			'F'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Fax'],
			'I'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['InPerson']
		);
		$step_remarks = array(
			'0'=>'step 0',
			'1'=>'step 1',
			'2'=>$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox2'][$quotationType],
			'3'=>$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox3'][$quotationType],
			'4'=>$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox4'][$quotationType]
		);

        /**
         * Page Description:
         * contains
         * - info table
         * - step_box (div) x n
         *		a: class -> past | current | 'nothing' -> color different
         */

		 $x ='';
		 $x .= '<form name="form1" id="form1" method="POST" action="">';

		 $editable = false;
		 /**
		  * Editable when:
		  * 1. is Draft
		  * 2. is Rejected by someone => need start a new case
		  */

		 ##### Price Comparison table
		 if(empty($priceComparisonInfo)){

		 }else{
		 	$priceComparisonTable = '<h3>'.$Lang['ePCM']['Mgmt']['Case']['PriceComparisonTable'].'</h3>'.'<!--edit_button-->';
		 	$priceComparisonTable .= '<table  width="100%" border="0" cellspacing="0" cellpadding="6" class="table_type2 inner_lines"><tbody valign="top">';
		 	$priceComparisonTable .= '<tr>';
		 	$priceComparisonTable .= '<th>';
		 	$priceComparisonTable .= $Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier'];
		 	$priceComparisonTable .= '</th>';
		 	foreach ((array)$priceComparisonInfo as $_priceItem_ID=>$_item){
		 		$priceComparisonTable .= '<th>';
		 		$priceComparisonTable .= $_item[key($_item)]['PriceItemName'];
		 		$priceComparisonTable .= '</th>';
		 	}
             $priceComparisonTable.='<th>'.$Lang['ePCM']['SettingsArr']['SupplierArr']['Total'].'</th>';
		 	$priceComparisonTable .= '</tr>';


		 	foreach($quotationInfoAry as $_quotation){
		 	    $priceSum=0;
		 		$priceComparisonTable .= '<tr>';
		 		$priceComparisonTable .='<td>';
		 		$priceComparisonTable .=Get_Lang_Selection($_quotation['SupplierNameChi'],$_quotation['SupplierName']);
		 		$priceComparisonTable .='</td>';
		 		foreach((array)$priceComparisonInfo as $_priceItem_ID=>$_item){
		 			$priceComparisonTable .='<td>';
		 			$priceComparisonTable .= $this->displayMoneyFormat($_item[$_quotation['QuotationID']]['Price']);
		 			$priceComparisonTable .='</td>';
                    $priceSum+=$_item[$_quotation['QuotationID']]['Price'];
		 		}
                $priceComparisonTable .= '<td>'.$this->displayMoneyFormat($priceSum).'</td>';
		 		$priceComparisonTable .= '</tr>';


		 		$quotationsAry[$_quotation['QuotationID']]['Price'] = $this->displayMoneyFormat($priceSum);
		 	}
		 	$priceComparisonTable .= '</table>';
		 }


		 if($currentStage==0){ // is draft
		 	$editable = true;
		 }else{

			if($isCaseRejected){	//is Rejected by someone => need start a new case
				$editable = true;
			}
		 }
		 if($isDeleted){
		 	$editable = false;
		 }

		 //counter of steps
		 $steps_count = 0;
		 $thisBoxStage = 0;
			 $x .= '<div class="table_board eproc_detail">';
			 	$x .= '<div class="content_box">';
			 		#E114398
				 	if($editable){ //by kenneth 20160429
				 		if($libPCM_auth->caseOwner($caseID)){
				 			$x .= '<div class="table_row_tool row_content_tool">';
				 			$x .= '<a href="javascript:editCase();" class="icon_edit_dim" style="margin:5px;"></a>';
				 			// 		 		$x .= $this->Get_Small_Btn($Lang['Btn']['Edit'], "button", "editCase()", "", "", "DisplayView");
				 			$x .= '</div>';
				 		}
// 				 		elseif($libPCM_auth->isAdmin()){
// 				 			$x .= '<div class="table_row_tool row_content_tool">';
// 				 			$x .= '<a href="javascript:editCase2();" class="icon_edit_dim" style="margin:5px;"></a>';
// 				 			// 		 		$x .= $this->Get_Small_Btn($Lang['Btn']['Edit'], "button", "editCase()", "", "", "DisplayView");
// 				 			$x .= '</div>';
// 				 		}
				 	}elseif($libPCM_auth->isAdmin()){
				 		$x .= '<div class="table_row_tool row_content_tool">';
				 		$x .= '<a href="javascript:editCase2();" class="icon_edit_dim" style="margin:5px;"></a>';
				 		$x .= '</div>';
				 	}


			 		//Warning Table for deleted case
				 	if($isDeleted){
				 		$x .= $this->GET_WARNING_TABLE($err_msg='', $DeleteItemName='', $DeleteButtonName='', $contentMsg=$Lang['ePCM']['Mgmt']['Case']['Warning']['CaseIsDeleted'].'<br>'.$Lang['ePCM']['Mgmt']['Case']['DeleteReason2'].': '.$deleteReason,$Lang['ePCM']['General']['Notice']);
				 		$x .= '<div style="height:10px"></div>';
                        $rejectedReasonAry[]=$deleteReason;
				 	}
                    if ($quotationType != 'T' &&($isDeleted || $isCaseRejected || $isCustCaseRejected)) {
                        $exportEDBAnnexIIBtn = '<div class="Conntent_tool" style="float: right;">' . '<a class="print" href="javascript:exportPurchase_by_OralQuotationForm(\'en\')">' . $Lang['ePCM']['Mgmt']['Case']['Purchase-by-OralQuotationForm']['ExportBtnEn'] . '</a>' . '</div>';
                        $exportEDBAnnexIIBtn.= '<div class="Conntent_tool" style="float: right;">' . '<a class="print" href="javascript:exportPurchase_by_OralQuotationForm(\'b5\')">' . $Lang['ePCM']['Mgmt']['Case']['Purchase-by-OralQuotationForm']['ExportBtnB5'] . '</a>' . '</div>';
                    }
				 	$x .= '<div class="step_box info">';
				 	$x .= '<div class="box_title"><span>'.++$steps_count.'</span>'.$Lang['ePCM']['Mgmt']['Case']['CaseApplication'].$exportEDBAnnexIIBtn.'</div>';
			 		//info table
				 	$x .= '<div class="Conntent_tool">'.'<a class="print" href="javascript:void(0)" Onclick="PrintApplcationFrom()">'.$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['PrintBtn'] .'</a>'.'</div>';
				 	$x .= '<br>';
			 		$x .= '<table cellpadding="7" cellspacing="0" border="0" width="100%" class="info">';
			 			$x .= '<colgroup>';
			                $x .= '<col width="120" />';
			                $x .= '<col width="30" />';
			            $x .= '</colgroup>';
			 			$x .= '<tbody valign="top">';
			 				$x .= '<tr>';
			 					$x .= '<th colspan="3" id="codeAndName_show">';
								$x .= ($code!='')?'['.'<span id="ajax_caseCode">'.$code.'</span>'.'] ':'';
								$x .= '<span id="ajax_caseName">'.$caseName.'</span>';
									if($libPCM_auth->caseOwner($caseID)){
										$x .= '<a href="javascript:editCaseCodeAndName();" class="icon_edit_dim" style="margin:5px;"></a>';
									}
									$x .= ' <span id="updateFailed_codeAndName" class="tabletextrequire"></span>';
								$x .= '</th>';
								$x .= '<th colspan="3" id="codeAndName_edit" style="display:none">';
									$x .= '<label for="codeEdit">'.$Lang['ePCM']['Mgmt']['Case']['CaseCode'].': </label>';
									$x .= $this->GET_TEXTBOX_NAME('codeEdit', 'codeEdit', $code, $OtherClass='', $OtherPar=array('style'=>'width:100px'));
									$x .= ' <label for="nameEdit">'.$Lang['ePCM']['Mgmt']['Case']['CaseName'].': </label>';
									$x .= $this->GET_TEXTBOX_NAME('nameEdit', 'nameEdit', $caseName, $OtherClass='', $OtherPar=array('style'=>'width:150px')).' ';
									$x .= $this->GET_SMALL_BTN(	$Lang['Btn']['Submit'], 'button', $ParOnClick="onClickCodeAndName();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="").' ';
									$x .= $this->GET_SMALL_BTN($Lang['Btn']['Cancel'], 'button', $ParOnClick="onClickCancelCodeAndName();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="").' ';
									$x .= ' <span id="warningDiv_codeAndName" class="tabletextrequire"></span>';
								$x .= '</th>';
			 				$x .= '</tr>';
			 				$x .= '<tr>';
			 					$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Category'].'</td>';
			 					$x .= '<td>:</td>';
			 					$x .= '<td>'.Get_Lang_Selection($categoryNameChi,$categoryName).'</td>';
			 				$x .= '</tr>';
			 				$x .= '<tr>';
			 					$x .= '<td>'.nl2br($Lang['ePCM']['Mgmt']['Case']['Purpose']).'</td>';
			 					$x .= '<td>:</td>';
			 					$x .= '<td>'.Get_String_Display($description).'</td>';
			 				$x .= '</tr>';
			 				$x .= '<tr>';
			 					$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Group'].'</td>';
			 					$x .= '<td>:</td>';
			 					$x .= '<td>'.Get_Lang_Selection($groupNameChi,$groupName).'</td>';
			 				$x .= '</tr>';
			 				$x .= '<tr>';
			 					$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['FinancialItem'].'</td>';
			 					$x .= '<td>:</td>';
			 					$x .= '<td>'.Get_Lang_Selection($financialItemNameChi,$financialItemName).'</td>';
			 				$x .= '</tr>';
			 				$x .= '<tr>';
			 					$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Budget'].'</td>';
			 					$x .= '<td>:</td>';
// 			 					$x .= '<td>'.$this->displayMoneyFormat($budget),0).'</td>';
//	Villa update budget display with float in stead of int
			 					$x .= '<td>'.$this->displayMoneyFormat($budget).'</td>';
			 				$x .= '</tr>';
							//Villa #K113812
							if($_PAGE['libPCM']->enableFundingSource()){
							$x .= '<tr>';
								$x .= '<td>'.$Lang['ePCM']['FundingSource']['FundingSource'].'</td>';
								$x .= '<td>'.':'.'</td>';
								$x .= '<td>';
								$x .= '<table width="100%" style="float:left; ">';
								//Print the Funding Source
								$print_funding = true;
								foreach ((array)$FundingSourceArr as $_FundingSourceArr){ //loop every funding
									foreach ((array)$_FundingSourceArr['Case'] as $_Case){ //loop every case
										if($_Case['CaseID']==$caseID){ //print only this case
											$x .= '<tr>';
											$x .= '<td width="30%">';
												$x .= Get_Lang_Selection($_FundingSourceArr['FundingNameCHI'], $_FundingSourceArr['FundingNameEN']);
												$x .= "&nbsp;";
												$x .= '('.$Lang['ePCM']['FundingSource']['RemainingBudget'] .':'.$this->displayMoneyFormat($_FundingSourceArr['BugetRemain']).')';
											$x .= '</td>';
											$x .= '<td style="float:left;">';
												$x .= $Lang['ePCM']['FundingSource']['UsingBudget'];
												$x .= ':'.$this->displayMoneyFormat($_Case['Amount']);
											$x .= '</td>';
											$x .= '</tr>';
											$print_funding = false;
										}
									}
								}
								if($print_funding){
									$x .= '<tr>';
									$x .= '<td>'.'-'.'</td>';
									$x .= '</tr>';
								}
								$x .= '</table>';
								$x .= '</td>';
							$x .= '</tr>';
			 				}
			 				$x .= '<tr>';
			 					$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Document'].'</td>';
			 					$x .= '<td>:</td>';
			 					$x .= '<td>';
			 					$x .= $this->getFileListUI($docInfoArray[0]);
								$x .= '</td>';
			 				$x .= '</tr>';
			 				$x .= '<tr>';
			 					$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Applicant'].'</td>';
			 					$x .= '<td>:</td>';
			 					$x .= '<td>'.$applicant.'</td>';
			 				$x .= '</tr>';
			 				$x .= '<tr>';
			 					$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['ApplicationDate'].'</td>';
			 					$x .= '<td>:</td>';
			 					$x .= '<td>'.$dateInput.'</td>';
			 				$x .= '</tr>';

			 				if(count($versions)>1){	//If no other version=> count(version) =1
					 			$x .= '<tr>';
						 			$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['PreviousRecord'].'</td>';
						 			$x .= '<td>:</td>';
						 			$x .= '<td>';
						 				foreach($versions as $_version){
						 					if($_version['CaseID']==$caseID){
						 						continue;
						 					}else{
						 						$x .= '<a href="javascript:goToCase('.$_version['CaseID'].');"> Version: '.$_version['Version'].'</a>';
						 					}
						 					$x .= '<br>';
						 				}
						 			$x .= '</td>';
					 			$x .= '</tr>';
			 				}
			 				if(true){
			 					$approverNameAry = Get_Array_By_Key($caseApprovalAry, "ApproverName");
			 					$x .= '<tr>';
			 						$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['RelatedUsers'].'</td>';
			 						$x .= '<td>:</td>';
			 						$x .= '<td>';
			 							$x .= '<table>';
			 								$x .= '<tr>';
			 									$x .= '<td>'.$Lang['ePCM']['Report']['FCS']['Applicant'].'</td>';
			 									$x .= '<td>:</td>';
			 									$x .= '<td>'.$applicantUserName.'</td>';
			 								$x .= '</tr>';
			 								if(!($quotationType=='N' || $sys_custom['ePCM_skipCaseApproval'])){
			 									$x .= '<tr>';
			 										$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['CaseApproval'].'</td>';
			 										$x .= '<td>:</td>';
			 										$x .= '<td>'.implode(', ',$approverNameAry).'</td>';
			 									$x .= '</tr>';
			 								}
			 								if($quotationType=='V'){
				 								$x .= '<tr>';
				 									$x .= '<td>'.$Lang['ePCM']['Setting']['Rule']['Verbal'].'</td>';
				 									$x .= '<td>:</td>';
				 									$x .= '<td>'.$applicantUserName.'</td>';
				 								$x .= '</tr>';
			 								}
			 								if($quotationType=='Q'){
				 								$x .= '<tr>';
				 									$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Quotation'].'</td>';
				 									$x .= '<td>:</td>';
				 									$x .= '<td>'.$applicantUserName.'</td>';
				 								$x .= '</tr>';
				 								$x .= '<tr>';
				 									$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['UploadQuoation'].'</td>';
				 									$x .= '<td>:</td>';
				 									$x .= '<td>'.$applicantUserName.'</td>';
				 								$x .= '</tr>';
			 								}
			 								if($quotationType=='T'){
				 								$x .= '<tr>';
				 									$x .= '<td>'.$Lang['ePCM']['Setting']['Rule']['TenderAuditing'].'</td>';
				 									$x .= '<td>:</td>';
				 									$x .= '<td>'.$tenderAuditingRole.'</td>';
				 								$x .= '</tr>';
				 								$x .= '<tr>';
				 									$x .= '<td>'.$Lang['ePCM']['Setting']['Rule']['TenderApproval'].'</td>';
				 									$x .= '<td>:</td>';
				 									$x .= '<td>'.$tenderApprovalRole.'</td>';
				 								$x .= '</tr>';
			 								}
			 								if($quotationType=='Q'||$quotationType=='N'||$quotationType=='V'){
			 									switch($quotationType){
			 										case 'Q':
			 										case 'V':
			 										$inputRole = $quotationApprovalRole;
			 										break;
			 										case 'N';
			 										$inputRole = $applicantUserName;
			 										break;
			 									}
				 								$x .= '<tr>';
				 									$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['InputResult'].'</td>';
				 									$x .= '<td>:</td>';
				 									$x .= '<td>'.$inputRole.'</td>';
				 								$x .= '</tr>';
			 								}
			 								if($quotationType=='N'||$sys_custom['ePCM_skipCaseApproval']){
			 									$x .= '<tr>';
			 									$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Endorsement'].'</td>';
			 									$x .= '<td>:</td>';
			 									$x .= '<td>'.implode(', ',$approverNameAry).'</td>';
			 									$x .= '</tr>';
			 								}
			 							$x .= '</table>';
			 						$x .= '</td>';
			 					$x .= '</tr>';
			 				}
			 			$x .= '</tbody>';
			 		$x .= '</table>';

			 		//$x .= '<div class="remarks"><strong>'.$Lang['ePCM']['General']['Notice'].'���/strong><br />'.$step_remarks[$thisBoxStage].'</div>';

			 		$x .= '</div>';
			 		//End of info Table

			 		if($currentStage==0){
			 			 // is draft
			 		}else{
			 			if(!($quotationType=='N'||$sys_custom['ePCM_skipCaseApproval'])){	//20160810 if Type = 'N' Skip
					 		//step_box 1
					 		$thisBoxStage = 1;
					 		if(intval($currentStage)==$thisBoxStage){
					 			$timeStatus = 'current';
					 		}else if(intval($currentStage)>$thisBoxStage){
					 			$timeStatus = 'past';
					 		}else{
					 			$timeStatus = '';
					 		}
					 		if(!($timeStatus==''&& $isDeleted)){	//Hide stage which not yet do for deleted case
					 			if($isCaseRejected) $additionClass = 'rejected';
						 		$x .= '<div class="step_box '.$timeStatus.' '.$additionClass.'" id="step_box_'.step_box_1.'">';
						 			$x .= '<div class="box_title"><span>'.++$steps_count.'</span>'.$Lang['ePCM']['Mgmt']['Case']['CaseApproval'].'</div>';
						 			//$x .= '<div class="remarks"><strong>'.$Lang['ePCM']['General']['Notice'].'</strong><br />'.$step_remarks[$thisBoxStage].'</div>';
						 			if($sys_custom['ePCM']['ExceedBudgetRemarks'] && $exceedBudget != 'OK'){
						 			    if($exceedBudget == 'ITEM'){
                                            $exceedRemarks = $Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Item'];
                                        }elseif($exceedBudget == 'GROUP'){
                                            $exceedRemarks = $Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Group'];
                                        }elseif($exceedBudget == 'GROUP:ITEM'){
                                            $exceedRemarks = $Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['GroupItem'];
                                        }else{
                                            $exceedRemarks = $Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['GroupItem'];
                                        }
						 				$x .= '<div class="remarks red" style="color:red;"><strong>'.$Lang['ePCM']['General']['Notice'].': </strong>'.$exceedRemarks.'</div>';
						 			}
						 			$counter = 1;
						 			foreach((array)$caseApprovalAry as $caseApprovalInfo) {
                                        // display approval role name
                                        $_approvalRoleId = $caseApprovalInfo['ApprovalRoleID'];
                                        $roleDisplay = ($_approvalRoleId > 0) ? Get_Lang_Selection($roleInfoAry[$_approvalRoleId]['RoleNameChi'],
                                            $roleInfoAry[$_approvalRoleId]['RoleName']) : $Lang['ePCM']['Group']['GroupHead'];
                                        $x .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_type1 with_icon">';
                                        $x .= '<colgroup>';
                                        $x .= '<col width="30" />';
                                        $x .= '<col width="135" />';
                                        $x .= '<col width="30" />';
                                        $x .= '<col width="200" />';
                                        $x .= '<col width="" />';
                                        $x .= '</colgroup>';
                                        $x .= '<thead valign="top">';
                                        $x .= '<tr>';
                                        $x .= '<th><img src="' . $PATH_WRT_ROOT . '/images/' . $LAYOUT_SKIN . '/iPortfolio/scheme/icon_staff.gif" width="30" height="28"></th>';
                                        $x .= '<th style="padding-right:0px;">' . $Lang['ePCM']['Mgmt']['Case']['Approver'] . ' #' . $counter . '<br>(' . $roleDisplay . ')</th>';
                                        $x .= '<th>:</th>';
                                        $x .= '<th>';
                                        if($caseApprovalInfo['IsDeletedApprover']){$requiredSymbol = $linterface->RequiredSymbol();}
                                        else{$requiredSymbol = '';}
                                        $x .= $requiredSymbol.$caseApprovalInfo['ApproverName'];
                                        $x .= '<br />';
                                        $x .= $case_approval_status[$caseApprovalInfo['RecordStatus']];
                                        $string = '';
                                        if ($caseApprovalInfo['ModifiedBy'] != $caseApprovalInfo['ApproverUserID']) {
                                            $string = '<br><span class="tabletextremark">' . str_replace('<!--adminName-->',
                                                    $caseApprovalInfo['ModifiedByName'],
                                                    $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['AdminAction']) . '</span>';
                                        }
                                        $x .= ($caseApprovalInfo['RecordStatus'] == 0) ? '' : ' (' . $this->convertDatetimeToDate($caseApprovalInfo['DateModified']) . ')' . $string;
                                        $x .= '</th>';
                                        $x .= '<th>';
                                        //Can edit only when you are the approver
                                        //if($_SESSION['UserID']==$caseApprovalInfo['ApproverUserID']){
                                        if (!$isDeleted) {
                                            if ($libPCM_auth->checkCaseApprovalRight($caseID,
                                                $caseApprovalInfo['ApproverUserID'])) {
                                                //if( $caseAry['ApprovalStage'] == 'NA' || in_array($caseApprovalInfo['ApprovalType'], $caseAry['ApprovalStage'] )){
                                                if (($caseAry['CurrentApprovalSequence'] == 'NA' || $caseApprovalInfo['ApprovalSequence'] <= $caseAry['CurrentApprovalSequence'])&& !$caseApprovalInfo['IsDeletedApprover']) {
                                                    $x .= '<a href="javascript:editCaseApproval(' . $caseApprovalInfo['ApproverUserID'] . ',' . $caseApprovalInfo['ApprovalRoleID'] . ');" class="icon_edit_dim"></a>';
                                                }
                                            }
                                        }
                                        //}
                                        $x .= '</th>';
                                        $x .= '</tr>';
                                        $x .= '</thead>';

                                        $x .= '<tbody valign="top">';
                                        if ($caseApprovalInfo['Feedback'] != '') {
                                            $x .= '<tr class="top_border">';
                                            $x .= '<td>&nbsp;</td>';
                                            $x .= '<td>' . $Lang['ePCM']['Mgmt']['Case']['Remarks'] . '</td>';
                                            $x .= '<td>&nbsp;</td>';
                                            $x .= '<td colspan="2">' . Get_String_Display($caseApprovalInfo['Feedback']) . '</td>';
                                            $x .= '</tr>';
                                        }
                                        $x .= '</tbody>';
                                        $x .= '</table>';
                                        $counter++;
                                        if ($caseApprovalInfo['RecordStatus'] == -1) {
                                            $rejectedReasonAry[] = Get_String_Display($caseApprovalInfo['Feedback']);
                                        }
						 			}
						 			if($isCaseRejected && $libPCM_auth->caseOwner($caseID)){
						 				//$x .= '<div>';
						 				//$x .= $this->GET_WARNING_TABLE($err_msg='', $DeleteItemName='', $DeleteButtonName='', $contentMsg=$Lang['ePCM']['Mgmt']['Case']['Warning']['CaseIsRejected'],$Lang['ePCM']['General']['Notice']);
						 				//$x .= '</div>';
						 				$x .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_type1">';
						 					$x .= '<tr>';
						 						$x .= '<th class="reject">'.$Lang['ePCM']['General']['Notice'].':<br>'.$Lang['ePCM']['Mgmt']['Case']['Warning']['CaseIsRejected'].'</th>';
						 					$x .= '</tr>';
						 				$x .= '</table>';
						 			}
                                $x .= $Lang['ePCM']['Setting']['Role']['DeletedUserLegend'];
						 		$x .= '</div>';
					 		}
			 			}
				 		//End of step_box 1

				 		//End if quotation type = N
				 		if($quotationType!='N'){
					 		//step_box 2
					 		$thisBoxStage = 2;
					 		if($currentStage==$thisBoxStage){
					 			$timeStatus = 'current';
					 		}else if($currentStage>$thisBoxStage){
					 			$timeStatus = 'past';
					 		}else{
					 			$timeStatus = '';
					 		}
					 		if(!($timeStatus==''&& ($isDeleted||$isCaseRejected))){
					 			$x .= '<div class="step_box '.$timeStatus.'" id="step_box_'.$thisBoxStage.'">';
						 			$x .= '<div class="box_title"><span>'.++$steps_count.'</span>';
						 			if($quotationType=='T'){
						 				$x .= $Lang['ePCM']['Mgmt']['Case']['Tender'];
						 			}else if($quotationType=='Q'){
						 				$x .= $Lang['ePCM']['Mgmt']['Case']['Quotation'];
						 			}else if($quotationType=='V'){
						 				$x .= $Lang['ePCM']['Setting']['Rule']['Verbal'];
						 			}
						 			$x .= '</div>';

						 			$x .= '<div class="remarks"><table><tr><td style="vertical-align:text-top;width:70px;"><strong>'.$Lang['ePCM']['General']['Notice'].':</strong></td><td>'.$step_remarks[$thisBoxStage].'</td></tr></table></div>';
						 			if($quotationType!=='V'){
						 				//2016-06-29 Hide for Verbal Quotation
							 			$x .= '<p><strong>';
							 			if($quotationType=='Q'){
							 				$x .= $Lang['ePCM']['Mgmt']['Case']['Deadlines'];
							 			}else if($quotationType=='T'){
							 				$x .= $Lang['ePCM']['Mgmt']['Case']['TenderClosing'];
							 			}

							 			$x .= '</strong></p>';
							 			$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_type1">';
								 			$x .= '<colgroup><col width="180" /><col width="30" /></colgroup>';
								 			$x .= '<tbody valign="top">';
								 				$x .= '<tr>';
								 					$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Deadlines'].'</td>';
								 					$x .= '<td>:</td>';
								 					// 20160927 Omas fix 1970-01-01 problem
// 								 					$quotationEndDate = ($quotationEndDate!='')?$quotationEndDate:$Lang['General']['EmptySymbol'];
// 								 					$quotationEndDate_date = ($quotationEndDate!='')?$this->convertDatetimeToDate($quotationEndDate):$Lang['General']['EmptySymbol'];
// 								 					$quotationEndDateAry = explode(' ',$quotationEndDate);
// 								 					$quotationEndDate_time = $quotationEndDateAry[1];
								 					if($quotationEndDate == ''){
								 						$displayQuoEndDate = $Lang['General']['EmptySymbol'];
								 						$quotationEndDate_date = '';
								 						$quotationEndDate_time = '';
								 					}else{
								 						$displayQuoEndDate = $quotationEndDate;
								 						$quotationEndDate_date = ($quotationEndDate!='')?$this->convertDatetimeToDate($quotationEndDate):$Lang['General']['EmptySymbol'];
									 					$quotationEndDateAry = explode(' ',$quotationEndDate);
									 					$quotationEndDate_time = $quotationEndDateAry[1];
								 					}
								 					$x .= '<td><div id="QuotationEndDate_div" style="display:inline-block">'.$quotationEndDate.'</div>';
								 						$x .= '<div id="datePickerQuotationEndDate_div" style="display:none">';
								 							$x .= $this->GET_DATE_PICKER('datePickerQuotationEndDate', $quotationEndDate_date,'',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=true, $cssClass="textboxnum");
	 							 							$x .= $this->Get_Time_Selection('timePickerQuotationEndDate', $quotationEndDate_time, $others_tab='',$hideSecondSeletion=true, $intervalArr=array(1,15,1));
								 							if($libPCM_auth->checkSendQuotationInvitationRight($caseID)){
								 								$x .= $this->GET_SMALL_BTN($Lang['Btn']['Submit'], "button", "datePickerQuotationEndDateSelected();", "", $ParOtherAttribute="style=\"margin-left:10px;\"", $OtherClass="", $ParTitle='');
								 							}
								 						$x .= '</div>';
								 						if(!$isDeleted){
								 							if($libPCM_auth->checkSendQuotationInvitationRight($caseID)){
								 								$x.= '<a href="javascript:editDatePicker(\'QuotationEndDate\');" class="icon_edit_dim" id="QuotationEndDateEdit"></a>';
								 							}
								 						}
								 						if($quotationEndDate!=''){
											 				$timeNow = date("Y-m-d H:i:s");
											 				$today = strtotime($timeNow);
											 				$dayLeft = (strtotime($quotationEndDate) - $today)/(60*60*24) ;
											 				if($timeStatus!=''){
											 					$x .= '<span class="expiry">'.$Lang['ePCM']['Mgmt']['Case']['OnlyNDaysLeft'][0].'<span class="expiry_date" id="expiry_date">'.floor($dayLeft).'</span>'.$Lang['ePCM']['Mgmt']['Case']['OnlyNDaysLeft'][1].'</span></td>';
											 				}
								 						}
								 				$x .= '</tr>';
								 				if($emailNotificationEnable||$eClassAppNotificationEnable){
									 				$x .= '<tr>';
											 			$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['AutoNotify'].'</td>';
											 			$x .= '<td>:</td>';
											 			$notificationDate = $notiSchedules[2]['ScheduledDate'];
											 			$x .= '<td>';

											 				$x .= '<div id="Notification_div" style="display:inline-block">';
											 				if(empty($notiSchedules[2])||$notiSchedules[2]==''){
											 					$enableNotification = false;
											 					$datePickStyle = 'none';
											 					$x .= $Lang['ePCM']['General']['Disable'];
											 				}else{
											 					$enableNotification = true;
											 					//$x .= $Lang['ePCM']['General']['Enable'];
											 					$datePickStyle = 'inline-block';
											 					//$x .= '<br>';
											 					$x .= $notificationDate;
											 				}
											 				$x .= '</div>';
											 				if($libPCM_auth->checkSendQuotationInvitationRight($caseID)){
											 					$x.= '<a href="javascript:editNotification();" class="icon_edit_dim" id="NotificationEdit"></a>';
											 				}
											 				$x .= '<div id="Notification_edit_div" style="display:none">';
											 					$x .= $this->Get_Radio_Button('notification_yes', 'notification', $Value=1, ($enableNotification)?1:0, $Class="", $Display=$Lang['ePCM']['General']['Enable'], $Onclick="toggleEnableNotification(this.value)",$isDisabled=0);
											 					$x .= $this->Get_Radio_Button('notification_no', 'notification', $Value=0, (!$enableNotification)?1:0, $Class="", $Display=$Lang['ePCM']['General']['Disable'], $Onclick="toggleEnableNotification(this.value)",$isDisabled=0);
											 					$x .= '<div id="datePickerNotificationDate_div" style="display:'.$datePickStyle.';padding-left:20px;" >';
											 						$x .= $this->GET_DATE_PICKER('datePickerNotificationDate', ($notificationDate!='')?$this->convertDatetimeToDate($notificationDate):date('Y-m-d',strtotime($quotationEndDate.'-3 days')),'',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=true, $cssClass="textboxnum");
											 						$notificationDateAry = explode(' ',$notificationDate);
											 						$notiTime = $notificationDateAry[1];
											 						$x .= $this->Get_Time_Selection('timePickerNotificationDate', $notiTime, $others_tab='',$hideSecondSeletion=true, $intervalArr=array(1,15,1));
											 					$x .= '</div>';
											 					if($libPCM_auth->checkSendQuotationInvitationRight($caseID)){
											 						$x .= $this->GET_SMALL_BTN($Lang['Btn']['Submit'], "button", "sumbitNotiSchedule();", "", $ParOtherAttribute="style=\"margin-left:10px;\"", $OtherClass="", $ParTitle='');
											 					}
											 				$x .= '</div>';
											 			$x .= '</td>';
									 				$x .= '</tr>';
									 				$x .= '<tr>';
									 					$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['NotifyTarget'].'</td>';
									 					$x .= '<td>:</td>';
									 					$x .= '<td id="ajax_notified_user">' ;
										 					if($notiUsers[2]==''||empty($notiUsers[2])){
										 						$x .= $Lang['General']['EmptySymbol'];
										 					}
												 			$x .= $notiUsers[2]['Name'] ;

											 			$x .= '</td>';
										 			$x .= '</tr>';
								 				}
									 			$x .= '<tr>';
									 				$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['InvitationLetter'].'</td>';
									 				$x .= '<td>:</td>';
									 				$x .= '<td>';
									 				$x .= $this->getFileListUI($invDocInfoArray[2]);
									 					if($libPCM_auth->checkSendQuotationInvitationRight($caseID)){
									 						//$x .= $this->GET_SMALL_BTN($Lang['General']['PleaseSelectFiles'], "button", "goUploadInvitationDocument();", $pluploadButtonId, $ParOtherAttribute="", $OtherClass="", $ParTitle='');
									 						$x.= '<a href="javascript:goUploadInvitationDocument();" class="icon_edit_dim" id=""></a>';
									 					}
									 				$x .= '</td>';
									 			$x .= '</tr>';
// 									 			if($countOfQuotation<$minQuotation){
// 									 				$x .= '<tr>';
// 									 					$x .= '<td>'.$this->RequiredSymbol().$Lang['ePCM']['Mgmt']['Case']['Remarks'].'</td>';
// 									 					$x .= '<td>:</td>';
// 									 					$x .= '<td>';
// 									 					if($timeStatus=='current'){
// 									 						$x .= $this->GET_TEXTAREA('remarks_2', $taContents='', $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='remarks_2', $CommentMaxLength='');
// 									 					}else{
// 									 						$x .= Get_String_Display($remarks[2]['Remark']);
// 									 					}
// 									 					$x .= '</td>';
// 									 				$x .= '</tr>';
// 									 			}

								 			$x .= '</tbody>';
							 			$x .= '</table>';
						 			}
						 			$countOfQuotation = count($quotationInfoAry);
						 			if($countOfQuotation>0){
						 				$InvitationOfSuppliersStr = $Lang['ePCM']['Mgmt']['Case']['InvitationOfSuppliersStr'];
						 				$InvitationOfSuppliersStr = str_replace('{-numOfQuotation-}',$countOfQuotation,$InvitationOfSuppliersStr);
						 				$x .= '<p>'.$InvitationOfSuppliersStr.'</p>';
						 			}else{
						 				//
						 			}

						 			$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="8" class="table_type2">';
						 				if($quotationType!=='V'){
						 					$x .= '<colgroup><col width="" /><col width="20%" /><col width="10%" /><col width="10%" /><col width="10%" /><col width="10%" /><col width="20" /><col width="10%" /></colgroup>';
						 				}
						 				$hide = ($quotationType=='V')?'style="display:none"':'';
						 				$x .= '<tbody valign="top">';
						 					$x .= '<tr>';
												$x .= '<th>&nbsp;</th>';
												  $x .= '<th>'.$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier'].'</th>';
												  $x .= '<th>'.$Lang['ePCM']['SettingsArr']['SupplierArr']['Email'].'</th>';
												  $x .= '<th>'.$Lang['ePCM']['SettingsArr']['SupplierArr']['Phone'].'</th>';
												  $x .= '<th>'.$Lang['ePCM']['SettingsArr']['SupplierArr']['Fax'].'</th>';

												  	$x .= '<th '.$hide.'>'.$Lang['ePCM']['Mgmt']['Quotation']['ContactMethod'].'</th>';
												  	$x .= '<th '.$hide.'>&nbsp;</th>';
												  	$x .= '<th '.$hide.'>'.$Lang['ePCM']['Mgmt']['Case']['Supplier']['ResponseStatus'].'</th>';
												  	$x .= '<th '.$hide.'>'.$Lang['ePCM']['Mgmt']['Case']['Supplier']['ResponseDate'].'</th>';

												  $x .= '<th>&nbsp;</th>';
											$x .= '</tr>';
	//										if(empty($quotationInfoAry)||$quotationInfoAry==''){
	//											$quotationInfoAry = array();
	//										}
											$counter = 1;
											$replyStatusAry = array(array('0',$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Pending']),array('1',$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Accepted']),array('-1',$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Rejected']));
                                            $contactMethodAry = array(
                                                array('R', $Lang['ePCM']['SettingsArr']['SupplierArr']['RegisteredLetter']),
                                                array(
                                                    'E',
                                                    $Lang['ePCM']['SettingsArr']['SupplierArr']['Email']
                                                ),
                                                array('P', $Lang['ePCM']['SettingsArr']['SupplierArr']['Phone']),
                                                array('F', $Lang['ePCM']['SettingsArr']['SupplierArr']['Fax']),
                                                array('I', $Lang['ePCM']['SettingsArr']['SupplierArr']['InPerson'])
                                            );

											foreach((array)$quotationInfoAry as $quotationInfo){
												$_quotationStatus = $quotation_status[$quotationInfo['ReplyStatus']];
												$_quotationResponseDate = ($quotationInfo['ReplyDate']>1)?$this->convertDatetimeToDate($quotationInfo['ReplyDate']):'<span class="grey_text">-</span>';
                                                $quotationsAry[$quotationInfo['QuotationID']]['SupplierName'] = $quotationInfo['SupplierName'];
                                                $quotationsAry[$quotationInfo['QuotationID']]['SupplierNameChi'] = $quotationInfo['SupplierNameChi'];
                                                $quotationsAry[$quotationInfo['QuotationID']]['Phone'] = $quotationInfo['Phone'];
                                                $quotationsAry[$quotationInfo['QuotationID']]['Quantity'] = 1;

												$x .= '<tr>';
													$x.= '<td>'.$counter.'</td>';
													$x.= '<td id="supplierName_'.$quotationInfo['QuotationID'].'">'.$quotationInfo[Get_Lang_Selection('SupplierNameChi','SupplierName')].'</td>';
													$x.= '<td>'.$quotationInfo['Email'].'</td>';
													$x.= '<td>'.$quotationInfo['Phone'].'</td>';
													$x.= '<td>'.$quotationInfo['Fax'].'</td>';
														$x.= '<td '.$hide.'>';
															$x .= '<div class="ajax_edit_contact_method" id="ajax_edit_contact_method_'.$quotationInfo['QuotationID'].'"  style="display:inline-block">';
																$x.= $quotation_contact_method[$quotationInfo['ContactMethod']];
															$x .= '</div>';
															$x.= getSelectByArray($contactMethodAry, $tags='name="selContactMethod_'.$quotationInfo['QuotationID'].'" id="selContactMethod_'.$quotationInfo['QuotationID'].'" class="selContactMethod" style="display:none" onchange="saveContactMethod('.$quotationInfo['QuotationID'].')"', $quotationInfo['ContactMethod'], $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);
															if($libPCM_auth->checkSendQuotationInvitationRight($caseID)){
																$x.= '<a href="javascript:editContactMethod('.$quotationInfo['QuotationID'].');" class="icon_edit_dim edit_icon_contact_method" style="float:right" id="edit_icon_contact_method_'.$quotationInfo['QuotationID'].'"></a>';
															}
														$x.= '</td>';
														$x.= '<td '.$hide.' id="icon_td_'.$quotationInfo['QuotationID'].'">';
															$x.= $quotation_status_icon_html[$quotationInfo['ReplyStatus']];
														$x.= '</td>';
														$x.= '<td '.$hide.'>';
															$x.= '<div class="ajax_edit_reply_status" id="ajax_edit_reply_status_'.$quotationInfo['QuotationID'].'" style="display:inline-block">'.$_quotationStatus.'</div>';
															$x.= getSelectByArray($replyStatusAry, $tags='name="selReplyStatus_'.$quotationInfo['QuotationID'].'" id="selReplyStatus_'.$quotationInfo['QuotationID'].'" class="selReplyStatus" style="display:none" onchange="saveReplyStatus('.$quotationInfo['QuotationID'].')"', $quotationInfo['ReplyStatus'], $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
															if($timeStatus!=''){
																if(!$isDeleted){
																	if($libPCM_auth->checkSendQuotationInvitationRight($caseID)){
																		$x.= '<a href="javascript:editReplyStatus('.$quotationInfo['QuotationID'].');" class="icon_edit_dim edit_icon_reply_status" style="float:right" id="edit_icon_reply_status_'.$quotationInfo['QuotationID'].'"></a>';
																	}
																}
															}
															$x.= '</td>';
                                                        $x.= '<td '.$hide.' style>';
														    $x.= '<div '.$hide.' class="ajax_replyDate" id="ajax_replyDate_'.$quotationInfo['QuotationID'].'" style="display:inline-block ">'.$_quotationResponseDate.'</div>';
                                                            $x .= '<div id="selInputReplyDate_'.$quotationInfo['QuotationID'].'_div" class="selInputReplyDate" style="display:none" >';
                                                            if($quotationInfo['ReplyStatus']==0){$_quotationResponseDate='';}
                                                            $x.= $this->GET_DATE_PICKER('selInputReplyDate_'.$quotationInfo['QuotationID'], $_quotationResponseDate,'',$DateFormat="yy-mm-dd",'','','',"saveInputReplyDate(".$quotationInfo['QuotationID'].")");
                                                            $x.='<script>';
                                                            $x.='document.getElementById("selInputReplyDate_'.$quotationInfo['QuotationID'].'").addEventListener("blur", function(){
                                                                setTimeout(function() {saveInputReplyDate('.$quotationInfo['QuotationID'].')}, 200)
                                                            });';
                                                            $x.='</script>';
                                                            $x.='<input type="hidden" id="lastInputReplyDate_'.$quotationInfo['QuotationID'].'" name="lastInputReplyDate_'.$quotationInfo['QuotationID'].'" value="'.$quotationInfo['ReplyDate'].'">';
                                                            $x.= '</div>';
                                                            if($timeStatus!=''){
                                                                if(!$isDeleted){
                                                                    if($libPCM_auth->checkSendQuotationInvitationRight($caseID)){
                                                                        if($quotationInfo['ReplyStatus']==0){$display = 'display:none;';                                                                        }
                                                                        $x.= '<a href="javascript:editInputReplyDate('.$quotationInfo['QuotationID'].');" class="icon_edit_dim edit_icon_reply_Date" style="float:right;'.$display.'" id="edit_icon_reply_Date_'.$quotationInfo['QuotationID'].'"></a>';
                                                                    }
                                                                }
                                                            }
                                                $x.= '</td>';
//                                                $x .= '<td>';
//                                                $x.= '</td>';
                                                $x .= '<td>';
														if($timeStatus=='current'){
															if($libPCM_auth->checkSendQuotationInvitationRight($caseID)){
																$x .= '<a href="javascript:deleteQuotes('.$quotationInfo['QuotationID'].');" class="delete_dim"></a>';
															}
														}
													$x .= '</td>';
												$x .= '</tr>';
												$counter++;
											}
											//$numOfQuote = count($quotationInfoAry);
											$x .= '</tbody>';
						 			$x .= '</table>';

						 			$x .= '<div class="table_board">';
						 			if($countOfQuotation<$minQuotation){
						 				if($timeStatus=='current'){
						 					$contentMsg = $Lang['ePCM']['Mgmt']['Case']['Warning']['NotEnoughSupplier'];
						 					$contentMsg = str_replace ('{-MinQuote-}',$minQuotation,$contentMsg);
						 					$x .= '<hr>';
						 					$x .= $this->GET_WARNING_TABLE($err_msg='', $DeleteItemName='', $DeleteButtonName='', $contentMsg,$Lang['ePCM']['General']['Notice']);
						 					$x .= '<hr>';
						 				}
						 				if($countOfQuotation > 0){
							 				$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_type1">';
								 			$x .= '<colgroup><col width="180" /><col width="30" /></colgroup>';
							 				$x .= '<tr>';
							 				$x .= '<td>'.(($timeStatus=='current')?$this->RequiredSymbol():'').$Lang['ePCM']['Mgmt']['Case']['Supplier']['Reason'] .'</td>';
							 				$x .= '<td>:</td>';
							 				$x .= '<td>';
							 				if($timeStatus=='current'){
							 					$x .= $this->GET_TEXTAREA('remarks_2', $taContents='', $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='remarks_2', $CommentMaxLength='');
							 				}else{
							 					// 2016-10-12	Villa fix the "Reason cannot be display" problem
							 					$x .= Get_String_Display($remarks[$stageID[0]]['Remark']);
							 				}
							 				$x .= '</td>';
							 				$x .= '</tr>';
							 				$x .= '</table>';
						 				}
						 			}

						 			if(!$isDeleted){
							 			if($timeStatus=='current'){
							 				if($libPCM_auth->checkSendQuotationInvitationRight($caseID)){
							 					$x .= '<a href="javascript: addNewQuote();" class="tablelink add_new"><strong>&#43;</strong> '.$Lang['ePCM']['Mgmt']['Case']['Supplier']['AddInvitation'].'</a>';
							 					if($quotationType=='V'){
							 						$editTableBtn = '<a href="javascript: goPriceComparison()" class="tablelink add_new"><strong>&#43;</strong> '.$Lang['ePCM']['Mgmt']['Case']['InputPriceComparison'].'</a>';
						 							$x .= '<br><br>'.$editTableBtn;
						 							$x .= $priceComparisonTable;
							 					}
							 				}
							 			}else{
							 				//$x .= '<span class="add_new grey_text"><strong>&#43;</strong> '.$Lang['ePCM']['Mgmt']['Case']['Supplier']['AddInvitation'].'</span>';
							 				if($quotationType=='V'){
							 					if($_PAGE['libPCM']->isAdmin()){
							 						$x .= '<a href="javascript: goPriceComparison()" class="tablelink add_new"><strong>&#43;</strong> '.$Lang['ePCM']['Mgmt']['Case']['InputPriceComparison'].'</a>';
							 					}
							 					$x .= $priceComparisonTable;
							 				}
							 			}
						 			}

						 			$x .= '</div>';

							 		if($timeStatus == 'current'&&$countOfQuotation>0){
							 			if($libPCM_auth->checkSendQuotationInvitationRight($caseID)){

											$latest_log_time='';
											foreach($log as $log_element){
												$latest_log_time=$log_element['LogTime'];
											}

											$log_message=$Lang['ePCM']['Mgmt']['Case']['Latest Email Sent Date'].':&nbsp;<i id="latest_email_sent_date">'.substr($latest_log_time,0,10).'</i>';

											$x .= '<div id="editBottomDiv" class="edit_bottom_v30">';
											if($quotationType == 'Q'||$quotationType == 'T'){
												$x .= $this->GET_ACTION_BTN($Lang['ePCM']['Mgmt']['Email']['Preview'], 'button', $ParOnClick="previewEmail();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="")."\r\n";
												$x .= $this->GET_ACTION_BTN($latest_log_time==''?$Lang['ePCM']['Mgmt']['Case']['EmailToSupplier']:$Lang['ePCM']['Mgmt']['Case']['Re-send Invitation Email'], 'button', $ParOnClick="sendEmailInvitation();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="sendEmailInvitation");
												$x .= ' ';
											}
											if($quotationType!=='V'){
												//go stage 3 directly
												$js_function = "finishStage2();";
											}else{
												//go stage 4 directly
												$js_function = "finishStage3();";
											}
											$x .= $this->GET_ACTION_BTN($Lang['ePCM']['Mgmt']['Case']['ToNextStage'], 'button', $ParOnClick=$js_function, $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
											$x .= '<div style="width:300px;margin:auto;"><span id="NoEmailWarnDiv" class="warnMsgDiv" style="text-align:center;width:100%;display:none;">'.$Lang['ePCM']['Mgmt']['Case']['No Emails are sent'].'</span></div>';
											$x .= '<div style="width:300px;margin:auto;"><span id="EmailSentWarnDiv" class="warnMsgDiv" style="text-align:center;width:100%;display:none;">'.$Lang['ePCM']['Mgmt']['Case']['Emails are sent'].'</span></center></div>';
											$x .= '<div style="width:500px;margin:auto;"><span id="div_latest_email_sent_date" style="text-align:center;width:100%;'.($latest_log_time==''?'display:none;':'').'">'.$log_message.'</span></div>';
											$x .= '</div>';
							 			}
									}

						 		$x .= '</div>';
					 		}
					 		// End of step_box_2

					 		//step_box_3
					 		$thisBoxStage = 3;
					 		if($currentStage==$thisBoxStage){
					 			$timeStatus = 'current';
					 		}else if($currentStage>$thisBoxStage){
					 			$timeStatus = 'past';
					 		}else{
					 			$timeStatus = '';
					 		}
					 		if($quotationType!=='V'){
					 			//2016-06-29 hide for vebal quotation
						 		if(!($timeStatus==''&& ($isDeleted||$isCaseRejected))){
							 		$x .= '<div class="step_box '.$timeStatus.'" id="step_box_'.$thisBoxStage.'">';
							 				$x .= '<div class="box_title"><span>'.++$steps_count.'</span>';
												$x .= ($quotationType=='Q'||$quotationType=='V')?$Lang['ePCM']['Mgmt']['Case']['UploadQuoation']:$Lang['ePCM']['Mgmt']['Case']['TenderOpening'];
											$x .= '</div>';
											if($quotationType=='T'||$quotationType=='Q'){
												$x .= '<div class="remarks"><table><tr><td style="vertical-align:text-top;width:70px;"><strong>'.$Lang['ePCM']['General']['Notice'].':</strong></td><td>'.$step_remarks[$thisBoxStage].'</td></tr></table></div>';
											}
					                        if($quotationType=='T'){
						                        $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_type1">';
													$x .= '<colgroup><col width="180" /><col width="30" /></colgroup>';
								 					$x .= '<tbody valign="top">';
								 						$x .= '<tr>';
															$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['TenderOpeningStartDate'].'</td>';
															$x .= '<td>:</td>';
															$tenderOpeningStartDate = ($tenderOpeningStartDate!='')?date("Y-m-d",strtotime($tenderOpeningStartDate)):$Lang['General']['EmptySymbol'];
															$x .= '<td>';
																$x .= '<div id="TenderOpeningStartDate_div" style="display:inline-block">'.$tenderOpeningStartDate.'</div>';
																$x .= '<div id="datePickerTenderOpeningStartDate_div" style="display:none">';
																$x .= $this->GET_DATE_PICKER('datePickerTenderOpeningStartDate', $tenderOpeningStartDate,'',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="datePickerTenderOpeningStartDateSelected();",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=true, $cssClass="textboxnum");
																$x .= '</div>';
																if(!$isDeleted){
																	if($libPCM_auth->checkInputQuotationResponseRight($caseID)){
																		$x .= '<a href="javascript:editDatePicker(\'TenderOpeningStartDate\');" class="icon_edit_dim" id="TenderOpeningStartDateEdit"></a>';
																	}
																}
															$x .= '</td>';
														$x .= '</tr>';
														$x .= '<tr>';
															$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['TenderOpeningEndDate'].'</td>';
															$x .= '<td>:</td>';
															$tenderOpeningEndDate_date = ($tenderOpeningEndDate!='')?date("Y-m-d",strtotime($tenderOpeningEndDate)):$Lang['General']['EmptySymbol'];

															$x .= '<td>';
																$x .= '<div id="TenderOpeningEndDate_div" style="display:inline-block">'.$tenderOpeningEndDate_date.'</div>';
																$x .= '<div id="datePickerTenderOpeningEndDate_div" style="display:none">';
																$x .= $this->GET_DATE_PICKER('datePickerTenderOpeningEndDate', $tenderOpeningEndDate_date,'',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="datePickerTenderOpeningEndDateSelected();",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=true, $cssClass="textboxnum");
																$x .= '</div>';
																if(!$isDeleted){
																	if($libPCM_auth->checkInputQuotationResponseRight($caseID)){
																		$x .= '<a href="javascript:editDatePicker(\'TenderOpeningEndDate\');" class="icon_edit_dim" id="TenderOpeningEndDateEdit"></a>';
																	}
																}
															$x .= '</td>';
														$x .= '</tr>';
														$x .= '<tr class="top_border">';
															$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Declaration'].'</td>';
															$x .= '<td>:</td>';
															$x .= '<td>';
																$x .= '<span class="grey_text"></span>';
																if(!$isDeleted){
																	if($libPCM_auth->checkInputQuotationResponseRight($caseID)){
																		$x .= $this->getFileListUI($decDocInfoArray[4]);
																		$x .= '<a href="javascript:goTenderOpeningForm();" class="icon_edit_dim"></a>';
																	}
																}
															$x .= '</td>';
														$x .= '</tr>';
								 					$x .= '</tbody>';
								 				$x .= '</table>';

					                        }
					                        if($quotationType=='T'){
							 					$x .= ' <p>'.$Lang['ePCM']['Mgmt']['Case']['UploadTenders'].'</p>';
					                        }else if($quotationType=='Q'||$quotationType=='V'){
					                        	$x .= ' <p>'.$Lang['ePCM']['Mgmt']['Case']['UploadQuoation'].'</p>';
					                        }
							 				$x .= '<div class="table_box">';
						                        $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="6" class="table_type2 inner_lines">';
													$x .= '<tbody valign="top">';
													$x .= '<tr>';
															$x .= '<th>&nbsp;</th>';
															$x .= '<th>'.$Lang['ePCM']['SettingsArr']['SupplierArr'][Get_Lang_Selection('Supplier Name Chi','Supplier Name')].'</th>';
															if($quotationType=='T'){
																$x .= '<th>'.$Lang['ePCM']['Mgmt']['Case']['TenderDate'].'</th>';
															}else{
																$x .= '<th>'.$Lang['ePCM']['Mgmt']['Case']['QuotationDate'].'</th>';
															}
															$x .= '<th>'.$Lang['ePCM']['Mgmt']['Case']['Document'].'</th>';
															$x .= '<th>'.$Lang['ePCM']['Mgmt']['Case']['Remarks'].'</th>';
															$x .= '<th>'.$Lang['ePCM']['Mgmt']['Case']['LastModified'].'</th>';
															$x .= '<th>&nbsp;</th>';
													$x .= '</tr>';
													$counter = 1;
													foreach((array)$quotationInfoAry as $_quotation){
													    $_thisQuoId = $_quotation['QuotationID'];
													    $_quotationFiles = $quoFilesAssocArr[$_thisQuoId];
														$_responseDateTd ='';
														if($_quotation['QuoteDate']==''){
															if($_quotation['ReplyStatus']==-1){
																$_responseDateTd =$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Rejected'];
															}else{
																$_responseDateTd =$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Pending'];
															}
														}else if($_quotation['QuoteDate'] > $tenderOpeningEndDate && $tenderOpeningEndDate!=''){
															$_responseDateTd = $_quotation['QuoteDate'].' ('.$Lang['ePCM']['Mgmt']['Case']['LateSubmition'].')';
														}else{
															$_responseDateTd = $_quotation['QuoteDate'];
														}

														//$_docTd = ($_quotation['hasDoc'])?'<a href="javascript:clickQuotationDoc('.$_quotation['quotationID'].');">Click (need someone confrim design)</a> ':'';
														$_docTd = $this->getFileListUI($_quotationFiles);
														$x .= '<tr>';
															$x .= '<td>'.$counter.'</td>';
															$x .= '<td>'.$_quotation[Get_Lang_Selection('SupplierNameChi','SupplierName')].'</td>';
															$x .= '<td>';
																$_responseDateSpanClass = ($timeStatus!='current')?'grey_text':'';
																$x .= '<span class="'.$_responseDateSpanClass.'">';
																	$x .= $_responseDateTd;
																$x .= '</span>';
															$x .= '</td>';
															$x .= '<td>'.$_docTd.'</td>';
															$x .= '<td>'.Get_String_Display($_quotation['Remarks']).'</td>';
															$x .= '<td>';
																$x .= ($_quotation['QuoteDate']=='')?'':$_quotation['DateModified'] .' <br>('.$_quotation['ModifiedByUserName'].')';
															$x .= '</td>';

															$x .= '<td>';
															if(!$isDeleted){
																if($libPCM_auth->checkInputQuotationResponseRight($caseID)){
																	$x .= '<a href="javascript:editQuotation('.$_quotation['QuotationID'].');" class="icon_edit_dim"></a>';
																}
															}
															$x .= '</td>';
														$x .= '</tr>';
														$counter++;
													}


													$x .= '</tbody>';
												$x .= '</table>';
												if($timeStatus=='current'){
													if($libPCM_auth->checkInputQuotationResponseRight($caseID)){
													$x .= '<a href="javascript: goPriceComparison()" class=""><strong>&#43;</strong> '.$Lang['ePCM']['Mgmt']['Case']['InputPriceComparison'].'</a>';
// 													$x .= $priceComparisonTable;
													}
												}else{
													if($_PAGE['libPCM']->isAdmin()){
														$x .= '<a href="javascript: goPriceComparison()" class=""><strong>&#43;</strong> '.$Lang['ePCM']['Mgmt']['Case']['InputPriceComparison'].'</a>';
													}
												}
												$x .= $priceComparisonTable;
												if(!$isDeleted){
													if($timeStatus == 'current'){
														if($libPCM_auth->checkInputQuotationResponseRight($caseID)){
															$x .= '<div id="editBottomDiv" class="edit_bottom_v30">';
																$x .= $this->GET_ACTION_BTN($Lang['ePCM']['Mgmt']['Case']['ToNextStage'], 'button', $ParOnClick="finishStage3();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
															$x .= '</div>';
														}
													}
												}
											$x .= '</div>';
							 		$x .= '</div>';
						 		}
					 		}
					 		//End of step_box 3
					 	}//End of quotationType = N hiding
					 	//step_box 4
					 	$thisBoxStage = 4;
					 	if($currentStage==$thisBoxStage){
					 		$timeStatus = 'current';
					 	}else if($currentStage>$thisBoxStage){
					 		$timeStatus = 'past';
					 	}else{
					 		$timeStatus = '';
					 	}
					 	if(!($timeStatus==''&& ($isDeleted||$isCaseRejected))){
						 	$x .= '<div class="step_box '.$timeStatus.'" id="step_box_'.$thisBoxStage.'">';
						 		$x .= '<div class="box_title"><span>'.++$steps_count.'</span>';
								if($quotationType=='T'){
									$x .= $Lang['ePCM']['Setting']['Rule']['TenderApproval'];
								}else{
									$x .= $Lang['ePCM']['Mgmt']['Case']['InputResult'];
								}
								$x .= '</div>';
								if($quotationType!='N'){
									$x .= '<div class="remarks"><table><tr><td style="vertical-align:text-top;width:70px;"><strong>'.$Lang['ePCM']['General']['Notice'].':</strong></td><td>'.$step_remarks[$thisBoxStage].'</td></tr></table></div>';
								}

						 		if($quotationType=='T'){
							 		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_type1">';
										$x .= '<colgroup><col width="180" /><col width="30" /></colgroup>';
								 			$x .= '<tbody valign="top">';
											$x .= '<tr>';
						                           $x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['TenderApprovalStartDate'].'</td>';
						                           $x .= '<td>:</td>';
						                           $tenderApprovalStartDate = ($tenderApprovalStartDate!='')?date("Y-m-d",strtotime($tenderApprovalStartDate)):$Lang['General']['EmptySymbol'];
						                           $x .= '<td>';
						                           	$x .= '<div id="TenderApprovalStartDate_div" style="display:inline-block">'.$tenderApprovalStartDate.'</div>';
						                           	$x .= '<div id="datePickerTenderApprovalStartDate_div" style="display:none">';
						                           	$x .= $this->GET_DATE_PICKER('datePickerTenderApprovalStartDate', $tenderApprovalStartDate,'',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="datePickerTenderApprovalStartDateSelected();",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=true, $cssClass="textboxnum");
						                           	$x .= '</div>';
						                           	if(!$isDeleted){
						                           		if($libPCM_auth->checkInputResultRight($caseID)){
						                           			$x .= '<a href="javascript:editDatePicker(\'TenderApprovalStartDate\');" class="icon_edit_dim" id="TenderApprovalStartDateEdit"></a>';
						                           		}
						                           	}
						                           $x .= '</td>';
						                         $x .= '</tr>';
						                         $x .= '<tr>';
						                           $x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['TenderApprovalEndDate'].'</td>';
						                           $x .= '<td>:</td>';
						                           $tenderApprovalEndDate = ($tenderApprovalEndDate!='')?date("Y-m-d",strtotime($tenderApprovalEndDate)):$Lang['General']['EmptySymbol'];
						                           $x .= '<td>';
						                           $x .= '<div id="TenderApprovalEndDate_div" style="display:inline-block">'.$tenderApprovalEndDate.'</div>';
						                           $x .= '<div id="datePickerTenderApprovalEndDate_div" style="display:none">';
						                           $x .= $this->GET_DATE_PICKER('datePickerTenderApprovalEndDate', $tenderApprovalEndDate,'',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="datePickerTenderApprovalEndDateSelected();",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=true, $cssClass="textboxnum");
						                           $x .= '</div>';
						                           if(!$isDeleted){
						                           		if($libPCM_auth->checkInputResultRight($caseID)){
						                           			$x .= '<a href="javascript:editDatePicker(\'TenderApprovalEndDate\');" class="icon_edit_dim" id="TenderApprovalEndDateEdit"></a>';
						                           		}
						                           }
						                           $x .= '</td>';
						                         $x .= '</tr>';
						                         $x .= '<tr class="top_border">';
						                           $x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Declaration'].'</td>';
						                           $x .= '<td>:</td>';
						                           $x .= '<td>';
						                           		$x .= '<span class="grey_text"></span>';
						                           		if(!$isDeleted){
						                           			if($libPCM_auth->checkInputResultRight($caseID)){
						                           				$x .= $this->getFileListUI($decDocInfoArray[5]);
						                           				$x .= ' <a href="javascript:goTenderApprovalForm()" class="icon_edit_dim"></a>';
						                           			}
						                           		}
						                           $x .= '</td>';
						                        $x .= '</tr>';
											$x .= '</tbody>';
							 		$x .= '</table>';
						 		}
	//					 		$x .= '<div class="result">';
	//								$x .= '<span class="grey_text icon_result_dim">'.$Lang['ePCM']['Mgmt']['Case']['InputResult'].'</span>';
	//							$x .= '</div>';
						 		$x .= '<div class="result">';
						 		if(!(empty($result)||$result=='')){
						 			$resultContent = '<table width="90%" border="0" cellspacing="0" cellpadding="6" class="">';
							 			$resultContent .= '<colgroup>';
							 			$resultContent .= '<col width="165" />';
							 			$resultContent .= '<col width="30" />';
							 			$resultContent .= '</colgroup>';
						 				$resultContent .= '<tr>';
						 					$resultContent .= '<td>';
						 						$resultContent .= $Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier'];
						 					$resultContent .= '</td>';
						 					$resultContent .= '<td>';
						 						$resultContent .= ':';
						 					$resultContent .= '</td>';
						 					$resultContent .= '<td>';
						 						$resultContent .= Get_Lang_Selection($result['SupplierNameChi'],$result['SupplierName']);
						 					$resultContent .= '</td>';
						 				$resultContent .= '</tr>';
						 				$resultContent .= '<tr>';
						 					$resultContent .= '<td>';
						 						$resultContent .= $Lang['ePCM']['Mgmt']['Case']['Result']['DealPrice'];
						 					$resultContent .= '</td>';
						 					$resultContent .= '<td>';
						 						$resultContent .= ':';
						 					$resultContent .= '</td>';
						 					$resultContent .= '<td>';
						 						$resultContent .= $result['DealPrice'];
						 					$resultContent .= '</td>';
						 				$resultContent .= '</tr>';

						 				//by villa
						 				//if (isset($result['FundingSource1']) ){
						 				if ( $result['FundingSource1'] != 0 ){
						 					$resultContent .= '<tr>';
						 						$resultContent .= '<td>';
						 							$resultContent .= $Lang['ePCM']['Mgmt']['Case']['Result']['FundingSource'].'(1)';
						 						$resultContent .= '</td>';
						 						$resultContent .= '<td>';
						 							$resultContent .= ':';
						 						$resultContent .= '</td>';
						 						$resultContent .= '<td>';
						 							for($i = 0; $i < count($fundingList_arr); $i++){
						 								if($fundingList_arr[$i]['FundingSourceID']== $result['FundingSource1']){
						 									$resultContent .= $fundingList_arr[$i][1];
						 								}
						 							}

						 						$resultContent .= '</td>';
						 					$resultContent .= '</tr>';

						 					$resultContent .= '<tr>';
						 						$resultContent .= '<td>';
						 							$resultContent .= $Lang['ePCM']['Mgmt']['Funding']['Unit_Price'].'(1)';
						 						$resultContent .= '</td>';
						 						$resultContent .= '<td>';
						 							$resultContent .= ':';
						 						$resultContent .= '</td>';
						 						$resultContent .= '<td>';
						 							$resultContent .= $result['UnitPrice1'];
						 						$resultContent .= '</td>';
						 					$resultContent .= '</tr>';

						 					//if (isset($result['FundingSource2']) ){
						 					if ( $result['FundingSource2'] != 0 ){
						 						$resultContent .= '<tr>';
						 							$resultContent .= '<td>';
						 								$resultContent .= $Lang['ePCM']['Mgmt']['Case']['Result']['FundingSource'].'(2)';
						 							$resultContent .= '</td>';
						 							$resultContent .= '<td>';
						 								$resultContent .= ':';
						 							$resultContent .= '</td>';
						 							$resultContent .= '<td>';
						 								for($i = 0; $i < count($fundingList_arr); $i++){
						 									if($fundingList_arr[$i]['FundingSourceID']== $result['FundingSource2']){
						 										$resultContent .= $fundingList_arr[$i][1];

						 									}
						 								}

						 							$resultContent .= '</td>';
						 						$resultContent .= '</tr>';

						 						$resultContent .= '<tr>';
						 							$resultContent .= '<td>';
						 								$resultContent .= $Lang['ePCM']['Mgmt']['Funding']['Unit_Price'].'(2)';
						 							$resultContent .= '</td>';
						 							$resultContent .= '<td>';
						 								$resultContent .= ':';
						 							$resultContent .= '</td>';
						 							$resultContent .= '<td>';
						 								$resultContent .= $result['UnitPrice2'];
						 							$resultContent .= '</td>';
						 						$resultContent .= '</tr>';
						 					}

						 				}



						 				// hehehe
						 				if(!empty($remarks[4])){
							 				$resultContent .= '<tr>';
								 				$resultContent .= '<td>';
								 				$resultContent .= $Lang['ePCM']['Mgmt']['Case']['Result']['Description'];
								 				$resultContent .= '</td>';
								 				$resultContent .= '<td>';
								 				$resultContent .= ':';
								 				$resultContent .= '</td>';
								 				$resultContent .= '<td>';
								 				$resultContent .= Get_String_Display($remarks[4]['Remark']);
								 				$resultContent .= '</td>';
							 				$resultContent .= '</tr>';
						 				}
						 				$resultContent .= '<tr>';
						 					$resultContent .= '<td>';
						 						$resultContent .= $Lang['ePCM']['Mgmt']['Case']['Result']['Description'];
						 					$resultContent .= '</td>';
						 					$resultContent .= '<td>';
						 						$resultContent .= ':';
						 					$resultContent .= '</td>';
						 					$resultContent .= '<td>';
						 						$resultContent .= Get_String_Display($result['Description']);
						 					$resultContent .= '</td>';
						 				$resultContent .= '</tr>';
						 				$resultContent .= '<tr>';
						 					$resultContent .= '<td>';
						 						$resultContent .= $Lang['ePCM']['Mgmt']['Case']['Result']['DealDate'];
						 					$resultContent .= '</td>';
						 					$resultContent .= '<td>';
						 						$resultContent .= ':';
						 					$resultContent .= '</td>';
						 					$resultContent .= '<td>';
						 						$resultContent .= $result['DealDate'];
						 					$resultContent .= '</td>';
						 				$resultContent .= '</tr>';
						 				$resultContent .= '<tr>';
							 				$resultContent .= '<td>';
							 					$resultContent .= $Lang['ePCM']['Mgmt']['Case']['Document'];
							 				$resultContent .= '</td>';
							 				$resultContent .= '<td>';
							 					$resultContent .= ':';
							 				$resultContent .= '</td>';
							 				$resultContent .= '<td>';
							 					$resultContent .= $this->getFileListUI($docInfoArray[5]);
							 				$resultContent .= '</td>';
						 				$resultContent .= '</tr>';
						 				$resultContent .= '<tr>';
							 				$resultContent .= '<td>';
							 					$resultContent .= $Lang['ePCM']['Mgmt']['Case']['LastModified'];
							 				$resultContent .= '</td>';
							 				$resultContent .= '<td>';
							 					$resultContent .= ':';
							 				$resultContent .= '</td>';
							 				$resultContent .= '<td>';
							 					$resultContent .= $result[DateModified].' ('.$result['ModifyUserName'].')';
							 				$resultContent .= '</td>';
						 				$resultContent .= '</tr>';
						 			$resultContent .= '</table>';

                                    $resultForExportAry['SupplierID'] = $result['SupplierID'];
                                    $resultForExportAry['Price'] = $this->displayMoneyFormat($result['DealPrice']);
                                    $resultForExportAry['Accepted'] = true;
                                    $resultForExportAry['SupplierName'] = $result['SupplierName'];
                                    $resultForExportAry['SupplierNameChi'] = $result['SupplierNameChi'];
                                    $resultForExportAry['NotCheapestReason'] = $remarks[4]['Remark'];
                                    $resultForExportAry['Description'] = $result['Description'];
                                    $resultForExportAry['Quantity'] = 1;
                                    foreach((array)$quotationsAry as $quotationID => $quotation){
                                        ($quotation['SupplierName']==$resultForExportAry['SupplierName'] && $quotation['SupplierNameChi']==$resultForExportAry['SupplierNameChi'])?$quotationsAry[$quotationID]['Accepted'] = true : "";
                                    }
						 		}else{
						 			$resultContent = $Lang['ePCM']['Mgmt']['Case']['InputResult'];
						 		}
						 		if($timeStatus=='past'){
						 			$x .= '<table>';
						 			$x .= '<tr>';
						 			$x .= '<td style="width:99%">';
						 			$x.= $resultContent;
						 			$x .= '</td>';
						 			$x .= '<td style="vertical-align: text-top;">';
						 			if($libPCM_auth->checkInputResultRight($caseID)){
						 				$x .= '<a href="javascript:setResult()" class="icon_edit_dim" style="margin-left:20px;min-height:15px"></a>';
						 			}
						 			$x .= '</td>';
						 			$x .= '</tr>';
						 			$x .= '</table>';
						 			//$x .= '<div class="icon_result_dim">'.$resultContent.'</div>';
						 		}else if($timeStatus==''){
						 			if(!$isDeleted){
						 				$x.= '<span class="grey_text">'.$resultContent.'</span>';
						 				if($libPCM_auth->checkInputResultRight($caseID)){
						 					$x .= '<span class="grey_text icon_result_dim" style="margin-left:20px;min-height:15px"></span>';
						 				}
						 				//$x .= '<span class="grey_text icon_result_dim">'.$resultContent.'</span>';
						 			}
						 		}else{
						 			if(!$isDeleted){
										//$x.= $resultContent;
										//$x .= '<a href="javascript:setResult()" class="icon_result"></a>';
						 				$x .= '<table>';

						 				$x .= '<tr>';
						 				$x .= '<td>';
						 				$x.= '<span class="grey_text" style="display:inline-block">'.$resultContent.'</span>';
						 				$x .= '</td>';
						 				$x .= '<td>';
						 				if($libPCM_auth->checkInputResultRight($caseID)){
						 					$x .= '<a href="javascript:setResult()" class="icon_edit_dim" style="margin-left:20px;min-height:15px;display:inline-block"></a>';
						 				}
						 				$x .= '</td>';
						 				$x .= '</tr>';
						 				$x .= '</table>';
						 				//$x .= '<a href="javascript:setResult()" class="icon_result">'.$resultContent.'</a>';
						 			}
						 		}
								$x .= '</div> ';
								if($quotationType=='T'||$quotationType=='Q'){
									if($timeStatus!=''){
										if($libPCM_auth->checkInputResultRight($caseID)||$libPCM_auth->caseOwner($caseID)){
											$x .= '<a href="javascript: downloadDocument(\'Summary\');" class="download">'.$Lang['ePCM']['Mgmt']['DownloadDocuments']['Summary']['Eng'][$quotationType].'</a>';
											$x .= '<br>&nbsp;';
										}

										$x .= '<div>';
											$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_type1">';
												$x .= '<colgroup><col width="180" /><col width="30" /></colgroup>';
												$x .= '<tr>';
													$x .= '<td>'.$Lang['ePCM']['Mgmt']['UploadDocuments']['Summary'].'</td>';
													$x .= '<td>:</td>';
													$x .= '<td>';
													if($libPCM_auth->checkInputResultRight($caseID)){
														$x .= $this->getFileListUI($invDocInfoArray[6]);
													}
													$x .= '<a href="javascript:goUploadFinalDocument();" class="icon_edit_dim" id=""></a>';
													$x .= '</td>';
												$x .= '</tr>';
											$x .= '</table>';
										$x .= '</div>';

									}
								}
						 	$x .= '</div>';
					 	}
					 	//End of step_box 4

					 	if( $quotationType=='N' || $sys_custom['ePCM_skipCaseApproval']){	//20160810 if Type = 'N' Skip
					 		//step_box 5
					 		$thisBoxStage = 5;
					 		if(intval($currentStage)==$thisBoxStage){
					 			$timeStatus = 'current';
					 		}else if(intval($currentStage)>$thisBoxStage){
					 			$timeStatus = 'past';
					 		}else{
					 			$timeStatus = '';
					 		}
					 		if(!($timeStatus==''&& $isDeleted)){	//Hide stage which not yet do for deleted case
					 			if($isCaseRejected) $additionClass = 'rejected';
					 			$x .= '<div class="step_box '.$timeStatus.' '.$additionClass.'" id="step_box_'.$thisBoxStage.'">';
					 			$x .= '<div class="box_title"><span>'.++$steps_count.'</span>'.$Lang['ePCM']['Mgmt']['Case']['Endorsement'].'</div>';
					 			$counter = 1;
					 			foreach((array)$caseApprovalAry as $caseApprovalInfo){
				 					// display approval role name
					 				$_approvalRoleId = $caseApprovalInfo['ApprovalRoleID'];
					 				$roleDisplay = ($_approvalRoleId > 0) ? Get_Lang_Selection($roleInfoAry[$_approvalRoleId]['RoleNameChi'], $roleInfoAry[$_approvalRoleId]['RoleName']) : $Lang['ePCM']['Group']['GroupHead'];

					 				$x .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_type1 with_icon">';
					 				$x .= '<colgroup>';
						 				$x .= '<col width="30" />';
						 				$x .= '<col width="135" />';
						 				$x .= '<col width="30" />';
						 				$x .= '<col width="200" />';
						 				$x .= '<col width="" />';
					 				$x .= '</colgroup>';
					 				$x .= '<thead valign="top">';
					 				$x .= '<tr>';
					 				$x .= '<th><img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/iPortfolio/scheme/icon_staff.gif" width="30" height="28"></th>';
					 				$x .= '<th style="padding-right:0px;">'.$Lang['ePCM']['Mgmt']['Case']['Endorser'].' #'.$counter.'<br>('.$roleDisplay.')</th>';
					 				$x .= '<th>:</th>';
					 				$x .= '<th>';
                                    if($caseApprovalInfo['IsDeletedApprover']){$requiredSymbol = $linterface->RequiredSymbol();}
                                    else{$requiredSymbol = '';}
					 				$x .= $requiredSymbol.$caseApprovalInfo['ApproverName'];
					 				$x .= '<br />';
					 				//$x .= str_replace($Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approved'], $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsed'], $case_approval_status[$caseApprovalInfo['RecordStatus']]);
					 				$x .= $case_approval_status[$caseApprovalInfo['RecordStatus']];
					 				$string = '';
					 				if($caseApprovalInfo['ModifiedBy'] != $caseApprovalInfo['ApproverUserID']){
					 					$string = '<br><span class="tabletextremark">'.str_replace('<!--adminName-->', $caseApprovalInfo['ModifiedByName'], $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['AdminAction']).'</span>';
					 				}
					 				$x .= ($caseApprovalInfo['RecordStatus']==0)? '' : ' ('.$this->convertDatetimeToDate($caseApprovalInfo['DateModified']).')'.$string;
					 				$x .= '</th>';
					 				$x .= '<th>';
					 				//Can edit only when you are the approver
					 				//if($_SESSION['UserID']==$caseApprovalInfo['ApproverUserID']){
					 				if(!$isDeleted){
					 					if($libPCM_auth->checkCaseApprovalRight($caseID,$caseApprovalInfo['ApproverUserID'])){
					 						if($currentStage >= 5){
// 					 							if( $caseAry['ApprovalStage'] == 'NA' || in_array($caseApprovalInfo['ApprovalType'], $caseAry['ApprovalStage'] )){
					 							if( ($caseAry['CurrentApprovalSequence'] == 'NA' || $caseApprovalInfo['ApprovalSequence'] <= $caseAry['CurrentApprovalSequence'] )&& !$caseApprovalInfo['IsDeletedApprover']){
					 								$x .= '<a href="javascript:editCaseApproval('.$caseApprovalInfo['ApproverUserID'].','.$caseApprovalInfo['ApprovalRoleID'].');" class="icon_edit_dim"></a>';
					 							}
					 						}
					 					}
					 				}
					 				//}
					 				$x .= '</th>';
					 				$x .= '</tr>';
					 				$x .= '</thead>';

					 				$x .= '<tbody valign="top">';
					 				if($caseApprovalInfo['Feedback']!=''){
					 					$x .= '<tr class="top_border">';
					 					$x .= '<td>&nbsp;</td>';
					 					$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Remarks'].'</td>';
					 					$x .= '<td>&nbsp;</td>';
					 					$x .= '<td colspan="2">'.Get_String_Display($caseApprovalInfo['Feedback']).'</td>';
					 					$x .= '</tr>';
					 				}
					 				$x .= '</tbody>';
					 				$x .= '</table>';
					 				$counter++;
                                    if ($caseApprovalInfo['RecordStatus'] == -1) {
                                        $rejectedReasonAry[] = Get_String_Display($caseApprovalInfo['Feedback']);
                                    }
					 			}
					 			if($isCaseRejected && $libPCM_auth->caseOwner($caseID)){
					 				//$x .= '<div>';
					 				//$x .= $this->GET_WARNING_TABLE($err_msg='', $DeleteItemName='', $DeleteButtonName='', $contentMsg=$Lang['ePCM']['Mgmt']['Case']['Warning']['CaseIsRejected'],$Lang['ePCM']['General']['Notice']);
					 				//$x .= '</div>';
					 				$x .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_type1">';
					 				$x .= '<tr>';
					 				$x .= '<th class="reject">'.$Lang['ePCM']['General']['Notice'].':<br>'.$Lang['ePCM']['Mgmt']['Case']['Warning']['CaseIsRejected'].'</th>';
					 				$x .= '</tr>';
					 				$x .= '</table>';
					 			}
                                $x .= $Lang['ePCM']['Setting']['Role']['DeletedUserLegend'];
					 			$x .= '</div>';
					 		}
					 	}

					 	// new endorsement workflow (Start)
					 	if( $quotationType!='N' && $sys_custom['ePCM']['extraApprovalFlow']){
					 		//step_box 6
					 		$thisBoxStage = 6;
					 		if(intval($currentStage)==$thisBoxStage){
					 			$timeStatus = 'current';
					 		}else if(intval($currentStage)>$thisBoxStage){
					 			$timeStatus = 'past';
					 		}else{
					 			$timeStatus = '';
					 		}
					 		//if(!($timeStatus==''&& $isDeleted && !$isCaseRejected)){	//Hide stage which not yet do for deleted case
                            if(($isCaseRejected && $currentStage == 6) || !$isCaseRejected){
					 			if($isCustCaseRejected) $additionClass = 'rejected';
						 		$x .= '<div class="step_box '.$timeStatus.' '.$additionClass.'" id="step_box_'.$thisBoxStage.'">';
						 			$x .= '<div class="box_title"><span>'.++$steps_count.'</span>'.$Lang['ePCM']['Mgmt']['Case']['Endorsement'].'</div>';
                                    if($sys_custom['ePCM']['ExceedBudgetRemarks'] && $exceedBudget != 'OK'){
                                        if($exceedBudget == 'ITEM'){
                                            $exceedRemarks = $Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Item'];
                                        }elseif($exceedBudget == 'GROUP'){
                                            $exceedRemarks = $Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Group'];
                                        }elseif($exceedBudget == 'GROUP:ITEM'){
                                            $exceedRemarks = $Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['GroupItem'];
                                        }else{
                                            $exceedRemarks = $Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['GroupItem'];
                                        }
                                        $x .= '<div class="remarks red" style="color:red;"><strong>'.$Lang['ePCM']['General']['Notice'].': </strong>'.$exceedRemarks.'</div>';
                                    }
						 			$counter = 1;
						 			foreach((array)$custCaseApprovalAry as $caseApprovalInfo){
						 				// display approval role name
					 					$_approvalRoleId = $caseApprovalInfo['ApprovalRoleID'];
					 					$roleDisplay = ($_approvalRoleId > 0) ? Get_Lang_Selection($roleInfoAry[$_approvalRoleId]['RoleNameChi'], $roleInfoAry[$_approvalRoleId]['RoleName']) : $Lang['ePCM']['Group']['GroupHead'];

					            		$x .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_type1 with_icon">';
							                $x .= '<colgroup>';
													$x .= '<col width="30" />';
													$x .= '<col width="135" />';
													$x .= '<col width="30" />';
													$x .= '<col width="200" />';
													$x .= '<col width="" />';
												$x .= '</colgroup>';
							                $x .= '<thead valign="top">';
							                        $x .= '<tr>';
							                            $x .= '<th><img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/iPortfolio/scheme/icon_staff.gif" width="30" height="28"></th>';
							                            $x .= '<th style="padding-right:0px;">'.$Lang['ePCM']['Mgmt']['Case']['Approver'].' #'.$counter.'<br>('.$roleDisplay.')</th>';
							                            $x .= '<th>:</th>';
							                            $x .= '<th>';
                                                            if($caseApprovalInfo['IsDeletedApprover']){$requiredSymbol = $linterface->RequiredSymbol();}
                                                            else{$requiredSymbol = '';}
							                            	$x .= $requiredSymbol.$caseApprovalInfo['ApproverName'];
															$x .= '<br />';
							                                $x .= $case_approval_status[$caseApprovalInfo['RecordStatus']];
							                                $string = '';
							                                if($caseApprovalInfo['ModifiedBy'] != $caseApprovalInfo['ApproverUserID']){
							                                	$string = '<br><span class="tabletextremark">'.str_replace('<!--adminName-->', $caseApprovalInfo['ModifiedByName'], $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['AdminAction']).'</span>';
							                                }
							                                $x .= ($caseApprovalInfo['RecordStatus']==0)?'':' ('.$this->convertDatetimeToDate($caseApprovalInfo['DateModified']).')'.$string;
														$x .= '</th>';
														$x .= '<th>';
															//Can edit only when you are the approver
															if(!$isDeleted && $currentStage >= 6){
																if($libPCM_auth->checkCustCaseApprovalRight($caseID,$caseApprovalInfo['ApproverUserID'])){
																	if( ($caseAry['CurrentCustApprovalSequence'] == 'NA' || $caseApprovalInfo['ApprovalSequence'] <= $caseAry['CurrentCustApprovalSequence']) && !$caseApprovalInfo['IsDeletedApprover'] ){
																		$x .= '<a href="javascript:editCustCaseApproval('.$caseApprovalInfo['ApproverUserID'].','.$caseApprovalInfo['ApprovalRoleID'].');" class="icon_edit_dim"></a>';
																	}
																}
															}
															//}
														$x .= '</th>';
							                        $x .= '</tr>';
							                    $x .= '</thead>';

							                $x .= '<tbody valign="top">';
							                		if($caseApprovalInfo['Feedback']!=''){
								                        $x .= '<tr class="top_border">';
								                            $x .= '<td>&nbsp;</td>';
								                            $x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Remarks'].'</td>';
								                            $x .= '<td>&nbsp;</td>';
								                            $x .= '<td colspan="2">'.Get_String_Display($caseApprovalInfo['Feedback']).'</td>';
								                        $x .= '</tr>';
							                		}
							                    $x .= '</tbody>';
							            $x .= '</table>';
							            $counter++;
                                        if ($caseApprovalInfo['RecordStatus'] == -1) {
                                            $rejectedReasonAry[] = Get_String_Display($caseApprovalInfo['Feedback']);
                                        }
						 			}
						 			if($isCustCaseRejected && $libPCM_auth->caseOwner($caseID)){
						 				$x .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_type1">';
						 					$x .= '<tr>';
						 						$x .= '<th class="reject">'.$Lang['ePCM']['General']['Notice'].':<br>'.$Lang['ePCM']['Mgmt']['Case']['Warning']['CaseIsRejected'].'</th>';
						 					$x .= '</tr>';
						 				$x .= '</table>';
						 			}
                                $x .= $Lang['ePCM']['Setting']['Role']['DeletedUserLegend'];
						 		$x .= '</div>';
					 		}
			 			}

					 	// new endorsement workflow (End)

					 	if($currentStage == $ePCMcfg['StageID']['Competed']) {
                            if ($quotationType != 'T') {
                                $exportEDBAnnexIIBtn = '<div class="Conntent_tool" style="float: right;">' . '<a class="print" href="javascript:exportPurchase_by_OralQuotationForm(\'en\')">' . $Lang['ePCM']['Mgmt']['Case']['Purchase-by-OralQuotationForm']['ExportBtnEn'] . '</a>' . '</div>';
                                $exportEDBAnnexIIBtn .= '<div class="Conntent_tool" style="float: right;">' . '<a class="print" href="javascript:exportPurchase_by_OralQuotationForm(\'b5\')">' . $Lang['ePCM']['Mgmt']['Case']['Purchase-by-OralQuotationForm']['ExportBtnB5'] . '</a>' . '</div>';

                            }
					 		$x .= '<div class="step_box '.$timeStatus.' '.$additionClass.'" id="step_box_'.$thisBoxStage.'">';
					 		$x .= '<div class="box_title"><span>'.$Lang['ePCM']['Mgmt']['Case']['Finished'].'</span>'.$exportEDBAnnexIIBtn.'</div>';
					 		$x .= '<div class="result">&nbsp;'.str_replace('<!--datetime-->',$caseAry['DateModified'], $Lang['ePCM']['Mgmt']['Case']['FinishOn']).'&nbsp;</div>';
					 		$x .= '</div>';

					 	}
			 		} //End of isDraft hiding

			 		//Check Rule is Valid
			 		$settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
			 		$isRuleValid = $settingObj->isRuleValid();

			 		if(!$isRuleValid&&$currentStage==0&&!$isDeleted){
			 			$x .= $this->getStopNewApplicationWarningBox();
			 		}

// 			 		if(!$isDeleted && $currentStage!=5){
			 		if(!$isDeleted){
			 		    $x .= '<div id="editBottomDiv" class="edit_bottom_v30">';
                        if($currentStage!=10){
                            if($currentStage ==0){
                                $deleteText = $Lang['ePCM']['Mgmt']['Case']['DeleteDraft'];
                                if($isRuleValid){
                                    $x .= $this->GET_ACTION_BTN($Lang['ePCM']['Mgmt']['Case']['SubmitDraft'], 'button', $ParOnClick="goSubmitDraft();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
                                }else{

                                }
                            }else{
                                $deleteText = $Lang['ePCM']['Mgmt']['Case']['Termination'];
                            }
                        }
                        // mfs cust print report
                        if($sys_custom['ePCM']['SummaryDocCustTemplate'] == 'mfs'){
                            $x.=' ';
                            $x .= $this->GET_ACTION_BTN($Lang['Btn']['Print'], 'button', $ParOnClick="void(0);", $ParName="printMfsForm", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
                        }
                        if($currentStage!=10) {
                            if ($libPCM_auth->caseOwner($caseID)) {
                                $x .= ' ';
                                $x .= $this->GET_ACTION_BTN($deleteText, 'button',
                                    $ParOnClick = "terminationThickBox();", $ParName = "", $ParOtherAttribute = "",
                                    $ParDisabled = 0, $ParClass = "", $ParExtraClass = "");
                            }
                        }
                        $x .= '</div>';
			 		}

			 		## if this case is lastest version
			 		$countOfVersion = count($versions);
			 		if($versions[$countOfVersion-1]['CaseID']==$caseID){
			 			$isLastestVersion = true;
			 		}
			 		if($isLastestVersion&&$isDeleted){
			 			$x .= '<div id="editBottomDiv" class="edit_bottom_v30">';
			 			if($libPCM_auth->caseOwner($caseID)){
			 				$x .= $this->GET_ACTION_BTN($Lang['ePCM']['Mgmt']['Case']['UndoTermination'], 'button', $ParOnClick="undoTermination();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
			 			}
			 			$x .= '</div>';
			 		}

			 	$x .= '</div>';
			 $x .= '</div>';

             $hiddenF .= $this->GET_HIDDEN_INPUT('caseID', 'caseID', $caseID);
             $hiddenF .= $this->GET_HIDDEN_INPUT('currentStage', 'currentStage', $currentStage);
             $hiddenF .= $this->GET_HIDDEN_INPUT('description', 'description', $description);
             $hiddenF .= $this->GET_HIDDEN_INPUT('caseName', 'caseName', $caseName);
             $hiddenF .= $this->GET_HIDDEN_INPUT('categoryID', 'categoryID', $categoryID);
             $hiddenF .= $this->GET_HIDDEN_INPUT('groupID', 'groupID', $applicantGroupID);
             $hiddenF .= $this->GET_HIDDEN_INPUT('itemID', 'itemID', $itemID);
             $hiddenF .= $this->GET_HIDDEN_INPUT('budget', 'budget', $this->displayMoneyFormat($budget));
             $hiddenF .= $this->GET_HIDDEN_INPUT('applicantType', 'applicantType', $applicantType);
             $hiddenF .= $this->GET_HIDDEN_INPUT('chooseIndividual', 'chooseIndividual', $applicantUserID);
             $hiddenF .= $this->GET_HIDDEN_INPUT('applicantNameEn', 'applicantNameEn', $caseAry['ApplicantUserNameEn']);
             $hiddenF .= $this->GET_HIDDEN_INPUT('applicantNameCh', 'applicantNameCh', $caseAry['ApplicantUserNameCh']);
             $hiddenF .= $this->GET_HIDDEN_INPUT('disableBudget', 'disableBudget', false);
             $hiddenF .= $this->GET_HIDDEN_INPUT('annex', 'annex','');
             $hiddenF .= $this->GET_HIDDEN_INPUT('ruleID', 'ruleID',$ruleID);
             foreach((array)$quotationsAry as $quotationID => $quotationDetailAry){
                 foreach ($quotationDetailAry as $detailName => $info){
                     $hiddenF .= $this->GET_HIDDEN_INPUT('QuotationsAry', "QuotationsAry[$quotationID][$detailName]", $info);
                 }
             }
             $hiddenF .= $this->GET_HIDDEN_INPUT('supplierLessThenReason', 'supplierLessThenReason',$remarks[$stageID[0]]['Remark']);
             foreach((array)$resultForExportAry as $detailName => $detail){
                $hiddenF .= $this->GET_HIDDEN_INPUT('resultForExportAry', "resultForExportAry[$detailName]", $detail);
             }
             $hiddenF .= $this->GET_HIDDEN_INPUT('EDB_export_lang', 'EDB_export_lang',$EDB_export_lang);
             $hiddenF .= $this->GET_HIDDEN_INPUT('rejectedReason', 'rejectedReason',$rejectedReasonAry[0]);

             $htmlAry['hiddenField'] = $hiddenF;
			 $x .= $htmlAry['hiddenField'];
		 $x .= '</form>';
		 return $x;
	}
	
	public function getNavigation_PCM($PAGE_NAVIGATION){
		return $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
	}
	
	function displayMoneyFormat($float,$deimal_place=2){
		global $Lang;
		// rounding up to 2 d.p.
		if(trim($float)===''){
			return $float;
		}
		//$deimal_place = 2;
		return $Lang['PCM']['Symbol']['DollarSign'].number_format($float, $deimal_place);
	}
	
	public function displayFileSize($sizeInByte, $recursiveNum=0){
	    $arr = array('', 'K', 'M', 'G', 'T');
	    $callNum = $recursiveNum;
	    if($sizeInByte < 1024){
	        return round($sizeInByte, 1).$arr[$callNum].'B';
	    }
	    else{
	        $callNum ++;
	        // for safety
	        if($callNum > 4){
	            return false;
	        }else{
	            return $this->displayFileSize(($sizeInByte/1024),$callNum);
	        }
	    }
	}
	
	public function displayFileDownloadLinkSize($AttachmentID, $LangDisplay){
	    $html = '';
	    $html .= '<a class="icon_clip tablelink" href="index.php?p=file.download.'.$AttachmentID.'">';
	    $html .= $LangDisplay;
	    $html .= '</a>';
	    return $html;
	}
	
	public function getFileListUI($fileInfoArr, $displaySize=true, $returnArr=false){
	    $x = '';
	    if(!empty($fileInfoArr)){
	    	$count = 0;
	    	$num = count($fileInfoArr);
	    	$resultArr = array();
	        foreach((array)$fileInfoArr as $_fileInfo){
	            $count ++;
	        	
	            $linkLang = $_fileInfo['FileName'];
	            if($displaySize){
	            	$linkLang .= ' ('.$this->displayFileSize($_fileInfo['FileSize']).')';
	            }
	            $fileLink = $this->displayFileDownloadLinkSize($_fileInfo['AttachmentID'], $linkLang);
	            $x .= $fileLink;
	            if($count != $num){
	            	$x .= '<br />';
	            }
	            if($returnArr)
	            	$resultArr[] = $fileLink;
	        }
	    }
	    else{
	        $x .= Get_String_Display('');
	    }
	    if($returnArr){
	    	return $resultArr;
	    }
	    else{
	  		return $x;
	    }
	}
	public function convertDatetimeToDate($datetime){
		if($datetime==''){
			return '';
		}else{
			return date("Y-m-d",strtotime($datetime));
		}
		
	}
	
	public function getTemplateFileUI($template_type, $ruleID='', $getAttachWithRuleIDAndRuleID = false, $CaseID=-1){
		global $Lang, $_PAGE;


		$db = $_PAGE['Controller']->requestObject('libPCM_db_file','includes/ePCM/libPCM_db_file.php');
		$fileArr = $db->getCaseStageAttachment($CaseID, $Stage=$template_type, $AssocWithStage=false, $type='0', $ruleID, $getAttachWithRuleIDAndRuleID);
		if(!empty($fileArr)){
			$fileUI = $this->getFileListUI($fileArr, false, true);
			$html = $this->Get_Warning_Message_Box($Lang['ePCM']['Template'].' - '.$Lang['ePCM']['Setting']['Template'][$template_type], $fileUI);
		}
		
		return $html;
	}

    public function getTempRuleTemplateFileUI($ruleID, $session){
        global $Lang, $_PAGE, $ePCMcfg;

        $db = $_PAGE['Controller']->requestObject('libPCM_db_file','includes/ePCM/libPCM_db_file.php');
        $fileArr = $db->getTempFileList($ruleID, $session, $AssocWithStage=true);
        $html='<table id="ruleTemplateTable" class="common_table_list_v30">
                    <thead>
                        <tr class="tabletop">
                            <th width="20%">'.$Lang['ePCM']['Template'].'</th>
                            <th>'.$Lang['General']['File'].'</th>
                        <tr>
                    </thead>
                <tbody>';

            foreach((array)$ePCMcfg['Template'] as $template_key) {
                $html.= '<tr id="template_type_'.$template_key.'">';
                $html.= '<td><a href="javascript:goUpload(' . $template_key . ')" class="icon_edit_dim">' . $Lang['ePCM']['Setting']['Template'][$template_key] . '</a><div class="table_row_tool row_content_tool"><a href="javascript:goUpload(' . $template_key . ')" class="icon_edit_dim"></a></div></td>';
                $html.= '<td>' . $this->getFileListUI($fileArr[$template_key], $displaySize = false, $returnArr = false) . '</td>';
                $html.= '</tr>';
            }
            $html.= '</tbody>';
            $html.= '</table>';
        $html.= $Lang['ePCM']['Setting']['Rule']['Remarks']['rollbackTemplateFile'];
        return $html;
    }
	
	public function GET_TB_TEXTAREA($taName, $taContents, $taCols=70, $taRows=5, $OnFocus = "", $readonly = "", $other='', $class='', $taID='', $CommentMaxLength=''){
// 		$initialRows = 2;
// 		$initialRows = (trim($taContents)=="") ? $initialRows : $taRows;
		$initialRows = $taRows;
	
		$taID = ($taID=='')? $taName : $taID;
	
		if ($readonly == 1)
			$readonly = " readonly ";
			else {
				$readonly = " ";
			}
	
			if (!empty($CommentMaxLength)) {
				$limitText = 'onkeyup="limitText(this, '.$CommentMaxLength.');"';
			}
				
			$rx = "<textarea class=\"tabletext $class\" name=\"{$taName}\" ID=\"{$taID}\" cols=\"{$taCols}\" rows=\"{$initialRows}\" wrap=\"virtual\" $other $limitText onFocus=\"this.rows={$taRows}; $OnFocus\" $readonly >{$taContents}</textarea>";
	
			return $rx;
	}
	function getPriceComparisonTable($priceItem,$quotationAry){
		$quotationAry = BuildMultiKeyAssoc($quotationAry,array('QuotationID'));
		
		$headingAry = Get_Array_By_Key(array_values($priceItem[key($priceItem)]),'PriceItemName');
		$countOfPriceItem = count($headingAry);
		
		$x .= '<table width="100%" class="border">';
			// Price Item Heading
			$x .= '<tr>';
				$x .= '<th rowspan="2">';
				$x .= 'Supplier';
				$x .= '</th>';
				
				$x .= '<th colspan="'.$countOfPriceItem.'">';
				$x .= 'Price';
				$x .= '</th>';
				
				$x .= '<th rowspan="2">';
				$x .= 'Remark';
				$x .= '</th>';
				
			$x .= '</tr>';
		
			$x .= '<tr>';
			foreach($headingAry as $_heading){
				$x .= '<td>';
				$x .= $_heading;
				$x .= '</td>';
			}
			$x .= '</tr>';
			
			foreach($priceItem as $_quotationID => $_priceItemInfoAry){
				$x .= '<tr>';
				
				$x .= '<td class="text_center">';
					$x .= $quotationAry[$_quotationID]['SupplierName'];
				$x .= '</td>';
				
				foreach($_priceItemInfoAry as $__priceItemID => $__priceItemInfo){
					$x .= '<td class="text_center">';
						$x .= ($__priceItemInfo['Price']===NULL)?'':'$'.$__priceItemInfo['Price'];
					$x .= '</td>';
				}
				
				
				$x .= '<td>';
				$x .= $quotationAry[$_quotationID]['Remarks'];
				$x .= '</td>';
				
				$x .= '</tr>';
			}
			
		$x .= '</table>';
		return $x;
	}
	
	function getFundingByCategorySelectionBox($id='Funding_Selection_Box_0',$name='Funding_Selection_Box',$fundingID='',$BudgetUsed=''){
		global $_PAGE,$Lang;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
		$data = $db->getFundingSourceTableData();
		$x = '<table width="100%" class="inside_form_table_v30">';
		$x .= '<tr>';
			$x .= '<td width="50%" style="vertical-align: bottom">';
				$x .= '<select id="'.$id.'" name="'.$name.'[]">';
					$x .= '<option value="0">'.$Lang['ePCM']['Mgmt']['FundingName']['NotApplicable'].'</option>';
					if(!empty($data)){
						foreach ($data as $_category){
							$print = false;
							foreach ((array)$_category['Funding'] as $Funding){
								if(strtotime($Funding['DeadLineDate'])>strtotime(date("Y-m-d H:i:s")) && $Funding['RecordStatus']=='1') { //Only Show the Funding doesnt pass the deadline and Funding is not stop service
									$print = true;
								}
							}
							if($print){
								$x .= '<optgroup label="'.Get_Lang_Selection($_category['FundingCategoryNameCHI'], $_category['FundingCategoryNameEN']).'" id="'.$_category['CategoryID'].'">';
								foreach ((array)$_category['Funding'] as $Funding){
									if((strtotime($Funding['DeadLineDate'])>strtotime(date("Y-m-d H:i:s")) && $Funding['RecordStatus']=='1')||$Funding['FundingID']==$fundingID){ //Only Show the Funding doesnt pass the deadline and Funding is not stop service
										if($Funding['FundingID']==$fundingID){
											$check = "selected";
	// 										$canMatch = true;
										}else{
											$check = "";
										}
										$x .= '<option value="'.$Funding['FundingID'].'" '.$check.'>';
											$x .= Get_Lang_Selection($Funding['FundingNameCHI'], $Funding['FundingNameEN']);
											$x .= '(';
											$x .= $Lang['ePCM']['FundingSource']['RemainingBudget'];
											$x .= ': ';
											$x .= $Funding['BudgetRemain'];
											$x .= ')';
										$x .= '</option>';
									}
								}
								$x .= '</optgroup>';
							}
						}
					}
				$x .= '</select>';
			$x .= '</td>';
			$x .= '<td width="50%">';
				$x .= $Lang['ePCM']['FundingSource']['UsingBudget'].":	";
				$x .= '<input id="'.$id.'_usingBudget" name="'.$name.'_usingBudget[]" value="'.$BudgetUsed.'">';
				$x .= '<span id="AlertMsg_'.$id.'" class="warnMsgDiv">'.'</span>';
			$x .= '</td>';
// 			if(!$canMatch&&$fundingID){
// 				$x .= '<span  class="warnMsgDiv" style="color:red;"> This funding source is expired</span>';
// 			}
		$x .= '</tr>';
		$x .= '</table>';
		return $x;
	}
	function getFundingCaseTable($FundingID, $RecordStatus){
		global $_PAGE,$Lang;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		### Get Data Start
		$FundingData = $db->getFundingCaseArr($FundingID);
		$CaseData = $db->getCaseRawInfo();
		### Get Data End
		### Table Start
		$RecordStatus_Name = "";
		$x = $ui->GET_NAVIGATION2_IP25($Lang['ePCM']['FundingSource']['ApprovalStatus'][$RecordStatus]);
		$x .= '<br>';
		$x .= '<table  class="common_table_list" width="100%">';
			$x .= '<thead>';
				$x .= '<th width="30%">';
					$x .= $Lang['ePCM']['Mgmt']['Case']['CaseCode'];
				$x .= '</th>';
				$x .= '<th width="40%">';
					$x .= $Lang['ePCM']['Mgmt']['Case']['CaseName'];
				$x .= '</th>';
				$x .= '<th width="30%">';
					$x .= $Lang['ePCM']['FundingSource']['UsingBudget'] ;
				$x .= '</th>';
			$x .= '</thead>';
			$x .= '<tbody>';
				foreach ((array)$FundingData as $_FundingData){ //Load Funding
					$print = true;
					foreach ((array)$_FundingData['Case'] as $Case){ //LOOP every Case
						if($Case['RecordStatus']===$RecordStatus){
							$x .= '<tr>';
								$x .= '<td>';
									$x .= $CaseData[$Case['CaseID']]['Code'];
								$x .= '</td>';
								$x .= '<td>';
									$x .= $CaseData[$Case['CaseID']]['CaseName'];
								$x .= '</td>';
								$x .= '<td>';
									$x .= $Case['Amount'];
								$x .= '</td>';
							$x .= '</tr>';
							$print =false;
						}
					}
					if($print){
						$x .= '<tr>';
							$x .= '<td colspan="3">';
								$x .= $Lang['General']['NoRecordAtThisMoment'];
							$x .= '</td>';
						$x .= '</tr>';
					}
				}
			$x .= '</tbody>';
		$x .= '</table>';
		### Table End
		return $x;
	}
	function getFundingFilter($SelectedStatus=''){
		global $_PAGE,$Lang;
		$x = "<select id='FundingFilter' name='FundingFilter'>";
		$x .= "<option value=''>".$Lang['ePCM']['FundingSource']['RecordStatus']['ALL']."</option>";
		for($i=1;$i<3;$i++){
			$check = ("$i"==="$SelectedStatus")? "Selected":"";
			$x .= "<option value='$i' $check>".$Lang['ePCM']['FundingSource']['RecordStatus'][$i]."</option>";
		}
		$x .= "</select>";
		return $x;
	}
	function Get_PCM_Radio_Button($ID, $Name, $Value, $isChecked=0, $Class="", $Display="", $Onclick="",$isDisabled=0, $otherTags="")
	{
		$onclick_par = '';
		if ($Onclick != "")
			$onclick_par = 'onclick="'.$Onclick.'"';
				
		$class_par = '';
		if ($Class != "")
			$class_par = 'class="'.$Class.'"';
				
		$checked = '';
		if ($isChecked)
			$checked = 'checked="checked"';

		if($isDisabled)
			$disabled = 'disabled="disabled"';

		$chkbox = '';
		$chkbox .= '<input type="radio" value="'.$Value.'" id="'.$ID.'" name="'.$Name.'" '.$class_par.' '.$onclick_par.' '.$checked.' '.$disabled.' '.$otherTags.'/>';

		if ($Display != "")
			$chkbox .= '<label for="'.$ID.'"> '.$Display.'</label>';

		return $chkbox;
	}
}
?>