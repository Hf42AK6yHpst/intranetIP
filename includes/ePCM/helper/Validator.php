<?php
class Validator{
	
	public function __construct(){
		
	}
	
	public function getErrorMsg($type, $parms){
		global $Lang;
		
		$errorMsg = $Lang['ePCM']['Import']['Validator'];
		if(!empty($parms)){
			$replace = array_keys($parms);
			$replaceBy = array_values($parms);
			return str_replace($replace, $replaceBy, $errorMsg[$type]);
		}else{
			return $errorMsg[$type];
		}
	}
	
	public function required($val){
		$valid = strlen(trim($val)) > 0;

		if(!$valid){
			$error = $this->getErrorMsg(__FUNCTION__, $parms);
		}
		
		return array('valid' => $valid, 'error' => $error);
	}
	
	public function maxlength($val, $length){
		$valid = strlen(trim($val)) <= $length;
		
		if(!$valid){
			$parms = array('<!--length-->' => $length);
			$error = $this->getErrorMsg(__FUNCTION__, $parms);
		}
		
		return array('valid' => $valid, 'error' => $error);
	}
	
	public function numeric($val){
		$valid = is_numeric($val);
		if(!$valid){
			$error = $this->getErrorMsg(__FUNCTION__, $parms);
		}
		return array('valid' => $valid, 'error' => $error);
	}
	
	public function date($val){
		if(trim($val) != ''){
			$time = strtotime($val);
			$convertedDate = date("Y-m-d",$time);
			$convertedTime = strtotime($convertedDate);
			$valid = $time == $convertedTime;
		}else{
			$valid = false;
		}
		if(!$valid){
			$error = $this->getErrorMsg(__FUNCTION__, $parms);
		}else{
			$mapTo = $convertedDate;
		}
		return array('valid' => $valid, 'error' => $error, 'mapTo' => $mapTo);
	}
	public function aaa($val){
		$valid = false;
		if(!$valid){
			$error = 'hihi';
		}else{
			$mapTo = 'xxxx';
		}
		return array('valid' => $valid, 'error' => $error, 'mapTo' => $mapTo);
	}
}