<?php 
class ReferenceHelper{
	var $model;
	var $scheme;
	var $refType;
	
	public function __construct($model, $scheme, $refType){
		$this->model = $model;
		$this->scheme = $scheme;
		$this->refType = $refType;
	}
	public function getReferenceHeaderHTML(){
		$headerArr = $this->scheme->getRemarksThickBoxHeader($this->refType);
		$title = '<tr>';
		foreach($headerArr as $_col){
			$title .= '<th>'.$_col.'</th>';
		}
		$title .= '</tr>';
		return $title;
	}
	public function getReferenceContentHTMLArr(){
		$detailsArr = array();
		$contentArr = $this->model->getRefContent($this->refType);
		
		foreach($contentArr as $_row){
			$html = '';
			foreach($_row as $__col){
				$html .= '<td>'.$__col.'</td>';
			}
			$detailsArr[] = $html;
		}
		return $detailsArr;
	}
}