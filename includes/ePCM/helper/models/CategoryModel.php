<?php 
include_once "importHelperModel.php";
class CategoryModel implements importHelperModel{
	var $db; 
	
	public function __construct($db){
		$this->db = $db;
	}
	public function getExportData($keyword){
		$catArr = $this->db->getCategoryInfo(array(),$keyword);
		$dataArr = array();
		if(!empty($catArr)){
			foreach($catArr as $_cat){
				$_temp = array();
				$_temp[0] = $_cat['Code'];
				$_temp[1] = $_cat['CategoryName'];
				$_temp[2] = $_cat['CategoryNameChi'];
				$dataArr[] = $_temp; 
			}
		}
		
		return $dataArr;
	}
	public function insertToDb($data){
		$table_name = 'INTRANET_PCM_CATEGORY';
		$array_field_name = array('Code','CategoryName','CategoryNameChi');
		
		$success = $this->db->insertData($table_name, $array_field_name, $data);
		return $this->db->db_affected_rows();
	}
	public function getRefContent($refType){
		
	}
	public function validate_unique($value){
		
		global $Lang;
		
		$table = 'INTRANET_PCM_CATEGORY';
		$field = 'Code';
		$excluded_value = '';
		$valid = $this->db->checkDuplicate($table,$field,$value,$excluded_value);
		
		if(!$valid){
			$error = $Lang['ePCM']['Category']['Duplicated code'];
		}
		return array('valid' => $valid, 'error' => $error);
	}
}