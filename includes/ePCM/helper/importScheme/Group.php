<?php 
include_once "importScheme.php";
class Group implements importScheme{
	
	public function __construct(){
		
	}
	### For Import Template (Start) ###
	public function getHeaderTemplate(){
		$header = array();
		$header['En'] = array('Code','Group name (English)','Group name (Chinese)', 'Group head', 'Member');
		$header['Ch'] = array('代碼','部門/科組名稱 (英文)','部門/科組名稱 (中文)', '上級', '成員');
		return $header;
	}
	
	public function getHeaderProperty(){
		// 1 = complusory , 2 =ref , 3 = optional
		// pls set all 1, otherwise will not able to extract the value
		return array(1,1,1,1,1);
	}
	
	public function getContentTemplate(){
		$contentArr = array();
		$contentArr[] = array('PHY', 'PHY Team', '物理小組', 'broadlearning, broadlearning2', 'Chan Tai Man');
		return $contentArr;
	}
	### For Import Template (End) ###
	
	### For All Step (Start) ###
	public function setCurrentSectionAndPage(){
		global $_PAGE;
		
		$_PAGE['CurrentSection'] = 'Settings';
		$_PAGE['CurrentPage'] = 'Group';
	}
	public function getNavigationAry(){
		global $Lang;
		
		$originPath = $this->getOriginPath();
		
		$navigationAry = array();
		$navigationAry[] = array($Lang['ePCM']['Group']['Group'], 'javascript: window.location=\'index.php?p='.$originPath.'\';');
		$navigationAry[] = array($Lang['Btn']['Import']);
		return $navigationAry;
	}
	public function getOriginPath(){
		return 'setting.group';
	}
	public function getTempFilePath(){
		global $intranet_root, $ePCMcfg;
		return $intranet_root.'/'.$ePCMcfg['INTERNATDATA'].'temp/import/group/';
	}
	### For All Step (End) ###
	
	### For Import Step 1 (Start) ###
	public function getColumnTitleArr(){
		global $Lang;
		
		$ColumnTitleArr = array();
		$mapping = $this->getContentMapping();
		$desc = $this->getContentDescripiton();
		foreach($mapping as $_key){
			$ColumnTitleArr[] = $desc[$_key];
		}
		return $ColumnTitleArr; 
	}
	public function getColumnPropertyArr(){
		return array(1,1,1,0,0);
	}
	public function getRemarksArr(){
		global $Lang;
		return array(
				'',
				'',
				'',
				'<span class="tabletextremark">'. $Lang['ePCM']['Group']['UserLogin'].'</span>',
				'<span class="tabletextremark">'. $Lang['ePCM']['Group']['MemberInput'].'</span>'
		);
	}
	public function getRemarksThickBoxHeader($refType){
		global $Lang;
		$headerArr = array();
		switch($refType){
			case 'groupcat';			
				$headerArr[] = $Lang['ePCM']['Import']['Ref']['Code'];
				break;
		}
		return $headerArr;
	}
	### For Import Step 1 (End) ###
	
	### For Import Step 2 (Start) ###
	public function getContentDescripiton(){
		global $Lang;
		
		$desc = array();
		$desc['code'] = $Lang['ePCM']['Group']['Code'];
		$desc['engGroupName'] = $Lang['ePCM']['Group']['GroupName'];
		$desc['chiGroupName'] = $Lang['ePCM']['Group']['GroupNameChi'];
		$desc['groupHead'] = $Lang['ePCM']['Group']['GroupHead'];
		$desc['member'] = $Lang['ePCM']['Group']['Member'];
		return $desc;
	}
	
	public function getContentMapping(){
		$desc = array('code', 'engGroupName', 'chiGroupName', 'groupHead', 'member');
		return $desc;
	}
	
	# for error display table
	public function getContentWidth(){
		$widthArr = array(10,15,15,15,15);
		return $widthArr;
	}
	
	public function getValidationRule(){
		$desc = array('code', 'engGroupName', 'chiGroupName', 'groupHead', 'member');
		$rule = array();
		$rule['code'] = 'required|unique:Group';
		$rule['engGroupName'] = 'required';
		$rule['chiGroupName'] = 'required';
		$rule['groupHead'] = 'existedUser:Group';
		$rule['member'] = 'existedUser:Group';
		
		return $rule; 
	}
	### For Import Step 2 (End) ###
	
	# export function
	public function getExportHeader(){
		return $this->getHeaderTemplate();
	}
}