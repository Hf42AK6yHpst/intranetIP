<?php 
interface importScheme{
	## for tmpl (start)
	public function getHeaderTemplate();
	public function getHeaderProperty();
	public function getContentTemplate();
	## for tmpl (end)
	## general (start)
	public function setCurrentSectionAndPage();
	public function getNavigationAry();
	public function getOriginPath();
	public function getTempFilePath();
	## general (end)
	## for step 1 (start)
	public function getColumnTitleArr();
	public function getColumnPropertyArr();
	public function getRemarksArr();
	public function getRemarksThickBoxHeader($refType);
	## for step 1 (end)
	## for step 2 (start)
	public function getContentDescripiton();
	public function getContentMapping();
	# for error display
	public function getContentWidth();
	public function getValidationRule();
	## for step 2 (end)
}