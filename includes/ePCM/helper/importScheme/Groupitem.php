<?php 
include_once "importScheme.php";
class Groupitem implements importScheme{
	
	public function __construct(){
		
	}
	### For Import Template (Start) ###
	public function getHeaderTemplate(){
		$header = array();
		$header['En'] = array('Group code','Code','Financial item (English)', 'Financial item (Chinese)');
		$header['Ch'] = array('部門/科組','財政項目代碼','財政項目 (英文)', '財政項目 (中文)');
		return $header;
	}
	
	public function getHeaderProperty(){
		// 1 = complusory , 2 =ref , 3 = optional
		// pls set all 1, otherwise will not able to extract the value
		return array(1,1,1,1);
	}
	
	public function getContentTemplate(){
		$contentArr = array();
		$contentArr[] = array('BIO', 'BIOT', 'training', 'training');
		$contentArr[] = array('PO', 'POT', 'facility', 'facility');
		
		return $contentArr;
	}
	### For Import Template (End) ###
	
	### For All Step (Start) ###
	public function setCurrentSectionAndPage(){
		global $_PAGE;
		
		$_PAGE['CurrentSection'] = 'Settings';
		$_PAGE['CurrentPage'] = 'Group';
	}
	public function getNavigationAry(){
		global $Lang;
		
		$originPath = $this->getOriginPath();
		
		$navigationAry = array();
		$navigationAry[] = array($Lang['ePCM']['Group']['GroupItem'], 'javascript: window.location=\'index.php?p='.$originPath.'\';');
		$navigationAry[] = array($Lang['Btn']['Import']);
		return $navigationAry;
	}
	public function getOriginPath(){
		return 'setting.group';
	}
	public function getTempFilePath(){
		global $intranet_root, $ePCMcfg;
		return $intranet_root.'/'.$ePCMcfg['INTERNATDATA'].'temp/import/groupitem/';
	}
	### For All Step (End) ###
	
	### For Import Step 1 (Start) ###
	public function getColumnTitleArr(){
		global $Lang;
		
		$ColumnTitleArr = array();
		$mapping = $this->getContentMapping();
		$desc = $this->getContentDescripiton();
		foreach($mapping as $_key){
			$ColumnTitleArr[] = $desc[$_key];
		}
		return $ColumnTitleArr; 
	}
	public function getColumnPropertyArr(){
		return array(1,1,1,1);
	}
	public function getRemarksArr(){
		global $Lang;
		return array(
				'<a id="groupCode" class="tablelink" href="javascript:void(0);" onclick="Load_Reference(\'groupCode\')">'.$Lang["ePCM"]["Group"]["GroupCodeEnquiry"].'</a>',
				'',
				'',
				''
		);
	}
	public function getRemarksThickBoxHeader($refType){
		global $Lang;
		$headerArr = array();
		switch($refType){
			case 'groupCode';			
				$headerArr[] = $Lang['ePCM']['Group']['GroupCode'];
				$headerArr[] = Get_Lang_Selection($Lang['ePCM']['Group']['GroupNameChi'], $Lang['ePCM']['Group']['GroupName']);
				break;
		}
		return $headerArr;
	}
	### For Import Step 1 (End) ###
	
	### For Import Step 2 (Start) ###
	public function getContentDescripiton(){
		global $Lang;
		
		$desc = array();
		$desc['groupCode'] = $Lang['ePCM']['Group']['GroupCode'];
		$desc['code'] = $Lang['ePCM']['Group']['Item']['FinancialCode'];
		$desc['engFinancialItem'] = $Lang['ePCM']['Group']['Item']['FinancialItemEng'];
		$desc['chiFinancialItem'] = $Lang['ePCM']['Group']['Item']['FinancialItemChi'];
		return $desc;
	}
	
	public function getContentMapping(){
		$desc = array('groupCode', 'code', 'engFinancialItem', 'chiFinancialItem');
		return $desc;
	}
	
	# for error display table
	public function getContentWidth(){
		$widthArr = array(15,15,15,15);
		return $widthArr;
	}
	
	public function getValidationRule(){
		$desc = array('groupCode', 'code', 'engFinancialItem', 'chiFinancialItem');
		$rule = array();
		$rule['groupCode'] = 'required|existed:Group';
		$rule['code'] = 'required|unique:Groupitem';
		$rule['engFinancialItem'] = 'required';
		$rule['chiFinancialItem'] = 'required';
		
		return $rule; 
	}
	### For Import Step 2 (End) ###
	# export function
	public function getExportHeader(){
		return $this->getHeaderTemplate();
	}
}