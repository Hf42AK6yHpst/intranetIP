<?php 
// using by : 

/**
 * Date:	2019-07-17 Henry
 * - modify innerJoinSqlForNonAdmin to fix the duplcate problem
 * Date:	2019-06-03 Henry
 * - modify getCompletedCaseSqlForDbTable to support confirm completed case by principal
 * Date:	2017-08-14 Simon
 * - modify the sql order by conditions on getCaseInfoForReportPCMRS function
 * Date:	2017-04-05 Villa
 * - modified getFundingCategoryRelationRawData() - sort by DeadlineDate
 * Date:	2017-03-15 Villa
 * - modified getFundingRawData() - add Record Status Filtering
 * 
 * Date:	2017-03-07 Villa
 * - add getCaseRawInfo() - return CaseInfoArr with the index of CaseID
 * 
 * Date:	2017-03-03 Villa
 * - add getFundingRawData() - return Funding Data with the index of FundingID
 * - add getFundingCaseRelationRawData() - return Funding - Case with the index of FundingID
 * - add getFundingCategoryRawData - return Category Data with the index of CategoryID
 * - add getFundingCategoryRelationRawData - return Funding - Category with the index of CategoryID
 * - add getFundingCategoryLastestDisplayOrder -  return last displayorder of FundingSource Category
 * - add getFundingLastestDisplayOrder - return last displayorder of FundingSource
 * - add getFundingLastestFundingID - return Latest ID of FundingSource
 * 
 * Date:	2017-03-01	Villa
 * 		add getSMCUserInfo - get SMC UserInformation
 * Date:	2016-11-09	Villa
 * 		getGroupHeadByUserID()
 * Date:	2016-11-03	Villa
 * - modified getCompletedCaseSqlForDbTableForNonAdminForOtherApplication() 
 * - 	Situation 1(approved): No one can edit 
 * -	Situation 2(pending): groupleader can edit all the case from the group he is group leader, edit the case created by him from the group he is member
 * -  						  edit the case created by him from the group he is member
 * - 	add 2tags in selection box for js checking
 * * - modified getCompletedCaseSqlForDbTableForNonAdminForOtherApplication() 
 * 		add 2tags in selection box for js checking
 * 
 * Date:	2016-11-01	Omas
 * - modified getCompletedCaseSqlForDbTableForNonAdminForOtherApplication() - check only case owner can edit, add column applicant
 * - modified getCompletedCaseSqlForDbTable2() - add column applicant
 * Date:	2016-10-24	Omas
 * - modified getCaseInfoForReportBER() - improved SQL reduce subquery 
 * Date: 2016-10-06 Villa
 * - Add getRuleOInfo() - get Rule Info where Rule type is Other
 * Date: 2016-10-04 Villa
 * - modified getCaseInfoForReportBER() - add two parms to get do diff sql checking
 * Date: 2016-10-03 Villa
 * - getLatestRuleIDForOtherApplication() - get the RuleID for adding other application
 * - modified getCaseInfoForReportFCS() - filter out the item from other application
 * - modified getCaseInfoForReportPCMRS() - filter out the item from other application
 * - modified getCompletedCaseSqlForDbTable() - filter out the item from other application
 * - modified getCompletedCaseSqlForDbTable2() - only show the item in other application
 * - modified getCaseInfo() - earse the other application display
 * - modified getCompletedCaseSqlForDbTableForNonAdmin() - filter out the item from other application
 * 
 * Date: 2016-09-28 Omas
 *  - add convert2unicode_recursive() - for EJ only , returnFundingSourceWithFundingType_eInv() - EJ will convert to unicode
 *  
 * Date: 2016-09-27	Villa
 * -add getGroupCode() -> get groupCode
 * 
 * Date: 2016-09-01 Omas
 * - modified getCaseInfo() - add ApprovalMode
 * - modified getGroupBudget() - add param academicYear
 * 
 * Date: 2016-08-26 Omas
 * - modified saveRule(), getCaseRemarks(), getCaseApproval()
 * 
 * Date: 2016-08-11 Kenneth
 * - set currentStage of completed case = 10 for all methods
 * - modified getResult(), select DateModified
 * 
 * Date: 2016-07-05	Kenneth
 * - modified getSupplierSqlForDbTable(), return all if typeID = ''
 * - modified getSupplierSqlForDbTable(), getSupplierTypeSqlForDbTable(), to includes links
 * - modified getCaseInfo(), add search by quotation type
 * 
 * Date: 2016-06-30	Kenneth
 * - added getTokenInfo(),updateTokenPassword() 
 * Date: 2016-06-29	Kenneth
 * - Modified getAndUpdateQuotationByToken(), added check is token expired logic
 *
 * Date: 2016-07-22 Henry HM
 * - Added field include_zero_expenses for BER Report
 */

class libPCM_db extends libdb {

	public function __construct(){
		parent::__construct();
	}

	public function getUserInfo($UserID){
		global $ePCMcfg;
		$sql = "SELECT * From ".$ePCMcfg['INTRANET_USER']." WHERE UserID = '".IntegerSafe($UserID)."' ";
		return $this->returnResultSet($sql);
	}
	
	//General Setting
	public function getGeneralSettings(){
		global $_PAGE;
		$gsObj = $_PAGE['Controller']->requestObject('libgeneralsettings','includes/libgeneralsettings.php');
		return $gsObj->Get_General_Setting($ModuleName='ePCM',$SettingList=array());
	}
	public function updateGeneralSettings($updateAry){
		global $_PAGE;
		$gsObj = $_PAGE['Controller']->requestObject('libgeneralsettings','includes/libgeneralsettings.php');
		$gsObj->Save_General_Setting($ModuleName='ePCM',$SettingList=$updateAry);
	}
	
	//External Module
	### eInventory
	public function getEInvAdminGroup($AdminGroupIDAry=''){
		
		if(!empty($AdminGroupIDAry)){
			$cond = " AND AdminGroupID IN ('".implode("','",(array)$AdminGroupIDAry)."') ";
		}
		
		$nameField = Get_Lang_Selection('NameChi','NameEng');
		
		$sql = "SELECT 
					AdminGroupID,
					$nameField AS Name,
					Code,
					NameChi,
					NameEng
				FROM INVENTORY_ADMIN_GROUP AS iag
				WHERE 1 $cond
				GROUP BY iag.AdminGroupID";
// 		debug_pr($sql);
		return $this->returnArray($sql);	//must use returnArray for multiple select
	}
	public function getEInvAdminGroupMember($AdminGroupIDAry){
		global $ePCMcfg;
		$sql = "SELECT iagm.UserID,iu.ChineseName,iu.EnglishName, AdminGroupID,iagm.RecordType
				FROM INVENTORY_ADMIN_GROUP_MEMBER AS iagm
				INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS iu ON (iu.UserID = iagm.UserID)
				WHERE AdminGroupID IN ('".implode("','",(array)$AdminGroupIDAry)."') 
				ORDER BY AdminGroupID";
		$groupMemberAry = $this->returnArray($sql);
		return BuildMultiKeyAssoc($groupMemberAry,'AdminGroupID',array(),0,1);
	}
	
	//Subject Group
	public function getGroupSQL($IsDeleted = 0){
		$sql = "SELECT * FROM INTRANET_PCM_GROUP WHERE IsDeleted = $IsDeleted";
		return $sql;
	}
	public function getGroupInfo($groupIDAry, $keyword=''){
        if(isset($keyword) && $keyword!==''){
            $conds = " AND 
						(
							Code Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR GroupName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR GroupNameChi Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
						)			";
        }

		if(!empty($groupIDAry)){
			$groupID_conds = " AND GroupID IN ('".implode("','", (array)$groupIDAry)."') ";
		}

        $sort = "ORDER BY Code Asc";
		
		$sql = "SELECT * FROM INTRANET_PCM_GROUP WHERE IsDeleted = 0 $groupID_conds $conds $sort";
		return $this->returnResultSet($sql);
	}
	
	public function getGroupCode($groupID){
		
		$groupInfo = $this->getGroupInfo($groupID);
		$groupCode = $groupInfo[0]['Code'];
		return $groupCode;
		
	}
	public function getGroupSqlForDbTable($keyword){
		$settings = $this->getGeneralSettings();
		$academicYearID = $settings['defaultAcademicYearID'];
		if(empty($academicYearID)){
			$academicYearID = Get_Current_Academic_Year_ID();
		}

        $cond = "";
		if($keyword!==''&&$keyword!==NULL){
			$cond .=  " AND
					 	(
					 		Code Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
					 		OR GroupName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
					 		OR GroupNameChi Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
					 	) ";
		}
		
		$GroupNameField = Get_Lang_Selection('GroupNameChi','GroupName');
		
		$sql = "SELECT 
					ipg.Code,
					$GroupNameField as GroupName,
					/*CONCAT('<a href=\"index.php?p=setting.group.item.',ipg.GroupID,'\" >' ,GroupName, '</a>') AS GroupName,*/
					CONCAT('<a href=\"index.php?p=setting.group.item.',ipg.GroupID,'\" >' ,IF(ipgfi.countItem is null,'0',ipgfi.countItem) , '</a>') AS ItemLink,
					/*Budget AS Budget,*/
					ifnull(CONCAT('<a href=\"index.php?p=setting.group.budget.',ipg.GroupID,'\" >' , FORMAT(Budget,2), '</a>'),CONCAT('<a href=\"index.php?p=setting.group.budget.',ipg.GroupID,'\">0.00</a>')) AS Budget,
					CONCAT('<a href=\"index.php?p=setting.groupMember.',ipg.GroupID,'\" >' ,COUNT(ipgm.UserID), '</a>') AS Member,
					CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipg.GroupID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />'),
					ipgfi.countItem AS ItemCount,
					Budget AS BudgetAmount,
					COUNT(ipgm.UserID) AS MemberCount
				FROM INTRANET_PCM_GROUP AS ipg
					LEFT JOIN INTRANET_PCM_GROUP_BUDGET AS ipgb ON (ipg.GroupID = ipgb.GroupID AND AcademicYearID = $academicYearID AND ipgb.IsDeleted = 0)
					LEFT JOIN INTRANET_PCM_GROUP_MEMBER AS ipgm ON (ipg.GroupID = ipgm.GroupID AND ipgm.UserID != '0')
					/*LEFT JOIN INTRANET_PCM_GROUP_FINANCIAL_ITEM as ipgfi on (ipg.GroupID = ipgfi.GroupID)*/
					left join (select groupID, count(itemID) as countItem from INTRANET_PCM_GROUP_FINANCIAL_ITEM where isDeleted = 0 group by GroupID) as ipgfi on (ipgfi.groupID = ipg.GroupID)
				WHERE ipg.IsDeleted = 0  $cond
				GROUP BY ipg.GroupID		";

		return $sql;
	}
	public function getGroupInfoByMemberUserID($UserID=''){
		if($UserID===''){
			global $UserID;
		}
		$sql = "Select *
		From INTRANET_PCM_GROUP AS ipg
		INNER JOIN INTRANET_PCM_GROUP_MEMBER AS ipgm ON (ipg.GroupID = ipgm.GroupID)
		WHERE ipg.IsDeleted = 0 AND ipgm.UserID = '$UserID'";
	
		return $this->returnResultSet($sql);
	}
	public function getGroupHeadByUserID($parUserID){
	    $sql = "SELECT GroupID FROM INTRANET_PCM_GROUP_MEMBER WHERE UserID = '$parUserID' and IsGroupHead = 1";
	    // is Delete???
	    return $this->returnResultSet($sql);
	}
	public function getGroupMemberInfo($groupID){
		global $ePCMcfg;
		$sql = "SELECT ipgm.UserID, GroupID, IsGroupHead, UserLogin, EnglishName, ChineseName
				FROM INTRANET_PCM_GROUP_MEMBER AS ipgm
				INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS iu ON (iu.UserID = ipgm.UserID)
				WHERE GroupID = '$groupID'";
		return $this->returnResultSet($sql);
	}
	public function getGroupMemberSqlForDbTable($groupID){
		global $ePCMcfg;
		$name_field = getNameFieldByLang2('iu.');
		$sql = "SELECT 
					CONCAT($name_field,IF(`IsGroupHead`='1',' <span class=\"tabletextrequire\">*</span>','')) as UserID,
					CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', gm.GroupMemberID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')		
				FROM 
					INTRANET_PCM_GROUP_MEMBER as gm
					INNER JOIN ".$ePCMcfg['INTRANET_USER']." as iu on (gm.UserID = iu.UserID)
				WHERE 
					gm.GroupID = '$groupID'";
		return $sql;
	} 
	public function insertGroupMember($userIDAry,$groupID){
		$userIDAry = (array)$userIDAry;
		
		$groupID = $this->Get_Safe_Sql_Query($groupID);
		$count = 0;
		foreach($userIDAry as $userID){
			$sql = "INSERT INTO INTRANET_PCM_GROUP_MEMBER (UserID,GroupID,IsGroupHead) VALUES ('$userID','$groupID','0')";
			$success = $this->db_db_query($sql);
			if($success){
				$count++;
			}
		}
		return $count;
	}
	public function deleteGroupMember($groupMemberIDArray){
		$UserID = $_SESSION["UserID"];
		$groupMemberIDArray = (array)$groupMemberIDArray;
		$groupMemberIDCond = implode("','",$groupMemberIDArray);
		
		$sql = "DELETE FROM INTRANET_PCM_GROUP_MEMBER  

					WHERE
						GroupMemberID IN ('$groupMemberIDCond') 					
						";
		$this->db_db_query($sql);
	}
	public function GroupMemberAssignAdmin($groupMemberIDArray,$setAdmin){
		$UserID = $_SESSION["UserID"];
		$groupMemberIDArray = (array)$groupMemberIDArray;
		$groupMemberIDCond = implode("','",$groupMemberIDArray);
		if($setAdmin){
			$isGroupHead = 1;
		}else{
			$isGroupHead = 0;
		}
		$sql = "UPDATE INTRANET_PCM_GROUP_MEMBER  
					SET
						IsGroupHead = '$isGroupHead'	
					WHERE
						GroupMemberID IN ('$groupMemberIDCond') 					
						";
		
// 		debug_pr($sql);
		$this->db_db_query($sql);
	}
	
	public function GetGroupMemberIdArr($groupId, $userIdArray){
		
		$userId_arr = implode("','", $userIdArray);
		
		$sql = " select GroupMemberID from INTRANET_PCM_GROUP_MEMBER where GroupID = '$groupId' and UserID in ( '".$userId_arr."') ";
		//debug_pr($sql);
		return $this->returnVector($sql);
	}

	public function getRuleInfo($ruleIDAry=''){
		$cond = '';
		if($ruleIDAry!=''){
			$ruleIDAry = (array)$ruleIDAry;
			$ruleIDCond = implode("','",$ruleIDAry);
			$cond.= " AND RuleID IN ('$ruleIDCond') " ;
		}
		$sql = "Select * From INTRANET_PCM_RULE WHERE 1 $cond AND IsDeleted = '0'";
		return $this->returnResultSet($sql);
	}
	public function getRuleTemplateInfo($ruleID='', $quotationType=''){
		$conds ='';
		if($ruleID!==''){
			$conds .= " AND ipr.RuleID = '$ruleID' ";
		}
		if($quotationType!==''){
			$conds .= " AND ipr.QuotationType = '$quotationType' ";
		}
		$sql = "Select ipr.*, count(ipa.AttachmentID) as numAttach From INTRANET_PCM_RULE as ipr LEFT JOIN INTRANET_PCM_ATTACHMENT as ipa on (ipa.RuleID = ipr.RuleID  AND ipa.IsDeleted = '0' )   WHERE ipr.IsTemplate = 1  AND ipr.IsDeleted = '0' $conds GROUP BY ipr.RuleID ORDER BY ipr.FloorPrice ASC, ipr.CeillingPrice ASC";
		return $this->returnResultSet($sql);
	}
	
	public function getRuleOInfo(){
		$sql = "Select * From INTRANET_PCM_RULE WHERE Code  = 'O' AND IsDeleted = '0' $conds  ORDER BY FloorPrice ASC, CeillingPrice ASC";
		return $this->returnResultSet($sql);
	}
	public function saveRule($ruleID='',$code,$floorPrice,$cellingPrice='',$approvalRoleID='',$needGroupHeadApproval='',$quotationType,$minQuote=0,$quotationApprovalRoleID='',$tenderAuditingRoleID='',$tenderApprovalRoleID='',$isTemplate=0,$isDeleted=0,$needVicePricipalApproval=0, $approvalMode='T', $caseApprovalRuleJson='', $custApprovalMode='T', $custApprovalRuleJson=''){
		$UserID = $_SESSION["UserID"];
		($cellingPrice==='')?$cellingPrice='NULL':$cellingPrice="'".$cellingPrice."'"; ###Important: prevent passing 0 to db

		if($ruleID<0 || $ruleID==''){
			//Insert
			$sql = "INSERT INTO INTRANET_PCM_RULE 
						(
						Code, FloorPrice, CeillingPrice, ApprovalRoleID, 
						NeedGroupHeadApprove, QuotationType, MinQuotation, QuotationApprovalRoleID, 
						TenderAuditingRoleID, TenderApprovalRoleID, IsTemplate, DateInput, 
						DateModified, InputBy, ModifiedBy,IsDeleted, 
						NeedVicePrincipalApprove, ApprovalMode, CaseApprovalRule, CustApprovalMode, CustCaseApprovalRule
						)
					VALUES
						(
						'$code', '$floorPrice', $cellingPrice, '$approvalRoleID', 
						'$needGroupHeadApproval', '$quotationType', '$minQuote', '$quotationApprovalRoleID', 
						'$tenderAuditingRoleID', '$tenderApprovalRoleID', '$isTemplate', NOW(),
						NOW(), '$UserID', '$UserID', $isDeleted, 
						'$needVicePricipalApproval', '$approvalMode', '$caseApprovalRuleJson', '$custApprovalMode', '$custApprovalRuleJson'
						)";
		}else{
			//Update
			$sql = "UPDATE INTRANET_PCM_RULE 
						SET 
							Code = '$code',
							FloorPrice = '$floorPrice',
							CeillingPrice = $cellingPrice,
							ApprovalRoleID = '$approvalRoleID',
							NeedGroupHeadApprove = '$needGroupHeadApproval',
							QuotationType = '$quotationType',
							MinQuotation = '$minQuote',
							QuotationApprovalRoleID = '$quotationApprovalRoleID',
							TenderAuditingRoleID = '$tenderAuditingRoleID',
							TenderApprovalRoleID = '$tenderApprovalRoleID',
							IsTemplate = '$isTemplate',
							DateModified = NOW(),		
							ModifiedBy = '$UserID',
							IsDeleted = '$isDeleted',
							NeedVicePrincipalApprove = '$needVicePricipalApproval',
							ApprovalMode = '$approvalMode',
							CaseApprovalRule = '$caseApprovalRuleJson',
							CustApprovalMode = '$custApprovalMode',
							CustCaseApprovalRule  = '$custApprovalRuleJson'
					WHERE RuleID = '$ruleID' ";
		}
		$this->db_db_query($sql);
	}
	public function deleteRule($ruleIDAry){
		$UserID = $_SESSION["UserID"];
		$ruleIDAry = (array)$ruleIDAry;
		$ruleIDCond = implode("','",$ruleIDAry);
		
		$sql = "UPDATE INTRANET_PCM_RULE 
					Set
						IsDeleted = '1',
						DateModified = NOW(),
						ModifiedBy = '$UserID'
					WHERE
						RuleID IN ('$ruleIDCond') 					
						";
		$this->db_db_query($sql);
	}
	
	public function getSupplierSqlForDbTable($typeID='', $isActive='',$keyword='',$excludedSupplierAry='',$returnSql=1,$additionSelectStatement="",$withLinks=false){
		if($typeID!=''){
			$conds .= " AND ipst.TypeID = '$typeID' ";
		}
		if($isActive!==''&&$isActive!==NULL){
			$conds .= " AND ips.IsActive = '$isActive' ";
		}
		if($keyword!==''&&$keyword!==NULL){
			$conds .= " AND 
						(
							SupplierName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR SupplierNameChi Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							
							OR	Description	Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' 
						)			";
		}
		if($excludedSupplierAry!==''){
			$supplierList = implode("','",(array)$excludedSupplierAry);
			$conds .= " AND ips.SupplierID NOT IN ('$supplierList') ";
			$onClick = "onchangeSupplierID(',ips.SupplierID,');";
		}else{
			$onClick = "unset_checkall(this, document.getElementById(\'form1\'));";
		}
		if($withLinks){	
			$SupplierNameField = 'CONCAT(\'<a href=javascript:editSupplier(\',cast(ips.SupplierID as char),\');>\','.Get_Lang_Selection('SupplierNameChi', 'SupplierName').',\'</a>\') as SupplierName';
			$TypeNameField = Get_Lang_Selection('TypeNameChi as Type', 'TypeName as Type');
// 			$TypeNameField = 'CONCAT(\'<a href="javascript:editType(\',ipsm.TypeID,\');">\','.Get_Lang_Selection('TypeNameChi', 'TypeName').',\'</a>\')  as Type';
		}else{
			$SupplierNameField = Get_Lang_Selection('SupplierNameChi as SupplierName', 'SupplierName as SupplierName');
			$TypeNameField = Get_Lang_Selection('TypeNameChi as Type', 'TypeName as Type');
		}
		
		$sql = "SELECT * FROM (SELECT ".$SupplierNameField.", ".$TypeNameField.", Description," .
				"(SELECT count(`SupplierID`) FROM `INTRANET_PCM_CASE_QUOTATION` `ipcq` where `IsDeleted`='0' AND `ipcq`.`SupplierID`=`ips`.`SupplierID` AND `CaseID` IN (SELECT `CaseID` FROM `INTRANET_PCM_CASE` WHERE `IsDeleted`='0' AND `RuleID` IN (SELECT `RuleID` FROM `INTRANET_PCM_RULE` WHERE `IsDeleted`='0' AND `QuotationType`='Q'))) AS QuoteInvitation," .
				"(SELECT count(`SupplierID`) FROM `INTRANET_PCM_CASE_QUOTATION` `ipcq` WHERE `IsDeleted`='0' AND `ipcq`.`SupplierID`=`ips`.`SupplierID` AND `CaseID` IN (SELECT `CaseID` FROM `INTRANET_PCM_CASE` WHERE `IsDeleted`='0' AND `RuleID` IN (SELECT `RuleID` FROM `INTRANET_PCM_RULE` WHERE `IsDeleted`='0' AND `QuotationType`='Q')) AND `QuoteDate` IS NOT NULL) AS Quote," .
				"(SELECT count(`SupplierID`) FROM `INTRANET_PCM_CASE_QUOTATION` `ipcq` where `IsDeleted`='0' AND `ipcq`.`SupplierID`=`ips`.`SupplierID` AND `CaseID` IN (SELECT `CaseID` FROM `INTRANET_PCM_CASE` WHERE `IsDeleted`='0' AND `RuleID` IN (SELECT `RuleID` FROM `INTRANET_PCM_RULE` WHERE `IsDeleted`='0' AND `QuotationType`='T'))) AS BidInvitation," .
				"(SELECT count(`SupplierID`) FROM `INTRANET_PCM_CASE_QUOTATION` `ipcq` WHERE `IsDeleted`='0' AND `ipcq`.`SupplierID`=`ips`.`SupplierID` AND `CaseID` IN (SELECT `CaseID` FROM `INTRANET_PCM_CASE` WHERE `IsDeleted`='0' AND `RuleID` IN (SELECT `RuleID` FROM `INTRANET_PCM_RULE` WHERE `IsDeleted`='0' AND `QuotationType`='T')) AND `QuoteDate` IS NOT NULL) AS Bid," .
//				"(SELECT count(`SupplierID`) FROM `INTRANET_PCM_CASE_QUOTATION` `ipcq` WHERE `IsDeleted`='0' AND `ipcq`.`SupplierID`=`ips`.`SupplierID` AND `CaseID` IN (SELECT `CaseID` FROM `INTRANET_PCM_RESULT` WHERE `IsDeleted`='0') AND `CaseID` IN (SELECT `CaseID` FROM `INTRANET_PCM_CASE` WHERE `IsDeleted`='0')) AS Deal," .
				"(SELECT count(`SupplierID`)        FROM `INTRANET_PCM_RESULT` `ipr` WHERE `IsDeleted`='0' AND `ipr`.`SupplierID`=`ips`.`SupplierID` AND `CaseID` IN (SELECT `CaseID` FROM `INTRANET_PCM_CASE` WHERE `IsDeleted`='0')) AS Deal," .
				"(SELECT ifnull(max(`DealDate`),'') FROM `INTRANET_PCM_RESULT` `ipr` WHERE `IsDeleted`='0' AND `ipr`.`SupplierID`=`ips`.`SupplierID` AND `CaseID` IN (SELECT `CaseID` FROM `INTRANET_PCM_CASE` WHERE `IsDeleted`='0')) AS LastDealDate," .
				"CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ips.SupplierID ,'\" onclick=\"$onClick\" />') $additionSelectStatement
			FROM INTRANET_PCM_SUPPLIER AS ips
			LEFT JOIN INTRANET_PCM_SUPPLIER_TYPE_MAPPING AS ipsm ON (ips.SupplierID=ipsm.SupplierID)
			INNER JOIN INTRANET_PCM_SUPPLY_TYPE AS ipst ON (ipsm.TypeID = ipst.TypeID)
			WHERE ips.IsDeleted = 0 $conds) as `data`";
		if($returnSql){
			return $sql;
		}else{
			$sql .= " ORDER BY SupplierName";
			return $this->returnResultSet($sql);
		}
	}
	
	public function getSupplierTypeSqlForDbTable($keyword=''){
		if($keyword!==''&&$keyword!==NULL){
			$conds .= " AND 
						(
							a.TypeName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR a.TypeNameChi Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
						)			";
		}
		$onClick = "unset_checkall(this, document.getElementById(\'form1\'));";
		
		$TypeNameField = Get_Lang_Selection('TypeNameChi','TypeName');
		
		
		
		
		$sql = 'SELECT CONCAT(\'<a href="javascript:editType(\',cast(TypeID as char),\');">\','.$TypeNameField.',\'</a>\')  as TypeName,CONCAT(\'<input type="checkbox" name="targetIdAry[]" id="targetIdChk_Global" value="\', TypeID ,\'" onclick="'.$onClick.'" />\') FROM `INTRANET_PCM_SUPPLY_TYPE` AS a WHERE `IsDeleted` = 0 '.$conds;
		return $sql;
	}

    // Category
    public function getCategoryInfo($catIDAry, $keyword=''){

        if(!empty($catIDAry)){
            $catIDCond = " AND CategoryID IN ('".implode("','", (array)$catIDAry)."') ";
        }
        if($keyword!==''&&$keyword!==NULL){
            $conds = " AND 
						(
							CategoryName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR Code Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
						)			";
        }

        $sort = "ORDER BY Code Asc";

        $sql = "SELECT * FROM INTRANET_PCM_CATEGORY WHERE isDeleted = 0 $catIDCond $conds $sort";
        return $this->returnResultSet($sql);
    }
	public function getCategoryCode($catID){
		$catAry = $this->getCategoryInfo($catID);
		return $catAry[0]['Code'];
	}
	public function getCategorySqlForDbTable($keyword=''){
		if($keyword!==''&&$keyword!==NULL){
			$conds .= " AND 
						(
							CategoryName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR CategoryNameChi Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							
							OR	Code	Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' 
						)			";
		}
		
		$CategoryNameField = Get_Lang_Selection('CategoryNameChi as CategoryName', 'CategoryName as CategoryName');
		
		$sql = "SELECT Code, $CategoryNameField,CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipc.CategoryID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
			FROM INTRANET_PCM_CATEGORY  AS ipc
			WHERE ipc.IsDeleted = 0 $conds";
		return $sql;
		
	}
	
	#### ROLE
	public function getRoleInfo($roleIDAry='', $ForCaseApproval='',$ForQuotationApproval='',$ForTenderAuditing='',$ForTenderApproval='',$isDeleted='',$isPrincipal=''){
		$cond = '';
		if($roleIDAry!=''){
			$roleIDAry = (array)$roleIDAry;
			$roleIDCond = implode("','",$roleIDAry);
			$cond.= " AND RoleID IN ('$roleIDCond') " ;
		}
		
		$cond.= ($isDeleted!=='')?" AND IsDeleted = '$isDeleted' ":"";
		
		$cond.= ($ForCaseApproval!=='')?" AND ForCaseApproval = '$ForCaseApproval' ":"";
		$cond.= ($ForQuotationApproval!=='')?" AND ForQuotationApproval = '$ForQuotationApproval' ":"";
		$cond.= ($ForTenderAuditing!=='')?" AND ForTenderAuditing = '$ForTenderAuditing' ":"";
		$cond.= ($ForTenderApproval!=='')?" AND ForTenderApproval = '$ForTenderApproval' ":"";
		$cond.= ($isPrincipal!=='')?" AND IsPrincipal = '$isPrincipal' ":"";
		
		$sql = "SELECT * FROM INTRANET_PCM_ROLE WHERE 1 $cond";
		return $this->returnResultSet($sql);
	}
	public function getRoleUser($roleID='',$academicYearID='',$isVicePrincipal=''){
		global $ePCMcfg;
		if($roleID!=''){
			$cond = " AND RoleID ='$roleID' ";
		}
        $academicYearID=$academicYearID;
        if(empty($academicYearID)) {
            if ($academicYearID == '') {
                $gsObj = $this->getGeneralSettings();
                $academicYearID = $gsObj['defaultAcademicYearID'];
            }
        }
		if($isVicePrincipal!=''){
			$cond.= " AND IsVicePrincipal = 1 ";
			$extraJoin.=" INNER JOIN INTRANET_PCM_ROLE AS role ON (ipru.RoleID=role.RoleID) ";
		}
            $sorting ="ORDER BY iu.EnglishName ASC, iu.ChineseName ASC";

		$sql = "SELECT RoleUserID, ipru.UserID, ipru.RoleID, ipru.AcademicYearID, ipru.IsActive, iu.UserLogin, iu.EnglishName, iu.ChineseName
				FROM INTRANET_PCM_ROLE_USER AS ipru
				INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS iu	ON (iu.UserID = ipru.UserID)
				LEFT JOIN 	INTRANET_ARCHIVE_USER AS iau ON (iu.UserID = iau.UserID)
				$extraJoin			
				WHERE ipru.IsDeleted = 0 AND IsActive = 1 AND AcademicYearID = '$academicYearID' AND iau.UserID IS NULL $cond $sorting";
		return $this->returnResultSet($sql);
	}

	public function getRoleSqlForDbTable($keyword='', $roleYearID=''){
		$gsObj = $this->getGeneralSettings();
        $academicYearID=$roleYearID;
        if(empty($academicYearID)) {
            $academicYearID = $gsObj['defaultAcademicYearID'];
            if (empty($academicYearID)) {
                $academicYearID = Get_Current_Academic_Year_ID();
            }
        }

        $conds = "";
		if($keyword!==''&&$keyword!==NULL){
			$conds .= " AND 
						(
							RoleName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR RoleNameChi Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							
						)			";
		}
		
		$RoleNameField=Get_Lang_Selection('RoleNameChi as RoleName','RoleName as RoleName');
		
		$sql = "SELECT ".$RoleNameField.",
				CONCAT('<a href=\"index.php?p=setting.roleMember.',ipr.RoleID,'&academicYearId=$academicYearID\" >',COUNT(UserID),'</a>') AS Member ,
				IF( IsPrincipal = 0 AND IsVicePrincipal = 0,
					CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipr.RoleID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />'),
					'<img src=\"/images/red_checkbox.gif\" vspace=\"3\" hspace=\"4\" border=\"0\"'),
				COUNT(UserID) as MemberCount
			FROM INTRANET_PCM_ROLE  AS ipr
			LEFT JOIN INTRANET_PCM_ROLE_USER AS ipru ON (ipr.RoleID=ipru.RoleID AND ipru.IsDeleted = 0 AND AcademicYearID = $academicYearID)		
			WHERE ipr.IsDeleted = 0 $conds
			GROUP BY ipr.RoleID		
					";
		return $sql;
	}
	public function getRoleUserSqlDbTable($roleID, $roleYearID=''){
		global $ePCMcfg, $_PAGE;

        $linterface = $_PAGE['libPCM_ui'];
        $requiredSymbol = $linterface->RequiredSymbol();
		$name_field = getNameFieldByLang2('iu.');
		$archiveNameField = getNameFieldByLang2('iau.');
        $concatArchiveUser = "CONCAT('$requiredSymbol',$archiveNameField)";
        $academicYearID=$roleYearID;
        if(empty($academicYearID)) {
            $gsObj = $this->getGeneralSettings();
            $academicYearID = $gsObj['defaultAcademicYearID'];
        }
		$sql = "SELECT IFNULL($concatArchiveUser , $name_field ) as UserID ,
				CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ru.RoleUserID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
				FROM INTRANET_PCM_ROLE_USER as ru
				left JOIN ".$ePCMcfg['INTRANET_USER']." as iu on (ru.UserID = iu.UserID)
				left JOIN INTRANET_ARCHIVE_USER AS iau ON (iau.UserID = ru.UserID)
				WHERE ru.IsDeleted = 0 AND ru.IsActive = 1 AND ru.AcademicYearID = '$academicYearID' AND ru.RoleID = '$roleID'			";

		return $sql;
	}

	public function insertRoleMember($userIDAry,$roleID,$roleYearID=''){
		$ParUserID = $_SESSION["UserID"];

		$roleID = $this->Get_Safe_Sql_Query($roleID);

        $academicYearID=$roleYearID;
        if(empty($academicYearID)) {
            $gsObj = $this->getGeneralSettings();
            $academicYearID = $gsObj['defaultAcademicYearID'];
        }
		$userIDAry = (array)$userIDAry;
		foreach($userIDAry as $userID){
			$sql = "INSERT INTO INTRANET_PCM_ROLE_USER (UserID,RoleID,IsActive,AcademicYearID,DateInput,InputBy) VALUES ('$userID','$roleID',1,'$academicYearID',NOW(),'$ParUserID')";
			$this->db_db_query($sql);
		}
	}
	public function deleteRoleMember($roleMemberIDArray){
		$UserID = $_SESSION["UserID"];
		$roleMemberIDArray = (array)$roleMemberIDArray;
		$roleMemberIDCond = implode("','",$roleMemberIDArray);
		
		$sql = "UPDATE INTRANET_PCM_ROLE_USER
					SET IsDeleted = 1,
						DeletedBy = '$UserID'	
					WHERE
						RoleUserID IN ('$roleMemberIDCond') 					
						";
		$this->db_db_query($sql);
	}
	########### Quotation
	public function getCaseQuotation($caseID='',$quotationID='', $needStatus=false){
		global $ePCMcfg;
		$name_field = getNameFieldByLang2('iu.');
		if($caseID!==''){
			$cond = " AND ipcq.CaseID = '$caseID' ";
		}
		if($quotationID!==''){
			$cond .= " AND QuotationID = '$quotationID' ";
		}
		
		if($needStatus&&$caseID!==''){
			$selectStatment = ", IF(ipcq.QuoteDate > ipc.QuotationEndDate, 1,0)  AS IsLate ";
			$selectStatment .= ", IF(ipcq.QuoteDate IS NULL , 1,0)  AS IsNoReply ";
			$joinStatment = " INNER JOIN INTRANET_PCM_CASE AS ipc ON (ipc.CaseID=ipcq.CaseID) "; 
		}
		
		$sql = "SELECT ipcq.QuotationID, ipcq.CaseID, ipcq.SupplierID, ips.SupplierName, ips.SupplierNameChi, ips.Website,ips.Fax,ips.Email,ips.Phone,  ipcq.ReplyStatus,  ipcq.Remarks, ipcq.ContactID, ipcq.ReplyDate,  ipcq.QuoteDate, ipcq.ContactMethod, ipcq.DateInput, ipcq.DateModified, ipcq.InputBy, ipcq.ModifiedBy, $name_field AS ModifiedByUserName $selectStatment
				FROM INTRANET_PCM_CASE_QUOTATION AS ipcq
				INNER JOIN INTRANET_PCM_SUPPLIER AS ips ON (ipcq.SupplierID=ips.SupplierID)
				INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS iu ON (iu.UserID=ipcq.ModifiedBy)
				$joinStatment
		WHERE ipcq.IsDeleted = 0 $cond";
		return $this->returnResultSet($sql);
	}
	####### Case Remark
	public function getCaseRemarks($caseID='', $stageID=''){
		$cond = '';
		if($caseID!==''){
			$cond .= " AND CaseID = '$caseID' ";
		}
		if($stageID!==''){
			$cond .= " AND StageID = '$stageID' ";
		}
		$sql = "SELECT * FROM INTRANET_PCM_CASE_REMARK WHERE IsDeleted = 0 $cond";
		return $this->returnResultSet($sql);
	}
	### Result
	public function getResult($caseID=''){
		global $ePCMcfg;
		if($caseID!==''){
			$cond = " AND ipr.CaseID = '$caseID' ";


		}
		$name_field = getNameFieldByLang2('modifyUser.');

		$sql = "SELECT 
					ipr.ResultID, 
					ipr.CaseID,
					ipr.SupplierID,
					ips.SupplierName,
					ips.SupplierNameChi,
					ipr.DealPrice,
					ipr.Description,
					ipr.DealDate,
					ipr.DateModified,
					ipr.ModifiedBy,
					ipr.FundingSource1,
					ipr.UnitPrice1,
					ipr.FundingSource2,
					ipr.UnitPrice2,
					$name_field AS ModifyUserName
				FROM INTRANET_PCM_RESULT AS ipr
				INNER JOIN INTRANET_PCM_SUPPLIER AS ips ON (ips.SupplierID = ipr.SupplierID)
				INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS modifyUser ON (ipr.ModifiedBy = modifyUser.UserID)
				WHERE ipr.IsDeleted = 0  $cond";
		return $this->returnResultSet($sql);
	}
	
	public function getCompletedCaseApplicantUserInfo($AcademicYearID = ''){
		global $ePCMcfg;
		
		$cond = " AND CurrentStage = 10 ";
		$cond .= " AND ipc.IsDeleted = 0 ";
		$cond2 = " AND ipc.RuleID NOT IN (Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = 'O') ";
				
		if($AcademicYearID){
			$cond .= " AND ipc.AcademicYearID = '".$AcademicYearID."'";
		}

		$sql = "SELECT 
					IF (ApplicantType='I',CONCAT('U', applicantuser.UserID),CONCAT('G', applicantgroup.GroupID)) AS UserID,
					IF (ApplicantType='I',applicantuser.ChineseName,applicantgroup.GroupNameChi) AS ChineseName,
					IF (ApplicantType='I',applicantuser.EnglishName,applicantgroup.GroupName) AS EnglishName
				FROM INTRANET_PCM_CASE AS ipc
				INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS applicantuser ON (applicantuser.UserID= ipc.ApplicantUserID)
				INNER JOIN INTRANET_PCM_GROUP AS applicantgroup ON (applicantgroup.GroupID = ipc.ApplicantGroupID)
				WHERE 1 $cond $cond2"; 
		
		$result = $this->returnResultSet($sql);

		$serialized = array_map('serialize', $result);
	    $unique = array_unique($serialized);
	    return array_values(array_intersect_key($result, $unique));
	}
	
	public function getCompletedCaseSqlForDbTable(){	//2016-09-29	Villa
		global $ePCMcfg, $Lang, $sys_custom;
		$name_field = getNameFieldByLang2('applicantuser.');

		$cond = " AND CurrentStage = 10 ";
		$cond .= " AND ipc.IsDeleted = 0 ";
		$cond2 = " AND ipc.RuleID NOT IN (Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = 'O') ";
		
		$ItemNameField = Get_Lang_Selection('ipgf.ItemNameChi as ItemName','ipgf.ItemName as ItemName');
		$CategoryNameField = Get_Lang_Selection('cat.CategoryNameChi as CategoryName','cat.CategoryName as CategoryName');
		$GroupNameField = Get_Lang_Selection('applicantgroup.GroupNameChi','applicantgroup.GroupName');
		$SupplierNameField = Get_Lang_Selection('dealsupplier.SupplierNameChi AS DealSupplier','dealsupplier.SupplierName AS DealSupplier');

		/**
		 * MYSQL BUG
		 * - ref: http://bugs.mysql.com/bug.php?id=12030
		 * Solutions: cast(ipc.CaseID as char)
		 */
		$sql = "SELECT 
					ipc.Code,
					CONCAT('<a href=\"index.php?p=mgmt.caseDetials.',cast(ipc.CaseID as char),'\">',CaseName,'</a>') AS CaseName,
					$ItemNameField,
					$CategoryNameField,
					IF (ApplicantType='I',$name_field,$GroupNameField) AS Applicant,
					DATE_FORMAT(ipc.DateInput,'%Y-%m-%d')  AS ApplicationDate,
					ipc.Budget,
					ipr.DealPrice,
					DATE_FORMAT(ipr.DealDate,'%Y-%m-%d') ,
					$SupplierNameField,
					IF(rule.CeillingPrice > 0 AND rule.CeillingPrice <= '".$sys_custom['ePCM']['enablePrincipalApprovalCompletedCaseWithBudgetAtOrBelow']."',
						IF(ipc.IsPrincipalApproved = '1','".$Lang['ePCM']['Mgmt']['Case']['Status']['Confirmed']."',IF(ipc.IsPrincipalApproved = '0','".$Lang['ePCM']['Mgmt']['Case']['Status']['PendingForConfirmation']."','".$Lang['ePCM']['Mgmt']['Case']['Status']['Reject']."')),
						'--'
					) as RecordStatus,
					IF(rule.CeillingPrice > 0 AND rule.CeillingPrice <= '".$sys_custom['ePCM']['enablePrincipalApprovalCompletedCaseWithBudgetAtOrBelow']."',
							IF(ipc.IsPrincipalApproved = '1',
								CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipc.CaseID ,'\" isApprove =\"1\" canApprove =\"1\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />'),
								CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipc.CaseID ,'\"  canApprove =\"1\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
							),
							''
					)
				FROM INTRANET_PCM_CASE AS ipc
				INNER JOIN INTRANET_PCM_GROUP_FINANCIAL_ITEM AS ipgf ON (ipc.ItemID = ipgf.ItemID)
				INNER JOIN INTRANET_PCM_CATEGORY AS cat ON (cat.CategoryID = ipc.CategoryID)
				INNER JOIN INTRANET_PCM_RESULT AS ipr ON (ipr.CaseID=ipc.CaseID)
				INNER JOIN INTRANET_PCM_SUPPLIER AS dealsupplier ON (dealsupplier.SupplierID = ipr.SupplierID)
				INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS applicantuser ON (applicantuser.UserID= ipc.ApplicantUserID)
				INNER JOIN INTRANET_PCM_GROUP AS applicantgroup ON (applicantgroup.GroupID = ipc.ApplicantGroupID)
				INNER JOIN INTRANET_PCM_RULE AS rule ON (rule.RuleID=ipc.RuleID)
				$isGroupMemberJoinCond
				WHERE 1 $cond $cond2";
// 		debug_pr($sql);
		return $sql;
	}
	

	public function getCompletedCaseSqlForDbTableForNonAdmin(){		//2016-09-29	Villa
		global $UserID;
		global $ePCMcfg, $sys_custom, $Lang;
		$name_field = getNameFieldByLang2('applicantuser.');
	
		$cond = " AND ipc.CurrentStage = 10 ";
		$cond .= " AND ipc.IsDeleted = 0 ";
		$cond2 = " AND ipc.RuleID NOT IN (Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = 'O')";
	
		$ItemNameField = Get_Lang_Selection('ipgf.ItemNameChi as ItemName','ipgf.ItemName as ItemName');
		$CategoryNameField = Get_Lang_Selection('cat.CategoryNameChi as CategoryName','cat.CategoryName as CategoryName');
		$GroupNameField = Get_Lang_Selection('applicantgroup.GroupNameChi','applicantgroup.GroupName');
		$SupplierNameField = Get_Lang_Selection('dealsupplier.SupplierNameChi AS DealSupplier','dealsupplier.SupplierName AS DealSupplier');
		
		/**
		 * MYSQL BUG
		 * - ref: http://bugs.mysql.com/bug.php?id=12030
		 * Solutions: cast(ipc.CaseID as char)
		 */
		$sql = "SELECT 
					ipc.Code,
					CONCAT('<a href=\"index.php?p=mgmt.caseDetials.',cast(ipc.CaseID as char),'\">',CaseName,'</a>') AS CaseName,
					$ItemNameField,
					$CategoryNameField,
					IF (ApplicantType='I',$name_field,$GroupNameField) AS Applicant,
					DATE_FORMAT(ipc.DateInput,'%Y-%m-%d')  AS ApplicationDate,
					ipc.Budget,
					ipr.DealPrice,
					DATE_FORMAT(ipr.DealDate,'%Y-%m-%d') ,
					$SupplierNameField,
					IF(rule.CeillingPrice > 0 AND rule.CeillingPrice <= '".$sys_custom['ePCM']['enablePrincipalApprovalCompletedCaseWithBudgetAtOrBelow']."',
						IF(ipc.IsPrincipalApproved = '1','".$Lang['ePCM']['Mgmt']['Case']['Status']['Confirmed']."',IF(ipc.IsPrincipalApproved = '0','".$Lang['ePCM']['Mgmt']['Case']['Status']['PendingForConfirmation']."','".$Lang['ePCM']['Mgmt']['Case']['Status']['Reject']."')),
						'--'
					) as RecordStatus,
					IF(rule.CeillingPrice > 0 AND rule.CeillingPrice <= '".$sys_custom['ePCM']['enablePrincipalApprovalCompletedCaseWithBudgetAtOrBelow']."',
							IF(ipc.IsPrincipalApproved = '1',
								CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipc.CaseID ,'\" isApprove =\"1\" canApprove =\"1\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />'),
								CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipc.CaseID ,'\"  canApprove =\"1\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
							),
							''
					)
				FROM INTRANET_PCM_CASE AS ipc
				INNER JOIN INTRANET_PCM_GROUP_FINANCIAL_ITEM AS ipgf ON (ipc.ItemID = ipgf.ItemID)
				INNER JOIN INTRANET_PCM_CATEGORY AS cat ON (cat.CategoryID = ipc.CategoryID)
				INNER JOIN INTRANET_PCM_RESULT AS ipr ON (ipr.CaseID=ipc.CaseID)
				INNER JOIN INTRANET_PCM_SUPPLIER AS dealsupplier ON (dealsupplier.SupplierID = ipr.SupplierID)
				INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS applicantuser ON (applicantuser.UserID= ipc.ApplicantUserID)
				INNER JOIN INTRANET_PCM_GROUP AS applicantgroup ON (applicantgroup.GroupID = ipc.ApplicantGroupID)
				INNER JOIN INTRANET_PCM_RULE AS rule ON (rule.RuleID=ipc.RuleID)
				".$this->innerJoinSqlForNonAdmin('ipc.',$UserID). $cond. $cond2;
		return $sql;
	}
	
		public function getTerminatedCaseSqlForDbTable(){
		global $ePCMcfg;
		$name_field = getNameFieldByLang2('applicantuser.');
		
		$cond = " AND ipc.IsDeleted = 1  AND ipc.CurrentStage > 0 ";
		
		$ItemNameField = Get_Lang_Selection('ipgf.ItemNameChi as ItemName','ipgf.ItemName as ItemName');
		$CategoryNameField = Get_Lang_Selection('cat.CategoryNameChi as CategoryName','cat.CategoryName as CategoryName');
		$GroupNameField = Get_Lang_Selection('applicantgroup.GroupNameChi','applicantgroup.GroupName');
// 		$SupplierNameField = Get_Lang_Selection('dealsupplier.SupplierNameChi AS DealSupplier','dealsupplier.SupplierName AS DealSupplier');
		
		/**
		 * MYSQL BUG
		 * - ref: http://bugs.mysql.com/bug.php?id=12030
		 * Solutions: cast(ipc.CaseID as char)
		 */
		$sql = "SELECT
			ipc.Code,
			CONCAT('<a href=\"index.php?p=mgmt.caseDetials.',cast(ipc.CaseID as char),'\">',CaseName,'</a>') AS CaseName,
			$ItemNameField,
			$CategoryNameField,
			IF (ApplicantType='I',$name_field,$GroupNameField) AS Applicant,
			DATE_FORMAT(ipc.DateInput,'%Y-%m-%d')  AS ApplicationDate,
			ipc.Budget,
			CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipc.CaseID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
		FROM INTRANET_PCM_CASE AS ipc
		INNER JOIN INTRANET_PCM_GROUP_FINANCIAL_ITEM AS ipgf ON (ipc.ItemID = ipgf.ItemID)
		INNER JOIN INTRANET_PCM_CATEGORY AS cat ON (cat.CategoryID = ipc.CategoryID)
		INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS applicantuser ON (applicantuser.UserID= ipc.ApplicantUserID)
		INNER JOIN INTRANET_PCM_GROUP AS applicantgroup ON (applicantgroup.GroupID = ipc.ApplicantGroupID)
		WHERE 1 $cond";
		return $sql;
	}
	public function getTerminatedCaseSqlForDbTableForNonAdmin(){
		global $UserID;
		global $ePCMcfg;
		$name_field = getNameFieldByLang2('applicantuser.');
		
		$cond = " AND ipc.IsDeleted = 1  AND ipc.CurrentStage > 0 ";
		
		$ItemNameField = Get_Lang_Selection('ipgf.ItemNameChi as ItemName','ipgf.ItemName as ItemName');
		$CategoryNameField = Get_Lang_Selection('cat.CategoryNameChi as CategoryName','cat.CategoryName as CategoryName');
		$GroupNameField = Get_Lang_Selection('applicantgroup.GroupNameChi','applicantgroup.GroupName');
		
		/**
		 * MYSQL BUG
		 * - ref: http://bugs.mysql.com/bug.php?id=12030
		 * Solutions: cast(ipc.CaseID as char)
		 */
		$sql = "SELECT
		ipc.Code,
		CONCAT('<a href=\"index.php?p=mgmt.caseDetials.',cast(ipc.CaseID as char),'\">',CaseName,'</a>') AS CaseName,
		$ItemNameField,
		$CategoryNameField,
		IF (ApplicantType='I',$name_field,$GroupNameField) AS Applicant,
		DATE_FORMAT(ipc.DateInput,'%Y-%m-%d')  AS ApplicationDate,
		ipc.Budget,
		CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipc.CaseID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
		FROM INTRANET_PCM_CASE AS ipc
		INNER JOIN INTRANET_PCM_GROUP_FINANCIAL_ITEM AS ipgf ON (ipc.ItemID = ipgf.ItemID)
		INNER JOIN INTRANET_PCM_CATEGORY AS cat ON (cat.CategoryID = ipc.CategoryID)
		INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS applicantuser ON (applicantuser.UserID= ipc.ApplicantUserID)
		INNER JOIN INTRANET_PCM_GROUP AS applicantgroup ON (applicantgroup.GroupID = ipc.ApplicantGroupID)
		" . $this->innerJoinSqlForNonAdmin('ipc.',$UserID)  . $cond;
		
		return $sql;
	}
	private function innerJoinSqlForNonAdmin($originalIpcPrefix='ipc.',$userId){
	    global $sys_custom;
        if($sys_custom['ePCM']['extraApprovalFlow']||$sys_custom['ePCM_skipCaseApproval']){
            $isCustEndorsement = ',IF(ipcca.ApproverUserID >= 1 , 1,0) as isCustApprover';
            $custApprovalTable =  "LEFT JOIN INTRANET_PCM_CUST_CASE_APPROVAL as ipcca ON ipcca.CaseID = ipc2.CaseID AND ipcca.ApproverUserID = $userId";
            $isCustEndorsementCond = 'OR isCustApprover = 1';
        }
		$sql ="INNER JOIN (SELECT 
								ipc2.CaseID,
								IF(case2.ApplicantUserID > 0,1,0) as isApplicant,
								IF(ipgm2.GroupMemberID > 0 , 1,0) as isGroupMember,
								IF(ipca2.ApproverUserID >= 1 , 1,0) as isApprover,
								IF(role1.RoleUserID > 0 , 1,0) as isTenderAuditing,
								IF(role2.RoleUserID > 0 , 1,0) as isTenderApproval,
								IF(role3.RoleUserID > 0 , 1,0) as isQuotationApproval
								$isCustEndorsement
								FROM INTRANET_PCM_CASE as ipc2
								LEFT JOIN INTRANET_PCM_GROUP_MEMBER as ipgm2 ON ipc2.ApplicantGroupID = ipgm2.GroupID  AND ipgm2.UserID = $userId
								LEFT JOIN INTRANET_PCM_CASE_APPROVAL as ipca2 ON ipca2.CaseID = ipc2.CaseID AND ipca2.ApproverUserID = $userId
								$custApprovalTable
								INNER JOIN INTRANET_PCM_RULE AS rule ON (rule.RuleID=ipc2.RuleID)
								LEFT JOIN INTRANET_PCM_ROLE_USER as role1 ON rule.TenderAuditingRoleID= role1.RoleID AND ipc2.AcademicYearID = role1.AcademicYearID AND role1.UserID = $userId
								LEFT JOIN INTRANET_PCM_ROLE_USER as role2 ON rule.TenderApprovalRoleID= role2.RoleID AND ipc2.AcademicYearID = role2.AcademicYearID AND role2.UserID= $userId
								LEFT JOIN INTRANET_PCM_ROLE_USER as role3 ON rule.QuotationApprovalRoleID= role3.RoleID AND ipc2.AcademicYearID = role3.AcademicYearID AND role3.UserID= $userId
								LEFT JOIN INTRANET_PCM_CASE as case2 ON case2.CaseID = ipc2.CaseID AND case2.ApplicantUserID = $userId
								WHERE  1 GROUP BY ipc2.CaseID) AS a ON (a.CaseID = ".$originalIpcPrefix."CaseID) 
		
		WHERE 1 AND ( isApplicant = 1 OR isGroupMember = 1 OR isApprover = 1 OR isTenderAuditing = 1 OR isTenderApproval = 1 OR isQuotationApproval = 1 $isCustEndorsementCond) ";
		return $sql;
	}
	public function getCurrentSettingsAcademicYear(){
		return $this->getGeneralSettings();
	}

    public function getSupplyTypeInfo($isDeleted=0,$typeID='', $excludeEmptySupplierType=false){
        $cond = '';
        if($typeID!==''){
            $cond .= " AND ipst.TypeID = '$typeID' ";
        }
        if($excludeEmptySupplierType){
            $joinMappingTable = 'INNER JOIN INTRANET_PCM_SUPPLIER_TYPE_MAPPING AS ipstm ON (ipst.TypeID = ipstm.TypeID)';
            $cond .= " Group BY ipst.TypeID";
        }
        $sql = "Select ipst.* FROM INTRANET_PCM_SUPPLY_TYPE AS ipst "."$joinMappingTable"." WHERE ipst.IsDeleted = $isDeleted $cond ORDER BY ipst.TypeName";
        return $this->returnResultSet($sql);
    }

    public function getParentUserInfo($excludeUserIdAry=''){
        global $ePCMcfg;
        if ($excludeUserIdAry !== '') {
            $conds_excludeUserId = " And UserID Not In ('".implode("','", (array)$excludeUserIdAry)."') ";
        }
        $sql = "SELECT UserID, EnglishName,ChineseName, UserLogin FROM ".$ePCMcfg['INTRANET_USER']." WHERE RecordType = 3 AND RecordStatus = 1 $conds_excludeUserId ORDER BY EnglishName, ChineseName";
        return $this->returnResultSet($sql);
    }
	
	public function getStaffUserInfo($teaching='',$excludeUserIdAry=''){
		global $ePCMcfg;
		if($teaching==-1) {
        		$conds = "";
        	}
        	else if($teaching==1)
	        	$conds = " AND Teaching=1";
	        else if($teaching==0)
	        	$conds = " AND (Teaching IS NULL OR Teaching=0 OR Teaching='')";
		if ($excludeUserIdAry !== '') {
	        	$conds_excludeUserId = " And UserID Not In ('".implode("','", (array)$excludeUserIdAry)."') ";
	      }
		
		$sql = "SELECT UserID, EnglishName,ChineseName, UserLogin FROM ".$ePCMcfg['INTRANET_USER']." WHERE RecordType = 1 AND RecordStatus = 1 $conds  $conds_excludeUserId ORDER BY EnglishName, ChineseName";
		return $this->returnResultSet($sql);
	} 
	
	public function getSMCUserInfo($excludeUserIdAry=''){
		global $ePCMcfg;
        if ($excludeUserIdAry !== '') {
            $conds_excludeUserId = " And UserID Not In ('".implode("','", (array)$excludeUserIdAry)."') ";
        }
		$sql = "SELECT UserID, EnglishName,ChineseName, UserLogin FROM ".$ePCMcfg['INTRANET_USER']." WHERE RecordType = 5 AND RecordStatus = 1 $conds_excludeUserId ORDER BY EnglishName, ChineseName";
		return $this->returnResultSet($sql);
	}
	
	
	######################################################################################
	######################################################################################
	###																					##
	###																					##
	###																					##
	###																					##
	###																					##
	###																					##
	######################################################################################
	######################################################################################
	
	public $debugMode = false;
	private function getStandardInsertValue($stringArr, $excludeCommonField=false){
		$userID = $_SESSION['UserID'];
		
		$delimeter = ', ';
		$numString = count($stringArr);
		if($numString > 0){
			$result = '( ';
			for($i = 0; $i < $numString; $i++){
				$_string = $this->Get_Safe_Sql_Query($stringArr[$i]);
				$result .= "'$_string'";
				if($i != ($numString-1)){
				    $result .= $delimeter;
				}
			}
			
			// before closing add "isDeleted", "DateInput", "InputBy", "DateModified", "ModifiedBy"
			if(!$excludeCommonField){
    			$result .= "$delimeter'0' ,now(), $userID, now(), $userID";
			}
			$result .= ' )';
		}
		return $result;
	}
	
	private function getSafeInsertValueList($fieldNameArr, $multiRowValueArr, $excludeCommonField=false){
		$valueListArr = array();
		foreach((array)$multiRowValueArr as $key => $row){
			$tempString = '';
			$valueListArr[] = $this->getStandardInsertValue($row, $excludeCommonField);
		}
		return $valueListArr;
	}
	
	public function insertData($tableName, $fieldNameArr, $multiRowValueArr, $excludeCommonField=false){
		
		if(empty($fieldNameArr) || empty($multiRowValueArr) || $tableName=='' ){
// 			$sql = "/*Cannot empty any param when calling libPCM_db->getInsertSQL */";
		}
		else{
			// Must have this 4 fields - "DateInput", "InputBy", "DateModified", "ModifiedBy"
			// and it will be add automatically
			if(!$excludeCommonField){
    			$fieldNameArr[] = 'IsDeleted';
    			$fieldNameArr[] = 'DateInput';
    			$fieldNameArr[] = 'InputBy';
    			$fieldNameArr[] = 'DateModified';
    			$fieldNameArr[] = 'ModifiedBy';
			}

			$valueListArr = $this->getSafeInsertValueList($fieldNameArr, $multiRowValueArr, $excludeCommonField);
			if(!empty($valueListArr)){
				$sql = "INSERT INTO $tableName (".implode(', ',$fieldNameArr).") Values ".implode(',', $valueListArr).";";
				//goSubmit()_pr($sql);
			}
			else{
// 				$sql = "/* NO VALUE */";
			}
		}
		if($this->debugMode){
			debug_pr($sql); 
		}
		else{
			return $this->db_db_query($sql);
			
		}
	}
	
	
	
	public function deleteData($tableName, $primaryKeyName, $primaryKeyValArr){
		$userID = $_SESSION['UserID'];
		$sql = "UPDATE $tableName SET IsDeleted = 1, DateModified = now(), ModifiedBy = '$userID' WHERE `$primaryKeyName` IN ('".implode("','", (array)$primaryKeyValArr)."') ;";
		return $this->db_db_query($sql);
	}
	public function permanentDeleteDate($tableName, $primaryKeyName, $primaryKeyValArr){
		$sql = "DELETE FROM $tableName WHERE $primaryKeyName IN ('".implode("','", (array)$primaryKeyValArr)."')";
		return $this->db_db_query($sql);
	}
	
	private function getSafeUpateValueList($fieldNameArr, $multiRowValueArr){
		$valueListArr = array();
		foreach((array)$multiRowValueArr as $key => $row){
			$tempString = '';
			$valueListArr[] = $this->getStandardUpdateValue($fieldNameArr, $row);
		}
		return $valueListArr;
	}
	
	private function getStandardUpdateValue($fieldNameArr, $row){
		$numField = count($fieldNameArr);
		if($numField > 0){
    		$count = 0;
			$result = ''; 
			foreach((array)$fieldNameArr as $_key => $_fieldName){
				$count ++;
				$_fieldValue = $this->Get_Safe_Sql_Query($row[$_key]);
				$result .= ' ';
				$result .= " `$_fieldName` = '$_fieldValue' ";
				if($count != $numField){
				    $result .= ", ";
				}
			}
		}
		
		return $result;
	}
	
	public function updateData($tableName, $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField=false){
		$userID = $_SESSION['UserID'];
		$updateSQLArr = $this->getSafeUpateValueList($changefieldNameArr, $multiRowValueArr);
		foreach((array)$updateSQLArr as $updateSQL){
		    if(!$excludeCommonField){    
    		    $sql = "UPDATE $tableName SET $updateSQL,  DateModified = now(), ModifiedBy = '$userID' WHERE IsDeleted = 0 AND $conditions";
		    }
		    else{
		        $sql = "UPDATE $tableName SET $updateSQL WHERE $conditions";
		    }
			if($this->debugMode){
				debug_pr($sql);
			}
			else{
				$result[] = $this->db_db_query($sql);
			}
		}
		if(!in_array(0,$result)){
			return true;
		}
		else{
			return false;			
		}
	}
	
	# Group
	
	# Group > Financial Item
	public function getFinancialItemSQL($parGroupID){
		
		$ItemNameField = Get_Lang_Selection('ItemNameChi as ItemName','ItemName as ItemName');
		
		$sql = " SELECT Code, 
						$ItemNameField, 
						CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ItemID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
				 FROM INTRANET_PCM_GROUP_FINANCIAL_ITEM WHERE isDeleted = 0 AND GroupID = '$parGroupID' ";
		return $sql;
	}
	
	public function getFinancialItem(){
		$sql = " SELECT ItemID, Code, ItemName, ItemNameChi FROM INTRANET_PCM_GROUP_FINANCIAL_ITEM WHERE isDeleted = 0";
		//echo $sql;
		return $this-> returnResultSet($sql);
	}
	public function getFinancialItemByGroup($parGroupID){
		$sql = " SELECT ItemID, Code, ItemName, ItemNameChi FROM INTRANET_PCM_GROUP_FINANCIAL_ITEM WHERE isDeleted = 0 AND GroupID = '$parGroupID' ";
		//echo $sql;
		return $this-> returnResultSet($sql);
	}
	public function getFinancialItemByItemID($parItemID){
		$sql = " SELECT ItemID, Code, ItemName, ItemNameChi FROM INTRANET_PCM_GROUP_FINANCIAL_ITEM WHERE isDeleted = 0 AND ItemID = '$parItemID' ";
		return $this-> returnResultSet($sql);
	}
	public function getFinancialItemCode($itemID){
		$itemAry = $this->getFinancialItemByItemID($itemID);
		return $itemAry[0]['Code'];
	}
	public function getRuleCode($ruleID){
		$sql = "SELECT Code FROM INTRANET_PCM_RULE WHERE RuleID = '$ruleID'";
		$ary = $this->returnResultSet($sql);
		return $ary[0]['Code'];
	}
	
	### Case
	public function getCaseInfo($caseID='',$code='',$keyword='',$isDeleted=0,$currentStage='',$quotationType=''){
		global $ePCMcfg;
		$name_field = getNameFieldByLang2('iu.');
		if($caseID!==''){
			$conds = " AND ipc.CaseID ='$caseID' ";
		}
		if($code!==''){
			$conds .= " AND ipc.Code ='$code' ";
		}
		if($keyword!==''){
			$searchConds = " AND (
								ipc.Code LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
								OR ipc.CaseName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'	
								OR ipc.Description LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'			
								) ";
		}
		if($isDeleted !== ''){
			$conds .= " AND ipc.IsDeleted = $isDeleted ";
		}
		if($currentStage!==''){
			$conds .= " AND ipc.CurrentStage = '$currentStage' ";
// 			debug_pr($conds);
		}
		if(!empty($quotationType)){
			$conds .= " AND rule.QuotationType = '$quotationType' ";
		}
		
		$eraseOtherSql = ' AND ipc.RuleID NOT IN (Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = "O")'; //2016-09-29	villa
		$sql = "SELECT 
					ipc.CaseID,
					ipc.Code,
					ipc.CaseName,
					ipc.RuleID,
					ipc.CategoryID,
					cat.CategoryName,
					cat.CategoryNameChi,
					ipc.Description,
					ipc.ItemID,
					ipf.ItemName,
					ipf.ItemNameChi,
					ipc.AcademicYearID,
					ipc.Budget,
					ipc.ApplicantType,
					ipc.ApplicantUserID,
					$name_field AS ApplicantUserName,
					iu.EnglishName AS ApplicantUserNameEn,
					iu.ChineseName AS ApplicantUserNameCh,
					ipc.ApplicantGroupID,
					ipg.GroupName,
					ipg.GroupNameChi,
					rule.QuotationType,
					ipc.CaseApprovedDate,
					ipc.QuotationStartDate,
					ipc.QuotationEndDate,
					ipc.TenderOpeningStartDate,
					ipc.TenderOpeningEndDate,
					ipc.TenderApprovalStartDate,
					ipc.TenderApprovalEndDate,
					ipc.CurrentStage,
					ipc.PurchaseDate,
					ipc.DateInput,
					ipc.DateModified,
					ipc.InputBy,
					ipc.ModifiedBy,
					ipc.IsDeleted,
					ipc.DeleteReason,
					rule.ApprovalRoleID,
					rule.NeedGroupHeadApprove,
					rule.NeedVicePrincipalApprove,
					rule.MinQuotation,
					rule.QuotationApprovalRoleID,
					rule.TenderAuditingRoleID,
					rule.TenderApprovalRoleID,
					rule.ApprovalMode,
					rule.CaseApprovalRule,
					rule.CustCaseApprovalRule,
					rule.CustApprovalMode
				FROM INTRANET_PCM_CASE AS ipc 
				INNER JOIN INTRANET_PCM_RULE AS rule ON (rule.RuleID=ipc.RuleID)		
				INNER JOIN INTRANET_PCM_CATEGORY AS cat ON (cat.CategoryID=ipc.CategoryID)
				INNER JOIN INTRANET_PCM_GROUP AS ipg ON (ipg.GroupID=ipc.ApplicantGroupID)
				INNER JOIN INTRANET_PCM_GROUP_FINANCIAL_ITEM AS ipf ON (ipf.ItemID=ipc.ItemID)
				INNER JOIN 	".$ePCMcfg['INTRANET_USER']." AS iu	ON (iu.UserID = ipc.ApplicantUserID)
				WHERE 1 $conds $searchConds  $eraseOtherSql
				ORDER BY ipc.CurrentStage ASC, ipc.DateModified DESC	";
		return $this-> returnResultSet($sql);
	}
	public function getCaseUserRole(){
	    global $sys_custom;
		$userID = $_SESSION['UserID'];
		if($sys_custom['ePCM']['extraApprovalFlow']||$sys_custom['ePCM_skipCaseApproval']){
            $isCustEndorsement = ',IF(ipcca.ApproverUserID >= 1 , 1,0) as isCustApprover';
		    $custApprovalTable =  "LEFT JOIN INTRANET_PCM_CUST_CASE_APPROVAL as ipcca ON ipcca.CaseID = ipc.CaseID AND ipcca.ApproverUserID = $userID";
        }
		$sql = "SELECT 
				ipc.CaseID,
				IF(case2.ApplicantUserID > 0,1,0) as isApplicant,
				IF(ipgm.GroupMemberID > 0 , 1,0) as isGroupMember,
				IF(ipca.ApproverUserID >= 1 , 1,0) as isApprover,
				IF(role1.RoleUserID > 0 , 1,0) as isTenderAuditing,
				IF(role2.RoleUserID > 0 , 1,0) as isTenderApproval,
				IF(role3.RoleUserID > 0 , 1,0) as isQuotationApproval
				$isCustEndorsement
				FROM INTRANET_PCM_CASE as ipc
				LEFT JOIN INTRANET_PCM_GROUP_MEMBER as ipgm ON ipc.ApplicantGroupID = ipgm.GroupID  AND ipgm.UserID = '$userID'
				LEFT JOIN INTRANET_PCM_CASE_APPROVAL as ipca ON ipca.CaseID = ipc.CaseID AND ipca.ApproverUserID = '$userID'
				$custApprovalTable
				INNER JOIN INTRANET_PCM_RULE AS rule ON (rule.RuleID=ipc.RuleID)
				LEFT JOIN INTRANET_PCM_ROLE_USER as role1 ON rule.TenderAuditingRoleID= role1.RoleID AND role1.UserID = '$userID'
				LEFT JOIN INTRANET_PCM_ROLE_USER as role2 ON rule.TenderApprovalRoleID= role2.RoleID AND role2.UserID= '$userID'
				LEFT JOIN INTRANET_PCM_ROLE_USER as role3 ON rule.QuotationApprovalRoleID= role3.RoleID AND role3.UserID= '$userID'
				LEFT JOIN INTRANET_PCM_CASE as case2 ON case2.CaseID = ipc.CaseID AND case2.ApplicantUserID = '$userID'
				WHERE ipc.IsDeleted = 0;";
		return $this-> returnResultSet($sql);
	}
	public function getCaseCode($caseID){
		$sql = "SELECT Code FROM INTRANET_PCM_CASE WHERE CaseID = '$caseID'";
		$return = $this-> returnResultSet($sql);
		return $return[0]['Code'];
	}
	public function getCaseOwnerUserID($caseID){
		$sql = "SELECT ApplicantUserID FROM INTRANET_PCM_CASE WHERE CaseID = '$caseID'";
		$return = $this-> returnResultSet($sql);
		return $return[0]['ApplicantUserID'];
	}
	public function getCaseApproval($caseID='',$approverUserID='',$approverRoleID='',$sorting='', $userType=''){
		global $ePCMcfg, $_PAGE, $junior_mck;

		$name_field = getNameFieldByLang2('iu.');
		$name_field2 = getNameFieldByLang2('iu2.');
        $name_field3 = getNameFieldByLang2('iu3.');
        $archivedNameField = getNameFieldByLang2('iau.');
        $archivedNameField2 = getNameFieldByLang2('iau2.');

		if($caseID!==''){
			$conds = " AND CaseID ='$caseID' ";
		}
		if($approverUserID!==''){
			$conds .= " AND ApproverUserID = '$approverUserID' ";
		}
		if($approverRoleID!==''){
			$conds .= " AND ApprovalRoleID = '$approverRoleID' ";
		}
		if($sorting !== ''){
			$sorting = " ORDER BY $sorting ";
		}
		if($userType !== ''){
            $conds .= " AND iu.RecordType = '$userType'";
        }

		$sql = "SELECT 
					ipca.CaseID, ipca.ApproverUserID, IFNULL($archivedNameField,IFNULL($name_field, $name_field3)) AS ApproverName, 
					CASE WHEN iu3.UserID IS NULL
                           THEN 1
                           ELSE 0
                    END AS IsDeletedApprover,
					ipca.Feedback, ipca.RecordStatus, ipca.ApprovalType, ipca.DateInput, ipca.DateModified, ipca.InputBy, ipca.ModifiedBy, IFNULL($archivedNameField2,$name_field2) as ModifiedByName, ipca.IsDeleted, ipca.ApprovalRoleID, ipca.ApprovalSequence 
				FROM INTRANET_PCM_CASE_APPROVAL AS ipca
				LEFT JOIN 	".$ePCMcfg['INTRANET_USER']." AS iu	ON (iu.UserID = ipca.ApproverUserID)
				LEFT JOIN 	INTRANET_ARCHIVE_USER AS iau	ON (iau.UserID = ipca.ApproverUserID)
				LEFT JOIN 	".$ePCMcfg['INTRANET_USER']." AS iu2 ON (iu2.UserID = ipca.ModifiedBy)
				LEFT JOIN 	INTRANET_ARCHIVE_USER AS iau2 ON (iau2.UserID = ipca.ModifiedBy)
				LEFT JOIN   INTRANET_USER AS iu3 ON (iu3.UserID = ipca.ApproverUserID)
				WHERE IsDeleted = 0 $conds
				$sorting
				";
		$resultAry = $this-> returnResultSet($sql);
        if(isset($junior_mck)) {
            foreach ($resultAry AS $i => $ApproverDetails) {
                if($ApproverDetails['IsDeletedApprover']) {
                    $resultAry[$i]['ApproverName'] = convert2unicode(trim($ApproverDetails['ApproverName']), 1);
                }
            }
        }
		return $resultAry;
	}
	public function getCustCaseApproval($caseID='',$approverUserID='',$approverRoleID='',$sorting=''){
		global $ePCMcfg, $_PAGE;
        $name_field = getNameFieldByLang2('iu.');
        $name_field2 = getNameFieldByLang2('iu2.');
        $name_field3 = getNameFieldByLang2('iu3.');
        $archivedNameField = getNameFieldByLang2('iau.');
        $archivedNameField2 = getNameFieldByLang2('iau2.');
		if($caseID!==''){
			$conds = " AND CaseID ='$caseID' ";
		}
		if($approverUserID!==''){
			$conds .= " AND ApproverUserID = '$approverUserID' ";
		}
		if($approverRoleID!==''){
			$conds .= " AND ApprovalRoleID = '$approverRoleID' ";
		}
		if($sorting !== ''){
			$sorting = " ORDER BY $sorting ";
		}
        $sql = "SELECT 
					ipca.CaseID, ipca.ApproverUserID, IFNULL($archivedNameField,IFNULL($name_field, $name_field3)) AS ApproverName, 
					CASE WHEN iu3.UserID IS NULL
                           THEN 1
                           ELSE 0
                    END AS IsDeletedApprover,
                    ipca.Feedback, ipca.RecordStatus, ipca.ApprovalType, ipca.DateInput, ipca.DateModified, ipca.InputBy, ipca.ModifiedBy, IFNULL($archivedNameField2,$name_field2) as ModifiedByName, ipca.IsDeleted, ipca.ApprovalRoleID, ipca.ApprovalSequence 
				FROM INTRANET_PCM_CUST_CASE_APPROVAL AS ipca
				LEFT JOIN 	".$ePCMcfg['INTRANET_USER']." AS iu	ON (iu.UserID = ipca.ApproverUserID)
				LEFT JOIN 	INTRANET_ARCHIVE_USER AS iau	ON (iau.UserID = ipca.ApproverUserID)
				LEFT JOIN 	".$ePCMcfg['INTRANET_USER']." AS iu2 ON (iu2.UserID = ipca.ModifiedBy)
				LEFT JOIN 	INTRANET_ARCHIVE_USER AS iau2 ON (iau2.UserID = ipca.ModifiedBy)
				LEFT JOIN   INTRANET_USER AS iu3 ON (iu3.UserID = ipca.ApproverUserID)
				WHERE IsDeleted = 0 $conds
				$sorting
				";
        $resultAry = $this-> returnResultSet($sql);
        if($_PAGE['libPCM']->isEJ()) {
            foreach ($resultAry AS $i => $ApproverDetails) {
                if($ApproverDetails['IsDeletedApprover']) {
                    $resultAry[$i]['ApproverName'] = convert2unicode(trim($ApproverDetails['ApproverName']), 1);
                }
            }
        }
        return ($resultAry);
	}
	public function countOfRejectedApproval($caseID){
		$sql = "SELECT COUNT(*) AS numofrejected FROM INTRANET_PCM_CASE_APPROVAL WHERE IsDeleted = 0 AND CaseID = '$caseID' AND RecordStatus = '-1' GROUP BY RecordStatus";
		$resultSet = $this->returnResultSet($sql);
		return intval($resultSet[0]['numofrejected']);
	}

	### Case Notification
	public function getPushNotificationSchedule($caseID,$stageID=''){
		if($stageID!==''){
			$conds = " AND StageID = '$stageID' ";
		}
		$sql = "SELECT * FROM INTRANET_PCM_NOTIFICATION_SCHEDULE WHERE CaseID = '$caseID' $conds AND IsDeleted = '0'";
		$resultSet = $this->returnResultSet($sql);
		return $resultSet;
	}
	public function deletePushNotificationSchedule($caseID,$stageID){
		$sql = "DELETE FROM INTRANET_PCM_NOTIFICATION_SCHEDULE WHERE CaseID = '$caseID'  AND StageID = '$stageID' ";
		return $this->db_db_query($sql);
	}
	public function getNotificationScheduleUser($caseID,$stageID=''){
		global $ePCMcfg;
		$name_field = getNameFieldByLang2('iu.');
		if($stageID!==''){
			$cond = " AND StageID = '$stageID'";
		}
		$sql = "SELECT CaseID,StageID,ipnsu.UserID,$name_field AS Name FROM INTRANET_PCM_NOTIFICATION_SCHEDULE_USER AS ipnsu
			INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS iu ON(ipnsu.UserID=iu.UserID)
		WHERE CaseID = '$caseID' $cond";
		return $this->returnResultSet($sql);
	}
	public function addNotificationScheduleUser($caseID,$stageID,$userID){
		$sql = "INSERT INTO INTRANET_PCM_NOTIFICATION_SCHEDULE_USER 
				(CaseID, StageID, UserID)
				VALUES
				('$caseID','$stageID','$userID')";
		return $this->db_db_query($sql);
	}
	#### supplier
	
	public function getSupplierType() {
		$return = array ();

		$sql = 'Select `TypeID`,`TypeName`,`TypeNameChi` From `INTRANET_PCM_SUPPLY_TYPE` WHERE `IsDeleted`=0;';
		$result = $this->returnResultSet($sql);
		if (count($result) > 0) {
			foreach ($result as $row) {
				$return[$row['TypeID']] = $row[Get_Lang_Selection('TypeNameChi','TypeName')];
			}
		}

		return $return;
	}

	public function getSupplierInfo($supplierIDAry='',$isDeleted='',$isActive='') {
		$record_array = array ();

		if($supplierIDAry!==''){
			$supplierIDAry = (array) $supplierIDAry;
			$supplierIDCond = implode("','", $supplierIDAry);
			$cond = " AND `SupplierID` IN ('$supplierIDCond') ";
		}
		if($isDeleted!==''){
			$cond .= " AND IsDeleted = $isDeleted ";
		}
		if($isActive!==''){
			$isActiveCond = " AND IsActive = $isActive";
		}
		
		$sql = "Select * From `INTRANET_PCM_SUPPLIER` WHERE 1 $cond $isActiveCond";
		$record_array['MAIN'] = $this->returnResultSet($sql);

		$sql = 'Select * From `INTRANET_PCM_SUPPLIER_TYPE_MAPPING` WHERE 1 '.$cond;
		$record_array['TYPE_MAPPING'] = $this->returnResultSet($sql);

		$sql = 'Select * From `INTRANET_PCM_SUPPLIER_CONTACT` WHERE 1 '.$cond.' AND `IsDeleted`=\'0\';';
		$record_array['CONTACT'] = $this->returnResultSet($sql);

		return $record_array;
	}
	public function getSupplierContactInfo($supplier_contact_id){
		$supplier_contact_id=(array)$supplier_contact_id;
		$sql_supplier_contact_id = implode("','", $supplier_contact_id);
		
		
		$sql = 'Select * From `INTRANET_PCM_SUPPLIER_CONTACT` WHERE `ContactID` IN (\''.$sql_supplier_contact_id.'\') AND `IsDeleted`=\'0\';';
		return $this->returnResultSet($sql);
	}
	public function getSupplierInfoTypeID($typeID=''){
		if($typeID!==''){
			$cond = " AND ipsm.TypeID = '$typeID' ";
		}
		$sql = "SELECT * 
				FROM INTRANET_PCM_SUPPLIER AS ips
				INNER JOIN INTRANET_PCM_SUPPLIER_TYPE_MAPPING AS ipsm ON (ips.SupplierID=ipsm.SupplierID)
				WHERE ips.IsDeleted = 0 AND ips.IsActive = 1  $cond	
				ORDER BY SupplierName";
		return $this->returnResultSet($sql);		
	}

	public function getSupplierTypeInfo($supplierTypeIDAry='',$isDeleted='') {
		if($supplierTypeIDAry!==''){
			$supplierTypeIDAry = (array) $supplierTypeIDAry;
			$supplierTypeIDCond = implode("','", $supplierTypeIDAry);
			$cond = " AND `TypeID` IN ('$supplierTypeIDCond') ";
		}
		if($isDeleted!==''){
			$cond .= " AND IsDeleted = $isDeleted ";
		}
		
		$sql = "Select * From `INTRANET_PCM_SUPPLY_TYPE` WHERE 1 $cond";
		return $this->returnResultSet($sql);
	}

	#### Group > Budget

	public function getGroupBudget($array_id, $academicYearID='') {
		$array_id = (array) $array_id;
		$sql_ids = implode("','", $array_id);

		if($academicYearID == ''){
            $settings = $this->getGeneralSettings();
            $academicYearID = $settings['defaultAcademicYearID'];
        }
        $year_conds = " and AcademicYearID = '$academicYearID'";
        $sql = "SELECT * FROM INTRANET_PCM_GROUP_BUDGET WHERE GroupID in ('$sql_ids') $year_conds AND isDeleted = 0";

		return $this->returnResultSet($sql);
	}

	public function getGroupBudgetByGroupIdAndAcademicYearId($group_id,$academic_year_id='') {
		
		if($academic_year_id===''){
			$settings = $this->getGeneralSettings();
			$academic_year_id = $settings['defaultAcademicYearID'];
		}
		$sql = "SELECT * FROM INTRANET_PCM_GROUP_BUDGET WHERE GroupID = '$group_id' AND AcademicYearID = '$academic_year_id' AND IsDeleted = 0";
		return $this->returnResultSet($sql);
	}

	public function getGroupFinancialItemBudget($array_id) {
		$array_id = (array) $array_id;
		$sql_ids = implode("','", $array_id);
		$sql = 'SELECT * FROM `INTRANET_PCM_FINANCIAL_ITEM_BUDGET` WHERE `ItemBudgetID` in (\'' . $sql_ids . '\');';
		return $this->returnResultSet($sql);
	}

	public function getGroupFinancialItemBudgetByGroupId($group_id, $academic_year_id, $term_id) {

		$sql = 'SELECT * FROM `INTRANET_PCM_FINANCIAL_ITEM_BUDGET` WHERE `ItemID` in (SELECT `ItemID` FROM `INTRANET_PCM_GROUP_FINANCIAL_ITEM` WHERE `GroupID`=\'' . $group_id . '\') AND `AcademicYearID`=\'' . $academic_year_id . '\'' . ($term_id == '' ? '' : (' AND `TermID`=\'' . $term_id . '\'')) . ';';
		return $this->returnResultSet($sql);
	}
	
	#### Case Number Generation
		
	public function getNextCaseNumber($array_code, $sequenceBaseItemsAry){
		$sequence_format=$this->getSequenceFormat();
		//Code Replacement
		$replaced_format=$sequence_format['format'];
		foreach((array)$array_code as $key=>$code){
			$replaced_format=str_replace($key.';',$code,$replaced_format);
		}

		//Function Replacement
		$array_function=array(
			0=>'FUNCTION::CalendarYear',
			1=>'FUNCTION::AcademicYear',
		);

		foreach($array_function as $function_name){
			if(strpos($replaced_format,$function_name)!==false){
				$value='';
				switch($function_name){
					case $array_function[0]:
						$value=$this->getCalendarYear();
						break;
					case $array_function[1]:
						$value=$this->getAcademicYear();
						break;
				}
				$replaced_format=str_replace($function_name.';',$value,$replaced_format);
			}
		}
		//Sequence Replacement
		$no_of_digit=$sequence_format['no_of_digit'];
        $SequenceNumberBy = $sequence_format['sequencedBy'];
        if($SequenceNumberBy == "AcademicYear"){
            $array_code['FUNCTION::AcademicYear'] = $value;
        }

		if($SequenceNumberBy=='General'){
		    $str_sequence=$this->getNextSequenceNumber($replaced_format,$no_of_digit);
        }else{
            $str_sequence=$this->getNextSequenceNumber($replaced_format,$no_of_digit,$SequenceNumberBy,$sequenceBaseItemsAry, $sequence_format['format']);
        }
		$replaced_format=str_replace('FUNCTION::Sequence;',$str_sequence,$replaced_format);
		return strtoupper($replaced_format);
	}
	
	public function getSequenceFormat(){
		$array_setting=$this->getGeneralSettings();
		return array(
			'format'=>$array_setting['caseCodeGeneration'],
			'sequencedBy'=>$array_setting['caseCodeGenerationSequence'],
			'no_of_digit'=>$array_setting['caseCodeNumberOfDigit'],
		);
	}
	
	public function getCalendarYear(){
		return date('Y');
	}
	
	public function getAcademicYear(){
		$array_setting=$this->getGeneralSettings();
		$period=getPeriodOfAcademicYear($array_setting['defaultAcademicYearID']);
		$year=substr($period['StartDate'],2,2).substr($period['EndDate'],2,2);
		return $year;
	}

    public function getCaseCodeSequenceNumber($caseCode, $sequence_format){
        $sql ='Select C.`CategoryID`, C.`AcademicYearID`, C.`ApplicantGroupID`, C.`ItemID`, R.`Code` From `INTRANET_PCM_CASE` AS C JOIN `INTRANET_PCM_RULE` AS R ON C.`RuleID` = R.`RuleID` WHERE C.`Code` = "'.$caseCode.'";';
        $sequenceItemAry = $this->returnResultSet($sql);
        $sequence_formatAry = array_filter(explode(';',$sequence_format));
        foreach($sequence_formatAry as $format){
            $numberOfInstances = 1;
            switch ($format){
                case 'CODE::Category':
                    $CategoryCode = $this->getCategoryCode($sequenceItemAry[0]['CategoryID']);
                    $caseCode = preg_replace('/'.strtoupper($CategoryCode).'/', '', $caseCode, $numberOfInstances);
                    break;
                case 'FUNCTION::AcademicYear':
                    $period=getPeriodOfAcademicYear($sequenceItemAry[0]['AcademicYearID']);
                    $year=substr($period['StartDate'],2,2).substr($period['EndDate'],2,2);
                    $caseCode = preg_replace('/'.$year.'/', '', $caseCode, $numberOfInstances);
                    break;
                case 'CODE::GroupCode':
                    $GroupCode = $this->getGroupCode($sequenceItemAry[0]['ApplicantGroupID']);
                    $caseCode = preg_replace('/'.strtoupper($GroupCode).'/', '', $caseCode, $numberOfInstances);
                    break;
                case 'CODE::GroupItem':
                    $FinancialItemCode = $this->getFinancialItemCode($sequenceItemAry[0]['ItemID']);
                    $caseCode = preg_replace('/'.strtoupper($FinancialItemCode).'/', '', $caseCode, $numberOfInstances);
                    break;
                case 'CODE::Rule':
                    $RuleCode = $sequenceItemAry[0]['Code'];
                    $caseCode = preg_replace('/'.strtoupper($RuleCode).'/', '', $caseCode, $numberOfInstances);
                    break;
            }
        }
        return $caseCode;
    }

    public function getNextSequenceNumber($prefix_with_function_name,$number_of_digit=6,$SequencedBy="",$sequenceBaseItemsAry=array(), $sequence_format='' ){
       if($SequencedBy!=""&&$SequencedBy!='General'&&isset($SequencedBy)){
            if ($SequencedBy == "CategoryCode") {
                $idStr = 'C.`CategoryID`';
            } elseif ($SequencedBy == "AcademicYear") {
                $idStr = 'C.`AcademicYearID`';
            } elseif ($SequencedBy == "GroupCode") {
                $idStr = 'C.`ApplicantGroupID`';
            } elseif ($SequencedBy == "FinancialItemCode") {
                $idStr = 'C.`ItemID`';
            } elseif ($SequencedBy == "RuleCode") {
                $idStr = 'R.`Code`';
            }
            $SqlsequencedBy = "$idStr = '$sequenceBaseItemsAry[$SequencedBy]'";
            $sql='Select C.`Code` COLLATE utf8_general_ci as `code` From `INTRANET_PCM_CASE` AS C JOIN `INTRANET_PCM_RULE` AS R ON C.`RuleID` = R.`RuleID` WHERE '.$SqlsequencedBy.';';
            $SequencedBy_AllSequenceNum = $this->returnResultSet($sql);
            $maxNum = 0;
            foreach($SequencedBy_AllSequenceNum as $SequencedBy_AllSequenceNumKey) {
                $caseCode = $SequencedBy_AllSequenceNumKey['code'];
                $sequenceNumber =$this->getCaseCodeSequenceNumber($caseCode, $sequence_format);
                is_numeric($sequenceNumber)? $sequenceNumberAry[] = $sequenceNumber : '' ;
            }
           if(count($sequenceNumberAry)>0){
               $maxNum = max($sequenceNumberAry);
               $new_sequence = $maxNum+1;
           } else{
               $new_sequence=1;
           }
        }else {
            $str_sql_like = str_replace('FUNCTION::Sequence;', '([0-9]{' . $number_of_digit . '})',
                $prefix_with_function_name);

            $sql = 'Select MAX(C.`Code` COLLATE utf8_general_ci) as `max_code` From `INTRANET_PCM_CASE` AS C JOIN `INTRANET_PCM_RULE` AS R ON C.`RuleID` = R.`RuleID` WHERE C.`Code` rlike \'^' . $str_sql_like . '$\';';

            $rows_case = $this->returnResultSet($sql);

            $latest_code='';
            if(count($rows_case)>0){
                $row_case=$rows_case[0];
                $latest_code=$row_case['max_code'];
            }

            if($latest_code==''){
                $new_sequence=1;
            }else{
                    $str_regexp = '/^' . str_replace(preg_quote('FUNCTION::Sequence;'), '(\d{' . $number_of_digit . '})',
                            preg_quote($prefix_with_function_name)) . '$/';
                preg_match($str_regexp.'i',$latest_code,$array_match);
                $new_sequence=$array_match[1]+1;
            }
        }
        return sprintf('%0'.$number_of_digit.'d',$new_sequence);
    }
	
	### Log Related 
	public function addLog($Type, $caseID, $logDetails){
		$fieldNameArr = array('Type ','caseID ','Description','LogBy');
		$multiRowValueArr = array();
		$multiRowValueArr[0] = array($Type, $caseID, $logDetails, $_SESSION['UserID']);
		$result = $this->insertData($tableName='INTRANET_PCM_LOG', $fieldNameArr, $multiRowValueArr, true);
	}
	
	public function getLog($Type, $caseID){
		$sql = "SELECT
					*
				FROM
					INTRANET_PCM_LOG
				WHERE
					Type = '".$Type."'
					AND caseID = '".$caseID."' ";
		return $this->returnResultSet($sql);
	}
	
	### Attachment Related
	public function getQuoAttachment($QuoIDArr, $returnAssoArr=false){
	    $sql = "SELECT
            	    ipa.AttachmentID,
            	    ipa.FileName,
            	    ipa.FileSize,
	                ipqa.QuotationID
        	    FROM
            	    INTRANET_PCM_ATTACHMENT as ipa
            	    INNER JOIN INTRANET_PCM_QUOTATION_ATTACHMENT as ipqa on (ipa.AttachmentID = ipqa.AttachmentID)
        	    WHERE
            	    ipqa.QuotationID IN ('".implode("','", (array)$QuoIDArr)."')
            	    AND ipa.isDeleted = 0 ";
	    if($returnAssoArr == true){
	        $result = $this->returnResultSet($sql);
	        foreach((array)$result as $_infoAry){
	            $_quoID = $_infoAry['QuotationID'];
	            $assocAry[$_quoID][] = $_infoAry;
	        }
	        return $assocAry;
	    }
	    else{
    	    return $this->returnResultSet($sql);
	    }
	}

    public function getRuleAttachment($ruleID, $AssocWithStage=false, $type='0'){
        if($ruleID !==''){
            $stage_conds = " AND ipca.RuleID = '$ruleID' ";
        }
        $sql = "SELECT
            	    ipa.AttachmentID,
            	    ipa.FileName,
            	    ipa.FileSize,
	                ipca.StageID
        	    FROM
            	    INTRANET_PCM_ATTACHMENT as ipa
            	    INNER JOIN INTRANET_PCM_CASE_ATTACHMENT as ipca on (ipa.AttachmentID = ipca.AttachmentID)
        	    WHERE
            	    ipa.isDeleted = 0
            	    AND ipca.Type = '$type'  
            	    $stage_conds";
        $result = $this->returnResultSet($sql);
        if($AssocWithStage){
            foreach((array)$result as $_infoAry){
                $_stageID = $_infoAry['StageID'];
                $assocAry[$_stageID][] = $_infoAry;
            }
            return $assocAry;
        }
        else{
            return $this->returnResultSet($sql);
        }
    }
	
	public function getCaseStageAttachment($CaseID, $Stage='', $AssocWithStage=false, $type='0', $ruleID='', $getAttachWithoutRuleIDAndRuleID = false){
            if($Stage !==''){
                $stage_conds = " AND ipca.StageID = '$Stage' ";
            }
            if($ruleID==''){
                $rule_conds = " AND ipca.RuleID = 0 ";
            } elseif($getAttachWithoutRuleIDAndRuleID){
                $rule_conds = " AND ipca.RuleID IN('$ruleID', 0) ";
            }else{
                $rule_conds = " AND ipca.RuleID = '$ruleID' ";
            }
            $sql = "SELECT
	               ipa.AttachmentID,
	               ipa.FileName,
	               ipa.FileSize,
	               ipca.StageID
	            FROM 
    	           INTRANET_PCM_ATTACHMENT as ipa 
    	           INNER JOIN INTRANET_PCM_CASE_ATTACHMENT as ipca on (ipa.AttachmentID = ipca.AttachmentID)
	            WHERE
	               ipca.CaseID IN (-1, $CaseID)
	               $stage_conds 
	               $rule_conds
	               AND ipa.isDeleted = 0 
	    		   AND ipca.Type = '$type' ";
            $result = $this->returnResultSet($sql);
	        
	    if($AssocWithStage){
	        foreach((array)$result as $_infoAry){
	            $_stageID = $_infoAry['StageID'];
	            $assocAry[$_stageID][] = $_infoAry;
	        }
	        return $assocAry;        
	    }
	    else{
    	    return $this->returnResultSet($sql);
	    }
	}

    public function getCaseAttachment($CaseID, $type='0', $ruleID='', $getAttachWithoutRuleIDAndRuleID = false)
    {
        if ($ruleID == '') {
            $rule_conds = " AND ipca.RuleID = 0 ";
        } elseif ($getAttachWithoutRuleIDAndRuleID) {
            $rule_conds = " AND ipca.RuleID IN('$ruleID', 0) ";
        } else {
            $rule_conds = " AND ipca.RuleID = '$ruleID' ";
        }
        $sql = "SELECT
	               *
	            FROM 
    	           INTRANET_PCM_CASE_ATTACHMENT as ipca
	            WHERE
	               ipca.CaseID = '$CaseID' 
	               $rule_conds
	               AND ipca.isDeleted = 0 
	               
	    		   AND ipca.Type = '$type' ";
        return $this->returnResultSet($sql);
    }
	
	public function getUserSessionFileList($session, $ruleID='', $stageID=''){
		global $ePCMcfg;
        $cond='';
        if($ruleID!=''){
            $cond .= 'AND ipa.RuleID ='.$ruleID;
        } else {
            $cond .= 'AND ipa.RuleID =0';
        }
        if($stageID!=''){
            $cond.=' AND ipa.StageID ='.$stageID;
        }

		$name_field = getNameFieldByLang2('iu.');
		$sql = "SELECT
					ipa.AttachmentID,
					ipa.FileName,
		            ipa.FileSize,
	                IF(ipa.CaseID = 0, '1', '0') as isTempFile,
	                ipa.WillDelete,
	    			ipa.Owner,
	    			$name_field AS OwnerName,
					ipa.DateInput
				FROM INTRANET_PCM_ATTACHMENT AS ipa
	    		LEFT JOIN ".$ePCMcfg['INTRANET_USER']." AS iu ON (iu.UserID=ipa.Owner)
				WHERE
					UploadBy = '".$_SESSION['UserID']."' 
				    AND Session = '".$session."'
			        AND isDeleted = 0
			        $cond";
	    return $this->returnResultSet($sql);
	}

    public function getTempFileList($ruleID='', $session='', $AssocWithStage=false, $willdeleted=false){
        global $ePCMcfg;
        $cond='';
        if($ruleID!=''){
            $cond .= ' AND ipa.RuleID ='.$ruleID;
        } else {
            $cond .= ' AND ipa.RuleID =0';
        }
        if($session!=''){
            $cond.=' AND ipa.Session ='.$session;
        }
        if($willdeleted==false){
            $willdeleted = 'ipa.WillDelete = 0';
        } else{
            $willdeleted = 'ipa.WillDelete = 1';
        }

        $sql = "SELECT
					ipa.AttachmentID,
					ipa.FileName,
		            ipa.FileSize,
					ipa.StageID
				FROM INTRANET_PCM_ATTACHMENT AS ipa
	    		LEFT JOIN ".$ePCMcfg['INTRANET_USER']." AS iu ON (iu.UserID=ipa.Owner)
				WHERE
				    $willdeleted
			        AND isDeleted = 0 
			        $cond";
        $result = $this->returnResultSet($sql);
        if($AssocWithStage){
            foreach((array)$result as $_infoAry){
                $_stageID = $_infoAry['StageID'];
                $assocAry[$_stageID][] = $_infoAry;
            }
            return $assocAry;
        }
        else{
            return $this->returnResultSet($sql);
        }

    }
	
	public function getFileById($Id, $conds=''){
	    if(is_array($Id)){
	        $attachmentID_conds = " AttachmentID IN ('".implode("','", $Id)."') ";
	    }
	    else{
	        $attachmentID_conds = " AttachmentID = '".$Id."' ";
	    }
	    $sql = "SELECT
					AttachmentID,
					FileName,
					FilePath,
		            Checksum,
		            CaseID
				FROM INTRANET_PCM_ATTACHMENT
				WHERE
					$attachmentID_conds
					AND isDeleted = 0 
				    $conds
					";
	    return $this->returnResultSet($sql);
	}
	
	###History
	public function getOriginalCaseID($caseID){
		$sql = "SELECT OriginalCaseID FROM INTRANET_PCM_CASE_HISTORY WHERE CaseID = '$caseID' AND IsDeleted = 0";
		$return = $this->returnResultSet($sql);
		$originalID = $return[0]['OriginalCaseID'];
		
		//If no original case id => caseID is orignal case id
		if($originalID==''){
			$originalID = $caseID;
		}
		return $originalID;
	}
	public function getLastestVersion($originalCaseID){
		$sql = "SELECT Version FROM INTRANET_PCM_CASE_HISTORY WHERE OriginalCaseID = '$originalCaseID'  AND IsDeleted = 0 ORDER BY Version DESC";
		$return = $this->returnResultSet($sql);
		$version = intval($return[0]['Version']);
		if($version==''){
			$version = 0;
		}
		return $version; //First row version is greatest
	}
	public function getAllHistoryCaseID($caseID){ //canbe current or original
		$sql = "Select CaseID,Version 
				From INTRANET_PCM_CASE_HISTORY 
				WHERE OriginalCaseID = 
					(SELECT OriginalCaseID 
					FROM INTRANET_PCM_CASE_HISTORY 
					WHERE CaseID = '$caseID') 
				ORDER BY Version";
		return $this->returnResultSet($sql);
	}
	public function getIntranetCategoryInfo(){
		$sql = "SELECT GroupCategoryID, CategoryName FROM INTRANET_GROUP_CATEGORY
                        Where GroupCategoryID not IN (0,3,4,5) ORDER BY CategoryName";
		return $this->returnResultSet($sql);
	}
	public function getIntranetGroupInfo($groupCatIdArr){
		$sql = "SELECT GroupID, Title as TitleEn, TitleChinese, RecordType as GroupCategoryID  FROM INTRANET_GROUP WHERE RecordType IN ('".implode("','", $groupCatIdArr)."') AND AcademicYearID = '".Get_Current_Academic_Year_ID()."' ORDER BY RecordType,Title";
		return $this->returnResultSet($sql);
	}
	
	### check duplicate
	
	public function checkDuplicate($table,$field,$value,$excluded_value,$having_is_deleted=true,$allow_time_of_duplication=0){
		$sql='SELECT count(1) as `count` FROM `'.$table.'` WHERE `'.$field.'`=\''.$value.'\' AND `'.$field.'`<>\''.$excluded_value.'\''.($having_is_deleted?' AND `IsDeleted`=\'0\'':'');
		$result=$this->returnResultSet($sql);
		return $result[0]['count']>$allow_time_of_duplication?false:true;
	}
	
	### Token
	
	public function getToken($quotation_id,$token_id=''){
		
		#####################################
		#	Get token valid date
		#	- Assume token valid for 28 days
		#	- Assume End time is 23:59:59
		####################################
		$validDays = 28;
		$endDateEnable = 1;
		$endDate = date('Y-m-d',strtotime("+".$validDays." days")).' 23:59:59';
		
		
		if($token_id==''){
			$sql='SELECT `TokenID`,`Token` FROM `INTRANET_PCM_QUOTATION_TOKEN` WHERE `QuotationID`=\''.$quotation_id.'\' AND `IsDeleted`=\'0\'';
			$rows_token=$this->returnResultSet($sql);
			if(count($rows_token)>0){
				$row_token=$rows_token[0];
				$token_id = $row_token['TokenID'];
				$token =  $row_token['Token'];
				
				//Update Token EndDate
				$changefieldNameArr = array('EndDateEnable','EndDate');
				$multiRowValueArr = array(array($endDateEnable,$endDate));
				$conditions = "TokenID = '$token_id'";
				$this->updateData('INTRANET_PCM_QUOTATION_TOKEN', $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField=false);
				
				return $token;
			}else{
				$rows_quotation=$this->getCaseQuotation('',$quotation_id);
				$row_quotation=$rows_quotation[0];
				$case_id=$row_quotation['CaseID'];
				$supplier_id=$row_quotation['SupplierID'];
				
				$token=sha1(date('YmdHis').sprintf('%011d',$quotation_id).sprintf('%011d',$case_id).sprintf('%011d',$supplier_id));
				
				$table_name = 'INTRANET_PCM_QUOTATION_TOKEN';
				$array_field_name = array('Token','QuotationID','Status','EndDateEnable','EndDate');
				$rows=array(
					array($token,$quotation_id,'0',$endDateEnable,$endDate),
				);
				
				$this->insertData($table_name, $array_field_name, $rows);
				
				return $token;
			}
		}else{
			$sql='SELECT `Token` FROM `INTRANET_PCM_QUOTATION_TOKEN` WHERE `TokenID`=\''.$token_id.'\' AND `IsDeleted`=\'0\'';
			$rows_token=$this->returnResultSet($sql);
			$row_token=$rows_token[0];
			$token = $row_token['Token'];
			
			//Update Token EndDate
			$changefieldNameArr = array('EndDateEnable','EndDate');
			$multiRowValueArr = array(array($endDateEnable,$endDate));
			$conditions = "TokenID = '$token_id'";
			$this->updateData('INTRANET_PCM_QUOTATION_TOKEN', $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField=false);
			
			return $token;
		}
	}
	public function getTokenEndDate($token){
		$sql='SELECT `EndDateEnable`, `EndDate` FROM `INTRANET_PCM_QUOTATION_TOKEN` WHERE `Token`=\''.$token.'\' AND `IsDeleted`=\'0\'';
		$rows_token=$this->returnResultSet($sql);
		$row_token=$rows_token[0];
		$enabled = $row_token['EndDateEnable'];
		$endDate = $row_token['EndDate'];
		if($enabled){
			return $endDate;
		}else{
			return null;
		}
	}
	public function getQuotationIdByToken($token){
		$sql='SELECT * FROM `INTRANET_PCM_QUOTATION_TOKEN` WHERE SHA1(`Token`)=\''.sha1($token).'\' AND `IsDeleted`=\'0\'';
		$rows_token=$this->returnResultSet($sql);
		if(count($rows_token)>0){
			$row_token=$rows_token[0];
			return $row_token['QuotationID'];
		}else{
			return false;
		}
	}
	public function getAndUpdateQuotationByToken($token){
		
		$sql='SELECT * FROM `INTRANET_PCM_QUOTATION_TOKEN` WHERE SHA1(`Token`)=\''.sha1($token).'\' AND `Status`=\'0\' AND `IsDeleted`=\'0\'';
		$rows_token=$this->returnResultSet($sql);
		if(count($rows_token)>0){
			$row_token=$rows_token[0];
			
			if($row_token['EndDateEnable']){
				$endTime = strtotime($row_token['EndDate']);
				$now = strtotime(date('Y-m-d h:i:s'));
				if($now>$endTime){
					//invalid, expired
					return 'EXPIRED';
				}
			}
			//update token
			$table_name = 'INTRANET_PCM_QUOTATION_TOKEN';
			$array_field_name = array('Status');
			$rows=array(
				array('1'),
			);
			$conditions = ' `TokenID` = \''.$row_token['TokenID'].'\'';
			
			$this->updateData($table_name, $array_field_name, $rows,$conditions);
			
			$quotation_id=$row_token['QuotationID'];
			
			//update quotation
			$table_name = 'INTRANET_PCM_CASE_QUOTATION';
			$array_field_name = array('ReplyStatus','ReplyDate');
			$rows=array(
				array('1',date('Y-m-d H:i:s')),
			);
			$conditions = ' `QuotationID` = \''.$quotation_id.'\'';
			$this->updateData($table_name, $array_field_name, $rows,$conditions,true);
			return $quotation_id;
		}else{
			return false;
		}
	}
	
	public function getFinancialItemBudget($itemID){
		$settings = $this->getGeneralSettings();
		$academicYearID = $settings['defaultAcademicYearID'];
		$sql = "SELECT Budget FROM INTRANET_PCM_FINANCIAL_ITEM_BUDGET WHERE AcademicYearID = '$academicYearID' AND ItemID= '$itemID' AND IsDeleted = 0";
		$returnSet = $this->returnResultSet($sql);
		return $returnSet[0]['Budget'];
	}
	public function getItemSpendingUsed($itemID){
		$settings = $this->getGeneralSettings();
		$academicYearID = $settings['defaultAcademicYearID'];
		$sql = "Select SUM(DealPrice) AS sumOfSpendedMoney From INTRANET_PCM_RESULT AS ipr
				INNER JOIN INTRANET_PCM_CASE AS ipc ON (ipc.CaseID=ipr.CaseID )
				WHERE ItemID = '$itemID' AND AcademicYearID = '$academicYearID' AND ipr.IsDeleted = 0 AND ipc.IsDeleted = 0 
				GROUP BY ItemID";
		
		$returnSet = $this->returnResultSet($sql);
		return $returnSet[0]['sumOfSpendedMoney'];
	}
	public function getGroupSpendingUsed($groupID){
		$settings = $this->getGeneralSettings();
		$academicYearID = $settings['defaultAcademicYearID'];
		$sql = "Select SUM(DealPrice) AS sumOfSpendedMoney From INTRANET_PCM_RESULT AS ipr
		INNER JOIN INTRANET_PCM_CASE AS ipc ON (ipc.CaseID=ipr.CaseID )
		WHERE ApplicantGroupID = '$groupID' AND AcademicYearID = '$academicYearID' AND ipr.IsDeleted = 0 AND ipc.IsDeleted = 0
		GROUP BY ApplicantGroupID";
	
		$returnSet = $this->returnResultSet($sql);
		return $returnSet[0]['sumOfSpendedMoney'];
	}
	
	### report - file code summary
	
	public function getCaseInfoForReportFCS($academic_year_id,$date_from,$date_to,$array_category,$array_group,$array_rule,$array_status){ //2016-10-03	villa
		global $ePCMcfg;
		$name_field = getNameFieldByLang2('iu.');
		
		$conds = ' AND ipc.IsDeleted = 0 ';
		if($academic_year_id!=''){
			//academic year
			$conds.=' AND `ipc`.`AcademicYearID`=\''.$academic_year_id.'\'';
		}else{
			//date
			$conds.=' AND `ipc`.`DateInput` BETWEEN \''.$date_from.' 00:00:00\' AND \''.$date_to.' 23:59:59\'';	
		}
		
		$sql_category=implode('\',\'',$array_category);
		if(count($array_category)>0){
			$conds.=' AND `ipc`.`CategoryID` IN (\''.$sql_category.'\')';	
		}
		
		$sql_group=implode('\',\'',$array_group);
		if(count($array_group)>0){
			$conds.=' AND `ipf`.`GroupID` IN (\''.$sql_group.'\')';	
		}
		
		$conds .= "AND ipc.RuleID NOT IN (Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = 'O') ";
		
//		$sql_rule=implode('\',\'',$array_rule);
//		if(count($array_rule)>0 && $array_rule[0]!=''){
//			$conds.=' AND `ipc`.`RuleID` IN (\''.$sql_rule.'\')';	
//		}
		
		//budget
		if(count($array_rule)>0 && $array_rule[0]!=''){
			$sql_budget=' AND (FALSE';
			$rows_rule=$this->getRuleInfo($array_rule);
			foreach((array)$rows_rule as $row_rule){
				$sql_budget.=' OR (`ipc`.`Budget` >= \''.$row_rule['FloorPrice'].'\''.($row_rule['CeillingPrice']==''?'':(' AND `ipc`.`Budget` <= \''.$row_rule['CeillingPrice'].'\'')).')';
			}
			$sql_budget.=')';
			$conds.=$sql_budget;
		}
		
		//status
		if(count($array_status)>0 && $array_status[0]!=''){
			$sql_status=' AND (FALSE';
			foreach((array)$array_status as $status){
				switch($status){
					case '1':
						$sql_status.=' OR (`CurrentStage` BETWEEN 1 AND 5 and `CaseID` NOT IN (SELECT `CaseID` FROM `INTRANET_PCM_CASE_APPROVAL` `ipca` WHERE `ipca`.`RecordStatus`=-1) )';
						break;
					case '2':
						$sql_status.=' OR (`CurrentStage`=10 and `CaseID` NOT IN (SELECT `CaseID` FROM `INTRANET_PCM_CASE_APPROVAL` `ipca` WHERE `ipca`.`RecordStatus`=-1))';
						break;
					case '3':
						$sql_status.=' OR (`CaseID` IN (SELECT `CaseID` FROM `INTRANET_PCM_CASE_APPROVAL` `ipca` WHERE `ipca`.`RecordStatus`=-1))';
						break;
				}
			}
			$sql_status.=')';
			$conds.=$sql_status;
		}

		//report data query
		$sql = "SELECT 
					(ipc.`CaseID` IN (SELECT `ipca`.`CaseID` FROM `INTRANET_PCM_CASE_APPROVAL` `ipca` WHERE `ipca`.`RecordStatus`=-1)) as `rejected`,
					CONCAT(`rule`.`FloorPrice`,'-',`rule`.`CeillingPrice`) AS `rule_range`,
					`rule`.`FloorPrice`,
					`rule`.`CeillingPrice`,
					ipc.CaseID,
					ipc.Code,
					ipc.CaseName,
					ipc.RuleID,
					ipc.CategoryID,
					cat.CategoryName,
					cat.CategoryNameChi,
					ipc.Description,
					ipc.ItemID,
					ipf.ItemName,
					ipf.ItemNameChi,
					ipc.AcademicYearID,
					ipc.Budget,
					ipc.ApplicantType,
					ipc.ApplicantUserID,
					$name_field AS ApplicantUserName, 
					ipc.ApplicantGroupID,
					ipg.GroupName,
					ipg.GroupNameChi,
					rule.QuotationType,
					ipc.CaseApprovedDate,
					ipc.QuotationStartDate,
					ipc.QuotationEndDate,
					ipc.TenderOpeningStartDate,
					ipc.TenderOpeningEndDate,
					ipc.TenderApprovalStartDate,
					ipc.TenderApprovalEndDate,
					ipc.CurrentStage,
					ipc.PurchaseDate,
					ipc.DateInput,
					ipc.DateModified,
					ipc.InputBy,
					ipc.ModifiedBy,
					ipc.IsDeleted,
					ipc.DeleteReason,
					rule.QuotationType,
					rule.ApprovalRoleID,
					rule.NeedGroupHeadApprove,
					rule.MinQuotation,
					rule.QuotationApprovalRoleID,
					rule.TenderAuditingRoleID,
					rule.TenderApprovalRoleID
				FROM INTRANET_PCM_CASE AS ipc 
				INNER JOIN (SELECT * FROM INTRANET_PCM_RULE WHERE IsTemplate=1 AND IsDeleted=0) AS rule ON (ifnull(rule.CeillingPrice,999999999)>=ipc.Budget AND rule.FloorPrice<=ipc.Budget)			
				INNER JOIN INTRANET_PCM_CATEGORY AS cat ON (cat.CategoryID=ipc.CategoryID)
				INNER JOIN INTRANET_PCM_GROUP AS ipg ON (ipg.GroupID=ipc.ApplicantGroupID)
				INNER JOIN INTRANET_PCM_GROUP_FINANCIAL_ITEM AS ipf ON (ipf.ItemID=ipc.ItemID)
				INNER JOIN 	".$ePCMcfg['INTRANET_USER']." AS iu	ON (iu.UserID = ipc.ApplicantUserID)
				WHERE 1 $conds 
				ORDER BY `rule`.`FloorPrice` ASC, `rule`.`CeillingPrice` ASC, `ipc`.`Code` ASC";

		return $this-> returnResultSet($sql);
	}
	
	### report - Budget and Expenses Report
	
	public function getCaseInfoForReportBER($academic_year_id,$array_category,$array_group,$is_include_zero_expenses='0',$array_application_type){ //villa 2016-10-03
// 		$conds = '';
		
		$sql_group=implode('\',\'',$array_group);
		if(count($array_group)>0){
			$group_conds .= ' AND `ipg`.`GroupID` IN (\''.$sql_group.'\')';	
		}
		
// 		$conds2 = $conds;
		
		//academic year
// 		$conds .= ' AND `ipc`.`AcademicYearID`=\''.$academic_year_id.'\'';
		
		$sql_category=implode('\',\'',$array_category);
		if(count($array_category)>0){
			//$conds .= ' AND `ipc`.`CategoryID` IN (\''.$sql_category.'\')';
			$category_conds = ' AND `ipc`.`CategoryID` IN (\''.$sql_category.'\')';
		}
		
		// RecordType condition
		$recordType_conds = '';
		if(in_array('select_application',(array)$array_application_type) && in_array('select_otherApplication',(array)$array_application_type)){
			//no cond
		}elseif(in_array('select_otherApplication',(array)$array_application_type)){
			$recordType_conds.= " AND ipc.RuleID IN (Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = 'O') ";
		}else{
			$recordType_conds.= " AND ipc.RuleID NOT IN (Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = 'O') ";
		}
		
		//
		if(!$is_include_zero_expenses){
			$include_zero_expenses_having_conds = " HAVING ItemExpense > 0 ";
		}
// 		$conds .= "AND ipc.RuleID NOT IN (Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = 'O') ";

//	20161024 Revised by Omas
// 		$sql = 'SELECT `ipg`.`GroupID`,`ipg`.`GroupName`,`ipg`.`GroupNameChi`,`ipgfi`.`ItemID`,`ipgfi`.`ItemName`,`ipgfi`.`ItemNameChi`, avg(ifnull((SELECT `Budget` FROM `INTRANET_PCM_FINANCIAL_ITEM_BUDGET` `ipfib` WHERE `ipfib`.`ItemID`=`ipgfi`.`ItemID` AND `ipfib`.`IsDeleted`=\'0\'),0)) as `Budget`, sum(ifnull((SELECT `DealPrice` FROM `INTRANET_PCM_RESULT` `ipr` WHERE `ipc`.`CaseID`=`ipr`.`CaseID` AND `ipr`.`IsDeleted`=\'0\'),0)) as `DealPrice`' .
// 				' FROM `INTRANET_PCM_GROUP_FINANCIAL_ITEM` `ipgfi`, `INTRANET_PCM_CASE` `ipc`,`INTRANET_PCM_GROUP` `ipg`' .
// 				' WHERE `ipc`.`ItemID`=`ipgfi`.`ItemID` AND `ipgfi`.`GroupID`=`ipg`.`GroupID` AND `ipgfi`.`IsDeleted`=\'0\' AND `ipc`.`IsDeleted`=\'0\' AND `ipg`.`IsDeleted`=\'0\'' .
// 				' AND `ipc`.`CurrentStage`=\'10\''.
// 				' '.$conds.$conds3.''.
// 				' GROUP BY `ipg`.`GroupID`,`ipg`.`GroupName`,`ipg`.`GroupNameChi`,`ipgfi`.`ItemID`,`ipgfi`.`ItemName`,`ipgfi`.`ItemNameChi`' .
// 				' ORDER BY `ipg`.`GroupName`,`ipgfi`.`ItemName`';
// // 		debug_pr($sql);
// 		if($is_include_zero_expenses == '1'){
// 			$sql2 = 'SELECT `ipg`.`GroupID`,`ipg`.`GroupName`,`ipg`.`GroupNameChi`,`ipgfi`.`ItemID`,`ipgfi`.`ItemName`,`ipgfi`.`ItemNameChi`, (ifnull((SELECT `Budget` FROM `INTRANET_PCM_FINANCIAL_ITEM_BUDGET` `ipfib` WHERE `ipfib`.`ItemID`=`ipgfi`.`ItemID` AND `ipfib`.`IsDeleted`=\'0\'),0)) as `Budget`, 0 as `DealPrice`' .
// 					' FROM `INTRANET_PCM_GROUP_FINANCIAL_ITEM` `ipgfi`, `INTRANET_PCM_GROUP` `ipg`' .
// 					' WHERE `ipgfi`.`GroupID`=`ipg`.`GroupID` AND `ipgfi`.`IsDeleted`=\'0\' AND `ipg`.`IsDeleted`=\'0\'' .
// 					' '.$conds2.'';
			
// 			$sql3 = 'SELECT `GroupID`,`GroupName`,`GroupNameChi`,`ItemID`,`ItemName`,`ItemNameChi`, avg(`Budget`) as `Budget`, sum(`DealPrice`) as `DealPrice`' .		
// 					' FROM (('.$sql.') UNION ('.$sql2.')) as `data`' .
// 					' GROUP BY `GroupID`,`GroupName`,`GroupNameChi`,`ItemID`,`ItemName`,`ItemNameChi`' .
// 					' ORDER BY `GroupName`,`ItemName`;';
					
// 			$sql = $sql3;
// 		}
$sql = "SELECT
			ipg.GroupID,
			ipg.GroupName,
			ipg.GroupNameChi,
			ipgfi.ItemID,
			ipgfi.ItemName,
			ipgfi.ItemNameChi,
			group_concat(ipc.CaseID) as GroupedCaseNum,
			group_concat(ip_rule.QuotationType) as GroupedType,
			TRUNCATE(AVG(ipfib.Budget), 2) as ItemBudget,
			sum(ipr.DealPrice) as ItemExpense
		FROM 
			INTRANET_PCM_GROUP as ipg  
			LEFT JOIN INTRANET_PCM_GROUP_FINANCIAL_ITEM as ipgfi ON ipgfi.GroupID=ipg.GroupID AND ipgfi.IsDeleted='0'
			LEFT JOIN INTRANET_PCM_FINANCIAL_ITEM_BUDGET as ipfib ON ipgfi.itemID = ipfib.itemID AND ipfib.AcademicYearID = '".$academic_year_id."'
			LEFT JOIN INTRANET_PCM_CASE as ipc ON ipc.ItemID=ipgfi.ItemID and ipc.CurrentStage = '10' AND ipc.IsDeleted = '0' AND ipc.AcademicYearID = '".$academic_year_id."'".$category_conds." ".$recordType_conds."
			LEFT JOIN INTRANET_PCM_RULE as ip_rule ON ip_rule.RuleID = ipc.RuleID
			LEFT JOIN INTRANET_PCM_RESULT as ipr ON ipc.CaseID = ipr.CaseID and ipr.IsDeleted = '0'
		WHERE 
			ipg.IsDeleted='0'
			$group_conds 
		GROUP BY 
			ipg.GroupID, ipgfi.ItemID
			$include_zero_expenses_having_conds
		ORDER BY 
			ipg.GroupName,ipgfi.ItemName";
			return $this-> returnResultSet($sql);
	}
	
	private function getBudgetConds($parRuleInfoArr, $parColumnPrefix){
		$budget_conds = "";
		$budget_conds .= " (";
		if(count($parRuleInfoArr) > 0){
			$first = true;
			$budget_conds .= " (";
			foreach((array)$parRuleInfoArr as $_ruleInfo){
				$_floor = $_ruleInfo['FloorPrice'];
				$_ceiling = $_ruleInfo['CeillingPrice'];
				if(!$first){
					$budget_conds .= " OR (";
				}
					
				$budget_conds .= " ".$parColumnPrefix."Budget >= $_floor";
				if(!empty($_ceiling)){
					$budget_conds .= " AND ".$parColumnPrefix."Budget <= $_ceiling ";
				}
				$budget_conds .= " )";
				$first = false;
			}
		}
		$budget_conds .= " )";
		return $budget_conds;
	}
	
	public function getCaseInfoForReportPCMRS_v2($parAcademicYearId,$parCatIdArr,$parGroupIdArr,$parBudgetArr,$parStatusArr, $parRecordTypeArr, $parOrder){
		
		
		global $ePCMcfg;
		
		$name_field = getNameFieldByLang2('iu.');
		$commonConds = "ipc.IsDeleted = 0 ";
		if($parAcademicYearId != ''){
			//academic year
			$commonConds .= " AND ipc.AcademicYearID='".$parAcademicYearId."'";
		}
		
		if(count($parCatIdArr)>0){
			$commonConds .= " AND ipc.CategoryID IN ('".implode("','",$parCatIdArr)."')";
		}
		
		if(count($parGroupIdArr)>0){
			//$conds .= " AND ipf.GroupID IN ('".implode("','",$parGroupIdArr)."')";
			$commonConds .= " AND ipc.ApplicantGroupID IN ('".implode("','",$parGroupIdArr)."')";
		}
		
		$baseSql = " SELECT
			(ipc.CaseID IN (SELECT ipca.CaseID FROM INTRANET_PCM_CASE_APPROVAL
			ipca WHERE ipca.RecordStatus=-1)) as rejected,
			CONCAT(rule.FloorPrice,'-',rule.CeillingPrice) AS rule_range,
			rule.FloorPrice,
			rule.CeillingPrice,
			ipc.CaseID,
			ipc.Code,
			ipc.CaseName,
			ipc.RuleID,
			ipc.CategoryID,
			cat.CategoryName,
			cat.CategoryNameChi,
			ipc.Description,
			ipc.ItemID,
			ipf.ItemName,
			ipf.ItemNameChi,
			ipc.AcademicYearID,
			ipc.Budget,
			ipc.ApplicantType,
			ipc.ApplicantUserID,
			$name_field AS ApplicantUserName,
			ipc.ApplicantGroupID,
			ipg.GroupName,
			ipg.GroupNameChi,
			rule.QuotationType,
			ipc.CaseApprovedDate,
			ipc.QuotationStartDate,
			ipc.QuotationEndDate,
			ipc.TenderOpeningStartDate,
			ipc.TenderOpeningEndDate,
			ipc.TenderApprovalStartDate,
			ipc.TenderApprovalEndDate,
			ipc.CurrentStage,
			ipc.PurchaseDate,
			ipc.DateInput,
			ipc.DateModified,
			ipc.InputBy,
			ipc.ModifiedBy,
			ipc.IsDeleted,
			ipc.DeleteReason,
			rule.ApprovalRoleID,
			rule.NeedGroupHeadApprove,
			rule.MinQuotation,
			rule.QuotationApprovalRoleID,
			rule.TenderAuditingRoleID,
			rule.TenderApprovalRoleID,
			DealPrice,
			DealDate,
			ips.SupplierName,
			ips.SupplierNameChi
		FROM INTRANET_PCM_CASE AS ipc
			INNER JOIN INTRANET_PCM_RULE AS rule ON (rule.RuleID = ipc.RuleID )
			INNER JOIN INTRANET_PCM_CATEGORY AS cat ON (cat.CategoryID = ipc.CategoryID)
			INNER JOIN INTRANET_PCM_GROUP AS ipg ON (ipg.GroupID = ipc.ApplicantGroupID)
			INNER JOIN INTRANET_PCM_GROUP_FINANCIAL_ITEM AS ipf ON (ipf.ItemID = ipc.ItemID)
			INNER JOIN 	".$ePCMcfg['INTRANET_USER']." AS iu	ON (iu.UserID = ipc.ApplicantUserID)
			LEFT JOIN INTRANET_PCM_RESULT AS ipr ON (ipr.CaseID = ipc.CaseID and ipr.isDeleted = 0)
			LEFT JOIN INTRANET_PCM_SUPPLIER AS ips ON (ipr.SupplierID = ips.SupplierID)
		WHERE ";

		$pcmConds = " AND rule.QuotationType != 'O' ";
		$miscConds = " AND rule.QuotationType = 'O' ";
		
		//budget (not apply to misc expenditure)
		$ruleTemplateArr = $this->getRuleTemplateInfo();
		$isIncludedAllBudget = count($parBudgetArr) == count($ruleTemplateArr);
		if(count($parBudgetArr)>0 && !$isIncludedAllBudget){
			// not yet cater change rule !
			$selectedRules = $this->getRuleInfo($parBudgetArr);
			$pcmConds .= ' AND '.$this->getBudgetConds($selectedRules, "ipc.");
		}

		// status
		// pcm
		$statusSqlArr = array();
		if(count($parStatusArr) < 3){
			if(in_array('1', $parStatusArr)){
				$statusSqlArr[] = '(CurrentStage BETWEEN 1 AND 5)';
			}
			if(in_array('2', $parStatusArr)){
				$statusSqlArr[] = '(CurrentStage = 10)';
			}
			if(in_array('3', $parStatusArr)){
				$statusSqlArr[] = '(ipc.CaseID IN (SELECT CaseID FROM INTRANET_PCM_CASE_APPROVAL ipca WHERE ipca.RecordStatus = -1))';
			}
		}
		if(!empty($statusSqlArr)){
			$pcmConds .= 'AND ('.implode(' OR ', $statusSqlArr).')';
		}
		
		// misc
		$statusSqlArr = array();
		if(count($parStatusArr) < 3){
			if(in_array('1', $parStatusArr)){
				$statusSqlArr[] = '(CurrentStage = 2)';
			}
			if(in_array('2', $parStatusArr)){
				$statusSqlArr[] = '(CurrentStage = 10)';
			}
			if(in_array('3', $parStatusArr)){
				$statusSqlArr[] = '(CurrentStage = 1)';
			}
		}
		if(!empty($statusSqlArr)){
			$miscConds .= 'AND ('.implode(' OR ', $statusSqlArr).')';
		}
		
		
		// Record Type (select_otherApplication : misc)
		$recordTypeOptions = array('select_application','select_otherApplication');
		// if disabled misc from setting		
		if(!isset($parRecordTypeArr)){
			$parRecordTypeArr = array('select_application');
		}
		// normal  procuremnt
		$pcmArr = array();
		if(in_array('select_application' ,$parRecordTypeArr)){
			$pcmArr = $this->returnResultSet($baseSql.$commonConds.$pcmConds);
		}
		// misc expenditure
		$miscArr = array();
		if(in_array('select_otherApplication' ,$parRecordTypeArr)){
			$miscArr = $this->returnResultSet($baseSql.$commonConds.$miscConds);
		}
		
		$resultArr = array_merge($pcmArr, $miscArr);

		// order data  DateInput,  Code
		
		switch($parOrder){
			case 'DateInput':
				sortByColumn2($resultArr, 'DateInput', 0, 0, 'Code');			
				break;
			default:
				sortByColumn2($resultArr, 'Code', 0, 0, '');
		}
		
		return $resultArr;
	}
	
	public function cancelQuotationDateAndFile($quotationID,$remarks){
		global $UserID;
		$sql = "UPDATE INTRANET_PCM_CASE_QUOTATION 
				SET QuoteDate = NULL, DateModified = NOW(), ModifiedBy = '$UserID', Remarks = '$remarks'
				WHERE QuotationID = '$quotationID'";
		return $this->db_db_query($sql);
	}
	public function getQuotationPriceItemInfo($caseID='',$quotationIDAry='',$supplierIDAry=''){
		if($caseID!==''){
			$cond = " AND ipcr.CaseID = '$caseID' ";
		}
		if($quotationIDAry!==''){
			$cond .= " AND ippq.QuotationID IN ('".implode("','",$quotationIDAry)."') ";
		}
		if($supplierIDAry!==''){
			$supplierIDAry = (array)$supplierIDAry;
			$cond .= " AND SupplierID IN ('".implode("','",$supplierIDAry)."') ";
			$additionalJoin = " INNER JOIN INTRANET_PCM_CASE_QUOTATION AS ipcq ON (ipcq.QuotationID = ippq.QuotationID) ";
		}
		$sql = "SELECT 
					ippq.PriceItemID, ippq.QuotationID, ippq.Price, ipcr.CaseID, ipcr.PriceItemName
					FROM INTRANET_PCM_PRICEITEM_QUOTATION AS ippq
					INNER JOIN INTRANET_PCM_CASE_PRICEITEM AS ipcr ON (ippq.PriceItemID=ipcr.PriceItemID)
					$additionalJoin
					WHERE ipcr.IsDeleted = 0 $cond";
		return $this->returnResultSet($sql);
	}
	
	public function getTempTable_supplierType(){
		global $_SESSION;
		$sql = "SELECT TypeName,TypeNameChi FROM TEMP_INTRANET_PCM_SUPPLY_TYPE WHERE InputBy = '".$_SESSION['UserID']."'";
		return $this->returnResultSet($sql);
	}
	public function getTempTable_supplier(){
		global $_SESSION;
		$sql = "SELECT SupplierName,SupplierNameChi,Website,Description,Phone,Fax,Email,TypeID  as Type, 1 as IsActive FROM TEMP_INTRANET_PCM_SUPPLIER WHERE InputBy = '".$_SESSION['UserID']."'";
		return $this->returnResultSet($sql);
	}
	public function updateTokenPassword($token,$pwd){
		$tableName = 'INTRANET_PCM_QUOTATION_TOKEN';
		$changefieldNameArr = array('Pw');
		$multiRowValueArr = array(array($pwd));
		$conditions = "Token = '$token'";
		return $this->updateData($tableName, $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField=false);
	}
	public function getTokenInfo($token='',$quotationID=''){
		if($token===''&&$quotationID===''){
			//return null;
			return array();
		}else if($token!==''){
			$cond = " AND Token = '$token' ";
		}else if($quotationID!==''){
			$cond .= " AND QuotationID = '$quotationID' ";
		}
		$sql = "SELECT * FROM INTRANET_PCM_QUOTATION_TOKEN WHERE 1 $cond";
		$result =  $this->returnResultSet($sql);
		return $result[0];
	}
	
	###################
	##	eInv (Start)
	###################
	
	function returnFundingSourceWithFundingType_eInv(){
		global $_PAGE;
		$linventory = $_PAGE['Controller']->requestObject('libinventory','includes/libinventory.php');
		$fundingList_arr = $linventory->returnFundingSourceWithFundingType();
		
		if($_PAGE['libPCM']->isEJ()){
			return $this->convert2unicode_recursive($fundingList_arr);
		}else{
			return $fundingList_arr;
		}
	}
	
	###################
	##	eInv (End)
	###################
	
	###################
	## Convert to UFT-8 
	###################
	function convert2unicode_recursive($input){
		global $_PAGE;
		if($_PAGE['libPCM']->isEJ()){
			if(!is_array($input)){
				$output = convert2unicode($input, $isAbsOn="", $direction=1);
			}else{
				$inputArr = $input;
				foreach($inputArr as $key => $val){
					$inputArr[$key] = $this->convert2unicode_recursive($val);
				}
				$output = $inputArr;
			}
			return $output;
		}else{
			return $input;
		}
	}
	
	public function getCompletedCaseSqlForDbTable2(){	//2016-09-29	Villa for display other application
		global $ePCMcfg, $Lang;
		$name_field = getNameFieldByLang2('applicantuser.');
	
		$cond = " AND ipc.IsDeleted = 0 ";
		$cond2 = " AND ipc.RuleID IN (Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = 'O') ";
	
	
		//$ItemNameField = Get_Lang_Selection('ipgf.ItemNameChi as ItemName','ipgf.ItemName as ItemName');
		$ItemNameField = Get_Lang_Selection('ipgf.ItemNameChi as Financialitem','ipgf.ItemName as Financialitem');
		$CategoryNameField = Get_Lang_Selection('cat.CategoryNameChi as CategoryName','cat.CategoryName as CategoryName');
		$GroupNameField = Get_Lang_Selection('applicantgroup.GroupNameChi','applicantgroup.GroupName');
	
		/**
		 * MYSQL BUG
		 * - ref: http://bugs.mysql.com/bug.php?id=12030
		 * Solutions: cast(ipc.CaseID as char)
		 */
		// 		INNER JOIN INTRANET_PCM_RESULT AS ipr ON (ipr.CaseID=ipc.CaseID)
		$sql = "SELECT
		CaseName,
		$CategoryNameField,
		$GroupNameField AS Department,
		$ItemNameField,
		ipc.Budget,
		DATE_FORMAT(ipc.DateInput,'%Y-%m-%d') AS DealDate,
		$name_field as Applicant,
		IF(ipc.CurrentStage = '10','".$Lang['ePCM']['Mgmt']['Case']['Status']['Approved']."',IF(ipc.CurrentStage = '2','".$Lang['ePCM']['Mgmt']['Case']['Status']['Pending']."','".$Lang['ePCM']['Mgmt']['Case']['Status']['Reject']."')) as RecordStatus,
		IF(ipc.CurrentStage = '10',
				CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipc.CaseID ,'\"  isApprove =\"1\" canApprove =\"1\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />'),
				CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipc.CaseID ,'\"   canApprove =\"1\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
		), 
		IF (ipc.CurrentStage = '10', 'row_avaliable', IF(ipc.CurrentStage = '2','row_waiting','row_suspend')) as trCustClass
		FROM INTRANET_PCM_CASE AS ipc
		INNER JOIN INTRANET_PCM_GROUP_FINANCIAL_ITEM AS ipgf ON (ipc.ItemID = ipgf.ItemID)
		INNER JOIN INTRANET_PCM_CATEGORY AS cat ON (cat.CategoryID = ipc.CategoryID)
		INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS applicantuser ON (applicantuser.UserID= ipc.ApplicantUserID)
		INNER JOIN INTRANET_PCM_GROUP AS applicantgroup ON (applicantgroup.GroupID = ipc.ApplicantGroupID)
		$isGroupMemberJoinCond
		WHERE 1 $cond $cond2
		";
		return $sql;
	}
	
	public function getCaseInfoForFinishApplication($CaseID=''){
		$cond = " AND ipc.IsDeleted = 0 ";
		$cond2 = " AND ipc.RuleID IN (Select RuleID From INTRANET_PCM_RULE WHERE CurrentStage = '10') ";
		$cond3 = $CaseID!=''? " AND ipc.CaseID = $CaseID " : "";
	
	
		/**
		 * MYSQL BUG
		 * - ref: http://bugs.mysql.com/bug.php?id=12030
		 * Solutions: cast(ipc.CaseID as char)
		 */
		// 		INNER JOIN INTRANET_PCM_RESULT AS ipr ON (ipr.CaseID=ipc.CaseID)
		$sql = "SELECT
		CaseName, CategoryID, Description, ApplicantGroupID, ItemID, Budget, ApplicantType, ApplicantUserID, DateInput, CurrentStage, IsPrincipalApproved 
		FROM INTRANET_PCM_CASE AS ipc
		WHERE 1 $cond $cond2 $cond3
		";
	
		return $this->returnResultSet($sql);
	
	}
	
	public function getCaseInfoForOtherApplication($CaseID=''){
		$cond = " AND ipc.IsDeleted = 0 ";
		$cond2 = " AND ipc.RuleID IN (Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = 'O') ";
		$cond3 = $CaseID!=''? " AND ipc.CaseID = $CaseID " : "";
	
	
		/**
		 * MYSQL BUG
		 * - ref: http://bugs.mysql.com/bug.php?id=12030
		 * Solutions: cast(ipc.CaseID as char)
		 */
		// 		INNER JOIN INTRANET_PCM_RESULT AS ipr ON (ipr.CaseID=ipc.CaseID)
		$sql = "SELECT
		CaseName, CategoryID, Description, ApplicantGroupID, ItemID, Budget, ApplicantType, ApplicantUserID, DateInput, CurrentStage
		FROM INTRANET_PCM_CASE AS ipc
		WHERE 1 $cond $cond2 $cond3
		";
	
		return $this->returnResultSet($sql);
	
	}
	
	public function getCompletedCaseSqlForDbTableForNonAdminForOtherApplication(){		//2016-09-29	Villa
		global $UserID, $Lang;
		global $ePCMcfg;
		$sql = "SELECT GroupID FROM INTRANET_PCM_GROUP_MEMBER WHERE UserID=".$UserID." AND isGroupHead='1'";
		$result = $this->returnResultSet($sql);
		$a = '';
		if(empty($result)){
			$checkingCond = 0;
		}else{
		
			foreach($result as $value){
				$checkingCond .= $a.'ipc.ApplicantGroupID ='.$value['GroupID'];
				$a = '||';
			}
		}
		
		$name_field = getNameFieldByLang2('applicantuser.');
		
		$cond .= " AND ipc.IsDeleted = 0 ";
		$cond2 = " AND ipc.RuleID IN (Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = 'O')";
	
		$ItemNameField = Get_Lang_Selection('ipgf.ItemNameChi as ItemName','ipgf.ItemName as ItemName');
		$CategoryNameField = Get_Lang_Selection('cat.CategoryNameChi as CategoryName','cat.CategoryName as CategoryName');
		$GroupNameField = Get_Lang_Selection('applicantgroup.GroupNameChi','applicantgroup.GroupName');
		
		/**
		 * MYSQL BUG
		 * - ref: http://bugs.mysql.com/bug.php?id=12030
		 * Solutions: cast(ipc.CaseID as char)
		 */
		$sql = "SELECT
		CaseName,
		$CategoryNameField,
		$GroupNameField AS Department,
		$ItemNameField,
		ipc.Budget,
		DATE_FORMAT(ipc.DateInput,'%Y-%m-%d') AS DealDate,
		".$name_field." as Applicant,
		IF(ipc.CurrentStage = '10','".$Lang['ePCM']['Mgmt']['Case']['Status']['Approved']."', IF(ipc.CurrentStage = '2','".$Lang['ePCM']['Mgmt']['Case']['Status']['Pending']."','".$Lang['ePCM']['Mgmt']['Case']['Status']['Reject']."')) as RecordStatus,
		IF(
				".$checkingCond.",
				IF(
					ipc.CurrentStage = '10',
					'',
					CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipc.CaseID ,'\"   canApprove =\"1\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
				),
				IF( 
					ipc.ApplicantUserID = '".$_SESSION['UserID']."',
					IF(ipc.CurrentStage = '10',
						'',
						CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', ipc.CaseID ,'\"   onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
					),	
					'' 
				)
		),
		IF (ipc.CurrentStage = '10', 'row_avaliable', IF(ipc.CurrentStage = '2','row_waiting','row_suspend')) as trCustClass
		
		FROM INTRANET_PCM_CASE AS ipc
		INNER JOIN INTRANET_PCM_GROUP_FINANCIAL_ITEM AS ipgf ON (ipc.ItemID = ipgf.ItemID)
		INNER JOIN INTRANET_PCM_CATEGORY AS cat ON (cat.CategoryID = ipc.CategoryID)
		INNER JOIN ".$ePCMcfg['INTRANET_USER']." AS applicantuser ON (applicantuser.UserID= ipc.ApplicantUserID)
		INNER JOIN INTRANET_PCM_GROUP AS applicantgroup ON (applicantgroup.GroupID = ipc.ApplicantGroupID)
		".$this->innerJoinSqlForNonAdmin('ipc.',$UserID).$cond.$cond2;
// 				debug_pr($sql); 
	
		return $sql;
	}
	
	public function getLatestRuleIDForOtherApplication(){
		$sql = "Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = 'O' Order By RuleID Desc limit 1";
		return $this->returnResultSet($sql);
	}

    public function getLatestRuleID(){
        $sql = "Select RuleID From INTRANET_PCM_RULE Order By RuleID Desc limit 1";
        return $this->returnResultSet($sql);
    }
	
	protected function insertDataWithCustDateInput($tableName, $fieldNameArr, $multiRowValueArr){
	
		if(empty($fieldNameArr) || empty($multiRowValueArr) || $tableName=='' ){
			// 			$sql = "/*Cannot empty any param when calling libPCM_db->getInsertSQL */";
		}
		else{
			// Must have this 4 fields - "DateInput", "InputBy", "DateModified", "ModifiedBy"
			// and it will be add automatically
			if(!$excludeCommonField){
				$fieldNameArr[] = 'IsDeleted';
				$fieldNameArr[] = 'InputBy';
				$fieldNameArr[] = 'DateModified';
				$fieldNameArr[] = 'ModifiedBy';
			}
			// 			debug_pr($multiRowValueArr);
			// 			$valueListArr = $this->getSafeInsertValueList($fieldNameArr, $multiRowValueArr, $excludeCommonField);
			for($i=0; $i<count($multiRowValueArr[0]);$i++){
				$valueListArr[] = "'".$multiRowValueArr[0][$i]."'";
			}
			$valueListArr[] = '0';
			$valueListArr[] = $_SESSION['UserID'];
			$valueListArr[] = "now()";
			$valueListArr[] = $_SESSION['UserID'];
			// 			debug_pr($fieldNameArr);
			// 			debug_pr($valueListArr);
			if(!empty($valueListArr)){
				$sql = "INSERT INTO $tableName (".implode(', ',$fieldNameArr).") Values (".implode(',', $valueListArr).");";
			}
			else{
				// 				$sql = "/* NO VALUE */";
			}
		}
		if($this->debugMode){
			debug_pr($sql);
		}
		else{
			return $this->db_db_query($sql);
	
		}
	}
	
	public function getFundingRawData($FundingID='',$RecordStatus=''){
		$cond = '';
		if($FundingID){
			$cond .= ' AND FundingID = '.$FundingID;
		}
		if($RecordStatus||$RecordStatus==='0'){
			$cond .= ' AND RecordStatus = '.$RecordStatus;
		}
		$sql = "Select 
					* 
				FROM 
					INTRANET_PCM_FUNDING_SOURCE
				WHERE 
					IsDeleted  = '0'
					$cond
				";
		if($this->debugMode){
			debug_pr($sql);
		}else{
			$rs = $this->returnResultSet($sql);
			foreach ((array)$rs as $_rs){
				$data[$_rs['FundingID']] = $_rs;
			}
			return $data;
		}
	}
	public function getFundingCaseRelationRawData($CaseID='',$FundingID=''){
		$cond = "";
		if($CaseID){
			$cond .= " AND CaseID = $CaseID  ";
		}
		if($FundingID){
			$cond .= " AND FundingID  = $FundingID  ";
		}
		$sql = "Select
					*
				FROM
					INTRANET_PCM_FUNDING_SOURCE_CASE_RELATION
				WHERE
					IsDeleted  = '0'
					$cond
				";
		if($this->debugMode){
			debug_pr($sql);
		}else{
			$rs = $this->returnResultSet($sql);
			foreach ((array)$rs as $key=>$_rs){
				$data[$_rs['FundingID']][$key] = $_rs;
			}
			return $data;
		}
	}
	public function getFundingCategoryRawData($CategoryID=''){
		$cond = "";
		if($CategoryID){
			$cond = " AND FundingCategoryID = $CategoryID ";
		}
		$sql = "Select
					*
				FROM
					INTRANET_PCM_FUNDING_CATEGORY
				WHERE
					IsDeleted  = '0'
					$cond
				";
		if($this->debugMode){
			debug_pr($sql);
		}else{
			$rs = $this->returnResultSet($sql);
			if(!empty($rs)){
				foreach ((array)$rs as $_rs){
					$data[$_rs['FundingCategoryID']] = $_rs;
				}
			}else{
				$data = '';
			}
			return $data;
		}
	}
	public function getFundingCategoryRelationRawData($CategoryID='',$FundingID=''){
		$cond = "";
		if($CategoryID){
			$cond = " AND FundingCategoryID = $CategoryID ";
		};
		if($FundingID){
			$cond = " AND FundingID  = $FundingID ";
		};
		
		$sql = "Select
					FundingID, FundingCategoryID
				FROM
					INTRANET_PCM_FUNDING_SOURCE 
				WHERE
					IsDeleted  = '0'
					$cond
				ORDER BY
					DeadLineDate Desc
				";
		if($this->debugMode){
			debug_pr($sql);
		}else{
			$rs = $this->returnResultSet($sql);
			if(!empty($rs)){
				foreach ((array)$rs as $_rs){
					$data[$_rs['FundingCategoryID']][] = $_rs['FundingID'];
				}
			}else{
				$data = '';
			}
			return $data;
		}
	}
	public function getFundingCategoryLastestDisplayOrder(){
		$sql = "Select
					DisplayOrder 
				FROM
					INTRANET_PCM_FUNDING_CATEGORY
				WHERE
					IsDeleted  = '0'
				ORDER BY
					DisplayOrder desc
				Limit 1
				";
		if($this->debugMode){
			debug_pr($sql);
		}else{
			$rs = $this->returnResultSet($sql);
			foreach ((array)$rs as $_rs){
				$data = $_rs['DisplayOrder'];
			}
			return $data;
		}
	}
	public function getFundingLastestDisplayOrder(){
		$sql = "Select
					DisplayOrder
				FROM
					INTRANET_PCM_FUNDING_SOURCE
				WHERE
					IsDeleted  = '0'
				ORDER BY
					DisplayOrder desc
				Limit 1
				";
		if($this->debugMode){
			debug_pr($sql);
		}else{
			$rs = $this->returnResultSet($sql);
			foreach ((array)$rs as $_rs){
				$data = $_rs['DisplayOrder'];
			}
			return $data;
		}
	}
	public function getFundingLastestFundingID(){
		$sql = "Select
					FundingID 
				FROM
					INTRANET_PCM_FUNDING_SOURCE
				WHERE
					IsDeleted  = '0'
				ORDER BY
					DateModified   desc
				Limit 1
				";
		if($this->debugMode){
			debug_pr($sql);
		}else{
			$rs = $this->returnResultSet($sql);
			foreach ((array)$rs as $_rs){
				$data = $_rs['FundingID'];
			}
			return $data;
		}
	}
	public function getCaseRawInfo(){
		$sql = "Select
					*
				FROM
					INTRANET_PCM_CASE 

				ORDER BY
					DateModified   desc
				";
		if($this->debugMode){
			debug_pr($sql);
		}else{
			$rs = $this->returnResultSet($sql);
			foreach ((array)$rs as $_rs){
				$data[$_rs['CaseID']] = $_rs;
			}
			return $data;
		}
	}
	
}


?>