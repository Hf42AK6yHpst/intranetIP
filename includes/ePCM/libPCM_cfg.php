<?php
###########################################################################
# Date : 2016-09-01 Omas - Improved this file can upload to EJ directly
###########################################################################
if(isset($junior_mck)){
	if($sys_custom['ePCM']['env'] == 'dev'){
		// dev 31011
		$ePCMcfg['INTRANET_USER'] = $sys_custom['INTRANET_USER'];
		$ePCMcfg['ROLE_MANAGE']='librole.php';
		$ePCMcfg['INTERNATDATA']='../../intranetdata_dev/ePCM/';	#Normal Site should be ../intranetdata/ePCM/
	}
	else{
		// cient site & testing 31012
		$ePCMcfg['INTRANET_USER'] = 'INTRANET_USER_UTF8';
		$ePCMcfg['ROLE_MANAGE']='librole.php';
		$ePCMcfg['INTERNATDATA']='../../intranetdata/ePCM/';	#Normal Site should be ../intranetdata/ePCM/
	}
}else{
	$ePCMcfg['INTRANET_USER'] = $sys_custom['INTRANET_USER'];
	$ePCMcfg['ROLE_MANAGE']='role_manage.php';
	$ePCMcfg['INTERNATDATA']='../intranetdata/ePCM/';
}

$ePCMcfg['passphrase'] = 'ePCMisVERYsecure';
$ePCMcfg['masterKey'] = 'a&!BH<6>r1S3f' . $config_school_code . 'g0=!sBp{v702';

// comment on 2016-09-01 if no problem please delete it
// $ePCMcfg['type']['description'] = 'value';
// $ePCMcfg['Stage']['11'] = 1;
// $ePCMcfg['xxxRecordStatus']['sss'] = 1;

$ePCMcfg['Template']['Application'] = 1;
$ePCMcfg['Template']['Invitation'] = 2;
$ePCMcfg['Template']['Declaration'] = 3;
$ePCMcfg['Template']['Result'] = 4;

$ePCMcfg['StageID']['Draft'] = 0;
$ePCMcfg['StageID']['CaseApproval'] = 1;
$ePCMcfg['StageID']['SendInvitation'] = 2;
$ePCMcfg['StageID']['InputQuotation'] = 3;
$ePCMcfg['StageID']['InputResult'] = 4;
$ePCMcfg['StageID']['Endorsement'] = 5;
$ePCMcfg['StageID']['Competed'] = 10;
// $ePCMcfg['StageID']['competed'] = 5;
?>