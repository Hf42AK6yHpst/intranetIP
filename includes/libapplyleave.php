<?php
/** [Modification Log] Modifying By: 
 * ******************************************
 *  2017-09-19 Carlos - Fix getDocumentFilePathBase() for KIS
 *  2015-07-29 Shan - Modified getApplyLeaveRecordListPageSql() for export
 * 	2014-12-09 Omas -Modified getApplyLeaveDataOrderByStudent() to get Document Status
 * *******************************************
 */ 

class libapplyleave extends libdb {
	var $settingsAry;
	
	/*
	 * ApprovalStatus:
	 * 		0: pending
	 * 		1: acknowledged
	 * 
	 * DocumentStatus:
	 * 		1: pending
	 * 		2: approved
	 * 		3: rejected
	 */
	
	function libapplyleave() {
		$this->libdb();
		$this->ModuleName = 'ApplyLeave';
	}
	
	function getSettingsAry() {
		if ($this->settingsAry == null) {
			$sql = "Select SettingKey, SettingValue From CARD_STUDENT_APPLY_LEAVE_SETTING";
			$this->settingsAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'SettingKey', array('SettingValue'), $SingleValue=1, $BuildNumericArray=0);
		}
		
		return $this->settingsAry;
	}
	
	function updateSetting($settingKey, $settingValue) {
		$settingKey = trim($settingKey);
		$settingValue = trim($settingValue);
		
		$sql = "Insert Into CARD_STUDENT_APPLY_LEAVE_SETTING ( SettingKey , SettingValue, InputDate, InputBy, ModifiedDate, ModifiedBy)
					Values ('".$settingKey."', '".$this->Get_Safe_Sql_Query($settingValue)."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."')
					On Duplicate Key Update SettingValue = VALUES(SettingValue), ModifiedDate = VALUES(ModifiedDate), ModifiedBy = VALUES(ModifiedBy)
				 ";
		return $this->db_db_query($sql);
	}
	
	function deleteApplyLeaveRecord($recordIdAry) {
		$successAry = array();
		
		$sql = "Update CARD_STUDENT_APPLY_LEAVE_RECORD Set IsDeleted = 1, ModifiedDate = now(), ModifiedBy = '".$_SESSION['UserID']."' Where RecordID in ('".implode("','", (array)$recordIdAry)."')";
		$successAry['deleteRecord'] = $this->db_db_query($sql);
		
		if ($successAry['deleteRecord']) {
			$successAry['deleteAttachment'] = $this->deleteLeaveRecordAttachement($recordIdAry, $fileIdAry='', $hardDelete=true);
		}
		
		return !in_array(false, $successAry);
	}
	
	function updateApplyLeaveStatus($recordIdAry, $dbField, $dbValue) {
		$changeByField = $dbField."ChangedBy";
		
		$sql = "Update CARD_STUDENT_APPLY_LEAVE_RECORD Set $dbField = '".$dbValue."', $changeByField = '".$_SESSION['UserID']."', ModifiedDate = now(), ModifiedBy = '".$_SESSION['UserID']."' Where RecordID in ('".implode("','", (array)$recordIdAry)."')";
		return $this->db_db_query($sql);
	}
	
	function updateApplyLeaveRejectReason($recordId, $reason) {		
		$sql = "Update CARD_STUDENT_APPLY_LEAVE_RECORD Set RejectReason = '".$reason."',ApprovalStatus = '2', ApprovalStatusChangedBy = '".$_SESSION['UserID']."', ModifiedDate = now(), ModifiedBy = '".$_SESSION['UserID']."' Where RecordID = '".$recordId."'";
		return $this->db_db_query($sql);
	}	
	
	function getApplyLeaveData($recordIdAry='') {
		if ($recordIdAry != '') {
			$cond_recordId = " And RecordID In ('".implode("','", (array)$recordIdAry)."') ";
		}
		$sql = "Select RecordID, StudentID, InputDate, StartDate, StartDateType, EndDate, EndDateType From CARD_STUDENT_APPLY_LEAVE_RECORD Where 1 $cond_recordId";
		return $this->returnResultSet($sql);
	}
	
	function getApplyLeaveDataOrderByStudent($recordIdAry='') {
		if ($recordIdAry != '') {
			$cond_recordId = " And r.RecordID In ('".implode("','", (array)$recordIdAry)."') ";
		}

		if(get_client_region() == 'zh_TW') {
			global $i_DayTypeWholeDay,$i_DayTypeAM,$i_DayTypePM,$i_DayTypeSession,$Lang;
			$EmptySymbol = $Lang['General']['EmptySymbol'];
			$sql = "Select 
						r.RecordID, r.StudentID, StartDate, StartDateType, EndDate, EndDateType, Duration, Reason, ApprovalStatus, DocumentStatus,
						CASE 
							WHEN r.AttendanceType = 0 THEN '$i_DayTypeWholeDay'
							WHEN r.AttendanceType = 1 THEN '$i_DayTypeAM'
							WHEN r.AttendanceType = 2 THEN '$i_DayTypePM'
							WHEN r.AttendanceType = 3 THEN '$i_DayTypeSession'
						END as AttendanceTypeText, 
						IF(r.AttendanceType = 3, r.SessionFrom, '$EmptySymbol') as SessionFromText,
						IF(r.AttendanceType = 3, r.SessionTo, '$EmptySymbol') as SessionToText, 
						IF(r.AttendanceType = 3, r.SessionTo - r.SessionFrom + 1, '$EmptySymbol') as SessionDiff, 
						lt.TypeName as LeaveTypeName,
						r.AttendanceType,
						r.SessionFrom,
						r.SessionTo,
						r.LeaveTypeID
				From 
						CARD_STUDENT_APPLY_LEAVE_RECORD as r
						Inner Join YEAR_CLASS_USER as ycu ON (r.StudentID = ycu.UserID)
						Inner Join YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
						Inner Join YEAR as y ON (yc.YearID = y.YearID)
						LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON r.LeaveTypeID = lt.TypeID
				Where 
						yc.AcademicYearID = '" . Get_Current_Academic_Year_ID() . "'
						$cond_recordId
				Order By
						y.Sequence, yc.Sequence, ycu.ClassNumber
				";
		} else {
			global $sys_custom;
			$leavetype_fields = '';
			$leavetype_join = '';
			if($sys_custom['StudentAttendance']['LeaveType']) {
				$leavetype_fields = ", lt.TypeName as LeaveTypeName ";
				$leavetype_join = "LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON r.LeaveTypeID = lt.TypeID";
			}
		$sql = "Select 
						r.RecordID, r.StudentID, StartDate, StartDateType, EndDate, EndDateType, Duration, Reason, ApprovalStatus, DocumentStatus 
						$leavetype_fields
				From 
						CARD_STUDENT_APPLY_LEAVE_RECORD as r
						Inner Join YEAR_CLASS_USER as ycu ON (r.StudentID = ycu.UserID)
						Inner Join YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
						Inner Join YEAR as y ON (yc.YearID = y.YearID)
						$leavetype_join
				Where 
						yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
						$cond_recordId
				Order By
						y.Sequence, yc.Sequence, ycu.ClassNumber
				";
		}
		return $this->returnResultSet($sql);
	}
	
	function getApplyLeaveRecordListPageSql($keyword, $applicationTimeType, $applicationStatus, $documentStatus,$forExport=false,$enableApplyLeaveHomeworkPassSetting=0) {
		global $Lang, $linterface, $sys_custom;

		$studentNameField = getNameFieldByLang('iu.');
		$classNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

		if($enableApplyLeaveHomeworkPassSetting==1){
            $methodField="CONCAT(
								CASE r.HomeworkDeliverMethodID
									WHEN '-1' THEN ''
									WHEN '0' THEN r.HomeworkDeliverMethod
									ELSE ".Get_Lang_Selection('hm.Method', 'hm.MethodEng')."
								END
							) as Method,";
        }else{
            $methodField = "";
        }


        ### keyword
		if ($keyword !== '' && $keyword !== null) {
			$cond_keyword = " And (
									$studentNameField Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
									Or $classNameField Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
									Or r.Reason Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
								  ) 
							";
		}
		
		
		### application date type
		$cond_startDate = '';
		if ($applicationTimeType == 1) {
			// all records
		}
		else if ($applicationTimeType == 2) {
			// past records
			$cond_startDate = " AND DATE(r.InputDate) < DATE(now()) ";
		}
		else if ($applicationTimeType == 3) {
			// today records
			$cond_startDate = " AND DATE(r.InputDate) = DATE(now()) ";
		}
		
		
		### acknowledgment status
		$cond_approvalStatus = '';
		if ($applicationStatus == 1) {
			// all records
		}
		else {
			// $dbApplicationStatus => 0: pending; 1: acknowledged;
			$dbApplicationStatus = $applicationStatus - 2;
			$cond_approvalStatus = " AND r.ApprovalStatus = '".$dbApplicationStatus."' ";
		}
		
		
		### document status
		$cond_documentStatus = '';
		if ($documentStatus == 1) {
			// all document
		}
		else {
			// $dbDocumentStatus => 0: pending; 1: approved; 2: rejected;
			$dbDocumentStatus = $documentStatus - 2;
			$cond_documentStatus = " AND r.DocumentStatus = '".$dbDocumentStatus."' ";
		}
		
		
		
				
		
		
		if ($forExport){
			$StartDate="CONCAT(
								CAST(r.StartDate as char),
								CASE r.StartDateType
									WHEN 'WD' THEN CONCAT('(', '".$Lang['StudentAttendance']['ApplyLeaveAry']['WD']."', ')')
									WHEN 'AM' THEN CONCAT('(', '".$Lang['StudentAttendance']['ApplyLeaveAry']['AM']."', ')')
									WHEN 'PM' THEN CONCAT('(', '".$Lang['StudentAttendance']['ApplyLeaveAry']['PM']."', ')')
									ELSE ''
								END
							)";
				
			$EndDate="CONCAT(
								CAST(r.EndDate as char),
								CASE r.EndDateType
									WHEN 'WD' THEN CONCAT('(', '".$Lang['StudentAttendance']['ApplyLeaveAry']['WD']."', ')')
									WHEN 'AM' THEN CONCAT('(', '".$Lang['StudentAttendance']['ApplyLeaveAry']['AM']."', ')')
									WHEN 'PM' THEN CONCAT('(', '".$Lang['StudentAttendance']['ApplyLeaveAry']['PM']."', ')')
									ELSE ''
								END
							)";
							
			$DocumentLink ="IF (
								lf.FileID IS NOT NULL, 
								CONCAT('". 	$Lang['General']['Yes2']."'), 
								'". 	$Lang['General']['No2']."'
							)";
			
			$ApprovalStatus ="	CASE r.ApprovalStatus
								    WHEN 0 THEN '".$Lang['StudentAttendance']['ApplyLeaveAry']['PendingAcknowledgeLeave']."'
								    WHEN 1 THEN '".$Lang['StudentAttendance']['ApplyLeaveAry']['AcknowledgedLeave']."'
								    WHEN 2 THEN '".$Lang['StudentAttendance']['ApplyLeaveAry']['RejectedLeave']."'
								    WHEN 3 THEN '".$Lang['StudentAttendance']['ApplyLeaveAry']['CancelledLeave']."'
								END";
							
			$DocumentStatus ="	CASE r.DocumentStatus
								    WHEN 0 THEN '".$Lang['General']['EmptySymbol']."'
								    WHEN 1 THEN '".$Lang['General']['Pending']."'
									WHEN 2 THEN '".$Lang['General']['Approved']."'
									WHEN 3 THEN '".$Lang['General']['Rejected']."'
								END";
		}
		else {
			if(get_client_region() == 'zh_TW') {
				$StartDate = "CAST(r.StartDate as char)";
			} else {
			$StartDate="CONCAT(
								CAST(r.StartDate as char),
								CASE r.StartDateType
									WHEN 'WD' THEN CONCAT('<br />(', '".$Lang['StudentAttendance']['ApplyLeaveAry']['WD']."', ')')
									WHEN 'AM' THEN CONCAT('<br />(', '".$Lang['StudentAttendance']['ApplyLeaveAry']['AM']."', ')')
									WHEN 'PM' THEN CONCAT('<br />(', '".$Lang['StudentAttendance']['ApplyLeaveAry']['PM']."', ')')
									ELSE ''
								END
							)";
			}
			
			if(get_client_region() == 'zh_TW') {
				$EndDate = "CAST(r.EndDate as char)";
			} else {
			$EndDate="CONCAT(
								CAST(r.EndDate as char),
								CASE r.EndDateType
									WHEN 'WD' THEN CONCAT('<br />(', '".$Lang['StudentAttendance']['ApplyLeaveAry']['WD']."', ')')
									WHEN 'AM' THEN CONCAT('<br />(', '".$Lang['StudentAttendance']['ApplyLeaveAry']['AM']."', ')')
									WHEN 'PM' THEN CONCAT('<br />(', '".$Lang['StudentAttendance']['ApplyLeaveAry']['PM']."', ')')
									ELSE ''
								END
							)";
			}
				
			$DocumentLink ="IF (
								lf.FileID IS NOT NULL, 
								CONCAT('<a href=\"javascript:void(0);\" class=\"tablelink\" onclick=\"viewDocument(', r.RecordID, ');\">".$Lang['Btn']['View']."</a>'), 
								'".$Lang['General']['EmptySymbol']."'
							)	";
			$pendingImage_approvalStatus = $linterface->Get_Pending_Image($Lang['StudentAttendance']['ApplyLeaveAry']['PendingAcknowledgeLeave']);
			$approveImage_approvalStatus = $linterface->Get_Approved_Image($Lang['StudentAttendance']['ApplyLeaveAry']['AcknowledgedLeave']);
			$rejectImage_approvalStatus = $linterface->Get_Rejected_Image($Lang['StudentAttendance']['ApplyLeaveAry']['Reject']);
        	$cancelImage_approvalStatus = '<img title="'.$Lang['StudentAttendance']['ApplyLeaveAry']['Cancel'].'" src="/images/2009a/icon_cancel.png" border="0" align="absmiddle" width="20px" />';
			$ApprovalStatus ="CONCAT(
								'<span id=\"approvalStatusSpan_', r.RecordID, '\">', 
								CASE r.ApprovalStatus
								    WHEN 0 THEN '".$this->Get_Safe_Sql_Query($pendingImage_approvalStatus)."'
								    WHEN 1 THEN '".$this->Get_Safe_Sql_Query($approveImage_approvalStatus)."'
								    WHEN 2 THEN '".$this->Get_Safe_Sql_Query($rejectImage_approvalStatus)."'
									WHEN 3 THEN '".$this->Get_Safe_Sql_Query($cancelImage_approvalStatus)."'
								END,
								'</span>',
								'<br><span id=\"rejectReasonSpan_', r.RecordID, '\">',
								CASE r.ApprovalStatus
								    WHEN 0 THEN ''									
								    WHEN 1 THEN ''									
								    WHEN 2 THEN r.RejectReason
								    WHEN 3 THEN ''									
								END,
								'</span>'
							)";
			
			$pendingImage_documentStatus = $linterface->Get_Pending_Image();
			$approveImage_documentStatus = $linterface->Get_Approved_Image();
			$rejectImage_documentStatus = $linterface->Get_Rejected_Image();
			
			$DocumentStatus ="CONCAT(
								'<span id=\"documentStatusSpan_', r.RecordID, '\">', 
								CASE r.DocumentStatus
								    WHEN 0 THEN '".$Lang['General']['EmptySymbol']."'
								    WHEN 1 THEN '".$this->Get_Safe_Sql_Query($pendingImage_documentStatus)."'
									WHEN 2 THEN '".$this->Get_Safe_Sql_Query($approveImage_documentStatus)."'
									WHEN 3 THEN '".$this->Get_Safe_Sql_Query($rejectImage_documentStatus)."'
								END,
								'</span>'
							)";
		}
		
	 		
		if(get_client_region() == 'zh_TW') {
			global $i_DayTypeWholeDay,$i_DayTypeAM,$i_DayTypePM,$i_DayTypeSession;
			$EmptySymbol = $Lang['General']['EmptySymbol'];
			$sql = "Select
						$classNameField as ClassName, 
						ycu.ClassNumber, 
						$studentNameField as StudentName,
						r.InputDate,
						CASE 
							WHEN r.AttendanceType = 0 THEN '$i_DayTypeWholeDay'
							WHEN r.AttendanceType = 1 THEN '$i_DayTypeAM'
							WHEN r.AttendanceType = 2 THEN '$i_DayTypePM'
							WHEN r.AttendanceType = 3 THEN '$i_DayTypeSession'
						END as AttendanceType,
						$StartDate as StartDate,
						IF(r.AttendanceType = 0, $EndDate, '$EmptySymbol') as EndDate,
						IF(r.AttendanceType = 0, r.Duration, '$EmptySymbol') as Duration,
						IF(r.AttendanceType = 3, r.SessionFrom, '$EmptySymbol') as SessionFrom,
						IF(r.AttendanceType = 3, r.SessionTo, '$EmptySymbol') as SessionTo, 
						IF(r.AttendanceType = 3, r.SessionTo - r.SessionFrom + 1, '$EmptySymbol') as SessionDiff, 
						lt.TypeName as LeaveTypeName, 
						r.Reason,
						$ApprovalStatus as ApprovalStatus,
						r.ModifiedDate,
						CONCAT('<input type=\"checkbox\" name=\"recordIdAry[]\" id=\"recordIdChk_', r.RecordID, '\" class=\"recordIdChk\" value=\"', r.RecordID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
				From 
						CARD_STUDENT_APPLY_LEAVE_RECORD as r
						Inner Join INTRANET_USER as iu ON (r.StudentID = iu.UserID)
						Inner Join YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID)
						Inner Join YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
						Left Outer Join CARD_STUDENT_APPLY_LEAVE_FILE as lf ON (r.RecordID = lf.LeaveRecordID)
						Left Outer Join APPLY_LEAVE_HOMEWORK_PASS_METHOD_SETTING as hm ON (r.HomeworkDeliverMethodID = hm.MethodID)
						LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON r.LeaveTypeID = lt.TypeID
				Where
						r.IsDeleted = '0'
						And yc.AcademicYearID = '" . Get_Current_Academic_Year_ID() . "'
						$cond_startDate
						$cond_keyword
						$cond_approvalStatus
				";
		} else {
			$leavetype_fields = '';
			$leavetype_join = '';
			if($sys_custom['StudentAttendance']['LeaveType']) {
				$leavetype_fields = " lt.TypeName as LeaveTypeName, ";
				$leavetype_join = "LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON r.LeaveTypeID = lt.TypeID";
			}
		$sql = "Select
						$classNameField as ClassName, 
						ycu.ClassNumber, 
						$studentNameField as StudentName,
						r.InputDate,
						$StartDate as StartDate,
						$EndDate as EndDate,
						r.Duration,
						$leavetype_fields
						r.Reason,
						$methodField
						$DocumentLink as DocumentLink,
						$ApprovalStatus as ApprovalStatus,
						$DocumentStatus as DocumentStatus,
						r.ModifiedDate,
						CONCAT('<input type=\"checkbox\" name=\"recordIdAry[]\" id=\"recordIdChk_', r.RecordID, '\" class=\"recordIdChk\" value=\"', r.RecordID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
				From 
						CARD_STUDENT_APPLY_LEAVE_RECORD as r
						Inner Join INTRANET_USER as iu ON (r.StudentID = iu.UserID)
						Inner Join YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID)
						Inner Join YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
						Left Outer Join CARD_STUDENT_APPLY_LEAVE_FILE as lf ON (r.RecordID = lf.LeaveRecordID)
						Left Outer Join APPLY_LEAVE_HOMEWORK_PASS_METHOD_SETTING as hm ON (r.HomeworkDeliverMethodID = hm.MethodID)
						$leavetype_join
				Where
						r.IsDeleted = '0'
						And yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
						$cond_startDate
						$cond_keyword
						$cond_approvalStatus
						$cond_documentStatus
				";
		}
		return $sql;
	}
	
	function getApplyLeaveApplicationTimeTypeSelection($id, $name, $selected, $onchange="") {
		global $Lang;
		
		$selectArr = array();
		$selectArr['1'] = $Lang['StudentAttendance']['ApplyLeaveAry']['AllApplicationTimeRecord'];
		$selectArr['2'] = $Lang['StudentAttendance']['ApplyLeaveAry']['PastApplicationTimeRecord'];
		$selectArr['3'] = $Lang['StudentAttendance']['ApplyLeaveAry']['TodayApplicationTimeRecord'];
		
		$onchangeAttr = '';
		if ($onchange != "")
			$onchangeAttr = 'onchange="'.$onchange.'"';
		
		$selectionTags = ' id="'.$id.'" name="'.$name.'" '.$onchangeAttr;
		
		return getSelectByAssoArray($selectArr, $selectionTags, $selected, $all=0, $noFirst=1);
	}
	
	function getApplyLeaveAcknowledgmentStatusSelection($id, $name, $selected, $onchange="") {
		global $Lang;
		
		$selectArr = array();
		$selectArr['1'] = $Lang['StudentAttendance']['ApplyLeaveAry']['AllAcknowledgeStatus'];
		$selectArr['2'] = $Lang['StudentAttendance']['ApplyLeaveAry']['PendingAcknowledgeLeave'];
		$selectArr['3'] = $Lang['StudentAttendance']['ApplyLeaveAry']['AcknowledgedLeave'];
		$selectArr['4'] = $Lang['StudentAttendance']['ApplyLeaveAry']['RejectedLeave'];
		$selectArr['5'] = $Lang['StudentAttendance']['ApplyLeaveAry']['CancelledLeave'];
		
		$onchangeAttr = '';
		if ($onchange != "")
			$onchangeAttr = 'onchange="'.$onchange.'"';
		
		$selectionTags = ' id="'.$id.'" name="'.$name.'" '.$onchangeAttr;
		
		return getSelectByAssoArray($selectArr, $selectionTags, $selected, $all=0, $noFirst=1);
	}
	
	function getApplyLeaveDocumentStatusSelection($id, $name, $selected, $onchange="") {
		global $Lang;
		
		$selectArr = array();
		$selectArr['1'] = $Lang['StudentAttendance']['ApplyLeaveAry']['AllDocumentStatus'];
		$selectArr['2'] = $Lang['StudentAttendance']['ApplyLeaveAry']['NoDocument'];
		$selectArr['3'] = $Lang['StudentAttendance']['ApplyLeaveAry']['PendingDocument'];
		$selectArr['4'] = $Lang['StudentAttendance']['ApplyLeaveAry']['ApprovedDocument'];
		$selectArr['5'] = $Lang['StudentAttendance']['ApplyLeaveAry']['RejectedDocument'];
		
		$onchangeAttr = '';
		if ($onchange != "")
			$onchangeAttr = 'onchange="'.$onchange.'"';
		
		$selectionTags = ' id="'.$id.'" name="'.$name.'" '.$onchangeAttr;
		
		return getSelectByAssoArray($selectArr, $selectionTags, $selected, $all=0, $noFirst=1);
		
	}
	
	function getDocumentAry($leaveRecordIdAry='', $fileIdAry='') {
		if ($leaveRecordIdAry !== '') {
			$conds_leaveRecordId = " AND LeaveRecordID IN ('".implode("','", (array)$leaveRecordIdAry)."') ";
		}
		if ($fileIdAry !== '') {
			$conds_fileId = " AND FileID IN ('".implode("','", (array)$fileIdAry)."') ";
		}
		
		$sql = "Select FileID, LeaveRecordID, FilePath, FileName From CARD_STUDENT_APPLY_LEAVE_FILE Where 1 $conds_leaveRecordId $conds_fileId ";
		return $this->returnResultSet($sql);
	}
	
	function getDocumentFilePathBase() {
		global $PATH_WRT_ROOT, $intranet_db;
		
		if (isKIS()) {
			$filePath = $PATH_WRT_ROOT.'../'.$intranet_db.'data';
		}
		else {
			$filePath = $PATH_WRT_ROOT.'../intranetdata';
		}
		
		return $filePath;
	}
	
	function getDocumentImageHtml($recordId) {
		global $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$lfs = new libfilesystem();
		
		$filePathBase = $this->getDocumentFilePathBase();
		$fileAry = $this->getDocumentAry($recordId);
		$numOfFile = count($fileAry);
		
		$returnAry = array();
		for ($i=0; $i<$numOfFile; $i++) {
			$_filePath = $fileAry[$i]['FilePath'];
			
			$_fileExt = get_file_ext($_filePath);
			$_fullFilePath = $filePathBase.$_filePath;
			
			$_uri_data = 'data:image/'.$_fileExt.';base64,';
			if(file_exists($_fullFilePath)) {
				$_base64content = base64_encode($lfs->file_read($_fullFilePath));
				$_uri_data = $_uri_data.$_base64content;
				//$_imgTab = '<img style="width:300px;" src="'.$_uri_data.'">';
				$_imgTab = '<img style="width:100%;" src="'.$_uri_data.'">';
				
				$_tmpAry = array();
				$_tmpAry['imageHtml'] = $_imgTab;
				
				$returnAry[] = $_tmpAry;
			}
		}
		
		return $returnAry;
	}
	
	function returnFolderDivisionNum($fileId) {
		return ceil($fileId / 1500);
	}
	
	function deleteLeaveRecordAttachement($leaveRecordIdAry, $fileIdAry='', $hardDelete=false) {
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
		include_once($PATH_WRT_ROOT.'includes/liblog.php');
		
		$lfs = new libfilesystem();
		$liblog = new liblog();
		
		$filePathBase = $this->getDocumentFilePathBase();
		
		$fileAry = $this->getDocumentAry($leaveRecordIdAry, $fileIdAry);
		$numOfFile = count($fileAry);
		
		
		$logInfoAry = array();
		for ($i=0; $i<$numOfFile; $i++) {
			$_filePath = $fileAry[$i]['FilePath'];
			
			$_fullFilePath = $filePathBase.$_filePath;
			$_fullFilePathNew = $_fullFilePath.'.'.date('Ymd_His'); 
			
			if ($_filePath!='' && file_exists($_fullFilePath)) {
				if ($hardDelete) {
					if (is_file($_fullFilePath)) {
						$_renameSuccess = $lfs->file_remove($_fullFilePath);
					}
				}
				else {
					$_renameSuccess = $lfs->file_rename($_fullFilePath, $_fullFilePathNew);
				}
				$logInfoAry[] = $liblog->BUILD_DETAIL($fileAry[$i]);
			}
		}
		
		$conds_fileId = '';
		if ($fileIdAry != '') {
			$conds_fileId = " AND FileID In ('".implode("','", (array)$fileIdAry)."') ";
		}
		$sql = "delete from CARD_STUDENT_APPLY_LEAVE_FILE where LeaveRecordID in ('".implode("','", (array)$leaveRecordIdAry)."') $conds_fileId";
		$successAry['Delete_DB_Record'] = $this->db_db_query($sql);
		
		# insert delete log
		$tmpAry = array();
	    $tmpAry['LeaveRecordIdAry'] = $leaveRecordIdAry;
	    $tmpAry['FileIDAry'] = $fileIdAry;
	    $tmpAry['FileInfoAry'] = implode('|||', $logInfoAry);
	    $successAry['Log_Delete'] = $liblog->INSERT_LOG($this->ModuleName, 'Delete_Attachment', $liblog->BUILD_DETAIL($tmpAry), 'CARD_STUDENT_APPLY_LEAVE_FILE', '');
		
		return !in_array(false, (array)$successAry);
	}
}
?>