<?php

// Modifing by Max

if (!defined("LIBWORDTEMPLATES_IPF_DEFINED"))         // Preprocessor directives
{


define("LIBWORDTEMPLATES_IPF_DEFINED",true);

class libwordtemplates_ipf extends libfilesystem{
	var $file_array;
	var $path;
	var $defaults;

	/*
	* $ParArrType = 0 : original
	*               1 : iportfolio word template
	*/
	function libwordtemplates_ipf($ParArrType=0)
	{
		global $ec_wordtemplates, $file_path, $ck_course_id, $plugin, $intranet_root;
		$this->libfilesystem();
		
		if ($ParArrType == 0)
		{
			$this->path = $intranet_root."/file/".classNamingDB($ck_course_id)."/templates";		
	
			$this->file_array[] = array(1, $ec_wordtemplates['grading_comment'], "grading_comment.txt");
			if ($plugin['essay_marking'])
			{
				$this->file_array[] = array(null, $ec_wordtemplates['marking_code']);
				$this->file_array[] = array(0, $ec_wordtemplates['marking_code']." (1)", "marking_code.txt");
				$this->file_array[] = array(7, $ec_wordtemplates['marking_code']." (2)", "marking_code2.txt");
				$this->file_array[] = array(8, $ec_wordtemplates['marking_code']." (3)", "marking_code3.txt");
				$this->file_array[] = array(9, $ec_wordtemplates['marking_code']." (4)", "marking_code4.txt");
				$this->file_array[] = array(10, $ec_wordtemplates['marking_code']." (5)", "marking_code5.txt");
	
				$this->file_array[] = array(null, $ec_wordtemplates['marking_scheme']);
				$this->file_array[] = array(2, $ec_wordtemplates['marking_scheme']." (1)", "marking_scheme1.txt");
				$this->file_array[] = array(3, $ec_wordtemplates['marking_scheme']." (2)", "marking_scheme2.txt");
				$this->file_array[] = array(4, $ec_wordtemplates['marking_scheme']." (3)", "marking_scheme3.txt");
				$this->file_array[] = array(5, $ec_wordtemplates['marking_scheme']." (4)", "marking_scheme4.txt");
				$this->file_array[] = array(6, $ec_wordtemplates['marking_scheme']." (5)", "marking_scheme5.txt");
			}
	
			# default presets
			$this->defaults[0] = array("adj", "adv", "art", "binf", "ce", "conj", "ger", "inf", "n", "pastp", "pp", "prep", "pron", "t", "v", "vo", "ww", "sp", "//", "/");
			
		} else if ($ParArrType == 1) {
			
			global $ec_iPortfolio, $LibPortfolio;
			
			$this->path = $intranet_root."/file/";
			
			$this->file_array[] = array(1, $ec_iPortfolio['type_competition'], "portfolio_olr_competition.txt");
			$this->file_array[] = array(2, $ec_iPortfolio['type_activity'], "portfolio_olr_activity.txt");
			$this->file_array[] = array(3, $ec_iPortfolio['type_course'], "portfolio_olr_course.txt");
			$this->file_array[] = array(4, $ec_iPortfolio['type_service'], "portfolio_olr_service.txt");
			$this->file_array[] = array(5, $ec_iPortfolio['type_award'], "portfolio_olr_award.txt");
			$this->file_array[] = array(6, $ec_iPortfolio['type_others'], "portfolio_olr_others.txt");
			
			# new added category
			$NewCategoryArray = $LibPortfolio->get_OLR_Category(1);
			
			if(!empty($NewCategoryArray))
			{
				foreach($NewCategoryArray as $CategoryID => $CategoryTitle)
				{
					$this->file_array[] = array($CategoryID, $CategoryTitle, "portfolio_olr_".$CategoryID.".txt");
				}
			}

		} 
		else if($ParArrType == 2) {
			global $ec_iPortfolio;
			
			$this->path = $intranet_root."/file/";
			$this->file_array[] = array(0, $ec_iPortfolio['ole_role'], "portfolio_olr_role.txt");
		}
		else if ($ParArrType == 3) {
			
			global $ec_iPortfolio, $lpf, $eclass_filepath;
			
			$this->path = $eclass_filepath."/files/";
			
			/*
			$this->file_array[] = array(1, $ec_iPortfolio['type_competition'], "portfolio_olr_competition.txt");
			$this->file_array[] = array(2, $ec_iPortfolio['type_activity'], "portfolio_olr_activity.txt");
			$this->file_array[] = array(3, $ec_iPortfolio['type_course'], "portfolio_olr_course.txt");
			$this->file_array[] = array(4, $ec_iPortfolio['type_service'], "portfolio_olr_service.txt");
			$this->file_array[] = array(5, $ec_iPortfolio['type_award'], "portfolio_olr_award.txt");
			$this->file_array[] = array(6, $ec_iPortfolio['type_others'], "portfolio_olr_others.txt");
			*/
			
			# new added category
			//$NewCategoryArray = $lpf->get_OLR_Category(1);
//			$NewCategoryArray = $lpf->get_OLR_Category("", true);
			$NewCategoryArray = $lpf->get_OLR_Category();
			
			if(!empty($NewCategoryArray))
			{
				foreach($NewCategoryArray as $CategoryID => $CategoryTitle)
				{
					// check if default category
					if($CategoryID == 1 && $CategoryTitle == $ec_iPortfolio['type_competition'])
					$TmpCategoryID = "competition";
					else if($CategoryID == 2 && $CategoryTitle == $ec_iPortfolio['type_activity'])
					$TmpCategoryID = "activity";
					else if($CategoryID == 3 && $CategoryTitle == $ec_iPortfolio['type_course'])
					$TmpCategoryID = "course";
					else if($CategoryID == 4 && $CategoryTitle == $ec_iPortfolio['type_service'])
					$TmpCategoryID = "service";
					else if($CategoryID == 5 && $CategoryTitle == $ec_iPortfolio['type_award'])
					$TmpCategoryID = "award";
					else if($CategoryID == 6 && $CategoryTitle == $ec_iPortfolio['type_others'])
					$TmpCategoryID = "others";
					else
					$TmpCategoryID = $CategoryID;
					
					
					$this->file_array[] = array($CategoryID, $CategoryTitle, "portfolio_olr_".$TmpCategoryID.".txt");
				}
			} else {
				$this->file_array = array();
			}

		}
		else if($ParArrType == 4) {
			global $ec_iPortfolio, $eclass_filepath;
			
			$this->path = $eclass_filepath."/files/";
			$this->file_array[] = array(0, $ec_iPortfolio['ole_role'], "portfolio_olr_role.txt");
		}
		else if($ParArrType == 5) {
			global $ec_iPortfolio, $eclass_filepath;
			
			$this->path = $eclass_filepath."/files/";
			$this->file_array[] = array(0, $ec_iPortfolio['ole_role'], "portfolio_olr_awards.txt");
		}

		
		# check existence of templates folder
		if (!is_dir($this->path))
		{
			$this->folder_new($this->path);
		}
	}

	function getWordList($type){
		$fileUsed = $this->getFileUsed($type);
		$content = trim($this->file_read($fileUsed));
		if ($content != "")
		{
			$result = explode("\n",$content);
			for ($i=0; $i<sizeof($result); $i++)
			{
				$result[$i] = trim($result[$i]);
			}
			return $result;
		} elseif (!file_exists($fileUsed))
		{
			# return defaults
			return $this->getDefaults($type);
		} else
		{
			return array();
		}
	}


	function getDefaults($type){
		return $this->defaults[$type];
	}


	function getTemplatesContent($type){
		$fileUsed = $this->getFileUsed($type);

		if (!file_exists($fileUsed))
		{
			$defaults = $this->getDefaults($type);
			if (is_array($defaults))
			{
				$rx = implode("\n", $defaults);
			}
		} else
		{
			$rx = get_file_content($this->getFileUsed($type));
		}

		return $rx;
	}


	function updateTemplatesContent($type, $data){
		return $this->file_write($data, $this->getFileUsed($type));
	}


	function getFileUsed($type){
		
		$file_array = $this->file_array;

		for ($i=0; $i<sizeof($file_array); $i++)
		{
			if ($file_array[$i][0]==$type && $file_array[$i][2]!="")
			{
				$fileUsed = $this->path."/".$file_array[$i][2];
				break;
			}
		}

		return $fileUsed;
	}


	function getMarkingCode($code_type)
	{
		return $this->getWordList($code_type);
	}


	function getMarkingScheme($myNo)
	{
		return $this->getWordList($myNo+1);
	}


	function getGradingComment()
	{
		return $this->getWordList(1);
	}
	
	function setFileArr($ParShowAllCat=false)
	{
		global $ec_iPortfolio, $lpf, $eclass_filepath;
		
		$this->path = $eclass_filepath."/files/";
		
		$NewCategoryArray = $lpf->get_OLR_Category(0, $ParShowAllCat);
		
		
		if(!empty($NewCategoryArray))
		{
			unset($this->file_array);
		
			foreach($NewCategoryArray as $CategoryID => $CategoryTitle)
			{
				// check if default category
				if($CategoryID == 1 && $CategoryTitle == $ec_iPortfolio['type_competition'])
				$TmpCategoryID = "competition";
				else if($CategoryID == 2 && $CategoryTitle == $ec_iPortfolio['type_activity'])
				$TmpCategoryID = "activity";
				else if($CategoryID == 3 && $CategoryTitle == $ec_iPortfolio['type_course'])
				$TmpCategoryID = "course";
				else if($CategoryID == 4 && $CategoryTitle == $ec_iPortfolio['type_service'])
				$TmpCategoryID = "service";
				else if($CategoryID == 5 && $CategoryTitle == $ec_iPortfolio['type_award'])
				$TmpCategoryID = "award";
				else if($CategoryID == 6 && $CategoryTitle == $ec_iPortfolio['type_others'])
				$TmpCategoryID = "others";
				else
				$TmpCategoryID = $CategoryID;
				
				
				$this->file_array[] = array($CategoryID, $CategoryTitle, "portfolio_olr_".$TmpCategoryID.".txt");
			}
		}
		
		return $this->file_array;
	}
}


} // End of directives
?>
