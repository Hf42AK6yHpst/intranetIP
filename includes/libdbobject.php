<?
# modifying :  
/*
 * 	Log
 * 	
 * 	2016-01-04 Cameron
 * 		- don't use trim function in comparing array value, change [if trim($objectIdAry) == ""] to [if !$objectIdAry] 
 */

if (!defined("LIBDBOBJECT_DEFINED"))         // Preprocessor directives
{
	define("LIBDBOBJECT_DEFINED", true);

	class libdbobject {
		protected $objDb;
		
		private $tableName;
		private $primaryKeyFieldName;
		private $sourceObjectIdAry;
		private $fieldConfigAry;
		
		private $curObjectId;
		private $objectDataAry;
		
		public function __construct($tableName, $pkFieldName, $fieldConfigAry, $sourceObjectIdAry) {
			global $intranet_db;
			
			$this->objDb = new libdb();
			
			$this->setTableName($tableName);
			$this->setPrimaryKeyFieldName($pkFieldName);
			$this->setFieldConfigAry($fieldConfigAry);
			
			if ($sourceObjectIdAry != null && $sourceObjectIdAry != '') {
				if (!is_array($sourceObjectIdAry)) {
					$sourceObjectIdAry = array($sourceObjectIdAry);
				}
				$this->setSourceObjectIdAry($sourceObjectIdAry);
				$this->setCurObjectId($sourceObjectIdAry[0]);		// default load the first object data into the object
				
				$this->loadBatchRecordFromStorage();
			}
		}
		
				
		################################################################
		################# Get / Set Functions [Start] ################## 
		################################################################
		private function setTableName($val) {
			$this->tableName = $val;
		}
		protected function getTableName() {
			return $this->tableName;
		}
		
		private function setPrimaryKeyFieldName($val) {
			$this->primaryKeyFieldName = $val;
		}
		private function getPrimaryKeyFieldName() {
			return $this->primaryKeyFieldName;
		}
		
		private function setSourceObjectIdAry($arr) {
			$this->sourceObjectIdAry = $arr;
		}
		private function getSourceObjectIdAry() {
			return $this->sourceObjectIdAry;
		}
		
		private function setFieldConfigAry($arr) {
			$this->fieldConfigAry = array();
			
			$numOfField = count($arr);
			for ($i=0; $i<$numOfField; $i++) {
				$_fieldName = $arr[$i][0];
				$_fieldType = $arr[$i][1];
				$_setFunc = $arr[$i][2];
				$_getFunc = $arr[$i][3];
				
				$this->fieldConfigAry[$_fieldName] = array('fieldType' => $_fieldType, 'setFunc' => $_setFunc, 'getFunc' => $_getFunc);
			}
		}
		private function getFieldConfigAry() {
			return $this->fieldConfigAry;
		}
		
		private function setCurObjectId($val) {
			$this->objectId = $val;
		}
		private function getCurObjectId() {
			return $this->objectId;
		}
		
		private function setObjectDataAry($arr) {
			$this->objectDataAry = $arr;
		}
		private function getObjectDataAry($targetObjectId='') {
			if ($targetObjectId === '') {
				return $this->objectDataAry;
			}
			else {
				return $this->objectDataAry[$targetObjectId];
			}
		}
		################################################################
		################## Get / Set Functions [End] ################### 
		################################################################
		
		
		
		private function returnObjectDataDbAry() {
			$fieldConfigAry = $this->getFieldConfigAry();
			$primaryKeyFieldName = $this->getPrimaryKeyFieldName();
			
			$dataAry = array();
			foreach ($fieldConfigAry as $_fieldName => $_fieldInfoAry) {
				if ($_fieldName == $primaryKeyFieldName) {
					continue;
				}
				
				$_fieldType = $_fieldInfoAry['fieldType'];
				$_getFunc = $_fieldInfoAry['getFunc'];
				
				if (method_exists($this, $_getFunc)) {
					// $dataAry["Code"]	= $this->objDb->pack_value($this->getCode(), "str");
					eval('$dataAry["'.$_fieldName.'"] = $this->objDb->pack_value($this->'.$_getFunc.'(), "'.$_fieldType.'");');
				}
			}
			
			return $dataAry;
		}
		
		private function loadBatchRecordFromStorage(){
			$objectIdAry = $this->getSourceObjectIdAry();
//			if( (trim($objectIdAry) == "") || (intval($objectIdAry) < 0)) {
			if( (!$objectIdAry) || (intval($objectIdAry) < 0)) {				
				// do nth
			} else {
				$tableName = $this->getTableName();
				$primaryKeyFieldName = $this->getPrimaryKeyFieldName();
				$sql = "Select * From $tableName Where $primaryKeyFieldName In ('".implode("','", (array)$objectIdAry)."')";
				$this->setObjectDataAry(BuildMultiKeyAssoc($this->objDb->returnResultSet($sql), $primaryKeyFieldName));
				
				$this->loadObjectData($this->getCurObjectId());
			}
		}
		
		private function loadObjectData($targetObjectId) {
			$success = true;
			if( (trim($targetObjectId) == "") || (intval($targetObjectId) < 0)) {
				// do nth
				$success = false;
			} else {
				$fieldConfigAry = $this->getFieldConfigAry();
				$this->setCurObjectId($targetObjectId);
				$dataAry = $this->getObjectDataAry($targetObjectId);
				
				foreach ((array)$fieldConfigAry as $_fieldName => $_fieldConfigAry) {
					$_setFunc = $_fieldConfigAry['setFunc'];
					$_value = $dataAry[$_fieldName];
					
					if (method_exists($this, $_setFunc)) {
						// $this->setCode($_value);
						eval('$this->'.$_setFunc.'($_value);');
					}
				}
			}
			
			return $success;
		}
		
		
		public function save(){
			$objectId = $this->getCurObjectId();
			if( (trim($objectId) == "") || (intval($objectId) < 0)) {
				$resultId = $this->newRecord();
			}
			else{	
				$resultId = $this->updateRecord();
			}

			return $resultId;
		}
		
		
		private function newRecord(){
			$successAry = array();
			
			### do self-defined new record pre-process - usually set InputBy and InputDateField
			$successAry['beforeHandling'] = $this->newRecordBeforeHandling();
			
			### process object data
			$dataAry = $this->returnObjectDataDbAry();
			
			### build insert SQL statement and insert into DB
			$insertedId = false;
			if (count($dataAry) > 0) {
				$infoAry = $this->objDb->concatFieldValueToSqlStr($dataAry);
				$sqlFieldText = $infoAry['sqlField'];
				$sqlValueText = $infoAry['sqlValue'];
				
				$tableName = $this->getTableName();
				$sql = "Insert Into $tableName ($sqlFieldText) Values ($sqlValueText)";
				$successAry['insertRecord'] = $this->objDb->db_db_query($sql);
				if ($successAry['insertRecord']) {
					$insertedId = $this->objDb->db_insert_id();
					$this->setCurObjectId($insertedId);
					$this->setSourceObjectIdAry(array($insertedId));
					$this->loadBatchRecordFromStorage();
				}
			}
			
			### do self-defined new record post-process
			$successAry['afterHandling'] = $this->newRecordAfterHandling();
			
			return (in_array(false, $successAry))? false : $insertedId;
		}
		protected function newRecordBeforeHandling() {
			$this->setInputDate('now()');
			$this->setInputBy($_SESSION['UserID']);
			return true;
		}
		protected function newRecordAfterHandling() {
			return true;
		}
		
		
		private function updateRecord() {
			$successAry = array();
			
			### do self-defined new record pre-process
			$successAry['beforeHandling'] = $this->updateRecordBeforeHandling();
			
			### process object data
			$dataAry = $this->returnObjectDataDbAry();
			
			### build update SQL statement and update the record in DB
			if (count($dataAry) > 0) {
				$objectId = $this->getCurObjectId();
				
				$valueFieldAry = array();
				foreach ((array)$dataAry as $_field => $_value) {
					$valueFieldAry[] = $_field." = ".$_value;
				}
				$valueFieldText = implode(', ', (array)$valueFieldAry);
				
				$tableName = $this->getTableName();
				$primaryKeyFieldName = $this->getPrimaryKeyFieldName();
				$sql = "Update $tableName Set $valueFieldText Where $primaryKeyFieldName = '".$objectId."'";
				$successAry['updateRecord'] = $this->objDb->db_db_query($sql);
				
				if ($successAry['updateRecord']) {
					$this->loadBatchRecordFromStorage();
				}
			}
			
			### do self-defined new record post-process
			$successAry['afterHandling'] = $this->updateRecordAfterHandling();
			
			return (in_array(false, $successAry))? false : $objectId;
		}
		protected function updateRecordBeforeHandling() {
			return true;
		}
		protected function updateRecordAfterHandling() {
			return true;
		}
		
		
		public function deleteRecord() {
			$successAry = array();
			
			### do self-defined new record pre-process
			$successAry['beforeHandling'] = $this->deleteRecordBeforeHandling();
			
			### delete record from DB
			$objectId = $this->getCurObjectId();
			$tableName = $this->getTableName();
			$primaryKeyFieldName = $this->getPrimaryKeyFieldName();
			$sql = "Delete From $tableName Where $primaryKeyFieldName = '".$objectId."'";
			$successAry['deleteRecord'] = $this->objDb->db_db_query($sql);
			
			### do self-defined new record post-process
			$successAry['afterHandling'] = $this->deleteRecordAfterHandling();
			
			return in_array(false, (array)$successAry)? false : true;
		}
		protected function deleteRecordBeforeHandling() {
			return true;
		}
		protected function deleteRecordAfterHandling() {
			return true;
		}
	}
}
?>