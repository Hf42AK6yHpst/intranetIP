<?php
// Editing by 
include_once("json.php");
include_once("libdisciplinev12.php");

class libdiscipline_api_cust extends libdb
{
	var $LibDis = null;
	var $LibJSON = null;
	var $LibAccessRight = null;
	var $LibUserRightTarget = null;
	var $apiMethods = array('testConnection','checkLogin','getUserInfo','getConductCategoryAndItems','getConductRecords','addConductRecord');
	
	function libdiscipline_api_cust() 
	{
		parent::libdb();
	}
	
	function getLibDis()
	{
		if(!$this->LibDis)
		{
			$this->LibDis = new libdisciplinev12();
		}
		
		return $this->LibDis;
	}
	
	function getLibJSON()
	{
		if(!$this->LibJSON){
			$this->LibJSON = new JSON_obj();
		}
		
		return $this->LibJSON;
	}
	
	function getLibAccessRight()
	{
		global $intranet_root;
		if(!$this->LibAccessRight)
		{
			include_once($intranet_root."/includes/libaccessright.php");
			$this->LibAccessRight = new libaccessright();
		}
		
		return $this->LibAccessRight;
	}
	
	function getLibUserRightTarget()
	{
		global $intranet_root;
		if(!$this->LibUserRightTarget)
		{
			include_once($intranet_root."/includes/user_right_target.php");
			$this->LibUserRightTarget = new user_right_target();
		}
		
		return $this->LibUserRightTarget;
	}
	
	function getClientIP()
	{
		if(!empty($_SERVER['HTTP_CLIENT_IP'])){
		   $ip = $_SERVER['HTTP_CLIENT_IP'];
		}else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		   $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
		   $ip= $_SERVER['REMOTE_ADDR'];
		}
		
		return $ip;
	}
	
	// eDiscipline does not have allow ip setting, use eAttendance's allow ip list instead
	function isIPAllowed()
	{
	    global $intranet_root, $HTTP_SERVER_VARS;
		
		include_once($intranet_root.'/includes/libgeneralsettings.php');
		$GeneralSetting = new libgeneralsettings();
		$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',array("'TerminalIP'"));
        $ips = $Settings['TerminalIP'];
		
		$clientIP = $this->getClientIP();
		
        $ipa = explode("\n", $ips);
        $ip_ok = false;
        foreach ($ipa as $key => $ip)
        {
              $ip = trim($ip);
              if ($ip == "0.0.0.0")
              {
                  $ip_ok = true;
              }
              else if (testip($ip,$clientIP))
              {
                   $ip_ok = true;
              }
        }
        if (!$ip_ok)
        {
              return false;
        }
        return true;
	}
	
	function discardNumericIndexArrayData(&$ary)
	{
		if(count($ary)>0)
		{
			foreach($ary as $key => $val)
			{
				if(is_numeric($key)){
					unset($ary[$key]);
				}
			}
		}
	}
	
	function testConnection()
	{
		$json = $this->getLibJSON();
		$result = array('connected'=>1);
		$json_data = $json->encode($result);
		return $json_data;
	}
	
	function checkLogin($loginId, $password, $byCard=0)
	{
		global $intranet_root, $PATH_WRT_ROOT, $plugin, $sys_custom, $intranet_authentication_method, $ldap_user_type_mode, $ldap_teacher_base_dn, $ldap_student_base_dn, $ldap_parent_base_dn, $ldap_tcp_dns, $ldap_host, $ldap_port, $SESSION_ACCESS_RIGHT;
		include_once($intranet_root."/includes/libauth.php");
		
		$result = 0;
		
		$sql = "SELECT UserID,UserLogin,RecordType FROM INTRANET_USER WHERE ".($byCard ? " CardID='$loginId' " : " UserLogin='$loginId' ") . " AND RecordStatus='1'";
		$records = $this->returnResultSet($sql);
		
		$user_id = '';
		$userType = '';
		if(count($records)==1){
			$user_id = $records[0]['UserID'];
			$userType = $records[0]['RecordType'];
		}
		
		if(!$byCard && $loginId != '' && in_array($userType,array(1,2)))
		{
			if($intranet_authentication_method == 'LDAP')
			{
				# Different users using different LDAP dn string
			    if ($ldap_user_type_mode)
			    {
			        switch ($userType)
			        {
			                case 1: $special_ldap_dn = $ldap_teacher_base_dn; break;
			                case 2: $special_ldap_dn = $ldap_student_base_dn; break;
			                case 3: $special_ldap_dn = $ldap_parent_base_dn; break;
			                default: $special_ldap_dn = $ldap_teacher_base_dn;
			        }
			    }
			
			    $lldap = new libldap();
			    // find all ldap servers with nslookup
			    if(isset($ldap_tcp_dns) && $ldap_tcp_dns != ''){
			    	$tmp_ldap_hosts = $lldap->getLdapHosts($ldap_tcp_dns);
			    }else{
			    	$tmp_ldap_hosts = array(array($ldap_host, $ldap_port));
			    }
			    $is_ldap_connected = false;
			    // connect to one of the ldap servers that is available 
			    for($i=0;$i<count($tmp_ldap_hosts);$i++){
			    	$lldap->changeToHost($tmp_ldap_hosts[$i][0], $tmp_ldap_hosts[$i][1]);
			    	$is_ldap_connected = $lldap->connect();
			    	if($is_ldap_connected) break;
			    }
			
			    if ($is_ldap_connected && $lldap->validate_try_all_ou($loginId,$password) && $lldap->validate($loginId,$password))
			    {
			    	$result = 1;
			    }
				
			}else{
				$libauth = new libauth();
				$id = $libauth->validate($loginId, $password);
				if($id){
					$result = 1;
				}
			}
		}
		
		// check either is eDiscipline admin or has function access right for good conduct and misconduct management rights
		if($result || $byCard)
		{
			$old_user_id = '';
			if(isset($_SESSION['UserID'])){
				// store current user session data
				$old_user_id = $_SESSION['UserID'];
				$old_user_access_rights = $_SESSION['SSV_USER_ACCESS'];
				$old_access_rights = $_SESSION['SESSION_ACCESS_RIGHT'];
			}
			
			// temporary change to access user
			$_SESSION['UserID'] = $user_id;
			
			if($old_user_id != $user_id)
			{	
				$AccessRight = $this->getLibAccessRight();
				$_SESSION['SESSION_ACCESS_RIGHT'] = $AccessRight->retrieveUserAccessRight($_SESSION['UserID']);
				$SESSION_ACCESS_RIGHT = $_SESSION['SESSION_ACCESS_RIGHT'];
				$UserRightTarget = $this->getLibUserRightTarget();
				$_SESSION['SSV_USER_ACCESS'] = $UserRightTarget->Load_User_Right($_SESSION['UserID']);
			}
			//debug_pr($_SESSION);
			$lib = $this->getLibDis();
			//debug_pr($_SESSION['UserID']);
			//debug_pr($_SESSION['SSV_USER_ACCESS']);
			//debug_pr($SESSION_ACCESS_RIGHT);
			if($lib->IS_ADMIN_USER($_SESSION['UserID'])){
				$result = 1;
			}else if($lib->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-New")){
				$result = 1;
			}else{
				$result = 0;
			}
			
			if($old_user_id != ''){
			// restore user session data
				$_SESSION['UserID'] = $old_user_id;
				$_SESSION['SSV_USER_ACCESS'] = $old_user_access_rights;
				$_SESSION['SESSION_ACCESS_RIGHT'] = $old_access_rights;
			}else{
				// if no user session, unset it
				unset($_SESSION['UserID']);
				unset($_SESSION['SSV_USER_ACCESS']);
				unset($_SESSION['SESSION_ACCESS_RIGHT']);
			}
			//debug_pr($_SESSION['UserID']);
			//debug_pr($_SESSION['SSV_USER_ACCESS']);
		}
		
		$json = $this->getLibJSON();
		$data = array('validated'=>$result);
		$json_data = $json->encode($data);
		return $json_data;
	}
	
	function getUserInfo($Id, $withPhoto=0, $byLogin=0)
	{
		global $intranet_root;
		
		$sql = "SELECT UserID,UserLogin,EnglishName,ChineseName,ClassName,ClassNumber,RecordStatus,RecordType,PhotoLink,CardID FROM INTRANET_USER WHERE ";
		if($byLogin){
			$sql .= " UserLogin='$Id'";
		}else{
			$sql.= " CardID='$Id'";
		}
		$users = $this->returnResultSet($sql);
		
		$default_photo_filepath = $intranet_root."/images/myaccount_personalinfo/samplephoto.gif";
		
		if($withPhoto && count($users)>0){
			for($i=0;$i<count($users);$i++){
				if($users[$i]['PhotoLink']!='' && file_exists($intranet_root.$users[$i]['PhotoLink'])){
					$base64_data = base64_encode(file_get_contents($intranet_root.$users[$i]['PhotoLink']));
					$users[$i]['PhotoData'] = $base64_data;
				}
				/*else if(file_exists($default_photo_filepath)){
					$base64_data = base64_encode(file_get_contents($default_photo_filepath));
					$users[$i]['PhotoData'] = $base64_data;
				}*/
				else{
					$users[$i]['PhotoData'] = '';
				}
			}
		}
		
		$json = $this->getLibJSON();
		$json_data = $json->encode($users);
		return $json_data;
	}
	
	function getConductRecords($studentId,$limit=5)
	{
		global $Lang;
		
		$studentId = IntegerSafe($studentId);
		$currentYearInfo = Get_Current_Academic_Year_ID();
		$picNameField = getNameFieldByLang2("u2.");
		
		$sql = "SELECT 
					a.RecordID,
					a.CategoryID,
					a.ItemID,
					LEFT(a.RecordDate,10) as RecordDate,
					a.StudentID,
					yc.ClassTitleEN,
					yc.ClassTitleB5,
					ycu.ClassNumber,
					c.Name as CategoryName,
					d.Name as ItemName,
					a.PICID,
					$picNameField as PicName,
					a.DateModified,
					a.RecordStatus,
					a.Remark,
					a.ApprovedBy,
					a.ApprovedDate,
					a.WaivedBy,
					a.WaivedDate,
					a.RejectedBy,
					a.RejectedDate 
				FROM DISCIPLINE_ACCU_RECORD as a
				INNER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID AND b.RecordStatus IN (0,1,2))
				INNER JOIN DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
				LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
				INNER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
				INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID AND yc.AcademicYearID=$currentYearInfo) 
				LEFT JOIN INTRANET_USER as u2 ON a.PICID=u2.UserID 
				WHERE a.StudentID='$studentId' AND a.RecordStatus='".DISCIPLINE_STATUS_APPROVED."' AND a.DateInput IS NOT NULL 	
		 	GROUP BY a.RecordID 
			ORDER BY a.RecordDate DESC, a.DateInput DESC  
			LIMIT $limit";
		//debug_pr($sql);
		$records = $this->returnResultSet($sql);
		
		$json = $this->getLibJSON();
		$json_data = $json->encode($records);
		return $json_data;
	}
	
	function getConductCategoryAndItems($accessUserId)
	{
		global $intranet_root, $PATH_WRT_ROOT, $plugin, $sys_custom, $SESSION_ACCESS_RIGHT, $UserID;
		$old_user_id = '';
		if(isset($_SESSION['UserID'])){
			// store current user session data
			$old_user_id = $_SESSION['UserID'];
			$old_user_access_rights = $_SESSION['SSV_USER_ACCESS'];
			$old_access_rights = $_SESSION['SESSION_ACCESS_RIGHT'];
		}
		
		// temporary change to access user
		$_SESSION['UserID'] = $accessUserId;
		$UserID = $accessUserId;
		
		if($old_user_id != $accessUserId)
		{	
			$AccessRight = $this->getLibAccessRight();
			$_SESSION['SESSION_ACCESS_RIGHT'] = $AccessRight->retrieveUserAccessRight($_SESSION['UserID']);
			$SESSION_ACCESS_RIGHT = $_SESSION['SESSION_ACCESS_RIGHT'];
			$UserRightTarget = $this->getLibUserRightTarget();
			$_SESSION['SSV_USER_ACCESS'] = $UserRightTarget->Load_User_Right($UserID);
		}
		
		$lib = $this->getLibDis();
		
		$data = array(GOOD_CONDUCT=>array(),MISCONDUCT=>array());
		
		$good_conducts = $lib->RETRIEVE_CONDUCT_CATEGORY(GOOD_CONDUCT);
		$good_conducts_size = count($good_conducts);
		$misconducts = $lib->RETRIEVE_CONDUCT_CATEGORY(MISCONDUCT);
		$misconducts_size = count($misconducts);
		
		$items = $lib->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT();
		$items_size = count($items);
		
		$catIdToItemAry = array();
		for($i=0;$i<$items_size;$i++)
		{
			$this->discardNumericIndexArrayData($items[$i]);
			if(!isset($catIdToItemAry[$items[$i]['CategoryID']])){
				$catIdToItemAry[$items[$i]['CategoryID']] = array();
			}
			$catIdToItemAry[$items[$i]['CategoryID']][] = array('ItemID'=>$items[$i]['ItemID'],'Name'=>$items[$i]['Name'],'ItemCode'=>$items[$i]['ItemCode']);
		}
		
		for($i=0;$i<$good_conducts_size;$i++){
			$this->discardNumericIndexArrayData($good_conducts[$i]);
			$good_conducts[$i]['Items'] = isset($catIdToItemAry[$good_conducts[$i]['CategoryID']])? $catIdToItemAry[$good_conducts[$i]['CategoryID']] : array();
		}
		
		for($i=0;$i<$misconducts_size;$i++){
			$this->discardNumericIndexArrayData($misconducts[$i]);
			$misconducts[$i]['Items'] = isset($catIdToItemAry[$misconducts[$i]['CategoryID']])? $catIdToItemAry[$misconducts[$i]['CategoryID']] : array();
		}
		
		//debug_pr($good_conducts);
		//debug_pr($items);
		
		$data[GOOD_CONDUCT] = $good_conducts;
		$data[MISCONDUCT] = $misconducts;
		
		$json = $this->getLibJSON();
		$json_data = $json->encode($data);
		
		if($old_user_id != ''){
			// restore user session data
			$_SESSION['UserID'] = $old_user_id;
			$_SESSION['SSV_USER_ACCESS'] = $old_user_access_rights;
			$_SESSION['SESSION_ACCESS_RIGHT'] = $old_access_rights;
		}else{
			// if no user session, unset it
			unset($_SESSION['UserID']);
			unset($_SESSION['SSV_USER_ACCESS']);
			unset($_SESSION['SESSION_ACCESS_RIGHT']);
		}
		
		return $json_data;
	}
	
	function addConductRecord($accessUserId,$studentId,$recordDate,$recordType,$categoryId,$conductItemId)
	{
		global $intranet_root, $PATH_WRT_ROOT, $plugin, $sys_custom, $SESSION_ACCESS_RIGHT, $UserID;
		$old_user_id = '';
		if(isset($_SESSION['UserID'])){
			// store current user session data
			$old_user_id = $_SESSION['UserID'];
			$old_user_access_rights = $_SESSION['SSV_USER_ACCESS'];
			$old_access_rights = $_SESSION['SESSION_ACCESS_RIGHT'];
		}
		
		// temporary change to access user
		$_SESSION['UserID'] = $accessUserId;
		$UserID = $accessUserId;
		
		if($old_user_id != $accessUserId)
		{	
			$AccessRight = $this->getLibAccessRight();
			$_SESSION['SESSION_ACCESS_RIGHT'] = $AccessRight->retrieveUserAccessRight($_SESSION['UserID']);
			$SESSION_ACCESS_RIGHT = $_SESSION['SESSION_ACCESS_RIGHT'];
			$UserRightTarget = $this->getLibUserRightTarget();
			$_SESSION['SSV_USER_ACCESS'] = $UserRightTarget->Load_User_Right($UserID);
		}
		
		$lib = $this->getLibDis();
		$data = array('result'=>0);
		$can_access = 0;
		
		if($lib->IS_ADMIN_USER($_SESSION['UserID'])){
			$can_access = 1;
		}else if($lib->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-New")){
			$can_access = 1;
		}
		
		if($can_access){
			
			$is_exist = 0;
			if($lib->NotAllowSameItemInSameDay) {	# Not allow same student with same item in a same day
				$is_exist = $lib->checkSameGMItemInSameDay($studentId, $categoryId, $conductItemId, $recordDate);
			}
			if(!$is_exist)
			{
				// insert the conduct record 
				$school_year = GET_ACADEMIC_YEAR3($recordDate);
				$semInfo = getAcademicYearAndYearTermByDate($recordDate);
				$semester = $semInfo[1];
				$period_info = $lib->RETRIEVE_TARGET_CATEGORY_PERIOD_SETTING($recordDate,$recordType,$categoryId);
				if(sizeof($period_info)){
					$dataAry = array();
					$dataAry['CategoryID'] = $categoryId;
					$dataAry['ItemID'] = $conductItemId;
					$dataAry['RecordDate'] = $recordDate;
					$dataAry['Remark'] = '';
					$dataAry['PICID'] = $accessUserId;
					$dataAry['RecordType'] = $recordType;
					$dataAry['Year'] = addslashes($school_year);
					$dataAry['Semester'] = addslashes($semester);
					$dataAry['AcademicYearID'] = $lib->getAcademicYearIDByYearName($school_year);
					$dataAry['StudentID'] = $studentId;
					$dataAry['YearTermID'] = $semInfo[0];
					$dataAry['Attachment'] = '';
					//$dataAry['ActionID'] = $thisActionID;
					$dataAry['GMCount'] = 1;
					
					$accu_record_id = $lib->INSERT_MISCONDUCT_RECORD($dataAry);
					if($accu_record_id)
					{
						$lib->REGROUP_CONDUCT_RECORDS($accu_record_id);
						$data['result'] = 1;
					}
				}
			}	
		}
		
		$json = $this->getLibJSON();
		$json_data = $json->encode($data);
		
		if($old_user_id != ''){
			// restore user session data
			$_SESSION['UserID'] = $old_user_id;
			$_SESSION['SSV_USER_ACCESS'] = $old_user_access_rights;
			$_SESSION['SESSION_ACCESS_RIGHT'] = $old_access_rights;
		}else{
			// if no user session, unset it
			unset($_SESSION['UserID']);
			unset($_SESSION['SSV_USER_ACCESS']);
			unset($_SESSION['SESSION_ACCESS_RIGHT']);
		}
		
		return $json_data;
	}
	
}

?>