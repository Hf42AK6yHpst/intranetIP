<?php

class libpf_group {

	protected $db;
	protected $course_db;
	protected $group_id;
	protected $group_name;
	protected $group_desc;
	protected $status;
	protected $has_right;
	
	protected $group_member;
	
	public function __construct(){
		global $eclass_prefix, $ck_course_id;
	

    $this->db = new libdb();
    $this->db->db = $eclass_prefix."c".$ck_course_id;
    $this->group_id = 0;
    $this->group_name = "";
    $this->group_desc = "";
    $this->status = 0;
    $this->has_right = 0;
    
    $this->group_member = array();
  }

##########################################################################
# Get Functions
##########################################################################

  public function getGroupID()
  {
		return $this->group_id;
	}

  public function getGroupName()
  {
		return $this->group_name;
	}
	
  public function getGroupDesc()
  {
		return $this->group_desc;
	}
	
  public function getGroupStatus()
  {
		return $this->status;
	}
	
	public function getGroupHasRight()
	{
		return $this->has_right;
	}
	
	public function getGroupMember()
  {
		return $this->group_member;
	}

##########################################################################
# Set Functions
##########################################################################
  
  public function setGroupID($group_id)
  {
		$this->group_id = $group_id;
	}
  
  public function setGroupName($group_name)
  {
		$this->group_name = $group_name;
	}
	
  public function setGroupDesc($group_desc)
  {
		$this->group_desc = $group_desc;
	}
	
  public function setGroupStatus($status)
  {
		$this->status = $status;
	}
	
	public function setGroupHasRight($has_right)
	{
		$this->has_right = $has_right;
	}
	
  public function setGroupProperty()
  {
    $t_group_id = $this->getGroupID();
    if($t_group_id == "") return false;
    
    $sql =  "
              SELECT DISTINCT
                group_name,
                group_desc,
                Status,
                has_right
              FROM
                grouping
              WHERE
                group_id = {$t_group_id}
            ";

    $db = $this->db;
    $group_arr = current($db->returnArray($sql));
    
    if(!empty($group_arr))
    {
      $this->setGroupName($group_arr["group_name"]);
      $this->setGroupDesc($group_arr["group_desc"]);
      $this->setGroupStatus($group_arr["status"]);
      $this->setGroupHasRight($group_arr["has_right"]);
    }
  }
  
  public function setGroupMember($group_member_arr=null)
  {
    if(!isset($group_member_arr))
    {
	    $t_group_id = $this->getGroupID();
	    if($t_group_id == "") return false;
	    
	    $sql =  "
	              SELECT DISTINCT
	                user_id
	              FROM
	                user_group
	              WHERE
	                group_id = {$t_group_id}
	            ";
	
	    $db = $this->db;
	    $group_member_arr = $db->returnVector($sql);
	  }
	
		$this->group_member = $group_member_arr;
	}
	
##########################################################################
# Manipulation
##########################################################################

  public function ADD_GROUP()
  {
    $t_group_name = mysql_real_escape_string(stripslashes($this->getGroupName()));
    $t_group_desc = mysql_real_escape_string(stripslashes($this->getGroupDesc()));
    $t_group_status = $this->getGroupStatus();
    $t_has_right = $this->getGroupHasRight();
    
    $fields = "(group_name, group_desc, inputdate, modified, Status, has_right)";
    $values = "('{$t_group_name}', '{$t_group_desc}', NOW(), NOW(), {$t_group_status}, {$t_has_right})";
    
    $sql = "INSERT INTO grouping $fields VALUES $values";
    $db = $this->db;
    $returnVal = $db->db_db_query($sql);
    $t_group_id = $db->db_insert_id();
    $this->setGroupID($t_group_id);
    return $returnVal;

  }
  
  public function UPDATE_GROUP()
  {
    $t_group_id = $this->getGroupID();
    if($t_group_id == "") return false;
    
    $t_group_name = mysql_real_escape_string(stripslashes($this->getGroupName()));
    $t_group_desc = mysql_real_escape_string(stripslashes($this->getGroupDesc()));

    $fields = "SET ";
    $fields .= "group_name = '{$t_group_name}', ";
    $fields .= "group_desc = '{$t_group_desc}', ";
		$fields .= "modified = NOW() ";
    
    $sql = "UPDATE grouping $fields WHERE group_id = '{$t_group_id}'";
    $db = $this->db;
    $returnVal = $db->db_db_query($sql);

    return $returnVal;
  }
  
  public function DELETE_GROUP($group_id_arr)
  {
		$group_id_str = is_array($group_id_arr) ? implode(", ", $group_id_arr) : $group_id_arr;
		
		$sql = "DELETE g, ug, mgf ";
		$sql .= "FROM grouping g ";
		$sql .= "LEFT JOIN mgt_grouping_function mgf ON g.group_id = mgf.group_id ";
		$sql .= "LEFT JOIN user_group ug ON g.group_id = ug.group_id ";
		$sql .= "WHERE g.group_id IN (".$group_id_str.")";
    
    $db = $this->db;          
    $returnVal = $db->db_db_query($sql);
    
    return $returnVal;
	}
	
	public function ADD_GROUP_MEMBER()
	{
    $t_group_id = $this->getGroupID();
    if($t_group_id == "") return false;
    
    $group_member_id = $this->getGroupMember();
    
    $field_value = array();
    for($i=0, $i_max=count($group_member_id); $i<$i_max; $i++)
    {
    	$t_uid = $group_member_id[$i];
    
			$field_value[] = "({$t_uid}, {$t_group_id})";
		}
	
		$db = $this->db;
	
		if(count($field_value) > 0)
		{
			$sql = "INSERT IGNORE INTO user_group ";
			$sql .= "(user_id, group_id) VALUES ";
			$sql .= implode(", ", $field_value);
			$db->db_db_query($sql);
		}
	}
	
  public function DELETE_GROUP_MEMBER($teacher_id_arr)
  {
    $t_group_id = $this->getGroupID();
    if($t_group_id == "") return false;
  
		$teacher_id_str = is_array($teacher_id_arr) ? implode(", ", $teacher_id_arr) : $teacher_id_arr;
		
		$sql = "DELETE FROM user_group ";
		$sql .= "WHERE group_id = {$t_group_id} AND user_id IN (".$teacher_id_str.")";
    
    $db = $this->db;          
    $returnVal = $db->db_db_query($sql);
    
    return $returnVal;
	}
	
	/**
	* Get groups which user belongs to
	* @owner : Eric (20101118) , Fai (20110128)
	* @param : Int $ec_uID  User ID of the user (in classroom)
	* @return : Array ID of groups
	*
	*/
	public function GET_USER_IN_GROUP($ec_uID)
	{
		/*
		$sql = "SELECT group_id ";
		$sql .= "FROM user_group ";
		$sql .= "WHERE user_id = {$ec_uID}";
		*/

		// Add inner join t:grouping, since there may be some group_id in t:user_group but there is no related group_id in t:grouping (fai , 20110128)
		$sql  = "SELECT ug.group_id ";
		$sql .= "FROM user_group as ug ";
		$sql .= "inner join grouping g on ug.group_id = g.group_id ";
		$sql .= "WHERE user_id = {$ec_uID}";
		$db = $this->db;
		$group_arr = $db->returnVector($sql);

	
		return $group_arr;
	}
	
	/**
	* Check if the user is group teacher (i.e. belong to any group)
	* @owner : Eric (20101118)
	* @param : Int $ec_uID  User ID of the user (in classroom)
	* @return : Boolean result of checking
	*
	*/
	public function IS_GROUP_TEACHER($ec_uID)
	{
		$group_arr = $this->GET_USER_IN_GROUP($ec_uID);
		
		return count($group_arr) > 0;
	}
}

?>