<?php

/**
 * Provide language translations for student report template.
 * Suppose languages are stored in srt_lang.{$intranet_session_language}.php, which will set the $srt_lang variable.
 * @return the translated value or $s if no translation.
 */
function srt_lang($s)
{
	static $srt_lang;
	
	if (count($srt_lang) == 0) {
		global $intranet_session_language;
		$lang_file = dirname(__FILE__) . "/srt_lang.{$intranet_session_language}.php";
		if (file_exists($lang_file)) {
			include $lang_file;
		}
	}
	
	return $srt_lang[$s] == '' ? $s : $srt_lang[$s];
}

?>
