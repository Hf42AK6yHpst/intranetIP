<?php

/**
 * Provide some user-defined comparison functions for usort().
 * Used by StudentReportDataSourceManager for sorting a set of database records by a field.
 */
class StudentReportDataSorter
{
	/**
	 * Used to identify which field to sort by. A zero-based index.
	 */
	private $field_index;
	
	/**
	 * Indicate whether to sort ascendingly or descendingly.
	 */
	private $is_ascending;
	
	/**
	 * Check the descriptions of the member variables for the meaning of the parameters.
	 */
	function __construct($field_index, $is_ascending) {
		$this->field_index = $field_index;
		$this->is_ascending = $is_ascending;
	}
	
	/**
	 * A user-defined comparison function for usort().
	 * For sorting a set of database records by a field, using a "natural order" algorithm.
	 * This elminates the need to check the type of the field.
	 * But this sort method has a problem that it may sort Chinese first, while string sort puts Chinese last.
	 * @depreciated
	 */
	function sort_records_by_field($a, $b) {
		$a = $a[$this->field_index];
		$b = $b[$this->field_index];
		$sign = $this->is_ascending ? 1 : -1;
		
		if ($a === $b) {
			return 0;
		}
		else if (!isset($a)) {
			return -1 * $sign;
		}
		else if (!isset($b)) {
			return 1 * $sign;
		}
		return strnatcasecmp($a, $b) * $sign;
	}
	
	/**
	 * A user-defined comparison function for usort().
	 * For sorting a set of database records by a numeric field.
	 */
	function sort_records_by_numeric_field($a, $b) {
		$a = $a[$this->field_index];
		$b = $b[$this->field_index];
		$sign = $this->is_ascending ? 1 : -1;
		
		if ($a === $b) {
			return 0;
		}
		else if (!isset($a)) {
			return -1 * $sign;
		}
		else if (!isset($b)) {
			return 1 * $sign;
		}
		return (((float)$a) < ((float)$b) ? -1 : 1) * $sign;
	}
	
	/**
	 * A user-defined comparison function for usort().
	 * For sorting a set of database records by a string field.
	 */
	function sort_records_by_string_field($a, $b) {
		$a = $a[$this->field_index];
		$b = $b[$this->field_index];
		$sign = $this->is_ascending ? 1 : -1;
		
		if ($a === $b) {
			return 0;
		}
		else if (!isset($a)) {
			return -1 * $sign;
		}
		else if (!isset($b)) {
			return 1 * $sign;
		}
		return strcasecmp($a, $b) * $sign;
	}
}

?>
