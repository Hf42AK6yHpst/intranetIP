<?php

include_once 'StudentReportDataSource_Merit.php';

class StudentReportDataSource_MeritOnly extends StudentReportDataSource_Merit
{
	/**
	 * Get the display name of the data source.
	 */
	function get_display_name() {
		return srt_lang('MeritOnly');
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::get_field_information() for the detail description of this method.
	 */
	function get_field_information() {
		return array(
			# Being Henry Yuen (2010-05-06): add fields
			'Year' => array(
				'display_name' => srt_lang('School Year'),
				'is_numeric' => false,
			),
			'Semester' => array(
				'display_name' => srt_lang('Semester'),
				'is_numeric' => false,
			),
			'ClassName' => array(
				'display_name' => srt_lang('Class Name'),
				'is_numeric' => false,
			),
			'RecordType' => array(
				'display_name' => srt_lang('Merit Type'),
				'is_numeric' => false,
			),
			# End Henry Yuen (2010-05-06): add fields
			'MeritDate' => array(
				'display_name' => srt_lang('Merit Merit Date'),
				'is_numeric' => false,
			),
			'NumberOfUnit' => array(
				'display_name' => srt_lang('Merit Number of Unit'),
				'is_numeric' => true,
			),
			'Reason' => array(
				'display_name' => srt_lang('Merit Reason'),
				'is_numeric' => false,
			),
			'PersonInCharge' => array(
				'display_name' => srt_lang('Person in Charge'),
				'is_numeric' => false,
			),
			'Remark' => array(
				'display_name' => srt_lang('Remark'),
				'is_numeric' => false,
			),
		);
	}
	
	/**
	 * See StudentReportDataSource_Merit::get_field_information() for the detail description of this method.
	 */
	protected function displayMeritType() {
		return 'meritOnly';
	}
}

?>
