<?php
#using: 
include_once 'StudentReportDataSourceBase_TableSection.php';
include_once 'srt_lang.php';

/**
 * The data source for accessing data from Award.
 * This data source is supposed to be managed by StudentReportDataSourceManager.
 */
class StudentReportDataSource_Award extends StudentReportDataSourceBase_TableSection
{
	/**
	 * Get the display name of the data source.
	 */
	function get_display_name() {
		return srt_lang('Award');
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::get_field_information() for the detail description of this method.
	 */
	function get_field_information() {
		return array(
			# Being Henry Yuen (2010-05-04): add fields
			'Year' => array(
				'display_name' => srt_lang('School Year'),
				'is_numeric' => false,
			),
			'Semester' => array(
				'display_name' => srt_lang('Semester'),
				'is_numeric' => false,
			),
			# End Henry Yuen (2010-05-04): add fields			
			'AwardDate' => array(
				'display_name' => srt_lang('Award Date'),
				'is_numeric' => false,
			),
			'AwardName' => array(
				'display_name' => srt_lang('Award Name'),
				'is_numeric' => false,
			),
			'Details' => array(
				'display_name' => srt_lang('Details'),
				'is_numeric' => false,
			),
			'Organization' => array(
				'display_name' => srt_lang('Organization'),
				'is_numeric' => false,
			),
			'SubjectArea' => array(
				'display_name' => srt_lang('Subject Area'),
				'is_numeric' => false,
			),
			'Remark' => array(
				'display_name' => srt_lang('Remark'),
				'is_numeric' => false,
			),
		);
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::load_data() for the detail description of this method.
	 */
	function load_data($student_id, $fields, $sort_by_field, $is_ascending, $max_num_of_records, $print_criteria, $filter_criteria, $sort_by_field2, $is_ascending2) {
		// Suppose that StudentReportDataSourceManager::load_data_from_data_source() has validated some of the parameters.
		
		global $eclass_db, $intranet_db,$Lang;
		
		// Transform some fields.
		$field_mapping = array(
			'AwardDate' => "
				{$this->transform_date_field('AwardDate')} AS AwardDate
			",
		);
		$fields = $this->transform_fields_for_loading_data($fields, $field_mapping);
		
		$conds = $this->transform_print_criteria($print_criteria);

		$innerTableOrderBy = ' aw_s.ModifiedDate DESC, aw_s.InputDate DESC ';
		//$outerTableOrderBy =  ($sort_by_field ? ("ORDER BY $sort_by_field " . ($is_ascending ? "ASC" : "DESC")) : "") ;
		$orderBySqlAry = array();
		$orderBySqlAry[] = ($sort_by_field ? (" $sort_by_field " . ($is_ascending ? "ASC" : "DESC")) : "");
		
		if ($sort_by_field2 == "") {
			// don't include in array
		}
		else {
			$orderBySqlAry[] = ($sort_by_field2 ? (" $sort_by_field2 " . ($is_ascending2 ? "ASC" : "DESC")) : "");
		}
		
		if (count($orderBySqlAry) > 0) {
			$outerTableOrderBy = "ORDER BY ".implode(',', (array)$orderBySqlAry);
		}


		if($sort_by_field == 'Year'){
			$orderSeq = ($is_ascending)  ? ' asc ':' desc ';
// 			$addSequence = ',ay.Sequence ';
			$addSequence = ',ayt.TermStart AS Sequence ';
			if(!$max_num_of_records){
// 				$innerTableOrderBy = ' ay.Sequence '.$orderSeq;
				$innerTableOrderBy = ' ayt.TermStart '.$orderSeq; // 2020-05-13 (Philips) [DM#3753]
			} else {
				$innerTableOrderBy = ' AwardDate desc ';
			}
			if (count($orderBySqlAry) > 0) {
				if(!$max_num_of_records){
					$innerTableOrderBy .= ','.implode(',', (array)$orderBySqlAry);
				}
				$outerTableOrderBy = "ORDER BY Sequence $orderSeq, ".implode(',', (array)$orderBySqlAry); // 2020-02-19 (Philips) [2020-0212-0854-20206] Fix: Year Order incorrect
			}
			
			// 			$outerTableOrderBy = '';
		}


		// Select the most recent records temporarily, as selecting records by academic year is not supported yet due to the possible schema changes related to academic year in the near future.
		
		return $this->returnArray("
			SELECT " . join(', ', $fields) . "
			FROM (
				SELECT
					" . Get_Lang_Selection("ay.YearNameB5", "ay.YearNameEN") . " AS Year,
					If (ayt.YearTermID Is Null, 
						if((aw_s.Semester='' OR  aw_s.Semester IS NULL), '".$Lang['General']['WholeYear']."', aw_s.Semester),
						".Get_Lang_Selection("ayt.YearTermNameB5", "ayt.YearTermNameEN")."
					) AS Semester,
					aw_s.AwardDate,
					aw_s.AwardName,
					aw_s.Details,
					aw_s.Organization,
					aw_s.SubjectArea,
					aw_s.Remark
					$addSequence
				FROM 
					{$eclass_db}.AWARD_STUDENT aw_s
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay
          			ON aw_s.AcademicYearID = ay.AcademicYearID
        			Left Join {$intranet_db}.ACADEMIC_YEAR_TERM ayt
          			ON aw_s.YearTermID = ayt.YearTermID
				WHERE aw_s.UserID = '{$student_id}' {$conds}
				ORDER BY 
				$innerTableOrderBy				
				" . ($max_num_of_records ? "LIMIT $max_num_of_records" : "") . "
			) a
			$outerTableOrderBy
		");
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::get_total_count() for the detail description of this method.
	 */
	function get_total_count($student_id, $print_criteria) {
		
		global $eclass_db;
		
		$conds = $this->transform_print_criteria($print_criteria);
		
		return current($this->returnVector("
			SELECT count(*) FROM {$eclass_db}.AWARD_STUDENT aw_s
			WHERE aw_s.UserID = '{$student_id}' {$conds}
		"));
	}
	
	/**
	 * Override method of parent class
	 * @param $print_criteria from load_data().
	 */
	protected function transform_print_criteria($print_criteria) {
	
    $conds = "";
		foreach ($print_criteria as $field_name => &$field_values) {
		  switch($field_name) {
        case "AcademicYearIDs":
          $conds .= empty($field_values) ? " AND 0" : " AND aw_s.AcademicYearID IN (" . join(', ', $field_values) .")";
          break;
        case "YearTermIDs":
          $conds .= empty($field_values) ? "" : " AND aw_s.YearTermID IN (" . join(', ', $field_values) .")"; 
          break;
      }
		}
		return $conds;
  }
}

?>
