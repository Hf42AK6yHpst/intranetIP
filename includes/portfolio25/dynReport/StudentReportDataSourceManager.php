<?php
//	Using By : 
include_once 'StudentReportClassLoader.php';
include_once 'StudentReportDataSourceBase_TableSection.php';
include_once 'StudentReportDataSorter.php';

/**
 * Responsible for instantiating the appropriate data source classes.
 */
class StudentReportDataSourceManager extends StudentReportClassLoader
{
	/**
	 * Setup $member_to_class_map for use by StudentReportClassLoader.
	 * Here, StudentReportClassLoader is used to load the proper data source object.
	 */
	protected $member_to_class_map = array(
		// Data source name => Class for the data source
		// Data source for table section should extend the class StudentReportDataSourceBase_TableSection.
		'OLE' => 'StudentReportDataSource_OLE',
		'PAKP Outside School' => 'StudentReportDataSource_PAKPOutsideSchool',
		'Activity' => 'StudentReportDataSource_Activity',
		'Attendance Record' => 'StudentReportDataSource_AttendanceRecord',
		'Award' => 'StudentReportDataSource_Award',
		'Merit' => 'StudentReportDataSource_Merit',
		'MeritOnly' => 'StudentReportDataSource_MeritOnly',
		'DemeritOnly' => 'StudentReportDataSource_DemeritOnly',
		'Service' => 'StudentReportDataSource_Service',
		// StudentInfo is not really a data source for table sections, but make it autoloaded to simplify instantiation of it.
		'student_info' => 'StudentReportDataSource_StudentInfo',
	);
	
	/**
	 * Get the map from data source to the list of fields provided by the data source for use in template.
	 * @return array (
	 * 		'Data Source 1' => array(Field11, Field12, ...),
	 * 		'Data Source 2' => array(Field21, Field22, ...),
	 * 		...
	 * )
	 */
	function get_data_source_to_fields_map() {
		$result = array();
		foreach ($this->member_to_class_map as $data_source_member => $class) {
			if ($this->$data_source_member instanceof StudentReportDataSourceBase_TableSection) {
				$result[$data_source_member] = $this->$data_source_member->get_fields();
			}
		}
		return $result;
	}
	
	/**
	 * Get the map from data source/field to its display name.
	 * @return array(
	 * 		'Data Source 1' => 'Data Source Display Name 1',
	 * 		'Data Source 2' => 'Data Source Display Name 2',
	 * 		// The key for a field is of the format "$data_source_name.$field_name".
	 * 		'Data Source 1.Field1' => 'Data Source Field Display Name 1',
	 * 		'Data Source 1.Field2' => 'Data Source Field Display Name 2',
	 * )
	 */
	function get_data_source_and_field_display_names() {
		$result = array();
		foreach ($this->get_data_source_to_fields_map() as $data_source => $fields) {
			$result[$data_source] = $this->$data_source->get_display_name();
			foreach ($this->$data_source->get_field_display_names() as $field_name => $display_name) {
				$result["$data_source.$field_name"] = $display_name;
			}
		}
		return $result;
	}
	
	/**
	 * Just load some student data from a data source.
	 * @param $data_source The name of the data source. It is one of the keys of $member_to_class_map.
	 * @param $student_id A student ID. Retrieve the data for the student specified.
	 * @param $fields an array of field names. Used to specify which fields to retrieve.
	 * 		If a field name is "", it will be replaced by NULL.
	 * @param $sort_by_field Sort by the given field, if specified. Should be one of the values in $fields.
	 * @param $is_ascending Indicate whether to sort asc or desc.
	 * @param $max_num_of_records The maximum number of records to retrieve, if specified.
	 * @param $print_criteria The options provided by the user through the Print Report page, like:
	 * 		array(
	 * 			'AcademicYearIDs' => array(...),
	 * 			'YearTermIDs' => array(...),
	 * 		)
	 * @return array(), like those returned by libdb::returnArray().
	 */
	function load_data_from_data_source($data_source, $student_id, $fields, $sort_by_field, $is_ascending, $max_num_of_records, $print_criteria, $filter_criteria, $sort_by_field2, $is_ascending2) {
		if (!$data_source) throw new Exception("The parameter data_source should be given.");
		$data_source_member = $data_source;
		if (!$this->member_to_class_map[$data_source_member]) throw new Exception("The data source '$data_source' is not supported.");
		if (!$this->$data_source_member instanceof StudentReportDataSourceBase_TableSection) throw new Exception("The data source '$data_source' should extend the class StudentReportDataSourceBase_TableSection.");
		if (!$student_id) throw new Exception("The parameter student_id should be given.");
		if (count($fields) == 0) throw new Exception("The parameter fields should be given.");
		// Empty field name is OK too.
		// Try to display the unknown fields, so remove some checks.
		//if (count($unknown_fields = array_diff($fields, $this->$data_source_member->get_fields(), array(''))) > 0) throw new Exception("The following field(s) in the parameter fields are unknown: " . join(', ', $unknown_fields) . ".");
		if ($sort_by_field && !in_array($sort_by_field, $fields)) throw new Exception("The parameter sort_by_field should be one of the values in fields.");
		if ($sort_by_field2 && !in_array($sort_by_field2, $fields)) throw new Exception("The parameter sort_by_field2 should be one of the values in fields.");
		
		// Get the related filter criteria
		$data_source_without_space = $this->get_data_source_name_without_space($data_source);
		$data_source_filter_criteria = $filter_criteria[$data_source_without_space];
		
		return $this->$data_source_member->load_data($student_id, $fields, $sort_by_field, $is_ascending, $max_num_of_records, $print_criteria, $data_source_filter_criteria, $sort_by_field2, $is_ascending2);
	}
	
	/**
	 * Load some student data from several data sources. And merge them.
	 * This function helps to retrieve data for a table section which may contain data from several data sources.
	 * @param $student_id A student ID. Retrieve the data for the student specified.
	 * @param $list_of_data_source_metadata Actually come from StudentReportTemplate, like
	 * 		array(
	 * 			array(
	 * 				'name' => OLE/Attendance Record/Activity/Merit/...,
	 * 				'max_num_of_records' => ?,
	 * 				'fields' => array(
	 * 					field_name1, field_name2, ...
	 * 				)
	 * 			),
	 * 			...
	 * 		)
	 * @param $sort_by_column_number Sort by the given column, if specified. Base index is 1.
	 * @param $is_ascending Indicate whether to sort asc or desc.
	 * @param $print_criteria The options provided by the user through the Print Report page, like:
	 * 		array(
	 * 			'AcademicYearIDs' => array(...),
	 * 			'YearTermIDs' => array(...),
	 * 		)
	 * @return array(), like those returned by libdb::returnArray().
	 */
	function load_data_from_data_sources($student_id, $list_of_data_source_metadata, $sort_by_column_number, $is_ascending, $print_criteria, $filter_criteria, $sort_by_column_number2, $is_ascending2) {
		if (!$student_id) throw new Exception("The parameter student_id should be given.");
		
		$result = array();
		
		if (count($list_of_data_source_metadata) == 0) return $result;
		
		$is_to_sort_alphabetically = false;
				
		foreach ($list_of_data_source_metadata as $data_source) {
			$result = array_merge(
				$result,
				$this->load_data_from_data_source(
					$data_source['name'],
					$student_id,
					$data_source['fields'],
					$sort_by_field = ($sort_by_column_number ? $data_source['fields'][$sort_by_column_number - 1] : null),
					$is_ascending,
					$data_source['max_num_of_records'],
					$print_criteria,
					$filter_criteria,
					$sort_by_field2 = ($sort_by_column_number2 ? $data_source['fields'][$sort_by_column_number2 - 1] : null),
					$is_ascending2
				)
			);
			if ($sort_by_field) {
				// Sort alphabetically if any one of the fields is not a numeric field.
				$is_to_sort_alphabetically |= !$this->{$data_source['name']}->is_field_numeric($sort_by_field);
			}
			if ($sort_by_field2) {
				// Sort alphabetically if any one of the fields is not a numeric field.
				$is_to_sort_alphabetically2 |= !$this->{$data_source['name']}->is_field_numeric($sort_by_field2);
			}
		}
		
		// Sort the results again if there are more than one data sources.
		if ($print_criteria['PrintSelRec']) {
			// skip this sorting if order by student SLP order
		}
		else {
			if ($sort_by_column_number && count($list_of_data_source_metadata) > 1) {
				$sorter = new StudentReportDataSorter($sort_by_column_number - 1, $is_ascending);
				// Sort numerically or alphabetically?
				if ($is_to_sort_alphabetically) {
					usort($result, array($sorter, 'sort_records_by_string_field'));
				}
				else {
					usort($result, array($sorter, 'sort_records_by_numeric_field'));
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * Get the list of data sources which can do filtering.
	 * @return array('OLE' 					=> array( 'Title', 'Role', 'CategoryName', 'ELE' )
	 * 				 'PAKP Outside School' 	=> array (...)
	 * 				)
	 */
	 static function get_criteria_data_sources() {
	 	global $ipf_cfg;
		return $ipf_cfg['dynReport']['CriteriaDataSourceArr'];
	 }
	 
	 static function get_critieria_options() {
	 	global $ipf_cfg;
		return $ipf_cfg['dynReport']['CriteriaArr'];
	 }
	 
	 static function get_criteria_options_display_names() {
	 	global $Lang;
	 	return $Lang['iPortfolio']['DynamicReport']['FilterCriteriaArr'];
	 }
	 
	 static function get_criteria_condition_options() {
	 	global $ipf_cfg;
	 	return $ipf_cfg['dynReport']['CriteriaConditionArr'];
	 }
	 
	 function get_data_source_name_by_object_class($object_class, $without_space) {
	 	$object_class = trim($object_class);
	 	foreach ((array)$this->member_to_class_map as $_data_source => $_object_class) {
	 		if ($_object_class == $object_class) {
	 			return ($without_space)? $this->get_data_source_name_without_space($_data_source) : $_data_source;
	 		}
	 	}
	 	return false;
	 }
	 
	 function get_data_source_name_without_space($data_source) {
	 	return str_replace(' ', '_', $data_source);
	 }
	 
	 static function get_statistics_options_display_names() {
	 	global $Lang;
	 	return $Lang['iPortfolio']['DynamicReport']['StatisticsArr']['StatOptionAry'];
	 }
}

?>
