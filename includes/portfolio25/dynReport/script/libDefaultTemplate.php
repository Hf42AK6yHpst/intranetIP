<?php
class libDefaultTemplate {
	var $latestVersion;
	var $defaultTemplateInfoArr;
	
	public function __construct() {
		$this->objDb = new libdb();
		$this->loadDefaultTemplateInfo();
	}
	
	private function loadDefaultTemplateInfo() {
		global $PATH_WRT_ROOT;
		
		include($PATH_WRT_ROOT.'includes/portfolio25/dynReport/script/data/defaultTemplate.php');
		$iPfDynamicReportDefaultTemplateVersion = ($iPfDynamicReportDefaultTemplateVersion=='')? 0 : $iPfDynamicReportDefaultTemplateVersion;
		$this->setLatestVersion($iPfDynamicReportDefaultTemplateVersion);
		$this->setDefaultTemplateInfoArr($iPfDynamicReportDefaultTemplateInfoArr);
	}
	
	private function setLatestVersion($int) {
		$this->latestVersion = $int;
	}
	public function getLatestVersion() {
		return $this->latestVersion;
	}	
	
	private function setDefaultTemplateInfoArr($arr) {
		$this->defaultTemplateInfoArr = $arr;
	}
	private function getDefaultTemplateInfoArr() {
		return $this->defaultTemplateInfoArr;
	}
	
	public function isLatestVersion() {
		$versionInSystem = $this->getVersionInSystem();
		$latestVersion = $this->getLatestVersion();
		
		return ($versionInSystem >= $latestVersion)? true : false;
	}
	
	public function run() {
		global $eclass_db;
		
		$successArr = array();
		$successArr['updateTemplate'] = $this->updateDefaultTemplateInfoInSystem();
		if ($successArr['updateTemplate']) {
			$successArr['updateVersion'] = $this->updateVersionInSystem();
		}
		
		return (in_array(false, (array)$successArr))? false : true;
	}
	
	private function getVersionInSystem() {
		global $ipf_cfg;
		
		$objIpfSetting = iportfolio_settings::getInstance();
		$versionInSystem = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["dynamicReportDefaultTemplateVersion"]);
		
		return ($versionInSystem=='')? 0 : $versionInSystem;
	}
	
	private function updateVersionInSystem() {
		global $ipf_cfg;
		
		$objIpfSetting = iportfolio_settings::getInstance();
		return $objIpfSetting->updateSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["dynamicReportDefaultTemplateVersion"], $this->getLatestVersion());
	}
	
	private function updateDefaultTemplateInfoInSystem() {
		global $eclass_db;
		
		$student_report_template = $eclass_db.'.student_report_template';
		
		$templateInfoArr = $this->getDefaultTemplateInfoArr();
		$numOfTemplate = count((array)$templateInfoArr);
		for ($i=0; $i<$numOfTemplate; $i++) {
			$_template_code = $this->objDb->Get_Safe_Sql_Query($templateInfoArr[$i]['template_code']);
			$_template_title = $this->objDb->Get_Safe_Sql_Query($templateInfoArr[$i]['template_title']);
			$_data_source_list = $this->objDb->Get_Safe_Sql_Query($templateInfoArr[$i]['data_source_list']);
			$_template_data = $this->objDb->Get_Safe_Sql_Query($templateInfoArr[$i]['template_data']);
			$_is_default = 1;
			
			$sql = "INSERT INTO $student_report_template 
							(template_code, template_title, data_source_list, template_data, is_default, inputdate)
					VALUES 
							('".$_template_code."', '".$_template_title."', '".$_data_source_list."', '".$_template_data."', '".$_is_default."', now())
					ON DUPLICATE KEY UPDATE 
							template_title = '".$_template_title."',
							data_source_list = '".$_data_source_list."',
							template_data = '".$_template_data."',
							modified = now()
					";
			$successArr[$_template_code] = $this->objDb->db_db_query($sql);
		}
		
		return (in_array(false, (array)$successArr))? false : true;
	}
}	
?>