<?php
/*
################# Change Log [Start] #####
#	Date	:	2016-06-07	Ivan [U96863] [ip.2.5.7.7.1]
#				modified load_data to add flag $sys_custom['iPf']['DynamicReport']['ShowMeritRecordsOnly'] to show merit records only
#
#	Date	:	2016-05-10	Omas
#				modified load_data SQL add support to recordtype = 0
#
################## Change Log [End] ######
 */

include_once 'StudentReportDataSourceBase_TableSection.php';
include_once 'srt_lang.php';

# To convert merit count
global $intranet_root;
include_once $intranet_root.'/includes/form_class_manage.php';
include_once $intranet_root.'/includes/portfolio25/libpf-account-student.php';

/**
 * The data source for accessing data from Merit.
 * This data source is supposed to be managed by StudentReportDataSourceManager.
 */
class StudentReportDataSource_Merit extends StudentReportDataSourceBase_TableSection
{
	/**
	 * Get the display name of the data source.
	 */
	function get_display_name() {
		return srt_lang('Merit');
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::get_field_information() for the detail description of this method.
	 */
	function get_field_information() {
		return array(
			# Being Henry Yuen (2010-05-06): add fields
			'Year' => array(
				'display_name' => srt_lang('School Year'),
				'is_numeric' => false,
			),
			'Semester' => array(
				'display_name' => srt_lang('Semester'),
				'is_numeric' => false,
			),
			'ClassName' => array(
				'display_name' => srt_lang('Class Name'),
				'is_numeric' => false,
			),
			'RecordType' => array(
				'display_name' => srt_lang('Record Type'),
				'is_numeric' => false,
			),
			# End Henry Yuen (2010-05-06): add fields
			'MeritDate' => array(
				'display_name' => srt_lang('Merit Date'),
				'is_numeric' => false,
			),
			'NumberOfUnit' => array(
				'display_name' => srt_lang('Number of Unit'),
				'is_numeric' => true,
			),
			'Reason' => array(
				'display_name' => srt_lang('Reason'),
				'is_numeric' => false,
			),
			'PersonInCharge' => array(
				'display_name' => srt_lang('Person in Charge'),
				'is_numeric' => false,
			),
			'Remark' => array(
				'display_name' => srt_lang('Remark'),
				'is_numeric' => false,
			),
		);
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::load_data() for the detail description of this method.
	 */
	function load_data($student_id, $fields, $sort_by_field, $is_ascending, $max_num_of_records, $print_criteria, $filter_criteria, $sort_by_field2, $is_ascending2) {
		// Suppose that StudentReportDataSourceManager::load_data_from_data_source() has validated some of the parameters.
		
		global $eclass_db,$Lang,$sys_custom;
		# Being Henry Yuen (2010-05-06): RecordType naming
		global $i_Merit_Warning;
		global $i_Merit_Merit;
		global $i_Merit_MinorCredit;
		global $i_Merit_MajorCredit;
		global $i_Merit_SuperCredit;
		global $i_Merit_UltraCredit;
		global $i_Merit_BlackMark;
		global $i_Merit_MinorDemerit;
		global $i_Merit_MajorDemerit;
		global $i_Merit_SuperDemerit;
		global $i_Merit_UltraDemerit;
		# End Henry Yuen (2010-05-06): RecordType naming
		 
		
		// Transform some fields.
		$field_mapping = array(
			'MeritDate' => "
				{$this->transform_date_field('MeritDate')} AS MeritDate
			",
			# Being Henry Yuen (2010-05-06): add fields 
			'RecordType' => "
				(CASE RecordType
				WHEN 0 THEN '$i_Merit_Warning'
				WHEN 1 THEN '$i_Merit_Merit'
				WHEN 2 THEN '$i_Merit_MinorCredit'
				WHEN 3 THEN '$i_Merit_MajorCredit'
				WHEN 4 THEN '$i_Merit_SuperCredit'
				WHEN 5 THEN '$i_Merit_UltraCredit'
				WHEN -1 THEN '$i_Merit_BlackMark'
				WHEN -2 THEN '$i_Merit_MinorDemerit'
				WHEN -3 THEN '$i_Merit_MajorDemerit'
				WHEN -4 THEN '$i_Merit_SuperDemerit'
				WHEN -5 THEN '$i_Merit_UltraDemerit'
				WHEN -9 THEN '--'
				ELSE RecordType END) AS RecordType
			",
			# End Henry Yuen (2010-05-06): add fields 
		);
		$fields = $this->transform_fields_for_loading_data($fields, $field_mapping);
		
		$conds = $this->transform_print_criteria($print_criteria);
		
		//$outerTableOrderBy = ($sort_by_field ? ("ORDER BY $sort_by_field " . ($is_ascending ? "ASC" : "DESC")) : "");
		$orderBySqlAry = array();
		$orderBySqlAry[] = ($sort_by_field ? (" $sort_by_field " . ($is_ascending ? "ASC" : "DESC")) : "");
		
		if ($sort_by_field2 == "") {
			// don't include in array
		}
		else {
			$orderBySqlAry[] = ($sort_by_field2 ? (" $sort_by_field2 " . ($is_ascending2 ? "ASC" : "DESC")) : "");
		}
		
		if (count($orderBySqlAry) > 0) {
			$outerTableOrderBy = "ORDER BY ".implode(',', (array)$orderBySqlAry);
		}

		$innerTableOrderBy = ' ms.ModifiedDate DESC, ms.InputDate DESC ';


		$orderSeq = ($is_ascending)  ? ' asc ':' desc ';
// 		if ($sort_by_field == 'Year'){
// 			$innerTableOrderBy = ' ay.Sequence '.$orderSeq;
// 			if (count($orderBySqlAry) > 0) {
// 				$innerTableOrderBy .= ','.implode(',', (array)$orderBySqlAry);
// 			}
// 			$outerTableOrderBy = '';
// 		}
		if($sort_by_field == 'Year'){
			$orderSeq = ($is_ascending)  ? ' asc ':' desc ';
// 			$addSequence = ',ay.Sequence ';
			$addSequence = ',ayt.TermStart AS Sequence ';
			if(!$max_num_of_records){
// 				$innerTableOrderBy = ' ay.Sequence '.$orderSeq;
				$innerTableOrderBy = ' ayt.TermStart '.$orderSeq; // 2020-05-13 (Philips) [DM#3753]
			} else {
				$innerTableOrderBy = ' MeritDate desc ';
			}
			if (count($orderBySqlAry) > 0) {
				if(!$max_num_of_records){
					$innerTableOrderBy .= ','.implode(',', (array)$orderBySqlAry);
				}
				$outerTableOrderBy = "ORDER BY Sequence $orderSeq, ".implode(',', (array)$orderBySqlAry); // 2020-04-27 (Philips) [2020-0212-0854-20206] Fix: Year Order incorrect
			}
			
			// 			$outerTableOrderBy = '';
		}
		else if ($sort_by_field == 'RecordType') {
			$innerTableOrderBy = ' MeritFirstOrdering '.$orderSeq.', ms.RecordType '.$orderSeq;
			if (count($orderBySqlAry) > 0) {
				$innerTableOrderBy .= ','.implode(',', (array)$orderBySqlAry);
			}
			$outerTableOrderBy = '';
		}
		
		if (($this->displayMeritType() == 'both' && $sys_custom['iPf']['DynamicReport']['ShowMeritRecordsOnly']) || $this->displayMeritType() == 'meritOnly') {
			$conds .= " And ms.RecordType > 0 ";
		}
		else if ($this->displayMeritType() == 'demeritOnly') {
			$conds .= " And ms.RecordType <= 0 ";
			
			if (isset($sys_custom['iPf']['DynamicReport']['ShowDemeritRecordTypeAry'])) {
				$conds .= " And ms.RecordType In ('".implode("','", (array)$sys_custom['iPf']['DynamicReport']['ShowDemeritRecordTypeAry'])."') ";
			}
		}
		
		// Select the most recent records temporarily, as selecting records by academic year is not supported yet due to the possible schema changes related to academic year in the near future.
		return $this->returnArray("
			SELECT " . join(', ', $fields) . "
			FROM (
				SELECT
					" . Get_Lang_Selection("ay.YearNameB5", "ay.YearNameEN") . " AS Year,
					If (ayt.YearTermID Is Null, 
						if((ms.Semester='' OR  ms.Semester IS NULL), '".$Lang['General']['WholeYear']."', ms.Semester),
						".Get_Lang_Selection("ayt.YearTermNameB5", "ayt.YearTermNameEN")."
					) AS Semester,
					ms.ClassName,
					ms.RecordType,
					ms.MeritDate,
					ms.NumberOfUnit,
					ms.Reason,
					ms.PersonInCharge,
					ms.Remark,
					If (ms.RecordType > 0, 1, 2) as MeritFirstOrdering
					$addSequence
				FROM 
					{$eclass_db}.MERIT_STUDENT ms
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay
          			ON ms.AcademicYearID = ay.AcademicYearID
        			Left Join {$intranet_db}.ACADEMIC_YEAR_TERM ayt
          			ON ms.YearTermID = ayt.YearTermID
				WHERE ms.UserID = '{$student_id}' {$conds}
				ORDER BY 
				$innerTableOrderBy
				" . ($max_num_of_records ? "LIMIT $max_num_of_records" : "") . "
			) a
			$outerTableOrderBy
		");
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::get_total_count() for the detail description of this method.
	 */
	function get_total_count($student_id, $print_criteria) {
		
		global $sys_custom, $eclass_db;
		
		# Eric Yip (20100408): Temporarily for UCCKE only, should be standard deploy
    if($sys_custom['uccke_full_report'])
    {
  		$lpf_acc = new libpf_account_student();
  		$lpf_acc->SET_CLASS_VARIABLE("user_id", $student_id);
  		
  		if(func_num_args() > 2)
  		{
  		  $merit_type = func_get_arg(2);
  		  $merit_type = str_replace(array("d", "m"), array("-", ""), $merit_type);
      }
      
      $t_count = 0;
      if(!empty($print_criteria["YearTermIDs"]))
      {
        $ayt_arr = $print_criteria["YearTermIDs"];
      
        for($i=0; $i<count($ayt_arr); $i++)
        {
          $t_ayt_id = $ayt_arr[$i];
          $layt = new academic_year_term($t_ayt_id);
          
          $t_ay_id = $layt->AcademicYearID;
          $lay = new academic_year($t_ay_id);
          
          $t_ayt_name = $layt->YearTermNameEN;
          $t_ay_name = $lay->YearNameEN;
          
          $t_merit_array = $lpf_acc->GET_CONVERT_MERIT_SUMMARY($t_ay_name, $t_ayt_name);
        
          $t_count += $t_merit_array[$t_ay_name][$t_ayt_name][$merit_type];
        }
        return $t_count;
      }
      else if(!empty($print_criteria["AcademicYearIDs"]))
      {
        $ay_arr = $print_criteria["AcademicYearIDs"];
      
        for($i=0; $i<count($ay_arr); $i++)
        {
          $t_ay_id = $ay_arr[$i];
          $lay = new academic_year($t_ay_id);
          
          $t_ay_name = $lay->YearNameEN;
          
          $t_merit_array = $lpf_acc->GET_CONVERT_MERIT_SUMMARY($t_ay_name);
          
          if(is_array($t_merit_array))
          {
            foreach($t_merit_array[$t_ay_name] AS $_merit_arr)
            {
              $t_count += $_merit_arr[$merit_type];
            }
          }
        }
        return $t_count;
      }
      
/*      
    	foreach ($print_criteria as $field_name => $field_values) {
    	  switch($field_name) {
          case "AcademicYearIDs":
            if(empty($field_values))
            {
              $t_count += 0;
            }
            else
            {
              for($i=0; $i<count($field_values); $i++)
              {
                $t_ay_id = $field_values[$i];
                $lay = new academic_year($t_ay_id);
              
                $t_count += 0;
              }
            }
            $t_count += empty($field_values) ? 0 : " AND AcademicYearID IN (" . join(', ', $field_values) .")";
            break;
          case "YearTermIDs":
            $conds .= empty($field_values) ? "" : " AND YearTermID IN (" . join(', ', $field_values) .")"; 
            break;
        }
    	}
*/
    }
    else
    {
  		$conds = $this->transform_print_criteria($print_criteria);
  		
  		if(func_num_args() > 2)
  		{
  		  $merit_type = func_get_arg(2);
  		  $merit_type = str_replace(array("d", "m"), array("-", ""), $merit_type);
        $conds .= " AND ms.RecordType = ".$merit_type;
      }
    
  		return current($this->returnVector("
  			SELECT IF(SUM(ms.NumberOfUnit) IS NULL, 0, SUM(ms.NumberOfUnit)) FROM {$eclass_db}.MERIT_STUDENT ms
  			WHERE ms.UserID = '{$student_id}' {$conds}
  		"));
    }
	}
	
	/**
	 * Override method of parent class
	 * @param $print_criteria from load_data().
	 */
	protected function transform_print_criteria($print_criteria) {
	
    $conds = "";
		foreach ($print_criteria as $field_name => &$field_values) {
		  switch($field_name) {
        case "AcademicYearIDs":
          $conds .= empty($field_values) ? " AND 0" : " AND ms.AcademicYearID IN (" . join(', ', $field_values) .")";
          break;
        case "YearTermIDs":
          $conds .= empty($field_values) ? "" : " AND ms.YearTermID IN (" . join(', ', $field_values) .")"; 
          break;
      }
		}
		return $conds;
  }
  
  /**
	 * See StudentReportDataSource_Merit::get_field_information() for the detail description of this method.
	 */
	protected function displayMeritType() {
		return 'both';
	}
}

?>
