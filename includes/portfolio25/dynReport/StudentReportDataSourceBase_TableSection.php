<?php

/**
 * Define the base class for the data source for accessing data for the table section in a student report.
 * The data sources extending this class are supposed to be managed by StudentReportDataSourceManager.
 */
abstract class StudentReportDataSourceBase_TableSection extends libdb
{
	/**
	 * Get the display name of the data source.
	 */
	abstract function get_display_name();
	
	/**
	 * Get the information of the fields in the data source.
	 * Sub-classes should implement this method to provide the information.
	 * @return An array of field information:
	 * array(
	 * 		field_name1 => array(
	 * 			'display_name' => field_display_name1,	// The name to be shown in the dropdown list.
	 * 			'is_numeric' => true/false,				// Indicate whether this field is a numeric field.
	 * 			...
	 * 		),
	 * 		field_name2 => array(
	 * 			...
	 * 		),
	 * 		...
	 * )
	 */
	abstract function get_field_information();
	
	/**
	 * Load some data of a student for use in template.
	 * @param $student_id A student ID. Retrieve the data for the student specified.
	 * @param $fields an array of field names. Used to specify which fields to retrieve.
	 * 		If a field is "", you need to return NULL for that column.
	 * @param $sort_by_field Sort by the given field, if specified. Should be one of the values in $fields.
	 * @param $is_ascending Indicate whether to sort asc or desc.
	 * @param $max_num_of_records The maximum number of records to retrieve, if specified.
	 * @param $print_criteria The options provided by the user through the Print Report page, like:
	 * 		array(
	 * 			'AcademicYearIDs' => array(...),
	 * 			'YearTermIDs' => array(...),
	 * 		)
	 * @return array(), like those returned by libdb::returnArray().
	 */
	abstract function load_data($student_id, $fields, $sort_by_field, $is_ascending, $max_num_of_records, $print_criteria, $filter_criteria, $sort_by_field2, $is_ascending2);
	
	/**
	 * Get the total row count of table.
	 * @param $student_id A student ID. Retrieve the data for the student specified.
	 * @param $print_criteria The options provided by the user through the Print Report page, like:
	 * 		array(
	 * 			'AcademicYearIDs' => array(...),
	 * 			'YearTermIDs' => array(...),
	 * 		)    	 
	 */
	abstract function get_total_count($student_id, $print_criteria);
	
	/**
	 * Get the list of the names of the fields available for use in template.
	 */
	function get_fields() {
		return array_keys($this->get_field_information());
	}
	
	/**
	 * Get the map from the field name to its display name.
	 * @return array(
	 * 		field_name1 => field_display_name1,
	 * 		field_name2 => field_display_name2,
	 * 		...
	 * )
	 */
	function get_field_display_names() {
		$result = array();
		foreach ($this->get_field_information() as $field_name => $information) {
			$result[$field_name] = $information['display_name'];
		}
		return $result;
	}
	
	/**
	 * Check whether the type of a field is numeric. This helps to determine whether to sort a field numerically or alphabetically.
	 * As StudentReportDataSourceManager uses "natural order" algorithm to sort the records now, this function is not really needed now.
	 * But as the sort result of the "natural order" algorithm is a bit different from that of string sort, this function may be used if the "natural order" algorithm is not used later.
	 */
	function is_field_numeric($field) {
		$information = $this->get_field_information();
		return $information[$field]['is_numeric'];
	}
	
	/**
	 * Get the data source name from the object class
	 * e.g. return "OLE" for class "StudentReportDataSource_OLE"
	 */
	function get_data_source_name($without_space=1) {
		$StudentReportDataSourceManager = new StudentReportDataSourceManager();
		return $StudentReportDataSourceManager->get_data_source_name_by_object_class(get_class($this), $without_space);
	}
	
	/**
	 * Used by load_data() to transform the fields to the appropriate table columns for loading data from database.
	 * @param $fields an array of field names. Used to specify which fields to retrieve.
	 * 		If a field is "", it will be replaced by NULL, unless overrided by $field_to_table_column_mapping.
	 * 		If a field is not supported, it will be displayed as [field].
	 * @param $field_to_table_column_mapping An array(
	 * 		field_name1 => the actual SQL to use in the SELECT clause,
	 * 		field_name2 => the actual SQL to use in the SELECT clause,
	 * 		...
	 * )
	 * @return $fields where fields appearing in the keys in $field_to_table_column_mapping will be replaced with the corresponding values.
	 */
	protected function transform_fields_for_loading_data($fields, $field_to_table_column_mapping) {
		$field_to_info_map = $this->get_field_information();
		foreach ($fields as &$field) {
			if ($field_to_table_column_mapping[$field] != '') {
				$field = $field_to_table_column_mapping[$field];
			}
			else if ($field == '') {
				$field = 'NULL';
			}
			else if (!array_key_exists($field, $field_to_info_map)) {
				// Just to mark the unsupported field.
				$field = "'[$field]' AS $field";
			}
		}
		return $fields;
	}
	
	/**
	 * Initially created to remove '0000-00-00' from date results.
	 */
	protected function transform_date_field($field) {
		return <<<SQLEND
			(CASE DATE($field)
			WHEN '0000-00-00' THEN NULL
			ELSE DATE($field) END)
SQLEND;
	}
	
	/**
	 * Helper function for load_data, which transform print criteria to part of SQL statement
	 * @param $print_criteria from load_data().
	 */
	protected function transform_print_criteria($print_criteria) {
	
		$conds = "";
		foreach ($print_criteria as $field_name => &$field_values) {
			switch($field_name) {
				case "AcademicYearIDs":
				$conds .= empty($field_values) ? " AND 0" : " AND AcademicYearID IN (" . join(', ', $field_values) .")";
				break;
				case "YearTermIDs":
				$conds .= empty($field_values) ? "" : " AND YearTermID IN (" . join(', ', $field_values) .")"; 
				break;
			}
		}
		return $conds;
	}
	
	protected function transform_filter_criteria($data_field, $db_field, $filter_criteria_arr) {
		global $ipf_cfg;
		
		$data_source = $this->get_data_source_name($without_space=1);
		$data_source_field_key = $data_source.'###'.$data_field;
		
		$criteria_info_arr = $filter_criteria_arr['criteria_info_arr'];
		$criteria_relationship_arr = $filter_criteria_arr['criteria_relationship_arr'];
		$numOfCriteria = count((array)$criteria_info_arr);
		
		
		$conds = '';
		if ($data_source_field_key == 'OLE###CategoryName' || $data_source_field_key == 'PAKP_Outside_School###CategoryName') {
			// Special handling of OLE Category
			$categoryIDArr = Get_Array_By_Key($filter_criteria_arr, 'RecordID');
			
			$libCategory = new Category();
			$CategoryInfoArr = $libCategory->GET_CATEGORY_LIST($ParShowInactive=false);
			
			// Don't set where clause if num of selected Category = num of active Category => to cater no Category OLE
			if (count($categoryIDArr) != count($CategoryInfoArr)) {
				$conds .= " And ".$db_field." In ('".implode("', '", (array)$categoryIDArr)."') ";
			}
		}
		else if ($data_source_field_key == 'OLE###ELE') {
			// Special handling of OLE ELE
			$ELEIDArr = Get_Array_By_Key($filter_criteria_arr, 'RecordID');
			$numOfELE = count($ELEIDArr);
			$find_arr = array('[', ']');
			$replace_arr = array('\\\\[', '\\\\]');
			$ELEIDArr = str_replace($find_arr, $replace_arr, $ELEIDArr);
			
			$lpf_ole = new libpf_ole();
			$ELEInfoArr = $lpf_ole->Get_ELE_Info(array($ipf_cfg["OLE_ELE_RecordStatus"]["Public"], $ipf_cfg["OLE_ELE_RecordStatus"]["DefaultAndPublic"]));
			// Don't set where clause if num of selected ELE = num of active ELE => to cater no ELE OLE
			if ($numOfELE != count($ELEInfoArr)) {
				//Select Distinct(ELE) From OLE_PROGRAM Where ELE REGEXP '\\[PD\\]|\\[8\\]|\\[CS\\]'
				$conds .= " And ".$db_field." REGEXP '".implode('|', (array)$ELEIDArr)."' ";
			}
		}
		else {
			// General Text handling
			$sql_condition = '';
			for ($i=0; $i<$numOfCriteria; $i++) {
				$_criteria = $criteria_info_arr[$i]['criteria']; 
				$_criteria_text = $criteria_info_arr[$i]['criteria_text'];
				
				if ($_criteria == '' || $_criteria == $ipf_cfg['dynReport']['CriteriaArr'][0]['NoCriteria']) {
					// No Criteria
					break;
				}
				else {
					if ($i > 0) {
						$_condition = $criteria_relationship_arr[$i-1]['condition'];
						$sql_condition .= ' '.$_condition.' ';
					}
					
					switch ($_criteria) {
						case $ipf_cfg['dynReport']['CriteriaArr'][1]['TextContains']:
							$sql_condition .= $db_field." Like '%".$this->Get_Safe_Sql_Like_Query($_criteria_text)."%' ";
							break;
						case $ipf_cfg['dynReport']['CriteriaArr'][1]['TextDoesNotContain']:
							$sql_condition .= $db_field." Not Like '%".$this->Get_Safe_Sql_Like_Query($_criteria_text)."%' ";
							break;
						case $ipf_cfg['dynReport']['CriteriaArr'][1]['TextIsExactly']:
							$sql_condition .= $db_field." = '".$this->Get_Safe_Sql_Query($_criteria_text)."' ";
							break;
						case $ipf_cfg['dynReport']['CriteriaArr'][1]['MustHaveText']:
							$sql_condition .= " trim(".$db_field.") <>'' ";
							break;
					}
				}
			}
			
			if ($sql_condition != '') {
				$conds .= " And (".$sql_condition.") ";
			}
		}
		return $conds;
	}
}

?>
