<?php
#Using: 
/* 
 * 2020-07-13 [Philips] [#P189301]
 * - modified DetectLastSectionOnNextPage, $Count missing, modified getY() > 180 to getY() > 175
 * 2019-07-04 [Anna] [#P162787]
 * - added $sys_custom['iPf']['DynamicReport']['DetectLastSectionOnNextPage']
 * 2018-12-06 [Anna]
 * - added $sys_custom['iPf']['DynamicReport']['TableHeaderTextAlignCenter']
 * 
 * 2017-03-21 [Villa](E114891)
 * - modified generate_report() - cust
 * 
 * 2015-12-10 [Omas] (V90063)
 * - modified generate_report() - fixed cannot initialize first student margin problem
 * 
 * 2013-12-20 [Ivan] (L57030)
 * 	- improved to cater the case that have student info in footer
 * 
 * 2013-01-08 [Ivan]
 * 	- added total hours statistics for table section
 */

include_once 'StudentReportTemplate.php';
include_once 'StudentReportDataSourceManager.php';
include_once 'StudentReportGenerator.php';
include_once 'StudentReportPDFGenerator_TCPDF.php';

/**
 * Responsible for outputting PDF, given a report template and a set of students.
 */
class StudentReportPDFGenerator extends StudentReportGenerator
{
	/**
	 * Output the report in PDF format.
	 * @param $report_template An object of StudentReportTemplate.
	 * @param $student_id_list array(student_id1, student_id2, ...)
	 * @param $print_criteria The options provided by the user through the Print Report page, like:
	 * 		array(
	 * 			'AcademicYearIDs' => array(...),
	 * 			'YearTermIDs' => array(...),
	 * 		)
	 * @param $progress_callback The callback for receiving progress information.
	 *   A float number from 0 to 1 will be passed to the callback as the first parameter.
	 * @return PDF content
	 */
	function generate_report($report_template, $student_id_list, $print_criteria, $progress_callback = null, $progress_percentage=null) {
		global $no_record_msg, $sys_custom, $ipf_cfg;
		// TODO: to be completed.
		
		$data_source_manager = new StudentReportDataSourceManager();
		
		$PrintingDirection = $print_criteria['PrintingDirection'];
		$pdf = new StudentReportPDFGenerator_TCPDF($PrintingDirection);
		
		if($report_template->get_header_height() != ''){ 
			$header_height = $report_template->get_header_height();
		}
		else{
			$header_height = PDF_MARGIN_TOP;
		}
		if($report_template->get_footer_height() != ''){ 
			$footer_height = $report_template->get_footer_height();
		}
		else{		
			$footer_height = PDF_MARGIN_FOOTER;
		}		
		
		if(!$report_template->is_to_show_footer_on_last_page_only()){
			$pdf->SetAutoPageBreak(TRUE, $footer_height);
			$pdf->SetFooterMargin($footer_height);				
		}
		else{
			// first page initalize here #V90063 
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_FOOTER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER + 5);
		}			
					
		// Progress
		if (is_callable($progress_callback) && $progress_percentage=='') call_user_func($progress_callback, 0);
		$raw_progress = 0;
		
		$h = 'htmlspecialchars';
		$nb = 'nl2br';
		$t = 'trim';
		$count = 'count';
		
		foreach ($student_id_list as $student_id) {		
			
			# start new page number for each student		
			// $pdf->startPageGroup();		
										
		  // Get student info
			$student_info = $data_source_manager->student_info->load_student_info($student_id, array(), $print_criteria, $ipf_cfg['dynReport']['batchPrintMode']['pdf']);
			
			$pdf->set_is_first_page(true); // this will be set to false in the StudentPDFGenerator_TCPDF class after calling Header()	
						
			// Header and footer...
			// Pass the header and the footer to the PDF object.
			
			$pdf->set_student_report_header(
				$report_template->assign_values_to_template($report_template->get_header_template(), $student_info),
				$report_template->is_to_show_header_on_first_page_only(),
				$header_height
			);				
			
			
			// L57030	
//			$pdf->set_student_report_footer(
//				$report_template->assign_values_to_template($report_template->get_footer_template(), $student_info),
//				$report_template->is_to_show_footer_on_last_page_only(),
//				$footer_height
//			);
			
			
//			// Start a new page for each student.			
			$pdf->AddPage();
			
			// L57030 - add page before set footer to prevent the wrong student info shown in footer
			$pdf->set_student_report_footer(
				$report_template->assign_values_to_template($report_template->get_footer_template(), $student_info),
				$report_template->is_to_show_footer_on_last_page_only(),
				$footer_height
			);
			
			
			#echo '<br>student:'. $student_id;
			
			// Sections...
			foreach ($report_template->get_sections() as $_section_num => $section) {
				if ($section['section_type'] == 'table') {		   
					$table_section = $section['table'];
					$columns = $table_section['columns'];
					$showStatistics = $table_section['statistics_show'];
					$statisticsInfoAry = $table_section['statistics_info'];
				
					
					if (count($table_section['data_sources']) > 0 && count($columns) > 0) {
						$records = $data_source_manager->load_data_from_data_sources($student_id, $table_section['data_sources'], $table_section['sort_by_column_number'], $table_section['is_ascending'], $print_criteria, $table_section['filter_criteria'], $table_section['sort_by_column_number2'], $table_section['is_ascending2']);
						$numOfRecords = count($records);
					
						//if ($sys_custom['iPf']['DynamicReport']['DataSectionApplyConfigFontMapping']) {
							$styleArr = array('section_title_style', 'column_title_style', 'row_text_style');
							for ($i=0, $i_max=count($styleArr); $i<$i_max; $i++) {
								$_style = $styleArr[$i];
								
								$_orgFontFamily = $table_section[$_style]['font_family'];
								$_newFontFamily = $ipf_cfg['dynReport']['fontMappingArr'][$_orgFontFamily];
								
								if ($_newFontFamily != '') {
									$table_section[$_style]['font_family'] = $_newFontFamily;
								}
							}
						//}
						
						if ($print_criteria['ShowNoDataTable'] == 0 && $numOfRecords == 0) {
							// do nth
						}
						else {
							// Section title.
							$pdf->writeHTML(<<<HTMLEND
								<table width="100%" border="{$table_section['border_size']}" style="{$this->generate_css_style($table_section['section_title_style'])}" cellpadding="2">
								<tr><td>{$nb($h($table_section['section_title']))}</td></tr>
								</table>
HTMLEND
								,
								// No new line after section title.
								false
							);
			
							// Henry Yuen (2010-06-30): Section description.						
							if($table_section['section_description'] != ''){
								$pdf->writeHTML(<<<HTMLEND
									<table width="100%" border="{$table_section['border_size']}" style="{$this->generate_css_style($table_section['section_description_style'])}" cellpadding="2">
									<tr><td>{$nb($h($t($table_section['section_description'])))}</td></tr>
									</table>
HTMLEND
									,
									// No new line after section title.
									false
								);
							}
							
							// The table.
							// It seems that border is a boolean value only, i.e., either show or hide the border.
							// TCPDF::SetLineWidth() may be used to set border size, but need to convert the unit of the parameter using TCPDF::getHTMLUnitToUnits().
							// It seems that border color does not work.
							$table_html = <<<HTMLEND
								<table border="{$table_section['border_size']}" cellpadding="2">
									<thead>
										<tr style="{$this->generate_css_style($table_section['column_title_style'])}">
HTMLEND;
						
							foreach ($columns as $column) {
								$width_attribute = $column['width'] ? ('width="' . $column['width'] . '"') : '';
							
								$textAlign_attribute = '';
								if ($sys_custom['iPf']['DynamicReport']['TableSectionTextAlignCenter']) {
									$textAlign_attribute = 'text-align:center;';
								}
								//&& $table_section['data_sources'][0]['name'] == 'OLE'
								if($sys_custom['iPf']['DynamicReport']['TableHeaderTextAlignCenter'] ){							  
								    $textAlign_attribute = 'text-align:center;';
								}
								if($sys_custom['iPf']['DynamicReport']['MCWCust']['TitleInTwoLine']){
									$textAlign_attribute = 'text-align:center;';
									$title = str_replace('##', '<br />', $column['title']);
								}else{
									$title = $h($column['title']);
								}
								$table_html .= <<<HTMLEND
											<th $width_attribute style="{$textAlign_attribute}">{$title}</th>
HTMLEND;
							}
							$table_html .= "
										</tr>
									</thead>
									<tbody>
							";
							if (count($records) > 0) {
								$fields_mapping = $table_section['data_sources'][0]['fields'];
								$_statisticsDataAry = array();
								$_rowCount = 0;
								foreach ($records as $record) {
									$table_html .= <<<HTMLEND
										<tr nobr="true" style="{$this->generate_css_style($table_section['row_text_style'])}">
HTMLEND;
									// As the records may come from several data sources, get the data by numeric index instead of field name.
									for ($i = 0; $i < count($columns); $i++) {
										$width_attribute = $columns[$i]['width'] ? ('width="' . $columns[$i]['width'] . '"') : '';
										
										$textAlign_attribute = '';
										if ($sys_custom['iPf']['DynamicReport']['TableSectionTextAlignCenter']) {
											$textAlign_attribute = 'text-align:center;';
										}
										if(in_array($fields_mapping[$i],(array)$sys_custom['iPf']['DynamicReport']['MCWCust']['CenterColumn']) ){
											$textAlign_attribute = 'text-align:center;';
										}
										$table_html .= <<<HTMLEND
											<td $width_attribute style="{$textAlign_attribute}">{$this->transform_table_cell_data($record[$i], $print_criteria)}</td>
HTMLEND;
										$_statisticsDataAry[$i][$_rowCount] = $record[$i];
									}
									$table_html .= "
										</tr>
									";
									
									$_rowCount++;
								}
								
								// statistics row
								$_tableSourceAry = $table_section['data_sources'];
								$_numOfTableSource = count($_tableSourceAry);
								if ($showStatistics) {
									$table_html .= $this->return_statistics_row_html($statisticsInfoAry, $table_section, $_statisticsDataAry);
								}
							}
							else {
								$table_html .= <<<HTMLEND
										<tr nobr="true" align="center" style="{$this->generate_css_style($table_section['row_text_style'])}">
											<td colspan="{$count($columns)}">{$print_criteria['NoDataTableContent']}</td>
										</tr>
HTMLEND;
							}
							$table_html .= "
									</tbody>
								</table>
							";
							
							$pdf->writeHTML($table_html);
			
							# Henry Yuen (2010-06-30): debug - Footer not shown if table just finished the current page 
							# details: tcpdf will delete the last page automatically if last page contains only <thead> tag
							#$pdf->writeHTML("</br>");	
						}
																	
					}
				}
				else if ($section['section_type'] == 'free_text') {					
					$pdf->writeHTML($report_template->assign_values_to_template($section['free_text']['text_template'], $student_info));	
					
					#TEMP FOR TESTING
					//$free_text = "<font style=\"font-family: stsongstdlight;\">". $report_template->assign_values_to_template($section['free_text']['text_template'], $student_info). "</font>";
					//$pdf->writeHTML($free_text);
					//$free_text = $report_template->assign_values_to_template($section['free_text']['text_template'], $student_info);
					//echo $free_text;
					//$free_text = preg_replace()
					#TEMP FOR TESTING
				}
				else if ($section['section_type'] == 'page_break') {
					$pdf->AddPage();
				}
				else {
					throw new Exception("The section type '{$section['section_type']}' is not supported.");
				}
				
				if($sys_custom['iPf']['DynamicReport']['DetectLastSectionOnNextPage']){
					$Count = $_section_num + 1;
				    if(($Count+1) == count($report_template->get_sections()) && $pdf->GetY()>175){
				        $pdf->AddPage();
				    }
				    
				}	
			}
			$pdf->set_is_last_page(true);
			
			
			// Progress
			$raw_progress++;
			if (is_callable($progress_callback)) {
				$cur_progress_percentage = ($progress_percentage=='')? ($raw_progress / count($student_id_list)) : $progress_percentage;
				//call_user_func($progress_callback, $raw_progress / count($student_id_list));
				call_user_func($progress_callback, $cur_progress_percentage);
			}
		}
		
		// Progress
		if (!$raw_progress && is_callable($progress_callback)) call_user_func($progress_callback, 1);

		return $pdf->Output(null, 'S');
	}
	
	// moved to StudentReportGenerator.php
//	/**
//	 * Transform the data in a table cell:
//	 * - Trim the leading and trailing spaces.
//	 * - nl2br. (after trim, to prevent new line after the last line)
//	 * (Assume that the data has been HTML escaped here? So no need to run htmlspecialchars?)
//	 */
//	protected function transform_table_cell_data($data, $print_criteria=array()) {
//		$EmptyDataSymbol = $print_criteria['EmptyDataSymbol'];
//		
//		$DataDisplay = nl2br(trim($data));
//		if ($DataDisplay == '') {
//			$DataDisplay = $EmptyDataSymbol;
//		}
//		
//		return $DataDisplay;
//		//return nl2br(trim($data));
//	}
//	
//	/**
//	 * Generate CSS style values for the HTML style attribute, given some style information.
//	 * @param $style Actually come from StudentReportTemplate, like
//	 * 		array(
//	 * 			'font_family' => ?,
//	 * 			'font_size' => ?,
//	 * 			'is_bold' => ?,
//	 * 			'is_italic' => ?,
//	 * 			'is_underline' => ?,
//	 * 			'text_color' => ?,
//	 * 			'background_color' => ?
//	 * 		)
//	 * @return CSS style string, like 'background-color:...; font-size:...; ...'.
//	 */
//	protected function generate_css_style($style) {
//		$css = '';
//		if ($v = $style['font_family']) $css .= "font-family:$v;";
//		if ($v = $style['font_size']) $css .= "font-size:$v;";
//		if ($v = $style['is_bold']) $css .= "font-weight:bold;";
//		if ($v = $style['is_italic']) $css .= "font-style:italic;";
//		if ($v = $style['is_underline']) $css .= "text-decoration:underline;";
//		if ($v = $style['text_color']) $css .= "color:$v;";
//		if ($v = $style['background_color']) $css .= "background-color:$v;";
//		return $css;
//	}
}

?>
