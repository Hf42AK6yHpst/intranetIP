<?php
#Using: Henry Yuen

#modifiecations:
# Henry Yuen (2010-07-26): modify function load() to fill template 'inputdate' as well

include_once 'StudentReportTemplate.php';

/**
 * Responsible for adding/updating/removing student report templates.
 */
class StudentReportTemplateManager extends libdb
{
	/**
	 * Load a template, given its ID.
	 * @return StudentReportTemplate, or false if the template does not exist.
	 */
	function load($id) {
		if (!isset($id)) throw new Exception("Template ID should be given.");
		
		global $eclass_db;
		$id = (int)$id;
		
		# Begin Henry Yuen (2010-07-26): get and set template 'inputdate' as well
		$records = $this->returnArray("SELECT template_data, inputdate, is_default FROM {$eclass_db}.student_report_template WHERE template_id = '$id'");
		if (count($records) > 0) {
			$template = new StudentReportTemplate();
			$template->set_data_from_xml($records[0][0]);
			$template->set_inputdate($records[0][1]);
			$template->set_is_default($records[0]['is_default']);
			return $template;
		}
		else {
			return false;
		}
		/*
		$records = $this->returnVector("SELECT template_data FROM {$eclass_db}.student_report_template WHERE template_id = '$id'");
		if (count($records) > 0) {
			$template = new StudentReportTemplate();
			$template->set_data_from_xml($records[0]);
			return $template;
		}
		else {
			return false;
		}
		*/
		# End Henry Yuen (2010-07-26): get and set template 'inputdate' as well
	}
	
	/**
	 * Add a template, given some form data.
	 * @param $data Should be of the format acceptable by StudentReportTemplate::set_data_from_array(). The data should not be escaped.
	 * @return the template ID on success; false on failure.
	 */
	function add_by_form_data($data) {
		$template = new StudentReportTemplate();
		$template->set_data_from_array($data);
		
		global $eclass_db;
		
		$escape = 'mysql_real_escape_string';
		return $this->db_db_query(<<<SQLEND
			insert into {$eclass_db}.student_report_template
			(template_title, data_source_list, template_data, inputdate, modified)
			values
			('{$escape($data['template_title'])}', '{$escape(join(', ', $template->get_data_sources()))}', '{$escape($template->get_data_as_xml())}', now(), now())
SQLEND
		)
		? $this->db_insert_id() : false;
	}
	
	/**
	 * Update a template, given its ID and some form data.
	 * @param $data Should be of the format acceptable by StudentReportTemplate::set_data_from_array(). The data should not be escaped.
	 * @return true/false
	 */
	function update_by_form_data($id, $data) {
		if (!isset($id)) throw new Exception("Template ID should be given.");
		
		global $eclass_db;
		$id = (int)$id;
		
		$template = new StudentReportTemplate();
		$template->set_data_from_array($data);
		
		$escape = 'mysql_real_escape_string';
		return $this->db_db_query(<<<SQLEND
			update {$eclass_db}.student_report_template
			set
				template_title = '{$escape($data['template_title'])}',
				data_source_list = '{$escape(join(', ', $template->get_data_sources()))}',
				-- Order is important for backup to work.
				template_data_bak2 = template_data_bak1,
				template_data_bak1 = template_data,
				template_data = '{$escape($template->get_data_as_xml())}',
				modified = now()
			where template_id = '$id'
SQLEND
		);
	}
	
	/**
	 * Delete a template, given its ID.
	 * @return true/false
	 */
	function delete($id) {
		if (!isset($id)) throw new Exception("Template ID should be given.");
		
		global $eclass_db;
		$id = (int)$id;
		
		return $this->db_db_query("delete from {$eclass_db}.student_report_template where template_id = '$id'");
	}
	
	/**
	 * Get the list of all report templates, for the template list.
	 */
	function get_list_of_all_report_templates($defaultOnly='') {
		global $eclass_db;
		
		if ($defaultOnly === '') {
			// return all templates
		}
		else if ($defaultOnly === true) {
			// return default template only
			$cond_is_default = " And is_default = 1 ";
		}
		else if ($defaultOnly === false) {
			// reutrn non-default template only
			$cond_is_default = " And is_default = 0 ";
		}
		
		return $this->returnArray("SELECT * FROM {$eclass_db}.student_report_template WHERE 1 $cond_is_default ORDER BY template_title");
	}
	
	/**
	 * Get the list of printable report templates, which excludes drafts, if draft is supported.
	 * @return array(
	 * 		array(template_id1, template_title1),
	 * 		array(template_id2, template_title2),
	 * 		...
	 * )
	 */
	function get_list_of_printable_report_templates_as_id_and_name() {
		global $eclass_db;
		
		return $this->returnArray("SELECT template_id, template_title FROM {$eclass_db}.student_report_template ORDER BY template_title");
	}
	
	/**
	 * Get the list of printable report templates with optgroup, which excludes drafts, if draft is supported.
	 * @return $ary[$optgroup_name][$template_id] = $template_title
	 */
	function get_list_of_printable_report_templates_as_id_and_name_with_optgroup() {
		global $eclass_db, $Lang;
		
		$templateInfoArr = $this->returnArray("SELECT template_id, template_title, is_default FROM {$eclass_db}.student_report_template ORDER BY template_title");
		$numOfTemplate = count($templateInfoArr);
		
		$selectArr = array();
		// Initialize first so that default is always at the top of the selection 
		$selectArr[$Lang['General']['Default']] = array();
		$selectArr[$Lang['General']['Custom']] = array();
		for ($i=0; $i<$numOfTemplate; $i++) {
			$_template_id = $templateInfoArr[$i]['template_id'];
			$_template_title = $templateInfoArr[$i]['template_title'];
			$_is_default = $templateInfoArr[$i]['is_default'];
			$_optgroup_name = ($_is_default)? $Lang['General']['Default'] : $Lang['General']['Custom']; 
			
			$selectArr[$_optgroup_name][$_template_id] = $_template_title;
		}
		
		if (count($selectArr[$Lang['General']['Default']]) == 0) {
			unset($selectArr[$Lang['General']['Default']]);
		}
		
		return $selectArr;
	}
}

?>
