<?php
# Using:
#
# Modifications:
#
# Bill	(2020-06-08):	[2020-0608-1457-25235] - fixed self account content display problem
# Philips (2020-03-11): [2020-0310-1155-09066] - avoid line break after space character
# Ivan (2011-12-16): added new member field 'is_default'
# Henry Yuen (2010-07-26): added new member field 'inputdate'
#

/**
 * Represent a student report template. Provide helper methods to access template data.
 */
class StudentReportTemplate
{
	/**
	 * array (
	 * 		'template_title' => ?,
	 * 		'is_to_show_header_on_first_page_only' => ?,
	 * 		'header_template' => ?,
	 * 		'header_height' => ?, // Henry Yuen (2010-04-20)
	 * 		'is_to_show_footer_on_last_page_only' => ?,
	 * 		'footer_template' => ?,
	 * 		'footer_height' => ?, // Henry Yuen (2010-04-20)
	 * 		'sections' => array(
	 * 			array(
	 * 				'section_type' => table/free_text/page_break,
	 * 				'free_text' => array( // For free text section only.
	 * 					'text_template' => ?
	 * 				),
	 * 				'table' => array( // For table section only.
	 * 					'section_title' => ?,
	 * 					'sort_by_column_number' => 1/2/3/...,
	 * 					'is_ascending' => ?,
	 * 					'columns' => array(
	 * 						array(
	 * 							'title' => ?,
	 * 							'width' => ?
	 * 						),
	 * 						...
	 * 					),
	 * 					'data_sources' => array(
	 * 						array(
	 * 							'name' => OLE/Attendance Record/Activity/Merit/...,
	 * 							'max_num_of_records' => ?,
	 * 							'fields' => array(
	 * 								field_name1, field_name2, ...
	 * 							)
	 * 						),
	 * 						...
	 * 					),
	 * 					'filter_criteria' => array (
	 * 						'OLE' => array (
	 * 							'CategoryName' => array (
	 * 								'0' => array (
	 * 									'RecordID' => ?
	 * 								),
	 * 								'1' => array (
	 * 									'RecordID' => ?
	 * 								),
	 * 								...
	 * 							),
	 * 							'ELE' => array (
	 * 								'0' => array (
	 * 									'RecordID' => ?
	 * 								),
	 * 								'1' => array (
	 * 									'RecordID' => ?
	 * 								),
	 * 								...
	 * 							),
	 * 							'Role' => array (
	 * 								'criteria_info_arr' => array (
	 * 									'0' => array (
	 * 										'criteria' => ?,
	 * 										'criteria_text' => ?
	 * 									),
	 * 									'1' => array (
	 * 										'criteria' => ?,
	 * 										'criteria_text' => ?
	 * 									),
	 * 									...
	 * 								),
	 * 								'criteria_relationship_arr' => array (
	 * 									'0' => array (
	 * 										'condition' => ?
	 * 									),
	 * 									...
	 * 								)
	 * 							),
	 * 							'Title' => array (
	 * 								'criteria_info_arr' => array (
	 * 									'0' => array (
	 * 										'criteria' => ?,
	 * 										'criteria_text' => ?
	 * 									),
	 * 									'1' => array (
	 * 										'criteria' => ?,
	 * 										'criteria_text' => ?
	 * 									),
	 * 									...
	 * 								),
	 * 								'criteria_relationship_arr' => array (
	 * 									'0' => array (
	 * 										'condition' => ?
	 * 									),
	 * 									...
	 * 								)
	 * 							)
	 * 						),
	 * 						'PAKP_Outside_School' => array (
	 * 							...
	 * 						)
	 * 					),
	 *
	 * 					// Style information.
	 * 					'border_size' => ?,
	 * 					'border_color' => ?,
	 * 					'num_of_records_to_show_per_row' => 1/2/3,
	 * 					'section_title_style' => array(
	 * 						'font_family' => ?,
	 * 						'font_size' => ?,
	 * 						'is_bold' => ?,
	 * 						'is_italic' => ?,
	 * 						'is_underline' => ?,
	 * 						'text_color' => ?,
	 * 						'background_color' => ?
	 * 					),
	 * 					'column_title_style' => array(
	 * 						'font_family' => ?,
	 * 						'font_size' => ?,
	 * 						'is_bold' => ?,
	 * 						'is_italic' => ?,
	 * 						'is_underline' => ?,
	 * 						'text_color' => ?,
	 * 						'background_color' => ?
	 * 					),
	 * 					'row_text_style' => array(
	 * 						'font_family' => ?,
	 * 						'font_size' => ?,
	 * 						'is_bold' => ?,
	 * 						'is_italic' => ?,
	 * 						'is_underline' => ?,
	 * 						'text_color' => ?,
	 * 						'background_color' => ?
	 * 					)
	 * 				)
	 * 			),
	 * 			...
	 * 		)
	 * )
	 */
	private $data;
	private $inputdate;
	private $is_default;
	
	function set_inputdate($inputdate){
		$this->inputdate = $inputdate;
	}
	
	function get_inputdate() {
		return $this->inputdate;
	}
	
	function set_is_default($int){
		$this->is_default = $int;
	}
	function get_is_default() {
		return $this->is_default;
	}
	
	/**
	 * Set the template data from an array.
	 * @param $data See the description of the member variable $data for the format.
	 */
	function set_data_from_array($data) {
		// Do some checks.
		if (array_key_exists('template_id', $data)) throw new Exception("Template ID should not be set in the template.");
		if (trim($data['template_title']) == '') throw new Exception("Template title should be given.");
		
		if (count($data['sections']) > 0) {
			foreach ($data['sections'] as &$section) {
				switch ($section['section_type']) {
					case 'table':
						if (!array_key_exists('table', $section)) throw new Exception("More information should be given for table section.");
						// At least set the key even if the section title is empty.
						if (!array_key_exists('section_title', $section['table'])) throw new Exception("Section title should be given for table section.");
						// Remove the data sources without name given.
						$data_sources = &$section['table']['data_sources'];
						if (count($data_sources) > 0) {
							for ($data_source_index = count($data_sources) - 1; $data_source_index >= 0; $data_source_index--) {
								if ($data_sources[$data_source_index]['name'] == '') {
									array_splice($data_sources, $data_source_index, 1);
								}
							}
						}
						break;
					case 'free_text':
						if (!array_key_exists('free_text', $section)) throw new Exception("More information should be given for free text section.");
						// At least set the key even if the template is empty.
						if (!array_key_exists('text_template', $section['free_text'])) throw new Exception("Template should be given for free text section.");
						break;
					case 'page_break':
						break;
					default:
						throw new Exception("Unknown section type: '{$section['section_type']}'.");
				}
			}
		}
		
		// OK, set the template data.
		$this->data = $data;
	}
	
	/**
	 * Set the template data from XML.
	 */
	function set_data_from_xml($xml) {
		// Just assume $xml is the string exported by var_export() by now.
		eval('$this->set_data_from_array(' . $xml . ');');
	}
	
	/**
	 * Get the template data.
	 */
	function get_data_as_array() {
		return $this->data;
	}
	
	/**
	 * Convert the template data to XML.
	 */
	function get_data_as_xml() {
		// Just use var_export() by now.
		// It is easier to edit the data exported by var_export() than serialize().
		return var_export($this->data, true);
	}
	
	/**
	 * Get the template title.
	 */
	function get_template_title() {
		return $this->data['template_title'];
	}
	
	/**
	 * Check whether to show header on the first page only.
	 * @return true/false.
	 */
	function is_to_show_header_on_first_page_only() {
		return $this->data['is_to_show_header_on_first_page_only'];
	}
	
	/**
	 * Get the header template, with placeholders.
	 * @return HTML with placeholders.
	 */
	function get_header_template() {
		return $this->data['header_template'];
	}
	
	/**
	 * Check whether to show footer on the last page only.
	 * @return true/false.
	 */
	function is_to_show_footer_on_last_page_only() {
		return $this->data['is_to_show_footer_on_last_page_only'];
	}
	
	/**
	 * Get the footer template, with placeholders.
	 * @return HTML with placeholders.
	 */
	function get_footer_template() {
		return $this->data['footer_template'];
	}
	
	# Begin Henry Yuen (2010-04-20): header/ footer height can be set by user
	function get_header_height(){
		return $this->data['header_height'];
	}
	
	function get_footer_height(){
		return $this->data['footer_height'];
	}
	# End Henry Yuen (2010-04-20): header/ footer height can be set by user
	
	/**
	 * Assign values to placeholders in a template, given a field to value map.
	 * Assume that placeholders are of the format "[[Placeholder1]]".
	 * Keep the placeholders whose values are not set in $field_to_value_map.
	 * @param $tpl a HTML string with placeholders, like that returned by get_header_template().
	 * @param $field_to_value_map Example, array('Placeholder1' => Value1, 'Placeholder2' => Value2).
	 * @return HTML.
	 */
	function assign_values_to_template($tpl, $field_to_value_map) {
		if (count($field_to_value_map) == 0) return $tpl;
		
		$this->assign_values_to_template__preg_replace_callback__field_to_value_map = $field_to_value_map;
		return preg_replace_callback(
				'/\[\[(\w+)\]\]/',
				array($this, 'assign_values_to_template__preg_replace_callback'),
				$tpl
				);
	}
	
	/**
	 * A callback used by assign_values_to_template().
	 * Need to set $assign_values_to_template__preg_replace_callback__field_to_value_map before using the callback.
	 */
	private $assign_values_to_template__preg_replace_callback__field_to_value_map;
	private function assign_values_to_template__preg_replace_callback($matches) {
		$place_holder_name = $matches[1];
		
		if (array_key_exists($place_holder_name, $this->assign_values_to_template__preg_replace_callback__field_to_value_map)) {
			// 2020-03-11 (Philips) [2020-0310-1155-09066] - avoid line break after space character
			$content = $this->assign_values_to_template__preg_replace_callback__field_to_value_map[$place_holder_name];
			if(is_int(strpos($place_holder_name,'SelfAccount'))){
				//$content= str_replace("\n\n",'br/',$content);
                // [2020-0608-1457-25235] disabled as content would be shown out of the box
				//$content = str_replace(' ', '&nbsp;',$content);
				$content = strip_tags($content); // Show the spaicng properly
				$content = str_replace("\n",'<br/>',$content);
			}
			return $content;
		}
		else {
			// No value is available.
			return $matches[0];
		}
	}
	
	/**
	 * Get the list of content sections in a template.
	 * @return See the description for the member variable $data, array(
	 * 		array(
	 * 			'section_type',
	 * 			...
	 * 		),
	 * 		...
	 * )
	 */
	function get_sections() {
		return (count($this->data['sections']) > 0) ? $this->data['sections'] : array();
	}
	
	/**
	 * Get the list of data sources used in the template.
	 * @return array(data_source1, data_source2)
	 */
	function get_data_sources() {
		$result = array();
		if (count($this->data['sections']) > 0) {
			foreach ($this->data['sections'] as $section) {
				if ($section['section_type'] == 'table') {
					if (count($section['table']['data_sources']) > 0) {
						foreach ($section['table']['data_sources'] as $data_source) {
							if ($data_source['name'] != '') $result[] = $data_source['name'];
						}
					}
				}
			}
		}
		$result = array_unique($result);
		sort($result);
		return $result;
	}
}

?>
