<?
## Modifying By: 
//PLEASE DON'T CHANGE THIS FILE VALUE UNLESS YOU KNOW THE EXACTLY THE VALUE
###########Please leave this statement on the top#################
##	Please follow this standard to create configuration variables
##	1) if it is database related config
##	-	$varname["DB_[TABLE_NAME]_[FIELD_NAME]"]["MEANING"]
$ipf_cfg["DB_SELF_ACCOUNT_STUDENT_DefaultSA"]["SLP"] = "SLP";
$ipf_cfg["DB_SELF_ACCOUNT_STUDENT_RecordStatus"]["Normal"] = 0;
$ipf_cfg["DB_SELF_ACCOUNT_STUDENT_RecordStatus"]["Approved"] = 1;


$ipf_cfg["DB_ASSESSMENT_STUDENT_MAIN_RECORD_ComeFrom"]["import"] = 1;
$ipf_cfg["DB_ASSESSMENT_STUDENT_MAIN_RECORD_ComeFrom"]["erc"] = 2;

$ipf_cfg["DB_ASSESSMENT_STUDENT_SUBJECT_RECORD_ComeFrom"]["import"] = 1;
$ipf_cfg["DB_ASSESSMENT_STUDENT_SUBJECT_RECORD_ComeFrom"]["erc"] = 2;
$ipf_cfg["DB_ASSESSMENT_STUDENT_SUBJECT_RECORD_ComeFrom"]["edit"] = 3;      // [2017-1207-0959-51277]

$ipf_cfg["DB_CONDUCT_STUDENT_ComeFrom"]["import"] = 1;
$ipf_cfg["DB_CONDUCT_STUDENT_ComeFrom"]["erc"] = 2;

$ipf_cfg["DB_AWARD_STUDENT_ComeFrom"]["sports"] = 1;

/** the config belows are created before the above standard is set **/
## OLE SETTING: RECORD APPROVAL
$ipf_cfg["RECORD_APPROVAL_SETTING"]["onlyTeachersAdminsWithApprovalRight"] = 0;  // Only class teachers and administrators have approval right
$ipf_cfg["RECORD_APPROVAL_SETTING"]["studentSelectApprovalTeacher"] = 1;  // Allow student to select approval teacher
$ipf_cfg["RECORD_APPROVAL_SETTING"]["noApprovalNeeded"] = 2;  // No Approval Needed

## value of OLE TYPE , store the integer
$ipf_cfg["OLE_TYPE_INT"]["INT"] = 0;  // Internal activity  
$ipf_cfg["OLE_TYPE_INT"]["EXT"] = 1;  // External activity

## value of OLE TYPE , store the string
$ipf_cfg["OLE_TYPE_STR"]["INT"] = "int";  // Internal activity
$ipf_cfg["OLE_TYPE_STR"]["EXT"] = "ext";  // External activity


## Value of session variable for the action on OLE
$ipf_cfg["OLE_ACTION"]["view"] = "v";  // View only
$ipf_cfg["OLE_ACTION"]["full"] = "f";  // Full management

##OLE_PROGRAM_REQUEST is not a table (up to 20110414), this config for indicate the OLE FROM WHERE , used in /includes/portfolio25/slp/libpf-OLETeacherRecord.php
$ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["selfCreated"] = 1;  // the program is created by himself
$ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["fromIndividual"] = 2; // the record is request approved from  individual student
$ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["fromClass"] = 3;    // the record is request approved by his/her class student
$ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["fromGroup"] = 4;   // the record is request approved by his/her group


##TABLE : OLE_PROGRAM
$ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherInput"] = 1;  // TEACHER INPUT FORM THE UI
$ipf_cfg["OLE_PROGRAM_COMEFROM__Lang"][$ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherInput"]] = $ec_iPortfolio['SLP']['InputByTeacher'];
$ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherImport"] = 2; // TEACHER INPUT BY IMPORT
$ipf_cfg["OLE_PROGRAM_COMEFROM__Lang"][$ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherImport"]] = $ec_iPortfolio['SLP']['ImportByTeacher'];
$ipf_cfg["OLE_PROGRAM_COMEFROM"]["studentInput"] = 3;  // STUDENT INPUT FROM UI
$ipf_cfg["OLE_PROGRAM_COMEFROM__Lang"][$ipf_cfg["OLE_PROGRAM_COMEFROM"]["studentInput"]] = $ec_iPortfolio['SLP']['InputByStudent'];
$ipf_cfg["OLE_PROGRAM_COMEFROM"]["enrollmentTransfer"] = 4; //CREATE FROM ENROLLMENT TRANSFER RECORD
$ipf_cfg["OLE_PROGRAM_COMEFROM__Lang"][$ipf_cfg["OLE_PROGRAM_COMEFROM"]["enrollmentTransfer"]] = $ec_iPortfolio['SLP']['EnrollmentTransfer'];



$ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"] = "T"; //CREATE BY TEACHER
$ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"] = "S"; //CREATE BY STUDENT



##TABLE : OLE_STUDENT
$ipf_cfg["OLE_STUDENT_COMEFROM"]["teacherInput"] = 1;  // TEACHER INPUT FORM THE UI
$ipf_cfg["OLE_STUDENT_COMEFROM"]["teacherImport"] = 2;  // TEACHER INPUT FORM THE UI
$ipf_cfg["OLE_STUDENT_COMEFROM"]["studentInput"] = 3;  // STUDENT INPUT FORM THE UI
$ipf_cfg["OLE_STUDENT_COMEFROM"]["enrollmentTransfer"] = 4;  // TEACHER INPUT FORM THE UI

##TABLE : OLE_STUDENT
$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"] = 1;  // this OLE STUDENT is PENDING FOR APPROVE
$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"] = 2;  // this OLE STUDENT is approved
$ipf_cfg["OLE_STUDENT_RecordStatus"]["rejected"] = 3;  // this OLE STUDENT status is rejected
$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"] = 4;  // this OLE STUDENT status is teacherSubmit

##TABLE : OLE_CATEGORY
$ipf_cfg["OLE_CATEGORY_RecordStatus"]["disabled"] = 0;
$ipf_cfg["OLE_CATEGORY_RecordStatus"]["enabled"] = 1;
$ipf_cfg["OLE_CATEGORY_RecordStatus"]["default"] = 2; 


##TABLE : OLE_SUBCATEGORY
$ipf_cfg["OLE_SUBCATEGORY_RecordStatus"]["user_private"] = 0;	//	in-activate subcat. defined by user
$ipf_cfg["OLE_SUBCATEGORY_RecordStatus"]["user_public"] = 1;  //	activate subcat. defined by user
$ipf_cfg["OLE_SUBCATEGORY_RecordStatus"]["default_public"] = 2;  //	activate subcat. defined by system
$ipf_cfg["OLE_SUBCATEGORY_RecordStatus"]["default_private"] = 3;  //	in-activate subcat. defined by system

##TABLE : IPORTFOLIO_SETTING
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpIsAllowed"] = 'studentPrintSlpIsAllowed';	//	Store admin setting , allow student print SLP 
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpIssuesDate"] = 'studentPrintSlpIssuesDate';	//	Store admin setting , issues date for student print SLP by themselves
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpLang"] = 'studentPrintSlpLang';	//	Store admin setting , lang display for student print SLP
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpFormAllowed"] = 'studentPrintSlpFormAllowed';	//	Store admin setting , FORM allowed for student print SLP
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpWithIssuesDate"] = 'studentPrintSlpWithIssuesDate';	//	Store admin setting , FORM allowed for student print SLP
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["dynamicReportDefaultTemplateVersion"] = 'dynamicReportDefaultTemplateVersion';	//	Store the dynamic report default template version
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpWithSchoolHeaderImage"] = 'studentPrintSlpWithSchoolHeaderImage'; // Store SLP setting, allow user to print the School Header Image
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["academicScoreDisplayMode"] = 'academicScoreDisplayMode';   
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["displayFullMark"] = 'displayFullMark';
// store SLP setting, period of allow student to "Set record for SLP report"
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["SetRecordToSlpPeriod"] = 'SetRecordToSlpPeriod';
// store SLP setting, main control display of "Set record for SLP report"  
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["SetRecordToSlpPeriodAllowSubmit"] = 'SetRecordToSlpPeriodAllowSubmit';  
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["selfAccountAllowStudentSubmit"] = 'selfAccountAllowStudentSubmit';	//	store settings, Self account all student submit or not
$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["displayComponentSubject"] = 'displayComponentSubject';

#######################################################################
## Teacher Create OLE Program -> Student page compulsory field (Start)
#######################################################################
##OLE compulsory fields
##	- Please be reminded that values can only be single characters
##		e.g. 1-9, A-Z, a-z
##	- include the lang files before getting the following
##		/home/web/eclass40/intranetIP25/lang/iportfolio_lang.b5.php
##		/home/web/eclass40/intranetIP25/lang/iportfolio_lang.en.php
$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Hours"] = 1;
$ipf_cfg["OLE_PROGRAM_CompulsoryFields__Lang"][$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Hours"]] = $ec_iPortfolio['hours'];

$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["RoleOfParticipation"] = 2;
$ipf_cfg["OLE_PROGRAM_CompulsoryFields__Lang"][$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["RoleOfParticipation"]] = $ec_iPortfolio['ole_role'];

$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Achievement"] = 3;
$ipf_cfg["OLE_PROGRAM_CompulsoryFields__Lang"][$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Achievement"]] = $ec_iPortfolio['achievement'];

$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Attachment"] = 4;
$ipf_cfg["OLE_PROGRAM_CompulsoryFields__Lang"][$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Attachment"]] = $ec_iPortfolio['attachment'];

// $ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Details"] = 5;
// $ipf_cfg["OLE_PROGRAM_CompulsoryFields__Lang"][$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Details"]] = $ec_iPortfolio['details'];
$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["ApprovedBy"] = 5;
$ipf_cfg["OLE_PROGRAM_CompulsoryFields__Lang"][$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["ApprovedBy"]] = $ec_iPortfolio['approved_by'];

$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Details"] = 6;
$ipf_cfg["OLE_PROGRAM_CompulsoryFields__Lang"][$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Details"]] = $ec_iPortfolio['details'];
#######################################################################
## Teacher Create OLE Program -> Student page compulsory field (End)
#######################################################################

#######################################################################
## Student Create OLE Program -> Student page compulsory field (Start)
## Teacher > Setting > SLP > Compulsory field 
#######################################################################
$ipf_cfg["Student_OLE_CompulsoryFields"]["SettingName"] = 'Student_OLE_CompulsoryFields';
$ipf_cfg["Student_OLE_CompulsoryFields"]["Field"][] = 'sub_category';
$ipf_cfg["Student_OLE_CompulsoryFields"]["Field"][] = 'ele';
$ipf_cfg["Student_OLE_CompulsoryFields"]["Field"][] = 'organization';
$ipf_cfg["Student_OLE_CompulsoryFields"]["Field"][] = 'hours';
$ipf_cfg["Student_OLE_CompulsoryFields"]["Field"][] = 'ole_role';
$ipf_cfg["Student_OLE_CompulsoryFields"]["Field"][] = 'achievement';
$ipf_cfg["Student_OLE_CompulsoryFields"]["Field"][] = 'attachment';
$ipf_cfg["Student_OLE_CompulsoryFields"]["Field"][] = 'details';
$ipf_cfg["Student_OLE_CompulsoryFields"]["Field"][] = 'preferred_approver';

$ipf_cfg["Student_OLEExt_CompulsoryFields"]["SettingName"] = 'Student_OLEExt_CompulsoryFields';
$ipf_cfg["Student_OLEExt_CompulsoryFields"]["Field"][] = 'sub_category';
$ipf_cfg["Student_OLEExt_CompulsoryFields"]["Field"][] = 'organization';
$ipf_cfg["Student_OLEExt_CompulsoryFields"]["Field"][] = 'ole_role';
$ipf_cfg["Student_OLEExt_CompulsoryFields"]["Field"][] = 'achievement';
$ipf_cfg["Student_OLEExt_CompulsoryFields"]["Field"][] = 'attachment';
$ipf_cfg["Student_OLEExt_CompulsoryFields"]["Field"][] = 'details';

## Lang e.g. using $ec_iPortfolio['sub_category']
#######################################################################
## Student Create OLE Program -> Student page compulsory field (End)
#######################################################################

##TABLE : OLE_ELE
$ipf_cfg["OLE_ELE_RecordStatus"]["Private"] = 0;
$ipf_cfg["OLE_ELE_RecordStatus"]["Public"] = 1;
$ipf_cfg["OLE_ELE_RecordStatus"]["DefaultAndPublic"] = 2;


/****************************** group functions ******************************/
// student profile
$student_profile_functions[] = array('Profile:Student', $Lang['iPortfolio']['SP_Rights']['management_student']);
$student_profile_functions[] = array('Profile:ImportData', $Lang['iPortfolio']['SP_Rights']['data_import']);
$student_profile_functions[] = array('Profile:ExportData', $Lang['iPortfolio']['SP_Rights']['export_slp_data']);
$student_profile_functions[] = array('Profile:OLR', $Lang['iPortfolio']['SP_Rights']['student_ole_manage']);
$student_profile_functions[] = array('Profile:ReportSetting', $Lang['iPortfolio']['SP_Rights']['student_report_setting']);
$student_profile_functions[] = array('Profile:ReportPrint', $Lang['iPortfolio']['SP_Rights']['student_report_printing']);
$student_profile_functions[] = array('Profile:Stat', $Lang['iPortfolio']['SP_Rights']['assessment_stat_report']);
$student_profile_functions[] = array('Profile:SelfAccount', $ec_iPortfolio['self_account']);
// Customized for Chung Wing Kwong
if($sys_custom['cwk_SLP']) $student_profile_functions[] = array('Profile:TeacherComment', $ec_iPortfolio['teacher_comment']);

# T71071 - request to hide the select all checkebox in OLE Programme list
# bmsa (Profile:DelSelfAccountOnly) => change to short form 
# due to DB field "function_right varchar(255)" character size limitation
//$student_profile_functions[] = array('Profile:DelSelfAccountOnly', $Lang['iPortfolio']['SP_Rights']['self_added_record_delete']);
$student_profile_functions[] = array('bmsa', $Lang['iPortfolio']['SP_Rights']['self_added_record_delete']);

// LP
$sharing_functions[] = array('Sharing:Content', $Lang['iPortfolio']['LP_Rights']['learning_portfolio_content']);
$sharing_functions[] = array('Sharing:Template', $Lang['iPortfolio']['LP_Rights']['manage_templates']);
$sharing_functions[] = array('Sharing:PeerReview', $ec_iPortfolio['peer_review_setting']);
$sharing_functions[] = array('Sharing:CDBurning', $ec_iPortfolio['prepare_cd_burning']);

//JUPAS 
//up to 20110809 , there is only one access role for JUPAS, it may be have more in the future
$jupas_functions[] = array('Jupas:All', $Lang['iPortfolio']['OEA']['JUPAS']);
/****************************** group functions ******************************/


// Dynamic Report
$ipf_cfg['dynReport']['CriteriaDataSourceArr'] = array();
(array)$ipf_cfg['dynReport']['CriteriaDataSourceArr']['OLE']['Title'] = "Title";
(array)$ipf_cfg['dynReport']['CriteriaDataSourceArr']['OLE']['Role'] = "Role";
(array)$ipf_cfg['dynReport']['CriteriaDataSourceArr']['OLE']['CategoryName'] = "CategoryName";
(array)$ipf_cfg['dynReport']['CriteriaDataSourceArr']['OLE']['ELE'] = "ELE";
(array)$ipf_cfg['dynReport']['CriteriaDataSourceArr']['OLE']['Achievement'] = "Achievement";
(array)$ipf_cfg['dynReport']['CriteriaDataSourceArr']['PAKP Outside School']['Title'] = "Title";
(array)$ipf_cfg['dynReport']['CriteriaDataSourceArr']['PAKP Outside School']['Role'] = "Role";
(array)$ipf_cfg['dynReport']['CriteriaDataSourceArr']['PAKP Outside School']['CategoryName'] = "CategoryName";
(array)$ipf_cfg['dynReport']['CriteriaDataSourceArr']['PAKP Outside School']['Achievement'] = "Achievement";


// Criteria Selection Options
$ipf_cfg['dynReport']['CriteriaArr'][0]['NoCriteria'] = 'NoCriteria';
$ipf_cfg['dynReport']['CriteriaArr'][1]['MustHaveText'] = 'MustHaveText';
$ipf_cfg['dynReport']['CriteriaArr'][1]['TextContains'] = 'TextContains';
$ipf_cfg['dynReport']['CriteriaArr'][1]['TextDoesNotContain'] = 'TextDoesNotContain';
$ipf_cfg['dynReport']['CriteriaArr'][1]['TextIsExactly'] = 'TextIsExactly';
//$ipf_cfg['dynReport']['CriteriaArr'][2]['GreaterThan'] = "GreaterThan";
//$ipf_cfg['dynReport']['CriteriaArr'][2]['LessThan'] = "LessThan";

// Criteria Condition Options
$ipf_cfg['dynReport']['CriteriaConditionArr']['and'] = $Lang['General']['And'];
$ipf_cfg['dynReport']['CriteriaConditionArr']['or'] = $Lang['General']['Or'];

// Font mapping
$ipf_cfg['dynReport']['fontMappingArr']['arialunicid0'] = 'droidsansfallback';
$ipf_cfg['dynReport']['fontMappingArr']['arialunicid0-chi'] = 'droidsansfallback';

// Printing direction
$ipf_cfg['dynReport']['printingDirection']['portrait'] = 'P';
$ipf_cfg['dynReport']['printingDirection']['landscape'] = 'L';

// Output mode
$ipf_cfg['dynReport']['batchPrintMode']['all'] = 'all';
$ipf_cfg['dynReport']['batchPrintMode']['zip'] = 'zip';
$ipf_cfg['dynReport']['batchPrintMode_default'] = $ipf_cfg['dynReport']['batchPrintMode']['all'];

// Print mode
$ipf_cfg['dynReport']['batchPrintMode']['pdf'] = 'pdf';
$ipf_cfg['dynReport']['batchPrintMode']['html'] = 'html';

// Student file path
$ipf_cfg['dynReport']['studentReportPath'] = $PATH_WRT_ROOT.'../intranetdata/iPortfolio/dynRpt/stuRpt';
$ipf_cfg['dynReport']['encryptKey'] = 'esf5h8Sf';


### SLP Academic Result Print Mode
$ipf_cfg['slpAcademicResultPrintMode']['mark'] = 'mark';
$ipf_cfg['slpAcademicResultPrintMode']['grade'] = 'grade';
$ipf_cfg['slpAcademicResultPrintMode']['markThenGrade'] = 'markThenGrade';
$ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark'] = 'gradeThenMark';
$ipf_cfg['slpAcademicResultPrintMode_default'] = $ipf_cfg['slpAcademicResultPrintMode']['markThenGrade']; 
$ipf_cfg['slpAcademicResultEmptySymbol_default'] = '--';

$ipf_cfg['SLPArr']['SLPSchoolHeaderImageName'] = 'schoolLogo'; 
$ipf_cfg['SLPArr']['SLPFolderPath'] = '/file/iportfolio/slp/'; 

### GROUP setting , GROUP TYPE 
### Define admin type or student type
$ipf_cfg['GroupSetting']['isStudentTypeGroup'] = 0;
$ipf_cfg['GroupSetting']['isAdminTypeGroup'] = 1;


#######################################################################

### Academic Result Print Request From
// there is a program checking , key "DynamicReport" must be same as value "DynamicReport"
$ipf_cfg['slpAcademicResultPrintRequestFrom']['DynamicReport'] = 'DynamicReport';


## SIS Hide Columns ##
// >>>>>>>>>> libconfig 
/* This shows which columns ARE ABLE TO HIDE, */
/* show/hide option of columns in here can setup in settings.php ['IPF_HIDE_COLUMN'][INT||EXT_STUDENT] per each school */
$cfg_column_displayConfig['INT_STUDENT'] = array('OLE_ROLE','ACHIEVEMENTS','ATTACHMENT','COMMENT','STATUS','APPROVALDATE','APPROVER');
$cfg_column_displayConfig['EXT_STUDENT'] = array('OLE_ROLE','ATTACHMENT','COMMENT','STATUS','APPROVALDATE','APPROVER');
// End of libconfig <<<<<<<<<<<<

## TAB CONFIG ##
// Eric Yip (20101005):
// - Moved to iPortfolioTabConfig.inc.php
// - For centralized management of tabs
include_once("iPortfolioTabConfig.inc.php");
?>
