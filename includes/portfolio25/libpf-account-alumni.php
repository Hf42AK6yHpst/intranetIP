<?php
// Editing by
/*
 * Date 2018-02-12 Omas (X135671)
 * modified GET_IPORTFOLIO_PHOTO(), if don't have official photo show personal photo
 */

include_once ($intranet_root . "/includes/libdb.php");

include_once ($intranet_root . "/includes/libportfolio.php");
include_once ($intranet_root . "/includes/libportfolio2007a.php");
include_once ($intranet_root . "/includes/libpf-slp.php");
include_once ($intranet_root . "/includes/portfolio25/libpf-account.php");

class libpf_account_alumni extends libpf_account
{

    private $WebSAMSRegNo;

    private $english_name;

    private $chinese_name;

    private $gender;

    private $dob;

    private $nationality;

    private $pob;

    private $home_tel;

    private $address;

    private $admission_date;

    private $student_house;

    private $class_name;

    private $class_number;

    private $parent_arr;

    private $ay_history_arr;

    private $class_history_arr;

    private $sem_history_arr;

    private $assessment_sem_arr;

    private $assessment_subject_arr;

    private $assessment_subject_result_arr;

    private $assessment_overall_result_arr;

    private $merit_summary_arr;

    public function __construct()
    {
        parent::__construct();
        
        $this->WebSAMSRegNo = "";
        $this->english_name = "";
        $this->chinese_name = "";
        $this->gender = "";
        $this->dob = "";
        $this->nationality = "";
        $this->pob = "";
        $this->home_tel = "";
        $this->admission_date = "";
        $this->student_house = "";
        $this->class_name = "";
        $this->class_number = "";
        $this->parent_arr = array();
        $this->ay_history_arr = array();
        $this->class_history_arr = array();
        $this->sem_history_arr = array();
        $this->merit_summary_arr = array();
    }

    public function IS_PORTFOLIO_USER()
    {
        global $eclass_db;
        
        $user_id = $this->GET_CLASS_VARIABLE("user_id");
        $sql = "
              SELECT
                count(*)
              FROM
                {$eclass_db}.PORTFOLIO_STUDENT
              WHERE
                UserID = '" . $user_id . "'
            ";
        $returnVal = $this->db->returnVector($sql);
        
        return ($returnVal[0] == 1);
    }

    public function SET_WEBSAMS_REGNO()
    {
        global $eclass_db;
        
        $sql = "
              SELECT
                IF(WebSAMSRegNo IS NULL, '', WebSAMSRegNo)
              FROM
                {$eclass_db}.PORTFOLIO_STUDENT
              WHERE
                UserID = '" . $this->GET_CLASS_VARIABLE("user_id") . "'
            ";
        $returnVal = current($this->db->returnVector($sql));
        
        $this->SET_CLASS_VARIABLE("WebSAMSRegNo", $returnVal);
    }
    
    // Get the link of official photo of a student by Registion Number
    public function GET_OFFICIAL_PHOTO()
    {
        global $intranet_rel_path, $intranet_root;
        
        if ($this->GET_CLASS_VARIABLE("WebSAMSRegNo") == "") {
            $this->SET_WEBSAMS_REGNO();
        }
        $RegNo = $this->GET_CLASS_VARIABLE("WebSAMSRegNo");
        
        $tmp_regno = trim(str_replace("#", "", $RegNo));
        $photolink = "/file/official_photo/" . $tmp_regno . ".jpg";
        $photo_filepath = $intranet_root . $photolink;
        $photo_url = $intranet_rel_path . $photolink;
        
        return array(
            $photo_filepath,
            $photo_url
        );
    }

    /*
     * Modified by key 2008-09-29: Update the get photo function
     */
    public function GET_IPORTFOLIO_PHOTO($ParReturnNullPhoto = true)
    {
        global $intranet_root;
        
        $user_id = $this->GET_CLASS_VARIABLE("user_id");
        
        // ## user
        include_once ($intranet_root . "/includes/libuser.php");
        $luser = new libuser($user_id);
        
        // Set student photo in left menu
        if (is_object($luser)) {
            $TmpPhotoLink = $this->GET_OFFICIAL_PHOTO();
            if ($TmpPhotoLink != "")
                $PhotoLink = str_replace($intranet_root, "", $TmpPhotoLink[0]);
        }
        
        // get the student photo
        if (! file_exists($TmpPhotoLink[0])) {
            $PhotoLink = "";
        }
        // X135671
//         if ($PhotoLink == "" && $luser->PhotoLink != "") {
//             if (is_file($intranet_root . $luser->PhotoLink)) {
//                 $PhotoLink = $luser->PhotoLink;
//             }
//         }
        if($PhotoLink == ""){
            $linkArr = $luser->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);
            $PhotoLink = $linkArr[2];
        }
        
        if ($ParReturnNullPhoto && $PhotoLink == "") {
            $PhotoLink = "/images/2009a/iPortfolio/no_photo.jpg";
        }
        
        return $PhotoLink;
    }
 // end function
      
    // Require User ID
    public function SET_STUDENT_PROPERTY()
    {
        global $intranet_db, $eclass_db;
        
        $user_id = $this->GET_CLASS_VARIABLE("user_id");
        if ($user_id == "")
            return false;
        
        $sql = "
              SELECT DISTINCT
                iau.UserID,
                iau.EnglishName,
                iau.ChineseName,
                IFNULL(iau.Gender, '') AS Gender,
                IFNULL(iau.DateOfBirth, '') AS DateOfBirth,
                IFNULL(ps.WebSAMSRegNo, '') AS WebSAMSRegNo,
                IFNULL(ps.Nationality, '') AS Nationality,
                IFNULL(ps.PlaceOfBirth, '') AS PlaceOfBirth,
                IFNULL(iau.HomeTelNo, '') AS HomeTelNo,
                IFNULL(iau.Address, '') AS Address,
                IFNULL(ps.AdmissionDate, '') AS AdmissionDate,
                IFNULL(t_house.Title, '') AS StudentHouse,
                iau.ClassName,
                iau.ClassNumber
              FROM
                {$intranet_db}.INTRANET_ARCHIVE_USER AS iau
              LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
                ON iau.UserID = ps.UserID
              LEFT JOIN 
                (
                  SELECT
                    iug.UserID,
                    iug.GroupID,
                    ig.Title
                  FROM
                    {$intranet_db}.INTRANET_USERGROUP as iug
                  INNER JOIN {$intranet_db}.INTRANET_GROUP as ig
                    ON ig.GroupID = iug.GroupID
                  WHERE
                    ig.RecordType = '4' AND
                    iug.UserID = " . $user_id . "
                ) AS t_house
              ON t_house.UserID = iau.UserID
              WHERE
                iau.UserID = " . $user_id . "
            ";
        $student_arr = $this->db->returnArray($sql);
        $student_arr = $student_arr[0];
        
        if (! empty($student_arr)) {
            $this->english_name = ($student_arr["EnglishName"] == '') ? '--' : $student_arr["EnglishName"];
            
            $this->chinese_name = ($student_arr["ChineseName"] == '') ? '--' : $student_arr["ChineseName"];
            
            $this->gender = ($student_arr["Gender"] == '') ? '--' : $student_arr["Gender"];
            $this->dob = ($student_arr["DateOfBirth"] == '') ? '--' : $student_arr["DateOfBirth"];
            $this->WebSAMSRegNo = ($student_arr["WebSAMSRegNo"] == '') ? '--' : $student_arr["WebSAMSRegNo"];
            $this->nationality = ($student_arr["Nationality"] == '') ? '--' : $student_arr["Nationality"];
            $this->pob = ($student_arr["PlaceOfBirth"] == '') ? '--' : $student_arr["PlaceOfBirth"];
            $this->home_tel = ($student_arr["HomeTelNo"] == '') ? '--' : $student_arr["HomeTelNo"];
            $this->address = ($student_arr["Address"] == '') ? '--' : $student_arr["Address"];
            $this->admission_date = ($student_arr["AdmissionDate"] == '') ? '--' : $student_arr["AdmissionDate"];
            
            $this->student_house = ($student_arr["StudentHouse"] == '') ? '--' : $student_arr["StudentHouse"];
            $this->class_name = ($student_arr["ClassName"] == '') ? '--' : $student_arr["ClassName"];
            $this->class_number = ($student_arr["ClassNumber"] == '') ? '--' : $student_arr["ClassNumber"];
        }
    }
    
    // Get parent record ID by student User ID
    public function SET_PARENT()
    {
        global $eclass_db;
        
        $user_id = $this->GET_CLASS_VARIABLE("user_id");
        if ($user_id == "")
            return false;
        
        $sql = "
							SELECT
								ChName AS chinese_name,
								EnName AS english_name,
								IF(CHAR_LENGTH(Relation) = 1, CONCAT('0', Relation), Relation) AS relation,
								Phone AS phone,
								EmPhone AS em_phone,
								IsMain AS is_main
							FROM
								{$eclass_db}.GUARDIAN_STUDENT
							WHERE
								UserID = '" . $user_id . "'
							ORDER BY
								IsMain DESC
						";
        $parent_arr = $this->db->returnArray($sql);
        
        $this->SET_CLASS_VARIABLE("parent_arr", $parent_arr);
    }
    
    // Assumption 1: Academic Year Name in history / assessment records must match
    // one of academic year record in school settings
    public function SET_ACADMEMIC_YEAR_HISTORY()
    {
        global $intranet_db, $eclass_db;
        
        $user_id = $this->GET_CLASS_VARIABLE("user_id");
        if ($user_id == "")
            return false;
        
        $sql = "
              SELECT DISTINCT
                ay.AcademicYearID,
                t_ayear.AcademicYear
              FROM
                (
                  SELECT DISTINCT AcademicYear
                  FROM
                    {$intranet_db}.PROFILE_CLASS_HISTORY
                  WHERE UserID = " . $user_id . "
                  UNION
                  SELECT DISTINCT Year AS AcademicYear
                  FROM
                    {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
                  WHERE UserID = " . $user_id . "
                  UNION
                  SELECT ay.YearNameEN
                  FROM {$intranet_db}.YEAR_CLASS_USER ycu
                  INNER JOIN {$intranet_db}.YEAR_CLASS yc
                    ON ycu.YearClassID = yc.YearClassID
                  INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay
                    ON yc.AcademicYearID = ay.AcademicYearID
                  WHERE ycu.UserID = " . $user_id . "
                ) AS t_ayear
              INNER JOIN
                {$intranet_db}.ACADEMIC_YEAR ay
                ON t_ayear.AcademicYear = ay.YearNameEN
              ORDER BY
                ay.Sequence
            ";
        $t_ay_arr = $this->db->returnArray($sql);
        
        $ay_arr = array();
        for ($i = 0; $i < count($t_ay_arr); $i ++) {
            $ay_id = $t_ay_arr[$i]['AcademicYearID'];
            $ay_name = $t_ay_arr[$i]['AcademicYear'];
            
            $ay_arr[$ay_id] = $ay_name;
        }
        
        $this->ay_history_arr = $ay_arr;
    }
    
    // Assumption 1: Academic Year Name in history / assessment records must match
    // one of academic year record in school settings
    public function SET_CLASS_HISTORY()
    {
        global $intranet_db, $eclass_db;
        
        $user_id = $this->GET_CLASS_VARIABLE("user_id");
        if ($user_id == "")
            return false;
        
        $sql = "
              SELECT DISTINCT
                ay.AcademicYearID,
                t_class.ClassName
              FROM
                (
                  SELECT DISTINCT AcademicYear, ClassName
                  FROM
                    {$intranet_db}.PROFILE_CLASS_HISTORY
                  WHERE UserID = " . $user_id . "
                  UNION
                  SELECT DISTINCT Year AS AcademicYear, ClassName
                  FROM
                    {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
                  WHERE UserID = " . $user_id . "
                  UNION
                  SELECT ay.YearNameEN, yc.ClassTitleEN
                  FROM {$intranet_db}.YEAR_CLASS_USER ycu
                  INNER JOIN {$intranet_db}.YEAR_CLASS yc
                    ON ycu.YearClassID = yc.YearClassID
                  INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay
                    ON yc.AcademicYearID = ay.AcademicYearID
                  WHERE ycu.UserID = " . $user_id . "
                ) AS t_class
              INNER JOIN
                {$intranet_db}.ACADEMIC_YEAR ay
                ON t_class.AcademicYear = ay.YearNameEN
              ORDER BY
                ay.Sequence DESC
            ";
        $t_class_arr = $this->db->returnArray($sql);
        
        $class_arr = array();
        for ($i = 0; $i < count($t_class_arr); $i ++) {
            $ay_id = $t_class_arr[$i]['AcademicYearID'];
            $class_name = $t_class_arr[$i]['ClassName'];
            
            $class_arr[$ay_id] = $class_name;
        }
        
        $this->class_history_arr = $class_arr;
    }

    public function SET_ASSESSMENT_SEMESTER()
    {
        global $eclass_db, $intranet_db;
        
        $user_id = $this->GET_CLASS_VARIABLE("user_id");
        if ($user_id == "")
            return false;
        
        $this->SET_ACADMEMIC_YEAR_HISTORY();
        $academic_year_arr = $this->GET_CLASS_VARIABLE("ay_history_arr");
        $ay_id_arr = array_keys($academic_year_arr);
        $ay_name_arr = array_values($academic_year_arr);
        
        $sem_arr = array();
        for ($i = 0; $i < count($ay_id_arr); $i ++) {
            $ay_id = $ay_id_arr[$i];
            $ay_name = $ay_name_arr[$i];
            
            $sql = "
                SELECT
                  ayt.YearTermID
                FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
                INNER JOIN {$intranet_db}.ACADEMIC_YEAR_TERM ayt
                  ON assr.Semester = ayt.YearTermNameEN AND
                    (assr.Year = '" . $ay_name . "' AND ayt.AcademicYearID = " . $ay_id . ")
                WHERE
                  assr.UserID = " . $user_id . "
              ";
            $t_sem_id_arr = $this->db->returnVector($sql);
            
            $tt_sem_arr = array();
            for ($j = 0; $j < count($t_sem_id_arr); $j ++) {
                $t_sem_id = $t_sem_id_arr[$j]["YearTermID"];
                
                $layt = new academic_year_term($t_sem_id);
                $tt_sem_arr[$t_sem_id] = $layt->Get_Year_Term_Name();
            }
            
            $sem_arr[$ay_id] = $tt_sem_arr;
        }
        
        $this->assessment_sem_arr = $sem_arr;
    }

    public function SET_ASSESSMENT_SUBJECT()
    {
        global $eclass_db, $intranet_db;
        
        $user_id = $this->GET_CLASS_VARIABLE("user_id");
        if ($user_id == "")
            return false;
        
        $sql = "
              CREATE TEMPORARY TABLE tempSubjectTable (
                MainSubjCode varchar(20),
                MainSubjName varchar(255),
                MainSubjDispOrder int(11),
                CompSubjCode varchar(20),
                CompSubjName varchar(255),
                CompSubjDispOrder int(11),
                KEY MainSubjCode (MainSubjCode)
              )
            ";
        $this->db->db_db_query($sql);
        
        // Add main subject
        $sql = "
              INSERT INTO
                tempSubjectTable
                  (MainSubjCode, MainSubjName, MainSubjDispOrder)
              SELECT DISTINCT
                subj.EN_SNAME,
                subj.EN_DES,
                subj.DisplayOrder
              FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
              INNER JOIN {$intranet_db}.ASSESSMENT_SUBJECT subj
                ON assr.SubjectCode = subj.EN_SNAME
              WHERE
                assr.UserID = " . $user_id . "
            ";
        $this->db->db_db_query($sql);
        
        // Add component subject
        $sql = "
              INSERT INTO
                tempSubjectTable
                  (MainSubjCode, MainSubjDispOrder, CompSubjCode, CompSubjName, CompSubjDispOrder)
              SELECT DISTINCT
                main_subj.EN_SNAME,
                main_subj.DisplayOrder,
                comp_subj.EN_SNAME,
                comp_subj.EN_DES,
                comp_subj.DisplayOrder
              FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
              INNER JOIN {$intranet_db}.ASSESSMENT_SUBJECT comp_subj
                ON assr.SubjectComponentCode = comp_subj.EN_SNAME
              INNER JOIN {$intranet_db}.ASSESSMENT_SUBJECT main_subj
                ON assr.SubjectCode = main_subj.EN_SNAME
                  AND main_subj.CODEID = comp_subj.CODEID
              WHERE
                assr.UserID = " . $user_id . "
            ";
        $this->db->db_db_query($sql);
        
        $sql = "
              SELECT
                MainSubjCode,
                MainSubjName,
                CompSubjCode,
                CompSubjName
              FROM
                tempSubjectTable
              ORDER BY
                MainSubjDispOrder, CompSubjDispOrder
            ";
        $subj_arr = $this->db->returnArray($sql);
        
        $this->assessment_subject_arr = $subj_arr;
    }

    public function SET_ASSESSMENT_RESULT()
    {
        $this->SET_ASSESSMENT_SUBJECT_RESULT();
        $this->SET_ASSESSMENT_ANNUAL_RESULT();
    }

    private function SET_ASSESSMENT_SUBJECT_RESULT()
    {
        global $eclass_db;
        
        $user_id = $this->GET_CLASS_VARIABLE("user_id");
        if ($user_id == "")
            return false;
        
        $assessment_sem_arr = $this->assessment_sem_arr;
        if (! is_array($assessment_sem_arr))
            return false;
        
        $assessment_subject_arr = $this->assessment_subject_arr;
        if (! is_array($assessment_subject_arr))
            return false;
        
        $sql = "
              CREATE TEMPORARY TABLE tempResultTable (
                AcademicYearID int(11),
                YearTermID int(11),
                MainSubjCode varchar(20),
                CompSubjCode varchar(20),
                Score float,
                Grade char(10),
                KEY MainSubjCode (MainSubjCode),
                KEY CompSubjCode (CompSubjCode),
                UNIQUE KEY YearTermCompSubjectResult (AcademicYearID, YearTermID, MainSubjCode, CompSubjCode)
              )
            ";
        $this->db->db_db_query($sql);
        
        for ($i = 0; $i < count($assessment_subject_arr); $i ++) {
            $main_subject_code = $assessment_subject_arr[$i][0];
            $comp_subject_code = $assessment_subject_arr[$i][2];
            
            foreach ($assessment_sem_arr as $ay_id => $sem_arr) {
                // insert result of whole year
                $sql = "
                  INSERT IGNORE INTO
                    tempResultTable
                      (AcademicYearID, MainSubjCode, CompSubjCode, Score, Grade)
                  SELECT
                    AcademicYearID,
                    SubjectCode,
                    SubjectComponentCode,
                    IF(Score=0 AND (Grade IS NULL OR Grade = ''), NULL, Score),
                    Grade
                  FROM
                    {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
                  WHERE
                    UserID = " . $user_id . " AND
                    AcademicYearID = " . $ay_id . " AND
                    (YearTermID IS NULL OR YearTermID = '' OR Semester IS NULL OR Semester = '') AND
                    SubjectCode = '" . $main_subject_code . "'
                ";
                $this->db->db_db_query($sql);
                
                // insert result of each sem
                if (is_array($sem_arr)) {
                    foreach ($sem_arr as $yt_id => $sem_name) {
                        $sql = "
                      INSERT IGNORE INTO
                        tempResultTable
                          (AcademicYearID, YearTermID, MainSubjCode, CompSubjCode, Score, Grade)
                      SELECT
                        AcademicYearID,
                        YearTermID,
                        SubjectCode,
                        SubjectComponentCode,
                        IF(Score=0 AND (Grade IS NULL OR Grade = ''), NULL, Score),
                        Grade
                      FROM
                        {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
                      WHERE
                        UserID = " . $user_id . " AND
                        AcademicYearID = " . $ay_id . " AND
                        (YearTermID = " . $yt_id . " OR Semester = '" . $sem_name . "') AND
                        SubjectCode = '" . $main_subject_code . "'
                    ";
                        $this->db->db_db_query($sql);
                    }
                }
            }
        }
        
        $sql = "
              SELECT DISTINCT
                AcademicYearID,
                IF((YearTermID IS NULL OR YearTermID = ''), 'overall', YearTermID) AS YearTermID,
                MainSubjCode,
                IF((CompSubjCode IS NULL OR CompSubjCode = ''), 'major', CompSubjCode) AS CompSubjCode,
                Score,
                Grade
              FROM
                tempResultTable
            ";
        $t_result_arr = $this->db->returnArray($sql);
        
        $result_arr = array();
        for ($i = 0; $i < count($t_result_arr); $i ++) {
            $t_ay_id = $t_result_arr[$i]['AcademicYearID'];
            $t_yt_id = $t_result_arr[$i]['YearTermID'];
            $t_main_subj_code = $t_result_arr[$i]['MainSubjCode'];
            $t_comp_subj_code = $t_result_arr[$i]['CompSubjCode'];
            $t_score = $t_result_arr[$i]['Score'];
            $t_grade = $t_result_arr[$i]['Grade'];
            
            $result_arr[$t_ay_id][$t_yt_id][$t_main_subj_code][$t_comp_subj_code]["score"] = $t_score;
            $result_arr[$t_ay_id][$t_yt_id][$t_main_subj_code][$t_comp_subj_code]["grade"] = $t_grade;
        }
        
        $this->assessment_subject_result_arr = $result_arr;
        
        $sql = "DROP TABLE tempResultTable";
        $this->db->db_db_query($sql);
    }

    private function SET_ASSESSMENT_ANNUAL_RESULT()
    {
        global $eclass_db;
        
        $user_id = $this->GET_CLASS_VARIABLE("user_id");
        if ($user_id == "")
            return false;
        
        $assessment_sem_arr = $this->assessment_sem_arr;
        if (! is_array($assessment_sem_arr))
            return false;
        
        $sql = "
              CREATE TEMPORARY TABLE tempResultTable (
                AcademicYearID int(11),
                YearTermID int(11),
                Score float,
                Grade char(10),
                UNIQUE KEY YearTermResult (AcademicYearID, YearTermID)
              )
            ";
        $this->db->db_db_query($sql);
        
        foreach ($assessment_sem_arr as $ay_id => $sem_arr) {
            // insert result of whole year
            $sql = "
                INSERT IGNORE INTO
                  tempResultTable
                    (AcademicYearID, Score, Grade)
                SELECT
                  AcademicYearID,
                  IF(Score=0 AND (Grade IS NULL OR Grade = ''), NULL, Score),
                  Grade
                FROM
                  {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
                WHERE
                  UserID = " . $user_id . " AND
                  AcademicYearID = " . $ay_id . " AND
                  (YearTermID IS NULL OR YearTermID = '' OR Semester IS NULL OR Semester = '')
              ";
            $this->db->db_db_query($sql);
            
            // insert result of each sem
            if (is_array($sem_arr)) {
                foreach ($sem_arr as $yt_id => $sem_name) {
                    $sql = "
                    INSERT IGNORE INTO
                      tempResultTable
                        (AcademicYearID, YearTermID, Score, Grade)
                    SELECT
                      AcademicYearID,
                      YearTermID,
                      IF(Score=0 AND (Grade IS NULL OR Grade = ''), NULL, Score),
                      Grade
                    FROM
                      {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
                    WHERE
                      UserID = " . $user_id . " AND
                      AcademicYearID = " . $ay_id . " AND
                      (YearTermID = " . $yt_id . " OR Semester = '" . $sem_name . "')
                  ";
                    $this->db->db_db_query($sql);
                }
            }
        }
        
        $sql = "
              SELECT DISTINCT
                AcademicYearID,
                IF((YearTermID IS NULL OR YearTermID = ''), 'overall', YearTermID) AS YearTermID,
                Score,
                Grade
              FROM
                tempResultTable
            ";
        $t_result_arr = $this->db->returnArray($sql);
        
        $result_arr = array();
        for ($i = 0; $i < count($t_result_arr); $i ++) {
            $t_ay_id = $t_result_arr[$i]['AcademicYearID'];
            $t_yt_id = $t_result_arr[$i]['YearTermID'];
            $t_score = $t_result_arr[$i]['Score'];
            $t_grade = $t_result_arr[$i]['Grade'];
            
            $result_arr[$t_ay_id][$t_yt_id]["score"] = $t_score;
            $result_arr[$t_ay_id][$t_yt_id]["grade"] = $t_grade;
        }
        
        $this->assessment_overall_result_arr = $result_arr;
        
        $sql = "DROP TABLE tempResultTable";
        $this->db->db_db_query($sql);
    }

    public function SET_MERIT_SUMMARY($parYear = "", $parSemester = "")
    {
        global $eclass_db;
        
        $user_id = $this->GET_CLASS_VARIABLE("user_id");
        if ($user_id == "")
            return false;
        
        $lpf_slp = new libpf_slp();
        
        $conds = $lpf_slp->getReportYearCondition($parYear);
        if ($parSemester != "") {
            $conds .= "AND (Semester = '$parSemester')";
        }
        
        $sql = "
              SELECT
                Year,
                Semester,
                Month(MeritDate) AS meritMonth,
                RecordType,
                SUM(NumberOfUnit) AS unitSum
              FROM
                {$eclass_db}.MERIT_STUDENT
              WHERE
                UserID = " . $user_id . "
                $conds
              GROUP BY
                Year, Semester, meritMonth, RecordType
              ORDER BY
      					Year DESC, Semester DESC, RecordType DESC
            ";
        $t_meritArr = $this->db->returnArray($sql);
        
        $meritArr = array();
        for ($i = 0; $i < count($t_meritArr); $i ++) {
            list ($year, $semester, $month, $type, $count) = $t_meritArr[$i];
            $meritArr[$year][$semester][$month][$type] = $count;
        }
        
        $this->merit_summary_arr = $meritArr;
    }
    
    // Convert Student Merit Summary, copy from retrieveStudentConvertedMeritTypeCountByDate() in libdisciplinev12.php
    public function GET_CONVERT_MERIT_SUMMARY($parYear = "", $parSemester = "")
    {
        $this->SET_MERIT_SUMMARY($parYear, $parSemester);
        $dataAry = $this->merit_summary_arr;
        
        $ldiscipline = new libdisciplinev12();
        // Get Promotion Method
        $PromotionMethod = $ldiscipline->getDisciplineGeneralSetting("PromotionMethod");
        $conversionType_punish = $ldiscipline->getConversionPeriod();
        $conversionType_award = $ldiscipline->getConversionPeriod('', 1);
        
        $MeritPromotionSetting_punish = $ldiscipline->retrieveMeritPromotionInfo($PromotionMethod);
        $MeritPromotionSetting_award = $ldiscipline->retrieveMeritPromotionInfo($PromotionMethod, '', 1);
        
        if ($PromotionMethod == "global") {
            // $dataAry[year][sem][meritType] = no. of meritType (this array is for one student)
            // $MeritPromotionSetting[meritType] = array(FromMerit(meritType), ToMerit, UpgradeNum)
            $_dataAry = array();
            foreach ($dataAry as $year => $semMeritArr) {
                foreach ($semMeritArr as $sem => $monthMeritArr) {
                    foreach ($monthMeritArr as $month => $meritDataArr) {
                        foreach ($meritDataArr as $meritType => $thisMeritVal) {
                            $conversionType = (intval($meritType) > 0) ? $conversionType_award : $conversionType_punish;
                            
                            // Group the number of merit/demerit according to conversion type
                            switch ($conversionType["global"]) {
                                case 0:
                                case 1:
                                case 2:
                                    // 'whole' as place holder
                                    $_dataAry[$year][$sem]['whole'][$meritType] += $thisMeritVal;
                                    break;
                                case 3:
                                    $_dataAry[$year][$sem][$month][$meritType] += $thisMeritVal;
                                    break;
                            }
                        }
                    }
                }
            }
            
            // Convert the numbers according to rules
            foreach ($_dataAry as $year => $semMeritArr) {
                foreach ($semMeritArr as $sem => $monthMeritArr) {
                    foreach ($monthMeritArr as $month => $meritDataArr) {
                        foreach ($meritDataArr as $meritType => $thisMeritVal) {
                            
                            $MeritPromotionSetting = (intval($meritType) > 0) ? $MeritPromotionSetting_award : $MeritPromotionSetting_punish;
                            
                            $thisFromMerit = $MeritPromotionSetting[$meritType]["UpgradeFromType"];
                            $thisToMerit = $MeritPromotionSetting[$meritType]["MeritType"];
                            $thisUpgradeNum = $MeritPromotionSetting[$meritType]["UpgradeNum"];
                            
                            if (empty($thisMeritVal))
                                continue;
                                
                                // if no conversion rule, use original value
                            if (empty($thisUpgradeNum)) {
                                $returnAry[$year][$sem][$meritType] += $thisMeritVal;
                            }                             // apply conversion rule if there is one
                            else {
                                $returnAry[$year][$sem][$thisToMerit] += floor($thisMeritVal / $thisUpgradeNum);
                                $returnAry[$year][$sem][$thisFromMerit] += fmod($thisMeritVal, $thisUpgradeNum);
                            }
                        }
                    }
                }
            }
        } else {
            // 20091208, not support category conversion
            /*
             * foreach($MeritPromotionSetting as $thisCatID =>$thisCatMeritPromotionSetting)
             * {
             * foreach($thisCatMeritPromotionSetting as $thisMeritPromotionSetting)
             * {
             * $thisFromMerit = $thisMeritPromotionSetting["UpgradeFromType"];
             * $thisToMerit = $thisMeritPromotionSetting["MeritType"];
             * $thisUpgradeNum = $thisMeritPromotionSetting["UpgradeNum"];
             *
             * if(empty($dataAry[$thisCatID][$thisFromMerit]) || empty($thisUpgradeNum)) continue;
             *
             * $dataAry[$thisCatID][$thisToMerit] += floor($RawDataAry[$thisCatID][$thisFromMerit]/$thisUpgradeNum);
             * $dataAry[$thisCatID][$thisFromMerit] = fmod($RawDataAry[$thisCatID][$thisFromMerit],$thisUpgradeNum);
             * }
             * }
             * $returnAry = Array();
             * foreach ($dataAry as $thisCatMeritCount)
             * {
             * foreach($thisCatMeritCount as $thisMeritType =>$thisMeritCount)
             * {
             * $returnAry[$thisMeritType] += $thisMeritCount;
             * }
             * }
             *
             *
             */
            $returnAry = $dataAry;
        }
        
        return $returnAry;
    }
    
    // $ParVariableName is case-sensitive
    public function SET_CLASS_VARIABLE($ParVariableName, $ParValue)
    {
        // Prevent declaring new variables to a class
        try {
            if (! isset($this->{$ParVariableName})) {
                throw new Exception("Class variable \"{$ParVariableName}\" is not declared!!");
            } else {
                $this->{$ParVariableName} = $ParValue;
            }
        } catch (Exception $e) {
            $error_trace = current(debug_backtrace());
            
            echo "Caught exception: ", $e->getMessage(), "<br />\n";
            echo "In script: ", $error_trace["file"], "<br />\n";
            echo "Line no: ", $error_trace["line"], "<br />\n";
            echo "Method called: ", $error_trace["function"], " @ ", $error_trace["class"], "<br />\n";
            DIE();
        }
    }
    
    // $ParVariableName is case-sensitive
    public function GET_CLASS_VARIABLE($ParVariableName)
    {
        try {
            if (! isset($this->{$ParVariableName})) {
                throw new Exception("Class variable \"{$ParVariableName}\" is not declared!!");
            } else {
                return $this->{$ParVariableName};
            }
        } catch (Exception $e) {
            $error_trace = current(debug_backtrace());
            
            echo "Caught exception: ", $e->getMessage(), "<br />\n";
            echo "In script: ", $error_trace["file"], "<br />\n";
            echo "Line no: ", $error_trace["line"], "<br />\n";
            echo "Method called: ", $error_trace["function"], " @ ", $error_trace["class"], "<br />\n";
            DIE();
        }
    }
}