<?
# modifying : 

if (!defined("LIBOEA_ITEM_DEFINED"))         // Preprocessor directives
{
  define("LIBOEA_ITEM_DEFINED",true);

	class liboea_item {
		private $objDB;
		private $recordID;

		private $StudentID;
		private $Title;
		private $StartYear;
		private $EndYear;
		private $Participation;
		private $Role;
		private $ParticipationNature;
		private $Achievement;
		private $Description;
		private $OEA_ProgramCode;
		private $OLE_STUDENT_RecordID;
		private $OLE_PROGRAM_ProgramID;
		private $RecordStatus;
		private $InputDate;
		private $InputBy;
		private $ApprovalDate;
		private $ApprovedBy;
		private $ModifyDate;
		private $ModifiedBy;
		private $MaxOEAItem;
		private $TempStudentID;
		private $TempTitle;
		private $TempStartYear;
		private $TempEndYear;
		private $TempAwardBearing;
		private $TempParticipation;
		private $TempParticipationNature;
		private $TempRole;
		private $TempAchievement;
		private $TempDescription;
		private $TempApprovalDate;
		private $TempApprovedBy;
		private $TempOEA_ProgramCode;
		private $TempOLE_STUDENT_RecordID;
		private $TempOLE_PROGRAM_ProgramID;
		private $TempRecordStatus;
		private $NewRecord;
		private $EditMode;
		

//		public function liboea_item(int $recordID = null){
		public function liboea_item($recordID = null){
			global $eclass_db, $oea_cfg;
			
			include_once("liboea.php");
			$liboea = new liboea();
			
			$this->objDB = new libdb();
			$this->MaxOEAItem = $liboea->getMaxNoOfOea();
			//$this->OEA_AwardBearing = "N";
			$this->EditMode = 0;
			
			//LOAD THE OEA DETAILS FORM STOREAGE (EG DB)
			if($recordID != null)
			{
				$this->recordID = intval($recordID);
				$this->loadRecordFromStorage();
			}
		}

		public function setStudentID($sID){
			$this->StudentID = $sID;
		}
		public function getStudentID(){
			return $this->StudentID;
		}
		public function setTitle ($str){
			$this->Title = $str;
		}
		public function getTitle(){
			return $this->Title;
		}
		public function setStartYear($intStarYear){
			$this->StartYear = $intStarYear;
		}
		public function getStartYear(){
			return $this->StartYear;
		}
		public function setEndYear($intEndYear){
			$this->EndYear = $intEndYear;
		}
		public function getEndYear(){
			return $this->EndYear;
		}
		public function getOEAYear() {
			global $i_To;
			return $this->StartYear.(($this->EndYear=="") ? "" : " ".$i_To." ".$this->EndYear);	
		}
		public function get_Edit_OEAYear() {
			global $i_To;
			$editYear = "<input type='text' name='StartYear' id='StartYear' size='4' laxlength='4' value='".$this->StartYear."'>";
			$editYear .= "<input type='text' name='EndYear' id='EndYear' size='4' laxlength='4' value='".$this->EndYear."'>";
			return $editYear;
		}
		public function setParticipation($str){
			$this->Participation = $str;
		}
		public function getParticipation(){
			return $this->Participation;
		}
		public function setRole($str){
			$this->Role= $str;
		}
		public function getRole(){
			return $this->Role;
		}
		public function setParticipationNature($str){
			$this->ParticipationNature= $str;
		}
		public function getParticipationNature(){
			return $this->ParticipationNature;
		}
		public function setAchievement($str){
			$this->Achievement= $str;
		}
		public function getAchievement(){
			return $this->Achievement;
		}
		public function setDescription($str){
			$this->Description= $str;
		}
		public function getDescription(){
			return $this->Description;
		}
		public function setOEA_ProgramCode($str){
			$this->OEA_ProgramCode= $str;
		}
		public function getOEA_ProgramCode(){
			return $this->OEA_ProgramCode;
		}
		public function setOLE_STUDENT_RecordID($intID){
			$this->OLE_STUDENT_RecordID= $intID;
		}
		public function getOLE_STUDENT_RecordID(){
			return $this->OLE_STUDENT_RecordID;
		}
		public function setOLE_PROGRAM_ProgramID($intID){
			$this->OLE_PROGRAM_ProgramID= $intID;
		}
		public function getOLE_PROGRAM_ProgramID(){
			return $this->OLE_PROGRAM_ProgramID;
		}
		public function setRecordStatus($intStatus){
			$this->RecordStatus= $intStatus;
		}
		public function getRecordStatus(){
			return $this->RecordStatus;
		}
		public function setInputDate($value){
			$this->InputDate= $value;
		}
		public function getInputDate(){
			return $this->InputDate;
		}
		public function setInputBy($intValue){
			$this->InputBy= $intValue;
		}
		public function getInputBy(){
			return $this->InputBy;
		}
		public function setApprovalDate($value){
			$this->ApprovalDate= $value;
		}
		public function getApprovalDate(){
			return $this->ApprovalDate;
		}
		public function setApprovedBy($intValue){
			$this->ApprovedBy= $intValue;
		}
		public function getApprovedBy(){
			return $this->ApprovedBy;
		}
		public function setModifyDate($value){
			$this->ModifyDate= $value;
		}
		public function getModifyDate(){
			return $this->ModifyDate;
		}
		public function setModifiedBy($intValue){
			$this->ModifiedBy= $intValue;
		}
		public function getModifiedBy(){
			return $this->ModifiedBy;
		}
		public function setTempStudentID($value) {
			$this->TempStudentID = $value;
		}
		public function setTempTitle($value) {
			$this->TempTitle = $value;
		}
		public function setTempStartYear($value) {
			$this->TempStartYear = $value;
		}
		public function setTempEndYear($value) {
			$this->TempEndYear = $value;
		}
		public function setTempAwardBearing($value) {
			$this->TempAwardBearing = $value;
		}
		public function setTempParticipation($value) {
			$this->TempParticipation = $value;
		}
		public function setTempParticipationNature($value) {
			$this->TempParticipationNature = $value;
		}
		public function setTempRole($value) {
			$this->TempRole = $value;
		}
		public function setTempAchievement($value) {
			$this->TempAchievement = $value;
		}
		public function setTempDescription($value) {
			$this->TempDescription = $value;
		}
		public function setTempApprovalDate($value) {
			$this->TempApprovalDate = $value;
		}
		public function setTempApprovedBy($value) {
			$this->TempApprovedBy = $value;
		}
		public function setTempOEA_ProgramCode($value) {
			$this->TempOEA_ProgramCode = $value;
		}
		public function setTempOLE_STUDENT_RecordID($value) {
			$this->TempOLE_STUDENT_RecordID = $value;
		}
		public function setTempOLE_PROGRAM_ProgramID($value) {
			$this->TempOLE_PROGRAM_ProgramID = $value;
		}
		public function setTempRecordStatus($value) {
			$this->TempRecordStatus = $value;
		}
		public function setNewRecord($val) {
			$this->NewRecord = $val;
		}
		public function getNewRecord() {
			return $this->NewRecord;	
		}
		public function getMaxNoOfOea() {
			return $this->MaxOEAItem;	
		}
		public function setEditMode($val) {
			$this->EditMode = $val;
		}
		
		
		public function getDefaultParticipation($associateAry=1)
		{
			global $oea_cfg;
			if($associateAry)
				return $oea_cfg["OEA_Participation_Type"];
			else  {
				$_c = 0;
				if(sizeof($oea_cfg["OEA_Participation_Type"])>0) {
					foreach($oea_cfg["OEA_Participation_Type"] as $_key=>$_val) {
						$_ary[$_c][] = $_key;
						$_ary[$_c][] = $_val;
						$_c++;
					}	
				}
				return $_ary;	
			}	
		}

		private function loadRecordFromStorage(){
			global $eclass_db;			
			$result = null;
			
			$_recordID = $this->recordID;

			if( (trim($_recordID) == "") || (intval($_recordID) < 0)) {
				//DO NOTHING
//				$result = null;

			}else{
				
				$sql = "select		
							RecordID,StudentID,Title,
							StartYear,EndYear,OEA_AwardBearing, Participation,
							Role,ParticipationNature,Achievement, Description,
							OEA_ProgramCode,OLE_STUDENT_RecordID,OLE_PROGRAM_ProgramID,
							RecordStatus,
							InputDate,InputBy,ApprovalDate,ApprovedBy,ModifyDate,ModifiedBy 
						from 
							{$eclass_db}.OEA_STUDENT 
						where 
							RecordID = ".$this->recordID;
				$resultSet = $this->objDB->returnArray($sql);
				
				if(is_array($resultSet) && sizeof($resultSet) == 1){
					$this->StudentID = $resultSet[0]["StudentID"];
					$this->Title = $resultSet[0]["Title"];
					$this->StartYear = $resultSet[0]["StartYear"];
					$this->EndYear = $resultSet[0]["EndYear"];
					$this->OEA_AwardBearing = $resultSet[0]["OEA_AwardBearing"];
					$this->Participation = $resultSet[0]["Participation"];
					$this->Role = $resultSet[0]["Role"];
					$this->ParticipationNature = $resultSet[0]["ParticipationNature"];
					$this->Achievement = $resultSet[0]["Achievement"];
					$this->Description = $resultSet[0]["Description"];
					$this->OEA_ProgramCode = $resultSet[0]["OEA_ProgramCode"];
					$this->OLE_STUDENT_RecordID = $resultSet[0]["OLE_STUDENT_RecordID"];
					$this->OLE_PROGRAM_ProgramID = $resultSet[0]["OLE_PROGRAM_ProgramID"];
					$this->RecordStatus = $resultSet[0]["RecordStatus"];
					$this->InputDate = $resultSet[0]["InputDate"];
					$this->InputBy = $resultSet[0]["InputBy"];
					$this->ApprovalDate = $resultSet[0]["ApprovalDate"];
					$this->ApprovedBy = $resultSet[0]["ApprovedBy"];
					$this->ModifyDate = $resultSet[0]["ModifyDate"];
					$this->ModifiedBy  = $resultSet[0]["ModifiedBy"];
				}
			}
//			return $result;

		}

		public function save()
		{
			if((trim($this->recordID) != "") && (intval($this->recordID) > 0)) {
				$resultId = $this->update_record();
			}
			else{	
				$resultId = $this->new_record();
			}

			return $resultId;

		}
		
		private function update_record()
		{
			global $eclass_db, $UserID, $oea_cfg;
			
			include_once("liboea.php");
			$liboea = new liboea();
			
//			if($_SESSION['UserType']==USERTYPE_STUDENT) {
//				if($liboea->getOeaAutoApproval()==1 || $this->TempParticipation=='P') {
//					$field = "RecordStatus='".$oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"]."', ";
//				} else {
//					$field = "RecordStatus='".$oea_cfg["OEA_STUDENT_RECORDSTATUS_PENDING"]."', ";	
//				}	
//			} else {
//				$field = "RecordStatus='".$oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"]."', ";
//			}

			$sql = "UPDATE 
						{$eclass_db}.OEA_STUDENT 
					SET 
						Title='".$this->objDB->Get_Safe_Sql_Query($this->TempTitle)."', 
						StartYear='".$this->TempStartYear."', 
						EndYear='".$this->TempEndYear."', 
						OEA_AwardBearing='".$this->TempAwardBearing."', 
						Participation='".$this->TempParticipation."', 
						Role='".$this->TempRole."', 
						ParticipationNature='".$this->TempParticipationNature."', 
						Achievement='".$this->TempAchievement."', 
						Description='".$this->objDB->Get_Safe_Sql_Query($this->TempDescription)."',
						OEA_ProgramCode='".$this->TempOEA_ProgramCode."', 
						RecordStatus='".$this->TempRecordStatus."',
						ModifyDate=NOW(), 
						ModifiedBy='$UserID'
					WHERE
						RecordID = '".$this->recordID."'";

			$result = $this->objDB->db_db_query($sql);
			
			return $result;

		}
		
		private function new_record()
		{
			global $eclass_db, $UserID;
			
			# check duplication
			$sql = "SELCET COUNT(*) FROM {$eclass_db}.OEA_STUDENT WHERE StudentID='".$this->TempStudentID."' AND OLE_STUDENT_RecordID='".$this->TempOLE_STUDENT_RecordID."'";
			$count = $this->objDB->returnVector($sql);
			
			if($count[0]==0) {
				include_once("liboea.php");
				$liboea = new liboea();
				
//				if($liboea->getOeaAutoApproval()==1 || $this->TempParticipation=='P') {
//					$approveField = ", ApprovalDate, ApprovedBy";	
//					$approveValue = ", NOW(), '$UserID'";
//				}
				
				if ($this->TempApprovalDate != '') {
					$approveDateField = ", ApprovalDate";
					$approveDateValue = ", ".$this->TempApprovalDate;
				}
				if ($this->TempApprovedBy != '') {
					$approvedByField = ", ApprovedBy";
					$approvedByValue = ", ".$this->TempApprovedBy;
				}
				
				$sql = "INSERT INTO {$eclass_db}.OEA_STUDENT
							(StudentID, Title, StartYear, EndYear, OEA_AwardBearing, Participation, Role, ParticipationNature, Achievement, Description, OEA_ProgramCode, OLE_STUDENT_RecordID, OLE_PROGRAM_ProgramID, RecordStatus, InputDate, InputBy, ModifyDate, ModifiedBy $approveDateField $approvedByField)
						VALUES 
							('".$this->TempStudentID."', '".$this->objDB->Get_Safe_Sql_Query($this->TempTitle)."', '".$this->TempStartYear."', '".$this->TempEndYear."', '".$this->TempAwardBearing."', '".$this->TempParticipation."', '".$this->TempRole."', '".$this->TempParticipationNature."', '".$this->TempAchievement."', '".$this->objDB->Get_Safe_Sql_Query($this->TempDescription)."', '".$this->TempOEA_ProgramCode."', '".$this->TempOLE_STUDENT_RecordID."', '".$this->TempOLE_PROGRAM_ProgramID."', '".$this->TempRecordStatus."', NOW(), '$UserID', NOW(), '$UserID' $approveDateValue $approvedByValue)
						";
				$result = $this->objDB->db_db_query($sql);
				$thisRecordID = $this->objDB->db_insert_id();
				$this->recordID = $thisRecordID;
			}
			
			return $this->recordID;
		}
		
		
		public function get_OEA_Role_Array()
		{
			global $oea_cfg;
			$ary = $oea_cfg["OEA_Role_Type"];
			return $ary; 	
		}		
		public function Get_OEA_RoleName_By_RoleCode($Code)
		{
			$InfoAssoArr = $this->get_OEA_Role_Array();
			return $InfoAssoArr[$Code];
		}
		
		public function get_OEA_Achievement_Array()
		{
			global $oea_cfg;
			$ary = $oea_cfg["OEA_Achievement_Type"];
			return $ary; 	
		}		
		public function Get_OEA_AchievementName_By_AchievementCode($Code)
		{
			$InfoAssoArr = $this->get_OEA_Achievement_Array();
			return $InfoAssoArr[$Code];
		}
		
		public function get_OEA_ParticipationNature_Array()
		{
			global $oea_cfg;
			$ary = $oea_cfg["OEA_ParticipationNature_Type"];
			return $ary;	
		}		
		public function Get_OEA_ParticipationNatureName_By_ParticipationNatureCode($Code)
		{
			$InfoAssoArr = $this->get_OEA_ParticipationNature_Array();
			return $InfoAssoArr[$Code];
		}
		
		public function get_OEA_Participation_Array()
		{
			global $oea_cfg;
			$ary = $oea_cfg["OEA_Participation_Type"];
			return $ary;	
		}		
		public function Get_OEA_ParticipationName_By_ParticipationCode($Code)
		{
			$InfoAssoArr = $this->get_OEA_Participation_Array();
			return $InfoAssoArr[$Code];
		}
		
		public function get_OEA_Pending_Status()
		{
			global $oea_cfg;
			return $oea_cfg["OEA_STUDENT_RECORDSTATUS_PENDING"];
		}
		
		public function get_OEA_Approved_Status()
		{
			global $oea_cfg;
			return $oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"];
		}
		
		public function get_OEA_Rejected_Status()
		{
			global $oea_cfg;
			return $oea_cfg["OEA_STUDENT_RECORDSTATUS_REJECTED"];
		}
		
		public function OeaQuotaUsed() {
			global $eclass_db, $UserID;
			$sql = "SELECT COUNT(*) FROM {$eclass_db}.OEA_STUDENT WHERE StudentID='$UserID'";
			$count = $this->objDB->returnVector($sql);
			return $count[0];
		}
		public function OeaQuotaRemain() {
			include_once("liboea.php");
			$liboea = new liboea();
			$maxOeaAllow = $liboea->getMaxNoOfOea();
			$OeaUsed = $this->OeaQuotaUsed();
			return ($maxOeaAllow - $OeaUsed);
		}
		
		public function checkDisplayNewButton()
		{
			global $UserID, $eclass_db;
			
			$count = $this->OeaQuotaUsed();

			return ($count>=$this->MaxOEAItem) ? 0 : 1;	
		}

		public function getAwardBearing($ItemCode="", $OLE_ProgramID="")
		{
			global $oea_cfg, $eclass_db;
			
			if($ItemCode!="") {
				$sql = "SELECT DefaultAwardBearing FROM {$eclass_db}.OEA_OLE_MAPPING WHERE OEA_ProgramCode='$ItemCode'";
				$awardBearing = $this->objDB->returnVector($sql);
				
			} else if($OLE_ProgramID!="") {
				$sql = "SELECT DefaultAwardBearing FROM {$eclass_db}.OEA_OLE_MAPPING WHERE OLE_ProgramID='$OLE_ProgramID'";
				$awardBearing = $this->objDB->returnVector($sql);
			}
			return ($awardBearing[0]=="Y") ? 1 : 0;
		}
		
		public function getOeaItem_NewRelated()
		{	# create / edit OEA record which is independent on OLE 
				
		}
		
		public function getOeaItemTable()
		{	# create / edit OEA record
			$table = $this->get_New_Edit_OEA_Table();
			return $table;
		}
		
		private function get_Edit_OEA_Table() 
		{
			# Do nothing
		}
		
		private function get_New_Edit_OEA_Table()
		{
			global $Lang, $PATH_WRT_ROOT, $intranet_session_language, $i_To, $oea_cfg, $i_general_yes, $i_general_no, $i_general_or, $button_edit;
			
			include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libole_student_item.php");
			include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");
			include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
			include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting_search.php");
			
			$liboea = new liboea();
			$liboea_setting = new liboea_setting();
			$liboea_setting_search = new liboea_setting_search($liboea_setting);
			
			
			# OLE Item 
			if($this->TempOLE_STUDENT_RecordID=="") {		# retrieve OLE data from OEA id
				$ole_item = new libole_student_item($this->OLE_STUDENT_RecordID);
			} else { 										# retrieve OLE data from OLE id
				$ole_item = new libole_student_item($this->TempOLE_STUDENT_RecordID);
			}
			
			# OLE Date	
			$ole_date = $ole_item->getOLEDate();
			
			# OLE Role
			$ole_role = ($ole_item->getRole()=="") ? "-" : $ole_item->getRole();
			
			# OLE Achievement 
			$ole_achievement = ($ole_item->getAchievement()=="") ? "-" : $ole_item->getAchievement();
			
			# OLE Details	
			$ole_details = $ole_item->getDetails();
			$ole_details = ($ole_details=='')? '&nbsp;' : $ole_details;
			
			# OLE IntExt
			$ole_IntExt = (strtoupper($ole_item->getIntExt())=="INT") ? $Lang['iPortfolio']['OEA']['OLE_Int'] : $Lang['iPortfolio']['OEA']['OLE_Ext'];
			
			# prepare OEA Item
			$oea_cat_ary = $liboea_setting->get_OEA_Item($this->OEA_ProgramCode);
			
			# prepare OEA Program Code
			$mapped_OEA_Code = $liboea_setting->getDefaultMappedOEACode($ole_item->getProgramID());
			
			if($this->OEA_ProgramCode=="" && $mapped_OEA_Code=="") {
				$OEA_Code_Display = "";
			} else if($mapped_OEA_Code=="") {
				$OEA_Code_Display = $this->OEA_ProgramCode;
				$remarkField = "<span class='tabletextrequire'>*</span> <span class='tabletextremark'>".$Lang['iPortfolio']['OEA']['Self_Mapped']."</span>";
				$asterisk = "<span class='tabletextrequire' style='float:left;'>*</span>";
			} else {
				$OEA_Code_Display = $mapped_OEA_Code;
				$remarkField = "<span  class='tabletextrequire'>#</span> <span class='tabletextremark'>".$Lang['iPortfolio']['OEA']['MappedByTeacher']."</span>";
				$asterisk = "<span class='tabletextrequire' style='float:left;'>#</span>";
			}
				
			# prepare OEA Category	
			$oea_cat = ($oea_cat_ary[$OEA_Code_Display]['CatName']=="") ? "-" : $oea_cat_ary[$OEA_Code_Display]['CatName'];
			
			# prepare OEA Item
			$stored_oea_title = intranet_htmlspecialchars($this->Title);
			$oea_title = '';
			if ($stored_oea_title != '') {
				$oea_title = $stored_oea_title;
			}
			else {
				if($this->OEA_ProgramCode!=$oea_cfg["OEA_Default_Category_Others"]) {
					$oea_title = $oea_cat_ary[$OEA_Code_Display]['ItemName'];
				}
				if($this->OEA_ProgramCode==$oea_cfg["OEA_Default_Category_Others"]) {
					$oea_title .= $this->Title;
				}
			}
			
			if($oea_title == "") { 
				if($this->NewRecord) {
					$preloadText = $Lang['iPortfolio']['OEA']['NoInfo'];
				} else {
					$preloadText = $Lang['iPortfolio']['OEA']['Mapping_No_OEA_Record_Is_Mapped'];
				}
				
				$oea_title = "<b><span  class='tabletextrequire'>".$preloadText."</span></b>";
				if($this->NewRecord) {
					//$editLink = " (<a href='javascript:;' onClick='CopyOthers()'>$button_edit</a>)";
				}
			} else {
				$oea_title = "<b>".$oea_title."</b>";
			}
			if($_SESSION['UserType']==USERTYPE_STAFF && $oea_cat_ary[$OEA_Code_Display]['ItemCode']==$oea_cfg["OEA_Default_Category_Others"]) {
				//$editLink = " (<a href='javascript:;' onClick='CopyOthers()'>$button_edit</a>)";	
			}		
			
					
			# prepare OEA Date
			$startyear = ($this->TempStartYear=="") ? $this->getStartYear() : $this->TempStartYear;
			if($startyear=="") $startyear = date("Y");
			$endyear = ($this->TempStartYear=="") ? $this->getEndYear() : $this->TempEndYear;
			if($endyear=="") $endyear = date("Y");
			
			# prepare OEA Item stored value
			$oea_itemSelect = "<span id='selectedItemSpan' style='float:left;'>".$oea_title." $editLink </span><input type='hidden' name='ItemCode' id='ItemCode' value='".$OEA_Code_Display."'>";
			
			if($this->EditMode && $mapped_OEA_Code=='') {
				$divStyle = "display:block;";	
			} else {
				$divStyle = "display:none;";		
			}
			$oea_itemSelect .= "<span id='divPencil' class='table_row_tool' style='float:left; $divStyle'><a title='".$button_edit."' class='edit_dim' href='javascript:;' onClick='js_show_layer(\"MapOptions\")'></a></span>";
			$oea_itemSelect .= "<br style='clear:both;' />";
			
			if($this->EditMode) {
				$divStyle = "display:none;";	
			} else {
				$divStyle = "display:block;";	
			}
			$oea_itemSelect .= "<div id='MapOptions' style='$divStyle'>";

			if($mapped_OEA_Code=="") {
			$oea_itemSelect .= "<br>";
$oea_itemSelect .= $Lang['iPortfolio']['OEA']['selectMapMethod'].'<br/>'; 
				if(!$this->NewRecord) {			# display suggestion option
					$ole_title = $ole_item->getTitle();
					$liboea_setting_search->setCriteriaTitle($ole_title);
					$liboea_setting_search->getSuggestResult();
					if(sizeof($liboea_setting_search->getSearchResultTitle())>0) {
						if($_SESSION['UserType']==USERTYPE_STAFF) {
							$ItemSuggestCountDisplay = "- <a href='javascript:;' onClick=\"loadNextPage('SearchDiv','suggestionList')\" class='setting_row' rel='advance_options' title='".$Lang['iPortfolio']['OEA']['SuggestedItems']."'>".$Lang['iPortfolio']['OEA']['SuggestedItems']." (".$Lang['iPortfolio']['OEA']['MatchItemCaption']." ".sizeof($liboea_setting_search->getSearchResultTitle()).")</a>";
						} else {
							$ItemSuggestCountDisplay = "- <a href='#TB_inline?height=500&width=750&inlineId=FakeLayer' onclick='Show_SuggestionList(); return false;' class='setting_row thickbox' title='".$Lang['iPortfolio']['OEA']['SuggestedItems']."'>".$Lang['iPortfolio']['OEA']['SuggestedItems']." (".$Lang['iPortfolio']['OEA']['MatchItemCaption']." ".sizeof($liboea_setting_search->getSearchResultTitle()).")</a>";
						}	
					} else {
						$ItemSuggestCountDisplay = "- ".$Lang['iPortfolio']['OEA']['SuggestedItems']." (".$Lang['iPortfolio']['OEA']['MatchItemCaption']." ".sizeof($liboea_setting_search->getSearchResultTitle()).")";
					}
//					$oea_itemSelect .= $ItemSuggestCountDisplay."; $i_general_or <br>";
					$oea_itemSelect .= $ItemSuggestCountDisplay."<br>";
					
				}
			
				if($_SESSION['UserType']==USERTYPE_STAFF) {	# teacher view, display advance search in layer
					$oea_itemSelect .= "
						- <a href='javascript:;' onClick=\"loadNextPage('SearchDiv','advanceSearch')\" class='setting_row' rel='advance_options' title='".$Lang['iPortfolio']['OEA']['AdvanceSearch']."'>".$Lang['iPortfolio']['OEA']['AdvanceSearch']."</a> <br>";	
					
				} else {		# student view, display advance search in thickbox
					$oea_itemSelect .= "
						- <a href='#TB_inline?height=500&width=750&inlineId=FakeLayer' onclick='Show_Advance_Search(); return false;' class='setting_row thickbox' rel='advance_options' title='".$Lang['iPortfolio']['OEA']['AdvanceSearch']."'>".$Lang['iPortfolio']['OEA']['AdvanceSearch']."</a> <br>";
				}
			# option "Others"
				if(!$this->NewRecord) {	# from OLE
					$oea_itemSelect .= "	
						- <a href='javascript:;' onClick='CopyOthers()' rel='advance_options' id='oea_category_others'>".$Lang['iPortfolio']['OEA']['UseOleName']."</a><br>";
				} else {				# created from "NEW"
					$oea_itemSelect .= "	
						- <a href='javascript:;' onClick='CopyOthers()' rel='advance_options' id='oea_category_others'>".$Lang['iPortfolio']['OEA']['SelfInput']."</a><br>";
				}
			}
			$oea_itemSelect .= "</div>";
			
			# OEA category stored value
			$oea_catSelect = "<span id='span_OEA_Category'>".$oea_cat."</span><input type='hidden' name='CatCode' id='CatCode' value='".$oea_cat_ary[$OEA_Code_Display]['CatCode']."'>";
			
			# OEA date
			$oea_date = $this->getYearSelect('name="startdate" id="startdate"',$startyear)." $i_To ".$this->getYearSelect('name="enddate" id="enddate"',$endyear);
			
			# OEA Role selection
			$CanEditMappedRole = ($liboea->getForceStudentToApplyRoleMapping())? false : true;
			$ShowTeacherMappedText = false;
			$ShowRoleSelection = false;
			$ShowRoleText = false;
			$DisabledRoleCodeArr = '';
			
			$OleRole = $ole_item->getRole();
			$OeaMappedRole = $this->Get_OLE_OEA_Role_Mapping_By_OLE_Role($OleRole);
			
			if ($this->EditMode) {
				if($mapped_OEA_Code == '' || $OeaMappedRole == '') {
					// OEA not mapped from OLE or OLE Role not mapped
					$ShowRoleSelection = true;
				}
				else {
					// 1. OEA mapped from OLE
					// 2. OLE role mapped
					if ($CanEditMappedRole) {
						// Can override teacher mapped role
						$ShowTeacherMappedText = true;
						$ShowRoleSelection = true;
					}
					else {
						// CANNOT override teacher mapped role
						if ($OeaMappedRole == $this->getRole()) {
							// OEA Role is the same as the OLE mapped role 
							$ShowRoleText = true;
						}
						else {
							// OEA Role is NOT the same as the OLE mapped role => Show the role selection
							$ShowTeacherMappedText = true;
							$ShowRoleSelection = true;
							$PerformRoleFormChecking = 1;
							
							// Only allow the user to choose the existing role or the teacher mapped role
							$RoleAry = $this->get_OEA_Role_Array();
							foreach((array)$RoleAry as $thisRoleKey => $thisRoleLang) {
								if ($thisRoleKey != $this->getRole() && $thisRoleKey != $OeaMappedRole) {
									$DisabledRoleCodeArr[] = $thisRoleKey;
								}
							}
						}
					}
				}
			}
			else {
				if ($this->NewRecord) {
					// Create new OEA => Show role selection
					$ShowRoleSelection = true;
				}
				else {
					// Map from OLE
					if ($OeaMappedRole) {
						if ($CanEditMappedRole) {
							// Role mapped by teacher and can be changed
							$ShowTeacherMappedText = true;
							$ShowRoleSelection = true;
							$this->setRole($OeaMappedRole);		// preset selection value
						}
						else {
							// Role mapped by teacher and cannot be changed => show role text and set hidden form field
							$ShowRoleText = true;
						}
					}
					else {
						// Role NOT mapped by teacher => show role selection
						$ShowRoleSelection = true;
					}
				}
			}
			
			$roleDisplay = '';
			if ($ShowTeacherMappedText) {
				$roleDisplay .= '<span class="tabletextrequire">#</span> '.$Lang['iPortfolio']['OEA']['TeacherMapping'].': <b>'.$this->Get_OEA_RoleName_By_RoleCode($OeaMappedRole).'</b>';
				$roleDisplay .= '<br />';
			}
			if ($ShowRoleSelection) {
				$roleDisplay .= $this->getRoleSelection($DisabledRoleCodeArr);
			}
			if ($ShowRoleText) {
				$roleDisplay .= '<span class="tabletextrequire">#</span> '.$this->Get_OEA_RoleName_By_RoleCode($OeaMappedRole);
				$roleDisplay .= '<input type="hidden" id="RoleID" name="RoleID" value="'.$OeaMappedRole.'">';
			}
			
			
			# OEA Participation (Int / Ext)
			$OleIntExt = $ole_item->getIntExt();
			$oea_participationOption = $this->get_OEA_IntExt($OleIntExt);
			
			
			# OEA Achievement (Award Bearing + Participation Nature + Achievement)
			$award = $this->getAwardSelections();
			
			# OEA Description
			$oea_description = $this->getDescriptionTextarea();
			
			#####################
			# table content
			#####################
			$firstColumnWidth = 10;
			
			$h_result = "<div id='TableDiv'>";
			$h_result .= "<table class='common_table_list_v30'>";
			$h_result .= "<th width='$firstColumnWidth%'>&nbsp; </th>";
			if(!$this->NewRecord) {
				$h_result .= "<th width='$secondColumnWidth%'>".$Lang['iPortfolio']['OEA']['OLE']."</th>";
				$secondColumnWidth = 35;
			}
			$thirdColumnWidth = 100-$firstColumnWidth-$secondColumnWidth;
			$h_result .= "<th width='$thirdColumnWidth%'>".$Lang['iPortfolio']['OEA']['OEA_Name2']."</th>";
			
			$h_result .= "<tr>";
			$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Title']."</td>";
			if(!$this->NewRecord) {
				$h_result .= "<td>".$ole_item->getTitle()."</td>";
			}
			$h_result .= "<td>{$asterisk} {$oea_itemSelect}</td>";
			$h_result .= "</tr>";
			$h_result .= "<tr>";
			$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Category']."</td>";
			if(!$this->NewRecord) {
				$h_result .= "<td>".$ole_item->getCategoryTitle($intranet_session_language)."</td>";
			}
			$h_result .= "<td>$oea_catSelect</td>";
			$h_result .= "</tr>";
			$h_result .= "<tr>";
			$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Date']."</td>";
			if(!$this->NewRecord) {
				$h_result .= "<td>$ole_date</td>";
			}
			$h_result .= "<td>$oea_date</td>";
			$h_result .= "</tr>";
			$h_result .= "<tr>";
			$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Role']."</td>";
			if(!$this->NewRecord) {
				$h_result .= "<td>$ole_role</td>";
			}
			$h_result .= "<td>$roleDisplay</td>";
			$h_result .= "</tr>";
			$h_result .= "<tr>";
			$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Achievement']."</td>";
			if(!$this->NewRecord) {
				$h_result .= "<td>$ole_achievement</td>";
			}
			$h_result .= "<td>$award</td>";
			$h_result .= "</tr>";
			$h_result .= "<tr>";
			$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Participation']."</td>";
			if(!$this->NewRecord) {
				$h_result .= "<td>$ole_IntExt</td>";
			}
			$h_result .= "<td>$oea_participationOption</td>";
			$h_result .= "</tr>";
			
				### Description
				$h_result .= "<tr>";
					$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Description']."</td>";
					if(!$this->NewRecord) {
						$h_result .= "<td>$ole_details</td>";
					}
					$h_result .= "<td>$oea_description</td>";
				$h_result .= "</tr>";
			
			$h_result .= "</table>";
			$h_result .= "</div>";
			$h_result .= $remarkField;
			$h_result .= "<div id='SearchDiv'></div>";
			
			$h_result .= "<input type='hidden' name='ole_title' id='ole_title' value='".intranet_htmlspecialchars($ole_item->getTitle())."'>";
			$h_result .= "<input type='hidden' name='stored_oea_title' id='stored_oea_title' value='".$stored_oea_title."'>";
			
			return $h_result;
			
		}
		
		private function getRoleSelection($DisabledRoleCodeArr='') 
		{
			$RoleAry = $this->get_OEA_Role_Array();
			$rAry = array();
			$DisabledIndexArr = array();
			
			if(sizeof($RoleAry)>0) {
				$i = 0;
				foreach($RoleAry as $key=>$val) {
					$rAry[$i] = array($key, $val); 
					
					if ($DisabledRoleCodeArr!='' && in_array($key, (array)$DisabledRoleCodeArr)) {
						$DisabledIndexArr[] = $i;
					}
					
					$i++;	
				}	
			}
			$roleSelect = $this->getSelectByArray_OEA($rAry, 'name="RoleID" id="RoleID"', $this->getRole(), $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1, $DisabledIndexArr);	
			
			return $roleSelect;
		}
		
		private function get_OEA_IntExt($ole_int_ext='')
		{
			global $PATH_WRT_ROOT, $oea_cfg, $ipf_cfg, $sys_custom;
			global $Lang;
			
			$liboea = new liboea();
			
//			$oea_int_ext = $this->getParticipation();
//			
//			if($oea_int_ext=="S" || $oea_int_ext=="") {
//				$int_selected = " checked";
//			}
//			else {
//				$ext_selected = " checked";
//			}
//				
//			$oea_participationOption = '';
//			$oea_participationOption .= "<input type='radio' name='Participation' id='Participation_S' value='S' $int_selected><label for='Participation_S'>".$oea_cfg["OEA_Participation_Type"]["S"]."</label>";
//			$oea_participationOption .= "<br>";
//			$oea_participationOption .= "<input type='radio' name='Participation' id='Participation_P' value='P' $ext_selected><label for='Participation_P'>".$oea_cfg["OEA_Participation_Type"]["P"]."</label>";
//			
//			return $oea_participationOption;
			
			$ParticipationInfoArr = $this->getDefaultParticipation($associateAry=1);
			$oea_int_ext = $this->getParticipation();
			$isEdit = $this->EditMode;
			$isFromNewOEARecord = $this->getNewRecord();
			
			$CanApplyPart1Only = false;
			$OleType_ApplicablePartArr = array();
			
			if ($ole_int_ext!='') {
				// OEA mapped from OLE
				$ApplicableParticipationTypeArr = $liboea->getAllowOEAPartByOLEType($ole_int_ext);
			}
			else {
				// New OEA record directly
//				if ($sys_custom['iPf']['JUPASArr']['OEA']['NewRecordForPartIIOnly']) {
//					$ApplicableParticipationTypeArr = $oea_cfg["OEA_Participation_Type_Private"];
//				}
//				else {
//					foreach ((array)$ParticipationInfoArr as $thisParticipationCode => $thisParticipationLang) {
//						if ($liboea->getThisOEAPartIsAllowed($thisParticipationCode)) {
//							$ApplicableParticipationTypeArr[] = $thisParticipationCode;
//						}
//					}
//				}

				if ($isFromNewOEARecord) {
					foreach ((array)$ParticipationInfoArr as $thisParticipationCode => $thisParticipationLang) {
						if ($liboea->getPartIsAllowForNewOEA($thisParticipationCode)) {
							$ApplicableParticipationTypeArr[] = $thisParticipationCode;
						}
					}
				}
				else {
					foreach ((array)$ParticipationInfoArr as $thisParticipationCode => $thisParticipationLang) {
						if ($liboea->getThisOEAPartIsAllowed($thisParticipationCode)) {
							$ApplicableParticipationTypeArr[] = $thisParticipationCode;
						}
					}
				}
			}
			$numOfApplicableParticipationType = count((array)$ApplicableParticipationTypeArr);
			
			$EnableBothRadio = false;
			$ShowTeacherSuggestion = false;
			$ShowParticipationText = false;
			$ShowParticipationRadio = false;
			$x = '';
			
			//if ($isEdit && $ole_int_ext!='' && !in_array($oea_int_ext, (array)$ApplicableParticipationTypeArr)) {
			if ($isEdit && !in_array($oea_int_ext, (array)$ApplicableParticipationTypeArr)) {
				// Previously saved settings but now the settings is invalid => Show teacher's suggestion and allow student to choose again
				$EnableBothRadio = true;
				$ShowTeacherSuggestion = true;
				$TargetParticipationCode = ($oea_int_ext==$oea_cfg["OEA_Participation_Type_School"])? $oea_cfg["OEA_Participation_Type_Private"] : $oea_cfg["OEA_Participation_Type_School"];
			}
			
			if (!$EnableBothRadio && $numOfApplicableParticipationType==1) {
				// Only Applicable to one type => Show the mapped type without radio buttons
				$ShowParticipationText = true;
				$TargetParticipationCode = $ApplicableParticipationTypeArr[0];
			}
			else {
				$Part1Checked = '';
				$Part2Checked = '';
				$ShowParticipationRadio = true;
					
				if($oea_int_ext==$oea_cfg["OEA_Participation_Type_School"] || $oea_int_ext=="") {
					$Part1Checked = " checked";
				}
				else {
					$Part2Checked = " checked";
				}
				
				//if ($ole_int_ext != '') {
					// for OEA which is mapped from OLE only
					
					$Part1Disabled = '';
					$Part2Disabled = '';
					if (in_array($oea_cfg["OEA_Participation_Type_School"], (array)$ApplicableParticipationTypeArr)==false) {
						if (!$EnableBothRadio) {
							$Part1Disabled = 'disabled';
						}
						$ShowTeacherSuggestion = true;
						$TargetParticipationCode = $oea_cfg["OEA_Participation_Type_Private"];
					}
					if (in_array($oea_cfg["OEA_Participation_Type_Private"], (array)$ApplicableParticipationTypeArr)==false) {
						if (!$EnableBothRadio) {
							$Part2Disabled = 'disabled';
						}
						$ShowTeacherSuggestion = true;
						$TargetParticipationCode = $oea_cfg["OEA_Participation_Type_School"];
					}
				//}
			}
			
			
			if ($ShowTeacherSuggestion) {
				$x .= '<span class="tabletextrequire">#</span> '.$Lang['iPortfolio']['OEA']['TeacherMapping'].': <b>'.$this->Get_OEA_ParticipationName_By_ParticipationCode($TargetParticipationCode).'</b>';
				$x .= '<br />';
			}
			
			if ($ShowParticipationText) {
				$x .= '<span class="tabletextrequire">#</span> '.$this->Get_OEA_ParticipationName_By_ParticipationCode($TargetParticipationCode);
				$x .= '<input type="hidden" id="Participation" name="Participation" value="'.$TargetParticipationCode.'" />';
			}
			
			if ($ShowParticipationRadio) {
				$x .= "<input type='radio' name='Participation' id='Participation_S' value='".$oea_cfg["OEA_Participation_Type_School"]."' $Part1Checked $Part1Disabled><label for='Participation_S'>".$this->Get_OEA_ParticipationName_By_ParticipationCode($oea_cfg["OEA_Participation_Type_School"])."</label>";
				$x .= "<br>";
				$x .= "<input type='radio' name='Participation' id='Participation_P' value='".$oea_cfg["OEA_Participation_Type_Private"]."' $Part2Checked $Part2Disabled><label for='Participation_P'>".$this->Get_OEA_ParticipationName_By_ParticipationCode($oea_cfg["OEA_Participation_Type_Private"])."</label>";
			}
			
			return $x;
		}
		
		private function getAwardSelections() 
		{
			global $PATH_WRT_ROOT, $Lang, $i_general_yes, $i_general_no;
			
			include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libole_student_item.php");
			
			$_id = ($this->OLE_STUDENT_RecordID=="") ? $this->TempOLE_STUDENT_RecordID : $this->OLE_STUDENT_RecordID;
			
			$ole_item = new libole_student_item($_id);
			$ole_programID = $ole_item->getProgramID();
			
			//echo $_id;
			$mappedItemCode = $ole_item->getMapped_OEA_ProgramCode();	
			
			$defaultAwardBearing = 1;
			if($mappedItemCode) {
				$defaultAwardBearing = $this->getAwardBearing('', $ole_programID);
			} 
			
			
			//echo $defaultAwardBearing.'/';
			$h_HiddenOLE_Item = '';
			if($mappedItemCode=='' || $defaultAwardBearing) { # with award bearing
			
				$AwardBearing = '';
				
				if ($mappedItemCode != '') {
					$AwardBearing .= '<span class="tabletextrequire">#</span> '; 
				}
				$AwardBearing .= $Lang['iPortfolio']['OEA']['OEA_AwardBearing'];
				
				
				if($this->OEA_AwardBearing=="N") {
					$withoutAward = " checked";
				}
				else {
					$withAward = " checked";
				}
				if($mappedItemCode) {
					$AwardBearing .= "<br /><input type='hidden' name='awardBearing' id='awardBearing' value='Y' />";
				}
				else {
					$AwardBearing .= ": <input type='radio' name='awardBearing' id='awardBearing1' value='Y' $withAward onClick='changeAwardSetting(1)'><label for='awardBearing1'>".$i_general_yes."</label>";
					$AwardBearing .= "<input type='radio' name='awardBearing' id='awardBearing0' value='N' $withoutAward onClick='changeAwardSetting(0)'><label for='awardBearing0'>".$i_general_no."</label><br>";
				}
				
				
				$AchievementAry = $this->get_OEA_Achievement_Array();
				$aAry = array();
				if(sizeof($AchievementAry)>0) {
					$i = 0;
					foreach($AchievementAry as $key=>$val) {
						$aAry[$i] = array($key,$val); $i++;	
					}	
				}
				if($this->OEA_AwardBearing=="N")
					$disabled = " disabled";
					
				$achievementSelect = getSelectByArray($aAry, 'name="AchievementID" id="AchievementID" '.$disabled, $this->Achievement);
				/*
				# OEA Participation Nature
				$ParticipationNatureAry = $this->get_OEA_ParticipationNature_Array();
				$aAry = array();
				if(sizeof($ParticipationNatureAry)>0) {
					$i = 0;
					foreach($ParticipationNatureAry as $key=>$val) {
						$aAry[$i] = array($key,$val); $i++;	
					}	
				}
				*/
				$participationNatureSelect = $this->getParticipationNatureSelect();
			} else {	# without award bearing
				$AwardBearing = '<span class="tabletextrequire">#</span> '.$Lang['iPortfolio']['OEA']['OEA_NotAwardBearing'];
				
				$h_HiddenOLE_Item .= "<input type='hidden' name='awardBearing' id='awardBearing' value='N'>";
				$h_HiddenOLE_Item .= "<input type='hidden' name='AchievementID' id='AchievementID' value='N'>";
				/*
				# OEA Participation Nature
				$ParticipationNatureAry = $this->get_OEA_ParticipationNature_Array();
				
				$aAry = array();
				if(sizeof($ParticipationNatureAry)>0) {
					$i = 0;
					foreach($ParticipationNatureAry as $key=>$val) {
						if($key!="C") {
							$aAry[$i] = array($key,$val); $i++;
						}	
					}	
				}
				*/
				$participationNatureSelect = $this->getParticipationNatureSelect();
			}
			
			
			$option = $AwardBearing." ".$participationNatureSelect." ".$achievementSelect." ".$h_HiddenOLE_Item;
			return $option;
		} 
		
		private function getDescriptionTextarea() {
			global $linterface, $oea_cfg;
			
			$Description = $this->getDescription();
			
			$ProgramCode = $this->getOEA_ProgramCode();
//			if ($ProgramCode == $oea_cfg["OEA_Default_Category_Others"]) {
//				$readonly = '0';
//			}
//			else {
//				$readonly = '1';
//			}
			$readonly = '0';
			
			$onkeyup = ' onkeyup="js_Onkeyup_Info_Textarea(\'Description\', \'DescWordCountSpan\', \''.$oea_cfg["OEA_STUDENT_Description"]["MaxWordCount"].'\', \''.$oea_cfg["OEA_STUDENT_Description"]["MaxLength"].'\');" ';
			
			$x = '';
			$x .= $linterface->GET_TEXTAREA('Description', $Description, $taCols=70, $taRows=5, $OnFocus = "", $readonly, $onkeyup, $class='', $taID='');
			$x .= '<br />';
			$x .= liboea::Get_Word_Count_Div('DescWordCountSpan');
			return $x;
		}
		
		private function getParticipationNatureSelect() 
		{
			# OEA Participation Nature
			$ParticipationNatureAry = $this->get_OEA_ParticipationNature_Array();
			
			$_id = ($this->OLE_STUDENT_RecordID=="") ? $this->TempOLE_STUDENT_RecordID : $this->OLE_STUDENT_RecordID;
			
			$ole_item = new libole_student_item($_id);
			$ole_programID = $ole_item->getProgramID();
			
			//echo $_id;
			$mappedItemCode = $ole_item->getMapped_OEA_ProgramCode();	
			
			$defaultAwardBearing = 1;
			if($mappedItemCode) {
				$defaultAwardBearing = $this->getAwardBearing('', $ole_programID);	
			}
			if($this->OEA_AwardBearing=="N") 	# override default AwardBearing of mapping
				$defaultAwardBearing = 0;
				
			$aAry = array();
			if(sizeof($ParticipationNatureAry)>0) {
				$i = 0;
				
				foreach($ParticipationNatureAry as $key=>$val) {
					if($defaultAwardBearing || (!$defaultAwardBearing && $key!="C")) {
						$aAry[$i] = array($key,$val); $i++;
					}	
				}	
			}
			
			$participationNatureSelect = getSelectByArray($aAry, 'name="ParticipationNature" id="ParticipationNature"', $this->ParticipationNature);
			
			return $participationNatureSelect;
		}
		
		public function getYearSelect($tag="", $selected="")
		{
			global $oea_cfg;
			$currentYear = date("Y");
			$lowerBound = $currentYear - $oea_cfg["OEA_YearSelection_NoOfYearBefore"];
			$upperBound = $currentYear + $oea_cfg["OEA_YearSelection_NoOfYearAfter"];
			
			for($i=$lowerBound; $i<=$upperBound; $i++) {
				$years[] = array($i, $i);	
			}
			
			$yearSelect = getSelectByArray($years, $tag, $selected, 0, 1);
			
			return $yearSelect;

		}
		
		public function get_Selected_Oea_Item_List_SQL()
		{
			global $eclass_db, $UserID, $i_To, $ipf_cfg, $Lang;
			
			# with linkage with OEA item			
			$sql = "SELECT 
						p.title AS 'TITLE',
						IF(o.RecordID IS NULL, CONCAT('<input type =\"checkbox\" name=\"recordID[]\" id=\"recordID[]\" value=\"' , s.recordid, '\" onClick=\"countTotal()\" />'),'&nbsp;') AS 'ACTION',
						o.RecordID AS OEA_RecordID,
						s.RecordID AS OLE_Student_RecordID,
						IF(m.OEA_ProgramCode IS NOT NULL, m.OEA_ProgramCode, o.OEA_ProgramCode) as OEA_ProgramCode,
						p.StartDate as OLE_StartDate,
						IF(p.EndDate='0000-00-00','',p.EndDate) as OLE_EndDate,
						IF(m.OEA_ProgramCode IS NOT NULL, 1, 0) AS Mapped,
						CASE p.IntExt
							WHEN '".strtoupper($ipf_cfg["OLE_TYPE_STR"]["INT"])."' THEN '".$Lang['iPortfolio']['OEA']['Internal']."'
							WHEN '".strtoupper($ipf_cfg["OLE_TYPE_STR"]["EXT"])."' THEN '".$Lang['iPortfolio']['OEA']['External']."'
						END AS OLE_Type,
						If (s.SLPOrder Is Null, '".$Lang['General']['EmptySymbol']."', s.SLPOrder) as SLPOrder
					FROM 
						{$eclass_db}.OLE_STUDENT as s inner join
						{$eclass_db}.OLE_PROGRAM as p on p.programid = s.programid LEFT OUTER JOIN
						{$eclass_db}.OEA_STUDENT as o ON (o.OLE_STUDENT_RecordID=s.RecordID) LEFT OUTER JOIN
						{$eclass_db}.OEA_OLE_MAPPING m On (m.OLE_ProgramID=p.ProgramID)
					WHERE 
						s.UserID = '{$UserID}' AND (s.RecordStatus=".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]." or s.RecordStatus=".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"].") AND s.RecordID IN (SELECT OLE_STUDENT_RecordID FROM {$eclass_db}.OEA_STUDENT WHERE StudentID='$UserID')
					ORDER BY
						p.Title, OLE_StartDate";	
						//debug_pr($this->returnArray($sql));
//						echo $sql;
			return $sql;
		}	
		
		private function Get_OLE_OEA_Role_Mapping_Settings() {
			global $eclass_db;
			
			$OEA_OLE_ROLE_MAPPING = $eclass_db.'.OEA_OLE_ROLE_MAPPING';
			$sql = "Select
							LOWER(OLE_ROLE) as OLE_ROLE,
							OEA_ROLE
					From
							$OEA_OLE_ROLE_MAPPING
					";
			$ReturnArr = BuildMultiKeyAssoc($this->objDB->returnArray($sql, null, 1), 'OLE_ROLE', array('OEA_ROLE'), $SingleValue=1, $BuildNumericArray=0);
			
			// For testing only
			//$ReturnArr['Prize Winner'] = 'L';
			//$ReturnArr['Prize Winner'] = 'M';
			
			return $ReturnArr;
		}
		
		public function Get_OLE_OEA_Role_Mapping_By_OLE_Role($OleRole) {
			$MappingSettingsArr = $this->Get_OLE_OEA_Role_Mapping_Settings();
			return $MappingSettingsArr[strtolower($OleRole)];
		}
		
		private function getSelectByArray_OEA($data, $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1, $DisabledIndexArr='')
		{
			global $button_select,$i_status_all,$i_general_NotSet;
			$x = "<SELECT $tags>\n";
			if ($noFirst == 0)
			{
				$empty_selected = ($selected == '')? "SELECTED":"";
				if($FirstTitle=="")
				{
					if ($all==0)
					{
						$title = "-- $button_select --";
					}
					else if ($all == 2)
					{
						$title = "$i_general_NotSet";
					}
					else
					{
						$title = "$i_status_all";
					}
				}
				else
					$title = $FirstTitle;
		
				# Eric Yip : Quote values in pull-down list
				switch($ParQuoteValue)
				{
					case 1:
						$x .= "<OPTION value='' $empty_selected> $title </OPTION>\n";
						break;
					case 2:
						$x .= "<OPTION value=\"\" $empty_selected> $title </OPTION>\n";
						break;
				}
			}
		
			for ($i=0; $i<sizeof($data); $i++)
			{
				list ($id, $name) = $data[$i];
				if(is_array($selected))
					$sel_str = (in_array($id,$selected)? "SELECTED":"");
				else 
					$sel_str = ($selected == $id? "SELECTED":"");
					
				$thisDisabled = '';
				if ($DisabledIndexArr != '' && in_array($i, (array)$DisabledIndexArr)) {
					$thisDisabled = 'disabled';
				}
		
				# Eric Yip : Quote values in pull-down list
				switch($ParQuoteValue)
				{
					case 1:
						$x .= "<OPTION value='".htmlspecialchars($id,ENT_QUOTES)."' $sel_str $thisDisabled>$name</OPTION>\n";
						break;
					case 2:
						$x .= "<OPTION value=\"".htmlspecialchars($id,ENT_QUOTES)."\" $sel_str $thisDisabled>$name</OPTION>\n";
						break;
				}
			}
		
			$x .= "</SELECT>\n";
			return $x;
		}
				
	}

}
?>
