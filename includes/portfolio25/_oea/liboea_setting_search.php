<?
# modifying :  


if (!defined("LIBOEA_SETTING_SEARCH"))         // Preprocessor directives
{
	define("LIBOEA_SETTING_SEARCH",true);

	class liboea_setting_search{
		private $objOEA_Setting; //object of liboea_setting
		private $criteria_title;
		private $suggestLimit;
		private $suggestRankLowerLimit;
		private $SearchResultTitle;
		private $criteria_advance_search_title;
		private $advance_search_available_array;
		private $advanceSearchResult;
		private $ResultsetSize;
	

		public function liboea_setting_search($objOEA_Setting)
		{
			global $oea_cfg;
			$this->objOEA_Setting = $objOEA_Setting;
			$this->suggestLimit = $oea_cfg["OEA_SuggestListDisplayLimit"];
			$this->suggestRankLowerLimit = $oea_cfg["OEA_SuggestRankLowerLimit"];
			$this->ResultsetSize = 0;
		}

		public function set_setting($objOEA_Setting){	
			$this->objOEA_Setting = $objOEA_Setting;
		}

		public function setCriteriaTitle($str){
			$this->criteria_title = $str;
		}
		public function setResultsetSize($val) {
			$this->ResultsetSize = $val;	
		}
		public function getResultsetSize() {
			return $this->ResultsetSize;	
		}
		public function getSearchResultTitle() {
			return $this->SearchResultTitle;	
		}
		
		public function setAdvanceSearchTextStr($str){
			$this->criteria_advance_search_title = $str;
		}

		public function getSuggestResult(){
			global $oea_cfg;
			
			$data = $this->objOEA_Setting->get_OEA_Item();
			
			if($this->criteria_title!="") {	// search by text
				$default_oea_item = $this->objOEA_Setting->default_oeaItemOnlyArr;
				
				$rankResultAry = array();
				if(sizeof($default_oea_item)>0) {
					$i = 0;
					
					foreach($default_oea_item as $_itemCode=>$_itemName) {
						if ($_itemCode == $oea_cfg["OEA_Default_Category_Others"]) {
							// do not suggest "Others" to user
							continue;
						}
						
						similar_text($this->criteria_title, $_itemName, $p);
						
						if($p>=$this->suggestRankLowerLimit) {
							$rankResultAry[$i] = array($p,$_itemCode, $_itemName);
							$i++;
						} 
					}
					
					rsort($rankResultAry);	
					$result = array();
					for($j=0; $j<sizeof($rankResultAry) && $j<$this->suggestLimit; $j++) {
						$result[$j][] = $rankResultAry[$j][1];
						$result[$j][] = $rankResultAry[$j][2];
	
					}
					$this->SearchResultTitle = $result;
				}
			}
			
		}

		public function loadSuggestResultDisplay()
		{
			global $Lang, $button_hide, $i_general_show;
			
			$this->setResultsetSize(sizeof($this->SearchResultTitle));
			
			if($this->criteria_title!="") {	# display result of "Title" search
				if($this->SearchResultTitle=="" || $this->ResultsetSize==0) {
					
					$table .= "<b>- ".$Lang['iPortfolio']['OEA']['NoSuggestedItemReturn']." -</b><br style='clear:both'><br style='clear:both'>";
				} else {
					$result = $this->SearchResultTitle;
					
					if($_SESSION['UserType']==USERTYPE_STAFF) {
						$option = "copyback(this.value);";
					}
					
					$table .= $Lang['iPortfolio']['OEA']['NoOfItemsSuggested']." : ".sizeof($result)."<br style='clear:both'><br style='clear:both'>";
					$table .= "<table class='common_table_list_v30'>";
					
					$table .= "<tr><td><table class='common_table_list_v30' id='suggestionTable'>";
					for($i=0, $i_max=sizeof($result); $i<$i_max; $i++) {
						$table .= "<tr><td width='1' valign='top'><input type='radio' name='ItemCode' id='ItemCode_".$result[$i][0]."' value='".$result[$i][0]."' onClick='assignItemToTemp(this.value,\"".intranet_htmlspecialchars(addslashes($result[$i][1]))."\");copyback(document.form2.hiddenItemCode.value);'></td><td width='100%'><label for='ItemCode_".$result[$i][0]."'>".$result[$i][1]."</label></td></tr>";
					}	
					
					$table .= "<tr><td colspan='2' align='right'></td></tr></table>";
					
					$table .= "</td></tr>";
					$table .= "</table>";
					$table .= "<input type='hidden' name='hiddenItemCode' id='hiddenItemCode' value=''>";
					$table .= "<input type='hidden' name='hiddenItemName' id='hiddenItemName' value=''>";
					
				}
				
				return $table;
			}
		}
			
		public function setAdvanceAvaliableSearchAry($advanceSearchArray=array()) 
		{
			$this->advance_search_available_array = $advanceSearchArray;
		}
		
		public function getAdvanceSearchTextResult() 
		{
			$availableAry = $this->advance_search_available_array;
			$searchText = $this->criteria_advance_search_title;
			
			if($searchText=="") {
				$resultAry = $availableAry;
			} else {
				$max_ary_size = sizeof($availableAry);
				if($max_ary_size>0) {
					$c = 0;
					foreach($availableAry as $_key=>$_ary) {
						$posItemName = stripos($_ary['ItemName'],$searchText);
						$posItemCode = stripos($_ary['ItemCode'],$searchText);
						
						if(is_numeric($posItemName) || is_numeric($posItemCode)) {
							$resultAry[$_key] = $_ary;
						}	
					}	
				}
			}
			
			$this->advanceSearchResult = $resultAry;
		}  
		
		public function displayAdvanceSearchResult($PageNo = NULL, $NumPerPage = NULL)
		{
			global $Lang, $i_no_record_exists_msg, $PATH_WRT_ROOT, $button_submit, $button_cancel, $_SESSION;
			include_once($PATH_WRT_ROOT."includes/libpagination.php");			
			include_once($PATH_WRT_ROOT."includes/libinterface.php");
			$linterface = new interface_html();
			
			$resultAry = $this->advanceSearchResult;

			$divHeight = 285;	
			if($_SESSION['UserType']==USERTYPE_STAFF) {
				$option = "copyback(this.value);";
				$divHeight = 230;
			}

			

			$table .= "<div style='width:100%;height:".$divHeight."px;overflow:auto'>";
			$table .= "<div style='width:95%'>";
			$table .= $Lang['iPortfolio']['OEA']['NoOfResults']." : ".sizeof($resultAry);
			$table .= "<table class='common_table_list_v30' id='ResultTable' >";
			$table .= "<tr>";
			$table .= "<th width='1'>&nbsp;</th>";
			$table .= "<th width='1'>&nbsp;</th>";
			$table .= "<th width='15%'>".$Lang['iPortfolio']['OEA']['ProgramCode']."</th>";
			$table .= "<th width='60%'>".$Lang['iPortfolio']['OEA']['Title']."</th>";
			$table .= "<th width='25%'>".$Lang['iPortfolio']['OEA']['Category']."</th>";
			$table .= "</tr>";
			if(sizeof($resultAry)==0) {
				$table .= "<tr><td colspan='100%' height='40' align='center'>$i_no_record_exists_msg</td></tr>";	
			} else {
				
				
				$startRecord = intval((($PageNo -1) * $NumPerPage  ) +1);
				
				$endRecord = intval($PageNo * $NumPerPage);
//error_log("ss --> ".$startRecord." ee -->".$endRecord."\n", 3, "/tmp/aaa.txt");
				$_currentRecord = 0;
				$totalOfRecord =count($resultAry);
				foreach($resultAry as $_key=>$_ary) {
					$_currentRecord = $_currentRecord + 1;
					if($_currentRecord < $startRecord){
//error_log(" ".$_currentRecord." < ".$startRecord."  -- skip \n", 3, "/tmp/aaa.txt");
						continue; 
					}

					if($_currentRecord > $endRecord){

						break;
					}
//error_log($_ary['ItemCode']."\n", 3, "/tmp/aaa.txt");
					$table .= "<tr>";
					$table .= "<td><label for='ItemCode2_".$_ary['ItemCode']."'>".$_currentRecord."</label></td>";
					$table .= "<td><input type='radio' name='ItemCode2' id='ItemCode2_".$_ary['ItemCode']."' value='".$_ary['ItemCode']."' onClick='assignItemToTemp(\"".$_ary['ItemCode']."\",\"".intranet_htmlspecialchars(addslashes($_ary['ItemName']))."\");copyback(document.getElementById(\"hiddenItemCode\").value);'><input type='hidden' name='ItemName2' id='ItemName2_".$_ary['ItemCode']."' value='".addslashes($_ary['ItemName'])."'></td>";
					$table .= "<td><label for='ItemCode2_".$_ary['ItemCode']."'>".$_ary['ItemCode']."</label></td>";
					$table .= "<td><label for='ItemCode2_".$_ary['ItemCode']."'>".$_ary['ItemName']."</label></td>";
					$table .= "<td><label for='ItemCode2_".$_ary['ItemCode']."'>".$_ary['CatName']."</label></td>";	
					$table .= "</tr>";
				
				}

				//$button = $linterface->GET_ACTION_BTN($button_submit, "button", "copyback(document.getElementById('hiddenItemCode').value)")."&nbsp;";

				$hidden_field .= "<input type='hidden' name='hiddenItemCode' id='hiddenItemCode' value=''>";
				$hidden_field .= "<input type='hidden' name='hiddenItemName' id='hiddenItemName' value=''>";
			}

			if($totalOfRecord > 0){
				$objPagination = new libpagination();
				$objPagination->setTotalNumOfResult($totalOfRecord);
				$objPagination->setNumPerPage($NumPerPage);
				$objPagination->setCurPage($PageNo);
				$objPagination->setJsFunctionName('goGenerateResult');
				$pageination = $objPagination->getPaginationHtml();
				
				$table .= '<tr><td colspan="5">'.$pageination.'</td></tr>';
			}
			$table .= "</table>";

			$table .= "</div>";
			$table .= "</div>";
			$table .= $hidden_field;			

			


			$table .= "<div class='edit_bottom_v30'>";

			$button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.top.tb_remove();");
			$table .= $button;
			$table .= "</div>";

			return $table;
		}

		
	}

}
?>