<?php
/*
 * Modification Log:
 *  2018-09-27 Bill     [2018-0914-1458-46073]
 *  - modified Get_Student_Academic_Result(), support ignore term assessment result
 * 
 *  2017-10-03 Simon 
 *  - Update_Subject_Full_Mark / add wording space inside the set fields and where condition
 *
 *  2015-03-06 Pun [2014-0808-1405-07066]
 *  - Added Pass Mark Int
 * 
 * 	2012-02-27 Ivan [2012-0224-1648-56073]
 * 	- modified Get_Student_Academic_Result(), remove check assr.Score != '' since this will filter out some records which score is zero
 */
class libpf_academic extends libdb {
	
	public function libpf_academic() {
		$this->libdb();
	}
	
	private static function Get_Subject_Full_Mark_Table() 
	{
		global $eclass_db;
		return $eclass_db.'.ASSESSMENT_SUBJECT_FULLMARK';
	}
	
	/*
	 * return: $Array[$AcademicYearID][$YearID][$SubjectID]['FullMarkInt', 'FullMarkGrade'] = Value
	 */
	public function Get_Subject_Full_Mark($AcademicYearIDArr='')
	{
		if ($AcademicYearIDArr != '' && count((array)$AcademicYearIDArr) > 0) {
			$conds_AcademicYearID = " And AcademicYearID In (".implode(',', (array)$AcademicYearIDArr).") ";
		}
		
		$ASSESSMENT_SUBJECT_FULLMARK = $this->Get_Subject_Full_Mark_Table();
		$sql = "Select
						FullMarkID,
						SubjectID,
						YearID,
						AcademicYearID,
						FullMarkInt,
						PassMarkInt,
						FullMarkGrade
				From
						$ASSESSMENT_SUBJECT_FULLMARK
				Where
						1
						$conds_AcademicYearID
				";
// 		debug_pr($sql);
// 		die();
		$InfoArr = $this->returnArray($sql);
		
		return BuildMultiKeyAssoc($InfoArr, array('AcademicYearID', 'YearID', 'SubjectID'), $IncludedDBField=array('FullMarkID', 'FullMarkInt', 'PassMarkInt', 'FullMarkGrade'), $SingleValue=0, $BuildNumericArray=0);
	}
	
	/*
	 * $DataArr[0]['AcademicYearID', 'YearID', 'SubjectID', 'FullMarkInt', 'PassMarkInt', 'FullMarkGrade']
	 */
	public function Insert_Subject_Full_Mark($DataArr)
	{
		$numOfData = count((array)$DataArr);
		if ($numOfData == 0)
			return false;
			
		
		### Get DB Field Array
		$FieldArr = array();
		foreach ((array)$DataArr[0] as $thisField => $thisValue) {
			$FieldArr[] = $thisField;
		}
		$FieldArr[] = 'DateInput';
		$FieldArr[] = 'InputBy';
		$FieldArr[] = 'DateModified';
		$FieldArr[] = 'LastModifiedBy';
		
		
		### Get DB Value Array
		$ValueArr = array();
		for ($i=0; $i<$numOfData; $i++)
		{
			$thisDataArr = $DataArr[$i];
			
			$thisValueArr = array();
			foreach ((array)$thisDataArr as $thisField => $thisValue) {
				$thisValue = trim($thisValue);
				$thisValueArr[] = ($thisValue=='')? 'null' : "'".$this->Get_Safe_Sql_Query($thisValue)."'";
			}
			$thisValueArr[] = 'now()';
			$thisValueArr[] = ($_SESSION['UserID'])? "'".$_SESSION['UserID']."'" : 'NULL';
			$thisValueArr[] = 'now()';
			$thisValueArr[] = ($_SESSION['UserID'])? "'".$_SESSION['UserID']."'" : 'NULL';
			
			$ValueArr[] = '('.implode(',', (array)$thisValueArr).')';
		}
		
		$FieldList = implode(", ", $FieldArr);
		$ValueList = implode(", ", $ValueArr);
		
		$ASSESSMENT_SUBJECT_FULLMARK = $this->Get_Subject_Full_Mark_Table();
		$sql = "Insert Into $ASSESSMENT_SUBJECT_FULLMARK
					($FieldList)
				Values
					$ValueList
				";
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	public function Update_Subject_Full_Mark($FullMarkID, $DataArr)
	{
		if (count((array)$DataArr) == 0)
			return false;
			
		
		# Build field update values string
		$ValueArr = array();
		foreach ($DataArr as $thisField => $thisValue) {
			$thisValue = trim($thisValue);
			$thisValue = ($thisValue=='')? 'null' : "'".$this->Get_Safe_Sql_Query($thisValue)."'";
			$ValueArr[] = $thisField." = ".$thisValue;
		}
		$ValueArr[] = 'DateModified = now()';
		
		$LastModifiedBy = ($_SESSION['UserID'])? "'".$_SESSION['UserID']."'" : 'NULL';
		$ValueArr[] = 'LastModifiedBy = '.$LastModifiedBy;
		
		$ValueFieldText = implode(', ', (array)$ValueArr);
		
		$ASSESSMENT_SUBJECT_FULLMARK = $this->Get_Subject_Full_Mark_Table();
		$sql = "Update $ASSESSMENT_SUBJECT_FULLMARK
					 Set $ValueFieldText
				 Where 
						FullMarkID = '".$FullMarkID."'
				";
// 		debug_pr($sql);
// 		die();
		$success = $this->db_db_query($sql);		
		return $success;
	}
	
	function Get_Student_Academic_Result($StudentIDArr='', $SubjectIDArr='', $AcademicYearIDArr='', $YearTermIDArr='', $ParentSubjectOnly=1, $IgnoreEmptyScore=0, $IgnoreNoRanking=0, $IgnoreTermAssessmentResult=0) {
		global $eclass_db, $intranet_db;
		
		$conds_StudentID = '';
		if ($StudentIDArr != '') {
			$conds_StudentID = " And assr.UserID In ('".implode("','", (array)$StudentIDArr)."') ";
		}
		
		$conds_SubjectID = '';
		if ($SubjectIDArr != '') {
			$conds_SubjectID = " And assr.SubjectID In ('".implode("','", (array)$SubjectIDArr)."') ";
		}
		
		$conds_AcademicYearID = '';
		if ($AcademicYearIDArr != '') {
			$conds_AcademicYearID = " And assr.AcademicYearID In ('".implode("','", (array)$AcademicYearIDArr)."') ";
		}
		
		$conds_YearTermID = '';
		if ($YearTermIDArr != '') {
			if (in_array(0, (array)$YearTermIDArr)) {
				$conds_Overall_Term = " Or assr.YearTermID Is Null Or assr.YearTermID = '' ";
			}
			$conds_YearTermID = " And (assr.YearTermID In ('".implode("','", (array)$YearTermIDArr)."') $conds_Overall_Term)";
		}
		
		$conds_SubjectComponentID = '';
		if ($ParentSubjectOnly) {
			$conds_SubjectComponentID = " And (assr.SubjectComponentID Is Null Or assr.SubjectComponentID = '' Or assr.SubjectComponentID = 0) ";
		}
		
		$conds_IgnoreEmptyScore = '';
		if ($IgnoreEmptyScore == 1) {
			//$conds_IgnoreEmptyScore = " And ((assr.Score Is Not Null And assr.Score != '' And assr.Score != -1) Or (assr.Grade Is Not Null And assr.Grade != '')) ";
			$conds_IgnoreEmptyScore = " And ((assr.Score Is Not Null And assr.Score != -1) Or (assr.Grade Is Not Null And assr.Grade != '')) ";
		}
		
		$conds_IgnoreNoRanking = '';
		if ($IgnoreNoRanking == 1) {
			$conds_IgnoreNoRanking = " And (assr.OrderMeritForm > 0 || assr.Grade = 'ABS') ";
		}
		
		$cond_IgnoreTermAssessmentResult = '';
		if ($IgnoreTermAssessmentResult == 1) {
		    $cond_IgnoreTermAssessmentResult = ' And assr.TermAssessment IS NULL ';
		}
		
		$ASSESSMENT_STUDENT_SUBJECT_RECORD = $eclass_db.'.ASSESSMENT_STUDENT_SUBJECT_RECORD';
		$ASSESSMENT_SUBJECT = $intranet_db.'.ASSESSMENT_SUBJECT';
		$LEARNING_CATEGORY = $intranet_db.'.LEARNING_CATEGORY';
		$sql = "Select
						assr.UserID as StudentID,
						assr.AcademicYearID,
						assr.YearTermID,
						assr.SubjectID,
						assr.SubjectComponentID,
						assr.Score,
						assr.Grade,
						assr.OrderMeritClass,
						assr.OrderMeritClassTotal,
						assr.OrderMeritForm,
						assr.OrderMeritFormTotal,
						assr.OrderMeritStream,
						assr.OrderMeritStreamTotal,
						assr.OrderMeritSubjGroup,
						assr.OrderMeritSubjGroupTotal
				From
						$ASSESSMENT_STUDENT_SUBJECT_RECORD as assr
						Left Join $ASSESSMENT_SUBJECT as s On (assr.SubjectID = s.RecordID)
						Left Join $LEARNING_CATEGORY as lc On (s.LearningCategoryID = lc.LearningCategoryID)
				Where
						1
						$conds_StudentID
						$conds_SubjectID
						$conds_AcademicYearID
						$conds_YearTermID
						$conds_SubjectComponentID
						$conds_IgnoreEmptyScore
						$conds_IgnoreNoRanking
						$cond_IgnoreTermAssessmentResult
				Order By
						lc.DisplayOrder, s.DisplayOrder, s.EN_DES
				";
		return $this->returnArray($sql);
	}
}
?>