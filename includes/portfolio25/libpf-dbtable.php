<?php
/**
 * Modifying by: Bill
 */

##############################################################
#
#   Date:   2019-03-19  Bill    [2017-1207-0959-51277]
#           modified displayAssessmentReport(), displayAssessmentReportEditTable(), hide stat button for non-activated student accounts
#
#   Date:   2018-11-30  Bill    [2017-1207-0959-51277]
#           modified displayAssessmentReportEditTable(), to add edit mode for assessment report
#
#   Date:   2018-08-06  Bill    [2017-0901-1527-45265]
#           modified displayAssessmentReport(), to add <thead> for float header
#
#	Date:	2013-05-29	Yuen
#			added exportOLE_StudentRecordList() to export member list to CSV [Case#2013-0110-1235-02054]
#
#	Date:	2013-05-27	YatWoon
#			update displayOLE_StudentRecordList(), add nl2br to display the line break [Case#2013-0527-1021-49054]
#
##############################################################

include_once($intranet_root."/includes/libdbtable.php");
include_once($intranet_root."/includes/libdbtable2007a.php");
include_once($intranet_root."/includes/portfolio25/libpf-account-student.php");

class libpf_dbtable extends libdbtable2007
{
  function built_sql($isLimit="Limit"){
  
    $x = $this->sql;
    
    // Eric Yip (20100727): replace 3 char with single function
    //$x = str_replace("\n"," ",$x);
    //$x = str_replace("\r"," ",$x);
    //$x = str_replace("\t"," ",$x);
    $x = preg_replace("/[\n\r\t]/", " ", $x);
    
    // Eric Yip (20100727): search for the last "FROM"
    // (to gurantee it is not a part of function syntax)
    //$y = "SELECT COUNT(*) " . stristr($x, " FROM ");
    $lastFrom = strripos($x, " FROM ");
    $y = "SELECT COUNT(*) " . substr($x, $lastFrom);
    
    if($this->IsColOff =='eDisciplineConductMarkView')
    {
      $tmp = $this->returnArray($x);
      $size = sizeof($tmp);
      $y = "SELECT $size " . stristr($x, " FROM ");
    }
     if($this->count_mode != "")
	  {
	  	$tmp = $this->returnArray($x);
	  	$size = sizeof($tmp);
	  	$y = "SELECT $size";
	  }
    $this->rs=$this->db_db_query($y);
    
    if ($this->db_num_rows()!=0)
    {
      $row = $this->db_fetch_array();
      
      # Modified by key (2008-07-22) for iportfolio 2.5 shcool based scheme
      if($this->record_num_mode == "iportfolio")
        $this->total_row = ($this->db_num_rows() >= 1) ? $this->db_num_rows() : 0;
      else
        $this->total_row = ($this->db_num_rows()!=1) ? $this->db_num_rows() : $row[0];
    }
    
    $max_page = ceil($this->total_row/$this->page_size);
    if ($this->pageNo > $max_page && $max_page != 0)
    {
      $this->pageNo = $max_page;
    }
    $this->n_start=($this->pageNo-1)*$this->page_size;
    
    # added by Kelvin on 11 Feb 09 for admin console->Admin Mgmt->School Calendar->School Events CRM Ref No.: 2009-0210-1044
    if($this->IsColOff=='adminMgtSchoolCalendarEvents')
    {
      $x .= " ORDER BY ";
      if($this->field==0)
      {
        $x .= $this->field_array[$this->field];
        $x .= ($this->order==0) ? " DESC," : " ASC,";
        $x .= $this->field_array[($this->field+1)];
        $x .=  " ASC";
        $x .= " ".$this->fieldorder2;
      }
      else
      {
        $x .= (count($this->field_array)<=$this->field) ? $this->field_array[0] : $this->field_array[$this->field];
        $x .= ($this->order==0) ? " DESC" : " ASC";
        $x .= " ".$this->fieldorder2;
      }
    }
    else
    {
      $x .= " ORDER BY ";
      $x .= (count($this->field_array)<=$this->field) ? $this->field_array[0] : $this->field_array[$this->field];
      $x .= ($this->order==0) ? " DESC" : " ASC";
      $x .= " ".$this->fieldorder2;
    }
    
    if($isLimit == "Limit")
      $x .= " LIMIT ".$this->n_start.", ".$this->page_size;
    return $x;
  }

  # override function
  function displayPlain(){
    $i=0;
    $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
    $this->n_start=($this->pageNo-1)*$this->page_size;
    $this->rs=$this->db_db_query($this->built_sql());
    
    $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
//    $bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
//    $bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
    $tdClass = "class=\"tabletext tablerow\"";

    $x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
    $x .= $this->displayPlainColumn(1);

    if ($this->db_num_rows()==0)
    {
        $x .= "<tr class=\"tablerow1\"><td align=center class=\"td_no_record\" colspan=".$this->no_col." height='80'>".$this->no_msg."</td></tr>\n";
    } else
    {
      while($row = $this->db_fetch_array())
      {
        $i++;
        if($i>$this->page_size) break;
        $rowClass = ($i%2) ? "tablerow1" : "tablerow2";
        $x .= "<tr class=\"$rowClass\">\n";
        $x.= ($this->noNumber) ? "" : "<td class='tabletext' $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
        for($j=0; $j<$total_col; $j++){
          $t = $row[$j];                  
          $x .= $this->displayPlainCell($j, $t, $tdClass, 1);
        }
        $x .= "</tr>\n";
      }
    }
    $x .= "</table>\n";
 
    if ($this->db_num_rows()>0)
    {
      $this->navigationHTML = $this->navigation_IP25();
    }
  
    $this->db_free_result();
    return $x;
  }

  function displayStudentList_ListView(){
    global $image_path, $LAYOUT_SKIN;
  
    $i=0;
    $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
    $this->n_start=($this->pageNo-1)*$this->page_size;
    $this->rs=$this->db_db_query($this->built_sql());
    
    $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
    $bg_style1 = "class='row_on tabletext'";
    $bg_style2 = "class='row_off tabletext'";
    
    $x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
    $x .= $this->displayPlainColumn(1);
    
    if ($this->db_num_rows()==0)
    {
      $x.="<tr><td $bg_style1 align=center colspan=".$this->no_col." height='80'>".$this->no_msg."</td></tr>\n";
    } else
    {
      while($row = $this->db_fetch_array())
      {
        $i++;
        if($i>$this->page_size) break;
        $x.="<tr class=\"tablerow1\">\n";
        $bg_style_row = ($row['Activated'] == "1" && $row['Suspended'] == "0") ? $bg_style1 : $bg_style2;
        $x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;

        for($j=0; $j<$total_col; $j++){
          $t = $row[$j];
          
          if($t == '<!--StudentPhoto-->')
          {
            $lpf_acc = new libpf_account_student();
            $lpf_acc->SET_CLASS_VARIABLE("user_id", $row['UserID']);
            $student_photo = $lpf_acc->GET_IPORTFOLIO_PHOTO(false);
            
            $t = ($student_photo != "") ? "<a href=\"javascript:;\" class=\"contenttool\" onMouseOver=\"jSHOW_PHOTO(event, '".$student_photo."')\" onMouseOut=\"jHIDE_PHOTO()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/smartcard/icon_student_photo.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>" : "&nbsp;";
          }
          $x .= $this->displayPlainCell($j, $t, $bg_style_row, 1);
        }
        $x .= "</tr>\n";
      }
    }
    $x .= "</table>\n";
    if ($this->db_num_rows()>0)
    {
      $this->navigationHTML = $this->navigation(1);
    }
    
    $this->db_free_result();
    return $x;
  }
  
  function displayStudentList_ThumbnailView($peer_view = ""){
    global $image_path, $LAYOUT_SKIN, $intranet_session_language, $ec_iPortfolio;
  
    $i=0;
    $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
    $this->n_start=($this->pageNo-1)*$this->page_size;
    $this->rs=$this->db_db_query($this->built_sql());
    
    $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
    $bg_style1 = "class='row_on tabletext'";
    $bg_style2 = "class='row_off tabletext'";
    
    $x = "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
    
    if ($this->db_num_rows()==0)
    {
      $x.="<tr><td $bg_style1 align=center colspan=".$this->no_col." height='80'>".$this->no_msg."</td></tr>\n";
    } else
    {
      while($row = $this->db_fetch_array())
      {
        $i++;
        if($i>$this->page_size) break;
        
        if(($i-1) % 5 == 0)
        {
          $x.="<tr class=\"tablerow1\">\n";
        }
        //$bg_style_row = ($row['Activated'] == "1" && $row['Suspended'] == "0") ? $bg_style1 : $bg_style2;
        //$x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;

        $lpf_acc = new libpf_account_student();
        $lpf_acc->SET_CLASS_VARIABLE("user_id", $row['UserID']);
        
        $PhotoStyle = ($row['Activated'] && !$row['Suspended']) ? "on" : "off"; 
        $student_photo = $lpf_acc->GET_IPORTFOLIO_PHOTO();
		if(!$peer_view){
        $t =  "
                <table border='0' cellspacing='0' cellpadding='0' align='center'>
    							<tr>
    								<td background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_01.gif' style='padding-left:10px; padding-top:10px;'><img src=\"".$student_photo."\" width=\"100\" height=\"130\" border=\"0\" align=\"absmiddle\"></td>
    								<td height='10' background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_02.gif'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' title='' width='10' height='10'></td>
    							</tr>
    							<tr>
    								<td height='10' background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_03.gif'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='10' height='10'></td>
    								<td height='10'><img src='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_04.gif' width='10' height='10'></td>
    							</tr>
    							<tr>
                    <td colspan='2' width='100'>".$row['UserName']."</td>
                  </tr>
                  <tr>
                    <td colspan='2' width='100'>".$row['WebSAMSRegNo']."&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan='2' width='100'>".$row['PILink']."&nbsp;".$row['SRLink']."&nbsp;".$row['LPLink']."&nbsp;".$row['Report']."</td>
                  </tr>
    						</table>
    					";
		}
		else{
			$t =  "
                <table border='0' cellspacing='0' cellpadding='0' align='center'>
    							<tr>
    								<td background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_01.gif' style='padding-left:10px; padding-top:10px;'><a href=\"javascript:jTO_LP('".$row['UserID']."')\" class=\"new_alert\" title=\"".$ec_iPortfolio['View'].$row['UserName'].$ec_iPortfolio['learning_portfolio']."\"><img src=\"".$student_photo."\" width=\"100\" height=\"130\" border=\"0\" align=\"absmiddle\"></a></td>
    								<td height='10' background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_02.gif'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' title='' width='10' height='10'></td>
    							</tr>
    							<tr>
    								<td height='10' background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_03.gif'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='10' height='10'></td>
    								<td height='10'><img src='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_04.gif' width='10' height='10'></td>
    							</tr>
    							<tr>
                    <td colspan='2' width='100'>".$row['UserName']."</td>
                  </tr>
                  <tr>
                    <td colspan='2' width='100'>".$row['WebSAMSRegNo']."&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan='2' width='100'>".$row['PILink']."&nbsp;".$row['SRLink']."&nbsp;".$row['LPLink']."&nbsp;".$row['Report']."</td>
                  </tr>
    						</table>
    					";
		}
        $x .= $this->displayPlainCell($i, $t, $bg_style_row, 1);

        if(($i-1) % 5 == 4)
        {
          $x .= "</tr>\n";
        }
      }
    }
    $x .= "</table>\n";
    if ($this->db_num_rows()>0)
    {
      $this->navigationHTML = $this->navigation(1);
    }
    
    $this->db_free_result();
    return $x;
  }
  
  function displayOLE_StudentRecordList()
  {
    global $PATH_WRT_ROOT, $LAYOUT_SKIN;
    
    $i=0;
    $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
    $this->n_start=($this->pageNo-1)*$this->page_size;
    $this->rs=$this->db_db_query($this->built_sql());
    
    $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
    $bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
    $bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
    
    $x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
    $x .= $this->displayColumnCustom();

    if ($this->db_num_rows()==0)
    {
      $x.="<tr class=\"tablerow1\"><td $bg_style1 align=center colspan=".($this->no_col+$this->additionalCols)." height='80'>".$this->no_msg."</td></tr>\n";
    }
    else
    {
      while($row = $this->db_fetch_array())
      {
        $i++;
        if($i>$this->page_size) break;

        $rowClass = ($i%2) ? "tablerow1" : "tablerow2";

          $TmpStatus = $row[0];
        switch($TmpStatus)
        {
          case 1:
            $x.="<tr class=\"{$rowClass} status_waiting\" >\n";
            break;
          case 2:
            $x.="<tr class=\"{$rowClass} status_approved\" >\n";
            break;
          case 3:
            $x.="<tr class=\"{$rowClass} status_reject\" >\n";
            break;
          case 4:
            $x.="<tr class=\"{$rowClass} status_teacher\" >\n";
            break;
          default:
            $x.="<tr class=\"{$rowClass}\" >\n";
            break;
        }

        $bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
        //$x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
        $x.= ($this->noNumber) ? "" : "<td  $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;

        $TmpCnt = 3;
        for($j=1; $j<$total_col; $j++)
        {
          $t = nl2br($row[$j]);

          $x .= $this->displayCell($TmpCnt, $t, $bg_style_row, "align='center' valign='middle' ");
          $TmpCnt++;
        }
        $x .= "</tr>\n";
      }
    }
    $x .= "</table>\n";
    if ($this->db_num_rows()>0)
    {
      $this->navigationHTML = $this->navigation(1);
    }
    
    $this->db_free_result();
    return $x;
  }
  
  function exportOLE_StudentRecordList($column_start=1, $ELEArray=null, $IntExt=0)
  {
    global $PATH_WRT_ROOT, $LAYOUT_SKIN, $ec_iPortfolio;
    
    
    $i=0;
    $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
    $this->n_start=($this->pageNo-1)*$this->page_size;
    $this->rs=$this->db_db_query($this->built_sql());
    
    $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;    
    
    if ($this->db_num_rows()==0)
    {
      
    }
    else
    {
      while($row = $this->db_fetch_array())
      {
        $i++;
        if($i>$this->page_size) break;
      
        $TmpStatus = $row[0];

        $columns = array(); 
        unset($columns);

        for($j=$column_start; $j<$total_col-1; $j++)
        {
        	
        	if (is_array($ELEArray) && sizeof($ELEArray)>0 && $j==5)
        	{
        		if ($IntExt!=1)
        		{
					$oletitle_count = 0;
					foreach($ELEArray as $ELECode => $ELETitle)
					{
						//$ELEDisplay = str_replace(array("[", "]"), "", $ELECode);
						//$ELEDisplay = is_numeric($ELEDisplay) ? $ELETitle : $ELEDisplay;		
						$export_header[] = $ELETitle;
						$columns[] = (strstr($row[$j], $ELECode)) ? "Y" : "";
					}
        		}
        	} else
        	{
	        	if (strstr($row[$j], "attach.php"))
	        	{
	        		$row[$j] = "Y";
	        	} elseif (strstr($row[$j], "icon_waiting.gif"))
	        	{
	        		$row[$j] = $ec_iPortfolio['pending'];
	        	} elseif (strstr($row[$j], "icon_approve_b.gif"))
	        	{
	        		$row[$j] = $ec_iPortfolio['approved'];
	        	} elseif (strstr($row[$j], "icon_reject_l.gif"))
	        	{
	        		$row[$j] = $ec_iPortfolio['rejected'];
	        	} elseif (strstr($row[$j], "icon_teacher_input.gif"))
	        	{
	        		$row[$j] = $ec_iPortfolio['teacher_submit_record'];
	        	}
	          $columns[] = str_replace("&nbsp;", "",strip_tags($row[$j]));
        	}
        }
        $rows_return[] = $columns;
      }
    }
    
    $this->db_free_result();

    return $rows_return;
  }
  
/*
  function displaySuspendResult($fail_user_arr){
    global $ec_iPortfolio, $ec_guide;
    
    $i=0;
    $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";

    $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
    $bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
		$bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
    
    $x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
    $x .= $this->displayPlainColumn(1);
    
    for($i=0; $i<count($fail_user_arr); $i++)
    {
      $x.="<tr>\n";
      $bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
      $x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i+1)."&nbsp;</td>\n" ;

      $row = $fail_user_arr[$i];
      for($j=0; $j<$total_col; $j++){
        if($j==4)
        {
          switch($row[$j])
          {
            case "SUSPENDED":
              $t = $ec_iPortfolio['suspend_result_suspended'];
              break;
            case "INACTIVE":
              $t = $ec_iPortfolio['suspend_result_inactive'];
              break;
            case "UNKNOWN":
            default:
              $t = $ec_guide['import_error_unknown'];
              break;
          }        
        }
        else
        {
          $t = $row[$j];
        }
        
        $x .= $this->displayPlainCell($j, $t, $bg_style_row, 1);
      }
      $x .= "</tr>\n";
    }
    $x .= "</table>\n";
    
    echo $x;
  }
  
  function displayOLEStatTable($ParOLESummaryColArr)
  {
    $i=0;
    $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
    $this->n_start=($this->pageNo-1)*$this->page_size;
    $this->rs=$this->db_db_query($this->built_sql());

    $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
    $bg_row_style1 = 'class="tablerow1"';
    $bg_row_style2 = 'class="tablerow2"';
    $bg_cell_style1 = 'class="tablerow_total"';
    $bg_cell_style2 = 'class="tablerow_total2"';
    
    $x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
    $x .= $this->displayPlainColumn(1);
    
    if ($this->db_num_rows()==0)
    {
      $x.="<tr><td $bg_style1 align=center colspan=".$this->no_col." height='80'>".$this->no_msg."</td></tr>\n";
    } else
    {
    //			$this->total_row = $this->db_num_rows();
      while($row = $this->db_fetch_array())
      {
        $i++;
        if($i>$this->page_size) break;

        $bg_style_row = ($i%2) ? $bg_row_style1 : $bg_row_style2;
        $x.="<tr $bg_style_row>\n";
        $x.= ($this->noNumber) ? "" : "<td $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
        for($j=0; $j<$total_col; $j++){
          $t = $row[$j];
          $bg_style_cell = (in_array($j, $ParOLESummaryColArr)) ? (($i%2) ? $bg_cell_style1 : $bg_cell_style2) : '';
          $x .= $this->displayPlainCell($j, $t, $bg_style_cell, 1);
        }
        $x .= "</tr>\n";
      }
    }
    $x .= "</table>\n";
    if ($this->db_num_rows()>0)
    {
      $this->navigationHTML = $this->navigation(1);
    }
    
    $this->db_free_result();
    echo $x;
  }
*/
  function displayOLEStudentSummaryTable(){
    global $list_total;
  
    $i=0;
    $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
    $this->n_start=($this->pageNo-1)*$this->page_size;
    $this->rs=$this->db_db_query($this->built_sql());
    
    $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
    $bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
    $bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
    
    $x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
    $x .= $this->displayPlainColumn(1);
    
    if ($this->db_num_rows()==0)
    {
      $x.="<tr><td $bg_style1 align=center colspan=".$this->no_col." height='80'>".$this->no_msg."</td></tr>\n";
    } else
    {
      //			$this->total_row = $this->db_num_rows();
      while($row = $this->db_fetch_array())
      {
        $i++;
        if($i>$this->page_size) break;
        $x.="<tr>\n";
        $bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
        $x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
        for($j=0; $j<$total_col; $j++){
          $t = $row[$j];
          
          # Eric Yip (20100201): a hyperlink implies hour / record count
          $a_count = preg_match_all("/(<a.*>)(\w.*)(<.*>)/ismU",$t,$patterns); 
          if($a_count && $a_count > 0)
          {
            $t_sum[$j] += (float) $patterns[2][0];

            $t_link = preg_replace("/(<a.*>)(\w.*)(<.*>)/ismU", '${1}'.$t_sum[$j].'${3}', $t);
            $t_link = preg_replace("/AcademicYearID=.*&/U", "", $t_link);
          
            $total_row[$j] = "<strong>$t_link</strong>";
          }
          else
          {
            $total_row[$j] = "<strong>$list_total</strong>";
          }
          
          $x .= $this->displayPlainCell($j, $t, $bg_style_row, 1);
        }
        $x .= "</tr>\n";
      }
      
      # Display Total row
      $i++;
      $x.="<tr>\n";
      $bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
      $x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
      for($j=0; $j<$total_col; $j++){
        $t = $total_row[$j];
        
        $x .= $this->displayPlainCell($j, $t, $bg_style_row, 1);
      }
      $x .= "</tr>\n";
    }
    
    $x .= "</table>\n";
    if ($this->db_num_rows()>0)
    {
      $this->navigationHTML = $this->navigation(1);
    }
    
    $this->db_free_result();
    return $x;
  }
  
  function displayOLEStudentRecordTable(){
    $i=0;
    $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
    $this->n_start=($this->pageNo-1)*$this->page_size;
    $this->rs=$this->db_db_query($this->built_sql());
    
    $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
    $bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
    $bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
    
    $x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
    $x .= $this->displayPlainColumn(1);
    
    if ($this->db_num_rows()==0)
    {
      $x.="<tr><td $bg_style1 align=center colspan=".$this->no_col." height='80'>".$this->no_msg."</td></tr>\n";
    } else
    {
    //			$this->total_row = $this->db_num_rows();
      while($row = $this->db_fetch_array())
      {
        $i++;
        $bg_style_row = "";

        if($i>$this->page_size) break;
        switch($row['RecordStatus'])
        {
          case 1:
            $bg_style_row = "class='status_waiting'";
            break;
          case 2:
            $bg_style_row = "class='status_approved'";
            break;
          case 3:
            $bg_style_row = "class='status_rejected'";
            break;
          case 4:
            $bg_style_row = "class='status_teacher'";
            break;
        }
        
        $x.="<tr>\n";
        $x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
        for($j=0; $j<$total_col; $j++){
          $t = $row[$j];
          $x .= $this->displayPlainCell($j, $t, $bg_style_row, 1);
        }
        $x .= "</tr>\n";
      }
    }
    $x .= "</table>\n";
    if ($this->db_num_rows()>0)
    {
      $this->navigationHTML = $this->navigation_IP25();
    }
    
    $this->db_free_result();
    return $x;
  }
  
  function displayAssessmentReport($displayBy)
  {
    global $ec_iPortfolio, $image_path, $is_skip_activated_checking, $LAYOUT_SKIN;
  
    $i=0;
    $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
    $this->n_start=($this->pageNo-1)*$this->page_size;
    $this->rs=$this->db_db_query($this->built_sql());
    
    $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
    $bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext' nowrap" : "";
    $bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext' nowrap" : "";

    $x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='common_table_list_v30'>\n";
    $x .= '<thead>';
    $x .= $this->displayPlainColumn(1);
    $x .= '</thead>';
  
    if ($this->db_num_rows()==0)
    {
        $x .= "<tr class=\"tablerow1\"><td $bg_style1 align=center colspan=".$this->no_col." height='80' class=\"td_no_record\">".$this->no_msg."</td></tr>\n";
    } else
    {
      while($row = $this->db_fetch_array())
      {
        $i++;
        if($i>$this->page_size) break;
          $x.="<tr>\n";
        $bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
        $x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
        
        $mainSubjCode = $row[0];
        $compSubjCode = $row[1];
        $subjName = $row[2];
        if($compSubjCode == "##Overall##")
        {
          $x .= "<td align='right' nowrap>{$subjName}</td>";
          for($j=3; $j<$total_col; $j++){
            $t = $row[$j];
            $x .= $this->displayPlainCell($j, $t, "class='tablerow_total' align='center' nowrap", 1);
          }
        }
        else
        {
          $subjStyle = ($compSubjCode == "") ? "class='tablelist_subject' nowrap" : "class='tablelist_sub_subject' nowrap";
          
          $x .= "<td {$subjStyle}>";
          $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
          $x .= "<td nowrap>{$subjName}</td>";
          $x .= ($displayBy=="Grade" || $displayBy=="ScoreGrade" || $is_skip_activated_checking) ? "" : "<td width=\"20\" align=\"right\"><a href='javascript:jVIEW_DETAIL(\"assessment_report\", \"".base64_encode("MainSubjectCode={$mainSubjCode}&CompSubjectCode={$compSubjCode}&displayBy={$displayBy}")."\")'><img src='{$image_path}/{$LAYOUT_SKIN}/iPortfolio/icon_resultview.gif' width='20' height='20' border='0'></a></td>";
          $x .= "</tr></table>";
          $x .= "</td>";
          
          for($j=3; $j<$total_col; $j++){
            $t = $row[$j];
            $x .= $this->displayPlainCell($j, $t, $bg_style_row, 1);
          }
        }
        $x .= "</tr>\n";
      }
    }
    $x .= "</table>\n";
    if ($this->db_num_rows()>0)
    {
      $this->navigationHTML = $this->navigation(1);
    }
    
    $this->db_free_result();
    return $x;
  }
  
  function displayAssessmentReportEditTable($displayBy)
  {
      global $ec_iPortfolio, $image_path, $is_skip_activated_checking, $LAYOUT_SKIN;
      
      $i = 0;
      $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
      $this->n_start = ($this->pageNo - 1) * $this->page_size;
      $this->rs = $this->db_db_query($this->built_sql());
      
      $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
      $bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext' nowrap" : "";
      $bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext' nowrap" : "";
      
      $x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
      $x .= '<thead>';
      $x .= $this->displayPlainColumn(1);
      $x .= '</thead>';
      
      if ($this->db_num_rows()==0)
      {
          $x .= "<tr><td $bg_style1 align=center colspan=".$this->no_col." height='80' class=\"td_no_record\">".$this->no_msg."</td></tr>\n";
      }
      else
      {
          while($row = $this->db_fetch_array())
          {
              $i++;
              if($i > $this->page_size) break;
              
              $x .= "<tr>\n";
              
              $bg_style_row = ($i % 2)? $bg_style1 : $bg_style2;
              $x .= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start + $i)."&nbsp;</td>\n" ;
              
              $mainSubjCode = $row[0];
              $compSubjCode = $row[1];
              $subjName = $row[2];
              $mainSubjID = $row['MainSubjCodeID'];
              $compSubjID = $row['CompSubjCodeID'];
              
              if($compSubjCode == "##Overall##")
              {
                  $x .= "<td align='right' nowrap>{$subjName}</td>";
                  //for($j=3; $j<$total_col; $j++) {
                  
                  $j = 0;
                  foreach($row as $index => $value)
                  {
                      if(is_integer($index)) {
                          continue;
                      }
                      if(in_array($index, array('MainSubjCode', 'CompSubjCode', 'SubjName', 'MainSubjCodeID', 'CompSubjCodeID'))) {
                          $j++;
                          continue;
                      }
                      $j++;
                      
                      $value_type = str_replace("IFNULL(t_am.Score_", "", $index);
                      $value_type = str_replace(", '')", "", $value_type);
                      $value_type = explode('_', $value_type);
                      
                      $yearID = $value_type[0]? $value_type[0] : 0;
                      $yearTermID = $value_type[1]? $value_type[1] : 0;
                      $subjectID = 0;
                      
                      //$input_field_type = $displayBy == "Grade"? 'text' : 'number';
                      $x .= $this->displayPlainCell($j, "<input type='text' id='score[$subjectID][$yearID][$yearTermID]' name='score[$subjectID][$yearID][$yearTermID]' value='$value'>", "class='tablerow_total' align='center' nowrap", 1);
                  }
              }
              else
              {
                  $subjStyle = ($compSubjCode == "") ? "class='tablelist_subject' nowrap" : "class='tablelist_sub_subject' nowrap";
                  
                  $x .= "<td {$subjStyle}>";
                  $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                  $x .= "<td nowrap>{$subjName}</td>";
                  $x .= ($displayBy=="Grade" || $displayBy=="ScoreGrade" || $is_skip_activated_checking)? "" : "<td width=\"20\" align=\"right\"><a href='javascript:jVIEW_DETAIL(\"assessment_report\", \"".base64_encode("MainSubjectCode={$mainSubjCode}&CompSubjectCode={$compSubjCode}&displayBy={$displayBy}")."\")'><img src='{$image_path}/{$LAYOUT_SKIN}/iPortfolio/icon_resultview.gif' width='20' height='20' border='0'></a></td>";
                  $x .= "</tr></table>";
                  $x .= "</td>";
                  
                  $j = 0;
                  foreach($row as $index => $value)
                  {
                      if(is_integer($index)) {
                          continue;
                      }
                      if(in_array($index, array('MainSubjCode', 'CompSubjCode', 'SubjName', 'MainSubjCodeID', 'CompSubjCodeID'))) {
                          $j++;
                          continue;
                      }
                      $j++;
                      
                      $value_type = str_replace("IFNULL(t_am.Score_", "", $index);
                      $value_type = str_replace(", '')", "", $value_type);
                      $value_type = explode('_', $value_type);
                      
                      $yearID = $value_type[0]? $value_type[0] : 0;
                      $yearTermID = $value_type[1]? $value_type[1] : 0;
                      $subjectID = $row['CompSubjCodeID']? $row['CompSubjCodeID'] : $row['MainSubjCodeID'];
                      
                      //$input_field_type = $displayBy == "Grade"? 'text' : 'number';
                      $x .= $this->displayPlainCell($j, "<input type='text' id='score[$subjectID][$yearID][$yearTermID]' name='score[$subjectID][$yearID][$yearTermID]' value='$value'>", $bg_style_row, 1);
                  }
              }
              $x .= "</tr>\n";
          }
      }
      $x .= "</table>\n";
      if ($this->db_num_rows()>0)
      {
          $this->navigationHTML = $this->navigation(1);
      }
      
      $this->db_free_result();
      return $x;
  }
  
  function displayAssessmentScoreList($resultType, $highlightCriteriaArr){
    $i=0;
    $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
    $this->n_start=($this->pageNo-1)*$this->page_size;
    $this->rs=$this->db_db_query($this->built_sql());
    
    $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
    $bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext' nowrap" : "";
    $bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext' nowrap" : "";
    
    $x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
    $x .= $this->displayPlainColumn(1);
  
    if ($this->db_num_rows()==0)
    {
      $x.="<tr><td $bg_style1 align=center colspan=".$this->no_col." height='80'>".$this->no_msg."</td></tr>\n";
    } else
    {
      while($row = $this->db_fetch_array())
      {
        $i++;
        if($i>$this->page_size) break;
        $x.="<tr>\n";
        $bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
        
        $x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
        for($j=0; $j<$total_col; $j++){
          $t = $row[$j];
          
          $bg_style_cell =  $bg_style_row;
          $bg_style_cell .= " style='";
          for($k=0, $k_max=count($highlightCriteriaArr); $k<$k_max; $k++)
          {
            $highlightCriteria = $highlightCriteriaArr[$k];
            
            $_bold = $highlightCriteria["bold"];
            $_italic = $highlightCriteria["italic"];
            $_underline = $highlightCriteria["underline"];
            $_color = $highlightCriteria["color"];

            switch($resultType)
            {
              case "SCORE":
                $_upper = $highlightCriteria["upper"];
                $_lower = $highlightCriteria["lower"];
              
                if($t >= $_lower && $t <= $_upper)
                {
                  $bg_style_cell .= ($_bold) ? "font-weight:bold;" : "";
                  $bg_style_cell .= ($_italic) ? "font-style:italic;" : "";
                  $bg_style_cell .= ($_underline) ? "text-decoration:underline;" : "";
                  $bg_style_cell .= isset($_color) ? "color:{$_color};" : "";
                }
                break;
              case "GRADE":
                $_grade = $highlightCriteria["grade"];

                if(in_array($t, $_grade))
                {
                  $bg_style_cell .= ($_bold) ? "font-weight:bold;" : "";
                  $bg_style_cell .= ($_italic) ? "font-style:italic;" : "";
                  $bg_style_cell .= ($_underline) ? "text-decoration:underline;" : "";
                  $bg_style_cell .= isset($_color) ? "color:{$_color};" : "";
                }
                break;
              case "RANK":
                $_upper = $highlightCriteria["upper"];
                $_lower = $highlightCriteria["lower"];
              
                if($t >= $_lower && $t <= $_upper)
                {
                  $bg_style_cell .= ($_bold) ? "font-weight:bold;" : "";
                  $bg_style_cell .= ($_italic) ? "font-style:italic;" : "";
                  $bg_style_cell .= ($_underline) ? "text-decoration:underline;" : "";
                  $bg_style_cell .= isset($_color) ? "color:{$_color};" : "";
                }
                break;
            }
          }
          $bg_style_cell .= "'";
          
          $x .= $this->displayPlainCell($j, $t, $bg_style_cell, 1);
        }
        $x .= "</tr>\n";
      }
    }
    $x .= "</table>\n";
    if ($this->db_num_rows()>0)
    {
      $this->navigationHTML = $this->navigation_IP25();
    }
    
    $this->db_free_result();
    return $x;
  }
}

?>