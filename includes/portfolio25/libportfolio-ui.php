<?php
/**
 * [Modification Log] Modifying By: 
 * 
 * *********************************************
 * 2019-06-26 Anna
 * - Modified Get_Management_Advancement_DBTable_SDAS, fix P162850
 * 2019-05-10 Anna
 * - Modified Get_Management_Advancement_DBTable_SDAS(), set subject FullMarkInt as 100 when have not set (for subject as overall result)
 * 
 * 2019-02-26 Anna
 * - modified Get_Management_Advancement_DBTable_SDAS, highchart same value point display format [#J157314]
 * 
 * 2019-02-25 Anna
 * - modified Get_Management_Advancement_DBTable_SDAS, handle cannot show expected mark when target group is class
 * 
 * 2019-01-14 Anna [#155645]
 * - Modified Get_Management_Advancement_DBTable_SDAS(), handle monitoring group don't have yearID cannot get full mark issue 
 * 2019-01-02 Anna
 *  modified Get_Student_DynamicReport_DBTable [#P154991]
 * 
 * 2018-12-20 Anna
 *  modified Get_Management_Advancement_DBTable_SDAS, loss pass target type to get Get_Management_Advancement_DBTable_Sql
 * 
 * 2017-12-13	Pun
 * modified Get_Management_Advancement_DBTable_SDAS(), calculateRegressionData() - changed regression line calculation
 * 
 * 2017-06-26	Villa
 * modified Get_Management_Advancement_DBTable_SDAS() - add monitoring group related
 * 
 * 2017-05-08	Villa #B116602 
 * modified Get_Management_Advancement_DBTable_SDAS() - fix X-Y Axis Problem
 * 
 * 2017-04-07	Villa #X114294 
 * modified Get_Management_Advancement_DBTable_SDAS() - fix <span class='tabletextrequire'>*</span> cause highchart error
 * 
 * 2017-04-06	Villa #J114859 
 * modified Get_Management_Advancement_DBTable_SDAS() - Support Term Assessment
 * 
 * 2017-01-05	Villa #J111033
 * modified Get_Management_Advancement_DBTable_SDAS() - Change Axis max to the full marks of subject
 * 
 * 2016-12-07	Villa
 * modified Get_Management_Advancement_DBTable_SDAS() - fix showing UserID if the filter is closed
 * 													  - Add subject group filter
 * 
 * 2016-11-24	Villa
 * modiflied Get_Management_Advancement_DBTable_SDAS() - add parma to support advanced search
 * 
 * **** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [start] ****
 * 2016-08-03 Ivan [ip.2.5.7.9.1]
 * Modified GEN_PARENT_INFO_TABLE() added CompanyName for macau pui ching (but treat as general improvement)
 * **** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [end] ****
 * 
 * 2016-08-01 Ronald
 * Create Get_Management_Advancement_DBTable_SDAS()
 * 
 * 2016-06-06 Cara
 * Change data table's CSS
 * 
 * 2016-04-18 Omas
 * Modified GEN_CLASS_SELECTION_OPTGROUP(), add param to hide the first option for avoid unauthorized access to All class
 * 
 * 2016-03-11 Pun
 * Modified Get_Management_Advancement_DBTable(), remove sorting by rewrite the UI
 * 
 * 2016-02-18 Pun
 * Modified Get_Management_Advancement_DBTable(), add standard score field
 * 
 * 2016-02-17 Pun
 * Modified Get_Management_Advancement_DBTable(), add academic year name to table header
 * 
 * 2013-03-20 Rita
 * add Get_Student_DynamicReport_DBTable()
 * 
 * 2012-12-04 Bill
 * - add GEN_MULTIPLE_CLASS_SELECTION_OPTGROUP() to handle multiple class selection, element name has to be named as 'name[]'
 * 
 * 2010-04-22 Max (201004221134)
 * - Modify function GET_TAB_MENU() to get information from associative array
 */
class libportfolio_ui{

  # Parameter: Student Object @ libpf_account_student
  function GEN_STUDENT_INFO_TABLE($ParStudentObj)
  {
		global $ec_student_word, $ec_iPortfolio, $profile_dob, $profile_gender, $profile_nationality, $profile_pob, $i_gender_male, $i_gender_female;

		$t_eng_name = $ParStudentObj->GET_CLASS_VARIABLE("english_name");
		$t_chi_name = $ParStudentObj->GET_CLASS_VARIABLE("chinese_name");
		$t_gender = $ParStudentObj->GET_CLASS_VARIABLE("gender");
    $t_dob = substr($ParStudentObj->GET_CLASS_VARIABLE("dob"), 0, 10);
		$t_regno = $ParStudentObj->GET_CLASS_VARIABLE("WebSAMSRegNo");
		$t_nationality = $ParStudentObj->GET_CLASS_VARIABLE("nationality");
		$t_pob = $ParStudentObj->GET_CLASS_VARIABLE("pob");
		$t_home_tel = $ParStudentObj->GET_CLASS_VARIABLE("home_tel");
		$t_address = $ParStudentObj->GET_CLASS_VARIABLE("address");
		
		switch($t_gender)
		{
      case "M":
        $t_gender = $i_gender_male;
        break;
      case "F":
        $t_gender = $i_gender_female;
        break;
      default:
        $t_gender = "--";
    }
		$t_dob = ($t_dob == "" || $t_dob == "0000-00-00") ? "--" : $t_dob;
		$t_regno = ($t_regno == "") ? "--" : $t_regno;
		$t_nationality = ($t_nationality == "") ? "--" : $t_nationality;
		$t_pob = ($t_pob == "") ? "--" : $t_pob;
		$t_home_tel = ($t_home_tel == "") ? "--" : $t_home_tel;
		$t_address = ($t_address == "") ? "--" : $t_address;
  
		$ReturnStr =	"
										<table width=\"100%\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">
											<tr>
												<td class=\"form_field_name\">".$ec_student_word['name_english']."</td>
												<td class=\"form_field_content\">".$t_eng_name."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$ec_student_word['name_chinese']."</td>
												<td class=\"form_field_content\">".$t_chi_name."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$profile_gender."</td>
												<td class=\"form_field_content\">".$t_gender."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$profile_dob."</td>
												<td class=\"form_field_content\">".$t_dob."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$ec_student_word['registration_no']."</td>
												<td class=\"form_field_content\">".$t_regno."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$profile_nationality."</td>
												<td class=\"form_field_content\">".$t_nationality."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$ec_iPortfolio['place_of_birth']."</td>
												<td class=\"form_field_content\">".$t_pob."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$ec_iPortfolio['phone']."</td>
												<td class=\"form_field_content\">".$t_home_tel."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$ec_iPortfolio['address']."</td>
												<td class=\"form_field_content\">".$t_address."</td>
											</tr>
										</table>
									";

		return $ReturnStr;
  }
  
	# Parameter: Student Object @ libpf_account_student
	function GEN_STUDENT_HOUSE_TABLE($ParStudentObj)
	{
		global $ec_iPortfolio, $sys_custom;

		# Get student admission date
		$t_admission_date = $ParStudentObj->GET_CLASS_VARIABLE("admission_date");
		$t_house = $ParStudentObj->GET_CLASS_VARIABLE("student_house");
		
		$t_admission_date = ($t_admission_date == "" || $t_admission_date=="0000-00-00") ? '--' : $t_admission_date;
		$t_house = ($t_house=="") ? '--' : $t_house;
		
		$ReturnStr = "<table width=\"100%\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">";
		if(!isset($sys_custom['student_info_table_no_house']) || !$sys_custom['student_info_table_no_house'])
		{
		  $ReturnStr .=	"
											<tr>
												<td class=\"form_field_name\">".$ec_iPortfolio['house']."</td>
												<td class=\"form_field_content\">".$t_house."</td>
											</tr>
										";
		}
		$ReturnStr .=	"
										<tr>
											<td class=\"form_field_name\">".$ec_iPortfolio['admission_date']."</td>
											<td class=\"form_field_content\">".$t_admission_date."</td>
										</tr>
									";
    $ReturnStr .=	"</table>";

		return $ReturnStr;
	}
	
	# Generate parent information table for display
	function GEN_PARENT_INFO_TABLE($ParStudentObj)
	{
		global $ec_student_word, $ec_guardian, $ec_iPortfolio, $no_record_msg, $i_StudentGuardian_Occupation, $Lang;

    $parent_arr = $ParStudentObj->GET_CLASS_VARIABLE("parent_arr");

		$ReturnStr = "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";

		if(sizeof($parent_arr) > 0)
		{
			for($i=0; $i<sizeof($parent_arr); $i++)
			{
				# Display two guardians per row
				if($i % 2 == 0)
					$ReturnStr .=	"<tr>\n";
				
				$p_arr = $parent_arr[$i];

        $t_is_main = $p_arr["is_main"];
				$t_eng_name = $p_arr["english_name"];
				$t_chi_name = $p_arr["chinese_name"];
				$t_relation = $p_arr["relation"];
				$t_phone = $p_arr["phone"];
				$t_em_phone = $p_arr["em_phone"];
				$t_occupation = $p_arr["Occupation"];
				$t_companyName = $p_arr["CompanyName"];
				
				$t_relation = ($t_relation == "") ? "--" : $ec_guardian[$t_relation];
				$t_phone = ($t_phone == "") ? "--" : $t_phone;
				$t_em_phone = ($t_em_phone == "") ? "--" : $t_em_phone;
				$t_occupation = ($t_occupation == "") ? "--" : $t_occupation;
				$t_companyName = ($t_companyName == "") ? "--" : $t_companyName;

				$ReturnStr .=	"
												<td width='50%'>
													<table width='100%' border='0' cellspacing='6' cellpadding='0'>
											";
				# Highlight "Main guardian"
				$ReturnStr .= ($t_is_main==1) ?	"
																					<tr>
																						<td colspan=\"2\"><span class=\"sub_page_title\">(".$ec_iPortfolio['main_guardian'].")</span></td>
																					</tr>
																				" : "&nbsp;";
				$ReturnStr .=	"
													<tr>
														<td class=\"form_field_name\">".$ec_student_word['name_english']."</td>
														<td class=\"form_field_content\">".$t_eng_name."</td>
													</tr>
													<tr>
														<td class=\"form_field_name\">".$ec_student_word['name_chinese']."</td>
														<td class=\"form_field_content\">".$t_chi_name."</td>
													</tr>
													<tr>
														<td class=\"form_field_name\">".$ec_iPortfolio['relation']."</td>
														<td class=\"form_field_content\">".$t_relation."</td>
													</tr>
													<tr>
														<td class=\"form_field_name\">".$ec_iPortfolio['phone']."</td>
														<td class=\"form_field_content\">".$t_phone."</td>
													</tr>
													<tr>
														<td class=\"form_field_name\">".$ec_iPortfolio['em_phone']."</td>
														<td class=\"form_field_content\">".$t_em_phone."</td>
													</tr>
													<tr>
														<td class=\"form_field_name\">".$i_StudentGuardian_Occupation."</td>
														<td class=\"form_field_content\">".$t_occupation."</td>
													</tr>
													<tr>
														<td class=\"form_field_name\">".$Lang['AccountMgmt']['NameOfCompany/Organization']."</td>
														<td class=\"form_field_content\">".$t_companyName."</td>
													</tr>
												</table>
											</td>
										";
				
				if($i % 2 == 1)
					$ReturnStr .=	"</tr>\n";
				else if(($i+1)==sizeof($parent_arr))
					$ReturnStr .=	"
														<td>&nbsp;</td>\n
													</tr>\n
												";
			}
		}
		else
		{
			$ReturnStr .=	"
											<tr>
												<td>
													<table width='100%' border='0' cellspacing='0' cellpadding='0' height='100'>
														<tr height=60>
															<td colspan=3 align=center class=\"tabletext\">&nbsp;$no_record_msg</td>
														</tr>
													</table>
												</td>
											</tr>
										";
		}
		$ReturnStr .= "</table>";

		return $ReturnStr;
	}
	
	function GEN_BROTHER_SISTER_INFO_TABLE($parStudentId) {
		global $Lang;
		
		$lpf = new libportfolio2007($parStudentId);
		
		$brotherSisterAry = $lpf->GET_BROTHER_SISTER_INFO($parStudentId);
		$numOfBrotherSister = count($brotherSisterAry);
		
		$x = '';
		$x .= '<table class="common_table_list_v30">'."\r\n";
			$x .= '<thead>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<th>'.$Lang['General']['UserLogin'].'</th>'."\r\n";
					$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['ClassTitle'].'</th>'."\r\n";
					$x .= '<th>'.$Lang['General']['ClassNumber'].'</th>'."\r\n";
					$x .= '<th>'.$Lang['General']['StudentNameEng'].'</th>'."\r\n";
					$x .= '<th>'.$Lang['General']['StudentNameChi'].'</th>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</thead>'."\r\n";
			$x .= '<tbody>'."\r\n";
			if ($numOfBrotherSister == 0) {
				$x .= '<tr><td colspan="5">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\r\n";
			}
			else {
				for ($i=0; $i<$numOfBrotherSister; $i++) {
					$_studentId = $brotherSisterAry[$i]['UserID'];
					$_studentUserLogin = $brotherSisterAry[$i]['UserLogin'];
					$_studentNameEn = $brotherSisterAry[$i]['EnglishName'];
					$_studentNameCh = $brotherSisterAry[$i]['ChineseName'];
					$_yearClassId = $brotherSisterAry[$i]['YearClassID'];
					$_className = Get_Lang_Selection($brotherSisterAry[$i]['ClassTitleB5'], $brotherSisterAry[$i]['ClassTitleEN']);
					$_classNumber = $brotherSisterAry[$i]['ClassNumber'];
					
					$x .= '<tr>'."\r\n";
						$x .= '<td>'.$_studentUserLogin.'</td>'."\r\n";
						$x .= '<td>'.$_className.'</td>'."\r\n";
						$x .= '<td>'.$_classNumber.'</td>'."\r\n";
						$x .= '<td><a href="javascript:void(0);" class="tablelink" onclick="jTO_INFO(\''.$_studentId.'\', \''.$_yearClassId.'\');">'.$_studentNameEn.'</a></td>'."\r\n";
						$x .= '<td><a href="javascript:void(0);" class="tablelink" onclick="jTO_INFO(\''.$_studentId.'\', \''.$_yearClassId.'\');">'.$_studentNameCh.'</a></td>'."\r\n";
					$x .= '</tr>'."\r\n";
				}
			}
			
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	# Generate parent information table for display
	function GEN_SELF_ACCOUNT_TABLE($sa_record_id, $self_account_default_arr, $yc_id, $user_id)
	{
		global $ec_student_word, $ec_iPortfolio, $no_record_msg;
		global $image_path, $LAYOUT_SKIN, $button_edit, $file_path;

		$ReturnStr = "";
		$ReturnStr .= "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";
		if(isset($self_account_default_arr))
		{
			/* Edit: 20091218 */
/*
      // Eric Yip (20100610): Skip attachment in self account
			$attachment_str = "";
			
			# Build a string to display attachment
			if($self_account_default_arr['AttachmentPath'] != "")
			{
				$lf = new libfilesystem();				
				$dir_path = $file_path."/file/portfolio/self_account/".$self_account_default_arr['AttachmentPath']."/";					
				$attachment_list = $lf->return_folderlist($dir_path);

				if(is_array($attachment_list))
				{
					for($j=0; $j<count($attachment_list); $j++)
					{
						$attachment_str .= "<a href=\"/file/portfolio/self_account/".$self_account_default_arr['AttachmentPath']."/".basename($attachment_list[$j])."\" target=\"blank\" class=\"tablelink\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_attachment.gif\" width=\"12\" height=\"18\" border=\"0\" align=\"absmiddle\">".basename($attachment_list[$j])."</a><br />\n";
					}
				}					
				unset($lf);
			}		
*/
			# Edit student info button
/*
      // Eric Yip (20100902): No edit in personal info page as there is self-account management module
			$ReturnStr .= "
							<tr>
								<td align=\"right\" style=\"padding:0 10px 0 0;\">
									<a href=\"student_account_teacher_edit.php?StudentID=".$user_id."&RecordID=".$sa_record_id."&YearClassID=".$yc_id."\"> <img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> ".$button_edit."</a>
								</td>
							</tr>
						";
*/
			/* ----  20091218 -----*/
			$ReturnStr .=	"
											<tr>
												<td>
													<table width='100%' border='0' cellspacing='6' cellpadding='0'>
														<tr>
															<td><span class=\"sub_page_title\">(".$self_account_default_arr['Title'].")</span></td>
														</tr>
														<tr>
															<td class=\"form_field_content\">".$self_account_default_arr["Details"]."</td>
														</tr>
													</table>
												</td>
											</tr>
										";
		}
		else
		{
			$ReturnStr .=	"
											<tr>
												<td>
													<table width='100%' border='0' cellspacing='0' cellpadding='0' height='100'>
														<tr height=60>
															<td colspan=3 align=center class=\"tabletext\">&nbsp;$no_record_msg</td>
														</tr>
													</table>
												</td>
											</tr>
										";
		}
		$ReturnStr .= "</table>";

		return $ReturnStr;
	}
	
	// Get The Page tab selection menu
	function GET_TAB_MENU($ParArr="")
	{
    $x = "";
    $x .= "<table border=\"0\" cellpadding=\"7\" cellspacing=\"0\" width=\"100%\">";
    $x .= "<tbody>";
    $x .= "<tr>";
    $x .= "<td class=\"tab_underline\" nowrap>";
    $x .= "<div class=\"shadetabs\">";
    $x .= "<ul>";
    
    foreach($ParArr as $key => $element) {
        $Link = $element[0];
        $Title = $element[1];
        $Selected = $element[2];
        
        if($Selected == 1)
        $ClassStyle = "selected";
        else
    	$ClassStyle = "";
        
   		$x .= "<li class=\"".$ClassStyle."\"><a href=\"".$Link."\"><strong>".$Title."</strong></a></li>";
    }
    
    $x .= "</ul>";
    $x .= "</div>";
    $x .= "</td>";
    $x .= "</tr>";
    $x .= "</tbody>";
    $x .= "</table>";
    
    return $x;
	}
	
	// Generate class selection with optgroup
	function GEN_CLASS_SELECTION_OPTGROUP($ParClassArr, $ParAttr="", $ParSelected="", $ParGeneralOpt=true, $hideFirst=false, $ParReplaceGeneralOpt=array())
	{
    global $ec_iPortfolio,$button_select;
	
    $x = "<select {$ParAttr}>";
    if(!$hideFirst){
    	$firstWord = (empty($ParReplaceGeneralOpt))?"-- ".$button_select." --":$ParReplaceGeneralOpt[1];
    	$firstValue = (empty($ParReplaceGeneralOpt))?"":$ParReplaceGeneralOpt[0];
    	$x .= ($ParGeneralOpt) ? "<option value=\"$firstValue\">$firstWord</option>" : "";
    }
    if(is_array($ParClassArr))
    {
      foreach($ParClassArr AS $classlevel_name => $t_class_arr)
      {
        $x .= "<optgroup label=\"".$classlevel_name."\">";
        for($i=0; $i<count($t_class_arr); $i++)
        {
          $t_selected = ($ParSelected == $t_class_arr[$i][0]) ? "SELECTED" : "";
          
          $x .= "<option value=\"".$t_class_arr[$i][0]."\" {$t_selected}>".$t_class_arr[$i][1]."</option>";
        }
        $x .= "</optgroup>";
      }
    }
    $x .= "</select>";
    
    return $x;
	}
	
	// Generate Multiple Class selection with optgroup
	function GEN_MULTIPLE_CLASS_SELECTION_OPTGROUP($ParName, $ParID, $ParClassArr, $ParAttr="", $ParSelected="", $JsAction="")
	{
    global $ec_iPortfolio,$button_select;
	$x = '';
    $x .= "<select {$ParAttr} name='{$ParName}' id='{$ParID}' multiple='multiple' onchange='{$JsAction}'>";
    //
    if(is_array($ParClassArr))
    {
      foreach($ParClassArr AS $classlevel_name => $t_class_arr)
      {
        $x .= '<optgroup label="'.$classlevel_name.'">';
        for($i=0; $i<count($t_class_arr); $i++)
        {
          //$t_selected = ($ParSelected == $t_class_arr[$i][0]) ? "SELECTED" : "";
          $t_selected = in_array($t_class_arr[$i][0], (array)$ParSelected) ? "SELECTED" : "";
          $x .= "<option value=\"".$t_class_arr[$i][0]."\" {$t_selected}>".$t_class_arr[$i][1]."</option>";
        }
        $x .= "</optgroup>";
      }
    }
    $x .= "</select>";
    
    $x .= '<script>
			CtrlPressed = false;
			$(\'#'.$ParID.' optgroup\').click(function(e){
				if ($(e.target).is(\'option\')){ return; }
				if (CtrlPressed==false) { $(\'#'.$ParID.' option\').attr(\'selected\',\'\'); }
				$(this).children().attr(\'selected\',\'selected\');
				'.$JsAction.'
			})
		 	$(document).keydown(function(e) { CtrlPressed = e.keyCode==17?true:false; })
		 	$(document).keyup(function(e) { CtrlPressed = false; })
		  </script>';
	
    return $x;
	}
  
	function Get_Management_Advancement_DBTable($YearID, $YearClassID, $FromAcademicYearID, $FromYearTermID, $FromSubjectID, $ToAcademicYearID, $ToYearTermID, $ToSubjectID, $field='',$order='',$page='')
	{
		global $PATH_WRT_ROOT, $Lang, $page_size, $ck_management_advancement_page_size, $intranet_session_language, $ec_iPortfolio, $LAYOUT_SKIN;
		
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		include_once($PATH_WRT_ROOT."includes/libportfolio.php");
		include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
		include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		
		$field = ($field=='')? 3 : $field;
		$order = ($order=='')? 0 : $order;
		$page = ($page=='')? 1 : $page;
		
		
		if (isset($ck_management_advancement_page_size) && $ck_management_advancement_page_size != "") {
			$page_size = $ck_management_advancement_page_size;
		}
		$libdbtable = new libdbtable2007($field, $order, $page);
		$libpf_asr = new libpf_asr();
		$libTerm = new academic_year_term();
		
		### Get Term Names
		$FromYearTermName = ($FromYearTermID == '')? $Lang['General']['WholeYear'] : $libTerm->Get_YearNameByID($FromYearTermID, $intranet_session_language);
		$ToYearTermName = ($ToYearTermID == '')? $Lang['General']['WholeYear'] : $libTerm->Get_YearNameByID($ToYearTermID, $intranet_session_language);
		
		### Get Academic Year Names
		$objFromYear = new academic_year($FromAcademicYearID);
		$FromYearName = $objFromYear->Get_Academic_Year_Name();
		$objToYear = new academic_year($ToAcademicYearID);
		$ToYearName = $objToYear->Get_Academic_Year_Name();
		
		### Get DBTable SQL
		$sql = $libpf_asr->Get_Management_Advancement_DBTable_Sql($YearID, $YearClassID, $FromAcademicYearID, $FromYearTermID, $FromSubjectID, $ToAcademicYearID, $ToYearTermID, $ToSubjectID);
		/* Old Code * /
		$libdbtable->sql = $sql;
		$libdbtable->IsColOff = "IP25_table";
		
		$libdbtable->field_array = array("ClassName", "ClassNumber", "StudentName", "FromScore", "ToScore", "ScoreDiff", "ScoreDiffPercentage", "FromZScore", "ToZScore", "ZScoreDiffDisplay", "ZScoreDiffPercentageDisplay");
		$libdbtable->column_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		$libdbtable->wrap_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		$li->fieldorder2 = " ,ClassName, ClassNumber";
		
		$pos = 0;
		$libdbtable->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
		$libdbtable->column_list .= "<th width='5%'>".$libdbtable->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['Class'])."</td>\n";
		$libdbtable->column_list .= "<th width='5%'>".$libdbtable->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['ClassNo'])."</td>\n";
		$libdbtable->column_list .= "<th width='9%' >".$libdbtable->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['StudentName'])."</td>\n";
		$libdbtable->column_list .= "<th width='11%'>{$FromYearName} {$FromYearTermName}<br />{$ec_iPortfolio["score"]}</td>\n";
		$libdbtable->column_list .= "<th width='11%'>{$ToYearName} {$ToYearTermName}<br />{$ec_iPortfolio["score"]}</td>\n";
		$libdbtable->column_list .= "<th width='9%'>".$libdbtable->column_IP25($pos++, $Lang['iPortfolio']['MarkDifference'])."</td>\n";
		$libdbtable->column_list .= "<th width='9%'>".$libdbtable->column_IP25($pos++, $Lang['iPortfolio']['MarkDifference(%)'])."</td>\n";
		
		$libdbtable->column_list .= "<th width='11%'>{$FromYearName} {$FromYearTermName}<br />{$ec_iPortfolio['stand_score']}</td>\n";
		$libdbtable->column_list .= "<th width='11%'>{$ToYearName} {$ToYearTermName}<br />{$ec_iPortfolio['stand_score']}</td>\n";
		$libdbtable->column_list .= "<th width='9%'>".$Lang['iPortfolio']['ZScoreDifference']."</td>\n";
		$libdbtable->column_list .= "<th width='9%'>".$Lang['iPortfolio']['ZScoreDifference(%)']."</td>\n";
		$libdbtable->no_col = $pos + 7;
	
		$x .= $libdbtable->display();
// 		debug_pr($libdbtable->built_sql());
//		debug_pr(mysql_error());
		$x .= '<input type="hidden" name="pageNo" value="'.$libdbtable->pageNo.'" />';
		$x .= '<input type="hidden" name="order" value="'.$libdbtable->order.'" />';
		$x .= '<input type="hidden" name="field" value="'.$libdbtable->field.'" />';
		$x .= '<input type="hidden" name="page_size_change" value="" />';
		$x .= '<input type="hidden" name="numPerPage" value="'.$libdbtable->page_size.'" />';
		/* */
		
		/* Rewrite code to not use libdbtable */
		$rs = $libdbtable->returnResultSet($sql);
		foreach($rs as $rs_key=>$_rs){
			unset($rs[$rs_key]['StudentID']);
		}
		//debug_r($rs);
		
		######## Report UI START ########
		$x = "<div class='chart_tables'><table class='common_table_list_v30 view_table_list_v30' id='resultTable'>";
		
		$x .= '<thead>';
		$x .= '<tr>';
		$x .= '<th>#</th>';
		$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</th>';
		$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</th>';
		$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['StudentName'].'</th>';
		$x .= '<th>'."{$FromYearName} {$FromYearTermName}<br />{$ec_iPortfolio["score"]}".'</th>';
		$x .= '<th>'."{$ToYearName} {$ToYearTermName}<br />{$ec_iPortfolio["score"]}".'</th>';
		$x .= '<th>'.$Lang['iPortfolio']['MarkDifference'].'</th>';
		$x .= '<th>'.$Lang['iPortfolio']['MarkDifference(%)'].'</th>';
		
		$x .= '<th>'."{$FromYearName} {$FromYearTermName}<br />{$ec_iPortfolio['stand_score']}".'</th>';
		$x .= '<th>'."{$ToYearName} {$ToYearTermName}<br />{$ec_iPortfolio['stand_score']}".'</th>';
		$x .= '<th>'.$Lang['iPortfolio']['ZScoreDifference'].'</th>';
		$x .= '<th>'.$Lang['iPortfolio']['ZScoreDifference(%)'].'</th>';
		$x .= '</tr>';
		$x .= '</thead>';

		foreach ($rs as $index=>$r){
			$x .= '<tr>';
			$x .= '<td>'.($index+1).'</td>';
			foreach ($r as $data){
				$x .= "<td>{$data}</td>";
			}
			$x .= '</tr>';
		}
		
		$x .= '</table></div>';
		######## Report UI END ########

		$floatHeader = <<<HTML
		  		<link href="/templates/{$LAYOUT_SKIN}/css/content_30.css" rel="stylesheet" type="text/css">
  				<script>\$(function() {\$('#resultTable').floatHeader();});</script>
HTML;
		return $floatHeader.$x;
	}
	
	function Get_Management_Advancement_DBTable_SDAS($YearID, $YearClassID, $FromAcademicYearID, $FromYearTermID, $FromSubjectID, $ToAcademicYearID, $ToYearTermID, $ToSubjectID, $field='',$order='',$page='',$sort_by, $sort_order, $low_score, $high_score, $x_name, $y_name,$Advanced_Search='',$TargetType='')
	{
		global $PATH_WRT_ROOT, $Lang, $page_size, $ck_management_advancement_page_size, $intranet_session_language, $ec_iPortfolio, $eclass_db, $LAYOUT_SKIN;
	
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		include_once($PATH_WRT_ROOT."includes/libportfolio.php");
		include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
		include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
		include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		include_once($PATH_WRT_ROOT."includes/json.php");
		$json = new JSON_obj();
		$field = ($field=='')? 3 : $field;
		$order = ($order=='')? 0 : $order;
		$page = ($page=='')? 1 : $page;
	
		if (isset($ck_management_advancement_page_size) && $ck_management_advancement_page_size != "") {
			$page_size = $ck_management_advancement_page_size;
		}
		$libdbtable = new libdbtable2007($field, $order, $page);
		$libpf_asr = new libpf_asr();
		$libTerm = new academic_year_term();
	
		### Get Term Names
		#J114859 
		$FromYearTermID_array = explode('_',$FromYearTermID);
		$ToYearTermID_array = explode('_',$ToYearTermID);
		if(sizeof($FromYearTermID_array)>1){
			$FromYearTermName = $FromYearTermID_array[1];
		}else{
			//Old Logic
			$FromYearTermName = ($FromYearTermID == '')? $Lang['General']['WholeYear'] : $libTerm->Get_YearNameByID($FromYearTermID, $intranet_session_language);
		}
		if(sizeof($ToYearTermID_array)>1){
			$ToYearTermName = $ToYearTermID_array[1];
		}else{
			//Old Logic
			$ToYearTermName = ($ToYearTermID == '')? $Lang['General']['WholeYear'] : $libTerm->Get_YearNameByID($ToYearTermID, $intranet_session_language);
		}
// 		$FromYearTermName = ($FromYearTermID == '')? $Lang['General']['WholeYear'] : $libTerm->Get_YearNameByID($FromYearTermID, $intranet_session_language);
// 		$ToYearTermName = ($ToYearTermID == '')? $Lang['General']['WholeYear'] : $libTerm->Get_YearNameByID($ToYearTermID, $intranet_session_language);

		#### Calculate Regression Line by whole year START ####
		if($TargetType['Target']=='SubjectClass'){
		    $subjectGroup = new subject_term_class($TargetType['SubjectGroup']);
		    $YearID = $subjectGroup->YearID[0];
		}
		
		if($TargetType['Target']=='SubjectClass' || $TargetType['Target']=='MonitoringGroup'){
            $yearSql = $libpf_asr->Get_Management_Advancement_DBTable_Sql($YearID, $YearClassID, $FromAcademicYearID, $FromYearTermID, $FromSubjectID, $ToAcademicYearID, $ToYearTermID, $ToSubjectID,$TargetType);
		}else{
		    $yearSql = $libpf_asr->Get_Management_Advancement_DBTable_Sql($YearID, $YearClassID, $FromAcademicYearID, $FromYearTermID, $FromSubjectID, $ToAcademicYearID, $ToYearTermID, $ToSubjectID);		    
		}
		$yearRs = $libdbtable->returnResultSet($yearSql);
		
		$sql = "TRUNCATE TMP_IPF_MGMT_ADVANCEMENT";
		$libdbtable->db_db_query($sql);
		
		$regressionData = array();
		foreach($yearRs as $r){
		    if(is_numeric($r['FromScore']) && is_numeric($r['ToScore'])){
		        $regressionData[] = array($r['FromScore'],$r['ToScore']);
		    }
		}

		
		$regressionData = $this->calculateRegressionData($regressionData);
		ksort($regressionData, SORT_NUMERIC);
		
		$count = count($regressionData);
		$regressionLine = array();
		if($count){
    		$xArr = array_keys($regressionData);
    		$yArr = array_values($regressionData);
    		$regressionLine = array(
    		    array(floatval(my_round($xArr[0],1)), floatval(my_round($yArr[0],1))),
    		    array(floatval(my_round($xArr[$count-1],1)), floatval(my_round($yArr[$count-1],1))),
    		);
		}
		$regressionLinePoint = $json->encode($regressionLine);
		#### Calculate Regression Line by whole year END ####
		
		### Get Academic Year Names
		$objFromYear = new academic_year($FromAcademicYearID);
		$FromYearName = $objFromYear->Get_Academic_Year_Name();
		$objToYear = new academic_year($ToAcademicYearID);
		$ToYearName = $objToYear->Get_Academic_Year_Name();
		
		### Get DBTable SQL
		if($TargetType['Target']=='SubjectClass' || $TargetType['Target']=='MonitoringGroup'){
			$sql = $libpf_asr->Get_Management_Advancement_DBTable_Sql($YearID, $YearClassID, $FromAcademicYearID, $FromYearTermID, $FromSubjectID, $ToAcademicYearID, $ToYearTermID, $ToSubjectID, $TargetType);
		}else{
			$sql = $libpf_asr->Get_Management_Advancement_DBTable_Sql($YearID, $YearClassID, $FromAcademicYearID, $FromYearTermID, $FromSubjectID, $ToAcademicYearID, $ToYearTermID, $ToSubjectID);
		}
		/* Rewrite code to not use libdbtable */
		$rs = $libdbtable->returnResultSet($sql);
		
		
// 		$rs_2 = $rs; //copy from rs
		if($Advanced_Search['Advanced_Search']){ //if open the advanced searching
			$cond = '';
			$AND = "AND";
			foreach ($Advanced_Search as $filter_key=>$filter){
				switch ($filter_key){
					
					case 'gender':
// 						$gender=implode(',',(array)$filter);
						$cond .= $AND." Gender = '{$gender}' ";
						
						break;
// 					case 'non_chinese':
// 						$cond .= $AND." NonChineseSpeaking = '{$filter}' ";
// 						break;
// 					case 'come_from_china':
// 						$cond .= $AND." NonChineseSpeaking = '{$filter}' ";
// 						break;
// 					case 'cross_boundary':
// 						$cond .= $AND." NonChineseSpeaking = '{$filter}' ";
// 						break;
// 					case 'SEN':
// 						$SEN = implode(',',(array)$filter);
// 						$cond .= $AND." NonChineseSpeaking IN ({$SEN}) ";
// 						break;
					default:
						break;
				}
			}
			foreach($rs as $key=>$studentRecord){
				$sql_1 = "SELECT UserID FROM INTRANET_USER WHERE UserID = '{$studentRecord['StudentID']}' ".$cond;
				$result = $libdbtable->returnResultSet($sql_1);
				if(empty($result[0])){
					unset($rs[$key]); // filter the unwanted record
				}else{
					unset($rs[$key]['StudentID']);// unset student ID avoid display in the tb
				}
			}
		}
		
		if($TargetType['Target']=='MonitoringGroup'){
		    $StudentIDAry = Get_Array_By_Key($rs, 'StudentID');
		    $YearID = $libpf_asr->returnStudentsClassLevel($StudentIDAry);
		}
		
		foreach($rs as $rs_key=>$_rs){
			unset($rs[$rs_key]['StudentID']);
		}
		foreach($rs as $rs_key=>$r){
		    if(is_numeric($r['FromScore']) && is_numeric($r['ToScore'])){
		        $expectScoreDisplay = my_round($regressionData[$r['FromScore']], 2);
		        $expectScoreDiffDisplay = my_round($r['ToScore'] - $regressionData[$r['FromScore']], 2);
		    }else{
		        $expectScoreDiffDisplay = $expectScoreDisplay = $Lang['General']['EmptySymbol'];
		    }
		    
		    $rs[$rs_key] = array(
		        'ClassName' => $r['ClassName'],
		        'ClassNumber' => $r['ClassNumber'],
		        'StudentName' => $r['StudentName'],
		        'FromScore' => $r['FromScore'],
		        'ToScore' => $r['ToScore'],
		        'ExpectedScoreDisplay' => $expectScoreDisplay,
		        'ExpectedScoreDiffDisplay' => $expectScoreDiffDisplay,
		        'FromZScore' => $r['FromZScore'],
		        'ToZScore' => $r['ToZScore'],
		        'ZScoreDiffDisplay' => $r['ZScoreDiffDisplay'],
		        'ZScoreDiffPercentageDisplay' => $r['ZScoreDiffPercentageDisplay'],
		    );
		}
		
		### HighChart Data Strat ### 
		### Get DBTable SQL
		$rs_2 = $libdbtable->returnResultSet($sql);
		if($Advanced_Search['Advanced_Search']){ //if open the advanced searching
			$cond = '';
			$AND = "AND";
			foreach ($Advanced_Search as $filter_key=>$filter){
				switch ($filter_key){
		
					case 'gender':
// 						$gender=implode(',',(array)$filter);
// 						$cond .= $AND." Gender IN ({$gender}) ";
						$cond .= $AND." Gender = '{$gender}' ";
						break;
						// 						case 'non_chinese':
						// 							$cond .= $AND." NonChineseSpeaking = '{$filter}' ";
						// 							break;
						// 						case 'come_from_china':
						// 							$cond .= $AND." NonChineseSpeaking = '{$filter}' ";
						// 							break;
						// 						case 'cross_boundary':
						// 							$cond .= $AND." NonChineseSpeaking = '{$filter}' ";
						// 							break;
						// 						case 'SEN':
						// 							$SEN = implode(',',(array)$filter);
						// 							$cond .= $AND." NonChineseSpeaking IN ({$SEN}) ";
						// 							break;
					default:
						break;
				}
			}
			foreach($rs_2 as $key=>$studentRecord){
				$sql = "SELECT UserID FROM INTRANET_USER WHERE UserID = '{$studentRecord['StudentID']}' ".$cond;
				$result = $libdbtable->returnResultSet($sql);
				if(empty($result[0])){
					unset($rs_2[$key]); // filter the unwanted record
				}else{
					unset($rs_2[$key]['StudentID']);// unset student ID avoid display in the tb
				}
			}
		}
		foreach($rs_2 as $k1=>$q) {

			unset($rs_2[$k1]['ScoreDiffDisplay']);
			unset($rs_2[$k1]['ScoreDiffPercentageDisplay']);
			unset($rs_2[$k1]['FromZScore']);
			unset($rs_2[$k1]['ToZScore']);
			unset($rs_2[$k1]['ZScoreDiffDisplay']);
			unset($rs_2[$k1]['ZScoreDiffPercentageDisplay']);
			$rs_2[$k1]['x'] = floatval($rs_2[$k1]['FromScore']);
			$rs_2[$k1]['y'] = floatval($rs_2[$k1]['ToScore']);
			unset($rs_2[$k1]['FromScore']);
			unset($rs_2[$k1]['ToScore']);
			$pos = strpos($rs_2[$k1]['StudentName'],'<span class="tabletextrequire">*</span>');
			if($pos !== false){
				$rs_2[$k1]['StudentName'] = str_replace('<span class="tabletextrequire">*</span>','',$rs_2[$k1]['StudentName']);
			}
		}
		### XAxis YAxis Max ###
		$sql= "Select
					FullMarkInt
				From
					$eclass_db.ASSESSMENT_SUBJECT_FULLMARK 
				WHERE
					AcademicYearID = $FromAcademicYearID
				AND 
					YearID IN ('".implode('\',\'',(array)$YearID)."')
				AND
					SubjectID = $FromSubjectID";
		$temp = $libdbtable->returnResultSet($sql);
		// 		$XAxisMax= ($temp[0]['FullMarkInt']>0)? $temp[0]['FullMarkInt']:100;
		$emptyFullMarkAry[0]['FullMarkInt'] = '100';
		$temp = empty($temp)?$emptyFullMarkAry:$temp;
		$FullMarkIntAry = Get_Array_By_Key($temp, 'FullMarkInt');
		$XAxisMax= (max($FullMarkIntAry))? max($FullMarkIntAry):100;
		
		
		$sql= "Select
					FullMarkInt
				From
					$eclass_db.ASSESSMENT_SUBJECT_FULLMARK
				WHERE
					AcademicYearID = $ToAcademicYearID
				AND
				    YearID IN ('".implode('\',\'',(array)$YearID)."')
				AND
					SubjectID = $ToSubjectID";
		$temp = $libdbtable->returnResultSet($sql);
		$temp = empty($temp)?$emptyFullMarkAry:$temp;
		// 		$YAxisMax= ($temp[0]['FullMarkInt']>0)? $temp[0]['FullMarkInt']:100;
		$FullMarkIntAry = Get_Array_By_Key($temp, 'FullMarkInt');
		$YAxisMax= (max($FullMarkIntAry))? max($FullMarkIntAry):100;
		
		
		$tmp = array();
		
		foreach($rs_2 as $arg)
		{
			$tmp[$arg['ClassName']][] = $arg;
		}
		
		$tmp_2 = array();
		$i = 0;
		foreach($tmp as $arg)
		{
			$tmp_2[$i] = $arg;
			$i++;
		}
		
		$rs_2 = $tmp_2;
		$rs_2 = $json->encode($rs_2);
		#X114294 
// 		$rs_highchart = str_replace("<span class='tabletextrequire'>*</span>", "", $rs_2);
		#P162850
		$rs_highchart = str_replace("<span class='tabletextrequire'>*", " ", $rs_2);
		$rs_highchart = str_replace("</span>", " ", $rs_highchart);
		### HighChart Data END ###
		?>
		<script>
			var result_data='<?=$rs_highchart?>';
			result_data = JSON.parse(result_data);
			var to_chart_data = new Array();
			
			var i;
			to_chart_data[0] = {
			        type: 'line',
			        name: 'Regression Line',
			        data: <?=$regressionLinePoint ?>,
			        marker: {
			            enabled: false
			        },
			        states: {
			            hover: {
			                lineWidth: 0
			            }
			        },
			};
			for(i = 0; i < result_data.length; i++){
				to_chart_data[i+1] = {name: result_data[i][0]['ClassName'], type: 'scatter', data: result_data[i] };
			}

			<?php if($TargetType['Target']=='MonitoringGroup'){ ?>
				to_chart_data.shift();
			<?php } ?>
			
			$('.chart').highcharts({
	
			title: {
		        text: 'Scatter Plot'
		    },
			 xAxis: {
		         min: 0,
		         max: <?=$XAxisMax?>,
		         title: {
		             text: '<?=$x_name?>'
		         },
		         
		     },
		     yAxis: {
		         min: 0,
		         max: <?=$YAxisMax?>,
		         title: {
		             text: '<?=$y_name?>'
		         },
		     },
		     tooltip: {
		         useHTML: true,
// 		         headerFormat: '<table>',
// 		         pointFormat: '<tr><th colspan="2"><h3>{point.ClassName} {point.StudentName}</h3></th></tr>' +
//		             '<tr><th><?=$x_name?> : </th><td>{point.x}</td></tr>' +
//		             '<tr><th><?=$y_name?> : </th><td>{point.y}</td></tr>',
// 		         footerFormat: '</table>',
		         formatter:function(){

			        	 var series = this.series,
	                     x = this.x,
	                     y = this.y,
	                     tooltip = '<table>';

			        	 $.each(series.data,function(i,point){
			        		  if(point.x === x && point.y === y)
	                          tooltip += 
		                            	'<tr><th colspan="2"><h3>' + point.ClassName + point.StudentName +'</h3></th></tr>'+
			                            ' <tr><th><?=$x_name?>: </th><td>' + point.x + '</td></tr>' + 
		                            	' <tr><th><?=$y_name?>: </th><td>' + point.y + '</td></tr>';
		                    });

			        	 tooltip += '</table>';
						 return tooltip;
				 },
		         followPointer: true
		     },
		    series: to_chart_data,
		    credits : {enabled: false,},
		});
		</script>	
		
		<?php
		//debug_r($rs);
	
		function sort_result_s($a,$b){
			return ($a['ExpectedScoreDiffDisplay'] < $b['ExpectedScoreDiffDisplay']) ? -1 : 1;
		}
		function sort_result_z($a,$b){
			return ($a['ZScoreDiffDisplay'] < $b['ZScoreDiffDisplay']) ? -1 : 1;
		}
	
		if($sort_by == 1){
			usort($rs, 'sort_result_s');
		}else if($sort_by == 2){
			usort($rs, 'sort_result_z');
		}
	
		if($sort_by >= 0 && $sort_order == 1){
			$rs = array_reverse($rs);
		}
		######## Report UI START ########
		$x = '<div class="chart" style="height: 500px"></div> <div align="center" style="color:#707070" id="hide_show"></div><br/>';
		$x .= "<span class=\"tabletextremark\">{$Lang['iPortfolio']['RegressionLineEquation']}</span>";
		$x .= "<div class='chart_tables'><table class='common_table_list_v30 view_table_list_v30' id='resultTable'>";
	
		$x .= '<thead>';
		$x .= '<tr>';
		$x .= '<th>#</th>';
		$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</th>';
		$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</th>';
		$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['StudentName'].'</th>';
		$x .= '<th>'."{$FromYearName} {$FromYearTermName}<br />{$ec_iPortfolio["score"]}".'</th>';
		$x .= '<th>'."{$ToYearName} {$ToYearTermName}<br />{$ec_iPortfolio["score"]}".'</th>';
		/*$x .= '<th>'.$Lang['iPortfolio']['MarkDifference'].'</th>';
		$x .= '<th>'.$Lang['iPortfolio']['MarkDifference(%)'].'</th>';*/
		
		$x .= "<th>{$ToYearName} {$ToYearTermName}<br />{$Lang['iPortfolio']['ExpectedMark']}</th>";
		$x .= '<th>'.$Lang['iPortfolio']['ExpectedMarkDifference'].'</th>';
	
		$x .= '<th>'."{$FromYearName} {$FromYearTermName}<br />{$ec_iPortfolio['stand_score']}".'</th>';
		$x .= '<th>'."{$ToYearName} {$ToYearTermName}<br />{$ec_iPortfolio['stand_score']}".'</th>';
		$x .= '<th>'.$Lang['iPortfolio']['ZScoreDifference'].'</th>';
		$x .= '<th>'.$Lang['iPortfolio']['ZScoreDifference(%)'].'</th>';
		
		$x .= '</tr>';
		$x .= '</thead>';
	
		foreach ($rs as $index=>$r){
			if($r['ExpectedScoreDiffDisplay'] == $Lang['General']['EmptySymbol']){
				$x .= '<tr>';
			}else if($r['ExpectedScoreDiffDisplay'] > $high_score){
				$x .= '<tr class=high_score >';
			}else if ($r['ExpectedScoreDiffDisplay'] < $low_score){
				$x .= '<tr class=low_score >';
			}else{
				$x .= '<tr>';
			}
			$x .= '<td>'.($index+1).'</td>';
			foreach ($r as $index => $data){
			    $x .= "<td>{$data}</td>";
			}
			
			
			$x .= '</tr>';
		}
	
		$x .= '</table></div>';
		######## Report UI END ########
	
		$floatHeader = <<<HTML
		  		<link href="/templates/{$LAYOUT_SKIN}/css/content_30.css" rel="stylesheet" type="text/css">
  				<script>\$(function() {\$('#resultTable').floatHeader();});</script>
HTML;
		return $floatHeader.$x;
	}
	
	/**
	 * Calculate the regression data [ x1 => E(y1), x2 => E(y2), ... ]
	 * Data: [ (dx1,dy1), (dx2,dy2) ... ]
	 */
	function calculateRegressionData($data){
	    $countData = count($data);
	    
	    if($countData == 0){
	        return array();
	    }
	    
	    $xArr = array();
	    $yArr = array();
	    foreach($data as $d){
	        $xArr[] = $d[0];
	        $yArr[] = $d[1];
	    }
	    
	    $s2MeanBarY = array_sum($yArr) / $countData;
	    $s1MeanBarX = array_sum($xArr) / $countData;
	    
	    
	    $yiSubBarY = array();
	    foreach($yArr as $y){
	        $yiSubBarY[] = $y - $s2MeanBarY;
	    }
	    
	    
	    $xiSubBarX = array();
	    foreach($xArr as $x){
	        $xiSubBarX[] = $x - $s1MeanBarX;
	    }
	    
	    
	    $product_yiSubBarY_xiSubBarX = array();
	    for($i=0;$i<$countData;$i++){
	        $product_yiSubBarY_xiSubBarX[] = $yiSubBarY[$i] * $xiSubBarX[$i];
	    }
	    $sum_product_yiSubBarY_xiSubBarX = array_sum($product_yiSubBarY_xiSubBarX);
	    
	    $square_xiSubBarX = array();
	    foreach($xiSubBarX as $x){
	        $square_xiSubBarX[] = $x * $x;
	    }
	    $sum_square_xiSubBarX = array_sum($square_xiSubBarX);
	    
	    
	    $b1 = $sum_product_yiSubBarY_xiSubBarX / $sum_square_xiSubBarX;
	    $b0 = $s2MeanBarY - ($b1 * $s1MeanBarX);
	    
	    
	    $regressionEquation = array();
	    foreach($xArr as $x){
	        $value = $b0 + ($b1 * $x);
	        $regressionEquation[$x] = $value;
	    }
	    
	    return $regressionEquation;
	}
	
	/*
	 * $CurrentStep starts from 1
	 */
	function Get_OEA_SelectFromSLP_Steps_Arr($CurrentStep)
	{
		global $Lang, $linterface;
		
		$thisStep = 0;
		$STEPS_OBJ = array();
		$STEPS_OBJ[$thisStep++] = array($Lang['Btn']['Select'], ($CurrentStep == $thisStep)? true : false);
		$STEPS_OBJ[$thisStep++] = array($Lang['iPortfolio']['OEA']['Convert'], ($CurrentStep == $thisStep)? true : false);
		$STEPS_OBJ[$thisStep++] = array($Lang['Btn']['Confirm'], ($CurrentStep == $thisStep)? true : false);
		
		$x = '';
		$x .= $linterface->GET_STEPS_IP25($STEPS_OBJ);
		
		return $x;
	}
	
	function GET_STEP_CHAIN($StepArr, $CurrentStep="")
	{
		global $Lang;
	  
		$x = '';
		$x .= "<div class=\"task_steps\">";
			$x .= "<ul class=\"line\"><span>".$Lang['iPortfolio']['OEA']['SLP']."</span>";
			
			$numOfStep = count($StepArr);
			for($i=0; $i<$numOfStep; $i++)
			{
				$thisStep = $i + 1;
				$thisSLPName = $StepArr[$i]['SLPName'];
				$thisClass = ($thisStep == $CurrentStep)? 'class="current"' : '';
				
				$x .= "<li class=\"circle\">";
					$x .= (($i + 1) == $CurrentStep) ? "<a class=\"current\" href=\"javascript:;\" title=\"".$thisSLPName."\">" : "<a href=\"javascript:doStep({$t_step_id})\" title=\"".$thisSLPName."\">";
					$x .= $thisStep."</a></li>";
				$x .= ($i == $numOfStep - 1)? "" : "<li class=\"spacer\"></li>";
			}
			$x .= "</ul>";
		$x .= "</div>";
		    
		return $x;
	}
	
	function Get_OEA_SelectFromSLP_Confirm_Table($SlpIDArr)
	{
		global $Lang, $linterface;
		
		$numOfSlp = count($SlpIDArr);
		$x = '';
		
//		$x .= '<div style="width:95%;">'."\n";
//			$x .= '<div class="common_table_tool">'."\n";
//			$x .= '<a class="tool_delete" href="#">'.$Lang['Btn']['Delete'].'</a>'."\n";		    
//			$x .= '</div>'."\n";	
//		$x .= '</div>'."\n";	
//		$x .= '<br style="clear:both;" />'."\n";
		$x .= "<table border = \"0\" width='95%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
		
		$x .= "<tr class=\"tabletop\">";
		$x .= "<td>&nbsp;</td>";
		$x .= "<td>Title </td>";
		$x .= "<td>Category </td>";
		$x .= "<td>Role</td>";
		$x .= "<td>Achievement</td>";
		$x .= "<td>&nbsp;</td>";
		$x .= "<td><input type=\"checkbox\" onclick=\"(this.checked)?setChecked(1,this.form,'oea_id[]'):setChecked(0,this.form,'oea_id[]')\" name=\"checkmaster\" checked></td>";
		$x .= "</tr>";
		
		$bgColor = '#F3F3F3';
		for($i=0; $i<$numOfSlp; $i++)
		{
			$_rowNum = $i + 1;
			//$_recordID = $returnArray[$i]["RecordID"];
			$_pTitle   = 'Program1';
			$_catTitle = 'Community Service';
			$_role = 'Leader';
			$_achievement = 'Champion / Gold Medal';
			$_thickBoxEditLink = $linterface->Get_Thickbox_Div(400, 550, 'edit', 'Edit '.$_pTitle.' with thickbox', '', $InlineID="FakeLayer", $Content="");
		
		
			$bgColor = ($bgColor=="#F3F3F3") ? "#FFFFFF": "#F3F3F3";
		
			
			$x .= "<tr>";
		
			$x .= "<td bgcolor = \"{$bgColor}\">";
			$x .= "{$_rowNum}";
			$x .= "</td>";
		
			$x .= "<td bgcolor = \"{$bgColor}\">";
			$x .= "{$_pTitle}";
			$x .= "</td>";
			$x .= "<td bgcolor = \"{$bgColor}\">";
			$x .= $_catTitle;
			$x .= "</td>";
		
			$x .= "<td bgcolor = \"{$bgColor}\">";
			$x .= $_role;
			$x .= "</td>";
		
			$x .= "<td bgcolor = \"{$bgColor}\">";
			$x .= $_achievement;
			$x .= "</td>";
			
			$x .= "<td bgcolor = \"{$bgColor}\">";
			$x .= $_thickBoxEditLink;
			$x .= "</td>";
		
			$x .= "<td bgcolor = \"{$bgColor}\">";
			$x .= "<input type =\"checkbox\" name=\"oea_id[]\" value=\"{$_recordID}\" checked>";
			$x .= "</td>";
		
			$x .= "</tr>";
		}
		$x .= "</table>";
		
		return $x;
	}
	
	function Get_Admin_Edit_OEA_Settings_UI()
	{
		global $linterface, $Lang;
		
		$h_ImportBtn = $linterface->Get_Content_Tool_v30("import","javascript:js_Import();");
		//$h_ExportBtn = $linterface->Get_Content_Tool_v30("export","javascript:js_Export();");
		$h_ExportBtn = '<a class="export" href="/templates/get_sample_csv.php?file=slp_oea_mapping_export_unicode.csv&amp;path=/home/web/eclass40/intranetIP25/home/portfolio/teacher/management/oea/csv_sample/">'.$Lang['Btn']['Export'].'</a>';
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								$x .= $h_ImportBtn."\n";
								$x .= $h_ExportBtn."\n";
							$x .= '</div>'."\n";
						$x .= '</div>'."\n";	
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$this->Get_Admin_Edit_OEA_Settings_Table().'</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";
		
		return $x;
	}
	
	function Get_Admin_Edit_OEA_Settings_Table()
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x = '';
		$x .= '<table id="TopicInfoTable" class="common_table_list_v30" align="center">';
			$x .= '<col align="left" style="width:50%;">'."\n";
			$x .= '<col align="left" style="width:50%;">'."\n";
			
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th>'.$Lang['iPortfolio']['OEA']['SLPProgramName'].'</th>';
					$x .= '<th>'.$Lang['iPortfolio']['OEA']['OEAProgramName'].'</th>';
				$x .= '</tr>';
			$x .= '</thead>';
			
			$x .= '<tbody>';
				$x .= '<tr>';
					$x .= '<td>Football Club</td>';
					$x .= '<td><input class="textboxtext OEA_Name" value="" /></td>';
				$x .= '</tr>';
				$x .= '<tr>';
					$x .= '<td>Basketball Club</td>';
					$x .= '<td>'."\n";
						$x .= '<div onmouseover="Show_jEditable_Edit_Background(this, \''.$PATH_WRT_ROOT.'\', \''.$LAYOUT_SKIN.'\');" 
									onmouseout="Hide_jEditable_Edit_Background(this, \''.$PATH_WRT_ROOT.'\', \''.$LAYOUT_SKIN.'\');" class="edit">';
							$x .= 'Basketball Team';
						$x .= '</div>';
					$x .= '</td>';
				$x .= '</tr>';
			$x .= '</tbody>';
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_iPortfolio_Not_Activated_Remarks()
	{
		global $Lang;
		$x = "
			<span style=\"background-color:#F0F0F0;\">&nbsp;&nbsp;&nbsp;</span>
			<span class=\"tabletextremark\">
				".$Lang['iPortfolio']['NotActivatedPortfolioStudent']."
			</span>
			<p class=\"spacer\"></p>
		";
		
		return $x;
	}
	
	function Get_Category_Checkboxes_Table($IdPrefix='')
	{
		global $Lang, $linterface;
		
		$libCategory = new Category();
		
		$IdPrefix = str_replace(' ', '_', $IdPrefix);
		$ChkboxName = $IdPrefix.'CategoryIDArr[]';
		$ChkboxClass = $IdPrefix.'CategoryChk';
		
		$ChkboxID_Global = $IdPrefix.'CategoryChk_Global';
		
		$CategoryInfoArr = $libCategory->GET_CATEGORY_LIST($ParShowInactive=false);
		$numOfCategory = count($CategoryInfoArr);
		
		$numOfColumn = 2;
		$x = '';
		$x .= '<table>'."\n";
			// Select All
			$x .= '<tr>'."\n";
				$x .= '<td style="border:0px;padding:1px;" nowrap>'."\n";
					$x .= '<input type="checkbox" id="'.$ChkboxID_Global.'" onclick="Check_All_Options_By_Class(\''.$ChkboxClass.'\', this.checked);" checked /><label for="'.$ChkboxID_Global.'"> '.$Lang['General']['All'].'</label>'."\n";
				$x .= '</td>'."\n";
				$x .= '<td style="border:0px;padding:1px;" colspan="'.($numOfColumn - 1).'">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
			
			// Loop each Category
			for ($i=0; $i<$numOfCategory; $i++)
			{
				$thisCategoryID = $CategoryInfoArr[$i]['RecordID'];
				$thisCategoryName = Get_Lang_Selection($CategoryInfoArr[$i]['ChiTitle'], $CategoryInfoArr[$i]['EngTitle']);
				$thisChkboxID = $IdPrefix.'CategoryChk_'.$thisCategoryID;
				$thisOnClick = "Uncheck_SelectAll('".$ChkboxID_Global."', this.checked)";
				
				if ($i % $numOfColumn == 0) {
					$x .= '<tr>'."\n";
						$x .= '<td style="border:0px;padding:1px;">&nbsp;</td>'."\n";
				}
							
					$x .= '<td style="border:0px;padding:1px;">'."\n";
						$x .= $linterface->Get_Checkbox($thisChkboxID, $ChkboxName, $thisCategoryID, $isChecked=1, $ChkboxClass, $thisCategoryName, $thisOnClick);
					$x .= '</td>'."\n";
				
				// Last in the row or the Last Category => close the row
				if (($i + 1) % $numOfColumn == 0 || ($i == $numOfCategory - 1)) {
					$x .= '</tr>'."\n";
				}
			}
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_ELE_Checkboxes_Table($IdPrefix='')
	{
		global $ipf_cfg, $Lang, $linterface;
		
		$lpf_ole = new libpf_ole();
		$ELEInfoArr = $lpf_ole->Get_ELE_Info(array($ipf_cfg["OLE_ELE_RecordStatus"]["Public"], $ipf_cfg["OLE_ELE_RecordStatus"]["DefaultAndPublic"]));
		$numOfELE = count($ELEInfoArr);
		
		$IdPrefix = str_replace(' ', '_', $IdPrefix);
		$ChkboxName = $IdPrefix.'ELEIDArr[]';
		$ChkboxClass = $IdPrefix.'ELEChk';
		$ChkboxID_Global = $IdPrefix.'ELEChk_Global';
		
		$numOfColumn = 2;
		$x = '';
		$x .= '<table>'."\n";
			// Select All
			$x .= '<tr>'."\n";
				$x .= '<td style="border:0px;padding:1px;" nowrap>'."\n";
					$x .= '<input type="checkbox" id="'.$ChkboxID_Global.'" onclick="Check_All_Options_By_Class(\''.$ChkboxClass.'\', this.checked);" checked /><label for="'.$ChkboxID_Global.'"> '.$Lang['General']['All'].'</label>'."\n";
				$x .= '</td>'."\n";
				$x .= '<td style="border:0px;padding:1px;" colspan="'.($numOfColumn - 1).'">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
			
			// Loop each ELE
			for ($i=0; $i<$numOfELE; $i++)
			{
				$thisELEID = $ELEInfoArr[$i]['RecordID'];
				$thisELEName = Get_Lang_Selection($ELEInfoArr[$i]['ChiTitle'], $ELEInfoArr[$i]['EngTitle']);
				$thisOnClick = "Uncheck_SelectAll('".$ChkboxID_Global."', this.checked)";
				
				$thisDefaultID = $ELEInfoArr[$i]['DefaultID'];
				$thisDefaultID = ($thisDefaultID=='')? '['.$thisELEID.']' : $thisDefaultID;
				$thisChkboxID = $IdPrefix.'ELEChk_'.$thisDefaultID;
				
				if ($i % $numOfColumn == 0) {
					$x .= '<tr>'."\n";
						$x .= '<td style="border:0px;padding:1px;">&nbsp;</td>'."\n";
				}
							
					$x .= '<td style="border:0px;padding:1px;">'."\n";
						$x .= $linterface->Get_Checkbox($thisChkboxID, $ChkboxName, $thisDefaultID, $isChecked=1, $ChkboxClass, $thisELEName, $thisOnClick);
					$x .= '</td>'."\n";
				
				// Last in the row or the Last Category => close the row
				if (($i + 1) % $numOfColumn == 0 || ($i == $numOfELE - 1)) {
					$x .= '</tr>'."\n";
				}
			}
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	public function Get_Export_Batch_Thickbox_Layer() {
		global $linterface, $Lang;
		
		$SubmitBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Go_Export_Batch();");
		$CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();");
		
		$x = '';
		$x .= '<div id="debugArea"></div>';
		
		$x .= '<div class="edit_pop_board" style="height:210px;">';
			$x .= $linterface->Get_Thickbox_Return_Message_Layer();
			$x .= '<div id="LevelSettingLayer" class="edit_pop_board_write" style="height:170px;">';
				$x .= '<div style="height:80px;">';
					$x .= '<table class="form_table_v30">'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['iPortfolio']['StudentReport']['PrintingMode'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $linterface->Get_Radio_Button('PrintingMode_All', 'PrintingMode', 'all', $isChecked=1, $Class="", $Lang['iPortfolio']['StudentReport']['PrintingMode_All'], $Onclick="", $isDisabled=0);
								$x .= '<br />'."\n";
								$x .= $linterface->Get_Radio_Button('PrintingMode_Zip', 'PrintingMode', 'zip', $isChecked=0, $Class="", $Lang['iPortfolio']['StudentReport']['PrintingMode_Zip'], $Onclick="", $isDisabled=0);
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</div>';
				$x .= '<br />'."\n";
				$x .= '<div class="edit_bottom_v30">';
					$x .= $SubmitBtn."\n";
					$x .= $CancelBtn."\n";
				$x .= '</div>';
			$x .= '</div>';
		$x .= '</div>';
		
		return $x;
	}
	
	function Get_Student_DynamicReport_DBTable(){
		
		global $PATH_WRT_ROOT, $Lang, $ec_form_word, $ck_student_dynamicReport_page_no,$ck_student_dynamicReport_page_field,$ck_student_dynamicReport_page_size ;
		global $page, $field, $order, $page_size;
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-slp.php");
		
		$libpf_student = new libpf_student();
		
		# Page Cookies
		if (isset($ck_student_dynamicReport_page_no) && $ck_student_dynamicReport_page_no != "") {
			$page = $ck_student_dynamicReport_page_no;
		}
		if (isset($ck_student_dynamicReport_page_field) && $ck_student_dynamicReport_page_field != "") {
			$field = $ck_student_dynamicReport_page_field;
		}
		
		if (isset($ck_student_dynamicReport_page_size) && $ck_student_dynamicReport_page_size != "") {
			$page_size = $ck_student_dynamicReport_page_size;
		}
		
		$field = ($field=='')? 4 : $field;
		$order = ($order=='')? 0 : $order;
		$page = ($page=='')? 1 : $page;
		$page_size = ($page_size=='')? 20: $page_size;
		
		$libdbtable = new libdbtable2007($field, $order, $page);

		### Get DBTable SQL
		$sql = $libpf_student->Get_Student_DynamicReport_DBTable_Sql($_SESSION['UserID']);
		
		$DBTable = '';
		
		$libdbtable->sql = $sql;
		$libdbtable->IsColOff = "IP25_table";
		$libdbtable->page_size = $page_size;
		$libdbtable->field_array = array("ClassName", "ClassNumber", "TemplateTitle", "JSReportTitle",  "modifieddate", "modifiedby");
		$libdbtable->column_array = array(0, 0, 0, 0, 0, 0);
		$libdbtable->wrap_array = array(0, 0, 0, 0, 0, 0);
// 		$li->fieldorder2 = " ,ClassName, ClassNumber";
		$libdbtable->fieldorder2 = " ,ClassName, ClassNumber";
		
		$pos = 0;
		$libdbtable->column_list .= "<th width='4%' class='tabletoplink'>#</td>\n";
		$libdbtable->column_list .= "<th width='10%'>".$libdbtable->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['Class'])."</td>\n";
		$libdbtable->column_list .= "<th width='10%'>".$libdbtable->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['ClassNo'])."</td>\n";
		$libdbtable->column_list .= "<th width='23%' >".$libdbtable->column_IP25($pos++, $ec_form_word['answersheet_template'])."</td>\n";
		$libdbtable->column_list .= "<th width='23%' >".$libdbtable->column_IP25($pos++, $Lang['iPortfolio']['SLP']['DynamicReport']['Report'])."</td>\n";
		$libdbtable->column_list .= "<th width='10%'>".$libdbtable->column_IP25($pos++, $Lang['iPortfolio']['SLP']['DynamicReport']['GeneratedDate'])."</td>\n";
		$libdbtable->column_list .= "<th width='10%'>".$libdbtable->column_IP25($pos++, $Lang['iPortfolio']['SLP']['DynamicReport']['GeneratedBy'])."</td>\n";
		$libdbtable->no_col = $pos + 1;
	
		$DBTable .= $libdbtable->display();
		$DBTable .= '<input type="hidden" name="pageNo" value="'.$libdbtable->pageNo.'" />';
		$DBTable .= '<input type="hidden" name="order" value="'.$libdbtable->order.'" />';
		$DBTable .= '<input type="hidden" name="field" value="'.$libdbtable->field.'" />';
		$DBTable .= '<input type="hidden" name="page_size_change" value="" />';
		$DBTable .= '<input type="hidden" name="numPerPage" value="'.$libdbtable->page_size.'" />';
		
		return $DBTable;	
	}
}

?>