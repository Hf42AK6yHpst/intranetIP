<?php

$SysMgmtRightList['SLP'][0] = array("學校檔案", "School Record");
$SysMgmtRightList['SLP']['STUDENT_ACCOUNT']['MANAGE'] = array("學生管理", "Student Management", false);
$SysMgmtRightList['SLP']['PROFILE_INFO']['UPDATE'] = array("資料更新 (eClass/WEBSAMS)", "Update info (eClass/WEBSAMS)", false);
$SysMgmtRightList['SLP']['PROFILE_INFO']['EXPORT'] = array("學生學習概覽資料匯出", "Student Learning Profile Data Export", false);
$SysMgmtRightList['SLP']['OLE']['MANAGE'] = array("其他學習紀錄管理", "Other Learning Experience Management", false);
$SysMgmtRightList['SLP']['OFFICIAL_PHOTO']['UPLOAD'] = array("學生正式相片", "Official Photo", false);



?>