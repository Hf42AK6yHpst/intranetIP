<?
# Modifying By: 

class OLETeacherRecord{
	private $oleSQL; // STRING
	private $userID; // INT
	private $eclass_db;
	private $intranet_db;
	private $oleType; // INT / EXT
	private $db;
	private $OLETempRecordTable;
	private $ipfConfig;

	public function OLETeacherRecord($userID){
		global $eclass_db;
		global $intranet_db;
		global $ipf_cfg;

		$this->db = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;

		$this->setUserID($userID);		
		$this->OLETempRecordTable = $this->intranet_db.".TempOLERecord";
		$this->ipfConfig = $ipf_cfg;
	}

	public function getOLETeacherRecordTable(){
		return $this->OLETempRecordTable;
	}

	public function createRecordTable(){

		
		$createSQL = "create temporary table ".$this->getOLETeacherRecordTable()."(
						programID int(11) COMMENT 'store OLE_PROTRAM.programid', 
						recordid int(11) COMMENT 'store OLE_STUDENT.recordid',
						UserID int(8) COMMENT 'store OLE_STUDENT.userid',
						selfCreated int(1),
						fromIndividual int(1),
						fromClass int(1),
						fromGroup int(1),
						key `programID` (`programID`),
						key `recordid` (`recordid`),
						key `UserID` (`UserID`),
						UNIQUE KEY `uKey1` (`programID`,`recordid`,`UserID`)
					)";
//error_log($createSQL."\n", 3, "/tmp/aaa.txt");
		$this->db->db_db_query($createSQL);

		$sqlAry = $this->getSQL();

		foreach ($sqlAry as $type => $sqlStr){		

			if(trim($sqlStr) != ""){
				$_insertSQL = "insert into ".$this->getOLETeacherRecordTable()." ".$sqlStr." on DUPLICATE KEY UPDATE {$type} = ".$this->ipfConfig["TEACHER_OLE_FILTER_RECORD"][$type];
				if($_insertSQL != ""){
//					error_log($_insertSQL."\n", 3, "/tmp/aaa.txt");
					$this->db->db_db_query($_insertSQL);
				}
			}

		}
		//$sql = "select * from TempOLERecord"; //<-- debug only
//		$result = $this->db->returnArray($sql); //<-- debug only
	//debug_r($result); //<-- debug only
	}
	
	private function getSQL(){
		$sql["selfCreated"] = $this->getApprovalRecordBySelfCreated();
		$sql["fromIndividual"] = $this->getApprovalRecordByIndividaulRole();

		//check the user setting , allow teacher to approve record?
		$LibPortfolio = new libpf_slp();
		$approvalSettings  = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2($this->getOLEType());		
		if($approvalSettings["Elements"]["ClassTeacher"] == 1){
			$sql["fromClass"] = $this->getApprovalRecordByClassTeacherRole();
		}		

		
		if($this->getOLEType() == "int"){			
			$sql["fromGroup"] = $this->getApprovalRecordByGroupRole();
		}		
//debug_r($sql);		
		return $sql;
		
	}
	public function setUserID($intValue){
		$this->userID = $intValue;
	}

	public function getUserID(){
		return $this->userID;
	}

	public function setOLEType($strType){	
		$this->oleType = $strType;		
	}

	public function getOLEType(){
		return strtolower($this->oleType);
	}

	private function getApprovalRecordBySelfCreated(){
		$sql = "select op.ProgramID,os.recordid,os.userid,".$this->ipfConfig["TEACHER_OLE_FILTER_RECORD"]["selfCreated"].",0,0,0 from ".$this->eclass_db.".OLE_PROGRAM op left join ".$this->eclass_db.".OLE_STUDENT os on op.ProgramID = os.ProgramID 
				where op.CreatorID = ".$this->userID;
				
		$_requireIntType = trim($this->getOLEType());
		if($_requireIntType != ""){
				$sql .= " and op.IntExt = '{$_requireIntType}'";
		}
		
		return $sql;
	}

	private function getApprovalRecordByIndividaulRole(){
		$sql = "select op.ProgramID,os.recordid,os.userid,0,".$this->ipfConfig["TEACHER_OLE_FILTER_RECORD"]["fromIndividual"].",0,0 from ".$this->eclass_db.".OLE_STUDENT os inner join ".$this->eclass_db.".OLE_PROGRAM op on op.ProgramID = os.ProgramID 
				where RequestApprovedBy = ".$this->userID;
				
		$_requireIntType = trim($this->getOLEType());
		if($_requireIntType != ""){
				$sql .= " and op.IntExt = '{$_requireIntType}'";
		}
		
		return $sql;
	}
	private function getApprovalRecordByClassTeacherRole(){
		
				
		$sql .= "select op.Programid,os.recordid,os.UserID,0,0,".$this->ipfConfig["TEACHER_OLE_FILTER_RECORD"]["fromClass"].",0 FROM {$this->eclass_db}.OLE_PROGRAM op ";
		$sql .= "INNER JOIN {$this->eclass_db}.OLE_STUDENT os ON op.ProgramID = os.ProgramID ";
		$sql .= "INNER JOIN {$this->intranet_db}.YEAR_CLASS_USER ycu ON os.UserID = ycu.UserID ";
		$sql .= "INNER JOIN {$this->intranet_db}.YEAR_CLASS_TEACHER yct ON ycu.YearClassID = yct.YearClassID ";
		$sql .= "INNER JOIN {$this->intranet_db}.YEAR_CLASS yc ON yc.YearClassID = yct.YearClassID AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." ";
		$sql .= "WHERE yct.UserID = ".$this->userID;
		
		$_requireIntType = trim($this->getOLEType());
		if($_requireIntType != ""){
				$sql .= " and op.IntExt = '{$_requireIntType}'";
		}
		
		return $sql;
	}
	private function getApprovalRecordByGroupRole(){
		$mgmt_ele_arr = array();
		$conds = "";
		$sql = "";

		$LibPortfolio = new libpf_slp();
		$lpf_mgmt_grp = new libpf_mgmt_group();
		$ec_uID = $LibPortfolio->IP_USER_ID_TO_EC_USER_ID($this->userID);
		$group_id_arr = $lpf_mgmt_grp->GET_USER_IN_GROUP($ec_uID);


		if(empty($group_id_arr) || sizeof($group_id_arr) == 0){
			//do nothing
		}else{
			//get USER's OLE component
			$mgmt_ele_arr = $lpf_mgmt_grp->getGroupComponent($group_id_arr);		
		}

		$sizeOfMgmt_ele_arr = sizeof($mgmt_ele_arr);
//error_log("totla -->".$sizeOfMgmt_ele_arr."\n", 3, "/tmp/aaa.txt");
		if(empty($group_id_arr)){
			//not in any group
			//do nothing
		}else{
			if($LibPortfolio->isOLEMasterAdmin($mgmt_ele_arr)){
				// login in is a OLE master admin , no need to restrict the viewing of OLE component 
				//do noting
			}elseif($sizeOfMgmt_ele_arr > 0){
				for($i=0, $i_max=$sizeOfMgmt_ele_arr; $i<$i_max; $i++)
				{
					$mgmt_ele = $mgmt_ele_arr[$i];				
					if($conds == "")
					{
						$conds .= " and ("; 
					}else{
						$conds .= " or ";
					}
					$conds .= " INSTR(op.ELE, '{$mgmt_ele}') > 0 ";
				}

				$conds .= ")";
			}
			else {
				// all groups have no edit rights
				$conds .= " and 0 ";
			}	

			$sql .= "SELECT op.ProgramID,os.Recordid ,os.Userid,0,0,0,".$this->ipfConfig["TEACHER_OLE_FILTER_RECORD"]["fromGroup"]." FROM {$this->eclass_db}.OLE_PROGRAM op 
						 inner join {$this->eclass_db}.OLE_STUDENT as os on op.programid = os.programid where 1
					";		

			$sql .= $conds;

			$_requireIntType = trim($this->getOLEType());
			if($_requireIntType != ""){
				$sql .= " and op.IntExt = '{$_requireIntType}'";
			}
		}
//error_log($sql."\n", 3, "/tmp/aaa.txt");
		return $sql;
	}
}
?>