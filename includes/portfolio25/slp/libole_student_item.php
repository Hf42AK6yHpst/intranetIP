<?
# modifying : 

if (!defined("LIBOLE_STUDENT_DEFINED"))         // Preprocessor directives
{
  define("LIBOLE_STUDENT_DEFINED",true);

	class libole_student_item{
		private $objDB;
		private $eclass_db;

		private $recordID;
		private $UserID;
		private $StartDate;
		private $EndDate;
		private $Title;
		private $Details;
		private $Category;
		private $CategoryTitleB5;
		private $CategoryTitleEn;
		private $Role;
		private $Hours;
		private $Achievement;
		private $Remark;
		private $IntExt;
		private $ProgramID;
		private $Mapped_OEA_ProgramCode;
		private $ProgramDetails;


		public function getUserID(){
			return $this->UserID;
		}
		public function getStartDate(){
			return $this->StartDate;
		}
		public function getEndDate(){
			return ($this->EndDate=="0000-00-00") ? "" : $this->EndDate;
		}
		
		public function getOLEDate() {
			global $i_To;
			return ($this->StartDate).(($this->getEndDate()=="") ? "" : " ".$i_To." ".$this->EndDate);	
		}

		public function getTitle(){
			return $this->Title;
		}

		public function getDetails(){
			return $this->Details;
		}

		public function getCategory(){
			return $this->Category;
		}


		private function getCategoryTitleB5(){
			return $this->CategoryTitleB5;
		}

		private function getCategoryTitleEn(){
			return $this->CategoryTitleEn;
		}

		public function getCategoryTitle($strLang){
			if(strtoupper($strLang) == "B5"){
				return $this->getCategoryTitleB5();
			}elseif(strtoupper($strLang) == "EN"){
				return $this->getCategoryTitleEn();
			}
		}

		public function getRole(){
			return $this->Role;
		}
		public function getHours(){
			return $this->Hours;
		}
		public function getAchievement(){
			return $this->Achievement;
		}

		public function getRemark(){
			return $this->Remark;
		}
		public function getIntExt(){
			return $this->IntExt;
		}
		public function getProgramID(){
			return $this->ProgramID;
		}
		public function getMapped_OEA_ProgramCode() {
			return $this->Mapped_OEA_ProgramCode;
		}
		public function getProgramDetails() {
			return $this->ProgramDetails;
		}
		

		public function libole_student_item($recordID = null){  
			global $eclass_db;
			$this->objDB = new libdb();

			//LOAD THE PROGRAM DETAILS FORM STOREAGE (EG DB)
			if($recordID != null)
			{				
				$this->recordID = intval($recordID);
				$this->loadItemFromStorage();
			}
		}
		
		private function loadItemFromStorage(){
			global $eclass_db;
			if((trim($this->recordID) == "") || ($this->recordID<0)){
				//do nothing
			}else{
				$_record = $this->recordID;
				$sql = "select 
							s.RecordID as s_RecordID,
							s.UserID  as s_UserID,
							p.StartDate as StartDate,
							p.EndDate as EndDate,         
							p.Title as Title,            
							s.Details as Details,    
							p.Category as CategoryID,
							c.ChiTitle as CategoryTitleB5,
							c.EngTitle as CategoryTitleEn,
							s.Role      as Role       , 
							s.Hours     as Hours       , 
							s.Achievement   as Achievement,
							s.Attachment    as Attachment  ,         
							s.Remark        as Remark    ,
							s.ApprovedBy    as ApprovedBy,
							s.RequestApprovedBy as RequestApprovedBy,
							s.RecordStatus  as   RecordStatus ,
							s.ProcessDate       ,
							s.InputDate         ,
							s.ModifiedDate      ,
							s.ELE               ,
							s.Organization      ,
							s.ProgramID     as     ProgramID,
							s.TeacherComment    ,
							s.IntExt        as IntExt    ,
							s.GroupID           ,
							s.EnrolType         ,
							s.SLPOrder          ,
							s.ComeFrom          ,
							s.ProgramID			,
							m.OEA_ProgramCode	,
							p.Details as ProgramDetails
						from 
							({$eclass_db}.OLE_STUDENT as s inner join 
							{$eclass_db}.OLE_PROGRAM as p on s.ProgramID = p.ProgramID)
							left join {$eclass_db}.OLE_CATEGORY as c on c.RecordID = p.Category
							LEFT OUTER JOIN {$eclass_db}.OEA_OLE_MAPPING as m ON (m.OLE_ProgramID=s.ProgramID)
						where 
							s.RecordID = {$_record}					
				";
				//echo $sql."<br/>";
				
				$resultSet = $this->objDB->returnArray($sql);
				
				if(is_array($resultSet) && (sizeof($resultSet) == 1)){
					$this->UserID					= $resultSet[0]["s_UserID"];
					$this->StartDate				= $resultSet[0]["StartDate"];
					$this->EndDate					= $resultSet[0]["EndDate"];
					$this->Title					= $resultSet[0]["Title"];
					$this->Details					= $resultSet[0]["Details"];
					$this->CategoryTitleB5			= $resultSet[0]["CategoryTitleB5"];
					$this->CategoryTitleEn			= $resultSet[0]["CategoryTitleEn"];
					$this->Role						= $resultSet[0]["Role"];
					$this->Hours					= $resultSet[0]["Hours"];
					$this->Achievement				= $resultSet[0]["Achievement"];
					$this->Remark					= $resultSet[0]["Remark"];		
					$this->IntExt					= $resultSet[0]["IntExt"];		
					$this->ProgramID				= $resultSet[0]["ProgramID"];
					$this->Mapped_OEA_ProgramCode	= $resultSet[0]["OEA_ProgramCode"];
					$this->ProgramDetails			= $resultSet[0]["ProgramDetails"];
				}
			}
		}
		
		public function get_OLE_Student_List_SQL($OleTypeArr='')
		{
			global $eclass_db, $UserID, $i_To, $ipf_cfg, $Lang;
			
			if ($OleTypeArr != '') {
				$conds_OleType = " And p.IntExt In ('".implode("','", (array)$OleTypeArr)."') ";
			}
			
			# with linkage with OEA item			
			$sql = "SELECT 
						p.title AS 'TITLE',
						IF(o.RecordID IS NULL, CONCAT('<input type =\"checkbox\" name=\"recordID[]\" id=\"recordID[]\" value=\"' , s.recordid, '\" onClick=\"countTotal()\" />'),'&nbsp;') AS 'ACTION',
						o.RecordID AS OEA_RecordID,
						s.RecordID AS OLE_Student_RecordID,
						IF(m.OEA_ProgramCode IS NOT NULL, m.OEA_ProgramCode, o.OEA_ProgramCode) as OEA_ProgramCode,
						p.StartDate as OLE_StartDate,
						IF(p.EndDate='0000-00-00','',p.EndDate) as OLE_EndDate,
						IF(m.OEA_ProgramCode IS NOT NULL, 1, 0) AS Mapped,
						CASE p.IntExt
							WHEN '".strtoupper($ipf_cfg["OLE_TYPE_STR"]["INT"])."' THEN '".$Lang['iPortfolio']['OEA']['Internal']."'
							WHEN '".strtoupper($ipf_cfg["OLE_TYPE_STR"]["EXT"])."' THEN '".$Lang['iPortfolio']['OEA']['External']."'
						END AS OLE_Type,
						If (s.SLPOrder Is Null, '".$Lang['General']['EmptySymbol']."', s.SLPOrder) as SLPOrder
					FROM 
						{$eclass_db}.OLE_STUDENT as s inner join
						{$eclass_db}.OLE_PROGRAM as p on p.programid = s.programid LEFT OUTER JOIN
						{$eclass_db}.OEA_STUDENT as o ON (o.OLE_STUDENT_RecordID=s.RecordID) LEFT OUTER JOIN
						{$eclass_db}.OEA_OLE_MAPPING m On (m.OLE_ProgramID=p.ProgramID)
					WHERE 
						s.UserID = '{$UserID}' 
						AND (s.RecordStatus=".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]." or s.RecordStatus=".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"].") 
						AND s.RecordID NOT IN (SELECT OLE_STUDENT_RecordID FROM {$eclass_db}.OEA_STUDENT WHERE StudentID='$UserID')
						$conds_OleType
					ORDER BY
						p.Title, OLE_StartDate";	
						//debug_pr($this->returnArray($sql));
						//echo $sql;
			return $sql;
		}		
	}
}
?>
