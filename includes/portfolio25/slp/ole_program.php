<?
# Modifying By: 

/** [Modification Log] 
 * *******************************************
 * 2018-03-14 Anna
 *  - Added inputBy
 * 
 * 2016-05-23 Henry HM [2016-0415-1449-22214]
 * - Added field of "Default Hours"
 * 
 * 2015-01-12 Bill
 * - Added setSASCategory(), getSASCategory(), setSASPoint(), getSASPoint() for Ng Wah SAS Cust
 * - Modifed new_program(), update_program()
 * 2010-07-06 Max (201007061014)
 * - Modified update_program and new_program to accept no value for enddate
 * - Modified pack_value to have default value for type 
 * *******************************************
 */
class ole_program{
	private $objDB;
	private $eclass_db;
	private $intranet_db;

	private $ProgramID;	
	private $ProgramType;
	private $Title;
	private $TitleChi;
	private $StartDate;
	private $EndDate;
	private $Category;
	private $catChiTitle;
	private $catEngTitle;
	private $SubCategoryID;
	private $subCatChiTitle;
	private $subCatEngTitle;
	private $Organization;
	private $Details;
	private $DetailsChi;
	private $SchoolRemarks;
	private $ELE;
	private $InputDate;
	private $InputBy;
	private $ModifiedDate;
	private $ModifyBy;
	private $CreatorID;
	private $CreatorType;
	private $IntExt;
	private $AcademicYearID;
	private $YearTermID;
	private $Period;
	private $CanJoin;
	private $CanJoinStartDate;
	private $CanJoinEndDate;
	private $AutoApprove;
	private $JoinableYear;
	private $ComeFrom;
	private $MaximumHours;
	private $DefaultHours;
	private $CompulsoryFields;
	private $DefaultApprover;
	private $eleName;
	private $teacherInCharge;
	private $IsSAS;
	private $IsOutsideSchool;
	// Added: 2015-01-12
	private $SASCategory;
	private $SASPoint;

	public function ole_program($programId = null){  
		global $eclass_db,$intranet_db;
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		//LOAD THE PROGRAM DETAILS FORM STOREAGE (EG DB)
		if($programId != null)
		{
			$this->ProgramID = intval($programId);
			$this->loadProgramFromStorage();
		}
	}

	public function setProgramID($value){
		$this->ProgramID = intval($value);
	}	
	public function getProgramID(){
		return $this->ProgramID;
	}	

	public function setProgramType($value){
		$this->ProgramType = $value;
	}
	public function getProgramType(){
		return $this->ProgramType;
	}

	public function setTitle($value){
		$this->Title = $value;
	}
	public function getTitle(){
		return $this->Title;
	}

	public function setTitleChi($value){
		$this->TitleChi = $value;
	}
	public function getTitleChi(){
		return $this->TitleChi;
	}

	public function setStartDate($value){
		$this->StartDate= $value;
	}
	public function getStartDate(){
		return $this->StartDate;
	}

	public function setEndDate($value){
		$this->EndDate= $value;
	}
	public function getEndDate(){
		return $this->EndDate;
	}
	
	public function setCategory($value){
		$this->Category = $value;
	}
	public function getCategory(){
		return $this->Category;
	}

	public function setSubCategoryID($value){
		$this->SubCategoryID= $value;
	}
	public function getSubCategoryID(){
		return $this->SubCategoryID;
	}

	public function setOrganization($value){
		$this->Organization = $value;
	}
	public function getOrganization(){
		return $this->Organization;
	}

	public function setDetails($value){
		$this->Details = $value;
	}
	public function getDetails(){
		return $this->Details;
	}

	public function setDetailsChi($value){
		$this->DetailsChi= $value;
	}
	public function getDetailsChi(){
		return $this->DetailsChi;
	}
	public function setSchoolRemarks($value){
		$this->SchoolRemarks= $value;
	}	
	public function getSchoolRemarks(){
		return $this->SchoolRemarks;
	}

	public function setELE($value){
		$this->ELE = $value;
	}
	public function getELE(){
		return $this->ELE;
	}

	public function setInputDate($value){
		$this->InputDate= $value;
	}
	public function getInputDate(){
		return $this->InputDate;
	}
	
	public function setInputBy($value){
	    $this->InputBy = $value;
	}
	public function getInputBy(){
	    return $this->InputBy;
	}

	public function setModifiedDate($value){
		$this->ModifiedDate = $value;
	}
	public function getModifiedDate(){
		return $this->ModifiedDate;
	}
	
	public function setModifyBy($value){
		$this->ModifyBy = $value;
	}
	public function getModifyBy(){
		return $this->ModifyBy;
	}

	public function setCreatorID($value){
		$this->CreatorID = $value;
	}
	public function getCreatorID(){
		return $this->CreatorID;
	}

	public function setCreatorType($value){
		$this->CreatorType= $value;
	}
	public function getCreatorType(){
		return $this->CreatorType;
	}

	public function setIntExt($value){
		$this->IntExt = $value;
	}
	public function getIntExt(){
		return $this->IntExt;
	}

	public function setAcademicYearID($value){
		$this->AcademicYearID= $value;
	}
	public function getAcademicYearID(){
		return $this->AcademicYearID;
	}

	public function setYearTermID($value){
		$this->YearTermID = $value;
	}
	public function getYearTermID(){
		return $this->YearTermID;
	}
	public function getPeriod(){
		return $this->Period;
	}

	public function setCanJoin($value){
		$this->CanJoin = $value;
	}
	public function getCanJoin(){
		return $this->CanJoin;
	}

	public function setCanJoinStartDate($value){
		$this->CanJoinStartDate= $value;
	}
	public function getCanJoinStartDate(){
		return $this->CanJoinStartDate;
	}

	public function setCanJoinEndDate($value){
		$this->CanJoinEndDate= $value;
	}
	public function getCanJoinEndDate(){
		return $this->CanJoinEndDate;
	}

	public function setAutoApprove($value){
		$this->AutoApprove= $value;
	}
	public function getAutoApprove(){
		return $this->AutoApprove;
	}
	
	public function setJoinableYear($value){
		$this->JoinableYear= $value;
	}
	public function getJoinableYear(){
		return $this->JoinableYear;
	}

	public function setComeFrom($value)
	{
		$this->ComeFrom = $value;
	}

	public function getComeFrom()
	{
		return $this->ComeFrom;
	}
	public function setMaximumHours($value)
	{
		$this->MaximumHours = $value;
	}

	public function getMaximumHours()
	{
		return $this->MaximumHours;
	}
	public function setDefaultHours($value)
	{
		$this->DefaultHours = $value;
	}
	public function getDefaultHours()
	{
		return $this->DefaultHours;
	}
	
	public function setCompulsoryFields($value)
	{
		$this->CompulsoryFields = $value;
	}

	public function getCompulsoryFields()
	{
		return $this->CompulsoryFields;
	}
	
	public function setDefaultApprover($value)
	{
		$this->DefaultApprover = $value;
	}
	
	public function getDefaultApprover()
	{
		return $this->DefaultApprover;
	}
	public function setTeacherInCharge($intVal){
		$this->teacherInCharge = $intVal;
	}
	public function getTeacherInCharge(){
		return $this->teacherInCharge;
	}
	public function setIsSAS($intVal){
		$this->IsSAS = $intVal;
	}
	public function getIsSAS(){
		return $this->IsSAS;
	}
	public function setIsOutsideSchool($intVal){
		$this->IsOutsideSchool = $intVal;
	}
	public function getIsOutsideSchool(){
		return $this->IsOutsideSchool;
	}
	// Added: 2015-01-12
	public function setSASCategory($intVal){
		$this->SASCategory = $intVal;
	}
	public function getSASCategory(){
		return $this->SASCategory;
	}
	public function setSASPoint($intVal){
		$this->SASPoint = $intVal;
	}
	public function getSASPoint(){
		return $this->SASPoint;
	}
	public function getTeacherInChargeName(){
		$objDB = new libdb();
		$teacherName = '';
		if(intval($this->teacherInCharge) > 0){
			$sql = 'select '.getNameFieldByLang("i.").' AS teacherInCharge  from '.$this->intranet_db.'.INTRANET_USER as i where i.userid = '.$this->teacherInCharge;
			//since the sql map with userid , suppose return a unique record
			$resultSet = current($objDB->returnResultSet($sql));
			$teacherName = $resultSet['teacherInCharge'];
		}
		return $teacherName;
	}
	private function loadProgramFromStorage()
	{	
		$programId = $this->ProgramID;

		if(trim($programId) == "" || intval($programId < 0))
		{
			//FOR THE PROGRAM ID IS NOT VALIDE VALUE
			$result = null;
		}
		else
		{
			$sql = "select 
						ProgramID,
						ProgramType,
						Title,
						TitleChi,
						StartDate,
						EndDate,
						Category,
						SubCategoryID,
						Organization,
						Details,
						DetailsChi,
						SchoolRemarks,
						ELE,
						op.InputDate,
						op.ModifiedDate,
						op.ModifyBy as 'ModifyBy',
						CreatorID,
						CreatorType,
						IntExt,
						op.AcademicYearID as 'AcademicYearID',
						op.YearTermID as 'YearTermID',
						IF(AY.AcademicYearID IS NULL, '--', IF(AYT.YearTermID IS NULL, ".Get_Lang_Selection("AY.YearNameB5","AY.YearNameEN").", CONCAT(".Get_Lang_Selection("AY.YearNameB5","AY.YearNameEN").", ',&nbsp;', ".Get_Lang_Selection("AYT.YearTermNameB5", "AYT.YearTermNameEN")."))) AS Period,
						CanJoin,
						CanJoinStartDate,
						CanJoinEndDate,
						AutoApprove,
						JoinableYear,
						ComeFrom,
						MaximumHours,
						DefaultHours,
						CompulsoryFields,
						DefaultApprover,
						TeacherInCharge,
						c.ChiTitle as 'catChiTitle',
						c.EngTitle as 'catEngTitle',
						s_cat.ChiTitle as 'subCatChiTitle',
						s_cat.EngTitle as 'subCatEngTitle'
					from 				
						".$this->eclass_db.".OLE_PROGRAM as op
						LEFT JOIN ".$this->eclass_db.".OLE_CATEGORY AS c ON c.RecordID = op.Category
						left join ".$this->eclass_db.".OLE_SUBCATEGORY AS s_cat ON s_cat.SubCatID = op.SubCategoryID
						LEFT JOIN ".$this->intranet_db.".ACADEMIC_YEAR AS AY ON op.AcademicYearID = AY.AcademicYearID
						LEFT JOIN ".$this->intranet_db.".ACADEMIC_YEAR_TERM AS AYT ON op.YearTermID = AYT.YearTermID
					where 
						ProgramID = ".$programId;
//error_log($sql."\n", 3, "/tmp/aaa.txt");
			$rsTmp = $this->objDB->returnArray($sql);

			if(is_array($rsTmp) && sizeof($rsTmp)> 0)
			{
				$result = $rs;

				$this->ProgramType		=	$rsTmp[0]["ProgramType"];
				$this->Title			=	$rsTmp[0]["Title"];
				$this->TitleChi			=	$rsTmp[0]["TitleChi"];
				$this->StartDate		=	$rsTmp[0]["StartDate"];
				$this->EndDate			=	$rsTmp[0]["EndDate"];
				$this->Category			=	$rsTmp[0]["Category"];
				$this->SubCategoryID	=	$rsTmp[0]["SubCategoryID"];
				$this->Organization		=	$rsTmp[0]["Organization"];
				$this->Details			=	$rsTmp[0]["Details"];
				$this->DetailsChi		=	$rsTmp[0]["DetailsChi"];
				$this->SchoolRemarks	=	$rsTmp[0]["SchoolRemarks"];	
				$this->ELE				=	$rsTmp[0]["ELE"];
				$this->InputDate		=	$rsTmp[0]["InputDate"];	
				$this->ModifiedDate		=	$rsTmp[0]["ModifiedDate"];
				$this->ModifyBy		=	$rsTmp[0]["ModifyBy"];
				$this->CreatorID		=	$rsTmp[0]["CreatorID"];
				$this->CreatorType		=	$rsTmp[0]["CreatorType"];
				$this->IntExt			=	$rsTmp[0]["IntExt"];
				$this->AcademicYearID	=	$rsTmp[0]["AcademicYearID"];
				$this->YearTermID		=	$rsTmp[0]["YearTermID"];
				$this->Period           =   $rsTmp[0]["Period"];
				$this->CanJoin			=	$rsTmp[0]["CanJoin"];
				$this->CanJoinStartDate	=	$rsTmp[0]["CanJoinStartDate"];
				$this->CanJoinEndDate	=	$rsTmp[0]["CanJoinEndDate"];
				$this->AutoApprove		=	$rsTmp[0]["AutoApprove"];
				$this->JoinableYear		=	$rsTmp[0]["JoinableYear"];
				$this->ComeFrom			=   $rsTmp[0]["ComeFrom"];
				$this->MaximumHours		=   $rsTmp[0]["MaximumHours"];
				$this->DefaultHours		=   $rsTmp[0]["DefaultHours"];
				$this->CompulsoryFields	=   $rsTmp[0]["CompulsoryFields"];
				$this->catChiTitle		=   $rsTmp[0]["catChiTitle"];
				$this->catEngTitle		=   $rsTmp[0]["catEngTitle"];
				$this->subCatChiTitle	=   $rsTmp[0]["subCatChiTitle"];
				$this->subCatEngTitle	=   $rsTmp[0]["subCatEngTitle"];
				$this->teacherInCharge  =   
				$this->DefaultApprover =   $rsTmp[0]["DefaultApprover"];

				$this->setEleDetails();
			}
		}
		return $result;
	}
	
	public function getCatChiTitle(){
		return $this->catChiTitle;
	}

	public function getCatEngTitle(){
		return $this->catEngTitle;
	}

	public function getSubCatChiTitle(){
		return $this->subCatChiTitle;
	}

	public function getSubCatEngTitle(){
		return $this->subCatEngTitle;
	}

	private function setEleDetails(){
		global $intranet_root;
		$eleName = array();
	

		if(trim($this->ELE) != ""){
			include_once($intranet_root."/includes/libpf-slp.php");
			$_lpf = new libpf_slp();
			$DefaultELEArray = $_lpf->GET_ELE();

			$ELEArr = explode(",", $this->ELE);
			for($i=0; $i<sizeof($ELEArr); $i++){
				$t_ele = trim($ELEArr[$i]);

				$eleName[] = $DefaultELEArray[$t_ele];

			}
		}
		$this->eleName = $eleName;
	}

	public function getEleDetails(){
		return $this->eleName;
	}

	public function SaveProgram()
	{
		if(($this->ProgramID != "") && intval($this->ProgramID) > 0)
		{
			$resultId = $this->update_program();
		}
		else
		{	
			$resultId = $this->new_program();
		}
		return $resultId;
	}
	private function new_program()
	{
		
		$dbValue = array();
		$dbValue["ProgramType"]		= $this->pack_value($this->ProgramType,"str");
		$dbValue["Title"]			= $this->pack_value($this->Title,"str");
		$dbValue["TitleChi"]		= $this->pack_value($this->TitleChi,"str");
		$dbValue["StartDate"]		= $this->pack_value($this->StartDate,"date");
		$dbValue["EndDate"]			= $this->pack_value($this->EndDate);	// Max: EndDate accept no value
		$dbValue["Category"]		= $this->pack_value($this->Category,"str");
		$dbValue["SubCategoryID"]	= $this->pack_value($this->SubCategoryID,"int");
		$dbValue["Organization"]	= $this->pack_value($this->Organization,"str");
		$dbValue["Details"]			= $this->pack_value($this->Details,"str");
		$dbValue["DetailsChi"]		= $this->pack_value($this->DetailsChi,"str");
		$dbValue["SchoolRemarks"]	= $this->pack_value($this->SchoolRemarks,"str");
		$dbValue["ELE"]				= $this->pack_value($this->ELE,"str");
		$dbValue["InputDate"]		= $this->pack_value($this->InputDate,"date");
		$dbValue["ModifiedDate"]	= $this->pack_value($this->ModifiedDate,"date");
		$dbValue["ModifyBy"]		= $this->pack_value($this->ModifyBy,"int");
		$dbValue["CreatorID"]		= $this->pack_value($this->CreatorID,"int");
		$dbValue["CreatorType"]		= $this->pack_value($this->CreatorType,"str");
		$dbValue["IntExt"]			= $this->pack_value($this->IntExt,"str");
		$dbValue["AcademicYearID"]	= $this->pack_value($this->AcademicYearID,"int");
		$dbValue["YearTermID"]		= $this->pack_value($this->YearTermID,"int");
		$dbValue["CanJoin"]			= $this->pack_value($this->CanJoin,"int");
		$dbValue["CanJoinStartDate"] = $this->pack_value($this->CanJoinStartDate,"date");
		$dbValue["CanJoinEndDate"]	= $this->pack_value($this->CanJoinEndDate,"date");
		$dbValue["AutoApprove"]		= $this->pack_value($this->AutoApprove,"str");
		$dbValue["JoinableYear"]		= $this->pack_value($this->JoinableYear,"str");
		$dbValue["ComeFrom"]		= $this->pack_value($this->ComeFrom,"int");
		$dbValue["MaximumHours"]		= $this->pack_value($this->MaximumHours,"int");
		$dbValue["DefaultHours"]		= $this->pack_value($this->DefaultHours,"int");
		$dbValue["CompulsoryFields"]		= $this->pack_value($this->CompulsoryFields,"str");
		$dbValue["DefaultApprover"]		= $this->pack_value($this->DefaultApprover,"int");
		$dbValue["TeacherInCharge"]		= $this->pack_value($this->teacherInCharge,"int");
		$dbValue["IsSAS"]			= $this->pack_value($this->IsSAS,"int");
		$dbValue["IsOutsideSchool"]		= $this->pack_value($this->IsOutsideSchool,"int");
		// Added: 2015-01-12
		$dbValue["SASCategory"]		= $this->pack_value($this->SASCategory,"int");
		$dbValue["SASPoint"]		= $this->pack_value($this->SASPoint,"int");

		$dbField = "";
		$saveValue = "";

		foreach ($dbValue as $fieldName => $data)
		{
			$dbField .= $fieldName.",";
			$saveValue .= $data.",";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$dbField = substr($dbField,0,-1);
		$saveValue = substr($saveValue,0,-1);

		$sql = "insert into ".$this->eclass_db.".OLE_PROGRAM(".$dbField.")	values (".$saveValue.")";

		$this->objDB->db_db_query($sql);
		$RecordID = $this->objDB->db_insert_id();
		
		$this->ProgramID = $RecordID;
		
		return $RecordID;
	}
	
	private function update_program()
	{


		if(trim($this->ProgramID) == "" || intval($this->ProgramID) < 0 )
		{
			//for the program id is not valide value
			return null;
		}

		$dbValue = array();
		$dbValue["ProgramType"]		= $this->pack_value($this->ProgramType,"str");
		$dbValue["Title"]			= $this->pack_value($this->Title,"str");
		$dbValue["TitleChi"]		= $this->pack_value($this->TitleChi,"str");
		$dbValue["StartDate"]		= $this->pack_value($this->StartDate,"date");
		$dbValue["EndDate"]			= $this->pack_value($this->EndDate);	// Max: EndDate accept no value
		$dbValue["Category"]		= $this->pack_value($this->Category,"str");
		$dbValue["SubCategoryID"]	= $this->pack_value($this->SubCategoryID,"int");
		$dbValue["Organization"]	= $this->pack_value($this->Organization,"str");
		$dbValue["Details"]			= $this->pack_value($this->Details,"str");
		$dbValue["DetailsChi"]		= $this->pack_value($this->DetailsChi,"str");
		$dbValue["SchoolRemarks"]	= $this->pack_value($this->SchoolRemarks,"str");
		$dbValue["ELE"]				= $this->pack_value($this->ELE,"str");
		$dbValue["ModifiedDate"]	= "now()";
		$dbValue["ModifyBy"]		= $this->pack_value($this->ModifyBy,"int");
		$dbValue["CreatorID"]		= $this->pack_value($this->CreatorID,"int");
		$dbValue["CreatorType"]		= $this->pack_value($this->CreatorType,"str");
		$dbValue["IntExt"]			= $this->pack_value($this->IntExt,"str");
		$dbValue["AcademicYearID"]	= $this->pack_value($this->AcademicYearID,"int");
		$dbValue["YearTermID"]		= $this->pack_value($this->YearTermID,"int");
		$dbValue["CanJoin"]			= $this->pack_value($this->CanJoin,"int");
		$dbValue["CanJoinStartDate"] = $this->pack_value($this->CanJoinStartDate,"date");
		$dbValue["CanJoinEndDate"]	= $this->pack_value($this->CanJoinEndDate,"date");
		$dbValue["AutoApprove"]		= $this->pack_value($this->AutoApprove,"str");
		$dbValue["JoinableYear"]		= $this->pack_value($this->JoinableYear,"str");
		$dbValue["ComeFrom"]		= $this->pack_value($this->ComeFrom,"int");
		$dbValue["MaximumHours"]		= $this->pack_value($this->MaximumHours,"int");
		$dbValue["DefaultHours"]		= $this->pack_value($this->DefaultHours,"int");
		$dbValue["CompulsoryFields"]		= $this->pack_value($this->CompulsoryFields,"str");
		$dbValue["DefaultApprover"]		= $this->pack_value($this->DefaultApprover,"int");
		$dbValue["TeacherInCharge"]		= $this->pack_value($this->teacherInCharge,"int");
		$dbValue["IsSAS"]		= $this->pack_value($this->IsSAS,"int");
		$dbValue["IsOutsideSchool"]		= $this->pack_value($this->IsOutsideSchool,"int");
		// Added: 2015-01-12
		$dbValue["SASCategory"]		= $this->pack_value($this->SASCategory,"int");
		$dbValue["SASPoint"]		= $this->pack_value($this->SASPoint,"int");

		$updateDetails = "";

		foreach ($dbValue as $fieldName => $data)
		{
			$updateDetails .= $fieldName."=".$data.",";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$updateDetails = substr($updateDetails,0,-1);
		$sql = "update ".$this->eclass_db.".OLE_PROGRAM set ".$updateDetails." where ProgramID = ".$this->ProgramID;
		$this->objDB->db_db_query($sql);
		return $this->ProgramID;

	}
	
	/**
	 * Merge old programs to a new program (involving OLE_STUDENT records redirect)
	 * @param INT $ParNewProgramID - ProgramID that merge to
	 * @param Array(INT) $ParOldProgramIDs - ProgramIDs that merge from
	 * @return bool/String	true if success, error message if failed
	 */
	static public function MergePrograms($ParNewProgramID,$ParOldProgramIDs) {
		
		# if this condition is false, will return false, not to merge program
		if (empty($ParNewProgramID) || !is_array($ParOldProgramIDs) || count($ParOldProgramIDs) < 1) return false;
		
		global $eclass_db;
		
		$libdb = new libdb();
		$libdb->Start_Trans();
		try {
			# get OLE_STUDENT record ids
			$sql = "SELECT RECORDID FROM $eclass_db.OLE_STUDENT WHERE PROGRAMID IN (".implode(",",$ParOldProgramIDs).")";
			$recordIDs = $libdb->returnVector($sql);
			
			# link OLE_STUDENT records to new OLE_PROGRAM
			if (count($recordIDs)>0) {			
				# re-assign new program to students
				$sql = "UPDATE $eclass_db.OLE_STUDENT SET PROGRAMID = $ParNewProgramID WHERE RECORDID IN (".implode(",",$recordIDs).")";
				$resultLinkToNew = $libdb->db_db_query($sql);
				if (!$resultLinkToNew) {
					throw new Exception("Link student to new program failed!");
				}
				
				# verify updated students records
				$sql = "SELECT DISTINCT PROGRAMID FROM $eclass_db.OLE_STUDENT WHERE  RECORDID IN (".implode(",",$recordIDs).")";
				$verifyProgramID = $libdb->returnVector($sql);
				if (count($verifyProgramID)>1 || current($verifyProgramID) != $ParNewProgramID) {
					throw new Exception("Verify linked data failed!");
				}
			}
			
			# remove old programs
			$sql = "DELETE FROM $eclass_db.OLE_PROGRAM WHERE PROGRAMID IN (".implode(",",$ParOldProgramIDs).")";
			$resultDeleteOldPrograms = $libdb->db_db_query($sql);
			if (!$resultDeleteOldPrograms) {
				throw new Exception("Delete old programs failed!");
			}
		} catch (Exception $e) {
			$libdb->RollBack_Trans();
			return $e->getMessage();
		}
		$libdb->Commit_Trans();
		return true;
	}

	function pack_value($value,$type="")
	{
		$returnValue = "";
		switch (strtolower($type))
		{
			case "int":			
				$returnValue = empty($value)? "''" : $value;
			break;

			case "date":
				$returnValue = empty($value) ? "now()" : "'".$value."'";

			break;

			default :
				$returnValue = "'".$value."'";
		}
		return $returnValue;
	}

}
?>