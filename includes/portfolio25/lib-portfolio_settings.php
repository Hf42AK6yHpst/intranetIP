<?php 
class iportfolio_settings {
	private static $objInstance; 

	private $objDb;
	private $settingValue; // store the setting value in Associative array 

	function iportfolio_settings() {

		$this->objDb = new libdb();

		$this->loadSettingFromStorage();
		$this->setDefaultValue();
	}

	public static function getInstance()
	{
		if (!self::$objInstance)
		{
			self::$objInstance = new iportfolio_settings();
		}

		return self::$objInstance;
	}  


	public function updateSetting($settingName,$settingValue){
		global $eclass_db;

		if(trim($settingName) == ''){
			//do nothing
			return;
		}

		$sql = 'INSERT INTO '.$eclass_db.'.IPORTFOLIO_SETTING
			(`SETTING_NAME`, `SETTING_VALUE`,`DATE_INPUT`,`INPUT_BY`,`DATE_MODIFIED`,`MODIFY_BY`) 
		VALUES 
			(\''.$settingName.'\',\''.$settingValue.'\',now(),\''.$_SESSION['UserID'].'\',now(),\''.$_SESSION['UserID'].'\') ON DUPLICATE KEY UPDATE 
			SETTING_VALUE = VALUES(SETTING_VALUE), DATE_MODIFIED = VALUES(DATE_MODIFIED), MODIFY_BY = VALUES(MODIFY_BY)
		';

		$success = $this->objDb->db_db_query($sql);
		$this->loadSettingFromStorage();
		
		return $success;
	}

	public function findSetting($settingName){
		return $this->settingValue[$settingName];
	}

	private function loadSettingFromStorage(){
		global $eclass_db;
		$sql= 'select * from '.$eclass_db.'.IPORTFOLIO_SETTING';

		$result = $this->objDb->returnResultSet($sql);
		if(is_array($result) && count($result) > 0){
			$settingInfo = BuildMultiKeyAssoc($result,'SETTING_NAME');
			foreach ($settingInfo as $settingName => $details){
				$this->settingValue[$settingName] = $details["SETTING_VALUE"];
			}
		}
	}
	private function setDefaultValue(){
		//empty function now
		/*
			if($this->settingValue['aaa'] == ''){
				$this->settingValue['aaa'] = 'aValue';
			}
		*/
		return;
	}
}
?>