<?php

include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/portfolio25/libpf-account.php");
include_once($intranet_root."/includes/portfolio25/iportfolio.inc.php");

class libpf_account_parent extends libpf_account {

  function __construct(){
    parent::__construct();
  }
  
  function IS_PORTFOLIO_USER(){

    $student_arr = $this->GET_PORTFOLIO_STUDENT_LIST();
  
    return (count($student_arr) > 0);
  }
  
  function GET_PORTFOLIO_STUDENT_LIST(){
    global $lpf_gDB, $eclass_db, $intranet_db;
    
    $user_id = $this->GET_CLASS_VARIABLE("user_id");
    $sql =  "
              SELECT
                ip.StudentID
              FROM
                {$eclass_db}.PORTFOLIO_STUDENT AS ps
              INNER JOIN
                {$intranet_db}.INTRANET_PARENTRELATION AS ip
              ON
                ps.UserID = ip.StudentID
              WHERE
                ip.ParentID = '".$user_id."'
            ";
    $returnArray = $lpf_gDB->returnVector($sql);

    return $returnArray;
  }
}