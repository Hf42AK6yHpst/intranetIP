<?php
/**
 * Modifying By: Max
 */
$Lang["Cust_Tmgss"]["StudentPerformanceRecord"] = "Student Performance Record";
$Lang["Cust_Tmgss"]["StudnetNonAcademicPerformanceRecord"] = "Student Non-academic Performance Record";
$Lang["Cust_Tmgss"]["Name"] = "Name";
$Lang["Cust_Tmgss"]["RegistrationNumber"] = "Registration Number";
$Lang["Cust_Tmgss"]["Class"] = "Class";
$Lang["Cust_Tmgss"]["ClassNo"] = "Class no.";
$Lang["Cust_Tmgss"]["Club"] = "Club";
$Lang["Cust_Tmgss"]["Team"] = "Team";
$Lang["Cust_Tmgss"]["Period"] = "Period";
$Lang["Cust_Tmgss"]["Post"] = "Post";
$Lang["Cust_Tmgss"]["Performance"] = "Performance";
$Lang["Cust_Tmgss"]["Activity"] = "Activity";
$Lang["Cust_Tmgss"]["Service"] = "Service";
$Lang["Cust_Tmgss"]["Organizer"] = "Organizer";
$Lang["Cust_Tmgss"]["Title"] = "Title";
$Lang["Cust_Tmgss"]["Award"] = "Award";
$Lang["Cust_Tmgss"]["Competition"] = "Name of Competition";
$Lang["Cust_Tmgss"]["Membership"] = "Members and Posts";
$Lang["Cust_Tmgss"]["ActivitiesandServices"] = "Activities and Services";
$Lang["Cust_Tmgss"]["Scholarships"] = "Prizes, Awards and Scholarships";
$Lang["Cust_Tmgss"]["Principal"] = "Principal";
$Lang["Cust_Tmgss"]["DateOfIssue"] = "Date of Issue";
$Lang["Cust_Tmgss"]["NoRecord"] = "There is no record at the moment.";
$Lang["Cust_Tmgss"]["WholeYear"] = "Whole Year";
$Lang["Cust_Tmgss"]["Date"] = "Date";
$Lang["Cust_Tmgss"]["SchoolChop"] = "School Chop";
?>