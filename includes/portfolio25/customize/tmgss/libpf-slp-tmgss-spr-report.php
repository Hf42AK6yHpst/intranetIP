<?
/**
 * [Modification Log] Modifying By:
 * ***********************************************
 * 2020-07-31 (Bill) 2020-0730-1808-55054
 * - modified getReportTemplate() - set school icon col width for display in MS Word
 * 2015-12-10 (Omas) #G90302
 * - modified initializePrincipalInfoArray() - changed current principal
 * 2011-03-11 (Ivan) 2011-0217-1612-15128
 * 	- modified function generateMembershipTable() to cancel the hard-coding for always show "Whole Year" when AcademicYearID = 1 
 * ***********************************************
 */
include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/portfolio25/libpf-common.php");
include_once("libpf-slp-tmgss-spr.php");
include("lang/lang.$intranet_session_language.php");

class libpf_slp_tmgss_spr_report
{
	private $db;
	private $studentList;
	private $academicYearId;
	private $schoolInfoArray;
	private $principalInfoArray;
	private $issueDate;
	
	public function __construct($ParStudentList, $ParAcademicYearId, $ParYearTermId=-1,$DateofIssue) {
		$this->db = new libdb();
		$this->studentList = $ParStudentList;
		$this->academicYearId = $ParAcademicYearId;
		$this->yearTermId = $ParYearTermId;
		$this->initializeSchoolInfoArray();
		$DateofIssue = strtotime($DateofIssue);
		$this->issueDate = date("d M Y",$DateofIssue);
// 		$this->issueDate = date("d-m-Y");
		$this->initializePrincipalInfoArray($ParAcademicYearId);

	}
	
	private function initializeSchoolInfoArray() {
		$this->schoolInfoArray["SchoolName"]["en"] = "Tuen Mun Government Secondary School";
		$this->schoolInfoArray["Address1"]["en"] = "393 Castle Peak Road";
		$this->schoolInfoArray["Address2"]["en"] = "Tuen Mun, Hong Kong";
		$this->schoolInfoArray["Tel"]["en"] = "Tel. 電話 ： 2458 0459";
		$this->schoolInfoArray["Fax"]["en"] = "Fax 傳真 ： 2618 3160";
		$this->schoolInfoArray["SchoolName"]["b5"] = "屯 門 官 立 中 學";
 		$this->schoolInfoArray["Address1"]["b5"] = "香 港 屯 門 青 山 公 路 393 號";
// 		$this->schoolInfoArray["Address2"]["b5"] = "&nbsp;";
// 		$this->schoolInfoArray["Tel"]["b5"] = "電　　話 : 二五七一八&#x25CB;一八";
// 		$this->schoolInfoArray["Fax"]["b5"] = "圖文傳真 : 二五七八五六九八";
		$this->schoolInfoArray["EducationBureau"]["b5"] = "教 育 局";
		$this->schoolInfoArray["EducationBureau"]["en"] = "Education Bureau";
		$this->schoolInfoArray["SchoolIcon"] = "<img src=\"http://".$_SERVER['HTTP_HOST']."/home/portfolio/customize/tmgss/tmgss.png\" alt='tmgss.png' width='76' height='88'/>";
	}
	
	private function initializePrincipalInfoArray($ParAcademicYearId) {		
		/* if( strtotime($this->issueDate) >= strtotime('03-09-2012') && strtotime($this->issueDate) <= strtotime('31-08-2015')  ){
			//agree with client if issues date > 20120903 , display this name . Although the print issues cannot input by user now 
			//case 2012-0913-1456-53147 Request to change principal name in Customized Report in iPortfolio
			//date format must be DD-MM-YYYY
			$this->principalInfoArray["PrincipalName"]["en"] = "CHAN Tsze-ying";
			$this->principalInfoArray["PrincipalName"]["b5"] = "CHAN Tsze-ying";
		}else if ( strtotime($this->issueDate) < strtotime('03-09-2012') ){
			$this->principalInfoArray["PrincipalName"]["en"] = "Ho Fung-ping, Wendy";
			$this->principalInfoArray["PrincipalName"]["b5"] = "Ho Fung-ping, Wendy";
		}
		else{
			// current principal #G90302
			$this->principalInfoArray["PrincipalName"]["en"] = "HO Yuk-yee";
			$this->principalInfoArray["PrincipalName"]["b5"] = "HO Yuk-yee";
		} */
	    $this->principalInfoArray["PrincipalName"]["en"] = "LI Wai-bing Vickie";
	    $this->principalInfoArray["PrincipalName"]["b5"] = "LI Wai-bing Vickie";
	}
	
	public function generateHtmlReport() {
		global $Lang;
		
		$stylesheet = $this->returnStyleSheet();	
		$reportTemplate = $this->getReportTemplate();
		
		$reportsArray = array();
		foreach($this->studentList as $key => $studentId) {
		    $libpf_slp_tmgss_spr = new libpf_slp_tmgss_spr($studentId, $this->academicYearId, $this->yearTermId);
			$reportsArray[] = $this->generateAReport($reportTemplate, $libpf_slp_tmgss_spr);
		}
		
		$reports = implode("<div class='pageBreakAfter'>&nbsp;</div>", $reportsArray);
		$Html = $stylesheet."<html><head><title>" . $Lang["Cust_Tmgss"]["StudentPerformanceRecord"] . "</title></head><body><div id='globalDiv'>$reports</div></body></html>";
		return $Html;
	}
	public function generateWordReport(){
	    global $Lang;
	   
	    $reportTemplate = $this->getReportTemplate();
	    $stylesheet = $this->returnStyleSheet();
	    
	    $reportsArray = array();
	    foreach($this->studentList as $key => $studentId) {
	        $libpf_slp_tmgss_spr = new libpf_slp_tmgss_spr($studentId, $this->academicYearId, $this->yearTermId);
	        $reportsArray[] = $this->generateAReport($reportTemplate, $libpf_slp_tmgss_spr);
	    }
	    
	    $reports = implode("<div style=\"page-break-before:always;\">&nbsp;</div>", $reportsArray);    
	    $report_html= $stylesheet."<html><head><title>" . $Lang["Cust_Tmgss"]["StudentPerformanceRecord"] . "</title></head><body><div id='globalDiv'>$reports</div></body></html>";
	    
	    $ContentType = "application/x-msword";
	    $filename = "Student_Performance_Record".date("ymd").".doc";
	    $tags_to_strip = Array("A", "a", "HTML", "html", "BODY", "body");
	    foreach ($tags_to_strip as $tag)
	    {
	        $report_html = preg_replace("/<\/?" . $tag . "(.|\s)*?>/", "", $report_html);
	    }
	    
	    $output = "<HTML xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\">\n";
	    $output .= "<HEAD>\n";
	    $output .= returnHtmlMETA();
	    $output .= "<style>\n";
	    $output .= "div.page_wrapper{page:page_wrapper;}\n";	    
	    $output .= "@page page_wrapper{	margin:36.0pt 36.0pt 36.0pt 36.0pt;}\n";
	    $output .= "</style>\n";
	    $output .= "</HEAD>\n";
	    $output .= "<BODY LANG=\"zh-HK\">\n";
	    $output .= "<div class=\"page_wrapper\">\n";
	    $output .= $report_html;
	    $output .= "</div>\n";
	    $output .= "</BODY>\n";
	    $output .= "</HTML>\n";
	    
	    
	    header('Pragma: public');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    
	    header('Content-type: '.$ContentType);
	    header('Content-Length: '.strlen($output));
	    
	    header('Content-Disposition: attachment; filename="'.$filename.'";');
	    
	    return $output;
	}
	
	public function returnStyleSheet(){
	    $style = '<style>

                    .pageBreakAfter {page-break-after: always;}
                    table {empty-cells:show; }

                    
                    #globalTable {width:700px; margin-left:auto; }
                    #globalTable {margin-left:auto; margin-right:auto;} /* Firefox */
                    #globalDiv {text-align:center;} /* IE */
                    
                    /* School Information */
                    #schoolInfoTable {border-collapse:separate; border-spacing:0px 0px; width:100%; text-align:center; font-size:8pt;}
                    #schoolInfoTable .chineseInfo {font-family: "PMingLiU"; }
                    #schoolInfoTable .englishInfo { font-family: Times New Roman; }
                    #schoolInfoTable .Column1 {width:28%; text-align:left; line-height:12px;}
                    #schoolInfoTable .Column3 {width:28%; line-height:12px; text-align:right;} 
                    .schoolNameField {font-size:12pt;}
                    #reportTitle { font-family: Times New Roman;}
                    
                    /* Student Information */
                    #studentInfoTable tr td {padding: 0px 8px 0px 0px; font-family:Times New Roman; font-size:12pt;}
                    #studentInfoTable {width:100%}
                    #studentInfoTable .Column {width:60%}
                    #studentInfoTable .Column2 {width:40%}
                    
                    /* Report Content */
                    .aReportContent {height: 15cm}
                    #reportContentTable {width:100%; font-family:Times New Roman; font-size:11pt; border-collapse: collapse;}
                    #reportContentTable .noRecordCell {text-align:center;}
                    #reportContentTable table tr th, #reportContentTable table tr td {padding: 0px 8px 0px 8px;}
                    #reportContentTable table caption {text-align: left;  padding: 10px 0px 5px 8px; font-family: "Times New Roman Rounded MT Bold", Times New Roman, PMingLiU;}
                    .categoryContentTable {width:100%; border-collapse: collapse;}
                    .categoryContentTable tr td, .categoryContentTable tr th {border: 1px solid black;}
                    
                    .reportName {font-weight:bold; font-size:12pt; height:3px;}
                    
                    #membershipTable .Column1 {width: 55%}
                    #membershipTable .Column3 {width: 25%}
                    #membershipTable .Column4 {width: 20%}
                    
                    #AS_by_SchoolTable .Column1 {width: 50%}
                    #AS_by_SchoolTable .Column2 {width: 50%}
                    
                    
                    #scholarshipTable .Column1 {width:50%}
                    #scholarshipTable .Column2 {width:30%}
                    #scholarshipTable .Column3 {width:20%}
                    
                    /* Principal and Date */
                    #principalAndDateTable {width:100%; table-collapse: collapse; text-align:center;}
                    table.pagefooter td{text-align:center; }
                    #principalAndDateTable .Column1 {width:4%} 
                    #principalAndDateTable .Column2 {width:20%}                 
                    #principalField {border-top: 1px solid black;}
                    #dateField {border-top: 1px solid black;}

				</style>';
	    
	    return $style;
	}
	
	private function generateAReport($ParReportTemplate, $libpf_slp_tmgss_spr) {
		$studentInfoTable = $this->generateStudentInfoTable($libpf_slp_tmgss_spr->getStudentInfo());

		$reportContentTable = $this->generateReportContentTable($libpf_slp_tmgss_spr);
// 		$reportContentTable = '';
		$Html = str_replace(array("{{{ STUDENT_INFO }}}","{{{ REPORT_CONTENT }}}"),array($studentInfoTable,$reportContentTable),$ParReportTemplate);
		return $Html;
	}

	private function generateStudentInfoTable($ParStudentInfo) {
		global $Lang, $intranet_session_language;
// 		$studentName = ($intranet_session_language == "en" ? $ParStudentInfo->EnglishName : $ParStudentInfo->ChineseName);
		$studentName = $ParStudentInfo->EnglishName.' ('.$ParStudentInfo->ChineseName.')';
		$registrationNumber = substr_replace($ParStudentInfo->WebSamsRegNo, "", 0, 1);
		$class = $ParStudentInfo->ClassName;
		$classNumber = $ParStudentInfo->ClassNumber;
		
		$studentInfoTable = <<<HTMLEND
        	<table id="studentInfoTable" >
                  <tr>
                    <td class="Column" nowrap="nowrap">
                      {$Lang["Cust_Tmgss"]["Name"]} : {$studentName}
                    </td>
                    <td class="Column" >
                      {$Lang["Cust_Tmgss"]["RegistrationNumber"]} : {$registrationNumber}
                    </td>
                  </tr>
                  </tr>  
                    <td class="Column2">
                      {$Lang["Cust_Tmgss"]["Class"]} / {$Lang["Cust_Tmgss"]["ClassNo"]} : $class($classNumber)
                    </td>
                    <td class="Column2">
                      {$Lang["Cust_Tmgss"]["Date"]} : {$this->issueDate}
                    </td>
                  </tr>
                  <tr><td><br/></td></tr>
            </table>
HTMLEND;
		return $studentInfoTable;
	}
	private function generateMembershipTable($ParData) {
		global $Lang, $intranet_session_language;
		$table = "";
		if (count($ParData)>0) {
            $table = "
                <p class='reportName'>" .$Lang["Cust_Tmgss"]["Membership"]."</p>
                <table class='categoryContentTable' id='membershipTable'>
            	   <tr>
                    <th class='Column1' style='font-size:11pt;'>
                      " . $Lang["Cust_Tmgss"]["Club"] . " / " . $Lang["Cust_Tmgss"]["Team"] . "
                    </th>
                    <th class='Column3' style='font-size:11pt;'>
                      " . $Lang["Cust_Tmgss"]["Post"] . "
                    </th>       
                  </tr>";
		
    		foreach($ParData as $key => $element) {
    		  $table .= "
                   <tr>
                    <td style='font-size:11pt;'>".$element["ACTIVITYNAME"]."</td>
                    <td style='font-size:11pt;'>".$element["ROLE"]."</td>
                   </tr>";
    			}
    		$table .= "</table>";
        }    	
		return $table;
	}
	private function generateAS_by_SchoolTable($ParData) {
		global $Lang,$intranet_session_language;
		$table = "";
		if (count($ParData)>0) {
		  $table = "
            <p class='reportName'>" .$Lang["Cust_Tmgss"]["ActivitiesandServices"]."</p>
            <table class='categoryContentTable' id='AS_by_SchoolTable'>
              <tr>
                <th class='Column1' style='font-size:11pt;'>
                  ".$Lang["Cust_Tmgss"]["Activity"]." / ".$Lang["Cust_Tmgss"]["Service"]."
                </th>
                <th class='Column2' style='font-size:11pt;'>
                  ".$Lang["Cust_Tmgss"]["Organizer"]."
                </th>
              </tr>";
        	   foreach($ParData as $key => $element) {
        	$table .= "
              <tr>
                <td style='font-size:11pt;'>" . $element["TITLE"] . "</td>
                <td style='font-size:11pt;'>" . $element["ORGANIZATION"] . "</td>
              </tr>";
        			}
		$table .= "</table>";
		}
		return $table;
	}
	
	private function generateScholarshipsTable($ParData) {
		global $Lang;
		$table = "";
		if (count($ParData)>0) {
		$table = "
             <p class='reportName'>" .$Lang["Cust_Tmgss"]["Scholarships"]."</p>
             <table class='categoryContentTable' id='scholarshipTable'>
                  <tr>
                    <th class='Column1' style='font-size:11pt;'>
                      " . $Lang["Cust_Tmgss"]["Title"] . "
                    </th>
                    <th class='Column2' style='font-size:11pt;'>
                      ".$Lang["Cust_Tmgss"]["Organizer"]."
                    </th>
                    <th class='Column3' style='font-size:11pt;'>
                      ".$Lang["Cust_Tmgss"]["Award"]."
                    </th>
                  </tr>";
            			foreach($ParData as $key => $element) {
            				$table .= "
                  <tr>
                    <td style='font-size:11pt;'>" . $element["TITLE"] . "</td>
                    <td style='font-size:11pt;'>" . $element["ORGANIZATION"] . "</td>
                    <td style='font-size:11pt;'>" . $element["Achievement"] . "</td>
                  </tr>";
			}
		$table .= "</table>";
		} else {
			// do nothing
		}
		return $table;
	}
	private function generateReportContentTable($libpf_slp_tmgss_spr) {
		$memberShipTable = $this->generateMembershipTable($libpf_slp_tmgss_spr->getMembershipArray());
		$AS_by_SchoolTable = $this->generateAS_by_SchoolTable($libpf_slp_tmgss_spr->getActivitiesOrServicesArray());
		$scholarshipsTable = $this->generateScholarshipsTable($libpf_slp_tmgss_spr->getScholarshipsArray());
		$reportContentTable = <<<HTMLEND
            <table id="reportContentTable">
              <tr>
                <td>
                  $memberShipTable
                </td>
              </tr>
               <tr><td><br/><br/> </td></tr>
              <tr>
                <td>
                  $AS_by_SchoolTable
                </td>
              </tr>
                  <tr><td><br/><br/> </td></tr>
              <tr>
                <td>
                  $scholarshipsTable
                </td>
              </tr>
            </table>
HTMLEND;
		return $reportContentTable;
	}
	
	private function getReportTemplate() {
		global $Lang, $intranet_session_language;

		$academicYearName = getAcademicYearInfoByAcademicYearId($this->academicYearId);

		$globalTable = <<<HTMLEND
                <table id="globalTable">
                  <tr>
                    <td>
                      <table id="schoolInfoTable">
                        <tr>
                           <td class="englishInfo Column1" style="text-align:left;" >
                            {$this->schoolInfoArray["Address1"]["en"]}
                           </td>
                            <td rowspan="6" style="text-align: central; width: 44%;" >
                            {$this->schoolInfoArray["SchoolIcon"]}
                            </td>
                            <td class="Column1"></td>
                        </tr>
                        <tr>
                             <td class="englishInfo Column1" style="text-align:left;">
                            {$this->schoolInfoArray["Address2"]["en"]}
                             </td>
                            
                            <td></td>
                        </tr>

                        <tr>
                           <td class="chineseInfo Column1" style="text-align:left;">
                            {$this->schoolInfoArray["Address1"]["b5"]}
                          </td>
                           
                          <td class="chineseInfo Column3" style="text-align:center;">
                            {$this->schoolInfoArray["EducationBureau"]["b5"]}
                          </td>
                        </tr>
            
                        <tr>
                          <td class="chineseInfo Column1" style="text-align:left;">
                             {$this->schoolInfoArray["Fax"]["en"]}
                          </td>
                          
                           <td class="englishInfo Column3" style="font-weight:bold; text-align:center;">
                           {$this->schoolInfoArray["EducationBureau"]["en"]}
                          </td>
                        </tr>
            
                        <tr>
                          <td class="chineseInfo Column1" style="text-align:left;">
                          {$this->schoolInfoArray["Tel"]["en"]}
                          </td>
                         <td>
                         </td>
                        </tr>
      
                        <tr>
                          <td></td>
                          <td></td>
                        </tr>

                        <tr>
                          <td><br></td>
                          <td></td>
                        </tr>

                        <tr>
                          <td colspan="3" id="reportTitle" class="schoolNameField" style="font-size:13pt; text-align:central;">
                            {$this->schoolInfoArray["SchoolName"]["b5"]}
                          </td>
                        </tr>

                        <tr>
                          <td colspan="3" id="reportTitle" class="schoolNameField" style="font-weight:bold; font-size:13pt;">
                            {$this->schoolInfoArray["SchoolName"]["en"]}
                          </td>
                        </tr>
                        
                        <tr>
                          <td colspan="3" id="reportTitle" class="schoolNameField" style="font-weight:bold;">
                          {$Lang["Cust_Tmgss"]["StudnetNonAcademicPerformanceRecord"]} {$academicYearName}
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {{{ STUDENT_INFO }}}
                    </td>
                  </tr>
                  <tr>
                    <td class="aReportContent" valign="top">
                      {{{ REPORT_CONTENT }}}
                    </td>
                  </tr>
                </table>
                <div style=" text-align:center;">
                <table id="globalTable">
                  <tr>
                    <td>
                      <table id="principalAndDateTable">
                        <tr height="155px">
                          <td class="Column1">
                            &nbsp;
                          </td>
                          <td class="Column2">
                            &nbsp;
                          </td>
                          <td class="Column2">
                            &nbsp;
                          </td>
                          <td class="Column2">
                            &nbsp;
                          </td>
                          <td class="Column1">
                            &nbsp;
                          </td>
                        </tr>
                        <tr valign="top">
                          <td></td>
                          <td id="principalField">
                            {$this->principalInfoArray["PrincipalName"][$intranet_session_language]}<br />
                            ({$Lang["Cust_Tmgss"]["Principal"]})
                          </td>
                          <td>
                            &nbsp;
                          </td>
                          <td id="dateField">
                            {$Lang["Cust_Tmgss"]["SchoolChop"] }
                          </td>
                           <td></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                </div>
HTMLEND;
		return $globalTable;
	}
}
?>
