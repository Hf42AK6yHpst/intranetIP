<?php
include_once($intranet_root."/includes/portfolio25/iPortfolioConfig.inc.php");
include_once($intranet_root."/includes/libuser.php");
class libpf_slp_tmgss_spr {
	private $db;
	private $studentId;
	private $academicYearId;
	private $yearTermId;
	private $studentInfo;
	private $membershipArray;
	private $activitiesOrServicesArray;
	private $scholarshipsArray;
		
// 	const ID_COMPETITIONS = 1;
// 	const ID_ACTIVITIES = 2;
// 	const ID_SERVICES = 4;
	const ID_AWARD = 5;
	
	private $MAX_LIMIT = 5;
	// [2020-0730-1808-55054]
	// const LIMIT_MEMBER = 4;
    const LIMIT_MEMBER = 5;
	private $LIMIT_SERVICES = 5;	// Limit of services is depending on the others, i.e. $MAX_LIMIT - "Other limits"
	const LIMIT_SCHOLARSHIPS = 8;
	

	public function __construct($ParStudentId, $ParAcademicYearId, $ParYearTermId=-1) {
		$this->db = new libdb();
		$this->LIMIT_SERVICES = $this->MAX_LIMIT;
		$this->studentId = $ParStudentId;
		$this->academicYearId = $ParAcademicYearId;
		$this->yearTermId = $ParYearTermId;
		$this->studentInfo = new libuser($this->studentId);
		$this->membershipArray = $this->generateMemberShipArray();
		$this->scholarshipsArray = $this->generateScholarshipsArray();
		
		# This one should be generated at last amount the result arrays since the size is determined by others
		$this->activitiesOrServicesArray = $this->generateActivitiesOrServicesArray();
	}
	
	private function groupMembershipRecordsToYears($ParOriginalResult, $ParYearTermIds) {
		global $intranet_session_language, $Lang;
//debug_r($ParOriginalResult);
		#############################################
		##	Start: Get the year term ids of an activity
		#############################################
		$membershipYearCount = array();
		foreach($ParOriginalResult as $key => $element) {
			$membershipYearCount[$element["ACTIVITYNAME"]][] = array("KEY"=>$key, "YEARTERMID"=>$element["YEARTERMID"]);
		}		
		#############################################
		##	End: Get the year term ids of an activity
		#############################################
		
		#############################################
		##	Start: Re-construct the original result
		#############################################
		$keyToSplice = array();
		foreach($membershipYearCount as $mKey => $mElement) {
			# if the number of year term ids of an activity is the same as the target years,
			# pad the information of the latter years to the 1st one 
			if (count($mElement)==count($ParYearTermIds)) {	# Handling the WHOLE YEAR membership
				$sizeOfmElement = count($mElement);
				$ParOriginalResult[$mElement[0]["KEY"]]["YEARTERMNAME".strtoupper($intranet_session_language)] = $Lang["Cust_Tmgss"]["WholeYear"];
				
				# Put the 1st role and performance into respective array for checking existance below
				$existedRole = array($ParOriginalResult[$mElement[0]["KEY"]]["ROLE"]);
				$existedPerformance = array($ParOriginalResult[$mElement[0]["KEY"]]["PERFORMANCE"]);
				for($i=1;$i<$sizeOfmElement; $i++) {
					$ParOriginalResult[$mElement[0]["KEY"]]["ROLE"] = $ParOriginalResult[$mElement[0]["KEY"]]["ROLE"].(empty($ParOriginalResult[$mElement[0]["KEY"]]["ROLE"])?"":" / ").$ParOriginalResult[$mElement[$i]["KEY"]]["ROLE"];				
					$keyToSplice[] = $mElement[$i]["KEY"];					
				}
			}
		}
		# remove the unwanted activities that with information already concatenated to the 1st one
		sort($keyToSplice);
		$keyToSplice = array_reverse($keyToSplice);
		
		foreach($keyToSplice as $kKey => $keyToKill) {
			array_splice($ParOriginalResult, $keyToKill, 1);
		}
		array_splice($ParOriginalResult, self::LIMIT_MEMBER);
		$resultArray = $ParOriginalResult;
		#############################################
		##	End: Re-construct the original result
		#############################################		
		return $resultArray;
	}
	private function generateMemberShipArray() {
		global $eclass_db, $intranet_db, $intranet_session_language;
		
	
		if ($this->yearTermId>0) {
			$cond_yearTerm = " AND AYT.YEARTERMID = {$this->yearTermId}";
		}
		$sql = "SELECT
					AY.YEARNAME" . strtoupper($intranet_session_language) . ",
					AYT.YEARTERMID,
					AYT.YEARTERMNAME" . strtoupper($intranet_session_language) . ",
					ACTS.ACTIVITYNAME,
					ACTS.ROLE
				FROM {$eclass_db}.ACTIVITY_STUDENT ACTS
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR AY
						ON ACTS.ACADEMICYEARID = AY.ACADEMICYEARID
					LEFT JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AYT
						ON AYT.YEARTERMID = ACTS.YEARTERMID
				WHERE
					ACTS.USERID = {$this->studentId}
					AND
					AY.ACADEMICYEARID = {$this->academicYearId}
					$cond_yearTerm
				ORDER BY
					ACTS.ACTIVITYNAME ASC
					";
					

		if ($this->yearTermId == '') {
		    $cond_YearTermID= " AND (iegi.Semester Is Null Or iegi.Semester = 0 Or iegi.Semester = '')";		
		}else{
		    $cond_YearTermID = "And iegi.Semester = '{$this->yearTermId}'";
		}
	
// 		iu.UserGroupID,
// 		iu.GroupID,
// 		iu.UserID,
// 		iu.RoleID,
// 		iu.RecordType,
// 		iu.RecordStatus,
// 		iu.DateInput,
// 		iu.DateModified,
// 		iu.AdminAccessRight,
// 		iu.Performance,
// 		iu.CommentStudent,
// 		iu.isActiveMember,
// 		iu.ApprovedBy,
// 		iu.OLE_STUDENT_RecordID,
// 		iu.EnrolGroupID,

		$titleName = Get_Lang_Selection("ig.TitleChinese", "ig.Title");
	
		$yearTermIds = $this->getYearTermIds();
	    $SQL = " SELECT							
							ig.Title as ClubTitle,
							ir.Title as ROLE,							
							$titleName as ACTIVITYNAME													
					FROM
							INTRANET_USERGROUP as iu
							Inner Join INTRANET_GROUP as ig ON (iu.GroupID = ig.GroupID)
							Inner Join INTRANET_ENROL_GROUPINFO as iegi On (iu.EnrolGroupID = iegi.EnrolGroupID)
							Left Outer Join INTRANET_ROLE as ir On (iu.RoleID = ir.RoleID)							
					WHERE
							ig.RecordType = 5
							AND	iu.UserID = '{$this->studentId}'
							AND	ig.AcademicYearID = '{$this->academicYearId}'							
							$cond_YearTermID	 
                    ORDER BY ig.Title limit 5 	
            
        ";
	
		$resultArray = $this->groupMembershipRecordsToYears($this->db->returnArray($SQL),$yearTermIds);
		return $resultArray;
	}
	private function generateActivitiesOrServicesArray() {
		global $eclass_db, $intranet_db, $intranet_session_language, $ipf_cfg;
		if ($this->yearTermId>0) {
			$join_yearTerm = "
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AYT
						ON AYT.YEARTERMID = OP.YEARTERMID";
			$cond_yearTerm = " AND AYT.YEARTERMID = {$this->yearTermId}";
		}
		$sql = "SELECT
					OP.TITLE,
					AY.YEARNAME" . strtoupper($intranet_session_language) . ",
					OP.STARTDATE,
					OP.ORGANIZATION
				FROM
					{$eclass_db}.OLE_STUDENT OS
					INNER JOIN {$eclass_db}.OLE_PROGRAM OP
						ON OS.PROGRAMID = OP.PROGRAMID
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR AY
						ON OP.ACADEMICYEARID = AY.ACADEMICYEARID
					$join_yearTerm
				WHERE
					OS.USERID = {$this->studentId}
					AND
					AY.ACADEMICYEARID = {$this->academicYearId}
					AND
					OP.CATEGORY NOT IN (" . self::ID_AWARD. ")
					AND 
					(
						OS.RECORDSTATUS = " . $ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"] . "
						OR
						OS.RECORDSTATUS = " . $ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"] . "
					)
					AND
					(
						OS.SLPORDER IS NOT NULL AND OS.SLPORDER <> ''
					)
					$cond_yearTerm
				ORDER BY
					OS.SLPORDER, OP.CATEGORY ASC
				LIMIT " . $this->LIMIT_SERVICES;

		return $this->db->returnArray($sql);
	}

	private function generateScholarshipsArray() {
		global $eclass_db, $intranet_db, $intranet_session_language, $ipf_cfg;
		if ($this->yearTermId>0) {
			$join_yearTerm = "
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AYT
						ON AYT.YEARTERMID = OP.YEARTERMID";
			$cond_yearTerm = " AND AYT.YEARTERMID = {$this->yearTermId}";
		}
		$sql = "SELECT
					OP.TITLE,
					OS.ORGANIZATION,
                    OS.Achievement
				FROM
					{$eclass_db}.OLE_STUDENT OS
					INNER JOIN {$eclass_db}.OLE_PROGRAM OP
						ON OS.PROGRAMID = OP.PROGRAMID
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR AY
						ON OP.ACADEMICYEARID = AY.ACADEMICYEARID
					$join_yearTerm
				WHERE
					OS.USERID = {$this->studentId}
					AND
					AY.ACADEMICYEARID = {$this->academicYearId}
					AND
					OP.CATEGORY = " . self::ID_AWARD. "
					$cond_yearTerm
				ORDER BY
					OP.TITLE ASC
				LIMIT " . self::LIMIT_SCHOLARSHIPS;

		$resultArray = $this->db->returnArray($sql);
	
		return $resultArray;
	}
	private function getYearTermIds() {
		$sql = "SELECT YEARTERMID FROM {$intranet_db}.ACADEMIC_YEAR_TERM
				WHERE ACADEMICYEARID = {$this->academicYearId}";
		return $this->db->returnArray($sql);
	}
	
	
	#########################################
	##	Accessors
	#########################################
	public function getDb() {
		return $this->db;
	}
	public function getStudentId() {
		return $this->studentId;
	}
	public function getAcademicYearId() {
		return $this->academicYearId;
	}
	public function getYearTermId() {
		return $this->yearTermId;
	}
	public function getStudentInfo() {
		return $this->studentInfo;
	}
	public function getMembershipArray() {
		return $this->membershipArray;
	}
	public function getActivitiesOrServicesArray() {
		return $this->activitiesOrServicesArray;
	}

	public function getScholarshipsArray() {
		return $this->scholarshipsArray;
	}
	#########################################
	##	Mutators
	#########################################
	public function setDb($value) {
		$this->db=$value;
	}
	public function setStudentId($value) {
		$this->studentId=$value;
	}
	public function setAcademicYearId($value) {
		$this->academicYearId=$value;
	}
	public function setYearTermId($value) {
		$this->yearTermId=$value;
	}
	public function setStudentInfo($value) {
		$this->studentInfo=$value;
	}
	public function setMembershipArray($value) {
		$this->membershipArray=$value;
	}
	public function setActivitiesOrServicesArray($value) {
		$this->activitiesOrServicesArray=$value;
	}

	public function setScholarshipsArray($value) {
		$this->scholarshipsArray=$value;
	}
	
}
?>