<?php
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	private $StdClassName;
	private $All_AcademicYearID;
	
	private $PageNumberOfThisPage;
	
	private $NumberOfFirstPage;
	private $NumberOfOtherPage;
	private $nextLeftRow;
	private $RowNoNow;
	
	private $DisplayComponent_Array;
	
	private $YearClassID;
	
	private $AcademicYearStr;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;
		
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '--';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#D0D0D0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->StdClassName = '';
		
		$this->NumberOfFirstPage = '12';
		$this->NumberOfOtherPage = '30';
		
		$this->DisplayComponent_Array = array();
		$this->DisplayComponent_Array[] = '[MCE]';
		$this->DisplayComponent_Array[] = '[AD]';
		$this->DisplayComponent_Array[] = '[CS]';		
		$this->DisplayComponent_Array[] = '[CE]';
		$this->DisplayComponent_Array[] = '[PD]';	
				
		$this->nextLeftRow = $this->NumberOfFirstPage;
		$this->RowNoNow = 0;
		
		$this->PageNumberOfThisPage = 1;
		
		$this->SchoolTitleEn = 'Yan Chai Hospital Wong Wha San Secondary School';
		$this->SchoolTitleCh = '仁濟醫院王華湘中學';
	}
	
	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	
	public function setAll_AcademicYearID($All_AcademicYearID)
	{
		$this->All_AcademicYearID = $All_AcademicYearID;
	}
	
	public function setNoOfFirstPage($NoOfFirstPage){
		$this->NumberOfFirstPage = $NoOfFirstPage;
		$this->nextLeftRow = $this->NumberOfFirstPage;
	}
	public function setNoOfOtherPage($NoOfOtherPage){
		$this->NumberOfOtherPage = $NoOfOtherPage;
	}
	public function getPageNumberOfThisPage()
	{
		return $this->PageNumberOfThisPage;
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
		
	private function getYearID($ParYearID='thisYear')
	{
		if($ParYearID=='thisYear')
		{
			$YearClassID = $this->YearClassID;
		
			$YEAR_CLASS = $this->Get_IntranetDB_Table_Name('YEAR_CLASS');
			$sql = "select 
							ClassTitleEN,
							YearID
				    from 
							$YEAR_CLASS 
					where 
							YearClassID =$YearClassID
					";
			$result = $this->objDB->returnArray($sql);
			$result = current($result);
			
			$yearID = $result['YearID'];
		}
		else
		{
			$YEAR_CLASS = $this->Get_IntranetDB_Table_Name('YEAR_CLASS');
			$sql = "select 
							YearID
				    from 
							$YEAR_CLASS 
					where 
							YearClassID =$ParYearID
					";
			$result = $this->objDB->returnArray($sql);
			$result = current($result);
			
			$yearID = $result['YearID'];
		}

		return $yearID;
	}
	

	
	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '18';

	//	$imgFile = get_website().'/includes/portfolio25/customize/ychwws/wws_logo.jpg';
		$imgFile = get_website().'/includes/portfolio25/customize/ychwws/ychwws_SchoolLogo.jpg';
		$schoolLogoImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:700px;">' : '&nbsp;';

		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:center; border:0px;">';
//			$x .= '	<tr>
//						<td rowspan="3" align="center" " style="width:18%;">'.$schoolLogoImage.'</td>
//						<td style="font-size:'.$header_font_size.'px;"><b>'.$this->SchoolTitleEn.'</b></td>
//						<td rowspan="3" align="left" style="width:18%;">&nbsp;</td>
//				   </tr>';
//			$x .= '<tr><td style="font-size:'.$header_font_size.'px;"><font face="Arial"><b>'.$this->SchoolTitleCh.'</b></font></td></tr>';
//			$x .= '<tr><td style="font-size:'.$header_font_size.'px;"><b>Student Learning Profile</b></td></tr>';

			$x .= '<tr><td align="center" style="">'.$schoolLogoImage.'</td></tr>';

		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						ChineseName,
						HKID,
						DateOfBirth,
						Gender,
						STRN,
						ClassNumber,
						WebSAMSRegNo,
						ClassName
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";

		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result)
	{		
		$fontSize = 'font-size:15px';
		
		$thisPartTitle = 'Student Particulars';
		
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$IssueDate = $this->issuedate;
		
		if (date($IssueDate)==0) {
			$DateOfLeaveYear = $this->EmptySymbol;
			$IssueDate = $this->EmptySymbol;	
		} else {
			$DateOfLeaveYear = date('Y',strtotime($IssueDate));
			$IssueDate = date('d M Y',strtotime($IssueDate));
		}
		
		$DOB = $result[0]["DateOfBirth"];
		if (date($DOB)==0) {
			$DOB = $this->EmptySymbol;
		} else {
			$DOB = date('d M Y',strtotime($DOB));
		} 
		
		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $this->EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];		
		
		if($Gender=='M')
		{
			$Gender ='Male';
		}
		else if($Gender=='F')
		{
			$Gender ='Female';
		}
		
		$Gender = ($Gender=='')? $this->EmptySymbol:$Gender;
		
		$RegistrationNo = $result[0]["WebSAMSRegNo"];		
		$RegistrationNo = str_replace('#','S',$RegistrationNo);
		if($RegistrationNo=='')
		{
			$RegistrationNo = $this->EmptySymbol;
		}
		
		$DateOfAdmisDate = substr($RegistrationNo,1,4);
		if($DateOfAdmisDate=='')
		{
			$DateOfAdmisDate = $this->EmptySymbol;
		}
		
		$ClassNumber = $result[0]["ClassNumber"];
		$ClassNumber = ($ClassNumber=='')? $this->EmptySymbol:$ClassNumber;
		
		$ClassName = $result[0]["ClassName"];
		$ClassName = ($ClassName=='')? $this->EmptySymbol:$ClassName;

		$this->StdClassName = $ClassName;
		
		$html = '';
		$html .= '<table width="90%" cellpadding="2" border="0px" align="center" style="border-collapse: collapse;">
					<col width="15%" />
					<col width="40%" />
					<col width="25%" />
					<col width="20%" />

					<tr>
							<td style="'.$fontSize.'" colspan="3"><b>'.$thisPartTitle.'</b></td>
					</tr>
					<tr>
							<td style="'.$fontSize.'"><b>Name</b></td>
							<td style="'.$fontSize.'">'.$EngName.' ('.$ChiName.')</td>
							<td style="'.$fontSize.'"><b>Student Number</b></td>
							<td style="'.$fontSize.'">'.$RegistrationNo.'</td>
					</tr>
					<tr>
							<td style="'.$fontSize.'"><b>Class</b></td>
							<td style="'.$fontSize.'">'.$ClassName.' ('.$ClassNumber.')</td>
							<td style="'.$fontSize.'"><b>Year of Admission</b></td>
							<td style="'.$fontSize.'">'.$DateOfAdmisDate.'</td>
					</tr>
					<tr>
							<td style="'.$fontSize.'"><b>Sex</b></td>
							<td style="'.$fontSize.'">'.$Gender.'</td>
							<td style="'.$fontSize.'"><b>Year of Graduation</b></td>
							<td style="'.$fontSize.'">'.$DateOfLeaveYear.'</td>
					</tr>
					<tr>
							<td style="'.$fontSize.'"><b>Date of Birth</b></td>
							<td style="'.$fontSize.'">'.$DOB.'</td>
							<td style="'.$fontSize.'"><b>Date of Issue</b></td>
							<td style="'.$fontSize.'">'.$IssueDate.'</td>
					</tr>
					';

		$html .= '</table>'; 
	
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	
/***************** Part 3 : Academic Performance******************/	
	public function getPart3_HTML(){
		
		$Part3_Title = 'Academic Performance in School';
		
		$Page_break='page-break-after:always;';
		$table_style = 'border-collapse: collapse; '; 
		
		$html = '';
		
	//	$YearID = $this->getYearID('thisYear');
		
		//if($YearID=='6')
		//{
			$html .=$this->getAcademicResultContentDisplay($Part3_Title);
		//}
		return $html;	
	}		
	
	private function getAcademicResultContentDisplay($MainTitle='')
	{
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$remark_html ='';
		$remark_html = '<table width="100%"><tr>';
		
		$ContentTable = $this->getAcademicResult_ContentTable($MainTitle);	
		
		$html='';
		$html='<table width="90%" height="" cellpadding="0" border="0px" align="center" style="border-collapse: collapse;">
					<tr>				 							
						<td style="vertical-align: top;">'.$ContentTable.'</td>						 														
					</tr>
			   </table>';
	
		return $html;
	}
	
	private function getAcademicResult_ContentTable($MainTitle='')
	{	
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$YearClassArray = $this->getStudentAcademic_YearClassNameArr(true);
	
		$Subject_string = 'Subject';
		$AcademicYearForm_string = 'Academic Year<br/>Form';
		$Mark_string = 'Mark';
		
		$StudentID = $this->uid;
		
		$lpf_report = new libpf_report();
		list($SubjectIDNameAssoArr, $StudentFormInfoArr, $StudentAcademicResultInfoArr, $SubjectInfoOrderedArr) = $lpf_report->GET_ACADEMIC_PERFORMANCE_SLP_IN_ID($StudentID);


		$NewSubjectInfoOrderedArr = array();
		$OrderSubjectIDArr = array();
		
		$OrderSubjectIDArr[] = 13;
		$OrderSubjectIDArr[] = 18;
		$OrderSubjectIDArr[] = 21;
		$OrderSubjectIDArr[] = 29;
		$OrderSubjectIDArr[] = 7;
		
		$OrderSubjectIDArr[] = 16;
		$OrderSubjectIDArr[] = 9;
		$OrderSubjectIDArr[] = 3;
		$OrderSubjectIDArr[] = 5;
		$OrderSubjectIDArr[] = 35;
		$OrderSubjectIDArr[] = 15;
		
		$OrderSubjectIDArr[] = 17;
		$OrderSubjectIDArr[] = 11;
		$OrderSubjectIDArr[] = 27;
		$OrderSubjectIDArr[] = 2;
		$OrderSubjectIDArr[] = 28;
		$OrderSubjectIDArr[] = 33;
		
		$OrderSubjectIDArr[] = 34;
		$OrderSubjectIDArr[] = 38;
		$OrderSubjectIDArr[] = 8;
		$OrderSubjectIDArr[] = 24;
		$OrderSubjectIDArr[] = 20;
		$OrderSubjectIDArr[] = 35;
		$OrderSubjectIDArr[] = 36;
		$OrderSubjectIDArr[] = 23;
		
		$OrderSubjectIDArr[] = 32;
		$OrderSubjectIDArr[] = 17;
		$OrderSubjectIDArr[] = 31;
		$OrderSubjectIDArr[] = 19;
		$OrderSubjectIDArr[] = 30;
		$OrderSubjectIDArr[] = 22;
		
	
		for($a=0,$a_MAX=count($SubjectInfoOrderedArr);$a<$a_MAX;$a++)
		{
			$thisSubjectID = $SubjectInfoOrderedArr[$a]['SubjectID'];
			
			$NewSubjectInfoOrderedArr[$thisSubjectID] = $SubjectInfoOrderedArr[$a];
		}
		
		$SubjectOrderingArray = array();
		for($i=0,$i_MAX=count($OrderSubjectIDArr);$i<$i_MAX;$i++)
		{
			$thisSubjectID = $OrderSubjectIDArr[$i];
			$SubjectOrderingArray[] = $NewSubjectInfoOrderedArr[$thisSubjectID];
		}
		
		$SubjectInfoOrderedArr = $SubjectOrderingArray;


		$lpf_academic = new libpf_academic();
		$FullMarkArr = $lpf_academic ->Get_Subject_Full_Mark($this->All_AcademicYearID);

		$countColumn=0;

		$MarkArr = array();	
	
		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{		
			$countColumn++;
		}	
		$AcademicYearArray = $this->getAcademicYearArr($this->All_AcademicYearID);
		$YearClassArray = $this->getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,'desc');

		$lpf_academic = new libpf_academic();
		$FullMarkArr = $lpf_academic ->Get_Subject_Full_Mark($this->All_AcademicYearID);

		
		for($a=0,$a_MAX=count($SubjectInfoOrderedArr);$a<$a_MAX;$a++)
		{
			$thisSubjectID = $SubjectInfoOrderedArr[$a]['SubjectID'];
			$thisSubjectName = $SubjectInfoOrderedArr[$a]['SubjectDescEN'];
		
			$IsStudy = false;
			for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
			{			
				
				$thisYear = $YearClassArray[$i]['AcademicYear'];
				$thisClassName = $YearClassArray[$i]['ClassName'];
				
				$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
				
				$thisFormNo = (int)$thisClassName;
				
				$thisMark = $StudentAcademicResultInfoArr[$StudentID][$thisSubjectID][$thisAcademicYearID]['Score'];
				$thisGrade = $StudentAcademicResultInfoArr[$StudentID][$thisSubjectID][$thisAcademicYearID]['Grade'];
				
				if ($thisMark != 'N.A.' && $thisMark != '') {
					$IsStudy = true;					
				}
				
				$thisSubjectPositionArr = $this->getSubjectPosition($thisSubjectID,$thisAcademicYearID);
				$thisFormPosition = $thisSubjectPositionArr['OrderMeritForm'];
				$thisTotalStd = $thisSubjectPositionArr['OrderMeritFormTotal'];
	
				$thisPercent = 100;
				
				$thisConditionArr = array();
				$thisConditionArr[] = $thisTotalStd!=0;
				$thisConditionArr[] = $thisFormPosition!=0;
				$thisConditionArr[] = $thisTotalStd!='';
				$thisConditionArr[] = $thisFormPosition!='';
		
				if(in_array(false,$thisConditionArr))
				{			
				}
				else
				{
					$thisPercent = ($thisFormPosition/$thisTotalStd)*100;
				}
				
				$thisStar = '';
				if($thisPercent<=10)
				{
					$thisStar = '*';
				}
		
				$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Star'] = $thisStar;
		
				$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Score'] = $thisMark;
				$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Grade'] = $thisGrade;
				
				$YearName = 'S'.$thisFormNo;
				$thisYearID = $this->getYearID($thisFormNo);
//			hdebug_r($thisYearID);
//			hdebug_r($FullMarkArr[$thisAcademicYearID]);

				$thisFullMark = $FullMarkArr[$thisAcademicYearID][$thisFormNo][$thisSubjectID]['FullMarkInt'];
				
				if ($thisMark != 'N.A.' && $thisMark != '') 
				{
					
				}
				else
				{
					$thisFullMark = $this->EmptySymbol;
				}
				
				$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['FullMark'] = $thisFullMark;
							
			}	
			
			$NewSubjectSequenceArr[$thisSubjectID]['IsStudy']=$IsStudy;
						
			$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = $thisSubjectName;
			
		}
		

		$html='';
		$html .= '<table width="100%" height="" cellpadding="2" border="0px" align="center" style="font-size:10px;border-collapse: collapse;">
					<tr> 
						<td style=" font-size:15px;text-align:left;vertical-align:text-top;"><b>
						'.$MainTitle.'
						</b></td>
				  	</tr>
				  </table>';
		
		$html .='<table width="100%"  cellpadding="2" style="border-collapse: collapse;border:1px solid #000000;">';
		
			$html .='<col width="40%"/>';
			
			if($countColumn>0)
			{
				$thisWidth = 52/$countColumn;
			}
			
			for($i=0,$i_MAX=$countColumn;$i<$i_MAX;$i++)
			{			
				$html .='<col width="'.$thisWidth.'%"/>';
			}

			$html .='<tr>'; 
				$html .='<td bgcolor="'.$this->TableTitleBgColor.'" style="font-size:11px;text-align:center;padding-left:8px;'.$_borderBottom.$_borderTop.$_borderLeft.'"><b>'.$AcademicYearForm_string.'</b></td>';
				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{
					$thisYear = $YearClassArray[$i]['AcademicYear'];
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNo = (int)$thisClassName;

					$colspan ='';

					$thisBorder = $_borderBottom.$_borderTop;

					$html .='<td '.$colspan.' bgcolor="'.$this->TableTitleBgColor.'" style="font-size:11px;text-align:center;'.$thisBorder.'"><b>'.$thisYear.'<br/>S'.$thisFormNo.'</b></td>';
				}			
			$html .='</tr>';
			$html .='<tr>';
				$html .='<td style="font-size:11px;text-align:center;padding-left:8px;'.$_borderBottom.$_borderLeft.$_borderTop.'"><b>'.$Subject_string.'</b></td>';

				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNo = (int)$thisClassName;
					
					$html .='<td style="font-size:11px;text-align:center;'.$_borderTop.$_borderBottom.'"><b>'.$Mark_string.'</b></td>';											
				}								
			$html .='</tr>';
					
								
			foreach((array)$NewSubjectSequenceArr as $thisSubjectID => $thisSubjectInfoArr)
			{		
				$_IsStudy = $NewSubjectSequenceArr[$thisSubjectID]['IsStudy'];
				$_fullMark = $NewSubjectSequenceArr[$thisSubjectID]['FullMark'];
				$_fullMark = ($_fullMark=='')? '--':$_fullMark;
				
				if($_IsStudy==false)
				{
					continue;
				}		
				$thisSubjectName = $thisSubjectInfoArr['SubjectNameEn'];
				$thisIsComponentSubject = $thisSubjectInfoArr['thisIsComponentSubject'];
			
				if($thisIsComponentSubject)
				{
					$fontSize='9px';
				}
				else
				{
					$fontSize='11px';
				}
				
				$html .='<tr>';
			
				$html .='<td style="padding-left:9px;font-size:'.$fontSize.';text-align:center;'.$_borderLeft.'">'.$thisSubjectName.'</td>';

				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{
					$thisYear = $YearClassArray[$i]['AcademicYear'];
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNo = (int)$thisClassName;				

					$thisStar = $thisSubjectInfoArr[$thisYear]['Star'] ;

					$thisMark = $thisSubjectInfoArr[$thisYear]['Score'] ;				
					if ($thisMark == 'N.A.' || $thisMark == '' || $thisMark<0) {
						$thisMark = $this->EmptySymbol;
					}
								
					$thisFullMark = $thisSubjectInfoArr[$thisYear]['FullMark'] ;
					if ($thisFullMark == '' ) {
						$thisFullMark = $this->EmptySymbol;
					}			
					
					$html .='<td style="padding-left:13px;font-size:'.$fontSize.';text-align:center;">'.$thisStar.$thisMark.' / '.$thisFullMark.'</td>';
		
				}			
				
				$html .='</tr>';								
			}		
			
			$html .='<tr>';
	
			
			if($countColumn==0)
			{
				$noRecord = 'No Record Found.';
				$html .='<td style="padding-left:9px;font-size:11px;text-align:center;'.$_borderLeft.$_borderRight.'">'.$noRecord.'</td>';
			}
			else
			{
				$html .='<td style="padding-left:9px;font-size:11px;text-align:center;'.$_borderTop.'">Average</td>';
				for($i=0,$i_MAX=($countColumn);$i<$i_MAX;$i++)
				{					
					for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
					{						
						$thisYear = $YearClassArray[$i]['AcademicYear'];
						$thisClassName = $YearClassArray[$i]['ClassName'];
						
						$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
						
						$thisFormNo = (int)$thisClassName;
						
						$thisAveragePositionArr = $this->getAveragePosition($thisAcademicYearID);
						$thisScore = $thisAveragePositionArr['Score'];
						$thisScore = ($thisScore=='')? $this->EmptySymbol : $thisScore;
						
						$thisStdFormPosition = $thisAveragePositionArr['OrderMeritForm'];
						$thisStdFormTotal = $thisAveragePositionArr['OrderMeritFormTotal'];
						
						$thisFullMark = $this->getAverageFullMark($thisAcademicYearID);
						
						$thisPercent = 100;
				
						$thisConditionArr = array();
						$thisConditionArr[] = $thisStdFormPosition!=0;
						$thisConditionArr[] = $thisStdFormTotal!=0;
						$thisConditionArr[] = $thisStdFormPosition!='';
						$thisConditionArr[] = $thisStdFormTotal!='';
				
						if(in_array(false,$thisConditionArr))
						{			
						}
						else
						{
							$thisPercent = ($thisStdFormPosition/$thisStdFormTotal)*100;
						}
						
						$thisStar = '';
						if($thisPercent<=10)
						{
							$thisStar = '*';
						}
		

						$html .='<td style="padding-left:9px;font-size:11px;text-align:center;'.$_borderTop.'">'.$thisStar.$thisScore.' / '.$thisFullMark.'</td>';
					}
				}
			}
			
			$html .='</tr>';
			
		$html .='</table>';
		
		$html .= '<table width="100%" height="1%" cellpadding="0" border="0px" align="center" style="">					
	   			 <tr>
				    <td style="text-align:left;font-size:11px;" >* Top 10%</td>
				 </tr>
			  </table>';
		
		return $html;
		
	}


	/***************** End Part 3 ******************/
	
	/***************** Part 4 ******************/	
	public function getPart4_HTML($OLEInfoArr){
		
		$Part4_Title = 'Other Learning Experiences and Achievements';
		
		$titleArray = array();
		$titleArray[] = 'Programme Name';
		$titleArray[] = 'Academic Year';
		$titleArray[] = 'Organisation';
		$titleArray[] = 'Role/Award/Performance';

		$colWidthArr = array();
		$colWidthArr[] = '<col width="35%" />';
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="32%" />';
		$colWidthArr[] = '<col width="18%" />';
		  
		$DataArr = array();

		$DataArray = $OLEInfoArr;

		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('4',$Part4_Title,$titleArray,$colWidthArr,$DataArray);
		
		return $html;
		
	}

	/***************** End Part 4 ******************/
	
	
	/***************** Part 5 ******************/
	
	public function getPart5_HTML(){
		
		$Part7_Title = "Self-Account";
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$Part7_Title) ;
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
						Title,
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnResultSet($sql);

		return $roleData;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$MainTitle='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:11px;" '; 


		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Title = $DataArr['Title'];
			$Detail = $DataArr['Details'];
			
			$Detail = strip_tags($Detail,'<p><br>');
		}
		else
		{
			$Detail = 'No Record Found.';
		}	
		$html .= '<table width="90%" height="" cellpadding="2" border="0px" align="center" style="font-size:16px;border-collapse: collapse;">
					<tr> 
						<td style=" font-size:15px;text-align:left;vertical-align:text-top;"><b>
						'.$MainTitle.'
						</b></td>
				  	</tr>
				  </table>';
		
		$html .= '<table width="90%" height="700px"  cellpadding="7" border="0px" align="center" '.$table_style.'>
					<tr>				 							
						<td style="font-size:14px;text-align:center;vertical-align: top;line-height: 1.5; height:10px;'.$_borderTop.$_borderLeft.$_borderRight.'">'.$Title.'</td>						 														
					</tr>
					<tr>				 							
						<td style="text-align:justify; text-justify:inter-ideograph; vertical-align: top;line-height: 1.5;'.$_borderLeft.$_borderBottom.$_borderRight.'">'.$Detail.'</td>						 														
					</tr>
				  </table>';	
		
			
		return $html; 
	}
	
	
	/***************** End Part 5 ******************/
	
	
	
	/***************** Part 6 : Footer ******************/
	public function getSignature_HTML($PageBreak='')
	{
		$SchoolChopStr = "School Chop";
		$PrincipalStr = "YAU Siu Hung";
		
		$ClassTeacherArray = $this->GetClassTeacherInfo();
		
		$_borderTop =  'border-top: 1px solid black; ';	
		
		$colwidth_html = '';
		
		$colwidth_html = '<col width="40%" />';
		$colwidth_html .= '<col width="20%" />';
		$colwidth_html .= '<col width="40%" />';
		
		if(count($ClassTeacherArray)==1)
		{
			$colwidth_html = '<col width="27%" />';
			$colwidth_html .= '<col width="10%" />';
			$colwidth_html .= '<col width="26%" />';
			$colwidth_html .= '<col width="10%" />';
			$colwidth_html .= '<col width="27%" />';
			
			$colspan_html = 'colspan="1"';
			$TearcherStr = 'Class Teacher ('.$this->StdClassName.')';
			
			$loopMax = 1;
		}
		else if(count($ClassTeacherArray)>=2)
		{
			$colwidth_html = '<col width="22%" />';
			$colwidth_html .= '<col width="5%" />';
			$colwidth_html .= '<col width="21%" />';
			$colwidth_html .= '<col width="5%" />';
			$colwidth_html .= '<col width="22%" />';
			$colwidth_html .= '<col width="5%" />';
			$colwidth_html .= '<col width="22%" />';
			
			$colspan_html = 'colspan="3"';
			$TearcherStr = 'Class Teachers ('.$this->StdClassName.')';
			
			$loopMax = 2;
		}
		
		$HardCodeStr = '<tr>
							<td colspan="2" align="center" style="vertical-align:top;"><b>&nbsp;</b></td>
							<td '.$colspan_html.' align="center" style="vertical-align:top;"><b>'.$TearcherStr.'</b></td>
							<td align="center" style="vertical-align:top;"><b>&nbsp;</b></td>
							<td align="center" style="vertical-align:top;"><b>Principal</td>
						</tr>';
		
		$ClassTeacher_html = '';
		for($i=0,$i_MAX=$loopMax;$i<$i_MAX;$i++)
		{
			$thisClassTeacherName = $ClassTeacherArray[$i]['EnglishName'];
			
			$ClassTeacher_html .= '	<td align="center" style="'.$_borderTop.'vertical-align:top;"><b>'.$thisClassTeacherName.'</b></td>';
			$ClassTeacher_html .= '<td>&nbsp;</td>';
		}
		
		
		$html = '';
		
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="font-size:15px;'.$PageBreak.'">';		
		   $html .= $colwidth_html;

		$html .= '	<tr>					
					    <td align="center" style="'.$_borderTop.'vertical-align:top;"><b>'.$SchoolChopStr.'</b></td>
						<td>&nbsp;</td>';

		$html .= $ClassTeacher_html;
		
		$html .= '		<td align="center" style="'.$_borderTop.'vertical-align:top;"><b>'.$PrincipalStr.'</b></td>
					</tr>';
		$html .= $HardCodeStr;
		$html .= ' </table>';

		return $html;
		
	}
	/***************** End Part 6 ******************/

	
	
	private function getPartContentDisplay_NoRowLimit($PartNum, $mainTitle,$subTitleArr='',$colWidthArr='',$DataArr='') 
	{				
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		$BgColor = $this->TableTitleBgColor;
		
		$Page_break='page-break-after:always;';
		
		$table_style = 'style="font-size:13px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 
		$FontSize = 'font-size:11px;';

		$ComponentNameArr = $this->getOLEcomponentName();
		
		$subtitle_html = '';
			
		foreach((array)$subTitleArr as $key=>$_title)
		{ 			
			$subtitle_html .='<td style="text-align:center;vertical-align:text-top;padding-top:5px;padding-bottom:5px;'.$_borderTop.$_borderBottom.$FontSize.'"><b>';
			$subtitle_html .=$_title;
			$subtitle_html .='</b></td>';			
		}	
		
		$ComponentID_Array = $this->DisplayComponent_Array;
				
		
							
		$total_Record=10;
	//	$rowHight = 10;

		$paddingLeft_html = '8px' ;

		$text_alignStr='center';
		
		$content_html = '';;
		
		$rowMax_count = 0;
		$rowMax_count = $DataArr['totalRecord'];

		$count_col =count($colWidthArr);

		$mainNumberOfPage = $this->NumberOfOtherPage;

		$ContentArray = array();

		$StartIndex = 0;
		
		$a_MAX = $this->nextLeftRow;
		
		if($rowMax_count<$this->nextLeftRow)
		{
			$a_MAX = $rowMax_count;
		}
		
		$breakLoop_ComponentID_Array = false;
		$a=0;
		for($i=0,$i_MAX=count($ComponentID_Array);$i<$i_MAX;$i++)
		{	
			$thisComponentID = $ComponentID_Array[$i];
			$thisComponentName = $ComponentNameArr[$thisComponentID];
		
			$thisComponentArr = $DataArr[$thisComponentID];
			
			if(is_array($thisComponentArr))
			{
				$this_content_html = '';
				$this_content_html .= '<tr>';	
					$this_content_html .= ' <td bgcolor="'.$this->TableTitleBgColor.'" colspan="'.$count_col.'" style="height:'.$rowHight.'px;text-align:left;vertical-align: top;padding-left:'.$paddingLeft_html.';'.$_borderTop.$_borderRight.$_borderBottom.$FontSize.'"><b><i>'.$thisComponentName.'</i></b></td>';
				$this_content_html .= '</tr>';
				
				$ContentArray['first'][$thisComponentID] = $this_content_html;	

				for($k=0,$k_MAX=count($thisComponentArr);$k<$k_MAX;$k++)
				{
					
					$breakLoop_ComponentID_Array = false;
					
					$this_content_html = '';
					$this_content_html .= '<tr>';	
							
					$thisColCount=0;		 
					foreach((array)$thisComponentArr[$k] as $_title=>$_titleVal)
					{
						if($_titleVal=='')
						{
						//	$_titleVal =  $this->EmptySymbol;
						}
						
						$text_alignStr = 'left';
						if($thisColCount==1)
						{
							$text_alignStr = 'center';
						}
						
						$this_content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;padding-left:'.$paddingLeft_html.';'.$FontSize.'">'.$_titleVal.'</td>';								
						$thisColCount++;
					}				
					$this_content_html .= '</tr>';		
				
					$ContentArray['first'][] = $this_content_html;				
					$StartIndex = $a;				
				
					$this->RowNoNow++;
					$a++;
					
					if($a==$a_MAX)
					{
						$breakLoop_ComponentID_Array = true;
						// 2014-0326-1025-36066 - start the next page with the index of this specific component
						// [Case #C60551] - ensure start the next page with the index of next component - prevent duplicate records
						$StartIndex = ++$k;
						break;					
					}				
			
				}
			}
			if($breakLoop_ComponentID_Array)
			{
				break;
			}
		}
		
		$StartComponentIndex = $i;
		$StartIndex = ($StartIndex==0)? 1: $StartIndex;

		$loopCount = 0;
		$numberOfRow = 1;

		$breakLoop_ComponentID_Array = false;
		$a=0;
		
		//debug_pr($StartComponentIndex);
		for($i=$StartComponentIndex,$i_MAX=count($ComponentID_Array);$i<$i_MAX;$i++)
		{
			$thisComponentID = $ComponentID_Array[$i];

			$thisComponentName = $ComponentNameArr[$thisComponentID];
			
			$thisComponentArr = $DataArr[$thisComponentID];
			
			if(count($thisComponentArr)==0)
			{
				continue;
			}

			if($i==$StartComponentIndex && $StartIndex!=0)
			{

			}
			else 
			{
				$this_content_html = '';
				$this_content_html .= '<tr>';	
					$this_content_html .= ' <td bgcolor="'.$this->TableTitleBgColor.'" colspan="'.$count_col.'" style="height:'.$rowHight.'px;text-align:left;padding-left:'.$paddingLeft_html.';vertical-align: top;'.$_borderTop.$_borderRight.$_borderBottom.$FontSize.'"><b><i>'.$thisComponentName.'</i></b></td>';
				$this_content_html .= '</tr>';
				$ContentArray[$loopCount][$thisComponentID] = $this_content_html;	
			}			
			
			if(is_array($thisComponentArr))
			{	
				$thisStartCounter = 0;
				
				if($i==$StartComponentIndex)
				{
					$thisStartCounter = $StartIndex;
				}
				
				for($k=$thisStartCounter,$k_MAX=count($thisComponentArr);$k<$k_MAX;$k++)
				{
					
					$breakLoop_ComponentID_Array = false;
					
					$this_content_html = '';
					$this_content_html .= '<tr>';	
						
					$thisColCount = 0;			 
					foreach((array)$thisComponentArr[$k] as $_title=>$_titleVal)
					{
						if($_titleVal=='')
						{
				//			$_titleVal =  $this->EmptySymbol;
						}
						
						$text_alignStr = 'left';
						if($thisColCount==1)
						{
							$text_alignStr = 'center';
						}
						
						$this_content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;padding-left:'.$paddingLeft_html.';'.$FontSize.'">'.$_titleVal.'</td>';								
						$thisColCount++;
					}				
					$this_content_html .= '</tr>';		
				
					$ContentArray[$loopCount][] = $this_content_html;

					if($numberOfRow%$mainNumberOfPage ==0 && $numberOfRow!=1)
					{	
						$loopCount++;
					}		
					$this->RowNoNow++;
					$numberOfRow++;			
					
					$a++;
								
				}
				
			}

		}
		
	//debug_r($ContentArray);	
				
		$colNumber = count($colWidthArr);
			
		/****************** Main Table ******************************/
		
		$html = '';
			$html .= '<table width="100%" height="1017px" cellpadding="1" border="0px" align="center">';
				$html .= '<tr><td style="vertical-align:top;">';
				
					$html .=$this->getEmptyTable('3%');
					$html .= $this->getPart1_HTML();	
					$html .=$this->getEmptyTable('1%');
					$html .= $this->getPart2_HTML();	
					$html .=$this->getEmptyTable('2%');
					$html .= $this->getPart3_HTML();	
					$html .=$this->getEmptyTable('2%');

					$Page_breakHTML = '';
					if($rowMax_count>0)
					{
						$thisNumberPage = ($PartNum=='3')?$this->NumberOfFirstPage : $this->NumberOfOtherPage;	
				
						if($this->RowNoNow>$thisNumberPage)
						{
							$Page_break='page-break-after:always;';		
							$Page_breakHTML = $this->getPagebreakTable(); 
						}
						else if ($this->RowNoNow==0)
						{
							$Page_break='';
						}
						
						$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 
			
						
						if(count($ContentArray)==1)
						{
							$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;"'; 
							
							if($PartNum=='4')
							{
								$this->nextLeftRow =$this->NumberOfFirstPage -count($ContentArray['first']);
							}
							else
							{
								$this->nextLeftRow =$this->NumberOfOtherPage -count($ContentArray['first']);
							}
						}
										
			
						$html .= '<table width="90%" height="10px" cellpadding="7" border="0px" align="center" style="font-size:10px;border-collapse: collapse;">
									<tr> 
										<td style=" font-size:15px;text-align:left;vertical-align:text-top;"><b>
										'.$mainTitle.'
										</b></td>
								  	</tr>';
						
						$html .=    '</table>';
						
						$html .= '<table width="90%" height="" cellpadding="1" border="0px" align="center" style="font-size:14px;border-collapse: collapse;border:1px solid #000000;">';
								foreach((array)$colWidthArr as $key=>$_colWidthInfo)
								{
									$html .=$_colWidthInfo;
								}
						$html .= '<tr>'.$subtitle_html.'</tr>';
						foreach((array)$ContentArray['first'] as $_key => $_rowHtml)
						{
							$html .= $_rowHtml;
						}
					}
				
					$html .='</table>';
				$html .= '</td></tr>';
			$html .='</table>';	
	
			$html .=$this->getEmptyTable($Height='',$Content='Page '.$this->PageNumberOfThisPage.' of <!--totalPage-->');		 
			$html .=$this->getPagebreakTable(); 
			$this->PageNumberOfThisPage++;
			
			
		if($rowMax_count>0)
		{		

			for($a=0,$a_MAX=count($ContentArray)-1;$a<$a_MAX;$a++)
			{
				if($a==$a_MAX-1)
				{			
					if($PartNum=='4')
					{
						$this->nextLeftRow =$this->NumberOfFirstPage -count($ContentArray[$a]);
					}
					else
					{
						$this->nextLeftRow =$this->NumberOfOtherPage -count($ContentArray[$a]);
					}
						
					if($this->nextLeftRow<0)
					{
						$Page_break='page-break-after:always;';	
						$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 
						
					}
					else
					{					
						$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;"'; 
					}
					
				}
				$html .=$this->getEmptyTable('3%');
				$html .= '<table width="100%" height="985px" cellpadding="1" border="0px" align="center">';
				$html .= '<tr><td style="vertical-align:top;">';
					$html .= '<table width="90%" height="" cellpadding="1" border="0px" align="center" style="font-size:14px;border-collapse: collapse;border:1px solid #000000;">';
						foreach((array)$colWidthArr as $key=>$_colWidthInfo)
						{
							$html .=$_colWidthInfo;
						}
					$html .= '<tr>'.$subtitle_html.'</tr>';
					foreach((array)$ContentArray[$a] as $_key => $_rowHtml)
					{
						$html .= $_rowHtml;
					}
					$html .='</table>';
				$html .= '</td></tr>';
				$html .='</table>';	
				
				$html .=$this->getEmptyTable($Height='',$Content='Page '.$this->PageNumberOfThisPage.' of <!--totalPage-->');	
				$html .=$this->getPagebreakTable(); 
				$this->PageNumberOfThisPage++;
				
			} 
		}	
			
		return $html;
	}

	
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false,$isMember=false)
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		
		$OLEcomponentArr = $this->getOLEcomponentName();
		
		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = " And SLPOrder is not null"; 
		}
	
		if($IntExt!='')
		{
			$cond_IntExt = " And ole_prog.IntExt='".$IntExt."'";
		}
				
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = " And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$thisDisplayComponent_Array = $this->DisplayComponent_Array;
				
		$cond_ComponentDisplay = "And ole_prog.ELE IN ('".implode("','",(array)$thisDisplayComponent_Array)."')";
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
		
		$sql = "Select
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_std.role as Role,
						ole_prog.ELE as OleComponent
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID

				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
						$cond_ComponentDisplay
				order by 
						ole_prog.ELE,
						y.Sequence asc,	
						ole_prog.Title asc		

				";
		$roleData = $this->objDB->returnResultSet($sql);

		$returnData['totalRecord'] = count($roleData);
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{		
		    $thisDataArr = array();	

			$thisDataArr['Program'] =$roleData[$i]['Title'];
			$thisDataArr['SchoolYear'] =$roleData[$i]['YEAR_NAME'];		
			$thisDataArr['Organization'] =str_replace(',',',<br/>',$roleData[$i]['Organization']);		
		//	$thisDataArr['Organization'] = $roleData[$i]['Organization'];		
			
			$RoleAppendStr='';
			if(trim($roleData[$i]['Achievement'])!='')
			{
				$RoleAppendStr = ',<br/>'.$roleData[$i]['Achievement'];
			}				
			$thisDataArr['RoleAchievement'] =str_replace(',',',<br/>',$roleData[$i]['Role']).$RoleAppendStr;		
			$thisComponent =$roleData[$i]['OleComponent'];	
					
			$returnData[$thisComponent][] = $thisDataArr;	
		
		}
//debug_r($sql);
		return $returnData;
	}
	
	public function GetClassTeacherInfo() {

		$AcademicId = Get_Current_Academic_Year_ID();
		

		$sql = "Select
						iu.EnglishName,
						iu.ChineseName,
						iu.TitleEnglish,
						iu.TitleChinese
				From
						YEAR_CLASS_USER as ycu
						Inner Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
						Inner Join YEAR_CLASS_TEACHER as yct On (yc.YearClassID = yct.YearClassID)
						Inner Join INTRANET_USER as iu On (yct.UserID = iu.UserID)
				Where
						ycu.UserID = '".$this->uid."'
						And yc.AcademicYearID = '".$AcademicId."'
				order by 
						iu.EnglishName
				";
		$roleData = $this->objDB->returnResultSet($sql);

		return $roleData;
	}
	
	public function getOLEcomponentName()
	{
		$OLE_ELE = $this->Get_eClassDB_Table_Name('OLE_ELE');
		
		$sql="Select 
					EngTitle,
					DefaultID 
			  from
					$OLE_ELE
			";
		$roleData = $this->objDB->returnResultSet($sql);
		
		$returnArray = array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$thisName_en = $roleData[$i]['EngTitle'];
			$thisID = $roleData[$i]['DefaultID'];
			
			$returnArray[$thisID] = $thisName_en;
		}
	
		return $returnArray;
	}
	
	public function getSubjectPosition($ParSubjectID='',$ParAcademicYearID='')
	{
		$ASSESSMENT_STD_SUBJECT_RECORD = $this->Get_eClassDB_Table_Name('ASSESSMENT_STUDENT_SUBJECT_RECORD');
		
		$sql="Select 
					OrderMeritForm,
					OrderMeritFormTotal

			  from
					$ASSESSMENT_STD_SUBJECT_RECORD
			  where 
					UserID = '".$this->uid."'
					AND IsAnnual='1'
					AND SubjectID = '".$ParSubjectID."'
					AND AcademicYearID = '".$ParAcademicYearID."'
			";
		$roleData = $this->objDB->returnResultSet($sql);
		
		if(is_array($roleData))
		{
			$roleData = current($roleData);
		}
		return $roleData;
	}
	
	public function getAveragePosition($ParAcademicYearID='')
	{
		$ASSESSMENT_STD_MAIN_RECORD = $this->Get_eClassDB_Table_Name('ASSESSMENT_STUDENT_MAIN_RECORD');
		
		$sql="Select 
					OrderMeritForm,
					OrderMeritFormTotal,
					Score

			  from
					$ASSESSMENT_STD_MAIN_RECORD
			  where 
					UserID = '".$this->uid."'
					AND IsAnnual='1'
					AND AcademicYearID = '".$ParAcademicYearID."'
			";
		$roleData = $this->objDB->returnResultSet($sql);
		
		if(is_array($roleData))
		{
			$roleData = current($roleData);
		}
		return $roleData;
	}
	
	public function getAverageFullMark($ParAcademicYearID='')
	{		
		return 100;
	}
	
	public function getStudentAcademic_YearClassNameArr($AllYear=false)
	{
		$StudentID = $this->uid;
		
		$academicYearIDArr = $this->academicYearIDArr;
		
		if($AllYear)
		{
			$academicYearIDArr = $this->All_AcademicYearID;
		}
		
		$SelectStr= 'h.AcademicYear as AcademicYear,
					 h.ClassName as ClassName';
		
		if($academicYearIDArr!='')
		{		
			$YearArr = array();
			for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
			{
				if($academicYearIDArr[$i]=='')
				{
					continue;
				}
				$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
				$YearArr[] = $thisAcademicYear;
			}
			
			$cond_academicYear = "And h.AcademicYear In ('".implode("','",(array)$YearArr)."')";		
		}
		

		$Profile_his = $this->Get_IntranetDB_Table_Name('PROFILE_CLASS_HISTORY');
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$sql = 'select 
						'.$SelectStr.' 
				from 
						'.$Profile_his.' as h 
				where 
						h.UserID = "'.$StudentID.'"
						'.$cond_academicYear.'
				Order by
						h.AcademicYear desc';
						
		$roleData = $this->objDB->returnResultSet($sql);
		
	
		return $roleData;
	}
	
	
	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		
		if(count($returnArray)==1)
		{
			return $returnArray[0];
		}
		else
		{
			$lastNo = count($returnArray)-1;
			
			$firstYear = substr($returnArray[0], 0, 4);
			$lastYear = substr($returnArray[$lastNo],-4);
			
			$returnValue = $firstYear."-".$lastYear;
			
			return $returnValue;	
		}
		
	}
	
	/********** Display Form 4, 5 and 6  ***********/
	private function getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,$order='asc')
	{
		
		$YearDisplayArr = array();
	
		$Form4_no = 0;
		$Form5_no = 0;
		$Form6_no = 0;
		

		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{						
			$thisYear = $YearClassArray[$i]['AcademicYear'];
			$thisClassName = $YearClassArray[$i]['ClassName'];
			
			$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
			
			$thisFormNo = (int)$thisClassName;
			
			preg_match('/\d+/', $thisClassName, $number);
			
			if(count($number)>0 && is_array($number))
			{
				$thisFormNo = $number[0];
				
				if($order=='asc')
				{
					if($thisFormNo==4 || $thisFormNo==5 || $thisFormNo==6)
					{				
						$YearDisplayArr[$thisFormNo] = $YearClassArray[$i];
					}
				}
				else if($order=='desc')
				{
					$CondArr = array();
					$CondArr[] = ($thisFormNo==4 && $Form4_no==0);
					$CondArr[] = ($thisFormNo==5 && $Form5_no==0);
					$CondArr[] = ($thisFormNo==6 && $Form6_no==0);
					
					if(in_array(true,$CondArr))
					{
						$YearDisplayArr[$thisFormNo] = $YearClassArray[$i];
					}
				}			
				
				if($thisFormNo==4)
				{
					$Form4_no++;
				}
				else if($thisFormNo==5)
				{
					$Form5_no++;
				}
				else if($thisFormNo==6)
				{
					$Form6_no++;
				}
				
				
			}
		}	
		
		$ThisClassArr = array();
		$count_YearDisplayArr = 0;
		foreach((array)$YearDisplayArr as $_formNo=>$_ClassArr)
		{
			$ThisClassArr[$count_YearDisplayArr] = $_ClassArr;
			$count_YearDisplayArr++;
		}
		
		return $ThisClassArr;
	}
	
	
	private function getAcademicYearArr($academicYearIDArr)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	
	public function getEmptyTable($Height='',$Content='&nbsp;') {
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="font-size:11px;">					
		   			 <tr>
					    <td style="text-align:center">'.$Content.'</td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>