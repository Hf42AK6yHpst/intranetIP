<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/libportfolio.php");
include_once($intranet_root."/includes/libportfolio2007a.php");
include_once($intranet_root."/includes/libpf-slp.php");
include_once($intranet_root."/includes/portfolio25/iPortfolioConfig.inc.php");
include_once($intranet_root."/includes/libclass.php");
include_once($intranet_root."/includes/libinterface.php");
include_once($intranet_root."/includes/libuser.php");
include("lang/lang.$intranet_session_language.php");

class libpf_slp_cnecc
{
  private $db;
  private $displayType;
  private $studentId;
  private $academicYearId;
  private $userInfo;
  private $roles;
	private $logFile = "";
	
  const ENABLED = 1;
  const DISABLED = 0;
  const DISPLAY_TYPE_HTML = "HTML";
  const DISPLAY_TYPE_PRINT = "PRINT";
  


  function __construct($ParStudentId=-1, $ParAcademicYearId=-1) {
  	global $Lang;
	global $intranet_root;
    $this->db = new libdb();
    $this->displayType = self::DISPLAY_TYPE_HTML;
    $this->studentId = $ParStudentId;


//	error_log("init ".$ParStudentId." ParAcademicYearId == > ".$ParAcademicYearId."\n", 3, "/tmp/aaa.txt");
    if ($ParStudentId>0) {
    	$this->userInfo = new libuser($this->studentId);
    } else {
    	// do nothing
    }
    if ($ParAcademicYearId>0) {
    	$this->academicYearId = $ParAcademicYearId;
    } else {
    	$this->academicYearId = Get_Current_Academic_Year_ID();
    }
    $this->roles = array($Lang["Cust_Cnecc"]["Participant"],$Lang["Cust_Cnecc"]["Organizer"],$Lang["Cust_Cnecc"]["Helper"]);
	$this->logFile = $intranet_root."/file/iportfolio/cnecc_Program_COPY_HISTORY.txt";
  }
	public function getLogFile(){
		return $this->logFile;
	}

	//GET ALL the SUN SHINE SCHEME program create in one year
	private function getEventInfo($ParStudentId=-1) {
		global $eclass_db;
		$returnArray = array();
		if ($ParStudentId>-1) {
			$cond_studentSpecified = " AND (CCSE.SPECIFIEDSTUDENTID = $ParStudentId OR CCSE.SPECIFIEDSTUDENTID IS NULL) ";
		}
		$sql = "SELECT
					OP.TITLE,
					OP.ELE,
					OP.CATEGORY AS CATEGORYID,
					CCSE.EVENTID,
					CCSE.PROGRAMID,
					CCSE.ATTAINMENT,
					CCSE.ROLE,
					CCSE.POINT,
					CCSE.STATUS
				FROM
					$eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
					INNER JOIN $eclass_db.OLE_PROGRAM OP
					ON CCSE.PROGRAMID = OP.PROGRAMID
				WHERE
					OP.INTEXT = 'INT'
					AND
					CCSE.STATUS = " . self::ENABLED . " 
					AND
					OP.ACADEMICYEARID = $this->academicYearId
					$cond_studentSpecified
					ORDER BY OP.TITLE, CCSE.POINT, CCSE.ATTAINMENT, CCSE.ROLE";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
		$returnArray = $this->db->returnArray($sql);		
		return $returnArray;
	}
	
	private function getStudentsSuccessInfo() {
		global $eclass_db;
		$returnArray = array();
		$sql = "SELECT
					CCSS.RECORDID,
					CCSS.EVENTID,
					CCSS.ENDDATE,
					CCSS.STUDENTID
				FROM
					$eclass_db.CUSTOM_CNECC_SS_STUDENT CCSS
					INNER JOIN $eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
						ON CCSS.EVENTID = CCSE.EVENTID
					INNER JOIN $eclass_db.OLE_STUDENT OS
						ON CCSS.OS_RECORDID = OS.RECORDID
					INNER JOIN $eclass_db.OLE_PROGRAM OP
						ON OS.PROGRAMID = OP.PROGRAMID
				WHERE
					CCSS.STUDENTID = $this->studentId
					AND CCSE.STATUS = ".self::ENABLED."
					AND OS.RECORDSTATUS = 2
					AND OP.ACADEMICYEARID = $this->academicYearId";
//		debug_r($sql);
//error_log("f:getStudentsSuccessInfo ".$sql."\n", 3, "/tmp/aaa.txt");
		$returnArray = $this->db->returnArray($sql);
		return $returnArray;
	}
	private function combineInfoWithStudents($ParInfoArray, $ParStudentsArray) {

		$returnArray = array();
		$sizeOfInfoArray = count($ParInfoArray);
		$sizeOfStudentsArray = count($ParStudentsArray);

		for($i=0;$i<$sizeOfInfoArray;$i++) {
			for($j=0;$j<$sizeOfStudentsArray;$j++){
				if ($ParStudentsArray[$j]["EVENTID"] == $ParInfoArray[$i]["EVENTID"]) {
					$ParInfoArray[$i]["SUCCESS"] = true;
					$ParInfoArray[$i]["ENDDATE"] = $ParStudentsArray[$j]["ENDDATE"];
				} else {
					// do nothing
				}
			}
		}
		$returnArray = $ParInfoArray;
		return $returnArray;
	}
	
	private function setDisplayType($ParDisplayType=DISPLAY_TYPE_HTML) {
		switch($ParDisplayType) {
			case $this->DISPLAY_TYPE_HTML:
				$this->displayType = self::DISPLAY_TYPE_HTML;
				break;
			case $this->DISPLAY_TYPE_PRINT:
				$this->displayType = self::DISPLAY_TYPE_PRINT;
				break;
			default:
				$this->displayType = self::DISPLAY_TYPE_HTML;
				break;
		}
	}
	
	private function getHtmlStyle() {
		$cssArray = array();
		$cssArray["TABLE1"] = "  width='100%' cellspacing='1' cellpadding='4' border='0' bgcolor='#cccccc' ";
		$cssArray["TR1"] = " class='tabletop' ";
		$cssArray["TD1"] = " class='tabletopnolink' ";
		$cssArray["TD2_ODD"] = " class='tabletext' height='20' bgcolor='#ffffff' align='center' ";
		$cssArray["TD2_EVEN"] = " class='tabletext' height='20' bgcolor='#f3f3f3' align='center' ";
		return $cssArray;
	}
	
	private function getPrintStyle() {
		$cssArray = array();
		return $cssArray;
	}
	
	private function generateCheckListHtml($ParCheckListResultArray) {
		global $Lang, $intranet_session_language,$intranet_root,$button_new;
		
		# setting css
		$cssArray = array();
		if ($this->displayType == self::DISPLAY_TYPE_HTML) {
			$cssArray = $this->getHtmlStyle();
		} else if ($this->displayType == self::DISPLAY_TYPE_PRINT) {
			$cssArray = $this->getPrintStyle();
		} else {
			// do nothing
		}
		
		# construct table shell
		$table = "<table " . $cssArray["TABLE1"] . ">
					<thead>
					<tr " . $cssArray["TR1"] . ">
						<td " . $cssArray["TD1"] . ">
							" . $Lang["Cust_Cnecc"]["Domain"] . "
						</td>
						<td " . $cssArray["TD1"] . ">
							" . $Lang["Cust_Cnecc"]["Event"] . "
						</td>
						<td " . $cssArray["TD1"] . ">
							" . $Lang["Cust_Cnecc"]["Attainment"] . "
						</td>
						<td " . $cssArray["TD1"] . ">
							" . $Lang["Cust_Cnecc"]["Role"] . "
						</td>
						<td " . $cssArray["TD1"] . ">
							" . $Lang["Cust_Cnecc"]["Point(s)"] . "
						</td>
						<td " . $cssArray["TD1"] . ">
							<span id='noOfSuccess'></span>" . $Lang["Cust_Cnecc"]["Success(v)"] . "<input type='checkbox' id='successCheck' onClick='checkUncheckAll()' />
						</td>
						<td " . $cssArray["TD1"] . ">
							" . $Lang["Cust_Cnecc"]["EndDate"] . "
						</td>
					</tr>
					</thead>
					<tbody>
					{{{ TBODY }}}
					</tbody>
				</table>";

		# construct table body
		$libinterface = new interface_html();
		$tbody = "";
		$row = 0;
		foreach($ParCheckListResultArray as $key => $info) {
			$name = $info["NAME"];
			$elements = $info["ELEMENTS"];
			$sizeOfElements = count($elements);
			$elementsHtml = "";
			
			if ($row%2==0) {
				$tdstyle = $cssArray["TD2_ODD"];
			} else {
				$tdstyle = $cssArray["TD2_EVEN"];
			}
			if ($sizeOfElements > 0) {
				for($j=0;$j<$sizeOfElements;$j++) {
					$checked = "";
					if ($elements[$j]["SUCCESS"]) {
						$checked = "checked='checked'";
					} else {
						
					}
					$elementsHtml .= "<tr>";
					if ($j==0) {
						$elementsHtml .= "{{{ DOMAIN }}}";
					} else {
						// do nothing
					}
					$elementsHtml .= "<td $tdstyle>" . $elements[$j]["TITLE"] . "</td>";
					$elementsHtml .= "<td $tdstyle>" . $elements[$j]["ATTAINMENT"] . "</td>";
					$elementsHtml .= "<td $tdstyle>" . $Lang["Cust_Cnecc"][$elements[$j]["ROLE"]]["label"] . "</td>";
					$elementsHtml .= "<td $tdstyle>" . $elements[$j]["POINT"] . "</td>";
					$elementsHtml .= "<td $tdstyle><input type='checkbox' id='success" . $elements[$j]["EVENTID"] . "' name='success[]' onClick='writeSuccess();' value='" . $elements[$j]["EVENTID"] . "' $checked /></td>";
					
					$dateBox = "<input type='text' name='date" . $elements[$j]["EVENTID"] . "' size='8' maxlength='10' class='tabletext' value='" . $elements[$j]["ENDDATE"] . "' onFocus='if(this.value==\"\"){this.value=\"" . date('Y-m-d') . "\";}' />";
					$dateBox .= $libinterface->GET_CALENDAR("form1", "date" . $elements[$j]["EVENTID"] , "");//$elements[$j]["EVENTID"]
					$elementsHtml .= "<td $tdstyle nowrap>" . $dateBox . "</td>";
					$elementsHtml .= "</tr>";
				}
			} else {
				$elementsHtml .= "<tr>";
				$elementsHtml .= "{{{ DOMAIN }}}";
				$elementsHtml .= "<td colspan='6' $tdstyle>" . $Lang["Cust_Cnecc"]["NoRecordFound"] . "</td>";
				$elementsHtml .= "</tr>";
			}
			$tbody .= str_replace("{{{ DOMAIN }}}", "<td " . $cssArray["TR1"] . $cssArray["TD1"] . " rowspan='" . ( $sizeOfElements==0?"1":$sizeOfElements) . "'>$name <a href='/home/portfolio/customize/cnecc/addStudentSpecificSs.php?KeepThis=true&studentId={$this->studentId}&ELE=" . $info["ELEID"] . "&TB_iframe=true&height=450&width=760' class='thickbox' title='OLE Program Specified for Students'>[$button_new+]</a></td>", $elementsHtml);
			$row++;
		}
		$Html .= str_replace("{{{ TBODY }}}", $tbody, $table);
		
		return $Html;
	}
	private function categorizeByEle($ParEleArray, $ParResultArray) {
		$returnArray = array();
		$sizeOfEleArray = count($ParEleArray);
		$sizeOfResultArray = count($ParResultArray);

		foreach($ParEleArray as $key => $name) {
			$tempArray = array("NAME"=>$name, "ELEID"=>$key);
			for($i=0;$i<$sizeOfResultArray;$i++) {
				$resultEleArray = explode(",", $ParResultArray[$i]["ELE"]);
				$resultFirstEle = $resultEleArray[0];
				if ($key == $resultFirstEle) {
					$tempArray["ELEMENTS"][] = $ParResultArray[$i];
				} else {
					// do nothing
				}
			}
			$returnArray[$key] = $tempArray;
		}
		return $returnArray;
	}
	
	private function getCategorizedResult() {
		# combine students success status to event
		####################################################
		# get event info
		$eventInfo = $this->getEventInfo($this->studentId);
		
		# if get table with user id
		if ($this->studentId > -1) {
			# get joint student
			$students = $this->getStudentsSuccessInfo($this->studentId);

			# combine event info and joint student
			$combinedResult = $this->combineInfoWithStudents($eventInfo,$students);

		} else {
			# combined result = event info
			$combinedResult = $eventInfo;
		}
		
		# categorize the combined results by ELE
		####################################################
		# get ELE list
		$libpf_slp = new libpf_slp();
		$eleList = $libpf_slp->GET_ELE();	// Domain
		
		$catergorizedResult = $this->categorizeByEle($eleList, $combinedResult);
		return $catergorizedResult;
	}
	function getCheckListTable() {
		# get categorized result
		$catergorizedResult = $this->getCategorizedResult();

		# generate check list html
		$Html = $this->generateCheckListHtml($catergorizedResult);
		
		# return html
		return $Html;
	}
	
	function getRecordIdByEventId($ParEventId) {
		global $eclass_db;
		$returnResult = false;
		$sql = "SELECT
					DISTINCT OS.RECORDID
				FROM
					$eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
					INNER JOIN $eclass_db.OLE_PROGRAM OP
						ON CCSE.PROGRAMID = OP.PROGRAMID
					INNER JOIN $eclass_db.OLE_STUDENT OS
						ON OS.PROGRAMID = OP.PROGRAMID
				WHERE
					CCSE.STATUS = " . self::ENABLED . "
					AND
					CCSE.EVENTID = $ParEventId
					AND
					OS.USERID = " . $this->studentId;

		$returnArray = $this->db->returnArray($sql);
		$returnResult = $returnArray[0]["RECORDID"];
		return $returnResult;
	}
	
	private function getProgramInfoByEventId($ParEventId) {
		global $eclass_db;
		$sql = "SELECT
					DISTINCT OP.PROGRAMID,
					OP.TITLE,
					OP.STARTDATE,
					OP.ENDDATE,
					OP.CATEGORY,
					OP.ELE,
					OP.INTEXT
				FROM
					$eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
					INNER JOIN $eclass_db.OLE_PROGRAM OP
						ON CCSE.PROGRAMID = OP.PROGRAMID
				WHERE
					CCSE.EVENTID = $ParEventId";
//		debug_r($sql);
		$returnArray = $this->db->returnArray($sql);
		return $returnArray[0];
	}
	
	function getEventIds() {
		global $eclass_db;
		$returnVector = array();
		$sql = "SELECT
					DISTINCT CCSS.EVENTID
				FROM
					$eclass_db.CUSTOM_CNECC_SS_STUDENT CCSS
				WHERE
					CCSS.STUDENTID = " . $this->studentId;
		$returnVector = $this->db->returnVector($sql);
		return $returnVector;
	}
	
	function insertOleStudentByEventId($ParEventId) {
		global $eclass_db, $ipf_cfg;
		$programInfo = $this->getProgramInfoByEventId($ParEventId);

		$approvalPerson = $_SESSION["UserID"];
		$sql = "INSERT INTO
					$eclass_db.OLE_STUDENT
				SET
					USERID = " . $this->studentId . ",
					TITLE = '" . $programInfo["TITLE"] . "',
					CATEGORY = '" . $programInfo["CATEGORY"] . "',
					ELE = '" . $programInfo["ELE"] . "',
					ROLE = '',
					HOURS = '',
					ACHIEVEMENT = '',
					ORGANIZATION = '',
					APPROVEDBY = $approvalPerson,
					RECORDSTATUS = " . $ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"] . ",
					DETAILS = '',
					PROGRAMID = " . $programInfo["PROGRAMID"] . ",
					PROCESSDATE = NOW(),
					STARTDATE = '" . $programInfo["STARTDATE"] . "',
					ENDDATE = '" . $programInfo["ENDDATE"] . "',
					INPUTDATE = NOW(),
					MODIFIEDDATE = NOW(),
					INTEXT = '" . $programInfo["INTEXT"] . "',
					COMEFROM = " . $ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherInput"];
//	debug_r($sql);die;
		$this->db->db_db_query($sql);
		
		return $this->db->db_insert_id();
	}
	
	function updateRecord($ParStudentId=-1, $ParEventId, $ParEndDate=null) {
		global $eclass_db;
		if (isset($ParEndDate) && !empty($ParEndDate) && $ParEndDate != '0000-00-00') {
			$endDate = "'" . $ParEndDate . "'";
		} else {
			$endDate = 'NULL';
		}
		
		$sql = "UPDATE
					$eclass_db.CUSTOM_CNECC_SS_STUDENT
				SET
					ENDDATE = $endDate,
					MODIFIEDDATE = NOW()
				WHERE
					STUDENTID = $ParStudentId
					AND
					EVENTID = $ParEventId";
//		debug_r($sql);
		$this->db->db_db_query($sql);
		return mysql_info();
	}
	
	function insertRecord($ParOsRecordId, $ParEventId, $ParEndDate=null) {
		global $eclass_db;
		if (isset($ParEndDate) && !empty($ParEndDate)) {
			$endDate = "'" . $ParEndDate . "'";
		} else {
			$endDate = 'NULL';
		}
		
		$sql = "INSERT INTO
					$eclass_db.CUSTOM_CNECC_SS_STUDENT
				SET
					OS_RECORDID = $ParOsRecordId,
					EVENTID = $ParEventId,
					STUDENTID = " . $this->studentId . ",
					ENDDATE = $endDate,
					INPUTDATE = NOW(),
					MODIFIEDDATE = NOW()";
//		debug_r($sql);die;
		$this->db->db_db_query($sql);
		return $this->db->db_insert_id();
	}
	
	private function getCcseEventIds() {
		global $eclass_db;
		$sql = "SELECT
					CCSE.EVENTID
				FROM
					$eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
					INNER JOIN $eclass_db.OLE_PROGRAM OP
					ON CCSE.PROGRAMID = OP.PROGRAMID
				WHERE
					OP.INTEXT = 'INT'
					AND
					CCSE.STATUS = " . self::ENABLED . "
					AND
					OP.ACADEMICYEARID = $this->academicYearId";
		$returnVector = array();
		$returnVector = $this->db->returnVector($sql);
//		debug_r($returnVector);die;
		return $returnVector;
	}
	
	private function removeOleStudent($ParRemoveIdArray) {
		global $eclass_db;
		$q_result = false;
		if (count($ParRemoveIdArray) > 0) {
			$removeIdString = implode(",", $ParRemoveIdArray);
			$sql = "DELETE FROM
						$eclass_db.OLE_STUDENT
					WHERE
						RECORDID IN ($removeIdString)";
			$q_result = $this->db->db_db_query($sql);
		} else {
			$q_result = true;
		}
		return $q_result;
	}
	
	private function getStudentCcssOsRecordIds() {
		global $eclass_db;
		$returnVector = array();
		$sql = "SELECT
					DISTINCT CCSS.OS_RECORDID
				FROM
					$eclass_db.CUSTOM_CNECC_SS_STUDENT CCSS
				INNER JOIN $eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
					ON CCSS.EVENTID = CCSE.EVENTID
				INNER JOIN $eclass_db.OLE_PROGRAM OP
					ON CCSE.PROGRAMID = OP.PROGRAMID
				WHERE
					OP.INTEXT='INT'
				AND
					CCSE.STATUS=" . self::ENABLED . "
				AND
					OP.ACADEMICYEARID = $this->academicYearId";
		$returnVector = $this->db->returnVector($sql);
		return $returnVector; 
	}
	
	private function getCcssRecordIdForRemove($ParEventIdArray) {
		global $eclass_db;
		$eventIdCond = "";
		if (isset($ParEventIdArray) && count($ParEventIdArray)>0) {
			$eventIdString = implode(",",$ParEventIdArray);
			$eventIdCond = " AND
					CCSS.EVENTID NOT IN ($eventIdString) ";
		} else {
			// do nothing
		}
		$sql = "SELECT CCSS.RECORDID
				FROM $eclass_db.CUSTOM_CNECC_SS_STUDENT CCSS
				INNER JOIN $eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
					ON CCSS.EVENTID = CCSE.EVENTID
				INNER JOIN $eclass_db.OLE_PROGRAM OP
					ON CCSE.PROGRAMID = OP.PROGRAMID
				WHERE
					CCSS.STUDENTID = $this->studentId
					AND
					OP.ACADEMICYEARID = $this->academicYearId
					$eventIdCond";
		$returnVector = $this->db->returnVector($sql);
		return $returnVector;
	}
	
	function removeRecord($ParEventIdArray) {
		global $eclass_db;
		$q_result = true;
		$ccssRecordIdArray = $this->getCcssRecordIdForRemove($ParEventIdArray);
		$ccssRecordIdString = implode(",",$ccssRecordIdArray);
		# student id should be exist
		if ($this->studentId<0) {
			// do nothing
		} else {
			$preOsRecordIds = $this->getStudentCcssOsRecordIds();			

			if (empty($ccssRecordIdString)) {
				// do nothing
			} else {
			##################################
			##	Start: Handling for specified program for a student
			##################################
			$sql = "SELECT CCSS.EVENTID, CCSS.OS_RECORDID 
					FROM $eclass_db.CUSTOM_CNECC_SS_STUDENT CCSS
					INNER JOIN $eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
						ON CCSS.EVENTID = CCSE.EVENTID
					INNER JOIN $eclass_db.OLE_STUDENT OS
						ON CCSS.OS_RECORDID = OS.RECORDID
					WHERE CCSS.RECORDID IN ($ccssRecordIdString)
						AND CCSE.SPECIFIEDSTUDENTID = {$this->studentId}";
			$os_cnss_to_remove = $this->db->returnArray($sql);
			foreach($os_cnss_to_remove as $key => $element) {
				$recordIdToRemove[] = $element["OS_RECORDID"];
				$eventIdToRemove[] = $element["EVENTID"];
			}	
			if (count($recordIdToRemove)>0) {
				$sql = "DELETE FROM $eclass_db.OLE_STUDENT WHERE RECORDID IN (" . implode(",",$recordIdToRemove) .")";
				$q_result = $q_result && $this->db->db_db_query($sql);
			}
			if (count($eventIdToRemove)>0) {
				$sql = "DELETE FROM $eclass_db.CUSTOM_CNECC_SS_EVENT WHERE EVENTID IN (" . implode(",",$eventIdToRemove) .")";
				$q_result = $q_result && $this->db->db_db_query($sql);
			}
			##################################
			##	End: Handling for specified program for a student
			##################################
			
			# delete record in CCSS
			$sql = "DELETE FROM
						$eclass_db.CUSTOM_CNECC_SS_STUDENT
					WHERE
						RECORDID IN ($ccssRecordIdString)";
			$q_result = $q_result && $this->db->db_db_query($sql);
			}
			$postOsRecordIds = $this->getStudentCcssOsRecordIds();
			
			# use the pre and post os_recordId comparison to remove ole_student
			$removeIdArray = array();
			if (count($preOsRecordIds)>0) {
				foreach($preOsRecordIds as $key => $preId) {
					if (in_array($preId, $postOsRecordIds)) {
						// do nothing
					} else {
						$removeIdArray[] = $preId;
					} 
				}
				$q_result = $q_result && $this->removeOleStudent($removeIdArray);
			} else {
				// do nothing
			}
		}
		return $q_result;
	}

	///////////////// REPORT private functionS //////////////////
	function getWholeReportTemplate() {
		$Html = "<html>
				<body>
				<link href='report_style.css' rel='stylesheet' type='text/css'>
				<table width='100%' border = '0'>
					<!-- School Title and Info -->
					<tr>
						<td>{{{ SCHOOL TITLE AND INFO }}}</td>
					</tr>
					
					<!-- Student Info -->
					<tr>
						<td>{{{ STUDENT INFO }}}</td>
					</tr>
					
					<!-- Report Content -->
					<tr valign='top'>
						<td> {{{ REPORT CONTENT }}}</td>
					</tr>
					
					<!-- Remarks -->
					<tr>
						<td>{{{ REMARKS }}}</td>
					</tr>
					
					<!-- Signatures -->
					<tr>
						<td>{{{ SIGNATURES }}}</td>
					</tr>
				</table>
				<div style='page-break-after:always'>&nbsp;</div>
				<table width='100%' style='text-align: center;'><tr><td><img src='/home/portfolio/customize/cnecc/markingSchema.jpg' alt='Marking Schema' width='80%'/></td></tr></table>
				</body>
				<html>";
		return $Html;
	}
	private function getAcademicYearInfoByAcademicYearId($ParAcademicYearId=-1) {
		global $intranet_session_language;
		if ($ParAcademicYearId<0) {
			$academicYearName = getCurrentAcademicYear();
		} else {
			$sql = "SELECT
						YEARNAME".strtoupper($intranet_session_language)."
					FROM
						$intranet_db.ACADEMIC_YEAR
					WHERE
						ACADEMICYEARID = $ParAcademicYearId";
			$returnVector = $this->db->returnVector($sql);
			$academicYearName = $returnVector[0];
		}
		return $academicYearName;
	}
	
	function generateSchoolTitleAndInfo() {
		$Html = "";
		$element = array();
		$currentAcademicYear = $this->getAcademicYearInfoByAcademicYearId($this->academicYearId);
		$nextAcademicYear = $currentAcademicYear + 1;
		
		$element["SCHOOL_ICON"] = GET_SCHOOL_BADGE();
		$element["SCHOOL_NAME_ENG"] = "CNEC CHRISTIAN COLLEGE";
		$element["SCHOOL_NAME_CHI"] = "中華傳道會安柱中學";
		$element["REPORT_NAME_ENG"] = "Student's Other Learning Experience Record";
		$element["REPORT_NAME_CHI"] = "學生其他學習經歷紀錄";
		$element["SCHOOL_YEAR"] = "( " . $currentAcademicYear . "-" . substr($nextAcademicYear, -2) . " )";
		$element["ADDRESS"] = "6 Lei Pui Street,<br />Kwai Chung, N.T.,<br />Hong Kong.";
		$element["Tel"] = "852-24230365";
		$element["FAX"] = "852-24801429";
		
		$Html .= "<table width='100%'>
					<tr>
				
						<!-- School Icon -->
						<td>" . $element["SCHOOL_ICON"] . "</td>
				
						<!-- School Name and Report Name -->
						<td align='center'>" . 
						"<strong>" . $element["SCHOOL_NAME_ENG"] . "</strong><br />" . 
						$element["SCHOOL_NAME_CHI"] . "<br />" .  
						"<strong>" . $element["REPORT_NAME_ENG"] . "</strong><br />" .  
						$element["REPORT_NAME_CHI"] . "<br />" . $element["SCHOOL_YEAR"] . 
						"</td>
				
						<!-- School Contact Detail -->
						<td id='address'>" . 
						$element["ADDRESS"] . "<br />" . 
						$element["Tel"] . "<br />" . 
						$element["FAX"] . 
						"</td>
					</tr>
				</table>";
		return $Html;
	}
	
	function generateStudentInfo($ParIssueDate) {
		global $intranet_session_language, $Lang;
		
		include("lang/lang.en.php");
		$LangEN = $Lang["Cust_Cnecc"];
		include("lang/lang.b5.php");
		$LangB5 = $Lang["Cust_Cnecc"];
		
		include("lang/lang.$intranet_session_language.php");
				
		$Html = "";
		$Html = "<table id='student_info_table' width='100%'>
					<tr>
						<td width='90px'>" . $LangEN["NameOfStudent"] . "</td>
						<td width='80px' style='text-align:right'>" . $LangB5["NameOfStudent"] . "&nbsp; : </td>
						<td colspan='4'>" . $this->userInfo->EnglishName . "&nbsp;(" . $this->userInfo->ChineseName . ")" . "</td>
						<td width='70px'>" . $LangEN["RegNo"] . "</td>
						<td width='80px' style='text-align:right'>" . $LangB5["RegNo"] . "&nbsp; : </td>
						<td width='80px'>" . str_replace("#", "", $this->userInfo->WebSamsRegNo) . "</td>
					</tr>
					<tr>
						<td>" . $LangEN["Class&ClassNo"] . "</td>
						<td style='text-align:right;'>" . $LangB5["Class&ClassNo"] . " : </td>
						<td colspan='4'>" . $this->userInfo->ClassName . "&nbsp;&nbsp;&nbsp;(&nbsp;" . $this->userInfo->ClassNumber . "&nbsp;)</td>
						<td>" . $LangEN["HkidNo"] . "</td>
						<td style='text-align:right'>" . $LangB5["HkidNo"] . "&nbsp; : </td>
						<td>" . $this->userInfo->HKID . "</td>
					</tr>
					<tr>
						<td>" . $LangEN["DateOfBirth"] . "</td>
						<td style='text-align:right'>" . $LangB5["DateOfBirth"] . "&nbsp; : </td>
						<td width='100px;'>" . date("d/m/Y", strtotime($this->userInfo->DateOfBirth)) . "</td>
						<td width='30px;'>" . $LangEN["Sex"] . "</td>
						<td width='40px;' style='text-align:right'>" . $LangB5["Sex"] . "&nbsp; : </td>
						<td>" . $this->userInfo->Gender . "</td>
						<td>" . $LangEN["DateOfIssue"] . "</td>
						<td style='text-align:right'>" . $LangB5["DateOfIssue"] . "&nbsp; : </td>
						<td>" . $ParIssueDate . "</td>
					</tr>
				</table>";
		
		return $Html;
	}
	
	private function PutElementToOwnDefineCategoryArray(&$ParOwnDefineCategoryArray, $ParEInfo) {
		global $intranet_session_language;
		include("lang/lang.en.php");
		$LangEN = $Lang;
		include("lang/lang.b5.php");
		$LangB5 = $Lang;
		include("lang/lang.$intranet_session_language.php");
		
		switch ($ParEInfo["CATEGORYID"]) {
			case 1:
			case 2:
				$ParOwnDefineCategoryArray[$LangEN["Cust_Cnecc"]["OwnDefineCategory"]["ParticipationInActivities"] . " " . $LangB5["Cust_Cnecc"]["OwnDefineCategory"]["ParticipationInActivities"]][] = $ParEInfo;
				break;
			case 3:
				$ParOwnDefineCategoryArray[$LangEN["Cust_Cnecc"]["OwnDefineCategory"]["CoursesAndTrainingTaken"] . " " . $LangB5["Cust_Cnecc"]["OwnDefineCategory"]["CoursesAndTrainingTaken"]][] = $ParEInfo;
				break;
			case 4:
				$ParOwnDefineCategoryArray[$LangEN["Cust_Cnecc"]["OwnDefineCategory"]["Services"] . " " . $LangB5["Cust_Cnecc"]["OwnDefineCategory"]["Services"]][] = $ParEInfo;
				break;
			case 5:
				$ParOwnDefineCategoryArray[$LangEN["Cust_Cnecc"]["OwnDefineCategory"]["Awards"] . " " . $LangB5["Cust_Cnecc"]["OwnDefineCategory"]["Awards"]][] = $ParEInfo;
				break;
			default:
				$ParOwnDefineCategoryArray[$LangEN["Cust_Cnecc"]["OwnDefineCategory"]["Others"] . " " . $LangB5["Cust_Cnecc"]["OwnDefineCategory"]["Others"]][] = $ParEInfo;
				break;
		}
	}
	
	private function reorderDomainPoint($ParDomainPoint) {
		$returnDomainPoint = array();
		$domainNameOrder = array(
								"Aesthetic Development",
								"Physical Development",
								"Moral and Civic Education",
								"Community Service",
								"Career-related Experiences",
								"Intellectual Development",
								"Others"
								);
		
		foreach($domainNameOrder as $key => $eleName) {
			$returnDomainPoint[$eleName] = $ParDomainPoint[$eleName];
		}
		return $returnDomainPoint;
	}
	
	function generateReportContent($ParAchievementArray) {
		global $intranet_session_language;
		include("lang/lang.en.php");
		$LangEN = $Lang;
		include("lang/lang.b5.php");
		$LangB5 = $Lang;
		include("lang/lang.$intranet_session_language.php");
		#######################################################
		##	Start: Define Shells for Report Content
		#######################################################
		# construct content table shell
		$table = "<table id='report_content' width='100%'>
			      <tr>
			        <td>
			          {{{ CATEGORY_TABLE }}}
			        </td>
			      </tr>
			      <tr>
			        <td>
			          {{{ DOMAIN_TABLE }}}
			        </td>
			      </tr>
			    </table>";
			    
		# construct category table shell
		$categoryTable = "<table width='100%'>
			      {{{ CATEGORY_CONTENT }}}
			    </table>";
		
		# construct domain table shell
		$domainTable = "<table id='domainTable' width='80%'>
					<thead>
					<tr>
						<th>
							" . $LangEN["Cust_Cnecc"]["Domain"] . " " . $LangB5["Cust_Cnecc"]["Domain"] . "
						</th>
						<th nowrap='nowrap'>
							" . $LangEN["Cust_Cnecc"]["PointsAttained"] . " " . $LangB5["Cust_Cnecc"]["PointsAttained"] . "
						</th>
					</tr>
					</thead>
					<tbody>
					{{{ DOMAIN_CONTENT }}}
					</tbody>
				</table>";
		
		$sizeOfAchievement = count($ParAchievementArray);
		$achievementIndicator = $ParAchievementArray[$sizeOfAchievement-1];
		
		$ParAchievementArrayEN[] = $LangEN["Cust_Cnecc"][$achievementIndicator];
		$ParAchievementArrayB5[] = $LangB5["Cust_Cnecc"][$achievementIndicator];
//debug_r($ParAchievementArray); // fai
		if (count($ParAchievementArray)<1) {
			$domainTable .= "<br/>" . $LangEN["Cust_Cnecc"]["AwardAttained"] . ": " . $LangEN["Cust_Cnecc"]["NIL"];
			$domainTable .= "<br/>" . $LangB5["Cust_Cnecc"]["AwardAttained"] . ": " . $LangB5["Cust_Cnecc"]["NIL"];
		} else {
			$domainTable .= "<br/>" . $LangEN["Cust_Cnecc"]["AwardAttained"] . ": " . implode("/",$ParAchievementArrayEN);
			$domainTable .= "<br/>" . $LangB5["Cust_Cnecc"]["AwardAttained"] . ": " . implode("/",$ParAchievementArrayB5);
		}
		#######################################################
		##	End: Define Shells for Report Content
		#######################################################
		
		
		#######################################################
		##	Start: Contruct Contents Elements
		#######################################################
		$catergorizedResult = $this->getCategorizedResult();

		$libpf_slp = new libpf_slp();
		$CategoryArray = $libpf_slp->GET_OLR_CATEGORY();
		
		$tbody = "";
		$OwnDefineCategoryArray = array();
		$domainTotal = 0;
		foreach($catergorizedResult as $key => $info) {
			$domainName = $info["NAME"];
			$elements = $info["ELEMENTS"];
			$domainPoint[$domainName] = 0;
			if (count($elements)>0) {
				foreach($elements as $eKey => $eInfo) {
					if ($eInfo["SUCCESS"]) {
						# Categorize Elements in Participation in Activities, Courses and Training Taken, Services, Awards and Others
						$this->PutElementToOwnDefineCategoryArray($OwnDefineCategoryArray, $eInfo);
						
						# Calculate points for differenct domain
						$domainPoint[$domainName] += $eInfo["POINT"];
					} else {
						// do nothing
					}
				}
			}
			# Accumulate the total of all domains
			$domainTotal += $domainPoint[$domainName];
		}
		# Arrange the Domain order
		$domainPoint = $this->reorderDomainPoint($domainPoint);
		$domainPoint[$LangEN["Cust_Cnecc"]["Total"]] = $domainTotal;
		
		# Add the Chinese Name
		$eleEngChiMapping = $this->getEleEngChiMapping();
		$eleEngChiMapping[$LangEN["Cust_Cnecc"]["Total"]] = $LangB5["Cust_Cnecc"]["Total"];
		foreach($domainPoint as $key => $eInfo) {
			$domainContent .= "<tr><td>" . $key . " " . $eleEngChiMapping[$key] . "</td>" . "<td style='text-align: center;'>" . $eInfo . "</td></tr>";
		}
		$categoryContentTables = "";
		$OwnDefineCategoryArray = $this->arrangeCategory($OwnDefineCategoryArray);
		foreach($OwnDefineCategoryArray as $title => $element) {
			// contruct the category table here
			$categoryContentTables .= "
				<tr><td>
				<table width='100%'class='categoryTable' >
			      <tr>
			        <td></td>
			      </tr>
			      <tr>
			        <th>
			          $title
			        </th>
			      </tr>";
			 foreach($element as $key => $program) {
			 	$categoryContentTables .= "<tr><td>" . $program["TITLE"] . "</td></tr>";
			 }
			$categoryContentTables .= "</table>
				</td></tr>";
		}

		$categoryContentTables .= "<tr style='page-break-before:always'><td><br />
				<span class='attainment_wpds'>" . $LangEN["Cust_Cnecc"]["AttainmentOfCnecWPDS"] . "*<br />" . $LangB5["Cust_Cnecc"]["AttainmentOfCnecWPDS"] . "</span>				
				<br /><br /></td></tr>"; 
		#######################################################
		##	End: Contruct Contents Elements
		#######################################################
		
		
		#######################################################
		##	Start: Put Elements into Table Shells
		#######################################################
		$categoryTable = str_replace("{{{ CATEGORY_CONTENT }}}", $categoryContentTables, $categoryTable);
		$domainTable = str_replace("{{{ DOMAIN_CONTENT }}}", $domainContent, $domainTable);
		$Html = "";
		$Html = str_replace(array("{{{ CATEGORY_TABLE }}}","{{{ DOMAIN_TABLE }}}"),array($categoryTable,$domainTable),$table);
		#######################################################
		##	End: Put Elements into Table Shells
		#######################################################
		return $Html;
	}
	
	private function arrangeCategory($ParOwnDefineCategoryArray) {
		global $intranet_session_language;
		include("lang/lang.en.php");
		$LangEN = $Lang;
		include("lang/lang.b5.php");
		$LangB5 = $Lang;
		include("lang/lang.$intranet_session_language.php");

		# put the category in the orders you want in this array
		$order = array(
						$LangEN["Cust_Cnecc"]["OwnDefineCategory"]["ParticipationInActivities"] . " " . $LangB5["Cust_Cnecc"]["OwnDefineCategory"]["ParticipationInActivities"],
						$LangEN["Cust_Cnecc"]["OwnDefineCategory"]["CoursesAndTrainingTaken"] . " " . $LangB5["Cust_Cnecc"]["OwnDefineCategory"]["CoursesAndTrainingTaken"],
						$LangEN["Cust_Cnecc"]["OwnDefineCategory"]["Services"] . " " . $LangB5["Cust_Cnecc"]["OwnDefineCategory"]["Services"],
						$LangEN["Cust_Cnecc"]["OwnDefineCategory"]["Awards"] . " " . $LangB5["Cust_Cnecc"]["OwnDefineCategory"]["Awards"],
						$LangEN["Cust_Cnecc"]["OwnDefineCategory"]["Others"] . " " . $LangB5["Cust_Cnecc"]["OwnDefineCategory"]["Others"]
						);

		$returnArray = array();
		
		foreach($order as $key => $categoryName) {
			if (isset($ParOwnDefineCategoryArray[$categoryName])) {
				$returnArray[$categoryName] = $ParOwnDefineCategoryArray[$categoryName];
			} else {
				// do nothing
			}
		}
		return $returnArray;
	}
	
	private function getEleEngChiMapping() {
		global $eclass_db;
		$sql = "SELECT ENGTITLE, CHITITLE, DEFAULTID FROM {$eclass_db}.OLE_ELE";
		$resultArray = $this->db->returnArray($sql);
		foreach($resultArray as $key => $element) {
			$returnArray[$element["ENGTITLE"]] = $element["CHITITLE"];	
		}
		return $returnArray;
	}
	
	
	function generateRemarks() {
		global $intranet_session_language;
		include("lang/lang.en.php");
		$LangEN = $Lang;
		include("lang/lang.b5.php");
		$LangB5 = $Lang;
		include("lang/lang.$intranet_session_language.php");
		
		$Html = "<p class='remark'>*" . $LangEN["Cust_Cnecc"]["CriteriaContent"] . "<br />*" . $LangB5["Cust_Cnecc"]["CriteriaContent"] . "</p>";
		return $Html;
	}
	
	function generateSignatures() {
		global $Lang, $intranet_session_language;
		
		include("lang/lang.en.php");
		$LangEN = $Lang["Cust_Cnecc"];
		include("lang/lang.b5.php");
		$LangB5 = $Lang["Cust_Cnecc"];
		
		include("lang/lang.$intranet_session_language.php");
		
		$classTeacherVector = $this->getClassTeacher($this->studentId);
		$classTeacherDisplay = "(" . implode(")<br />(", $classTeacherVector) . ")";

		$Html = "";
		$Html = "<table id='signature_table' width='100%'>
					<tr>
						<td width='30%' style='border-bottom: 1px solid;'><br /><br /><br />&nbsp;</td>
						<td width='5%'>&nbsp;</td>
						<td width='30%' style='border-bottom: 1px solid;'><br /><br /><br />&nbsp;</td>
						<td width='5%'>&nbsp;</td>
						<td width='30%' style='border-bottom: 1px solid;'><br /><br /><br />&nbsp;</td>
					</tr>
					<tr style='text-align:center;'>
						<td width='30%'>" . $LangEN["Parent/Guardian"] . "&nbsp;&nbsp;&nbsp;" . $LangB5["Parent/Guardian"] . "</td>
						<td width='5%'>&nbsp;</td>
						<td width='30%'>" . $LangEN["ClassTeacher"] . "&nbsp;&nbsp;&nbsp;" . $LangB5["ClassTeacher"] . "</td>
						<td width='5%'>&nbsp;</td>
						<td width='30%'>" . $LangEN["Principal"] . "&nbsp;&nbsp;&nbsp;" . $LangB5["Principal"] . "</td>
					</tr>
					<tr style='text-align:center;' valign='top'>
						<td width='30%'>&nbsp;</td>
						<td width='5%'>&nbsp;</td>
						<td width='30%'>$classTeacherDisplay</td>
						<td width='5%'>&nbsp;</td>
						<td width='30%'>(" . $Lang["Cust_Cnecc"]["Mr.NgaiShuChiu"] . ")</td>
					</tr>
				</table>";
		return $Html;
	}
	
	private function getClassTeacher() {
		global $intranet_db;
		$returnVector = array();
		$sql = "SELECT
					YCT.USERID
				FROM
					$intranet_db.INTRANET_USER IU
					INNER JOIN $intranet_db.YEAR_CLASS_USER YCU
						ON YCU.USERID = IU.USERID
					INNER JOIN $intranet_db.YEAR_CLASS YC
						ON YCU.YEARCLASSID = YC.YEARCLASSID
					INNER JOIN $intranet_db.YEAR_CLASS_TEACHER YCT
						ON YC.YEARCLASSID = YCT.YEARCLASSID
					INNER JOIN $intranet_db.ACADEMIC_YEAR AY
						ON YC.ACADEMICYEARID = AY.ACADEMICYEARID
				WHERE
					AY.ACADEMICYEARID = $this->academicYearId
					AND
					IU.USERID = $this->studentId
					AND YCT.USERID <> 2481"; // 2481 is the user broadlearning(t)
		//	debug_r($sql);die;
		$returnVector = $this->db->returnVector($sql);
		
		if (count($returnVector) > 0) {
			$classTeacherIds = implode(",",$returnVector);
			$nameField = getNameFieldByLang("IU.");
			$sql = "SELECT
						$nameField AS CLASSTEACHER
					FROM
						$intranet_db.INTRANET_USER IU
					WHERE
						IU.USERID IN ($classTeacherIds)";
//			debug_r($sql);
			$returnVector = $this->db->returnVector($sql);
		} else {
			$returnVector = array();
		}
		return $returnVector;
	}
	
	private function packDateOfIssue($ParUserInfoArray) {
		$sizeOfUserInfoArray = count($ParUserInfoArray);
		if ($sizeOfUserInfoArray>0) {
			$ParUserInfoArray[0][$sizeOfUserInfoArray] = $ParUserInfoArray[0]["DATE_OF_ISSUE"] = date("d/m/Y");
		}
		return $ParUserInfoArray;
	}
	
	
	private function getUserInfo() {
		global $intranet_db;
		$returnArray = array();
		$sql = "SELECT
					IU.ENGLISHNAME,
					IU.CHINESENAME,
					YC.CLASSTITLEEN,
					YC.CLASSTITLEB5,
					YCU.CLASSNUMBER,
					IU.DATEOFBIRTH,
					IU.GENDER,
					IU.WEBSAMSREGNO,
					IU.HKID
				FROM
					$intranet_db.INTRANET_USER IU
					INNER JOIN $intranet_db.YEAR_CLASS_USER YCU
						ON YCU.USERID = IU.USERID
					INNER JOIN $intranet_db.YEAR_CLASS YC
						ON YCU.YEARCLASSID = YC.YEARCLASSID
					INNER JOIN $intranet_db.ACADEMIC_YEAR AY
						ON YC.ACADEMICYEARID = AY.ACADEMICYEARID
				WHERE IU.USERID = " . $this->studentId . " AND AY.ACADEMICYEARID = " . $this->academicYearId;
		$returnArray = $this->db->returnArray($sql);
		return $returnArray;
	}
	
	function isSunshineProgram($ParProgramId=-1) {
		global $eclass_db;
		if ($ParProgramId<0) {
			return false;
		} else {
			$sql = "SELECT
						COUNT(EVENTID)
					FROM
						$eclass_db.CUSTOM_CNECC_SS_EVENT
					WHERE
						PROGRAMID=$ParProgramId
					GROUP BY PROGRAMID";

			$returnVector = $this->db->returnVector($sql);

			$count = $returnVector[0];
			if ($count>0) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	private function getSunshineProgramInfo($ParProgramId=-1) {
		global $eclass_db;
		$returnArray = array();
		if ($ParProgramId>0) {
			$sql = "SELECT
						EVENTID,
						ATTAINMENT,
						ROLE,
						POINT
					FROM
						$eclass_db.CUSTOM_CNECC_SS_EVENT
					WHERE
						PROGRAMID = $ParProgramId
						AND
						STATUS = " . self::ENABLED;
			$returnArray = $this->db->returnArray($sql);
		} else {
			// do nothing
		}
		return $returnArray;
	}
	
	function getResultDisplayCustomization($ParProgramId=-1) {
		global $Lang, $intranet_session_language;
		$sunshineProgramInfo = $this->getSunshineProgramInfo($ParProgramId);
		
		$sizeOfSsPgmInfo = count($sunshineProgramInfo);
		if ($sizeOfSsPgmInfo>0) {
			foreach($sunshineProgramInfo as $key => $element) {
				$infoDisplay .= "<tr><td>" . $element["ATTAINMENT"] . "</td><td>" . $Lang["Cust_Cnecc"][$element["ROLE"]]["label"] . "</td><td>" . $element["POINT"] . "</td></tr>";
			}
		} else {
			// do nothing
		}
		$infoDisplay = "<table><tr><td>" . $Lang["Cust_Cnecc"]["Attainment"] . "</td><td>" . $Lang["Cust_Cnecc"]["Role"] . "</td><td>" . $Lang["Cust_Cnecc"]["Point(s)"] . "</td></tr>".$infoDisplay."</table>";
		
		
		$Html = "";
		$Html .= "<tr valign='top'>
						<td valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>" . $Lang["Cust_Cnecc"]["SunshineProgramInfo"] . "</span></td>
						<td>$infoDisplay</td>
					</tr>";
		return $Html;
	}
	
	function getOleNewCustomization($ParProgramId=-1, $ParStudentSpecific=false) {
		global $ec_iPortfolio, $intranet_session_language, $Lang;
		
		$sunshineProgramInfo = array();
		if ($ParStudentSpecific) {
			$ssSelectHidden = "<td><input id='ss_yes' name='setSunshine' value='1' type='hidden' /></td><td></td>";
			$isSsHidden = true;
		}
		
		if ($this->isSunshineProgram($ParProgramId) || $ParStudentSpecific) {
			$ss_yes_check = " checked='checked' ";
			$sunshineProgramInfo = $this->getSunshineProgramInfo($ParProgramId);
		} else {
			$ss_no_check = " checked='checked' ";
		}

		$sizeOfSsPgmInfo = count($sunshineProgramInfo);
		$setSunshineOptions = "";
		if ($sizeOfSsPgmInfo<1) {
			$createMultipleHtmlOptions = "createSunshineOption();\n";
		} else {
			for($i=0;$i<$sizeOfSsPgmInfo;$i++) {
				$createMultipleHtmlOptions .= "createSunshineOption('" . $sunshineProgramInfo[$i]["EVENTID"] . "');\n";
			}
		}
		if ($sizeOfSsPgmInfo>0) {
			for($i=0;$i<$sizeOfSsPgmInfo;$i++) {
				$setSunshineOptions .= "\$('input[name=cnecc_event_info_A" . $sunshineProgramInfo[$i]["EVENTID"] . "\\[\\]]').get(0).value='" . $sunshineProgramInfo[$i]["ATTAINMENT"] . "';\n";
				$setSunshineOptions .= "\$('select[name=cnecc_event_info_R" . $sunshineProgramInfo[$i]["EVENTID"] . "\\[\\]]').get(0).value='" . $sunshineProgramInfo[$i]["ROLE"] . "';\n";
				$setSunshineOptions .= "\$('input[name=cnecc_event_info_P" . $sunshineProgramInfo[$i]["EVENTID"] . "\\[\\]]').get(0).value='" . $sunshineProgramInfo[$i]["POINT"] . "';\n";
			}
		} else {
			// do nothing
		}
		
		$roleValues = array();
		$roleLabels = array();
		foreach($this->roles as $key=>$roleElement) {
			$roleValues[] = $roleElement["value"];
			$roleLabels[] = $roleElement["label"];
		}
		$availableRolesValue = ConvertToJSArray($roleValues, "availableRolesValue");
		$availableRolesLabel = ConvertToJSArray($roleLabels, "availableRolesLabel");

		$Html = $availableRolesValue.$availableRolesLabel
				."<script language='javascript'>
					var newEventCount = 0;
					\$(document).ready(function(){
						$createMultipleHtmlOptions$setSunshineOptions
						var sunshineSet = \$('input[name=setSunshine]:checked').val();
						" . ($isSsHidden?"sunshineSet=1;":"") . "
						if (sunshineSet == 1) {
								setSunshineSetting(sunshineSet);
						}
					});
					function getRoleSelection(eventId) {
						
						var selectOptions = '';
						for(i=0;i<availableRolesValue.length;i++) {
							selectOptions = selectOptions + '<option value=\"'+availableRolesValue[i]+'\">'+availableRolesLabel[i]+'</option>'; 
						}
						selectOptions = '<select name=\"cnecc_event_info_R'+eventId+'[]\">'+selectOptions+'</select>';
						return selectOptions;
					}
					function createSunshineOption(eventId) {
						if (eventId == null || eventId == '') {
							newEventCount = newEventCount + 1;
							eventId = '_N'+newEventCount;							
						}
						var selectString = getRoleSelection(eventId);
						\$('#sunshineInput').append('<tr><td><input type=\"text\" name=\"cnecc_event_info_A'+eventId+'[]\" /></td><td>'+selectString+'</td><td><input type=\"text\" name=\"cnecc_event_info_P'+eventId+'[]\" value=\"1\" /></td><td>".($ParStudentSpecific?"&nbsp;":"<a href=\"javascript: void(0);\" class=\"tablelink\" onClick=\"\$(this).parent().parent().remove();\">X</a>")."</td></tr><input type=\"hidden\" name=\"cnecc_event_id[]\" value=\"'+eventId+'\" />');
					}

					function setSunshineSetting(on) {
						if (on) {
							\$('input:radio[name=allowStudentsToJoin]').val(0).attr('checked','checked');
							\$('#joinSelectField').hide(); \$('#joinOptions').hide();
							\$('#sunshineOption').show();

//// comment: this jquery do not work for IE7
////							\$('form[name=form1]').attr('onsubmit', 'return checkForSunshineSetting(this)');

//// comment: instead use the DOM to set and bind the submit function
							document.form1.onsubmit = 'return checkForSunshineSetting(this)';

							\$('form[name=form1]').unbind('submit');
							\$('form[name=form1]').submit(function() {
								return checkForSunshineSetting(\$(this).get(0));
							});
						} else {
							\$(\"#joinSelectField\").show();
							\$('#sunshineOption').hide();

//// comment: this jquery do not work for IE7
////							\$('form[name=form1]').attr('onSubmit', 'return checkform(this);');
							document.form1.onsubmit = 'return checkform(this)';

							\$('form[name=form1]').unbind('submit');
							\$('form[name=form1]').submit(function() {
								return checkform(\$(this).get(0));
							});
						}
					}

					function checkForSunshineSetting(obj) {

						if (!checkform(obj)) return false;

						try {
							var checkedEle = \$('input[name=ele\\[\\]]:checked').length;
							" . ($ParStudentSpecific?"":"
							if (checkedEle!=1) {
								throw '" . $Lang["Cust_Cnecc"]["PleaseChooseExactlyOneOption"] . "';
							}
							"). "
							var attainment = \$('input[name*=cnecc_event_info_A]');
							var role = \$('input[name*=cnecc_event_info_R]');
							var point = \$('input[name*=cnecc_event_info_P]');

							var sunshineSet = \$('input[name=setSunshine]:checked').val();

							if (sunshineSet == 1 && attainment.length<1) {
								throw '" . $Lang["Cust_Cnecc"]["PleaseEnterSunshineProgramInfo"] . "';
							}


							\$.each(attainment, function(key, inputElement){
								if (inputElement.value == '') {
									throw '" . $Lang["Cust_Cnecc"]["Attainment(s)Empty"] . "';
								}
							});
	
							\$.each(role, function(key, inputElement){
								if (inputElement.value == '') {
									throw '" . $Lang["Cust_Cnecc"]["Role(s)Empty"] . "';
								}
							});

							\$.each(point, function(key, inputElement){
								if (inputElement.value == '') {
									throw '" . $Lang["Cust_Cnecc"]["Point(s)Empty"] . "';
								} else if (isNaN(inputElement.value)) {
									throw '" . $Lang["Cust_Cnecc"]["Point(s)ShouldBeANumber"] . "';
								}
							});

						} catch (errMsg) {
							alert(errMsg);
							return false;
						}						
						\$('input:radio[name=allowStudentsToJoin]').val(0).attr('checked','checked');
						return true;
					}
				</script>
				
				<!-- Sunshine Program -->
				<tr>" . ($ParStudentSpecific?$ssSelectHidden:"
					<td valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><div id='div_sunshine'>" . $Lang["Cust_Cnecc"]["SetAsSunshineProgram"] . "</div></span></td>
					<td>
						<label for='ss_yes'>" . $ec_iPortfolio['SLP']['Yes']. "</label><input id='ss_yes' name='setSunshine' value='1' type='radio' $ss_yes_check onClick='javascript: setSunshineSetting(1)' ".($ParStudentSpecific?" disabled='disabled' ":"")."/>
						<label for='ss_no'>" . $ec_iPortfolio['SLP']['No'] . "</label><input id='ss_no' name='setSunshine' value='0' type='radio' $ss_no_check onClick='javascript: setSunshineSetting(0)' ".($ParStudentSpecific?" disabled='disabled' ":"")."/>
					</td>
						")."
				</tr>
				<tr id='sunshineOption' style='display: none;'>
					<td>&nbsp;</td>
					<td>
						<table class='tablerow2' align='center' border='0' cellpadding='5' cellspacing='0' width='100%'>
							<thead>
							<tr>
								<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>" . $Lang["Cust_Cnecc"]["Attainment"] . "</td>
								<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>" . $Lang["Cust_Cnecc"]["Role"] . "</td>
								<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>" . $Lang["Cust_Cnecc"]["Point(s)"] . "</td>
								<td width='10%' valign='top' nowrap='nowrap' class='formfieldtitle'>".($ParStudentSpecific?"&nbsp;":"<a href='javascript: void(0);' class='tablelink' onClick='createSunshineOption()'>+</a>") . "</td>
							</tr>
							</thead>
							<tbody id='sunshineInput'>
							</tbody>
						</table>
					</td>
				</tr>";
		return $Html;
	}
	
	function removeCcseProgram($ParProgramID=-1){
		global $eclass_db;
		
		$sql = "DELETE FROM
					$eclass_db.CUSTOM_CNECC_SS_EVENT
				WHERE
					PROGRAMID = $ParProgramID";
		$this->db->db_db_query($sql);
	}
	
	function saveCcseProgram($ParProgramID, $ParInsertArray, $ParUpdateArray, $ParUserID=-1) {
		global $eclass_db;
		$sizeofInsertArray = count($ParInsertArray);
		$sizeofUpdateArray = count($ParUpdateArray);
		$queryResult1 = true;
		$queryResult2 = true;
//		debug_r($ParInsertArray);die;
		# INSERT
		for($i=0;$i<$sizeofInsertArray;$i++) {			
			$insertElement = $ParInsertArray[$i];

			$attainment = $insertElement["ATTAINMENT"];
			$role = $insertElement["ROLE"];
			$point = $insertElement["POINT"];
			
			$sql = "INSERT INTO $eclass_db.CUSTOM_CNECC_SS_EVENT
					SET
						PROGRAMID = $ParProgramID,
						ATTAINMENT = '" . $attainment . "',
						ROLE = '" . $role . "',
						POINT = " . $point . ",
						SPECIFIEDSTUDENTID=" . ($ParUserID>-1?$ParUserID:"NULL") . ",
						STATUS = 1,
						INPUTDATE = NOW(),
						MODIFIEDDATE = NOW()";
			$queryResult1 = $this->db->db_db_query($sql);
		}
		
		# UPDATE AND DELETE
		foreach($ParUpdateArray as $key => $elements) {
			
			$attainment = $elements["ATTAINMENT"];
			$role = $elements["ROLE"];
			$point = $elements["POINT"];
			
			if (empty($attainment)) {
				# DELETE
				$sql = "DELETE FROM
							$eclass_db.CUSTOM_CNECC_SS_EVENT
						WHERE
							EVENTID=".$key;
			} else {
				# UPDATE
				$sql = "UPDATE
							$eclass_db.CUSTOM_CNECC_SS_EVENT
						SET
							ATTAINMENT = '" . $attainment . "',
							ROLE = '" . $role . "',
							POINT = " . $point . ",
							MODIFIEDDATE = NOW()
						WHERE
							EVENTID=".$key;
			}
//			debug_r($sql);die;	
			$queryResult2 = $this->db->db_db_query($sql);
		}
//		debug_r($queryResult1);
//		debug_r($queryResult2);die;
		return ($queryResult1&&$queryResult2);
	}
	
	private function getPrevAcademicYearId($ParCurrentAcademicYearId) {
		global $intranet_db;
		$sql = "SELECT
					AY1.ACADEMICYEARID
				FROM
					ACADEMIC_YEAR AY1 LEFT JOIN ACADEMIC_YEAR AY2
					ON AY1.SEQUENCE = AY2.SEQUENCE - 1
				WHERE AY2.ACADEMICYEARID = $ParCurrentAcademicYearId
					";
		$returnVector = $this->db->returnVector($sql);
//		debug_r($sql);
		$returnAcademicYearId = $returnVector[0];
		return $returnAcademicYearId;
	}
	

	//get the whole list for the sunshine scheme for the student recusively by year , store the result in $ParResultArray (by by reference)
	//return a full list , no matter the student the student has join it or not
	private function getStudentRecordsInYears($ParBaseAcademicYearId, $ParCurrentAcademicYearId, &$ParResultArray=null) {
		# initialize result array
		if (isset($ParResultArray)) {
			// do nothing
		} else {
			$ParResultArray = array();
		}
		# base case
		if ($ParCurrentAcademicYearId == null || $ParCurrentAcademicYearId < $ParBaseAcademicYearId) {
			return $ParResultArray;
		} else {
			$prevAcademicYearId = $this->getPrevAcademicYearId($ParCurrentAcademicYearId);
			$this->getStudentRecordsInYears($ParBaseAcademicYearId, $prevAcademicYearId, $ParResultArray);
			$libpf_slp_cnecc = new libpf_slp_cnecc($this->studentId, $ParCurrentAcademicYearId);
			$ParResultArray[$ParCurrentAcademicYearId] = $libpf_slp_cnecc->getCategorizedResult();
		}
		return $ParResultArray;		 
	}
	
	function getFormAwardLimitation() {
		$limitation = array(
							1=>"BasicCompetency",
							2=>"AwardOfAchievement",
							3=>"AwardOfExcellence"
							);
		return $limitation;
	}
	
	function getAchievementStandard() {
		global $Lang;
		$achievementStandard = array(
								"Basic_Competency" => array(
														"CS_Domain_Min._Point"=> 2,
														"Other_Domain_Min._Point" => 1,
														"Min._Occupation_in_Other_Domain" => 4,
														"Min._Score_in_Other_Domain" => 6,
														"Must_have_Role" => $Lang["Cust_Cnecc"]["Participant"]["value"]
														),
								"Award_of_Achievement" => array(
														"CS_Domain_Min._Point" => 2,
														"Other_Domain_Min._Point" => 1,
														"Min._Occupation_in_Other_Domain" => 4,
														"Min._Score_in_Other_Domain" => 6,
														"Must_have_Role" => $Lang["Cust_Cnecc"]["Helper"]["value"]
														),
								"Award_of_Excellence" => array(
														"CS_Domain_Min._Point" => 2,
														"Other_Domain_Min._Point" => 1,
														"Min._Occupation_in_Other_Domain" => 4,
														"Min._Score_in_Other_Domain" => 6,
														"Must_have_Role" => $Lang["Cust_Cnecc"]["Organizer"]["value"]
														),
								);
		return $achievementStandard;
	}
	
	private function calculateAchievement($ParStudentRecordsInYears) {
		global $Lang;
		$achievementStandard = $this->getAchievementStandard();
		
		$Achieved_BC = array("achievedAcademicYearId"=>-1, "isAchieved"=>false);
		$Achieved_AA = array("achievedAcademicYearId"=>-1, "isAchieved"=>false);
		$Achieved_AE = array("achievedAcademicYearId"=>-1, "isAchieved"=>false);
//debug_r($ParStudentRecordsInYears);
		foreach($ParStudentRecordsInYears as $keyAcademicYearId =>$yearElement) {

//error_log("Year ID is ".$keyAcademicYearId."\n", 3, "/tmp/aaa.txt"); //fai
			$Domain["[CS]"] = 0;
			$Domain["[ID]"] = 0;
			$Domain["[MCE]"] = 0;
			$Domain["[PD]"] = 0;
			$Domain["[AD]"] = 0;
			$Domain["[CE]"] = 0;
			$Domain["[OTHERS]"] = 0;
			
			$Role[$Lang["Cust_Cnecc"]["Participant"]["value"]] = 0;
			$Role[$Lang["Cust_Cnecc"]["Helper"]["value"]] = 0;
			$Role[$Lang["Cust_Cnecc"]["Organizer"]["value"]] = 0;
	
			foreach($yearElement as $key => $domain) {

					$domainElement = $domain["ELEMENTS"];
					if (isset($domainElement)) {
						foreach($domainElement as $elementKey => $element) {
							if ($element["SUCCESS"])
							{
//error_log("key == ".$key." point == ".$element["POINT"]."\n", 3, "/tmp/aaa.txt"); 
//								echo "success -> $key : ".$element["POINT"]."<br/>";
								$Domain[$key] += $element["POINT"];
								$Role[$element["ROLE"]]++;
							}
						}
					}
			}

//error_log("Achieved_BC == ".$Achieved_BC["isAchieved"]." Achieved_AA == ".$Achieved_AA["isAchieved"]." Achieved_AE == ".$Achieved_AE["isAchieved"]."\n", 3, "/tmp/aaa.txt"); 
//error_log("Participant == ".$Role[$Lang["Cust_Cnecc"]["Participant"]["value"]]."\n", 3, "/tmp/aaa.txt"); //fai
//error_log("Helper == ".$Role[$Lang["Cust_Cnecc"]["Participant"]["Helper"]]."\n", 3, "/tmp/aaa.txt"); //fai
//error_log("Organizer == ".$Role[$Lang["Cust_Cnecc"]["Participant"]["Organizer"]]."\n", 3, "/tmp/aaa.txt"); //fai
			if (!$Achieved_BC["isAchieved"]) {
				if ($Role[$Lang["Cust_Cnecc"]["Participant"]["value"]] < 1) {
//error_log("break from Participant\n", 3, "/tmp/aaa.txt"); //fai
					continue;
				}
			}
			else if (!$Achieved_AA["isAchieved"]) {
				if ($Role[$Lang["Cust_Cnecc"]["Helper"]["value"]] < 1) {
//error_log("break from Helper\n", 3, "/tmp/aaa.txt"); //fai
					continue;
				}
			}
			else if (!$Achieved_AE["isAchieved"]) {
				if ($Role[$Lang["Cust_Cnecc"]["Organizer"]["value"]] < 1) {
//error_log("break from Organizer\n", 3, "/tmp/aaa.txt"); //fai
					continue;
				}
			}

			if (!$Achieved_BC["isAchieved"]) {	// Basic_Competency
				$requirement = $achievementStandard["Basic_Competency"];
				$isAchieveScore = $this->compareWithStandardScore($requirement, $Domain, $Role);

				if ($isAchieveScore) {
					$Achieved_BC["achievedAcademicYearId"] = $keyAcademicYearId;
					$Achieved_BC["isAchieved"] = true;
//error_log("Achieved_BC is true\n", 3, "/tmp/aaa.txt"); //fai
				}

			}
			if ($Achieved_BC["isAchieved"] && !$Achieved_AA["isAchieved"]) {	// Award_of_Achievement
				$requirement = $achievementStandard["Award_of_Achievement"];
//foreach($Domain as $a =>$b){
//	error_log($a.'--AA---'.$b."\n", 3, "/tmp/aaa.txt"); //fai
//}
				$isAchieveScore = $this->compareWithStandardScore($requirement, $Domain, $Role);
//foreach($Domain as $a =>$b){
//	error_log($a.'--AA FF ---'.$b."\n", 3, "/tmp/aaa.txt"); //fai
//}
				if ($isAchieveScore) {
					$Achieved_AA["achievedAcademicYearId"] = $keyAcademicYearId;
					$Achieved_AA["isAchieved"] = true;
//error_log("Achieved_AA is true\n", 3, "/tmp/aaa.txt"); //fai
				}
			}

//debug_r($Achieved_AA);
			if ($Achieved_AA["isAchieved"]) {	// Award_of_Excellence
				$requirement = $achievementStandard["Award_of_Excellence"];
//foreach($Domain as $a =>$b){
//	error_log($a.'--AE---'.$b."\n", 3, "/tmp/aaa.txt"); //fai
//}
				$isAchieveScore = $this->compareWithStandardScore($requirement, $Domain, $Role);
				if ($isAchieveScore) {
					$Achieved_AE["achievedAcademicYearId"] = $keyAcademicYearId;
					$Achieved_AE["isAchieved"] = true;
//error_log("Achieved_AE is true\n", 3, "/tmp/aaa.txt"); //fai
				}else{
//error_log("Achieved_AE is false\n", 3, "/tmp/aaa.txt"); //fai
				}
			}
		}
//		if ($Achieved_BC["isAchieved"] && $Achieved_BC["achievedAcademicYearId"] == $this->academicYearId) {
//			$returnArray[] = $Lang["Cust_Cnecc"]["BasicCompetency"];
//		}
//		if ($Achieved_AA["isAchieved"] && $Achieved_AA["achievedAcademicYearId"] == $this->academicYearId) {
//			$returnArray[] = $Lang["Cust_Cnecc"]["AwardOfAchievement"];
//		}
//		if ($Achieved_AE["isAchieved"] && $Achieved_AE["achievedAcademicYearId"] == $this->academicYearId) {
//			$returnArray[] = $Lang["Cust_Cnecc"]["AwardOfExcellence"];
//		}

//error_log("BC ==>".$Achieved_BC["achievedAcademicYearId"]."\n", 3, "/tmp/aaa.txt"); //fai
//error_log("AA ==>".$Achieved_AA["achievedAcademicYearId"]."\n", 3, "/tmp/aaa.txt");
//error_log("AC ==>".$Achieved_AE["achievedAcademicYearId"]."\n", 3, "/tmp/aaa.txt");
//error_log("Year ==>".$this->academicYearId."\n", 3, "/tmp/aaa.txt");
//debug_r($Achieved_BC);
//debug_r($Achieved_AA);
//debug_r($Achieved_AE);
		$awardArray = array();
		if ($Achieved_BC["isAchieved"] && $Achieved_BC["achievedAcademicYearId"] == $this->academicYearId) {
			$awardArray[] = "BasicCompetency";
		}
		if ($Achieved_AA["isAchieved"] && $Achieved_AA["achievedAcademicYearId"] == $this->academicYearId) {
			$awardArray[] = "AwardOfAchievement";
		}
		if ($Achieved_AE["isAchieved"] && $Achieved_AE["achievedAcademicYearId"] == $this->academicYearId) {
			$awardArray[] = "AwardOfExcellence";
		}
//debug_r($awardArray);		
		#######################################################
		# form limitation in the award achieved
		$form = trim(substr($this->userInfo->ClassName,0,-1));
		$limitation = $this->getFormAwardLimitation();
//debug_r($limitation[$form]);
//error_log("limitation ==>".$limitation[$form]."\n", 3, "/tmp/aaa.txt");
//for($zz = 0,$zz_max = sizeof($awardArray);$zz < $zz_max;$zz++){
//	error_log("award ".$zz." ==>".$awardArray[$zz]."\n", 3, "/tmp/aaa.txt");
//}
		if (in_array($limitation[$form],$awardArray)) {
			$returnArray = array($limitation[$form]);
		} else {
//			$returnArray = array();
//error_log("size ==>".sizeof($awardArray)."\n", 3, "/tmp/aaa.txt");
			if(sizeof($awardArray) >0){
				$returnArray = array($awardArray[sizeof($awardArray)-1]);
			}else{
				$returnArray= array();
			}
//			$returnArray = array($awardArray[sizeof($awardArray)-1]);
		}
		#######################################################
		
		
		return $returnArray;
	}
	
	private function compareWithStandardScore($requirement, &$Domain, &$Role) {
//		error_log("\n\n\n \n", 3, "/tmp/aaa.txt");
		arsort($Domain);
		$flag_Role = false;
		$flag_CsMinPoint = false;
		$flag_OtherDomainSuccess = false;
		$flag_MinScore = false;
//debug_r($requirement);
//debug_r($Domain);
//debug_r($Role);
//echo "<hr/>";
		# validating $flag_Role
		if ($Role[$requirement["Must_have_Role"]] > 0) {
			$flag_Role = true;
		} else {
			return false;
		}

		# validating $flag_CsMinPoint
//error_log(" Domain[\"[CS]\"]==>".$Domain["[CS]"] ." and CS_Domain_Min._Point -->".$requirement["CS_Domain_Min._Point"]."\n", 3, "/tmp/aaa.txt");
		if ($Domain["[CS]"] >= $requirement["CS_Domain_Min._Point"]) {

			$flag_CsMinPoint = true;
//			$Domain["[CS]"] = $Domain["[CS]"] - $requirement["CS_Domain_Min._Point"];  // fai comment 20110622, don't minus this , don't know why should minus in the previous version
//error_log(" Domain[\"[CS]\"]==>".$Domain["[CS]"] ." larger than CS_Domain_Min._Point -->".$requirement["CS_Domain_Min._Point"]." is true\n", 3, "/tmp/aaa.txt");
//error_log(" update to ".$Domain["[CS]"]." by minus ".$requirement["CS_Domain_Min._Point"]."\n", 3, "/tmp/aaa.txt");
		} else {
			return false;
		}

		# validating $flag_OtherDomainSuccess and $flag_MinScore
		$noPointsOtherDomains = 0;
		$sumOfDomainPoints = 0;
		$count = 0;	// use to minus domain point
		$sizeOfDomainExcludingCs = count($Domain)-1;
		
		foreach($Domain as $eleId => &$domainPoint) {
			if ($eleId == "[CS]") {
				continue;
			} else {
				if ($domainPoint < 1) {
					$noPointsOtherDomains++;
				} else {
					$sumOfDomainPoints += $domainPoint;
					$domainPoint -= $requirement["Other_Domain_Min._Point"];	// --> 1.1 for domain that is valued, minus the min point
					$count++;
				}		 					
			}
		}

		// --> 1.2 for remaining points of the min other domain total score, minus over here
		if ($count >= $requirement["Min._Score_in_Other_Domain"]) {
			// do nothing
		} else {
			$remainPointToMinus = $requirement["Min._Score_in_Other_Domain"]-$count;
			foreach($Domain as $eleId => &$domainPoint) {
				if ($eleId == "[CS]") {
					continue;
				} else {
					$domainPoint -= $requirement["Other_Domain_Min._Point"];
					$remainPointToMinus--;
					if ($remainPointToMinus == 0) {
						break;
					}
				}
			}
		}

		// -->$flag_OtherDomainSuccess
		$occupiedOtherDomains = $sizeOfDomainExcludingCs - $noPointsOtherDomains;
		if ($occupiedOtherDomains >= $requirement["Min._Occupation_in_Other_Domain"]) {
			$flag_OtherDomainSuccess = true;
		} else {
			return false;
		}

		// -->$flag_MinScore
//		error_log(" sumOfDomainPoints  ==>".$sumOfDomainPoints ." and Min._Score_in_Other_Domain -->".$requirement["Min._Score_in_Other_Domain"]."\n", 3, "/tmp/aaa.txt");
		if ($sumOfDomainPoints >= $requirement["Min._Score_in_Other_Domain"]) {
			$flag_MinScore = true;
		} else {
			return false;
		}

		if ($flag_Role && $flag_CsMinPoint && $flag_OtherDomainSuccess && $flag_MinScore) {
			return true;
		} else {
			return false;
		}
		
	}
	
	private function getBaseYearId() {
		global $intranet_db;
		$sql = "SELECT YEARID FROM $intranet_db.YEAR WHERE SEQUENCE = 0";
		$returnVector = $this->db->returnVector($sql);
		$returnYearId = $returnVector[0];
		return $returnYearId;
	}
	
	
	// base on the yearid to get the academicyearid
//	private function getBaseAcademicYearIdOfAStudent0($ParBaseYearId) {
//		global $intranet_db;
//		$sql = "SELECT
//					AY.ACADEMICYEARID
//				FROM
//					$intranet_db.ACADEMIC_YEAR AY
//					INNER JOIN $intranet_db.YEAR_CLASS YC ON YC.ACADEMICYEARID = AY.ACADEMICYEARID
//					INNER JOIN $intranet_db.YEAR_CLASS_USER YCU ON YCU.YEARCLASSID = YC.YEARCLASSID
//					INNER JOIN $intranet_db.INTRANET_USER IU ON YCU.USERID = IU.USERID
//				WHERE
//					YC.YEARID = $ParBaseYearId
//					AND
//					IU.USERID = $this->studentId
//					";
//		$returnVector = $this->db->returnVector($sql);
////		debug_r($sql);
//		$returnAcademicYearId = $returnVector[0];
//		return $returnAcademicYearId;
//	}
	
	private function getBaseAcademicYearIdOfAStudent() {
		global $intranet_db;
		$sql = "SELECT
					AY1.ACADEMICYEARID
				FROM
					ACADEMIC_YEAR AY1 LEFT JOIN ACADEMIC_YEAR AY2
					ON AY1.SEQUENCE = AY2.SEQUENCE - 2
				WHERE AY2.ACADEMICYEARID = $this->academicYearId
					";
		$returnVector = $this->db->returnVector($sql);
//		debug_r($sql);
		$returnAcademicYearId = $returnVector[0];
		return $returnAcademicYearId;
	}
	
	function getAchievement() {
//		$baseYearId = $this->getBaseYearId(); // year id with sequence == 0
//		$baseAcademicYearId = $this->getBaseAcademicYearIdOfAStudent($baseYearId);
		$baseAcademicYearId = $this->getBaseAcademicYearIdOfAStudent();
		$currentAcademicYearId = $this->academicYearId;
		$studentRecordsInYears = $this->getStudentRecordsInYears($baseAcademicYearId,$currentAcademicYearId);
//		debug_r($studentRecordsInYears); // fai
		# calculate achievement
		$achievementArray = $this->calculateAchievement($studentRecordsInYears);
		return $achievementArray;		 
	}
	function test_getStudentRecordsInYears(){
			$baseAcademicYearId = $this->getBaseAcademicYearIdOfAStudent();
		$currentAcademicYearId = $this->academicYearId;
		$studentRecordsInYears = $this->getStudentRecordsInYears($baseAcademicYearId,$currentAcademicYearId);
		return $studentRecordsInYears;
	}
	//-------------------------------
	function generateWordReport() {
		$output = "";
		return $output;
	}
	function generatePdfReport() {
		$output = "";
		return $output;
	}
	
	
	/**
	 * function to echo debug message
	 */
	private function echod($ParString="",$ParSeperator="b") {
	 	$debugOn = true;
	 	if ($ParSeperator == "b") {
	 		$seperator = "<br/>";
	 	} else if ($ParSeperator == "h") {
	 		$seperator = "<hr/>";
	 	} else {
	 		$seperator = $ParSeperator;
	 	}
	 	if ($debugOn == false) {
	 		echo "";
	 	} else {
	 		echo $ParString.$seperator;
	 	}
	 }
	public function copy_sunshine_program($academicYearId){
		global $eclass_db,$intranet_root;
		$objDB = new libdb();

		//ONE PROGRAM MAY HAS ONE OR MORE RELATE ITEM IN CUSTOM_CNECC_SS_EVENT , SO USE DISTINCT P.PORGORAMID
		//(c.SpecifiedStudentID is null)  ==> the program is specific to a student. System will not copy program that is specific to one student
		$sql = "select 
					distinct p.programid as 'PROGRAMID'
				from 
					{$eclass_db}.OLE_PROGRAM as p
				inner join {$eclass_db}.CUSTOM_CNECC_SS_EVENT as c on c.ProgramId = p.Programid
				where academicYearid = {$academicYearId} and (c.SpecifiedStudentID is null)  
				";
		
		$result = $objDB->returnArray($sql);

		//FOR LOG WHEN DO THIS CLONE PROCESS
		$now = date("YmdGis");


		/***** get the current term start date for the program start date ****/
		$programStart_date = getCurrentAcademicYearAndYearTerm();
		$AcademicYearID = $programStart_date["AcademicYearID"];
		$YearTermID = $programStart_date["YearTermID"];
		$CurrentTermStartDate = getStartDateOfAcademicYear($AcademicYearID,$YearTermID);
		/*********************************************************************/

		if(is_array($result) && sizeof($result) > 0){
			for($i = 0, $i_max = sizeof($result);$i < $i_max;$i++){
				$_pid = $result[$i]["PROGRAMID"];
				$_objOEA_PROGRAM_SRC = new ole_program($_pid);


				$_objOEA_PROGRAM_TARGET = new ole_program();
				$_objOEA_PROGRAM_TARGET->setProgramType($_objOEA_PROGRAM_SRC->getProgramType());
				$_objOEA_PROGRAM_TARGET->setTitle($_objOEA_PROGRAM_SRC->getTitle());
				$_objOEA_PROGRAM_TARGET->setTitleChi($_objOEA_PROGRAM_SRC->getTitleChi());
				$_objOEA_PROGRAM_TARGET->setStartDate($CurrentTermStartDate);
				$_objOEA_PROGRAM_TARGET->setCategory($_objOEA_PROGRAM_SRC->getCategory());
				$_objOEA_PROGRAM_TARGET->setSubCategoryID($_objOEA_PROGRAM_SRC->getSubCategoryID());
				$_objOEA_PROGRAM_TARGET->setOrganization($_objOEA_PROGRAM_SRC->getOrganization());
				$_objOEA_PROGRAM_TARGET->setDetails($_objOEA_PROGRAM_SRC->getDetails());
				$_objOEA_PROGRAM_TARGET->setDetailsChi($_objOEA_PROGRAM_SRC->getDetailsChi());
				$_objOEA_PROGRAM_TARGET->setSchoolRemarks($_objOEA_PROGRAM_SRC->getSchoolRemarks());
				$_objOEA_PROGRAM_TARGET->setELE($_objOEA_PROGRAM_SRC->getELE());
				$_objOEA_PROGRAM_TARGET->setCreatorID($UserID);
				$_objOEA_PROGRAM_TARGET->setCreatorType($_objOEA_PROGRAM_SRC->getCreatorType());
				$_objOEA_PROGRAM_TARGET->setIntExt($_objOEA_PROGRAM_SRC->getIntExt());
				$_objOEA_PROGRAM_TARGET->setAcademicYearID($AcademicYearID);
				$_objOEA_PROGRAM_TARGET->setYearTermID($YearTermID);
				$_objOEA_PROGRAM_TARGET->setCanJoin($_objOEA_PROGRAM_SRC->getCanJoin());
				$_objOEA_PROGRAM_TARGET->setCanJoinStartDate($_objOEA_PROGRAM_SRC->getCanJoinStartDate());
				$_objOEA_PROGRAM_TARGET->setCanJoinEndDate($_objOEA_PROGRAM_SRC->getCanJoinEndDate());
				$_objOEA_PROGRAM_TARGET->setAutoApprove($_objOEA_PROGRAM_SRC->getAutoApprove());
				$_objOEA_PROGRAM_TARGET->setJoinableYear($_objOEA_PROGRAM_SRC->getJoinableYear());
				$_objOEA_PROGRAM_TARGET->setComeFrom($_objOEA_PROGRAM_SRC->getComeFrom());
				$_objOEA_PROGRAM_TARGET->setMaximumHours($_objOEA_PROGRAM_SRC->getMaximumHours());
				$_objOEA_PROGRAM_TARGET->setCompulsoryFields($_objOEA_PROGRAM_SRC->getCompulsoryFields());
				$_new_programid = $_objOEA_PROGRAM_TARGET->SaveProgram();

				if($_new_programid> 0){
					$sql = "insert into 
								{$eclass_db}.CUSTOM_CNECC_SS_EVENT
								(ProgramID,Attainment,Role,Point,Status,InputDate,ModifiedDate)
							select 
								{$_new_programid}, Attainment,Role,Point,Status,now(),now()
							from 
								{$eclass_db}.CUSTOM_CNECC_SS_EVENT 
							where
								ProgramID = {$_pid}
					";	

					$objDB->db_db_query($sql);
				}

			}
			//write log
			$lf = new libfilesystem();	

			$log_file_folder = $intranet_root."/file/iportfolio";

			if(!file_exists($log_file_folder)){
				$lf->folder_new($log_file_folder);
			}
//			$log_file = $log_file_folder."/".$this->logFile;
			$log_file = $this->logFile;

			if(file_exists($log_file)){
				$org_content = $lf->file_read($log_file);
			}else
			{
				$org_content = "## THIS FILE GENERATE FROM SYSTEM\n";
				$org_content .= "## FORMAT : ACADEMICYEARID==YYYYMMDDHHMMSS   (COMMENT : CONVERTED ACADEMICYEAR ID == WHEN CONVERTED\n";
			}

			$content = $org_content."{$academicYearId}=={$now}\n";

			$lf->file_write($content ,$log_file);
		}
	}
	public function getCopyLogRecord(){
		global $intranet_root;
		$lf = new libfilesystem();
		//$file = $intranet_root."/file/iportfolio/".$this->logFile;
		$log_file = $this->logFile;
		
		$content= $lf->file_read($log_file);
		$tempArr = preg_split("/\n/",$content);

		$copiedAcademicYearID = array();

		for($i = 0 , $i_max = sizeof($tempArr); $i < $i_max; $i++){
			$_line = $tempArr[$i];

			if(trim($_line) == ""){
				//skip line if start empty
				continue; 
			}
			if(preg_match('/^##/',$_line)){
				//skip line if start with ##
				continue; 
			}
			
			$logRecdord = explode("==",$_line);
			if(is_array($logRecdord) && (sizeof ($logRecdord) > 0)){
				$copiedAcademicYearID[] = $logRecdord[0];
			}
		}
		return $copiedAcademicYearID;
	}

	//THIS FUNCTION IS WRITEN FOR TESTING TEAM TO TEST , CLIENT SHOULD NOT HAVE THIS FUNCTION
	public function clearCopyLog(){
		$lf = new libfilesystem();
		$log_file = $this->logFile;

		if(file_exists($log_file)){
			$lf->file_remove($log_file);
		}
		
		if(file_exists($log_file)){
			return false;
		}else{
			return true;
		}
	}
		
}

?>