<?php
/**
 * Modifying By: Max
 */
$Lang["Cust_Cnecc"]["Domain"] = "範疇";
$Lang["Cust_Cnecc"]["PointsAttained"] = "得分";
$Lang["Cust_Cnecc"]["Event"] = "事件";
$Lang["Cust_Cnecc"]["Attainment"] = "成績";
$Lang["Cust_Cnecc"]["Role"] = "角色";
$Lang["Cust_Cnecc"]["Point(s)"] = "分數";
$Lang["Cust_Cnecc"]["Success(v)"] = "成功(&#10004;)";
$Lang["Cust_Cnecc"]["EndDate"] = "完結日";
$Lang["Cust_Cnecc"]["NameOfStudent"] = "學生姓名";
$Lang["Cust_Cnecc"]["Class&ClassNo"] = "班級及班號";
$Lang["Cust_Cnecc"]["DateOfBirth"] = "出生日期";
$Lang["Cust_Cnecc"]["Sex"] = "性別";
$Lang["Cust_Cnecc"]["RegNo"] = "學生編號";
$Lang["Cust_Cnecc"]["HkidNo"] = "身份証號碼";
$Lang["Cust_Cnecc"]["DateOfIssue"] = "發出日期";
$Lang["Cust_Cnecc"]["Parent/Guardian"] = "家長/監護人";
$Lang["Cust_Cnecc"]["ClassTeacher"] = "班主任";
$Lang["Cust_Cnecc"]["Principal"] = "校長";
$Lang["Cust_Cnecc"]["PleaseChooseExactlyOneOption"] = "請只選一項其他學習經歷";
$Lang["Cust_Cnecc"]["SetAsSunshineProgram"] = "設定為陽光記錄";
$Lang["Cust_Cnecc"]["SunshineProgramInfo"] = "陽光記錄資訊";
$Lang["Cust_Cnecc"]["Attainment(s)Empty"] = "成績空白";
$Lang["Cust_Cnecc"]["Role(s)Empty"] = "角色空白";
$Lang["Cust_Cnecc"]["Point(s)Empty"] = "分數空白";
$Lang["Cust_Cnecc"]["Point(s)ShouldBeANumber"] = "分數應該是一個數目";
$Lang["Cust_Cnecc"]["PleaseEnterSunshineProgramInfo"] = "請填入陽光記錄資訊";
$Lang["Cust_Cnecc"]["SunshineProgram"] = "陽光記錄";
$Lang["Cust_Cnecc"]["Mr.NgaiShuChiu"] = "Mr. NGAI SHU CHIU";
$Lang["Cust_Cnecc"]["TitleOfProgram"] = "記錄名稱";
$Lang["Cust_Cnecc"]["Category"] = "分類";
$Lang["Cust_Cnecc"]["GenerateReport"] = "產生報告";
$Lang["Cust_Cnecc"]["SunshineProgramReport"] = "陽光記錄報告";
$Lang["Cust_Cnecc"]["NoRecordFound"] = "找不到記錄";
$Lang["Cust_Cnecc"]["RemarkContent"] = "備註: 這是證明 {{{ STUDENT_NAME }}} 達到全人發展計劃的{{{ ACHIEVEMENT }}}級別*。";
$Lang["Cust_Cnecc"]["RemarkDescription"] = "*{{{ ACHIEVEMENT }}}成績:-<br />最少在社會服務得{{{ CS_POINT }}} 分 及 在其他 {{{ ELE NUMBERS }}} 個範圍的其中{{{ OTHER_DOMAIN_OCCUPIED }}} 個得最少{{{ OTHER_DOMAIN_MIN }}} 分:<br /> {{{ ELE ELEMENTS }}}";
$Lang["Cust_Cnecc"]["Participant"] = array("value"=>"Participant", "label"=>"參與者");
$Lang["Cust_Cnecc"]["Organizer"] = array("value"=>"Organizer", "label"=>"籌辦人員");
$Lang["Cust_Cnecc"]["Helper"] = array("value"=>"Helper", "label"=>"工作人員");
$Lang["Cust_Cnecc"]["Error"]["InvalidDateFormat"] = "日期格式錯誤";
$Lang["Cust_Cnecc"]["CannotAssignStudentsForSunshinePrograms"] = "陽光記錄分類不能分配學生";
$Lang["Cust_Cnecc"]["BasicCompetency"] = "實踐獎";
$Lang["Cust_Cnecc"]["AwardOfAchievement"] = "成就獎";
$Lang["Cust_Cnecc"]["AwardOfExcellence"] = "卓越獎";
$Lang["Cust_Cnecc"]["NIL"] = "不適用";
$Lang["Cust_Cnecc"]["Total"] = "總分";
$Lang["Cust_Cnecc"]["AttainmentOfCnecWPDS"] = "中華傳道會安柱中學全人發展計劃*的表現";
$Lang["Cust_Cnecc"]["AwardAttained"] = "獲得之獎項";
$Lang["Cust_Cnecc"]["CriteriaContent"] = "本計劃之準則已於背面列明。";

$Lang["Cust_Cnecc"]["OwnDefineCategory"]["ParticipationInActivities"] = "曾參與之活動";
$Lang["Cust_Cnecc"]["OwnDefineCategory"]["CoursesAndTrainingTaken"] = "曾參與之課程及訓練";
$Lang["Cust_Cnecc"]["OwnDefineCategory"]["Services"] = "服務";
$Lang["Cust_Cnecc"]["OwnDefineCategory"]["Awards"] = "獎項";
$Lang["Cust_Cnecc"]["OwnDefineCategory"]["Others"] = "其他";
$Lang["Cust_Cnecc"]["CloneYear"]["Instruction"] = "你可以從過往學年建立 / 複製Sunshine Program(s)。";
$Lang["Cust_Cnecc"]["JS_EmptyYear"]= "請選擇一個學年";
$Lang["Cust_Cnecc"]["JS_YearWithRecord"]= "所選學年已曾經複製。 \\n複製停止";
?>