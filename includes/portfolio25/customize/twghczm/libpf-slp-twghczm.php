<?php

function EmptyContentAsEmptyString($Content=''){
	
	global $EmptyString;
	
	if($Content ==''){
		$Content = $EmptyString;
	}
	else{
		$Content = $Content;
	}
	
	return $Content;
}

class objPrinting{
//-- Start of Class

	var $EmptyString = '---';
	var $AcademicYearName;
	
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	private $cfgValue;
	
	private $AcademicYear;
	private $studentListAry;
	private $studentId;
	private $Student_info_Ary;
	private $Student_Assessment_Ary;
	private $Student_Assessment_YearID_Ary;
	private $Student_OLE_Ary;
	private $Student_Comment_Ary;
	private $Student_SelfAccount_Ary;
	private $Student_Character_Ary;
	private $Character_Header_Ary;
	private $Student_ClassName;
	private $yearClassID;
	private $Student_YearIDAry;
	private $Student_Awards_Ary;
	private $Student_ClassHistory_Ary;
	
	public function __construct(){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		$this->cfgValue = $ipf_cfg;
	}
		
	private function returnLatest3YearID($curAcademicYear){
		$StartOfCurYear = getStartDateOfAcademicYear($curAcademicYear);
		
		$sql = "select 
							AcademicYearID 
						from 
							ACADEMIC_YEAR_TERM 
						where 
							TermEnd < '".$StartOfCurYear."' 
						order by 
							TermEnd Desc 
						limit 0,1";
		$PrevYearID = $this->objDB->returnVector($sql);

		$StartOfPrevYear = getStartDateOfAcademicYear($PrevYearID[0]);
		
		$sql = "select 
							AcademicYearID 
						from 
							ACADEMIC_YEAR_TERM 
						where 
							TermEnd < '".$StartOfPrevYear."' 
						order by 
							TermEnd Desc 
						limit 0,1";
		$PrevPrevYearID = $this->objDB->returnVector($sql);
		
		$returnArray = array($curAcademicYear,$PrevYearID[0],$PrevPrevYearID[0]);
		return $returnArray;
	}
	public function setAcademicYear($AcademicYear){
		$this->AcademicYear = $AcademicYear;
		$this->AcademicYearName = getAcademicYearByAcademicYearID($AcademicYear);
//		$this->AcademicYearArr = $this->returnLatest3YearID($AcademicYear);
	}
	public function getAcademicYear($format='ID'){
		if($format == 'ID'){
			return $this->AcademicYear;
		}
		else{
			return $this->AcademicYearName;	
		}
	}
	
	public function setYearClassID($yearClassId){
		$this->yearClassID = $yearClassId;
	}
	public function getYearClassID(){
		return $this->yearClassID;
	}
	
	public function setStudentList($studentListAry){
		$this->studentListAry = $studentListAry;
	}
	public function getStudentList(){
		return $this->studentListAry;
	}
	
	
	public function setStudent($studentId){
		$this->studentId = $studentId;
	}
	public function getStudent(){
		return $this->studentId;
	}
	
	public function contentReady(){
		//Please Set StudentList Before call this function		
		$this->Student_info_Ary = $this->getStudentList_Info();
		//$this->Student_Assessment_Ary = $this->getStudentListAssessmentResult();
		$this->Student_OLE_Ary = $this->getStudentList_OLE();
		$this->Student_Comment_Ary = $this->getStudentListComment();
		//$this->Student_SelfAccount_Ary = $this->getStudentList_SelfAccount();
		$this->Student_Character_Ary = $this->getAllStudentCharacter();
		//$this->School_Subject_Ary = $this->getSchoolSubjectInfo();
		$this->Student_Awards_Ary = $this->getStudentList_Awards();
		$this->Student_ClassHistory_Ary = $this->getStudentList_ClassHistory();
	}
	
			
	//Get Student Info
	private function getStudentList_Info(){
		$sql = "select 
					iu.EnglishName,
					iu.ChineseName,
					DATE_FORMAT(iu.DateOfBirth, '%d/%m/%Y') AS DateOfBirth,
					iu.ClassName,
					iu.ClassNumber,
					IF(iu.Gender='M', 'Male', 'Female') as Gender,
					iu.BarCode,
					iu.STRN,
					iu.HKID,
					substring(iu.WebSAMSRegNo from 2) as WebSAMSRegNo ,
					iu.userid,
					iups.AdmissionDate as AdmissionDate,
					iu.UserLogin
				from
					".$this->intranet_db.".INTRANET_USER AS iu
					LEFT OUTER JOIN INTRANET_USER_PERSONAL_SETTINGS as iups on iu.UserID = iups.UserID
				where
					iu.userid IN ('".implode('\',\'',$this->getStudentList())."') 
					AND (iu.RecordType = '2' OR iu.UserLogin like 'tmpUserLogin_%') ";

		$result = $this->objDB->returnResultSet($sql);
		
		return BuildMultiKeyAssoc($result,'userid');
	}
	public function getStudentInfo(){
		return $this->Student_info_Ary[$this->getStudent()];
	}
	
	private function getClassTeacherInfo(){
	    $sql = 'SELECT
					 IF(u.EnglishName IS NULL OR TRIM(u.EnglishName) = \'\',u.ChineseName,u.EnglishName) AS TeacherName
				FROM
					YEAR_CLASS_TEACHER as yct
					LEFT JOIN
					INTRANET_USER as u ON (yct.UserID = u.UserID)
				WHERE
					YearClassID = \''.$this->yearClassID.'\'';
	    $result = $this->objDB->returnResultSet($sql);
	    return Get_Array_By_Key($result, 'TeacherName');
	}

	private function getStudentList_ClassHistory(){
		# Class History
		$sql = "Select UserID, ClassName, ay.YearNameEN as AcademicYear, pch.AcademicYearID 
				From PROFILE_CLASS_HISTORY as pch 
					INNER JOIN ACADEMIC_YEAR as ay on pch.AcademicYearID = ay.AcademicYearID
				where 
					pch.userid IN  ('".implode('\',\'',$this->getStudentList())."')  
				order by pch.AcademicYear";
		$result = $this->objDB->returnResultSet($sql);
		foreach((array)$result as $_AYdataArr){
			$AYAssocArr[$_AYdataArr['UserID']][] = $_AYdataArr;
		}
		return $AYAssocArr;
	}
	public function getStudentClassHistory(){
		return $this->Student_ClassHistory_Ary[$this->getStudent()];
	}
	
//	private function getSchoolSubjectInfo(){
//		
//		$sql = "Select 
//			ass.RecordID, 
//			ass2.CODEID, 
//			ass2.EN_SNAME as SubjectCode, 
//			ass2.EN_DES as SubjectDisplayName,
//			ass2.CH_DES as SubjectDisplayNameChi,
//			ass2.DisplayOrder,
//			ass2.CMP_CODEID as isComponent
//		From 
//			LEARNING_CATEGORY as lc 
//			inner join ASSESSMENT_SUBJECT as ass on lc.LearningCategoryID = ass.LearningCategoryID and ass.RecordStatus =1
//			left outer join ASSESSMENT_SUBJECT as ass2 on ass.CODEID = ass2.CODEID and ass2.RecordStatus =1
//		where 
//			lc.RecordStatus = 1 order by lc.DisplayOrder, ass.DisplayOrder, ass2.CMP_CODEID , ass2.DisplayOrder";
//			
//		$result = $this->objDB->returnResultSet($sql);
//		
//		return $result;
//	}
//	private function getStudentListAssessmentResult(){
//		# Class History
//		$sql = "Select UserID, ClassName, ay.YearNameEN as AcademicYear, pch.AcademicYearID 
//				From PROFILE_CLASS_HISTORY as pch 
//					INNER JOIN ACADEMIC_YEAR as ay on pch.AcademicYearID = ay.AcademicYearID
//				where 
//					pch.userid IN  ('".implode('\',\'',$this->getStudentList())."') 
//					/*AND pch.ClassName REGEXP '".implode('|',array(4,5,6))."'*/
//					AND ay.AcademicYearID IN ('".implode("','",$this->AcademicYearArr)."') 
//				order by pch.AcademicYear";
//		$result = $this->objDB->returnResultSet($sql);
//		$academicYearIDArr = array_unique(Get_Array_By_Key($result,'AcademicYearID'));
//		foreach((array)$result as $_AYdataArr){
//			$AYAssocArr[$_AYdataArr['UserID']][] = $_AYdataArr;
//		}
//		$this->Student_Assessment_YearID_Ary =  $AYAssocArr;
//		
//		$sql ="SELECT 
//						a.RecordID,
//						IFNULL(iu.WebSAMSRegNo, iau.WebSAMSRegNo) as WebSAMSRegNo,
//						a.ClassName as ClassName,
//						a.ClassNumber as ClassNumber,
//						a.Year,
//						a.Semester as Semester,
//						a.AcademicYearID,
//						a.YearTermID,
//						a.SubjectCode,
//						a.SubjectComponentCode,
//						a.Score,
//						a.Grade,
//						a.OrderMeritForm,
//						asub.EN_ABBR as Subject,
//						a.SubjectID,
//						a.UserID
//					FROM
//						".$this->eclass_db.".ASSESSMENT_STUDENT_SUBJECT_RECORD as a
//						LEFT JOIN ".$this->intranet_db.".INTRANET_USER AS iu ON a.UserID = iu.UserID
//						LEFT JOIN ".$this->intranet_db.".INTRANET_ARCHIVE_USER AS iau ON a.UserID = iau.UserID
//						LEFT JOIN ".$this->intranet_db.".ASSESSMENT_SUBJECT AS asub ON a.subjectID = asub.RecordID
//					WHERE
//						a.AcademicYearID IN  ('".implode($academicYearIDArr,"','")."')
//						AND (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
//						AND a.userid IN ('".implode('\',\'',$this->getStudentList())."')  
//					ORDER BY
//						a.ClassName,
//						ClassNumber,
//						a.Year desc,
//						a.Semester desc,
//						asub.DisplayOrder
//						/*a.SubjectCode,
//						a.SubjectComponentCode*/
//				";
//				
//		$result = $this->objDB->returnResultSet($sql);
//		
//		foreach((array)$result as $dataArr){
//			$AssocArr[$dataArr['UserID']][$dataArr['SubjectID']][$dataArr['AcademicYearID']] = $dataArr;
//		}
//		return $AssocArr;
//	}
//	public function getStudentAssessmentResult(){
//		
//		$resultArr['SchoolSubject']= $this->School_Subject_Ary;
//		$resultArr['AssessmentResult'] = $this->Student_Assessment_Ary[$this->getStudent()];
//		$resultArr['Year'] = $this->Student_Assessment_YearID_Ary[$this->getStudent()];
//		return $resultArr; 
//	}
	
	//Get Student OLE
	private function getStudentList_OLE(){
		$condApprove = " and os.RecordStatus in( ".$this->cfgValue["OLE_STUDENT_RecordStatus"]["approved"]." ,".$this->cfgValue["OLE_STUDENT_RecordStatus"]["teacherSubmit"].")";
		
		$sql = "select 
					op.Title,
					op.StartDate,
					op.EndDate,
					os.Role,
					os.Hours,
					os.Achievement,
					op.ELE,
					op.Details,
					ay.YearNameEN as AcademicYear,
					IF(ayt.YearTermNameEN IS NULL, 'Whole Year',ayt.YearTermNameEN) as Term,
					oc.EngTitle as Category,
					os.userid,
					os.Organization as Organization,
					oc.RecordID as ProgramCategoryID,
					Concat(oc.ChiTitle ,'<br>', oc.EngTitle) as CategoryName,
					op.IntExt as IntExt,
					os.SLPOrder
				from 
					".$this->eclass_db.".OLE_STUDENT as os 
					inner join ".$this->eclass_db.".OLE_PROGRAM as op on os.programid = op.programid 
					inner join ".$this->eclass_db.".OLE_CATEGORY as oc on op.Category = oc.RecordID 
					inner join ".$this->intranet_db.".ACADEMIC_YEAR as ay on op.AcademicYearID = ay.AcademicYearID
					left join ".$this->intranet_db.".ACADEMIC_YEAR_TERM  as ayt on op.YearTermID = ayt.YearTermID
				where 
					os.userid IN ('".implode('\',\'',$this->getStudentList())."')  
					AND os.Role  <> 'participant'
					$condApprove
				order by 
					AcademicYear desc, -os.SLPOrder desc /*sort Null last*/ ,op.Title asc
				";

		$result = $this->objDB->returnResultSet($sql);
		
		foreach($result as $_OLEInfoArr){
			$_studentId = $_OLEInfoArr['userid'];
			$_programCategoryID = $_OLEInfoArr['ProgramCategoryID'];
			$_IntExt = $_OLEInfoArr['IntExt'];
					
			if($_programCategoryID == 5){
				//services
				$resultAssoArr[$_studentId][$_IntExt]['SERVICE'][] = $_OLEInfoArr;
			}
			else{
				//external or internal OLE
				$resultAssoArr[$_studentId][$_IntExt]['OLE'][] = $_OLEInfoArr;
			}

		}
	
		return $resultAssoArr;
	}
	public function GetOLE($IntExt, $Category){
		$IntExt = strtoupper($IntExt);
		$Category= strtoupper($Category);
		
		return $this->Student_OLE_Ary[$this->getStudent()][$IntExt][$Category];
	}
	
	//Get Student Awards
	private function getStudentList_Awards(){
		$sql ="
			SELECT
				awds.userid,
				ay.YearNameEN as AcademicYear,
				yt.YearTermNameEN as Term,
				awds.AwardName
			FROM
				".$this->eclass_db.".AWARD_STUDENT as awds
				LEFT JOIN ".$this->intranet_db.".ACADEMIC_YEAR as ay on awds.AcademicYearID = ay.AcademicYearID
				LEFT JOIN ".$this->intranet_db.".ACADEMIC_YEAR_TERM as yt on awds.YearTermID = yt.YearTermID
			WHERE
				awds.Userid IN ('".implode('\',\'',$this->getStudentList())."') 
                AND awds.AwardName NOT LIKE '%participant%'
				AND awds.RecordType = '1'
			ORDER BY
				ay.YearNameEN desc
			";

		$result = $this->objDB->returnResultSet($sql);
		
		$returnArray = array();
		foreach((array)$result as $dataArr){
			$_userid = $dataArr['userid'];
			$returnArray[$_userid][] = $dataArr;
		}
		
		return $returnArray;
	}
	public function getAwards(){
		return $this->Student_Awards_Ary[$this->getStudent()];
	}
	
	//Get Self Account
	private function getStudentList_SelfAccount(){
		$sql = "select 
					sas.Details as Details,
					userid
				from 
					".$this->eclass_db.".SELF_ACCOUNT_STUDENT as sas 
				where 
					Userid IN ('".implode('\',\'',$this->getStudentList())."')  
					and DefaultSA = 'SLP' 
			   ";

		$result = $this->objDB->returnResultSet($sql);
		return BuildMultiKeyAssoc($result,'userid');
	}
	public function getSelfAccount(){
		return $this->Student_SelfAccount_Ary[$this->getStudent()];
	}
	
	private function getStudentListComment(){
		$sql ="
			SELECT
				StudentID AS UserID,
				Remark
			FROM
				".$this->eclass_db.".CUSTOM_STUDENT_REMARKS 
			WHERE
				StudentID IN ('".implode("','",$this->getStudentList())."')
                AND AcademicYearID = '".$this->AcademicYear."'
			ORDER BY ModifiedDate";
		
		$result = $this->objDB->returnArray($sql); 

		return BuildMultiKeyAssoc($result,'UserID');
	}
	public function getStudentComment(){
		
		$resultArr = $this->Student_Comment_Ary[$this->getStudent()];

		return $resultArr;
	}
	
	private function getAllStudentCharacter(){
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libimporttext.php");
		$libimport = new libimporttext();
		
		# Personal Attributes 
		$FileCSV = $PATH_WRT_ROOT."file/portfolio/twghczm_pc_".$this->AcademicYear.".csv";
		if (file_exists($FileCSV))
		{
			$csv_data = $libimport->GET_IMPORT_TXT($FileCSV);
			$csv_header = $csv_data[0];
			$this->Character_Header_Ary = $csv_header;		
			for ($i=1; $i<sizeof($csv_data); $i++)
			{
				$rowObj = $csv_data[$i];
				$PersonalAttributes[trim($rowObj[0], "#")] = $rowObj;
			}
		}
		return $PersonalAttributes;
	}
	private function getSelectedStudentCharacter($StudentID){
			global $PATH_WRT_ROOT,$Lang,$eclass_db;
			include_once($PATH_WRT_ROOT."includes/libimporttext.php");
			$libimport = new libimporttext();
			
			# Personal Attributes
			$PersonalCharacter = $Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type'];
			$this->Character_Header_Ary = $PersonalCharacter;
			$YearClassID = $_POST['YearClassID'];
			
			for($i=0;$i<sizeof($PersonalCharacter);$i++){
				$typeID = $i;
				$sql = "SELECT * FROM  {$eclass_db}.CUSTOM_STUDENT_PERSONAL_CHARACTER
				WHERE YearClassID = '$YearClassID' and TypeID = '$typeID' and StudentID = '$StudentID'";
				$PersonalCharacterAry = $this->objDB->returnArray($sql);
				$StudentPersonalCharacter[$i] = $PersonalCharacterAry[0]['Grade'];
			//	$PersonalCharacterAry = BuildMultiKeyAssoc($PersonalCharacterAry,'StudentID');
			
			}
// 			debug_pr($StudentPersonalCharacter);
// 			$FileCSV = $PATH_WRT_ROOT."file/portfolio/twghczm_pc_".$this->AcademicYear.".csv";
// 						$csv_data = $libimport->GET_IMPORT_TXT($FileCSV);
// 						$csv_header = $csv_data[0];
// 						$this->Character_Header_Ary = $csv_header;
// 						for ($i=1; $i<sizeof($csv_data); $i++)
// 							{
// 								$rowObj = $csv_data[$i];
// 								$PersonalAttributes[trim($rowObj[0], "#")] = $rowObj;
// 							}
// 							debug_Pr($PersonalAttributes);
			return $StudentPersonalCharacter;
		}
	
		
	
	public function getStudentCharacterHeader(){
		return $this->Character_Header_Ary;
	}
	public function getStudentCharacter($WebSAMSNum){
		return $this->Student_Character_Ary[$WebSAMSNum];
	}
	
//	public function getDisplayFormName($ClassName){
//		$numberChiArr = array( 1=>'一',2=>'二',3=>'三',4=> '四',5=> '五',6=> '六');
//		preg_match("/\d/", $ClassName, $formNumArr);
//		$string = '中'.$numberChiArr[$formNumArr[0]].' Sec '.$formNumArr[0];
//		return $string;
//	}
//	
//	public function display2Lang($string){
//		$ChineseStringArr = explode('[',$string);
//		$EnglishStringArr = explode(']',$ChineseStringArr[1]);
//		return trim($ChineseStringArr[0]).'<br>'.trim($EnglishStringArr[0]);
//	}

	public function getUserPhoto($UserLogin){
		global $intranet_root;
		//;
		//return $user_photo_path = $intranet_root."/file/user_photo/".$UserLogin.".jpg";
		return $user_photo_path = "http://".$_SERVER['HTTP_HOST']."/file/user_photo/".$UserLogin.".jpg";
	}
	
	public function displayAcademicYear($string){
		$academicYearArr = explode('-',$string);
		
		if( strlen($academicYearArr[0]) == 4 && strlen($academicYearArr[1]) == 4 ){
			return substr($academicYearArr[0],2,2).'-'.substr($academicYearArr[1],2,2);	
		}
		else{
			return $string;
		}
	}
	
	//------------------------------------------------ ABOVE ARE DATA HANDLING RELATED ------------------------------------------------//
	#####################################################################################################################################
	#																																	#
	#																																	#
	#####################################################################################################################################	
	//------------------------------------------------ BELOW ARE HTML AND CSS RELATED -------------------------------------------------//

	public function returnStyleSheet(){
		$style = '<style>
					@page {size: A4 portrait; margin: 50px 25px 0px 25px;}
					@media print
						{
						  .signTable 		{ page-break-after:auto }
						  .signTable tr    { page-break-inside:avoid; page-break-after:auto;}
						  .signTable td    { page-break-inside:avoid; page-break-after:auto;}
						}
					body { padding:0; margin:0 25px 0 25px; font-family: "Arial","新細明體",Arial;}
					table {width:100%;}
					.page {width:21cm;height:29.7cm; background: blue;}
					.page_wrapper {width: 16cm; height: 27cm; margin: 0 auto ;  page-break-inside:avoid; /*display:block; background: yellow;*/ }
					.secondpage   {width: 18cm; height: 27cm; margin: 0 auto; page-break-inside:avoid;}
                    .marginDiv { height:10mm;width:100%; } 
					.sectionHeader { font-size:16px; text-align:left; font-family:"Arial"; font-weight:blod; line-height: 150%;}
					.studentInfo {border-spacing:1px; border-collapse:separate; }
                    .studentInfo td { font-size:16px; padding:0px 0px 6px 0; font-family: "Arial";line-height: 120%; }
					.remarksTable td {  font-size:14px;  line-height: 150%; font-family: "Arial";}
					.characterTable { font-family:"Arial";  font-size:15px;border:1px solid black; border-collapse:collapse;}
					.characterTable th{ font-size: 15x;  font-weight: bold;}
					.characterTable td,.characterTable th{ border:1px solid black; }
					.characterTable td { font-size: 15px; padding:1px 13px;}
					.characterTable .gradeCell { text-align:center; font-family:"Courier New"; font-family: "Arial";  font-size: 15px;  line-height: 150%; }
					
					.characterTable2 { font-family:"Arial"; font-size: 15px; text-align:center;}
					.characterTable2 .DCell { text-align:left; padding:0px 25px; }
					.characterTable2 th{ font-size: 14px; line-height: 150%; font-weight:normal; text-decoration:underline; text-align:left; }
					
					 .size8table td{ font-size:10px; }
					.signTable {  font-size:14px;  text-align:left;}
					.signTable .signLine { height:10mm; border-bottom:1px solid black; }

					.font8 { font-size:8px;}
					.schoolNameChi {  font-size:16px; font-family: "微軟正黑體"; font-weight:bold; }
					.schoolNameEng {  font-size:15px; font-family: "Arial"; font-weight:bold; }
					.title {  font-size:24px; font-family: "Arial";}
					table.studentPhoto {border-collapse: collapse;}
					table.studentPhoto tr{ border: 1px solid black;}
		            table.admissionNo td { text-align:right; font-size:12px;}
                    .pagefooter {font-size:7px;bottom: 8px; right: 16px;}
                    table.pagefooter td{text-align:center; }
                    .schoolimg {margin-left: 10px;}
				</style>';
				
		return $style;
	}
	
	public function returnSchoolHeader(){
		$school_logo = "<table class=\"schoolimg\"><tr><td><img src=\"http://".$_SERVER['HTTP_HOST']."/file/customization/twghczm/Testimonial_heading.jpg\" width=\"600\" height=\"95\"/></td></tr></table>";
		
		$school_header = '<table>
							<tr>
								<td width="14%" rowspan="2">'.$school_logo.'</td>
								<td width="60%" valign="top" align="center">
									<font class="schoolNameChi" style="font-size:24px;">東 華 三 院 陳 兆 民 中 學</font>
									<br/><br/>
									<font class="schoolNameEng">Tung Wah Group of Hospitals Chen Zao Men College</font>
									<br/><br>
									<font class="schoolNameChi">新界葵涌葵合街 1-5 號</font>
								</td>
								<td width="16%" align="left" style="font-size:12px;line-height: 110%; font-family: Arial;">1 Kwai Hop Street,<br><br>Kwai Chung, N.T. <br><br>Tel:2424 5318<br><br>Fax:2149 6144</td>
							</tr>
<tr></tr><tr></tr>
					
						</table><br>';
// 		<tr>
// 		<td class="title" align="center" colspan="3">Testimonial</td>
// 		</tr>
						
		return $school_logo;
	}
	
	public function returnStudentInfo(){
		
		$studentInfoArr = $this->getStudentInfo();
		$studentCHArr= $this->getStudentClassHistory();
		$lastIndex = count($studentCHArr) - 1;
		
		if($studentInfoArr['AdmissionDate'] != '0000-00-00'){
			$displayAD = date('m/Y',strtotime($studentInfoArr['AdmissionDate']));
		}
		else{
			$displayAD = '';
		}
		
		# leave date
		$AcademicYear = getCurrentAcademicYear();
		$YearArr = explode('-',$AcademicYear);
		$leavingDate = '05/'.$YearArr[1];
		
// 		$student_info = '<table class="studentInfo">
// 							<tr>
// 								<td width="17%">Name</td>
// 								<td width="30%">: '.$studentInfoArr['EnglishName'].' ('.$studentInfoArr['ChineseName'].')</td>
// 								<td width="20%">Sex: '.$studentInfoArr['Gender'].'</td>
// 								<td width="18%">Admission No.</td>
// 								<td width="25%">: '.$studentInfoArr['WebSAMSRegNo'].'</td>
// 							</tr>
// 							<tr>
// 								<td>Date of Admission</td>
// 								<td colspan="2">: '.$displayAD.'</td>
// 								<td>Class on Admission</td>
// 								<td>: '.$studentCHArr[0]['ClassName'].'</td>
// 							</tr>
// 							<tr>
// 								<td>Date of Leaving</td>
// 								<td colspan="2">: '.$leavingDate.'</td>
// 								<td>Class Completed</td>
// 								<td>: '.$studentCHArr[$lastIndex]['ClassName'].'</td>
// 							</tr>
// 						</table>';
		$student_info = '<br><br><table>

                            <tr>
                                <td width="70%">';
        		$student_info .= '<table class="studentInfo">
        							<tr>
        								<td width="8%">Name:</td>
        								<td width="15%">'.$studentInfoArr['EnglishName'].' ('.$studentInfoArr['ChineseName'].')</td>
        							</tr>
        							<tr>							
        								<td>Sex:</td>
        								<td>'.$studentInfoArr['Gender'].'</td>
        							</tr>
        							<tr>							
        								<td>Admission No.:</td>
        								<td>'.$studentInfoArr['WebSAMSRegNo'].'</td>
        							</tr>
        							<tr>
        								<td>Class on Admission:</td>
        								<td>'.$studentCHArr[0]['ClassName'].'</td>
        							</tr>
        							<tr>
        								<td>Class Completed:</td>
        								<td>'.$studentCHArr[$lastIndex]['ClassName'].'</td>
        							</tr>
        							<tr>
        								<td>Date of Admission:</td>
        								<td>'.$displayAD.'</td>
        							</tr>
        
        							<tr>
        								<td>Date of Leaving:</td>
        								<td>'.$leavingDate.'</td>
        							</tr>
        							
        						 </table>';
		      $student_info .='</td>

                               <td width="12%">
							     <table style="border: 1px solid black; text-align:center; font-size:10px;"  >
                                 <tr width="20mm"><td><br><br><br><br> Photo <br><br><br><br></td></tr>
                                </table>	
						        </td>
<td width="8%"></td>
                            </tr>
                        </table>';
		
		return $student_info;
	}
	
	public function returnstudentAdmissionNo(){
	    
	    $studentInfoArr = $this->getStudentInfo();
	    $AdmissionNo = $studentInfoArr['WebSAMSRegNo'];
	    $AdmissionNoHTML = '<table class="admissionNo">
                            <tr>
                            <td>Admission No. :'.$AdmissionNo.'</td>                    
                            </tr>
                            </table>';
	    return $AdmissionNoHTML;
	}
	
	public function returnActivitiesTable($maxECAsize = ''){
		
		$activityAssoArr = $this->GetOLE('INT','OLE'); //School based activity

		$numActivity = count($activityAssoArr);
//		$maxECAsize = $maxECAsize ==''? $numActivity : ($maxECAsize-1);
		$maxECAsize = $maxECAsize-1;
		// $activityAssoArr['Title'] $activityAssoArr['AcademicYear']
		$activities_table = '<table class="size8table"  width="110%">
								<tr>
									<th class="sectionHeader" colspan="4">Extra-Curricular Activities and Services</th>
								</tr>';
	
// 		if($numActivity>0){
		    
// 		    $activities_table .= '<tr>';
		    
// 			if($numActivity > 11){
// 				if($numActivity<=20){					
// 					$countItem = $numActivity;
// 				}
// 				else{
// // 					$countItem = 20;
// 					$halfRecordNum = $countItem/2;
// 				}
				
// 				// double column
// // 				for($x = 0;$x<11;$x++){
// 				for($x = 0;$x<$halfRecordNum;$x++){
				        
// 				    $rightColumn = ($x+$halfRecordNum);
// 				    $MaxrightColumn = ($x+$halfRecordNum);
// 					$roleName = $activityAssoArr[$x]['Role']==''||$activityAssoArr[$x]['Role']=='N/A'?'':$activityAssoArr[$x]['Role'].' of ';
					

// 					if($MaxrightColumn>19){
// 					    $MaxrightColumn= '';
// 					}
// 					if($rightColumn>19){
// 					    $rightColumn= '';
// 					}
						
// 						$roleMaxrightColumn= $activityAssoArr[$MaxrightColumn]['Role']==''?'':$activityAssoArr[$MaxrightColumn]['Role'].' of ';						
// 						$roleRightName = $activityAssoArr[$rightColumn]['Role']==''?'':$activityAssoArr[$rightColumn]['Role'].' of ';
// 						$right_table = '';
// 						if($maxECAsize == '-1'){
						    
// 						    $left_activities_table .='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top">'.$this->displayAcademicYear($activityAssoArr[$x]['AcademicYear']).'</td>
// 											  <td width="90%" style="font-family:Arial; font-size:13px;">'.$roleName.$activityAssoArr[$x]['Title'].'</td></tr>';
// 						    $right_activities_table .= $activityAssoArr[$rightColumn]['Title'] == ''?'':'<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top">'.$this->displayAcademicYear($activityAssoArr[$rightColumn]['AcademicYear']).'</td>
// 											<td width="90%" style="font-family:Arial; font-size:13px;">'.$roleRightName.$activityAssoArr[$rightColumn]['Title'].'</td></tr>';
// 						    $right_table = '1';
// 						} elseif($maxECAsize >= $x && $maxECAsize < $MaxrightColumn){
					
// 						    $left_activities_table .='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top">'.$this->displayAcademicYear($activityAssoArr[$x]['AcademicYear']).'</td>
// 											  <td style="font-family:Arial; font-size:13px;" width="90%">'.$roleName.$activityAssoArr[$x]['Title'].'</td></tr>';
							
// 						} elseif($maxECAsize >= $x && $maxECAsize >= $MaxrightColumn){
						   
// 						    $left_activities_table .='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top">'.$this->displayAcademicYear($activityAssoArr[$x]['AcademicYear']).'</td>
// 											  <td style="font-family:Arial; font-size:13px;" width="90%">'.$roleName.$activityAssoArr[$x]['Title'].'</td></tr>';
// 						    $right_activities_table .= $activityAssoArr[$MaxrightColumn]['Title'] == ''?'':'<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top">'.$this->displayAcademicYear($activityAssoArr[$MaxrightColumn]['AcademicYear']).'</td>
// 											<td style="font-family:Arial; font-size:13px;" width="90%">'.$roleMaxrightColumn.$activityAssoArr[$MaxrightColumn]['Title'].'</td></tr>';
// 						    $right_table = '1';
// 						}
// 				}
// 			}
// 			else{
// 				// single column
// 				for($x = 0;$x<$numActivity;$x++){
				    
// 				    $roleName = $activityAssoArr[$x]['Role']==''||$activityAssoArr[$x]['Role']=='N/A'?'':$activityAssoArr[$x]['Role'].' of ';
				    
// 					if($maxECAsize == '-1'){
// 						$left_activities_table .='<tr>
// 										<td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top">'.$this->displayAcademicYear($activityAssoArr[$x]['AcademicYear']).'</td>
// 										<td  style="font-family:Arial; font-size:13px;" width="90%">'.$roleName.$activityAssoArr[$x]['Title'].'</td>
// 										</tr>';
// 					}else if($maxECAsize >= $x){
// 					    $left_activities_table .=	'<tr>
// 										<td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top">'.$this->displayAcademicYear($activityAssoArr[$x]['AcademicYear']).'</td>
// 										<td style="font-family:Arial; font-size:13px;" width="90%">'.$roleName.$activityAssoArr[$x]['Title'].'</td>
// 										</tr>';
// 					}
					 
// 				}
// 			}
		
// 			if($right_table != ''){
// 			    $right_activities_table.='<tr><td colspan="2">***</td></tr>';
// 			}else{
// 			    $left_activities_table.='<tr><td colspan="2">***</td></tr>';
// 			}
// 			$activities_table .= '
//                                 <td style="font-family:Arial; font-size:13px;" width="50%" style="vertical-align: top;"><table class="size8table" >'.$left_activities_table.'</table></td>
//                                 <td  style="font-family:Arial; font-size:13px;" width="50%" style="vertical-align: top;"><table class="size8table">'.$right_activities_table.'</table></td>';
// 			$activities_table .= '</tr>';
			
// 		}
// 		else{
// 		    $activities_table .= '<tr>
// 								<td colspan="4" align="center">No Record.</td>
// 							</tr>';		
// 		}
		
		

		if($numActivity>0){
		    $activities_table.= '<tr>';
		    if($numActivity<10){
		        for($x=0;$x < $numActivity; $x++){
		            
		            $leftColNum = $x;
		            
		            $RoleName = $activityAssoArr[$leftColNum]['Role']==''?'':$activityAssoArr[$leftColNum]['Role'].' of ';
		            $RoleName = $activityAssoArr[$leftColNum]['Role']=='' || $activityAssoArr[$leftColNum]['Role']=='N/A'?'':$activityAssoArr[$leftColNum]['Role'].' of ';
		            if($maxECAsize== '-1'){
		                $left_activities_table .= '<tr>
									<td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top">'.$this->displayAcademicYear($activityAssoArr[$x]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;" width="90%"> '.$RoleName.$activityAssoArr[$leftColNum]['Title'].'</td>
									</tr>';
		            }else if($maxECAsize>= $x){
		                $left_activities_table .= '<tr>
									<td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top">'.$this->displayAcademicYear($activityAssoArr[$x]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;" width="90%"> '.$RoleName.$activityAssoArr[$leftColNum]['Title'].'</td>
									</tr>';
		            }
		            
		        }
		        $activities_table .= '</tr>';
		    }
		    else{
		      
		        if($numActivity%2 ==0){
		            $halfNumActivity = $numActivity/2;
		        }
		        else{
		            $halfNumActivity= ($numActivity+ 1)/2;
		          
		        }
		
		        if($maxECAsize < $numActivity && $maxECAsize!= -1){
		           if($maxECAsize%2 ==0){
		               $halfNumActivity = $maxECAsize/2;
		            }
		            else{
		                $halfNumActivity= ($maxECAsize+ 1)/2;		                
		            }
		       }
	
		        // 				$left_activities_table.='<td><table  class="size8table">';
		        for($x=0;$x < $halfNumActivity; $x++){
		            
		            $leftColNum = $x;
		            $rightColNum = $x+$halfNumActivity;
		            
// 		            $leftRoleName = $activityAssoArr[$leftColNum]['Role']==''?'':$activityAssoArr[$leftColNum]['Role'].' of ';
		            $leftRoleName = $activityAssoArr[$leftColNum]['Role']=='' || $activityAssoArr[$leftColNum]['Role']=='N/A'?'':$activityAssoArr[$leftColNum]['Role'].' of ';
// 		            $rightRoleName = $activityAssoArr[$rightColNum]['Role']==''?'':$activityAssoArr[$rightColNum]['Role'].' of ';		            
		            $rightRoleName = $activityAssoArr[$rightColNum]['Role']=='' || $activityAssoArr[$rightColNum]['Role']=='N/A'?'':$activityAssoArr[$rightColNum]['Role'].' of ';
		            
		            // 					$activities_table .= '<tr> <td width="5%"></td>';
		            if($maxECAsize== '-1'){
		                $left_activities_table .='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top" >'.$this->displayAcademicYear($activityAssoArr[$x]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;"  width="90%">'.$leftRoleName.$activityAssoArr[$leftColNum]['Title'].'</td></tr>';
		                $right_activities_table .='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top" >'.$this->displayAcademicYear($activityAssoArr[($x+$halfNumActivity)]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;" width="90%">'.$rightRoleName.$activityAssoArr[$rightColNum]['Title'].'</td></tr>';
		            }else if($maxECAsize>= $x && $maxECAsize< $rightColNum){
		                $left_activities_table.='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top" >'.$this->displayAcademicYear($activityAssoArr[$x]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;" width="90%">'.$leftRoleName.$activityAssoArr[$leftColNum]['Title'].'</td></tr>';
		                
		            }else if($maxECAsize>= $x && $maxECAsize>= $rightColNum){
		                $left_activities_table.='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top" >'.$this->displayAcademicYear($activityAssoArr[$x]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;" width="90%">'.$leftRoleName.$activityAssoArr[$leftColNum]['Title'].'</td></tr>';
		                
		                $right_activities_table.='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top" >'.$this->displayAcademicYear($activityAssoArr[($x+$halfNumActivity)]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;" width="90%">'.$rightRoleName.$activityAssoArr[$rightColNum]['Title'].'</td></tr>';
		                
		            }
		            // 					$activities_table .='</tr>';
		        }
		    }
		    if($right_activities_table !=''){
		        $right_activities_table .='<tr><td style="font-family:Arial; font-size:13px;" colspan="2">***</td></tr>';
		    }else{
		        $left_activities_table .='<tr><td style="font-family:Arial; font-size:13px;" colspan="2">***</td></tr>';
		    }
		    $activities_table .= '
                                <td width="50%" style="vertical-align: top; font-family:Arial; font-size:13px;" ><table class="size8table" >'.$left_activities_table.'</table></td>
                                <td width="4%"></td>
                                <td width="56%" style="vertical-align: top; font-family:Arial; font-size:13px;"><table class="size8table">'.$right_activities_table.'</table></td>';
		    $activities_table .= '</tr>';
		} 
		else{
		    $activities_table .= '<tr>
								<td colspan="4" align="center">No Record.</td>
							</tr>';
		}
		
		
		
		
		$activities_table .= '</table>';
		
		return $activities_table;
	}
	
	public function returnAwardsTable($maxPrizesize){
		
		$awardsInfoArr = $this->getAwards();
		$awards_table = '<table class="size8table"  width="110%">
								<tr>
									<th class="sectionHeader" colspan="4">Prizes, Scholarships and Special Achievements</th>
								</tr>';
		$numAwards = count($awardsInfoArr);
		$maxPrizesize= $maxPrizesize-1;
		
		if($numAwards > 0){
		    $awards_table .= '<tr>';
			if($numAwards<10){
				for($x=0;$x < $numAwards; $x++){
					
					$leftColNum = $x;
					if($maxPrizesize== '-1'){
					    $left_awards_table .= '<tr>
									<td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top">'.$this->displayAcademicYear($awardsInfoArr[$x]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;" width="90%"> '.$awardsInfoArr[$leftColNum]['AwardName'].'</td>
									</tr>';	
					}else if($maxPrizesize>= $x){
					    $left_awards_table .= '<tr>
									<td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top">'.$this->displayAcademicYear($awardsInfoArr[$x]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;" width="90%"> '.$awardsInfoArr[$leftColNum]['AwardName'].'</td>
									</tr>';						
					}
					
				}	
				$awards_table .= '</tr>';
			}
			else{
				if($numAwards%2 ==0){
					$halfNumAwards = $numAwards/2;
				}
				else{
					$halfNumAwards = ($numAwards + 1)/2;
				}
// 				$left_awards_table.='<td><table  class="size8table">';
				for($x=0;$x < $halfNumAwards; $x++){
					
					$leftColNum = $x;
					$rightColNum = $x+$halfNumAwards;
// 					$awards_table .= '<tr> <td width="5%"></td>';
					if($maxPrizesize == '-1'){
						$left_awards_table .='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top" >'.$this->displayAcademicYear($awardsInfoArr[$x]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;"  width="90%">'.$awardsInfoArr[$leftColNum]['AwardName'].'</td></tr>';
						$right_awards_table .='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top" >'.$this->displayAcademicYear($awardsInfoArr[($x+$halfNumAwards)]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;" width="90%">'.$awardsInfoArr[$rightColNum]['AwardName'].'</td></tr>';	
					}else if($maxPrizesize >= $x && $maxPrizesize < $rightColNum){
					    $left_awards_table.='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top" >'.$this->displayAcademicYear($awardsInfoArr[$x]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;" width="90%">'.$awardsInfoArr[$leftColNum]['AwardName'].'</td></tr>';
						
					}else if($maxPrizesize >= $x && $maxPrizesize >= $rightColNum){
					    $left_awards_table.='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top" >'.$this->displayAcademicYear($awardsInfoArr[$x]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;" width="90%">'.$awardsInfoArr[$leftColNum]['AwardName'].'</td></tr>';
						
					    $right_awards_table.='<tr><td style="font-family:Arial; font-size:13px;" width="10%" nowrap="nowrap" valign="top" >'.$this->displayAcademicYear($awardsInfoArr[($x+$halfNumAwards)]['AcademicYear']).'</td>
									<td style="font-family:Arial; font-size:13px;" width="90%">'.$awardsInfoArr[$rightColNum]['AwardName'].'</td></tr>';
						
					}			
// 					$awards_table .='</tr>';	
				}				
			}
			if($right_awards_table !=''){
			    $right_awards_table .='<tr><td style="font-family:Arial; font-size:13px;" colspan="2">***</td></tr>';
			}else{
			    $left_awards_table .='<tr><td style="font-family:Arial; font-size:13px;" colspan="2">***</td></tr>';
			}
			$awards_table .= ' 
                                <td width="50%" style="vertical-align: top; font-family:Arial; font-size:13px;" ><table class="size8table" >'.$left_awards_table.'</table></td>
                                <td width="4%"></td>
                                <td width="56%" style="vertical-align: top; font-family:Arial; font-size:13px;"><table class="size8table">'.$right_awards_table.'</table></td>';
			$awards_table .= '</tr>';
		}
		else{
			$awards_table .= '<tr>
								<td colspan="4" align="center">No Record.</td>
							</tr>';		
		}
		
		$awards_table .= '</table>';
		
		return $awards_table;
	}
	
	public function returnRemarks(){
		
		$teacherCommentArr = $this->getStudentComment();
		
		$remarks = '<table class="remarksTable">
						<tr>
							<th colspan="2" class="sectionHeader" style="font-family: Arial;">Remarks:</th>
						</tr>
						<tr>
							<td colspan="2" style="font-size:15px; font-family: Arial;">'.$teacherCommentArr['Remark'].' ***</td>
						</tr>
					</table>';
					
		return $remarks;
	}
	
	public function returnStudentCharacter(){
		
		$studentInfoArr = $this->getStudentInfo();
		
	//	$StudentCharacterArr = $this->getStudentCharacter($studentInfoArr['WebSAMSRegNo']);
		$StudentCharacterArr = $this->getSelectedStudentCharacter($studentInfoArr['userid']);
		
		$CharacterHeader = $this->getStudentCharacterHeader();
// /		debug_pr($StudentCharacterArr);
		$numCharacter = count($CharacterHeader);
		
		$character_tables = '<table>
								<tr>
									<td width="60%" style="padding:5mm 10mm 0mm 0mm">
										<table class="characterTable">
											<tr>
												<th width="75%">Character</th>
												<th width="25%">Grade</th>
											</tr>';
									for($x=0;$x<$numCharacter;$x++){
										$character_tables .= '<tr>
												<td>'.$CharacterHeader[$x].'</td>
												<td class="gradeCell">'.$StudentCharacterArr[$x].'</td>
											</tr>';
									}
		
		$character_Arr = array('A'=>'Excellent','B'=>'Very Good','C'=>'Good','D'=>'Fair','E'=>'Below Average');
		$character_tables .=	'</table>
									</td>
									<td style="padding:10mm mm" width="100mm">
										<table class="characterTable2" >
											<tr>
												<th width="1">&nbsp;Grade&nbsp;</th>
												<th class="DCell" >&nbsp;Description&nbsp;</th>
											</tr>';
								foreach($character_Arr as $_grade => $_description){
									$character_tables.=	'<tr>
													<td>'.$_grade.'</td>
													<td class="DCell" width="100mm" >&nbsp;'.$_description.'</td>
												</tr>';
								}
		$character_tables.=				'</table>
									</td>
								</tr>
							</table>';
		
		return $character_tables;
	}
	
	public function returnSignTable($issuedate=''){
		$studentInfoArr = $this->getStudentInfo();
	//	$photoAbsPath = $this->getUserPhoto($studentInfoArr['UserLogin']);
	//	$officialPhoto= '<img src="'.$photoAbsPath.'" width="115" height="150"/>';
	    $classTeacherNameArr = $this->getClassTeacherInfo();
	
// 		$sign_table = '<table>
// 						<tr>
// 							<td width="50%" align="center">
// 								'.$officialPhoto.'
// 							</td>
// 							<td width="50%">
// 								<table class="signTable" style="padding:0mm 10mm">
// 									<tr>
// 										<td class="signLine"></td>
// 									</tr>
// 									<tr>
// 										<td>Form Teacher</td>
// 									</tr>
// 									<tr>
// 										<td class="signLine"></td>
// 									</tr>
// 									<tr>
// 										<td>Principal</td>
// 									</tr>
// 									<tr>
// 										<td style="padding:10px 0px;">Date of issue: '.$issuedate.'</td>
// 									</tr>
// 								</table>
// 							</td>
// 						</tr>
// 					</table>';
		
		$sign_table = '<table class="signTable"  width="100%" >
						<tr>
							<td width="16%" align="right">Form Teacher: </td>
							<td class="signLine" width="33%">  </td>
							<td width="16%" align="right">Principal: </td>					
							<td class="signLine" width="33%">   </td>
							<td width="2%"></td>						
						</tr>
                        <tr>
							<td> </td>
							<td align="center">('.implode(', ', (array)$classTeacherNameArr).')</td>
							<td></td>						
							<td align="center">(AU YEUNG Suk Lan)</td>
							<td></td>					
						</tr>
                        <tr><td><br><br></td></tr> 
						<tr>

							<td style="padding:10px 0px 0 5px;" colspan="5">Date of issue: '.$issuedate.'</td>
						</tr>
					</table>';
		return $sign_table;
	}
	
	function returnPageFooter($page=''){
	    $pageFooter='<table class="pagefooter">
                        <tr>
                            <td>Page '.$page.' of 2</td>
                        </tr>
                    </table>';
	    return $pageFooter;
	}
//-- End of Class
}


?>