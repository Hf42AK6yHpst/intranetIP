<?php
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	private $All_AcademicYearID;
	
	private $StdNameCh;
	private $StdNameEn;
	
	private $StdRegNo;
	
	private $YearClassID;
	
	private $AcademicYearStr;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;
		
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '--';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#D0D0D0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->SchoolTitleEn = 'NG WAH CATHOLIC SECONDARY SCHOOL';
		$this->SchoolTitleCh = '天主教伍華中學';
		
		$this->StdNameCh = '';
		$this->StdNameEn = '';
		
		$this->StdRegNo = '';
	}
	
	
	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	public function setAll_AcademicYearID($All_AcademicYearID)
	{
		$this->All_AcademicYearID = $All_AcademicYearID;
	}
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	

	
	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '13';
		$spaceStr = '&nbsp;&nbsp;&nbsp;&nbsp;';
	
		$ChiAddress = '香港九龍新蒲崗彩虹道五號';
		$EngAddress = '5 CHOI HUNG ROAD, SAN PO KONG, KOWLOON, HONG KONG';
		$TelStr = '電話 Tel.: 2383 8077'.$spaceStr.'傳真 Fax: 2718 2543';
	
		$IssueDate = $this->issuedate;
		
		if (date($IssueDate)==0) {
			$IssueDate = $this->EmptySymbol;
		} else {
			$IssueDate = date('d/m/Y',strtotime($IssueDate));
		}
		
	
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:center; border:0px;">
					<tr>
						<td rowspan="4" align="center" " style="width:18%;">&nbsp;</td>
						<td>'.$this->getSchoolHeader(false).'</td>
						<td rowspan="4" align="left" style="width:18%;">&nbsp;</td>
				   </tr>';
			
			$x .= '<tr><td style="font-size:'.$header_font_size.'px;"><font face="Arial"><b>'.$ChiAddress.'</b></font></td></tr>';
			$x .= '<tr><td style="font-size:'.$header_font_size.'px;"><font face="Arial"><b>'.$EngAddress.'</b></font></td></tr>';
			$x .= '<tr><td style="font-size:'.$header_font_size.'px;"><font face="Arial"><b>'.$TelStr.'</b></font></td></tr>';
		$x .= '</table>'."\r\n";
		
		$x .= $this ->getEmptyTable('2%');
		
		$x .= '<table align="center" cellpadding="0" style="width:100%; text-align:center; border:0px;">
					<tr><td style="font-size:18px;"><font face="Arial"><b>學生學習概覽 STUDENT LEARNING PROFILE</b></font></td></tr>
			   </table>';
			   
		$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:center; border:0px;">
					<tr><td style="font-size:12px;text-align:right;"><font face="Arial">頁 P.1/4</font></td></tr>';
			$x .= '</table>';
			
		$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:right; border:0px;">
					<tr><td style="font-size:15px;"><font face="Arial">發出日期 Date of Issue: '.$IssueDate.'</font></td></tr>
			   </table>';
		return $x;
	}
	
	public function getSchoolHeader($hasSLPName=true,$PageNo='')
	{
		$header_font_size = '20';
	
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:100%; text-align:center; border:0px;">
					<tr><td style="font-size:'.$header_font_size.'px;"><font face="Arial"><b>'.$this->SchoolTitleCh.'</b></font></td></tr>
					<tr><td style="font-size:'.$header_font_size.'px;"><font face="Arial"><b>'.$this->SchoolTitleEn.'</b></font></td></tr>';
			if($hasSLPName)
			{
				$x .= '<tr><td style="font-size:15px;"><font face="Arial"><b>學生學習概覽 STUDENT LEARNING PROFILE</b></font></td></tr>';
			}

		$x .= '</table>'."\r\n";
		
		if($hasSLPName)
		{	
			$stdInfo_font_size = '16';
			
			$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:center; border:0px;">
					<tr><td style="font-size:12px;text-align:right;"><font face="Arial">頁 P.'.$PageNo.'/'.'4</font></td></tr>';
			$x .= '</table>';
			
			
			$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:left; border:0px;">
					  <tr>
						  <td style="font-size:'.$stdInfo_font_size.'px;"><font face="Arial">學生姓名:</font></td>
						  <td style="font-size:'.$stdInfo_font_size.'px;"><font face="Arial">'.$this->StdNameCh.'</font></td>
						  <td style="font-size:'.$stdInfo_font_size.'px;"><font face="Arial">學生註冊編號:</font></td>
						  <td style="font-size:'.$stdInfo_font_size.'px;"><font face="Arial">&nbsp;</font></td>
					  </tr>
					  <tr>
						  <td style="font-size:'.$stdInfo_font_size.'px;"><font face="Arial">Student Name:</font></td>
						  <td style="font-size:'.$stdInfo_font_size.'px;"><font face="Arial">'.$this->StdNameEn.'</font></td>
						  <td style="font-size:'.$stdInfo_font_size.'px;"><font face="Arial">Student Reg. No.:</font></td>
						  <td style="font-size:'.$stdInfo_font_size.'px;"><font face="Arial">'.$this->StdRegNo.'</font></td>
					  </tr>
					  <tr>
						  
					  </tr>';
			$x .= '</table>';
			$x .= $this ->getEmptyTable('1%');
		}
		
		return $x;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	public function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						ChineseName,
						HKID,
						DateOfBirth,
						Gender,
						STRN,
						ClassNumber,
						ClassName
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";

		$result = $this->objDB->returnResultSet($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result)
	{		
		$fontSize = 'font-size:15px;';
		
		$thisPartTitle = '學生資料 Student Particulars';
		
		$SchoolNumStr ='00000';
		
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		
		$DOB = $result[0]["DateOfBirth"];
		if (date($DOB)==0) {
			$DOB = $this->EmptySymbol;
		} else {
			$DOB = date('d/m/Y',strtotime($DOB));
		}
		
		$EngName = strtoupper($result[0]["EnglishName"]);
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;		
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $this->EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];		
		
		if($Gender=='M')
		{
			$GenderEng ='Male';
			$GenderChi ='男';
		}
		else if($Gender=='F')
		{
			$GenderEng ='Female';
			$GenderChi ='女';
		}
		
		$GenderEng = ($GenderEng=='')? $this->EmptySymbol:$GenderEng;
		$GenderChi = ($GenderChi=='')? $this->EmptySymbol:$GenderChi;
		
		$RegistrationNo = $result[0]["STRN"];
		$RegistrationNo = ($RegistrationNo=='')? $this->EmptySymbol:$RegistrationNo;
		
		$ClassNumber = $result[0]["ClassNumber"];
		$ClassNumber = ($ClassNumber=='')? $this->EmptySymbol:$ClassNumber;
		
		$ClassName = $result[0]["ClassName"];
		$ClassName = ($ClassName=='')? $this->EmptySymbol:$ClassName;

		$this->StdNameCh = $ChiName;
		$this->StdNameEn = $EngName;
		
		$this->StdRegNo = $RegistrationNo;

		$html = '';
		$html .= '<table width="90%" cellpadding="1" border="0px" align="center" style="border-collapse: collapse;">
					<col width="15%" />
					<col width="40%" />
					<col width="20%" />
					<col width="15%" />

					<tr>
							<td bgcolor="#E8E8E8" style="font-size:17px;'.$_paddingLeft.$_borderLeft.$_borderRight.$_borderTop.'" colspan="4"><font face="Arial"><b>'.$thisPartTitle.'</b></font></td>
					</tr>
					
					<tr>
							<td style="'.$_borderTop.$_borderLeft.' '.$_borderRight.'" colspan="4">&nbsp;</td>
					</tr>
					<tr>
							<td style="'.$_borderLeft.' '.$fontSize.$_paddingLeft.'" ><font face="Arial">學生姓名:</font></td>
							<td style="'.$fontSize.'" ><font face="Arial">'.$ChiName.'</font></td>					
							<td style="'.$fontSize.'"><font face="Arial">香港身份證號碼:</font></td>
							<td style="'.$_borderRight.'">&nbsp;</td>
					</tr>				

					<tr>
							<td style="'.$_borderLeft.$fontSize.$_paddingLeft.'" ><font face="Arial">Student Name:</font></td>
							<td style="'.$fontSize.'" ><font face="Arial">'.$EngName.'</font></td>					
							<td style="'.$fontSize.'"><font face="Arial">HKID No.:</font></td>
							<td style="'.$_borderRight.'"><font face="Arial">'.$HKID.'</font></td>
					</tr>

					<tr>
							<td style="'.$_borderLeft.' '.$_borderRight.'" colspan="4">&nbsp;</td>
					</tr>

					<tr>
							<td style="'.$_borderLeft.$fontSize.$_paddingLeft.'"><font face="Arial">性別:</font></td>
							<td style="'.$fontSize.'" ><font face="Arial">'.$GenderChi.'</font></td>	
							<td style="'.$fontSize.'"><font face="Arial">學生註冊編號:</font></td>
							<td style="'.$_borderRight.'">&nbsp;</td>
					</tr>
					<tr>
							<td style="'.$_borderLeft.$fontSize.$_paddingLeft.'"><font face="Arial">Sex:</font></td>
							<td style="'.$fontSize.'" ><font face="Arial">'.$GenderEng.'</font></td>	
							<td style="'.$fontSize.'"><font face="Arial">Student Reg. No.:</font></td>
							<td style="'.$_borderRight.'"><font face="Arial">'.$RegistrationNo.'</font></td>
					</tr>

					<tr>
							<td style="'.$_borderLeft.' '.$_borderRight.'" colspan="4">&nbsp;</td>
					</tr>

					<tr>
							<td style="'.$_borderLeft.$fontSize.$_paddingLeft.'"><font face="Arial">出生日期:</font></td>
							<td style="'.$fontSize.'">&nbsp;</td>
							<td style="'.$fontSize.'"><font face="Arial">學校編號:</font></td>
							<td style="'.$_borderRight.'">&nbsp;</td>
					</tr>
					<tr>
							<td style="'.$_borderLeft.$fontSize.$_paddingLeft.'"><font face="Arial">Date of Birth:</font></td>
							<td style="'.$fontSize.'" ><font face="Arial">'.$DOB.'</font></td>	
							<td style="'.$fontSize.'"><font face="Arial">Student No.:</font></td>
							<td style="'.$_borderRight.'"><font face="Arial">'.$SchoolNumStr.'</font></td>
					</tr>   
					<tr>
							<td style="'.$_borderBottom.$_borderLeft.' '.$_borderRight.'" colspan="4">&nbsp;</td>
					</tr>

					';

		$html .= '</table>';
	
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	
	/***************** Part 3 : Academic Performance******************/	
	public function getPart3_HTML($SubjectArr,$AcademicResultInfoArr,$FullMarkArr){
		
		$Part3_Title = '校內學科成績  Academic Performance in School';
		$Part3_Remark = '&nbsp;';

		$html = '';
	
		$html .= $this ->getAcademicResultDisplay($Part3_Title,$SubjectArr,$AcademicResultInfoArr,$FullMarkArr,$hasPageBreak=false);
		
		return $html;	
	}		

	public function getAcademicResultDisplay($mainTitle,$SubjectArr,$AcademicResultInfoArr,$FullMarkArr,$hasPageBreak=false)
	{
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';			
  		
//debug_r($FullMarkArr);
		
		$Page_break='page-break-after:always;';	
		if($hasPageBreak==false)
		{
			$Page_break = '';
		}
		
		$table_style = 'style="font-size:12px;border-collapse: collapse; border:1px solid #000000;'.$Page_break.'"';
		
		$LastYearAcademicID = '';
		$LastYearClassID = '';
		
		$rowMax_count = count(current($SubjectArr));

		$rowHight = 10;
		$text_alignStr = 'center';

		$noRecordFound_html = '';

		$AcademicID_Array = $this->AcademicYearStr;

		$countCol = 3+2;
		
		$ClassNameArr = $this->getAcademicClassName();		
		
		$NewSubjectSequenceArr = array();
		$countMAX_NewSubjectSequenceArr = 0;
	
		$YearClassArray = $this->getStudentAcademic_YearClassNameArr(true);
		
		$AcademicYearArray = $this->getAcademicYearArr($this->All_AcademicYearID);
		$YearClassArray = $this->getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,'asc');


		for($i=0,$i_MAX=count($SubjectArr);$i<$i_MAX;$i++)
		{
			$thisSubjectNameEn = $SubjectArr[$i]["SubjectDescEN"];
			$thisSubjectNameCh = $SubjectArr[$i]["SubjectDescB5"];
			
			$thisSubjectID = $SubjectArr[$i]["SubjectID"];
			
			$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = $thisSubjectNameEn;
			$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameCh'] = $thisSubjectNameCh;
					
			$SubjectAcademicResultArr = $AcademicResultInfoArr[$thisSubjectID];

			$hasStudy = false;
		
			$count_AcademicID_Array = 0; 
	
			for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
			{			
				
				$thisYear = $YearClassArray[$i]['AcademicYear'];
				$thisClassName = $YearClassArray[$i]['ClassName'];
				
				$_academicID =array_search($thisYear,$AcademicYearArray);
			
				$thisFormNo = (int)$thisClassName;
				preg_match('/\d+/', $thisClassName, $number);
				
				if(count($number)>0 && is_array($number))
				{
					$thisFormNo = $number[0];		
				}
				
				$thisScore = $SubjectAcademicResultArr[$_academicID]['Score'];
			
				$_yearID = $ClassNameArr[$_academicID]['YearID'];
				  
				if($count_AcademicID_Array==0)
				{
					$LastYearAcademicID = $_academicID;
					$LastYearClassID = $_yearID;
				}
				
				if($thisScore!='')
				{
					$hasStudy = true;
				}
				
				$NewSubjectSequenceArr[$thisSubjectID]['Score'][$_academicID] = $thisScore;
				
				$count_AcademicID_Array++;
			}
			$_fullMark = $FullMarkArr[$LastYearAcademicID][$LastYearClassID][$thisSubjectID]['FullMarkGrade'];

			$NewSubjectSequenceArr[$thisSubjectID]['fullMark'] = $_fullMark;
			
			$NewSubjectSequenceArr[$thisSubjectID]['hasStudy'] = $hasStudy;
			
			if($hasStudy==true)
			{
				$countMAX_NewSubjectSequenceArr++;
			}

		}	
		
		$subTitle_html = '';
		
		$MarkInSchoolStr = '校內表現<br/>Mark/Performance<br/>in School';
		
		$subTitle_html = '';
		$subTitle_html .='<tr>';
		$subTitle_html .= ' <td style="height:'.$rowHight.'px;text-align:center;vertical-align: center;'.$_borderLeft.'"><font face="Arial"><b>科目<br/>Subject</b></font></td>';
		$subTitle_html .= ' <td style="height:'.$rowHight.'px;text-align:center;vertical-align: center;"><font face="Arial"><b>滿分<br/>Full Mark</b></font></td>';
		
		$count_AcademicID_Array = 0;
		
		
		for ($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{							
			$thisYear = $YearClassArray[$i]['AcademicYear'];
			$thisClassName = $YearClassArray[$i]['ClassName'];
			
			$_academicID =array_search($thisYear,$AcademicYearArray);
			
			$thisFormNo = (int)$thisClassName;
			
			preg_match('/\d+/', $thisClassName, $number);
				
			if(count($number)>0 && is_array($number))
			{
				$thisFormNo = $number[0];		
			}
			
			$thisBorder='';
			
			if($i==($i_MAX-1))
			{
				$thisBorder = $_borderRight;
			}
			
			$_className = $ClassNameArr[$_academicID]['ClassName'];
			$_yearID = $ClassNameArr[$_academicID]['YearID'];
						

			$thisClassName = '中'.$this->ch_num($thisFormNo).' '.'S'.$thisFormNo;
		
			$subTitle_html .= ' <td style="height:'.$rowHight.'px;text-align:center;vertical-align: top;'.$thisBorder.'"><font face="Arial"><b>'.$thisYear.'<br/>'.$thisClassName.'<br/>'.$MarkInSchoolStr.'</b></font></td>';
		
		
			$count_AcademicID_Array++;
		}
		$subTitle_html .='</tr>';
		
		
		$content_html = '';
		$count_SubjectArray = 0;		
		foreach((array)$NewSubjectSequenceArr as $thisSubjectID=>$thisSubjectResultInfoArr)
		{		
			if($NewSubjectSequenceArr[$thisSubjectID]['hasStudy']==false)
			{
				continue;
			}
			
			$thisBorderRight='';
			$thisBorderBottom='';
			
			if($count_SubjectArray==($countMAX_NewSubjectSequenceArr-1))
			{
				$thisBorderRight = $_borderRight;
				$thisBorderBottom = $_borderBottom;
			}
			
			$content_html .='<tr>';
			$thisSubjectNameEn = $thisSubjectResultInfoArr['SubjectNameEn'];
			$thisSubjectNameCh = $thisSubjectResultInfoArr['SubjectNameCh'];
			
			$thisFullMark = $thisSubjectResultInfoArr['fullMark'];
			
			$content_html .= ' <td style="height:'.$rowHight.'px;text-align:left;vertical-align: top;'.$_borderLeft.$thisBorderBottom.'"><font face="Arial">'.$thisSubjectNameCh.'<br/>'.$thisSubjectNameEn.'</font></td>';		
			$content_html .= ' <td style="height:'.$rowHight.'px;text-align:left;vertical-align: top;'.$thisBorderBottom.'"><font face="Arial">'.$thisFullMark.'</font></td>';
			
			$SubjectAcademicResultArr = $AcademicResultInfoArr[$thisSubjectID];

			$count_AcademicID_Array = 0;
	
			for ($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
			{			
					
				$thisYear = $YearClassArray[$i]['AcademicYear'];
				$thisClassName = $YearClassArray[$i]['ClassName'];
				
				$_academicID =array_search($thisYear,$AcademicYearArray);
		
				$thisFormNo = (int)$thisClassName;
				
				preg_match('/\d+/', $thisClassName, $number);
				
				if(count($number)>0 && is_array($number))
				{
					$thisFormNo = $number[0];		
				}
				
				$thisBorderRight = '';
				if($i==($i_MAX-1))
				{
					$thisBorderRight = $_borderRight;
				}
			
						
				$thisScore = $thisSubjectResultInfoArr['Score'][$_academicID];
				$content_html .= ' <td style="height:'.$rowHight.'px;text-align:center;vertical-align: top;'.$thisBorderRight.$thisBorderBottom.'"><font face="Arial">'.$thisScore.'</font></td>';
			
				$count_AcademicID_Array++;
			}
			$count_SubjectArray++;			
			$content_html .='</tr>';
		}

		if($content_html=='')
		{				
			$content_html .='<tr>';
			$content_html .= ' <td colspan="'.$countCol.'" style="height:'.$rowHight.'px;text-align:center;vertical-align: top;'.$_borderTop.$_borderLeft.$_borderRight.$_borderBottom.'"><font face="Arial">No Record Found.</font></td>';
			$content_html .='</tr>';
		}
		
		/****************** Main Table ******************************/
		
		$html = '';

		$html .= '<table width="90%" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>';

		  $html .= '<tr> 
						<td colspan="'.$countCol.'"  bgcolor="#E8E8E8" style=" font-size:15px;text-align:left;vertical-align:text-top;'.$_borderTop.$_borderLeft.$_borderRight.$_borderBottom.'"><font face="Arial"><b>
						'.$mainTitle.'
						</b></font></td>
				  	</tr>';
		$html .=$subTitle_html;
		$html .= $content_html;
		
		$html .='</table>';

			
		return $html;		
	}

	private function getAcademicClassName()
	{
		$_studentID = $this->uid;
		
		$Profile_his = $this->Get_IntranetDB_Table_Name('PROFILE_CLASS_HISTORY');
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$SelectStr= 'h.AcademicYearID as AcademicYearID,
					 h.ClassName as ClassName,
					 h.YearClassID as YearID';
		
		$academicYearIDArr = $this->academicYearIDArr;
		$cond_academicYear = "And h.AcademicYearID In ('".implode("','",(array)$academicYearIDArr)."')";		
		
		$sql = 'select 
						'.$SelectStr.' 
				from 
						'.$Profile_his.' as h 
				where 
						h.UserID = "'.$_studentID.'"
						'.$cond_academicYear.'
				Order by
						h.AcademicYear asc';
						
		$roleData = $this->objDB->returnResultSet($sql);
		$returnArr = array();
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$thisAcademicYearID = $roleData[$i]['AcademicYearID'];
			$thisClassName =  $roleData[$i]['ClassName'];
			$thisYearID =  $roleData[$i]['YearID'];
			$returnArr[$thisAcademicYearID] = array('ClassName'=>$thisClassName,'YearID'=>$thisYearID);
		}
//debug_r($sql);
		return $returnArr;
		
	}
		
	/***************** End Part 3 ******************/
	
	/***************** Part 4 ******************/	
	public function getPart4_HTML($OLEInfoArr){
		
		$Part4_Title = '其他學習經歷  Other Learning Experiences';
		
		$titleArray = array();
		$titleArray[] = '學年<br/>School Year';
		$titleArray[] = '活動項目<br/>Programmes';
		$titleArray[] = '參與角色<br/>Role';
		$titleArray[] = '合辦機構 (如有)<br/>Partner Organisations';
		$titleArray[] = '獎項 / 成就<br/>Awards /<br/>Achievements';
		$titleArray[] = '其他學習經歷範疇<br/>O.L.E. Components';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="9%" />';
		$colWidthArr[] = '<col width="35%" />';
		$colWidthArr[] = '<col width="5%" />';
		$colWidthArr[] = '<col width="17%" />';
		$colWidthArr[] = '<col width="17%" />';
		$colWidthArr[] = '<col width="17%" />';

		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('4',$Part4_Title,$titleArray,$colWidthArr,$OLEInfoArr,$hasPageBreak=false);
		
		return $html;
		
	}

	/***************** End Part 4 ******************/
	
	
	
	/***************** Part 5 : Other Learning Experience******************/	
	public function getPart5_HTML(){
		
		$Part5_Title = '校內頒發的主要獎項及成就   List of Awards and Major Achievements Issued by the School';
				
		$titleArray = array();
		$titleArray[] = '學年 School Year';
		$titleArray[] = '獎項及成就   Awards and Achievements';
		$titleArray[] = '備註  Remarks';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="53%" />';
		$colWidthArr[] = '<col width="32%" />';
		
		$DataArray = array();
		$DataArray = $this->getAward_Info();  

		$html = '';
	
		$html .= $this->getPartContentDisplay_NoRowLimit('5',$Part5_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false);
		
		return $html;
		
	}	
	   
	private function getAward_Info(){
		
		global $ipf_cfg;
		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'"; 
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And s.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$cond_limit ='limit 8';
		
		$sql = "Select
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						AwardName,
						Remark
				From
						$Award_student s
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = s.AcademicYearID
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence asc, 
						AwardName asc		
				$cond_limit
				";
		$roleData = $this->objDB->returnResultSet($sql);

		return $roleData;
	}
	
	/***************** End Part 5 ******************/

	/***************** Part 6 : Performance / Awards Gained Outside School ******************/
	public function getPart6_HTML($OLEInfoArr){
		
		$Part6_Title = '校外的表現 / 獎項   Performance / Awards Gained Outside School';
		
		$titleArray = array();
		$titleArray[] = '學年<br/>School Year';
		$titleArray[] = '活動項目<br/>Programmes';
		$titleArray[] = '參與角色<br/>Role';
		$titleArray[] = '主辦機構<br/>Organisation';
		$titleArray[] = '獎項/成就<br/>Awards / Achievements';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="10%" />';
		$colWidthArr[] = '<col width="40%" />';
		$colWidthArr[] = '<col width="10%" />';
		$colWidthArr[] = '<col width="20%" />';
		$colWidthArr[] = '<col width="20%" />';

		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('6',$Part6_Title,$titleArray,$colWidthArr,$OLEInfoArr,$hasPageBreak=false);
		
		return $html;
		
	}
	/***************** End Part 6 ******************/
	
	/***************** Part 7 ******************/
	
	public function getPart7_HTML(){
		
		$Part7_Title = "學生的自述 Students' Self-Account";
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$Part7_Title) ;
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnResultSet($sql);
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			
			$returnDate[] = $thisDataArr;
			
		}
		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$MainTitle='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:16px;border:1px solid #000000;" '; 


		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];
			
			$Detail = strip_tags($Detail,'<p><br/>');
		}
		else
		{
			$Detail = 'No Record Found.';
		}	

		$html .= '<table width="90%" height="800px"  cellpadding="7" border="0px" align="center" '.$table_style.'>
					<tr> 
						<td bgcolor="#E8E8E8" style="height:5px;font-size:15px;text-align:left;vertical-align:text-top;'.$_borderBottom.'"><font face="Arial"><b>
						'.$MainTitle.'
						</font></b></td>
				  	</tr>
					<tr>				 							
						<td style="vertical-align: top;"><font face="Arial">'.$Detail.'</font></td>						 														
					</tr>
				  </table>';	
		
			
		return $html; 
	}
	
	
	/***************** End Part 7 ******************/
	
	

	
	
	private function getPartContentDisplay_NoRowLimit($PartNum, $mainTitle,$subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false) 
	{
				
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderBottom_Dotted =  'border-bottom: 1px dotted black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		$BgColor = $this->TableTitleBgColor;
		
		$Page_break='page-break-after:always;';
		
		if($hasPageBreak==false)
		{
			$Page_break='';
		}
		
	//	$table_style = 'style="font-size:10px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 
		$table_style = 'style="font-size:10px;border-collapse: collapse;'.$Page_break.'"';
		
		$subtitle_html = '';
				
		$count_subTitleArr=0;
		foreach((array)$subTitleArr as $key=>$_title)
		{
			$text_alignStr='left';
			
			$thisBorder='';
			if($count_subTitleArr==0)
			{
				$thisBorder = $_borderLeft;
			}
			else if($count_subTitleArr==(count($subTitleArr)-1))
			{
				$thisBorder = $_borderRight;
			}
//vertical-align:text-top;

			$subtitle_html .='<td bgcolor="#E8E8E8" style="text-align:'.$text_alignStr.';'.$_borderBottom.$thisBorder.'"><font face="Arial"><b>';
			$subtitle_html .=$_title;
			$subtitle_html .='</b></font></td>';
			
			$count_subTitleArr++;		
		}	
					
		
		$rowHight = 30;

		if($PartNum=='4')
		{
			$total_Record=20;
		}
		else if($PartNum=='5')
		{
			$total_Record=8;
		}
		else if($PartNum=='6')
		{
			$total_Record=8;
		}
		

		$text_alignStr='center';
		$content_html='';	
		$rowMax_count = 0;
		
		if(count($DataArr)>0)
		{
			$rowMax_count = count($DataArr);
		}	
		
		$count_col =count($colWidthArr);


		if($rowMax_count>0)
		{
			for ($i=0; $i<$rowMax_count; $i++)
			{		
				$thisBorderDot = $_borderBottom_Dotted;
				if($i==($total_Record-1))
				{
					$thisBorderDot = $_borderBottom;
				}
				
				$content_html .= '<tr>';	
					
				$count_DataArr=0;		 
				foreach((array)$DataArr[$i] as $_title=>$_titleVal)
				{
					if($_titleVal=='')
					{
						$_titleVal =  $this->EmptySymbol;
					}
					
					$thisBorder='';
					if($count_DataArr==0)
					{
						$thisBorder = $_borderLeft;
					}
					else if($count_DataArr==(count($DataArr[$i])-1))
					{
						$thisBorder = $_borderRight;
					}
					
					$text_alignStr = 'left';
					
					$content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$thisBorder.$thisBorderDot.'"><font face="Arial">'.$_titleVal.'</font></td>';								
					
					
					$count_DataArr++;
				}				
				$content_html .= '</tr>';		
			}
		}
//		else
//		{
//			$content_html = '<tr>';
//			$content_html .= ' <td colspan="'.$count_col.'" style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderTop.$_borderRight.$_borderBottom.'"><font face="Arial">No Record Found.</font></td>';
//			$content_html .= '</tr>';
//		}
		
		
		$remainRow = $total_Record-$rowMax_count;
		for($i=0,$i_MAX=$remainRow;$i<$i_MAX;$i++)
		{

			$thisContent = '&nbsp;';
			
			$thisBorderDot = $_borderBottom_Dotted;
			if($i==($i_MAX-1))
			{
				$thisBorderDot = $_borderBottom;
			}
			
			$text_alignStr='center';
			$content_html .= '<tr>';					
			for($j=0;$j<$count_col;$j++)
			{
				$thisBorder='';			
				
				if($j==0)
				{
					$thisBorder = $_borderLeft;
				}
				else if($j==($count_col)-1)
				{
					$thisBorder = $_borderRight;				
				}				
				$content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$thisBorder.$thisBorderDot.'"><font face="Arial">'.$thisContent.'</font></td>';
			}				
			$content_html .= '</tr>';
		}					
		
		$colNumber = count($colWidthArr);
			
		/****************** Main Table ******************************/
		
		$html = '';

		$html .= '<table width="90%" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$html .=$_colWidthInfo;
					}
		$html .= '<tr> 
						<td colspan="'.$colNumber.'"  bgcolor="#E8E8E8" style=" font-size:15px;text-align:left;vertical-align:text-top;'.$_borderTop.$_borderLeft.$_borderRight.'"><font face="Arial"><b>
						'.$mainTitle.'
						</b></font></td>
				  	</tr>';
		$html .= '<tr>'.$subtitle_html.'</tr>';
		
		$html .= $content_html;
		
		$html .='</table>';

			
		return $html;
	}
	
	

	
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false)
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		$OLE_Cate = $this->Get_eClassDB_Table_Name('OLE_CATEGORY'); 
		
		$OLEcomponentArr = $this->getOLEcomponentName();
		
		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = " And SLPOrder is not null"; 
		}
	
		if($IntExt!='')
		{
			$cond_IntExt = " And ole_prog.IntExt='".$IntExt."'";
		}
		
		if(strtoupper($IntExt)=='INT')
		{
			$cond_limit = 'limit 20';
		}
		else if(strtoupper($IntExt)=='EXT')
		{
			$cond_limit = 'limit 8';
		}
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = " And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
		
		$sql = "Select
						ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_prog.ELE as OleComponent
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID
				Inner join
						$OLE_Cate as ole_cate
				On
						ole_prog.Category = ole_cate.RecordID
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
				order by 
						y.Sequence asc,	
						ole_prog.Title asc		
				$cond_limit
				";
		$roleData = $this->objDB->returnResultSet($sql);

		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$_StudentID = $roleData[$i]['StudentID'];
			$_IntExt = $roleData[$i]['IntExt'];
			$thisAcademicYear = $roleData[$i]['YEAR_NAME'];
			
			$thisOleComponentArr = explode(',',$roleData[$i]['OleComponent']);
		
			for($a=0,$a_MAX=count($thisOleComponentArr);$a<$a_MAX;$a++)
			{
				$thisOleComponentID = $thisOleComponentArr[$a];		
				$thisOleComponentArr[$a] = $OLEcomponentArr[$thisOleComponentID];
			}
			$thisOLEstr = implode("<br/>",(array)$thisOleComponentArr);

			$thisDataArr = array();
			
			if(strtoupper($IntExt)=='INT')
			{
				$thisDataArr['SchoolYear'] =$thisAcademicYear;	
				$thisDataArr['Program'] =$roleData[$i]['Title'];						
				$thisDataArr['Role'] =$roleData[$i]['Role'];
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
				$thisDataArr['OLEComponent'] =$thisOLEstr;
				
			}
			else if(strtoupper($IntExt)=='EXT')
			{
				$thisDataArr['SchoolYear'] =$thisAcademicYear;	
				$thisDataArr['Program'] =$roleData[$i]['Title'];						
				$thisDataArr['Role'] =$roleData[$i]['Role'];
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
			}
					
		//	$returnDate[$_StudentID][$_IntExt][] = $thisDataArr;
			$returnDate[] = $thisDataArr;
		}
		return $returnDate;
	}
	
	public function getOLEcomponentName()
	{
		$OLE_ELE = $this->Get_eClassDB_Table_Name('OLE_ELE');
		
		$sql="Select 
					EngTitle,
					ChiTitle,
					DefaultID 
			  from
					$OLE_ELE
			";
		$roleData = $this->objDB->returnResultSet($sql);
		
		$returnArray = array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$thisName_en = $roleData[$i]['EngTitle'];
			$thisName_ch = $roleData[$i]['ChiTitle'];
			$thisID = $roleData[$i]['DefaultID'];
			
			$returnArray[$thisID] =$thisName_en;
		}
	
		return $returnArray;
	}
	
	/********** Display Form 4, 5 and 6  ***********/
	private function getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,$order='asc')
	{
		
		$YearDisplayArr = array();
	
		$Form4_no = 0;
		$Form5_no = 0;
		$Form6_no = 0;
		

		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{						
			$thisYear = $YearClassArray[$i]['AcademicYear'];
			$thisClassName = $YearClassArray[$i]['ClassName'];
			
			$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
			
			$thisFormNo = (int)$thisClassName;
			
			preg_match('/\d+/', $thisClassName, $number);
			
			if(count($number)>0 && is_array($number))
			{
				$thisFormNo = $number[0];
				
				if($order=='asc')
				{
					if($thisFormNo==4 || $thisFormNo==5 || $thisFormNo==6)
					{				
						$YearDisplayArr[$thisFormNo] = $YearClassArray[$i];
					}
				}
				else if($order=='desc')
				{
					$CondArr = array();
					$CondArr[] = ($thisFormNo==4 && $Form4_no==0);
					$CondArr[] = ($thisFormNo==5 && $Form5_no==0);
					$CondArr[] = ($thisFormNo==6 && $Form6_no==0);
					
					if(in_array(true,$CondArr))
					{
						$YearDisplayArr[$thisFormNo] = $YearClassArray[$i];
					}
				}			
				
				if($thisFormNo==4)
				{
					$Form4_no++;
				}
				else if($thisFormNo==5)
				{
					$Form5_no++;
				}
				else if($thisFormNo==6)
				{
					$Form6_no++;
				}
				
				
			}
		}	
		
		$ThisClassArr = array();
		$count_YearDisplayArr = 0;
		foreach((array)$YearDisplayArr as $_formNo=>$_ClassArr)
		{
			$ThisClassArr[$count_YearDisplayArr] = $_ClassArr;
			$count_YearDisplayArr++;
		}
		
		return $ThisClassArr;
	}
	
	public function getStudentAcademic_YearClassNameArr($AllYear=false)
	{
		$StudentID = $this->uid;
		
		$academicYearIDArr = $this->academicYearIDArr;
		
		if($AllYear)
		{
			$academicYearIDArr = $this->All_AcademicYearID;
		}
		
		$SelectStr= 'h.AcademicYear as AcademicYear,
					 h.ClassName as ClassName';
		
		if($academicYearIDArr!='')
		{		
			$YearArr = array();
			for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
			{
				if($academicYearIDArr[$i]=='')
				{
					continue;
				}
				$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
				$YearArr[] = $thisAcademicYear;
			}
			
			$cond_academicYear = "And h.AcademicYear In ('".implode("','",(array)$YearArr)."')";		
		}
		

		$Profile_his = $this->Get_IntranetDB_Table_Name('PROFILE_CLASS_HISTORY');
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$sql = 'select 
						'.$SelectStr.' 
				from 
						'.$Profile_his.' as h 
				where 
						h.UserID = "'.$StudentID.'"
						'.$cond_academicYear.'
				Order by
						h.AcademicYear asc';
						
		$roleData = $this->objDB->returnResultSet($sql);
		
	
		return $roleData;
	}
	
	private function getAcademicYearArr($academicYearIDArr)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		asort($returnArray);	
		return $returnArray;
	}
	
	public function getEmptyTable($Height='',$Content='&nbsp;') {
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td style="text-align:center"><font face="Arial">'.$Content.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	private function ch_num($num,$mode=true)
	{
		$char = array("零","一","二","三","四","五","六","七","八","九");
		$dw = array("","十","百","千","","萬","億","兆");
		$dec = "點";
		$retval = "";
		if($mode)
		preg_match_all("/^0*(\d*)\.?(\d*)/",$num, $ar);
		else
		preg_match_all("/(\d*)\.?(\d*)/",$num, $ar);
		if($ar[2][0] != "")
		$retval = $dec . ch_num($ar[2][0],false); //如果有小數，先遞歸處理小數
		if($ar[1][0] != "") {
		$str = strrev($ar[1][0]);
		for($i=0;$i<strlen($str);$i++) {
		$out[$i] = $char[$str[$i]];
		if($mode) {
		$out[$i] .= $str[$i] != "0"? $dw[$i%4] : "";
		if($str[$i]+$str[$i-1] == 0)
		$out[$i] = "";
		if($i%4 == 0)
		$out[$i] .= $dw[4+floor($i/4)];
		}
		}
		$retval = join("",array_reverse($out)) . $retval;
		}
		return $retval;
	}
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>