<?php
function BuildMappingMultiKeyAssoc($dataAry,$MappingKey){
	
	$resultAssocAry = array();
	foreach($dataAry as $_data){
		$_newMappingKey = $_data[$MappingKey];
		
		$resultAssocAry[$_newMappingKey][] = $_data;
	}
	
	return $resultAssocAry;
}


class objPrinting{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	private $cfgValue;
	
	var $AcademicYear;
	//var $yearClassID;
	var $studentListAry;
	var $studentId;
	
	var $School_SubjectInfo;
	var $Student_info_Ary;
	var $Student_Assessment_Ary;
	var $Student_OLE_Ary;
	var $Student_SelfAccount_Ary;
	var	$Student_ClassHistory_Ary;
	var $Student_FormName_Ary;
	var $Student_ClassName;
	
	function __construct(){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		$this->cfgValue = $ipf_cfg;
	}
		
	function setAcademicYear($AcademicYear){
		$this->AcademicYear = $AcademicYear;
	}
	function getAcademicYear(){
		return $this->AcademicYear;
	}
	
	function setYearClassID($yearClassId){
		$this->yearClassID = $yearClassId;
	}
	function getYearClassID(){
		return $this->yearClassID;
	}
	
	function setStudentList($studentListAry){
		$this->studentListAry = $studentListAry;
	}
	function getStudentList(){
		return $this->studentListAry;
	}
	
	
	function setStudent($studentId){
		$this->studentId = $studentId;
	}
	function getStudent(){
		return $this->studentId;
	}
	
	public function contentReady(){
		$this->getTargetFormReady();
	//Please Set StudentList Before call this function		
		$this->School_SubjectInfo = $this->getSchoolSubjectInfo();
		$this->Student_info_Ary = $this->getStudentList_Info();
		$this->Student_Assessment_Ary =$this->getStudentList_AssessmentReport();
		$this->Student_OLE_Ary = $this->getStudentList_OLE();
		$this->Student_SelfAccount_Ary = $this->getStudentList_SelfAccount();
		$this->Student_ClassHistory_Ary = $this->getStudentList_ClassHistory();
	}
	
	//Get Subject Info
	private function getSchoolSubjectInfo(){
		$sql = "Select YearID from ".$this->intranet_db.".YEAR_CLASS where YearClassID = ".$this->yearClassID." ";
		$result = $this->objDB->returnVector($sql);
		
		$yearID = $result[0];
		
		$sql = "Select 
			ass.RecordID, 
			ass2.CODEID, 
			ass2.EN_SNAME as SubjectCode, 
			ass2.EN_DES as SubjectDisplayName,
			ass2.CH_DES as SubjectDisplayChineseName,  
			ass2.DisplayOrder,
			ass2.CMP_CODEID as isComponent,
			IF(asf.FullMarkGrade is null,asf.FullMarkInt,asf.FullMarkGrade) as Fullmark
		From 
			LEARNING_CATEGORY as lc 
			inner join ASSESSMENT_SUBJECT as ass on lc.LearningCategoryID = ass.LearningCategoryID and ass.RecordStatus =1
			left outer join ASSESSMENT_SUBJECT as ass2 on ass.CODEID = ass2.CODEID and ass2.RecordStatus =1
			LEFT outer JOIN ".$this->eclass_db.".ASSESSMENT_SUBJECT_FULLMARK as asf ON ass2.RecordID = asf.SubjectID and asf.YearID = '".$yearID."' and asf.AcademicYearID='".Get_Current_Academic_Year_ID()."'
		where 
			lc.RecordStatus = 1 order by lc.DisplayOrder, ass.DisplayOrder, ass2.CMP_CODEID , ass2.DisplayOrder";
		$result = $this->objDB->returnResultSet($sql);
		
		return $result;
	}
		
	//Get Student Info
	private function getStudentList_Info(){
		$sql = "select 
					iu.EnglishName,
					iu.ChineseName,
					DATE_FORMAT(iu.DateOfBirth, '%d/%m/%Y') AS DateOfBirth,
					iu.ClassName,
					iu.ClassNumber,
					iu.Gender,
					iu.BarCode,
					iu.STRN,
					iu.HKID,
					DATE_FORMAT(uextra.AdmissionDate, '%d/%m/%Y') AS AdmissionDate,
					substring(iu.WebSAMSRegNo from 2) as WebSAMSRegNo ,
					iu.userid
				from
					".$this->intranet_db.".INTRANET_USER AS iu LEFT JOIN ".$this->intranet_db.".INTRANET_USER_PERSONAL_SETTINGS AS uextra ON uextra.UserID=iu.UserID 
				where
					iu.userid IN ('".implode('\',\'',$this->getStudentList())."') AND (iu.RecordType = '2' OR iu.UserLogin like 'tmpUserLogin_%') ";

		$result = $this->objDB->returnResultSet($sql);
		
		return BuildMultiKeyAssoc($result,'userid');
	}
	public function getStudentInfo(){
		return $this->Student_info_Ary[$this->getStudent()];
	}
	
	private function getTargetFormReady(){
		$sql = 'select ClassTitleEN from YEAR_CLASS where yearclassid = '.$this->yearClassID.'';
		$targetclassnameAry = $this->objDB->returnVector($sql); 
		$this->Student_ClassName = $targetclassnameAry[0];
		preg_match_all("/\d+\B/", $targetclassnameAry[0], $tempform);
		
		$form = $tempform[0][0];
		$formAry[] = $form;
		if($form > 1){
			$formAry[] = --$form;
		}
		if($form > 1){
			$formAry[] = --$form;
		}
		sort($formAry);
		$this->Student_FormName_Ary = $formAry;
		
		if(count($formAry)>1){
			return true;
		}
	}
	public function getStudent_AcademicYear(){
		
		$StudentClassHistroyArray = $this->getStudentClassHistory();
		
		return Get_Array_By_Key($StudentClassHistroyArray,'AcademicYearID');
	}
	
	private function getStudentList_ClassHistory(){
		
		$formAry = $this->Student_FormName_Ary;		
		
		$sql = "Select UserID, ClassName, AcademicYear, AcademicYearID From PROFILE_CLASS_HISTORY as pch
				where 
					pch.userid IN  ('".implode('\',\'',$this->getStudentList())."') 
					AND pch.ClassName REGEXP '".implode('|',$formAry)."' 
					AND pch.AcademicYearID IS NOT NULL
				order by pch.AcademicYear desc";
		$result = $this->objDB->returnResultSet($sql);
		$academicYearIdAry = Get_Array_By_Key($result, 'AcademicYearID');
		
		
		$sql = "Select AcademicYearID, YearTermID From ACADEMIC_YEAR_TERM where AcademicYearID IN ('".implode("','", (array)$academicYearIdAry)."')";
//		$academicYearAry = $this->objDB->returnResultSet($sql);
		
//		$yearTermAssoAry = BuildMultiKeyAssoc($academicYearAry, 'AcademicYearID', $IncludedDBField=array('YearTermID'), $SingleValue=1, $BuildNumericArray=1);
				
		
//		foreach($result as $_userClassHistoryAry){
//			$_userID = $_userClassHistoryAry['UserID'];
//			$tempClassName = $_userClassHistoryAry['ClassName'];
//			preg_match_all("/\d+\B/", $tempClassName, $formNameAry);
//			$resultAssoAry[$_userID][$formNameAry[0][0]] = $_userClassHistoryAry;
//			
//			$_academicYearID = $_userClassHistoryAry['AcademicYearID'];
//			$resultAssoAry[$_userID][$formNameAry[0][0]]['YearTermIDAry'] = $yearTermAssoAry[$_academicYearID];
//		}
		
		return BuildMappingMultiKeyAssoc($result,'UserID');
	}
	public function getStudentClassHistory(){
		return $this->Student_ClassHistory_Ary[$this->getStudent()];
	}
	
	//Get Student Assessment Report
	private function getStudentList_AssessmentReport(){
		
		$formAry = $this->Student_FormName_Ary;
		
		$sql ="SELECT 
						a.RecordID,
						IFNULL(iu.WebSAMSRegNo, iau.WebSAMSRegNo) as WebSAMSRegNo,
						a.ClassName as ClassName,
						a.ClassNumber as ClassNumber,
						a.Year,
						a.Semester as Semester,
						a.AcademicYearID,
						a.YearTermID,
						a.SubjectCode,
						a.SubjectComponentCode,
						a.Score,
						a.Grade,
						a.OrderMeritForm,
						asub.EN_ABBR as Subject,
						a.SubjectID,
						a.userid
					FROM
						".$this->eclass_db.".ASSESSMENT_STUDENT_SUBJECT_RECORD as a
						LEFT JOIN ".$this->intranet_db.".INTRANET_USER AS iu ON a.UserID = iu.UserID
						LEFT JOIN ".$this->intranet_db.".INTRANET_ARCHIVE_USER AS iau ON a.UserID = iau.UserID
						LEFT JOIN ".$this->intranet_db.".ASSESSMENT_SUBJECT AS asub ON a.subjectID = asub.RecordID
					WHERE
						 (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
						AND a.ClassName REGEXP '".implode('|',$formAry)."'
						AND a.userid IN ('".implode('\',\'',$this->getStudentList())."')  AND a.IsAnnual = 1
					ORDER BY
						a.ClassName,
						ClassNumber,
						a.Year desc,
						a.Semester desc,
						asub.DisplayOrder
						/*a.SubjectCode,
						a.SubjectComponentCode*/
				";
		$result = $this->objDB->returnResultSet($sql);
		return BuildMappingMultiKeyAssoc($result,'userid');		
	}
	public function GetAssessmentReport(){
		return $this->Student_Assessment_Ary[$this->getStudent()];
	}
	
	//Get Student OLE
	private function getStudentList_OLE(){
		$condApprove = " and os.RecordStatus in( ".$this->cfgValue["OLE_STUDENT_RecordStatus"]["approved"]." ,".$this->cfgValue["OLE_STUDENT_RecordStatus"]["teacherSubmit"].")";
		
		$sql = "select 
					op.Title,
					op.StartDate,
					op.EndDate,
					os.Role,
					os.Hours,
					os.Achievement,
					op.ELE,
					op.Details,
					ay.YearNameEN as AcademicYear,
					oc.EngTitle as Category,
					os.userid,
					os.Organization as Organization,
					oc.RecordID as ProgramCategoryID,
					op.IntExt as IntExt
				from 
					".$this->eclass_db.".OLE_STUDENT as os 
					inner join ".$this->eclass_db.".OLE_PROGRAM as op on os.programid = op.programid 
					inner join ".$this->eclass_db.".OLE_CATEGORY as oc on op.Category = oc.RecordID 
					inner join ".$this->intranet_db.".ACADEMIC_YEAR as ay on op.AcademicYearID = ay.AcademicYearID
				where 
					os.userid IN ('".implode('\',\'',$this->getStudentList())."')  
					$condApprove
				order by 
					op.StartDate desc
				";
				//debug_pr($sql);

		$result = $this->objDB->returnResultSet($sql);
		
		foreach($result as $_OLEInfoArr){
			$_studentId = $_OLEInfoArr['userid'];
			$_programCategoryID = $_OLEInfoArr['ProgramCategoryID'];
			$_IntExt = $_OLEInfoArr['IntExt'];
			$_academicYear = $_OLEInfoArr['AcademicYear'];
			
			if($_programCategoryID == 5){
				//Awards
				$resultAssoArr[$_studentId][$_IntExt]['AWARDS'][$_academicYear][] = $_OLEInfoArr;
			}
			else if($_programCategoryID == 7 || $_programCategoryID == 8 ){
				//ECA & Responsible Post
				$resultAssoArr[$_studentId][$_IntExt]['ECA'][$_academicYear][] = $_OLEInfoArr;
			}
			else{
				//Other OLE
				$resultAssoArr[$_studentId][$_IntExt]['OLE'][$_academicYear][] = $_OLEInfoArr;
			}

		}
		
		return $resultAssoArr;
	}
	public function GetOLE($IntExt, $Category){
		$IntExt = strtoupper($IntExt);
		$Category= strtoupper($Category);
		
		return $this->Student_OLE_Ary[$this->getStudent()][$IntExt][$Category];
	}
	
	//Get Self Account
	private function getStudentList_SelfAccount(){
		$sql = "select 
					sas.Details as Details,
					userid
				from 
					".$this->eclass_db.".SELF_ACCOUNT_STUDENT as sas 
				where 
					Userid IN ('".implode('\',\'',$this->getStudentList())."')  
					and DefaultSA = 'SLP' 
			   ";

		$result = $this->objDB->returnResultSet($sql);
		return BuildMappingMultiKeyAssoc($result,'userid');
	}
	public function getSelfAccount(){
		return $this->Student_SelfAccount_Ary[$this->getStudent()];
	}
}

?>