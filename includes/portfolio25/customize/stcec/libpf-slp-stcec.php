<?php
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	private $All_AcademicYearID;
	
	private $FooterStr;
	private $TableWidthPercentage;
	
	private $YearClassID;
	
	private $AcademicYearStr;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		
		//2012-0220-1352-46132
		$academicYearIDArr = array_remove_empty($academicYearIDArr);
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;
		
		$this->cfgValue = $ipf_cfg;
		
		$this->FooterStr = '*Explanatory Notes are printed overleaf.';
		
		$this->TableWidthPercentage = '95%';
		
		$this->EmptySymbol = '--';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#D0D0D0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->SchoolTitleEn = 'SHUNG TAK CATHOLIC ENGLISH COLLEGE';
		$this->SchoolTitleCh = '天主教崇德英文書院';
	}
	
	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	public function setAll_AcademicYearID($All_AcademicYearID)
	{
		$this->All_AcademicYearID = $All_AcademicYearID;
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	
	private function getFormIdByClassName($className, $academicYearId) {
		$YEAR_CLASS = $this->Get_IntranetDB_Table_Name('YEAR_CLASS');
		
		$sql = "Select
						YearID
				From
						$YEAR_CLASS
				Where
						ClassTitleEN = '".$this->objDB->Get_Safe_Sql_Query($className)."'
						Or ClassTitleB5 = '".$this->objDB->Get_Safe_Sql_Query($className)."'
				";
		$result = $this->objDB->returnArray($sql);
		return $result[0]['YearID'];
	}
	
	private function getYearID($YearName='thisYear')
	{
		if($YearName=='thisYear')
		{
			$YearClassID = $this->YearClassID;
		
			$YEAR_CLASS = $this->Get_IntranetDB_Table_Name('YEAR_CLASS');
			$sql = "select 
							ClassTitleEN,
							YearID
				    from 
							$YEAR_CLASS 
					where 
							YearClassID =$YearClassID
					";
			$result = $this->objDB->returnArray($sql);
			$result = current($result);
			
			$yearID = $result['YearID'];
		}
		else
		{
			$YEAR = $this->Get_IntranetDB_Table_Name('YEAR');
			$sql = "select 
							YearID
				    from 
							$YEAR 
					where 
							YearName = '$YearName'
					";
			$result = $this->objDB->returnArray($sql);
			$result = current($result);
			
			$yearID = $result['YearID'];
		}
		
		return $yearID;
	}
	
	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '15';

		$_borderBottom =  'border-bottom: 1px solid black; ';
		
		$header_title1 = $this->SchoolTitleEn;
		$header_title2 = $this->SchoolTitleCh;
		$header_title3 = 'SENIOR SECONDARY EDUCATION';
		$header_title4 = "Student Learning Profile  ".'('.$this->YearRangeStr.')';
		
		$EmptySpace_html = '<tr><td>'.'&nbsp;'.'</td></tr>';
		
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:$this->TableWidthPercentage; text-align:center; border:0px;">
					<tr>
						<td rowspan="5" align="center" " style="width:25%;">&nbsp;</td>
						<td style="font-size:'.$header_font_size.'px;"><font face="Arial">'.$header_title1.'</font></td>
						<td rowspan="5" align="left" style="width:25%;">&nbsp;</td>
				   </tr>';
			
			$x .= '<tr><td style="font-size:'.$header_font_size.'px;"><font face="Arial">'.$header_title2.'</font></td></tr>';
			$x .= '<tr><td style="font-size:15px;">&nbsp;</td></tr>';
			$x .= '<tr><td style="font-size:15px;"><font face="Arial">'.$header_title3.'</font></td></tr>';
			$x .= '<tr><td style="font-size:15px;"><font face="Arial">'.$header_title4.'</font></td></tr>';
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						ChineseName,
						HKID,
						DateOfBirth,
						Gender,
						STRN,
						ClassNumber,
						ClassName
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";

		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result)
	{		
		$fontSize = 'font-size:10px';
		
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		
		$_borderBottom_dot =  'border-bottom: 1px dashed black; ';
		
		$IssueDate = $this->issuedate;
		
		if (date($IssueDate)==0) {
			$IssueDate = $this->EmptySymbol;
		} else {
			$IssueDate = date('Y-n-j',strtotime($IssueDate));
		}
		
		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $this->EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];		
		$Gender = ($Gender=='')? $this->EmptySymbol:$Gender;
		
		$RegistrationNo = $result[0]["STRN"];
		$RegistrationNo = ($RegistrationNo=='')? $this->EmptySymbol:$RegistrationNo;
		
		$ClassNumber = $result[0]["ClassNumber"];
		$ClassNumber = ($ClassNumber=='')? $this->EmptySymbol:$ClassNumber;
		
		$ClassName = $result[0]["ClassName"];
		$ClassName = ($ClassName=='')? $this->EmptySymbol:$ClassName;


		$html = '';
		$html .= '<table width="'.$this->TableWidthPercentage.'" cellpadding="4" border="0px" align="center" style="border-collapse: collapse;">
					<col width="20%" />
					<col width="5%" />
					<col width="38%" />
					<col width="20%" />
					<col width="5%" />
					<col width="22%" />

					<tr>
							<td ><font face="Arial" style="'.$fontSize.'">Name</font></td>
							<td ><font face="Arial" style="'.$fontSize.'">:</font></td>
							<td ><font face="Arial" style="'.$fontSize.'">'.$EngName." (".$ChiName.')</font></td>

							<td ><font face="Arial" style="'.$fontSize.'">Class</font></td>
							<td ><font face="Arial" style="'.$fontSize.'">:</font></td>
							<td ><font face="Arial" style="'.$fontSize.'">'.$ClassName.'</font></td>
							
					</tr>
					<tr>
							<td><font face="Arial" style="'.$fontSize.'">Registration No</font></td>
							<td><font face="Arial" style="'.$fontSize.'">:</font></td>
							<td><font face="Arial" style="'.$fontSize.'">'.$RegistrationNo.'</font></td>

							<td><font face="Arial" style="'.$fontSize.'">Class No.</font></td>
							<td><font face="Arial" style="'.$fontSize.'">:</font></td>
							<td><font face="Arial" style="'.$fontSize.'">'.$ClassNumber.'</font></td>
					</tr>
					<tr>
							<td><font face="Arial" style="'.$fontSize.'">Sex</font></td>
							<td><font face="Arial" style="'.$fontSize.'">:</font></td>
							<td><font face="Arial" style="'.$fontSize.'">'.$Gender.'</font></td>

							<td><font face="Arial" style="'.$fontSize.'">Date of Issue</font></td>
							<td><font face="Arial" style="'.$fontSize.'">:</font></td>
							<td><font face="Arial" style="'.$fontSize.'">'.$IssueDate.'</font></td>
					</tr>
					<tr>
							<td style="'.$_borderBottom_dot.';">&nbsp;</td>
							<td style="'.$_borderBottom_dot.';">&nbsp;</td>
							<td style="'.$_borderBottom_dot.';">&nbsp;</td>
						
							<td style="'.$_borderBottom_dot.';">&nbsp;</td>
							<td style="'.$_borderBottom_dot.';">&nbsp;</td>
							<td style="'.$_borderBottom_dot.';">&nbsp;</td>
					</tr>
					';

		$html .= '</table>';
	
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	
	/***************** Part 3 : Academic Performance******************/	
	public function getPart3_HTML(){
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$Part3_Title = 'Academic Performance in School at Senior Secondary Level';
		
		$Part3_remark_String = 'Our College offers 3 years of junior secondary education followed by 3 years of senior secondary education. ';
		$Part3_remark_String .= 'At the end of the sixth year, students sit for the Hong Kong Diploma of Secondary Education Examination, a public examination ';
		$Part3_remark_String .= 'conducted by the Hong Kong Examinations and Assessment Authority.<br/>';
		$Part3_remark_String .= 'The school year, excluding holidays, normally covers 190 school days, with 48 class-periods per 6-day cycle. The average length ';
		$Part3_remark_String .= 'of a period is 40 minutes. ';
 		$Part3_remark_String .= 'The pass mark is a half of the full mark of the subject.';
 		
 		$Part3_remark_Descriptor = 'Descriptors of grades used are as follows:';
 		$Part3_remark_Arr['A'][] ='A';
 		$Part3_remark_Arr['A'][] ='-';
 		$Part3_remark_Arr['A'][] ='Comprehensive knowledge and understanding and the ability to apply concepts and skills effectively.';
 		
 		$Part3_remark_Arr['B'][] ='B';
 		$Part3_remark_Arr['B'][] ='-';
 		$Part3_remark_Arr['B'][] ='Good knowledge and understanding and the ability to apply concepts and skills in unfamiliar situations with insight.';
 		
 		$Part3_remark_Arr['C'][] ='C';
 		$Part3_remark_Arr['C'][] ='-';
 		$Part3_remark_Arr['C'][] ='Adequate knowledge and understanding and the ability to apply concepts and skills in different familiar situations.';
 		
 		$Part3_remark_Arr['D'][] ='D';
 		$Part3_remark_Arr['D'][] ='-';
 		$Part3_remark_Arr['D'][] ='Basic knowledge and understanding and the ability to apply concepts and skills in familiar situations.';
 		
 		$Part3_remark_Arr['E'][] ='E';
 		$Part3_remark_Arr['E'][] ='-';
 		$Part3_remark_Arr['E'][] ='Elementary knowledge and understanding and the ability to apply concepts and skills in simple familiar situations with support.';
 		
 		$Part3_remark_Arr['U'][] ='U';
 		$Part3_remark_Arr['U'][] ='-';
 		$Part3_remark_Arr['U'][] ='Unclassified';
 		
 		$Part3_remark_Arr_html = '';
 		
 		$Part3_remark_Arr_html .= '<table style="font-size:10px;">';
 		foreach((array)$Part3_remark_Arr as $key => $_contentArr)
 		{
 			$Part3_remark_Arr_html .='<tr>';
 			
 			for($i=0,$i_MAX=count($_contentArr);$i<$i_MAX;$i++) 
 			{
 				$Part3_remark_Arr_html .='<td  style="vertical-align:top;">'.$_contentArr[$i].'</td>';
 			}
 			
 			$Part3_remark_Arr_html .='</tr>';
 		}
 		$Part3_remark_Arr_html .= '</table>';

		$Part3_remark_html ='<table width="100%" style="font-size:11px;">';
		$Part3_remark_html .='<tr><td style="vertical-align:top;"><div id="part3_remark" align="justify">'.$Part3_remark_String.'</div></td></tr>';
		$Part3_remark_html .='<tr><td style="vertical-align:top;">'.$Part3_remark_Descriptor.'</td></tr>';
		$Part3_remark_html .='<tr><td style="vertical-align:top;">'.$Part3_remark_Arr_html.'</td></tr>';
		$Part3_remark_html .='</table>';

		$Page_break='page-break-after:always;';
		$table_style = 'border-collapse: collapse; '; 
		
		$html = '';
		
		$YearID = $this->getYearID('thisYear');
		
		if($YearID=='6')
		{
			$html =$this->getEmptyTable('2%');
			$html .='<table width="'.$this->TableWidthPercentage.'" height="" cellpadding="0" border="0px" align="center" style="border:1px solid #000000;border-collapse: collapse;">';
			$html .='<col width="60%"/>';
			$html .='<col width="1%"/>';
			$html .='<col width="39%"/>';
			
			$html .='<tr>';
			$html .='<td style="vertical-align:top;">'.$this->getAcademicResultContentDisplay($Part3_Title).'</td>';
			$html .='<td style="vertical-align:top;'.$_borderLeft.$_borderRight.'">&nbsp;</td>';
			$html .='<td style="vertical-align:top;">'.$Part3_remark_html.'</td>';
			$html .='</tr>';
			$html .='</table>';
		}
		return $html;	
	}		
	
	private function getAcademicResultContentDisplay($MainTitle='')
	{
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$remark_html ='';
		$remark_html = '<table width="100%"><tr>';
		
		$ContentTable = $this->getAcademicResult_ContentTable($MainTitle);	
		
		$html='';
		$html='<table width="100%" height="" cellpadding="0" border="0px" align="center" style="border-collapse: collapse;border:1px solid #000000;">
					<tr>				 							
						<td style="vertical-align: top;">'.$ContentTable.'</td>						 														
					</tr>
			   </table>';
	
		return $html;
	}
	
	private function getAcademicResult_ContentTable($MainTitle='')
	{	
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$YearClassArray = $this->getStudentAcademic_YearClassNameArr(true);
	
		$Subject_string = 'Subjects';
		$FullMark_string = 'Full Mark';
		$Perform_in_School_string = 'Mark Attained';
		
		$StudentID = $this->uid;
		
		$lpf_report = new libpf_report();
		list($SubjectIDNameAssoArr, $StudentFormInfoArr, $StudentAcademicResultInfoArr, $SubjectInfoOrderedArr) = $lpf_report->GET_ACADEMIC_PERFORMANCE_SLP_IN_ID($StudentID);
	
		$lpf_academic = new libpf_academic();
		$FullMarkArr = $lpf_academic ->Get_Subject_Full_Mark($this->All_AcademicYearID);
		
		$countColumn=0;

		$MarkArr = array();	
	
		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{		
			$countColumn++;
		}	
		$AcademicYearArray = $this->getAcademicYearArr($this->All_AcademicYearID);
		$YearClassArray = $this->getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,'desc');
		
		for($a=0,$a_MAX=count($SubjectInfoOrderedArr);$a<$a_MAX;$a++)
		{
			$thisSubjectID = $SubjectInfoOrderedArr[$a]['SubjectID'];
			$thisSubjectName = $SubjectInfoOrderedArr[$a]['SubjectDescEN'];
		
			$IsStudy = false;
			$fullMarkClassName = '';
			$fullMarkAcademicYearId = '';
			for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
			{			
				
				$thisYear = $YearClassArray[$i]['AcademicYear'];
				$thisClassName = $YearClassArray[$i]['ClassName'];
				
				$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
				
				$thisFormNo = (int)$thisClassName;
				
				$thisMark = $StudentAcademicResultInfoArr[$StudentID][$thisSubjectID][$thisAcademicYearID]['Score'];
				$thisGrade = $StudentAcademicResultInfoArr[$StudentID][$thisSubjectID][$thisAcademicYearID]['Grade'];
				
				if ($thisMark != 'N.A.' && $thisMark != '') {
					$IsStudy = true;					
				}
				
				$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Score'] = $thisMark;
				$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Grade'] = $thisGrade;
				
				if($i==0)
				{
					//$YearName = 'S'.$thisFormNo;
					
					$fullMarkClassName = $thisClassName;
					$fullMarkAcademicYearId = $thisAcademicYearID;
				}				
			}
			//$thisYearID = $this->getYearID($YearName);
			$fullMarkYearID = $this->getFormIdByClassName($fullMarkClassName, $fullMarkAcademicYearId);
			
			
			//$NewSubjectSequenceArr[$thisSubjectID]['FullMark'] =$FullMarkArr[$thisAcademicYearID][$thisYearID][$thisSubjectID];
			$_fullMarkInfoArr = $FullMarkArr[$fullMarkAcademicYearId][$fullMarkYearID][$thisSubjectID];
			$_fullMarkInt = $_fullMarkInfoArr['FullMarkInt'];
			$_fullMarkGrade = $_fullMarkInfoArr['FullMarkGrade'];
			$_fullMark = ($_fullMarkInt=='')? $_fullMarkGrade : $_fullMarkInt;
			$NewSubjectSequenceArr[$thisSubjectID]['FullMark'] = $_fullMark;
			
			$NewSubjectSequenceArr[$thisSubjectID]['IsStudy']=$IsStudy;
						
			$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = $thisSubjectName;
			
		}
		

		$html='';
		
		$html .='<table width="100%"  cellpadding="4" style="border-collapse: collapse;">';
		
			$html .='<col width="40%"/>';
			$html .='<col width="8%"/>';
			
			if($countColumn>0)
			{
				$thisWidth = 52/$countColumn;
			}
			
			for($i=0,$i_MAX=$countColumn;$i<$i_MAX;$i++)
			{			
				$html .='<col width="'.$thisWidth.'%"/>';
			}
			$html .='<tr>';
				$html .='<td bgcolor="'.$this->TableTitleBgColor.'" colspan="'.($countColumn+2).'" style="font-size:15px;text-align:left;padding-left:8px;'.$_borderBottom.'"><font face="Arial">'.$MainTitle.'</font></td>';
			$html .='</tr>';
					
			$html .='<tr>'; 
				$html .='<td style="font-size:12px;text-align:left;padding-left:8px;'.$_borderBottom.$_borderRight.$_borderTop.'"><font face="Arial">'.$Subject_string.'</font></td>';
				$html .='<td style="font-size:12px;text-align:left;padding-left:8px;'.$_borderBottom.$_borderRight.$_borderTop.'"><font face="Arial">&nbsp;</font></td>';
				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{
					$thisYear = $YearClassArray[$i]['AcademicYear'];
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNo = (int)$thisClassName;

					$colspan ='';

					$html .='<td '.$colspan.' style="font-size:8px;text-align:center;'.$_borderLeft.$_borderBottom.$_borderTop.$_borderLeft.'"><font face="Arial">'.$thisYear.'<br/>S'.$thisFormNo.'</font></td>';
				}			
			$html .='</tr>';
			$html .='<tr>';
				$html .='<td style="font-size:12px;text-align:left;padding-left:8px;'.$_borderBottom.$_borderRight.$_borderTop.'"><font face="Arial">&nbsp;</font></td>';
				$html .='<td style="font-size:9px;text-align:center;padding-left:8px;'.$_borderBottom.$_borderRight.$_borderTop.'"><font face="Arial">'.$FullMark_string.'</font></td>';
				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNo = (int)$thisClassName;
					
					$html .='<td style="font-size:9px;text-align:center;'.$_borderTop.$_borderLeft.$_borderBottom.'"><font face="Arial">'.$Perform_in_School_string.'</font></td>';											
				}								
			$html .='</tr>';
					
								
			foreach((array)$NewSubjectSequenceArr as $thisSubjectID => $thisSubjectInfoArr)
			{		
				$_IsStudy = $NewSubjectSequenceArr[$thisSubjectID]['IsStudy'];
				$_fullMark = $NewSubjectSequenceArr[$thisSubjectID]['FullMark'];
				$_fullMark = ($_fullMark=='')? $this->EmptySymbol:$_fullMark;
				
				if($_IsStudy==false)
				{
					continue;
				}		
				$thisSubjectName = $thisSubjectInfoArr['SubjectNameEn'];
				$thisIsComponentSubject = $thisSubjectInfoArr['thisIsComponentSubject'];
				
				if($thisIsComponentSubject)
				{
					$fontSize='8px';
				}
				else
				{
					$fontSize='10px';
				}
				
				$html .='<tr>';
			
				$html .='<td style="padding-left:9px;font-size:'.$fontSize.';text-align:left;'.$_borderRight.$_borderTop.' nowrap"><font face="Arial">'.$thisSubjectName.'</font></td>';
				$html .='<td style="padding-left:9px;font-size:'.$fontSize.';text-align:center;'.$_borderRight.$_borderTop.'"><font face="Arial">'.$_fullMark.'</font></td>';
				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{
					$thisYear = $YearClassArray[$i]['AcademicYear'];
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNo = (int)$thisClassName;				

					$thisMark = $thisSubjectInfoArr[$thisYear]['Score'] ;
					
//					if($thisSubjectID==20 || $thisSubjectID==21)
//					{
//						$thisMark = $thisSubjectInfoArr[$thisYear]['Grade'] ;
//					}								
				
					if (preg_match('/^[0-9]/', $_fullMark))
					{
					    $thisMark = $thisSubjectInfoArr[$thisYear]['Score'] ;
				    } 
				    else 
				    {
					     $thisMark = $thisSubjectInfoArr[$thisYear]['Grade'] ;
					}
					
					if ($thisMark == 'N.A.' || $thisMark == '' || $thisMark<0) {
						$thisMark = $this->EmptySymbol;
					}
					
					$html .='<td style="padding-left:9px;font-size:'.$fontSize.';text-align:center;'.$_borderLeft.$_borderTop.$_borderLeft.'"><font face="Arial">'.$thisMark.'</font></td>';
		
				}			
				
				$html .='</tr>';								
			}		
			
			$html .='<tr>';
	
			
			if($countColumn==0)
			{
				$noRecord = 'No Record Found.';
				$html .='<td style="padding-left:9px;font-size:12px;text-align:center;'.$_borderLeft.$_borderRight.'"><font face="Arial">'.$noRecord.'</font></td>';
			}
			else
			{
				for($i=0,$i_MAX=(3+2);$i<$i_MAX;$i++)
				{
					$thisBorder_right=$_borderRight;
					
					if($i==($i_MAX-1))
					{
						$thisBorder_right='';
					}
					$html .='<td style="padding-left:9px;font-size:12px;text-align:center;'.$_borderTop.$thisBorder_right.'">***</td>';
				}
			}
			
			$html .='</tr>';
			
		$html .='</table>';
		
		return $html;
		
	}


	/***************** End Part 3 ******************/
	
	
	
	
	
	/***************** Part 4 : Other Learning Experience******************/	
	public function getPart4_HTML(){
		
		$Part4_Title = 'List of Awards and Major Achievements Issued by the School';
		
		
		$titleArray = array();	
		$titleArray[] = 'Name of Award';
		$titleArray[] = 'Details';
		$titleArray[] = 'School Year';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="25%" />';
		$colWidthArr[] = '<col width="55%" />';
		$colWidthArr[] = '<col width="20%" />';
		
		$DataArray = array();
		$DataArray = $this->getAward_Info();

		$html = '';
	
		$html .= $this->getPartContentDisplay_NoRowLimit('4',$Part4_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false);
		$html .=$this->getEmptyTable('2%',$this->FooterStr,'left','font-size:13px;');		
		
		return $html;
		
	}	
	
	private function getAward_Info(){
		
		global $ipf_cfg;
		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		
		$UserID = $this->uid;
		
		if($UserID!='')
		{
			$cond_studentID = "And UserID ='$UserID'";
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And s.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$sql = "Select
						AwardName,
						Remark,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' 
				From
						$Award_student as s
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = s.AcademicYearID
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence desc, 
						Details asc		

				";
		$roleData = $this->objDB->returnArray($sql,2);

		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['AwardName'] =$roleData[$i]['AwardName'];
			$thisDataArr['Details'] =$roleData[$i]['Remark'];
			$thisDataArr['YEAR_NAME'] = $roleData[$i]['YEAR_NAME'];
			
			$returnDate[] = $thisDataArr;	
		}
//hdebug_r($sql);
		return $returnDate;
	}
	
	/***************** End Part 4 ******************/
	
	
	
	/***************** Part 5 ******************/	
	public function getPart5_HTML( $OLEInfoArr_INT){
		
		$Part5_Title = 'Other Learning Experiences';
		$Part5_Remark = '&nbsp;';
		
		$titleArray = array();
		$titleArray[] = 'Programmes';
		$titleArray[] = 'School Year';
		$titleArray[] = 'Role';
		$titleArray[] = 'OLE Components **';
		$titleArray[] = 'Achievement *';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="40%" />';
		$colWidthArr[] = '<col width="13%" />';
		$colWidthArr[] = '<col width="13%" />';
		$colWidthArr[] = '<col width="17%" />';
		$colWidthArr[] = '<col width="17%" />';
		
		$DataArr = array();
		$DataArray = $OLEInfoArr_INT[$this->uid]['INT'];
		

		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('5',$Part5_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false);
		
		return $html;
		
	}

	/***************** End Part 5 ******************/
	
	
	/***************** Part 6 ******************/
	public function getPart6_HTML($OLEInfoArr_EXT){
		
		$Part6_Title = 'Performance / Awards and Key Participation Outside School';
		
		$titleArray = array();
		$titleArray[] = 'Programmes';
		$titleArray[] = 'School Year';
		$titleArray[] = 'Role';
		$titleArray[] = 'Organisation';
		$titleArray[] = 'Achievement *';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="38%" />';
		$colWidthArr[] = '<col width="13%" />';
		$colWidthArr[] = '<col width="12%" />';
		$colWidthArr[] = '<col width="20%" />';
		$colWidthArr[] = '<col width="17%" />';
		
		
		$DataArray = array();
		$DataArray = $OLEInfoArr_EXT[$this->uid]['EXT'];
		
		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('6',$Part6_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false);
		$html .=$this->getEmptyTable('2%',$this->FooterStr,'left','font-size:13px;');	
		return $html;
		
	}
	

	
	/***************** End Part 6 ******************/
	
	
	
	/***************** Part 7 ******************/
	
	public function getPart7_HTML(){
		
		$Part7_Title = "Student's Self-Account";
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$Part7_Title) ;
		$html .=$this->getEmptyTable('2%',$this->FooterStr,'left','font-size:13px;');	
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$UserID = $this->uid;
		
		if($UserID!='')
		{
			$cond_studentID = "And UserID ='$UserID'";
		}	
		$cond_DefaultSA = "And DefaultSA ='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnArray($sql,2);

		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$MainTitle='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';				
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:10px;" '; 

		$_StudentID = $this->uid;

		$html = '';
				
		$StdSkipArr = array();
		$StdSkipArr[] = '270';
		$StdSkipArr[] = '315';
					
		$rowMax_count = count($DataArr);

		if($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];
			
			
			if(in_array($_StudentID,$StdSkipArr))
			{			
			}
			else
			{
				$Detail = strip_tags($Detail,'<p><br>');
			}
		}
		else{
			$Detail = 'No Record Found.';
		}	
		
		$html .= '<table width="'.$this->TableWidthPercentage.'" height="860px"  cellpadding="7" border="0px" align="center" '.$table_style.'>
					<tr>				 							
						<td bgcolor="'.$this->TableTitleBgColor.'" style="font-size:15px;height:30px;vertical-align: top;'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'"><font face="Arial">'.$MainTitle.'</font></td>						 														
					</tr>
					<tr>				 							
						<td style="vertical-align: top;height:1px;'.$_borderTop.$_borderLeft.$_borderRight.'"><font face="Arial">'.$Detail.'</font></td>						 														
					</tr>';
		$html .= '	<tr>				 							
						<td style="vertical-align: top;text-align:center;'.$_borderLeft.$_borderBottom.$_borderRight.'"><font face="Arial">END OF SELF - ACCOUNT</font></td>						 														
					</tr>';
		$html .= ' </table>';	
		
			
		return $html;
	}
	
	
	/***************** End Part 7 ******************/
	
	
	
	/***************** Part 8 : Footer A ******************/
	public function getPart8_HTML($PageBreak='')
	{
		$SchoolChopStr = "School Chop";
		$PrincipalStr = "Principal";
		
		$_borderTop =  'border-top: 1px solid black; ';
		
		$html = '';
		
		$html .= '<table width="'.$this->TableWidthPercentage.'" cellpadding="0" border="0px" align="center" style="font-size:14px;'.$PageBreak.'">		
					<col width="18%" />
					<col width="64%" />
					<col width="18%" />

				 	<tr>					
					    <td align="center" style="'.$_borderTop.'"><font face="Arial">'.$PrincipalStr.'</font></td>
						<td>&nbsp;</td>
						<td align="center" style="'.$_borderTop.'"><font face="Arial">'.$SchoolChopStr.'</font></td>
					</tr>

				  </table>';
		return $html;
		
	}
	/***************** End Part 8A ******************/

	
	
	
	private function getStringDisplay($String='',$font_size='10',$Page_break='') {
		
		$html = '';
		$html .= '<table width="'.$this->TableWidthPercentage.'" cellpadding="7" border="0px" align="center" style="font-size:'.$font_size.'px;" '.$Page_break.'>					
		   			 <tr>
					    <td><font face="Arial">'.$String.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}

	
	private function getPartContentDisplay_NoRowLimit($PartNum, $mainTitle,$subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false) 
	{
				
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		$BgColor = $this->TableTitleBgColor;
		
		$Page_break='page-break-after:always;';
		
		if($hasPageBreak==false)
		{
			$Page_break='';
		}
		
		$table_style = 'style="font-size:10px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 

		
		$subtitle_html = '';
		
		if($PartNum=='4')
		{
			$subtitle_html = '';
			
			$i=0;
			foreach((array)$subTitleArr as $key=>$_title)
			{
				$text_alignStr='center';
				if($i==0 || $i==1)
				{
					$text_alignStr='left';
				}
				
				$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'"><b><font face="Arial">';
				$subtitle_html .=$_title;
				$subtitle_html .='</font></b></td>';
				
				$i++;
			}	
						
			$total_Record=10;
			$rowHight = 30;
			
			$thisYearId = $this->getYearID();

			if($thisYearId!='6')
			{
				$rowHight = 40;
			}
			
			
		}
		else if ($PartNum=='5')
		{
			$subtitle_html = '';
			
			foreach((array)$subTitleArr as $key=>$_title)
			{
				$text_alignStr='center';
				
				$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'"><b><font face="Arial">';
				$subtitle_html .=$_title;
				$subtitle_html .='</font></b></td>';
			}	
						
			$total_Record=20;
			$rowHight = 25;
		  
		}
		else if ($PartNum=='6')
		{
			$subtitle_html = '';
			$i=0;
			foreach((array)$subTitleArr as $key=>$_title)
			{
				$text_alignStr='center';
				if($i==0)
				{
					$text_alignStr='left';
				}			
				$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'"><b><font face="Arial">';
				$subtitle_html .=$_title;
				$subtitle_html .='</font></b></td>';
				$i++;
			}	
						
			$total_Record=5;
			$rowHight = 30;
		}
		
		$text_alignStr='center';
		$content_html='';	
		$rowMax_count = 0;
		
		if(count($DataArr)>0)
		{
			$rowMax_count = count($DataArr);
		}	
		
		$count_col =count($colWidthArr);

		for ($i=0; $i<$rowMax_count; $i++)
		{		
			$content_html .= '<tr>';	
				
			$j=0;		 
			foreach((array)$DataArr[$i] as $_title=>$_titleVal)
			{
				if($_titleVal=='')
				{
					$_titleVal =  $this->EmptySymbol;
				}
				
				if($PartNum=='4')
				{
					if($j==0 || $j==1)
					{
						$text_alignStr = 'left';
					}
					else
					{
						$text_alignStr = 'center';
					}
				}
				else if($PartNum=='5' || $PartNum=='6')
				{
					if($j==0)
					{
						$text_alignStr = 'left';
					}
					else
					{
						$text_alignStr = 'center';
					}
				}
				$content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderTop.$_borderRight.$_borderBottom.'"><font face="Arial">'.$_titleVal.'</font></td>';								
				$j++;
			}				
			$content_html .= '</tr>';		
		}
		
		$remainRow = $total_Record-$rowMax_count;
		for($i=0,$i_MAX=$remainRow;$i<$i_MAX;$i++)
		{

			$thisContent = '&nbsp;';
			
			if($i==0)
			{
				$thisContent = '***';
			}
			
			$text_alignStr='center';
			$content_html .= '<tr>';					
			for($j=0;$j<$count_col;$j++)
			{
				if($PartNum=='5' || $PartNum=='6')
				{
					if($j==0)
					{
						$text_alignStr = 'left';
					}
					else
					{
						$text_alignStr = 'center';
					}
				}					
				$content_html .= ' <td style="height:'.$rowHight.'px;vertical-align: top;text-align:'.$text_alignStr.';'.$_borderTop.$_borderRight.$_borderBottom.'"><font face="Arial">'.$thisContent.'</font></td>';
			}				
			$content_html .= '</tr>';
		}			
		
		if($PartNum=='5')
		{
			$content_html .= '<tr><td colspan="'.$count_col.'" bgcolor="'.$this->TableTitleBgColor.'">';	
			$content_html .= $this->getOLE_RemarkTable();
			$content_html .= '</td></tr>';	
		}
		else if($PartNum=='6')
		{
//			$content_html .= '<tr>';	
//			$content_html .= $this->getOLE_RemarkStr($count_col);
//			$content_html .= '</tr>';	
		}
		
		$colNumber = count($colWidthArr);
			
		/****************** Main Table ******************************/
		
		$html = '';

		$html = '<table width="'.$this->TableWidthPercentage.'" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$html .=$_colWidthInfo;
					}
					
		$html .= '<tr> 
					<td colspan="'.$colNumber.'" bgcolor="'.$BgColor.'" style=" font-size:15px;text-align:left;vertical-align:text-top;'.$_borderRight.$_borderBottom.'"><font face="Arial">
					'.$mainTitle.'
					</font></td>
				  </tr>';
		
		$html .= '<tr>'.$subtitle_html.'</tr>';
		
		$html .= $content_html;
		
		$html .='</table>';

			
		return $html;
	}
	
	
	private function getOLE_RemarkTable()
	{	
		$font_size = 11;
		$table_style = 'style="border-collapse: collapse;"'; 
		
		$html='';
		$html ='<table width="100%" bgcolor="'.$this->TableTitleBgColor.'" '.$table_style.' >
					<tr>
						<td style="font-size:'.$font_size.'px"><font face="Arial">OLE Components :</font></td>
						<td style="font-size:'.$font_size.'px"><font face="Arial">AD - Aesthetic Development</font></td>
						<td style="font-size:'.$font_size.'px"><font face="Arial">CRE - Career Related Experience</font></td>
						<td style="font-size:'.$font_size.'px"><font face="Arial">CS - Community Service</font></td>
					</tr>
					<tr>
						<td style="font-size:'.$font_size.'px"><font face="Arial">&nbsp;</font></td>
						<td style="font-size:'.$font_size.'px"><font face="Arial">MCE - Moral and Civic Education</font></td>
						<td style="font-size:'.$font_size.'px"><font face="Arial">PD - Physical Development</font></td>
						<td style="font-size:'.$font_size.'px"><font face="Arial">&nbsp;</font></td>
					</tr>';
//		   $html .='<tr>
//						'.$this->getOLE_RemarkStr(4).'
//					</tr>';
		$html .='</table>';
		
		return $html;
	}	
	private function getOLE_RemarkStr($colspan='')
	{
		$font_size = 11;
		
		$html='';
		$html ='<td bgcolor="'.$this->TableTitleBgColor.'" colspan="'.$colspan.'" style="font-size:'.$font_size.'px"><font face="Arial">*Evidence of awards/ certifications/ achievements listed is available for submission when required</font></td>';
		
		return $html;
	}
	
	public function getStudentAcademic_YearClassNameArr($AllYear=false)
	{
		$StudentID = $this->uid;
		
		$academicYearIDArr = $this->academicYearIDArr;
		
		if($AllYear)
		{
			$academicYearIDArr = $this->All_AcademicYearID;
		}
		
		$SelectStr= 'h.AcademicYear as AcademicYear,
					 h.ClassName as ClassName';
		
		if($academicYearIDArr!='')
		{		
			$YearArr = array();
			for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
			{
				if($academicYearIDArr[$i]=='')
				{
					continue;
				}
				$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
				$YearArr[] = $thisAcademicYear;
			}
			
			$cond_academicYear = "And h.AcademicYear In ('".implode("','",(array)$YearArr)."')";		
		}
		

		$Profile_his = $this->Get_IntranetDB_Table_Name('PROFILE_CLASS_HISTORY');
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$sql = 'select 
						'.$SelectStr.' 
				from 
						'.$Profile_his.' as h 
				where 
						h.UserID = "'.$StudentID.'"
						'.$cond_academicYear.'
				Order by
						h.AcademicYear desc';
						
		$roleData = $this->objDB->returnArray($sql,2);
		
	
		return $roleData;
	}
	

	
	public function getOLE_Info($UserID='',$academicYearIDArr='',$IntExt='',$Selected=false)
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		
		if($UserID!='')
		{
			$cond_studentID = "And UserID ='$UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = "And SLPOrder is not null"; 
		}
	
		if($IntExt!='')
		{
			$cond_IntExt = "And ole_prog.IntExt='".$IntExt."'";
		}
		
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = "And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
		
		$sql = "Select
						ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_prog.ELE as OleComponent
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
				order by 
						ole_prog.IntExt,
						y.Sequence desc,	
						ole_std.SLPOrder asc,
						ole_prog.Title asc		

				";

		$roleData = $this->objDB->returnArray($sql,2);

		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$_StudentID = $roleData[$i]['StudentID'];
			$_IntExt = $roleData[$i]['IntExt'];
			$thisAcademicYear = $roleData[$i]['YEAR_NAME'];
			
			$thisOleComponentArr = explode(',',$roleData[$i]['OleComponent']);
		
	
			for($a=0,$a_MAX=count($thisOleComponentArr);$a<$a_MAX;$a++)
			{
				$_str = $thisOleComponentArr[$a];
				$_str = substr($_str, 0, strlen($_str)-1); 
				$_str = substr($_str, 1);
				
				$thisOleComponentArr[$a] = $_str;
			}
			
			$thisOLEstr = implode(",",(array)$thisOleComponentArr);

			$thisDataArr = array();
			
			if($IntExt=='INT')
			{
				$thisDataArr['Program'] =$roleData[$i]['Title'];	
				$thisDataArr['SchoolYear'] =$thisAcademicYear;			
				$thisDataArr['Role'] =$roleData[$i]['Role'];
				$thisDataArr['OLEComponent'] =$thisOLEstr;
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
			}
			else if($IntExt=='EXT')
			{
				$thisDataArr['Program'] =$roleData[$i]['Title'];
				$thisDataArr['SchoolYear'] =$thisAcademicYear;						
				$thisDataArr['Role'] =$roleData[$i]['Role'];
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
			}
		
			
			$returnDate[$_StudentID][$_IntExt][] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		
		if(count($returnArray)==1)
		{
			return $returnArray[0];
		}
		else
		{
			$lastNo = count($returnArray)-1;
			
			$firstYear = substr($returnArray[0], 0, 4);
			$lastYear = substr($returnArray[$lastNo],-4);
			
			$returnValue = $firstYear."-".$lastYear;
			
			return $returnValue;	
		}
		
		
	}
	
		/********** Display Form 4, 5 and 6  ***********/
	private function getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,$order='asc')
	{
		
		$YearDisplayArr = array();
	
		$Form4_no = 0;
		$Form5_no = 0;
		$Form6_no = 0;
		

		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{						
			$thisYear = $YearClassArray[$i]['AcademicYear'];
			$thisClassName = $YearClassArray[$i]['ClassName'];
			
			$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
			
			$thisFormNo = (int)$thisClassName;
			
			preg_match('/\d+/', $thisClassName, $number);
			
			if(count($number)>0 && is_array($number))
			{
				$thisFormNo = $number[0];
				
				if($order=='asc')
				{
					if($thisFormNo==4 || $thisFormNo==5 || $thisFormNo==6)
					{				
						$YearDisplayArr[$thisFormNo] = $YearClassArray[$i];
					}
				}
				else if($order=='desc')
				{
					$CondArr = array();
					$CondArr[] = ($thisFormNo==4 && $Form4_no==0);
					$CondArr[] = ($thisFormNo==5 && $Form5_no==0);
					$CondArr[] = ($thisFormNo==6 && $Form6_no==0);
					
					if(in_array(true,$CondArr))
					{
						$YearDisplayArr[$thisFormNo] = $YearClassArray[$i];
					}
				}			
				
				if($thisFormNo==4)
				{
					$Form4_no++;
				}
				else if($thisFormNo==5)
				{
					$Form5_no++;
				}
				else if($thisFormNo==6)
				{
					$Form6_no++;
				}
				
				
			}
		}	
		
		$ThisClassArr = array();
		$count_YearDisplayArr = 0;
		foreach((array)$YearDisplayArr as $_formNo=>$_ClassArr)
		{
			$ThisClassArr[$count_YearDisplayArr] = $_ClassArr;
			$count_YearDisplayArr++;
		}
		
		return $ThisClassArr;
	}
	
	
	private function getAcademicYearArr($academicYearIDArr)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	
	public function getEmptyTable($Height='',$Content='&nbsp;',$textAlign='center',$fontSize='') {
		
		$html = '';
		$html .= '<table width="'.$this->TableWidthPercentage.'" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td style="text-align:'.$textAlign.';'.$fontSize.'"><font face="Arial">'.$Content.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="'.$this->TableWidthPercentage.'" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>