<?php
## Modifying By: 
include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/libportfolio.php");
include_once($intranet_root."/includes/libportfolio2007a.php");
include_once($intranet_root."/includes/libpf-slp.php");
include_once($intranet_root."/includes/portfolio25/iPortfolioConfig.inc.php");
include_once($intranet_root."/includes/libclass.php");
include_once($intranet_root."/includes/libinterface.php");
include_once($intranet_root."/includes/libuser.php");
include("cnecc/lang/lang.$intranet_session_language.php");

class libpf_slp_cnecc
{
  private $db;
  private $displayType;
  private $studentId;
  private $academicYearId;
  private $userInfo;
  private $roles;
  const ENABLED = 1;
  const DISABLED = 0;
  const DISPLAY_TYPE_HTML = "HTML";
  const DISPLAY_TYPE_PRINT = "PRINT";
  


  function __construct($ParStudentId=-1, $ParAcademicYearId=-1) {
  	global $Lang;
    $this->db = new libdb();
    $this->displayType = self::DISPLAY_TYPE_HTML;
    $this->studentId = $ParStudentId;
    if ($ParStudentId>0) {
    	$this->userInfo = new libuser($this->studentId);
    } else {
    	// do nothing
    }
    if ($ParAcademicYearId>0) {
    	$this->academicYearId = $ParAcademicYearId;
    } else {
    	$this->academicYearId = Get_Current_Academic_Year_ID();
    }
    $this->roles = array($Lang["Cust_Cnecc"]["Participant"],$Lang["Cust_Cnecc"]["Organizer"],$Lang["Cust_Cnecc"]["Helper"]);
  }

	function getEventInfo() {
		global $eclass_db;
		$returnArray = array();
		$sql = "SELECT
					OP.TITLE,
					OP.ELE,
					OP.CATEGORY AS CATEGORYID,
					CCSE.EVENTID,
					CCSE.PROGRAMID,
					CCSE.ATTAINMENT,
					CCSE.ROLE,
					CCSE.POINT,
					CCSE.STATUS
				FROM
					$eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
					INNER JOIN $eclass_db.OLE_PROGRAM OP
					ON CCSE.PROGRAMID = OP.PROGRAMID
				WHERE
					OP.INTEXT = 'INT'
					AND
					CCSE.STATUS = " . self::ENABLED . " 
					AND
					OP.ACADEMICYEARID = $this->academicYearId
					ORDER BY OP.TITLE, CCSE.POINT, CCSE.ATTAINMENT, CCSE.ROLE";
//debug_r($sql);
		$returnArray = $this->db->returnArray($sql);		
		return $returnArray;
	}
	
	function getStudentsSuccessInfo() {
		global $eclass_db;
		$returnArray = array();
		$sql = "SELECT
					CCSS.RECORDID,
					CCSS.EVENTID,
					CCSS.ENDDATE,
					CCSS.STUDENTID
				FROM
					$eclass_db.CUSTOM_CNECC_SS_STUDENT CCSS
					INNER JOIN $eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
						ON CCSS.EVENTID = CCSE.EVENTID
					INNER JOIN $eclass_db.OLE_STUDENT OS
						ON CCSS.OS_RECORDID = OS.RECORDID
					INNER JOIN $eclass_db.OLE_PROGRAM OP
						ON OS.PROGRAMID = OP.PROGRAMID
				WHERE
					CCSS.STUDENTID = $this->studentId
					AND CCSE.STATUS = ".self::ENABLED."
					AND OS.RECORDSTATUS = 2
					AND OP.ACADEMICYEARID = $this->academicYearId";
//		debug_r($sql);
		$returnArray = $this->db->returnArray($sql);
		return $returnArray;
	}
	function combineInfoWithStudents($ParInfoArray, $ParStudentsArray) {

		$returnArray = array();
		$sizeOfInfoArray = count($ParInfoArray);
		$sizeOfStudentsArray = count($ParStudentsArray);

		for($i=0;$i<$sizeOfInfoArray;$i++) {
			for($j=0;$j<$sizeOfStudentsArray;$j++){
				if ($ParStudentsArray[$j]["EVENTID"] == $ParInfoArray[$i]["EVENTID"]) {
					$ParInfoArray[$i]["SUCCESS"] = true;
					$ParInfoArray[$i]["ENDDATE"] = $ParStudentsArray[$j]["ENDDATE"];
				} else {
					// do nothing
				}
			}
		}
		$returnArray = $ParInfoArray;
		return $returnArray;
	}
	
	function setDisplayType($ParDisplayType=DISPLAY_TYPE_HTML) {
		switch($ParDisplayType) {
			case $this->DISPLAY_TYPE_HTML:
				$this->displayType = self::DISPLAY_TYPE_HTML;
				break;
			case $this->DISPLAY_TYPE_PRINT:
				$this->displayType = self::DISPLAY_TYPE_PRINT;
				break;
			default:
				$this->displayType = self::DISPLAY_TYPE_HTML;
				break;
		}
	}
	
	function getHtmlStyle() {
		$cssArray = array();
		$cssArray["TABLE1"] = "  width='100%' cellspacing='1' cellpadding='4' border='0' bgcolor='#cccccc' ";
		$cssArray["TR1"] = " class='tabletop' ";
		$cssArray["TD1"] = " class='tabletopnolink' ";
		$cssArray["TD2_ODD"] = " class='tabletext' height='20' bgcolor='#ffffff' align='center' ";
		$cssArray["TD2_EVEN"] = " class='tabletext' height='20' bgcolor='#f3f3f3' align='center' ";
		return $cssArray;
	}
	
	function getPrintStyle() {
		$cssArray = array();
		return $cssArray;
	}
	
	function generateCheckListHtml($ParCheckListResultArray) {
		global $Lang, $intranet_session_language;
		
		# setting css
		$cssArray = array();
		if ($this->displayType == self::DISPLAY_TYPE_HTML) {
			$cssArray = $this->getHtmlStyle();
		} else if ($this->displayType == self::DISPLAY_TYPE_PRINT) {
			$cssArray = $this->getPrintStyle();
		} else {
			// do nothing
		}
		
		# construct table shell
		$table = "<table " . $cssArray["TABLE1"] . ">
					<thead>
					<tr " . $cssArray["TR1"] . ">
						<td " . $cssArray["TD1"] . ">
							" . $Lang["Cust_Cnecc"]["Domain"] . "
						</td>
						<td " . $cssArray["TD1"] . ">
							" . $Lang["Cust_Cnecc"]["Event"] . "
						</td>
						<td " . $cssArray["TD1"] . ">
							" . $Lang["Cust_Cnecc"]["Attainment"] . "
						</td>
						<td " . $cssArray["TD1"] . ">
							" . $Lang["Cust_Cnecc"]["Role"] . "
						</td>
						<td " . $cssArray["TD1"] . ">
							" . $Lang["Cust_Cnecc"]["Point(s)"] . "
						</td>
						<td " . $cssArray["TD1"] . ">
							<span id='noOfSuccess'></span>" . $Lang["Cust_Cnecc"]["Success(v)"] . "<input type='checkbox' id='successCheck' onClick='checkUncheckAll()' />
						</td>
						<td " . $cssArray["TD1"] . ">
							" . $Lang["Cust_Cnecc"]["EndDate"] . "
						</td>
					</tr>
					</thead>
					<tbody>
					{{{ TBODY }}}
					</tbody>
				</table>";

		# construct table body
		$libinterface = new interface_html();
		$tbody = "";
		$row = 0;
		foreach($ParCheckListResultArray as $key => $info) {
			$name = $info["NAME"];
			$elements = $info["ELEMENTS"];
//			debug_r($name);
			$sizeOfElements = count($elements);
			$elementsHtml = "";
//debug_r($elements);die;
			if ($row%2==0) {
				$tdstyle = $cssArray["TD2_ODD"];
			} else {
				$tdstyle = $cssArray["TD2_EVEN"];
			}
			if ($sizeOfElements > 0) {
				for($j=0;$j<$sizeOfElements;$j++) {
					$checked = "";
					if ($elements[$j]["SUCCESS"]) {
						$checked = "checked='checked'";
					} else {
						
					}
					$elementsHtml .= "<tr>";
					if ($j==0) {
						$elementsHtml .= "{{{ DOMAIN }}}";
					} else {
						// do nothing
					}
					$elementsHtml .= "<td $tdstyle>" . $elements[$j]["TITLE"] . "</td>";
					$elementsHtml .= "<td $tdstyle>" . $elements[$j]["ATTAINMENT"] . "</td>";
					$elementsHtml .= "<td $tdstyle>" . $elements[$j]["ROLE"] . "</td>";
					$elementsHtml .= "<td $tdstyle>" . $elements[$j]["POINT"] . "</td>";
					$elementsHtml .= "<td $tdstyle><input type='checkbox' id='success' name='success[]' onClick='writeSuccess();' value='" . $elements[$j]["EVENTID"] . "' $checked /></td>";
					
					$dateBox = "<input type='text' name='date" . $elements[$j]["EVENTID"] . "' size='8' maxlength='10' class='tabletext' value='" . $elements[$j]["ENDDATE"] . "' onFocus='if(this.value==\"\"){this.value=\"" . date('Y-m-d') . "\";}' />";
					$dateBox .= $libinterface->GET_CALENDAR("form1", "date" . $elements[$j]["EVENTID"] , "");//$elements[$j]["EVENTID"]
					$elementsHtml .= "<td $tdstyle nowrap>" . $dateBox . "</td>";
					$elementsHtml .= "</tr>";
				}
			} else {
				$elementsHtml .= "<tr>";
				$elementsHtml .= "{{{ DOMAIN }}}";
				$elementsHtml .= "<td colspan='6' $tdstyle>" . $Lang["Cust_Cnecc"]["NoRecordFound"] . "</td>";
				$elementsHtml .= "</tr>";
			}
			$tbody .= str_replace("{{{ DOMAIN }}}", "<td " . $cssArray["TR1"] . $cssArray["TD1"] . " rowspan='" . ( $sizeOfElements==0?"1":$sizeOfElements) . "'>$name</td>", $elementsHtml);
			$row++;
		}
		$Html .= str_replace("{{{ TBODY }}}", $tbody, $table);
		
		return $Html;
	}
	function categorizeByEle($ParEleArray, $ParResultArray) {
		$returnArray = array();
		$sizeOfEleArray = count($ParEleArray);
		$sizeOfResultArray = count($ParResultArray);

		foreach($ParEleArray as $key => $name) {
			$tempArray = array("NAME"=>$name);
			for($i=0;$i<$sizeOfResultArray;$i++) {
				$resultEleArray = explode(",", $ParResultArray[$i]["ELE"]);
				$resultFirstEle = $resultEleArray[0];
				if ($key == $resultFirstEle) {
					$tempArray["ELEMENTS"][] = $ParResultArray[$i];
				} else {
					// do nothing
				}
			}
			$returnArray[$key] = $tempArray;
		}
		return $returnArray;
	}
	
	function getCategorizedResult() {
		# combine students success status to event
		####################################################
		# get event info
		$eventInfo = $this->getEventInfo();
		
		# if get table with user id
		if ($this->studentId > -1) {
			# get joint student
			$students = $this->getStudentsSuccessInfo($this->studentId);

			# combine event info and joint student
			$combinedResult = $this->combineInfoWithStudents($eventInfo,$students);

		} else {
			# combined result = event info
			$combinedResult = $eventInfo;
		}
		
		# categorize the combined results by ELE
		####################################################
		# get ELE list
		$libpf_slp = new libpf_slp();
		$eleList = $libpf_slp->GET_ELE();	// Domain
		
		$catergorizedResult = $this->categorizeByEle($eleList, $combinedResult);
		return $catergorizedResult;
	}
	
	function getCheckListTable() {
		# get categorized result
		$catergorizedResult = $this->getCategorizedResult();
//		debug_r($catergorizedResult);die;
		# generate check list html
		$Html = $this->generateCheckListHtml($catergorizedResult);
		
		# return html
		return $Html;
	}
	
	function getRecordIdByEventId($ParEventId) {
		global $eclass_db;
		$returnResult = false;
		$sql = "SELECT
					DISTINCT OS.RECORDID
				FROM
					$eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
					INNER JOIN $eclass_db.OLE_PROGRAM OP
						ON CCSE.PROGRAMID = OP.PROGRAMID
					INNER JOIN $eclass_db.OLE_STUDENT OS
						ON OS.PROGRAMID = OP.PROGRAMID
				WHERE
					CCSE.STATUS = " . self::ENABLED . "
					AND
					CCSE.EVENTID = $ParEventId
					AND
					OS.USERID = " . $this->studentId;

		$returnArray = $this->db->returnArray($sql);
		$returnResult = $returnArray[0]["RECORDID"];
		return $returnResult;
	}
	
	function getProgramInfoByEventId($ParEventId) {
		global $eclass_db;
		$sql = "SELECT
					DISTINCT OP.PROGRAMID,
					OP.TITLE,
					OP.STARTDATE,
					OP.ENDDATE,
					OP.CATEGORY,
					OP.ELE,
					OP.INTEXT
				FROM
					$eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
					INNER JOIN $eclass_db.OLE_PROGRAM OP
						ON CCSE.PROGRAMID = OP.PROGRAMID
				WHERE
					CCSE.EVENTID = $ParEventId";
//		debug_r($sql);
		$returnArray = $this->db->returnArray($sql);
		return $returnArray[0];
	}
	
	function getEventIds() {
		global $eclass_db;
		$returnVector = array();
		$sql = "SELECT
					DISTINCT CCSS.EVENTID
				FROM
					$eclass_db.CUSTOM_CNECC_SS_STUDENT CCSS
				WHERE
					CCSS.STUDENTID = " . $this->studentId;
		$returnVector = $this->db->returnVector($sql);
		return $returnVector;
	}
	
	function insertOleStudentByEventId($ParEventId) {
		global $eclass_db, $ipf_cfg;
		$programInfo = $this->getProgramInfoByEventId($ParEventId);

		$approvalPerson = $_SESSION["UserID"];
		$sql = "INSERT INTO
					$eclass_db.OLE_STUDENT
				SET
					USERID = " . $this->studentId . ",
					TITLE = '" . $programInfo["TITLE"] . "',
					CATEGORY = '" . $programInfo["CATEGORY"] . "',
					ELE = '" . $programInfo["ELE"] . "',
					ROLE = '',
					HOURS = '',
					ACHIEVEMENT = '',
					ORGANIZATION = '',
					APPROVEDBY = $approvalPerson,
					RECORDSTATUS = " . $ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"] . ",
					DETAILS = '',
					PROGRAMID = " . $programInfo["PROGRAMID"] . ",
					PROCESSDATE = NOW(),
					STARTDATE = '" . $programInfo["STARTDATE"] . "',
					ENDDATE = '" . $programInfo["ENDDATE"] . "',
					INPUTDATE = NOW(),
					MODIFIEDDATE = NOW(),
					INTEXT = '" . $programInfo["INTEXT"] . "',
					COMEFROM = " . $ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherInput"];
//	debug_r($sql);die;
		$this->db->db_db_query($sql);
		
		return $this->db->db_insert_id();
	}
	
	function updateRecord($ParStudentId=-1, $ParEventId, $ParEndDate=null) {
		global $eclass_db;
		if (isset($ParEndDate) && !empty($ParEndDate) && $ParEndDate != '0000-00-00') {
			$endDate = "'" . $ParEndDate . "'";
		} else {
			$endDate = 'NULL';
		}
		
		$sql = "UPDATE
					$eclass_db.CUSTOM_CNECC_SS_STUDENT
				SET
					ENDDATE = $endDate,
					MODIFIEDDATE = NOW()
				WHERE
					STUDENTID = $ParStudentId
					AND
					EVENTID = $ParEventId";
//		debug_r($sql);
		$this->db->db_db_query($sql);
		return mysql_info();
	}
	
	function insertRecord($ParOsRecordId, $ParEventId, $ParEndDate=null) {
		global $eclass_db;
		if (isset($ParEndDate) && !empty($ParEndDate)) {
			$endDate = "'" . $ParEndDate . "'";
		} else {
			$endDate = 'NULL';
		}
		
		$sql = "INSERT INTO
					$eclass_db.CUSTOM_CNECC_SS_STUDENT
				SET
					OS_RECORDID = $ParOsRecordId,
					EVENTID = $ParEventId,
					STUDENTID = " . $this->studentId . ",
					ENDDATE = $endDate,
					INPUTDATE = NOW(),
					MODIFIEDDATE = NOW()";
//		debug_r($sql);die;
		$this->db->db_db_query($sql);
		return $this->db->db_insert_id();
	}
	
	function getCcseEventIds() {
		global $eclass_db;
		$sql = "SELECT
					CCSE.EVENTID
				FROM
					$eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
					INNER JOIN $eclass_db.OLE_PROGRAM OP
					ON CCSE.PROGRAMID = OP.PROGRAMID
				WHERE
					OP.INTEXT = 'INT'
					AND
					CCSE.STATUS = " . self::ENABLED . "
					AND
					OP.ACADEMICYEARID = $this->academicYearId";
		$returnVector = array();
		$returnVector = $this->db->returnVector($sql);
//		debug_r($returnVector);die;
		return $returnVector;
	}
	
	function removeOleStudent($ParRemoveIdArray) {
		global $eclass_db;
		$q_result = false;
		if (count($ParRemoveIdArray) > 0) {
			$removeIdString = implode(",", $ParRemoveIdArray);
			$sql = "DELETE FROM
						$eclass_db.OLE_STUDENT
					WHERE
						RECORDID IN ($removeIdString)";
			$q_result = $this->db->db_db_query($sql);
		} else {
			$q_result = true;
		}
		return $q_result;
	}
	
	function getStudentCcssOsRecordIds() {
		global $eclass_db;
		$returnVector = array();
		$sql = "SELECT
					DISTINCT CCSS.OS_RECORDID
				FROM
					$eclass_db.CUSTOM_CNECC_SS_STUDENT CCSS
				INNER JOIN $eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
					ON CCSS.EVENTID = CCSE.EVENTID
				INNER JOIN $eclass_db.OLE_PROGRAM OP
					ON CCSE.PROGRAMID = OP.PROGRAMID
				WHERE
					OP.INTEXT='INT'
				AND
					CCSE.STATUS=" . self::ENABLED . "
				AND
					OP.ACADEMICYEARID = $this->academicYearId";
		$returnVector = $this->db->returnVector($sql);
		return $returnVector; 
	}
	
	function getCcssRecordIdForRemove($ParEventIdArray) {
		global $eclass_db;
		$eventIdCond = "";
		if (isset($ParEventIdArray) && count($ParEventIdArray)>0) {
			$eventIdString = implode(",",$ParEventIdArray);
			$eventIdCond = " AND
					CCSS.EVENTID NOT IN ($eventIdString) ";
		} else {
			// do nothing
		}
		$sql = "SELECT CCSS.RECORDID
				FROM $eclass_db.CUSTOM_CNECC_SS_STUDENT CCSS
				INNER JOIN $eclass_db.CUSTOM_CNECC_SS_EVENT CCSE
					ON CCSS.EVENTID = CCSE.EVENTID
				INNER JOIN $eclass_db.OLE_PROGRAM OP
					ON CCSE.PROGRAMID = OP.PROGRAMID
				WHERE
					CCSS.STUDENTID = $this->studentId
					AND
					OP.ACADEMICYEARID = $this->academicYearId
					$eventIdCond";
		$returnVector = $this->db->returnVector($sql);
		return $returnVector;
	}
	
	function removeRecord($ParEventIdArray) {
		global $eclass_db;
		$q_result = true;
		$ccssRecordIdArray = $this->getCcssRecordIdForRemove($ParEventIdArray);
		$ccssRecordIdString = implode(",",$ccssRecordIdArray);
		
		# student id should be exist
		if ($this->studentId<0) {
			// do nothing
		} else {
			$preOsRecordIds = $this->getStudentCcssOsRecordIds();			

			if (empty($ccssRecordIdString)) {
				// do nothing
			} else {
			# delete record in CCSS
			$sql = "DELETE FROM
						$eclass_db.CUSTOM_CNECC_SS_STUDENT
					WHERE
						RECORDID IN ($ccssRecordIdString)";
			$q_result = $this->db->db_db_query($sql);
			}
			$postOsRecordIds = $this->getStudentCcssOsRecordIds();
			
			# use the pre and post os_recordId comparison to remove ole_student
			$removeIdArray = array();
			if (count($preOsRecordIds)>0) {
				foreach($preOsRecordIds as $key => $preId) {
					if (in_array($preId, $postOsRecordIds)) {
						// do nothing
					} else {
						$removeIdArray[] = $preId;
					} 
				}
				$q_result = $q_result && $this->removeOleStudent($removeIdArray);
			} else {
				// do nothing
			}
		}
		return $q_result;
	}

	///////////////// REPORT FUNCTIONS //////////////////
	function getWholeReportTemplate() {
		$Html = "<html>
				<body>
				<link href='report_style.css' rel='stylesheet' type='text/css'>
				<table width='100%'>
					<!-- School Title and Info -->
					<tr>
						<td>{{{ SCHOOL TITLE AND INFO }}}</td>
					</tr>
					
					<!-- Student Info -->
					<tr>
						<td>{{{ STUDENT INFO }}}</td>
					</tr>
					
					<!-- Report Content -->
					<tr>
						<td>{{{ REPORT CONTENT }}}</td>
					</tr>
					
					<!-- Remarks -->
					<tr>
						<td>{{{ REMARKS }}}</td>
					</tr>
					
					<!-- Signatures -->
					<tr>
						<td>{{{ SIGNATURES }}}</td>
					</tr>
				</table>
				</body>
				<html>";
		return $Html;
	}
	function getAcademicYearInfoByAcademicYearId($ParAcademicYearId=-1) {
		global $intranet_session_language;
		if ($ParAcademicYearId<0) {
			$academicYearName = getCurrentAcademicYear();
		} else {
			$sql = "SELECT
						YEARNAME".strtoupper($intranet_session_language)."
					FROM
						$intranet_db.ACADEMIC_YEAR
					WHERE
						ACADEMICYEARID = $ParAcademicYearId";
			$returnVector = $this->db->returnVector($sql);
			$academicYearName = $returnVector[0];
		}
		return $academicYearName;
	}
	function generateSchoolTitleAndInfo() {
		$Html = "";
		$element = array();
		$element["SCHOOL_ICON"] = "<img src='xxx' alt='schoolIcon' />";
		$element["SCHOOL_NAME_ENG"] = "CNEC CHRISTIAN COLLEGE";
		$element["SCHOOL_NAME_CHI"] = "中華傳道會安柱中學";
		$element["REPORT_NAME_ENG"] = "STUDENT'S OTHER LEARNING EXPERIENCE RECORD";
		$element["REPORT_NAME_CHI"] = "學生其他學習經歷紀錄";
		$element["SCHOOL_YEAR"] = "( " . $this->getAcademicYearInfoByAcademicYearId($this->academicYearId) . " )";
		$element["ADDRESS"] = "6 Lei Pui Street,<br />Kwai Chung, N.T.,<br />Hong Kong.";
		$element["Tel"] = "852-24230365";
		$element["FAX"] = "852-24801429";
		
		$Html .= "<table width='100%'>
					<tr>
				
						<!-- School Icon -->
						<td>" . $element["SCHOOL_ICON"] . "</td>
				
						<!-- School Name and Report Name -->
						<td align='center'>" . 
						"<strong>" . $element["SCHOOL_NAME_ENG"] . "</strong><br />" . 
						$element["SCHOOL_NAME_CHI"] . "<br />" .  
						"<strong>" . $element["REPORT_NAME_ENG"] . "</strong><br />" .  
						$element["REPORT_NAME_CHI"] . "<br />" .  
						$element["SCHOOL_YEAR"] . 
						"</td>
				
						<!-- School Contact Detail -->
						<td>" . 
						$element["ADDRESS"] . "<br />" . 
						$element["Tel"] . "<br />" . 
						$element["FAX"] . 
						"</td>
					</tr>
				</table>";
		return $Html;
	}
	
	function generateStudentInfo() {
		global $intranet_session_language, $Lang;
		
		include("cnecc/lang/lang.en.php");
		$LangEN = $Lang["Cust_Cnecc"];
		include("cnecc/lang/lang.b5.php");
		$LangB5 = $Lang["Cust_Cnecc"];
		
		include("cnecc/lang/lang.$intranet_session_language.php");
				
		$Html = "";
		$Html = "<table id='student_info_table' width='100%'>
					<tr>
						<td width='110px'>" . $LangEN["NameOfStudent"] . "</td>
						<td width='100px' style='text-align:right'>" . $LangB5["NameOfStudent"] . "&nbsp; : </td>
						<td colspan='4'>" . $this->userInfo->EnglishName . "&nbsp;(" . $this->userInfo->ChineseName . ")" . "</td>
						<td width='80px'>" . $LangEN["RegNo"] . "</td>
						<td width='100px' style='text-align:right'>" . $LangB5["RegNo"] . "&nbsp; : </td>
						<td width='100px'>" . str_replace("#", "", $this->userInfo->WebSamsRegNo) . "</td>
					</tr>
					<tr>
						<td>" . $LangEN["Class&ClassNo"] . "</td>
						<td style='text-align:right;'>" . $LangB5["Class&ClassNo"] . "&nbsp; : </td>
						<td colspan='4'>" . $this->userInfo->ClassName . "&nbsp;&nbsp;&nbsp;(&nbsp;" . $this->userInfo->ClassNumber . "&nbsp;)</td>
						<td>" . $LangEN["HkidNo"] . "</td>
						<td style='text-align:right'>" . $LangB5["HkidNo"] . "&nbsp; : </td>
						<td>" . $this->userInfo->HKID . "</td>
					</tr>
					<tr>
						<td>" . $LangEN["DateOfBirth"] . "</td>
						<td style='text-align:right'>" . $LangB5["DateOfBirth"] . "&nbsp; : </td>
						<td width='100px;'>" . date("d/m/Y", strtotime($this->userInfo->DateOfBirth)) . "</td>
						<td width='30px;'>" . $LangEN["Sex"] . "</td>
						<td width='40px;' style='text-align:right'>" . $LangB5["Sex"] . "&nbsp; : </td>
						<td>" . $this->userInfo->Gender . "</td>
						<td>" . $LangEN["DateOfIssue"] . "</td>
						<td style='text-align:right'>" . $LangB5["DateOfIssue"] . "&nbsp; : </td>
						<td>" . date("d/m/Y") . "</td>
					</tr>
				</table>";
		
		return $Html;
	}
	
	function generateReportContent() {	
		global $Lang, $intranet_session_language;
		
		$catergorizedResult = $this->getCategorizedResult();

		$Html = "";

		# construct table shell
		$table = "<table id='report_content' width='100%'>
					<thead>
					<tr>
						<th>
							" . $Lang["Cust_Cnecc"]["Domain"] . "
						</th>
						<th>
							" . $Lang["Cust_Cnecc"]["TitleOfProgram"] . "
						</th>
						<th>
							" . $Lang["Cust_Cnecc"]["Category"] . "
						</th>
						<th>
							" . $Lang["Cust_Cnecc"]["Role"] . "
						</th>
					</tr>
					</thead>
					<tbody>
					{{{ TBODY }}}
					</tbody>
				</table>";

		# construct table body
		$libpf_slp = new libpf_slp();
		$CategoryArray = $libpf_slp->GET_OLR_CATEGORY();
		
		$tbody = "";
		foreach($catergorizedResult as $key => $info) {
			$domainName = $info["NAME"];
			$elements = $info["ELEMENTS"];

			$tempHtml = "";
			$rowcount=0;
			if (count($elements)>0) {
				foreach($elements as $eKey => $eInfo) {
					if ($eInfo["SUCCESS"]) {

						$tempHtml .= "<tr>";
						if ($rowcount==0) {
							$tempHtml = "<td rowspan='{{{ ROWCOUNT }}}'>$domainName</td>";
						} else {
							// do nothing
						}
						$tempHtml .= "<td>" . $eInfo["TITLE"] . "</td>";
						$tempHtml .= "<td>" . $CategoryArray[$eInfo["CATEGORYID"]] . "</td>";
						$tempHtml .= "<td>" . $eInfo["ROLE"] . "</td>";
						$tempHtml .= "</tr>";
						$rowcount++;
					} else {
						// do nothing
					}
				}
			}
			$tbody .= str_replace("{{{ ROWCOUNT }}}",$rowcount,$tempHtml);
		}
		if (empty($tbody)) {
			$tbody = "<tr><td colspan='4' style='text-align: center'>" . $Lang["Cust_Cnecc"]["NoRecordFound"] . "</td></tr>";
		} else {
			// do nothing
		}
		$Html = str_replace("{{{ TBODY }}}", $tbody, $table);
		
		if ($intranet_session_language == "en") {
			$displayName = $this->userInfo->EnglishName;
		} else {
			$displayName = $this->userInfo->ChineseName;
		}
		
		$Html .= "<span class='remark'>".str_replace("{{{ STUDENT_NAME }}}", $displayName, $Lang["Cust_Cnecc"]["RemarkContent"])."</span><br /><br />";
		return $Html;
	}
	
	function generateRemarks() {
		global $Lang,$intranet_session_language;
		$Html = "";
		$libpf_slp = new libpf_slp();
		$eleList = $libpf_slp->GET_ELE();
		$eleString = implode(", ", $eleList);
		$sizeOfEleList = count($eleList);
		$lang_and = ($intranet_session_language == "en" ? " and":" 及");
		$eleString = preg_replace('#,(?![^,]+,)#',$lang_and,$eleString); 		
		$Html = "<p class='remark'>" . str_replace(array("{{{ ELE NUMBERS }}}", "{{{ ELE ELEMENTS }}}"), array($sizeOfEleList, $eleString), $Lang["Cust_Cnecc"]["RemarkDescription"] ) . "</p>";
		return $Html;
	}
	
	function generateSignatures() {
		global $Lang, $intranet_session_language;
		
		$classTeacherVector = $this->getClassTeacher($this->studentId);
		$classTeacherDisplay = "(" . implode(")<br />(", $classTeacherVector) . ")";

		$Html = "";
		$Html = "<table id='signature_table' width='100%'>
					<tr>
						<td width='30%' style='border-bottom: 1px solid;'><br /><br /><br />&nbsp;</td>
						<td width='5%'>&nbsp;</td>
						<td width='30%' style='border-bottom: 1px solid;'><br /><br /><br />&nbsp;</td>
						<td width='5%'>&nbsp;</td>
						<td width='30%' style='border-bottom: 1px solid;'><br /><br /><br />&nbsp;</td>
					</tr>
					<tr style='text-align:center;'>
						<td width='30%'>" . $Lang["Cust_Cnecc"]["Parent/Guardian"] . "&nbsp;&nbsp;&nbsp;" . $Lang["Cust_Cnecc"]["Parent/Guardian"] . "</td>
						<td width='5%'>&nbsp;</td>
						<td width='30%'>" . $Lang["Cust_Cnecc"]["ClassTeacher"] . "&nbsp;&nbsp;&nbsp;" . $Lang["Cust_Cnecc"]["ClassTeacher"] . "</td>
						<td width='5%'>&nbsp;</td>
						<td width='30%'>" . $Lang["Cust_Cnecc"]["Principal"] . "&nbsp;&nbsp;&nbsp;" . $Lang["Cust_Cnecc"]["Principal"] . "</td>
					</tr>
					<tr style='text-align:center;' valign='top'>
						<td width='30%'>&nbsp;</td>
						<td width='5%'>&nbsp;</td>
						<td width='30%'>$classTeacherDisplay</td>
						<td width='5%'>&nbsp;</td>
						<td width='30%'>(" . $Lang["Cust_Cnecc"]["Mr.NgaiShuChiu"] . ")</td>
					</tr>
				</table>";
		return $Html;
	}
	
	function getClassTeacher() {
		global $intranet_db;
		$returnVector = array();
		$sql = "SELECT
					YCT.USERID
				FROM
					$intranet_db.INTRANET_USER IU
					INNER JOIN $intranet_db.YEAR_CLASS_USER YCU
						ON YCU.USERID = IU.USERID
					INNER JOIN $intranet_db.YEAR_CLASS YC
						ON YCU.YEARCLASSID = YC.YEARCLASSID
					INNER JOIN $intranet_db.YEAR_CLASS_TEACHER YCT
						ON YC.YEARCLASSID = YCT.YEARCLASSID
					INNER JOIN $intranet_db.ACADEMIC_YEAR AY
						ON YC.ACADEMICYEARID = AY.ACADEMICYEARID
				WHERE
					AY.ACADEMICYEARID = $this->academicYearId
					AND
					IU.USERID = $this->studentId";
		//	debug_r($sql);die;
		$returnVector = $this->db->returnVector($sql);
		
		if (count($returnVector) > 0) {
			$classTeacherIds = implode(",",$returnVector);
			$nameField = getNameFieldByLang("IU.");
			$sql = "SELECT
						$nameField AS CLASSTEACHER
					FROM
						$intranet_db.INTRANET_USER IU
					WHERE
						IU.USERID IN ($classTeacherIds)";
//			debug_r($sql);
			$returnVector = $this->db->returnVector($sql);
		} else {
			$returnVector = array();
		}
		return $returnVector;
	}
	
	function packDateOfIssue($ParUserInfoArray) {
		$sizeOfUserInfoArray = count($ParUserInfoArray);
		if ($sizeOfUserInfoArray>0) {
			$ParUserInfoArray[0][$sizeOfUserInfoArray] = $ParUserInfoArray[0]["DATE_OF_ISSUE"] = date("d/m/Y");
		}
		return $ParUserInfoArray;
	}
	
	
	function getUserInfo() {
		global $intranet_db;
		$returnArray = array();
		$sql = "SELECT
					IU.ENGLISHNAME,
					IU.CHINESENAME,
					YC.CLASSTITLEEN,
					YC.CLASSTITLEB5,
					YCU.CLASSNUMBER,
					IU.DATEOFBIRTH,
					IU.GENDER,
					IU.WEBSAMSREGNO,
					IU.HKID
				FROM
					$intranet_db.INTRANET_USER IU
					INNER JOIN $intranet_db.YEAR_CLASS_USER YCU
						ON YCU.USERID = IU.USERID
					INNER JOIN $intranet_db.YEAR_CLASS YC
						ON YCU.YEARCLASSID = YC.YEARCLASSID
					INNER JOIN $intranet_db.ACADEMIC_YEAR AY
						ON YC.ACADEMICYEARID = AY.ACADEMICYEARID
				WHERE IU.USERID = " . $this->studentId . " AND AY.ACADEMICYEARID = " . $this->academicYearId;
		$returnArray = $this->db->returnArray($sql);
		return $returnArray;
	}
	
	function isSunshineProgram($ParProgramId=-1) {
		global $eclass_db;
		if ($ParProgramId<0) {
			return false;
		} else {
			$sql = "SELECT
						COUNT(EVENTID)
					FROM
						$eclass_db.CUSTOM_CNECC_SS_EVENT
					WHERE
						PROGRAMID=$ParProgramId
					GROUP BY PROGRAMID";

			$returnVector = $this->db->returnVector($sql);

			$count = $returnVector[0];
			if ($count>0) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	function getSunshineProgramInfo($ParProgramId=-1) {
		global $eclass_db;
		$returnArray = array();
		if ($ParProgramId>0) {
			$sql = "SELECT
						EVENTID,
						ATTAINMENT,
						ROLE,
						POINT
					FROM
						$eclass_db.CUSTOM_CNECC_SS_EVENT
					WHERE
						PROGRAMID = $ParProgramId
						AND
						STATUS = " . self::ENABLED;
			$returnArray = $this->db->returnArray($sql);
		} else {
			// do nothing
		}
		return $returnArray;
	}
	
	function getResultDisplayCustomization($ParProgramId=-1) {
		global $Lang, $intranet_session_language;
		$sunshineProgramInfo = $this->getSunshineProgramInfo($ParProgramId);
		
		$sizeOfSsPgmInfo = count($sunshineProgramInfo);
		if ($sizeOfSsPgmInfo>0) {
			foreach($sunshineProgramInfo as $key => $element) {
				$infoDisplay .= "<tr><td>" . $element["ATTAINMENT"] . "</td><td>" . $element["ROLE"] . "</td><td>" . $element["POINT"] . "</td></tr>";
			}
		} else {
			// do nothing
		}
		$infoDisplay = "<table><tr><td>" . $Lang["Cust_Cnecc"]["Attainment"] . "</td><td>" . $Lang["Cust_Cnecc"]["Role"] . "</td><td>" . $Lang["Cust_Cnecc"]["Point(s)"] . "</td></tr>".$infoDisplay."</table>";
		
		
		$Html = "";
		$Html .= "<tr valign='top'>
						<td valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>" . $Lang["Cust_Cnecc"]["SunshineProgramInfo"] . "</span></td>
						<td>$infoDisplay</td>
					</tr>";
		return $Html;
	}
	
	function getOleNewCustomization($ParProgramId=-1) {
		global $ec_iPortfolio, $intranet_session_language, $Lang;
		
		$sunshineProgramInfo = array();
		
		if ($this->isSunshineProgram($ParProgramId)) {
			$ss_yes_check = " checked='checked' ";
			$sunshineProgramInfo = $this->getSunshineProgramInfo($ParProgramId);
		} else {
			$ss_no_check = " checked='checked' ";
		}
//		debug_r($sunshineProgramInfo);
		$sizeOfSsPgmInfo = count($sunshineProgramInfo);
		$setSunshineOptions = "";
		if ($sizeOfSsPgmInfo<1) {
			$createMultipleHtmlOptions = "createSunshineOption();\n";
		} else {
			for($i=0;$i<$sizeOfSsPgmInfo;$i++) {
				$createMultipleHtmlOptions .= "createSunshineOption('" . $sunshineProgramInfo[$i]["EVENTID"] . "');\n";
			}
		}
		if ($sizeOfSsPgmInfo>0) {
			for($i=0;$i<$sizeOfSsPgmInfo;$i++) {
				$setSunshineOptions .= "\$('input[name=cnecc_event_info_A" . $sunshineProgramInfo[$i]["EVENTID"] . "\\[\\]]').get(0).value='" . $sunshineProgramInfo[$i]["ATTAINMENT"] . "';\n";
				$setSunshineOptions .= "\$('select[name=cnecc_event_info_R" . $sunshineProgramInfo[$i]["EVENTID"] . "\\[\\]]').get(0).value='" . $sunshineProgramInfo[$i]["ROLE"] . "';\n";
				$setSunshineOptions .= "\$('input[name=cnecc_event_info_P" . $sunshineProgramInfo[$i]["EVENTID"] . "\\[\\]]').get(0).value='" . $sunshineProgramInfo[$i]["POINT"] . "';\n";
			}
		} else {
			// do nothing
		}
		$availableRoles = ConvertToJSArray($this->roles, "availableRoles");
		$Html = $availableRoles."<script language='javascript'>
					var newEventCount = 0;
					\$(document).ready(function(){
						$createMultipleHtmlOptions$setSunshineOptions
						var sunshineSet = \$('input[name=setSunshine]:checked').val();
						if (sunshineSet == 1) {
								setSunshineSetting(sunshineSet);
						}
					});
					function getRoleSelection(eventId) {
						
						var selectOptions = '';
						for(i=0;i<availableRoles.length;i++) {
							selectOptions = selectOptions + '<option value=\"'+availableRoles[i]+'\">'+availableRoles[i]+'</option>'; 
						}
						selectOptions = '<select name=\"cnecc_event_info_R'+eventId+'[]\">'+selectOptions+'</select>';
						return selectOptions;
					}
					function createSunshineOption(eventId) {
						if (eventId == null || eventId == '') {
							newEventCount = newEventCount + 1;
							eventId = '_N'+newEventCount;							
						}
						var selectString = getRoleSelection(eventId);
						\$('#sunshineInput').append('<tr><td><input type=\"text\" name=\"cnecc_event_info_A'+eventId+'[]\" /></td><td>'+selectString+'</td><td><input type=\"text\" name=\"cnecc_event_info_P'+eventId+'[]\" value=\"1\" /></td><td><a href=\"javascript: void(0);\" class=\"tablelink\" onClick=\"\$(this).parent().parent().remove();\">X</a></td></tr><input type=\"hidden\" name=\"cnecc_event_id[]\" value=\"'+eventId+'\" />');
					}

					function setSunshineSetting(on) {
						if (on) {
							\$('input:radio[name=allowStudentsToJoin]').val(0).attr('checked','checked');
							\$('#joinSelectField').hide(); \$('#joinOptions').hide();
							\$('#sunshineOption').show();

//// comment: this jquery do not work for IE7
////							\$('form[name=form1]').attr('onsubmit', 'return checkForSunshineSetting(this)');

//// comment: instead use the DOM to set and bind the submit function
							document.form1.onsubmit = 'return checkForSunshineSetting(this)';
							\$('form[name=form1]').submit(function() {
								return checkForSunshineSetting(\$(this).get(0));
							});
						} else {
							\$(\"#joinSelectField\").show();
							\$('#sunshineOption').hide();

//// comment: this jquery do not work for IE7
////							\$('form[name=form1]').attr('onSubmit', 'return checkform(this);');
							document.form1.onsubmit = 'return checkform(this)';
							\$('form[name=form1]').submit(function() {
								return checkform(\$(this).get(0));
							});
						}
					}

					function checkForSunshineSetting(obj) {

						if (!checkform(obj)) return false;

						try {
							var checkedEle = \$('input[name=ele\\[\\]]:checked').length;

							if (checkedEle!=1) {
								throw '" . $Lang["Cust_Cnecc"]["PleaseChooseExactlyOneOption"] . "';
							}

							var attainment = \$('input[name*=cnecc_event_info_A]');
							var role = \$('input[name*=cnecc_event_info_R]');
							var point = \$('input[name*=cnecc_event_info_P]');

							var sunshineSet = \$('input[name=setSunshine]:checked').val();

							if (sunshineSet == 1 && attainment.length<1) {
								throw '" . $Lang["Cust_Cnecc"]["PleaseEnterSunshineProgramInfo"] . "';
							}


							\$.each(attainment, function(key, inputElement){
								if (inputElement.value == '') {
									throw '" . $Lang["Cust_Cnecc"]["Attainment(s)Empty"] . "';
								}
							});
	
							\$.each(role, function(key, inputElement){
								if (inputElement.value == '') {
									throw '" . $Lang["Cust_Cnecc"]["Role(s)Empty"] . "';
								}
							});

							\$.each(point, function(key, inputElement){
								if (inputElement.value == '') {
									throw '" . $Lang["Cust_Cnecc"]["Point(s)Empty"] . "';
								} else if (isNaN(inputElement.value)) {
									throw '" . $Lang["Cust_Cnecc"]["Point(s)ShouldBeANumber"] . "';
								}
							});

						} catch (errMsg) {
							alert(errMsg);
							return false;
						}						
						\$('input:radio[name=allowStudentsToJoin]').val(0).attr('checked','checked');
						return true;
					}
				</script>
				
				<!-- Sunshine Program -->
				<tr>
					<td valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><div id='div_sunshine'>" . $Lang["Cust_Cnecc"]["SetAsSunshineProgram"] . "</div></span></td>
					<td>
						<label for='ss_yes'>" . $ec_iPortfolio['SLP']['Yes']. "</label><input id='ss_yes' name='setSunshine' value='1' type='radio' $ss_yes_check onClick='javascript: setSunshineSetting(1)'/>
						<label for='ss_no'>" . $ec_iPortfolio['SLP']['No'] . "</label><input id='ss_no' name='setSunshine' value='0' type='radio' $ss_no_check onClick='javascript: setSunshineSetting(0)' />
					</td>
				</tr>
				<tr id='sunshineOption' style='display: none;'>
					<td>&nbsp;</td>
					<td>
						<table class='tablerow2' align='center' border='0' cellpadding='5' cellspacing='0' width='100%'>
							<thead>
							<tr>
								<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>" . $Lang["Cust_Cnecc"]["Attainment"] . "</td>
								<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>" . $Lang["Cust_Cnecc"]["Role"] . "</td>
								<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>" . $Lang["Cust_Cnecc"]["Point(s)"] . "</td>
								<td width='10%' valign='top' nowrap='nowrap' class='formfieldtitle'><a href='javascript: void(0);' class='tablelink' onClick='createSunshineOption()'>+</a></td>
							</tr>
							</thead>
							<tbody id='sunshineInput'>
							</tbody>
						</table>
					</td>
				</tr>";
		return $Html;
	}
	
	function removeCcseProgram($ParProgramID=-1){
		global $eclass_db;
		
		$sql = "DELETE FROM
					$eclass_db.CUSTOM_CNECC_SS_EVENT
				WHERE
					PROGRAMID = $ParProgramID";
		$this->db->db_db_query($sql);
	}
	
	function saveCcseProgram($ParProgramID, $ParInsertArray, $ParUpdateArray) {
		global $eclass_db;
		$sizeofInsertArray = count($ParInsertArray);
		$sizeofUpdateArray = count($ParUpdateArray);
//		debug_r($ParInsertArray);die;
		# INSERT
		for($i=0;$i<$sizeofInsertArray;$i++) {			
			$insertElement = $ParInsertArray[$i];
//			debug_r($insertElement);
			$attainment = $insertElement["ATTAINMENT"];
			$role = $insertElement["ROLE"];
			$point = $insertElement["POINT"];
			
			$sql = "INSERT INTO $eclass_db.CUSTOM_CNECC_SS_EVENT
					SET
						PROGRAMID = $ParProgramID,
						ATTAINMENT = '" . $attainment . "',
						ROLE = '" . $role . "',
						POINT = " . $point . ",
						STATUS = 1,
						INPUTDATE = NOW(),
						MODIFIEDDATE = NOW()";
//						debug_r($sql);
			$this->db->db_db_query($sql);
		}
		
		# UPDATE AND DELETE
		foreach($ParUpdateArray as $key => $elements) {
			
			$attainment = $elements["ATTAINMENT"];
			$role = $elements["ROLE"];
			$point = $elements["POINT"];
			
			if (empty($attainment)) {
				# DELETE
				$sql = "DELETE FROM
							$eclass_db.CUSTOM_CNECC_SS_EVENT
						WHERE
							EVENTID=".$key;
			} else {
				# UPDATE
				$sql = "UPDATE
							$eclass_db.CUSTOM_CNECC_SS_EVENT
						SET
							ATTAINMENT = '" . $attainment . "',
							ROLE = '" . $role . "',
							POINT = " . $point . ",
							MODIFIEDDATE = NOW()
						WHERE
							EVENTID=".$key;
			}
//			debug_r($sql);die;	
			$this->db->db_db_query($sql);
		}

	}
	//-------------------------------
	function generateWordReport() {
		$output = "";
		return $output;
	}
	function generatePdfReport() {
		$output = "";
		return $output;
	}
	
}

?>