<?php

class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	
	private $AcademicYearStr;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;

		$this->uid = $uid; 
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->issuedate = $issuedate;
		
		//$this->colorBlue = "#3300FF";
		$this->colorBlue = "#000000";
		
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '—';
		$this->SelfAccount_Break = '[P]';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#C8C8C8';
		$this->TableTitleRemarkBgColor = '#E0E0E0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->SchoolTitleEn = 'Ma Kam Ming Charitable Foundation Ma Chan Duen Hey Memorial College';
		$this->SchoolTitleCh = '馬錦明慈善基金馬陳端喜紀念中學';
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	
	
	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '20';

//		$imgFile = get_website().'/file/reportcard2008/templates/sha_tin_methodhist.gif';
 		$imgFile = get_website().'/includes/portfolio25/customize/mcdhmc/mcdhmc_SchoolLogo.png';

		$schoolLogoImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:90px;">' : '&nbsp;';
	
		$header_title1 = '<b>'.$this->SchoolTitleEn.'</b>';
		$header_title2 = $this->SchoolTitleCh;
		$header_title3 = "STUDENT LEARNING PROFILE";
		$header_title4 = "學生學習概覽";
		
		$EmptySpace_html = '<tr><td>'.'&nbsp;'.'</td></tr>'."\r\n";
		
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:center; border:0px;">
					<col width ="5%"/>
					<col width ="95%"/>
					<tr>';
				$x .=	'<td rowspan="4" align="center">'.$schoolLogoImage.'</td>';
				$x .= 	'<td class="report_title" style=""><b>'.$header_title1.'</b></td>
						
				   </tr>'; 

			$x .= '<tr><td class="report_title" style=""><font face="新細明體"><b>'.$header_title2.'</b></font></td></tr>'."\r\n";
			$x .= '<tr><td class="report_title" style=""><b>'.$header_title3.'</b></td></tr>'."\r\n";
			$x .= '<tr><td class="report_title" style=""><b>'.$header_title4.'</b></td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		
//		$x = '';
//		$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:center; border:0px;">';
//			$x .= '<tr><td class="" style="height:120px;">&nbsp;</td></tr>';
//		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						ChineseName,
						HKID,
						DateOfBirth,
						Gender,
						ClassName,
						WebSAMSRegNo,
						STRN
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And (RecordType = '2' OR UserLogin like 'tmpUserLogin_%')
				";

		$result = $this->objDB->returnResultSet($sql);

		return $result;
	}
	private function getStudentInfoDisplay($result)
	{	
		$fontSize = 'font-size:14px;';
		$fontSize2 = 'font-size:13px;';
		
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		$_borderBottom =  'border-bottom: 1px solid black; ';
	
			
		$DOB = $result[0]["DateOfBirth"];

		if (date($DOB)==0) {
			$DOB = $this->EmptySymbol;
		} else {
			$DOB = date('jS F Y',strtotime($DOB));
		} 
		
		$IssueDate = $this->issuedate;
		
		if (date($IssueDate)==0) {
			$IssueDate = $this->EmptySymbol;
		} else {
			$IssueDate = date('jS F Y',strtotime($IssueDate));
		}
		
		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $this->EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];		
		$Gender = ($Gender=='')? $this->EmptySymbol:$Gender;
		
		$ClassName = $result[0]["ClassName"];		
		$ClassName = ($ClassName=='')? $this->EmptySymbol:$ClassName;
		
		$RegistrationNo = str_replace("#", "", $result[0]["WebSAMSRegNo"]);
		$RegistrationNo = ($RegistrationNo=='')? $this->EmptySymbol:$RegistrationNo;

		$STRNNo = $result[0]["STRN"];	
		$STRNNo = ($STRNNo=='')? $this->EmptySymbol:$STRNNo;


		$colWidth_html = '<col width="20%" />
						  <col width="35%" />
						  <col width="20%" />
						  <col width="25%" />
						  ';

		$html = '';
		$html .= '<table width="90%" cellpadding="4" border="0px" align="center" style="border-collapse: collapse;">';
		
		$html .= $colWidth_html;

		$html .=	'<tr>
							<td class="font_16px">Student Name</td>
							<td class="font_16px">'.$EngName.' <font face="新細明體">('.$ChiName.')</td>
							<td class="font_16px">I.D. Number</td>
							<td class="font_16px">'.$HKID.'</td>
							
					</tr>
					<tr>
							<td class="font_16px">Class</td>
							<td class="font_16px">'.$ClassName.'</td>
							<td class="font_16px">Date of Issue</td>
							<td class="font_16px">'.$IssueDate.'</td>
					</tr>';

		$html .= "</table>";
	
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	

	
	/***************** Part 3 : Other Learning Experience******************/	
	public function getPart3_HTML($OLEInfoArr_INT){
		
		$Part3_Title = '<b>其他學習經歷 Other Learning Experiences (OLE)</b>';
	//	$Part3_Remark = '*Evidence of awards/certifications/achievements listed is available for submission when required.';
		
		$Part3_HeaderRemark = '除核心及選修科目外，學生在高中階段，所參與由學校舉辦或與校外機構合辦的學習活動就是其他學習經歷。';
		$Part3_HeaderRemark .='當中包括德育及公民教育，藝術發展，體育發展，社會服務及與工作有關的經驗。';
		$Part3_HeaderRemark .='學校已確認有關資料。<br/>';
		$Part3_HeaderRemark .='Apart from core and elective subjects, \'Other Learning Experiences\' are the learning programs that the student participated ';
		$Part3_HeaderRemark .='during the senior secondary education. It includes Moral and Civic Education, Aesthetic Development, Physical ';
		$Part3_HeaderRemark .='Development, Community Service and Career-Related Experiences. \'Other Learning Experiences\' could be gained through ';
		$Part3_HeaderRemark .='programs organized by the school or co-organized with the school from outside organizations.';
		
		$titleArray = array();	
		$titleArray[] = '活動項目<br/>Programs';
		$titleArray[] = '學年<br/>School Year';
		$titleArray[] = '參與角色<br/>Role of Participation';
		$titleArray[] = '合辦機構<br/>(如有)<br/>Partner Organizations<br/>(if any)';
		$titleArray[] = '其他學習<br/>經歷範疇<br/>Components<br/>of OLE';
		$titleArray[] = '獎項 /<br/>證書文憑 /<br/>成就*(如有)<br/>Awards /<br/>Certifications /<br/>Achievements*(if any)';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="19%" />';
		$colWidthArr[] = '<col width="16%" />';
		$colWidthArr[] = '<col width="14%" />';
		$colWidthArr[] = '<col width="19%" />';
		$colWidthArr[] = '<col width="9%" />';
		$colWidthArr[] = '<col width="23%" />';
		
		$DataArray = array();
		$DataArray = $OLEInfoArr_INT;
	
		$StringDisplayArr ['Title']= $Part3_Title;
		$StringDisplayArr ['Remark']=  $Part3_Remark;
		
		$html = '';
		
		$html .= $this->getPartContentDisplay_PageRowLimit('3',$titleArray,$colWidthArr,$DataArray,$hasPageBreak=true,$StringDisplayArr,$Part3_HeaderRemark);
		
		return $html;
		
	}	
	/***************** End Part 3 ******************/
	
	
	
	/***************** Part 4 ******************/	
	public function getPart4_HTML($OLEInfoArr_EXT){
		
		$Part4_Title = '<b>校外表現 / 獎項及重要參與 Performances / Awards and Key Participation Outside of School</b>';
		$Part4_Remark = '有需要時可提供 獎項 / 證書文憑 / 成就 作證明<br/>';
		$Part4_Remark .= 'Evidence of awards/ certifications/ achievements listed is available for submission when required';
		
		$Part4_HeaderRemark = '學生可向學校提供一些在高中階段曾參與過而並非由學校舉辦的學習活動資料。';
		$Part4_HeaderRemark .='學校不須確認學生的參與資料。';
		$Part4_HeaderRemark .='在有需要時，學生須負全責向相關人仕提供適當證明。<br/>';
		$Part4_HeaderRemark .='For learning programs not organized by the school during the senior secondary education, ';
		$Part4_HeaderRemark .='the student could provide the information to the school.';
		$Part4_HeaderRemark .='It is not necessary for the school to validate information below.';
		$Part4_HeaderRemark .='The student should hold full responsibility to provide references upon request.';
		
		
		$titleArray = array();	
		$titleArray[] = '活動項目<br/>Programs';
		$titleArray[] = '學年<br/>School Year';
		$titleArray[] = '合辦機構 (如有)<br/>Partner Organizations (if any)';
		$titleArray[] = '獎項 / 證書文憑 /<br/>成就*(如有)<br/>Awards /<br/>Certifications /<br/>Achievements*(if any)';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="32%" />';
		$colWidthArr[] = '<col width="11%" />';
		$colWidthArr[] = '<col width="34%" />';
		$colWidthArr[] = '<col width="23%" />';
		
		//$DataArr = array();
		//$DataArr = $this->getAward_Info();
		
		$StringDisplayArr ['Title']= $Part4_Title;
		$StringDisplayArr ['Remark']=  $Part4_Remark;
		
		$html = ''; 
		
		$html .= $this->getPartContentDisplay_NoRowLimit('4', $Part4_Title,$titleArray,$colWidthArr,$OLEInfoArr_EXT,$hasPageBreak=false,$StringDisplayArr,$Part4_HeaderRemark);
		
		return $html;
		
	}
	
	
	
	/***************** End Part 4 ******************/
	
	
	/***************** Part 5 ******************/
	public function getPart5_HTML(){
		
		$Part5_Title = '<b>校內頒發的主要獎項及成就 List of Awards and Major Achievements Issued by the School</b>';
		$Part5_Remark = '';

		$Part5_HeaderRemark = '';		
		
		$titleArray = array();	
		$titleArray[] = '學年<br/>School Year';
		$titleArray[] = '獎項及成就<br/>Awards and Achievements';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="20%" />';
		$colWidthArr[] = '<col width="80%" />';

		$DataArr = array();
		$DataArr = $this->getAward_Info();
		
		$StringDisplayArr ['Title']= $Part5_Title;
		$StringDisplayArr ['Remark']=  $Part5_Remark;
		
		$html = ''; 
		
		$html .= $this->getPartContentDisplay_NoRowLimit('5', $Part5_Title,$titleArray,$colWidthArr,$DataArr,$hasPageBreak=false,$StringDisplayArr,$Part5_HeaderRemark);
		
		return $html;
		
	}
	
	private function getAward_Info(){
		
		global $ipf_cfg;
		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		
		$_UserID = $this->uid;
		
		if($_UserID!='')
		{
			$cond_studentID = "And UserID ='$_UserID'";
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And a.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$sql = "Select
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						AwardName
				From
						$Award_student as a
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = a.AcademicYearID
				
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence desc, 
						AwardName asc		
				limit 5
				";
		$roleData = $this->objDB->returnResultSet($sql);

		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();

			$thisDataArr['AcademicYearID'] =$roleData[$i]['YEAR_NAME'];
			$thisDataArr['AwardName'] =$roleData[$i]['AwardName'];
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	/***************** End Part 5 ******************/
	
	
	
	/***************** Part 6 ******************/
	
	public function getPart6_HTML(){
		
		$Part7_Title = "學生自述 Student's Personal Statements";
		$Part7_Remark = '';
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		
		$StringDisplayArr ['Title']= $Part7_Title;
		$StringDisplayArr ['Remark']=  $Part7_Remark;
		
		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$StringDisplayArr) ;
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$_UserID = $this->uid;
		
		if($_UserID!='')
		{
			$cond_studentID = "And UserID ='$_UserID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnResultSet($sql);
//hdebug_r($sql);		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			$thisDataArr['Details'] = strip_tags($thisDataArr['Details'],'<p><br>');
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$StringDisplayArr='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		$title_font_size = '16px';		
		$content_font_size = '13px';	
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:14px;" '; 


		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count<=0)
		{		
			$html =$this->getEmptyTable('2%');
			$html .= $this->getPart1_HTML();
			$html .= $this->getEmptyTable('1%');
			$html .= $this->getPart2_HTML();

			$html .= '<table cellpadding="0" style="" width="100%">
						<tr>
							<td height="800px" valign="top">
								<table width="90%" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>			
									<tr>
										<td style="vertical-align: top;font-size:'.$title_font_size.';'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'text-align:left;height:10px;" bgcolor="'.$this->TableTitleBgColor.'"><b>'.$StringDisplayArr['Title'].'</b></td>	
									</tr>
									<tr>
										<td style="vertical-align: top;font-size:'.$content_font_size.';'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'text-align:center;"><b>No record found.</b></td>	
									</tr>
								</table>
							</td>
						</tr>
					</table>';
			$html .= $this->getFooter_HTML($Page_break,$this->PageNo); 
			$this->PageNo++;
		}
		else if ($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];			
	
			$DetailArr = explode($this->SelfAccount_Break,$Detail);	
	
			for ($i=0,$i_MAX=count($DetailArr); $i<$i_MAX; $i++)
			{		
				if($i==$i_MAX-1)
				{
					$Page_break='';
				}
								
				$html .= $this->getEmptyTable('2%');
				$html .= $this->getPart1_HTML();
				$html .= $this->getEmptyTable('1%');
				$html .= $this->getPart2_HTML();

				$html .= '<table cellpadding="0" style="" width="100%">';
					$html .= '<tr>
								<td height="800px" valign="top">
									<table width="90%" height="650px" cellpadding="7" border="0px" align="center" '.$table_style.'>
										<tr>
											<td style="vertical-align: top;font-size:'.$title_font_size.';'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'text-align:left;height:10px;" bgcolor="'.$this->TableTitleBgColor.'"><b>'.$StringDisplayArr['Title'].'</b></td>	
										</tr>
										<tr>				 							
											<td style="vertical-align: top;font-size:'.$content_font_size.';'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'font-size:16px;"><font face="Times New Roman">'.$DetailArr[$i].'</font></td>						 														
										</tr>
									</table>
								</td>
							 </tr>';
				$html .='</table>';
				$html .=$this->getFooter_HTML($Page_break,$this->PageNo); 
				$this->PageNo++;
			}
		}
			
		return $html;
	}
	
	
	/***************** End Part 7 ******************/
	
	
	
	/***************** Footer ******************/
	public function getFooter_HTML($PageBreak='')
	{
		$PrincipalSignStr = "Signature of the Principal";
		
		$_borderBottom =  'border-bottom: 1px solid black; ';
		
		$html = '';
		
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$PageBreak.'">		
					<col width="33%" />
					<col width="34%" />
					<col width="33%" />

				 	<tr>
						<td>&nbsp;</td>
						<td class="ms_footer_score" align="center"><b>'.$PrincipalSignStr.' :</b></td>
						<td style="'.$_borderBottom.'">&nbsp;</td>
					</tr>

				  </table>';
		return $html;
		
	}
	/***************** END Footer ******************/
	
	
	
	
	private function getStringDisplay($String='',$class_html='',$Page_break='') 
	{	
		$html = '';
		$html .= '<table width="90%" cellpadding="7" border="0px" align="center" class="'.$class_html.'" style="" '.$Page_break.'>					
		   			 <tr>
					    <td><font style="">'.$String.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}

	
	
	
	private function getPartContentDisplay_PageRowLimit($PartNum, $subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false,$StringDisplayArr='',$HeaderRemark='')
	{	
		$Page_break='page-break-after:always;';
		$Page_break = ($hasPageBreak==false)? '':$Page_break;
		
		
		/****** Settings *****/		
		###		$RowPerPage : it indicate the number of rows perpage. If you want to set 2 row perpage, change it to 2;			
		###     $RowHeight  : the height of each row
		###	    $font_size  : the font size of data
		###     $footer_html: the signature footer
		
		if($PartNum=='3')
		{
			$RowPerPage = 7;
			$RowHeight = 60; 	 
			$title_font_size = '14px';		
			$content_font_size = '13px';						
		}				
		
		$footer_html = $this->getFooter_HTML($Page_break); //principal signature	
		
		/***** End Settings *********/
		
				
		$rowMax_count = count($DataArr);
		$RowHeight_style = 'height="'.$RowHeight.'px"'; 
		
		$count_col =count($colWidthArr);
			
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';		
		
		$colSpanStr = '';

		$text_alignStr ='center;';		
			
		$html = '';
		
		$HeaderInfo =$this->getEmptyTable('2%');
		$HeaderInfo .=$this->getPart1_HTML();
		$HeaderInfo .= $this->getEmptyTable('1%');
		$HeaderInfo .=$this->getPart2_HTML();

		$HeaderInfo .= '<table align="center" width="90%" height="800px" cellpadding="0" style="">
						<tr><td valign="top">';
		$HeaderInfo .= '<table class="tabletext" height="" width="100%" cellpadding="7" border="0px" align="center" style="border-collapse: collapse;">';
	
						foreach((array)$colWidthArr as $key=>$_colWidthInfo)
						{
							$HeaderInfo .=$_colWidthInfo;
						} 
		$HeaderInfo .= ' <tr><td colspan="'.$count_col.'" style="'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'font-size:'.$title_font_size.';" bgcolor="'.$this->TableTitleBgColor.'">'.$StringDisplayArr['Title'].'</td></tr>';	
		$HeaderInfo .= ' <tr><td colspan="'.$count_col.'" style="'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'font-size:'.$content_font_size.';" bgcolor="'.$this->TableTitleRemarkBgColor.'">'.$HeaderRemark.'</td></tr>';	
		$HeaderInfo .= '<tr '.$RowHeight_style.'>';
					$k=0;
					foreach((array)$subTitleArr as $key=>$_title)
					{
						if($k==0)
						{
							$HeaderInfo .='<td class="font_16px" style="text-align:'.$text_alignStr.';vertical-align:text-top;font-size:'.$title_font_size.';'.$_borderLeft.$_borderTop.$_borderRight.'">';
						}
						else
						{
							$HeaderInfo .='<td class="font_16px" style="text-align:'.$text_alignStr.';vertical-align:text-top;font-size:'.$title_font_size.';'.$_borderTop.$_borderRight.'">';
						}
						
						$HeaderInfo .='<b>'.$_title.'</b>';
						$HeaderInfo .='</td>';
						$k++;
					}					
		$HeaderInfo .= '</tr>';	
		
		
		$tableBottomInfo .= '<tr><td align="left" style="font-size:'.$content_font_size.';" colspan="'.count($colWidthArr).'">'.$StringDisplayArr['Remark'].'</td></tr>';	
		$tableBottomInfo .='</table>';
		$tableBottomInfo .='</td></tr></table>';
		$tableBottomInfo .=$footer_html;

		if($rowMax_count<=0)
		{			
			$content_html ='<tr>';	
			
			for ($i=0; $i<$count_col; $i++)
			{	
				$content_html .='<td style="'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'font-size:'.$content_font_size.';text-align:'.$text_alignStr.'">'.$this->EmptySymbol.'</td>';
			}
			$content_html .='</tr>';	
			
			$html .=$HeaderInfo;
			$html .=$content_html;
			$html .=$tableBottomInfo;
		}

		else if($rowMax_count>0)
		{ 
			### 15 rows in one page	
			$NumOfDisplayedRow = 0;
			$SetHeader=false;
			
			
			for ($i=0; $i<$rowMax_count; $i++)
			{		
				$NumOfDisplayedRow = $i;
				
				$content_html = '';
				$content_html .= '<tr '.$RowHeight_style.'>';
							
				$k=0;		 
				foreach((array)$DataArr[$i] as $_title=>$_titleVal)
				{				
					$text_Align= 'left';
					
					if($_titleVal=='')
					{
						$_titleVal = $this->EmptySymbol;
					}
				
					$content_html .= ' <td style="text-align:'.$text_Align.';vertical-align:top;font-size:'.$content_font_size.';'.$_borderTop.$_borderRight.$_borderLeft.$_borderBottom.'">'.$_titleVal.'</td>';			
											 	
					$k++;
				}	
				$content_html .= '</tr>';	

				 if($NumOfDisplayedRow==0)  // if it is first row, add the header
				{
					$header_html=$HeaderInfo;
					
					 if($rowMax_count<=1)  // if there is only one record
					{
						$bottom_html = $tableBottomInfo;
					}				
				}
				else
				{
					$header_html='';						
						
					if(($NumOfDisplayedRow+1)%$RowPerPage==0)   // if it is  10th or 20th or 30th ....row (last row), add </table> 
					{
						$bottom_html = $tableBottomInfo;
						$SetHeader=true;
					}
					else if($SetHeader==true) // if it is 11th or 21th or 31th....row (first row), add the header
					{
						$header_html=$HeaderInfo;
						$SetHeader=false;
						
					}
				
				}
				
				if($NumOfDisplayedRow >= $rowMax_count-1)  // if it is the last row , add </table> 
				{			
					$bottom_html = $tableBottomInfo;		
				}
				

				$html .= $header_html; 		
				$html .= $content_html; // it is the subject content			
				$html .= $bottom_html;
		
				$header_html='';
				$bottom_html='';

			}
		}
		return $html;
	}
	
	
	private function getPartContentDisplay_NoRowLimit($PartNum, $mainTitle,$subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false,$StringDisplayArr='',$HeaderRemark='') 
	{
				
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		$BgColor = $this->TableTitleBgColor;
		
		$Page_break='page-break-after:always;';
	
		if($hasPageBreak==false)
		{
			$Page_break='';
		}
		
		$table_style = 'style="font-size:12px;border-collapse: collapse;border:1px solid #000000;"'; 
		
		$subtitle_html = '';
		
		if($PartNum=='4')
		{		
			$total_Record=5;
			$rowHight = 40;					
					
		}
		else if ($PartNum=='5')
		{				
			$total_Record=5;
			$rowHight = 40;
		  
		}
		
		$title_font_size = '14px';		
		$content_font_size = '13px';	
		
		$text_alignStr='center';
		$subtitle_html= '';
		foreach((array)$subTitleArr as $key=>$_title)
		{	
			$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;font-size:'.$title_font_size.';'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'"><b>';
			$subtitle_html .=$_title;
			$subtitle_html .='</b></td>';
		}	
		
		
		$content_html='';	
		$rowMax_count = 0;
		$rowMax_count = count($DataArr);
		$count_col =count($colWidthArr);
	
		if($rowMax_count<=0)
		{			
			$content_html ='<tr>';	
			
			for ($i=0; $i<$count_col; $i++)
			{	
				$content_html .='<td style="'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'font-size:'.$content_font_size.';text-align:'.$text_alignStr.'">'.$this->EmptySymbol.'</td>';
			}
			$content_html .='</tr>';	

		}

		else if($rowMax_count>0)
		{ 				
	
			for ($i=0; $i<$rowMax_count; $i++)
			{		
				$content_html .= '<tr>';	
					
				$j=0;		 
				foreach((array)$DataArr[$i] as $_title=>$_titleVal)
				{
					if($_titleVal=='')
					{
						$_titleVal =  $this->EmptySymbol;
					}
					$text_alignStr = 'left';

					$content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;font-size:'.$content_font_size.';'.$_borderTop.$_borderRight.$_borderBottom.'">'.$_titleVal.'</td>';								
					$j++;
				}				
				$content_html .= '</tr>';		
			}
		}

		
		$colNumber = count($colWidthArr);
			
		/****************** Main Table ******************************/
		
		$html = '';


		$html .= '<table width="90%" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$html .=$_colWidthInfo;
					}
		$html .= ' <tr><td colspan="'.$count_col.'" style="'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'font-size:'.$title_font_size.';" bgcolor="'.$this->TableTitleBgColor.'">'.$StringDisplayArr['Title'].'</td></tr>';	
		
		if($PartNum!='5')
		{
			$html .= ' <tr><td colspan="'.$count_col.'" style="'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'font-size:'.$content_font_size.';" bgcolor="'.$this->TableTitleRemarkBgColor.'">'.$HeaderRemark.'</td></tr>';
		}		
			
		
		$html .= '<tr>'.$subtitle_html.'</tr>';
		
		$html .= $content_html;

		$html .='</table>';
		
		$html .= '<table width="90%" height="" cellpadding="7" border="0px" align="center" style="font-size:11px;border-collapse: collapse;">';
		
		$html .= '<tr><td align="left" style="font-size:'.$content_font_size.';" colspan="'.count($colWidthArr).'">'.$StringDisplayArr['Remark'].'</td></tr>';
		$html .='</table>';

			
		return $html;
	}
	
	
	public function getStudentAcademic_YearClassNameArr($IsTitle=false)
	{
		$StudentID = $this->uid;
		
		$academicYearIDArr = $this->academicYearIDArr;
		
		$SelectStr= 'h.AcademicYear as AcademicYear,
					 h.ClassName as ClassName';
		
		if($academicYearIDArr!='')
		{		
			$YearArr = array();
			for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
			{
				if($academicYearIDArr[$i]=='')
				{
					continue;
				}
				$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
				
				if($IsTitle)
				{
					$thisAcademicYear = substr($thisAcademicYear,0,4);
				}
				
				$YearArr[] = $thisAcademicYear;
			}
			
			$cond_academicYear = "And h.AcademicYearID In ('".implode("','",(array)$academicYearIDArr)."')";		
		}

		$Profile_his = $this->Get_IntranetDB_Table_Name('PROFILE_CLASS_HISTORY');
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$sql = 'select 
						'.$SelectStr.' 
				from 
						'.$Profile_his.' as h 
				where 
						h.UserID = "'.$StudentID.'"
						'.$cond_academicYear.'
				Order by
						h.AcademicYear asc';
						
		$roleData = $this->objDB->returnResultSet($sql);
		
	//hdebug_r($sql);
		return $roleData;
	}
	

	
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false)
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		$OLE_CATE = $this->Get_eClassDB_Table_Name('OLE_CATEGORY');
		
		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = " And SLPOrder is not null"; 
		}
	
		if($IntExt!='')
		{
			$cond_IntExt = " And ole_prog.IntExt='".$IntExt."'";
		}
		
		$OLEcomponentArr = $this->getOLEcomponentName();
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
	
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = "And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$selectStr = "ole_std.UserID as StudentID,
					  ole_prog.Title as Title,
					  ole_prog.IntExt as IntExt,
					  y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
					  ole_std.role as Role,
					  ole_prog.Organization as Organization,
					  ole_std.Achievement as Achievement,
					  ole_prog.ELE as OleComponent";
		
		if($IntExt=='INT')
		{
			$cond_limit = 'limit 21';
		}
		else if($IntExt=='EXT')
		{
			$selectStr = "
					  ole_prog.Title as Title,
					  y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
					  ole_prog.Organization as Organization,
					  ole_std.Achievement as Achievement";
					  
			$cond_limit = 'limit 5';
		}
		$cond_SLPOrder = ' AND SLPOrder is not null ';
		
		$order_by = ' SLPOrder asc ';
		
		$sql = "Select
						$selectStr
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID

				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
									
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
						$cond_SLPOrder
				order by 
						$order_by	
				$cond_limit
					
				";
				//y.Sequence desc,	
				//		ole_prog.Title asc	
		$roleData = $this->objDB->returnResultSet($sql);

		if($IntExt=='INT')
		{
			for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
			{
				$_StudentID = $roleData[$i]['StudentID'];
				$_IntExt = $roleData[$i]['IntExt'];
				$thisAcademicYear = $roleData[$i]['YEAR_NAME'];
				
				
				$thisDataArr = array();
				
				$thisOleComponentArr = explode(',',$roleData[$i]['OleComponent']);
		
				for($a=0,$a_MAX=count($thisOleComponentArr);$a<$a_MAX;$a++)
				{
					$thisOleComponentID = $thisOleComponentArr[$a];		
					$thisOleComponentArr[$a] = $OLEcomponentArr[$thisOleComponentID];
				}
				$thisOLEstr = implode(", ",(array)$thisOleComponentArr);

				
				$thisDataArr['Program'] =$roleData[$i]['Title'];
				$thisDataArr['SchoolYear'] =$thisAcademicYear;							
				$thisDataArr['Role'] =$roleData[$i]['Role'];
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];
				$thisDataArr['OLEcomponent'] =$thisOLEstr;
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
				
				$returnData[] = $thisDataArr;
			}
			
			return $returnData;
		}
		else if($IntExt=='EXT')
		{
			return $roleData;
		}

		
	}
	
	public function getOLEcomponentName()
	{
		$OLE_ELE = $this->Get_eClassDB_Table_Name('OLE_ELE');
		
		$sql="Select 
					EngTitle,
					ChiTitle,
					DefaultID 
			  from
					$OLE_ELE
			";
		$roleData = $this->objDB->returnResultSet($sql);
		
		$returnArray = array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$thisName_en = $roleData[$i]['EngTitle'];
			$thisName_ch = $roleData[$i]['ChiTitle'];
			$thisID = $roleData[$i]['DefaultID'];
			
			$returnArray[$thisID] = $thisName_ch;
		}
	
		return $returnArray;
	}
	
	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		
		if(count($returnArray)==1)
		{
			return $returnArray[0];
		}
		else
		{
			$lastNo = count($returnArray)-1;
			
			$firstYear = substr($returnArray[0], 0, 4);
			$lastYear = substr($returnArray[$lastNo],-4);
			
			$returnValue = $firstYear."-".$lastYear;
			
			return $returnValue;	
		}
		
		
	}
	
	
	private function getAcademicYearArr($academicYearIDArr,$IsTrim=false)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			
			if($IsTrim)
			{
				$thisAcademicYear = substr($thisAcademicYear,0,4);
			}
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	
	public function getEmptyTable($Height='') {
	
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td>&nbsp;</td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>