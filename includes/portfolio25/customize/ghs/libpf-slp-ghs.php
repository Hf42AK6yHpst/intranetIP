<?php
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $academcYearDisplay; // store the academic Year display in the student Info
	private $PrincipalName;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $ClassGroupCategoryName;	// store the Category Name for the Group of Small Classes (1A1, 1A2)
	private $FontSizeArr;		// store the font size info for different field
	private $OrderInfoArr;		// store ordering fields of different section

	public function studentInfo($uid){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid;
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = 'N.A.';
		$this->TableTitleBgColor = '#DDDDDD';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->FontSizeArr['ReportHeader'] = 14;
		$this->FontSizeArr['SectionTitle'] = 12;
		$this->FontSizeArr['TableTitle'] = 11;
		$this->FontSizeArr['TableContent'] = 10;
		$this->FontSizeArr['Signature'] = 10;
		
		$this->OrderInfoArr['GroupCategory'] = array();
		$this->OrderInfoArr['GroupCategory'][] = 'SERVICE';
		$this->OrderInfoArr['GroupCategory'][] = 'HOUSE';
		$this->OrderInfoArr['GroupCategory'][] = 'TEAM';
		$this->OrderInfoArr['GroupCategory'][] = 'CLUB';
		$this->OrderInfoArr['GroupCategory'][] = 'OTHERS';
		
		$this->OrderInfoArr['ELECode'] = array();
		$this->OrderInfoArr['ELECode'][] = '[AD]';		// Aesthetic Development
		$this->OrderInfoArr['ELECode'][] = '[CE]';		// Career - related Experiences
		$this->OrderInfoArr['ELECode'][] = '[CS]';		// Community Service
		$this->OrderInfoArr['ELECode'][] = '[MCE]';		// Moral and Civic Education
		$this->OrderInfoArr['ELECode'][] = '[PD]';		// Physical Development
		$this->OrderInfoArr['ELECode'][] = '[8]';		// Others (Cultural Experience)
		$this->OrderInfoArr['ELECode'][] = '[9]';		// Others (Intellectual Development)
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearDisplay($academcYearDisplay){
		$this->academcYearDisplay = $academcYearDisplay;
	}
	public function getAcademicYearDisplay(){
		return $this->academcYearDisplay;
	}
	public function setPrincipalName($Name) {
		$this->PrincipalName = $Name;
	}
	public function getPrincipalName(){
		return $this->PrincipalName;
	}
	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	
	
	/*************************/
	/*******STUDENT INFO******/
	/*************************/
	public function getReportHeader_HTML() {
		return $this->getReportTitleDisplay().'<br />'.$this->getStudentInfo_HTML();
	}
	
	private function getReportTitleDisplay() {
		$header_font_size = $this->getFontSize('ReportHeader');
		
		$x = '';
		$x .= '<table style="width:100%; text-align:center; border:0px;">'."\r\n";
			$x .= '<tr><td style="font-size:'.$header_font_size.'"><b>STUDENT LEARNING PROFILE</b></td></tr>'."\r\n";
			$x .= '<tr><td style="font-size:'.$header_font_size.'"><b>'.$this->getAcademicYearDisplay().'</b></td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function getStudentInfo_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->intranet_db.".INTRANET_USER";
		$sql = "Select
						NickName as EnglishName,
						ChineseName,
						ClassName,
						ClassNumber,
						REPLACE(WebSAMSRegNo, '#', '') as StudentNo,
						HKID
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";
		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result){
		
//		debug_r($this->printIssueDate);
		$html = '';
		$html .= "<table width=\"100%\" cellpadding=\"2\" border=\"0px\">";
			$html .= "<tr>
							<td width=\"16%\"><b>NAME: </b></td>
							<td width=\"25%\"><b>".$result[0]["EnglishName"]."&nbsp;</b></td>
							<td width=\"15%\"><b>&nbsp;</b></td>
							<td width=\"44%\" style=\"font-size:12;\"><b>".$result[0]["ChineseName"]."&nbsp;</b></td>
					</tr>";
			$html .= "<tr>
							<td><b>CLASS: </b></td>
							<td><b>".$result[0]["ClassName"]."&nbsp;</b></td>
							<td><b>CLASS NO.: </b></td>
							<td><b>".$result[0]["ClassNumber"]."&nbsp;</b></td>
					</tr>";
			$html .= "<tr>
							<td><b>STUDENT NO.: </b></td>
							<td><b>".$result[0]["StudentNo"]."&nbsp;</b></td>
							<td><b>H.K.I.D. NO.: </b></td>
							<td><b>".$result[0]["HKID"]."&nbsp;</b></td>
					</tr>";
			$html .= "<tr>
							<td><b>DATE OF ISSUE: </b></td>
							<td><b>".$this->getPrintIssueDate()."&nbsp;</b></td>
							<td><b>&nbsp;</b></td>
							<td><b>&nbsp;</b></td>
					</tr>";
		$html .= "</table>";
		
		return $html;	
	}
	
	public function getPartC_HTML(){
		return $this->getPartC_Display();
	}
	private function getPartC_Display(){
		$ColumnInfoArr = array();
		$ColumnInfoArr[] = array('Title' => 'Awards and Scholarships', 'DBField' => 'AwardName', 'Width' => '100%');
		
		return $this->getSectionDisplay('C', $ColumnInfoArr, $this->getPartC_Data());
	}
	private function getPartC_Data(){
		$libpf_sturec = new libpf_sturec();
		return $libpf_sturec->Get_Student_Award_Record($this->getUid(), $this->getAcademicYear());
	}
	
	
	public function getPartD_HTML(){
		return $this->getPartD_Display();
	}
	private function getPartD_Display(){
		$ColumnInfoArr = array();
		$ColumnInfoArr[] = array('Title' => 'Group', 'DBField' => 'GroupName', 'Width' => '34%');
		$ColumnInfoArr[] = array('Title' => 'Role of Participation', 'DBField' => 'RoleName', 'Width' => '33%');
		$ColumnInfoArr[] = array('Title' => 'Performance', 'DBField' => 'Performance', 'Width' => '33%');
		
		return $this->getSectionDisplay('D', $ColumnInfoArr, $this->getPartD_Data());
	}
	private function getPartD_Data(){
		$INTRANET_GROUP = $this->intranet_db.'.INTRANET_GROUP';
		$INTRANET_USERGROUP = $this->intranet_db.'.INTRANET_USERGROUP';
		$INTRANET_ENROL_GROUPINFO = $this->intranet_db.'.INTRANET_ENROL_GROUPINFO';
		$INTRANET_ENROL_CATEGORY = $this->intranet_db.'.INTRANET_ENROL_CATEGORY';
		$INTRANET_ROLE = $this->intranet_db.'.INTRANET_ROLE';
		
		$CategoryDisplayOrderField = $this->getSqlDisplayOrderField('GroupCategory', 'iec.CategoryName');
		$sql = "Select
						ig.Title as GroupName,
						ir.Title as RoleName,
						iug.Performance,
						$CategoryDisplayOrderField as CategoryDisplayOrder
				From
						$INTRANET_USERGROUP as iug
						Inner Join
						$INTRANET_GROUP as ig On (iug.GroupID = ig.GroupID)
						Inner Join
						$INTRANET_ENROL_GROUPINFO as iegi On (iug.EnrolGroupID = iegi.EnrolGroupID)
						Inner Join
						$INTRANET_ENROL_CATEGORY as iec On (iegi.GroupCategory = iec.CategoryID)
						Inner Join
						$INTRANET_ROLE as ir On (iug.RoleID = ir.RoleID)
				Where
						ig.AcademicYearID In ('".implode("','", (array)$this->getAcademicYear())."')
						And iug.UserID = '".$this->getUid()."'
				Order By
						CategoryDisplayOrder, ig.Title
				";
		return $this->objDB->returnArray($sql);
	}
	
	public function getPartE_HTML(){
		return $this->getPartE_Display();
	}
	private function getPartE_Display(){
		$ColumnInfoArr = array();
		$ColumnInfoArr[] = array('Title' => 'Programme', 'DBField' => 'ProgramName', 'Width' => '24%');
		$ColumnInfoArr[] = array('Title' => 'Organization', 'DBField' => 'Organization', 'Width' => '22%');
		$ColumnInfoArr[] = array('Title' => 'Major Component(s) of OLE', 'DBField' => 'ELEName', 'Width' => '18%');
		$ColumnInfoArr[] = array('Title' => 'Role of Participation', 'DBField' => 'Role', 'Width' => '15%');
		$ColumnInfoArr[] = array('Title' => 'Awards / Certificate / Achievements<br />(if any)', 'DBField' => 'Achievement', 'Width' => '21%');
		
		return $this->getSectionDisplay('E', $ColumnInfoArr, $this->getPartE_Data());
	}
	private function getPartE_Data(){
		$libpf_ole = new libpf_ole();
		$ELEInfoAssoArr = BuildMultiKeyAssoc($libpf_ole->Get_ELE_Info(), 'ELECode');
		
		$ELEDisplayOrderField = $this->getSqlDisplayOrderField('ELECode', 'op.ELE');
		
		$OLE_STUDENT = $this->eclass_db.'.OLE_STUDENT';
		$OLE_PROGRAM = $this->eclass_db.'.OLE_PROGRAM';
		$sql = "Select
						op.Title as ProgramName,
						op.Organization,
						os.Role,
						os.Achievement,
						op.ELE,
						$ELEDisplayOrderField as ELE_DisplayOrder
				From
						$OLE_STUDENT as os
						Inner Join $OLE_PROGRAM as op On (os.ProgramID = op.ProgramID)
				Where
						op.AcademicYearID In ('".implode("','", (array)$this->getAcademicYear())."')
						And os.UserID = '".$this->getUid()."'
				Order By
						ELE_DisplayOrder, op.Title
				";
		$DataArr = $this->objDB->returnArray($sql);
		$numOfData = count($DataArr);
		
		### Add the ELE Name into the Array
		for ($i=0; $i<$numOfData; $i++) {
			$thisELE = $DataArr[$i]['ELE'];
			$DataArr[$i]['ELEName'] = $ELEInfoAssoArr[$thisELE]['EngTitle'];
		}
		
		return $DataArr;
	}
	
	private function getPartTitleDisplay($part) {
		$part = strtoupper($part);
		$section_font_size = $this->getFontSize('SectionTitle');
		
		switch ($part) {
			case 'C':
				$title = 'Awards / Scholarships';
				break;
			case 'D':
				$title = 'Posts';
				break;
			case 'E':
				$title = 'Other Learning Experiences (Organized or Endorsed by School)';
				break;
			default:
				$title = '';
		}
		
		return '<div style="width:100%; font-size:'.$section_font_size.'; text-align:left;"><b>'.$part.') '.$title.'</b></div>';
	}
	
	private function getSectionDisplay($SectionCode, $ColumnInfoArr, $DataArr) {
		### Get Table Style Info
		$title_font_size = $this->getFontSize('TableTitle');
		$content_font_size = $this->getFontSize('TableContent');
		$table_title_bg_color = $this->getTableTitleBackgroundColor();
		
		$numOfColumn = count($ColumnInfoArr);
		$numOfRow = count($DataArr);
		
		$html = '';
		$html .= $this->getPartTitleDisplay($SectionCode)."\r\n";
		$html .= '<table cellpadding="3" style="width:100%; border:1px;">'."\r\n";
			$html .= '<thead>'."\r\n";
				$html .= '<tr>'."\r\n";
					for ($i=0; $i<$numOfColumn; $i++) {
						$thisWidth = $ColumnInfoArr[$i]['Width'];
						$thisTitle = $ColumnInfoArr[$i]['Title'];
						$html .= '<th valign="middle" style="background-color:'.$table_title_bg_color.'; font-size:'.$title_font_size.'; width:'.$thisWidth.'; text-align:center; vertical-align:middle;"><b>'.$thisTitle.'</b></th>'."\r\n";
					}
				$html .= '</tr>'."\r\n";
			$html .= '</thead>'."\r\n";
			
			$html .= '<tbody>'."\r\n";
			if ($numOfRow == 0) {
				$html .= '<tr><td colspan="'.$numOfColumn.'" style="font-size:'.$content_font_size.'; text-align:center;">'.$this->getEmptySymbol().'</td></tr>'."\r\n";
			}
			else {
				for ($i=0; $i<$numOfRow; $i++) {
					$html .= '<tr>'."\r\n";
						for ($j=0; $j<$numOfColumn; $j++) {
							$thisWidth = $ColumnInfoArr[$j]['Width'];
							$thisDBField = $ColumnInfoArr[$j]['DBField'];
							$thisData = $DataArr[$i][$thisDBField];
							$thisData = ($thisData=='')? $this->getEmptySymbol() : $thisData;
							
							$html .= '<td style="font-size:'.$content_font_size.'; text-align:left; width:'.$thisWidth.';">'.$thisData.'</td>'."\r\n";
						}
					$html .= '</tr>'."\r\n";
				}
			}
			$html .= '</tbody>'."\r\n";
		$html .= '</table>'."\r\n";
		
		return $html;
	}
	
	public function getReportSignature_HTML() {
		$ClassTeacherName = $this->getStudentClassTeacherInfo();
		$PrincipalName = $this->getPrincipalName();
		
		$font_size = $this->getFontSize('Signature');
		$signature_line = '_________________________________________________';
				
		$html = '';
		$html .= '<br />'."\r\n";
		$html .= '<br />'."\r\n";
		$html .= '<br />'."\r\n";
		$html .= '<br />'."\r\n";
		$html .= '<br />'."\r\n";
		$html .= '<br />'."\r\n";
		$html .= '<br />'."\r\n";
		$html .= '<table style="width:100%; border:0px;">'."\r\n";
			$html .= '<tr>'."\r\n";
				$html .= '<td style="width:33%; text-align:center; border-bottom: 1px solid #000000;"><b>'.$signature_line.'</b></td>'."\r\n";
				$html .= '<td style="width:34%;">&nbsp;</td>'."\r\n";
				$html .= '<td style="width:33%; text-align:center; border-bottom: 1px solid #000000;"><b>'.$signature_line.'</b></td>'."\r\n";
			$html .= '</tr>'."\r\n";
			$html .= '<tr>'."\r\n";
				$html .= '<td style="text-align:center; font-size:'.$font_size.';"><b>'."\r\n";
					$html .= 'CLASS TEACHER'."\r\n";
					$html .= '<br />'."\r\n";
					$html .= $ClassTeacherName."\r\n";
				$html .= '</b></td>'."\r\n";
				$html .= '<td>&nbsp;</td>'."\r\n";
				$html .= '<td style="text-align:center; font-size:'.$font_size.';"><b>'."\r\n";
					$html .= 'ACTING PRINCIPAL'."\r\n";
					$html .= '<br />'."\r\n";
					$html .= $PrincipalName."\r\n";
				$html .= '</b></td>'."\r\n";
			$html .= '</tr>'."\r\n";
		$html .= '</table>'."\r\n";
		
		return $html;
	}
	
	private function getSqlDisplayOrderField($FieldType, $DBFieldName) {
		$DataArr = $this->getOrderInfoArr($FieldType);
		$numOfData = count($DataArr);
		
		$field = '';
		$field .= 'CASE '.$DBFieldName;
			for ($i=0; $i<$numOfData; $i++) {
				$thisData = $DataArr[$i];
				$field .= " WHEN '".$thisData."' THEN ".$i." ";
			}
		$field .= 'ELSE 999 END';
		
		return $field;
	}
	
	private function getOrderInfoArr($FieldType) {
		return $this->OrderInfoArr[$FieldType];
	}
	
	private function getEmptySymbol() {
		return $this->EmptySymbol;
	}
	
	private function getFontSize($type) {
		return $this->FontSizeArr[$type];
	}
	
	private function getTableTitleBackgroundColor() {
		return $this->TableTitleBgColor;
	}
	
	private function getClassGroupCategoryName() {
		return $this->ClassGroupCategoryName;
	}
	
	private function getStudentClassTeacherInfo() {
		$INTRANET_USERGROUP = $this->intranet_db.'.INTRANET_USERGROUP';
		$INTRANET_GROUP = $this->intranet_db.'.INTRANET_GROUP';
		$INTRANET_GROUP_CATEGORY = $this->intranet_db.'.INTRANET_GROUP_CATEGORY';
		$INTRANET_USER = $this->intranet_db.'.INTRANET_USER';
		
		### Get the Student Small Class Group
		$sql = "Select
						ig.GroupID
				From
						$INTRANET_USERGROUP as iug
						Inner Join $INTRANET_GROUP as ig On (iug.GroupID = ig.GroupID)
						Inner Join $INTRANET_GROUP_CATEGORY as igc On (ig.RecordType = igc.GroupCategoryID)
				Where
						iug.UserID = '".$this->getUid()."'
						And ig.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
						And igc.CategoryName = '".$this->objDB->Get_Safe_Sql_Query($this->getClassGroupCategoryName())."'
				";
		$ClassGroupInfoArr = $this->objDB->returnArray($sql);
		$ClassGroupID = $ClassGroupInfoArr[0]['GroupID'];
		
		### Get the Group Admin (i.e. the Class Teacher) and their NickName
		$sql = "Select
						iu.UserID,
						iu.NickName
				From
						$INTRANET_USERGROUP as iug
						Inner Join $INTRANET_USER as iu On (iug.UserID = iu.UserID)
				Where
						iug.GroupID = '".$ClassGroupID."'
						And iug.RecordType = 'A'
				";
		$ClassTeacherInfoArr = $this->objDB->returnArray($sql);
		$ClassTeacherNickNameArr = Get_Array_By_Key($ClassTeacherInfoArr, 'NickName');
		
		return implode(', ', $ClassTeacherNickNameArr);
	}
}
?>