<?php
/*
 *  Using:
 *
 * 	Note: please use utf-8 to edit!
 *
 *  2020-07-13 Cameron
 *      - don't apply filter to following functions so that it can show the actual number of good points and number of target met in the scope even that scope does not match
 *          the medal criteria but others match [case #J189433]
 * 		setGoodPoint_BSQ(),
		setGoodPoint_OFS(),
		setGoodPoint_BTP(),
		setTargetMet_BSQ(),
		setTargetMet_EPW(),
		setTargetMet_BTP()
        - add parameter $nrScopeMet to getStudentWithMedal()

 *  2019-12-12 Cameron
 *      - add case EPW-PW, EPW-OC, BTP-IS, BTP-TP to getMedalStatByItem() and getNumberOfStudentByPerformance()
 *      - set parameter $semester be empty by default in getPerformanceSqlGoodPassFail()
 *
 *  2019-12-10 Cameron
 *      - retrieve TargetMetScopeNumber in getStudentWithMedal()
 *
 *  2019-12-04 Cameron
 *      - modify getGoodPointsByAward() for new mapping [case #J161484]
 *      - add IsMet_* to tmpTableName in create_temp_award_table()
 *      - add function setNrScopeMet() and apply it in getStudentWithMedal()
 *      - add parameter $medal to getStudentWithMedal() and revise the filter for attaining medal
 *
 *  2019-12-02 Cameron
 *      - add case 'EPW-PW', 'EPW-OC', 'BTP-TP', 'BTP-IS' in getPresetHeader() [case #J161484]
 *
 *  2019-07-05 Cameron
 *      - change RecordID to RecordRow in getStudentWithMedal() to avoid casting it to Integer in lib.php [case #J164347]
 *      
 *  2018-05-03 Cameron
 *      - change to "order by Sequence" in getNumOfStudentsByClassLevel()
 *      
 *  2018-04-16 Cameron
 *      - retrieve numMedalAll, numMedalNone, percentMedalAll and percentMedalNone in getMedalStatByClassLevel()
 *      - add function getMajorClassLevel()
 *      	
 * 	2017-09-13 Cameron
 * 		- retrieve OrganizationNameEng & OrganizationNameChi in getJinYingData()
 * 
 * 	2017-08-28 Cameron
 * 		- retrieve WebSAMS (without leading #) in getJinYingData()
 * 
 * 	2017-04-28 Cameron
 * 		- add function getRecordID() and getPresetHeader()
 * 
 * 	2017-04-26 Cameron
 * 		- add function getJinYingData() for export
 * 
 * 	2017-02-15 Cameron
 * 		- add functions for statistics: getNumOfStudentsByClassLevel(), getNumOfStudentsGotMedalByClassLevel(), getMedalStatByClassLevel(),
 * 			getNumOfStudentsByClass(), getNumOfStudentsGotMedalByClass(), getMedalStatByClass()
 * 
 * 	2017-01-13 Cameron
 * 		- change base factor: 1 Minor = 3 Merit, 1 Major = 3 Minor 
 * 		- change ClassLevel to comma separated string
 * 
 * 	2017-01-09 Cameron
 * 		- add StatisticalAnalysis tab in getJinYingTab() 
 * 
 * 	2016-12-14 Cameron
 * 		- create this file
 */
 
include_once($intranet_root."/includes/portfolio25/customize/tmchkwc/config.php");

class libJinYing{
//-- Start of Class

	private $objDB;
	private $eclass_db;
	private $intranet_db;
	private $tmpTableName;

	private $AcademicYearID;
	private $ClassLevel;		// comma separated string
	private $Awarded;
	private $NotAwarded;
	
	public function getGoodPointsByAward($recommendMerit) {
		$nbr = 0;
		if ($recommendMerit) {
            switch($recommendMerit) {
                case 'Merit_1':
                    $nbr = 1;
                    break;
                case 'Merit_2':
                    $nbr = 2;
                    break;
                case 'MinorM_1':        // 1 Minor = 3 Merit
                    $nbr = 3;
                    break;
                case 'MinorM_1_Merit_1':
                    $nbr = 4;
                    break;
                case 'MinorM_1_Merit_2':
                    $nbr = 5;
                    break;
                case 'MinorM_2':
                    $nbr = 6;
                    break;
                case 'MinorM_2_Merit_1':
                    $nbr = 7;
                    break;
                case 'MinorM_2_Merit_2':
                    $nbr = 8;
                    break;
                case 'MajorM_1':        // 1 Major = 3 Minor
                    $nbr = 9;
                    break;
            }
		}
		return $nbr;
	}
	
	// return number of good points (every 10 service hours reward 1 good point, max 5)
	public function getGoodPointsByHours($serviceHours) {
		global $conf;
		
		$maxGP = $conf['MaxServiceGP'] ? $conf['MaxServiceGP'] : 5;		// max number of good points
		$gpEXR = $conf['GPExRate'] ? $conf['GPExRate'] : 10;			// good point exchange rate
		if (is_numeric($serviceHours)) {
			$nbr = floor($serviceHours / $gpEXR);
			if ($nbr > $maxGP) {
				$nbr = $maxGP;
			}
		}
		else {
			$nbr = 0;
		}
		return $nbr;
	}
	
	public function setAcademicYearID($AcademicYearID) {
	  	$this->AcademicYearID = $AcademicYearID;
	}
	public function setClassLevel($ClassLevel) {
		$this->ClassLevel = $ClassLevel;
	}
	public function setAwarded($Awarded) {
		$this->Awarded = $Awarded;
	}
	public function setNotAwarded($NotAwarded) {
		$this->NotAwarded = $NotAwarded;
	}
	
	public function getAcademicYearID() {
		return $this->AcademicYearID;
	}
	public function getClassLevel() {
		return $this->ClassLevel;
	}
	public function getAwarded() {
		return $this->Awarded;
	}
	public function getNotAwarded() {
		return $this->NotAwarded;
	}
 
	
	public function __construct(){
		global $eclass_db,$intranet_db;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		$this->tmpTableName = "TMP_PORTFOLIO_JINYING_AWARD_".$_SESSION['UserID'];
	}

	public function getJinYingTab($CurrentTag)
	{
		global $Lang;
		$TAGS_OBJ = array();
		$TagNow[$CurrentTag] = true;
		$TAGS_OBJ[] = array($Lang['iPortfolio']['JinYing']['JinYingScheme'],"index.php",$TagNow['JinYing']);
		$TAGS_OBJ[] = array($Lang['iPortfolio']['JinYing']['Medal']['Medal'],"medal.php",$TagNow['Medal']);
		$TAGS_OBJ[] = array($Lang['iPortfolio']['JinYing']['StatisticalAnalysis'],"statistics.php",$TagNow['Statistics']);
		return $TAGS_OBJ;
	}		

	public function getImportPageNavigation()
	{
		global $Lang;
		$PAGE_NAVIGATION = array();
		$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['JinYing']['JinYingScheme'],"index.php");
		$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'],"");
		return $PAGE_NAVIGATION;
	}		

	public function getDeletePageNavigation()
	{
		global $Lang;
		$PAGE_NAVIGATION = array();
		$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['JinYing']['JinYingScheme'],"index.php");
		$PAGE_NAVIGATION[] = array($Lang['Btn']['Delete'],"");
		return $PAGE_NAVIGATION;
	}		
	
	// 2nd term is not applicable to Form 6 students
	public function getLastAcademicYearTermID() 
	{
	    if ($this->AcademicYearID) {
	        $sql = "SELECT 	YearTermID
					FROM	".$this->intranet_db.".ACADEMIC_YEAR_TERM
					WHERE	AcademicYearID='".$this->AcademicYearID."'
					ORDER BY TermEnd DESC";
	        $rs = $this->objDB->returnVector($sql);
	        if (count($rs)>1) {	// at least two terms
	            return $rs[0];
	        }
	    }
	    return '';
	}
	
	public function getSemesterName($yearTermID)
	{
	    $termTitle = Get_Lang_Selection("YearTermNameB5","YearTermNameEN");
	    $sql = "SELECT $termTitle AS TermTitle FROM ".$this->intranet_db.".ACADEMIC_YEAR_TERM WHERE YearTermID='$yearTermID'";
	    $rs = $this->objDB->returnResultSet($sql);
	    return count($rs) ? $rs[0]['TermTitle'] : '';
	}
	
	// check if the pass-in yearTermID is the last term in the academic year
	public function isLastTerm($yearTermID) 
	{
	    if ($yearTermID && ($yearTermID == $this->getLastAcademicYearTermID())) {
	        return true;
	    }
	    else {
	        return false;
	    }
	}
	
	public function create_temp_award_table() {
		$sql = "CREATE TABLE IF NOT EXISTS ".$this->tmpTableName." (
					 UserID int(11) default 0 NOT NULL,
					 AcademicYearID int(8) default 0 NOT NULL,
					 GoodPoint_BSQ int(8) default 0 NOT NULL,
					 GoodPoint_OFS int(8) default 0 NOT NULL,
					 GoodPoint_BTP int(8) default 0 NOT NULL,
					 TargetMet_BSQ int(8) default 0 NOT NULL,
					 TargetMet_EPW int(8) default 0 NOT NULL,
					 TargetMet_BTP int(8) default 0 NOT NULL,
					 IsMet_BSQ tinyint default 0 NOT NULL,
					 IsMet_EPW tinyint default 0 NOT NULL,
					 IsMet_OFS tinyint default 0 NOT NULL,
					 IsMet_BTP tinyint default 0 NOT NULL,
					 INDEX IdxUser(UserID),
					 INDEX IdxAcademicYearID(AcademicYearID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		$result = $this->objDB->db_db_query($sql);
		return $result;			
	}
	
	public function drop_temp_award_table() {
		$sql = "DROP TABLE IF EXISTS ".$this->tmpTableName;
		$result = $this->objDB->db_db_query($sql);
		return $result;
	}

	public function setGoodPoint_BSQ($num=0) {
		global $eclass_db,$conf;
		
		if ($this->AcademicYearID && $this->ClassLevel) {
			$SQL = "SELECT 	p.UserID, COUNT(p.PerformanceCode) AS Num
					FROM 	{$eclass_db}.PORTFOLIO_JINYING_SCHEME p
					INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=p.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						AND yc.AcademicYearID='".$this->AcademicYearID."'
					LEFT JOIN ".$this->tmpTableName." t ON t.UserID=p.UserID 
						AND t.AcademicYearID=p.AcademicYearID
					WHERE 
						p.AcademicYearID='".$this->AcademicYearID."'
					AND yc.YearID IN (".$this->ClassLevel.")
					AND p.PerformanceCode IN ('".implode("','",$conf['GoodPoint']['BSQ'])."')";
					
//			$groupBy = " GROUP BY p.UserID HAVING COUNT(p.PerformanceCode)>='".$num."' ORDER BY p.UserID";
            $groupBy = " GROUP BY p.UserID ORDER BY p.UserID";

			## insert
			$sql = $SQL . " AND t.UserID IS NULL".$groupBy;
			$rs = $this->objDB->returnResultSet($sql);

			$insertAry = array();
			foreach((array)$rs as $r) {
				$insertAry[] = " ('". $r['UserID']."', '".
									  $this->AcademicYearID."', '".
									  $r['Num']."')";
			}
			if (count($insertAry)) {
				$sql = "INSERT INTO ".$this->tmpTableName." (UserID, AcademicYearID, GoodPoint_BSQ) VALUES ".implode(', ', $insertAry);
				$successAry[] = $this->objDB->db_db_query($sql);
			}

			## update
			$sql = $SQL . " AND t.UserID IS NOT NULL".$groupBy;
			$rs = $this->objDB->returnResultSet($sql);
			if (count($rs)) {
				foreach((array)$rs as $r) {
					$sql = "UPDATE ".$this->tmpTableName." SET GoodPoint_BSQ='".$r['Num']."'
							WHERE UserID='".$r['UserID']."' 
							AND	AcademicYearID='".$this->AcademicYearID."'";
					$successAry[] = $this->objDB->db_db_query($sql);
				}
			}
			
		}
	} 

	public function setGoodPoint_OFS($num=0) {
		global $eclass_db;
		
		if ($this->AcademicYearID && $this->ClassLevel) {
			$gp_ofs = array();
			## service in school
			$sql = "SELECT 	p.UserID, p.RecommendMerit
					FROM 	{$eclass_db}.PORTFOLIO_JINYING_SCHEME p
					INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=p.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						AND yc.AcademicYearID='".$this->AcademicYearID."'
					WHERE 
						p.AcademicYearID='".$this->AcademicYearID."'
					AND yc.YearID IN (".$this->ClassLevel.")
					AND p.PerformanceCode='OFS-SI-P-01'
					ORDER BY p.UserID";
			
			$rs = $this->objDB->returnResultSet($sql);
			foreach((array)$rs as $r) {
				$gp_ofs[$r['UserID']] += $this->getGoodPointsByAward($r['RecommendMerit']);	
			}
			
			## service outside school
			$sql = "SELECT 	p.UserID, p.ServiceHours
					FROM 	{$eclass_db}.PORTFOLIO_JINYING_SCHEME p
					INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=p.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						AND yc.AcademicYearID='".$this->AcademicYearID."'
					WHERE 
						p.AcademicYearID='".$this->AcademicYearID."'
					AND yc.YearID IN (".$this->ClassLevel.")
					AND p.PerformanceCode='OFS-SO-P-01'
					ORDER BY p.UserID";
			
			$rs = $this->objDB->returnResultSet($sql);
			foreach((array)$rs as $r) {
				$gp_ofs[$r['UserID']] += $this->getGoodPointsByHours($r['ServiceHours']);	
			}
			
			$insertAry = array();
			foreach((array)$gp_ofs as $u=>$v) {
				//if ($v >= $num && $v > 0) {
                if ($v > 0) {
					$sql = "SELECT UserID FROM ".$this->tmpTableName." WHERE UserID='".$u."' AND AcademicYearID='".$this->AcademicYearID."'";
					$check_rs = $this->objDB->returnResultSet($sql);
					if (count($check_rs)) {
						$sql = "UPDATE ".$this->tmpTableName." SET GoodPoint_OFS='".$v."'
								WHERE UserID='".$u."' 
								AND	AcademicYearID='".$this->AcademicYearID."'";
						$successAry[] = $this->objDB->db_db_query($sql);
					}
					else {
						$insertAry[] = " ('". $u."', '".
											  $this->AcademicYearID."', '".
											  $v."')";
					}					
				}
			}
			if (count($insertAry)) {
				$sql = "INSERT INTO ".$this->tmpTableName." (UserID, AcademicYearID, GoodPoint_OFS) VALUES ".implode(', ', $insertAry);
				$successAry[] = $this->objDB->db_db_query($sql);
			}
		}
	} 

	public function setGoodPoint_BTP($num=0) {
		global $eclass_db;
		
		if ($this->AcademicYearID && $this->ClassLevel) {
			$gp_btp = array();
			## Participation in inter-school competitions
			$sql = "SELECT 	p.UserID, p.RecommendMerit
					FROM 	{$eclass_db}.PORTFOLIO_JINYING_SCHEME p
					INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=p.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						AND yc.AcademicYearID='".$this->AcademicYearID."'
					WHERE 
						p.AcademicYearID='".$this->AcademicYearID."'
					AND yc.YearID IN (".$this->ClassLevel.")
					AND p.PerformanceCode='BTP-IC-P-03'
					ORDER BY p.UserID";
			
			$rs = $this->objDB->returnResultSet($sql);
			
			foreach((array)$rs as $r) {
				$gp_btp[$r['UserID']] += $this->getGoodPointsByAward($r['RecommendMerit']);	
			}
			
			$insertAry = array();
			foreach((array)$gp_btp as $u=>$v) {
//				if ($v >= $num && $v > 0) {
                if ($v > 0) {
					$sql = "SELECT UserID FROM ".$this->tmpTableName." WHERE UserID='".$u."' AND AcademicYearID='".$this->AcademicYearID."'";
					$check_rs = $this->objDB->returnResultSet($sql);
					if (count($check_rs)) {
						$sql = "UPDATE ".$this->tmpTableName." SET GoodPoint_BTP='".$v."'
								WHERE UserID='".$u."' 
								AND	AcademicYearID='".$this->AcademicYearID."'";
						$successAry[] = $this->objDB->db_db_query($sql);
					}
					else {
						$insertAry[] = " ('". $u."', '".
											  $this->AcademicYearID."', '".
											  $v."')";
					}					
				}
			}
			if (count($insertAry)) {
				$sql = "INSERT INTO ".$this->tmpTableName." (UserID, AcademicYearID, GoodPoint_BTP) VALUES ".implode(', ', $insertAry);
				$successAry[] = $this->objDB->db_db_query($sql);
			}
		}
	} 
		
	public function setTargetMet_BSQ($num=0) {
		global $eclass_db,$conf;
		
		if ($this->AcademicYearID && $this->ClassLevel) {
			$SQL = "SELECT 	p.UserID, COUNT(p.PerformanceCode) AS Num
					FROM 	{$eclass_db}.PORTFOLIO_JINYING_SCHEME p
					INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=p.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						AND yc.AcademicYearID='".$this->AcademicYearID."'
					LEFT JOIN ".$this->tmpTableName." t ON t.UserID=p.UserID 
						AND t.AcademicYearID=p.AcademicYearID
					WHERE 
						p.AcademicYearID='".$this->AcademicYearID."'
					AND yc.YearID IN (".$this->ClassLevel.")
					AND p.PerformanceCode IN ('".implode("','",$conf['TargetMet']['BSQ'])."')";

//			$groupBy = " GROUP BY p.UserID HAVING COUNT(p.PerformanceCode)>='".$num."' ORDER BY p.UserID";
            $groupBy = " GROUP BY p.UserID ORDER BY p.UserID";
			
			## insert
			$sql = $SQL . " AND t.UserID IS NULL".$groupBy;
			$rs = $this->objDB->returnResultSet($sql);

			$insertAry = array();
			foreach((array)$rs as $r) {
				$insertAry[] = " ('". $r['UserID']."', '".
									  $this->AcademicYearID."', '".
									  $r['Num']."')";
			}
			if (count($insertAry)) {
				$sql = "INSERT INTO ".$this->tmpTableName." (UserID, AcademicYearID, TargetMet_BSQ) VALUES ".implode(', ', $insertAry);
				$successAry[] = $this->objDB->db_db_query($sql);
			}
						
			## update
			$sql = $SQL . " AND t.UserID IS NOT NULL".$groupBy;
			$rs = $this->objDB->returnResultSet($sql);
			if (count($rs)) {
				foreach((array)$rs as $r) {
					$sql = "UPDATE ".$this->tmpTableName." SET TargetMet_BSQ='".$r['Num']."'
							WHERE UserID='".$r['UserID']."' 
							AND	AcademicYearID='".$this->AcademicYearID."'";
					$successAry[] = $this->objDB->db_db_query($sql);
				}
			}
		}
	} 

	public function setTargetMet_EPW($num=0) {
		global $eclass_db,$conf;
		
		if ($this->AcademicYearID && $this->ClassLevel) {
			$SQL = "SELECT 	p.UserID, COUNT(p.PerformanceCode) AS Num
					FROM 	{$eclass_db}.PORTFOLIO_JINYING_SCHEME p
					INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=p.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						AND yc.AcademicYearID='".$this->AcademicYearID."'
					LEFT JOIN ".$this->tmpTableName." t ON t.UserID=p.UserID 
						AND t.AcademicYearID=p.AcademicYearID
					WHERE 
						p.AcademicYearID='".$this->AcademicYearID."'
					AND yc.YearID IN (".$this->ClassLevel.")
					AND p.PerformanceCode IN ('".implode("','",$conf['TargetMet']['EPW'])."')";

//			$groupBy = " GROUP BY p.UserID HAVING COUNT(p.PerformanceCode)>='".$num."' ORDER BY p.UserID";
            $groupBy = " GROUP BY p.UserID ORDER BY p.UserID";

			## insert
			$sql = $SQL . " AND t.UserID IS NULL".$groupBy;
			$rs = $this->objDB->returnResultSet($sql);
			$insertAry = array();
			foreach((array)$rs as $r) {
				$insertAry[] = " ('". $r['UserID']."', '".
									  $this->AcademicYearID."', '".
									  $r['Num']."')";
			}
			if (count($insertAry)) {
				$sql = "INSERT INTO ".$this->tmpTableName." (UserID, AcademicYearID, TargetMet_EPW) VALUES ".implode(', ', $insertAry);
				$successAry[] = $this->objDB->db_db_query($sql);
			}
						
			## update
			$sql = $SQL . " AND t.UserID IS NOT NULL".$groupBy;
			$rs = $this->objDB->returnResultSet($sql);
			if (count($rs)) {
				foreach((array)$rs as $r) {
					$sql = "UPDATE ".$this->tmpTableName." SET TargetMet_EPW='".$r['Num']."'
							WHERE UserID='".$r['UserID']."' 
							AND	AcademicYearID='".$this->AcademicYearID."'";
					$successAry[] = $this->objDB->db_db_query($sql);					
				}
			}
		}
	} 

	public function setTargetMet_BTP($num=0) {
		global $eclass_db,$conf;
		
		if ($this->AcademicYearID && $this->ClassLevel) {
			$SQL = "SELECT 	p.UserID, COUNT(p.PerformanceCode) AS Num
					FROM 	{$eclass_db}.PORTFOLIO_JINYING_SCHEME p
					INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=p.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						AND yc.AcademicYearID='".$this->AcademicYearID."'
					LEFT JOIN ".$this->tmpTableName." t ON t.UserID=p.UserID 
						AND t.AcademicYearID=p.AcademicYearID
					WHERE 
						p.AcademicYearID='".$this->AcademicYearID."'
					AND yc.YearID IN (".$this->ClassLevel.")
					AND p.PerformanceCode IN ('".implode("','",$conf['TargetMet']['BTP'])."')";

//			$groupBy = " GROUP BY p.UserID HAVING COUNT(p.PerformanceCode)>='".$num."' ORDER BY p.UserID";
            $groupBy = " GROUP BY p.UserID ORDER BY p.UserID";

			## insert
			$sql = $SQL . " AND t.UserID IS NULL".$groupBy;
			$rs = $this->objDB->returnResultSet($sql);
			$insertAry = array();
			foreach((array)$rs as $r) {
				$insertAry[] = " ('". $r['UserID']."', '".
									  $this->AcademicYearID."', '".
									  $r['Num']."')";
			}
			if (count($insertAry)) {
				$sql = "INSERT INTO ".$this->tmpTableName." (UserID, AcademicYearID, TargetMet_BTP) VALUES ".implode(', ', $insertAry);
				$successAry[] = $this->objDB->db_db_query($sql);
			}
						
			## update
			$sql = $SQL . " AND t.UserID IS NOT NULL".$groupBy;
			$rs = $this->objDB->returnResultSet($sql);
			if (count($rs)) {
				foreach((array)$rs as $r) {
					$sql = "UPDATE ".$this->tmpTableName." SET TargetMet_BTP='".$r['Num']."'
							WHERE UserID='".$r['UserID']."' 
							AND	AcademicYearID='".$this->AcademicYearID."'";
					$successAry[] = $this->objDB->db_db_query($sql);					
				}
			}
		}
	} 

	// should call $this->setGoodPoint_* and $this->setTargetMet_* before call this
	public function setNrScopeMet($filter)
    {
        if ($this->AcademicYearID) {
            $sql = "UPDATE ".$this->tmpTableName." SET IsMet_BSQ=1 
                    WHERE AcademicYearID='".$this->AcademicYearID."'
                        AND GoodPoint_BSQ>='".$filter['BSQ_GP']."'
                        AND TargetMet_BSQ>='".$filter['BSQ_TM']."'";
			$successAry[] = $this->objDB->db_db_query($sql);

            $sql = "UPDATE ".$this->tmpTableName." SET IsMet_EPW=1 
                    WHERE AcademicYearID='".$this->AcademicYearID."'
                        AND TargetMet_EPW>='".$filter['EPW_TM']."'";
            $successAry[] = $this->objDB->db_db_query($sql);

            $sql = "UPDATE ".$this->tmpTableName." SET IsMet_OFS=1 
                    WHERE AcademicYearID='".$this->AcademicYearID."'
                        AND GoodPoint_OFS>='".$filter['OFS_GP']."'";
            $successAry[] = $this->objDB->db_db_query($sql);

            $sql = "UPDATE ".$this->tmpTableName." SET IsMet_BTP=1 
                    WHERE AcademicYearID='".$this->AcademicYearID."'
                        AND (GoodPoint_BTP>='".$filter['BTP_GP']."'
                            OR TargetMet_BTP>='".$filter['BTP_TM']."')";
            $successAry[] = $this->objDB->db_db_query($sql);
        }
    }

	public function getStudentWithMedal($filter=array(), $medal='', $nrScopeMet=1) {
		global $eclass_db;
		
		$this->drop_temp_award_table();
		$this->create_temp_award_table();
		if (!count($filter)) {
			$filter['BSQ_GP'] = 0;
			$filter['OFS_GP'] = 0;
			$filter['BTP_GP'] = 0;
			$filter['BSQ_TM'] = 0;
			$filter['EPW_TM'] = 0;
			$filter['BTP_TM'] = 0;
		}
		
		$this->setGoodPoint_BSQ($filter['BSQ_GP']);
		$this->setGoodPoint_OFS($filter['OFS_GP']);
		$this->setGoodPoint_BTP($filter['BTP_GP']);
		$this->setTargetMet_BSQ($filter['BSQ_TM']);
		$this->setTargetMet_EPW($filter['EPW_TM']);
		$this->setTargetMet_BTP($filter['BTP_TM']);

		$this->setNrScopeMet($filter);

		$classTitle = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
		$studentName = Get_Lang_Selection('u.ChineseName','u.EnglishName');
		if ($this->Awarded && $this->NotAwarded) {
			$cond = '';
		}
		else if (!$this->Awarded && $this->NotAwarded) {
			$cond = " AND a.Medal IS NULL";  
		}
		else if ($this->Awarded && !$this->NotAwarded) {
			$cond = " AND a.Medal IS NOT NULL";  
		}
		else {
			$cond = " AND 1=0";		// conflict 
		}
        $nrScopeMet= IntegerSafe($nrScopeMet);
        $cond .= " AND (t.IsMet_BSQ + t.IsMet_EPW + t.IsMet_OFS + t.IsMet_BTP)={$nrScopeMet}";

/*		switch ($medal) {
            case 'Gold':
                $cond .= " AND (t.IsMet_BSQ + t.IsMet_EPW + t.IsMet_OFS + t.IsMet_BTP)=4";
                break;
            case 'Silver':
                $cond .= " AND (t.IsMet_BSQ + t.IsMet_EPW + t.IsMet_OFS + t.IsMet_BTP)=3";
                break;
            case 'Bronze':
                $cond .= " AND (t.IsMet_BSQ + t.IsMet_EPW + t.IsMet_OFS + t.IsMet_BTP)=2";
                break;
        }*/

		$sql = "SELECT $classTitle AS ClassName, 
						ycu.ClassNumber, 
						$studentName as StudentName,
						t.GoodPoint_BSQ,
						t.GoodPoint_OFS,
						t.GoodPoint_BTP,
						t.TargetMet_BSQ,
						t.TargetMet_EPW,
						t.TargetMet_BTP,
						t.IsMet_BSQ + t.IsMet_EPW + t.IsMet_OFS + t.IsMet_BTP AS TargetMetScopeNumber,
						a.Medal,
						CONCAT('<input type=\'checkbox\' name=\'RecordRow[]\' id=\'RecordRow[]\' value=\'', t.UserID,'^~','".$this->AcademicYearID."\'>') AS CheckBox 
				FROM 	YEAR_CLASS yc
				INNER JOIN YEAR_CLASS_USER ycu ON ycu.YearClassID=yc.YearClassID
				INNER JOIN ".$this->tmpTableName." t  
						ON t.UserID=ycu.UserID AND t.AcademicYearID='".$this->AcademicYearID."'
				INNER JOIN INTRANET_USER u ON u.UserID=t.UserID
				LEFT JOIN {$eclass_db}.PORTFOLIO_JINYING_AWARD a
						ON a.UserID=t.UserID AND a.AcademicYearID=t.AcademicYearID
				WHERE yc.AcademicYearID='".$this->AcademicYearID."'
/*						AND t.GoodPoint_BSQ>='".$filter['BSQ_GP']."'
						AND t.GoodPoint_OFS>='".$filter['OFS_GP']."'
						AND t.GoodPoint_BTP>='".$filter['BTP_GP']."'
						AND t.TargetMet_BSQ>='".$filter['BSQ_TM']."'
						AND t.TargetMet_EPW>='".$filter['EPW_TM']."'
						AND t.TargetMet_BTP>='".$filter['BTP_TM']."'*/
						$cond
 				ORDER BY ClassName, ycu.ClassNumber";

		$rs = $this->objDB->returnResultSet($sql);

		$this->drop_temp_award_table();
		return $rs;			
	}
	
	// return junior / high school
	public function getMajorClassLevel($webSAMSCode) {
	    if (($webSAMSCode == 'S1') || ($webSAMSCode == 'S2') || ($webSAMSCode == 'S3')) {
	        return 'junior';
	    }
	    elseif (($webSAMSCode == 'S4') || ($webSAMSCode == 'S5') || ($webSAMSCode == 'S6')) {
	        return 'high';
	    }
	    else  {
	        return '';
	    }
	}
	
	public function getNumOfStudentsByClassLevel() {
		$sql = "SELECT 	y.YearID, 
						y.YearName AS Name,                        
						COUNT(u.UserID) AS NumStudent,
                        y.WEBSAMSCode
				FROM 
						`YEAR` y
				INNER JOIN 
						YEAR_CLASS yc ON yc.YearID=y.YearID
						AND yc.AcademicYearID='".$this->AcademicYearID."'
				INNER JOIN
						YEAR_CLASS_USER ycu ON ycu.YearClassID=yc.YearClassID
				INNER JOIN
						INTRANET_USER u ON u.UserID=ycu.UserID
				WHERE
						yc.AcademicYearID='".$this->AcademicYearID."'
				GROUP BY 
						y.YearID
				ORDER BY 
						y.Sequence
				";
		$rs = $this->objDB->returnResultSet($sql);
		return $rs;
	}
	
	public function getNumOfStudentsGotMedalByClassLevel($medal) {
		global $eclass_db;
		
		$sql = "SELECT 	y.YearID,
						COUNT(a.UserID) AS NumAwarded                        
				FROM 
						`YEAR` y
				INNER JOIN 
						YEAR_CLASS yc ON yc.YearID=y.YearID
						AND yc.AcademicYearID='".$this->AcademicYearID."'
				INNER JOIN
						YEAR_CLASS_USER ycu ON ycu.YearClassID=yc.YearClassID
				INNER JOIN
						INTRANET_USER u ON u.UserID=ycu.UserID
				LEFT JOIN		
						{$eclass_db}.PORTFOLIO_JINYING_AWARD a ON a.UserID=u.UserID
						AND a.AcademicYearID='".$this->AcademicYearID."'
						AND a.Medal='".$medal."'
				WHERE
						yc.AcademicYearID='".$this->AcademicYearID."'
				GROUP BY 
						y.YearID
				ORDER BY 
						y.YearName
				";
		$rs = $this->objDB->returnResultSet($sql);
		return $rs;
	}		

	public function getMedalStatByClassLevel() {
	    global $Lang;
	    
		$class_level = $this->getNumOfStudentsByClassLevel();
		$class_level = BuildMultiKeyAssoc($class_level,'YearID');
		$stat_gold = $this->getNumOfStudentsGotMedalByClassLevel('Gold');
		$stat_gold = BuildMultiKeyAssoc($stat_gold,'YearID');
		$stat_silver = $this->getNumOfStudentsGotMedalByClassLevel('Silver');
		$stat_silver = BuildMultiKeyAssoc($stat_silver,'YearID');
		$stat_bronze = $this->getNumOfStudentsGotMedalByClassLevel('Bronze');
		$stat_bronze = BuildMultiKeyAssoc($stat_bronze,'YearID');
		
		// initialize subTotal, last two rows
		$subTotal['junior']['gold'] = 0;
		$subTotal['junior']['silver'] = 0;
		$subTotal['junior']['bronze'] = 0;
		$subTotal['junior']['all'] = 0;
		$subTotal['junior']['none'] = 0;
		$subTotal['high']['gold'] = 0;
		$subTotal['high']['silver'] = 0;
		$subTotal['high']['bronze'] = 0;
		$subTotal['high']['all'] = 0;
		$subTotal['high']['none'] = 0;
		$juniorSubTotalNum = 0;
		$highSubTotalNum = 0;
		
		foreach((array)$class_level as $k=>$v) {
		    $numMedalAll = $stat_gold[$k]['NumAwarded'] + $stat_silver[$k]['NumAwarded'] + $stat_bronze[$k]['NumAwarded'];
			if ($v['NumStudent'] > 0) {
				$percent_gold = number_format($stat_gold[$k]['NumAwarded']/$v['NumStudent'] * 100,0);
				$percent_silver = number_format($stat_silver[$k]['NumAwarded']/$v['NumStudent'] * 100,0);
				$percent_bronze = number_format($stat_bronze[$k]['NumAwarded']/$v['NumStudent'] * 100,0);				
				$numMedalNone = $v['NumStudent'] - $numMedalAll;
				$percent_medalAll = number_format($numMedalAll/$v['NumStudent'] * 100,0);
				$percent_medalNone = 100 - $percent_medalAll;
				
				if ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'junior') {
				    $juniorSubTotalNum += $v['NumStudent'];
				    $subTotal['junior']['gold'] += $stat_gold[$k]['NumAwarded'];
				    $subTotal['junior']['silver'] += $stat_silver[$k]['NumAwarded'];
				    $subTotal['junior']['bronze'] += $stat_bronze[$k]['NumAwarded'];
				    $subTotal['junior']['all'] += $numMedalAll;
				    $subTotal['junior']['none'] += $numMedalNone;
				}
				elseif ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'high') {
				    $highSubTotalNum += $v['NumStudent'];
				    $subTotal['high']['gold'] += $stat_gold[$k]['NumAwarded'];
				    $subTotal['high']['silver'] += $stat_silver[$k]['NumAwarded'];
				    $subTotal['high']['bronze'] += $stat_bronze[$k]['NumAwarded'];
				    $subTotal['high']['all'] += $numMedalAll;
				    $subTotal['high']['none'] += $numMedalNone;
				}
			}
			else {
			    $numMedalNone = 0;
				$percent_gold = 0;
				$percent_silver = 0;
				$percent_bronze = 0;
				$percent_medalAll = 0;
				$percent_medalNone = 0;
			}
			$class_level[$k]['NumGold'] = $stat_gold[$k]['NumAwarded'];
			$class_level[$k]['PercentGold'] = $percent_gold;
			$class_level[$k]['NumSilver'] = $stat_silver[$k]['NumAwarded'];
			$class_level[$k]['PercentSilver'] = $percent_silver;
			$class_level[$k]['NumBronze'] = $stat_bronze[$k]['NumAwarded'];
			$class_level[$k]['PercentBronze'] = $percent_bronze;
			$class_level[$k]['NumMedalAll'] = $numMedalAll;
			$class_level[$k]['PercentMedalAll'] = $percent_medalAll;
			$class_level[$k]['NumMedalNone'] = $numMedalNone;
			$class_level[$k]['PercentMedalNone'] = $percent_medalNone;
		}
		
		
		// second last row
	    $majorLevel = 'junior';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['JuniorSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $juniorSubTotalNum;
	    $class_level[$majorLevel]['NumGold'] = $subTotal[$majorLevel]['gold'];
	    $class_level[$majorLevel]['NumSilver'] =  $subTotal[$majorLevel]['silver'];
	    $class_level[$majorLevel]['NumBronze'] =  $subTotal[$majorLevel]['bronze'];
	    $class_level[$majorLevel]['NumMedalAll'] = $subTotal[$majorLevel]['all'];
	    $class_level[$majorLevel]['NumMedalNone'] = $subTotal[$majorLevel]['none'];
		if ($juniorSubTotalNum > 0) {
		    $class_level[$majorLevel]['PercentGold'] = number_format($subTotal[$majorLevel]['gold']/$juniorSubTotalNum * 100,0);
		    $class_level[$majorLevel]['PercentSilver'] = number_format($subTotal[$majorLevel]['silver']/$juniorSubTotalNum * 100,0);
		    $class_level[$majorLevel]['PercentBronze'] = number_format($subTotal[$majorLevel]['bronze']/$juniorSubTotalNum * 100,0);
		    $class_level[$majorLevel]['PercentMedalAll'] = number_format($subTotal[$majorLevel]['all']/$juniorSubTotalNum * 100,0);
		    $class_level[$majorLevel]['PercentMedalNone'] = 100 - $class_level[$majorLevel]['PercentMedalAll'];
		}
		else {
		    $class_level[$majorLevel]['PercentGold'] = 0;
		    $class_level[$majorLevel]['PercentSilver'] = 0;
		    $class_level[$majorLevel]['PercentBronze'] = 0;
		    $class_level[$majorLevel]['PercentMedalAll'] = 0;
		    $class_level[$majorLevel]['PercentMedalNone'] = 0;
		}
		
		// last row
		$majorLevel = 'high';
		$class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['HighSecondary'];
		$class_level[$majorLevel]['NumStudent'] = $highSubTotalNum;
		$class_level[$majorLevel]['NumGold'] = $subTotal[$majorLevel]['gold'];
		$class_level[$majorLevel]['NumSilver'] =  $subTotal[$majorLevel]['silver'];
		$class_level[$majorLevel]['NumBronze'] =  $subTotal[$majorLevel]['bronze'];
		$class_level[$majorLevel]['NumMedalAll'] = $subTotal[$majorLevel]['all'];
		$class_level[$majorLevel]['NumMedalNone'] = $subTotal[$majorLevel]['none'];
		if ($highSubTotalNum> 0) {
		    $class_level[$majorLevel]['PercentGold'] = number_format($subTotal[$majorLevel]['gold']/$highSubTotalNum * 100,0);
		    $class_level[$majorLevel]['PercentSilver'] = number_format($subTotal[$majorLevel]['silver']/$highSubTotalNum * 100,0);
		    $class_level[$majorLevel]['PercentBronze'] = number_format($subTotal[$majorLevel]['bronze']/$highSubTotalNum * 100,0);
		    $class_level[$majorLevel]['PercentMedalAll'] = number_format($subTotal[$majorLevel]['all']/$highSubTotalNum * 100,0);
		    $class_level[$majorLevel]['PercentMedalNone'] = 100 - $class_level[$majorLevel]['PercentMedalAll'];
		}
		else {
		    $class_level[$majorLevel]['PercentGold'] = 0;
		    $class_level[$majorLevel]['PercentSilver'] = 0;
		    $class_level[$majorLevel]['PercentBronze'] = 0;
		    $class_level[$majorLevel]['PercentMedalAll'] = 0;
		    $class_level[$majorLevel]['PercentMedalNone'] = 0;
		}
		
		return $class_level;
	}		

	public function getNumOfStudentsByClass() {
		$classTitle = Get_Lang_Selection('ClassTitleB5','ClassTitleEN');
		$sql = "SELECT 	yc.YearClassID,
						yc.{$classTitle} AS Name,
						COUNT(u.UserID) AS NumStudent
				FROM 
						YEAR_CLASS yc
				INNER JOIN
						YEAR_CLASS_USER ycu ON ycu.YearClassID=yc.YearClassID
				INNER JOIN
						INTRANET_USER u ON u.UserID=ycu.UserID
				WHERE
						yc.AcademicYearID='".$this->AcademicYearID."'
				GROUP BY 
						yc.YearClassID
				ORDER BY 
						yc.{$classTitle}
				";
		$rs = $this->objDB->returnResultSet($sql);
		return $rs;
	}
	
	public function getNumOfStudentsGotMedalByClass($medal) {
		global $eclass_db;
		
		$classTitle = Get_Lang_Selection('ClassTitleB5','ClassTitleEN');
		$sql = "SELECT 	yc.YearClassID,
						COUNT(a.UserID) AS NumAwarded
				FROM 
						YEAR_CLASS yc
				INNER JOIN
						YEAR_CLASS_USER ycu ON ycu.YearClassID=yc.YearClassID
				INNER JOIN
						INTRANET_USER u ON u.UserID=ycu.UserID
				LEFT JOIN		
						{$eclass_db}.PORTFOLIO_JINYING_AWARD a ON a.UserID=u.UserID
						AND a.AcademicYearID='".$this->AcademicYearID."'
						AND a.Medal='".$medal."'
				WHERE
						yc.AcademicYearID='".$this->AcademicYearID."'
				GROUP BY 
						yc.YearClassID 
				ORDER BY 
						yc.{$classTitle}
				";
		$rs = $this->objDB->returnResultSet($sql);
		return $rs;
	}		

	public function getMedalStatByClass() {
		$class = $this->getNumOfStudentsByClass();
		$class = BuildMultiKeyAssoc($class,'YearClassID');
		$stat_gold = $this->getNumOfStudentsGotMedalByClass('Gold');
		$stat_gold = BuildMultiKeyAssoc($stat_gold,'YearClassID');
		$stat_silver = $this->getNumOfStudentsGotMedalByClass('Silver');
		$stat_silver = BuildMultiKeyAssoc($stat_silver,'YearClassID');
		$stat_bronze = $this->getNumOfStudentsGotMedalByClass('Bronze');
		$stat_bronze = BuildMultiKeyAssoc($stat_bronze,'YearClassID');
		foreach((array)$class as $k=>$v) {
			if ($v['NumStudent'] > 0) {
				$percent_gold = number_format($stat_gold[$k]['NumAwarded']/$v['NumStudent'] * 100,0);
				$percent_silver = number_format($stat_silver[$k]['NumAwarded']/$v['NumStudent'] * 100,0);
				$percent_bronze = number_format($stat_bronze[$k]['NumAwarded']/$v['NumStudent'] * 100,0);
			}
			else {
				$percent_gold = 0;
				$percent_silver = 0;
				$percent_bronze = 0;
			}
			$class[$k]['NumGold'] = $stat_gold[$k]['NumAwarded'];
			$class[$k]['PercentGold'] = $percent_gold;
			$class[$k]['NumSilver'] = $stat_silver[$k]['NumAwarded'];
			$class[$k]['PercentSilver'] = $percent_silver;
			$class[$k]['NumBronze'] = $stat_bronze[$k]['NumAwarded'];
			$class[$k]['PercentBronze'] = $percent_bronze;
		}
		return $class;
	}		

	// for export
	public function getJinYingData($para) {
		global $eclass_db;
		
		$classTitle = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
		$studentName = Get_Lang_Selection('u.ChineseName','u.EnglishName');

		$semester_filter = count($para['Semester']) ? " AND s.YearTermID IN ('".implode("','",(array)$para['Semester'])."')" : '';
		$class_filter = count($para['YearClassID']) ? " AND yc.YearClassID IN ('".implode("','",(array)$para['YearClassID'])."')" : '';
		
		$sql = "SELECT 	CONCAT('rid_',s.RecordID) AS RecordID,
						$classTitle AS ClassName, 
						ycu.ClassNumber,
						REPLACE(u.WebSAMSRegNo,'#','') as WebSAMSRegNo, 
						$studentName as StudentName,
						s.YearTermID,
						s.PerformanceCode,
						s.ActivityDate,
						s.ActivityNameEng,
						s.ActivityNameChi,
						s.AwardNameEng,
						s.AwardNameChi,
						s.RecommendMerit,
						s.OrganizationNameEng,
						s.OrganizationNameChi,
						s.ServiceHours
				FROM 
						{$eclass_db}.PORTFOLIO_JINYING_SCHEME s
				INNER JOIN
						INTRANET_USER u ON u.UserID=s.UserID
				INNER JOIN
						YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
				INNER JOIN 
						YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$para['AcademicYearID']."'
				WHERE
						s.AcademicYearID='".$para['AcademicYearID']."'
				AND 	s.PerformanceCode LIKE '".$para['Item']."%'".$semester_filter.$class_filter."
				ORDER BY 
						{$classTitle}, ycu.ClassNumber
				";
		$rs = $this->objDB->returnResultSet($sql);
		
		return $rs;
	}
	

	public function getRecordID($recordIDs) {
		global $eclass_db;
		$rs = array();
		if (is_array($recordIDs) && !empty($recordIDs)) {
			$sql = "SELECT 	RecordID
					FROM 
							{$eclass_db}.PORTFOLIO_JINYING_SCHEME
					WHERE
							RecordID IN ('".implode("','",(array)$recordIDs)."')
					ORDER BY RecordID";
			$rs = $this->objDB->returnVector($sql);
		}
		return $rs;
	}

	
	public function getPresetHeader($item, $actionType='insert') {
		global $PATH_WRT_ROOT, $intranet_session_language, $Lang;
		$presetHeader = array();
		
		if ($intranet_session_language != 'b5') {
			include($PATH_WRT_ROOT."lang/iportfolio_lang.b5.php");
		}
		switch($item) {
			case 'BTP-EA':
			case 'BTP-IC':
			case 'OFS-SI':
			case 'OFS-SO':
				$columnCode = $item;
				break;	
			case 'BSQ-EC':
			case 'BSQ-TA':
			case 'EPW-TA':
				$columnCode = 'WITH-ACTIVITY';
				break;	
			default:
				$columnCode = 'STANDARD';
				break;
		}
		foreach((array)$Lang['iPortfolio']['JinYing']['ImportColumns'][$columnCode] as $col) {		// Column Name is in Chinese in excel file 
			$presetHeader[] = $col;
		}

		if ($actionType != 'insert') {		
			switch($item) {
				case 'BSQ-EC':
				case 'BSQ-TA':
				case 'EPW-TA':
					$columnCode = 'WITH-ACTIVITY-DATE';
					$filter['Semester'] = null;
					break;
				case 'EPW-PY':
				case 'EPW-PJ':
				case 'EPW-PS':
                case 'BTP-TP':
                case 'BTP-IS':
					$columnCode = 'PHYSICAL';
					$filter['Semester'] = null;
					break;
                case 'EPW-PW':
                case 'EPW-OC':
                    $columnCode = 'STANDARD-WO-AWARD';
                    break;
				case 'OFS-SI':
				case 'OFS-SO':
				case 'BTP-EA':
				case 'BTP-IC':
					$columnCode = $item;
					$filter['Semester'] = null;
					break;
				default:
					$columnCode = 'STANDARD';
					break;
			}
			
			foreach((array)$Lang['iPortfolio']['JinYing']['ImportColumns4Update'][$columnCode] as $col) {		// Column Name is in Chinese in excel file 
				$presetHeader[] = $col;
			}
		}
		
		if ($intranet_session_language != 'b5') {
			include($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
		}
		
		return $presetHeader;		
	}		

	// Get The Page tab selection menu
	public function getStatisticsAnalysisTabMenu($selected='')
	{
	    global $Lang, $conf;
	    
	    $x = "";
	    $x .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
	    $x .= '<tbody>';
	    $x .= '<tr>';
	    $x .= '<td class="tab_underline" nowrap>';
	    $x .= '<div class="shadetabs">';
	    $x .= '<ul>';
	    
	    foreach((array)$conf['StatusticsAnalysis']['Tab']as $_tab => $_detail) {
	        $title = $_detail[0];
	        $linking = $_detail[1];
	        $classStyle = ($selected == $_tab) ? 'selected' : '';
	        $x .= '<li class="'.$classStyle.'"><a href="'.$linking.'"><span style="font-weight:bold">'.$title.'</span></a></li>';
	    }
	    
	    $x .= '</ul>';
	    $x .= '</div>';
	    $x .= '</td>';
	    $x .= '</tr>';
	    $x .= '</tbody>';
	    $x .= '</table>';
	    
	    return $x;
	}
	
	// getAll ClassLevel (YearID) of the academic year
	public function getAllClassLevel() 
	{
	    if ($this->AcademicYearID) {
    	    $sql = "SELECT     
                                DISTINCT y.YearID
                    FROM 
                                YEAR y 
                    INNER JOIN 
                                YEAR_CLASS yc ON yc.YearID=y.YearID
                    WHERE 
                                yc.AcademicYearID='".$this->AcademicYearID."'
                    ORDER BY    y.YearID";
    	    $rs = $this->objDB->returnVector($sql);
    	    return $rs;
	    }
	    else {
	        return '';
	    }
	}
	
	// group by class level
	public function getNumberOfStudentMeetMedalCriteriaByScope($scope,$filter)
	{
        switch ($scope) {
            case 'BSQ':
                $cond = " AND GoodPoint_BSQ>='".$filter['GoodPoint']."' AND TargetMet_BSQ>='".$filter['TargetMet']."'";
                break;
            case 'EPW':
                $cond = " AND TargetMet_EPW>='".$filter['TargetMet']."'";
                break;
            case 'OFS':
                $cond = " AND GoodPoint_OFS>='".$filter['GoodPoint']."'";
                break;
            case 'BTP':
                $cond = " AND GoodPoint_BTP>='".$filter['GoodPoint']."' AND TargetMet_BTP>='".$filter['TargetMet']."'";
                break;
            default:
                $cond = "";
                break;
        }
	    
	    $sql = "SELECT 	      y.YearID,
						      COUNT(t.UserID) AS NumAwarded
				FROM
						      `YEAR` y
				INNER JOIN
						      YEAR_CLASS yc ON yc.YearID=y.YearID AND yc.AcademicYearID='".$this->AcademicYearID."'
				INNER JOIN
						      YEAR_CLASS_USER ycu ON ycu.YearClassID=yc.YearClassID
				INNER JOIN
						      INTRANET_USER u ON u.UserID=ycu.UserID
				LEFT JOIN
                            (   SELECT      UserID 
                                FROM 
                                            ".$this->tmpTableName." 
                                WHERE 
                                            AcademicYearID='".$this->AcademicYearID."'".$cond.") AS t ON t.UserID=u.UserID
				WHERE
						      yc.AcademicYearID='".$this->AcademicYearID."'
				GROUP BY
						      y.YearID
				ORDER BY
						      y.YearName";
		$rs = $this->objDB->returnResultSet($sql);

		return $rs;				
	}
	
	public function getMedalStatByScope($filter) 
	{
	    global $Lang;
	    
	    $this->drop_temp_award_table();
	    $this->create_temp_award_table();
	    
	    $minGoodPoint = 0;
	    $minTargetMet = 0;	    
	    if (count($filter)) {
	        $scope = $filter['Scope'];
	        switch ($scope) {
	            case 'BSQ':
	                $minGoodPoint = $filter['GoodPointBronze'];
	                $minTargetMet = $filter['TargetMetBronze'];
	                $this->setGoodPoint_BSQ($minGoodPoint);
	                $this->setTargetMet_BSQ($minTargetMet);
	                break;
	            case 'EPW':	                
	                $minTargetMet = $filter['TargetMetBronze'];
	                $this->setTargetMet_EPW($minTargetMet);
	                break;
	            case 'OFS':
	                $minGoodPoint = $filter['GoodPointBronze'];
	                $this->setGoodPoint_OFS($minGoodPoint);
	                break;
	            case 'BTP':
	                $minGoodPoint = $filter['GoodPointBronze'];
	                $minTargetMet = $filter['TargetMetBronze'];
	                $this->setGoodPoint_BTP($minGoodPoint);
	                $this->setTargetMet_BTP($minTargetMet);
	                break;	                
	        }
	    }
	    else {
	        $filter['GoodPointGold'] = 0;
	        $filter['GoodPointSilver'] = 0;
	        $filter['GoodPointBronze'] = 0;
	        $filter['TargetMetGold'] = 0;
	        $filter['TargetMetSilver'] = 0;
	        $filter['TargetMetBronze'] = 0;
	    }
	    
	    $class_level = $this->getNumOfStudentsByClassLevel();
	    $class_level = BuildMultiKeyAssoc($class_level,'YearID');
	    
	    $filterGold['GoodPoint'] = $filter['GoodPointGold'];
	    $filterGold['TargetMet'] = $filter['TargetMetGold'];
	    $stat_gold = $this->getNumberOfStudentMeetMedalCriteriaByScope($scope,$filterGold);
	    $stat_gold = BuildMultiKeyAssoc($stat_gold,'YearID');
	    
	    $filterSilver['GoodPoint'] = $filter['GoodPointSilver'];
	    $filterSilver['TargetMet'] = $filter['TargetMetSilver'];
	    
	    $stat_silver= $this->getNumberOfStudentMeetMedalCriteriaByScope($scope,$filterSilver);
	    $stat_silver = BuildMultiKeyAssoc($stat_silver,'YearID');
	    
	    $filterBronze['GoodPoint'] = $filter['GoodPointBronze'];
	    $filterBronze['TargetMet'] = $filter['TargetMetBronze'];
	    $stat_bronze= $this->getNumberOfStudentMeetMedalCriteriaByScope($scope,$filterBronze);
	    $stat_bronze = BuildMultiKeyAssoc($stat_bronze,'YearID');
	    
	    // initialize subTotal, last two rows
	    $subTotal['junior']['gold'] = 0;
	    $subTotal['junior']['silver'] = 0;
	    $subTotal['junior']['bronze'] = 0;
	    $subTotal['junior']['none'] = 0;
	    $subTotal['high']['gold'] = 0;
	    $subTotal['high']['silver'] = 0;
	    $subTotal['high']['bronze'] = 0;
	    $subTotal['high']['none'] = 0;
	    $juniorSubTotalNum = 0;
	    $highSubTotalNum = 0;
	    
	    foreach((array)$class_level as $k=>$v) {
	        $numMedalAll = $stat_bronze[$k]['NumAwarded'];     // meet bronze criteria include silver and gold
	        if ($v['NumStudent'] > 0) {
	            $percent_gold = number_format($stat_gold[$k]['NumAwarded']/$v['NumStudent'] * 100,0);
	            $percent_silver = number_format($stat_silver[$k]['NumAwarded']/$v['NumStudent'] * 100,0);
	            $percent_bronze = number_format($stat_bronze[$k]['NumAwarded']/$v['NumStudent'] * 100,0);
	            $numMedalNone = $v['NumStudent'] - $numMedalAll;
	            $percent_medalNone = 100 - $percent_bronze;
	            
	            if ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'junior') {
	                $juniorSubTotalNum += $v['NumStudent'];
	                $subTotal['junior']['gold'] += $stat_gold[$k]['NumAwarded'];
	                $subTotal['junior']['silver'] += $stat_silver[$k]['NumAwarded'];
	                $subTotal['junior']['bronze'] += $stat_bronze[$k]['NumAwarded'];
	                $subTotal['junior']['none'] += $numMedalNone;
	            }
	            elseif ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'high') {
	                $highSubTotalNum += $v['NumStudent'];
	                $subTotal['high']['gold'] += $stat_gold[$k]['NumAwarded'];
	                $subTotal['high']['silver'] += $stat_silver[$k]['NumAwarded'];
	                $subTotal['high']['bronze'] += $stat_bronze[$k]['NumAwarded'];
	                $subTotal['high']['none'] += $numMedalNone;
	            }
	        }
	        else {
	            $numMedalNone = 0;
	            $percent_gold = 0;
	            $percent_silver = 0;
	            $percent_bronze = 0;
	            $percent_medalNone = 0;
	        }
	        $class_level[$k]['NumGold'] = $stat_gold[$k]['NumAwarded'];
	        $class_level[$k]['PercentGold'] = $percent_gold;
	        $class_level[$k]['NumSilver'] = $stat_silver[$k]['NumAwarded'];
	        $class_level[$k]['PercentSilver'] = $percent_silver;
	        $class_level[$k]['NumBronze'] = $stat_bronze[$k]['NumAwarded'];
	        $class_level[$k]['PercentBronze'] = $percent_bronze;
	        $class_level[$k]['NumMedalNone'] = $numMedalNone;
	        $class_level[$k]['PercentMedalNone'] = $percent_medalNone;
	    }
	    
	    
	    // second last row
	    $majorLevel = 'junior';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['JuniorSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $juniorSubTotalNum;
	    $class_level[$majorLevel]['NumGold'] = $subTotal[$majorLevel]['gold'];
	    $class_level[$majorLevel]['NumSilver'] =  $subTotal[$majorLevel]['silver'];
	    $class_level[$majorLevel]['NumBronze'] =  $subTotal[$majorLevel]['bronze'];
	    $class_level[$majorLevel]['NumMedalAll'] = $subTotal[$majorLevel]['all'];
	    $class_level[$majorLevel]['NumMedalNone'] = $subTotal[$majorLevel]['none'];
	    if ($juniorSubTotalNum > 0) {
	        $class_level[$majorLevel]['PercentGold'] = number_format($subTotal[$majorLevel]['gold']/$juniorSubTotalNum * 100,0);
	        $class_level[$majorLevel]['PercentSilver'] = number_format($subTotal[$majorLevel]['silver']/$juniorSubTotalNum * 100,0);
	        $class_level[$majorLevel]['PercentBronze'] = number_format($subTotal[$majorLevel]['bronze']/$juniorSubTotalNum * 100,0);
	        $class_level[$majorLevel]['PercentMedalNone'] = 100 - $class_level[$majorLevel]['PercentBronze'];
	    }
	    else {
	        $class_level[$majorLevel]['PercentGold'] = 0;
	        $class_level[$majorLevel]['PercentSilver'] = 0;
	        $class_level[$majorLevel]['PercentBronze'] = 0;
	        $class_level[$majorLevel]['PercentMedalNone'] = 0;
	    }
	    
	    // last row
	    $majorLevel = 'high';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['HighSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $highSubTotalNum;
	    $class_level[$majorLevel]['NumGold'] = $subTotal[$majorLevel]['gold'];
	    $class_level[$majorLevel]['NumSilver'] =  $subTotal[$majorLevel]['silver'];
	    $class_level[$majorLevel]['NumBronze'] =  $subTotal[$majorLevel]['bronze'];
	    $class_level[$majorLevel]['NumMedalNone'] = $subTotal[$majorLevel]['none'];
	    if ($highSubTotalNum> 0) {
	        $class_level[$majorLevel]['PercentGold'] = number_format($subTotal[$majorLevel]['gold']/$highSubTotalNum * 100,0);
	        $class_level[$majorLevel]['PercentSilver'] = number_format($subTotal[$majorLevel]['silver']/$highSubTotalNum * 100,0);
	        $class_level[$majorLevel]['PercentBronze'] = number_format($subTotal[$majorLevel]['bronze']/$highSubTotalNum * 100,0);
	        $class_level[$majorLevel]['PercentMedalNone'] = 100 - $class_level[$majorLevel]['PercentBronze'];
	    }
	    else {
	        $class_level[$majorLevel]['PercentGold'] = 0;
	        $class_level[$majorLevel]['PercentSilver'] = 0;
	        $class_level[$majorLevel]['PercentBronze'] = 0;
	        $class_level[$majorLevel]['PercentMedalNone'] = 0;
	    }
	    
	    $this->drop_temp_award_table();
	    
	    return $class_level;
	}
	
	public function getPerformanceSqlGoodPassFail($itemCode, $performanceType, $semester='')
	{
	    $cond = '';
        if ($semester) {
            $cond = " AND YearTermID='" . $semester . "'";
        }
	    if ($performanceType == JINYING_PERFORMANCE_GOOD) {
	        $cond .= " AND PerformanceCode='".$itemCode."-P-01'";
	    }
	    elseif ($performanceType == JINYING_PERFORMANCE_PASS) {
	        $cond .= " AND PerformanceCode='".$itemCode."-P-02'";
	    }
	    else { // fail
	        $cond .= " AND PerformanceCode='".$itemCode."-N-01'";
	    }	    
	    $sql = "SELECT UserID FROM ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME WHERE AcademicYearID='".$this->AcademicYearID."'".$cond;
	    return $sql;
	}

	public function getPerformanceSqlGoodFail($itemCode, $performanceType, $semester) 
	{
        $cond = " AND YearTermID='".$semester."'";
        if ($performanceType == JINYING_PERFORMANCE_GOOD) {
	        $cond .= " AND PerformanceCode='".$itemCode."-P-01'";
	    }
	    else { // fail
	        $cond .= " AND PerformanceCode='".$itemCode."-N-01'";
	    }
	    $sql = "SELECT UserID FROM ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME WHERE AcademicYearID='".$this->AcademicYearID."'".$cond;
	    return $sql;
	}

	public function getPerformanceSqlPassFail($itemCode, $performanceType, $semester='') 
	{
	    $cond = '';
	    if ($semester) {
	        $cond .= " AND YearTermID='".$semester."'";
	    }
	    if ($performanceType == JINYING_PERFORMANCE_PASS) {
	        $cond .= " AND PerformanceCode='".$itemCode."-P-01'";
	    }
	    else { // fail
	        $cond .= " AND PerformanceCode='".$itemCode."-N-01'";
	    }
	    $sql = "SELECT UserID FROM ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME WHERE AcademicYearID='".$this->AcademicYearID."'".$cond;
	    return $sql;
	}
	
	public function getPerformanceSqlGoodPassFailNotJoin($itemCode, $performanceType) 
	{
	    switch ($performanceType) {
	        case JINYING_PERFORMANCE_GOOD:
    	        $sql = "SELECT
                                    p.UserID
                        FROM
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
                        WHERE
                                    p.AcademicYearID='".$this->AcademicYearID."'
                        AND
                                    p.PerformanceCode='".$itemCode."-P-01'
                        GROUP BY    p.UserID";
    	        break;
	    
	        case JINYING_PERFORMANCE_PASS:
    	        $sql = "SELECT
                                    p.UserID
                        FROM
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
                        LEFT JOIN
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME s ON s.UserID=p.UserID
                                    AND s.AcademicYearID=p.AcademicYearID
                                    AND s.PerformanceCode='".$itemCode."-P-01'
                        WHERE
                                    p.AcademicYearID='".$this->AcademicYearID."'
                        AND
                                    p.PerformanceCode='".$itemCode."-P-02'
                        AND
                                    s.UserID IS NULL
                        GROUP BY    p.UserID";
    	        break;
	        case JINYING_PERFORMANCE_FAIL:
    	        $sql = "SELECT
                                    p.UserID
                        FROM
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
                        LEFT JOIN
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME s ON s.UserID=p.UserID
                                    AND s.AcademicYearID=p.AcademicYearID
                                    AND s.PerformanceCode IN ('".$itemCode."-P-01','".$itemCode."-P-02')
                        WHERE
                                    p.AcademicYearID='".$this->AcademicYearID."'
                        AND
                                    p.PerformanceCode='".$itemCode."-N-01'
                        AND
                                    s.UserID IS NULL
                        GROUP BY    p.UserID";
                break;
	        default:   // not participate, not need to call this sql, should use mutually exclusive method
    	        $sql = "SELECT
                                        ycu.UserID
                        FROM
                                        YEAR_CLASS_USER ycu
    	                INNER JOIN
    	                                YEAR_CLASS yc ON ycu.YearClassID=yc.YearClassID
                        AND
                                        yc.AcademicYearID='".$this->AcademicYearID."'
                        LEFT JOIN
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p ON p.UserID=ycu.UserID
                                    AND p.AcademicYearID='".$this->AcademicYearID."'
                        WHERE
                                    p.PerformanceCode NOT LIKE '".$itemCode."-%'
                        GROUP BY    ycu.UserID";
    	        break;
	    }
	    return $sql;
	}

	public function getPerformanceSqlInternalService($performanceType) 
	{
	    switch ($performanceType) {
	        case JINYING_PERFORMANCE_AWARD_WITH_MERIT:
	            $sql = "SELECT DISTINCT UserID FROM ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME WHERE AcademicYearID='".$this->AcademicYearID."' AND PerformanceCode='OFS-SI-P-01'";
                break;                
	        case JINYING_PERFORMANCE_AWARD_WITHOUT_MERIT:
	            $sql = "SELECT
                                    p.UserID
                        FROM
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
                        LEFT JOIN
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME s ON s.UserID=p.UserID
                                    AND s.AcademicYearID=p.AcademicYearID
                                    AND s.PerformanceCode='OFS-SI-P-01'
                        WHERE
                                    p.AcademicYearID='".$this->AcademicYearID."'
                        AND
                                    p.PerformanceCode='OFS-SI-P-02'
                        AND
                                    s.UserID IS NULL
                        GROUP BY    p.UserID";
	            break;	            
	        default:   // not participate, not need to call this sql, should use mutually exclusive method
	        $sql = "SELECT
                                    ycu.UserID
                    FROM
                                    YEAR_CLASS_USER ycu
	                INNER JOIN
	                                YEAR_CLASS yc ON ycu.YearClassID=yc.YearClassID
                    AND
                                    yc.AcademicYearID='".$this->AcademicYearID."'
                    LEFT JOIN
                                ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p ON p.UserID=ycu.UserID
                                AND p.AcademicYearID='".$this->AcademicYearID."'
                    WHERE
                                p.PerformanceCode NOT LIKE 'OFS-SI-%'
                    GROUP BY    ycu.UserID";
	           break;
	    }
	    return $sql;
	}
	
	public function getPerformanceSqlExternalService($performanceType) 
	{
	    global $conf;
	    
	    switch ($performanceType) {
	        case JINYING_PERFORMANCE_AWARD_WITH_MERIT:
    	        $sql = "SELECT
                                    p.UserID
                        FROM
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
                        WHERE
                                    p.AcademicYearID='".$this->AcademicYearID."'
                        AND
                                    p.ServiceHours>='".$conf['GPExRate']."'
                        GROUP BY    p.UserID";
    	        break;
	        case JINYING_PERFORMANCE_AWARD_WITHOUT_MERIT:
	           $sql = "SELECT
                                    p.UserID
                        FROM
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
                        WHERE
                                    p.AcademicYearID='".$this->AcademicYearID."'
                        AND
                                    p.ServiceHours>0 
                        AND
                                    p.ServiceHours<'".$conf['GPExRate']."'
                        GROUP BY    p.UserID";
	            break;
	        default:       // not participate, not need to call this sql, should use mutually exclusive method
	           $sql = "SELECT
                                        ycu.UserID
                        FROM
                                        YEAR_CLASS_USER ycu
    	                INNER JOIN
    	                                YEAR_CLASS yc ON ycu.YearClassID=yc.YearClassID
                        AND
                                        yc.AcademicYearID='".$this->AcademicYearID."'
                        LEFT JOIN
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p ON p.UserID=ycu.UserID
                                    AND p.AcademicYearID='".$this->AcademicYearID."'
	                    WHERE
                                    p.PerformanceCode NOT LIKE 'OFS-SO-%'
                        GROUP BY    ycu.UserID";
	           break;
	    }
	    return $sql;
	}

	public function getPerformanceSqlExternalCompetitions($performanceType)
	{
	    switch ($performanceType) {
	        case JINYING_PERFORMANCE_AWARD_WITH_MERIT:
    	        $sql = "SELECT
                                    p.UserID
                        FROM
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
                        WHERE
                                    p.AcademicYearID='".$this->AcademicYearID."'
                        AND
                                    p.PerformanceCode='BTP-IC-P-03'
                        AND
                                    p.RecommendMerit<>''
                        AND
                                    p.RecommendMerit IS NOT NULL
                        GROUP BY    p.UserID";
    	        break;
	        case JINYING_PERFORMANCE_AWARD_WITHOUT_MERIT:
	            $sql = "SELECT
                                    p.UserID
                        FROM
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
                        LEFT JOIN
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME s ON s.UserID=p.UserID
                                    AND s.AcademicYearID=p.AcademicYearID
                                    AND s.PerformanceCode='BTP-IC-P-03'
                                    AND s.RecommendMerit<>''
                                    AND s.RecommendMerit IS NOT NULL
	                    WHERE
                                    p.AcademicYearID='".$this->AcademicYearID."'
                        AND
                                    p.PerformanceCode='BTP-IC-P-03'
                        AND
                                    (p.RecommendMerit='' OR p.RecommendMerit IS NOT NULL)
                        AND
                                    s.UserID IS NULL
                        GROUP BY    p.UserID";
	            break;
	        case JINYING_PERFORMANCE_GOOD:
	            $sql = "SELECT
                                    p.UserID
                        FROM
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
                        LEFT JOIN
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME s ON s.UserID=p.UserID
                                    AND s.AcademicYearID=p.AcademicYearID
                                    AND s.PerformanceCode='BTP-IC-P-03'
	                    WHERE
                                    p.AcademicYearID='".$this->AcademicYearID."'
                        AND
                                    p.PerformanceCode='BTP-IC-P-01'
                        AND
                                    s.UserID IS NULL
                        GROUP BY    p.UserID";
	            break;
	        case JINYING_PERFORMANCE_PASS:
	            $sql = "SELECT
                                    p.UserID
                        FROM
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
                        LEFT JOIN
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME s ON s.UserID=p.UserID
                                    AND s.AcademicYearID=p.AcademicYearID
                                    AND s.PerformanceCode IN ('BTP-IC-P-01','BTP-IC-P-03')
	                    WHERE
                                    p.AcademicYearID='".$this->AcademicYearID."'
                        AND
                                    p.PerformanceCode='BTP-IC-P-02'
                        AND
                                    s.UserID IS NULL
                        GROUP BY    p.UserID";
	            break;
	        case JINYING_PERFORMANCE_PARTICIPATION:
	            $sql = "SELECT
                                    p.UserID
                        FROM
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
                        LEFT JOIN
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME s ON s.UserID=p.UserID
                                    AND s.AcademicYearID=p.AcademicYearID
                                    AND s.PerformanceCode IN ('BTP-IC-P-01','BTP-IC-P-02','BTP-IC-P-03')
	                    WHERE
                                    p.AcademicYearID='".$this->AcademicYearID."'
                        AND
                                    p.PerformanceCode='BTP-IC-P-04'
                        AND
                                    s.UserID IS NULL
                        GROUP BY    p.UserID";
	            break;
	        case JINYING_PERFORMANCE_FAIL:
	            $sql = "SELECT
                                    p.UserID
                        FROM
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
                        LEFT JOIN
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME s ON s.UserID=p.UserID
                                    AND s.AcademicYearID=p.AcademicYearID
                                    AND s.PerformanceCode LIKE 'BTP-IC-P%'
	                    WHERE
                                    p.AcademicYearID='".$this->AcademicYearID."'
                        AND
                                    p.PerformanceCode='BTP-IC-N-01'
                        AND
                                    s.UserID IS NULL
                        GROUP BY    p.UserID";
	            break;
	        default:       // not participate, not need to call this sql, should use mutually exclusive method
	            $sql = "SELECT
                                        ycu.UserID
                        FROM
                                        YEAR_CLASS_USER ycu
    	                INNER JOIN
    	                                YEAR_CLASS yc ON ycu.YearClassID=yc.YearClassID
                        AND
                                        yc.AcademicYearID='".$this->AcademicYearID."'
                        LEFT JOIN
                                    ".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p ON p.UserID=ycu.UserID
                                    AND p.AcademicYearID='".$this->AcademicYearID."'
	                    WHERE
                                    p.PerformanceCode NOT LIKE 'BTP-IC-%'
                        GROUP BY    ycu.UserID";
	            break;
	    }
	    return $sql;
	}
	
	// group by class level
	public function getNumberOfStudentByPerformance($itemCode, $performanceType, $semester='')
	{
	    $performanceSql = '';
	    switch ($itemCode) {
	        
	        case 'BSQ-HW':
	        case 'BSQ-PC':
            case 'EPW-PW':
	            $performanceSql = $this->getPerformanceSqlGoodPassFail($itemCode, $performanceType, $semester);
	            break;
	            
	        case 'BSQ-AD':
	            $performanceSql = $this->getPerformanceSqlGoodFail($itemCode, $performanceType, $semester);
	            break;
	            
	        case 'BSQ-AI':
	        case 'BSQ-RC':
	        case 'BSQ-RE':
	        case 'EPW-CA':
            case 'EPW-OC':
	            $performanceSql = $this->getPerformanceSqlPassFail($itemCode, $performanceType, $semester);
	            break;

	        case 'EPW-PY':
	        case 'EPW-PJ':
	        case 'EPW-PS':
            case 'BTP-TP':
	            $performanceSql = $this->getPerformanceSqlPassFail($itemCode, $performanceType);
	            break;

	        case 'BSQ-EC':
	        case 'BSQ-TA':
	        case 'EPW-TA':
	        case 'BTP-EA':
	            $performanceSql = $this->getPerformanceSqlGoodPassFailNotJoin($itemCode, $performanceType);
	            break;
	            
	        case 'OFS-SI':
	            $performanceSql = $this->getPerformanceSqlInternalService($performanceType);
	            break;
	            
	        case 'OFS-SO':
	            $performanceSql = $this->getPerformanceSqlExternalService($performanceType);
	            break;

	        case 'BTP-IC':
	            $performanceSql = $this->getPerformanceSqlExternalCompetitions($performanceType);
	            break;

            case 'BTP-IS':
                $performanceSql = $this->getPerformanceSqlGoodPassFail($itemCode, $performanceType);
                break;
	    }
	    
	    if ($performanceSql != '') {
    	    $sql = "SELECT 	      y.YearID,
    						      COUNT(p.UserID) AS NumStudent
    				FROM
    						      `YEAR` y
    				INNER JOIN
    						      YEAR_CLASS yc ON yc.YearID=y.YearID AND yc.AcademicYearID='".$this->AcademicYearID."'
    				INNER JOIN
    						      YEAR_CLASS_USER ycu ON ycu.YearClassID=yc.YearClassID
    				INNER JOIN
    						      INTRANET_USER u ON u.UserID=ycu.UserID
    				LEFT JOIN   
                                (".$performanceSql. ") AS p ON p.UserID=u.UserID
    				WHERE
    						      yc.AcademicYearID='".$this->AcademicYearID."'
    				GROUP BY
    						      y.YearID
    				ORDER BY
    						      y.YearName";
	       $rs = $this->objDB->returnResultSet($sql);

	       return $rs;
	    }
	    else {
	        return '';
	    }
	}
	
	public function getPerformanceGoodPassFail($itemCode, $semester)
	{
	    global $Lang, $conf;
	    
	    $class_level = $this->getNumOfStudentsByClassLevel();
	    $class_level = BuildMultiKeyAssoc($class_level,'YearID');

	    $stat_good = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_GOOD,$semester);
	    $stat_pass = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_PASS,$semester);
	    $stat_fail = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_FAIL,$semester);

	    $stat_good = BuildMultiKeyAssoc($stat_good,'YearID');
	    $stat_pass = BuildMultiKeyAssoc($stat_pass,'YearID');
	    $stat_fail = BuildMultiKeyAssoc($stat_fail,'YearID');

	    // initialize subTotal, last two rows
	    $subTotal['junior']['good'] = 0;
	    $subTotal['junior']['pass'] = 0;
	    $subTotal['junior']['fail'] = 0;
	    $subTotal['junior']['noData'] = 0;
	    $subTotal['high']['good'] = 0;
	    $subTotal['high']['pass'] = 0;
	    $subTotal['high']['fail'] = 0;
	    $subTotal['high']['noData'] = 0;
	    $juniorSubTotalNum = 0;
	    $highSubTotalNum = 0;

	    $isLastTerm = $this->isLastTerm($semester);
	    
	    foreach((array)$class_level as $k=>$v) {
	        $numAllStudent = $stat_good[$k]['NumStudent'] + $stat_pass[$k]['NumStudent'] + $stat_fail[$k]['NumStudent'];

	        if ($isLastTerm && $v['WEBSAMSCode'] == 'S6') {    // Form 6, 2nd Term show '/'
	            $percent_good = $conf['NoDataSymbol'];
	            $percent_pass = $conf['NoDataSymbol'];
	            $percent_fail = $conf['NoDataSymbol'];
	            $stat_good[$k]['NumStudent'] = $conf['NoDataSymbol'];
	            $stat_pass[$k]['NumStudent'] = $conf['NoDataSymbol'];
	            $stat_fail[$k]['NumStudent'] = $conf['NoDataSymbol'];
	            $class_level[$k]['NumStudent'] = $conf['NoDataSymbol'];
	            $numNoData = $conf['NoDataSymbol'];
	            $percent_noData = $conf['NoDataSymbol'];
	        }
	        else {
	            $numNoData = 0;      // default number of student that haven't imported data
	            
    	        if ($v['NumStudent'] > 0) {
    	            $percent_good = number_format($stat_good[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
    	            $percent_pass = number_format($stat_pass[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
    	            $percent_fail = number_format($stat_fail[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
    	            if ($numAllStudent != $v['NumStudent']) {
    	                $numNoData = $v['NumStudent'] - $numAllStudent;    	                
    	                $percent_noData = number_format($numNoData/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
    	            }
    	            else {
    	                $percent_noData= '0'.$conf['PercentSymbol'];
    	            }
    	            
    	            if ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'junior') {
    	                $juniorSubTotalNum += $v['NumStudent'];
    	                $subTotal['junior']['good'] += $stat_good[$k]['NumStudent'];
    	                $subTotal['junior']['pass'] += $stat_pass[$k]['NumStudent'];
    	                $subTotal['junior']['fail'] += $stat_fail[$k]['NumStudent'];
    	                $subTotal['junior']['noData'] = $numNoData;
    	            }
    	            elseif ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'high') {
    	                $highSubTotalNum += $v['NumStudent'];
    	                $subTotal['high']['good'] += $stat_good[$k]['NumStudent'];
    	                $subTotal['high']['pass'] += $stat_pass[$k]['NumStudent'];
    	                $subTotal['high']['fail'] += $stat_fail[$k]['NumStudent'];
    	                $subTotal['high']['noData'] = $numNoData;
    	            }
    	        }
    	        else {
    	            $percent_good = '0'.$conf['PercentSymbol'];
    	            $percent_pass = '0'.$conf['PercentSymbol'];
    	            $percent_fail = '0'.$conf['PercentSymbol'];
    	            $percent_noData= '0'.$conf['PercentSymbol'];
    	        }
	        }
	        
	        $class_level[$k]['NumGood'] = $stat_good[$k]['NumStudent'];
	        $class_level[$k]['PercentGood'] = $percent_good;
	        $class_level[$k]['NumPass'] = $stat_pass[$k]['NumStudent'];
	        $class_level[$k]['PercentPass'] = $percent_pass;
	        $class_level[$k]['NumFail'] = $stat_fail[$k]['NumStudent'];
	        $class_level[$k]['PercentFail'] = $percent_fail;
	        $class_level[$k]['NumNoData'] = $numNoData;
	        $class_level[$k]['PercentNoData'] = $percent_noData;
	    }
	    
	    
	    // second last row
	    $majorLevel = 'junior';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['JuniorSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $juniorSubTotalNum;
	    $class_level[$majorLevel]['NumGood'] = $subTotal[$majorLevel]['good'];
	    $class_level[$majorLevel]['NumPass'] = $subTotal[$majorLevel]['pass'];
	    $class_level[$majorLevel]['NumFail'] = $subTotal[$majorLevel]['fail'];
	    $class_level[$majorLevel]['NumNoData'] = $juniorSubTotalNum - $subTotal[$majorLevel]['good'] - $subTotal[$majorLevel]['pass'] - $subTotal[$majorLevel]['fail'];
	    if ($juniorSubTotalNum > 0) {
	        $class_level[$majorLevel]['PercentGood'] = number_format($subTotal[$majorLevel]['good']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentPass'] = number_format($subTotal[$majorLevel]['pass']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = number_format($subTotal[$majorLevel]['fail']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        if ($class_level[$majorLevel]['NumNoData'] > 0 ) {
	            $class_level[$majorLevel]['PercentNoData'] = number_format($class_level[$majorLevel]['NumNoData']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        }
	        else {
	            $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	        }
	    }
	    else {
	        $class_level[$majorLevel]['PercentGood'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentPass'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	    }
	    
	    // last row
	    $majorLevel = 'high';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['HighSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $highSubTotalNum;
	    $class_level[$majorLevel]['NumGood'] = $subTotal[$majorLevel]['good'];
	    $class_level[$majorLevel]['NumPass'] = $subTotal[$majorLevel]['pass'];
	    $class_level[$majorLevel]['NumFail'] = $subTotal[$majorLevel]['fail'];
	    $class_level[$majorLevel]['NumNoData'] = $highSubTotalNum - $subTotal[$majorLevel]['good'] - $subTotal[$majorLevel]['pass'] - $subTotal[$majorLevel]['fail'];
	    if ($highSubTotalNum> 0) {
	        $class_level[$majorLevel]['PercentGood'] = number_format($subTotal[$majorLevel]['good']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentPass'] = number_format($subTotal[$majorLevel]['pass']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = number_format($subTotal[$majorLevel]['fail']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        if ($class_level[$majorLevel]['NumNoData'] > 0 ) {
	            $class_level[$majorLevel]['PercentNoData'] = number_format($class_level[$majorLevel]['NumNoData']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        }
	        else {
	            $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	        }
	    }
	    else {
	        $class_level[$majorLevel]['PercentGood'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentPass'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	    }
	    
	    return $class_level;
	}

	public function getPerformanceGoodFail($itemCode, $semester)
	{
	    global $Lang, $conf;
	    
	    $class_level = $this->getNumOfStudentsByClassLevel();
	    $class_level = BuildMultiKeyAssoc($class_level,'YearID');
	    
	    $stat_good = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_GOOD,$semester);
	    $stat_fail = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_FAIL,$semester);
	    
	    $stat_good = BuildMultiKeyAssoc($stat_good,'YearID');
	    $stat_fail = BuildMultiKeyAssoc($stat_fail,'YearID');
	    
	    // initialize subTotal, last two rows
	    $subTotal['junior']['good'] = 0;
	    $subTotal['junior']['fail'] = 0;
	    $subTotal['junior']['noData'] = 0;
	    $subTotal['high']['good'] = 0;
	    $subTotal['high']['fail'] = 0;
	    $subTotal['high']['noData'] = 0;
	    $juniorSubTotalNum = 0;
	    $highSubTotalNum = 0;
	    
	    $isLastTerm = $this->isLastTerm($semester);
	    
	    foreach((array)$class_level as $k=>$v) {
	        $numAllStudent = $stat_good[$k]['NumStudent'] + $stat_fail[$k]['NumStudent'];
	        
	        if ($isLastTerm && $v['WEBSAMSCode'] == 'S6') {    // Form 6, 2nd Term show '/'
	            $percent_good = $conf['NoDataSymbol'];
	            $percent_fail = $conf['NoDataSymbol'];
	            $stat_good[$k]['NumStudent'] = $conf['NoDataSymbol'];
	            $stat_fail[$k]['NumStudent'] = $conf['NoDataSymbol'];
	            $class_level[$k]['NumStudent'] = $conf['NoDataSymbol'];
	            $numNoData = $conf['NoDataSymbol'];
	            $percent_noData = $conf['NoDataSymbol'];
	        }
	        else {
	            $numNoData = 0;      // default number of student that haven't imported data
	            
	            if ($v['NumStudent'] > 0) {
	                $percent_good = number_format($stat_good[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	                $percent_fail = number_format($stat_fail[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	                if ($numAllStudent != $v['NumStudent']) {
	                    $numNoData = $v['NumStudent'] - $numAllStudent;
	                    $percent_noData = number_format($numNoData/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	                }
	                else {
	                    $percent_noData= '0'.$conf['PercentSymbol'];
	                }
	                
	                if ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'junior') {
	                    $juniorSubTotalNum += $v['NumStudent'];
	                    $subTotal['junior']['good'] += $stat_good[$k]['NumStudent'];
	                    $subTotal['junior']['fail'] += $stat_fail[$k]['NumStudent'];
	                    $subTotal['junior']['noData'] = $numNoData;
	                }
	                elseif ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'high') {
	                    $highSubTotalNum += $v['NumStudent'];
	                    $subTotal['high']['good'] += $stat_good[$k]['NumStudent'];
	                    $subTotal['high']['fail'] += $stat_fail[$k]['NumStudent'];
	                    $subTotal['high']['noData'] = $numNoData;
	                }
	            }
	            else {
	                $percent_good = '0'.$conf['PercentSymbol'];
	                $percent_fail = '0'.$conf['PercentSymbol'];
	                $percent_noData= '0'.$conf['PercentSymbol'];
	            }
	        }
	        
	        $class_level[$k]['NumGood'] = $stat_good[$k]['NumStudent'];
	        $class_level[$k]['PercentGood'] = $percent_good;
	        $class_level[$k]['NumFail'] = $stat_fail[$k]['NumStudent'];
	        $class_level[$k]['PercentFail'] = $percent_fail;
	        $class_level[$k]['NumNoData'] = $numNoData;
	        $class_level[$k]['PercentNoData'] = $percent_noData;
	    }
	    
	    
	    // second last row
	    $majorLevel = 'junior';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['JuniorSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $juniorSubTotalNum;
	    $class_level[$majorLevel]['NumGood'] = $subTotal[$majorLevel]['good'];
	    $class_level[$majorLevel]['NumFail'] = $subTotal[$majorLevel]['fail'];
	    $class_level[$majorLevel]['NumNoData'] = $juniorSubTotalNum - $subTotal[$majorLevel]['good'] - $subTotal[$majorLevel]['fail'];
	    if ($juniorSubTotalNum > 0) {
	        $class_level[$majorLevel]['PercentGood'] = number_format($subTotal[$majorLevel]['good']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = number_format($subTotal[$majorLevel]['fail']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        if ($class_level[$majorLevel]['NumNoData'] > 0 ) {
	            $class_level[$majorLevel]['PercentNoData'] = number_format($class_level[$majorLevel]['NumNoData']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        }
	        else {
	            $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	        }
	    }
	    else {
	        $class_level[$majorLevel]['PercentGood'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	    }
	    
	    // last row
	    $majorLevel = 'high';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['HighSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $highSubTotalNum;
	    $class_level[$majorLevel]['NumGood'] = $subTotal[$majorLevel]['good'];
	    $class_level[$majorLevel]['NumFail'] = $subTotal[$majorLevel]['fail'];
	    $class_level[$majorLevel]['NumNoData'] = $highSubTotalNum - $subTotal[$majorLevel]['good'] - $subTotal[$majorLevel]['fail'];
	    if ($highSubTotalNum> 0) {
	        $class_level[$majorLevel]['PercentGood'] = number_format($subTotal[$majorLevel]['good']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = number_format($subTotal[$majorLevel]['fail']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        if ($class_level[$majorLevel]['NumNoData'] > 0 ) {
	            $class_level[$majorLevel]['PercentNoData'] = number_format($class_level[$majorLevel]['NumNoData']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        }
	        else {
	            $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	        }
	    }
	    else {
	        $class_level[$majorLevel]['PercentGood'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	    }
	    
	    return $class_level;
	}

	public function getPerformancePassFail($itemCode, $semester='')
	{
	    global $Lang, $conf;
	    
	    $class_level = $this->getNumOfStudentsByClassLevel();
	    $class_level = BuildMultiKeyAssoc($class_level,'YearID');
	    
	    $stat_pass = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_PASS,$semester);
	    $stat_fail = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_FAIL,$semester);
	    
	    $stat_pass = BuildMultiKeyAssoc($stat_pass,'YearID');
	    $stat_fail = BuildMultiKeyAssoc($stat_fail,'YearID');
	    
	    // initialize subTotal, last two rows
	    $subTotal['junior']['pass'] = 0;
	    $subTotal['junior']['fail'] = 0;
	    $subTotal['junior']['noData'] = 0;
	    $subTotal['high']['pass'] = 0;
	    $subTotal['high']['fail'] = 0;
	    $subTotal['high']['noData'] = 0;
	    $juniorSubTotalNum = 0;
	    $highSubTotalNum = 0;
	    
	    $isLastTerm = $this->isLastTerm($semester);
	    
	    foreach((array)$class_level as $k=>$v) {
	        $numAllStudent = $stat_pass[$k]['NumStudent'] + $stat_fail[$k]['NumStudent'];
	        
	        if ($isLastTerm && $v['WEBSAMSCode'] == 'S6') {    // Form 6, 2nd Term show '/'
	            $percent_pass = $conf['NoDataSymbol'];
	            $percent_fail = $conf['NoDataSymbol'];
	            $stat_pass[$k]['NumStudent'] = $conf['NoDataSymbol'];
	            $stat_fail[$k]['NumStudent'] = $conf['NoDataSymbol'];
	            $class_level[$k]['NumStudent'] = $conf['NoDataSymbol'];
	            $numNoData = $conf['NoDataSymbol'];
	            $percent_noData = $conf['NoDataSymbol'];
	        }
	        else {
	            $numNoData = 0;      // default number of student that haven't imported data
	            
	            if ($v['NumStudent'] > 0) {
	                $percent_pass = number_format($stat_pass[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	                $percent_fail = number_format($stat_fail[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	                if ($numAllStudent != $v['NumStudent']) {
	                    $numNoData = $v['NumStudent'] - $numAllStudent;
	                    $percent_noData = number_format($numNoData/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	                }
	                else {
	                    $percent_noData= '0'.$conf['PercentSymbol'];
	                }
	                
	                if ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'junior') {
	                    $juniorSubTotalNum += $v['NumStudent'];
	                    $subTotal['junior']['pass'] += $stat_pass[$k]['NumStudent'];
	                    $subTotal['junior']['fail'] += $stat_fail[$k]['NumStudent'];
	                    $subTotal['junior']['noData'] = $numNoData;
	                }
	                elseif ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'high') {
	                    $highSubTotalNum += $v['NumStudent'];
	                    $subTotal['high']['pass'] += $stat_pass[$k]['NumStudent'];
	                    $subTotal['high']['fail'] += $stat_fail[$k]['NumStudent'];
	                    $subTotal['high']['noData'] = $numNoData;
	                }
	            }
	            else {
	                $percent_pass = '0'.$conf['PercentSymbol'];
	                $percent_fail = '0'.$conf['PercentSymbol'];
	                $percent_noData= '0'.$conf['PercentSymbol'];
	            }
	        }
	        
	        $class_level[$k]['NumPass'] = $stat_pass[$k]['NumStudent'];
	        $class_level[$k]['PercentPass'] = $percent_pass;
	        $class_level[$k]['NumFail'] = $stat_fail[$k]['NumStudent'];
	        $class_level[$k]['PercentFail'] = $percent_fail;
	        $class_level[$k]['NumNoData'] = $numNoData;
	        $class_level[$k]['PercentNoData'] = $percent_noData;
	    }
	    
	    
	    // second last row
	    $majorLevel = 'junior';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['JuniorSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $juniorSubTotalNum;
	    $class_level[$majorLevel]['NumPass'] = $subTotal[$majorLevel]['pass'];
	    $class_level[$majorLevel]['NumFail'] = $subTotal[$majorLevel]['fail'];
	    $class_level[$majorLevel]['NumNoData'] = $juniorSubTotalNum - $subTotal[$majorLevel]['pass'] - $subTotal[$majorLevel]['fail'];
	    if ($juniorSubTotalNum > 0) {
	        $class_level[$majorLevel]['PercentPass'] = number_format($subTotal[$majorLevel]['pass']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = number_format($subTotal[$majorLevel]['fail']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        if ($class_level[$majorLevel]['NumNoData'] > 0 ) {
	            $class_level[$majorLevel]['PercentNoData'] = number_format($class_level[$majorLevel]['NumNoData']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        }
	        else {
	            $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	        }
	    }
	    else {
	        $class_level[$majorLevel]['PercentPass'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	    }
	    
	    // last row
	    $majorLevel = 'high';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['HighSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $highSubTotalNum;
	    $class_level[$majorLevel]['NumPass'] = $subTotal[$majorLevel]['pass'];
	    $class_level[$majorLevel]['NumFail'] = $subTotal[$majorLevel]['fail'];
	    $class_level[$majorLevel]['NumNoData'] = $highSubTotalNum - $subTotal[$majorLevel]['pass'] - $subTotal[$majorLevel]['fail'];
	    if ($highSubTotalNum> 0) {
	        $class_level[$majorLevel]['PercentPass'] = number_format($subTotal[$majorLevel]['pass']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = number_format($subTotal[$majorLevel]['fail']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        if ($class_level[$majorLevel]['NumNoData'] > 0 ) {
	            $class_level[$majorLevel]['PercentNoData'] = number_format($class_level[$majorLevel]['NumNoData']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        }
	        else {
	            $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	        }
	    }
	    else {
	        $class_level[$majorLevel]['PercentPass'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	    }
	    
	    return $class_level;
	}

    public function getPerformanceGoodPassFailNotJoin($itemCode)
    {
        global $Lang, $conf;

        $class_level = $this->getNumOfStudentsByClassLevel();
        $class_level = BuildMultiKeyAssoc($class_level,'YearID');

        $stat_good = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_GOOD);
        $stat_pass = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_PASS);
        $stat_fail = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_FAIL);

        $stat_good = BuildMultiKeyAssoc($stat_good,'YearID');
        $stat_pass = BuildMultiKeyAssoc($stat_pass,'YearID');
        $stat_fail = BuildMultiKeyAssoc($stat_fail,'YearID');

        // initialize subTotal, last two rows
        $subTotal['junior']['good'] = 0;
        $subTotal['junior']['pass'] = 0;
        $subTotal['junior']['fail'] = 0;
        $subTotal['junior']['noData'] = 0;     // no participation
        $subTotal['high']['good'] = 0;
        $subTotal['high']['pass'] = 0;
        $subTotal['high']['fail'] = 0;
        $subTotal['high']['noData'] = 0;
        $juniorSubTotalNum = 0;
        $highSubTotalNum = 0;

        foreach((array)$class_level as $k=>$v) {
            $numAllStudent = $stat_good[$k]['NumStudent'] + $stat_pass[$k]['NumStudent'] + $stat_fail[$k]['NumStudent'];

            $numNoData = 0;      // default number of student that haven't imported data

            if ($v['NumStudent'] > 0) {
                $percent_good = number_format($stat_good[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
                $percent_pass = number_format($stat_pass[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
                $percent_fail = number_format($stat_fail[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
                $numNoData = $v['NumStudent'] - $numAllStudent;
                $percent_noData = number_format($numNoData/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];

                if ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'junior') {
                    $juniorSubTotalNum += $v['NumStudent'];
                    $subTotal['junior']['good'] += $stat_good[$k]['NumStudent'];
                    $subTotal['junior']['pass'] += $stat_pass[$k]['NumStudent'];
                    $subTotal['junior']['fail'] += $stat_fail[$k]['NumStudent'];
                    $subTotal['junior']['noData'] = $numNoData;
                }
                elseif ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'high') {
                    $highSubTotalNum += $v['NumStudent'];
                    $subTotal['high']['good'] += $stat_good[$k]['NumStudent'];
                    $subTotal['high']['pass'] += $stat_pass[$k]['NumStudent'];
                    $subTotal['high']['fail'] += $stat_fail[$k]['NumStudent'];
                    $subTotal['high']['noData'] = $numNoData;
                }
            }
            else {
                $percent_good = '0'.$conf['PercentSymbol'];
                $percent_pass = '0'.$conf['PercentSymbol'];
                $percent_fail = '0'.$conf['PercentSymbol'];
                $percent_noData= '0'.$conf['PercentSymbol'];
            }

            $class_level[$k]['NumGood'] = $stat_good[$k]['NumStudent'];
            $class_level[$k]['PercentGood'] = $percent_good;
            $class_level[$k]['NumPass'] = $stat_pass[$k]['NumStudent'];
            $class_level[$k]['PercentPass'] = $percent_pass;
            $class_level[$k]['NumFail'] = $stat_fail[$k]['NumStudent'];
            $class_level[$k]['PercentFail'] = $percent_fail;
            $class_level[$k]['NumNoData'] = $numNoData;
            $class_level[$k]['PercentNoData'] = $percent_noData;
        }


        // second last row
        $majorLevel = 'junior';
        $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['JuniorSecondary'];
        $class_level[$majorLevel]['NumStudent'] = $juniorSubTotalNum;
        $class_level[$majorLevel]['NumGood'] = $subTotal[$majorLevel]['good'];
        $class_level[$majorLevel]['NumPass'] = $subTotal[$majorLevel]['pass'];
        $class_level[$majorLevel]['NumFail'] = $subTotal[$majorLevel]['fail'];
        $class_level[$majorLevel]['NumNoData'] = $juniorSubTotalNum - $subTotal[$majorLevel]['good'] - $subTotal[$majorLevel]['pass'] - $subTotal[$majorLevel]['fail'];
        if ($juniorSubTotalNum > 0) {
            $class_level[$majorLevel]['PercentGood'] = number_format($subTotal[$majorLevel]['good']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
            $class_level[$majorLevel]['PercentPass'] = number_format($subTotal[$majorLevel]['pass']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
            $class_level[$majorLevel]['PercentFail'] = number_format($subTotal[$majorLevel]['fail']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
            $class_level[$majorLevel]['PercentNoData'] = number_format($class_level[$majorLevel]['NumNoData']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
        }
        else {
            $class_level[$majorLevel]['PercentGood'] = '0'.$conf['PercentSymbol'];
            $class_level[$majorLevel]['PercentPass'] = '0'.$conf['PercentSymbol'];
            $class_level[$majorLevel]['PercentFail'] = '0'.$conf['PercentSymbol'];
            $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
        }

        // last row
        $majorLevel = 'high';
        $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['HighSecondary'];
        $class_level[$majorLevel]['NumStudent'] = $highSubTotalNum;
        $class_level[$majorLevel]['NumGood'] = $subTotal[$majorLevel]['good'];
        $class_level[$majorLevel]['NumPass'] = $subTotal[$majorLevel]['pass'];
        $class_level[$majorLevel]['NumFail'] = $subTotal[$majorLevel]['fail'];
        $class_level[$majorLevel]['NumNoData'] = $highSubTotalNum - $subTotal[$majorLevel]['good'] - $subTotal[$majorLevel]['pass'] - $subTotal[$majorLevel]['fail'];
        if ($highSubTotalNum> 0) {
            $class_level[$majorLevel]['PercentGood'] = number_format($subTotal[$majorLevel]['good']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
            $class_level[$majorLevel]['PercentPass'] = number_format($subTotal[$majorLevel]['pass']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
            $class_level[$majorLevel]['PercentFail'] = number_format($subTotal[$majorLevel]['fail']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
            $class_level[$majorLevel]['PercentNoData'] = number_format($class_level[$majorLevel]['NumNoData']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
        }
        else {
            $class_level[$majorLevel]['PercentGood'] = '0'.$conf['PercentSymbol'];
            $class_level[$majorLevel]['PercentPass'] = '0'.$conf['PercentSymbol'];
            $class_level[$majorLevel]['PercentFail'] = '0'.$conf['PercentSymbol'];
            $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
        }

        return $class_level;
    }

	public function getPerformanceService($itemCode)
	{
	    global $Lang, $conf;
	    
	    $class_level = $this->getNumOfStudentsByClassLevel();
	    $class_level = BuildMultiKeyAssoc($class_level,'YearID');
	    
        $stat_good = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_AWARD_WITH_MERIT);
        $stat_pass = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_AWARD_WITHOUT_MERIT);
	    
	    $stat_good = BuildMultiKeyAssoc($stat_good,'YearID');
	    $stat_pass = BuildMultiKeyAssoc($stat_pass,'YearID');
	    
	    // initialize subTotal, last two rows
	    $subTotal['junior']['good'] = 0;       // award with merit
	    $subTotal['junior']['pass'] = 0;       // award without merit
	    $subTotal['junior']['join'] = 0;       // participation
	    $subTotal['junior']['noData'] = 0;     // no participation
	    $subTotal['high']['good'] = 0;
	    $subTotal['high']['pass'] = 0;
	    $subTotal['high']['join'] = 0;
	    $subTotal['high']['noData'] = 0;
	    $juniorSubTotalNum = 0;
	    $highSubTotalNum = 0;
	    
	    foreach((array)$class_level as $k=>$v) {
	        $numAllStudent = $stat_good[$k]['NumStudent'] + $stat_pass[$k]['NumStudent'];
	        
	        $numNoData = 0;      // default number of student that haven't imported data
	        
	        if ($v['NumStudent'] > 0) {
	            $percent_good = number_format($stat_good[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	            $percent_pass = number_format($stat_pass[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	            $percent_join = number_format($numAllStudent/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	            $numNoData = $v['NumStudent'] - $numAllStudent;
	            $percent_noData = number_format($numNoData/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	            
	            if ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'junior') {
	                $juniorSubTotalNum += $v['NumStudent'];
	                $subTotal['junior']['good'] += $stat_good[$k]['NumStudent'];
	                $subTotal['junior']['pass'] += $stat_pass[$k]['NumStudent'];
	                $subTotal['junior']['join'] += $numAllStudent;
	                $subTotal['junior']['noData'] = $numNoData;
	            }
	            elseif ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'high') {
	                $highSubTotalNum += $v['NumStudent'];
	                $subTotal['high']['good'] += $stat_good[$k]['NumStudent'];
	                $subTotal['high']['pass'] += $stat_pass[$k]['NumStudent'];
	                $subTotal['high']['join'] += $numAllStudent;
	                $subTotal['high']['noData'] = $numNoData;
	            }
	        }
	        else {
	            $percent_good = '0'.$conf['PercentSymbol'];
	            $percent_pass = '0'.$conf['PercentSymbol'];
	            $percent_join = '0'.$conf['PercentSymbol'];
	            $percent_noData= '0'.$conf['PercentSymbol'];
	        }
	        
	        $class_level[$k]['NumGood'] = $stat_good[$k]['NumStudent'];
	        $class_level[$k]['PercentGood'] = $percent_good;
	        $class_level[$k]['NumPass'] = $stat_pass[$k]['NumStudent'];
	        $class_level[$k]['PercentPass'] = $percent_pass;
	        $class_level[$k]['NumJoin'] = $numAllStudent;
	        $class_level[$k]['PercentJoin'] = $percent_join;
	        $class_level[$k]['NumNoData'] = $numNoData;
	        $class_level[$k]['PercentNoData'] = $percent_noData;
	    }
	    
	    
	    // second last row
	    $majorLevel = 'junior';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['JuniorSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $juniorSubTotalNum;
	    $class_level[$majorLevel]['NumGood'] = $subTotal[$majorLevel]['good'];
	    $class_level[$majorLevel]['NumPass'] = $subTotal[$majorLevel]['pass'];
	    $class_level[$majorLevel]['NumJoin'] = $subTotal[$majorLevel]['join'];
	    $class_level[$majorLevel]['NumNoData'] = $juniorSubTotalNum - $subTotal[$majorLevel]['join'];
	    if ($juniorSubTotalNum > 0) {
	        $class_level[$majorLevel]['PercentGood'] = number_format($subTotal[$majorLevel]['good']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentPass'] = number_format($subTotal[$majorLevel]['pass']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentJoin'] = number_format($subTotal[$majorLevel]['join']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = number_format($class_level[$majorLevel]['NumNoData']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	    }
	    else {
	        $class_level[$majorLevel]['PercentGood'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentPass'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentJoin'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	    }
	    
	    // last row
	    $majorLevel = 'high';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['HighSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $highSubTotalNum;
	    $class_level[$majorLevel]['NumGood'] = $subTotal[$majorLevel]['good'];
	    $class_level[$majorLevel]['NumPass'] = $subTotal[$majorLevel]['pass'];
	    $class_level[$majorLevel]['NumJoin'] = $subTotal[$majorLevel]['join'];
	    $class_level[$majorLevel]['NumNoData'] = $highSubTotalNum - $subTotal[$majorLevel]['join'];
	    if ($highSubTotalNum> 0) {
	        $class_level[$majorLevel]['PercentGood'] = number_format($subTotal[$majorLevel]['good']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentPass'] = number_format($subTotal[$majorLevel]['pass']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentJoin'] = number_format($subTotal[$majorLevel]['join']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = number_format($class_level[$majorLevel]['NumNoData']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	    }
	    else {
	        $class_level[$majorLevel]['PercentGood'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentPass'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentJoin'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	    }
	    
	    return $class_level;
	}
	
	public function getPerformanceExternalCompetitions($itemCode)
	{
	    global $Lang, $conf;
	    
	    $class_level = $this->getNumOfStudentsByClassLevel();
	    $class_level = BuildMultiKeyAssoc($class_level,'YearID');
	    
	    $stat_merit = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_AWARD_WITH_MERIT);
	    $stat_nomerit = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_AWARD_WITHOUT_MERIT);
	    $stat_good = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_GOOD);
	    $stat_pass = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_PASS);
	    $stat_join = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_PARTICIPATION);
	    $stat_fail = $this->getNumberOfStudentByPerformance($itemCode,JINYING_PERFORMANCE_FAIL);
	    
	    $stat_merit= BuildMultiKeyAssoc($stat_merit,'YearID');
	    $stat_nomerit= BuildMultiKeyAssoc($stat_nomerit,'YearID');
	    $stat_good = BuildMultiKeyAssoc($stat_good,'YearID');
	    $stat_pass = BuildMultiKeyAssoc($stat_pass,'YearID');
	    $stat_join = BuildMultiKeyAssoc($stat_join,'YearID');
	    $stat_fail = BuildMultiKeyAssoc($stat_fail,'YearID');
	    
	    // initialize subTotal, last two rows
	    $subTotal['junior']['merit'] = 0;
	    $subTotal['junior']['nomerit'] = 0;
	    $subTotal['junior']['good'] = 0;
	    $subTotal['junior']['pass'] = 0;
	    $subTotal['junior']['join'] = 0;
	    $subTotal['junior']['fail'] = 0;
	    $subTotal['junior']['noData'] = 0;     // no participation
	    
	    $subTotal['high']['merit'] = 0;
	    $subTotal['high']['nomerit'] = 0;	    
	    $subTotal['high']['good'] = 0;
	    $subTotal['high']['pass'] = 0;
	    $subTotal['high']['join'] = 0;
	    $subTotal['high']['fail'] = 0;	    
	    $subTotal['high']['noData'] = 0;
	    $juniorSubTotalNum = 0;
	    $highSubTotalNum = 0;
	    
	    foreach((array)$class_level as $k=>$v) {
	        $numAllStudent = $stat_merit[$k]['NumStudent'] + $stat_nomerit[$k]['NumStudent'] + $stat_good[$k]['NumStudent'] + $stat_pass[$k]['NumStudent'] + $stat_join[$k]['NumStudent'] + $stat_fail[$k]['NumStudent'];
	        
	        $numNoData = 0;      // default number of student that haven't imported data
	        
	        if ($v['NumStudent'] > 0) {
	            $percent_merit = number_format($stat_merit[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	            $percent_nomerit = number_format($stat_nomerit[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	            $percent_good = number_format($stat_good[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	            $percent_pass = number_format($stat_pass[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	            $percent_join = number_format($stat_join[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	            $percent_fail = number_format($stat_fail[$k]['NumStudent']/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	            $numNoData = $v['NumStudent'] - $numAllStudent;
	            $percent_noData = number_format($numNoData/$v['NumStudent'] * 100,0).$conf['PercentSymbol'];
	            
	            if ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'junior') {
	                $juniorSubTotalNum += $v['NumStudent'];
	                $subTotal['junior']['merit'] += $stat_merit[$k]['NumStudent'];
	                $subTotal['junior']['nomerit'] += $stat_nomerit[$k]['NumStudent'];
	                $subTotal['junior']['good'] += $stat_good[$k]['NumStudent'];
	                $subTotal['junior']['pass'] += $stat_pass[$k]['NumStudent'];
	                $subTotal['junior']['join'] += $stat_join[$k]['NumStudent'];
	                $subTotal['junior']['fail'] += $stat_fail[$k]['NumStudent'];
	                $subTotal['junior']['noData'] = $numNoData;
	            }
	            elseif ($this->getMajorClassLevel($v['WEBSAMSCode']) == 'high') {
	                $highSubTotalNum += $v['NumStudent'];
	                $subTotal['high']['merit'] += $stat_merit[$k]['NumStudent'];
	                $subTotal['high']['nomerit'] += $stat_nomerit[$k]['NumStudent'];
	                $subTotal['high']['good'] += $stat_good[$k]['NumStudent'];
	                $subTotal['high']['pass'] += $stat_pass[$k]['NumStudent'];
	                $subTotal['high']['join'] += $stat_join[$k]['NumStudent'];
	                $subTotal['high']['fail'] += $stat_fail[$k]['NumStudent'];
	                $subTotal['high']['noData'] = $numNoData;
	            }
	        }
	        else {
	            $percent_merit = '0'.$conf['PercentSymbol'];
	            $percent_nomerit = '0'.$conf['PercentSymbol'];
	            $percent_good = '0'.$conf['PercentSymbol'];
	            $percent_pass = '0'.$conf['PercentSymbol'];
	            $percent_join = '0'.$conf['PercentSymbol'];
	            $percent_fail = '0'.$conf['PercentSymbol'];
	            $percent_noData= '0'.$conf['PercentSymbol'];
	        }
	        
	        $class_level[$k]['NumMerit'] = $stat_merit[$k]['NumStudent'];
	        $class_level[$k]['PercentMerit'] = $percent_merit;
	        $class_level[$k]['NumNoMerit'] = $stat_nomerit[$k]['NumStudent'];
	        $class_level[$k]['PercentNoMerit'] = $percent_nomerit;
	        $class_level[$k]['NumGood'] = $stat_good[$k]['NumStudent'];
	        $class_level[$k]['PercentGood'] = $percent_good;
	        $class_level[$k]['NumPass'] = $stat_pass[$k]['NumStudent'];
	        $class_level[$k]['PercentPass'] = $percent_pass;
	        $class_level[$k]['NumJoin'] = $stat_join[$k]['NumStudent'];
	        $class_level[$k]['PercentJoin'] = $percent_join;
	        $class_level[$k]['NumFail'] = $stat_fail[$k]['NumStudent'];
	        $class_level[$k]['PercentFail'] = $percent_fail;
	        $class_level[$k]['NumNoData'] = $numNoData;
	        $class_level[$k]['PercentNoData'] = $percent_noData;
	    }
	    
	    
	    // second last row
	    $majorLevel = 'junior';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['JuniorSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $juniorSubTotalNum;
	    $class_level[$majorLevel]['NumMerit'] = $subTotal[$majorLevel]['merit'];
	    $class_level[$majorLevel]['NumNoMerit'] = $subTotal[$majorLevel]['nomerit'];
	    $class_level[$majorLevel]['NumGood'] = $subTotal[$majorLevel]['good'];
	    $class_level[$majorLevel]['NumPass'] = $subTotal[$majorLevel]['pass'];
	    $class_level[$majorLevel]['NumJoin'] = $subTotal[$majorLevel]['join'];
	    $class_level[$majorLevel]['NumFail'] = $subTotal[$majorLevel]['fail'];
	    $class_level[$majorLevel]['NumNoData'] = $juniorSubTotalNum - $subTotal[$majorLevel]['merit'] - $subTotal[$majorLevel]['nomerit'] - $subTotal[$majorLevel]['good'] - $subTotal[$majorLevel]['pass'] - $subTotal[$majorLevel]['join'] - $subTotal[$majorLevel]['fail'];
	    if ($juniorSubTotalNum > 0) {
	        $class_level[$majorLevel]['PercentMerit'] = number_format($subTotal[$majorLevel]['merit']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoMerit'] = number_format($subTotal[$majorLevel]['nomerit']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentGood'] = number_format($subTotal[$majorLevel]['good']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentPass'] = number_format($subTotal[$majorLevel]['pass']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentJoin'] = number_format($subTotal[$majorLevel]['join']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = number_format($subTotal[$majorLevel]['fail']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = number_format($class_level[$majorLevel]['NumNoData']/$juniorSubTotalNum * 100,0).$conf['PercentSymbol'];
	    }
	    else {
	        $class_level[$majorLevel]['PercentMerit'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoMerit'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentGood'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentPass'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentJoin'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	    }
	    
	    // last row
	    $majorLevel = 'high';
	    $class_level[$majorLevel]['Name'] = $Lang['iPortfolio']['JinYing']['Statistics']['HighSecondary'];
	    $class_level[$majorLevel]['NumStudent'] = $highSubTotalNum;
	    $class_level[$majorLevel]['NumMerit'] = $subTotal[$majorLevel]['merit'];
	    $class_level[$majorLevel]['NumNoMerit'] = $subTotal[$majorLevel]['nomerit'];	    
	    $class_level[$majorLevel]['NumGood'] = $subTotal[$majorLevel]['good'];
	    $class_level[$majorLevel]['NumPass'] = $subTotal[$majorLevel]['pass'];
	    $class_level[$majorLevel]['NumJoin'] = $subTotal[$majorLevel]['join'];
	    $class_level[$majorLevel]['NumFail'] = $subTotal[$majorLevel]['fail'];
	    $class_level[$majorLevel]['NumNoData'] = $highSubTotalNum - $subTotal[$majorLevel]['merit'] - $subTotal[$majorLevel]['nomerit'] - $subTotal[$majorLevel]['good'] - $subTotal[$majorLevel]['pass'] - $subTotal[$majorLevel]['join'] - $subTotal[$majorLevel]['fail'];
	    if ($highSubTotalNum> 0) {
	        $class_level[$majorLevel]['PercentMerit'] = number_format($subTotal[$majorLevel]['merit']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoMerit'] = number_format($subTotal[$majorLevel]['nomerit']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentGood'] = number_format($subTotal[$majorLevel]['good']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentPass'] = number_format($subTotal[$majorLevel]['pass']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentJoin'] = number_format($subTotal[$majorLevel]['join']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = number_format($subTotal[$majorLevel]['fail']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = number_format($class_level[$majorLevel]['NumNoData']/$highSubTotalNum * 100,0).$conf['PercentSymbol'];
	    }
	    else {
	        $class_level[$majorLevel]['PercentMerit'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoMerit'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentGood'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentPass'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentJoin'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentFail'] = '0'.$conf['PercentSymbol'];
	        $class_level[$majorLevel]['PercentNoData'] = '0'.$conf['PercentSymbol'];
	    }
	    
	    return $class_level;
	}

	public function getMedalStatByItem($itemCode, $semester='')
	{
	    global $Lang;
	    
	    switch ($itemCode){
	        case 'BSQ-HW':
	        case 'BSQ-PC':
            case 'EPW-PW':
            case 'BTP-IS':
	            $result = $this->getPerformanceGoodPassFail($itemCode, $semester);
	            break;
	            
	        case 'BSQ-AD':
	            $result = $this->getPerformanceGoodFail($itemCode, $semester);
	            break;
	            
	        case 'BSQ-AI':
	        case 'BSQ-RC':
	        case 'BSQ-RE':
	        case 'EPW-CA':
            case 'EPW-OC':
	            $result = $this->getPerformancePassFail($itemCode, $semester);
	            break;
	            
	        case 'EPW-PY':
	        case 'EPW-PJ':
	        case 'EPW-PS':
            case 'BTP-TP':
	            $result = $this->getPerformancePassFail($itemCode);
	            break;
	            
	        case 'BSQ-EC':
	        case 'BSQ-TA':
	        case 'EPW-TA':
	        case 'BTP-EA':
	            $result= $this->getPerformanceGoodPassFailNotJoin($itemCode);
	            break;
	            
	        case 'OFS-SI':
	        case 'OFS-SO':
	            $result= $this->getPerformanceService($itemCode);
	            break;
	            
	        case 'BTP-IC':
	            $result= $this->getPerformanceExternalCompetitions($itemCode);
	            break;
	    }
	    
	    return $result;
	}
	
//-- End of Class
}


?>