<?
/*
 * 	Need to include iportfolio_lang file before including this file
 *
 *  2019-12-04 Cameron
 *      - modify $conf['TargetMet']
 *      - add $conf['ScopeRequirement']
 *      - disable $conf['MedalRequirement']
 *
 *  2019-12-03 Cameron
 *      - change JinYingItemCodeWithTerm from two chars to six chars including scope
 */

## GoodPoint and TargetMet to get award of medal
$conf['GoodPoint']['BSQ'] = array('BSQ-HW-P-01','BSQ-PC-P-01','BSQ-AD-P-01');
$conf['TargetMet']['BSQ'] = array('BSQ-HW-P-02','BSQ-PC-P-02','BSQ-AI-P-01','BSQ-RC-P-01','BSQ-RE-P-01','BSQ-EC-P-01','BSQ-EC-P-02','BSQ-TA-P-01','BSQ-TA-P-02');
$conf['TargetMet']['EPW'] = array('EPW-PY-P-01','EPW-PJ-P-01','EPW-PS-P-01','EPW-PW-P-01','EPW-PW-P-02','EPW-CA-P-01','EPW-TA-P-01','EPW-TA-P-02','EPW-OC-P-01');
$conf['TargetMet']['BTP'] = array('BTP-EA-P-01','BTP-IS-P-01','BTP-IC-P-01');
$conf['MaxServiceGP'] = 5;	// max number of good points
$conf['GPExRate'] = 10;		// good point exchange rate: 1 good points = 10 service hours
$conf['JinYingItemCodeWithTerm'] = array('BSQ-HW','BSQ-PC','BSQ-AD','BSQ-AI','BSQ-RC','BSQ-RE','EPW-PW','EPW-CA','EPW-OC');
$conf['NoDataSymbol'] = '/';
$conf['PercentSymbol'] = '%';

## statistics tab, name / linking
if (count($Lang['iPortfolio']['JinYing']['Statistics']['Tab'])) {
    $conf['StatusticsAnalysis']['Tab']['ByReceive'] = array($Lang['iPortfolio']['JinYing']['Statistics']['Tab']['ByReceive'], "statistics.php");
    $conf['StatusticsAnalysis']['Tab']['ByScope'] = array($Lang['iPortfolio']['JinYing']['Statistics']['Tab']['ByScope'], "statisticsByScope.php");
    $conf['StatusticsAnalysis']['Tab']['ByItem'] = array($Lang['iPortfolio']['JinYing']['Statistics']['Tab']['ByItem'], "statisticsByItem.php");
}
else {
    $conf['StatusticsAnalysis']['Tab']['ByReceive'] = array("Percentage of award by class level","statistics.php");
    $conf['StatusticsAnalysis']['Tab']['ByScope'] = array("Percentage of award by scope","statisticsByScope.php");
    $conf['StatusticsAnalysis']['Tab']['ByItem'] = array("Percentage of award by item","statisticsByItem.php");
}

// minimum requirement for medal award <- void after 2019-12-04 [case#J161484]
/*
$conf['MedalRequirement']['Gold']['BSQ']['GoodPoint'] = 3;
$conf['MedalRequirement']['Gold']['BSQ']['TargetMet'] = 6;
$conf['MedalRequirement']['Gold']['EPW']['TargetMet'] = 4;
$conf['MedalRequirement']['Gold']['OFS']['GoodPoint'] = 5;
$conf['MedalRequirement']['Gold']['BTP']['GoodPoint'] = 1;
$conf['MedalRequirement']['Gold']['BTP']['TargetMet'] = 2;

$conf['MedalRequirement']['Silver']['BSQ']['GoodPoint'] = 2;
$conf['MedalRequirement']['Silver']['BSQ']['TargetMet'] = 4;
$conf['MedalRequirement']['Silver']['EPW']['TargetMet'] = 3;
$conf['MedalRequirement']['Silver']['OFS']['GoodPoint'] = 3;
$conf['MedalRequirement']['Silver']['BTP']['GoodPoint'] = 0;
$conf['MedalRequirement']['Silver']['BTP']['TargetMet'] = 2;

$conf['MedalRequirement']['Bronze']['BSQ']['GoodPoint'] = 1;
$conf['MedalRequirement']['Bronze']['BSQ']['TargetMet'] = 2;
$conf['MedalRequirement']['Bronze']['EPW']['TargetMet'] = 2;
$conf['MedalRequirement']['Bronze']['OFS']['GoodPoint'] = 1;
$conf['MedalRequirement']['Bronze']['BTP']['GoodPoint'] = 0;
$conf['MedalRequirement']['Bronze']['BTP']['TargetMet'] = 1;
*/

// scope requirement for medal award
$conf['ScopeRequirement']['BSQ']['GoodPoint'] = 2;
$conf['ScopeRequirement']['BSQ']['TargetMet'] = 4;
$conf['ScopeRequirement']['EPW']['TargetMet'] = 2;
$conf['ScopeRequirement']['OFS']['GoodPoint'] = 2;
$conf['ScopeRequirement']['BTP']['GoodPoint'] = 1;
$conf['ScopeRequirement']['BTP']['TargetMet'] = 1;

define("JINYING_PERFORMANCE_WITHOUT_PARTICIPATION",	    1);
define("JINYING_PERFORMANCE_FAIL",                      2);
define("JINYING_PERFORMANCE_PARTICIPATION",	            3);
define("JINYING_PERFORMANCE_PASS",						4);
define("JINYING_PERFORMANCE_GOOD",						5);
define("JINYING_PERFORMANCE_AWARD_WITHOUT_MERIT",		6);
define("JINYING_PERFORMANCE_AWARD_WITH_MERIT",			7);

?>