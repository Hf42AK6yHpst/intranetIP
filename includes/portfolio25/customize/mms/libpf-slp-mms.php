<?php

class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $academcYearDisplay; // store the academic Year display in the student Info
	private $uid;
	private $cfgValue;


	public function studentInfo($uid){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid;
		$this->cfgValue = $ipf_cfg;

	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	private function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function getUid(){
		return $this->uid;
	}
	/*************************/
	/*******STUDENT INFO******/
	/*************************/
	public function getStudentInfo_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	

	private function getStudentInfo(){
		$sql = "select " .
					"EnglishName as 'studentName'," .
					"concat(ClassName,'',if(classnumber is null or trim(classnumber) = '','',concat(' ',classnumber,' '))) as 'classNameNo'".
				"from " .
					$this->intranet_db.".INTRANET_USER " .
				"where " .
					"userid =".$this->uid;
				
		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result){
		
		$str .= "<table width=\"100%\">";
		$str .= "<tr><td width = \"150\">Name of Student:</td><td class=\"bottomline ArialCSS\" align=\"center\"><b>".$result[0]["studentName"]."</b>&nbsp</td><td width =\"150\">Class(Number):</td><td class =\"bottomline ArialCSS\" align=\"center\"><b>".$result[0]["classNameNo"]."</b>&nbsp</td></tr>";
		$str .= "<tr><td colspan=\"4\" background=\"/images/2009a/eOffice/scissors_line.gif\">&nbsp;</td></tr>";
		$str .= "</table>";
		
		return $str;	
	}

	/*****************/
	/*** GET PART A **/
	/*****************/
	public function getPartA_HTML(){
		$result = $this->getPartA();
		$html = $this->getPartA_Display($result);
		return $html;
	}


	private function getPartA(){
		$result = $this->getOLE($this->cfgValue["OLE_TYPE_STR"]["EXT"]);
		return $result;
	}
	
	private function getPartA_Display($result){
		$html = $this->getOLEExt_Display($result);
		return $html;
	}
	
	private function getOLEExt_Display($data){
		


		$header="Performances / Awards and Key Participation Outside School";
		$desc = "Information on learning programmes not organised by the school is provided by the student herself. <b>The school does not validate the information, and the student holds full responsibility for providing evidence to the relevant parties as appropriate.<b/>";

		$displayHeader = $header;
		$displayDesc = "";		
		$display = 	"<col width= \"160\"/>".
					"<col width= \"130\"/>".
					"<col width= \"150\"/>".
					"<col width= \"150\"/>".
					"<col width= \"150\"/>".
		$display .= "<tr>".
					"<td class=\"styleA\" align =\"center\"><b>Programme</b></td>".
					"<td class=\"styleA\" align =\"center\"><b>Role</b></td>".
					"<td class=\"styleA\" align =\"center\"><b>Organisation</b></td>".
					"<td class=\"styleA\" align =\"center\"><b>Achievement</b></td>".
					"<td  align =\"center\" class=\"styleA\" style=\"border-right-width: 1px; border-right-style: solid;	border-right-color: #000000;\"><b>Remark</b></td>".
					"</tr>";

			$resultHtml = "";

			for($i =0,$i_max= sizeof($data);$i<$i_max;$i++){
				$_title = $data[$i]["title"];
				$_role = $data[$i]["Role"];
				$_org = $data[$i]["Organization"];
				$_Achievement = $data[$i]["Achievement"];
				$_Details = $data[$i]["Details"];


	
				$resultHtml .="<tr>".
						"<td align =\"center\">{$_title}&nbsp;</td>".
						"<td align =\"center\">{$_role}&nbsp;</td>".
						"<td align =\"center\">{$_org}&nbsp;</td>".
						"<td align =\"center\">{$_Achievement}&nbsp;</td>".
						"<td align =\"center\" style=\"border-right-width: 1px; border-right-style: solid;	border-right-color: #000000;\">{$_Details}&nbsp;</td>".
						"</tr>";	
			}

			if(trim($resultHtml) == ""){

				$resultHtml = "<tr>".
						   "<td colspan=\"6\" align=\"center\">No Record</td>".
				           "</tr>";
			}
			$display .= $resultHtml;			


		$html = "<table border =\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" class=\"ole \">".$display."</table>";

		return $this->getSectionHTML($html,$header,$desc);

	}

	private function getOLE($oleType,$createBy = null){

		
//		debug_r($this->getAcademicYear());
		$requestYear = $this->getAcademicYear();
		if(is_array($requestYear) && sizeof($requestYear) > 0){
			$requestYearStr = implode(",",$requestYear);
			$cond_requestYear = " and op.AcademicYearID in ({$requestYearStr})";
		}
		
		$cond_ext = " and op.INTEXT ='{$oleType}' ";
//		$cond_CreatedBY = (trim($createBy)=="") ?"":" and op.PROGRAMTYPE = '{$createBy}' "; 
		$cond_2 = ' and (SLPOrder != null  or SLPOrder != 0)';
		//common sql for both INT / EXT
		$sql = "select " .
						"op.title , " .
						"os.Role,".
						"op.Organization,".
						"os.Achievement, ".
						"os.Details " .
					"from " .
						$this->eclass_db.".OLE_STUDENT as os " .
						"inner join " .$this->eclass_db.".OLE_PROGRAM as op on os.programid = op.programid ".
					"where " .
						"os.userid = ".$this->getUid()." and ".
						"os.RecordStatus = ".$this->cfgValue["OLE_STUDENT_RecordStatus"]["approved"]." ".
						$cond_ext.
						$cond_requestYear.
						$cond_2.
					" order by op.StartDate desc, op.title desc";

			$result = $this->objDB->returnArray($sql);	 				
		return $result;	
	}
	


	private function getSectionHTML($content,$header="",$desc=""){
			$returnStr  = "<table width = \"100%\" border =\"0\" class=\"section\" cellpadding=\"0\" cellspacing=\"0\" >".			
	                      "<tr><td class=\"styleA timesNewRoman bottomline2\" ><b>".$header."</b></td></tr>";

			$returnStr .= "<tr><td class=\"timesNewRoman\">".$desc."</td></tr>";
			$returnStr .= "</table>";
			$returnStr .= $content;


//			$returnStr = "<table width = \"100%\" border =\"0\" class=\"section\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td>".$returnStr."</td></td></table>";
			return $returnStr;
	}
	
	public function setAcademicYearDisplay($academcYearDisplay){
		$this->academcYearDisplay = $academcYearDisplay;
	}
	public function getAcademicYearDisplay(){
		return $this->academcYearDisplay;
	}
	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
}
?>