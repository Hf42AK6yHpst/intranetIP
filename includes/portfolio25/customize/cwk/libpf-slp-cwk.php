<?php

include_once($intranet_root."/includes/libdb.php");

class libpf_slp_cwk
{
  private $db;

  function __construct() {
    $this->db = new libdb();
  }

  function createActAbility_temp(){
    global $eclass_db;
  
    $tempTableName = "tempActivityAbility";
  
    $sql = "CREATE TEMPORARY TABLE {$tempTableName} ";
    $sql .= "SELECT caa.ActivityName, GROUP_CONCAT(cca.AbilityCode separator ', ') AS AbilityCode ";
    $sql .= "FROM {$eclass_db}.CWK_ACTIVITY_ABILITY AS caa ";
    $sql .= "LEFT JOIN {$eclass_db}.CWK_COMMON_ABILITY AS cca ";
    $sql .= "ON caa.AbilityID = cca.AbilityID ";
    $sql .= "GROUP BY caa.ActivityName";
    $this->db->db_db_query($sql);
    
    return $tempTableName;
  }
  
  function createActCriteria_temp(){
    global $eclass_db;
    
    $tempTableName = "tempActivityCriteria";
    
    $sql = "CREATE TEMPORARY TABLE {$tempTableName} ";
    $sql .= "SELECT cac.ActivityName, GROUP_CONCAT(ele.DefaultID separator ', ') AS CriteriaCode ";
    $sql .= "FROM {$eclass_db}.CWK_ACTIVITY_CRITERIA AS cac ";
    $sql .= "LEFT JOIN {$eclass_db}.OLE_ELE AS ele ";
    $sql .= "ON cac.ComponentID = ele.RecordID ";
    $sql .= "GROUP BY cac.ActivityName";
    $this->db->db_db_query($sql);
  
    return $tempTableName;
  }

  function createProgramAbility_temp(){
    global $eclass_db;
  
    $tempTableName = "tempProgramAbility";
  
    $sql = "CREATE TEMPORARY TABLE {$tempTableName} ";
    $sql .= "SELECT copa.ProgramID, GROUP_CONCAT(cca.AbilityCode separator ', ') AS AbilityCode ";
    $sql .= "FROM {$eclass_db}.CWK_OLE_PROGRAM_ABILITY AS copa ";
    $sql .= "LEFT JOIN {$eclass_db}.CWK_COMMON_ABILITY AS cca ";
    $sql .= "ON copa.AbilityID = cca.AbilityID ";
    $sql .= "GROUP BY copa.ProgramID";
    $this->db->db_db_query($sql);
    
    return $tempTableName;
  }
}

?>