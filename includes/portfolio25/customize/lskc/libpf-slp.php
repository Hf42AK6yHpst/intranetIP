<?php
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	
	private $YearClassID;
	
	private $AcademicYearStr;
	
	private $globalCounter;
	
	private $PageNum;
	private $hasNextPageOLE;
	private $NextPageOLE_html;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 

		$academicYearIDArr = array_filter($academicYearIDArr);

		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;
		
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '---';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#D0D0D0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->SchoolTitleEn = 'The HKFYG Lee Shau Kee College';
		$this->SchoolTitleCh = '香港青年協會李兆基書院';
		
		$this->globalCounter = 0;
		
		$this->PageNum = 1;
		$this->hasNextPageOLE = false;
		$this->NextPageOLE_html = '';
	}
	
	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getNextPageOLE_html(){
		return $this->NextPageOLE_html;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function getHasNextPageOLE(){
		return $this->hasNextPageOLE;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function addPageNum(){
		$this->PageNum ++;
	}
	public function getPageNum(){
		return $this->PageNum;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}



	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '19';

		$header_title = "Student Learning Portfolio (SLP) ";
		
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:89%; text-align:center; border:0px;">';
			$x .= '<tr><td style="font-size:'.$header_font_size.'px;"><b>'.$header_title.'</b></td></tr>';
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						ChineseName
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";

		$result = $this->objDB->returnResultSet($sql);
		return $result;
	}
	
	
	private function getStudentInfoDisplay($result)
	{		
		$fontSize = 'font-size:14px';
		
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		
		$_borderBottom_dot =  'border-bottom: 1px dashed black; ';


		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;
		


		$html = '';
		$html .= '<table width="89%" cellpadding="4" border="0px" align="center" style="border-collapse: collapse;">
					<col width="17%" />
				    <col width="33%" />
					<col width="17%" />
					<col width="33%" />

					<tr>
							<td ><font style="'.$fontSize.'"><b>English Name :</b></font></td>				
							<td ><font style="'.$fontSize.'">'.$EngName.'</font></td>

							<td ><font style="'.$fontSize.'"><b>Chinese Name :</b></font></td>
							<td ><font style="'.$fontSize.'">'.$ChiName.'</font></td>
					</tr>
					';

		$html .= '</table>';
	
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	
	/***************** Part 3 : Membership******************/	
	public function getPart3_HTML(){
		
		$Part3_Title = 'Membership';
		
		$titleArray = array();	
		$titleArray[] = 'Club / Team';
		$titleArray[] = 'Post';
		$titleArray[] = 'Year';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="45%" />';
		$colWidthArr[] = '<col width="35%" />';
		$colWidthArr[] = '<col width="20%" />';
		
		$DataArray = $this->getMembership();

		$this->globalCounter +=count($DataArray);
		
		if(count($DataArray)==0)
		{
			$this->globalCounter +=1;
		}

		$Page_break='page-break-after:always;';
		$table_style = 'border-collapse: collapse; '; 
		
		$html = '';
		
		$html .= $this->getPartContentDisplay_NoRowLimit('3',$Part3_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false);
		
		
		return $html;	
	}		
	
	private function getMembership()
	{
		$ACTIVITY_STUDENT = $this->Get_eClassDB_Table_Name('ACTIVITY_STUDENT');
		$ACADEMIC_YEAR = $this->Get_IntranetDB_Table_Name('ACADEMIC_YEAR');
		$_UserID = $this->uid;
		
		$roleAry = array();
		$roleAry[] = 'Chairman';
        $roleAry[] = 'Captain';
		$roleAry[] = 'Head Prefect';
		$roleAry[] = 'Prefect';
		$roleAry[] = 'Vice-chairman';
		$roleAry[] = 'Vice Chairman';		
		$roleAry[] = 'Vice-captain';
		$roleAry[] = 'Member';	
		$roleAry[] = '';	

//		$cond_role_specifc = ' and s.role in ('.implode(',',$roleAry).')';

		if($_UserID!='')
		{
			$cond_studentID = "And UserID ='$_UserID'";
		}
		
		$sql = "select 
						ActivityName,
						Role,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' 
			    from 
						$ACTIVITY_STUDENT s 
				inner join 
						$ACADEMIC_YEAR as y 
				on
						y.AcademicYearID = s.AcademicYearID
				where 
						1
						$cond_studentID
						$cond_role_specifc
				order by
						y.sequence desc
				limit 4
				";



		$resultArr = $this->objDB->returnResultSet($sql);
		$resultAry = array();
		$resultOtherAry = array();
		$lastResultAry = array();

		//urgent fix , may need to rewrite the mechanism;
		for($i = 0, $i_max = count($resultArr); $i < $i_max; $i++){
			$recordFind = false;
			$_activityName = $resultArr[$i]['ActivityName'];
			$_role = $resultArr[$i]['Role'];
			$_yearName = $resultArr[$i]['YEAR_NAME'];

			for($j = 0, $j_max = count($roleAry); $j < $j_max; $j++){
				$_defaultRole = $roleAry[$j];

				if(trim(strtolower($_defaultRole)) == trim(strtolower($_role))){

					$resultAry[$_defaultRole][] = array('ActivityName' => $_activityName,'Role'=>$_role,'YEAR_NAME'=>$_yearName);
					$recordFind = true;
					break;
				}
			}
			if($recordFind == false){
				$resultOtherAry[] = array('ActivityName' => $_activityName,'Role'=>$_role,'YEAR_NAME'=>$_yearName);
			}
		}


		$roleOrderArray = array();

		for($i = 0, $i_max = count($roleAry);$i < $i_max; $i++){
			$_defaultRole = $roleAry[$i];

			if(count($resultAry[$_defaultRole]) > 0){
				for($j = 0,$j_max = count($resultAry[$_defaultRole]);$j < $j_max; $j++){
					$roleOrderArray[] = $resultAry[$_defaultRole][$j];
				}
			}

		}



		if(count($resultArr) ==0){
				$lastResultAry = NULL;
		}else{
			if(count($roleOrderArray) > 0 && count($resultOtherAry) > 0){
				$lastResultAry  = array_merge($roleOrderArray,$resultOtherAry);
			}else{
				if(count($resultAry) >0){
					$lastResultAry = $roleOrderArray;
				}else{
					$lastResultAry = $resultOtherAry;
				}
			}
		}

		/*
		verify sql 
		select u.classname , u.classnumber , i.ActivityName as 'ip_name', i.Role as 'ip_role', p.ActivityName  as 'ipf_name',p.role as 'ipf_role'
 from intranet.PROFILE_STUDENT_ACTIVITY as i left join eclass.ACTIVITY_STUDENT as p on i.ActivityName = p.ActivityName  and i.userid = p.userid left join 
 intranet.INTRANET_USER as u on u.userid = i.userid 
 where i.userid in (select userid from INTRANET_USER where classname like '%6%' order by classname , classnumber) order by u.classname , u.classnumber ;
 
		*/
		return $lastResultAry;
	}


	/***************** End Part 3 ******************/
	
	
	
	
	
	/***************** Part 4 : Awards / Scholarship******************/	
	public function getPart4_HTML(){
		
		$Part4_Title = 'Awards / Scholarships';
		
		
		$titleArray = array();	
		$titleArray[] = 'Awards / Scholarships';
		$titleArray[] = 'Year';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="70%" />';
		$colWidthArr[] = '<col width="30%" />';
		
		$DataArray = array();
		$DataArray = $this->getAward_Info();
		
		$this->globalCounter +=count($DataArray);
		
		if(count($DataArray)==0)
		{
			$this->globalCounter +=1;
		}

		$html = '';
	
		$html .= $this->getPartContentDisplay_NoRowLimit('4',$Part4_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false);
		
		return $html;
		
	}	
	
	private function getAward_Info(){
		
		global $ipf_cfg;
		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		$ACADEMIC_YEAR = $this->Get_IntranetDB_Table_Name('ACADEMIC_YEAR');
		
		$_UserID = $this->uid;
		
		if($_UserID!='')
		{
			$cond_studentID = "And os.UserID = '{$_UserID}' ";
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And op.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."') ";
		}
		

		$sql = "Select
						AwardName,
						Semester,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' 
				From
						$Award_student as s
				inner join 
						$ACADEMIC_YEAR as y 
				on
						y.AcademicYearID = s.AcademicYearID
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence desc
				limit 6
				";

		$ole_program = $this->Get_eClassDB_Table_Name('OLE_PROGRAM');
		$ole_student = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$YEAR_TERM = $this->Get_IntranetDB_Table_Name('ACADEMIC_YEAR_TERM');
		

		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";

				$sql = 'select 
						op.title as \'AwardName\',
						y.'.Get_Lang_Selection('YearNameB5','YearNameEN').' as \'YEAR_NAME\' ,
						t.'.Get_Lang_Selection('YearTermNameB5','YearTermNameEN').' as \'Semester\'
				from 
					'.$ole_student.' as os 
					inner join 
						'.$ole_program.' as op  on os.ProgramID= op.ProgramID
					inner join 
						'.$ACADEMIC_YEAR.' as y   on y.AcademicYearID = op.AcademicYearID
					inner join 
						'.$YEAR_TERM.' as t  on t.YearTermID = op.YearTermID
					where 1
					'.$cond_studentID.' 
					'.$cond_academicYearID.'
					and os.SLPOrder >=0 
					and os.RecordStatus in('.$approve.','.$teacherSubmit.')
					and op.IntExt = \'ext\'
				order by 
				y.Sequence desc 
				limit 6

				';

		$roleData = $this->objDB->returnResultSet($sql);
		
		$returnDate=array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{								
			$returnDate[$i]['AwardName'] =$roleData[$i]['AwardName'];
			$returnDate[$i]['YEAR_NAME'] =$roleData[$i]['YEAR_NAME'].' '.$roleData[$i]['Semester'];			
		}

		return $returnDate;
	}
	
	/***************** End Part 4 ******************/
	
	
	
	/***************** Part 5 : Other Learning Experiences******************/	
	public function getPart5_HTML($OLEInfoArr_INT){
		
		$Part5_Title = 'Other Learning Experiences';
		$Part5_Remark = '&nbsp;';
		
		$titleArray = array();
		$titleArray[] = 'Programme';
		$titleArray[] = 'Organization';	
		$titleArray[] = 'Major Component(s)<br/>of OLE';
		$titleArray[] = 'Year';		
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="35%" />';
		$colWidthArr[] = '<col width="25%" />';
		$colWidthArr[] = '<col width="27%" />';
		$colWidthArr[] = '<col width="13%" />';
		
		$DataArr = array();
		$DataArray = $OLEInfoArr_INT[$this->uid]['INT'];
		

		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('5',$Part5_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=true);
		
		return $html;
		
	}

	/***************** End Part 5 ******************/
	
	
	
	
	/***************** Part 6 : Student's Self-Account******************/
	
	public function getPart6_HTML(){
		
		$Part7_Title = "Student's Self-Account";
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$Part7_Title) ;
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$_UserID = $this->uid;
		
		if($_UserID!='')
		{
			$cond_studentID = "And UserID ='$_UserID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnResultSet($sql);
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$MainTitle='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:12px;" '; 


		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];
			
			//$Detail = strip_tags($Detail,'<p>');
			
			$Str_Replace_Arr = array();
			$Str_Replace_Arr[] = '<p class="MsoNormal"></p>';
			$Str_Replace_Arr[] = '<p class="MsoNormal"/>';
			$Str_Replace_Arr[] = '<p class="MsoNormal">&nbsp;</p>';
			$Str_Replace_Arr[] = '<p class="MsoNormal" style="margin-top:36.0pt;mso-para-margin-top:2.0gd;line-height:200%">&nbsp;</p>';
			$Str_Replace_Arr[] = '<p class="MsoNormal" style="line-height: 200%; margin-top: 36pt;">&nbsp;</p>';
			$Str_Replace_Arr[] = '<p class="MsoNormal" style="line-height: 200%; margin-top: 36pt;"></p>';
			$Str_Replace_Arr[] = '<p style="text-align:justify;text-justify:inter-ideograph;line-height:200%" class="MsoNormal">&nbsp;</p>';
			$Detail = str_replace($Str_Replace_Arr,'<br/>',$Detail);
		}
		else
		{
			$Detail = 'No Record Found.';
		}	
		
		$html .= '<table width="89%" height=""  cellpadding="7" border="0px" align="center" '.$table_style.'>
					<tr>				 							
						<td style="font-size:15px;height:30px;vertical-align: top;"><b>'.$MainTitle.'</b></td>						 														
					</tr>
					<tr>				 							
						<td style="vertical-align: top;text-align:justify;text-justify:inter-ideograph;'.$_borderTop.$_borderBottom.'">'.$Detail.'</td>						 														
					</tr>
				  </table>';	
		
			
		return $html;
	}
	
	
	/***************** End Part 6 ******************/
	
	
	
	/***************** Part 7 : Footer  ******************/
	public function getPart7_HTML($PageBreak='')
	{
		$PrincipalStr = "Principal";
		$ClassTeacherStr = "Class Teacher";
		$SchoolChopStr = "School Chop";
		$DateOfIssueStr = "Date of Issue";
		
		$_borderTop =  'border-top: 1px solid black; ';
		
		$IssueDate = $this->issuedate;
		
		if (date($IssueDate)==0) {
			$IssueDate = $this->EmptySymbol;	
		} else {
			$IssueDate = date('d/m/Y',strtotime($IssueDate));
		}
		$html = '';
		
		$html .= '<table width="89%" cellpadding="0" border="0px" align="center" style="font-size:11px;'.$PageBreak.'">		
					<col width="24%" />
					<col width="1%" />
					<col width="24%" />
					<col width="1%" />
					<col width="24%" />
					<col width="1%" />
					<col width="24%" />

					<tr>					
					   	<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td align="center">'.$IssueDate.'</td>
					</tr>

				 	<tr>					
					    <td align="center" style="'.$_borderTop.'">'.$PrincipalStr.'</td>
						<td>&nbsp;</td>
						<td align="center" style="'.$_borderTop.'">'.$ClassTeacherStr.'</td>
						<td>&nbsp;</td>
						<td align="center" style="'.$_borderTop.'">'.$SchoolChopStr.'</td>
						<td>&nbsp;</td>
						<td align="center" style="'.$_borderTop.'">'.$DateOfIssueStr.'</td>
					</tr>

				  </table>';
		return $html;
		
	}
	/***************** End Part 7 ******************/

	
	
	
	private function getStringDisplay($String='',$font_size='10',$Page_break='') {
		
		$html = '';
		$html .= '<table width="89%" cellpadding="7" border="0px" align="center" style="font-size:'.$font_size.'px;" '.$Page_break.'>					
		   			 <tr>
					    <td><font face="Arial">'.$String.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}

	
	private function getPartContentDisplay_NoRowLimit($PartNum, $mainTitle,$subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false) 
	{
				
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		$BgColor = $this->TableTitleBgColor;
		
		$table_style = 'style="font-size:11px;border-collapse: collapse;"'; 
		
	
		$thisGlobalCount = 10;
		
		$subtitle_html = '';
		
		if(count($DataArr)==0)
		{
			return '';
		}
		
		
		$i=0;
		foreach((array)$subTitleArr as $key=>$_title)
		{
			$text_alignStr='center';
			
			$subtitle_html .='<td style="font-size:15px;text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'"><b>';
			$subtitle_html .=$_title;
			$subtitle_html .='</b></td>';
			
			$i++;
		}	
		
		if($PartNum=='3')
		{							
			$rowHight = 30;		
		}
		else if($PartNum=='4')
		{					
			$rowHight = 30;		
		}
		else if ($PartNum=='5')
		{
			$rowHight = 30;	
		}
		
		$text_alignStr='center';
		$content_html='';	
		$rowMax_count = 0;
		
		$count_col =count($colWidthArr);
		
		if(count($DataArr)>0)
		{
			$rowMax_count = count($DataArr);
		}	
		else
		{
			$NoRecordStr = 'No Record Found.';
			$content_html = '<td colspan="'.$count_col.'" style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderTop.$_borderBottom.$_borderRight.$_borderLeft.'">'.$NoRecordStr.'</td>';
		}
		

	//	$this->globalCounter +=$rowMax_count;

		for ($i=0; $i<$rowMax_count; $i++)
		{		
			$content_html .= '<tr>';	
			
			if($PartNum=='5')
			{
				$thisCount = $i + $this->globalCounter;
				if($thisCount>$thisGlobalCount)
				{
					$content_html_2 .='<tr>';
				}
			}
			
				
			$j=0;		// $j is stated for column 
			foreach((array)$DataArr[$i] as $_title=>$_titleVal)
			{
				$_titleVal =  ($_titleVal=='')? $this->EmptySymbol:$_titleVal;		
				
				if($PartNum=='3')
				{
					$text_alignStr = 'left';
				}
				else if($PartNum=='4')
				{
					if($j==0 || $j==1)
					{
						$text_alignStr = 'left';
					}
					else
					{
						$text_alignStr = 'left';
					}
				}
				else if($PartNum=='5')
				{
					if($j==0)
					{
						$text_alignStr = 'left';
					}
					else
					{
						$text_alignStr = 'left';
					}
				}
				
				$thisContentHtml = ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderTop.$_borderLeft.$_borderRight.$_borderBottom.'">'.$_titleVal.'</td>';
				
				$thisCount = $i + $this->globalCounter;
				if($thisCount>$thisGlobalCount && $PartNum=='5')
				{
					$content_html_2 .= $thisContentHtml;
				}
				else
				{
					$content_html .= $thisContentHtml;
				}
															
				//$this->globalCounter +=count($DataArray);
				$j++;
			}	
						
			if($PartNum=='5')
			{
				$thisCount = $i + $this->globalCounter;
				if($thisCount>$thisGlobalCount)
				{
					$content_html_2 .='</tr>';
				}
			}
			$content_html .= '</tr>';		
		}		
		
		$colNumber = count($colWidthArr);
			
		/****************** Main Table ******************************/
		
		$html = '';
	
		$html .= '<table width="89%" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$html .=$_colWidthInfo;
					}
					
		$html .= '<tr> 
					<td colspan="'.$colNumber.'" style=" font-size:15px;text-align:left;vertical-align:text-top;"><b>
					'.$mainTitle.'
					</b></td>
				  </tr>';
		
		$html .= '<tr>'.$subtitle_html.'</tr>';
		
		$html .= $content_html;
		
		$html .='</table>';
		
		
		$thisCount =  $this->globalCounter + $rowMax_count;
		if($thisCount>$thisGlobalCount && $PartNum=='5' &&  $content_html_2!='')		
		{
			$this->hasNextPageOLE = true;
			
//			$this->NextPageOLE_html .=$this->getPart1_HTML();
//			$this->NextPageOLE_html .=$this->getEmptyTable('1%');
//			$this->NextPageOLE_html .=$this->getPart2_HTML();
//			$this->NextPageOLE_html .=$this->getEmptyTable('1%');
			$this->NextPageOLE_html .= '<table width="89%" height="" cellpadding="7" border="0px" align="center" style="font-size:11px;border-collapse: collapse;">';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$this->NextPageOLE_html .=$_colWidthInfo;
					}
			
			$this->NextPageOLE_html .= '<tr>'.$subtitle_html.'</tr>';
			
			$this->NextPageOLE_html .= $content_html_2;
			
			$this->NextPageOLE_html .='</table>';
		}
			
		return $html;
	}
	
	
	
	public function getOLEcomponentName()
	{
		$OLE_ELE = $this->Get_eClassDB_Table_Name('OLE_ELE');
		
		$sql="Select 
					EngTitle,
					ChiTitle,
					DefaultID 
			  from
					$OLE_ELE
			";
		$roleData = $this->objDB->returnResultSet($sql);
		
		$returnArray = array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$thisName_en = $roleData[$i]['EngTitle'];
			$thisName_ch = $roleData[$i]['ChiTitle'];
			$thisID = $roleData[$i]['DefaultID'];
			
			$returnArray[$thisID] = $thisName_en.' '.$thisName_ch;
		}
	
		return $returnArray;
	}
	
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false)
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		$ACADEMIC_YEAR = $this->Get_IntranetDB_Table_Name('ACADEMIC_YEAR');
		 		
		$OLEcomponentArr = $this->getOLEcomponentName();
	
		if($_UserID!='')
		{
			$cond_studentID = "And UserID ='$_UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = "And SLPOrder is not null"; 
		}
	
		if($IntExt!='')
		{
			$cond_IntExt = "And ole_prog.IntExt='".$IntExt."'";
		}
		
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = "And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
	
		
		$sql = "Select
						ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_prog.ELE as OleComponent
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID
				inner join 
						$ACADEMIC_YEAR as y 
				on
						y.AcademicYearID = ole_prog.AcademicYearID
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
				order by 
						y.Sequence desc,	
						ole_prog.Title asc		
				limit 10

				";
		$roleData = $this->objDB->returnResultSet($sql);
//hdebug_r($sql);
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$_StudentID = $roleData[$i]['StudentID'];
			$_IntExt = $roleData[$i]['IntExt'];
			$thisAcademicYear = $roleData[$i]['YEAR_NAME'];
			
			$thisOleComponentArr = explode(',',$roleData[$i]['OleComponent']);
		
			for($a=0,$a_MAX=count($thisOleComponentArr);$a<$a_MAX;$a++)
			{
				$thisOleComponentID = $thisOleComponentArr[$a];		
				$thisOleComponentArr[$a] = $OLEcomponentArr[$thisOleComponentID];
			}
			$thisOLEstr = implode("<br/>",(array)$thisOleComponentArr);

			$thisDataArr = array();
			
			if(strtoupper($IntExt)=='INT')
			{
				$thisDataArr['Program'] =$roleData[$i]['Title'];				
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];		
				$thisDataArr['OLEComponent'] =$thisOLEstr;
				$thisDataArr['SchoolYear'] =$thisAcademicYear;	
			}

			$returnDate[$_StudentID][$_IntExt][] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		$lastNo = count($returnArray)-1;
		
		$firstYear = substr($returnArray[0], 0, 4);
		$lastYear = substr($returnArray[$lastNo], 0, 4);
		
		$returnValue = $firstYear."-".$lastYear;
		
		return $returnValue;
		
	}
	
	private function getAcademicYearArr($academicYearIDArr)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	
	public function getEmptyTable($Height='',$Content='&nbsp;') {
		
		$html = '';
		$html .= '<table width="89%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td style="text-align:center"><font face="Arial">'.$Content.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="89%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>