<?php
class hktlcRptMgr extends ipfReportManager {
	public function hktlcRptMgr(){
		parent::ipfReportManager();
	}

	public function getIssueDate(){
		return date('d/m/Y', strtotime($this->issuedate));
	}
	
	public function getAcademicYearDisplayMultiple() {
		$yearIDStr = implode(',',$this->academicYearID);
		$sql = "select YearNameEN from ".$this->intranet_db.".ACADEMIC_YEAR where AcademicYearID in ({$yearIDStr}) order by Sequence desc";
		$AcademicYearArr = $this->objDB->returnVector($sql);
		
		$FirstYear = '';
		$LastYear = '';
		$numOfAcademicYear = count($AcademicYearArr);
		for ($i=0; $i<$numOfAcademicYear; $i++)
		{
			$thisAcademicYear = $AcademicYearArr[$i];
			$thisYearNameArr = explode('-', $thisAcademicYear);
			$thisYearStart = $thisYearNameArr[0];
			$thisYearEnd = $thisYearNameArr[1];
			
			if ($FirstYear == '' || $thisYearStart < $FirstYear)
				$FirstYear = $thisYearStart;
			if ($LastYear == '' || $thisYearEnd > $LastYear)
				$LastYear = $thisYearEnd;
		}
		
		return $FirstYear.'-'.$LastYear;
	}
}
?>