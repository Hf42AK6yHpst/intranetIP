<?php
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	
	private $YearClassID;	
	private $AcademicYearStr;

	private $PageNumber;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;
		
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '--';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#D0D0D0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->PageNumber =1;
		
		$this->SchoolTitleEn = 'HOLY TRINITY COLLEGE STUDNET LEARNING PROFILE';
		$this->SchoolTitleCh = '寶血會上智英文書院學生學習概覽';
	}
	
	public function setPageNo($PageNo)
	{
		$this->PageNumber = $PageNo;
	}
	public function getPageNo()
	{
		return $this->PageNumber;
	}
	
	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	
	private function getCurrentYearID()
	{
		$YearClassID = $this->YearClassID;
	
		$YEAR_CLASS = $this->Get_IntranetDB_Table_Name('YEAR_CLASS');
		$sql = "select 
						ClassTitleEN,
						YearID
			    from 
						$YEAR_CLASS 
				where 
						YearClassID =$YearClassID
				";
		$result = $this->objDB->returnArray($sql);
		$result = current($result);
		
		$yearID = $result['YearID'];
		
		return $yearID;
	}
	
	private function getYearID($YearName='thisYear')
	{
		if($YearName=='thisYear')
		{
			$YearClassID = $this->YearClassID;
		
			$YEAR_CLASS = $this->Get_IntranetDB_Table_Name('YEAR_CLASS');
			$sql = "select 
							ClassTitleEN,
							YearID
				    from 
							$YEAR_CLASS 
					where 
							YearClassID =$YearClassID
					";
			$result = $this->objDB->returnArray($sql);
			$result = current($result);
			
			$yearID = $result['YearID'];
		}
		else
		{
			$YEAR = $this->Get_IntranetDB_Table_Name('YEAR');
			$sql = "select 
							YearID
				    from 
							$YEAR 
					where 
							YearName =$YearName
					";
			$result = $this->objDB->returnArray($sql);
			$result = current($result);
			
			$yearID = $result['YearID'];
		}
		
		return $yearID;
	}
	
	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {

	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						ChineseName,
						HKID,
						DateOfBirth,
						Gender,
						STRN,
						ClassNumber,
						ClassName
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";

		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result)
	{		
		$fontSize = 'font-size:10px;';
		
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		
		$_borderBottom_dot =  'border-bottom: 1px dashed black; ';
		
		$_style_Align = ' text-align:right;';
		
		$IssueDate = $this->issuedate;
		
		if (date($IssueDate)==0) {
			$IssueDate = $this->EmptySymbol;
		} else {
			$IssueDate = date('j/n/Y',strtotime($IssueDate));
		}
		
		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $this->EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];		
		$Gender = ($Gender=='')? $this->EmptySymbol:$Gender;
		
		$RegistrationNo = $result[0]["STRN"];
		$RegistrationNo = ($RegistrationNo=='')? $this->EmptySymbol:$RegistrationNo;
		
		$ClassNumber = $result[0]["ClassNumber"];
		$ClassNumber = ($ClassNumber=='')? $this->EmptySymbol:$ClassNumber;
		
		$ClassName = $result[0]["ClassName"];
		$ClassName = ($ClassName=='')? $this->EmptySymbol:$ClassName;

		$CurrentSchoolYear = getCurrentAcademicYear('en');

		$PageNo = $this->PageNumber;
		

		$html = '';
		$html .= '<table width="90%" cellpadding="1" border="0px" align="center" style="border-collapse: collapse;">
					<col width="22%" />
					<col width="1%" />
					<col width="30%" />
					<col width="2%" />
					<col width="22%" />
					<col width="1%" />
					<col width="15%" />
					<col width="7%" />

					<tr>
							<td colspan="8" style="text-align:right;font-size:9px;"><font face="Arial">Page '.$PageNo.' of <!--totalPageNo--></font></td>					
					</tr>
					<tr>
							<td style="'.$_style_Align.'"><font face="Arial" style="'.$fontSize.'">Name</font></td>
							<td ><font face="Arial" style="'.$fontSize.'">:</font></td>
							<td style="'.$_borderBottom.'"><font face="Arial" style="'.$fontSize.'">'.$EngName." (".$ChiName.')</font></td>

							<td style="'.$_style_Align.'">&nbsp;</td>

							<td style="'.$_style_Align.'"><font face="Arial" style="'.$fontSize.'">School Year</font></td>
							<td ><font face="Arial" style="'.$fontSize.'">:</font></td>
							<td style="'.$_borderBottom.'"><font face="Arial" style="'.$fontSize.'">'.$CurrentSchoolYear.'</font></td>	

							<td style="'.$_style_Align.'">&nbsp;</td>					
					</tr>
					
					<tr>
							<td style="'.$_style_Align.'"><font face="Arial" style="'.$fontSize.'">Class</font></td>
							<td ><font face="Arial" style="'.$fontSize.'">:</font></td>
							<td style="'.$_borderBottom.'"><font face="Arial" style="'.$fontSize.'">'.$ClassName.'</font></td>

							<td style="'.$_style_Align.'">&nbsp;</td>
							
							<td style="'.$_style_Align.'"><font face="Arial" style="'.$fontSize.'">Class No.</font></td>
							<td><font face="Arial" style="'.$fontSize.'">:</font></td>
							<td style="'.$_borderBottom.'"><font face="Arial" style="'.$fontSize.'">'.$ClassNumber.'</font></td>

							<td style="'.$_style_Align.'">&nbsp;</td>
					</tr>

					<tr>
							<td style="'.$_style_Align.'"><font face="Arial" style="'.$fontSize.'">Registration No</font></td>
							<td><font face="Arial" style="'.$fontSize.'">:</font></td>
							<td style="'.$_borderBottom.'"><font face="Arial" style="'.$fontSize.'">'.$RegistrationNo.'</font></td>

							<td style="'.$_style_Align.'">&nbsp;</td>

							<td style="'.$_style_Align.'"><font face="Arial" style="'.$fontSize.'">Date of Issue</font></td>
							<td><font face="Arial" style="'.$fontSize.'">:</font></td>
							<td style="'.$_borderBottom.'"><font face="Arial" style="'.$fontSize.'">'.$IssueDate.'</font></td>

							<td style="'.$_style_Align.'">&nbsp;</td>
					</tr>
 
					';

		$html .= '</table>';
		
		$PageNo++;
		$this->setPageNo($PageNo);
	
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	
	/***************** Part 3 : Academic Performance******************/	
	public function getPart3_HTML($OLEInfoArr_INT){
		
		$Part3_Title = 'Major Achievements in Other Learning Experiences';
		$Part3_Remark = '&nbsp;';
		
		$titleArray = array();
		$titleArray[] = 'Year';
		$titleArray[] = 'Programme';
		$titleArray[] = 'Organization';
		$titleArray[] = 'Role';
		$titleArray[] = 'Award/Achievement';
		$titleArray[] = 'OLE Component';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="12%" />';
		$colWidthArr[] = '<col width="23%" />';
		$colWidthArr[] = '<col width="20%" />';
		$colWidthArr[] = '<col width="18%" />';
		$colWidthArr[] = '<col width="13%" />';
		$colWidthArr[] = '<col width="15%" />';
		
		$DataArr = array();
		$DataArray = $OLEInfoArr_INT[$this->uid]['INT'];
	

		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('3',$Part3_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false);
		
		return $html;	
	}		

	/***************** End Part 3 ******************/
	
	
	
	
	
	/***************** Part 4 : Other Learning Experience******************/	
	
	public function getPart4_HTML($OLEInfoArr_EXT){
		$Part4_Title = 'Performance / Awards and Key Participation Outside School';
		
		$titleArray = array();
		$titleArray[] = 'Year';
		$titleArray[] = 'Programme';
		$titleArray[] = 'Organisation';
		$titleArray[] = 'Role';
		$titleArray[] = 'Award/Achievement';
		
		$colWidthArr = array();		
		$colWidthArr[] = '<col width="13%" />';
		$colWidthArr[] = '<col width="27%" />';
		$colWidthArr[] = '<col width="23%" />';
		$colWidthArr[] = '<col width="17%" />';
		$colWidthArr[] = '<col width="20%" />';
		
		
		$DataArray = array();
		$DataArray = $OLEInfoArr_EXT[$this->uid]['EXT'];
		
		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('4',$Part4_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false);

		return $html;
	}
	
	
	/***************** End Part 4 ******************/
	
	
	
	/***************** Part 5 ******************/	
	public function getPart5_HTML(){
		
		$Part5_Title = 'School Posts';
		$Part5_Remark = '&nbsp;';
		
		$titleArray = array();
		$titleArray[] = 'Year';
		$titleArray[] = 'Group';
		$titleArray[] = 'Post';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="45%" />';
		$colWidthArr[] = '<col width="40%" />';
		
		$DataArr = array();
		$DataArray = $this->getSchoolPost();
		

		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('5',$Part5_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false);
		
		return $html;
		
	}

	public function getSchoolPost()
	{
		global $ipf_cfg;
		$Activity_student = $this->Get_eClassDB_Table_Name('ACTIVITY_STUDENT');
		
		$_UserID = $this->uid;
		
		if($_UserID!='')
		{
			$cond_studentID = "And UserID ='$_UserID'";
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And a.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$sql = "Select
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ActivityName,
						Role
				From
						$Activity_student as a
						inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = a.AcademicYearID
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence desc, 
						ActivityName asc		

				";
		$roleData = $this->objDB->returnResultSet($sql);
		
		return $roleData;
	}

	/***************** End Part 5 ******************/
	
	
	/***************** Part 6 ******************/
	public function getPart6_HTML(){
		
		$Part4_Title = 'List of Awards and Major Achievements Issued by the School';
		
		
		$titleArray = array();	
		$titleArray[] = 'Year';
		$titleArray[] = 'Award';		
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="85%" />';
		
		$DataArray = array();
		$DataArray = $this->getAward_Info();

		$html = '';
	
		$html .= $this->getPartContentDisplay_NoRowLimit('6',$Part4_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false);
		
		return $html;
		
	}
	
	
	private function getAward_Info(){
		
		global $ipf_cfg;
		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		
		$_UserID = $this->uid;
		
		if($_UserID!='')
		{
			$cond_studentID = "And UserID ='$_UserID'";
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And a.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$sql = "Select
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME',
						a.AwardName
				From
						$Award_student as a
						inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = a.AcademicYearID
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence desc, 
						a.AwardName asc		

				";
		$roleData = $this->objDB->returnResultSet($sql);


		return $roleData;
	}
	

	
	/***************** End Part 6 ******************/
	
	
	
	/***************** Part 7 ******************/
	
	public function getPart7_HTML(){
		
		$Part7_Title = "Student's Self-Account";
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$Part7_Title) ;
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$UserID = $this->uid;
		
		if($UserID!='')
		{
			$cond_studentID = "And UserID ='$UserID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnArray($sql,2);
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			
			$returnDate[] = $thisDataArr;
			
		}
//hdebug_r($sql);
		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$MainTitle='')
	{
		
		$_borderTop =  'border-top: 2px solid black; ';
		$_borderLeft =  'border-left: 2px solid black; ';
		$_borderBottom =  'border-bottom: 2px solid black; ';
		$_borderRight =  'border-right: 2px solid black; ';
		$text_alignStr ='center';		
		
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:11px;" '; 


		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];
	
			if($this->uid =='587')
			{
				$Detail = strip_tags($Detail,"<p><br /><o:p></o:p><br/>");
				$Detail = str_replace('<o:p></o:p>','<br/><br/>',$Detail);
			}
			else
			{
				$Detail = strip_tags($Detail,"<p><br>");
			}
		}
		else
		{
			$Detail = 'No Record Found.';
		}	
		
		$Detail = str_replace('<p class="MsoNormal">&nbsp;</p>','',$Detail);
//debug_r($Detail);		
		$html .= '<table width="90%" height="700px"  cellpadding="7" border="0px" align="center" '.$table_style.'>
					<tr>				 							
						<td style="font-size:15px;height:30px;vertical-align: top;"><font face="Arial"><b>'.$MainTitle.'</b></font></td>						 														
					</tr>
					<tr>
						<td style="padding-left: 10px; padding-right: 10px; padding-top: 20px;  padding-bottom: 20px; vertical-align: top;'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'"><font size="2" face="Arial">'.$Detail.'</font></td>						 														
					</tr>
				  </table>';	
		#case P72773 added padding and font size="2"
			
		return $html;
	}
	
	
	/***************** End Part 7 ******************/
	
	
	
	/***************** Part 8 : Footer ******************/
	public function getPart8_HTML($PageBreak='')
	{
		$PrincipalSign = "Principal's Signature:";
		$ClassTeacherSign = "Class Teacher's Signature:";
		$SchoolChop = "School Chop:";
		
		$_borderTop =  'border-top: 2px solid black; ';
		$_borderLeft =  'border-left: 2px solid black; ';
		$_borderBottom =  'border-bottom: 2px solid black; ';
		$_borderRight =  'border-right: 2px solid black; ';
		
		$html = '';
		
		$html .= '<table height="120px" width="90%" cellpadding="5" border="0px" align="center" style="font-size:11px;border-collapse: collapse;border: 2px solid black;'.$PageBreak.'">		
					<col width="33%" />
					<col width="34%" />
					<col width="33%" />

				 	<tr>					
					    <td align="center" style="text-align:left;vertical-align: top;'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'"><font face="Arial">'.$PrincipalSign.'</font></td>
						<td align="center" style="text-align:left;vertical-align: top;'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'"><font face="Arial">'.$ClassTeacherSign.'</font></td>
						<td align="center" style="text-align:left;vertical-align: top;'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'"><font face="Arial">'.$SchoolChop.'</font></td>
					</tr>

				  </table>';
		return $html;
		
	}
	/***************** End Part 8 ******************/

	
	
	
	private function getStringDisplay($String='',$font_size='10',$Page_break='') {
		
		$html = '';
		$html .= '<table width="90%" cellpadding="7" border="0px" align="center" style="font-size:'.$font_size.'px;" '.$Page_break.'>					
		   			 <tr>
					    <td><font face="Arial">'.$String.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}

	
	private function getPartContentDisplay_NoRowLimit($PartNum, $mainTitle,$subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false) 
	{
				
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$_borderTop_B =  'border-top: 2px solid black; ';
		$_borderBottom_B =  'border-bottom: 2px solid black; ';
		
		$text_alignStr ='center';		
		
		$BgColor = $this->TableTitleBgColor;
		
		$Page_break='page-break-after:always;';
		
		if($hasPageBreak==false)
		{
			$Page_break='';
		}
		
		$table_style = 'style="font-size:9px;border-collapse: collapse;'.$Page_break.'"'; 

	
		$subtitle_html = '';
		
		if($PartNum=='3')
		{
			$subtitle_html = '';
			
			if(count($DataArr)==0)
			{
				return '';
			}
			
			$i=0;
			foreach((array)$subTitleArr as $key=>$_title)
			{
				$text_alignStr='center';
				
				$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderTop_B.$_borderBottom_B.'"><b><font face="Arial">';
				$subtitle_html .=$_title;
				$subtitle_html .='</font></b></td>';
				
				$i++;
			}	
						
			$total_Record=25;
			$rowHight = 25;
			$TableWidth = '90%';
			$TableAlign = 'center';		
		}
		
		else if($PartNum=='4')
		{
			$subtitle_html = '';
			
			if(count($DataArr)==0)
			{
				return '';
			}
			
			$i=0;
			foreach((array)$subTitleArr as $key=>$_title)
			{
				$text_alignStr='center';
				
				$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderTop_B.$_borderBottom_B.'"><b><font face="Arial">';
				$subtitle_html .=$_title;
				$subtitle_html .='</font></b></td>';
				
				$i++;
			}	
						
			$total_Record=5;
			$rowHight = 30;	
			$TableWidth = '90%';
			$TableAlign = 'center';		
		}
		else if ($PartNum=='5')
		{
			$subtitle_html = '';
			
			if(count($DataArr)==0)
			{
				return '';
			}
			
			foreach((array)$subTitleArr as $key=>$_title)
			{
				$text_alignStr='center';
				
				$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderTop_B.$_borderBottom_B.'"><b><font face="Arial">';
				$subtitle_html .=$_title;
				$subtitle_html .='</font></b></td>';
			}	
						
			$total_Record=20;
			$rowHight = 30;
			$TableWidth = '90%';
			$TableAlign = 'center';	
		
		}
		else if ($PartNum=='6')
		{
			$subtitle_html = '';
			
			if(count($DataArr)==0)
			{
				return '';
			}
			
			$i=0;
			foreach((array)$subTitleArr as $key=>$_title)
			{
				$text_alignStr='center';
			
				$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderTop_B.$_borderBottom_B.'"><b><font face="Arial">';
				$subtitle_html .=$_title;
				$subtitle_html .='</font></b></td>';
				$i++;
			}	
						
			$total_Record=5;
			$rowHight = 30;
			$TableWidth = '90%';	
			$TableAlign = 'center';
		}
		
		$text_alignStr='center';
		$content_html='';	
		$rowMax_count = 0;
		
		$count_col =count($colWidthArr);
		
		if(count($DataArr)>0)
		{
			$rowMax_count = count($DataArr);
		}
		else
		{
			$NoRecordStr = 'No Record Found.';
			
			if($PartNum=='5' || $PartNum=='6')
			{
				$content_html = '<td colspan="'.($count_col-1).'" style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderTop.$_borderBottom.'"><font face="Arial">'.$NoRecordStr.'</font></td>';
				$content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.'"><font face="Arial">&nbsp;</font></td>';
			}
			else 
			{
				$content_html = '<td colspan="'.$count_col.'" style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderTop.$_borderBottom.'"><font face="Arial">'.$NoRecordStr.'</font></td>';
			}
		}	
		
	

		for ($i=0; $i<$rowMax_count; $i++)
		{		
			$content_html .= '<tr>';	
				
			$j=0;		 
			foreach((array)$DataArr[$i] as $_title=>$_titleVal)
			{
				if($_titleVal=='')
				{
					$_titleVal =  $this->EmptySymbol;
				}
				
				if($PartNum=='3')
				{
					if($j==1 || $j==2)
					{
						$text_alignStr = 'left';
					}
					else
					{
						$text_alignStr = 'center';
					}
				}
				
				else if($PartNum=='4')
				{
					if($j==1 || $j==2)
					{
						$text_alignStr='left';
					}
					else
					{
						$text_alignStr = 'center';
					}
				}
				else if($PartNum=='5' || $PartNum=='6')
				{
					if($j==1)
					{
						$text_alignStr = 'left';
					}
					else
					{
						$text_alignStr = 'center';
					}
				}
				$content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderTop.$_borderBottom.'"><font face="Arial">'.$_titleVal.'</font></td>';								
				$j++;
			}			
			$content_html .= '</tr>';		
		}
		
//		if ($PartNum!='5' && $PartNum!='6')
//		{
//			$remainRow = $total_Record-$rowMax_count;
//			for($i=0,$i_MAX=$remainRow;$i<$i_MAX;$i++)
//			{
//	
//				$thisContent = '&nbsp;';
//				
//				if($i==0)
//				{
//					$thisContent = '&nbsp;';
//				}
//				
//				$text_alignStr='center';
//				$content_html .= '<tr>';					
//				for($j=0;$j<$count_col;$j++)
//				{
//					if($PartNum=='3')
//					{
//						if($j==1 || $j==2)
//						{
//							$text_alignStr = 'left';
//						}
//						else
//						{
//							$text_alignStr = 'center';
//						}
//					}				
//					$content_html .= ' <td style="height:'.$rowHight.'px;vertical-align: top;text-align:'.$text_alignStr.';'.$_borderTop.$_borderBottom.'"><font face="Arial">'.$thisContent.'</font></td>';
//				}				
//				$content_html .= '</tr>';
//			}
//		}
					
		
		$colNumber = count($colWidthArr);
			
		/****************** Main Table ******************************/
		
		$html = '';

		$html = '<table width="'.$TableWidth.'" height="" cellpadding="4" border="0px" align="'.$TableAlign.'" '.$table_style.'>';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$html .=$_colWidthInfo;
					}
					
		$html .= '<tr> 
					<td colspan="'.$colNumber.'"  style=" font-size:15px;text-align:left;vertical-align:text-top;"><font face="Arial"><b>
					'.$mainTitle.'
					</b></font></td>
				  </tr>';
		
		$html .= '<tr>'.$subtitle_html.'</tr>';
		
		$html .= $content_html;
		
		$html .='</table>';

			
		return $html;
	}
	
	

	
	public function getStudentAcademic_YearClassNameArr()
	{
		$StudentID = $this->uid;
		
		$academicYearIDArr = $this->academicYearIDArr;
		
		$SelectStr= 'h.AcademicYear as AcademicYear,
					 h.ClassName as ClassName';
		
		if($academicYearIDArr!='')
		{		
			$YearArr = array();
			for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
			{
				if($academicYearIDArr[$i]=='')
				{
					continue;
				}
				$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
				$YearArr[] = $thisAcademicYear;
			}
			
			$cond_academicYear = "And h.AcademicYear In ('".implode("','",(array)$YearArr)."')";		
		}
		

		$Profile_his = $this->Get_IntranetDB_Table_Name('PROFILE_CLASS_HISTORY');
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$sql = 'select 
						'.$SelectStr.' 
				from 
						'.$Profile_his.' as h 
				where 
						h.UserID = "'.$StudentID.'"
						'.$cond_academicYear.'
				Order by
						h.AcademicYear desc';
						
		$roleData = $this->objDB->returnArray($sql,2);
		
	
		return $roleData;
	}
	
	public function getOLEcomponentName()
	{
		$OLE_ELE = $this->Get_eClassDB_Table_Name('OLE_ELE');
		
		$sql="Select 
					EngTitle,
					DefaultID
			  from
					$OLE_ELE
			";
		$roleData = $this->objDB->returnArray($sql,2);
		
		$returnArray = array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$thisName = $roleData[$i]['EngTitle'];
			$thisID = $roleData[$i]['DefaultID'];
			
			$returnArray[$thisID] = $thisName;
		}
	
		return $returnArray;
	}
	
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false,$OLEcomponentArr='')
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		
		if($_UserID!='')
		{
			$cond_studentID = "And UserID ='$_UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = "And SLPOrder is not null"; 
		}
	
		if($IntExt!='')
		{
			$cond_IntExt = "And ole_prog.IntExt='".$IntExt."'";
		}
		
		if($IntExt=='INT')
		{
			$cond_limit = "limit 25";
		}
		else if($IntExt=='EXT')
		{
			$cond_limit = "limit 5" ;
		} 
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = "And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
		
		$sql = "Select
						ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_prog.ELE as OleComponent
						
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID
						inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
				order by 
						y.Sequence desc,	
						ole_prog.Title asc		
				$cond_limit

				"; 
		$roleData = $this->objDB->returnResultSet($sql);
//hdebug_r($sql);  
//debug_r($roleData); 
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$_StudentID = $roleData[$i]['StudentID'];
			$_IntExt = $roleData[$i]['IntExt'];
			$thisAcademicYear = $roleData[$i]['YEAR_NAME'];
			
			$thisOleComponentArr = explode(',',$roleData[$i]['OleComponent']);
		
	
			for($a=0,$a_MAX=count($thisOleComponentArr);$a<$a_MAX;$a++)
			{
				$thisOleComponentID = $thisOleComponentArr[$a];		
				$thisOleComponentArr[$a] = $OLEcomponentArr[$thisOleComponentID];
			}
			
			$thisOLEstr = implode(",",(array)$thisOleComponentArr);

			$thisDataArr = array();
			
			if($IntExt=='INT')
			{
				$thisDataArr['SchoolYear'] =$thisAcademicYear;	
				$thisDataArr['Program'] =$roleData[$i]['Title'];	
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];		
				$thisDataArr['Role'] =$roleData[$i]['Role'];			
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
				$thisDataArr['OLEComponent'] =$thisOLEstr;
			}
			else if($IntExt=='EXT')
			{
				$thisDataArr['SchoolYear'] =$thisAcademicYear;
				$thisDataArr['Program'] =$roleData[$i]['Title'];
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];			
				$thisDataArr['Role'] =$roleData[$i]['Role'];		
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
			}
		
			$returnData[$_StudentID][$_IntExt][] = $thisDataArr;
			
		}
//debug_r($returnData);
		return $returnData;
	}
	
	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		$lastNo = count($returnArray)-1;
		
		$firstYear = substr($returnArray[0], 0, 4);
		$lastYear = substr($returnArray[$lastNo], 0, 4);
		
		$returnValue = $firstYear."-".$lastYear;
		
		return $returnValue;
		
	}
	
	private function getAcademicYearArr($academicYearIDArr)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	
	public function getEmptyTable($Height='',$Content='&nbsp;') {
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td style="text-align:center"><font face="Arial">'.$Content.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>