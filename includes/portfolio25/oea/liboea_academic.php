<?
# modifying :
/*
 * Modification Log:
 *  2018-09-27 Bill     [2018-0914-1458-46073]
 *  - modified generate_academic_performance(), fixed incorrect percentile calculation > ignore term assessment result
 */

if (!defined("LIBOEA_ACADEMIC_DEFINED"))         // Preprocessor directives
{
  define("LIBOEA_ACADEMIC_DEFINED",true);

	class liboea_academic {
		private $objDB;
		private $recordID;
		private $TableName;

		private $StudentID;
		
		private $SubjectPercentileInfoArr;
		private $SubjectOverallRatingInfoArr;
		
		public function liboea_academic($recordID = null){
			global $eclass_db, $oea_cfg;

			$this->TableName = $eclass_db.'.OEA_STUDENT_ACADEMIC';
			$this->ID_FieldName = 'StudentAcademicID';
			$this->TypeCode = liboea::getAcademicInternalCode();
			
			$this->objDB = new libdb();
			$this->SubjectPercentileInfoArr = array();
			$this->SubjectOverallRatingInfoArr = array();

			//LOAD THE OEA ABILITY FORM STOREAGE (EG DB)
			if($recordID != null)
			{
				$this->recordID = intval($recordID);
				$this->loadRecordFromStorage();
			}
		}

		private function getObjectTableName(){
			return $this->TableName;
		}
		public function getObjectIdFieldName(){
			return $this->ID_FieldName;
		}		
		public function getObjectID(){
			return $this->recordID;
		}
		public function getObjectInternalCode(){
			return $this->TypeCode;
		}

		public function setStudentID($val){
			$this->StudentID = $val;
		}
		public function getStudentID(){
			return $this->StudentID;
		}
		public function setRecordStatus($val){
			$this->RecordStatus = $val;
		}
		public function getRecordStatus(){
			return $this->RecordStatus;
		}
		
		public function setInputDate($val){
			$this->InputDate = $val;
		}
		public function getInputDate(){
			return $this->InputDate;
		}

		public function setInputBy($val){
			$this->InputBy = $val;
		}
		public function getInputBy(){
			return $this->InputBy;
		}
		
		public function setApprovalDate($val){
			$this->ApprovalDate = $val;
		}
		public function getApprovalDate(){
			return $this->ApprovalDate;
		}
		
		public function setApprovedBy($intValue){
			$this->ApprovedBy = $intValue;
		}
		public function getApprovedBy(){
			return $this->ApprovedBy;
		}

		public function setModifyDate($val){
			$this->ModifyDate = $val;
		}
		public function getModifyDate(){
			return $this->ModifyDate;
		}
	
		public function setModifiedBy($val){
			$this->ModifiedBy = $val;
		}
		public function getModifiedBy(){
			return $this->ModifiedBy;
		}
		
		
		
		
		public function setPercentile($val){
			$this->Percentile = $val;
		}
		public function getPercentile(){
			return $this->Percentile;
		}
		
		public function setOverallRating($val){
			$this->OverallRating = $val;
		}
		public function getOverallRating(){
			return $this->OverallRating;
		}
		
		public function setPercentileRemark($val){
			$this->PercentileRemark = $val;
		}
		public function getPercentileRemark(){
			return $this->PercentileRemark ;
		}
		
		public function setOverallRatingRemark($val){
			$this->OverallRatingRemark = $val;
		}
		public function getOverallRatingRemark(){
			return $this->OverallRatingRemark;
		}

		public function get_pending_status() {
			global $oea_cfg;
			return $oea_cfg["OEA_STUDENT_ACADEMIC_RecordStatus"]["Pending"];
		}
		public function get_approved_status() {
			global $oea_cfg;
			return $oea_cfg["OEA_STUDENT_ACADEMIC_RecordStatus"]["Approved"];
		}
		public function get_rejected_status() {
			global $oea_cfg;
			return $oea_cfg["OEA_STUDENT_ACADEMIC_RecordStatus"]["Rejected"];
		}
		
		
		public static function Get_Academic_Percentile_Config_Array()
		{
			global $oea_cfg;
			return $oea_cfg["Academic"]["Percentile"];
		}
		
		public static function Get_Academic_Percentile_Code_Array()
		{
			global $oea_cfg;
			return array_keys($oea_cfg["Academic"]["Percentile"]);
		}
		
		public static function Get_Academic_Percentile_Name($InternalCode)
		{
			global $Lang;
			return $Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][$InternalCode];
		}
		
		public static function Get_Academic_Percentile_JupasCode($InternalCode)
		{
			global $oea_cfg;
			return $oea_cfg["Academic"]["Percentile"][$InternalCode]["JupasCode"];
		}
		
		public static function Get_Academic_Overall_Rating_Config_Array() 
		{
			global $oea_cfg;
			return $oea_cfg["Academic"]["OverallRating"];
		}
		
		public static function Get_Academic_Overall_Rating_Grade_Array()
		{
			global $oea_cfg;
			return array_keys($oea_cfg["Academic"]["OverallRating"]);
		}
		
		public static function Get_Academic_Overall_Rating_Name($InternalCode)
		{
			global $Lang;
			return $Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr'][$InternalCode];
		}
		
		public static function Get_Academic_Overall_Rating_JupasCode($InternalCode)
		{
			global $oea_cfg;
			return $oea_cfg["Academic"]["OverallRating"][$InternalCode]["JupasCode"];
		}
		
		public function Get_Overall_Rating_InternalCode_By_Percentile_Mapping($PercentileInternalCode, $SubjectID='') {
			global $liboea;
			
			$PercentileOverallRatingAssoArr = $liboea->getAcademic_PercentileMapWithOverAllRating($SubjectID);
			return $PercentileOverallRatingAssoArr[$PercentileInternalCode];
		}
		
		private function loadRecordFromStorage(){
			global $eclass_db;			
			$result = null;
			
			$_recordID = $this->recordID;

			if( (trim($_recordID) == "") || (intval($_recordID) < 0)) {
				//DO NOTHING
			}else{
				$ObjTable = $this->getObjectTableName();
				$ObjIdFieldName = $this->getObjectIdFieldName();
				$sql = "select		
							StudentAcademicID,
							StudentID,
							Percentile,
							OverallRating,
							PercentileRemark,
							OverallRatingRemark,
							InputDate,
							InputBy,
							ModifyDate,
							ModifiedBy,
							ApprovedBy,
							ApprovalDate
						from 
							$ObjTable
						where 
							$ObjIdFieldName = ".$this->recordID;
				$resultSet = $this->objDB->returnArray($sql);
				$infoArr = $resultSet[0];
				
				foreach ((array)$infoArr as $_key => $_value)
				{
					if (!is_numeric($_key))
						$this->{$_key} = $_value;
				}
				
				$this->setSubjectPercentileToObject();
				$this->setSubjectOverallRatingToObject();
			}
		}

		public function save()
		{
			if( 
				(trim($this->recordID) != "") && 
					(intval($this->recordID) > 0)
				){
				$resultId = $this->update_record();
			}
			else{	
				$resultId = $this->new_record();
			}

			return $resultId;

		}
		
		private function update_record()
		{
			global $eclass_db;
			
			# Set object value
			$this->setRecordStatus($this->get_approved_status());
			$this->setModifyDate('now()');
			$this->setModifiedBy($_SESSION['UserID']);
			$this->setPercentile($this->convertObjectSubjectPercentileToDBValue());
			$this->setOverallRating($this->convertObjectSubjectOverallRatingToDBValue());
			
			# Prepare value for SQL update
			$DataArr = array();
			$DataArr["Percentile"]		= $this->objDB->pack_value($this->Percentile,"str");
			$DataArr["OverallRating"]	= $this->objDB->pack_value($this->OverallRating,"str");
			$DataArr["PercentileRemark"]		= $this->objDB->pack_value($this->PercentileRemark,"str");
			$DataArr["OverallRatingRemark"]		= $this->objDB->pack_value($this->OverallRatingRemark,"str");
			$DataArr["RecordStatus"]	= $this->objDB->pack_value($this->RecordStatus, "int");
			$DataArr["ModifyDate"]		= $this->objDB->pack_value($this->ModifyDate, "date");
			$DataArr["ModifiedBy"]		= $this->objDB->pack_value($this->ModifiedBy, "int");
			$DataArr["ApprovalDate"]	= $this->objDB->pack_value($this->ApprovalDate, "date");
			$DataArr["ApprovedBy"]		= $this->objDB->pack_value($this->ApprovedBy, "int");
			
			# Build field update values string
			$valueFieldArr = array();
			foreach ($DataArr as $_field => $_value)
			{
				$valueFieldArr[] = " $_field = $_value ";
			}
			$valueFieldText .= implode(',', $valueFieldArr);
			
			$ObjTable = $this->getObjectTableName();
			$ObjIdFieldName = $this->getObjectIdFieldName();
			$sql = "Update $ObjTable Set $valueFieldText Where $ObjIdFieldName = '".$this->recordID."'";
			$success = $this->objDB->db_db_query($sql);
			
			$this->loadRecordFromStorage();
			return $this->recordID;
		}
		
		private function new_record()
		{
			global $eclass_db, $UserID;

			$this->setPercentile($this->convertObjectSubjectPercentileToDBValue());
			$this->setOverallRating($this->convertObjectSubjectOverallRatingToDBValue());
			
			$this->setRecordStatus($this->get_approved_status());
			$this->setInputDate('now()');
			$this->setInputBy($_SESSION['UserID']);
			$this->setModifyDate('now()');
			$this->setModifiedBy($_SESSION['UserID']);
			$this->setApprovalDate('null');
			$this->setApprovedBy('null');

			$DataArr = array();
			$DataArr["StudentID"]		= $this->objDB->pack_value($this->StudentID,"int");
			$DataArr["Percentile"]		= $this->objDB->pack_value($this->Percentile,"str");
			$DataArr["OverallRating"]	= $this->objDB->pack_value($this->OverallRating,"str");
			$DataArr["PercentileRemark"]		= $this->objDB->pack_value($this->PercentileRemark,"str");
			$DataArr["OverallRatingRemark"]		= $this->objDB->pack_value($this->OverallRatingRemark,"str");
			$DataArr["RecordStatus"]	= $this->objDB->pack_value($this->RecordStatus,"int");
			$DataArr["InputDate"]		= $this->objDB->pack_value($this->InputDate,"int");
			$DataArr["InputBy"]			= $this->objDB->pack_value($this->InputBy,"int");
			$DataArr["ApprovalDate"]	= $this->objDB->pack_value($this->ApprovalDate,"date");
			$DataArr["ApprovedBy"]		= $this->objDB->pack_value($this->ApprovedBy,"int");
			$DataArr["ModifyDate"]		= $this->objDB->pack_value($this->ModifyDate,"date");
			$DataArr["ModifiedBy"]		= $this->objDB->pack_value($this->ModifiedBy,"date");


			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = $value;
			}
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$ObjTable = $this->getObjectTableName();
			$sql = "Insert Into $ObjTable ($fieldText) Values ($valueText)";
			$success = $this->objDB->db_db_query($sql);
			
			$RecordID = $this->objDB->db_insert_id();
			$this->recordID = $RecordID;
			
			$this->loadRecordFromStorage();
			return $RecordID;
		}
		
		private function update_recordStatus($ParStatus) {
			$this->setRecordStatus($ParStatus);
			$this->setApprovalDate('now()');
			$this->setApprovedBy($_SESSION['UserID']);
			
			return $this->save();
		}
		
		public function approve_record(){
			return $this->update_recordStatus($this->get_approved_status());
		}
		
		public function reject_record(){
			return $this->update_recordStatus($this->get_rejected_status());
		}
		
		
		
		public function resetObjectSubjectPercentileInfo() {
			$this->SubjectPercentileInfoArr = array();
		}		
		public function setObjectSubjectPercentileInfo($SubjectID, $PercentileInternalCode) {
			$this->SubjectPercentileInfoArr[$SubjectID] = $PercentileInternalCode;
		}		
		public function getObjectSubjectPercentileInfoArr() {
			return $this->SubjectPercentileInfoArr;
		}		
		public function convertObjectSubjectPercentileToDBValue()
		{
			$SubjectPercentileInfoArr = $this->getObjectSubjectPercentileInfoArr();
			
			$percentileArr = array();
			foreach ((array)$SubjectPercentileInfoArr as $_subjectID => $_percentileInternalCode){
				$percentileArr[] = $_subjectID."==".$_percentileInternalCode;
			}
			$percentileValue = implode('##', $percentileArr);
			
			return $percentileValue;
		}		
		private function setSubjectPercentileToObject()
		{
			$PercentileValue = $this->getPercentile();
			
			if ($PercentileValue == '')
			{
				// no value => don't set value
			}
			else
			{
				$this->resetObjectSubjectPercentileInfo();
				$SubjectInfoArr = $this->getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=0, $CheckStudentHaveScore=1);
				$SubjectIDArr = Get_Array_By_Key($SubjectInfoArr, 'SubjectID');
				unset($SubjectInfoArr);
				
				$SubjectPercentileInfoArr = explode('##', $PercentileValue);
				$numOfSubjectPercentile = count($SubjectPercentileInfoArr);
				
				for ($i=0; $i<$numOfSubjectPercentile; $i++)
				{
					$_SubjectPercentileInfoArr = explode('==', $SubjectPercentileInfoArr[$i]);
					$_SubjectID = $_SubjectPercentileInfoArr[0];
					$_PercentileInternalCode = $_SubjectPercentileInfoArr[1];
					
					if (!in_array($_SubjectID, $SubjectIDArr)) {
						continue;
					}
					
					$this->setObjectSubjectPercentileInfo($_SubjectID, $_PercentileInternalCode);
				}
			}
		}
		
		
		public function resetObjectSubjectOverallRatingInfo() {
			$this->SubjectOverallRatingInfoArr = array();
		}		
		public function setObjectSubjectOverallRatingInfo($SubjectID, $OverallRatingInternalCode) {
			$this->SubjectOverallRatingInfoArr[$SubjectID] = $OverallRatingInternalCode;
		}		
		public function getObjectSubjectOverallRatingInfoArr() {
			return $this->SubjectOverallRatingInfoArr;
		}		
		public function convertObjectSubjectOverallRatingToDBValue()
		{
			$SubjectOverallRatingInfoArr = $this->getObjectSubjectOverallRatingInfoArr();
			
			$OverallRatingArr = array();
			foreach ((array)$SubjectOverallRatingInfoArr as $_subjectID => $_overallRatingInternalCode){
				$OverallRatingArr[] = $_subjectID."==".$_overallRatingInternalCode;
			}
			$OverallRatingValue = implode('##', $OverallRatingArr);
			
			return $OverallRatingValue;
		}		
		private function setSubjectOverallRatingToObject()
		{
			$OverallRatingValue = $this->getOverallRating();
			
			if ($OverallRatingValue == '')
			{
				// no value => don't set value
			}
			else
			{
				$this->resetObjectSubjectOverallRatingInfo();
				$SubjectInfoArr = $this->getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=0, $CheckStudentHaveScore=1);
				$SubjectIDArr = Get_Array_By_Key($SubjectInfoArr, 'SubjectID');
				unset($SubjectInfoArr);
				
				$SubjectOverallRatingInfoArr = explode('##', $OverallRatingValue);
				$numOfSubjectOverallRating = count($SubjectOverallRatingInfoArr);
				
				for ($i=0; $i<$numOfSubjectOverallRating; $i++)
				{
					$_SubjectOverallRatingInfoArr = explode('==', $SubjectOverallRatingInfoArr[$i]);
					$_SubjectID = $_SubjectOverallRatingInfoArr[0];
					$_OverallRatingInternalCode = $_SubjectOverallRatingInfoArr[1];
					
					if (!in_array($_SubjectID, $SubjectIDArr)) {
						continue;
					}
					
					$this->setObjectSubjectOverallRatingInfo($_SubjectID, $_OverallRatingInternalCode);
				}
			}
		}
		
		// not include save to DB => need to call save() explicitly
		public function generate_academic_performance() {
			global $liboea, $oea_cfg;
			
			$libpf_academic = new libpf_academic();
			
			### Reset Object values
			$this->resetObjectSubjectPercentileInfo();
			$this->resetObjectSubjectOverallRatingInfo();
		
			
			### Get JUPAS academic settings
			$AcademicYearID = $liboea->getAcademic_AcademicYearID();
			$YearTermID = $liboea->getAcademic_YearTermID();
			$PercentileConfigArr = $this->Get_Academic_Percentile_Config_Array();
			$JupasSubjectInfoArr = $this->getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=0, $CheckStudentHaveScore=0);
			$JupasSubjectIDArr = Get_Array_By_Key($JupasSubjectInfoArr, 'SubjectID'); 
			
			
			### Get Student's academic result
			$StudentSubjectRankInfoArr = $libpf_academic->Get_Student_Academic_Result($this->getStudentID(), $SubjectIDArr='', $AcademicYearID, $YearTermID, $ParentSubjectOnly=1, $IgnoreEmptyScore=1, $IgnoreNoRanking=1, $IgnoreTermAssessmentResult=1);
			$numOfSubject = count($StudentSubjectRankInfoArr);
			
			
			for ($i=0; $i<$numOfSubject; $i++) {
				$thisSubjectID = $StudentSubjectRankInfoArr[$i]['SubjectID'];
				$thisGrade = $StudentSubjectRankInfoArr[$i]['Grade'];
				$thisOrderMeritForm = $StudentSubjectRankInfoArr[$i]['OrderMeritForm'];
				$thisOrderMeritFormTotal = $StudentSubjectRankInfoArr[$i]['OrderMeritFormTotal'];
				
				// Skip if the Subject is not included in the JUPAS settings
				if (!in_array($thisSubjectID, (array)$JupasSubjectIDArr)) {
					continue;
				}
				
				if ($thisGrade == 'ABS') {
					$this->setObjectSubjectPercentileInfo($thisSubjectID, $oea_cfg["Academic"]["PercentileUnabletoJudge"]);
					$thisOverallRatingInternalCode = $this->Get_Overall_Rating_InternalCode_By_Percentile_Mapping($oea_cfg["Academic"]["PercentileUnabletoJudge"], $thisSubjectID);
					$this->setObjectSubjectOverallRatingInfo($thisSubjectID, $thisOverallRatingInternalCode);
				}
				else {
					// Skip if no ranking or no one study this Subject
					if ($thisOrderMeritForm == '' || $thisOrderMeritForm == 0 || $thisOrderMeritForm == '-1' || $thisOrderMeritFormTotal == '' || $thisOrderMeritFormTotal == 0) {
						continue;
					}
					
					// Calculate Percentile
					if ($thisOrderMeritFormTotal > 0) {
						$thisPercentile = my_round(($thisOrderMeritForm / $thisOrderMeritFormTotal) * 100, 0);
					}
					else {
						$thisPercentile = 0;
					}
					
					// Determine the student fall in which percentile and overall rating
					foreach ((array)$PercentileConfigArr as $thisPercentileInternalCode => $thisPercentileInfoArr) {
						$thisLowerLimit = $thisPercentileInfoArr['LowerLimit'];
						
						if ((float)$thisPercentile <= (float)$thisLowerLimit) {
							$thisOverallRatingInternalCode = $this->Get_Overall_Rating_InternalCode_By_Percentile_Mapping($thisPercentileInternalCode, $thisSubjectID);
							
							$this->setObjectSubjectPercentileInfo($thisSubjectID, $thisPercentileInternalCode);
							$this->setObjectSubjectOverallRatingInfo($thisSubjectID, $thisOverallRatingInternalCode);
							
							break;
						}
					}
				}
			}
		}
		
		
		
		###################################################################################################
		############################ Jupas Subject Settings Functions [Start] #############################
		###################################################################################################
		
		public function Save_Subject_Info($SubjectCodeArr, $SubjectRecordStatusArr) {
			$SubjectInfoArr = $this->getSubjectInfoArr($WithJupasSubjectCodeOnly=0, $RecordStatus='', $returnAsso=1);
			
			$this->objDB->Start_Trans();
			$SuccessArr = array();
			foreach ((array)$SubjectCodeArr as $thisSubjectID => $thisJupasSubjectCode) {
				$thisRecordStatus = $SubjectRecordStatusArr[$thisSubjectID];
				$thisRecordStatus = ($thisRecordStatus == '')? 0 : $thisRecordStatus;
				
				$DataArr = array();
				$DataArr['RecordStatus'] = $thisRecordStatus;
				$DataArr['JupasSubjectCode'] = $thisJupasSubjectCode;
				
				if (isset($SubjectInfoArr[$thisSubjectID])) {
					$SuccessArr[$thisSubjectID] = $this->Update_Subject_Info($thisSubjectID, $DataArr);
				}
				else {
					$DataArr['SubjectID'] = $thisSubjectID;
					$SuccessArr[$thisSubjectID] = $this->Insert_Subject_Info($DataArr);
				}
			}
			
			if (in_array(false, (array)$SuccessArr)) {
				$this->objDB->RollBack_Trans();
				return false;
			}
			else {
				$this->objDB->Commit_Trans();
				return true;
			}
		}
		
		private function Insert_Subject_Info($DataArr)
		{
			global $eclass_db;
			
			if(!is_array($DataArr))
				return false;
				
			foreach((array)$DataArr as $key => $data)
			{
				$fieldArr[] = $key;
				if(trim($data)=='')
					$valueArr[] = "NULL";
				else
					$valueArr[] = "'".$this->objDB->Get_Safe_Sql_Query(trim($data))."'";
			}
			
			# DateInput
			$fieldArr[] = 'InputDate';
			$valueArr[] = 'now()';
			# InputBy
			$fieldArr[] = 'InputBy';
			$valueArr[] = "'".$_SESSION['UserID']."'";
			# DateModified
			$fieldArr[] = 'ModifyDate';
			$valueArr[] = 'now()';
			# LastModifiedBy
			$fieldArr[] = 'ModifiedBy';
			$valueArr[] = "'".$_SESSION['UserID']."'";
			
			
			$fields = implode(",",$fieldArr);
			$values = implode(",",$valueArr);
			
			$OEA_SUBJECT_INFO = $eclass_db.'.OEA_SUBJECT_INFO';
			$sql = "INSERT INTO $OEA_SUBJECT_INFO
						($fields)
					VALUES
						($values)
			";
			$result = $this->objDB->db_db_query($sql);
			
			return $result? true : false;	
	
		}
		
		private function Update_Subject_Info($SubjectID, $DataArr)
		{
			global $eclass_db;
			
			if(!is_array($DataArr))
				return false;
				
			foreach((array)$DataArr as $key => $data)
			{
				if(trim($data)=='')
					$valueFieldArr[] = $key." = NULL";
				else
					$valueFieldArr[] = $key." = '".$this->objDB->Get_Safe_Sql_Query(trim($data))."'";
			}
			
			# DateModified
			$valueFieldArr[] = " ModifyDate = NOW() ";
			# LastModifiedBy
			$valueFieldArr[] = " ModifiedBy = '".$_SESSION['UserID']."'";
			
			$valueFields = implode(",",$valueFieldArr);
			
			$OEA_SUBJECT_INFO = $eclass_db.'.OEA_SUBJECT_INFO';
			$sql = "UPDATE $OEA_SUBJECT_INFO
					SET $valueFields
					WHERE SubjectID = '$SubjectID'
			";
			$result = $this->objDB->db_db_query($sql);
			
			return $result? true : false;	
		}
		
		private function getSubjectInfoLastModified() {
			global $eclass_db;
			
			$OEA_SUBJECT_INFO = $eclass_db.'.OEA_SUBJECT_INFO';
			$sql = "Select
							ModifyDate,
							ModifiedBy
					From
							$OEA_SUBJECT_INFO
					Order By
							ModifyDate desc
					Limit 1
					";
			return $this->objDB->returnArray($sql);
		}
		
		
		public function getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=0, $CheckStudentHaveScore=0, $OrderbyJupasCode=0) {
			global $intranet_db, $eclass_db;
			
			$ASSESSMENT_SUBJECT = $intranet_db.'.ASSESSMENT_SUBJECT';
			$LEARNING_CATEGORY = $intranet_db.'.LEARNING_CATEGORY';
			$OEA_SUBJECT_INFO = $eclass_db.'.OEA_SUBJECT_INFO';
			
			$conds_JupasSubjectCode = '';
			if ($WithJupasSubjectCodeOnly) {
				$conds_JupasSubjectCode = " And osi.JupasSubjectCode != '' And osi.JupasSubjectCode Is Not Null ";
			}
			
			$conds_RecordStatus = '';
			if ($RecordStatus === '') {
				// do nth
			} 
			else {
				$conds_RecordStatus = " And osi.RecordStatus = '$RecordStatus' ";
			}
			
			if ($CheckStudentHaveScore) {
				$liboea = new liboea();
				$libpf_academic = new libpf_academic();
				
				$Academic_AcademicYearID = $liboea->getAcademic_AcademicYearID();
				$Academic_YearTermID = $liboea->getAcademic_YearTermID();
				
				$AcademicInfoArr = $libpf_academic->Get_Student_Academic_Result($this->getStudentID(), '', $Academic_AcademicYearID, $Academic_YearTermID, $ParentSubjectOnly=1, $IgnoreEmptyScore=1, $IgnoreNoRanking=1);
				$SubjectIDArr = array_values(array_unique(Get_Array_By_Key($AcademicInfoArr, 'SubjectID')));
				
				$conds_SubjectID = " And s.RecordID In ('".implode("','", (array)$SubjectIDArr)."') ";
			}
			
			if($OrderbyJupasCode){
				$conds_Order = "osi.JupasSubjectCode";
			}else{
				$conds_Order = "lc.DisplayOrder, s.DisplayOrder";
			}
			
			$sql = "Select
							s.RecordID as SubjectID,
							osi.JupasSubjectCode,
							s.EN_DES as SubjectNameEn,
							s.CH_DES as SubjectNameCh,
							osi.RecordStatus as ApplyToJupas
					From
							$ASSESSMENT_SUBJECT as s
							Left Outer Join $LEARNING_CATEGORY as lc On (s.LearningCategoryID = lc.LearningCategoryID)
							Inner Join $OEA_SUBJECT_INFO as osi On (s.RecordID = osi.SubjectID)
					Where
							s.RecordStatus = '1'
							And (s.CMP_CODEID Is Null Or s.CMP_CODEID = '')
							$conds_JupasSubjectCode
							$conds_RecordStatus
							$conds_SubjectID
					Order By
							$conds_Order
					";
			$DataArr = $this->objDB->returnArray($sql);
			
			if ($ReturnAsso) {
				return BuildMultiKeyAssoc($DataArr, 'SubjectID'); 
			}
			else {
				return $DataArr;
			}
		}
		
		
		
		public function Get_Subject_Settings_Table($SubjectJupasStatus) {
			global $Lang, $linterface;
			
			$libscm = new subject_class_mapping();
			
			### Get full Subject List
			$SubjectInfoArr = $libscm->Get_Subject_List();
			$numOfSubject = count($SubjectInfoArr);
						
			
			### Get Jupas Subject Info
			$RecordStatus = '';
			switch ($SubjectJupasStatus) {
				case 0:
					$RecordStatus = '';
					break;
				case 1:
					$RecordStatus = 1;
					break;
				case 2:
					$RecordStatus = 0;
					break;
			} 
			$JupasSubjectInfoArr = $this->getSubjectInfoArr($WithJupasSubjectCodeOnly=0, $RecordStatus='', $returnAsso=1);
			$numOfApplyToJupas = 0;
			foreach ((array)$JupasSubjectInfoArr as $thisSubjectID => $thisInfoArr) {
				$thisApplyToJupas = $thisInfoArr['ApplyToJupas'];
				
				if ($thisApplyToJupas == 1) {
					$numOfApplyToJupas++;
				}
			}
			
			$x = '';
			$x .= '<table id="ContentTable" class="common_table_list_v30 edit_table_list_v30">'."\r\n";
				$x .= '<col style="width:1%;">'."\n";
				$x .= '<col style="width:59%;">'."\n";
				$x .= '<col style="width:15%;">'."\n";
				$x .= '<col style="width:25%;">'."\n";
				
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th rowspan="2">#</th>'."\n";
						$x .= '<th rowspan="2">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</th>'."\n";
						$x .= '<th style="text-align:center;">'.$Lang['iPortfolio']['OEA']['AcademicArr']['ApplyToJUPAS'].'</th>'."\n";
						$x .= '<th><span class="row_content_v30">'.$Lang['iPortfolio']['OEA']['AcademicArr']['JUPASSubjectCode'].'</span></th>'."\n";
					$x .= '</tr>'."\n";
					
					$x .= '<tr class="edit_table_head_bulk">'."\n";
						$ID = 'Global_ApplyToJupasChk';
						$OnClick = "js_Change_Global_Apply_To_Jupas_Checkbox(this.checked);";
						$x .= '<th style="text-align:center;">'.$linterface->Get_Checkbox($ID, $Name, $Value, $isChecked=0, $Class='', $Display='', $OnClick).'</th>'."\n";
						$x .= '<th>&nbsp;</th>'."\n";
					$x .= '</tr>'."\n";
				$x .= '<thead>'."\n";
				
				$x .= '<tbody>'."\n";
					if ($SubjectJupasStatus == 1 && $numOfApplyToJupas == 0) {
						$x .= '<tr><td colspan="4" style="text-align:center;">'.$Lang['iPortfolio']['OEA']['AcademicArr']['NoSubjectApplyToJUPAS'].'</td></tr>'."\n";
					}
					else if ($numOfSubject == 0) {
						$x .= '<tr><td colspan="4" style="text-align:center;">'.$Lang['General']['EmptyTableMsg']['NoSubject'].'</td></tr>'."\n";
					}
					else {
						$DisplayIndex = 0;
						for ($i=0; $i<$numOfSubject; $i++) {
							$thisSubjectID = $SubjectInfoArr[$i]['SubjectID'];
							$thisSubjectName = Get_Lang_Selection($SubjectInfoArr[$i]['SubjectDescB5'], $SubjectInfoArr[$i]['SubjectDescEN']);
							
							$thisApplyToJupas = $JupasSubjectInfoArr[$thisSubjectID]['ApplyToJupas'];
							$thisSubjectJupasCode = $JupasSubjectInfoArr[$thisSubjectID]['JupasSubjectCode'];
							
							if ($SubjectJupasStatus == 0) {
								// no filtering
							}
							else if ($SubjectJupasStatus == 1 && !$thisApplyToJupas) {
								// show Apply to JUPAS Subject only => skip Not Apply to JUPAS Subject
								continue;
							}
							else if ($SubjectJupasStatus == 2 && $thisApplyToJupas) {
								// show Not Apply to JUPAS Subject only => skip Apply to JUPAS Subject
								continue;
							}
							
							
							$thisChkID = 'ApplyToJupasChk_'.$thisSubjectID;
							$thisChkName = 'ApplyToJupasArr['.$thisSubjectID.']';
							$thisClass = 'ApplyToJupasChk';
							$thisOnClick = "js_Changed_Apply_To_Jupas_Checkbox(this.checked, '".$thisSubjectID."');";
							$thisCheckbox = $linterface->Get_Checkbox($thisChkID, $thisChkName, $Value=1, $thisApplyToJupas, $thisClass, $Display='', $thisOnClick);
							
							$thisTbID = 'JupasSubjectCodeTb_'.$thisSubjectID;
							$thisTbName = 'JupasSubjectCodeArr['.$thisSubjectID.']';
							$thisOnClick = 'js_Clicked_Subject_Code_Textbox(\''.$thisSubjectID.'\');';
							$thisReadOnly = ($thisApplyToJupas)? '' : 'readonly';
							$thisTextbox = '<input id="'.$thisTbID.'" name="'.$thisTbName.'" class="textboxtext JupasSubjectCodeTb" onclick="'.$thisOnClick.'" subjectID="'.$thisSubjectID.'" value="'.intranet_htmlspecialchars($thisSubjectJupasCode).'" '.$thisReadOnly.' />'."\n";
							
							// Empty Subject Code Warning
							$thisDivID = 'JupasSubjectCodeEmptyWarningMsgDiv_'.$thisSubjectID;
							$thisClass = 'WarningMsgDiv';
							$thisEmptyWarningMsg = $linterface->Get_Form_Warning_Msg($thisDivID, $Lang['iPortfolio']['OEA']['AcademicArr']['JUPASSubjectCodeEmpty'], $thisClass);
							
							// Duplicate Subject Code Warning
							$thisDivID = 'JupasSubjectCodeDuplicateWarningMsgDiv_'.$thisSubjectID;
							$thisClass = 'WarningMsgDiv';
							$thisDuplicateWarningMsg = $linterface->Get_Form_Warning_Msg($thisDivID, $Lang['iPortfolio']['OEA']['AcademicArr']['JUPASSubjectCodeDuplicated'], $thisClass);
							
							$x .= '<tr>'."\n";
								$x .= '<td>'.(++$DisplayIndex).'</td>'."\n";
								$x .= '<td>'.$thisSubjectName.'</td>'."\n";
								$x .= '<td style="text-align:center;">'.$thisCheckbox.'</td>'."\n";
								$x .= '<td>'.$thisTextbox.$thisEmptyWarningMsg.$thisDuplicateWarningMsg.'</td>'."\n";
							$x .= '</tr>'."\n";
						}
					}
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\r\n";
			
			
			$LastModifiedInfoArr = $this->getSubjectInfoLastModified();
			if (count($LastModifiedInfoArr) > 0) {
				$LastModifiedRemarks = Get_Last_Modified_Remark($LastModifiedInfoArr[0]['ModifyDate'], $byUserName='', $LastModifiedInfoArr[0]['ModifiedBy']);
				$x .= '<div><span class="tabletextremark">'.$LastModifiedRemarks.'</span></div>'."\r\n";
			}			
			
			return $x;
		} 
		
		public function Get_Subject_Jupas_Status_Selection($ID, $Name, $SelectedStatus, $OnChange='') {
			global $Lang;
			
			$SelectArr = array();
			$SelectArr['1'] = $Lang['iPortfolio']['OEA']['AcademicArr']['ApplyToJUPAS'];
			$SelectArr['2'] = $Lang['iPortfolio']['OEA']['AcademicArr']['NotApplyToJUPAS'];
			
			$onchange = "";
			if ($OnChange != "")
				$onchange = ' onchange="'.$OnChange.'" ';
				
			$selectionTags = ' id="'.$ID.'" name="'.$Name.'" '.$onchange;
			$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['SubjectClassMapping']['All']['Subject']);
			
			return getSelectByAssoArray($SelectArr, $selectionTags, $SelectedStatus, $isAll=1, $noFirst=0, $firstTitle);
		}
		
		###################################################################################################
		############################# Jupas Subject Settings Functions [End] ##############################
		###################################################################################################
	}
}
?>