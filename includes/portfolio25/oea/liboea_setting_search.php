<?

# modifying : 

/******************************************************************************
 * Modification log:
 * 
 * Yuen (2011-12-02): 
 * - optimized getSuggestResult() by using caching of the different values of activity items
 * 
 * Yuen (2011-11-15): 
 * - improved the ranking accuracy in function getSuggestResult()
 * 
 * 
 *****************************************************************************/


if (!defined("LIBOEA_SETTING_SEARCH"))         // Preprocessor directives
{
	define("LIBOEA_SETTING_SEARCH",true);

	class liboea_setting_search{
		private $objOEA_Setting; //object of liboea_setting
		private $criteria_title;
		private $suggestLimit;
		private $suggestRankLowerLimit;
		private $SearchResultTitle;
		private $criteria_advance_search_title;
		private $advance_search_available_array;
		private $advanceSearchResult;
		private $ResultsetSize;
		private $ActivityArrByLang;
	

		public function liboea_setting_search($objOEA_Setting)
		{
			global $oea_cfg;
			$this->objOEA_Setting = $objOEA_Setting;
			$this->suggestLimit = $oea_cfg["OEA_SuggestListDisplayLimit"];
			$this->suggestRankLowerLimit = $oea_cfg["OEA_SuggestRankLowerLimit"];
			$this->ResultsetSize = 0;
		}

		public function set_setting($objOEA_Setting){	
			$this->objOEA_Setting = $objOEA_Setting;
		}

		public function setCriteriaTitle($str){
			$this->criteria_title = $str;
		}
		public function setResultsetSize($val) {
			$this->ResultsetSize = $val;	
		}
		public function getResultsetSize() {
			return $this->ResultsetSize;	
		}
		public function getSearchResultTitle() {
			return $this->SearchResultTitle;	
		}
		
		public function setAdvanceSearchTextStr($str){
			$this->criteria_advance_search_title = $str;
		}
		
		
		public function getSubStrByLang($itemName, $byLang)
		{
			if ($this->ActivityArrByLang[$itemName][$byLang]!="")
			{
				$p_fine_text = $this->ActivityArrByLang[$itemName][$byLang];
			} else
			{
				$p_fine_text = substr_lang($itemName, $byLang);
				if ($p_fine_text=="")
				{
					$p_fine_text = $itemName;
				}
				$this->ActivityArrByLang[$itemName][$byLang] = $p_fine_text;
			}
			
			return $p_fine_text;
		}

		public function getSuggestResult(){
			global $oea_cfg;
			
			# it seems not in used in following codes, so now is disabled by Yuen on 2011-12-02
			//$data = $this->objOEA_Setting->get_OEA_Item();
			
			$p = 0;
			
			if ($this->criteria_title!="")	// search by text
			{
				$default_oea_item = $this->objOEA_Setting->default_oeaItemOnlyArr;

				$LangInside = str_lang($this->criteria_title);
				
				$rankResultAry = array();
				if (sizeof($default_oea_item)>0)
				{
					//$i = 0;
					foreach($default_oea_item as $_itemCode=>$_itemName)
					{
						if ($_itemCode == $oea_cfg["OEA_Default_Category_Others"])
						{
							// do not suggest "Others" to user
							continue;
						}
						
						$p_fine_text = "";
						if ($LangInside=="CHI")
						{
							# try Chinese
							$p_fine_text = $this->getSubStrByLang($_itemName, "CHI");
							similar_text($this->criteria_title, $p_fine_text, $p);	
						} elseif ($LangInside=="ENG")
						{
							# try English
							$p_fine_text = $this->getSubStrByLang($_itemName, "ENG");
							similar_text($this->criteria_title, $p_fine_text, $p);
						} else
						{
							# mixed case!
							similar_text($this->criteria_title, $_itemName, $p_full);
							similar_text(substr_lang($this->criteria_title, "ENG"), $this->getSubStrByLang($_itemName, "ENG"), $p_eng);
							similar_text(substr_lang($this->criteria_title, "CHI"), $this->getSubStrByLang($_itemName, "CHI"), $p_chi);
							$p = ($p_full>$p_eng)? $p_full : $p_eng;
							$p = ($p>$p_chi)? $p : $p_chi;
						}
						
						if ($p>=$this->suggestRankLowerLimit)
						{
							$rankResultAry[] = array($p,$_itemCode, $_itemName);
							//$i++;
						} 
					}
					
					rsort($rankResultAry);
					$result = array();
					for($j=0; $j<sizeof($rankResultAry) && $j<$this->suggestLimit; $j++)
					{
						$result[$j][] = $rankResultAry[$j][1];
						$result[$j][] = $rankResultAry[$j][2];
					}
					$this->SearchResultTitle = $result;
				}
			}
		}

		public function loadSuggestResultDisplay()
		{
			global $Lang, $button_hide, $i_general_show;
			
			$this->setResultsetSize(sizeof($this->SearchResultTitle));
			
			if($this->criteria_title!="") {	# display result of "Title" search
				if($this->SearchResultTitle=="" || $this->ResultsetSize==0) {
					
					$table .= "<b>- ".$Lang['iPortfolio']['OEA']['NoSuggestedItemReturn']." -</b><br style='clear:both'><br style='clear:both'>";
				} else {
					$result = $this->SearchResultTitle;
					
					if($_SESSION['UserType']==USERTYPE_STAFF) {
						$option = "copyback(this.value);";
					}
					
					$table .= $Lang['iPortfolio']['OEA']['NoOfItemsSuggested']." : ".sizeof($result)."<br style='clear:both'><br style='clear:both'>";
					$table .= "<table class='common_table_list_v30' >";
					
					$table .= "<tr><td><table class='common_table_list_v30' id='suggestionTable'>";
					for($i=0, $i_max=sizeof($result); $i<$i_max; $i++)
					{
						$css = $i%2==0?"row_avaliable" : "";
						$table .= "<tr class='{$css}'><td width='1' valign='top'><input type='radio' name='ItemCode' id='ItemCode_".$result[$i][0]."' value='".$result[$i][0]."' onClick='assignItemToTemp(this.value,\"".intranet_htmlspecialchars(addslashes($result[$i][1]))."\");copyback(document.form2.hiddenItemCode.value);'></td><td width='100%'><label for='ItemCode_".$result[$i][0]."'>[".$result[$i][0]."] ".$result[$i][1]."</label></td></tr>";
					}	
					
					$table .= "<tr><td colspan='2' align='right'></td></tr></table>";
					
					$table .= "</td></tr>";
					$table .= "</table>";
					$table .= "<input type='hidden' name='hiddenItemCode' id='hiddenItemCode' value=''>";
					$table .= "<input type='hidden' name='hiddenItemName' id='hiddenItemName' value=''>";

				}
				
				return $table;
			}
		}
			
		public function setAdvanceAvaliableSearchAry($advanceSearchArray=array()) 
		{
			$this->advance_search_available_array = $advanceSearchArray;
		}
		
		public function getAdvanceSearchTextResult() 
		{
			global $oea_cfg, $liboea;
			
			$availableAry = $this->advance_search_available_array;
			$searchText = $this->criteria_advance_search_title;
			
			if ($_SESSION['UserType']==USERTYPE_STUDENT && $liboea->getAllowStudentApplyOthersOea()==false) {
				// do not allow students to select "Others"
				unset($availableAry[$oea_cfg["OEA_Default_Category_Others"]]);
			}
			
			if($searchText=="") {
				$resultAry = $availableAry;
			} else {
				$max_ary_size = sizeof($availableAry);
				if($max_ary_size>0) {
					$c = 0;
					foreach($availableAry as $_key=>$_ary) {
						$posItemName = stripos($_ary['ItemName'],$searchText);
						$posItemCode = stripos($_ary['ItemCode'],$searchText);
						
						if(is_numeric($posItemName) || is_numeric($posItemCode)) {
							$resultAry[$_key] = $_ary;
						}	
					}	
				}
			}
			
			$this->advanceSearchResult = $resultAry;
		}  
		
		public function displayAdvanceSearchResult($PageNo = NULL, $NumPerPage = NULL)
		{
			global $Lang, $i_no_record_exists_msg, $PATH_WRT_ROOT, $button_submit, $button_cancel, $_SESSION;
			global $IsForOLEEdit;
			include_once($PATH_WRT_ROOT."includes/libpagination.php");			
			include_once($PATH_WRT_ROOT."includes/libinterface.php");
			$linterface = new interface_html();
			
			$resultAry = $this->advanceSearchResult;

			//$divHeight = 330;
			$divHeight = 280;
			if(!$IsForOLEEdit && $_SESSION['UserType']==USERTYPE_STAFF) {
				$option = "copyback(this.value);";
				//$divHeight = 330;
				$divHeight = 220;
			}
			
			$table .= "<div style='width:100%;height:".$divHeight."px;overflow:auto;'>";
			$table .= "<div style='width:95%;'>";
			$table .= $Lang['iPortfolio']['OEA']['NoOfResults']." : ".sizeof($resultAry);
			$table .= "<table class='common_table_list_v30' id='ResultTable' align='center' >";
			$table .= "<tr>";
			$table .= "<th width='1'>&nbsp;</th>";
			$table .= "<th width='1'>&nbsp;</th>";
			$table .= "<th width='15%'>".$Lang['iPortfolio']['OEA']['ProgramCode']."</th>";
			$table .= "<th width='60%'>".$Lang['iPortfolio']['OEA']['Title']."</th>";
			$table .= "<th width='25%'>".$Lang['iPortfolio']['OEA']['Category']."</th>";
			$table .= "</tr>";
			if(sizeof($resultAry)==0) {
				$table .= "<tr><td colspan='100%' height='40' align='center'>$i_no_record_exists_msg</td></tr>";	
			} else {
				
				
				$startRecord = intval((($PageNo -1) * $NumPerPage  ) +1);
				
				$endRecord = intval($PageNo * $NumPerPage);

				$_currentRecord = 0;
				$totalOfRecord =count($resultAry);
				foreach($resultAry as $_key=>$_ary) {
					$_currentRecord = $_currentRecord + 1;
					if($_currentRecord < $startRecord){

						continue; 
					}

					if($_currentRecord > $endRecord){

						break;
					}

					$css = $_currentRecord%2==0?"row_avaliable" : "";
						
					$table .= "<tr class='{$css}' >";
					$table .= "<td><label for='ItemCode2_".$_ary['ItemCode']."'>".$_currentRecord."</label></td>";
					$table .= "<td><input type='radio' name='ItemCode2' id='ItemCode2_".$_ary['ItemCode']."' value='".$_ary['ItemCode']."' onClick='assignItemToTemp(\"".$_ary['ItemCode']."\",\"".intranet_htmlspecialchars(addslashes($_ary['ItemName']))."\");copyback(document.getElementById(\"hiddenItemCode\").value);'><input type='hidden' name='ItemName2' id='ItemName2_".$_ary['ItemCode']."' value='".addslashes($_ary['ItemName'])."'></td>";
					$table .= "<td><label for='ItemCode2_".$_ary['ItemCode']."'>".$_ary['ItemCode']."</label></td>";
					$table .= "<td><label for='ItemCode2_".$_ary['ItemCode']."'>".$_ary['ItemName']."</label></td>";
					$table .= "<td><label for='ItemCode2_".$_ary['ItemCode']."'>".$_ary['CatName']."</label></td>";	
					$table .= "</tr>";
				
				}

				//$button = $linterface->GET_ACTION_BTN($button_submit, "button", "copyback(document.getElementById('hiddenItemCode').value)")."&nbsp;";

				$hidden_field .= "<input type='hidden' name='hiddenItemCode' id='hiddenItemCode' value=''>";
				$hidden_field .= "<input type='hidden' name='hiddenItemName' id='hiddenItemName' value=''>";
			}

			if($totalOfRecord > 0){
				$objPagination = new libpagination();
				$objPagination->setTotalNumOfResult($totalOfRecord);
				$objPagination->setNumPerPage($NumPerPage);
				$objPagination->setCurPage($PageNo);
				$objPagination->setJsFunctionName('goGenerateResult');
				$pageination = $objPagination->getPaginationHtml();
				
				$table .= '<tr><td colspan="5">'.$pageination.'</td></tr>';
			}
			$table .= "</table>";

			$table .= "</div>";
			$table .= "</div>";
			$table .= $hidden_field;			

			


			$table .= "<div class='edit_bottom_v30'>";

			$button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.top.tb_remove();");
			$table .= $button;
			$table .= "</div>";

			return $table;
		}

		
	}

}
?>