<?
# modifying : 

/******************************************************************************
 * Modification log:
 * Yuen (2012-12-08)
 * Optimized for better performance by modifing liboea_setting(), load_OEA_Version() & loadDefaultOEA()
 * 
 * Connie (2011/10/20): 
 * Modified getTeacher_listStuAllInfo_oeaItemTable_HTML(), 
 * getStudentOeaItemInfoArr(), getTeacher_listStuAllInfo_oeaItemTable()
 * 
 * 
 *****************************************************************************/
 
 
include_once($intranet_root."/includes/portfolio25/oea/oeaConfig.inc.php");

if (!defined("LIBOEA_SETTING"))         // Preprocessor directives
{
  //define("LIBOEA_DEFINED",true);

	class liboea_setting extends libdb{
		private $default_oeaVersion;
		private $default_oeaItemArr;
		private $suggestCriteriaText;
		
		public function liboea_setting()
		{
			global $oea_cfg;
			$OEAItemPhpFile = $oea_cfg["OEA_Item_Info_Folder"].$oea_cfg["OEA_Item_Php_File_Name"];
			include($OEAItemPhpFile);
			
			$this->libdb();
			//$this->default_oeaVersion = $this->load_OEA_Version();
			$this->default_oeaVersion = $default_oea_version;
			//$this->default_oeaItemArr = $this->loadDefaultOEA();
			$this->default_oeaItemArr = $default_oea;
			$this->default_oeaCategoryOnlyArr = $this->loadOEADefaultCategory();
			
			$this->default_oeaItemOnlyArr = $this->loadOEADefaultItem();
		}
		
		private function load_OEA_Version()
		{
			return $this->default_oeaVersion;
			
			# no need to read and load each time! should load once when this class is initialized
			/*
			global $oea_cfg;
			$OEAItemPhpFile = $oea_cfg["OEA_Item_Info_Folder"].$oea_cfg["OEA_Item_Php_File_Name"];
			include($OEAItemPhpFile);
			
			return $default_oea_version;	// $default_oea_version is in the $OEAItemPhpFile
			*/
			
		}

		private function loadDefaultOEA()
		{
			return $this->default_oeaItemArr;
			
			# no need to read and load each time! should load once when this class is initialized
			/*
			global $oea_cfg;
			
			$OEAItemPhpFile = $oea_cfg["OEA_Item_Info_Folder"].$oea_cfg["OEA_Item_Php_File_Name"];
			include($OEAItemPhpFile);
			
			
			return $default_oea;	// $default_oea is in the $OEAItemPhpFile
			*/
			
		}
		
		public function get_OEA_Version()
		{
			return $this->default_oeaVersion;
		}
		
		
		public function get_OEA_Item($itemCode="", $withOthersWarning=false)
		{
			global $Lang, $oea_cfg;
			
			$oeaArr = $this->default_oeaItemArr;
			
			if($itemCode!="") {	# only return specific item array
				if(isset($oeaArr[$itemCode])) {
					$returnAry = array($itemCode=>$oeaArr[$itemCode]);
				}	
			} else {			# return all item
				$returnAry = $oeaArr;
			}	
			
			if ($withOthersWarning) {
				// attach warning for "Others" category program
				foreach((array)$returnAry as $_itemCode => $_itemInfoArr) {
					if ($_itemInfoArr['CatCode'] == $oea_cfg["OEA_Default_Category_Others_CatCode"]) {
						$returnAry[$_itemCode]['CatName'] .= '<br><span style=\'color:red;\'><b>'.$Lang['iPortfolio']['OEA']['ImportOtherWarningMsg'].'</b></span>';	// don't use double quote as it will break the js
					}
				}
			}
			
			return $returnAry;
		} 
		
		public function get_OEA_Default_Category($returnAssociatedArry=1, $include_others=1)
		{
			if($returnAssociatedArry)				
				return $this->default_oeaCategoryOnlyArr;
			else { 
				if(sizeof($this->default_oeaCategoryOnlyArr)>0) {
					$c = 0;
					$ary = array();
					foreach($this->default_oeaCategoryOnlyArr as $key=>$val) {
						if($key!="06" || ($key=="06" && $include_others)) {
							$array[$c][] = $key;
							$array[$c][] = $val;
							$c++;
						}
					}
				}
				return $array;	
			}
		}
		
		private function loadOEADefaultCategory() 
		{
			$default_oea = $this->loadDefaultOEA();
			$returnAry = array();
			$c = 0;
			if(sizeof($default_oea)>0) {
				foreach($default_oea as $key=>$ary) {
					
					if(!in_array($ary['CatCode'],array_keys($returnAry))) {
						$returnAry[$ary['CatCode']] = $ary['CatName'];
						$c++;
					}
				}
			}
			return $returnAry;
		}

		public function get_OEA_Default_Item($returnAssociatedArry=1)
		{
			if($returnAssociatedArry)				
				return $this->default_oeaItemOnlyArr;
			else { 
				if(sizeof($this->default_oeaItemOnlyArr)>0) {
					$c = 0;
					$ary = array();
					foreach($this->default_oeaItemOnlyArr as $key=>$val) {
						$array[$c][] = $key;
						$array[$c][] = $val;
						$c++;
					}
				}
				return $array;	
			}
		}
		
		private function loadOEADefaultItem() 
		{
			$default_oea = $this->loadDefaultOEA();
			$returnAry = array();

			if(sizeof($default_oea)>0) {
				foreach($default_oea as $key=>$ary) {

					
					//if(!in_array($ary['ItemCode'],array_keys($returnAry))) {
					if(!array_key_exists($ary['ItemCode'], $returnAry)) {
						$returnAry[$ary['ItemCode']] = $ary['ItemName'];

					}
				}
			}
			return $returnAry;
		}
		
		public function getOEADefaultItemByCategory($CatCode) 
		{
			$default_oea = $this->loadDefaultOEA();
			$returnAry = array();
			//$c = 0;
			
			if($CatCode=="") {
				$returnAry = $default_oea;
			} else {
				if(sizeof($default_oea)>0) {
					foreach($default_oea as $key=>$ary) {
						if($ary['CatCode']==$CatCode) {
							$returnAry[$key]['ItemCode'] = $ary['ItemCode'];
							$returnAry[$key]['ItemName'] = $ary['ItemName'];
							$returnAry[$key]['CatCode'] = $ary['CatCode'];
							$returnAry[$key]['CatName'] = $ary['CatName'];
							//$c++;		
						}
					}
				}	
			}
			return $returnAry;
		}
		
		public function getOEACategoryInfoByOEAItemCode($OEAItemCode)
		{
			$ItemInfoArr = $this->get_OEA_Item($OEAItemCode);
			
			$ReturnArr = array();
			$ReturnArr['CatCode'] = $ItemInfoArr[$OEAItemCode]['CatCode'];
			$ReturnArr['CatName'] = $ItemInfoArr[$OEAItemCode]['CatName'];
			
			return $ReturnArr;
		}
		
		public function getOeaItemTempTable()
		{
			
		}
		
		public function getDefaultMappedOEACode($OLE_ProgramID)
		{
			global $eclass_db;
			$sql = "SELECT OEA_ProgramCode FROM {$eclass_db}.OEA_OLE_MAPPING WHERE OLE_ProgramID='$OLE_ProgramID'";	
			$result = $this->returnVector($sql);
			
			return $result[0];
		}
	}

}
?>