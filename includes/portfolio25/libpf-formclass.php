<?php
/*
 * Editing by 
 * 
 * Modification Log:
 * 2017-05-31 Omas
 * 		- modified GET_ACTIVATED_STUDENT_LIST() - add academicYear Conditions for inner join to get correct number -#Z117719 
 * 2016-12-23 Omas
 * 		- modified GET_STUDENT_LIST() - add param
 * 2014-02-20 Jason
 * 		- edit GET_STUDENT_DETAIL_LIST() to join the Year Form and order by form
 * 2011-01-11 Ivan
 * 		- modified GET_ACTIVATED_STUDENT_LIST(), added para $ActiveStudentOnly to count the current student only so that the number of activated portfolio should be less than the total number of students
 */
 
include_once($intranet_root."/includes/libdb.php");
class libpf_formclass {

  private $db;
  private $YearID;
  private $YearClassID;
  
  # Initializing class variables to prevent throwing exceptions in get/set functions
  public function __construct(){
    $this->db = new libdb();
    $this->YearID = "";
    $this->YearClassID = "";
  }
  
	function getClassListArray($class_teach_arr = ""){
		$t_classlevel_arr = $this->GET_CLASSLEVEL_LIST();
		for($i=0; $i<count($t_classlevel_arr); $i++)
		{
			$t_classlevel_id = $t_classlevel_arr[$i]["YearID"];
			$t_classlevel_name = $t_classlevel_arr[$i]["YearName"];
			
			$this->SET_CLASS_VARIABLE("YearID", $t_classlevel_id);
			$t_class_arr = $this->GET_CLASS_LIST();
			
			for($j=0; $j<count($t_class_arr); $j++)
			{
				$t_yc_id = $t_class_arr[$j][0];
				$t_yc_title = Get_Lang_Selection($t_class_arr[$j]['ClassTitleB5'], $t_class_arr[$j]['ClassTitleEN']);
				if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
				{
					$class_arr[$t_classlevel_name][] = array($t_yc_id, $t_yc_title);
				}
			}
		}
		return $class_arr;
	}
	
  public function GET_CLASS_LIST()
  {
    global $intranet_db;
    
    $year_id = $this->YearID;
    $cond = ($year_id == "") ? "" : " AND yc.YearID = ".$year_id;
    
    $sql =  "
              SELECT DISTINCT
                yc.YearClassID,
                yc.ClassTitleEN,
                yc.ClassTitleB5
              FROM
                {$intranet_db}.YEAR_CLASS yc
              INNER JOIN
                {$intranet_db}.YEAR y
              ON
                y.YearID = yc.YearID
              WHERE
                yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
                $cond
              ORDER BY
                y.Sequence, yc.Sequence
            ";
    $returnArr = $this->db->returnArray($sql);
 
    return $returnArr;
  }

  public function GET_CLASSLEVEL_LIST()
  {
    global $intranet_db;
    
    $sql =  "
              SELECT DISTINCT
                y.YearID,
                y.YearName
              FROM
                {$intranet_db}.YEAR AS y
              INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
                ON  y.YearID = yc.YearID
              WHERE
                yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
              ORDER BY
                y.Sequence
            ";
    $returnArr = $this->db->returnArray($sql);
  
    return $returnArr;
  }
  
  # This function retrieves student User ID only
  # Don't add fields to query
  # To retrieve more fields, use GET_STUDENT_DETAIL_LIST() instead
  public function GET_ACTIVATED_STUDENT_LIST($ActiveStudentOnly=0)
  {
    global $intranet_db, $eclass_db;
  
  	$Inner_Join_INTRANET_USER = '';
  	if ($ActiveStudentOnly == 1)
  		$Inner_Join_INTRANET_USER = " Inner Join {$intranet_db}.INTRANET_USER as iu On (ps.UserID = iu.UserID) ";
  	
    $cond = "";
    $cond .= ($this->YearID == "") ? "" : " AND yc.YearID = ".$this->YearID;
    $cond .= ($this->YearClassID == "") ? "" : " AND yc.YearClassID in (".$this->YearClassID.")";
    $cond .= ($ActiveStudentOnly == 0) ? "" : " AND iu.RecordType = 2 And iu.RecordStatus = 1 ";;
  
    $sql =  "
              SELECT
                Distinct (ps.UserID)
              FROM
                {$eclass_db}.PORTFOLIO_STUDENT AS ps
              INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
                ON ps.UserID = ycu.UserID
              INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
                ON ycu.YearClassID = yc.YearClassID And yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
			  $Inner_Join_INTRANET_USER
              WHERE
                1
                $cond
            ";
    $returnArr = $this->db->returnVector($sql);
    return $returnArr;
  }
  
  # This function retrieves student User ID only
  # Don't add fields to query
  # To retrieve more fields, use GET_STUDENT_DETAIL_LIST() instead
  public function GET_STUDENT_LIST($activatedPorfolioOnly=false, $haveClassOnly=false, $yearClassIDArr='')
  {
    global $intranet_db, $eclass_db;
  
    $cond = "";
    $cond .= ($this->YearID == "") ? "" : " AND yc.YearID = ".$this->YearID;
    //$cond .= ($this->YearClassID == "") ? "" : " AND yc.YearClassID = ".$this->YearClassID;
    $cond .= ($this->YearClassID == "") ? "" : " AND yc.YearClassID in (".$this->YearClassID.")";

	if ($activatedPorfolioOnly) {
		$innerJoin_PORTFOLIO_STUDENT = " Inner Join {$eclass_db}.PORTFOLIO_STUDENT as ps On (iu.UserID = ps.UserID) ";
		$cond_IsSuspend = " And ps.IsSuspend = 0 ";
	}
	
	if ($haveClassOnly) {
		$joinType = ' INNER JOIN ';
		$cond_currentAcademicYearClass = " And yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' ";
	}
	else {
		$joinType = ' LEFT JOIN ';
	}
	
	if(!empty($yearClassIDArr)){
		$cond_class = " AND ycu.YearClassID IN ('".implode("','", (array)$yearClassIDArr )."')";
	}

    $sql =  "
              SELECT
                Distinct (iu.UserID)
              FROM
                {$intranet_db}.INTRANET_USER AS iu
			  	$innerJoin_PORTFOLIO_STUDENT
              	$joinType {$intranet_db}.YEAR_CLASS_USER AS ycu ON iu.UserID = ycu.UserID
              	$joinType {$intranet_db}.YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID
              WHERE
                iu.RecordType = 2 AND
                iu.RecordStatus = 1
                $cond
				$cond_IsSuspend
				$cond_currentAcademicYearClass
				$cond_class
            ";
    $returnArr = $this->db->returnVector($sql);

    return $returnArr;
  }
  
  # Get more fields of students
  public function GET_STUDENT_DETAIL_LIST($ParStudentIDArr)
  {
    global $intranet_db;
    
    $sql =  "
              SELECT
                iu.UserID,
                ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")." AS ClassName,
                iu.ClassNumber,
                iu.EnglishName,
                iu.ChineseName
              FROM
                {$intranet_db}.INTRANET_USER AS iu
              INNER JOIN
                {$intranet_db}.YEAR_CLASS_USER AS ycu
              ON
                iu.UserID = ycu.UserID
              INNER JOIN
                {$intranet_db}.YEAR_CLASS AS yc
              ON
                yc.YearClassID = ycu.YearClassID
			  INNER JOIN 
				{$intranet_db}.YEAR as y 
			  ON 
				y.YearID = yc.YearID
              WHERE
                iu.UserID IN (".implode(',', $ParStudentIDArr).") AND
                yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
              ORDER BY
                y.Sequence, yc.Sequence, iu.ClassNumber
            ";
    $returnArr = $this->db->returnArray($sql);

    return $returnArr;
  }

  # $ParVariableName is case-sensitive
  public function SET_CLASS_VARIABLE($ParVariableName, $ParValue)
  {
    # Prevent declaring new variables to a class
    try
    {
      if(!isset($this->{$ParVariableName}))
      {
        throw new Exception("Class variable \"{$ParVariableName}\" is not declared!!");
      }
      else{
        $this->{$ParVariableName} = $ParValue;
      }
    }
    catch (Exception $e) {
      $error_trace = current(debug_backtrace());
    
      echo "Caught exception: ",  $e->getMessage(), "<br />\n";
      echo "In script: ",  $error_trace["file"], "<br />\n";
      echo "Line no: ",  $error_trace["line"], "<br />\n";
      echo "Method called: ", $error_trace["function"], " @ ", $error_trace["class"], "<br />\n";
      DIE();
    }
  }
  
  # $ParVariableName is case-sensitive
  public function GET_CLASS_VARIABLE($ParVariableName)
  {
    try
    {
      if(!isset($this->{$ParVariableName}))
      {
        throw new Exception("Class variable \"{$ParVariableName}\" is not declared!!");
      }
      else{
        return $this->{$ParVariableName};
      }
    }
    catch (Exception $e) {
      $error_trace = current(debug_backtrace());
    
      echo "Caught exception: ",  $e->getMessage(), "<br />\n";
      echo "In script: ",  $error_trace["file"], "<br />\n";
      echo "Line no: ",  $error_trace["line"], "<br />\n";
      echo "Method called: ", $error_trace["function"], " @ ", $error_trace["class"], "<br />\n";
      DIE();
    }
  }
  
}

?>