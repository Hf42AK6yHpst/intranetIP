<?php

include_once($intranet_root."/includes/libdb.php");

class libpf_account {

  protected $db;
  protected $course_id;
  protected $course_db;
  protected $user_email;
  protected $user_id;
  protected $course_user_id;

  public function __construct(){
    global $eclass_prefix, $ck_course_id;
  
    /* Eric Yip: Assume login ipf -> course_id in session
    $lpf = new libportfolio();
    $course_id = $lpf->GET_COURSE_ID();
    */
    $this->db = new libdb();
  
    $this->course_id = $ck_course_id;
    $this->course_db = $eclass_prefix."c".$ck_course_id;
    $this->user_email = "";
    $this->user_id = "";
    $this->course_user_id = "";
  }

  # Set course user ID by eMail
  public function SET_COURSE_USER_ID(){
  
    $t_course_user_id = $this->GET_CLASS_VARIABLE("course_user_id");
  
    if(empty($t_course_user_id))
    {
      $sql =  "
                SELECT
                  user_id
                FROM
                  {$this->course_db}.usermaster
                WHERE
                  user_email = '".$this->GET_CLASS_VARIABLE("user_email")."' AND
                  (status IS NULL OR status != 'deleted')
              ";
      $returnVal = $this->db->returnVector($sql);
  
      $this->SET_CLASS_VARIABLE("course_user_id", $returnVal[0]);
    }
  }
  
  public function SET_USER_ID(){
    global $intranet_db;
  
    $t_user_id = $this->GET_CLASS_VARIABLE("user_id");
  
    if(empty($t_user_id))
    {
      $sql =  "
                SELECT
                  UserID
                FROM
                  {$intranet_db}.INTRANET_USER
                WHERE
                  UserEmail = '".$this->GET_CLASS_VARIABLE("user_email")."'
              ";
      $returnVal = $this->db->returnVector($sql);
      
      $this->SET_CLASS_VARIABLE("user_id", $returnVal[0]);
    }
  }
  
  public function SET_USER_EMAIL(){
    global $intranet_db;
  
    $t_user_email = $this->GET_CLASS_VARIABLE("user_email");
  
    if(empty($t_user_email))
    {
      $sql =  "
                SELECT
                  UserEmail
                FROM
                  {$intranet_db}.INTRANET_USER
                WHERE
                  UserID = '".$this->GET_CLASS_VARIABLE("user_id")."'
              ";
      $returnVal = $this->db->returnVector($sql);
      
      $this->SET_CLASS_VARIABLE("user_email", $returnVal[0]);
    }
  }
/*  
  function GET_USER_IDENTITY(){
    return $_SESSION['iPortfolio']['portfolio_usertype'];
  }
*/  
  public function SET_CLASS_VARIABLE($ParVariableName, $ParValue)
  {
    $this->{$ParVariableName} = $ParValue;
  }
  
  public function GET_CLASS_VARIABLE($ParVariableName)
  {
    return $this->{$ParVariableName};
  }
}


?>