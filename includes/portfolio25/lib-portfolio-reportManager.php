<?php 
class ipfReportManager{
	protected $yearClassID;
	protected $studentID;
	protected $academicYearID;

	protected $objDB;
	protected $eclass_db;
	protected $intranet_db;

	public function ipfReportManager(){
		global $eclass_db,$intranet_db;
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;

	}

	public function setYearClassID($yearClassID){
		$this->yearClassID = $yearClassID;
	}
	public function setAcademicYearID($academicYearID){

		if(!is_array($academicYearID)){
			$this->academicYearID = array($academicYearID);
		}else{
			$_tmpID = array();
			for($i = 0,$i_max=sizeof($academicYearID);$i< $i_max;$i++){
				//remove the yearid in case select the "ALL year" checkbox
				if(trim($academicYearID[$i]) != ""){
					$_tmpID[] = $academicYearID[$i];
				}
			}
			$this->academicYearID = $_tmpID;
//			debug_r($this->academicYearID);
		}
	}
	public function getAcademicYearID(){
		return $this->academicYearID;
	}
	public function setStudentID($studentID){
		$this->studentID = $studentID;
	}
	public function getAcademicYearDisplayMultiple(){
		$yearIDStr = implode(',',$this->academicYearID);
		$sql = "select YearNameEN from ".$this->intranet_db.".ACADEMIC_YEAR where AcademicYearID in ({$yearIDStr}) order by Sequence desc";

		$result = $this->objDB->returnArray($sql);
		//debug_r($result);
		for($i = 0,$i_max = sizeof($result);$i< $i_max;$i++){
			//confirm with client , assume all the academic year format is yyyy-yyyy
			$_tmp = explode("-",trim($result[$i]["YearNameEN"]));

			if(sizeof($_tmp) == 2){
				$yearIDStack[] = trim($_tmp[0]);
				$yearIDStack[] = trim($_tmp[1]);
			}
		}
		$endYearPointer = sizeof($yearIDStack)-1;
		$yearStartEndDisplay  = $yearIDStack[0]." - ".$yearIDStack[$endYearPointer];
		return $yearStartEndDisplay;
	}

	public function getAcademicYearDisplay(){

		$yearIDStr = implode(',',$this->academicYearID);

		$sql = "select YearNameEN as 'YearNameEN' from ".$this->intranet_db.".ACADEMIC_YEAR where AcademicYearID in({$yearIDStr})";

		$result = $this->objDB->returnArray($sql);
		return $result[0]["YearNameEN"];
	}

	public function setIssueDate($issuedate){
		$this->issuedate = $issuedate;
	}
	public function getIssueDate(){
		$dateAry = explode('-',$this->issuedate);
		$_year =$dateAry[0]; 
		$_month =$dateAry[1]; 
		$_day =$dateAry[2]; 

		$returnDate = $_day."-".$_month."-".$_year;

		return $returnDate;
	}
	public function outPutToHTML($content){
		$output = "<html>\n";
    $output .= "<head>\n";
    $output .= returnHtmlMETA()."\n";
//    $output .= "<link href=\"http://{$eclass_httppath}/src/includes/css/style_portfolio_report.css\" rel=\"stylesheet\" type=\"text/css\">\n";
//    $output .= "<link href=\"http://{$eclass_httppath}/src/includes/css/style_portfolio_table.css\" rel=\"stylesheet\" type=\"text/css\">\n";
    $output .= "</head>\n";
    $output .= "<body>\n";
    $output .= $content."\n";
    $output .= "</body>\n";
    $output .= "</html>\n";
	return $output;
	}
	public function outPutToWord($report_html,$fileName=""){


		$ContentType = "application/x-msword";
		$fileName = (trim($fileName) == "")? "export_word_file".date("ymd").".doc" :$fileName;
		$tags_to_strip = Array("A", "a", "HTML", "html", "BODY", "body");
		foreach ($tags_to_strip as $tag)
		{
			$report_html = preg_replace("/<\/?" . $tag . "(.|\s)*?>/", "", $report_html);
		}
		
		$output = "<HTML>\n";
		$output .= "<HEAD>\n";
		$output .= returnHtmlMETA();
		$output .= "</HEAD>\n";
		$output .= "<BODY LANG=\"zh-HK\">\n";
		$output .= $report_html;
		$output .= "</BODY>\n";
		$output .= "</HTML>\n";

		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

		header('Content-type: '.$ContentType);
		header('Content-Length: '.strlen($output));

		header('Content-Disposition: attachment; filename="'.$fileName.'";');

		echo $output;

	}
	public function getStudentIDList($ActivatedOnly=false){

		if(is_array($this->studentID) && sizeof($this->studentID)>0)
		{

			$StudentArr = $this->studentID;
		} elseif($this->studentID != "")
		{

			$StudentArr = array($this->studentID);
		}
		else if($this->yearClassID != "")
		{
		  # The GET_CLASS_TEACHER_STUDENT get the users from "YEAR_CLASS_USER" that does not join "YEAR_CLASS_TEACHER"
		  if($ActivatedOnly){
		  	$t_StudentArr = GET_CLASS_TEACHER_STUDENT($this->yearClassID, true);
		  }
		  else{
		  	$t_StudentArr = GET_CLASS_TEACHER_STUDENT($this->yearClassID);
		  }

		  for($i=0; $i<count($t_StudentArr); $i++)
		  {
			$StudentArr[] = $t_StudentArr[$i]['UserID'];
		  }
		}

		return $StudentArr;
	}
}
?>