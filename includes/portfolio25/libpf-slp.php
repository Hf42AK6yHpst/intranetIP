<?php
// modifying : 

/************************************************
 * Date: 2020-05-21 (Bill)  [2020-0521-1400-38206]
 * Details: add group_by to libpf_attendance
 * Date: 2013-03-26 (Rita)
 * Details: add function Get_Student_DynamicReport_DBTable_Sql()
 *************************************************/

class libpf_activity {

  private $db;
  private $fullActivityList;
  
  public function __construct(){
    $this->db = new libdb();
  }
  
  # Get all data from table
  # Please use other functions for retrieving specific columns
  private function getActivityFullList(){
    global $eclass_db;
    
    $sql =  "
              SELECT
                RecordID,
                UserID,
                AcademicYearID,
                Year,
                YearTermID,
                Semester,
                IsAnnual,
                ClassName,
                ClassNumber,
                ActivityName,
                Role,
                Performance,
                Organization,
                Remark,
                InputDate,
                ModifiedDate
              FROM
                {$eclass_db}.ACTIVITY_STUDENT
              ORDER BY
                Year DESC, Semester
            ";
    $returnArr = $this->db->returnArray($sql);
    
    return $returnArr;
  }

  public function getActivityYearTerm() {
    global $eclass_db;
  
    if(!is_array($this->fullActivityList))
    {
      $this->fullActivityList = $this->getActivityFullList();
    }
    $fullActivityList = $this->fullActivityList;
    
    $year_term_arr = array();
    for($i=0; $i<count($fullActivityList); $i++)
    {
      $t_year = $fullActivityList[$i]['Year'];
      $t_semester = $fullActivityList[$i]['Semester'];
      
      if(!is_array($year_term_arr[$t_year]) || !in_array($t_semester, $year_term_arr[$t_year]))
      {
        $year_term_arr[$t_year][] = $t_semester;
      }
    }
    
    return $year_term_arr;
  }
}

class libpf_attendance {

  private $db;
  private $fullAttendanceList;
  
  public function __construct(){
    $this->db = new libdb();
  }
  
  # Get all data from table
  # Please use other functions for retrieving specific columns
  private function getAttendanceFullList($groupByYearTerm=false) {
    global $eclass_db;

    // [2020-0521-1400-38206]
    $group_by = '';
    if($groupByYearTerm) {
        $group_by = ' GROUP BY Year, Semester ';
    }
    
    $sql =  "SELECT
                RecordID,
                UserID,
                AcademicYearID,
                Year,
                YearTermID,
                Semester,
                IsAnnual,
                ClassName,
                ClassNumber,
                AttendanceDate,
                Periods,
                DayType,
                Reason,
                RecordType,
                RecordStatus,
                Remark,
                InputDate,
                ModifiedDate
              FROM
                {$eclass_db}.ATTENDANCE_STUDENT
              $group_by
              ORDER BY
                Year DESC, Semester
            ";
    $returnArr = $this->db->returnArray($sql);
    
    return $returnArr;
  }

  public function getAttendanceYearTerm($groupByYearTerm=false) {
    global $eclass_db;

    if (!is_array($this->fullAttendanceList)) {
        $this->fullAttendanceList = $this->getAttendanceFullList($groupByYearTerm);
    }
    $fullAttendanceList = $this->fullAttendanceList;
    
    $year_term_arr = array();
    for($i=0; $i<count($fullAttendanceList); $i++)
    {
      $t_year = $fullAttendanceList[$i]['Year'];
      $t_semester = $fullAttendanceList[$i]['Semester'];
      
      if(!is_array($year_term_arr[$t_year]) || !in_array($t_semester, $year_term_arr[$t_year]))
      {
        $year_term_arr[$t_year][] = $t_semester;
      }
    }
    
    return $year_term_arr;
  }
}

class libpf_award {

  private $db;
  private $fullAwardList;
  
  public function __construct(){
    $this->db = new libdb();
  }
  
  # Get all data from table
  # Please use other functions for retrieving specific columns
  private function getAwardFullList(){
    global $eclass_db;
    
    
    $sql =  "
              SELECT
                RecordID,
                UserID,
                AcademicYearID,
                Year,
                YearTermID,
                Semester,
                IsAnnual,
                ClassName,
                ClassNumber,
                AwardDate,
                AwardName,
                AwardFile,
                Details,
                Organization,
                SubjectArea,
                Remark,
                RecordType,
                RecordStatus,
                ProcessDate,
                InputDate,
                ModifiedDate
              FROM
                {$eclass_db}.AWARD_STUDENT
              ORDER BY
                Year DESC, Semester
            ";
    $returnArr = $this->db->returnArray($sql);
    
    return $returnArr;
  }

  public function getAwardYearTerm() {
    global $eclass_db;
  
    if(!is_array($this->fullAwardList))
    {
      $this->fullAwardList = $this->getAwardFullList();
    }
    $fullAwardList = $this->fullAwardList;
    
    $year_term_arr = array();
    for($i=0; $i<count($fullAwardList); $i++)
    {
      $t_year = $fullAwardList[$i]['Year'];
      $t_semester = $fullAwardList[$i]['Semester'];
      
      if(!is_array($year_term_arr[$t_year]) || !in_array($t_semester, $year_term_arr[$t_year]))
      {
        $year_term_arr[$t_year][] = $t_semester;
      }
    }
    
    return $year_term_arr;
  }
  
  public function getAwardStudentList($StudentIDArr=''){
    global $eclass_db;    
    
    if($StudentIDArr!='')
    {
    	$cond_student ="And UserID In ('".implode("','", (array)$StudentIDArr)."')";
    }
    
    $sql =  "
              SELECT
                UserID,
                AwardName,
                Organization
              FROM
                {$eclass_db}.AWARD_STUDENT
			  WHERE
					1
					$cond_student
              ORDER BY
                Year DESC, Semester, AwardName
            ";
    $returnArr = $this->db->returnArray($sql);
 
    return $returnArr;
  }
}

class libpf_teacher_comment {

  private $db;
  private $fullTeacherCommentList;
  
  public function __construct(){
    $this->db = new libdb();
  }
  
  # Get all data from table
  # Please use other functions for retrieving specific columns
  private function getTeacherCommentFullList(){
    global $eclass_db;
    
    $sql =  "
              SELECT
                RecordID,
                UserID,
                AcademicYearID,
                Year,
                YearTermID,
                Semester,
                IsAnnual,
                ClassName,
                ClassNumber,
                ConductGradeChar,
                CommentChi,
                CommentEng,
                InputDate,
                ModifiedDate
              FROM
                {$eclass_db}.CONDUCT_STUDENT
              ORDER BY
                Year DESC, Semester
            ";
    $returnArr = $this->db->returnArray($sql);
    
    return $returnArr;
  }

  public function getTeacherCommentYearTerm() {
    global $eclass_db;
  
    if(!is_array($this->fullTeacherCommentList))
    {
      $this->fullTeacherCommentList = $this->getTeacherCommentFullList();
    }
    $fullTeacherCommentList = $this->fullTeacherCommentList;
    
    $year_term_arr = array();
    for($i=0; $i<count($fullTeacherCommentList); $i++)
    {
      $t_year = $fullTeacherCommentList[$i]['Year'];
      $t_semester = $fullTeacherCommentList[$i]['Semester'];
      
      if(!is_array($year_term_arr[$t_year]) || !in_array($t_semester, $year_term_arr[$t_year]))
      {
        $year_term_arr[$t_year][] = $t_semester;
      }
    }
    
    return $year_term_arr;
  }
}

class libpf_merit {

  private $db;
  private $fullMeritList;
  
  public function __construct(){
    $this->db = new libdb();
  }
  
  # Get all data from table
  # Please use other functions for retrieving specific columns
  private function getMeritFullList(){
    global $eclass_db;
    
    $sql =  "
              SELECT
                RecordID
                UserID,
                AcademicYearID,
                Year,
                YearTermID,
                Semester,
                IsAnnual,
                ClassName,
                ClassNumber,
                MeritDate,
                NumberOfUnit,
                Reason,
                PersonInCharge,
                RecordType,
                RecordStatus,
                Remark,
                InputDate,
                ModifiedDate
              FROM
                {$eclass_db}.MERIT_STUDENT
              ORDER BY
                Year DESC, Semester
            ";
    $returnArr = $this->db->returnArray($sql);
    
    return $returnArr;
  }

  public function getMeritYearTerm() {
    global $eclass_db;
  
    if(!is_array($this->fullMeritList))
    {
      $this->fullMeritList = $this->getMeritFullList();
    }
    $fullMeritList = $this->fullMeritList;
    
    $year_term_arr = array();
    for($i=0; $i<count($fullMeritList); $i++)
    {
      $t_year = $fullMeritList[$i]['Year'];
      $t_semester = $fullMeritList[$i]['Semester'];
      
      if(!is_array($year_term_arr[$t_year]) || !in_array($t_semester, $year_term_arr[$t_year]))
      {
        $year_term_arr[$t_year][] = $t_semester;
      }
    }
    
    return $year_term_arr;
  }
}

class libpf_service {

  private $db;
  private $fullServiceList;
  
  public function __construct(){
    $this->db = new libdb();
  }
  
  # Get all data from table
  # Please use other functions for retrieving specific columns
  private function getServiceFullList(){
    global $eclass_db;
    
    $sql =  "
              SELECT
                RecordID,
                UserID,
                AcademicYearID,
                Year,
                YearTermID,
                Semester,
                ServiceDate,
                ServiceName,
                Role,
                Performance,
                Organization,
                Hours,
                Remark,
                ClassName,
                ClassNumber,
                InputDate,
                ModifiedDate
              FROM
                {$eclass_db}.SERVICE_STUDENT
              ORDER BY
                Year DESC, Semester
            ";
    $returnArr = $this->db->returnArray($sql);
    
    return $returnArr;
  }

  public function getServiceYearTerm() {
    global $eclass_db;
  
    if(!is_array($this->fullServiceList))
    {
      $this->fullServiceList = $this->getServiceFullList();
    }
    $fullServiceList = $this->fullServiceList;
    
    $year_term_arr = array();
    for($i=0; $i<count($fullServiceList); $i++)
    {
      $t_year = $fullServiceList[$i]['Year'];
      $t_semester = $fullServiceList[$i]['Semester'];
      
      if(!is_array($year_term_arr[$t_year]) || !in_array($t_semester, $year_term_arr[$t_year]))
      {
        $year_term_arr[$t_year][] = $t_semester;
      }
    }
    
    return $year_term_arr;
  }
}


class libpf_self_account{
	
	  public function __construct(){
	    $this->db = new libdb();
	  }
	
	public function getSelfAccountsByUserIds($ParUserIds,$IsDefault=false) {
		global $intranet_db,$eclass_db,$ipf_cfg,$intranet_session_language;
		
		if (isset($ParYearClassUserIds) && !is_array($ParYearClassUserIds)) {
			$ParYearClassUserIds = array($ParYearClassUserIds);
		}
		
//		if (strtoupper($intranet_session_language)=="EN") {
//			$userNameField = "IU.ENGLISHNAME";
//		} else {
//			$userNameField = "IU.CHINESENAME";
//		}

		if($IsDefault)
		{
			$cond_defaultSA = 'And DefaultSA is not null';
		}

		$userNameField = Get_Lang_Selection("IU.CHINESENAME", "IU.ENGLISHNAME");
		
		$sql = "SELECT
					SAS.RECORDID,
					SAS.DEFAULTSA,
					YC.CLASSTITLE".strtoupper($intranet_session_language)." AS CLASSTITLE,
					IU.CLASSNUMBER,
					$userNameField AS USERNAME,
					SAS.TITLE,
					SAS.DETAILS,
					SAS.RECORDSTATUS,
					SAS.APPROVEDBY,
					SAS.APPROVEDDATE,
					IU.USERID,
					YCU.YEARCLASSUSERID,
					SAS.DEFAULTSA
				FROM {$intranet_db}.INTRANET_USER IU
					LEFT JOIN {$eclass_db}.SELF_ACCOUNT_STUDENT SAS
					ON SAS.USERID = IU.USERID
					INNER JOIN {$intranet_db}.YEAR_CLASS_USER YCU
					ON IU.USERID = YCU.USERID
					INNER JOIN {$intranet_db}.YEAR_CLASS YC
					ON YCU.YEARCLASSID = YC.YEARCLASSID
					INNER JOIN {$intranet_db}.YEAR Y
					ON YC.YEARID = Y.YEARID
				WHERE 
					YCU.USERID IN ('".implode('","',(array)$ParUserIds)."')
					$cond_defaultSA
				ORDER BY 
					Y.SEQUENCE, 
					YC.SEQUENCE, 
					IU.CLASSNUMBER";
		$resultArray = $this->db->returnArray($sql);
//debug_r($sql);
		$returnArray = array();
		if (is_array($resultArray)) {
			foreach($resultArray as $key=>$element) {
				if ($element["DEFAULTSA"] == $ipf_cfg["DB_SELF_ACCOUNT_STUDENT_DefaultSA"]["SLP"]) {
					$returnArray[$element["USERID"]]["HAS_DEFAULT"] = true;
					$returnArray[$element["USERID"]]["ELEMENT"] = $element;
				} else if ($returnArray[$element["USERID"]]["HAS_DEFAULT"] == true) {
					// do nothing
				} else {
					$returnArray[$element["USERID"]]["HAS_DEFAULT"] = false;
					$returnArray[$element["USERID"]]["ELEMENT"] = $element;
				}
			}
		}
		return $returnArray; //WHERE YCU.YEARCLASSUSERID IN (".implode(",",$ParYearClassUserIds).")
	}
	
	
	
	public function getSelfAccounts_MaxCount($ParYearClassID='',$groupIds='') 
	{
		global $intranet_db,$eclass_db,$ipf_cfg,$intranet_session_language,$ck_course_id;
	
		$IsStudent=2;
		$IsApproved=1;

		$keyword = addslashes($keyword);
		$currentAcademicYearId = Get_Current_Academic_Year_ID();
		$userNameField = getNameFieldByLang("IU.");
		$approvedByNameField = getNameFieldByLang("IU2.");
		if (!empty($ParYearClassID)) {
			$cond = " AND YC.YEARCLASSID = '$ParYearClassID' ";
		}
		
		$coursedb = classNamingDB($ck_course_id);
		if (!empty($groupIds)) {
			$groupsCond = "AND ug.group_id IN (".implode(",",$groupIds).")";
			$groupsJoinCond = "
		INNER JOIN {$coursedb}.user_group ug 
			ON uc.user_id = ug.user_id
		INNER JOIN {$coursedb}.mgt_grouping_function mgf 
			ON ug.group_id = mgf.group_id AND INSTR(mgf.function_right, 'Profile:SelfAccount') > 0";
		}
		
		$checkBoxField = "CONCAT('<input type=\"checkbox\" name=\"YearClassUserID[]\" value=\"', YCU.YEARCLASSUSERID,'\">')";
		
		
//		$sql = "SELECT max(NumberOfSA) as MaxSAcount from (
//						select 
//						count(SAS.DETAILS) as NumberOfSA,
//						IU.USERID as UserID";
//		
//		$sql .= "
//				FROM {$intranet_db}.INTRANET_USER IU
//				INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT PS ON IU.USERID = PS.USERID AND PS.ISSUSPEND = 0 
//				LEFT JOIN {$eclass_db}.SELF_ACCOUNT_STUDENT SAS
//							ON SAS.USERID = IU.USERID
//				LEFT JOIN {$intranet_db}.INTRANET_USER IU2
//					ON SAS.APPROVEDBY = IU2.USERID AND IU2.RECORDTYPE = 1
//				INNER JOIN {$intranet_db}.YEAR_CLASS_USER YCU
//					ON YCU.USERID = IU.USERID
//				INNER JOIN {$intranet_db}.YEAR_CLASS YC
//					ON YCU.YEARCLASSID = YC.YEARCLASSID
//				INNER JOIN {$eclass_db}.user_course uc 
//					ON uc.user_email = IU.USEREMAIL
//				$groupsJoinCond
//				WHERE YC.ACADEMICYEARID = $currentAcademicYearId
//				AND uc.course_id = $ck_course_id
//				AND (IU.RECORDTYPE=".$IsStudent." AND IU.RECORDSTATUS = ".$IsApproved.")
//				$cond
//				$groupsCond
//				Group by IU.USERID
//				Order by IU.CLASSNUMBER ) as CountSelected  		
//				
//		";

		

		$sql = "
						select 
						count(SAS.DETAILS) as NumberOfSA,
						IU.USERID as UserID";
		
		$sql .= "
				FROM {$intranet_db}.INTRANET_USER IU
				INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT PS ON IU.USERID = PS.USERID AND PS.ISSUSPEND = 0 
				LEFT JOIN {$eclass_db}.SELF_ACCOUNT_STUDENT SAS
							ON SAS.USERID = IU.USERID
				LEFT JOIN {$intranet_db}.INTRANET_USER IU2
					ON SAS.APPROVEDBY = IU2.USERID AND IU2.RECORDTYPE = 1
				INNER JOIN {$intranet_db}.YEAR_CLASS_USER YCU
					ON YCU.USERID = IU.USERID
				INNER JOIN {$intranet_db}.YEAR_CLASS YC
					ON YCU.YEARCLASSID = YC.YEARCLASSID
				INNER JOIN {$eclass_db}.user_course uc 
					ON uc.user_email = IU.USEREMAIL
				$groupsJoinCond
				WHERE YC.ACADEMICYEARID = '$currentAcademicYearId'
				AND uc.course_id = '$ck_course_id'
				AND (IU.RECORDTYPE='".$IsStudent."' AND IU.RECORDSTATUS = '".$IsApproved."')
				$cond
				$groupsCond
				Group by IU.USERID
				Order by IU.CLASSNUMBER 
		";

		$resultArray = $this->db->returnArray($sql);

		$maxVal=0;
		for($i=0,$i_MAX=count($resultArray);$i<$i_MAX;$i++)
		{
			$thisValue = (int)$resultArray[$i]['NumberOfSA'];

			if($thisValue>=$maxVal)
			{
				$maxVal = $thisValue;
				
				$thisUserID =  $resultArray[$i]['UserID'];
				$thisArray = $this->getSelfAccounts_Export($ParYearClassID,$groupIds,$thisUserID);
				
				$_hasDefaultSA= false;
				for($j=0,$j_MAX=count($thisArray);$j<$j_MAX;$j++)
				{
					  $thisDefaultSA = $thisArray[$j]['DefaultSA'];
					  
					  if($thisDefaultSA=='')
					  {				  	
					  }
					  else
					  {
					  	$_hasDefaultSA= true;
					  	break;
					  }
				}
					
				if($_hasDefaultSA==false)	
				{
					$maxVal = ($maxVal+1);
				}		
			}
			
		}

		return $maxVal; 
	}
	
	
	
	public function getSelfAccounts_Export($ParYearClassID='',$groupIds='',$ParUserID='') 
	{
		global $intranet_db,$eclass_db,$ipf_cfg,$intranet_session_language,$ck_course_id;
	
		$IsStudent=2;
		$IsApproved=1;

		//~	Setting content			--------------------------||
		$keyword = addslashes($keyword);
		$currentAcademicYearId = Get_Current_Academic_Year_ID();
		$userNameField = getNameFieldByLang("IU.");
		$approvedByNameField = getNameFieldByLang("IU2.");
		if (!empty($ParYearClassID)) {
			$cond = " AND YC.YEARCLASSID = '$ParYearClassID' ";
		}
		
		if (!empty($ParUserID)) {
			$cond_UserID = " AND IU.USERID = '$ParUserID' ";
		}
		
		
		$coursedb = classNamingDB($ck_course_id);
		if (!empty($groupIds)) {
			$groupsCond = "AND ug.group_id IN (".implode(",",$groupIds).")";
			$groupsJoinCond = "
		INNER JOIN {$coursedb}.user_group ug 
			ON uc.user_id = ug.user_id
		INNER JOIN {$coursedb}.mgt_grouping_function mgf 
			ON ug.group_id = mgf.group_id AND INSTR(mgf.function_right, 'Profile:SelfAccount') > 0";
		}
		
		$checkBoxField = "CONCAT('<input type=\"checkbox\" name=\"YearClassUserID[]\" value=\"', YCU.YEARCLASSUSERID,'\">')";
		
		
		$sql = "SELECT
						IU.WebSAMSRegNo as RegNo,
						IU.ENGLISHNAME as EngName,
						IU.CHINESENAME as ChiName,
						YC.CLASSTITLE".strtoupper($intranet_session_language)." AS ClassName,
						IU.CLASSNUMBER as ClassNo,					
						SAS.DETAILS as Details,
						SAS.DEFAULTSA as DefaultSA,
						IU.USERID as UserID";
		
		$sql .= "
				FROM {$intranet_db}.INTRANET_USER IU
				INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT PS ON IU.USERID = PS.USERID AND PS.ISSUSPEND = 0 
				LEFT JOIN {$eclass_db}.SELF_ACCOUNT_STUDENT SAS
							ON SAS.USERID = IU.USERID
				LEFT JOIN {$intranet_db}.INTRANET_USER IU2
					ON SAS.APPROVEDBY = IU2.USERID AND IU2.RECORDTYPE = 1
				INNER JOIN {$intranet_db}.YEAR_CLASS_USER YCU
					ON YCU.USERID = IU.USERID
				INNER JOIN {$intranet_db}.YEAR_CLASS YC
					ON YCU.YEARCLASSID = YC.YEARCLASSID
				INNER JOIN {$eclass_db}.user_course uc 
					ON uc.user_email = IU.USEREMAIL
				$groupsJoinCond
				WHERE YC.ACADEMICYEARID = '$currentAcademicYearId'
				AND uc.course_id = '$ck_course_id'
				AND (IU.RECORDTYPE='".$IsStudent."' AND IU.RECORDSTATUS = '".$IsApproved."')
				$cond
				$groupsCond
				$cond_UserID
				Order by ClassName,IU.CLASSNUMBER 
		";
//debug_r($sql);

		$resultArray = $this->db->returnArray($sql);



		return $resultArray; 
	}
	
	function Update_Self_Account_Record($RecordId, $dataAry=array(), $StudentId)
	{
		global $eclass_db, $UserID;
		
		if($RecordId=="" || count($dataAry)==0) return false;
		
		// update content of Self Account
		$sql = "UPDATE {$eclass_db}.SELF_ACCOUNT_STUDENT SET ";
		foreach($dataAry as $_field=>$_value) {
			$_value = ($_value=="") ? "NULL" : "'$_value'";
			$sql .= "$_field = $_value, ";	
		}
		if($dataAry['RecordStatus']) {
			$sql .= " ApprovedBy='$UserID', ApprovedDate=NOW(), ";
		}
		$sql .= " ModifiedDate=NOW() WHERE RecordID='$RecordId'";
		$Result = $this->db->db_db_query($sql);

		if($Result == true){
			// change default SA
			if($dataAry['DefaultSA']=="SLP") {
				$this->Set_Default_Self_Account($RecordId, $StudentId);
			}
		}
		return $Result;
	}
	
	function Set_Default_Self_Account($RecordId, $StudentId)
	{
		global $eclass_db;
		
		if($RecordId=="" || $StudentId=="") return false;
		
		# set all self account to non-default
		$sql = "UPDATE {$eclass_db}.SELF_ACCOUNT_STUDENT SET DefaultSA=NULL, ModifiedDate=NOW() WHERE UserID='$StudentId'";
		$this->db->db_db_query($sql);
		
		# set default SA
		$sql = "UPDATE {$eclass_db}.SELF_ACCOUNT_STUDENT SET DefaultSA='SLP', ModifiedDate=NOW() WHERE UserID='$StudentId' AND RecordID='$RecordId'";
		$this->db->db_db_query($sql);
	}
	
	
}


class libpf_student{
	
	public function __construct(){
	    $this->db = new libdb();
	}
	  
	function Get_Student_DynamicReport_DBTable_Sql($user_id='',$recordId=''){
		global $eclass_db;	
		
		if($user_id!=''){			
			$userIdCond = " AND srgr.student_id = '$user_id' ";
		}
		
		if($recordId!=''){
			$recordIdCond = "AND srgr.record_id = '$recordId' ";
		}

		$sql = "SELECT 
			 		yc.ClassTitleEN AS ClassName,
					IF (TRIM(ycu.ClassNumber) IS NULL , '---' , ycu.ClassNumber) AS ClassNumber,
					IF (TRIM(srt.template_title) IS NULL , '---' , srt.template_title) AS TemplateTitle,
					CONCAT('<a href=\"javascript:js_Get_Report(', srgr.record_id, ' , ',srgr.template_id,')\" class=\"tablelink\">', srgr.report_title, '</a>') AS JSReportTitle,
					srgr.modifieddate AS modifieddate,     
					".getNameFieldByLang('iut.')." AS modifiedby,
					srgr.report_title as ReportName
				
				FROM 
					{$eclass_db}.student_report_generated_record srgr
					INNER JOIN {$intranet_db}.YEAR_CLASS yc on yc.YearClassID = srgr.year_class_id
					INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu on ( ycu.YearClassID = srgr.year_class_id AND ycu.UserID = srgr.student_id)
					INNER JOIN {$intranet_db}.INTRANET_USER iut on iut.UserID = srgr.inputby
					INNER JOIN {$eclass_db}.student_report_template srt on srt.template_id = srgr.template_id
				WHERE
					srgr.record_status = 1
					$userIdCond
					$recordIdCond
				";

		return $sql;
	}	  
}
?>