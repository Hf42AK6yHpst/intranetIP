<?php
//ALTER TABLE OLE_PROGRAM ADD `TeacherInCharge` int(11) default 0;
class oleLasalleImportDataDefinition extends importDataDefinition{

	public function oleLasalleImportDataDefinition(){
		global $Lang;
		$this->fields = array(
				'TeacherInCharge' => array(
					'description' => "OLE Program Teacher In Charge",
					'data_type_description' => "STRING",
					'data_mandatory' => $this->cfgFieldTypeValue['is_optional'],
					'sample_data' => 'chantaiman (Teacher Login)',
				),
				'OEACode' => array(
					'description' => "JUPAS OEA CODE",
					'data_type_description' => "NUMBER",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => '00324 / 324',
				),			
				'OEAParticipationMode' => array(
					'description' => "STUDENT OEA Participation Mode (Role)",
					'data_type_description' => "STRING",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => 'L',
					'remark' => 'L (Leader) , C (Committe Member) , M (Member / Participant)',
				),			
				'OEAYearStart' => array(
					'description' => "OEA Program Year Start",
					'data_type_description' => "NUMBER",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => '2012',
					'remark' => '',
				),			
				'OEAYearEnd' => array(
					'description' => "OEA Program Year End",
					'data_type_description' => "NUMBER",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => '2012',
					'remark' => '',
				),			
				'OEAAwardNature' => array(
					'description' => "STUDENT OEA Award Nature",
					'data_type_description' => "STRING",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => 'N',
					'remark' => 'C (By competition) , N (By nomination) , P (By participation)',
				),
				'OEAAwardBearing' => array(
					'description' => "STUDENT OEA Award Bearing",
					'data_type_description' => "STRING",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => 'Y',
					'remark' => 'Y (Award-bearing) , N (Not award-bearing)',
				),
				'OEAAwardType' => array(
					'description' => "STUDENT OEA Award Type",
					'data_type_description' => "STRING",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => 'G',
					'remark' => 'G ('.$Lang['iPortfolio']['OEA']['AchievementType']['G'].') , B ('.$Lang['iPortfolio']['OEA']['AchievementType']['B'].') <Br/> O ('.	$Lang['iPortfolio']['OEA']['AchievementType']['O'].') , N ('.$Lang['iPortfolio']['OEA']['AchievementType']['N'].')',
				),
		);
	}

	

}
?>