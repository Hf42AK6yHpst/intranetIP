<?php
##### Change Log [Start] #####
#
# **** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [start] ****
# 	Date	:	2016-08-03 Ivan [ip.2.5.7.9.1]
#  				- modified SET_PARENT() added CompanyName for macau pui ching (but treat as general improvement)
# **** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [end] ****
# 
#	Date	:	2016-02-04	Omas
# 				Fix: if admission date=='' cannot access to /home/portfolio/profile/student_info_teacher.php - K92467
#
#	Date	:	2015-09-17	Omas
# 				Fix: if dont have admission date in PORTFOLIO_STUDENT grep it in intranet- C85471
#
###### Change Log [End] ######

include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/form_class_manage.php");
include_once($intranet_root."/includes/libdisciplinev12.php");

include_once($intranet_root."/includes/libportfolio.php");
include_once($intranet_root."/includes/libportfolio2007a.php");
include_once($intranet_root."/includes/libpf-slp.php");
include_once($intranet_root."/includes/portfolio25/libpf-account.php");

class libpf_account_student extends libpf_account {

  private $WebSAMSRegNo;
  private $english_name;
  private $chinese_name;
  private $gender;
  private $dob;
  private $nationality;
  private $pob;
  private $home_tel;
  private $address;
  private $admission_date;
  private $student_house;
  private $class_name;
  private $class_number;
  
  private $parent_arr;
  private $ay_history_arr;
  private $class_history_arr;
  private $sem_history_arr;
  
  private $assessment_sem_arr;
  private $assessment_subject_arr;
  private $assessment_subject_result_arr;
  private $assessment_overall_result_arr;
  
  private $merit_summary_arr;

  public function __construct(){
    parent::__construct();
    
    $this->WebSAMSRegNo = "";
    $this->english_name = "";
    $this->chinese_name = "";
    $this->gender = "";
    $this->dob = "";
    $this->nationality = "";
    $this->pob = "";
    $this->home_tel = "";
    $this->admission_date = "";
    $this->student_house = "";
    $this->class_name = "";
    $this->class_number = "";
    $this->parent_arr = array();
    $this->ay_history_arr = array();
    $this->class_history_arr = array();
    $this->sem_history_arr = array();
    $this->merit_summary_arr = array();
  }
  
  public function IS_PORTFOLIO_USER(){
    global $eclass_db;
    
    $user_id = $this->GET_CLASS_VARIABLE("user_id");
    $sql =  "
              SELECT
                count(*)
              FROM
                {$eclass_db}.PORTFOLIO_STUDENT
              WHERE
                UserID = '".$user_id."'
            ";
    $returnVal = $this->db->returnVector($sql);
  
    return ($returnVal[0] == 1);
  }
  
  public function SET_WEBSAMS_REGNO()
  {
    global $intranet_db;
  
    $sql =  "
              SELECT
                IF(WebSAMSRegNo IS NULL, '', WebSAMSRegNo)
              FROM
                {$intranet_db}.INTRANET_USER
              WHERE
                UserID = '".$this->GET_CLASS_VARIABLE("user_id")."'
            ";
    $returnVal = current($this->db->returnVector($sql));
    
    $this->SET_CLASS_VARIABLE("WebSAMSRegNo", $returnVal);
  }
  
  # Get the link of official photo of a student by Registion Number
  public function GET_OFFICIAL_PHOTO()
  {
    global $intranet_rel_path, $intranet_root;
    
    if($this->GET_CLASS_VARIABLE("WebSAMSRegNo") == "")
    {
      $this->SET_WEBSAMS_REGNO();
    }
    $RegNo = $this->GET_CLASS_VARIABLE("WebSAMSRegNo");
    
    $tmp_regno = trim(str_replace("#", "", $RegNo));
    $photolink = "/file/official_photo/".$tmp_regno.".jpg";
    $photo_filepath = $intranet_root.$photolink;
    $photo_url = $intranet_rel_path.$photolink;
    
    return array($photo_filepath, $photo_url);
  }
  
  /*
  * Modified by key 2008-09-29: Update the get photo function
  */
  public function GET_IPORTFOLIO_PHOTO($ParReturnNullPhoto=true)
  {
    global $intranet_root;
    
    $user_id = $this->GET_CLASS_VARIABLE("user_id");
    
    ### user
    include_once($intranet_root."/includes/libuser.php");
    $luser = new libuser($user_id);
    $PhotoLink = $luser->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);
	$PhotoLink= str_replace($intranet_root, "", $PhotoLink[0]);
	return $PhotoLink;
	/*
    # Set student photo in left menu
    if(is_object($luser))
    {
      $TmpPhotoLink = $this->GET_OFFICIAL_PHOTO();
      if($TmpPhotoLink !="")
        $PhotoLink = str_replace($intranet_root, "", $TmpPhotoLink[0]);
    }
    
    // get the student photo
    if(!file_exists($TmpPhotoLink[0]))
    {
      $PhotoLink = "";
    }
    if ($PhotoLink == "" && $luser->PhotoLink !="")
    {
      if (is_file($intranet_root.$luser->PhotoLink))
      {
        $PhotoLink = $luser->PhotoLink;
      }
    }
    if ($ParReturnNullPhoto && $PhotoLink == "")
    {
      $PhotoLink = "/images/2009a/iPortfolio/no_photo.jpg";
    }
    echo $PhotoLink."<Br/>";
    return $PhotoLink;
	*/
  } // end function
  
  # Require User ID
  public function SET_STUDENT_PROPERTY()
  {
    global $intranet_db, $eclass_db;
    
    $user_id = $this->GET_CLASS_VARIABLE("user_id");
    if($user_id == "") return false;
  
    $sql =  "
              SELECT DISTINCT
                iu.UserID,
                IFNULL(iu.EnglishName,'') AS EnglishName,
                IFNULL(iu.ChineseName,'') AS ChineseName,
                IFNULL(iu.Gender, '') AS Gender,
                IFNULL(iu.DateOfBirth, '') AS DateOfBirth,
                IFNULL(iu.WebSAMSRegNo, '') AS WebSAMSRegNo,
                IFNULL(ps.Nationality, '') AS Nationality,
                IFNULL(ps.PlaceOfBirth, '') AS PlaceOfBirth,
                IFNULL(iu.HomeTelNo, '') AS HomeTelNo,
                IFNULL(iu.Address, '') AS Address,
                IFNULL(ps.AdmissionDate, '') AS AdmissionDate,
                IFNULL(t_house.Title, '') AS StudentHouse,
                IFNULL(iu.ClassName, '') AS ClassName,
                IFNULL(iu.ClassNumber, '') AS ClassNumber
              FROM
                {$intranet_db}.INTRANET_USER AS iu
              LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
                ON iu.UserID = ps.UserID
              LEFT JOIN 
                (
                  SELECT
                    iug.UserID,
                    iug.GroupID,
                    ig.Title
                  FROM
                    {$intranet_db}.INTRANET_USERGROUP as iug
                  INNER JOIN {$intranet_db}.INTRANET_GROUP as ig
                    ON ig.GroupID = iug.GroupID
                  WHERE
                    ig.RecordType = '4' AND
                    iug.UserID = ".$user_id."
                ) AS t_house
              ON t_house.UserID = iu.UserID
              WHERE
                iu.UserID = ".$user_id."
            ";
    $student_arr = $this->db->returnArray($sql);
    $student_arr = $student_arr[0];

    if(!empty($student_arr))
    {
      $this->english_name = $student_arr["EnglishName"];
      $this->chinese_name = $student_arr["ChineseName"];
      $this->gender = $student_arr["Gender"];
      $this->dob = $student_arr["DateOfBirth"];
      $this->WebSAMSRegNo = $student_arr["WebSAMSRegNo"];
      $this->nationality = $student_arr["Nationality"];
      $this->pob = $student_arr["PlaceOfBirth"];
      $this->home_tel = $student_arr["HomeTelNo"];
      $this->address = $student_arr["Address"];
      $this->student_house = $student_arr["StudentHouse"];
      $this->student_house = $student_arr["StudentHouse"];
      $this->class_name = $student_arr["ClassName"];
      $this->class_number = $student_arr["ClassNumber"];
      
      # Admission Date C85471
      if($student_arr["AdmissionDate"] == '0000-00-00' || $student_arr["AdmissionDate"]==''){
      	$sql =	"SELECT
  					AdmissionDate
  				FROM
  					{$intranet_db}.INTRANET_USER_PERSONAL_SETTINGS
  				WHERE UserID = '".$user_id."'
  				";
  		 $result = $this->db->returnVector($sql);
  		 $student_arr["AdmissionDate"] = $result[0];
      }
      
      // #K92467 
      //$this->admission_date = $student_arr["AdmissionDate"];
      $this->admission_date = ($student_arr["AdmissionDate"] =='') ? '0000-00-00' : $student_arr["AdmissionDate"];
    }
  }
  
  # Get parent record ID by student User ID
	public function SET_PARENT()
	{
		global $eclass_db;

    $user_id = $this->GET_CLASS_VARIABLE("user_id");
    if($user_id == "") return false;

    $sql =	"
							SELECT
								gs.ChName AS chinese_name,
								gs.EnName AS english_name,
								IF(CHAR_LENGTH(gs.Relation) = 1, CONCAT('0', gs.Relation), gs.Relation) AS relation,
								gs.Phone AS phone,
								gs.EmPhone AS em_phone,
								gs.IsMain AS is_main,
								gse1.Occupation,
								gse1.CompanyName
							FROM
								{$eclass_db}.GUARDIAN_STUDENT as gs
								LEFT OUTER JOIN $eclass_db.GUARDIAN_STUDENT_EXT_1 AS gse1 ON gs.RecordID = gse1.RecordID
							WHERE
								gs.UserID = '".$user_id."'
							ORDER BY
								gs.IsMain DESC
						";
    $parent_arr = $this->db->returnArray($sql);
		
		$this->SET_CLASS_VARIABLE("parent_arr", $parent_arr);
	}
  
  public function SET_MERIT_SUMMARY($parYear="", $parSemester=""){
    global $eclass_db;
  
    $user_id = $this->GET_CLASS_VARIABLE("user_id");
    if($user_id == "") return false;
    
    $lpf_slp = new libpf_slp();
    
    $conds = $lpf_slp->getReportYearCondition($parYear);
    if($parSemester!="")
    {
    	$conds .= "AND (Semester = '$parSemester')";
    }
    
    $sql =  "
              SELECT
                Year,
                Semester,
                Month(MeritDate) AS meritMonth,
                RecordType,
                SUM(NumberOfUnit) AS unitSum
              FROM
                {$eclass_db}.MERIT_STUDENT
              WHERE
                UserID = ".$user_id."
                $conds
              GROUP BY
                Year, Semester, meritMonth, RecordType
              ORDER BY
      					Year DESC, Semester DESC, RecordType DESC
            ";
    $t_meritArr = $this->db->returnArray($sql);
    
    $meritArr = array();
    for($i=0; $i<count($t_meritArr); $i++)
    {
      list($year, $semester, $month, $type, $count) = $t_meritArr[$i];
  		$meritArr[$year][$semester][$month][$type] = $count;
    }
    
    $this->merit_summary_arr = $meritArr;
  }
  
  # Convert Student Merit Summary, copy from retrieveStudentConvertedMeritTypeCountByDate() in libdisciplinev12.php
  public function GET_CONVERT_MERIT_SUMMARY($parYear="", $parSemester="")
  {
    $this->SET_MERIT_SUMMARY($parYear, $parSemester);
    $dataAry = $this->merit_summary_arr;
  
    $ldiscipline = new libdisciplinev12();
    # Get Promotion Method
		$PromotionMethod = $ldiscipline->getDisciplineGeneralSetting("PromotionMethod");
		$conversionType_punish = $ldiscipline->getConversionPeriod();
		$conversionType_award = $ldiscipline->getConversionPeriod('', 1);
		
    $MeritPromotionSetting_punish = $ldiscipline->retrieveMeritPromotionInfo($PromotionMethod);
    $MeritPromotionSetting_award = $ldiscipline->retrieveMeritPromotionInfo($PromotionMethod, '', 1);
    
    if($PromotionMethod == "global")
		{
      # $dataAry[year][sem][meritType] = no. of meritType (this array is for one student)
      # $MeritPromotionSetting[meritType] = array(FromMerit(meritType), ToMerit, UpgradeNum)
      $_dataAry = array();        
      foreach($dataAry as $year=> $semMeritArr){
        foreach($semMeritArr as $sem =>$monthMeritArr){
          foreach($monthMeritArr as $month => $meritDataArr){
            foreach($meritDataArr as $meritType => $thisMeritVal){
              $conversionType = (intval($meritType) > 0) ? $conversionType_award : $conversionType_punish;
            
              # Group the number of merit/demerit according to conversion type
              switch($conversionType["global"])
              {
                case 0:
                case 1:
                case 2:
                  # 'whole' as place holder
  				        $_dataAry[$year][$sem]['whole'][$meritType] += $thisMeritVal; 
  				        break;
                case 3:
  				        $_dataAry[$year][$sem][$month][$meritType] += $thisMeritVal;
  				        break;
              }
            }
          }
        }          
      }

      # Convert the numbers according to rules
      foreach($_dataAry as $year=> $semMeritArr){
        foreach($semMeritArr as $sem =>$monthMeritArr){
          foreach($monthMeritArr as $month => $meritDataArr){
            foreach($meritDataArr as $meritType => $thisMeritVal){
  
              $MeritPromotionSetting = (intval($meritType) > 0) ? $MeritPromotionSetting_award : $MeritPromotionSetting_punish;

              $thisFromMerit = $MeritPromotionSetting[$meritType]["UpgradeFromType"];
    					$thisToMerit = $MeritPromotionSetting[$meritType]["MeritType"];
    					$thisUpgradeNum = $MeritPromotionSetting[$meritType]["UpgradeNum"];
  
              if(empty($thisMeritVal)) continue;
  
              # if no conversion rule, use original value
              if(empty($thisUpgradeNum))
              {
                $returnAry[$year][$sem][$meritType] += $thisMeritVal;
              }
              # apply conversion rule if there is one
              else
              {
                $returnAry[$year][$sem][$thisToMerit] += floor($thisMeritVal/$thisUpgradeNum);
    		        $returnAry[$year][$sem][$thisFromMerit] += fmod($thisMeritVal,$thisUpgradeNum);
              }
            }
          }
        }          
      }
		}
		else
    {
      # 20091208, not support category conversion
      /*
      foreach($MeritPromotionSetting as $thisCatID =>$thisCatMeritPromotionSetting)
			{
				foreach($thisCatMeritPromotionSetting as $thisMeritPromotionSetting)
				{
					$thisFromMerit = $thisMeritPromotionSetting["UpgradeFromType"];
					$thisToMerit = $thisMeritPromotionSetting["MeritType"];
					$thisUpgradeNum = $thisMeritPromotionSetting["UpgradeNum"];
					
					if(empty($dataAry[$thisCatID][$thisFromMerit]) || empty($thisUpgradeNum)) continue;
					
					$dataAry[$thisCatID][$thisToMerit] += floor($RawDataAry[$thisCatID][$thisFromMerit]/$thisUpgradeNum);
					$dataAry[$thisCatID][$thisFromMerit] = fmod($RawDataAry[$thisCatID][$thisFromMerit],$thisUpgradeNum);
				}
			}
			$returnAry = Array();	
			foreach ($dataAry as $thisCatMeritCount)
			{
				foreach($thisCatMeritCount as $thisMeritType =>$thisMeritCount)
				{
					$returnAry[$thisMeritType] += $thisMeritCount;
				}
			}
			
			
      */        
      $returnAry = $dataAry;
    }
    
		return $returnAry;
  }
	
  # $ParVariableName is case-sensitive
  public function SET_CLASS_VARIABLE($ParVariableName, $ParValue)
  {
    # Prevent declaring new variables to a class
    try
    {
      if(!isset($this->{$ParVariableName}))
      {
        throw new Exception("Class variable \"{$ParVariableName}\" is not declared!!");
      }
      else{
        $this->{$ParVariableName} = $ParValue;
      }
    }
    catch (Exception $e) {
      $error_trace = current(debug_backtrace());
    
      echo "Caught exception: ",  $e->getMessage(), "<br />\n";
      echo "In script: ",  $error_trace["file"], "<br />\n";
      echo "Line no: ",  $error_trace["line"], "<br />\n";
      echo "Method called: ", $error_trace["function"], " @ ", $error_trace["class"], "<br />\n";
      DIE();
    }
  }
  
  # $ParVariableName is case-sensitive
  public function GET_CLASS_VARIABLE($ParVariableName)
  {
    try
    {
      if(!isset($this->{$ParVariableName}))
      {
        throw new Exception("Class variable \"{$ParVariableName}\" is not declared!!");
      }
      else{
        return $this->{$ParVariableName};
      }
    }
    catch (Exception $e) {
      $error_trace = current(debug_backtrace());
    
      echo "Caught exception: ",  $e->getMessage(), "<br />\n";
      echo "In script: ",  $error_trace["file"], "<br />\n";
      echo "Line no: ",  $error_trace["line"], "<br />\n";
      echo "Method called: ", $error_trace["function"], " @ ", $error_trace["class"], "<br />\n";
      DIE();
    }
  }

}