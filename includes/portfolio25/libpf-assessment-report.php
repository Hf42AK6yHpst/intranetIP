<?php
/*
 * Editing by Pun
 *
 * Modification Log:
 *  2018-03-14 (Pun) [ip.2.5.9.3.1]
 *      - Modified getTeacherReportListSql(), fixed cannot display Chinese class name if the class only have English name
 *  2017-03-09 (Paul)
 *  	- Created this file
 * 
 */

class libpf_assessment_report  extends libdb {
	
	static $attachment_url = "/file/iportfolio/";
	private $schoolyear = "";
	private $user_id = "";
	private static $unsafe_file_char = array('\\', '/', ':', '*', '?', '"', '<', '>', '|');
	
	# Initializing class variables to prevent throwing exceptions in get/set functions
	public function libpf_assessment_report(){
		$this->libdb();
		$this->schoolyear = $_SESSION['CurrentSchoolYearID'];
		$this->user_id = $_SESSION['UserID'];
	}
	
	function getAttachmentUrl(){
		return $this->attachment_url;
	}
	
	function getSaveFileName($file_name){
		return str_replace($this->unsafe_file_char, '', $file_name);
	}
	
	//Assessment Report Function
	function getClassInfo($ClassName=''){
		global $intranet_db;
		$cond = (!empty($ClassName))?" AND (ClassTitleEN like '%$ClassName%' OR ClassTitleB5 like '%$ClassName%')":"";
		$sql = "SELECT
				YearClassID as class_id,
				ClassTitleEN as class_name_en,
				ClassTitleB5 as class_name_b5
			FROM ".$intranet_db.".YEAR_CLASS
			WHERE
				 AcademicYearID = ".$this->schoolyear."
				".$cond."
			ORDER BY ClassTitleEN
			";
		return $this->returnArray($sql);
	}
	
	function getTeacherReportListSql($keyword="", $status="all"){
		global $intranet_db,$eclass_db, $intranet_session_language;
		$sql =  "
          CREATE TEMPORARY TABLE tempAssessmentRecord(
             RecordID int(11) NOT NULL auto_increment,
		     AssessmentTitle varchar(255) default NULL,
		     ReleaseDate datetime default NULL,
		     ClassName varchar(50) default NULL,
		     ModifiedDate datetime default NULL,
			 SubmittedRecord int(11) default 0,
		     PRIMARY KEY (RecordID)
          )ENGINE=InnoDB DEFAULT CHARSET=utf8
        ";
		$this->db_db_query($sql);
		
		$keywordSql  = ($keyword=="")?"":" AND ar.AssessmentTitle LIKE '%$keyword%' ";
		$statusSql = ($status=="all")?"":(($status=="1")?" AND ar.ReleaseDate <= NOW() ":" AND ar.ReleaseDate > NOW() ");
		$sql = "
			INSERT INTO tempAssessmentRecord(
			RecordID,AssessmentTitle,ReleaseDate,ClassName,ModifiedDate,SubmittedRecord
			) SELECT
			ar.RecordID,ar.AssessmentTitle,ar.ReleaseDate,ar.ClassName,ar.ModifiedDate , COUNT(sr.RecordID)
			FROM {$eclass_db}.ASSESSMENT_REPORT ar
			LEFT JOIN
			{$eclass_db}.ASSESSMENT_REPORT_STUDENT_RECORD sr
			ON ar.RecordID=sr.AssessmentID
			WHERE ar.AcademicYearID = '".$_SESSION['CurrentSchoolYearID']."'
				$keywordSql
				$statusSql
			GROUP BY ar.RecordID
		";
		$this->db_db_query($sql);
		
		$checkBoxField = "CONCAT('<input type=\"checkbox\" name=\"assessment_id[]\" value=\"', ar.RecordID,'\">')";
		$titleLink = "CONCAT('<a href=\"assessment_report_detail.php?assessment_id=',ar.RecordID,'\">',ar.AssessmentTitle,'</a>')";
		
		if($intranet_session_language=='b5'){
		    $classNameSql = "IF(c.ClassTitleB5, c.ClassTitleB5, c.ClassTitleEN) AS classname";
		}else{
		    $classNameSql = "IF(c.ClassTitleEN, c.ClassTitleEN, c.ClassTitleB5) AS classname";
		}
		$mainSql = "SELECT
						$titleLink,
						ar.ReleaseDate,
						{$classNameSql},
						CONCAT(ar.SubmittedRecord,'/', COUNT(cu.YearClassUserID)) as SubmittedRatio,
						ar.ModifiedDate,
						$checkBoxField
					FROM
						tempAssessmentRecord ar
					LEFT JOIN
						{$intranet_db}.YEAR_CLASS c ON ar.ClassName = c.YearClassID
					LEFT JOIN
						{$intranet_db}.YEAR_CLASS_USER cu ON c.YearClassID = cu.YearClassID
					GROUP BY cu.YearClassID,ar.RecordID
					
					";

		return 	$mainSql;		
	}
	
	function removeAssessment($toBeRemoved){
		global $eclass_db;
		$isDone = true;
		$sql = "DELETE FROM {$eclass_db}.ASSESSMENT_REPORT WHERE RecordID IN ($toBeRemoved)";
		$isDone =  $this->db_db_query($sql);
				
		return (isDone);
	}
	
	function getAssessmentInfo($assessment_id){
		global $eclass_db,$intranet_db;
		$sql = "SELECT AssessmentTitle, ReleaseDate, ClassName FROM {$eclass_db}.ASSESSMENT_REPORT WHERE RecordID='".$assessment_id."'";	
		return current($this->returnArray($sql));
	}
	
	function getAssessmentReportName($assessment_id){
		global $eclass_db,$intranet_db;
		$sql = "SELECT CONCAT(ar.AssessmentTitle, ' - ', ".(($intranet_session_language=='b5')?"c.ClassTitleB5":"c.ClassTitleEN").") FROM
		{$eclass_db}.ASSESSMENT_REPORT ar
		INNER JOIN
		{$intranet_db}.YEAR_CLASS c
		ON ar.ClassName = c.YearClassID
		WHERE ar.RecordID=$assessment_id";
		return current($this->returnVector($sql));
	}
	
	function getAssessmentReportReleaseDate($assessment_id){
		global $eclass_db;
		$sql = "SELECT ReleaseDate FROM {$eclass_db}.ASSESSMENT_REPORT WHERE RecordID='".$assessment_id."'";
		return current($this->returnVector($sql));
	}
	
	function addAssessmentInfo($title,$release_date,$classId){
		global $eclass_db;
		if($classId=='all'){
			$classArr = $this->getClassInfo();
			$classArr = Get_Array_By_Key($classArr,'class_id');
		}else{
			$classArr = (array)$classId;
		}	
		if(!empty($title)){
			$insertValuesArr = array();
			$sql = "INSERT INTO ".$eclass_db.".ASSESSMENT_REPORT
						(AcademicYearID,AssessmentTitle,ReleaseDate,ClassName,InputDate,Createdby,ModifiedDate,ModifiedBy)
					VALUES ";
			for($i=0;$i<count($classArr);$i++){
				$insertValuesArr[] = "(
						'".$_SESSION['CurrentSchoolYearID']."',
						'".$title."',
						'".$release_date."',
						'".$classArr[$i]."',
						NOW(),
						'".$_SESSION['UserID']."',
						NOW(),
						'".$_SESSION['UserID']."'
					)";
			}
			$sql .= implode(',',$insertValuesArr);
			$result = $this->db_db_query($sql);
			return ($result==true)?1:0;
		}
		return 0;
	}
	
	function updateAssessment($assessmentId,$title,$release_date){
		global $eclass_db;
		$sql = "UPDATE ".$eclass_db.".ASSESSMENT_REPORT SET
					AssessmentTitle = '".$title."',
					ReleaseDate = '".$release_date."',
					ModifiedDate = NOW(),
					ModifiedBy = '".$_SESSION['UserID']."'
				WHERE
					RecordID = '".$assessmentId."'";
		$result = $this->db_db_query($sql);
		return ($result==true)?1:0;	
	}
	
	function getStudentSubmisstionInfo($assessment_id, $keyword="", $status="all"){
		global $intranet_db,$eclass_db, $intranet_session_language;
		
		$statusSQL = ($status=='2')?" AND (sr.ReportTitle IS NULL OR sr.ReportTitle='') ":(($status=='1')?" AND (sr.ReportTitle IS NOT NULL AND sr.ReportTitle <> '') ":"");
		
		$fileSQL = " IF(sr.ReportTitle IS NULL OR sr.ReportTitle='',
						CONCAT('<view>hide</view><upload>show</upload>','<data>',ar.RecordID,'?=?',u.UserID,'</data>'),
						CONCAT('<view>show</view><upload>hide</upload>','<data>',ar.RecordID,'?=?',u.UserID,'</data>')
					) ";
		
		$dateSQL = "IF( sr.ModifiedDate IS NULL OR sr.ModifiedDate='',
					CONCAT('<span id=\'date_',u.UserID,'\'></span>'), 
					CONCAT('<span id=\'date_',u.UserID,'\'>',DATE_FORMAT(sr.ModifiedDate,'%Y-%m-%d'),'</span>') 
					)";
		
		$mainSql = "SELECT  
					".(($intranet_session_language=='b5')?"IF (ChineseName IS NULL OR ChineseName='', EnglishName, ChineseName)":"EnglishName")." as Name, 
					$fileSQL,
					$dateSQL
					FROM {$eclass_db}.ASSESSMENT_REPORT ar
					LEFT JOIN {$intranet_db}.YEAR_CLASS c ON ar.ClassName = c.YearClassID
					LEFT JOIN {$intranet_db}.YEAR_CLASS_USER cu ON c.YearClassID = cu.YearClassID
					LEFT JOIN {$intranet_db}.INTRANET_USER u ON cu.UserID=u.UserID
					LEFT JOIN {$eclass_db}.ASSESSMENT_REPORT_STUDENT_RECORD sr ON ar.RecordID=sr.AssessmentID AND sr.UserID = u.UserID
					WHERE ar.RecordID='".$assessment_id."'
					$statusSQL
					";

		return 	$mainSql;		
	}
	
	function getStudentAssessmentList($assessmentId='',$classId='',$studentIdAry=array(), $status='all', $keyword=''){
		/* Get Student List */
		global $intranet_session_language,$eclass_db,$intranet_db;
		$user_name_field = ($intranet_session_language=='b5')?'user_name_b5':'user_name_en';
		$cond = '';
		$cond .= (!empty($assessmentId))?" AND r.AssessmentID = '".$assessmentId."'":"";
		$cond .= (!empty($classId))?" AND r.ClassName = '".$classId."'":"";
		//$cond .= (!empty($keyword))?" OR r.ReportTitle LIKE '%".$keyword."%'":"";
		$cond .= (is_array($studentIdAry) && count($studentIdAry) > 0)?" AND r.UserID IN ('".(implode("','", $studentIdAry))."')":"";
	
		$sql = "
			SELECT
				r.RecordID,
				r.UserID,
				DATE_FORMAT(r.InputDate,'%Y-%m-%d') as input_date,
				DATE_FORMAT(r.ModifiedDate,'%Y-%m-%d') as modified_date,
				".getNameFieldByLang("u.")." as created_user,
				r.Status,
				r.ReportTitle
			FROM
				".$eclass_db.".ASSESSMENT_REPORT_STUDENT_RECORD r
			INNER JOIN
				".$intranet_db.".INTRANET_USER u ON r.CreatedBy = u.UserID
			INNER JOIN
				".$intranet_db.".INTRANET_USER iu ON r.UserID = iu.UserID
			WHERE
				r.AcademicYearID = '".$this->schoolyear."'
			".$cond."
		";
		$result = $this->returnArray($sql);
		$StudentAssessmentList = BuildMultiKeyAssoc($result,'UserID');
		$StudentAssessmentIDList = Get_Array_By_Key($result, 'UserID');
		if(is_array($studentIdAry) && count($studentIdAry) > 0){
			$StudentList = $this->getUsers(array('user_ids'=>$studentIdAry,'keyword'=>$keyword));
		}else{
			switch($status){
				case '1': //File uploaded
					$StudentList = $this->getUsers(array('class_id'=>$classId,'user_ids'=>$StudentAssessmentIDList,'keyword'=>$keyword,'user_type'=>'S'));
					break;
				case '2':
					$StudentList = $this->getUsers(array('class_id'=>$classId,'excludes'=>$StudentAssessmentIDList,'keyword'=>$keyword,'user_type'=>'S'));
					break;
				default:
					$StudentList = $this->getUsers(array('class_id'=>$classId,'keyword'=>$keyword,'user_type'=>'S'));
			}
		}
		$StudentIDList = BuildMultiKeyAssoc($StudentList,'user_class_number',array('user_id',$user_name_field));
		ksort($StudentIDList);
		$NewStudentAssessmentList = array();
		foreach($StudentIDList as $_studentNumber => $_studentInfo){
			$_studentId = $_studentInfo['user_id'];
			$_studentName = $_studentInfo[$user_name_field];
				
			$_report = $StudentAssessmentList[$_studentId]['ReportTitle']?$StudentAssessmentList[$_studentId]['ReportTitle']:'';
			$_modifiedDate = $StudentAssessmentList[$_studentId]['input_date']?$StudentAssessmentList[$_studentId]['input_date']:'';;
			$_modifiedUser = $StudentAssessmentList[$_studentId]['created_user']?$StudentAssessmentList[$_studentId]['created_user']:'';;
				
			$NewStudentAssessmentList[$_studentNumber]['user_id'] = $_studentId;
			$NewStudentAssessmentList[$_studentNumber]['name'] = $_studentName;
			$NewStudentAssessmentList[$_studentNumber]['status'] = $StudentAssessmentList[$_studentId]['Status'];
			if(!empty($_report))
				$NewStudentAssessmentList[$_studentNumber]['assessment'] = array(
						'title'=>$_report,
						'path'=>$this->attachment_url.'assessment/'.$assessmentId.'/'.$_studentId.'/'.$_report,
						'modified_date'=>$_modifiedDate,
						'modified_user'=>$_modifiedUser
				);
	
					
		}
	
		return $NewStudentAssessmentList;
	}
	
	 function getUsers($params = array()){
			
		extract($params);
		$cond = "";
		$cond .= isset($user_id)&&!empty($user_id)? "u.UserID = $user_id AND ": "";
		$cond .= isset($user_ids)? "u.UserID IN ('".implode("','",$user_ids)."') AND ": "";
		$cond .= isset($user_type)? "u.RecordType = '".array_search($user_type, kis::$user_record_types)."' AND ": "";
		$cond .= isset($excludes)? "u.UserID NOT IN ('".implode("','",$excludes)."') AND ": "";
	
		if (isset($class_id)&&!empty($class_id)){
		  
			$tables .= "LEFT JOIN YEAR_CLASS_USER cu ON cu.UserID = u.UserID ";
			$cond .= "cu.YearClassID = $class_id AND ";
		  
		}
		if (isset($academic_year_id)&&!empty($academic_year_id)){
		  
			$tables .= "LEFT JOIN YEAR_CLASS_USER cu ON cu.UserID = u.UserID ";
			$tables .= "LEFT JOIN YEAR_CLASS yc ON yc.YearClassID = cu.YearClassID ";
			$cond .= "yc.AcademicYearID = $academic_year_id AND ";
		  
		}
	
		$sql = "SELECT
		u.UserID user_id,
		u.EnglishName user_name_en,
		u.ChineseName user_name_b5,
		u.Gender user_gender,
		u.PhotoLink user_photo,
		u.ClassName as user_class_name,
		u.ClassNumber as user_class_number,
		u.RecordType user_type
		FROM INTRANET_USER u
		$tables
		WHERE $cond
		u.RecordStatus = 1 AND
		(
		u.UserLogin LIKE '%$keyword%' OR
		u.EnglishName LIKE '%$keyword%' OR
		u.ChineseName LIKE '%$keyword%' OR
		u.ClassName LIKE '%$keyword%'
		)
		ORDER BY u.ClassName, u.EnglishName";
	
		return $this->returnArray($sql);
	
	}
	
	function downloadFile($path, $name=false, $size=false){
	
		$name = $name? $name: preg_replace('`^.*/`u','', $path);
	
		header('Content-Disposition: attachment; filename="'.$name.'"');
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
	
		if ($size){
			header("Content-length: $size");
		}
		header("Content-Description: File Transfer");
	
		readfile($path);
	}
	
	function saveStudentAssessment($assessmentId,$classId,$studentId,$file_name){
		$AssessmentArr = current($this->getStudentAssessmentList($assessmentId,$classId,array($studentId)));
		if(count($AssessmentArr['assessment'])>0){ //remove old file first
			$this->removeStudentAssessment($assessmentId,$studentId,$file_name);
		}
		$this->insertStudentAssessment($assessmentId,$classId,$studentId,$file_name);
	
	}
	function updateStudentAssessment($assessmentId,$studentId,$status){
		global $eclass_db;
	
		$sql = "UPDATE ".$eclass_db.".ASSESSMENT_REPORT_STUDENT_RECORD
			SET
				Status = '".$status."',
				ModifiedDate = NOW()
	
			WHERE
				AssessmentID = '".$assessmentId."'
			AND
				UserID = '".$studentId."'
	
		";
		$this->db_db_query($sql);
	}
	function insertStudentAssessment($assessmentId,$classId,$studentId,$file_name){
		global $eclass_db;
		$StudentInfoArr = current($this->getUsers(array('user_id'=>$studentId)));
		$sql = "INSERT INTO
					".$eclass_db.".ASSESSMENT_REPORT_STUDENT_RECORD
					( AssessmentID,AcademicYearID,UserID,ClassName,ClassNumber,ReportTitle,Status,InputDate,Createdby,ModifiedDate,ModifiedBy)
				VALUES
					(
						'".$assessmentId."',
						'".$this->schoolyear."',
						'".$studentId."',
						'".$classId."',
						'".$StudentInfoArr['user_class_number']."',
						'".$file_name."',
						'0',
						NOW(),
						'".$this->user_id."',
						NOW(),
						'".$this->user_id."'
					)
	
		";
		$this->db_db_query($sql);
	}
	function removeStudentAssessment($assessmentId,$classId,$studentId){
		global $eclass_db,$file_path;
		$AssessmentArr = current($this->getStudentAssessmentList($assessmentId,$classId,array($studentId)));
		if(count($AssessmentArr['assessment'])>0){ //remove old file first
			$exist_file = $file_path.'/file/iportfolio/assessment/'.$assessmentId.'/'.$studentId.'/'.$AssessmentArr['assessment']['title'];
			if (file_exists($exist_file)){
				unlink($exist_file);
			}
			$sql = "
				DELETE FROM
					".$eclass_db.".ASSESSMENT_REPORT_STUDENT_RECORD
				WHERE
					AssessmentID = '".$assessmentId."'
				AND
					ClassName = '".$classId."'
				AND
					UserID = '".$studentId."'
			";
			$this->db_db_query($sql);
		}
	}
	
	function getClassIDByAssessmentID($assessment_id){
		global $eclass_db;
		$sql = "SELECT ClassName FROM {$eclass_db}.ASSESSMENT_REPORT WHERE RecordID='".$assessment_id."'";
		return current($this->returnVector($sql));
	}
}	
?>