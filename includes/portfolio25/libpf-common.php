<?php
include_once($intranet_root."/includes/libdb.php");
# this function is copied from ../cnecc/libpf-slp-cnecc.php on date 2010-05-11
function getAcademicYearInfoByAcademicYearId($ParAcademicYearId=-1) {
	global $intranet_session_language;
	$db = new libdb();
	if ($ParAcademicYearId<0) {
		$academicYearName = getCurrentAcademicYear();
	} else {
		$sql = "SELECT
					YEARNAME".strtoupper($intranet_session_language)."
				FROM
					$intranet_db.ACADEMIC_YEAR
				WHERE
					ACADEMICYEARID = $ParAcademicYearId";
		$returnVector = $db->returnVector($sql);
		$academicYearName = $returnVector[0];
	}
	return $academicYearName;
}
function portfolio_GetYearClassTitleFieldByLang($prefix)
{
  $firstChoice = Get_Lang_Selection($prefix."ClassTitleB5", $prefix."ClassTitleEN");
  $altChoice = Get_Lang_Selection($prefix."ClassTitleEN", $prefix."ClassTitleB5");
  
  $year_class_title_field = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '', $altChoice, $firstChoice)";

  return $year_class_title_field;
}

?>