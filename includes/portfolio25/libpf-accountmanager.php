<?php

include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/libeclass40.php");
include_once($intranet_root."/includes/portfolio25/libpf-account-student.php");

class libpf_accountmanager {

  private $db;
  private $file_path;
  private $key_encrpyt;
  private $quota;
  
  public function __CONSTRUCT()
  {
    global $eclass_filepath;
  
    $this->db = new libdb();
    $this->SET_CLASS_VARIABLE("file_path", $eclass_filepath."/files");
    $this->SET_CLASS_VARIABLE("key_encrpyt", "protect8key9by9eclass");
  }

	public function SET_LICENSE_KEYS(){
		global $eclass_db, $eclass_filepath;

    $key_file_path = $this->GET_CLASS_VARIABLE("file_path")."/keys.dat";
		$keys = $this->GET_KEY_LICENSE($key_file_path);

		# select last key used
		$sql =	"
							SELECT
								UserKey
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT
							ORDER BY
								RecordID DESC
							LIMIT
								1
						";
		$row = $this->db->returnArray($sql);
		$key_last = $row[0]["UserKey"];

		$key_total_used = 0;
		$key_total = 0;
		$keys_available = array();

		# determine the usage of keys
		if (sizeof($keys['records'])>0)
		{
			foreach ($keys['records'] as $key_date => $record)
			{
				for ($i=0; $i<sizeof($record); $i++)
				{
					$key_total ++;
					# record keys not in used
					if ($key_total_used>0 || $key_last=="")
					{
						$keys_available[] = $record[$i];
					}
					if ($key_last==$record[$i])
					{
						$key_total_used = $key_total;
					}
				}
			}
		}

		# update the global object
		$this->quota["total"] = $key_total;
		$this->quota["used"] = $key_total_used;
		$this->quota["free"] = $key_total - $key_total_used;
		$this->quota["free_keys"] = $keys_available;
		$this->quota["school"] = $keys[sizeof($keys)-1];
	}
	
	# Read encrypted key details
	private function GET_KEY_LICENSE($my_file_path){
		$data = array();

		if (file_exists($my_file_path))
		{
			if ($fd = fopen($my_file_path, "r"))
			{
				$data = $this->DATA_DECRYPT(fread($fd, filesize($my_file_path)));
			}
			fclose ($fd);
		}
		return $data;
	}
	
	# Decrypt key string
	private function DATA_DECRYPT($string)
	{
		$key = $this->GET_CLASS_VARIABLE("key_encrpyt");
		$result = '';
		for($i=1; $i<=strlen($string); $i++)
		{
			$char = substr($string, $i-1, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		$result = unserialize($result);

		return $result;
	}
	
	public function GET_NUMBER_FREE_LICENSE()
  {
    $quota_info = $this->GET_CLASS_VARIABLE("quota");

    return $quota_info["free"];
  }
  
	public function GET_FREE_LICENSE()
  {
    $quota_info = $this->GET_CLASS_VARIABLE("quota");
  
    return $quota_info["free_keys"];
  }
  
  public function DO_ACTIVATE_STUDENT($ParUserIDArr){
		global $eclass_db, $intranet_db, $intranet_root;

    $course_id = $_SESSION['iPortfolio']['course_id'];
    $leclass = new libeclass($course_id);
    $user_id_str = is_array($ParUserIDArr) ? implode(",", $ParUserIDArr) : "";
		$ClassNumberField = getClassNumberField("iu.");
		# check if quota is enough
		$sql =	"
							SELECT
								iu.UserID,
								iu.WebSAMSRegNo AS RegNo,
								ps.UserKey,
								ps.RecordID,
								ps.IsSuspend,
								iu.UserEmail,
								iu.UserPassword,
								$ClassNumberField as ClassNumber,
								iu.FirstName,
								iu.LastName,
								iu.ClassName,
								CASE iu.Title
									WHEN 0 THEN 'MR.'
									WHEN 1 THEN 'MISS.'
									WHEN 2 THEN 'MRS.'
									WHEN 3 THEN 'MS.'
									WHEN 4 THEN 'DR.'
									WHEN 5 THEN 'PROF.'
								END AS Title,
								iu.EnglishName,
								iu.ChineseName,
								iu.NickName,
								iu.Gender
							FROM
								{$intranet_db}.INTRANET_USER AS iu
							LEFT JOIN
								{$eclass_db}.PORTFOLIO_STUDENT AS ps
							ON
								ps.UserID = iu.UserID
							WHERE
								iu.UserID IN (".$user_id_str.")
							ORDER BY
								iu.ClassName,
								$ClassNumberField
						";
		$row = $this->db->returnArray($sql);

		if (sizeof($row)<=$this->GET_NUMBER_FREE_LICENSE())
		{
			$keys = $this->GET_FREE_LICENSE();
			$key_index = 0;

			$school_lang = get_file_content($intranet_root."/file/language.txt");
			$MemberType = "S";

			for ($i=0; $i<sizeof($row); $i++)
			{
				$user_obj = $row[$i];
				$user_key = $keys[$key_index];

				# attempt to insert if qualify (RegNo exist but not activated before)
				# 20081016 : Check if user key to be used is empty				
				if ($user_obj["UserID"] != "" && $user_obj["RegNo"] != "" && $user_obj["UserKey"] == "" && $user_key != "")
				{
					$sql =	"
										INSERT INTO
											{$eclass_db}.PORTFOLIO_STUDENT
												(UserID, WebSAMSRegNo, UserKey, IsSuspend, InputDate, ModifiedDate)
										VALUES
											('".$user_obj["UserID"]."', '".$user_obj["RegNo"]."', '".$user_key."', 0, now(), now())
							";
					$this->db->db_db_query($sql);
					if ($this->db->db_affected_rows()==1)
					{
						$key_index ++;

						# import to eClass
						$thisUserID = $user_obj["UserID"];
						$UserEmail = $user_obj["UserEmail"];
						$UserPassword = $user_obj["UserPassword"];
						$ClassNumber = $user_obj["ClassNumber"];
						$FirstName = $user_obj["FirstName"];
						$LastName = $user_obj["LastName"];
						$ClassName = $user_obj["ClassName"];
						$Title = $user_obj["Title"];
						$EngName = $user_obj["EnglishName"];
						$ChiName = $user_obj["ChineseName"];
						$NickName = $user_obj["NickName"];
						$Gender = $user_obj["Gender"];
						$eclassClassNumber = ($ClassNumber=="" || $ClassName=="" || stristr($ClassNumber, $ClassName)) ? $ClassNumber : $ClassName ." - ".$ClassNumber;
						$leclass->eClassUserAddFullInfo($UserEmail, $Title, $FirstName, $LastName, $EngName, $ChiName, $NickName, $MemberType, $UserPassword, $eclassClassNumber, $Gender, $ICQNo, $HomeTelNo, $FaxNo, $DateOfBirth, $Address, $Country, $URL, $Info, $ClassName);

            # Get Course User ID of newly added account
            $lpf_acc = new libpf_account_student();
            $lpf_acc->SET_CLASS_VARIABLE("user_email", $UserEmail);
            $lpf_acc->SET_COURSE_USER_ID();
            $CourseUserID = $lpf_acc->GET_CLASS_VARIABLE("course_user_id");

						# update for course user_id
						if ($CourseUserID>0)
						{
							$sql =	"
												UPDATE
													{$eclass_db}.PORTFOLIO_STUDENT
												SET
													CourseUserID = '".$CourseUserID."'
												WHERE
													UserID = '".$user_obj["UserID"]."'
											";
							$this->db->db_db_query($sql);
						}
					}
					else
					{
						# failed by unknown reason
						$failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "UNKNOWN");
					}
				}
				else
				{
					# check regno
					if ($user_obj["RegNo"]=="")
					{
						# no reg no
						$failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "NO_REGNO");
					}
					else if($user_obj["UserKey"]!="")
					{
						# already activated
						$failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "ACTIVATED");
					}
					else if($user_key == "")
					{
						# failed by unknown reason
						$failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "EMPTY_USER_KEY");
					}
					else
					{
						# failed by unknown reason
						$failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "UNKNOWN");
					}
				}
			}

			# Update eClass Course Usage
			//$leclass->eClassUserNumber($course_id);
		}
		else
			$failed_record = "activation_no_quota";

		return array($key_index, $failed_record);
	}
  
  public function DO_REACTIVATE_STUDENT($ParUserIDArr)
  {
    global $eclass_db;

    if(is_array($ParUserIDArr) && !empty($ParUserIDArr))
    {
      $user_id_str = implode(",", $ParUserIDArr);
  
      $sql =  "
                UPDATE
  								{$eclass_db}.PORTFOLIO_STUDENT
  							SET
  								IsSuspend = 0
  							WHERE
  								UserID IN (".$user_id_str.")
              ";
      $this->db->db_db_query($sql);
      
      $success_count = $this->db->db_affected_rows();
    }
    else
    {
      $success_count = 0;
    }

    return $success_count;
  }

  public function DO_SUSPEND_STUDENT($ParUserIDArr)
	{
		global $eclass_db, $intranet_db;

		$ClassNumberField = getClassNumberField("iu.");
		$user_id_str = is_array($ParUserIDArr) ? implode(",", $ParUserIDArr) : "";

		# check if this user activated
		$sql =	"
							SELECT
								ps.RecordID,
								ps.IsSuspend,
								iu.EnglishName,
								iu.ChineseName,
								iu.ClassName,
								$ClassNumberField as ClassNumber
							FROM
								{$intranet_db}.INTRANET_USER AS iu
							LEFT JOIN
								{$eclass_db}.PORTFOLIO_STUDENT AS ps
							ON
								ps.UserID = iu.UserID
							WHERE
								iu.UserID IN (".$user_id_str.")
							ORDER BY
								iu.ClassName, ClassNumber
						";
		$row = $this->db->returnArray($sql);

		for($i=0; $i<sizeof($row); $i++)
		{
			$user_obj = $row[$i];
			if($user_obj["RecordID"]!="" && $user_obj["IsSuspend"]==0)
			{
        $pass_recordid_arr[] = $user_obj["RecordID"];
			}
			else if($user_obj["RecordID"]!="" && $user_obj["IsSuspend"]==1)
			{
				$failed_record[] = Array($user_obj["ClassName"],  $user_obj["ClassNumber"], $user_obj["EnglishName"], $user_obj["ChineseName"], "SUSPENDED");
			}
			else if($user_obj["RecordID"]=="")
			{
				$failed_record[] = Array($user_obj["ClassName"],  $user_obj["ClassNumber"], $user_obj["EnglishName"], $user_obj["ChineseName"], "INACTIVE");
			}
			else
			{
				$failed_record[] = Array($user_obj["ClassName"],  $user_obj["ClassNumber"], $user_obj["EnglishName"], $user_obj["ChineseName"], "UNKNOWN");
			}
		}

    $key_index = 0;		
		if(is_array($pass_recordid_arr))
		{
		  $record_id_str = implode(",", $pass_recordid_arr);
			$sql = "
								UPDATE
									{$eclass_db}.PORTFOLIO_STUDENT
								SET
									IsSuspend = 1,
									ModifiedDate = NOW()
								WHERE
									RecordID IN (".$record_id_str.")";
			$this->db->db_db_query($sql);

			$key_index = $this->db->db_affected_rows();
		}

		return array($key_index, $failed_record);
	}
	
	public function GET_SUSPENDED_STUDENT()
	{
    global $eclass_db;
    
    $sql =  "
              SELECT
                UserID
              FROM
                {$eclass_db}.PORTFOLIO_STUDENT
              WHERE
                IsSuspend = 1
            ";
    $returnArr = $this->db->returnVector($sql);
    
    return $returnArr;
  }
	
	public function CHECK_SUSPENDED_STUDENT($ParUserIDArr)
	{
    $suspended_stu_arr = $this->GET_SUSPENDED_STUDENT();
    
    $returnArr = array_intersect($suspended_stu_arr, $ParUserIDArr);
    
    return $returnArr;
  }
  
  public function SET_CLASS_VARIABLE($ParVariableName, $ParValue)
  {
    $this->{$ParVariableName} = $ParValue;
  }
  
  public function GET_CLASS_VARIABLE($ParVariableName)
  {
    return $this->{$ParVariableName};
  }
}