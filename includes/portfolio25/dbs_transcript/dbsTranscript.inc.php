<?
# modifying :  

//THIS CONFIG IS AVALIABLE FOR ALL CLIENT ENVIRONMENT
##	-	$varname["DB_[TABLE_NAME]_[FIELD_NAME]"]["MEANING"]

// TABLE : CURRICULUMN_SETTINGS
// FIELD : COMEFROM
//$ipf_cfg["Volunteer"]["CURRICULUMN_SETTINGS_COMEFROM"]["teacherInput"] = 1;  // TEACHER INPUT FORM THE UI

### GENERAL
$ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Inactive"] = "0";
$ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"] = "1";
$ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Deleted"] = "2";

### REPORT TYPE
$ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["PredictedGrade"] = "1";
$ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["Transcript"] = "2";

### IMAGE TYPE
$ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["back_cover"] = "B";
$ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["header"] = "H";
$ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["school_seal"] = "SS";
$ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["signature"] = "S";

### IMAGE DIRECTORY
$ipf_cfg["DSBTranscript"]["IMAGE_DIRECTORY"] = 'file/portfolio/dbs_transcript/';

### IB GRADE
$ipf_cfg["DSBTranscript"]["IB_GRADE"] = array('7', '6', '5', '4', '3', '2', '1');

### SUBJECT GRADE
$ipf_cfg["DSBTranscript"]["SUBJECT_GRADE"] = array('A', 'B', 'C', 'D', 'F');

### DSE GRADE
$ipf_cfg["DSBTranscript"]["DSE_GRADE"] = array('5**', '5*', '5', '4', '3', '2', '1', 'U');

### IMPORT CSV HEADER (PREDICTED GRADE)
$ipf_cfg["DSBTranscript"]["PREDICT_GRADE_IMPORT"]["IB"] = array('Subject Code', 'Subject (Optional)', 'Student UserLogin', 'Student Name (Optional)', 'G11 Max PG', 'G11 Min PG', 'Level');
$ipf_cfg["DSBTranscript"]["PREDICT_GRADE_IMPORT"]["IBDP"] = $ipf_cfg["DSBTranscript"]["PREDICT_GRADE_IMPORT"]["IB"];
$ipf_cfg["DSBTranscript"]["PREDICT_GRADE_IMPORT"]["HKDSE"] = array('userlogin', 'class (opt)', 'no (opt)', 'name (opt)', 'sbjcode', 'subject (opt)', 'mark (opt)', 'rank (opt)', 'pos (opt)', 'total (opt)', 'grade');

// ### VOLUNTEER_EVENT
// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["Code"]["MaxLength"] = 255;
// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["Title"]["MaxLength"] = 255;
// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["Organization"]["MaxLength"] = 200;

// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["RemoveStatus"]["Active"] = 'N';
// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["RemoveStatus"]["Deleted"] = 'Y';

// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["CreatorType"]["Teacher"] = 'T';
// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["CreatorType"]["Student"] = 'S';
// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["CreatorType"]["Parent"] = 'P';


// ### VOLUNTEER_EVENT_USER
// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["Role"]["MaxLength"] = 64;

// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["RemoveStatus"]["Active"] = 'N';
// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["RemoveStatus"]["Deleted"] = 'Y';

// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["ApproveStatus"]["Pending"] = 1;
// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["ApproveStatus"]["Approved"] = 2;
// $ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["ApproveStatus"]["Rejected"] = 3;
?>