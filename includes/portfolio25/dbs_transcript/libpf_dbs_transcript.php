<?php 
# Modifying : 

include_once($intranet_root."/includes/portfolio25/dbs_transcript/dbsTranscript.inc.php");
if (!defined("LIBPF_DBS_TRANSCRIPT_DEFINED"))         // Preprocessor directives
{
	define("LIBPF_DBS_TRANSCRIPT_DEFINED",true);

	class libpf_dbs_transcript {
		private $objDB;
		private $ipf_cfg;
		
		public function __construct() {
			global $ipf_cfg;
			if(empty($ipf_cfg))
			{
			    global $intranet_root;
			    include($intranet_root."/includes/portfolio25/dbs_transcript/dbsTranscript.inc.php");
			}
			
			$this->objDB = new libdb();
			$this->ipf_cfg = $ipf_cfg;
		}
		
		public function getStatus() {
		    $returnArr = array();
		    $returnArr['Activate'] = $this->ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"];
		    $returnArr['Deactivate'] = $this->ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Inactive"];
		    $returnArr['Deleted'] = $this->ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Deleted"];
		    
		    return $returnArr;
		}
		
		public function getCurriculumGrades()
		{
		    $returnArr = array();
		    $returnArr['HKDSE'] = $this->ipf_cfg["DSBTranscript"]["DSE_GRADE"];
		    $returnArr['IBDB'] = $this->ipf_cfg["DSBTranscript"]["IB_GRADE"];
    		
    		return $returnArr;
		}
		
		public function getCurriculumFormClassList($curriculum, $yearID="", $groupByYear=false)
		{
		    global $intranet_session_language;
		    
		    $studentCurriculumClassAry = $this->getClassListByStudentCurriculum($curriculum);
		    $studentCurriculumClassAry = Get_Array_By_Key($studentCurriculumClassAry, 'YearClassID');
		    
		    $cond = " yc.CurriculumID = '$curriculum' ";
		    if(!empty($studentCurriculumClassAry)) {
		        $cond .= " OR yc.YearClassID IN ('".implode("', '", (array)$studentCurriculumClassAry)."') ";
		    }
		    $extra_cond = $yearID? " AND y.YearID = '$yearID' " : "";   
		    $group_by = $groupByYear? ' GROUP BY y.YearID ' : '';
		    $clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
		    
		    $sql = "SELECT
                        y.YearName, y.YearID, $clsName, yc.YearClassID
                    FROM
                        YEAR_CLASS yc
                        INNER JOIN YEAR y ON (y.YearID = yc.YearID)
                    WHERE
                        yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' AND
                        ($cond)
                        $extra_cond
                    $group_by
                    ORDER BY
                        y.Sequence, yc.Sequence, $clsName";
		    $result = $this->objDB->returnArray($sql,2);
		    return $result;
		}
		
		public function getCurriculumClassList($curriculum, $yearID="", $allCurriculum=false)
		{
            global $intranet_session_language;
            
		    $clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
		    
		    $cond_curriculum = $allCurriculum? '1' : " yc.CurriculumID	= '$curriculum' ";
		    if(!$allCurriculum) {
		        $studentCurriculumClassAry = $this->getClassListByStudentCurriculum($curriculum);
		        $studentCurriculumClassAry = Get_Array_By_Key($studentCurriculumClassAry, 'YearClassID');
		        if(!empty($studentCurriculumClassAry)) {
		            $cond_curriculum .= " OR yc.YearClassID IN ('".implode("', '", (array)$studentCurriculumClassAry)."') ";
		        }
		    }
		    $cond_year_id = $yearID? $yearID : Get_Current_Academic_Year_ID();
		    
		    $sql = "SELECT
                        $clsName, yc.YearClassID, y.YearName, y.YearID
                    FROM YEAR_CLASS yc
                        LEFT OUTER JOIN YEAR y ON (y.YearID = yc.YearID)
                    WHERE
                        yc.AcademicYearID='".$cond_year_id."' AND
                        ($cond_curriculum)
                    ORDER BY y.Sequence, yc.Sequence, $clsName";
            $result = $this->objDB->returnArray($sql,2);
		    return $result;
		}
		
		public function getCurriculumSetting($curriculumID='', $code='', $nameEn='', $nameCh='', $status='', $returnSQL=false, $excludeCurriculumID='') {
		    global $eclass_db, $intranet_db, $iPort;
		    
		    if($curriculumID != '') {
		        $conds_id = " AND CurriculumID = '$curriculumID' ";
		    }
		    if($excludeCurriculumID != '') {
		        $conds_id2 = " AND CurriculumID != '$excludeCurriculumID' ";
		    }
		    if(is_array($code) || (!is_array($code) && $code != '')) {
		        $conds_code = " AND Code IN ('".implode("', '", (array)$code)."') ";
		    }
		    if(is_array($nameEn) || (!is_array($nameEn) && $nameEn != '')) {
		        $conds_nameEn = " AND NameEn IN ('".implode("', '", (array)$nameEn)."') ";
		    }
		    if(is_array($nameCh) || (!is_array($nameCh) && $nameCh != '')) {
		        $conds_nameCh = " AND NameCh IN ('".implode("', '", (array)$nameCh)."') ";
		    }
		    if($status == '') {
		        $status = array($this->ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Inactive"], $this->ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
		    }
		    $conds_status = " AND Status IN ('".implode("', '", (array)$status)."') ";
		    
		    $fields = " Code, NameEn, NameCh, ";
		    if($returnSQL) {
		        $fields .= " REPLACE(SubjectLevel, '\\n', '<br />') as SubjectLevel, ";
		        $fields .= " IF(Status = ".$this->ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"].", '".$iPort['activate']."', '". $iPort['deactivate']."') as Status, "; 
		        $fields .= " CONCAT('<input type=\"checkbox\" name=\"curriculumIds[]\" value=\"', CurriculumID, '\" onclick=\"unset_checkall(this, this.form);\">') as Checkbox ";
		    } else {
		        $fields .= " SubjectLevel, CurriculumID, Status "; 
		    }
		    
		    $sql = "SELECT 
                       ".$fields."
		            FROM
                        ".$eclass_db.".DBS_TRANSCRIPT_CURRICULUMN_SETTINGS
		            WHERE
		                1
                        $conds_id
                        $conds_id2
                        $conds_code
                        $conds_nameEn
                        $conds_nameCh
		                $conds_status ";
            if($returnSQL) {
                return $sql;
            } else {
                return $this->objDB->returnArray($sql);
            }
		}
		
		public function insertUpdateCurriculumSetting($dataAry, $id='') {
		    global $eclass_db, $intranet_db, $UserID;
		    
		    $table = $eclass_db.".DBS_TRANSCRIPT_CURRICULUMN_SETTINGS";
		    if($id) {
		        $field_sql = '';
		        foreach($dataAry as $field => $value) {
		            $field_sql .= $field_sql == ''? '' : ', '; 
		            $field_sql .= " $field = '".HTMLtoDB($value)."' ";
		        }
		        $field_sql .= ", DateModified = NOW(), ModifiedBy = '$UserID' ";
		        
		        $sql = " UPDATE $table SET $field_sql WHERE CurriculumID = '$id' ";
		    } else {
		        $field_sql = '';
		        $value_sql = '';
		        foreach($dataAry as $field => $value) {
		            $field_sql .= $field_sql == ''? '' : ', ';
		            $field_sql .= "$field ";
		            $value_sql .= $value_sql == ''? '' : ', ';
		            $value_sql .= "'".HTMLtoDB($value)."' ";
		        }
		        $field_sql .= ", DateInput, InputBy, DateModified, ModifiedBy ";
		        $value_sql .= ", NOW(), '$UserID', NOW(), '$UserID' ";
		        
		        $sql = " INSERT INTO $table ($field_sql) VALUES ($value_sql) ";
		    }
		    return $this->objDB->db_db_query($sql);
		}
		
		public function getStudentCurriculumSettings($studentID)
		{
		    global $eclass_db, $intranet_db, $UserID;
		    
		    if(is_array($studentID)) {
		        $cond = " AND StudentID IN ('".implode("', '", (array)$studentID)."') ";
		    } else {
		        $cond = " AND StudentID = '$studentID' ";
		    }
		    
		    $sql = "SELECT * FROM ".$eclass_db.".DBS_STUDENT_CURRICULUM WHERE 1 $cond";
		    return $this->objDB->returnArray($sql);
		}
		
		public function getStudentWithoutCurriculumSettings($studentID, $curriculumID='')
		{
		    global $eclass_db, $intranet_db, $UserID;
		    
		    if(is_array($studentID)) {
		        $cond = " AND iu.UserID IN ('".implode("', '", (array)$studentID)."') ";
		    } else {
		        $cond = " AND iu.UserID = '$studentID' ";
		    }
		    
		    if($curriculumID != '') {
		        $extra_table = "";
		        $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID) ";
		        $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') ";
		        $extra_cond = " AND yc.CurriculumID = '$curriculumID' ";
		    }
		    
		    $sql = "SELECT 
                        iu.* 
                    FROM 
                        ".$intranet_db.".INTRANET_USER iu
                        $extra_table
                        LEFT JOIN ".$eclass_db.".DBS_STUDENT_CURRICULUM dsc ON (iu.UserID = dsc.StudentID)
                    WHERE 
                        dsc.RecordID IS NULL AND 
                        iu.RecordType = 2
                        $cond
                        $extra_cond";
		    return $this->objDB->returnArray($sql);
		}
		
		public function insertUpdateStudentCurriculumSettings($studentID, $curriculumID) {
		    global $eclass_db, $intranet_db, $UserID;
		    
		    $table = $eclass_db.".DBS_STUDENT_CURRICULUM";
		    if($studentID != '' && $curriculumID != '')
		    {
	            $sql = "SELECT COUNT(RecordID) FROM $table WHERE StudentID = '$studentID'";
	            $is_exist = $this->objDB->returnVector($sql);
	            if($is_exist[0]) {
                    $field_sql .= " CurriculumID = '$curriculumID', DateModified = NOW(), ModifiedBy = '$UserID' ";
                    $sql = " UPDATE $table SET $field_sql WHERE StudentID = '$studentID' ";
                } else {
                    $field_sql .= "StudentID, CurriculumID, DateInput, InputBy, DateModified, ModifiedBy ";
		            $value_sql .= "'$studentID', '$curriculumID', NOW(), '$UserID', NOW(), '$UserID' ";
                    $sql = " INSERT INTO $table ($field_sql) VALUES ($value_sql) ";
                }
                
                return $this->objDB->db_db_query($sql);
		    }
		}
		
		public function getClassListByStudentCurriculum($curriculumID, $targetClassIDAry = '') {
		    global $eclass_db, $intranet_db, $iPort;
		    
		    if(is_array($targetClassIDAry) || (!is_array($targetClassIDAry) && $targetClassIDAry != '')) {
		        $conds_class = " AND yc.ClassID IN ('".implode("', '", (array)$targetClassIDAry)."') ";
		    }
		    
		    $table = $eclass_db.".DBS_STUDENT_CURRICULUM as sc ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS_USER as ycu ON (sc.StudentID = ycu.UserID) ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') ";
		    $sql = "SELECT
                        yc.*
		            FROM
                        $table
                        $extra_table
                    WHERE
                        1
                        $conds_class AND 
                        sc.CurriculumID = '$curriculumID'
		            GROUP BY
                        yc.YearClassID";
            return $this->objDB->returnArray($sql);
		}
		
		public function getSubjectLevel($recordID='', $classID='', $studentID='', $subjectID='', $subjectLevel='') {
		    global $eclass_db, $intranet_db, $iPort;
		    
		    if($recordID != '') {
		        $conds_id = " AND RecordID = '$recordID' ";
		    }
		    if(is_array($classID) || (!is_array($classID) && $classID != '')) {
		        $conds_class = " AND ClassID IN ('".implode("', '", (array)$classID)."') ";
		    }
		    if(is_array($studentID) || (!is_array($studentID) && $studentID != '')) {
		        $conds_student = " AND StudentID IN ('".implode("', '", (array)$studentID)."') ";
		    }
		    if(is_array($subjectID) || (!is_array($subjectID) && $subjectID != '')) {
		        $conds_subject = " AND SubjectID IN ('".implode("', '", (array)$subjectID)."') ";
		    }
		    if(is_array($subjectLevel) || (!is_array($subjectLevel) && $subjectLevel != '')) {
		        $conds_subject_level = " AND SubjectID IN ('".implode("', '", (array)$subjectLevel)."') ";
		    }
	       
		    $sql = "SELECT
                       RecordID, ClassID, StudentID, SubjectID, SubjectLevel
		            FROM
                        ".$eclass_db.".DBS_TRANSCRIPT_STUDENT_SUBJECT_LEVEL
                    WHERE
                        1
                        $conds_id
                        $conds_class
                        $conds_student
                        $conds_subject
                        $conds_subject_level ";
            return $this->objDB->returnArray($sql);
		}
		
		public function insertUpdateSubjectLevel($dataAry, $classId) {
		    global $eclass_db, $intranet_db, $UserID;
		    
		    $studentIDAry = array_keys((array)$dataAry);
		    $studentIDCond = implode("', '", $studentIDAry);
		    
		    $table = $eclass_db.".DBS_TRANSCRIPT_STUDENT_SUBJECT_LEVEL";
		    $field_sql = " ClassID, StudentID, SubjectID, SubjectLevel, DateInput, InputBy, DateModified, ModifiedBy ";
		    
		    //$sql = " DELETE FROM $table WHERE ClassID = '$classId' AND StudentID IN ('".$studentIDCond."') ";
		    $sql = " DELETE FROM $table WHERE StudentID IN ('".$studentIDCond."') ";
		    $this->objDB->db_db_query($sql);
		    
		    $values_ary = array();
		    foreach((array)$dataAry as $thisStudentID => $thisStudentDataAry) {
		        foreach((array)$thisStudentDataAry as $thisSubjectID => $thisSubjectLevel) {
		            //$values_ary[] = " ('$classId', '$thisStudentID', '$thisSubjectID', '$thisSubjectLevel', NOW(), '$UserID', NOW(), '$UserID') ";
		            $values_ary[] = " ('0', '$thisStudentID', '$thisSubjectID', '$thisSubjectLevel', NOW(), '$UserID', NOW(), '$UserID') ";
		        }
		    }
		    $value_sql = implode(", ", $values_ary);
		    
		    if($value_sql != '') {
		        $sql = " INSERT INTO $table ($field_sql) VALUES $value_sql ";
    		    return $this->objDB->db_db_query($sql);
		    } else {
		        return false;
		    }
		}
		
		public function getReportImage($imageType, $imageID='', $curriculumID='', $reportTypeID='', $description='', $signatureName='', $status='', $returnSQL=false, $excludeImageID='') {
		    global $PATH_WRT_ROOT, $eclass_db, $ipf_cfg, $iPort, $Lang;
		    
		    $table = $eclass_db.".DBS_TRANSCRIPT_REPORT_IMAGE as image ";
		    $extra_table = " INNER JOIN ".$eclass_db.".DBS_TRANSCRIPT_CURRICULUMN_SETTINGS as curriculumn ON (image.CurriculumID = curriculumn.CurriculumID) ";
		        
		    if($imageID != '') {
		        $conds_id = " AND image.ImageID = '$imageID' ";
		    }
		    if($excludeImageID != '') {
		        $conds_id2 = " AND image.ImageID != '$excludeImageID' ";
		    }
	        if(is_array($curriculumID) || (!is_array($curriculumID) && $curriculumID != '')) {
	            $conds_curriculum = " AND image.CurriculumID IN ('".implode("', '", (array)$curriculumID)."') ";
	        }
	        if(is_array($reportTypeID) || (!is_array($reportTypeID) && $reportTypeID != '')) {
	            $conds_reportType = " AND image.ReportTypeID IN ('".implode("', '", (array)$reportTypeID)."') ";
	        }
	        if(is_array($description) || (!is_array($description) && $description != '')) {
	            $conds_description = " AND image.Description IN ('".implode("', '", (array)$description)."') ";
	        }
	        if(is_array($signatureName) || (!is_array($signatureName) && $signatureName != '')) {
	            $conds_signature_name = " AND image.SignatureName IN ('".implode("', '", (array)$signatureName)."') ";
	        }
	        if($status == '') {
	            $status = array($this->ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
	        }
	        $conds_status = " AND image.Status IN ('".implode("', '", (array)$status)."') ";
	        
	        $report_type_field = "  CASE
                                        WHEN image.ReportTypeID = '".$ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["PredictedGrade"]."' THEN '".$Lang['iPortfolio']['DBSTranscript']['PredictedGrade']."'
                                        WHEN image.ReportTypeID = '".$ipf_cfg["DSBTranscript"]["REPORT_TYPE"]["Transcript"]."' THEN '".$Lang['iPortfolio']['DBSTranscript']['Transcript']."'
                                        ELSE '---'
                                    END AS ReportType";
	        $display_image_html = " IF(image.ImageLink IS NULL, '---', CONCAT('<img src=\"".$PATH_WRT_ROOT.$ipf_cfg["DSBTranscript"]["IMAGE_DIRECTORY"]."', image.ImageLink, '?t=".time()."\" style=\"max-width:500px;\">')) as displayImage ";
	        if($returnSQL) {
	            if($imageType == $ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["back_cover"] || $imageType == $ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["header"] || $imageType == $ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["school_seal"]) {
	                $fields = " curriculumn.Code, image.Description, $display_image_html, ";
	            } else if($imageType == $ipf_cfg["DSBTranscript"]["IMAGE_TYPE"]["signature"]) {
	                $fields = " curriculumn.Code, $report_type_field, image.SignatureName, REPLACE(image.SignatureDetails, '\\n', '<br />') as SignatureDetails, $display_image_html, ";
	            }
	            $fields .= " CONCAT('<input type=\"checkbox\" name=\"imageIds[]\" value=\"', ImageID, '\" onclick=\"unset_checkall(this, this.form);\">') as Checkbox ";
	        } else {
	            $fields = " image.ImageID, image.ImageType, image.CurriculumID, image.ReportTypeID, $report_type_field, image.Description, image.SignatureName, image.SignatureDetails, image.ImageLink, $display_image_html, image.Status ";
	        }
	        
	        $sql = "SELECT
                        $fields
                    FROM
                        $table
                        $extra_table 
                    WHERE
                        image.ImageType = '$imageType'
                        $conds_id
                        $conds_id2
                        $conds_curriculum
                        $conds_reportType
                        $conds_description
                        $conds_signature_name
                        $conds_status ";
            if($returnSQL) {
                return $sql;
            } else {
                return $this->objDB->returnArray($sql);
            }
		}
		
		public function insertUpdateImageSetting($dataAry, $id) {
		    global $eclass_db, $intranet_db, $UserID;
		    
		    $table = $eclass_db.".DBS_TRANSCRIPT_REPORT_IMAGE";
		    if($id) {
		        $field_sql = '';
		        foreach($dataAry as $field => $value) {
		            $field_sql .= $field_sql == ''? '' : ', ';
		            $field_sql .= " $field = '".HTMLtoDB($value)."' ";
		        }
		        $field_sql .= ", DateModified = NOW(), ModifiedBy = '$UserID' ";
		        
		        $sql = " UPDATE $table SET $field_sql WHERE ImageID = '$id' ";
		    } else {
		        $field_sql = '';
		        $value_sql = '';
		        foreach($dataAry as $field => $value) {
		            $field_sql .= $field_sql == ''? '' : ', ';
		            $field_sql .= "$field ";
		            $value_sql .= $value_sql == ''? '' : ', ';
		            $value_sql .= "'".HTMLtoDB($value)."' ";
		        }
		        $field_sql .= ", DateInput, InputBy, DateModified, ModifiedBy ";
		        $value_sql .= ", NOW(), '$UserID', NOW(), '$UserID' ";
		        
		        $sql = " INSERT INTO $table ($field_sql) VALUES ($value_sql) ";
		    }
		    $result = $this->objDB->db_db_query($sql);
		    
		    if($result) {
		        return $id? $id : $this->objDB->db_insert_id();
		    } else {
		        return false;
		    }
		}
		
		public function getIBGradeMapping($subjectCode='', $subjectLevel='', $IBGrade='', $targetGrade='', $map_ids='') {
		    global $eclass_db;
		    
		    $table = $eclass_db.".DBS_TRANSCRIPT_GRADE_MAPPING as grade_map ";
		    
		    if(is_array($subjectCode) || (!is_array($subjectCode) && $subjectCode != '')) {
		        $conds_subjectCode = " AND grade_map.SubjectCode IN ('".implode("', '", (array)$subjectCode)."') ";
		    }
		    if(is_array($subjectLevel) || (!is_array($subjectLevel) && $subjectLevel != '')) {
		        $conds_subjectLevel = " AND grade_map.SubjectLevel IN ('".implode("', '", (array)$subjectLevel)."') ";
		    }
		    if(is_array($IBGrade) || (!is_array($IBGrade) && $IBGrade != '')) {
		        $conds_IB_Grade = " AND grade_map.IBGrade IN ('".implode("', '", (array)$IBGrade)."') ";
		    }
		    if(is_array($targetGrade) || (!is_array($targetGrade) && $targetGrade != '')) {
		        $conds_targetGrade = " AND grade_map.TargetGrade IN ('".implode("', '", (array)$targetGrade)."') ";
		    }
		    if(is_array($map_ids) || (!is_array($map_ids) && $map_ids != '')) {
		        $conds_id = " AND grade_map.MappingID IN ('".implode("', '", (array)$map_ids)."') ";
		    }
		    
		    $sql = "SELECT
                        MappingID, SubjectCode, SubjectLevel, IBGrade, TargetGrade
                    FROM
                        $table
                    WHERE
                        1
                        $conds_subjectCode
                        $conds_subjectLevel
                        $conds_IB_Grade
                        $conds_targetGrade
                        $conds_id
                    ORDER BY
                        IBGrade DESC";
	        return $this->objDB->returnArray($sql);
		}
		
		public function updateIBGradeMapping($dataAry) {
		    global $eclass_db, $intranet_db, $UserID;
		    
		    $result = array();
		    if(!empty($dataAry))
		    {
		        $table = $eclass_db.".DBS_TRANSCRIPT_GRADE_MAPPING";
		        foreach($dataAry as $id => $grade) {
		            $field_sql = " TargetGrade = '".$grade."', DateModified = NOW(), ModifiedBy = '$UserID' ";
		            $sql = " UPDATE $table SET $field_sql WHERE MappingID = '$id' ";
		            $result[] = $this->objDB->db_db_query($sql);
		        }
		    }
		    return in_array(false, (array)$result);
		}
		
		public function getDisabledStudentList($recordID='', $classID='', $status='', $keyword='', $returnSQL=false) {
		    global $PATH_WRT_ROOT, $eclass_db, $intranet_db, $ipf_cfg, $iPort, $Lang;
		    
		    $table = $eclass_db.".DBS_TRANSCRIPT_DISABLED_STUDENT as stu ";
		    $extra_table = "";
		    //$extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') ";
		    //$extra_table .= " INNER JOIN ".$intranet_db.".INTRANET_USER as iu ON (stu.StudentID = iu.UserID) ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS_USER as ycu ON (stu.StudentID = ycu.UserID) ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID) ";
		    $extra_table .= " LEFT OUTER JOIN INTRANET_USER iu ON (ycu.UserID = iu.UserID AND iu.RecordType = 2) ";
		    $extra_table .= " LEFT OUTER JOIN INTRANET_ARCHIVE_USER iau ON (ycu.UserID = iau.UserID AND iau.RecordType = 2) ";
		    $extra_table .= " LEFT JOIN ".$intranet_db.".INTRANET_USER as iu_t ON (stu.InputBy = iu_t.UserID) ";
		    
		    if($recordID != '') {
		        $conds_id = " AND stu.RecordID = '$recordID' ";
		    }
		    if(is_array($classID) || (!is_array($classID) && $classID != '')) {
		        $conds_class = " AND ycu.YearClassID IN ('".implode("', '", (array)$classID)."') ";
		    }
		    if($status == '') {
		        $status = array($this->ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"], $this->ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Inactive"]);
		    }
		    $conds_status = " AND stu.RecordStatus IN ('".implode("', '", (array)$status)."') ";
		    
		    $name_field = getNameFieldByLang("iu.");
		    $name_field2 = getNameFieldByLang("iu_t.");
		    $archive_name_field = getNameFieldByLang("iau.");
		    
		    $cond_keyword = '';
		    if(trim($keyword) != '') {
		        $keyword = HTMLtoDB($keyword);
		        $cond_keyword = " AND ($name_field LIKE '%$keyword%' OR $name_field2 LIKE '%$keyword%' OR $archive_name_field LIKE '%$keyword%' OR stu.Reason LIKE '%$keyword%') ";
		    }
		    
		    $student_name_field = " IF(iu.UserID IS NOT NULL, $name_field, CONCAT($archive_name_field, '<span class=\"tabletextrequire\">*</span>')) AS StudentName ";
		    $class_name_field = " IF(iu.UserID IS NOT NULL, CONCAT(iu.ClassName, '-', iu.ClassNumber), '---') AS ClassName ";
		    $status_field = "  CASE
                                        WHEN stu.RecordStatus = '".$ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]."' THEN '".$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Locked']."'
                                        WHEN stu.RecordStatus = '".$ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Inactive"]."' THEN '".$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Unlocked']."'
                                        ELSE '---'
                                    END AS StatusDisplay";
		    if($returnSQL) {
		        $fields = " $class_name_field, $student_name_field, REPLACE(stu.Reason, '\\n', '<br />') as Reason, stu.DateInput, $name_field2 as InputName, $status_field, ";
		        $fields .= " CONCAT('<input type=\"checkbox\" name=\"recordIds[]\" value=\"', stu.RecordID, '\" onclick=\"unset_checkall(this, this.form);\">') as Checkbox, ";
		        $fields .= " stu.RecordID, stu.StudentID ";
		    } else {
		        $fields = " stu.RecordID, stu.StudentID, $student_name_field, $class_name_field, stu.Reason, stu.RecordStatus ";
		    }
		    
		    $sql = "SELECT
                        $fields
		            FROM
		                $table
		                $extra_table
		            WHERE
                        (iu.UserID IS NOT NULL OR iau.UserID IS NOT NULL)
            		    $conds_id
            		    $conds_class
            		    $conds_status
		                $cond_keyword
                    GROUP BY
                        IF(iu.UserID IS NOT NULL, iu.UserID, iau.UserID)";
		    if($returnSQL) {
		        return $sql;
		    } else {
		        return $this->objDB->returnArray($sql);
		    }
		}
		
		public function insertUpdateDisabledStudentList($dataAry, $id='') {
		    global $eclass_db, $intranet_db, $UserID;
		    
		    $table = $eclass_db.".DBS_TRANSCRIPT_DISABLED_STUDENT";
		    if($id) {
		        $field_sql = '';
		        foreach($dataAry as $field => $value) {
		            $field_sql .= $field_sql == ''? '' : ', ';
		            $field_sql .= " $field = '".HTMLtoDB($value)."' ";
		        }
		        $field_sql .= ", DateModified = NOW(), ModifiedBy = '$UserID' ";
		        
		        $sql = " UPDATE $table SET $field_sql WHERE RecordID = '$id' ";
		        $result = $this->objDB->db_db_query($sql);
		    } else {
		        $result = array();
		        foreach((array)$dataAry['StudentID'] as $thisTargetID) {
		            $field_sql = '';
		            $value_sql = '';
    		        foreach($dataAry as $field => $value) {
    		            if($field == 'StudentID') {
    		                $value = $thisTargetID;
    		            }
    		            
    		            $field_sql .= $field_sql == ''? '' : ', ';
    		            $field_sql .= "$field ";
    		            $value_sql .= $value_sql == ''? '' : ', ';
    		            $value_sql .= "'".HTMLtoDB($value)."' ";
    		        }
    		        $field_sql .= ", DateInput, InputBy, DateModified, ModifiedBy ";
    		        $value_sql .= ", NOW(), '$UserID', NOW(), '$UserID' ";
    		        
    		        $sql = " INSERT INTO $table ($field_sql) VALUES ($value_sql) ";
    		        $result[] = $this->objDB->db_db_query($sql);
		        }
		        $result = !in_array(false, (array)$result);
		    }
		    
		    if($result) {
		        return $id? $id : $this->objDB->db_insert_id();
		    } else {
		        return false;
		    }
		}
		
		function getStudentAdjustment($recordID='', $studentID='', $yearID='', $subjectID='', $subjectGroupID='', $predictedPhase='', $predictedType='', $portfolioAdmin=false, $returnSQL=false, $isViewOnly=false) {
		    global $PATH_WRT_ROOT, $intranet_db, $eclass_db, $ipf_cfg, $iPort, $Lang;
		    
		    if($recordID != '') {
		        $conds_id = " AND predict_grade.PredictedGradeID = '$recordID' ";
		    }
		    if(is_array($studentID) || (!is_array($studentID) && $studentID != '')) {
		        $conds_student = " AND iu.UserID IN ('".implode("', '", (array)$studentID)."') ";
		    }
		    if(is_array($yearID) || (!is_array($yearID) && $yearID != '')) {
		        $conds_year = " AND y.YearID IN ('".implode("', '", (array)$yearID)."') ";
		    }
		    if(is_array($subjectID) || (!is_array($subjectID) && $subjectID != '')) {
// 		        $conds_subject = " AND predict_grade.SubjectID IN ('".implode("', '", (array)$subjectID)."') ";
                $extra_conds_subject = " AND predict_grade.SubjectID IN ('".implode("', '", (array)$subjectID)."') ";
		    }
		    if(is_array($subjectGroupID) || (!is_array($subjectGroupID) && $subjectGroupID != '')) {
		        //$conds_subject_group = " AND predict_grade.SubjectGroupID IN ('".implode("', '", (array)$subjectGroupID)."') ";
		        
		        include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		        $scm = new subject_class_mapping();
		        
		        $subjectGroupStudentIDs = $scm->Get_Subject_Group_Student_List($subjectGroupID);
		        $subjectGroupStudentIDs = Get_Array_By_Key($subjectGroupStudentIDs, 'UserID');
		        
		        $conds_subject_group = " AND iu.UserID IN ('".implode("', '", (array)$subjectGroupStudentIDs)."') ";
		    }
		    if(is_array($predictedPhase) || (!is_array($predictedPhase) && $predictedPhase != '')) {
		        $conds_predict_phase = " AND predict_grade.PredictPhase IN ('".implode("', '", (array)$predictedPhase)."') ";
		    }
		    if(is_array($predictedType) || (!is_array($predictedType) && $predictedType != '')) {
		        $conds_predict_type = " AND predict_grade.PredictType IN ('".implode("', '", (array)$predictedType)."') ";
		    }
		    
		    $table = $intranet_db.".INTRANET_USER as iu ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID) ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR as y ON (yc.YearID = y.YearID) ";
		    $extra_table .= " LEFT OUTER JOIN ".$eclass_db.".DBS_STUDENT_PREDICTED_GRADE as predict_grade ON (iu.UserID = predict_grade.StudentID ".$extra_conds_subject.") ";
		    $extra_table .= " LEFT OUTER JOIN ".$eclass_db.".DBS_STUDENT_PREDICTED_GRADE_PERIOD_SETTING as predict_grade_period ON (y.YearID = predict_grade_period.ClassLevelID) ";
		    
		    $name_field = getNameFieldByLang("iu.");
		    $isDisabledInput = $portfolioAdmin? " '' " : " IF((NOW() BETWEEN predict_grade_period.PeriodStart AND predict_grade_period.PeriodEnd), '', ' disabled ')";
		    if($returnSQL) {
		        $fields = "";
		        $fields .= " iu.ClassName, iu.ClassNumber, $name_field as StudentName, ";
		        $fields .= " '' as PredictedGradeOriginal, '' as PredictedGradeInput1, '' as PredictedGradeInput2, '' as PredictedGradeFinal, iu.UserID, predict_grade.SubjectID, predict_grade.PredictedGradeID, predict_grade.PredictType, ";
		        $fields .= ($isViewOnly? " '1' " : " '0' ")." as isViewOnly ";
		    } else {
		        $fields = " predict_grade.PredictedGradeID, predict_grade.StudentID, predict_grade.SubjectID, predict_grade.SubjectGroupID, predict_grade.PredictedGrade, predict_grade.PredictPhase, predict_grade.PredictType ";
		    }
		    
		    $sql = "SELECT
                        $fields
                    FROM
                        $table
                        $extra_table
                    WHERE
                        1
                        $conds_id
                        $conds_student
                        $conds_year
                        $conds_subject
                        $conds_subject_group ";
		    if($returnSQL) {
		        $sql .= ' GROUP BY iu.UserID ';
		        return $sql;
		    } else {
		        return $this->objDB->returnArray($sql);
		    }
		}
		
		function getStudentAdjustmentLastModify($studentID='', $yearID='', $subjectID='', $subjectGroupID='', $portfolioAdmin=false, $predictedPhase='', $predictedType='') {
		    global $PATH_WRT_ROOT, $intranet_db, $eclass_db, $ipf_cfg, $iPort, $Lang;
		    
		    if(is_array($studentID) || (!is_array($studentID) && $studentID != '')) {
		        $conds_student = " AND iu.UserID IN ('".implode("', '", (array)$studentID)."') ";
		    }
		    if(is_array($yearID) || (!is_array($yearID) && $yearID != '')) {
		        $conds_year = " AND y.YearID IN ('".implode("', '", (array)$yearID)."') ";
		    }
		    if(is_array($subjectID) || (!is_array($subjectID) && $subjectID != '')) {
		        $extra_conds_subject = " AND predict_grade.SubjectID IN ('".implode("', '", (array)$subjectID)."') ";
		    }
		    if(is_array($subjectGroupID) || (!is_array($subjectGroupID) && $subjectGroupID != '')) {
		        include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		        $scm = new subject_class_mapping();
		        
		        $subjectGroupStudentIDs = $scm->Get_Subject_Group_Student_List($subjectGroupID);
		        $subjectGroupStudentIDs = Get_Array_By_Key($subjectGroupStudentIDs, 'UserID');
		        
		        $conds_subject_group = " AND iu.UserID IN ('".implode("', '", (array)$subjectGroupStudentIDs)."') ";
		    }
		    if(is_array($predictedPhase) || (!is_array($predictedPhase) && $predictedPhase != '')) {
		        $conds_predict_phase = " AND predict_grade.PredictPhase IN ('".implode("', '", (array)$predictedPhase)."') ";
		    }
		    if(is_array($predictedType) || (!is_array($predictedType) && $predictedType != '')) {
		        $conds_predict_type = " AND predict_grade.PredictType IN ('".implode("', '", (array)$predictedType)."') ";
		    }
		    
		    $table = $intranet_db.".INTRANET_USER as iu ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID) ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR as y ON (yc.YearID = y.YearID) ";
		    $extra_table .= " LEFT OUTER JOIN ".$eclass_db.".DBS_STUDENT_PREDICTED_GRADE as predict_grade ON (iu.UserID = predict_grade.StudentID ".$extra_conds_subject.") ";
		    $extra_table .= " LEFT OUTER JOIN ".$eclass_db.".DBS_STUDENT_PREDICTED_GRADE_PERIOD_SETTING as predict_grade_period ON (y.YearID = predict_grade_period.ClassLevelID) ";
		    
		    $fields = " predict_grade.ModifiedBy, predict_grade.DateModified ";
		    
		    $sql = "SELECT
		                $fields
		            FROM
		                $table
		                $extra_table
                    WHERE
                        1
                        $conds_year
                        $conds_subject
                        $conds_subject_group
                    ORDER BY
                        predict_grade.DateModified DESC
                    LIMIT 1";
            $result = $this->objDB->returnArray($sql);
            return $result[0];
		}
		
		public function insertUpdateStudentAdjustment($dataAry, $id='', $studentID='', $subjectID='', $predictedPhase='', $predictedType='') {
		    global $eclass_db, $UserID;
		    
		    $table = $eclass_db.".DBS_STUDENT_PREDICTED_GRADE";
		    if($id)
		    {
		        $field_sql = '';
		        foreach($dataAry as $field => $value) {
		            $field_sql .= $field_sql == ''? '' : ', ';
		            $field_sql .= " $field = '".HTMLtoDB($value)."' ";
		        }
		        $field_sql .= ", DateModified = NOW(), ModifiedBy = '$UserID' ";
		        
		        $sql = " UPDATE $table SET $field_sql WHERE PredictedGradeID = '$id' ";
		        $result = $this->objDB->db_db_query($sql);
		    }
		    else
		    {
		        if($studentID != '' && $subjectID != '' && $predictedPhase != '')
		        {
		            if($predictedType == 1) {
		                $sql = "SELECT COUNT(PredictedGradeID) FROM $table WHERE StudentID = '$studentID' AND SubjectID = '$subjectID' AND PredictPhase = '$predictedPhase' AND PredictType = '0'";
		                $is_exist = $this->objDB->returnVector($sql);
		                if($is_exist[0] > 0) {
		                    return;
		                }
		            }
		            
		            $sql = "SELECT COUNT(PredictedGradeID) FROM $table WHERE StudentID = '$studentID' AND SubjectID = '$subjectID' AND PredictPhase = '$predictedPhase'";
		            if($predictedType == 1) {
		                $sql .= "  AND PredictType = '1' ";
		            }
		            $is_exist = $this->objDB->returnVector($sql);
		            if($is_exist[0] > 0)
		            {
    		            $field_sql = '';
    		            foreach($dataAry as $field => $value) {
    		                $field_sql .= $field_sql == ''? '' : ', ';
    		                $field_sql .= " $field = '".HTMLtoDB($value)."' ";
    		            }
    		            $field_sql .= ", DateModified = NOW(), ModifiedBy = '$UserID' ";
    		            
    		            $sql = " UPDATE $table SET $field_sql WHERE StudentID = '$studentID' AND SubjectID = '$subjectID' AND PredictPhase = '$predictedPhase'";
    		            $result = $this->objDB->db_db_query($sql);
		            }
		        }
		        
		        if(!$result)
		        {
    		        $field_sql = '';
    		        $value_sql = '';
    		        foreach($dataAry as $field => $value) {
    		            $field_sql .= $field_sql == ''? '' : ', ';
    		            $field_sql .= "$field ";
    		            $value_sql .= $value_sql == ''? '' : ', ';
    		            $value_sql .= "'".HTMLtoDB($value)."' ";
    		        }
    		        $field_sql .= ", DateInput, InputBy, DateModified, ModifiedBy ";
    		        $value_sql .= ", NOW(), '$UserID', NOW(), '$UserID' ";
    		        
    		        $sql = " INSERT INTO $table ($field_sql) VALUES ($value_sql) ";
    		        $result = $this->objDB->db_db_query($sql);
		        }
		    }
		    //debug_pr($sql);
		    return $result;
		}
		
		function getStudentAdjustMinMax($recordID='', $studentID='', $yearID='', $subjectID='', $subjectGroupID='', $portfolioAdmin=false, $returnSQL=false, $isViewOnly=false) {
		    global $PATH_WRT_ROOT, $intranet_db, $eclass_db, $ipf_cfg, $iPort, $Lang;
		    
		    if($recordID != '') {
		        $conds_id = " AND predict_grade.PredictedGradeID = '$recordID' ";
		    }
		    if(is_array($studentID) || (!is_array($studentID) && $studentID != '')) {
		        $conds_student = " AND iu.UserID IN ('".implode("', '", (array)$studentID)."') ";
		    }
		    if(is_array($yearID) || (!is_array($yearID) && $yearID != '')) {
		        $conds_year = " AND y.YearID IN ('".implode("', '", (array)$yearID)."') ";
		    }
		    if(is_array($subjectID) || (!is_array($subjectID) && $subjectID != '')) {
		        // 		        $conds_subject = " AND predict_grade.SubjectID IN ('".implode("', '", (array)$subjectID)."') ";
		        $extra_conds_subject = " AND predict_grade.SubjectID IN ('".implode("', '", (array)$subjectID)."') ";
		    }
		    if(is_array($subjectGroupID) || (!is_array($subjectGroupID) && $subjectGroupID != '')) {
		        //$conds_subject_group = " AND predict_grade.SubjectGroupID IN ('".implode("', '", (array)$subjectGroupID)."') ";
		        
		        include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		        $scm = new subject_class_mapping();
		        
		        $subjectGroupStudentIDs = $scm->Get_Subject_Group_Student_List($subjectGroupID);
		        $subjectGroupStudentIDs = Get_Array_By_Key($subjectGroupStudentIDs, 'UserID');
		        
		        $conds_subject_group = " AND iu.UserID IN ('".implode("', '", (array)$subjectGroupStudentIDs)."') ";
		    }
		    
		    $table = $intranet_db.".INTRANET_USER as iu ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID) ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR as y ON (yc.YearID = y.YearID) ";
		    $extra_table .= " LEFT OUTER JOIN ".$eclass_db.".DBS_STUDENT_PREDICTED_GRADE_MIN_MAX as predict_grade ON (iu.UserID = predict_grade.StudentID ".$extra_conds_subject.") ";
		    
		    $name_field = getNameFieldByLang("iu.");
// 		    $isDisabledInput = $portfolioAdmin? " '' " : " IF((NOW() BETWEEN predict_grade_period.PeriodStart AND predict_grade_period.PeriodEnd), '', ' disabled ')";
		    if($returnSQL) {
		        $fields = " iu.ClassName, iu.ClassNumber, $name_field as StudentName, predict_grade.PredictedGradeMin, predict_grade.PredictedGradeMax, ";
		        if($portfolioAdmin) {
//                     $fields .= " IF(predict_grade.PredictedGradeID IS NOT NULL, 
//                                         CONCAT(
//                                             '<input type=\"text\" class=\"predict_inputs\" name=\"predict[0][', iu.UserID, ']\" value=\"',
//                                             IF((predict_grade.PredictedGrade IS NOT NULL AND predict_grade.PredictedGrade != ''), 
//                                                 predict_grade.PredictedGrade,
//                                                 IF((predict_grade.PredictedGradeMax IS NOT NULL AND predict_grade.PredictedGradeMax != ''), predict_grade.PredictedGradeMax, '')
//                                             ),
//                                             '\">'
//                                         ), '') as InputFields ";
                    $fields .= " predict_grade.PredictedGrade as InputFields, predict_grade.PredictedGradeID, iu.UserID ";
		        } else {
		            $fields .= "predict_grade.PredictedGrade as InputFields ";
		        }
		    } else {
		        $fields = " predict_grade.PredictedGradeID, predict_grade.StudentID, predict_grade.SubjectID, predict_grade.SubjectGroupID, predict_grade.PredictedGrade, predict_grade.PredictedGradeMin, predict_grade.PredictedGradeMax ";
		    }
		    
		    $sql = "SELECT
		              $fields
                    FROM
		                $table
                        $extra_table
		            WHERE
                        1
            		    $conds_id
            		    $conds_student
            		    $conds_year
            		    $conds_subject
            		    $conds_subject_group ";
		    if($returnSQL) {
		        return $sql;
		    } else {
		        return $this->objDB->returnArray($sql);
		    }
		}
		
		public function getStudentAdjustMinMaxLastModify($studentID='', $yearID='', $subjectID='', $subjectGroupID='', $portfolioAdmin=false) {
		    global $PATH_WRT_ROOT, $intranet_db, $eclass_db, $ipf_cfg, $iPort, $Lang;
		    
		    if(is_array($studentID) || (!is_array($studentID) && $studentID != '')) {
		        $conds_student = " AND iu.UserID IN ('".implode("', '", (array)$studentID)."') ";
		    }
		    if(is_array($yearID) || (!is_array($yearID) && $yearID != '')) {
		        $conds_year = " AND y.YearID IN ('".implode("', '", (array)$yearID)."') ";
		    }
		    if(is_array($subjectID) || (!is_array($subjectID) && $subjectID != '')) {
		        $extra_conds_subject = " AND predict_grade.SubjectID IN ('".implode("', '", (array)$subjectID)."') ";
		    }
		    if(is_array($subjectGroupID) || (!is_array($subjectGroupID) && $subjectGroupID != '')) {
		        //$conds_subject_group = " AND predict_grade.SubjectGroupID IN ('".implode("', '", (array)$subjectGroupID)."') ";
		        
		        include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		        $scm = new subject_class_mapping();
		        
		        $subjectGroupStudentIDs = $scm->Get_Subject_Group_Student_List($subjectGroupID);
		        $subjectGroupStudentIDs = Get_Array_By_Key($subjectGroupStudentIDs, 'UserID');
		        
		        $conds_subject_group = " AND iu.UserID IN ('".implode("', '", (array)$subjectGroupStudentIDs)."') ";
		    }
		    
		    $table = $intranet_db.".INTRANET_USER as iu ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID) ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR as y ON (yc.YearID = y.YearID) ";
		    $extra_table .= " LEFT OUTER JOIN ".$eclass_db.".DBS_STUDENT_PREDICTED_GRADE_MIN_MAX as predict_grade ON (iu.UserID = predict_grade.StudentID ".$extra_conds_subject.") ";
		    
	        $fields = " predict_grade.ModifiedBy, predict_grade.DateModified ";
		    
		    $sql = "SELECT
        		        $fields
        		    FROM
        		        $table
        		        $extra_table
        		    WHERE
        		        1
        		        $conds_student
            		    $conds_year
            		    $conds_subject
            		    $conds_subject_group 
                    ORDER BY 
                        predict_grade.DateModified DESC 
                    LIMIT 1";
		    $result = $this->objDB->returnArray($sql);
		    return $result[0];
		}
		
		public function insertUpdateStudentAdjustMinMax($dataAry, $id='', $studentID='', $subjectID='') {
		    global $eclass_db, $UserID;
		    
		    $table = $eclass_db.".DBS_STUDENT_PREDICTED_GRADE_MIN_MAX";
		    if($id)
		    {
		        $field_sql = '';
		        foreach($dataAry as $field => $value) {
		            $field_sql .= $field_sql == ''? '' : ', ';
		            $field_sql .= " $field = '".HTMLtoDB($value)."' ";
		        }
		        $field_sql .= ", DateModified = NOW(), ModifiedBy = '$UserID' ";
		        
		        $sql = " UPDATE $table SET $field_sql WHERE PredictedGradeID = '$id' ";
		        $result = $this->objDB->db_db_query($sql);
		    }
		    else
		    {
		        if($studentID != '' && $subjectID != '')
		        {
		            $sql = "SELECT COUNT(PredictedGradeID) FROM $table WHERE StudentID = '$studentID' AND SubjectID = '$subjectID' ";
		            $is_exist = $this->objDB->returnVector($sql);
		            if($is_exist[0] > 0)
		            {
    		            $field_sql = '';
    		            foreach($dataAry as $field => $value) {
    		                $field_sql .= $field_sql == ''? '' : ', ';
    		                $field_sql .= " $field = '".HTMLtoDB($value)."' ";
    		            }
    		            $field_sql .= ", DateModified = NOW(), ModifiedBy = '$UserID' ";
    		            
    		            $sql = " UPDATE $table SET $field_sql WHERE StudentID = '$studentID' AND SubjectID = '$subjectID'";
    		            $result = $this->objDB->db_db_query($sql);
		            }
		        }
		        
		        if(!$result)
		        {
		            $field_sql = '';
		            $value_sql = '';
		            foreach($dataAry as $field => $value) {
		                $field_sql .= $field_sql == ''? '' : ', ';
		                $field_sql .= "$field ";
		                $value_sql .= $value_sql == ''? '' : ', ';
		                $value_sql .= "'".HTMLtoDB($value)."' ";
		            }
		            $field_sql .= ", DateInput, InputBy, DateModified, ModifiedBy ";
		            $value_sql .= ", NOW(), '$UserID', NOW(), '$UserID' ";
		            
		            $sql = " INSERT INTO $table ($field_sql) VALUES ($value_sql) ";
		            $result = $this->objDB->db_db_query($sql);
		        }
		    }
		    
		    return $result;
		}
		
		public function getAdjustmentPeriodSettings($recordID='', $phase='') {
		    global $PATH_WRT_ROOT, $intranet_db, $eclass_db, $ipf_cfg, $iPort, $Lang;
		    
		    if($recordID != '') {
		        $conds_id = " AND RecordID = '$recordID' ";
		    }
		    if(is_array($phase) || (!is_array($phase) && $phase != '')) {
		        $conds_phase = " AND PredictPhase IN ('".implode("', '", (array)$phase)."') ";
		    }
		    
		    $table = $eclass_db.".DBS_STUDENT_PREDICTED_GRADE_PERIOD_SETTING ";
		    $fields = " RecordID, PeriodStart, PeriodEnd, PredictPhase ";
		    
		    $sql = "SELECT
                        $fields
                    FROM
                        $table
                        $extra_table
                    WHERE
                        1
                        $conds_id
                        $conds_phase ";
	        return $this->objDB->returnArray($sql);
		}
		
		public function insertUpdateAdjustmentPeriodSettings($dataAry, $phase)
		{
		    global $eclass_db, $UserID;
		    
		    $table = $eclass_db.".DBS_STUDENT_PREDICTED_GRADE_PERIOD_SETTING";
		    
		    $sql = "SELECT COUNT(RecordID) FROM $table WHERE PredictPhase = '$phase' ";
            $is_exist = $this->objDB->returnVector($sql);
            if($is_exist[0] > 0)
            {
                $field_sql = '';
                foreach($dataAry as $field => $value) {
                    $field_sql .= $field_sql == ''? '' : ', ';
                    $field_sql .= " $field = '".HTMLtoDB($value)."' ";
                }
                $field_sql .= ", DateModified = NOW(), ModifiedBy = '$UserID' ";
                
                $sql = " UPDATE $table SET $field_sql WHERE PredictPhase = '$phase' ";
                $result = $this->objDB->db_db_query($sql);
            }
            else
	        {
	            $field_sql = '';
	            $value_sql = '';
	            foreach($dataAry as $field => $value) {
	                $field_sql .= $field_sql == ''? '' : ', ';
	                $field_sql .= "$field ";
	                $value_sql .= $value_sql == ''? '' : ', ';
	                $value_sql .= "'".HTMLtoDB($value)."' ";
	            }
	            $field_sql .= ", DateInput, InputBy, DateModified, ModifiedBy ";
	            $value_sql .= ", NOW(), '$UserID', NOW(), '$UserID' ";
	            
	            $sql = " INSERT INTO $table ($field_sql) VALUES ($value_sql) ";
	            $result = $this->objDB->db_db_query($sql);
	        }
		    
		    return $result;
		}
		
		public function getClassListFromStudent($studentIDArr)
		{
		    global $PATH_WRT_ROOT, $intranet_db, $eclass_db, $ipf_cfg, $iPort, $Lang;
		    
		    $conds_student = " AND iu.UserID IN ('".implode("', '", (array)$studentIDArr)."') ";
		    
		    $table = $intranet_db.".INTRANET_USER as iu ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID) ";
		    $extra_table .= " INNER JOIN ".$intranet_db.".YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') ";
		    
	        $fields = " yc.YearClassID, yc.ClassTitleEN, yc.ClassTitleB5 ";
		    
		    $sql = "SELECT
                        $fields
		            FROM
		                $table
		                $extra_table
		            WHERE
		                1
                        $conds_id
                        $conds_student
                    GROUP BY yc.YearClassID ";
	        return $this->objDB->returnArray($sql);
		}
		
		public function GetAcademicYearListFromClassHistory()
		{
		    $sql = "SELECT DISTINCT AcademicYearID, AcademicYear FROM PROFILE_CLASS_HISTORY WHERE AcademicYearID IS NOT NULL ORDER BY AcademicYear ASC";
		    return $this->objDB->returnArray($sql);
		}
	}
}