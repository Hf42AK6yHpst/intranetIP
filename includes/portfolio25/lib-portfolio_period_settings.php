<?php 
// Using by : 

/*******************************************
*	modification log
*	2018-02-02 Anna
*	- added updatePersonalCharacterPeriodSetting()
*	2017-03-27	Omas 
*	- modified loadPeriodSettingFromStorage() fix new add classlevel cannot show problem
*	2014-12-23	Bill [#J62561]
*	- Create file
*******************************************/

class iportfolio_period_settings {
	private $objDb;
	private $classLevelArr;
	private $recordTypeArr;
	private $periodSettings;
	private $emptyArray;

	function iportfolio_period_settings() {
		$this->objDb = new libdb();
        $this->recordTypeArr = array("OLE", "EXT", "SLP");
     
    	$this->SetEmptyRecordArr();
    	
        # Get Class Level
		$this->getClassLevel();
		# Get all Period Settings from DB
		$this->loadPeriodSettingFromStorage();
	}
	
	# Get all Period Settings from DB
	private function loadPeriodSettingFromStorage(){
		global $eclass_db,$sys_custom;
		
		$classLevels = $this->classLevelArr;
		$recordTypesArr = $this->recordTypeArr;
		$classLevelIDArr = array_keys($classLevels);
		$periodArr = array('OLE' => array(), 'EXT' => array(), 'SLP' => array());

		if($sys_custom['iPf']['twghczm']['Report']['SLP']){
			$periodArr = array('OLE' => array(), 'EXT' => array(), 'SLP' => array(),'TWGHCZM' => array());
		}

		# Get records from DB
		$sql= "Select * From {$eclass_db}.IPORTFOLIO_SUBMIT_PERIOD";
		$result = $this->objDb->returnResultSet($sql);

		$emptyArr = $this->emptyArray;
		# Handle existing records
		if(count($result) > 0){
			$addedClassID = array();
			
			foreach($result as $record){
				$recordTypes = $record['RecordType'];
				$ClassLevelIDs = $record['ClassLevelID'];			
				$allow_submit = $record['AllowSubmit'];
				# Get display time format
				list($startTime, $sh, $sm) = $this->returnDisplayTimeFormat($record['StartTime']);
				list($endTime, $eh, $em) = $this->returnDisplayTimeFormat($record['EndTime']);
				
				# Add to settings array
				$periodArr[$recordTypes][$ClassLevelIDs]['ClassLevel'] = $classLevels[$ClassLevelIDs];
				$periodArr[$recordTypes][$ClassLevelIDs]['StartDate'] = $startTime;
				$periodArr[$recordTypes][$ClassLevelIDs]['StartHour'] =  $sh;
				$periodArr[$recordTypes][$ClassLevelIDs]['StartMinute'] = $sm;
				$periodArr[$recordTypes][$ClassLevelIDs]['EndDate'] = $endTime;
				$periodArr[$recordTypes][$ClassLevelIDs]['EndHour'] = $eh;
				$periodArr[$recordTypes][$ClassLevelIDs]['EndMinute'] = $em;
				$periodArr[$recordTypes][$ClassLevelIDs]['AllowSubmit'] = $allow_submit;
				
				$addedClassID[] = $ClassLevelIDs;
			}
			
			# Set default value to forms without records
			$missedClassID = array_diff($classLevelIDArr, $addedClassID);
			
			// 2017-03-27 Omas Fixed , new added class level cannot show problem
			//for($i=0; $i<count($missedClassID); $i++){
 			//$ClassLevelIDs = $missedClassID[$i];
			foreach((array)$missedClassID as $_key=>$_val){
				$ClassLevelIDs = $_val;
				for($j=0; $j<count($recordTypesArr); $j++){
					$recordTypes = $recordTypesArr[$j];
					
					if(!isset($periodArr[$recordTypes][$ClassLevelIDs])){
						$periodArr[$recordTypes][$ClassLevelIDs] = $emptyArr;
						$periodArr[$recordTypes][$ClassLevelIDs]['ClassLevel'] = $classLevels[$ClassLevelIDs];
					}
				}
			}
		}
		else {
		# set default value if no records exist
			foreach($periodArr as $recordTypes => $recordContent){
				foreach($classLevels as $ClassID => $classContent){
					# set default values
					$periodArr[$recordTypes][$ClassID] = $emptyArr;
					$periodArr[$recordTypes][$ClassID]['ClassLevel'] = $classLevels[$ClassID];
				}
			}
		}
		if(empty($periodArr['TWGHCZM'])){
				foreach($classLevels as $ClassID => $classContent){
					# set default values
					$periodArr['TWGHCZM'][$ClassID] = $emptyArr;
					$periodArr['TWGHCZM'][$ClassID]['ClassLevel'] = $classLevels[$ClassID];
				}
		}
	//	debug_pr($periodArr['twghczm']);
		$this->periodSettings = $periodArr;
	}
	
	# Update all period settings in DB
	public function updatePeriodSetting($request){
		global $eclass_db;

		$success = array();
		$types = $this->recordTypeArr;
		$countTypes = count($types);
		$countClasses = count($request['starttime_OLE']);
		
		# Get existing Class ID stored in DB
		$sql = "Select ClassLevelID From {$eclass_db}.IPORTFOLIO_SUBMIT_PERIOD Group By ClassLevelID";
		$classIDs = $this->objDb->returnVector($sql);
		
		# Remove existing records in DB
//		$sql = "DELETE FROM {$eclass_db}.IPORTFOLIO_SUBMIT_PERIOD Where ClassLevelID IN ('".implode("', '", array_keys($this->classLevelArr))."')";
//		$success[] = $this->objDb->db_db_query($sql);
		
		for($i=0; $i<$countTypes; $i++){
			$recordType = trim($types[$i]);
			
			# Get time format for storage
			$startTime = $this->returnDBTimeFormat($request['starttime_'.$recordType], $request['sh_'.$recordType], $request['sm_'.$recordType]);
			$endTime = $this->returnDBTimeFormat($request['endtime_'.$recordType], $request['eh_'.$recordType], $request['em_'.$recordType]);
			
			for($j=0; $j<$countClasses; $j++){
				$ClassLevelID = $request['LevelID_'.$recordType][$j];
				$allowSubmit = ($request['allow_submit_'.$recordType.'_'.$j] == true)? '1' : '0';
				
				# Update existing records in DB
				if(in_array($ClassLevelID, $classIDs)){
					$sql = "UPDATE {$eclass_db}.IPORTFOLIO_SUBMIT_PERIOD SET StartTime = '".$startTime[$j]."', EndTime = '".$endTime[$j]."', AllowSubmit = '".$allowSubmit."', 
								ModifiedBy = '".$_SESSION["UserID"]."', DateModified = now() Where RecordType = '".$recordType."' AND ClassLevelID = '".$ClassLevelID."'";
				} 
				# Insert records to DB
				else {
					$sql = "INSERT INTO {$eclass_db}.IPORTFOLIO_SUBMIT_PERIOD (RecordType, ClassLevelID, StartTime, EndTime, AllowSubmit, InputBy, DateInput, ModifiedBy, DateModified) 
								VALUES ('".$recordType."', '".$ClassLevelID."', '".$startTime[$j]."', '".$endTime[$j]."', '".$allowSubmit."', '".$_SESSION["UserID"]."', now(), '".$_SESSION["UserID"]."', now())";
				}
				$success[] = $this->objDb->db_db_query($sql);
			}
		}
		
		if(in_array(false, $success))
			return false;
		else 
			return true;
	}
	public function updatePersonalCharacterPeriodSetting($request){
		
		global $eclass_db,$sys_custom;
		
		$success = array();
		
		$countClasses = count($request['starttime_TWGHCZM']);
		$recordType = 'TWGHCZM';
		# Get existing Class ID stored in DB
		$sql = "Select ClassLevelID From {$eclass_db}.IPORTFOLIO_SUBMIT_PERIOD where RecordType='$recordType' Group By ClassLevelID ";
		$classIDs = $this->objDb->returnVector($sql);

			# Get time format for storage
		$startTime = $this->returnDBTimeFormat($request['starttime_'.$recordType], $request['sh_'.$recordType], $request['sm_'.$recordType]);
		$endTime = $this->returnDBTimeFormat($request['endtime_'.$recordType], $request['eh_'.$recordType], $request['em_'.$recordType]);
			
		for($j=0; $j<$countClasses; $j++){
			$ClassLevelID = $request['LevelID_'.$recordType][$j];
			$allowSubmit = ($request['allow_submit_'.$recordType.'_'.$j] == true)? '1' : '0';
		
			# Update existing records in DB
			if(in_array($ClassLevelID, $classIDs)){
				$sql = "UPDATE {$eclass_db}.IPORTFOLIO_SUBMIT_PERIOD SET StartTime = '".$startTime[$j]."', EndTime = '".$endTime[$j]."', AllowSubmit = '".$allowSubmit."',
							ModifiedBy = '".$_SESSION["UserID"]."', DateModified = now() Where RecordType = '".$recordType."' AND ClassLevelID = '".$ClassLevelID."'";
			}
				# Insert records to DB
			else {
				$sql = "INSERT INTO {$eclass_db}.IPORTFOLIO_SUBMIT_PERIOD (RecordType, ClassLevelID, StartTime, EndTime, AllowSubmit, InputBy, DateInput, ModifiedBy, DateModified)
				VALUES ('".$recordType."', '".$ClassLevelID."', '".$startTime[$j]."', '".$endTime[$j]."', '".$allowSubmit."', '".$_SESSION["UserID"]."', now(), '".$_SESSION["UserID"]."', now())";
			
			}
	
			$success[] = $this->objDb->db_db_query($sql);
		}
		
		
		if(in_array(false, $success))
			return false;
			else
				return true;
	}
	
	# return date, hour and time from string
	public function returnDisplayTimeFormat($timeStr){
		$timeStr = trim($timeStr);
				
		if($timeStr != "" && $timeStr != '0000-00-00 00:00:00'){
			$timeDetailArr = explode(" ", $timeStr);
			$timeDate = $timeDetailArr[0]; 
			$timeArray = explode(":", $timeDetailArr[1]);
			$timeHour = $timeArray[0];
			$timeMin = $timeArray[1];
			return array($timeDate, $timeHour, $timeMin);
		} 
		# Empty time
		else {
			return array("", "", "");
		}
	}
	
	# return time string array
	public function returnDBTimeFormat($dateArr, $hrArr, $minArr){
		$timeStr = array();
		for($i=0; $i<count($dateArr); $i++){
			$timeStr[$i] = (trim($dateArr[$i])!="")? sprintf($dateArr[$i]." %02s:%02s:00", $hrArr[$i], $minArr[$i]) : "";
		}
		return $timeStr;
	}

	# Get Class level
	public function getClassLevel(){
		$sql = 'Select YearID as ClassLevelID, YearName as LevelName From YEAR Order By Sequence';
        $classArr = $this->objDb->returnArray($sql,2);
        $this->classLevelArr = BuildMultiKeyAssoc($classArr, "ClassLevelID");
	}
	
	private function SetEmptyRecordArr(){
		$emptyArr = array();
		
		$emptyArr['StartDate'] = "";
		$emptyArr['StartHour'] = "00";
		$emptyArr['StartMinute'] = "00";
		$emptyArr['EndDate'] = "";
		$emptyArr['EndHour'] = "00";
		$emptyArr['EndMinute'] = "00";
		$emptyArr['AllowSubmit'] = 0;
		
		$this->emptyArray = $emptyArr;
	}
	
	# Return Period Settings
	public function getSettingsArray($targetType="", $classLevelID=""){
		$settings = $this->periodSettings;
		$classIDs = array_keys($this->classLevelArr);
		if(trim($targetType) != "" && in_array($targetType, $this->recordTypeArr)){
			$settings = $settings[$targetType];
			if(trim($classLevelID) != "" && in_array($classLevelID, $classIDs)){
				$settings = $settings[$classLevelID];
			}
		}
		return $settings;
	}
	
}
?>