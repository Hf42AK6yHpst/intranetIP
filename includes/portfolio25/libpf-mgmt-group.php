<?
/*
 * Change Log:
 * Date:	2017-05-23 Villa
 * -		Modified HAS_FUNCTION_RIGHT, UPDATE_GROUP_MGMT_RIGHT: modified getGroupFunctionRight() to getGroupFunctionRight(true) in handling cannot get the group setting if User is Module Admin
 * -		Modified getGroupFunctionRight:	Add variable $ignoreAdminOverwriteGroupAccess to skip the reset of group_mgmt_function
 * 
 * Date:	2017-01-13 Villa 
 * -		Modified getGroupFunctionRight() Overwrite groupAccess Right to allow admin control all the function in the module
 */
include_once($intranet_root."/includes/portfolio25/libpf-group.php");

class libpf_mgmt_group extends libpf_group{

	protected $group_function;

  public function __construct(){
  
		parent::__construct();
		
		$this->group_mgmt_function = array();
  }

##########################################################################
# Get Functions
##########################################################################
  
	/**
	 * return OLE components the groups can manage
	 * @global string eclass DB name
	 * @param array $group_id_arr id of groups
	 * @return array 
	 */
  public function getGroupComponent($group_id_arr){
		global $eclass_db;
		
		$group_id_str = is_array($group_id_arr) ? implode("', '", $group_id_arr) : $group_id_arr;
	
		$sql = "SELECT DISTINCT ";
		$sql .= "ELE_ID ";
		$sql .= "FROM {$eclass_db}.OLE_MGMT_GROUP ";
		$sql .= "WHERE group_id IN ('{$group_id_str}')";
		
		$db = $this->db;
	  $ele_id_arr = $db->returnVector($sql);
	  
	  return $ele_id_arr;
	}
	
	/**
	* return group management function
	* please run set function before use	
	* @return array 
	*/
	public function getGroupFunctionRight($ignoreAdminOverwriteGroupAccess=false)
	{
		global $sys_custom;
		$isAdmin = $_SESSION['SSV_USER_ACCESS']['other-iPortfolio'];
		if($sys_custom['iPf']['AdminOverwriteGroupAccess'] && $isAdmin && !$ignoreAdminOverwriteGroupAccess){
			$x = array();
		}else{
			$x = $this->group_mgmt_function;
		}
		return $x;
// 		return $this->group_mgmt_function;
	}
	
##########################################################################
# Set Functions
##########################################################################

	/**
	 * set group function rights
	 * @param array $mgmt_func_arr array of function codes, retrieve from DB if nothing is passed 
	 */
	public function setGroupFunctionRight($mgmt_func_arr=null)
	{
    $t_group_id = $this->getGroupID();
    if($t_group_id == "") return false;
	
		if(!isset($mgmt_func_arr))
    {
			$sql = "SELECT function_right ";
			$sql .= "FROM mgt_grouping_function ";
			$sql .= "WHERE group_id = {$t_group_id}";
			$db = $this->db;
			$mgmt_func = current($db->returnVector($sql));
	
			$mgmt_func_arr = array_filter(explode(", ", $mgmt_func));
		}
	
		$this->group_mgmt_function = $mgmt_func_arr;
	}
	
##########################################################################
# Manipulation
##########################################################################

	/**
	 * update group function rights set in class variable
	 * please run set function before use	 
	 */
	public function UPDATE_GROUP_MGMT_RIGHT()
	{
    $t_group_id = $this->getGroupID();
    if($t_group_id == "") return false;
		
    $group_right_str = implode(", ", $this->getGroupFunctionRight(true));
		
		$sql = "INSERT INTO mgt_grouping_function (group_id, function_right) ";
		$sql .= "VALUES ({$t_group_id}, '{$group_right_str}') ";
		$sql .= "ON DUPLICATE KEY UPDATE function_right = VALUES(function_right)";
		
		$db = $this->db;
		$db->db_db_query($sql);
	}
	
	/**
	 * update components which the group can manage
	 * @global string eclass DB name
	 * @param array $ele_arr array of component code
	 * @param integer $uID user ID of of modifier
	 */
	public function UPDATE_GROUP_COMPONENT($ele_arr, $uID)
	{
		global $eclass_db;
	
    $t_group_id = $this->getGroupID();
    if($t_group_id == "") return false;
    
    $t_function_right = $this->getGroupFunctionRight(true);
		$db = $this->db;
		
		$sql = "DELETE FROM {$eclass_db}.OLE_MGMT_GROUP ";
		$sql .= "WHERE group_id = {$t_group_id}";
		$db->db_db_query($sql);

		if($this->HAS_FUNCTION_RIGHT("Profile:OLR"))
		{
			if(count($ele_arr) > 0)
			{
				$val_str = array();
				for($i=0, $i_max=count($ele_arr); $i<$i_max; $i++)
				{
					$val_arr[] = "({$t_group_id}, '{$ele_arr[$i]}', NOW(), {$uID}, {$uID})";
				}
				$val_str = implode(", ", $val_arr);
				
				if(!empty($val_str))
				{
					$sql = "INSERT INTO {$eclass_db}.OLE_MGMT_GROUP ";
					$sql .= "(group_id, ELE_ID, DateInput, InputBy, ModifyBy) ";
					$sql .= "VALUES {$val_str}";
					$db->db_db_query($sql);
				}
			}
		}
	}
	
	/**
	 * Check if the group has certain function right
	 * @param string $function_code code of function right to be checked
	 * @return boolean 
	 */
	public function HAS_FUNCTION_RIGHT($function_code){
		$this->setGroupFunctionRight();
		if($function_code=='Profile:OLR'){
			$mgmt_func_arr = $this->getGroupFunctionRight(true);
		}else{
			$mgmt_func_arr = $this->getGroupFunctionRight();
		}
// 		$mgmt_func_arr = $this->getGroupFunctionRight();
		
		return in_array($function_code, $mgmt_func_arr);
	}
}

?>