<?
# modifying :  

include_once($intranet_root."/includes/portfolio25/volunteer/volunteerConfig.inc.php");
if (!defined("LIBPF_VOLUNTEER_DEFINED"))         // Preprocessor directives
{
	define("LIBPF_VOLUNTEER_DEFINED",true);

//	class libpf_volunteer extends libdb {
	class libpf_volunteer {
		
		private $objDB;
		private $ipf_cfg;
		
		public function __construct() {
			global $ipf_cfg;

			$this->objDB = new libdb();
			$this->ipf_cfg = $ipf_cfg;
		}

//		private function db_db_query($sql){
//			return $this->objDB->db_db_query($sql);
//		}
//
//		private function returnArray($sql){
//			return $this->objDB->returnArray($sql);
//		}
//		
//		private function pack_value($value, $type='') {
//			return $this->objDB->pack_value($value, $type);
//		}
		
		public function getComeFromCode_TeacherInput() {
			return $this->ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherInput"];
		}
		public function getComeFromCode_StudentInput() {
			return $this->ipf_cfg["OLE_PROGRAM_COMEFROM"]["studentInput"];
		}
		
		public function getEventCodeMaxLength() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["Code"]["MaxLength"];
		}
		public function getEventNameMaxLength() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["Title"]["MaxLength"];
		}
		public function getEventOrganizationMaxLength() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["Organization"]["MaxLength"];
		}		
		public function getEventRemoveStatusCode_Active() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["RemoveStatus"]["Active"];
		}
		public function getEventRemoveStatusCode_Deleted() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["RemoveStatus"]["Deleted"];
		}
		public function getEventCreatorTypeCode_Teacher() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["CreatorType"]["Teacher"];
		}
		public function getEventCreatorTypeCode_Student() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["CreatorType"]["Student"];
		}
		public function getEventCreatorTypeCode_Parent() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["CreatorType"]["Parent"];
		}
		
		public function getEventUserRoleMaxLength() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["Role"]["MaxLength"];
		}
		public function getEventUserRemoveStatusCode_Active() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["RemoveStatus"]["Active"];
		}
		public function getEventUserRemoveStatusCode_Deleted() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["RemoveStatus"]["Deleted"];
		}
		public function getEventUserApproveStatusCode_Pending() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["ApproveStatus"]["Pending"];
		}
		public function getEventUserApproveStatusCode_Approved() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["ApproveStatus"]["Approved"];
		}
		public function getEventUserApproveStatusCode_Rejected() {
			return $this->ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["ApproveStatus"]["Rejected"];
		}
		
		
		/**
		 * Check if the Event Code is valid
		 * @owner : Ivan (20110819)
		 * @param : String $Code, the target Event Code to be checked
		 * @param : Integer $ExcludeEventID, the EventID to be excluded in the checking (for edit Event use)
		 * @return : Boolean, true or false
		 */
		public function checkEventCodeValid($Code, $ExcludeEventID='') {
			global $eclass_db;
			
			$conds_ExcludeEventID = '';
			if ($ExcludeEventID != '') {
				$conds_ExcludeEventID = " And EventID != '$ExcludeEventID' ";
			}
			
			$VOLUNTEER_EVENT = $eclass_db.'.VOLUNTEER_EVENT';
			$sql = "Select
							EventID
					From
							$VOLUNTEER_EVENT
					Where
							Code = '".$this->objDB->Get_Safe_Sql_Query(trim($Code))."'
							And	RemoveStatus = '".$this->getEventRemoveStatusCode_Active()."'
							$conds_ExcludeEventID
					";
			$ResultArr = $this->objDB->returnArray($sql, null, 1);
			return (count($ResultArr) > 0)? false : true;
		}
		
		/**
		 * Get the SQL statement for the Event List of Teacher View 
		 * @owner : Ivan (20110824)
		 * @para : String $Keyword, the searching keyword
		 * @return : String, the SQL statement
		 */
		public function getEventListTeacherViewSQL($Keyword='') {
			global $eclass_db, $intranet_db;
			
			if ($Keyword != '') {
				$Keyword = $this->objDB->Get_Safe_Sql_Like_Query($Keyword);
				$conds_Keyword = " And  (	
											e.Code Like '%".$Keyword."%'
											Or e.Title Like '%".$Keyword."%'
											Or e.StartDate Like '%".$Keyword."%'
											Or e.Organization Like '%".$Keyword."%'
										)";
			}
			
			$VOLUNTEER_EVENT = $eclass_db.'.VOLUNTEER_EVENT';
			$INTRANET_USER = $intranet_db.'.INTRANET_USER';
			
			$LastModifiedNameField = getNameFieldByLang('iu.');
			$sql = "Select
							e.Code,
							e.Title,
							e.StartDate,
							e.Organization,
							e.ModifiedDate,
							$LastModifiedNameField as LastModifiedName,
							CONCAT('<input type=\"checkbox\" name=\"EventIDArr[]\" value=\"', e.EventID, '\" onclick=\"unset_checkall(this, this.form);\">') as Checkbox
					From
							$VOLUNTEER_EVENT as e
							Inner Join $INTRANET_USER as iu On (e.ModifyBy = iu.UserID)
					Where
							e.RemoveStatus = '".$this->getEventRemoveStatusCode_Active()."'
							$conds_Keyword
					";
			return $sql;
		}
		
		/**
		 * Get the SQL statement for the Applied Event List of Student View 
		 * @owner : Ivan (20110830)
		 * @para : String $Keyword, the searching keyword
		 * @return : String, the SQL statement
		 */
		public function getAppliedEventListStudentViewSQL($StudentIDArr, $Keyword='') {
			global $eclass_db, $intranet_db;
			global $linterface;
			
			if ($Keyword != '') {
				$Keyword = $this->objDB->Get_Safe_Sql_Like_Query($Keyword);
				$conds_Keyword = " And  (	
											e.Title Like '%".$Keyword."%'
											Or e.StartDate Like '%".$Keyword."%'
											Or e.Organization Like '%".$Keyword."%'
											Or eu.Hours Like '%".$Keyword."%'
											Or eu.Role Like '%".$Keyword."%'
											Or eu.Achievement Like '%".$Keyword."%'
										)";
			}
			
			$VOLUNTEER_EVENT = $eclass_db.'.VOLUNTEER_EVENT';
			$VOLUNTEER_EVENT_USER = $eclass_db.'.VOLUNTEER_EVENT_USER';
			$INTRANET_USER = $intranet_db.'.INTRANET_USER';
			
			$TeacherPICNameField = getNameFieldByLang('iu.');
			$sql = "Select
							e.Title,
							e.StartDate,
							e.Organization,
							eu.Hours,
							eu.Role,
							eu.Achievement,
							$TeacherPICNameField as TeacherPICName,
							CASE eu.ApproveStatus
								WHEN ".$this->getEventUserApproveStatusCode_Pending()." THEN '".$linterface->Get_Pending_Image($Title='')."'
								WHEN ".$this->getEventUserApproveStatusCode_Approved()." THEN '".$linterface->Get_Approved_Image($Title='')."'
								WHEN ".$this->getEventUserApproveStatusCode_Rejected()." THEN '".$linterface->Get_Rejected_Image($Title='')."'
							END as ApproveStatusIcon,
							eu.ModifiedDate,
							CONCAT(	'<input type=\"checkbox\" name=\"RecordIDArr[]\" value=\"', 
									eu.RecordID, 
									'\" onclick=\"unset_checkall(this, this.form);\"',
									If (eu.ApproveStatus = '".$this->getEventUserApproveStatusCode_Approved()."', 'Disabled', ''),
									' />'
							) as Checkbox
					From
							$VOLUNTEER_EVENT as e
							Inner Join $VOLUNTEER_EVENT_USER as eu On (e.EventID = eu.EventID)
							Inner Join $INTRANET_USER as iu On (eu.TeacherPIC = iu.UserID)
					Where
							e.RemoveStatus = '".$this->getEventRemoveStatusCode_Active()."'
							And eu.RemoveStatus = '".$this->getEventUserRemoveStatusCode_Active()."'
							And eu.UserID In ('".implode("','", (array)$StudentIDArr)."')
							$conds_Keyword
					";
			return $sql;
		}
		
		/**
		 * Get Event Info array which satisfy the parameter conditions
		 * @owner : Ivan (20110824)
		 * @para : Integer / Array $ExcludeEventIDArr, the EventID array which should be ignored (e.g. The Event which the student has applied already)
		 * @return : Array, the Event Info Array
		 */
		public function getEventInfo($EventIDArr='') {
			global $eclass_db, $intranet_db;
			
			if ($EventIDArr != '') {
				$conds_EventID = " And e.EventID In ('".implode("','", (array)$EventIDArr)."') ";
			}
			
			$VOLUNTEER_EVENT = $eclass_db.'.VOLUNTEER_EVENT';
			$sql = "Select
							e.EventID,
							e.Code,
							e.Title,
							e.StartDate,
							e.Organization
					From
							$VOLUNTEER_EVENT as e
					Where
							e.RemoveStatus = '".$this->getEventRemoveStatusCode_Active()."'
							$conds_EventID
					Order By
							e.Title, e.StartDate desc
					";
			return $this->objDB->returnArray($sql, null, 1);
		}
		
		/**
		 * Get Event Info array which the student has joined
		 * @owner : Ivan (20110826)
		 * @para : Integer / Array $StudentIDArr, the StudentID who to be checked
		 * @return : Array, an Associative array contains Students' Event Info. $Arr[$StudentID][0,1,2,...]['Title' / 'Code' / 'Role' / 'Hours' / ...] = Value
		 */
		public function getStudentAppliedEventInfo($StudentIDArr, $RecordIDArr='') {
			global $eclass_db, $intranet_db;
			
			if ($RecordIDArr != '') {
				$conds_RecordID = " And eu.RecordID In ('".implode("','", (array)$RecordIDArr)."') ";
			}
			
			$VOLUNTEER_EVENT = $eclass_db.'.VOLUNTEER_EVENT';
			$VOLUNTEER_EVENT_USER = $eclass_db.'.VOLUNTEER_EVENT_USER';
			$sql = "Select
							eu.RecordID,
							eu.UserID,
							e.EventID,
							e.Code,
							e.Title,
							e.StartDate,
							e.Organization,
							eu.Hours,
							eu.Role,
							eu.Achievement,
							eu.TeacherPIC
					From
							$VOLUNTEER_EVENT as e
							Inner Join $VOLUNTEER_EVENT_USER as eu On (e.EventID = eu.EventID)
					Where
							e.RemoveStatus = '".$this->getEventRemoveStatusCode_Active()."'
							And eu.RemoveStatus = '".$this->getEventUserRemoveStatusCode_Active()."'
							And eu.UserID In ('".implode("','", (array)$StudentIDArr)."')
							$conds_RecordID
					Order By
							e.Title, e.StartDate desc
					";
			return $this->objDB->returnArray($sql, null, 1);
		}
		
		/**
		 * Get the SQL statement for the Event Approval List of Teacher View 
		 * @owner : Ivan (20110831)
		 * @para : String $Keyword, the searching keyword
		 * @return : String, the SQL statement
		 */
		public function getEventApprovalListTeacherViewSQL($Keyword, $ApproveStatus, $ProgramType) {
			global $eclass_db, $intranet_db;
			global $linterface, $lpf;
			
			$StudentNameField = getNameFieldByLang('iu_student.');
			$TeacherPICNameField = getNameFieldByLang('iu_pic.');
			
			
			$IsSuperAdmin = $lpf->IS_IPF_SUPERADMIN();
			if (!$IsSuperAdmin || $ProgramType == 'MyProgram') {
				$conds_TeacherPIC = " And eu.TeacherPIC = '".$_SESSION['UserID']."' ";
			}
			
			if ($Keyword != '') {
				$Keyword = $this->objDB->Get_Safe_Sql_Like_Query($Keyword);
				$conds_Keyword = " And  (	
											iu_student.ClassName Like '%".$Keyword."%'
											Or iu_student.ClassNumber Like '%".$Keyword."%'
											Or $StudentNameField Like '%".$Keyword."%'
											Or e.Title Like '%".$Keyword."%'
											Or e.StartDate Like '%".$Keyword."%'
											Or e.Organization Like '%".$Keyword."%'
											Or eu.Hours Like '%".$Keyword."%'
											Or eu.Role Like '%".$Keyword."%'
											Or eu.Achievement Like '%".$Keyword."%'
											Or $TeacherPICNameField Like '%".$Keyword."%'
										)";
			}
			
			if ($ApproveStatus != '') {
				$conds_ApprovalStatus = " And eu.ApproveStatus = '".$ApproveStatus."' ";
			}
			
			$VOLUNTEER_EVENT = $eclass_db.'.VOLUNTEER_EVENT';
			$VOLUNTEER_EVENT_USER = $eclass_db.'.VOLUNTEER_EVENT_USER';
			$INTRANET_USER = $intranet_db.'.INTRANET_USER';
			
			$sql = "Select
							iu_student.ClassName,
							iu_student.ClassNumber,
							$StudentNameField as StudentName,
							e.Title,
							e.StartDate,
							e.Organization,
							eu.Hours,
							eu.Role,
							eu.Achievement,
							$TeacherPICNameField as TeacherPICName,
							CASE eu.ApproveStatus
								WHEN ".$this->getEventUserApproveStatusCode_Pending()." THEN '".$linterface->Get_Pending_Image($Title='')."'
								WHEN ".$this->getEventUserApproveStatusCode_Approved()." THEN '".$linterface->Get_Approved_Image($Title='')."'
								WHEN ".$this->getEventUserApproveStatusCode_Rejected()." THEN '".$linterface->Get_Rejected_Image($Title='')."'
							END as ApproveStatusIcon,
							CONCAT('<input type=\"checkbox\" name=\"RecordIDArr[]\" value=\"', eu.RecordID, '\" onclick=\"unset_checkall(this, this.form);\">') as Checkbox
					From
							$VOLUNTEER_EVENT as e
							Inner Join $VOLUNTEER_EVENT_USER as eu On (e.EventID = eu.EventID)
							Inner Join $INTRANET_USER as iu_pic On (eu.TeacherPIC = iu_pic.UserID)
							Inner Join $INTRANET_USER as iu_student On (eu.UserID = iu_student.UserID)
					Where
							e.RemoveStatus = '".$this->getEventRemoveStatusCode_Active()."'
							And eu.RemoveStatus = '".$this->getEventUserRemoveStatusCode_Active()."'
							$conds_Keyword
							$conds_TeacherPIC
							$conds_ApprovalStatus
					";
			return $sql;
		}
	}
}
?>