<?
# modifying :  

if (!defined("LIBPF_VOLUNTEER_UI_DEFINED"))         // Preprocessor directives
{
	define("LIBPF_VOLUNTEER_UI_DEFINED",true);

	class libpf_volunteer_ui {
		private $linterface;
		
		public function __construct() {
			
		}
		
		/**
		 * Get the DB Table for the Teacher's view Event List
		 * @owner : Ivan (20110824)
		 * @param : Integer $field, the sorting field of the DB Table
		 * @param : Integer $order, the sorting order of the DB Table
		 * @param : Integer $pageNo, the current page number of the DB Table
		 * @param : String $Keyword, the seraching keyword 
		 * @return : String, the html of the DB Table
		 */
		public function getEventListTeacherViewDBTable($field='', $order='', $pageNo='', $Keyword='') {
			global $lpf_volunteer, $Lang, $page_size, $ck_ipf_volunteer_event_list_page_size;
			
			$field = ($field=="")? 0 : $field;
			$order = ($order=="")? 1 : $order;
			$pageNo = ($pageNo=="")? 0 : $pageNo;
			
			if (isset($ck_ipf_volunteer_event_list_page_size) && $ck_ipf_volunteer_event_list_page_size != "") {
				$page_size = $ck_ipf_volunteer_event_list_page_size;
			}
			$li = new libdbtable2007($field, $order, $pageNo);
			
			$li->sql = $lpf_volunteer->getEventListTeacherViewSQL($Keyword);
			$li->field_array = array('e.Code', 'e.Title', 'e.StartDate', 'e.Organization', 'e.ModifiedDate', 'iu.LastModifiedName');
			$li->no_col = count($li->field_array) + 2;
			$li->IsColOff = 'IP25_table';
			
			//debug_pr($li->built_sql());
			//debug_pr($li->returnArray($li->built_sql()));
			
			# TABLE COLUMN
			$pos = 0;
			$li->column_list .= "<th width='2%'>#</th>\n";
			$li->column_list .= "<th width='8%'>".$li->column_IP25($pos++, $Lang['General']['Code'])."</th>\n";
			$li->column_list .= "<th width='27%'>".$li->column_IP25($pos++, $Lang['General']['Name'])."</th>\n";
			$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['General']['StartDate'])."</th>\n";
			$li->column_list .= "<th width='25%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['PartnerOrganization'])."</th>\n";
			$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['General']['LastModified'])."</th>\n";
			$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['General']['LastModifiedBy'])."</th>\n";
			$li->column_list .= "<th width='3%'>".$li->check("EventIDArr[]")."</th>\n";
			
			$x = '';
			$x .=  $li->display();
			$x .= '<input type="hidden" id="pageNo" name="pageNo" value="'.$li->pageNo.'" />';
			$x .= '<input type="hidden" id="order" name="order" value="'.$li->order.'" />';
			$x .= '<input type="hidden" id="field" name="field" value="'.$li->field.'" />';
			$x .= '<input type="hidden" id="page_size_change" name="page_size_change" value="" />';
			$x .= '<input type="hidden" id="numPerPage" name="numPerPage" value="'.$li->page_size.'" />';
			
			return $x;
		}
		
		/**
		 * Get the DB Table for the Teacher's view Event List
		 * @owner : Ivan (20110824)
		 * @param : Integer $StudentID, the UserID of the student to be viewed
		 * @return : String, the html of the Table
		 */
		public function getStudentApplicableEventListTable($StudentID) {
			global $linterface, $lpf_volunteer, $Lang;
			
			$StudentAppliedEventInfoArr = $lpf_volunteer->getStudentAppliedEventInfo($StudentID);
			$StudentAppliedEventIDArr = Get_Array_By_Key($StudentAppliedEventInfoArr, 'EventID');
			
			$EventInfoArr = $lpf_volunteer->getEventInfo($ExcludeEventIDArr='');
			$numOfEvent = count($EventInfoArr);
			
			$EventCheckboxAllID = 'EventIDChk_All';
			$EventCheckboxClass = 'EventIDChk';
			
			$x = '';
			$x .= '<table id="ContentTable" class="common_table_list_v30">'."\r\n";
				$x .= '<col style="width:10;">'."\n";
				$x .= '<col style="width:43%;">'."\n";
				$x .= '<col style="width:15%;">'."\n";
				$x .= '<col style="width:27%;">'."\n";
				$x .= '<col style="width:10%;">'."\n";
				$x .= '<col style="width:1;">'."\n";
				
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th>#</th>'."\n";
						$x .= '<th>'.$Lang['General']['Name'].'</th>'."\n";
						$x .= '<th>'.$Lang['General']['StartDate'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['VolunteerArr']['PartnerOrganization'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['VolunteerArr']['Applied'].'</th>'."\n";
						$x .= '<th>'.$linterface->Get_Checkbox($EventCheckboxAllID, $EventCheckboxAllID, '', $isChecked=false, $thisClass='', '', "Check_All_Options_By_Class('".$EventCheckboxClass."', this.checked, 1);");
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";
				
				$x .= '<tbody>'."\n";
					for ($i=0; $i<$numOfEvent; $i++) {
						$thisEventID = $EventInfoArr[$i]['EventID'];
						$thisTitle = $EventInfoArr[$i]['Title'];
						$thisStartDate = $EventInfoArr[$i]['StartDate'];
						$thisOrganization = $EventInfoArr[$i]['Organization'];
						
						$thisApplied = (in_array($thisEventID, (array)$StudentAppliedEventIDArr))? true : false;
						if ($thisApplied) {
							$thisAppliedDisplay = $Lang['General']['Yes'];
							$thisRowCss = 'tabletextremark';
							$thisCheckboxDisable = true;
						}
						else {
							$thisAppliedDisplay = $Lang['General']['No'];
							$thisRowCss = '';
							$thisCheckboxDisable = false;
						}
						
						$thisID = 'EventIDChk_'.$thisEventID;
						$thisName = 'EventIDArr[]';
						$thisCheckbox = $linterface->Get_Checkbox($thisID, $thisName, $thisEventID, $isChecked=false, $EventCheckboxClass, '', "Uncheck_SelectAll('".$EventCheckboxAllID."', this.checked);", $thisCheckboxDisable);
						
						$x .= '<tr class="'.$thisRowCss.'">'."\n";
							$x .= '<td>'.($i+1).'</td>'."\n";
							$x .= '<td>'.$thisTitle.'</td>'."\n";
							$x .= '<td>'.$thisStartDate.'</td>'."\n";
							$x .= '<td>'.$thisOrganization.'</td>'."\n";
							$x .= '<td>'.$thisAppliedDisplay.'</td>'."\n";
							$x .= '<td>'.$thisCheckbox.'</td>'."\n";
						$x .= '</tr>'."\n";
					}
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\r\n";
			
			return $x;
		}
		
		/**
		 * Get the HTML of the Student add / edit Event info Page 
		 * @owner : Ivan (20110830)
		 * @param : Integer $StudentID, the UserID of the Student to be viewed
		 * @param : Integer / Array $EventIDArr, the EventID of the event(s) to be edited
		 * @return : String, the html of the add / edit Page
		 */
		public function getStudentFillInEventDetailsTable($StudentID, $EventIDArr, $RecordIDArr='') {
			global $lpf_volunteer, $linterface, $Lang;
			
			$libfcm_ui = new form_class_manage_ui();
			
			if ($RecordIDArr != '') {
				// Come from index => edit => Re-build EventIDArr from RecordIDArr
				$StudentEventInfoArr = $lpf_volunteer->getStudentAppliedEventInfo($StudentID, $RecordIDArr);
				
				$EventIDArr = Get_Array_By_Key($StudentEventInfoArr, 'EventID');
				$StudentEventAssoArr = BuildMultiKeyAssoc($StudentEventInfoArr, array('UserID', 'EventID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
			}
			
			$numOfEvent = count($EventIDArr);
			$EventInfoArr = $lpf_volunteer->getEventInfo($EventIDArr);
			$EventInfoAssoArr = BuildMultiKeyAssoc($EventInfoArr, 'EventID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
			
			$x = '';
			for ($i=0; $i<$numOfEvent; $i++) {
				$thisEventID = $EventIDArr[$i];
				
				$thisTitle = $EventInfoAssoArr[$thisEventID]['Title'];
				$thisStartDate = $EventInfoAssoArr[$thisEventID]['StartDate'];
				$thisOrganization = $EventInfoAssoArr[$thisEventID]['Organization'];
				
				$thisHours = $StudentEventAssoArr[$StudentID][$thisEventID]['Hours'];
				$thisRole = $StudentEventAssoArr[$StudentID][$thisEventID]['Role'];
				$thisAchievement = $StudentEventAssoArr[$StudentID][$thisEventID]['Achievement'];
				$thisTeacherPIC = $StudentEventAssoArr[$StudentID][$thisEventID]['TeacherPIC'];
				
				$x .= '<div>'."\r\n";
					$x .= $linterface->GET_NAVIGATION2_IP25($thisTitle);
					$x .= '<br />'."\r\n";
					
					$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\r\n";
						$x .= '<col class="field_title" />'."\r\n";
						$x .= '<col class="field_c">'."\r\n";

						// Start Date
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['General']['StartDate'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $thisStartDate;
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						// Organization
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['iPortfolio']['VolunteerArr']['PartnerOrganization'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $thisOrganization;
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						// Hours
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['iPortfolio']['VolunteerArr']['Hours'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $linterface->GET_TEXTBOX_NUMBER('HourTb_'.$thisEventID, 'StudentEventInfoArr['.$thisEventID.'][Hours]', $thisHours, $OtherClass='HourTb', $OtherPar=array());
								$x .= $linterface->Get_Form_Warning_Msg('HourBlankWarningDiv_'.$thisEventID, $Lang['General']['JS_warning']['CannotBeBlank'], $Class='WarningDiv');
								$x .= $linterface->Get_Form_Warning_Msg('HourMustBePositiveWarningDiv_'.$thisEventID, $Lang['General']['JS_warning']['MustBePositiveNumber'], $Class='WarningDiv');
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						// Role
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['iPortfolio']['VolunteerArr']['Role'].'</td>'."\n";
							$x .= '<td>'."\n";
								$OtherParArr = array();
								$OtherParArr['onkeypress'] = 'limitText(this, \''.$lpf_volunteer->getEventUserRoleMaxLength().'\')';
								$x .= $linterface->GET_TEXTBOX('RoleTb_'.$thisEventID, 'StudentEventInfoArr['.$thisEventID.'][Role]', $thisRole, $OtherClass='RoleTb', $OtherParArr);
								$x .= $linterface->Get_Form_Warning_Msg('RoleBlankWarningDiv_'.$thisEventID, $Lang['General']['JS_warning']['CannotBeBlank'], $Class='WarningDiv');
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						// Awards / Certifications / Achievements
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['iPortfolio']['VolunteerArr']['Awards/Certifications/Achievements'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $linterface->GET_TEXTAREA('StudentEventInfoArr['.$thisEventID.'][Achievement]', $thisAchievement, $taCols=70, $taRows=5, $OnFocus="", $readonly="0", $other='', $class='AchievementTa', 'AchievementTa_'.$thisEventID);
								$x .= $linterface->Get_Form_Warning_Msg('AchievementBlankWarningDiv_'.$thisEventID, $Lang['General']['JS_warning']['CannotBeBlank'], $Class='WarningDiv');
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						// Teacher PIC
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['iPortfolio']['VolunteerArr']['TeacherPIC'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $libfcm_ui->Get_Staff_Selection('StudentEventInfoArr['.$thisEventID.'][TeacherPIC]', $isTeachingStaff=1, $thisTeacherPIC, $Onchange='', $noFirst=0, $isMultiple=0, $isAll=0, 'TeacherPICSel_'.$thisEventID, 'TeacherPICSel');
								$x .= $linterface->Get_Form_Warning_Msg('TeacherPICBlankWarningDiv_'.$thisEventID, $Lang['General']['JS_warning']['CannotBeBlank'], $Class='WarningDiv');
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\r\n";
				$x .= '</div>'."\r\n";
				
				if ($i < $numOfEvent - 1) {
					$x .= '<div class="edit_bottom_v30">'."\r\n";
					$x .= '</div>'."\r\n";
				}
				else {
					$x .= '<br /><br /><br />'."\r\n";
				}
			}
			
			return $x;
		}
		
		/**
		 * Get the DB Table for the Student's view Applied Event List
		 * @owner : Ivan (20110830)
		 * @param : Integer $StudentID, the UserID of the Student to be viewed
		 * @param : Integer $field, the sorting field of the DB Table
		 * @param : Integer $order, the sorting order of the DB Table
		 * @param : Integer $pageNo, the current page number of the DB Table
		 * @param : String $Keyword, the seraching keyword 
		 * @return : String, the html of the DB Table
		 */
		public function getAppliedEventListStudentViewDBTable($StudentID, $field='', $order='', $pageNo='', $Keyword='') {
			global $lpf_volunteer, $Lang, $page_size, $ck_ipf_volunteer_student_applied_event_list_page_size;
			
			$field = ($field=="")? 0 : $field;
			$order = ($order=="")? 1 : $order;
			$pageNo = ($pageNo=="")? 0 : $pageNo;
			
			if (isset($ck_ipf_volunteer_student_applied_event_list_page_size) && $ck_ipf_volunteer_student_applied_event_list_page_size != "") {
				$page_size = $ck_ipf_volunteer_student_applied_event_list_page_size;
			}
			$li = new libdbtable2007($field, $order, $pageNo);
			
			$li->sql = $lpf_volunteer->getAppliedEventListStudentViewSQL($StudentID, $Keyword);
			$li->field_array = array('e.Title', 'e.StartDate', 'e.Organization', 'eu.Hours', 'eu.Role', 'eu.Achievement', 'TeacherPICName', 'ApproveStatusIcon', 'eu.ModifiedDate');
			$li->column_array = array(0, 0, 0, 0, 0, 18, 0, 0, 0);
			$li->no_col = count($li->field_array) + 2;
			$li->IsColOff = 'IP25_table';
			
			//debug_pr($li->built_sql());
			//debug_pr($li->returnArray($li->built_sql()));
			
			# TABLE COLUMN
			$pos = 0;
			$li->column_list .= "<th width='2%'>#</th>\n";
			$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['General']['Name'])."</th>\n";
			$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['General']['StartDate'])."</th>\n";
			$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['PartnerOrganization'])."</th>\n";
			$li->column_list .= "<th width='5%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['Hours'])."</th>\n";
			$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['Role'])."</th>\n";
			$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['Awards/Certifications/Achievements'])."</th>\n";
			$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['TeacherPIC'])."</th>\n";
			$li->column_list .= "<th width='5%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['Status'])."</th>\n";
			$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['General']['LastModified'])."</th>\n";
			$li->column_list .= "<th width='3%'>".$li->check("RecordIDArr[]")."</th>\n";
			
			$x = '';
			$x .=  $li->display();
			$x .= '<input type="hidden" id="pageNo" name="pageNo" value="'.$li->pageNo.'" />';
			$x .= '<input type="hidden" id="order" name="order" value="'.$li->order.'" />';
			$x .= '<input type="hidden" id="field" name="field" value="'.$li->field.'" />';
			$x .= '<input type="hidden" id="page_size_change" name="page_size_change" value="" />';
			$x .= '<input type="hidden" id="numPerPage" name="numPerPage" value="'.$li->page_size.'" />';
			
			return $x;
		}
		
		/**
		 * Get the DB Table for the Teacher's view Event Approval List
		 * @owner : Ivan (20110831)
		 * @param : Integer $field, the sorting field of the DB Table
		 * @param : Integer $order, the sorting order of the DB Table
		 * @param : Integer $pageNo, the current page number of the DB Table
		 * @param : String $Keyword, the seraching keyword 
		 * @return : String, the html of the DB Table
		 */
		public function getEventApprovalListTeacherViewDBTable($field, $order, $pageNo, $Keyword, $ApproveStatus, $ProgramType) {
			global $lpf_volunteer, $Lang, $page_size, $ck_ipf_volunteer_event_approval_list_page_size;
			
			$field = ($field=="")? 0 : $field;
			$order = ($order=="")? 1 : $order;
			$pageNo = ($pageNo=="")? 0 : $pageNo;
			
			if (isset($ck_ipf_volunteer_event_approval_list_page_size) && $ck_ipf_volunteer_event_approval_list_page_size != "") {
				$page_size = $ck_ipf_volunteer_event_approval_list_page_size;
			}
			$li = new libdbtable2007($field, $order, $pageNo);
			
			$li->sql = $lpf_volunteer->getEventApprovalListTeacherViewSQL($Keyword, $ApproveStatus, $ProgramType);
			$li->field_array = array('iu_student.ClassName', 'iu_student.ClassNumber', 'StudentName', 'e.Title', 'e.StartDate', 'e.Organization', 'eu.Hours', 'eu.Role', 'eu.Achievement', 'TeacherPICName');
			$li->no_col = count($li->field_array) + 3;
			$li->IsColOff = 'IP25_table';
			$li->fieldorder2 = " , iu_student.ClassNumber";
			$li->column_array = array(0, 0, 0, 0, 0, 0, 0, 0, 18, 0);
			
			//debug_pr($li->built_sql());
			//debug_pr($li->returnArray($li->built_sql()));
			
			# TABLE COLUMN
			$pos = 0;
			$li->column_list .= "<th width='2%'>#</th>\n";
			$li->column_list .= "<th width='6%'>".$li->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['Class'])."</th>\n";
			$li->column_list .= "<th width='5%'>".$li->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['ClassNo'])."</th>\n";
			$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['SysMgr']['FormClassMapping']['StudentName'])."</th>\n";
			$li->column_list .= "<th width='14%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['EventName'])."</th>\n";
			$li->column_list .= "<th width='8%'>".$li->column_IP25($pos++, $Lang['General']['StartDate'])."</th>\n";
			$li->column_list .= "<th width='14%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['PartnerOrganization'])."</th>\n";
			$li->column_list .= "<th width='5%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['Hours'])."</th>\n";
			$li->column_list .= "<th width='7%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['Role'])."</th>\n";
			$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['Awards/Certifications/Achievements'])."</th>\n";
			$li->column_list .= "<th width='8%'>".$li->column_IP25($pos++, $Lang['iPortfolio']['VolunteerArr']['TeacherPIC'])."</th>\n";
			$li->column_list .= "<th width='3%'>".$Lang['iPortfolio']['VolunteerArr']['Status']."</th>\n";
			$li->column_list .= "<th width='3%'>".$li->check("RecordIDArr[]")."</th>\n";
			
			$x = '';
			$x .=  $li->display();
			$x .= '<input type="hidden" id="pageNo" name="pageNo" value="'.$li->pageNo.'" />';
			$x .= '<input type="hidden" id="order" name="order" value="'.$li->order.'" />';
			$x .= '<input type="hidden" id="field" name="field" value="'.$li->field.'" />';
			$x .= '<input type="hidden" id="page_size_change" name="page_size_change" value="" />';
			$x .= '<input type="hidden" id="numPerPage" name="numPerPage" value="'.$li->page_size.'" />';
			
			return $x;
		}
		
		public function getEventUserApprovalStatusSelection($ID_Name, $SelectedStatus='', $OnChange='') {
			global $lpf_volunteer, $Lang;
		
			$OptionArr = array();
			$OptionArr[$lpf_volunteer->getEventUserApproveStatusCode_Pending()] = $Lang['General']['Pending'];
			$OptionArr[$lpf_volunteer->getEventUserApproveStatusCode_Approved()] = $Lang['General']['Approved'];
			$OptionArr[$lpf_volunteer->getEventUserApproveStatusCode_Rejected()] = $Lang['General']['Rejected'];
			
			$onchange = "";
			if ($OnChange != "") {
				$onchange = ' onchange="'.$OnChange.'" ';
			}
				
			$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange;
			$selection = getSelectByAssoArray($OptionArr, $selectionTags, $SelectedStatus, $isAll=1, $noFirst=0, Get_Selection_First_Title($Lang['iPortfolio']['VolunteerArr']['AllStatus']));
		
			return $selection;	
		}
		
		public function getEventUserProgramTypeSelection($ID_Name, $SelectedStatus='', $OnChange='') {
			global $lpf_volunteer, $Lang;
		
			$OptionArr = array();
			$OptionArr['MyProgram'] = $Lang['iPortfolio']['VolunteerArr']['MyPICProgram'];
			
			$onchange = "";
			if ($OnChange != "") {
				$onchange = ' onchange="'.$OnChange.'" ';
			}
				
			$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange;
			$selection = getSelectByAssoArray($OptionArr, $selectionTags, $SelectedStatus, $isAll=1, $noFirst=0, Get_Selection_First_Title($Lang['iPortfolio']['VolunteerArr']['AllProgram']));
		
			return $selection;	
		}
	}
}
?>