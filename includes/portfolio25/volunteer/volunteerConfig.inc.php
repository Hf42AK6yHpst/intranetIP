<?
# modifying :  

//THIS CONFIG IS AVALIABLE FOR ALL CLIENT ENVIRONMENT
##	-	$varname["DB_[TABLE_NAME]_[FIELD_NAME]"]["MEANING"]

// TABLE : SELF_ACCOUNT_STUDENT
// FIELD : COMEFROM
//$ipf_cfg["Volunteer"]["OLE_STUDENT_COMEFROM"]["teacherInput"] = 1;  // TEACHER INPUT FORM THE UI


### VOLUNTEER_EVENT
$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["Code"]["MaxLength"] = 255;
$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["Title"]["MaxLength"] = 255;
$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["Organization"]["MaxLength"] = 200;

$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["RemoveStatus"]["Active"] = 'N';
$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["RemoveStatus"]["Deleted"] = 'Y';

$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["CreatorType"]["Teacher"] = 'T';
$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["CreatorType"]["Student"] = 'S';
$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT"]["CreatorType"]["Parent"] = 'P';


### VOLUNTEER_EVENT_USER
$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["Role"]["MaxLength"] = 64;

$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["RemoveStatus"]["Active"] = 'N';
$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["RemoveStatus"]["Deleted"] = 'Y';

$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["ApproveStatus"]["Pending"] = 1;
$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["ApproveStatus"]["Approved"] = 2;
$ipf_cfg["Volunteer"]["VOLUNTEER_EVENT_USER"]["ApproveStatus"]["Rejected"] = 3;
?>