<?php
/*
####################################################
#	Date	2017-03-29 Omas modified GET_SUBJECT_TEACHER_CLASS()
####################################################
*/
include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/portfolio25/libpf-account.php");

class libpf_account_teacher extends libpf_account {

  function __construct(){
    parent::__construct();
  }
  
  function IS_PORTFOLIO_USER(){
    global $eclass_db;
    
    $user_id = $this->GET_CLASS_VARIABLE("user_id");
    $sql =  "
              SELECT
                count(*)
              FROM
                {$eclass_db}.PORTFOLIO_TEACHER
              WHERE
                UserID = '".$user_id."'
            ";
    $returnVal = $this->db->returnVector($sql);
  
    return ($returnVal[0] == 1);
  }

  function ADD_PORTFOLIO_TEACHER(){
    global $eclass_db;
    
    $user_id = $this->GET_CLASS_VARIABLE("user_id");
    $course_user_id = $this->GET_CLASS_VARIABLE("course_user_id");
    
    $sql =  "
              INSERT INTO
                {$eclass_db}.PORTFOLIO_TEACHER
                  (UserID, CourseUserID, IsSuspend, IsAdmin, InputDate, ModifiedDate)
              VALUES
                ('".$user_id."', '".$course_user_id."', 0, 0, NOW(), NOW())
            ";
    $this->db->db_db_query($sql);
  }
  
  function DELETE_PORTFOLIO_TEACHER(){
    global $eclass_db;
    
    $user_id = $this->GET_CLASS_VARIABLE("user_id");
    $course_user_id = $this->GET_CLASS_VARIABLE("course_user_id");
    
    $sql =  "
              DELETE FROM
                {$eclass_db}.PORTFOLIO_TEACHER
              WHERE
                UserID = '".$user_id."' OR
                CourseUserID = '".$course_user_id."'
            ";
    $this->db->db_db_query($sql);
  }
  
  function GET_CLASS_TEACHER_CLASS(){
    global $intranet_db;
  
    $user_id = $this->GET_CLASS_VARIABLE("user_id");
    $sql =  "
              SELECT
                yc.YearClassID
              FROM
                {$intranet_db}.YEAR_CLASS_TEACHER AS yct
              INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
                ON yc.YearClassID = yct.YearClassID
              WHERE
                yct.UserID = '".$user_id."' AND
                yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
            ";
    $returnArr = $this->db->returnVector($sql);
    
    return $returnArr;
  }
  
  function GET_CLASS_TEACHER_CLASSLEVEL(){
    global $intranet_db;
  
    $user_id = $this->GET_CLASS_VARIABLE("user_id");
    $sql =  "
              SELECT
                yc.YearID
              FROM
                {$intranet_db}.YEAR_CLASS_TEACHER AS yct
              INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
                ON yc.YearClassID = yct.YearClassID
              WHERE
                yct.UserID = '".$user_id."' AND
                yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
            ";
    $returnArr = $this->db->returnVector($sql);
    
    return $returnArr;
  }
  
  function GET_SUBJECT_TEACHER_CLASS(){
    global $intranet_db;
  
    $user_id = $this->GET_CLASS_VARIABLE("user_id");
    $sql =  "
              SELECT
                yc.YearClassID
              FROM
                {$intranet_db}.SUBJECT_TERM_CLASS_TEACHER AS stct
              INNER JOIN
                {$intranet_db}.SUBJECT_TERM_CLASS_USER AS stcu
              	ON stct.SubjectGroupID = stcu.SubjectGroupID
              INNER JOIN
                {$intranet_db}.YEAR_CLASS_USER AS ycu
              	ON stcu.UserID = ycu.UserID
              INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
                ON yc.YearClassID = ycu.YearClassID
              INNER JOIN {$intranet_db}.SUBJECT_TERM as st
				ON stct.SubjectGroupID = st.SubjectGroupID
			  INNER JOIN {$intranet_db}.ACADEMIC_YEAR_TERM as ayt
				ON ayt.YearTermID = st.YearTermID 
              WHERE
                stct.UserID = '".$user_id."' 
                AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
                AND ayt.AcademicYearID  = '".Get_Current_Academic_Year_ID()."'
            ";
    $returnArr = $this->db->returnVector($sql);
    
    return $returnArr;
  }


}