<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') . " Create new PHPExcel object\n";
$objPHPExcel = new PHPExcel();

// Set properties
echo date('H:i:s') . " Set properties\n";
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


// Create a first sheet, representing sales data
echo date('H:i:s') . " Add some data\n";
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Registration Number');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'School Year');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Duration');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'STA Code');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'STA Type');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'STA Post Code');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'STA Performance Code');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Report Card Readable Indicator');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Major Components of Other Learning Experiences Code');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Awards / Certifications / Achievements Code');
$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Awards / Certifications /Achievements (by text 1) in English');
$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Awards / Certifications /Achievements (by text 1) in Chinese');
$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Awards / Certifications /Achievements (by text 2) in English');
$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Awards / Certifications /Achievements (by text 2) in Chinese');
$objPHPExcel->getActiveSheet()->setCellValue('O1', 'Awards / Certifications /Achievements (by text 3) in English');
$objPHPExcel->getActiveSheet()->setCellValue('P1', 'Awards / Certifications /Achievements (by text 3) in Chinese');
$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Awards / Certifications /Achievements (by text 4) in English');
$objPHPExcel->getActiveSheet()->setCellValue('R1', 'Awards / Certifications /Achievements (by text 4) in Chinese');

//$objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
//$objPHPExcel->getActiveSheet()->getStyle('F2')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);

//$objPHPExcel->getActiveSheet()->getStyle('A2')->getNumberFormat()->setFormatCode('@');
$objPHPExcel->getActiveSheet()->getCell('A2')->setValueExplicit('0710015', PHPExcel_Cell_DataType::TYPE_STRING);
//$objPHPExcel->getActiveSheet()->setCellValue('A2', '0710015');

//$objPHPExcel->getActiveSheet()->getStyle('A2')->getNumberFormat()->setFormatCode('0000000');
//$objPHPExcel->getActiveSheet()->setCellValue('B2', '2011');
$objPHPExcel->getActiveSheet()->getCell('B2')->setValueExplicit('2011', PHPExcel_Cell_DataType::TYPE_STRING);
//$objPHPExcel->getActiveSheet()->setCellValue('C2', '1');
$objPHPExcel->getActiveSheet()->getCell('C2')->setValueExplicit('1', PHPExcel_Cell_DataType::TYPE_STRING);
//$objPHPExcel->getActiveSheet()->setCellValue('D2', '5113');
$objPHPExcel->getActiveSheet()->getCell('D2')->setValueExplicit('5113', PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->setCellValue('E2', 'E');
$objPHPExcel->getActiveSheet()->getCell('F2')->setValueExplicit('0064', PHPExcel_Cell_DataType::TYPE_STRING);
//$objPHPExcel->getActiveSheet()->setCellValue('F2', '0064');
/*$objPHPExcel->getActiveSheet()->getStyle('B2')->getNumberFormat()->setFormatCode('0000');
$objPHPExcel->getActiveSheet()->getStyle('C2')->getNumberFormat()->setFormatCode('0');
$objPHPExcel->getActiveSheet()->getStyle('D2')->getNumberFormat()->setFormatCode('0000');
$objPHPExcel->getActiveSheet()->getStyle('F2')->getNumberFormat()->setFormatCode('0000');
*/
//$objPHPExcel->getActiveSheet()->setCellValue('G2', 'STA Performance Code');
$objPHPExcel->getActiveSheet()->setCellValue('H2', 'Y');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
