<?php
if (!defined("LIBWEBSAMSATTENDCODE_DEFINED"))                     // Preprocessor directive
{
define("LIBWEBSAMSATTENDCODE_DEFINED", true);

class libwebsamsattendcode extends libdb{

      var $SchoolID;
      var $SchoolYear;
      var $SchoolLevel;
      var $SchoolSession;
      var $ReasonCodePool;

      function libwebsamsattendcode ()
      {
               $this->libdb();
      }

      function retrieveBasicInfo ()
      {
               $sql = "SELECT SchoolID, SchoolYear, SchoolLevel, SchoolSession
                              FROM INTRANET_WEBSAMS_ATTENDANCE_BASIC
                              WHERE RecordID = 0";
               $temp = $this->returnArray($sql,4);
               list($this->SchoolID, $this->SchoolYear,
                    $this->SchoolLevel, $this->SchoolSession) = $temp[0];
      }

}


}        // End of directive
?>