<?php
// Using by 

#########################################
#   2019-11-05 Cameron: fix $month in getEverydayTimetableByMonth()
#   2019-07-18 Bill: modified getCacheResult(), setCacheResult(), isCacheResultExist(), to remove ';' to prevent eval() security problem
#   2019-06-25 Cameron: add parameter $startDate and $endDate to getSkipSchoolEventDateByYear()
#   2019-06-04 Cameron: add function getEverydayTimetableByPeriod(), getAllTimeslotByTimetableID()
#   2019-05-21 Cameron: add function getEverydayTimetableByMonth(), getSkipSchoolEventDateByYear()
#	2017-03-02 Carlos: Modified Copy_Timetable(), copy time slot SessionType.
#	2017-02-09 Add Get_NextDate_byCycleDay()
#	2015-12-20 Kenneth: Add Get_Subject_Group_Info()
#########################################

include_once('lib.php');
include_once('libdb.php');

## Include subject_class_mapping_ui.php at the top due to subject_class_mapping.php included libeclass40.php
## If included in the function, system cannot get the value of some global variables like $eclass_version
include_once("subject_class_mapping_ui.php");

if (!defined("LIBTIMETABLE_DEFINED"))         // Preprocessor directives
{
	define("LIBTIMETABLE_DEFINED", true);
	
	class Timetable extends libdb {
		var $TableName;
		var $ID_FieldName;
		var $ObjectID;
		var $PreloadInfoArr;
		
		function Timetable($TimetableID=""){
			parent:: libdb();
			
			$this->TableName = 'INTRANET_SCHOOL_TIMETABLE';
			$this->ID_FieldName = 'TimetableID';
			$this->PreloadInfoArr = array();
			
			if ($TimetableID!="")
			{
				$this->ObjectID = $TimetableID;
				$this->Get_Self_Info();
			}
		}
		
		function Get_Self_Info()
		{
			$sql = "";
			$sql .= " SELECT * FROM ".$this->TableName;
			$sql .= " WHERE ".$this->ID_FieldName." = '".$this->ObjectID."'";
			$resultSet = $this->returnArray($sql);
			$infoArr = $resultSet[0];
			
			if (count($infoArr) > 0) {
				foreach ($infoArr as $key => $value)
					$this->{$key} = $value;
			}
		}
		
		function Get_Timetable_Name()
		{
			return $this->TimetableName;
		}
		
		function Get_Subject_Group_Info($yearTermID){
			$sql = "SELECT ClassCode, ClassTitleEN, ClassTitleB5
					FROM SUBJECT_TERM_CLASS AS stc 
					INNER JOIN SUBJECT_TERM AS st ON stc.SubjectGroupID = st.SubjectGroupID
					WHERE YearTermID = ".$yearTermID.";";
			return $this->returnResultSet($sql);
		}
		
		function Get_All_TimeTable($AcademicYearID='', $YearTermID='', $AllocatedOnly=0, $ReleaseStatus='')
		{
			$conds_AcademicYearID = '';
			if ($AcademicYearID != '') {
				$conds_AcademicYearID = " AND timetable.AcademicYearID = '$AcademicYearID' ";
			}
				
			$conds_YearTerm = '';
			if ($YearTermID != '') {
				$conds_YearTerm = " AND timetable.YearTermID = '$YearTermID' ";
			}
				
			if ($AllocatedOnly) {
				$join_table = "INNER JOIN
								INTRANET_TIMETABLE_ROOM_ALLOCATION as allocationTable ON (timetable.TimetableID = allocationTable.TimetableID)";
			}
			
			if ($ReleaseStatus !== '') {
				$conds_releaseStatus = " And timetable.ReleaseStatus IN ('".implode("','", (array)$ReleaseStatus)."') ";
			}
				
			$sql = " 	SELECT 
								timetable.*
						FROM 
								".$this->TableName." as timetable
								Inner Join ACADEMIC_YEAR_TERM as ayt On (timetable.YearTermID = ayt.YearTermID)
								$join_table
						WHERE 
								1
								$conds_AcademicYearID
								$conds_YearTerm
								$conds_releaseStatus
						Order By
								ayt.TermStart, timetable.TimetableName
					";
			return $this->returnArray($sql);
		}
		
		function Insert_Record($DataArr=array())
		{
			if (count($DataArr) == 0)
				return false;
			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
			}
			
			## set others fields
			# DateInput
			$fieldArr[] = 'DateInput';
			$valueArr[] = 'now()';
			# DateModified
			$fieldArr[] = 'DateModified';
			$valueArr[] = 'now()';
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$sql = '';
			$sql .= ' INSERT INTO '.$this->TableName;
			$sql .= ' ( '.$fieldText.' ) ';
			$sql .= ' VALUES ';
			$sql .= ' ( '.$valueText.' ) ';
			
			$success = $this->db_db_query($sql);
			if ($success == false)
			{
				$this->RollBack_Trans();
				return 0;
			}
			else
			{
				$this->Commit_Trans();
				
				$insertedID = $this->Get_Max_ObjectID();
				
				return $insertedID;
			}
		}
		
		function Get_Max_ObjectID()
		{
			# return the inserted TimetableID
			$sql = "SELECT MAX(".$this->ID_FieldName.") FROM ".$this->TableName;
			$resultSet = $this->returnVector($sql);
			$insertedID = $resultSet[0];
			
			return $insertedID;
		}
		
		function Update_Record($DataArr=array())
		{
			if (count($DataArr) == 0)
				return false;
			
			# Build field update values string
			$valueFieldText = '';
			foreach ($DataArr as $field => $value)
			{
				$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
			}
			$valueFieldText .= ' DateModified = now() ';
			
			$sql = '';
			$sql .= ' UPDATE '.$this->TableName;
			$sql .= ' SET '.$valueFieldText;
			$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$this->ObjectID.'\' ';
			$success = $this->db_db_query($sql);
			
			return $success;
		}
		
		function Delete_Record()
		{
			$sql = "DELETE FROM ".$this->TableName." WHERE ".$this->ID_FieldName." = '".$this->ObjectID."' ";
			$success = $this->db_db_query($sql);
			
			return $success;
		}
		
		function Get_TimeSlot_List($ReturnAsso=0)
		{
			$sql = "SELECT
							itt.TimeSlotID, 
							itt.TimeSlotName, 
							itt.StartTime, 
							itt.EndTime,
							itt.DisplayOrder,
							itt.SessionType
					FROM
							INTRANET_TIMETABLE_TIMESLOT as itt
							INNER JOIN
							INTRANET_TIMETABLE_TIMESLOT_RELATION as ittr ON (itt.TimeSlotID = ittr.TimeSlotID)
					WHERE
							ittr.TimetableID = '".$this->ObjectID."'
					ORDER BY
							itt.StartTime
					";
			$TimeSlotInfoArr = $this->returnArray($sql);
			
			$ReturnArr = array();
			if ($ReturnAsso == 1)
			{
				$numOfTimeSlot = count($TimeSlotInfoArr);
				for ($i=0; $i<$numOfTimeSlot; $i++)
				{
					$thisTimeSlotID = $TimeSlotInfoArr[$i]['TimeSlotID'];
					$ReturnArr[$thisTimeSlotID] = $TimeSlotInfoArr[$i];
				}
			}
			else
			{
				$ReturnArr = $TimeSlotInfoArr;
			}
			
			return $ReturnArr;
		}
		
		# LocationID can be an integer LocationID or an array of LocationID
		function Get_Room_Allocation_List($TimeSlotID='', $Day='', $SubjectGroupIDArr=array(), $LocationID='', $OthersLocation='', $DayAsFirstKey=0, $CheckTemp=0, $returnList=false, $RelatedRoomAllocationIDArr='', $returnKeyOnly=false)
		{
			if ($CheckTemp==1) {
				$table = 'INTRANET_TIMETABLE_ROOM_ALLOCATION_TEMP';
				$cond_ImportUserID = " And itra.ImportUserID = '".$_SESSION['UserID']."' ";
				$RelatedRoomAllocationIDField = '';
			}
			else {
				$table = 'INTRANET_TIMETABLE_ROOM_ALLOCATION';
				$cond_ImportUserID = '';
				$RelatedRoomAllocationIDField = ', RelatedRoomAllocationID';
			}
			
			# Subject Group Filtering
			$cond_subjectGroup = "";
			if (count($SubjectGroupIDArr) > 0) {
				$SubjectGroupIDList = implode(", ", $SubjectGroupIDArr);
				$cond_subjectGroup = " AND itra.SubjectGroupID IN ($SubjectGroupIDList) ";
			}
			
			# Location Filtering
			$cond_locationID = "";
			if ($LocationID != "") {
				if (is_array($LocationID)) {
					if (count($LocationID) > 0) {
						$LocationIDList = implode(", ", $LocationID);
						$cond_locationID = " AND itra.LocationID IN ($LocationIDList) ";
					}
				}
				else {
					$cond_locationID = " AND itra.LocationID = '".$LocationID."' ";
				}
			}
			
			# Other Location Filtering
			$cond_OthersLocation = "";
			if ($OthersLocation != "") {
				$cond_OthersLocation = " AND itra.OthersLocation = '".$this->Get_Safe_Sql_Query($OthersLocation)."' ";
			}
				
			# Time Slot Filtering
			$cond_timeslotID = "";
			if ($TimeSlotID != "")
				$cond_timeslotID = " AND itra.TimeSlotID = '".$TimeSlotID."' ";
			
			# Day Filtering
			$cond_day = "";
			if ($Day != "")
				$cond_day = " AND itra.Day = '".$Day."' ";
				
			if ($RelatedRoomAllocationIDArr != '') {
				$cond_relatedRoomAllocationId = " And RelatedRoomAllocationID In ('".implode("','", array_remove_empty((array)$RelatedRoomAllocationIDArr))."') ";
			}
			
//			$sql = "SELECT
//							itra.RoomAllocationID, itra.TimetableID, itra.TimeSlotID, itra.Day, itra.LocationID, itra.OthersLocation, itra.SubjectGroupID
//					FROM
//							$table as itra
//							Inner Join INTRANET_TIMETABLE_TIMESLOT as itt On (itra.TimeSlotID = itt.TimeSlotID)
//					WHERE
//							itra.TimetableID = '".$this->ObjectID."'
//							$cond_subjectGroup
//							$cond_locationID
//							$cond_OthersLocation
//							$cond_timeslotID
//							$cond_day
//					Order By
//							itra.Day, itt.StartTime
//					";

			$subjectGroupOrderField = Get_Lang_Selection('stc.ClassTitleEN', 'stc.ClassTitleB5');
			$sql = "SELECT
							itra.RoomAllocationID, itra.TimetableID, itra.TimeSlotID, itra.Day, itra.LocationID, itra.OthersLocation, itra.SubjectGroupID $RelatedRoomAllocationIDField
					FROM
							$table as itra
							Inner Join INTRANET_TIMETABLE_TIMESLOT as itt On (itra.TimeSlotID = itt.TimeSlotID)
							Inner Join INTRANET_SCHOOL_TIMETABLE as ist On (itra.TimetableID = ist.TimetableID)
							Inner Join SUBJECT_TERM as st On (ist.YearTermID = st.YearTermID And itra.SubjectGroupID = st.SubjectGroupID)
							Inner Join SUBJECT_TERM_CLASS as stc On (st.SubjectGroupID = stc.SubjectGroupID)
							Inner Join ASSESSMENT_SUBJECT as subj On (st.SubjectID = subj.RecordID)
							Inner Join LEARNING_CATEGORY as lc On (subj.LearningCategoryID = lc.LearningCategoryID)
					WHERE
							itra.TimetableID = '".$this->ObjectID."'
							And subj.RecordStatus = 1
							$cond_subjectGroup
							$cond_locationID
							$cond_OthersLocation
							$cond_timeslotID
							$cond_day
							$cond_relatedRoomAllocationId
							$cond_ImportUserID
					Order By
							itra.Day, itt.StartTime, lc.DisplayOrder, subj.DisplayOrder, $subjectGroupOrderField
					";
			$RoomAllocationArr = ($returnKeyOnly)? $this->returnResultSet($sql) : $this->returnArray($sql);
			
			if ($returnList) {
				return $RoomAllocationArr;
			}
			else {
				$numOfRoomAllocation = count($RoomAllocationArr);
				$ReturnArr = array();
				# $ReturnArr[$TimeSlotID][$Day][0,1,2...] = RoomAllocationInfoArr;
				for ($i=0; $i<$numOfRoomAllocation; $i++) {
					$thisTimeSlotID = $RoomAllocationArr[$i]['TimeSlotID'];
					$thisDay = $RoomAllocationArr[$i]['Day'];
					
					if ($DayAsFirstKey == 0)
						$ReturnArr[$thisTimeSlotID][$thisDay][] = $RoomAllocationArr[$i];
					else
						$ReturnArr[$thisDay][$thisTimeSlotID][] = $RoomAllocationArr[$i];
				}
				
				return $ReturnArr;
			}
		}
		
		function Delete_Room_Allocation_Record($TimeSlotID='', $Day='', $ReArrangeDay=1, $SubjectGroupIDArr='', $LocationIDArr='', $OthersLocation='')
		{
//			# Time Slot Filtering
//			$cond_timeslotID = "";
//			if ($TimeSlotID != "")
//				$cond_timeslotID = " AND TimeSlotID = '".$TimeSlotID."' ";
//			
//			# Day Filtering
//			$cond_day = "";
//			if ($Day != "")
//				$cond_day = " AND Day = '".$Day."' ";
//				
//			$cond_SubjectGroupID = '';
//			if ($SubjectGroupIDArr != '' && count($SubjectGroupIDArr) > 0)
//				$cond_SubjectGroupID = " And SubjectGroupID In (".implode(',', $SubjectGroupIDArr).") ";
//				
//			$cond_LocationID = '';
//			if ($LocationIDArr != '' && count($LocationIDArr) > 0)
//				$cond_LocationID = " And LocationID In (".implode(',', $LocationIDArr).") ";
//				
//			$cond_OthersLocation = "";
//			if ($OthersLocation != "")
//				$cond_OthersLocation = " And OthersLocation = '".$this->Get_Safe_Sql_Query($OthersLocation)."' ";
//				
//			# Get all Room Allocations
//			$sql = "SELECT
//							RoomAllocationID, RelatedRoomAllocationID
//					FROM
//							INTRANET_TIMETABLE_ROOM_ALLOCATION
//					WHERE
//							".$this->ID_FieldName." = '".$this->ObjectID."'
//							$cond_timeslotID
//							$cond_day
//							$cond_SubjectGroupID
//							$cond_LocationID
//							$OthersLocation
//					";
//			$RoomAllocationArr = $this->returnResultSet($sql);
			$RoomAllocationArr = $this->Get_Room_Allocation_List($TimeSlotID, $Day, $SubjectGroupIDArr, $LocationIDArr, $OthersLocation, $DayAsFirstKey=0, $CheckTemp=0, $returnList=true);
			
			if (count($RoomAllocationArr) > 0) {
				# Delete all Room Allocations
				$RoomAllocationIdList = implode(",", Get_Array_By_Key($RoomAllocationArr, 'RoomAllocationID'));
				
				$RelatedRoomAllocationIdList = implode(",", array_remove_empty(Get_Array_By_Key($RoomAllocationArr, 'RelatedRoomAllocationID')));
				if ($RelatedRoomAllocationIdList != '') {
					$cond_RelatedRoomAllocationId = " Or RelatedRoomAllocationID IN ($RelatedRoomAllocationIdList) ";
				}
				
				$sql_delete = "DELETE FROM INTRANET_TIMETABLE_ROOM_ALLOCATION
								WHERE (RelatedRoomAllocationID is null And RoomAllocationID IN ($RoomAllocationIdList)) $cond_RelatedRoomAllocationId";
				$success['Delete_RoomAllocation'] = $this->db_db_query($sql_delete);
				
				# Re-arrange Room Allocation Day if delete cycle day
				# e.g. Delete Day3 => "Day4->Day3", "Day5->Day4"...
				if ($ReArrangeDay == 1 && $Day != '') {
					$sql_update = "UPDATE INTRANET_TIMETABLE_ROOM_ALLOCATION SET Day = Day-1 WHERE Day > ".$Day;
					$success['Update_RoomAllocation_Day'] = $this->db_db_query($sql_update);
				}
				
				if (in_array(false, $success))
					return 0;
				else
					return 1;
			}
			else
			{
				# no room allocation set up yet
				return 1;
			}
		}
		
		function Delete_All_Time_Slot()
		{
			# Get All Time Slots of the timetable
			$sql = "SELECT TimeSlotID FROM INTRANET_TIMETABLE_TIMESLOT_RELATION WHERE ".$this->ID_FieldName." = '".$this->ObjectID."' ";
			$TimeSlotIDArr = $this->returnVector($sql);
			$numOfTimeSlotID = count($TimeSlotIDArr);
			
			if ($numOfTimeSlotID == 0)
				return 1;
				
			$TimeSlotIDList = implode(", ", $TimeSlotIDArr);
			
			# Delete TimeSlot Record
			$sql_record = "DELETE FROM INTRANET_TIMETABLE_TIMESLOT WHERE TimeSlotID IN ($TimeSlotIDList) ";
			$success['Record'] = $this->db_db_query($sql_record);
			
			# Delete Timetable-TimeSlot Relation
			$sql_relation = "DELETE FROM INTRANET_TIMETABLE_TIMESLOT_RELATION WHERE ".$this->ID_FieldName." = '".$this->ObjectID."' ";
			$success['Relation'] = $this->db_db_query($sql_relation);
			
			if (in_array(0, $success))
				return 0;
			else
				return 1;
		}
		
		function Is_Valid_TimeSlot($StartTime, $EndTime, $ExcludeTimeSlotID='')
		{
			$cond_TimeSlotID = '';
			if ($ExcludeTimeSlotID != '')
				$cond_TimeSlotID = " AND timeSlotTable.TimeSlotID != '$ExcludeTimeSlotID' ";
			
			# condition 1: start time in between another time slot
			# condition 2: end time in between another time slot
			# condition 3: another timeslot is in between the time slot
			$sql = "SELECT
							timeSlotTable.TimeSlotID
					FROM
							INTRANET_TIMETABLE_TIMESLOT as timeSlotTable
						INNER JOIN
							INTRANET_TIMETABLE_TIMESLOT_RELATION as relationTable ON (timeSlotTable.TimeSlotID = relationTable.TimeSlotID)
					WHERE
							relationTable.TimetableID = '".$this->ObjectID."'
						AND
							(	
								('".$StartTime."' > timeSlotTable.StartTime AND '".$StartTime."' < timeSlotTable.EndTime)
								OR
								('".$EndTime."' > timeSlotTable.StartTime AND '".$EndTime."' < timeSlotTable.EndTime)
								OR
								('".$StartTime."' <= timeSlotTable.StartTime AND '".$EndTime."' >= timeSlotTable.EndTime)
							)
						$cond_TimeSlotID
					";
			$resultSet = $this->returnVector($sql);
			
			if (count($resultSet) > 0)
				return 0;
			else
				return 1;
		}
		
		function Is_Available_Room($TimeSlotID, $Day, $LocationID, $RoomAllocationID='', $CheckTemp=0)
		{
			# LocationID=0 means using others location
			# assume always available if using others location
			if ($LocationID == 0)
				return 1;

			if ($CheckTemp==1)
			{
				$table = 'INTRANET_TIMETABLE_ROOM_ALLOCATION_TEMP';
				$cond_ImportUserID = " And ImportUserID = '".$_SESSION['UserID']."' ";
			}
			else
			{
				$table = 'INTRANET_TIMETABLE_ROOM_ALLOCATION';
				$cond_ImportUserID = '';
			}
			
			$cond_RoomAllocationID = '';
			if ($RoomAllocationID != '')
				$cond_RoomAllocationID = " AND RoomAllocationID != '$RoomAllocationID' ";
			
			$sql = "SELECT
							RoomAllocationID
					FROM
							$table
					WHERE
							".$this->ID_FieldName." = '".$this->ObjectID."'
							AND
							TimeSlotID = '$TimeSlotID'
							AND
							Day = '$Day'
							AND
							LocationID = '$LocationID'
							$cond_RoomAllocationID
							$cond_ImportUserID
					";
			$resultSet = $this->returnArray($sql);
			
			if (count($resultSet) > 0)
				return 0;
			else
				return 1;
		}
		
		function Is_Subject_Group_Overlapped($TimeSlotID, $Day, $SubjectGroupID, $RoomAllocationID='', $CheckTemp=0)
		{
			if ($CheckTemp==1)
			{
				$table = 'INTRANET_TIMETABLE_ROOM_ALLOCATION_TEMP';
				$cond_ImportUserID = " And ImportUserID = '".$_SESSION['UserID']."' ";
			}
			else
			{
				$table = 'INTRANET_TIMETABLE_ROOM_ALLOCATION';
				$cond_ImportUserID = '';
			}
			
			$cond_RoomAllocationID = '';
			if ($RoomAllocationID != '')
				$cond_RoomAllocationID = " AND RoomAllocationID != '$RoomAllocationID' ";
			
			$sql = "	SELECT
								RoomAllocationID
						FROM
								$table
						WHERE
								".$this->ID_FieldName." = '".$this->ObjectID."'
								AND
								TimeSlotID = '$TimeSlotID'
								AND
								Day = '$Day'
								AND
								SubjectGroupID = '$SubjectGroupID'
								$cond_RoomAllocationID
								$cond_ImportUserID
					";
			$resultSet = $this->returnArray($sql);
			
			if (count($resultSet) > 0)
				return 1;
			else
				return 0;
		}
		
		function Has_User_Joined_Others_Subject_Group($TimeSlotID, $Day, $SubjectGroupID, $RoomAllocationID='', $CheckTemp=0)
		{
			global $PATH_WRT_ROOT;
			$YearTermID = $this->YearTermID;
			
			# Get Subject Group Allocated in the Timeslot
			$RoomAllocationArr = $this->Get_Room_Allocation_List($TimeSlotID, $Day, array(), '', '', 0, $CheckTemp);
			$numOfRoomAllocation = count($RoomAllocationArr[$TimeSlotID][$Day]);
			
			# if no room allocation => no conflict
			if ($numOfRoomAllocation == 0)
				return 0;
				
			$ExcludeSubjectGroupID = '';
			if ($RoomAllocationID != '')
			{
				$obj_RoomAllocation = new Room_Allocation($RoomAllocationID);
				$ExcludeSubjectGroupID = $obj_RoomAllocation->SubjectGroupID;
			}
			
			# Get Allocated Subject Group List
			$AllocatedSubjectGroupIDArr = array();
			for ($i=0; $i<$numOfRoomAllocation; $i++)
			{
				$thisSubjectGroupID = $RoomAllocationArr[$TimeSlotID][$Day][$i]['SubjectGroupID'];
				if ($thisSubjectGroupID != $ExcludeSubjectGroupID)
					$AllocatedSubjectGroupIDArr[] = $thisSubjectGroupID;
			}
			$numOfAllocatedSubjectGroup = count($AllocatedSubjectGroupIDArr);
			
			# if no room allocation => no conflict
			if ($numOfAllocatedSubjectGroup == 0)
				return 0;
			
			# Get student and teacher list of the Subject Group
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
			$scm = new subject_class_mapping();      
			$objSubjectGroup = new subject_term_class($SubjectGroupID, $GetTeacherList=true, $GetStudentList=true);
			
			
			# Check Student
			$isConflictStudent = 0;
			$StudentList = $objSubjectGroup->ClassStudentList;
			if (is_array($StudentList))
				$numOfStudent = count($StudentList);
			else
				$numOfStudent = 0;
				
			for($i=0; $i<$numOfStudent; $i++)
			{
				$thisUserID = $StudentList[$i]['UserID'];
				$thisSubjectGroupTaken = $scm->Get_SubjectGroupID_Taken($YearTermID, "Personal", '', $thisUserID, '', "Student", $SubjectGroupID, $AllocatedSubjectGroupIDArr);
				if (count($thisSubjectGroupTaken) > 0)
				{
					$isConflictStudent = 1;
					break;
				}
			}
			
			return $isConflictStudent;
		}
		
		function Is_Code_Vaild($InputTitle, $YearTeamID, $TimetableID='')
		{
			$InputTitle = $this->Get_Safe_Sql_Query(trim($InputTitle));
			
			if ($TimetableID != '')
				$conds = " AND TimetableID != '$TimetableID' ";
			
			# Check Timetable Title
			$sql = "	SELECT 
										DISTINCT(".$this->ID_FieldName.") 
								FROM 
										".$this->TableName." 
								WHERE 
										TimetableName = '".$InputTitle."'
									AND
										YearTermID = '".$YearTeamID."'
									$conds
							";
			$TimetableArr = $this->returnVector($sql);
			
			$isVaild = (count($TimetableArr)==0)? 1 : 0;
			
			return $isVaild;
		}
		
		function Get_All_Others_Location()
		{
			$sql = "SELECT
							OthersLocation
					FROM
							INTRANET_TIMETABLE_ROOM_ALLOCATION
					WHERE
							".$this->ID_FieldName." = '".$this->ObjectID."'
						AND
							LocationID = '0'
						AND
							OthersLocation IS NOT NULL
						AND
							OthersLocation != ''
					";
			$OthersLocationArr = $this->returnVector($sql);
			
			return $OthersLocationArr;
		}
		
		function Add_TimeSlot($DataArr=array())
		{
			$TimetableID = $this->ObjectID;
			
			if (count($DataArr) == 0 || $TimetableID == '')
				return false;
			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
			}
			
			## set others fields
			# DateInput
			$fieldArr[] = 'DateInput';
			$valueArr[] = 'now()';
			# DateModified
			$fieldArr[] = 'DateModified';
			$valueArr[] = 'now()';
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$sql = '';
			$sql .= ' INSERT INTO INTRANET_TIMETABLE_TIMESLOT ';
			$sql .= ' ( '.$fieldText.' ) ';
			$sql .= ' VALUES ';
			$sql .= ' ( '.$valueText.' ) ';
			
			$success['Insert_TimeSlot'] = $this->db_db_query($sql);
			
			if ($success == false)
			{
				$this->RollBack_Trans();
				return 0;
			}
			else
			{
				$this->Commit_Trans();
				
				# return the inserted TimeSlotID
				$sql_max = "SELECT MAX(TimeSlotID) FROM INTRANET_TIMETABLE_TIMESLOT";
				$resultSet = $this->returnVector($sql_max);
				$insertedTimeSlotID = $resultSet[0];
				
				# Add Timetable-Timeslot Relation
				$sql_insert = "	INSERT INTO 
									INTRANET_TIMETABLE_TIMESLOT_RELATION
									(TimetableID, TimeSlotID, DateInput, DateModified)
								VALUES
									('$TimetableID', '$insertedTimeSlotID', now(), now())
							";
				$success['Add_Relation'] = $this->db_db_query($sql_insert);
				
				# Reorder the display order of the timeslot
				# If I insert a timeslot between TimeSlot1 and TimeSlot2, 
				# TimeSlot1 remains 1, add TimeSlot becomes TimeSlot2, and the TimeSlot2 becomes TimeSlot3
				$thisDisplayOrder = $DataArr['DisplayOrder'];
				$sql_update = "UPDATE ".$this->TableName." 
									SET DisplayOrder = DisplayOrder+1 
									WHERE 
										TimeSlotID != '".$insertedTimeSlotID."' 
									AND
										DisplayOrder >= ".$thisDisplayOrder;
				$success['Update_DisplayOrder'] = $this->db_db_query($sql_update);
				
				return $success;
			}
		}
		
		function Get_Current_Timetable($ParDate='', $ParCheckSpecialTimetable=true)
		{
			global $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/libcycleperiods.php');
			$libcycleperiods = new libcycleperiods();
			
			# get current timezone (PeriodID)
			$curTimeZoneInfoArr = GetCurrentTimeZoneInfo($ParDate);
			$curPeriodID = $curTimeZoneInfoArr[0]['PeriodID'];
			
			# check special timetable
			$TargetDate = ($ParDate=='')? date('Y-m-d') : $ParDate;
			$SpecialSettingsArr = $libcycleperiods->Get_Special_Timetable_Settings_Date_Info($SpecialTimetableSettingsIDArr='', $TimezoneIDArr='', $TargetDate);
			$TimetableID = $SpecialSettingsArr[$TargetDate]['TimetableID'];
			
			if (!$ParCheckSpecialTimetable || ($ParCheckSpecialTimetable && $TimetableID == '')) {
				// no special timetable settings => get the general one from time zone settings
				$sql = "SELECT TimetableID FROM INTRANET_PERIOD_TIMETABLE_RELATION Where PeriodID = '".$curPeriodID."'";
				$resultSet = $this->returnVector($sql);
				$TimetableID = $resultSet[0];
			}		
			
			return $TimetableID;
		}
		
		// $ParDate = "yyyy-mm-dd"
		function Get_Current_Timetable_Cycle_Day($ParDate='')
		{
			global $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
			$lcycleperiods = new libcycleperiods();
			
			$ParDate = ($ParDate=='')? date("Y-m-d") : $ParDate;
			
			### Get Special Timetable Info
			$SpecialTimetableAssoArr = $lcycleperiods->Get_Special_Timetable_Settings_Date_Info($SpecialTimetableSettingsIDArr='', $TimezoneIDArr='', $ParDate);
			$SpecialTimetableSettingsID = $SpecialTimetableAssoArr[$ParDate]['SpecialTimetableSettingsID'];
			
			$LoadDefaultCycleDay = true;
			if ($SpecialTimetableSettingsID != '') {
				$SpecialTimetableInfoArr = $lcycleperiods->Get_Special_Timetable_Settings_Info($SpecialTimetableSettingsID);
				$SpecialTimetableRepeatType = $SpecialTimetableInfoArr[$SpecialTimetableSettingsID]['BasicInfo']['RepeatType'];
				
				if ($SpecialTimetableRepeatType == 'weekday') {
					$dateInfoArr = getdate();
					$timetableCycleDay = $dateInfoArr['wday'];
					
					$LoadDefaultCycleDay = false;
				}
			}
			
			if ($LoadDefaultCycleDay) {
				//$CurCycleDate = $lcycleperiods->getCycleDayStringByDate($ParDate);
				$CurCycleDateInfoArr = $lcycleperiods->getCycleInfoByDate($ParDate);
				$CurCycleDate = $CurCycleDateInfoArr['CycleDay'];
				if ($CurCycleDate == '') {
					$dateInfoArr = getdate();
					$timetableCycleDay = $dateInfoArr['wday'];
				}
				else {
					$timetableCycleDay = $CurCycleDate;
				}
			}
			
			return $timetableCycleDay;
		}
		
		function Get_Copied_Timetable_By_Term($FromYearTermID, $ToYearTermID)
		{
			$TimetableArr = $this->Get_All_TimeTable('', $FromYearTermID);
			$TimetableIDArr = Get_Array_By_Key($TimetableArr, 'TimetableID');
			
			$CopiedTimtableArr = array();
			if (count($TimetableIDArr) > 0)
			{
				$TimetableIDList = implode(',', $TimetableIDArr);
				$sql = "Select
								TimetableID
						From
								INTRANET_SCHOOL_TIMETABLE
						Where
								CopyFromTimetableID In (".$TimetableIDList.")
								And
								YearTermID = '".$ToYearTermID."'
						";
				$CopiedTimtableIDArr = $this->returnVector($sql);
			}
			
			return $CopiedTimtableIDArr;
		}
		
		### return $array[$FromTimeSlotID] = $ToTimeSlotID
		function Get_Copied_Timetable_TimeSlot_Mapping($TargetTimetable)
		{
			$PreloadArrKey = 'Get_Copied_Timetable_TimeSlot_Mapping';
			$FuncArgArr = get_defined_vars();
			$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
	    	if ($returnArr !== false) {
	    		return $returnArr;
	    	}
	    	
			$FromTimeSlotArr = $this->Get_TimeSlot_List();
			$FromTimeSlotIDArr = Get_Array_By_Key($FromTimeSlotArr, 'TimeSlotID');
			$numOfFromTimeSlot = count($FromTimeSlotIDArr);
			
			$ReturnArr = array();
			if ($numOfFromTimeSlot > 0)
			{
				$FromTimeSlotIDList = implode(',', $FromTimeSlotIDArr);
				
				$sql = "Select
								ittFrom.TimeSlotID as FromTimeSlotID, ittTo.TimeSlotID as ToTimeSlotID
						From
								INTRANET_TIMETABLE_TIMESLOT as ittFrom
								Inner Join
								INTRANET_TIMETABLE_TIMESLOT as ittTo
								On (ittFrom.TimeSlotID = ittTo.CopyFromTimeSlotID)
								Inner Join
								INTRANET_TIMETABLE_TIMESLOT_RELATION as ittr
								On (ittTo.TimeSlotID = ittr.TimeSlotID)
						Where
								ittFrom.TimeSlotID In ($FromTimeSlotIDList)
								And
								ittr.TimetableID = '".$TargetTimetable."'
						";
				$resultSet = $this->returnArray($sql);
				$numOfResult = count($resultSet);
				
				for ($i=0; $i<$numOfResult; $i++) {
					$ReturnArr[$resultSet[$i]['FromTimeSlotID']] = $resultSet[$i]['ToTimeSlotID'];
				}
			}
			
			$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ReturnArr);
			return $ReturnArr;
		}
		
		function Copy_Timetable($FromTimetableID, $ToTimetableID, $SubjectGroupIDMapping='')
		{
			$obj_FromTimetable = new Timetable($FromTimetableID);
			$obj_ToTimetable = new Timetable($ToTimetableID);
			
			$FromYearTermID = $obj_FromTimetable->YearTermID;
			$ToYearTermID = $obj_ToTimetable->YearTermID;
			
			# Set Cycle Days
			$targetCycleDays = $obj_FromTimetable->CycleDays;
			$releaseStatus = $obj_FromTimetable->ReleaseStatus;
			$DataArr['CycleDays'] = $targetCycleDays;
			$DataArr['ReleaseStatus'] = $releaseStatus;
			$successArr['Update_CycleDays'] = $obj_ToTimetable->Update_Record($DataArr);
			
			# Copy Time Slot
			$TimeSlotMappingArr = $obj_FromTimetable->Get_Copied_Timetable_TimeSlot_Mapping($ToTimetableID);
			
			$numOfMapping = count($TimeSlotMappingArr);
			if ($numOfMapping == 0)
			{
				$TimeSlotArr = $obj_FromTimetable->Get_TimeSlot_List();
				$numOfTimeSlot = count($TimeSlotArr);
				
				for ($i=0; $i<$numOfTimeSlot; $i++)
				{
					unset($DataArr);
					$DataArr['TimeSlotName'] = $TimeSlotArr[$i]['TimeSlotName'];
					$DataArr['StartTime'] = $TimeSlotArr[$i]['StartTime'];
					$DataArr['EndTime'] = $TimeSlotArr[$i]['EndTime'];
					$DataArr['DisplayOrder'] = $TimeSlotArr[$i]['DisplayOrder'];
					$DataArr['SessionType'] = $TimeSlotArr[$i]['SessionType'];
					$DataArr['CopyFromTimeSlotID'] = $TimeSlotArr[$i]['TimeSlotID'];
					
					$success['Add_TimeSlot_'.$DataArr['TimeSlotName']] = $obj_ToTimetable->Add_TimeSlot($DataArr);
				}
			}
			
			# Copy Room Allocation
			# Get Time Slot info
			$libRoomAllocation = new Room_Allocation();
			$TimeSlotArr = $obj_ToTimetable->Get_TimeSlot_List();
			$TargetTimeSlotArr = $obj_FromTimetable->Get_TimeSlot_List();
			
			# Build old-new timeslot mapping
			$numOfTimeSlot = count($TimeSlotArr);
			$TimeSlotAssoArr = array();
			for ($i=0; $i<$numOfTimeSlot; $i++)
			{
				$thisTimeSlotID = $TimeSlotArr[$i]['TimeSlotID'];
				$thisTargetTimeSlotID = $TargetTimeSlotArr[$i]['TimeSlotID'];
				$TimeSlotAssoArr[$thisTargetTimeSlotID] = $thisTimeSlotID;
			}
			
			# Get Room Allocation Info
			$RoomAllocationArr = $obj_FromTimetable->Get_Room_Allocation_List();
			$numOfTimeSlot = count($RoomAllocationArr);
			
			$scm = new subject_class_mapping();
			$SubjectGroupArr = $scm->Get_Subject_Group_List($obj_ToTimetable->YearTermID, '' ,0);
			$numOfSubjectGroup = count($SubjectGroupArr);
			$SubjectGroupIDArr = array();
			for ($i=0; $i<$numOfSubjectGroup; $i++)
				$SubjectGroupIDArr[] = $SubjectGroupArr[$i]['SubjectGroupID'];
				
			# Build SubjectGroupID Mapping if YearTermID is different
			if ($FromYearTermID != $ToYearTermID && $SubjectGroupIDMapping=='')
			{
				$SubjectGroupIDMapping = $scm->Get_Term_Copied_SubjectGroup($FromYearTermID, $ToYearTermID);
			}
				
			$DataArr = array();
			if ($numOfTimeSlot > 0)
			{
				foreach ($RoomAllocationArr as $timeSlotID => $timeSlotRoomAllocationArr)
				{
					foreach ($timeSlotRoomAllocationArr as $day => $thisRoomAllocationArr)
					{
						$thisNumOfRoomAllocation = count($thisRoomAllocationArr);
						for ($i=0; $i<$thisNumOfRoomAllocation; $i++)
						{
							unset($DataArr);
							$DataArr['TimetableID'] = $ToTimetableID;
							$DataArr['TimeSlotID'] = $TimeSlotAssoArr[$timeSlotID];
							$DataArr['Day'] = $day;
							$DataArr['LocationID'] = $thisRoomAllocationArr[$i]['LocationID'];
							$DataArr['OthersLocation'] = $thisRoomAllocationArr[$i]['OthersLocation'];
							
							if ($FromYearTermID == $ToYearTermID)
							{
								$targetSubjectGroupID = $thisRoomAllocationArr[$i]['SubjectGroupID'];
								if (!in_array($targetSubjectGroupID, $SubjectGroupIDArr))
									continue;
							}
							else
							{
								$thisFromSubjectGroupID = $thisRoomAllocationArr[$i]['SubjectGroupID'];
								$targetSubjectGroupID = $SubjectGroupIDMapping[$thisFromSubjectGroupID];
								
								if ($targetSubjectGroupID == '')
									continue;
							}
							$DataArr['SubjectGroupID'] = $targetSubjectGroupID;
							
							$successArr['Add_Room_Allocation_'.$DataArr['TimeSlotID'].'_'.$day.'_'.$i] = $libRoomAllocation->Insert_Record($DataArr);
						}
					}
				}
			}
			
			if (in_array(false, $successArr))
				return false;
			else
				return true;
		}
		
		function Get_Copied_Timetable($YearTermID)
		{
			$sql = "Select
							TimetableID
					From
							".$this->TableName."
					Where
							YearTermID = '".$YearTermID."'
							And
							CopyFromTimetableID = '".$this->ObjectID."'
					";
			$resultSet = $this->returnVector($sql);
			return $resultSet;
		}
		
		/*
		 * $ViewMode = Class / SubjectGroup / Personal / Room
		 * $Identity = Student / TeachingStaff
		 */
		function Get_Filter_Array_By_Filter_Settings($ViewMode, $YearClassIDArr, $SubjectGroupIDArr, $Identity, $UserIDArr, $BuildingID, $LocationIDArr, $OthersLocation)
		{
			global $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
			$scm = new subject_class_mapping();
			
			$YearTermID = $this->YearTermID;
			
			$SubjectGroupID_DisplayFilterArr = array();
			$LocationID_DisplayFilterArr = array();
			
			if ($ViewMode == "Class")
			{
				# Get all subject groups of the class and assign to $SubjectGroupID_DisplayFilterArr
				$numOfYearClassID = count($YearClassIDArr);
				for($i=0; $i<$numOfYearClassID; $i++)
				{
					$thisYearClassID = $YearClassIDArr[$i];
					
					$this_SubjectGroupID_DisplayFilterArr = $scm->Get_SubjectGroupID_Taken($YearTermID, $ViewMode, $thisYearClassID);
					
					// no subject group in the class
					if(count($this_SubjectGroupID_DisplayFilterArr) == 0)
						$this_SubjectGroupID_DisplayFilterArr = array(0);
						
					$SubjectGroupID_DisplayFilterArr = array_merge($SubjectGroupID_DisplayFilterArr, $this_SubjectGroupID_DisplayFilterArr);
				}
			}
			else if ($ViewMode == "SubjectGroup")
			{
				$SubjectGroupID_DisplayFilterArr = $SubjectGroupIDArr;
			}
			else if ($ViewMode == "Personal")
			{
				# Get all subject groups which the person has joined and assign to $SubjectGroupID_DisplayFilterArr
				$numOfUserID = count($UserIDArr);
				for($i=0; $i<$numOfUserID; $i++)
				{
					$thisUserID = $UserIDArr[$i];
					$this_SubjectGroupID_DisplayFilterArr = $scm->Get_SubjectGroupID_Taken($YearTermID,$ViewMode,'',$thisUserID,'',$Identity);
					if(count($this_SubjectGroupID_DisplayFilterArr) == 0)
						$this_SubjectGroupID_DisplayFilterArr = array(0);
					
					$SubjectGroupID_DisplayFilterArr = array_merge($SubjectGroupID_DisplayFilterArr, $this_SubjectGroupID_DisplayFilterArr);
				}
			}
			else if ($ViewMode == "Room")
			{
				# Loop through all Selected Locations to show the individual location timetable
				# Assign the selected locations into the array $LocationID_DisplayFilterArr
				if ($BuildingID != -1)
				{
					# Location in School
					$LocationID_DisplayFilterArr = $LocationIDArr;
				}
				else
				{
					# Others Location
					$LocationID_DisplayFilterArr = array(0);
				}
			}
			
			return array("SubjectGroupIDArr" => $SubjectGroupID_DisplayFilterArr, "LocationIDArr" => $LocationID_DisplayFilterArr);
		}
		
		function Get_Import_Lesson_Column_Title($ParLang='', $TargetPropertyArr='')
		{
			global $Lang;
			
			$ReturnArr = array();
			
			if ($ParLang != '')
				$TitleArr = $Lang['SysMgr']['Timetable']['ExportTimetable_Column'][$ParLang];
			else
				$TitleArr = Get_Lang_Selection($Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'], $Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En']);
				
			if ($TargetPropertyArr != '')
			{
				if (!is_array($TargetPropertyArr))
					$TargetPropertyArr = array($TargetPropertyArr);
					
				$numOfColumn = count($TitleArr);
				$ColumnPropertyArr = $this->Get_Import_Lesson_Column_Property();
				for ($i=0; $i<$numOfColumn; $i++)
				{
					if (in_array($ColumnPropertyArr[$i], $TargetPropertyArr))
						$ReturnArr[] = $TitleArr[$i];
				}
			}
			else
			{
				$ReturnArr = $TitleArr;
			}
			
			return $ReturnArr;
		}
		
		function Get_Import_Lesson_Column_Property()
		{
			// 1: Required, 2: Reference, 3: Optional
			return array(1, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1);
		}
		
		function Delete_Import_Temp_Data()
		{
			$sql = "Delete from TEMP_TIMETABLE_LESSON_IMPORT Where UserID = '".$_SESSION["UserID"]."' ";
			$SuccessArr['DeleteTempImportCsvData'] = $this->db_db_query($sql);
			
			$sql = "Delete from INTRANET_TIMETABLE_ROOM_ALLOCATION_TEMP Where ImportUserID = '".$_SESSION["UserID"]."' ";
			$SuccessArr['DeleteTempTimetableData'] = $this->db_db_query($sql);
			
			return !in_array(false, $SuccessArr);
		}
		
		function Copy_Lesson_To_Temp_Data_Table()
		{
			$SuccessArr = array();
			
			// $LessonInfoArr[$TimeSlotID][$Day][0,1,2...] = Lesson Info Array
			$LessonInfoArr = $this->Get_Room_Allocation_List();
			foreach ((array)$LessonInfoArr as $thisTimeSlotID => $thisTimeSlotLessonArr)
			{
				foreach ((array)$thisTimeSlotLessonArr as $thisDay => $thisDayTimeSlotLessonArr)
				{
					$numOfLesson = count($thisDayTimeSlotLessonArr);
					for ($i=0; $i<$numOfLesson; $i++)
					{
						$InsertInfoArr = array();
						
						$thisLessonInfoArr = $thisDayTimeSlotLessonArr[$i];
						$thisRoomAllocationID = $thisLessonInfoArr['RoomAllocationID'];
						
						foreach ((array)$thisLessonInfoArr as $field => $value)
						{
							if (is_numeric($field))
								continue;
								
							$InsertInfoArr[$field] = $value;
						}
						
						$fieldArr = array();
						$valueArr = array();
						foreach ($InsertInfoArr as $field => $value)
						{
							$fieldArr[] = $field;
							$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
						}
						
						## set others fields
						# ImportUserID
						$fieldArr[] = 'ImportUserID';
						$valueArr[] = $_SESSION['UserID'];
						
						$fieldText = implode(", ", $fieldArr);
						$valueText = implode(", ", $valueArr);
						
						# Insert Record
						$sql = '';
						$sql .= ' INSERT INTO INTRANET_TIMETABLE_ROOM_ALLOCATION_TEMP ';
						$sql .= ' ( '.$fieldText.' ) ';
						$sql .= ' VALUES ';
						$sql .= ' ( '.$valueText.' ) ';
						
						$SuccessArr[$thisRoomAllocationID] = $this->db_db_query($sql);
					}
				}
			}
		}
		
		function getTeacherLessonByDate($parUserId, $parDate) {
			global $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
			include_once($PATH_WRT_ROOT.'includes/liblocation.php');
			$lscm = new subject_class_mapping();
			$llocation = new liblocation();
			
			// get basic info of the date (e.g. term, cycle day, timetable, etc...)
			$timetableId = $this->Get_Current_Timetable($parDate);
			$cycleDay = $this->Get_Current_Timetable_Cycle_Day($parDate);
			$academicInfoAry = getAcademicYearAndYearTermByDate($parDate);
			$yearTermId = $academicInfoAry['YearTermID'];
			
			// get the teaching subject groups of the teacher
			$subjectGroupIdAry = $lscm->Get_SubjectGroupID_Taken($yearTermId, 'Personal', $YearClassID="", $parUserId, $LocationID="", "Teacher");
			
			// get the info of the teaching subject groups
			$subjectGroupAssoAry = $lscm->Get_Subject_Group_Info('', $yearTermId, '', $subjectGroupIdAry);
			$numOfSubjectGroup = count($subjectGroupAssoAry);
			
			$lessonAry = array();
			if ($numOfSubjectGroup > 0) {
				// get the timeslot of the timetable
				$timetableObj = new Timetable($timetableId);
				$timeslotAssoAry = $timetableObj->Get_TimeSlot_List($ReturnAsso=true);
				
				// get the lesson info of the teaching subjects
				$lessonAry = $timetableObj->Get_Room_Allocation_List($TimeSlotID='', $cycleDay, $subjectGroupIdAry, $LocationID='', $OthersLocation='', $DayAsFirstKey=0, $CheckTemp=0, $returnList=true,$RelatedRoomAllocationIDArr='', $returnKeyOnly=true);
				$locationIdAry = Get_Array_By_Key($lessonAry, 'LocationID');
				
				// get the location info of the involved lessons
				$locationAssoAry = $llocation->Get_Building_Floor_Room_Name_Arr($timetableId, $locationIdAry);
				
				// consolidate all info in the lesson array
				$numOfLesson = count($lessonAry);
				for ($i=0; $i<$numOfLesson; $i++) {
					$_timeslotId = $lessonAry[$i]['TimeSlotID'];
					$_subjectGroupId = $lessonAry[$i]['SubjectGroupID'];
					$_locationId = $lessonAry[$i]['LocationID'];
					
					// subject group info
					$lessonAry[$i]['SubjectGroupCode'] = $subjectGroupAssoAry[$_subjectGroupId]['SubjectGroupCode'];
					$lessonAry[$i]['SubjectGroupNameEn'] = $subjectGroupAssoAry[$_subjectGroupId]['ClassTitleEN'];
					$lessonAry[$i]['SubjectGroupNameCh'] = $subjectGroupAssoAry[$_subjectGroupId]['ClassTitleB5'];
					$lessonAry[$i]['SubjectNameEn'] = $subjectGroupAssoAry[$_subjectGroupId]['SubjectDescEN'];
					$lessonAry[$i]['SubjectNameCh'] = $subjectGroupAssoAry[$_subjectGroupId]['SubjectDescB5'];
					
					// timeslot info
					$lessonAry[$i]['TimeSlotName'] = $timeslotAssoAry[$_timeslotId]['TimeSlotName'];
					$lessonAry[$i]['StartTime'] = $timeslotAssoAry[$_timeslotId]['StartTime'];
					$lessonAry[$i]['EndTime'] = $timeslotAssoAry[$_timeslotId]['EndTime'];
					
					// location info
					if ($_locationId > 0) {
						$lessonAry[$i]['BuildingID'] = $locationAssoAry[$_locationId]['BuildingID'];
						$lessonAry[$i]['BuildingCode'] = $locationAssoAry[$_locationId]['BuildingCode'];
						$lessonAry[$i]['BuildingNameCh'] = $locationAssoAry[$_locationId]['BuildingNameChi'];
						$lessonAry[$i]['BuildingNameEn'] = $locationAssoAry[$_locationId]['BuildingNameEng'];
						
						$lessonAry[$i]['FloorID'] = $locationAssoAry[$_locationId]['LocationLevelID'];
						$lessonAry[$i]['FloorCode'] = $locationAssoAry[$_locationId]['FloorCode'];
						$lessonAry[$i]['FloorNameCh'] = $locationAssoAry[$_locationId]['FloorNameChi'];
						$lessonAry[$i]['FloorNameEn'] = $locationAssoAry[$_locationId]['FloorNameEng'];
						
						$lessonAry[$i]['RoomCode'] = $locationAssoAry[$_locationId]['RoomCode'];
						$lessonAry[$i]['RoomNameCh'] = $locationAssoAry[$_locationId]['RoomNameChi'];
						$lessonAry[$i]['RoomNameEn'] = $locationAssoAry[$_locationId]['RoomNameEng'];
					}
				}
			}
			
			return $lessonAry;
		}
		
		function getFreeLessonTeacher($parDate, $parTimeslotId) {
			global $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
			include_once($PATH_WRT_ROOT.'includes/liblocation.php');
			include_once($PATH_WRT_ROOT.'includes/libuser.php');
			$lscm = new subject_class_mapping();
			$llocation = new liblocation();
			$luser = new libuser();
			
			// get basic info of the date (e.g. term, cycle day, timetable, etc...)
			$timetableId = $this->Get_Current_Timetable($parDate);
			$cycleDay = $this->Get_Current_Timetable_Cycle_Day($parDate);
			$academicInfoAry = getAcademicYearAndYearTermByDate($parDate);
			$yearTermId = $academicInfoAry['YearTermID'];
			
			// get lessons of the timetable
			$objTimetable = new Timetable($timetableId);
			$roomAllocationAry = $objTimetable->Get_Room_Allocation_List($parTimeslotId, $cycleDay, $SubjectGroupIDArr=array(), $LocationID='', $OthersLocation='', $DayAsFirstKey=0, $CheckTemp=0, $returnList=true, $RelatedRoomAllocationIDArr='', $returnKeyOnly=false);
			$withLessonSubjectGroupIdAry = Get_Array_By_Key($roomAllocationAry, 'SubjectGroupID'); 
			
			// get all teachers info
			$allTeacherInfoAry = $luser->returnUsersType2(USERTYPE_STAFF, $teaching=true);
			$allTeacherIdAry = Get_Array_By_Key($allTeacherInfoAry, 'UserID');
			
			// get teacher info for all Subject Groups
			$subjectGroupTeacherInfoAry = $lscm->Get_Subject_Group_Teacher_Info($withLessonSubjectGroupIdAry, $yearTermId);
			$withLessonTeacherIdAry = array_values(array_unique(Get_Array_By_Key($subjectGroupTeacherInfoAry, 'UserID')));
			
			// get free lesson teachers and info
			$freeLessonTeacherIdAry = array_values(array_diff($allTeacherIdAry, $withLessonTeacherIdAry));
			$numOfFreeLessonTeacher = count($freeLessonTeacherIdAry);
			$userObj = new libuser('', '', $freeLessonTeacherIdAry);
			
			$freeLessonTeacherAry = array();
			for ($i=0; $i<$numOfFreeLessonTeacher; $i++) {
				$_teahcerUserId = $freeLessonTeacherIdAry[$i];
				
				$userObj->LoadUserData($_teahcerUserId);
				
				$_userInfoAry = array();
				$_userInfoAry['UserID'] = $userObj->UserID;
				$_userInfoAry['EnglishName'] = $userObj->EnglishName;
				$_userInfoAry['ChineseName'] = $userObj->ChineseName;
				
				$freeLessonTeacherAry[] = $_userInfoAry;
			}
			 
			
			return $freeLessonTeacherAry;
		}
		
		function Get_Preload_Result($ArrayKey, $FunctionArgumentArr) {
			$PreloadInfoArrVariableStr = $this->Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr);
	    	$PreloadInfoArrExist = $this->Check_PreloadInfoArr_Exist($ArrayKey, $FunctionArgumentArr);
	    	
	    	$returnAry = array();
	    	if ($PreloadInfoArrExist)
	    	{
                $PreloadInfoArrVariableStr = str_replace(';','',$PreloadInfoArrVariableStr);
	    		eval('$returnAry = '.$PreloadInfoArrVariableStr.';');
	    		return $returnAry;
	    	}
	    	else {
	    		return false;
	    	}
		}
		
		function Set_Preload_Result($ArrayKey, $FunctionArgumentArr, $ResultArr) {
			$PreloadInfoArrVariableStr = $this->Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr);
            $PreloadInfoArrVariableStr = str_replace(';','',$PreloadInfoArrVariableStr);
			eval($PreloadInfoArrVariableStr.' = $ResultArr;');
		}
		
		function Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr) {
			$PreloadInfoArrVariableStr = '$this->PreloadInfoArr[\''.$ArrayKey.'\']';
	    	foreach( (array)$FunctionArgumentArr as  $arg_key => $arg_val)
	    	{
	    		if (is_array($arg_val)) {
	    			$key_name = implode(',', $arg_val);
	    		}
	    		else {
	    			$key_name = trim($arg_val);
	    		}
	    		$PreloadInfoArrVariableStr .= "['".addslashes($key_name)."']";
	    	}
	    	
	    	return $PreloadInfoArrVariableStr;
		}
		
		function Check_PreloadInfoArr_Exist($ArrayKey, $FunctionArgumentArr) {
			$PreloadInfoArrVariableStr = $this->Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr);
            $PreloadInfoArrVariableStr = str_replace(';','',$PreloadInfoArrVariableStr);
			
			$PreloadInfoArrExist = false;
	    	eval('$PreloadInfoArrExist = isset('.$PreloadInfoArrVariableStr.');');
	    	
	    	return $PreloadInfoArrExist;
		}
		
		function Get_NextDate_byCycleDay($cycDay, $fromDate, $thisTermPeriodEnd = "") {
			$RecordDate = $fromDate;
			$strSQL = "SELECT PeriodID, PeriodType FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE PeriodStart <='" . $fromDate . "' AND PeriodEnd >= '" . $fromDate . "'";
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				extract($result[0]);
				if ($PeriodType > 0) {
					$strSQL = "SELECT RecordDate FROM INTRANET_CYCLE_DAYS WHERE RecordDate >= '" . $fromDate . "' AND CycleDay='" . $cycDay . "' AND PeriodID='" . $PeriodID . "' ORDER BY RecordDate ASC LIMIT 1;";
					
					$result = $this->returnResultSet($strSQL);
					$RecordDate = "";
					if (count($result) > 0) {
						$RecordDate = $result[0]["RecordDate"];
						if (!empty($thisTermPeriodEnd) && strtotime($RecordDate) > strtotime($thisTermPeriodEnd)) {
							$strSQL = "SELECT RecordDate FROM INTRANET_CYCLE_DAYS WHERE RecordDate < '" . $fromDate . "' AND CycleDay='" . $cycDay . "' AND PeriodID='" . $PeriodID . "' ORDER BY RecordDate DESC LIMIT 1;";
							$result = $this->returnResultSet($strSQL);
							if (count($result) > 0) {
								$RecordDate = $result[0]["RecordDate"];
							}
						}
					}
				} else {
					if ($fromData == date("Y-m-d")) {
						$RecordDate = $fromDate;
					} else {
						switch ($cycDay) {
							case "1":
								if (strtolower(date("l")) == "monday") {
									$RecordDate = date("Y-m-d");
								} else {
									$RecordDate = date("Y-m-d", strtotime('next monday'));
								}
								break;
							case "2":
								if (strtolower(date("l")) == "tuesday") {
									$RecordDate = date("Y-m-d");
								} else {
									$RecordDate = date("Y-m-d", strtotime('next tuesday'));
								}
								break;
							case "3":
								if (strtolower(date("l")) == "wednesday") {
									$RecordDate = date("Y-m-d");
								} else {
									$RecordDate = date("Y-m-d", strtotime('next wednesday'));
								}
								break;
							case "4":
								if (strtolower(date("l")) == "thursday") {
									$RecordDate = date("Y-m-d");
								} else {
									$RecordDate = date("Y-m-d", strtotime('next thursday'));
								}
								break;
							case "5":
								if (strtolower(date("l")) == "Friday") {
									$RecordDate = date("Y-m-d");
								} else {
									$RecordDate = date("Y-m-d", strtotime('next Friday'));
								}
								break;
							case "6":
								if (strtolower(date("l")) == "Saturday") {
									$RecordDate = date("Y-m-d");
								} else {
									$RecordDate = date("Y-m-d", strtotime('next Saturday'));
								}
								break;
							case "7":
								if (strtolower(date("l")) == "Sunday") {
									$RecordDate = date("Y-m-d");
								} else {
									$RecordDate = date("Y-m-d", strtotime('next Sunday'));
								}
								break;
						}
					}
				}
			}
			return $RecordDate;
		}
		
		function getEverydayTimetableByMonth($year, $month)
		{
		    $month = substr('0'.$month,-2);
		    $dateStr = $this->Get_Safe_Sql_Like_Query($year.'-'.$month);
		    $sql = "SELECT
                		    RecordDate, IF(TimetableID>0,TimetableID,DefaultTimetableID) AS TimetableID
        		    FROM
                		    INTRANET_SCHOOL_EVERYDAY_TIMETABLE
        		    WHERE
                		    RecordDate LIKE '$dateStr%'
                		    ORDER BY RecordDate";
		    $timetableArr = $this->returnResultSet($sql);
		    $timetableAssoc = BuildMultiKeyAssoc($timetableArr,array('RecordDate'),array('TimetableID'),1);

		    return $timetableAssoc;
		}

		// return array(Timestamp=>TimetableID)
		function getEverydayTimetableByPeriod($startDate, $endDate, $ts=true)
		{
		    $dateField = $ts ? "UNIX_TIMESTAMP(RecordDate) AS RecordDate" : "RecordDate";
		    $sql = "SELECT
                		    $dateField, IF(TimetableID>0,TimetableID,DefaultTimetableID) AS TimetableID
        		    FROM
                		    INTRANET_SCHOOL_EVERYDAY_TIMETABLE
        		    WHERE
        		            RecordDate BETWEEN '".$startDate."' AND '".$endDate."'
        		            ORDER BY RecordDate";
		    $timetableArr = $this->returnResultSet($sql);
		    $timetableAssoc = BuildMultiKeyAssoc($timetableArr,array('RecordDate'),array('TimetableID'),1);
		    return $timetableAssoc;
		}
		
		function getAllTimeslotByTimetableID($timetableIDAry)
		{
		    if (!is_array($timetableIDAry)) {
		        $timetableIDAry = array($timetableIDAry);
		    }
		    
		    $sql = "SELECT
                            ittr.TimetableID,
							itt.TimeSlotID,
							itt.StartTime,
							itt.EndTime,
                            itt.TimeSlotName,
                            itt.DisplayOrder-1 AS DisplayOrder
					FROM
							INTRANET_TIMETABLE_TIMESLOT as itt
				    INNER JOIN
							INTRANET_TIMETABLE_TIMESLOT_RELATION as ittr ON (itt.TimeSlotID = ittr.TimeSlotID)
					WHERE
							ittr.TimetableID IN ('".implode("','",$timetableIDAry)."')
					ORDER BY
							itt.StartTime
					";
		    $timetableArr = $this->returnResultSet($sql);
		    $timetableAssoc = BuildMultiKeyAssoc($timetableArr,array('TimetableID','DisplayOrder'),array('TimeSlotID','StartTime','EndTime','TimeSlotName'));
		    return $timetableAssoc;
		}
		
		function getSkipSchoolEventDateByYear($academicYearID, $startDate='', $endDate='')
		{
		    $academicYearID = IntegerSafe($academicYearID);
		    if ($startDate=='') {
		        $startDate = getStartDateOfAcademicYear($academicYearID);
		    }
		    if ($endDate=='') {
		        $endDate = getEndDateOfAcademicYear($academicYearID);
		    }
		    
		    ## Get All Group Event ##
		    $sql = "SELECT 
                            DISTINCT UNIX_TIMESTAMP(a.EventDate) AS EventDateTs 
                    FROM 
                            INTRANET_EVENT AS a, 
                            INTRANET_GROUPEVENT AS b 
                    WHERE 
                            b.EventID = a.EventID 
                        AND a.RecordStatus = '1'
                        AND a.isSkipCycle=1
                        AND a.EventDate>='$startDate'
                        AND a.EventDate<='$endDate'";
		    $row1 = $this->returnResultSet($sql);

		    ## Get All Event except Group Event ##
		    $sql = "SELECT 
                            UNIX_TIMESTAMP(EventDate) AS EventDateTs 
                    FROM 
                            INTRANET_EVENT 
                    WHERE 
                            RecordStatus = '1'
                        AND isSkipCycle=1 
                        AND RecordType <>'2'
                        AND EventDate>='$startDate'
                        AND EventDate<='$endDate'";
		    $row2 = $this->returnResultSet($sql);
		    
		    ## Get Any Missing Group Event (Only Have Record In INTRANET_EVENT) ##
		    $sql = "SELECT 
                            UNIX_TIMESTAMP(a.EventDate) AS EventDateTs
                    FROM 
                            INTRANET_EVENT AS a 
                            LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID 
                    WHERE 
                            b.EventID IS NULL 
                        AND a.RecordType = '2' 
                        AND a.RecordStatus = '1'
                        AND a.isSkipCycle=1
                        AND a.EventDate>='$startDate'
                        AND a.EventDate<='$endDate'";

		    $row3 = $this->returnResultSet($sql);
		    
		    $result = array();
		    if (sizeof($row1) != 0)
		    {
		        $result = $row1;
		    }
		    if (sizeof($row2) != 0)
		    {
		        $result = array_merge($result, $row2);
		    }
		    if (sizeof($row3) != 0)
		    {
		        $result = array_merge($result, $row3);
		    }
		    $result = BuildMultiKeyAssoc($result, array('EventDateTs'), array('EventDateTs'), $SingleValue=1);

		    return $result;
		}
		
		// used in: 
		// 1. change everyday timetable for a date, hence change corresponding TimeslotID and time range in booking
		// 2. change timezone timetable
		// 3. add/update/delete special timetable
		function updateBookingTimeslotByTimetable($date, $oldTimetableID, $timetableID)
		{
		    global $intranet_root, $intranet_session_language;
		    include_once($intranet_root."/includes/libebooking.php");
		    
		    $result = array();
		    // update eBooking
		    $libebooking = new libebooking();
		    $oldTimeSlotAssoc = $this->getAllTimeslotByTimetableID($oldTimetableID);
		    $oldTimeSlotAssoc = current($oldTimeSlotAssoc);
		    $newTimeSlotAssoc = $this->getAllTimeslotByTimetableID($timetableID);
		    $newTimeSlotAssoc = current($newTimeSlotAssoc);
		    $inputDateResumeAry = array();      // for resume calendard event InputDate
		    
		    foreach((array)$oldTimeSlotAssoc as $_displayOrder=>$_oldTimeSlotAry) {
		        $_newTimeSlotAry = $newTimeSlotAssoc[$_displayOrder];
		        if ($_newTimeSlotAry) {
		            $_oldTimeSlotID = $_oldTimeSlotAry['TimeSlotID'];
		            $_newTimeSlotID = $_newTimeSlotAry['TimeSlotID'];
		            $_newStartTime = $_newTimeSlotAry['StartTime'];
		            $_newEndTime = $_newTimeSlotAry['EndTime'];
		            
		            $_thisResultAry = $libebooking->updateBookingTimeslotInfo($_oldTimeSlotID, $_newTimeSlotID, $date, $_newStartTime, $_newEndTime);
		            $result['updateBookingInfo_'.$_newTimeSlotID] = $_thisResultAry['result'];
		            if (count($_thisResultAry['changeInputDate'])) {
		                $inputDateResumeAry[] = $_thisResultAry['changeInputDate'];
		            }
		        }
		    }
		    
		    if (count($inputDateResumeAry)) {
		        foreach((array)$inputDateResumeAry as $_inputDateResumeAssoc) {
		            foreach((array)$_inputDateResumeAssoc as $__eventID=>$__shiftSecond) {
		                $sql = "UPDATE CALENDAR_EVENT_ENTRY SET InputDate=DATE_ADD(InputDate,INTERVAL -{$__shiftSecond} SECOND) WHERE EventID='".$__eventID."'";
		                $result['ResumeInputDate_'.$__eventID] = $this->db_db_query($sql);
		            }
		        }
		    }
		    
		    return in_array(false,$result) ? false : true; 
		}
		
	}
	
	
	class TimeSlot extends Timetable {
		var $TimetableID;
		
		function TimeSlot($TimeSlotID=""){
			parent:: libdb();
			
			$this->TableName = 'INTRANET_TIMETABLE_TIMESLOT';
			$this->ID_FieldName = 'TimeSlotID';
			
			if ($TimeSlotID!="")
			{
				$this->ObjectID = $TimeSlotID;
				$this->Get_Self_Info();
			}
		}
		
		function Get_Self_Info()
		{
			$sql = " 	SELECT 
								* 
						FROM
								INTRANET_TIMETABLE_TIMESLOT as timeslotTable
							INNER JOIN
								INTRANET_TIMETABLE_TIMESLOT_RELATION as relationTable ON (timeslotTable.TimeSlotID = relationTable.TimeSlotID)
						WHERE
							timeslotTable.TimeSlotID = '".$this->ObjectID."'
					";
			$resultSet = $this->returnArray($sql);
			$infoArr = $resultSet[0];
			
			foreach ((array)$infoArr as $key => $value)
				$this->{$key} = $value;
				
			$this->TimetableID = $infoArr['TimetableID'];
		}
		
		function Insert_Record($DataArr=array(), $TimetableID='', $isFirstPMSession='')
		{
			if (count($DataArr) == 0 || $TimetableID == '')
				return false;
			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
			}
			
			## set others fields
			# DateInput
			$fieldArr[] = 'DateInput';
			$valueArr[] = 'now()';
			# DateModified
			$fieldArr[] = 'DateModified';
			$valueArr[] = 'now()';
			
			## determine am/pm
			$sql = "SELECT 
							a.TimeSlotID, a.SessionType
					FROM 
							".$this->TableName ." as a
							INNER JOIN INTRANET_TIMETABLE_TIMESLOT_RELATION as b ON (a.TimeslotID = b.TimeSlotID)
					WHERE
							b.TimetableID = '".$TimetableID."'
							And a.StartTime < '".$DataArr['StartTime']."'
					ORDER BY
							a.StartTime
					";
			$resultAry = $this->returnResultSet($sql);
			$numOfResult = count($resultAry);
			$defaultSessionType = $resultAry[$numOfResult-1]['SessionType'];
			if ($defaultSessionType == '') {
				$defaultSessionType = 'am';
			}
			$fieldArr[] = 'SessionType';
			$valueArr[] = "'".$defaultSessionType."'";
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$sql = '';
			$sql .= ' INSERT INTO '.$this->TableName;
			$sql .= ' ( '.$fieldText.' ) ';
			$sql .= ' VALUES ';
			$sql .= ' ( '.$valueText.' ) ';
			
			$success['Insert_TimeSlot'] = $this->db_db_query($sql);
			
			if ($success == false)
			{
				$this->RollBack_Trans();
				return 0;
			}
			else
			{
				$this->Commit_Trans();
				
				# return the inserted TimeSlotID
				$insertedTimeSlotID = $this->Get_Max_ObjectID();
				
				# Add Timetable-Timeslot Relation
				$sql_insert = "	INSERT INTO 
									INTRANET_TIMETABLE_TIMESLOT_RELATION
									(TimetableID, TimeSlotID, DateInput, DateModified)
								VALUES
									('$TimetableID', '$insertedTimeSlotID', now(), now())
							";
				$success['Add_Relation'] = $this->db_db_query($sql_insert);
				
				/*
				# Reorder the display order of the timeslot
				# If I insert a timeslot between TimeSlot1 and TimeSlot2, 
				# TimeSlot1 remains 1, add TimeSlot becomes TimeSlot2, and the TimeSlot2 becomes TimeSlot3
				$thisDisplayOrder = $DataArr['DisplayOrder'];
				$sql_update = "UPDATE ".$this->TableName." 
									SET DisplayOrder = DisplayOrder+1 
									WHERE 
										TimeSlotID != '".$insertedTimeSlotID."' 
									AND
										DisplayOrder >= ".$thisDisplayOrder;
				$success['Update_DisplayOrder'] = $this->db_db_query($sql_update);
				*/
			}
			
			# Re-order the timeslot according to the start time
			$ObjTimeSlot = new TimeSlot($insertedTimeSlotID);
			$success['Reorder'] = $ObjTimeSlot->Reorder_By_StartTime();
			$success['UpdateSessionType'] = $ObjTimeSlot->UpdateSessionType($isFirstPMSession);
			
			return $success;
		}
		
		function Update_Record($DataArr=array(), $isFirstPMSession='')
		{
			if (count($DataArr) == 0)
				return false;
			
			# Build field update values string
			$valueFieldText = '';
			foreach ($DataArr as $field => $value)
			{
				$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
			}
			$valueFieldText .= ' DateModified = now() ';
			
			$sql = '';
			$sql .= ' UPDATE '.$this->TableName;
			$sql .= ' SET '.$valueFieldText;
			$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$this->ObjectID.'\' ';
			$success['Update'] = $this->db_db_query($sql);
			
			$success['Reorder'] = $this->Reorder_By_StartTime();
			$success['UpdateSessionType'] = $this->UpdateSessionType($isFirstPMSession);
			
			if (in_array(false, $success))
				return false;
			else
				return true;
		}
		
		function Delete_Record()
		{
			# Delete TimeSlot Record
			$sql_record = "DELETE FROM ".$this->TableName." WHERE ".$this->ID_FieldName." = '".$this->ObjectID."' ";
			$success['Record'] = $this->db_db_query($sql_record);
			
			# Delete Timetable-TimeSlot Relation
			$sql_relation = "DELETE FROM INTRANET_TIMETABLE_TIMESLOT_RELATION WHERE ".$this->ID_FieldName." = '".$this->ObjectID."' ";
			$success['Relation'] = $this->db_db_query($sql_relation);
			
			$success['Reorder'] = $this->Reorder_By_StartTime();
			
			if (in_array(false, $success))
				return false;
			else
				return true;
		}
		
		function Get_Max_Display_Order()
		{
			$sql = '';
			$sql .= 'SELECT max(DisplayOrder) FROM '.$this->TableName;
			$MaxDisplayOrder = $this->returnVector($sql);
			
			return $MaxDisplayOrder[0];
		}
		
		function Reorder_By_StartTime()
		{
			$sql = "SELECT 
							a.TimeSlotID 
					FROM 
							".$this->TableName ." as a
							INNER JOIN
							INTRANET_TIMETABLE_TIMESLOT_RELATION as b
							ON (a.TimeslotID = b.TimeSlotID)
					WHERE
							b.TimetableID = '".$this->TimetableID."'
					ORDER BY
							a.StartTime
					";
			$TimeSlotIDArr = $this->returnVector($sql);
			$numOfTimeSlot = count($TimeSlotIDArr);
			
			for ($i=0; $i<$numOfTimeSlot; $i++)
			{
				$thisTimeSlotID = $TimeSlotIDArr[$i];
				$thisDisplayOrder = $i + 1;
				
				$sql = "UPDATE ".$this->TableName ." SET DisplayOrder = '".$thisDisplayOrder."' Where ".$this->ID_FieldName." = '".$thisTimeSlotID."'";
				$successArr[$i."_".$thisTimeSlotID] = $this->db_db_query($sql);
			}
			
			return $successArr;
		}
		
		function UpdateSessionType($isFirstPmSession='') {
			if ($isFirstPmSession === '') {
				return true;
			}
			else {
				$successAry = array();
				$startTime = $this->StartTime;
				
				// update timeslot of related timetable only
				$sql = "SELECT 
								a.TimeSlotID 
						FROM 
								".$this->TableName ." as a
								INNER JOIN INTRANET_TIMETABLE_TIMESLOT_RELATION as b ON (a.TimeslotID = b.TimeSlotID)
						WHERE
								b.TimetableID = '".$this->TimetableID."'
						ORDER BY
								a.StartTime
						";
				$timeSlotIdAry = Get_Array_By_Key($this->returnResultSet($sql), 'TimeSlotID');
				
				if ($isFirstPmSession) {
					$sql = "Update ".$this->TableName ." Set SessionType = 'am' Where TimeSlotID IN ('".implode("','", (array)$timeSlotIdAry)."') and StartTime < '".$startTime."'";
					$successAry['updateToAm'] = $this->db_db_query($sql);
					
					$sql = "Update ".$this->TableName ." Set SessionType = 'pm' Where TimeSlotID IN ('".implode("','", (array)$timeSlotIdAry)."') and StartTime >= '".$startTime."'";
					$successAry['updateToPm'] = $this->db_db_query($sql);
				}
				else {
					$sessionType = $this->SessionType;
					if ($sessionType == 'am') {
						$sql = "Update ".$this->TableName ." Set SessionType = 'am' Where TimeSlotID IN ('".implode("','", (array)$timeSlotIdAry)."') and StartTime < '".$startTime."'";
						$successAry['updateToAm'] = $this->db_db_query($sql);
					}
					else if ($sessionType == 'pm') {
						$sql = "Update ".$this->TableName ." Set SessionType = 'pm' Where TimeSlotID IN ('".implode("','", (array)$timeSlotIdAry)."') and StartTime >= '".$startTime."'";
						$successAry['updateToPm'] = $this->db_db_query($sql);
					}
				}
			}
			
			return !in_array(false, (array)$successAry);
		}
		
		function Is_First_PM_Session() {
			$sql = "SELECT
							t.TimeSlotID
					FROM
							".$this->TableName." as t
							Inner Join INTRANET_TIMETABLE_TIMESLOT_RELATION as r ON (t.TimeslotID = r.TimeSlotID)
					WHERE
							r.TimetableID = '".$this->TimetableID."'
							And t.SessionType = 'pm'
					ORDER BY
							t.StartTime
					";
			$resultAry = $this->returnResultSet($sql);
			return ($resultAry[0]['TimeSlotID'] == $this->ObjectID)? true : false;
		}
	}
	
	
	class Room_Allocation extends Timetable {
		var $RoomAllocationID;
		
		function Room_Allocation($RoomAllocationID=""){
			parent:: Timetable();
			
			$this->TableName = 'INTRANET_TIMETABLE_ROOM_ALLOCATION';
			$this->ID_FieldName = 'RoomAllocationID';
			
			if ($RoomAllocationID!="")
			{
				$this->ObjectID = $RoomAllocationID;
				$this->RoomAllocationID = $RoomAllocationID;
				$this->Get_Self_Info();
			}
		}
		
		function Insert_Record($DataArr=array())
		{
			if (count($DataArr) == 0)
				return false;
				
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
			}
			
			## set others fields
			# DateInput
			$fieldArr[] = 'DateInput';
			$valueArr[] = 'now()';
			# DateModified
			$fieldArr[] = 'DateModified';
			$valueArr[] = 'now()';
			# LastModifiedBy
			$fieldArr[] = 'LastModifiedBy';
			$valueArr[] = $_SESSION['UserID'];
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$sql = '';
			$sql .= ' INSERT INTO '.$this->TableName;
			$sql .= ' ( '.$fieldText.' ) ';
			$sql .= ' VALUES ';
			$sql .= ' ( '.$valueText.' ) ';
			$success = $this->db_db_query($sql);
			
			$insertedId = '';
			if ($success == false) {
				$this->RollBack_Trans();
			}
			else {
				$insertedId = $this->db_insert_id();
				$this->Commit_Trans();
			}
				
			return $insertedId;
		}
		
		function Update_Record($DataArr=array())
		{
			if (count($DataArr) == 0)
				return false;
			
			# Build field update values string
			$valueFieldText = '';
			foreach ($DataArr as $field => $value)
			{
				$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
			}
			$valueFieldText .= ' DateModified = now(), ';
			$valueFieldText .= ' LastModifiedBy = \''.$_SESSION['UserID'].'\' ';
			
			$sql = '';
			$sql .= ' UPDATE '.$this->TableName;
			$sql .= ' SET '.$valueFieldText;
			$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$this->ObjectID.'\' ';
			$success = $this->db_db_query($sql);
			
			return $success;
		}
		
		function Get_Related_Room_Allocation_Info() {
			if ($this->RelatedRoomAllocationID == '') {
				$conds_id = " ".$this->ID_FieldName." = '".$this->ObjectID."' ";
			}
			else {
				$conds_id = " ".RelatedRoomAllocationID." = '".$this->RelatedRoomAllocationID."' ";
			}
			$sql = "Select RoomAllocationID, TimeSlotID From ".$this->TableName." Where $conds_id ";
			return $this->returnResultSet($sql);
		}
		
		/*
		function Is_Room_Allocation_Overlapped($TimeSessionID, $CycleDays, $Day, $LocationID, $RoomAllocationID='')
		{
			$cond_RoomAllocationID = '';
			if ($RoomAllocationID != '')
				$cond_RoomAllocationID = " AND RoomAllocationID != '$RoomAllocationID' ";
			
			$sql = "	SELECT
								RoomAllocationID
						FROM
								INTRANET_TIMETABLE_ROOM_ALLOCATION
						WHERE
								TimeSessionID = '$TimeSessionID'
							AND
								CycleDays = '$CycleDays'
							AND
								Day = '$Day'
							AND
								LocationID = '$LocationID'
							$cond_RoomAllocationID
					";
			$resultSet = $this->returnArray($sql);
			
			if (count($resultSet) > 0)
				return 1;
			else
				return 0;
		}
		*/
		
		/*
		function Is_Subject_Group_Overlapped($TimeSessionID, $CycleDays, $Day, $SubjectGroupID, $RoomAllocationID='')
		{
			$cond_RoomAllocationID = '';
			if ($RoomAllocationID != '')
				$cond_RoomAllocationID = " AND RoomAllocationID != '$RoomAllocationID' ";
			
			$sql = "	SELECT
								RoomAllocationID
						FROM
								INTRANET_TIMETABLE_ROOM_ALLOCATION
						WHERE
								TimeSessionID = '$TimeSessionID'
							AND
								CycleDays = '$CycleDays'
							AND
								Day = '$Day'
							AND
								SubjectGroupID = '$SubjectGroupID'
							$cond_RoomAllocationID
					";
			$resultSet = $this->returnArray($sql);
			
			if (count($resultSet) > 0)
				return 1;
			else
				return 0;
		}
		*/

	}

} // End of directives
?>