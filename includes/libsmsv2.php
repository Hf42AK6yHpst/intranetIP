<?php
# using: 


###################### Change Log Start ########################
#	Date:	2015-02-04	Omas
#			Add basic checking to isValidPhoneNumber() even $bypass_phone_validation
#
#	Date:	2014-09-22	Bill
#			Add charge remark to Get_SMS_Fee_Notes()
#
#	Date:	2013-9-26	Roy
#			Add UEMO notes to Get_SMS_Fee_Notes()
#	Date: 	2012-11-30 yuen 
#			add function parsePhoneNumber() to parse phone number to remove unnecessary " " and "-"
#	Date: 	2012-11-13 Rita 
#			add function getSMSStatusMultiSelection()-  for SMS multi selection 
#
#	Date:	2012-02-29	Ivan [2012-0224-1220-47066]
#			updated isValidPhoneNumber() to support mobile number starts with "5" for HK mobile checking
#
#	Date:	2011-11-04	Yuen
#			i. only refresh the message being viewed in message details page
#			ii. only refresh the message which is not in delivered status and within 8 days for TACTUS SMS
#
#	Date:	2010-01-27	YatWoon
#			update replace_content(), add "extra_data" array to allow the content can replaced by specific data value (in_school_time)
#
###################### Change Log End ########################

#########################################################################
# 2008-10-24:  The valid prefix (HK) - 6,9,51,52,54,56,59  (info provided by OFTA)
# 	5(1,3,4,6,9) ��w�@���ʪA�Ȫ����X�αN�ӱ��X�s�A��(�p��)�����X
# 	52,55 �w�d�ѱN�ӹ�I�ιB�@���X�p���ɨϥ�
# 	58 �^�w�q�H�����A��/�T�w�ǰe�̩ΪA��/���ӵP�ӤU�ĤG���A�Ȫ����X
# 	500 �S�O�A�ȹw�d
#########################################################################

if (!defined("LIBSMSV2_DEFINED"))                     // Preprocessor directive
{
	define("LIBSMSV2_DEFINED", true);

	//ORANGE no longer supported
	/*
	 if ($sms_vendor == "ORANGE")
	 {
	 die("Configuration of SMS is wrong. Please contact eClass Technical Support <support@broadlearning.com>");
	 }
	 else if ($sms_vendor == "KANHAN")
	 {
	 include_once("$intranet_root/includes/libkanhansms.php");
	 }
	 else if ($sms_vendor == "CTM")
	 {
	 include_once("$intranet_root/includes/libctmsms.php");
	 }
	 else if ($sms_vendor == "CLICKATELL")
	 {
	 include_once("$intranet_root/includes/libclickatellsms.php");
	 }
	 else
	 {
	 die("Configuration of SMS is wrong. Please contact eClass Technical Support <support@broadlearning.com>");
	 }
	 */

	define("SMS_FR_MODULE_SMS","SMS_MAIN");
	define("SMS_FR_MODULE_ATTENDANCE","SMS_ATTEND");
	define("SMS_FR_MODULE_PAYMENT","SMS_PAYMENT");
	define("SMS_FR_MODULE_ENROLLMENT","SMS_ENROLLMENT");
	define("SMS_PHONE_NUMBER_CHECKING_SCHEME",1); # 1 - HK
	define("SMS_PHONE_NUMBER_CHECKING_SCHEME_MACAU",3); # 3 - MACAU
	define("SMS_REPLY_PHONE_NUMBER_CHECKING_SCHEME",1);  # 1 - KANHAN
	define("SMS_FR_MODULE_ARRIVAL","SMS_ARRIVAL");
	define("SMS_FR_MODULE_LEAVE","SMS_LEAVE");

	class libsmsv2 extends libdb{

		# ------------------------------------------------------------------------------------

		function libsmsv2(){
			//global $sms_vendor;
			//global $intranet_root;

			/*
			 if ($sms_vendor == "KANHAN")
			 {
			 include_once("$intranet_root/includes/libkanhansms.php");
			 $this->vendor_object = new libkanhansms();
			 }
			 else if ($sms_vendor == "CTM")
			 {
			 include_once("$intranet_root/includes/libctmsms.php");
			 $this->vendor_object = new libctmsms();
			 }
			 else if ($sms_vendor == "CLICKATELL")
			 {
			 include_once("$intranet_root/includes/libclickatellsms.php");
			 $this->vendor_object = new libclickatellsms();
			 }
			 else
			 {
			 die("Configuration of SMS is wrong. Please contact eClass Technical Support <support@broadlearning.com>");
			 }
			 */

			$this->libdb();
		}

		function octets2char( $input ) {
			if(!function_exists("octets2char_callback")){
				function octets2char_callback($octets) {
					$dec = $octets[1];
					if ($dec < 128) {
						$utf8Substring = chr($dec);
					} else if ($dec < 2048) {
						$utf8Substring = chr(192 + (($dec - ($dec % 64)) / 64));
						$utf8Substring .= chr(128 + ($dec % 64));
					} else {
						$utf8Substring = chr(224 + (($dec - ($dec % 4096)) / 4096));
						$utf8Substring .= chr(128 + ((($dec % 4096) - ($dec % 64)) / 64));
						$utf8Substring .= chr(128 + ($dec % 64));
					}
					return $utf8Substring;
				}
			}
			return preg_replace_callback('|&#([0-9]{1,});|', 'octets2char_callback', $input );

		}//end function

		# convert the string with octets to represted chars in UTF-8 form
		function utf8Encode2($source) {
			$utf8Str = '';
			$entityArray = explode ("&#", $source);
			$size = count ($entityArray);
			for ($i = 0; $i < $size; $i++) {
				$subStr = $entityArray[$i];
				$nonEntity = strpos ($subStr, ';');
				if ($nonEntity !== false) {
					$str = substr($subStr,0,$nonEntity);
					$str = "&#".$str.";";
					$str_end=substr($subStr,$nonEntity+1);

					//$convertedStr=rawurlencode($this->octets2char($str));
					$convertedStr=$this->octets2char($str);

					$utf8Str.=$convertedStr.$str_end;
				}
				else {
					$utf8Str .= $subStr;
				}
			}
			//return preg_replace('/[:\\x5c\\/*?"<>|]/', '_', $utf8Str);
			return $utf8Str;

		}

		//pre-condition: UTF8 encoded message
		//1. convert all SMS messages special character include '&' '>' '`' '|'  to URL encoded
		//2. convert all html named ssymbol to URL encoded  (URL coded SMS messages found on processing SMS messages -> intranet user function)
		function message_post_processing($message)
		{
			//intranet msg -> url encoded
			// >
			/*
			$post_message=str_replace('&gt;','%3E',$message);
			//&
			$post_message=str_replace('&amp;','%26',$post_message);
			// "
			$post_message=str_replace('&quot;','%22',$post_message);
			// <
			$post_message=str_replace('&lt;','%3C',$post_message);

			*/
			/*
			 // & processing most put in last row before html code process
			 $post_message=str_replace('&','%26',$post_message);

			 //special chinese convert to URl encode
			 // API accept UTF8 urlencoded ���� -> UTF8 (E4 B8 AD E5 9B BD)
			 //�e
			 $post_message=str_replace('�e','%E7%86%BA',$post_message);
			 //��
			 $post_message=str_replace('��','%E9%A4%BB',$post_message);
			 //�]
			 $post_message=str_replace('�]','%E5%B3%AF',$post_message);
			 //�I
			 $post_message=str_replace('�I','%E7%83%B1',$post_message);
			 //��
			 $post_message=str_replace('��','%E7%B3%89',$post_message);
			 //�D
			 $post_message=str_replace('�D','%E9%8D%A9',$post_message);
			 //��
			 $post_message=str_replace('��','%E5%95%93',$post_message);
			 // ¤ (��r����) (common CJK Area, Unicode only
			 $post_message=str_replace('&#26325;','%E6%9B%95',$post_message);
			 */

			//indiv msg -> url encoded
			//all special character strip to html safe
			//$post_message=htmlspecialchars($post_message,ENT_COMPAT,'BIG5');
			/*
			 $post_message=str_replace('>','%3E',$post_message);
			 $post_message=str_replace('`','%60',$post_message);
			 $post_message=str_replace('|','%7C',$post_message);
			 */

			/* UTF-8 already in IP25
			 //cannot use encode and rawurlencode to encode messages
			 //Both ignore / Transit produce &#xxxx code, which enlarge SMS messges to additional 7 words
			 $message=stripslashes(intranet_undo_htmlspecialchars($message));
			 $post_message=iconv("BIG5", "UTF-8//IGNORE", $message);
			 //$post_message=iconv("BIG5", "UTF-8//TRANSLIT", $message);
			 //echo 'ASCII<br>'.$post_message.'<br>';

			 $post_message=$this->utf8Encode2($post_message);
			 //echo 'URLENOCDE<br>'.$post_message.'<br>';
			 */

			$post_message = stripslashes(intranet_undo_htmlspecialchars($message));


			//$post_message=rawurlencode($post_message);

			//do not convert this code as may affect unicode char include '<'
			//$post_message=str_replace('<','%3C',$post_message);

			//last: convert encoding to UTF8
			//$post_message=iconv("BIG5", "UTF-8", $post_message);
			//$post_message=mb_convert_encoding($post_message,"BIG-5","auto");
			//echo mb_detect_encoding($post_message);
			//echo $post_message.'<br>';

			return $post_message;
			//return $message;

		}//end function

		function message_post_processing_clickatell($message)
		{
			$message = stripslashes(intranet_undo_htmlspecialchars($message));

			# UTF-8 already in IP25
			//$post_message = iconv("BIG5", "UTF-8//IGNORE", $message);
			//$post_message=$this->utf8Encode2($post_message);
			$post_message = $message;

			$post_message16 = $this->convert_message_to_utf16($post_message);

			$post_message = str_replace(" ", "", $post_message16);

			# remove the last "0000" if there exists
			if (substr($post_message, strlen($post_message) - 4, 4) == "0000")
			{
				$post_message = substr($post_message, 0, strlen($post_message) - 4);
			}

			return $post_message;

		}//end function


		function convert_message_to_utf16 ( $textString ) {
			$haut = 0;
			$n = 0;
			$CPstring = '';

			# First convert the UTF-8 characters to UTF-8 codes
			# Then convert the UTF-8 codes to CP (Character Point)
			# Finally convert the CP to UTF-16 codes
			# Reference: http://rishida.net/scripts/uniview/conversion.php

			for ($i = 0; $i < strlen($textString); $i++) {
				$b = ord( $textString[$i] );

				if ($b < 0 || $b > 0xFFFF) {
					$CPstring .= 'Error ' . dechex($b) . '!';
				}

				if ($haut != 0) {
					if (0xDC00 <= dechex($b) && dechex($b) <= 0xDFFF) {
						$CPstring .= dechex(0x10000 + ((dechex($haut) - 0xD800) << 10) + (dechex($b) - 0xDC00)) . ' ';
						$haut = 0;
						continue;
					}
					else {
						$CPstring .= '!erreur ' . dechex($haut) . '!';
						$haut = 0;
					}
				}

				if (0xD800 <= dechex($b) && dechex($b) <= 0xDBFF) {
					$haut = $b;
				}
				else {
					$CPstring .= dechex($b) . ' ';
				}
			}

			$CPstring = substr($CPstring, 0, strlen($CPstring)-1);

			return $this->convertUTF82CP( $CPstring );
		}

		function convertUTF82CP ( $textString ) {
			$outputString = "";
			$CPstring = '';
			$compte = 0;
			$n = 0;

			$textString = str_replace("/^\s+/", "", $textString);

			if (strlen($textString) == 0) { return ""; }

			$textString = str_replace("/\s+/g", "", $textString);

			$listArray = explode(" ", $textString);

			for ( $i = 0; $i < count($listArray); $i++ ) {
				$b = hexdec($listArray[ $i ]);

				switch ($compte) {
					case 0:
						if (0 <= $b && $b <= 0x7F) {  // 0xxxxxxx
							$outputString .= dechex($b) . ' ';
						} else if (0xC0 <= $b && $b <= 0xDF) {  // 110xxxxx
							$compte = 1;
							$n = $b & 0x1F;
						} else if (0xE0 <= $b && $b <= 0xEF) {  // 1110xxxx
							$compte = 2;
							$n = $b & 0xF;
						} else if (0xF0 <= $b && $b <= 0xF7) {  // 11110xxx
							$compte = 3;
							$n = $b & 0x7;
						} else {
							$outputString .= '!erreur ' . dechex($b) . '! ';
						}
						break;
					case 1:
						if ($b < 0x80 || $b > 0xBF) {
							$outputString .= '!erreur ' . dechex($b) . '! ';
						}
						$compte--;
						$outputString .= dechex(($n << 6) | ($b-0x80)) . ' ';
						$n = 0;
						break;
					case 2: case 3:
						if ($b < 0x80 || $b > 0xBF) {
							$outputString .= '!erreur ' . dechex($b) . '! ';
						}
						$n = ($n << 6) | ($b-0x80);
						$compte--;
						break;
				}
			}

			$CPstring = str_replace("/ $/", "", $outputString);

			return $this->convertCP2UTF16($CPstring);
		}

		function convertCP2UTF16 ( $textString ) {
			$outputString = "";

			$textString = str_replace("/^\s+/", "", $textString);
			//textString = textString.replace(/^\s+/, '');

			if (strlen($textString) == 0)
			{
				return "";
			}

			$textString = str_replace("/\s+/g", " ", $textString);
			//textString = textString.replace(/\s+/g, ' ');

			$listArray = explode(" ", $textString);

			for ( $i = 0; $i < count($listArray); $i++ ) {
				$n = hexdec($listArray[$i]);

				if ($i > 0) { $outputString .= ' ';}

				if ($n <= 0xFFFF) {
					$outputString .= $this->dec2hex4($n);
				} else if ($n <= 0x10FFFF) {
					$n -= 0x10000;
					$outputString .= $this->dec2hex4(0xD800 | ($n >> 10)) . ' ' . $this->dec2hex4(0xDC00 | ($n & 0x3FF));
				} else {
					$outputString .= '!erreur ' . $n .'!';
				}
			}
			return $outputString;
		}

		function dec2hex4($ts)
		{
			$hexequiv = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
			return $hexequiv[($ts >> 12) & 0xF] . $hexequiv[($ts >> 8) & 0xF] . $hexequiv[($ts >> 4) & 0xF] . $hexequiv[$ts & 0xF];
		}




		/*
		 function Fix_Backslash($org_str)
		 {
		 //if ( mysql_client_encoding() != "big5" ) return $org_str;

		 $tmp_length = strlen($org_str);

		 for ( $tmp_i=0; $tmp_i<$tmp_length; $tmp_i++ ) {
		 $ascii_str_a = substr($org_str, $tmp_i , 1);
		 $ascii_str_b = substr($org_str, $tmp_i+1, 1);

		 $ascii_value_a = ord($ascii_str_a);
		 $ascii_value_b = ord($ascii_str_b);

		 if ( $ascii_value_a > 128 ) {
			if ( $ascii_value_b == 92 ) {
			$org_str = substr($org_str, 0, $tmp_i+2) . substr($org_str,$tmp_i+3);
			$tmp_length = strlen($org_str);
			}
			$tmp_i++;
			}
			}

			$tmp_length = strlen($org_str);
			if ( substr($org_str, ($tmp_length-1), 1) == "\\" ) $org_str .= chr(32);

			$org_str = str_replace("\\0", "\ 0", $org_str);
			return $org_str;
			}//end Fix_Backslash
			*/

		# Param:
		#  message: SMS message
		#  recipientData: Array(phone, userid, name, individual_msg)
		#  targetType: 1 - Number input, 2 - file input, 3 - select intranet user, 4 - Guardian selection
		#  PICType: 1 - Admin Console user, 2 - INTRANET_USER.UserID
		#  AdminPIC: admin console username
		#  UserPIC: UserID
		#  frModule : which module
		#  deliveryTime: scheduled time YYYY-MM-DD HH:mm:ss
		#  isIndividualMessage: the message is different for each user (it uses message in argument recipientData)
		#  isReplyMessage
		function sendSMS($message, $recipientData, $targetType, $picType, $adminPIC, $userPIC, $frModule, $deliveryTime="", $isIndividualMessage=false,$isReplyMessage=false)
		{
			global $sms_vendor, $sms_school_code, $sms_kanhan_host, $sms_kanhan_path, $sms_kanhan_login, $sms_kanhan_passwd;
			global $sms_vendor_for_ReplyMessage;
			global $intranet_root, $smsConfig;


			# Create the different sms vendor object depends on the nature of the message (1-way or 2-way sms)
			if ($isReplyMessage && $sms_vendor_for_ReplyMessage)
			{
				$in_use_sms_vendor = $sms_vendor_for_ReplyMessage;
			}
			else
			{
				$in_use_sms_vendor = $sms_vendor;
			}

			if ($in_use_sms_vendor == "KANHAN")
			{
				include_once("$intranet_root/includes/libkanhansms.php");
				$in_use_vendor_object = new libkanhansms();
			}
			else if ($in_use_sms_vendor == "CTM")
			{
				include_once("$intranet_root/includes/libctmsms.php");
				$in_use_vendor_object = new libctmsms();
			}
			else if ($in_use_sms_vendor == "CLICKATELL")
			{
				include_once("$intranet_root/includes/libclickatellsms.php");
				$in_use_vendor_object = new libclickatellsms();
			}
			else if ($in_use_sms_vendor == "TACTUS")
			{
				include_once("$intranet_root/includes/libtactussms.php");
				$in_use_vendor_object = new libtactussms();
			}
			else
			{
				die("Configuration of SMS is wrong. Please contact eClass Technical Support <support@broadlearning.com>");
			}

			if ($frModule == "") $frModule = SMS_FR_MODULE_SMS;

			if ($isIndividualMessage)
			{
				# Insert source message
				//if the frModule is "SMS_FR_MODULE_ARRIVAL" or "SMS_FR_MODULE_LEAVE", check record exists or not (that day), is yes, no need to insert, but need to insert
				$exists_record = 0;
				if($frModule=="SMS_FR_MODULE_ARRIVAL" or $frModule=="SMS_FR_MODULE_LEAVE")
				{
					$today = date("Y-m-d");
					$sql = "select SourceID from INTRANET_SMS2_SOURCE_MESSAGE where FromModule='$frModule' and date_format(DateInput, '%Y-%m-%d')='$today'";
					$temp = $this->returnArray($sql, 1);
					$tSourceID = $temp[0][0]+0;

					//update RecipientCount in INTRANET_SMS2_SOURCE_MESSAGE
					$sql = "update INTRANET_SMS2_SOURCE_MESSAGE set RecipientCount = RecipientCount + 1 where SourceID=$tSourceID";
					$this->db_db_query($sql);
				}

				if(!$tSourceID)
				{
					$messageType = $isReplyMessage?1:'NULL';
					$deliveryTimeSql = (is_date_empty($deliveryTime))? 'null' : "'$deliveryTime'";
					
					$sql = "INSERT INTO INTRANET_SMS2_SOURCE_MESSAGE
							(Content,RecipientCount,FromModule,PICType,AdminPIC,UserPIC,TargetType,IsIndividualMessage,RecordType,DateInput,DateModified,ScheduleDateTime)
						VALUES
							('$message','".sizeof($recipientData)."', '$frModule','$picType','$adminPIC','$userPIC','$targetType',1,$messageType,NOW(),NOW(),$deliveryTimeSql)";
					$source_success = $this->db_db_query($sql);

					if (!$source_success)
					return false;

					$tSourceID = $this->db_insert_id();
				}

				$t_sql = "	INSERT IGNORE INTO  INTRANET_SMS2_MESSAGE_RECORD
							(SourceMessageID, RecipientPhoneNumber, RecipientName, RelatedUserID, Message, DateInput, DateModified)
						VALUES
					";
				$values = "";
				$delim = "";

				/*
				 if ($sms_vendor == "KANHAN")
				 {
				 $lkanhan = new libkanhansms();
				 } else if ($sms_vendor == "CTM")
				 {
				 $lctm = new libctmsms();
				 }
				 */

				$indRecipientData = array();
				for ($i=0; $i<sizeof($recipientData); $i++)
				{
					list($t_phone,$t_userID, $t_name, $t_indMsg) = $recipientData[$i];
					$t_name = addslashes($t_name);

					# Trim the first zero of the phone number if required
					if ($smsConfig['Trim_First_Zero'])
					{
						$t_phone = $this->Trim_Phone_Number_First_Zeros($t_phone);
					}

					$tempSql = $t_sql." ('$tSourceID', '$t_phone','$t_name','$t_userID','$t_indMsg',now(), NULL)";

					$this->db_db_query($tempSql);

					$t_recordID = $this->db_insert_id();


					if($isReplyMessage){
						$replyMessageSql ="INSERT INTO INTRANET_SMS2_REPLY_MESSAGE (MessageID,MessageCode,DateInput,DateModified) VALUES('$tSourceID','$t_recordID',NOW(),NOW())";
						$this->db_db_query($replyMessageSql);
					}

					if ($in_use_sms_vendor == "CLICKATELL")
					{
						$post_t_indMsg = $this->message_post_processing_clickatell($t_indMsg);
					}
					else
					{
						$post_t_indMsg = $this->message_post_processing($t_indMsg);
					}

					$indRecipientData[] = array($t_phone,$post_t_indMsg,$t_recordID,$deliveryTime);
				}

				# Send SMS request to Service Provider
				if ($in_use_sms_vendor == "KANHAN")
				{
					//$response_objects = $lkanhan->sendSMS($t_phone, $t_indMsg, $t_recordID,$deliveryTime);
					//$response_objects_list = $this->vendor_object->sendBulkSMS($indRecipientData,$isReplyMessage);
					$response_objects_list = $in_use_vendor_object->sendBulkSMS($indRecipientData,$isReplyMessage);

					for($p=0;$p<sizeof($response_objects_list);$p++)
					{
						$response_objects = $response_objects_list[$p];
						if ($response_objects == "-100" || $response_objects == "-3" || $response_objects == "-2" || $response_objects == "-1")
						{
							return false;
						}
						if ($response_objects == "SIMULATE")
						{
						}
						else
						{
							# Set reference id to DB
							for ($x=0; $x<sizeof($response_objects); $x++)
							{
								$t_refID = $response_objects[$x]->reference_id;
								$t_recipient = $response_objects[$x]->recipient;
								$t_messageCode = $response_objects[$x]->message_code;
								$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
									SET ReferenceID = '$t_refID'
									WHERE SourceMessageID = '$tSourceID' AND RecipientPhoneNumber = '$t_recipient'
											AND RecordID = '$t_messageCode'";

								$this->db_db_query($sql);
							}
						}
					}
				}
				else if  ($in_use_sms_vendor == "CTM")
				{
					//$response_objects = $lkanhan->sendSMS($t_phone, $t_indMsg, $t_recordID,$deliveryTime);
					$response_objects_list = $in_use_vendor_object->sendBulkSMS($indRecipientData,$isReplyMessage);

					if ($response_objects_list == "-99")
					{
						# record all sms status as cannot connect to sms gateway
						$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
								SET RecordStatus = '$response_objects_list'
								WHERE SourceMessageID = '$tSourceID'";
						$success = $this->db_db_query($sql);
						return -99;
					}


					for($p=0;$p<sizeof($response_objects_list);$p++)
					{
						$response_objects = $response_objects_list[$p];
						//error code defined by CTM
						// error code
						// -1 wrong school code
						// -2 wrong login id / password
						// -4 account locaed
						// -11 invalid message content
						// -12 invalid delivery time format
						// -13 invalid char field
						// -14 invalid recipient field
						// -15 not allow long sms
						// -100 internet error, please contact support

						if ($response_objects == "-1" || $response_objects == "-2" || $response_objects == "-4" || $response_objects == "-11" || $response_objects == "-12" || $response_objects == "-13" || $response_objects == "-14" || $response_objects == "-15" || $response_objects == "-99" || $response_objects == "-100")
						{

							list($t_phone,$t_userID, $t_name, $t_indMsg) = $recipientData[$p];
							$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
								SET RecordStatus = '$response_objects'
								WHERE SourceMessageID = '$tSourceID' AND RecipientPhoneNumber = '$t_phone'";
							$success = $this->db_db_query($sql);

							return $response_objects;
						}
						if ($response_objects == "SIMULATE")
						{
						}
						else
						{
							# Set reference id to DB
							for ($x=0; $x<sizeof($response_objects); $x++)
							{
								$t_refID = $response_objects[$x]->reference_id;
								$t_recipient = $response_objects[$x]->recipient;
								$t_messageCode = $response_objects[$x]->message_code;
								$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
									SET ReferenceID = '$t_refID'
									WHERE SourceMessageID = '$tSourceID' AND RecipientPhoneNumber = '$t_recipient'";

								$this->db_db_query($sql);

							}
						}
					}
				} //end if CTM
				else if  ($in_use_sms_vendor == "CLICKATELL")	#Gary - 20090305
				{
					//$response_objects = $lkanhan->sendSMS($t_phone, $t_indMsg, $t_recordID,$deliveryTime);
					$response_objects_list = $in_use_vendor_object->sendBulkSMS($indRecipientData,$isReplyMessage);

					for($p=0;$p<sizeof($response_objects_list);$p++)
					{
						$response_objects = $response_objects_list[$p];
						//error code defined by CTM
						// error code
						// -1 wrong school code
						// -2 wrong login id / password
						// -4 account locaed
						// -11 invalid message content
						// -12 invalid delivery time format
						// -13 invalid char field
						// -14 invalid recipient field
						// -15 not allow long sms
						// -100 internet error, please contact support
						if ($response_objects == "-1" || $response_objects == "-2" || $response_objects == "-4" || $response_objects == "-11" || $response_objects == "-12" || $response_objects == "-13" || $response_objects == "-14" || $response_objects == "-15" || $response_objects == "-100")
						{
							return false;
						}

						if ($response_objects == "SIMULATE")
						{
						}
						else
						{

							# Set reference id to DB
							for ($x=0; $x<sizeof($response_objects); $x++)
							{
								$t_refID = $response_objects[$x]->reference_id;
								$t_recipient = $response_objects[$x]->recipient;
								$t_messageCode = $response_objects[$x]->message_code;
								$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
									SET ReferenceID = '$t_refID'
									WHERE SourceMessageID = '$tSourceID' AND RecipientPhoneNumber = '$t_recipient'
										AND RecordID = '$t_messageCode'";

								$this->db_db_query($sql);
							}
						}
					}
				}//end else if
				else if  ($in_use_sms_vendor == "TACTUS")	#Simon 20101015
				{


					//$response_objects = $lkanhan->sendSMS($t_phone, $t_indMsg, $t_recordID,$deliveryTime);
					$response_objects_list = $in_use_vendor_object->sendBulkSMS($indRecipientData,$isReplyMessage);



					if ($response_objects_list == "-99")
					{
						# record all sms status as cannot connect to sms gateway
						$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
								SET RecordStatus = '$response_objects_list'
								WHERE SourceMessageID = '$tSourceID'";
						$success = $this->db_db_query($sql);
						return -99;
					}//end if
					//print_r($response_objects_list);

					foreach ($response_objects_list as $key => $response_objects)
					{
						$t_refID = $response_objects->reference_id;
						$t_recipient = $response_objects->recipient;
						$t_messageCode = $response_objects->message_code;


						$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
									SET ReferenceID = '$t_refID',RecordStatus=1
									WHERE recordid = '$t_messageCode'";
						//   echo $sql.'<br>';

						$this->db_db_query($sql);
					}//end foreach
					/*

					for($p=0;$p<sizeof($response_objects_list);$p++)
					{
					$response_objects = $response_objects_list[$p];

					# Set reference id to DB
					$t_refID = $response_objects->reference_id;
					$t_recipient = $response_objects->recipient;
					$t_messageCode = $response_objects->message_code;


					$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
					SET ReferenceID = '$t_refID'
					WHERE SourceMessageID = '$tSourceID'";
					$this->db_db_query($sql);









					}//end for
					*/


				}//end else if

				########################################

			}
			else //not indivdual messages	#DE1
			{
				# Insert source message
				$messageType = $isReplyMessage?1:'NULL';
				$deliveryTimeSql = (is_date_empty($deliveryTime))? 'null' : "'$deliveryTime'";
				
				$sql = "INSERT INTO INTRANET_SMS2_SOURCE_MESSAGE
						(Content,RecipientCount,FromModule,PICType,AdminPIC,UserPIC,TargetType,IsIndividualMessage,RecordType,DateInput,DateModified,ScheduleDateTime)
					VALUES
						('$message','".sizeof($recipientData)."', '$frModule','$picType','$adminPIC','$userPIC','$targetType',0,$messageType,NOW(),NOW(),$deliveryTimeSql)";
				$source_success = $this->db_db_query($sql);
				if (!$source_success)
				return false;

				$tSourceID = $this->db_insert_id();

				$sql = "INSERT IGNORE INTO INTRANET_SMS2_MESSAGE_RECORD
						(SourceMessageID, RecipientPhoneNumber, RecipientName, RelatedUserID, Message, DateInput, DateModified)
					VALUES
					";

				$values = "";
				$delim = "";
				for ($i=0; $i<sizeof($recipientData); $i++)
				{
					list($t_phone,$t_userID, $t_name, $t_indMsg) = $recipientData[$i];

					# Trim the first zero of the phone number if required
					if ($smsConfig['Trim_First_Zero'])
					{
						$t_phone = $this->Trim_Phone_Number_First_Zeros($t_phone);
					}

					$list_phone .= $delim.$t_phone;
					$list_msgCode .= $delim.$t_msgCode;
					$t_name = addslashes($t_name);
					$values .= "$delim ('$tSourceID', '$t_phone','$t_name', '$t_userID','$message',
								now(),NULL)";
					$delim = ",";
				}

				# Process DB records
				$sql .= $values;
				$success = $this->db_db_query($sql);
				if (!$success)
				return false;

				# Get back message code
				$list_phone = "";
				$list_msgCode = "";
				$delim = "";
				$sql = "SELECT RecipientPhoneNumber, RecordID FROM INTRANET_SMS2_MESSAGE_RECORD WHERE SourceMessageID = '$tSourceID'";
				$data = $this->returnArray($sql,2);

				$replyMessageRecords = "";

				for ($i=0; $i<sizeof($data); $i++)
				{
					list($t_phone, $t_msgCode) = $data[$i];
					$list_phone .= $delim.$t_phone;
					$list_msgCode .= $delim.$t_msgCode;
					if($isReplyMessage){
						$replyMessageRecords.=$delim."('$tSourceID','$t_msgCode',NOW(),NOW())";
					}
					$delim = ",";
				}

				## insert Reply Message Records
				if($isReplyMessage){
					$sql = "INSERT INTO INTRANET_SMS2_REPLY_MESSAGE (MessageID,MessageCode,DateInput,DateModified) VALUES $replyMessageRecords ";
					$success = $this->db_db_query($sql);
					if (!$success)
					return false;
				}

				# Send SMS request to Service Provider
				if ($in_use_sms_vendor == "KANHAN")
				{
					//$lkanhan = new libkanhansms();

					$post_message=$this->message_post_processing($message);

					$response_objects = $in_use_vendor_object->sendSMS($list_phone, $post_message, $list_msgCode,$deliveryTime,$isReplyMessage);
					if ($response_objects == "-100" || $response_objects == "-3" || $response_objects == "-2" || $response_objects == "-1")
					{
						return false;
					}

					if ($response_objects == "SIMULATE")
					{
					}
					else
					{
						# Set reference id to DB
						for ($i=0; $i<sizeof($response_objects); $i++)
						{
							$t_refID = $response_objects[$i]->reference_id;
							$t_recipient = $response_objects[$i]->recipient;
							$t_messageCode = $response_objects[$i]->message_code;
							$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
								SET ReferenceID = '$t_refID'
								WHERE SourceMessageID = '$tSourceID' AND RecipientPhoneNumber = '$t_recipient'
									AND RecordID = '$t_messageCode'";
							$this->db_db_query($sql);
						}
					}
				}
				else if ($in_use_sms_vendor == "CTM")
				{
					//$lctm = new libctmsms();
					$post_message=$this->message_post_processing($message);
					$response_objects = $in_use_vendor_object->sendSMS($list_phone, $post_message, $list_msgCode,$deliveryTime,$isReplyMessage);

					//error code defined by CTM
					// error code
					// -1 wrong school code
					// -2 wrong login id / password
					// -4 account locaed
					// -11 invalid message content
					// -12 invalid delivery time format
					// -13 invalid char field
					// -14 invalid recipient field
					// -15 not allow long sms
					// -99 unable to connect SMS Gateway
					// -100 internet error, please contact support
					//echo "======================".print_r($response_objects)."=============";

					//the error code return to eclass for error messages display
					if ($response_objects == "-1" || $response_objects == "-2" || $response_objects == "-4" || $response_objects == "-11" || $response_objects == "-12" || $response_objects == "-13" || $response_objects == "-14" || $response_objects == "-15" || $response_objects == "-99" || $response_objects == "-100")
					{
						//if ($response_objects == "-99")
						//	echo "Unable to connect SMS Gateway";

						$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
							SET RecordStatus = '$response_objects'
							WHERE SourceMessageID = '$tSourceID'";
						$success = $this->db_db_query($sql);

						return $response_objects;
					}

					if ($response_objects == "SIMULATE")
					{
					}
					else //if not simulate
					{
						# Set reference id to DB
						for ($i=0; $i<sizeof($response_objects); $i++)
						{
							$t_refID = $response_objects[$i]->reference_id;
							$t_recipient = $response_objects[$i]->recipient;
							$t_messageCode = $response_objects[$i]->message_code;

							/*
							 $sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
							 SET ReferenceID = '$t_refID'
							 WHERE SourceMessageID = '$tSourceID' AND RecipientPhoneNumber = '$t_recipient'
							 AND RecordID = '$t_messageCode'";

							 */

						 $sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
									SET ReferenceID = '$t_refID'
									WHERE SourceMessageID = '$tSourceID' AND RecipientPhoneNumber = '$t_recipient'";


						 $this->db_db_query($sql);

						}//end for

					}//end else
				} //end if CTM module
				else if ($in_use_sms_vendor == "CLICKATELL")	#Gary - 20090305
				{
					//$lctm = new libctmsms();

					$post_message=$this->message_post_processing_clickatell($message);

					$response_objects = $in_use_vendor_object->sendSMS($list_phone, $post_message, $list_msgCode,$deliveryTime,$isReplyMessage);
					//error code defined by CTM
					// error code
					// -1 wrong school code
					// -2 wrong login id / password
					// -4 account locaed
					// -11 invalid message content
					// -12 invalid delivery time format
					// -13 invalid char field
					// -14 invalid recipient field
					// -15 not allow long sms
					// -100 internet error, please contact support
					if ($response_objects == "-1" || $response_objects == "-2" || $response_objects == "-4" || $response_objects == "-11" || $response_objects == "-12" || $response_objects == "-13" || $response_objects == "-14" || $response_objects == "-15" || $response_objects == "-100")
					{
						return false;
					}

					if ($response_objects == "SIMULATE")
					{
					}
					else
					{
						# Set reference id to DB
						for ($i=0; $i<sizeof($response_objects); $i++)
						{
							$t_refID = $response_objects[$i]->reference_id;
							$t_recipient = $response_objects[$i]->recipient;
							$t_messageCode = $response_objects[$i]->message_code;
							$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
								SET ReferenceID = '$t_refID'
								WHERE SourceMessageID = '$tSourceID' AND RecipientPhoneNumber = '$t_recipient'
									AND RecordID = '$t_messageCode'";
							$this->db_db_query($sql);
						}
					}
				}//end clickatell

				//test code in here
				else if  ($in_use_sms_vendor == "TACTUS")	#Simon 20101015
				{
					
					# IMPORTANT!!!
					$tactus_cut_number = 30;
					$post_message=$this->message_post_processing($message);



					//================split recepient here (maxmium multiple recipient in single HTTP API is 35 telephone numbers)
					$list_phone_pieces = explode(",",$list_phone);
					$j=0;
					$i=0;
					foreach ($list_phone_pieces as $list_phone_pieces_value)
					{
						$list_phone_array[$j].=$list_phone_pieces_value.',';
						$i++;

						if ($i>=$tactus_cut_number)
						{
							//reset counter
							$i=0;
							//remove tail ','
							$list_phone_array[$j]=substr($list_phone_array[$j], 0, -1);
							//next telephone string
							$j++;
						}//end if
					}//end foreach

					//remove tail ','
					$list_phone_array[$j]=substr($list_phone_array[$j], 0, -1);
					//==========================split recepient end


					$batch_ii = 0;
					foreach ($list_phone_array as $list_phone)
					{
							
						$code_arr = explode(",", $list_msgCode);
						$list_msgCode_batch = "";
						for ($kkk=$batch_ii*$tactus_cut_number; $kkk<($batch_ii+1)*$tactus_cut_number; $kkk++)
						{
							if ($kkk >= sizeof($code_arr))
							{
								break;
							}
							$list_msgCode_batch .= (($list_msgCode_batch!="") ? "," : "") . $code_arr[$kkk];
						}
						$response_objects = $in_use_vendor_object->sendSMS($list_phone, $post_message, $list_msgCode_batch,$deliveryTime,$isReplyMessage);

						//messge code must be come back to here
						//have tsourceid
						//no response_objects_list
						//have object


						for ($i=0; $i<sizeof($response_objects); $i++)
						{
							$t_refID = $response_objects[$i]->reference_id;
							$t_recipient = $response_objects[$i]->recipient;
							$t_messageCode = $response_objects[$i]->message_code;
							$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD
							SET ReferenceID = '$t_refID', RecordStatus=1
							WHERE RecordID = '$t_messageCode'";
							$this->db_db_query($sql);
						}//end for
						
						$batch_ii ++;

					}//end foreach

				}//end else if tactus

			}

			return true;
		}

		# ------------------------------------------------------------------------------------

		function returnRecipientIDArray($Recipient){
			$GroupIDList = "";
			$UserIDsList = "";
			$ParentStudentIDsList = "";
			$ParentGroupIDsList = "";
			$group_delimiter = "";
			$user_delimiter = "";
			$parent_student_delim = "";
			$parent_group_delim = "";
			$row = explode(",",$Recipient);
			for($i=0; $i<sizeof($row); $i++){
				$RecipientType = substr($row[$i],0,1);
				$RecipientID = substr($row[$i],1);
				if($RecipientType=="G")    # Group
				{
					$GroupIDList .= $group_delimiter.$RecipientID;
					$group_delimiter = ",";
				}
				else if ($RecipientType=="Q")       # Parents of the students in group
				{
					$ParentGroupIDsList .= $parent_group_delim.$RecipientID;
					$parent_group_delim = ",";
				}
				else if ($RecipientType=="P")    # Parents of the students
				{
					$ParentStudentIDsList .= $parent_student_delim.$RecipientID;
					$parent_student_delim = ",";
				}
				else # Target User
				{
					$UserIDsList .= $user_delimiter.$RecipientID;
					$user_delimiter = ",";
				}
			}
			$x[0] = ($GroupIDList == ""? 0:$GroupIDList);
			$x[1] = ($UserIDsList == ""? 0:$UserIDsList);
			$x[2] = ($ParentGroupIDsList == ""? 0:$ParentGroupIDsList);
			$x[3] = ($ParentStudentIDsList == ""? 0:$ParentStudentIDsList);
			return $x;
		}

		function returnRecipientUserIDArray($Recipient){

			$row = $this->returnRecipientIDArray($Recipient);
			$GroupIDList = $row[0];
			$UserIDsList = $row[1];
			# Method in 2.x
			# Select UserIDs separately from group and user basis
			# then merge the 2 arrays

			# Group
			$username_field = $this->getSMSIntranetUserNameField("a.");
			if ($GroupIDList!="")
			{
				$groupList = $GroupIDList;
				$sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',a.RecordType)
                      FROM INTRANET_USER as a, INTRANET_USERGROUP as b
                      WHERE a.UserID = b.UserID
                      AND b.GroupID IN ($groupList)";
				$groupNames = $this->returnVector($sql);
			}
			else
			{
				$groupNames = array();
			}

			if ($UserIDsList!="")
			{
				$userList = $UserIDsList;
				$sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',a.RecordType)
                      FROM INTRANET_USER as a
                      WHERE a.UserID IN ($userList)";
				$userNames = $this->returnVector($sql);
			}
			else
			{
				$userNames = array();
			}

			if ($UserIDsList=="" && $GroupIDList=="") return array();

			# Take Union of arrays
			$overall = array_union($userNames,$groupNames);

			# Separate UserIDs and Names
			while (list ($key, $val) = each ($overall))
			{
				$row = explode(":::",$val);
				$result[] = $row;
			}
			return $result;

		}

		//$sms_phone_number_checking_scheme define in plugins/sms_conf.php
		//not define => HK
		//define => 1-HK 2-MALAY or 3-MACAU
		function isValidPhoneNumber($phone){

			global $sms_phone_number_checking_scheme;
			global $bypass_phone_validation;

			if ($bypass_phone_validation){
				//simple checking 
				if($phone == ''){
					return false;
				}
				
				if(!preg_match('/^([0-9]+)$/',$phone)){
					return false;
				}
				
				if(strlen($phone) < 8){
					return false;
				}
				return true;
			}

			$digit = array("+","0","1","2","3","4","5","6","7","8","9");
			for($i=0;$i<strlen($phone);$i++){
				if(!in_array(substr($phone,$i,1),$digit))
				return false;
			}

			if(isset($sms_phone_number_checking_scheme) && is_array($sms_phone_number_checking_scheme) )
			{
				for($i=0;$i<sizeof($sms_phone_number_checking_scheme);$i++){

					//HK
					if ($sms_phone_number_checking_scheme[$i] == SMS_PHONE_NUMBER_CHECKING_SCHEME){

						//not accept local number don't have 8 digit
						if(strlen($phone)!=8){
							return false;
						}

						$flag = false;
						
						// 2012-0224-1220-47066
						//$key = array(6,9);
						$key = array(6,9,5);
						//not accept phone number not start with 6,9
						if(in_array(substr($phone,0,1),$key)){
							$flag = true;
						}
						
						// 2012-0224-1220-47066
						//$key = array(51,52,54,56,59);
						////not accept phone number not start with 51,52,54,56,59
						//if(in_array(substr($phone,0,2),$key)){
						//	$flag = true;
						//}

						return $flag;
					}//end if

					//MACAU
					else if ($sms_phone_number_checking_scheme[$i] == SMS_PHONE_NUMBER_CHECKING_SCHEME_MACAU){

						//not accept local number less than 8 digit
						if(strlen($phone)< 8){
							return false;
						}
						//Note: local fix number number is 28xx xxxx
						//Note: local cell phone number is 6xxx xxxx
						//all 8 digit mobile number should start with 6, else reject
						if((strlen($phone)== 8) && (substr($phone,0,1)!='6')){
							return false;
						}

						//Test: support international phone number (to HK only?)

						return true;
					}//end if

				}//end for
			}//end if
			//
			//use HK scheme for fall back scheme if sms_conf.php not define  sms_phone_number_checking_scheme
			else {

				//HK Scheme
				//not accept local number don't have 8 digit
				if(strlen($phone)!=8){
					return false;
				}

				$flag = false;
				
				// 2012-0224-1220-47066
				//$key = array(6,9);
				$key = array(6,9,5);
				//not accept phone number not start with 6,9
				if(in_array(substr($phone,0,1),$key)){
					$flag = true;
				}
				
				// 2012-0224-1220-47066
				//$key = array(51,52,54,56,59);
				////not accept phone number not start with 51,52,54,56,59
				//if(in_array(substr($phone,0,2),$key)){
				//	$flag = true;
				//}

				return $flag;
			}//end else

			return false;
		}//end function

		# get name field of the INTRANET_USER according to $intranet_default_lang
		function getSMSIntranetUserNameField($prefix=""){
			global $intranet_default_lang;

			$chi = ($intranet_default_lang =="b5" || $intranet_default_lang == "gb");
			if ($chi)
			{
				$firstChoice = "ChineseName";
				$altChoice = "EnglishName";
			}
			else
			{
				$firstChoice = "EnglishName";
				$altChoice = "ChineseName";
			}
			$username_field = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice)";
			//$field = "IF($prefix"."RecordType=4,CONCAT($username_field,'(',$prefix"."YearOfLeft,'-',$prefix"."ClassName,')') , CONCAT($username_field,IF($prefix"."ClassNumber IS NULL OR $prefix"."ClassNumber = '','',CONCAT(' (',$prefix"."ClassName,'-',$prefix"."ClassNumber,')'))) )";
			//return $field;
			return $username_field;
		}

		# return Guardian User Name field according to $intranet_default_lang
		function getSMSGuardianUserNameField($prefix=""){
			global $intranet_default_lang;
			if($intranet_default_lang=="en")
			return $prefix."EnName";
			else return $prefix."ChName";
		}


		function getTemplateTypeSelect($tags,$selected, $FirstOnly=0)
		{
			global $i_SMS_NormalTemplate, $i_SMS_SystemTemplate, $plugin;

			if($plugin['attendancestudent'] or $plugin['payment'])
			{
				$templateType = array(
				array(1,$i_SMS_NormalTemplate),
				array(2,$i_SMS_SystemTemplate)
				);
			}
			else
			{
				$templateType = array(
				array(1,$i_SMS_NormalTemplate)
				);
			}

			if($FirstOnly)
			{
				$templateType = array(
				array(1,$i_SMS_NormalTemplate)
				);
			}

			return getSelectByArray($templateType,$tags,$selected, 0 ,1);
		}

		function returnTagAry()
		{
			$tags = "name=Tag";
			return getSelectByArray($this->MsgTagAry(),$tags,1, 0 ,1);

		}

		function returnNonExistsTemplateAry($returnSizeOnly=0)
		{
			global $i_SMS_Personalized_Msg_Template, $TemplateAry;

			$ary = array();
			$TemplateAry = $this->TemplateAry();

			foreach ($TemplateAry as $tempAry)
			{
				$sql = "select count(TemplateID) from INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE where TemplateCode='". $tempAry[0] ."'";
				$temp = $this->returnArray($sql, 1);
				if(!$temp[0][0])	$ary[]=$tempAry;
			}
			if($returnSizeOnly)
			{
				return sizeof($ary);
			}
			else
			{
				$tags = "name=TemplateCode";
				return getSelectByArray($ary,$tags,1, 0 ,1);
			}

		}

		function returnSystemMsgArray()
		{
			$sql = "SELECT TemplateCode, TemplateCode FROM INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE";
			$temp = $this->returnArray($sql, 1);

			$tags = "name=TemplateCode";
			return getSelectByArray($temp,$tags,1, 0 ,1);
		}

		function returnSystemMsg($TemplateCode)
		{
			$sql = "SELECT Content FROM INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE where TemplateCode='$TemplateCode'";
			$temp = $this->returnArray($sql, 1);

			return $temp[0][0];
		}

		function returnTemplateName($TemplateCode)
		{
			$TemplateAry = $this->TemplateAry();
			return $TemplateAry[$TemplateCode][1];
		}

		function returnTemplateStatus($TemplateID="", $TemplateCode="")
		{
			if($TemplateID)
			$sql = "SELECT RecordStatus FROM INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE where TemplateID='$TemplateID'";
			else
			$sql = "SELECT RecordStatus FROM INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE where TemplateCode='$TemplateCode'";

			$temp = $this->returnArray($sql, 1);

			return $temp[0][0];
		}

		function replace_content($student_id, $msg, $extra_data=null)
		{
			global $intranet_root, $lu;

			$TagAry = $this->MsgTagAry();

			for($i=0;$i<sizeof($TagAry);$i++)
			{
				$this_word = "($".$TagAry[$i][0].")";

				# check data in $extra_data array first
				$this_extra_word = $TagAry[$i][0];
				if(isset($extra_data[$this_extra_word]))
				{
					$this_extra_word_value = trim($extra_data[$this_extra_word]) ? trim($extra_data[$this_extra_word]) : "-";
					$msg = str_replace($this_word, $this_extra_word_value, $msg);
				}
				else
				{
					$msg = str_replace($this_word, $this->returnUserData($student_id, $TagAry[$i][2]), $msg);
				}
			}

			return $msg;
		}

		function returnUserData($user_id, $field)
		{
			global $intranet_root;

			$x = "";

			if(substr($field, 0, 14)=="INTRANET_USER.")
			{
				$sql = "SELECT {$field} FROM INTRANET_USER WHERE UserID='".$user_id."'";
				$result = $this->returnVector($sql);
				$x = $result[0];
			}

			if(substr($field, 0, 5)=="Now()")
			{
				$x = date("Y-m-d H:i:s");
			}

			if(substr($field, 0, 16)=="PAYMENT_ACCOUNT.")
			{
				$sql = "SELECT {$field} FROM PAYMENT_ACCOUNT WHERE StudentID='".$user_id."'";
				$result = $this->returnVector($sql);
				$x = $result[0];
			}

			if($field=="payment_outstanding_amount")
			{
				include_once("$intranet_root/includes/libpayment.php");
				$lp = new libpayment();
				$x = $lp->getStudentOutstandingAmt($user_id);
			}

			return $x;
		}

		function MsgTagAry()
		{
			global $i_SMS_Personalized_Tag;

			$TagAry		= array();
			$TagAry[] 	= array("user_name_eng",	$i_SMS_Personalized_Tag['user_name_eng'],						"INTRANET_USER.EnglishName");
			$TagAry[]	= array("user_name_chi",	$i_SMS_Personalized_Tag['user_name_chi'],						"INTRANET_USER.ChineseName");
			$TagAry[]	= array("user_class_name",	$i_SMS_Personalized_Tag['user_class_name'],						"INTRANET_USER.ClassName");
			$TagAry[]	= array("user_class_number",$i_SMS_Personalized_Tag['user_class_number'],					"INTRANET_USER.ClassNumber");
			$TagAry[]	= array("user_login",		$i_SMS_Personalized_Tag['user_login'],							"INTRANET_USER.UserLogin");
			$TagAry[]	= array("attend_card_time",	$i_SMS_Personalized_Tag['attend_card_time'],					"Now()");
			$TagAry[]	= array("payment_balance",	$i_SMS_Personalized_Tag['payment_balance'],						"PAYMENT_ACCOUNT.Balance");
			$TagAry[]	= array("payment_outstanding_amount", $i_SMS_Personalized_Tag['payment_outstanding_amount'],"payment_outstanding_amount");

			return $TagAry;
		}

		function TemplateAry()
		{
			global $i_SMS_Personalized_Msg_Template, $plugin;

			$TemplateAry	= array();
			if($plugin['attendancestudent'])
			{
				$TemplateAry['STUDENT_ATTEND_ABSENCE'] 		= array("STUDENT_ATTEND_ABSENCE",	$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_ABSENCE']);
				$TemplateAry['STUDENT_ATTEND_LATE'] 		= array("STUDENT_ATTEND_LATE",		$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_LATE']);
				$TemplateAry['STUDENT_ATTEND_EARLYLEAVE'] 	= array("STUDENT_ATTEND_EARLYLEAVE",$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_EARLYLEAVE']);
				$TemplateAry['STUDENT_ATTEND_ARRIVAL'] 		= array("STUDENT_ATTEND_ARRIVAL",	$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_ARRIVAL']);
				$TemplateAry['STUDENT_ATTEND_LEAVE'] 		= array("STUDENT_ATTEND_LEAVE",		$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_LEAVE']);
			}

			if($plugin['payment'])
			{
				$TemplateAry['STUDENT_MAKE_PAYMENT'] 		= array("STUDENT_MAKE_PAYMENT",		$i_SMS_Personalized_Msg_Template['STUDENT_MAKE_PAYMENT']);
				$TemplateAry['BALANCE_REMINDER'] 			= array("BALANCE_REMINDER",			$i_SMS_Personalized_Msg_Template['BALANCE_REMINDER']);
			}

			return $TemplateAry;
		}

		function sendSMS_student($student_id, $template_code, $frModule="")
		{
			global $bl_sms_version, $PHP_AUTH_USER, $intranet_root, $eclass_db;

			if($bl_sms_version==2)
			{
				include_once("$intranet_root/includes/libuser.php");
				$lu = new libuser($student_id);
				# check is student or not
				if($lu->isStudent())
				{

					$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
					$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";
					$namefield2 		= $this->getSMSGuardianUserNameField("");

					$sql = "
						SELECT
						$main_content_field,
								Relation,
								Phone,
								EmPhone,
								IsMain,
								$namefield2
						FROM
						$eclass_db.GUARDIAN_STUDENT
						WHERE
								UserID = $student_id and
								isSMS = 1
						ORDER BY
								IsMain DESC, Relation ASC
					";
						$temp = $this->returnArray($sql);

						// guardian info
						for($j=0;$j<sizeof($temp);$j++)
						{
							# phone / emergency Phone
							$temp[$j][2] = $this->parsePhoneNumber($temp[$j][2]);
							$temp[$j][3] = $this->parsePhoneNumber($temp[$j][3]);
							$p  		= $this->isValidPhoneNumber($temp[$j][2]) ? $temp[$j][2] : "";
							$e  		= $this->isValidPhoneNumber($temp[$j][3]) ? $temp[$j][3] : "";
							$sms_phone 	= ($p!="") ? $p : $e;

							if($sms_phone)
							{
								$mobile 	= ($p!="") ? $p : $e;
								$guardianName	= $temp[$j][5];
								break;
							}
						}

						//Check Guardian
						if(sizeof($temp)==0)
						{
							$mobile					= "";
						}

						$msg				= trim($this->returnSystemMsg($template_code));
						if($msg and $mobile)
						{
							$this_msg 		= $this->replace_content($student_id, $msg);

							$recipientData[] = array($mobile, $student_id, $guardianName, $this_msg);

							# send sms
							$sms_message = $msg;
							$targetType = 3;
							$picType = 3;
							$adminPIC = "";
							$userPIC = "";
							$frModule= (($frModule==1)? "SMS_FR_MODULE_ARRIVAL" : (($frModule==2)? "SMS_FR_MODULE_LEAVE" : ""));
							$deliveryTime = "";
							$isIndividualMessage=true;

							$this->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC, $frModule, $deliveryTime,$isIndividualMessage);
						}
				}
			}
		}

		function updateMessageCountRecord($year, $month, $count)
		{
			//delete old record
			$sql = "delete from INTRANET_SMS2_MONTHLY_REPORT where Year=$year and Month=$month";
			$this->db_db_query($sql);

			//insert new record
			$sql = "insert into INTRANET_SMS2_MONTHLY_REPORT (Year, Month, MessageCount) values ($year, $month, $count)";
			$this->db_db_query($sql);
		}


		# Param:
		#  starttime: (yyyy-mm-dd HH:ii:ss)
		#  endtime  : (yyyy-mm-dd HH:ii:ss)
		function retrieveReplyMessage($starttime="",$endtime="")
		{
			global $sms_vendor_for_ReplyMessage, $intranet_root;
			include_once("../../../../includes/libkanhansms.php");

			$sms_vender = $sms_vendor_for_ReplyMessage;

			if($starttime=="" || $endtime==""){
				$starttime = date('Y-m-d')." 00:00:00";
				$endtime = date('Y-m-d')." 23:59:59";
				//$today_ts = strtotime(date('Y-m-d'));
				//$starttime = date('Y-m-d H:i:s',$today_ts - 60*60*24*6);   // start from 7 days ago
				//$endtime = date('Y-m-d H:i:s',$today_ts + 60*60*24);   // till the end of today
			}
			if ($sms_vender == "KANHAN")
			{
				$lkanhan = new libkanhansms();
				$data = $lkanhan->retrieveReply($starttime,$endtime);

				if(sizeof($data)>0){
					for($i=1;$i<sizeof($data);$i++){
						list($phone_number,$message_code,$school_code,$message,$reply_time) = $data[$i];
						if(trim($message_code!="")){
							$phone_number = $this->getReplyPhoneNumber($phone_number);
							$related_userid = $this->getUserIDByMessageCode($message_code);
							//$related_userid= $this->getUserIDByReplyPhoneNumber($phone_number);
							$sql = "UPDATE INTRANET_SMS2_REPLY_MESSAGE SET ReplyMessage='$message', ReplyPhoneNumber='$phone_number', RelatedUserID='$related_userid',RecordStatus=1,DateModified='$reply_time' WHERE MessageCode='$message_code'";

							$this->db_db_query($sql);
						}
					}
				}
			}
		}

		## parse and extract the reply phone number
		function getReplyPhoneNumber($phone){
			if(SMS_REPLY_PHONE_NUMBER_CHECKING_SCHEME==1){
				$str = substr($phone,0,3);
				if($str=="852"){
					$phone = substr($phone,3);
				}
			}
			return $phone;
		}
		function getUserIDByReplyPhoneNumber($phone){
			$sql="SELECT UserID FROM INTRANET_USER WHERE MobileTelNo='$phone'";
			$temp = $this->returnVector($sql);
			return $temp[0];
		}
		function getUserIDByMessageCode($message_code){
			$sql="SELECT RelatedUserID FROM INTRANET_SMS2_MESSAGE_RECORD WHERE RecordID='$message_code'";
			$temp = $this->returnVector($sql);
			return $temp[0];
		}


		function Trim_Phone_Number_First_Zeros($Number)
		{
			$string_Number = "$Number";

			while (substr($string_Number, 0, 1) == '0')
			{
				$string_Number = ltrim($string_Number, '0');
			}

			return $this->parsePhoneNumber($string_Number);
		}

		//pre-condition: list of ref id from table,in_use_vendor_object
		//post-condiction: SMS status write back to tbale recordstatus
		function getSMSStatus($ref_id_array)
		{
			global $sms_vendor, $intranet_root;
			//select all refid and put it in array
			//$ref_id_array=array(1253094603217);

			if ($sms_vendor == 'CTM')
			{

				include_once("$intranet_root/includes/libctmsms.php");
				$in_use_vendor_object = new libctmsms();

				$tempRefIDArr = array();
				$numOfRefID = count($ref_id_array);
				for ($i=0; $i<$numOfRefID; $i++)
				{
					$tempRefIDArr[] = $ref_id_array[$i];

					# Retrieve status for every 200 sms ReferenceID / the remaining ReferenceID so that the request URL will not be too long
					if ( (($i%200==0) && ($i!=0)) || ($i==$numOfRefID-1) )
					{
						$response_objects = $in_use_vendor_object->retrieveSMSStatus($tempRefIDArr);
						$numOfResponse = sizeof($response_objects);

						for ($j=0; $j<$numOfResponse; $j++)
						{
							$t_refID = $response_objects[$j]->reference_id;
							$t_recipient = $response_objects[$j]->recipient;
							$t_status = $response_objects[$j]->status;
							$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD SET RecordStatus = '".$t_status."' WHERE ReferenceID = '".$t_refID."'";
							$this->db_db_query($sql);
						}//end for

						$tempRefIDArr = array();
					}
				}
			}//end CTM part
			//start TACTUS Part
			else if ($sms_vendor == 'TACTUS')
			{
				include_once("$intranet_root/includes/libtactussms.php");
				$in_use_vendor_object = new libtactussms();

				//require addition recepient info (to phone number)
				$numOfRefID = count($ref_id_array);

				//fetch to list
				$RecipientPhoneNumber_Arr = array();

				for ($i=0; $i<$numOfRefID; $i++)
				{
					$sql = "Select RecipientPhoneNumber from INTRANET_SMS2_MESSAGE_RECORD WHERE ReferenceID = '".$ref_id_array[$i]."'";
					$data = $this->returnArray($sql,1);
					$RecipientPhoneNumber_Arr[$i]=$data[0]['RecipientPhoneNumber'];

				}//end for




				$response_objects = $in_use_vendor_object->retrieveSMSStatus($RecipientPhoneNumber_Arr,$ref_id_array);

				for ($j=0; $j<sizeof($response_objects); $j++)
				{
					$t_gw_dlr_status = $response_objects[$j]->gw_dlr_status;
					$t_gw_msgid = $response_objects[$j]->gw_msgid;
					
					//follow CTM sms status
					//expired
					if ($t_gw_dlr_status==2)
					{
						$t_gw_dlr_status=4;
						$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD SET RecordStatus = '".$t_gw_dlr_status."' WHERE ReferenceID = '".$t_gw_msgid."'";
						$this->db_db_query($sql);

					}
					//success
					if ($t_gw_dlr_status==1)
					{
						$t_gw_dlr_status=2;
						$sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD SET RecordStatus = '".$t_gw_dlr_status."' WHERE ReferenceID = '".$t_gw_msgid."'";
						$this->db_db_query($sql);

					}
					//unknown / die
					/*
					 if ($t_gw_dlr_status==0)
					 {
					 $t_gw_dlr_status=4;
					 $sql = "UPDATE INTRANET_SMS2_MESSAGE_RECORD SET RecordStatus = '".$t_gw_dlr_status."' WHERE ReferenceID = '".$t_gw_msgid."'";
					 $this->db_db_query($sql);
					 }//end if
					 */
				}//end for

			}//end else if
			else
			{
				return false;
			}
		}//end function

		function Refresh_SMS_Status($GivenSourceID=0)
		{
			global $sms_vendor;

			//if (($sms_vendor != 'CTM') && ($sms_vendor != 'TACTUS'))
			//   return false;
			
			if ($GivenSourceID>0)
			{
				$sql_particular = " and SourceMessageID='{$GivenSourceID}' ";
			}

			if ($sms_vendor == 'CTM')
			{
				# need not update status for those sent, failed and errored sms
				$sql = "Select
						ReferenceID
				From
						INTRANET_SMS2_MESSAGE_RECORD
				Where
						(ReferenceID Is Not NULL Or ReferenceID != '')
						$sql_particular And 
						(RecordStatus Is Null Or (RecordStatus != 2 And RecordStatus != 4 And RecordStatus != '-39'))
				";
				$ReferenceIDArr = $this->returnVector($sql);

				$this->getSMSStatus($ReferenceIDArr);
			}//end CTM
			else if ($sms_vendor == 'TACTUS')
			{
				# TACTUS keeps log for 7 days only!
				$sql = "Select
						ReferenceID
				From
						INTRANET_SMS2_MESSAGE_RECORD
				Where
						(ReferenceID Is Not NULL Or ReferenceID != '')
						$sql_particular And TIMESTAMPDIFF(DAY,DateInput,now())<=8 AND
						(RecordStatus Is Null or (RecordStatus != 2 And RecordStatus != 4)) order by dateinput desc
				";

				$ReferenceIDArr = $this->returnVector($sql);
				$this->getSMSStatus($ReferenceIDArr);
			}//end if
			else
			{
				return false;
			}
		}
		
		
		/*
		 * yuen: query to get the mobile numbers which are suspected as not in use
		 * Select        *      From        INTRANET_SMS2_MESSAGE_RECORD      Where        (ReferenceID Is Not NULL Or ReferenceID != '')         AND        RecordStatus=4 order by dateinput desc
		 * 
		 * 
		 */

		function Get_SMS_Status_Field($prefix='ismr', $needStyle=1)
		{
			global $i_SMS;

			$stylePrefix = '';
			$styleSuffix = '';
			if ($needStyle)
			{
				$stylePrefix = "<font color=\"red\">";
				$styleSuffix = "</font>";
			}

			$StatusField = "IF ($prefix.ReferenceID Is Null,
							'$stylePrefix".$i_SMS['SMSStatus']['FailToSendSMS']."$styleSuffix',
							CASE $prefix.RecordStatus
								WHEN 0 THEN 		'".$i_SMS['SMSStatus'][0]."'
								WHEN 1 THEN 		'".$i_SMS['SMSStatus'][1]."'
								WHEN 2 THEN 		'".$i_SMS['SMSStatus'][2]."'
								WHEN 4 THEN 		'$stylePrefix".$i_SMS['SMSStatus'][4]."$styleSuffix'
								WHEN 5 THEN 		'".$i_SMS['SMSStatus'][5]."'
								WHEN '-39' THEN 	'$stylePrefix".$i_SMS['SMSStatus']['-39']."$styleSuffix'

								WHEN '-1' THEN 		'$stylePrefix".$i_SMS['SMSStatus']['-1']."$styleSuffix'
								WHEN '-2' THEN 		'$stylePrefix".$i_SMS['SMSStatus']['-2']."$styleSuffix'
								WHEN '-4' THEN 		'$stylePrefix".$i_SMS['SMSStatus']['-4']."$styleSuffix'
								WHEN '-31' THEN 	'$stylePrefix".$i_SMS['SMSStatus']['-31']."$styleSuffix'
								WHEN '-32' THEN 	'$stylePrefix".$i_SMS['SMSStatus']['-32']."$styleSuffix'
								WHEN '-99' THEN 	'$stylePrefix".$i_SMS['SMSStatus']['-99']."$styleSuffix'
								WHEN '-100' THEN 	'$stylePrefix".$i_SMS['SMSStatus']['-100']."$styleSuffix'
							END
						)
						";
			return $StatusField;
		}
		
		function Get_SMS_Status_Array() {
			global $sms_vendor;
			
			$statusAry = array();
			if ($sms_vendor == 'TACTUS') {
				$statusAry[0] = $this->Get_SMS_Status_Wordings(0);
				$statusAry[1] = $this->Get_SMS_Status_Wordings(1);
				$statusAry[2] = $this->Get_SMS_Status_Wordings(2);
				$statusAry[4] = $this->Get_SMS_Status_Wordings(4);
			}
			
			return $statusAry;
		}
		
		function Get_SMS_Status_Wordings($StatusCode) {
			global $i_SMS;
			
			return $i_SMS['SMSStatus'][$StatusCode];
		}

		function Get_SMS_Status_Display($StatusCode)
		{
			global $i_SMS;

			$msg = '';
			$msg .= '<font color="red">';
			$msg .= $i_SMS['SMSStatus']["$StatusCode"];
			$msg .= '<br />';
			$msg .= $i_SMS['PleaseContactBLForEnquiry'];
			$msg .= '</font>';

			return $msg;
		}

		function Get_SMS_Limitation_Note_Table()
		{
			global $Lang, $i_SMS_Cautions;

			$MsgArr = $Lang['SMS']['LimitationArr'];
			$numOfMsg = count($MsgArr);

			$x = '';
			$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr><td colspan="2">'.$i_SMS_Cautions.'</td></tr>'."\n";
			for ($i=0; $i<$numOfMsg; $i++)
			{
				$thisContent = $MsgArr[$i]['Content'];
				$thisIndex = $i + 1;
				$thisSubContentArr = $MsgArr[$i]['SubContentArr'];
					
				$x .= '<tr>'."\n";
				$x .= '<td width="5%">'.$thisIndex.'.</td>'."\n";
				$x .= '<td>'.$thisContent.'</td>'."\n";
				$x .= '</tr>'."\n";
					
				if (isset($thisSubContentArr))
				{
					$thisNumOfSubContent = count((array)$thisSubContentArr);
					for ($j=0; $j<$thisNumOfSubContent; $j++)
					{
						$thisSubContent = $thisSubContentArr[$j];
							
						$x .= '<tr>'."\n";
						$x .= '<td>&nbsp;</td>'."\n";
						$x .= '<td>'.$thisSubContent.'</td>'."\n";
						$x .= '</tr>'."\n";
					}
				}
			}
			$x .= '</table>'."\n";

			return $x;
		}
		
		function Get_SMS_Fee() 
		{
			global $sms_vendor;
			
			$Fee = '';
			if ($sms_vendor == "TACTUS") {
				$Fee = '0.45';
			}
			
			return $Fee;
		}
		
		function Get_SMS_Fee_Notes() 
		{
			global $Lang;
			
			$Fee = $this->Get_SMS_Fee();
			
			$Note = '';
			if ($Fee != '') {
				$Note = str_replace('<!--FeeValue-->', $Fee, $Lang['SMS']['SMSFeeNotes']);
			}
			$Note .= $Lang['SMS']['SMSSentChargeNotes'];
			$Note .= $Lang['SMS']['SMSUEMONotes'];
			
			return $Note;
		}
		
		function parsePhoneNumber($phoneNumber)
		{
			return trim(str_replace("-", "", str_replace(" ", "", $phoneNumber)));
		}
		
		function getSMSStatusMultiSelection($MultipleStatus=false, $statusOptionArr=array()){
			global $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT, $Lang, $button_view, $button_cancel, $i_Discipline_System_Award_Punishment_Select_Status, $i_SMS;
			include_once($PATH_WRT_ROOT."includes/libinterface.php");
			$linterface = new interface_html();
			$optionArray = $this->Get_SMS_Status_Array();
			$numOfOptionArray = count($optionArray);
		
			$OptionLayer = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
			$OptionLayer .= '<tr>';
			$OptionLayer .= '<td><div class="selectbox_group"> <a href="javascript:;" onClick="MM_showHideLayers(\'status_option\', \'\', \'show\')">'. $i_Discipline_System_Award_Punishment_Select_Status .'</a> </div>';
			$OptionLayer .= '<br style="clear:both">';
												
			$OptionLayer .= '<div id="status_option" class="selectbox_layer">';
				$OptionLayer .= '<table width="" border="0" cellspacing="0" cellpadding="3">';
								
					# Loop each options
				
					$numOfStatusOptionArr = count($statusOptionArr);
					foreach ($optionArray as $optionValue => $OptionDisplay){
						$isChecked = false;
						
						if($numOfStatusOptionArr>0){											
							if(in_array("$optionValue", $statusOptionArr, true)){
								$isChecked = true;
							}
							else{
								$isChecked = false;
							}
						}
						else{
							$isChecked = true;
						}
						$OptionLayer .= '<tr>';
							$OptionLayer .= '<td>';						
							$OptionLayer .= $linterface->Get_Checkbox($OptionDisplay, 'StatusOption[]', $optionValue, $isChecked, 'StatusOption', $OptionDisplay, $Onclick='', $Disabled='');
							$OptionLayer .= '</td>';
						$OptionLayer .= '</tr>';
					}
					
//					if($numOfStatusOptionArr>0){	
//						if(in_array('FailToSendSMS', $statusOptionArr)){
//							$FailToSendSMSIsChecked = true;
//						}
//						else{
//							$FailToSendSMSIsChecked = false;
//						}
//					}else{
//						$FailToSendSMSIsChecked = true;
//					}
//					
//					$OptionLayer .= '<tr>';
//							$OptionLayer .= '<td>';
//							$OptionLayer .= $linterface->Get_Checkbox('FailToSendSMS', 'StatusOption[]', 'FailToSendSMS', $FailToSendSMSIsChecked, 'StatusOption', $i_SMS['SMSStatus']['FailToSendSMS'], $Onclick='', $Disabled='');
//							$OptionLayer .= '</td>';
//					$OptionLayer .= '</tr>';
					
					
					if($MultipleStatus==true){
						
						if($numOfStatusOptionArr>0){	
							if(in_array('MultipleStatus', $statusOptionArr)){
								$multipleStatusOptionIsChecked = true;
							}
							else{
								$multipleStatusOptionIsChecked = false;
							}
						}
						else{
							$multipleStatusOptionIsChecked = true;
						}
						$OptionLayer .= '<tr>';
							$OptionLayer .= '<td>';
							$OptionLayer .= $linterface->Get_Checkbox('MultipleStatus', 'StatusOption[]', 'MultipleStatus', $multipleStatusOptionIsChecked, 'StatusOption', $i_SMS['MultipleStatus'], $Onclick='', $Disabled='');
							$OptionLayer .= '</td>';
						$OptionLayer .= '</tr>';
				
					}							
					$OptionLayer .= '<tr>';
						$OptionLayer .= '<td align="center" class="tabletext">';
							$OptionLayer .=  $linterface->GET_BTN($button_view, "submit", "javascript:refresh()");
							$OptionLayer .=  $linterface->GET_BTN($button_cancel, "button", "javascript: onClick=MM_showHideLayers('status_option','','hide');form.reset();");
							$OptionLayer .=  $linterface->GET_BTN($Lang['eDiscipline']['SelectAll'], "button", "javascript:checkboxCheck(true)");
							$OptionLayer .=  $linterface->GET_BTN($Lang['eDiscipline']['UnselectAll'], "button", "javascript:checkboxCheck(false)");
						$OptionLayer .= '</td>';
					$OptionLayer .= '</tr>';
				$OptionLayer .= '</table>';
			$OptionLayer .= '</div>';
			$OptionLayer .= '</td>';
			$OptionLayer .= '</tr>';
			$OptionLayer .= '</table>';
			
			return $OptionLayer;
		}	
		
	}
}
?>