<?php
#  Editing by 

/*******************************************************
 * 	20120207 Ivan [2012-0207-1111-39071]
 * 		- modified CALCULATE_COLUMN_MARK() to fix the "abs" cannot be excluded in the grand marks problem 
 * 	20111128 Marcus
 * 		- modified Get_Number_Of_Counted_Mark_Student, if $eRCTemplateSetting['SpecialCaseThatExcludedFromNoOfStudent'] isset, exclude special case of this array only 
 * 	20100111 Marcus
 * 		- modified UPDATE_ORDER, Add flag to order position by SD instead of rawmark 
 * *****************************************************/

class libreportcard2008_generate extends libreportcard2008j {

	function libreportcard2008_generate(){
		$this->libreportcard();
	}
	function GENERATE_TERM_REPORTCARD($ReportID, $ClassLevelID) {
		$success = array();
		
		// delete the consolidated data (if exist)
		$success["DELETE_REPORT_RESULT_SCORE"] = $this->DELETE_REPORT_RESULT_SCORE($ReportID);
		$success["DELETE_REPORT_RESULT"] = $this->DELETE_REPORT_RESULT($ReportID);
		$success["DELETE_NEW_FULLMARK"] = $this->DELETE_NEW_FULLMARK($ReportID);
		
		################### Preload settings & data ###################
		// load Mark Storage & Display settings: SubjectScore (0,1,2), SubjectTotal (0,1,2), GrandAverage (0,1,2)
		$storageDisplaySettings = $this->LOAD_SETTING("Storage&Display");
		
		// load Calculation settings: OrderTerm (1,2), OrderFullYear (1,2), UseWeightedMark (0,1)
		$calculationSettings = $this->LOAD_SETTING("Calculation");
		$isAbjustFullMark = $calculationSettings["AdjustToFullMarkFirst"];
		$calculationOrder = $calculationSettings["OrderTerm"];
		
		// load Absent & Exempt settings: Exempt (ExWeight, ExFullMark, Zero), Absent (ExWeight, ExFullMark, Zero)
		$absentExemptSettings = $this->LOAD_SETTING("Absent&Exempt");
		
		// get report template basic info: Semester, PercentageOnColumnWeight
		$reportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		
		// Check for Combination of Special Case of Input Mark / Term Mark
		$SpecialCaseArrH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		foreach($SpecialCaseArrH as $key){
			$SpecialCaseCountArrH[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		}
		$SpecialCaseArrPF = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
		foreach($SpecialCaseArrPF as $key){
			$SpecialCaseCountArrPF[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		}
		
		// $reportColumnData[$i] + ["ReportColumnID"], ["ColumnTitle"], ["DefaultWeight"], 
		// ["SemesterNum"], ["IsDetails"], ["ShowPosition"], ["PositionRange"], ["DisplayOrder"]
		$reportColumnData = $this->returnReportTemplateColumnData($ReportID);
		if (sizeof($reportColumnData) > 0) {
			//$totalReportColumnWeight = 0;
			for($i=0; $i<sizeof($reportColumnData); $i++) {
				$reportColumnIDList[] = $reportColumnData[$i]["ReportColumnID"];
				$reportInvolveTerms[] = $reportColumnData[$i]["SemesterNum"];
				$reportColumnTermMap[$reportColumnData[$i]["ReportColumnID"]] = $reportColumnData[$i]["SemesterNum"];
				$reportTermColumnMap[$reportColumnData[$i]["SemesterNum"]] = $reportColumnData[$i]["ReportColumnID"];
			}
		}
		
		// $classArr[$i] + ["ClassID"], ["ClassName"]
		$classArr = $this->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0; $i<sizeof($classArr); $i++) {
			// $studentArr[#ClassID][$i] + ["UserID"], ["WebSAMSRegNo"],...
			$studentArr[$classArr[$i]["ClassID"]] = $this->GET_STUDENT_BY_CLASS($classArr[$i]["ClassID"]);
		}
		
		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, $withGrandMark=0, $ReportID);
		################################################################
		
		// Loop 1: Classes
		for($i=0; $i<sizeof($classArr); $i++) {
			// construct the $studentIDArr
			$classID = $classArr[$i]["ClassID"];
			$studentIDArr = array();
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			// SKIP this class if no students
			if (sizeof($studentIDArr) == 0) continue;
			
			// Horizontal -> Vertical (subject overall -> grand total)
			if ($calculationOrder == 1) {
				// Loop 2: Subjects
				$studentSubjectOverallList = array();
				foreach ($subjectList as $subjectID => $subjectInfo) {
					// $schemeInfo[0] is main info, [1] is ranges info
					$schemeInfo = $this->GET_GRADING_SCHEME_INFO($subjectInfo["schemeID"]);
					
					$schemeType = $schemeInfo[0]["SchemeType"];
					$passWording = $schemeInfo[0]["Pass"];
					$failWording = $schemeInfo[0]["Fail"];
					$subjectFullMark = $schemeInfo[0]["FullMark"];
					
					$scaleInput = $subjectInfo["scaleInput"];
					$scaleDisplay = $subjectInfo["scaleDisplay"];
					
					// get the grading scheme range info
					$schemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($subjectInfo["schemeID"]);
					for($m=0; $m<sizeof($schemeRangeInfo); $m++) {
						$schemeRangeIDGradeMap[$schemeRangeInfo[$m]["GradingSchemeRangeID"]] = $schemeRangeInfo[$m]["Grade"];
					}
					
					// check if it is a parent subject, if yes find info of its components subjects
					$CmpSubjectArr = array();
					$isCalculateByCmpSub = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
					$isCmpSubject = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($subjectID);
					if (!$isCmpSubject) {
						$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($subjectID, $ClassLevelID);
						$CmpSubjectIDArr = array();
						if(!empty($CmpSubjectArr)){	
							for($k=0 ; $k<count($CmpSubjectArr) ; $k++){
								$CmpSubjectIDArr[] = $CmpSubjectArr[$k]['SubjectID'];
								$CmpSubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$k]['SubjectID'], $withGrandResult=0, $returnAsso=0, $ReportID);
								if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
									$isCalculateByCmpSub = 1;
							}
						}
					}
					
					// Weight of each of the columns (Assessment)
					$totalReportColumnWeight = 0;
					$reportColumnWeight = array();
					$reportColumnWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, "SubjectID = ".$subjectID);
					for($x=0 ; $x<sizeof($reportColumnWeightInfo) ; $x++){
						if ($reportColumnWeightInfo[$x]['ReportColumnID'] != "" && $reportColumnWeightInfo[$x]['Weight'] != "") {
							$reportColumnWeight[$reportColumnWeightInfo[$x]['ReportColumnID']] = $reportColumnWeightInfo[$x]['Weight'];
							$totalReportColumnWeight += $reportColumnWeightInfo[$x]['Weight'];
						}
					}
					
					# Calculate all marks first for calculating weighted mark below (not very efficient)
					$AllColumnMarkArr = array();
					for($n=0; $n<sizeof($reportColumnData); $n++) {
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						// get raw marks of all students in a class of this assessment
						if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub))
							$marksheetScore = $this->GET_MARKSHEET_SCORE($studentIDArr, $subjectID, $reportColumnID);
						else
							$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID);
						
						foreach ($marksheetScore as $studentID => $scoreDetailInfo) {
							$scoreInfo = array();
							if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
								$scoreInfo = $scoreDetailInfo;
							} else {
								// skip consolidate this student if the mark is NULL
								if ($scoreDetailInfo == "")	continue;
								$scoreInfo["MarkRaw"] = $scoreDetailInfo;
								$scoreInfo["MarkNonNum"] = $scoreDetailInfo;
								$scoreInfo["MarkType"] = "M";
							}
							
							if($scoreInfo['MarkType'] == "M") {
								if ($scoreInfo["MarkRaw"] != "-1")
									$AllColumnMarkArr[$studentID][$reportColumnID] = $scoreInfo['MarkRaw'];
								else
									$AllColumnMarkArr[$studentID][$reportColumnID] = $scoreInfo['MarkNonNum'];
							}
						}
						
						// compute the difference of available student mark and all student, assign "Exempted" to them
						$noMarkStudentID = array_diff($studentIDArr, array_keys($marksheetScore));
						if (sizeof($noMarkStudentID) > 0) {
							for($g=0; $g<sizeof($noMarkStudentID); $g++) {
								$AllColumnMarkArr[$noMarkStudentID[$g]][$reportColumnID] = "/";
							}
						}
					}
					
					// Loop 3: ReportColumns (Assessments)
					$calculationMark = array();
					for($n=0; $n<sizeof($reportColumnData); $n++) {
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						$CmpInfoArr = $this->CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID);
						$CmpSubjectArr = $CmpInfoArr[0];
						$CmpSubjectIDArr = $CmpInfoArr[1];
						$isCmpSubject = $CmpInfoArr[2];
						$isCalculateByCmpSub = $CmpInfoArr[3];
						// get raw marks of all students in a class of this assessment
						if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub))
							$marksheetScore = $this->GET_MARKSHEET_SCORE($studentIDArr, $subjectID, $reportColumnID);
						else
						{
							
							if ($isCalculateByCmpSub)
							{
								# updated by Ivan on 21 Jan 2009 - follow the calculation of export function
								//$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID, 1);
								$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID);
							}
							else
							{
								$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID);
							}
						}
						// For term report, column is Assessment, therefore semester is NULL
						$semester = "";
						$conAssessmentMarkValues = array();
						
						// for storing student mark to sort order
						$markListForOrderMeritClass = array();
						
						// Loop 4: Student's Score
						foreach ($marksheetScore as $studentID => $scoreDetailInfo) {
							$consolidateMark = "";
							$scoreInfo = array();
							if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
								$scoreInfo = $scoreDetailInfo;
							} else {
								$scoreInfo = $this->CREATE_SCORE_INFO_ARRAY($scoreDetailInfo);
							}
							
							$thisRawMark = $scoreInfo["MarkRaw"];
							if ($scoreInfo["MarkType"] == "M" || $scoreInfo["MarkType"] == "G") {
								if ($scaleInput == "M" && $scaleDisplay == "M") {
									// check if the consolidation mark need to be converted to weight mark
									if ($calculationSettings["UseWeightedMark"] == "1") {
										$consolidateMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $scoreInfo["MarkRaw"]);
									} else {
										$consolidateMark = $scoreInfo["MarkRaw"];
									}
									// Horizontal -> Vertical: need to store weighted mark to calculate subject overall
									$calculationMark[$studentID][$reportColumnID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $scoreInfo["MarkRaw"], 1, $AllColumnMarkArr[$studentID]);
									$calculationMark[$studentID][$reportColumnID] = $this->ROUND_MARK($calculationMark[$studentID][$reportColumnID], "SubjectScore");
								} else if ($scaleInput == "M" && $scaleDisplay == "G") {
									// convert to grade according to ranges
									$consolidateMark = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $scoreInfo["MarkRaw"], $ReportID);
									$calculationMark[$studentID][$reportColumnID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $scoreInfo["MarkRaw"], 1, $AllColumnMarkArr[$studentID]);
									$calculationMark[$studentID][$reportColumnID] = $this->ROUND_MARK($calculationMark[$studentID][$reportColumnID], "SubjectScore");
								} else if ($scaleInput == "G" && $scaleDisplay == "G") {
									$consolidateMark = $schemeRangeIDGradeMap[$scoreInfo["MarkNonNum"]];
									$calculationMark[$studentID][$reportColumnID] = "-";
									$thisRawMark = 0;
								}
								
								// round off the weighted mark
								if (is_numeric($consolidateMark)) {
									$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
									$markListForOrderMeritClass[$studentID] = $consolidateMark;
								}
								
								// consolidate the mark into DB (RC_REPORT_RESULT_SCORE)
								$mark = "";
								$grade = "";
								if (is_numeric($consolidateMark)) {
									$mark = $consolidateMark;
								} else {
									$grade = $consolidateMark;
								}
								
								$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '$mark', '$grade', '$thisRawMark', '', '$semester', '', '', NOW(), NOW())";
							} else if ($scoreInfo["MarkType"] == "PF") {
								$consolidateMark = ($scoreInfo["MarkNonNum"] == "P") ? $passWording : $failWording;
								$PassFailMark = $consolidateMark;
								$thisRawMark = 0;
								$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$PassFailMark', '$thisRawMark', '', '$semester', '', '', NOW(), NOW())";
								// By default, setting Pass/Fail based subject to Exempted
								$calculationMark[$studentID][$reportColumnID] = "/";
							} else {						
								$specialCase = $scoreInfo["MarkNonNum"];
								$thisRawMark = 0;
								$calculationMark[$studentID][$reportColumnID] = $specialCase;
								$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$specialCase', '$thisRawMark', '', '$semester', '', '', NOW(), NOW())";
							}
						} // End Loop 4: Student's Score
						
						// insert the converted mark into DB
						if (sizeof($conAssessmentMarkValues) > 0) {
							$conAssessmentMarkValues = implode(",", $conAssessmentMarkValues);
							$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($conAssessmentMarkValues);
						}
						
						// Subject mark order in a  class may be calculated here
						if (sizeof($markListForOrderMeritClass) > 0) {
							arsort($markListForOrderMeritClass);
							$success["UPDATE_SCORE_ORDER_MERIT"] = $this->UPDATE_SCORE_ORDER_MERIT(array_keys($markListForOrderMeritClass), "Class", $subjectID, $ReportID, $reportColumnID);
						}
					} // End Loop 3: ReportColumns
					
					// consolidate existing (entered by user directly) overall mark, for the case of input & display are both Grade
					$existOverallScoreInfo = $this->GET_MARKSHEET_OVERALL_SCORE($studentIDArr, $subjectID, $ReportID);
					$subjectOverallValues = array();
					$consolidatedSubjectOverallStudentList = array();
					if (sizeof($existOverallScoreInfo) > 0) {
						foreach ($existOverallScoreInfo as $markOverallStudentID => $markOverallInfo) {
							$consolidatedSubjectOverallStudentList[] = $markOverallStudentID;
							
							$thisRawMark = $markOverallInfo['MarkRaw'];
							if ($markOverallInfo["MarkType"] == "G") {
								$gradeToConsolidate = $schemeRangeIDGradeMap[$markOverallInfo["MarkNonNum"]];
								$thisRawMark = 0;
								$subjectOverallValues[] = "('$markOverallStudentID', '$subjectID', '$ReportID', '', '', '$gradeToConsolidate', '$thisRawMark', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
							} else if ($markOverallInfo["MarkType"] == "SC") {
								$thisRawMark = 0;
								$subjectOverallValues[] = "('$markOverallStudentID', '$subjectID', '$ReportID', '', '', '".$markOverallInfo["MarkNonNum"]."', '$thisRawMark', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
							}
						}
						
						if (sizeof($subjectOverallValues) > 0) {
							$subjectOverallValues = implode(",", $subjectOverallValues);
							$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($subjectOverallValues);
						}
					}
					
					// calculate the subject overall mark of each student
					if (sizeof($calculationMark) > 0) {
						$subjectOverallValues = array();
						foreach ($calculationMark as $markStudentID => $markList) {
							// skip calculation of subject overall mark since it already been entered by user directly and consolidated above
							if (in_array($markStudentID, $consolidatedSubjectOverallStudentList))
								continue;
							$subjectOverallMark = "";
							$excludeColumnWeight = 0;
							$excludeFullMark = 0;
							$specialCaseDifferentiate = array();
							
							// loop column by column and sum up the overall mark & exclude weight
							foreach ($markList as $markColumnID => $markColumn) {
								if (is_numeric($markColumn)) {
									// if any of the assessment marks is numeric, make sure $subjectOverallMark is initiated to zero
									$subjectOverallMark = ($subjectOverallMark == "") ? 0 : $subjectOverallMark;
									$subjectOverallMark += $markColumn;
								} else {
									// Build the $specialCaseDifferentiate Array for determining the subject overall mark below
									$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $markColumn, $absentExemptSettings);
									
									// Only concern numeric mark calculation here
									if ($specialCaseDifferentiate[$markColumn]["isExFullMark"] == 1) {
										$excludeFullMark += $subjectFullMark*$reportColumnWeight[$markColumnID];
									} else if ($specialCaseDifferentiate[$markColumn]["isCount"] == 0) {
										// not in use since CONVERT_MARK_TO_WEIGHTED_MARK is used above
										$excludeColumnWeight += $reportColumnWeight[$markColumnID];
									}
								}
							}
							
							// compute the difference of available mark and all report column
							#$noMarkColumnID = array_diff($reportColumnIDList, array_keys($markList));
							
							// in case of excluding full mark
							if ($excludeFullMark > 0) {
								$newFullMark = $subjectFullMark - $excludeFullMark;
								$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK($newFullMark, $ReportID, $markStudentID, $subjectID);
							}
							
							// subject overall mark can be calculated as number
							$thisRawMark = '';
							if (is_numeric($subjectOverallMark)) {
								if ($totalReportColumnWeight == $excludeColumnWeight) {
									$subjectOverallMark = "/";
									$thisRawMark = 0;
								} else {
									$thisRawMark = $subjectOverallMark;
									$subjectOverallMark = $this->ROUND_MARK($subjectOverallMark, "SubjectTotal");
								}
							} else { // subject cannot be calculated as number
								// There's only one kind of special case in all column
								if (sizeof($specialCaseDifferentiate) == 1) {
									$onlySpecialCase = array_keys($specialCaseDifferentiate);
									$subjectOverallMark = $onlySpecialCase[0];
								} else {	// How to handle different special cases mixed up together?
									// Temporary assign it to Zero 
									$subjectOverallMark = 0;
								}
								$thisRawMark = 0;
							}
							
							// for calculating average mark (skip component subjects)
							if (!$isCmpSubject) {
								if ($subjectOverallMark!='' && !is_numeric($subjectOverallMark))
									$studentSubjectOverallList[$markStudentID][$subjectID] = $subjectOverallMark;
								else
									$studentSubjectOverallList[$markStudentID][$subjectID] = $thisRawMark;
							}
							
							if (is_numeric($subjectOverallMark))
								$subjectOverallValues[] = "('$markStudentID', '$subjectID', '$ReportID', '', '$subjectOverallMark', '', '$thisRawMark', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
							else
								$subjectOverallValues[] = "('$markStudentID', '$subjectID', '$ReportID', '', '', '$subjectOverallMark', '$thisRawMark', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
						}
						
						// consolidate the resulting subject overall mark
						if (sizeof($subjectOverallValues) > 0) {
							$subjectOverallValues = implode(",", $subjectOverallValues);
							$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($subjectOverallValues);
						}
					}
				} // End Loop 2: Subjects
				
				
				
				// calculate average mark
				// for Horizontal to Vertical, only average mark of subject overall need to be calculated
				$reportAverageTotal = array();
				$grandValues = array();
				
				// order, gpa yet to be done....
				// much easier when doing it after mark have been consolidated
				
				
				foreach ($studentSubjectOverallList as $studentID => $subjectOverallList) {
					$reportAverageTotal[$studentID] = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $studentID, $subjectOverallList, '', 1, 1);
//				debug_r($ReportID);exit();
					// round grand total according to the same setting of average???
					$tmpTotal = $this->ROUND_MARK($reportAverageTotal[$studentID]["GrandTotal"], "GrandAverage");
					$tmpAverage = $this->ROUND_MARK($reportAverageTotal[$studentID]["GrandAverage"], "GrandAverage");
					$tmpAverageRaw = $reportAverageTotal[$studentID]["GrandAverage"];
					$grandValues[] = "('$studentID', '$ReportID', '', '$tmpTotal', '$tmpAverage', '$tmpAverageRaw', '', '', '', '', '', '', '', '', NOW(), NOW())";
				}
				// consolidate grand total & grand average (no value for ReportColumnID)
				$grandValues = implode(",", $grandValues);
				$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($grandValues);
			// Vertical > Horizontal: All subject have a common weight (as a preview only, since these weight is actually for average mark)
			} else if ($calculationOrder == 2) {
				// get the weight (NOT really meaningful for calculating subject overall mark)
				for($i=0; $i<sizeof($reportColumnData); $i++) {
					$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$reportColumnData[$i]["ReportColumnID"];
					$reportColumnWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
					$reportColumnWeight[$reportColumnWeightInfo[0]["ReportColumnID"]] = $reportColumnWeightInfo[0]['Weight'];
					$totalReportColumnWeight += $reportColumnWeightInfo[0]['Weight'];
				}
				
				// Loop 2: ReportColumns
				for($i=0; $i<sizeof($reportColumnData); $i++) {
					$reportColumnID = $reportColumnData[$i]["ReportColumnID"];
					
					// Loop 3: Subjects
					$calculationMark = array();
					foreach ($subjectList as $subjectID => $subjectInfo) {
						// $schemeInfo[0] is main info, [1] is ranges info
						$schemeInfo = $this->GET_GRADING_SCHEME_INFO($subjectInfo["schemeID"]);
						
						$schemeType = $schemeInfo[0]["SchemeType"];
						$passWording = $schemeInfo[0]["Pass"];
						$failWording = $schemeInfo[0]["Fail"];
						
						$scaleInput = $subjectInfo["scaleInput"];
						$scaleDisplay = $subjectInfo["scaleDisplay"];
						
						// get the grading scheme range info
						$schemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($subjectInfo["schemeID"]);
						for($m=0; $m<sizeof($schemeRangeInfo); $m++) {
							$schemeRangeIDGradeMap[$schemeRangeInfo[$m]["GradingSchemeRangeID"]] = $schemeRangeInfo[$m]["Grade"];
						}
						
						// check if it is a parent subject
						$CmpSubjectArr = array();
						$isCalculateByCmpSub = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
						$isCmpSubject = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($subjectID);
						if (!$isCmpSubject) {
							$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($subjectID, $ClassLevelID);
							$CmpSubjectIDArr = array();
							if(!empty($CmpSubjectArr)){	
								for($k=0 ; $k<count($CmpSubjectArr) ; $k++){
									$CmpSubjectIDArr[] = $CmpSubjectArr[$k]['SubjectID'];
									$CmpSubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$k]['SubjectID']);
									if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
										$isCalculateByCmpSub = 1;
								}
							}
						}
						
						// get raw marks of all students in a class of this assessment
						if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub))
							$marksheetScore = $this->GET_MARKSHEET_SCORE($studentIDArr, $subjectID, $reportColumnID);
						else
							$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID);
						
						# 20140804 Missing $conAssessmentMarkValues = array();	
						$conAssessmentMarkValues = array();	
						// Loop 4: Student's Score
						foreach ($marksheetScore as $studentID => $scoreDetailInfo) {
							$AllColumnMarkArr = array();
							$consolidateMark = "";
							$scoreInfo = array();
							if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
								$scoreInfo = $scoreDetailInfo;
							} else {
								$scoreInfo["MarkRaw"] = $scoreDetailInfo;
								$scoreInfo["MarkNonNum"] = $scoreDetailInfo;
								$scoreInfo["MarkType"] = "M";
							}
							
							$thisRawMark = $scoreInfo["MarkRaw"];
							if ($scoreInfo["MarkType"] == "M" || $scoreInfo["MarkType"] == "G") {
								if ($scaleInput == "M" && $scaleDisplay == "M") {
									// check if the consolidation mark need to be converted to weight mark
									if ($calculationSettings["UseWeightedMark"] == "1") {
										#$consolidateMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $scoreInfo["MarkRaw"]);
										$consolidateMark = $scoreInfo["MarkRaw"];
									} else {
										$consolidateMark = $scoreInfo["MarkRaw"];
									}
									// Vertical -> Horizontal: need to store weighted mark to calculate subject overall
									#$calculationMark[$studentID][$subjectID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $scoreInfo["MarkRaw"]);
									$calculationMark[$studentID][$subjectID] = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
								} else if ($scaleInput == "M" && $scaleDisplay == "G") {
									// convert to grade according to ranges
									$consolidateMark = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $scoreInfo["MarkRaw"]);
									#$calculationMark[$studentID][$subjectID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $scoreInfo["MarkRaw"]);
									$calculationMark[$studentID][$subjectID] = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
								} else if ($scaleInput == "G" && $scaleDisplay == "G") {
									$consolidateMark = $schemeRangeIDGradeMap[$scoreInfo["MarkNonNum"]];
									$calculationMark[$studentID][$subjectID] = "-";
									$thisRawMark = 0;
								}
								
								// round off the weighted mark
								if (is_numeric($consolidateMark)) {
									$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
								}
								
								// consolidate the mark into DB (RC_REPORT_RESULT_SCORE)
								$mark = "";
								$grade = "";
								if (is_numeric($consolidateMark)) {
									$mark = $consolidateMark;
								} else {
									$grade = $consolidateMark;
								}
								
								$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '$mark', '$grade', '$thisRawMark', '', '$semester', '', '', NOW(), NOW())";
							} else if ($scoreInfo["MarkType"] == "PF") {
								$consolidateMark = ($scoreInfo["MarkNonNum"] == "P") ? $passWording : $failWording;
								$PassFailMark = $consolidateMark;
								$thisRawMark = 0;
								$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$PassFailMark', '$thisRawMark', '', '$semester', '', '', NOW(), NOW())";
								$calculationMark[$studentID][$subjectID] = "-";
							} else {								
								$specialCase = $scoreInfo["MarkNonNum"];
								$thisRawMark = 0;
								$calculationMark[$studentID][$subjectID] = $specialCase;
								$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$specialCase', '$thisRawMark', '', '$semester', '', '', NOW(), NOW())";
							}
						} // End Loop 4
						
						// Subject mark order in a  class may be calculated here
						/* to be written */
						
						// insert the converted mark into DB
						if (sizeof($conAssessmentMarkValues) > 0) {
							$conAssessmentMarkValues = implode(",", $conAssessmentMarkValues);
							$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($conAssessmentMarkValues);
						}
					} // End Loop 3: Subjects
					
					// calculate the term average mark (vertically across subjects) of each student
					if (sizeof($calculationMark) > 0) {
						$termAverageValues = array();
						$grandAverageTotalList = array();
						$termAverageMark = "";
						$grandAverageTotalExistWeight = array();
						foreach ($calculationMark as $markStudentID => $markList) {
							$termAverageMark = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $markStudentID, $markList, $reportColumnID);
							// didn't consider the extreme case with no average mark can be calculated
							if (!isset($grandAverageTotalExistWeight[$markStudentID])) 
								$grandAverageTotalExistWeight[$markStudentID] = 0;
							$grandAverageTotalExistWeight[$markStudentID] += $reportColumnWeight[$reportColumnID];
							$grandAverageTotalList[$markStudentID]["GrandTotal"] += $termAverageMark["GrandTotal"] * $reportColumnWeight[$reportColumnID];
							$grandAverageTotalList[$markStudentID]["GrandAverage"] += $termAverageMark["GrandAverage"] * $reportColumnWeight[$reportColumnID];
							// round grand total according to the same setting of average???
							$tmpTotal = $this->ROUND_MARK($termAverageMark["GrandTotal"], "GrandAverage");
							$tmpAverage = $this->ROUND_MARK($termAverageMark["GrandAverage"], "GrandAverage");
							$tmpAverageRaw = $termAverageMark["GrandAverage"];
							
							$termAverageValues[] = "('$markStudentID', '$ReportID', '$reportColumnID', '$tmpTotal', '$tmpAverage', '$tmpAverageRaw', '', '', '', '', '', '', '', '', NOW(), NOW())";
						}
						
						// consolidate the term average & total mark
						if (sizeof($termAverageValues) > 0) {
							$termAverageValues = implode(",", $termAverageValues);
							$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($termAverageValues);
						}
					}
				} // End Loop 2: ReportColumns
				
				// consolidate the grand average & total mark
				if (sizeof($grandAverageTotalList) > 0) {
					foreach ($grandAverageTotalList as $grandStudentID => $grandList) {
						if (isset($grandAverageTotalExistWeight[$grandStudentID])) {
							##### handle total weight <> exist weight => need to exclude weight?
						}
					
						// round grand total according to the same setting of average???
						$tmpTotal = $this->ROUND_MARK($grandList["GrandTotal"], "GrandAverage");
						$tmpAverage = $this->ROUND_MARK($grandList["GrandAverage"], "GrandAverage");
						$tmpAverageRaw = $grandList["GrandAverage"];
						$grandAverageValues[] = "('$grandStudentID', '$ReportID', '', '$tmpTotal', '$tmpAverage', '$tmpAverageRaw', '', '', '', '', '', '', '', '', NOW(), NOW())";
					}
					$grandAverageValues = implode(",", $grandAverageValues);
					$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($grandAverageValues);
				}
			} // End Condition: CalculationOrder
		} // End Loop 1: Classes
		
		// Update order (class & form)
		$this->UPDATE_ORDER($ReportID, $ClassLevelID);
	}
	
	function GENERATE_WHOLE_YEAR_REPORTCARD($ReportID, $ClassLevelID) {
		$success = array();
		// delete the consolidated data (if exist)
		$success["DELETE_REPORT_RESULT_SCORE"] = $this->DELETE_REPORT_RESULT_SCORE($ReportID);
		$success["DELETE_REPORT_RESULT"] = $this->DELETE_REPORT_RESULT($ReportID);
		$success["DELETE_NEW_FULLMARK"] = $this->DELETE_NEW_FULLMARK($ReportID);
		
		################### Preload settings & data ###################
		// load Mark Storage & Display settings: SubjectScore (0,1,2), SubjectTotal (0,1,2), GrandAverage (0,1,2)
		$storageDisplaySettings = $this->LOAD_SETTING("Storage&Display");
		
		// load Calculation settings: OrderTerm (1,2), OrderFullYear (1,2), UseWeightedMark (0,1)
		$calculationSettings = $this->LOAD_SETTING("Calculation");
		$isAbjustFullMark = $calculationSettings["AdjustToFullMarkFirst"];
		
		// load Absent & Exempt settings: Exempt (ExWeight, ExFullMark, Zero), Absent (ExWeight, ExFullMark, Zero)
		$absentExemptSettings = $this->LOAD_SETTING("Absent&Exempt");
		
		// get report template basic info: Semester, PercentageOnColumnWeight
		$reportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		
		// Check for Combination of Special Case of Input Mark / Term Mark
		$SpecialCaseArrH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		foreach($SpecialCaseArrH as $key){
			$SpecialCaseCountArrH[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		}
		$SpecialCaseArrPF = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
		foreach($SpecialCaseArrPF as $key){
			$SpecialCaseCountArrPF[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		}
		
		// $reportColumnData[$i] + ["ReportColumnID"], ["ColumnTitle"], ["DefaultWeight"], 
		// ["SemesterNum"], ["IsDetails"], ["ShowPosition"], ["PositionRange"], ["DisplayOrder"]
		$reportColumnData = $this->returnReportTemplateColumnData($ReportID);
		if (sizeof($reportColumnData) > 0) {
			//$totalReportColumnWeight = 0;
			for($i=0; $i<sizeof($reportColumnData); $i++) {
				$reportInvolveTerms[] = $reportColumnData[$i]["SemesterNum"];
				$reportColumnTermMap[$reportColumnData[$i]["ReportColumnID"]] = $reportColumnData[$i]["SemesterNum"];
				$reportTermColumnMap[$reportColumnData[$i]["SemesterNum"]] = $reportColumnData[$i]["ReportColumnID"];
			}
		}
		
		// whole year report
		$calculationOrder = $calculationSettings["OrderFullYear"];
		
		// what terms include in this whole year report?
		// does these terms have its own term report before?
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		$reportInvolveTermsList = implode("','", $reportInvolveTerms);
		$reportInvolveTermsList = "'".$reportInvolveTermsList."'";
		$sql = "SELECT ReportID, Semester FROM $table WHERE ClassLevelID = '$ClassLevelID' AND Semester IN ($reportInvolveTermsList)";
		$existTermReportID = $this->returnArray($sql, 2);
		
		$searchExistTermMode = "T";
		if (sizeof($existTermReportID) == 0) {
			# search completed whole year report also
			$sql = "SELECT ReportID, Semester FROM $table WHERE ClassLevelID = '$ClassLevelID' AND Semester = 'F' AND ReportID != '$ReportID'";
			$existTermReportID = $this->returnArray($sql, 2);
			$searchExistTermMode = "W";
		}
		
		if (sizeof($existTermReportID) > 0) {
			$completedTerm = array();
			$completedTermReportID = array();
			for($i=0; $i<sizeof($existTermReportID); $i++) {
				$errArr = array();
				#######################
				# comment the following line will enable consolidation of existing term mark even if it's not completed
				#$errArr = $this->checkCanGenernateReport($existTermReportID[$i]["ReportID"]);
				#######################
				
				// no errors = report was completed
				if (empty($errArr) && sizeof($errArr) == 0) {
					if ($existTermReportID[$i]["Semester"] == "F") {
						$existTermReportColumnData = $this->returnReportTemplateColumnData($existTermReportID[$i]["ReportID"]);
						for($j=0; $j<sizeof($existTermReportColumnData); $j++) {
							if (in_array($existTermReportColumnData[$j]["SemesterNum"], $reportInvolveTerms))
								$completedTermReportID[$existTermReportColumnData[$j]["SemesterNum"]] = $existTermReportID[$i]["ReportID"];
						}
					} else {
						$completedTermReportID[$existTermReportID[$i]["Semester"]] = $existTermReportID[$i]["ReportID"];
					}
				}
			}
			
			if (sizeof($completedTermReportID) > 0) {
				// retrieve existing term marks & consolidate them
				foreach ($completedTermReportID as $completedSemester => $completedReportID) {
					// skip copying if this report is an older report
					// MUST maintain the report creation order for this to work
					if ($searchExistTermMode != "T" && $ReportID < $completedReportID) {
						continue;
					}
					$tmpExistTermSubjectOverall = $this->COPY_EXIST_TERM_RESULT_SCORE($completedReportID, $completedSemester, $ReportID, $reportTermColumnMap, $searchExistTermMode);
					$tmpExistTermAverageTotal = $this->COPY_EXIST_TERM_RESULT($completedReportID, $completedSemester, $ReportID, $reportTermColumnMap);
					if (is_array($tmpExistTermSubjectOverall))
						$existTermSubjectOverallArr[] = $tmpExistTermSubjectOverall;
					
					if (is_array($tmpExistTermAverageTotal))
						$existTermAverageTotalArr[] = $tmpExistTermAverageTotal;
				}
			}
		}
		
		
		// $classArr[$i] + ["ClassID"], ["ClassName"]
		$classArr = $this->GET_CLASSES_BY_FORM($ClassLevelID);
		
		for($i=0; $i<sizeof($classArr); $i++) {
			// $studentArr[#ClassID][$i] + ["UserID"], ["WebSAMSRegNo"],...
			$studentArr[$classArr[$i]["ClassID"]] = $this->GET_STUDENT_BY_CLASS($classArr[$i]["ClassID"]);
		}
		
		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// for use in checking promotion criteria
		$classLevelName = $this->returnClassLevel($ClassLevelID);
		$nextPromoteClassLevel = $this->GET_NEXT_PROMOTED_CLASSLEVEL($classLevelName);
		$subjectCodeIDMap = $this->GET_SUBJECTS_CODEID_MAP();
		######## Hardcoded subject's CODEID required for checking promotion criteria ########
		$requirePassSubjectCodeID = array("101", "114");
		
		// Loop 1: Classes
		for($i=0; $i<sizeof($classArr); $i++) {
			// construct the $studentIDArr
			$classID = $classArr[$i]["ClassID"];
			$studentIDArr = array();
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			
			// skip this class if no students
			$numOfStudent = count($studentIDArr);
			if ($numOfStudent == 0) continue;
			
			
			### Add mark for newly added student which has no Term Report Records
			//for ($j=0; $j<$numOfStudent; $j++)
			//{
				//$thisStudentID = $studentIDArr[$j];
				foreach((array)$existTermAverageTotalArr as $thisIndex => $thisReportColumnMarkArr)
				{
					foreach((array)$thisReportColumnMarkArr as $thisReportColumnID => $thisStudentReportColumnMarkArr)
					{
						$thisTermReportStudentIDArr = array_keys((array)$thisStudentReportColumnMarkArr);
						$thisNewStudentIDArr = array_diff($studentIDArr, $thisTermReportStudentIDArr);
						$thisNewStudentIDArr = array_values($thisNewStudentIDArr);
						$numNewStudentID = count($thisNewStudentIDArr);
						
						for ($k=0; $k<$numNewStudentID; $k++)
						{
							$thisStudentID = $thisNewStudentIDArr[$k];
							
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['GrandTotal'] = -1;
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['GrandAverage'] = -1;
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['GPA'] = -1;
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['OrderMeritClass'] = -1;
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['OrderMeritForm'] = -1;
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['ClassNoOfStudent'] = '';
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['FormNoOfStudent'] = '';
						}
					}
				}
			//}
			
			if ($calculationOrder == 1) {
				// Loop 2: Subjects
				$studentSubjectOverallList = array();
				$studentSubjectColumnMark = array();
				foreach ($subjectList as $subjectID => $subjectInfo) {
					// $schemeInfo[0] is main info, [1] is ranges info
					$schemeInfo = $this->GET_GRADING_SCHEME_INFO($subjectInfo["schemeID"]);
					
					$schemeType = $schemeInfo[0]["SchemeType"];
					$passWording = $schemeInfo[0]["Pass"];
					$failWording = $schemeInfo[0]["Fail"];
					$subjectFullMark = $schemeInfo[0]["FullMark"];
					
					$scaleInput = $subjectInfo["scaleInput"];
					$scaleDisplay = $subjectInfo["scaleDisplay"];
					
					// get the grading scheme range info
					$schemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($subjectInfo["schemeID"]);
					for($m=0; $m<sizeof($schemeRangeInfo); $m++) {
						$schemeRangeIDGradeMap[$schemeRangeInfo[$m]["GradingSchemeRangeID"]] = $schemeRangeInfo[$m]["Grade"];
					}
					
					// check if it is a parent subject
					$CmpSubjectArr = array();
					$isCalculateByCmpSub = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
					$isCmpSubject = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($subjectID);
					if (!$isCmpSubject) {
						$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($subjectID, $ClassLevelID);
						$CmpSubjectIDArr = array();
						if(!empty($CmpSubjectArr)){	
							for($k=0 ; $k<count($CmpSubjectArr) ; $k++){
								$CmpSubjectIDArr[] = $CmpSubjectArr[$k]['SubjectID'];
								$CmpSubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$k]['SubjectID']);
								if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
									$isCalculateByCmpSub = 1;
							}
						}
					}
					
					$totalReportColumnWeight = 0;
					$reportColumnWeight = array();
					$reportColumnWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, "SubjectID = ".$subjectID);
					for($x=0 ; $x<sizeof($reportColumnWeightInfo) ; $x++){
						if ($reportColumnWeightInfo[$x]['ReportColumnID'] != "" && $reportColumnWeightInfo[$x]['Weight'] != "") {
							$reportColumnWeight[$reportColumnWeightInfo[$x]['ReportColumnID']] = $reportColumnWeightInfo[$x]['Weight'];
							$totalReportColumnWeight += $reportColumnWeightInfo[$x]['Weight'];
						}
					}
					
					################ Construct $AllColumnMarkArr #################
					$AllColumnMarkArr = array();
					// exist term mark in other report
					if (sizeof($existTermSubjectOverallArr) > 0) {
						for($a=0; $a<sizeof($studentIDArr); $a++) {
							for($w=0; $w<sizeof($existTermSubjectOverallArr); $w++) {
								if (isset($existTermSubjectOverallArr[$w][$studentIDArr[$a]][$subjectID])) {
									$tmpTermOverallDetail = $existTermSubjectOverallArr[$w][$studentIDArr[$a]][$subjectID];
									for ($p=0; $p<sizeof($tmpTermOverallDetail); $p++) {
										if ($tmpTermOverallDetail[$p]["Grade"] == "" && is_numeric($tmpTermOverallDetail[$p]["Mark"])) {
											$tmpTermOverallDetail[$p]["Mark"] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $tmpTermOverallDetail[$p]["ReportColumnID"], $subjectID, 1, $tmpTermOverallDetail[$p]["Mark"]);
											$tmpTermOverallMark = $this->ROUND_MARK($tmpTermOverallDetail[$p]["Mark"], "SubjectTotal");
											$AllColumnMarkArr[$studentIDArr[$a]][$tmpTermOverallDetail[$p]["ReportColumnID"]] = $tmpTermOverallMark;
										} else {
											$AllColumnMarkArr[$studentIDArr[$a]][$tmpTermOverallDetail[$p]["ReportColumnID"]] = $tmpTermOverallDetail[$p]["Grade"];
										}
									}
								}
							}
						}
					}
					
					for($n=0; $n<sizeof($reportColumnData); $n++) {
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						// get raw marks of all students in a class of this assessment
						if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub))
							$marksheetScore = $this->GET_MARKSHEET_SCORE($studentIDArr, $subjectID, $reportColumnID);
						else
						{
							$marksheetScore = $this->GET_STUDENT_TERM_SUBJECT_MARK_WITHOUT_TEMPLATE($studentIDArr, $ReportID, $reportColumnID, $subjectID, $CmpSubjectIDArr);
							//$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID, 1);
						}
						
						if (sizeof($marksheetScore) > 0) {
							foreach ($marksheetScore as $studentID => $scoreDetailInfo) {
								$scoreInfo = array();
								if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
									$scoreInfo = $scoreDetailInfo;
								} else {
									// skip consolidate this student if the mark is NULL
									if ($scoreDetailInfo == "")	continue;
									$scoreInfo["MarkRaw"] = $scoreDetailInfo;
									$scoreInfo["MarkNonNum"] = $scoreDetailInfo;
									$scoreInfo["MarkType"] = "M";
								}
								
								if($scoreInfo['MarkType'] == "M") {
									if ($scoreInfo["MarkRaw"] != "-1")
										$AllColumnMarkArr[$studentID][$reportColumnID] = $scoreInfo['MarkRaw'];
									else
										$AllColumnMarkArr[$studentID][$reportColumnID] = $scoreInfo['MarkNonNum'];
								} else {
									$AllColumnMarkArr[$studentID][$reportColumnID] = $scoreInfo['MarkNonNum'];
								}
							}
						}
					}
					
					
					// 20110531 Ivan: Added to cater the new student in Term 2
					// fill in "Exempt" for any missing score
					for($n=0; $n<sizeof($reportColumnData); $n++) {
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						for($g=0; $g<sizeof($studentIDArr); $g++) {
							if (!isset($AllColumnMarkArr[$studentIDArr[$g]][$reportColumnID]) || $AllColumnMarkArr[$studentIDArr[$g]][$reportColumnID]==='') {
								$AllColumnMarkArr[$studentIDArr[$g]][$reportColumnID] = "/";
								$calculationMark[$studentIDArr[$g]][$reportColumnID] = "/";
							}
						}
					}
					
					
					#################################################
					
					// Loop 3: ReportColumns (Terms)
					$calculationMark = array();
					for($n=0; $n<sizeof($reportColumnData); $n++) {
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						// check if this column already have consolidated data, if yes, skip consolidation of this column
						// Don't skip if the existing report is a newer report
						if (isset($completedTermReportID[$reportColumnTermMap[$reportColumnID]]) && $completedTermReportID[$reportColumnTermMap[$reportColumnID]] < $ReportID) {
							// retrieve consolidated data first for later usage???
							continue;
						}
						
						$CmpInfoArr = $this->CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID);
						$CmpSubjectArr = $CmpInfoArr[0];
						$CmpSubjectIDArr = $CmpInfoArr[1];
						$isCmpSubject = $CmpInfoArr[2];
						$isCalculateByCmpSub = $CmpInfoArr[3];
						// get raw marks of all students in a class of this assessment
						if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub))
							$marksheetScore = $this->GET_MARKSHEET_SCORE($studentIDArr, $subjectID, $reportColumnID);
						else
						{
							if ($isCalculateByCmpSub)
							{
								# updated by Ivan on 21 Jan 2009 - follow the calculation of export function
								//$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID,1);
								# updated by Ivan on 11 Feb 2009 - follow the calculation of marksheet preview page
								//$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID);
								$marksheetScore = $this->GET_STUDENT_TERM_SUBJECT_MARK_WITHOUT_TEMPLATE($studentIDArr, $ReportID, $reportColumnID, $subjectID, $CmpSubjectIDArr);
							}
							else
							{
								$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID);
							}
						}
						
						
						
						$semester = $reportColumnData[$n]["SemesterNum"]; 
						$conAssessmentMarkValues = array();
						
						// Loop 4: Student's Score
						if (sizeof($marksheetScore) > 0) {
							foreach ($marksheetScore as $studentID => $scoreDetailInfo) {
								$consolidateMark = "";
								$scoreInfo = array();
								if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
									$scoreInfo = $scoreDetailInfo;
								} else {
									// skip consolidate this student if the mark is NULL
									if ($scoreDetailInfo == "")	continue;
									$scoreInfo["MarkRaw"] = $scoreDetailInfo;
									$scoreInfo["MarkNonNum"] = $scoreDetailInfo;
									if ($scoreDetailInfo == "-")
									{
										$scoreInfo["MarkType"] = "SC";
									}
									else
									{
										$scoreInfo["MarkType"] = "M";
									}
								}
								
								$thisRawMark = $scoreInfo["MarkRaw"];
								if ($scoreInfo["MarkType"] == "M" || $scoreInfo["MarkType"] == "G") {
									if ($scaleInput == "M" && $scaleDisplay == "M") {
										// check if the consolidation mark need to be converted to weight mark
										if ($calculationSettings["UseWeightedMark"] == "1") {
											$consolidateMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $scoreInfo["MarkRaw"]);
										} else {
											$consolidateMark = $scoreInfo["MarkRaw"];
											# updated by Ivan on 11 Feb 2009 - follow the calculation of marksheet preview page
											//$consolidateMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $consolidateMark, 1, $AllColumnMarkArr);
										}
										
										// Horizontal -> Vertical: need to store weighted mark to calculate subject overall
										$calculationMark[$studentID][$reportColumnID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $scoreInfo["MarkRaw"], 1, $AllColumnMarkArr[$studentID]);
										
										#$calculationMark[$studentID][$reportColumnID] = $this->ROUND_MARK($calculationMark[$studentID][$reportColumnID], "SubjectScore");
									} else if ($scaleInput == "M" && $scaleDisplay == "G") {
										// convert to grade according to ranges
										$consolidateMark = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $scoreInfo["MarkRaw"]);
										#$calculationMark[$studentID][$reportColumnID] = $this->ROUND_MARK($calculationMark[$studentID][$reportColumnID], "SubjectScore");
									} else if ($scaleInput == "G" && $scaleDisplay == "G") {
										$consolidateMark = $schemeRangeIDGradeMap[$scoreInfo["MarkNonNum"]];
										$calculationMark[$studentID][$reportColumnID] = "-";
										$thisRawMark = 0;
									}
									
									// round off the weighted mark
									if (is_numeric($consolidateMark)) {
										$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
									}
									
									// consolidate the mark into DB (RC_REPORT_RESULT_SCORE)
									$mark = "";
									$grade = "";
									if (is_numeric($consolidateMark)) {
										$mark = $consolidateMark;
									} else {
										$grade = $consolidateMark;
									}
									
									$studentSubjectColumnMark[$reportColumnID][$studentID][$subjectID] = $consolidateMark;
									
									$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '$mark', '$grade', '$thisRawMark', '', '$semester', '', '', NOW(), NOW())";
								} else if ($scoreInfo["MarkType"] == "PF") {
									$consolidateMark = ($scoreInfo["MarkNonNum"] == "P") ? $passWording : $failWording;
									$PassFailMark = $consolidateMark;
									$thisRawMark = 0;
									$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$PassFailMark', '$thisRawMark', '', '$semester', '', '', NOW(), NOW())";
									$calculationMark[$studentID][$reportColumnID] = "-";
									$studentSubjectColumnMark[$reportColumnID][$studentID][$subjectID] = "-";
								} else {	
									$specialCase = $scoreInfo["MarkNonNum"];
									$thisRawMark = 0;
									$calculationMark[$studentID][$reportColumnID] = $specialCase;
									$studentSubjectColumnMark[$reportColumnID][$studentID][$subjectID] = $specialCase;
									$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$specialCase', '$thisRawMark', '', '$semester', '', '', NOW(), NOW())";
								}
							} // End Loop 4: Student's Score
						}
						
						
						// Subject mark order in a  class may be calculated here
						/* to be written */
						
						// insert the converted mark into DB
						if (sizeof($conAssessmentMarkValues) > 0) {
							$conAssessmentMarkValues = implode(",", $conAssessmentMarkValues);
							$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($conAssessmentMarkValues);
						}
					} // End Loop 3: ReportColumns
					
					// consolidate existing (entered by user directly) overall mark, for the case of input & display are both Grade
					$existOverallScoreInfo = $this->GET_MARKSHEET_OVERALL_SCORE($studentIDArr, $subjectID, $ReportID);

					$subjectOverallValues = array();
					$consolidatedSubjectOverallStudentList = array();
					if (sizeof($existOverallScoreInfo) > 0) {
						foreach ($existOverallScoreInfo as $markOverallStudentID => $markOverallInfo) {
							$thisRawMark = $markOverallInfo["MarkRaw"];
								
							$consolidatedSubjectOverallStudentList[] = $markOverallStudentID;
							if ($markOverallInfo["MarkType"] == "G") {
								$thisRawMark = 0;
								$gradeToConsolidate = $schemeRangeIDGradeMap[$markOverallInfo["MarkNonNum"]];
								$subjectOverallValues[] = "('$markOverallStudentID', '$subjectID', '$ReportID', '', '', '$gradeToConsolidate', '$thisRawMark', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
							} else if ($markOverallInfo["MarkType"] == "SC") {
								$thisRawMark = 0;
								$subjectOverallValues[] = "('$markOverallStudentID', '$subjectID', '$ReportID', '', '', '".$markOverallInfo["MarkNonNum"]."', '$thisRawMark', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
							}
						}
						if (sizeof($subjectOverallValues) > 0) {
							$subjectOverallValues = implode(",", $subjectOverallValues);
							$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($subjectOverallValues);
						}
					}
					
					// calculate the subject overall mark of each student
					$subjectOverallValues = array();
					
					for($a=0; $a<sizeof($studentIDArr); $a++) {
						// skip calculation of subject overall mark of this student since it already been entered by user directly and consolidated above
						if (in_array($studentIDArr[$a], $consolidatedSubjectOverallStudentList))
							continue;
						$subjectOverallMark = "";
						$excludeColumnWeight = 0;
						$excludeFullMark = 0;
						$specialCaseDifferentiate = array();
						
						$CountedReportColumnIDArr = array(); 
						if (sizeof($calculationMark) > 0) {
							if (isset($calculationMark[$studentIDArr[$a]])) {
								$markList = $calculationMark[$studentIDArr[$a]];
								
								// loop column by column and sum up the overall mark & exclude weight
								foreach ($markList as $markColumnID => $markColumn) {
									if (is_numeric($markColumn)) {
										// if any of the assessment marks is numeric, make sure $subjectOverallMark is initiate to zero
										$subjectOverallMark = ($subjectOverallMark == "") ? 0 : $subjectOverallMark;
										$subjectOverallMark += $markColumn;
									} else {
										$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $markColumn, $absentExemptSettings);
										
										if ($specialCaseDifferentiate[$markColumn]["isExFullMark"] == 1) {
											$excludeFullMark += $subjectFullMark*$reportColumnWeight[$markColumnID];
										} else if ($specialCaseDifferentiate[$markColumn]["isCount"] == 0) {
											// not in use since CONVERT_MARK_TO_WEIGHTED_MARK is used above
											$excludeColumnWeight += $reportColumnWeight[$markColumnID];
										}
									}
									$CountedReportColumnIDArr[] = $markColumnID;
								}
							}
						}
						
						// if there exist consolidated term overall mark
						$thisSubjectTotalFullMark = 0;
						for($w=0; $w<sizeof($existTermSubjectOverallArr); $w++) {
							if (isset($existTermSubjectOverallArr[$w][$studentIDArr[$a]][$subjectID])) {
								$tmpTermOverallDetail = $existTermSubjectOverallArr[$w][$studentIDArr[$a]][$subjectID];
								
								for ($p=0; $p<sizeof($tmpTermOverallDetail); $p++) {
									$thisSubjectTotalFullMark += $subjectFullMark * $reportColumnWeight[$tmpTermOverallDetail[$p]["ReportColumnID"]];
									
									if ($tmpTermOverallDetail[$p]["Grade"] == "" && is_numeric($tmpTermOverallDetail[$p]["Mark"])) {
										$subjectOverallMark = ($subjectOverallMark == "") ? 0 : $subjectOverallMark;
										// only convert the mark if the existing mark is previously consolidated as Raw mark
										if ($calculationSettings["UseWeightedMark"] != "1") {
											$tmpTermOverallDetail[$p]["Mark"] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $tmpTermOverallDetail[$p]["ReportColumnID"], $subjectID, $tmpTermOverallDetail[$p]["Mark"], 1, $AllColumnMarkArr[$studentIDArr[$a]]);
										}
										
										$subjectOverallMark += $tmpTermOverallDetail[$p]["Mark"];
									} else {
										if ($tmpTermOverallDetail[$p]["Grade"] == "abs") {
											$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $tmpTermOverallDetail[$p]["Grade"], $absentExemptSettings);
										} else if (in_array($tmpTermOverallDetail[$p]["Grade"], $SpecialCaseArrH)) {
											$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $tmpTermOverallDetail[$p]["Grade"], $absentExemptSettings);
										}
										
										if ($specialCaseDifferentiate[$tmpTermOverallDetail[$p]["Grade"]]["isExFullMark"] == 1) {
											$excludeFullMark += $subjectFullMark*$reportColumnWeight[$tmpTermOverallDetail[$p]["ReportColumnID"]];
											
										} else {
											$excludeColumnWeight += $reportColumnWeight[$tmpTermOverallDetail[$p]["ReportColumnID"]];
										}
									}
								}
							}
							else {
								// 20110531 Ivan: Added to cater the new student in Term 2
								for($n=0; $n<sizeof($reportColumnData); $n++) {
									$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
									
									if (!in_array($reportColumnID, (array)$CountedReportColumnIDArr)) {
										$excludeColumnWeight += $reportColumnWeight[$reportColumnID];
									}
								}
							}
						}
						
						
						
						// in case of excluding full mark
						if ($excludeFullMark > 0) {
							$newFullMark = $subjectFullMark - $excludeFullMark;
							$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK($newFullMark, $ReportID, $studentIDArr[$a], $subjectID);
						}
						
						$thisRawMark = '';
						
						
						// subject overall mark can be calculated as number
						if ($subjectOverallMark !=="" && is_numeric($subjectOverallMark)) {
							// adjust the subject overall mark according to remaining weight
							if ($totalReportColumnWeight == $excludeColumnWeight) 
							{
								$subjectOverallMark = "-";
								$thisRawMark = 0;
							} 
							else if ($excludeFullMark > 0)
							{
								if ($excludeFullMark == $thisSubjectTotalFullMark) {
									$subjectOverallMark = "N.A.";
									$thisRawMark = 0;
								}
								else {
									# added on 25 Jun 2009
									# handle the case of Term 1: N.A. and Term 2 has marks
									$subjectOverallMark = ($subjectOverallMark / ($subjectFullMark - $excludeFullMark)) * $subjectFullMark;
									$thisRawMark = $subjectOverallMark;
//									$subjectOverallMark = $this->ROUND_MARK($subjectOverallMark, "SubjectTotal");
									$subjectOverallMark = $this->ROUND_MARK($subjectOverallMark, "GrandTotal");
								}
							}
							else 
							{
								if ($isCalculateByCmpSub)
									$subjectOverallMark = $subjectOverallMark/($totalReportColumnWeight-$excludeColumnWeight);
									
								$thisRawMark = $subjectOverallMark;
								$subjectOverallMark = $this->ROUND_MARK($subjectOverallMark, "GrandTotal");
//						PMS		$subjectOverallMark = $this->ROUND_MARK($subjectOverallMark, "SubjectTotal");
							}
						} else { // subject cannot be calculated as number
							if (sizeof($specialCaseDifferentiate) == 1) {
								$onlySpecialCase = array_keys($specialCaseDifferentiate);
								$subjectOverallMark = $onlySpecialCase[0];
							} else {
								$subjectOverallMark = 0;
							}
							$thisRawMark = 0;
						}
						
						// for calculating average mark
						if (!$isCmpSubject) {
							$studentSubjectOverallList[$studentIDArr[$a]][$subjectID] = $subjectOverallMark;
						}
						
						if (is_numeric($subjectOverallMark)) {
							if (in_array($subjectCodeIDMap[$subjectID], $requirePassSubjectCodeID)) {
								$promoteCriteriaSubjectMark[$studentIDArr[$a]][$subjectCodeIDMap[$subjectID]] = $subjectOverallMark;
							}
							$subjectOverallValues[] = "('".$studentIDArr[$a]."', '$subjectID', '$ReportID', '', '$subjectOverallMark', '', '$thisRawMark', '1', 'F', '', '', NOW(), NOW())";
						} else {
							$subjectOverallValues[] = "('".$studentIDArr[$a]."', '$subjectID', '$ReportID', '', '', '$subjectOverallMark', '$thisRawMark', '1', 'F', '', '', NOW(), NOW())";
						}
					}
					
					// consolidate the resulting subject overall mark
					if (sizeof($subjectOverallValues) > 0) {
						$subjectOverallValues = implode(",", $subjectOverallValues);
						$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($subjectOverallValues);
					}
				} // End Loop 2: Subjects
				
				### Calculate Grand Marks of each assessment (Vertical Calculation)
				if (sizeof($studentSubjectColumnMark) > 0) 
				{
					$thisTermAverageValues = array();
					foreach((array)$studentSubjectColumnMark as $thisReportColumnID => $columnMarkList) 
					{
						// calculate the term average mark (vertically across subjects) of each student
						foreach((array)$columnMarkList as $markStudentID => $markList) {
							//$termAverageMark = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $markStudentID, $markList, '', 1);
							$termAverageMark = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $markStudentID, $markList, $thisReportColumnID, 1);
							
							// round grand total according to the same setting of average???
							$tmpTotal = $this->ROUND_MARK($termAverageMark["GrandTotal"], "GrandAverage");
							$tmpAverage = $this->ROUND_MARK($termAverageMark["GrandAverage"], "GrandAverage");
							$tmpAverageRaw = $termAverageMark["GrandAverage"];
							
							$thisTermAverageValues[] = "('$markStudentID', '$ReportID', '$thisReportColumnID', '$tmpTotal', '$tmpAverage', '$tmpAverageRaw', '', '', '', '', '', '', '', '', NOW(), NOW())";
						}
					}
					
					// consolidate the term average & total mark
					if (sizeof($thisTermAverageValues) > 0) {
						$thisTermAverageValues = implode(",", $thisTermAverageValues);
						$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($thisTermAverageValues);
					}
				}
				
				// calculate average mark
				// for Horizontal to Vertical, only average mark of subject overall need to be calculated
				$reportAverageTotal = array();
				$grandValues = array();
				// order, gpa yet to be done.... 
				
				foreach ($studentSubjectOverallList as $studentID => $subjectOverallList) {

					$reportAverageTotal[$studentID] = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $studentID, $subjectOverallList, '', 1, 1);
					// round grand total according to the same setting of average???
					$tmpTotal = $this->ROUND_MARK($reportAverageTotal[$studentID]["GrandTotal"], "GrandAverage");
					$tmpAverage = $this->ROUND_MARK($reportAverageTotal[$studentID]["GrandAverage"], "GrandAverage");
					$tmpAverageRaw = $reportAverageTotal[$studentID]["GrandAverage"];
					
					// check if the student can be promoted based on pre-set criteria
					if (isset($promoteCriteriaSubjectMark[$studentID])) {
						$isPromoted = $this->IS_PROMOTED($classLevelName, $tmpAverage, $promoteCriteriaSubjectMark[$studentID]);
					} else {
						$isPromoted = $this->IS_PROMOTED($classLevelName, $tmpAverage);
					}
					$newClassName = ($isPromoted == 1) ? $nextPromoteClassLevel : $classLevelName;
					$grandValues[] = "('$studentID', '$ReportID', '', '$tmpTotal', '$tmpAverage', '$tmpAverageRaw', '', '', '', '$isPromoted', '$isPromoted', '$newClassName', 'F', '', NOW(), NOW())";
				}
				// consolidate grand total & grand average
				// no value for ReportColumnID
				$grandValues = implode(",", $grandValues);
				$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($grandValues);
			// Vertical > Horizontal: All subject have a common weight (as a preview only, since these weight is actually for average mark)
			} else if ($calculationOrder == 2) {
				// get the weight (NOT really meaningful for calculating subject overall mark)
				for($i=0; $i<sizeof($reportColumnData); $i++) {
					$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$reportColumnData[$i]["ReportColumnID"];
					$reportColumnWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
					$reportColumnWeight[$reportColumnWeightInfo[0]["ReportColumnID"]] = $reportColumnWeightInfo[0]['Weight'];
					$totalReportColumnWeight += $reportColumnWeightInfo[0]['Weight'];
				}
				
				$grandAverageTotalList = array();
				// Loop 2: ReportColumns
				for($i=0; $i<sizeof($reportColumnData); $i++) {
					$reportColumnID = $reportColumnData[$i]["ReportColumnID"];
					$calculationMark = array();
					if (!isset($completedTermReportID[$reportColumnTermMap[$reportColumnID]])) {
						// Loop 3: Subjects
						foreach ($subjectList as $subjectID => $subjectInfo) {
							// $schemeInfo[0] is main info, [1] is ranges info
							$schemeInfo = $this->GET_GRADING_SCHEME_INFO($subjectInfo["schemeID"]);
							
							$schemeType = $schemeInfo[0]["SchemeType"];
							$passWording = $schemeInfo[0]["Pass"];
							$failWording = $schemeInfo[0]["Fail"];
							
							$scaleInput = $subjectInfo["scaleInput"];
							$scaleDisplay = $subjectInfo["scaleDisplay"];
							
							// get the grading scheme range info
							$schemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($subjectInfo["schemeID"]);
							for($m=0; $m<sizeof($schemeRangeInfo); $m++) {
								$schemeRangeIDGradeMap[$schemeRangeInfo[$m]["GradingSchemeRangeID"]] = $schemeRangeInfo[$m]["Grade"];
							}
							
							// check if it is a parent subject
							$CmpSubjectArr = array();
							$isCalculateByCmpSub = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
							$isCmpSubject = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($subjectID);
							if (!$isCmpSubject) {
								$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($subjectID, $ClassLevelID);
								$CmpSubjectIDArr = array();
								if(!empty($CmpSubjectArr)){	
									for($k=0 ; $k<count($CmpSubjectArr) ; $k++){
										$CmpSubjectIDArr[] = $CmpSubjectArr[$k]['SubjectID'];
										$CmpSubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$k]['SubjectID']);
										if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
											$isCalculateByCmpSub = 1;
									}
								}
							}
							
							// get raw marks of all students in a class of this assessment
							if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub))
								$marksheetScore = $this->GET_MARKSHEET_SCORE($studentIDArr, $subjectID, $reportColumnID);
							else
								$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID);
								
							// Loop 4: Student's Score
							foreach ($marksheetScore as $studentID => $scoreDetailInfo) {
								$AllColumnMarkArr = array();
								$consolidateMark = "";
								$scoreInfo = array();
								if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
									$scoreInfo = $scoreDetailInfo;
								} else {
									$scoreInfo["MarkRaw"] = $scoreDetailInfo;
									$scoreInfo["MarkNonNum"] = $scoreDetailInfo;
									$scoreInfo["MarkType"] = "M";
								}
								if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
									$scoreInfo = $scoreDetailInfo;
								} else {
									$scoreInfo["MarkRaw"] = $scoreDetailInfo;
									$scoreInfo["MarkNonNum"] = $scoreDetailInfo;
									$scoreInfo["MarkType"] = "M";
								}
								
								$thisRawMark = $scoreInfo["MarkRaw"];
								if ($scoreInfo["MarkType"] == "M" || $scoreInfo["MarkType"] == "G") {
									if ($scaleInput == "M" && $scaleDisplay == "M") {
										// check if the consolidation mark need to be converted to weight mark
										if ($calculationSettings["UseWeightedMark"] == "1") {
											$consolidateMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $scoreInfo["MarkRaw"]);
										} else {
											$consolidateMark = $scoreInfo["MarkRaw"];
										}
										// Horizontal -> Vertical: need to store weighted mark to calculate subject overall
										$calculationMark[$studentID][$subjectID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $scoreInfo["MarkRaw"]);
										$calculationMark[$studentID][$subjectID] = $this->ROUND_MARK($calculationMark[$studentID][$reportColumnID], "SubjectScore");
									} else if ($scaleInput == "M" && $scaleDisplay == "G") {
										// convert to grade according to ranges
										$consolidateMark = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $scoreInfo["MarkRaw"]);
										$calculationMark[$studentID][$subjectID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $scoreInfo["MarkRaw"]);
										$calculationMark[$studentID][$subjectID] = $this->ROUND_MARK($calculationMark[$studentID][$reportColumnID], "SubjectScore");
									} else if ($scaleInput == "G" && $scaleDisplay == "G") {
										$consolidateMark = $schemeRangeIDGradeMap[$scoreInfo["MarkNonNum"]];
										$calculationMark[$studentID][$subjectID] = "-";
										$thisRawMark = 0;
									}
									
									// round off the weighted mark
									if (is_numeric($consolidateMark)) {
										$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
									}
									
									// consolidate the mark into DB (RC_REPORT_RESULT_SCORE)
									$mark = "";
									$grade = "";
									if (is_numeric($consolidateMark)) {
										$mark = $consolidateMark;
									} else {
										$grade = $consolidateMark;
									}
									
									$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '$mark', '$grade', '$thisRawMark', '', '$semester', '', '', NOW(), NOW())";
								} else if ($scoreInfo["MarkType"] == "PF") {
									$consolidateMark = ($scoreInfo["MarkNonNum"] == "P") ? $passWording : $failWording;
									$PassFailMark = $consolidateMark;
									$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$PassFailMark', '$thisRawMark', '', '$semester', '', '', NOW(), NOW())";
									$calculationMark[$studentID][$subjectID] = "-";
									$thisRawMark = 0;
								} else {
									$thisRawMark = 0;
									if ($schemeType == "PF") {
										#$specialCase = $this->GET_MARKSHEET_SPECIAL_CASE_SET2_STRING($scoreInfo["MarkNonNum"]);
										$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $scoreInfo["MarkNonNum"], $absentExemptSettings);
									} else {
										#$specialCase = $this->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($scoreInfo["MarkNonNum"]);
										$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $scoreInfo["MarkNonNum"], $absentExemptSettings);
									}
									$specialCase = $scoreInfo["MarkNonNum"];
									$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$specialCase', '$thisRawMark', '', '$semester', '', '', NOW(), NOW())";
									
									if ($specialCaseDifferentiate[$scoreInfo["MarkNonNum"]]["isExFullMark"] == 1) {
										$calculationMark[$studentID][$subjectID] = "*";
									} else if ($specialCaseDifferentiate[$scoreInfo["MarkNonNum"]]["isCount"] == 0)
										$calculationMark[$studentID][$subjectID] = "-";
									else
										$calculationMark[$studentID][$subjectID] = $specialCaseDifferentiate[$scoreInfo["MarkNonNum"]]["Value"];
								}
							} // End Loop 4
							
							// Subject mark order in a  class may be calculated here
							/* to be written */
							
							// insert the converted mark into DB
							if (sizeof($conAssessmentMarkValues) > 0) {
								$conAssessmentMarkValues = implode(",", $conAssessmentMarkValues);
								$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($conAssessmentMarkValues);
							}
						} // End Loop 3: Subjects
					}
					
					// calculate the term average mark (vertically across subjects) of each student
					$termAverageValues = array();
					$termAverageMark = "";
					if (sizeof($calculationMark) > 0) {
						foreach ($calculationMark as $markStudentID => $markList) {
							$termAverageMark = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $markStudentID, $markList, $reportColumnID);
							// didn't consider the extreme case with no average mark can be calculated
							$grandAverageTotalList[$markStudentID]["GrandTotal"] += $termAverageMark["GrandTotal"] * $reportColumnWeight[$reportColumnID];
							$grandAverageTotalList[$markStudentID]["GrandAverage"] += $termAverageMark["GrandAverage"] * $reportColumnWeight[$reportColumnID];
							// round grand total according to the same setting of average???
							$tmpTotal = $this->ROUND_MARK($termAverageMark["GrandTotal"], "GrandAverage");
							$tmpAverage = $this->ROUND_MARK($termAverageMark["GrandAverage"], "GrandAverage");
							$tmpAverageRaw = $termAverageMark["GrandAverage"];
							$termAverageValues[] = "('$markStudentID', '$ReportID', '$reportColumnID', '$tmpTotal', '$tmpAverage', '$tmpAverageRaw', '', '', '', '', '', '', '".$reportColumnTermMap[$reportColumnID]."', '', NOW(), NOW())";
						}
					}
					
					// exist term average mark
					for($y=0; $y<sizeof($existTermAverageTotalArr); $y++) {
						if (isset($existTermAverageTotalArr[$y][$reportColumnID])) {
							$existStudentAverageTotalArr = $existTermAverageTotalArr[$y][$reportColumnID];
							foreach ($existStudentAverageTotalArr as $markStudentID => $markAverageTotalInfo) {
								if (!isset($grandAverageTotalList[$markStudentID]["GrandTotal"]))
									$grandAverageTotalList[$markStudentID]["GrandTotal"] = 0;
								if (!isset($grandAverageTotalList[$markStudentID]["GrandAverage"]))
									$grandAverageTotalList[$markStudentID]["GrandAverage"] = 0;
								$grandAverageTotalList[$markStudentID]["GrandTotal"] += $markAverageTotalInfo["GrandTotal"] * $reportColumnWeight[$reportColumnID];
								$grandAverageTotalList[$markStudentID]["GrandAverage"] += $markAverageTotalInfo["GrandAverage"] * $reportColumnWeight[$reportColumnID];
							}
						}
					}
					
					// consolidate the term average & total mark
					if (sizeof($termAverageValues) > 0) {
						$termAverageValues = implode(",", $termAverageValues);
						$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($termAverageValues);
					}
				} // End Loop 2: ReportColumns
				
				// consolidate the grand average & total mark
				if (sizeof($grandAverageTotalList) > 0) {
					$grandAverageValues = array();
					foreach ($grandAverageTotalList as $grandStudentID => $grandList) {
						// round grand total according to the same setting of average???
						$tmpTotal = $this->ROUND_MARK($grandList["GrandTotal"], "GrandAverage");
						$tmpAverage = $this->ROUND_MARK($grandList["GrandAverage"], "GrandAverage");
						$tmpAverageRaw = $grandList["GrandAverage"];
						$grandAverageValues[] = "('$grandStudentID', '$ReportID', '', '$tmpTotal', '$tmpAverage', '$tmpAverageRaw', '', '', '', '', '', '', 'F', '', NOW(), NOW())";
					}
					$grandAverageValues = implode(",", $grandAverageValues);
					$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($grandAverageValues);
				}
			} // End Condition: CalculationOrder
		}
		
		// Update order (class & form)
		$this->UPDATE_ORDER($ReportID, $ClassLevelID);
	}
		
	function CREATE_SCORE_INFO_ARRAY($score) {
		$scoreInfo = array();
		if (is_numeric($score)) {
			$scoreInfo["MarkRaw"] = $score;
			$scoreInfo["MarkNonNum"] = $score;
			$scoreInfo["MarkType"] = "M";
		} else {
			$scoreInfo["MarkRaw"] = "-1";
			$scoreInfo["MarkNonNum"] = $score;
			if (in_array($scoreInfo["MarkNonNum"], $this->specialCasesSet1) || in_array($scoreInfo["MarkNonNum"], $this->specialCasesSet2))
				$scoreInfo["MarkType"] = "SC";
			else
				$scoreInfo["MarkType"] = "G";
		}
		return $scoreInfo;
	}
	
	function INSERT_REPORT_RESULT_SCORE($values) {
		#>>>> IP25 
		if (trim($values)=='') {
			return false;
		}
		#<<<<<<<
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$fields = "StudentID, SubjectID, ReportID, ReportColumnID, Mark, Grade, RawMark, IsOverall, Semester, OrderMeritClass, OrderMeritForm, DateInput, DateModified";
		$sql = "INSERT INTO $table ($fields) VALUES $values";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function DELETE_REPORT_RESULT_SCORE($ReportID) {
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sql = "DELETE FROM $table WHERE ReportID = '$ReportID'";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function INSERT_REPORT_RESULT($values) {
		if (trim($values)=='') {
			return false;
		}
		
		$table = $this->DBName.".RC_REPORT_RESULT";
//ip25		$fields = "StudentID, ReportID, ReportColumnID, GrandTotal, GrandAverage, GPA, OrderMeritClass, OrderMeritForm, NewClassName, Semester, AdjustedBy, DateInput, DateModified";
/*PMS*/		$fields = "StudentID, ReportID, ReportColumnID, GrandTotal, GrandAverage, RawGrandAverage, GPA, OrderMeritClass, OrderMeritForm, Promotion, Passed, NewClassName, Semester, AdjustedBy, DateInput, DateModified";
		$sql = "INSERT INTO $table ($fields) VALUES $values";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function DELETE_REPORT_RESULT($ReportID) {
		$table = $this->DBName.".RC_REPORT_RESULT";
		$sql = "DELETE FROM $table WHERE ReportID = '$ReportID'";
		return $this->db_db_query($sql);
	}
	#IP25Only
	function UPDATE_REPORT_RESULT($ResultID, $ActualAverage) {
		$table = $this->DBName.".RC_REPORT_RESULT";
		$sql = "UPDATE $table Set ActualAverage = '".$ActualAverage."' Where ResultID = '".$ResultID."'";
		return $this->db_db_query($sql);
	}
	
	# INSERT the new full mark of subject overall (horizontal) or assessment/term total (vertical) in case of excluding full mark
	function INSERT_NEW_FULLMARK($newFullMark, $ReportID, $StudentID, $SubjectID="", $ReportColumnID="") {
		$table = $this->DBName.".RC_REPORT_RESULT_FULLMARK";
		$fields = "StudentID, ReportID, ReportColumnID, SubjectID, FullMark, DateInput, DateModified";
		$values = "'$StudentID', '$ReportID', '$ReportColumnID', '$SubjectID', '$newFullMark', NOW(), NOW()";
		$sql = "INSERT INTO $table ($fields) VALUES ($values)";
		return $this->db_db_query($sql);
	}
	
	function DELETE_NEW_FULLMARK($ReportID) {
		$table = $this->DBName.".RC_REPORT_RESULT_FULLMARK";
		$sql = "DELETE FROM $table WHERE ReportID = '$ReportID'";
		return $this->db_db_query($sql);
	}
	
	# UPDATE the OrderMerit field of existing record in RC_REPORT_RESULT_SCORE
	# $OrderList is the sorted list of StudentID
	# $Type = "Class" or "Form"
	function UPDATE_SCORE_ORDER_MERIT($OrderList, $Type, $SubjectID, $ReportID, $ReportColumnID=0) {
		$orderField = "OrderMerit".$Type;
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sqlPart = "UPDATE $table SET $orderField =";
		$cond = "WHERE ReportID = '$ReportID' AND ReportColumnID = '$ReportColumnID' AND SubjectID = '$SubjectID' AND StudentID =";
		
		$success = array();
		for($i=0; $i<sizeof($OrderList); $i++) {
			$order = $i+1;
			$sql = "$sqlPart '$order' $cond '".$OrderList[$i]."'";
			$success[] = $this->db_db_query($sql);
		}
		
		return !in_array(0, $success);
	}
	
	function CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID,$ReportID='') {
		$PreloadArrKey = 'CHECK_HAVE_CMP_SUBJECT';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	
		// check if it is a parent subject, if yes find info of its components subjects
		$CmpSubjectArr = array();
		$CmpSubjectIDArr = array();
		$isCalculateByCmpSub = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
		$isCmpSubject = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($subjectID);
		if (!$isCmpSubject) {
			$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($subjectID, $ClassLevelID);
			if(!empty($CmpSubjectArr)){
				for($k=0 ; $k<count($CmpSubjectArr) ; $k++){
					$CmpSubjectIDArr[] = $CmpSubjectArr[$k]['SubjectID'];
					$CmpSubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$k]['SubjectID'],0,0,$ReportID);
//noreportID PMS	$CmpSubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$k]['SubjectID']);
					if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
						$isCalculateByCmpSub = 1;
				}
			}
		}
		$returnArr = array($CmpSubjectArr, $CmpSubjectIDArr, $isCmpSubject, $isCalculateByCmpSub);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $returnArr);
		return $returnArr;
	}
	
	# Calculate the Assessment/Term total across subjects
		function CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $StudentID, $SubjectOverallList, $ReportColumnID="", $CheckSubjectExclusion=0, $RoundSubjectMark=0) {
		global $PATH_WRT_ROOT,$eRCTemplateSetting;
		
//		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		
		$averageBase = 100;
		
		$calculationSettings = $this->LOAD_SETTING("Calculation");
		$isAbjustFullMark = $calculationSettings["AdjustToFullMarkFirst"];
		
		$absentExemptSettings = $this->LOAD_SETTING("Absent&Exempt");
		
		// get subject gradnig info
//		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
		
		// get subject weight info of the column
		if ($ReportColumnID == "")
			$cond = "ReportColumnID IS NULL";
		else
			$cond = "ReportColumnID = '$ReportColumnID'";
		$subjectWeightTemp = $this->returnReportTemplateSubjectWeightData($ReportID, $cond);
		
		for($i=0; $i<sizeof($subjectWeightTemp); $i++) {
			$subjectWeight[$subjectWeightTemp[$i]["SubjectID"]] = $subjectWeightTemp[$i]["Weight"];
		}
		
		// preload data for subject exclusion
		if ($CheckSubjectExclusion)
		{
			$activeYear = $this->GET_ACTIVE_YEAR();
			$classLevelName = $this->returnClassLevel($ClassLevelID);
			$excludeSubjectIDArr = $eRCCalculationSetting['ExcludeSubjectInGrandMark'][$activeYear]['SubjectID'];
			$excludeClassLevelNameArr = $eRCCalculationSetting['ExcludeSubjectInGrandMark'][$activeYear]['ClassLevelName'];
			
			$haveExcludeSubject = (is_array($excludeSubjectIDArr))? 1 : 0;
		}
		
		$weight = 0;
		$columnTotalMark = 0;
		$schemeFullMark = 0;
		$columnFullMark = 0;
		$excludeFullMark = 0;
		$excludeWeight = 0;
		$totalWeight = 0;
		
		// for calculation of average
		$totalDenomenator = 0;
		$excludeDenomenator = 0;
		$columnNumerator = 0;
		
		$specialCaseDifferentiate = array();
		$specialCaseSetH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		$specialCaseSetPF = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
		
		foreach ($subjectList as $subjectID => $subjectInfo) {
			
			# Skip the subject if the student is in specific class level and the subject is in the exclusion list
			if ($CheckSubjectExclusion && $haveExcludeSubject)
			{
				if (in_array($classLevelName, $excludeClassLevelNameArr) && in_array($subjectID, $excludeSubjectIDArr))
				{
					continue;
				}
			}
			
			$weight = $subjectWeight[$subjectID];
			
			// check if it is a parent subject
			$isCmpSubject = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($subjectID);
			if ($isCmpSubject) {
				continue;
			}
			
			// get grading scheme
			$schemeInfo = $this->GET_GRADING_SCHEME_INFO($subjectInfo["schemeID"]);
			$schemeType = $schemeInfo[0]["SchemeType"];
			$schemeFullMark = $schemeInfo[0]["FullMark"];
			
			$columnFullMark += $schemeFullMark*$weight;
			
			$scaleInput = $subjectInfo["scaleInput"];
			$scaleDisplay = $subjectInfo["scaleDisplay"];
			
			$totalWeight += $schemeFullMark*$weight;
			$totalDenomenator += ($isAbjustFullMark) ? $weight : $schemeFullMark*$weight;
			
			if (isset($SubjectOverallList[$subjectID])) {
				$score = $SubjectOverallList[$subjectID];
				if ($score !== "") {
					// Numeric -> can sum it up
					if (is_numeric($score) && $score > 0) {
						// mark will only be counted for this case
						$columnTotalMark += $score*$weight;
						$columnNumerator += ($isAbjustFullMark) ? ($score/$schemeFullMark)*$weight : $score*$weight;
											
						
					} else {	// either Grade or Special Case
						if ($score == "abs" || in_array($score, $specialCaseSetH)) {
							// #########Customize for SIS#########
							// when calculating grand total/average, special handling of special case
							if (($ReportColumnID == "" || $ReportColumnID == "0") && ($score == "/" || $score == "*" || $score == "N.A.")) {
									$totalDenomenator -= ($isAbjustFullMark) ? $weight : $schemeFullMark*$weight;
									$excludeFullMark += $schemeFullMark*$weight;
							} else {
								if ($score == "abs") {	// "abs" only appear as special case for Pass/Fail based
									$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $score, $absentExemptSettings);
								} else if (in_array($score, $specialCaseSetH)) {	// other special case
									$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $score, $absentExemptSettings);
								}
								
								if ($specialCaseDifferentiate[$score]["isExFullMark"] == 1) {
									$totalDenomenator -= ($isAbjustFullMark) ? $weight : $schemeFullMark*$weight;
									$excludeFullMark += $schemeFullMark*$weight;
								} else if ($specialCaseDifferentiate[$score]["isCount"] == 0) {
									$excludeWeight += $schemeFullMark*$weight;
									$excludeDenomenator += ($isAbjustFullMark) ? $weight : $schemeFullMark*$weight;
								} else {
									$columnTotalMark += $specialCaseDifferentiate[$score]["Value"]*$weight;
									$columnNumerator += ($isAbjustFullMark) ? ($score/$schemeFullMark)*$weight : $score*$weight;
									
								}
							}
						} else {	// score is a grade or P/F
							// Exclude weight by default
							$excludeWeight += $schemeFullMark*$weight;
							$excludeDenomenator += ($isAbjustFullMark) ? $weight : $schemeFullMark*$weight;
						}
					}
				} else {
					$excludeWeight += $schemeFullMark*$weight;
					$excludeDenomenator += ($isAbjustFullMark) ? $weight : $schemeFullMark*$weight;
				}
			} else {
				$excludeWeight += $schemeFullMark*$weight;
				$excludeDenomenator += ($isAbjustFullMark) ? $weight : $schemeFullMark*$weight;
			}
		}
		
		// insert new full mark
		if ($excludeFullMark > 0) {
			$newFullMark = $columnFullMark - $excludeFullMark;
			$success = $this->INSERT_NEW_FULLMARK($newFullMark, $ReportID, $StudentID, "", $ReportColumnID);
			if (!$success)
				echo "Error inserting new fullmark.";
		}
		
		$remainWeight = $totalWeight-$excludeWeight;
		$remainDenomenator = $totalDenomenator-$excludeDenomenator;
		
		# insert new grandtotal full mark for exclusion of ������������踝蕭
		if ($CheckSubjectExclusion && $haveExcludeSubject)
		{
			if (in_array($classLevelName, $excludeClassLevelNameArr))
			{
				$newFullMark = $remainDenomenator;
				$success = $this->INSERT_NEW_FULLMARK($newFullMark, $ReportID, $StudentID, "", $ReportColumnID);
				if (!$success)
					echo "Error inserting new fullmark.";
			}
		}
		
		// If all subject marks are of the same special case, assign GrandTotal & GrandAverage to that special case also
		if (sizeof($specialCaseDifferentiate) == 1) {
			$onlySpecialCase = array_keys($specialCaseDifferentiate);
			if ($specialCaseDifferentiate[$onlySpecialCase[0]]["Count"] == sizeof($subjectList)) {
				$returnArr["GrandTotal"] = $onlySpecialCase[0];
				$returnArr["GrandAverage"] = $onlySpecialCase[0];
				return $returnArr;
			}
		}
		
		
		$returnArr["GrandTotal"] = ($remainWeight == 0) ? 0 : $columnTotalMark;
		if ($RoundSubjectMark==1)
			$columnNumerator = $this->ROUND_MARK($columnNumerator, "SubjectTotal", 1);
		$returnArr["GrandAverage"] = ($remainDenomenator == 0) ? 0 : (($columnNumerator/$remainDenomenator)*$averageBase);
		
		/*
		if ($CheckSubjectExclusion && $haveExcludeSubject)
		{
			if (in_array($classLevelName, $excludeClassLevelNameArr) && $StudentID==3725)
			{
				hdebug_r('columnTotalMark = '.$columnTotalMark);
				hdebug_r('GrandTotal = '.$returnArr["GrandTotal"]);
				hdebug_r('columnNumerator = '.$columnNumerator);
				hdebug_r('remainDenomenator = '.$remainDenomenator);
				hdebug_r('GrandAverage = '.$returnArr["GrandAverage"]);
			}
		}
		*/
		
		#debug_r("Grand Total of $StudentID = $columnTotalMark) = ".$returnArr["GrandTotal"]);
		#debug_r("Grand Average of $StudentID = ($columnNumerator/$remainDenomenator)*$averageBase = ".$returnArr["GrandAverage"]);
		
		return $returnArr;
	}
	#Ip25 only
	function CALCULATE_VERTICAL_SUBJECT_OVERALL($subjectColumnMarkList, $reportColumnWeight, $ReportID, $ClassLevelID, $absentExemptSettings, $reportBasicInfo, $totalReportColumnWeight, $debug=0) {
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName, $eRCTemplateSetting;
		
		//$SubjectGradingSchemeAssoArr[$SubjectID] = InfoArr
		$SubjectGradingSchemeAssoArr = array();
		
		$reportColumnData = $this->returnReportTemplateColumnData($ReportID);
		$lastReportColumnID = $reportColumnData[count($reportColumnData)-1]['ReportColumnID'];
		$lastReportColumnTermID = $reportColumnData[count($reportColumnData)-1]['SemesterNum'];
		
		$SemID = $reportBasicInfo['Semester'];
		$ReportType = ($SemID=='F')? 'W' : 'T';
		
		if ($reportBasicInfo['Semester'] == "F")
			$isWholeYearReport = true;
		else
			$isWholeYearReport = false;
			
		$existSpFullMarkArr = $this->GET_NEW_FULLMARK($ReportID, '', '', '');
		$existSpFullMarkAssoArr = BuildMultiKeyAssoc($existSpFullMarkArr, array('StudentID', 'SubjectID', 'ReportColumnID'));
		unset($existSpFullMarkArr);	
		
		$StudentIDArr = array_keys((array)$subjectColumnMarkList);
		$OverallMarksheetArr = array();
		foreach((array)$subjectColumnMarkList as $studentID => $markSubjectList) {
			foreach((array)$markSubjectList as $subjectID => $markColumnList) {
				if (!isset($OverallMarksheetArr[$subjectID])) {
					$OverallMarksheetArr[$subjectID] = $this->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $subjectID, $ReportID);
				}
			}
		}
		
		$subjectOverallValues = array();
		$success = array();
		
		foreach((array)$subjectColumnMarkList as $studentID => $markSubjectList) {
			foreach((array)$markSubjectList as $subjectID => $markColumnList) {
				
				$subjectFullMark = $this->GET_SUBJECT_FULL_MARK($subjectID, $ClassLevelID,$ReportID);
				$subjectOverallMark = "";
				$subjectOverallMarkConverted = '';
				$subjectOverallMarkRaw = '';
				$excludeColumnWeight = 0;
				$excludeFullMark = 0;
				$specialCaseDifferentiate = array();
				
				if (!isset($SubjectGradingSchemeAssoArr[$subjectID])) {
					$SubjectGradingSchemeAssoArr[$subjectID] = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $subjectID, 0, 0, $ReportID);
				}
				$subjectSchemeInfo = $SubjectGradingSchemeAssoArr[$subjectID];
				
				$schemeID = $subjectSchemeInfo["SchemeID"];
				$scaleInput = $subjectSchemeInfo["ScaleInput"];
				$scaleDisplay = $subjectSchemeInfo["ScaleDisplay"];
				
				if ($eRCTemplateSetting['ConsolidatedReportUseGPAToCalculate'] && $ReportType=='W') {
					$scaleInput = 'M';
				}
					
				$schemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($schemeID);
				$schemeRangeIDGradeMap = array();
				for($m=0; $m<sizeof($schemeRangeInfo); $m++) 
					$schemeRangeIDGradeMap[$schemeRangeInfo[$m]["GradingSchemeRangeID"]] = $schemeRangeInfo[$m]["Grade"];
					
				
				// add back score for ���Z�� not taking exam
				if ( !($this->Get_Num_Of_Column($ReportID) == 4) || !($eRCTemplateSetting['UseConsolidatedReportOverallScoreToCalculateSubjectOverall']) )
				{
					for ($i=0; $i<count($reportColumnData); $i++)
					{
						$columnID = $reportColumnData[$i]["ReportColumnID"];
						
						if (!isset($markColumnList[$columnID]))
						{
							$markColumnList[$columnID] = 'N.A.';
						}
					}
				}
				
				
				foreach((array)$markColumnList as $columnID => $mark) {
					
					if (trim($mark)==='') {
						$mark = 'N.A.';
					}
					
					if ($eRCTemplateSetting['LastColumnTermNotInSubjectGroupEqualsDropped'] && $ReportType=='W' && $columnID==$lastReportColumnID) {
						if (!$this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $studentID, $subjectID, $lastReportColumnTermID)) {
							$subjectOverallMark = 'N.A.';
							$subjectOverallMarkConverted = 'N.A.';
							break;
						}
					}
					
					if (is_numeric($mark) and $scaleInput=="M") {
						// if any of the assessment marks is numeric, make sure $subjectOverallMark is initiated to zero
						$subjectOverallMark = ($subjectOverallMark == "") ? 0 : $subjectOverallMark;
						//$subjectOverallMark += $mark*$reportColumnWeight[$columnID];
						
						// rounded weighted mark before add to overall (2011-0126-1606-48073)
						$thisWeightedMark = $mark * $reportColumnWeight[$columnID];
						
						if ($eRCTemplateSetting['RoundSubjectMarkBeforeAddToOverallMark'])
							$thisWeightedMark = $this->ROUND_MARK($thisWeightedMark, "SubjectScore"); 
						$subjectOverallMark += $thisWeightedMark;
						
						//$existSpFullMark = $this->GET_NEW_FULLMARK($ReportID, $studentID, $subjectID, $columnID);
						$existSpFullMark = $existSpFullMarkAssoArr[$studentID][$subjectID][$columnID];
						if (count((array)$existSpFullMark) > 0) {
							$excludeFullMark += ($subjectFullMark-$existSpFullMark["FullMark"])*$reportColumnWeight[$columnID];
						}
					} else {
						if($scaleInput=="M")
						{
							// Build the $specialCaseDifferentiate Array for determining the subject overall mark below
							$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $mark, $absentExemptSettings);
							
							// Only concern numeric mark calculation here
							if ($specialCaseDifferentiate[$mark]["isExFullMark"] == 1) {
								$excludeFullMark += $subjectFullMark * $reportColumnWeight[$columnID];
							}
							//2012-0706-1451-01140
							//if ($specialCaseDifferentiate[$mark]["isCount"] == 0) {
							else if ($specialCaseDifferentiate[$mark]["isCount"] == 0) {
								$excludeColumnWeight += $reportColumnWeight[$columnID];
							}
							
							
						}
						else	#$scaleDisplay=="G"
						{
							# ?
						}
					}
				}
				
				
				### If one of the column is N.A. or other special case, the overall column will be the special case also
				if ($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']==true && count($specialCaseDifferentiate) > 0)
				{
					$tmpKeyArr = array_keys($specialCaseDifferentiate);
					$numOfKey = count($tmpKeyArr);
					
					for ($i=0; $i<$numOfKey; $i++)
					{
						$thisSC = $tmpKeyArr[$i];
						
						// check if copy specific special case only
						if ($this->Check_Copy_Special_Case($thisSC, $ReportType, $studentID))
						{
							$subjectOverallMark = $thisSC;
							$subjectOverallMarkConverted = $thisSC;
							break;
						}
					}
				}
				
				// in case of excluding full mark
				if ($excludeFullMark > 0) {
					//$newFullMark = $subjectFullMark - $excludeFullMark;
					//$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK($newFullMark, $ReportID, $studentID, $subjectID);
					
					$newFullMark = $subjectFullMark - $excludeFullMark;
					//$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK($newFullMark, $ReportID, $markStudentID, $subjectID);
					
					// exclude full mark
					if ($newFullMark > 0 && is_numeric($subjectOverallMark))
					{
						$subjectOverallMark = $subjectOverallMark * ($subjectFullMark / $newFullMark);
					}
				}
				
				// 2012-0507-1530-27066 point 2 - added the if clause
				if (isset($OverallMarksheetArr[$subjectID][$studentID]['MarkNonNum'])) {
					// 2012-0531-1034-48073
					//$subjectOverallMark = $OverallMarksheetArr[$subjectID][$studentID]['MarkNonNum'];
					//$subjectOverallMarkConverted = $OverallMarksheetArr[$subjectID][$studentID]['MarkNonNum'];
					
					$thisMarkNonNum = $OverallMarksheetArr[$subjectID][$studentID]['MarkNonNum'];
					$thisMarkType = $OverallMarksheetArr[$subjectID][$studentID]['MarkType'];
					
					if ($thisMarkType == 'G') {
						if ($schemeRangeIDGradeMap[$thisMarkNonNum] != '') {
							# map mark with the corresponding grade
							$subjectOverallMark = $schemeRangeIDGradeMap[$thisMarkNonNum];
							$subjectOverallMarkConverted = $schemeRangeIDGradeMap[$thisMarkNonNum];
						}
						else {
							# Can't map grade => Use the input mark
							$subjectOverallMark = $thisMarkNonNum;
							$subjectOverallMarkConverted = $thisMarkNonNum;
						}
					}
					else {
						$subjectOverallMark = $thisMarkNonNum;
						$subjectOverallMarkConverted = $thisMarkNonNum;
					}
				}
				
				$subjectOverallMark = trim($subjectOverallMark);
				
				// subject overall mark can be calculated as number
				if ($scaleInput=='M' && is_numeric($subjectOverallMark)) {
					$subjectOverallMarkConverted = $subjectOverallMark;
					$subjectOverallMarkRaw = $subjectOverallMark;
					
					if ($scaleInput == "M" && $scaleDisplay == "G") {
						
						if ($totalReportColumnWeight == $excludeColumnWeight) {
							$subjectOverallMarkConverted = "/";
							$subjectOverallMarkRaw = 0;
						}
						else {
							if ($excludeColumnWeight > 0) {
								$activeWeightRatio = $totalReportColumnWeight / ($totalReportColumnWeight - $excludeColumnWeight);
								$subjectOverallMarkTmp = $subjectOverallMark;
								$subjectOverallMark = $subjectOverallMarkTmp * $activeWeightRatio;
								$subjectOverallMarkRaw = $subjectOverallMarkTmp * $activeWeightRatio;
							}
							
							if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade']) {
								$subjectOverallMarkToConvert = $this->ROUND_MARK($subjectOverallMark, "SubjectTotal");
							}
							else {
								$subjectOverallMarkToConvert = $subjectOverallMark;
							}
							$subjectOverallMarkConverted = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectSchemeInfo["SchemeID"], $subjectOverallMarkToConvert);
							//$subjectOverallMarkConverted = $this->ROUND_MARK($subjectOverallMarkConverted, "SubjectTotal");
						}
					} else {
						if ($totalReportColumnWeight == $excludeColumnWeight) {
							$subjectOverallMarkConverted = "/";
							$subjectOverallMarkRaw = 0;
						} else {
							# updated on 12 Jan 2009 by Ivan
							# divide the overall marks by the total column weight
							if ($excludeFullMark > 0)
								$subjectOverallMarkConverted = $subjectOverallMark;
							else
								$subjectOverallMarkConverted = $subjectOverallMark / ($totalReportColumnWeight - $excludeColumnWeight);
							
							if ($isWholeYearReport && $eRCTemplateSetting['CustomizedConsolidatedSubjectTotalRounding'])
								$subjectOverallMarkConverted = my_round($subjectOverallMarkConverted, $eRCTemplateSetting['ConsolidatedSubjectTotalRounding']);
							else
								$subjectOverallMarkConverted = $this->ROUND_MARK($subjectOverallMarkConverted, "SubjectTotal");
						}
					}
				} else { // subject cannot be calculated as number  # or is input mark display grade (20080926)
					// There's only one kind of special case in all column
					$subjectOverallMarkRaw = 0;
					
					if ($subjectOverallMarkConverted == '')
					{
						# retrieve the data (column id =0)
						$ovarllTemp = $this->GET_MARKSHEET_OVERALL_SCORE(array($studentID), $subjectID, $ReportID);
						
						if ($ovarllTemp[$studentID]['MarkType'] == 'M' && $ovarllTemp[$studentID]['MarkNonNum'] != '') {
							$subjectOverallMarkRaw = $ovarllTemp[$studentID]['MarkNonNum'];
							$subjectOverallMark = $ovarllTemp[$studentID]['MarkNonNum'];
							$subjectOverallMarkConverted = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($schemeID, $subjectOverallMark, $ReportID, $studentID, $subjectID, $ClassLevelID, 0, "");
						}
						else if (sizeof($specialCaseDifferentiate) == 1) {
							$onlySpecialCase = array_keys($specialCaseDifferentiate);
							$subjectOverallMarkConverted = $onlySpecialCase[0];
							
						} else {	// How to handle different special cases mixed up together?
							// Temporary assign it to Zero 
							$subjectOverallMark = "0";
							
							if ($schemeRangeIDGradeMap[$ovarllTemp[$studentID]['MarkNonNum']] != '')
							{
								# map mark with the corresponding grade
								$subjectOverallMarkConverted = $schemeRangeIDGradeMap[$ovarllTemp[$studentID]['MarkNonNum']];
								
							}
							else
							{
								# Can't map grade => Use the input mark
								$subjectOverallMarkConverted = $ovarllTemp[$studentID]['MarkNonNum'];
							}
						}
					}
				}
				
				if ($scaleDisplay == "M" && is_numeric($subjectOverallMark) && is_numeric($subjectOverallMarkConverted))
				{
					$subjectOverallValues[] = "('$studentID', '$subjectID', '$ReportID', '', '$subjectOverallMarkConverted', '', '$subjectOverallMarkRaw', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
				}
				else
				{
					$subjectOverallValues[] = "('$studentID', '$subjectID', '$ReportID', '', '$subjectOverallMarkRaw', '$subjectOverallMarkConverted', '$subjectOverallMarkRaw', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
				}
			}
		}
		
		// consolidate the resulting subject overall mark
		if (sizeof($subjectOverallValues) > 0) {
			$subjectOverallValues = implode(",", $subjectOverallValues);
			$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($subjectOverallValues);
		}
		
		return !in_array(false, $success);
	}
	
	
	/* 20100312 Ivan - Performance Tunning
	 * Use the new UPDATE_ORDER() function now to change all update statements to one insert statement
	// Helper function for UPDATE_ORDER() to avoid repeat writing similiar coding blocks
	function UPDATE_ORDER_METHOD($studentScore, $orderField, $table, $cond, $idMap, $excludeList=array(), $scaleInput='') {
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName;
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		
		$orderCount = 0;
		$numberCount = 0;
		$lastMark = -1;
		$success = array();
		$noOfStudent = sizeof($studentScore);
		
		// student in the excluded list will not be counted in the number of students in class / form
		if ($eRCTemplateSetting['DoNotCountExcludedStudentInStudentNumOfClassAndForm'] && count($excludeList) > 0)
		{
			foreach((array)$studentScore as $studentID => $mark) {
				if (in_array($studentID, $excludeList)) {
					$noOfStudent--;
				}
			}
		}
		
		if ($orderField == "OrderMeritClass")
			$noOfStudentField = "ClassNoOfStudent";
		else if ($orderField == "OrderMeritForm")
			$noOfStudentField = "FormNoOfStudent";
		else if ($orderField == "OrderMeritStream")
			$noOfStudentField = "StreamNoOfStudent";
		else if ($orderField == "OrderMeritSubjectGroup")
			$noOfStudentField = "SubjectGroupNoOfStudent";
			
		// Marks in $studentScore already sorted by highest mark
		foreach((array)$studentScore as $studentID => $mark) {
			// exclude student in the list or the subject is grade input, they will not be count in the ranking
			if (in_array($studentID, $excludeList) || $scaleInput=='M') {
				$sql = "UPDATE $table SET $orderField = '-1', $noOfStudentField = '$noOfStudent' WHERE $cond = ".$idMap[$studentID];
				$success[] = $this->db_db_query($sql);
				continue;
			}
			$numberCount++;
			
			if ($eRCTemplateSetting['RankingMethod'] == '1223')
			{
				# 1,2,2,3
				if ($mark != $lastMark)
					$orderCount++;
			}
			else
			{
				# 1,2,2,4
				if ($mark == $lastMark)
				{
					$orderCount = $lastOrderCount;
				}
				else
				{
					$orderCount = $numberCount;
				}
				$lastOrderCount = $orderCount;
			}
			
			$sql = "UPDATE $table SET $orderField = '$orderCount', $noOfStudentField = '$noOfStudent' WHERE $cond = ".$idMap[$studentID];
			$success[] = $this->db_db_query($sql);
			
			$lastMark = $mark;
		}

		return !in_array(false, $success);
	}
	
	// Update OrderMeritClass & OrderMeritForm field in the tables RC_REPORT_RESULT_SCORE & RC_REPORT_RESULT
	// Run once after consolidation of score finished in GENERATE_TERM_REPORTCARD() & GENERATE_WHOLE_YEAR_REPORTCARD()
	function UPDATE_ORDER($ReportID, $ClassLevelID) {
		global $PATH_WRT_ROOT, $eRCTemplateSetting;
		//include($PATH_WRT_ROOT."includes/eRCConfig.php");
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		####### Added on 20080708 #######
		// Get the list of students who will not be count when calculating order
		$excludeStudentsList = $this->GET_EXCLUDE_ORDER_STUDENTS();
		if (!is_array($excludeStudentsList))
			$excludeStudentsList = array();
		
		// Get Report Columns (Assessment)
		$reportColumnData = $this->returnReportTemplateColumnData($ReportID);
		if (sizeof($reportColumnData) > 0) {
			//$totalReportColumnWeight = 0;
			for($i=0; $i<sizeof($reportColumnData); $i++) {
				$reportColumnIDList[] = $reportColumnData[$i]["ReportColumnID"];
				$reportInvolveTerms[] = $reportColumnData[$i]["SemesterNum"];
				$reportColumnTermMap[$reportColumnData[$i]["ReportColumnID"]] = $reportColumnData[$i]["SemesterNum"];
				$reportTermColumnMap[$reportColumnData[$i]["SemesterNum"]] = $reportColumnData[$i]["ReportColumnID"];
			}
		}
		
		// Get Classes
		$classArr = $this->GET_CLASSES_BY_FORM($ClassLevelID);
		
		for($i=0; $i<sizeof($classArr); $i++) {
			// $studentArr[#ClassID][$i] + ["UserID"], ["WebSAMSRegNo"],...
			$studentArr[$classArr[$i]["ClassID"]] = $this->GET_STUDENT_BY_CLASS($classArr[$i]["ClassID"]);
		}
		// Get Subjects
		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// Init the huge array for storing mark of entire class level
		$orderClassLevelList = array();
		
		// Init the huge array for storing mark of entire stream (ClassGroup)
		$orderStreamList = array();
		
		// Init the huge array for storing mark of entire subject group
		$orderSubjectGroupList = array();
		
		// Mapping between $StudentID & $ReportResultScoreID
		$studentIDScoreIDMap = array();
		
		// Get Form Subject Average and SD
		if($eRCTemplateSetting['OrderPositionMethod'] == 'SD') 
			list($SDAry,$AvgAry) = $this->getFormSubjectSDAndAverage($ReportID);
			
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		
		// Loop 1: Classes
		// Sort class order of individual subject
		for($i=0; $i<sizeof($classArr); $i++) {
			$classID = $classArr[$i]["ClassID"];
			$StreamNo = $classArr[$i]["ClassGroupID"];
			
			$studentIDArr = array();
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			$studentIDArrList = implode(",", $studentIDArr);
			// Loop 2: Subjects
			foreach((array)$subjectList as $subjectID => $subjectInfo) {
				$scaleInput = $subjectInfo["ScaleInput"];
				$scaleDisplay = $subjectInfo["ScaleDisplay"];
				//if ($scaleInput != "M") continue;
				// Loop 3: Report Columns
				for($n=0; $n<sizeof($reportColumnData); $n++) {
					$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
					if (!isset($orderClassLevelList[$subjectID][$reportColumnID]))
						$orderClassLevelList[$subjectID][$reportColumnID] = array();
					if ($StreamNo && !isset($orderStreamList[$StreamNo][$subjectID][$reportColumnID]))
						$orderStreamList[$StreamNo][$subjectID][$reportColumnID] = array();
					$sql = "SELECT ReportResultScoreID, StudentID, RawMark, SDScore FROM $table ";
					$sql .= " WHERE ReportID = '$ReportID' AND ReportColumnID = '$reportColumnID' ";
					$sql .= " AND SubjectID = '$subjectID' AND StudentID IN ($studentIDArrList) ";
					$sql .= " AND Grade != 'N.A.' ";
					$result = $this->returnArray($sql, 3);
					
					// Skip if no result
					if (sizeof($result) <= 0) continue;
					$studentScore = array();
					$studentSDScore = array();
					for($j=0; $j<sizeof($result); $j++) {
						$studentIDScoreIDMap[$subjectID][$reportColumnID][$result[$j]["StudentID"]] = $result[$j]["ReportResultScoreID"];
						$studentScore[$result[$j]["StudentID"]] = $result[$j]["RawMark"];
						$studentSDScore[$result[$j]["StudentID"]] = $result[$j]["SDScore"];
					}
					if($eRCTemplateSetting['OrderPositionMethod'] == 'SD')
					{
						$SD = $SDAry[$subjectID];
						$Avg = $AvgAry[$subjectID];
						//list($SD,$Avg) = $this->getFormSubjectSDAndAverage($ReportID,$subjectID);
						foreach((array) $studentScore as $key=>$thisScore)
						{
							$studentScore[$key] = $SD==0?0:($thisScore-$Avg)/$SD;
						}
					}
					else if($eRCTemplateSetting['OrderPositionMethod'] == 'WeightedSD')
					{
						$studentScore = $studentSDScore;
					}
					arsort($studentScore);
					
					### ClassLevel ranking array
					$orderClassLevelList[$subjectID][$reportColumnID] = $orderClassLevelList[$subjectID][$reportColumnID] + $studentScore;
					
					### Class Group ranking array
					if(!empty($StreamNo))
						$orderStreamList[$StreamNo][$subjectID][$reportColumnID] = $orderStreamList[$StreamNo][$subjectID][$reportColumnID] + $studentScore;
						
					### Subject Group ranking array
					if ($ReportType == 'T')
					{
						foreach ((array)$studentScore as $thisStudentID => $thisRawMark)
						{
							$thisSubjectGroupID = $this->Get_Student_Studying_Subject_Group($SemID, $thisStudentID, $subjectID);
							
							if ($thisSubjectGroupID != '')
							{
								$orderSubjectGroupList[$thisSubjectGroupID][$subjectID][$reportColumnID][$thisStudentID] = $thisRawMark;
							}
						}
					}
					
					$this->UPDATE_ORDER_METHOD($studentScore, "OrderMeritClass", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
				} // Loop 3 End : Report Columns
			} // Loop 2 End: Subjects
		} // Loop 1 End: Classes
		
		
		// Sort class level order of individual subject
		foreach((array)$orderClassLevelList as $subjectID => $reportColumnMarkList) {
			$scaleInput = $subjectList[$subjectID]['ScaleInput'];
			foreach((array)$reportColumnMarkList as $reportColumnID => $studentMarkList) {
				// Skip if no result
				if (sizeof($studentMarkList) <= 0) continue;
				arsort($studentMarkList);
				
				$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritForm", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
			}
		}
		
		// Sort stream order of individual subject
		if (count($orderStreamList) > 0)
		{
			foreach((array)$orderStreamList as $StreamNo => $orderClassLevelList){
				foreach((array)$orderClassLevelList as $subjectID => $reportColumnMarkList) {
					$scaleInput = $subjectList[$subjectID]['ScaleInput'];
					foreach((array)$reportColumnMarkList as $reportColumnID => $studentMarkList) {
						// Skip if no result
						if (sizeof($studentMarkList) <= 0) continue;
						arsort($studentMarkList);
						$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritStream", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
					}
				}
			}
		}
		
		// Sort subject group order of individual subject
		if ($ReportType == 'T')
		{
			foreach((array)$orderSubjectGroupList as $subjectGroupID => $orderClassLevelList){
				foreach((array)$orderClassLevelList as $subjectID => $reportColumnMarkList) {
					$scaleInput = $subjectList[$subjectID]['ScaleInput'];
					foreach((array)$reportColumnMarkList as $reportColumnID => $studentMarkList) {
						// Skip if no result
						if (sizeof($studentMarkList) <= 0) continue;
						arsort($studentMarkList);
						$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritSubjectGroup", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
					}
				}
			}
		}
		
		
		
		$orderClassLevelList = array();
		$orderStreamList = array();
		$orderSubjectGroupList = array();
		
		// Loop 1: Classes
		// Sort class order of individual subject overall mark
		for($i=0; $i<sizeof($classArr); $i++) {
			$classID = $classArr[$i]["ClassID"];
			$StreamNo = $classArr[$i]["ClassGroupID"];
			$studentIDArr = array();
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			$studentIDArrList = implode(",", $studentIDArr);
			// Loop 2: Subjects
			foreach((array)$subjectList as $subjectID => $subjectInfo) {
				$scaleInput = $subjectInfo["ScaleInput"];
				$scaleDisplay = $subjectInfo["ScaleDisplay"];
				//if ($scaleInput != "M") continue;
				$reportColumnID = 0;
				if (!isset($orderClassLevelList[$subjectID][$reportColumnID]))
					$orderClassLevelList[$subjectID][$reportColumnID] = array();
				if ($StreamNo && !isset($orderStreamList[$StreamNo][$subjectID][$reportColumnID]))
					$orderStreamList[$StreamNo][$subjectID][$reportColumnID] = array();
				
				$sql = "SELECT ReportResultScoreID, StudentID, RawMark, SDScore FROM $table ";
				$sql .= "WHERE ReportID = '$ReportID' AND ReportColumnID = '$reportColumnID' ";
				$sql .= "AND SubjectID = '$subjectID' AND StudentID IN ($studentIDArrList)";
				$sql .= " AND Grade != 'N.A.' ";
				$result = $this->returnArray($sql, 3);
				
				// Skip if no result
				if (sizeof($result) <= 0) continue;
				$studentScore = array();
//				for($j=0; $j<sizeof($result); $j++) {
//					$studentIDScoreIDMap[$subjectID][$reportColumnID][$result[$j]["StudentID"]] = $result[$j]["ReportResultScoreID"];
//					$studentScore[$result[$j]["StudentID"]] = $result[$j]["RawMark"];
//				}
//
//				if($eRCTemplateSetting['OrderPositionMethod'] == 'SD')
//				{
//					list($SD,$Avg) = $this->getFormSubjectSDAndAverage($ReportID,$subjectID);
//					foreach((array) $studentScore as $key=>$thisScore)
//					{
//						$studentScore[$key] = $SD==0?0:($thisScore-$Avg)/$SD;
//					}
//				}

				$studentScore = array();
				$studentSDScore = array();
				for($j=0; $j<sizeof($result); $j++) {
					$studentIDScoreIDMap[$subjectID][$reportColumnID][$result[$j]["StudentID"]] = $result[$j]["ReportResultScoreID"];
					$studentScore[$result[$j]["StudentID"]] = $result[$j]["RawMark"];
					$studentSDScore[$result[$j]["StudentID"]] = $result[$j]["SDScore"];
				}
				
				if($eRCTemplateSetting['OrderPositionMethod'] == 'SD')
				{
					$SD = $SDAry[$subjectID];
					$Avg = $AvgAry[$subjectID];
					//list($SD,$Avg) = $this->getFormSubjectSDAndAverage($ReportID,$subjectID);
					foreach((array) $studentScore as $key=>$thisScore)
					{
						$studentScore[$key] = $SD==0?0:($thisScore-$Avg)/$SD;
					}
				}
				else if($eRCTemplateSetting['OrderPositionMethod'] == 'WeightedSD')
				{
					$studentScore = $studentSDScore;
				}
				arsort($studentScore);
				### Class Level ranking array
				$orderClassLevelList[$subjectID][$reportColumnID] = $orderClassLevelList[$subjectID][$reportColumnID] + $studentScore;
				
				### Class Group ranking array
				if(!empty($StreamNo))
					$orderStreamList[$StreamNo][$subjectID][$reportColumnID] = $orderStreamList[$StreamNo][$subjectID][$reportColumnID] + $studentScore;
					
				### Subject Group ranking array
				if ($ReportType == 'T')
				{
					foreach ((array)$studentScore as $thisStudentID => $thisRawMark)
					{
						$thisSubjectGroupID = $this->Get_Student_Studying_Subject_Group($SemID, $thisStudentID, $subjectID);
						
						if ($thisSubjectGroupID != '')
						{
							$orderSubjectGroupList[$thisSubjectGroupID][$subjectID][$reportColumnID][$thisStudentID] = $thisRawMark;
						}
					}
				}
				
				$this->UPDATE_ORDER_METHOD($studentScore, "OrderMeritClass", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
			} // Loop 2 End: Subjects
		} // Loop 1 End: Classes
		
		// Sort class level order of individual subject overall mark
		foreach((array)$orderClassLevelList as $subjectID => $reportColumnMarkList) {
			$reportColumnID = 0;
			$studentMarkList = $reportColumnMarkList[0];
			$scaleInput = $subjectList[$subjectID]['ScaleInput'];

			// Skip if no result
			if (sizeof($studentMarkList) <= 0) continue;
			arsort($studentMarkList);
			$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritForm", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
		}
		
		if (count($orderStreamList) > 0)
		{
			foreach((array)$orderStreamList as $StreamNo => $orderClassLevelList){
					foreach((array)$orderClassLevelList as $subjectID => $reportColumnMarkList) {
					$reportColumnID = 0;
					$studentMarkList = $reportColumnMarkList[0];
					$scaleInput = $subjectList[$subjectID]['ScaleInput'];
		
					// Skip if no result
					if (sizeof($studentMarkList) <= 0) continue;
					arsort($studentMarkList);
					$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritStream", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
				}
			}
		}
		
		if ($ReportType == 'T')
		{
			foreach((array)$orderSubjectGroupList as $subjectGroupID => $orderClassLevelList){
					foreach((array)$orderClassLevelList as $subjectID => $reportColumnMarkList) {
					$reportColumnID = 0;
					$studentMarkList = $reportColumnMarkList[0];
					$scaleInput = $subjectList[$subjectID]['ScaleInput'];
		
					// Skip if no result
					if (sizeof($studentMarkList) <= 0) continue;
					arsort($studentMarkList);
					$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritSubjectGroup", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
				}
			}
		}
		
		
		
		// Switch table
		$table = $this->DBName.".RC_REPORT_RESULT";
		
		// Mapping between $StudentID & $ResultID
		$studentIDResultIDMap = array();
		
		// Init the huge array for storing mark of entire class level
		$orderClassLevelList = array();
		
		// Init the huge array for storing mark of entire stream (ClassGroup)
		$orderStreamList = array();
		
		// Set field to determine position
		$orderField = ($eRCTemplateSetting['OrderingPositionBy']=="")? "GrandAverage" : $eRCTemplateSetting['OrderingPositionBy'];
		
		// Sort class order of term/assessment total
		for($i=0; $i<sizeof($classArr); $i++) {
			$classID = $classArr[$i]["ClassID"];
			$StreamNo = $classArr[$i]["ClassGroupID"];
			$studentIDArr = array();
			
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			$studentIDArrList = implode(",", $studentIDArr);
			
			for($n=0; $n<sizeof($reportColumnData); $n++) {
				$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
				// Term/assessment total : have ReportColumnID
				$sql = "SELECT ResultID, StudentID, GrandTotal, GrandAverage, GPA, GrandSDScore FROM $table ";
				$sql .= "WHERE ReportID = '$ReportID' AND StudentID IN ($studentIDArrList) ";
				$sql .= "AND ReportColumnID = '$reportColumnID'";
				$result = $this->returnArray($sql, 5);
				
				// Skip if no result
				if (sizeof($result) <= 0) continue;
				$studentScore = array();
				$studentSDScore = array();
				for($k=0; $k<sizeof($result); $k++) { 
					$studentIDResultIDMap[$reportColumnID][$result[$k]["StudentID"]] = $result[$k]["ResultID"];
					$studentScore[$result[$k]["StudentID"]] = $result[$k][$orderField];
				}
				
				if($eRCTemplateSetting['OrderPositionMethod'] == 'SD')
				{
					$SD = $SDAry[$orderField];
					$Avg = $AvgAry[$orderField];
					//list($SD,$Avg) = $this->getFormSubjectSDAndAverage($ReportID,$subjectID);
					foreach((array) $studentScore as $key=>$thisScore)
					{
						$studentScore[$key] = $SD==0?0:($thisScore-$Avg)/$SD;
					}
				}
				arsort($studentScore);
				// Init array
				if (!isset($orderClassLevelList[$reportColumnID]))
					$orderClassLevelList[$reportColumnID] = array();
				if ($StreamNo && !isset($orderStreamList[$StreamNo][$reportColumnID]))
					$orderStreamList[$StreamNo][$reportColumnID] = array();
				
				### ClassLevel ranking array
				$orderClassLevelList[$reportColumnID] = $orderClassLevelList[$reportColumnID] + $studentScore;
				
				### Class Group ranking array
				if(!empty($StreamNo))
					$orderStreamList[$StreamNo][$reportColumnID] = $orderStreamList[$StreamNo][$reportColumnID] + $studentScore;
				
				$this->UPDATE_ORDER_METHOD($studentScore, "OrderMeritClass", $table, "ResultID", $studentIDResultIDMap[$reportColumnID], $excludeStudentsList);
				
			}
		}
		
		// Sort class level order of term/assessment total
		foreach((array)$orderClassLevelList as $reportColumnID => $studentMarkList) {
			// Skip if no result
			if (sizeof($studentMarkList) <= 0) continue;
			arsort($studentMarkList);
			$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritForm", $table, "ResultID", $studentIDResultIDMap[$reportColumnID], $excludeStudentsList);
		}
		
		// Sort stream order of term/assessment total
		if (count($orderStreamList) > 0)
		{
			foreach((array)$orderStreamList as $StreamNo => $orderClassLevelList) {
				foreach((array)$orderClassLevelList as $reportColumnID => $studentMarkList) {
					// Skip if no result
					if (sizeof($studentMarkList) <= 0) continue;
					arsort($studentMarkList);
					$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritStream", $table, "ResultID", $studentIDResultIDMap[$reportColumnID], $excludeStudentsList);
				}
			}
		}
		
		
		// Mapping between $StudentID & $ResultID
		$studentIDResultIDMap = array();
		
		// Init the huge array for storing mark of entire class level
		$orderClassLevelList = array();

		// Init the huge array for storing mark of entire stream (ClassGroup)
		$orderStreamList = array();
		
		// Sort class order of grand total
		for($i=0; $i<sizeof($classArr); $i++) {
			$classID = $classArr[$i]["ClassID"];
			$StreamNo = $classArr[$i]["ClassGroupID"];
			$studentIDArr = array();
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			$studentIDArrList = implode(",", $studentIDArr);
			
			$sql = "SELECT ResultID, StudentID, GrandTotal, GrandAverage, GPA, GrandSDScore FROM $table ";
			$sql .= "WHERE ReportID = '$ReportID' AND StudentID IN ($studentIDArrList) ";
			$sql .= "AND (ReportColumnID IS NULL OR ReportColumnID = '' OR ReportColumnID = '0')";
			$result = $this->returnArray($sql, 5);
			
			// Skip if no result
			if (sizeof($result) <= 0) continue;
			$studentScore = array();
			for($k=0; $k<sizeof($result); $k++) {
				$studentIDResultIDMap[$result[$k]["StudentID"]] = $result[$k]["ResultID"];
				$studentScore[$result[$k]["StudentID"]] = $result[$k][$orderField];
			}
			
			if($eRCTemplateSetting['OrderPositionMethod'] == 'SD')
			{
				$SD = $SDAry[$orderField];
				$Avg = $AvgAry[$orderField];
				//list($SD,$Avg) = $this->getFormSubjectSDAndAverage($ReportID,$subjectID);
				foreach((array) $studentScore as $key=>$thisScore)
				{
					$studentScore[$key] = $SD==0?0:($thisScore-$Avg)/$SD;
				}
			}
			arsort($studentScore);
			$orderClassLevelList = $orderClassLevelList + $studentScore;
			if ($StreamNo && !isset($orderStreamList[$StreamNo]))
				$orderStreamList[$StreamNo] = array();
			if(!empty($StreamNo))
				$orderStreamList[$StreamNo] = $orderStreamList[$StreamNo] + $studentScore;
				
			$this->UPDATE_ORDER_METHOD($studentScore, "OrderMeritClass", $table, "ResultID", $studentIDResultIDMap, $excludeStudentsList);
		}
		
		// Sort class level order of grand total
		if (sizeof($orderClassLevelList) > 0) {
			arsort($orderClassLevelList);
			$this->UPDATE_ORDER_METHOD($orderClassLevelList, "OrderMeritForm", $table, "ResultID", $studentIDResultIDMap, $excludeStudentsList);
		}
		
		if (count($orderStreamList) > 0)
		{
			foreach((array)$orderStreamList as $StreamNo => $orderClassLevelList) {
				if (sizeof($orderClassLevelList) > 0) {
					arsort($orderClassLevelList);		
					$this->UPDATE_ORDER_METHOD($orderClassLevelList, "OrderMeritStream", $table, "ResultID", $studentIDResultIDMap, $excludeStudentsList);
				}
			}
		}
	}
	*/
	# Update_Order , follow IP25 
	function UPDATE_ORDER($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$SuccessArr = array();
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$YearTermID = $ReportSetting['Semester'];
		$ReportType = ($YearTermID == 'F')? 'W' : 'T';
		
		### Get all Students Info
		$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ParClassID="", $ParStudentID="", $ReturnAsso=1);

		### Get all classes info
		$YearClassInfoArr = $this->GET_CLASSES_BY_FORM($ClassLevelID, "", $returnAssoName=0, $returnAssoInfo=1);
		
		### Get the list of students who will not be count when calculating order
//		$ExcludeStudentsArr = $this->GET_EXCLUDE_ORDER_STUDENTS();
//		if (!is_array($ExcludeStudentsArr))
//			$ExcludeStudentsArr = array();
		$ExcludeStudentsAssoArr = array();
		if ($ReportType == 'T') {
			$ExcludeStudentsAssoArr[0] = $this->GET_EXCLUDE_ORDER_STUDENTS($ReportID);
		}
		else {
			$termReportInfoAry = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
			$numOfTermReport = count($termReportInfoAry);
			for ($i=0; $i<$numOfTermReport; $i++) {
				$_termReportId = $termReportInfoAry[$i]['ReportID'];
				$_reportColumnId = $termReportInfoAry[$i]['ReportColumnID'];
				
				$ExcludeStudentsAssoArr[$_reportColumnId] = $this->GET_EXCLUDE_ORDER_STUDENTS($_termReportId);
			}
			$ExcludeStudentsAssoArr[0] = $this->GET_EXCLUDE_ORDER_STUDENTS($ReportID);
		}

		### Get Subjects Grading Scheme Info
		$SubjectGradingArr = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, $WithGrandMark=1, $ReportID);
		
		### Get all subject marks (from highest to lowest mark for ranking)
		$RankingField = 'RawMark';
		if ($eRCTemplateSetting['OrderPositionMethod'] == 'WeightedSD')
			$RankingField = 'SDScore';
			
		### Since the data in the table will be deleted and inserted again in a batch,
		### we should select all records here and do filtering in the later part of the code
		$subject_score_table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sql = "Select
						*
				From
						$subject_score_table
				Where
						ReportID = '".$ReportID."'
						
				Order By
						$RankingField Desc 
				";
		$SubjectScoreArr = $this->returnArray($sql, null, $ReturnAssoOnly=1);
		
		$SubjectGroupInfoArr = array();
		$SubjectScoreAssoArr = array();
		$SubjectFormMarkArr = array();
		$SubjectClassMarkArr = array();
		$SubjectSubjectGroupMarkArr = array();
		$SubjectStreamMarkArr = array();
		
		foreach ((array)$SubjectScoreArr as $key => $thisSubjectScoreArr)
		{
			
			$thisReportResultScoreID 	= $thisSubjectScoreArr['ReportResultScoreID'];
			$thisSubjectID 				= $thisSubjectScoreArr['SubjectID'];
			$thisReportColumnID 		= $thisSubjectScoreArr['ReportColumnID'];
			$thisStudentID 				= $thisSubjectScoreArr['StudentID'];
			
			# Add records to the associate array so that all marks can be inserted again after the ranking process
			$SubjectScoreAssoArr[$thisReportResultScoreID] = $thisSubjectScoreArr;
			
			# Get the ranking determine mark
			if ($eRCTemplateSetting['OrderPositionMethod'] == 'WeightedSD')
				$thisRankingMark = $thisSubjectScoreArr['SDScore'];
			else
				$thisRankingMark = $thisSubjectScoreArr['RawMark'];
				
			$thisGrade = $thisSubjectScoreArr['Grade'];
			
			# Get Student Class Info
			$thisYearClassID = $StudentInfoArr[$thisStudentID]['YearClassID'];
			$thisClassGroupID = $YearClassInfoArr[$thisYearClassID]['ClassGroupID'];
			
			
				
			# Build Form Ranking Array
			(array)$SubjectFormMarkArr[$thisSubjectID][$thisReportColumnID]['KeyIDArr'][] = $thisReportResultScoreID;
			(array)$SubjectFormMarkArr[$thisSubjectID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
			(array)$SubjectFormMarkArr[$thisSubjectID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
			(array)$SubjectFormMarkArr[$thisSubjectID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
			
			# Build Class Ranking Array
			(array)$SubjectClassMarkArr[$thisYearClassID][$thisSubjectID][$thisReportColumnID]['KeyIDArr'][] = $thisReportResultScoreID;
			(array)$SubjectClassMarkArr[$thisYearClassID][$thisSubjectID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
			(array)$SubjectClassMarkArr[$thisYearClassID][$thisSubjectID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
			(array)$SubjectClassMarkArr[$thisYearClassID][$thisSubjectID][$thisReportColumnID]['GradeArr'][] = $thisGrade;

			# Build Subject Group Ranking Array for Term Report
			if ($ReportType == 'T')
			{
				if ($SubjectGroupInfoArr[$thisStudentID][$thisSubjectID] == '')
					$SubjectGroupInfoArr[$thisStudentID][$thisSubjectID] = $this->Get_Student_Studying_Subject_Group($YearTermID, $thisStudentID, $thisSubjectID);
				
				$thisSubjectGroupID = $SubjectGroupInfoArr[$thisStudentID][$thisSubjectID];
				if ($SubjectGroupInfoArr[$thisStudentID][$thisSubjectID] != '')
				{
					(array)$SubjectSubjectGroupMarkArr[$thisSubjectGroupID][$thisSubjectID][$thisReportColumnID]['KeyIDArr'][] = $thisReportResultScoreID;
					(array)$SubjectSubjectGroupMarkArr[$thisSubjectGroupID][$thisSubjectID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
					(array)$SubjectSubjectGroupMarkArr[$thisSubjectGroupID][$thisSubjectID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
					(array)$SubjectSubjectGroupMarkArr[$thisSubjectGroupID][$thisSubjectID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
				}
			}

			# Build Stream Ranking Array
			if ($thisClassGroupID != '' && $thisClassGroupID != 0)
			{
				(array)$SubjectStreamMarkArr[$thisClassGroupID][$thisSubjectID][$thisReportColumnID]['KeyIDArr'][] = $thisReportResultScoreID;
				(array)$SubjectStreamMarkArr[$thisClassGroupID][$thisSubjectID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
				(array)$SubjectStreamMarkArr[$thisClassGroupID][$thisSubjectID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
				(array)$SubjectStreamMarkArr[$thisClassGroupID][$thisSubjectID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
			}
		}
		
		### Get Form Ranking Info
		foreach ((array)$SubjectFormMarkArr as $thisSubjectID => $thisSubjectFormMarkArr)
		{
			
			$thisScaleInput = $SubjectGradingArr[$thisSubjectID]['scaleInput'];
			foreach ((array)$thisSubjectFormMarkArr as $thisReportColumnID => $thisColumnSubjectFormMarkArr)
			{
				if ($ReportType == 'T') {
					$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
				}
				else {
					$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
				}
				$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisColumnSubjectFormMarkArr, $ExcludeStudentsArr, $thisScaleInput);
				$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisColumnSubjectFormMarkArr, $ExcludeStudentsArr);
				
				foreach((array)$thisRankInfoArr as $thisReportResultScoreID => $thisRanking)
				{
					$SubjectScoreAssoArr[$thisReportResultScoreID]['OrderMeritForm'] = $thisRanking;
					$SubjectScoreAssoArr[$thisReportResultScoreID]['FormNoOfStudent'] = $thisNumOfStudent;
				} 
			}
		}
		
		### Get Class Ranking Info
		foreach ((array)$SubjectClassMarkArr as $thisYearClassID => $thisClassMarkArr)
		{
			foreach ((array)$thisClassMarkArr as $thisSubjectID => $thisSubjectClassMarkArr)
			{
				$thisScaleInput = $SubjectGradingArr[$thisSubjectID]['scaleInput'];
				foreach ((array)$thisSubjectClassMarkArr as $thisReportColumnID => $thisColumnSubjectClassMarkArr)
				{
					if ($ReportType == 'T') {
						$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
					}
					else {
						$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
					}
					$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisColumnSubjectClassMarkArr, $ExcludeStudentsArr, $thisScaleInput);
					$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisColumnSubjectClassMarkArr, $ExcludeStudentsArr);
									
					foreach((array)$thisRankInfoArr as $thisReportResultScoreID => $thisRanking)
					{
						$SubjectScoreAssoArr[$thisReportResultScoreID]['OrderMeritClass'] = $thisRanking;
						$SubjectScoreAssoArr[$thisReportResultScoreID]['ClassNoOfStudent'] = $thisNumOfStudent;
					} 
				}
			}
		}
		
		### Get Subject Group Ranking Info
		foreach ((array)$SubjectSubjectGroupMarkArr as $thisSubjectGroupID => $thisSubjectGroupMarkArr)
		{
			foreach ((array)$thisSubjectGroupMarkArr as $thisSubjectID => $thisSubjectSubjectGroupMarkArr)
			{
				$thisScaleInput = $SubjectGradingArr[$thisSubjectID]['scaleInput'];
				foreach ((array)$thisSubjectSubjectGroupMarkArr as $thisReportColumnID => $thisColumnSubjectSubjectGroupMarkArr)
				{
					if ($ReportType == 'T') {
						$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
					}
					else {
						$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
					}
					$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisColumnSubjectSubjectGroupMarkArr, $ExcludeStudentsArr, $thisScaleInput);
					$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisColumnSubjectSubjectGroupMarkArr, $ExcludeStudentsArr);
									
					foreach((array)$thisRankInfoArr as $thisReportResultScoreID => $thisRanking)
					{
						$SubjectScoreAssoArr[$thisReportResultScoreID]['OrderMeritSubjectGroup'] = $thisRanking;
						$SubjectScoreAssoArr[$thisReportResultScoreID]['SubjectGroupNoOfStudent'] = $thisNumOfStudent;
					} 
				}
			}
		}
		
		### Get Subject Stream Ranking Info
		foreach ((array)$SubjectStreamMarkArr as $thisClassGroupID => $thisStreamMarkArr)
		{
			foreach ((array)$thisStreamMarkArr as $thisSubjectID => $thisSubjectStreamMarkArr)
			{
				$thisScaleInput = $SubjectGradingArr[$thisSubjectID]['scaleInput'];
				foreach ((array)$thisSubjectStreamMarkArr as $thisReportColumnID => $thisColumnSubjectStreamMarkArr)
				{
					if ($ReportType == 'T') {
						$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
					}
					else {
						$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
					}
					$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisColumnSubjectStreamMarkArr, $ExcludeStudentsArr, $thisScaleInput);
					$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisColumnSubjectStreamMarkArr, $ExcludeStudentsArr);
									
					foreach((array)$thisRankInfoArr as $thisReportResultScoreID => $thisRanking)
					{
						$SubjectScoreAssoArr[$thisReportResultScoreID]['OrderMeritStream'] = $thisRanking;
						$SubjectScoreAssoArr[$thisReportResultScoreID]['StreamNoOfStudent'] = $thisNumOfStudent;
					} 
				}
			}
		}
		
		
		### Delete old data and insert Subject Score with Ranking Info into the Database
		$sql = "Delete From $subject_score_table Where ReportID = '".$ReportID."'";
		$SuccessArr['SubjectMark']['DeleteOldScore'] = $this->db_db_query($sql);
		
		$insertValuesArr = array();
		$currentIndex = 0;
		$tmpInsertCounter = 0;
		$maxInsertNum = 1000;
		foreach((array)$SubjectScoreAssoArr as $thisReportResultScoreID => $thisScoreInfoArr)
		{
			$thisInsertValuesArr = array();
			foreach((array)$thisScoreInfoArr as $thisDBField => $thisValue)
			{
				if (is_numeric($thisDBField))
					continue;
				else if ($thisDBField == 'DateInput' || $thisDBField == 'DateModified')
					$thisInsertValuesArr[] = 'now()';
				else if ($thisValue == '')
					$thisInsertValuesArr[] = 'Null';
				else
					$thisInsertValuesArr[] = "'".$thisValue."'";
			}
			
			if (count($insertValuesArr[$currentIndex]) > $maxInsertNum)
			{
				$currentIndex++;
				$tmpInsertCounter = 0;
			}
			
			(array)$insertValuesArr[$currentIndex][] = "(".implode(",", $thisInsertValuesArr).")";
			$tmpInsertCounter++;
		}
		 
		$numOfInsertArr = count($insertValuesArr);
		if ($numOfInsertArr > 0)
		{
			for ($i=0; $i<$numOfInsertArr; $i++)
			{
				$thisInsertValueArr = $insertValuesArr[$i];
				$insertValuesStatement = implode(',', $thisInsertValueArr);
				$sql = "Insert Into 
							$subject_score_table
						Values
							$insertValuesStatement
						";
				$SuccessArr['SubjectMark']['InsertNewScore'][$i] = $this->db_db_query($sql) or die($sql.'<br />'.mysql_error());
			}
		}		
		
		
		
		### Get all Grand Marks (from highest to lowest mark for ranking)
		$RankingField = ($eRCTemplateSetting['OrderingPositionBy']=="")? "GrandAverage" : $eRCTemplateSetting['OrderingPositionBy'];
					
		$grand_score_table = $this->DBName.".RC_REPORT_RESULT";
		$sql = "Select
						*
				From
						$grand_score_table
				Where
						 ReportID = '".$ReportID."'
				Order By
						$RankingField Desc 
				";
		$GrandScoreArr = $this->returnResultSet($sql);
		
		$GrandScoreAssoArr = array();
		$GrandFormMarkArr = array();
		$GrandClassMarkArr = array();
		$GrandStreamMarkArr = array();
		foreach ((array)$GrandScoreArr as $key => $thisGrandScoreArr)
		{
			$thisResultID 			= $thisGrandScoreArr['ResultID'];
			$thisReportColumnID 	= $thisGrandScoreArr['ReportColumnID'];
			$thisStudentID 			= $thisGrandScoreArr['StudentID'];
			
			# Add records to the associate array so that all marks can be inserted again after the ranking process
			$GrandScoreAssoArr[$thisResultID] = $thisGrandScoreArr;
			
			# Get the ranking determine mark
			$thisRankingMark = $thisGrandScoreArr[$RankingField];
			$thisGrade = $thisGrandScoreArr['Grade'];
			
			# Get Student Class Info
			$thisYearClassID = $StudentInfoArr[$thisStudentID]['YearClassID'];
			$thisClassGroupID = $YearClassInfoArr[$thisYearClassID]['ClassGroupID'];
			
			
			# Build Form Ranking Array
			(array)$GrandFormMarkArr[$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
			(array)$GrandFormMarkArr[$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
			(array)$GrandFormMarkArr[$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
			(array)$GrandFormMarkArr[$thisReportColumnID]['GradeArr'][] = $thisGrade;
			
			# Build Class Ranking Array
			(array)$GrandClassMarkArr[$thisYearClassID][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
			(array)$GrandClassMarkArr[$thisYearClassID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
			(array)$GrandClassMarkArr[$thisYearClassID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
			(array)$GrandClassMarkArr[$thisYearClassID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
			
			# Build Stream Ranking Array
			if ($thisClassGroupID != '' && $thisClassGroupID != 0)
			{
				(array)$GrandStreamMarkArr[$thisClassGroupID][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
				(array)$GrandStreamMarkArr[$thisClassGroupID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
				(array)$GrandStreamMarkArr[$thisClassGroupID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
				(array)$GrandStreamMarkArr[$thisClassGroupID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
			}
		}
		
		### Get Form Ranking Info
		foreach ((array)$GrandFormMarkArr as $thisReportColumnID => $thisGrandFormMarkArr)
		{
			if ($ReportType == 'T') {
				$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
			}
			else {
				$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
			}
			$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisGrandFormMarkArr, $ExcludeStudentsArr);
			$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisGrandFormMarkArr, $ExcludeStudentsArr);
							
			foreach((array)$thisRankInfoArr as $thisResultID => $thisRanking)
			{
				$GrandScoreAssoArr[$thisResultID]['OrderMeritForm'] = $thisRanking;
				$GrandScoreAssoArr[$thisResultID]['FormNoOfStudent'] = $thisNumOfStudent;
			} 
		}
		
		### Get Class Ranking Info
		foreach ((array)$GrandClassMarkArr as $thisYearClassID => $thisClassMarkArr)
		{
			foreach ((array)$thisClassMarkArr as $thisReportColumnID => $thisColumnClassMarkArr)
			{
				if ($ReportType == 'T') {
					$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
				}
				else {
					$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
				}
				$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisColumnClassMarkArr, $ExcludeStudentsArr);
				$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisColumnClassMarkArr, $ExcludeStudentsArr);
				
				foreach((array)$thisRankInfoArr as $thisResultID => $thisRanking)
				{
					$GrandScoreAssoArr[$thisResultID]['OrderMeritClass'] = $thisRanking;
					$GrandScoreAssoArr[$thisResultID]['ClassNoOfStudent'] = $thisNumOfStudent;
				} 
			}
		}
		
		### Get Subject Stream Ranking Info
		foreach ((array)$GrandStreamMarkArr as $thisClassGroupID => $thisStreamMarkArr)
		{
			foreach ((array)$thisStreamMarkArr as $thisReportColumnID => $thisColumnStreamMarkArr)
			{
				if ($ReportType == 'T') {
					$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
				}
				else {
					$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
				}
				$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisColumnStreamMarkArr, $ExcludeStudentsArr);
				$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisColumnStreamMarkArr, $ExcludeStudentsArr);
				
				foreach((array)$thisRankInfoArr as $thisResultID => $thisRanking)
				{
					$GrandScoreAssoArr[$thisResultID]['OrderMeritStream'] = $thisRanking;
					$GrandScoreAssoArr[$thisResultID]['StreamNoOfStudent'] = $thisNumOfStudent;
				} 
			}
		}
		
		
		### Delete old data and insert Subject Score with Ranking Info into the Database
		$sql = "Delete From $grand_score_table Where ReportID = '".$ReportID."'";
		$SuccessArr['GrandMark']['DeleteOldScore'] = $this->db_db_query($sql);
		
		$insertValuesArr = array();
		foreach((array)$GrandScoreAssoArr as $thisResultID => $thisScoreInfoArr)
		{
			$thisInsertValuesArr = array();
			foreach((array)$thisScoreInfoArr as $thisDBField => $thisValue)
			{
				if (is_numeric($thisDBField))
					continue;
				else if ($thisDBField == 'DateInput' || $thisDBField == 'DateModified')
					$thisInsertValuesArr[] = 'now()';
				else if ($thisValue == '')
					$thisInsertValuesArr[] = 'Null';
				else
					$thisInsertValuesArr[] = "'".$thisValue."'";
			}
			$insertValuesArr[] = "(".implode(",", $thisInsertValuesArr).")";
		}
		 
		if (count($insertValuesArr) > 0)
		{
			$insertValuesStatement = implode(',', $insertValuesArr);
			$sql = "Insert Into 
						$grand_score_table
					Values
						$insertValuesStatement
					";
			$SuccessArr['GrandMark']['InsertNewScore'] = $this->db_db_query($sql);	
		}
		
		if (in_array(false, $SuccessArr))
			return false;
		else
			return true;
	}
	#IP25only
	function Get_Student_Ranking_Array($MarkInfoArr, $ExcludeStudentsArr=array(), $ScaleInput='')
	{
		global $eRCTemplateSetting;
		
		if ($ScaleInput == '')
			$ScaleInput = 'M';
		
		$KeyIDArr = $MarkInfoArr['KeyIDArr'];
		$StudentIDArr = $MarkInfoArr['StudentIDArr'];
		$MarkArr = $MarkInfoArr['MarkArr'];
		$GradeArr = $MarkInfoArr['GradeArr'];
		$numOfMark = count($MarkArr);
				
		$thisRank = 0;
		$lastMark = -1;
		$sameMarkCount = 0;
		$StudentRankArr = array();
		for ($i=0; $i<$numOfMark; $i++)
		{
			$thisKeyID = $KeyIDArr[$i];
			$thisStudentID = $StudentIDArr[$i];
			$thisMark = $MarkArr[$i];
			$thisGrade = $GradeArr[$i];
			
			// exclude student in the list or the subject is not mark input, they will not be count in the ranking
			if (in_array($thisStudentID, (array)$ExcludeStudentsArr) || $ScaleInput != 'M' || ($eRCTemplateSetting['OrderPositionMethod']!='WeightedSD' && $thisMark=='-1') || in_array($thisGrade, $this->Get_Exclude_Ranking_Special_Case())) {
				$StudentRankArr[$thisKeyID] = '-1';
				continue;
			}
			
			if ($eRCTemplateSetting['RankingMethod'] == '1223')
			{
				# 1,2,2,3
				if ($thisMark != $lastMark)
					$thisRank++;
			}
			else
			{
				# 1,2,2,4
				if ($thisMark != $lastMark)
				{
					$thisRank++;
					$thisRank += $sameMarkCount;
					$sameMarkCount = 0;
				}
				else
				{
					$sameMarkCount++;
				}
			}
			
			$lastMark = $thisMark;
			//$StudentRankArr[$thisKeyID] = $thisRank + $sameMarkCount;
			$StudentRankArr[$thisKeyID] = $thisRank;
		}
		
		return $StudentRankArr;
	}
	#Ip25Only
	function Get_Number_Of_Counted_Mark_Student($MarkInfoArr, $ExcludeStudentsArr=array()) 
	{
		global $eRCTemplateSetting;
		
		$MarkArr = $MarkInfoArr['MarkArr'];
		$GradeArr = $MarkInfoArr['GradeArr'];
		$StudentIDArr = $MarkInfoArr['StudentIDArr'];
		
		$numOfStudent = count($StudentIDArr);
		$numOfExclude = count($ExcludeStudentsArr);
		$numOfCountStudent = 0;
		
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisStudentID = $StudentIDArr[$i];
			$thisMark = $MarkArr[$i];
			$thisGrade = $GradeArr[$i];
			
			### If the student get a N.A. score, do not count the student
			if ($eRCTemplateSetting['OrderPositionMethod']!='WeightedSD' && $thisMark=='-1') {
				continue;
			}
			if ($eRCTemplateSetting['OrderPositionMethod']=='WeightedSD' && ($thisMark==null || $thisMark==='')) {
				continue;
			}
			//2011-1128-1135-55073 - 沙田循���衛���中學 - The Grade is not consistent in Master Report and Performance Repor
			if($eRCTemplateSetting['SpecialCaseThatExcludedFromNoOfStudent'] )
			{
				if(in_array($thisGrade,$eRCTemplateSetting['SpecialCaseThatExcludedFromNoOfStudent']))
					continue;
			}
			else
			{			
				if (in_array($thisGrade, $this->Get_Exclude_Ranking_Special_Case())) 
					continue;
			}
				
			### If the student is in the exclude list, do not count the student
			if ($eRCTemplateSetting['DoNotCountExcludedStudentInStudentNumOfClassAndForm'] && $numOfExclude > 0) {
				if (in_array($thisStudentID, (array)$ExcludeStudentsArr)) {
					continue;
				}
			}
			
			$numOfCountStudent++;
		}
		
		return $numOfCountStudent;
	}
	#IP25Only
	function COPY_EXIST_TERM_RESULT_SCORE($completedReportID, $completedSemester, $ReportID, $reportTermColumnMap, $Mode="T", $ClassLevelID='') {
		global $calculationSettings, $ReportCardCustomSchoolName, $eRCTemplateSetting;
		
		if (!isset($calculationSettings))
			$calculationSettings = $this->LOAD_SETTING("Calculation");
			
		// $SubjectGradingSchemeAssoArr[$SubjectID] = Scheme InfoArr
		$SubjectGradingSchemeAssoArr = array();
		
		// select existing result from TERM report
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sql  = "SELECT StudentID, SubjectID, Mark, Grade, RawMark, OrderMeritClass, OrderMeritForm, ReportColumnID ";
		//$sql .= "FROM $table WHERE ReportID = '$completedReportID' AND Semester = '$completedSemester' ";
		$sql .= "FROM $table WHERE ReportID = '$completedReportID' ";
		if ($Mode == "T") {
			//$sql .= "AND (ReportColumnID IS NULL OR ReportColumnID = 0)";
			if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment']) {
				$sql .= " AND ReportColumnID IN ('".implode("','", array_keys((array)$reportTermColumnMap[$completedSemester]))."') ";
			}
			else {
				$sql .= " AND Semester = '$completedSemester' AND (ReportColumnID IS NULL OR ReportColumnID = 0) ";
			}
		}
		else {
			$sql .= "AND ReportColumnID IS NOT NULL AND ReportColumnID != 0";
		}
		$existTermSubjectOverall = $this->returnArray($sql, 6);
		
		
		$existTermSubjectOverallArr = array();
		$existTermSubjectOverallValues = array();
		if (sizeof($existTermSubjectOverall) > 0) {
			for($i=0; $i<sizeof($existTermSubjectOverall); $i++) {
				$tmpStudentID = $existTermSubjectOverall[$i]["StudentID"];
				$tmpSubjectID = $existTermSubjectOverall[$i]["SubjectID"];
				$tmpMark = $existTermSubjectOverall[$i]["Mark"];
				$tmpGrade = $existTermSubjectOverall[$i]["Grade"];
				$tmpRawMark = $existTermSubjectOverall[$i]["RawMark"];
				$tmpOrderMeritClass = $existTermSubjectOverall[$i]["OrderMeritClass"];
				$tmpOrderMeritForm = $existTermSubjectOverall[$i]["OrderMeritForm"];
				//$tmpReportColumnID = $reportTermColumnMap[$completedSemester];
				$tmpTermReportColumnID = $existTermSubjectOverall[$i]["ReportColumnID"];
				
				if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment']) {
					$tmpReportColumnID = $reportTermColumnMap[$completedSemester][$tmpTermReportColumnID];
				}
				else {
					$tmpReportColumnID = $reportTermColumnMap[$completedSemester];
				}
				
				if ($tmpMark !== "" && $calculationSettings["UseWeightedMark"] == "1") {
					$tmpMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $tmpReportColumnID, $tmpSubjectID, $tmpMark);
				}
				
				if ($eRCTemplateSetting['ConsolidatedReportUseGPAToCalculate']) {
					if (!isset($SubjectGradingSchemeAssoArr[$tmpSubjectID])) {
						$SubjectGradingSchemeAssoArr[$tmpSubjectID] = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 0, 0, $ReportID);
					}
					
					$SubjectFormGradingSettings = $SubjectGradingSchemeAssoArr[$tmpSubjectID]; 
					$SchemeID = $SubjectFormGradingSettings['SchemeID'];
					$tmpMark = $this->CONVERT_GRADE_TO_GRADE_POINT_FROM_GRADING_SCHEME($SchemeID, $tmpGrade);
					$tmpRawMark = $tmpMark;
				}
				
				$existTermSubjectOverallArr[$tmpStudentID][$tmpSubjectID][] = 
					array (
						"ReportColumnID" => $tmpReportColumnID,
						"Mark" => $tmpMark,
						"Grade" => $tmpGrade,
						"RawMark" => $tmpRawMark,
						"OrderMeritClass" => $tmpOrderMeritClass,
						"OrderMeritForm" => $tmpOrderMeritForm
					);
				$existTermSubjectOverallValues[] = "('$tmpStudentID', '$tmpSubjectID', '$ReportID', '$tmpReportColumnID', '$tmpMark', '$tmpGrade', '$tmpRawMark', '0', '$completedSemester', '$tmpOrderMeritClass', '$tmpOrderMeritForm', NOW(), NOW())";
			}
		}
		
		if (sizeof($existTermSubjectOverallValues) > 0) {
			$existTermSubjectOverallValues = implode(",", $existTermSubjectOverallValues);
			$success = $this->INSERT_REPORT_RESULT_SCORE($existTermSubjectOverallValues);
		} else {
			$success = false;
		}
		if ($success) {
			return $existTermSubjectOverallArr;
		} else {
			return false;
		}
	}
	//USE PMS 
	function COPY_EXIST_TERM_RESULT($completedReportID, $completedSemester, $ReportID, $reportTermColumnMap) {
		$table = $this->DBName.".RC_REPORT_RESULT";
		$sql  = "SELECT StudentID, GrandTotal, GrandAverage, GPA, OrderMeritClass, OrderMeritForm ";
		$sql .= "FROM $table ";
		$sql .= "WHERE ReportID = '$completedReportID'";
		$existTermAverageTotal = $this->returnArray($sql, 6);
		
		$existTermAverageTotalArr = array();
		$existTermAverageTotalValues = array();
		for($i=0; $i<sizeof($existTermAverageTotal); $i++) {
			$tmpStudentID = $existTermAverageTotal[$i]["StudentID"];
			$tmpGrandTotal = $existTermAverageTotal[$i]["GrandTotal"];
			$tmpGrandAverage = $existTermAverageTotal[$i]["GrandAverage"];
			$tmpRawGrandAverage = $existTermAverageTotal[$i]["RawGrandAverage"];
			$tmpGPA = $existTermAverageTotal[$i]["GPA"] == 0 ? "" : $existTermAverageTotal[$i]["GPA"];
			$tmpOrderMeritClass = $existTermAverageTotal[$i]["OrderMeritClass"] == 0 ? "" : $existTermAverageTotal[$i]["OrderMeritClass"];
			$tmpOrderMeritForm = $existTermAverageTotal[$i]["OrderMeritForm"] == 0 ? "" : $existTermAverageTotal[$i]["OrderMeritForm"];;
			$tmpReportColumnID = $reportTermColumnMap[$completedSemester];
			
			$existTermAverageTotalArr[$tmpReportColumnID][$tmpStudentID] = 
				array (
					"GrandTotal" => $tmpGrandTotal,
					"GrandAverage" => $tmpGrandAverage,
					"RawGrandAverage" => $tmpRawGrandAverage,
					"GPA" => $tmpGPA,
					"OrderMeritClass" => $tmpOrderMeritClass,
					"OrderMeritForm" => $tmpOrderMeritForm
				);
			$existTermAverageTotalValues[] = "('$tmpStudentID', '$ReportID', '$tmpReportColumnID', '$tmpGrandTotal', '$tmpGrandAverage', '$tmpRawGrandAverage', '$tmpGPA', '$tmpOrderMeritClass', '$tmpOrderMeritForm', '', '',NOW(), NOW())";
		}
		
		if (sizeof($existTermAverageTotalValues) > 0) {
			$existTermAverageTotalValues = implode(",", $existTermAverageTotalValues);
			$success = $this->INSERT_REPORT_RESULT($existTermAverageTotalValues);
		} else {
			$success = false;
		}
		if ($success) {
			return $existTermAverageTotalArr;
		} else {
			return false;
		}
	}
	#Ip25 only
	function ROUND_GRAND_MARKS($GrandMarkArr)
	{
		$thisGrandTotal = $GrandMarkArr["GrandTotal"];
		$thisGrandAverage = $GrandMarkArr["GrandAverage"];
		$thisGradePointAverage = $GrandMarkArr["GradePointAverage"];
		
		// round grand total according to the same setting of average???
		$thisGrandTotal = $this->ROUND_MARK($thisGrandTotal, "GrandTotal");
		$thisGrandAverage = $this->ROUND_MARK($thisGrandAverage, "GrandAverage");
		$thisGradePointAverage = $this->ROUND_MARK($thisGradePointAverage, "GrandAverage");
		
		$returnArr = array();
		$returnArr["GrandTotal"] = $thisGrandTotal;
		$returnArr["GrandAverage"] = $thisGrandAverage;
		$returnArr["GradePointAverage"] = $thisGradePointAverage;
		
		return $returnArr;
	}
	
	
	################################################################################
	#	For carmel alison SD Score calcution and Rank Ordering START	
	#Ip25 only
	# This function is for Carmel Alison Only , Used to Calculate WeightedStandardScore
	function UPDATE_SD_SCORE($ReportID, $ClassLevelID, $ReportType)
	{
		global $PATH_WRT_ROOT, $eRCTemplateSetting;
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		
		if($ReportType == "W")
		{
			$MarksColumnID = 0;
			
			$success["COPY_OVERALL_SDSCORE_FROM_TERM"] = $this->COPY_OVERALL_SDSCORE_FROM_TERM($ReportID);
		}
		else 
			$MarksColumnID = '';
			
		$thisMarksAry = $this->getMarks($ReportID, "","","",1,"","",$MarksColumnID);
		$SpcialCase = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		
		foreach((array)$thisMarksAry as $StudentID => $thisStudentMarks)
		{
			foreach((array) $thisStudentMarks as $thisSubjectID => $thisColumnMarks)
			{
				# Retrieve Subject Scheme ID & settings
				
				if(!isset($SubjectFormGradingSettingsArr[$ClassLevelID][$thisSubjectID]))
				{
					$SubjectFormGradingSettingsArr[$ClassLevelID][$thisSubjectID] = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID, 0, 0, $ReportID);
				}
				$SubjectFormGradingSettings = $SubjectFormGradingSettingsArr[$ClassLevelID][$thisSubjectID];
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				//$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
								
				foreach((array) $thisColumnMarks as $thisColumnID => $thisMarks)
				{
					if ($eRCTemplateSetting['CalculateAssessmentSDScore']==false && $thisColumnID != 0)
						continue;
						
					if(!isset($SDAVGAry[$ReportID][$thisSubjectID][$thisColumnID]))
					{
						$SDAVGAry[$ReportID][$thisSubjectID][$thisColumnID] = $this->getFormSubjectSDAndAverage($ReportID,$thisSubjectID,'',$thisColumnID); 
					}
					list($SD,$AVG) = $SDAVGAry[$ReportID][$thisSubjectID][$thisColumnID];
					
					$thisReportResultScoreID = $thisMarks['ReportResultScoreID'];
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMarks['Grade']!='' )? $thisMarks['Grade'] : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMarks['Mark'] : "";
					$thisRawMark = $thisMarks['RawMark'];
					
					$thisTargetMark = ($eRCTemplateSetting['UseRawMarkToCalculateSDScore'])? $thisRawMark : $thisMark;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisTargetMark : $thisGrade;
					
					$StandardMark = 0;
					$StandardMarkRaw = 0;
					# calculate and save Standard Score
					if (trim($thisMark)!=='' && !in_array($thisMark,$SpcialCase) && $thisMark!= -1 && $SD>0)
					{
						$StandardMarkRaw = is_numeric($thisMark)? $thisMark : 0;
						$StandardMarkRaw = ($StandardMarkRaw - $AVG) / $SD;
						$StandardMark = my_round($StandardMarkRaw, 4);
						
						// update standardScore
						$success["UPDATE_STANDARD_SCORE"][]=$this->UPDATE_STANDARD_SCORE($ReportID, $thisColumnID, $thisSubjectID,$StudentID, $StandardMark, $StandardMarkRaw, $thisReportResultScoreID);
					}
					else
					{
						$StandardMark = 0;
						$StandardMarkRaw = 0;
						$success["UPDATE_STANDARD_SCORE"][]=$this->UPDATE_STANDARD_SCORE($ReportID, $thisColumnID, $thisSubjectID,$StudentID, $StandardMark, $StandardMarkRaw, $thisReportResultScoreID);
						continue;
					}

					# only works when CalSetting=1
					if(!isset($SubjectWeightAry[$thisSubjectID])) // check if weight was retrieve before, to save query time
					{
						# get vertical weight, weight among subjects 
						$ColumnIDCond = "( ReportColumnID='' OR ReportColumnID IS NULL )";
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, $ColumnIDCond." and SubjectID = '$thisSubjectID' ");
						$SubjectWeightAry[$thisSubjectID] = $thisSubjectWeightData[0]['Weight'];
					}
					
					# Calculate overall SD Score (exclude component subjects)
					//$objSubject = new subject($thisSubjectID);
					//$isComponent = $objSubject->Is_Component_Subject();
					$isComponent = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID);
					if ($isComponent==false)
					{
						# calculate total weight, exclude special case
						if (!in_array($thisMark,$SpcialCase) && $thisMark!= -1)
							$TotalWeight[$StudentID][$thisColumnID] += $SubjectWeightAry[$thisSubjectID];
							 
						# sum up weighted SD Score
						$TargetStandardMark = ($eRCTemplateSetting['UseRawMarkToCalculateSDScore'])? $StandardMarkRaw : $StandardMark;
						$TotalWeightedSDScoreAry[$StudentID][$thisColumnID] +=  $TargetStandardMark * $SubjectWeightAry[$thisSubjectID];
					} 
				}
			}
		}
		
		# Calculate GrandSDScore 
		foreach((array)$TotalWeightedSDScoreAry as $StudentID => $ColumnWeightedSDScore)
		{
			foreach((array)$ColumnWeightedSDScore as $thisColumnID => $WeightedSDScore)
			{
				if ($eRCTemplateSetting['CalculateAssessmentSDScore']==false && $thisColumnID != 0)
						continue;
						
				$thisTotalWeight = $TotalWeight[$StudentID][$thisColumnID];
				$thisAvgWeightSDScoreRaw = $WeightedSDScore / $thisTotalWeight;
				$thisAvgWeightSDScore = my_round($thisAvgWeightSDScoreRaw, 4);

				// update grandstandardScore
				$success["UPDATE_GRAND_STANDARD_SCORE"][]=$this->UPDATE_GRAND_STANDARD_SCORE($ReportID, $thisColumnID, $StudentID, $thisAvgWeightSDScore, $thisAvgWeightSDScoreRaw);
			}
		}
		
		return in_array(false,(array)$success["UPDATE_STANDARD_SCORE"]) || in_array(false,(array)$success["UPDATE_GRAND_STANDARD_SCORE"])	?false:true;	
	}
	############# All below Are IP 25 ONLY
	function UPDATE_STANDARD_SCORE($ReportID, $ReportColumnID, $SubjectID, $StudentID, $SDScore, $RawSDScore, $ReportResultScoreID='')
	{
		$conds = '';
		if ($ReportResultScoreID != '')
			$conds .= " ReportResultScoreID = '$ReportResultScoreID' ";
		else
			$conds .= " ReportID = '$ReportID'
						AND ReportColumnID = '$ReportColumnID'
						AND SubjectID = '$SubjectID'
						AND StudentID = '$StudentID'
						";
		
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sql = "
			UPDATE 
				$table
			SET
				SDScore = '$SDScore',
				RawSDScore = '$RawSDScore'
			WHERE
				$conds
		";

		//debug_pr($sql);
		$success = $this->db_db_query($sql) or die(mysql_error());
		
		return $success;
	
	}	
	
	function UPDATE_GRAND_STANDARD_SCORE($ReportID, $ReportColumnID, $StudentID, $GrandSDScore, $RawGrandSDScore)
	{
		$table = $this->DBName.".RC_REPORT_RESULT";
			
		$sql = "
			UPDATE 
				$table
			SET
				GrandSDScore = '$GrandSDScore',
				RawGrandSDScore = '$RawGrandSDScore'
			WHERE
				ReportID = '$ReportID'
				AND ReportColumnID = '$ReportColumnID'
				AND StudentID = '$StudentID'
		";
		//debug_pr($sql);
		$success = $this->db_db_query($sql);		
		return $success;
	
	}	
	
	function COPY_OVERALL_SDSCORE_FROM_TERM($ReportID)
	{
		# Get ReportColumnID (Used to get involved ReportID)
		$ReportColumnIDArr = $this->returnReportTemplateColumnData($ReportID);
		
		for($i=0; $i<sizeof($ReportColumnIDArr);$i++)
		{
			# Get Overall Marks Of Involved Term Report Marks (i.e. ReportColumnID = 0)
			$ReportColumnID = $ReportColumnIDArr[$i]["ReportColumnID"];
			$thisReportID = $this->getCorespondingTermReportID($ReportID,$ReportColumnID);
			$thisMarksAry = $this->getMarks($thisReportID, "","","",1,"","",0);
			
			foreach((array)$thisMarksAry as $StudentID => $thisStudentMarks)
			{
				foreach((array) $thisStudentMarks as $thisSubjectID => $thisColumnMarks)
				{
					foreach((array) $thisColumnMarks as $thisColumnID => $thisMarks)
					{
						# Copy Marks to column of Consolidate Report
						$success[] = $this->UPDATE_STANDARD_SCORE($ReportID, $ReportColumnID, $thisSubjectID, $StudentID, $thisMarks["SDScore"], $thisMarks["RawSDScore"]);	
					}
				}
			}

		}
		
		return in_array(false,$success)? false:true;
		
	} 
	
	#	For carmel alison SD Score calcution and Rank Ordering END
	################################################################################
	
	function UpdateOverallGradeFromAssessmentGrade($ReportID) {
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID =  $ReportInfoArr['ClassLevelID'];
		
		### Get Subject
		$SubjectInfoAssoArr = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, 0, $ReportID);
		$SubjectIDArr = array_keys((array)$SubjectInfoAssoArr);
		
		### Get Student
		$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
		$numOfStudent = count($StudentInfoArr);
		
		### Get Report Column
		$ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
		$numOfColumn = count($ReportColumnInfoArr);
		
		### Get Weight
		$WeightAssoArr = BuildMultiKeyAssoc($this->returnReportTemplateSubjectWeightData($ReportID, '', $SubjectIDArr), array('SubjectID', 'ReportColumnID'));
				
		### Get Marks
		//$MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Mark', 'Grade', etc...] = data
		$MarksArr = $this->getMarks($ReportID, $StudentID='', $cons='', $ParentSubjectOnly=0, $includeAdjustedMarks=0, $OrderBy='', $SubjectID='', $ReportColumnID='', $CheckPositionDisplay=0, $SubOrderArr='', $ByCache=false);
		
		// loop subjects
		$SuccessArr = array();
		foreach ((array)$SubjectInfoAssoArr as $thisSubjectID => $thisSubjectInfoArr) {
			$thisSchemeID = $thisSubjectInfoArr['schemeID'];
			$thisScaleInput = $thisSubjectInfoArr['scaleInput'];
			$thisScaleDisplay = $thisSubjectInfoArr['scaleDisplay'];
			
			// for input grade and display grade subject only
			if ($thisScaleInput=='G' && $thisScaleDisplay=='G') {
				// loop students
				for ($i=0; $i<$numOfStudent; $i++) {
					$thisStudentID = $StudentInfoArr[$i]['UserID'];
					
					$thisOverallWeightedGpa = '';
					$thisOverallGrade = '';
					$thisTotalWeight = 0;
					$thisExcludedWeight = 0;
					for ($j=0; $j<$numOfColumn; $j++) {
						$thisReportColumnID = $ReportColumnInfoArr[$j]['ReportColumnID'];
						$thisWeight = $WeightAssoArr[$thisSubjectID][$thisReportColumnID]['Weight'];
						$thisTotalWeight += $thisWeight;
						
						$thisMark = $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Mark'];
						$thisGrade = $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Grade'];
						
						if ($this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
							$thisExcludedWeight += $thisWeight;
						}
						else {
							$thisGpa = $this->CONVERT_GRADE_TO_GRADE_POINT_FROM_GRADING_SCHEME($thisSchemeID, $thisGrade, $ReportID, $thisStudentID, $thisSubjectID, $ClassLevelID, $thisReportColumnID);
							$thisWeightedGpa = $thisGpa * $thisWeight;
							$thisOverallWeightedGpa += $thisWeightedGpa;
						}
					}
					
					if ($thisExcludedWeight == $thisTotalWeight) {
						$thisOverallGrade = 'N.A.';
					}
					else {
						$thisRemainingWeight = $thisTotalWeight - $thisExcludedWeight;
						if ($thisRemainingWeight > 0) {
							$thisOverallWeightedGpa = $thisOverallWeightedGpa * ($thisTotalWeight / $thisRemainingWeight);
						}
						$thisOverallWeightedGpa = my_round($thisOverallWeightedGpa, 0);
						
						$thisSchemeRangeInfoArr = $this->GET_GRADING_SCHEME_RANGE_INFO($thisSchemeID);
						$numOfRange = count($thisSchemeRangeInfoArr);
						for ($j=0; $j<$numOfRange; $j++) {
							$thisGradePoint = $thisSchemeRangeInfoArr[$j]['GradePoint'];
							$thisGrade = $thisSchemeRangeInfoArr[$j]['Grade'];
							
							if ($thisGradePoint==$thisOverallWeightedGpa) {
								$thisOverallGrade = $thisGrade;
								break;
							}
						}
					}
					
					if ($thisOverallGrade !== '') {
						$thisReportResultScoreID = $MarksArr[$thisStudentID][$thisSubjectID][0]['ReportResultScoreID'];
						
						$RC_REPORT_RESULT_SCORE = $this->DBName.".RC_REPORT_RESULT_SCORE";
						$sql = "Update $RC_REPORT_RESULT_SCORE set Grade = '".$this->Get_Safe_Sql_Query($thisOverallGrade)."' Where ReportResultScoreID = '".$thisReportResultScoreID."'";
						$SuccessArr['Student='.$thisStudentID.'_Subject='.$thisSubjectID] = $this->db_db_query($sql);
					}
				}	// end loop students
			}	// end if ($thisScaleInput=='G' && $thisScaleDisplay=='G')
		}	// end loop subjects
		
		return (in_array(false, $SuccessArr))? false : true;
	}
	
	function UpdateActualAverage($ReportID) {
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$lfs = new libfilesystem();
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID =  $ReportInfoArr['ClassLevelID'];
		$YearTermID =  $ReportInfoArr['SemID'];
		$ReportType = ($YearTermID=='F')? 'W' : 'T'; 
		
		$TermStartDate 	= (is_date_empty($ReportInfoArr['TermStartDate']))? '' : $ReportInfoArr['TermStartDate'];
		$TermEndDate 	= (is_date_empty($ReportInfoArr['TermEndDate']))? '' : $ReportInfoArr['TermEndDate'];
		
		if ($ReportType == 'W') {
			$YearTermID = 0;
		}		
		
		### Get Grand Average
		$ReportColumnID = 0;
		$ResultScoreArr = $this->getReportResultScore($ReportID, $ReportColumnID, $StudentID='', $other_conds='', $debug=0, $includeAdjustedMarks=0, $CheckPositionDisplay=0, $FilterNoRanking=0, $OrderFieldArr='');
		
		### Get all classes
		$classInfoAry = $this->GET_CLASSES_BY_FORM($ClassLevelID);
		$numOfClass = count($classInfoAry);
		
		### Get Attendance Info
		//$campassionateLeaveInfoAry[$classId][$studentId]['Days Absent', 'Time Late', 'Early Leave'] = data
		$campassionateLeaveInfoAry = array();
		$truancyInfoAry = array();
		for ($i=0; $i<$numOfClass; $i++) {
			$_classId = $classInfoAry[$i]['ClassID'];
			
			$campassionateLeaveInfoAry[$_classId] = $this->Get_Student_Profile_Attendance_Data($YearTermID, $_classId, $TermStartDate, $TermEndDate, $ClassName='', $StudentID='', '事���');
			$truancyInfoAry[$_classId] = $this->Get_Student_Profile_Attendance_Data($YearTermID, $_classId, $TermStartDate, $TermEndDate, $ClassName='', $StudentID='', '���课');
			
			//2013-0405-0845-35156
			$attendaceInfoAry[$_classId] = $this->Get_Student_Profile_Attendance_Data($YearTermID, $_classId, $TermStartDate, $TermEndDate);
		}
		
		
		$successAry = array();
		$logContent = '';
		$logContent .= "ResultID"."\t"."StudentID"."\t"."GrandAverage"."\t"."CampassionateLeave"."\t"."Truancy"."\t"."Late"."\t"."DeductMark"."\t"."ActualAverage"."\r\n";
		foreach ((array)$ResultScoreArr as $_studentId => $_studentResultAry) {
			$_resultId = $_studentResultAry[$ReportColumnID]['ResultID'];
			$_studentId = $_studentResultAry[$ReportColumnID]['StudentID'];
			$_grandAverage = $_studentResultAry[$ReportColumnID]['GrandAverage'];
			
			$_studentInfoAry = $this->Get_Student_Class_ClassLevel_Info($_studentId);
			$_classId = $_studentInfoAry[0]['ClassID'];
			
			if ($_grandAverage == -1) {
				$_actualAverage = $_grandAverage;
				$_deductMark = 0;
				$_campassionateLeave = 0;
				$_truancy = 0;
				$_late = 0;
				
				$successAry[$_studentId] = $this->UPDATE_REPORT_RESULT($_resultId, $_actualAverage);
			}
			else {
				$_campassionateLeave = $campassionateLeaveInfoAry[$_classId][$_studentId]['Days Absent'];
				$_campassionateLeave = ($_campassionateLeave=='')? 0 : $_campassionateLeave;
				
				$_truancy = $truancyInfoAry[$_classId][$_studentId]['Days Absent'];
				$_truancy = ($_truancy=='')? 0 : $_truancy;
				
				//2013-0405-0845-35156
				$_late = $attendaceInfoAry[$_classId][$_studentId]['Time Late'];
				$_late = ($_late=='')? 0 : $_late;
				
				$_deductMark = $_campassionateLeave * 0.2 + $_truancy * 0.5 + $_late * 0.01;
				$_actualAverage = $_grandAverage - $_deductMark; 
				
				$successAry[$_studentId] = $this->UPDATE_REPORT_RESULT($_resultId, $_actualAverage);
			}
			
			$logContent .= $_resultId."\t".$_studentId."\t".$_grandAverage."\t".$_campassionateLeave."\t".$_truancy."\t".$_late."\t".$_deductMark."\t".$_actualAverage."\r\n";
		}
		
		### Log the Processs in a file
		$logFolder = $intranet_root."/file/reportcard2008/".$this->GET_ACTIVE_YEAR()."/report_generate_log";
		if(!file_exists($logFolder)) {
			$SuccessAry['log']['createLogFolder'] = $lfs->folder_new($logFolder);
		}
		$logFile = $logFolder.'/UpdateActualAverage_'.$ReportID.'_'.date('Ymd_His').'_'.$_SESSION['UserID'].'.txt';
		$successAry['log']['writeLog'] = $lfs->file_write($logContent, $logFile);
		
		return in_array(false, (array)$successAry)? false : true;
	}
	
	function UpdatePassingStatusSpecialChecking($ReportID) {
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$lfs = new libfilesystem();
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$classLevelId =  $reportInfoAry['ClassLevelID'];
		
		$reportColumnAry = $this->returnReportTemplateColumnData($ReportID);
		$reportColumnAry[count($reportColumnAry)]['ReportColumnID'] = 0;
		$numOfColumn = count($reportColumnAry);
		
		$studentIdAry = Get_Array_By_Key($this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $classLevelId), 'UserID');
		$numOfStudent = count($studentIdAry);
		
		$subjectAry = $this->returnSubjectwOrder($classLevelId, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		$gradingSchemeAry = $this->GET_SUBJECT_FORM_GRADING($classLevelId, $SubjectID='', $withGrandResult=0, $returnAsso=1, $ReportID);
		$marksAry = $this->getMarksCommonIpEj($ReportID, $StudentID='', $cons='', $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', $SubjectID='', $ReportColumnID='', $CheckPositionDisplay=0, $SubOrderArr='', $ByCache=false);
		$markNatureAry = $this->Get_Student_Subject_Mark_Nature_Arr($ReportID, $classLevelId, $marksAry);
		
		
		$successAry = array();
		$logContent = '';
		$logContent .= "ConvertType"."\t"."StudentID"."\t"."SubjectID"."\t"."ReportColumnID"."\t"."OriginalGrade"."\t"."NewGrade"."\t"."RecordID"."\r\n";
		
		for ($i=0; $i<$numOfStudent; $i++) {
			$_studentId = $studentIdAry[$i];
			
			### if all columns grade is pass => overall column grade is pass also
			foreach ((array)$subjectAry as $__parentSubjectId => $__cmpSubjectAry) {
				$__numOfCmpSubject = count($__cmpSubjectAry) - 1;
				$__isParentSubject = ($__numOfCmpSubject==0)? false : true;
				
				foreach ((array)$__cmpSubjectAry as $___cmpSubjectId => $___cmpSubjectName) {
					$___isCmpSubject = ($___cmpSubjectId == 0)? false : true;
					$___subjectId = ($___isCmpSubject)? $___cmpSubjectId : $__parentSubjectId;
					
					$___schemeId = $gradingSchemeAry[$___subjectId]['SchemeID'];
					$___scaleInput = $gradingSchemeAry[$___subjectId]['ScaleInput'];
					
					// skip parent subject
					if ($__isParentSubject && !$___isCmpSubject) {
						continue;
					}
					
					// Consider input mark subject only
					if ($___scaleInput == 'M') {
						// check if overall grade is failed or not
						$___originalOverallGradeNature = $markNatureAry[$_studentId][$___subjectId][0];
						
						// do special grade conversion for failed subject only
						// check if all columns' grades are pass or not
						if ($___originalOverallGradeNature == 'Fail') {
							$___isAllColumnGradePass = true;
							
							for ($j=0; $j<$numOfColumn; $j++) {
								$____reportColumnId = $reportColumnAry[$j]['ReportColumnID'];
								
								if ($____reportColumnId == 0) {
									continue;
								}
								
								$____columnGradeNature = $markNatureAry[$_studentId][$___subjectId][$____reportColumnId];
								if ($____columnGradeNature == 'Fail') {
									$___isAllColumnGradePass = false;
									break;
								}
							}
							
							// convert the subject grade to the last passing grade if all other columns' grades are pass
							if ($___isAllColumnGradePass) {
								$___lastPassingGrade = $this->GET_GRADING_SCHEME_LAST_PASSING_GRADE($___schemeId);
								$___reportResultScoreId = $marksAry[$_studentId][$___subjectId][0]['ReportResultScoreID'];
								$___originalMark = $marksAry[$_studentId][$___subjectId][0]['Mark'];
								$___originalGrade = $marksAry[$_studentId][$___subjectId][0]['Grade'];
								
								$successAry['subjectOverallGradeChange']['student'.$_studentId.'_subject'.$___subjectId] = $this->UPDATE_REPORT_RESULT_SCORE('', '', '', '', $___originalMark, $___lastPassingGrade, $___reportResultScoreId);
								$logContent .= "subjectOverallGradeChange"."\t".$_studentId."\t".$___subjectId."\t"."0"."\t".$___originalGrade."\t".$___lastPassingGrade."\t".$___reportResultScoreId."\r\n";
							}
						}
					}
				}	// end loop component subject
			}	// end loop parent subject
			
			
			### If all component subjects are pass => parent subject is also pass
			foreach ((array)$subjectAry as $__parentSubjectId => $__cmpSubjectAry) {
				$__numOfCmpSubject = count($__cmpSubjectAry) - 1;
				$__isParentSubject = ($__numOfCmpSubject==0)? false : true;
				
				if (!$__isParentSubject) {
					continue;
				}
				
				$__schemeId = $gradingSchemeAry[$__parentSubjectId]['SchemeID'];
				$__lastPassingGrade = $this->GET_GRADING_SCHEME_LAST_PASSING_GRADE($__schemeId);
				
				for ($j=0; $j<$numOfColumn; $j++) {
					$___reportColumnId = $reportColumnAry[$j]['ReportColumnID'];
					
					// do special conversion for failed subject only
					$___originalGradeNature = $markNatureAry[$_studentId][$__parentSubjectId][$___reportColumnId];
					if ($___originalGradeNature != 'Fail') {
						continue;
					}
					
					$___isAllComponentPass = true;
					foreach ((array)$__cmpSubjectAry as $____cmpSubjectId => $____cmpSubjectName) {
						$____isCmpSubject = ($____cmpSubjectId == 0)? false : true;
						$____subjectId = ($____isCmpSubject)? $____cmpSubjectId : $__parentSubjectId;
						
						if (!$____isCmpSubject) {
							continue;
						}
						
						$____scaleInput = $gradingSchemeAry[$____subjectId]['ScaleInput'];
						if ($____scaleInput == 'M') {
							$____cmpGradeNature = $markNatureAry[$_studentId][$____subjectId][$___reportColumnId];
							
							if ($____cmpGradeNature == 'Fail') {
								$___isAllComponentPass = false;
								break;
							}
						}
					}
					
					if ($___isAllComponentPass) {
						$___reportResultScoreId = $marksAry[$_studentId][$__parentSubjectId][$___reportColumnId]['ReportResultScoreID'];
						$___originalMark = $marksAry[$_studentId][$__parentSubjectId][$___reportColumnId]['Mark'];
						$___originalGrade = $marksAry[$_studentId][$__parentSubjectId][$___reportColumnId]['Grade'];
						
						$successAry['parentSubjectGradeChange']['student'.$_studentId.'_parentSubject'.$__parentSubjectId.'_reportColumnId'.$___reportColumnId] = $this->UPDATE_REPORT_RESULT_SCORE('', '', '', '', $___originalMark, $__lastPassingGrade, $___reportResultScoreId);
						$logContent .= "parentSubjectGradeChange"."\t".$_studentId."\t".$__parentSubjectId."\t".$___reportColumnId."\t".$___originalGrade."\t".$__lastPassingGrade."\t".$___reportResultScoreId."\r\n";
					}
				}
			}
			
		}	// end loop student
		
		
		
		### Log the Processs in a file
		$logFolder = $intranet_root."/file/reportcard2008/".$this->GET_ACTIVE_YEAR()."/report_generate_log";
		if(!file_exists($logFolder)) {
			$SuccessAry['log']['createLogFolder'] = $lfs->folder_new($logFolder);
		}
		$logFile = $logFolder.'/UpdatePassingStatusSpecialChecking_'.$ReportID.'_'.date('Ymd_His').'_'.$_SESSION['UserID'].'.txt';
		$successAry['log']['writeLog'] = $lfs->file_write($logContent, $logFile);
		
		return !in_array(false, (array)$successAry);
	}
}
?>