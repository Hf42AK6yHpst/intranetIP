<?php
// page editing by: 
if (!defined("LIBIMAP_DEFINED"))                     // Preprocessor directive
{
define("LIBIMAP_DEFINED", true);

//include_once("osaccount.php");
include_once("libosaccount.php");
//include_once("httpclient.php");

//include_once("$intranet_root/includes/libwebmail.php");

//For checking sendmail
//include_once("database.php");
//include_once("user.php");
//include_once("common_function.php");
//include_once("$intranet_root/includes/libaccess.php");

//include_once("PHPMailer/PHPMailer.php");

class imap extends libosaccount
{

        var $has_webmail;
  var $type;
  var $host;
  var $folderDelimiter;
  var $mailaddr_domain;
  var $mailSystemAction;
  var $message_format;
  var $mail_server_port;

  var $inbox;

 var $embed_part_ids;

  var $times;

  var $parsed_part;          # Variable for parse_message
  var $message_charset;      # Variable for retrieve_message
  var $is_html_message;      # Variable for storing the flag of HTML message
  var $mime_type;
  var $mime_encoding;
        var $CurUserAddress;
        var $CurUserName;
        var $CurUserPassword;
        var $RootPrefix;
        var $MimeLineBreak;
        var $login_type;
        var $DefaultFolderList;
        var $FolderPrefix;

      var $mail_api_url;         # Separated mail server API


      ### Constructor:
      ### Param: skipAutoLogin - not open imap connection if true, targetLogin/targetPassword - use this login information, if it is set, otherwise use session variables
      function imap($skipAutoLogin=false, $targetLogin = "", $targetPassword="",$isHalfOpen=false)
      {
               $this->osaccount();

               /// Temp development use
               //$targetLogin = "roundcube";
               //$targetPassword = "roundcubeeclass";
               //$targetLogin = "tguser3@tg-a.broadlearning.com";
               //$targetPassword = "eclasstguser3";

               // for alpha
               /*$targetLogin = "roundcube";
               //$targetPassword = "roundcubeeclass";*/

               // for beta
               /*$targetLogin = "roundcube";
               //$targetPassword = "roundcubeeclass";*/
               // temp mailbox open, should remove later(20080425)
                                                         //$this->Temp_Init();

               global $plugin,$webmail_SystemType,$webmail_info,$SYS_CONFIG;

               #### Kenneth Wong (20080531): Fixed iMail configuration for ESF project
               $this->has_webmail = true;
               $this->type = 3;

               #######

               #### Configuration from config file
               if ($_SESSION['SSV_USER_TYPE'] == 'S') {
               	 $this->FolderPrefix = $SYS_CONFIG['StudMail']['FolderPrefix'];
	               $this->folderDelimiter = ($SYS_CONFIG['StudMail']['FolderDelimiter']==""?".":$SYS_CONFIG['StudMail']['FolderDelimiter']);
	               $this->RootPrefix = ($SYS_CONFIG['StudMail']['FolderPrefix'] == "")? "":$SYS_CONFIG['StudMail']['FolderPrefix'].$this->folderDelimiter;
	               $this->MimeLineBreak = $SYS_CONFIG['Mail']['MimeLineBreak'];
	               $this->host = $SYS_CONFIG['StudMail']['ServerIP'];
	               $this->mail_server_port = $SYS_CONFIG['StudMail']['Port'].'/ssl';
	               $this->mailaddr_domain = $SYS_CONFIG['StudMail']['UserNameSubfix'];                # Default mail domain only, domain name should be passed together with login name in ESF
	               $this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);
	             }
	             else {
	             	 $this->FolderPrefix = $SYS_CONFIG['Mail']['FolderPrefix'];
	             	 $this->folderDelimiter = ($SYS_CONFIG['Mail']['FolderDelimiter']==""?".":$SYS_CONFIG['Mail']['FolderDelimiter']);
	               $this->RootPrefix = ($SYS_CONFIG['Mail']['FolderPrefix'] == "")? "":$SYS_CONFIG['Mail']['FolderPrefix'].$this->folderDelimiter;
	               $this->MimeLineBreak = $SYS_CONFIG['Mail']['MimeLineBreak'];
	               $this->host = $SYS_CONFIG['Mail']['ServerIP'];
	               $this->mail_server_port = $SYS_CONFIG['Mail']['Port'];
	               $this->mailaddr_domain = $SYS_CONFIG['Mail']['UserNameSubfix'];                # Default mail domain only, domain name should be passed together with login name in ESF
	               $this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);
	             }

               #### preset info
               $this->mime_type = array("text", "multipart", "message", "application", "audio", "image", "video", "other");
               $this->mime_encoding = array("7bit", "8bit", "binary", "base64", "quoted-printable", "other", "uuencode");
               $this->message_format = 'html';

               // Parent Object attribute
               $this->actype = "postfix";
               $this->login_type = "email";
               $this->domain_name = $this->mailaddr_domain;
               // End of parent Object attribute

               ### API path
               $this->mail_api_url['GetAliasInfo'] = "/osapi/getAliasInfo.php";
               $this->mail_api_url['GetAliasList'] = "/osapi/getAliasList.php";
               $this->mail_api_url['AddAlias'] = "/osapi/addAlias.php";
               $this->mail_api_url['RemoveAlias'] = "/osapi/removeAlias.php";

               $this->mail_api_url['AddAutoReply'] = "/osapi/addAutoreply.php";
			   $this->mail_api_url['AddAliasAutoReply'] = "/osapi/addAliasAutoreply.php";	# added on 8 Dec 2008
               $this->mail_api_url['ReadAutoReply'] = "/osapi/readAutoreply.php";
               $this->mail_api_url['RemoveAutoReply'] = "/osapi/removeAutoreply.php";
			   $this->mail_api_url['RemoveAliasAutoReply'] = "/osapi/removeAliasAutoreply.php";
			   
			   $this->mail_api_url['SetGroupMailForward'] = "/osapi/setGroupMailForward.php"; #added on 2009/5/15
			   $this->mail_api_url['RemoveGroupMailForward'] = "/osapi/removeGroupMailForward.php"; #added on 2009/5/14

               $this->mail_api_url['AddBlackList'] = "/osapi/addBlackList.php";
               $this->mail_api_url['ReadBlackList'] = "/osapi/readBlackList.php";
               $this->mail_api_url['RemoveBlackListRecord'] = "/osapi/removeBlackListRecord.php";

               $this->mail_api_url['AddWhiteList'] = "/osapi/addWhiteList.php";
               $this->mail_api_url['ReadWhiteList'] = "/osapi/readWhiteList.php";
               $this->mail_api_url['RemoveWhiteList'] = "/osapi/removeWhiteListRecord.php";

               $this->mail_api_url['BypassSpamCheck'] = "/osapi/bypassSpamCheck.php";
               $this->mail_api_url['BypassVirusCheck'] = "/osapi/bypassVirusCheck.php";
               $this->mail_api_url['ReadSpamCheck'] = "/osapi/readSpamStatus.php";
               $this->mail_api_url['ReadVirusCheck'] = "/osapi/readVirusStatus.php";
			   
			   #added on 2009-11-13
			   $this->mail_api_url['GetUsedQuotaApi'] = '/osapi/getUsedQuota.php'; 
			   $this->mail_api_url['GetTotalQuotaApi'] = '/osapi/getTotalQuota.php';

               # Login info
               if ($targetLogin != "")
               {
                   $this->CurUserAddress = $targetLogin;
                   $this->CurUserPassword = $targetPassword;
               }
               else
               {
                   /*$this->CurUserName = $_SESSION['SSV_LOGIN_LOGIN'];
                   $this->CurUserPassword = $_SESSION['SSV_LOGIN_PASSWORD'];
                   $this->CurUserAddress = ($_SESSION['SSV_LOGIN_EMAIL']==""?($this->CurUserName."@".$this->mailaddr_domain):$_SESSION['SSV_LOGIN_EMAIL']);*/
                   // changed to entertain the mailbox switch function
                   $this->CurUserName = $_SESSION['SSV_EMAIL_LOGIN'];
                   $this->CurUserPassword = $_SESSION['SSV_EMAIL_PASSWORD'];
                   $this->CurUserAddress = ($_SESSION['SSV_LOGIN_EMAIL']==""?($this->CurUserName."@".$this->mailaddr_domain):$_SESSION['SSV_LOGIN_EMAIL']);
                   
                   if ($this->CurUserAddress == 'tony.chan@mail22.esf.edu.hk' || 
                   		 $this->CurUserAddress == 'testing3@esfcentre.edu.hk' || 
                   		 $this->CurUserAddress == 'testing1@esfcentre.edu.hk' || 
                   		 $this->CurUserAddress == 'testing2@esfcentre.edu.hk' || 
                   		 $this->CurUserAddress == 'tgcestest1@esfcentre.edu.hk' || 
	    								 $this->CurUserAddress == 'tgcestest2@esfcentre.edu.hk' || 
	    								 $this->CurUserAddress == 'tgcestest3@esfcentre.edu.hk') {
                   	$this->host = $SYS_CONFIG['ExchangeServerIP'];
                   }
               }
							
							 //ad hoc flow to handle exchange server type 2
               if ($SYS_CONFIG['Mail']['ServerType'] == "exchange" && $SYS_CONFIG['Mail']['ExchangeUsernameLogin'] && $this->CurUserAddress != "tgcestest1@esfcentre.edu.hk" && $this->CurUserAddress != 'testing2@esfcentre.edu.hk' && $this->CurUserAddress != 'testing3@esfcentre.edu.hk' && $this->CurUserAddress != 'testing1@esfcentre.edu.hk')
               {
	                $this->CurUserName = substr($this->CurUserAddress, 0, strpos($this->CurUserAddress, "@")); //get the username before '@' as the login to exchange server
               }	
								
               if (!$skipAutoLogin)
               {
                    $this->openInbox($this->CurUserAddress, $this->CurUserPassword,'', $isHalfOpen);

                    if ($this->inbox)
                    {
                    		if ($_SESSION['SSV_USER_TYPE'] == 'S') {
                    			$DefaultFolderList = $SYS_CONFIG['StudMail']['SystemMailBox'];
                    		}
                    		else {
                    			$DefaultFolderList = $SYS_CONFIG['Mail']['SystemMailBox'];
                    		}
                    		
                        // create default folder list
                        foreach ($DefaultFolderList as $Key=> $Val) {
                        	//if ($Val != $SYS_CONFIG['Mail']['FolderPrefix']) {
                          	//$Result[$Val.'Create'] = $this->createMailFolder($SYS_CONFIG['Mail']['FolderPrefix'],$Val);
                            $DefaultFolderList[$Key] = $this->RootPrefix.$Val;
                         	//}
                        }
                        //for ($i=0; $i < sizeof($DefaultFolderList); $i++) {
                             //if ($DefaultFolderList[$i] != $SYS_CONFIG['Mail']['FolderPrefix']) {
                                 //$Result[$DefaultFolderList[$i].'Create'] = $this->createMailFolder($SYS_CONFIG['Mail']['FolderPrefix'],$DefaultFolderList[$i]);
                                 //$DefaultFolderList[$i] = $this->RootPrefix.$DefaultFolderList[$i];
                             //}
                        //}
                    }
                    $this->DefaultFolderList = $DefaultFolderList;

                                                                                // create new preference record if record doesn't exists
                                                                                intranet_opendb();
                                                                                $this->New_Preference($this->CurUserAddress);
               }
               else
               {
                   $this->inbox = null;
               }

      } // End of constructor

      function open_account($UserLogin, $UserPassword, $quota="")
      {
               global $webmail_info;
               return $this->openAccount($UserLogin,$UserPassword);       # Call parent method
      }

      function delete_account ($UserLogin)
      {
               return $this->removeAccount($UserLogin);            # Call parent method
      }

      function change_password ($UserLogin, $NewPassword)
      {
               return $this->changePassword($UserLogin,$NewPassword);  # Call parent method
      }

      function is_user_exist($UserLogin)
      {
               return $this->isAccountExist($UserLogin);  # Call parent method
      }
	  
	  # login : email address
	  # quota : set to be (MB)
	  function set_total_quota($login, $usertype, $quota='')
	  {
			   global $webmail_info, $SYS_CONFIG;
			   $isvalid = false;
			   switch ($usertype){
					case 'T':	
						$quota = $SYS_CONFIG['mail']['MaxQuotaOfStaffMailBox']; 
						$isvalid = true;
						break;
					case 'S':
						$quota = '';
						$isvalid = true;
						break;
					/* Other usertype could be added in below */
					default:	
						$quota = ''; 
						break;
			   }
			   if(!$isvalid){
					return false;
			   } else {
					return ($quota != '') ? $this->setTotalQuota($login, $quota) : true;       # Call parent method
			   }
	  }


      # Functions for Type 3 Only
      # ------------------------------------

      function encodeFolderName($folderName)
      {
              return $this->Conver_Encoding($folderName, "UTF7-IMAP", "UTF-8");
      }

      function decodeFolderName($folderName)
      {
              return $this->Conver_Encoding($folderName, "UTF-8", "UTF7-IMAP");
              //big5 way of retrieving the folder name
      }

      # Connect to mail server
      # Store the connection resource variable in $this->connection
      function openInbox($UserLogin, $UserPassword,$tarGetfolder='', $isHalfOpen=false)
      {
               global $webmail_info;
               global $intranet_inbox_connect_failed;
               global $SYS_CONFIG;
               
               $UserLogin = strtolower($UserLogin);
               if ($SYS_CONFIG['Mail']['ServerType'] == "exchange" && $SYS_CONFIG['Mail']['ExchangeUsernameLogin'] && $this->CurUserAddress != "tgcestest1@esfcentre.edu.hk")
               {
										$login_string = substr($UserLogin, 0, strpos($UserLogin, "@"));
               }
               else
               {
                    $login_string = $this->attachDomain($UserLogin);
               }
               
               $mailbox = $this->getMailServerRef().($targetFolder!=''?$this->encodeFolderName($targetFolder):"");
               if ($isHalfOpen)
               {
                   $inbox = imap_open($mailbox,$login_string,$UserPassword,OP_HALFOPEN);
               }
               else
               {
                   $inbox = imap_open($mailbox,$login_string,$UserPassword);
               }

               if (!$inbox)
               {
                    $intranet_inbox_connect_failed = true;
                    return false;
               }
               $this->inbox = $inbox;
               return $this->inbox;
      }

      # Close the stream
      function close()
      {
               if ($this->inbox)
                   @imap_close($this->inbox,CL_EXPUNGE);
      }

      # Remove Mail in msgno
      function removeMail($msgno)
      {
               if ($this->inbox)
               {
                   return imap_delete($this->inbox,$msgno);
               }
               else return false;
      }

      # return number of mails in the inbox
      function Check_Mail_Count($Folder)
      {
              imap_reopen($this->inbox,$this->getMailServerRef().$this->encodeFolderName($Folder));
                                return imap_num_msg($this->inbox);
      }

      /* Old IP 2.0 functions */

      # Return mail information if the inbox is not empty
      # Attachment needs to copy to path if any
      # Remove the mail immediately
      #
      # Param:
      # $quota - Try to get most mails if quota is enough. (You can refer space with mails or attachment only, just depends which is easy to get)
      #          If no limit , $quota == "UNLIMIT";
      #
      # Return : false if quota not enough
      #          "empty" if the inbox is empty
      #          array of (subject,message,sender address, receiver addresses(To),receiver addresses (cc),receiver addresses (bcc)  , attachment path, date of receive)
      #
      # Attachment path can be get by $this->getNewAttachmentPath()
      # You can write the files using function write_file_content($content,$filepath)
      # Not used as use checkmailOne
      function checkMail($quota)
      {
               $this->embed_part_ids = array();
               $lf = new libfilesystem();
               global $file_path;
               $mailCount = $this->checkMailCount();
               if ($mailCount==0) return;

               $used_quota = 0;                     # Calculate in Kbytes

               $overview = imap_fetch_overview($this->inbox, "1:".$mailCount);

               if (!is_array($overview)) return;

               reset($overview);
               while (list($key,$val) = each($overview))
               {
                      $this->embed_part_ids = array();
                      $msg_no = imap_msgno($this->inbox, $val->uid);      # Messge Sequence Number

                      # Grab Structure of the mail
                      $structure = imap_fetchstructure($this->inbox, $val->uid, FT_UID);
                      $i = 1;
                      while (imap_fetchbody( $this->inbox, $val->uid, $i , FT_UID) )
                      {
                             $structure->parts[$i-1] = imap_bodystruct( $this->inbox, $msg_no, $i );
                             $i++;
                      }

                      # Subject
                      $subject_decoded = imap_mime_header_decode($val->subject);
                      $subject = trim($subject_decoded[0]->text);
                      if ($subject == "")
                      {
                          $subject = "[Unknown]";
                      }

                      # Size (in Kbytes)
                      $size = number_format ($val->size / 1024, 1);
                      # Check Quota enough

                      $used += $size;
                      if ($quota != 0 && $quota < $used)
                      {
                          return $mail;
                      }

                      # Grab information
                      $headerinfo = imap_headerinfo($this->inbox, $msg_no);
                      $temp = imap_mime_header_decode($headerinfo->fromaddress);
                      $fromaddress = "";
                      $delim = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $fromaddress .= $delim.$temp[$i]->text;
                           $delim = " ";
                      }
                      $temp = imap_mime_header_decode($headerinfo->toaddress);
                      $delim = "";
                      $toaddress = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $toaddress .= $delim.$temp[$i]->text;
                           $delim = ",";
                      }
                      $temp = imap_mime_header_decode($headerinfo->reply_toaddress);
                      $delim = "";
                      $reply_toaddress = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $reply_toaddress .= $delim.$temp[$i]->text;
                           $delim = ",";
                      }
                      if (isset($headerinfo->ccaddress))
                      {
                          $temp = imap_mime_header_decode($headerinfo->ccaddress);
                          $delim = "";
                          $ccaddress = "";
                          for ($i=0; $i<sizeof($temp); $i++)
                          {
                               $ccaddress .= $delim.$temp[$i]->text;
                               $delim = ",";
                          }
                      }
                      $dateReceive = date('Y-m-d H:i:s',strtotime($val->date));

                      # Attachment Path
                      $attachment_path = $this->getNewAttachmentPath();
                      $actual_file_path = $file_path."/file/mail/".$attachment_path;

                      # Message Content
                      $message = $this->parseMessageBody($this->inbox, $val->uid, $structure,'',$actual_file_path);
                      $filtered_message = $this->removeHTMLHeader($message);
                      $isHTML = ($filtered_message != $message);

                      # Get Attachment Size
                      if (is_dir($actual_file_path))
                      {
                          $temp = $lf->folder_size($actual_file_path);
                          $attachment_size = $temp[0]/1024;
                      }
                      else $attachment_size = 0;
                      $mail[] = array($val->uid,$subject,$filtered_message,$isHTML,$attachment_size,$fromaddress,$toaddress,$ccaddress,$dateReceive,$attachment_path);

               }
               return $mail;
      }
      # Return ONE and ONLY ONE mail information if the inbox is not empty
      # Same as checkMail()
      function checkOneMail($quota)
      {
               $this->embed_part_ids = array();
               $lf = new libfilesystem();
               global $file_path;
               $mailCount = $this->checkMailCount();
               if ($mailCount==0) return false;

               $used_quota = 0;                     # Calculate in Kbytes

               $overview = imap_fetch_overview($this->inbox, "1:".$mailCount);

               if (!is_array($overview)) return;

               reset($overview);
               while (list($key,$val) = each($overview))
               {
                      $msg_no = imap_msgno($this->inbox, $val->uid);      # Messge Sequence Number

                      # Grab Structure of the mail
                      $structure = imap_fetchstructure($this->inbox, $msg_no, FT_UID);
                      $i = 1;
                      while (imap_fetchbody( $this->inbox, $msg_no, $i , FT_UID) )
                      {
                             $structure->parts[$i-1] = imap_bodystruct( $this->inbox, $msg_no, $i );
                             /*
                             $j = 1;
                             while( imap_fetchbody( $this->inbox, $msg_no, "$i.$j" ) ){
                                    $structure->parts[$i-1]->parts[$j-1] = imap_bodystruct( $this->inbox, $msg_no, "$i.$j" );
                                    $j++;
                             }
                             */


                             $i++;
                      }
                      /*
                      $i = 1;
                      while (imap_fetchbody( $this->inbox, $val->uid, $i , FT_UID) )
                      {
                             $structure->parts[$i-1] = imap_bodystruct( $this->inbox, $msg_no, $i );
                             $i++;
                      }
*/
                      # Subject
                      $subject_decoded = imap_mime_header_decode($val->subject);
                      $subject = trim($subject_decoded[0]->text);
                      if ($subject == "")
                      {
                          $subject = "[Unknown]";
                      }

                      # Size (in Kbytes)
                      $size = number_format ($val->size / 1024, 1);
                      # Check Quota enough

                      $used += $size;
                      if ($quota != 0 && $quota < $used)
                      {
                          return $mail;
                      }

                      # Grab information
                      $headerinfo = imap_headerinfo($this->inbox, $msg_no);
                      $temp = imap_mime_header_decode($headerinfo->fromaddress);
                      $fromaddress = "";
                      $delim = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $fromaddress .= $delim.$temp[$i]->text;
                           $delim = " ";
                      }
                      $temp = imap_mime_header_decode($headerinfo->toaddress);
                      $delim = "";
                      $toaddress = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $toaddress .= $delim.$temp[$i]->text;
                           $delim = ",";
                      }
                      $temp = imap_mime_header_decode($headerinfo->reply_toaddress);
                      $delim = "";
                      $reply_toaddress = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $reply_toaddress .= $delim.$temp[$i]->text;
                           $delim = ",";
                      }
                      if (isset($headerinfo->ccaddress))
                      {
                          $temp = imap_mime_header_decode($headerinfo->ccaddress);
                          $delim = "";
                          $ccaddress = "";
                          for ($i=0; $i<sizeof($temp); $i++)
                          {
                               $ccaddress .= $delim.$temp[$i]->text;
                               $delim = ",";
                          }
                      }
                      $dateReceive = date('Y-m-d H:i:s',strtotime($val->date));

                      # Attachment Path
                      $attachment_path = $this->getNewAttachmentPath();
                      $actual_file_path = $file_path."/file/mail/".$attachment_path;

                      # Message Content
                      $message = $this->parseMessageBody($this->inbox, $val->uid, $structure,'',$actual_file_path);
                      $filtered_message = $this->removeHTMLHeader($message);
                      $isHTML = ($filtered_message != $message);

                      # Get Attachment Size
                      if (is_dir($actual_file_path))
                      {
                          $temp = $lf->folder_size($actual_file_path);
                          $attachment_size = $temp[0]/1024;
                      }
                      else $attachment_size = 0;
                      $mail = array($val->uid,$subject,$filtered_message,$isHTML,$attachment_size,$fromaddress,$toaddress,$ccaddress,$dateReceive,$attachment_path);
                      return $mail;

               }
               return false;
      }

      # Send external mails
      # Mail Subject: $subject
      # Mail Content: $message
      # To: $receiver_to (array)
      # CC: $receiver_cc (array)
      # Bcc: $receiver_bcc (array)
      # Attachment: All files under $attachment_path (no sub-directory)
      # Return true if send is successful
      function sendMail_old($subject,$message,$from,$receiver_to,$receiver_cc,$receiver_bcc,$attachment_path,$IsImportant="")
      {
               # Content-type of attachment
               $type="application/octet-stream";

               $priority = ($IsImportant==1? 1: 3);

               # receiving email addresses
               $numTo = sizeof($receiver_to);
               $numCC = sizeof($receiver_cc);
               $numBCC = sizeof($receiver_bcc);
               if ($numTo + $numCC + $numBCC == 0) return false;

               $to = ($numTo != 0)? implode(", ",$receiver_to) : "";
               $cc = ($numCC != 0)? implode(", ",$receiver_cc) : "";
               $bcc = ($numBCC != 0)? implode(", ",$receiver_bcc) : "";

               # Mail Header
               $headers='';
               $files = $this->listfiles($attachment_path);
               $isMulti = (sizeof($files)!=0);

               $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
               if ($isMulti)
               {
                   $headers .= "MIME-Version: 1.0".$this->MimeLineBreak;
                   $headers .= "X-Priority: $priority".$this->MimeLineBreak;
                   $headers .= "Content-Type: multipart/mixed; ";
                   $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
               }
               $headers .= "From: $from".$this->MimeLineBreak;
               if ($cc != "")
                   $headers .= "cc: $cc".$this->MimeLineBreak; // CC to
               if ($bcc != "")
                   $headers .= "bcc: $bcc".$this->MimeLineBreak; // BCCs to, separete multiple with commas mail@mail.com, mail2@mail.com

               if ($isMulti)
               {
                   $mime = "This is a multi-part message in MIME format.".$this->MimeLineBreak;
                   $mime .= "".$this->MimeLineBreak;
                   $mime .= "--" . $mime_boundary . "".$this->MimeLineBreak;

                   # Message Body
                   $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
                   $mime .= "Content-Type: text/plain; charset=\"iso-8859-1\"".$this->MimeLineBreak;
                   $mime .= "".$this->MimeLineBreak;
               }
               $mime .= $message . $this->MimeLineBreak.$this->MimeLineBreak;
               if ($isMulti)
               {
                   $mime .= "--" . $mime_boundary . $this->MimeLineBreak;
               }

               # Embed attachment files
               for ($i=0; $i<sizeof($files); $i++)
               {
                    $target = $files[$i];
                    $data = Get_File_Content("$attachment_path/$target");
                    $fname = addslashes($target);

                    $mime .= "Content-Type: $type;".$this->MimeLineBreak;
                    $mime .= "\tname=\"$fname\"".$this->MimeLineBreak;
                    $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
                    $mime .= "Content-Disposition: attachment;".$this->MimeLineBreak;
                    $mime .= "\tfilename=\"$fname\"".$this->MimeLineBreak.$this->MimeLineBreak;
                    $mime .= base64_encode($data);
                    $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
                    $mime .= "--" . $mime_boundary;

                    # Different if end of email
                    if ($i==sizeof($files)-1)
                        $mime .= "--".$this->MimeLineBreak;
                    else
                        $mime .= $this->MimeLineBreak;
               }
               $result = mail($to, $subject, $mime, $headers);
               return $result;
      }

      # New Method
      # Send external mails
      # Mail Subject: $subject
      # Mail Content: $message
      # To: $receiver_to (array)
      # CC: $receiver_cc (array)
      # Bcc: $receiver_bcc (array)
      # Attachment: All files under $attachment_path (no sub-directory)
      # Return true if send is successful
      function sendMail($subject,$message,$from,$receiver_to,$receiver_cc,$receiver_bcc,$attachment_path,$IsImportant="",$mail_return_path="",$reply_address="")
      {
                      global $intranet_session_language,$intranet_default_lang_set;

               # Content-type of attachment
               $type="application/octet-stream";

               $priority = ($IsImportant==1? 1: 3);

               # receiving email addresses
               $numTo = sizeof($receiver_to);
               $numCC = sizeof($receiver_cc);
               $numBCC = sizeof($receiver_bcc);
               if ($numTo + $numCC + $numBCC == 0) return false;

               $to = ($numTo != 0)? implode(", ",$receiver_to) : "";
               $cc = ($numCC != 0)? implode(", ",$receiver_cc) : "";
               $bcc = ($numBCC != 0)? implode(", ",$receiver_bcc) : "";

               # Mail Header
               $headers='';
               $files = $this->listfiles($attachment_path);
               global $intranet_session_language;
               $charset = (($intranet_session_language=="gb")?"gb2312":"big5");

               #$charset = (isBig5($message)?"big5":"iso-8859-1");
               #$isHTMLMessage = $this->isHTMLContent($message);
               global $special_feature;
               $isHTMLMessage = ($special_feature['imail_richtext']);

               $isMulti = (sizeof($files)!=0);

               $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
               $mime_boundary2 = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for multi-part
               if ($isMulti)
               {
                   $headers .= "MIME-Version: 1.0".$this->MimeLineBreak;
                   $headers .= "X-Priority: $priority".$this->MimeLineBreak;
                   $headers .= "Content-Type: multipart/mixed; ";
                   $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
               }
               else if ($isHTMLMessage)         # HTML message w/o attachments
               {
                   $headers .= "MIME-Version: 1.0".$this->MimeLineBreak;
                   $headers .= "X-Priority: $priority".$this->MimeLineBreak;
                   $headers .= "Content-Type: multipart/alternative; ";
                   $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
               }

               $headers .= "From: $from".$this->MimeLineBreak;
               if ($cc != "")
                   $headers .= "cc: $cc".$this->MimeLineBreak; // CC to
               if ($bcc != "")
                   $headers .= "bcc: $bcc".$this->MimeLineBreak; // BCCs to, separete multiple with commas mail@mail.com, mail2@mail.com
               if ($reply_address != "")
               {
                   $headers .= "Reply-To: $reply_address".$this->MimeLineBreak;
               }

               if ($isMulti || $isHTMLMessage)
               {
                   $mime = "This is a multi-part message in MIME format.".$this->MimeLineBreak;
                   $mime .= $this->MimeLineBreak;
                   $mime .= "--" . $mime_boundary . $this->MimeLineBreak;
               }
               if (!$isMulti)  # No attachments
               {
                    if ($isHTMLMessage)
                    {
                        $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
                        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
                        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
                        $mime .= $this->MimeLineBreak;
                        $text_message = Remove_HTML_Tags($message);
                        #$encoded_text_message = QuotedPrintableEncode($message);
                        $encoded_text_message = chunk_split(base64_encode($text_message));
                        $mime .= $encoded_text_message. $this->MimeLineBreak.$this->MimeLineBreak;
                        $mime .= "--". $mime_boundary . $this->MimeLineBreak;
                        # HTML part
                        $mime .= "Content-Type: text/html; charset=\"$charset\"".$this->MimeLineBreak;
                        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
                        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
                        $mime .= $this->MimeLineBreak;
                        #$mime .= "<HTML><BODY>\n".QuotedPrintableEncode($message)."</BODY></HTML>\n";
                        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
                        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
                        $mime .= "--" . $mime_boundary . "--".$this->MimeLineBreak;
                        $mime .= $this->MimeLineBreak;
                    }
                    else
                    {
                        $mime .= $message;
                    }
               }
               else
               {
                   if ($isHTMLMessage)
                   {
                       $mime .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary2\"".$this->MimeLineBreak.$this->MimeLineBreak;
                       $mime .= "--" . $mime_boundary2 . $this->MimeLineBreak;
                       $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
                       #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
                       $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
                       $mime .= $this->MimeLineBreak;
                       #$mime .= QuotedPrintableEncode(Remove_HTML_Tags($message)). $this->MimeLineBreak.$this->MimeLineBreak;
                       $mime .= chunk_split(base64_encode(Remove_HTML_Tags($message))). $this->MimeLineBreak.$this->MimeLineBreak;
                       $mime .= "--". $mime_boundary2 . $this->MimeLineBreak;
                       # HTML part
                       $mime .= "Content-Type: text/html; charset=\"$charset\"".$this->MimeLineBreak;
                       #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
                       $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
                       $mime .= $this->MimeLineBreak;
                       #$mime .= QuotedPrintableEncode($message);
                       $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
                       $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
                       $mime .= "--" . $mime_boundary2. "--".$this->MimeLineBreak;
                       $mime .= $this->MimeLineBreak;
                       $mime .= "--" . $mime_boundary.$this->MimeLineBreak;

                   }
                   else
                   {
                       # Message Body
                       $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
                       $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
                       $mime .= $this->MimeLineBreak;

                       $mime .= $message;
                       $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
                       $mime .= "--" . $mime_boundary. $this->MimeLineBreak;
                   }
                                $filename_encoding = "=?".strtoupper($charset)."?B?";

                   # Embed attachment files
                   for ($i=0; $i<sizeof($files); $i++)
                   {
                        $target = $files[$i];
                        $data = Get_File_Content("$attachment_path/$target");
                        $fname = addslashes($target);

                        $mime .= "Content-Type: $type;".$this->MimeLineBreak;
                        //$mime .= "\tname=\"$fname\"".$this->MimeLineBreak;
                        $mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak;
                        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
                        $mime .= "Content-Disposition: attachment;".$this->MimeLineBreak;
                        //$mime .= "\tfilename=\"$fname\"\r\n\r\n";
                        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak.$this->MimeLineBreak;
                        $mime .= chunk_split(base64_encode($data));
                        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
                        $mime .= "--" . $mime_boundary;

                        # Different if end of email
                        if ($i==sizeof($files)-1)
                            $mime .= "--".$this->MimeLineBreak;
                        else
                            $mime .= $this->MimeLineBreak;
                   }

               }

               #global $intranet_root;
               #write_file_content($mime, "$intranet_root/file/outmail.txt");
               if ($mail_return_path!="")
               {
                   $result = mail($to, $subject, $mime, $headers, "-f $mail_return_path");
               }
               else
               {
                   $result = mail($to, $subject, $mime, $headers);
               }
               return $result;
      }

      # ---------------------------------------
      # Supporting functions

      # get a random path
      function getNewAttachmentPath()
      {
               $path = session_id().".".substr( md5(uniqid(rand(),1)),10 );
               global $UserID;
               if ($UserID == "")
               {
                   return $path;
               }
               else
               {
                   global $intranet_root;
                   if (!is_dir("$intranet_root/file/mail/u$UserID"))
                   {
                        mkdir("$intranet_root/file/mail/u$UserID",0777);
                   }
                   $path = "u$UserID/$path";
                   return $path;
               }
      }

      # Return all filenames under directory $path (sub-directories will be ignored)
      # For Debian version, is_dir will not return correct value if the directory is just removed
      # therefore, need to add @ to suppress error message and return null array if error occurs
      function listfiles($path)
      {
               clearstatcache();
               if (!is_dir($path)) return array();
               $d = @dir($path);
               if (!$d) return array();
               while($entry = $d->read())
               {
                     $filepath = $path."/".$entry;
                     if (is_file($filepath))
                     {
                         $files[] = $entry;
                     }
               }
               $d->close();
               return $files;
      }
      # Modify significant HTML tag that will affect the whole document outlook
      function removeHTMLHeader($str)
      {
               $pos = strpos($str,"<body");
               if ($pos===false)
               {
                   $pos = strpos($str,"<BODY");
               }
               if ($pos!==false)
               {
                   $str = substr($str,$pos);
               }
               $epos = strpos($str,"</body>");
               if ($epos===false)
               {
                   $epos = strpos($str,"</BODY>");
               }
               if ($epos!==false)
               {
                   $str = substr($str,0,$epos+7);
               }

               $str = str_replace("<BODY","<xbody",$str);
               $str = str_replace("<body","<xbody",$str);
               $str = str_replace("</BODY>","</xbody>",$str);
               $str = str_replace("</body>","</xbody>",$str);
               $str = str_replace("<HTML","<xhtml",$str);
               $str = str_replace("<html","<xhtml",$str);
               $str = str_replace("</HTML>","</xhtml>",$str);
               $str = str_replace("</html>","</xhtml>",$str);
               $str = str_replace("<META","<xmeta",$str);
               $str = str_replace("<meta","<xmeta",$str);

               return $str;

      }

      # Convert Mail message
      function convertMessage($str)
      {
               $coded = str_replace("=\r\n","",$str);
               if ($coded == $str)
               {
                   return $str;
               }
               else
               {
                  $result = quoted_printable_decode($coded);
                  return $result;
               }
      }

      // This function recursively analyze and output the contents of the mail message.
      function parseMessageBody($imap_stream, $msg_number, $structure, $part_number, $attachment_path)
      {
               #echo "part: $part_number";
               #print_r($structure);
               // Split the $part_number. For example, 1.2.3 will be split into 1.2.(major) and 3(minor)
               if (strlen($part_number) == 0)
               {
                   $major_part_number = '';
                   $minor_part_number = 1;
               }
               elseif (strlen($part_number) == 1)
               {
                   $major_part_number = "$part_number.";        // be aware of the 'dot' at the end.
                   $minor_part_number = 0;
               }
               else
               {
                   $major_part_number = substr($part_number, 0, strlen($part_number)-1);
                   $minor_part_number = intval(substr($part_number, -1));
               }

               // Determine whether it is a leaf node. Perform recursion if not a leaf node.
               // NOTE: For "subtype = alternative", it is most likely generated by email clients
               // such as outlook express for sending rich text message and plain text message
               // at the same time. So only in this case no recursion is nessary.
               // NOTE: For "type = message",
               $parts = isset($structure->parts)?$structure->parts:'';

               if (is_array($parts) && ($structure->ifsubtype) && strtolower($structure->subtype) != 'alternative')
               {
                   foreach ($parts as $part)
                   {
                            $body = $this->parseMessageBody($imap_stream, $msg_number, $part, "$major_part_number".$minor_part_number,$attachment_path);
                            $minor_part_number++;

                            if ($part->encoding==0 && (strtolower($part->disposition)=="attachment" || $part->ifdisposition==1))
                            {
                                $part->encoding = 4;
                            }

                            $body = $this->decode_part($body, $part->encoding);
                            $message_body .= $this->manage_part($body,$part,$attachment_path);
                            if ($part->id != "")
                            {
                                $temp = imap_mime_header_decode($part->parameters[0]->value);
                                $t_filename = $temp[0]->text;
                                $temp_str = substr($part->id,1,strlen($part->id)-2);
                                $this->embed_part_ids[] = array($temp_str,$t_filename);
                            }
                   }
                   return $message_body;
               }


               // See whether it is an 'alternative' message. Output the content and return if so.
               #
               if (($structure->ifsubtype) && strtolower($structure->subtype) == 'alternative' && is_array($structure->parts))
               {
                    // Load user preferences
                    $message_format = $this->message_format;
                    if ($message_format != 'html') $message_format = 'plain';

                    // Read the message according to the preferences of the user
                    foreach ($structure->parts as $part)
                    {
                             if (strtolower($part->subtype) == $message_format)
                             {
                                 $body = $this->readMessageBody($imap_stream, $msg_number, $major_part_number.$minor_part_number);
                                 if ($part->encoding==0 && (strtolower($part->disposition)=="attachment" || $part->ifdisposition==1))
                                 {
                                     $part->encoding = 4;
                                 }
                                 $body = $this->decode_part($body, $part->encoding, $part->subtype);
                                 $message_body .= $this->manage_part($body,$part,$attachment_path);
                             }
                             $minor_part_number++;
                    }

                    return $message_body;
               }
               else if (($structure->ifsubtype) && (strtolower($structure->subtype) == 'alternative'))
               {
                    // Load user preferences
                    $message_format = $this->message_format;
                    if ($message_format != 'html') $message_format = 'plain';
                    if (true || strtolower($structure->subtype) == $message_format)
                    {
                        #$body = $this->readMessageBody($imap_stream, $msg_number,$structure, $major_part_number.$minor_part_number, $attachment_path);
                        $body = $this->readMessageBody($imap_stream, $msg_number, $part_number);
                        if ($structure->encoding==0 && (strtolower($structure->disposition)=="attachment" || $structure->ifdisposition==1))
                        {
                            $structure->encoding = 4;
                        }
                        $body = $this->decode_part($body, $structure->encoding,$structure->subtype);
                        $message_body .= $this->manage_part($body,$structure,$attachment_path);
                        $message_body = $this->handle_nested_message($message_body);
                    }
                    $minor_part_number++;
                    return $message_body;
               }
               else
               {
                   if ( strtolower($structure->subtype)=="related")
                   {
                   }
               }


               // Something other than those mentioned above at the leaf node.
               $body = $this->readMessageBody($imap_stream, $msg_number, $part_number);
               #$body = $this->handle_mixed_message($body);

               # Perform encoding
               $body = $this->decode_part($body, $structure->encoding, $structure->subtype);
               $message_body .= $this->manage_part($body,$structure,$attachment_path);
               return $message_body;
      }

      // This function opens the message. This function is a helper of parseMessageBody().
      function readMessageBody ($imap_stream, $msg_number, $part_number)
      {
               // If $part_number is empty, it assume the message does not have multiple parts
               // and just throw them to screen.
               echo "Read body $part_number<br>\n";
               if (empty($part_number) || $part_number === "1.0")
               {
                   $temp_msg  =imap_body($imap_stream, $msg_number, FT_UID);
                   echo $temp_msg;
                   return $temp_msg;
               }
               else
               {
                   $temp_msg = imap_fetchbody($imap_stream, $msg_number, $part_number, FT_UID);
                   echo $temp_msg;
                   return $temp_msg;
               }
      }

      # Decode message part
      function decode_part($body, $encoding, $subtype="")
      {
               if ($encoding=="")
               {
                   $lower_subtype = trim(strtolower($subtype));
                   if ($lower_subtype=='plain' || $lower_subtype=='html' || $lower_subtype=='')
                   {
                       return $body;
                   }
                   else
                   {
                       return quoted_printable_decode($body);
                   }
               }
               switch ($encoding)
               {
                       case 0:
                            #$body = $this->convertMessage($body);
                            break;
                       case 1:
                            break;
                       case 2:
                       case 3:
                            $body = base64_decode ($body); break;
                       case 4:
                            $body = quoted_printable_decode ($body); break;
                       default:
                            $body = quoted_printable_decode ($body); break;
               }

               return $body;
      }

      # Copy the content to file if this part is attachment
      function manage_part($body, $structure, $attachment_path)
      {
               if ($body == "") {
               return ""; }
               #print_r($structure);
               if (strtolower($structure->disposition)=="attachment" || $structure->ifdisposition==1 || $structure->ifid==1 || ($structure->type!=0 && $structure->type!=1 &&$structure->type!=2))
               {
                   $file_content = $body;

                   if (!is_dir($attachment_path))
                   {
                        mkdir($attachment_path,0777);
                   }
                   # Get Filename

                   if ($structure->ifdparameters==1)
                   {
                       $temp = imap_mime_header_decode($structure->dparameters[0]->value);
                   }
                   else if ($structure->ifparameters==1)
                   {
                       $temp = imap_mime_header_decode($structure->parameters[0]->value);
                   }
                   $filename = $temp[0]->text;
                   if ($filename == "")
                   {
                       $filename = "unknown";
                   }
                   $target_file = $attachment_path."/".$filename; #$structure->parameters[0]->value;
                   write_file_content($file_content,$target_file);
                   return "";                       # Empty String (not affect message body)
               }
               else
               {
                   return $body;
               }
      }
      function handle_nested_message($msg)
      {

               $is_quoted_printable = false;
               $msg = trim($msg);
               if (strpos($msg,"Content-Transfer-Encoding: quoted-printable\r\n")!==false)
               {
                   $is_quoted_printable = true;
               }
               else
               {
               }

               $firstnl = strpos($msg,"\r\n");
               if ($firstnl === false) return $msg;
               $bound_str = trim(substr($msg,0,$firstnl));

               if ($bound_str == "") return $msg;
               if (strlen($bound_str)<=70)    # RFC 1521 standard
               {
                   $left_str = substr($msg,$firstnl+2);
                   $alt_bpos = strpos($left_str,$bound_str);     # begin of alternative part boundary
                   $alt_bendpos = $alt_bpos + strlen($bound_str);  # end of alternative part boundary
                   #$temp = substr($left_str, $alt_bpos, strlen($bound_str));

                   if ($alt_bpos===false || ($left_str[$alt_bendpos]!="\r" && $left_str[$alt_bendpos]!="\n" ) )
                   {
                       return $msg;
                   }
                   else
                   {
                       $left_str2 = substr($left_str,$alt_bendpos);
                       $pos = strpos($left_str2,"\r\n\r\n");
                       if ($pos === false) return $msg;
                       $left_str3 = trim(substr($left_str2,$pos));
                       $end_boundary = substr($left_str3,0-strlen($bound_str)-2);
                       if ($end_boundary == "$bound_str--")
                       {
                           $left_str3 = trim(substr($left_str3,0,strlen($left_str3)-strlen($bound_str)-2));
                       }
                       if ($is_quoted_printable)
                       {
                           $pos_3d = strpos($left_str3,"=3D");
                           if ($pos_3d === false)
                           {
                           }
                           else
                           {
                               $left_str3 = quoted_printable_decode($left_str3);
                           }
                       }

                       return $left_str3;
                   }
               }
               else
               {
                   return $msg;
               }
      }

      # Supporting function
      function isHTMLContent($content)
      {
               $filtered_content = Remove_HTML_Tags($content);
               return ($filtered_content != $content);
      }


      ##########################################################################################
      # New Mail retrieving function
      # Param: Quota : quota in Mbytes, num : number of emails to be retrieved
      function checkMail2($quota, $num=0)
      {
               $mailCount = $this->checkMailCount();
               if ($mailCount==0) return;

               $used_quota = 0;                     # Calculate in Kbytes
               $overview = imap_fetch_overview($this->inbox, "1:".$mailCount);
               if (!is_array($overview)) return;
               if ($num==0)
               {
                   $num = sizeof($overview);
               }
               unset($result_mail);
               for ($i=0; $i < sizeof($overview) && $i < $num; $i++)
               {
                    $mail_msgno = $overview[$i]->msgno;
                    /* Fix error of retriving multiple address in To: */
                    $text_header = imap_fetchheader($this->inbox, $mail_msgno);
                    $raw_header = imap_rfc822_parse_headers($text_header);
                    /* */

                    $header = imap_header($this->inbox, $mail_msgno);

                    $from = $header->fromaddress;
                    #$to = $header->toaddress;
                    # Special function
                    $to = $raw_header->toaddress;
                    $subject = $header->Subject;


                    $elements = imap_mime_header_decode($from);
                    unset($subtmp);
                    for($k=0;$k<sizeof($elements);$k++) {
                        $subtmp .= $elements[$k]->text;
                    }
                    $fromname = $subtmp; # From Address to return

                    $elements=imap_mime_header_decode($subject);
                    unset($subtmp);
                    for($k=0;$k<count($elements);$k++) {
                        $subtmp .= $elements[$k]->text;
                    }
                    $subject = $subtmp; # Subject to return

                    $elements=imap_mime_header_decode($to);
                    unset($subtmp);
                    for($k=0;$k<count($elements);$k++) {
                        $subtmp .= $elements[$k]->text;
                    }
                    $showto = $subtmp;       # To address to return
                    #var_dump($header->toaddress);
                    #var_dump($showto);

                    $elements=imap_mime_header_decode($header->ccaddress);
                    unset($subtmp);
                    for($k=0;$k<count($elements);$k++) {
                        $subtmp .= $elements[$k]->text;
                    }
                    $showcc = $subtmp;       # CC address to return

                    /*
                    $this_offset = $tzoffset * 3600;
                    $date = @Date("r", $header->udate+$this_offset);      # Date of email
                    */
                    $dateReceive = date('Y-m-d H:i:s',strtotime($header->date));

                    $structure = imap_fetchstructure($this->inbox, $mail_msgno);
                    unset($this->parsed_part);
                    $this->parse_message($structure);
                    $attachments = $this->grab_attach($this->parsed_part);

                    # Handle attachments
                    unset($attach_parts);
                    unset($embed_cids);
                    $total_attach_size = 0;
                    for ($k=0; $k<sizeof($attachments); $k++)
                    {
                         $pid = $attachments[$k]["partnum"];
                         $attach_filename = $attachments[$k]["filename"];
                         $attach_type = $attachments[$k]["type"];
                         $attach_size = $attachments[$k]["size"];
                         $attach_encoding = $attachments[$k]["encoding"];
                         $attach_cid = $attachments[$k]["cid"];

                         $elements=imap_mime_header_decode($attach_filename);

                         unset($subtmp);
                         for($f_i=0;$f_i<count($elements);$f_i++) {
                                 $subtmp .= $elements[$f_i]->text;
                         }
                         $attach_filename = $subtmp;


                         $attach_content = $this->retrieve_attachment_content($this->inbox, $mail_msgno, $pid);
                         $decoded_content = $this->mime_decode_msg($attach_content,$this->mime_encoding[$attach_encoding]);

                         unset($attach_entry);
                         $attach_entry['filename'] = $attach_filename;
                         $attach_entry['type'] = $attach_type;
                         $attach_entry['size'] = $attach_size;
                         $attach_entry['cid'] = $attach_cid;
                         $attach_entry['content'] = $decoded_content;
                         $attach_parts[] = $attach_entry;
                         $embed_cids[] = array($attach_entry['cid'],$attach_entry['filename']);
                         $total_attach_size += $attach_size;
                    }

                    # Check Quota enough
                    //$size = number_format($total_attach_size/1024,1);
                    $size = round($total_attach_size/1024,1);
                    $used_quota += $size;
                    /*
                    if ($quota != 0 && $quota < $used_quota)
                    {
                        return $result_mail;
                    }
                                        */
                                        # updated on 2007-08-27
                    if ($quota < $used_quota)
                    {
                        return $result_mail;
                    }

                    # Retrieve Message Content
                    $message = $this->retrieve_message($this->inbox, $mail_msgno, $this->parsed_part);
                    # $this->message_charset, $this->is_html_message
                    $result_mail[] = array($mail_msgno, $subject,$message,$total_attach_size,
                                           $fromname, $showto, $showcc, $dateReceive, $attach_parts,
                                           $embed_cids, $this->message_charset, $this->is_html_message);

               }
               return $result_mail;

      }
      function retrieve_message($inbox, $uid, $partArr)
      {
               $this->is_html_message = -1;
               $viewpart = 0;
               $totalviewed = 0;
               $result_message = "";
               while ($partArr[$viewpart] != "")
               {
                      $p = $partArr[$viewpart];

                      #if ((($p["type"] == 0 && strtolower($p["subtype"]) == ("plain" || "html")) || (strtolower($p["disposition"]) == "inline")) && strtolower($p["disposition"])!='attachment')
                      if ((($p["type"] == 0 && strtolower($p["subtype"]) == ("plain" || "html")) ) && strtolower($p["disposition"])!='attachment')
                      {
                           $htmltransform = false;
                           $this->is_html_message = -1;
                           $this->message_charset = $p["charset"];

                           if ($p["type"] == 0 && strtolower($p["subtype"]) == "plain")
                           {
                               $transform = true;
                           }
                           if (strtolower($p["subtype"]) == "html")
                           {
                               $htmltransform = true;
                               $this->is_html_message = 1;
                           }

                           /*Fix for Inaccurate Part number finding*/
                           $tmp = $p["partnum"];
                           while (strlen($new = str_replace("..",".",$tmp)) < strlen($tmp))
                                $tmp = $new;

                           $myBod = imap_fetchbody($inbox, $uid, (string)($tmp));

                           $myBod = $this->mime_decode_msg($myBod, $this->mime_encoding[$partArr[$viewpart]["encoding"]]);
                           #echo "<br>================<br>$uid:<br>";
                           #echo "encoding: ".$partArr[$viewpart]["encoding"];
                           #print_r($partArr);
                           #echo $myBod;



                           if ($transform)
                           {
                               $htmltransform = true;
                               if (!$htmltransform)
                               {
                                    if ($fwd == false)
                                    {
                                        $myBod = "<code class='dispmsg'>".$myBod."</code>";
                                        $myBod = ereg_replace("\n","<br>",$myBod);
                                    }
                               }
                               $myBod = preg_replace("/([^\w\/])(www\.[a-z0-9\-]+\.[a-z0-9\-]+)/i","$1http://$2",    $myBod);
                               $myBod = preg_replace("/([\w]+:\/\/[\w-?%:&;#~=\.\/\@]+[\w\/])/i","<A TARGET=\"_blank\" HREF=\"$1\">$1</A>", $myBod);

                               //Old Pattern Finder
                               //$myBod = preg_replace("/([\w-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3}))/i","<A    HREF=\"compose.php?to=$1\">$1</A>",$myBod);
                               $myBod= eregi_replace('(<)?([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})(>)?','\\1<a href="compose.php?to=\\2">\\2</a>\\4', $myBod);

                           }

                           if ($htmltransform)
                           {
                               /*** GET RID OF EXCESS ITEMS IN HTML EMAILS ***/
                               #$myBod = preg_replace ("/(<\/?)(\w+)([^>]*>)/e", "'\\1'.strtoupper('\\2').'\\3'", $myBod);
                               $myBod = str_replace ("<HTML>", "", $myBod);
                               $myBod = str_replace ("</HTML>", "", $myBod);
                               $myBod = preg_replace ("'<HEAD[^>]*?>.*?</HEAD>'si", "", $myBod);
                               $myBod = preg_replace ("'<SCRIPT[^>]*?>.*?</SCRIPT>'si", "", $myBod);
                               $myBod = preg_replace ("'<STYLE[^>]*?>.*?</STYLE>'si", "", $myBod);
                               $myBod = preg_replace("'<BODY([^>]*?>)'si", "<DIV$1", $myBod);
                               $myBod = str_replace ("</BODY>", "</DIV>", $myBod);
                               #$myBod = preg_replace ('/cid:(.*?)("|;|\s|\\\|\')/e', "'view_img.php?mailbox=$mailbox&id=$id&cid='.urlencode('\\1').'\\2'", $myBod);
                               #echo "<!-- checkbody\n";
                               #echo $myBod;
                               #echo "--->";
                               $myBod = preg_replace ('/background=(.*?)(\'|"|;|\s|\\\)/e', "'style=\"background-image: url('.'\\1'.');\"'", $myBod);
                               $myBod= str_replace("mailto:", "compose.php?targetemail=",$myBod);
                               #$myBod = stripslashes($myBod);
                               $myBod = str_replace("\\\"","\"",$myBod);
                           }

                           /* View Attachment part
                           if ($totalviewed > 0 && $fwd == false)
                           {
                                echo "<hr><br>\n";
                                echo "<table width=100% cellpadding=3 cellspacing=0 border=0><tr bgcolor='#cccccc'><td>";
                                //echo "<a href='view_img?mailbox=".$mailbox."&id=".$id."&pid=".$p["partnum"]."' target='_blank'><b>View Part: ".$tmp."</b></a></td>";
                                echo "<b>Viewing Part: ".$tmp."</b></td>";
                                echo "<td align=right><small>Content-Type: <b>".strtolower($type[$p["type"]]."/".$p["subtype"])."</b></small></td></tr></table>";

                           }
                        if ($totalviewed > 0 && $fwd == 'true')
                        {
                                echo "\n";
                                return;
                        }
                        echo $myBod;
                        if ($fwd == 'true')
                                echo "\n";
                        else echo "<br>";
                        */
                        $result_message .= $myBod;

                        $transform = false;
                        $totalviewed++;
                      }

                      $viewpart++;
               }
               return $result_message;


      }

      //to retrieve the plain text message from the mail, if any
      //can't find any reusable material at the moment...

      //encoding issues not handled yet
          function retrieve_textmessage($inbox, $mail_msgno, $structure)
      {
                      $returnStr = "";

                      if (strtolower($structure->subtype) == "plain") //text only email
                      {
                              $returnStr = imap_body($inbox, $mail_msgno);
                      }
                      else
                      {
                                $parts = $structure->parts;

                                for($i = 0; $i < count($parts); ++$i)
                                {
                                        if (strtolower($parts[$i]->subtype) == "plain" || strtolower($parts[$i]->subtype) == "text") //anymore criteria to find the text message?
                                        {
                                                $returnStr = $this->mime_decode_msg(imap_fetchbody($inbox, $mail_msgno, ($i+1)),$this->mime_encoding[$parts[$i]->encoding]);
                                        }
                                }
                        }

                        return $returnStr;
      }

      function mime_decode_msg($msg, $encoding)
      {
               //Decode Msg Body if needed....
               if ($encoding == "base64")
                   $msg = base64_decode($msg);
               else if ($encoding == "quoted-printable")
                    $msg = quoted_printable_decode($msg);
               else if ($encoding == "8bit")
                    $msg = quoted_printable_decode(imap_8bit($msg));
               else { }

               return $msg;
      }
      function retrieve_attachment_content($inbox, $uid, $pid)
      {
               /*Fix for Inaccurate Part number finding*/
               while (strlen($new = str_replace("..",".",$pid)) < strlen($pid))
                      $pid = $new;
               $attachment = imap_fetchbody($inbox, $uid, $pid);
               return $attachment;
      }
      function parse_message($obj,$prefix="")
      {
               if (strtolower($obj->subtype) == "alternative")
               {
                   $objArr = $this->grab_alt($obj, $onlyText);
                   $obj = $objArr['part'];

                   if ($prefix =="")
                       $prefix = substr($objArr['pid'], 1);
                   else
                       $prefix = $prefix.$objArr['pid'];
               }
               else
               {
                   if (sizeof($obj->parts) > 0)
                       foreach ($obj->parts as $count=>$p)
                           $this->parse_part($p, $prefix.($count+1));
               }

               if ($prefix[strlen($prefix)-1] == ".")
                   $prefix = substr($prefix, 0, strlen($prefix)-1);
               if ($prefix[0] == ".")
                   $prefix = substr($prefix, 1);

               if (sizeof($obj->parts) == 0 && is_integer($obj->type))
                   $this->create_partArray($obj, $prefix);
      }
      function grab_alt($obj, $text=false)
      {
               for ($i = sizeof($obj->parts) - 1; $i > -1 ; $i--)
               {
                    if (strtolower($obj->parts[$i]->subtype) == "html" && $text == false)
                    {
                        $ret['pid'] = "." . ($i+1);
                        $ret['part'] = $obj->parts[$i];
                        return $ret;
                    }
                    if (strtolower($obj->parts[$i]->subtype) == "plain")
                    {
                        $ret['pid'] = "." . ($i+1);
                        $ret['part'] = $obj->parts[$i];
                        return $ret;
                    }
               }
      }
      function parse_part($obj, $partno)
      {
               if ($obj->subtype == "ALTERNATIVE")
               {
                   $objArr = $this->grab_alt($obj);
                   $obj = $objArr['part'];
                   $partno = $partno.$objArr['pid'];
               }
               else
               {
                   if ($obj->type == TYPEMESSAGE)
                       $this->parse_message($obj->parts[0], $partno.".");
                   else if (sizeof($obj->parts) > 0)
                        foreach ($obj->parts as $count=>$p)
                            $this->parse_part($p, $partno.".".($count+1));
               }
               if ($partno[strlen($partno)-1] == ".")
                   $partno = substr($partno, 0, strlen($prefix)-1);
               if ($partno[0] == ".")
                   $partno = substr($partno, 1);

               if (sizeof($obj->parts) == 0 && is_integer($obj->type))
                   $this->create_partArray($obj, $partno);


      }

      function create_partArray($obj, $partno)
      {

               $k = sizeof($this->parsed_part);
               if ($partno == "")
                   $partno = 1;

               $this->parsed_part[$k]["disposition"] = $obj->disposition;
               $this->parsed_part[$k]["partnum"] = $partno;
               $this->parsed_part[$k]["description"] = $obj->description;
               $this->parsed_part[$k]["type"] = $obj->type;
               $this->parsed_part[$k]["subtype"] = $obj->subtype;
               $this->parsed_part[$k]["encoding"] = $obj->encoding;
               $this->parsed_part[$k]["size"] = $obj->bytes;

               if ($obj->ifid == 1)
               {
                   $tmp = str_replace ("<", "", $obj->id);
                   $this->parsed_part[$k]["cid"] = str_replace (">", "", $tmp);
               }

               if ($obj->ifdparameters == 1)
               {
                   $params = $obj->dparameters;
                   foreach ($params as $p)
                   {
                        if(strtolower($p->attribute) == "filename")
                        {
                                $this->parsed_part[$k]["filename"] = $p->value;
                                break;
                        }
                   }
               }
               else if ($obj->ifparameters == 1 && $obj->ifdparameters == 0)
               {
                    $params = $obj->parameters;
                    foreach ($params as $p)
                    {
                        if(strtolower($p->attribute) == "name")
                        {
                                $this->parsed_part[$k]["filename"] = $p->value;
                        }
                        if(strtolower($p->attribute) == "charset")
                        {
                                $this->parsed_part[$k]["charset"] = $p->value;
                        }

                    }
               }
               else {}
      }

      function grab_attach($partArr)
      {
               $k = 0;
               for ($i=0; $i < sizeof($partArr); $i++)
               {
                    $p = $partArr[$i];
                    if (strtolower($p["disposition"]) == "attachment" || $p["type"] >= 3)
                    {
                        if ($p["filename"] != "")
                        {
                                $retArr[$k] = $p;
                                $k++;
                        }
                    }

               }

               return $retArr;

      }


        /*
        * Get MODULE_OBJ array
        */
        function GET_MODULE_OBJ_ARR()
        {

                global $PATH_WRT_ROOT, $i_Discipline_System, $CurrentPage, $iDiscipline, $LAYOUT_SKIN, $i_CampusMail_New_iMail;
                global $image_path;

                global $i_CampusMail_New_CheckMail, $i_CampusMail_New_Inbox, $i_CampusMail_New_AddressBook;
                global $i_CampusMail_Internal_Recipient_Group, $i_CampusMail_External_Recipient, $i_CampusMail_External_Recipient_Group;
                global $i_CampusMail_New_Outbox, $i_CampusMail_New_Draft, $i_CampusMail_New_Trash;
                global $i_CampusMail_New_Settings, $i_CampusMail_New_FolderManager, $i_CampusMail_New_ComposeMail;
                global $i_CampusMail_New_Mail_Search, $i_CampusMail_New_Settings_PersonalSetting;
                global $i_CampusMail_New_Settings_Signature;
                global $unreadInboxNo, $folders, $FolderID, $UserID, $i_CampusMail_New_Settings_EmailForwarding;
                global $special_option,  $iNewCampusMail;

                $anc = $special_option['no_anchor'] ?"":"#anc";

                //Check has webmail or not
                $lwebmail = new libwebmail();
                if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
                {
                    include_once("../../includes/libsystemaccess.php");
                    $lsysaccess = new libsystemaccess($UserID);
                    if ($lsysaccess->hasMailRight())
                    {
                        $noWebmail = false;
                    }
                    else
                    {
                        $noWebmail = true;
                    }
                }
                else
                {
                    $noWebmail = true;
                }

                # Current Page Information init
                $PageComposeMail = 0;

                $PageCheckMail = 0;
                $PageCheckMail_Inbox = 0;
                $PageCheckMail_Outbox = 0;
                $PageCheckMail_Draft = 0;
                $PageCheckMail_Trash = 0;
                $PageCheckMail_FolderManager = 0;

                $PageAddressBook = 0;
                $PageAddressBook_InternalReceipientGroup = 0;
                $PageAddressBook_ExternalReceipientGroup = 0;
                $PageAddressBook_ExternalReceipient = 0;

                $PageSearch = 0;

                $PageSettings = 0;
                $PageSettings_PersonalSetting = 0;
                $PageSettings_Signature = 0;

                switch ($CurrentPage) {
                        case "PageComposeMail":
                                $PageComposeMail = 1;
                                break;

                        case "PageCheckMail_Inbox":
                                $PageCheckMail = 1;
                                $PageCheckMail_Inbox = 1;
                                break;
                        case "PageCheckMail_Outbox":
                                $PageCheckMail = 1;
                                $PageCheckMail_Outbox = 1;
                                break;
                        case "PageCheckMail_Draft":
                                $PageCheckMail = 1;
                                $PageCheckMail_Draft = 1;
                                break;
                        case "PageCheckMail_Trash":
                                $PageCheckMail = 1;
                                $PageCheckMail_Trash = 1;
                                break;
                        case "PageCheckMail_FolderManager":
                                $PageCheckMail = 1;
                                $PageCheckMail_FolderManager = 1;
                                break;

                        case "PageAddressBook_InternalReceipientGroup":
                                $PageAddressBook = 1;
                                $PageAddressBook_InternalReceipientGroup = 1;
                                break;

                        case "PageAddressBook_ExternalReceipientGroup":
                                $PageAddressBook = 1;
                                $PageAddressBook_ExternalReceipientGroup = 1;
                                break;

                        case "PageAddressBook_ExternalReceipient":
                                $PageAddressBook = 1;
                                $PageAddressBook_ExternalReceipient = 1;
                                break;

                        case "PageSearch":
                                $PageSearch = 1;
                                break;

                        case "PageSettings_PersonalSetting":
                                $PageSettings = 1;
                                $PageSettings_PersonalSetting = 1;
                                break;
                        case "PageSettings_Signature":
                                $PageSettings = 1;
                                $PageSettings_Signature = 1;
                                break;
                        case "PageSettings_EmailForwarding":
                                $PageSettings = 1;
                                $PageSettings_EmailForwarding = 1;
                                break;
                }

                /*
                structure:
                $MenuArr["Section"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)");
                $MenuArr["Section"]["Child"]["name"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)", "haveSubMenu");
                */

                /*

                */

                ### No of new email (to be set)
                $unreadInboxNo = $iNewCampusMail;
                if ($unreadInboxNo != "")
                {
                        $NoOfNewMail = $unreadInboxNo;
                } else {
                        $NoOfNewMail = 0;
                }
                if ($NoOfNewMail > 0) $NewMailText = " (".$NoOfNewMail.")";

                # Menu information
                if (auth_sendmail())
                                {
                        $MenuArr["ComposeMail"] = array($i_CampusMail_New_ComposeMail, $PATH_WRT_ROOT."home/imail/compose.php", $PageComposeMail, "$image_path/2007a/iMail/icon_compose.gif");
                    }

                $MenuArr["CheckMail"] = array($i_CampusMail_New_CheckMail, $PATH_WRT_ROOT."home/imail/checkmail.php", $PageCheckMail, "$image_path/2007a/iMail/icon_check_mail.gif");
                $MenuArr["CheckMail"]["Child"]["Inbox"] = array($i_CampusMail_New_Inbox.$NewMailText, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=2", $PageCheckMail_Inbox, "$image_path/2007a/iMail/icon_inbox.gif");
                $MenuArr["CheckMail"]["Child"]["Outbox"] = array($i_CampusMail_New_Outbox, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=0", $PageCheckMail_Outbox, "$image_path/2007a/iMail/icon_outbox.gif");
                $MenuArr["CheckMail"]["Child"]["Draft"] = array($i_CampusMail_New_Draft, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=1", $PageCheckMail_Draft, "$image_path/2007a/iMail/icon_draft.gif");
                $MenuArr["CheckMail"]["Child"]["Trash"] = array($i_CampusMail_New_Trash, $PATH_WRT_ROOT."home/imail/trash.php", $PageCheckMail_Trash, "$image_path/2007a/iMail/btn_trash.gif");
                $MenuArr["CheckMail"]["Child"]["FolderManager"] = array($i_CampusMail_New_FolderManager, $PATH_WRT_ROOT."home/imail/folder.php", $PageCheckMail_FolderManager, "$image_path/2007a/iMail/icon_folder_open.gif");

                ### Child Folder Array (to be set)
                /*
                structure:
                $ChildFolderArr[] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)");
                */

                //$ChildFolderArr[] = array($i_CampusMail_New_Draft, $PATH_WRT_ROOT."home/imail/", 0, "$image_path/2007a/iMail/icon_folder_close.gif");
                //$ChildFolderArr[] = array($i_CampusMail_New_Trash, $PATH_WRT_ROOT."home/imail/", 1, "$image_path/2007a/iMail/icon_folder_close.gif");

                if (is_array($folders) && count($folders) >0)
                {
                        for ($i=0;$i<count($folders);$i++)
                        {
                                if (($folders[$i][0] != 0) && ($folders[$i][0] != 1) && ($folders[$i][0] != 2) && ($folders[$i][0] != -1))
                                {
                                        if ($FolderID == $folders[$i][0] && $FolderID != "")
                                        {
                                                $ChildFolderArr[] = array($folders[$i][1], $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=".$folders[$i][0], 1, "$image_path/2007a/iMail/icon_folder_close.gif");
                                        } else {
                                                $ChildFolderArr[] = array($folders[$i][1], $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=".$folders[$i][0], 0, "$image_path/2007a/iMail/icon_folder_close.gif");
                                        }
                                }
                        }
                }


                $MenuArr["CheckMail"]["Child"]["FolderManager"]["child"] = $ChildFolderArr;

                $MenuArr["AddressBook"] = array($i_CampusMail_New_AddressBook, "", $PageAddressBook, "$image_path/2007a/iMail/icon_address_book.gif");
                $MenuArr["AddressBook"]["Child"]["InternalRecipientGroup"] = array($i_CampusMail_Internal_Recipient_Group, $PATH_WRT_ROOT."home/imail/groupalias.php?aliastype=0&TabID=1$anc", $PageAddressBook_InternalReceipientGroup, "$image_path/2007a/iMail/icon_address_ingroup.gif");
                if(!$noWebmail)
                {
                        $MenuArr["AddressBook"]["Child"]["ExternalRecipientGroup"] = array($i_CampusMail_External_Recipient_Group, $PATH_WRT_ROOT."home/imail/groupalias.php?aliastype=1&TabID=3$anc", $PageAddressBook_ExternalReceipientGroup, "$image_path/2007a/iMail/icon_address_exgroup.gif");
                        $MenuArr["AddressBook"]["Child"]["ExternalRecipient"] = array($i_CampusMail_External_Recipient, $PATH_WRT_ROOT."home/imail/addressbook.php", $PageAddressBook_ExternalReceipient, "$image_path/2007a/iMail/icon_address_ex.gif");
                }

                $MenuArr["Search"] = array($i_CampusMail_New_Mail_Search, $PATH_WRT_ROOT."home/imail/search.php", $PageSearch, "$image_path/2007a/iMail/btn_view_source.gif");

                $MenuArr["Settings"] = array($i_CampusMail_New_Settings, "", $PageSettings, "$image_path/2007a/iMail/icon_preference.gif");
                $MenuArr["Settings"]["Child"]["PersonalSetting"] = array($i_CampusMail_New_Settings_PersonalSetting, $PATH_WRT_ROOT."home/imail/pref.php", $PageSettings_PersonalSetting);
                $MenuArr["Settings"]["Child"]["Signature"] = array($i_CampusMail_New_Settings_Signature, $PATH_WRT_ROOT."home/imail/pref_signature.php", $PageSettings_Signature);
                if (!$noWebmail)
                {
                        $MenuArr["Settings"]["Child"]["EmailForwarding"] = array($i_CampusMail_New_Settings_EmailForwarding, $PATH_WRT_ROOT."home/imail/pref_email_forwarding.php", $PageSettings_EmailForwarding);
                }


                # module information
                $MODULE_OBJ['title'] = $i_CampusMail_New_iMail;
                $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_iMail.gif";
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/imail/index.php";
                //$MODULE_OBJ['menu'] = array();
                $MODULE_OBJ['menu'] = $MenuArr;

                return $MODULE_OBJ;
        }

        function getMailList()
        {
               $mailCount = $this->checkMailCount();
               if ($mailCount==0) return;

               $used_quota = 0;                     # Calculate in Kbytes

               $overview = imap_fetch_overview($this->inbox, "1:".$mailCount);

               if (!is_array($overview)) return;
               reset($overview);
               return $overview;

        }
        function getMailServerRef()
        {
                return "{".$this->host.":".$this->mail_server_port."}";
        }

        function getMailFolders()
        {
                $mail_string_ref = $this->getMailServerRef();
                $length = strlen($mail_string_ref);

                $folders = imap_getmailboxes($this->inbox, $mail_string_ref,"*");
                $result = array();
                $i = 0;
                while (list($key, $value) = each($folders))
                {
                        $temp = substr($value->name, $length);
                        $result[] = $this->decodeFolderName($temp);
                }
                //sort($result,SORT_STRING);
                usort($result,"String_To_Lower_Array_Cmp");
                
                return $result;
        }

        /* ------ Get Mail Folder List by level, by kenneth chung 20080423 ------ */
        function Get_Folder_List_In_Path($MailFolder="",$AllGet=false,$Recursive=false) {
                global $SYS_CONFIG;
                if ($MailFolder == "") $MailFolder = $this->encodeFolderName($this->FolderPrefix);
                else $MailFolder = $this->encodeFolderName($MailFolder);

                $mail_string_ref = $this->getMailServerRef();
                $length = strlen($mail_string_ref);
                if (!$AllGet) {
                        if (!$Recursive)
                                $folders = imap_getsubscribed($this->inbox, $mail_string_ref.$MailFolder,"%");
                        else
                                $folders = imap_getsubscribed($this->inbox, $mail_string_ref.$MailFolder,"*");
                }
                else {
                        if (!$Recursive)
                                $folders = imap_getmailboxes($this->inbox, $mail_string_ref.$MailFolder,"%");
                        else
                                $folders = imap_getmailboxes($this->inbox,$mail_string_ref.$MailFolder,"*");
                }
                $result = array();

                if ($folders !== false) {
                        for ($i=0; $i< sizeof($folders); $i++) {

                                $temp = substr($folders[$i]->name, $length);
                                if (!in_array($temp,$this->DefaultFolderList)) {
                                        $result[] = $this->decodeFolderName($temp);
                                }
                        }
                }

                //sort($result,SORT_STRING);
                usort($result,"String_To_Lower_Array_Cmp");

                return $result;
        }

        /* ----- Move to Folder, by kenneth chung 20080423 ------ */
        function Go_To_Folder($MailFolder=""){
                return imap_reopen($this->inbox, $this->getMailServerRef().$this->encodeFolderName($MailFolder));
        }

        /* Unused
        function getMailFolderList()
    {
            ### like $this->getMailFolders()
        }
        */
/* To-do: */

  /* Generate a MIME format of email, for save (Draft, sent) and send email */
  /* Param: Subject, email message, from address, to, cc, bcc, attachment path , Is important, mail return path
     Return: mime text
  */
        // Edited by kenneth chung at 20080430 to adopt new attachment structure
  function buildMimeText($subject,$message,$from,$receiver_to=array(),$receiver_cc=array(),$receiver_bcc=array(),$AttachList=array(),$IsImportant="",$DraftFlag=false, $Perference="") {
    /*  mainly follow the logic of sendmail()
            check charset, how about UTF-8 encoding?
    */

    # Content-type of attachment
    $type="application/octet-stream";

    $priority = ($IsImportant==1)? 1: 3;
   	/*echo '<pre>';
    var_dump($receiver_to);
    echo sizeof($receiver_to);
    echo '</pre>';*/
    # receiving email addresses
    for ($i=0; $i< sizeof($receiver_to); $i++) {
      if (trim($receiver_to[$i]) != "") {
      	$FinalReceiverTo[] = $receiver_to[$i];
      }
    }
    for ($i=0; $i< sizeof($receiver_cc); $i++) {
      if (trim($receiver_cc[$i]) != "") {
        $FinalReceiverCc[] = $receiver_cc[$i];
      }
    }
    for ($i=0; $i< sizeof($receiver_bcc); $i++) {
      if (trim($receiver_bcc[$i]) != "") {
        $FinalReceiverBcc[] = $receiver_bcc[$i];
      }
    }
    $numTo = sizeof($FinalReceiverTo);
    $numCC = sizeof($FinalReceiverCc);
    $numBCC = sizeof($FinalReceiverBcc);
    /*shuffle($receiver_to);
    shuffle($receiver_cc);
    shuffle($receiver_bcc);*/
    if (!$DraftFlag) {
            if ($numTo + $numCC + $numBCC == 0) return false;
                }

    $to = ($numTo != 0)? str_replace("\'","'",str_replace('\"','"',implode(", ",$FinalReceiverTo))) : "";
    $cc = ($numCC != 0)? str_replace("\'","'",str_replace('\"','"',implode(", ",$FinalReceiverCc))) : "";
    $bcc = ($numBCC != 0)? str_replace("\'","'",str_replace('\"','"',implode(", ",$FinalReceiverBcc))) : "";
    /*echo '<pre>';
    var_dump($receiver_to);
    echo '</pre>';
                echo $to; die;*/
    # Mail Header
    $headers='';
    // have peference
                if ($Perference!="") {
            $fromName = (trim($Perference[0]["DisplayName"]) == "")? substr($from,0,stripos($from,"@")):$Perference[0]["DisplayName"];
            $ReplyAddress = (trim($Perference[0]["ReplyEmail"]) != "")? $Perference[0]["ReplyEmail"]:$from;
            $HeaderFrom .= "From: \"$fromName\" <$from>".$this->MimeLineBreak;
            
            $ReturnMsg['From'] = trim('"'.$fromName.'" <'.$from.'>');
    				$ReturnMsg['SortFrom'] = str_replace('"','',$ReturnMsg['From']);
          }
          // for auto mail, e.g password reset
          else {
                  $HeaderFrom .= 'From: '.$from.$this->MimeLineBreak;
                  $ReplyAddress = $from;
          }

    for ($i=0; $i< sizeof($AttachList); $i++) {
            $files[] = $AttachList[$i][2];
            $fileRealName[] = $AttachList[$i][1];
    }

    //global $intranet_session_language;
    $charset = "UTF-8";
    //$charset = (($intranet_session_language=="gb")?"gb2312":"big5");

    #$charset = (isBig5($message)?"big5":"iso-8859-1");
    #$isHTMLMessage = $this->isHTMLContent($message);
    //global $special_feature;
    $isHTMLMessage = 1; //($special_feature['imail_richtext']);

    $isMulti = (sizeof($files)!=0);

    $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
    $mime_boundary2 = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for multi-part

    if ($isMulti)
    {
            $headers .= "MIME-Version: 1.0".$this->MimeLineBreak;
      $headers .= "X-Priority: $priority".$this->MimeLineBreak;
      $headers .= "Content-Type: multipart/mixed; ";
      $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
    }
    else if ($isHTMLMessage)         # HTML message w/o attachments
    {
            $headers .= "MIME-Version: 1.0".$this->MimeLineBreak;
      $headers .= "X-Priority: $priority".$this->MimeLineBreak;
      $headers .= "Content-Type: multipart/alternative; ";
      $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
    }
    if ($Perference == "")
                        $headers .= $HeaderFrom;

    $headers .= "Date: " . date("r") .$this->MimeLineBreak;
    # Commented by Kenneth Wong (20080612)
    #$headers .= "Return-Path: $HeaderFrom ".$this->MimeLineBreak;


    if ($ReplyAddress != "")
    {
            $headers .= "Reply-To: $ReplyAddress".$this->MimeLineBreak;
    }
                //$headers .= "Message-ID: <".time()."@inc.broadlearner.com>\r\n";
                //$headers .= "X-Mailer: PHP ".phpversion()."\r\n";
                //$headers .= "X-Sender: $from\r\n";

                //echo $headers;

                //die;

    if ($isMulti || $isHTMLMessage)
    {
            $mime = "This is a multi-part message in MIME format.".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        $mime .= "--" . $mime_boundary . $this->MimeLineBreak;
                }

                if (!$isMulti) { # No attachments
                        if ($isHTMLMessage) {
              $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        $text_message = Remove_HTML_Tags($message);
        #$encoded_text_message = QuotedPrintableEncode($message);
        $encoded_text_message = chunk_split(base64_encode($text_message));
        $mime .= $encoded_text_message. $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--". $mime_boundary . $this->MimeLineBreak;
        # HTML part
        $mime .= "Content-Type: text/html; charset=\"$charset\"".$this->MimeLineBreak;
        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        #$mime .= "<HTML><BODY>\n".QuotedPrintableEncode($message)."</BODY></HTML>\n";
        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary . "--".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
                  }
      else {
                                $mime .= $message;
      }
          }
    else {
            if ($isHTMLMessage) {
              $mime .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary2\"".$this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary2 . $this->MimeLineBreak;
        $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        #$mime .= QuotedPrintableEncode(Remove_HTML_Tags($message)). $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= chunk_split(base64_encode(Remove_HTML_Tags($message))). $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--". $mime_boundary2 . $this->MimeLineBreak;
        # HTML part
        $mime .= "Content-Type: text/html; charset=\"$charset\"".$this->MimeLineBreak;
        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        #$mime .= QuotedPrintableEncode($message);
        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary2. "--".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        $mime .= "--" . $mime_boundary. "".$this->MimeLineBreak;
                        }
      else {
              # Message Body
        $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
        $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;

        $mime .= $message;
        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary. $this->MimeLineBreak;
                        }
                   $filename_encoding = "=?".strtoupper($charset)."?B?";

      # Embed attachment files
      for ($i=0; $i<sizeof($files); $i++)
      {
              //$target = $files[$i];
        $data = Get_File_Content($files[$i]);
        $fname = addslashes($fileRealName[$i]);

        $mime .= "Content-Type: $type;".$this->MimeLineBreak;
        //$mime .= "\tname=\"$fname\"\r\n";
        $mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= "Content-Disposition: attachment;".$this->MimeLineBreak;
        //$mime .= "\tfilename=\"$fname\"\r\n\r\n";
        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= chunk_split(base64_encode($data));
        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary;

        # Different if end of email
        if ($i==sizeof($files)-1)
                $mime .= "--".$this->MimeLineBreak;
        else
                $mime .= $this->MimeLineBreak;
                        }
                }

      $ReturnMsg['to'] = str_replace('\"','"',$to);
      $ReturnMsg['cc'] = str_replace('\"','"',$cc);
      $ReturnMsg['bcc'] = str_replace('\"','"',$bcc);
      $ReturnMsg['subject'] = (trim($subject) == "")? "No Subject":$this->Get_Mail_Format($subject,"UTF-8");
      $ReturnMsg['headers'] = $headers;
      $ReturnMsg['body'] = $mime;

      // Kenneth Wong (20080612) Added element headers2 for mail()
      $ReturnMsg['headers2'] = $headers;
      if ($cc != "")
      {
          $ReturnMsg['headers2'] .= "cc: $cc".$this->MimeLineBreak;
      }
      if ($bcc != "")
      {
          $ReturnMsg['headers2'] .= "bcc: $bcc".$this->MimeLineBreak;
      }
      $ReturnMsg['headers2'] .= $HeaderFrom . $this->MimeLineBreak;

      $headers .= $HeaderFrom;
      # Commented by Kenneth Wong (20080612): return path set in mail() -f option
            #$headers .= "Return-Path: $HeaderFrom ".$this->MimeLineBreak;
            $headers .= "To: $to".$this->MimeLineBreak;
            $headers .= "cc: $cc".$this->MimeLineBreak;
            $headers .= "bcc: $bcc".$this->MimeLineBreak;
            $headers .= "Subject: ".$this->Get_Mail_Format($subject,"UTF-8").$this->MimeLineBreak;
            $ReturnMsg['FullMimeMsg'] = $headers.$mime;
    
    $ReturnMsg['SortTo'] = trim(str_replace('"','',$ReturnMsg['to']));
    $ReturnMsg['Boundary'] = $mime_boundary;

    return $ReturnMsg;
  }

        /*
          Save an email entry to SENT Folder
          Param : mail - mime text
          Return : Successful - true / false
        */
        //edited by kenneth chung at 20080502, will change later for folder structure
        function saveSentCopy($mail)
        {
                $Result["AppendFlag"] = imap_append($this->inbox, $this->getMailServerRef().$this->RootPrefix."Sent", $mail);
                $uid = $this->Get_Last_Msg_UID($this->RootPrefix."Sent");
                $Result["SetReadFlag"] = imap_setflag_full($this->inbox,$uid,'\\Seen',ST_UID);
								
								/*echo '<pre>';
								var_dump($mail);
								echo '</pre>';
								echo '<pre>';
								var_dump($Result);
								echo '</pre>';
								die;*/
                return $Result["AppendFlag"];
        }

        /*
          Save an email entry to DRAFT Folder
          Param : mail - mime text
          Return : Successful - true / false
        */
        //edited by kenneth chung at 20080506, will change later for folder structure
        function saveDraftCopy($mail)
        {
                                        return imap_append($this->inbox, $this->getMailServerRef().$this->RootPrefix."Draft", $mail);
        }

        /*
          Read the email record
          Param : uid - UID of the mail
          Return : Array()
                     - email UID
                     - email subject
                     - email content (text)
                     - email (HTML alternative)
                     - From (decoded)
                     - To (decoded)
                     - CC (decoded)
                     - Date of the email
                     - charset encoding
                     - attachment array()
                       - part number/ID String
                       - attachment filename
                       - attachment content type
                       - attachment size
                       - attachment encoding
                       - attachment cid (for embedded image)

        */
      function getMailRecord($folderName, $uid)
      {
        /* refer to $this->checkmail2() imap_bodystruct() and imap_body() */
        //main follow the flow of checkmail2()
        $mailbox = $this->inbox;
        imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));

        $overview = imap_fetch_overview($mailbox, $uid, FT_UID); //to get the msgno for the following use
        if ($overview === false) return null;

        $mail_msgno = $overview[0]->msgno;
        $text_header = imap_fetchheader($this->inbox, $mail_msgno);
        $raw_header = imap_rfc822_parse_headers($text_header);
        $header = imap_header($this->inbox, $mail_msgno);

        $from = $header->fromaddress;
        $fromDetail = $header->from;
              $to = $raw_header->toaddress;
              $toDetail = $raw_header->to;
              $cc = $raw_header->ccaddress;
              $ccDetail = $raw_header->cc;
              $bcc = $raw_header->bccaddress;
              $bccDetail = $raw_header->bcc;
        $subject = $header->Subject;


        $elements = imap_mime_header_decode(imap_utf8($from));
        unset($subtmp);
        for($k=0;$k<sizeof($elements);$k++) {
                $subtmp .= $elements[$k]->text;
              }
        $fromname = $subtmp; # From Address to return

        $elements=imap_mime_header_decode(imap_utf8($subject));
        unset($subtmp);
        for($k=0;$k<count($elements);$k++) {
                $subtmp .= $elements[$k]->text;
        }
        $subject = $subtmp; # Subject to return

        $elements=imap_mime_header_decode(imap_utf8($to));
        unset($subtmp);
        for($k=0;$k<count($elements);$k++) {
                $subtmp .= $elements[$k]->text;
        }
        $showto = $subtmp;       # To address to return

        $elements=imap_mime_header_decode(imap_utf8($cc));
        unset($subtmp);
        for($k=0;$k<count($elements);$k++) {
          $subtmp .= $elements[$k]->text;
        }
        $showcc = $subtmp;       # CC address to return

                                $elements=imap_mime_header_decode(imap_utf8($bcc));
        unset($subtmp);
        for($k=0;$k<count($elements);$k++) {
          $subtmp .= $elements[$k]->text;
        }
        $showbcc = $subtmp;       # CC address to return

        $dateReceive = date('Y-m-d H:i:s',strtotime($header->date));

        $structure = imap_fetchstructure($mailbox, $mail_msgno);

        unset($this->parsed_part);
        $this->parse_message($structure);
        $attachments = $this->grab_attach($this->parsed_part);

                # Handle attachments
        unset($attach_parts);
        unset($embed_cids);
        $total_attach_size = 0;
        for ($k=0; $k<sizeof($attachments); $k++)
        {
          $pid = $attachments[$k]["partnum"];
          $attach_filename = $attachments[$k]["filename"];
          $attach_type = $attachments[$k]["type"];
          $attach_subtype = $attachments[$k]["subtype"];
          $attach_size = $attachments[$k]["size"];
          $attach_encoding = $attachments[$k]["encoding"];
          $attach_cid = $attachments[$k]["cid"];

          $elements=imap_mime_header_decode($attach_filename);

          unset($subtmp);
          for($f_i=0;$f_i<count($elements);$f_i++) {
                  $subtmp .= $elements[$f_i]->text;
          }
          $attach_filename = $subtmp;

          $attach_content = $this->retrieve_attachment_content($mailbox, $mail_msgno, $pid);
          $decoded_content = $this->mime_decode_msg($attach_content,$this->mime_encoding[$attach_encoding]);

          unset($attach_entry);
          $attach_entry['partid'] = $pid;
          $attach_entry['filename'] = $attach_filename;
          $attach_entry['type'] = $this->mime_type[$attach_type];
          $attach_entry['subtype'] = $attach_subtype;
          $attach_entry['size'] = $attach_size;
          $attach_entry['cid'] = $attach_cid;
          $attach_entry['content'] = $decoded_content;
          $attach_parts[] = $attach_entry;
          $embed_cids[] = array($attach_entry['cid'],$attach_entry['filename']);
          $total_attach_size += $attach_size;
        }

        $message = $this->retrieve_message($mailbox, $mail_msgno, $this->parsed_part);
        $text_message = $this->retrieve_textmessage($mailbox, $mail_msgno, $structure);
        $Priority = $this->Get_Header_Flag($mail_msgno,'X-Priority');

        $returnArray = array();
        $returnArray[0]["msgno"] = $mail_msgno;
        $returnArray[0]["uid"] = $uid;
        $returnArray[0]["Priority"] = ($Priority == 1)? $Priority:0;
        $returnArray[0]["subject"] = $subject;
        $returnArray[0]["message"] = $this->Conver_Encoding($message,"UTF-8",$this->message_charset);
        $returnArray[0]["text_message"] = $this->Conver_Encoding($text_message,"UTF-8",$this->message_charset);
        $returnArray[0]["total_attach_size"] = $total_attach_size;
        $returnArray[0]["fromname"] = $fromname;
        $returnArray[0]["showto"] = $showto;
        $returnArray[0]["showcc"] = $showcc;
        $returnArray[0]["showbcc"] = $showbcc;
        $returnArray[0]["toDetail"] = $toDetail;
        $returnArray[0]["fromDetail"] = $fromDetail;
        $returnArray[0]["ccDetail"] = $ccDetail;
        $returnArray[0]["bccDetail"] = $bccDetail;
        $returnArray[0]["dateReceive"] = $dateReceive;
        $returnArray[0]["ReceiveTimeStamp"] = strtotime($header->date);
        $returnArray[0]["attach_parts"] = $attach_parts;
        $returnArray[0]["embed_cids"] = $embed_cids;
        $returnArray[0]["message_charset"] = $this->message_charset;
        $returnArray[0]["is_html_message"] = $this->is_html_message;

        return $returnArray;
                                  /*
                                          array($mail_msgno, $subject,$message, $total_attach_size,
                          $fromname, $showto, $showcc, $dateReceive, $attach_parts,
                          $embed_cids, $this->message_charset, $this->is_html_message);
                                  */
      }

        /*
          Check Folder name is valid (not INBOX, DRAFT, SENT, SPAM and special characters)
          Param: folderName
          Return: True / False
        */
        function isValidMailFolderName($folderName)
        {
                        return !(substr_count($folderName, '"') || substr_count($folderName, "\\") ||
                                substr_count($folderName, $this->folderDelimiter) || ($folderName == '') ||
                                (strtoupper($folderName) == "INBOX") || (strtoupper($folderName) == "DRAFT") ||
                                (strtoupper($folderName) == "SENT") || (strtoupper($folderName) == "SPAM"));

                        //need to put those predefined names into config?
        }

        /*
          Create Folder
          Param: folderName
          Return: True / False
        */
        function createMailFolder($parentFolder, $folderName)
        {
        	$parentFolder = ($parentFolder == "")? $this->FolderPrefix:$parentFolder;
                //$folderName - in UTF-8 format
            $newFolder = $folderName;
          if ($parentFolder != "")
          {
                    $newFolder = $parentFolder . $this->folderDelimiter . $folderName;
                }

                if (imap_createmailbox($this->inbox, $this->encodeFolderName($this->getMailServerRef().$newFolder))) {
                        if (imap_subscribe($this->inbox, $this->encodeFolderName($this->getMailServerRef().$newFolder))) {
                                $Result = true;
                        }
                }
                else {
                        $Result = false;
                }

                                        return $Result;
        }

        /*
          Remove Folder
          Param: folderName
          Return: True / False
        */
        function Delete_Mail_Folder($folderName){
          global $SYS_CONFIG;

                imap_close($this->inbox);
                $mailbox_prefix = $this->getMailServerRef();
                #$this->inbox = imap_open($mailbox_prefix.$this->encodeFolderName($folderName),$this->CurUserAddress,$this->CurUserPassword,OP_HALFOPEN);
                $this->inbox = $this->openInbox($this->CurUserAddress, $this->CurUserPassword, $folderName, true);

                $SubFolderList = $this->Get_Folder_List_In_Path($folderName,true,true);
                /*echo '<pre>';
                var_dump($SubFolderList);
                echo '</pre>';
                echo $mailbox_prefix.'<br>';*/
                for ($i=0;$i < sizeof($SubFolderList); $i++) {
                    $result[$SubFolderList[$i]."UnSubscribe"] = imap_unsubscribe($this->inbox,$mailbox_prefix.$this->encodeFolderName($SubFolderList[$i]));
                    imap_deletemailbox($this->inbox, $mailbox_prefix.$this->encodeFolderName($SubFolderList[$i]));
                }
									
								if (!in_array($folderName,$SubFolderList)) {
                	$result[$folderName."UnSubscribe"] = imap_unsubscribe($this->inbox, $mailbox_prefix.$this->encodeFolderName($folderName));
                	imap_deletemailbox($this->inbox, $mailbox_prefix.$this->encodeFolderName($folderName));
                }
								
								imap_close($this->inbox);
								
                return !in_array(false,$result);
        }

        function expungeMailFolder($folderName)
        {
                $mailbox = $this->inbox;
                        imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
                        return imap_expunge($mailbox);
        }

        function Rename_Mail_Folder($sourceFolder, $newFolder)
        {
                $TempPath = explode($this->folderDelimiter, $sourceFolder);
                $FolderPath = "";
                for ($i=0; $i< (sizeof($TempPath)-1); $i++)
                        $FolderPath .= $TempPath[$i].$this->folderDelimiter;
                $newFolder = $FolderPath.$newFolder;

                $mailbox_prefix = $this->getMailServerRef();
                if (imap_unsubscribe ($this->inbox, $this->encodeFolderName($mailbox_prefix.$sourceFolder))) {
	                $SubFolderList = $this->Get_Folder_List_In_Path($sourceFolder,true,true);
	                for($i=0; $i< sizeof($SubFolderList); $i++) {
	                  $Result[] = imap_unsubscribe($this->inbox, $this->encodeFolderName($mailbox_prefix.$SubFolderList[$i]));
	                }
	                if (imap_renamemailbox($this->inbox,$mailbox_prefix.$this->encodeFolderName($sourceFolder), $mailbox_prefix.$this->encodeFolderName($newFolder))) {
                    if (imap_subscribe($this->inbox, $this->encodeFolderName($mailbox_prefix.$newFolder))) {
                      $SubFolderList = $this->Get_Folder_List_In_Path($newFolder,true,true);
                      for($i=0; $i< sizeof($SubFolderList); $i++) {
                              $Result[] = imap_subscribe($this->inbox, $this->encodeFolderName($mailbox_prefix.$SubFolderList[$i]));
                      }
                    }
	                }
	                else {
	                	$Result['RenameFolder'] = false;
	                }
	              }

                if (!$Result) $Result = array();

                return !in_array(false,$Result);
        }

                        // create at 20080506, by kenneth chung
                        function Move_To_Trash($folderName, $uid) {
                                $mailbox = $this->inbox;
                                imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));

                                if ($folderName != $this->RootPrefix."Trash") $Result['MoveResult'] = imap_mail_move($mailbox,$uid,$this->encodeFolderName($this->RootPrefix."Trash"),CP_UID);
                                else $Result['MoveResult'] = imap_delete($mailbox,$uid,FT_UID);

                                $Result['DeleteResult'] = imap_expunge($mailbox);

                                return (!in_array(false,$Result));
                        }

                        // edited at 20080506, by kenneth chung
      function deleteMail($folderName, $uid) {
                                /* $this->removeMail() may do the same thing already */
                                /* not tested with multi level folder */
                                $mailbox = $this->inbox;
                                imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));

                                $Result['DeleteResult'] = imap_delete($mailbox,$uid,FT_UID);

                                $Result['ExpungeResult'] = imap_expunge($mailbox);

                                return (!in_array(false,$Result));
      }

                        // create by kenneth chung to undelete a mail
                        function Undelete_Mail($folderName, $uid) {
                                /* $this->removeMail() may do the same thing already */
                                /* not tested with multi level folder */
                                $mailbox = $this->inbox;
                                imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
                                return imap_clearflag_full($mailbox, $uid, '\\DELETED', ST_UID);
      }
        /*
          List the emails in the folder
          Param: folderName - listing the folder
                 startMsgNumber - retrieve email starting from startMsgNumber
                 count - how many to return
                 sort - sorting criteria
          Return: Array()
                    - Subject
                    - From
                    - To
                    - Date
                    - UID
                    - size
                    - msgno
                    - recent
                    - answered
                    - deleted
                    - seen
                    - [Pls check whether reply / forward / reply all]
        */
        function listMailInFolder($folderName, $startMsgNumber, $count, $sort="DateSort", $reverse=0, $Keyword="", $SearchField=array(), $FromDate="", $ToDate="")
        {
        	global $Lang;
        	//$time_start = microtime(true);
          /*echo $Keyword.'<br>';
          var_dump($SearchField).'<br>';*/
          /*echo $FromDate.'<br>';
          echo $ToDate.'<br>';
          echo $folderName.'<br>';*/
          /* refer to imap_fetch_overview(), imap_sort() */
          // Fetch an overview for all messages in INBOX
          //$startMsgNumber: start from 1
          //
          //e.g. listMailInFolder("INBOX", 1, 20, SORTDATE, 1);

                                        //$timestart = time();
          $mailbox = $this->inbox;
          imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
          //echo "imap reopen: " . (microtime(true) - $time_start) . "<br>\n";
          
          //echo "imap_reopen used ".$timespend =  time() - $timestart;

          //$timestart = time();
          //check the max msg number in the mailbox
          //$mailcount = imap_check($mailbox);
          //$MaxMsgNo = imap_num_msg($mailbox);
          
          //$FetchStartNumber = (($MaxMsgNo - ($startMsgNumber+$count)) < 0)? 1:($MaxMsgNo - ($startMsgNumber+$count) + 1);
	  
	  /*
          $FetchStartNumber = (($MaxMsgNo - ($startMsgNumber+$count)) < 0)? 1:($MaxMsgNo - ($startMsgNumber + $count) + 2);
          $FetchEndNumber = $MaxMsgNo - $startMsgNumber + 1;
          echo "MaxMsgNo: " . $MaxMsgNo . "<br>\n";
          echo "startMsgNumber: " . $startMsgNumber . "<br>\n";
          echo "count: " . $count . "<br>\n";
          echo "FetchStartNumber: " . $FetchStartNumber . "<br>\n";
          echo "FetchEndNumber: " . $FetchEndNumber . "<br>\n";
	  */
          
          $sortIndex = imap_sort($mailbox, SORTDATE, 1);
          //echo "imap sort: " . (microtime(true) - $time_start) . "<br>\n";
	  /*echo '<pre>';
	  var_dump($sortIndex);
	  echo '</pre>';
	  */
          //echo "imap_sort used ".$timespend =  time() - $timestart;

          //check do not exceed the max msg number, otherwise it'll return nothing
          $adjustedCount = ($startMsgNumber+$count-1 > sizeof($sortIndex)) ? sizeof($sortIndex) - $startMsgNumber + 1 : $count;

          //$sortIndex = imap_sort($mailbox, SORTDATE, $reverse);
          $msgList = array_slice($sortIndex, $startMsgNumber-1, $adjustedCount);
          for ($i=0; $i< sizeof($msgList); $i++) {
          	//$MsgFolderArray[] = array($folderName, $msgList[$i]);
          	$OverviewArray[] = $this->Get_Mail_In_Folder($folderName,$msgList[$i]);
          }
          	
          //$OverviewArray = $this->Get_Mail_Overview($MsgFolderArray);
          //echo "get mail overview: " . (microtime(true) - $time_start) . "<br>\n";
          
          /*//echo sizeof($sortIndex).'<br>';
          //echo $startMsgNumber.' '.$adjustedCount.'<br>';
          //echo sizeof($msgList).'<br>';
          //echo '<pre>'; var_dump($sortIndex); echo '</pre>';
          //echo '<pre>'; var_dump($msgList); echo '</pre>';
          //$result = imap_fetch_overview($mailbox, implode("," , $sortIndex), 0);
          //$timestart = time();
          $result = imap_fetch_overview($mailbox, implode("," , $msgList), 0);
          //$result = imap_fetch_overview($mailbox, $FetchStartNumber.":".$FetchEndNumber, 0);
          //echo $FetchStartNumber.":".$FetchEndNumber."<br>";
          //echo "imap_fetch_overview used ".$timespend =  time() - $timestart;
          //$result = imap_fetch_overview($mailbox, "1",0);
          //echo sizeof($result);

          //$timestart = time();
          if (sizeof($result) != 0) {
            for ($i=0; $i< sizeof($result); $i++) {
              $TempObj = $result[$i];
              unset($DisplayFrom);
              $FromElement = imap_mime_header_decode($TempObj->from);
              for ($j=0; $j< sizeof($FromElement); $j++) {
                                                                $DisplayFrom .= $this->Conver_Encoding($FromElement[$j]->text,"UTF-8", $FromElement[$j]->charset);
                      if ($j==0) $DisplayFrom .= " ";
              }
              $OverviewArray[$i]['From'] = $DisplayFrom;

              unset($DisplayTo);
              $ToElement = imap_mime_header_decode($TempObj->to);
              if (sizeof($ToElement) == 0) {
              	$header = imap_fetchheader($mailbox, $TempObj->msgno);
              	$RawHeader = imap_rfc822_parse_headers($header);
              	$ToElement = $RawHeader->to;
              	
              	if (sizeof($ToElement) > 0) {
              		if (!isset($ToElement[0]->personal)) $DisplayName = $ToElement[0]->mailbox."@".$ToElement[0]->host;
									else {
										$DisplayObject = imap_mime_header_decode(imap_utf8($ToElement[0]->personal));
										$DisplayName = $DisplayObject[0]->text;
									}
              		
					        $OverviewArray[$i]['To'] = $DisplayName."...";
              		//$OverviewArray[$i]['To'] = $Lang['email']['MultiReceiver'];
              	}
              	else {
              		$OverviewArray[$i]['To'] = $Lang['email']['NoReceiver'];
              	}
							}
							else {
	              for ($j=0; $j< sizeof($ToElement); $j++) {
	                $DisplayTo .= $this->Conver_Encoding($ToElement[$j]->text,"UTF-8", $ToElement[$j]->charset);
	                if ($j==0) $DisplayTo .= " ";
	              }
	              $OverviewArray[$i]['To'] = $DisplayTo;
	            }
              

              // trim the mini second for the date
              $Date = substr($TempObj->date, 0, -5);
              $OverviewArray[$i]['DateReceived'] = $Date;
              $TempDate = explode(',',$TempObj->date);
              $DayOfWeek = $TempDate[0];
              $DateTime = explode(' ',trim($TempDate[1]));
              list($Day,$Month,$Year,$Time,$Zone) = $DateTime;
              list($Hour,$Min,$Sec) = explode(':',$Time);
              $SortTime = mktime($Hour,$Min,$Sec,Get_Month_Number($Month),$Day,$Year);
              $OverviewArray[$i]['DateSort'] = $SortTime;

              // Get Priority
              $MailPriority = $this->Get_Header_Flag($TempObj->msgno,"x-priority");
              $OverviewArray[$i]['Priority'] = $MailPriority;

              // replace NO SUBJECT if subject is empty
              $SubjectObj = imap_mime_header_decode(imap_utf8($TempObj->subject));
              unset($subtmp);
              for($k=0;$k<count($SubjectObj);$k++) {
                                                                $subtmp .= $SubjectObj[$k]->text;
                                                        }
              $Subject = ($subtmp == "")? "NO SUBJECT":$subtmp;
              $OverviewArray[$i]['Subject'] = $Subject;

              // check if mail had attachment
              $ContentType = $this->Get_Header_Flag($TempObj->msgno,'Content-Type');
              $OverviewArray[$i]['HaveAttachment'] = (preg_match("/multipart\/m/i", $ContentType) == 0)? false:true;
              //$OverviewArray[$i]['HaveAttachment'] = false;
              //$MailStructure = $this->Get_Mail_Structure($folderName,$TempObj->uid);
              //$MailStructure = imap_fetchstructure($this->inbox,$TempObj->uid,FT_UID);
              //$MailParts = $MailStructure->parts;
              //for ($j=0; $j< sizeof($MailParts); $j++) {
              //        if ($MailParts[$j]->disposition == "attachment") {
              //                $OverviewArray[$i]['HaveAttachment'] = true;
              //                break;
              //        }
              //}

              // Get UID
              $OverviewArray[$i]['UID'] = $TempObj->uid;

              // Get Mail size
              $OverviewArray[$i]['Size'] = round(($TempObj->size/1024),2);

              // Get Flag
              $OverviewArray[$i]["recent"] = $TempObj->recent;
              $OverviewArray[$i]["flagged"] = $TempObj->flagged;
              $OverviewArray[$i]["answered"] = $TempObj->answered;
              $OverviewArray[$i]["deleted"] = $TempObj->deleted;
              $OverviewArray[$i]["seen"] = $TempObj->seen;
              $OverviewArray[$i]["draft"] = $TempObj->draft;

              // Get Folder Name
              $OverviewArray[$i]["Folder"] = $folderName;*/

              /*// Get Mail Body if needed
              if (in_array('Body',$SearchField)) {
                      $structure = imap_fetchstructure($mailbox, $TempObj->msgno);
                      $OverviewArray[$i]["Body"] = $this->retrieve_textmessage($mailbox, $TempObj->msgno, $structure);
              }

              if (in_array('Cc',$SearchField)) {
                      $header = imap_header($mailbox, $TempObj->msgno);
                      $elements=imap_mime_header_decode(imap_utf8($header->ccaddress));
                      unset($subtmp);
                      for($k=0;$k<count($elements);$k++) {
                              $subtmp .= $elements[$k]->text;
                                                                                        }
                      $OverviewArray[$i]["Cc"] = $subtmp;
              }
                                                }
                                                //echo '<pre>';
                                                //var_dump($OverviewArray);
                                                //echo '</pre>';

                                                // is Search action
                                                if (($Keyword != "" && sizeof($SearchField) > 0) || ($FromDate!="" && $ToDate!="")) {
                                                  $FromYear = substr($FromDate,0,4);
                                                  $FromMonth = substr($FromDate,5,2);
                                                  $FromDay = substr($FromDate,8,2);
                                                  $ToYear = substr($ToDate,0,4);
                                                  $ToMonth = substr($ToDate,5,2);
                                                  $ToDay = substr($ToDate,8,2);

                                                  $FromTime = mktime(0,0,0,$FromMonth,$FromDay,$FromYear);
                                                  $ToTime = mktime(0,0,0,$ToMonth,$ToDay,$ToYear);
                                                  for ($i=0; $i< sizeof($OverviewArray); $i++) {
                                                          $found = false;
                                                          if ($Keyword != "" && sizeof($SearchField) > 0) {
                                                                  for ($j=0; $j< sizeof($SearchField); $j++) {
                                                                          if (stristr($OverviewArray[$i][$SearchField[$j]],$Keyword) != false) {
                                                                                  $found = true;
                                                                                  break;
                                                                          }
                                                                  }
                                                          }
                                                          if ($FromDate!="" && $ToDate!="") {
                                                                  $found = ($FromTime < $OverviewArray[$i]['DateSort'] && $OverviewArray[$i]['DateSort'] < $ToTime)? true:false;
                                                          }

                                                          if ($found) $FinalArray[] = $OverviewArray[$i];
                                                  }
                                                  if (!is_array($FinalArray)) {
                                                          $FinalArray = array();
                                                  }
                                                }
                                            
                                              else {*/
					if (sizeof($OverviewArray) > 0) {
            // Sort the result array
            switch ($sort) {
                    case "DateSort":
                            $RealSort = "DateSort";
                            break;
                    case "From":
                            $RealSort = "From";
                            break;
                    case "Subject":
                            $RealSort = "Subject";
                            break;
                    case "Size":
                            $RealSort = "Size";
                            break;
                    case "To":
                            $RealSort = "To";
                            break;
                    default:
                            $RealSort = "DateSort";
                            break;
            }
						
            foreach ($OverviewArray as $pos =>  $val)
                    $tmp_array[$pos] = $val[$RealSort];

            if ($reverse == 0)
                    uasort($tmp_array,"String_To_Lower_Array_Cmp");
            else
                    uasort($tmp_array,"String_To_Lower_Array_Cmp_Reverse");
            //echo "asort: " . (microtime(true) - $time_start) . "<br>\n";

            // display however you want
            foreach ($tmp_array as $pos =>  $val) {
           	 	$FinalArray[] = $OverviewArray[$pos];
           	}
			//echo "final array: " . (microtime(true) - $time_start) . "<br>\n";

            //$FinalArray = array_slice($FinalArray, $startMsgNumber-1, $adjustedCount);
            //}
          }
          else {
                  $FinalArray = array();
          }
                  /*if (count($result) > 0) {
                  $sortedResult = array(count($result));
                  for($i=0; $i < count($result); $i++)
                  {
                    $sortedResult[array_search($result[$i]->msgno, $msgList)] = $result[$i];
                  }
                  }*/

          //echo "message process time used ".$timespend =  time() - $timestart;
          return $FinalArray;
        }

        /*
          Get decoded mail part (attachment)
          Param: folderName, UID, part number
          Return: part content
        */
        function getMailPart($folderName, $uid, $partNumber)
        {
                        /* To-DO: */
                        $msgno = -1;
                        $mailbox = $this->inbox;
                        imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
                        $mailcount = imap_check($mailbox);
                        $mails = imap_fetch_overview($mailbox, "1:".$mailcount->Nmsgs, 0);

                        for($i=0; $i < count($mails); $i++)
                        {
                                if ($mails[$i]->uid == $uid) $msgno = $mails[$i]->msgno;
                        }

                        if ($msgno != -1)
                        {
                                $structure = imap_fetchstructure($mailbox, $msgno); //have to use msg no here, instead of $uid
                                $parts = $structure->parts;

                                return $parts[$partNumber];
                        }
                        else
                        {
                                return null;
                        }
                        //type
                        //encoding
                        //size
                        //disposition
                        //
                        //
                        //

        }

                        // edit by kenneth chung at 20080507
      function Move_Mail($sourceFolder, $uid, $destinationFolder) {
        $mailbox = $this->inbox;
        
	      imap_reopen($mailbox,$this->getMailServerRef().$this->encodeFolderName($sourceFolder));
	
	      $Result['MoveResult'] = imap_mail_move($mailbox,$uid,$this->encodeFolderName($destinationFolder),CP_UID);
	      $Result['DeleteResult'] = imap_expunge($mailbox);
	
	      return (!in_array(false,$Result));
      }

        /* Return whole email with raw header */
        function getMailRawHeader($folderName, $uid)
        {
                $mailbox = $this->inbox;
                        imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));

                $header = imap_fetchheader($mailbox, $uid, FT_UID);
                           $returnObj = imap_rfc822_parse_headers($header);
                           return $returnObj;
        }

        /*
          Search email on mail folder
          Param: folderName, criteria
          Return: Array() of entries
                    - Array()
                      - FolderName, UID, Subject, From, To
        */
    
    function Search_Mail($folderName,$key_subject="",$key_content="",$key_from="",$key_recipient="",$key_attachmentFilename="",$key_dateFrom="",$key_dateTo=""){
	    ### refer to imap_search(). Cross folder may be done by PHP algorithm
	    $searchCriteria = "";
	    $mailMsgNos = array();
	    //$searchCriteria .= 'CHARSET "iso-8859-1" ';
	    /*$SearchEncoding = array("big5","UTF-8","iso-8859-1");

	    for ($i=0; $i< sizeof($SearchEncoding); $i++) {
	    	if ($SearchEncoding[$i] == "UTF-8") {
		    	$Subject = $key_subject;
		    	$Content = $key_content;
		    	$From = $key_from;
		    	$To = $key_recipient;
		    }
		    else {
		    	$Subject = mb_convert_encoding($key_subject,$SearchEncoding[$i],"UTF-8");
		    	$Content = mb_convert_encoding($key_content,$SearchEncoding[$i],"UTF-8");
		    	$From = mb_convert_encoding($key_from,$SearchEncoding[$i],"UTF-8");
		    	$To = mb_convert_encoding($key_recipient,$SearchEncoding[$i],"UTF-8");
		    }
	    	
	    	if ($key_subject != "") $searchCriteria .= 'SUBJECT "' . $Subject . '" ';
		    //if ($key_content != "") $searchCriteria .= '(BODY "' . $Content . '") ';
		    //if ($key_from != "") $searchCriteria .= '(FROM "' . $From . '") ';
		    //if ($key_recipient != "") $searchCriteria .= '(TO "' . $To . '") ';
		    //if ($key_attachmentFilename != "") $searchCriteria .= '(SUBJECT "' . $key_attachmentFilename . '") ';
		    //if ($key_dateFrom != "") $searchCriteria .= '(SINCE "' . $key_dateFrom . '") ';
		    //if ($key_dateTo != "") $searchCriteria .= '(BEFORE "' . $key_dateTo . '") ';
		    
		    $mailbox = $this->inbox;
	    	imap_reopen($mailbox,$this->getMailServerRef().$this->encodeFolderName($folderName));
	    	
	    	$ResultMsgList = imap_search($mailbox,$searchCriteria,0,$SearchEncoding[$i]);
	    	echo $Subject.'<br>';
	    	echo '<pre>';
	    	var_dump($ResultMsgList);
	    	echo '</pre>';
	    	if ($ResultMsgList !== FALSE) 
	    		$mailMsgNos = array_merge($mailMsgNos,$ResultMsgList);
	    }
	    
	    echo sizeof($mailMsgNos).'<br>';
	    $mailMsgNos = array_unique($mailMsgNos);
	    echo sizeof($mailMsgNos).'<br>';
	    die;*/
	   
	    //if ($key_subject != "") $searchCriteria .= 'SUBJECT "' . $key_subject . '" ';
	    //if ($key_content != "") $searchCriteria .= '(BODY "' . $key_content . '") ';
	    //if ($key_from != "") $searchCriteria .= '(FROM "' . $key_from . '") ';
	    //if ($key_recipient != "") $searchCriteria .= '(TO "' . $key_recipient . '") ';
	    //if ($key_attachmentFilename != "") $searchCriteria .= '(SUBJECT "' . $key_attachmentFilename . '") ';
	    //if ($key_dateFrom != "") $searchCriteria .= '(SINCE "' . $key_dateFrom . '") ';
	    //if ($key_dateTo != "") $searchCriteria .= '(BEFORE "' . $key_dateTo . '") ';
			//echo phpinfo(); die;	
	    $mailbox = $this->inbox;
	    imap_reopen($mailbox,$this->getMailServerRef().$this->encodeFolderName($folderName));
	    
	    $OverAllTime = time();
	    $time = time();
	    if ($key_subject != "") $searchCriteria = 'SUBJECT "' . $key_subject . '" ';
	    //echo "Criteria: $searchCriteria <br>\n";
	    $Temp = imap_search($mailbox,$searchCriteria);
	    echo '<div style="visibility:hidden; display:none">';
	    echo '<pre>';
	    var_dump($Temp);
	    echo '</pre>';
	    if ($Temp !== FALSE) $mailMsgNos = array_merge($mailMsgNos,$Temp);
	    //echo 'Used Time: '. (time() - $time).'<br>';
	    
	    $time = time();
	    if ($key_from != "") $searchCriteria = 'FROM "' . $key_from . '" ';
	    //echo "Criteria: $searchCriteria <br>\n";
	    $Temp = imap_search($mailbox,$searchCriteria);
	    echo '<pre>';
	    var_dump($Temp);
	    echo '</pre>';
	    if ($Temp !== FALSE) $mailMsgNos = array_merge($mailMsgNos,$Temp);
	    //echo 'Used Time: '. (time() - $time).'<br>';
	    
	    $time = time();
	    if ($key_recipient != "") $searchCriteria = 'TO "' . $key_recipient . '" ';
	   	//echo "Criteria: $searchCriteria <br>\n";
	    $Temp = imap_search($mailbox,$searchCriteria);
	    echo '<pre>';
	    var_dump($Temp);
	    echo '</pre>';
	    if ($Temp !== FALSE) $mailMsgNos = array_merge($mailMsgNos,$Temp);
	    //echo 'Used Time: '. (time() - $time).'<br>';
	    echo '</div>';
	    
	    $mailMsgNos = array_unique($mailMsgNos);
	    //echo 'Total Messages Found: '.sizeof($mailMsgNos).'<br>';
	    //echo 'Over All used Time for '.$folderName.': '. (time() - $OverAllTime).'<br><br>';
	    /*
	    echo "Criteria: $searchCriteria <br>\n";
	    echo 'UTF-8: '.sizeof($mailMsgNos).'<br>';
	    $mailMsgNos = imap_search($mailbox,$searchCriteria,0,'big5');
	    echo 'BIG5: '.sizeof($mailMsgNos).'<br>';
	    echo 'Used Time: '. (time() - $time); die;*/
	    
//	    var_dump($mailMsgNos);
//echo "Criteria: $searchCriteria <br>\n";
//print_r($mailMsgNos);
	
	    if ($mailMsgNos === false) return null;
	//echo 'test';
	//die;
			foreach ( $mailMsgNos as $key => $value)
				$ReturnArray[] = array($folderName, $value);
				
	    return $ReturnArray;
    }
    
    function Get_Mail_Overview($MsgNoArray) {
    	for ($i=0; $i< sizeof($MsgNoArray); $i++) {
    		if ($MsgNoArray[$i][0] != $MsgNoArray[$i-1][0] || $i == 0) {
    			$this->Go_To_Folder($MsgNoArray[$i][0]);
    		}
    		
    		$result = imap_fetch_overview($this->inbox, $MsgNoArray[$i][1]);
    		
    		$TempObj = $result[0];
        unset($DisplayFrom);
        $FromElement = imap_mime_header_decode($TempObj->from);
        for ($j=0; $j< sizeof($FromElement); $j++) {
           $DisplayFrom .= $this->Conver_Encoding($FromElement[$j]->text,"UTF-8", $FromElement[$j]->charset);
           if ($j==0) $DisplayFrom .= " ";
        }
        $OverviewArray[$i]['From'] = $DisplayFrom;

        unset($DisplayTo);
        $ToElement = imap_mime_header_decode($TempObj->to);
        /*echo '<pre>';
      	var_dump($ToElement);
      	echo '</pre>';die;*/
        if (sizeof($ToElement) == 0) {
        	$header = imap_fetchheader($this->inbox, $TempObj->msgno);
        	$RawHeader = imap_rfc822_parse_headers($header);
        	$ToElement = $RawHeader->to;
        	
        	if (sizeof($ToElement) > 0) {
        		if (!isset($ToElement[0]->personal)) $DisplayName = $ToElement[0]->mailbox."@".$ToElement[0]->host;
						else {
							$DisplayObject = imap_mime_header_decode(imap_utf8($ToElement[0]->personal));
							$DisplayName = $DisplayObject[0]->text;
						}
        		
		        $OverviewArray[$i]['To'] = $DisplayName."...";
        		//$OverviewArray[$i]['To'] = $Lang['email']['MultiReceiver'];
        	}
        	else {
        		$OverviewArray[$i]['To'] = $Lang['email']['NoReceiver'];
        	}
				}
				else {
          for ($j=0; $j< sizeof($ToElement); $j++) {
            $DisplayTo .= $this->Conver_Encoding($ToElement[$j]->text,"UTF-8", $ToElement[$j]->charset);
            if ($j==0) $DisplayTo .= " ";
          }
          $OverviewArray[$i]['To'] = $DisplayTo;
        }
        

        // trim the mini second for the date
        $Date = substr($TempObj->date, 0, -5);
        $OverviewArray[$i]['DateReceived'] = $Date;
        $TempDate = explode(',',$TempObj->date);
        $DayOfWeek = $TempDate[0];
        $DateTime = explode(' ',trim($TempDate[1]));
        list($Day,$Month,$Year,$Time,$Zone) = $DateTime;
        list($Hour,$Min,$Sec) = explode(':',$Time);
        $SortTime = mktime($Hour,$Min,$Sec,Get_Month_Number($Month),$Day,$Year);
        $OverviewArray[$i]['DateSort'] = $SortTime;

        // Get Priority
        $MailPriority = $this->Get_Header_Flag($TempObj->msgno,"x-priority");
        $OverviewArray[$i]['Priority'] = $MailPriority;

        // replace NO SUBJECT if subject is empty
        $SubjectObj = imap_mime_header_decode(imap_utf8($TempObj->subject));
        unset($subtmp);
        for($k=0;$k<count($SubjectObj);$k++) {
        	$subtmp .= $SubjectObj[$k]->text;
        }
        $Subject = ($subtmp == "")? "NO SUBJECT":$subtmp;
        $OverviewArray[$i]['Subject'] = $Subject;
        //$OverviewArray[$i]['Subject'] = $TempObj->subject;

        // check if mail had attachment
        $ContentType = $this->Get_Header_Flag($TempObj->msgno,'Content-Type');
        $OverviewArray[$i]['HaveAttachment'] = (preg_match("/multipart\/m/i", $ContentType) == 0)? false:true;
        //$OverviewArray[$i]['HaveAttachment'] = false;
        //$MailStructure = $this->Get_Mail_Structure($folderName,$TempObj->uid);
        /*$MailStructure = imap_fetchstructure($this->inbox,$TempObj->uid,FT_UID);
        $MailParts = $MailStructure->parts;
        for ($j=0; $j< sizeof($MailParts); $j++) {
                if ($MailParts[$j]->disposition == "attachment") {
                        $OverviewArray[$i]['HaveAttachment'] = true;
                        break;
                }
        }*/

        // Get UID
        $OverviewArray[$i]['UID'] = $TempObj->uid;

        // Get Mail size
        $OverviewArray[$i]['Size'] = round(($TempObj->size/1024),2);

        // Get Flag
        $OverviewArray[$i]["recent"] = $TempObj->recent;
        $OverviewArray[$i]["flagged"] = $TempObj->flagged;
        $OverviewArray[$i]["answered"] = $TempObj->answered;
        $OverviewArray[$i]["deleted"] = $TempObj->deleted;
        $OverviewArray[$i]["seen"] = $TempObj->seen;
        $OverviewArray[$i]["draft"] = $TempObj->draft;

        // Get Folder Name
        $OverviewArray[$i]["Folder"] = $MsgNoArray[$i][0];
    	}
	    
	    return $OverviewArray;
    }
/*
        function checkMailFolderStatus($folderName)
        {

                $status = @imap_status($this->inbox, $this->getMailServerRef().imap_utf7_encode($folderName), SA_ALL);
                $msg = "";
                    if ($status) {
                        $msg .= "Mailbox: '$folderName' has the following status:<br />\n";
                        $msg .= "Messages:   " . $status->messages    . "<br />\n";
                        $msg .= "Recent:     " . $status->recent      . "<br />\n";
                        $msg .= "Unseen:     " . $status->unseen      . "<br />\n";
                        $msg .= "UIDnext:    " . $status->uidnext     . "<br />\n";
                        $msg .= "UIDvalidity:" . $status->uidvalidity . "<br />\n";
                    }

                    return $msg;
        }
        */

                                function getMailDisplayFolder($FullPath) {
                                        if (strrpos($FullPath,".") !== false) {
                                                $FolderDisplayName = substr($FullPath,strrpos($FullPath,".")+1, strlen($FullPath)-1);
                                        }
                                        else {
                                                $FolderDisplayName = $FullPath;
                                        }
                                        return $FolderDisplayName;
                                }

                                // get folder Structure for UI use
                                function Get_Folder_Structure($Shift=false,$IncludeDefaultFolder=true) {
                                        Global $Lang, $SYS_CONFIG;

                                        $FolderList = $this->getMailFolders();
                                        //sort($FolderList,SORT_STRING);
                                        //usort($FolderList,"String_To_Lower_Array_Cmp");
                                        /*echo '<pre>';
                                        var_dump($FolderList);
                                        echo '</pre>';*/

                                        $FolderArray[] = array($Lang['email']['MoveToFolder'],'');
                                        if ($IncludeDefaultFolder) {
                                                // please default folder list at top
                                                $FolderArray[] = array($Lang['email']['DefaultFolderList'],'Default','LabelGroupStart');
                                                foreach ($this->DefaultFolderList as $Key => $Val) {
                                                        $FolderArray[] = array($this->getMailDisplayFolder($Val),$Val);
                                                }
                                                $FolderArray[] = array('','','LabelGroupEnd');
                                                $FolderArray[] = array($Lang['email']['PersonalFolderList'],'Personal','LabelGroupStart');
                                              }

                                        for ($i=0; $i< sizeof($FolderList); $i++) {
                                                // exclude all default folder
                                          $defaultFolderList = $this->DefaultFolderList;
                                          foreach ($defaultFolderList as $Key => $Val) {
                                                  if ($FolderList[$i] == $this->FolderPrefix) {
                                                          continue 2;
                                                  }
                                                  //if (stristr($FolderList[$i],$defaultFolderList[$j]) !== FALSE && $defaultFolderList[$j] != $this->FolderPrefix) {
                                                  if ($FolderList[$i] == $Val && $Val != $this->FolderPrefix) {
                                                          continue 2;
                                                  }
                                          }

                                          $Level = substr_count($FolderList[$i],$this->folderDelimiter);
                                          $LevelSpaces = "";
                                          for ($j=0; $j< $Level; $j++) {
                                                  $LevelSpaces .= "&nbsp;&nbsp;";
                                          }
                                          $FolderDisplayName = $this->getMailDisplayFolder($FolderList[$i]);

                                          $FolderArray[] = array($LevelSpaces.$FolderDisplayName,$FolderList[$i]);
                                        }

																				if ($IncludeDefaultFolder) {
                                          $FolderArray[] = array('','','LabelGroupEnd');
                                        }
                                        if ($Shift) {
                                        	array_shift($FolderArray);
                                        }

                                        return $FolderArray;
                                }

                                // function to convert plain text subject to mail server's format
                                function Get_Mail_Format($string, $encoding='UTF-8') {
                            $string = str_replace(" ", "_", trim($string)) ;
                            // We need to delete "=\r\n" produced by imap_8bit() and replace '?'
                            $string = str_replace("?", "=3F", str_replace("=\r\n", "", imap_8bit($string))) ;

                            // Now we split by \r\n - i'm not sure about how many chars (header name counts or not?)
                            $string = chunk_split($string, 73);
                            // We also have to remove last unneeded \r\n :
                            $string = substr($string, 0, strlen($string)-2);
                            // replace newlines with encoding text "=?UTF ..."
                            $string = str_replace("\r\n", "?=".chr(13).chr(10)." =?".$encoding."?Q?", $string) ;

                            return '=?'.$encoding.'?Q?'.$string.'?=';
                                }

                                // Get structure of a mail
                                function Get_Mail_Structure($Folder,$UID) {
                                        imap_reopen($this->inbox, $this->getMailServerRef().$this->encodeFolderName($Folder));

                                        return imap_fetchstructure($this->inbox,$UID,FT_UID);
                                }

        function Send_Mail_By_PHPMailer($To=array(),$Cc=array(),$Bcc=array(),$Subject="",$Body="",$AttachList=array()) {
                /* ----- PHP Mailer ----- */
                $lmailer = new PHPMailer();
                $lmailer->IsSMTP();
                $lmailer->Host = $this->host;
                $lmailer->SMTPAuth = true;
                $lmailer->Username = $this->CurUserAddress;
                $lmailer->Password = $this->CurUserPassword;

                $lmailer->From = $this->CurUserAddress;
                $lmailer->FromName = '';

                // config reply-to
                $lmailer->AddReplyTo($this->CurUserAddress);

                // config To
                for ($i=0; $i< sizeof($To); $i++) {
                        $lmailer->AddAddress($To[$i]);
                }

                // config CC
                for ($i=0; $i< sizeof($Cc); $i++) {
                        $lmailer->AddCC($Cc[$i]);
                }

                // config BCC
                for ($i=0; $i< sizeof($Bcc); $i++) {
                        $lmailer->AddBCC($Bcc[$i]);
                }

                // add attachment
                for ($i=0; $i< sizeof($AttachList); $i++) {
                        $lmailer->AddAttachment($AttachList[$i][2],$AttachList[$i][1]);
                }

                $lmailer->Subject = $Subject;
                $lmailer->Body = $Body;
                $lmailer->AltBody = $Body;
                $lmailer->IsHTML(true);

                return $lmailer->Send();
        }

        //get Mail Importance Flag/ content type
        function Get_Header_Flag($MsgNo,$Flag) {
                $Flag = strtolower($Flag);
                $TextHeader = imap_fetchheader($this->inbox, $MsgNo);
								
                $TempHeader = trim(str_replace(array("\r\n", "\n\t", "\n "),array("\n", ' ', ' '), $TextHeader));
                $TempHeader = explode("\n" , $TempHeader);
                for ($i=0; $i< sizeof($TempHeader); $i++) {
                        $pos = strpos($TempHeader[$i], ':');
                  if ($pos > 0) {
                    $field = strtolower(substr($TempHeader[$i], 0, $pos));
                    if (!strstr($field,' ')) { /* valid field */
                      $value = trim(substr($TempHeader[$i], $pos+1));
                      switch($field) {
                        case 'x-priority':
                                      $aMsg['x-priority'] = ($value) ? (int) $value: 3;
                                      break;
                        case 'priority':
                        case 'importance':
                                            if (!isset($aMsg['x-priority'])) {
                                        $aPrio = split('/\w/',trim($value));
                                        $sPrio = strtolower(array_shift($aPrio));
                                        if  (is_numeric($sPrio)) {
                                            $iPrio = (int) $sPrio;
                                        } elseif ( $sPrio == 'non-urgent' || $sPrio == 'low' ) {
                                            $iPrio = 3;
                                        } elseif ( $sPrio == 'urgent' || $sPrio == 'high' ) {
                                            $iPrio = 1;
                                        } else {
                                            // default is normal priority
                                            $iPrio = 3;
                                        }
                                        $aMsg['x-priority'] = $iPrio;
                                            }
                                            break;
                                          case 'subject':
                                                  $aMsg['subject'] = imap_mime_header_decode(imap_utf8($value));
                                                  break;
                                                default:
                                                        break;
                        case 'content-type':
                        	$aMsg['content-type'] = $value;
                        	break;
                      }
                    }
                  }
                }

                return $aMsg[$Flag];
        }

        // Get the number of unseen message
        function Get_Unseen_Mail_Number($Folder) {
                $status = @imap_status($this->inbox,$this->getMailServerRef().$Folder,SA_UNSEEN);

                $NumberOfUnseen = ($status != false)? $status->unseen:$status;
                $DisplayUnseen = ($NumberOfUnseen == false || $NumberOfUnseen == 0)? "": "(".$NumberOfUnseen.")";

                return $DisplayUnseen;
        }

        function Get_Last_Msg_UID($Folder) {
                $mailbox = $this->inbox;
                imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($Folder));

                //check the max msg number in the mailbox
                $mailcount = imap_check($mailbox);
                $UID = imap_uid($mailbox,$mailcount->Nmsgs);
                return $UID;
        }

                                /* ------ Start of Mail Preference -------- */
                                function Check_Preference_Exists() {
                                        $db = new database();
                                        $sql = 'Select
                                                                                count(1)
                                                                        From
                                                                                MAIL_PREFERENCE
                                                                        Where
                                                                                MailBoxName = \''.$this->CurUserAddress.'\'';
                                        $Result = $db->returnVector($sql);

                                        if ($Result[0] > 0) {
                                                return true;
                                        }
                                        else {
                                                return false;
                                        }
                                }

        function setMailForward($targetEmail, $keepCopy, $EmailList)
        {
                GLOBAL $SYS_CONFIG;

                #$login = $this->CurUserName;
                $login = $this->CurUserAddress;
				
				$tmpResponse = $login."<br>";
				
				// return $tmpResponse;
				// break;
				
				$this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);
                if (!$this->checkSecret())
                {
                     return false;
                }
				$path = $this->api_url['SetEmailForward'];
				$targetEmail = stripslashes($targetEmail);
                
				$success = true;
                for ($i = 0; $i < count($EmailList); ++$i)
                {
                	$login = strtolower($EmailList[$i]);
                
                	//$post_data = "loginID=".urlencode($login)."&targetEmail=".urlencode($targetEmail)."&keepCopy=".urlencode($keepCopy)."&secret=".urlencode($this->secret);
                	$post_data = "login=".urlencode($login)."&recipient=".urlencode($targetEmail)."&type=".($keepCopy?"1":"0")."&secret=".urlencode($this->secret);
                	if ($this->actype != "")
                	{
                	    $post_data .= "&actype=".$this->actype;
                	}
                	$response = $this->executePost($path, $post_data);
                	
                	$success = $success && ($response[0]=="1");
					
            	}
				
                if ($success)
                {
                    $db = new database();
	                $sp = "exec Update_MailPreference_Forward @MailBoxName='" . $this->CurUserAddress . "', ";
	                $sp .= "@ForwardedEmail='" . $targetEmail . "', ";
	                $sp .= "@ForwardKeepCopy=" . ($keepCopy ? "1" : "0");

	                $db->db_db_query($sp);
                    return true;
                }
                else return false;
        }

        function removeMailForward($EmailList)
        {
            Global $SYS_CONFIG;

            #$login = $this->CurUserName;
            $login = $this->CurUserAddress;
            if (!$this->checkSecret())
            {
                 return false;
            }
                
            $success = true;
            for ($i = 0; $i < count($EmailList); ++$i)
            {
	            $login = strtolower($EmailList[$i]);

            	$path = $this->api_url['RemoveEmailForward']."?loginID=".urlencode($login)."&secret=".urlencode($this->secret);
               	if ($this->actype != "")
               	{
	            	$path .= "&actype=".$this->actype;
               	}
               	$response = $this->getWebPage($path);
               	$success = $success && ($response[0]=="1");
            }
            
            if ($success)
            {
                    $db = new database();
                        $sp = "exec Update_MailPreference_Forward @MailBoxName='" . $this->CurUserAddress . "', ";
                        $sp .= "@ForwardedEmail='', ";
                        $sp .= "@ForwardKeepCopy=0";
                        $db->db_db_query($sp);

                    return true;
            }
            else return false;
        }

        #### Get .forward content
        #### Return : array ($keepCopy, $forwarded_email)
        /*function getMailForward()
        {
                //get from DB, do not use the api
                $db = new database();
                $sp = "exec Get_MailPreference_ByMailBoxName @MailBoxName='" . $this->CurUserAddress . "'";
                   $ReturnContent = $db->returnArray($sp);

                        //$db->Close();
                        return $ReturnContent;

        }*/

		# EmailList[0] -> Real Email Address, others are Alias 
        function setAutoReply($ReplyMessage, $EmailList)
        {
                 GLOBAL $SYS_CONFIG;

                 $login = $this->CurUserAddress;

                 if (!$this->checkSecret())
                 {
                      return false;
                 }
                 $ReplyMessage = stripslashes($ReplyMessage);
                 $path = $this->api_url['AddAutoreply'];
                 
                 $success = true;
                 for ($i = 0; $i < count($EmailList); ++$i)
                 {
					 $path = ($i == 0) ? $this->api_url['AddAutoreply'] : $this->api_url['AddAliasAutoreply'];
	                 $login = strtolower($EmailList[$i]);
	                 $post_data  = "login=".urlencode($EmailList[0]);
					 $post_data .= ($i == 0) ? "" : "&alias=".urlencode($login);
					 $post_data .= "&text=".urlencode($ReplyMessage);
					 
	                 if ($this->actype != "")
	                 {
	                     $post_data .= "&actype=".$this->actype;
	                 }
					 
	                 $response = $this->executePost($path, $post_data);
	                 
	                 $success = $success && ($response[0]=="1");
             	 }

                if ($success)
                {
                        $db = new database();
                        $sp = "exec Update_MailPreference_AutoReply @MailBoxName='" . $this->CurUserAddress . "', ";
                        $sp .= "@AutoReplyMessage='" . stripslashes($db->Get_Safe_Sql_Query($ReplyMessage)) . "', ";
                        $sp .= "@EnableAutoReply=true";

                        $db->db_db_query($sp);

                        return true;
                }
                else return false;
        }

        function removeAutoReply($ReplyMessage, $EmailList)
        {
                Global $SYS_CONFIG;

                $login = $this->CurUserAddress;
                if (!$this->checkSecret())
                {
                     return false;
                }
				
                $success = true;
				
				$tmpResponse = $login."<br>";
				
				for ($i = count($EmailList)-1; $i >= 0; --$i)
                {
	                $login = strtolower($EmailList[$i]);
	                //$path = $this->api_url['RemoveAutoreply']."?login=".urlencode($login);
					$path = ($i == 0) ? $this->api_url['RemoveAutoreply'] : $this->api_url['RemoveAliasAutoreply'];
	                $login = strtolower($EmailList[$i]);
	                $post_data = "login=".urlencode($EmailList[0]);
					$post_data .= ($i == 0) ? "" : "&alias=".urlencode($login);
	
	                if ($this->actype != "")
	                {
	                    $post_data .= "&actype=".$this->actype;
	                }
					$response = $this->executePost($path, $post_data);
					
	                $success = $success && ($response[0]=="1");
					
					if ($success) {
						$tmpSuccessFlag = 1;
					}
					
					
				}
					
                if ($success)
                {
                    $db = new database();
                        $sp = "exec Update_MailPreference_AutoReply @MailBoxName='" . $this->CurUserAddress . "', ";
                        // $sp .= "@AutoReplyMessage='', ";
						$sp .= "@AutoReplyMessage='" . $db->Get_Safe_Sql_Query($ReplyMessage) . "', ";
                        $sp .= "@EnableAutoReply=false";
						
						// $tmpResponse .= $sp;
						// return $tmpResponse;
                        $db->db_db_query($sp);
					
                    return true;
                }
                else return false;
				
        }

		function SetGroupMailForward($fromEmail, $toEmail, $keepCopy)
        {
                Global $SYS_CONFIG;
										
                if (!$this->checkSecret())
                {
                     return false;
                }

				$this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);				
				$path = $this->mail_api_url['SetGroupMailForward'];
                $path .= "?login=".urlencode($fromEmail);
				$path .= "&recipient=".urlencode($toEmail);
				$path .= "&type=".($keepCopy?"1":"0");
                if ($this->actype != "")
                {
                    $path .= "&actype=".$this->actype;
                }
				
               	$response = $this->getWebPage($path);
                $success = ($response[0]=="1");
                return $success;
        }
		
		function RemoveGroupMailForward($email)
        {
                Global $SYS_CONFIG;
										
                if (!$this->checkSecret())
                {
                     return false;
                }

				$this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);				
				$path = $this->mail_api_url['RemoveGroupMailForward'];
                $path .= "?login=".urlencode($email);
                if ($this->actype != "")
                {
                    $path .= "&actype=".$this->actype;
                }
				
               	$response = $this->getWebPage($path);
                $success = ($response[0]=="1");
                return $success;
        }
		
        function Get_Preference()
        {
                $db = new database();
                $sp = "exec Get_MailPreference_ByMailBoxName @MailBoxName = '" . $this->CurUserAddress. "'";
                $ReturnContent = $db->returnArray($sp);
                        //$db->Close();
                return $ReturnContent;

        }

        function Update_Preference($UpdateValue)
        {
                $db = new database();
				
				# set the display name to "Surname, Preferred Name" if it is null
				if(trim($UpdateValue["DisplayName"]) == "") {
					$sql = "SELECT Surname+','+' '+PrefName as 'DisplayName'
							FROM INTRANET_USER
							WHERE EmailAlias1 = '".$UpdateValue["MailBoxName"]."'
							OR Email = '".$UpdateValue["MailBoxName"]."'";
					$result = $db->returnArray($sql);
					
					$UpdateValue["DisplayName"] = $result[0]['DisplayName'];
				}
				
                $sp = "exec Update_MailPreference ";
                while ($element = each($UpdateValue))
                        {
                                $sp .= "@" . $element["key"] . " = '" . $db->Get_Safe_Sql_Query($element["value"]) . "', ";
                        }
                        $sp = substr($sp, 0, -2);

                $ReturnBln = $db->db_db_query($sp);

                        //$db->Close();
                        return $ReturnBln;
        }

        function New_Preference($MailBoxName,$UserID="") {

                $db = new database();

                $Result = $this->Check_Preference_Exists($MailBoxName);

                if (!$Result) {
                        if ($UserID == "") $UserID = $_SESSION['SSV_USERID'];
                                                $user = new user($UserID);
                                                if ($MailBoxName == $user->Email) {
                                $sql = 'Insert Into MAIL_PREFERENCE
                                                                        (
                                                                        MailBoxName,
                                                                        UserID,
                                                                        DisplayName
                                                                        )
                                                                Values
                                                                        (
                                                                        \''.$MailBoxName.'\',
                                                                        \''.$UserID.'\',
                                                                        \''.$user->Surname.','.' '.$user->PrefName.'\'
                                                                        )
                                                                ';
                                $Result = $db->db_db_query($sql);
                        }
                        else {
                                $sql = 'Insert Into MAIL_PREFERENCE
                                                                        (
                                                                        MailBoxName
                                                                        )
                                                                Values
                                                                        (
                                                                        \''.$MailBoxName.'\'
                                                                        )
                                                                ';
                                $Result = $db->db_db_query($sql);
                        }
                }

                return $Result;
        }
                                /* ------ End of Mail Preference -------- */

  // get prev/ next message UID for Read mail navigation buttons
  function Get_Prev_Next_UID($folderName, $sort="", $reverse=0, $CurUID) {
    $mailbox = $this->inbox;
    imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
				
		$MsgNoList = imap_sort($mailbox, SORTDATE, $reverse);
		$MsgNo = $this->Get_MessageID($CurUID);
		for($i=0; $i < sizeof($MsgNoList); $i++) {
			if ($MsgNoList[$i] == $MsgNo) {
				$CurIndex = $i;
				break;
			}
		}
		
		$PrevUID = ($i == sizeof($MsgNoList))? 0:$this->Get_UID($MsgNoList[$i+1]);
		$NextUID = ($i == 0)? 0:$this->Get_UID($MsgNoList[$i-1]);
		
		if ($PrevUID != 0)
            $ReturnVal['PrevUID'] = $PrevUID;
    else
            $ReturnVal['PrevUID'] = false;

    if ($NextUID != 0)
            $ReturnVal['NextUID'] = $NextUID;
    else
            $ReturnVal['NextUID'] = false;
            
    return $ReturnVal;
  }

        // Get result of simple/ advance search
        function Get_Search_Mail_List($FromMsg=1, $ToMsg="", $SortField="", $Reverse=0, $SearchField=array(),$SearchFolder=array(),$FromDate="",$ToDate="", $Keyword="") {
                if (sizeof($SearchFolder) == 0) {
                        $FolderList = $this->getMailFolders();
                }
                else {
                        $FolderList = $SearchFolder;
                }
                //sort($FolderList,SORT_STRING);

                $mailbox = $this->inbox;
                $MessageIDs = array();
                for ($i=0; $i< sizeof($FolderList); $i++) {
                  //imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($FolderList[$i]));
                  //$TotalNumMsg = imap_num_msg($mailbox);
									$Temp = $this->Search_Mail($FolderList[$i], $Keyword, $Keyword, $Keyword, $Keyword, $Keyword, $FromDate, $ToDate);
                  //$Temp = $this->listMailInFolder($FolderList[$i], 1, $TotalNumMsg, null, null, $Keyword, $SearchField, $FromDate, $ToDate);
                  if (!is_null($Temp)) {
                  	$MessageIDs = array_merge($MessageIDs,$Temp);	
                	}
                }
                
                $FetchMsgNo = array_slice($MessageIDs,$FromMsg-1,$ToMsg);
                for ($i=0; $i< sizeof($FetchMsgNo) ; $i++) {
                	if ($FetchMsgNo[$i][0] != $FetchMsgNo[$i-1][0] || $i == 0) {
                		$this->Go_To_Folder($FetchMsgNo[$i][0]);
                	}
                	$OverviewArray[] = $this->Get_Mail_In_Folder($FetchMsgNo[$i][0],$FetchMsgNo[$i][1]);
                }
                //$OverviewArray = $this->Get_Mail_Overview($FetchMsgNo);

                if (sizeof($OverviewArray) > 0) {
                        // Sort the result array
                        switch ($SortField) {
                                case "DateSort":
                                        $RealSort = "DateSort";
                                        break;
                                case "From":
                                        $RealSort = "From";
                                        break;
                                case "Subject":
                                        $RealSort = "Subject";
                                        break;
                                case "Size":
                                        $RealSort = "Size";
                                        break;
                                case "To":
                                        $RealSort = "To";
                                        break;
                                case "Folder":
                                        $RealSort = "Folder";
                                        break;
                                default:
                                        $RealSort = "DateSort";
                                        break;
                        }

                        foreach ($OverviewArray as $pos =>  $val)
                                $tmp_array[$pos] = $val[$RealSort];

                        if ($reverse == 0)
						            	uasort($tmp_array,"String_To_Lower_Array_Cmp");
						            else
						            	uasort($tmp_array,"String_To_Lower_Array_Cmp_Reverse");

                        // display however you want
                        foreach ($tmp_array as $pos =>  $val)
                                $FinalArray[] = $OverviewArray[$pos];

                        //$FinalArray = array_slice($FinalArray, $FromMsg-1, $ToMsg);
                }
                else {
                        $FinalArray = array();
                }

                return array($FinalArray,sizeof($MessageIDs));
        }

        function Clear_Folder($FolderName) {
                imap_reopen($this->inbox,$this->getMailServerRef().$this->encodeFolderName($FolderName));
                $MaxMsgNum = imap_num_msg($this->inbox);

                imap_setflag_full($this->inbox,'1:'.$MaxMsgNum,'\\Deleted');

                $result = imap_expunge($this->inbox);

                return $result;
        }

        function Set_Answered($FolderName,$UID) {
                imap_reopen($this->inbox,$this->getMailServerRef().$this->encodeFolderName($FolderName));

                $result = imap_setflag_full($this->inbox,$UID,'\\Answered',ST_UID);

                return $result;
        }

        function Clear_Expired_Mail($FolderName) {
                if ($FolderName == $this->RootPrefix."Trash" || $FolderName == $this->RootPrefix."Spam") {
                        $Perference = $this->Get_Preference($_SESSION['SSV_USERID']);

                        if ($FolderName == $this->RootPrefix."Trash") $ExpirePeriod = $Perference[0][10];
                        else $ExpirePeriod = $Perference[0][9];

                        if ($ExpirePeriod == 0) return true;

                        imap_reopen($this->inbox,$this->getMailServerRef().$this->encodeFolderName($FolderName));
                        $NumberOfMessage = imap_num_msg($this->inbox);
                        $MailToRemove = $this->listMailInFolder($FolderName, 1, $NumberOfMessage);

                        $DateToClear = time() - ($ExpirePeriod * 24 * 60 * 60);
                        if (sizeof($MailToRemove) > 0) {
                                if ($MailToRemove[0]['DateSort'] < $DateToClear) {
                                        for ($i=0; $i< sizeof($MailToRemove); $i++) {
                                                if ($MailToRemove[$i]['DateSort'] < $DateToClear)
                                                        $UIDRemoveList[] = $MailToRemove[$i]['UID'];
                                        }

                                        imap_delete($this->inbox,implode(",",$UIDRemoveList),FT_UID);

                                        return imap_expunge($this->inbox);
                                }
                                else return true;
                        }
                        else return true;
                }
                else return false;
        }

      #### New functions for ASAV by BL
      function readBlackList()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['ReadBlackList']."?actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response == "")
               {
                   return "";
               }
               else
               {
                   $array = explode(";",$response);
                   return $array;
               }
      }
      function addBlackList($target_address)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['AddBlackList']."?actype=".$this->actype."&list=".urlencode($target_address)."&secret=".urlencode($this->secret);
               echo $path;
               $response = $this->getWebPage($path);
               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function removeBlackList($target_addresss)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['RemoveBlackList']."?actype=".$this->actype."&list=".urlencode($target_address)."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function readWhiteList()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['ReadWhiteList']."?actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response == "")
               {
                   return "";
               }
               else
               {
                   $array = explode(";",$response);
                   return $array;
               }
      }
      function addWhiteList($target_address)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['AddWhiteList']."?actype=".$this->actype."&list=".urlencode($target_address)."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function removeWhiteList($target_address)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['RemoveWhiteList']."?actype=".$this->actype."&list=".urlencode($target_address)."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);

               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }

      function readSpamCheck()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['ReadSpamCheck']."?actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               return $response;
      }
      function turnOnSpamCheck()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['BypassSpamCheck']."?onOff=Y&actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function turnOffSpamCheck()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['BypassSpamCheck']."?onOff=N&actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function readVirusCheck()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['ReadVirusCheck']."?actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               return $response;
      }
      function turnOnVirusCheck()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['BypassVirusCheck']."?onOff=Y&actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function turnOffVirusCheck()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['BypassVirusCheck']."?onOff=N&actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }


      ## Alias functions
      function addAlias($newAlias, $loginname)
      {
      	global $SYS_CONFIG;
      	
               if (!$this->checkSecret())
               {
                    return false;
               }

               $loginname = $this->attachDomain($loginname);
               //echo $loginname.'<br>';
               $newAlias = $this->attachDomain($newAlias);
               //echo $newAlias.'<br>';
               $path = $this->mail_api_url['AddAlias'];
               $post_data = "login=".$newAlias."&recipient=".$loginname."&actype=".$this->actype."&secret=".urlencode($this->secret);
               //$response = $this->executePost($path, $post_data);
               
               //1. found cannot call the api using executePost, use the old method first
               //2. urlencoded parameter is not supported by api
               $path = $this->mail_api_url['AddAlias']."?".$post_data;
               //echo $SYS_CONFIG['Mail']['APIHost'].':'.$SYS_CONFIG['Mail']['APIPort'].$path.'<br>';
               $this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);
               $response = $this->getWebPage($path);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }

      }
	  
      function removeAlias($alias, $UserEmail)
      {
			global $SYS_CONFIG;
			if (!$this->checkSecret())
            {
				return false;
			}
			$this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);
			
			//Remove auto reply
			$path = $this->api_url['RemoveAliasAutoreply'];
	        $path .= "?login=".urlencode($UserEmail);
			$path .= "&alias=".urlencode(strtolower($alias));
			if ($this->actype != "")
	        {
				$path .= "&actype=".$this->actype;
	        }
			$response = $this->getWebPage($path);
					 
			//Remove mail forward
			$path = $this->api_url['RemoveEmailForward']."?loginID=".urlencode($alias)."&secret=".urlencode($this->secret);
            if ($this->actype != "")
            {
				$path .= "&actype=".$this->actype;
			}
            $response = $this->getWebPage($path);

			//remove the alias
            $loginname = $this->attachDomain($alias);
            $path = $this->mail_api_url['RemoveAlias'];
            $path .= "?login=".$loginname."&actype=".$this->actype."&secret=".urlencode($this->secret);
            $response = $this->getWebPage($path);
			
            if ($response[0]=="1")
            {
				return true;
            }
            else
            {
				return false;
            }
      }

      # Return array( alias 1, alias 2, alias 3, ......)
      function getAliasInfo($loginname)
      {
      	global $SYS_CONFIG;
               if (!$this->checkSecret())
               {
                    return false;
               }

               $loginname = $this->attachDomain($loginname);
               $path = $this->mail_api_url['GetAliasInfo'];
               $post_data = "login=".$loginname."&actype=".$this->actype."&secret=".urlencode($this->secret);
               $this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);
               $response = $this->getWebPage($path."?".$post_data);
               //$response = $this->executePost($path, $post_data);
               if ($response == "")
               {
                   return array();
               }
               $entries = explode(",",$response);
               return $entries;
      }

      # Return array (
      #               array (primary email, alias1, alias2, alias3, .....)
      # )
      function getAliasList()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }

               $path = $this->mail_api_url['GetAliasList'];
               $post_data = "actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->executePost($path, $post_data);
               if ($response == "")
               {
                   return array();
               }

               # Create array information
               $result = array();
               $entries = explode(",",$response);
               for ($i=0; $i<sizeof($entries); $i++)
               {
                    if ($entries[$i]=="")
                    {
                        $temp = explode(";", $entries[$i]);
                        if (sizeof($temp)<2)          # Skip error row
                        {

                        }
                        else
                        {
                            $result[] = $temp;
                        }

                    }
               }

               return $result;
      }
		
		
		function GetUserQuota() {
		Global $SYS_CONFIG;
			// echo 'APIHost : '.$SYS_CONFIG['Mail']['APIHost'].'<br>';
			// echo 'APIPort : '.$SYS_CONFIG['Mail']['APIPort'].'<br>';
			// echo 'CurUserAddress : '.$this->CurUserAddress.'<br>';
			$this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);
			$UsedQuotaPath = $this->mail_api_url['GetUsedQuotaApi'];
	        $UsedQuotaPath .= "?login=".urlencode($this->CurUserAddress);
			if ($this->actype != "")
	        {
				$UsedQuotaPath .= "&actype=".$this->actype;
	        }
			
			$UsedQuota = $this->getWebPage($UsedQuotaPath);
			$Returned['UsedQuota'] = str_ireplace('<br>','',trim(str_ireplace('result: ->','',$UsedQuota)));
			
			$TotalQuotaPath = $this->mail_api_url['GetTotalQuotaApi'];
	        $TotalQuotaPath .= "?login=".urlencode($this->CurUserAddress);
			if ($this->actype != "")
	        {
				$TotalQuotaPath .= "&actype=".$this->actype;
	        }
			$TotalQuota = $this->getWebPage($TotalQuotaPath);
			
			$Returned['TotalQuota'] = str_ireplace('<br>','',trim(str_ireplace('result: ->','',$TotalQuota)));
			
			if ($Returned['TotalQuota'] > 0) {
				$Returned['Percent'] = round($Returned['UsedQuota'] / $Returned['TotalQuota'] *100 ); 
			}
			
			return $Returned;
		}
      # Supporting function
      function executePost($action_url, $post_data)
      {

               # Try to connect to service host
                $lhttp = new httpclient($this->remote_server, $this->remote_server_port);
                if (!$lhttp->connect())    # Exit if failed to connect
                return false;
                $lhttp->set_path($action_url);
                $lhttp->post_data = $post_data;
                $response = $lhttp->send_request();
                $pos = strpos($response,"\r\n\r\n");
                $response = substr($response,$pos+4);
                
                return $response;
        }
		
      ###### End of new functions


        function Conver_Encoding($OriginText, $TargetEncode, $OriginEncode)
        {
                if ($OriginEncode == 'default')
                {
                        $ReturnX = mb_convert_encoding($OriginText, $TargetEncode);
                } else
                {
                        if ($OriginEncode!="" && !strstr($OriginEncode, "unknown"))
                        {
                                $ReturnX = mb_convert_encoding($OriginText,$TargetEncode,$OriginEncode);
                        } else
                        {
                                $ReturnX = $OriginText;
                        }
                }

                return $ReturnX;
        }

        function Close_Connect() {
                return imap_close($this->inbox);
        }

        function Get_UID($MessageID) {
                return @imap_uid($this->inbox,$MessageID);
        }

        function Get_MessageID($UID) {
                return @imap_msgno($this->inbox,$UID);
        }
        
        /*
          List the emails in the folder
          Param: folderName - listing the folder
                 startMsgNumber - retrieve email starting from startMsgNumber
                 count - how many to return
                 sort - sorting criteria
          Return: Array()
                    - Subject
                    - From
                    - To
                    - Date
                    - UID
                    - size
                    - msgno
                    - recent
                    - answered
                    - deleted
                    - seen
                    - [Pls check whether reply / forward / reply all]
        */
        function Get_Mail_In_Folder($folderName, $MsgNumber)
        {
        	global $Lang;

          //imap_reopen($this->inbox, $this->getMailServerRef().$this->encodeFolderName($folderName));

					$TempObj = imap_headerinfo($this->inbox,$MsgNumber);
					$RawHeader = imap_fetchheader($this->inbox,$MsgNumber);
	        
					$fromList = $TempObj->from;
          for ($j=0; $j< 1; $j++) {
						if (!isset($fromList[$j]->personal)) $DisplayName = $fromList[$j]->mailbox."@".$fromList[$j]->host;
						else {
							$DisplayObject = imap_mime_header_decode(imap_utf8($fromList[$j]->personal));
							$DisplayName = $DisplayObject[0]->text;
						}
						
						$Address = $fromList[$j]->mailbox."@".$fromList[$j]->host;
						
						$OverviewArray['From'] .= $DisplayName;
					}

					$toList = $TempObj->to;
					if (sizeof($toList) > 0) {
						for ($j=0; $j< 1; $j++) {
							if (!isset($toList[$j]->personal)) $DisplayName = $toList[$j]->mailbox."@".$toList[$j]->host;
							else {
								$DisplayObject = imap_mime_header_decode(imap_utf8($toList[$j]->personal));
								$DisplayName = $DisplayObject[0]->text;
							}
							
							$Address = $toList[$j]->mailbox."@".$toList[$j]->host;
							
							$OverviewArray['To'] .= $DisplayName;
						}
						if (sizeof($toList) > 1) {
							$OverviewArray['To'] .= '...';
						}
					}
					else {
						$OverviewArray['To'] = $Lang['email']['NoReceiver'];
					}
					
					/*$ccList = $TempObj->cc;
          for ($j=0; $j< sizeof($ccList); $j++) {
						if (!isset($ccList[$j]->personal)) $DisplayName = $ccList[$j]->mailbox."@".$ccList[$j]->host;
						else {
							$DisplayObject = imap_mime_header_decode(imap_utf8($ccList[$j]->personal));
							$DisplayName = $DisplayObject[0]->text;
						}
						
						$Address = $ccList[$j]->mailbox."@".$ccList[$j]->host;
						
						$OverviewArray['Cc'] .= $DisplayName." ".$Address;
						$OverviewArray['Cc'] .= ";";
					}
					
					$bccList = $TempObj->bcc;
          for ($j=0; $j< sizeof($bccList); $j++) {
						if (!isset($bccList[$j]->personal)) $DisplayName = $bccList[$j]->mailbox."@".$bccList[$j]->host;
						else {
							$DisplayObject = imap_mime_header_decode(imap_utf8($bccList[$j]->personal));
							$DisplayName = $DisplayObject[0]->text;
						}
						
						$Address = $bccList[$j]->mailbox."@".$bccList[$j]->host;
						
						$OverviewArray['Bcc'] .= $DisplayName." ".$Address;
						$OverviewArray['Bcc'] .= ";";
					}*/
					
					$Date = substr($TempObj->date, 0, -5);
        	$OverviewArray['DateReceived'] = $Date;
					$OverviewArray['DateSort'] = $TempObj->udate;

          // Get Priority
          $MailPriority = $this->Get_Header_Value($RawHeader,"x-priority");
          $OverviewArray['Priority'] = ($MailPriority == 1)? $MailPriority:0;

          // replace NO SUBJECT if subject is empty
          $SubjectObj = imap_mime_header_decode(imap_utf8($TempObj->subject));
          unset($subtmp);
          for($k=0;$k<count($SubjectObj);$k++) {
          	$subtmp .= $SubjectObj[$k]->text;
          }
          $Subject = ($subtmp == "")? "NO SUBJECT":$subtmp;
          $OverviewArray['Subject'] = $Subject;

          // check if mail had attachment
          /*$OverviewArray['HaveAttachment'] = false;
          $MailStructure = imap_fetchstructure($this->inbox,$TempObj->Msgno);
          $MailParts = $MailStructure->parts;
          for ($j=0; $j< sizeof($MailParts); $j++) {
            if ($MailParts[$j]->disposition == "attachment") {
              $OverviewArray['HaveAttachment'] = true;
              $AttachmentPart = $MailParts[$j]->dparameters;
              $Attachment = imap_mime_header_decode(imap_utf8($AttachmentPart[0]->value));
              $OverviewArray['Attachment'][] = $Attachment[0]->text;
            }
          }*/
          // check if mail had attachment
	        $ContentType = $this->Get_Header_Value($RawHeader,'Content-Type');
	        $OverviewArray['HaveAttachment'] = (preg_match("/multipart\/m/i", $ContentType) == 0)? false:true;
					
          // Get UID
          $OverviewArray['UID'] = $this->Get_UID($TempObj->Msgno);

          // Get Mail size
          $OverviewArray['Size'] = round(($TempObj->Size/1024),2);

          // Get Flag
          $OverviewArray["recent"] = ($TempObj->Recent == "R" || $TempObj->Recent == "N")? true:false;
          $OverviewArray["flagged"] = ($TempObj->Flagged == "F")? true:false;
          $OverviewArray["answered"] = ($TempObj->Answered == "A")? true:false;
          $OverviewArray["deleted"] = ($TempObj->Deleted == "D")? true:false;
          $OverviewArray["seen"] = ($TempObj->Unseen == "U" || $TempObj->Recent == "N")? false:true;
          $OverviewArray["draft"] = ($TempObj->Draft == "X")? true:false;

          // Get Folder Name
          $OverviewArray["Folder"] = $folderName;
					
					// Get Mail Body
					/*unset($this->parsed_part);
        	$this->parse_message($MailStructure);
        	$text_message = $this->retrieve_textmessage($this->inbox, $TempObj->Msgno, $MailStructure);
        	
        	if ($text_message == "") {
        		$OverviewArray["Body"] = Remove_HTML_Tags($this->retrieve_message($this->inbox, $TempObj->Msgno, $this->parsed_part));
          }
          else {
          	$OverviewArray["Body"] = $text_message;
          }*/
		              
          return $OverviewArray;
        }
        
      	function Get_Header_Value($RawHeader,$Flag) {
      		$Flag = strtolower($Flag);
          $TextHeader = $RawHeader;
					
          $TempHeader = trim(str_replace(array("\r\n", "\n\t", "\n "),array("\n", ' ', ' '), $TextHeader));
          $TempHeader = explode("\n" , $TempHeader);
          for ($i=0; $i< sizeof($TempHeader); $i++) {
                  $pos = strpos($TempHeader[$i], ':');
            if ($pos > 0) {
              $field = strtolower(substr($TempHeader[$i], 0, $pos));
              if (!strstr($field,' ')) { /* valid field */
                $value = trim(substr($TempHeader[$i], $pos+1));
                switch($field) {
                  case 'x-priority':
                                $aMsg['x-priority'] = ($value) ? (int) $value: 3;
                                break;
                  case 'priority':
                  case 'importance':
	                  if (!isset($aMsg['x-priority'])) {
	                    $aPrio = split('/\w/',trim($value));
	                    $sPrio = strtolower(array_shift($aPrio));
	                    if  (is_numeric($sPrio)) {
	                        $iPrio = (int) $sPrio;
	                    } elseif ( $sPrio == 'non-urgent' || $sPrio == 'low' ) {
	                        $iPrio = 3;
	                    } elseif ( $sPrio == 'urgent' || $sPrio == 'high' ) {
	                        $iPrio = 1;
	                    } else {
	                        // default is normal priority
	                        $iPrio = 3;
	                    }
	                    $aMsg['x-priority'] = $iPrio;
	                  }
	                  break;
                  case 'subject':
                        $aMsg['subject'] = imap_mime_header_decode(imap_utf8($value));
                        break;
                        default:
                  	break;
                  case 'content-type':
                  	$aMsg['content-type'] = $value;
                  	break;
                }
              }
            }
          }

          return $aMsg[$Flag];
      	}
}
}        // End of directive
?>
