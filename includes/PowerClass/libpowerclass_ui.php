<?php
if (!class_exists("libpowerclass", false)) {
	include_once($intranet_root."/includes/PowerClass/libpowerclass.php");
}
class libpowerclass_ui extends libpowerclass {
	public function __construct() {
		parent::__construct();
		$this->view_path = dirname(dirname(dirname(__FILE__))) . "/templates/" . $this->config['Project_Label']. "/views/";
	}

	public function getViewPath() {
		return $this->view_path;
	}
	
	public function getTopMenuHTML($args)
	{
		global $li_hp, $CurrentPageArr, $lhomework;
		extract($args);
		$dhl_menu = $this->getTopMenu($Lang, $CurrentPageArr);
		unset($args);

		include_once($this->view_path. "top_menu.php");

		unset($source_path);
		unset($dhl_menu);
		return $topMenuCustomization;
	}
	
	public function getPortalHTML($args) {
		if (is_array($args)) extract($args);
		$dhl_menu = $this->getTopMenu($Lang, $CurrentPageArr);
		unset($args);
		include_once($this->view_path. "portal_main.php");
		unset($source_path);
		unset($dhl_menu);
		// return $topMenuCustomization;
	}
	
	public function loadView($pagename = "", $args) {
		if (is_array($args)) extract($args);
		unset($args);
		if (file_exists($this->view_path. strtolower($pagename) . ".php")) include_once($this->view_path. strtolower($pagename) . ".php");
		unset($source_path);
		unset($dhl_menu);
		// return $custInlineHTML;
	}
	
	public function curlForPowerLession20() {
		global $luser, $intranet_root, $web_protocol, $session_expiry_time, $eclass40_httppath, $intranet_session_language;
		echo "--";
		if (!isset($luser)) {
			if (!class_exists("libuser", false)) {
				include_once($intranet_root."/includes/libuser.php");
			}
			$luser = new libuser($_SESSION['UserID']);
		}
		$sessionKeepAliveTime = $session_expiry_time * 60;
		$memberType = ($luser->RecordType == USERTYPE_STAFF)? 'T' : 'S';
		
		if (isset($web_protocol) && preg_match('/^https?$/', $web_protocol)===1){
			$protocol = $web_protocol;
		} else {
			$protocol = 'http';
		}
		$powerLesson2_path = $protocol . '://' . $eclass40_httppath;
		$powerLesson2_path = trim($powerLesson2_path, '/')."/src/powerlesson/portal/login.php?enableUrlRedirect=1";
		
		$defaultLang = '';
		switch($intranet_session_language){
			case 'b5':
				$defaultLang = 'zh-hk';
				break;
			case 'gb':
				$defaultLang = 'zh-cn';
				break;
			default:
				$defaultLang = 'en';
		}
		
		$platformType    = 'intranet';
		$platformVersion = Get_IP_Version();
		$target = "";
		if ($newWindow) {
			$target = " target='PowerLesson20' ";
		}
		$PowerLesson2 = '
    	<form id="powerLessonLogin" name="powerLessonLogin" style="display:none;" action="' . $powerLesson2_path . '" ' . $target . ' method="POST">
	        <input type="hidden" name="sessionKey" value="' . $luser->sessionKey . '" />
        	<input type="hidden" name="sessionKeepAliveTime" value="'. $sessionKeepAliveTime .'" />
        	<input type="hidden" name="defaultLang" value="' . $defaultLang .'" />
        	<input type="hidden" name="platformType" value="' . $platformType . '" />
        	<input type="hidden" name="platformVersion" value="'.$platformVersion.'" />
    	</form>';
		echo $PowerLesson2;
		#####################################
		# PowerLesson2 (PL2.0) Logo [End]
		#####################################
	}
	
	public function getPowerLesson2HTML($newWindow = true) {
		global $luser, $intranet_root, $web_protocol, $session_expiry_time, $eclass40_httppath, $intranet_session_language;
		if (!isset($luser)) {
			if (!class_exists("libuser", false)) {
				include_once($intranet_root."/includes/libuser.php");
			}
			$luser = new libuser($_SESSION['UserID']);
		}
		$sessionKeepAliveTime = $session_expiry_time * 60;
		$memberType = ($luser->RecordType == USERTYPE_STAFF)? 'T' : 'S';
		
		if (isset($web_protocol) && preg_match('/^https?$/', $web_protocol)===1){
			$protocol = $web_protocol;
		} else {
			$protocol = 'http';
		}
		$powerLesson2_path = $protocol . '://' . $eclass40_httppath;
		$powerLesson2_path = trim($powerLesson2_path, '/')."/src/powerlesson/portal/login.php?enableUrlRedirect=1";
		
		$defaultLang = '';
		switch($intranet_session_language){
			case 'b5':
				$defaultLang = 'zh-hk';
				break;
			case 'gb':
				$defaultLang = 'zh-cn';
				break;
			default:
				$defaultLang = 'en';
		}
		
		$platformType    = 'intranet';
		$platformVersion = Get_IP_Version();
		$target = "";
		if ($newWindow) {
			$target = " target='PowerLesson20' ";
		}
		$PowerLesson2 = '
    	<form id="powerLessonLogin" name="powerLessonLogin" style="display:none;" action="' . $powerLesson2_path . '" ' . $target . ' method="POST">
	        <input type="hidden" name="sessionKey" value="' . $luser->sessionKey . '" />
        	<input type="hidden" name="sessionKeepAliveTime" value="'. $sessionKeepAliveTime .'" />
        	<input type="hidden" name="defaultLang" value="' . $defaultLang .'" />
        	<input type="hidden" name="platformType" value="' . $platformType . '" />
        	<input type="hidden" name="platformVersion" value="'.$platformVersion.'" />
    	</form>';
		#####################################
		# PowerLesson2 (PL2.0) Logo [End]
		#####################################
		return $PowerLesson2;
	}

	/* create function loadFooter($template){ switch defult or cust template footer*/
	public function loadFooter($source_path, $template=''){
	    switch($template){
	        case "custom": 
	            return '<div id="lbleClass"><a href="http://eclass.com.hk" title="PowerClass" target="_blank"><img id="imgPC" src="'.$source_path.'images/powerClassLogo_w.png"></a><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="'.$source_path.'images/eClassLogo_w.png"></a></div>';
	            break;
	        default:
	            return '<div id="lbleClass"><span id="lblBL">BroadLearning</span><span>'.$Lang['Footer']['PowerBy'].'</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="'.$source_path.'images/eClassLogo.png"></a></div>';
	    }
	}
}