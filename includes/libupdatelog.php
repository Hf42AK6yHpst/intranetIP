<?php
// Modifying by : 

/***************************************
 * Date: 	2015-08-14 (Omas)
 * Details: add LOG_IMPORT_FILE() to log large data into a file
 * 
 * Date: 	2013-01-23 (Rita)
 * Details: create this page for update log
 **************************************/


class libupdatelog extends libdb 
{
	function libupdatelog()
	{
		$this->libdb();
	}
	    
	function BUILD_UPDATE_DETAIL($ary=array())
	{
		$str = "";
		$br = "";
		if(!empty($ary))
		{
			foreach($ary as $k=>$d)
			{
				$str .= $br . str_replace("_", " ", $k) . ": ".$d;
				$br = "<br>";
			}
		}
		return $str;
	}
	
	function INSERT_UPDATE_LOG($Module='', $Section='', $RecordDetail='', $TableName='', $RecodID='')
	{
		if($Module)
		{
			if(empty($RecordDetail))
				$RecordDetail = "";
			else if(is_array($RecordDetail))
				$RecordDetail = $this->BUILD_DETAIL($RecordDetail);
				
			$RecodID = $RecodID ? $RecodID : NULL;
			$sql = "insert into MODULE_RECORD_UPDATE_LOG 
					(Module, Section, RecordDetail, UpdateTableName, UpdateRecordID, LogDate, LogBy)
					values
					('$Module', '$Section', '".$this->Get_Safe_Sql_Query($RecordDetail)."', '$TableName', '$RecodID', now(), '".$_SESSION['UserID']."')
					";
			return $this->db_db_query($sql) or die(mysql_error());
		}
	}    
	
	function DELETE_UPDATE_LOG($Module='', $StartDate='', $EndDate='')
	{
		if($Module && $StartDate && $EndDate)
		{
			$sql = "delete from 
						MODULE_RECORD_UPDATE_LOG 
					where
						Module = '". $Module ."' and 
						left(LogDate,10) >= '". $StartDate ."' and 
						left(LogDate,10) <= '". $EndDate ."'
					";	
			return $this->db_db_query($sql) or die(mysql_error());
		}
	}
	
	function LOG_IMPORT_FILE($data,$path){
		global $PATH_WRT_ROOT;
		
		if(count($data)>0){
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			$lexport = new libexporttext();
			$export_content = $lexport->GET_EXPORT_TXT($data, array());
			$filename = $_SESSION['UserID'].'_'.date('YmdHis');
			//e.g. $_SERVER['DOCUMENT_ROOT'].'/file/logfiles/accountMgmt';
			$filepath = $path;
			if(!file_exists($filepath)){
				mkdir($filepath,0777,true);
			}
			$file = fopen($filepath.'/'.$filename,"w");
			fwrite($file,$export_content);
			fclose($file);
			
			return $filename;
		}
		else{
			return false;
		}
	}
	    
}
?>