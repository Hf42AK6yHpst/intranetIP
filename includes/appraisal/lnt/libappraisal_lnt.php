<?php
// editing by 
/**
 * Change Log:
 * 2018-01-05 Pun
 *  - Modified getAnswerByAcademicYearIdSubjectIdTeacherId(), added order by
 */
if (!defined("LIBAPPRAISAL_LNT_DEFINED")) {
define("LIBAPPRAISAL_LNT_DEFINED", true);

class libappraisal_lnt extends libdb {
    const LNT_IMPORT_TYPE_QUESTION = 'QUESTION';
    const LNT_IMPORT_TYPE_ANSWER = 'ANSWER';
    
    const LNT_QUESTION_TYPE_SCALE = 'SCALE';
    const LNT_QUESTION_TYPE_TEXT = 'TEXT';
    
    ######################################## Question START ########################################
    function getAllQuestionByAcademicYearID($AcademicYearID){
        $AcademicYearIdSql = implode("','", (array)$AcademicYearID);
        $sql = "SELECT
            P.RecordID AS PartID,
            P.Part,
            P.Title AS PartTitle,
            P.Sequence AS PartSequence,
            Q.RecordID AS QuestionID,
            Q.QuestionNumber,
            Q.Title AS QuestionTitle,
            Q.QuestionType,
            Q.MinScore,
            Q.MaxScore,
            Q.Sequence AS QuestionSequence,
            Q.AcademicYearID,
            Q.SubjectID
        FROM
            INTRANET_PA_LNT_QUESTION Q
        INNER JOIN
            INTRANET_PA_LNT_QUESTION_PART P
        ON
            Q.PartID = P.RecordID
        WHERE
            Q.AcademicYearID IN ('{$AcademicYearIdSql}')
        ORDER BY
            P.Sequence, Q.Sequence
        ";
        $rs = $this->returnResultSet($sql);
        
        return $rs;
    }
    
    function getAllQuestionCount(){
        $sql = "SELECT AcademicYearID, COUNT(*) AS COUNT_QUESTION FROM INTRANET_PA_LNT_QUESTION GROUP BY AcademicYearID";
        $rs = $this->returnResultSet($sql);
        
        $countArr = BuildMultiKeyAssoc($rs, array('AcademicYearID') , array('COUNT_QUESTION'), $SingleValue=1);
        
        return $countArr;
    }
    ######################################## Question END ########################################
    
    ######################################## Answer START ########################################
    function getTeacherListByAcademicYearIdSubjectId($AcademicYearID, $SubjectID){
        $cond = '';
        if($AcademicYearID){
            $AcademicYearIdSql = implode("','", (array)$AcademicYearID);
            $cond .= " AND IPLA.AcademicYearID IN ('{$AcademicYearIdSql}')";
        }
        if($SubjectID){
            $SubjectIDSql = implode("','", (array)$SubjectID);
            $cond .= " AND IPLA.SubjectID IN ('{$SubjectIDSql}')";
        }

        $username_field = getNameFieldWithClassNumberByLang("IU.");
        $sql = "SELECT DISTINCT
            IPLA.TeacherID,
            $username_field AS TeacherName
        FROM
            INTRANET_PA_LNT_ANSWER IPLA
        INNER JOIN
            INTRANET_USER IU
        ON
            IPLA.TeacherID = IU.UserID
        WHERE
            1=1
            {$cond}
		ORDER BY
			TRIM(IU.EnglishName)";
        $rs = $this->returnArray($sql);
        return $rs;
    }
    
    function getAnswerByAcademicYearIdSubjectIdTeacherId($AcademicYearID='', $SubjectID='', $TeacherID=''){
        $cond = '';
        if($AcademicYearID){
            $AcademicYearIdSql = implode("','", (array)$AcademicYearID);
            $cond .= " AND LA.AcademicYearID IN ('{$AcademicYearIdSql}')";
        }
        if($SubjectID){
            $SubjectIDSql = implode("','", (array)$SubjectID);
            $cond .= " AND LA.SubjectID IN ('{$SubjectIDSql}')";
        }
        
        if($TeacherID){
            $TeacherIDSql = implode("','", (array)$TeacherID);
            $cond .= " AND LA.TeacherID IN ('{$TeacherIDSql}')";
        }
        
        $sql = "SELECT
            LA.*
        FROM
            INTRANET_PA_LNT_ANSWER LA
        INNER JOIN
            ACADEMIC_YEAR AY
        ON
            LA.AcademicYearID = AY.AcademicYearID
        INNER JOIN
            YEAR_CLASS YC
        ON
            LA.YearClassID = YC.YearClassID
        INNER JOIN
            YEAR Y
        ON
            YC.YearID = Y.YearID
        WHERE
            1=1
            {$cond}
        ORDER BY    
            AY.Sequence, Y.Sequence, YC.Sequence";
            
        $rs = $this->returnResultSet($sql);
        return $rs;
    }
    ######################################## Answer END ########################################
    
    ######################################## Helper function START ########################################

    function getAllGeneralSubjectId($AcademicYearID=''){
        $sql = "SELECT RecordID AS SubjectID FROM ASSESSMENT_SUBJECT WHERE (CMP_CODEID = '' OR CMP_CODEID IS NULL) AND RecordStatus='1' ";
        $allSubjectId = $this->returnVector($sql);
        
        $cond = '';
        if($AcademicYearID){
            $AcademicYearIdSql = implode("','", (array)$AcademicYearID);
            $cond .= " AND AcademicYearID IN ('{$AcademicYearIdSql}')";
        }

        $sql = "SELECT SubjectID FROM INTRANET_PA_LNT_QUESTION_PART WHERE SubjectID IS NOT NULL $cond";
        $currentQuestionSubjectIdArr = $this->returnVector($sql);
        
        $sql = "SELECT SubjectID FROM INTRANET_PA_LNT_ANSWER WHERE 1=1 $cond";
        $currentAnswerSubjectIdArr = $this->returnVector($sql);
        
        $normalQuestionSubjectIdArr = array_diff($allSubjectId, $currentQuestionSubjectIdArr);
        $normalQuestionSubjectIdArr = array_intersect($normalQuestionSubjectIdArr, $currentAnswerSubjectIdArr);
        
        return $normalQuestionSubjectIdArr;
    }
    
    function sd($array) {
        if(count($array) > 1){
            return sqrt(array_sum(array_map(array($this, 'sd_square'), $array, array_fill(0,count($array), (array_sum($array) / count($array)) ) ) ) / (count($array)-1) );
        }
        return 0;
    }

    function sd_square($x, $mean) {
        return pow($x - $mean,2);
    }
    ######################################## Helper function END ########################################
    
} // End Class
}
