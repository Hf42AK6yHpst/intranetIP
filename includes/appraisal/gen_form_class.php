<?php
// Editing by: Paul
class genform extends libdb {
    var $ModuleTitle;
    var $ColForPreSetField = 10;
    var $GeneralSetting;
    
    function genform() {
        global $appraisalConfig,$Lang;
        
        $this->libdb();
        $this->ModuleTitle = $appraisalConfig['moduleCode'];
    }
    function exeForm($connection,$templateID,$userID,$recordID,$cycleID,$rlsNo,$batchID, $isPrint=false, $isCombinePrint=false){
        global $appraisalConfig;
        /* supported combination:
         * 1. $templateID: preview templates
         * 2. $userID: list all reports belong to that user
         * 3. $userID & $cycleID: list those reports that belong to that user for the specific period
         * 4. $recordID: list the report information itself
         */
        $this->GeneralSetting = $connection->Get_General_Setting("TeacherApprisal");
        $indexVar['viewScript'] = 'views/'.str_replace($appraisalConfig['taskSeparator'], '/', $indexVar['task']).'.tmpl.php';
        $content = "";
        $x = "";
        if($templateID!="" && $cycleID==""){
            $sql="SELECT TemplateType FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".IntegerSafe($templateID)."';";
            //echo $sql."<br/><br/>";
            $a=$connection->returnResultSet($sql);
            if($a[0]["TemplateType"]==0){
                $x .= $this->getPreForm($connection,$templateID,$userID,$recordID,$cycleID,$rlsNo,$batchID);
                
            }
            else if($a[0]["TemplateType"]==1){
                $x .= $this->getSdfForm($connection,$templateID,$userID,$recordID,$cycleID,$rlsNo,$batchID);
                
            }
        }
        else if($templateID!="" && $cycleID!=""){
            $sql="SELECT TemplateType FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".IntegerSafe($templateID)."';";
            //echo $sql."<br/><br/>";
            $a=$connection->returnResultSet($sql);
            if($a[0]["TemplateType"]==0){
                $x .= $this->getPreFormPrint($connection,$templateID,$userID,$recordID,$cycleID,$rlsNo,$batchID, $isPrint);
                
            }
            else if($a[0]["TemplateType"]==1){
                $x .= $this->getSdfFormPrint($connection,$templateID,$userID,$recordID,$cycleID,$rlsNo,$batchID, $isPrint, $isCombinePrint);
                
            }
        }
        return $x;
    }
    function getPreForm($connection,$templateID,$userID,$recordID,$cycleID,$rlsNo,$batchID){
        $indexVar['libappraisal'] = new libappraisal();
        $indexVar['libappraisal_ui'] = new libappraisal_ui();
        global $intranet_session_language, $Lang, $ColForPreSetField, $appraisalConfig, $sys_custom;
        
        if($userID==""&&$recordID==""&&$cycleID==""&&$rlsNo==""&&$batchID==""){
            $sql="SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormTitle,"
                .$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode,"
                    .$indexVar['libappraisal']->getLangSQL("HdrRefChi","HdrRefEng")." as HdrRef,"
                        .$indexVar['libappraisal']->getLangSQL("ObjChi","ObjEng")." as Obj,"
                            .$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,
				AppTgtChi,AppTgtEng
				FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".IntegerSafe($templateID)."'";
        }
        else{
            $sql="SELECT iptf.RecordID,UserID,iptf.CycleID,CycleDescr,iptf.TemplateID,FormTitle,FormCode,HdrRef,Obj,Descr,AppTgtChi,AppTgtEng,AcademicYearID,COALESCE(SubDate,'') as SubDate,
					EditPrdFr,EditPrdTo,CASE WHEN NOW()>EditPrdTo THEN '1' ELSE '0' END as OverdueDate,RlsNo
					FROM(
						SELECT RecordID,CycleID,TemplateID,UserID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".IntegerSafe($recordID)."'
					) as iptf
					INNER JOIN(
						SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormTitle,"
						    .$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode,"
						        .$indexVar['libappraisal']->getLangSQL("HdrRefChi","HdrRefEng")." as HdrRef,"
						            .$indexVar['libappraisal']->getLangSQL("ObjChi","ObjEng")." as Obj,"
						                .$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,
						AppTgtChi,AppTgtEng
						FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".IntegerSafe($templateID)."'
					) as ipsf ON iptf.TemplateID=ipsf.TemplateID
					INNER JOIN(
						SELECT CycleID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as CycleDescr,AcademicYearID
						FROM INTRANET_PA_C_CYCLE
					) as ipcc ON iptf.CycleID=ipcc.CycleID
					INNER JOIN(
						SELECT BatchID,CycleID,EditPrdFr,EditPrdTo FROM INTRANET_PA_C_BATCH
					) as ipcb ON ipcc.CycleID=ipcb.CycleID
					INNER JOIN(
						SELECT RlsNo,RecordID,BatchID,SubDate FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
					) as iptfrmSub ON iptf.RecordID=iptfrmSub.RecordID AND ipcb.BatchID=iptfrmSub.BatchID
				";
        }
        //echo $sql."<br/><br/>";
        $a=$connection->returnResultSet($sql);
        //======================================================================== Pre-defined form header ========================================================================//
        $sql = "SELECT TemplateID,DataTypeID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,DisplayOrder,Incl
			FROM INTRANET_PA_S_FRMPRE WHERE TemplateID='".IntegerSafe($templateID)."'
			ORDER BY DataTypeID,DisplayOrder
			";
        //echo $sql."<br/><br/>";
        $preDefHeader=$connection->returnResultSet($sql);
        $cycleID = $a[0]["CycleID"];
        $userID = $a[0]["UserID"];
        $academicYearID=$a[0]["AcademicYearID"];
        
        $x .= "<div class=\"formTitle\">".$a[0]["FormTitle"];
        if($a[0]["CycleDescr"]!=""){
            $x .= "(".$a[0]["CycleDescr"].")";
        }
        $x .= "</div><br/>"."\r\n";
        $x .= "<div>".
            "<span class=\"formObjective\" style=\"float: left;\">".$a[0]["FormCode"]." - ".$a[0]["Obj"]."</span>".
            "<span class=\"formRefHeader\" style=\"float: right;\">".$a[0]["HdrRef"]."</span>".
            "</div><br/><br/>"."\r\n";
        $x .= "<div>".$a[0]["Descr"]."</div><br/>"."\r\n";
        //======================================================================== Personal Master ========================================================================//
        $sql = "SELECT UserID,EnglishName,ChineseName,Gender
			FROM(
				SELECT UserID,EnglishName,ChineseName,Gender FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".IntegerSafe($a[0]["UserID"])."'
			) as iu";
        $iu=$connection->returnResultSet($sql);
        $tgtName = ($_SESSION['intranet_session_language']=="en")?$Lang['Appraisal']['ARFormTgtName']." ".$a[0]["AppTgtEng"]:$a[0]["AppTgtChi"].$Lang['Appraisal']['ARFormTgtName'];
        $x .= "<table class=\"form_table_v30\">";
        $x .= "<tr><td class=\"field_title\">".$tgtName."</td><td>".$iu[0]["EnglishName"];
        if($iu[0]["ChineseName"]!=""){
            $x .= "(".$iu[0]["ChineseName"].")";
        }
        $x .= "</td></tr>"."\r\n";
        $x .= "</table><br/>"."\r\n";
        //======================================================================== Personal Pariculars ========================================================================//
        if($preDefHeader[0]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,NameChi,NameEng,Gender,DATE_FORMAT(DateOfBirth, '%Y-%m-%d') as DateOfBirth,Religion,MaritalStatus,AddressChi,AddressEng,PhoneNo,Publication,ExtServ,TotClsCnt
			FROM INTRANET_PA_T_PRE_GEN WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			";
            $perPar=$connection->returnResultSet($sql);
            $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr, Incl, DataSubTypeID FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID='".IntegerSafe($templateID)."' AND DataTypeID=1 ORDER BY DisplayOrder";
            $subSectionPar = $connection->returnResultSet($sql);
            $gender = ($perPar[0]["Gender"]=="M")?$Lang['Appraisal']['ARFormMale']:(($perPar[0]["Gender"]=="F")?$Lang['Appraisal']['ARFormFemale']:"");
            $x0 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[0]["Descr"])."<br/>\r\n";
            $x0 .= "<table class=\"form_table_v30\">";
            foreach($subSectionPar as $subsection){
                if($subsection['Incl']==false)continue;
                switch($subsection['DataSubTypeID']){
                    case 1:
                        $x0 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$perPar[0]["NameEng"]."</td></tr>"."\r\n";
                        break;
                    case 2:
                        $x0 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$perPar[0]["NameChi"]."</td></tr>"."\r\n";
                        break;
                    case 3:
                        $x0 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$gender."</td></tr>"."\r\n";
                        break;
                    case 4:
                        $x0 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_DATE_PICKER('DateOfBirth',$perPar[0]["DateOfBirth"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('DateOfBirth')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum")."</td></tr>"."\r\n";
                        break;
                    case 5:
                        $x0 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('Religion', 'Religion', $perPar[0]["Religion"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                    case 6:
                        $x0 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('MaritalStatus', 'MaritalStatus', $perPar[0]["MaritalStatus"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                    case 7:
                        $x0 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('AddressChi', 'AddressChi', $perPar[0]["AddressChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                    case 8:
                        $x0 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('AddressEng', 'AddressEng', $perPar[0]["AddressEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                    case 9:
                        $x0 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('PhoneNo', 'PhoneNo', $perPar[0]["PhoneNo"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                }
            }
            
            $x0 .= "</table><br/>"."\r\n";
        }
        //======================================================================== Present Job Information ========================================================================//
        if($preDefHeader[1]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,JobGrade,DATE_FORMAT(GradeEntDate,'%Y-%m-%d') as GradeEntDate,DATE_FORMAT(HireDateSch,'%Y-%m-%d') as HireDateSch,DATE_FORMAT(HireDateOrgn,'%Y-%m-%d') as HireDateOrgn,SenGov,SenPri,SenOrgn,RegNo,SalaryPoint,ActingPosition,LongHoliday,PositionOnEntry
			FROM INTRANET_PA_T_PRE_GEN WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            
            $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr, Incl, DataSubTypeID FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID='".IntegerSafe($templateID)."' AND DataTypeID=2 ORDER BY DisplayOrder";
            $subSectionPar = $connection->returnResultSet($sql);
            
            $x1 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[1]["Descr"])."<br/>\r\n";
            $x1 .= "<table class=\"form_table_v30\">";
            foreach($subSectionPar as $subsection){
                if($subsection['Incl']==false)continue;
                switch($subsection['DataSubTypeID']){
                    case 1:
                        $x1 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('jobGrade', 'jobGrade', $a[0]["JobGrade"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                    case 2:
                        $x1 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_DATE_PICKER('gradeEntranceDate',$a[0]["GradeEntDate"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('gradeEntranceDate')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum")."</td></tr>"."\r\n";
                        break;
                    case 3:
                        $x1 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_DATE_PICKER('hireDateSch',$a[0]["HireDateSch"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('hireDateSch')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum")."</td></tr>"."\r\n";
                        break;
                    case 4:
                        $x1 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_DATE_PICKER('hireDateOrgn',$a[0]["HireDateOrgn"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('hireDateOrgn')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum")."</td></tr>"."\r\n";
                        break;
                    case 5:
                        $x1 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('senGov', 'senGov', $a[0]["SenGov"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                    case 6:
                        $x1 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('senPri', 'senPri', $a[0]["SenPri"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                    case 7:
                        $x1 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('SenOrgn', 'SenOrgn', $a[0]["SenOrgn"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                    case 8:
                        $x1 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('RegNo', 'RegNo', $a[0]["RegNo"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                    case 9:
                        $x1 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('SalaryPoint', 'SalaryPoint', $a[0]["SalaryPoint"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                    case 10:
                        $x1 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('ActingPosition', 'ActingPosition', $a[0]["ActingPosition"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                    case 11:
                        if($a[0]["LongHoliday"] == ""){
                            $holidayStart = "";
                            $holidayEnd = "";
                            $holidayType = "";
                            $holidayDuration = "";
                        }else{
                            $longHolidayArr = unserialize($a[0]["LongHoliday"]);
                            $holidayStart = $longHolidayArr['holidayStart'];
                            $holidayEnd = $longHolidayArr['holidayEnd'];
                            $holidayType = $longHolidayArr['holidayType'];
                            $holidayDuration = $longHolidayArr['holidayDuration'];
                        }
                        $x1 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td>";
                        $x1 .= "<td>".$Lang['General']['From'].$indexVar['libappraisal_ui']->GET_DATE_PICKER('holidayStart',$holidayStart,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('holidayStart')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
                        $x1 .= $Lang['General']['To'].$indexVar['libappraisal_ui']->GET_DATE_PICKER('holidayEnd',$holidayEnd,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('holidayEnd')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum")."<br>\n\r";
                        $x1 .= $Lang['Appraisal']['ARFormNature'].":".$indexVar['libappraisal_ui']->GET_TEXTBOX('holidayType', 'holidayType', $holidayType, $OtherClass='', $OtherPar=array('maxlength'=>255))."\t\t";
                        $x1 .= $Lang['Appraisal']['CycleExportLeaveChi']['Days'].":".$indexVar['libappraisal_ui']->GET_TEXTBOX('holidayDuration', 'holidayDuration', $holidayDuration, $OtherClass='', $OtherPar=array('maxlength'=>255));
                        $x1 .= "</td>";
                        break;
                    case 12:
                        $x1 .= "<tr><td class=\"field_title\">".$subsection['Descr']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('PositionOnEntry', 'PositionOnEntry', $a[0]["PositionOnEntry"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                        break;
                }
            }
            
            $x1 .= "</table><br/>"."\r\n";
        }
        //======================================================================== Qualification Acquired/ Study-in-progress during appraising period ========================================================================//
        if($preDefHeader[2]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,SeqID,IsCompleted,DATE_FORMAT(IssueDate,'%Y-%m-%d') as IssueDate,Issuer,QualiDescr,Programme
			FROM INTRANET_PA_T_PRE_QUALI WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."';";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x2 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[2]["Descr"])."<br/>\r\n";
            
            $x2 .= "<div id=\"quaAcqInfoLayer\">";
            $x2 .= "<table id=\"quaAcqInfoTable\" class=\"common_table_list\">";
            $x2 .= "<thead>";
            $x2 .= "<tr><th style=\"width:2%\">".$Lang['Appraisal']['ARFormSeq']."</th><th style=\"width:20%\">".$Lang['Appraisal']['ARFormQuaAcqQuaDesc']."</th>";
            $x2 .= "<th style=\"width:16%\">".$Lang['Appraisal']['ARFormQuaAcqSts']."</th><th style=\"width:17%\">".$Lang['Appraisal']['ARFormQuaAcqCmpDat']."</th>";
            $x2 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormQuaAcqAccBody']."</th><th style=\"width:20%\">".$Lang['Appraisal']['ARFormQuaAcqPrgDtl']."</th>";
            $x2 .= "<th style=\"width:10%\"></th>";
            $x2 .= "</tr></thead>";
            $x2 .= "<tbody>";
            $runningIndex = 0;
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x2 .= "<tr id=\"qualificationRow_".$j."\">";
                $x2 .= "<td>";
                $x2 .= "<div id=\"seqID_".$a[$i]["SeqID"]."\" name=\"seqID_".$a[$i]["SeqID"]."\">";
                $x2 .= $a[$i]["SeqID"];
                $x2 .= "</div>";
                $x2 .= "</td>";
                $x2 .= "<td>";
                $x2 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('qualiDescr_'.$j, 'qualiDescr_'.$j, $a[$i]["QualiDescr"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x2 .= "</td>";
                $x2 .= "<td>";
                $x2 .= getSelectByAssoArray($indexVar['libappraisal']->cnvArrToSelect($Lang['Appraisal']['ARFormQuaAcqStsDes'],null), $selectionTags='id="quaAcqSts_'.$j.'" name="quaAcqSts_'.$j.'"', $SelectedType=$a[$i]["IsCompleted"], $all=0, $noFirst=0);
                $x2 .= "</td>";
                $x2 .= "<td>";
                $x2 .= $indexVar['libappraisal_ui']->GET_DATE_PICKER('issueDate_'.$j,$a[$i]["IssueDate"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('issueDate_".$j."')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
                $x2 .= "</td>";
                $x2 .= "<td>";
                $x2 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('issuer_'.$j, 'issuer_'.$j, $a[$i]["Issuer"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x2 .= "</td>";
                $x2 .= "<td>";
                $x2 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('programme_'.$j, 'programme_'.$j, $a[$i]["Programme"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x2 .= "</td>";
                $x2 .= "<td>";
                $x2 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                $x2 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteQualification('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                $x2 .= "</span>";
                $x2 .= "<input type=\"hidden\" id=\"hiddenQualification_".$j."\" name=\"hiddenQualification_".$j."\" VALUE=\"ACTIVE\">";
                $x2 .= "<input type=\"hidden\" id=\"hiddenSeqID_".$j."\" name=\"hiddenSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
                $x2 .= "</td>";
                $x2 .= "</tr>";
                $runningIndex++;
            }
            $x2 .= "<tr height=\"34px\"><td></td><td></td><td></td><td></td><td></td><td></td><td>";
            $x2 .= "<div class=\"table_row_tool row_content_tool\">";
            $x2 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addQualification('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
            $x2 .= "<input type=\"hidden\" id=\"currentQualificationRow\" name=\"currentQualificationRow\" VALUE=\"".sizeof($a)."\">";
            $x2 .= "<input type=\"hidden\" id=\"totalQualificationRow\" name=\"totalQualificationRow\" VALUE=\"".$runningIndex."\">";
            $x2 .= "</td></tr>";
            $x2 .= "</tbody>";
            $x2 .= "</table>";
            $x2 .= "</div><br/>";
            if($sys_custom['eAppraisal']['HidePublishingThisYear']!=true || $sys_custom['eAppraisal']['HideServiceOutsideSchool']!=true){
                $x2 .= "<table class=\"form_table_v30\">";
                if($sys_custom['eAppraisal']['HidePublishingThisYear']!=true){
                    $x2 .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormPubYear']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('pubYear','pubYear',$perPar[0]["Publication"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                }
                if($sys_custom['eAppraisal']['HideServiceOutsideSchool']!=true){
                    $x2 .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormExtServ']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('extServ','extServ',$perPar[0]["ExtServ"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
                }
                $x2 .= "</table><br/>";
            }
        }
        //======================================================================== Present Teaching Duties per Class ========================================================================//
        if($preDefHeader[3]["Incl"]==1){
            $x3 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[3]["Descr"])."<br/>\r\n";
            $x3 .= "<table class=\"form_table_v30\">";
            $x3 .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormTotClsCnt']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('totClsCnt','totClsCnt',$perPar[0]["TotClsCnt"],$OtherClass='',$OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
            $x3 .= "</table><br/>";
            if ($indexVar['libappraisal']->isEJ()) {
                $sql = "SELECT iptpt.RecordID,BatchID,iptpt.YearClassID,SubjectID,Period,DateTimeInput,IFNULL(SName,SubjectID) as SName,ClassTitle
						FROM(
							SELECT RecordID,BatchID,YearClassID,SubjectID,Period,DateTimeInput
							FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
						) as iptpt
						LEFT JOIN(
							SELECT SubjectID as RecordID, SubjectName as SName
							FROM INTRANET_SUBJECT WHERE RecordStatus = 1
						) as assub ON iptpt.SubjectID=assub.RecordID
						LEFT JOIN(
							SELECT ClassID as YearClassID, ClassName as ClassTitle
							FROM INTRANET_CLASS
						) as yc ON iptpt.YearClassID=yc.YearClassID
						ORDER BY DateTimeInput";
            }else{
                $sql = "SELECT iptpt.RecordID,BatchID,iptpt.YearClassID,SubjectID,Period,DateTimeInput,IFNULL(SName,SubjectID) as SName,ClassTitle
						FROM(
							SELECT RecordID,BatchID,YearClassID,SubjectID,Period,DateTimeInput
							FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
						) as iptpt
						LEFT JOIN(
							SELECT RecordID,CODEID,".$indexVar['libappraisal']->getLangSQL("CH_SNAME","EN_SNAME")." as SName
							FROM ASSESSMENT_SUBJECT
						) as assub ON iptpt.SubjectID=assub.RecordID
						LEFT JOIN(
							SELECT YearClassID,AcademicYearID,".$indexVar['libappraisal']->getLangSQL("ClassTitleB5","ClassTitleEN")." as ClassTitle
							FROM YEAR_CLASS
						) as yc ON iptpt.YearClassID=yc.YearClassID
						ORDER BY DateTimeInput";
            }
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $runningIndex = 0;
            
            //$x3 .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($preDefHeader[4]["Descr"])."<br/>\r\n";
            $x3 .= "<div id=\"teachingClassLayer\">";
            $x3 .= "<table id=\"teachingClassTable\" class=\"common_table_list\">";
            $x3 .= "<thead>";
            $x3 .= "<tr><th style=\"width:30%\">".$Lang['Appraisal']['ARFormCls']."</th><th style=\"width:30%\">".$Lang['Appraisal']['ARFormTeachSubj']."</th>";
            $x3 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormClsPerCyc']."</th>";
            $x3 .= "<th style=\"width:10%\"></th>";
            $x3 .= "</tr></thead>";
            $x3 .= "<tbody>";
            
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x3 .= "<tr id=\"teachClassRow_".$j."\">";
                $x3 .= "<td>".$indexVar['libappraisal']->displayChinese($a[$i]["ClassTitle"])."</td>";
                $x3 .= "<td >".$indexVar['libappraisal']->displayChinese($a[$i]["SName"])."</td>";
                $x3 .= "<td>";
                $x3 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('clsPerCyc_'.$j,'clsPerCyc_'.$j,$a[$i]["Period"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x3 .= "</td>";
                $x3 .= "<td>";
                $x3 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                $x3 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteClass('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                $x3 .= "</span>";
                $x3 .= "</td>";
                $x3 .= "<input type=\"hidden\" id=\"hiddenYearClass_".$j."\" name=\"hiddenYearClass_".$j."\" VALUE=\"".$a[$i]["YearClassID"]."\">";
                $x3 .= "<input type=\"hidden\" id=\"hiddenSubject_".$j."\" name=\"hiddenSubject_".$j."\" VALUE=\"".$a[$i]["SubjectID"]."\">";
                $x3 .= "<input type=\"hidden\" id=\"hiddenClass_".$j."\" name=\"hiddenClass_".$j."\" VALUE=\"ACTIVE\">";
                $x3 .= "</tr>";
                $runningIndex++;
            }
            $x3 .= "<tr height=\"34px\"><td></td><td></td><td></td><td>";
            $x3 .= "<div class=\"table_row_tool row_content_tool\">";
            $x3 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addClass('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
            $x3 .= "<input type=\"hidden\" id=\"currentClassRow\" name=\"currentClassRow\" VALUE=\"".sizeof($a)."\">";
            $x3 .= "<input type=\"hidden\" id=\"totalClassRow\" name=\"totalClassRow\" VALUE=\"".$runningIndex."\">";
            $x3 .= "</td></tr>";
            $x3 .= "</tbody>";
            $x3 .= "</table>";
            $x3 .= "</div><br/>";
        }
        //======================================================================== Present Teaching Duties per Form ========================================================================//
        if($preDefHeader[4]["Incl"]==1){
            $x4 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[4]["Descr"])."<br/>\r\n";
            $x4 .= "<table class=\"form_table_v30\">";
            $x4 .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormTotClsCnt']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('totFormCnt','totFormCnt',$perPar[0]["TotClsCnt"],$OtherClass='',$OtherPar=array('maxlength'=>255))."</td></tr>"."\r\n";
            $x4 .= "</table><br/>";
            if ($indexVar['libappraisal']->isEJ()) {
                $sql = "SELECT iptpt.RecordID,BatchID,iptpt.YearID,SubjectID,Period,DateTimeInput,SName,YearName
						FROM(
							SELECT RecordID,BatchID,YearID,SubjectID,Period,DateTimeInput
							FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
						) as iptpt
						INNER JOIN(
							SELECT SubjectID as RecordID, SubjectName as SName
							FROM INTRANET_SUBJECT  WHERE RecordStatus = 1
						) as assub ON iptpt.SubjectID=assub.RecordID
						INNER JOIN(
							SELECT ClassLevelID as YearID, LevelName as YearName FROM INTRANET_CLASSLEVEL
						) as year ON iptpt.YearID=year.YearID
						ORDER BY DateTimeInput";
            }else{
                $sql = "SELECT iptpt.RecordID,BatchID,iptpt.YearID,SubjectID,Period,DateTimeInput,SName,YearName
						FROM(
							SELECT RecordID,BatchID,YearID,SubjectID,Period,DateTimeInput
							FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
						) as iptpt
						INNER JOIN(
							SELECT RecordID,CODEID,".$indexVar['libappraisal']->getLangSQL("CH_SNAME","EN_SNAME")." as SName
							FROM ASSESSMENT_SUBJECT
						) as assub ON iptpt.SubjectID=assub.RecordID
						INNER JOIN(
							SELECT YearID,YearName,Sequence FROM YEAR
						) as year ON iptpt.YearID=year.YearID
						ORDER BY DateTimeInput";
            }
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $runningIndex = 0;
            
            //$x4 .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($preDefHeader[4]["Descr"])."<br/>\r\n";
            $x4 .= "<div id=\"teachingClassLayer\">";
            $x4 .= "<table id=\"teachingClassTable\" class=\"common_table_list\">";
            $x4 .= "<thead>";
            $x4 .= "<tr><th style=\"width:30%\">".$Lang['Appraisal']['ARFormClsLv']."</th><th style=\"width:30%\">".$Lang['Appraisal']['ARFormTeachSubj']."</th>";
            $x4 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormClsPerCyc']."</th>";
            $x4 .= "<th style=\"width:10%\"></th>";
            $x4 .= "</tr></thead>";
            $x4 .= "<tbody>";
            
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x4 .= "<tr id=\"teachFormRow_".$j."\">";
                $x4 .= "<td>".$indexVar['libappraisal']->displayChinese($a[$i]["YearName"])."</td>";
                $x4 .= "<td >".$indexVar['libappraisal']->displayChinese($a[$i]["SName"])."</td>";
                $x4 .= "<td>";
                $x4 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('formPerCyc_'.$j,'formPerCyc_'.$j,$a[$i]["Period"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x4 .= "</td>";
                $x4 .= "<td>";
                $x4 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                $x4 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteForm('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                $x4 .= "</span>";
                $x4 .= "</td>";
                $x4 .= "<input type=\"hidden\" id=\"hiddenYear_".$j."\" name=\"hiddenYear_".$j."\" VALUE=\"".$a[$i]["YearID"]."\">";
                $x4 .= "<input type=\"hidden\" id=\"hiddenFormSubject_".$j."\" name=\"hiddenFormSubject_".$j."\" VALUE=\"".$a[$i]["SubjectID"]."\">";
                $x4 .= "<input type=\"hidden\" id=\"hiddenForm_".$j."\" name=\"hiddenForm_".$j."\" VALUE=\"ACTIVE\">";
                $x4 .= "</tr>";
                $runningIndex++;
            }
            $x4 .= "<tr height=\"34px\"><td></td><td></td><td></td><td>";
            $x4 .= "<div class=\"table_row_tool row_content_tool\">";
            $x4 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addForm('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
            $x4 .= "<input type=\"hidden\" id=\"currentFormRow\" name=\"currentFormRow\" VALUE=\"".sizeof($a)."\">";
            $x4 .= "<input type=\"hidden\" id=\"totalFormRow\" name=\"totalFormRow\" VALUE=\"".$runningIndex."\">";
            $x4 .= "</td></tr>";
            $x4 .= "</tbody>";
            $x4 .= "</table>";
            $x4 .= "</div><br/>";
        }
        //======================================================================== Present Administrative Duties ========================================================================//
        if($preDefHeader[5]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,DutyType,SeqID,Descr
			FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			AND DutyType='A'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x5 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[5]["Descr"])."<br/>\r\n";
            $x5 .= "<div id=\"othAdmInfoLayer\">";
            $x5 .= "<table id=\"othAdmInfoTable\" class=\"common_table_list\">";
            $x5 .= "<thead>";
            $x5 .= "<tr><th style=\"width:5%\">".$Lang['Appraisal']['ARFormSeq']."</th><th style=\"width:95%\">".$Lang['Appraisal']['ARFormOthAdmDesc']."</th>";
            $x5 .= "</tr></thead>";
            $x5 .= "<tbody>";
            for($i=0;$i<$ColForPreSetField;$i++){
                $j=$i+1;
                $x5 .= "<tr id=\"othAdmRow_".$j."\">";
                $x5 .= "<td >".$a[$i]["SeqID"]."</td>";
                $x5 .= "<td>";
                $x5 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('othAdmDesc_'.$j,'othAdmDesc_'.$j,$a[$i]["Descr"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x5 .= "</td>";
                $x5 .= "</tr>";
            }
            $x5 .= "</tbody>";
            $x5 .= "</table></div><br/>";
        }
        //======================================================================== Present Extra-Curriculum Activity Duties ========================================================================//
        if($preDefHeader[6]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,DutyType,SeqID,Descr
			FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			AND DutyType='E'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x6 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[6]["Descr"])."<br/>\r\n";
            $x6 .= "<div id=\"othExtCurInfoLayer\">";
            $x6 .= "<table id=\"othExtCurInfoTable\" class=\"common_table_list\">";
            $x6 .= "<thead>";
            $x6 .= "<tr><th style=\"width:5%\">".$Lang['Appraisal']['ARFormSeq']."</th><th style=\"width:95%\">".$Lang['Appraisal']['ARFormOthExtCurDesc']."</th>";
            $x6 .= "</tr></thead>";
            $x6 .= "<tbody>";
            for($i=0;$i<$ColForPreSetField;$i++){
                $j=$i+1;
                $x6 .= "<tr id=\"othExtCurRow_".$j."\">";
                $x6 .= "<td >".$a[$i]["SeqID"]."</td>";
                $x6 .= "<td>";
                $x6 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('othExtCurDesc_'.$j,'othExtCurDesc_'.$j,$a[$i]["Descr"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x6 .= "</td>";
                $x6 .= "</tr>";
            }
            $x6 .= "</tbody>";
            $x6 .= "</table></div><br/>";
        }
        //======================================================================== Present Other Duties ========================================================================//
        if($preDefHeader[7]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,DutyType,SeqID,Descr
			FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			AND DutyType='O'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x7 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[7]["Descr"])."<br/>\r\n";
            $x7 .= "<div id=\"othDutInfoLayer\">";
            $x7 .= "<table id=\"othDutInfoTable\" class=\"common_table_list\">";
            $x7 .= "<thead>";
            $x7 .= "<tr><th style=\"width:5%\">".$Lang['Appraisal']['ARFormSeq']."</th><th style=\"width:95%\">".$Lang['Appraisal']['ARFormOthDutDesc']."</th>";
            $x7 .= "</tr></thead>";
            $x7 .= "<tbody>";
            for($i=0;$i<$ColForPreSetField;$i++){
                $j=$i+1;
                $x7 .= "<tr id=\"othDutRow_".$j."\">";
                $x7 .= "<td >".$a[$i]["SeqID"]."</td>";
                $x7 .= "<td>";
                $x7 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('othDutDesc_'.$j,'othDutDesc_'.$j,$a[$i]["Descr"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x7 .= "</td>";
                $x7 .= "</tr>";
            }
            $x7 .= "</tbody>";
            $x7 .= "</table></div><br/>";
        }
        //======================================================================== Absence & Lateness ========================================================================//
        if($preDefHeader[8]["Incl"]==1){
            $sql = "SELECT '1' as SeqID,AbsLsnCnt as Value FROM INTRANET_PA_C_ABSENCE WHERE CycleID='".$cycleID."' AND UserID='".IntegerSafe($_SESSION['UserID'])."'
			UNION
			SELECT '2' as SeqID,SubLsnCnt as Value FROM INTRANET_PA_C_ABSENCE WHERE CycleID='".$cycleID."' AND UserID='".IntegerSafe($_SESSION['UserID'])."'
			UNION
			SELECT '3' as SeqID,LateCnt as Value FROM INTRANET_PA_C_ABSENCE WHERE CycleID='".$cycleID."' AND UserID='".IntegerSafe($_SESSION['UserID'])."'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x8 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[8]["Descr"])."\r\n";
            $x8 .= "<div id=\"asbLatInfoLayer\">";
            $x8 .= "<table id=\"asbLatInfoTable\" class=\"form_table_v30\">";
            $x8 .= "<thead>";
            $x8 .= "<tr>";
            $x8 .= "</tr></thead>";
            $x8 .= "<tbody>";
            $x8 .= "<tr>";
            $x8 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormNoLesAsb']."</td>";
            $x8 .= "<td>";
            $x8 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('lesAsb','lesAsb',$a[0]["Value"], $OtherClass='', $OtherPar=array('maxlength'=>255));
            $x8 .= "</td>";
            $x8 .= "</tr>";
            $x8 .= "<tr>";
            $x8 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormNoSubLes']."</td>";
            $x8 .= "<td>";
            $x8 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('subLes','subLes',$a[1]["Value"], $OtherClass='', $OtherPar=array('maxlength'=>255));
            $x8 .= "</td>";
            $x8 .= "</tr>";
            $x8 .= "<tr>";
            $x8 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormNoTimLat']."</td>";
            $x8 .= "<td>";
            $x8 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('timLat','timLat',$a[2]["Value"], $OtherClass='', $OtherPar=array('maxlength'=>255));
            $x8 .= "</td>";
            $x8 .= "</tr>";
            $x8 .= "</tbody>";
            $x8 .= "</table></div><br/>";
        }
        //======================================================================== Leaves Records (Pay & Non-Pay) ========================================================================//
        if($preDefHeader[9]["Incl"]==1){
            $sql = "SELECT CycleID,1 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Sick Leave' AND CycleID='".$cycleID."' AND UserID='".IntegerSafe($_SESSION['UserID'])."'
			UNION
			SELECT CycleID,2 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Official Leave' AND CycleID='".$cycleID."' AND UserID='".IntegerSafe($_SESSION['UserID'])."'
			UNION
			SELECT CycleID,3 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Study Leave' AND CycleID='".$cycleID."' AND UserID='".IntegerSafe($_SESSION['UserID'])."'
			UNION
			SELECT CycleID,4 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Maternity Leave' AND CycleID='".$cycleID."' AND UserID='".IntegerSafe($_SESSION['UserID'])."'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x9 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[9]["Descr"])."<br/>\r\n";
            $x9 .= "<div id=\"leavePaidInfoLayer\">";
            $x9 .= "<table id=\"leavePaidInfoTable\" class=\"common_table_list\">";
            $x9 .= "<thead>";
            $x9 .= "<tr>";
            $x9 .= "<th style=\"width:25%\">".$Lang['Appraisal']['ARFormLeaCat']."</th><th style=\"width:25%\">".$Lang['Appraisal']['ARFormPayLea']."</th>";
            $x9 .= "<th style=\"width:25%\">".$Lang['Appraisal']['ARFormNoPayLea']."</th><th style=\"width:25%\">".$Lang['Appraisal']['ARFormRemarks']."</th>";
            $x9 .= "</tr></thead>";
            $x9 .= "<tbody>";
            $x9 .= "<tr>";
            $x9 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormSickLeave']."</td>";
            $x9 .= "<td>";
            $x9 .= $a[0]["DaysPaid"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[0]["DaysNoPay"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[0]["Remark"];
            $x9 .= "</td>";
            $x9 .= "</tr>";
            $x9 .= "<tr>";
            $x9 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormOfficialLeave']."</td>";
            $x9 .= "<td>";
            $x9 .= $a[1]["DaysPaid"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[1]["DaysNoPay"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[1]["Remark"];
            $x9 .= "</td>";
            $x9 .= "</tr>";
            $x9 .= "<tr>";
            $x9 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormStudyLeave']."</td>";
            $x9 .= "<td>";
            $x9 .= $a[2]["DaysPaid"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[2]["DaysNoPay"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[2]["Remark"];
            $x9 .= "</td>";
            $x9 .= "</tr>";
            $x9 .= "<tr>";
            $x9 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormMaternityLeave']."</td>";
            $x9 .= "<td>";
            $x9 .= $a[3]["DaysPaid"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[3]["DaysNoPay"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[3]["Remark"];
            $x9 .= "</td>";
            $x9 .= "</tr>";
            $x9 .= "</tbody>";
            $x9 .= "</table></div><br/>";
        }
        //======================================================================== Leave Records ========================================================================//
        if($preDefHeader[10]["Incl"]==1){
            $x10 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[10]["Descr"])."<br/>\r\n";
            $x10 .= "<div id=\"leavePaidTotalInfoLayer\">";
            $x10 .= "<table id=\"leavePaidTotalInfoTable\" class=\"common_table_list\">";
            $x10 .= "<thead>";
            $x10 .= "<tr>";
            $x10 .= "<th style=\"width:25%\">".$Lang['Appraisal']['ARFormLeaCat']."</th><th style=\"width:25%\">".$Lang['Appraisal']['ARFormPayTot']."</th>";
            $x10 .= "<th style=\"width:50%\">".$Lang['Appraisal']['ARFormRemarks']."</th>";
            $x10 .= "</tr></thead>";
            $x10 .= "<tbody>";
            $x10 .= "<tr>";
            $x10 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormSickLeave']."</td>";
            $x10 .= "<td>";
            $x10 .= $a[0]["DaysTot"];
            $x10 .= "</td>";
            $x10 .= "<td>";
            $x10 .= $a[0]["Remark"];
            $x10 .= "</td>";
            $x10 .= "</tr>";
            $x10 .= "<tr>";
            $x10 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormOfficialLeave']."</td>";
            $x10 .= "<td>";
            $x10 .= $a[1]["DaysTot"];
            $x10 .= "</td>";
            $x10 .= "<td>";
            $x10 .= $a[1]["Remark"];
            $x10 .= "</td>";
            $x10 .= "</tr>";
            $x10 .= "<tr>";
            $x10 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormStudyLeave']."</td>";
            $x10 .= "<td>";
            $x10 .= $a[2]["DaysTot"];
            $x10 .= "</td>";
            $x10 .= "<td>";
            $x10 .= $a[2]["Remark"];
            $x10 .= "</td>";
            $x10 .= "</tr>";
            $x10 .= "<tr>";
            $x10 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormMaternityLeave']."</td>";
            $x10 .= "<td>";
            $x10 .= $a[3]["DaysTot"];
            $x10 .= "</td>";
            $x10 .= "<td>";
            $x10 .= $a[3]["Remark"];
            $x10 .= "</td>";
            $x10 .= "</tr>";
            $x10 .= "</tbody>";
            $x10 .= "</table></div><br/>";
        }
        //======================================================================== Attended Course/Seminar/Workshop ========================================================================//
        if($preDefHeader[11]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,SeqID,DATE_FORMAT(EventDate,'%Y-%m-%d') as EventDate,Orgn,CrsSemWs,Hours,Certification
			FROM INTRANET_PA_T_PRE_CRS WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."';";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x11 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[11]["Descr"])."<br/>\r\n";
            
            $x11 .= "<div id=\"crsSemWsInfoLayer\">";
            $x11 .= "<table id=\"crsSemWsInfoTable\" class=\"common_table_list\">";
            $x11 .= "<thead>";
            $x11 .= "<tr><th style=\"width:2%\">".$Lang['Appraisal']['ARFormSeq']."</th><th style=\"width:10%\">".$Lang['Appraisal']['ARFormDate']."</th>";
            $x11 .= "<th style=\"width:28%\">".$Lang['Appraisal']['ARFormOrganization']."</th><th style=\"width:20%\">".$Lang['Appraisal']['ARFormCourseSeminarWorkshop']."</th>";
            $x11 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormHours']."</th><th style=\"width:20%\">".$Lang['Appraisal']['ARFormCertification']."</th>";
            $x11 .= "<th style=\"width:10%\"></th>";
            $x11 .= "</tr></thead>";
            $x11 .= "<tbody>";
            $runningIndex = 0;
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x11 .= "<tr id=\"crsSemWsRow_".$j."\">";
                $x11 .= "<td>";
                $x11 .= "<div id=\"crsSemWsSeqID_".$a[$i]["SeqID"]."\" name=\"crsSemWsSeqID_".$a[$i]["SeqID"]."\">";
                $x11 .= $a[$i]["SeqID"];
                $x11 .= "</div>";
                $x11 .= "</td>";
                $x11 .= "<td>";
                $x11 .= $indexVar['libappraisal_ui']->GET_DATE_PICKER('eventDate_'.$j,$a[$i]["EventDate"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('issueDate_".$j."')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
                $x11 .= "</td>";
                $x11 .= "<td>";
                $x11 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('orgn_'.$j, 'orgn_'.$j, $a[$i]["Orgn"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x11 .= "</td>";
                $x11 .= "<td>";
                $x11 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('crssemws_'.$j, 'crssemws_'.$j, $a[$i]["CrsSemWs"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x11 .= "</td>";
                $x11 .= "<td>";
                $x11 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('hours_'.$j, 'hours_'.$j, $a[$i]["Hours"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x11 .= "</td>";
                $x11 .= "<td>";
                $x11 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('certification_'.$j, 'certification_'.$j, $a[$i]["Certification"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x11 .= "</td>";
                $x11 .= "<td>";
                $x11 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                $x11 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteCrsSemWs('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                $x11 .= "</span>";
                $x11 .= "<input type=\"hidden\" id=\"hiddenCrsSemWsSts_".$j."\" name=\"hiddenCrsSemWsSts_".$j."\" VALUE=\"ACTIVE\">";
                $x11 .= "<input type=\"hidden\" id=\"hiddenCrsSemWsSeqID_".$j."\" name=\"hiddenCrsSemWsSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
                $x11 .= "</td>";
                $x11 .= "</tr>";
                $runningIndex++;
            }
            $x11 .= "<tr height=\"34px\"><td></td><td></td><td></td><td></td><td></td><td></td><td>";
            $x11 .= "<div class=\"table_row_tool row_content_tool\">";
            $x11 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addCrsSemWs('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
            $x11 .= "<input type=\"hidden\" id=\"currentCrsSemWsRow\" name=\"currentCrsSemWsRow\" VALUE=\"".sizeof($a)."\">";
            $x11 .= "<input type=\"hidden\" id=\"totalCrsSemWsRow\" name=\"totalCrsSemWsRow\" VALUE=\"".$runningIndex."\">";
            $x11 .= "</td></tr>";
            $x11 .= "</tbody>";
            $x11 .= "</table>";
            $x11 .= "</div><br/>";
        }
        //======================================================================== Teaching / Administrative Duties Objectives and Assessment ========================================================================//
        if($preDefHeader[12]["Incl"]==1){
            $x12 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[12]["Descr"])."<br/>\r\n";
            $sql = "SELECT RecordID,BatchID,SeqID,Objective,Plan,Assessment,CatID,Standard
						FROM INTRANET_PA_T_PRE_TASKASSESSMENT WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."';";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $runningIndex = 0;
            
            //$x3 .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($preDefHeader[4]["Descr"])."<br/>\r\n";
            $x12 .= "<div id=\"taskAssessmentLayer\">";
            if($Lang['Appraisal']['ARFormTaskDesc']!=""){
                $x12 .= "<p>";
                $x12 .= $Lang['Appraisal']['ARFormTaskDesc'];
                $x12 .= "</p>";
            }
            if($sys_custom['eAppraisal']['DutyObjectivesAssessmentIn4Parts']==true){
                $taskListByCatArr = array();
                foreach($a as $taskRecord){
                    if(!isset($taskListByCatArr[$taskRecord['CatID']])){
                        $taskListByCatArr[$taskRecord['CatID']] = array();
                    }
                    array_push($taskListByCatArr[$taskRecord['CatID']], $taskRecord);
                }
                for($index=1; $index <= 4; $index++){
                    $x12 .= "<p>".$Lang['Appraisal']['ARFormTaskParts'][$index]."</p>";
                    $x12 .= "<table id=\"taskAssessmentTable_$index\" class=\"common_table_list\">";
                    $x12 .= "<thead>";
                    $x12 .= "<tr>";
                    $x12 .= "<th style=\"width:22%\">".$Lang['Appraisal']['ARFormTaskObjective']."</th>";
                    $x12 .= "<th style=\"width:22%\">".$Lang['Appraisal']['ARFormTaskStandard']."</th>";
                    $x12 .= "<th style=\"width:22%\">".$Lang['Appraisal']['ARFormTaskAction']."</th>";
                    $x12 .= "<th style=\"width:22%\">".$Lang['Appraisal']['ARFormTaskAchievement']."</th>";
                    $x12 .= "<th style=\"width:5%\"></th>";
                    $x12 .= "</tr></thead>";
                    $x12 .= "<tbody>";
                    $x12 .= "<tr height=\"34px\"><td></td><td></td><td></td><td></td><td>";
                    $x12 .= "<div class=\"table_row_tool row_content_tool\">";
                    $x12 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addClass('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
                    $x12 .= "<input type=\"hidden\" id=\"currentClassRow\" name=\"currentClassRow\" VALUE=\"".sizeof($a)."\">";
                    $x12 .= "<input type=\"hidden\" id=\"totalClassRow\" name=\"totalClassRow\" VALUE=\"".$runningIndex."\">";
                    $x12 .= "</td></tr>";
                    $x12 .= "</tbody>";
                    $x12 .= "</table>";
                }
                $x12 .= "</div><br/>";
            }else{
                $x12 .= "<table id=\"taskAssessmentTable\" class=\"common_table_list\">";
                $x12 .= "<thead>";
                $x12 .= "<tr>";
                $x12 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormTaskObjective']."</th>";
                $x12 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormTaskAction']."</th>";
                $x12 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormTaskAssessment']."</th>";
                $x12 .= "<th style=\"width:5%\"></th>";
                $x12 .= "</tr></thead>";
                $x12 .= "<tbody>";
                
                for($i=0;$i<sizeof($a);$i++){
                    $j=$i+1;
                    $x12 .= "<tr id=\"teachClassRow_".$j."\">";
                    $x12 .= "<td>";
                    $x12 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('taskObjective_'.$j,'taskObjective_'.$j,$a[$i]["Objective"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                    $x12 .= "</td>";
                    $x12 .= "<td>";
                    $x12 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('taskPlan_'.$j,'taskPlan_'.$j,$a[$i]["Plan"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                    $x12 .= "</td>";
                    $x12 .= "<td>";
                    $x12 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('taskAssessment_'.$j,'taskAssessment_'.$j,$a[$i]["Assessment"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                    $x12 .= "</td>";
                    $x12 .= "<td>";
                    $x12 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                    $x12 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteClass('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                    $x12 .= "</span>";
                    $x12 .= "</td>";
                    $x12 .= "<td>";
                    $x12 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                    $x12 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteTaskAssessment('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                    $x12 .= "</span>";
                    $x12 .= "<input type=\"hidden\" id=\"hiddenTaskAssessmentSts_".$j."\" name=\"hiddenTaskAssessmentSts_".$j."\" VALUE=\"ACTIVE\">";
                    $x12 .= "<input type=\"hidden\" id=\"hiddenTaskAssessmentSeqID_".$j."\" name=\"hiddenTaskAssessmentSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
                    $x12 .= "</td>";
                    $x12 .= "</tr>";
                    $runningIndex++;
                }
                
                $x12 .= "<tr height=\"34px\"><td></td><td></td><td></td><td>";
                $x12 .= "<div class=\"table_row_tool row_content_tool\">";
                $x12 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addClass('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
                $x12 .= "<input type=\"hidden\" id=\"currentClassRow\" name=\"currentClassRow\" VALUE=\"".sizeof($a)."\">";
                $x12 .= "<input type=\"hidden\" id=\"totalClassRow\" name=\"totalClassRow\" VALUE=\"".$runningIndex."\">";
                $x12 .= "</td></tr>";
                $x12 .= "</tbody>";
                $x12 .= "</table>";
                $x12 .= "</div><br/>";
            }
        }
        //======================================================================== Present Academic Duties ========================================================================//
        if($preDefHeader[13]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,DutyType,SeqID,Descr
			FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			AND DutyType='S'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x13 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[13]["Descr"])."<br/>\r\n";
            $x13 .= "<div id=\"othAdmInfoLayer\">";
            $x13 .= "<table id=\"othAdmInfoTable\" class=\"common_table_list\">";
            $x13 .= "<thead>";
            $x13 .= "<tr><th style=\"width:5%\">".$Lang['Appraisal']['ARFormSeq']."</th><th style=\"width:95%\">".$Lang['Appraisal']['ARFormOthAcaDesc']."</th>";
            $x13 .= "</tr></thead>";
            $x13 .= "<tbody>";
            for($i=0;$i<$ColForPreSetField;$i++){
                $j=$i+1;
                $x13 .= "<tr id=\"othAdmRow_".$j."\">";
                $x13 .= "<td >".$a[$i]["SeqID"]."</td>";
                $x13 .= "<td>";
                $x13 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('othAcaDesc_'.$j,'othAcaDesc_'.$j,$a[$i]["Descr"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x13 .= "</td>";
                $x13 .= "</tr>";
            }
            $x13 .= "</tbody>";
            $x13 .= "</table></div><br/>";
        }
        //======================================================================== Student Training for Competition ========================================================================//
        if($preDefHeader[14]["Incl"]==1){
            $x14 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[14]["Descr"])."<br/>\r\n";
            
            $sql = "SELECT RecordID,BatchID,SeqID,Competition,Trainee,Duration,Result,Remark
					FROM INTRANET_PA_T_PRE_STUDENTTRAINING WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."';";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $runningIndex = 0;
            
            $x14 .= "<div id=\"studentTrainingLayer\">";
            $x14 .= "<table id=\"studentTrainingTable\" class=\"common_table_list\">";
            $x14 .= "<thead>";
            $x14 .= "<tr><th style=\"width:5%\">#</th>";
            $x14 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormStdTrainComp']."</th>";
            $x14 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormStdTrainNo']."</th>";
            $x14 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormStdTrainDur']."</th>";
            $x14 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormStdTrainResult']."</th>";
            $x14 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormRemarks']."</th>";
            $x14 .= "<th style=\"width:5%\"></th>";
            $x14 .= "</tr></thead>";
            $x14 .= "<tbody>";
            
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x14 .= "<tr id=\"studentTrainingRow_".$j."\">";
                $x14 .= "<td>";
                $x14 .= "<div id=\"StudentTrainingSeqID_".$a[$i]["SeqID"]."\" name=\"StudentTrainingSeqID_".$a[$i]["SeqID"]."\">";
                $x14 .= $j;
                $x14 .= "</div>";
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('stdTrainComp_'.$j,'stdTrainComp_'.$j,$a[$i]["Competition"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('stdTrainNo_'.$j,'stdTrainNo_'.$j,$a[$i]["Trainee"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('stdTrainDur_'.$j,'stdTrainDur_'.$j,$a[$i]["Duration"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('stdTrainResult_'.$j,'stdTrainResult_'.$j,$a[$i]["Result"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('stdTrainRmk_'.$j,'stdTrainRmk_'.$j,$a[$i]["Remark"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                $x14 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteStudentTraining('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                $x14 .= "</span>";
                $x14 .= "<input type=\"hidden\" id=\"hiddenStudentTrainingSts_".$j."\" name=\"hiddenStudentTrainingSts_".$j."\" VALUE=\"ACTIVE\">";
                $x14 .= "<input type=\"hidden\" id=\"hiddenStudentTrainingSeqID_".$j."\" name=\"hiddenStudentTrainingSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
                $x14 .= "</td>";
                $x14 .= "</tr>";
                $runningIndex++;
            }
            for($i=sizeof($a);$i<sizeof($a)+10;$i++){
                $j=$i+1;
                $x14 .= "<tr id=\"studentTrainingRow_".$j."\" style=\"display:none;\">";
                $x14 .= "<td>";
                $x14 .= "<div id=\"seqID_".$a[$i]["SeqID"]."\" name=\"seqID_".$a[$i]["SeqID"]."\">";
                $x14 .= $a[$i]["SeqID"];
                $x14 .= "</div>";
                $x14 .= "<td>";
                $x14 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('stdTrainComp_'.$j,'stdTrainComp_'.$j,$a[$i]["Competition"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('stdTrainNo_'.$j,'stdTrainNo_'.$j,$a[$i]["Trainee"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('stdTrainDur_'.$j,'stdTrainDur_'.$j,$a[$i]["Duration"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('stdTrainResult_'.$j,'stdTrainResult_'.$j,$a[$i]["Result"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('stdTrainRmk_'.$j,'stdTrainRmk_'.$j,$a[$i]["Remark"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                $x14 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteStudentTraining('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                $x14 .= "</span>";
                $x14 .= "<input type=\"hidden\" id=\"hiddenStudentTrainingSts_".$j."\" name=\"hiddenStudentTrainingSts_".$j."\" VALUE=\"ACTIVE\">";
                $x14 .= "<input type=\"hidden\" id=\"hiddenStudentTrainingSeqID_".$j."\" name=\"hiddenStudentTrainingSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
                $x14 .= "</td>";
                $x14 .= "</tr>";
                $runningIndex++;
            }
            $x14 .= "<tr height=\"34px\"><td></td><td></td><td></td><td></td><td></td><td></td><td>";
            $x14 .= "<div class=\"table_row_tool row_content_tool\">";
            $x14 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addStudentTraining('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
            $x14 .= "<input type=\"hidden\" id=\"currentStudentTrainingRow\" name=\"currentStudentTrainingRow\" VALUE=\"".sizeof($a)."\">";
            $x14 .= "<input type=\"hidden\" id=\"totalStudentTrainingRow\" name=\"totalStudentTrainingRow\" VALUE=\"".$runningIndex."\">";
            $x14 .= "</td></tr>";
            $x14 .= "</tbody>";
            $x14 .= "</table>";
            $x14 .= "</div><br/>";
        }
        //======================================================================== CPD ========================================================================//
        if($preDefHeader[15]["Incl"]==1){
            $x15 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[15]["Descr"])."<br/>\r\n";
            $academicYear = $indexVar['libappraisal']->getAcademicYearTermOfCurrentCycle(date('Y-m-d h:i:s'));
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDDurationSetting'";
            $cpdHoursCountingSeparateMonth = current($connection->returnVector($sql));
            if(empty($cpdHoursCountingSeparateMonth)){
                $cpdHoursCountingSeparateMonth = 4;
            }else{
                $cpdHoursCountingSeparateMonth = intval($cpdHoursCountingSeparateMonth);
            }
            $nextCpdHoursCountingSeparateMonth = $cpdHoursCountingSeparateMonth+1;
            if($cpdHoursCountingSeparateMonth==12){
                $nextCpdHoursCountingSeparateMonth = 1;
            }
            
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDShowingTotalHourSetting'";
            $cpdShowingTotalHour= current($connection->returnVector($sql));
            if(empty($cpdShowingTotalHour)){
                $cpdShowingTotalHour = false;
            }
            
            $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("EventNameEng","EventNameChi")." as CourseTitle,"
                .$indexVar['libappraisal']->getLangSQL("EventTypeEng","EventTypeChi")." as Nature,
				Content,Organization,NoOfSection,CPDMode,CPDDomain,BasicLawRelated, SubjectRelated
					FROM INTRANET_PA_S_T_PRE_CPD WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."' AND AcademicYearID=".IntegerSafe($academicYear['AcademicYearID'])." ORDER BY StartDate ASC";
                //echo $sql;
                $a=$connection->returnResultSet($sql);
                $runningIndex = 0;
                
                $CPDHours = array(0,0,0);
                for($i=0;$i<sizeof($a);$i++){
                    $month = date('n',strtotime($a[$i]['StartDate']));
                    if($cpdHoursCountingSeparateMonth >= 9){
                        if($month >= 9 && $month <= $cpdHoursCountingSeparateMonth){
                            $CPDHours[0] = $CPDHours[0] + $a[$i]['NoOfSection'];
                        }else{
                            $CPDHours[1] = $CPDHours[1] + $a[$i]['NoOfSection'];
                        }
                    }else{
                        if($month >= $nextCpdHoursCountingSeparateMonth && $month <= 8){
                            $CPDHours[1] = $CPDHours[1] + $a[$i]['NoOfSection'];
                        }else{
                            $CPDHours[0] = $CPDHours[0] + $a[$i]['NoOfSection'];
                        }
                    }
                    $CPDHours[2] = $CPDHours[2] + $a[$i]['NoOfSection'];
                    $domain = explode(",",$a[$i]['CPDDomain']);
                    foreach($domain as $idx=>$dm){
                        $a[$i]['CPDDomain'.$idx] = $dm;
                    }
                    $subjectRelated = explode(",",$a[$i]['SubjectRelated']);
                    foreach($subjectRelated as $idx=>$dm){
                        $a[$i]['SubjectRelated'.$idx] = $dm;
                    }
                }
                $x15 .= "<div id=\"cpdHourLayer\">";
                $x15 .= "<table id=\"cpdHourTable\" class=\"common_table_list\">";
                $x15 .= "<thead>";
                $x15 .= "<tr>";
                if($cpdShowingTotalHour==true){
                    $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDHourCategory']."</th>";
                    $x15 .= "<th style=\"width:40%\">".str_replace(array('{{START_MONTH}}','{{END_MONTH}}'),array($Lang['Appraisal']['Month'][9],$Lang['Appraisal']['Month'][$cpdHoursCountingSeparateMonth]),$Lang['Appraisal']['ARFormCPDHourUndefined'])."</th>";
                    $x15 .= "<th style=\"width:40%\">".str_replace(array('{{START_MONTH}}','{{END_MONTH}}'),array($Lang['Appraisal']['Month'][$nextCpdHoursCountingSeparateMonth],$Lang['Appraisal']['Month'][8]),$Lang['Appraisal']['ARFormCPDHourUndefined'])."</th>";
                    $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDHourTotal']."</th>";
                }else{
                    $x15 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormCPDHourCategory']."</th>";
                    $x15 .= "<th style=\"width:40%\">".str_replace(array('{{START_MONTH}}','{{END_MONTH}}'),array($Lang['Appraisal']['Month'][9],$Lang['Appraisal']['Month'][$cpdHoursCountingSeparateMonth]),$Lang['Appraisal']['ARFormCPDHourUndefined'])."</th>";
                    $x15 .= "<th style=\"width:40%\">".str_replace(array('{{START_MONTH}}','{{END_MONTH}}'),array($Lang['Appraisal']['Month'][$nextCpdHoursCountingSeparateMonth],$Lang['Appraisal']['Month'][8]),$Lang['Appraisal']['ARFormCPDHourUndefined'])."</th>";
                }
                $x15 .= "</tr></thead>";
                $x15 .= "<tbody>";
                $x15 .= "<tr height=\"34px\"><td>".$Lang['Appraisal']['ARFormCPDHourCategoryHour'] ."</td><td><span id=\"cpdHourA\">".$CPDHours[0]."</span>".$Lang['Appraisal']['ARFormCPDHourHour']."</td><td><span id=\"cpdHourB\">".$CPDHours[1]."</span>".$Lang['Appraisal']['ARFormCPDHourHour']."</td><td style=\"".(($cpdShowingTotalHour==true)?"":"display:none")."\"><span id=\"cpdHourTotal\">".$CPDHours[2]."</span>".$Lang['Appraisal']['ARFormCPDHourHour']."</td></tr>";
                $x15 .= "</tbody>";
                $x15 .= "</table>";
                $x15 .= "</div><br/>";
                if($sys_custom['eAppraisal']['3YearsCPD']==true){
                    if($academicYearID==""){
                        $sql = "SELECT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE TermStart <= '".date('Y-m-d')."' AND TermEnd >= '".date('Y-m-d')."'";
                        $academicYearID = current($connection->returnVector($sql));
                    }
                    $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDSetting'";
                    $cpdSettingList = unserialize(current($connection->returnVector($sql)));
                    $displayHTML = "";
                    $cpdSettingList['Period'] = ($cpdSettingList['Period'])?$cpdSettingList['Period']:1;
                    $cpdSettingList["SelectedYear"] = ($cpdSettingList['SelectedYear'])?$cpdSettingList['SelectedYear']:$_SESSION['CurrentSchoolYearID'];
                    $sql = "SELECT Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID=".$cpdSettingList["SelectedYear"];
                    $startYearSeq = current($connection->returnVector($sql));
                    $sql = "SELECT Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID=".$academicYearID;
                    $currSeq = current($connection->returnVector($sql));
                    $yearNo = (($startYearSeq - $currSeq)%$cpdSettingList['Period']);
                    $x15 .= "<div id=\"CPDPastYears\">";
                    $x15 .= $Lang['Appraisal']['ARFormCPDPastYearHead']."<br>";
                    $x15 .= $Lang['Appraisal']['ARFormCPDPastYearRemark']."<br>";
                    $x15 .= "<div id=\"CPDPastYearsHoursList\">";
                    $sql = "SELECT AcademicYearID, Hours FROM INTRANET_PA_T_PRE_CPD_HOURS WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."' AND UserID='".$userID."'";
                    $yearHourData=$connection->returnResultSet($sql);
                    $yearCPDHour = array();
                    foreach($yearHourData as $yhd){
                        $yearCPDHour[$yhd['AcademicYearID']] = $yhd['Hours'];
                    }
                    if($yearNo < 0){
                        for($y=$yearNo; $y <= 0; $y++){
                            $sql = "SELECT AcademicYearID, ".$indexVar['libappraisal']->getLangSQL("YearNameB5","YearNameEN")." as YearName FROM ACADEMIC_YEAR WHERE Sequence='".($currSeq+$y)."'";
                            $pastYearData = current($connection->returnArray($sql));
                            if(!isset($yearCPDHour[$pastYearData['AcademicYearID']])){
                                $yearCPDHour[$pastYearData['AcademicYearID']] = 0;
                            }
                            $x15 .= $pastYearData['YearName']." ".$Lang['Appraisal']['ARFormCPDHourSchoolYear']."( <span id='yearHour_".$pastYearData['AcademicYearID']."_span'>".$yearCPDHour[$pastYearData['AcademicYearID']]."</span>".$Lang['Appraisal']['ARFormCPDHourHour'].")   ";
                        }
                    }else{
                        for($y=$yearNo; $y >= 0; $y--){
                            $sql = "SELECT AcademicYearID, ".$indexVar['libappraisal']->getLangSQL("YearNameB5","YearNameEN")." as YearName FROM ACADEMIC_YEAR WHERE Sequence='".($currSeq+$y)."'";
                            $pastYearData = current($connection->returnArray($sql));
                            if(!isset($yearCPDHour[$pastYearData['AcademicYearID']])){
                                $yearCPDHour[$pastYearData['AcademicYearID']] = 0;
                            }
                            $x15 .= $pastYearData['YearName']." ".$Lang['Appraisal']['ARFormCPDHourSchoolYear']."( <span id='yearHour_".$pastYearData['AcademicYearID']."_span'>".$yearCPDHour[$pastYearData['AcademicYearID']]."</span>".$Lang['Appraisal']['ARFormCPDHourHour'].")   ";
                        }
                    }
                    $x15 .= "</div>";
                    $x15 .= "</div><br/>";
                }
                $x15 .= "<div id=\"CPDLayer\">";
                $x15 .= "<table id=\"CPDTable\" class=\"common_table_list\">";
                $x15 .= "<thead>";
                $x15 .= "<tr>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormCourseTitle']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormCourseNature']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormCourseContent']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormOrgnBody']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormStartDate']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormEndDate']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormCPDMode']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormCPDDomain']."</th>";
                if($sys_custom['TeacherPortfolio']["cpdSubjectTag"]){
                    $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormSubjectRelated']."</th>";
                }
                $x15 .= "<th style=\"width:8%\">".$Lang['Appraisal']['ARFormCPDFormCPDHour']."</th>";
                //$x15 .= "<th style=\"width:8%\">".$Lang['Appraisal']['ARFormCPDFormBasicLawHour']."</th>";
                $x15 .= "<th style=\"width:4%\"></th>";
                $x15 .= "</tr></thead>";
                $x15 .= "<tbody>";
                for($i=0;$i<sizeof($a);$i++){
                    $j=$i+1;
                    $x15 .= "<tr id=\"CPDRow_".$j."\">";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('CPDTitle_'.$j,'CPDTitle_'.$j,$a[$i]["CourseTitle"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('CPDNature_'.$j,'CPDNature_'.$j,$a[$i]["Nature"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('CPDContent_'.$j,'CPDContent_'.$j,$a[$i]["Content"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('CPDOrganization_'.$j,'CPDOrganization_'.$j,$a[$i]["Organization"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_DATE_PICKER('CPDStartDate_'.$j,$a[$i]["StartDate"], $OtherMember='',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID='CPDStartDate_'.$j);
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_DATE_PICKER('CPDEndDate_'.$j,$a[$i]["EndDate"], $OtherMember='',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID='CPDEndDate_'.$j);
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_SELECTION_BOX($ParData=array(array(0,$Lang['Appraisal']['ARFormCPDMode'][0]),array(1,$Lang['Appraisal']['ARFormCPDMode'][1])), $ParTags="name='CPDMode_$j' id='CPDMode_$j'", $ParDefault="", $ParSelected="", $CheckType=false);
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    for($idx=0; $idx < 6; $idx++){
                        $x15 .= $indexVar['libappraisal_ui']->Get_Checkbox("CPDDomain_".$j."_".$idx, "CPDDomain_".$j."[]", 1, $isChecked=$a[$i]['CPDDomain'.$idx], $Class='', $Display=$Lang['Appraisal']['ARFormCPDDomain'][$idx], $Onclick='', $Disabled='')."<br>";
                    }
                    $x15 .= "</td>";
                    if($sys_custom['TeacherPortfolio']["cpdSubjectTag"]){
                        $x15 .= "<td>";
                        for($idx=0; $idx < count($Lang['Appraisal']['ARFormCPDSubject']); $idx++){
                            $x15 .= $indexVar['libappraisal_ui']->Get_Checkbox("SubjectRelated_".$j."_".$idx, "SubjectRelated_".$j."_".$idx, 1, $isChecked=$a[$i]['SubjectRelated'.$idx], $Class='', $Display=$Lang['Appraisal']['ARFormCPDSubject'][$idx], $Onclick='', $Disabled='true')."<br>";
                        }
                        $x15 .= "</td>";
                    }
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER('CPDNoOfSection_'.$j, 'CPDNoOfSection_'.$j, $a[$i]['NoOfSection']);
                    $x15 .= "</td>";
                    //$x15 .= "<td>";
                    //$x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER('CPDBasicLaw_'.$j, 'CPDBasicLaw_'.$j, $a[$i]['BasicLawRelated']);
                    //$x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                    $x15 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteCPD('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                    $x15 .= "</span>";
                    $x15 .= "<input type=\"hidden\" id=\"hiddenCPDSts_".$j."\" name=\"hiddenCPDSts_".$j."\" VALUE=\"ACTIVE\">";
                    $x15 .= "<input type=\"hidden\" id=\"hiddenCPDSeqID_".$j."\" name=\"hiddenCPDSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
                    $x15 .= "</td>";
                    $x15 .= "</tr>";
                    $runningIndex++;
                }
                for($i=sizeof($a);$i<sizeof($a)+10;$i++){
                    $j=$i+1;
                    $x15 .= "<tr id=\"CPDRow_".$j."\" style=\"display:none;\">";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('CPDTitle_'.$j,'CPDTitle_'.$j,$a[$i]["CourseTitle"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('CPDNature_'.$j,'CPDNature_'.$j,$a[$i]["Nature"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('CPDContent_'.$j,'CPDContent_'.$j,$a[$i]["Content"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('CPDOrganization_'.$j,'CPDOrganization_'.$j,$a[$i]["Organization"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_DATE_PICKER('CPDStartDate_'.$j,$a[$i]["StartDate"], $OtherMember='',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID='CPDStartDate_'.$j);
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_DATE_PICKER('CPDEndDate_'.$j,$a[$i]["EndDate"], $OtherMember='',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID='CPDEndDate_'.$j);
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_SELECTION_BOX($ParData=array(array(0,$Lang['Appraisal']['ARFormCPDMode'][0]),array(1,$Lang['Appraisal']['ARFormCPDMode'][1])), $ParTags="name='CPDMode_$j' id='CPDMode_$j'", $ParDefault="", $ParSelected=$a[$i]["CPDMode"], $CheckType=false);
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    for($idx=0; $idx < 6; $idx++){
                        $x15 .= $indexVar['libappraisal_ui']->Get_Checkbox("CPDDomain_".$j."_".$idx, "CPDDomain_".$j."[]", 1, $isChecked=$a[$i]['CPDDomain'.$idx], $Class='', $Display=$Lang['Appraisal']['ARFormCPDDomain'][$idx], $Onclick='', $Disabled='')."<br>";
                    }
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER('CPDNoOfSection_'.$j, 'CPDNoOfSection_'.$j, $a[$i]['NoOfSection']);
                    $x15 .= "</td>";
                    //$x15 .= "<td>";
                    //$x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER('CPDBasicLaw_'.$j, 'CPDBasicLaw_'.$j, $a[$i]['BasicLawRelated']);
                    //$x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                    $x15 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteCPD('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                    $x15 .= "</span>";
                    $x15 .= "<input type=\"hidden\" id=\"hiddenCPDSts_".$j."\" name=\"hiddenCPDSts_".$j."\" VALUE=\"ACTIVE\">";
                    $x15 .= "<input type=\"hidden\" id=\"hiddenCPDSeqID_".$j."\" name=\"hiddenCPDSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
                    $x15 .= "</td>";
                    $x15 .= "</tr>";
                    $runningIndex++;
                }
                if($sys_custom['TeacherPortfolio']["cpdSubjectTag"]){
                    $x15 .= "<tr height=\"34px\"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>";
                }else{
                    $x15 .= "<tr height=\"34px\"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>";
                }
                $x15 .= "<div class=\"table_row_tool row_content_tool\">";
                $x15 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addCPD('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
                $x15 .= "<input type=\"hidden\" id=\"currentCPDRow\" name=\"currentCPDRow\" VALUE=\"".sizeof($a)."\">";
                $x15 .= "<input type=\"hidden\" id=\"totalCPDRow\" name=\"totalCPDRow\" VALUE=\"".$runningIndex."\">";
                $x15 .= "</td></tr>";
                $x15 .= "</tbody>";
                $x15 .= "</table>";
                if($Lang['Appraisal']['ARFormCPDRmk']!=""){
                    $x15 .= "<p>";
                    $x15 .= $Lang['Appraisal']['ARFormCPDRmk'];
                    $x15 .= "</p>";
                }
                $x15 .= "</div><br/>";
        }
        //======================================================================== Attendnace Information ========================================================================//
        if($preDefHeader[16]["Incl"]==1){
            $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("TAReasonNameChi","TAReasonNameEng")." as ReasonTitle, ReasonUnit, CountTotalBeforeThisReason FROM INTRANET_PA_S_ATTEDNACE_REASON";
            $reasonTable = $connection->returnArray($sql);
            $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("TAColumnNameChi","TAColumnNameEng")." as ColumnTitle, PeriodStart, PeriodEnd FROM INTRANET_PA_S_ATTEDNACE_COLUMN ORDER BY DisplayOrder ASC";
            $columnTable = $connection->returnArray($sql);
            if(empty($this->GeneralSetting['PreDefinedFormAttendanceTableOrientation'])){
                $orientation = 0;
            }else{
                $settingValueArr = unserialize($this->GeneralSetting['PreDefinedFormAttendanceTableOrientation']);
                $orientation = isset($settingValueArr[$templateID])?$settingValueArr[$templateID]:0;
            }
            $x16 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[16]["Descr"])."<br/>\r\n";
            $x16 .= "<style>";
            $x16 .= ".common_table_list .tableTotal td{border-top: 2px solid ;border-bottom: 2px solid ;}";
            $x16 .= "</style>";
            $x16 .= "<div id=\"AttendanceRecordInfoLayer\">";
            $x16 .= "<table id=\"AttendanceRecordInfoTable\" class=\"common_table_list\">";
            if(count($columnTable) > 0 && count($reasonTable) > 0){
                if($orientation==0){
                    $x16 .= "<thead>";
                    $x16 .= "<tr>";
                    $x16 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormLeaCat']."</th>";
                    $colWidth = round(50/count($columnTable),0);
                    foreach($columnTable as $col){
                        $x16 .= "<th style=\"width:$colWidth%\">".$col['ColumnTitle']."</th>";
                    }
                    $x16 .= "<th style=\"width:40%\">".$Lang['Appraisal']['ARFormOptionalRemarks']."</th>";
                    $x16 .= "</tr></thead>";
                    $x16 .= "<tbody>";
                    foreach($reasonTable as $reason){
                        $x16 .= "<tr>";
                        $x16 .= "	<td>".$reason['ReasonTitle']."</td>";
                        foreach($columnTable as $col){
                            $x16 .= "	<td style='text-align: right'>".$Lang['Appraisal']['ARFormAttendanceUnitList'][$reason['ReasonUnit']]."</td>";
                        }
                        $x16 .= "	<td></td>";
                        $x16 .= "</tr>";
                        if($reason['CountTotalBeforeThisReason']){
                            $x16 .= "<tr class='tableTotal'>";
                            $x16 .= "	<td>".$Lang['Appraisal']['ARFormAttendanceLeaveTotal']."</td>";
                            foreach($columnTable as $col){
                                $x16 .= "	<td style='text-align: right'>".$Lang['Appraisal']['ARFormAttendanceUnitList'][$reason['ReasonUnit']]."</td>";
                            }
                            $x16 .= "	<td></td>";
                            $x16 .= "</tr>";
                        }
                    }
                    $x16 .= "</tbody>";
                }elseif($orientation==1){
                    $x16 .= "<thead>";
                    $x16 .= "<tr>";
                    $x16 .= "<th></th>";
                    foreach($reasonTable as $reason){
                        $x16 .= "<th width='".round(100/(count($reasonTable)+1),0)."%'>".$reason['ReasonTitle']."</th>";
                    }
                    $x16 .= "</tr></thead>";
                    $x16 .= "<tbody>";
                    foreach($columnTable as $col){
                        $x16 .= "<tr>";
                        $x16 .= "<td class=\"field_title\">".$col['ColumnTitle']."</td>";
                        foreach($reasonTable as $reason){
                            $x16 .= "	<td style='text-align: right'>".$Lang['Appraisal']['ARFormAttendanceUnitList'][$reason['ReasonUnit']]."</td>";
                        }
                        $x16 .= "</tr>";
                    }
                    $x16 .= "<tr>";
                    $x16 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormOptionalRemarks']."</td>";
                    foreach($reasonTable as $reason){
                        $x16 .= "	<td style='text-align: right'></td>";
                    }
                    $x16 .= "</tr>";
                    $x16 .= "</tbody>";
                }
            }
            $x16 .= "</table></div><br/>";
            
        }
        //======================================================================== Teaching Experience ========================================================================//
        if($preDefHeader[17]["Incl"]==1){
            $x17 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[17]["Descr"])."<br/>\r\n";
            
            $sql = "SELECT RecordID,BatchID,SeqID,SchoolName,StartDate,EndDate,Post
					FROM INTRANET_PA_T_PRE_TEACHEREXP WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."';";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $runningIndex = 0;
            
            $x17 .= "<div id=\"teachingExpLayer\">";
            $x17 .= "<table id=\"teachingExpTable\" class=\"common_table_list\">";
            $x17 .= "<thead>";
            $x17 .= "<tr>";
            $x17 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormExpSchoolName']."</th>";
            $x17 .= "<th style=\"width:40%\">".$Lang['Appraisal']['ARFormExpDate']."</th>";
            $x17 .= "<th style=\"width:25%\">".$Lang['Appraisal']['ARFormExpPost']."</th>";
            $x17 .= "<th style=\"width:5%\"></th>";
            $x17 .= "</tr></thead>";
            $x17 .= "<tbody>";
            
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x17 .= "<tr id=\"teachingExpRow_".$j."\">";
                $x17 .= "<td>";
                $x17 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('expSchoolName_'.$j,'expSchoolName_'.$j,$a[$i]["SchoolName"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x17 .= "</td>";
                $x17 .= "<td>";
                $x17 .= $Lang['General']['From'].$indexVar['libappraisal_ui']->GET_DATE_PICKER('expStartDate_'.$j,$a[$i]["StartDate"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('expStartDate_".$j."')",$ID='expStartDate_'.$j,$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
                $x17 .= $Lang['General']['To'].$indexVar['libappraisal_ui']->GET_DATE_PICKER('expEndDate_'.$j,$a[$i]["EndDate"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('expEndDate_".$j."')",$ID='expEndDate_'.$j,$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum")."<br>\n\r";
                $x17 .= "</td>";
                $x17 .= "<td>";
                $x17 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('expPost_'.$j,'expPost_'.$j,$a[$i]["Post"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x17 .= "</td>";
                $x17 .= "<td>";
                $x17 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                $x17 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteTeachingExp('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                $x17 .= "</span>";
                $x17 .= "<input type=\"hidden\" id=\"hiddenTeachingExpSts_".$j."\" name=\"hiddenTeachingExpSts_".$j."\" VALUE=\"ACTIVE\">";
                $x17 .= "<input type=\"hidden\" id=\"hiddenTeachingExpSeqID_".$j."\" name=\"hiddenTeachingExpSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
                $x17 .= "</td>";
                $x17 .= "</tr>";
                $runningIndex++;
            }
            for($i=sizeof($a);$i<sizeof($a)+10;$i++){
                $j=$i+1;
                $x17 .= "<tr id=\"teachingExpRow_".$j."\" style=\"display:none;\">";
                $x17 .= "<td>";
                $x17 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('expSchoolName_'.$j,'expSchoolName_'.$j,$a[$i]["SchoolName"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x17 .= "</td>";
                $x17 .= "<td>";
                $x17 .= $Lang['General']['From'].$indexVar['libappraisal_ui']->GET_DATE_PICKER('expStartDate_'.$j,$a[$i]["StartDate"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('expStartDate_".$j."')",$ID='expStartDate_'.$j,$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
                $x17 .= $Lang['General']['To'].$indexVar['libappraisal_ui']->GET_DATE_PICKER('expEndDate_'.$j,$a[$i]["EndDate"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('expEndDate_".$j."')",$ID='expEndDate_'.$j,$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum")."<br>\n\r";
                $x17 .= "</td>";
                $x17 .= "<td>";
                $x17 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('expPost_'.$j,'expPost_'.$j,$a[$i]["Post"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x17 .= "</td>";
                $x17 .= "<td>";
                $x17 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                $x17 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteTeachingExp('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                $x17 .= "</span>";
                $x17 .= "<input type=\"hidden\" id=\"hiddenStudentTrainingSts_".$j."\" name=\"hiddenStudentTrainingSts_".$j."\" VALUE=\"ACTIVE\">";
                $x17 .= "<input type=\"hidden\" id=\"hiddenStudentTrainingSeqID_".$j."\" name=\"hiddenStudentTrainingSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
                $x17 .= "</td>";
                $x17 .= "</tr>";
                $runningIndex++;
            }
            $x17 .= "<tr height=\"34px\"><td></td><td></td><td></td><td>";
            $x17 .= "<div class=\"table_row_tool row_content_tool\">";
            $x17 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addTeachingExp('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
            $x17 .= "<input type=\"hidden\" id=\"currentTeachingExpRow\" name=\"currentTeachingExpRow\" VALUE=\"".sizeof($a)."\">";
            $x17 .= "<input type=\"hidden\" id=\"totalTeachingExpRow\" name=\"totalTeachingExpRow\" VALUE=\"".$runningIndex."\">";
            $x17 .= "</td></tr>";
            $x17 .= "</tbody>";
            $x17 .= "</table>";
            $x17 .= "</div><br/>";
        }
        //======================================================================== Academic and Teaching Professional training ========================================================================//
        if($preDefHeader[18]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,SeqID,DATE_FORMAT(StartDate,'%Y-%m-%d') as StartDate,DATE_FORMAT(EndDate,'%Y-%m-%d') as EndDate,Issuer,QualiDescr,Programme
			FROM INTRANET_PA_T_PRE_TRAINING  WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."';";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x18 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[18]["Descr"])."<br/>\r\n";
            
            $x18 .= "<div id=\"trainingInfoLayer\">";
            $x18 .= "<table id=\"trainingInfoTable\" class=\"common_table_list\">";
            $x18 .= "<thead>";
            $x18 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormTrainingUni']."</th>";
            $x18 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormTrainingDate']."</th>";
            $x18 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormTrainingAward']."</th>";
            $x18 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormTrainingSbj']."</th>";
            $x18 .= "<th style=\"width:10%\"></th>";
            $x18 .= "</tr></thead>";
            $x18 .= "<tbody>";
            $runningIndex = 0;
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x18 .= "<tr id=\"trainingRow_".$j."\">";
                $x18 .= "<td>";
                $x18 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('trainingIssuer_'.$j, 'trainingIssuer_'.$j, $a[$i]["Issuer"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x18 .= "</td>";
                $x18 .= "<td>";
                $x18 .= $Lang['General']['From'].$indexVar['libappraisal_ui']->GET_DATE_PICKER('trainingStartDate_'.$j,$a[$i]["StartDate"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('trainingStartDate_".$j."')",$ID='trainingStartDate_'.$j,$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
                $x18 .= $Lang['General']['To'].$indexVar['libappraisal_ui']->GET_DATE_PICKER('trainingEndDate_'.$j,$a[$i]["EndDate"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('trainingEndDate_".$j."')",$ID='trainingEndDate_'.$j,$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
                $x18 .= "</td>";
                $x18 .= "<td>";
                $x18 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('trainingDescr_'.$j, 'trainingDescr_'.$j, $a[$i]["QualiDescr"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x18 .= "</td>";
                $x18 .= "<td>";
                $x18 .= $indexVar['libappraisal_ui']->GET_TEXTBOX('trainingProgramme_'.$j, 'trainingProgramme_'.$j, $a[$i]["Programme"], $OtherClass='', $OtherPar=array('maxlength'=>255));
                $x18 .= "</td>";
                $x18 .= "<td>";
                $x18 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                $x18 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteTraining('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                $x18 .= "</span>";
                $x18 .= "<input type=\"hidden\" id=\"hiddenTraining_".$j."\" name=\"hiddenTraining_".$j."\" VALUE=\"ACTIVE\">";
                $x18 .= "<input type=\"hidden\" id=\"hiddenTrainingSeqID_".$j."\" name=\"hiddenTrainingSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
                $x18 .= "</td>";
                $x18 .= "</tr>";
                $runningIndex++;
            }
            $x18 .= "<tr height=\"34px\"><td></td><td></td><td></td><td></td><td>";
            $x18 .= "<div class=\"table_row_tool row_content_tool\">";
            $x18 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addTraining('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
            $x18 .= "<input type=\"hidden\" id=\"currentTrainingRow\" name=\"currentTrainingRow\" VALUE=\"".sizeof($a)."\">";
            $x18 .= "<input type=\"hidden\" id=\"totalTrainingRow\" name=\"totalTrainingRow\" VALUE=\"".$runningIndex."\">";
            $x18 .= "</td></tr>";
            $x18 .= "</tbody>";
            $x18 .= "</table>";
            $x18 .= "</div><br/>";
        }
        //======================================================================== Sick Leave & lateness records ========================================================================//
        if($preDefHeader[19]["Incl"]==1){
            $sql = "SELECT SLLastYear,SLThisYear,LateLastYear,LateThisYear,IronMan,EarlyBird FROM INTRANET_PA_C_ATTENDANCE WHERE CycleID='".$cycleID."' AND UserID='".$userID."'";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x19 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[19]["Descr"])."<br/>\r\n";
            
            $x19 .= "<div id=\"trainingInfoLayer\">";
            $x19 .= "<table id=\"trainingInfoTable\" class=\"common_table_list\">";
            $x19 .= "<thead><tr>";
            $x19 .= "<th style=\"width:20%;text-align: center;\" colspan=2>".$Lang['Appraisal']['SickLeaveRecord']."</th>";
            $x19 .= "<th style=\"width:20%;text-align: center;\" colspan=2>".$Lang['Appraisal']['LatenessRecord']."</th>";
            $x19 .= "<th style=\"width:10%;text-align: center;\">".$Lang['Appraisal']['IronMan']."</th>";
            $x19 .= "<th style=\"width:10%;text-align: center;\">".$Lang['Appraisal']['EarlyBird']."</th>";
            $x19 .= "</tr><tr>";
            $x19 .= "<th style=\"width:20%\">".$Lang['Appraisal']['LastAcademicYear']."</th>";
            $x19 .= "<th style=\"width:20%\">".$Lang['Appraisal']['CurrentAcademicYearTilMar']."</th>";
            $x19 .= "<th style=\"width:20%\">".$Lang['Appraisal']['LastAcademicYear']."</th>";
            $x19 .= "<th style=\"width:20%\">".$Lang['Appraisal']['CurrentAcademicYearTilMar']."</th>";
            $x19 .= "<th style=\"width:10%\">".$Lang['Appraisal']['LastAcademicYear']."</th>";
            $x19 .= "<th style=\"width:10%\">".$Lang['Appraisal']['LastAcademicYear']."</th>";
            $x19 .= "</tr></thead>";
            $x19 .= "<tbody>";
            $x19 .= "<tr height=\"34px\">";
            $x19 .= "<td>".$a[0]['SLLastYear']."</td>";
            $x19 .= "<td>".$a[0]['SLThisYear']."</td>";
            $x19 .= "<td>".$a[0]['LateLastYear']."</td>";
            $x19 .= "<td>".$a[0]['LateThisYear']."</td>";
            $x19 .= "<td>".$a[0]['IronMan']."</td>";
            $x19 .= "<td>".$a[0]['EarlyBird']."</td>";
            $x19 .= "</tr>";
            $x19 .= "</tbody>";
            $x19 .= "</table>";
            $x19 .= "</div><br/>";
        }
        function headerSortByDisplayOrder($a, $b){
            if($a['DisplayOrder'] == $b['DisplayOrder']) return 0;
            return ($a['DisplayOrder'] < $b['DisplayOrder'])?-1:1;
        }
        usort($preDefHeader, 'headerSortByDisplayOrder');
        foreach($preDefHeader as $pdh){
            switch($pdh["DataTypeID"]){
                case "1":
                    $content .= $x0;
                    break;
                case "2":
                    $content .= $x1;
                    break;
                case "3":
                    $content .= $x2;
                    break;
                case "4":
                    $content .= $x3;
                    break;
                case "5":
                    $content .= $x4;
                    break;
                case "6":
                    $content .= $x5;
                    break;
                case "7":
                    $content .= $x6;
                    break;
                case "8":
                    $content .= $x7;
                    break;
                case "9":
                    $content .= $x8;
                    break;
                case "10":
                    $content .= $x9;
                    break;
                case "11":
                    $content .= $x10;
                    break;
                case "12":
                    $content .= $x11;
                    break;
                case "13":
                    $content .= $x12;
                    break;
                case "14":
                    $content .= $x13;
                    break;
                case "15":
                    $content .= $x14;
                    break;
                case "16":
                    $content .= $x15;
                    break;
                case "17":
                    $content .= $x16;
                    break;
                case "18":
                    $content .= $x17;
                    break;
                case "19":
                    $content .= $x18;
                    break;
                case "20":
                    $content .= $x19;
                    break;
            }
            
        }
        $x .= $content;
        return $x;
        
    }
    
    function getPreFormPrint($connection,$templateID,$userID,$recordID,$cycleID,$rlsNo,$batchID, $isPrint){
        $indexVar['libappraisal'] = new libappraisal();
        $indexVar['libappraisal_ui'] = new libappraisal_ui();
        $json = new JSON_obj();
        global $intranet_session_language, $Lang, $ColForPreSetField, $appraisalConfig, $sys_custom;
        
        if($userID==""&&$recordID==""&&$cycleID==""&&$rlsNo==""&&$batchID==""){
            $sql="SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormTitle,"
                .$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode,"
                    .$indexVar['libappraisal']->getLangSQL("HdrRefChi","HdrRefEng")." as HdrRef,"
                        .$indexVar['libappraisal']->getLangSQL("ObjChi","ObjEng")." as Obj,"
                            .$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,
				AppTgtChi,AppTgtEng
				FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".IntegerSafe($templateID)."'";
        }
        else{
            $sql="SELECT iptf.RecordID,UserID,iptf.CycleID,CycleDescr,iptf.TemplateID,FormTitle,FormCode,HdrRef,Obj,Descr,AppTgtChi,AppTgtEng,AcademicYearID,COALESCE(SubDate,'') as SubDate,
					EditPrdFr,EditPrdTo,CASE WHEN NOW()>EditPrdTo THEN '1' ELSE '0' END as OverdueDate,RlsNo,CycleStart,CycleClose
					FROM(
						SELECT RecordID,CycleID,TemplateID,UserID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".IntegerSafe($recordID)."'
					) as iptf
					INNER JOIN(
						SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormTitle,"
						    .$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode,"
						        .$indexVar['libappraisal']->getLangSQL("HdrRefChi","HdrRefEng")." as HdrRef,"
						            .$indexVar['libappraisal']->getLangSQL("ObjChi","ObjEng")." as Obj,"
						                .$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,
						AppTgtChi,AppTgtEng
						FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".IntegerSafe($templateID)."'
					) as ipsf ON iptf.TemplateID=ipsf.TemplateID
					INNER JOIN(
						SELECT CycleID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as CycleDescr,AcademicYearID,DATE_FORMAT(CycleStart,'%Y-%m-%d') as CycleStart,DATE_FORMAT(CycleClose,'%Y-%m-%d') as CycleClose
						FROM INTRANET_PA_C_CYCLE
					) as ipcc ON iptf.CycleID=ipcc.CycleID
					INNER JOIN(
						SELECT BatchID,CycleID,EditPrdFr,EditPrdTo FROM INTRANET_PA_C_BATCH
					) as ipcb ON ipcc.CycleID=ipcb.CycleID
					INNER JOIN(
						SELECT RlsNo,RecordID,BatchID,SubDate FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
					) as iptfrmSub ON iptf.RecordID=iptfrmSub.RecordID AND ipcb.BatchID=iptfrmSub.BatchID
				";
        }
        //echo $sql."<br/><br/>";
        $a=$connection->returnResultSet($sql);
        //======================================================================== Pre-defined form header ========================================================================//
        $sql = "SELECT TemplateID,DataTypeID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,DisplayOrder,Incl,PageBreakAfter
			FROM INTRANET_PA_S_FRMPRE WHERE TemplateID='".IntegerSafe($templateID)."'
			ORDER BY DataTypeID,DisplayOrder
			";
        //echo $sql."<br/><br/>";
        $preDefHeader=$connection->returnResultSet($sql);
        $cycleID = $a[0]["CycleID"];
        $userID = $a[0]["UserID"];
        $academicYearID=$a[0]["AcademicYearID"];
        $cycleStart=$a[0]["CycleStart"];
        $cycleClose=$a[0]["CycleClose"];
        
        $activeForm = false;
        if(strtotime($cycleStart) <= time() && strtotime($cycleClose) >= time()){
            $activeForm = true;
        }
        
        if($sys_custom['eAppraisal']['LTMPSPrintingStyle']){
            $x .= "<div class=\"formTitle\" style=\"text-align: center;\">";
            $x .= "<b>".$a[0]["FormTitle"]."</b>";
            $x .= "</div>"."\r\n";
        }else{
            $x .= "<div class=\"formTitle\">";
            $x .= $a[0]["FormTitle"];
            if($a[0]["CycleDescr"] != ""){
                $x .= "(".$a[0]["CycleDescr"].")";
            }
            $sql = "Select CycleClose From INTRANET_PA_C_CYCLE WHERE CycleID='".$cycleID."'";
            $c=$connection->returnResultSet($sql);
            if(strtotime($c[0]['CycleClose']) > time()){
                $x .= " ".$Lang['Appraisal']['Report']['ReportBeforeCycleEnd'];
            }
            $x .= "</div><br/>"."\r\n";
        }
        if($sys_custom['eAppraisal']['LTMPSPrintingStyle']){
            $x .= "<div>".
                "<span class=\"formObjective\" style=\"float: left;\">".$a[0]["FormCode"]."</span>".
                "</div><br/>"."\r\n";
        }else{
            $x .= "<div>".
                "<span class=\"formObjective\" style=\"float: left;\">".$a[0]["FormCode"]." - ".$a[0]["Obj"]."</span>".
                "<span class=\"formRefHeader\" style=\"float: right;\">".$a[0]["HdrRef"]."</span>".
                "</div><br/><br/>"."\r\n";
            $x .= "<div>".$a[0]["Descr"]."</div><br/>"."\r\n";
        }
        //======================================================================== Personal Master ========================================================================//
        if(!$sys_custom['eAppraisal']['LTMPSPrintingStyle']){
            $sql = "SELECT UserID,EnglishName,ChineseName,Gender
				FROM(
					SELECT UserID,EnglishName,ChineseName,Gender FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".IntegerSafe($a[0]["UserID"])."'
				) as iu";
            $iu=$connection->returnResultSet($sql);
            $tgtName = ($_SESSION['intranet_session_language']=="en")?$Lang['Appraisal']['ARFormTgtName']." ".$a[0]["AppTgtEng"]:$a[0]["AppTgtChi"].$Lang['Appraisal']['ARFormTgtName'];
            $x .= "<table class=\"form_table_v30\">";
            $x .= "<tr><td class=\"field_title\">".$tgtName."</td><td>".$iu[0]["EnglishName"];
            if($iu[0]["ChineseName"]!=""){
                $x .= "(".$iu[0]["ChineseName"].")";
            }
            $x .= "</td></tr>"."\r\n";
            $x .= "</table><br/>"."\r\n";
        }
        //======================================================================== Personal Pariculars ========================================================================//
        if($preDefHeader[0]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,NameChi,NameEng,Gender,DATE_FORMAT(DateOfBirth, '%Y-%m-%d') as DateOfBirth,Religion,MaritalStatus,AddressChi,AddressEng,PhoneNo,Publication,ExtServ,TotClsCnt
			FROM INTRANET_PA_T_PRE_GEN WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			";
            $perPar=$connection->returnResultSet($sql);
            $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr, Incl, DataSubTypeID FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID='".IntegerSafe($templateID)."' AND DataTypeID=1 ORDER BY DisplayOrder";
            $subSectionPar = $connection->returnResultSet($sql);
            $gender = ($perPar[0]["Gender"]=="M")?$Lang['Appraisal']['ARFormMale']:(($perPar[0]["Gender"]=="F")?$Lang['Appraisal']['ARFormFemale']:"");
            $x0 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[0]["Descr"])."<br/>\r\n";
            $x0 .= "<table class=\"form_table_v30 personal_details_form\">";
            if($sys_custom['eAppraisal']['LTMPSPrintingStyle']){
                preg_match('/(\w+)*(\d+ )/m', $perPar[0]["NameEng"], $teachersData);
                $perPar[0]["NameEng"] = preg_replace('/(\w+)*(\d+ )/m', '', $perPar[0]["NameEng"]);
                $perPar[0]["NameChi"] = preg_replace('/(\w+)*(\d+ )/m', '', $perPar[0]["NameChi"]);
                $teacherID = $teachersData[0];
            }
            $peronalIndex = 0;
            foreach($subSectionPar as $subsection){
                if($subsection['Incl']==false)continue;
                $peronalIndex++;
                if($peronalIndex%2==1){
                    $x0 .= "<tr>";
                }
                switch($subsection['DataSubTypeID']){
                    case 1:
                        $x0 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$perPar[0]["NameEng"]."</td>";
                        break;
                    case 2:
                        $x0 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$perPar[0]["NameChi"]."</td>";
                        break;
                    case 3:
                        $x0 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$gender."</td>";
                        break;
                    case 4:
                        $x0 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$perPar[0]["DateOfBirth"]."</td>";
                        break;
                    case 5:
                        $x0 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$perPar[0]["Religion"]."</td>";
                        break;
                    case 6:
                        $x0 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$perPar[0]["MaritalStatus"]."</td>";
                        break;
                    case 7:
                        $x0 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$perPar[0]["AddressChi"]."</td>";
                        break;
                    case 8:
                        $x0 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$perPar[0]["AddressEng"]."</td>";
                        break;
                    case 9:
                        $x0 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$perPar[0]["PhoneNo"]."</td>";
                        break;
                }
                if($peronalIndex%2==0){
                    $x0 .= "</tr>\r\n";
                }
            }
            if($sys_custom['eAppraisal']['LTMPSPrintingStyle']){
                $peronalIndex++;
                if($peronalIndex%2==1){
                    $x0 .= "<tr>";
                }
                $x0 .= "<td class=\"field_title\">教師編號</td><td>".$teacherID."</td>";
                if($peronalIndex%2==0){
                    $x0 .= "</tr>\r\n";
                }
            }
            if($peronalIndex%2==1){
                $x0 .= "</tr>\n\r";
            }
            $x0 .= "</table><br/>"."\r\n";
            
            if($preDefHeader[0]["PageBreakAfter"]==1){
                $x0 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Present Job Information ========================================================================//
        if($preDefHeader[1]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,JobGrade,DATE_FORMAT(GradeEntDate,'%Y-%m-%d') as GradeEntDate,DATE_FORMAT(HireDateSch,'%Y-%m-%d') as HireDateSch,DATE_FORMAT(HireDateOrgn,'%Y-%m-%d') as HireDateOrgn,SenGov,SenPri,SenOrgn,RegNo,SalaryPoint,ActingPosition,LongHoliday,PositionOnEntry
			FROM INTRANET_PA_T_PRE_GEN WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            
            $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr, Incl, DataSubTypeID FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID='".IntegerSafe($templateID)."' AND DataTypeID=2 ORDER BY DisplayOrder";
            $subSectionPar = $connection->returnResultSet($sql);
            
            $x1 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[1]["Descr"])."</b><br/>\r\n";
            $x1 .= "<table class=\"form_table_v30 personal_details_form\">";
            $presetIndex = 0;
            foreach($subSectionPar as $subsection){
                if($subsection['Incl']==false)continue;
                $presetIndex++;
                if($presetIndex%2==1){
                    $x1 .= "<tr>";
                }
                switch($subsection['DataSubTypeID']){
                    case 1:
                        $x1 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$a[0]["JobGrade"]."</td>";
                        break;
                    case 2:
                        $x1 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$a[0]["GradeEntDate"]."</td>";
                        break;
                    case 3:
                        $x1 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$a[0]["HireDateSch"]."</td>";
                        break;
                    case 4:
                        $x1 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$a[0]["HireDateOrgn"]."</td>";
                        break;
                    case 5:
                        $x1 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$a[0]["SenGov"]."</td>";
                        break;
                    case 6:
                        $x1 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$a[0]["SenPri"]."</td>";
                        break;
                    case 7:
                        $x1 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$a[0]["SenOrgn"]."</td>";
                        break;
                    case 8:
                        $x1 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$a[0]["RegNo"]."</td>";
                        break;
                    case 9:
                        $x1 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$a[0]["SalaryPoint"]."</td>";
                        break;
                    case 10:
                        $x1 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$a[0]["ActingPosition"]."</td>";
                        break;
                    case 11:
                        if($a[0]["LongHoliday"] == ""){
                            $holidayStart = "";
                            $holidayEnd = "";
                            $holidayType = "";
                            $holidayDuration = "";
                        }else{
                            $longHolidayArr = unserialize($a[0]["LongHoliday"]);
                            $holidayStart = $longHolidayArr['holidayStart'];
                            $holidayEnd = $longHolidayArr['holidayEnd'];
                            $holidayType = $longHolidayArr['holidayType'];
                            $holidayDuration = $longHolidayArr['holidayDuration'];
                        }
                        $x1 .= "<td class=\"field_title\">".$subsection['Descr']."</td>";
                        $x1 .= "<td>".$Lang['General']['From']." ".$holidayStart." ";
                        $x1 .= $Lang['General']['To']." ".$holidayEnd."<br>\n\r";
                        $x1 .= $Lang['Appraisal']['ARFormNature'].":".$holidayType."\t\t";
                        $x1 .= $Lang['Appraisal']['CycleExportLeaveChi']['Days'].":".$holidayDuration;
                        $x1 .= "</td>";
                        break;
                    case 12:
                        $x1 .= "<td class=\"field_title\">".$subsection['Descr']."</td><td>".$a[0]["PositionOnEntry"]."</td>";
                        break;
                }
                if($presetIndex%2==0){
                    $x1 .= "</tr>\n\r";
                }
            }
            if($presetIndex%2==1){
                $x1 .= "</tr>\n\r";
            }
            $x1 .= "</table><br/>"."\r\n";
            
            if($preDefHeader[1]["PageBreakAfter"]==1){
                $x1 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Qualification Acquired/ Study-in-progress during appraising period ========================================================================//
        if($preDefHeader[2]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,SeqID,IsCompleted,DATE_FORMAT(IssueDate,'%Y-%m-%d') as IssueDate,Issuer,QualiDescr,Programme
			FROM INTRANET_PA_T_PRE_QUALI WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."';";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x2 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[2]["Descr"])."</b><br/>\r\n";
            
            $x2 .= "<div id=\"quaAcqInfoLayer\">";
            $x2 .= "<table id=\"quaAcqInfoTable\" class=\"common_table_list\">";
            $x2 .= "<thead>";
            $x2 .= "<tr><th style=\"width:2%\">".$Lang['Appraisal']['ARFormSeq']."</th><th style=\"width:20%\">".$Lang['Appraisal']['ARFormQuaAcqQuaDesc']."</th>";
            $x2 .= "<th style=\"width:16%\">".$Lang['Appraisal']['ARFormQuaAcqSts']."</th><th style=\"width:17%\">".$Lang['Appraisal']['ARFormQuaAcqCmpDat']."</th>";
            $x2 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormQuaAcqAccBody']."</th><th style=\"width:20%\">".$Lang['Appraisal']['ARFormQuaAcqPrgDtl']."</th>";
            $x2 .= "<th style=\"width:10%\"></th>";
            $x2 .= "</tr></thead>";
            $x2 .= "<tbody>";
            $runningIndex = 0;
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x2 .= "<tr id=\"qualificationRow_".$j."\">";
                $x2 .= "<td>";
                $x2 .= "<div id=\"seqID_".$a[$i]["SeqID"]."\" name=\"seqID_".$a[$i]["SeqID"]."\">";
                $x2 .= $a[$i]["SeqID"];
                $x2 .= "</div>";
                $x2 .= "</td>";
                $x2 .= "<td>";
                $x2 .= $a[$i]["QualiDescr"];
                $x2 .= "</td>";
                $x2 .= "<td>";
                $x2 .= $Lang['Appraisal']['ARFormQuaAcqStsDes'][$a[$i]["IsCompleted"]];
                $x2 .= "</td>";
                $x2 .= "<td>";
                $x2 .= $a[$i]["IssueDate"];
                $x2 .= "</td>";
                $x2 .= "<td>";
                $x2 .= $a[$i]["Issuer"];
                $x2 .= "</td>";
                $x2 .= "<td>";
                $x2 .= $a[$i]["Programme"];
                $x2 .= "</td>";
                $x2 .= "<td>";
                $x2 .= "<input type=\"hidden\" id=\"hiddenQualification_".$j."\" name=\"hiddenQualification_".$j."\" VALUE=\"ACTIVE\">";
                $x2 .= "<input type=\"hidden\" id=\"hiddenSeqID_".$j."\" name=\"hiddenSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
                $x2 .= "</td>";
                $x2 .= "</tr>";
                $runningIndex++;
            }
            $x2 .= "</tbody>";
            $x2 .= "</table>";
            $x2 .= "</div><br/>";
            if($sys_custom['eAppraisal']['HidePublishingThisYear']!=true || $sys_custom['eAppraisal']['HideServiceOutsideSchool']!=true){
                $x2 .= "<table class=\"form_table_v30\">";
                if($sys_custom['eAppraisal']['HidePublishingThisYear']!=true){
                    $x2 .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormPubYear']."</td><td>".$perPar[0]["Publication"]."</td></tr>"."\r\n";
                }
                if($sys_custom['eAppraisal']['HideServiceOutsideSchool']!=true){
                    $x2 .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormExtServ']."</td><td>".$perPar[0]["ExtServ"]."</td></tr>"."\r\n";
                }
                $x2 .= "</table><br/>";
            }
            if($preDefHeader[2]["PageBreakAfter"]==1){
                $x2 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Present Teaching Duties per Class ========================================================================//
        if($preDefHeader[3]["Incl"]==1){
            $x3 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[3]["Descr"])."</b><br/>\r\n";
            if ($indexVar['libappraisal']->isEJ()) {
                $sql = "SELECT iptpt.RecordID,BatchID,iptpt.YearClassID,SubjectID,Period,DateTimeInput,IFNULL(SName,SubjectID) as SName,ClassTitle
						FROM(
							SELECT RecordID,BatchID,YearClassID,SubjectID,Period,DateTimeInput
							FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
						) as iptpt
						LEFT JOIN(
							SELECT SubjectID as RecordID, SubjectName as SName
							FROM INTRANET_SUBJECT WHERE RecordStatus = 1
						) as assub ON iptpt.SubjectID=assub.RecordID
						LEFT JOIN(
							SELECT ClassID as YearClassID, ClassName as ClassTitle
							FROM INTRANET_CLASS
						) as yc ON iptpt.YearClassID=yc.YearClassID
						ORDER BY DateTimeInput";
            }else{
                $sql = "SELECT iptpt.RecordID,BatchID,iptpt.YearClassID,SubjectID,Period,DateTimeInput,IFNULL(SName,SubjectID) as SName,ClassTitle
						FROM(
							SELECT RecordID,BatchID,YearClassID,SubjectID,Period,DateTimeInput
							FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
						) as iptpt
						LEFT JOIN(
							SELECT RecordID,CODEID,".$indexVar['libappraisal']->getLangSQL("CH_SNAME","EN_SNAME")." as SName
							FROM ASSESSMENT_SUBJECT
						) as assub ON iptpt.SubjectID=assub.RecordID
						LEFT JOIN(
							SELECT YearClassID,AcademicYearID,".$indexVar['libappraisal']->getLangSQL("ClassTitleB5","ClassTitleEN")." as ClassTitle
							FROM YEAR_CLASS
						) as yc ON iptpt.YearClassID=yc.YearClassID
						ORDER BY DateTimeInput";
            }
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $runningIndex = 0;
            $x3 .= "<table class=\"form_table_v30\">";
            $x3 .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormTotClsCnt']."</td><td>".$perPar[0]["TotClsCnt"]."</td></tr>"."\r\n";
            $x3 .= "</table><br/>";
            //$x3 .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($preDefHeader[4]["Descr"])."<br/>\r\n";
            $x3 .= "<div id=\"teachingClassLayer\">";
            $x3 .= "<table id=\"teachingClassTable\" class=\"common_table_list\">";
            $x3 .= "<thead>";
            $x3 .= "<tr><th style=\"width:30%\">".$Lang['Appraisal']['ARFormCls']."</th><th style=\"width:30%\">".$Lang['Appraisal']['ARFormTeachSubj']."</th>";
            $x3 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormClsPerCyc']."</th>";
            $x3 .= "<th style=\"width:10%\"></th>";
            $x3 .= "</tr></thead>";
            $x3 .= "<tbody>";
            
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x3 .= "<tr id=\"teachClassRow_".$j."\">";
                $x3 .= "<td>".$indexVar['libappraisal']->displayChinese($a[$i]["ClassTitle"])."</td>";
                $x3 .= "<td >".$indexVar['libappraisal']->displayChinese($a[$i]["SName"])."</td>";
                $x3 .= "<td>";
                $x3 .= $a[$i]["Period"];
                $x3 .= "</td>";
                $x3 .= "<td>";
                $x3 .= "</td>";
                $x3 .= "<input type=\"hidden\" id=\"hiddenYearClass_".$j."\" name=\"hiddenYearClass_".$j."\" VALUE=\"".$a[$i]["YearClassID"]."\">";
                $x3 .= "<input type=\"hidden\" id=\"hiddenSubject_".$j."\" name=\"hiddenSubject_".$j."\" VALUE=\"".$a[$i]["SubjectID"]."\">";
                $x3 .= "<input type=\"hidden\" id=\"hiddenClass_".$j."\" name=\"hiddenClass_".$j."\" VALUE=\"ACTIVE\">";
                $x3 .= "</tr>";
                $runningIndex++;
            }
            $x3 .= "</tbody>";
            $x3 .= "</table>";
            $x3 .= "</div><br/>";
            //			if($sys_custom['eAppraisal']['displayTeachClassesInSelTable'] ){
            //				$subjectList = array();
            //				$classList = array();
            //				if ($indexVar['libappraisal']->isEJ()) {
            //					$sql = "SELECT SubjectID as RecordID, SubjectName as SName FROM INTRANET_SUBJECT WHERE RecordStatus = 1";
            //					$subjectList = $connection->returnArray($sql);
            //					$sql = "SELECT ClassID as YearClassID, ClassName as ClassTitle FROM INTRANET_CLASS";
            //					$classList = $connection->returnArray($sql);
            //				}else{
            //					$sql = "SELECT RecordID,CODEID,".$indexVar['libappraisal']->getLangSQL("CH_SNAME","EN_SNAME")." as SName FROM ASSESSMENT_SUBJECT";
            //					$subjectList = $connection->returnArray($sql);
            //					$sql = "SELECT YearClassID,AcademicYearID,".$indexVar['libappraisal']->getLangSQL("ClassTitleB5","ClassTitleEN")." as ClassTitle FROM YEAR_CLASS";
            //					$classList = $connection->returnArray($sql);
            //				}
            //				$teachClassSbjData = array();
            //				for($i=0;$i<sizeof($a);$i++){
            //					$teachClassSbjData[$a[$i]['SubjectID']][$a[$i]['YearClassID']] = $a[$i]['Period'];
            //				}
            //				$x3 .= "<div id=\"teachingClassLayer\">";
            //				$x3 .= "<table id=\"teachingClassTable\" class=\"common_table_list\">";
            //				$x3 .= "<thead>";
            //				$x3 .= "<tr>";
            //				$x3 .= "<th style=\"width:".(100/(count($classList)+1))."%\"></th>";
            //				foreach($classList as $classes){
            //					$x3 .= "<th style=\"width:".(100/(count($classList)+1))."%\">".$classes['ClassTitle']."</th>";
            //				}
            //				$x3 .= "</tr></thead>";
            //				$x3 .= "<tbody>";
            //				foreach($subjectList as $subject){
            //					$x3 .= "<tr>";
            //					$x3 .= "<td>".$subject['SName']."</td>";
            //					foreach($classList as $classes){
            //						$x3 .= "<td>";
            //						$x3 .= (isset($teachClassSbjData[$subject['RecordID']][$classes['YearClassID']]))?"&#10004;":"";
            //						//$x3 .= (isset($teachClassSbjData[$subject['RecordID']][$classes['YearClassID']]))?"Y":"";
            //						$x3 .= "</td>";
            //					}
            //					$x3 .= "</tr>";
            //				}
            //				$x3 .= "</tbody>";
            //				$x3 .= "</table>";
            //				$x3 .= "</div><br/>";
            //
            //			}else{
            //				$x3 .= "<table class=\"form_table_v30\">";
            //				$x3 .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormTotClsCnt']."</td><td>".$perPar[0]["TotClsCnt"]."</td></tr>"."\r\n";
            //				$x3 .= "</table><br/>";
            //				//$x3 .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($preDefHeader[4]["Descr"])."<br/>\r\n";
            //				$x3 .= "<div id=\"teachingClassLayer\">";
            //				$x3 .= "<table id=\"teachingClassTable\" class=\"common_table_list\">";
            //				$x3 .= "<thead>";
            //				$x3 .= "<tr><th style=\"width:30%\">".$Lang['Appraisal']['ARFormCls']."</th><th style=\"width:30%\">".$Lang['Appraisal']['ARFormTeachSubj']."</th>";
            //				$x3 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormClsPerCyc']."</th>";
            //				$x3 .= "<th style=\"width:10%\"></th>";
            //				$x3 .= "</tr></thead>";
            //				$x3 .= "<tbody>";
            //
            //				for($i=0;$i<sizeof($a);$i++){
            //					$j=$i+1;
            //					$x3 .= "<tr id=\"teachClassRow_".$j."\">";
            //					$x3 .= "<td>".$indexVar['libappraisal']->displayChinese($a[$i]["ClassTitle"])."</td>";
            //					$x3 .= "<td >".$indexVar['libappraisal']->displayChinese($a[$i]["SName"])."</td>";
            //					$x3 .= "<td>";
            //					$x3 .= $a[$i]["Period"];
            //					$x3 .= "</td>";
            //					$x3 .= "<td>";
            //					$x3 .= "</td>";
            //					$x3 .= "<input type=\"hidden\" id=\"hiddenYearClass_".$j."\" name=\"hiddenYearClass_".$j."\" VALUE=\"".$a[$i]["YearClassID"]."\">";
            //					$x3 .= "<input type=\"hidden\" id=\"hiddenSubject_".$j."\" name=\"hiddenSubject_".$j."\" VALUE=\"".$a[$i]["SubjectID"]."\">";
            //					$x3 .= "<input type=\"hidden\" id=\"hiddenClass_".$j."\" name=\"hiddenClass_".$j."\" VALUE=\"ACTIVE\">";
            //					$x3 .= "</tr>";
            //					$runningIndex++;
            //				}
            //				$x3 .= "</tbody>";
            //				$x3 .= "</table>";
            //				$x3 .= "</div><br/>";
            //			}
            if($preDefHeader[3]["PageBreakAfter"]==1){
                $x3 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Present Teaching Duties per Form ========================================================================//
        if($preDefHeader[4]["Incl"]==1){
            $x4 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[4]["Descr"])."</b><br/>\r\n";
            $x4 .= "<table class=\"form_table_v30\">";
            $x4 .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormTotClsCnt']."</td><td>".$perPar[0]["TotClsCnt"]."</td></tr>"."\r\n";
            $x4 .= "</table><br/>";
            if ($indexVar['libappraisal']->isEJ()) {
                $sql = "SELECT iptpt.RecordID,BatchID,iptpt.YearID,SubjectID,Period,DateTimeInput,SName,YearName
						FROM(
							SELECT RecordID,BatchID,YearID,SubjectID,Period,DateTimeInput
							FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
						) as iptpt
						INNER JOIN(
							SELECT SubjectID as RecordID, SubjectName as SName
							FROM INTRANET_SUBJECT  WHERE RecordStatus = 1
						) as assub ON iptpt.SubjectID=assub.RecordID
						INNER JOIN(
							SELECT ClassLevelID as YearID, LevelName as YearName FROM INTRANET_CLASSLEVEL
						) as year ON iptpt.YearID=year.YearID
						ORDER BY DateTimeInput";
            }else{
                $sql = "SELECT iptpt.RecordID,BatchID,iptpt.YearID,SubjectID,Period,DateTimeInput,SName,YearName
						FROM(
							SELECT RecordID,BatchID,YearID,SubjectID,Period,DateTimeInput
							FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
						) as iptpt
						INNER JOIN(
							SELECT RecordID,CODEID,".$indexVar['libappraisal']->getLangSQL("CH_SNAME","EN_SNAME")." as SName
							FROM ASSESSMENT_SUBJECT
						) as assub ON iptpt.SubjectID=assub.RecordID
						INNER JOIN(
							SELECT YearID,YearName,Sequence FROM YEAR
						) as year ON iptpt.YearID=year.YearID
						ORDER BY DateTimeInput";
            }
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $runningIndex = 0;
            if($sys_custom['eAppraisal']['displayTeachClassesInSelTable'] ){
                $subjectList = array();
                $classList = array();
                if ($indexVar['libappraisal']->isEJ()) {
                    $sql = "SELECT SubjectID as RecordID, SubjectName as SName FROM INTRANET_SUBJECT  WHERE RecordStatus = 1";
                    $subjectList = $connection->returnArray($sql);
                    $sql = "SELECT ClassLevelID as YearID, LevelName as YearName FROM INTRANET_CLASSLEVEL ORDER BY WebSAMSLevel ASC";
                    $classList = $connection->returnArray($sql);
                }else{
                    $sql = "SELECT RecordID,CODEID,".$indexVar['libappraisal']->getLangSQL("CH_SNAME","EN_SNAME")." as SName FROM ASSESSMENT_SUBJECT";
                    $subjectList = $connection->returnArray($sql);
                    $sql = "SELECT YearID,YearName,Sequence FROM YEAR ORDER BY Sequence ASC";
                    $classList = $connection->returnArray($sql);
                }
                $teachClassSbjData = array();
                for($i=0;$i<sizeof($a);$i++){
                    $teachClassSbjData[$a[$i]['SubjectID']][$a[$i]['YearID']] = $a[$i]['Period'];
                }
                $x4 .= "<div id=\"teachingClassLayer\">";
                $x4 .= "<table id=\"teachingClassTable\" class=\"common_table_list\">";
                $x4 .= "<thead>";
                $x4 .= "<tr>";
                $x4 .= "<th style=\"width:".(100/(count($classList)+1))."%\"></th>";
                foreach($classList as $classes){
                    $x4 .= "<th style=\"width:".(100/(count($classList)+1))."%\">".$indexVar['libappraisal']->displayChinese($classes['YearName'])."</th>";
                }
                $x4 .= "</tr></thead>";
                $x4 .= "<tbody>";
                foreach($subjectList as $subject){
                    $x4 .= "<tr>";
                    $x4 .= "<td>".$indexVar['libappraisal']->displayChinese($subject['SName'])."</td>";
                    foreach($classList as $classes){
                        $x4 .= "<td>";
                        $x4 .= (isset($teachClassSbjData[$subject['RecordID']][$classes['YearID']]))?"&#10004;":"";
                        //$x4 .= (isset($teachClassSbjData[$subject['RecordID']][$classes['YearClassID']]))?"Y":"";
                        $x4 .= "</td>";
                    }
                    $x4 .= "</tr>";
                }
                $x4 .= "</tbody>";
                $x4 .= "</table>";
                $x4 .= "</div><br/>";
            }else{
                //$x4 .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($preDefHeader[4]["Descr"])."<br/>\r\n";
                $x4 .= "<div id=\"teachingClassLayer\">";
                $x4 .= "<table id=\"teachingClassTable\" class=\"common_table_list\">";
                $x4 .= "<thead>";
                $x4 .= "<tr><th style=\"width:30%\">".$Lang['Appraisal']['ARFormCls']."</th><th style=\"width:30%\">".$Lang['Appraisal']['ARFormTeachSubj']."</th>";
                $x4 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormClsPerCyc']."</th>";
                $x4 .= "<th style=\"width:10%\"></th>";
                $x4 .= "</tr></thead>";
                $x4 .= "<tbody>";
                
                for($i=0;$i<sizeof($a);$i++){
                    $j=$i+1;
                    $x4 .= "<tr id=\"teachFormRow_".$j."\">";
                    $x4 .= "<td>".$indexVar['libappraisal']->displayChinese($a[$i]["YearName"])."</td>";
                    $x4 .= "<td >".$indexVar['libappraisal']->displayChinese($a[$i]["SName"])."</td>";
                    $x4 .= "<td>";
                    $x4 .= $a[$i]["Period"];
                    $x4 .= "</td>";
                    $x4 .= "<td>";
                    $x4 .= "</td>";
                    $x4 .= "<input type=\"hidden\" id=\"hiddenYear_".$j."\" name=\"hiddenYear_".$j."\" VALUE=\"".$a[$i]["YearID"]."\">";
                    $x4 .= "<input type=\"hidden\" id=\"hiddenFormSubject_".$j."\" name=\"hiddenFormSubject_".$j."\" VALUE=\"".$a[$i]["SubjectID"]."\">";
                    $x4 .= "<input type=\"hidden\" id=\"hiddenForm_".$j."\" name=\"hiddenForm_".$j."\" VALUE=\"ACTIVE\">";
                    $x4 .= "</tr>";
                    $runningIndex++;
                }
                /*$x4 .= "<tr height=\"34px\"><td></td><td></td><td></td><td>";
                 $x4 .= "<div class=\"table_row_tool row_content_tool\">";
                 $x4 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addForm('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
                 $x4 .= "<input type=\"hidden\" id=\"currentFormRow\" name=\"currentFormRow\" VALUE=\"".sizeof($a)."\">";
                 $x4 .= "<input type=\"hidden\" id=\"totalFormRow\" name=\"totalFormRow\" VALUE=\"".$runningIndex."\">";
                 $x4 .= "</td></tr>";*/
                $x4 .= "</tbody>";
                $x4 .= "</table>";
                $x4 .= "</div><br/>";
            }
            
            if($preDefHeader[4]["PageBreakAfter"]==1){
                $x4 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Present Administrative Duties ========================================================================//
        if($preDefHeader[5]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,DutyType,SeqID,Descr
			FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			AND DutyType='A'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x5 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[5]["Descr"])."</b><br/>\r\n";
            $x5 .= "<div id=\"othAdmInfoLayer\">";
            $x5 .= "<table id=\"othAdmInfoTable\" class=\"common_table_list\">";
            $x5 .= "<thead>";
            $x5 .= "<tr><th style=\"width:5%\">".$Lang['Appraisal']['ARFormSeq']."</th><th style=\"width:95%\">".$Lang['Appraisal']['ARFormOthAdmDesc']."</th>";
            $x5 .= "</tr></thead>";
            $x5 .= "<tbody>";
            for($i=0;$i<count($a);$i++){
                $j=$i+1;
                if($a[$i]["Descr"]=="")continue;
                $x5 .= "<tr id=\"othAdmRow_".$j."\">";
                $x5 .= "<td >".$a[$i]["SeqID"]."</td>";
                $x5 .= "<td>";
                $x5 .= $a[$i]["Descr"];
                $x5 .= "</td>";
                $x5 .= "</tr>";
            }
            $x5 .= "</tbody>";
            $x5 .= "</table></div><br/>";
            if($preDefHeader[5]["PageBreakAfter"]==1){
                $x5 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Present Extra-Curriculum Activity Duties ========================================================================//
        if($preDefHeader[6]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,DutyType,SeqID,Descr
			FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			AND DutyType='E'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x6 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[6]["Descr"])."</b><br/>\r\n";
            $x6 .= "<div id=\"othExtCurInfoLayer\">";
            $x6 .= "<table id=\"othExtCurInfoTable\" class=\"common_table_list\">";
            $x6 .= "<thead>";
            $x6 .= "<tr><th style=\"width:5%\">".$Lang['Appraisal']['ARFormSeq']."</th><th style=\"width:95%\">".$Lang['Appraisal']['ARFormOthExtCurDesc']."</th>";
            $x6 .= "</tr></thead>";
            $x6 .= "<tbody>";
            for($i=0;$i<count($a);$i++){
                $j=$i+1;
                if($a[$i]["Descr"]=="")continue;
                $x6 .= "<tr id=\"othExtCurRow_".$j."\">";
                $x6 .= "<td >".$a[$i]["SeqID"]."</td>";
                $x6 .= "<td>";
                $x6 .= $a[$i]["Descr"];
                $x6 .= "</td>";
                $x6 .= "</tr>";
            }
            $x6 .= "</tbody>";
            $x6 .= "</table></div><br/>";
            if($preDefHeader[6]["PageBreakAfter"]==1){
                $x6 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Present Other Duties ========================================================================//
        if($preDefHeader[7]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,DutyType,SeqID,Descr
			FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			AND DutyType='O'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x7 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[7]["Descr"])."</b><br/>\r\n";
            $x7 .= "<div id=\"othDutInfoLayer\">";
            $x7 .= "<table id=\"othDutInfoTable\" class=\"common_table_list\">";
            $x7 .= "<thead>";
            $x7 .= "<tr><th style=\"width:5%\">".$Lang['Appraisal']['ARFormSeq']."</th><th style=\"width:95%\">".$Lang['Appraisal']['ARFormOthDutDesc']."</th>";
            $x7 .= "</tr></thead>";
            $x7 .= "<tbody>";
            for($i=0;$i<count($a);$i++){
                $j=$i+1;
                if($a[$i]["Descr"]=="")continue;
                $x7 .= "<tr id=\"othDutRow_".$j."\">";
                $x7 .= "<td >".$a[$i]["SeqID"]."</td>";
                $x7 .= "<td>";
                $x7 .= $a[$i]["Descr"];
                $x7 .= "</td>";
                $x7 .= "</tr>";
            }
            $x7 .= "</tbody>";
            $x7 .= "</table></div><br/>";
            if($preDefHeader[7]["PageBreakAfter"]==1){
                $x7 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Absence & Lateness ========================================================================//
        if($preDefHeader[8]["Incl"]==1){
            $sql = "SELECT '1' as SeqID,AbsLsnCnt as Value FROM INTRANET_PA_C_ABSENCE WHERE CycleID='".$cycleID."' AND UserID='".IntegerSafe($userID)."'
			UNION
			SELECT '2' as SeqID,SubLsnCnt as Value FROM INTRANET_PA_C_ABSENCE WHERE CycleID='".$cycleID."' AND UserID='".IntegerSafe($userID)."'
			UNION
			SELECT '3' as SeqID,LateCnt as Value FROM INTRANET_PA_C_ABSENCE WHERE CycleID='".$cycleID."' AND UserID='".IntegerSafe($userID)."'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x8 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[8]["Descr"])."</b>\r\n";
            $x8 .= "<div id=\"asbLatInfoLayer\">";
            $x8 .= "<table id=\"asbLatInfoTable\" class=\"form_table_v30\">";
            $x8 .= "<thead>";
            $x8 .= "<tr>";
            $x8 .= "</tr></thead>";
            $x8 .= "<tbody>";
            $x8 .= "<tr>";
            $x8 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormNoLesAsb']."</td>";
            $x8 .= "<td>";
            $x8 .= $a[0]["Value"];
            $x8 .= "</td>";
            $x8 .= "</tr>";
            $x8 .= "<tr>";
            $x8 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormNoSubLes']."</td>";
            $x8 .= "<td>";
            $x8 .= $a[1]["Value"];
            $x8 .= "</td>";
            $x8 .= "</tr>";
            $x8 .= "<tr>";
            $x8 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormNoTimLat']."</td>";
            $x8 .= "<td>";
            $x8 .= $a[2]["Value"];
            $x8 .= "</td>";
            $x8 .= "</tr>";
            $x8 .= "</tbody>";
            $x8 .= "</table></div><br/>";
            if($preDefHeader[8]["PageBreakAfter"]==1){
                $x8 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Leaves Records (Pay & Non-Pay) ========================================================================//
        if($preDefHeader[9]["Incl"]==1){
            $sql = "SELECT CycleID,1 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Sick Leave' AND CycleID='".$cycleID."' AND UserID='".IntegerSafe($userID)."'
			UNION
			SELECT CycleID,2 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Official Leave' AND CycleID='".$cycleID."' AND UserID='".IntegerSafe($userID)."'
			UNION
			SELECT CycleID,3 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Study Leave' AND CycleID='".$cycleID."' AND UserID='".IntegerSafe($userID)."'
			UNION
			SELECT CycleID,4 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Maternity Leave' AND CycleID='".$cycleID."' AND UserID='".IntegerSafe($userID)."'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x9 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[9]["Descr"])."</b><br/>\r\n";
            $x9 .= "<div id=\"leavePaidInfoLayer\">";
            $x9 .= "<table id=\"leavePaidInfoTable\" class=\"common_table_list\">";
            $x9 .= "<thead>";
            $x9 .= "<tr>";
            $x9 .= "<th style=\"width:25%\">".$Lang['Appraisal']['ARFormLeaCat']."</th><th style=\"width:25%\">".$Lang['Appraisal']['ARFormPayLea']."</th>";
            $x9 .= "<th style=\"width:25%\">".$Lang['Appraisal']['ARFormNoPayLea']."</th><th style=\"width:25%\">".$Lang['Appraisal']['ARFormRemarks']."</th>";
            $x9 .= "</tr></thead>";
            $x9 .= "<tbody>";
            $x9 .= "<tr>";
            $x9 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormSickLeave']."</td>";
            $x9 .= "<td>";
            $x9 .= $a[0]["DaysPaid"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[0]["DaysNoPay"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[0]["Remark"];
            $x9 .= "</td>";
            $x9 .= "</tr>";
            $x9 .= "<tr>";
            $x9 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormOfficialLeave']."</td>";
            $x9 .= "<td>";
            $x9 .= $a[1]["DaysPaid"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[1]["DaysNoPay"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[1]["Remark"];
            $x9 .= "</td>";
            $x9 .= "</tr>";
            $x9 .= "<tr>";
            $x9 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormStudyLeave']."</td>";
            $x9 .= "<td>";
            $x9 .= $a[2]["DaysPaid"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[2]["DaysNoPay"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[2]["Remark"];
            $x9 .= "</td>";
            $x9 .= "</tr>";
            $x9 .= "<tr>";
            $x9 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormMaternityLeave']."</td>";
            $x9 .= "<td>";
            $x9 .= $a[3]["DaysPaid"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[3]["DaysNoPay"];
            $x9 .= "</td>";
            $x9 .= "<td>";
            $x9 .= $a[3]["Remark"];
            $x9 .= "</td>";
            $x9 .= "</tr>";
            $x9 .= "</tbody>";
            $x9 .= "</table></div><br/>";
            if($preDefHeader[9]["PageBreakAfter"]==1){
                $x9 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Leave Records ========================================================================//
        if($preDefHeader[10]["Incl"]==1){
            $sql = "SELECT CycleID,1 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Sick Leave' AND CycleID='".$cycleID."' AND UserID='".IntegerSafe($userID)."'
			UNION
			SELECT CycleID,2 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Official Leave' AND CycleID='".$cycleID."' AND UserID='".IntegerSafe($userID)."'
			UNION
			SELECT CycleID,3 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Study Leave' AND CycleID='".$cycleID."' AND UserID='".IntegerSafe($userID)."'
			UNION
			SELECT CycleID,4 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Maternity Leave' AND CycleID='".$cycleID."' AND UserID='".IntegerSafe($userID)."'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x10 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[10]["Descr"])."</b><br/>\r\n";
            $x10 .= "<div id=\"leavePaidTotalInfoLayer\">";
            $x10 .= "<table id=\"leavePaidTotalInfoTable\" class=\"common_table_list\">";
            $x10 .= "<thead>";
            $x10 .= "<tr>";
            $x10 .= "<th style=\"width:25%\">".$Lang['Appraisal']['ARFormLeaCat']."</th><th style=\"width:25%\">".$Lang['Appraisal']['ARFormPayTot']."</th>";
            $x10 .= "<th style=\"width:50%\">".$Lang['Appraisal']['ARFormRemarks']."</th>";
            $x10 .= "</tr></thead>";
            $x10 .= "<tbody>";
            $x10 .= "<tr>";
            $x10 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormSickLeave']."</td>";
            $x10 .= "<td>";
            $x10 .= (empty($a[0]["DaysTot"]))?"0.00":$a[0]["DaysTot"];
            $x10 .= "</td>";
            $x10 .= "<td>";
            $x10 .= $a[0]["Remark"];
            $x10 .= "</td>";
            $x10 .= "</tr>";
            $x10 .= "<tr>";
            $x10 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormOfficialLeave']."</td>";
            $x10 .= "<td>";
            $x10 .= (empty($a[1]["DaysTot"]))?"0.00":$a[1]["DaysTot"];
            $x10 .= "</td>";
            $x10 .= "<td>";
            $x10 .= $a[1]["Remark"];
            $x10 .= "</td>";
            $x10 .= "</tr>";
            $x10 .= "<tr>";
            $x10 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormStudyLeave']."</td>";
            $x10 .= "<td>";
            $x10 .= (empty($a[2]["DaysTot"]))?"0.00":$a[2]["DaysTot"];
            $x10 .= "</td>";
            $x10 .= "<td>";
            $x10 .= $a[2]["Remark"];
            $x10 .= "</td>";
            $x10 .= "</tr>";
            $x10 .= "<tr>";
            $x10 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormMaternityLeave']."</td>";
            $x10 .= "<td>";
            $x10 .= (empty($a[3]["DaysTot"]))?"0.00":$a[3]["DaysTot"];
            $x10 .= "</td>";
            $x10 .= "<td>";
            $x10 .= $a[3]["Remark"];
            $x10 .= "</td>";
            $x10 .= "</tr>";
            $x10 .= "</tbody>";
            $x10 .= "</table></div><br/>";
            if($preDefHeader[10]["PageBreakAfter"]==1){
                $x10 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Attended Course/Seminar/Workshop ========================================================================//
        if($preDefHeader[11]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,SeqID,DATE_FORMAT(EventDate,'%Y-%m-%d') as EventDate,Orgn,CrsSemWs,Hours,Certification
			FROM INTRANET_PA_T_PRE_CRS WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."';";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x11 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[11]["Descr"])."</b><br/>\r\n";
            
            $x11 .= "<div id=\"crsSemWsInfoLayer\">";
            $x11 .= "<table id=\"crsSemWsInfoTable\" class=\"common_table_list\">";
            $x11 .= "<thead>";
            $x11 .= "<tr><th style=\"width:2%\">".$Lang['Appraisal']['ARFormSeq']."</th><th style=\"width:10%\">".$Lang['Appraisal']['ARFormDate']."</th>";
            $x11 .= "<th style=\"width:28%\">".$Lang['Appraisal']['ARFormOrganization']."</th><th style=\"width:20%\">".$Lang['Appraisal']['ARFormCourseSeminarWorkshop']."</th>";
            $x11 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormHours']."</th><th style=\"width:20%\">".$Lang['Appraisal']['ARFormCertification']."</th>";
            $x11 .= "<th style=\"width:10%\"></th>";
            $x11 .= "</tr></thead>";
            $x11 .= "<tbody>";
            $runningIndex = 0;
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x11 .= "<tr id=\"crsSemWsRow_".$j."\">";
                $x11 .= "<td>";
                $x11 .= "<div id=\"crsSemWsSeqID_".$a[$i]["SeqID"]."\" name=\"crsSemWsSeqID_".$a[$i]["SeqID"]."\">";
                $x11 .= $a[$i]["SeqID"];
                $x11 .= "</div>";
                $x11 .= "</td>";
                $x11 .= "<td>";
                $x11 .= $a[$i]["EventDate"];
                $x11 .= "</td>";
                $x11 .= "<td>";
                $x11 .= $a[$i]["Orgn"];
                $x11 .= "</td>";
                $x11 .= "<td>";
                $x11 .= $a[$i]["CrsSemWs"];
                $x11 .= "</td>";
                $x11 .= "<td>";
                $x11 .= $a[$i]["Hours"];
                $x11 .= "</td>";
                $x11 .= "<td>";
                $x11 .= $a[$i]["Certification"];
                $x11 .= "</td>";
                $x11 .= "<td>";
                $x11 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
                //$x11 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteCrsSemWs('".$recordID."','".$batchID."','".$j."');\"></a></span>";
                //$x11 .= "</span>";
                //$x11 .= "<input type=\"hidden\" id=\"hiddenCrsSemWsSts_".$j."\" name=\"hiddenCrsSemWsSts_".$j."\" VALUE=\"ACTIVE\">";
                //$x11 .= "<input type=\"hidden\" id=\"hiddenCrsSemWsSeqID_".$j."\" name=\"hiddenCrsSemWsSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
                $x11 .= "</td>";
                $x11 .= "</tr>";
                $runningIndex++;
            }
            /*$x11 .= "<tr height=\"34px\"><td></td><td></td><td></td><td></td><td></td><td></td><td>";
             $x11 .= "<div class=\"table_row_tool row_content_tool\">";
             $x11 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addCrsSemWs('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
             $x11 .= "<input type=\"hidden\" id=\"currentCrsSemWsRow\" name=\"currentCrsSemWsRow\" VALUE=\"".sizeof($a)."\">";
             $x11 .= "<input type=\"hidden\" id=\"totalCrsSemWsRow\" name=\"totalCrsSemWsRow\" VALUE=\"".$runningIndex."\">";
             $x11 .= "</td></tr>";*/
            $x11 .= "</tbody>";
            $x11 .= "</table>";
            $x11 .= "</div><br/>";
            if($preDefHeader[11]["PageBreakAfter"]==1){
                $x11 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Teaching / Administrative Duties Objectives and Assessment ========================================================================//
        if($preDefHeader[12]["Incl"]==1){
            $x12 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[12]["Descr"])."</b><br/>\r\n";
            
            $sql = "SELECT RecordID,BatchID,SeqID,Objective,Plan,Assessment,Standard,CatID
					FROM INTRANET_PA_T_PRE_TASKASSESSMENT WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."';";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $runningIndex = 0;
            
            //$x3 .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($preDefHeader[4]["Descr"])."<br/>\r\n";
            $x12 .= "<div id=\"taskAssessmentLayer\">";
            if($Lang['Appraisal']['ARFormTaskDesc']!=""){
                $x12 .= "<p>";
                $x12 .= $Lang['Appraisal']['ARFormTaskDesc'];
                $x12 .= "</p>";
            }
            if($sys_custom['eAppraisal']['DutyObjectivesAssessmentIn4Parts']==true){
                $taskListByCatArr = array();
                foreach($a as $taskRecord){
                    if(!isset($taskListByCatArr[$taskRecord['CatID']])){
                        $taskListByCatArr[$taskRecord['CatID']] = array();
                    }
                    array_push($taskListByCatArr[$taskRecord['CatID']], $taskRecord);
                }
                for($index=1; $index <= 4; $index++){
                    $runningIndex = 0;
                    
                    $x12 .= "<p>".$Lang['Appraisal']['ARFormTaskParts'][$index]."</p>";
                    $x12 .= "<table id=\"taskAssessmentTable\" class=\"common_table_list\">";
                    $x12 .= "<thead>";
                    $x12 .= "<tr>";
                    $x12 .= "<th style=\"width:25%\">".$Lang['Appraisal']['ARFormTaskObjective']."</th>";
                    $x12 .= "<th style=\"width:25%\">".$Lang['Appraisal']['ARFormTaskStandard']."</th>";
                    $x12 .= "<th style=\"width:25%\">".$Lang['Appraisal']['ARFormTaskAction']."</th>";
                    $x12 .= "<th style=\"width:25%\">".$Lang['Appraisal']['ARFormTaskAchievement']."</th>";
                    $x12 .= "</tr></thead>";
                    $x12 .= "<tbody>";
                    
                    for($i=0;$i<sizeof($taskListByCatArr[$index]);$i++){
                        $j=$i+1;
                        $x12 .= "<tr id=\"taskAssessmentRow_".$j."\">";
                        $x12 .= "<td>";
                        $x12 .= nl2br($taskListByCatArr[$index][$i]["Objective"]);
                        $x12 .= "</td>";
                        $x12 .= "<td>";
                        $x12 .= nl2br($taskListByCatArr[$index][$i]["Standard"]);
                        $x12 .= "</td>";
                        $x12 .= "<td>";
                        $x12 .= nl2br($taskListByCatArr[$index][$i]["Plan"]);
                        $x12 .= "</td>";
                        $x12 .= "<td>";
                        $x12 .= nl2br($taskListByCatArr[$index][$i]["Assessment"]);
                        $x12 .= "</td>";
                        $x12 .= "</tr>";
                        $runningIndex++;
                    }
                    $x12 .= "</tbody>";
                    $x12 .= "</table><br>";
                }
            }else{
                $x12 .= "<table id=\"taskAssessmentTable\" class=\"common_table_list\">";
                $x12 .= "<thead>";
                $x12 .= "<tr>";
                $x12 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormTaskObjective']."</th>";
                $x12 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormTaskAction']."</th>";
                $x12 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormTaskAssessment']."</th>";
                $x12 .= "</tr></thead>";
                $x12 .= "<tbody>";
                
                for($i=0;$i<sizeof($a);$i++){
                    $j=$i+1;
                    $x12 .= "<tr id=\"taskAssessmentRow_".$j."\" >";
                    $x12 .= "<td>";
                    //$x12 .= $indexVar['libappraisal_ui']->GET_TEXTAREA('taskObjective_'.$j, $a[$i]["Objective"], 70, 5, "", "", "", "", 'taskObjective_'.$j);
                    $x12 .= nl2br($a[$i]["Objective"]);
                    $x12 .= "</td>";
                    $x12 .= "<td>";
                    //$x12 .= $indexVar['libappraisal_ui']->GET_TEXTAREA('taskPlan_'.$j, $a[$i]["Plan"], 70, 5, "", "", "", "", 'taskPlan_'.$j);
                    $x12 .= nl2br($a[$i]["Plan"]);
                    $x12 .= "</td>";
                    $x12 .= "<td>";
                    //$x12 .= $indexVar['libappraisal_ui']->GET_TEXTAREA('taskAssessment_'.$j, $a[$i]["Assessment"], 70, 5, "", "", "", "", 'taskAssessment_'.$j);
                    $x12 .= nl2br($a[$i]["Assessment"]);
                    $x12 .= "</td>";
                    $x12 .= "</tr>";
                    $runningIndex++;
                }
                /*
                 $x12 .= "<tr height=\"34px\"><td></td><td></td><td></td><td>";
                 $x12 .= "<div class=\"table_row_tool row_content_tool\">";
                 $x12 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addClass('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
                 $x12 .= "<input type=\"hidden\" id=\"currentClassRow\" name=\"currentClassRow\" VALUE=\"".sizeof($a)."\">";
                 $x12 .= "<input type=\"hidden\" id=\"totalClassRow\" name=\"totalClassRow\" VALUE=\"".$runningIndex."\">";
                 $x12 .= "</td></tr>";*/
                $x12 .= "</tbody>";
                $x12 .= "</table>";
            }
            $x12 .= "</div><br/>";
            if($preDefHeader[12]["PageBreakAfter"]==1){
                $x12 .= "<p style=\"page-break-after:always;\"></p>";
            }
            
        }
        //======================================================================== Present Academic Duties ========================================================================//
        if($preDefHeader[13]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,DutyType,SeqID,Descr
			FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			AND DutyType='S'
			";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x13 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[13]["Descr"])."</b><br/>\r\n";
            $x13 .= "<div id=\"othAdmInfoLayer\">";
            $x13 .= "<table id=\"othAdmInfoTable\" class=\"common_table_list\">";
            $x13 .= "<thead>";
            $x13 .= "<tr><th style=\"width:5%\">".$Lang['Appraisal']['ARFormSeq']."</th><th style=\"width:95%\">".$Lang['Appraisal']['ARFormOthAcaDesc']."</th>";
            $x13 .= "</tr></thead>";
            $x13 .= "<tbody>";
            for($i=0;$i<count($a);$i++){
                $j=$i+1;
                if($a[$i]["Descr"]=="")continue;
                $x13 .= "<tr id=\"othAdmRow_".$j."\">";
                $x13 .= "<td >".$a[$i]["SeqID"]."</td>";
                $x13 .= "<td>";
                $x13 .= $a[$i]["Descr"];
                $x13 .= "</td>";
                $x13 .= "</tr>";
            }
            $x13 .= "</tbody>";
            $x13 .= "</table></div><br/>";
            if($preDefHeader[13]["PageBreakAfter"]==1){
                $x13 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Student Training for Competition ========================================================================//
        if($preDefHeader[14]["Incl"]==1){
            $x14 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[14]["Descr"])."</b><br/>\r\n";
            
            $sql = "SELECT RecordID,BatchID,SeqID,Competition,Trainee,Duration,Result,Remark
					FROM INTRANET_PA_T_PRE_STUDENTTRAINING WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."';";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $runningIndex = 0;
            
            $x14 .= "<div id=\"studentTrainingLayer\">";
            $x14 .= "<table id=\"studentTrainingTable\" class=\"common_table_list\">";
            $x14 .= "<thead>";
            $x14 .= "<tr><th style=\"width:5%\">#</th>";
            $x14 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormStdTrainComp']."</th>";
            $x14 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormStdTrainNo']."</th>";
            $x14 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormStdTrainDur']."</th>";
            $x14 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormStdTrainResult']."</th>";
            $x14 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormRemarks']."</th>";
            $x14 .= "</tr></thead>";
            $x14 .= "<tbody>";
            
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x14 .= "<tr id=\"studentTrainingRow_".$j."\">";
                $x14 .= "<td>";
                $x14 .= "<div id=\"StudentTrainingSeqID_".$a[$i]["SeqID"]."\" name=\"StudentTrainingSeqID_".$a[$i]["SeqID"]."\">";
                $x14 .= $j;
                $x14 .= "</div>";
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $a[$i]["Competition"];
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $a[$i]["Trainee"];
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $a[$i]["Duration"];
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $a[$i]["Result"];
                $x14 .= "</td>";
                $x14 .= "<td>";
                $x14 .= $a[$i]["Remark"];
                $x14 .= "</td>";
                $x14 .= "</tr>";
                $runningIndex++;
            }
            
            $x14 .= "</tbody>";
            $x14 .= "</table>";
            $x14 .= "</div><br/>";
            if($preDefHeader[14]["PageBreakAfter"]==1){
                $x14 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== CPD ========================================================================//
        if($preDefHeader[15]["Incl"]==1){
            $x15 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[15]["Descr"])."</b><br/>\r\n";
            $academicYear = $indexVar['libappraisal']->getAcademicYearTermByCycleID($cycleID);
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDDurationSetting'";
            $cpdHoursCountingSeparateMonth = current($connection->returnVector($sql));
            if(empty($cpdHoursCountingSeparateMonth)){
                $cpdHoursCountingSeparateMonth = 4;
            }else{
                $cpdHoursCountingSeparateMonth = intval($cpdHoursCountingSeparateMonth);
            }
            $nextCpdHoursCountingSeparateMonth = $cpdHoursCountingSeparateMonth+1;
            if($cpdHoursCountingSeparateMonth==12){
                $nextCpdHoursCountingSeparateMonth = 1;
            }
            
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDShowingTotalHourSetting'";
            $cpdShowingTotalHour= current($connection->returnVector($sql));
            if(empty($cpdShowingTotalHour)){
                $cpdShowingTotalHour = false;
            }
            
            if($sys_custom['eAppraisal']['CPDAutoSync'] == true){
                // Rebuild the CPD record list for auto-sync with Teacher Portfolio
                // 1st: collect CPD record of the corresponding year
                $sql = "SELECT AcademicYearID FROM INTRANET_PA_T_FRMSUM fs INNER JOIN INTRANET_PA_C_CYCLE cc ON fs.CycleID=cc.CycleID WHERE fs.RecordID='$recordID'";
                $academicYearIDs = $connection->returnArray($sql);
                $academicYear = $indexVar['libappraisal']->getAllAcademicYear($academicYearIDs[0]['AcademicYearID']);
                $sql = "SELECT FTDataRowID,StartDate,EndDate,NoOfSection,EventNameEn,EventNameChi,EventTypeEn,EventTypeChi,RemarksEn,RemarksChi,Content,Organization,CPDMode,CPDDomain,BasicLawRelated,SubjectRelated FROM INTRANET_TEACHER_PORTFOLIO_FORM_DATA_GRID_CPD WHERE UserID='".$userID."' AND AcademicYear='".$academicYear[0]['YearNameEN']."' ORDER BY StartDate";
                $cpdRecords = $connection->returnArray($sql);
                $newCPDList = array();
                // 2nd: move teacher portfolio CPD records to TA CPD record list
                if(!empty($cpdRecords)){
                    foreach($cpdRecords as $idx=>$cpd){
                        $sql = "SELECT CpdID,SeqID FROM INTRANET_PA_S_T_PRE_CPD WHERE FTDataRowID='".$cpd['FTDataRowID']."' AND RlsNo='$rlsNo' AND RecordID='$recordID' AND BatchID='$batchID' AND UserID='".$userID."'";
                        $existingRecord = current($connection->returnArray($sql));
                        if(empty($existingRecord)){
                            // insert newly added records
                            $sql = "INSERT INTO INTRANET_PA_S_T_PRE_CPD
										(RlsNo, RecordID, BatchID, SeqID, UserID, AcademicYearID, StartDate, EndDate, NoOfSection, EventNameEng, EventNameChi, EventTypeEng, EventTypeChi,Content,Organization,CPDMode,CPDDomain,BasicLawRelated,FTDataRowID,DateInput,InputBy,DateModified,ModifyBy,SubjectRelated)
										VALUES
										($rlsNo, $recordID, $batchID, '".($idx+1)."', '".$userID."','".$academicYearIDs[0]['AcademicYearID']."', '".$cpd['StartDate']."', '".$cpd['EndDate']."', '".$cpd['NoOfSection']."', '".$cpd['EventNameEn']."', '".$cpd['EventNameChi']."', '".$cpd['EventTypeEn']."', '".$cpd['EventTypeChi']."','".$cpd['Content']."','".$cpd['Organization']."','".$cpd['CPDMode']."','".$cpd['CPDDomain']."','".$cpd['BasicLawRelated']."', '".$cpd['FTDataRowID']."',NOW(),$userID,NOW(),$userID,'".$cpd['SubjectRelated']."')
									";
                            $connection->db_db_query($sql);
                            $newCPDList[] = $connection->db_insert_id();
                        }else{
                            // update sequence for existing records
                            if(($idx+1)!=$existingRecord['SeqID']){
                                $sql = "UPDATE INTRANET_PA_S_T_PRE_CPD SET SeqID='".($idx+1)."' WHERE CpdID='".$existingRecord['CpdID']."'";
                                $connection->db_db_query($sql);
                            }
                        }
                    }
                }
                
                // log cpd sync
                $logData = array();
                $recordDetail = array();
                $logData['RlsNo'] = $rlsNo;
                $logData['RecordID'] = $recordID;
                $logData['BatchID'] = $batchID;
                $logData['Function'] = 'Data Sync';
                $logData['RecordType'] = 'CPD Import';
                if(empty($newCPDList)){
                    $recordDetail[] = array(
                        "DisplayName"=>'New CPD Records',
                        "Message"=>$Lang['Appraisal']['Message']['NoNewCPDRecord']
                    );
                }else{
                    $recordDetail[] = array(
                        "Parameter"=>"CpdID",
                        "DisplayName"=>'New CPD Records',
                        "Original"=>$newCPDList,
                        "MapWithTable"=>'INTRANET_PA_S_T_PRE_CPD',
                        "MapDisplay"=>"EventNameEng"
                    );
                }
                $logData['RecordDetail'] = $json->encode($recordDetail);
                $result = $indexVar['libappraisal']->addFormFillingLog($logData);
                
                // 3rd: if using Lam Tin 3 years approach
                if($sys_custom['eAppraisal']['3YearsCPD']==true){
                    $pastYearCPDHours = array();
                    $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDSetting'";
                    $cpdSettingList = unserialize(current($connection->returnVector($sql)));
                    $cpdSettingList['Period'] = ($cpdSettingList['Period'])?$cpdSettingList['Period']:1;
                    $cpdSettingList["SelectedYear"] = ($cpdSettingList['SelectedYear'])?$cpdSettingList['SelectedYear']:$_SESSION['CurrentSchoolYearID'];
                    $sql = "SELECT Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID='".$cpdSettingList["SelectedYear"]."'";
                    $startYearSeq = current($connection->returnVector($sql));
                    $sql = "SELECT Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID='".$academicYearIDs[0]['AcademicYearID']."'";
                    $currSeq = current($connection->returnVector($sql));
                    $yearNo = (abs(($startYearSeq - $currSeq))%$cpdSettingList['Period']);
                    for($y=$yearNo; $y >= 0; $y--){
                        $sql = "SELECT AcademicYearID, YearNameB5, YearNameEN FROM ACADEMIC_YEAR WHERE Sequence='".($currSeq+$y)."'";
                        $pastYearData = $connection->returnArray($sql);
                        $sql = "SELECT SUM(NoOfSection) as totalCPDHours FROM INTRANET_TEACHER_PORTFOLIO_FORM_DATA_GRID_CPD WHERE UserID='".$userID."' AND AcademicYear='".$pastYearData[0]['YearNameEN']."'";
                        $totalCPDHours = current($connection->returnVector($sql));
                        $pastYearCPDHours[$pastYearData[0]['AcademicYearID']] = $totalCPDHours;
                        $sql = "INSERT INTO INTRANET_PA_T_PRE_CPD_HOURS VALUES ('".IntegerSafe($rlsNo)."', '".IntegerSafe($recordID)."', '".IntegerSafe($batchID)."', '".IntegerSafe($userID)."', '".IntegerSafe($pastYearData[0]['AcademicYearID'])."', '".$totalCPDHours."', '".IntegerSafe($_SESSION['UserID'])."',NOW(),'".IntegerSafe($_SESSION['UserID'])."',NOW())
									 ON DUPLICATE KEY UPDATE Hours='".$totalCPDHours."', ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."', DateTimeModified=NOW()";
                        $connection->db_db_query($sql);
                    }
                }
            }
            
            $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("EventNameEng","EventNameChi")." as CourseTitle,"
                .$indexVar['libappraisal']->getLangSQL("EventTypeEng","EventTypeChi")." as Nature,
				Content,Organization,NoOfSection,CPDMode,CPDDomain,BasicLawRelated,StartDate,EndDate,SubjectRelated
					FROM INTRANET_PA_S_T_PRE_CPD WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."' AND AcademicYearID=".IntegerSafe($academicYear[0]['AcademicYearID'])." ORDER BY StartDate ASC";
                //echo $sql;
                $a=$connection->returnResultSet($sql);
                $runningIndex = 0;
                
                $CPDHours = array(0,0,0);
                for($i=0;$i<sizeof($a);$i++){
                    $month = date('n',strtotime($a[$i]['StartDate']));
                    if($cpdHoursCountingSeparateMonth >= 9){
                        if($month >= 9 && $month <= $cpdHoursCountingSeparateMonth){
                            $CPDHours[0] = $CPDHours[0] + $a[$i]['NoOfSection'];
                        }else{
                            $CPDHours[1] = $CPDHours[1] + $a[$i]['NoOfSection'];
                        }
                    }else{
                        if($month >= $nextCpdHoursCountingSeparateMonth && $month <= 8){
                            $CPDHours[1] = $CPDHours[1] + $a[$i]['NoOfSection'];
                        }else{
                            $CPDHours[0] = $CPDHours[0] + $a[$i]['NoOfSection'];
                        }
                    }
                    $CPDHours[2] = $CPDHours[2] + $a[$i]['NoOfSection'];
                    $domain = explode(",",$a[$i]['CPDDomain']);
                    foreach($domain as $idx=>$dm){
                        $a[$i]['CPDDomain'.$idx] = $dm;
                    }
                    $subjectRelated = explode(",",$a[$i]['SubjectRelated']);
                    foreach($subjectRelated as $idx=>$dm){
                        $a[$i]['SubjectRelated'.$idx] = $dm;
                    }
                }
                $x15 .= "<div id=\"cpdHourLayer\">";
                $x15 .= "<table id=\"cpdHourTable\" class=\"common_table_list\">";
                $x15 .= "<thead>";
                $x15 .= "<tr>";
                if($cpdShowingTotalHour==true){
                    $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDHourCategory']."</th>";
                    $x15 .= "<th style=\"width:37%\">".str_replace(array('{{START_MONTH}}','{{END_MONTH}}'),array($Lang['Appraisal']['Month'][9],$Lang['Appraisal']['Month'][$cpdHoursCountingSeparateMonth]),$Lang['Appraisal']['ARFormCPDHourUndefined'])."</th>";
                    $x15 .= "<th style=\"width:37%\">".str_replace(array('{{START_MONTH}}','{{END_MONTH}}'),array($Lang['Appraisal']['Month'][$nextCpdHoursCountingSeparateMonth],$Lang['Appraisal']['Month'][8]),$Lang['Appraisal']['ARFormCPDHourUndefined'])."</th>";
                    $x15 .= "<th style=\"width:16%\">".$Lang['Appraisal']['ARFormCPDHourTotal']."</th>";
                }else{
                    $x15 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormCPDHourCategory']."</th>";
                    $x15 .= "<th style=\"width:40%\">".str_replace(array('{{START_MONTH}}','{{END_MONTH}}'),array($Lang['Appraisal']['Month'][9],$Lang['Appraisal']['Month'][$cpdHoursCountingSeparateMonth]),$Lang['Appraisal']['ARFormCPDHourUndefined'])."</th>";
                    $x15 .= "<th style=\"width:40%\">".str_replace(array('{{START_MONTH}}','{{END_MONTH}}'),array($Lang['Appraisal']['Month'][$nextCpdHoursCountingSeparateMonth],$Lang['Appraisal']['Month'][8]),$Lang['Appraisal']['ARFormCPDHourUndefined'])."</th>";
                }
                $x15 .= "</tr></thead>";
                $x15 .= "<tbody>";
                if($cpdShowingTotalHour==true){
                    $x15 .= "<tr height=\"34px\"><td>".$Lang['Appraisal']['ARFormCPDHourCategoryHour'] ."</td><td><span id=\"cpdHourA\">".$CPDHours[0]."</span>".$Lang['Appraisal']['ARFormCPDHourHour']."</td><td><span id=\"cpdHourB\">".$CPDHours[1]."</span>".$Lang['Appraisal']['ARFormCPDHourHour']."</td><td style=\"".(($cpdShowingTotalHour==true)?"":"display:none")."\"><span id=\"cpdHourTotal\">".$CPDHours[2]."</span>".$Lang['Appraisal']['ARFormCPDHourHour']."</td></tr>";
                }else{
                    $x15 .= "<tr height=\"34px\"><td>".$Lang['Appraisal']['ARFormCPDHourCategoryHour'] ."</td><td><span id=\"cpdHourA\">".$CPDHours[0]."</span>".$Lang['Appraisal']['ARFormCPDHourHour']."</td><td><span id=\"cpdHourB\">".$CPDHours[1]."</span>".$Lang['Appraisal']['ARFormCPDHourHour']."</td></tr>";
                }
                $x15 .= "</tbody>";
                $x15 .= "</table>";
                $x15 .= "</div><br/>";
                if($sys_custom['eAppraisal']['LTMPSPrintingStyle']==true){
                    $x15 .= "<p class=\"extraRmk\">".$Lang['Appraisal']['ARFormCPDHrRmk']."</p>";
                }
                $x15 .= "<br/>";
                
                if($sys_custom['eAppraisal']['3YearsCPD']==true){
                    $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDSetting'";
                    $cpdSettingList = unserialize(current($connection->returnVector($sql)));
                    $displayHTML = "";
                    $cpdSettingList['Period'] = ($cpdSettingList['Period'])?$cpdSettingList['Period']:1;
                    $cpdSettingList["SelectedYear"] = ($cpdSettingList['SelectedYear'])?$cpdSettingList['SelectedYear']:$_SESSION['CurrentSchoolYearID'];
                    $sql = "SELECT Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID=".$cpdSettingList["SelectedYear"];
                    $startYearSeq = current($connection->returnVector($sql));
                    $sql = "SELECT Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID=".$academicYearID;
                    $currSeq = current($connection->returnVector($sql));
                    $yearNo = (($startYearSeq - $currSeq)%$cpdSettingList['Period']);
                    $x15 .= "<div id=\"CPDPastYears\">";
                    $x15 .= $Lang['Appraisal']['ARFormCPDPastYearHead']."<br>";
                    $x15 .= $Lang['Appraisal']['ARFormCPDPastYearRemark']."<br>";
                    $x15 .= "<div id=\"CPDPastYearsHoursList\">";
                    $sql = "SELECT AcademicYearID, Hours FROM INTRANET_PA_T_PRE_CPD_HOURS WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."' AND UserID='".$userID."'";
                    $yearHourData=$connection->returnResultSet($sql);
                    $yearCPDHour = array();
                    foreach($yearHourData as $yhd){
                        $yearCPDHour[$yhd['AcademicYearID']] = $yhd['Hours'];
                    }
                    if($yearNo < 0){
                        for($y=$yearNo; $y <= 0; $y++){
                            $sql = "SELECT AcademicYearID, ".$indexVar['libappraisal']->getLangSQL("YearNameB5","YearNameEN")." as YearName FROM ACADEMIC_YEAR WHERE Sequence='".($currSeq+$y)."'";
                            $pastYearData = current($connection->returnArray($sql));
                            if(!isset($yearCPDHour[$pastYearData['AcademicYearID']])){
                                $yearCPDHour[$pastYearData['AcademicYearID']] = 0;
                            }
                            $x15 .= $pastYearData['YearName']." ".$Lang['Appraisal']['ARFormCPDHourSchoolYear']."( <span id='yearHour_".$pastYearData['AcademicYearID']."_span'>".$yearCPDHour[$pastYearData['AcademicYearID']]."</span>".$Lang['Appraisal']['ARFormCPDHourHour'].")   ";
                        }
                    }else{
                        for($y=$yearNo; $y >= 0; $y--){
                            $sql = "SELECT AcademicYearID, ".$indexVar['libappraisal']->getLangSQL("YearNameB5","YearNameEN")." as YearName FROM ACADEMIC_YEAR WHERE Sequence='".($currSeq+$y)."'";
                            $pastYearData = current($connection->returnArray($sql));
                            if(!isset($yearCPDHour[$pastYearData['AcademicYearID']])){
                                $yearCPDHour[$pastYearData['AcademicYearID']] = 0;
                            }
                            $x15 .= $pastYearData['YearName']." ".$Lang['Appraisal']['ARFormCPDHourSchoolYear']."( <span id='yearHour_".$pastYearData['AcademicYearID']."_span'>".$yearCPDHour[$pastYearData['AcademicYearID']]."</span>".$Lang['Appraisal']['ARFormCPDHourHour'].")   ";
                        }
                    }
                    $x15 .= "</div>";
                    $x15 .= "</div>";
                    $x15 .= "<br/>";
                }
                $x15 .= "<div id=\"CPDLayer\">";
                $x15 .= "<table id=\"CPDTable\" class=\"common_table_list\">";
                $x15 .= "<thead>";
                $x15 .= "<tr>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormCourseTitle']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormCourseNature']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormCourseContent']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormOrgnBody']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormStartDate']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormEndDate']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormCPDMode']."</th>";
                $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormCPDDomain']."</th>";
                if($sys_custom['TeacherPortfolio']["cpdSubjectTag"]){
                    $x15 .= "<th style=\"width:10%\">".$Lang['Appraisal']['ARFormCPDFormSubjectRelated']."</th>";
                }
                $x15 .= "<th style=\"width:8%\">".$Lang['Appraisal']['ARFormCPDFormCPDHour']."</th>";
                //$x15 .= "<th style=\"width:8%\">".$Lang['Appraisal']['ARFormCPDFormBasicLawHour']."</th>";
                //$x15 .= "<th style=\"width:4%\"></th>";
                $x15 .= "</tr></thead>";
                $x15 .= "<tbody>";
                for($i=0;$i<sizeof($a);$i++){
                    $j=$i+1;
                    $x15 .= "<tr id=\"CPDRow_".$j."\">";
                    $x15 .= "<td>";
                    $x15 .= $a[$i]["CourseTitle"];
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $a[$i]["Nature"];
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $a[$i]["Content"];
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $a[$i]["Organization"];
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $a[$i]["StartDate"];
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $a[$i]["EndDate"];
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    $x15 .= $Lang['Appraisal']['ARFormCPDMode'][$a[$i]["CPDMode"]];
                    $x15 .= "</td>";
                    $x15 .= "<td>";
                    for($idx=0; $idx < 6; $idx++){
                        if($a[$i]['CPDDomain'.$idx]=="1"){
                            $x15 .= $Lang['Appraisal']['ARFormCPDDomain'][$idx]."<br>";
                        }
                    }
                    $x15 .= "</td>";
                    if($sys_custom['TeacherPortfolio']["cpdSubjectTag"]){
                        $x15 .= "<td>";
                        for($idx=0; $idx < count($Lang['Appraisal']['ARFormCPDSubject']); $idx++){
                            if($a[$i]['SubjectRelated'.$idx]=="1"){
                                $x15 .= $Lang['Appraisal']['ARFormCPDSubject'][$idx]."<br>";
                            }
                        }
                        $x15 .= "</td>";
                    }
                    $x15 .= "<td>";
                    $x15 .= $a[$i]['NoOfSection'];
                    $x15 .= "</td>";
                    //$x15 .= "<td>";
                    //$x15 .= $a[$i]['BasicLawRelated'];
                    //$x15 .= "</td>";
                    //$x15 .= "<td>";
                    //$x15 .= "</td>";
                    $x15 .= "</tr>";
                    $runningIndex++;
                }
                $x15 .= "</tbody>";
                $x15 .= "</table>";
                if($Lang['Appraisal']['ARFormCPDRmk']!=""){
                    $x15 .= "<p>";
                    $x15 .= $Lang['Appraisal']['ARFormCPDRmk'];
                    $x15 .= "</p>";
                }
                $x15 .= "</div><br/>";
                if($preDefHeader[15]["PageBreakAfter"]==1){
                    $x15 .= "<p style=\"page-break-after:always;\"></p>";
                }
        }
        //======================================================================== Attendnace Information ========================================================================//
        if($preDefHeader[16]["Incl"]==1){
            if($sys_custom['eAppraisal']['AttendanceAutoSync'] == true && ($sys_custom['eAppraisal']['ltmpsTransferAttendnanceDataToForm'] || $activeForm==true)){
                global $PATH_WRT_ROOT;
                include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
                $StaffAttend3 = new libstaffattend3();
                $staffs = $StaffAttend3->Get_Staff_Info($userID);
                $Params = array();
                $Params["StatusAbsent"] = 1;
                $Params["StatusLate"] = 1;
                $Params["StatusEarlyLeave"] = 1;
                $Params["StatusOuting"] = 1;
                $Params["StatusHoliday"] = 1;
                $Params["HaveRecord"] = 1;
                $Params["SelectStaff"] = Get_Array_By_Key($staffs,'UserID');
                
                $sql = "SELECT AcademicYearID FROM INTRANET_PA_T_FRMSUM fs INNER JOIN INTRANET_PA_C_CYCLE cc ON fs.CycleID=cc.CycleID WHERE fs.RecordID='$recordID'";
                $academicYearID = current($connection->returnVector($sql));
                $sql = "SELECT MIN(TermStart) as YearStart, MAX(TermEnd) as YearEnd FROM ACADEMIC_YEAR_TERM ayt INNER JOIN ACADEMIC_YEAR ay ON ayt.AcademicYearID = ay.AcademicYearID WHERE ay.AcademicYearID='".$academicYearID."'";
                $academicYear = current($connection->returnArray($sql));
                
                $dataTable = array();
                $sql = "Select ReasonID,TAReasonID,Days,TypeID,MapID From INTRANET_PA_S_ATTEDNACE_REASON_MAPPING";
                $mappingTable = $connection->returnArray($sql);
                $mappingArray = array();
                foreach($mappingTable as $mt){
                    $mappingArray[$mt['TypeID'].'_'.$mt['ReasonID']] = array("ReasonID"=>$mt['ReasonID'], "TAReasonID"=>$mt['TAReasonID'], "Days"=>$mt['Days'], "TypeID"=>$mt['TypeID']);
                }
                $sql = "SELECT TAReasonID,CountTotalBeforeThisReason FROM INTRANET_PA_S_ATTEDNACE_REASON";
                $reasonTable = $connection->returnArray($sql);
                $sql = "SELECT DATE_FORMAT(PeriodStart, '%Y-%m-%d') as PeriodStart, DATE_FORMAT(PeriodEnd, '%Y-%m-%d') as PeriodEnd, TAColumnID FROM INTRANET_PA_S_ATTEDNACE_COLUMN ORDER BY DisplayOrder ASC";
                $columnTable = $connection->returnArray($sql);
                foreach($reasonTable as $reason){
                    $dataTable[$reason['TAReasonID']] = array();
                    foreach($columnTable as $col){
                        $dataTable[$reason['TAReasonID']][$col['TAColumnID']] = 0;
                        $Params["TargetStartDate"] = $col['PeriodStart'];
                        $Params["TargetEndDate"] = $col['PeriodEnd'];
                        $StartDate = explode('-',$Params["TargetStartDate"]);
                        $StartYear = $StartDate[0];
                        $StartMonth = $StartDate[1];
                        $StartDay = $StartDate[2];
                        if($sys_custom['eAppraisal']['ltmpsTransferAttendnanceDataToForm']){
                            if((strtotime($academicYear['YearStart']) > strtotime($Params["TargetStartDate"])) || (strtotime($academicYear['YearEnd']) < strtotime($Params["TargetStartDate"]))){
                                if($StartMonth >= 1 && $StartMonth <= 8){
                                    $StartYear = date('Y', strtotime($academicYear['YearEnd']));
                                }else{
                                    $StartYear = date('Y', strtotime($academicYear['YearStart']));
                                }
                                $Params["TargetStartDate"] = $StartYear."-".$StartMonth."-".$StartDay;
                            }
                        }
                        $EndDate = explode('-',$Params["TargetEndDate"]);
                        $EndYear = $EndDate[0];
                        $EndMonth = $EndDate[1];
                        $EndDay = $EndDate[2];
                        if($sys_custom['eAppraisal']['ltmpsTransferAttendnanceDataToForm']){
                            if((strtotime($academicYear['YearStart']) > strtotime($Params["TargetEndDate"])) || (strtotime($academicYear['YearEnd']) < strtotime($Params["TargetEndDate"]))){
                                if($EndMonth >= 1 && $EndMonth <= 8){
                                    $EndYear = date('Y', strtotime($academicYear['YearEnd']));
                                }else{
                                    $EndYear = date('Y', strtotime($academicYear['YearStart']));
                                }
                                $Params["TargetEndDate"] = $EndYear."-".$EndMonth."-".$EndDay;
                            }
                        }
                        $YearArr = array();// start year to end year
                        $MonthArr = array();// months of start year to months of end year [assoc array using numeric year as key]
                        for($y = intval($StartYear);$y<=intval($EndYear);$y++)
                        {
                            $YearArr[] = $y;
                            $MonthArr[$y] = array();
                        }
                        for($i=0;$i<sizeof($YearArr);$i++)
                        {
                            if(sizeof($YearArr)==1)//only 1 year
                            {
                                for($m=intval($StartMonth);$m<=intval($EndMonth);$m++)
                                {
                                    $MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
                                }
                            }else if($i==0)//first year
                            {
                                for($m=intval($StartMonth);$m<=12;$m++)
                                {
                                    $MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
                                }
                            }else if($i==(sizeof($YearArr)-1))//last year
                            {
                                for($m=1;$m<=$EndMonth;$m++)
                                {
                                    $MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
                                }
                            }else// middle years
                            {
                                for($m=1;$m<=12;$m++)
                                {
                                    $MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
                                }
                            }
                        }
                        $AssocDataList = array();
                        for($i=0;$i<sizeof($YearArr);$i++)
                        {
                            $CurYear = $YearArr[$i];// int type e.g. 2009
                            for($j=0;$j<sizeof($MonthArr[$CurYear]);$j++)
                            {
                                $CurMonth = $MonthArr[$CurYear][$j]; //string type e.g. 01 which is January
                                
                                $tempArr = $StaffAttend3->Get_Report_Customize_Monthly_Detail_Report_Data($Params, $CurYear, $CurMonth, 2);
                                if(sizeof($tempArr)>0)
                                    foreach($tempArr as $theUser => $Record)
                                    {
                                        if(sizeof($Record)>0)
                                            foreach($Record as $Datekey => $Value)
                                            {
                                                $AssocDataList[$theUser][$Datekey]=$Value;
                                            }
                                    }
                            }
                        }
                        
                        $attendanceData = $AssocDataList[$userID];
                        $startTimeTS = strtotime($Params["TargetStartDate"]);
                        $endTimeTS = strtotime($Params["TargetEndDate"]);
                        for($currTime = $startTimeTS; $currTime <= $endTimeTS; $currTime += (24*60*60)){
                            $currTimeYMD = date('Y-m-d',$currTime);
                            if(empty($attendanceData[$currTimeYMD])){
                                continue;
                            }else{
                                foreach($attendanceData[$currTimeYMD] as $attData){
                                    $realReasonID = $attData['ReasonID'];
                                    if(empty($realReasonID)){
                                        $realReasonID = $attData['ELReasonID'];
                                    }
                                    $statusNo = $attData['Status'];
                                    if($statusNo != CARD_STATUS_NORMAL){
                                        $isTreated = false;
                                        if($attData['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE){
                                            if(!is_null($realReasonID)){
                                                if($mappingArray[CARD_STATUS_EARLYLEAVE."_".$realReasonID]['TAReasonID']==$reason['TAReasonID']){
                                                    $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[CARD_STATUS_EARLYLEAVE."_".$realReasonID]['Days'];
                                                }
                                            }else{
                                                if(CARD_STATUS_EARLYLEAVE == $mappingArray[CARD_STATUS_EARLYLEAVE."_0"]['TypeID'] && $mappingArray[CARD_STATUS_EARLYLEAVE."_0"]['TAReasonID']==$reason['TAReasonID']){
                                                    $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[CARD_STATUS_EARLYLEAVE."_0"]['Days'];
                                                }
                                            }
                                            $isTreated = true;
                                        }
                                        if($attData['InSchoolStatus'] == CARD_STATUS_LATE){
                                            if(!is_null($realReasonID) && $realReasonID != $attData['ELReasonID']){
                                                if($mappingArray[CARD_STATUS_LATE."_".$realReasonID]['TAReasonID']==$reason['TAReasonID']){
                                                    $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[CARD_STATUS_LATE."_".$realReasonID]['Days'];
                                                }
                                            }else{
                                                if(CARD_STATUS_LATE == $mappingArray[CARD_STATUS_LATE."_0"]['TypeID'] && $mappingArray[CARD_STATUS_LATE."_0"]['TAReasonID']==$reason['TAReasonID']){
                                                    $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[CARD_STATUS_LATE."_0"]['Days'];
                                                }
                                            }
                                            $isTreated = true;
                                        }
                                        if(!$isTreated){
                                            if(!is_null($realReasonID)){
                                                if($mappingArray[$statusNo."_".$realReasonID]['TAReasonID']==$reason['TAReasonID']){
                                                    $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[$statusNo."_".$realReasonID]['Days'];
                                                }
                                            }else{
                                                if($statusNo == $mappingArray[$statusNo."_0"]['TypeID'] && $mappingArray[$statusNo."_0"]['TAReasonID']==$reason['TAReasonID']){
                                                    $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[$statusNo."_0"]['Days'];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if($reason['CountTotalBeforeThisReason'] == 1){
                        $totalData = array();
                        foreach($dataTable as $reasonID=>$reasonData){
                            foreach($reasonData as $colID=>$colData){
                                if(isset($totalData[$colID])){
                                    $totalData[$colID] += $dataTable[$reasonID][$colID];
                                }else{
                                    $totalData[$colID] = $dataTable[$reasonID][$colID];
                                }
                            }
                        }
                        $dataTable['total'] = $totalData;
                    }
                }
                
                foreach($dataTable as $reasonID=>$reasonData){
                    if($reasonID=="total")continue;
                    foreach($reasonData as $colID=>$colData){
                        $sql = "SELECT AttID FROM INTRANET_PA_S_ATTEDNACE_DATA WHERE RlsNo='$rlsNo' AND RecordID='$recordID' AND BatchID='$batchID' AND UserID='".$userID."' AND TAReasonID='".$reasonID."' AND TAColumnID='".$colID."'";
                        $attID = $connection->returnVector($sql);
                        if(empty($attID)){
                            $sql = "INSERT INTO INTRANET_PA_S_ATTEDNACE_DATA (RlsNo, RecordID, BatchID, UserID, TAReasonID, TAColumnID, Days, DateInput, InputBy, DateModified, ModifyBy)
									VALUES ('$rlsNo', '$recordID', '$batchID', '".$userID."', '".$reasonID."', '".$colID."', '".$colData."', NOW(), '".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."')";
                        }else{
                            $sql = "UPDATE INTRANET_PA_S_ATTEDNACE_DATA SET Days='".$colData."', DateModified=NOW(), ModifyBy='".$_SESSION['UserID']."' WHERE RlsNo='$rlsNo' AND RecordID='$recordID' AND BatchID='$batchID' AND UserID='".$userID."' AND TAReasonID='".$reasonID."' AND TAColumnID='".$colID."'";
                        }
                        //echo $sql;
                        $connection->db_db_query($sql);
                    }
                }
                unset($dataTable);
                unset($totalData);
                
                // log attendance sync
                $logData = array();
                $recordDetail = array();
                $logData['RlsNo'] = $rlsNo;
                $logData['RecordID'] = $recordID;
                $logData['BatchID'] = $batchID;
                $logData['Function'] = 'Data Sync';
                $logData['RecordType'] = 'Attedance Import';
                $recordDetail[] = array(
                    "Parameter"=>"UserID",
                    "DisplayName"=>$Lang['Appraisal']['CycleTemplate']['StfIdvName'],
                    "New"=>$userID,
                    "MapWithTable"=>$appraisalConfig['INTRANET_USER'],
                    "MapDisplay"=>"EnglishName"
                );
                $logData['RecordDetail'] = $json->encode($recordDetail);
                $result = $indexVar['libappraisal']->addFormFillingLog($logData);
            }
            
            $sql = "SELECT data.TAReasonID, data.TAColumnID, data.Days, data.Remarks FROM  INTRANET_PA_S_ATTEDNACE_DATA data
					INNER JOIN INTRANET_PA_S_ATTEDNACE_REASON reason ON data.TAReasonID=reason.TAReasonID
					INNER JOIN INTRANET_PA_S_ATTEDNACE_COLUMN col ON data.TAColumnID=col.TAColumnID
					WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."' AND UserID='".$userID."'
					ORDER BY data.TAReasonID ASC, col.DisplayOrder ASC";
            $dataRaw = 	$connection->returnArray($sql);
            $dataTable = array();
            $reasonDataTable = array();
            $hasReasonDataContent = false;
            foreach($dataRaw as $d){
                $dataTable[$d['TAReasonID']][$d['TAColumnID']] = $d['Days'];
                if(!isset($reasonDataTable[$d['TAReasonID']])){
                    $reasonDataTable[$d['TAReasonID']] = $d['Remarks'];
                    if($d['Remarks']!=""){
                        $hasReasonDataContent = true;
                    }
                }
            }
            if(!$hasReasonDataContent){
                $reasonDataTable = array();
            }
            $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("TAReasonNameChi","TAReasonNameEng")." as ReasonTitle, ReasonUnit, CountTotalBeforeThisReason, TAReasonID FROM INTRANET_PA_S_ATTEDNACE_REASON";
            $reasonTable = $connection->returnArray($sql);
            $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("TAColumnNameChi","TAColumnNameEng")." as ColumnTitle, PeriodStart, PeriodEnd, TAColumnID FROM INTRANET_PA_S_ATTEDNACE_COLUMN ORDER BY DisplayOrder ASC";
            $columnTable = $connection->returnArray($sql);
            if(empty($this->GeneralSetting['PreDefinedFormAttendanceTableOrientation'])){
                $orientation = 0;
            }else{
                $settingValueArr = unserialize($this->GeneralSetting['PreDefinedFormAttendanceTableOrientation']);
                $orientation = isset($settingValueArr[$templateID])?$settingValueArr[$templateID]:0;
            }
            if(empty($columnTable) || empty($reasonTable)){
                $x16 .= "";
            }else{
                $x16 .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[16]["Descr"])."</b><br/>\r\n";
                $x16 .= "<style>";
                $x16 .= ".common_table_list .tableTotal td{border-top: 2px solid ;border-bottom: 2px solid ;}";
                $x16 .= "</style>";
                $x16 .= "<div id=\"AttendanceRecordInfoLayer\">";
                $x16 .= "<table id=\"AttendanceRecordInfoTable\" class=\"common_table_list\">";
                if($orientation==0){
                    $x16 .= "<thead>";
                    $x16 .= "<tr>";
                    $x16 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormLeaCat']."</th>";
                    $colWidth = round(50/count($columnTable),0);
                    foreach($columnTable as $col){
                        $x16 .= "<th style=\"width:$colWidth%\">".$col['ColumnTitle']."</th>";
                    }
                    if(!empty($reasonDataTable)){
                        $x16 .= "<th style=\"width:40%\">".$Lang['Appraisal']['ARFormOptionalRemarks']."</th>";
                    }
                    $x16 .= "</tr></thead>";
                    $x16 .= "<tbody>";
                    foreach($reasonTable as $reason){
                        $x16 .= "<tr>";
                        $x16 .= "	<td>".$reason['ReasonTitle']."</td>";
                        foreach($columnTable as $col){
                            $x16 .= "	<td  id='attendanceField_".$reason['TAReasonID']."_".$col['TAColumnID']."' style='text-align: right'><span>".$dataTable[$reason['TAReasonID']][$col['TAColumnID']]."</span>".$Lang['Appraisal']['ARFormAttendanceUnitList'][$reason['ReasonUnit']]."</td>";
                            if(isset($totalData[$col['TAColumnID']])){
                                $totalData[$col['TAColumnID']] += $dataTable[$reason['TAReasonID']][$col['TAColumnID']];
                            }else{
                                $totalData[$col['TAColumnID']] = $dataTable[$reason['TAReasonID']][$col['TAColumnID']];
                            }
                        }
                        if(!empty($reasonDataTable)){
                            $x16 .= "	<td>".$reasonDataTable[$reason['TAReasonID']]."</td>";
                        }
                        $x16 .= "</tr>";
                        if($reason['CountTotalBeforeThisReason']){
                            $x16 .= "<tr class='tableTotal'>";
                            $x16 .= "	<td>".$Lang['Appraisal']['ARFormAttendanceLeaveTotal']."</td>";
                            foreach($columnTable as $col){
                                $x16 .= "	<td id='attendanceField_total_".$col['TAColumnID']."' style='text-align: right'><span>".$totalData[$col['TAColumnID']]."</span>".$Lang['Appraisal']['ARFormAttendanceUnitList'][$reason['ReasonUnit']]."</td>";
                            }
                            if(!empty($reasonDataTable)){
                                $x16 .= "	<td></td>";
                            }
                            $x16 .= "</tr>";
                        }
                    }
                    $x16 .= "</tbody>";
                }elseif($orientation==1){
                    $x16 .= "<thead>";
                    $x16 .= "<tr>";
                    $x16 .= "<th></th>";
                    foreach($reasonTable as $reason){
                        $x16 .= "<th width='".round(100/(count($reasonTable)+1),0)."%'>".$reason['ReasonTitle']."</th>";
                    }
                    $x16 .= "</tr></thead>";
                    $x16 .= "<tbody>";
                    foreach($columnTable as $col){
                        $x16 .= "<tr>";
                        $x16 .= "<td class=\"field_title\">".$col['ColumnTitle']."</td>";
                        foreach($reasonTable as $reason){
                            $x16 .= "	<td id='attendanceField_".$reason['TAReasonID']."_".$col['TAColumnID']."' style='text-align: right'><span>".$dataTable[$reason['TAReasonID']][$col['TAColumnID']]."</span>".$Lang['Appraisal']['ARFormAttendanceUnitList'][$reason['ReasonUnit']]."</td>";
                        }
                        $x16 .= "</tr>";
                    }
                    $x16 .= "<tr>";
                    if(!empty($reasonDataTable)){
                        $x16 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormOptionalRemarks']."</td>";
                        foreach($reasonTable as $reason){
                            $x16 .= "	<td style='text-align: right'>".$reasonDataTable[$reason['TAReasonID']]."</td>";
                        }
                        $x16 .= "</tr>";
                    }
                    $x16 .= "</tbody>";
                }
                if($sys_custom['eAppraisal']['LTMPSPrintingStyle']){
                    $x16 .= "</table>";
                    $x16 .= "<p class=\"extraRmk\">".$Lang['Appraisal']['ARFormAttendanceRmk']."</p>";
                    $x16 .= "</div><br/>";
                }else{
                    $x16 .= "</table></div><br/>";
                }
            }
            if($preDefHeader[16]["PageBreakAfter"]==1){
                $x16 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Teaching Experience ========================================================================//
        if($preDefHeader[17]["Incl"]==1){
            $x17 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[17]["Descr"])."<br/>\r\n";
            
            $sql = "SELECT RecordID,BatchID,SeqID,SchoolName,StartDate,EndDate,Post
					FROM INTRANET_PA_T_PRE_TEACHEREXP WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."';";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $runningIndex = 0;
            
            $x17 .= "<div id=\"studentTrainingLayer\">";
            $x17 .= "<div id=\"teachingExpLayer\">";
            $x17 .= "<table id=\"teachingExpTable\" class=\"common_table_list\">";
            $x17 .= "<thead>";
            $x17 .= "<tr>";
            $x17 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormExpSchoolName']."</th>";
            $x17 .= "<th style=\"width:40%\">".$Lang['Appraisal']['ARFormExpDate']."</th>";
            $x17 .= "<th style=\"width:25%\">".$Lang['Appraisal']['ARFormExpPost']."</th>";
            $x17 .= "</tr></thead>";
            $x17 .= "<tbody>";
            
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x17 .= "<tr id=\"teachingExpRow_".$j."\">";
                $x17 .= "<td>";
                $x17 .= $a[$i]["SchoolName"];
                $x17 .= "</td>";
                $x17 .= "<td>";
                $x17 .= $Lang['General']['From'].date('Y-m-d', strtotime($a[$i]["StartDate"]))." ".$Lang['General']['To']." ".date('Y-m-d', strtotime($a[$i]["EndDate"]));
                $x17 .= "</td>";
                $x17 .= "<td>";
                $x17 .= $a[$i]["Post"];
                $x17 .= "</td>";
                $x17 .= "</tr>";
                $runningIndex++;
            }
            
            $x17 .= "</tbody>";
            $x17 .= "</table>";
            $x17 .= "</div><br/>";
            if($preDefHeader[17]["PageBreakAfter"]==1){
                $x17 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Training Records ========================================================================//
        if($preDefHeader[18]["Incl"]==1){
            $sql = "SELECT RecordID,BatchID,SeqID,DATE_FORMAT(StartDate,'%Y-%m-%d') as StartDate,DATE_FORMAT(EndDate,'%Y-%m-%d') as EndDate,Issuer,QualiDescr,Programme
			FROM INTRANET_PA_T_PRE_TRAINING  WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."';";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x18 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[18]["Descr"])."<br/>\r\n";
            $runningIndex = 0;
            
            $x18 .= "<div id=\"trainingInfoLayer\">";
            $x18 .= "<table id=\"trainingInfoTable\" class=\"common_table_list\">";
            $x18 .= "<thead><tr>";
            $x18 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormTrainingUni']."</th>";
            $x18 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormTrainingDate']."</th>";
            $x18 .= "<th style=\"width:20%\">".$Lang['Appraisal']['ARFormTrainingAward']."</th>";
            $x18 .= "<th style=\"width:30%\">".$Lang['Appraisal']['ARFormTrainingSbj']."</th>";
            $x18 .= "</tr></thead>";
            $x18 .= "<tbody>";
            $runningIndex = 0;
            
            for($i=0;$i<sizeof($a);$i++){
                $j=$i+1;
                $x18 .= "<tr id=\"trainingRow_".$j."\">";
                $x18 .= "<td>";
                $x18 .= $a[$i]["Issuer"];
                $x18 .= "</td>";
                $x18 .= "<td>";
                $x18 .= $Lang['General']['From'].date('Y-m-d', strtotime($a[$i]["StartDate"]))." ".$Lang['General']['To']." ".date('Y-m-d', strtotime($a[$i]["EndDate"]));
                $x18 .= "</td>";
                $x18 .= "<td>";
                $x18 .= $a[$i]["QualiDescr"];
                $x18 .= "</td>";
                $x18 .= "<td>";
                $x18 .= $a[$i]["Programme"];
                $x18 .= "</td>";
                $x18 .= "</tr>";
                $runningIndex++;
            }
            
            $x18 .= "</tbody>";
            $x18 .= "</table>";
            $x18 .= "</div><br/>";
            if($preDefHeader[18]["PageBreakAfter"]==1){
                $x18 .= "<p style=\"page-break-after:always;\"></p>";
            }
        }
        //======================================================================== Sick Leave & lateness records ========================================================================//
        if($preDefHeader[19]["Incl"]==1){
            $sql = "SELECT SLLastYear,SLThisYear,LateLastYear,LateThisYear,IronMan,EarlyBird  FROM INTRANET_PA_C_ATTENDANCE WHERE CycleID='".$cycleID."' AND UserID='".$userID."'";
            //echo $sql;
            $a=$connection->returnResultSet($sql);
            $x19 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[19]["Descr"])."<br/>\r\n";
            
            $x19 .= "<div id=\"trainingInfoLayer\">";
            $x19 .= "<table id=\"trainingInfoTable\" class=\"common_table_list\">";
            $x19 .= "<thead><tr>";
            $x19 .= "<th style=\"width:20%;text-align: center;\" colspan=2>".$Lang['Appraisal']['SickLeaveRecord']."</th>";
            $x19 .= "<th style=\"width:20%;text-align: center;\" colspan=2>".$Lang['Appraisal']['LatenessRecord']."</th>";
            $x19 .= "<th style=\"width:10%;text-align: center;\">".$Lang['Appraisal']['IronMan']."</th>";
            $x19 .= "<th style=\"width:10%;text-align: center;\">".$Lang['Appraisal']['EarlyBird']."</th>";
            $x19 .= "</tr><tr>";
            $x19 .= "<th style=\"width:20%\">".$Lang['Appraisal']['LastAcademicYear']."</th>";
            $x19 .= "<th style=\"width:20%\">".$Lang['Appraisal']['CurrentAcademicYearTilMar']."</th>";
            $x19 .= "<th style=\"width:20%\">".$Lang['Appraisal']['LastAcademicYear']."</th>";
            $x19 .= "<th style=\"width:20%\">".$Lang['Appraisal']['CurrentAcademicYearTilMar']."</th>";
            $x19 .= "<th style=\"width:10%\">".$Lang['Appraisal']['LastAcademicYear']."</th>";
            $x19 .= "<th style=\"width:10%\">".$Lang['Appraisal']['LastAcademicYear']."</th>";
            $x19 .= "</tr></thead>";
            $x19 .= "<tbody>";
            $x19 .= "<tr height=\"34px\">";
            $x19 .= "<td>".$a[0]['SLLastYear']."</td>";
            $x19 .= "<td>".$a[0]['SLThisYear']."</td>";
            $x19 .= "<td>".$a[0]['LateLastYear']."</td>";
            $x19 .= "<td>".$a[0]['LateThisYear']."</td>";
            $x19 .= "<td>".$a[0]['IronMan']."</td>";
            $x19 .= "<td>".$a[0]['EarlyBird']."</td>";
            $x19 .= "</tr>";
            $x19 .= "</tbody>";
            $x19 .= "</table>";
            $x19 .= "</div><br/>";
        }
        
        if(!function_exists('headerSortByDisplayOrder')){
            function headerSortByDisplayOrder($a, $b){
                if($a['DisplayOrder'] == $b['DisplayOrder']) return 0;
                return ($a['DisplayOrder'] < $b['DisplayOrder'])?-1:1;
            }
        }
        usort($preDefHeader, 'headerSortByDisplayOrder');
        foreach($preDefHeader as $pdh){
            switch($pdh["DataTypeID"]){
                case "1":
                    $content .= $x0;
                    break;
                case "2":
                    $content .= $x1;
                    break;
                case "3":
                    $content .= $x2;
                    break;
                case "4":
                    $content .= $x3;
                    break;
                case "5":
                    $content .= $x4;
                    break;
                case "6":
                    $content .= $x5;
                    break;
                case "7":
                    $content .= $x6;
                    break;
                case "8":
                    $content .= $x7;
                    break;
                case "9":
                    $content .= $x8;
                    break;
                case "10":
                    $content .= $x9;
                    break;
                case "11":
                    $content .= $x10;
                    break;
                case "12":
                    $content .= $x11;
                    break;
                case "13":
                    $content .= $x12;
                    break;
                case "14":
                    $content .= $x13;
                    break;
                case "15":
                    $content .= $x14;
                    break;
                case "16":
                    $content .= $x15;
                    break;
                case "17":
                    $content .= $x16;
                    break;
                case "18":
                    $content .= $x17;
                    break;
                case "19":
                    $content .= $x18;
                    break;
                case "20":
                    $content .= $x19;
                    break;
            }
        }
        $x .= $content;
        return $x;
        
    }
    
    function getSdfForm($connection,$templateID,$userID,$recordID,$cycleID,$rlsNo,$batchID){
        $indexVar['libappraisal'] = new libappraisal();
        $indexVar['libappraisal_ui'] = new libappraisal_ui();
        global $intranet_session_language, $Lang, $sys_custom;
        
        if($userID==""&&$recordID==""&&$cycleID==""&&$rlsNo==""&&$batchID==""){
            $sql = "SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormTitle,"
                .$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode,"
                    .$indexVar['libappraisal']->getLangSQL("HdrRefChi","HdrRefEng")." as HdrRef,"
                        .$indexVar['libappraisal']->getLangSQL("ObjChi","ObjEng")." as Obj,"
                            .$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,
				AppTgtChi,AppTgtEng,NeedSubj,NeedClass,NeedSubjLang
				FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".IntegerSafe($templateID)."';";
                            //echo $sql."<br/>";
        }
        else {
            $sql = "SELECT iptf.RecordID,UserID,iptf.CycleID,CycleDescr,iptf.TemplateID,FormTitle,FormCode,HdrRef,Obj,Descr,AppRoleID,AppTgtChi,AppTgtEng,AcademicYearID,COALESCE(SubDate,'') as SubDate,
			EditPrdFr,EditPrdTo,CASE WHEN NOW()>EditPrdTo THEN '1' ELSE '0' END as OverdueDate,YearClassID,SubjectID,TeachLang,NeedSubj,NeedClass,NeedSubjLang
			FROM(
				SELECT RecordID,CycleID,TemplateID,UserID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".IntegerSafe($recordID)."'
			) as iptf
			INNER JOIN(
				SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormTitle,"
				    .$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode,"
				        .$indexVar['libappraisal']->getLangSQL("HdrRefChi","HdrRefEng")." as HdrRef,"
				            .$indexVar['libappraisal']->getLangSQL("ObjChi","ObjEng")." as Obj,"
				                .$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,
				AppTgtChi,AppTgtEng,NeedSubj,NeedClass,NeedSubjLang
				FROM INTRANET_PA_S_FRMTPL
			) as ipsf ON iptf.TemplateID=ipsf.TemplateID
			LEFT JOIN(
				SELECT CycleID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as CycleDescr,AcademicYearID
				FROM INTRANET_PA_C_CYCLE
			) as ipcc ON iptf.CycleID=ipcc.CycleID
			LEFT JOIN(
				SELECT BatchID,CycleID,EditPrdFr,EditPrdTo,AppRoleID FROM INTRANET_PA_C_BATCH WHERE BatchID='".IntegerSafe($batchID)."'
			) as ipcb ON ipcc.CycleID=ipcb.CycleID
			LEFT JOIN(
				SELECT RlsNo,RecordID,BatchID,SubDate FROM INTRANET_PA_T_FRMSUB
			) as iptfrmSub ON iptf.RecordID=iptfrmSub.RecordID AND ipcb.BatchID=iptfrmSub.BatchID
			LEFT JOIN(
				SELECT RlsNo,RecordID,BatchID,YearClassID,SubjectID,TeachLang FROM INTRANET_PA_T_SLF_HDR WHERE RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
			) as iptsh ON iptf.RecordID=iptsh.RecordID AND iptfrmSub.RlsNo=iptsh.RlsNo AND iptfrmSub.BatchID=iptsh.BatchID;";
				                //echo $sql."<br/>";
        }
        $header=$connection->returnResultSet($sql);
        $templateID=$header[0]["TemplateID"];
        $academicYearID=$header[0]["AcademicYearID"];
        /*$cycle=$header[0]["Cycle"];
         $userID = $header[0]["UserID"];
         $canEdit=($header[0]["OverdueDate"]=="0"&&$header[0]["SubDate"]=="")?"1":"0";
         $academicYearID=$header[0]["AcademicYearID"];
         $appRole=Get_Lang_Selection($header[0]["AppTgtChi"],$header[0]["AppTgtEng"]);
         */
        
        //======================================================================== Section ========================================================================//
        $sql="SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder
		FROM(
			SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
			$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder FROM INTRANET_PA_S_FRMSEC WHERE TemplateID='".IntegerSafe($templateID)."' AND ParentSecID IS NULL
		) as ipsf ORDER BY DisplayOrder;
		";
			//echo $sql."<br/><br/>";
			$sectionHeader=$connection->returnResultSet($sql);
			
			// prepare the QID for that form
			$parSecID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($sectionHeader,"SecID");
			$sql="SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,ParentSecID
		FROM(
			SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
			$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder,ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID='".IntegerSafe($templateID)."' AND ParentSecID IS NOT NULL
			AND ParentSecID IN (".$parSecID.")
		) as ipsf";
			//echo $sql."<br/><br/>";
			$y=$connection->returnResultSet($sql);
			$subSecID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"SecID");
			$sql="SELECT QCatID,SecID,".$indexVar['libappraisal']->getLangSQL("QCatCodChi","QCatCodEng")." as QCatCod,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as QCatDescr,".
			 			"DisplayOrder as QCatDisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID IN (".$subSecID.")";
			//echo $sql."<br/><br/>";
			$y=$connection->returnResultSet($sql);
			$qCatID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"QCatID");
			$sql="SELECT QID,QCatID FROM INTRANET_PA_S_QUES WHERE QCatID IN (".$qCatID.")";
			$y=$connection->returnResultSet($sql);
			$qID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"QID");
			// prepare the QID for that form
			
			//======================================================================== Personal Master ========================================================================//
			
			$x .= "<div class=\"formTitle\">".$header[0]["FormTitle"]."</div>"."\r\n";
			$x .= "<div>";
			if($header[0]["FormCode"]!=""&&$header[0]["Obj"]!=""){
			    $x .= "<span class=\"formObjective\" style=\"float: left;\">".$header[0]["FormCode"]." - ".$header[0]["Obj"]."</span>";
			}
			else if($header[0]["FormCode"]!=""&&$header[0]["Obj"]==""){
			    $x .= "<span class=\"formObjective\" style=\"float: left;\">".$header[0]["FormCode"]."</span>";
			}
			else if($header[0]["FormCode"]==""&&$header[0]["Obj"]!=""){
			    $x .= "<span class=\"formObjective\" style=\"float: left;\">".$header[0]["Obj"]."</span>";
			}
			else if($header[0]["FormCode"]==""&&$header[0]["Obj"]==""){
			    $x .= "<span class=\"formObjective\" style=\"float: left;\"></span>";
			}
			if($header[0]["Descr"]!=""){
			    $x .= "<br/><br/><span>".$header[0]["Descr"]."</span>";
			}
			$x .= "<span class=\"formRefHeader\" style=\"float: right;\">".$header[0]["HdrRef"]."</span>";
			$x .="</div>"."\r\n";
			$x .= "<div>".$header[0]["Descr"]."</div>".($header[0]["Descr"]!="")?"<br/>":""."\r\n";
			
			$tgtName = ($_SESSION['intranet_session_language']=="en")?$Lang['Appraisal']['ARFormTgtName']." ".$header[0]["AppTgtEng"]:$header[0]["AppTgtChi"].$Lang['Appraisal']['ARFormTgtName'];
			$x .= "<table class=\"form_table_v30\">";
			$x .= "<tr><td class=\"field_title\">".$tgtName."</td><td colspan=\"3\"></td></tr>"."\r\n";
			if($header[0]["NeedSubj"]=="1"){
			    $subjectSQL=$indexVar['libappraisal']->getAllSubject();
			    $x .="<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormSubject']."</td><td>".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($subjectSQL, "SubjectID","SubjectTitleB5","SubjectTitleEN"), $selectionTags='id="subjectID" name="subjectID"', $SelectedType=$header[0]["SubjectID"], $all=0, $noFirst=0)."</td>";
			    if($header[0]["NeedSubjLang"]=="1"){
			        $x .="<td class=\"field_title\">".$Lang['Appraisal']['ARFormSubjectMedium']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('teachLang', 'teachLang', $header[0]["TeachLang"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td>";
			    }
			    $x .= "</tr>";
			}
			if($header[0]["NeedClass"]=="1"){
			    $yearClassSQL=$indexVar['libappraisal']->getAllYearClass($academicYearID);
			    $x .="<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormYearClass']."</td><td colspan=\"3\">".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($yearClassSQL, "YearClassID","ClassTitleB5","ClassTitleEN"), $selectionTags='id="yearClassID" name="yearClassID"', $SelectedType=$header[0]["YearClassID"], $all=0, $noFirst=0)."</td></tr>";;
			}
			//$x .= "</table>".($header[0]["NeedClass"]!="0" || $header[0]["NeedClass"]!="0")?"<br/>":"\r\n";;
			if($header[0]["NeedClass"]!="0" || $header[0]["NeedClass"]!="0"){
			    $appTag = "<br/>";
			}
			else{
			    $appTag = "\r\n";
			}
			$x .= "</table>".$appTag;
			
			for($h=0;$h<sizeof($sectionHeader);$h++){
			    $x .= "<div id=\"SecID_".$sectionHeader[$h]["SecID"]."\" name=\"SecID_".$sectionHeader[$h]["SecID"]."\">";
			    if($sectionHeader[$h]["SecCod"]!="" && $sectionHeader[$h]["SecTitle"]!=""){
			        $x .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($sectionHeader[$h]["SecCod"]."-".$sectionHeader[$h]["SecTitle"])."<br/>\r\n";
			    }
			    else if($sectionHeader[$h]["SecCod"]!="" && $sectionHeader[$h]["SecTitle"]==""){
			        $x .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($sectionHeader[$h]["SecCod"])."<br/>\r\n";
			    }
			    else if($sectionHeader[$h]["SecCod"]=="" && $sectionHeader[$h]["SecTitle"]!=""){
			        $x .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($sectionHeader[$h]["SecTitle"])."<br/>\r\n";
			    }
			    $x .= "</div><br/>\r\n";
			    $x .= "<table>";
			    $x .= "<tr><td>". nl2br($sectionHeader[$h]["SecDescr"])."</td></tr></table>".(($sectionHeader[$h]["SecDescr"]!="")?"<br/>":""."\r\n");
			    //======================================================================== Sub Section ========================================================================//
			    $sql="SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,ParentSecID
				FROM(
					SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
					$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder,ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID='".IntegerSafe($templateID)."' AND ParentSecID IS NOT NULL
					AND ParentSecID='".IntegerSafe($sectionHeader[$h]["SecID"])."'
				) as ipsf
				ORDER BY DisplayOrder;
				";
					//echo $sql."<br/><br/>";
					$a=$connection->returnResultSet($sql);
					for($i=0;$i<sizeof($a);$i++){
					    if($a[$i]["SecCod"]!="" && $a[$i]["SecTitle"]!=""){
					        $x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecCod"]."-".$a[$i]["SecTitle"])."\r\n";
					    }
					    else if($a[$i]["SecCod"]!="" && $a[$i]["SecTitle"]==""){
					        $x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecCod"])."\r\n";
					    }
					    else if($a[$i]["SecCod"]=="" && $a[$i]["SecTitle"]!=""){
					        $x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecTitle"])."\r\n";
					    }
					    $x .= "<table>";
					    $x .= "<tr><td>".(($a[$i]["SecDescr"]!="")? $a[$i]["SecDescr"]:""."\r\n")."</td></tr></table>".(($a[$i]["SecDescr"]!="")?"<br/>":""."\r\n");
					    
					    $sql="SELECT QCatID,SecID,".$indexVar['libappraisal']->getLangSQL("QCatCodChi","QCatCodEng")." as QCatCod,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as QCatDescr,QuesDispMode,".
									    "DisplayOrder as QCatDisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID='".$a[$i]["SecID"]."' ORDER BY DisplayOrder;";
					    //echo $sql."<br/><br/>";
					    $b=$connection->returnResultSet($sql);
					    for($j=0;$j<sizeof($b);$j++){
					        $x .= "<table width=\"100%\"><tr><td valign=\"top\" style=\"width:5%;\">".$b[$j]["QCatCod"]."</td><td>".nl2br($b[$j]["QCatDescr"])."</td></tr></table>";
					        //======================================================================== Question Category ========================================================================//
					        if($b[$j]["QuesDispMode"]=="1"||$b[$j]["QuesDispMode"]=="2"){
					            $x .= "<div style=\"padding-left:20px;\"><table class=\"common_table_list\" width=\"100%\">";
					        }
					        else{
					            $x .= "<div style=\"padding-left:20px;\"><table width=\"100%\">";
					        }
					        $x .= $this->getSDFSectionContent("",$a[$i]["SecID"],$b[$j]["QCatID"],$recordID,$rlsNo,$batchID);
					        $x .= "</table></div><br/>\r\n";
					    }
					}
					//$x .="</div>";
			}
			return $x;
    }
    function getSDFSectionContent($prefix,$secID,$qCatID,$recordID,$rlsNo,$batchID){
        $indexVar['libappraisal'] = new libappraisal();
        $indexVar['libappraisal_ui'] = new libappraisal_ui();
        
        $sql = "SELECT SecID,ipsqcat.QCatID,QID,QuesDispMode,QCodDes,Descr,QuesDisplayOrder,IsMC,IsScore,HasRmk,Rmk,IsTxtAns,TxtBoxNote,InputType,RmkLabel,IsMand
					FROM(
						SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,DisplayOrder as CatDisplayOrder,QuesDispMode FROM INTRANET_PA_S_QCAT WHERE SecID='".$secID."' AND QCatID='".$qCatID."'
					) as ipsqcat
					LEFT JOIN(
						SELECT QID,QCatID,".$indexVar['libappraisal']->getLangSQL("QCodChi","QCodEng")." as QCodDes,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,DisplayOrder,InputType,IsMC,IsScore,IsTxtAns,TxtBoxNote,HasRmk,".$indexVar['libappraisal']->getLangSQL("RmkLabelChi","RmkLabelEng")." as Rmk,
						DisplayOrder as QuesDisplayOrder,".$indexVar['libappraisal']->getLangSQL("RmkLabelChi","RmkLabelEng")." as RmkLabel,IsMand
						FROM INTRANET_PA_S_QUES WHERE QCatID='".$qCatID."'
					) as ipsques ON ipsqcat.QCatID=ipsques.QCatID
					ORDER BY QuesDisplayOrder;";
        $a=$this->returnResultSet($sql);
        //echo ($sql)."<br/><br/>";
        $quesDispMode=$a[0]["QuesDispMode"];
        
        $sql="SELECT SUM(LENGTH(".$indexVar['libappraisal']->getLangSQL("QCodChi","QCodEng").")) as content FROM  INTRANET_PA_S_QUES WHERE QCatID='".$qCatID."';";
        $x=$this->returnResultSet($sql);
        if($x[0]["content"] > 0){
            $hasContent = true;
        }
        
        if($quesDispMode==1){
            $sql = "SELECT MarkID,QCatID,".$indexVar['libappraisal']->getLangSQL("MarkCodChi","MarkCodEng")." as MarkCodDes,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,Score,DisplayOrder,".
                $indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr
						FROM INTRANET_PA_S_QLKS WHERE QCatID='".$qCatID."'
						ORDER BY DisplayOrder";
                //echo $sql."<br/><br/>";
                $b=$this->returnResultSet($sql);
                $descWidth ="20%";
                
                $content.="<tr>";
                if($hasContent == true){
                    $content .="<th style=\"width:5%\"></th>";
                }
                $content .= "<th style=\"width:".$descWidth."\"></th>";
                $dnmtr = (sizeof($b)==0)?1:sizeof($b);
                $thWidth=60/$dnmtr;
                for($j=0;$j<sizeof($b);$j++){
                    $content.="<th style=\"width:".floor($thWidth)."%\">".nl2br($b[$j]["Descr"])."</th>";
                }
                $content.="</tr>";
                
                for($i=0;$i<sizeof($a);$i++){
                    $hasRemark=$a[$i]["HasRmk"];
                    $rmkLabel=$a[$i]["RmkLabel"];
                    $rowSpan=($hasRemark==1)?"2":"1";
                    
                    $intSecID=$a[$i]["SecID"];
                    $intQusCodID=$i+1;
                    $qID=$a[$i]["QID"];
                    $sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND BatchID='".$batchID."' AND QID='".$qID."';";
                    //echo $sql."<br/><br/>";;
                    $c=$this->returnResultSet($sql);
                    $content.="<tr>";
                    if($hasContent == true){
                        $content.="<td valign=\"top\" rowspan=\"".$rowSpan."\">".$a[$i]["QCodDes"]."</td>";
                    }
                    if(strstr($a[$i]["Descr"], '<br')){
                        $a[$i]["Descr"] = preg_replace( "/\r|\n/", "", $a[$i]["Descr"]);
                    }else{
                        $a[$i]["Descr"] = preg_replace( "/\r|\n/", "", nl2br($a[$i]["Descr"]));
                    }
                    $content.="<td rowspan=\"".$rowSpan."\">".(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").$a[$i]["Descr"]."</td>";
                    for($j=0;$j<sizeof($b);$j++){
                        $content.="<td>";
                        $content.=$indexVar['libappraisal_ui']->Get_Radio_Button("qID_".$a[$i]["QID"].$j,"qID_".$a[$i]["QID"], $Value=$b[$j]["MarkID"], $isChecked=($c[0]["MarkID"]==$b[$j]["MarkID"])?1:0, $Class="", $Display=$b[$j]["MarkCodDes"], $Onclick="",$isDisabled=0);
                        $content.="</td>";
                    }
                    //$content.="<input type=\"hidden\" id=\"MarkID_".$a[$i]["QID"]."\" name=\"MarkID_".$a[$i]["QID"]."\" value=\"".$c[0]["MarkID"]."\">";
                    $content.="</tr>";
                    if($hasRemark==1){
                        $content.="<tr>";
                        $content.="<td colspan=\"".sizeof($b)."\">";
                        if($rmkLabel!=""){
                            $content.= $rmkLabel."<br/>";
                        }
                        $content.=$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td>";
                        $content.="</tr>";
                    }
                }
        }
        else if($quesDispMode==2){
            $sql = "SELECT MtxFldID,QCatID,".$indexVar['libappraisal']->getLangSQL("FldHdrChi","FldHdrEng")." as FldHdrDes,InputType,DisplayOrder
					FROM INTRANET_PA_S_QMTX  WHERE QCatID='".$qCatID."'
					ORDER BY DisplayOrder";
            //echo $sql."<br/><br/>";
            $b=$this->returnResultSet($sql);
            $hasQuestionDisplay = false;
            for($i=0;$i<sizeof($a);$i++){
                if($a[$i]["QCodDes"]!="" || $a[$i]["Descr"]!=""){
                    $hasQuestionDisplay = true;
                }
            }
            $descWidth =100;
            $content.="<tr>";
            if($hasContent == true){
                $content.="<th style=\"width:5%\"></th>";
                $descWidth = 95;
            }
            $dnmtr = (sizeof($b)==0)?1:sizeof($b);
            if($hasQuestionDisplay){
                $content.="<th style=\"width:15%\"></th>";
                $thWidth=($descWidth-15)/$dnmtr;
            }else{
                $thWidth=$descWidth/$dnmtr;
                $content.="<th style=\"width:1%\"></th>";
            }
            for($j=0;$j<sizeof($b);$j++){
                $content.="<th style=\"width:".floor($thWidth)."%\">".$b[$j]["FldHdrDes"]."</th>";
            }
            $content .= "</tr>";
            for($i=0;$i<sizeof($a);$i++){
                $hasRemark=$a[$i]["HasRmk"];
                $rmkLabel=$a[$i]["RmkLabel"];
                $rowSpan=($hasRemark==1)?"2":"1";
                $intSecID=$a[$i]["SecID"];
                $intQusCodID=$i+1;
                $qID=$a[$i]["QID"];
                $sql="SELECT RecordID,BatchID,QID,MtxFldID,Score,TxtAns,DATE_FORMAT(DateAns,'%Y-%m-%d') FROM INTRANET_PA_T_SLF_MTX WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND BatchID='".$batchID."' AND QID='".$qID."';";
                //echo $sql."<br/><br/>";;
                $c=$this->returnResultSet($sql);
                $content.="<tr>";
                if($hasContent == true){
                    $content.="<td valign=\"top\" rowspan=\"".$rowSpan."\">".$a[$i]["QCodDes"]."</td>";
                }
                if(strstr($a[$i]["Descr"], '<br')){
                    $a[$i]["Descr"] = preg_replace( "/\r|\n/", "", $a[$i]["Descr"]);
                }else{
                    $a[$i]["Descr"] = preg_replace( "/\r|\n/", "", nl2br($a[$i]["Descr"]));
                }
                $content.="<td rowspan=\"".$rowSpan."\">".(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").$a[$i]["Descr"]."</td>";
                for($j=0;$j<sizeof($b);$j++){
                    $content.="<td>";
                    if($b[$j]["InputType"]==0){
                        $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                        $content.=$indexVar['libappraisal_ui']->GET_TEXTBOX("qIDTxtAns_".$a[$i]["QID"], "qIDTxtAns_".$a[$i]["QID"], "", $OtherClass='', $OtherPar=array('maxlength'=>255))."".$unit;
                    }
                    else if($b[$j]["InputType"]==1){
                        $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                        $content.=$indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER("qIDScore_".$a[$i]["QID"], "qIDScore_".$a[$i]["QID"], "", $OtherClass='', $OtherPar=array('maxlength'=>255))."".$unit;
                    }
                    else if($b[$j]["InputType"]==2){
                        $content.=$indexVar['libappraisal_ui']->GET_DATE_PICKER("qIDDate_".$a[$i]["QID"], "", $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
                    }
                    else if($b[$j]["InputType"]==3){
                        $content.=$indexVar['libappraisal_ui']->GET_TEXTAREA($taName="qIDTxtAns_".$a[$i]["QID"], $taContents="", $taCols=70, $taRows=5, $OnFocus = '$(this).height($(this).parent().height());', $readonly = "", $other='', $class='', $taID="qIDTxtAns_".$a[$i]["QID"], $CommentMaxLength='');
                    }
                    $content.="<input type=\"hidden\" id=\"MarkID_".$a[$i]["QID"]."\" name=\"MarkID_".$a[$i]["QID"]."\" value=\"".$c[0]["MarkID"]."\">";
                    $content.="</td>";
                }
                $content.="</tr>";
                if($hasRemark==1){
                    $content.="<tr>";
                    $content.="<td colspan=\"".sizeof($b)."\">";
                    if($rmkLabel!=""){
                        $content.= $rmkLabel."<br/>";
                    }
                    $content.=$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td>";
                    $content.="</tr>";
                }
            }
        }
        else if($quesDispMode==0){
            for($i=0;$i<sizeof($a);$i++){
                $intSecID=$a[$i]["SecID"];
                $intQusCodID=$i+1;
                $qID=$a[$i]["QID"];
                $sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,TxtAns,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns
						FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND BatchID='".$batchID."' AND QID='".$qID."';";
                //echo $sql."<br/><br/>";;
                $c=$this->returnResultSet($sql);
                if(strstr($a[$i]["Descr"], '<br')){
                    $a[$i]["Descr"] = preg_replace( "/\r|\n/", "", $a[$i]["Descr"]);
                }else{
                    $a[$i]["Descr"] = preg_replace( "/\r|\n/", "", nl2br($a[$i]["Descr"]));
                }
                $a[$i]["Descr"] = "<span style='line-height:200%;font-weight:bold;'>".$a[$i]["Descr"]."</span>";
                if($a[$i]["InputType"]==0){
                    $content.="<tr>";
                    $content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td style=\"border:none;width:95%\">".(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                    else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                }
                else if($a[$i]["InputType"]==1){
                    $sql="SELECT QAnsID,QID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,".$indexVar['libappraisal']->getLangSQL("QAnsChi","QAnsEng")." as QAns,DisplayOrder
						FROM INTRANET_PA_S_QANS WHERE QID='".$a[$i]["QID"]."' ORDER BY DisplayOrder;";
                    //echo $sql."<br/><br/>";
                    $b=$this->returnResultSet($sql);
                    $content.="<tr>";
                    $content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td style=\"border:none;width:95%\">".(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    for($j=0;$j<sizeof($b);$j++){
                        $content.="<tr><td style=\"border:none\"></td>";
                        $content.="<td style=\"border:none\">";
                        $content.=$indexVar['libappraisal_ui']->Get_Radio_Button("qID_".$a[$i]["QID"].$j, "qID_".$a[$i]["QID"], $Value=$b[$j]["QAnsID"], $isChecked=($c[0]["QAnsID"]==$b[$j]["QAnsID"])?1:0, $Class="", $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=0,$isIndented=1, $specialLabel=$b[$j]["QAns"]);
                        //$content.="\t <label for='"."qID_".$a[$i]["QID"].$j."'>".$b[$j]["Descr"]."</label>";
                        $content.="</td>";
                        $content.="</tr>";
                    }
                    if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                    else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                }
                else if($a[$i]["InputType"]==2){
                    $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                    $content.="<tr>";
                    $content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td style=\"border:none;width:95%\">".(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTBOX("qIDTxtAns_".$a[$i]["QID"], "qIDTxtAns_".$a[$i]["QID"], $c[0]["TxtAns"], $OtherClass='textboxUnit', $OtherPar=array('maxlength'=>255))."".$unit."</td>";;
                    $content.="</tr>";
                    if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                    else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                }
                else if($a[$i]["InputType"]==3){
                    $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                    $content.="<tr>";
                    $content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td style=\"border:none;width:95%\">".(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER("qIDScore_".$a[$i]["QID"], "qIDScore_".$a[$i]["QID"], $c[0]["Score"], $OtherClass='', $OtherPar=array('maxlength'=>255))."".$unit."</td></tr>";
                    if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                    else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                }
                else if($a[$i]["InputType"]==4){
                    $content.="<tr>";
                    $content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td style=\"border:none;width:95%\">".(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_DATE_PICKER("qIDDate_".$a[$i]["QID"], $c[0]["DateAns"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('AppPrdFr')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum")."</td></tr>";
                    if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                    else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                }
                else if($a[$i]["InputType"]==5){
                    $content.="<tr>";
                    $content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td style=\"border:none;width:95%\">".(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                }
                else if($a[$i]["InputType"]==6){
                    $sql="SELECT QAnsID,QID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,".$indexVar['libappraisal']->getLangSQL("QAnsChi","QAnsEng")." as QAns,DisplayOrder
						FROM INTRANET_PA_S_QANS WHERE QID='".$a[$i]["QID"]."' ORDER BY DisplayOrder;";
                    //echo $sql."<br/><br/>";
                    $b=$this->returnResultSet($sql);
                    $content.="<tr>";
                    $content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td style=\"border:none;width:95%\">".(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    for($j=0;$j<sizeof($b);$j++){
                        $content.="<tr><td style=\"border:none\"></td>";
                        $content.="<td style=\"border:none\">";
                        $content .= $indexVar['libappraisal_ui']->Get_Checkbox("qID_".$a[$i]["QID"].$j, "qID_".$a[$i]["QID"]."[]", $Value=$b[$j]["QAnsID"], $isChecked=($c[0]["QAnsID"]==$b[$j]["QAnsID"])?1:0, $Class='', $Display=$b[$j]["Descr"], $Onclick='', $Disabled='',$isIndented=1,$specialLabel=$b[$j]["QAns"]);
                        $content.="</td>";
                        $content.="</tr>";
                    }
                    if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                    else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                }
                else if($a[$i]["InputType"]==7){
                    $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                    
                    $content.="<tr>";
                    $content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td style=\"border:none;width:95%\">";
                    $content.=(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"]);
                    $content.="</td>";
                    $content.="</tr>";
                    $content.="<tr><td></td>";
                    $content.="<td>";
                    //content.="<input type=\"file\" name=\"qID_".$a[$i]["QID"]."\" id=\"qID_".$a[$i]["QID"]."\" class=\"file ".$isRequiredField."\">";
                    $content.="<input type=\"file\" name=\"qID_".$a[$i]["QID"]."\" id=\"qID_".$a[$i]["QID"]."\" class=\"file\"><br/>";
                    
                    $content.="</td>";
                    $content.="</tr>";
                    
                    if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                    else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                    }
                }
                $content .= "<tr><td colspan='2' style='height:10px;'></td></tr>";
            }
        }
        return $content;
    }
    
    function getSdfFormPrint($connection,$templateID,$userID,$recordID,$cycleID,$rlsNo,$batchID, $isPrint, $isCombinePrint){
        $indexVar['libappraisal'] = new libappraisal();
        $indexVar['libappraisal_ui'] = new libappraisal_ui();
        global $intranet_session_language, $Lang, $appraisalConfig, $sys_custom, $PATH_WRT_ROOT;
        
        if($userID==""&&$recordID==""&&$cycleID==""&&$rlsNo==""&&$batchID==""){
            $sql = "SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormTitle,"
                .$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode,"
                    .$indexVar['libappraisal']->getLangSQL("HdrRefChi","HdrRefEng")." as HdrRef,"
                        .$indexVar['libappraisal']->getLangSQL("ObjChi","ObjEng")." as Obj,"
                            .$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,
				AppTgtChi,AppTgtEng,NeedSubj,NeedClass,NeedSubjLang
				FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".IntegerSafe($templateID)."';";
        }
        else {
            $sql = "SELECT iptf.RecordID,UserID,iptf.CycleID,CycleDescr,iptf.TemplateID,FormTitle,FormCode,HdrRef,Obj,Descr,AppRoleID,AppTgtChi,AppTgtEng,AcademicYearID,COALESCE(SubDate,'') as SubDate,
		EditPrdFr,EditPrdTo,CASE WHEN NOW()>EditPrdTo THEN '1' ELSE '0' END as OverdueDate,YearClassID,SubjectID,TeachLang,NeedSubj,NeedClass,FillerID,CycleStart,CycleClose,ModDateFr,ModDateTo,AprID,ObsID,iptfrmSub.ModifiedBy_UserID,NeedSubjLang,iptf.GrpID
		FROM(
			SELECT RecordID,CycleID,TemplateID,UserID,AprID,GrpID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".IntegerSafe($recordID)."'
		) as iptf
		INNER JOIN(
			SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormTitle,"
			    .$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode,"
			        .$indexVar['libappraisal']->getLangSQL("HdrRefChi","HdrRefEng")." as HdrRef,"
			            .$indexVar['libappraisal']->getLangSQL("ObjChi","ObjEng")." as Obj,"
			                .$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,
			AppTgtChi,AppTgtEng,NeedSubj,NeedClass,NeedSubjLang
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf ON iptf.TemplateID=ipsf.TemplateID
		INNER JOIN(
			SELECT CycleID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as CycleDescr,AcademicYearID,DATE_FORMAT(CycleStart,'%Y-%m-%d') as CycleStart,DATE_FORMAT(CycleClose,'%Y-%m-%d') as CycleClose
			FROM INTRANET_PA_C_CYCLE
		) as ipcc ON iptf.CycleID=ipcc.CycleID
		INNER JOIN(
			SELECT RlsNo,RecordID,BatchID,SubDate,FillerID,ObsID,DATE_FORMAT(ModDateFr,'%Y-%m-%d') as ModDateFr,DATE_FORMAT(ModDateTo,'%Y-%m-%d') as ModDateTo,
					DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,DATE_FORMAT(EditPrdTo,'%Y-%m-%d') as EditPrdTo,AppRoleID,ModifiedBy_UserID
					FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".IntegerSafe($rlsNo). "' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
		) as iptfrmSub ON iptf.RecordID=iptfrmSub.RecordID
		LEFT JOIN(
			SELECT RlsNo,RecordID,BatchID,YearClassID,SubjectID,TeachLang FROM INTRANET_PA_T_SLF_HDR WHERE RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."'
		) as iptsh ON iptf.RecordID=iptsh.RecordID AND iptfrmSub.RlsNo=iptsh.RlsNo AND iptfrmSub.BatchID=iptsh.BatchID;";
			                //echo $sql."<br/>";
        }
        $header=$connection->returnResultSet($sql);
        $templateID=$header[0]["TemplateID"];
        $academicYearID=$header[0]["AcademicYearID"];
        /*$cycle=$header[0]["Cycle"];
         $userID = $header[0]["UserID"];
         $canEdit=($header[0]["OverdueDate"]=="0"&&$header[0]["SubDate"]=="")?"1":"0";
         $academicYearID=$header[0]["AcademicYearID"];
         $appRole=Get_Lang_Selection($header[0]["AppTgtChi"],$header[0]["AppTgtEng"]);
         */
        
        //======================================================================== Section ========================================================================//
        $sql="SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,PageBreakAfter
		FROM(
			SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
			$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder,PageBreakAfter FROM INTRANET_PA_S_FRMSEC WHERE TemplateID='".IntegerSafe($templateID)."' AND ParentSecID IS NULL
		) as ipsf ORDER BY DisplayOrder;
		";
			//echo $sql."<br/><br/>";
			$sectionHeader=$connection->returnResultSet($sql);
			
			// prepare the QID for that form
			$parSecID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($sectionHeader,"SecID");
			$sql="SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,ParentSecID
		FROM(
			SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
			$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder,ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID='".IntegerSafe($templateID)."' AND ParentSecID IS NOT NULL
			AND ParentSecID IN (".$parSecID.")
		) as ipsf";
			//echo $sql."<br/><br/>";
			$y=$connection->returnResultSet($sql);
			$subSecID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"SecID");
			$sql="SELECT QCatID,SecID,".$indexVar['libappraisal']->getLangSQL("QCatCodChi","QCatCodEng")." as QCatCod,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as QCatDescr,".
			 			"DisplayOrder as QCatDisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID IN (".$subSecID.")";
			//echo $sql."<br/><br/>";
			$y=$connection->returnResultSet($sql);
			$qCatID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"QCatID");
			$sql="SELECT QID,QCatID FROM INTRANET_PA_S_QUES WHERE QCatID IN (".$qCatID.")";
			$y=$connection->returnResultSet($sql);
			$qID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"QID");
			// prepare the QID for that form
			
			//======================================================================== Personal Master ========================================================================//
			if($sys_custom['eAppraisal']['LTMPSPrintingStyle']){
			    $x = "";
			}else{
			    $x .= "<div class=\"formTitle\">";
			    $x .= $header[0]["FormTitle"];
			    $sql = "Select CycleClose From INTRANET_PA_C_CYCLE WHERE CycleID='".$cycleID."'";
			    $c=$connection->returnResultSet($sql);
			    if(strtotime($c[0]['CycleClose']) > time()){
			        $x .= " ".$Lang['Appraisal']['Report']['ReportBeforeCycleEnd'];
			    }
			    $x .= "</div>"."\r\n";
			}
			$x .= "<div>";
			if($header[0]["FormCode"]!=""&&$header[0]["Obj"]!=""){
			    $x .= "<span class=\"formObjective\" style=\"float: left;\">".$header[0]["FormCode"]." - ".$header[0]["Obj"]."</span>";
			}
			else if($header[0]["FormCode"]!=""&&$header[0]["Obj"]==""){
			    $x .= "<span class=\"formObjective\" style=\"float: left;\">".$header[0]["FormCode"]."</span>";
			}
			else if($header[0]["FormCode"]==""&&$header[0]["Obj"]!=""){
			    $x .= "<span class=\"formObjective\" style=\"float: left;\">".$header[0]["Obj"]."</span>";
			}
			else if($header[0]["FormCode"]==""&&$header[0]["Obj"]==""){
			    $x .= "<span class=\"formObjective\" style=\"float: left;\"></span>";
			}
			if($header[0]["Descr"]!=""){
			    $x .= "<br/><br/><span>".$header[0]["Descr"]."</span>";
			}
			$x .= "<span class=\"formRefHeader\" style=\"float: right;\">".$header[0]["HdrRef"]."</span>";
			$x .="</div>"."\r\n";
			$x .= "<div>".$header[0]["Descr"]."</div>".($header[0]["Descr"]!="")?"<br/>":""."\r\n";
			
			$sql = "SELECT UserID,EnglishName,ChineseName,Gender
					FROM(
						SELECT UserID,EnglishName,ChineseName,Gender FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".IntegerSafe($header[0]['UserID'])."'
					) as iu
					";
			$iu=$connection->returnResultSet($sql);
			if($sys_custom['eAppraisal']['LTMPSPrintingStyle']){
			    $iu[0]["EnglishName"] = preg_replace('/(\w+)*(\d+ )/m', '', $iu[0]["EnglishName"]);
			    $iu[0]["ChineseName"] = preg_replace('/(\w+)*(\d+ )/m', '', $iu[0]["ChineseName"]);
			}
			if(!$sys_custom['eAppraisal']['LTMPSPrintingStyle']){
			    $tgtName = ($_SESSION['intranet_session_language']=="en")?$Lang['Appraisal']['ARFormTgtName']." ".$header[0]["AppTgtEng"]:$header[0]["AppTgtChi"].$Lang['Appraisal']['ARFormTgtName'];
			    $x .= "<table class=\"form_table_v30\">";
			    if($iu[0]["ChineseName"]==""){
			        $x .= "<tr><td class=\"field_title\">".$tgtName."</td><td colspan=\"3\">".$iu[0]["EnglishName"]."</td></tr>"."\r\n";
			    }else{
			        $x .= "<tr><td class=\"field_title\">".$tgtName."</td><td colspan=\"3\">".$iu[0]["EnglishName"]."(".$iu[0]["ChineseName"].")"."</td></tr>"."\r\n";
			    }
			}
			if($header[0]["NeedSubj"]=="1"){
			    $subjectSQL=$indexVar['libappraisal']->getAllSubject();
			    for($i=0;$i<sizeof($subjectSQL);$i++){
			        if($subjectSQL[$i]["SubjectID"]==$header[0]["SubjectID"]){
			            $subj = Get_Lang_Selection($subjectSQL[$i]["SubjectTitleB5"],$subjectSQL[$i]["SubjectTitleEN"]);
			        }
			    }
			    $x .="<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormSubject']."</td><td>".$subj."</td>";
			    if($header[0]["NeedSubjLang"]=="1"){
			        $x .="<td class=\"field_title\">".$Lang['Appraisal']['ARFormSubjectMedium']."</td><td>".$header[0]["TeachLang"]."</td>";
			    }
			    $x .= "</tr>";
			}
			if($header[0]["NeedClass"]=="1"){
			    $yearClassSQL=$indexVar['libappraisal']->getAllYearClass($academicYearID);
			    for($i=0;$i<sizeof($yearClassSQL);$i++){
			        if($yearClassSQL[$i]["YearClassID"]==$header[0]["YearClassID"]){
			            $yearClass = Get_Lang_Selection($yearClassSQL[$i]["ClassTitleB5"],$yearClassSQL[$i]["ClassTitleEN"]);
			        }
			    }
			    $x .="<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormYearClass']."</td><td colspan=\"3\">".$yearClass."</td></tr>";;
			}
			//$x .= "</table>".($header[0]["NeedClass"]!="0" || $header[0]["NeedClass"]!="0")?"<br/>":"\r\n";;
			if($header[0]["NeedClass"]!="0" || $header[0]["NeedClass"]!="0"){
			    $appTag = "<br/>";
			}
			else{
			    $appTag = "\r\n";
			}
			$x .= "</table>".$appTag;
			
			for($h=0;$h<sizeof($sectionHeader);$h++){
			    $x .= "<div id=\"SecID_".$sectionHeader[$h]["SecID"]."\" name=\"SecID_".$sectionHeader[$h]["SecID"]."\">";
			    if($sectionHeader[$h]["SecCod"]!="" && $sectionHeader[$h]["SecTitle"]!=""){
			        $x .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($sectionHeader[$h]["SecCod"]."-".$sectionHeader[$h]["SecTitle"])."</b><br/>\r\n";
			    }
			    else if($sectionHeader[$h]["SecCod"]!="" && $sectionHeader[$h]["SecTitle"]==""){
			        $x .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($sectionHeader[$h]["SecCod"])."</b><br/>\r\n";
			    }
			    else if($sectionHeader[$h]["SecCod"]=="" && $sectionHeader[$h]["SecTitle"]!=""){
			        $x .= "<b>".$indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($sectionHeader[$h]["SecTitle"])."</b><br/>\r\n";
			    }
			    /*$x .= "<table>";
			     $x .= "<tr><td>".$sectionHeader[$h]["SecDescr"]."</td></tr></table>".(($sectionHeader[$h]["SecDescr"]!="")?"<br/>":"")."\r\n";
			     */
			    $x .= "</div><br/>\r\n";
			    $x .= nl2br($sectionHeader[$h]["SecDescr"]).(($sectionHeader[$h]["SecDescr"]!="")?"<br/>":""."\r\n");
			    //======================================================================== Sub Section ========================================================================//
			    $sql="SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,ParentSecID
						FROM(
							SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
							$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder,ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID='".IntegerSafe($templateID)."' AND ParentSecID IS NOT NULL
							AND ParentSecID='".IntegerSafe($sectionHeader[$h]["SecID"])."'
						) as ipsf
						ORDER BY DisplayOrder;
						";
							//echo $sql."<br/><br/>";
							$a=$connection->returnResultSet($sql);
							
							if($sys_custom['eAppraisal']['WusichongStyle']==true){
							    $_SESSION['totalFormF5Score'] = 0;
							}
							for($i=0;$i<sizeof($a);$i++){
							    if($sys_custom['eAppraisal']['LTMPSPrintingStyle']){
							        $spacer = '<p class="spacer"></p>'."\n";
							        if($a[$i]["SecCod"]!="" && $a[$i]["SecTitle"]!=""){
							            $x .= str_replace($spacer,"",$indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecCod"]."-".$a[$i]["SecTitle"]))."\r\n";
							        }
							        else if($a[$i]["SecCod"]!="" && $a[$i]["SecTitle"]==""){
							            $x .= str_replace($spacer,"",$indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecCod"]))."\r\n";
							        }
							        else if($a[$i]["SecCod"]=="" && $a[$i]["SecTitle"]!=""){
							            $x .= str_replace($spacer,"",$indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecTitle"]))."\r\n";
							        }
							    }else{
							        if($a[$i]["SecCod"]!="" && $a[$i]["SecTitle"]!=""){
							            $x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecCod"]."-".$a[$i]["SecTitle"])."\r\n";
							        }
							        else if($a[$i]["SecCod"]!="" && $a[$i]["SecTitle"]==""){
							            $x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecCod"])."\r\n";
							        }
							        else if($a[$i]["SecCod"]=="" && $a[$i]["SecTitle"]!=""){
							            $x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecTitle"])."\r\n";
							        }
							    }
							    if($sys_custom['eAppraisal']['LTMPSPrintingStyle']){
							        $x .= "<table class=\"SecDescr\">";
							        $x .= "<tr><td>".(($a[$i]["SecDescr"]!="")? $a[$i]["SecDescr"]:""."\r\n")."</td></tr></table>";
							    }else{
							        $x .= "<table>";
							        $x .= "<tr><td>".(($a[$i]["SecDescr"]!="")? $a[$i]["SecDescr"]:""."\r\n")."</td></tr></table>".(($a[$i]["SecDescr"]!="")?"<br/>":""."\r\n");
							    }
							    
							    $sql="SELECT QCatID,SecID,".$indexVar['libappraisal']->getLangSQL("QCatCodChi","QCatCodEng")." as QCatCod,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as QCatDescr,QuesDispMode,".
											    "DisplayOrder as QCatDisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID='".$a[$i]["SecID"]."' ORDER BY DisplayOrder;";
							    //echo $sql."<br/><br/>";
							    $b=$connection->returnResultSet($sql);
							    
							    for($j=0;$j<sizeof($b);$j++){
							        $x .= "<div class=\"questionBundle\">";
							        $x .= "<table class=\"form_ques_gp_title\"><tr><td valign=\"top\">".$b[$j]["QCatCod"]."</td><td>".nl2br($b[$j]["QCatDescr"])."</td></tr></table>";
							        //$x .= $b[$j]["QCatCod"].nl2br($b[$j]["QCatDescr"]);
							        //======================================================================== Question Category ========================================================================//
							        if($b[$j]["QuesDispMode"]=="1"||$b[$j]["QuesDispMode"]=="2"){
							            $x .= "<div style=\"padding-left:20px;\"><table class=\"common_table_list\">";
							        }
							        else{
							            $x .= "<div style=\"padding-left:20px;\"><table>";
							        }
							        $x .= $this->getSDFSectionContentPrint("",$a[$i]["SecID"],$b[$j]["QCatID"],$recordID,$rlsNo,$batchID,$isCombinePrint);
							        $x .= "</table></div>";
							        if(($b[$j]['QCatID']=='293' || $b[$j]['QCatID']=='299') && $sys_custom['eAppraisal']['LTMPSPrintingStyle']){
							            $x .= "<div class=\"extraRmk\" style=\"padding-left:20px;\">註3：守時評級: (遲到次數) &nbsp;";
							            /*
							             $x .= "<table class=\"common_table_list\">";
							             $x .= "<thead><tr><th></th><th>優異</th><th>良好</th><th>滿意</th><th>尚可</th><th>宜多關注</th></tr></thead>";
							             $x .= "<tbody><tr><td>遲到次數</td><td>0次</td><td>1-4次</td><td>5-8次</td><td>9-12次</td><td>13次或以上</td></tr></tbody>";
							             $x .= "</table></div><br/><br/>\r\n";
							             */
							            $x .= "<b>優異</b>: 0次 &nbsp;&nbsp;";
							            $x .= "<b>良好</b>: 1-4次&nbsp;&nbsp;";
							            $x .= "<b>滿意</b>: 5-8次 &nbsp;&nbsp;";
							            $x .= "<b>尚可</b>: 9-12次 &nbsp;&nbsp;";
							            $x .= "<b>宜多關注</b>: 13次或以上 &nbsp;&nbsp;</div><br/><br/>\r\n";
							        }
							        if(!$sys_custom['eAppraisal']['LTMPSPrintingStyle']){
							            $x .= "<br/>\r\n";
							        }
							        $x .= "</div>";
							    }
							    if($sys_custom['eAppraisal']['WusichongStyle']==true){
							        $inF5 = false;
							        $sql = "SELECT TemplateType FROM INTRANET_PA_S_FRMTPL WHERE TemplateID = '".$templateID."' AND FrmTitleEng LIKE 'F5%' AND IsActive=1";
							        $temp=$this->returnResultSet($sql);
							        if(!empty($temp)){
							            $inF5 = true;
							        }
							        $sql = "SELECT DisplayOrder FROM INTRANET_PA_S_FRMSEC WHERE SecID = '".$a[$i]["ParentSecID"]."' AND ParentSecID IS NULL";
							        $orderSecTerm= current($this->returnVector($sql));
							        if($orderSecTerm == 1 && $i == sizeof($a)-1 && $inF5==true){
							            include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
							            $libacc = new libaccountmgmt();
							            
							            // Get form special identity list
							            $sql = "Select cycle.AcademicYearID From INTRANET_PA_C_CYCLE cycle
											INNER JOIN INTRANET_PA_T_FRMSUM sum ON cycle.CycleID = sum.CycleID
											WHERE sum.RecordID='".$recordID."'";
							            $sectionAcademicYear = current($this->returnVector($sql));
							            $specialIdentityList = $libacc->getSpecialIdentityMapping($sectionAcademicYear,true);
							            if($isCombinePrint==true && (in_array($_SESSION['UserID'],$specialIdentityList[1]) || !$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"])){
							                $f5FinalMark = round($_SESSION['totalFormF5Score']/1.5);
							                $x .= "<table class=\"form_table_v30\"><tr><td class=\"field_title\">".$Lang['Appraisal']['LNT']['Score'].":</td><td colspan=\"3\">".$f5FinalMark."</td></tr></table>";
							            }
							        }
							    }
							}
							$x .="</div>";
							if($sectionHeader[$h]['SecID']=="137" && $sys_custom['eAppraisal']['LTMPSPrintingStyle']){
							    $grpSql = "SELECT ChineseName, EnglishName, u.UserID FROM ".$appraisalConfig['INTRANET_USER']." u INNER JOIN INTRANET_PA_C_GRPMEM gm ON u.UserID=gm.UserID WHERE gm.IsLeader=1 AND gm.GrpID='".$header[0]["GrpID"]."'";
							    $leaderList=$this->returnResultSet($grpSql);
							    $subDateSql = "SELECT SubDate FROM INTRANET_PA_T_FRMSUB sub INNER JOIN INTRANET_PA_T_FRMSUM  sum ON sub.RecordID=sum.RecordID
											WHERE sum.CycleID='".$cycleID."' AND sub.BatchID='".$batchID."' AND sum.GrpID='".$header[0]["GrpID"]."' AND sum.UserID='".$userID."'";
							    $subDate = current($this->returnVector($subDateSql));
							    $x .= "<br><table class=\"common_table_list signature\">";
							    $x .= "<tr><td style=\"border-right-width: thick; width:20%;\">評估人姓名（正楷）</td>";
							    foreach($leaderList as $leader){
							        $leaderName = $leader['EnglishName'];
							        if($leader['ChineseName']!=""){
							            $leaderName  = $leaderName." (".$leader['ChineseName'].")";
							        }
							        $leaderName = preg_replace('/(\w+)*(\d+ )/m','',$leaderName);
							        $x .= "<td>".$leaderName."</td>";
							    }
							    $x .= "</tr>";
							    $x .= "<tr><td style=\"border-right-width: thick; width:20%;\">職位</td>";
							    foreach($leaderList as $leader){
							        $leaderPostSql = "SELECT IFNULL(JobGrade, ' ') as JobGrade FROM INTRANET_PA_T_PRE_GEN pg INNER JOIN INTRANET_PA_T_FRMSUM sum ON pg.RecordID=sum.RecordID WHERE sum.UserID='".$leader['UserID']."' AND sum.CycleID='".$cycleID."' ORDER BY pg.DateTimeModified DESC ";
							        $leaderPost=current($this->returnResultSet($leaderPostSql));
							        $x .= "<td>".$leaderPost['JobGrade']."</td>";
							    }
							    $x .= "</tr>";
							    //							$x .= "<tr><td style=\"border-right-width: thick; width:20%;\">簽署</td>";
							    //							foreach($leaderList as $leader){
							    //								$x .= "<td></td>";
							    //							}
							    //							$x .= "</tr>";
							    $x .= "<tr><td style=\"border-right-width: thick; width:20%;\">確認日期</td>";
							    foreach($leaderList as $leader){
							        //$x .= "<td>".date('Y-m-d',strtotime($subDate))."</td>";
							        $x .= "<td>".date('Y',strtotime($subDate))."-07-03</td>";
							    }
							    $x .= "</tr>";
							    $x .= "</table><br>";
							}
							if($sectionHeader[$h]['SecID']=="140" && $sys_custom['eAppraisal']['LTMPSPrintingStyle']){
							    $userSql = "SELECT ChineseName, EnglishName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$userID."'";
							    $userData=current($this->returnResultSet($userSql));
							    if($userData['ChineseName']==""){
							        $userName  = $userData['EnglishName'];
							    }else{
							        $userName  = $userData['ChineseName'];
							    }
							    $userName = preg_replace('/(\w+)*(\d+ )/m','',$userName);
							    
							    $x .= "<br><table class=\"common_table_list simpleSignature\">";
							    $x .= "<tr>";
							    // 20191028 [J174358] change hardcode content
							    // 							$x .= "<td style='text-align: right'>考績會見日期：</td><td>_____________________________</td>";
							    // 							$x .= "<td style='text-align: right'>校長簽署：</td><td>_____________________________</td>";
							    $x .= "<td style='text-align: right; width: 18%'>覆核者姓名：</td><td><u>梁麗琪校長</u></td>";
							    $x .= "<td style='text-align: right'>&nbsp;</td><td>&nbsp;</td>";
							    $x .= "</tr></table>";
							    $x .= "<hr>";
							    $x .= "<div style=\"text-align: center; font-size:16px;\">確認知悉上述回應及建議</div><br><br>";
							    $x .= "<table class=\"common_table_list simpleSignature\">";
							    $x .= "<tr>";
							    $x .= "<td style='text-align: right'>教師姓名：</td><td><u>".$userName."</u></td>";
							    // 20191028 [J174358] change hardcode content
							    // 							$x .= "<td style='text-align: right'>教師簽署：</td><td>_____________________</td>";
							    // 							$x .= "<td style='text-align: right'>日期：</td><td>_____________________</td>";
							    $x .= "<td style='text-align: right'>&nbsp;</td><td>&nbsp;</td>";
							    $x .= "<td style='text-align: right'>確認日期：</td><td><u>2019-11-01</u></td>";
							    $x .= "</tr></table>";
							    $x .= "<hr>";
							}
							if($sectionHeader[$h]['SecID']=="142" && $sys_custom['eAppraisal']['LTMPSPrintingStyle']){
							    $x .= "<br><table class=\"common_table_list simpleSignature\">";
							    $x .= "<tr>";
							    $x .= "<td style='text-align: right'>校監簽署：</td><td>_____________________________</td>";
							    $x .= "<td style='text-align: right'>日期：</td><td>_____________________________</td>";
							    $x .= "</tr></table>";
							}
							if($sectionHeader[$h]["PageBreakAfter"]==1){
							    $x .= "<p style=\"page-break-after:always;\"></p>";
							}
			}
			return $x;
    }
    function getSDFSectionContentPrint($prefix,$secID,$qCatID,$recordID,$rlsNo,$batchID,$isCombinePrint){
        $indexVar['libappraisal'] = new libappraisal();
        $indexVar['libappraisal_ui'] = new libappraisal_ui();
        global $intranet_session_language, $Lang, $sys_custom,$PATH_WRT_ROOT,$appraisalConfig;
        
        include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
        $libacc = new libaccountmgmt();
        
        // Get form special identity list
        $sql = "Select cycle.AcademicYearID From INTRANET_PA_C_CYCLE cycle
				INNER JOIN INTRANET_PA_T_FRMSUM sum ON cycle.CycleID = sum.CycleID
				WHERE sum.RecordID='".$recordID."'";
        $sectionAcademicYear = current($this->returnVector($sql));
        $specialIdentityList = $libacc->getSpecialIdentityMapping($sectionAcademicYear,true);
        // Check if the concerning section is current or not
        $sql = "SELECT GrpID,SecID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND BatchID='".$batchID."'";
        $batchData=current($this->returnResultSet($sql));
        
        $sql = "SELECT SecID,ipsqcat.QCatID,QID,QuesDispMode,QCodDes,Descr,QuesDisplayOrder,IsMC,IsScore,HasRmk,Rmk,IsTxtAns,TxtBoxNote,InputType,RmkLabel,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,ScoreIsAvgMark,ScoreAvgDec
					FROM(
						SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,DisplayOrder as CatDisplayOrder,QuesDispMode,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec FROM INTRANET_PA_S_QCAT WHERE SecID='".$secID."' AND QCatID='".$qCatID."'
					) as ipsqcat
					LEFT JOIN(
						SELECT QID,QCatID,".$indexVar['libappraisal']->getLangSQL("QCodChi","QCodEng")." as QCodDes,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,DisplayOrder,InputType,
								IsMC,IsScore,IsTxtAns,TxtBoxNote,HasRmk,".$indexVar['libappraisal']->getLangSQL("RmkLabelChi","RmkLabelEng")." as Rmk,IsAvgMark as ScoreIsAvgMark,AvgDec as ScoreAvgDec,
						DisplayOrder as QuesDisplayOrder,".$indexVar['libappraisal']->getLangSQL("RmkLabelChi","RmkLabelEng")." as RmkLabel
						FROM INTRANET_PA_S_QUES WHERE QCatID='".$qCatID."'
					) as ipsques ON ipsqcat.QCatID=ipsques.QCatID
					ORDER BY QuesDisplayOrder;";
        $a=$this->returnResultSet($sql);
        //echo ($sql)."<br/><br/>";
        $quesDispMode=$a[0]["QuesDispMode"];
        
        // check any content in the first content
        $hasContent = false;
        $sql="SELECT SUM(LENGTH(".$indexVar['libappraisal']->getLangSQL("QCodChi","QCodEng").")) as content FROM  INTRANET_PA_S_QUES WHERE QCatID=".$qCatID.";";
        $x=$this->returnResultSet($sql);
        if($x[0]["content"] > 0){
            $hasContent = true;
        }
        
        // check if the question is submitted or not
        if($isCombinePrint==true){
            $isSubmitted = false;
            $parentSecSql = "SELECT ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE SecID='".$secID."'";
            $parentSecID = current($this->returnVector($parentSecSql));
            $sql = "SELECT SubDate FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND SecID='".$parentSecID."'";
            $SubDate = current($this->returnVector($sql));
            $sql = "SELECT AppRoleID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND SecID='".$parentSecID."'";
            $appRoleID = current($this->returnVector($sql));
            $sql = "SELECT BatchID FROM INTRANET_PA_T_FRMSUB WHERE RecordID='".$recordID."' AND SecID='".$parentSecID."' AND appRoleID='".$appRoleID."'";
            $secBatchID = $this->returnVector($sql);
            $sql = "SELECT sum.TemplateID,FrmTitleChi,FrmTitleEng,sum.UserID FROM INTRANET_PA_S_FRMTPL tpl
					INNER JOIN INTRANET_PA_T_FRMSUM sum ON tpl.TemplateID=sum.TemplateID
					WHERE sum.RecordID='".$recordID."'";
            $templateData = current($this->returnArray($sql));
            $templateID = $templateData['TemplateID'];
            if($sys_custom['eAppraisal']['WusichongStyle']==true){
                $inF3 = false;
                $inF5 = false;
                $sql = "SELECT TemplateType FROM INTRANET_PA_S_FRMTPL WHERE TemplateID = '".$templateID."' AND FrmTitleEng LIKE 'F3%' AND IsActive=1";
                $temp=$this->returnResultSet($sql);
                if(!empty($temp)){
                    $inF3 = true;
                }
                $sql = "SELECT TemplateType FROM INTRANET_PA_S_FRMTPL WHERE TemplateID = '".$templateID."' AND FrmTitleEng LIKE 'F5%' AND IsActive=1";
                $temp=$this->returnResultSet($sql);
                if(!empty($temp)){
                    $inF5 = true;
                }
            }
            
            if(!empty($SubDate) || in_array($batchID,$secBatchID)){
                $isSubmitted = true;
                $sql = "SELECT UserID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".$recordID."'";
                $appraiseeID = current($this->returnVector($sql));
                
                $sql = "SELECT FillerID FROM INTRANET_PA_T_FRMSUB WHERE RecordID='".$recordID."' AND SecID='".$parentSecID."'";
                $fillerID = current($this->returnVector($sql));
                if($appraiseeID!=$fillerID){
                    // Check if there are section collaboratively contributing the same section (if not the current sec)
                    $sql = "SELECT DISTINCT frmsum.RecordID FROM INTRANET_PA_T_FRMSUB frmsub
							INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON frmsub.RecordID = frmsum.RecordID
							WHERE frmsub.GrpID='".$batchData['GrpID']."' AND frmsub.IsActive=1 AND frmsub.RecordID <> '".$recordID."' AND frmsum.UserID='".$templateData['UserID']."'";
                    $collaborativeForm = $this->returnVector($sql);
                    if(!in_array($recordID, $collaborativeForm)){
                        $collaborativeForm[] = $recordID;
                    }
                    // Check if the section from different record is filled by the same person
                    $collFillerList = array();
                    foreach($collaborativeForm as $idx=>$collRecordID){
                        $sql = "SELECT FillerID FROM INTRANET_PA_T_FRMSUB WHERE RecordID='".$collRecordID."' AND SecID='".$parentSecID."'";
                        $colFillerID = current($this->returnVector($sql));
                        if(!in_array($colFillerID,$collFillerList)){
                            $collFillerList[] = $colFillerID;
                        }
                    }
                    if(!in_array($fillerID,$collFillerList)){
                        $collFillerList[] = $fillerID;
                    }
                    if(count($collFillerList) <= 1){
                        unset($collaborativeForm);
                    }
                    if(!empty($collaborativeForm)){
                        $isCollaborate = true;
                    }
                }
            }
        }
        if($quesDispMode==1){
            if($a[0]["IsAvgMark"]==0){
                $sql = "SELECT MarkID,QCatID,".$indexVar['libappraisal']->getLangSQL("MarkCodChi","MarkCodEng")." as MarkCodDes,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,Score,DisplayOrder,".
                    $indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr
						FROM INTRANET_PA_S_QLKS WHERE QCatID='".$qCatID."'
						ORDER BY DisplayOrder";
                    //echo $sql."<br/><br/>";
                    $b=$this->returnResultSet($sql);
                    $descWidth ="20%";
                    $content.="<thead><tr>";
                    if($hasContent == true){
                        $content.="<th></th>";
                    }
                    $content.="<th style=\"width:35%\"></th>";
                    $dnmtr = (sizeof($b)==0)?1:sizeof($b);
                    $thWidth=60/$dnmtr;
                    for($j=0;$j<sizeof($b);$j++){
                        $content.="<th>".nl2br($b[$j]["Descr"])."</th>";
                    }
                    $content.="</tr></thead>";
                    for($i=0;$i<sizeof($a);$i++){
                        $intSecID=$a[$i]["SecID"];
                        $intQusCodID=$i+1;
                        $qID=$a[$i]["QID"];
                        //$sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND QID=".$qID.";";
                        $sql="SELECT frmdtl.RecordID,frmdtl.BatchID,frmdtl.QID,frmdtl.QAnsID,frmdtl.MarkID,frmdtl.Remark,frmdtl.Score,DATE_FORMAT(frmdtl.DateAns,'%Y-%m-%d') as DateAns
						FROM INTRANET_PA_T_SLF_DTL frmdtl
						INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmdtl.RlsNo = frmsub.RlsNo AND frmdtl.RecordID = frmsub.RecordID AND frmdtl.BatchID=frmsub.BatchID
						WHERE frmdtl.RlsNo='".$rlsNo."' AND frmdtl.RecordID='".$recordID."' AND frmdtl.QID='".$qID."' AND (frmsub.SubDate IS NOT NULL OR frmsub.EditPrdTo < NOW());";
                        //echo $sql."<br/><br/>";;
                        $c=$this->returnResultSet($sql);
                        $c = $indexVar['libappraisal']->getQuestionDataByCmpArr($c);
                        
                        if($isCombinePrint==true){
                            $collaborateAns = array();
                            $collaborateRmk = array();
                            if(!empty($collaborativeForm)){
                                foreach($collaborativeForm as $cRecord){
                                    if($sys_custom['eAppraisal']['WusichongStyle']==true){
                                        $sql="SELECT dtl.RecordID,dtl.BatchID,dtl.QID,dtl.QAnsID,dtl.MarkID,dtl.Remark,qlks.Score,DATE_FORMAT(dtl.DateAns,'%Y-%m-%d') FROM INTRANET_PA_T_SLF_DTL dtl INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON dtl.RlsNo = frmsub.RlsNo AND dtl.RecordID = frmsub.RecordID AND dtl.BatchID=frmsub.BatchID LEFT JOIN INTRANET_PA_S_QLKS qlks ON dtl.MarkID=qlks.MarkID WHERE dtl.RlsNo='".$rlsNo."' AND dtl.RecordID='".$cRecord."' AND dtl.QID='".$qID."' AND (frmsub.SubDate IS NOT NULL OR frmsub.EditPrdTo < NOW());";
                                        //echo $sql."<br/><br/>";
                                    }else{
                                        //$sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,DATE_FORMAT(DateAns,'%Y-%m-%d') FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo='".$rlsNo."' AND RecordID='".$cRecord."' AND QID='".$qID."';";
                                        $sql="SELECT frmdtl.RecordID,frmdtl.BatchID,frmdtl.QID,frmdtl.QAnsID,frmdtl.MarkID,frmdtl.Remark,frmdtl.Score,DATE_FORMAT(frmdtl.DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL frmdtl
										INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmdtl.RlsNo = frmsub.RlsNo AND frmdtl.RecordID = frmsub.RecordID AND frmdtl.BatchID=frmsub.BatchID
										WHERE frmdtl.RlsNo='".$rlsNo."' AND frmdtl.RecordID='".$cRecord."' AND frmdtl.QID='".$qID."' AND (frmsub.SubDate IS NOT NULL OR frmsub.EditPrdTo < NOW());";
                                        //echo $sql."<br/><br/>";
                                    }
                                    //echo $sql."<br/><br/>";
                                    $recordResult=$this->returnResultSet($sql);
                                    if(!empty($recordResult)){
                                        $collaborateAns[$cRecord] = $indexVar['libappraisal']->getQuestionDataByCmpArr($recordResult);
                                        $collaborateRmk[$cRecord] = array('Remark'=>$recordResult[0]['Remark'],'FillerName'=>'');
                                    }
                                }
                                if(!empty($collaborateAns)){
                                    $collaborateAns[$recordID] = $c;
                                    
                                    foreach($collaborateAns as $rID=>$ansList){
                                        foreach($ansList as $index=>$ansDetail){
                                            $sql = "SELECT ".$indexVar['libappraisal']->Get_Name_Field("u.")." as FillerName, u.UserID FROM INTRANET_PA_T_FRMSUB frmsub
												INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON frmsub.FillerID=u.UserID
												WHERE RecordID='".$ansDetail['RecordID']."' AND BatchID='".$ansDetail['BatchID']."'";
                                            $filler = current($this->returnArray($sql));
                                            $fillerName = $filler['FillerName'];
                                            $fillerID = $filler['UserID'];
                                            $collaborateAns[$rID][$index]['FillerName'] = $fillerName;
                                            $collaborateRmk[$rID]['FillerName'] = $fillerName;
                                            $collaborateAns[$rID][$index]['FillerID'] = $fillerID;
                                            $collaborateRmk[$rID]['FillerID'] = $fillerID;
                                        }
                                    }
                                }
                            }
                        }
                        
                        $hasRemark=$a[$i]["HasRmk"];
                        $rmkLabel=$a[$i]["RmkLabel"];
                        $rowSpan=($hasRemark==1 && $c[0]["Remark"]!="")?"2":"1";
                        
                        $content.="<tr>";
                        if($hasContent == true){
                            $content.="<td valign=\"top\" rowspan=\"".$rowSpan."\">".$a[$i]["QCodDes"]."</td>";
                        }
                        $content.="<td rowspan=\"".$rowSpan."\">".nl2br($a[$i]["Descr"])."</td>";
                        if($sys_custom['eAppraisal']['WusichongStyle']==true && ($inF5 || $inF3) && !empty($collaborateAns)){
                            $totalScore = 0;
                            $totalPrincipalScore = 0;
                            $totalNonPrincipal = 0;
                            for($j=0;$j<sizeof($b);$j++){
                                if($inF5){
                                    foreach($collaborateAns as $multiC){
                                        if($multiC[0]["MarkID"]==$b[$j]["MarkID"]){
                                            $totalScore += $b[$j]["MarkCodDes"];
                                        }
                                    }
                                }elseif($inF3){
                                    foreach($collaborateAns as $multiC){
                                        if(in_array($multiC[0]['FillerID'],$specialIdentityList['1'])){
                                            if($multiC[0]["MarkID"]==$b[$j]["MarkID"]){
                                                $totalPrincipalScore = 	$b[$j]["MarkCodDes"];
                                            }
                                        }else{
                                            if($multiC[0]["MarkID"]==$b[$j]["MarkID"]){
                                                $totalScore += 	$b[$j]["MarkCodDes"];
                                                $totalNonPrincipal++;
                                            }
                                        }
                                    }
                                }
                            }
                            if($inF5){
                                $average = round($totalScore/count($collaborateAns));
                                $_SESSION['totalFormF5Score'] = $_SESSION['totalFormF5Score'] + $average;
                            }elseif($inF3){
                                $average = round(($totalScore/$totalNonPrincipal)*0.5 + $totalPrincipalScore*0.5);
                            }
                            for($j=0;$j<sizeof($b);$j++){
                                $content.="<td>";
                                $content .= ($average == $b[$j]["Score"])?"&#10004;":"";
                                $content.="</td>";
                            }
                        }else{
                            for($j=0;$j<sizeof($b);$j++){
                                if($isCombinePrint==false || empty($collaborateAns)){
                                    $isChecked=($c[0]["MarkID"]==$b[$j]["MarkID"])?1:0;
                                    $Display=($isChecked)?"<b>".$b[$j]["MarkCodDes"]."</b>":$b[$j]["MarkCodDes"];
                                    $content.="<td>";
                                    //$content.=$indexVar['libappraisal_ui']->Get_Radio_Button("qID_".$a[$i]["QID"].$j,"qID_".$a[$i]["QID"], $Value=$b[$j]["MarkID"], $isChecked, $Class="", $Display, $Onclick="",$isDisabled=1);
                                    $content .= ($isChecked==1)?"&#10004;":"";
                                    $content.="</td>";
                                }else{
                                    $hasTickAlready = false;
                                    $content.="<td>";
                                    foreach($collaborateAns as $multiC){
                                        $displayInCell = ($hasTickAlready==false)?"&#10004;".$b[$j]["MarkCodDes"].":<br>".$multiC[0]["FillerName"]:$multiC[0]["FillerName"];
                                        $content .= ($multiC[0]["MarkID"]==$b[$j]["MarkID"])?$displayInCell."<br>":"";
                                        if($multiC[0]["MarkID"]==$b[$j]["MarkID"]){
                                            $hasTickAlready = true;
                                        }
                                    }
                                    $content.="</td>";
                                }
                            }
                        }
                        //$content.="<input type=\"hidden\" id=\"MarkID_".$a[$i]["QID"]."\" name=\"MarkID_".$a[$i]["QID"]."\" value=\"".$c[0]["MarkID"]."\">";
                        $content.="</tr>";
                        if($hasRemark==1 && $c[0]["Remark"]!=""){
                            if($isCombinePrint==true && !empty($collaborateRmk)){
                                if($rmkLabel!=""){
                                    $content.="<tr>";
                                    $content.="<td colspan=\"".sizeof($b)."\">";
                                    $content.= $rmkLabel;
                                    $content.="</td></tr>";
                                }
                                foreach($collaborateRmk as $remarkC){
                                    $content.="<tr>";
                                    $content.="<td colspan=\"".sizeof($b)."\">";
                                    if($remarkC['FillerName']!=""){
                                        $content.= $remarkC['FillerName'].": ";
                                    }
                                    $content.=nl2br($remarkC['Remark'])."</td>";
                                    $content.="</tr>";
                                }
                            }else{
                                $content.="<tr>";
                                $content.="<td colspan=\"".sizeof($b)."\">";
                                if($rmkLabel!=""){
                                    $content.= $rmkLabel."<br/>";
                                }
                                $content.=nl2br($c[0]["Remark"])."</td>";
                                $content.="</tr>";
                            }
                        }
                    }
            }
            else if($a[0]["IsAvgMark"]==1){
                $verValue=0;$countNA=0;
                for($i=0;$i<sizeof($a);$i++){
                    $value = 0;
                    if($sys_custom['eAppraisal']['WusichongStyle']){
                        $principalValue = 0;
                    }
                    $intSecID=$a[$i]["SecID"];
                    $intQusCodID=$i+1;
                    $qID=$a[$i]["QID"];
                    //$sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND QID='".$qID."';";
                    $sql="SELECT frmdtl.RecordID,frmdtl.BatchID,frmdtl.QID,frmdtl.QAnsID,frmdtl.MarkID,frmdtl.Remark,frmdtl.Score,DATE_FORMAT(frmdtl.DateAns,'%Y-%m-%d') as DateAns
						FROM INTRANET_PA_T_SLF_DTL frmdtl
						INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmdtl.RlsNo = frmsub.RlsNo AND frmdtl.RecordID = frmsub.RecordID AND frmdtl.BatchID=frmsub.BatchID
						WHERE frmdtl.RlsNo='".$rlsNo."' AND frmdtl.RecordID='".$recordID."' AND frmdtl.QID='".$qID."' AND (frmsub.SubDate IS NOT NULL OR frmsub.EditPrdTo < NOW());";
                    //echo $sql."<br/><br/>";;
                    $c=$this->returnResultSet($sql);
                    $c = $indexVar['libappraisal']->getQuestionDataByCmpArr($c);
                    $hasRemark=$a[$i]["HasRmk"];
                    $rmkLabel=$a[$i]["RmkLabel"];
                    $rowSpan=($hasRemark==1 && $c[0]["Remark"]!="")?"2":"1";
                    
                    $sql="SELECT UserID,TemplateID FROM(
							SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".$rlsNo."' AND BatchID='".$batchID."' AND RecordID='".$recordID."'
						) as iptfsub
						INNER JOIN(
							SELECT RecordID,UserID,TemplateID FROM INTRANET_PA_T_FRMSUM
						) as iptf ON iptfsub.RecordID=iptf.RecordID;";
                    //echo $sql."<br/><br/>";
                    $x=$this->returnResultSet($sql);
                    $userID=$x[0]["UserID"];
                    $templateID=$x[0]["TemplateID"];
                    $sql="SELECT RecordID FROM INTRANET_PA_T_FRMSUM WHERE UserID='".$userID."' AND TemplateID='".$templateID."';";
                    //echo $sql."<br/><br/>";
                    $x=$this->returnResultSet($sql);
                    $recordIDArr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($x,"RecordID");
                    //echo $recordIDArr."<br/><br/>";
                    
                    if($isCombinePrint==true){
                        $collaborateAns = array();
                        $collaborateRmk = array();
                        if(!empty($collaborativeForm)){
                            foreach($collaborativeForm as $cRecord){
                                //								$sql="SELECT RecordID,BatchID,dtl.QID,QAnsID,dtl.MarkID,dtl.Remark,qlks.Score,DATE_FORMAT(dtl.DateAns,'%Y-%m-%d') as DateAns, qlks.MarkCodChi, qlks.MarkCodEng FROM INTRANET_PA_T_SLF_DTL dtl";
                                //								$sql .= " INNER JOIN INTRANET_PA_S_QLKS qlks ON qlks.MarkID = dtl.MarkID";
                                //								$sql .= " WHERE RlsNo='".$rlsNo."' AND RecordID='".$cRecord."' AND dtl.QID='".$qID."';";
                                $sql="SELECT dtl.RecordID,dtl.BatchID,dtl.QID,QAnsID,dtl.MarkID,dtl.Remark,qlks.Score,DATE_FORMAT(dtl.DateAns,'%Y-%m-%d') as DateAns, qlks.MarkCodChi, qlks.MarkCodEng FROM INTRANET_PA_T_SLF_DTL dtl";
                                $sql .= " INNER JOIN INTRANET_PA_S_QLKS qlks ON qlks.MarkID = dtl.MarkID";
                                $sql .= " INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON dtl.RlsNo = frmsub.RlsNo AND dtl.RecordID = frmsub.RecordID AND dtl.BatchID=frmsub.BatchID";
                                $sql .= " WHERE dtl.RlsNo='".$rlsNo."' AND dtl.RecordID='".$cRecord."' AND dtl.QID='".$qID."' AND (frmsub.SubDate IS NOT NULL OR frmsub.EditPrdTo < NOW());";
                                //echo $sql."<br/><br/>";
                                $recordResult=$this->returnResultSet($sql);
                                if(!empty($recordResult)){
                                    $collaborateAns[$cRecord] = $indexVar['libappraisal']->getQuestionDataByCmpArr($recordResult);
                                    $collaborateRmk[$cRecord] = array('Remark'=>$recordResult[0]['Remark'],'FillerID'=>'');
                                }
                            }
                            if(!empty($collaborateAns)){
                                $collaborateAns[$recordID] = $c;
                                
                                foreach($collaborateAns as $rID=>$ansList){
                                    foreach($ansList as $index=>$ansDetail){
                                        $sql = "SELECT FillerID FROM INTRANET_PA_T_FRMSUB frmsub
												INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON frmsub.FillerID=u.UserID
												WHERE RecordID='".$ansDetail['RecordID']."' AND BatchID='".$ansDetail['BatchID']."'";
                                        $fillerID = current($this->returnVector($sql));
                                        $collaborateAns[$rID][$index]['FillerID'] = $fillerID;
                                        $collaborateRmk[$rID]['FillerID'] = $fillerID;
                                    }
                                }
                            }
                        }
                    }
                    if($isCombinePrint==true && !empty($collaborateAns)){
                        $content.="<tr>";
                        if($hasContent == true){
                            $content.="<td valign=\"top\" rowspan=\"".$rowSpan."\">".$a[$i]["QCodDes"]."</td>";
                        }
                        //$content.="<td rowspan=\"".$rowSpan."\">".nl2br($a[$i]["Descr"])."^".$sql."</td>";
                        $content.="<td rowspan=\"".$rowSpan."\">".nl2br($a[$i]["Descr"])."</td>";
                        $content.="<td>";
                        $sampleSize = sizeof($collaborateAns);
                        if($sys_custom['eAppraisal']['WusichongStyle']){
                            $principalNo = 0;
                            $teacherNo = 0;
                        }
                        foreach($collaborateAns as $multiC){
                            if($sys_custom['eAppraisal']['AverageReport']){
                                if($multiC[0]['MarkCodChi']=='N/A' || $multiC[0]['MarkCodEng']=='N/A'){
                                    $countNA++;
                                }else{
                                    $value=$value + $multiC[0]['Score'];
                                }
                            }elseif($sys_custom['eAppraisal']['WusichongStyle']){
                                if(strstr($templateData['FrmTitleChi'],'F3') || strstr($templateData['FrmTitleEng'],'F3')){
                                    if(isset($specialIdentityList[$multiC[0]['FillerID']]) && (in_array($multiC[0]['FillerID'],$specialIdentityList['1']))){
                                        $principalValue=$principalValue + $multiC[0]['Score'];
                                        $principalNo++;
                                    }else{
                                        $value=$value + $multiC[0]['Score'];
                                        $teacherNo++;
                                    }
                                }else if(strstr($templateData['FrmTitleChi'],'F4') || strstr($templateData['FrmTitleEng'],'F4')){
                                    if(isset($specialIdentityList[$multiC[0]['FillerID']]) && (in_array($multiC[0]['FillerID'],$specialIdentityList['1']) || in_array($multiC[0]['FillerID'],$specialIdentityList['2']))){
                                        $principalValue=$principalValue + $multiC[0]['Score'];
                                        $principalNo++;
                                    }else{
                                        $value=$value + $multiC[0]['Score'];
                                        $teacherNo++;
                                    }
                                }else{
                                    $value=$value + $multiC[0]['Score'];
                                }
                            }else{
                                $value = $value + $multiC[0]['Score'];
                            }
                        }
                        $averageNo = 0;
                        if($sampleSize > 0){
                            if($sys_custom['eAppraisal']['WusichongStyle']){
                                if(strstr($templateData['FrmTitleChi'],'F3') || strstr($templateData['FrmTitleEng'],'F3')){
                                    $averageNo = (($principalValue/$principalNo)*0.5 + ($value/$teacherNo)*0.5);
                                }else if(strstr($templateData['FrmTitleChi'],'F4') || strstr($templateData['FrmTitleEng'],'F4')){
                                    $averageNo = (($principalValue/$principalNo)*0.5 + ($value/$teacherNo)*0.5);
                                }else{
                                    $averageNo = ($value/$sampleSize);
                                }
                            }else{
                                $averageNo= ($value/$sampleSize);
                            }
                        }
                        if($sys_custom['eAppraisal']['WusichongStyle']){
                            $content.=floor($averageNo);
                        }else{
                            $content.=round($averageNo,$a[0]["AvgDec"]);
                        }
                        $content.="</td>";
                        //$content.="<input type=\"hidden\" id=\"MarkID_".$a[$i]["QID"]."\" name=\"MarkID_".$a[$i]["QID"]."\" value=\"".$c[0]["MarkID"]."\">";
                        $content.="</tr>";
                        $verValue=($sampleSize==0)?$verValue:$verValue + $averageNo;
                        if($hasRemark==1){
                            $content.="<tr>";
                            $content.="<td colspan=\"".$sampleSize."\">";
                            if($rmkLabel!=""){
                                $content.= $rmkLabel."<br/>";
                            }
                            $content.=nl2br($c[0]["Remark"])."</td>";
                            $content.="</tr>";
                        }
                    }else{
                        $sql="SELECT RecordID,BatchID,QID,QAnsID,ipstdtl.MarkID,Remark,DateAns,ipsqlks.Score,ipsqlks.MarkCodChi,ipsqlks.MarkCodEng
								FROM(
									SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL
									WHERE RlsNo='".$rlsNo."' AND BatchID='".$batchID."' AND QID='".$qID."' AND RecordID IN ('".$recordIDArr."')
								) as ipstdtl
								INNER JOIN(
									SELECT MarkID,Score,MarkCodChi,MarkCodEng FROM INTRANET_PA_S_QLKS
								) as ipsqlks ON ipstdtl.MarkID=ipsqlks.MarkID;";
                        //echo $sql."<br/><br/>";;
                        $b=$this->returnResultSet($sql);
                        $content.="<tr>";
                        if($hasContent == true){
                            $content.="<td valign=\"top\" rowspan=\"".$rowSpan."\">".$a[$i]["QCodDes"]."</td>";
                        }
                        //$content.="<td rowspan=\"".$rowSpan."\">".nl2br($a[$i]["Descr"])."^".$sql."</td>";
                        $content.="<td rowspan=\"".$rowSpan."\">".nl2br($a[$i]["Descr"])."</td>";
                        $content.="<td>";
                        for($j=0;$j<sizeof($b);$j++){
                            if($sys_custom['eAppraisal']['AverageReport']){
                                if($b[$j]['MarkCodChi']=='N/A' || $b[$j]['MarkCodEng']=='N/A'){
                                    unset($b[$j]);
                                    $countNA++;
                                }else{
                                    $value=$value + $b[$j]["Score"];
                                }
                            }else{
                                $value=$value + $b[$j]["Score"];
                            }
                        }
                        $content.=(sizeof($b)==0)?0:round($value/sizeof($b),$a[0]["AvgDec"]);;
                        $content.="</td>";
                        //$content.="<input type=\"hidden\" id=\"MarkID_".$a[$i]["QID"]."\" name=\"MarkID_".$a[$i]["QID"]."\" value=\"".$c[0]["MarkID"]."\">";
                        $content.="</tr>";
                        $verValue=(sizeof($b)==0)?$verValue:$verValue + ($value/sizeof($b));
                        if($hasRemark==1){
                            $content.="<tr>";
                            $content.="<td colspan=\"".sizeof($b)."\">";
                            if($rmkLabel!=""){
                                $content.= $rmkLabel."<br/>";
                            }
                            $content.=nl2br($c[0]["Remark"])."</td>";
                            $content.="</tr>";
                        }
                    }
                }
                if($a[0]["IsVerAvgMark"]==1){
                    $content.="<tr>";
                    $content.="<td>".sprintf($Lang['Appraisal']['TemplateSample']['SDFVerAvg'],$a[0]["VerAvgDec"])."</td>";
                    $content.="<td>";
                    $baseSize = sizeof($a) - $countNA;
                    if($sys_custom['eAppraisal']['WusichongStyle']){
                        $content.=floor($verValue/$baseSize);
                    }else{
                        $content.=round($verValue/$baseSize,$a[0]["VerAvgDec"]);
                    }
                    $content.="</td>";
                    
                    $content.="</tr>";
                }
            }
        }
        else if($quesDispMode==2){
            $sql = "SELECT MtxFldID,QCatID,".$indexVar['libappraisal']->getLangSQL("FldHdrChi","FldHdrEng")." as FldHdrDes,InputType,DisplayOrder
					FROM INTRANET_PA_S_QMTX  WHERE QCatID='".$qCatID."'
					ORDER BY DisplayOrder";
            //echo $sql."<br/><br/>";
            $b=$this->returnResultSet($sql);
            $hasQuestionDisplay = false;
            for($i=0;$i<sizeof($a);$i++){
                if($a[$i]["QCodDes"]!=""){
                    $hasQuestionDisplay = true;
                }
            }
            $descWidth =100;
            $content.="<tr>";
            if($hasContent == true){
                $content.="<th style=\"width:5%\"></th>";
                $descWidth = 95;
            }
            $dnmtr = (sizeof($b)==0)?1:sizeof($b);
            if($hasQuestionDisplay){
                $content.="<th style=\"width:15%\"></th>";
                $thWidth=($descWidth-15)/$dnmtr;
            }else{
                $thWidth=$descWidth/$dnmtr;
                $content.="<th style=\"width:1%\"></th>";
            }
            for($j=0;$j<sizeof($b);$j++){
                $content.="<th style=\"width:".floor($thWidth)."%\">".$b[$j]["FldHdrDes"]."</th>";
            }
            $content .= "</tr>";
            for($i=0;$i<sizeof($a);$i++){
                $intSecID=$a[$i]["SecID"];
                $intQusCodID=$i+1;
                $qID=$a[$i]["QID"];
                //$sql="SELECT RecordID,BatchID,QID,MtxFldID,Score,TxtAns,DateAns,Remark FROM INTRANET_PA_T_SLF_MTX WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND QID=".$qID.";";
                //				$sql="SELECT mtx.RecordID,mtx.BatchID,mtx.QID,mtx.MtxFldID,mtx.Score,mtx.TxtAns,mtx.DateAns,mtx.Remark FROM INTRANET_PA_T_SLF_MTX mtx
                //						INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON mtx.RlsNo = frmsub.RlsNo AND mtx.RecordID = frmsub.RecordID AND mtx.BatchID = frmsub.BatchID
                //						WHERE mtx.RlsNo='".$rlsNo."' AND mtx.RecordID='".$recordID."' AND mtx.QID='".$qID."' AND (frmsub.SubDate IS NOT NULL OR frmsub.EditPrdTo < NOW())";
                $sql= "SELECT mtx.RecordID,mtx.BatchID,mtx.QID,mtx.MtxFldID,mtx.Score,mtx.TxtAns,mtx.DateAns,mtx.Remark FROM INTRANET_PA_T_SLF_MTX mtx";
                $sql.=" INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON mtx.RlsNo = frmsub.RlsNo AND mtx.RecordID = frmsub.RecordID AND mtx.BatchID=frmsub.BatchID";
                $sql.=" WHERE mtx.RlsNo=".$rlsNo." AND mtx.RecordID=".$recordID." AND mtx.QID=".$qID." AND (frmsub.SubDate IS NOT NULL OR frmsub.EditPrdTo < NOW());";
                //echo $sql."<br/><br/>";;
                $c=$this->returnResultSet($sql);
                $c = $indexVar['libappraisal']->getQuestionDataByCmpArr($c);
                $hasRemark=$a[$i]["HasRmk"];
                $rmkLabel=$a[$i]["RmkLabel"];
                $rowSpan=($hasRemark==1 && $c[0]["Remark"]!="")?"2":"1";
                if($isCombinePrint==true){
                    $collaborateAns = array();
                    $collaborateRmk = array();
                    if(!empty($collaborativeForm)){
                        foreach($collaborativeForm as $cRecord){
                            //$sql="SELECT RecordID,BatchID,QID,MtxFldID,Score,TxtAns,DateAns,Remark FROM INTRANET_PA_T_SLF_MTX WHERE RlsNo='".$rlsNo."' AND RecordID='".$cRecord."' AND QID='".$qID."';";
                            $sql="SELECT mtx.RecordID,mtx.BatchID,mtx.QID,mtx.MtxFldID,mtx.Score,mtx.TxtAns,mtx.DateAns,mtx.Remark FROM INTRANET_PA_T_SLF_MTX mtx ";
                            $sql.=" INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON mtx.RlsNo = frmsub.RlsNo AND mtx.RecordID = frmsub.RecordID AND mtx.BatchID=frmsub.BatchID";
                            $sql.=" WHERE mtx.RlsNo='".$rlsNo."' AND mtx.RecordID='".$cRecord."' AND mtx.QID='".$qID."' AND (frmsub.SubDate IS NOT NULL OR frmsub.EditPrdTo < NOW());";
                            //echo $sql."<br/><br/>";
                            $recordResult=$this->returnResultSet($sql);
                            if(!empty($recordResult)){
                                $collaborateAns[$cRecord] = $indexVar['libappraisal']->getQuestionDataByCmpArr($recordResult);
                                $collaborateRmk[$cRecord] = array("Remark"=>$recordResult[0]['Remark'],'FillerName'=>'');
                            }
                        }
                        if(!empty($collaborateAns)){
                            $collaborateAns[$recordID] = $c;
                            
                            foreach($collaborateAns as $recordID=>$ansList){
                                foreach($ansList as $index=>$ansDetail){
                                    $sql = "SELECT ".$indexVar['libappraisal']->Get_Name_Field("u.")." as FillerName FROM INTRANET_PA_T_FRMSUB frmsub
											INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON frmsub.FillerID=u.UserID
											WHERE RecordID='".$ansDetail['RecordID']."' AND BatchID='".$ansDetail['BatchID']."'";
                                    $fillerName = current($this->returnVector($sql));
                                    $collaborateAns[$recordID][$index]['FillerName'] = $fillerName;
                                    $collaborateRmk[$recordID]['FillerName'] = $fillerName;
                                }
                            }
                        }
                    }
                }
                $content.="<tr>";
                if($hasContent == true){
                    $content.="<td valign=\"top\" rowspan=\"".$rowSpan."\">".$a[$i]["QCodDes"]."</td>";
                }
                $content.="<td rowspan=\"".$rowSpan."\">".nl2br($a[$i]["Descr"])."</td>";
                for($j=0;$j<sizeof($b);$j++){
                    $content.="<td>";
                    if($b[$j]["InputType"]==0){
                        $txtAns = $c[0]["TxtAns"];
                        $txtAnsArr = explode("#$^",$txtAns);
                        $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                        if($isCombinePrint==false ||empty($collaborateAns)){
                            $content.=$txtAnsArr[$j].$unit;
                        }else{
                            end($collaborateAns);
                            $lastKey = key($collaborateAns);
                            reset($collaborateAns);
                            foreach($collaborateAns as $ckey=>$multiC){
                                $multiTxtAns = $multiC[0]["TxtAns"];
                                $multiTxtAnsArr = explode("#$^",$multiTxtAns);
                                $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                $content.= $multiC[0]["FillerName"].":<br>".$multiTxtAnsArr[$j]."".$unit.$breakHtml;
                            }
                        }
                    }
                    else if($b[$j]["InputType"]==1){
                        $scoreAns = $c[0]["Score"];
                        $scoreAnsArr = explode("#$^",$scoreAns);
                        $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                        if($isCombinePrint==false ||empty($collaborateAns)){
                            $content.= $scoreAnsArr[$j]."".$unit;
                        }else{
                            end($collaborateAns);
                            $lastKey = key($collaborateAns);
                            reset($collaborateAns);
                            foreach($collaborateAns as $ckey=>$multiC){
                                $multiTxtAns = $multiC[0]["Score"];
                                $multiTxtAnsArr = explode("#$^",$multiTxtAns);
                                $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                $content.= $multiC[0]["FillerName"].":<br>".$multiTxtAnsArr[$j]."".$unit.$breakHtml;
                            }
                        }
                    }
                    else if($b[$j]["InputType"]==2){
                        $datAns = $c[0]["DateAns"];
                        $datAnsArr = explode("#$^",$datAns);
                        if($isCombinePrint==false ||empty($collaborateAns)){
                            $content.=$datAnsArr[$j];
                        }else{
                            end($collaborateAns);
                            $lastKey = key($collaborateAns);
                            reset($collaborateAns);
                            foreach($collaborateAns as $ckey=>$multiC){
                                $multiTxtAns = $multiC[0]["DateAns"];
                                $multiTxtAnsArr = explode("#$^",$multiTxtAns);
                                $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                $content.= $multiC[0]["FillerName"].":<br>".$multiTxtAnsArr[$j].$breakHtml;
                            }
                        }
                    }
                    else if($b[$j]["InputType"]==3){
                        $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                        $txtAns = $c[0]["TxtAns"];
                        $txtAnsArr = explode("#$^",$txtAns);
                        if($isCombinePrint==false ||empty($collaborateAns)){
                            $content.=nl2br($txtAnsArr[$j]).$unit;
                        }else{
                            end($collaborateAns);
                            $lastKey = key($collaborateAns);
                            reset($collaborateAns);
                            foreach($collaborateAns as $ckey=>$multiC){
                                $multiTxtAns = $multiC[0]["TxtAns"];
                                $multiTxtAnsArr = explode("#$^",$multiTxtAns);
                                $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                $content.= $multiC[0]["FillerName"].":<br>".$multiTxtAnsArr[$j]."".$unit.$breakHtml;
                            }
                        }
                    }
                    $content.="<input type=\"hidden\" id=\"MarkID_".$a[$i]["QID"]."\" name=\"MarkID_".$a[$i]["QID"]."\" value=\"".$b[$j]["MtxFldID"]."\">";
                    $content.="</td>";
                }
                $content.="</tr>";
                if($hasRemark==1 && $c[0]["Remark"]!=""){
                    if($isCombinePrint==true && !empty($collaborateRmk)){
                        if($rmkLabel!=""){
                            $content.="<tr>";
                            $content.="<td colspan=\"".sizeof($b)."\">";
                            $content.= $rmkLabel;
                            $content.="</td></tr>";
                        }
                        foreach($collaborateRmk as $remarkC){
                            $content.="<tr><td></td><td></td>";
                            $content.="<td colspan=\"".sizeof($b)."\">";
                            if($remarkC['FillerName']!=""){
                                $content.= $remarkC['FillerName'].": ";
                            }
                            $content.=nl2br($remarkC['Remark'])."</td>";
                            $content.="</tr>";
                        }
                    }else{
                        $content.="<tr>";
                        $content.="<td colspan=\"".sizeof($b)."\">";
                        if($rmkLabel!=""){
                            $content.= $rmkLabel."<br/>";
                        }
                        $content.=nl2br($c[0]["Remark"])."</td>";
                        $content.="</tr>";
                    }
                }
            }
        }
        else if($quesDispMode==0){
            for($i=0;$i<sizeof($a);$i++){
                $value=0;
                $intSecID=$a[$i]["SecID"];
                $intQusCodID=$i+1;
                $qID=$a[$i]["QID"];
                $sql = "SELECT IF(ParentSecID IS NULL, SecID,ParentSecID) as CurrentSection FROM INTRANET_PA_S_FRMSEC WHERE SecID='".$intSecID."'";
                $currentSection = current($this->returnVector($sql));
                $sql = "SELECT AppRoleID FROM INTRANET_PA_S_SECROL WHERE SecID='".$currentSection."' AND CanFill=1";
                $appRoleID = $this->returnVector($sql);
                $sql = "SELECT BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND AppRoleID IN ('".implode("','",$appRoleID)."') AND SecID='".$currentSection."' AND SubDate IS NOT NULL";
                $secBatchID = $this->returnVector($sql);
                //				$sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,TxtAns,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns
                //						FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID IN ('".implode("','",$secBatchID)."') AND QID='".$qID."';";
                $sql="SELECT dtl.RecordID,dtl.BatchID,dtl.QID,dtl.QAnsID,dtl.MarkID,dtl.Remark,dtl.Score,dtl.TxtAns,DATE_FORMAT(dtl.DateAns,'%Y-%m-%d') as DateAns
						FROM INTRANET_PA_T_SLF_DTL dtl ";
                $sql.= " INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON dtl.RlsNo = frmsub.RlsNo AND dtl.RecordID = frmsub.RecordID AND dtl.BatchID = frmsub.BatchID ";
                $sql.= " WHERE dtl.RlsNo='".$rlsNo."' AND dtl.RecordID='".$recordID."' AND dtl.BatchID IN ('".implode("','",$secBatchID)."') AND dtl.QID='".$qID."' AND (frmsub.SubDate IS NOT NULL OR frmsub.EditPrdTo < NOW());";
                //echo $sql."<br/><br/>";;
                $c=$this->returnResultSet($sql);
                $a[$i]["Descr"] = "<span style='line-height:200%;font-weight:bold;'>".$a[$i]["Descr"]."</span>";
                $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='PrintMcDisplayMode'";
                $mcDisplaySettingValue = current($this->returnVector($sql));
                
                if($isCombinePrint==true){
                    $collaborateAns = array();
                    $collaborateRmk = array();
                    if(!empty($collaborativeForm)){
                        foreach($collaborativeForm as $cRecord){
                            //$sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,TxtAns,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo='".$rlsNo."' AND RecordID='".$cRecord."' AND QID='".$qID."';";
                            $sql="SELECT dtl.RecordID,dtl.BatchID,dtl.QID,dtl.QAnsID,dtl.MarkID,dtl.Remark,dtl.Score,dtl.TxtAns,DATE_FORMAT(dtl.DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL dtl";
                            $sql.= " INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON dtl.RlsNo = frmsub.RlsNo AND dtl.RecordID = frmsub.RecordID AND dtl.BatchID = frmsub.BatchID ";
                            $sql.= " WHERE dtl.RlsNo='".$rlsNo."' AND dtl.RecordID='".$cRecord."' AND dtl.QID='".$qID."'  AND (frmsub.SubDate IS NOT NULL OR frmsub.EditPrdTo < NOW());";
                            //echo $sql."<br/><br/>";
                            $recordResult=$this->returnResultSet($sql);
                            if(!empty($recordResult)){
                                $collaborateAns[$cRecord] = $indexVar['libappraisal']->getQuestionDataByCmpArr($recordResult);
                                $collaborateRmk[$cRecord] = array('Remark'=>$recordResult[0]['Remark'],'FillerName'=>'');
                            }
                        }
                        if(!empty($collaborateAns)){
                            $collaborateAns[$recordID] = $c;
                            
                            foreach($collaborateAns as $recordID=>$ansList){
                                foreach($ansList as $index=>$ansDetail){
                                    $sql = "SELECT ".$indexVar['libappraisal']->Get_Name_Field("u.")." as FillerName FROM INTRANET_PA_T_FRMSUB frmsub
											INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON frmsub.FillerID=u.UserID
											WHERE RecordID='".$ansDetail['RecordID']."' AND BatchID='".$ansDetail['BatchID']."' ORDER BY frmsub.RlsNo DESC";
                                    $fillerName = current($this->returnVector($sql));
                                    $collaborateAns[$recordID][$index]['FillerName'] = $fillerName;
                                    $collaborateRmk[$recordID]['FillerName'] = $fillerName;
                                }
                            }
                        }
                    }
                }
                if($a[$i]["InputType"]==0){
                    $content.="<tr>";
                    $content.="<td valign=\"top\" style=\"border:none\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td style=\"border:none\">".nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none\">".nl2br($a[$i]["Rmk"])."</td></tr>";
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none\">".nl2br($c[0]["Remark"])."</td></tr>";
                    }
                    else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none\">".nl2br($c[0]["Remark"])."</td></tr>";
                    }
                }
                else if($a[$i]["InputType"]==1){
                    $sql="SELECT QAnsID,QID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,".$indexVar['libappraisal']->getLangSQL("QAnsChi","QAnsEng")." as QAns,DisplayOrder
						FROM INTRANET_PA_S_QANS WHERE QID='".$a[$i]["QID"]."' ORDER BY DisplayOrder;";
                    //echo $sql."<br/><br/>";
                    $b=$this->returnResultSet($sql);
                    $content.="<tr>";
                    $content.="<td valign=\"top\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td>".nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    if($isCombinePrint==false ||empty($collaborateAns)){
                        for($j=0;$j<sizeof($b);$j++){
                            $isChecked=($c[0]["QAnsID"]==$b[$j]["QAnsID"])?1:0;
                            if($mcDisplaySettingValue=="Checked"){
                                if($isChecked){
                                    $content.="<tr>";
                                    $content.="<td>";
                                    //$content .= (($isChecked==1)?"&#10004;":"");
                                    $content .="</td>";
                                    $content .= "<td><table><tr><td style='vertical-align: top;width:10px;'><b>".$b[$j]["QAns"]."</b></td><td>".$b[$j]["Descr"]."</td></tr></table></td>";
                                    $content.="</tr>";
                                }
                            }else{
                                $content.="<tr>";
                                $content.="<td>";
                                //$content.=$indexVar['libappraisal_ui']->Get_Radio_Button("qID_".$a[$i]["QID"]."_".$j, "qID_".$a[$i]["QID"], $Value=$b[$j]["QAnsID"], $isChecked=($c[0]["QAnsID"]==$b[$j]["QAnsID"])?1:0, $Class="", $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=1);
                                $content .= (($isChecked==1)?"&#10004;":"")."</td>";
                                $content .= "<td>".$b[$j]["Descr"]."</td>";
                                $content.="</tr>";
                            }
                        }
                    }else{
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none\"><table>";
                        end($collaborateAns);
                        $lastKey = key($collaborateAns);
                        reset($collaborateAns);
                        $content.="<tr><td style=\"border:none\"></td>";
                        foreach($collaborateAns as $ckey=>$multiC){
                            $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                            if($ckey!=$lastKey){
                                $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                            }
                        }
                        $content.="</tr>";
                        
                        $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                        for($j=0;$j<sizeof($b);$j++){
                            $content.="<tr><td style=\"border:none\"></td>";
                            foreach($collaborateAns as $ckey=>$multiC){
                                $isChecked=($multiC[0]["QAnsID"]==$b[$j]["QAnsID"])?1:0;
                                $content.="<td style=\"border:none\">";
                                //$content.=$indexVar['libappraisal_ui']->Get_Radio_Button($ckey."_qID_".$a[$i]["QID"]."_".$j, $ckey."_qID_".$a[$i]["QID"], $Value=$b[$j]["QAnsID"], $isChecked=($multiC[0]["QAnsID"]==$b[$j]["QAnsID"])?1:0, $Class=$isRequiredField, $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=1,$isIndented=1, $specialLabel=$b[$j]["QAns"]);
                                $content .= (($isChecked==1)?"&#10004;":"").$b[$j]["Descr"];
                                $content.="</td>";
                                if($ckey!=$lastKey){
                                    $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                }
                            }
                            $content.="</tr>";
                        }
                        $content.="</table></td>";
                        $content.="</tr>";
                    }
                    if($isCombinePrint==false ||empty($collaborateRmk)){
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                            $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                        }
                    }else{
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                            foreach($collaborateRmk as $rmkContent){
                                $content.="<tr><td>".$rmkContent['FillerName'].": </td><td>".nl2br($rmkContent["Remark"])."</td></tr>";
                            }
                        }else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            foreach($collaborateRmk as $rmkContent){
                                $content.="<tr><td>".$rmkContent['FillerName'].": </td><td>".nl2br($rmkContent["Remark"])."</td></tr>";
                            }
                        }
                    }
                }
                else if($a[$i]["InputType"]==2){
                    $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                    $content.="<tr>";
                    $content.="<td valign=\"top\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td>".nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    if($isCombinePrint==false ||empty($collaborateAns)){
                        $content.="<tr><td></td><td>".$c[0]["TxtAns"].$unit."</td>";;
                    }else{
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none\"><table>";
                        end($collaborateAns);
                        $lastKey = key($collaborateAns);
                        reset($collaborateAns);
                        $content.="<tr>";
                        foreach($collaborateAns as $ckey=>$multiC){
                            $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                            if($ckey!=$lastKey){
                                $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                            }
                        }
                        $content.="</tr>";
                        
                        $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                        $content.="<tr>";
                        foreach($collaborateAns as $ckey=>$multiC){
                            $content.="<td>".$multiC[0]["TxtAns"]."".$unit."</td>";
                            if($ckey!=$lastKey){
                                $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                            }
                        }
                        $content.="</tr>";
                        $content.="</table></td>";
                        $content.="</tr>";
                    }
                    $content.="</tr>";
                    if($isCombinePrint==false ||empty($collaborateRmk)){
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                            $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                        }
                    }else{
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                            foreach($collaborateRmk as $rmkContent){
                                $content.="<tr><td>".$rmkContent['FillerName'].": </td><td>".nl2br($rmkContent["Remark"])."</td></tr>";
                            }
                        }else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            foreach($collaborateRmk as $rmkContent){
                                $content.="<tr><td>".$rmkContent['FillerName'].": </td><td>".nl2br($rmkContent["Remark"])."</td></tr>";
                            }
                        }
                    }
                }
                else if($a[$i]["InputType"]==3){
                    if($a[$i]["ScoreIsAvgMark"]==0){
                        $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                        $content.="<tr>";
                        $content.="<td valign=\"top\">".$a[$i]["QCodDes"]."</td>";
                        $content.="<td>".nl2br($a[$i]["Descr"])."</td>";
                        $content.="</tr>";
                        if($isCombinePrint==false ||empty($collaborateAns)){
                            $content.="<tr><td></td><td>".$c[0]["Score"].$unit."</td></tr>";
                        }else{
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                            $content .= "<table>";
                            end($collaborateAns);
                            $lastKey = key($collaborateAns);
                            reset($collaborateAns);
                            $content.="<tr>";
                            foreach($collaborateAns as $ckey=>$multiC){
                                $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                                if($ckey!=$lastKey){
                                    $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                }
                            }
                            $content.="</tr>";
                            $content.="<tr>";
                            foreach($collaborateAns as $ckey=>$multiC){
                                $content.="<td style=\"border:none;\">".$multiC[0]["Score"]."".$unit."</td>";
                                if($ckey!=$lastKey){
                                    $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                }
                            }
                            $content.="</tr>";
                            $content .= "</table></td></tr>";
                        }
                        if($isCombinePrint==false ||empty($collaborateRmk)){
                            if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                                $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                                $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                            }
                            else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                                $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                            }
                        }else{
                            if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                                $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                                foreach($collaborateRmk as $rmkContent){
                                    $content.="<tr><td>".$rmkContent['FillerName'].": </td><td>".nl2br($rmkContent["Remark"])."</td></tr>";
                                }
                            }else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                                foreach($collaborateRmk as $rmkContent){
                                    $content.="<tr><td>".$rmkContent['FillerName'].": </td><td>".nl2br($rmkContent["Remark"])."</td></tr>";
                                }
                            }
                        }
                    }
                    else if($a[$i]["ScoreIsAvgMark"]==1){
                        if($isCombinePrint==false ||empty($collaborateAns)){
                            $sql="SELECT UserID,TemplateID FROM(
								SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".$rlsNo."' AND BatchID='".$batchID."' AND RecordID='".$recordID."'
							) as iptfsub
							INNER JOIN(
								SELECT RecordID,UserID,TemplateID FROM INTRANET_PA_T_FRMSUM
							) as iptf ON iptfsub.RecordID=iptf.RecordID;";
                            //echo $sql."<br/><br/>";
                            $x=$this->returnResultSet($sql);
                            $userID=$x[0]["UserID"];
                            $templateID=$x[0]["TemplateID"];
                            $sql="SELECT RecordID FROM INTRANET_PA_T_FRMSUM WHERE UserID='".$userID."' AND TemplateID='".$templateID."'";
                            //echo $sql."<br/><br/>";
                            $x=$this->returnResultSet($sql);
                            $recordIDArr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($x,"RecordID");
                            //echo $recordIDArr."<br/><br/>";
                            
                            $sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,TxtAns,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns
								FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo='".$rlsNo."' AND BatchID='".$batchID."' AND QID='".$qID."' AND RecordID IN (".$recordIDArr.");";
                            //echo $sql."<br/><br/>";;
                            $c=$this->returnResultSet($sql);
                            
                            $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                            $content.="<tr>";
                            $content.="<td valign=\"top\">".$a[$i]["QCodDes"]."</td>";
                            $content.="<td>".nl2br($a[$i]["Descr"])."</td>";
                            $content.="</tr>";
                            for($j=0;$j<sizeof($c);$j++){
                                $value = $value + $c[$j]["Score"];
                            }
                            if(sizeof($c) > 0){
                                $content.="<tr><td></td><td>".(round($value/sizeof($c),$a[0]["ScoreAvgDec"])).$unit."</td></tr>";
                            }else{
                                $content.="<tr><td></td><td>".(round(0,$a[0]["ScoreAvgDec"])).$unit."</td></tr>";
                            }
                        }else{
                            end($collaborateAns);
                            $lastKey = key($collaborateAns);
                            reset($collaborateAns);
                            $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                            $content.="<tr>";
                            $content.="<td valign=\"top\">".$a[$i]["QCodDes"]."</td>";
                            $content.="<td>".nl2br($a[$i]["Descr"])."</td>";
                            $content.="</tr>";
                            $value = 0;
                            foreach($collaborateAns as $ckey=>$multiC){
                                $value = $value + $multiC[0]['Score'];
                            }
                            if($value==0){
                                $content.="<tr><td></td><td>".(round(0,$a[0]["ScoreAvgDec"])).$unit."</td></tr>";
                            }else{
                                $content.="<tr><td></td><td>".(round($value/sizeof($collaborateAns),$a[0]["ScoreAvgDec"])).$unit."</td></tr>";
                            }
                            $content.="</tr>";
                        }
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                            $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                        }
                    }
                }
                else if($a[$i]["InputType"]==4){
                    $content.="<tr>";
                    $content.="<td valign=\"top\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td>".$a[$i]["Descr"]."</td>";
                    $content.="</tr>";
                    if($isCombinePrint==false ||empty($collaborateAns)){
                        $content.="<tr><td></td><td>".$c[0]["DateAns"]."</td></tr>";
                    }else{
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;\">";
                        $content .= "<table>";
                        end($collaborateAns);
                        $lastKey = key($collaborateAns);
                        reset($collaborateAns);
                        $content.="<tr>";
                        foreach($collaborateAns as $ckey=>$multiC){
                            $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                            if($ckey!=$lastKey){
                                $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                            }
                        }
                        $content.="</tr>";
                        $content.="<tr>";
                        foreach($collaborateAns as $ckey=>$multiC){
                            $content.="<td style=\"border:none;\">".$multiC[0]["DateAns"]."</td>";
                            if($ckey!=$lastKey){
                                $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                            }
                        }
                        $content.="</tr>";
                        $content .= "</table></td></tr>";
                    }
                    if($isCombinePrint==false ||empty($collaborateRmk)){
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                            $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                        }
                    }else{
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                            foreach($collaborateRmk as $rmkContent){
                                $content.="<tr><td>".$rmkContent['FillerName'].": </td><td>".nl2br($rmkContent["Remark"])."</td></tr>";
                            }
                        }else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            foreach($collaborateRmk as $rmkContent){
                                $content.="<tr><td>".$rmkContent['FillerName'].": </td><td>".nl2br($rmkContent["Remark"])."</td></tr>";
                            }
                        }
                    }
                }
                else if($a[$i]["InputType"]==5){
                    $content.="<tr class=\"form_ques_title\">";
                    $content.="<td valign=\"top\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td>".nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    //					foreach($c as $ans){
                    //						$content.="<tr class=\"form_ques_content\"><td></td><td>".nl2br($ans["Remark"])."</td></tr>";
                    //					}
                    if($isCombinePrint==false ||empty($collaborateAns)){
                        $content.="<tr class=\"form_ques_content\"><td></td><td>".nl2br($c[count($c)-1]["Remark"])."</td></tr>";
                    }else{
                        if($sys_custom['eAppraisal']['WusichongStyle']){
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;\">";
                            $content .= "<table>";
                            foreach($collaborateAns as $ckey=>$multiC){
                                /*
                                 $content .= "<tr><td style=\"border:none\"></td><td style=\"border:none;\">";
                                 $content .= $multiC[0]["FillerName"];
                                 $content .= "</td></tr>";
                                 */
                                if($multiC[0]["Remark"]!=""){
                                    $content .= "<tr><td style=\"border:none\"></td><td style=\"border:1px solid;padding-bottom:20px;padding-right:30px;\">";
                                    $content .= $multiC[0]["Remark"];
                                    $content .= "</td></tr>";
                                }
                            }
                            $content .= "</table></td></tr>";
                        }else{
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;\">";
                            $content .= "<table>";
                            end($collaborateAns);
                            $lastKey = key($collaborateAns);
                            reset($collaborateAns);
                            $content.="<tr>";
                            foreach($collaborateAns as $ckey=>$multiC){
                                $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                                if($ckey!=$lastKey){
                                    $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                }
                            }
                            $content.="</tr>";
                            $content.="<tr>";
                            foreach($collaborateAns as $ckey=>$multiC){
                                $content.="<td style=\"border:none;\">".nl2br($multiC[0]["Remark"])."</td>";
                                if($ckey!=$lastKey){
                                    $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                }
                            }
                            $content.="</tr>";
                            $content .= "</table></td></tr>";
                        }
                    }
                }
                else if($a[$i]["InputType"]==6){
                    $sql="SELECT QAnsID,QID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,".$indexVar['libappraisal']->getLangSQL("QAnsChi","QAnsEng")." as QAns,DisplayOrder
						FROM INTRANET_PA_S_QANS WHERE QID='".$a[$i]["QID"]."' ORDER BY DisplayOrder;";
                    //echo $sql."<br/><br/>";
                    $b=$this->returnResultSet($sql);
                    $content.="<tr>";
                    $content.="<td valign=\"top\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td>".nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    if($isCombinePrint==false ||empty($collaborateAns)){
                        $mcmAnsID = explode(",",$c[0]["TxtAns"]);
                        for($j=0;$j<sizeof($b);$j++){
                            $content.="<tr><td></td>";
                            $content.="<td>";
                            $chkBoxChecked = 0;
                            for($k=0;$k<sizeof($mcmAnsID);$k++){
                                if($b[$j]["QAnsID"] == $mcmAnsID[$k]){
                                    $chkBoxChecked = 1;
                                    break;
                                }
                            }
                            //$content.=$indexVar['libappraisal_ui']->Get_Checkbox("qID_".$a[$i]["QID"]."_".$j, "qID_".$a[$i]["QID"], $Value=$b[$j]["QAnsID"], $isChecked=$chkBoxChecked, $Class="", $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=0);
                            if($chkBoxChecked){
                                $content .= "<b>".$b[$j]["Descr"]."</b><br>";
                            }else{
                                $content .= $b[$j]["Descr"];
                            }
                            $content.="</td>";
                            $content.="</tr>";
                        }
                    }else{
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;\">";
                        $content .= "<table>";
                        end($collaborateAns);
                        $lastKey = key($collaborateAns);
                        reset($collaborateAns);
                        $content.="<tr>";
                        foreach($collaborateAns as $ckey=>$multiC){
                            $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                            if($ckey!=$lastKey){
                                $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                            }
                        }
                        $content.="</tr>";
                        for($j=0;$j<sizeof($b);$j++){
                            $content.="<tr>";
                            foreach($collaborateAns as $ckey=>$multiC){
                                $content.="<td style=\"border:none\">";
                                $mcmAnsID = explode(",",$multiC[0]["TxtAns"]);
                                $chkBoxChecked = 0;
                                for($k=0;$k<sizeof($mcmAnsID);$k++){
                                    if($b[$j]["QAnsID"] == $mcmAnsID[$k]){
                                        $chkBoxChecked = 1;
                                        break;
                                    }
                                }
                                //$content.=$indexVar['libappraisal_ui']->Get_Checkbox("qID_".$a[$i]["QID"]."_".$j, "qID_".$a[$i]["QID"]."[]", $Value=$b[$j]["QAnsID"], $isChecked=$chkBoxChecked, $Class=$isRequiredField, $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=1,$isIndented=1,$specialLabel=$b[$j]["QAns"]);
                                $content .= (($chkBoxChecked)?"<b>":"").$b[$j]["Descr"].(($chkBoxChecked)?"</b>":"");
                                
                                $content.="</td>";
                                if($ckey!=$lastKey){
                                    $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                }
                            }
                            $content.="</tr>";
                        }
                        $content.="<tr>";
                        
                        $content.="</tr>";
                        $content .= "</table></td></tr>";
                    }
                    if($isCombinePrint==false ||empty($collaborateRmk)){
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                            $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                        }
                    }else{
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                            foreach($collaborateRmk as $rmkContent){
                                $content.="<tr><td>".$rmkContent['FillerName'].": </td><td>".nl2br($rmkContent["Remark"])."</td></tr>";
                            }
                        }else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            foreach($collaborateRmk as $rmkContent){
                                $content.="<tr><td>".$rmkContent['FillerName'].": </td><td>".nl2br($rmkContent["Remark"])."</td></tr>";
                            }
                        }
                    }
                }
                else if($a[$i]["InputType"]==7){
                    $content.="<tr>";
                    $content.="<td valign=\"top\">".$a[$i]["QCodDes"]."</td>";
                    $content.="<td>".nl2br($a[$i]["Descr"])."</td>";
                    $content.="</tr>";
                    $content.="<tr><td></td>";
                    $content.="<td>";
                    if($isCombinePrint==false ||empty($collaborateAns)){
                        //$content.="<input type=\"file\" name=\"qID_".$a[$i]["QID"]."_".$j."\" id=\"qID_".$a[$i]["QID"]."_".$j."\" class=\"file\">";
                        $fileDataArr = explode("|",$c[0]["TxtAns"]);
                        $content .=$fileDataArr[2];
                    }else{
                        $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;\">";
                        $content .= "<table>";
                        end($collaborateAns);
                        $lastKey = key($collaborateAns);
                        reset($collaborateAns);
                        $content.="<tr>";
                        foreach($collaborateAns as $ckey=>$multiC){
                            $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                            if($ckey!=$lastKey){
                                $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                            }
                        }
                        $content.="</tr>";
                        $content.="<tr>";
                        foreach($collaborateAns as $ckey=>$multiC){
                            $fileDataArr = explode("|",$multiC[0]["TxtAns"]);
                            $content .= "<td style=\"border:none\">";
                            $content .= $fileDataArr[2];
                            $content .= "</td>";
                            if($ckey!=$lastKey){
                                $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                            }
                        }
                        $content.="</tr>";
                        $content .= "</table></td></tr>";
                    }
                    $content.="</td>";
                    $content.="</tr>";
                    
                    if($isCombinePrint==false ||empty($collaborateRmk)){
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                            $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            $content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
                        }
                    }else{
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
                            foreach($collaborateRmk as $rmkContent){
                                $content.="<tr><td>".$rmkContent['FillerName'].": </td><td>".nl2br($rmkContent["Remark"])."</td></tr>";
                            }
                        }else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            foreach($collaborateRmk as $rmkContent){
                                $content.="<tr><td>".$rmkContent['FillerName'].": </td><td>".nl2br($rmkContent["Remark"])."</td></tr>";
                            }
                        }
                    }
                }
                $content .= "<tr><td colspan='2' style='height:10px;'></td></tr>";
            }
        }
        return $content;
    }
    
}


?>