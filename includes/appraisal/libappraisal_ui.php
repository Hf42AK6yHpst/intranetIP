<?php
// editing by 
/*****************************************************************************
 * Modification Log:
 *  
 * 
 *****************************************************************************/

if (!defined("LIBAPPRAISAL_UI_DEFINED")) {
	define("LIBAPPRAISAL_UI_DEFINED", true);
	
	class libappraisal_ui extends interface_html {
		
		function libappraisal_ui($parTemplate='') {
			$template = ($parTemplate=='')? 'default.html' : $parTemplate;
			$this->interface_html($template);
		}
		
		function echoModuleLayoutStart($parPageCode, $parReturnMsg='', $parForPopup=false) {
			global $indexVar, $CurrentPage, $MODULE_OBJ, $TAGS_OBJ, $CurrentPageArr, $PATH_WRT_ROOT, $intranet_session_language;
			
			$CurrentPage = $parPageCode;
			$MODULE_OBJ = $indexVar['libappraisal']->getModuleObjArr();
			if ($_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
				$CurrentPageArr['eServiceeAppraisal'] = 1;
			} else {
				$CurrentPageArr['eAdmineAppraisal'] = 1;
			}
			
			if ($parForPopup) {
				$this->interface_html('popup.html');
			}
			$this->LAYOUT_START($parReturnMsg);
			echo '<!-- task: '.$indexVar['taskScript'].'-->';
			echo '<!-- template: '.$indexVar['templateScript'].'-->';
		}
		
		function echoModuleLayoutStop() {
			$this->LAYOUT_STOP();
		}
		
		
	}
}
?>