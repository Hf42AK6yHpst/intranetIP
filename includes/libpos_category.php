<?php
// Editing by Cameron
/*
 * 2020-02-06 (Cameron)
 *      - modify Delete_Photo(), also delete original photo
 * 2020-02-05 (Cameron): 
 *      - bypass checking image dimension in Upload_Photo() so that user can upload original image
 *      - add function resizeImage()
 *      - modify Upload_Photo() to store original image (in original path) and resized image (95x95 pixel)
 * 2019-10-15 (Cameron): add parameter $inventoryOnly to Get_All_Category()
 * 2014-02-13 (Carlos): Added Permanent_Delete_Category()
 * 2014-02-11 (Carlos): Added Get_Category_Item_Log_Counting() - count the number of logs related to each item
 */
//include_once('libpos.php');
include_once('libdb.php');

class POS_Category extends libdb {
	var $TableName;
	var $ID_FieldName;
	var $ObjectID;
	var $PhotoWidthMax;
	var $PhotoHeightMax;
	
	var $CategoryID;
	var $CategoryCode;
	var $CategoryName;
	var $PhotoExt;
	var $Sequence;
	var $RecordStatus;
	var $DateInput;
	var $InputBy;
	var $DateModify;
	var $ModifyBy;
	
	var $PhotoPath;
	var $OriginalPhotoPath;
// 	var $OriginalPhotoWidthMax;
// 	var $OriginalPhotoHeightMax;
	
	function POS_Category($ObjectID=""){
		parent:: libdb();
		
		$this->TableName = 'POS_ITEM_CATEGORY';
		$this->ID_FieldName = 'CategoryID';
		$this->PhotoWidthMax = 95;
		$this->PhotoHeightMax = 95;
// 		$this->OriginalPhotoWidthMax = 1000;
// 		$this->OriginalPhotoHeightMax = 750;
		
		if ($ObjectID!="")
		{
			$this->ObjectID = $ObjectID;
			$this->Get_Self_Info();
			
			// Get PhotoPath
			if ($this->PhotoExt != '')
			{
				$this->PhotoPath = "/file/pos/photo/category/".$this->ObjectID.$this->PhotoExt;
				$this->OriginalPhotoPath = "/file/pos/photo/category/original/".$this->ObjectID.$this->PhotoExt;
			}
		}
	}
	
	function Get_Self_Info()
	{
		$sql = "";
		$sql .= " SELECT * FROM ".$this->TableName;
		$sql .= " WHERE ".$this->ID_FieldName." = '".$this->ObjectID."'";
		
		$resultSet = $this->returnArray($sql);
		$infoArr = $resultSet[0];
		
		foreach ($infoArr as $key => $value)
			$this->{$key} = $value;
	}
	
	function Insert_Object($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
		
		# set field and value string
		$fieldArr = array();
		$valueArr = array();
		foreach ($DataArr as $field => $value)
		{
			$fieldArr[] = $field;
			$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
		}
		
		## set others fields
		# RecordStatus (max 10 enabled category)
		$StatusArr = $this->Get_RecordStatus_Count();
		$EnabledCount = $StatusArr['Enabled'];
		$RecordStatus = ($EnabledCount < 10)? 1 : 0;
		$fieldArr[] = 'RecordStatus';
		$valueArr[] = $RecordStatus;
		# Sequence
		$fieldArr[] = 'Sequence';
		$valueArr[] = $this->Get_Max_Display_Order() + 1;
		# DateInput
		$fieldArr[] = 'DateInput';
		$valueArr[] = 'now()';
		# InputBy
		$fieldArr[] = 'InputBy';
		$valueArr[] = $_SESSION['UserID'];
		# DateModify
		$fieldArr[] = 'DateModify';
		$valueArr[] = 'now()';
		# ModifyBy
		$fieldArr[] = 'ModifyBy';
		$valueArr[] = $_SESSION['UserID'];
		
		$fieldText = implode(", ", $fieldArr);
		$valueText = implode(", ", $valueArr);
		
		# Insert Record
		$this->Start_Trans();
		
		$sql = '';
		$sql .= ' INSERT INTO '.$this->TableName;
		$sql .= ' ( '.$fieldText.' ) ';
		$sql .= ' VALUES ';
		$sql .= ' ( '.$valueText.' ) ';
		$success = $this->db_db_query($sql);
		
		if ($success == false)
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$insertedID = $this->db_insert_id();
			$this->Commit_Trans();
			
			return $insertedID;
		}
	}
	
	function Update_Object($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
		
		# Build field update values string
		$valueFieldText = '';
		foreach ($DataArr as $field => $value)
		{
			$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
		}
		$valueFieldText .= ' DateModify = now(), ModifyBy = \''.$_SESSION['UserID'].'\' ';
		
		$sql = '';
		$sql .= ' UPDATE '.$this->TableName;
		$sql .= ' SET '.$valueFieldText;
		$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$this->ObjectID.'\' ';
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Delete_Record()
	{
		$success = $this->Inactivate_Record();
		return $success;
	}
	
	function Inactivate_Record()
	{
		$DataArr['RecordStatus'] = "0";
		$success = $this->Update_Record($DataArr);
		
		return $success;
	}
	
	function Activate_Record()
	{
		$DataArr['RecordStatus'] = 1;
		$success = $this->Update_Record($DataArr);
		
		return $success;
	}
	
	function Is_Code_Valid($Code, $ExcludeObjectID='')
	{
		$cond_excludeObjectID = '';
		if ($ExcludeObjectID != '')
			$cond_excludeObjectID = " And ".$this->ID_FieldName." != '".$ExcludeObjectID."' ";
			
		$sql = "Select
						".$this->ID_FieldName."
				From
						".$this->TableName."
				Where
						CategoryCode = '".$this->Get_Safe_Sql_Query($Code)."'
						$cond_excludeObjectID
				";
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) == 0)
			return 1;
		else
			return 0;
	}
	
	function Is_Name_Valid($Name, $ExcludeObjectID='')
	{
		$cond_excludeObjectID = '';
		if ($ExcludeObjectID != '')
			$cond_excludeObjectID = " And ".$this->ID_FieldName." != '".$ExcludeObjectID."' ";
			
		$sql = "Select
						".$this->ID_FieldName."
				From
						".$this->TableName."
				Where
						CategoryName = '".$this->Get_Safe_Sql_Query($Name)."'
						$cond_excludeObjectID
				";
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) == 0)
			return 1;
		else
			return 0;
	}
	
	function Upload_Photo($FileLocation, $FileName)
	{
		global $intranet_root;
		
// 		$isValidDimension = $this->Check_Valid_Photo_Dimension($FileLocation);
// 		if ($isValidDimension == false)
// 			return false;
		
		### Delete the old photo first
		$SuccessArr['DeleteOldPhoto'] = $this->Delete_Photo();
		
		$fs = new libfilesystem();
		
		# Create folder if not exist
		$folder_prefix = $intranet_root."/file/pos";
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
		$folder_prefix .= "/photo";
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
		$folder_prefix .= "/category";
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
		$original_folder = $folder_prefix."/original";
		if (!file_exists($original_folder)) {
		    $fs->folder_new($original_folder);
		}
		
		# Rename photo file
		$extention = $fs->file_ext($FileName);
		$filename = $this->ObjectID.$extention;
		
		### Store File Extension
		$DataArr = array();
		$DataArr['PhotoExt'] = $extention;
		$SuccessArr['StoreFileExtension'] = $this->Update_Object($DataArr);
		
		### Upload File
		$SuccessArr = array();
		$uploadResult = array();
		list($width, $height) = getimagesize($FileLocation);
// 		if ($width > $this->OriginalPhotoWidthMax || $height > $this->OriginalPhotoHeightMax) {
// 		    if ($width > $this->OriginalPhotoWidthMax){
// 		        $toWidth = $this->OriginalPhotoWidthMax;
// 		    }
// 		    else {
// 		        if ($height>0) {
// 		            $toWidth = floor (($this->OriginalPhotoHeightMax ) * ($width/$height));
// 		        }
// 		        else {
// 		            $toWidth = $width;
// 		        }
// 		    }
// 		    $largeImage = $this->resizeImage($FileLocation, $toWidth);
// 		    $toFile = $original_folder."/".$filename;
// 		    switch($extention){
// 		        case '.jpg':
// 		        case '.jpeg':
// 		            $uploadResult[] = imagejpeg($largeImage, $toFile, 100);
// 		            break;
// 		        case '.gif':
// 		            $uploadResult[] = imagegif($largeImage, $toFile);
// 		            break;
// 		        case '.png':
// 		            $uploadResult[] = imagepng($largeImage, $toFile, 0);
// 		            break;
// 		    }
// 		}
// 		else {
		    $uploadResult[] = $fs->file_copy($FileLocation, $original_folder."/".$filename);
//		}
		
		if ($width > $this->PhotoWidthMax || $height > $this->PhotoHeightMax) {
		    if ($width > $this->PhotoWidthMax){
		        $toWidth = $this->PhotoWidthMax;
		    }
		    else {
		        if ($height>0) {
		            $toWidth = floor (($this->PhotoHeightMax) * ($width/$height));
		        }
		        else {
		            $toWidth = $width;
		        }
		    }
		    $smallImage = $this->resizeImage($FileLocation, $toWidth);
		    $toFile = $folder_prefix."/".$filename;
		    switch($extention){
		        case '.jpg':
		        case '.jpeg':
		            $uploadResult[] = imagejpeg($smallImage, $toFile, 100);
		            break;
		        case '.gif':
		            $uploadResult[] = imagegif($smallImage, $toFile);
		            break;
		        case '.png':
		            $uploadResult[] = imagepng($smallImage, $toFile, 0);
		            break;
		    }
		}
		else {		    
		    $uploadResult[] = $fs->file_copy($FileLocation, $folder_prefix."/".$filename);
		}
		$SuccessArr['UploadPhoto'] = in_array(false,$uploadResult) ? false : true;
		
//		list($originalWidth, $originalHeight) = getimagesize($folder_prefix."/".$filename);
		
		if (in_array(false, $SuccessArr))
			return false;
		else
			return true;
	}
	
	function Check_Valid_Photo_Dimension($FileLocation)
	{
		list($PhotoWidth, $PhotoHeight) = getimagesize($FileLocation);
		
		if ($PhotoWidth > $this->PhotoWidthMax || $PhotoHeight > $this->PhotoHeightMax)
			return 0;
		else
			return 1;
	}
	
	function Delete_Photo()
	{
		global $intranet_root;
		$fs = new libfilesystem();
		
		$SuccessArr = array();
		$fileRemoveArr = array();
		$PhotoPath = $intranet_root.$this->PhotoPath;
		if($this->PhotoPath != '' && file_exists($PhotoPath)) {
		    $fileRemoveArr[] = $fs->file_remove($PhotoPath);
		}
		$OriginalPhotoPath = $intranet_root.$this->OriginalPhotoPath;
		if($this->OriginalPhotoPath != '' && file_exists($OriginalPhotoPath)) {
		    $fileRemoveArr[] = $fs->file_remove($OriginalPhotoPath);
		}
		$SuccessArr['FileRemove'] = in_array(false,$fileRemoveArr) ? false : true;
		
		if ($SuccessArr['FileRemove'] == true)
		{
			$DataArr = array();
			$DataArr['PhotoExt'] = '';
			$SuccessArr['StoreFileExtension'] = $this->Update_Object($DataArr);
		}
		
		if (in_array(false, $SuccessArr))
			return false;
		else
			return true;
	}
	
	function Get_Max_Display_Order()
	{
		$sql = '';
		$sql .= 'SELECT max(Sequence) FROM '.$this->TableName;
		$MaxDisplayOrder = $this->returnVector($sql);
		
		return $MaxDisplayOrder[0];
	}
	
	function Get_Update_Display_Order_Arr($OrderText, $Separator=",")
	{
		if ($OrderText == "")
			return false;
			
		$orderArr = explode($Separator, $OrderText);
		$orderArr = array_remove_empty($orderArr);
		$orderArr = Array_Trim($orderArr);
		
		$numOfOrder = count($orderArr);
		# display order starts from 1
		$counter = 1;
		$newOrderArr = array();
		for ($i=0; $i<$numOfOrder; $i++)
		{
			$thisID = $orderArr[$i];
			if (is_numeric($thisID))
			{
				$newOrderArr[$counter] = $thisID;
				$counter++;
			}
		}
		
		return $newOrderArr;
	}
	
	function Update_DisplayOrder($DisplayOrderArr=array())
	{
		if (count($DisplayOrderArr) == 0)
			return false;
			
		$this->Start_Trans();
		for ($i=1; $i<=sizeof($DisplayOrderArr); $i++) {
			$thisObjectID = $DisplayOrderArr[$i];
			
			$sql = 'UPDATE '.$this->TableName.' SET 
								Sequence = \''.$i.'\' 
							WHERE 
								'.$this->ID_FieldName.' = \''.$thisObjectID.'\'';
			$Result['ReorderResult'.$i] = $this->db_db_query($sql);
		}
		
		if (in_array(false,$Result)) 
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	
	function Get_All_Category($Keyword='', $ActiveOnly=0, $CategoryID='', $ItemKeyword='', $TemplateID="", $inventoryOnly=false)
	{
		$cond_keyword = '';
		if ($Keyword != '')
		{
			if ($ItemKeyword != '')
				$cond_keyword = " And (pic.CategoryName Like '%".$Keyword."%' Or pic.CategoryCode Like '%".$Keyword."%' Or pi.ItemName Like '%".$ItemKeyword."%' Or pi.Barcode Like '%".$ItemKeyword."%') ";
			else
				$cond_keyword = " And (pic.CategoryName Like '%".$Keyword."%' Or pic.CategoryCode Like '%".$Keyword."%') ";
		}
		
		$cond_recordstatus = '';
		if ($ActiveOnly == 1)
			$cond_recordstatus = " And pic.RecordStatus = 1 ";
			
		$cond_categoryid = '';
		if ($CategoryID != '')
			$cond_categoryid = " And pic.CategoryID = '".$CategoryID."' ";
		
		if ($TemplateID != "") {
			$cond_terminal = 'LEFT JOIN 
												POS_TERMINAL_CATEGORY_LINKAGE prcl 
												on pic.CategoryID = prcl.CategoryID 
													and 
													prcl.TemplateID = \''.$TemplateID.'\'';
			$RecordStatusField = 'CASE 
															WHEN prcl.CategoryID IS NULL THEN \'0\' 
															ELSE \'1\' 
														END as RecordStatus
													 ';
															
		}
		else {
			$RecordStatusField = 'pic.RecordStatus';
		}

		$cond_inventry = '';
		if ($inventoryOnly) {
		    $cond_inventry = " and pi.ItemCount>0 ";
        }

		$sql = "Select
						Distinct(pic.CategoryID),
						pic.CategoryCode,
						pic.CategoryName,
						pic.PhotoExt,
						pic.Sequence,
						$RecordStatusField,
						pic.DateInput,
						pic.InputBy,
						pic.InputBy,
						pic.ModifyBy
				From
						POS_ITEM_CATEGORY as pic
						Left Outer Join
						POS_ITEM as pi
						On (pic.CategoryID = pi.CategoryID) 
						$cond_terminal
				Where
						1
						$cond_keyword
						$cond_recordstatus
						$cond_categoryid
						$cond_inventry
				Order By
						pic.Sequence
				";
		//debug_r($sql);
		$CategoryArr = $this->returnArray($sql);
		
		return $CategoryArr;
	}
	
	function Get_All_Items($ActiveOnly=0, $Keyword='')
	{
		$cond_recordstatus = '';
		if ($ActiveOnly == 1)
			$cond_recordstatus = " And RecordStatus = 1 ";
		
		$cond_keyword = '';
		if ($Keyword != '')
			$cond_keyword = " And (ItemName Like '%".$Keyword."%' Or Barcode Like '%".$Keyword."%') ";
		
		$sql = "Select
						*
				From
						POS_ITEM
				Where
						CategoryID = '".$this->ObjectID."'
						$cond_recordstatus
						$cond_keyword
				Order By
						Sequence
				";
		$ItemArr = $this->returnArray($sql);
		
		return $ItemArr;
	}
	
	function Get_RecordStatus_Count($TemplateID="")
	{
		if ($TemplateID == "") {
			// count enabled
			$sql = "Select
							Count(CategoryID)
					From
							".$this->TableName." 
					Where
							RecordStatus = '1'
					";
			$CountArr = $this->returnVector($sql);
			$ReturnArr['Enabled'] = $CountArr[0];
			
			// count disabled
			$sql = "Select
							Count(CategoryID)
					From
							".$this->TableName."
					Where
							RecordStatus = '0' Or RecordStatus Is NULL
					";
			$CountArr = $this->returnVector($sql);
			$ReturnArr['Disabled'] = $CountArr[0];
		}
		else {
			// get total cat
			$sql = 'select 
								count(1) 
							from 
								'.$this->TableName.'';
			$Total = $this->returnVector($sql);
			$Total = $Total[0];
			
			// get active category
			$sql = 'select 
								count(1) 
							from 
								'.$this->TableName.' as pic 
								inner join 
								POS_TERMINAL_CATEGORY_LINKAGE as ptcl 
								on pic.CategoryID = ptcl.CategoryID 
									and 
									TemplateID = \''.$TemplateID.'\'';
			//debug_r($sql);
			$Active = $this->returnVector($sql);
			$Active = $Active[0];
			
			$ReturnArr['Enabled'] = $Active;
			$ReturnArr['Disabled'] = $Total-$Active;
		}
		
		return $ReturnArr;
	}
	
	/* if($CountCategoryOnly)
	 *   return array[CategoryID][ItemID] = Number of log records
	 * else 
	 *   return array[CategoryID] = Number of log records of items under the category
	 */
	function Get_Category_Item_Log_Counting($CategoryIdAry=array(), $CountCategoryOnly=false)
	{
		$cond_category = "";
		if(count($CategoryIdAry) > 0){
			$cond_category .= " AND c.CategoryID IN (".implode(",",(array)$CategoryIdAry).") ";
		}
		
		$sql = "SELECT 
					c.CategoryID, 
					b.ItemID,
					COUNT(a.ItemLogID) as LogCount 
				FROM POS_ITEM_CATEGORY as c 
				LEFT JOIN POS_ITEM as b ON b.CategoryID=c.CategoryID 
				LEFT JOIN POS_ITEM_LOG as a ON a.ItemID=b.ItemID AND a.LogType IN (1,2) 
				WHERE 1 $cond_category
				GROUP BY b.ItemID ";
		$records = $this->returnArray($sql);
		$record_count = count($records);
		$returnAry = array();
		
		if($CountCategoryOnly){
			for($i=0;$i<$record_count;$i++){
				list($category_id, $item_id, $log_count) = $records[$i];
				if(!isset($returnAry[$category_id])){
					$returnAry[$category_id] = 0;
				}
				$returnAry[$category_id] += $log_count;
			}
		}else{
			for($i=0;$i<$record_count;$i++){
				list($category_id, $item_id, $log_count) = $records[$i];
				$item_id = trim($item_id);
				if(!isset($returnAry[$category_id])){
					$returnAry[$category_id] = array();
				}
				if($item_id == '') continue;
				$returnAry[$category_id][$item_id] = $log_count;
			}
		}
		return $returnAry;
	}
	
	function Permanent_Delete_Category()
	{
		global $intranet_root;
		include_once($intranet_root."/includes/libpos_item.php");
		$LogCount = $this->Get_Category_Item_Log_Counting(array($this->CategoryID), true);
		
		if($LogCount[$this->CategoryID] > 0){
			return false;
		}
		
		$result = array();
		$result['DeletePhoto'] = $this->Delete_Photo();
		
		$items_ary = $this->Get_All_Items();
		$item_count = count($items_ary);
		
		for($i=0;$i<$item_count;$i++) {
			$itemObj = new POS_Item($items_ary[$i]['ItemID']);
			$result['DelteItem_'.$items_ary[$i]['ItemID']] = $itemObj->Permanent_Delete_Item();
		}
		
		$sql = "DELETE FROM POS_ITEM_CATEGORY WHERE CategoryID='".$this->CategoryID."'";
		$result['DeleteCategory'] = $this->db_db_query($sql);
		
		return !in_array(false,$result);
	}
	
	function resizeImage($img_path, $toWidth, $maxSize=1000)
	{
	    list($width, $height, $img_type) = getimagesize($img_path);
	    
	    $imgFormatNotMatch = false;
	    switch ($img_type) {
	        case 1:     // IMAGETYPE_GIF
	            $image = imagecreatefromgif($img_path);
	            break;
	        case 2:     // IMAGETYPE_JPEG
	            $image = imagecreatefromjpeg($img_path);
	            break;
	        case 3:     // IMAGETYPE_PNG
	            $image = imagecreatefrompng($img_path);
	            break;
	        default:
	            $imgFormatNotMatch = true;
	    }
	    
	    if ($image){
	        
	        $exif = @exif_read_data($img_path);
	        
	        //orientation handling [Start]
	        if (!empty($exif['Orientation'])) {
	            switch ($exif['Orientation']) {
	                case 3:
	                    $image = imagerotate($image, 180, 0);
	                    break;
	                    
	                case 6:
	                    $image = imagerotate($image, -90, 0);
	                    $temp_width = $width;
	                    $width = $height;
	                    $height = $temp_width;
	                    break;
	                    
	                case 8:
	                    $image = imagerotate($image, 90, 0);
	                    $temp_width = $width;
	                    $width = $height;
	                    $height = $temp_width;
	                    break;
	            }
	        }
	        //orientation handling [End]
	        
	        if ($height>0) {
	            $sr = $width / $height;
	        }
	        if ($sr>0) {
	            $toHeight = floor($toWidth / $sr);
	        }
	        if ($toWidth <= $maxSize) {
	            $rw = $toWidth;
	            $rh = $toHeight;
	        } else if ($sr < 1) {
	            $rw = $maxSize;
	            $rh = floor($maxSize / $sr);
	        } else {
	            $rw = floor($maxSize * $sr);
	            $rh = $maxSize;
	        }
	        
	        $rimage = imagecreatetruecolor($rw, $rh);
	        
	        imagecopyresampled($rimage, $image, 0, 0, 0, 0, $rw, $rh, $width, $height);
	        imagedestroy($image);
	        
	        return $rimage;	        
	        
	    }
	}
}

?>