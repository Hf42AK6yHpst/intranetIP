<?php
class libslslib2007 extends libdb {
	
	/*
	* Initialize
	*/
	function libslslib2007() {
	
		$this->libdb();
	}
	
	
	function displayTable($SpecialArr) {
		global $i_no_record_exists_msg, $i_frontpage_eclass_courses, $i_frontpage_eclass_lastlogin, $i_frontpage_eclass_whatsnews;
		global $i_status_suspended, $image_path, $LAYOUT_SKIN;

		$x .= "<table width=\"50%\" border=\"0\" cellspacing=\"0\" cellpadding=\"8\" align=\"center\">";		
		if(sizeof($SpecialArr)==0)
		{
			$x .= "
					<tr>
						<td width=\"100%\" class=\"indextabclassiconoff\" align='center'><br /><br /><br /><br /><br />$i_no_record_exists_msg</td>
					</tr>
					";			
		}
		else
		{
			
			for ($i = 0; $i < sizeof($SpecialArr); $i++) {				
				
				$course_link = $SpecialArr[$i][0];
				$course_name = $SpecialArr[$i][1];
							
				$class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_class.gif\" >";
				$RmTitle = "<a href=\"".$course_link."\" class=\"tabletoplink\">".$course_name."</a>";
                
				$x .= "<tr>\n";                
				$x .= "<td align='center' width='50%' valign='top'>". $this->MyeClassTable($class_icon, $RmTitle) ."</td>";
				$x .= "</tr>\n";
			}
		}       
		$x .= "</table>";         
		return $x;
     }
	
     
     function MyeClassTable($class_icon, $RmTitle) {
		global $image_path, $LAYOUT_SKIN;
                
		$x = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
		$x .= "<tr>";
		$x .= "<td width='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board01.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='26'></td>";
		$x .= "<td background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board02.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>";
		$x .= "<tr>";
		$x .= "<td width='19'>$class_icon</td>";
		$x .= "<td>$RmTitle</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "<td width='12' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board03.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='12' height='26'></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board04.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board04.gif' width='10' height='11'></td>";
		$x .= "<td background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board05.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>";
		$x .= "<tr>";
		$x .= "<td>";
		$x .= $whatsnew;
		$x .="</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td align='right' class='tabletext'>$lastlogin</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "<td width='12' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board06.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board06.gif' width='12' height='12'></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width='10' height='10'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board07.gif' width='10' height='10'></td>";
		$x .= "<td height='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board08.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board08.gif' width='10' height='10'></td>";
		$x .= "<td width='12' height='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board09.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board09.gif' width='12' height='10'></td>";
		$x .= "</tr>";
		$x .= "</table>";
                
		return $x;        
	}
     
}
?>