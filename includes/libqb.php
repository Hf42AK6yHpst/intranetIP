<?php
if (!defined("LIBQB_DEFINED"))                     // Preprocessor directive
{
define("LIBQB_DEFINED", true);

  class libqb extends libdb {

        var $settingTable;
        var $linkname;
        var $tableTitle;
        var $directoryNames;
        var $engName;
        var $chiName;
        var $displayOrder;
        var $AccessList;
        var $QuestionType;
        var $GroupListRetrieved;

        var $questionID;
        var $questionCode;
        var $type;
        var $levelID;
        var $categoryID;
        var $difficultyID;
        var $lang;
        var $questionEng;
        var $questionChi;
        var $answerEng;
        var $answerChi;
        var $originalFileEng;
        var $originalFileChi;
        var $ownerID;
        var $relatedGroupID;
        var $bulletinID;
        var $recordStatus;
        var $dateInput;
        var $lastModified;
        var $lastAdminModified;
        var $downloadedCount;
        var $score;

        function libqb($qid="")
        {
                 global $intranet_root;
                 $this->libdb();
                 $this->settingTable = array ("", "QB_CATEGORY","QB_LEVEL","QB_DIFFICULTY");
                 $this->linkname = array ("","cat","lvl","diff");
                 $this->questionLink = array("","CategoryID","LevelID","DifficultyID");
                 global $i_QB_Category,$i_QB_Level,$i_QB_Difficulty,$i_QB_MC,$i_QB_ShortQ,$i_QB_FillIn,$i_QB_Matching,$i_QB_TandF;
                 $this->tableTitle = array ("",$i_QB_Category,$i_QB_Level,$i_QB_Difficulty);
                 $this->AccessList = array();
                 $this->GroupListRetrieved = false;
                 $file_content = get_file_content("$intranet_root/file/qb_questionlist.txt");
                 if ($file_content == "")
                     $this->QuestionType = array($i_QB_MC,$i_QB_ShortQ,$i_QB_FillIn,$i_QB_Matching,$i_QB_TandF);
                 else
                 {
                     $qType = array($i_QB_MC,$i_QB_ShortQ,$i_QB_FillIn,$i_QB_Matching,$i_QB_TandF);
                     $this->QuestionType = array("","","","","");
                     $questions_type = explode(",",chop($file_content));

                     for ($i=0; $i<sizeof($questions_type); $i++)
                     {
                          $pos = intval ($questions_type[$i]);
                          $this->QuestionType[$pos] = $qType[$pos];
                     }

                 }
                 $this->directoryNames = array("multiplechoice","shortq","fillin","matching","tandf");
                 if ($qid != "")
                 {
                     $this->retrieveQuestion($qid);
                 }
        }
                 # For YLS only now
                function export2eClass($qids, $course_id)
                {
                        global $eclass_prefix;

                        if (!is_array($qids))
                        {
                                return;
                        }

                        $sQid = implode("','", $qids);
                        $ecDB = $eclass_prefix."c$course_id";

                        // retrieve data
                        $sql = "SELECT q.QuestionID, l.EngName, l.ChiName,
                                                c.EngName, c.ChiName, d.EngName, d.ChiName, g.Title,
                                                q.QuestionEng, q.QuestionChi, q.AnswerEng, q.AnswerChi,
                                                q.QuestionType, q.QuestionCode, q.DateModified
                                                FROM QB_QUESTION as q
                                                LEFT OUTER JOIN QB_LEVEL as l ON q.LevelID = l.UniqueID
                                                LEFT OUTER JOIN QB_CATEGORY as c ON q.CategoryID = c.UniqueID
                                                LEFT OUTER JOIN QB_DIFFICULTY as d ON q.DifficultyID = d.UniqueID
                                                LEFT OUTER JOIN INTRANET_GROUP as g ON q.RelatedGroupID = g.GroupID
                                                WHERE q.QuestionID IN ('$sQid')
                                                ";
                        $row = $this->returnArray($sql, 15);

                        // import to eClass
                        $field_name = "INTRANET_QID, LevelEng, LevelChi, CategoryEng, CategoryChi, DifficultyEng, DifficultyChi, ";
                        $field_name .= "RelatedGroup, QuestionEng, QuestionChi, AnswerEng, AnswerChi, ";
                        $field_name .= "qtype, QuestionCode, inputdate, modified";
                        for ($i=0; $i<sizeof($row); $i++)
                        {
                                $field_value = "'{$row[$i][0]}', '".addslashes($row[$i][1])."', '".addslashes($row[$i][2])."', ";
                                $field_value .= "'".addslashes($row[$i][3])."', '".addslashes($row[$i][4])."', ";
                                $field_value .= "'".addslashes($row[$i][5])."', '".addslashes($row[$i][6])."', ";
                                $field_value .= "'".addslashes($row[$i][7])."', '".addslashes($row[$i][8])."', ";
                                $field_value .= "'".addslashes($row[$i][9])."', '".addslashes($row[$i][10])."', ";
                                $field_value .= "'".addslashes($row[$i][11])."', '".addslashes($row[$i][12])."', '".addslashes($row[$i][13])."',";
                                $field_value .= "'{$row[$i][14]}', '{$row[$i][14]}' ";
                                $sql = "INSERT INTO {$ecDB}.question ($field_name) values ($field_value)";
                                $this->db_db_query($sql);
                        }

                        return;
                }

        function retrieveQuestion($qid)
        {
                 global $intranet_session_language;
                 $qb_name = $intranet_session_language=="en"? "EngName": "ChiName";
                 //$username_field = getNameFieldByLang("e.");
                 $fields = "a.QuestionCode, a.LevelID, a.CategoryID, a.DifficultyID";
                 $fields.= ",a.Lang, a.QuestionEng,a.QuestionChi,a.AnswerEng, a.AnswerChi, a.OriginalFileEng,a.OriginalFileChi";
                 $fields.= ",a.QuestionType,a.OwnerIntranetID,a.RelatedGroupID,a.BulletinID,a.RecordStatus";
                 $fields.= ",a.DateInput,a.DateModified,a.DateLastAdminAction";
                 $fields.= ",a.DownloadedCount, a.Score";
                 $db_tables = "QB_QUESTION as a";
                 $conds = "a.QuestionID = '$qid'";
                 $sql = "SELECT $fields FROM $db_tables WHERE $conds";
                 $result = $this->returnArray($sql,21);
                 $this->questionID = $qid;
                 $this->questionCode = $result[0][0];
                 $this->levelID = $result[0][1];
                 $this->categoryID = $result[0][2];
                 $this->difficultyID = $result[0][3];
                 $this->lang = $result[0][4];
                 $this->questionEng = $result[0][5];
                 $this->questionChi = $result[0][6];
                 $this->answerEng = $result[0][7];
                 $this->answerChi = $result[0][8];
                 $this->originalFileEng = $result[0][9];
                 $this->originalFileChi = $result[0][10];
                 $this->type = $result[0][11];
                 $this->ownerID = $result[0][12];
                 $this->relatedGroupID = $result[0][13];
                 $this->bulletinID = $result[0][14];
                 $this->recordStatus = $result[0][15];
                 $this->dateInput = $result[0][16];
                 $this->lastModified = $result[0][17];
                 $this->lastAdminModified = $result[0][18];
                 $this->downloadedCount = $result[0][19];
                 $this->score = $result[0][20];
        }
/*
        function retrieveAccessListFromFile()
        {
                 global $plugin,$qb_access_list_file;
                 if (isset($plugin['QB']) && $plugin['QB'])
                 {
                     $access_list = get_file_content($qb_access_list_file);
                     if ($access_list=="")
                         $access_list = "0";
                     $this->AccessList = explode(",",$access_list);
                 }
                 else
                 {
                     $this->AccessList = array(0);
                 }
        }
*/
        function retrieveQBAccessGroupList()
        {
                 global $plugin,$qb_access_list_file;
                 if (isset($plugin['QB']) && $plugin['QB'])
                 {
                     # Bit-6 is Question bank
                     $sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordType = 2 AND (FunctionAccess IS NULL OR SUBSTRING(REVERSE(BIN(FunctionAccess)),6,1)='1')";
                     $access_list = $this->returnVector($sql);
                     $this->AccessList = $access_list;
                 }
                 else
                 {
                     $this->AccessList = array(0);
                 }

                 $this->GroupListRetrieved = true;

        }
        function getAccessList ()
        {
                 if (!$this->GroupListRetrieved)
                 {
                     $this->retrieveQBAccessGroupList();
                 }
                 return $this->AccessList;
        }

        function isInAccessList ($GroupID)
        {
                 if (sizeof($this->AccessList)==0)
                 {
                     $this->retrieveQBAccessGroupList();
                 }
                 return in_array($GroupID,$this->AccessList);
        }

        function returnGroupID ($uniqueID, $type)
        {
                 $sql = "SELECT RelatedGroupID FROM ".$this->settingTable[$type]." WHERE UniqueID = '$uniqueID'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }

        function returnOwnerName ($ownerID="")
        {
                 if ($ownerID == "")
                 {
                     $ownerID = $this->ownerID;
                 }
                 $sql = "SELECT ".getNameFieldByLang()." FROM INTRANET_USER WHERE UserID = '$ownerID'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }

        function retrieveData ($uniqueID, $type)
        {
                 $sql = "SELECT EngName, ChiName, DisplayOrder FROM ".$this->settingTable[$type]." WHERE UniqueID = '$uniqueID'";
                 $result = $this->returnArray($sql,3);
                 if (sizeof($result)!=0)
                 {
                     list($this->engName,$this->chiName,$this->displayOrder) = $result[0];
                 }
                 else
                 {
                     $this->engName = "Error";
                     $this->chiName = "Error";
                     $this->displayOrder = 100;
                 }
        }

        function returnData ($uniqueID, $type)
        {
                 $sql = "SELECT EngName, ChiName, DisplayOrder FROM ".$this->settingTable[$type]." WHERE UniqueID = '$uniqueID'";
                 return $this->returnArray($sql,3);
        }

        function returnCategory($catID)
        {
                 return $this->returnData($catID, 1);
        }

        function returnLevel ($lvlID)
        {
                 return $this->returnData($lvlID, 2);
        }

        function returnDifficulty($diffID)
        {
                 return $this->returnData($diffID,3);
        }

        # /admin/qb/index.php
        function displayQBGroupSelection()
        {
                 global $i_GroupRole, $i_QB_AccessGroup, $button_add, $button_remove;
                 $current_access_list = implode("','",$this->getAccessList());

                 $sql = "SELECT GroupID, Title, IF (GroupID IN ('$current_access_list'),1,0),RecordType FROM INTRANET_GROUP WHERE RecordType = 2 ORDER BY Title";
                 $row = $this->returnArray($sql,4);
                 $x .= "<table width=100% border=0 cellpadding=10 cellspacing=0>\n";
                 $x .= "<tr><td class=tableContent colspan=3>$i_QB_AccessGroup:</td></tr>\n";
                 $x .= "<tr>\n";
                 $x .= "<td class=tableContent width=50%>\n";
                 $x .= "<select name=GroupID[] size=10 multiple>\n";
                 for($i=0; $i<sizeof($row); $i++)
                 {
                     $GroupID = $row[$i][0];
                     $Title = $row[$i][1];
                     $IsCheck = ($row[$i][2] > 0) ? 1 : 0;
                     $RecordType = $row[$i][3];
                     $RecordTypeName = $i_GroupRole[$RecordType];
                     $x .= ($IsCheck) ? "<option value=$GroupID>$Title</option>\n" : "";
                 }
                 $x .= "<option>\n";
                 for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
                 $x .= "</option>\n";
                 $x .= "</select>\n";
                 $x .= "</td>\n";
                 $x .= "<td class=tableContent align=center>\n";
                 $x .= "<p><input class=button type=button value=\"&lt;&lt; $button_add\" onClick=checkOptionTransfer(this.form.elements[\"AvailableGroupID[]\"],this.form.elements[\"GroupID[]\"])></p>\n";
                 $x .= "<p><input class=button type=button value=\"$button_remove &gt;&gt;\" onClick=checkOptionTransfer(this.form.elements[\"GroupID[]\"],this.form.elements[\"AvailableGroupID[]\"])></p>\n";
                 $x .= "</td>\n";
                 $x .= "<td class=tableContent width=50%>\n";
                 $x .= "<select name=AvailableGroupID[] size=10 multiple>\n";
                 for($i=0; $i<sizeof($row); $i++)
                 {
                     $GroupID = $row[$i][0];
                     $Title = $row[$i][1];
                     $IsCheck = ($row[$i][2] > 0) ? 1 : 0;
                     $RecordType = $row[$i][3];
                     $RecordTypeName = $i_GroupRole[$RecordType];
                     $x .= ($IsCheck) ? "" : "<option value=$GroupID>$Title</option>\n";
                 }
                 $x .= "<option>\n";
                 for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
                 $x .= "</option>\n";
                 $x .= "</select>\n";
                 $x .= "</td>\n";
                 $x .= "</tr>\n";
                 $x .= "</table>\n";
                 return $x;
        }

        # /admin/qb/index.php
        function displayQBLinkToEdit ()
        {
                 global $i_QB_NoGroupUsing, $i_QB_ModifySetting;
                 $access_list = implode("','",$this->getAccessList());
                 $x .= "<table width=100% border=0 cellpadding=10 cellspacing=0>\n";
                 $x .= "<tr><td class=tableContent>$i_QB_ModifySetting</td></tr>\n";
                 $x .= "<tr><td >\n";
                 if ($access_list == "0")
                 {
                     $x .= "$i_QB_NoGroupUsing\n";
                 }
                 else
                 {
                     $sql = "SELECT GroupID, Title FROM INTRANET_GROUP WHERE GroupID IN ('$access_list') AND RecordType = 2 ORDER BY Title";
                     $result = $this->returnArray($sql,2);
                     $x .= "<ul>\n";
                     for ($i=0; $i<sizeof($result); $i++)
                     {
                          list($id, $name) = $result[$i];
                          $count = $i+1;
                                                  $qbA[] = Array("viewgroup.php?GroupID=$id", $name);
                          $x .= "<li ><a class=functionlink href=viewgroup.php?GroupID=$id>$name</a></li>\n";
                     }
                 }
                 $x .= "</ul>\n";
                 $x .= "</td></tr></table>\n";
                 return $qbA;
        }

        function displayTable($gid,$type)
        {
                 global $button_new,$button_import,$button_export,$i_QB_EngName,$i_QB_ChiName,$i_QB_DisplayOrder,$i_no_record_exists_msg,$button_edit,$button_remove,$i_status_all;
                                 global $intranet_session_language;
                 $sql = "SELECT EngName, ChiName, DisplayOrder,
                         CONCAT('<input type=checkbox name=uniqueID[] value=',UniqueID,'>')
                         FROM ".$this->settingTable[$type]."
                         WHERE RelatedGroupID = '$gid' ORDER BY DisplayOrder";
                 //echo $sql;

                 $result = $this->returnArray($sql,4);
                 $toolbar = "<a class=iconLink href=\"new.php?type=$type&GroupID=$gid\">".newIcon()."$button_new</a>";
                 $toolbar .= " <a class=iconLink href=\"import.php?type=$type&GroupID=$gid\">".ImportIcon()."$button_import</a>";
                 $toolbar .= " <a class=iconLink href=\"export.php?type=$type&GroupID=$gid\">".ExportIcon()."$button_export $i_status_all</a>";
                 $functionbar = "<a href=\"javascript:checkEdit(document.form$type,'uniqueID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
                 $functionbar .= "<a href=\"javascript:checkRemove(document.form$type,'uniqueID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
                 $col_title = array($i_QB_EngName,$i_QB_ChiName,$i_QB_DisplayOrder,"<input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'uniqueID[]'):setChecked(0,this.form,'uniqueID[]')>");

                 $x = "<form method=post name=form$type>\n";
                 $x .= "<table width=100% cellspacing=0 cellpadding=0>\n<tr><td colspan=2><span class=extraInfo>[<span class=subTitle>".$this->tableTitle[$type]."</span>]</span></td></tr>\n";
                                 $x .= "<tr><td colspan=2><img src='/images/admin/table_head0.gif' width='560' height='13' border='0'></td></tr>\n";
                 $x .= "<tr><td class=admin_bg_menu align=left>$toolbar</td><td class=admin_bg_menu align=right>$functionbar</td></tr>\n";
                                 $x .= "<tr><td colspan=2><img src='/images/admin/table_head1.gif' width=560 height=7 border=0></td></tr>";
                 $x .= "<tr><td colspan=2>\n";
                 $x .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
                 $x .= "<tr>\n";
                 for ($i=0; $i<sizeof($col_title)-1; $i++)
                 {
                      $x .= "<td class=tableTitle>".$col_title[$i]."</td>\n";
                 }
                 $x .= "<td class=tableTitle width=1>".$col_title[3]."</td>\n";
                 $x .= "</tr>\n";
                 if (sizeof($result) != 0)
                 {
                     for ($i=0; $i<sizeof($result); $i++)
                     {
                          list ($engName, $chiName,$dorder, $checkbox) = $result[$i];
                          $x .= "<tr>";
                          $x .= "<td class=tableContent>$engName</td><td class=tableContent>$chiName</td><td class=tableContent>$dorder</td><td class=tableContent>$checkbox</td></tr>\n";
                     }
                 }
                 else
                 {
                     $x .= "<tr><td colspan=4 align=center>$i_no_record_exists_msg</td></tr>\n";
                 }
                 $x .= "</table></td></tr>\n";
                                 $x .= "<tr><td colspan=2><img src='/images/admin/table_bottom.gif' width='560' height='16' border='0'></td></tr></table>\n";
                 $x .= "<input type=hidden name=type value=$type>\n";
                 $x .= "<input type=hidden name=GroupID value=$gid>\n";
                 $x .= "</form>\n";
                 return $x;
        }

        function displayTableFrontend($gid,$type)
        {
                 global $button_new,$button_import,$button_export,$i_QB_EngName,$i_QB_ChiName,$i_QB_DisplayOrder,$i_no_record_exists_msg,$button_edit,$button_remove,$i_status_all;
                 global $image_edit, $image_delete;
                 $sql = "SELECT EngName, ChiName, DisplayOrder,
                         CONCAT('<input type=checkbox name=uniqueID[] value=',UniqueID,'>')
                         FROM ".$this->settingTable[$type]."
                         WHERE RelatedGroupID = '$gid' ORDER BY DisplayOrder";

                 $result = $this->returnArray($sql,4);
                 $toolbar = "<a href=cat_new.php?type=$type&GroupID=$gid>".newIcon2()."$button_new</a>";
                 $toolbar .= " <a href=cat_import.php?type=$type&GroupID=$gid>".ImportIcon2()."$button_import</a>";
                 $toolbar .= " <a href=cat_export.php?type=$type&GroupID=$gid>".ExportIcon2()."$button_export $i_status_all</a>";
                 $functionbar = "<input type=image src='$image_edit' onClick=\"checkEdit(this.form,'uniqueID[]','cat_edit.php'); return false;\">";
                 $functionbar .= " <input type=image src='$image_delete' onClick=\"checkRemove(this.form,'uniqueID[]','cat_remove.php'); return false;\">";
                 $col_title = array($i_QB_EngName,$i_QB_ChiName,$i_QB_DisplayOrder,"<input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'uniqueID[]'):setChecked(0,this.form,'uniqueID[]')>");

                 $x = "<form method=post name=form$type>\n";
                 $x .= "<table width=100% cellspacing=0 cellpadding=0>\n<tr><td colspan=2>".$this->tableTitle[$type]."</td></tr>\n";
                 $x .= "<tr><td align=left>$toolbar</td><td align=right>$functionbar</td></tr>\n";
                 $x .= "<tr><td colspan=2>\n";
                 $x .= "<table width=100% border=1 cellpadding=5 cellspacing=0 bordercolorlight=#DDEEFA bordercolordark=#AAB9D4 class=body>\n";
                 $x .= "<tr>\n";
                 for ($i=0; $i<sizeof($col_title)-1; $i++)
                 {
                      $x .= "<td class=qb_tableheader>".$col_title[$i]."</td>\n";
                 }
                 $x .= "<td class=qb_tableheader width=1>".$col_title[3]."</td>\n";
                 $x .= "</tr>\n";
                 if (sizeof($result) != 0)
                 {
                     for ($i=0; $i<sizeof($result); $i++)
                     {
                          list ($engName, $chiName,$dorder, $checkbox) = $result[$i];
                          $x .= "<tr>";
                          $x .= "<td>$engName</td><td >$chiName</td><td >$dorder</td><td >$checkbox</td></tr>\n";
                     }
                 }
                 else
                 {
                     $x .= "<tr><td colspan=4 align=center class=tableContent2>$i_no_record_exists_msg</td></tr>\n";
                 }
                 $x .= "</table><td></tr></table>\n";
                 $x .= "<input type=hidden name=type value=$type>\n";
                 $x .= "<input type=hidden name=GroupID value=$gid>\n";
                 $x .= "</form>\n";
                 return $x;
        }
        function displayCategoryTable($gid)
        {
                 return $this->displayTable($gid,1);
        }

        function displayLevelTable($gid)
        {
                 return $this->displayTable($gid,2);
        }
        function displayDifficultyTable($gid)
        {
                 return $this->displayTable($gid,3);
        }

        function displayCategoryTableFrontend($gid)
        {
                 return $this->displayTableFrontend($gid,1);
        }

        function displayLevelTableFrontend($gid)
        {
                 return $this->displayTableFrontend($gid,2);
        }
        function displayDifficultyTableFrontend($gid)
        {
                 return $this->displayTableFrontend($gid,3);
        }

        function newDisplayOrder ($GroupID, $type)
        {
                 $sql = "SELECT MAX(DisplayOrder) FROM ".$this->settingTable[$type]." WHERE RelatedGroupID = '$GroupID'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }

        function returnSelect ($GroupID, $type,$tags,$selected,$all)
        {
                 global $intranet_session_language;
                 $lang_suf = ($intranet_session_language=="en"? "EngName":"ChiName");
                 $sql = "SELECT UniqueID, $lang_suf FROM ".$this->settingTable[$type]." WHERE RelatedGroupID = '$GroupID' ORDER BY DisplayOrder";
                 $result = $this->returnArray($sql,2);
                 $x = "<SELECT $tags>\n";
                 if ($all == -1)      # For type removal
                 {
                     global $i_QB_TypeRemovalDelete;
                     $x .= "<OPTION value=0>$i_QB_TypeRemovalDelete</OPTION>\n";
                 }
                 else if ($all!=0)  # For filtering
                 {
                     global $i_QB_All;
                     $x .= "<OPTION value=0>$i_QB_All</OPTION>\n";
                 }
                 for ($i=0; $i<sizeof($result); $i++)
                 {
                      list($id , $name) = $result[$i];
                      $checked = ($selected == $id? "SELECTED":"");
                      $x .= "<OPTION value=$id $checked>$name</OPTION>\n";
                 }
                 $x .= "</SELECT>\n";
                 return $x;
        }

        function returnSelectLevel($GroupID,$tags,$selected=0,$all=0)
        {
                 return $this->returnSelect($GroupID, 2,$tags,$selected,$all);
        }
        function returnSelectCategory($GroupID,$tags,$selected=0,$all=0)
        {
                 return $this->returnSelect($GroupID, 1,$tags,$selected,$all);
        }
        function returnSelectDifficulty($GroupID,$tags,$selected=0,$all=0)
        {
                 return $this->returnSelect($GroupID, 3,$tags,$selected,$all);
        }

        function returnSelectQuestionType ($tags,$selected=0,$all=0)
        {
                 $x = "<SELECT $tags>\n";
                 if ($all!=0)
                 {
                     global $i_QB_All;
                     $x .= "<OPTION value=-1>$i_QB_All</OPTION>\n";
                 }
                 for ($i=0; $i<sizeof($this->QuestionType); $i++)
                 {
                      if ($this->QuestionType[$i] != "")
                      {
                          $checked = ($selected == $i? "SELECTED":"");
                          $x .= "<OPTION value=$i $checked>".$this->QuestionType[$i]."</OPTION>\n";
                      }
                 }
                 $x .= "</SELECT>\n";
                 return $x;
        }

        function returnSelectOwner($GroupID,$tags,$selected=0,$all=0)
        {
                 $x = "<SELECT $tags>\n";
                 if ($all!=0)
                 {
                     global $i_QB_All;
                     $x .= "<OPTION value=0>$i_QB_All</OPTION>\n";
                 }
                 $username_field = getNameFieldByLang("a.");
                 $sql = "SELECT a.UserID, $username_field FROM INTRANET_USER as a, INTRANET_USERGROUP as b
                         WHERE a.UserID = b.UserID AND b.GroupID = '$GroupID' AND a.RecordStatus = 1 ORDER BY a.EnglishName";
                 $result = $this->returnArray($sql,2);
                 for ($i=0; $i<sizeof($result); $i++)
                 {
                      list ($id, $name) = $result[$i];
                      $checked = ($selected == $id? "SELECTED":"");
                      $x .= "<OPTION value=$id $checked>$name</OPTION>\n";
                 }
                 $x .= "</SELECT>\n";
                 return $x;
        }

        function returnSelectLang($tags,$selected=0)
        {
                 $x = "<SELECT $tags>\n";
                 global $i_QB_LangSelectEnglish,$i_QB_LangSelectChinese,$i_QB_LangSelectBoth;
                 global $i_QB_All;
                 for ($i=0; $i<4; $i++)
                 {
                      $select_tag[$i] = ($selected==$i? " SELECTED":"");
                 }
                 $x .= "<OPTION value=0".$select_tag[0].">$i_QB_All</OPTION>\n";
                 $x .= "<OPTION value=1".$select_tag[1].">$i_QB_LangSelectEnglish</OPTION>\n";
                 $x .= "<OPTION value=2".$select_tag[2].">$i_QB_LangSelectChinese</OPTION>\n";
                 $x .= "<OPTION value=3".$select_tag[3].">$i_QB_LangSelectBoth</OPTION>\n";
                 $x .= "</SELECT>\n";
                 return $x;
        }

        function returnSelectStatus($tags,$selected=0)
        {
                 $x = "<SELECT $tags>\n";
                 global $i_QB_Pending,$i_QB_Approved,$i_QB_Rejected,$i_QB_All;
                 for ($i=0; $i<4; $i++)
                 {
                      $select_tag[$i] = ($selected==$i? " SELECTED":"");
                 }
                 $x .= "<OPTION value=0".$select_tag[0].">$i_QB_All</OPTION>\n";
                 $x .= "<OPTION value=1".$select_tag[1].">$i_QB_Pending</OPTION>\n";
                 $x .= "<OPTION value=2".$select_tag[2].">$i_QB_Approved</OPTION>\n";
                 $x .= "<OPTION value=3".$select_tag[3].">$i_QB_Rejected</OPTION>\n";
                 $x .= "</SELECT>\n";
                 return $x;
        }

        function returnSelectGroup($tags,$selected=0)
        {
                 global $UserID;
                 $sql = "SELECT a.GroupID, a.Title FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b
                         WHERE a.GroupID = b.GroupID AND b.UserID = '$UserID' ORDER BY a.Title";
                 $result = $this->returnArray($sql,2);
                 $x = "<SELECT $tags>\n";
                 for ($i=0; $i<sizeof($result); $i++)
                 {
                      list ($id,$name) = $result[$i];
                      if ($this->isInAccessList($id))
                      {
                          $selected_tag = ($id==$selected? " SELECTED": "");
                          $x .= "<OPTION value=$id$selected_tag>$name</OPTION>\n";
                      }
                 }
                 $x .= "</SELECT>\n";
                 return $x;
        }

        function returnSelectAdminGroup($tags,$selected=0)
        {
                 global $UserID;
                 $conds = "a.GroupID = b.GroupID AND b.UserID = '$UserID' AND b.RecordType = 'A'";
                 $sql = "SELECT a.GroupID, a.Title FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b WHERE $conds ORDER BY a.Title";
                 $result = $this->returnArray($sql,2);
                 $x = "<SELECT $tags>\n";
                 for ($i=0; $i<sizeof($result); $i++)
                 {
                      list ($id,$name) = $result[$i];
                      if ($this->isInAccessList($id))
                      {
                          $selected_tag = ($id==$selected? " SELECTED": "");
                          $x .= "<OPTION value=$id$selected_tag>$name</OPTION>\n";
                      }
                 }
                 $x .= "</SELECT>\n";
                 return $x;
        }

        function returnBulletinUsername()
        {
                 return "Auto-generated";
        }

        function returnBulletinSubject($qcode)
        {
                 return "Review of Question: $qcode";
        }

        function returnBulletinMessage()
        {
                 return "Please feel free to express your opinions for this question.";
        }

        function createReviewThread($qid)
        {
                 global $UserID;    # User who authorize this question
                 $sql = "SELECT QuestionCode, RelatedGroupID, BulletinID FROM QB_QUESTION WHERE QuestionID = '$qid'";
                 $result = $this->returnArray($sql,3);
                 list($qcode,$group,$bid) = $result[0];
                 if ($bid=="" || $bid==0)
                 {
                     $username = $this->returnBulletinUsername();
                     $parentMsgID = 0;
                     $subject = $this->returnBulletinSubject($qcode);
                     $message = $this->returnBulletinMessage();
                     $readFlag = ";$UserID;";
                     $field_name = "ParentID,GroupID,UserID,UserName,Subject,Message,ReadFlag,DateInput,DateModified";
                     $field_value = "'$parentMsgID','$group','$UserID','$username','$subject','$message','$readFlag',now(),now()";
                     $sql = "INSERT INTO INTRANET_BULLETIN ($field_name) VALUES ($field_value)";
                     $this->db_db_query($sql);
                     $bid = $this->db_insert_id();
                     if ($bid != "")
                     {
                         $sql = "UPDATE QB_QUESTION SET BulletinID = '$bid' WHERE QuestionID = '$qid'";
                         $this->db_db_query($sql);
                     }
                 }
        }

        function markQuestionRead ($qid)
        {
                 global $UserID;
                 if (is_array($qid))
                 {
                     $list = implode("','",$qid);
                 }
                 else
                 {
                     $list = $qid;
                 }
                 $sql = "UPDATE QB_QUESTION SET ReadFlag = CONCAT(ReadFlag,';$UserID;')
                         WHERE QuestionID IN ('$list') AND
                         (ReadFlag IS NULL OR LOCATE(';$UserID;',ReadFlag)=0 )";
                 $this->db_db_query($sql);
        }

        function markQuestionReadDownload($qid)
        {
                 global $UserID;
                 if (is_array($qid))
                 {
                     $list = implode("','",$qid);
                 }
                 else
                 {
                     $list = $qid;
                 }
                 $sql = "UPDATE QB_QUESTION SET ReadFlag = CONCAT(ReadFlag,';$UserID;')
                         WHERE QuestionID IN ('$list') AND
                         (ReadFlag IS NULL OR LOCATE(';$UserID;',ReadFlag)=0 )";
                 $this->db_db_query($sql);
                 $sql = "UPDATE QB_QUESTION SET DownloadFlag = CONCAT(DownloadFlag,';$UserID;')
                         WHERE QuestionID IN ('$list') AND
                         (DownloadFlag IS NULL OR LOCATE(';$UserID;',DownloadFlag)=0 )";
                 $this->db_db_query($sql);
        }

        function AddDownloadCount($qid)
        {
                 global $UserID;
                 if (is_array($qid))
                 {
                     $list = implode("','",$qid);
                 }
                 else
                 {
                     $list = $qid;
                 }
                 $sql = "UPDATE QB_QUESTION SET DownloadedCount = DownloadedCount + 1
                         WHERE QuestionID IN ('$list')";
                 $this->db_db_query($sql);
        }
        function markAllQuestionRead ($GroupID, $type)
        {
                 global $UserID;
                 $sql = "UPDATE QB_QUESTION SET ReadFlag = CONCAT(ReadFlag,';$UserID;')
                         WHERE QuestionType = '$type' AND RelatedGroupID = '$GroupID' AND
                         LOCATE(';$UserID;',ReadFlag)=0";
                 $this->db_db_query($sql);
        }

        function markAllQuestionReadDownload ($GroupID, $type)
        {
                 global $UserID;
                 $sql = "UPDATE QB_QUESTION SET ReadFlag = CONCAT(ReadFlag,';$UserID;')
                         WHERE QuestionType = '$type' AND RelatedGroupID = '$GroupID' AND
                         LOCATE(';$UserID;',ReadFlag)=0";
                 $this->db_db_query($sql);

                 $sql = "UPDATE QB_QUESTION SET DownloadFlag = CONCAT(DownloadFlag,';$UserID;')
                         WHERE QuestionType = '$type' AND RelatedGroupID = '$GroupID' AND
                         LOCATE(';$UserID;',DownloadFlag)=0";
                 $this->db_db_query($sql);
        }
        function addUserScore($uid, $score)
        {
                 $sql = "UPDATE QB_USER_SCORE SET Score = Score + $score WHERE UserID = '$uid'";
                 $this->db_db_query($sql);
                 if ($this->db_affected_rows()==0 && $score != 0)
                 {
                     # No record at this moment
                     $sql = "INSERT INTO QB_USER_SCORE (UserID, Score) VALUES ('$uid', '$score')";
                     $this->db_db_query($sql);
                 }
        }
        function deductUserScore($uid,$score)
        {
                 $sql = "UPDATE QB_USER_SCORE SET Score = Score - $score WHERE UserID = '$uid'";
                 $this->db_db_query($sql);
        }
        function returnUserScore($uid)
        {
                 $sql = "SELECT Score FROM QB_USER_SCORE WHERE UserID = '$uid'";
                 $result = $this->returnVector($sql);
                 return $result[0]+0;
        }

        function returnAllPublicQuestionIDs($GroupID, $type)
        {
                 $sql = "SELECT QuestionID FROM QB_QUESTION WHERE
                         QuestionType = '$type' AND RelatedGroupID = '$GroupID'
                         AND RecordStatus = 1";
                 return $this->returnVector($sql);
        }

        function returnSelectEclassAsT($tags, $useremail)
        {
                 global $intranet_root;
                 global $i_QB_SameServer;
//                 include_once("$intranet_root/includes/libeclass.php");
                 $a = new libeclass();
                 $course_list = $a->returnEClassUserIDCourseID($useremail);
                 /*
                 a.course_id, a.course_code, a.course_name, ";
                 b.user_id, b.memberType, b.lastused, b.user_course_id ";
                 */
                 if (sizeof($course_list)==0) return "";
                 $x = "<SELECT $tags>\n";
                 for ($i=0; $i<sizeof($course_list); $i++)
                 {
                      list ($cid, $ccode, $cname, $cuid, $cmemType, $cLastUsed, $cusercourseid) = $course_list[$i];
                      if ($cmemType == "T")
                      {
                          $x .= "<OPTION value=$cid>$ccode - $cname ($i_QB_SameServer)</OPTION>\n";
                      }
                 }
                 $x .= "</SELECT>\n";
                 return $x;
        }

        function startBucket()
        {
                 session_register("QuestionsInSession");
                 session_register("NumQsInSession");
        }

        function checkBucket()
        {
                 if(!session_is_registered("QuestionsInSession"))
                 {
                     $this->initBucket();
                 }
        }


        function emptyBucket($type)
        {
                 global $QuestionsInSession;
                 global $NumQsInSession;
                 $QuestionsInSession[$type] = array();
                 $NumQsInSession[$type] = 0;
        }

        function initBucket()
        {
                 $this->startBucket();
                 global $QuestionsInSession;
                 global $NumQsInSession;
                 $QuestionsInSession = array();
                 $NumQsInSession = array();
                 for ($i=0; $i < sizeof($this->QuestionType); $i++)
                 {
                      $this->emptyBucket($i);
                 }
        }

        function returnQtyInBucket($type="")
        {
                 $this->checkBucket();
                 global $NumQsInSession;
                 if ($type != "")
                 {
                     return $NumQsInSession[$type];
                 }
                 else
                 {
                     return $NumQsInSession;
                 }
        }
        function returnScoreInBucket($type="")
        {
                 $this->checkBucket();
                 global $QuestionsInSession;

                 if ($type == "")
                 {
                     $qlist = "";
                     $delimiter = "";
                     for ($i=0; $i<sizeof($QuestionsInSession); $i++)
                     {
                          if (sizeof($QuestionsInSession[$i])!=0)
                          {
                              $qlist .= "$delimiter ".implode(",",$QuestionsInSession[$i]);
                              $delimiter = ",";
                          }
                     }
                 }
                 else
                 {
                     if (sizeof($QuestionsInSession[$i])!=0)
                     {
                         $qlist = implode(",",$QuestionsInSession[$type]);
                     }
                 }
                 if ($qlist == "") $qlist = "0";
                 $qlist = "'".implode("','",explode(",",$qlist))."'";
                 global $qb_deduct_score,$UserID;
                 list ($mc,$sq,$fitb,$match,$tf) = $qb_deduct_score;
                 $mc += 0;
                 $sq += 0;
                 $fitb += 0;
                 $match += 0;
                 $tf += 0;
                 $sql = "SELECT SUM(
                        CASE QuestionType
                             WHEN 0 THEN $mc
                             WHEN 1 THEN $sq
                             WHEN 2 THEN $fitb
                             WHEN 3 THEN $match
                             WHEN 4 THEN $tf END
                        )  FROM QB_QUESTION WHERE QuestionID IN ($qlist)";
                 if ($UserID != "")
                 {
                     $sql .= " AND OwnerIntranetID != '$UserID'";
                 }
                 #echo $sql;
                 $result = $this->returnVector($sql);
                 return $result[0]+0;
        }

        function returnBucket($type="")
        {
                 $this->checkBucket();
                 global $NumQsInSession, $QuestionsInSession;
                 if ($type=="") return $QuestionsInSession;
                 else return $QuestionsInSession[$type];
        }

        function addToBucket($type, $qids)
        {
                 $this->checkBucket();
                 global $NumQsInSession,$QuestionsInSession;

                 for ($i=0; $i<sizeof($qids); $i++)
                 {
                      if (!in_array($qids[$i],$QuestionsInSession[$type]))
                      {
                           $QuestionsInSession[$type][$NumQsInSession[$type]] = $qids[$i];
                           $NumQsInSession[$type]++;
                      }
                 }
        }

        function getOriginals($qid)
        {
                 if (sizeof($qid)==0) return array();

                 $list = implode("','",$qid);
                 $sql = "SELECT QuestionID, OriginalFileEng, OriginalFileChi FROM QB_QUESTION WHERE QuestionID IN ('$list')";
                 return $this->returnArray($sql,3);
        }

        function parseQuestion ($question)
        {
                 $separator = "###";
                 /*
                 $pos = strpos($question,$separator);
                 $q = substr($question,0,$pos);
                 $pos2 = strpos($question,$separator,$pos+1);
                 $answerStart = $pos+strlen($separator);
                 $a = substr($question,$answerStart,$pos2-$answerStart);

                 return array($q,$a);
                 */
                 return split($separator,$question);
        }

        function parseMCQuestion ($question)
        {
                 $option_separator = "===@@@===";
                 return split($option_separator,$question);
        }

        function display()
        {
                 global $i_QB_Category,$i_QB_Level,$i_QB_Difficulty,$i_QB_Pending
                 ,$i_QB_Approved,$i_QB_Rejected, $i_QB_Question, $i_QB_Status, $i_QB_LangVer
                 ,$i_QB_EngOnly,$i_QB_ChiOnly,$i_QB_BothLang,$i_QB_LastModified
                 ,$i_QB_QuestionCode,$i_QB_Owner,$i_QB_QuestionType,$i_QB_LastModified
                 ,$i_QB_DateSubmission,$i_QB_EngVer,$i_QB_ChiVer,$i_QB_Answer,$i_QB_AnswerOption;
                 global $intranet_session_language;

                 $dataLvl = $this->returnLevel($this->levelID);
                 $dataCat = $this->returnCategory($this->categoryID);
                 $dataDiff = $this->returnDifficulty($this->difficultyID);
                 $lang_field = ($intranet_session_language=="en"? 0: 1);
                 $status_array = array($i_QB_Pending,$i_QB_Approved, $i_QB_Rejected);
                 /*
                 if ($this->questionEng != "")
                     $parsedEngQ = $this->parseQuestion($this->questionEng);
                 if ($this->questionChi != "")
                     $parsedChiQ = $this->parseQuestion($this->questionChi);
                     */

                 $option_first_letter = 65;
                 if ($this->questionEng != "")
                 {
                     if ($this->type == 0)
                     {
                         # Parse MC Answer Options
                         $parsedMCQ = $this->parseMCQuestion($this->questionEng);
                         $formatedQEng = $parsedMCQ[0]."\n$i_QB_AnswerOption\n";

                         for ($i=1; $i<sizeof($parsedMCQ); $i++)
                         {
                              $option = chr($option_first_letter + $i - 1);
                              $formatedQEng .= "$option. ".$parsedMCQ[$i];
                         }
                     }
                     else
                     {
                         $formatedQEng = $this->questionEng;
                     }
                     $formatedQEng = nl2br($formatedQEng)."<br>\n $i_QB_Answer:".$this->answerEng;
                 }
                 else
                 {
                     $formatedQEng = "-";
                 }

                 if ($this->questionChi != "")
                 {
                     if ($this->type == 0)
                     {
                         # Parse MC Answer Options
                         $parsedMCQ = $this->parseMCQuestion($this->questionChi);
                         $formatedQChi = $parsedMCQ[0]."\n$i_QB_AnswerOption\n";

                         for ($i=1; $i<sizeof($parsedMCQ); $i++)
                         {
                              $option = chr($option_first_letter + $i - 1);
                              $formatedQChi .= "$option. ".$parsedMCQ[$i];
                         }
                     }
                     else
                     {
                         $formatedQChi = $this->questionChi;
                     }
                     $formatedQChi = nl2br($formatedQChi)."<br>\n $i_QB_Answer:".$this->answerChi;
                 }
                 else
                 {
                     $formatedQChi = "-";
                 }


                 $x = "";
                 $x .= "<table width=95% border=0 cellspacing=2 cellpadding=2 align=center>\n";
                 $x .= "<tr><td width=20%>&nbsp;</td><td width=1></td><td width=80%></tr>\n";
                 $x .= "<tr><td align=right>$i_QB_QuestionCode</td><td>:</td><td>".$this->questionCode."</td></tr>\n";
                 $x .= "<tr><td align=right>$i_QB_QuestionType</td><td>:</td><td>".$this->QuestionType[$this->type]."</td></tr>\n";
                 $x .= "<tr><td align=right>$i_QB_Level</td><td>:</td><td>".$dataLvl[0][$lang_field]."</td></tr>\n";
                 $x .= "<tr><td align=right>$i_QB_Category</td><td>:</td><td>".$dataCat[0][$lang_field]."</td></tr>\n";
                 $x .= "<tr><td align=right>$i_QB_Difficulty</td><td>:</td><td>".$dataDiff[0][$lang_field]."</td></tr>\n";
                 $x .= "<tr><td align=right>$i_QB_Owner</td><td>:</td><td>".$this->returnOwnerName()."</td></tr>\n";
                 $x .= "<tr><td align=right>$i_QB_Status</td><td>:</td><td>".$status_array[$this->recordStatus]."</td></tr>\n";
                 $x .= "<tr><td align=right>$i_QB_DateSubmission</td><td>:</td><td>".$this->dateInput."</td></tr>\n";
                 $x .= "<tr><td align=right>$i_QB_LastModified</td><td>:</td><td>".$this->lastModified."</td></tr>\n";
                 $x .= "<tr><td align=right>$i_QB_EngVer</td><td>:</td><td>$formatedQEng</td></tr>\n";
                 $x .= "<tr><td align=right>$i_QB_ChiVer</td><td>:</td><td>$formatedQChi</td></tr>\n";
                 $x .= "</table>\n";
                 return $x;
        }

        function returnAllQuestions($GroupID)
        {
                 global $intranet_session_language;
                 $qb_name = $intranet_session_language=="en"? "EngName": "ChiName";
                 $username_field = getNameFieldByLang("e.");

                 $sql = "SELECT a.QuestionCode, a.QuestionType, b.$qb_name,
                         c.$qb_name, d.$qb_name, $username_field, a.RecordStatus,
                         a.DownloadedCount, a.Score,
                         a.DateInput, a.DateModified, a.QuestionEng, a.AnswerEng,
                         a.QuestionChi, a.AnswerChi
                         FROM QB_QUESTION as a
                         LEFT OUTER JOIN QB_LEVEL as b ON a.LevelID = b.UniqueID
                         LEFT OUTER JOIN QB_CATEGORY as c ON a.CategoryID = c.UniqueID
                         LEFT OUTER JOIN QB_DIFFICULTY as d ON a.DifficultyID = d.UniqueID
                         LEFT OUTER JOIN INTRANET_USER as e ON a.OwnerIntranetID = e.UserID
                         WHERE a.RelatedGroupID = '$GroupID'
                         ORDER BY a.DateModified DESC
                         ";
                         /*
                 echo $sql;
                 $sql = "SELECT a.QuestionCode, a.QuestionType, a.LevelID,
                         a.CategoryID, a.DifficultyID, $username_field, a.RecordStatus,
                         a.DownloadedCount, a.Score,
                         a.DateInput, a.DateModified, a.QuestionEng, a.AnswerEng,
                         a.QuestionChi, a.AnswerChi
                         FROM QB_QUESTION as a
                         LEFT OUTER JOIN INTRANET_USER as e ON a.OwnerIntranetID = e.UserID
                         WHERE a.RelatedGroupID = $GroupID
                         ORDER BY a.DateModified
                         ";
                 //echo $sql;
                 */
                 return $this->returnArray($sql,15);
        }

        function displayAll($GroupID)
        {
                 global $i_QB_Category,$i_QB_Level,$i_QB_Difficulty,$i_QB_Pending
                 ,$i_QB_Approved,$i_QB_Rejected, $i_QB_Question, $i_QB_Status, $i_QB_LangVer
                 ,$i_QB_EngOnly,$i_QB_ChiOnly,$i_QB_BothLang,$i_QB_LastModified
                 ,$i_QB_QuestionCode,$i_QB_Owner,$i_QB_QuestionType,$i_QB_LastModified
                 ,$i_QB_DateSubmission,$i_QB_EngVer,$i_QB_ChiVer,$i_QB_Answer,$i_QB_AnswerOption;
                 global $i_QB_DownloadedCount,$i_QB_Pts;
                 global $intranet_session_language,$button_print;

                 $questions = $this->returnAllQuestions($GroupID);
                 $status_array = array($i_QB_Pending,$i_QB_Approved, $i_QB_Rejected);
                 $option_first_letter = 65;
                 $x = "<!--\n";
                 /*
                 echo "<!--\n";
                 print_r($questions);
                 echo  "-->\n";
                 */

                 for ($j=0; $j<sizeof($questions); $j++)
                 {
                      list ($code, $type,$level,$cat,$diff,$owner,$status,$count,$score,
                            $dateInput,$dateModified,$queEng,$ansEng,$queChi,$ansChi) = $questions[$j];

                      if ($queEng != "")
                      {
                          if ($type == -1)
                          {
                              # Parse MC Answer Options
                              $parsedMCQ = $this->parseMCQuestion($queEng);
                              $formatedQEng = $parsedMCQ[0]."$i_QB_AnswerOption\n";

                              for ($i=1; $i<sizeof($parsedMCQ); $i++)
                              {
                                   $option = chr($option_first_letter + $i - 1);
                                   $formatedQEng .= "$option. ".$parsedMCQ[$i];
                              }
                          }
                          else
                          {
                              $formatedQEng = $queEng;
                          }
                          $formatedQEng = nl2br($formatedQEng)."<br>\n $i_QB_Answer:".$ansEng;
                      }
                      else
                      {
                          $formatedQEng = "-";
                      }

                      if ($queChi != "")
                      {
                          if ($type == -1)
                          {
                              # Parse MC Answer Options
                              $parsedMCQ = $this->parseMCQuestion($queChi);
                              $formatedQChi = $parsedMCQ[0]."$i_QB_AnswerOption\n";

                              for ($i=1; $i<sizeof($parsedMCQ); $i++)
                              {
                                   $option = chr($option_first_letter + $i - 1);
                                   $formatedQChi .= "$option. ".$parsedMCQ[$i];
                              }
                          }
                          else
                          {
                              $formatedQChi = $queChi;
                          }
                          $formatedQChi = nl2br($formatedQChi)."<br>\n $i_QB_Answer:".$ansChi;
                      }
                      else
                      {
                          $formatedQChi = "-";
                      }

                      $x .= "<table width=95% border=0 cellspacing=2 cellpadding=2 align=center>\n";
                      $x .= "<tr><td width=20%>&nbsp;</td><td width=1></td><td width=80%></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_QuestionCode</td><td>:</td><td>".$code."</td></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_QuestionType</td><td>:</td><td>".$this->QuestionType[$type]."</td></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_Level</td><td>:</td><td>".$level."</td></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_Category</td><td>:</td><td>".$cat."</td></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_Difficulty</td><td>:</td><td>".$diff."</td></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_Owner</td><td>:</td><td>".$owner."</td></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_Status</td><td>:</td><td>".$status_array[$status]."</td></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_DownloadedCount</td><td>:</td><td>".$count."</td></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_Pts</td><td>:</td><td>".$score."</td></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_DateSubmission</td><td>:</td><td>".$dateInput."</td></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_LastModified</td><td>:</td><td>".$dateModified."</td></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_EngVer</td><td>:</td><td>$formatedQEng</td></tr>\n";
                      $x .= "<tr><td align=right>$i_QB_ChiVer</td><td>:</td><td>$formatedQChi</td></tr>\n";
                      $x .= "</table>\n";
                      $x .= "<hr width=90%>\n";
                 }
                 $x .= "-->\n";
                 $x .= "<input type=button class=button value=\"$button_print\" onClick=window.print()>\n";

                 return $x;
        }


  }

} # End of directive
?>