<?php
if (!defined("STEM_LICENSE_DEFINED")) {
    define("STEM_LICENSE_DEFINED", true);
    
    class StemLicense
    {
        const TOKEN_LIFETIME = 600;
        
        protected static $publicAccessProcedure = array(
            'createSsoToken',
            'validateSsoToken'
        );
        
        protected $db;
        
        public function __construct()
        {
            $this->db = new libdb();
        }

        public function run($procedure, $args)
        {
            global $UserID;
        
            if (method_exists($this, $procedure)) {
                if (in_array($procedure, self::$publicAccessProcedure)) {
                    $args = is_array($args) ? $args : array();
        
                    return call_user_func_array(
                        array($this, $procedure),
                        array($args)
                    );
                } else {
                    return 202; // Error Code - No Permission
                }
            } else {
                return 3; // Error Code - Unknown Method
            }
        }
        
        private function getPowerLessonDbName()
        {
            global $eclass_prefix;
            
            return $eclass_prefix . 'powerlesson';
        }
        
        private function getSsoToken($userId)
        {
            $sql = "SELECT
                        token, create_date AS createDate
                    FROM
                        ".$this->getPowerLessonDbName() .".stem_license_sso_tokens
                    WHERE
                        user_id = '" . $userId . "'";
            $result = $this->db->returnResultSet($sql);
            
            return count($result) > 0? $result[0] : null;
        }
        
        protected function createSsoToken($args)
        {
            global $UserID, $config_school_code, $intranet_root;
            
            if ($UserID == '') {
                if(isset($args['userLogin']) && isset($args['password'])){
                    require_once $intranet_root .'/includes/libauth.php';
                    $lauth = new libauth();
                    $loginResult = $lauth->WS_Login($args['userLogin'], $args['password'], true, false);
                    
                    if(!is_array($loginResult)){
                        return $loginResult;
                    } else {
                        $UserID = $loginResult['UserID'];
                    }
                } else {
                    return 104; // Error Code - User not found
                }
            }
            
            $newToken = md5($config_school_code . $UserID . time());
            
            if(is_null($this->getSsoToken($UserID))){
                $sql = "INSERT INTO
                            ".$this->getPowerLessonDbName() .".stem_license_sso_tokens
                            (user_id, token, create_date)
                        VALUES
                            ('" . $UserID . "', '" . $newToken . "', NOW())";
            } else {
                $sql = "UPDATE
                            ".$this->getPowerLessonDbName() .".stem_license_sso_tokens
                        SET
                            token = '" . $newToken . "',
                            create_date = NOW()
                        WHERE
                            user_id = '" . $UserID . "'";
            }
            $this->db->db_db_query($sql);
            
            return array('ssoToken' => $newToken);
        }
        
        protected function validateSsoToken($args)
        {
            global $config_school_code, $intranet_db;
            
            if (!isset($args['schoolCode']) || !isset($args['userLogin']) && !isset($args['token'])) {
                return 102; // Error Code - Invalid parameters
            }
            
            # Validate SchoolCode
            if($args['schoolCode'] !== $config_school_code){
                return 1; // Error Code - Unknown error
            }
            
            # Validate UserLogin
            $sql    = "SELECT UserID FROM " . $intranet_db . ".INTRANET_USER WHERE UserLogin = '" . $args['userLogin'] . "'";
            $userId = current($this->db->returnVector($sql));
            
            if(empty($userId)){
                return 104; // Error Code - User Not Found
            }
            
            # Validate Token
            $token = $this->getSsoToken($userId);
            
            if($args['token'] !== $token['token']){
                return 501; // Error Code - Invalid Token
            }
                       
            if(strtotime($token['createDate']) + self::TOKEN_LIFETIME < time()){
                return 503; // Error Code - Token Expired
            }
            
            return array();
        }
    }
}