<?php
// Editing by 
/*
 * @Description
 * libpwm: Password Manager Library
 */
include_once("libdb.php");

class libpwm extends libdb
{
	var $key = "37gratmj4wn1kodr"; // 16 bytes key for aes-128
	var $iv = "uvm9qao1l66gzqy7"; // 16 bytes initilaization vector
	
	function libpwm()
	{
		$this->libdb();
	}
	
	/* @param
	 * $uid_ary : UserID array
	 * @return associative array $ary[UserID] = $data which $data is decrypted 
	 */
	function getData($uid_ary)
	{
		global $eclass_db;
		
		$sql = "SELECT 
					u.UserID,
					u.StaffCodeEnc,
					g.MemberInfo,
					p.RandomID,
					v.FileHashed,
					a.AttemptCode 
				FROM INTRANET_USER as u 
				LEFT JOIN INTRANET_GROUP_MEMBER as g ON g.MemberID=u.UserID
				LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as p ON p.UserID=u.UserID
				LEFT JOIN ".$eclass_db.".eclass_file_version as v ON v.FileID=u.UserID
				LEFT JOIN ".$eclass_db.".active_lesson_attempt as a ON a.ExpiredID=u.UserID
				WHERE u.UserID IN ('".implode("','",(array)$uid_ary)."')";	
		$rs = $this->returnResultSet($sql);
		$count = count($rs);
		
		$ary = array();
		for($i=0;$i<$count;$i++){
			$uid = $rs[$i]['UserID'];
			$pw = $this->getFileData($uid).$rs[$i]['RandomID'].$rs[$i]['StaffCodeEnc'].$rs[$i]['AttemptCode'].$rs[$i]['FileHashed'].$rs[$i]['MemberInfo'];
			$ary[$uid] = $this->decrypt($pw);
		}
		
		return $ary;
	}
	
	/*
	 * @param
	 * $uid_ary : associative array $uid_ary[UserID] = $data which $data is non-encrypted
	 */
	function setData($uid_ary)
	{
		global $intranet_root, $file_path, $eclass_db;
		if(count($uid_ary)==0) return false;
		
		$result = array();
		foreach($uid_ary as $uid => $data){
			
			$encrypted = $this->encrypt($data);
			$pieces = $this->split_text($encrypted);
			
			$this->setFileData($uid, $pieces[0]);
			
			$sql = "UPDATE INTRANET_USER SET StaffCodeEnc='".$pieces[2]."' WHERE UserID='$uid'";
			$result['StaffCodeEnc_U'.$uid] = $this->db_db_query($sql);
			
			$sql = "UPDATE INTRANET_GROUP_MEMBER SET MemberInfo='".$pieces[5]."' WHERE MemberID='$uid'";
			$MemberInfo_U = $this->db_db_query($sql);
			$MemberInfo_I = false;
			$rows_affected = $this->db_affected_rows();
			if($rows_affected == 0){
				$sql = "INSERT INTO INTRANET_GROUP_MEMBER (MemberID,MemberInfo) VALUES ('$uid','".$pieces[5]."')";
				$MemberInfo_I = $this->db_db_query($sql);
			}
			$result['MemberInfo_'.$uid] = $MemberInfo_U || $MemberInfo_I;
			
			$sql = "UPDATE INTRANET_USER_PERSONAL_SETTINGS SET RandomID='".$pieces[1]."',RandomID_DateModified=NOW() WHERE UserID='$uid'";
			$RandomID_U = $this->db_db_query($sql);
			$RandomID_I = false;
			$rows_affected = $this->db_affected_rows();
			if($rows_affected == 0){
				$sql = "INSERT INTO INTRANET_USER_PERSONAL_SETTINGS (UserID,RandomID,RandomID_DateModified) VALUES ('$uid','".$pieces[1]."',NOW())";
				$RandomID_I = $this->db_db_query($sql);
			}
			$result['RandomID_'.$uid] = $RandomID_U || $RandomID_I;
			
			$sql = "UPDATE ".$eclass_db.".eclass_file_version SET FileHashed='".$pieces[4]."' WHERE FileID='$uid'";
			$FileHashed_U = $this->db_db_query($sql);
			$FileHashed_I = false;
			$rows_affected = $this->db_affected_rows();
			if($rows_affected == 0){
				$sql = "INSERT INTO ".$eclass_db.".eclass_file_version (FileID,FileHashed) VALUES ('$uid','".$pieces[4]."')";
				$FileHashed_I = $this->db_db_query($sql);
			}
			$result['FileHashed_'.$uid] = $FileHashed_U || $FileHashed_I;
			
			$sql = "UPDATE ".$eclass_db.".active_lesson_attempt SET AttemptCode='".$pieces[3]."' WHERE ExpiredID='$uid'";
			$AttemptCode_U = $this->db_db_query($sql);
			$AttemptCode_I = false;
			$rows_affected = $this->db_affected_rows();
			if($rows_affected == 0){
				$sql = "INSERT INTO ".$eclass_db.".active_lesson_attempt (ExpiredID,AttemptCode) VALUES ('$uid','".$pieces[3]."')";
				$AttemptCode_I = $this->db_db_query($sql);
			}
			$result['AttemptCode_'.$uid] = $AttemptCode_U || $AttemptCode_I;
		}
		
		return !in_array(false,$result);
	}
	
	function setFileData($uid, $data, $prefix="u")
	{
		global $intranet_root, $file_path;
		
		if (!is_dir("$file_path/file/photo"))
		{
			mkdir("$file_path/file/photo",0777);
		}
		
		if (!is_dir("$file_path/file/photo/map"))
		{
			mkdir("$file_path/file/photo/map",0777);
		}
		
		$path = "$file_path/file/photo/map/".$prefix.$uid.".jpeg";
		$handle = fopen($path,'w');
		$bytesWritten = fwrite($handle, $data);
		fclose($handle);
		
		//chmod($path, 0777);
		
		return $bytesWritten > 0;
	}
	
	function getFileData($uid, $prefix="u")
	{
		global $intranet_root, $file_path;
		
		$path = "$file_path/file/photo/map/".$prefix.$uid.".jpeg";
		if(file_exists($path)){
			$data = file_get_contents($path);
			return $data;
		}
		return "";
	}
	
	/*
	 * @param
	 * $mid_ary : associative array $mid_ary[MailBoxID] = $data which $data is non-encrypted
	 */
	function setShareMailboxData($mid_ary)
	{
		if(count($mid_ary) == 0) return false;
		
		$result = array();
		foreach($mid_ary as $mid => $data){
			$encrypted = $this->encrypt($data);
			$pieces = $this->split_text($encrypted,2);
			
			$result['WriteFile_'.$mid] = $this->setFileData($mid,$pieces[0],"m");
			
			$sql = "UPDATE MAIL_SHARED_MAILBOX SET MailBoxHashed='".$pieces[1]."' WHERE MailBoxID='$mid'";
			$result['MailBoxHashed_U'.$mid] = $this->db_db_query($sql);
		}
		return !in_array(false,$result);
	}
	
	/*
	 * @param 
	 * $mid_ary : MailBoxID array
	 * @return
	 * associative array $ary[$MailBoxID] = $data where $data is decrypted
	 */
	function getShareMailboxData($mid_ary)
	{
		$sql = "SELECT MailBoxID,MailBoxHashed FROM MAIL_SHARED_MAILBOX WHERE MailBoxID IN ('".implode("','",(array)$mid_ary)."')";
		$rs = $this->returnResultSet($sql);
		$ary = array();
		$count = count($rs);
		for($i=0;$i<$count;$i++){
			 $data = $this->getFileData($rs[$i]['MailBoxID'],"m").$rs[$i]['MailBoxHashed'];
			 $ary[$rs[$i]['MailBoxID']] = $this->decrypt($data);
		}
		return $ary;
	}
	
	// encrypt with aes-128 in ecb mode
	function encrypt($text)
	{
		if($text == "") return $text;
		
		$data = $this->pad($text,16);
		/*
		 * echo string in this format $'abcd'edf' to escape single quotes and line feed characters
		 */
		$command="echo ".'$'."'".str_replace("'",'\\\'',$data)."' | openssl enc -aes-128-ecb -nosalt -a -A -K " . bin2hex ($this->key) . " -iv " . bin2hex ($this->iv);
		$encrypted_text=trim(shell_exec("$command"));
		
		return $encrypted_text;
	}
	
	// decrypt with aes-128 in ecb mode
	function decrypt($encrypted_text)
	{
		if($encrypted_text == "") return $encrypted_text;
		
		$command="echo $encrypted_text | openssl enc -d -aes-128-ecb -nosalt -a -A -K " . bin2hex ($this->key) . " -iv " . bin2hex ($this->iv);
		$plain_text=trim(shell_exec("$command"));
		
		return $this->unpad($plain_text);
	}
	
	// make string size multiple of block size, pad with the char of the pad size
	function pad($text, $block_size)
	{
		$data = $text;
		$pad = $block_size - (strlen($data) % $block_size);
		$data .= str_repeat(chr($pad), $pad);
		
		return $data;
	}
	
	// chop the tail of the number of pad size
	function unpad($text)
	{
		$len = strlen($text);
		$last_char = ord($text[$len-1]); // get the last character ascii value
		$chop = $last_char;
		
		if($chop > $len) return $text;
		
		for($i=$len-$last_char;$i<$len-1;$i++)
		{
			if(ord($text[$i]) != $last_char){
				$chop = 0;
				break;
			}
		}
		return substr($text,0,$len-$chop);
	}
	
	function split_text($text, $num_of_parts=6)
	{
		$chop_size = $num_of_parts;
		$piece_len = floor(strlen($text) / $chop_size);
		$pieces = array();
		for($i=0;$i<$chop_size;$i++)
		{
			$pieces[$i] = $i == ($chop_size-1)? substr($text,$i*$piece_len) : substr($text,$i*$piece_len,$piece_len);
		}
		return $pieces;
	}
}

?>