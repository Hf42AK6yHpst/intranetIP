<?php
# using:

#################################################
# Please save in UTF-8 Format
################################################# 
####### Change log [Start] #######
#
#   Date:   2020-08-29 (Cameron)
#           - fix: wrong option checked when GuardStay is 'No' in GENERATE_RADIO_BTN_LIST()
#           - fix: should retrieve isMain guardian from syncToAccountMainGuardian()
#               ExtendRecordID is from GUARDIAN_STUDENT_EXT_1 but not GUARDIAN_STUDENT
#
#   Date:   2020-08-28 (Cameron)
#           - add function getMacauBirthPlaceID(), getMacauNationID() [case #X193942]
#
#   Date:   2020-08-24 (Cameron)
#           - add function getGradeAndClass()
#
#   Date:   2020-08-18 (Cameron)
#           - add function lookupSraGrade(), copy convert2Big5() from ej
#
#   Date:   2020-06-15 (Cameron)
#           - fix sql in storeStudent(), should specify alias for RecordStatus to avoid ambiguous [case #X187526]
#
#   Date:   2020-04-08 (Cameron)
#           - add flag $sys_custom['StudentRegistry']['DisableSyncAccount'] to control show/hide "Synchronize Student Account" button in GET_MODULE_OBJ_ARR() [case #Y173401]
#
#   Date:   2020-04-06 (Cameron)
#           - show SynchronizeRegistryInfo button in GET_MODULE_OBJ_ARR() [case #Y173401]
#
#	Date:	2020-03-13 (Philips)
#			- modified updateStudentRegistry_Simple_Macau()
#			- added syncToAccountUserInfo(), syncToAccountPersonalSetting(), syncToAccountMainGuardian()
#
#	Date:	2020-03-12 (Philips)
#			- modified getCustomColumn(), addCustomColumn, added Section into cols
#
#   Date:   2019-10-29 (Cameron)
#           - add SynchronizeRegistryInfo button to GET_MODULE_OBJ_ARR(), hide it first
#
#   Date:   2019-10-25 (Cameron)
#           - modify insert2Table and update2Table to support database name as prefix passing in table name
#
#   Date:   2019-08-15 (Cameron)
#           - move insert2Table() and update2Table() from libstudentregistry_kv.php to here
#           - add function isSubmitted()
#           - add submittedcount in displayClassInfoInStudentRegistry()
#           - modify RETRIEVE_STUDENT_INFO_BASIC_HK() to retrieve DATA_CONFIRM_DATE and DATA_CONFIRM_BY from STUDENT_REGISTRY_SUBMITTED
#           - add parameter $academicYearID to RETRIEVE_STUDENT_INFO_BASIC_HK()
#           - modify RETRIEVE_STUDENT_INFO_BASIC_HK() to retrieve ClassName and ClassNumber by specific AcademicYear rather than current AcademicYear
#
#   Date:   2019-05-16 (Cameron)
#           - disable custom field for Kentville
#
#   Date:   2019-05-14 (Cameron)
#           - fix potential sql injection problem by enclosing var with apostrophe
#
#   Date:   2019-05-03 (Philips)
#           - add orderCustomColumn()
#
#   Date:   2019-04-30 (Philips)
#           - add getCustomColumn()
#           - add getCustomColumnMaxOrder()
#           - add checkCustomColumnCode()
#           - add addCustomColumn()
#           - add updateCustomColumn()
#           - add deleteCustomColumn()
#           - add getCustomColumnValueByUserID()
#           - add getCustomValueByUserID()
#           - add updateCustomValue()
#           - add insertCustomValue()
#
#   Date:   2019-04-04 (Cameron)
#           - add parameter $noFirst displayPresetCodeSelection()
#
#	Date:	2017-06-09 (Cameron)
#			- add parameter includeLeft (alumni & left student, all in INTRANET_USER) to storeStudent() 
#	
#	Date:	2015-07-24 (Cameron)
#			- filter out alumni (RecordType=4) and graduated student (RecordStatus=3) in storeStudent()
#
#	Date:	2015-07-22 (Cameron)
#			- retrieve more fields in RETRIEVE_STUDENT_INFO_BASIC_HK(): WEBSAMSCode, BIRTH_CERT_NO
#
#	Date:	2014-05-02	(YatWoon)
#			update RETRIEVE_BRO_SIS_HK(), updateStudentBroSisInfo_HK(), add student name [Requested by UCCKE]
#
#	Date:		2013-08-16	(YatWoon)
#			updated RETRIEVE_STUDENT_INFO_BASIC_HK(), add submit_date and submit_user fields 
#
#	Date:		2013-07-03	(YatWoon)
#			updated updateStudentInfo() to not die if there is any sql error which is most likely due to duplicate email address (student and parent)
#
#	Date:		2013-07-03	(YatWoon)
#			add getChildrenForm()
#			update checkParentFillOnlineReg(), cater current student online reg checking
#
#	Date	:	2013-03-28 (YatWoon)
#				modified libstudentregistry(), add setting session
#				add checkParentFillOnlineReg() for login checking
#
#	Date	:	2012-01-16 (Henry Chow)
#	Detail	:	modified GET_MODULE_OBJ_ARR(), only "Malaysia" can access "Statistics"
#
#	Date	:	2010-11-24 (Henry Chow)
#	Detail	:	modified updateStudentRegistry_Simple_Macau(), add checking on "Foreach" loop
#
#	Date	:	2010-11-24 (Henry Chow)
#	Detail	:	modified updateStudentRegistry_Simple_Macau(), update "Emergency Contact info" in basic view (Macau) 
#
#
#	Date	:	2010-11-24 (Henry Chow)
#	Detail	:	modified checkData_Macau(), only "Class Name", "Class Number", "Chinese Name" & "Gender" are compulsory
#
#	Date	:	2010-11-23 (Henry Chow)
#	Detail	:	modified RETRIEVE_STUDENT_INFO_ADV_MACAU(), retrieve "TagID"
#
#	Date	:	20101021 (Henry Chow)
#	Detail	:	modified getStudentInfo_by_UserID(), retrieve YearOfLeft
#
#	Date	:	20101019 (Henry Chow)
#	Detail	:	modified RETRIEVE_GUARDIAN_INFO_ADV_MAL(), retrieve data of "xxx_OTHERS" in Advance view
#
#	Date	:	20101013 (Henry Chow)
#	Detail	:	modified RETRIEVE_STUDENT_INFO_BASIC_MAL(), retrieve data of "NATION_OTHERS" in Basic view
#
#	Date	:	20101013 (Henry Chow)
#	Detail	:	modified MAL_EDU_LEVEL_DATA_ARY(), MAL_JOB_TITLE_DATA_ARY(), MAL_JOB_TITLE_DATA_ARY(), MAL_JOB_NATURE_DATA_ARY(), MAL_FAMILY_LANG_ARY(), MAL_NATION_DATA_ARY(), MAL_B_PLACE_DATA_ARY()
#				add option "Others"
#
#	Date	:	20101013 (Henry Chow)
#	Detail	:	modified checkData_Mal(), updateStudentRegistry_Simple_Mal(), update info in "Basic mode"
#
#	Date	:	20100914 (Henry Chow, Ted Chin)
#	Detail	:	modified MAL_NATION_DATA_ARY() & MAL_B_PLACE_DATA_ARY(), group some places as "Malaysia"
#
#	Date	:	20100913 (Henry Chow, Ted Chin)
#	Detail	:	modified Malaysia function in Simplified Chinese (GB)
#
#	Date	:	20100827 (Henry Chow)
#	Detail	:	modified getRegistryStatusReport(), getRegistryStatusStudentList(), getRegistryStatusStudentStatistics(), getReasonTable()
#				can get the data by date range
#
#	Date	:	20100826 (Henry Chow)
#	Detail	:	modified getRegistryStatusStudentStatistics(), add "Transferred Student" in Statistics
#
#	Date	:	20100825 (Henry Chow)
#	Detail	:	modified getRegistryStatusStudentList(), count the regsitry status even "IsCurrent=0"
#
#	Date	:	20100825 (Henry Chow)
#	Detail	:	Add getStudentRegistryStatistics(), for "Statistics > Students' Statistic"
#
#	Date	:	20100823 (Thomas Yau)
#	Detail	:	Add function getClassTeacherByYearClassID() for getting the class teacher information
#
#	Date	:	20100820 (Thomas Yau)
#	Detail	:	Add function getStudRegInfoReport_StudentList() for Displaying report of registry info in Student List form 
#
#	Date	:	20100820 (Henry Chow)
#	Detail	:	modified GET_MODULE_OBJ_ARR(), update the menu
##
#	Date	:	20100818 (Henry Chow)
#	Detail	:	Add getRegistryStatusReport(), getRegistryStatusStudentList(), getRegistryStatusStudentStatistics(), getReasonTable(),
#				for "Reports > Student Registry"
#
#	Date	:	20100818 (Henry Chow)
#	Detail	:	Add 'Reports > Student Registry' to GET_MODULE_OBJ_ARR()
#
#	Date	:	20100817 (Henry Chow)
#	Detail	:	add getNoticeAry(), return noticeID array
#
#	Date	:	20100816 (Thomas)
#	Detail	:	Add 'Statistic' to GET_MODULE_OBJ_ARR()
#
#	Data 	:	20100811 (Henry Chow)
#	Detail	:	add insertStudentRegistryStatus(), insert new entry of "Student Registry Status"
#
#	Data 	:	20100805 (Henry Chow)
#	Detail	:	add getRecordStatusMenu_Malaysia(), add "Record Status"" selection menu for Malaysia "Student Registry Status" 
#
#	Data 	:	20100804 (Henry Chow)
#	Detail	:	define "STUDENT REGISTRY RECORDSTATUS" parameters
#
#	Data 	:	20100804 (Henry Chow)
#	Detail	:	mofidied GET_MODULE_OBJ_ARR(), modify the link of "Mgmt>information"
#
#	Data 	:	20100728 (Henry Chow)
#	Detail	:	mofidied Get_Group_Access_Right_Users(), add "ORDER BY"
#
#	Data 	:	20100728 (Henry Chow)
#	Detail	:	added Check_Group_Title(), checking of duplicate Group Title
#
#	Data 	:	20100727 (Henry Chow)
#	Detail	:	added memberInMgmtGroup(), select member of access right group
#
#	Data 	:	20100727 (Henry Chow)
#	Detail	:	added removeStudentRegistry_PG(), remove STUDENT_REGISTRY_PG data
#
#	Data 	:	20100727 (Henry Chow)
#	Detail	:	added updateStudentRegistry_Simple_Macau(), updateStudentInfo(), updateStudentRegistry_PG()
#				update student registry data
#
#	Data 	:	20100727 (Henry Chow)
#	Detail	:	added getStudentClassInfo(), retrieve student info (year, class id, class name, class number) of current year
#
#	Data 	:	20100726 (Henry Chow)
#	Detail	:	added checkData_Macau(), retrieveStudentIDByClassOrLogin(), checkDateFormat(), import Student Registry (Macau) checking
#
#	Data 	:	20100721 (Henry Chow)
#	Detail	:	added getSelectClassWithWholeForm(), storeStudent(), selection of classes with "Form" option
#
#	Data 	:	20100721 (Henry Chow)
#	Detail	:	added getParentGuardianInfo(), retrieve student parent/guardian info
#
#	Data 	:	20100720 (Henry Chow)
#	Detail	:	added RETRIEVE_DATA_ARY(), RETRIEVE_STUDENT_INFO_BASIC_MACAU(), MACAU_NATION_DATA_ARY(), MACAU_ID_TYPE_DATA_ARY()
#				for "Student Registry" > "Information" (edit basic / advance information)
#
#	Data 	:	20100720 (Henry Chow)
#	Detail	:	added getUserInfoByID(), displayClassInfoInStudentRegistry(), getSelectClass(), getRegistryStatusMenu(), getRecordStatusMenu()
#				for "Student Registry" > "Information" (class list & student list)
#
#	Data 	:	20100716 (Henry Chow)
#	Detail	:	modified GET_MODULE_OBJ_ARR(), add access right checking on menu
#
#	Data 	:	20100713 (Henry Chow)
#	Detail	:	added functions for "Student Registry" access right
#				- Insert_Group_Access_Right()
#				- GET_ACCESS_RIGHT_GROUP_ID()
#				- NEW_ACCESS_RIGHT_GROUP(), UPDATE_ACCESS_RIGHT_GROUP(), DELETE_ACCESS_RIGHT()
#				- Get_Group_Info(), retrieveAccessRight()
#				- Delete_Group(), Rename_Group(), Set_Group_Description()
#				- Get_Group_Access_Right_Users(), Get_Group_Access_Right_Aval_User_List()
#				- Add_Member(), Remove_Member()
#
####### Change log [End] #######


if (!defined("LIBSTUDENTREGISTRY_DEFINED"))                     // Preprocessor directive
{
	define("LIBSTUDENTREGISTRY_DEFINED", true);
	
	# STUDENT REGISTRY RECORDSTATUS
	define("STUDENT_REGISTRY_RECORDSTATUS_NORMAL",		"0");
	define("STUDENT_REGISTRY_RECORDSTATUS_LEFT",		"1");
	define("STUDENT_REGISTRY_RECORDSTATUS_SUSPEND",		"2");
	define("STUDENT_REGISTRY_RECORDSTATUS_RESUME",		"3");
	
	class libstudentregistry extends libdb
	{
		var $Module;

        function GET_MODULE_OBJ_ARR()
        {
                global $UserID, $PATH_WRT_ROOT, $plugin, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $intranet_session_language, $intranet_root;
                global $ACCESS_RIGHT_RIGHT, $Lang, $plugin, $sys_custom;
                
                include_once("libaccessright.php");
                $laccessright = new libaccessright();
                
                # Current Page Information init
				$PageManagement					    = 0;
                $PageMgmt_RegistryInfo			    = 0;
                $PageMgmt_RegistrySynchronization   = 0;
                $PageMgmt_RegistryStatus		    = 0;
                
				$PageSettings 					    = 0;
                $PageSettings_AccessRight		    = 0;
                
                switch ($CurrentPage) {
                    case "Mgmt_RegistryInfo":
                    	$PageManagement = 1;
                    	$PageMgmt_RegistryInfo = 1;
                    	break;
                    case "Mgmt_RegistrySynchronization":
                        $PageManagement = 1;
                        $PageMgmt_RegistrySynchronization = 1;
                        break;
                    case "Mgmt_RegistryStatus":
                    	$PageManagement = 1;
                    	$PageMgmt_RegistryStatus = 1;
                    	break;
                    case "Stat_Student":
                    	$PageStatistic = 1;
                    	$PageStat_Student = 1;
                    	break;
                    case "Stat_Parent":
                    	$PageStatistic = 1;
                    	$PageStat_Parent = 1;
                    	break;
                    case "Report_RegistryInfo":
                    	$PageReport = 1;
                    	$PageReport_RegistryInfo = 1;
                    	break;
                    case "Report_RegistryStatus":
                    	$PageReport = 1;
                    	$PageReport_RegistryStatus = 1;
                    	break;
                    case "Setting_RegistryAccessRight":
                    	$PageSettings = 1;
                    	$PageSettings_AccessRight = 1;
                    	break;
                    case "Setting_RegistryNoticeTemplate":
                    	$PageSettings = 1;
                    	$PageSettings_NoticeTemplate = 1;
                    	break;
                    case "Settings_OnlineReg":
                    	$PageSettings = 1;
                    	$PageSettings_OnlineReg = 1;
                    	break;	
                    case "Settings_CustomCol":
                        $PageSettings = 1;
                        $PageSettings_CustomCol = 1;
                        break;
                }
                
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/";
                
                # Management
                //debug_pr($_SESSION); 
				if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewBasic") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewAdv") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageBasic") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageAdv")) {
					$MenuArr["Management"]	= array($Lang['Menu']['AccountMgmt']['Management'], "#", $PageManagement);
					if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewBasic") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewAdv")) 
						$MenuArr["Management"]["Child"]["RegistryInfo"] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/management/", $PageMgmt_RegistryInfo);

						if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && $sys_custom['StudentRegistry']['Kentville'] && !$sys_custom['StudentRegistry']['DisableSyncAccount']) {
                       $MenuArr["Management"]["Child"]["SynchronizeRegistryInfo"] = array($Lang['StudentRegistry']['Synchronization'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/management/hk/syn/", $PageMgmt_RegistrySynchronization);
                    }

					if($plugin['StudentRegistry_Malaysia'])
						if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-View") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-Manage"))
							$MenuArr["Management"]["Child"]["RegistryStatus"] = array($Lang['StudentRegistry']['StudentRegistryStatus'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/management/malaysia/registrystatus/", $PageMgmt_RegistryStatus);
				}
				
				# Statistic
				if($plugin['StudentRegistry_Malaysia'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-STATS-Parent-View") || $laccessright->CHECK_ACCESS("StudentRegistry-STATS-Student-View"))) {
					$MenuArr["Statistic"] = array($Lang['Menu']['AccountMgmt']['Statistics'], "", $PageStatistic);
					
					if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-STATS-Student-View"))
						$MenuArr["Statistic"]["Child"]["Student"] = array($Lang['Menu']['AccountMgmt']['Student'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/statistic/student/", $PageStat_Student);
					if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-STATS-Parent-View"))
						$MenuArr["Statistic"]["Child"]["Parent"] = array($Lang['Menu']['AccountMgmt']['Parent'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/statistic/parent/", $PageStat_Parent);
				}
				
				/*
				#Report
				if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-REPORTS-RegistryInformation-View") || $laccessright->CHECK_ACCESS("StudentRegistry-REPORTS-RegistryStatus-View")) {
					$MenuArr["Report"] = array($Lang['Menu']['AccountMgmt']['Reports'], "", $PageReport);
					if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-REPORTS-RegistryInformation-View"))
						$MenuArr["Report"]["Child"]["RegistryInfo"] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/report/information/", $PageReport_RegistryInfo);
					if($plugin['StudentRegistry_Malaysia'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-REPORTS-RegistryStatus-View")))
						$MenuArr["Report"]["Child"]["RegistryStatus"] = array($Lang['StudentRegistry']['StudentRegistryStatus'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/report/status/", $PageReport_RegistryStatus);
				}
				*/
				
				# Settings
				if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-AccessRight-Access") || $laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-eNoticeTemplate-Access")) {
					$MenuArr["Settings"] = array($Lang['Menu']['AccountMgmt']['Settings'], "", $PageSettings);
					if($plugin['StudentRegistry_Malaysia'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-eNoticeTemplate-Access")))
						$MenuArr["Settings"]["Child"]["eNoticeTemplate"] = array($Lang['StudentRegistry']['eNoticeTemplate'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/settings/eNoticeTemplate/", $PageSettings_NoticeTemplate);
					
					if(!$plugin['StudentRegistry_HongKong'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-AccessRight-Access"))
						$MenuArr["Settings"]["Child"]["AccessRight"] = array($Lang['AccountMgmt']['Settings']['AccessRights'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/settings/access_right/", $PageSettings_AccessRight);
					
					if(($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-AccessRight-Access")) && $sys_custom['OnlineRegistry'])
						$MenuArr["Settings"]["Child"]["OnlineReg"] = array($Lang['StudentRegistry']['Settings']['OnlineReg'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/settings/online_reg/", $PageSettings_OnlineReg);			

					if (!$sys_custom['StudentRegistry']['Kentville']) {
						$MenuArr['Settings']['Child']['CustomCol'] = array($Lang['StudentRegistry']['Settings']['CustomCol'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/settings/custom_column/", $PageSettings_CustomCol);
					}
				}
				
									
                ### module information
                $MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
                $MODULE_OBJ['title_css'] = "menu_opened";
                $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/leftmenu_big_icon_register.png";
				
            	
                $MODULE_OBJ['menu'] = $MenuArr;
				
                return $MODULE_OBJ;
        }
		
		
		function libstudentregistry($ID="")
		{
			$this->Module = "StudentRegistry";
			$this->libdb();
			
			if (!isset($_SESSION["SSV_PRIVILEGE"]["StudentRegistry"]))
         	{
				include_once("libgeneralsettings.php");
				$lgeneralsettings = new libgeneralsettings();
				
				$settings_ary = $lgeneralsettings->Get_General_Setting($this->Module);
				if(!empty($settings_ary))
				{
					foreach($settings_ary as $key=>$data)
					{
						$_SESSION["SSV_PRIVILEGE"]["StudentRegistry"][$key] = $data;
						$this->$key = $data; 
					}
				}
				else
				{
					$this->NewStudentStartDate = "";
                 	$this->NewStudentEndDate = "";
                 	$this->GroupID = "";
                 	$this->CurrentStudentStartDate = "";
                 	$this->CurrentStudentEndDate = "";
                 	$this->YearIDStr = "";
				}
			}
			else
			{
				$this->NewStudentStartDate = $_SESSION["SSV_PRIVILEGE"]["StudentRegistry"]["NewStudentStartDate"];
				$this->NewStudentEndDate = $_SESSION["SSV_PRIVILEGE"]["StudentRegistry"]["NewStudentEndDate"];
				$this->GroupID = $_SESSION["SSV_PRIVILEGE"]["StudentRegistry"]["GroupID"];			
				$this->CurrentStudentStartDate = $_SESSION["SSV_PRIVILEGE"]["StudentRegistry"]["CurrentStudentStartDate"];
				$this->CurrentStudentEndDate = $_SESSION["SSV_PRIVILEGE"]["StudentRegistry"]["CurrentStudentEndDate"];
				$this->YearIDStr = $_SESSION["SSV_PRIVILEGE"]["StudentRegistry"]["YearIDStr"];
				$this->NewStudentText2Display = stripslashes($_SESSION["SSV_PRIVILEGE"]["StudentRegistry"]["NewStudentText2Display"]);
				$this->CurrentStudentText2Display = stripslashes($_SESSION["SSV_PRIVILEGE"]["StudentRegistry"]["CurrentStudentText2Display"]);
             }
			
		}
			
		function Insert_Group_Access_Right($GroupID, $GroupTitle, $GroupDescription, $Right)
		{
			$Results = array();
			if ($GroupID == "") {
				$GroupID = $this->GET_ACCESS_RIGHT_GROUP_ID('A',$this->Get_Safe_Sql_Query($GroupTitle));
				
				if($GroupID == "")
				{
					$GroupID = $this->NEW_ACCESS_RIGHT_GROUP('A', $this->Get_Safe_Sql_Query($GroupTitle),$this->Get_Safe_Sql_Query($GroupDescription));
					$Result['CreateGroup'] = $GroupID;
				}
				else 
					$Result['UpdateGroup'] = $this->UPDATE_ACCESS_RIGHT_GROUP($GroupID, $this->Get_Safe_Sql_Query($GroupTitle), $this->Get_Safe_Sql_Query($GroupDescription));
			}
			$Result['ClearOldRight'] = $this->DELETE_ACCESS_RIGHT($GroupID,'T'); 
			
			for ($i=0; $i< sizeof($Right); $i++) {
				$RightDetail = explode('_',$Right[$i]);
				$sql = "INSERT INTO ACCESS_RIGHT_GROUP_SETTING 
						(GroupID, Module, Section, Function, Action, DateInput, DateModified)
						 VALUES('$GroupID', '".$this->Module."', '".$RightDetail[0]."', '".$RightDetail[1]."', '".$RightDetail[2]."', NOW(), NOW())";
						 
				$Result['InsertNewRight:'.$Right[$i]]=$this->db_db_query($sql);
			}
			
			if (in_array(false, $Result)) 
				return false;
			else 
				return $GroupID;
		}
		
		function GET_ACCESS_RIGHT_GROUP_ID($GroupType, $GroupTitle, $gID=0)
		{
			$sql = "SELECT GroupID FROM ACCESS_RIGHT_GROUP WHERE GroupType='$GroupType' and Module='". $this->Module."'";
			if($GroupTitle=='') {
				$sql .= " AND (GroupTitle is NULL or GroupTitle ='') ";
			} else {
				$sql .= " AND GroupTitle='$GroupTitle'";
			}
			
			$GroupID = $this->returnVector($sql);
			return $GroupID[0];
		}
		
		function NEW_ACCESS_RIGHT_GROUP($GroupType, $GroupTitle, $GroupDescription)
		{
			$sql = "INSERT INTO ACCESS_RIGHT_GROUP SET GroupType='$GroupType', GroupTitle='$GroupTitle', GroupDescription='$GroupDescription', DateInput=NOW(), DateModified=NOW(), Module='". $this->Module."'";
			$Result = $this->db_db_query($sql);
			
			if ($Result) 
				return $this->db_insert_id();
			else 
				return false;
		}
		
		function UPDATE_ACCESS_RIGHT_GROUP($GroupID, $GroupTitle, $GroupDescription)
		{
			$sql = "UPDATE ACCESS_RIGHT_GROUP SET GroupTitle='$GroupTitle', GroupDescription='$GroupDescription', DateModified=NOW() WHERE GroupID='$GroupID'";
			return $this->db_db_query($sql);
		}
		
		function DELETE_ACCESS_RIGHT($GroupID, $GroupType, $Module="", $Section="", $Function="", $Action="")
		{
			if($GroupType=='S' || $GroupType=='T') {	// Student & Teacher
				$sql = "DELETE FROM ACCESS_RIGHT_GROUP_SETTING WHERE GroupID='$GroupID'";
				return $this->db_db_query($sql);
			} else {	// $GroupType == 'A' (Admin Group)
				$Result = array();
				for($i=0;$i<sizeof($GroupID);$i++) {
					$sql = "DELETE FROM ACCESS_RIGHT_GROUP WHERE GroupID='$GroupID[$i]'";
					$Result[] = $this->db_db_query($sql);
					$sql = "DELETE FROM ACCESS_RIGHT_GROUP_SETTING WHERE GroupID='$GroupID[$i]'";
					$Result[] = $this->db_db_query($sql);
					$sql = "DELETE FROM ACCESS_RIGHT_GROUP_MEMBER WHERE GroupID='$GroupID[$i]'";
					$Result[] = $this->db_db_query($sql);
				}
				
				return !in_array(false,$Result);
			}
		}
		
		function Get_Group_Info($GroupID)
		{
			$sql = "SELECT
						GroupID,
						GroupTitle,
						GroupType,
						GroupDescription,
						DateModified
					FROM 
						ACCESS_RIGHT_GROUP 
					WHERE 
						GroupID = '$GroupID' 
						AND Module = '".$this->Module."' ";
						
			return $this->returnArray($sql);
		}

		function retrieveAccessRight($GroupID='')
		{
			$sql = "select UCASE(Module), UCASE(Section), UCASE(Function), UCASE(Action) from ACCESS_RIGHT_GROUP_SETTING where GroupID='$GroupID'";
			//echo $sql;
			return $this->returnArray($sql);
		}
		
		function Delete_Group($GroupID)
		{
			$Results=array();
			
			$sql = "DELETE FROM ACCESS_RIGHT_GROUP_MEMBER WHERE GroupID = '$GroupID' ";
			$Results['Delete_Members'] = $this->db_db_query($sql);
			
			$sql = "DELETE FROM ACCESS_RIGHT_GROUP_SETTING WHERE GroupID = '$GroupID' AND Module = '".$this->Module."' ";
			$Results['Delete_Settings'] = $this->db_db_query($sql);
			
			$sql = "DELETE FROM ACCESS_RIGHT_GROUP WHERE GroupID = '$GroupID' AND Module='".$this->Module."' ";
			$Results['Delete_Group'] = $this->db_db_query($sql);
			
			return (!in_array(false, $Results));
	 	}

	 	function Rename_Group($GroupTitle,$GroupID)
	 	{
			$sql = 'UPDATE ACCESS_RIGHT_GROUP SET
								GroupTitle = \''.$this->Get_Safe_Sql_Query($GroupTitle).'\', 
								DateModified = NOW()
					WHERE
						GroupID = \''.$GroupID.'\' AND Module = \''.$this->Module.'\' ';
			return $this->db_db_query($sql);
		}
		
		function Set_Group_Description($GroupDescription, $GroupID)
		{
			$sql = "UPDATE 
						ACCESS_RIGHT_GROUP 
					SET 
						GroupDescription = '".$this->Get_Safe_Sql_Query($GroupDescription)."',
						DateModified = NOW() 
					WHERE GroupID = '$GroupID' AND Module = '".$this->Module."' ";
					
			return $this->db_db_query($sql);
		}
		
		function Get_Group_Access_Right_Users($GroupID, $Keyword="")
		{
			$NameField = getNameFieldByLang("u.");
			
			$sql = "SELECT 
						u.UserID,
						".$NameField." as UserName,
						u.Teaching 
					FROM 
						INTRANET_USER as u 
						LEFT JOIN 
						ACCESS_RIGHT_GROUP_MEMBER as m 
						ON u.UserID = m.UserID 
						LEFT JOIN 
						ACCESS_RIGHT_GROUP as g
						ON m.GroupID = g.GroupID 
					WHERE 
						u.RecordType = '1' 
						AND 
						u.RecordStatus = '1' 
						AND 
						m.GroupID = '$GroupID'
						AND 
						(
						u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' 
						OR 
						u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' 
						or 
						u.EnglishName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						or 
						u.ChineseName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%'
						)
					ORDER BY
						$NameField";
			//echo ($sql);
			return $this->returnArray($sql);
		}
		
		function Get_Group_Access_Right_Aval_User_List()
		{
			$NameField = getNameFieldByLang("u.");
			$sql = "SELECT 
						DISTINCT m.UserID 
					FROM 
						ACCESS_RIGHT_GROUP as g
						INNER JOIN ACCESS_RIGHT_GROUP_MEMBER as m ON m.GroupID = g.GroupID
					WHERE
						g.Module = '".$this->Module."' ";
			$StaffList = $this->returnVector($sql);
			
			$sql = "SELECT 
						u.UserID,
						".$NameField." AS StaffName
					FROM 
						INTRANET_USER u  
					WHERE 
						u.RecordType = '1' 
						AND 
						u.RecordStatus = '1' 
						AND u.UserID NOT IN (".(sizeof($StaffList)>0?implode(",",$StaffList):"-1").") 
					ORDER BY 
						u.EnglishName ";
			return $this->returnArray($sql);
		}

		function Add_Member($GroupID,$AddUserID)
		{
			if (sizeof($AddUserID) > 0) 
			{
				$StaffList = implode(',',$AddUserID);
			}
			
			for ($i=0; $i< sizeof($AddUserID); $i++) {
				if($AddUserID[$i]!="") {
					$sql = 'INSERT INTO ACCESS_RIGHT_GROUP_MEMBER 
									(GroupID,UserID, DateInput) 
									VALUES
									(\''.$GroupID.'\',\''.$AddUserID[$i].'\',\'NOW()\')';
									
					$Result['Add-'.$AddUserID[$i]] = $this->db_db_query($sql);
				}
			}
			
			return (!in_array(false,$Result));
		}
		
		function Remove_Member($GroupID,$StaffID)
		{
			$StaffList = implode(',',$StaffID);
			
			$sql = "DELETE FROM ACCESS_RIGHT_GROUP_MEMBER WHERE UserID IN (".$StaffList.") AND GroupID = '$GroupID' ";
			$Result['delete-ACCESS_RIGHT_GROUP_MEMBER'] = $this->db_db_query($sql);
			
			return (!in_array(false,$Result));
		}

		function getUserInfoByID($id)
		{
			$sql = "SELECT * FROM INTRANET_USER WHERE UserID='$id'";	
			$result = $this->returnArray($sql);
			return $result[0];
		}
		
		function displayClassInfoInStudentRegistry($AcademicYearID="") 
		{
			if($AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();
			
			$sql = "SELECT 
						yc.YearClassID, 
						".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", 
						COUNT(ycu.UserID) as studentCount,
						0 as waitApproval,
						y.YearName,
						COUNT(srs.UserID) as submittedCount
					FROM 
						YEAR_CLASS yc INNER JOIN 
						YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID) INNER JOIN
						YEAR y ON (y.YearID=yc.YearID) INNER JOIN
						INTRANET_USER USR ON (USR.UserID=ycu.UserID) LEFT JOIN
						STUDENT_REGISTRY_SUBMITTED srs ON (srs.UserID=USR.UserID AND srs.AcademicYearID='$AcademicYearID')
					WHERE 
						yc.AcademicYearID='$AcademicYearID' AND
						USR.RecordStatus in (0,1,2) AND 
						USR.RecordType = 2
					GROUP BY 
						yc.YearClassID
					ORDER BY 
						y.Sequence, yc.Sequence";
				
			$result = $this->returnArray($sql,5);
			
			return $result;
		}
		
		function displayStudentInfoInStudentRegistry($ClassID="")
		{
			if($ClassID=="")
				$result = array();
			else
			{
				$sql = "SELECT
							USR.UserID,
							".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").",
							ycu.ClassNumber,
							".Get_Lang_Selection("USR.ChineseName","USR.EnglishName")."
						FROM
							YEAR_CLASS yc INNER JOIN
							YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID) INNER JOIN
							YEAR y ON (y.YearID=yc.YearID) INNER JOIN
							INTRANET_USER USR ON (USR.UserID=ycu.UserID)
						WHERE
							yc.YearClassID='$ClassID' AND
							USR.RecordStatus in (0,1,2)
						ORDER BY
							y.Sequence, yc.Sequence, ycu.ClassNumber";
			
				$result = $this->returnArray($sql,5);
			}
			
			return $result;
		}
		
		function getSelectClass($attrib, $selected="", $firstValue="",$displayAllClassValue=true, $firstValueOptValue="")
		{
			global $intranet_session_language;

			$currentYearID = Get_Current_Academic_Year_ID();

			$sql = "SELECT b.YearClassID, b.ClassTitleEN, b.ClassTitleB5 FROM YEAR AS a INNER JOIN YEAR_CLASS AS b ON (a.YearID = b.YearID) WHERE b.AcademicYearID='$currentYearID' ORDER BY a.Sequence, b.Sequence";
			$result = $this->returnArray($sql, 2);

			$x = "<SELECT $attrib>\n";
			$empty_selected = ($selected == '')? "SELECTED":"";
			if($displayAllClassValue)
			{
				$x .= "<OPTION value='". $firstValueOptValue ."' $empty_selected>$firstValue</OPTION>\n";
			}

			for ($i=0; $i < sizeof($result); $i++)
			{
				list($id, $nameEN, $nameB5) = $result[$i];
				$name = Get_Lang_Selection($nameB5, $nameEN);
				$selected_str = ($id==$selected? "SELECTED":"");
				$x .= "<OPTION value='$id' $selected_str>$name</OPTION>\n";
			}
			$x .= "</SELECT>\n";

			return $x;
		}
		
		function getSelectClassWithWholeForm($attrib, $selected="", $firstValue="", $pleaseSelect="", $year="")
		{
			global $i_general_all;
			$year = ($year=="") ? Get_Current_Academic_Year_ID() : $year;

			//$sql = "SELECT LVL.ClassLevelID, LVL.LevelName FROM INTRANET_CLASSLEVEL LVL left outer join INTRANET_CLASS CLS on (LVL.ClassLevelID=CLS.ClassLevelID) GROUP BY LVL.ClassLevelID";
			$sql = "SELECT YearID, YearName FROM YEAR ORDER BY Sequence";
			$form = $this->returnArray($sql,3);

			$x = "<SELECT $attrib>\n";
			$empty_selected = ($selected == '')? "SELECTED":"";
							
			if($firstValue!="") {
				$x .= "<OPTION value=''";
				$x .= ($selected=='') ? " selected" : "";
				$x .=">$firstValue</OPTION>\n";
			}

			if(!is_numeric($selected)) $selectedClass = substr($selected, 2);

			for($i=0;$i<sizeof($form);$i++) {
				list($temp, $tempLvlName) = $form[$i];

				//$sql2 = "SELECT ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 AND ClassLevelID=$temp ORDER BY ClassName";
				$sql2 = "SELECT ClassTitleB5, ClassTitleEN, YearClassID FROM YEAR_CLASS WHERE AcademicYearID='$year' AND YearID='$temp' ORDER BY Sequence, ".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
				$result = $this->returnArray($sql2,3);

				$selected_str = ($selected == "{$temp}")? "SELECTED":"";
				$x .= "<option value=\"{$temp}\" {$selected_str}>{$i_general_all} {$tempLvlName}</option>";

				for($j=0;$j<sizeof($result);$j++) {
					$tempName = Get_Lang_Selection($result[$j][0], $result[$j][1]);
					$clsID = $result[$j][2];
					$selected_str = ($clsID==$selectedClass? "SELECTED":"");
					$x .= "<OPTION value='::{$clsID}' {$selected_str}>&nbsp;&nbsp;&nbsp;{$tempName}</OPTION>\n";
				}
			}
			$x .= "</select>";
			return $x;
		}
			
		function storeStudent($stdid, $class, $academicYearID="",$includeLeft=false)	# all student (all classes / whole form / each class)
		{
			$dataAry = array();

			$academicYearID = ($academicYearID=="") ? Get_Current_Academic_Year_ID() : $academicYearID;
			if ($includeLeft) {
				$recordTypeCond = " AND USR.RecordType IN (2,4)";	# 2 - Student, 4 - Alumni
				$recordStatusCond = " AND USR.RecordStatus IN (0,1,2,3)";	# 3 - left
			}
			else {
				$recordTypeCond = " AND USR.RecordType IN (2)";
				$recordStatusCond = " AND USR.RecordStatus IN (0,1,2)";
			}
			
			if($stdid != '0') {								# specific student
				$sql = "SELECT UserID FROM INTRANET_USER USR WHERE USR.UserID='$stdid' {$recordTypeCond} {$recordStatusCond}";
//				if ($includeLeft) {
//					$sql .= " UNION SELECT UserID FROM INTRANET_ARCHIVE_USER WHERE UserID='$stdid' AND RecordType=2";
//				}
				$result = $this->returnArray($sql,1);

				for($i=0;$i<sizeof($result);$i++) {
					$dataAry[] = $result[$i][0];
				}
				
			} else if(empty($class)) {						# all classes all students
				$class = substr($class, 2);
				//$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) ORDER BY ClassName, ClassNumber";
				$sql = "SELECT 
								ycu.UserID 
						FROM 
								YEAR_CLASS_USER ycu 
						LEFT OUTER JOIN 
								YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$academicYearID') 
						LEFT OUTER JOIN 
								YEAR y ON (y.YearID=yc.YearID) 
						INNER JOIN 
								INTRANET_USER USR ON (USR.UserID=ycu.UserID {$recordTypeCond} {$recordStatusCond}) 
						WHERE 
								yc.AcademicYearID='$academicYearID' 
						ORDER BY 
								y.Sequence, yc.Sequence, ycu.ClassNumber";
								
//				if ($includeLeft) {
//					$sql .= " UNION SELECT 
//									ycu.UserID 
//							FROM 
//									YEAR_CLASS_USER ycu 
//							LEFT OUTER JOIN 
//									YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$academicYearID') 
//							LEFT OUTER JOIN 
//									YEAR y ON (y.YearID=yc.YearID) 
//							INNER JOIN 
//									INTRANET_ARCHIVE_USER au ON (au.UserID=ycu.UserID AND au.RecordType=2) 
//							WHERE 
//									yc.AcademicYearID='$academicYearID'  
//							ORDER BY 
//									y.Sequence, yc.Sequence, ycu.ClassNumber";
//				}								
								
				$result = $this->returnArray($sql,1);

				for($j=0;$j<sizeof($result);$j++) {
					$dataAry[] = $result[$j][0];
				}
				
			} else if(is_numeric($class)) {			# all students of specific form
				$sql = "SELECT 
								USR.UserID 
						FROM 
								INTRANET_USER USR 
						LEFT OUTER JOIN 
								YEAR_CLASS_USER ycu ON (USR.UserID=ycu.UserID) 
						LEFT OUTER JOIN 
								YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) 
						LEFT OUTER JOIN 
								YEAR y ON (y.YearID=yc.YearID) 
						WHERE 
								y.YearID=$class 
						AND 
								yc.AcademicYearID='$academicYearID' 
						{$recordTypeCond} {$recordStatusCond}
						ORDER BY
								yc.Sequence, ycu.ClassNumber";

//				if ($includeLeft) {
//					$sql = " UNION SELECT 
//									au.UserID 
//							FROM 
//									INTRANET_ARCHIVE_USER au 
//							LEFT OUTER JOIN 
//									YEAR_CLASS_USER ycu ON (au.UserID=ycu.UserID) 
//							LEFT OUTER JOIN 
//									YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) 
//							LEFT OUTER JOIN 
//									YEAR y ON (y.YearID=yc.YearID) 
//							WHERE 
//									y.YearID=$class 
//							AND 
//									yc.AcademicYearID='$academicYearID'  
//							ORDER BY
//									yc.Sequence, ycu.ClassNumber";
//				}								
				$result = $this->returnArray($sql,1);

				for($i=0;$i<sizeof($result);$i++) {
					$dataAry[] = $result[$i][0];
				}
				

			} else {										# specific class , all students
				$class = substr($class, 2);
				$sql = "SELECT 
								USR.UserID 
						FROM 
								INTRANET_USER USR 
						LEFT OUTER JOIN 
								YEAR_CLASS_USER ycu ON (USR.UserID=ycu.UserID) 
						LEFT OUTER JOIN 
								YEAR_CLASS yc ON (ycu.YearClassID=yc.YearClassID) 
						WHERE 
								yc.YearClassID='$class' 
						{$recordTypeCond} {$recordStatusCond}
						ORDER BY 
								ycu.ClassNumber";
//				if ($includeLeft) {
//					$sql .= " UNION SELECT 
//									au.UserID 
//							FROM 
//									INTRANET_ARCHIVE_USER au 
//							LEFT OUTER JOIN 
//									YEAR_CLASS_USER ycu ON (USR.UserID=ycu.UserID) 
//							LEFT OUTER JOIN 
//									YEAR_CLASS yc ON (ycu.YearClassID=yc.YearClassID) 
//							WHERE 
//									yc.YearClassID='$class'
//							ORDER BY 
//									ycu.ClassNumber";
//				}								
								
				$result = $this->returnArray($sql,1);

				for($j=0;$j<sizeof($result);$j++) {
					$dataAry[] = $result[$j][0];
				}

			}

			return $dataAry;
		}
			
		function getRegistryStatusMenu($status="", $tag="")
		{
			global $Lang;
			
			$registryStatusMenu = '<select name="registryStatus" id="registryStatus" '.$tag.'>';
			$selected_str = ($status==""? "SELECTED":"");
			$registryStatusMenu .= '<option value="" '.$selected_str.'>'.$Lang['StudentRegistry']['AllRegistryStatus'].'</option>';
			foreach($Lang['StudentRegistry']['RegistryStatusArray'] as $id=>$val) {
				$selected_str = ($status!="" && $status==$id ? "SELECTED":"");
				$registryStatusMenu .= '<option value="'.$id.'" '.$selected_str.'>'.$val.'</option>';
			}
			$registryStatusMenu .= '</select>';	
			return $registryStatusMenu;
		}

		function getRecordStatusMenu_Malaysia($status="", $tag="")
		{
			global $Lang;
			
			$registryStatusMenu = '<select name="recordstatus" id="recordstatus" '.$tag.'>';
			$registryStatusMenu .= '<option value="" '.($status==""? "SELECTED":"").'>'.$Lang['StudentRegistry']['AllRegistryStatus'].'</option>';
			$registryStatusMenu .= '<option value="'.STUDENT_REGISTRY_RECORDSTATUS_NORMAL.'" '.($status==STUDENT_REGISTRY_RECORDSTATUS_NORMAL? "SELECTED":"").'>'.$Lang['StudentRegistry']['StatusApproved'].'</option>';
			$registryStatusMenu .= '<option value="'.STUDENT_REGISTRY_RECORDSTATUS_LEFT.'" '.($status==STUDENT_REGISTRY_RECORDSTATUS_LEFT? "SELECTED":"").'>'.$Lang['StudentRegistry']['StatusLeft'].'</option>';
			$registryStatusMenu .= '<option value="'.STUDENT_REGISTRY_RECORDSTATUS_SUSPEND.'" '.($status==STUDENT_REGISTRY_RECORDSTATUS_SUSPEND? "SELECTED":"").'>'.$Lang['StudentRegistry']['StatusSuspended'].'</option>';
			$registryStatusMenu .= '<option value="'.STUDENT_REGISTRY_RECORDSTATUS_RESUME.'" '.($status==STUDENT_REGISTRY_RECORDSTATUS_RESUME? "SELECTED":"").'>'.$Lang['StudentRegistry']['StatusResume'].'</option>';
			$registryStatusMenu .= '</select>';	
			return $registryStatusMenu;
		}

		function getRecordStatusMenu($status="", $tag="")
		{
			global $Lang;
			
			$recordStatusMenu = '<select name="recordstatus" id="recordstatus" '.$tag.'>';
			$selected_str = ($status==""? "SELECTED":"");
			$recordStatusMenu .= '<option value="">'.$Lang['StudentRegistry']['AllRecordStatus'].'</option>';
			foreach($Lang['StudentRegistry']['RecordStatusArray'] as $id=>$val) {
				$selected_str = ($status!="" && $status==$id ? "SELECTED":"");
				$recordStatusMenu .= '<option value="'.$id.'" '.$selected_str.'>'.$val.'</option>';
			}
				
			return $recordStatusMenu;
		}
		
		function getParentGuardianInfo($userid, $pg_type="")
		{
			if($pg_type != "") $conds = " AND PG_TYPE='$pg_type'";
			
			$sql = "SELECT * FROM STUDENT_REGISTRY_PG WHERE StudentID='$userid' $conds";
			$result = $this->returnArray($sql);
			
			return $result[0];
		}
		
		function checkData_Macau($flag="", $dataAry=array(), $mode=1) 
		{
			## only "Class Name" , "Class No", "Chinese Name" & "Gender" are compulsory
			global $Lang, $i_general_name;
		
			$CLASS 			= trim($dataAry['CLASS']);
			$C_NO 			= trim($dataAry['C_NO']);
			$UserLogin 		= trim($dataAry['UserLogin']);
			$STUD_ID 		= trim($dataAry['STUD_ID']);
			$CODE 			= trim($dataAry['CODE']);
			$NAME_C 		= trim($dataAry['NAME_C']);
			$NAME_E 		= trim($dataAry['NAME_E']);
			$ENTRY_DATE 	= trim($dataAry['ENTRY_DATE']);
			$S_CODE 		= trim($dataAry['S_CODE']);
			$SEX 			= trim($dataAry['SEX']);
			$B_DATE 		= trim($dataAry['B_DATE']);
			$B_PLACE 		= trim($dataAry['B_PLACE']);
			$ID_TYPE 		= trim($dataAry['ID_TYPE']);
			$ID_NO 			= trim($dataAry['ID_NO']);
			$I_PLACE 		= trim($dataAry['I_PLACE']);
			$I_DATE 		= trim($dataAry['I_DATE']);
			$V_DATE 		= trim($dataAry['V_DATE']);
			$S6_TYPE 		= trim($dataAry['S6_TYPE']);
			$S6_IDATE 		= trim($dataAry['S6_IDATE']);
			$S6_VDATE 		= trim($dataAry['S6_VDATE']);
			$NATION 		= trim($dataAry['NATION']);
			$ORIGIN 		= trim($dataAry['ORIGIN']);
			$RELIGION 		= trim($dataAry['RELIGION']);
			$EMAIL 			= trim($dataAry['EMAIL']);
			$TEL 			= trim($dataAry['TEL']);
			$R_AREA 		= trim($dataAry['R_AREA']);
			$RA_DESC 		= trim($dataAry['RA_DESC']);
			$AREA 			= trim($dataAry['AREA']);
			$ROAD 			= trim($dataAry['ROAD']);
			$ADDRESS 		= trim($dataAry['ADDRESS']);
			
			$FATHER_C 		= trim($dataAry['FATHER_C']);
			$FATHER_E 		= trim($dataAry['FATHER_E']);
			$F_PROF 		= trim($dataAry['F_PROF']);
			$F_MARTIAL 		= trim($dataAry['F_MARTIAL']);
			$F_TEL 			= trim($dataAry['F_TEL']);
			$F_MOBILE 		= trim($dataAry['F_MOBILE']);
			$F_OFFICE 		= trim($dataAry['F_OFFICE']);
			$F_EMAIL 		= trim($dataAry['F_EMAIL']);
			
			$MOTHER_C 		= trim($dataAry['MOTHER_C']);
			$MOTHER_E 		= trim($dataAry['MOTHER_E']);
			$M_PROF 		= trim($dataAry['M_PROF']);
			$M_MARTIAL 		= trim($dataAry['M_MARTIAL']);
			$M_TEL 			= trim($dataAry['M_TEL']);
			$M_MOBILE 		= trim($dataAry['M_MOBILE']);
			$M_OFFICE 		= trim($dataAry['M_OFFICE']);
			$M_EMAIL 		= trim($dataAry['M_EMAIL']);
			
			$GUARDIAN_C 	= trim($dataAry['GUARDIAN_C']);
			$GUARDIAN_E 	= trim($dataAry['GUARDIAN_E']);
			$G_SEX 			= trim($dataAry['G_SEX']);
			$G_PROF 		= trim($dataAry['G_PROF']);
			$GUARD 			= trim($dataAry['GUARD']);
			$G_REL_OTHERS 	= trim($dataAry['G_REL_OTHERS']);
			$LIVE_SAME 		= trim($dataAry['LIVE_SAME']);
			$G_AREA 		= trim($dataAry['G_AREA']);
			$G_ROAD 		= trim($dataAry['G_ROAD']);
			$G_ADDRESS 		= trim($dataAry['G_ADDRESS']);
			$G_TEL 			= trim($dataAry['G_TEL']);
			$G_MOBILE 		= trim($dataAry['G_MOBILE']);
			$G_OFFICE 		= trim($dataAry['G_OFFICE']);
			$G_EMAIL 		= trim($dataAry['G_EMAIL']);
			
			$EC_NAME_C 		= trim($dataAry['EC_NAME_C']);
			$EC_NAME_E 		= trim($dataAry['EC_NAME_E']);
			$EC_REL 		= trim($dataAry['EC_REL']);
			$EC_TEL 		= trim($dataAry['EC_TEL']);
			$EC_MOBILE 		= trim($dataAry['EC_MOBILE']);
			$EC_AREA 		= trim($dataAry['EC_AREA']);
			$EC_ROAD 		= trim($dataAry['EC_ROAD']);
			$EC_ADDRESS 	= trim($dataAry['EC_ADDRESS']);
			
			### Data Checking [Start] ###
			
			$error_msg = array();
			
			if($flag!="edit") {
				# Check any existing student
				$studentID = $this->retrieveStudentIDByClassOrLogin($CLASS, $C_NO, $UserLogin);

				if(($CLASS=="" || $C_NO=="") && $UserLogin=="")
					$error_msg['UserInfo'] = $Lang['StudentRegistry']['StudentInfo'].$Lang['StudentRegistry']['IsMissing'];
				else if($studentID=="" || $studentID==0)
					$error_msg['UserInfo'] = $Lang['StudentRegistry']['StudentInfo'].$Lang['StudentRegistry']['IsInvalid'];
				
			}
			/*	
			# Check Student ID
			if($STUD_ID=="")	
				$error_msg['STUD_ID'] = $Lang['StudentRegistry']['Import_Macau'][3].$Lang['StudentRegistry']['IsMissing'];
				
			# Check DSEJ CODE
			if($CODE=="")
				$error_msg['CODE'] = $Lang['StudentRegistry']['Import_Macau'][4].$Lang['StudentRegistry']['IsMissing'];
			else if(!is_numeric(substr($CODE,0,7)) || substr($CODE,7,1)!="-")
				$error_msg['CODE'] = $Lang['StudentRegistry']['Import_Macau'][4].$Lang['StudentRegistry']['IsInvalid'];
			*/
			# Check name
			if($NAME_C=="")
				$error_msg['C_NAME'] = $Lang['StudentRegistry']['Import_Macau'][5].$Lang['StudentRegistry']['IsMissing'];

			# Check gender
			$genderAry = $this->GENDER_ARY();
			if($SEX=="")
				$error_msg['SEX'] = $Lang['StudentRegistry']['Import_Macau'][7].$Lang['StudentRegistry']['IsMissing'];
			else if(!array_key_exists($SEX, $genderAry))
				$error_msg['SEX'] = $Lang['StudentRegistry']['Import_Macau'][7].$Lang['StudentRegistry']['IsInvalid'];
			/*	
			# check date format of DOB
			if($mode)
			{
				if($B_DATE=="")	
					$error_msg['B_DATE'] = $Lang['StudentRegistry']['Import_Macau'][8].$Lang['StudentRegistry']['IsMissing'];
				else if(!$this->checkDateFormat($B_DATE))
					$error_msg['B_DATE'] = $Lang['StudentRegistry']['Import_Macau'][8].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check Place of Birth
			$B_PLACE_Ary = $this->MACAU_B_PLACE_DATA_ARY();
			if($mode)
			{
				if($B_PLACE=="")
					$error_msg['B_PLACE'] = $Lang['StudentRegistry']['Import_Macau'][9].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($B_PLACE, $B_PLACE_Ary))
					$error_msg['B_PLACE'] = $Lang['StudentRegistry']['Import_Macau'][9].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check NATION
			$NATION_Ary = $this->MACAU_NATION_DATA_ARY();
			if($mode)
			{
				if($NATION=="")
					$error_msg['NATION'] = $Lang['StudentRegistry']['Import_Macau'][10].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($NATION, $NATION_Ary))
					$error_msg['NATION'] = $Lang['StudentRegistry']['Import_Macau'][10].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check ORIGIN
			if($ORIGIN=="" && $mode)
				$error_msg['ORIGIN'] = $Lang['StudentRegistry']['Import_Macau'][11].$Lang['StudentRegistry']['IsMissing'];
			
			# Check S_CODE
			$S_CODE_Ary = $this->MACAU_S_CODE_DATA_ARY();
			if($S_CODE=="")	
				$error_msg['S_CODE'] = $Lang['StudentRegistry']['Import_Macau'][13].$Lang['StudentRegistry']['IsMissing'];
			else if(!array_key_exists($S_CODE, $S_CODE_Ary))
				$error_msg['S_CODE'] = $Lang['StudentRegistry']['Import_Macau'][13].$Lang['StudentRegistry']['IsInvalid'];			
			
			# check entry date	
			if($ENTRY_DATE!="" && $mode) {
				if(!$this->checkDateFormat($ENTRY_DATE)) {
					$error_msg['ENTRY_DATE'] = $Lang['StudentRegistry']['Import_Macau'][14].$Lang['StudentRegistry']['IsInvalid'];	
				}
			}
			
			# check ID TYPE
			$id_type_ary = $this->MACAU_ID_TYPE_DATA_ARY();
			if($mode)
			{
				if($ID_TYPE=="")
					$error_msg['ID_TYPE'] = $Lang['StudentRegistry']['Import_Macau'][15].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($ID_TYPE, $id_type_ary))
					$error_msg['ID_TYPE'] = $Lang['StudentRegistry']['Import_Macau'][15].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# Check ID no
			if($ID_NO=="" && $mode)
				$error_msg['ID_NO'] = $Lang['StudentRegistry']['Import_Macau'][16].$Lang['StudentRegistry']['IsMissing'];
				
			# check ID issue place
			$id_place_ary = $this->MACAU_ID_PLACE_DATA_ARY();
			if($mode)
			{
				if($I_PLACE=="")
					$error_msg['ID_PLACE'] = $Lang['StudentRegistry']['Import_Macau'][17].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($I_PLACE, $id_place_ary))
					$error_msg['ID_PLACE'] = $Lang['StudentRegistry']['Import_Macau'][17].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check ID issue date
			if($mode)
			{
				if($I_DATE=="")
					$error_msg['I_DATE'] = $Lang['StudentRegistry']['Import_Macau'][18].$Lang['StudentRegistry']['IsMissing'];
				else if(!$this->checkDateFormat($I_DATE) || compareDate(mktime(0,0,0,substr($I_DATE,8,2), substr($I_DATE,5,2), substr($I_DATE, 0,4)), time())==1)
					$error_msg['I_DATE'] = $Lang['StudentRegistry']['Import_Macau'][18].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check ID valid date
			if($mode)
			{
				if($V_DATE=="")
					$error_msg['V_DATE'] = $Lang['StudentRegistry']['Import_Macau'][19].$Lang['StudentRegistry']['IsMissing'];
				else if(!$this->checkDateFormat($V_DATE) || compareDate(mktime(0,0,0,substr($V_DATE,8,2), substr($V_DATE,5,2), substr($V_DATE, 0,4)), time())==-1)
					$error_msg['V_DATE'] = $Lang['StudentRegistry']['Import_Macau'][19].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check S6 Type
			$s6_type_ary = $this->MACAU_S6_TYPE_DATA_ARY();
			if($mode)
			{
				if($S6_TYPE=="")
					$error_msg['S6_TYPE'] = $Lang['StudentRegistry']['Import_Macau'][20].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($S6_TYPE, $s6_type_ary))
					$error_msg['S6_TYPE'] = $Lang['StudentRegistry']['Import_Macau'][20].$Lang['StudentRegistry']['IsInvalid'];
			}
				
			# check S6 Issue Date
			if($mode)
			{
				if($S6_IDATE=="")
					$error_msg['S6_IDATE'] = $Lang['StudentRegistry']['Import_Macau'][21].$Lang['StudentRegistry']['IsMissing'];
				else if(!$this->checkDateFormat($S6_IDATE) || compareDate(mktime(0,0,0,substr($S6_IDATE,8,2), substr($S6_IDATE,5,2), substr($S6_IDATE, 0,4)), time())==1)
					$error_msg['S6_IDATE'] = $Lang['StudentRegistry']['Import_Macau'][21].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check S6 valid date
			if($mode)
			{
				if($S6_TYPE==2 || $S6_TYPE==3) {
					if($S6_VDATE=="")
						$error_msg['S6_VDATE'] = $Lang['StudentRegistry']['Import_Macau'][22].$Lang['StudentRegistry']['IsMissing'];
					else if(!$this->checkDateFormat($S6_VDATE) || compareDate(mktime(0,0,0,substr($S6_VDATE,8,2), substr($S6_VDATE,5,2), substr($S6_VDATE, 0,4)), time())==-1)
						$error_msg['S6_VDATE'] = $Lang['StudentRegistry']['Import_Macau'][22].$Lang['StudentRegistry']['IsInvalid'];
				}
			}
			
			# Check Tel
			if($TEL=="")
				$error_msg['TEL'] = $Lang['StudentRegistry']['Import_Macau'][23].$Lang['StudentRegistry']['IsMissing'];
			
			# check EMAIL
			if($mode)
			{
				if($EMAIL!="" && !intranet_validateEmail($EMAIL))
					$error_msg['EMAIL'] = $Lang['StudentRegistry']['Import_Macau'][24].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check Resident Area
			$r_area_ary = $this->MACAU_R_AREA_DATA_ARY();
			if($mode)
			{
				if($R_AREA=="")
					$error_msg['R_AREA'] = $Lang['StudentRegistry']['Import_Macau'][25].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($R_AREA, $r_area_ary))
					$error_msg['R_AREA'] = $Lang['StudentRegistry']['Import_Macau'][25].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check Resident Area (Others)
			if($mode)
			{
				if($R_AREA=="O" && $RA_DESC=="")
					$error_msg['RA_DESC'] = $Lang['StudentRegistry']['Import_Macau'][26].$Lang['StudentRegistry']['IsMissing'];
			}
			
			# check Area
			$AREA_Ary = $this->MACAU_AREA_DATA_ARY();
			if($mode)
			{
				if($AREA=="")
					$error_msg['AREA'] = $Lang['StudentRegistry']['Import_Macau'][27].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($AREA, $AREA_Ary))
					$error_msg['AREA'] = $Lang['StudentRegistry']['Import_Macau'][27].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check Road
			if($ROAD=="" && $mode)
				$error_msg['ROAD'] = $Lang['StudentRegistry']['Import_Macau'][28].$Lang['StudentRegistry']['IsMissing'];
				
			# check address
			if($ADDRESS=="" && $mode)
				$error_msg['ADDRESS'] = $Lang['StudentRegistry']['Import_Macau'][29].$Lang['StudentRegistry']['IsMissing'];
			
			# Check father name
			if($mode)
			{
				if($FATHER_E=="")
					$error_msg['FATHER_E'] = $Lang['StudentRegistry']['Import_Macau'][31].$Lang['StudentRegistry']['IsMissing'];
			}
			
			# check Father profession
			if($F_PROF=="" && $mode)	
				$error_msg['F_PROF'] = $Lang['StudentRegistry']['Import_Macau'][32].$Lang['StudentRegistry']['IsMissing'];

				
			# check father marital status
			$martial_ary = $this->MACAU_MARITAL_STATUS_DATA_ARY();
			if($mode)
			{
				if($F_MARTIAL!="" && !array_key_exists($F_MARTIAL, $martial_ary))
					$error_msg['F_MARTIAL'] = $Lang['StudentRegistry']['Import_Macau'][33].$Lang['StudentRegistry']['IsInvalid'];
			}
	
			# check father EMAIL
			if($mode)
			{
				if($F_EMAIL!="" && !intranet_validateEmail($F_EMAIL))
					$error_msg['F_EMAIL'] = $Lang['StudentRegistry']['Import_Macau'][37].$Lang['StudentRegistry']['IsInvalid'];
			}
		
			
			# Check mother name
			if($mode)			
			{
				if($MOTHER_E=="")
					$error_msg['MOTHER_E'] = $Lang['StudentRegistry']['Import_Macau'][39].$Lang['StudentRegistry']['IsMissing'];
			}
				
			# check mother profession
			if($M_PROF=="" && $mode)	
				$error_msg['M_PROF'] = $Lang['StudentRegistry']['Import_Macau'][40].$Lang['StudentRegistry']['IsMissing'];

			
			# check mother martial status
			if($mode)
			{
				if($M_MARTIAL!="" && !array_key_exists($M_MARTIAL, $martial_ary))
					$error_msg['M_MARTIAL'] = $Lang['StudentRegistry']['Import_Macau'][41].$Lang['StudentRegistry']['IsInvalid'];
			}			

			# check mother EMAIL
			if($mode)
			{
				if($M_EMAIL!="" && !intranet_validateEmail($M_EMAIL))
					$error_msg['M_EMAIL'] = $Lang['StudentRegistry']['Import_Macau'][45].$Lang['StudentRegistry']['IsInvalid'];
			}

			
			# Check guardian name
			if($GUARD=="O" && $mode)
			{
				if($GUARDIAN_E=="")
					$error_msg['GUARDIAN_E'] = $Lang['StudentRegistry']['Import_Macau'][47].$Lang['StudentRegistry']['IsMissing'];
			}
			
			# Check gender
			if(!array_key_exists($G_SEX, $genderAry) && $mode)
				$error_msg['G_SEX'] = $Lang['StudentRegistry']['Import_Macau'][48].$Lang['StudentRegistry']['IsInvalid'];
					
			# check relationship with student
			$guard_ary = $this->MACAU_GUARD_DATA_ARY();
			if($mode)
			{
				if($GUARD=="")
					$error_msg['GUARD'] = $Lang['StudentRegistry']['Import_Macau'][49].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($GUARD, $guard_ary))
					$error_msg['GUARD'] = $Lang['StudentRegistry']['Import_Macau'][49].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check relationship (others)
			if($GUARD=="O" && $G_REL_OTHERS=="" && $mode)
				$error_msg['G_REL_OTHERS'] = $Lang['StudentRegistry']['Import_Macau'][50].$Lang['StudentRegistry']['IsMissing'];

			# check live together
			$live_same_ary = $this->MACAU_LIVE_SAME_ARY();
			if($mode)
			{
				if($LIVE_SAME=="")				
					$error_msg['LIVE_SAME'] = $Lang['StudentRegistry']['Import_Macau'][51].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($LIVE_SAME, $live_same_ary))
					$error_msg['LIVE_SAME'] = $Lang['StudentRegistry']['Import_Macau'][51].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check Guardian profession
			if($GUARD=="O" && $G_PROF=="" && $mode)	
				$error_msg['G_PROF'] = $Lang['StudentRegistry']['Import_Macau'][52].$Lang['StudentRegistry']['IsMissing'];
					
			# check guardian home telephone
			if($GUARD=="O" && $G_TEL=="" && $mode)
				$error_msg['G_TEL'] = $Lang['StudentRegistry']['Import_Macau'][53].$Lang['StudentRegistry']['IsMissing'];
			
			# check guardian mobile	
			if($GUARD=="O" && $G_MOBILE=="" && $mode)
				$error_msg['G_MOBILE'] = $Lang['StudentRegistry']['Import_Macau'][54].$Lang['StudentRegistry']['IsMissing'];
							
			# check guardian Area
			if($GUARD=="O" && $G_AREA=="" && $mode)
				$error_msg['G_AREA'] = $Lang['StudentRegistry']['Import_Macau'][56].$Lang['StudentRegistry']['IsMissing'];
				
			# check guardian Road
			if($GUARD=="O" && $G_ROAD=="" && $mode)
				$error_msg['G_ROAD'] = $Lang['StudentRegistry']['Import_Macau'][57].$Lang['StudentRegistry']['IsMissing'];
				
			# check guardian address
			if($GUARD=="O" && $G_ADDRESS=="" && $mode)
				$error_msg['G_ADDRESS'] = $Lang['StudentRegistry']['Import_Macau'][58].$Lang['StudentRegistry']['IsMissing'];

			# check EMAIL
			if($G_EMAIL!="" && !intranet_validateEmail($G_EMAIL) && $mode)
				$error_msg['G_EMAIL'] = $Lang['StudentRegistry']['Import_Macau'][59].$Lang['StudentRegistry']['IsInvalid'];
				
			# Check emergency contact person
			if($EC_REL=="O" && $mode)
			{
				if($EC_NAME_E=="")
					$error_msg['EC_NAME_E'] = $Lang['StudentRegistry']['Import_Macau'][61].$Lang['StudentRegistry']['IsMissing'];
			}
			
			# check emergency contact person relationship with student
			if($mode)
			{
				if($EC_REL=="")
					$error_msg['EC_REL'] = $Lang['StudentRegistry']['Import_Macau'][62].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($EC_REL, $guard_ary))
					$error_msg['EC_REL'] = $Lang['StudentRegistry']['Import_Macau'][62].$Lang['StudentRegistry']['IsInvalid'];
			}
				
			# check emergenc contact telephone
			if($EC_REL=="O" && $EC_TEL=="" && $mode)
				$error_msg['EC_TEL'] = $Lang['StudentRegistry']['Import_Macau'][63].$Lang['StudentRegistry']['IsMissing'];

			# check emergenc contact telephone
			if($EC_REL=="O" && $EC_MOBILE=="" && $mode)
				$error_msg['EC_MOBILE'] = $Lang['StudentRegistry']['Import_Macau'][64].$Lang['StudentRegistry']['IsMissing'];
								
			# check emergency contact person Area
			if($EC_REL=="O" && $EC_AREA=="" && $mode)
				$error_msg['EC_AREA'] = $Lang['StudentRegistry']['Import_Macau'][66].$Lang['StudentRegistry']['IsMissing'];
				
			# check emergency contact person Road
			if($EC_REL=="O" && $EC_ROAD=="" && $mode)
				$error_msg['EC_ROAD'] = $Lang['StudentRegistry']['Import_Macau'][67].$Lang['StudentRegistry']['IsMissing'];
				
			# check emergency contact person address
			if($EC_REL=="O" && $EC_ADDRESS=="" && $mode)
				$error_msg['EC_ADDRESS'] = $Lang['StudentRegistry']['Import_Macau'][68].$Lang['StudentRegistry']['IsMissing'];
			*/
			### Data Checking [Start] ###
			
			return $error_msg;
		}
		
		function checkData_Mal($flag="", $dataAry=array(), $mode=1) 
		{
			Global $Lang, $i_general_name;
		
			$CLASS 			= trim($dataAry['CLASS']);
			$C_NO 			= trim($dataAry['C_NO']);
			$UserLogin 		= trim($dataAry['UserLogin']);
			
			$NAME_C 		= trim($dataAry['NAME_C']);
			$NAME_E 		= trim($dataAry['NAME_E']);
			$SEX 			= trim($dataAry['SEX']);
			$B_DATE 		= trim($dataAry['B_DATE']);
			$TEL 			= trim($dataAry['TEL']);
			
			$STUD_ID 		= trim($dataAry['STUD_ID']);
			$B_PLACE		= trim($dataAry['B_PLACE']);
			$NATION 		= trim($dataAry['NATION']);
			$ANCESTRAL_HOME	= trim($dataAry['ANCESTRAL_HOME']);
			$RACE			= trim($dataAry['RACE']);
			$ID_NO 			= trim($dataAry['ID_NO']);
			$BIRTH_CERT_NO	= trim($dataAry['BIRTH_CERT_NO']);
			$RELIGION 		= trim($dataAry['RELIGION']);
			$FAMILY_LANG	= trim($dataAry['FAMILY_LANG']);
			$PAYMENT_TYPE	= trim($dataAry['PAYMENT_TYPE']);
			$EMAIL 			= trim($dataAry['EMAIL']);
			$LODGING		= trim($dataAry['LODGING']);
			$ADDRESS1		= trim($dataAry['ADDRESS1']);
			$ADDRESS2		= trim($dataAry['ADDRESS2']);
			$ADDRESS3		= trim($dataAry['ADDRESS3']);
			$ADDRESS4		= trim($dataAry['ADDRESS4']);
			$ADDRESS5		= trim($dataAry['ADDRESS5']);
			$ADDRESS6		= trim($dataAry['ADDRESS6']);
			$ENTRY_DATE 	= trim($dataAry['ENTRY_DATE']);
			$SIBLINGS_ARY	= explode(",", $dataAry['SIBLINGS']);
			$PRI_SCHOOL		= trim($dataAry['PRI_SCHOOL']);
			
			$FATHER_C		= trim($dataAry['FATHER_C']);
			$FATHER_E		= trim($dataAry['FATHER_E']);
			$F_PROF			= trim($dataAry['F_PROF']);
			$F_JOB_NATURE	= trim($dataAry['F_JOB_NATURE']);
			$F_JOB_TITLE	= trim($dataAry['F_JOB_TITLE']);
			$F_MARITAL		= trim($dataAry['F_MARITAL']);
			$F_EDU_LEVEL	= trim($dataAry['F_EDU_LEVEL']);
			$F_TEL 			= trim($dataAry['F_TEL']);
			$F_MOBILE 		= trim($dataAry['F_MOBILE']);
			$F_OFFICE 		= trim($dataAry['F_OFFICE']);
			$F_POSTAL_ADDR1 = trim($dataAry['F_POSTAL_ADDR1']);
			$F_POSTAL_ADDR2 = trim($dataAry['F_POSTAL_ADDR2']);
			$F_POSTAL_ADDR3 = trim($dataAry['F_POSTAL_ADDR3']);
			$F_POSTAL_ADDR4 = trim($dataAry['F_POSTAL_ADDR4']);
			$F_POSTAL_ADDR5 = trim($dataAry['F_POSTAL_ADDR5']);
			$F_POSTAL_ADDR6 = trim($dataAry['F_POSTAL_ADDR6']);
			$F_POST_CODE	= trim($dataAry['F_POST_CODE']);
			$F_EMAIL 		= trim($dataAry['F_EMAIL']);
			
			$MOTHER_C 		= trim($dataAry['MOTHER_C']);
			$MOTHER_E 		= trim($dataAry['MOTHER_E']);
			$M_PROF			= trim($dataAry['M_PROF']);
			$M_JOB_NATURE	= trim($dataAry['M_JOB_NATURE']);
			$M_JOB_TITLE	= trim($dataAry['M_JOB_TITLE']);
			$M_MARITAL		= trim($dataAry['M_MARITAL']);
			$M_EDU_LEVEL	= trim($dataAry['M_EDU_LEVEL']);
			$M_TEL 			= trim($dataAry['M_TEL']);
			$M_MOBILE 		= trim($dataAry['M_MOBILE']);
			$M_OFFICE 		= trim($dataAry['M_OFFICE']);
			$M_POSTAL_ADDR1 = trim($dataAry['M_POSTAL_ADDR1']);
			$M_POSTAL_ADDR2 = trim($dataAry['M_POSTAL_ADDR2']);
			$M_POSTAL_ADDR3 = trim($dataAry['M_POSTAL_ADDR3']);
			$M_POSTAL_ADDR4 = trim($dataAry['M_POSTAL_ADDR4']);
			$M_POSTAL_ADDR5 = trim($dataAry['M_POSTAL_ADDR5']);
			$M_POSTAL_ADDR6 = trim($dataAry['M_POSTAL_ADDR6']);
			$M_POST_CODE	= trim($dataAry['M_POST_CODE']);
			$M_EMAIL 		= trim($dataAry['M_EMAIL']);
			
			$GUARD 			= trim($dataAry['GUARD']);
			$GUARDIAN_C 	= trim($dataAry['GUARDIAN_C']);
			$GUARDIAN_E 	= trim($dataAry['GUARDIAN_E']);
			$G_SEX 			= trim($dataAry['G_SEX']);
			$G_REL_OTHERS   = trim($dataAry['G_REL_OTHERS']);
			$LIVE_SAME 		= trim($dataAry['LIVE_SAME']);
			$G_PROF 		= trim($dataAry['G_PROF']);
			$G_JOB_NATURE 	= trim($dataAry['G_JOB_NATURE']);
			$G_JOB_TITLE 	= trim($dataAry['G_JOB_TITLE']);
			$G_MARITAL		= trim($dataAry['G_MARITAL']);
			$G_EDU_LEVEL	= trim($dataAry['G_EDU_LEVEL']);
			$G_TEL 			= trim($dataAry['G_TEL']);
			$G_MOBILE 		= trim($dataAry['G_MOBILE']);
			$G_OFFICE 		= trim($dataAry['G_OFFICE']);
			$G_POSTAL_ADDR1 = trim($dataAry['G_POSTAL_ADDR1']);
			$G_POSTAL_ADDR2 = trim($dataAry['G_POSTAL_ADDR2']);
			$G_POSTAL_ADDR3 = trim($dataAry['G_POSTAL_ADDR3']);
			$G_POSTAL_ADDR4 = trim($dataAry['G_POSTAL_ADDR4']);
			$G_POSTAL_ADDR5 = trim($dataAry['G_POSTAL_ADDR5']);
			$G_POSTAL_ADDR6 = trim($dataAry['G_POSTAL_ADDR6']);
			$G_POST_CODE	= trim($dataAry['G_POST_CODE']);
			$G_EMAIL 		= trim($dataAry['G_EMAIL']);

			### Data Checking [Start] ###
			
			$error_msg = array();
			
			if($flag!="edit") {
				# Check any existing student
				$studentID = $this->retrieveStudentIDByClassOrLogin($CLASS, $C_NO, $UserLogin);
			//	$sql = "SELECT COUNT(*) FROM STUDENT_REGISTRY_STUDENT WHERE UserID=$studentID";
			//	$userIDExist = $this->returnVector($sql);
				if(($CLASS=="" || $C_NO=="") && $UserLogin=="")
					$error_msg['UserInfo'] = $Lang['StudentRegistry']['StudentInfo'].$Lang['StudentRegistry']['IsMissing'];
				else if($studentID=="" || $studentID==0)
					$error_msg['UserInfo'] = $Lang['StudentRegistry']['StudentInfo'].$Lang['StudentRegistry']['IsInvalid'];
			//	else if($userIDExist[0]!=0) 
			//		$error_msg['UserInfo'] = $Lang['StudentRegistry']['StudentInfo2'].$Lang['StudentRegistry']['alreadyExist'];
			}
			
			###########################
			#  STUDENT INFO CHECKING  #
			###########################
			
			# Check Student ID
			if($STUD_ID=="")	
				$error_msg['STUD_ID'] = $Lang['StudentRegistry']['Import_Malaysia'][3].$Lang['StudentRegistry']['IsMissing'];
							
			# Check name
			if($NAME_C=="" || $NAME_E=="")
				$error_msg['NAME'] = $i_general_name.$Lang['StudentRegistry']['IsMissing'];

			# Check gender
			$genderAry = $this->GENDER_ARY();
			if($SEX=="")
				$error_msg['SEX'] = $Lang['StudentRegistry']['Import_Malaysia'][6].$Lang['StudentRegistry']['IsMissing'];
			else if(!array_key_exists($SEX, $genderAry))
				$error_msg['SEX'] = $Lang['StudentRegistry']['Import_Malaysia'][6].$Lang['StudentRegistry']['IsInvalid'];
			
			# check date format of DOB
			if($B_DATE=="" && $mode)	
				$error_msg['B_DATE'] = $Lang['StudentRegistry']['Import_Malaysia'][7].$Lang['StudentRegistry']['IsMissing'];
			else if(!$this->checkDateFormat($B_DATE)  && $mode)
				$error_msg['B_DATE'] = $Lang['StudentRegistry']['Import_Malaysia'][7].$Lang['StudentRegistry']['IsInvalid'];

			# check Place of Birth
			$B_PLACE_Ary = $this->MAL_B_PLACE_DATA_ARY();
			if($mode)
			{
				if($B_PLACE=="")
					$error_msg['B_PLACE'] = $Lang['StudentRegistry']['Import_Malaysia'][8].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($B_PLACE, $B_PLACE_Ary))
					$error_msg['B_PLACE'] = $Lang['StudentRegistry']['Import_Malaysia'][8].$Lang['StudentRegistry']['IsInvalid'];
			}

			# check NATION
			$NATION_Ary = $this->MAL_NATION_DATA_ARY();
			if($NATION=="")
				$error_msg['NATION'] = $Lang['StudentRegistry']['Import_Malaysia'][9].$Lang['StudentRegistry']['IsMissing'];
			else if(!array_key_exists($NATION, $NATION_Ary))
				$error_msg['NATION'] = $Lang['StudentRegistry']['Import_Malaysia'][9].$Lang['StudentRegistry']['IsInvalid'];
				
			# check ANCESTAL_HOME
			if($ANCESTRAL_HOME=="" && $mode)
				$error_msg['ANCESTRAL_HOME'] = $Lang['StudentRegistry']['Import_Malaysia'][10].$Lang['StudentRegistry']['IsMissing'];
			
			# check RACE
			$RACE_Ary = $this->MAL_RACE_DATA_ARY();
			if($mode)
			{
				if($RACE=="")
					$error_msg['RACE'] = $Lang['StudentRegistry']['Import_Malaysia'][11].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($RACE, $RACE_Ary))
					$error_msg['RACE'] = $Lang['StudentRegistry']['Import_Malaysia'][11].$Lang['StudentRegistry']['IsInvalid'];
			}	
					
			# Check ID no
			if($ID_NO=="" && $mode)
				$error_msg['ID_NO'] = $Lang['StudentRegistry']['Import_Malaysia'][12].$Lang['StudentRegistry']['IsMissing'];
			
			# check BIRTH_CERT_NO
			if($BIRTH_CERT_NO=="" && $mode)
				$error_msg['BIRTH_CERT_NO'] = $Lang['StudentRegistry']['Import_Malaysia'][13].$Lang['StudentRegistry']['IsMissing'];
			
			# check RELIGION
			$RELIGION_Ary = $this->MAL_RELIGION_DATA_ARY();
			if($mode)
			{
				if($RELIGION=="")
					$error_msg['RELIGION'] = $Lang['StudentRegistry']['Import_Malaysia'][14].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($RELIGION, $RELIGION_Ary))
					$error_msg['RELIGION'] = $Lang['StudentRegistry']['Import_Malaysia'][14].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check FAMILY_LANG
			$FAMILY_LANG_Ary = $this->MAL_FAMILY_LANG_ARY();
			if($mode)
			{
				if($FAMILY_LANG=="")
					$error_msg['FAMILY_LANG'] = $Lang['StudentRegistry']['Import_Malaysia'][15].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($FAMILY_LANG, $FAMILY_LANG_Ary))
					$error_msg['FAMILY_LANG'] = $Lang['StudentRegistry']['Import_Malaysia'][15].$Lang['StudentRegistry']['IsInvalid'];		
			}
			
			# check PAYMENT_TYPE
			$PAYMENT_TYPE_Ary = $this->MAL_PAYMENT_TYPE_ARY();
			if($mode)
			{
				if($PAYMENT_TYPE=="")
					$error_msg['PAYMENT_TYPE'] = $Lang['StudentRegistry']['Import_Malaysia'][16].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($PAYMENT_TYPE, $PAYMENT_TYPE_Ary))
					$error_msg['PAYMENT_TYPE'] = $Lang['StudentRegistry']['Import_Malaysia'][16].$Lang['StudentRegistry']['IsInvalid'];
			}
							
			# check EMAIL
			if($EMAIL == "" && $mode)
				$error_msg['EMAIL'] = $Lang['StudentRegistry']['Import_Malaysia'][18].$Lang['StudentRegistry']['IsMissing'];
			else if(!intranet_validateEmail($EMAIL) && $mode)
				$error_msg['EMAIL'] = $Lang['StudentRegistry']['Import_Malaysia'][18].$Lang['StudentRegistry']['IsInvalid'];
			
			# check LODGING
			$LODGING_Ary = $this->MAL_LODGING_ARY();
			if($mode)
			{
				if($LODGING=="")
					$error_msg['LODGING'] = $Lang['StudentRegistry']['Import_Malaysia'][19].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($LODGING, $LODGING_Ary))
					$error_msg['LODGING'] = $Lang['StudentRegistry']['Import_Malaysia'][19].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			/*
			# check ADDRESS1
			if($ADDRESS1=="" && $mode)
				$error_msg['ADDRESS1'] = $Lang['StudentRegistry']['Import_Malaysia'][20].$Lang['StudentRegistry']['IsMissing'];
				
			# check ADDRESS2
			if($ADDRESS2=="" && $mode)
				$error_msg['ADDRESS2'] = $Lang['StudentRegistry']['Import_Malaysia'][21].$Lang['StudentRegistry']['IsMissing'];
				
			# check ADDRESS3
			if($ADDRESS3=="" && $mode)
				$error_msg['ADDRESS3'] = $Lang['StudentRegistry']['Import_Malaysia'][22].$Lang['StudentRegistry']['IsMissing'];
			
			# check ADDRESS4
			if($ADDRESS4=="" && $mode)
				$error_msg['ADDRESS4'] = $Lang['StudentRegistry']['Import_Malaysia'][23].$Lang['StudentRegistry']['IsMissing'];
				
			# check ADDRESS5
			if($ADDRESS5=="" && $mode)
				$error_msg['ADDRESS5'] = $Lang['StudentRegistry']['Import_Malaysia'][24].$Lang['StudentRegistry']['IsMissing'];
			
			# check ADDRESS6
			if($ADDRESS6=="" && $mode)
				$error_msg['ADDRESS6'] = $Lang['StudentRegistry']['Import_Malaysia'][25].$Lang['StudentRegistry']['IsMissing'];
			*/
			
			# check entry date	
			if($mode)
			{
				if($ENTRY_DATE!="") {
					if(!$this->checkDateFormat($ENTRY_DATE)) {
						$error_msg['ENTRY_DATE'] = $Lang['StudentRegistry']['Import_Malaysia'][26].$Lang['StudentRegistry']['IsInvalid'];	
					}
				}
				else
					$error_msg['ENTRY_DATE'] = $Lang['StudentRegistry']['Import_Malaysia'][26].$Lang['StudentRegistry']['IsMissing'];
			}
			
			# check Siblings
			if($mode && $dataAry['SIBLINGS']!="")
			{
				$result = array();
				$sql = "select UserID from STUDENT_REGISTRY_STUDENT where UserID = ";
				for($i=0;$i<sizeof($SIBLINGS_ARY);$i++)
				{
					$result = $this->returnVector($sql.$SIBLINGS_ARY[$i]);
					if(!$result)
					{
						$error_msg['SIBLINGS'] = $Lang['StudentRegistry']['Import_Malaysia'][27].$Lang['StudentRegistry']['IsInvalid'];
						break;
					}
				}
			}
			
			# check PRI_SCHOOL
			if($PRI_SCHOOL=="" && $mode)
				$error_msg['PRI_SCHOOL'] = $Lang['StudentRegistry']['Import_Malaysia'][28].$Lang['StudentRegistry']['IsMissing'];								
			
			##########################
			#  FATHER INFO CHECKING  #
			##########################						
			
			# Check father name
			if($mode)
			{
				if($FATHER_C=="" || $FATHER_E=="")
					$error_msg['FATHER'] = $Lang['StudentRegistry']['FatherName'].$Lang['StudentRegistry']['IsMissing'];
			}
				
			# check Father Job nature
			$JobNature_ary = $this->MAL_JOB_NATURE_DATA_ARY();
			if($mode)
			{
				if($F_JOB_NATURE=="")
					$error_msg['F_JOB_NATURE'] = $Lang['StudentRegistry']['Import_Malaysia'][32].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($F_JOB_NATURE, $JobNature_ary))
					$error_msg['F_JOB_NATURE'] = $Lang['StudentRegistry']['Import_Malaysia'][32].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check Father Job title
			$JobTitle_ary = $this->MAL_JOB_TITLE_DATA_ARY();
			if($mode)
			{
				if($F_JOB_TITLE=="")
					$error_msg['F_JOB_TITLE'] = $Lang['StudentRegistry']['Import_Malaysia'][33].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($F_JOB_TITLE, $JobTitle_ary))
					$error_msg['F_JOB_TITLE'] = $Lang['StudentRegistry']['Import_Malaysia'][33].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check father marital status
			$marital_ary = $this->MAL_MARITAL_STATUS_DATA_ARY();
			if($mode)
			{
				if($F_MARITAL=="")
					$error_msg['F_MARITAL'] = $Lang['StudentRegistry']['Import_Malaysia'][34].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($F_MARITAL, $marital_ary))
					$error_msg['F_MARITAL'] = $Lang['StudentRegistry']['Import_Malaysia'][34].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check father education level
			$EduLevel_ary = $this->MAL_EDU_LEVEL_DATA_ARY();
			if($mode)
			{
				if($F_EDU_LEVEL=="")
					$error_msg['F_EDU_LEVEL'] = $Lang['StudentRegistry']['Import_Malaysia'][35].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($F_EDU_LEVEL, $EduLevel_ary))
					$error_msg['F_EDU_LEVEL'] = $Lang['StudentRegistry']['Import_Malaysia'][35].$Lang['StudentRegistry']['IsInvalid'];					
			}
			
			# check father telephone
			if($F_TEL=="" && $mode)	
				$error_msg['F_TEL'] = $Lang['StudentRegistry']['Import_Malaysia'][36].$Lang['StudentRegistry']['IsMissing'];
				
			# check father mobile
			if($F_MOBILE=="" && $mode)	
				$error_msg['F_MOBILE'] = $Lang['StudentRegistry']['Import_Malaysia'][37].$Lang['StudentRegistry']['IsMissing'];
				
			# check father office phone
			if($F_OFFICE=="" && $mode)
				$error_msg['F_OFFICE'] = $Lang['StudentRegistry']['Import_Malaysia'][38].$Lang['StudentRegistry']['IsMissing'];	
					
			# check father address1
			if($F_POSTAL_ADDR1=="" && $mode)
				$error_msg['F_POSTAL_ADDR1'] = $Lang['StudentRegistry']['Import_Malaysia'][39].$Lang['StudentRegistry']['IsMissing'];

			# check father address2
			if($F_POSTAL_ADDR2=="" && $mode)
				$error_msg['F_POSTAL_ADDR2'] = $Lang['StudentRegistry']['Import_Malaysia'][40].$Lang['StudentRegistry']['IsMissing'];			
			
			# check father address3
			if($F_POSTAL_ADDR3=="" && $mode)
				$error_msg['F_POSTAL_ADDR3'] = $Lang['StudentRegistry']['Import_Malaysia'][41].$Lang['StudentRegistry']['IsMissing'];			
			
			# check father address4
			if($F_POSTAL_ADDR4=="" && $mode)
				$error_msg['F_POSTAL_ADDR4'] = $Lang['StudentRegistry']['Import_Malaysia'][42].$Lang['StudentRegistry']['IsMissing'];			
			
			# check father address5
			if($F_POSTAL_ADDR5=="" && $mode)
				$error_msg['F_POSTAL_ADDR5'] = $Lang['StudentRegistry']['Import_Malaysia'][43].$Lang['StudentRegistry']['IsMissing'];			
			
			# check father address6
			if($F_POSTAL_ADDR6=="" && $mode)
				$error_msg['F_POSTAL_ADDR6'] = $Lang['StudentRegistry']['Import_Malaysia'][44].$Lang['StudentRegistry']['IsMissing'];			

			# check father postal code
			if($F_POST_CODE=="" && $mode)
				$error_msg['F_POST_CODE'] = $Lang['StudentRegistry']['Import_Malaysia'][45].$Lang['StudentRegistry']['IsMissing'];			
				
			# check father EMAIL
			if($mode)
			{
				/*
				if($F_EMAIL=="")
					$error_msg['F_EMAIL'] = $Lang['StudentRegistry']['Import_Malaysia'][45].$Lang['StudentRegistry']['IsMissing'];
				else if(!intranet_validateEmail($F_EMAIL))
					$error_msg['F_EMAIL'] = $Lang['StudentRegistry']['Import_Malaysia'][45].$Lang['StudentRegistry']['IsInvalid'];
				*/
				if($F_EMAIL!="" && !intranet_validateEmail($F_EMAIL))
					$error_msg['F_EMAIL'] = $Lang['StudentRegistry']['Import_Malaysia'][46].$Lang['StudentRegistry']['IsInvalid'];
			}

			##########################
			#  MOTHER INFO CHECKING  #
			##########################				
			
			# Check mother name
			if($mode)
			{
				if($MOTHER_C=="" || $MOTHER_E=="")
					$error_msg['MOTHER'] = $Lang['StudentRegistry']['MotherName'].$Lang['StudentRegistry']['IsMissing'];
			}
			
			# check Mother Job nature
			if($mode)
			{
				if($M_JOB_NATURE=="")
					$error_msg['M_JOB_NATURE'] = $Lang['StudentRegistry']['Import_Malaysia'][50].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($M_JOB_NATURE, $JobNature_ary))
					$error_msg['M_JOB_NATURE'] = $Lang['StudentRegistry']['Import_Malaysia'][50].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check Mother Job title
			if($mode)
			{
				if($M_JOB_TITLE=="")
					$error_msg['M_JOB_TITLE'] = $Lang['StudentRegistry']['Import_Malaysia'][51].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($M_JOB_TITLE, $JobTitle_ary))
					$error_msg['M_JOB_TITLE'] = $Lang['StudentRegistry']['Import_Malaysia'][51].$Lang['StudentRegistry']['IsInvalid'];
			}
								
			# check mother marital status
			if($mode)
			{
				if($M_MARITAL=="")
					$error_msg['M_MARITAL'] = $Lang['StudentRegistry']['Import_Malaysia'][52].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($M_MARITAL, $marital_ary))
					$error_msg['M_MARITAL'] = $Lang['StudentRegistry']['Import_Malaysia'][52].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check mother education level
			if($mode)
			{
				if($M_EDU_LEVEL=="")
					$error_msg['M_EDU_LEVEL'] = $Lang['StudentRegistry']['Import_Malaysia'][53].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($M_EDU_LEVEL, $EduLevel_ary))
					$error_msg['M_EDU_LEVEL'] = $Lang['StudentRegistry']['Import_Malaysia'][53].$Lang['StudentRegistry']['IsInvalid'];					
			}
			
			# check mother telephone
			if($M_TEL=="" && $mode)	
				$error_msg['M_TEL'] = $Lang['StudentRegistry']['Import_Malaysia'][54].$Lang['StudentRegistry']['IsMissing'];

			# check mother mobile
			if($M_MOBILE=="" && $mode)	
				$error_msg['M_MOBILE'] = $Lang['StudentRegistry']['Import_Malaysia'][55].$Lang['StudentRegistry']['IsMissing'];
			
			# check mother office phone
			if($M_OFFICE=="" && $mode)
				$error_msg['M_OFFICE'] = $Lang['StudentRegistry']['Import_Malaysia'][56].$Lang['StudentRegistry']['IsMissing']; 

			# check mother address1
			if($M_POSTAL_ADDR1=="" && $mode)
				$error_msg['M_POSTAL_ADDR1'] = $Lang['StudentRegistry']['Import_Malaysia'][57].$Lang['StudentRegistry']['IsMissing'];

			# check mother address2
			if($M_POSTAL_ADDR2=="" && $mode)
				$error_msg['M_POSTAL_ADDR2'] = $Lang['StudentRegistry']['Import_Malaysia'][58].$Lang['StudentRegistry']['IsMissing'];

			# check mother address3
			if($M_POSTAL_ADDR3=="" && $mode)
				$error_msg['M_POSTAL_ADDR3'] = $Lang['StudentRegistry']['Import_Malaysia'][59].$Lang['StudentRegistry']['IsMissing'];

			# check mother address4
			if($M_POSTAL_ADDR4=="" && $mode)
				$error_msg['M_POSTAL_ADDR4'] = $Lang['StudentRegistry']['Import_Malaysia'][60].$Lang['StudentRegistry']['IsMissing'];
				
			# check mother address5
			if($M_POSTAL_ADDR5=="" && $mode)
				$error_msg['M_POSTAL_ADDR5'] = $Lang['StudentRegistry']['Import_Malaysia'][61].$Lang['StudentRegistry']['IsMissing'];

			# check mother address6
			if($M_POSTAL_ADDR6=="" && $mode)
				$error_msg['M_POSTAL_ADDR6'] = $Lang['StudentRegistry']['Import_Malaysia'][62].$Lang['StudentRegistry']['IsMissing'];

			# check mother postal code
			if($M_POST_CODE=="" && $mode)
				$error_msg['M_POST_CODE'] = $Lang['StudentRegistry']['Import_Malaysia'][63].$Lang['StudentRegistry']['IsMissing'];			
				
			# check mother EMAIL
			if($mode){
				/*
				if($M_EMAIL=="")
					$error_msg['M_EMAIL'] = $Lang['StudentRegistry']['Import_Malaysia'][63].$Lang['StudentRegistry']['IsMissing'];
				else if(!intranet_validateEmail($M_EMAIL))
					$error_msg['M_EMAIL'] = $Lang['StudentRegistry']['Import_Malaysia'][63].$Lang['StudentRegistry']['IsInvalid'];
				*/
				if($M_EMAIL!="" && !intranet_validateEmail($M_EMAIL))
					$error_msg['M_EMAIL'] = $Lang['StudentRegistry']['Import_Malaysia'][64].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			############################
			#  GUARDIAN INFO CHECKING  #
			############################

			# Check guardian name
			//if($mode)
			{
				if($GUARDIAN_C=="" || $GUARDIAN_E=="")
					$error_msg['GUARDIAN'] = $Lang['StudentRegistry']['GuardianName'].$Lang['StudentRegistry']['IsMissing'];
			}
			
			# Check gender
			$genderAry = $this->GENDER_ARY();
			if($mode)
			{
				if($G_SEX=="")
					$error_msg['G_SEX'] = $Lang['StudentRegistry']['Import_Malaysia'][67].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($G_SEX, $genderAry))
					$error_msg['G_SEX'] = $Lang['StudentRegistry']['Import_Malaysia'][67].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check relationship with student //Need to change
			$guard_ary = $this->MAL_GUARD_DATA_ARY();
			if($mode)
			{
				if($GUARD=="")
					$error_msg['GUARD'] = $Lang['StudentRegistry']['Import_Malaysia'][68].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($GUARD, $guard_ary))
					$error_msg['GUARD'] = $Lang['StudentRegistry']['Import_Malaysia'][68].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			
			# check relationship (others) //Need to change
			if($GUARD=="O" && $G_REL_OTHERS=="" && $mode)
				$error_msg['G_REL_OTHERS'] = $Lang['StudentRegistry']['Import_Malaysia'][69].$Lang['StudentRegistry']['IsMissing'];

			# check live together // Need to change
			$live_same_ary = $this->MAL_LIVE_SAME_ARY();
			if($mode)
			{
				if($LIVE_SAME=="")				
					$error_msg['LIVE_SAME'] = $Lang['StudentRegistry']['Import_Malaysia'][70].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($LIVE_SAME, $live_same_ary))
					$error_msg['LIVE_SAME'] = $Lang['StudentRegistry']['Import_Malaysia'][70].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check guardian Job nature
			if($mode)
			{
				if($G_JOB_NATURE=="")
					$error_msg['G_JOB_NATURE'] = $Lang['StudentRegistry']['Import_Malaysia'][72].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($G_JOB_NATURE, $JobNature_ary))
					$error_msg['G_JOB_NATURE'] = $Lang['StudentRegistry']['Import_Malaysia'][72].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check guardian Job title
			if($mode)
			{
				if($G_JOB_TITLE=="")
					$error_msg['G_JOB_TITLE'] = $Lang['StudentRegistry']['Import_Malaysia'][73].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($G_JOB_TITLE, $JobTitle_ary))
					$error_msg['G_JOB_TITLE'] = $Lang['StudentRegistry']['Import_Malaysia'][73].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			# check guardian education level
			if($mode)
			{
				if($G_EDU_LEVEL=="")
					$error_msg['G_EDU_LEVEL'] = $Lang['StudentRegistry']['Import_Malaysia'][74].$Lang['StudentRegistry']['IsMissing'];
				else if(!array_key_exists($G_EDU_LEVEL, $EduLevel_ary))
					$error_msg['G_EDU_LEVEL'] = $Lang['StudentRegistry']['Import_Malaysia'][74].$Lang['StudentRegistry']['IsInvalid'];					
			}
							
			# check guardian home telephone
			//if($G_TEL=="" && $mode)
			if($G_TEL=="")
				$error_msg['G_TEL'] = $Lang['StudentRegistry']['Import_Malaysia'][75].$Lang['StudentRegistry']['IsMissing'];
			
			# check guardian mobile	
			//if($G_MOBILE=="" && $mode)
			if($G_MOBILE=="")
				$error_msg['G_MOBILE'] = $Lang['StudentRegistry']['Import_Malaysia'][76].$Lang['StudentRegistry']['IsMissing'];
			
			# check guardian office phone
			if($G_OFFICE=="" && $mode)
				$error_msg['G_OFFICE'] = $Lang['StudentRegistry']['Import_Malaysia'][77].$Lang['StudentRegistry']['IsMissing'];
			
			# check guardian address1
			if($G_POSTAL_ADDR1=="" && $mode)
				$error_msg['G_POSTAL_ADDR1'] = $Lang['StudentRegistry']['Import_Malaysia'][78].$Lang['StudentRegistry']['IsMissing'];
			
			# check guardian address2
			if($G_POSTAL_ADDR2=="" && $mode)
				$error_msg['G_POSTAL_ADDR2'] = $Lang['StudentRegistry']['Import_Malaysia'][79].$Lang['StudentRegistry']['IsMissing'];
				
			# check guardian address3
			if($G_POSTAL_ADDR3=="" && $mode)
				$error_msg['G_POSTAL_ADDR3'] = $Lang['StudentRegistry']['Import_Malaysia'][80].$Lang['StudentRegistry']['IsMissing'];
				
			# check guardian address4
			if($G_POSTAL_ADDR4=="" && $mode)
				$error_msg['G_POSTAL_ADDR4'] = $Lang['StudentRegistry']['Import_Malaysia'][81].$Lang['StudentRegistry']['IsMissing'];
				
			# check guardian address5
			if($G_POSTAL_ADDR5=="" && $mode)
				$error_msg['G_POSTAL_ADDR5'] = $Lang['StudentRegistry']['Import_Malaysia'][82].$Lang['StudentRegistry']['IsMissing'];
				
			# check guardian address6
			if($G_POSTAL_ADDR6=="" && $mode)
				$error_msg['G_POSTAL_ADDR6'] = $Lang['StudentRegistry']['Import_Malaysia'][83].$Lang['StudentRegistry']['IsMissing'];

			# check guardian postal code
			if($G_POST_CODE=="" && $mode)
				$error_msg['G_POST_CODE'] = $Lang['StudentRegistry']['Import_Malaysia'][84].$Lang['StudentRegistry']['IsMissing'];			
											
			# check guardian EMAIL
			if($mode)
			{
				/*
				if($G_EMAIL=="")
					$error_msg['G_EMAIL'] = $Lang['StudentRegistry']['Import_Malaysia'][84].$Lang['StudentRegistry']['IsMissing'];
				else if(!intranet_validateEmail($G_EMAIL))
					$error_msg['G_EMAIL'] = $Lang['StudentRegistry']['Import_Malaysia'][84].$Lang['StudentRegistry']['IsInvalid'];
				*/
				if($G_EMAIL!="" && !intranet_validateEmail($G_EMAIL))
					$error_msg['G_EMAIL'] = $Lang['StudentRegistry']['Import_Malaysia'][85].$Lang['StudentRegistry']['IsInvalid'];
			}
			
			### Data Checking [Start] ###
			
			return $error_msg;
		}
		
		function retrieveStudentIDByClassOrLogin($className="", $classNo="", $loginName="")
		{
			IF($loginName=="" && ($className=="" || $classNo==""))	return false;
			if($className!="" && $classNo!="") {	# use class name + class number to retrieve student id
				$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE (ClassTitleB5='$className' OR ClassTitleEN='$className') AND AcademicYearID='".Get_Current_Academic_Year_ID()."'";
				$yearClassID = $this->returnVector($sql);
				
				$sql = "SELECT 
							USR.UserID 
						FROM
							INTRANET_USER USR LEFT OUTER JOIN
							YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
							YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) 
						WHERE 
							USR.RecordType=2 AND
							USR.RecordStatus IN (0,1,2) AND 
							yc.YearClassID='".$yearClassID[0]."' AND
							ycu.ClassNumber='$classNo' AND 
							AcademicYearID=".Get_Current_Academic_Year_ID();
				$result = $this->returnVector($sql);
				
			} else if($loginName!="") {
				$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND UserLogin='$loginName'";
				$result = $this->returnVector($sql);
			}
			return $result[0];
		}
		
		function checkDateFormat($RecordDate)
		{
			if(substr_count($RecordDate, "-") != 0)
				list($year, $month, $day) = split('-',$RecordDate);
			else 
				list($year, $month, $day) = split('/',$RecordDate);
						
			if((strlen($year)==4 && strlen($month)==2 && strlen($day)==2))
			{
				if(checkdate($month,$day,$year))
				{
					return true;
				}
			}
			return false;
		}	
		
		function getStudentInfo_by_UserID($userid='', $AcademicYearID='')
		{
			$result = array();
			
			if($AcademicYearID=='')
				$academicYearID = Get_Current_Academic_Year_ID();
				
			if($userid)
			{
				$sql = "SELECT
							USR.UserID AS UserID,
							".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." AS ClassName,
							ycu.ClassNumber AS ClassNumber,
							".Get_Lang_Selection('USR.ChineseName','USR.EnglishName')." AS StudentName,
							USR.YearOfLeft
						FROM
							INTRANET_USER AS USR LEFT OUTER JOIN
							YEAR_CLASS_USER AS ycu ON (USR.UserID = ycu.UserID) LEFT OUTER JOIN
							YEAR_CLASS AS yc ON (ycu.YearClassID = yc.YearClassID) LEFT OUTER JOIN
							ACADEMIC_YEAR_TERM yt ON (yt.AcademicYearID=yc.AcademicYearID)
						WHERE
							(yc.AcademicYearID = '$AcademicYearID' OR USR.YearOfLeft IS NOT NULL) AND USR.UserID = '$userid'
						HAVING 
							ClassName IS NOT NULL
						ORDER BY yt.TermStart DESC LIMIT 1
						";
				$result = $this->returnArray($sql,4);
				//echo $sql;
			}
			
			return $result[0];
		}
		
		function getStudentClassInfo($userid) 
		{
			$sql = "SELECT y.YearName, yc.YearClassID, ".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as className, ycu.ClassNumber FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID) WHERE ycu.UserID='$userid' AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."'";
			$result = $this->returnArray($sql,3);
			return $result[0];
		}		
		
		function updateStudentRegistry_Simple_Macau($dataAry=array(), $userid, $pgAry=array(), $isSync = false)
		{			
			global $UserID, $sys_custom;
			
			$sql = "SELECT COUNT(*) FROM STUDENT_REGISTRY_STUDENT WHERE UserID='$userid'";
			$count = $this->returnVector($sql);
			
			if($count[0]==0) {
				# insert record	
				foreach($dataAry as $fields[]=>$values[]);
				
				$sql = "INSERT INTO STUDENT_REGISTRY_STUDENT (UserID, ";
				foreach($fields as $field) $sql .= "$field, ";
				$sql .= "DateInput, InputBy, DateModified, ModifyBy) VALUES ('$userid', ";
				foreach($values as $value) $sql .= "'$value', ";
				$sql .= "NOW(), '$UserID', NOW(), '$UserID')";
			} else {
				# update record
				$sql = "UPDATE STUDENT_REGISTRY_STUDENT SET ";
				foreach($dataAry as $field=>$value)
					$sql .= $field."='".$value."', ";
				$sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE UserID='$userid'";
				
			}
			
			$result = $this->db_db_query($sql);

			# update STUDENT_REGISTRY_PG data
			$sql = "SELECT GUARD FROM STUDENT_REGISTRY_PG WHERE StudentID='$userid' AND PG_TYPE='E'";
			$temp = $this->returnVector($sql);

			if(sizeof($pgAry)>0) {
				foreach($pgAry as $pgfields[]=>$pgvalues[]);
			
				if($temp[0]!="") {	# existing guardian
					$sql = "UPDATE STUDENT_REGISTRY_PG SET ";
					foreach($pgAry as $pgfield=>$pgvalue)
						$sql .= $pgfield."='".$pgvalue."', ";
					$sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE StudentID='$userid' AND PG_TYPE='E'";
					$this->db_db_query($sql);
					//echo $sql.'<br>';
					if($temp[0]=="F" || $temp[0]=="M") {	# update Father / Mother
						$sql = "UPDATE STUDENT_REGISTRY_PG SET ";
						foreach($pgAry as $pgfield=>$pgvalue)
							$sql .= $pgfield."='".$pgvalue."', ";
						$sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE StudentID='$userid' AND PG_TYPE='".$temp[0]."'";
						$this->db_db_query($sql);
						//echo $sql.'<br>';
					}
				} else {			# no existing guardian
				
					$sql = "INSERT INTO STUDENT_REGISTRY_PG (StudentID, PG_TYPE, ";
					foreach($pgfields as $field) $sql .= "$field, ";
					$sql .= "GUARD, DateInput, InputBy, DateModified, ModifyBy) VALUES ('$userid','E',";
					foreach($pgvalues as $value) $sql .= "'$value', ";
					$sql .= "'O', NOW(), '$UserID', NOW(), '$UserID')";
					$this->db_db_query($sql);
					//echo $sql.'<br>';
				}
			}
			
			if($sys_custom['AccountMgmt']['cdsj_5_macau']['syncAccountAndRegistry'] && $result && !$isSync){
				$syncResult = array();
				$dataAry_UserInfo = array();
				if($dataAry['CODE']) $dataAry_UserInfo['STRN'] = $dataAry['CODE'];
				if(!empty($dataAry_UserInfo)) $syncResult['UserInfo'] = $this->syncToAccountUserInfo($userid, $dataAry_UserInfo);
				
				$dataAry_PersonalSetting = array();
				$nation_ary = $this->MACAU_NATION_DATA_ARY();
				$BPlace_ary = $this->MACAU_B_PLACE_DATA_ARY();
				$displayLang = Get_Lang_Selection(1,0);
				if($dataAry['B_PLACE']) $dataAry_PersonalSetting['PlaceOfBirth'] = $BPlace_ary[$dataAry['B_PLACE']][$displayLang];
				if($dataAry['NATION']) $dataAry_PersonalSetting['Nationality'] = $nation_ary[$dataAry['NATION']][$displayLang];
				if($dataAry['ADMISSION_DATE']) $dataAry_PersonalSetting['AdmissionDate'] = $dataAry['ADMISSION_DATE'];
				if(!empty($dataAry_PersonalSetting)) $syncResult['PersonalSetting'] = $this->syncToAccountPersonalSetting($userid, $dataAry_PersonalSetting);
				
				if(in_array(false, $syncResult)) return false;
			}
			
			return $result;
		}
				
		function updateStudentInfo($dataAry=array(), $userid)
		{
			global $UserID;
			$sql = "UPDATE INTRANET_USER SET ";	
			foreach($dataAry as $field=>$value)
				$sql .= $field."='".$value."', ";
			$sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE UserID='$userid'";
			
			//$result = $this->db_db_query($sql) or die(mysql_error());
			$result = $this->db_db_query($sql);
			if (mysql_error())
			{
				$sql = "SELECT * FROM INTRANET_USER where UserID<>'$userid' and UserEmail<>'".$dataAry["UserEmail"]."'";
				if (sizeof($this->returnVector($sql))>0)
				{
					echo "<p>Email '".$dataAry["UserEmail"]."' is already used by another user! Thus this information update failed!!</p>";
				}
			}
			
			return $result;
		}
		
		function updateStudentRegistry_PG($dataAry=array(), $PG_Type="", $userid)
		{
			global $UserID;
			
			$sql = "SELECT COUNT(*) FROM STUDENT_REGISTRY_PG WHERE StudentID='$userid' AND PG_TYPE='$PG_Type'";
			$count = $this->returnVector($sql);
			
			if($count[0]==0) {
				# insert record
				foreach($dataAry as $fields[]=>$values[]);
				
				$sql = "INSERT INTO STUDENT_REGISTRY_PG (StudentID, PG_TYPE, ";	
				foreach($fields as $field) $sql .= "$field, ";
				$sql .= "DateInput, InputBy, DateModified, ModifyBy) VALUES ('$userid', '$PG_Type', ";
				foreach($values as $value) $sql .= "'$value', ";
				$sql .= "NOW(), '$UserID', NOW(), '$UserID')";
				
			} else {
				# update record
				$sql = "UPDATE STUDENT_REGISTRY_PG SET ";	
				foreach($dataAry as $field=>$value)
					$sql .= $field."='".$value."', ";
				$sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE StudentID='$userid' AND PG_TYPE='$PG_Type'";
			}
			$result = $this->db_db_query($sql);
			
			return $result;
		}
		
		function removeStudentRegistry_PG($userid="", $PG_Type="", $PG_ID="")
		{
			if($PG_ID!="" || ($userid!="" && $PG_Type!=""))	 {
				if($PG_ID!="") $conds = " SRPG_ID='$PG_ID'";
				else $conds = " StudentID='$userid' AND PG_TYPE='$PG_Type'";
				
				$sql = "DELETE FROM STUDENT_REGISTRY_PG WHERE $conds";
				$this->db_db_query($sql);
			}
		}
		
		function insertStudentRegistry_PG($dataAry=array(), $PG_Type="")
		{
			global $UserID;
			foreach($dataAry as $fields[]=>$values[])
			$sql = "INSERT INTO STUDENT_REGISTRY_PG (";	
			foreach($fields as $field)	$sql .= $field .", ";
			$sql .= "DateInput, InputBy, DateModified, ModifyBy) VALUES (";
			foreach($values as $value)	$sql .= "'".$value ."', ";
			$sql .= "NOW(), '$UserID', NOW(), '$UserID')";
			
			$this->db_db_query($sql);
		}
	
		function memberInMgmtGroup($GroupID) 
		{
			$sql = "SELECT UserID FROM ACCESS_RIGHT_GROUP_MEMBER WHERE GroupID='$GroupID'";	
			return $this->returnVector($sql);
		}
		
		function Check_Group_Title($GroupTitle, $GroupID)
		{
			$sql = "SELECT
						GroupID 
					FROM 
						ACCESS_RIGHT_GROUP
					WHERE
						GroupID != '$GroupID' 
						AND GroupTitle = '".$this->Get_Safe_Sql_Query($GroupTitle)."' 
						AND Module = '".$this->Module."' ";
			return (sizeof($this->returnVector($sql))>0 ?false:true);
		}
		
		function getAcademicYearIDByYearName($yearName)
		{
			$sql = "SELECT AcademicYearID FROM ACADEMIC_YEAR WHERE YearNameEN='$yearName' OR YearNameB5='$yearName'";
			$result = $this->returnVector($sql);
			return $result[0];
		}

		function getAcademicYearNameByYearID($yearID)
		{
			$sql = "SELECT YearNameB5, YearNameEN FROM ACADEMIC_YEAR WHERE AcademicYearID='$yearID'";
			$result = $this->returnArray($sql, 2);
			$clsName = Get_Lang_Selection($result[0][0], $result[0][1]);
			return $clsName;
		}
		
		function getYearNameByYearClassID($AcademicYearID, $StudentID)
		{
			$sql = "SELECT
					y.YearName
					FROM
					YEAR y LEFT OUTER JOIN
					YEAR_CLASS yc ON (y.YearID=yc.YearID) LEFT OUTER JOIN
					YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID)
					WHERE
					yc.AcademicYearID='$AcademicYearID' AND
					ycu.UserID='$StudentID'";
			
			$result = $this->returnVector($sql);
			
			return $result[0];
		}
			
		##################################################################################
		# Common Functions
		##################################################################################
		function RETRIEVE_DATA_ARY($data_ary=array(), $data_id='')
		{
			global $intranet_session_language;
			
			//if($intranet_session_language == "en"){
				$lang_ary_id = $intranet_session_language == "en" ? 0 : 1;
			//}

			if($data_id!=='')			# return data value if $data_id data is passed into function
			{
				return $data_ary[$data_id][$lang_ary_id];
			}
			else				# return data array (use for selection)
			{
				$result = array();
				foreach($data_ary as $k=>$d)
				{
					$result[$k] = $d[$lang_ary_id];
				}
				
				return $result;
			}
		}
		
		function GENERATE_RADIO_BTN_LIST($obj_array, $array_name, $Selected_Option='', $Vertical=false)
		{
			$keys = array_keys($obj_array);

			$RadioBtnList = "<div class=\"Edit_Show\" style=\"display:none\">";
			for($i=0;$i<sizeof($obj_array);$i++)
			{
				if($Selected_Option == '--' && $i==0) {
                    if ($array_name == 'GuardStay') {
                        // do nothing
                    }
                    else {
                        $IsChecked = "checked=\"checked\"";
                    }
                }
				else if ($Selected_Option == $obj_array[$keys[$i]]) {
                    $IsChecked = "checked=\"checked\"";
                }
				else {
                    if ($array_name == 'GuardStay' && $Selected_Option == '--' && $i==1) {
                        $IsChecked = "checked=\"checked\"";
                    }
                    else {
                        $IsChecked = "";
                    }
                }

				$RadioBtnList .= "<input id=\"".$array_name."_".$keys[$i]."\" type=\"radio\" name=\"$array_name\" value=\"".$keys[$i]."\" ".$IsChecked."><label for=\"".$array_name."_".$keys[$i]."\">".$obj_array[$keys[$i]]."</label>";
				$RadioBtnList .= $Vertical? ($i == sizeof($obj_array)-1 ? "<br>" : "") : "";
			}
			$RadioBtnList .= "</div>";
			
			return $RadioBtnList;
		}
		
		function GENERATE_DROP_DWON_LIST($obj_array, $array_name, $Selected_Option='', $other_attr='')
		{
			$keys = array_keys($obj_array);
			$DropDownList = "<select class=\"Edit_Show\" name=\"$array_name\" style=\"display: none\" $other_attr>";
			for($i=0;$i<sizeof($obj_array);$i++)
			{
				$DropDownList .= "<option value=".$keys[$i].($Selected_Option == $obj_array[$keys[$i]]? " selected=\"selected\"" : "").">".$obj_array[$keys[$i]]."</option>";
			}
			$DropDownList .= "</select>";
			
			return $DropDownList;
		}
		/*
		function RETRIEVE_STUDENT_CLASS($StudentID='')
		{
			$result = array();
			
			if($StudentID)
			{
				$sql = "SELECT YearClassID FROM YEAR_CLASS_USER WHERE UserID = $StudentID";
				$result = $this->returnArray($sql);
			}
			return $result;
		}
		*/
		##################################################################################
		# Macau Functions
		##################################################################################
		function RETRIEVE_STUDENT_INFO_BASIC_MACAU($StudentID='')
		{
			$result = array();
			
			if($StudentID)
			{
				$sql = "SELECT
							d.SRS_ID,
							d.STUD_ID,
							d.CODE,
							a.EnglishName,
							a.ChineseName,
							a.Gender,
							d.NATION,
							a.HomeTelNo,
							d.S_CODE,
							IF(a.ClassName IS NULL OR a.ClassName = '', '--', a.ClassName) AS ClassName,
							IF(a.ClassNumber IS NULL OR a.ClassNumber = '', '--', a.ClassNumber) AS ClassNumber,
							d.DateModified,
							d.ModifyBy,
							IF(c.YearName IS NULL OR c.YearName = '', '--', c.YearName) AS YearName,
							b.YearClassID,
							a.RecordStatus
						FROM 
							INTRANET_USER as a
							left join YEAR_CLASS as b on (b.ClassTitleEN = a.ClassName and b.AcademicYearID='". Get_Current_Academic_Year_ID() ."')
							left join YEAR as c on (c.YearID = b.YearID)
							left join STUDENT_REGISTRY_STUDENT as d on (a.UserID = d.UserID)
						WHERE 
							a.UserID = '$StudentID'
						";
				$result = $this->returnArray($sql);
			}
			
			return $result;
		}
		
		function RETRIEVE_STUDENT_INFO_ADV_MACAU($StudentID='')
		{
			$result = array();
			
			if($StudentID)
			{
				$sql = "select 
							d.SRS_ID,
							d.STUD_ID, d.CODE,
							a.ChineseName, a.EnglishName,
							a.Gender, DATE_FORMAT(a.DateOfBirth, '%Y-%c-%e') AS DateOfBirth, d.B_PLACE,
							d.ORIGIN, d.NATION, d.RELIGION,
							d.S_CODE,
							IF(c.YearName IS NULL OR c.YearName = '', '--', c.YearName) AS YearName,
							IF(a.ClassName IS NULL OR a.ClassName = '', '--', a.ClassName) AS ClassName,
							IF(a.ClassNumber IS NULL OR a.ClassNumber = '', '--', a.ClassNumber) AS ClassNumber,
							d.ADMISSION_DATE,
							d.ID_TYPE, d.ID_NO, d.I_PLACE,
							d.I_DATE, d.V_DATE,
							d.S6_TYPE, d.S6_IDATE, d.S6_VDATE,
							a.HomeTelNo, d.EMAIL,
							d.R_AREA, d.RA_DESC,
							d.AREA,
							d.ROAD,
							d.ADDRESS,
							d.DateModified,
							d.ModifyBy,
							b.YearClassID,
							a.RecordStatus,
							d.TAGID
						from 
							INTRANET_USER as a
							left join YEAR_CLASS as b on (b.ClassTitleEN = a.ClassName and b.AcademicYearID='". Get_Current_Academic_Year_ID() ."')
							left join YEAR as c on (c.YearID = b.YearID)
							left join STUDENT_REGISTRY_STUDENT as d on (a.UserID = d.UserID)
						where 
							a.UserID = '$StudentID'
						";
				$result = $this->returnArray($sql);
			}

			return $result;
		}
		
		function RETRIEVE_STUDENT_PAST_ENROLLMENT_RECORD($StudentID='')
		{
			global $intranet_session_language;
			$lang_id = $intranet_session_language == "en" ? "EN" : "B5";
			$result = array();
			
			if($StudentID)
			{
				$sql = "SELECT
							c.YearName$lang_id AS YearName,
							d.YearName AS Grade,
							b.ClassTitle$lang_id AS Class,
							a.ClassNumber AS ClassNumber
						FROM
							YEAR_CLASS_USER AS a
							JOIN YEAR_CLASS AS b ON a.YearClassID = b.YearClassID
							JOIN ACADEMIC_YEAR AS c ON (b.AcademicYearID = c.AcademicYearID AND c.AcademicYearID <".Get_Current_Academic_Year_ID().")
							JOIN YEAR AS d ON b.YearID = d.YearID
						WHERE
							a.UserID = '$StudentID'
						";
				$result = $this->returnArray($sql);
			}
			
			return $result;
		}
		
		function RETRIEVE_GUARDIAN_INFO_ADV_MACAU($StudentID='', $Type='')
		{
			$result = array();
			
			if($StudentID)
			{
				$sql = "select 
							SRPG_ID,
							NAME_C, NAME_E,
							G_GENDER,
							GUARD, G_RELATION, LIVE_SAME,
							PROF, MARITAL_STATUS,
							TEL, MOBILE, OFFICE_TEL,
							G_AREA, G_ROAD, G_ADDRESS,
							EMAIL
						from 
							STUDENT_REGISTRY_PG
						where 
							StudentID = '$StudentID' AND PG_TYPE = '$Type'
						";
				$result = $this->returnArray($sql);
			}
			
			return $result;
		}
		
		function RETRIEVE_MODIFIED_BY_INFO($StudentID='')
		{
			$result = array();
			
			if($StudentID)
			{
				$name_field = getNameFieldByLang("");
				$sql = "select
							$name_field
						from
							INTRANET_USER
						where
							UserID = '$StudentID'
						";
				$result = $this->returnArray($sql);
			}
			
			return $result[0][0];
		}
		
		function GENDER_ARY()
		{
			$ary = array();
			$ary['M'] = array('Male', '男');
			$ary['F'] = array('Female', '女');
			
			return $ary;
		}
		
		function MACAU_NATION_DATA_ARY()
		{
			#####################################
			# declare nation data
			#####################################
			$ary = array();
			$ary['0']  = array('Vazio', '空');
			$ary['1']  = array('PORTUGUESA', '葡國');
			$ary['2']  = array('CHINESA', '中國');
			$ary['3']  = array('INGLESA', '英國');
			$ary['4']  = array('ESTADOS UNIDOS', '美國');
			$ary['5']  = array('FILIPINA', '菲律賓');
			$ary['6']  = array('CANADA', '加拿大');
			$ary['7']  = array('BIRMANIA', '緬甸');
			$ary['8']  = array('TAILANDIA', '泰國');
			$ary['9']  = array('COSTA RICA', '哥斯達黎加');
			$ary['10'] = array('TONGA', '東加王國');
			$ary['11'] = array('VENEZUELA', '委內瑞拉');
			$ary['12'] = array('AUSTRALIA', '澳洲');
			$ary['13'] = array('JAPAO', '日本');
			$ary['14'] = array('COREIA DO SUL', '韓國');
			$ary['15'] = array('PERU', '秘魯');
			$ary['16'] = array('INDIA', '印度');
			$ary['17'] = array('HONDURAS', '洪都拉斯');
			$ary['18'] = array('REPUBLICA DOMINICANA', '多明尼加共');
			$ary['19'] = array('DOMINICA', '多米尼克');
			$ary['20'] = array('MALASIA', '馬來西亞');
			$ary['21'] = array('BRASIL', '巴西');
			$ary['22'] = array('INDONESIA', '印尼');
			$ary['23'] = array('MADAGASCAR', '馬達加斯加');
			$ary['24'] = array('SERRA LEONA', '塞拉里昂');
			$ary['25'] = array('NOVA ZELANDIA', '紐西蘭');
			$ary['26'] = array('FRANCA', '法國');
			$ary['27'] = array('SRI LANKA', '斯里蘭卡');
			$ary['28'] = array('IRLANDA', '愛爾蘭');
			$ary['29'] = array('EQUADOR', '厄瓜多爾');
			$ary['30'] = array('BELIZE', '伯利茲');
			$ary['31'] = array('ALEMANHA', '德國');
			$ary['32'] = array('SINGAPURA', '新加坡');
			$ary['33'] = array('PANAMA', '巴拿馬');
			$ary['34'] = array('MAURICIAS', '毛里求斯');
			$ary['35'] = array('NICARAGUA', '尼加拉瓜');
			$ary['36'] = array('GUATEMALA', '危地馬拉');
			$ary['37'] = array('ESPANHA', '西班牙');
			$ary['38'] = array('TANZANIA', '坦桑尼亞');
			$ary['39'] = array('MOCAMBIQUE', '莫桑比克');
			$ary['40'] = array('FIJI', '斐濟');
			$ary['41'] = array('COREIA', '朝鮮');
			$ary['42'] = array('AUSTRIA', '奧地利');
			$ary['43'] = array('ARGENTINA', '阿根廷');
			$ary['44'] = array('AFRICA DO SUL', '南非');
			$ary['45'] = array('BOLIVIA', '玻利維亞');
			$ary['46'] = array('EGIPTO', '埃及');
			$ary['47'] = array('NORUEGA', '挪威');
			$ary['48'] = array('SUICA', '瑞士');
			$ary['49'] = array('SUECIA', '瑞典');
			$ary['50'] = array('PAPUA NOVA GUINE', '巴布亞新畿');
			$ary['51'] = array('UGANDA', '烏干達');
			$ary['52'] = array('MEXICO', '墨西哥');
			$ary['53'] = array('ITALY', '意大利');
			$ary['54'] = array('PAKISTAN', '巴基斯坦');
			$ary['55'] = array('SEYCHEUES', '塞席爾');
			$ary['56'] = array('CUBA', '古巴');
			$ary['57'] = array('SWEDEN', '瑞典');
			$ary['58'] = array('MALTA', '馬耳他');
			$ary['59'] = array('BELGICA', '比利時');
			$ary['60'] = array('VIETNAM', '越南');
			$ary['61'] = array('Austria', '奧地利');
			$ary['62'] = array('Holanda', '荷蘭');
			$ary['63'] = array('St. Lucia', '聖露西亞');
			$ary['64'] = array('Dinamarca', '丹麥');
			$ary['65'] = array('Camboja', '柬埔寨');
			$ary['66'] = array('Trindade', '千里達');
			$ary['67'] = array('Uraguay', '烏拉圭');
			$ary['68'] = array('Polandesa', '波蘭');
			$ary['69'] = array('Guine Basseu', '幾內亞比紹');
			$ary['70'] = array('Angolense', '安哥拉');
			$ary['71'] = array('Russo', '俄羅斯');
			$ary['72'] = array('Jugoslav', '南斯拉夫');
			$ary['73'] = array('Columbia', '哥倫比亞');
			$ary['74'] = array('CHILE', '智利');
			$ary['75'] = array('Hungria', '匈牙利');
			$ary['76'] = array('Samoa Ocidental', '西薩摩亞');
			$ary['77'] = array('Gambia', '岡比亞');
			$ary['78'] = array('Nauru', '諾魯/瑙魯');
			$ary['79'] = array('[UNKNOWN CODE]', '聖多美和林西比');
			$ary['80'] = array('Kiribati', '基里巴斯');
			$ary['81'] = array('Napel', '尼泊爾');
			$ary['82'] = array('Columbia', '哥倫比亞');
			$ary['83'] = array('Finland', '芬蘭');
			$ary['84'] = array('Bugarlia', '保加利亞');
			$ary['85'] = array('Turkia', '土耳其');
			$ary['86'] = array('NIGERIA', '尼日利亞');
			$ary['87'] = array('Arabes Unidos (Estados dos Emirados)', '阿拉伯聯合酋長國');
			$ary['88'] = array('Togo', '多哥');
			$ary['89'] = array('ROMENO', '羅馬尼亞');
			$ary['90'] = array('REPUBLIC OF ARMENIA', '亞美尼亞');
			$ary['91'] = array('DUBAI', '杜拜');
			$ary['999'] = array('INCONHECIDO', '不詳');
									
			return $ary;
		}
		
		function MACAU_RELIGION_DATA_ARY()
		{
			#####################################
			# declare religion data
			#####################################
			$ary = array();
			$ary['1'] = array('Religion1 [En]', 'Religion1 [Chi]');
			$ary['2'] = array('Religion2 [En]', 'Religion2 [Chi]');
			$ary['3'] = array('Religion3 [En]', 'Religion3 [Chi]');
			
			return $ary;
		}
		
		function MACAU_B_PLACE_DATA_ARY()
		{
			#####################################
			# declare birth place data
			#####################################
			$ary = array();
			$ary['0']  = array('Vazio', '空');
			$ary['1']  = array('MACAU', '澳門');
			$ary['2']  = array('CHINA CONTINENTAL', '內地');
			$ary['3']  = array('PORTUGAL', '葡國');
			$ary['4']  = array('HONG KONG', '香港');
			$ary['5']  = array('ESTADOS UNIDOS', '美國');
			$ary['6']  = array('BIRMANIA', '緬甸');
			$ary['7']  = array('FILIPINA', '菲律賓');
			$ary['8']  = array('CANADA', '加拿大');
			$ary['9']  = array('TAILANDIA', '泰國');
			$ary['10'] = array('BRASIL', '巴西');
			$ary['11'] = array('COSTA RICA', '哥斯達黎加');
			$ary['12'] = array('AUSTRALIA', '澳洲');
			$ary['13'] = array('VENEZUELA', '委內瑞拉');
			$ary['14'] = array('MOCAMBIQUE', '莫桑比克');
			$ary['15'] = array('MADAGASCAR', '馬達加斯加');
			$ary['16'] = array('INGLATERRA', '英國');
			$ary['17'] = array('PERU', '秘魯');
			$ary['18'] = array('COREIA DO SUL', '韓國');
			$ary['19'] = array('INDONESIA', '印尼');
			$ary['20'] = array('ANGOLA', '安哥拉');
			$ary['21'] = array('MALASIA', '馬來西亞');
			$ary['22'] = array('JAPAO', '日本');
			$ary['23'] = array('IRLANDA', '愛爾蘭');
			$ary['24'] = array('INDIA', '印度');
			$ary['25'] = array('FRANCA', '法國');
			$ary['26'] = array('SINGAPURA', '新加坡');
			$ary['27'] = array('REPUBLICA DOMINICANA', '多明尼加');
			$ary['28'] = array('DOMINICA', '多米尼克');
			$ary['29'] = array('COLOMBIA', '哥倫比亞');
			$ary['30'] = array('SRI LANKA', '斯里蘭卡');
			$ary['31'] = array('PANAMA', '巴拿馬');
			$ary['32'] = array('MAURICIAS', '毛里求斯');
			$ary['33'] = array('NICARAGUA', '尼加拉瓜');
			$ary['34'] = array('AFRICA DO SUL', '南非');
			$ary['35'] = array('NOVA ZELANDIA', '紐西蘭');
			$ary['36'] = array('TONGA', '東加王國');
			$ary['37'] = array('EQUADOR', '厄瓜多爾 ');
			$ary['38'] = array('GUATEMALA', '危地馬拉');
			$ary['39'] = array('LAOS', '老撾');
			$ary['40'] = array('HONDURAS', '洪都拉斯');
			$ary['41'] = array('FIJI', '斐濟');
			$ary['42'] = array('ALEMANHA', '德國');
			$ary['43'] = array('TANZANIA', '坦桑尼亞');
			$ary['44'] = array('CAMBOJA', '柬埔寨');
			$ary['45'] = array('NO MAR', '海上');
			$ary['46'] = array('LIBERIA', '利比里亞');
			$ary['47'] = array('TRINDADE', '千里達');
			$ary['48'] = array('ESPANHA', '西班牙');
			$ary['49'] = array('BELIZE', '伯利茲');
			$ary['50'] = array('ARGENTINA', '阿根廷');
			$ary['51'] = array('BOLIVIA]', '玻利維亞');
			$ary['52'] = array('EGIPTO', '埃及');
			$ary['53'] = array('HOLANDA', '荷蘭');
			$ary['54'] = array('COREIA', '朝鮮');
			$ary['55'] = array('VIETNAME', '越南');
			$ary['56'] = array('ITALIA', '意大利');
			$ary['57'] = array('SALVADOR', '薩爾瓦多');
			$ary['58'] = array('RUSSIA', '俄羅斯');
			$ary['59'] = array('PAPUA NOVA GUINE', '新畿內亞');
			$ary['60'] = array('UGANDA', '烏干達');
			$ary['61'] = array('MEXICO', '墨西哥');
			$ary['62'] = array('E. AFRICA', '東非');
			$ary['63'] = array('SEYCHEUES', '塞席爾');
			$ary['64'] = array('IRAN', '伊朗');
			$ary['65'] = array('TAIWAN, CHINA', '中國臺灣');
			$ary['66'] = array('PAKISTAN', '巴基斯坦');
			$ary['67'] = array('BAHRAIN', '巴林 ');
			$ary['68'] = array('CUBA', '古巴');
			$ary['69'] = array('SWEDEN', '瑞典');
			$ary['70'] = array('MALTA', '馬耳他');
			$ary['71'] = array('BELGICA', '比利時');
			$ary['72'] = array('Austria', '奧地利');
			$ary['73'] = array('Timor', '東帝汶');
			$ary['74'] = array('Guine Basseu', '幾內亞比紹');
			$ary['75'] = array('Noruega', '挪威');
			$ary['76'] = array('St. Lucia', '聖露西亞');
			$ary['77'] = array('Dinamarca', '丹麥');
			$ary['78'] = array('Uruguay', '烏拉圭');
			$ary['79'] = array('Suica', '瑞士');
			$ary['80'] = array('Polanda', '波蘭');
			$ary['81'] = array('Grandola', '格林納達');
			$ary['82'] = array('Jugoslavia', '南斯拉夫');
			$ary['83'] = array('CHILE', '智利');
			$ary['84'] = array('[UNKOWN CODE]', '突尼斯');
			$ary['85'] = array('Nauru', '諾魯/瑙魯');
			$ary['96'] = array('Cabo Verde', '佛得角');
			$ary['97'] = array('[UNKOWN CODE]', '聖多美和林西比');
			$ary['98'] = array('Napel', '尼泊爾');
			$ary['99'] = array('HUNGRIA', '匈牙利');
			$ary['100'] = array('Jamaica', '牙買加');
			$ary['101'] = array('Finland', '芬蘭');
			$ary['102'] = array('Bugarlia', '保加利亞');
			$ary['103'] = array('Turkia', '土耳其');
			$ary['104'] = array('NIGERIA', '尼日利亞');
			$ary['105'] = array('Arabes Unidos (Estados dos Emirados)', '阿拉伯聯合酋長國');
			$ary['106'] = array('Togo', '多哥');
			$ary['107'] = array('[UNKOWN CODE]', '羅馬尼亞');
			$ary['108'] = array('BBB [En]', '烏克蘭');
			$ary['109'] = array('Macau [En]', '希臘');
			$ary['110'] = array('REPUBLIC OF ARMENIA', '亞美尼亞');
			$ary['111'] = array('DUBAI', '杜拜');
			$ary['999'] = array('INCONHECIDO', '不詳');
						
			return $ary;
		}
		
		function MACAU_S_CODE_DATA_ARY()
		{
			#####################################
			# declare school code data
			#####################################
			$ary = array();
			$ary['001'] = array('Sheng Kung Hui Escola Choi Kou (Macau)', '聖公會(澳門)蔡高中學');
			$ary['002'] = array('Escola Choi Nong Chi Tai', '菜農子弟學校');
			$ary['003'] = array('[UNKNOWN CODE]', '聖善學校');
			$ary['004'] = array('Escola Estrela do Mar', '海星中學');
			$ary['005'] = array('Escola Estrela do Mar (Sucursal)', '海星中學(分校)');
			$ary['006'] = array('Escola da Associacao Geral das Mulheres de Macau', '婦聯學校');
			$ary['007'] = array('[UNKOWN CODE]', '婦聯學校(分校)');
			$ary['008'] = array('[UNKOWN CODE]', '勞工子弟學校(幼稚園)');
			$ary['009'] = array('[UNKOWN CODE]', '勞工子弟學校(小學部)');
			$ary['010'] = array('[UNKOWN CODE]', '勞工子弟學校(中學部)');
			$ary['011'] = array('Escola Fong Chong da Taipa (Sucursal)', '仔坊眾學校 (分校)');
			$ary['012'] = array('[UNKOWN CODE]', '下環浸會學校');
			$ary['013'] = array('[UNKOWN CODE]', '濠江中學附屬幼稚園');
			$ary['014'] = array('[UNKOWN CODE]', '濠江中學附屬小學');
			$ary['015'] = array('[UNKOWN CODE]', '濠江中學');
			$ary['017'] = array('[UNKOWN CODE]', '教業中學 (分校)');
			$ary['018'] = array('Escola "Ilha Verde"', '青洲小學');
			$ary['019'] = array('[UNKOWN CODE]', '教業中學 (分校)');
			$ary['020'] = array('[UNKOWN CODE]', '鏡平學校(小學部)');
			$ary['022'] = array('Escola Keang Peng (em regime nocturno)', '鏡平學校(夜校)');
			$ary['023'] = array('Escola Kwong Tai', '廣大中學');
			$ary['025'] = array('Escola Lin Fong Pou Chai', '蓮峰普濟學校');
			$ary['026'] = array('Escola Ling Nam', '嶺南中學');
			$ary['027'] = array('Escola "Madalena de Canossa"', '瑪大肋納嘉諾撒學校');
			$ary['028'] = array('Escola dos Moradores do Bairro do Patane', '沙梨頭坊眾學校');
			$ary['029'] = array('[UNKOWN CODE]', '化地瑪聖母女子學校');
			$ary['030'] = array('[UNKOWN CODE]', '陳瑞祺永援中學');
			$ary['031'] = array('[UNKOWN CODE]', '陳瑞祺永援中學（分校）');
			$ary['032'] = array('[UNKOWN CODE]', '培正中學');
			$ary['033'] = array('Escola Canossa Pui Ching', '嘉諾撒培貞學校');
			$ary['034'] = array('Escola Pui Ieng', '培英學校');
			$ary['035'] = array('Escola Pui Tou', '培道中學');
			$ary['036'] = array('Escola Pui Tou (Sucursal da Praia Grande)', '培道中學(南灣分校)');
			$ary['037'] = array('[UNKOWN CODE]', '利瑪竇中學(中學部)');
			$ary['038'] = array('[UNKOWN CODE]', '利瑪竇中學(小學部)');
			$ary['039'] = array('[UNKOWN CODE]', '利瑪竇中學(幼稚園)');
			$ary['040'] = array('[UNKOWN CODE]', '聖家學校');
			$ary['042'] = array('Escola Santa Maria Mazzarello', '聖瑪沙利羅學校');
			$ary['044'] = array('[UNKOWN CODE]', '聖羅撒英文中學');
			$ary['045'] = array('[UNKOWN CODE]', '聖羅撒女子中學中文部');
			$ary['046'] = array('Escola de Santa Teresa', '聖德蘭學校');
			$ary['047'] = array('[UNKOWN CODE]', '聖若瑟教區中學(第一校)');
			$ary['049'] = array('[UNKOWN CODE]', '聖若瑟教區中學(第二、三校)');
			$ary['050'] = array('[UNKOWN CODE]', '聖若瑟教區中學(第四校)');
			$ary['051'] = array('[UNKOWN CODE]', '聖若瑟教區中學第五校 (中文部)');
			$ary['052'] = array('[UNKOWN CODE]', '聖若瑟教區中學(第六校)');
			$ary['053'] = array('[UNKOWN CODE]', '九澳聖若瑟學校');
			$ary['054'] = array('[UNKOWN CODE]', '聖保祿學校');
			$ary['055'] = array('Escola Seong Fan', '商訓夜中學');
			$ary['056'] = array('[UNKOWN CODE]', '聖玫瑰學校');
			$ary['057'] = array('[UNKOWN CODE]', '下環坊眾學校');
			$ary['059'] = array('Escola Tak Meng', '德明學校');
			$ary['060'] = array('Escola Tong Nam', '東南學校');
			$ary['061'] = array('Escola Tong Sin Tong (Diurno)', '同善堂中學(日校)');
			$ary['062'] = array('Escola Tong Sin Tong (Nocturno)', '同善堂中學(夜校)');
			$ary['063'] = array('Escola Veng Chun', '潁川學校');
			$ary['064'] = array('[UNKOWN CODE]', '粵華中文中學');
			$ary['065'] = array('[UNKOWN CODE]', '粵華英文中學');
			$ary['066'] = array('[UNKOWN CODE]', '嘉諾撒聖心中學');
			$ary['067'] = array('[UNKOWN CODE]', '嘉諾撒聖心英文中學');
			$ary['069'] = array('[UNKOWN CODE]', '沙梨頭浸信學校');
			$ary['071'] = array('[UNKOWN CODE]', '協同特殊教育學校');
			$ary['072'] = array('Escola Cham Son de Macau', '澳門浸信中學');
			$ary['073'] = array('[UNKOWN CODE]', '雷鳴道主教紀念學校');
			$ary['074'] = array('[UNKOWN CODE]', '庇道學校');
			$ary['075'] = array('[UNKOWN CODE]', '明愛學校');
			$ary['076'] = array('[UNKOWN CODE]', '明愛幼稚園');
			$ary['077'] = array('Escola "Ma Lai Son Ke Lim"', '馬禮遜紀念學校');
			$ary['078'] = array('[UNKOWN CODE]', '聯國學校');
			$ary['079'] = array('Escola Hoi Fai', '海暉學校');
			$ary['081'] = array('[UNKOWN CODE]', '聖安東尼幼稚園');
			$ary['082'] = array('Escola Fukien', '福建學校');
			$ary['083'] = array('Centro de Desenvolvimento Infantil', '啟智中心');
			$ary['084'] = array('[UNKOWN CODE]', '魯彌士主教幼稚園');
			$ary['085'] = array('[UNKOWN CODE]', '樂富中葡幼稚園');
			$ary['087'] = array('[UNKOWN CODE]', '民安中葡幼稚園');
			$ary['089'] = array('[UNKOWN CODE]', '巴波沙中葡幼稚園');
			$ary['090'] = array('[UNKOWN CODE]', '永添中葡幼稚園');
			$ary['092'] = array('Escola Luso-Chinesa de Coloane', '路環中葡學校');
			$ary['093'] = array('[UNKOWN CODE]', '北區中葡小學');
			$ary['094'] = array('Escola Luso-Chinesa da Taipa', '仔中葡學校');
			$ary['095'] = array('Escola Primaria Oficial Luso-Chinesa "Sir Robert Ho Tung"', '何東中葡小學');
			$ary['096'] = array('[UNKOWN CODE]', '巴波沙中葡小學');
			$ary['100'] = array('[UNKOWN CODE]', '高美士中葡中學');
			$ary['102'] = array('Colegio Dom Bosco (Yuet Wah)', '鮑思高粵華小學（中文部）');
			$ary['106'] = array('[UNKOWN CODE]', '聯國幼稚園');
			$ary['107'] = array('[UNKOWN CODE]', '聖若瑟教區中學第五校 (英文部)');
			$ary['108'] = array('[UNKOWN CODE]', '黑沙官立小學');
			$ary['110'] = array('[UNKOWN CODE]', '聖若瑟教區中學﹝二、三校﹞(夜間部)');
			$ary['111'] = array('[UNKOWN CODE]', '灣景中葡小學');
			$ary['112'] = array('Escola Kwong Tai (Sucursal)', '廣大中學(分校)');
			$ary['113'] = array('Escola dos Moradores de Macau', '澳門坊眾學校');
			$ary['114'] = array('Escola Memorial Dr. Sun Yat Sen', '孫中山紀念學校');
			$ary['115'] = array('Escola Chong Tak de Macau', '澳門中德學校');
			$ary['116'] = array('[UNKOWN CODE]', '培華中學');
			$ary['117'] = array('Escola Choi Nong Chi Tai (Sucursal)', '菜農子弟學校(分校)');
			$ary['118'] = array('Escola Hou Kong (Sucursal da Taipa)', '濠江中學(仔分校)');
			$ary['119'] = array('[UNKOWN CODE]', '同善堂幼稚園');
			$ary['120'] = array('Escola Fong Chong da Taipa', '仔坊眾學校');
			$ary['121'] = array('[UNKOWN CODE]', '庇道學校(夜間部)');
			$ary['122'] = array('[UNKOWN CODE]', '啟聰中心');
			$ary['125'] = array('[UNKOWN CODE]', '小學回歸教育');
			$ary['127'] = array('[UNKOWN CODE]', '澳門三育中學');
			$ary['128'] = array('Centro de Desenvolvimento Infantil Kai Kin', '啟健中心');
			$ary['129'] = array('Escola Estrela do Mar (Nocturno)', '海星中學(夜間部)');
			$ary['130'] = array('Sheng Kung Hui Choi Kou Escola (Macau) (Nocturno)', '聖公會(澳門)蔡高中學(夜間部)');
			$ary['131'] = array('[UNKOWN CODE]', '鏡平學校(中學部)');
			$ary['132'] = array('Escola Xin Hua', '新華學校');
			$ary['133'] = array('[UNKOWN CODE]', '培道中學小學部分教處');
			$ary['134'] = array('[UNKOWN CODE]', '澳門三育中學(英文部)');
			$ary['135'] = array('[UNKOWN CODE]', '中葡職業技術學校');
			$ary['136'] = array('[UNKOWN CODE]', '高美士中葡中學(日間葡文部)');
			$ary['137'] = array('[UNKOWN CODE]', '澳門工聯職業技術中學');
			$ary['138'] = array('[UNKOWN CODE]', '二龍喉中葡小學');
			$ary['139'] = array('[UNKOWN CODE]', '二龍喉中葡小學(葡文部)');
			$ary['140'] = array('Escola Portuguesa de Macau', '澳門葡文學校');
			$ary['142'] = array('[UNKOWN CODE]', '中葡職業技術學校(夜校)');
			$ary['143'] = array('[UNKOWN CODE]', '中學回歸教育');
			$ary['144'] = array('[UNKOWN CODE]', '庇道學校(分校)');
			$ary['145'] = array('[UNKOWN CODE]', '澳門大學附屬應用學校');
			$ary['146'] = array('[UNKOWN CODE]', '新華夜中學');
			$ary['147'] = array('Sheng Kung Hui Escola Choi Kou (Macau)(Sucursal)', '聖公會(澳門)蔡高中學(分校)');
			$ary['148'] = array('[UNKOWN CODE]', '高美士中葡中學(葡文成人夜間課程)');
			$ary['149'] = array('Escola Tai Heng', '大興學校');
			$ary['150'] = array('Escola Tai Heng', '大興學校(夜間部)');
			$ary['151'] = array('[UNKOWN CODE]', '語言推廣中心');
			$ary['153'] = array('[UNKOWN CODE]', '東南學校-中學部');
			$ary['154'] = array('Escola Xin Hua', '新華學校');
			$ary['155'] = array('Escola dos Moradores de Macau', '澳門坊眾學校');
			$ary['156'] = array('Colegio Anglicano de Macau', '聖公會中學(澳門)');
			$ary['157'] = array('ESCOLA INTERNACIONAL DE MACAU', '澳門國際學校');
			$ary['158'] = array('[UNKOWN CODE]', '勞工事務局職業培訓廳 (學徒培訓)');
			$ary['159'] = array('[UNKOWN CODE]', '澳門浸信中學(中學部)');
			$ary['160'] = array('[UNKOWN CODE]', '澳門加拿大學院');
			$ary['161'] = array('[UNKOWN CODE]', '庇道學校〈分校〉英文部');
			$ary['162'] = array('[UNKOWN CODE]', '創新中學');
			$ary['163'] = array('Escola Kao Yip (Primaria e Pre-Primaria) Sucursal', '教業中學附屬小學暨幼稚園分校');
			$ary['164'] = array('[UNKOWN CODE]', '澳門演藝學院舞蹈學校');
			$ary['165'] = array('Escola Estrela do Mar', '海星中學');
			$ary['166'] = array('Escola Portuguesa de Macau (S. Nocturna)', '澳門葡文學校(夜間部)');
			$ary['167'] = array('Escola Pui Tou (Sucursal da Taipa)', '培道中學仔幼稚園分校');
			$ary['168'] = array('Colegio Dom Bosco (Yuet Wah)', '鮑思高粵華小學（英文部）');
			$ary['169'] = array('Escola de Musica do Conservatorio de Macau', '澳門演藝學院音樂學校');
			$ary['170'] = array('Escola Kao Yip', '教業中學');
			$ary['171'] = array('Colegio Mateus Ricci', '利瑪竇中學');
			$ary['172'] = array('Escola Secundaria Tecnico-Profissional da Associacao Geral dos Operarios de Macau (Nocturno)', '澳門工聯職業技術中學(夜校)');
			$ary['777'] = array('TEST SCHOOL', '測試學校');
															
			return $ary;
		}
		
		function MACAU_ID_TYPE_DATA_ARY()
		{
			#####################################
			# declare ID Type data
			#####################################
			$ary = array();
			$ary['*']     = array('NULO', '空');
			$ary['ADN']   = array('ASSENTO DE NASCIMENTO', '澳門出生登記');
			$ary['BIC']   = array('Billhete de Identidade Consular', '領事身份證');
			$ary['BIE']   = array('BILHETE DE IDENTIDADE DE CIDADAO ESTRANGEIRO', '非葡藉認別證');
			$ary['BIN']   = array('BILHETE DE IDENTIDADE DE CIDADAO NACIONAL', '葡藉認別證');
			$ary['BIR']   = array('BILHETE DE IDENTIDADE DE RESIDENTE DE MACAU', '澳門居民身份證');
			$ary['CEDUL'] = array('CEDULA PESSOAL', '葡國中央登記');
			$ary['CIP']   = array('CEDULA DE IDENTIFICACAO POLICIAL', '澳門身份證');
			$ary['COB']   = array('CERTIFICATE OF BIRTH', '香港出生登記');
			$ary['CPAS']  = array('PASSPORTE CHINES', '中國護照');
			$ary['CPASS'] = array('SALVO-CONDUTO DA CHINA', '中國通行証');
			$ary['HKID']  = array('HONG KONG IDENTITY CARD', '香港身份證');
			$ary['HKREP'] = array('RE-ENTRY PERMIT', '回港証');
			$ary['PAS']   = array('PASSPORTE', '外國護照');
			$ary['PPAS']  = array('PASSPORTE PORTUGUES', '葡國護照');
			$ary['RECIB'] = array('RECIBO', '移民局收條');
			$ary['TITNR'] = array('TITULO DE IDENTIFICACAO DE TRABALHADOR NAO-RESIDENTE', '非本地勞工身份咭');
			$ary['TPT']   = array('TITULO DE PERMANENCIA TEMPORARIA', '臨時逗留證');
			$ary['TRP']   = array('TITULO DE RESIDENCIA (PERMANENTE)', '永久居留證');
			$ary['TRT']   = array('TITULO DE RESIDENCIA (TEMPORARIO)', '臨時居留證');
			$ary['BIRNP'] = array('[UNKOWN CODE]', '非永久性居民身份證');
			$ary['BIRP']  = array('Bilhete de identidade de residente permanente', '永久性居民身份證');
			$ary['BICHN'] = array('Bilhelte de Identidade da RPC', '中國居民身份證');
			$ary['CI']    = array('Document of Identity for Visa Purpose', '簽證身份書');
			$ary['HKPAS'] = array('HKSAR passport', '香港特區護照');
						
			return $ary;
		}
		
		function MACAU_ID_PLACE_DATA_ARY()
		{
			#####################################
			# declare ID issue place data
			#####################################
			$ary = array();
			$ary['0']  = array('Vazio', '空');
			$ary['1']  = array('MACAU', '澳門');
			$ary['2']  = array('CHINA CONTINENTAL', '內地');
			$ary['3']  = array('PORTUGAL', '葡國');
			$ary['4']  = array('HONG KONG', '香港');
			$ary['5']  = array('ESTADOS UNIDOS', '美國');
			$ary['6']  = array('BIRMANIA', '緬甸');
			$ary['7']  = array('FILIPINA', '菲律賓');
			$ary['8']  = array('CANADA', '加拿大');
			$ary['9']  = array('TAILANDIA', '泰國');
			$ary['10'] = array('BRASIL', '巴西');
			$ary['11'] = array('COSTA RICA', '哥斯達黎加');
			$ary['12'] = array('AUSTRALIA', '澳洲');
			$ary['13'] = array('VENEZUELA', '委內瑞拉');
			$ary['14'] = array('MOCAMBIQUE', '莫桑比克');
			$ary['15'] = array('MADAGASCAR', '馬達加斯加');
			$ary['16'] = array('INGLATERRA', '英國');
			$ary['17'] = array('PERU', '秘魯');
			$ary['18'] = array('COREIA DO SUL', '韓國');
			$ary['19'] = array('INDONESIA', '印尼');
			$ary['20'] = array('ANGOLA', '安哥拉');
			$ary['21'] = array('MALASIA', '馬來西亞');
			$ary['22'] = array('JAPAO', '日本');
			$ary['23'] = array('IRLANDA', '愛爾蘭');
			$ary['24'] = array('INDIA', '印度');
			$ary['25'] = array('FRANCA', '法國');
			$ary['26'] = array('SINGAPURA', '新加坡');
			$ary['27'] = array('REPUBLICA DOMINICANA', '多明尼加');
			$ary['28'] = array('DOMINICA', '多米尼克');
			$ary['29'] = array('COLOMBIA', '哥倫比亞');
			$ary['30'] = array('SRI LANKA', '斯里蘭卡');
			$ary['31'] = array('PANAMA', '巴拿馬');
			$ary['32'] = array('MAURICIAS', '毛里求斯');
			$ary['33'] = array('NICARAGUA', '尼加拉瓜');
			$ary['34'] = array('AFRICA DO SUL', '南非');
			$ary['35'] = array('NOVA ZELANDIA', '紐西蘭');
			$ary['36'] = array('TONGA', '東加王國');
			$ary['37'] = array('EQUADOR', '厄瓜多爾 ');
			$ary['38'] = array('GUATEMALA', '危地馬拉');
			$ary['39'] = array('LAOS', '老撾');
			$ary['40'] = array('HONDURAS', '洪都拉斯');
			$ary['41'] = array('FIJI', '斐濟');
			$ary['42'] = array('ALEMANHA', '德國');
			$ary['43'] = array('TANZANIA', '坦桑尼亞');
			$ary['44'] = array('CAMBOJA', '柬埔寨');
			$ary['45'] = array('NO MAR', '海上');
			$ary['46'] = array('LIBERIA', '利比里亞');
			$ary['47'] = array('TRINDADE', '千里達');
			$ary['48'] = array('ESPANHA', '西班牙');
			$ary['49'] = array('BELIZE', '伯利茲');
			$ary['50'] = array('ARGENTINA', '阿根廷');
			$ary['51'] = array('BOLIVIA]', '玻利維亞');
			$ary['52'] = array('EGIPTO', '埃及');
			$ary['53'] = array('HOLANDA', '荷蘭');
			$ary['54'] = array('COREIA', '朝鮮');
			$ary['55'] = array('VIETNAME', '越南');
			$ary['56'] = array('ITALIA', '意大利');
			$ary['57'] = array('SALVADOR', '薩爾瓦多');
			$ary['58'] = array('RUSSIA', '俄羅斯');
			$ary['59'] = array('PAPUA NOVA GUINE', '新畿內亞');
			$ary['60'] = array('UGANDA', '烏干達');
			$ary['61'] = array('MEXICO', '墨西哥');
			$ary['62'] = array('E. AFRICA', '東非');
			$ary['63'] = array('SEYCHEUES', '塞席爾');
			$ary['64'] = array('IRAN', '伊朗');
			$ary['65'] = array('TAIWAN, CHINA', '中國臺灣');
			$ary['66'] = array('PAKISTAN', '巴基斯坦');
			$ary['67'] = array('BAHRAIN', '巴林 ');
			$ary['68'] = array('CUBA', '古巴');
			$ary['69'] = array('SWEDEN', '瑞典');
			$ary['70'] = array('MALTA', '馬耳他');
			$ary['71'] = array('BELGICA', '比利時');
			$ary['72'] = array('Austria', '奧地利');
			$ary['73'] = array('Timor', '東帝汶');
			$ary['74'] = array('Guine Basseu', '幾內亞比紹');
			$ary['75'] = array('Noruega', '挪威');
			$ary['76'] = array('St. Lucia', '聖露西亞');
			$ary['77'] = array('Dinamarca', '丹麥');
			$ary['78'] = array('Uruguay', '烏拉圭');
			$ary['79'] = array('Suica', '瑞士');
			$ary['80'] = array('Polanda', '波蘭');
			$ary['81'] = array('Grandola', '格林納達');
			$ary['82'] = array('Jugoslavia', '南斯拉夫');
			$ary['83'] = array('CHILE', '智利');
			$ary['84'] = array('[UNKOWN CODE]', '突尼斯');
			$ary['85'] = array('Nauru', '諾魯/瑙魯');
			$ary['96'] = array('Cabo Verde', '佛得角');
			$ary['97'] = array('[UNKOWN CODE]', '聖多美和林西比');
			$ary['98'] = array('Napel', '尼泊爾');
			$ary['99'] = array('HUNGRIA', '匈牙利');
			$ary['100'] = array('Jamaica', '牙買加');
			$ary['101'] = array('Finland', '芬蘭');
			$ary['102'] = array('Bugarlia', '保加利亞');
			$ary['103'] = array('Turkia', '土耳其');
			$ary['104'] = array('NIGERIA', '尼日利亞');
			$ary['105'] = array('Arabes Unidos (Estados dos Emirados)', '阿拉伯聯合酋長國');
			$ary['106'] = array('Togo', '多哥');
			$ary['107'] = array('[UNKOWN CODE]', '羅馬尼亞');
			$ary['108'] = array('BBB [En]', '烏克蘭');
			$ary['109'] = array('Macau [En]', '希臘');
			$ary['110'] = array('REPUBLIC OF ARMENIA', '亞美尼亞');
			$ary['111'] = array('DUBAI', '杜拜');
			$ary['999'] = array('INCONHECIDO', '不詳');
			
			return $ary;
		}
		
		function MACAU_S6_TYPE_DATA_ARY()
		{
			#####################################
			# declare stay permission type data
			#####################################
			$ary = array();
			$ary['1'] = array('Permanent', 'Permanent');
			$ary['2'] = array('Limited', 'Limited');
			$ary['3'] = array('Other', 'Other');
			
			return $ary;
		}
		
		function MACAU_R_AREA_DATA_ARY()
		{
			#####################################
			# declare residential area data
			#####################################
			$ary = array();
			$ary['M'] = array('Macau', 'Macau');
			$ary['C'] = array('China', 'China');
			$ary['O'] = array('Other', 'Other');
			
			return $ary;
		}
		
		function MACAU_AREA_DATA_ARY()
		{
			#####################################
			# declare address area data
			#####################################
			$ary = array();
			$ary['M'] = array('Macau', 'Macau');
			$ary['T'] = array('Taipa', 'Taipa');
			$ary['C'] = array('AAA', 'AAA');
			$ary['O'] = array('Other', 'Other');
			
			return $ary;
		}
		
		function MACAU_MARITAL_STATUS_DATA_ARY()
		{
			#####################################
			# declare marital status data
			#####################################
			$ary = array();
			$ary['0'] = array('Single', '單身');
			$ary['1'] = array('Married', '已婚');
			
			return $ary;
		}
		
		function MACAU_GUARD_DATA_ARY()
		{
			#####################################
			# declare guardian Type data
			#####################################
			$ary = array();
			$ary['F'] = array('Father', '父親');
			$ary['M'] = array('Mother', '母親');
			$ary['O'] = array('Other', '其他');
			
			return $ary;
		}
		
		function MACAU_LIVE_SAME_ARY()
		{
			#####################################
			# declare stay together data
			#####################################
			$ary = array();
			$ary['1'] = array('Yes', '是');
			$ary['0'] = array('No', '否');
			
			return $ary;
		}
		
		function MACAU_REASON_ARY()
		{
			$ary = array();
			$ary['0'] = array('Vazio', '空');
			$ary['10'] = array('Transferencia no territorio', '本澳內轉校');
			$ary['20'] = array('Nao cumprimento das regras da escola', '犯校規');
			$ary['21'] = array('[UNKOWN CODE]', '犯校規 - 學業問題');
			$ary['22'] = array('[UNKOWN CODE]', '犯校規 - 品行問題');
			$ary['23'] = array('[UNKOWN CODE]', '犯校規 - 其他(請註明)');
			$ary['30'] = array('Saida do territorio', '離開本澳');
			$ary['40'] = array('Falta de meios', '財政困難');
			$ary['50'] = array('Doenca', '疾病');
			$ary['60'] = array('Por iniciativa propria', '自動退學');
			$ary['70'] = array('[UNKOWN CODE]', '學業不理想自動退學');
			$ary['80'] = array('Nunca se apresentou no ano lectivo', '本學年從未入學');
			$ary['90'] = array('Emprego', '工作');
			$ary['100'] = array('falecimento', '逝世');
			$ary['110'] = array('[UNKOWN CODE]', '外地就讀');
			$ary['990'] = array('Outras', '其他');
			
			return $ary;
		}
		
		/*
		function MACAU_GRADE_ARY()
		{
			$ary = array();
			$ary['1']  = array('P1', '小一');
			$ary['2']  = array('P2', '小二');
			$ary['3']  = array('P3', '小三');
			$ary['4']  = array('P4', '小四');
			$ary['5']  = array('P5', '小五');
			$ary['6']  = array('P6', '小六');
			$ary['7']  = array('SG1', '初一');
			$ary['8']  = array('SG2', '初二');
			$ary['9']  = array('SG3', '初三');
			$ary['10'] = array('SC1', '高一');
			$ary['11'] = array('SC2', '高二');
			$ary['12'] = array('SC3', '高三');
			
			$ary['CR'] = array('CR', '高中回歸');
			
			$ary['E1'] = array('E1', '特一');
			$ary['E2'] = array('E2', '特二');
			$ary['E3'] = array('E3', '特三');
			
			$ary['EE'] = array('EE', '特殊教育');
			$ary['EE1'] = array('E1', '特一');
			$ary['EE2'] = array('E2', '特二');
			$ary['EE3'] = array('E3', '特三');
			
			$ary['EEI1'] = array('EI 1', '特幼一');
			$ary['EEI2'] = array('EI 2', '特幼二');
			$ary['EEI3'] = array('EI 3', '特幼三');
			
			$ary['EI1'] = array('EI 1', '特幼一');
			$ary['EI2'] = array('EI 2', '特幼二');
			$ary['EI3'] = array('EI 3', '特幼三');
			
			$ary['EEP5'] = array('EP5', '特小五');
			$ary['EEP6'] = array('EP6', '特小六');
			
			$ary['EP1'] = array('EP1', '特小一');
			$ary['EP2'] = array('EP2', '特小二');
			$ary['EP3'] = array('EP3', '特小三');
			$ary['EP4'] = array('EP4', '特小四');
			$ary['EP5'] = array('EP5', '特小五');
			$ary['EP6'] = array('EP6', '特小六');
						
			return $ary;
		}*/
		
		##################################################################################
		# Malaysia Functions
		##################################################################################
		
		function RETRIEVE_STUDENT_INFO_BASIC_MAL($StudentID='')
		{
			$result = array();
			
			if($StudentID)
			{
				$sql = "SELECT
							d.SRS_ID,
							d.STUD_ID,
							a.ChineseName, a.EnglishName,
							a.Gender, d.NATION,
							a.HomeTelNo,
							IF(c.YearName IS NULL OR c.YearName = '', '--', c.YearName) AS YearName,
							IF(a.ClassName IS NULL OR a.ClassName = '', '--', a.ClassName) AS ClassName,
							IF(a.ClassNumber IS NULL OR a.ClassNumber = '', '--', a.ClassNumber) AS ClassNumber,
							d.DateModified,
							d.ModifyBy,
							b.YearClassID,
							a.RecordStatus,
							d.NATION_OTHERS
						FROM 
							INTRANET_USER as a
							left join YEAR_CLASS as b on (b.ClassTitleEN = a.ClassName and b.AcademicYearID='". Get_Current_Academic_Year_ID() ."')
							left join YEAR as c on (c.YearID = b.YearID)
							left join STUDENT_REGISTRY_STUDENT as d on (a.UserID = d.UserID)
						WHERE 
							a.UserID = '$StudentID'
						";
				$result = $this->returnArray($sql);
			}
			
			return $result;
		}
		
		function RETRIEVE_STUDENT_INFO_ADV_MAL($StudentID='')
		{
			$result = array();
			
			if($StudentID)
			{
				$sql = "select 
							d.SRS_ID,
							d.STUD_ID,
							a.ChineseName, a.EnglishName,
							a.Gender, DATE_FORMAT(a.DateOfBirth, '%Y-%c-%e') AS DateOfBirth, d.B_PLACE,
							d.NATION, d.ANCESTRAL_HOME, d.RACE,
							d.ID_NO, d.BIRTH_CERT_NO,
							d.RELIGION, d.FAMILY_LANG, d.PAYMENT_TYPE,
							a.HomeTelNo, d.EMAIL,
							d.LODGING,
							d.ADDRESS1, d.ADDRESS2,
							d.ADDRESS3,
							d.ADDRESS4, d.ADDRESS5,
							d.ADDRESS6,
							IF(c.YearName IS NULL OR c.YearName = '', '--', c.YearName) AS YearName,
							IF(a.ClassName IS NULL OR a.ClassName = '', '--', a.ClassName) AS ClassName,
							IF(a.ClassNumber IS NULL OR a.ClassNumber = '', '--', a.ClassNumber) AS ClassNumber,
							d.ADMISSION_DATE,
							d.BRO_SIS_IN_SCHOOL, d.PRI_SCHOOL,
							d.DateModified,
							d.ModifyBy,
							b.YearClassID,
							d.NATION_OTHERS,
							d.B_PLACE_OTHERS,
							d.RACE_OTHERS,
							d.RELIGION_OTHERS,
							d.FAMILY_LANG_OTHERS,
							d.LODGING_OTHERS
						from 
							INTRANET_USER as a
							left join YEAR_CLASS as b on (b.ClassTitleEN = a.ClassName and b.AcademicYearID='". Get_Current_Academic_Year_ID() ."')
							left join YEAR as c on (c.YearID = b.YearID)
							left join STUDENT_REGISTRY_STUDENT as d on (a.UserID = d.UserID)
						where 
							a.UserID = '$StudentID'
						";
				$result = $this->returnArray($sql);
			}

			return $result;
		}
		
		function SEARCH_SIBLINGS($StudentNo='', $AcademicYearID='')
		{
			$result = array();
			//echo $AcademicYearID;
			if($StudentNo)
			{
				$sql = "select
							USR.UserID as UserID,
							".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as ClassName,
							ycu.ClassNumber as ClassNumber,
							".Get_Lang_Selection('USR.ChineseName','USR.EnglishName')." as StudentName
						from
							INTRANET_USER as USR INNER JOIN
							YEAR_CLASS_USER as ycu on (USR.UserID = ycu.UserID) INNER JOIN
							YEAR_CLASS as yc on (ycu.YearClassID = yc.YearClassID) INNER JOIN
							STUDENT_REGISTRY_STUDENT as srs on (USR.UserID = srs.UserID) INNER JOIN
							YEAR as y on (yc.YearID = y.YearID)
 						where
							yc.AcademicYearID = '$AcademicYearID' and srs.STUD_ID = '$StudentNo'
						order by
							y.Sequence, yc.Sequence, ycu.ClassNumber";
							
				$result = $this->returnArray($sql);
			}
			//echo $sql;
			return $result;	
		}
		
		function RETRIEVE_STUDENT_REGISTRY_STATUS($StudentID='')
		{
			$result = array();
			
			if($StudentID)
			{
				$sql = "select RecordStatus from STUDENT_REGISTRY_STATUS where UserID = '$StudentID' AND IsCurrent != 0";
				$result = $this->returnVector($sql);
			}
			
			return $result[0];			
		}
		
		function RETRIEVE_GUARDIAN_INFO_ADV_MAL($StudentID='', $Type='')
		{
			$result = array();
			
			if($StudentID)
			{
				$sql = "select 
							SRPG_ID,
							NAME_C, NAME_E,
							G_GENDER,
							GUARD, G_RELATION, LIVE_SAME,
							PROF, JOB_NATURE, JOB_TITLE,
							MARITAL_STATUS, EDU_LEVEL,
							TEL, MOBILE, OFFICE_TEL,
							POSTAL_ADDR1, POSTAL_ADDR2,
							POSTAL_ADDR3,
							POSTAL_ADDR4, POSTAL_ADDR5,
							POSTAL_ADDR6,
							POST_CODE, EMAIL,
							JOB_NATURE_OTHERS,
							JOB_TITLE_OTHERS,
							EDU_LEVEL_OTHERS
						from 
							STUDENT_REGISTRY_PG
						where 
							StudentID = '$StudentID' AND PG_TYPE = '$Type'
						";
				$result = $this->returnArray($sql);
			}
			
			return $result;
		}
		
		function updateStudentRegistry_Simple_Mal($dataAry=array(), $userid, $pgAry=array())
		{			
			global $UserID;
			
			$sql = "SELECT COUNT(*) FROM STUDENT_REGISTRY_STUDENT WHERE UserID='$userid'";
			$count = $this->returnVector($sql);
			
			# update STUDENT_REGISTRY_STUDENT data
			if($count[0]==0) {
				# insert record	
				foreach($dataAry as $fields[]=>$values[]);
				
				$sql = "INSERT INTO STUDENT_REGISTRY_STUDENT (UserID, ";
				foreach($fields as $field) $sql .= "$field, ";
				$sql .= "DateInput, InputBy, DateModified, ModifyBy) VALUES ('$userid', ";
				foreach($values as $value) $sql .= "'$value', ";
				$sql .= "NOW(), '$UserID', NOW(), '$UserID')";
			} else {
				# update record
				$sql = "UPDATE STUDENT_REGISTRY_STUDENT SET ";
				foreach($dataAry as $field=>$value)
					$sql .= $field."='".$value."', ";
				$sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE UserID='$userid'";
			}
			$result = $this->db_db_query($sql);
			
			# update STUDENT_REGISTRY_PG data
			$sql = "SELECT GUARD FROM STUDENT_REGISTRY_PG WHERE StudentID='$userid' AND PG_TYPE='G'";
			$temp = $this->returnVector($sql);
			
			foreach($pgAry as $pgfields[]=>$pgvalues[]);
			
			if($temp[0]!="") {	# existing guardian
				$sql = "UPDATE STUDENT_REGISTRY_PG SET ";
				foreach($pgAry as $pgfield=>$pgvalue)
					$sql .= $pgfield."='".$pgvalue."', ";
				$sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE StudentID='$userid' AND PG_TYPE='G'";
				$this->db_db_query($sql);
				
				if($temp[0]=="F" || $temp[0]=="M") {	# update Father / Mother
					$sql = "UPDATE STUDENT_REGISTRY_PG SET ";
					foreach($pgAry as $pgfield=>$pgvalue)
						$sql .= $pgfield."='".$pgvalue."', ";
					$sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE StudentID='$userid' AND PG_TYPE='".$temp[0]."'";
					$this->db_db_query($sql);
				}
			} else {			# no existing guardian
			
				$sql = "INSERT INTO STUDENT_REGISTRY_PG (StudentID, PG_TYPE, ";
				foreach($pgfields as $field) $sql .= "$field, ";
				$sql .= "GUARD, DateInput, InputBy, DateModified, ModifyBy) VALUES ('$userid','G',";
				foreach($pgvalues as $value) $sql .= "'$value', ";
				$sql .= "'O', NOW(), '$UserID', NOW(), '$UserID')";
				$this->db_db_query($sql);
				//echo $sql.'<br>';
			}
			
			return $result;
		}
		
		function MAL_B_PLACE_DATA_ARY()
		{
			#####################################
			# declare birth place data
			#####################################
			$ary = array();
			/*
			$ary['1'] = array('Selangor', '雪兰莪'); 
			$ary['2'] = array('Kuala Lumpur', '吉隆坡');
			$ary['3'] = array('Johor', '柔佛');
			$ary['4'] = array('Malacca', '马六甲');
			$ary['5'] = array('Negeri Sembilan', '森美兰');
			$ary['6'] = array('Perak', '吡叻');
			$ary['7'] = array('Kedah', '吉打');
			$ary['8'] = array('Penang', '槟城');
			$ary['9'] = array('Perlis', '玻璃市');
			$ary['10'] = array('Kelantan', '吉兰丹');
			$ary['11'] = array('Pahang', '彭亨');
			$ary['12'] = array('Terengganu', '丁加奴');
			$ary['13'] = array('Sarawak', '砂劳越');
			$ary['14'] = array('Sabah', '沙巴');
			$ary['15'] = array('Wilayah Persekutuan Labuan', '纳闽直辖区');
			*/
			$ary['1'] = array('Malaysia', '马来西亚');
			$ary['16'] = array('Australia', '澳洲');
			$ary['17'] = array('Brunei', '汶莱');
			$ary['18'] = array('Cambodia', '柬埔寨');
			$ary['19'] = array('China', '中国');
			$ary['20'] = array('England', '英国');
			$ary['21'] = array('Hong Kong', '香港');
			$ary['22'] = array('India', '印度');
			$ary['23'] = array('Indonesia', '印尼');
			$ary['24'] = array('Japan', '日本');
			$ary['25'] = array('Korea', '韩国');
			$ary['26'] = array('New Zealand', '纽西兰');
			$ary['27'] = array('Singapore', '新加坡');
			$ary['28'] = array('Taiwan', '台湾');
			$ary['29'] = array('Thailand', '泰国');
			$ary['30'] = array('United States', '美国');
			$ary['31'] = array('Taiwan', '台湾');
			$ary['-9'] = array('Others', '其他');
			
			return $ary;
		}
		
		function MAL_NATION_DATA_ARY()
		{
			#####################################
			# declare nation data
			#####################################
			$ary = array();
			/*
			$ary['1'] = array('Selangor', '雪兰莪'); 
			$ary['2'] = array('Kuala Lumpur', '吉隆坡');
			$ary['3'] = array('Johor', '柔佛');
			$ary['4'] = array('Malacca', '马六甲');
			$ary['5'] = array('Negeri Sembilan', '森美兰');
			$ary['6'] = array('Perak', '吡叻');
			$ary['7'] = array('Kedah', '吉打');
			$ary['8'] = array('Penang', '槟城');
			$ary['9'] = array('Perlis', '玻璃市');
			$ary['10'] = array('Kelantan', '吉兰丹');
			$ary['11'] = array('Pahang', '彭亨');
			$ary['12'] = array('Terengganu', '丁加奴');
			$ary['13'] = array('Sarawak', '砂劳越');
			$ary['14'] = array('Sabah', '沙巴');
			$ary['15'] = array('Wilayah Persekutuan Labuan', '纳闽直辖区');
			*/
			$ary['1'] = array('Malaysia', '马来西亚');
			$ary['16'] = array('Australia', '澳洲');
			$ary['17'] = array('Brunei', '汶莱');
			$ary['18'] = array('Cambodia', '柬埔寨');
			$ary['19'] = array('China', '中国');
			$ary['20'] = array('England', '英国');
			$ary['21'] = array('Hong Kong', '香港');
			$ary['22'] = array('India', '印度');
			$ary['23'] = array('Indonesia', '印尼');
			$ary['24'] = array('Japan', '日本');
			$ary['25'] = array('Korea', '韩国');
			$ary['26'] = array('New Zealand', '纽西兰');
			$ary['27'] = array('Singapore', '新加坡');
			$ary['28'] = array('Taiwan', '台湾');
			$ary['29'] = array('Thailand', '泰国');
			$ary['30'] = array('United States', '美国');
			$ary['31'] = array('Taiwan', '台湾');
			$ary['-9'] = array('Others', '其他');
			
			return $ary;
		}
		
		function MAL_RACE_DATA_ARY()
		{
			#####################################
			# declare race data
			#####################################
			$ary = array();
			$ary['1'] = array('Chinese', '华族');
			$ary['2'] = array('Malay', '马来族');
			$ary['3'] = array('Indian', '印族');
			$ary['4'] = array('Iban', '伊班族');
			$ary['5'] = array('Kadazan', '卡达山族');
			$ary['-9'] = array('Others', '其他');
			
			return $ary;
		}
		
		function MAL_RELIGION_DATA_ARY()
		{
			#####################################
			# declare religion data
			#####################################
			$ary = array();
			$ary['1'] = array('None', '无');
			$ary['2'] = array('Buddhism', '佛教');
			$ary['3'] = array('Christian', '基督教');
			$ary['4'] = array('Catholic', '天主教');
			$ary['5'] = array('Islam', '回教');
			$ary['6'] = array('Taoism', '道教');
			$ary['-9'] = array('Others', '其他');
			
			
			return $ary;
		}
		
		function MAL_FAMILY_LANG_ARY()
		{
			#####################################
			# declare nation data
			#####################################
			$ary = array();
			$ary['1'] = array('Dialect', '方言');
			$ary['2'] = array('Chinese', '华语');
			$ary['3'] = array('English', '英文');
			$ary['4'] = array('Mandarin', '国语');
			$ary['-9'] = array('Others', '其他');
			
			return $ary;
		}
		
		function MAL_PAYMENT_TYPE_ARY()
		{
			#####################################
			# declare nation data
			#####################################
			$ary = array();
			$ary['1'] = array('PayType1 [En]', 'PayType1 [Chi]');
			$ary['2'] = array('PayType2 [En]', 'PayType2 [Chi]');
			$ary['3'] = array('PayType3 [En]', 'PayType3 [Chi]');
			$ary['4'] = array('PayType4 [En]', 'PayType4 [Chi]');
			
			return $ary;
		}
		
		function MAL_LODGING_ARY()
		{
			#####################################
			# declare nation data
			#####################################
			$ary = array();
			$ary['1'] = array('Stay', '住宿');
			$ary['2'] = array('Live outside', '外宿');
			$ary['3'] = array('Live with parents', '与父母同住');
			$ary['4'] = array('Live with father', '与父亲同住');
			$ary['5'] = array('Live with mother', '与母亲同住');
			$ary['6'] = array('Live with guardian', '与监护人同住');
			$ary['-9'] = array('Others', '其他');
			
			return $ary;
		}		
		
		function MAL_JOB_NATURE_DATA_ARY()
		{
			#####################################
			# declare job nature data
			#####################################
			$ary = array();
			$ary['1'] = array('Professional', '专业人士');
			$ary['2'] = array('Cultural and Educational', '文教机构');
			$ary['3'] = array('Business', '商业');
			$ary['4'] = array('Agriculture', '农业');
			$ary['5'] = array('Blue Collar', '工');
			$ary['6'] = array('No Job', '无');
			$ary['7'] = array('Fishery', '渔业');
			$ary['-9'] = array('Others', '其他');
			
			return $ary;
		}
		
		function MAL_JOB_TITLE_DATA_ARY()
		{
			#####################################
			# declare job title data
			#####################################
			$ary = array();
			$ary['1'] = array('Accountant', '会计师');
			$ary['2'] = array('Agent', '代理员');
			$ary['3'] = array('Assistant', '助理');
			$ary['4'] = array('Babysitter', '褓母');
			$ary['5'] = array('Beautician', '美容师');
			$ary['6'] = array('Blacksmith', '铁匠');
			$ary['7'] = array('Business Owner', '老板');
			$ary['8'] = array('Carpenter', '木匠');
			$ary['9'] = array('Chef', '厨师');
			$ary['10'] = array('Clerk', '文员');
			$ary['11'] = array('Contractor', '承包商');
			$ary['12'] = array('Deliveryman', '送货员');
			$ary['13'] = array('Director', '董事');
			$ary['14'] = array('Doctor', '医生');
			$ary['15'] = array('Driver', '司机');
			$ary['16'] = array('Engineer', '工程师');
			$ary['17'] = array('Executive', '执行人员');
			$ary['18'] = array('Factory Manager', '厂长');
			$ary['19'] = array('Factory Worker', '工友');
			$ary['20'] = array('Foreman', '管工');
			$ary['21'] = array('Hair Stylist', '美发师');
			$ary['22'] = array('Hawker', '小贩');
			$ary['23'] = array('Lawyer', '律师');
			$ary['24'] = array('Manager', '经理');
			$ary['25'] = array('Nurse', '护士');
			$ary['26'] = array('Painter', '画家');
			$ary['27'] = array('Pastor', '牧师');
			$ary['28'] = array('Police Officer', '警官');
			$ary['29'] = array('President/CEO', '总裁');
			$ary['30'] = array('Principal', '校长');
			$ary['31'] = array('Professor', '教授');
			$ary['32'] = array('Salesman', '销售员');
			$ary['33'] = array('Stock Broker', '股票经纪');
			$ary['34'] = array('Supervisor', '主管');
			$ary['35'] = array('Tailor', '裁缝师');
			$ary['36'] = array('Teacher/Lecturer', '教师/讲师');
			$ary['37'] = array('Technical Personnel', '技术人员');
			$ary['38'] = array('Traditional Chinese Medicine Physician', '中医师');
			$ary['39'] = array('Waiter', '侍应生');
			$ary['40'] = array('No Title', '无');
			$ary['-9'] = array('Others', '其他');
						
			return $ary;			
		}
		
		function MAL_EDU_LEVEL_DATA_ARY()
		{
			#####################################
			# declare education level data
			#####################################
			$ary = array();
			$ary['1'] = array('Primary School', '小学');
			$ary['2'] = array('Secondary School', '中学');
			$ary['3'] = array('College', '大专');
			$ary['4'] = array('Master', '硕士');
			$ary['5'] = array('Doctor', '博士');
			$ary['-9'] = array('Others', '其他');
			
			return $ary;			
		}
		
		function MAL_MARITAL_STATUS_DATA_ARY()
		{
			#####################################
			# declare marital status data
			#####################################
			$ary = array();
			$ary['1'] = array('Good', '良好');
			$ary['2'] = array('Divorced', '离异');
			
			return $ary;
		}
		
		function MAL_GUARD_DATA_ARY()
		{
			#####################################
			# declare guardian Type data
			#####################################
			$ary = array();
			$ary['F'] = array('Father', '父亲');
			$ary['M'] = array('Mother', '母亲');
			$ary['O'] = array('Others', '其他');
			
			return $ary;
		}
		
		function MAL_LIVE_SAME_ARY()
		{
			#####################################
			# declare stay together data
			#####################################
			$ary = array();
			$ary['1'] = array('Yes', 'Yes');
			$ary['0'] = array('No', 'No');
			
			return $ary;
		}
		
		function getRegistryStatusByStatusID($status) 
		{
			global $Lang;
			
			switch($status) {
				case STUDENT_REGISTRY_RECORDSTATUS_NORMAL : return $Lang['StudentRegistry']['RegistryStatus_Approved']; break;
				case STUDENT_REGISTRY_RECORDSTATUS_LEFT : return $Lang['StudentRegistry']['RegistryStatus_Left']; break;
				case STUDENT_REGISTRY_RECORDSTATUS_SUSPEND : return $Lang['StudentRegistry']['RegistryStatus_Suspend']; break;
				case STUDENT_REGISTRY_RECORDSTATUS_RESUME : return $Lang['StudentRegistry']['RegistryStatus_Resume']; break;
				default : break;
			}	
		} 
		
		function getRegistryStatusHistory($userid)
		{
			global $Lang;
			$sql = "SELECT 
						CASE(RecordStatus)
						WHEN 1 THEN '".$Lang['StudentRegistry']['StatusLeft']."'
						WHEN 2 THEN '".$Lang['StudentRegistry']['StatusSuspended']."'
						WHEN 3 THEN '".$Lang['StudentRegistry']['StatusResume']."'
						ELSE '' END as status, 
						ApplyDate as applyDate, 
						IF(Reason='0', ReasonOthers,
						CASE(Reason)
							WHEN 1 THEN '".$Lang['StudentRegistry']['ReasonAry'][1]."'
							WHEN 2 THEN '".$Lang['StudentRegistry']['ReasonAry'][2]."' 
							WHEN 3 THEN '".$Lang['StudentRegistry']['ReasonAry'][3]."'
							WHEN 4 THEN '".$Lang['StudentRegistry']['ReasonAry'][4]."'
							WHEN 5 THEN '".$Lang['StudentRegistry']['ReasonAry'][5]."'
							WHEN 6 THEN '".$Lang['StudentRegistry']['ReasonAry'][6]."'
							WHEN 7 THEN '".$Lang['StudentRegistry']['ReasonAry'][7]."'
							WHEN 8 THEN '".$Lang['StudentRegistry']['ReasonAry'][8]."'
							WHEN 9 THEN '".$Lang['StudentRegistry']['ReasonAry'][9]."'
							WHEN 10 THEN '".$Lang['StudentRegistry']['ReasonAry'][10]."'
							ELSE '---' END
						) as reason, 
						NoticeID
					FROM 
						STUDENT_REGISTRY_STATUS 
					WHERE 
						UserID='$userid' 
					ORDER BY ApplyDate, RecordID";
			return $this->returnArray($sql, 4);
			
		}
		
		function insertStudentRegistryStatus($dataAry=array(), $studentid)
		{
			global $UserID;
			
			# archive existing status (set "IsCurrent" to 0)
			$sql = "UPDATE STUDENT_REGISTRY_STATUS SET IsCurrent=0 WHERE UserID='$studentid'";
			$this->db_db_query($sql);
			
			# insert new entry
			foreach($dataAry as $fields[]=>$values[])
			$sql = "INSERT INTO STUDENT_REGISTRY_STATUS (UserID, ";
			foreach($fields as $field) $sql .= "$field, ";
			$sql .= "DateInput, InputBy, DateModified, ModifyBy) VALUES ('$studentid', ";
			foreach($values as $value) $sql .= "'$value', ";
			$sql .= "NOW(), '$UserID', NOW(), '$UserID')";
			
			return $this->db_db_query($sql);
		}
		
		function retrieveTemplateDetails($TemplateID="")
		{
			if($TemplateID)
			{
				$sql = "select Module,CategoryID,Title,Subject,Content,ReplySlip,RecordType,RecordStatus,ReplySlipContent
					from INTRANET_NOTICE_MODULE_TEMPLATE where TemplateID = '$TemplateID'";
				return $this->returnArray($sql);
			}
		}
		
		function TemplateCategory()
		{
			global $Lang;

			$data = array();
			# array: [CategoryID][CategoryName]
			$data[] = array(STUDENT_REGISTRY_RECORDSTATUS_LEFT,$Lang['StudentRegistry']['RegistryStatus_Left']);
			$data[] = array(STUDENT_REGISTRY_RECORDSTATUS_SUSPEND,$Lang['StudentRegistry']['RegistryStatus_Suspend']);
			$data[] = array(STUDENT_REGISTRY_RECORDSTATUS_RESUME,$Lang['StudentRegistry']['RegistryStatus_Resume']);
			return $data;
		}	
		
		function TemplateInfoByCategoryID($CategoryID="")
		{
			if($CategoryID!="") {
				$sql = "SELECT TemplateID, Title FROM INTRANET_NOTICE_MODULE_TEMPLATE WHERE Module='".$this->Module."' AND CategoryID='$CategoryID' AND RecordStatus=1";
				return $this->returnArray($sql, 2);
			}
		}
		
		function TemplateVariable($CategoryID='')
		{
			global $i_Discipline_System_Template_Variable,$i_UserStudentName, $i_ClassName, $i_ClassNumber, $i_Notice_DateStart;
			global $intranet_root,$intranet_session_language, $Lang;
			include("$intranet_root/lang/discipline_templateeditor.lang.$intranet_session_language.php");

			# set default value
			$DefaultValue = $i_Discipline_System_Template_Variable['default_value'];
			$data = array();

			# [variable symbol][variable name][default value]
			# Common variables
			$data['SchoolYear'] = array($i_Discipline_System_Template_Variable['school_year'], $DefaultValue);
			$data['Semester'] = array($i_Discipline_System_Template_Variable['semester'], $DefaultValue);
			$data['StudentName'] = array($i_Discipline_System_Template_Variable['student_name'], $DefaultValue);
			$data['StudentNumber'] = array($Lang['StudentRegistry']['StudentNo'], $DefaultValue);
			$data['ClassName'] = array($i_Discipline_System_Template_Variable['class_name'], $DefaultValue);
			$data['ClassNumber'] = array($Lang['StudentRegistry']['ClassNumber'], $DefaultValue);
			$data['ContactPersonName'] = array($Lang['StudentRegistry']['eNotice_ContactPersonName'], $DefaultValue);
			$data['ContactPersonAddress'] = array($Lang['StudentRegistry']['eNotice_ContactPersonAddress'], $DefaultValue);
			$data['AdditionalInfo'] = array($i_Discipline_System_Template_Variable['additional_info'], $DefaultValue);
			$data['IssueDate'] = array($i_Discipline_System_Template_Variable['issue_date'], date("Y-m-d"));

			# Category variables
			switch($CategoryID)
			{
				case STUDENT_REGISTRY_RECORDSTATUS_LEFT:
					$data['ReasonOfLeft'] = array($Lang['StudentRegistry']['eNotice_ReasonOfLeft'], $DefaultValue);
					$data['DateOfLeft'] = array($Lang['StudentRegistry']['eNotice_DateOfLeft'], $DefaultValue);
					break;
				case STUDENT_REGISTRY_RECORDSTATUS_SUSPEND:
					$data['ReasonOfSuspend'] = array($Lang['StudentRegistry']['eNotice_ReasonOfSuspend'], $DefaultValue);
					$data['DateOfSuspend'] = array($Lang['StudentRegistry']['eNotice_DateOfSuspend'], $DefaultValue);
					break;
				case STUDENT_REGISTRY_RECORDSTATUS_RESUME:
					$data['DateOfResume'] = array($Lang['StudentRegistry']['eNotice_DateOfResume'], $DefaultValue);
					break;
				default:
					break;
			}

			return $data;
		}
		
		function RETRIEVE_NOTICE_TEMPLATE_INFO($templateID='')
		{
			$conds = $templateID ? "TemplateID='$templateID'" : "";
			
			$sql = "select * from INTRANET_NOTICE_MODULE_TEMPLATE where " . $conds;
			$result = $this->returnArray($sql);
			return $result;
		}

		function getRegistryStudentInfoByStudentID($StudentID)
		{
			$sql = "SELECT * FROM STUDENT_REGISTRY_STUDENT WHERE UserID='$StudentID'";
			$result = $this->returnArray($sql);
			return $result[0];
		}

		function TEMPLATE_VARIABLE_CONVERSION($StudentID="", $CategoryID="", $AdditionalInfo="", $reason="", $applyDate="")
		{
			global $PATH_WRT_ROOT, $intranet_session_language, $Lang;
			global $i_RecordDate, $i_Discipline_Reason2, $eDiscipline, $i_no_record_exists_msg;

			if($StudentID)
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				$lu = new libuser($StudentID);

				$templateVar = $this->TemplateVariable($CategoryID);
				$cur_school_year = getCurrentAcademicYear();
				$cur_semester = getCurrentSemester();
				$studentRegistryInfo = $this->getRegistryStudentInfoByStudentID($StudentID);
				$contactPerson = $this->getParentGuardianInfo($StudentID, "G");

				$x = array();
				foreach($templateVar as $key=>$data)
				{
					switch($key)
					{
						# Common variables
						case "StudentName":
							$x['StudentName'] = $lu->UserName();
							break;
						case "ClassName":
							$x['ClassName'] = $lu->ClassName;
							break;
						case "ClassNumber":
							$x['ClassNumber'] = $lu->ClassNumber;
							break;
						case "IssueDate":
							$x['IssueDate'] = $data[1];
							break;
						case "AdditionalInfo":
							$x['AdditionalInfo'] = $AdditionalInfo ? intranet_htmlspecialchars($AdditionalInfo) : $data[1];
							break;
						case "SchoolYear":
							$x['SchoolYear'] = $cur_school_year;
							break;
						case "Semester":
							$x['Semester'] = $cur_semester;
							break;
						case "StudentNumber":
							$x['StudentNumber'] = $studentRegistryInfo['STUD_ID'];
							break;
						case "ContactPersonName":
							$x['ContactPersonName'] = Get_Lang_Selection($contactPerson['NAME_C'],$contactPerson['NAME_E']);
							break;
						case "ContactPersonAddress":
							$x['ContactPersonAddress'] = $contactPerson['POSTAL_ADDR1'].", ".$contactPerson['POSTAL_ADDR2'].", ".$contactPerson['POSTAL_ADDR3'].", ".$contactPerson['POSTAL_ADDR4'].", ".$contactPerson['POSTAL_ADDR5'].", ".$contactPerson['POSTAL_ADDR6'];
							break;
						case "ReasonOfLeft":
							$x['ReasonOfLeft'] = ($reason!="") ? $reason : $data[1];
							break;
						case "DateOfLeft":
							$x['DateOfLeft'] = ($applyDate!="") ? $applyDate : $data[1];
							break;
						case "ReasonOfSuspend":
							$x['ReasonOfSuspend'] = ($reason!="") ? $reason : $data[1];
							break;
						case "DateOfSuspend":
							$x['DateOfSuspend'] = ($applyDate!="") ? $applyDate : $data[1];
							break;
						case "DateOfResume":
							$x['DateOfResume'] = ($applyDate!="") ? $applyDate : $data[1];
							break;
					}
				}
				return $x;
			}
		}

		function getUserNameByID($UserID)
		{
			$name_field = getNameFieldByLang();
			$sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '$UserID'";
			return $this->returnVector($sql);
		}
		
		function getNoticeAry()
		{
			$sql = "SELECT UserID, NoticeID FROM STUDENT_REGISTRY_STATUS WHERE NoticeID is not null and IsCurrent=1";
			return $this->returnArray($sql);	
		}
						
		function getRegistryStatusReport($AcademicYearID="", $Type="", $list="", $statistics="", $print=0, $dateSelect="", $dateAry=array()) 
		{		
			$x = "";
			
			if($list==1)
				$x .= $this->getRegistryStatusStudentList($AcademicYearID, $Type, $print, $dateSelect, $dateAry); 
			
			if($statistics==1) {
				if($list==1 && $print==1)
					$x .= "<p style='page-break-after:always'>";
				else 
					$x .= "<br>";
							
				$x .= $this->getRegistryStatusStudentStatistics($AcademicYearID, $Type, $print, $dateSelect, $dateAry);
				
				if($Type!=STUDENT_REGISTRY_RECORDSTATUS_RESUME) {
					if($print==1)
						$x .= "<p style='page-break-after:always'>";
					else 
						$x .= "<br>";
						
					$x .= $this->getReasonTable($AcademicYearID, $Type, $print, $dateSelect, $dateAry);
				}
			}			
				 
			return $x;
		}
		
		function getRegistryStatusStudentList($AcademicYearID, $Type, $print=0, $dateSelect="", $dateAry=array())
		{
			global $Lang, $i_UserChineseName, $i_UserClassName, $i_Attendance_Date, $i_no_record_exists_msg, $intranet_session_language, $i_general_print_date;
			
			$conds = "";
			
			if($dateSelect=="DATE") {
				$yearInfo = getAcademicYearInfoAndTermInfoByDate($dateAry['startDate']);
				$AcademicYearID = $yearInfo[0];
				$conds .= " AND st.ApplyDate BETWEEN '".$dateAry['startDate']."' AND '".$dateAry['endDate']."'";
			}
			
			$sql = "SELECT ycu.UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE yc.AcademicYearID='$AcademicYearID'";
			$temp = $this->returnVector($sql);
			$studentAry = $temp;
			if(sizeof($studentAry)>0)
				$conds .= " AND st.UserID IN (".implode(',', $studentAry).")";
				
			switch($Type) {
				case STUDENT_REGISTRY_RECORDSTATUS_LEFT : $listText = $Lang['StudentRegistry']['ListOfLeft']; $reasonText = $Lang['StudentRegistry']['eNotice_ReasonOfLeft']; break;
				case STUDENT_REGISTRY_RECORDSTATUS_SUSPEND : $listText = $Lang['StudentRegistry']['ListOfSuspend']; $reasonText = $Lang['StudentRegistry']['eNotice_ReasonOfSuspend']; break;
				case STUDENT_REGISTRY_RECORDSTATUS_RESUME : $listText = $Lang['StudentRegistry']['ListOfResume']; $reasonText = $Lang['StudentRegistry']['eNotice_ReasonOfResume']; break;
				default : $Type = STUDENT_REGISTRY_RECORDSTATUS_LEFT; $reasonText = $Lang['StudentRegistry']['ListOfLeft']; $reasonText = $Lang['StudentRegistry']['eNotice_ReasonOfLeft']; break;
			}
			
			# display student list even apply date not in current academic year
			$sql = "SELECT 
						USR.ChineseName,
						IFNULL(srs.STUD_ID, '---') as stud_id,
						USR.ClassName,
						st.ApplyDate,
						IF(st.Reason=0, st.ReasonOthers, st.Reason) as reason
					FROM
						INTRANET_USER USR LEFT OUTER JOIN
						STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT OUTER JOIN
						STUDENT_REGISTRY_STATUS st ON (st.UserID=USR.UserID) 
					WHERE 
						st.RecordStatus='$Type' 
						$conds				 
			";
			$result = $this->returnArray($sql,5);
			
			if($print==0) {
				$css1 = "common_table_list_v30";
				$css2 = "";
				
				$title = '<span class="sectiontitle_v30"> '.$listText.'</span>';
			} else {
				$css1 = "tableborder_print";
				$css2 = "tabletop_print";
				$css3 = "row_print";
				$css4 = "row_print";
				
				$title = "<div align='center'><h4>".GET_SCHOOL_NAME()."<br>";
				$title .= ($intranet_session_language=="en") ? $listText.$Lang['StudentRegistry']['Of'].$this->getAcademicYearNameByYearID($AcademicYearID) : $this->getAcademicYearNameByYearID($AcademicYearID).$Lang['General']['SchoolYear'].$reasonText;
				$title .= "</h4></div><br>".$i_general_print_date." : ".date('Y-m-d');
			}
	
			$table_th_td	  = $print? "td" : "th";
			$table_class 	  = $print? "class=\"tableborder_print\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" valign=\"top\"" : "class=\"common_table_list_v30\"";
	
		
			$x .= '<div class="table_board">
				'.$title.'<br />
				<table '.$table_class.'>
				<thead>
				<tr class="'.$css2.'">
				<'.$table_th_td.' class="num_check">#</'.$table_th_td.'>
				<'.$table_th_td.'> '.$i_UserChineseName.'</'.$table_th_td.'>
				<'.$table_th_td.'>'.$Lang['StudentRegistry']['StudentNo'].'</'.$table_th_td.'>
				<'.$table_th_td.'>'.$i_UserClassName.'</'.$table_th_td.'>
				<'.$table_th_td.'>'.$i_Attendance_Date.'</'.$table_th_td.'>';
			if($Type!=STUDENT_REGISTRY_RECORDSTATUS_RESUME)	
				$x .= '<'.$table_th_td.'>'.$reasonText.'</'.$table_th_td.'>';
			$x .= '</tr>';
		
			for($i=0; $i<sizeof($result); $i++) {
				list($chinesename, $stud_id, $classname, $applydate, $reason) = $result[$i];
				
				$x .= '<tr class="'.$css3.'">
					<td class="'.$css4.'">'.($i+1).'</td>
					<td class="'.$css4.'">'.$chinesename.'</td>
					<td class="'.$css4.'">'.$stud_id.'</td>
					<td class="'.$css4.'">'.$classname.'<br /></td>
					<td class="'.$css4.'">'.$applydate.'</td>';
				if($Type!=STUDENT_REGISTRY_RECORDSTATUS_RESUME)	
					$x .= '<td class="'.$css4.'">'.(is_numeric($reason) ? $Lang['StudentRegistry']['ReasonAry'][$reason] : $reason).'</td>';
				$x .= '</tr>
					';	
			}
			if(sizeof($result)==0) {
				$colspan = ($Type!=STUDENT_REGISTRY_RECORDSTATUS_RESUME) ? 6 : 5;
				$x .= '<tr class="'.$css3.'"><td colspan="'.$colspan.'" align="center" height="40" valign="absmiddle" class="'.$css4.'">'.$i_no_record_exists_msg.'</td></tr>';
			}
			$x .= '</thead>
					<tbody>
					</tbody>
					</table>
					</div><br>
					';
				
				return $x;
		}
		
		function getRegistryStatusStudentStatistics($AcademicYearID, $Type, $print=0, $dateSelect="", $dateAry=array()) 
		{
			global $Lang, $i_no_record_exists_msg, $intranet_session_language, $i_general_print_date;
			
			$yearName = $this->getAcademicYearNameByYearID($AcademicYearID);
			
			$conds = "";
			
			if($dateSelect=="DATE") {
				$yearInfo = getAcademicYearInfoAndTermInfoByDate($dateAry['startDate']);
				$AcademicYearID = $yearInfo[0];
				$conds .= " AND st.ApplyDate BETWEEN '".$dateAry['startDate']."' AND '".$dateAry['endDate']."'";
			}
			
			$sql = "SELECT ycu.UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE yc.AcademicYearID='$AcademicYearID'";
			$temp = $this->returnVector($sql);
			$studentAry = $temp;
			$totalStudent = sizeof($studentAry);
			$currentStudentAmt = $totalStudent;
			$withRecord = 0;
			
			$sql = "SELECT YearID, YearName FROM YEAR ORDER BY Sequence";
			$yearInfo = $this->returnArray($sql,2);
			
			$yearStartDate = getStartDateOfAcademicYear($AcademicYearID);
			
			switch($Type) {
				case 1 : $percentageText = $Lang['StudentRegistry']['PercentageOfLeft']; $reasonText = $Lang['StudentRegistry']['StatisticsOfLeftReason']; break;
				case 2 : $percentageText = $Lang['StudentRegistry']['PercentageOfSuspend']; $reasonText = $Lang['StudentRegistry']['StatisticsOfSuspendReason']; break; 
				case 3 : $percentageText = $Lang['StudentRegistry']['PercentageOfResume']; break;
			}
			
			if(sizeof($studentAry)>0)
				$conds .= " AND st.UserID IN (".implode(',', $studentAry).")";
			
			$sql = "SELECT 
						y.YearID, y.YearName, st.Reason, st.ReasonOthers, COUNT(st.RecordID) 
					FROM 
						STUDENT_REGISTRY_STATUS st LEFT OUTER JOIN
		 				YEAR_CLASS_USER ycu ON (ycu.UserID=st.UserID) LEFT OUTER JOIN
						YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
						YEAR y ON (y.YearID=yc.YearID)
					WHERE 
						st.RecordStatus='$Type' AND 
						st.ApplyDate<'$yearStartDate' AND 
						yc.AcademicYearID='$AcademicYearID'
						$conds 
					GROUP BY 
						yc.YearID, st.Reason, st.ReasonOthers";
			$row = $this->returnArray($sql);
			//echo $sql;
			//debug_pr($row);
			if($print==0) {
				$table_row = "total_row";
				$table_col = "total_col";
				$css1 = "common_table_list_v30";
				$css2 = "sub_row_top total_row";
				
				$title = '<span class="sectiontitle_v30"> '.$percentageText.'</span>';
			} else {
				$css1 = "tableborder_print";
				$css2 = "tabletop_print tabletext";
				$css3 = "row_print";
				$css4 = "table_text row_print";
				
				$title = "<div align='center'><h4>".GET_SCHOOL_NAME()."<br>";
				$title .= ($intranet_session_language=="en") ? $percentageText.$Lang['StudentRegistry']['Of'].$this->getAcademicYearNameByYearID($AcademicYearID) : $this->getAcademicYearNameByYearID($AcademicYearID).$percentageText;
				$title .= "</h4></div><br>".$i_general_print_date." : ".date('Y-m-d');
			}

			$table_th_td	  = $print? "td" : "th";
			$table_class 	  = $print? "class=\"tableborder_print\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" valign=\"top\"" : "class=\"common_table_list_v30\"";
			
			$x .= '<div class="table_board">'.$title.'
					<table '.$table_class.'>
					<thead>';
					
			if(sizeof($row)>0) {		# only display when has data
				$ResultAry = array();
				$rowAmount = array();
				$colAmount = array();
				$rowPercentage = array();
				$colPercentage = array();
				
				for($i=0; $i<sizeof($row); $i++) {
					list($yearid, $yearname, $reason, $reasonOthers, $s_count) = $row[$i];
					$ResultAry[$yearid][$reason] = $s_count;
				}
				
				# months before the academic year start
				$x .= '
					
					<tr class="'.$css2.'">
					
					<'.$table_th_td.' colspan="'.(sizeof($yearInfo)*2+1+2).'" class="'.$css2.'"><div align="center">'.$Lang['StudentRegistry']['NotYetAttended'].'</div></'.$table_th_td.'>
					</tr>
					<tr class="'.$css2.'">
					<'.$table_th_td.' rowspan="2">'.$Lang['StaffAttendance']['Reason'].'</'.$table_th_td.'>';
					
				for($i=0; $i<sizeof($yearInfo); $i++)
					$x .= '<'.$table_th_td.' colspan="2" class="'.$table_row.'">'.$yearInfo[$i][1].'</'.$table_th_td.'>';
					
				$x .= '<'.$table_th_td.' colspan="2" class="'.$table_row.'">'.$Lang['StudentRegistry']['SubTotal'].'</'.$table_th_td.'>';	
				$x .= '</tr><tr class="'.$css2.'">';
				
				for($i=0; $i<sizeof($yearInfo); $i++) 
					$x .= '<'.$table_th_td.' class="'.$table_col.'">'.$Lang['StudentRegistry']['Amount'].'</'.$table_th_td.'><'.$table_th_td.'>%</'.$table_th_td.'>';
					
				$x .= '<'.$table_th_td.' class="'.$table_col.'">'.$Lang['StudentRegistry']['Amount'].'</'.$table_th_td.'>
					<'.$table_th_td.'>%</'.$table_th_td.'>';	
				$x .= '</tr></thead><tbody>';
				
				foreach($Lang['StudentRegistry']['ReasonAry'] as $key=>$val) {	
					$x .= '
						<tr class="'.$css3.'">
						<td class="'.$css4.'">'.$val.'</td>';
					for($j=0; $j<sizeof($yearInfo); $j++) {
						$percentage = round(($ResultAry[$yearInfo[$j][0]][$key]/$currentStudentAmt)*100,2);
						if($percentage==0) $percentage = "&nbsp;";
						$x .= '
							<td class="'.$table_col.'">'.(isset($ResultAry[$yearInfo[$j][0]][$key])?$ResultAry[$yearInfo[$j][0]][$key]:"&nbsp;").'</td>
							<td class="'.$css3.'">'.$percentage.'</td>
						';
						$rowAmount[$key] += $ResultAry[$yearInfo[$j][0]][$key];
						$colAmount[$yearInfo[$j][0]] += $ResultAry[$yearInfo[$j][0]][$key];
						$rowPercentage[$key] += $percentage;
						$colPercentage[$yearInfo[$j][0]] += $percentage;
					}
					$x .= '<td class="'.$table_col.'">'.($rowAmount[$key]!=0?$rowAmount[$key]:"&nbsp;").'</th>
					<td>'.($rowPercentage[$key]!=0?$rowPercentage[$key]:"&nbsp;").'</th>';
					$x .= '</tr>';
				}
				$x .= '
					<tr  class="'.$table_row.' '.$css3.'">
					<td class="'.$css4.'">'.$Lang['StudentRegistry']['Total'].'</td>';
			
				for($i=0; $i<sizeof($yearInfo); $i++) 
					$x .= '<td class="'.$table_col.' '.$css4.'">'.($colAmount[$yearInfo[$i][0]]!=0?$colAmount[$yearInfo[$i][0]]:"&nbsp;").'</td><td>'.(($colPercentage[$yearInfo[$i][0]]!=0)?$colPercentage[$yearInfo[$i][0]]:"&nbsp;").'</td>';

				$x .= '
					<td class="'.$table_col.' '.$css4.'">'.array_sum($colAmount).'</td>
					<td class="'.$css4.'">'.array_sum($colPercentage).'</td>
					</tr>

					<tr  class="'.$css3.'">
					<td class="'.$css4.'">'.$Lang['StudentRegistry']['TransferredStudents'].'</td>';
				for($i=0; $i<sizeof($yearInfo); $i++) 
					$x .= '<td class="'.$table_col.'" colspan="2">&nbsp;</td>';

					
				$newStudentAmt = $currentStudentAmt - array_sum($colAmount);
				$x .= '	
					<td class="'.$table_col.' '.$css4.'">&nbsp;</td>
					<td class="'.$css4.'">&nbsp;</td>
					</tr>
					<tr class="'.$css3.'">
					<td class="'.$css4.'">'.$Lang['StudentRegistry']['TotalStudents'].'</td>
					<td colspan="'.(sizeof($yearInfo)*2+2).'" class="'.$table_col.'"><div align="center">'.$currentStudentAmt.'-'.array_sum($colAmount).'='.$newStudentAmt.'<br />
					</div></td>
					</tr>';		
				
				# store current amount of student after calculation
				$currentStudentAmt = $newStudentAmt;
				$withRecord = 1;
			}
			
			# other months within the academic year
			$sql = "SELECT 
						y.YearID, y.YearName, st.Reason, st.ReasonOthers, COUNT(st.RecordID), CONCAT(YEAR(st.ApplyDate),'-',IF(MONTH(st.ApplyDate)<10,CONCAT('0',MONTH(st.ApplyDate)),MONTH(st.ApplyDate))) as applyDate 
					FROM 
						STUDENT_REGISTRY_STATUS st LEFT OUTER JOIN
		 				YEAR_CLASS_USER ycu ON (ycu.UserID=st.UserID) LEFT OUTER JOIN
						YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
						YEAR y ON (y.YearID=yc.YearID)
					WHERE 
						st.RecordStatus='$Type' AND 
						st.ApplyDate>='$yearStartDate' AND 
						yc.AcademicYearID='$AcademicYearID'
						$conds 
					GROUP BY 
						applyDate, yc.YearID, st.Reason, st.ReasonOthers";
			$row = $this->returnArray($sql);
		
			$ResultAry = array();
			
			
			
			for($i=0; $i<sizeof($row); $i++) {
				list($yearid, $yearname, $reason, $reasonOthers, $s_count, $applyDate) = $row[$i];
				$ResultAry[$applyDate][$yearid][$reason] = $s_count;
			}
					
			foreach($ResultAry as $month=>$data[]) {
				$tranStudentTotal = 0;
				$tranStudent = array();
				# count transferred students
				$sql = "SELECT COUNT(distinct srs.UserID), yc.YearID FROM STUDENT_REGISTRY_STUDENT srs LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=srs.UserID) LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE yc.AcademicYearID='".Get_Current_Academic_Year_ID()."' AND srs.ADMISSION_DATE LIKE '$month%' GROUP BY srs.UserID";
				$temp = $this->returnArray($sql);

				for($p=0; $p<sizeof($temp); $p++) {
					list($sc, $y) = $temp[$p];
					$tranStudent[$y] += $sc;
				}
				
				$currentMonth = substr($month,-2);
				if(substr($currentMonth,0,1)=="0")
					$currentMonth = substr($currentMonth,-1);
		
				$x .= '
					<tr class="'.$css2.'">
					<'.$table_th_td.' colspan="'.(sizeof($yearInfo)*2+1+2).'" class="'.$css2.'"><div align="center">'.(($intranet_session_language=="en") ? $Lang['General']['month'][$currentMonth].", ".$yearName : $yearName.", ".$Lang['General']['month'][$currentMonth]).'</div></'.$table_th_td.'>
					</tr>
					<tr class="'.$css2.'">
					<'.$table_th_td.' rowspan="2">'.$Lang['StaffAttendance']['Reason'].'';
				
				for($i=0; $i<sizeof($yearInfo); $i++)
					$x .= '<'.$table_th_td.' colspan="2" class="'.$table_row.'">'.$yearInfo[$i][1].'</'.$table_th_td.'>';
					
				$x .= '<'.$table_th_td.' colspan="2" class="'.$table_row.'">'.$Lang['StudentRegistry']['SubTotal'].'</'.$table_th_td.'>';	
				$x .= '</tr>
						<tr class="'.$css2.'">';
				
				for($i=0; $i<sizeof($yearInfo); $i++) 
					$x .= '<'.$table_th_td.' class="'.$table_col.'">'.$Lang['StudentRegistry']['Amount'].'</'.$table_th_td.'><'.$table_th_td.'>%</'.$table_th_td.'>';
					
				$x .= '<'.$table_th_td.' class="'.$table_col.'">'.$Lang['StudentRegistry']['Amount'].'</'.$table_th_td.'>
					<'.$table_th_td.'>%</'.$table_th_td.'>';	
				$x .= '</tr>';
				
				# reason
				foreach($Lang['StudentRegistry']['ReasonAry'] as $key=>$val) {
					$x .= '
						<tr class="'.$css3.'">
						<td class="'.$css4.'">'.$val.'</td>';
					for($j=0; $j<sizeof($yearInfo); $j++) {
						$percentage = round(($ResultAry[$month][$yearInfo[$j][0]][$key]/$currentStudentAmt)*100,2);
						if($percentage==0) $percentage = "&nbsp;";
						$x .= '
							<td class="'.$table_col.'">'.(isset($ResultAry[$month][$yearInfo[$j][0]][$key])?$ResultAry[$month][$yearInfo[$j][0]][$key]:"&nbsp;").'</td>
							<td>'.$percentage.'</td>
						';
						$rowAmount[$month][$key] += $ResultAry[$month][$yearInfo[$j][0]][$key];
						$colAmount[$month][$yearInfo[$j][0]] += $ResultAry[$month][$yearInfo[$j][0]][$key];
						$rowPercentage[$month][$key] += $percentage;
						$colPercentage[$month][$yearInfo[$j][0]] += $percentage;
					}
					$x .= '<td class="'.$table_col.'">'.($rowAmount[$month][$key]!=0?$rowAmount[$month][$key]:"&nbsp;").'</th>
					<td>'.($rowPercentage[$month][$key]!=0?$rowPercentage[$month][$key]:"&nbsp;").'</th>';
					$x .= '</tr>';
				}
			
				$x .= '<tr  class="'.$table_row.' '.$css4.'"><td>'.$Lang['StudentRegistry']['Total'].'</td>';
			
				for($i=0; $i<sizeof($yearInfo); $i++) 
					$x .= '<td class="'.$table_col.'">'.($colAmount[$month][$yearInfo[$i][0]]!=0?$colAmount[$month][$yearInfo[$i][0]]:"&nbsp;").'</td><td>'.(($colPercentage[$month][$yearInfo[$i][0]]!=0)?$colPercentage[$month][$yearInfo[$i][0]]:"&nbsp;").'</td>';

				$x .= '
					<td class="'.$table_col.'">'.array_sum($colAmount[$month]).'</td>
					<td>'.array_sum($colPercentage[$month]).'</td>
					</tr>

					<tr  class="'.$css4.'"><td>'.$Lang['StudentRegistry']['TransferredStudents'].'</td>';
			
				for($i=0; $i<sizeof($yearInfo); $i++) {
					$x .= '<td class="'.$table_col.'" colspan="2" align="center">'.$tranStudent[$yearInfo[$i][0]].'&nbsp;</td>';
					$tranStudentTotal += $tranStudent[$yearInfo[$i][0]];
				}
				
				$newStudentAmt = $currentStudentAmt - array_sum($colAmount[$month]);
		
		
				$x .= '	
					<td class="'.$table_col.'" colspan="2" align="center">'.$tranStudentTotal.'</td>
					</tr>
					<tr class="'.$css3.'">
					<td class="'.$css4.'">'.$Lang['StudentRegistry']['TotalStudents'].'</td>
					<td colspan="'.(sizeof($yearInfo)*2+2).'" class="'.$table_col.'"><div align="center">';
				$x .= $currentStudentAmt.'-'.array_sum($colAmount[$month]).'='.$newStudentAmt;
				$x .= ' // ';
				$x .= $newStudentAmt.'+'.$tranStudentTotal.'='.($newStudentAmt+$tranStudentTotal);
				$x .= '<br />
					</div></td>
					</tr>
					';
					
					$currentStudentAmt = $newStudentAmt+$tranStudentTotal;
					$withRecord = 1;
			}
			if($withRecord==0) {		# no data
				$x .= '			<tr>
					<'.$table_th_td.' rowspan="2">'.$Lang['StaffAttendance']['Reason'].'</'.$table_th_td.'>';
					
				for($i=0; $i<sizeof($yearInfo); $i++)
					$x .= '<'.$table_th_td.' colspan="2" class="'.$table_row.'">'.$yearInfo[$i][1].'</'.$table_th_td.'>';
					
				$x .= '<'.$table_th_td.' colspan="2" class="'.$table_row.'">'.$Lang['StudentRegistry']['SubTotal'].'</'.$table_th_td.'>';	
				$x .= '</tr><tr>';
				
				for($i=0; $i<sizeof($yearInfo); $i++) 
					$x .= '<'.$table_th_td.' class="'.$table_col.'">'.$Lang['StudentRegistry']['Amount'].'</'.$table_th_td.'><'.$table_th_td.'>%</'.$table_th_td.'>';
					
				$x .= '<'.$table_th_td.' class="'.$table_col.'">'.$Lang['StudentRegistry']['Amount'].'</'.$table_th_td.'>
					<'.$table_th_td.'>%</'.$table_th_td.'>';	
				$x .= '</tr>';
				$x .= '<tr><td colspan="'.(sizeof($yearInfo)*2+1+2).'" align="center" height="40">'.$i_no_record_exists_msg.'</td></tr><tr>';
			}
			
			$x .= '			</tbody>
					</table>
					</div>';
				
				return $x;	
		}				

			
		function getReasonTable($AcademicYearID, $Type, $print=0, $dateSelect="", $dateAry=array()) 
		{
			global $Lang, $i_no_record_exists_msg, $intranet_session_language, $i_general_print_date;
			
			$conds = "";
			
			if($dateSelect=="DATE") {
				$yearInfo = getAcademicYearInfoAndTermInfoByDate($dateAry['startDate']);
				$AcademicYearID = $yearInfo[0];
				$conds .= " AND st.ApplyDate BETWEEN '".$dateAry['startDate']."' AND '".$dateAry['endDate']."'";
			}
			
			switch($Type) {
				case STUDENT_REGISTRY_RECORDSTATUS_LEFT : $titleText = $Lang['StudentRegistry']['StatisticsOfLeftReason']; $reasonText = $Lang['StudentRegistry']['eNotice_ReasonOfLeft']; break;
				case STUDENT_REGISTRY_RECORDSTATUS_SUSPEND : $titleText = $Lang['StudentRegistry']['StatisticsOfSuspendReason']; $reasonText = $Lang['StudentRegistry']['eNotice_ReasonOfSuspend']; break;
				default : $titleText = $Lang['StudentRegistry']['StatisticsOfLeftReason']; $reasonText = $Lang['StudentRegistry']['eNotice_ReasonOfLeft']; break;
			}
			
			$sql = "SELECT 
						st.Reason, count(st.RecordID) 
					FROM 
						STUDENT_REGISTRY_STATUS st LEFT OUTER JOIN
						YEAR_CLASS_USER ycu ON (st.UserID=ycu.UserID) LEFT OUTER JOIN
						YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
					WHERE 
						yc.AcademicYearID='$AcademicYearID' AND 
						st.RecordStatus='$Type'
						$conds
					GROUP BY 
						st.Reason
					";
			$result = $this->returnArray($sql,2);
			
			$data = array();
			for($i=0; $i<sizeof($result); $i++) {
				list($reason, $count) = $result[$i];
				$data[$reason] = $count;
			}
			$totalCount = array_sum($data);
			
			if($print==0) {
				$css1 = "common_table_list_v30";
				$css2 = "";
				$css4 = "total_row";
				
				$title = '<span class="sectiontitle_v30"> '.$titleText.'</span>';
			} else {
				$css1 = "tableborder_print";
				$css2 = "tabletop_print tabletext";
				$css3 = "row_print";
				$css4 = "table_text row_print";
				
				$title = "<div align='center'><h4>".GET_SCHOOL_NAME()."<br>";
				$title .= ($intranet_session_language=="en") ? $titleText.$Lang['StudentRegistry']['Of'].$this->getAcademicYearNameByYearID($AcademicYearID) : $this->getAcademicYearNameByYearID($AcademicYearID).$titleText;
				$title .= "</h4></div><br>".$i_general_print_date." : ".date('Y-m-d');
			}
			
			$table_th_td	  = $print? "td" : "th";
			$table_class 	  = $print? "class=\"tableborder_print\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" valign=\"top\"" : "class=\"common_table_list_v30\"";
			
			$x .= '
				<div class="table_board">
				'.$title.'<br />
				<table '.$table_class.'>
				<thead>
				<tr class="'.$css2.'">
				<'.$table_th_td.'> '.$reasonText.'</'.$table_th_td.'>
				<'.$table_th_td.'>'.$Lang['StudentRegistry']['Amount'].'</'.$table_th_td.'>
				<'.$table_th_td.'>'.$Lang['StudentRegistry']['Percentage'].' (%)</'.$table_th_td.'></tr>';	
			
			if(sizeof($result)==0) {
				$colspan = ($Type!=STUDENT_REGISTRY_RECORDSTATUS_RESUME) ? 6 : 5;
				$x .= '<tr class="'.$css3.'"><td colspan="3" align="center" height="40" valign="absmiddle" class="'.$css4.'">'.$i_no_record_exists_msg.'</td></tr>';
			} else {
				foreach($Lang['StudentRegistry']['ReasonAry'] as $reasonid=>$reasonText) {
					
					$x .= '<tr class="'.$css3.'">
						<td class="'.$css4.'">'.$reasonText.'</td>
						<td class="'.$css4.'">'.($data[$reasonid] ? $data[$reasonid] : "0").'</td>
						<td class="'.$css4.'">'.(($totalCount!=0) ? round(($data[$reasonid]/$totalCount)*100,2) : "0").'%</td>
						</tr>';	
				}		
				$x .= '<tr class="'.$css4.'">
						<td class="'.$css4.'">'.$Lang['StudentRegistry']['Total'].'</td>
						<td class="'.$css4.'">'.$totalCount.'</td>
						<td class="'.$css4.'">100%</td>
						</tr>';
			}
			$x .= '</thead>
					<tbody>
					</tbody>
					</table>
					</div>
					<br />';	
					
			return $x;
		}
		
		function getStudRegInfoReport_StudentList($userID_ary, $S_Info=0, $F_Info=0, $M_Info=0, $G_Info=0, $AcademicYearID='', $print=false)
		{
			global $Lang;
			$title_ary = array();
			
			$table_th_td	  = $print? "td" : "th";
			$table_class 	  = $print? "class=\"tableborder_print\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" valign=\"top\"" : "class=\"common_table_list\"";
			$table_head_class = $print? "class=\"tabletop_print tabletext\"" : "";
			$table_row_class  = $print? "class=\"row_print\"" : "";
			$table_item_class = $print? "class=\"table_text row_print\"" : "";
			
			if($S_Info)
			{
				if($S_Info==1)
				{
					for($i=0;$i<sizeof($Lang['StudentRegistry']['InfoReportTitle']['Stud_Basic']);$i++)
					{
						$title_ary[] = $Lang['StudentRegistry']['InfoReportTitle']['Stud_Basic'][$i];
					}
				}
				else
				{
					for($i=0;$i<sizeof($Lang['StudentRegistry']['InfoReportTitle']['Stud_Adv']);$i++)
					{
						$title_ary[] = $Lang['StudentRegistry']['InfoReportTitle']['Stud_Adv'][$i];
					}
				}
			}
			if($F_Info)
			{
					for($i=0;$i<sizeof($Lang['StudentRegistry']['InfoReportTitle']['FatherInfo']);$i++)
					{
						$title_ary[] = $Lang['StudentRegistry']['InfoReportTitle']['FatherInfo'][$i];
					}
			}
			if($M_Info)
			{
					for($i=0;$i<sizeof($Lang['StudentRegistry']['InfoReportTitle']['MotherInfo']);$i++)
					{
						$title_ary[] = $Lang['StudentRegistry']['InfoReportTitle']['MotherInfo'][$i];
					}
			}
			if($G_Info)
			{
					for($i=0;$i<sizeof($Lang['StudentRegistry']['InfoReportTitle']['GuardInfo']);$i++)
					{
						$title_ary[] = $Lang['StudentRegistry']['InfoReportTitle']['GuardInfo'][$i];
					}
			}

			$table_head_content = "<$table_th_td class=\"num_check\">#</$table_th_td>";
			for($i=0;$i<sizeof($title_ary);$i++)
			{
				$table_head_content .= "<$table_th_td>".$title_ary[$i]."</$table_th_td>";
			} 
			
			$table_head = "<table $table_class>
								<tr $table_head_class >
									$table_head_content
								</tr>";
			
			for($i=0;$i<sizeof($userID_ary);$i++)
			{
				$result[] = $this -> RETRIEVE_STUDENT_INFO_ADV_MAL($userID_ary[$i]);
				$Father[] = $this  -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($userID_ary[$i], 'F');
				$Mother[] = $this  -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($userID_ary[$i], 'M');
				$Guard[]  = $this  -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($userID_ary[$i], 'G');
			}
			
			$table_content = "";
			for($i=0;$i<sizeof($userID_ary);$i++)
			{
				#decoding
				$gender_ary  	  = $this->GENDER_ARY();
				$stud_gender 	  = $result[$i][0]['Gender']? $this->RETRIEVE_DATA_ARY($gender_ary, $result[$i][0]['Gender']) : "--";
				$g_gender		  = $Guard[$i][0]['G_GENDER']? $this->RETRIEVE_DATA_ARY($gender_ary, $Guard[$i][0]['G_GENDER']) : "--";   
			
				$nation_ary	 	  = $this->MAL_NATION_DATA_ARY();
				$stud_nation 	  = $result[$i][0]['NATION']? $this->RETRIEVE_DATA_ARY($nation_ary, $result[$i][0]['NATION']) : "--";
		
				$b_place_ary 	  = $this->MAL_B_PLACE_DATA_ARY();
				$stud_b_place	  = $result[$i][0]['B_PLACE']? $this->RETRIEVE_DATA_ARY($b_place_ary, $result[$i][0]['B_PLACE']) : "--";
		
				$race_ary		  = $this->MAL_RACE_DATA_ARY();
				$stud_race		  = $result[$i][0]['RACE']? $this->RETRIEVE_DATA_ARY($race_ary, $result[$i][0]['RACE']) : "--";
		
				$religion_ary	  = $this->MAL_RELIGION_DATA_ARY();
				$stud_religion	  = $result[$i][0]['RELIGION']? $this->RETRIEVE_DATA_ARY($religion_ary, $result[$i][0]['RELIGION']) : "--"; 
	
				$family_lang_ary  = $this->MAL_FAMILY_LANG_ARY();
				$stud_family_lang = $result[$i][0]['FAMILY_LANG']? $this->RETRIEVE_DATA_ARY($family_lang_ary, $result[$i][0]['FAMILY_LANG']) : "--"; 

				$pay_type_ary	  = $this->MAL_PAYMENT_TYPE_ARY();
				$stud_pay_type	  = $result[$i][0]['PAYMENT_TYPE']? $this->RETRIEVE_DATA_ARY($pay_type_ary, $result[$i][0]['PAYMENT_TYPE']) : "--"; 
	
				$lodging_ary	  = $this->MAL_LODGING_ARY();
				$stud_lodging	  = $result[$i][0]['LODGING']? $this->RETRIEVE_DATA_ARY($lodging_ary, $result[$i][0]['LODGING']) : "--";  
	
				$job_nature_ary	  = $this->MAL_JOB_NATURE_DATA_ARY();
				$f_job_nature	  = $Father[$i][0]['JOB_NATURE']? $this->RETRIEVE_DATA_ARY($job_nature_ary, $Father[$i][0]['JOB_NATURE']) : "--";   
				$m_job_nature	  = $Mother[$i][0]['JOB_NATURE']? $this->RETRIEVE_DATA_ARY($job_nature_ary, $Mother[$i][0]['JOB_NATURE']) : "--";   
				$g_job_nature	  = $Guard[$i][0]['JOB_NATURE']? $this->RETRIEVE_DATA_ARY($job_nature_ary, $Guard[$i][0]['JOB_NATURE']) : "--";   
	
				$job_title_ary	  = $this->MAL_JOB_TITLE_DATA_ARY();
				$f_job_title	  = $Father[$i][0]['JOB_TITLE']? $this->RETRIEVE_DATA_ARY($job_title_ary, $Father[$i][0]['JOB_TITLE']) : "--";   
				$m_job_title	  = $Mother[$i][0]['JOB_TITLE']? $this->RETRIEVE_DATA_ARY($job_title_ary, $Mother[$i][0]['JOB_TITLE']) : "--";   
				$g_job_title	  = $Guard[$i][0]['JOB_TITLE']? $this->RETRIEVE_DATA_ARY($job_title_ary, $Guard[$i][0]['JOB_TITLE']) : "--";   
	
				$marital_ary	  = $this->MAL_MARITAL_STATUS_DATA_ARY();
				$f_marital		  = $Father[$i][0]['MARITAL_STATUS']? $this->RETRIEVE_DATA_ARY($marital_ary, $Father[$i][0]['MARITAL_STATUS']) : "--";   
				$m_marital		  = $Mother[$i][0]['MARITAL_STATUS']? $this->RETRIEVE_DATA_ARY($marital_ary, $Mother[$i][0]['MARITAL_STATUS']) : "--";     
	
				$education_ary	  = $this->MAL_EDU_LEVEL_DATA_ARY();
				$f_education	  = $Father[$i][0]['EDU_LEVEL']? $this->RETRIEVE_DATA_ARY($education_ary, $Father[$i][0]['EDU_LEVEL']) : "--";   
				$m_education	  = $Mother[$i][0]['EDU_LEVEL']? $this->RETRIEVE_DATA_ARY($education_ary, $Mother[$i][0]['EDU_LEVEL']) : "--";     	
				$g_education	  = $Guard[$i][0]['EDU_LEVEL']? $this->RETRIEVE_DATA_ARY($education_ary, $Guard[$i][0]['EDU_LEVEL']) : "--";     	
	
				$relation_ary	  = $this->MAL_GUARD_DATA_ARY();
				$g_relation		  = $Guard[$i][0]['GUARD']? $this->RETRIEVE_DATA_ARY($relation_ary, $Guard[$i][0]['GUARD']) : "--";     	
	
				$live_same_ary	  = $this->MAL_LIVE_SAME_ARY();
				$g_live_same	  = $Guard[$i][0]['LIVE_SAME']? $this->RETRIEVE_DATA_ARY($live_same_ary, $Guard[$i][0]['LIVE_SAME']) : "--";     	
				
				# Display Siblings Info
				if($result[$i][0]['BRO_SIS_IN_SCHOOL'])
				{
					$Siblings_UserID_ary = explode(",", $result[$i][0]['BRO_SIS_IN_SCHOOL']);
					$Siblings_html = "";
					for($j=0;$j<sizeof($Siblings_UserID_ary);$j++)
					{
						$Siblings_Info = $this->getStudentInfo_by_UserID($Siblings_UserID_ary[$j], $AcademicYearID);
						if(!$Siblings_Info)
							continue;
						else
						{
							$Siblings_html .= "<tr>
													<td style=\"border-bottom: 0px;border-right: 0px\">".($j+1).".</td>
													<td style=\"border-bottom: 0px;border-right: 0px\">".$Siblings_Info['ClassName']."-".$Siblings_Info['ClassNumber']."</td>
													<td style=\"border-bottom: 0px;border-right: 0px\">".$Siblings_Info['StudentName']."</td>
											   <tr>";
						}
					}
					if($Siblings_html == "")
						$Siblings_html = "<tr><td style=\"border-bottom: 0px;border-right: 0px\">--</td></tr>";
				}
				else
					$Siblings_html = "<tr><td style=\"border-bottom: 0px;border-right: 0px\">--</td></tr>";
				
				$table_content .= "<tr $table_row_class><td $table_item_class>".($i+1)."</td>";
				if($S_Info==1)
				{
					$table_content .= "<td $table_item_class>".($result[$i][0]['STUD_ID']? $result[$i][0]['STUD_ID'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['ChineseName']? $result[$i][0]['ChineseName'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['EnglishName']? $result[$i][0]['EnglishName'] : "--")."</td>";
					$table_content .= "<td $table_item_class>$stud_gender</td>";
					$table_content .= "<td $table_item_class>$stud_nation</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['HomeTelNo']? $result[$i][0]['HomeTelNo'] : "--")."</td>";
				}
				else if($S_Info==2)
				{
					$table_content .= "<td $table_item_class>".($result[$i][0]['STUD_ID']? $result[$i][0]['STUD_ID'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['ChineseName']? $result[$i][0]['ChineseName'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['EnglishName']? $result[$i][0]['EnglishName'] : "--")."</td>";
					$table_content .= "<td $table_item_class>$stud_gender</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['DateOfBirth']? date("Y-m-d", strtotime($result[$i][0]['DateOfBirth'])) : "--")."</td>";
					$table_content .= "<td $table_item_class>$stud_b_place</td>";
					$table_content .= "<td $table_item_class>$stud_nation</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['ANCESTRAL_HOME']? $result[$i][0]['ANCESTRAL_HOME'] : "--")."</td>";
					$table_content .= "<td $table_item_class>$stud_race</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['ID_NO']? $result[$i][0]['ID_NO'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['BIRTH_CERT_NO']? $result[$i][0]['BIRTH_CERT_NO'] : "--")."</td>";
					$table_content .= "<td $table_item_class>$stud_religion</td>";
					$table_content .= "<td $table_item_class>$stud_family_lang</td>";
					$table_content .= "<td $table_item_class>$stud_pay_type</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['HomeTelNo']? $result[$i][0]['HomeTelNo'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['Email']? $result[$i][0]['Email'] : "--")."</td>";
					$table_content .= "<td $table_item_class>$stud_lodging</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['ADDRESS1']? $result[$i][0]['ADDRESS1'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['ADDRESS2']? $result[$i][0]['ADDRESS2'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['ADDRESS3']? $result[$i][0]['ADDRESS3'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['ADDRESS4']? $result[$i][0]['ADDRESS4'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['ADDRESS5']? $result[$i][0]['ADDRESS5'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['ADDRESS6']? $result[$i][0]['ADDRESS6'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['ADMISSION_DATE']? date("Y-m-d", strtotime($result[$i][0]['ADMISSION_DATE'])) : "--")."</td>";
					$table_content .= "<td $table_item_class><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">$Siblings_html</table></td>";
					$table_content .= "<td $table_item_class>".($result[$i][0]['PRI_SCHOOL']? $result[$i][0]['PRI_SCHOOL'] : "--")."</td>";
				}
				if($F_Info)
				{
					$table_content .= "<td $table_item_class>".($Father[$i][0]['NAME_C']? $Father[$i][0]['NAME_C'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['NAME_E']? $Father[$i][0]['NAME_E'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['PROF']? $Father[$i][0]['PROF'] : "--")."</td>";
					$table_content .= "<td $table_item_class>$f_job_nature</td>";
					$table_content .= "<td $table_item_class>$f_job_title</td>";
					$table_content .= "<td $table_item_class>$f_marital</td>";
					$table_content .= "<td $table_item_class>$f_education</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['TEL']? $Father[$i][0]['TEL'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['MOBILE']? $Father[$i][0]['MOBILE'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['OFFICE_TEL']? $Father[$i][0]['OFFICE_TEL'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['POSTAL_ADDR1']? $Father[$i][0]['POSTAL_ADDR1'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['POSTAL_ADDR2']? $Father[$i][0]['POSTAL_ADDR2'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['POSTAL_ADDR3']? $Father[$i][0]['POSTAL_ADDR3'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['POSTAL_ADDR4']? $Father[$i][0]['POSTAL_ADDR4'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['POSTAL_ADDR5']? $Father[$i][0]['POSTAL_ADDR5'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['POSTAL_ADDR6']? $Father[$i][0]['POSTAL_ADDR6'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['POST_CODE']? $Father[$i][0]['POST_CODE'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Father[$i][0]['EMAIL']? $Father[$i][0]['EMAIL'] : "--")."</td>";
				}
				if($M_Info)
				{
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['NAME_C']? $Mother[$i][0]['NAME_C'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['NAME_E']? $Mother[$i][0]['NAME_E'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['PROF']? $Mother[$i][0]['PROF'] : "--")."</td>";
					$table_content .= "<td $table_item_class>$m_job_nature</td>";
					$table_content .= "<td $table_item_class>$m_job_title</td>";
					$table_content .= "<td $table_item_class>$m_marital</td>";
					$table_content .= "<td $table_item_class>$m_education</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['TEL']? $Mother[$i][0]['TEL'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['MOBILE']? $Mother[$i][0]['MOBILE'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['OFFICE_TEL']? $Mother[$i][0]['OFFICE_TEL'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['POSTAL_ADDR1']? $Mother[$i][0]['POSTAL_ADDR1'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['POSTAL_ADDR2']? $Mother[$i][0]['POSTAL_ADDR2'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['POSTAL_ADDR3']? $Mother[$i][0]['POSTAL_ADDR3'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['POSTAL_ADDR4']? $Mother[$i][0]['POSTAL_ADDR4'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['POSTAL_ADDR5']? $Mother[$i][0]['POSTAL_ADDR5'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['POSTAL_ADDR6']? $Mother[$i][0]['POSTAL_ADDR6'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['POST_CODE']? $Mother[$i][0]['POST_CODE'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Mother[$i][0]['EMAIL']? $Mother[$i][0]['EMAIL'] : "--")."</td>";
				}
				if($G_Info)
				{
					$table_content .= "<td $table_item_class>$g_relation</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['NAME_C']? $Guard[$i][0]['NAME_C'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['NAME_C']? $Guard[$i][0]['NAME_C'] : "--")."</td>";
					$table_content .= "<td $table_item_class>$g_gender</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['G_RELATION']? $Guard[$i][0]['G_RELATION'] : "--")."</td>";
					$table_content .= "<td $table_item_class>$g_live_same</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['PROF']? $Guard[$i][0]['PROF'] : "--")."</td>";
					$table_content .= "<td $table_item_class>$g_job_nature</td>";
					$table_content .= "<td $table_item_class>$g_job_title</td>";
					$table_content .= "<td $table_item_class>$g_education</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['TEL']? $Guard[$i][0]['TEL'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['MOBILE']? $Guard[$i][0]['MOBILE'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['OFFICE_TEL']? $Guard[$i][0]['OFFICE_TEL'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['POSTAL_ADDR1']? $Guard[$i][0]['POSTAL_ADDR1'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['POSTAL_ADDR2']? $Guard[$i][0]['POSTAL_ADDR2'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['POSTAL_ADDR3']? $Guard[$i][0]['POSTAL_ADDR3'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['POSTAL_ADDR4']? $Guard[$i][0]['POSTAL_ADDR4'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['POSTAL_ADDR5']? $Guard[$i][0]['POSTAL_ADDR5'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['POSTAL_ADDR6']? $Guard[$i][0]['POSTAL_ADDR6'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['POST_CODE']? $Guard[$i][0]['POST_CODE'] : "--")."</td>";
					$table_content .= "<td $table_item_class>".($Guard[$i][0]['EMAIL']? $Guard[$i][0]['EMAIL'] : "--")."</td>";
				}
				$table_content .= "</tr>";
			}
			$table_end = "</table>";
			
			return $table_head.$table_content.$table_end;
		}
		
		function getClassTeacherByYearClassID($ClassID='')
		{
			$result = array();
			
			if($ClassID)
			{
				$sql = "SELECT
							USR.UserID,
							".Get_Lang_Selection("USR.ChineseName", "USR.EnglishName")."
						FROM
							INTRANET_USER AS USR INNER JOIN
							YEAR_CLASS_TEACHER AS yct ON (USR.UserID = yct.UserID)
						WHERE
							yct.YearClassID = '$ClassID'";
				$result = $this->returnArray($sql,2);
			}
			
			return $result;
		}
				
		function getStudentRegistryStatistics($type, $AcademicYearID, $StudentList=array(), $group1="", $group2="", $subtotal=0, $total=0, $print=0) 
		{
			global $linterface, $Lang, $i_gender_male, $i_gender_female, $button_print, $i_general_print_date;
			
			if($AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();
			
			$statusAry = array(0=>$Lang['StudentRegistry']['RegistryStatus_Approved'], 1=>$Lang['StudentRegistry']['RegistryStatus_Left'], 2=>$Lang['StudentRegistry']['RegistryStatus_Suspend'], 3=>$Lang['StudentRegistry']['RegistryStatus_Resume']);
			
			$fields = " srs.$type, COUNT(srs.UserID), st.RecordStatus";
			$tables = " STUDENT_REGISTRY_STUDENT srs INNER JOIN INTRANET_USER USR ON (USR.UserID=srs.UserID) LEFT OUTER JOIN STUDENT_REGISTRY_STATUS st ON (st.UserID=srs.UserID)";
			$conds .= " AND (st.IsCurrent=1 OR st.IsCurrent IS NULL)";
			
			switch($type) {
				case "GENDER" :
					$fields = " USR.$type, COUNT(srs.UserID), st.RecordStatus";
					$displayRowAry = array("M"=>array("M", $i_gender_male), "F"=>array("F", $i_gender_female));
					break;
				case "RACE" :
					$displayRowAry = $this->MAL_RACE_DATA_ARY(); break;
				case "NATION" :
					$displayRowAry = $this->MAL_NATION_DATA_ARY(); break;
				case "RELIGION" :
					$displayRowAry = $this->MAL_RELIGION_DATA_ARY(); break;
				case "B_PLACE" :
					$displayRowAry = $this->MAL_B_PLACE_DATA_ARY(); break;
				case "PAYMENT_TYPE" :
					$displayRowAry = $this->MAL_PAYMENT_TYPE_ARY(); break;
				case "FAMILY_LANG" :
					$displayRowAry = $this->MAL_FAMILY_LANG_ARY(); break;
				case "LODGING" :
					$displayRowAry = $this->MAL_LODGING_ARY(); break;
				default : break;
			}
				
			# Student list
			if(sizeof($StudentList)>0)
				$studentConds = " AND srs.UserID IN (".implode(',', $StudentList).")";
			
			
			# Group By
			$groupBy = "";
			if($group1!="NA") {
				if($group1=="Status") 
					$groupBy .= " st.RecordStatus";				
			}
			if($group2!="NA") {
				if($group2=="Form") {
					$fields .= ", y.YearID as g2";
					$tables .= " LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=srs.UserID) LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID)";
					$conds .= " AND yc.AcademicYearID='$AcademicYearID'";
					$groupBy .= ($groupBy!="") ? ", y.YearID" : " y.YearID";
					
					$sql = "SELECT YearID, YearName FROM YEAR ORDER BY Sequence";
					$rowAry = $this->returnArray($sql,2);
					
				} else if($group2=="Class") {
					$fields .= ", yc.YearClassID as g2";
					$tables .= " LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=srs.UserID) LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)";
					$conds .= " AND yc.AcademicYearID='$AcademicYearID'";
					$groupBy .= ($groupBy!="") ? ", yc.YearClassID" : " yc.YearClassID";
					
					$sql = "SELECT YearClassID, ".Get_Lang_Selection('ClassTitleB5','ClassTitleEN')." FROM YEAR_CLASS WHERE AcademicYearID='$AcademicYearID' ORDER BY Sequence";
					$rowAry = $this->returnArray($sql,2);
					
				} else if($group2=="Gender") {
					$fields .= ", USR.Gender as g2";
					$conds .= "";
					$groupBy .= ($groupBy!="") ? ", USR.Gender" : " USR.Gender";
					$rowAry = array(array("M", $i_gender_male), array("F", $i_gender_female));
					
				}
			}	
			
			if($group1=="NA" && $group2=="NA")
				$groupBy .= ($groupBy!="") ? ", srs.UserID" : " srs.UserID"; 	
				
			$groupBy = " Group By $type, $groupBy";
			
		
			
			$sql = "
				SELECT 
					$fields
				FROM
					$tables
				WHERE
					srs.UserID IS NOT NULL $studentConds $conds
				$groupBy
				";
			$row = $this->returnArray($sql);
			
			$dataAry = array();
			for($i=0; $i<sizeof($row); $i++) {
				if($group2!="NA") {
					list($typeRef, $count, $rs, $g2) = $row[$i];
					if($rs=="") $rs = 0;
					if($group1!="NA")
						$dataAry[$typeRef][$rs][$g2] += $count;
					else 
						$dataAry[$typeRef][$g2] += $count;
					
				}  else {
					list($typeRef, $count, $rs) = $row[$i];
					if($rs=="") $rs = 0;
					$dataAry[$typeRef][$rs] += $count;
					
				}
			}
			
			$rowspan = ($group1!="NA" || $group2!="NA") ? ' rowspan="2"' : '';
			
			
			$x = '
				<div class="table_filter">'.$Lang['StudentRegistry']['AcademicYear'].': '.$this->getAcademicYearNameByYearID($AcademicYearID).'</div>
		            <div class="table_board">';
		    if($print==1)
				$x .= '<div align="left">'.$i_general_print_date.': '.date('Y-m-d').'</div>';
			$x .= '			
		              <br />
		                  <div class="table_board">
		                    <table class="common_table_list_v30">
		                        <thead>
		
		                          <tr>
		                            <th'.$rowspan.'>'.$Lang['StudentRegistry']['StudentStatistics_Type_Malaysia'][$type].'</th>';
		    if($group1=="NA" && $group2=="NA") {		# $group1=="NA" AND $group2=="NA"
		    	$x .= '<th class="total_col">'.$Lang['StudentRegistry']['Amount'].'</th>';	
		    	if($subtotal==1)
		    		$x .= '<th class="total_col">'.$Lang['StudentRegistry']['SubTotal'].'</th>';
		    } 
		    if($group1!="NA") {		# $group1!="NA"
						$colspan = sizeof($rowAry) + (($subtotal==1) ? 1 : 0);
				
						$c = 0;
						foreach($statusAry as $val) {
							$css = ($c=0) ? "total_col" : "";
							$x .= ' <th colspan="'.$colspan.'" class="'.$css.' total_row">'.$val.'</th>';
						}
		
				if($group2=="NA" && $subtotal==1) {
				
		    		$x .= '<th>'.$Lang['StudentRegistry']['SubTotal'].'</th>';
				}
		
		        		$x .= '     </tr>';
					}
					
			$x .= '				  <tr>';
		
			if($group2!="NA") {
				if($group1!="NA") {		# $group1!="NA" && $group2!="NA"
					foreach($statusAry as $val) {
						for($k=0; $k<sizeof($rowAry); $k++) { 
							$css = ($k==0) ? "class=\"total_col\"" : "";
							$x .= '<th '.$css.'>'.$rowAry[$k][1].'</th>';
						}
						if($subtotal==1)
					    	$x .= '<th>'.$Lang['StudentRegistry']['SubTotal'].'</th>';
					}
				} else {				# $group1=="NA" && $group2!="NA"
						for($k=0; $k<sizeof($rowAry); $k++) { 
							$css = ($k==0) ? "class=\"total_col\"" : "";
							$x .= '<th '.$css.'>'.$rowAry[$k][1].'</th>';
						}
						if($subtotal==1)
					    	$x .= '<th>'.$Lang['StudentRegistry']['SubTotal'].'</th>';
		
				}
		
			}
			
			$countTotal = array();
			
			foreach($displayRowAry as $id=>$name) {
				$x .= '</tr>
		                        </thead>
		                        <tbody>
		                          <tr>
		                            <td>'.(($type=="GENDER") ? $name[1] : Get_Lang_Selection($name[1], $name[0])).'</td>';
									if($group1!="NA") {
										$countSubtotal = 0;
										$c = 0;
										foreach($statusAry as $key=>$val) {
											//$countSubtotal = 0;
											if($group2!="NA") {		# $group1!="NA" && $group2!="NA"
												$countSubtotal = 0;	
												for($k=0; $k<sizeof($rowAry); $k++) { 
													$css = ($k==0) ? "class=\"total_col\"" : "";
													$x .= '<td '.$css.'>'.(isset($dataAry[$id][$key][$rowAry[$k][0]]) ? $dataAry[$id][$key][$rowAry[$k][0]] : 0).'</td>';
													$countSubtotal += isset($dataAry[$id][$key][$rowAry[$k][0]]) ? $dataAry[$id][$key][$rowAry[$k][0]] : 0;
													$countTotal[$key][$rowAry[$k][0]] += isset($dataAry[$id][$key][$rowAry[$k][0]]) ? $dataAry[$id][$key][$rowAry[$k][0]] : 0;
												}
											} else {				# $group1!="NA" && $group2=="NA"
													$css = ($c==0) ? "class=\"total_col\"" : "";
													$x .= '<td '.$css.'>'.(isset($dataAry[$id][$key]) ? $dataAry[$id][$key] : 0).'</td>';
													$countSubtotal += isset($dataAry[$id][$key]) ? $dataAry[$id][$key] : 0;
													$countTotal[$key] += isset($dataAry[$id][$key]) ? $dataAry[$id][$key] : 0;
											}
											
											if($group2!="NA" && $subtotal==1)
										    	$x .= '<td>'.$countSubtotal.'</td>';
										    	
										    $c++;
										}
									} else if($group2!="NA") {	# $group1=="NA" && $group2!="NA"
										$countSubtotal = 0;
										for($k=0; $k<sizeof($rowAry); $k++) { 
											$css = ($k==0) ? "class=\"total_col\"" : "";
											 $x .= '<td '.$css.'>'.(isset($dataAry[$id][$rowAry[$k][0]])?$dataAry[$id][$rowAry[$k][0]]:0).'</td>';
											 $countSubtotal += isset($dataAry[$id][$rowAry[$k][0]])?$dataAry[$id][$rowAry[$k][0]]:0;
											 $countTotal[$rowAry[$k][0]] += isset($dataAry[$id][$rowAry[$k][0]])?$dataAry[$id][$rowAry[$k][0]]:0;
										}
									} else {		# $group1=="NA" && $group2=="NA"
										$countSubtotal = 0;
										$x .= '<td class="total_col">'.(isset($dataAry[$id]) ? array_sum($dataAry[$id]) : 0).'</td>';
										$countSubtotal += (isset($dataAry[$id]) ? array_sum($dataAry[$id]) : 0);
										$countTotal[0] += isset($dataAry[$id]) ? array_sum($dataAry[$id]) : 0;
									}
									
									if($group1=="NA" || $group2=="NA")
										if($subtotal==1)
											$x .= '<td>'.$countSubtotal.'</td>';
											
		    						$x .= '</tr>';	
		    	
			}
			
			if($total==1) {
				$x .= '
			                          <tr  class="total_row">
			                            <td>'.$Lang['StudentRegistry']['Total'].'</td>';
			                            if($group1=="NA" && $group2=="NA") {		# $group1=="NA" && $group2=="NA"
			                            	$x .= '<td class="total_col">'.$countTotal[0].'</td>';
			                            	if($subtotal==1)
			                            		$x .= '<td>'.$countTotal[0].'</td>';
			                            } else if($group1!="NA") {	
			                            	if($group2!="NA") {		# $group1!="NA" && $group2!="NA"
												foreach($statusAry as $key=>$val) {
													for($k=0; $k<sizeof($rowAry); $k++) { 
														$css = ($k==0) ? "class=\"total_col\"" : "";
														$x .= '<td '.$css.'>'.(isset($countTotal[$key][$rowAry[$k][0]])?$countTotal[$key][$rowAry[$k][0]]:0).'</td>';
													}
													if($subtotal==1)
												    	$x .= '<td>'.array_sum($countTotal[$key]).'</td>';
												}
			                            	} else {				# $group1!="NA" && $group2=="NA"
			                            			$c = 0;
			                            			
													foreach($statusAry as $key=>$val) {
														$css = ($c==0) ? "class=\"total_col\"" : "";
														$x .= '<td '.$css.'>'.(isset($countTotal[$key])?$countTotal[$key]:0).'</td>';
														$c++;
													}
													if($subtotal==1)
												    	$x .= '<td>'.array_sum($countTotal).'</td>';
			                            		
			                            	}
			                            } else if($group2!="NA") {		# group1=="NA" && $group2!="NA"
												for($k=0; $k<sizeof($rowAry); $k++) { 
													$css = ($k==0) ? "class=\"total_col\"" : "";
													$x .= '<td '.$css.'>'.(isset($countTotal[$rowAry[$k][0]])?$countTotal[$rowAry[$k][0]]:0).'</td>';
												}
												if($subtotal==1)
											    	$x .= '<td>'.array_sum($countTotal).'</td>';
			                            	
			                            } 
				$x .= '						</tr>';
			}
		    $x .= '                 </tr>
		                        </tbody>
		                      </table>
		              </div>
		            </div>	
					<br>		
			';
			
			if($print==0) {
				$x .= '<div class="edit_bottom">
		                            <p class="spacer"></p>
		           '.$linterface->GET_ACTION_BTN($button_print, "button", "checksubmit(this.form, 1)").'
		                            <p class="spacer"></p>
		                        </div>';
			}
			
			return $x;
		}
				
		function getStatisticsWithTransferredStudents($type, $AcademicYearID, $subtotal=0, $total=0, $print=0) 
		{
			global $linterface, $Lang, $i_gender_male, $i_gender_female, $button_print, $i_general_print_date;
			
			if($AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();
			
			switch($type) {
				case "GENDER" :
					$fields = " USR.$type";
					$displayRowAry = array("M"=>array("M", $i_gender_male), "F"=>array("F", $i_gender_female));
					break;
				case "RACE" :
					$fields = " srs.$type";
					$displayRowAry = $this->MAL_RACE_DATA_ARY(); break;
				case "NATION" :
					$fields = " srs.$type";
					$displayRowAry = $this->MAL_NATION_DATA_ARY(); break;
				case "RELIGION" :
					$fields = " srs.$type";
					$displayRowAry = $this->MAL_RELIGION_DATA_ARY(); break;
				case "B_PLACE" :
					$fields = " srs.$type";
					$displayRowAry = $this->MAL_B_PLACE_DATA_ARY(); break;
				case "PAYMENT_TYPE" :
					$fields = " srs.$type";
					$displayRowAry = $this->MAL_PAYMENT_TYPE_ARY(); break;
				case "FAMILY_LANG" :
					$fields = " srs.$type";
					$displayRowAry = $this->MAL_FAMILY_LANG_ARY(); break;
				case "LODGING" :
					$fields = " srs.$type";
					$displayRowAry = $this->MAL_LODGING_ARY(); break;
				default : break;
			}
			
			$gpBy = " GROUP BY $type, USR.Gender";
			
			$yearStartDate = getStartDateOfAcademicYear($AcademicYearID);
			
			$form1StudentID = array();
			
			# Form 1 students
			$sql = "SELECT $fields, USR.Gender, COUNT(ycu.UserID) FROM INTRANET_USER USR LEFT OUTER JOIN STUDENT_REGISTRY_STUDENT srs on USR.UserID=srs.UserID LEFT OUTER JOIN YEAR_CLASS_USER ycu ON srs.UserID=ycu.UserID INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE yc.YearID='1' AND yc.AcademicYearID='$AcademicYearID' $gpBy";
			$column1 = $this->returnArray($sql,3);
			
			# Transferred students
			$sql = "SELECT $fields, USR.Gender, COUNT(srs.UserID) FROM INTRANET_USER USR LEFT OUTER JOIN STUDENT_REGISTRY_STUDENT srs ON USR.UserID=srs.UserID INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=srs.UserID INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID WHERE yc.AcademicYearID='$AcademicYearID' AND srs.ADMISSION_DATE>='$yearStartDate' $gpBy";
			$column2 = $this->returnArray($sql,3);
			
			# current students (except form 1 students)
			$sql = "SELECT UserID FROM YEAR_CLASS_USER ycu INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID WHERE yc.AcademicYearID='$AcademicYearID' AND yc.YearID='1'";
			$form1Student = $this->returnVector($sql);
			if(sizeof($form1Student)>0) $conds = " AND ycu.UserID NOT IN (".implode(',',$form1Student).")";
			$sql = "SELECT $fields, USR.Gender, COUNT(ycu.UserID) FROM INTRANET_USER USR LEFT OUTER JOIN STUDENT_REGISTRY_STUDENT srs ON srs.UserID=USR.UserID INNER JOIN YEAR_CLASS_USER ycu ON srs.UserID=ycu.UserID INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID WHERE yc.AcademicYearID='$AcademicYearID' $conds $gpBy";
			$column3 = $this->returnArray($sql,3);
			
			# left students
			$sql = "SELECT $fields, USR.Gender, COUNT(sts.UserID) FROM INTRANET_USER USR LEFT OUTER JOIN STUDENT_REGISTRY_STUDENT srs on srs.UserID=USR.UserID INNER JOIN STUDENT_REGISTRY_STATUS sts ON sts.UserID=srs.userID INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=sts.UserID INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID WHERE yc.AcademicYearID='$AcademicYearID' AND sts.IsCurrent=1 AND sts.RecordStatus='".STUDENT_REGISTRY_RECORDSTATUS_LEFT."' $gpBy";
			$column4 = $this->returnArray($sql,3);
			/*
			debug_pr($column1);
			debug_pr($column2);
			debug_pr($column3);
			debug_pr($column4);
			*/
			$result1 = array();
			$result2 = array();
			$result3 = array();
			$result4 = array();
			
			
			for($i=0; $i<sizeof($column1); $i++) {
				list($tempType, $tempGender, $tempCount) = $column1[$i];
				$result1[$tempGender][$tempType] = $tempCount;
			}
			for($i=0; $i<sizeof($column2); $i++) {
				list($tempType, $tempGender, $tempCount) = $column2[$i];
				$result2[$tempGender][$tempType] = $tempCount;
			}
			for($i=0; $i<sizeof($column3); $i++) {
				list($tempType, $tempGender, $tempCount) = $column3[$i];
				$result3[$tempGender][$tempType] = $tempCount;
			}
			for($i=0; $i<sizeof($column4); $i++) {
				list($tempType, $tempGender, $tempCount) = $column4[$i];
				$result4[$tempGender][$tempType] = $tempCount;
			}			
			
			$x = '
				<div class="table_filter">'.$Lang['StudentRegistry']['AcademicYear'].': '.$this->getAcademicYearNameByYearID($AcademicYearID).'</div>
		            <div class="table_board">';
		            
		    if($print==1)
				$x .= '<div align="left">'.$i_general_print_date.': '.date('Y-m-d').'</div>';
				
			$rowspan = " rowspan='5'";	
			$colspan = ($subtotal) ? " colspan='3'" : " colspan='2'";
			$x .= '			
		              <br />
		                  <div class="table_board">
		                    <table class="common_table_list_v30">
		                        <thead>
		
		                          <tr>
		                            <th '.$rowspan.'>'.$Lang['StudentRegistry']['StudentStatistics_Type_Malaysia'][$type].'</th>
		                            <th '.$colspan.' class="total_col">'.$Lang['StudentRegistry']['NewStudents'].'</th>
		                            <th '.$colspan.' class="total_col">'.$Lang['StudentRegistry']['TransferredStudents'].'</th>
		                            <th '.$colspan.' class="total_col">'.$Lang['StudentRegistry']['CurrentStudents'].'</th>
		                            <th '.$colspan.' class="total_col">'.$Lang['StudentRegistry']['LeftStudents'].'</th>
								</tr><tr>
				';
		    
		    $x .= '<th class="total_col">'.$i_gender_male.'</th><th>'.$i_gender_female.'</th>';
		    if($subtotal)
		    	$x .= '<th>'.$Lang['StudentRegistry']['SubTotal'].'</th>';	
		    $x .= '<th class="total_col">'.$i_gender_male.'</th><th>'.$i_gender_female.'</th>';
		    if($subtotal)
		    	$x .= '<th>'.$Lang['StudentRegistry']['SubTotal'].'</th>';	
		    $x .= '<th class="total_col">'.$i_gender_male.'</th><th>'.$i_gender_female.'</th>';
		    if($subtotal)
		    	$x .= '<th>'.$Lang['StudentRegistry']['SubTotal'].'</th>';	
		    $x .= '<th class="total_col">'.$i_gender_male.'</th><th>'.$i_gender_female.'</th>';
		    if($subtotal)
		    	$x .= '<th>'.$Lang['StudentRegistry']['SubTotal'].'</th>';	
		    
		     
			$x .= '				  </tr><tr>';

			$countTotal = array();
			$sum = array();
			
			foreach($displayRowAry as $id=>$name) {
				$x .= '</tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>'.(($type=="GENDER") ? $name[1] : Get_Lang_Selection($name[1], $name[0])).'</td>';
                            
                # column 1 (New Student)
				$x .= '<td class="total_col">'.(isset($result1['M'][$id]) ? $result1['M'][$id] : 0).'</td>';
				$x .= '<td>'.(isset($result1['F'][$id]) ? $result1['F'][$id] : 0).'</td>';
				if($subtotal)
					$x .= '<td>'.($result1['M'][$id]+$result1['F'][$id]).'</td>';
				$sum[1]['M'] += $result1['M'][$id];
				$sum[1]['F'] += $result1['F'][$id];

                # column 2 (Transferred Student)
				$x .= '<td class="total_col">'.(isset($result2['M'][$id]) ? $result2['M'][$id] : 0).'</td>';
				$x .= '<td>'.(isset($result2['F'][$id]) ? $result2['F'][$id] : 0).'</td>';
				if($subtotal)
					$x .= '<td>'.($result2['M'][$id]+$result2['F'][$id]).'</td>';
				$sum[2]['M'] += $result2['M'][$id];
				$sum[2]['F'] += $result2['F'][$id];
					
                # column 3 (Current Student)
				$x .= '<td class="total_col">'.(isset($result3['M'][$id]) ? $result3['M'][$id] : 0).'</td>';
				$x .= '<td>'.(isset($result3['F'][$id]) ? $result3['F'][$id] : 0).'</td>';
				if($subtotal)
					$x .= '<td>'.($result3['M'][$id]+$result3['F'][$id]).'</td>';
				$sum[3]['M'] += $result3['M'][$id];
				$sum[3]['F'] += $result3['F'][$id];
					
                # column 4 (Left Student)
				$x .= '<td class="total_col">'.(isset($result4['M'][$id]) ? $result4['M'][$id] : 0).'</td>';
				$x .= '<td>'.(isset($result4['F'][$id]) ? $result4['F'][$id] : 0).'</td>';
				if($subtotal)
					$x .= '<td>'.($result4['M'][$id]+$result4['F'][$id]).'</td>';
				$sum[4]['M'] += $result4['M'][$id];
				$sum[4]['F'] += $result4['F'][$id];

				$x .= '</tr>';	
			}
			
			if($total==1) {
				$x .= '
                      <tr  class="total_row">
                        <td>'.$Lang['StudentRegistry']['Total'].'</td>';
                    
                # column 1 (New Student) 
                $x .= '<td class="total_col">'.$sum[1]['M'].'</td>';
                $x .= '<td>'.$sum[1]['F'].'</td>';
                if($subtotal)
                	$x .= '<td>'.array_sum($sum[1]).'</td>';
                	
                # column 2 (Transferred Student)
                $x .= '<td class="total_col">'.$sum[2]['M'].'</td>';
                $x .= '<td>'.$sum[2]['F'].'</td>';
                if($subtotal)
                	$x .= '<td>'.array_sum($sum[2]).'</td>';
                	
                # column 3 (Current Student)
                $x .= '<td class="total_col">'.$sum[3]['M'].'</td>';
                $x .= '<td>'.$sum[3]['F'].'</td>';
                if($subtotal)
                	$x .= '<td>'.array_sum($sum[3]).'</td>';
                	
                # column 4 (Left Student)
                $x .= '<td class="total_col">'.$sum[4]['M'].'</td>';
                $x .= '<td>'.$sum[4]['F'].'</td>';
                if($subtotal)
                	$x .= '<td>'.array_sum($sum[4]).'</td>';
			                            
				$x .= '						</tr>';
			}
		    $x .= '                 </tr>
		                        </tbody>
		                      </table>
		              </div>
		            </div>	
					<br>		
			';
			
			if($print==0) {
				$x .= '<div class="edit_bottom">
		                            <p class="spacer"></p>
		           '.$linterface->GET_ACTION_BTN($button_print, "button", "checksubmit(this.form, 1)").'
		                            <p class="spacer"></p>
		                        </div>';
			}
			
			return $x;
		}

		############################################################
		# HK
		############################################################
		function RETRIEVE_STUDENT_INFO_BASIC_HK($StudentID='', $StudentID_str='', $academicYearID='')
		{
			$result = array();
			
			if($StudentID)
			{
				$cond = " a.UserID = $StudentID ";
			}
			else
			{
				$cond = " a.UserID in ($StudentID_str) ";
			}

            if (!$academicYearID) {
                $academicYearID = Get_Current_Academic_Year_ID();
            }

			$NameField = getNameFieldByLang("f.");
			$NameField_submit = getNameFieldByLang("g.");
			$sql = "SELECT
						a.UserID,
						a.EnglishName,
						a.ChineseName,
						a.Gender,
						d.NATION,
						a.HomeTelNo,
						IF(b.ClassTitleEN IS NULL OR b.ClassTitleEN = '', '--', b.ClassTitleEN) AS ClassName,
						IF(b.ClassNumber IS NULL OR b.ClassNumber = '', '--', b.ClassNumber) AS ClassNumber,
						/*IF(a.ClassName IS NULL OR a.ClassName = '', '--', a.ClassName) AS ClassName,
						IF(a.ClassNumber IS NULL OR a.ClassNumber = '', '--', a.ClassNumber) AS ClassNumber,*/
						d.DateModified,
						d.ModifyBy,
						IF(c.YearName IS NULL OR c.YearName = '', '--', c.YearName) AS YearName,
						b.YearClassID,
						a.RecordStatus,
						DATE_FORMAT(a.DateOfBirth, '%Y-%m-%d') as DOB,
						d.ORIGIN,
						a.HKID,
						e.PassportNo,
						d.ID_NO,
						d.ID_TYPE,
						e.Nationality,
						d.ETHNICITY_CODE,
						d.FAMILY_LANG,
						d.FAMILY_LANG_OTHERS,
						d.RELIGION,
						d.RELIGION_OTHERS,
						d.CHURCH,
						a.MobileTelNo,
						a.UserEmail,
						d.ADDRESS_ROOM_EN,
						d.ADDRESS_FLOOR_EN,
						d.ADDRESS_BLK_EN,
						d.ADDRESS_BLD_EN,
						d.ADDRESS_EST_EN,
						d.ADDRESS_STR_EN,
						d.ADDRESS_AREA,
						d.LAST_SCHOOL_EN,
						d.LAST_SCHOOL_CH,
						". $NameField ." as ModifyByName,
						d.CHN_COMMERCIAL_CODE1,
						d.CHN_COMMERCIAL_CODE2,
						d.CHN_COMMERCIAL_CODE3,
						d.CHN_COMMERCIAL_CODE4,
						a.FirstName,
						a.LastName,
						a.CFirstName,
						a.CLastName,
						d.DISTRICT_CODE,
						d.NATION_CODE,
						a.WebSAMSRegNo,
						". $NameField_submit ." as SubmitByName,
						srs2.DATA_CONFIRM_DATE,
						c.WEBSAMSCode,
						d.BIRTH_CERT_NO
					FROM 
						INTRANET_USER as a
                        LEFT JOIN (SELECT   yc.YearClassID,
                        					yc.ClassTitleEN, 
                        					ycu.ClassNumber, 
                        					ycu.UserID,
                        					yc.YearID
                    				FROM
                    					    YEAR_CLASS_USER ycu
                    				INNER JOIN
                    					    YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
                    				WHERE 
                    					    yc.AcademicYearID='". $academicYearID ."'
                    				) AS b ON b.UserID=a.UserID
						/*left join YEAR_CLASS as b on (b.ClassTitleEN = a.ClassName and b.AcademicYearID='". $academicYearID ."')*/
						left join YEAR as c on (c.YearID = b.YearID)
						left join STUDENT_REGISTRY_STUDENT as d on (a.UserID = d.UserID)
						left join INTRANET_USER_PERSONAL_SETTINGS as e on (e.UserID=a.UserID)
						left join INTRANET_USER as f on (f.UserID=d.ModifyBy)
						left join STUDENT_REGISTRY_SUBMITTED srs2 ON (srs2.UserID=a.UserID AND srs2.AcademicYearID='".$academicYearID."')
						left join INTRANET_USER as g on (g.UserID=srs2.DATA_CONFIRM_BY AND srs2.AcademicYearID='".$academicYearID."')
					WHERE 
						$cond
					";
			$result = $this->returnArray($sql);

			if(!$StudentID)
			{
				$ary = array();
				# build data array with user id as index
				if(!empty($result))
				{
					for($i=0;$i<sizeof($result);$i++)
					{
						$ary[$result[$i]['UserID']] = $result[$i];
					}
				}
				$result = $ary;
			}	
			return $result;
		}
		
		function checkParentFillOnlineReg()
		{
			global $UserType, $UserID, $sys_custom;
				
			# check user type		 
			if($UserType != USERTYPE_PARENT || !$sys_custom['OnlineRegistry'])
				return false;
			
			# within new student online reg period?
			if(date("Y-m-d") >= $this->NewStudentStartDate && date("Y-m-d") <= $this->NewStudentEndDate)
			{
				# check in the selected group or not
				include_once("libgroup.php");
				$lgroup = new libgroup();
				if($lgroup->isGroupMember($UserID, $this->GroupID))
				{
					$_SESSION['ParentFillOnlineReg'] = true;
					$_SESSION['ParentFillOnlineRegMode'] = "new";
					return true;
				}
			}
			
				
			# within old student online reg period?
			$selected_year = trim_array(explode(",",$this->YearIDStr));
			if (false && $UserID==4883)
			{
				debug($this->CurrentStudentStartDate);
				debug($this->CurrentStudentEndDate);
				debug_r($selected_year);
			}
			if(date("Y-m-d") >= $this->CurrentStudentStartDate && date("Y-m-d") <= $this->CurrentStudentEndDate && !empty($selected_year))
			{
				# check their children is in the selected form or not
				# children info
				$child_form_ary = $this->getChildrenForm();

				if(sizeof($child_form_ary)>0)
				{
					foreach($child_form_ary as $k=>$d)
					{
						if(in_array($d,$selected_year))
						{
							$_SESSION['ParentFillOnlineReg'] = true;
							$_SESSION['ParentFillOnlineRegMode'] = "current";
							return true;
						}
					}
				}
			}
			
			return false;
			
			
			/*
			# check in new student group or old student group
			include_once("libgroup.php");
			$lgroup = new libgroup();
			if($lgroup->isGroupMember($UserID, $this->GroupID))
			{
				# check period
				if(!(date("Y-m-d") >= $this->NewStudentStartDate && date("Y-m-d") <= $this->NewStudentEndDate))
				{
					return false;
				}
				else	
				{
					$_SESSION['ParentFillOnlineReg'] = true;
					return true;
				}
			}
			else
			{
				# check belongs to selected Form
				
				
				
				
				return false;
			}
			*/
				

			
		}
		
		function updateStudentRegistry_HK($stuAry=array(), $student_id, $pAry=array(), $eContactAry=array())
		{			
			global $UserID;
			
			if(sizeof($stuAry)>0)
			{
				$sql = "SELECT COUNT(*) FROM STUDENT_REGISTRY_STUDENT WHERE UserID='$student_id'";
				$count = $this->returnVector($sql);
			
				if($count[0]==0) {
					# insert record	
					foreach($stuAry as $fields[]=>$values[]);
					
					$sql = "INSERT INTO STUDENT_REGISTRY_STUDENT (UserID, ";
					foreach($fields as $field) $sql .= "$field, ";
					$sql .= "DateInput, InputBy, DateModified, ModifyBy) VALUES ('$student_id', ";
					foreach($values as $value) $sql .= "'$value', ";
					$sql .= "NOW(), '$UserID', NOW(), '$UserID')";
				} else {
					# update record
					$sql = "UPDATE STUDENT_REGISTRY_STUDENT SET ";
					foreach($stuAry as $field=>$value)
						$sql .= $field."='".$value."', ";
					$sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE UserID='$student_id'";
				}
				$result = $this->db_db_query($sql);
			}
			
			if(sizeof($pAry)>0)
			{
				$sql = "SELECT SRPG_ID FROM STUDENT_REGISTRY_PG WHERE StudentID='$student_id' and SEQUENCE='". $pAry['SEQUENCE'] ."'";
				$result = $this->returnVector($sql);
				
				if(trim($result[0])==0) {
					# insert record	
					foreach($pAry as $fields[]=>$values[]);
					
					$sql = "INSERT INTO STUDENT_REGISTRY_PG (StudentID, ";
					foreach($fields as $field) $sql .= "$field, ";
					$sql .= "DateInput, InputBy, DateModified, ModifyBy) VALUES ('$student_id', ";
					foreach($values as $value) $sql .= "'$value', ";
					$sql .= "NOW(), '$UserID', NOW(), '$UserID')";
				} else {
					# update record
					$sql = "UPDATE STUDENT_REGISTRY_PG SET ";
					foreach($pAry as $field=>$value)
						$sql .= $field."='".$value."', ";
					$sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE SRPG_ID='". $result[0] ."'";
				}
				$result = $this->db_db_query($sql);
			}
			
			if(sizeof($eContactAry)>0)
			{
				$sql = "SELECT RecordID FROM STUDENT_REGISTRY_EMERGENCY_CONTACT WHERE StudentID='$student_id'";
				$result = $this->returnVector($sql);
				
				if(trim($result[0])==0) {
					# insert record	
					foreach($eContactAry as $fields[]=>$values[]);
					
					$sql = "INSERT INTO STUDENT_REGISTRY_EMERGENCY_CONTACT (StudentID, ";
					foreach($fields as $field) $sql .= "$field, ";
					$sql .= "DateInput, InputBy, DateModified, ModifyBy) VALUES ('$student_id', ";
					foreach($values as $value) $sql .= "'$value', ";
					$sql .= "NOW(), '$UserID', NOW(), '$UserID')";
				} else {
					# update record
					$sql = "UPDATE STUDENT_REGISTRY_EMERGENCY_CONTACT SET ";
					foreach($eContactAry as $field=>$value)
						$sql .= $field."='".$value."', ";
					$sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE RecordID='". $result[0] ."'";
				}
				$result = $this->db_db_query($sql);
			}
		}
		
		function RETRIEVE_GUARDIAN_INFO_HK($StudentID='', $seq=1, $StudentID_str='')
		{
			$result = array();
			
			if($StudentID)
			{
				$cond = " StudentID = '$StudentID' ";
			}
			else
			{
				$cond = " StudentID in ($StudentID_str) ";
			}
			
			$sql = "SELECT
						StudentID,
						PG_TYPE,
						NAME_C,
						NAME_E,
						JOB_TITLE_OTHERS,
						TEL,
						MOBILE,
						EMAIL,
						HKID,
						RELIGION,
						RELIGION_OTHERS,
						CHURCH,
						PG_TYPE_OTHERS,
						ADDRESS_ROOM_EN,
						ADDRESS_FLOOR_EN,
						ADDRESS_BLK_EN,
						ADDRESS_BLD_EN,
						ADDRESS_EST_EN,
						ADDRESS_STR_EN,
						ADDRESS_AREA,
						DISTRICT_CODE
					FROM 
						STUDENT_REGISTRY_PG
					WHERE 
						$cond 
						and SEQUENCE='$seq'
					";
			$result = $this->returnArray($sql);
			
			if($StudentID_str)
			{
				$ary = array();
				# build data array with user id as index
				if(!empty($result))
				{
					for($i=0;$i<sizeof($result);$i++)
					{
						$ary[$result[$i]['StudentID']] = $result[$i];
					}
				}
				return $ary;
			}	
			else
			{
				return $result[0];
			}
		}
		
		function RETRIEVE_EMERGENCY_CONTACT_INFO_HK($StudentID='', $StudentID_str='')
		{
			$result = array();
			
			if($StudentID)
			{
				$cond = " StudentID = '$StudentID' ";
			}
			else
			{
				$cond = " StudentID in ($StudentID_str) ";
			}
			
			$sql = "SELECT
						StudentID,
						Contact3_Title,
						Contact3_EnglishName,
						Contact3_ChineseName,
						Contact3_Relationship,
						Contact3_Phone,
						Contact3_Mobile							
					FROM 
						STUDENT_REGISTRY_EMERGENCY_CONTACT
					WHERE 
						$cond
					";
			$result = $this->returnArray($sql);
			
			if($StudentID_str)
			{
				$ary = array();
				# build data array with user id as index
				if(!empty($result))
				{
					for($i=0;$i<sizeof($result);$i++)
					{
						$ary[$result[$i]['StudentID']] = $result[$i];
					}
				}
				return $ary;
			}	
			else
			{
				return $result[0];
			}
		}
		
		function updateStudentLastClassInfo_HK($StudentID='',$academicAry=array())
		{
			# remove all existing records
			$sql = "delete from STUDENT_REGISTRY_LASTCLASSINFO where StudentID='$StudentID'";
			$this->db_db_query($sql);
				
			if(!empty($academicAry))
			{
				# insert records
				foreach($academicAry as $year_id=>$data)
				{
					$sql = "insert into STUDENT_REGISTRY_LASTCLASSINFO (StudentID, YearID, ClassName, ClassNumber) values ('$StudentID', '$year_id', '". $data['ClassName'] ."', '". $data['ClassNumber'] ."')";
					$this->db_db_query($sql);
				}
			}
		}
		
		function RETRIEVE_LAST_CLASSINFO_HK($StudentID='')
		{
			$return_result = array();
			
			if($StudentID)
			{
				$sql = "SELECT
							YearID, 
							ClassName,
							ClassNumber				
						FROM 
							STUDENT_REGISTRY_LASTCLASSINFO
						WHERE 
							StudentID = '$StudentID'
						";
				$result = $this->returnArray($sql);
			}
			
			# re-build the data array
			if(!empty($result))
			{
				for($i=0;$i<=sizeof($result);$i++)
				{
					$return_result[$result[$i]['YearID']]['ClassName'] = $result[$i]['ClassName'];
					$return_result[$result[$i]['YearID']]['ClassNumber'] = $result[$i]['ClassNumber'];
				}
			}
			return $return_result;
		}
		
		function updateStudentBroSisInfo_HK($StudentID='',$bsAry=array())
		{
			# remove all existing records
			$sql = "delete from STUDENT_REGISTRY_BROSIS where StudentID='$StudentID'";
			$this->db_db_query($sql);
				
			if(!empty($bsAry))
			{
				# insert records
				foreach($bsAry as $k=>$data)
				{
					$sql = "insert into STUDENT_REGISTRY_BROSIS (StudentID, BS_UserLogin, Relationship, StudentName) values ('$StudentID', '". $data['BS_UserLogin'] . "', '". $data['Relationship'] ."', '". $data['BS_Name'] ."')";
					$this->db_db_query($sql) or die(mysql_error());
				}
			}
		}
		
		function RETRIEVE_BRO_SIS_HK($StudentID='', $StudentID_str='')
		{
			$result = array();
			
			if($StudentID)
			{
				$cond = " StudentID = '$StudentID' ";
			}
			else
			{
				$cond = " StudentID in ($StudentID_str) ";
			}
			
			$sql = "SELECT
						StudentID,
						BS_UserLogin, 
						Relationship, 
						StudentName	
					FROM 
						STUDENT_REGISTRY_BROSIS
					WHERE 
						$cond
					";
			$result = $this->returnArray($sql);
			if($StudentID_str)
			{
				$ary = array();
				# build data array with user id as index
				if(!empty($result))
				{
					for($i=0;$i<sizeof($result);$i++)
					{
						$ary[$result[$i]['StudentID']][] = $result[$i];
					}
				}
				$result = $ary;
			}	
			
			return $result;
		}
		
		function displayPresetCodeSelection($code_type="", $selection_name="", $code_selected="", $noFirst=1, $id="")
		{
			$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and RecordStatus=1 order by DisplayOrder";
			$result = $this->returnArray($sql);
			
			$displayID = $id ? " id=".$id : "";
			
			return getSelectByArray($result, "name=".$selection_name.$displayID, $code_selected,0,$noFirst);
		}
		
		function returnPresetCodeName($code_type="", $selected_code="")
		{
			$sql = "select ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and Code='". $selected_code ."'";
			$result = $this->returnVector($sql);
			return $result[0];
		}
		
		function isNationailityInputByText()
		{
			$byText = 1;
			$bySelection = 0;
			
			# already with NATION_CODE >>> input by selection box
			$sql = "select count(*) from STUDENT_REGISTRY_STUDENT where NATION_CODE != ''";
			$result = $this->returnVector($sql);
			if($result[0])	return $bySelection;
			
			# wihout CODE
			$sql = "select count(*) from INTRANET_USER_PERSONAL_SETTINGS where Nationality != ''";
			$result = $this->returnVector($sql);
			
			## wihout CODE, with text  >>> input by text
			if($result[0])	
				return $byText;
			else
				return $bySelection;
		}
		
		function isStudentRegRecordExists($student_id)
		{
			$sql = "select count(*) from STUDENT_REGISTRY_STUDENT where UserID='".$student_id."'";
			$result = $this->returnVector($sql);
			return $result[0];
		}
		
		function getChildrenForm($id="")
        {
			global $UserID;

			$parentID = $id=="" ? $UserID : $id;
			if ($parentID == "") return array();

			$FormData = array();
			
			$sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$parentID'";
			$child = $this->returnVector($sql);
			if(sizeof($child)>0)
			{
				foreach($child as $k=>$sid)
					$FormData[] = $this->getStudentForm($sid);
			}
			return array_unique(trim_array($FormData));
        }
		
		function getStudentForm($sid="")
        {
			$AcademicYearID = Get_Current_Academic_Year_ID();
			
			$sql = "select b.YearID from YEAR_CLASS_USER as a left join YEAR_CLASS as b on b.YearClassID=a.YearClassID where a.UserID='$sid' and b.AcademicYearID = '$AcademicYearID'";
			$result = $this->returnVector($sql);
			return $result[0];
        }
        
        // Start of Custom Column
		
        function getCustomColumn($columnID = '', $showDeleted = false){
            $cols = "ColumnID, DisplayText_ch, DisplayText_en, DisplayOrder, Code, Section";
            $table = "STUDENT_REGISTRY_CUSTOM_COLUMN";
            $conds = "1";
            if($columnID != '')
                $conds .= " AND ColumnID = '$columnID'";
            if(!$showDeleted)
                $conds .= " AND isDelete = '0'";
            $order = "DisplayOrder asc";
            $sql = "SELECT 
                        $cols
                    FROM
                        $table
                    WHERE
                        $conds
                    ORDER BY
                        $order
            ";
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function getCustomColumnMaxOrder(){
            $col = 'IF(MAX(DisplayOrder), MAX(DisplayOrder), 0)';
            $table = "STUDENT_REGISTRY_CUSTOM_COLUMN";
            $cond = "isDelete = '0'";
            $sql = "SELECT $col FROM $table WHERE $cond";
            $result = $this->returnVector($sql);
            return $result[0];
        }
        
        function checkCustomColumnCode($code, $columnID = ''){
            $table = "STUDENT_REGISTRY_CUSTOM_COLUMN";
            $cond = "Code = '$code' AND isDelete = '0'";
            if($columnID != '')
                $cond .= " AND NOT ColumnID = '$columnID'";
            $sql = "SELECT * FROM $table WHERE $cond";
            $result = $this->returnArray($sql);
            
            return count($result) < 1;
        }
        
        function addCustomColumn($arr){
            $field = array("DisplayText_ch", "DisplayText_en", "DisplayOrder", "Code", "Section");
            $cols = implode(", ", $field);
            $values = "";
            foreach($field as $f){
                if($f == 'DisplayOrder' && $arr[$f] == ''){
                    $arr[$f] = $this->getCustomColumnMaxOrder() + 1;
                }
                $values .= "'".$arr[$f]."',";
            }
            $values = substr($values, 0, -1);
            $table = "STUDENT_REGISTRY_CUSTOM_COLUMN";
            $sql = "INSERT INTO $table ($cols) VALUES ($values)";
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        function updateCustomColumn($arr){
            $field = array("DisplayText_ch", "DisplayText_en", "Code", "Section");
            $updates = "";
            foreach($field as $f){
                $updates .= " $f = '$arr[$f]',";
            }
            $updates = substr($updates, 0, -1);
            $conds = "ColumnID = '$arr[ColumnID]'";
            $table = "STUDENT_REGISTRY_CUSTOM_COLUMN";
            $sql = "UPDATE $table SET $updates WHERE $conds";
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        function deleteCustomColumn($columnIDArr){
            $table = "STUDENT_REGISTRY_CUSTOM_COLUMN";
            $updates = "isDelete = 1";
            $conds = "ColumnID IN ('".implode("','",$columnIDArr)."')";
            $sql = "UPDATE $table SET $updates WHERE $conds";
            $result = $this->db_db_query($sql);
            
            $this->sortCustomColumn();
            return $result;
        }
        
        function orderCustomColumn($arr){
            $table = "STUDENT_REGISTRY_CUSTOM_COLUMN";
            $result = 0;
            foreach($arr as $k => $v){
                $update = "SET DisplayOrder = '$k'";
                $conds = "ColumnID = '$v'";
                $sql = "UPDATE $table $update WHERE $conds";
                $result += $this->db_db_query($sql);
            }
            return $result;
        }
        
        function sortCustomColumn(){
            $table = "STUDENT_REGISTRY_CUSTOM_COLUMN";
            $updates = "a.DisplayOrder =  b.rownum";
            $conds = "isDelete = 0";
            $order = "DisplayOrder asc";
            $table2 = "SELECT  @rownum := @rownum + 1 AS 'rownum', ColumnID
                        FROM $table
                        ,(SELECT @rownum := 0) b
                        WHERE isDelete = '0'
                        ORDER BY DisplayOrder asc";
            $sql = "UPDATE $table a join ($table2) b on a.ColumnID = b.ColumnID
                    SET $updates
                    WHERE $conds";
            $this->db_db_query($sql);
        }
        
        function getCustomColumnValueByUserID($uid){
            $cols = "srcc.ColumnID, srcc.DisplayText_ch, srcc.DisplayText_en, srcc.DisplayOrder, Code, Section";
            $cols .= ", srcv.ValueID, srcv.StudentID, srcv.Value";
            
            $table = "STUDENT_REGISTRY_CUSTOM_COLUMN srcc";
            $join = "STUDENT_REGISTRY_CUSTOM_VALUE srcv
                     ON srcc.ColumnID = srcv.ColumnID";
            
            $cond = "srcc.isDelete = '0' AND srcv.StudentID = '$uid'";
            
            $order = "srcc.DisplayOrder asc";
            
            $sql = "SELECT
                        $cols
                    FROM
                        $table
                        LEFT OUTER JOIN
                        $join
                    WHERE
                        $cond
                    ORDER BY
                        $order
                    ";
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function getCustomValueByUserID($uid){
            $cols = "ValueID, ColumnID, StudentID, Value";
            $table = "STUDENT_REGISTRY_CUSTOM_VALUE";
            $conds = "StudentID = '$uid'";
            
            $sql = "SELECT $cols FROM $table WHERE $conds";
            $result = $this->returnArray($sql);
            
            return $result;
        }
        
        function updateCustomValue($uid, $arr){
            $col = "ColumnID";
            $table = "STUDENT_REGISTRY_CUSTOM_VALUE";
            $sql = "SELECT $col FROM $table WHERE StudentID = '$uid'";
            $valueArr = $this->returnVector($sql);
            
            $updateArr = array();
            foreach($arr as $k => $v){
                if(in_array($v['ColumnID'], $valueArr)){
                    // already existed
                    $updateArr[] = $v;
                    unset($arr[$k]);
                }
            }
            
            $result = array();
            $result['insert'] = $this->insertCustomValue($uid, $arr);
            $result['update'] = 0;
            
            foreach($updateArr as $v){
                $update = "Value = '$v[value]'";
                $conds = "ColumnID = '$v[ColumnID]' AND StudentID = '$uid'";
                $sql = "UPDATE $table SET $update WHERE $conds";
                $res = $this->db_db_query($sql);
                $result['update'] += $res;
            }
            return $result;
        }
        
        function insertCustomValue($uid, $arr){
            $col = "ColumnID, StudentID, Value";
            $table = "STUDENT_REGISTRY_CUSTOM_VALUE";
            $values = "";
            foreach($arr as $val){
                $values .= "("
                                . "'$val[ColumnID]',"
                                . "'$uid',"
                                . "'$val[value]'" .
                           "),";
            }
            $values = substr($values, 0, -1);
            $sql = "INSERT $table ($col) VALUES $values";
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        // End of Custom Column

        // Add record to a table
        // $table = "table_name" ;
        // $dataAry = array( 'field1' => 'data1', field2 => 'data2');
        // $condition = array( 'field1' => 'data1', field2 => 'data2');
        //
        function insert2Table($table, $dataAry = array(), $condition = array(), $run = true, $insertIgnore = false, $updateDateModified = true)
        {
            global $is_debug;

            $result = false;
            $recordID = 0;
            if ($insertIgnore) {
                $IGNORE = " IGNORE ";
            }
            $dbTable = (strpos($table, ".") === false) ? "`{$table}`" : "{$table}";        // include database name or not
            $sql = "INSERT {$IGNORE} INTO {$dbTable} SET ";
            foreach ($dataAry as $field => $value) {
                $sql .= ($value === "now()") ? "`" . $field . "`= now()," : "`" . $field . "`= '" . $this->Get_Safe_Sql_Query($value) . "',";
            }

            if ($updateDateModified) {
                $sql .= "`DateModified`=now(),";
            }
            $sql = substr($sql, 0, - 1); // remove last comma

            if (! empty($condition)) {
                foreach ($condition as $field => $value) {
                    $tmp_cond[] = "`{$field}` = '" . $this->Get_Safe_Sql_Query($value) . "'";
                }
                $sql .= " WHERE " . implode(' AND ', $tmp_cond);
            }

            if ($is_debug) {
                echo $sql . '<br>';
                return;
            } else {
                if ($run) {
                    $result = $this->db_db_query($sql);
                    if ($result) {
                        $recordID = $this->db_insert_id();
                    }
                    return $recordID ? $recordID : $result;
                } else {
                    return $sql;
                }
            }
        }

        // Update a table
        // $table = "table_name" ;
        // $dataAry = array( 'field1' => 'data1', field2 => 'data2');
        // $condition = array( 'field1' => 'data1', field2 => 'data2');
        //
        function update2Table($table, $dataAry = array(), $condition = array(), $run = true, $updateDateModified = true)
        {
            global $UserID;
            if (! is_array($dataAry)) {
                return false;
            }

            $dbTable = (strpos($table, ".") === false) ? "`{$table}`" : "{$table}";        // include database name or not
            $sql = "UPDATE {$dbTable} SET ";

            foreach ($dataAry as $field => $value) {
                $sql .= ($value === "now()") ? "`" . $field . "`= now()," : "`" . $field . "`= '" . $this->Get_Safe_Sql_Query($value) . "',";
            }

            if ($updateDateModified) {
                $sql .= "`DateModified`=now(),";
            }
            $sql = substr($sql, 0, - 1); // remove last comma

            if (! empty($condition)) {
                foreach ($condition as $field => $value) {
                    $tmp_cond[] = "`{$field}` = '" . $this->Get_Safe_Sql_Query($value) . "'";
                }
                $sql .= " WHERE " . implode(' AND ', $tmp_cond);
            }

            if ($run) {
                $result = $this->db_db_query($sql);
                return $result;
            } else {
                return $sql;
            }
        }

        function isSubmitted($userID, $academicYearID)
        {
            $userID = IntegerSafe($userID);
            $academicYearID = IntegerSafe($academicYearID);
            $sql = "SELECT DATA_CONFIRM_DATE FROM STUDENT_REGISTRY_SUBMITTED WHERE UserID='".$userID."' AND AcademicYearID='".$academicYearID."'";
            $result = $this->returnResultSet($sql);
            return count($result) ? true : false;
        }
        
        # Sync to Student Account
        
        function syncToAccountPersonalSetting($uid, $dataAry_PersonalSetting=array())
        {
        	include_once('libaccountmgmt.php');
        	$la = new libaccountmgmt();
//         	debug_pr($dataAry_PersonalSetting);die();
        	$result = $la->updateUserPersonalSetting($uid, $dataAry_PersonalSetting, $isSync = true);
        	return $result;
        }
        
        function syncToAccountUserInfo($uid, $dataAry = array()){
        	include_once('libaccountmgmt.php');
        	$la = new libaccountmgmt();
        	$result = $la->updateUserInfo($uid, $dataAry, $isSync = true);
        	return $result;
        }
        
        function syncToAccountMainGuardian($sid, $dataAry){
        	global $UserID, $eclass_db, $ec_guardian;
        	# Find existing guardian
        	$table = $eclass_db . '.GUARDIAN_STUDENT';
        	$table_ext = $eclass_db . '.GUARDIAN_STUDENT_EXT_1';
        	$sql = "SELECT RecordID FROM $table WHERE UserID = '$sid' AND isMain=1";
        	$recordID = $this->returnVector($sql);
        	$recordID = $recordID[0];
        	$sql = "SELECT ExtendRecordID FROM $table_ext WHERE RecordId = '$recordID'";
        	$extRecordID = $this->returnVector($sql);
        	$extRecordID = $extRecordID[0];
        	
        	$relation = array_search($dataAry['G_RELATION'],$ec_guardian);
        	if(!$relation){
        		$relation = array_keys($ec_guardian);
        		$relation = $relation[sizeof($relation)-1];
        	}
        	if($recordID){
        		// update record
        		$colVals = "
					ChName = '".$dataAry['NAME_C']."',
					EnName = '".$dataAry['NAME_E']."',
					Phone = '".$dataAry['TEL']."',
					EmPhone = '".$dataAry['MOBILE']."',
					Address = '".$dataAry['G_ROAD'] . '\n' . $dataAry['G_ADDRESS']."',
					Relation = '".$relation."',
					IsMain = '1',
					ModifiedDate = NOW()
				";
        		$sql = "UPDATE $table
        					SET $colVals
        				WHERE RecordID = '$recordID'";
        		$result['record'] = $this->db_db_query($sql);
        	} else {
        		// insert record
        		$cols = "UserID, ChName, EnName, Phone, EmPhone, Address, Relation, IsMain, IsSMS, InputDate, ModifiedDate";
        		$vals = "'$sid', 
							'".$dataAry['NAME_C']."', 
							'".$dataAry['NAME_E']."', 
							'".$dataAry['TEL']."', 
							'".$dataAry['MOBILE']."',
							'".$dataAry['G_ROAD'] . '\n' . $dataAry['G_ADDRESS']."',
							'".$ec_guardian[$relation]."',
							'0', NOW(), NOW()
						";
        		$sql = "INSERT INTO $table ($cols) VALUES ($vals)";
        		$result['record'] = $this->db_db_query($sql);
        		$recordID = mysql_insert_id();
        	}
//         	debug_pr($sql);
        	
        	if($extRecordID){
        		$colVals = "
					IsLiveTogether = '".$dataAry['LIVE_SAME']."',
					Occupation = '".$dataAry['PROF']."',
					ModifiedDate = NOW()
				";
        		// update record
        		$sql = "UPDATE $table_ext
        		SET $colVals
        		WHERE RecordID = '$recordID'";
        		$result['record'] = $this->db_db_query($sql);
        	} else {
        		// insert record
        		$cols = "UserID,RecordID, IsLiveTogether, IsEmergencyContact, Occupation, InputDate, ModifiedDate";
        		$vals = "'$sid','$recordID','".$dataAry['LIVE_SAME']."', '0', '".$dataAry['PROF']."', NOW(), NOW()";
        		$sql = "INSERT INTO $table_ext ($cols) VALUES ($vals)";
        		$result['record'] = $this->db_db_query($sql);
        		$recordID = mysql_insert_id();
        	}
//         	debug_pr($sql);
//         	die();
        }
	}	
	
}        // End of directive
?>