<?php
include_once('libperiods.php');
include_once('libuser.php');

class libperiods_ui extends libperiods {
	
	function libperiods_ui(){
		$this->libperiods();
	}
	
	function initJavascript()
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
					
		$x = '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/script.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.datepick.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.jeditable.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.colorPicker.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.tablednd_0_5.js"></script>
				<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.colorPicker.css" type="text/css" />
				<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.css" type="text/css" />
				<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />
				<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.datepick.css" type="text/css" />';
				
		return $x;
	}
	
	function periodsCalendar()
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once("libdb.php");
		include_once("libinterface.php");
		include_once("form_class_manage.php");
		include_once("libcal.php");
		include_once("libcalevent.php");
		include_once("libcalevent2007a.php");
		
		$linterface = new interface_html();
		$lfcm = new form_class_manage();
		$libcalevent = new libcalevent2007();
		
		$academic_year_selection = getSelectAcademicYear("academic_year","onChange='ChangeSchoolYear(this.value);'", 1);
		
		$x .= $this->initJavascript();
		
		//$lastPublishDate = $this->getLastPublishDate();
		//$lastPublishDate = ($lastPublishDate == '' ? '-' : $lastPublishDate);
		
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
		$x .= "<tr><td align='left'>".$Lang['General']['SchoolYear']." : ".$academic_year_selection."</td></tr>";
		$x .= "<tr><td align='left'><span class='tabletextremark'>".$Lang['SysMgr']['CycleDay']['LastPublishDate'].":".$lastPublishDate."</span></td></tr>";
		$x .= "</table>";
		
		$x .= "<div id='loadingSpan' style='display:none'>";
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
		$x .= "<tr><td colspan='4' align='center'>".$Lang['General']['Loading']."</td></tr>";
		$x .= "</table>";
		$x .= "</div>";
		
		$x .= "<div id='periodCalendar'>";
		$x .= $this->loadPeriodsCalendar($current_school_year_id);
		$x .= "</div>";
		return $x;
	}
	
	function loadPeriodsCalendar($SchoolYearID='')
	{
		include_once("libdb.php");
		include_once("form_class_manage.php");
		include_once("libcycleperiods.php");
		include_once("libcal.php");
		include_once("libcalevent.php");
		include_once("libcalevent2007a.php");
		include_once("libinterface.php");
		
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;

		$lfcm = new form_class_manage();
		$libinterface = new interface_html();
		$libcalevent = new libcalevent2007();
		
		if($val == ""){
			$val = $lfcm->getCurrentAcademicaYearID();
		}
		
		$arrResult = $lfcm->Get_Academic_Year_List();		
		if(sizeof($arrResult)>0){
			for($i=0; $i<sizeof($arrResult); $i++){
				list($AcademicYearID, $YearNameEN, $YearNameB5, $Sequence, $CurrentSchoolYear, $StartDate, $EndDate) = $arrResult[$i];
				
				if($AcademicYearID==$val){
					$StartYear = substr($StartDate,0,4);
					$StartMonth = substr($StartDate,5,2);
				}
			}
		}
		
		$month = intval($StartMonth);
		$year = intval($StartYear); // get Current Year
		
		for($i=1; $i<=12; $i++)
		{
			$arrDate[$i]['Month'] = $month;
			$arrDate[$i]['Year'] = $year;
			$month = $month + 1;
			if($month > 12){
				$month = $month - 12;
				$year = $year + 1;	
			}
		}

		for($i=1; $i<=12; $i++){
			$month = $arrDate[$i]['Month'];
			$year = $arrDate[$i]['Year'];
			${calendarTable.$i} = $libcalevent->displayCalandar_PeriodView($month, $year);
		}
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
		$x .= "<tr valign=top>";
		$x .= "<td width=\"25%\">".$calendarTable1."</td>";
		$x .= "<td width=\"25%\">".$calendarTable2."</td>";
		$x .= "<td width=\"25%\">".$calendarTable3."</td>";
		$x .= "<td width=\"25%\">".$calendarTable4."</td>";
		$x .= "</tr>";
		$x .= "<tr valign=top>";
		$x .= "<td width=\"25%\">".$calendarTable5."</td>";
		$x .= "<td width=\"25%\">".$calendarTable6."</td>";
		$x .= "<td width=\"25%\">".$calendarTable7."</td>";
		$x .= "<td width=\"25%\">".$calendarTable8."</td>";
		$x .= "</tr>";
		$x .= "<tr valign=top>";
		$x .= "<td width=\"25%\">".$calendarTable9."</td>";
		$x .= "<td width=\"25%\">".$calendarTable10."</td>";
		$x .= "<td width=\"25%\">".$calendarTable11."</td>";
		$x .= "<td width=\"25%\">".$calendarTable12."</td>";
		$x .= "</tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	function loadPeriodsForm($StartDate)
	{	
		include_once("libinterface.php");
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $LANG;
		
		$linterface = new interface_html();
		
		$x .= "<br>";
		$x .= "<center>";
		$x .= $linterface->Get_Thickbox_Return_Message_Layer();
		$x .= "<table width='80%' border='1' cellpadding='3' cellspacing='0'>";
		$x .= "<div id='PeriodNameWarningLayer' style='display:none; color:red;'></div>";
		$x .= "<tr><td width='40%' align='left'>Name</td><td align='left'>
						<div id='period_name' 
						style='background-image:url(\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/icon_edit_b.gif\");
		          		background-position: center right;
		          		background-repeat: no-repeat;
		          		clear: both;'
		          		class='jEditInput'></div></td></tr>";
		$x .= "<tr><td width='40%' align='left'>Bg Color</td><td align='left'><input id='bgColorCode' name='bgColorCode' type='text' value='#ffffff' onChange='js_Change_BgColor()'/></td></tr>";
		$x .= "<input type='hidden' name='periodID' id='periodID' value=''>";
		$x .= "</table>";
		$x .= "<br>";
		$x .= $this->retrivePeriodDateRangeTable();
		$x .= "<br>";
		$x .= $this->retrivePeriodTimeSessionTable();
		$x .= "</center>";
		
		return $x;
	}
	
	function retrivePeriodDateRangeTable()
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once("libinterface.php");
		
		$libinterface = new interface_html();
		
		//$libBuilding = new Building($BuildingID);
		//$floorInfoArr = $libBuilding->Get_All_Floor();
		//$numOfDateRange = count($floorInfoArr);
		
		$x = "";
		$x .= '<table id="DateRangeTable" class="common_table_list" style="width:80%">';
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th style="width:40%">Date</th>';
					$x .= '<th style="width:20%">From</th>';
					$x .= '<th style="width:20%">To</th>';
					$x .= '<th>&nbsp;</th>';
				$x .= '</tr>';
			$x .= '</thead>';
			
			$x .= '<tbody>';
			
			# Display Floor Info
			if ($numOfDateRange > 0)
			{
				$jEditStyle = $this->Get_jEdit_Style();
				$moveToolTips = $Lang['SysMgr']['Location']['ToolTip']['MoveToArrangeDisplayOrder'];
				$deleteToolTips = $Lang['SysMgr']['Location']['ToolTip']['Delete'];
				
				for ($i=0; $i<$numOfDateRange; $i++)
				{
					$thisLocationLevelID = $floorInfoArr[$i]['LocationLevelID'];
					$thisCode = $floorInfoArr[$i]['Code'];
					$thisTitleEn = $floorInfoArr[$i]['NameEng'];
					$thisTitleCh = $floorInfoArr[$i]['NameChi'];
					
					# show delete icon if the building has no linked floor
					$thisLibFloor = new Floor($thisLocationLevelID);
					$roomInfoArr = $thisLibFloor->Get_All_Room();
					$numOfRoom = count($roomInfoArr);
					$showDeleteIcon = ($numOfRoom==0)? true : false;
					
					$x .= '<tr id="FloorRow_'.$thisLocationLevelID.'">';
						# Code
						$x .= '<td>';
							$x .= $libinterface->Get_Thickbox_Edit_Div("FloorCode_".$thisLocationLevelID, "FloorCode_".$thisLocationLevelID, "jEditCode", $thisCode);
							$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv_".$thisLocationLevelID);
						$x .= '</td>';
						# Title (English)
						$x .= '<td>';
							$x .= $libinterface->Get_Thickbox_Edit_Div("FloorTitleEn_".$thisLocationLevelID, "FloorTitleEn_".$thisLocationLevelID, "jEditTitleEn", $thisTitleEn);
						$x .= '</td>';
						# Title (Chinese)
						$x .= '<td>';
							$x .= $libinterface->Get_Thickbox_Edit_Div("FloorTitleCh_".$thisLocationLevelID, "FloorTitleCh_".$thisLocationLevelID, "jEditTitleCh", $thisTitleCh);
						$x .= '</td>';
						
						# Functional icons
						$x .= '<td class="Dragable" align="left">';
							# Move
							$x .= $libinterface->GET_LNK_MOVE("#", $moveToolTips);
							# Delete
							$x .= $libinterface->GET_LNK_DELETE("#", $deleteToolTips, "js_Delete_Floor('$thisLocationLevelID'); return false;");
						$x .= '</td>';
					$x .= '</tr>';
				}
			}
			
				# Add Floor Button
				$x .= '<tr id="AddDateRange" class="nodrop nodrag">';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>';
						$x .= '<div class="table_row_tool row_content_tool">';
							$x .= '<a href="#" onclick="jsAddDateRangeRow(); return false;" class="add_dim" title="'.$Lang['SysMgr']['Location']['New']['Floor'].'"></a>';
						$x .= '</div>';
					$x .= '</td>';
				$x .= '</tr>';
				
			$x .= '</tbody>';
		$x .= '</table>';
			
		return $x;
	}
	
	function retrivePeriodTimeSessionTable()
	{
	}
}

?>