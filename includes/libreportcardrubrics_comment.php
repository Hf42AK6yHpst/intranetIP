<?php
#  Editing by 
/*
 *	Date : 2018-03-29 (Bill)    [2017-1102-1135-27054]
 *          modified Get_Form_Selection(), Get_Settings_CommentBank_New_Edit_UI(), Get_Statistics_ClassTopicPerformance_Flash_Chart(), to fix PHP 5.4 problem of trim()
 */

class libreportcardrubrics_comment extends libreportcardrubrics {
	
	/**
	 * Constructor
	 */
	function libreportcardrubrics_comment() {
		parent:: libreportcardrubrics();
	}
	
	function Get_Class_Teacher_Comment_Info($ReportID, $YearClassID)
	{
		$RC_TEACHER_COMMENT = $this->Get_Table_Name("RC_TEACHER_COMMENT");
		
		$sql = "
			SELECT
				count(ycu.UserID) as TotalStudent,
				count(rtc.Comment) as TotalComment
			FROM
				YEAR_CLASS_USER ycu
				INNER JOIN YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."' AND yc.YearClassID = '$YearClassID'
				LEFT JOIN $RC_TEACHER_COMMENT rtc ON rtc.StudentID = ycu.UserID AND rtc.Comment <> '' AND rtc.Comment IS NOT NULL AND rtc.ReportID = '$ReportID' 
			GROUP BY
				ycu.YearClassID
		";
		
		$result = $this->returnArray($sql);
		
		return $result[0];
		//return BuildMultiKeyAssoc($result,array("YearClassID","ReportID"));
	}
	
	function Get_Class_Teacher_Comment_Latest_Modification($ReportID, $YearClassID='')
	{
		if(trim($YearClassID) != '')
			$cond_YearClassID= " AND ycu.YearClassID = '$YearClassID' ";
		
		$NameField = getNameFieldByLang("iu.");
		
		$RC_TEACHER_COMMENT = $this->Get_Table_Name("RC_TEACHER_COMMENT");
		
		$sql = "
			SELECT
				$NameField AS LastModifiedBy,
				rtc.DateModified
			FROM 
				$RC_TEACHER_COMMENT rtc
				INNER JOIN YEAR_CLASS_USER ycu ON rtc.StudentID = ycu.UserID
				INNER JOIN INTRANET_USER iu ON iu.UserID = rtc.LastModifiedBy
			WHERE
				rtc.ReportID = '$ReportID'
				$cond_YearClassID
			ORDER BY 
				rtc.DateModified DESC
			LIMIT 1
		";
		
		
		$result = $this->returnArray($sql);
		return $result[0];
	}
	
	function Get_Comment_Bank_Comment($keyword='', $TopicIDArr=0, $CommentIDArr='', $returnAllSubjectComment=false)
	{
		$RC_COMMENT_BANK = $this->Get_Table_Name("RC_COMMENT_BANK");
		
		if(trim($keyword)!='') {
			$cond_Keyword = "
				AND 
					(
						CommentCode LIKE '%".$this->Get_Safe_Sql_Query($keyword)."%'
						OR CommentEn LIKE '%".$this->Get_Safe_Sql_Query($keyword)."%'
						OR CommentCh LIKE '%".$this->Get_Safe_Sql_Query($keyword)."%' 
					)
			";
		}

		$cond_CommentID = '';
		//if (trim($CommentIDArr) != '')
		if(!is_array($CommentIDArr) && trim($CommentIDArr) != '' || is_array($CommentIDArr)) {
			$cond_CommentID = " And CommentID In ( ".implode(',', (array)$CommentIDArr)." ) ";
		}
			
		if ($returnAllSubjectComment === '') {
			// return all comments
		}
		else if ($returnAllSubjectComment) {
			$cond_TopicID = " And TopicID > 0 ";
		}
		else {
			$cond_TopicID = " And TopicID In ('".implode("','", (array)$TopicIDArr)."') ";
		}
		
			
		$sql = "
			SELECT
				CommentID,
				CommentCode,
				IF(CommentCode LIKE '%".$this->Get_Safe_Sql_Query($keyword)."%' OR CommentEn LIKE '%".$this->Get_Safe_Sql_Query($keyword)."%',CommentEn,NULL) AS CommentEn,
				IF(CommentCode LIKE '%".$this->Get_Safe_Sql_Query($keyword)."%' OR CommentCh LIKE '%".$this->Get_Safe_Sql_Query($keyword)."%',CommentCh,NULL) AS CommentCh,
				CommentID,
				TopicID
			FROM 
				$RC_COMMENT_BANK 
			WHERE 
				RecordStatus = 1
				$cond_Keyword
				$cond_CommentID
				$cond_TopicID
		";
		
		$result = $this->returnResultSet($sql);
		
		return $result;
	}
	
	function Insert_Comment($ReportID,$StudentCommentArr, $TopicID)
	{
		$RC_TEACHER_COMMENT =  $this->Get_Table_Name("RC_TEACHER_COMMENT");
		
		foreach((array)$StudentCommentArr as $thisStudentID => $thisComment)
		{
			if(trim($thisComment)=='')
				continue;
			$ValuesArr[] = "('$thisStudentID','$ReportID','$TopicID','".$this->Get_Safe_Sql_Query($thisComment)."',NOW(),'".$_SESSION['UserID']."', NOW(),'".$_SESSION['UserID']."')\n";
		}
		
		if(!empty($ValuesArr))
		{
			$ValuesSql = implode(",",(array)$ValuesArr);
			$this->Start_Trans();
			
			$sql = "
				INSERT INTO
					$RC_TEACHER_COMMENT
					(StudentID,	ReportID, TopicID, Comment, DateInput, InputBy, DateModified, LastModifiedBy)
				VALUES
					$ValuesSql
			";
			
			$success = $this->db_db_query($sql);
			
			if(!$success)
			{
//				debug_pr(mysql_error());
				$this->RollBack_Trans();
			}
			else
			{
				$this->Commit_Trans();
			}
			
			return $success;
		}
		else
			return true;
		
	}
	
	function Update_Comment_By_CommentID($ReportID,$StudentCommentArr)
	{
		$RC_TEACHER_COMMENT =  $this->Get_Table_Name("RC_TEACHER_COMMENT");
		
		$this->Start_Trans();
		foreach((array)$StudentCommentArr as $TeacherCommentID => $thisComment)
		{
			$sql = "
				UPDATE
					$RC_TEACHER_COMMENT
				SET
					Comment = '".$this->Get_Safe_Sql_Query($thisComment)."'
				WHERE
					TeacherCommentID = '$TeacherCommentID'
			";	
			
			$Success[$TeacherCommentID] = $this->db_db_query($sql);
		}
		
		if(in_array(false,$Success))
		{
//			debug_pr(mysql_error());
//			debug_pr($Success);
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	function Replace_Comment($ReportID, $StudentCommentArr, $TopicID)
	{
		if(empty($StudentCommentArr) || empty($ReportID)|| trim($TopicID)=='')
			return false;
			
		$StudentIDArr = array_keys($StudentCommentArr);
		
		$this->Start_Trans();
		
		$Success["DeleteComment"] = $this->Delete_Comment($ReportID, $StudentIDArr, $TopicID);
		$Success["InsertComment"] = $this->Insert_Comment($ReportID, $StudentCommentArr, $TopicID);
		
		if(in_array(false,$Success))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	function Delete_Comment($ReportID, $StudentIDArr, $TopicID)
	{
		
		if(empty($StudentIDArr) || empty($ReportID)|| trim($TopicID)=='')
			return false;
			
		$RC_TEACHER_COMMENT =  $this->Get_Table_Name("RC_TEACHER_COMMENT");
		
		$StudentIDSql = implode(",",(array)$StudentIDArr);
		$sql = "
			DELETE FROM
				$RC_TEACHER_COMMENT
			WHERE
				ReportID = '$ReportID'
				AND StudentID IN ($StudentIDSql)
				AND TopicID = '$TopicID'
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;
	}
	
	function Append_Class_Teacher_Comment($ReportID, $SelectedUserIDArr, $CommentIDArr)
	{
		$CommentArr = $this->Get_Comment_Bank_Comment('',0,$CommentIDArr);
		
		foreach((array)$CommentArr as $thisComment)
			$CommentStr .= "\n".Get_Lang_Selection($thisComment['CommentCh'],$thisComment['CommentEn']);
		
		$OriCommentArr = $this->Get_Student_Class_Teacher_Comment($ReportID,$SelectedUserIDArr,0);
		
		$this->Start_Trans();
		foreach((array)$OriCommentArr as $OriCommentDetail)
		{
			list($TeacherCommentID,$StudentID, $ReportID, $TopicID, $OriComment) = $OriCommentDetail;
			$UpdateStudentCommentArr[$TeacherCommentID] = $OriComment.$CommentStr;
			
			$UpdatedStudent[] = $StudentID;
		}
		if(isset($UpdateStudentCommentArr))
			$Success['Update'] = $this->Update_Comment_By_CommentID($ReportID,$UpdateStudentCommentArr);

		foreach((array)$SelectedUserIDArr as $StudentID)
		{
			if(in_array($StudentID,(array)$UpdatedStudent))
				continue;
				
			$InsertStudentCommentArr[$StudentID] = $CommentStr;
		}
		
		if(isset($InsertStudentCommentArr))
			$Success['Insert'] = $this->Insert_Comment($ReportID,$InsertStudentCommentArr,0);
		
		if(in_array(false,$Success))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}	
	}
	
	function Get_Settings_CommentBank_DBTable_Sql($Keyword='', $ForExport=0, $commentType='', $IncludeSubjectIDArr='', $FilterTopicStr='')
	{
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
		
		$CommentField = Get_Lang_Selection("CommentCh", "CommentEn");
		$RC_COMMENT_BANK = $this->Get_Table_Name("RC_COMMENT_BANK");
		
		if(trim($Keyword) != '')
		{
			$cond_Keyword = " AND (
									CommentCode LIKE '%".$this->Get_Safe_Sql_Query($Keyword)."%' 
									Or
									$CommentField LIKE '%".$this->Get_Safe_Sql_Query($Keyword)."%' 
								  )
							";
		}
		
		$EditPath = 'edit.php?';

		if($commentType=='subject')
		{
			$SelectCommentID = 'CommentID,';
			$SelectTopicID = 'TopicID,';
			$EditPath = 'edit.php?commentType=subject&';
			$cond_topicID = " And TopicID > 0 ";
			
			$SelectTopicID_Export = ',TopicID';
		}
		else if ($commentType=='class')
		{
			$EditPath = 'edit.php?commentType=class&';
			$cond_topicID = " And TopicID = '0' ";
		}		
		
		if ($IncludeSubjectIDArr!='' || $FilterTopicStr !='') {
			$lreportcard_topic = new libreportcardrubrics_topic();
			$FilterTopicID = $lreportcard_topic->Get_TopicID_By_Filtering_Text($FilterTopicStr, $IncludeSubjectIDArr);
			$cond_FilterTopicID = " And TopicID In ('".implode("','", (array)$FilterTopicID)."') ";
		}
		
		
		
		if ($ForExport == 0)
		{
			$sql  = "SELECT 
						CONCAT('<a class=\'tablelink\' href=\'".$EditPath."CommentID=', CommentID, '\'>', CommentCode, '</a>'),
						CONCAT('<a class=\'tablelink\' href=\'".$EditPath."CommentID=', CommentID, '\'>', REPLACE($CommentField, '"."//"."', ''), '</a>'),
						".$SelectCommentID."
						".$SelectTopicID."
						CONCAT('<input type=\'checkbox\' name=\'CommentID[]\' value=\'', CommentID ,'\'>')
					FROM
						$RC_COMMENT_BANK
					WHERE
						RecordStatus = 1
						$cond_topicID
						$cond_Keyword
						$cond_FilterTopicID
					 ";
		}
		else if ($ForExport == 1)
		{
			
			$sql  = "SELECT 
						CommentCode,
						CommentCh,
						CommentEn
						$SelectTopicID_Export
					FROM
						$RC_COMMENT_BANK
					WHERE
						RecordStatus = 1
						$cond_topicID
						$cond_Keyword
						$cond_FilterTopicID
					 ";
		}	 
				 
		return $sql;
	}
	
	function Insert_Comment_Bank_Comment($CommentCode,$CommentEn,$CommentCh, $TopicID)
	{
		$CommentCode = $this->Get_Safe_Sql_Query($CommentCode);
		$CommentEn = $this->Get_Safe_Sql_Query($CommentEn);
		$CommentCh = $this->Get_Safe_Sql_Query($CommentCh);
		
		$RC_COMMENT_BANK = $this->Get_Table_Name("RC_COMMENT_BANK");
		
		$sql ="
			INSERT INTO
				$RC_COMMENT_BANK
				(CommentCode,CommentEn,CommentCh,TopicID,RecordStatus,DateInput,InputBy,DateModified,LastModifiedBy)
				VALUES
				('$CommentCode','$CommentEn','$CommentCh','$TopicID',1,NOW(),'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."')
		";
		
		$success = $this->db_db_query($sql);
		
		return $success;
		
	}
	
	function Update_Comment_Bank_Comment($CommentID, $CommentDataArr)
	{
		foreach((array)$CommentDataArr as $Field => $Value)
		{
			$CommentSqlArr[] = $Field." = '".$this->Get_Safe_Sql_Query($Value)."' "; 
		}
		$CommentSql = implode(",",$CommentSqlArr);
		
		$RC_COMMENT_BANK = $this->Get_Table_Name("RC_COMMENT_BANK");
		$sql = "
			UPDATE
				$RC_COMMENT_BANK
			SET
				$CommentSql
			WHERE
				CommentID = '$CommentID'
		"; 
		
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	
	function Validate_Comment_Code($CommentCode, $CommentID='')
	{
		if(trim($CommentID)!='')
			$cond_CommentID = " AND CommentID <> '$CommentID' ";
		
		$RC_COMMENT_BANK = $this->Get_Table_Name("RC_COMMENT_BANK");
		$sql = "
			SELECT
				COUNT(*)
			FROM
				$RC_COMMENT_BANK
			WHERE
				CommentCode = '".$this->Get_Safe_Sql_Query($CommentCode)."'
				$cond_CommentID 
		";
		
		$result = $this->returnVector($sql);
		
		return $result[0]>0?false:true;
	}
	
	function Remove_Comment_Bank_Comment($CommentIDArr)
	{
		$CommentIDSql = implode(",",(array)$CommentIDArr);
		
		$RC_COMMENT_BANK = $this->Get_Table_Name("RC_COMMENT_BANK");
		$sql = "
			UPDATE
				$RC_COMMENT_BANK
			SET
				RecordStatus = 0
			WHERE
				CommentID IN ($CommentIDSql) 
		";
		
		$success = $this->db_db_query($sql);
		
		return $success;
	}
}

?>