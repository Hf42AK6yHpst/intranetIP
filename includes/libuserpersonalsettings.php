<?
# using: yat

#####################################
#	Date:	2013-04-11	YatWoon
#			update Save_Setting(), use $this_UserID instead of $UserID
#####################################

class libuserpersonalsettings extends libdb
{
	function libuserpersonalsettings()
	{
		$this->libdb();
	}
	
	function Get_Setting($UserID,$FieldAry=array()) 
	{
		$Fields = implode(",",$FieldAry);
		if(!empty($Fields))
		{
			$sql = "select 
								$Fields
							from 
								INTRANET_USER_PERSONAL_SETTINGS 
							where 
								UserID = $UserID";
	 		$Setting = $this->returnArray($sql); 
	 		return $Setting[0];
		}
	}
	
	function Save_Setting($this_UserID, $FieldAry=array()) 
	{
		### check user record exists or not
		$sql = "select count(*) from INTRANET_USER_PERSONAL_SETTINGS where UserID=$this_UserID";
		$result = $this->returnVector($sql);
		if(!$result[0])
		{
			### Add user record first
			$sql = "insert into INTRANET_USER_PERSONAL_SETTINGS (UserID) values ($this_UserID)";
			$this->db_db_query($sql);
		}
		
		### rebuild sql 
		$fields_str = array();
		foreach($FieldAry as $f=>$d)
		{
			### Field
			$fields_str[] = $f . " = '".  $d ."' ";
			
			### DateModified
			$fields_str[] = $f . "_DateModified = now() ";
		}
		
		$sql = "update INTRANET_USER_PERSONAL_SETTINGS set "; 
		$sql .= implode(",",$fields_str);
		$sql .= " where UserID=$this_UserID";
		$this->db_db_query($sql) or die(mysql_error());
	}
	
	
	
}
?>