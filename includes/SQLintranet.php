<?php
# MySQL dump 8.8
#
# Host: localhost    Database: intranet
#--------------------------------------------------------
# Server version     3.23.22-beta-log

#
# Table structure for table 'INTRANET_ANNOUNCEMENT'
#

$sql_table[] = "CREATE TABLE INTRANET_ANNOUNCEMENT (
  AnnouncementID int(8) NOT NULL auto_increment,
  Title varchar(100),
  Description text,
  AnnouncementDate datetime,
  EndDate datetime default NULL,
  ReadFlag text,
  UserID int(8),
  PosterName varchar(255),
  Attachment varchar(255) default NULL,
  Internal INT,
  SystemAdminLogin varchar(20),
  RecordType char(2),
  RecordStatus char(2),
  OwnerGroupID int(11) default NULL,
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (AnnouncementID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_BOOKING'
#

$sql_table[] = "CREATE TABLE INTRANET_BOOKING (
  BookingID int(8) NOT NULL auto_increment,
  ResourceID int(8),
  UserID int(8),
  DateStart datetime,
  DateEnd datetime,
  Remark text,
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (BookingID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_BOOKING_PERIOD'
#

$sql_table[] = "CREATE TABLE INTRANET_BOOKING_PERIOD (
  BookingPeriodID int(8) NOT NULL auto_increment,
  TimeStart int(8),
  TimeEnd int(8),
  Title varchar(255),
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (BookingPeriodID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_EVENT'
#

$sql_table[] = "CREATE TABLE INTRANET_EVENT (
  EventID int(8) NOT NULL auto_increment,
  Title varchar(255),
  Description text,
  EventDate datetime,
  EventVenue varchar(100),
  EventNature varchar(100),
  ReadFlag text,
  UserID int(8),
  isSkipCycle TINYINT,
  Internal int(11),
  RecordType char(2),
  RecordStatus char(2),
  OwnerGroupID int(11) default NULL,
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (EventID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_GROUP'
#

$sql_table[] = "CREATE TABLE INTRANET_GROUP (
  GroupID int(8) NOT NULL auto_increment,
  Title varchar(100),
  Description text,
  URL varchar(255) default NULL,
  StorageQuota INT,
  AnnounceAllowed char(2),
  FunctionAccess INT,
  RecordType INT,
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (GroupID),
  UNIQUE Title (Title)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_GROUPANNOUNCEMENT'
#

$sql_table[] = "CREATE TABLE INTRANET_GROUPANNOUNCEMENT (
  GroupAnnouncementID int(8) NOT NULL auto_increment,
  GroupID int(8),
  AnnouncementID int(8),
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (GroupAnnouncementID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_GROUPEVENT'
#

$sql_table[] = "CREATE TABLE INTRANET_GROUPEVENT (
  GroupEventID int(8) NOT NULL auto_increment,
  GroupID int(8),
  EventID int(8),
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (GroupEventID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_GROUPPOLLING'
#

$sql_table[] = "CREATE TABLE INTRANET_GROUPPOLLING (
  GroupPollingID int(8) NOT NULL auto_increment,
  GroupID int(8),
  PollingID int(8),
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (GroupPollingID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_GROUPRESOURCE'
#

$sql_table[] = "CREATE TABLE INTRANET_GROUPRESOURCE (
  GroupResourceID int(8) NOT NULL auto_increment,
  GroupID int(8),
  ResourceID int(8),
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (GroupResourceID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_GROUPTIMETABLE'
#

$sql_table[] = "CREATE TABLE INTRANET_GROUPTIMETABLE (
  GroupTimetableID int(8) NOT NULL auto_increment,
  GroupID int(8),
  TimetableID int(8),
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (GroupTimetableID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_HOMEWORK'
#

$sql_table[] = "CREATE TABLE INTRANET_HOMEWORK (
  HomeworkID int(11) NOT NULL auto_increment,
  ClassGroupID int(11) NOT NULL default '0',
  SubjectID int(11) NOT NULL default '0',
  PosterUserID int(11) NOT NULL default '0',
  PosterName varchar(255),
  StartDate date NOT NULL default '0000-00-00',
  DueDate date NOT NULL default '0000-00-00',
  Loading float NOT NULL default '0',
  Title varchar(150) NOT NULL default '',
  Description text,
  AttachmentPath varchar(255),
  HandinRequired int(11),
  CollectRequired int(11),
  HandinConfirmUserID int(11),
  HandinConfirmTime datetime,
  TypeID int(11),
  RecordType char(2),
  LastModified datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (HomeworkID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_POLLING'
#

$sql_table[] = "CREATE TABLE INTRANET_POLLING (
  PollingID int(8) NOT NULL auto_increment,
  Question varchar(255),
  AnswerA varchar(255),
  AnswerB varchar(255),
  AnswerC varchar(255),
  AnswerD varchar(255),
  AnswerE varchar(255),
  AnswerF varchar(255),
  AnswerG varchar(255),
  AnswerH varchar(255),
  AnswerI varchar(255),
  AnswerJ varchar(255),
  Reference varchar(255),
  DateStart datetime,
  DateEnd datetime,
  DateRelease date,
  ReadFlag text,
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (PollingID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_POLLINGRESULT'
#

$sql_table[] = "CREATE TABLE INTRANET_POLLINGRESULT (
  PollingResultID int(8) NOT NULL auto_increment,
  PollingID int(8),
  UserID int(8),
  Answer char(2),
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (PollingResultID),
  INDEX PollingID (PollingID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_RESOURCE'
#

$sql_table[] = "CREATE TABLE INTRANET_RESOURCE (
  ResourceID int(8) NOT NULL auto_increment,
  ResourceCode varchar(50),
  ResourceCategory varchar(50),
  Title varchar(255),
  Description text,
  Remark text,
  Attachment varchar(255),
  DaysBefore INT NOT NULL default 0,
  ItemsRelated CHAR(50) default NULL,
  TimeSlotBatchID INT default NULL,
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (ResourceID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_ROLE'
#

$sql_table[] = "CREATE TABLE INTRANET_ROLE (
  RoleID int(8) NOT NULL auto_increment,
  Title varchar(100),
  Description text,
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (RoleID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_TIMETABLE'
#

$sql_table[] = "CREATE TABLE INTRANET_TIMETABLE (
  TimetableID int(8) NOT NULL auto_increment,
  Title varchar(255),
  Description text,
  URL varchar(255),
  ReadFlag text,
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (TimetableID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_USER'
#

$sql_table[] = "CREATE TABLE INTRANET_USER (
  UserID int(8) NOT NULL auto_increment,
  UserLogin varchar(20),
  UserPassword varchar(20) BINARY,
  HashedPass char(50),
  UserEmail varchar(100) DEFAULT '' NOT NULL,
  FirstName varchar(100),
  LastName varchar(100),
  EnglishName varchar(255) default NULL,
  ChineseName varchar(100),
  NickName varchar(100),
  DisplayName varchar(100),
  Title char(2),
  TitleEnglish varchar(100),
  TitleChinese varchar(100),
  Gender char(2),
  DateOfBirth datetime,
  HomeTelNo varchar(20),
  OfficeTelNo varchar(20),
  MobileTelNo varchar(20),
  FaxNo varchar(20),
  ICQNo varchar(20),
  Address varchar(255),
  URL varchar(255) default NULL,
  Country varchar(50),
  Info text,
  Remark text,
  PhotoLink varchar(255) default NULL,
  ClassNumber varchar(20),
  ClassName varchar(20),
  ClassLevel varchar(20),
  CardID varchar(255),
  WebSAMSRegNo varchar(100),
  Teaching char(2),
  YearOfLeft varchar(20),
  LastUsed datetime,
  LastModifiedPwd datetime,
  SessionKey varchar(255),
  SessionLastUpdated datetime,
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (UserID),
  UNIQUE UserEmail (UserEmail),
  UNIQUE UserLogin (UserLogin),
  INDEX CardID (CardID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_USERGROUP'
#

$sql_table[] = "CREATE TABLE INTRANET_USERGROUP (
  UserGroupID int(8) NOT NULL auto_increment,
  GroupID int(8),
  UserID int(8),
  RoleID int(8),
  Performance text,
  AdminAccessRight INT,
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (UserGroupID),
  UNIQUE UserGroupID (GroupID,UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_USERFOLDER'
#

$sql_table[] = "CREATE TABLE INTRANET_USERFOLDER (
  UserFolderID int(8) NOT NULL auto_increment,
  UserID int(8),
  UserFolderName varchar(255),
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (UserFolderID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_CAMPUSMAIL'
#

$sql_table[] = "CREATE TABLE INTRANET_CAMPUSMAIL (
  CampusMailID int(8) NOT NULL auto_increment,
  CampusMailFromID int(8),
  UserID int(8),
  UserFolderID int(8),
  SenderID int(8),
  SenderEmail text,
  RecipientID text,
  InternalCC text,
  InternalBCC text,
  ExternalTo text,
  ExternalCC text,
  ExternalBCC text,
  Subject varchar(255),
  Message text,
  MessageEncoding varchar(60),
  isHTML int,
  Attachment varchar(255),
  IsAttachment char(2),
  AttachmentSize FLOAT,
  IsImportant char(2),
  IsNotification char(2),
  MailType int,
  Deleted int NOT NULL default 0,
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (CampusMailID),
  INDEX UserID (UserID),
  INDEX RecordType (RecordType),
  INDEX RecordStatus (RecordStatus),
  INDEX UserFolderID (UserFolderID),
  INDEX MailType (MailType),
  INDEX CampusMailFromID (CampusMailFromID),
  INDEX Deleted (Deleted),
  INDEX IsAttachment (IsAttachment),
  INDEX Attachment (Attachment)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_CAMPUSMAIL_REPLY'
#

$sql_table[] = "CREATE TABLE INTRANET_CAMPUSMAIL_REPLY (
  CampusMailReplyID int(8) NOT NULL auto_increment,
  CampusMailID int(8),
  UserID int(8),
  UserName varchar(255),
  Message text,
  IsRead char(2),
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (CampusMailReplyID),
  INDEX CampusMailID (CampusMailID),
  INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_BULLETIN'
#

$sql_table[] = "CREATE TABLE INTRANET_BULLETIN (
  BulletinID int(8) NOT NULL auto_increment,
  ParentID int(8),
  GroupID int(8),
  UserID int(8),
  UserName varchar(255),
  UserEmail varchar(255),
  Subject varchar(255),
  Message text,
  ReadFlag text,
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (BulletinID),
  INDEX GroupID (GroupID),
  INDEX UserID (UserID),
  INDEX ParentID (ParentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_LINK'
#

$sql_table[] = "CREATE TABLE INTRANET_LINK (
  LinkID int(8) NOT NULL auto_increment,
  GroupID int(8),
  UserID int(8),
  UserName varchar(255),
  UserEmail varchar(255),
  Category varchar(255),
  Title varchar(255),
  URL varchar(255),
  Keyword varchar(255),
  Description text,
  ReadFlag text,
  VoteNum int(4),
  VoteTot int(4),
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (LinkID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_FILE'
#

$sql_table[] = "CREATE TABLE INTRANET_FILE (
  FileID int(8) NOT NULL auto_increment,
  GroupID int(8),
  UserID int(8),
  UserName varchar(255),
  UserEmail varchar(255),
  Category varchar(255),
  Title varchar(255),
  Location varchar(255),
  Keyword varchar(255),
  Description text,
  Size INT,
  ReadFlag text,
  RecordType char(2),
  RecordStatus char(2),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (FileID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

#
# Table structure for table 'INTRANET_SUBJECT'
#

$sql_table[] = "CREATE TABLE INTRANET_SUBJECT (
  SubjectID int(8) NOT NULL auto_increment,
  SubjectName varchar(100) default NULL,
  RecordStatus char(2) default NULL,
  PRIMARY KEY  (SubjectID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

# DB schema After 2.0

$sql_table[] = "CREATE TABLE INTRANET_SLOT_BATCH (
 BatchID int(11) NOT NULL auto_increment,
 Title varchar(255) default NULL,
 RecordType char(2) default NULL,
 RecordStatus char(2) default NULL,
 Description text,
 PRIMARY KEY (BatchID),
 UNIQUE Title (Title)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_SLOT (
 SlotID int(11) NOT NULL auto_increment,
 BatchID int(11) default NULL,
 Title varchar(255) default NULL,
 TimeRange varchar(255) default NULL,
 SlotSeq int(11) NOT NULL default '0',
 PRIMARY KEY (SlotID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_BOOKING_RECORD (
 BookingID int(11) NOT NULL auto_increment,
 ResourceID int(11) default NULL,
 UserID int(11) default NULL,
 BookingDate Date default NULL,
 PeriodicBookingID int(11) default NULL,
 Remark text,
 TimeSlot int(11) default NULL,
 RecordType char(2) default NULL,
 RecordStatus char(2) default NULL,
 UserCleared char(1) default NULL,
 TimeApplied datetime NOT NULL default '0000-00-00 00:00:00',
 LastAction datetime NOT NULL default '0000-00-00 00:00:00',
 PRIMARY KEY (BookingID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_PERIODIC_BOOKING (
 PeriodicBookingID int(11) NOT NULL auto_increment,
 ResourceID int(11) default NULL,
 UserID int(11) default NULL,
 BookingStartDate Date default NULL,
 BookingEndDate Date default NULL,
 TimeSlots varchar(100) default NULL,
 Remark text default NULL,
 RecordType char(2) default NULL,
 TypeValues varchar(10) default NULL,
 RecordStatus char(2) default NULL,
 UserCleared char(1) default NULL,
 TimeApplied datetime default NULL,
 LastAction datetime default NULL,
 PRIMARY KEY (PeriodicBookingID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_BOOKING_ARCHIVE (
 BookingArchiveID int(11) NOT NULL auto_increment,
 BookingDate Date default NULL,
 TimeSlot varchar(255) default NULL,
 Username varchar(255) default NULL,
 Category varchar(255) default NULL,
 Item varchar(255) default NULL,
 ItemCode varchar(255) default NULL,
 FinalStatus varchar(2) default NULL,
 TimeApplied datetime default NULL,
 LastAction datetime default NULL,
 PRIMARY KEY (BookingArchiveID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

# Self-defined group category
$sql_table[] = "CREATE TABLE INTRANET_GROUP_CATEGORY (
 GroupCategoryID int(11) NOT NULL auto_increment,
 CategoryName varchar(100) NOT NULL,
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (GroupCategoryID),
 UNIQUE CategoryName (CategoryName)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

# Reform class structure
$sql_table[] = "CREATE TABLE INTRANET_CLASS (
 ClassID int(11) NOT NULL auto_increment,
 ClassName varchar(100) NOT NULL,
 ClassLevelID int(11),
 GroupID int(11),
 WebSAMSClassCode char(5),
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ClassID),
 UNIQUE ClassName (ClassName)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CLASSLEVEL (
 ClassLevelID int(11) NOT NULL auto_increment,
 LevelName varchar(100) NOT NULL,
 WebSAMSLevel char(5),
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ClassLevelID),
 UNIQUE LevelName (LevelName)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

# Subject with Class relation
$sql_table[] = "CREATE TABLE INTRANET_CLASS_SUBJECT (
 ClassID int(11) NOT NULL,
 SubjectID int(11) NOT NULL,
 DisplayOrder int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

# Table for profile management - Student
$sql_table[] = "CREATE TABLE PROFILE_STUDENT (
 UserID int(11) NOT NULL,
 StudentNumber char(30),
 Guidance text,
 GuidanceContact varchar(100),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_STUDENT_ATTENDANCE (
 StudentAttendanceID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 AttendanceDate datetime,
 Year char(20),
 Semester char(20),
 Periods int(11),
 DayType int(11),
 Reason text,
 RecordType char(2),
 RecordStatus char(2),
 Remark text,
 ClassName varchar(20),
 ClassNumber varchar(20),
 UpgradedToDemeritID int,
 UpgradedToDetentionID int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (StudentAttendanceID),
 UNIQUE StudentDateTypeDay (UserID, AttendanceDate, DayType, RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_STUDENT_MERIT (
 StudentMeritID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Year char(20),
 Semester char(20),
 MeritDate datetime,
 NumberOfUnit INT,
 Reason text,
 PersonInCharge int(11),
 RecordType char(2),
 RecordStatus char(2),
 Remark text,
 ClassName varchar(20),
 ClassNumber varchar(20),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (StudentMeritID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_STUDENT_ACTIVITY (
 StudentActivityID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Year char(20),
 Semester char(20),
 ActivityName Text,
 Role varchar(100),
 Performance text,
 Remark text,
 ClassName varchar(20),
 ClassNumber varchar(20),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (StudentActivityID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_STUDENT_SERVICE (
 StudentServiceID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Year char(20),
 Semester char(20),
 ServiceDate date,
 ServiceName Text,
 Role varchar(100),
 Performance text,
 Remark text,
 ClassName varchar(20),
 ClassNumber varchar(20),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (StudentServiceID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_STUDENT_AWARD (
 StudentAwardID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Year char(20),
 Semester char(20),
 AwardDate date,
 AwardName Text,
 Remark Text,
 ClassName varchar(20),
 ClassNumber varchar(20),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (StudentAwardID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_STUDENT_ASSESSMENT (
 AssessmentID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Year char(20),
 Semester char(20),
 AssessmentDate date,
 AssessBy int(11),
 AssessByName varchar(255),
 FormID int(11) NOT NULL,
 AnsString text,
 RecordType char(2),
 RecordStatus char(2),
 ClassName varchar(20),
 ClassNumber varchar(20),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (AssessmentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CLASSTEACHER (
 ClassTeacherID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 ClassID int(11) NOT NULL,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ClassTeacherID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_SUBJECT_TEACHER (
 SubjectTeacherID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 SubjectID int(11) NOT NULL,
 ClassID int(11) NOT NULL,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (SubjectTeacherID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_PARENTRELATION (
 ParentID int(11) NOT NULL,
 StudentID int(11) NOT NULL,
 UNIQUE ParentStudentID (ParentID, StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_FORM (
 FormID int(11) NOT NULL auto_increment,
 FormName Char(255),
 Description Text,
 QueString Text,
 OwnerGroup int(11),
 FormType Char(2),
 RecordType Char(2),
 RecordStatus Char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (FormID),
 UNIQUE FormName (FormName)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_LOGIN_SESSION (
 LoginSessionID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 ClientHost varchar(255),
 StartTime datetime,
 DateModified datetime,
 PRIMARY KEY (LoginSessionID),
 INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_ENROL_STUDENT (
 EnrolID int(11) NOT NULL auto_increment,
 StudentID int(11),
 Max int(11),
 Approved int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (EnrolID),
 UNIQUE StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_ENROL_GROUPSTUDENT (
 EnrolGroupID int(11) NOT NULL auto_increment,
 StudentID int(11) NOT NULL,
 GroupID int(11) NOT NULL,
 Choice int(11),
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (EnrolGroupID),
 UNIQUE StudentGroupID (GroupID, StudentID),
 INDEX StudentID (StudentID),
 INDEX GroupID (GroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

/*
$sql_table[] = "CREATE TABLE INTRANET_ENROL_GROUPINFO (
 EnrolGroupID int(11) NOT NULL auto_increment,
 GroupID int(11) NOT NULL,
 Quota int(11),
 Approved int(11) default 0,
 Description text,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (EnrolGroupID),
 UNIQUE GroupID (GroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";
*/
$sql_table[] = "CREATE TABLE INTRANET_ENROL_GROUPINFO (
 EnrolGroupID int(11) NOT NULL auto_increment,
 GroupID int(11) NOT NULL,
 Quota int(11),
 Approved int(11) default 0,
 Description text,
 UpperAge int(2) default NULL,
 LowerAge int(2) default NULL,
 Gender varchar(2) default NULL,
 AttachmentLink1 varchar(255) default NULL,
 AttachmentLink2 varchar(255) default NULL,
 AttachmentLink3 varchar(255) default NULL,
 AttachmentLink4 varchar(255) default NULL,
 AttachmentLink5 varchar(255) default NULL,
 AddToPayment int(2) default 0,
 GroupCategory int(11),
 PaymentAmount float default 0,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (EnrolGroupID),
 UNIQUE GroupID (GroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_STUDENT_FILES (
 FileID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Year char(20),
 Semester char(20),
 Title varchar(255),
 Description text,
 FileType varchar(100),
 FileName varchar(255),
 FileSize float,
 Path varchar(255),
 RecordType char(2),
 RecordStatus char(2),
 ClassName varchar(20),
 ClassNumber varchar(20),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (FileID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_SURVEY (
 SurveyID int(11) NOT NULL auto_increment,
 Title varchar(255) NOT NULL,
 Description text,
 Question text,
 DateStart datetime NOT NULL,
 DateEnd datetime NOT NULL,
 OwnerGroupID int(11),
 PosterID int(11),
 PosterName varchar(255),
 Internal char(2),
 AllFieldsReq int,
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (SurveyID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_GROUPSURVEY (
 SurveyID int(11),
 GroupID int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_SURVEYANSWER (
 AnswerID int(11) NOT NULL auto_increment,
 SurveyID int(11) NOT NULL,
 UserID int(11) NOT NULL,
 UserName varchar(255),
 Answer text,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (AnswerID),
 INDEX SurveyID (SurveyID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE QB_QUESTION (
 QuestionID int(11) NOT NULL auto_increment,
 QuestionCode varchar(100),
 LevelID int(11) NOT NULL,
 CategoryID int(11) NOT NULL,
 DifficultyID int(11) NOT NULL,
 Lang int(4) ,
 QuestionEng text,
 QuestionChi text,
 AnswerEng text,
 AnswerChi text,
 HintEng text,
 HintChi text,
 ExplanationEng text,
 ExplanationChi text,
 ReferenceEng text,
 ReferenceChi text,
 OriginalFileEng text,
 OriginalFileChi text,
 QuestionType varchar(2),
 OwnerIntranetID int(11) NOT NULL,
 OwnerName varchar(255),
 RelatedGroupID int(11) NOT NULL,
 BulletinID int(11),
 RecordStatus varchar(2),
 ReadFlag text,
 DownloadFlag text,
 DownloadedCount int(11) default 0,
 Score int(11) default 0,
 DateInput datetime,
 DateModified datetime,
 DateLastAdminAction datetime,
 LastAdminActionIntranetID int(11),
 PRIMARY KEY (QuestionID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE QB_LEVEL (
 UniqueID int(11) NOT NULL auto_increment,
 EngName varchar(100),
 ChiName varchar(100),
 RelatedGroupID int(11) NOT NULL,
 DisplayOrder int(11),
 PRIMARY KEY (UniqueID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE QB_CATEGORY (
 UniqueID int(11) NOT NULL auto_increment,
 EngName varchar(100),
 ChiName varchar(100),
 RelatedGroupID int(11) NOT NULL,
 DisplayOrder int(11),
 PRIMARY KEY (UniqueID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE QB_DIFFICULTY (
 UniqueID int(11) NOT NULL auto_increment,
 EngName varchar(100),
 ChiName varchar(100),
 RelatedGroupID int(11) NOT NULL,
 DisplayOrder int(11),
 PRIMARY KEY (UniqueID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE QB_USER_SCORE (
 UserID int(11) NOT NULL,
 Score int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_HOMEWORK_SPECIAL (
 HomeworkID int(11) NOT NULL,
 ClassGroupID int(11),
 INDEX HomeworkID (HomeworkID),
 INDEX ClassGroupID (ClassGroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_NOTICE (
 NoticeID int(11) NOT NULL auto_increment,
 NoticeNumber varchar(50),
 Title varchar(255),
 Description text,
 DateStart datetime,
 DateEnd datetime,
 IssueUserID int(11),
 IssueUserName varchar(255),
 RecipientID text,
 Question text,
 Attachment varchar(255),
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (NoticeID),
 INDEX IssuerUserID (IssueUserID),
 INDEX RecordStatus (RecordStatus)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_NOTICE_REPLY (
 NoticeReplyID int(11) NOT NULL auto_increment,
 NoticeID int(11) NOT NULL,
 StudentID int(11) NOT NULL,
 StudentName varchar(255),
 SignerID int(11),
 SignerName varchar(255),
 Answer text,
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (NoticeReplyID),
 INDEX NoticeID (NoticeID),
 INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_TV_CHANNEL (
 ChannelID int(11) NOT NULL auto_increment,
 Title varchar(255),
 Description text,
 DisplayOrder int(11),
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ChannelID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_TV_MOVIECLIP (
 ClipID int(11) NOT NULL auto_increment,
 Title varchar(255),
 Description text,
 ClipURL text,
 FilePath text,
 FileName text,
 Recommended char(2),
 isFile char(2),
 IsSecure int(11),
 UserID int(11),
 RecordType int(11),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ClipID),
 INDEX RecordType (RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_TV_BULLETIN (
 BulletinID int(11) NOT NULL auto_increment,
 ParentID int(11),
 UserID int(11),
 UserName varchar(255),
 Subject varchar(255),
 Message text,
 ReadFlag text,
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (BulletinID),
 INDEX ParentID (ParentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_TV_POLLING (
 PollingID int(11) NOT NULL auto_increment,
 Question text,
 Reference text,
 DateStart datetime,
 DateEnd datetime,
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (PollingID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_TV_POLLING_OPTION (
 PollingOptionID int(11) NOT NULL auto_increment,
 PollingID int(11),
 OptionText text,
 OptionOrder int(11),
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (PollingOptionID),
 INDEX PollingID (PollingID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_TV_POLLING_RESULT (
 PollingResultID int(11) NOT NULL auto_increment,
 PollingID int(11),
 AnswerOptionOrder int(11),
 UserID int(11),
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (PollingResultID),
 INDEX PollingID (PollingID),
 INDEX AnswerOptionOrder (AnswerOptionOrder)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_SYSTEM_ACCESS (
 UserID int(11) NOT NULL,
 ACL int(11),
 PRIMARY KEY (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_ARCHIVE_LOGIN_SESSION (
 RecordID int(11) NOT NULL auto_increment,
 UserName varchar(255),
 ClassName varchar(20),
 ClassNumber varchar(20),
 LoginID varchar(20),
 TimeIn datetime,
 TimeOut datetime,
 ClientHost varchar(255),
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_ARCHIVE_ATTENDANCE (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11),
 EnglishName varchar(255),
 ChineseName varchar(255),
 UserName varchar(255),
 ClassName varchar(20),
 ClassNumber varchar(20),
 RecordDate datetime,
 Year varchar(20),
 Semester varchar(20),
 Period varchar(10),
 DayType varchar(10),
 Reason text,
 RecordType varchar(20),
 Remark text,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_ARCHIVE_MERIT (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11),
 EnglishName varchar(255),
 ChineseName varchar(255),
 UserName varchar(255),
 ClassName varchar(20),
 ClassNumber varchar(20),
 RecordDate datetime,
 Year varchar(20),
 Semester varchar(20),
 NumberOfUnit int(11),
 Reason text,
 MeritType varchar(20),
 Remark text,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_ARCHIVE_SERVICE (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11),
 EnglishName varchar(255),
 ChineseName varchar(255),
 UserName varchar(255),
 ClassName varchar(20),
 ClassNumber varchar(20),
 RecordDate datetime,
 Year varchar(20),
 Semester varchar(20),
 ServiceName text,
 Role varchar(100),
 Performance text,
 Remark text,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_ARCHIVE_ACTIVITY (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11),
 EnglishName varchar(255),
 ChineseName varchar(255),
 UserName varchar(255),
 ClassName varchar(20),
 ClassNumber varchar(20),
 RecordDate datetime,
 Year varchar(20),
 Semester varchar(20),
 ActivityName text,
 Role varchar(100),
 Performance text,
 Remark text,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_ARCHIVE_AWARD (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11),
 EnglishName varchar(255),
 ChineseName varchar(255),
 UserName varchar(255),
 ClassName varchar(20),
 ClassNumber varchar(20),
 RecordDate datetime,
 Year varchar(20),
 Semester varchar(20),
 AwardName text,
 Remark text,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_ARCHIVE_ASSESSMENT (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11),
 EnglishName varchar(255),
 ChineseName varchar(255),
 UserName varchar(255),
 ClassName varchar(20),
 ClassNumber varchar(20),
 RecordDate datetime,
 Year varchar(20),
 Semester varchar(20),
 AssessorName varchar(255),
 FormContent text,
 UserRemoved int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_ARCHIVE_FILES (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11),
 EnglishName varchar(255),
 ChineseName varchar(255),
 UserName varchar(255),
 ClassName varchar(20),
 ClassNumber varchar(20),
 Year varchar(20),
 Semester varchar(20),
 Title varchar(255),
 Description text,
 FileType varchar(100),
 FileName varchar(255),
 FileSize float,
 Path varchar(255),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_ORPAGE_GROUP (
 GroupID int(11) NOT NULL,
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (GroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_SMS_LOG (
 LogID int(11) NOT NULL auto_increment,
 JobID char(50),
 ReceiverID int(11),
 MobileNumber char(20),
 Message text,
 TimeStamp datetime,
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (LogID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_COURSE (
 CourseID int(11),
 ClassGroupID int(11),
 SubjectID int(11),
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 INDEX CourseID (CourseID),
 INDEX ClassGroupID (ClassGroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_COURSE_HOMEWORK (
 CourseID int(11),
 CourseAssignmentID int(11),
 IntranetHomeworkID int(11),
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 UNIQUE IntranetHomeworkID (IntranetHomeworkID),
 INDEX CourseID (CourseID),
 INDEX CourseAssignmentID (CourseAssignmentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_SUBJECT_LEADER (
 LeaderID int(11) NOT NULL auto_increment,
 UserID int(11),
 ClassID int(11),
 SubjectID int(11),
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (LeaderID),
 INDEX UserID (UserID),
 INDEX ClassID (ClassID),
 INDEX SubjectID (SubjectID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_SMS_TEMPLATE (
 TemplateID int(11) NOT NULL auto_increment,
 UserID int(11),
 Title varchar(100),
 Content varchar(160),
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (TemplateID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_ADMIN_USER (
 AdminID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 AdminLevel int(11),
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (AdminID),
 UNIQUE UserIDRecordType (UserID, RecordType),
 INDEX RecordType (RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_ANNOUNCEMENT_APPROVAL (
 RecordID int(11) NOT NULL auto_increment,
 AnnouncementID int(11) NOT NULL,
 UserID int(11),
 ApprovalUserName varchar(255),
 Remark text,
 Reason text,
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE AnnouncementID (AnnouncementID),
 INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_LOG (
 LogID int(11) NOT NULL auto_increment,
 CardID varchar(100),
 SiteName varchar(255),
 Type varchar(100),
 RecordedTime datetime,
 PRIMARY KEY (LogID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_SLOT_SPECIAL (
 SpecialSlotID int(11) NOT NULL auto_increment,
 SlotID int(11) NOT NULL,
 SlotStart char(10),
 SlotEnd char(10),
 SlotBoundary char(10),
 DayType int,
 DayValue int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY(SpecialSlotID),
 UNIQUE SlotDayValue (SlotID,DayValue),
 INDEX SlotID (SlotID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_DAILYSLOTRECORD (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11),
 RecordDate date,
 SlotID int(11),
 RecordedTime datetime,
 MinDiffer int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE UserIDRecordDateSlot (UserID, RecordDate, SlotID),
 INDEX UserID (UserID),
 INDEX RecordDate (RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_OUTING (
 OutingID int(11) NOT NULL auto_increment,
 UserID int(11),
 RecordDate date,
 OutTime time,
 BackTime time,
 Location text,
 FromWhere text,
 Objective text,
 PIC text,
 Detail text,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (OutingID),
 INDEX UserID (UserID),
 INDEX RecordDate (RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_DETENTION (
 DetentionID int(11) NOT NULL auto_increment,
 StudentID int(11),
 RecordDate date,
 ArrivalTime time,
 DepartureTime time,
 Location text,
 Reason text,
 Remark text,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (DetentionID),
 INDEX StudentID (StudentID),
 INDEX RecordDate (RecordDate),
 UNIQUE Detention(StudentID,RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_PROMOTION_STATUS (
 UserID int(11) NOT NULL,
 NewClass int(11),
 NewClassNumber varchar(20),
 PRIMARY KEY (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_CLASS_HISTORY (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 ClassName varchar(20),
 ClassNumber varchar(20),
 AcademicYear varchar(20),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY(RecordID),
 INDEX UserID (UserID),
 INDEX AcademicYear (AcademicYear)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PROFILE_ARCHIVE_CLASS_HISTORY (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 EnglishName varchar(255),
 ChineseName varchar(255),
 ClassName varchar(20),
 ClassNumber varchar(20),
 AcademicYear varchar(20),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX UserID (UserID),
 INDEX AcademicYear (AcademicYear)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_ARCHIVE_USER (
 UserID int(11),
 UserLogin varchar(20),
 UserPassword varchar(20) binary,
 HashedPass varchar(50),
 EnglishName varchar(255),
 ChineseName varchar(255),
 NickName varchar(100),
 DisplayName varchar(100),
 Title char(2),
 Gender char(2),
 DateOfBirth datetime,
 HomeTelNo varchar(20),
 OfficeTelNo varchar(20),
 MobileTelNo varchar(20),
 FaxNo varchar(20),
 ICQNo varchar(20),
 Address varchar(255),
 URL varchar(255),
 Country varchar(255),
 Info text,
 Remark text,
 PhotoLink varchar(255),
 ClassName varchar(20),
 ClassNumber varchar(20),
 CardID varchar(255),
 WebSAMSRegNo varchar(100),
 RecordType int,
 YearOfLeft varchar(20),
 UNIQUE UserID (UserID),
 INDEX EnglishName (EnglishName),
 INDEX ChineseName (ChineseName),
 INDEX ClassName (ClassName),
 INDEX YearOfLeft (YearOfLeft)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_REMINDER (
 ReminderID int(11) NOT NULL auto_increment,
 StudentID int(11),
 TeacherID int(11),
 DateOfReminder date,
 Reason text,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY(ReminderID),
 INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PAYMENT_TERMINAL_USER (
 TerminalUserID int(11) NOT NULL auto_increment,
 Username varchar(255) NOT NULL,
 Password varchar(255),
 PaymentAllowed char(2),
 PurchaseAllowed char(2),
 LastLogin datetime,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY(TerminalUserID),
 UNIQUE Username (Username)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PAYMENT_TERMINAL_LOG (
 LogID int(11) NOT NULL auto_increment,
 Username varchar(255) NOT NULL,
 LoginTime datetime,
 LogoutTime datetime,
 IP varchar(255),
 TerminalID varchar(255),
 SessionKey varchar(255),
 PRIMARY KEY(LogID),
 INDEX Username (Username)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PAYMENT_ACCOUNT (
 StudentID int(11) NOT NULL,
 Balance Float,
 PPSAccountNo varchar(255),
 LastUpdateByAdmin varchar(255),
 LastUpdateByTerminal varchar(255),
 LastUpdated datetime,
 UNIQUE StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PAYMENT_CREDIT_TRANSACTION (
 TransactionID int(11) NOT NULL auto_increment,
 StudentID int(11),
 Amount float,
 RecordType int,
 RecordStatus int,
 RefCode varchar(100),
 PPSAccountNo varchar(255),
 AdminInCharge varchar(255),
 PPSType int,
 TransactionTime datetime,
 DateInput datetime,
 PRIMARY KEY(TransactionID),
 INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PAYMENT_PAYMENT_CATEGORY (
 CatID int(11) NOT NULL auto_increment,
 Name varchar(255),
 DisplayOrder int,
 Description text,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY(CatID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PAYMENT_PAYMENT_ITEM (
 ItemID int(11) NOT NULL auto_increment,
 CatID int(11),
 Name varchar(255),
 DisplayOrder int,
 PayPriority int,
 Description text,
 StartDate date,
 EndDate date,
 DefaultAmount float,
 ProcessingTerminalUser varchar(255),
 ProcessingAdminUser varchar(255),
 ProcessingTerminalIP varchar(255),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY(ItemID),
 INDEX CatID (CatID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PAYMENT_PAYMENT_ITEMSTUDENT (
 PaymentID int(11) NOT NULL auto_increment,
 ItemID int(11),
 StudentID int(11),
 Amount float,
 SubsidyAmount float,
 SubsidyUnitID int(11),
 SubsidyPICAdmin varchar(255),
 SubsidyPICUserID int(11),
 RecordType int,
 RecordStatus int,
 PaidTime datetime,
 ProcessingTerminalUser varchar(255),
 ProcessingAdminUser varchar(255),
 ProcessingTerminalIP varchar(255),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY(PaymentID),
 INDEX ItemID (ItemID),
 INDEX StudentID (StudentID),
 INDEX SubsidyUnitID (SubsidyUnitID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PAYMENT_OVERALL_TRANSACTION_LOG (
 LogID int(11) NOT NULL auto_increment,
 StudentID int(11),
 TransactionType int,
 Amount float,
 RelatedTransactionID int,
 BalanceAfter float,
 TransactionTime datetime,
 Details text,
 RefCode varchar(255),
 PRIMARY KEY(LogID),
 INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE_ADMIN (
 Username varchar(255),
 Password varchar(255),
 PRIMARY KEY (Username)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";



$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE_GROUP (
 GroupID int(11) NOT NULL auto_increment,
 Title varchar(255),
 StartTime varchar(10),
 EndTime varchar(10),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (GroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE_USERGROUP (
 UserID int(11) NOT NULL,
 GroupID int(11),
 UNIQUE UserID (UserID),
 INDEX GroupID (GroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE_GROUP_SPECIAL (
 SpecialID int(11) NOT NULL auto_increment,
 GroupID int(11),
 StartTime varchar(10),
 EndTime varchar(10),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY(SpecialID),
 UNIQUE GroupIDRecordType (GroupID,RecordType),
 INDEX GroupID (GroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE_LOG (
 LogID int(11) NOT NULL auto_increment,
 CardID varchar(100),
 SiteName varchar(255),
 Type varchar(100),
 RecordedTime datetime,
 PRIMARY KEY (LogID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE_DAILYRECORD (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11),
 RecordDate date,
 ArrivalTime time,
 DepartureTime time,
 MinLate int(11),
 MinEarly int(11),
 InSite varchar(255),
 OutSite varchar(255),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE UserIDRecordDate (UserID, RecordDate),
 INDEX UserID (UserID),
 INDEX RecordDate (RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE_RECORD (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11),
 RecordDate date,
 RecordType tinyint,
 Remark text,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE UserIDRecordDateType (UserID, RecordDate, RecordType),
 INDEX UserID (UserID),
 INDEX RecordDate (RecordDate),
 INDEX RecordType (RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE_OUTING (
 OutingID int(11) NOT NULL auto_increment,
 UserID int(11),
 RecordDate date,
 OutTime time,
 BackTime time,
 Location text,
 FromWhere text,
 Objective text,
 Remark text,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (OutingID),
 INDEX UserID (UserID),
 INDEX RecordDate (RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE_LEAVE (
 LeaveID int(11) NOT NULL auto_increment,
 UserID int(11),
 RecordDate date,
 Reason text,
 Remark text,
 TimeSlot int,
 Days int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (LeaveID),
 UNIQUE UserIDRecordDateTimeSlot (UserID,RecordDate,TimeSlot),
 INDEX UserID (UserID),
 INDEX RecordDate (RecordDate),
 INDEX RecordType (RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CAMPUSMAIL_USERQUOTA (
 UserID int NOT NULL,
 Quota int NOT NULL default 0,
 PRIMARY KEY (UserID)
)
";

$sql_table[] = "CREATE TABLE INTRANET_CAMPUSMAIL_FOLDER (
 FolderID int(11) NOT NULL auto_increment,
 OwnerID int,
 FolderName varchar(255) NOT NULL,
 RecordType int default 0,
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY(FolderID),
 INDEX OwnerID (OwnerID),
 INDEX RecordType (RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL (
 AddressID int NOT NULL auto_increment,
 OwnerID int,
 TargetName varchar(255),
 TargetAddress varchar(255),
 Remark text,
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (AddressID),
 INDEX OwnerID (OwnerID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL (
 AliasID int NOT NULL auto_increment,
 OwnerID int,
 AliasName varchar(255),
 Remark text,
 NumberOfEntry int,
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (AliasID),
 INDEX OwnerID (OwnerID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY (
 EntryID int NOT NULL auto_increment,
 AliasID int,
 TargetID int,
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (EntryID),
 UNIQUE AliasTargetIDRecordType (AliasID, TargetID, RecordType),
 INDEX AliasID (AliasID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CIRCULAR (
 CircularID int(11) NOT NULL auto_increment,
 CircularNumber varchar(100),
 Title varchar(255),
 Description text,
 DateStart date,
 DateEnd date,
 IssueUserID int(11),
 IssueUserName varchar(255),
 RecipientID text,
 Question text,
 Attachment text,
 SignedCount int default 0,
 TotalCount int default 0,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 HasStar int,
 PRIMARY KEY (CircularID),
 INDEX IssueUserID (IssueUserID),
 INDEX RecordStatus (RecordStatus)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CIRCULAR_REPLY (
 CircularReplyID int(11) NOT NULL auto_increment,
 CircularID int(11) NOT NULL,
 UserID int(11) NOT NULL,
 UserName varchar(255),
 Answer text,
 SignerID int(11),
 SignerName varchar(255),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 HasStar int,
 PRIMARY KEY (CircularReplyID),
 UNIQUE CircularIDUserID (CircularID,UserID),
 INDEX CircularID (CircularID),
 INDEX UserID (UserID),
 INDEX RecordStatus (RecordStatus)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_IMAIL_ATTACHMENT_PART (
 PartID int(11) NOT NULL auto_increment,
 CampusMailID int(11) NOT NULL,
 AttachmentPath varchar(255),
 FileName varchar(255),
 FileSize float,
 isEmbed int,
 PRIMARY KEY (PartID),
 INDEX CampusMailID (CampusMailID),
 INDEX AttachmentPath (AttachmentPath)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PAYMENT_MISSED_PPS_BILL (
 RecordID int(11) NOT NULL auto_increment,
 PPSAccount varchar(255),
 Amount float,
 PPSType int,
 InputTime datetime,
 MappedStudentID int,
 PRIMARY KEY RecordID (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PAYMENT_CREDIT_FILE_LOG (
 FileDate varchar(100),
 FileType int,
 UNIQUE FileDateType (FileDate, FileType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CYCLE_GENERATION_PERIOD (
 PeriodID int(11) NOT NULL auto_increment,
 PeriodStart date NOT NULL,
 PeriodEnd date NOT NULL,
 PeriodType int,
 CycleType int,
 PeriodDays int,
 FirstDay int,
 SaturdayCounted int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (PeriodID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CYCLE_IMPORT_RECORD (
 RecordID int(11) NOT NULL auto_increment,
 RecordDate date NOT NULL,
 TextEng varchar(50),
 TextChi varchar(50),
 TextShort varchar(50),
 PRIMARY KEY (RecordID),
 UNIQUE RecordDate (RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CYCLE_TEMP_DAYS_VIEW (
 RecordDate date NOT NULL,
 TextEng varchar(50),
 TextChi varchar(50),
 TextShort varchar(50),
 PRIMARY KEY RecordDate (RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CYCLE_DAYS (
 RecordDate date NOT NULL,
 TextEng varchar(50),
 TextChi varchar(50),
 TextShort varchar(50),
 PRIMARY KEY RecordDate (RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_PERIOD_TIME (
 SlotID int NOT NULL auto_increment,
 DayType int,
 DayValue varchar(50),
 MorningTime time,
 LunchStart time,
 LunchEnd time,
 LeaveSchoolTime time,
 NonSchoolDay int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (SlotID),
 UNIQUE TypeValue(DayType, DayValue)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_CLASS_SPECIFIC_MODE (
 ClassID int NOT NULL,
 Mode int default 0,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ClassID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_CLASS_PERIOD_TIME (
 RecordID int NOT NULL auto_increment,
 ClassID int NOT NULL,
 DayType int,
 DayValue varchar(10),
 MorningTime time,
 LunchStart time,
 LunchEnd time,
 LeaveSchoolTime time,
 NonSchoolDay int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX ClassID (ClassID),
 UNIQUE ClassTypeValue (ClassID, DayType, DayValue)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_LUNCH_ALLOW_LIST (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(11) NOT NULL,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_HELPER_STUDENT (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(11) NOT NULL,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_DAILY_DATA_CONFIRM (
 RecordID int(11) NOT NULL auto_increment,
 RecordDate date NOT NULL,
 LateConfirmed int,
 LateConfirmTime datetime,
 AbsenceConfirmed int,
 AbsenceConfirmTime datetime,
 EarlyConfirmed int,
 EarlyConfirmTime datetime,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX RecordDate (RecordDate),
 INDEX RecordType (RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_BAD_ACTION (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(11) NOT NULL,
 RecordDate date NOT NULL,
 RecordTime datetime,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX StudentID (StudentID),
 INDEX RecordDate (RecordDate),
 INDEX RecordType (RecordType),
 INDEX RecordStatus (RecordStatus)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_RECORD_DATE_STORAGE (
 Year int NOT NULL,
 Month int NOT NULL,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 UNIQUE YearMonth (Year, Month),
 INDEX Year(Year),
 INDEX Month(Month)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_PROFILE_RECORD_REASON (
 RecordID int NOT NULL auto_increment,
 RecordDate date NOT NULL,
 StudentID int,
 Reason text,
 ProfileRecordID int,
 DayType int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE DateStudentDayType (RecordDate, StudentID, DayType, RecordType),
 INDEX RecordDate (RecordDate),
 INDEX StudentID (StudentID),
 INDEX ProfileRecordID (ProfileRecordID),
 INDEX DayType (DayType),
 INDEX RecordType (RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE INTRANET_HOMEWORK_TYPE (
 TypeID int(11) NOT NULL auto_increment,
 TypeName varchar(255),
 DisplayOrder int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (TypeID),
 INDEX RecordStatus (RecordStatus)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE DISCIPLINE_SCHOOL_RULE (
 RecordID int(11) NOT NULL auto_increment,
 RuleCode varchar(100),
 RuleName text,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE RuleCode (RuleCode)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_MERIT_TYPE_SETTING (
 MeritType int(11) NOT NULL,
 UpgradeFromType int(11),
 UpgradeNum int(11),
 ApproveNum int(11),
 ApproveLevel int(11),
 UNIQUE MeritType (MeritType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_MERIT_ITEM_CATEGORY (
 CatID int(11) NOT NULL auto_increment,
 CategoryName varchar(255) NOT NULL,
 DisplayOrder int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (CatID),
 INDEX RecordType (RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_MERIT_ITEM (
 ItemID int(11) NOT NULL auto_increment,
 CatID int(11),
 ItemCode varchar(100),
 ItemName text,
 RuleID int(11),
 ConductScore int(11),
 SubScore1 int(11),
 NumOfMerit int(11),
 RelatedMeritType int,
 PunishmentID int,
 DetentionMinutes int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ItemID),
 INDEX CatID (CatID),
 INDEX RecordType (RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_SPECIAL_PUNISHMENT (
 RecordID int(11) NOT NULL auto_increment,
 PunishmentName text,
 DisplayOrder int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_MERIT_RECORD (
 RecordID int(11) NOT NULL auto_increment,
 RecordDate date,
 Year varchar(100),
 Semester varchar(100),
 StudentID int(11),
 ItemID int(11),
 ItemText text,
 MeritType int(11),
 Remark text,
 ProfileMeritType int(11),
 ProfileMeritCount int(11),
 ProfileMeritID int(11),
 ConductScoreChange int(11),
 ConductScoreAfter int(11),
 SubScore1Change int(11),
 SubScore1After int(11),
 Subject varchar(255),
 PICID int(11),
 SpecialPunishID int(11),
 SpecialPunishRemark text,
 DetentionMinutes int(11),
 hasParentContact int(11),
 hasSocialContact int(11),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX ItemID (ItemID),
 INDEX MeritType (MeritType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_CONTACT_RECORD (
 RecordID int(11) NOT NULL auto_increment,
 ContactType int(11),
 StudentID int(11),
 RecordDate date,
 RecordTime time,
 ContactVenue varchar(255),
 ContactPIC int(11),
 Remark text,
 MeritRecordID int(11),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX ContactType (ContactType),
 INDEX RecordDate (RecordDate),
 INDEX ContactPIC (ContactPIC),
 INDEX RecordStatus (RecordStatus)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_STUDENT_CONDUCT_BALANCE (
 StudentID int(11) NOT NULL,
 Year varchar(100),
 Semester varchar(100),
 IsAnnual int(11),
 ConductScore int(11),
 GradeChar varchar(100),
 AdjustedGradeChar varchar(100),
 RecordType int(11),
 RecordStatus int(11),
 UNIQUE StudentIDYear (StudentID, Year, Semester, IsAnnual)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE DISCIPLINE_ACCESS_LEVEL (
 LevelNum int(11) NOT NULL,
 LevelName varchar(255),
 UNIQUE LevelNum (LevelNum)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_USER_ACL (
 UserID int(11) NOT NULL,
 UserLevel int(11) default 0,
 UNIQUE UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_FUNCTION_SETTING (
 FunctionID int(11),
 UserLevelRequired int(11),
 Remark text,
 UNIQUE FunctionID (FunctionID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE DISCIPLINE_LATE_UPGRADE_RULE (
 RuleOrder int(11),
 NextNumLate int(11) NOT NULL default 1,
 ProfileMeritType int(11) NOT NULL default -1,
 ProfileMeritNum int(11) NOT NULL default 0,
 ReasonItemID int(11),
 Reason text,
 ConductScore int(11),
 UNIQUE RuleOrder(RuleOrder)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_LATE_UPGRADE_RULE_DETENTION (
 RuleOrder int(11),
 NextNumLate int(11) NOT NULL default 1,
 DetentionMinutes int(11) NOT NULL default 0,
 Reason text,
 UNIQUE RuleOrder(RuleOrder)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_DETENTION_RECORD (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(11),
 RecordDate date,
 Reason text,
 Subject varchar(255),
 PICID int(11),
 Minutes int(11),
 MeritRecordID int,
 PRIMARY KEY (RecordID),
 INDEX MeritRecordID (MeritRecordID),
 INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_DETENTION_PARTICIPATION_LOG (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(11),
 RecordDate date,
 Minutes int(11),
 PICID int(11),
 PRIMARY KEY (RecordID),
 INDEX StudentID(StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_CONDUCT_SETTING (
 ConductScore int(11),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_CONDUCT_GRADE_RULE (
 MinConductScore int(11),
 GradeChar varchar(100),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 UNIQUE MinConductScore (MinConductScore)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_SUBSCORE_WARNING_LEVEL (
 RecordID int(11) NOT NULL auto_increment,
 Score int(11),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX RecordType (RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$sql_table[] = "CREATE TABLE DISCIPLINE_SUBSCORE_SETTING (
 RecordID int(11) NOT NULL auto_increment,
 InitialScore int(11),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE RecordType (RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_STUDENT_SUBSCORE_BALANCE (
 StudentID int(11) NOT NULL,
 Year varchar(100),
 Semester varchar(100),
 IsAnnual int(11),
 SubScore int(11),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 UNIQUE StudentIDTypeYear (StudentID,RecordType,Year,Semester,IsAnnual)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_HOUSE (
 HouseID int(11) NOT NULL auto_increment,
 GroupID int(11),
 EnglishName varchar(255),
 ChineseName varchar(255),
 DisplayOrder int(11),
 ColorCode char(10),
 HouseCode char(10),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (HouseID),
 UNIQUE GroupID(GroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE SPORTS_SYSTEM_SETTING (
 NumberOfLanes int(11),
 EnrolScore float,
 PresentScore float,
 AbsentScore float,
 EnrolDateStart date,
 EnrolDateEnd date,
 EnrolDetails text,
 NumberGenerationType int,
 Rule1 int,
 Rule2 int,
 Rule3 int,
 AutoNumLength int,
 NumOfDays int,
 EnrolMinTotal int,
 EnrolMaxTotal int,
 EnrolMinTrack int,
 EnrolMaxTrack int,
 EnrolMinField int,
 EnrolMaxField int,
 DefaultLaneArrangement text,
 DateModified datetime
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE SPORTS_EVENT_TYPE_NAME (
 EventTypeID int(11),
 EnglishName varchar(255),
 ChineseName varchar(255),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 UNIQUE EventTypeID (EventTypeID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE SPORTS_SCORE_STANDARD (
 StandardID int(11) NOT NULL auto_increment,
 Name varchar(255),
 Position1 float,
 Position2 float,
 Position3 float,
 Position4 float,
 Position5 float,
 Position6 float,
 Position7 float,
 Position8 float,
 Position9 float,
 RecordBroken float,
 Qualified float,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (StandardID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE SPORTS_AGE_GROUP (
 AgeGroupID int(11) NOT NULL auto_increment,
 GradeChar char(10),
 EnglishName varchar(255),
 ChineseName varchar(255),
 Gender char(2),
 GroupCode varchar(100),
 DOBUpLimit date,
 DOBLowLimit date,
 DisplayOrder int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (AgeGroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE SPORTS_EVENT (
 EventID int(11) NOT NULL auto_increment,
 EventType int,
 EnglishName varchar(255),
 ChineseName varchar(255),
 SpecialLaneArrangment text,
 IsJump int,
 DisplayOrder int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (EventID),
 INDEX EventType (EventType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE SPORTS_EVENTGROUP (
 EventGroupID int(11) NOT NULL auto_increment,
 EventID int,
 GroupID int,
 IsOnlineEnrol int,
 CountPersonalQuota int,
 CountHouseScore int,
 CountClassScore int,
 CountIndividualScore int,
 ScoreStandardID int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (EventGroupID),
 INDEX EventID (EventID),
 INDEX GroupID (GroupID),
 INDEX IsOnlineEnrol (IsOnlineEnrol),
 INDEX CountPersonalQuota (CountPersonalQuota),
 INDEX ScoreStandardID (ScoreStandardID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE SPORTS_EVENTGROUP_EXT_TRACK (
 EventGroupID int(11),
 RecordHolderName varchar(255),
 RecordMin int,
 RecordSec int,
 RecordMs int,
 RecordYear varchar(100),
 RecordHouseID int,
 NewRecordHolderUserID int,
 NewRecordHolderName varchar(255),
 NewRecordMin int,
 NewRecordSec int,
 NewRecordMs int,
 NewRecordHouseID int,
 StandardMin int,
 StandardSec int,
 StandardMs int,
 FirstRoundType int,
 FirstRoundGroupCount int,
 FirstRoundRandom int,
 FirstRoundDay int,
 SecondRoundReq int,
 SecondRoundLanes int,
 SecondRoundGroups int,
 SecondRoundDay int,
 FinalRoundReq int,
 FinalRoundNum int,
 FinalRoundDay int,
 INDEX EventGroupID (EventGroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE SPORTS_EVENTGROUP_EXT_FIELD (
 EventGroupID int,
 RecordHolderName varchar(255),
 RecordMetre float,
 RecordYear varchar(100),
 RecordHouseID int,
 NewRecordHolderUserID int,
 NewRecordHolderName varchar(255),
 NewRecordMetre float,
 NewRecordHouseID int,
 StandardMetre float,
 FirstRoundType int,
 FirstRoundGroupCount int,
 FirstRoundRandom int,
 FirstRoundDay int,
 FinalRoundReq int,
 FinalRoundNum int,
 FinalRoundDay int,
 INDEX EventGroupID (EventGroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE SPORTS_EVENTGROUP_EXT_RELAY (
 EventGroupID int,
 RecordHolderName varchar(255),
 RecordMin int,
 RecordSec int,
 RecordMs int,
 RecordYear varchar(100),
 RecordHouseID int,
 NewRecordHolderName varchar(255),
 NewRecordMin int,
 NewRecordSec int,
 NewRecordMs int,
 NewRecordHouseID int,
 StandardMin int,
 StandardSec int,
 StandardMs int,
 ClassList text,
 CategoryName varchar(255),
 INDEX EventGroupID (EventGroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE SPORTS_STUDENT_ENROL_INFO (
 StudentID int(11) NOT NULL,
 AthleticNum varchar(100),
 TrackEnrolCount int,
 FieldEnrolCount int,
 DateModified int,
 UNIQUE StudentID(StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE SPORTS_STUDENT_ENROL_EVENT (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(11),
 EventGroupID int(11),
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX StudentID (StudentID),
 INDEX EventGroupID (EventGroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE SPORTS_LANE_ARRANGEMENT (
 EventGroupID int,
 RoundType int,
 StudentID int,
 ArrangeOrder int,
 Heat int,
 ResultMin int,
 ResultSec int,
 ResultMs int,
 ResultMetre float,
 RecordType int,
 RecordStatus int,
 IsAdvanced int,
 Score float,
 Rank int,
 DateModified datetime,
 INDEX EventGroupID (EventGroupID),
 INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE SPORTS_HOUSE_RELAY_LANE_ARRANGEMENT (
 EventGroupID int(11),
 HouseID int(11),
 ArrangeOrder int,
 ResultMin int,
 ResultSec int,
 ResultMs int,
 RecordType int,
 RecordStatus int,
 Score float,
 Rank int,
 DateModified datetime,
 INDEX EventGroupID (EventGroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE SPORTS_ADMIN_USER_ACL (
 AdminUserID int(11) NOT NULL,
 UserLevel int(11),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 UNIQUE AdminUserID(AdminUserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE CARD_STUDENT_SPECIFIC_DATE_TIME (
 RecordID int(11) NOT NULL auto_increment,
 RecordDate date,
 ClassID int(11) default 0,
 MorningTime time,
 LunchStart time,
 LunchEnd time,
 LeaveSchoolTime time,
 NonSchoolDay int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE DateClass (RecordDate, ClassID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_HOMEWORK_HANDIN_LIST (
 RecordID int(11) NOT NULL auto_increment,
 HomeworkID int(11) NOT NULL,
 StudentID int(11) NOT NULL,
 Score float,
 Grade varchar(20),
 Remark text,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE StudentHomeworkID (StudentID, HomeworkID),
 INDEX StudentID (StudentID),
 INDEX HomeworkID (HomeworkID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE2_GROUP (
 GroupID int(11) NOT NULL auto_increment,
 GroupName varchar(255) NOT NULL,
 DutySun int(11),
 DutyStartSun time,
 DutyEndSun time,
 DutyMon int(11),
 DutyStartMon time,
 DutyEndMon time,
 DutyTue int(11),
 DutyStartTue time,
 DutyEndTue time,
 DutyWed int(11),
 DutyStartWed time,
 DutyEndWed time,
 DutyThur int(11),
 DutyStartThur time,
 DutyEndThur time,
 DutyFri int(11),
 DutyStartFri time,
 DutyEndFri time,
 DutySat int(11),
 DutyStartSat time,
 DutyEndSat time,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (GroupID),
 UNIQUE GroupName (GroupName)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE2_GROUP_DATE_DUTY (
 RecordID int(11) NOT NULL auto_increment,
 GroupID int NOT NULL,
 DutyDate date NOT NULL,
 Duty int(11),
 DutyStart time,
 DutyEnd time,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE GroupDate (GroupID, DutyDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY (
 RecordID int(11) NOT NULL auto_increment,
 UserID int NOT NULL,
 DutyDate date NOT NULL,
 Duty int(11),
 DutyStart time,
 DutyEnd time,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE UserDate (UserID, DutyDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE2_REASON_TYPE (
 TypeID int(11) NOT NULL auto_increment,
 ReasonTypeName varchar(255),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (TypeID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE2_REASON_PRESET (
 ReasonID int(11) NOT NULL auto_increment,
 ReasonType int,
 ReasonText varchar(255),
 InWaived int,
 OutWaived int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ReasonID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE2_PROFILE (
 RecordID int(11) NOT NULL auto_increment,
 StaffID int(11) NOT NULL,
 RecordDate date,
 ReasonType int,
 Reason text,
 Waived int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX StaffID (StaffID),
 INDEX RecordDate (RecordDate),
 INDEX Waived (Waived)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE2_OT_RECORD (
 RecordID int(11) NOT NULL auto_increment,
 StaffID int(11) NOT NULL,
 RecordDate date,
 StartTime time,
 EndTime time,
 OTmins int,
 Waived int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE StaffDate (StaffID, RecordDate),
 INDEX StaffID (StaffID),
 INDEX RecordDate (RecordDate),
 INDEX Waived (Waived)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE2_OT_REDEEM (
 RecordID int(11) NOT NULL auto_increment,
 StaffID int(11),
 RedeemDate date,
 MinsRedeemed int,
 Remark text,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX StaffID (StaffID),
 INDEX RedeemDate (RedeemDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STAFF_ATTENDANCE2_LEAVE_RECORD (
 RecordID int(11) NOT NULL auto_increment,
 StaffID int(11) NOT NULL,
 RecordDate date,
 ReasonType int,
 Reason text,
 OutgoingType int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE StaffDate (StaffID, RecordDate),
 INDEX RecordType (RecordType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_DAILY_REMARK (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int NOT NULL,
 RecordDate date NOT NULL,
 Remark text,
 PRIMARY KEY (RecordID),
 UNIQUE StudentDate (StudentID, RecordDate),
 INDEX RecordDate (RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_WEBSAMS_ATTENDANCE_BASIC (
 RecordID int NOT NULL default 0,
 SchoolID char(10),
 SchoolYear char(5),
 SchoolLevel char(2),
 SchoolSession char(2),
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE (
 RecordID int(11) NOT NULL auto_increment,
 CodeID varchar(3),
 ReasonType int(11) NOT NULL,
 ReasonText varchar(255),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX ReasonType (ReasonType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL (
 AliasID int NOT NULL auto_increment,
 OwnerID int,
 AliasName varchar(255),
 Remark text,
 NumberOfEntry int,
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (AliasID),
 INDEX OwnerID (OwnerID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY (
 EntryID int NOT NULL auto_increment,
 AliasID int,
 TargetID int,
 RecordType char(2),
 RecordStatus char(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (EntryID),
 UNIQUE AliasTargetIDRecordType (AliasID, TargetID),
 INDEX AliasID (AliasID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_IMAIL_RECIPIENT_RESTRICTION (
 RecordID int(11) NOT NULL auto_increment,
 TargetType int NOT NULL default 0,
 Teaching int NOT NULL default 0,
 ClassLevel int NOT NULL default 0,
 Restricted int NOT NULL default 0,
 ToStaffOption int NOT NULL default 0,
 ToStudentOption int NOT NULL default 0,
 ToParentOption int NOT NULL default 0,
 BlockedGroupCat text,
 BlockedUserType text,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE IdentityType (TargetType,Teaching,ClassLevel)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_IMAIL_PREFERENCE (
 UserID int NOT NULL,
 DisplayName varchar(255),
 ReplyEmail varchar(255),
 Signature text,
 ForwardedEmail text,
 ForwardKeepCopy int,
 DaysInSpam int,
 DaysInTrash int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


#### After IP 2.0 and Smart Card Enhancement phase 2+3

$sql_table[] = "CREATE TABLE CARD_STUDENT_PRESET_LEAVE (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(11) NOT NULL,
 RecordDate date NOT NULL,
 DayPeriod int,
 TargetStatus int,
 Reason text,
 Remark text,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE StudentDatePeriod (StudentID, RecordDate, DayPeriod)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";



$sql_table[] = "CREATE TABLE CARD_STUDENT_ENTRY_LOG_REASON (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(11) NOT NULL,
 RecordDate date NOT NULL,
 Reason text,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE StudentDate (StudentID, RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE PAYMENT_PRINTING_PACKAGE (
 RecordID int(11) NOT NULL auto_increment,
 NumOfQuota int(11) NOT NULL default 0,
 Price float default 0,
 PurchasingItemName varchar(255),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX RecordStatus (RecordStatus)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PAYMENT_PRINTING_QUOTA (
 UserID int(11) NOT NULL,
 TotalQuota int(11),
 UsedQuota int(11),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(11),
 Year varchar(50),
 Semester varchar(50),
 FromScore int(11),
 ToScore int(11),
 ChangeUserID int(11),
 RelatedRecordID int(11),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX StudentYearSem(StudentID,Year,Semester)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE DISCIPLINE_SUB_SCORE_CHANGE_LOG (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(11),
 Year varchar(50),
 Semester varchar(50),
 ScoreType int(11),
 FromScore int(11),
 ToScore int(11),
 ChangeUserID int(11),
 RelatedRecordID int(11),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX StudentYearSemType(StudentID,Year,Semester,ScoreType),
 INDEX ScoreType(ScoreType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_TIME_SESSION (
 SessionID int(11) NOT NULL auto_increment,
 SessionName varchar(255),
 MorningTime time,
 LunchStart time,
 LunchEnd time,
 LeaveSchoolTime time,
 NonSchoolDay int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (SessionID),
 UNIQUE SessionName (SessionName)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_TIME_SESSION_REGULAR (
 RecordID int(11) NOT NULL auto_increment,
 ClassID int(11) default 0,
 DayType int,
 DayValue varchar(50),
 SessionID int(11),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE ClassDayTypeValue (ClassID,DayType,DayValue)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_TIME_SESSION_DATE (
 RecordID int(11) NOT NULL auto_increment,
 ClassID int(11) default 0,
 RecordDate date,
 SessionID int(11),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE ClassDate (ClassID,RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";



$sql_table[] = "CREATE TABLE DISCIPLINE_MERIT_RECORD_WAIVE (
 MeritRecordID int(11) NOT NULL,
 WaiveStatus int(11),
 PICID int(11),
 WaiveStartDate date,
 WaiveEndDate date,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (MeritRecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE DISCIPLINE_ACCU_CATEGORY (
 CategoryID int(11) NOT NULL auto_increment,
 Name varchar(255),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (CategoryID),
 UNIQUE Name (Name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_ACCU_CATEGORY_ITEM (
 ItemID int(11) NOT NULL auto_increment,
 CategoryID int(11) NOT NULL,
 Name varchar(255),
 DisplayOrder int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ItemID),
 UNIQUE CatName (CategoryID, Name),
 INDEX CategoryID (CategoryID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_ACCU_PERIOD (
 PeriodID int(11) NOT NULL auto_increment,
 DateStart date,
 DateEnd date,
 TargetYear varchar(100),
 TargetSemester varchar(100),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (PeriodID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING (
 SettingID int(11) NOT NULL auto_increment,
 CategoryID int(11) NOT NULL,
 PeriodID int(11) NOT NULL,
 UpgradeCount int(11) default 1,
 UpgradeToItemID int(11) NOT NULL,
 UpgradeDemeritType int(11) NOT NULL,
 UpgradeDemeritCount int(11) default 1,
 UpgradeDeductConductScore int(11) default 0,
 UpgradeDeductSubScore1 int(11) default 0,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (SettingID),
 UNIQUE CatPeriod (CategoryID, PeriodID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE DISCIPLINE_ACCU_RECORD (
 RecordID int(11) NOT NULL auto_increment,
 CategoryID int(11) NOT NULL,
 ItemID int(11) NOT NULL,
 RecordDate datetime NOT NULL,
 StudentID int(11) NOT NULL,
 Remark text,
 UpgradedRecordID int(11),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX StudentID (StudentID),
 INDEX CategoryID (CategoryID),
 INDEX ItemID (ItemID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE (
 RuleID int(11) NOT NULL auto_increment,
 CategoryID int(11) default NULL,
 PeriodID int(11) default NULL,
 RuleOrder int(11) default NULL,
 NextNumLate int(11) NOT NULL default 1,
 ProfileMeritType int(11) NOT NULL default -1,
 ProfileMeritNum int(11) NOT NULL default 0,
 ReasonItemID int(11) default NULL,
 Reason text,
 ConductScore int(11) default NULL,
 SubScore1 int(11) default NULL,
 DetentionMinutes int(11) NOT NULL default 0,
 DetentionReason text,
 RecordType int(11) default NULL,
 RecordStatus int(11) default NULL,
 DateInput datetime default NULL,
 DateModified datetime default NULL,
 PRIMARY KEY (RuleID),
 UNIQUE KEY RuleOrder (CategoryID,PeriodID,RuleOrder)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE PAYMENT_PURCHASE_DETAIL_RECORD (
 RecordID int(11) NOT NULL auto_increment,
 TransactionLogID int(11) NOT NULL,
 ItemName varchar(255),
 ItemQty int,
 ItemSubTotal float,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX TransactionLogID (TransactionLogID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE INTRANET_SMS2_SOURCE_MESSAGE (
 SourceID int(11) NOT NULL auto_increment,
 Content text,
 RecipientCount int default 0,
 FromModule varchar(100),
 PICType tinyint default 0,
 AdminPIC varchar(50),
 UserPIC int,
 TargetType int,
 IsIndividualMessage int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (SourceID),
 INDEX FromModule(FromModule)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE INTRANET_SMS2_MESSAGE_RECORD (
 RecordID int(11) NOT NULL auto_increment,
 SourceMessageID int(11),
 RecipientPhoneNumber varchar(20),
 RecipientName varchar(255),
 RelatedUserID int(11),
 Message text,
 ReferenceID varchar(20),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX SourceMessageID (SourceMessageID),
 INDEX RecordStatus (RecordStatus)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";



$sql_table[] = "CREATE TABLE INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE (
 TemplateID int(11) NOT NULL auto_increment,
 TemplateCode varchar(100) NOT NULL,
 Content varchar(255),
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (TemplateID),
 UNIQUE TemplateCode (TemplateCode)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE INTRANET_SMS2_MONTHLY_REPORT (
 RecordID int(11) NOT NULL auto_increment,
 Year int,
 Month int,
 MessageCount int default 0,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE YearMonth(Year, Month)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUPSTAFF (
 GroupStaffID int(11) NOT NULL auto_increment,
 EnrolGroupID int(11) default 0,
 UserID int(11) default 0,
 StaffType varchar(10) default NULL,
 INDEX EnrolGroupID (EnrolGroupID),
 PRIMARY KEY (GroupStaffID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUPCLASSLEVEL (
 GroupClassLvlID int(11) NOT NULL auto_increment,
 EnrolGroupID int(11) default 0,
 ClassLevelID int(11) default 0,
 INDEX EnrolGroupID (EnrolGroupID),
 INDEX ClassLevelID (ClassLevelID),
 PRIMARY KEY (GroupClassLvlID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUP_DATE (
 GroupDateID int(11) NOT NULL auto_increment,
 EnrolGroupID int(11) default 0,
 ActivityDateStart datetime default NULL,
 ActivityDateEnd datetime default NULL,
 INDEX EnrolGroupID (EnrolGroupID),
 PRIMARY KEY (GroupDateID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE INTRANET_ENROL_EVENTINFO (
 EnrolEventID int(11) NOT NULL auto_increment,
 Quota int(11) default 0,
 Approved int(11) default 0,
 EventTitle varchar(255) default NULL,
 Description text default NULL,
 UpperAge int(2) default NULL,
 LowerAge int(2) default NULL,
 Gender varchar(2) default NULL,
 AttachmentLink1 varchar(255) default NULL,
 AttachmentLink2 varchar(255) default NULL,
 AttachmentLink3 varchar(255) default NULL,
 AttachmentLink4 varchar(255) default NULL,
 AttachmentLink5 varchar(255) default NULL,
 ApplyStartTime datetime default NULL,
 ApplyEndTime datetime default NULL,
 ApplyUserType varchar(2) default NULL,
 ApplyMethod int(2) default NULL,
 EventCategory int(11),
 AddToPayment int(2) default 0,
 PaymentAmount float default 0,
 GroupID int(11) default 0,
 DateInput datetime default NULL,
 DateModified datetime default NULL,
 PRIMARY KEY (EnrolEventID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE INTRANET_ENROL_EVENTSTUDENT (
 EventStudentID int(11) NOT NULL auto_increment,
 StudentID int(11) NOT NULL default 0,
 EnrolEventID int(11) NOT NULL,
 RecordType char(2) default NULL,
 RecordStatus char(2) default NULL,
 CommentStudent text,
 Performance text,
 DateInput datetime default NULL,
 DateModified datetime default NULL,
 PRIMARY KEY (EventStudentID),
 INDEX StudentID (StudentID),
 INDEX EnrolEventID (EnrolEventID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENTSTAFF (
 EventStaffID int(11) NOT NULL auto_increment,
 EnrolEventID int(11) default 0,
 UserID int(11) default 0,
 StaffType varchar(10) default NULL,
 INDEX EnrolGroupID (EnrolEventID),
 PRIMARY KEY (EventStaffID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENTCLASSLEVEL (
 EventClassLvlID int(11) NOT NULL auto_increment,
 EnrolEventID int(11) default 0,
 ClassLevelID int(11) default 0,
 INDEX EnrolEventID (EnrolEventID),
 INDEX ClassLevelID (ClassLevelID),
 PRIMARY KEY (EventClassLvlID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENT_DATE (
 EventDateID int(11) NOT NULL auto_increment,
 EnrolEventID int(11) default 0,
 ActivityDateStart datetime default NULL,
 ActivityDateEnd datetime default NULL,
 INDEX EnrolEventID (EnrolEventID),
 PRIMARY KEY (EventDateID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUP_ATTENDANCE (
 GroupAttendanceID int(11) NOT NULL auto_increment,
 GroupDateID int(11) NOT NULL,
 GroupID int(11) NOT NULL,
 StudentID int(11) NOT NULL,
 SiteName varchar(255),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (GroupAttendanceID),
 INDEX GroupDateID (GroupDateID),
 INDEX GroupID (GroupID),
 INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENT_ATTENDANCE (
 EventAttendanceID int(11) NOT NULL auto_increment,
 EventDateID int(11) NOT NULL,
 EnrolEventID int(11) NOT NULL,
 StudentID int(11) NOT NULL,
 SiteName varchar(255),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (EventAttendanceID),
 INDEX EventDateID (EventDateID),
 INDEX EnrolEventID (EnrolEventID),
 INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE INTRANET_ENROL_USER_ACL (
 UserID int(11) NOT NULL,
 UserLevel int(11) default 0,
 UNIQUE UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE INTRANET_ENROL_CATEGORY (
 CategoryID int (8) NOT NULL auto_increment,
 CategoryName varchar(255),
 DisplayOrder int(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (CategoryID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE PAYMENT_SUBSIDY_UNIT (
 UnitID int(11) NOT NULL auto_increment,
 UnitName varchar(255),
 UnitCode varchar(100),
 TotalAmount float,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (UnitID),
 UNIQUE UnitName (UnitName)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE CARD_STUDENT_LEAVE_OPTION (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(11) NOT NULL,
 OptionType int(11),
 Weekday1 int(11),
 Weekday2 int(11),
 Weekday3 int(11),
 Weekday4 int(11),
 Weekday5 int(11),
 Weekday6 int(11),
 LastUpdateUser varchar(100),
 LastUpdateUserID int,
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE Student(StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$sql_table[] = "CREATE TABLE CARD_PATROL_SITE (
 SiteID int(11) NOT NULL auto_increment,
 SiteName varchar(20),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (SiteID),
 UNIQUE SiteInfo (SiteName)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$sql_table[] = "CREATE TABLE SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON (
 RecordID int(11),
 StudentID int(11),
 RecordDate date,
 DayType int,
 MedicalReasonType int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE StudentDateDayType (StudentID, RecordDate, DayType),
 INDEX RecordDate (RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

?>