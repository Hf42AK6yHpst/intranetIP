<?php 
/*************************************************************
 *	2012-11-13	YatWoon
 *				update StatusImg(), hide all status display (no private / public concept)
 *
 *  20101101 Marcus:
 * 		Add Batch Logic to save query
 * ************************************************************/
class libfile extends libdb{

	var $FileID;
	var $GroupID;
	var $UserID;
	var $UserName;
	var $UserEmail;
	var $Category;
	var $Title;
	var $Location;
	var $Keyword;
	var $Description;
	var $ReadFlag;
	var $RecordType;
	var $RecordStatus;
	var $DateInput;
	var $DateModified;
	var $FileType;
	var $PublicStatus;
	var $FolderID;
	var $UserTitle;
	var $Approved;
	var $onTop;
	var $CoverImg;

	# ------------------------------------------------------------------------------------

	function libfile($FileID=0, $FileIDArr= array()){
		$this->libdb();
		$row = $this->returnFile($FileID,$FileIDArr);
		
		$this->AssignToBatch($row);
		$this->LoadFileData($row[0][0]);
		
//		$this->FileID = $row[0][0];
//		$this->GroupID = $row[0][1];
//		$this->UserID = $row[0][2];
//		$this->UserName = $row[0][3];
//		$this->UserEmail = $row[0][4];
//		$this->Category = $row[0][5];
//		$this->Title = $row[0][6];
//		$this->Location = $row[0][7];
//		$this->Keyword = $row[0][8];
//		$this->Description = $row[0][9];
//		$this->ReadFlag = $row[0][10];
//		$this->RecordType = $row[0][11];
//		$this->RecordStatus = $row[0][12];
//		$this->DateInput = $row[0][13];
//		$this->DateModified = $row[0][14];
//		$this->FileType = $row[0][15];
//		$this->PublicStatus = $row[0][16];
//		$this->FolderID = $row[0][17];
//		$this->UserTitle = $row[0][18];
//		$this->Approved = $row[0][19];
//		$this->onTop = $row[0][20];
//		$this->CoverImg = $row[0][21];
	}

	function returnFile($FileID, $FileIDArr= array()){
		if(!empty($FileIDArr))
			$cond_FileID = " AND FileID IN (".implode(',',$FileIDArr).") ";
		else if(!empty($FileID))
			$cond_FileID = " AND FileID = $FileID ";
		else
			return false;
			
  		$sql = "SELECT FileID, GroupID, UserID, UserName, UserEmail, Category, Title, Location, Keyword, Description, ReadFlag, RecordType, RecordStatus, DateInput, DateModified, FileType, PublicStatus, FolderID, UserTitle, Approved, onTop, CoverImg FROM INTRANET_FILE WHERE 1 $cond_FileID";
  		
		return $this->returnArray($sql,22);
	}
	
	function AssignToBatch($arr)
	{
		foreach((array)$arr as $rec)
		{
			$this->BatchArr[$rec['FileID']] = $rec;
		}
	}
	
	function LoadFileData($FileID)
	{
		if(empty($this->BatchArr[$FileID]))
		{
			$row = $this->returnFile($FileID);
			$this->AssignToBatch($row);
		}
		
		$FileData = $this->BatchArr[$FileID];
		
		$this->FileID = $FileData[0];
		$this->GroupID = $FileData[1];
		$this->UserID = $FileData[2];
		$this->UserName = $FileData[3];
		$this->UserEmail = $FileData[4];
		$this->Category = $FileData[5];
		$this->Title = $FileData[6];
		$this->Location = $FileData[7];
		$this->Keyword = $FileData[8];
		$this->Description = $FileData[9];
		$this->ReadFlag = $FileData[10];
		$this->RecordType = $FileData[11];
		$this->RecordStatus = $FileData[12];
		$this->DateInput = $FileData[13];
		$this->DateModified = $FileData[14];
		$this->FileType = $FileData[15];
		$this->PublicStatus = $FileData[16];
		$this->FolderID = $FileData[17];
		$this->UserTitle = $FileData[18];
		$this->Approved = $FileData[19];
		$this->onTop = $FileData[20];
		$this->CoverImg = $FileData[21];
	}

	# ------------------------------------------------------------------------------------

	function GroupTitle(){
	    $sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID = '".IntegerSafe($this->GroupID)."'";
		$x = $this->returnArray($sql,1);
		return $x[0][0];
	}

	function UpdateReadFlag(){
		global $UserID;
		$sql = "UPDATE INTRANET_FILE SET ReadFlag = CONCAT(ReadFlag,';$UserID;') WHERE FileID = '".IntegerSafe($this->FileID)."'";
		$this->db_db_query($sql);
	}
	
	function FileUserTitle()
	{
		return $this->UserTitle ? $this->UserTitle : $this->Title;
	}
	
	function StatusImg()
	{
		/*
		global $LAYOUT_SKIN, $image_path, $eComm;
		return (!$this->PublicStatus) ? "<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_private.gif border=0 align=absmiddle title='". $eComm['Private'] ."' alt='". $eComm['Private'] ."'>" : "";
		*/
	}
	
	function getCommentNumber()
	{
	    $sql = "SELECT count(CommentID) FROM INTRANET_FILE_COMMENT WHERE FileID = '".IntegerSafe($this->FileID)."'";
		$x = $this->returnArray($sql,1);
		return $x[0][0];
	}
	
	function url_exists($url) 
	{
        $a_url = parse_url($url);
        if (!isset($a_url['port'])) $a_url['port'] = 80;
        $errno = 0;
        $errstr = '';
        $timeout = 30;
        
        $fid = @fsockopen($a_url['host'], $a_url['port'], $errno, $errstr, $timeout);
        if (!$fid) return false;
        $page = isset($a_url['path'])  ?$a_url['path']:'';
        $page .= isset($a_url['query'])?'?'.$a_url['query']:'';
        fputs($fid, 'HEAD '.$page.' HTTP/1.0'."\r\n".'Host: '.$a_url['host']."\r\n\r\n");
        $head = fread($fid, 4096);
        fclose($fid);
        return preg_match('#^HTTP/.*\s+[200|302]+\s#i', $head);
    }
    
    
	function FileTypeIcon()
	{
		global $LAYOUT_SKIN, $image_path;
		
		switch($this->FileType)
		{
			case "W":
				$x = "<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_website.gif border=0 align=absmiddle>";
				break;
			case "V":
				$x = "<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_video.gif border=0 align=absmiddle>";
				break;
			case "P":
				$x = "<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_photo.gif border=0 align=absmiddle>";
				break;
			default:
				$x = "<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_file.gif border=0 align=absmiddle>";
				break;
		}
		
		return $x;
	}
	
	function returnFolderName()
	{
	    $sql = "SELECT Title FROM INTRANET_FILE_FOLDER WHERE FolderID = '".IntegerSafe($this->FolderID)."'";
		$x = $this->returnArray($sql,1);
		return $x[0][0];
	}
	
	function viewFileRight()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $GroupID, $UserID;
		
		if($this->PublicStatus)
		{
			return true;
		}
		else
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
			include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
			$lu2007 	= new libuser2007($UserID);
			return $lu2007->isInGroup($GroupID);
		}
	}
	
	function returnFileType()
	{
		global $eComm;
		
		switch($this->FileType)
		{
			case "W":
				$x = $eComm['Website'];
				break;
			case "V":
				$x = $eComm['Video'];
				break;
			case "P":
				$x = $eComm['Photo'];
				break;
			default:
				$x = $eComm['File'];
				break;
		}
		
		return $x;
	}
	# ------------------------------------------------------------------------------------

}
?>