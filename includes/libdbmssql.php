<?php
// Editing by 

if (!defined("LIBDBMSSQL_DEFINED"))                     // Preprocessor directive
{
	define("LIBDBMSSQL_DEFINED", true);

	class libdbmssql 
	{

        var $sql;
        var $db;
        var $rs;
        var $ServerIP;
        var $UserName;
        var $UserPassword;
        var $Conn;
        var $Debug;

        function libdbmssql($db, $server_ip, $user, $password, $connect=false, $debug=false)
        {
			$this->db = $db;
			$this->ServerIP = $server_ip;
			$this->UserName = $user;
			$this->UserPassword = $password;
			
			if($connect){
				$this->opendb();
			}
			//$this->Conn = mssql_connect($this->ServerIP , $this->UserName, $this->UserPassword);				        	
			$this->Debug = $debug;
    	}
		
		function opendb()
		{
			$this->Conn = mssql_connect($this->ServerIP , $this->UserName, $this->UserPassword);
			return $this->Conn;
		}
		
		function closedb()
		{
			return @mssql_close();
		}
		
        function db_insert_id()
        {
        	$sql = "SELECT @@IDENTITY AS insert_id";
        	$result = $this->returnVector($sql);
        	
        	return $result[0];
        }

        function db_num_rows()
        {
            if ($this->rs)
        	{
        		return mssql_num_rows($this->rs);
        	}
        	else
        	{
        		return 0;
        	}            
        }

        function db_create_db($database)
        {
            $sql = 'CREATE DATABASE `'.$database.'`';
            return mssql_query($sql);
        }

        function db_db_query($query)
        {
			if ($this->Debug) {
					echo '<div id="SQL_Debug" style="display:none">'."\n";
					echo $query."\n";
					echo '</div>';
			}
				
            mssql_select_db($this->db) or exit(mssql_get_last_message ());
            return mssql_query($query);
        }

        function db_data_seek($row_number)
        {
                return mssql_data_seek($this->rs, $row_number);
        }

        function db_fetch_array($result_type=MSSQL_BOTH)
        {
                return mssql_fetch_array($this->rs,$result_type);
        }

        function db_free_result()
        {
                return mssql_free_result($this->rs);
        }

        function db_affected_rows()
        {
                 return mssql_rows_affected($this->Conn);
        }
        
        function Begin_Trans()	
        {
				   mssql_query('BEGIN TRAN');
				   return true;
		}
				   
		function Commit_Trans() 
		{ 
		   mssql_query('COMMIT TRAN');
		   return true;
		}
		
		function Rollback_Trans() 
		{
		   mssql_query('ROLLBACK TRAN');
		   return true;
		}

        function returnArray($sql, $field_no=null)
        {
			$i = 0;
			$this->rs = $this->db_db_query($sql);
			$x = array();
			if ($this->rs && $this->db_num_rows()!=0)
			{
				while ($row = $this->db_fetch_array())
				{
					$x[] = $row;
				}
				$this->db_free_result();
			}

			return $x;
		}

		function returnResultSet($sql, $field_no=null)
		{
			$i = 0;
			$this->rs = $this->db_db_query($sql);
			$x = array();
			if ($this->rs && $this->db_num_rows()!=0)
			{
				while ($row = $this->db_fetch_array(MSSQL_ASSOC))
				{
					$x[] = $row;
				}
				$this->db_free_result();
			}

			return $x;
		}

        #########################################################################
        # return 1-D array to store the sql result of selecting 1 column only
        #
        function returnVector($sql)
        {
                $i = 0;
                $x = array();
                $this->rs = $this->db_db_query($sql);
                if($this->rs && $this->db_num_rows()!=0){
                        while($row = $this->db_fetch_array()){
                                $x[$i] = $row[0];
                                $i++;
                        }
                }
                if ($this->rs)
                    $this->db_free_result();
                
                return $x;
        }


        function db_sub_select($sql)
        {
                $x = "";
                $row = $this->returnArray($sql,1);
                if (!$row) return 0;
                $delimiter = "";
                for($i=0; $i<sizeof($row); $i++)
                {
                    $x .= $delimiter.$row[$i][0];
                    $delimiter = ",";
                }
                if ($x == "") return 0;
                else return $x;
        }

        function Get_Safe_Sql_Query($value)
        {
	        return ereg_replace("'", "''", $value);
        }
        
        function Encode_Field($String) 
        {
        	return iconv('UTF-8', 'UCS-2LE//TRANSLIT', $String);
        }
        
        function Decode_Field($String) 
        {
        	return iconv('UCS-2LE', 'UTF-8//TRANSLIT', $String);
        }
	} // End of class
}        // End of directive
?>