<?php
# Modifying by : 

/*###***************** Change Log *****************###
#       Date:   2020-11-09 (Bill) modified displayFormat_eNoticedisplayNoticePrint(), to display scheulded send time    [2020-1009-1753-46096]  ($sys_custom['eDiscipline']['PushMsgReminder_ScheduledDelay'])
#       Date:   2020-11-06 (Bill) modified displayFormat_Split_PIC(), to add "Follow-up" to column "Action" [2020-0824-1151-40308]      ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#       Date:   2020-09-07 (Bill) modified displayFormat_displayTeacherView(), apply notice settings based on notice type   [2020-0604-1821-16170]
#		Date:	2020-04-15 (Bill & Philips) modified displayFormat_displayTeacherView(), displayFormat_eNoticedisplayMyNotice(), for specialNotice display in list table
#		Date:   2019-01-10 (Ray)  modified displayFormat_DetailedRecordByYear, displayFormat_DetailedRecordByYearWithReasonEditable with paging
#       Date:   2019-05-08 (Bill) modified displayFormat_eSportExportResult(), to Change Checkbox name "EventGroupID[]" to "EventGroupType[]"    [2019-0508-1200-59235]
#       Date:   2019-04-01 (Bill) added displayFormat_iPortfolio_DBS_Predicted_Grade_IB(), modified displayFormat_iPortfolio_DBS_Predicted_Grade()
#		Date:	2019-03-07 (Henry) modified displayFormat_eLibPlus_Circulation_Detail_Table() for adding class name and number
#		Date:	2019-02-01 (Carlos) modified displayFormat_SmartCardAttandencdListMonthly(), added and display attendance status. (dependency: /home/smartcard/attendance/attendance_record/attendance_list_monthly.php)
#       Date:   2018-11-12 (Cameron) consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#       Date:   2018-10-22 (Bill) added displayFormat_iPortfolio_DBS_Predicted_Grade()
#		Date:	2018-10-22 (Carlos) modified displayFormat_SmartCardTakeAttendance($display_period), added cust flag $sys_custom['SmartCardAttendance_SubmittedProveDocumentForEarlyLeave'] to show Submitted prove document of early leave option.
#       Date:   2018-10-03 (Philips) modified displayFormat_eEnrolmentGroupEnrolList() to display StudentGender
#		Date:	2018-08-03 (Carlos) modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance(), apply setting [iSmartCardDisableModifyStatusAfterConfirmed] to disable status change for confirmed attendance records.
#       Date:   2018-04-04 (Cameron) add following layout functions, no navigation and sorting function, target for ajax.
#               column_4ajax(), column_4ajax(), prev_n_4ajax(), next_n_4ajax(), go_page_4ajax(), page_size_input_4ajax(), view_type_input_4ajax(), displayFormat_4ajax()
#       Date:   2018-03-23 (Bill) modified displayFormat_Split_PIC(), to handle PIC field too long problem     [2018-0314-1225-25054]
#		Date:	2018-03-12 (Carlos) modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance(), enable/disable to manage [Submitted Prove Document] according to setting.
#       Date:   2018-03-07 (Cameron) add $sys_custom['ListTableDisplayPageMin'] to page_size_input()   
#		Date:	2018-01-08 (Anna) modified displayFormat_eDisciplineDetentionTakeAttendance() - added $plugin['eClassTeacherApp']
#		Date:	2017-11-27 (Cameron) configure for Oaks refer to Amway (don't show target type)
#		Date:	2017-11-22 (Carlos) modified displayFormat_SmartCardTakeGroupAttendance(), do not check is student helper for group attendance.
#		Date:	2017-11-16 (Henry) add para $c at constructor libdbtable2007()
#		Date:	2017-10-13 (Bill) modified displayFormat_eDisciplineConductMarkView(), to support Total Gain & Total Deduct column including Manual Adjustment		[2017-1004-0907-59206]	($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns_IncludeAdjust'])
#		Date:	2017-10-04 (Bill) modified displayFormat_eDisciplineConductMarkView(), to support pop-up layer for Total Gain & Total Deduct column		[2017-1004-0907-59206]
#		Date:	2017-08-15 (Omas) modified displayFormat_Enrolment_Display_Event_ReplySlip_Cust()
#		Date:	2017-08-14 (Cameron) add constructor libdbtable2007() to supress error in cloud site that use nginx (case #Q121726)
#		Date:	2017-08-08 (Carlos) modified displayPlain(), added column_formatter to modify the cell display value with dynamic function.
#		Date:	2017-07-20 (Cameron) modify displayFormat_with_multiple_values_in_a_column(), add parameter $subitem_array
#		Date:	2017-06-28 (Bill) 	modified displayFormat_Split_PIC(), to support Study Score and Activity Score column 
#		Date:	2017-06-19 (Villa)  #P110324  - modified displayFormat_Enrolment_Display_Event_ReplySlip_Cust change thichbox to dynamic
#		Date:	2017-05-26 (Carlos) $sys_custom['StudentAttendance']['RecordBodyTemperature'] modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance(), display body temperature.
#		Date:	2017-05-24 (Anna) - $sys_custom['eEnrolment']['ShowStudentNickName'] modified displayFormat_eEnrolmentGroupEnrolList(), displayFormat_Enrolment_Display_Club()
#		Date:	2017-05-17 (Carlos) - $sys_custom['StudentAttendance']['NoCardPhoto'] modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance() to display no bring card photo.
#		Date:	2017-04-28 (Paul)
#				- modifed displayFormat_eBook_license_table() for displaying one-off / expiry info during book selection process
#		Date:	2017-04-24 (Villa) #P110324 
#				add displayFormat_Enrolment_Display_Event_ReplySlip_Cust() for ReplySlip Cust
#		Date:	2017-04-12 (Carlos) modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance(), display [Waived], [Reason], [Teacher's remark] according to settings, added [Submitted Prove Document], added teacher's remark selection.
#		Date:	2017-03-28 (Carlos) modified displayFormat_UserMgmtStudentNoClass(), cater column offset for $sys_custom['BIBA_AccountMgmtCust'].
#		Date:	2017-03-16 (Bill)	[2016-1207-1221-39240]
#				added displayFormat_Activity_Score_Management(), for Mgmt > Activity Score	($sys_custom['UseActScore'])
#		Date:	2017-03-02 (Carlos) Modified displayFormat_iMail_Gamma(), handle special characters in sender name. 
#		Date:	2017-02-21 (Cameron)
#				add function displayFormat_with_multiple_values_in_a_column()	
#		Date:	2016-01-10 (Bill)	[2016-0823-1255-04240]
#				modified displayFormat_eDisciplineDetentionDetail(), to display student photo in detention session details page	($sys_custom['eDiscipline']['DetentionSessionDisplayStdPhoto'])
#		Date:	2016-01-09 (Bill)	[2016-0823-1255-04240]
#				modified displayFormat_Split_PIC(), to display Cancel Request message box
#		Date:	2016-11-09 (Cameron)
#				add function displayFormat_RecommendBook_table() [case #E107674]
#		Date:	2016-10-17 (Bill) [2016-1013-1541-17214]
#				modified displayFormat_displayTeacherView(), to handle display for approval user under approval tab
#	    Date:	2016-10-05 (Villa) #S105745
#				modified displayFormat_IP25_table(), make thead only include displayColumn
#				modified displayFormat_displayTeacherView(), make thead only include displayColumn
#		Date:	2016-10-03 (Ivan) [ip.2.5.7.10.1]: modified displayFormat_UserMgmtStaffAccount(), displayFormat_UserMgmtParentAccount(), displayFormat_UserMgmtStudentAccount() to fix thead tab for the Chrome printing duplication problem
#		Date:	2016-09-29 (Bill) modified displayFormat_eDisciplineConductMarkView(), displayFormat_eDisciplineConductMarkAdjust(), to display hyperlink if only misconduct have deducted marks ($sys_custom['eDiscipline']['GMConductMark']) [2016-0808-1526-52240]
#		Date:	2016-09-12 (Bill) modified displayFormat_displayTeacherView(), displayFormat_eNoticedisplayMyNotice(), for eNotice topic show hyperlink ($sys_custom['eNotice']['eServiceShowHyperlink']) [2016-0907-1010-32206]
#		Date:	2016-08-17 (Bill) modified displayFormat_eNoticedisplayNoticePrint(), to display Send Time column for discipline notice	[2016-0801-1134-09054]
#		Date: 	2016-08-16 (Ronald) add displayFormat_UserMgmtStaffAccountCentennial(), for display account mgmt table in centennial.
#		Date: 	2016-08-09 (Carlos) modified displayFormat_UserMgmtStudentAccount(), cater field index shifting for $sys_custom['BIBA_AccountMgmtCust']. 
#		Date:	2016-07-11 (Kenneth) modified displayFormat_displayTeacherView(), to allow approver to payment notice
#		Date:	2016-07-05 (Carlos) [ip2.5.7.7.1] $sys_custom['StudentAttendance']['AllowEditTime'] - modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance(), allow edit in time.
# 		Date:	2016-05-19 (Kenneth) modified displayFormat_displayTeacherView(), to allow approver to edit notice
#		Date:	2016-05-03 (Bill) [ip2.5.7.7.1]: modified displayFormat_displayTeacherView(), to allow eNotice admin to edit payment notice	[2016-0429-0912-09066]
#		Date:	2016-04-08 (Bill) [ip2.5.7.3.1]: modified displayFormat_eDisciplineCaseRecord(), added prefix style to teacher name of deleted account [2016-0224-1423-31073]
#		Date:	2016-04-07 (Bill) [ip2.5.7.4.1]: modified displayFormat_displayTeacherView(), display empty cell for Signed/Total Column if current user has Issue Right
#		Date:	2016-04-06 (Bill) [ip2.5.7.4.1]: modified displayFormat_Split_PIC(), added link to student name for Personal Report redirect [2015-0807-1048-01073]
#		Date:	2016-03-02 (Bill) [ip2.5.7.3.1]: modified displayFormat_eDisciplineCaseRecord(), to display PIC name for removed teacher [2016-0224-1423-31073]
#		Date:	2016-01-13 (Cameron) don't show targetType for Amway in displayFormat_displayStudentView() and displayFormat_displayStudentHistory() (case#P91044)
#		Date:	2015-12-11 (Omas) [ip2.5.7.1.1]: modified displayFormat_UserMgmtStudentNoClass(),displayFormat_eInventoryList() - replace deprecated split() by explode() for PHP 5.4 
#		Date:	2015-11-04 (Bill) [ip2.5.7.1.1]: modified displayFormat_Split_PIC() to hide eNotice link for Student Prefect - HKUGA Cust	[2015-0416-1040-06164]
#		Date:	2015-10-27 (Omas) [ip2.5.7.1.1]: fix : displayFormat_Enrolment_Display_Club() cannot display cannot display member list when member more than 50 - #88073
#		Date:	2015-10-06 (Bill): modified displayFormat_Split_PIC() to replace deprecated split() by explode() for PHP 5.4
#		Date:	2015-09-24 (Bill): modified displayFormat_eDisciplineDetentionDetail() to show Attendance Status & Attendance Remark [2015-0924-1046-51071]
# 		Date:	2015-09-02 (Cameron): add function displayFormat_eBook_license_table() to show book cover image before book title
#		Date:	2015-08-25 (Omas): modified displayFormat_GroupSettingGroupList() - fix cannot display admin for other academic year
# 		Date:	2015-08-06 (Evan): modified displayFormat_Enrolment_Display_Club(), take out the get class and class number sql command from the loop to improve performance
#		Date:	2015-07-27 (Bill): modified displayFormat_Split_PIC(), added displayFormat_Split_PIC_Rehabilitation() - $sys_custom['eDiscipline']['CSCProbation'] [2015-0611-1642-26164]
#		Date:	2015-07-27 (Bill): modified displayFormat_displayTeacherView() to reduce sql number when display PICs of eNotice for teachers [2015-0428-1214-11207]
#		Date:	2015-07-14 (Carlos): modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance(), disable all editable fields if disallow to edit n days before records. 
#		Date:	2015-06-29 (Omas): modified displayFormat_Enrolment_Display_Club() - moved 1 sql out of the $j forloop reduce queries
#		Date:	2015-05-28 (Bill): modified displayFormat_eDisciplineDetentionSetting() to display correct assign number under "Session Management > Session" [2015-0528-0910-00207]
#		Date:	2015-05-18 (Bill): modified displayFormat_displayTeacherView() to display PICs of eNotice [2015-0428-1214-11207]
#		Date:	2015-05-15 (Carlos) [ip2.5.6.8.1]: modified displayFormat_iMail_Gamma() and displayFormat_iMail_Gamma_Search(), handle UT and (GMT) in datetime string. 
#		Date:	2015-04-30 (Bill) [ip2.5.6.5.1]: modified displayFormat_Split_PIC(), to add "Send push message" to column "Action" [2014-1216-1347-28164]
#		Date:	2015-04-14 (Bill) [ip2.5.6.3.1]: modified displayFormat_displayTeacherView() to support eNotice PIC display logic [2015-0323-1602-46073]
#		Date:	2015-03-31 (Carlos)[ip2.5.6.3.1]: modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance(), Office Remark is for display only.
#		Date: 	2015-02-13 (Carlos)[ip2.5.6.3.1]: modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance(), added [Office Remark]
#	    Date:	2015-02-09 (Ryan) 	[ej5.0.5.2.1]:	
# 							DM#521 : Show Remarks line break, added displayFormat_eLibPlus_Search_User_Table()
#		Date:	2015-01-22 (Bill) [ip.2.5.6.3.1]:	Fix: modified displayFormat_displayParentHistory(), allow parent to sign enotice - All School Notice [Case #C74362]
#		Date:	2015-01-20 (Bill) [ip.2.5.6.3.1]:	Fix: modified displayFormat_displayParentHistory(), allow parent to sign enotice when not allow late signing [Case #B74229]
#		Date:	2014-11-21 (Omas) [ip2.5.5.12.1]:	Fix: Staff Mgmt Sorting by Classname. displayFormat_UserMgmtStaffAccount() - j=3 will not get ClassName
#		Date:	2014-11-11 (Omas) [ip2.5.5.12.1]:	Improve: displayFormat_UserMgmtParentAccount(), displayFormat_UserMgmtStudentAccount() - no more sql inside loop 
#		Date:	2014-11-04 (Omas) [ip2.5.5.12.1]:	add SQL to get&show Admin for each group in displayFormat_GroupSettingGroupList()
#		Date:	2014-10-10 (Carlos) [ip2.5.5.10.1] : Modified displayFormat_iMail_Gamma(), for draft and sent box emails, merge to & cc to be displayed
#		Date:	2014-10-07 (YatWoon)
#				Revised page_size_input(), add config maxDisplayPage [Case#U69108 ]
#				Deploy: IPv2.5.5.10.1
#		Date:	2014-09-30 (Bill):	modified displayFormat_displayTeacherView() to show column Signed/Total when "staffview" is turned on
#		Date:	2014-06-26 (Carlos): $sys_custom['eDiscipline']['PooiToMiddleSchool'] - modified displayFormat_eDisciplineGMMisCatSetting() to skip Period setting column 		
#		Date:	2014-06-20 (Carlos): modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance(), add $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] for the session step value
#		Date:	2014-04-11 (YatWoon): modified displayFormat_eDisciplineDetentionDetail(), incorrect row# checking that display the icon for empty remark
#		Date:	2014-03-17 (Carlos): modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance() - added Absent Session for $sys_custom['SmartCardAttendance_StudentAbsentSession2']
#		Date:	2013-09-10 (Siuwan): added displayFormat_eLibPlus_Circulation_Detail_Table()
#		Date:	2013-07-17 (Carlos): modified displayFormat_eDisciplineConductMarkView() for $sys_custom['eDiscipline']['yy3'], append * to Grade if down graded
#		Date:	2013-07-11 (Henry):	added displayFormat_Digital_Archive_File_Format_Table()
#		Date:	2013-03-27 (Ivan):	modified displayFormat_MessageCenter_SmsIndex() to change the array index of user id field from 3 to 2 
#		Date:	2013-03-25 (YatWoon): add displayFormat_StudentRegistry_StudentList_HK()
#		Date:	2013-03-21 (Ivan): added displayFormat_MessageCenter_SmsIndex()
#		Date:	2012-02-20 (YatWoon): modified displayFormat_iPortfolioStudentOLE1(), add nowrap to the cell [Case#2013-0220-1443-06054]
#		Date:	2012-06-06 (Thomas) 
#				added function displayFormat_ePost_portfolio_report_new()
#
#		Date:	2013-01-31 (YatWoon): modified displayFormat_displayParentHistory(), displayFormat_displayParentView(), use "signerid" to detect the notice is signed or not [Case#2013-0123-1122-59066]
#		Date:	2012-10-30 (YatWoon): modified displayFormat_Study_Score_Management(), display layer for study score details
#		Date: 	2012-10-25 (Rita): modified displayFormat_UserMgmtStudentAccount() for adding parenet column
#		Date:	2012-08-15 (YatWoon): update displayFormat_eCommForumList(), remove cell 1 overflow display
#		Date:	2012-07-19 (Carlos): Modified displayFormat_iMail_Gamma_Search() to set search fields to file cache
#		Date:	2012-07-17 (Carlos):
#				change download_attachment.php target to target_e at displayFormat_ReadingScheme_RecommendBookCoverView()
# 		
#		Date:	2012-06-06 (Thomas) 
#				added function displayFormat_ePost_report(), displayFormat_ePost_portfolio_report(), displayFormat_ePost_article_shelf()
#
#		Date:	2012-05-14 (Marcus)
#				modified displayFormat_ReadingScheme_RecommendBookReport(), fixed no response when click the book name
#
#		Date:	2012-05-02 (Carlos)
#				modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance() to handle html special chars for reason and remark
#
#		Date:	2012-04-30 (Carlos)
#				modified displayFormat_iMail_Gamma(), for Sent and Draft folder, display as many recipient as it could be (with tooltip to show all recipients) 
# 
#		Date:	2012-04-27 (Marcus)
#				update displayFormat_ReadingScheme_RecommendBookCoverView, cater book report setting.
#
#		Date:	2012-04-13 (YatWoon)
#				update displayFormat_displayParentView(), fixed the checking for singer [Case#2012-0413-1011-18132]
#
#		Date:	2012-02-28 (Henry Chow)
#				update displayFormat_Invoice_List_Table(), display 2 more columns in "Invoice Mgmt System"
#				
#		Date:	2012-02-16 (YatWoon)
#				update displayFormat_displayStudentHistory(), add back isLateSignAllow checking for student notice
#				update displayFormat_displayParentHistory()
#
#		Date:	2012-02-15 (YatWoon)
#				update displayFormat_UserMgmtStudentNoClass(), no need add checking for row[0] to display hyperlink in English Name column
#
#		Date:	2012-01-10 (YatWoon)
#				update displayFormat_UserMgmtStudentNoClass(), display lastyear and lastclass with group_concat function
#
#		Date:   2011-12-09 (Henry Chow)
#				added displayFormat_eDisciplineDetentionStudentRecordInStudentView, for "eService > eDis > Detention"
#
#		Date:   2011-12-02 (Connie)
#				added displayFormat_ERC_Rubrics_CommentBank_SubjectDetailsComment
#
#		Date:	2011-11-16 (Carlos)
#				modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance(), added early leave small icon with info shown as tooltip
#
#		Date:	2011-11-01 [YatWoon]
#				modified displayFormat_eCommShareList_4(), pass FileID directly to function DisplayPhoto()
#
#		Date:	2011-10-24 [Henry Chow]
#				modified displayFormat_Digital_Archive_Admin_Search_Result_Table(), change hyperlink of file
#
#		Date:	2011-10-18	[Marcus]
#				modified displayFormat_ReadingScheme_RecommendBookCoverView(), cater category default answer sheet
#
#		Date:	2011-10-03	[Carlos]
#				modified displayFormat_iMail_Gamma_Search(), fix sent folder and draft folder to display Recipient, not sender From
#
#		Date:	2011-09-30	[Henry Chow]
#				modified displayFormat_UserMgmtStudentNoClass(), revise the display of "Last Year" & "Last Class"
#
#		Date:	2011-09-17	[YatWoon]
#				modified displayFormat_eCommShareList_4(), update Waiting Approval file display
#
#		Date:	2011-09-14	[YatWoon]
#				modified displayFormat_displayParentHistory(), displayFormat_displayStudentHistory(), fixed: check for end date with "23:59:59"
#
#		Date:	2011-08-09	[Henry Chow]
#				modified displayFormat_Enrolment_Display_Club(), display class name / class no. according to the AcademicYearID
#
#		Date:	2011-07-12	[Henry Chow]
#				modified displayFormat_DigitalArchive_Newest_More_Table(), display "Like" option 
#
#		Date:	2011-06-21	[Henry Chow]
#				modified displayFormat_Digital_Archive_Admin_Search_Result_Table(), display column "FileName"
#
#		Date:	2011-06-15	[Ivan]
#				modified displayFormat_OEA_StudentList() to show the Participation Nature
#
#  		Date:	2011-06-14  [Carlos]
#  				modified displayFormat_iMail_Gamma_Search() to use cached mails for searching
#
#		Date:	2011-05-30	[YatWoon]
#				update displayFormat_eInventoryList(), replace "Remark" wordings
#
#		Date:	2011-05-25	[henry chow]
#				modified displayFormat_OEA_StudentList(), display row #
#
#		Date:	2011-05-25	[Marcus]
#				modified displayFormat_ReadingScheme_RecommendBookCoverView(), cater online writing
#
#		Date:	2011-05-24	[YatWoon]
#				new displayFormat_Enrolment_Display_Club()
#
#		Date:	2011-05-09	[Henry Chow]
#				new displayFormat_Digital_Archive_Admin_Search_Result_Table(), display search result in "Digital Archive > Admin Doc"
#
#		Date:	2011-05-09	[Henry Chow]
#				new displayFormat_Study_Score_Management(), display "Study Score" management
#
#		Date:	2011-05-09	[Henry Chow]
#				new displayFormat_DigitalArchive_Newest_More_Table(), display "More" newest record in Digital Archive (Student View)
#
#		Date:	2011-05-09	[Henry Chow]
#				modified displayFormat_eDisciplineDetentionList(), rearrange the display priority of "Status"
#
#		Date:	2011-05-04	[Henry Chow]
#				modified displayFormat_Split_PIC(), display "Dentention" only if the RecordStatus=1
#
#		Date:	2011-04-20	[Henry Chow]
#				modified displayFormat_OEA_StudentList(), display blank lines to fill up to 20
#
#		Date:	2011-04-14	[Carlos]
#				modified displayFormat_iMail_Gamma(), added readed counting column for Sent Folder
#
#		Date:	2011-04-11	[Henry Chow]
#				modified displayFormat_eDisciplineDetentionSetting(), allow to edit detention session of current day even start time has been passed
#
#		Date:	2011-04-07	[Henry Chow]
#				modified displayFormat_OEA_StudentList(), remove table footer
#
#		Date:	2011-03-24 [Henry Chow]
#				displayFormat_eDiscipline_WebSAMS_NotTransferredIndex(), display eDis AP record which not yet transferred to WebSAMS
#
#		Date:	2011-03-23	[Marcus]
#				modified displayFormat_ReadingScheme_MyReadingBook(), add linkage to ebook
#
#		Date:	2011-03-23	[Henry Chow]
#				added displayFormat_eDiscipline_GM_Index(), GM Mgmt index page 
#
#		Date:	2011-03-22	[Henry Chow]
#				modified displayFormat_eDisciplineDetentionList(), can assign to detention session even start time is passed on current date
#
#		Date:	2011-03-08	[Henry Chow]
#				modified displayFormat_OEA_StudentList(), add column "last modified"  
#
#		Date:	2011-02-23 [Carlos]
#				added displayFormat_ReadingScheme_RecommendBookReport(), for Reading Scheme
#
#		Date:	2011-02-23	Marcus
#				added displayFormat_ReadingSchemeReportComment(), for Reading Scheme
#
#		Date:	2011-02-22	[Henry Chow]
#				added displayFormat_OEA_StudentList(), for iPortfolio > OEA > Student OEA List  
#
#		Date:	2011-02-08	YatWoon
#				update displayFormat_UserMgmtStudentNoClass(), Fixed: <tr'> that broken the layout
#
#		Date:	2011-01-31 [Kenneth Chung]
#				modified displayFormat_SmartCardTakeAttendance, block waive option if is student helper login
#
#		Date:	2011-01-27 [Henry Chow]
#				modified displayFormat_eDisciplineCaseRecord(), display "student involved" in eDis > Case Record
#
#		Date:	2011-01-25 [Carlos]
#				Added storing sorted search mail result at displayFormat_iMail_Gamma_Search()
#
#		Date:	2011-01-20 [Marcus]
#				added displayFormat_ReadingScheme_RecommendBookCoverView()
#
#		Date:	2011-01-12 [Henry Chow]
#				added displayFormat_eDisciplineDetentionDetail(), fix js error in detention detail page
#
#		Date:	2011-01-11 [Marcus]
#				added displayFormat_ReadingScheme_MyReadingBook()
#
#		Date:	2011-01-11 [Henry Chow]
#				update displayFormat_eDisciplineDetentionStudentRecord(), add demerit icon in "Attendance" column
#
#		Date:	2011-01-10 [YatWoon]
#				update displayFormat_UserMgmtStaffAccount(),displayFormat_UserMgmtStudentAccount(), changed to IP25 UI standard
#
#		Date:	2011-01-06 [YatWoon]
#				update displayFormat_UserMgmtParentAccount(), changed to IP25 UI standard
#
#		Date: 	 2010-11-11 (Carlos)
#		Details: replace datetime string which with timezone UT to UTC at displayFormat_iMail_Gamma() and displayFormat_iMail_Gamma_Search()  
#
#		Date:	2010-10-29 [YatWoon]
#				update displayFormat_Split_PIC(), correct the hyperlink of "Case"
#
#		Date:	2010-10-29 [YatWoon]
#				update displayFormat_displayTeacherView(), displayFormat_displayParentView(),displayFormat_displayStudentView(),
#				parent / student notice
#
#		Date:	2010-10-26 [YatWoon]
#				[eNotice] update displayFormat_displayTeacherView(), hidden del checkbox if the user is not a admin
#
#		Date: 2010-10-26 (Ronald)
#		Details : modified displayFormat_iMail(), remove intranet_undo_htmlspecialchars() & stripslashes()
#
#		Date: 2010-10-20 (Henry Chow)
#		Details : modified displayFormat_UserMgmtStaffAccount(), revise display order of "English Name" in Staff Mgmt   
#
#		Date: 2010-10-18 (Henry Chow)
#		Details : modified displayFormat_Split_PIC(), revise the checking on display "Detention" in index page (both AP and GM)  
#
#		Date: 2010-10-14 (Kenneth Chung)
#		Details : modified displayFormat_SmartCardAttandencdList() for remove LIMIT to allow attendance list more than 50 row
#
#		Date: 2010-10-13 (Ivan)
#		Details : modified displayFormat_displayTeacherView(), displayFormat_displayStudentView(), displayFormat_displayParentView(), 
#				  displayFormat_displayStudentHistory(), displayFormat_displayParentHistory(), displayFormat_displayAllNotice() for eNotice performance tunning
#
#		Date: 2010-10-08 (Henry Chow)
#		Details : modified displayFormat_eNoticedisplayNoticePrint(), change <table> class
#
#		Date: 2010-10-07 (Carlos)
#		Details : added data column Size to displayFormat_iMail_Gamma() and displayFormat_iMail_Gamma_Search()
#
#		Date: 2010-10-07 (Ronald)
#		Details : modified displayFormat_iMail(), add intranet_undo_htmlspecialchars() & stripslashes() to remove special character before display in HTML format
#
#		Date: 2010-09-30 (Henry)
#		Details : Modified displayFormat_Split_PIC(), apply "Sign Notice" wording to eService > eDisciplinev12
#
#		Date: 2010-09-15 (Max) [201009141747]
#		Details : Modified displayFormat_module_manage_license to display in new format
#
#		Date: 2010-09-10 (Henry Chow)
#		Details : modified displayFormat_meritType(), add column of "Approval Group" 
#
#		Date: 2010-09-09 (Carlos)
#		Details : modified displayFormat_SmartCardTakeAttendance() and displayFormat_SmartCardTakeGroupAttendance(), change session interval from 1 to 0.5
#		
#		Date: 2010-09-03 (Henry Chow)
#		Details : modified displayFormat_displayAllNotice(), allow parent/student can view notice when select "All school notices" in eService
#
#		Date: 2010-08-24 (Henry Chow)
#		Details : added displayFormat_StudentRegistry_StudentList_Malaysia(), display Student list 
#
#		Date: 2010-08-20 (Henry Chow)
#		Details : modified displayFormat_StudentRegistry_StudentList(), add checking on display of "iPortfolio" column 
#
#		Date: 2010-08-05 (Henry Chow)
#		Details : modified displayFormat_Malaysia_RegistryStatus(), for "Student Registry Status" (Malaysia) 
#
#		Date: 2010-08-03 (Henry Chow)
#		Details : modified displayFormat_StudentRegistry_StudentList(), control the display of iPortfolio column in php, not in this function 
#
#		Date: 2010-07-30 (Henry Chow)
#		Details : modified displayFormat_StudentRegistry_StudentList(), display person of last modify 
#
#		Date: 2010-07-28 (Marcus)
#		Details : modified displayFormat_GroupSettingGroupList , check eEnrollment flag before getting Enrol Group Data
#
#		Date: 2010-07-23 (Jason Lam)
#		Details : modified the function displayFormat_IP25_table() to support dynamic TR Class Style according to the sql value
#					adding a new Variable 'trCustClass' to allow user to set each style of each Row in dbtable list
#					How to set the trCustClass values?
#					Ans: in the select sql query, add a new column field when prepare the sql for dbtable 
#						i.e. sql = " select ..., ..., if(SubjectGroupID != '', 'row_avaliable', 'row_approved') as trCustClass from ..."
#						then the function would recognize this variable 'trCustClass' and apply it if set
#
#		Date: 2010-07-22 (Henry Chow)
#		Details : modified displayFormat_eDisciplineGMGoodCatSetting(), remove "TargetYear" as 1 of the criteria in SQL
#
#		Date: 2010-07-20 (Henry Chow)
#		Details : added displayFormat_StudentRegistry_StudentList(), Student Registry Student list
#
#		Date: 2010-07-20 (Kenneth Chung)
#		Details : edited displayFormat_SmartCardTakeAttendance()/ displayFormat_SmartCardTakeGroupAttendance(), 
#							Display Last Modified Time and User info
#							Should be avaliable after version 1.8.1 (after July School wish list done)
#
#		Date: 2010-07-20 (Henry Chow)
#		Details : added displayFormat_IP25_table(), new IP25 table layout format
#
#		Date: 2010-07-19 (Marcus)
#		Details : added displayFormat_GroupSettingGroupList(), fixed wrong number of member
#
#		Date: 2010-07-16 (Henry Chow)
#		Details : modified displayFormat_UserMgmtStudentNoClass(), display English Name in correct order
#
#		Date: 2010-06-23 (Marcus)
#		Details : modified displayFormat_iMail_Gamma & displayFormat_iMail_Gamma_Search
#					Show Date only for mails today
#
#		Date: 2010-06-22 (Henry Chow)
#		Details : modified displayFormat_eDisciplineConductMarkView(), add AcademicYearID as the criteria to get the Conduct Mark
#
#		Date: 2010-06-21 (Henry Chow)
#		Details : added displayFormat_UserMgmtStudentAccount(), display "Account Mgmt > Student Account"
#
#		Date:	2010-06-18	YatWoon
#			update displayFormat_displayTeacherView(), Admin can edit notice even if the notice is not created by own
#
#		Date: 	2010-06-03 [YatWoon] 
#				update displayFormat_eDisciplineDetentionStudentRecord(), displayFormat_eDisciplineDetentionDetail(), displayFormat_eDisciplineDetentionTakeAttendance() add "Created by" column
#
# 
# 		Date: 2010-05-19 Max (201005181005)
# 			Detatils: Added function displayFormat_module_manage_license()
#
#		Date: 2010-05-13 (Sandy)
#		Details : mod displayFormat_eLib_manage_license(), add color to links
#
#		Date: 2010-05-10 [YatWoon]
#		Details:	Add PIC column in displayFormat_eDisciplineDetentionTakeAttendance() <== display PIC in take attendance list
#					update displayFormat_Split_PIC() <== GM/AP list: Display approved by, released by, rejected by.... data
#	
#		Date: 2010-05-07 (Henry Chow)
#		Details : modified displayFormat_UserMgmtParentAccount(), display "Account Mgmt > Parent Account"
#
#		Date: 2010-05-04 (Marcus)
#		Details : imail gamma:	display mail date instead of internal date
#
#		Date: 2010-05-03 (YatWoon)
#		Details : eNotice:	Parent allow sign past notice
#							Can edit past notice (just only some fields)
#
#		Date: 2010-04-26 (Henry)
#		Details : added displayFormat_UserMgmtStaffAccount(), display "Account Mgmt > Staff Account"
#
#		Date: 2010-04-26 (Henry)
#		Details : modified displayFormat_eDisciplineDetentionStudentRecord(), displayFormat_eDisciplineDetentionNewRecord(), displayFormat_eDisciplineDetentionDetail
#				  display "Remark" in icon and 2) display "PIC" column (Record PIC, not Session PIC)
#
#		Date: 2010-04-20 (Henry)
#		Details : add displayFormat_eAttendance_ParentLetter(),to display "eAttendance" > "Absent without parent letter"
#
#		Date: 2010-04-15 (Marcus)
#		Details : add IMap_Decode_Folder_name to displayFormat_iMail_Gamma & displayFormat_iMail_Gamma_Search
#
#		Date: 2010-04-15 (Henry)
#		Details : added displayFormat_RepairSystem() to display eAdmin>ResourcesMgmt>Repair System
#	
#		Date: 2010-04-14 (Sandy)
#		Details : added displayFormat_eLib_manage_license() to display elibrary student book license management page
#	
#		Date: 2010-04-07 (Henry)
#		Details :	modified displayFormat_Split_PIC() to display Munsang Customized New Leaf Record
#
#		Date: 2010-03-16 (Ivan)
#		Details :	added GET_TABLE_ACTION_BTN() to generate the icons at the top right of the dbtable
#
#		Date: 2010-03-08 (Marcus)
#		Details :	modified displayFormat_AwardPunishPromotion(), add param $mtype ro getPromotionMethod
#
#		Date: 2010-02-08 (Henry)
#		Details :	modified displayFormat_Number_Item(), add status of "in use" / "not in use" to display cateogry/item
#
#		Date: 2010-02-03 (Marcus)
#		Details :	Added displayFormat_iMail_Gamma()
#
#		Date: 2010-02-03 (Ronald)
#		Details :	Modified displayFormat_displayTeacherView(), correct the folder path (/notice/payment_notice/) if it is a payment notice
#
#		Date: 2010-01-26 (Marcus)
#		Details :	Modified displayFormat_AwardPunishPromotion in order to show conversion period 
#
#		Date: 2010-01-25 (Henry)
#		Details :	Modified displayFormat_displayTeacherView(), add checking on "Module" whether the record is "Payment" notice
#
#		Date: 2010-01-12 (Henry)
#		Details :	Modified displayFormat_eDisciplineGMMisNewLeafSetting(), displayFormat_eDisciplineGMMisCatSetting() & displayFormat_eDisciplineGMGoodCatSetting(), get the start date & end date of academic year
#
#		Date: 20090408 (Ronald)
#		Details :	Modified Function displayFormat_eDisciplineConductMarkView(), if calculate by year, then the conduct mark will accumlate from the last term.
#
#		Date: 20090408 (Ronald)
#				displayFormat_iMail (Still under modifing, please let me know if there is any issue)
#
###**********************************************#*/

	
	class libdbtable2007 extends libdbtable
	{
	
		var $row_valign;
		var $n_start;
		var $rs;
		var $no_col;
		var $row_alt = array("#F9DEAD", "");
		var $table_tag;
		var $no_msg;
		var $page_size;
		var $noNumber;
		var $navigationHTML;
		var $PageSizeInterval = 10;
		var $additionalCols = 0;
		var $forApproval = 0;
	
	        function libdbtable2007($f, $o, $p, $c=false){
				parent::libdbtable($f, $o, $p, $c);
	        }
	
	        function libdbtable($f, $o, $p){
	                $this->libdbtable($f, $o, $p);
	        }
	
	
	        function returnColumnClassID(){
	                        if ($this->IsColOff=="eCommForumList")
	                    $x = "forumtabletoplink";
	            else
	                                $x = "tabletoplink";
	
	                return $x;
	        }
	
	        function returnCellClassID()
	        {
	            switch ($this->IsColOff)
	            {
	                                case "imail": $x = ""; break;
	                                case "imail_gamma": $x = ""; break;
	                                case "imail_gamma_search": $x = ""; break;
	                                case "eDisciplineDetentionSetting":  $x = ""; break;
	                                case "UserMgmtStaffAccountCentennial": $x = ""; break;
	                                default: $x = "tabletext tablerow"; break;
	            }
	                        return $x;
	        }
	
	
	                function column($field_index, $field_name){
	
	                global $image_path, $LAYOUT_SKIN;
	
	                if ($this->IsColOff=="staff")
	                {
	                        $image_path="/images/staffattend";
	                }
	
	                $x = "";
	                if($this->field==$field_index)
	                {
	                        //if($this->order==1) $x .= "<a class=\"".$this->returnColumnClassID()."\" href=\"javascript:sortPage(0,$field_index,document.".$this->form_name.")\" onMouseOver=\"window.status='".$this->sortby." $field_name';MM_swapImage('sort_icon','','$image_path/{$LAYOUT_SKIN}/icon_sort_d_on.gif',1);return true;\" onMouseOut=\"window.status='';MM_swapImgRestore();return true;\" title=\"".$this->sortby." $field_name\" >";
	                        //if($this->order==0) $x .= "<a class=\"".$this->returnColumnClassID()."\" href=\"javascript:sortPage(1,$field_index,document.".$this->form_name.")\" onMouseOver=\"window.status='".$this->sortby." $field_name';MM_swapImage('sort_icon','','$image_path/{$LAYOUT_SKIN}/icon_sort_a_on.gif',1);return true;\" onMouseOut=\"window.status='';MM_swapImgRestore();return true;\" title=\"".$this->sortby." $field_name\" >";

	                                                //Change arrow display
	                        if($this->order==1) $x .= "<a class=\"".$this->returnColumnClassID()."\" href=\"javascript:sortPage(0,$field_index,document.".$this->form_name.")\" onMouseOver=\"window.status='".$this->sortby." $field_name';MM_swapImage('sort_icon','','$image_path/{$LAYOUT_SKIN}/icon_sort_a_on.gif',1);return true;\" onMouseOut=\"window.status='';MM_swapImgRestore();return true;\" title=\"".$this->sortby." $field_name\" >";
	                        if($this->order==0) $x .= "<a class=\"".$this->returnColumnClassID()."\" href=\"javascript:sortPage(1,$field_index,document.".$this->form_name.")\" onMouseOver=\"window.status='".$this->sortby." $field_name';MM_swapImage('sort_icon','','$image_path/{$LAYOUT_SKIN}/icon_sort_d_on.gif',1);return true;\" onMouseOut=\"window.status='';MM_swapImgRestore();return true;\" title=\"".$this->sortby." $field_name\" >";

	                } else
	                {
	                        $x .= "<a class=\"".$this->returnColumnClassID()."\" href=\"javascript:sortPage($this->order,$field_index,document.".$this->form_name.")\" onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\" title=\"".$this->sortby." $field_name\" >";
	                }
	                                         $x .= str_replace("_", " ", $field_name);
	                if ($this->field==$field_index)
	                {
	
	                                        //if($this->order==1) $x .= "<img name='sort_icon' id='sort_icon' src='$image_path/{$LAYOUT_SKIN}/icon_sort_d_off.gif' align='absmiddle' border='0' />";
	                                        //if($this->order==0) $x .= "<img name='sort_icon' id='sort_icon' onMouseOver=\"MM_swapImage('sort_asc','','$image_path/{$LAYOUT_SKIN}/icon_sort_a_on.gif',1)\" onMouseOut='MM_swapImgRestore()' src='$image_path/{$LAYOUT_SKIN}/icon_sort_a_off.gif' align='absmiddle'  border='0' />";
	
	                                        //Change arrow display
	                                        if($this->order==1) $x .= "<img name='sort_icon' id='sort_icon' src='$image_path/{$LAYOUT_SKIN}/icon_sort_a_off.gif' align='absmiddle' border='0' />";
	                                        if($this->order==0) $x .= "<img name='sort_icon' id='sort_icon' onMouseOver=\"MM_swapImage('sort_asc','','$image_path/{$LAYOUT_SKIN}/icon_sort_d_on.gif',1)\" onMouseOut='MM_swapImgRestore()' src='$image_path/{$LAYOUT_SKIN}/icon_sort_d_off.gif' align='absmiddle'  border='0' />";
	                }
	                $x .= "</a>";
	                return $x;
	        }
	        
			function displayColumn($css_temp='')
			{
				global $image_path;
				
				$css_temp = ($css_temp != "") ? $css_temp : "tabletop";
				
				$x .= "<tr class='{$css_temp}'>\n";
				$x .= ($this->IsColOff==1 || $this->IsColOff==4 || $this->IsColOff==8 ) ? "<td width=15>\n" :"";
				$x .= ($this->IsColOff=="imail" ) ? "<td width='1'>&nbsp;</td>\n":"";
				$x .= $this->column_list;
				$x .= ($this->IsColOff == 12) ? $this->column_list : "" ;
				$x .= ($this->IsColOff==1 || $this->IsColOff==4 || $this->IsColOff==8 ) ? "<td width=15>\n":"";
				$x .= ($this->IsColOff=="imail") ? "<td width='1'>&nbsp;</td>\n":"";
				$x .= "</tr>\n";
				return $x;
			}        
			/*
			### commented by Henry @ 20090624
	        function displayColumn($css_temp='')
	        {
	                global $image_path;
	
	                if($css_temp)
	                	$css = "class='{$css_temp}'";
	
	                $x .= "<tr $css>\n";
	                $x .= ($this->IsColOff==1 || $this->IsColOff==4 || $this->IsColOff==8 ) ? "<td width=15>\n" :"";
	                $x .= ($this->IsColOff=="imail") ? "<td width='1'>&nbsp;</td>\n":"";
	                $x .= $this->column_list;
	                $x .= ($this->IsColOff == 12) ? $this->column_list : "" ;
	                $x .= ($this->IsColOff==1 || $this->IsColOff==4 || $this->IsColOff==8 ) ? "<td width=15>\n":"";
	                $x .= ($this->IsColOff=="imail") ? "<td width='1'>&nbsp;</td>\n":"";
	                $x .= "</tr>\n";
	
	                return $x;
	        }
	        */
	
	        // For customized headers
	        function displayColumnCustom()
	        {
	
	            $x = $this->column_list;
	
				return $x;
	        }
	
	
	        function record_range()
	        {
	             global $i_email_message, $i_adminmenu_group, $Lang;
	
	            //$n_title = $this->title;
	            $n_title = ($this->title == '')? $Lang['General']['Record'] : $this->title;
	            $n_start = $this->n_start+1;
	            $n_end = min($this->total_row,($this->pageNo*$this->page_size));
	            $n_total = $this->total_row;
	
	            switch ($this->IsColOff)
	            {
	                case "imail":
	                case "imail_gamma":
	                case "imail_gamma_search":
	                        $x = "$i_email_message $n_start - $n_end / ".$this->total." $n_total";
	                        break;
	                case "imail_addressbook":
	                        $x = "$i_adminmenu_group $n_start - $n_end / ".$this->total." $n_total";
	                        break;
	                default:
	                        //$x = "( $n_start - $n_end ) / $n_total";
	                        //$x = "$n_title ( $n_start - $n_end ) / ".$this->total." $n_total";
	                        $x = "$n_title $n_start - $n_end, ".$this->total." $n_total";
	                        break;
	            }
	
	            return $x;
	        }
	
	
	        function prev_n()
	        {
	
	                global $image_path, $image_pre, $LAYOUT_SKIN;
	
	                                if ($this->IsColOff=="staff")
	                                {
	                                         $image_path="/images/staffattend";
	                                         $image_pre = "$image_path/previous_icon.gif";
	                                }
	                                if ($this->IsColOff == 2 || $this->IsColOff == 5)
	                                {
	                    $previous_icon = "<img src=$image_path/previous_icon.gif border=0 hspace=2 vspace=0 align=middle>";
	                }
	                else if ($this->IsColOff == "imail_recipient_selection")
					{
		                $previous_icon = "<img src=$image_path/btn_pre.gif border=0 hspace=2 vspace=0 align=middle title='".$this->pagePrev."'  >";
		            }
	                else
	                {
	                    $previous_icon = "<img src=$image_pre border=0 hspace=2 vspace=0 align=middle>";
	                }
	                $n_page = $this->pageNo;
	                $n_total = ceil($this->total_row/$this->page_size);
	                $n_size = $this->page_size;
	                if ($this->IsColOff == "imail_recipient_selection")
		            {
		            	$x = ($n_page==1) ? "&nbsp;".$previous_icon."&nbsp;" :
		            			"<a href=\"javascript:".$this->form_name."_gopage(".($n_page-1).",document.".$this->form_name.")\" class='tablebottomlink' >&nbsp;".$previous_icon."&nbsp;</a> ";
		            }
		            else
		            {
	                	$x = ($n_page==1) ? "&nbsp;<img src='{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif' name='page_previous' id='page_previous' width='11' height='10' border='0' hspace='3' align='absmiddle' />&nbsp;" :
	                                        "<a href=\"javascript:".$this->form_name."_gopage(".($n_page-1).",document.".$this->form_name.")\" class='tablebottomlink' onMouseOver=\"MM_swapImage('page_previous','','{$image_path}/{$LAYOUT_SKIN}/icon_prev_on.gif',1)\" onMouseOut='MM_swapImgRestore()'>&nbsp;<img src='{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif' name='page_previous' id='page_previous' width='11' height='10' border='0' align='absmiddle' title='".$this->pagePrev."' />&nbsp;</a> ";
					}
	
	                return $x;
	        }
	
	
	        function next_n(){
	
	                global $image_path,$image_next, $LAYOUT_SKIN;
	
	                                if ($this->IsColOff=="staff")
	                                {
	                                                $image_path="/images/staffattend";
	                                                $image_next="$image_path/next_icon.gif";
	                                }
	
	                if ($this->IsColOff == 2 || $this->IsColOff == 5)
	                {
	                    $next_icon = "<img src=$image_path/next_icon.gif border=0 hspace=2 vspace=0 align=middle>";
	                }
	                else if ($this->IsColOff == "imail_recipient_selection")
					{
		                $next_icon = "<img src=$image_path/btn_next.gif border=0 hspace=2 vspace=0 align=middle title='".$this->pageNext."'  >";
		            }
	                else
	                {
	                    $next_icon = "<img src=$image_next border=0 hspace=2 vspace=0 align=middle>";
	                }
	                $n_page = $this->pageNo;
	                $n_total = ceil($this->total_row/$this->page_size);
	
	                $n_size = $this->page_size;
	
	                if ($this->IsColOff == "imail_recipient_selection")
		            {
		                $x = ($n_page==$n_total) ? "&nbsp;".$next_icon."&nbsp;" :
		                			"<a href=\"javascript:".$this->form_name."_gopage(".($n_page+1).",document.".$this->form_name.")\" class='tablebottomlink' >&nbsp;".$next_icon."&nbsp;</a> ";
		            }
		            else
		            {
	                	$x = ($n_page==$n_total) ? "&nbsp;<img src='{$image_path}/{$LAYOUT_SKIN}/icon_next_off.gif' name='page_next' id='page_next' width='11' height='10' border='0' hspace='3' align='absmiddle' />&nbsp;" :
	                                        "<a href=\"javascript:".$this->form_name."_gopage(".($n_page+1).",document.".$this->form_name.")\" class='tablebottomlink' onMouseOver=\"MM_swapImage('page_next','','{$image_path}/{$LAYOUT_SKIN}/icon_next_on.gif',1)\" onMouseOut='MM_swapImgRestore()'>&nbsp;<img src='{$image_path}/{$LAYOUT_SKIN}/icon_next_off.gif' name='page_next' id='page_next' width='11' height='10' border='0' align='absmiddle' title='".$this->pageNext."' />&nbsp;</a> ";
					}
	
	                return $x;
	        }
	
	
	        function go_page(){
	                $n_page=$this->pageNo;
	                $n_total=ceil($this->total_row/$this->page_size);
	                $x = $this->pagename." <select class='formtextbox' onChange=\"this.form.".$this->pageNo_name.".value=this.options[this.selectedIndex].value;this.form.submit();\">\n";
	                for($i=1;$i<=$n_total;$i++)
	                $x .= ($n_page==$i) ? "<option value=\"$i\" selected>$i</option>\n" : "<option value=\"$i\">$i</option>\n";
	                $x .= "</select>\n";
	                return $x;
	        }
	
	 		function go_page_ebooking(){
	                $n_page=$this->pageNo;
	                $n_total=ceil($this->total_row/$this->page_size);
	                $x = $this->pagename." <select class='formtextbox' onChange=\"this.form.".$this->pageNo_name.".value=this.options[this.selectedIndex].value; SetCookies(); AssignCookies(); this.form.submit();\">\n";
	                for($i=1;$i<=$n_total;$i++)
	                $x .= ($n_page==$i) ? "<option value=\"$i\" selected>$i</option>\n" : "<option value=\"$i\">$i</option>\n";
	                $x .= "</select>\n";
	                return $x;
	        }
	        
	        function page_size_input ()
	        {
	                 global $i_general_EachDisplay ,$i_general_PerPage, $sys_custom;
	
	                 $x = "{$i_general_EachDisplay} <select name='num_per_page' class='formtextbox' onChange='this.form.pageNo.value=1;this.form.page_size_change.value=1;this.form.numPerPage.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
	                 if($sys_custom['ListTableDisplayPageMin'])
	                 {
	                     $x .= "<option value='". $sys_custom['ListTableDisplayPageMin'] ."' ".($this->page_size== $sys_custom['ListTableDisplayPageMin'] ? "SELECTED":"").">". $sys_custom['ListTableDisplayPageMin'] ."</option>\n";
	                 }
	                 $x .= "<option value='10' ".($this->page_size==10? "SELECTED":"").">10</option>\n";
	                 $x .= "<option value='20' ".($this->page_size==20? "SELECTED":"").">20</option>\n";
	                 $x .= "<option value='30' ".($this->page_size==30? "SELECTED":"").">30</option>\n";
	                 $x .= "<option value='40' ".($this->page_size==40? "SELECTED":"").">40</option>\n";
	                 $x .= "<option value='50' ".($this->page_size==50? "SELECTED":"").">50</option>\n";
	                 $x .= "<option value='60' ".($this->page_size==60? "SELECTED":"").">60</option>\n";
	                 $x .= "<option value='70' ".($this->page_size==70? "SELECTED":"").">70</option>\n";
	                 $x .= "<option value='80' ".($this->page_size==80? "SELECTED":"").">80</option>\n";
	                 $x .= "<option value='90' ".($this->page_size==90? "SELECTED":"").">90</option>\n";
	                 $x .= "<option value='100' ".($this->page_size==100? "SELECTED":"").">100</option>\n";
	                 if($sys_custom['ListTableDisplayPageMax'])
	                 {
 	                 	$x .= "<option value='". $sys_custom['ListTableDisplayPageMax'] ."' ".($this->page_size== $sys_custom['ListTableDisplayPageMax'] ? "SELECTED":"").">". $sys_custom['ListTableDisplayPageMax'] ."</option>\n";	
	                 }
	                 $x .= "</select> $i_general_PerPage";	
	                 return $x;
	        }
	        
	        function page_size_input_ebooking ()
	        {
	                 global $i_general_EachDisplay ,$i_general_PerPage;
	
	                 $x = "{$i_general_EachDisplay} <select name='".$this->numPerPage_name."' id='".$this->numPerPage_name."' class='formtextbox' onChange='document.".$this->form_name.".".$this->pageNo_name.".value=1; SetCookies(); AssignCookies(); this.form.submit();'>\n";
	                 $x .= "<option value='10' ".($this->page_size==10? "SELECTED":"").">10</option>\n";
	                 $x .= "<option value='20' ".($this->page_size==20? "SELECTED":"").">20</option>\n";
	                 $x .= "<option value='30' ".($this->page_size==30? "SELECTED":"").">30</option>\n";
	                 $x .= "<option value='40' ".($this->page_size==40? "SELECTED":"").">40</option>\n";
	                 $x .= "<option value='50' ".($this->page_size==50? "SELECTED":"").">50</option>\n";
	                 $x .= "<option value='60' ".($this->page_size==60? "SELECTED":"").">60</option>\n";
	                 $x .= "<option value='70' ".($this->page_size==70? "SELECTED":"").">70</option>\n";
	                 $x .= "<option value='80' ".($this->page_size==80? "SELECTED":"").">80</option>\n";
	                 $x .= "<option value='90' ".($this->page_size==90? "SELECTED":"").">90</option>\n";
	                 $x .= "<option value='100' ".($this->page_size==100? "SELECTED":"").">100</option>\n";
	                 $x .= "</select> $i_general_PerPage";
	                 return $x;
	        }
	
	
	        function navigation($bgcolor="")
	        {
	            global $image_path, $LAYOUT_SKIN;
	            global $pageSizeChangeEnabled, $viewTypeChangeEnabled;
	
	            $x  = "<table width='100%' border='0' cellpadding='3' cellspacing='0' class='tabletext'>\n";
	            $x .= "<tr>\n";
	            $x .= "<td width='100%' >\n";
	            $x .= $this->record_range();
	            $x .= "</td><td nowrap='nowrap'>\n";
	            $x .= $this->prev_n();
	            $x .= $this->go_page();
	            $x .= $this->next_n();
	            $x .= "&nbsp; &nbsp; &nbsp;";
	
	            if ($pageSizeChangeEnabled)
	            {
	                $x .= $this->page_size_input();
	            }
	            if ($viewTypeChangeEnabled)
	            {
	                $x .= $this->view_type_input();
	            }
	            $x .= "&nbsp;</td>\n";
	            $x .= "</tr>\n";
	            $x .= "</table>\n";
	            $x .= "<script language='javascript'>
	                                                function ".$this->form_name."_gopage(page, obj)
	                                                {
	                                                obj.".$this->pageNo_name.".value=page;
	                                                obj.submit();
	                                                }
	                            </script>\n
	                            ";
	
	            return $x;
	        }
	        
	        function navigation_eBooking($bgcolor="")
	        {
	            global $image_path, $LAYOUT_SKIN;
	            global $pageSizeChangeEnabled, $viewTypeChangeEnabled;
	
	            $x  = "<table width='100%' border='0' cellpadding='3' cellspacing='0' class='tabletext'>\n";
	            $x .= "<tr>\n";
	            $x .= "<td width='100%' >\n";
	            $x .= $this->record_range();
	            $x .= "</td><td nowrap='nowrap'>\n";
	            $x .= $this->prev_n();
	            $x .= $this->go_page_ebooking();
	            $x .= $this->next_n();
	            $x .= "&nbsp; &nbsp; &nbsp;";
	            
	            if ($pageSizeChangeEnabled)
	            {
	                //$x .= $this->page_size_input();
	                $x .= $this->page_size_input_ebooking();
	            }
	            if ($viewTypeChangeEnabled)
	            {
	                $x .= $this->view_type_input();
	            }
	            $x .= "&nbsp;</td>\n";
	            $x .= "</tr>\n";
	            $x .= "</table>\n";
	            $x .= "<script language='javascript'>
	                                                function ".$this->form_name."_gopage(page, obj)
	                                                {
	                                                	AssignCookies();
	                                                	SetCookies();
	                                                	obj.".$this->pageNo_name.".value=page;
	                                                	obj.submit();
	                                                }
	                            </script>\n
	                            ";
	
	            return $x;
	        }
	
	        function navigation_imailSelection($bgcolor="")
	        {
	            global $image_path, $LAYOUT_SKIN;
	            global $pageSizeChangeEnabled, $viewTypeChangeEnabled;
	/*
	          <tr align="center" bgcolor="#E8F2A6">
	            <td colspan="5">1 - 2, �`�� 2 <img src="images/icon_arrowleft.gif" width="18" height="18" align="absmiddle">�e�@��
	              ��@��<img src="images/icon_arrowright.gif" width="18" height="18" align="absmiddle">
	              ��
	              <select name="select2">
	              </select>
	              �C�������������������������      <select name="select">
	              </select>
	              �� </td>
	          </tr>
	*/
	            $x  = "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='body'>\n";
	            $x .= "<tr>\n";
	            $x .= "<td width='100%' >\n";
	            $x .= $this->record_range();
	            $x .= "</td><td nowrap='nowrap'>\n";
	            $x .= $this->prev_n();
	            $x .= $this->go_page();
	            $x .= $this->next_n();
	            $x .= "&nbsp; &nbsp; &nbsp;";
	
	            if ($pageSizeChangeEnabled)
	            {
	                $x .= $this->page_size_input();
	            }
	            if ($viewTypeChangeEnabled)
	            {
	                $x .= $this->view_type_input();
	            }
	            $x .= "&nbsp;</td>\n";
	            $x .= "</tr>\n";
	            $x .= "</table>\n";
	            $x .= "<script language='javascript'>
							function ".$this->form_name."_gopage(page, obj)
							{
						        obj.".$this->pageNo_name.".value=page;
						        obj.submit();
							}
	            		</script>\n
	            		";
	
	            return $x;
	        }
	
	        function column_image($field_index, $field_name, $field_image)
	        {
	            global $image_path, $LAYOUT_SKIN;
	            $x = "";
	            if($this->field==$field_index)
	            {
	                    if($this->order==1) $x .= "<a class=\"".$this->returnColumnClassID()."\" href=javascript:sortPage(0,$field_index,document.".$this->form_name.") onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
	                    if($this->order==0) $x .= "<a class=\"".$this->returnColumnClassID()."\" href=javascript:sortPage(1,$field_index,document.".$this->form_name.") onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
	            }else{
	                    $x .= "<a class=".$this->returnColumnClassID()." href=javascript:sortPage($this->order,$field_index,document.".$this->form_name.") onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
	            }
	            $x .= $field_image;
	            if($this->field==$field_index)
	            {
	                    //if($this->order==1) $x .= "<img name='sort_icon' id='sort_icon' src='$image_path/{$LAYOUT_SKIN}/icon_sort_d_off.gif' align='absmiddle' border='0' />";
	                    //if($this->order==0) $x .= "<img name='sort_icon' id='sort_icon' onMouseOver=\"MM_swapImage('sort_asc','','$image_path/{$LAYOUT_SKIN}/icon_sort_a_on.gif',1)\" onMouseOut='MM_swapImgRestore()' src='$image_path/{$LAYOUT_SKIN}/icon_sort_a_off.gif' align='absmiddle'  border='0' />";
	
	                    //Change arrow display
	                    if($this->order==1) $x .= "<img name='sort_icon' id='sort_icon' src='$image_path/{$LAYOUT_SKIN}/icon_sort_a_off.gif' align='absmiddle' border='0' />";
	                    if($this->order==0) $x .= "<img name='sort_icon' id='sort_icon' onMouseOver=\"MM_swapImage('sort_asc','','$image_path/{$LAYOUT_SKIN}/icon_sort_d_on.gif',1)\" onMouseOut='MM_swapImgRestore()' src='$image_path/{$LAYOUT_SKIN}/icon_sort_d_off.gif' align='absmiddle'  border='0' />";
	
	                    //if($this->order==1) $x .= "<img src='$image_path/desc.gif' hspace=2 border=0>";
	                    //if($this->order==0) $x .= "<img src='$image_path/asc.gif' hspace=2  border=0>";
	            }
	            $x .= "</a>";
	            return $x;
	        }
			
			function GET_TABLE_ACTION_BTN($ParArr=array())
			{
				$x = "";
				global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;
				
				$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
				$x .= "<tbody>";
				$x .= "<tr>";
				$x .= "<td width=\"21\"><img src=\"".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/table_tool_01.gif\" height=\"23\" width=\"21\"></td>";
				$x .= "<td background=\"".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/table_tool_02.gif\">";
				$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\">";
				$x .= "<tbody>";
				$x .= "<tr>";
				
				$numOfBtn = count($ParArr);
				for($i = 0; $i < $numOfBtn; $i++)
				{
					$JS = $ParArr[$i][0];
					$Img = $ParArr[$i][1];
					$ImgName = $ParArr[$i][2];
					$Title = $ParArr[$i][3];
					
					$x .= "<td nowrap=\"nowrap\">";
					$x .= "<a href=\"".$JS."\" class=\"tabletool\">";
					$x .= "<img src=\"".$Img."\" name=\"".$ImgName."\" align=\"absmiddle\" border=\"0\" height=\"12\" width=\"12\">";
					$x .= $Title;
					$x .= "</a></td>";
					
					if($i < count($ParArr) - 1)
					$x .= "<td><img src=\"".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/10x10.gif\" width=\"5\"></td>";
				}
				
				$x .= "</tr>";
				$x .= "</tbody>";
				$x .= "</table>";
				$x .= "</td>";
				$x .= "<td width=\"6\"><img src=\"".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/table_tool_03.gif\" height=\"23\" width=\"6\"></td>";
				$x .= "</tr>";
				$x .= "</tbody>";
				$x .= "</table>";
				
				return $x;
			}
	
	        function displayFormat2($width="", $tablecolor=""){
	                global $image_path, $LAYOUT_SKIN;
	                $i = 0;
	                $DisplayIndex = ($this->HideIndex) ? false : true;
	                $TotalColumn = ($DisplayIndex) ? $this->no_col : $this->no_col-1;
	                
	                $this->rs = $this->db_db_query($this->built_sql());
	                $width = ($width!="") ? $width : "100%";
	
	                $n_start = $this->n_start;
	                $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	                $x .= $this->displayColumn();
	                if ($this->db_num_rows()==0)
	                {
//	                        $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$TotalColumn."'>".$this->no_msg."</td></tr>\n";
                            $x .= "<tr><td height='80' class='tabletext td_no_record' align='center' colspan='".$TotalColumn."'>".$this->no_msg."</td></tr>\n";
                            $x .= '<tr><td colspan="'.$TotalColumn.'" class="dotline"><img src="/images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td></tr>'."\n";
	                } else
	                {
	                        while ($row = $this->db_fetch_array())
	                        {
	                                $i++;
	                                $css = ($i%2) ? "1" : "2";
	                                if ($i>$this->page_size)
	                                {
	                                        break;
	                                }
	                                $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                                if ($DisplayIndex)
	                                {
	                                	$x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                                }
	                                for ($j=0; $j<$this->no_col-1; $j++)
	                                {
	
	                                        $x .= $this->displayCell($j, $row[$j]);
	                                }
	                                $x .= "</tr>\n";
	                        }
	                }
	                if($this->db_num_rows()<>0 && $this->with_navigation) $x .= "<tr><td colspan='".$TotalColumn."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	
	                return $x;
	        }
	
	
	
	        # ------------------------------------------------------------------------------------
	        # Special display for iMail
	        function displayFormat_iMail($fb="",$fb2="")
	        {
	        		global $image_path, $LAYOUT_SKIN;
					global $i_campusmail_lettertext;
					global $intranet_root;
					include_once($intranet_root."/includes/libcampusmail.php");
					include_once($intranet_root."/includes/libwebmail.php");
					$lcampusmail = new libcampusmail();
					$lwebmail = new libwebmail();
					$i = 0;
					
					if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID))
				    {
				        $noWebmail = false;
				    }
				    else
				    {
				        $noWebmail = true;
				    }
	
					$this->rs = $this->db_db_query($this->built_sql());
	
					$n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	
					$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
					$x .= $this->displayColumn();
	
					if ($this->db_num_rows()==0)
					{
//							$x .= "<tr>
//										<td class='iMailrow tabletext' ><img src='$image_path/space.gif' width='1' height='1' border='0'></td>
//	                                    <td class='iMailrow tabletext' align='center' colspan='".($this->no_col-2)."' ><br />".$this->no_msg."<br /><br></td>
//	                                    <td class='iMailrow tabletext' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>
//									</tr>\n
//									";
							$x .= "<tr><td align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td></tr>\n";
                            $x .= '<tr><td colspan="'.($this->no_col).'" class="dotline"><img src="/images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td></tr>'."\n";
					} else {
							while($row = $this->db_fetch_array())
							{
								### Debug ###
								/*
								$x .= "<tr>";
								for($j=0; $j<$this->no_col-2; $j++)
	                			{
	                				$x .= "<td>".$row[$j]."</td>";
	                			}
	                			$x .= "</tr>";
	                			*/
							
									$i++;
	
									if($i>$this->page_size) break;
	                                $x .= "<tr>\n";
	
	                                if (($row[$this->no_col-2] == 1) || ($row[$this->no_col-2] == 2))
									{
											$x .= "<td class='iMailrow' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>\n";
	                    			} else {
	                            			$x .= "<td class='iMailrowunread' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>\n";
	                        		}
	                				for($j=0; $j<$this->no_col-2; $j++)
	                				{
	                             			if (trim($row[$j]) == "")
	                             			{
	                                     			$row[$j] = "&nbsp;";
	                             			}
	
	                             			if (!$noWebmail){
	                             				if(($row[$this->no_col] == 'INBOX') || ($row[$this->no_col] == 'OTHERS')){
	                             					$SenderNameColnum = 4;
	                             				}
	                             				if($row[$this->no_col] == 'TRASH'){
	                             					$SenderNameColnum = 2;
	                             				}
	                             				if($row[$this->no_col] == 'SEARCH'){
	                             					$SenderNameColnum = 3;
	                             				}
	                             			}else{
	                             				if(($row[$this->no_col] == 'INBOX') || ($row[$this->no_col] == 'OTHERS')){
	                             					$SenderNameColnum = 3;
	                             				}
	                             				if($row[$this->no_col] == 'TRASH'){
	                             					$SenderNameColnum = 2;
	                             				}
	                             				if($row[$this->no_col] == 'SEARCH'){
	                             					$SenderNameColnum = 3;
	                             				}
	                             			}
	                             			$AddressErrorMsg = array('UNEXPECTED_DATA_AFTER_ADDRESS','.SYNTAX-ERROR.','INVALID_ADDRESS');
	                             			
	                 						if($j == $SenderNameColnum){
	                 							$DisplayName = "";
	                 							$formList = array();
	                 							$fromList = imap_rfc822_parse_adrlist($row[$j],"");
	                 							if(sizeof($fromList) > 0)
	                 							{
		             								for ($a=0; $a< sizeof($fromList); $a++) {
														if (!in_array($fromList[$a]->personal,$AddressErrorMsg) && !in_array($fromList[$a]->mailbox,$AddressErrorMsg) && !in_array($fromList[$a]->host,$AddressErrorMsg)) {
															if(trim($fromList[$a]->host) != ""){
																if (!isset($fromList[$a]->personal)) {
																	if(trim($fromList[$a]->host)!=""){
																		$DisplayName = $fromList[$a]->mailbox."@".$fromList[$a]->host;
																	}else{
																		### for internal mail use only, as the query will pass a username into here
																		$DisplayName = $row[$j];
																	}
																} else {
																	$DisplayObject = imap_mime_header_decode(imap_utf8($fromList[$a]->personal));
																	$DisplayName = $DisplayObject[0]->text;
																}
															}else{
																$DisplayName = $row[$j];
															}
		         										}
		         										else
		         										{
		         											$DisplayName = $row[$j];
		         										}
		             								}
		             							}
		             							else
		             							{
		             								$DisplayName = $row[$j];
		             							}
	                 							if($row[$this->no_col-2] == 1)
	                 								$row[$j] = "<span class='iMailsender' >".$DisplayName."</span>";
	                 							elseif($row[$this->no_col-2] == 2)
	                 								$row[$j] = "<span class='iMailsender' >".$DisplayName."</span>";
	                 							elseif(($row[$this->no_col-2] == "")||($row[$this->no_col-2] == 0))
	                 								$row[$j] = "<span class='iMailsenderunread' >".$DisplayName."</span>";
	                 							else
	                 								continue;
	                 						}
	                     					
		                     				if (($row[$this->no_col-2] == 1) || ($row[$this->no_col-2] == 2))
	                        				{
	                             					// read message
	                             					if($row[$this->no_col] == 'OUTBOX'){
		                             						if($j == 2){ 	## internal recipients
			                             							$arr_internalReceiver = $lcampusmail->getRecipientNames($row[$j]);
																	$arrReceiver = array();
																	for($k=0; $k<sizeof($arr_internalReceiver); $k++){
																			list($ID, $Name) = $arr_internalReceiver[$k];
																			$arrReceiver[] = $Name;
																	}
																	if(sizeof($arrReceiver)>0){
																			$tmp_display = "";
																			if(sizeof($arrReceiver)>3){							## if more that 3 internal receipient, replace the remaining with "..."
																					for($z=0; $z<3; $z++){
																							$tmp_display .= $arrReceiver[$z].", ";
																					}
																					$tmp_display .= "...";
																					$row[$j] = implode(", ",$arrReceiver);		## original is userid, now replace with user name
																			}else{
																					$row[$j] = implode(", ",$arrReceiver);		## original is userid, now replace with user name
																					$tmp_display .= implode(", ",$arrReceiver);
																			}
																			$x .= $this->displayCell($j,$tmp_display, "iMailrow tabletext", "title='$row[$j]'");
																	}else{
																			$row[$j] = " - ";
																			$x .= $this->displayCell($j,$row[$j], "iMailrow tabletext");
																	}
	                             							}else if($j == 3){
		                             								if($row[$j] == "&nbsp;"){
																			$row[$j] = " - ";
																			$x .= $this->displayCell($j,$row[$j], "iMailrow tabletext");
																	}else{
																			$tmp_arr = explode(",",$row[$j]);
																			$tmp_display = "";
																			if(sizeof($tmp_arr)>1){								## if more than 1 external receipient, replace the remaining with "..."
																				for($z=0; $z<1; $z++){
																						$tmp_display .= $tmp_arr[$z].", ";
																				}
																				$tmp_display .= "...";
																				
																			}else{
																				$tmp_display = $row[$j];
																			}
																			$x .= $this->displayCell($j,$tmp_display, "iMailrow tabletext", "title='$row[$j]'");
																	}
	                         								}else{
		                         									$x .= $this->displayCell($j,$row[$j], "iMailrow tabletext");
	                         								}
	                             					}else{
		                             						//$x .= $this->displayCell($j, intranet_undo_htmlspecialchars(stripslashes($row[$j])), "iMailrow tabletext");
		                             						$x .= $this->displayCell($j, $row[$j], "iMailrow tabletext");
	                         						}
	                            			} else {
	                                    			//$x .= $this->displayCell($j, intranet_undo_htmlspecialchars(stripslashes($row[$j])), "iMailrowunread tabletext");
	                                    			$x .= $this->displayCell($j, $row[$j], "iMailrowunread tabletext");
	                            			}
	                        		}
	                                if (($row[$this->no_col-2] == 1) || ($row[$this->no_col-2] == 2))
	                                {
	                                    	$x .= "<td class='iMailrow' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>\n";
	                    			} else {
	                            			$x .= "<td class='iMailrowunread' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>\n";
	                    			}
	                				$x .= "</tr>\n";
	            			
	            			}
	            	}
	            	
	            	if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".($this->no_col)."' class='tablebottom'>".$this->navigation()."</td></tr>";
					$x .= "</table>\n";
	
	                $this->db_free_result();
	
	                return $x;
	        }
	        
	        function displayFormat_iMail_Gamma($fb="",$fb2="")
	        {
	        		global $image_path,$LAYOUT_SKIN;
					global $i_campusmail_lettertext;
					global $intranet_root;
//					include_once($intranet_root."/includes/libcampusmail.php");
//					include_once($intranet_root."/includes/libwebmail.php");
					global $MailHeaderList,$MailFlags,$MailPriority, $IMap, $Folder, $imapInfo, $isDraftInbox, $MailReadedCount;
					global $i_CampusMail_New_Icon_RepliedMail_2007,$i_CampusMail_New_Icon_ReadMail_2007,$i_CampusMail_New_Icon_NewMail_2007;
					global $order, $field;
					
//					$lcampusmail = new libcampusmail();
//					$lwebmail = new libwebmail();
//					$i = 0;
//					
//					if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID))
//				    {
//				        $noWebmail = false;
//				    }
//				    else
//				    {
//				        $noWebmail = true;
//				    }
	
					$x .= "<table width='100%' id='MailList' border='0' cellspacing='0' cellpadding='4' onselectstart='return false;' style='-moz-user-select:-moz-none;'>";
					$x .= $this->displayColumn();
	
					if (count($MailHeaderList)==0)
					{
//						$x .= "<tr>
//									<td class='iMailrow tabletext' ><img src='$image_path/space.gif' width='1' height='1' border='0'></td>
//                                    <td class='iMailrow tabletext' align='center' colspan='".($this->no_col-2)."' ><br />".$this->no_msg."<br /><br></td>
//                                    <td class='iMailrow tabletext' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>
//								</tr>\n
//								";
                        $x .= "<tr><td align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td></tr>\n";
                        $x .= '<tr><td colspan="'.($this->no_col).'" class="dotline"><img src="/images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td></tr>'."\n";
					} 
					else 
					{
						
						
						for($i=0; $i<count($MailHeaderList); $i++)
						{
							
							$j=0;
							$header = $MailHeaderList[$i];
							$MsgNo = trim($header->Msgno);
							$Uid 	= @imap_uid($IMap->inbox, $header->Msgno);
							
							$HaveAttachment = $IMap->Check_Any_Mail_Attachment($header->Msgno);
							$AttachmentIcon = $HaveAttachment?'<img border="0" align="absmiddle" alt="Attachment" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_attachment.gif">':"";
							unset($RowStyle);
					        unset($LinkStyle);
					        unset($ImageSrc);
					        unset($ImportantImage);
					        
					        # seen / unseen
							if ($header->Unseen=="U") 
							{
								$RowStyle = "iMailrowunread ";
						        $LinkStyle = "iMailsubjectunread";
						        $SenderStyle = "iMailsenderunread";
						        $StatusIcon = '<img border="0" align="absmiddle" alt="Unread" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_unread_mail.gif">';
					        }
					        else 
					        {
						        $RowStyle = "iMailrow";
						        $LinkStyle = "iMailsubject";
						        $SenderStyle = "iMailsender";
						        $StatusIcon = '<img border="0" align="absmiddle" alt="read" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_open_mail.gif">';
					        }
					        
					        # answered
					        if ($header->Answered=="A") 
					        {
					        	$repliedIcon = '<img border="0" align="absmiddle" alt="Replied" title="Replied" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_notification.gif">';
					        }
					        else
					     	{
					     		$repliedIcon = "<img src='$image_path/space.gif' width='1' height='1' border='0' />";
					     	}
					
							# forwarded
							if ($MailFlags[$MsgNo]["forwarded"]) 
					        {
					        	$forwardedIcon = '<img border="0" align="absmiddle" alt="Forwarded" title="Forwarded" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_forward.gif">';
					        }
					        else
					     	{
					     		$forwardedIcon = "<img src='$image_path/space.gif' width='1' height='1' border='0' />";
					     	}

							# importantFlag
							$importantIcon = '';
							if ($MailPriority[$MsgNo]["ImportantFlag"]=='true') 
					        	$importantIcon = '<img border="0" align="absmiddle" alt="Important" title="Important" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_important.gif">';
					        else
					     		$importantIcon = "<img src='$image_path/space.gif' width='1' height='1' border='0' />";
					     	
							# flagged /star
							$starPath = trim($header->Flagged)?"icon_star_on.gif":"icon_star_off.gif";
							$starIcon = "<img class='mailflag' align='absmiddle' src='$image_path/$LAYOUT_SKIN/$starPath' border='0' style='cursor:pointer' onclick='setFlag(\"$Folder\",\"$Uid\", this)'/>";
							
							# Subject Title 
							$subjectPrint = "" ;
							$subject = "";
							
							$subject = $IMap->MIME_Decode($header->subject); 
								
							if ($subject != "") {
								$subjectPrint = (substr($subject, 0, 10) == "=?UTF-8?Q?") ? substr($subject, 10, -2) : $subject ;
							} else {
								$subjectPrint = "No subject" ;
							}
							$subjectPrint = $AttachmentIcon.htmlspecialchars($subjectPrint);
							$subjectPrint = $IMap->BadWordFilter($subjectPrint);
		
							
							if ($Folder == $IMap->DefaultFolderList['DraftFolder'] || $Folder == $IMap->DefaultFolderList['SentFolder'])
							{
								# Receiver Info
								$toaddress = '';
								//$toaddress = $IMap->MIME_Decode($header->toaddress);
								$EmailAry = $IMap->Extract_Header_Email($header->to);
								if(count($header->cc)>0){
									if(count($EmailAry)==0) $EmailAry = array();
									$EmailAry = array_merge($EmailAry,$IMap->Extract_Header_Email($header->cc));
								}
								$DisplayName = '';
								$DisplayTitle = '';
								if(count($EmailAry)>1){
									$EmailAryCopy = array(); // make a deep copy of array
									for($k=0;$k<count($EmailAry);$k++){
										$EmailAryCopy[] = array($EmailAry[$k][0],$EmailAry[$k][1]);
										$DisplayTitle .= ($k>0?", ":"").htmlspecialchars(html_entity_decode($EmailAry[$k][0]),ENT_QUOTES)." &lt;".$EmailAry[$k][1]."&gt;";
									}
									while(count($EmailAryCopy)>0 && strlen($DisplayName)<50){
										$tmp_ary = array_shift($EmailAryCopy);
										if($DisplayName != ''){
											$DisplayName .= ', ';
										}
										$DisplayName .= htmlspecialchars(html_entity_decode($tmp_ary[0]));
									}
									if(count($EmailAryCopy)>0){
										$DisplayName .= '...';
									}
									$fromtotitle = "<span title='".$DisplayTitle."'>".$DisplayName."</span>";
								}else{
									list($DisplayName,$Email) = $EmailAry[0]; 
									$fromtotitle = "<span title='".htmlspecialchars(html_entity_decode($DisplayName))." &lt;$Email&gt;'>".htmlspecialchars(html_entity_decode($DisplayName))."</span>";
								}
							}
							else
							{
									# Sender Info
								$fromaddress = '';
								//$fromaddress = $IMap->MIME_Decode($header->fromaddress); 
								$EmailAry = $IMap->Extract_Header_Email($header->from);
								list($DisplayName,$Email) = $EmailAry[0]; 
								$fromtotitle = "<span title='".htmlspecialchars(html_entity_decode($DisplayName))." &lt;$Email&gt;'>".htmlspecialchars(html_entity_decode($DisplayName))."</span>";
								//$fromtotitle = ((mb_strlen($fromaddress) > 15) ? mb_substr($fromaddress, 0, 15, "UTF-8").'...' : $fromaddress);
								
							}
							
							$fromtotitle = "<span style='-moz-user-select:text'>".$fromtotitle."</span>";
							
							# Subject Title
							$Folder = ($Folder == '') ? 'INBOX' : $Folder;
							$params  = "uid=$Uid&Folder=".urlencode($Folder)."&CurMenu=1&CurTag=$CurTag&order=".$order."&field=".$field."&sort=".$imapInfo['sort'];
							$params .= "&reverse=".$imapInfo['reverse']."&pageNo=".$imapInfo['pageInfo']['pageNo']."&mailbox=".urlencode($Folder)."&MsgNo=$MsgNo";
							$url  = ($isDraftInbox == 1) ? "compose_email.php?" : "viewmail.php?";
							$url .= $params;
							if ($header->Deleted!="D")
								$subjectPrint='<span style="-moz-user-select:text"><a href="'.$url.'" class="'.$LinkStyle.'"> '.$subjectPrint.'</a>&nbsp;</span>';
							
							$formattedDatetime = $IMap->formatDatetimeString($header->date);
							# Date Time
							if($IMap->IsToday($formattedDatetime))
								$DateFormat = "H:i";
							else
								$DateFormat = "Y-m-d";
							$prettydate = date($DateFormat, strtotime($formattedDatetime));
							
							# Email Size
							$mailsize = round($header->Size / 1024, 0);
					        
					        # Check Box
					        $checkbox = "<input type='checkbox' value='$Uid' name='Uid[]'>";
					        $x .= "<tr>\n";
						        //$x .= "<td class='$RowStyle' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>\n";
						        $x.= $this->displayCell($j++,$StatusIcon, "$RowStyle tabletext", "nowrap" );
						        $x.= $this->displayCell($j++,$starIcon, "$RowStyle tabletext", "nowrap" );
						        $x.= $this->displayCell($j++,$importantIcon, "$RowStyle tabletext", "nowrap" );
						        $x.= $this->displayCell($j++,$repliedIcon, "$RowStyle tabletext","nowrap");
						        $x.= $this->displayCell($j++,$forwardedIcon, "$RowStyle tabletext","nowrap");
								$x.= $this->displayCell($j++,$fromtotitle, "$RowStyle tabletext $SenderStyle");	 
								$x.= $this->displayCell($j++,$subjectPrint, "$RowStyle tabletext");
								$x.= $this->displayCell($j++,$prettydate, "$RowStyle tabletext","nowrap");
								$x.= $this->displayCell($j++,number_format($mailsize), "$RowStyle tabletext");
								if($Folder==$IMap->SentFolder){
									if($MailReadedCount[$MsgNo]['Total']==0){
										$mailreaded = '-';
									}else{
										$mailreaded = sprintf("%d/%d",$MailReadedCount[$MsgNo]['NumOfReaded'],$MailReadedCount[$MsgNo]['Total']);
									}
									$x.= $this->displayCell($j++,$mailreaded, "$RowStyle tabletext");
								}
								$x.= $this->displayCell($j++,$checkbox, "$RowStyle tabletext","nowrap");
								
							$x .= "</tr>\n";
						}
				
	            	}
	            	
	            	$this->n_start = $imapInfo["pageInfo"]["pageStart"]-1;
	            	$this->total_row = $imapInfo["pageInfo"]["totalCount"];
	            	if($this->total_row<>0) $x .= "<tr><td colspan='".($this->no_col)."' class='tablebottom'>".$this->navigation()."</td></tr>";
					$x .= "</table>\n";
	
	                //$this->db_free_result();
	
	                return $x;
	        }
	        
	        function displayFormat_iMail_Gamma_Search($fb="",$fb2="")
	        {
	        		global $image_path, $LAYOUT_SKIN;
					global $intranet_root;
					global $MailList, $IMap, $Folder, $pagingInfo, $isDraftInbox, $SearchInfo;
					global $i_CampusMail_New_Icon_RepliedMail_2007,$i_CampusMail_New_Icon_ReadMail_2007,$i_CampusMail_New_Icon_NewMail_2007;
					global $SearchFields;
					
					$MailAry = $MailList[0];
					$UIDList = Get_Array_By_Key($MailAry,"UID");
					
					if(isset($IMap->CacheMailToDB) && $IMap->CacheMailToDB == true){
						// flags from db
					}else{
						//$MailPriority = $IMap->GetMailPriority($UIDList,$Folder,$isUID=1);
						$MailPriority = array();
						if(isset($SearchInfo['Folder']) && count($SearchInfo['Folder'])>0){
							foreach($SearchInfo['Folder'] as $FolderKey=>$FolderName){
								$TempMailPriority = $IMap->GetMailPriority($UIDList,$FolderName,$isUID=1);
								$MailPriority[$FolderName] = $TempMailPriority;
							}
						}
					}
					$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4' id='MailList'  onselectstart='return false;' style='-moz-user-select:-moz-none;;'>";
					$x .= $this->displayColumn();
					
					if (count($MailAry)==0)
					{
//						$x .= "<tr>
//                                    <td class='iMailrow tabletext' align='center' colspan='".($this->no_col-1)."' ><br />".$this->no_msg."<br /><br></td>
//								</tr>\n
//								";
                        $x .= "<tr><td align='center' colspan='".($this->no_col-1)."' ><br />".$this->no_msg."<br /><br></td></tr>\n";
                        $x .= '<tr><td colspan="'.($this->no_col-1).'" class="dotline"><img src="/images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td></tr>'."\n";

					} 
					else 
					{
						$sortby = $this->field_array[$this->field];
						if(isset($IMap->CacheMailToDB) && $IMap->CacheMailToDB == true){
							// sort by sql
						}else{
							sortByColumn2($MailAry,$sortby,$this->order);
						}
						## Store sorted search result to user file
						$CachedUID = Get_Array_By_Key($MailAry,"UID");
						$CachedFolder = Get_Array_By_Key($MailAry,"Folder");
						$IMap->Set_Cached_Search_Result($CachedUID,$CachedFolder,$SearchFields);
						
						if(isset($IMap->CacheMailToDB) && $IMap->CacheMailToDB == true){
							$start = 0;
							$end = count($MailAry);
						}else{
							$start = ($this->pageNo-1)*$this->page_size;
							$end = $this->page_size*$this->pageNo;
						}
						for($i=$start; $i<$end&&$i<count($MailAry); $i++)
						{
							
							$j=0;
							
							$From 	  = $MailAry[$i]["From"];
							$To 	  = $MailAry[$i]["To"];
							$Date 	  = $IMap->formatDatetimeString($MailAry[$i]["DateReceived"]);
							$Priority = $MailAry[$i]["Priority"];
							$Subject  = $IMap->BadWordFilter($MailAry[$i]["Subject"]);
							$Uid 	  = $MailAry[$i]["UID"];
							$Size 	  = $MailAry[$i]["Size"];
							$recent   = $MailAry[$i]["recent"];
							$flagged  = $MailAry[$i]["flagged"];
							$answered = $MailAry[$i]["answered"];
							$deleted  = $MailAry[$i]["deleted"];
							$seen  	  = $MailAry[$i]["seen"];
							$draft 	  = $MailAry[$i]["draft"];
							$Folder   = stripslashes($MailAry[$i]["Folder"]);
							$HaveAttachment = $MailAry[$i]["HaveAttachment"];
							if(isset($IMap->CacheMailToDB) && $IMap->CacheMailToDB == true){
								$ImportantFlag = $MailAry[$i]["Priority"]==1;
							}else{
								$ImportantFlag = $MailPriority[$Folder][$Uid]["ImportantFlag"]=='true';
							}
							# Get Folder Name
							$FolderDisplayName = IMap_Decode_Folder_Name($IMap->getMailDisplayFolder($Folder));
							
							
							if ($seen==1) 
					        {
						        $RowStyle = "iMailrow";
						        $LinkStyle = "iMailsubject";
						        $SenderStyle = "iMailsender";
						        $StatusIcon = '<img border="0" align="absmiddle" alt="read" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_open_mail.gif">';
					        }
					        else 
							{
								$RowStyle = "iMailrowunread ";
						        $LinkStyle = "iMailsubjectunread";
						        $SenderStyle = "iMailsenderunread";
						        $StatusIcon = '<img border="0" align="absmiddle" alt="Unread" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_unread_mail.gif">';
					        }
					        
					        if ($answered==1) 
					        {
					        	$repliedIcon = '<img border="0" align="absmiddle" alt="Replied" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_notification.gif">';
					        }
					        else
					     	{
					     		$repliedIcon = "<img src='$image_path/space.gif' width='1' height='1' border='0' />";
					     	}
					     	
					     	# flagged /star
							$starPath = trim($flagged)?"icon_star_on.gif":"icon_star_off.gif";
							$starIcon = "<img class='mailflag' align='absmiddle' src='$image_path/$LAYOUT_SKIN/$starPath' border='0' style='cursor:pointer' onclick='setFlag(\"$Folder\",\"$Uid\", this)'/>";
					     	
					     	# importantFlag
							$importantIcon = '';
							if ($ImportantFlag) 
					        	$importantIcon = '<img border="0" align="absmiddle" alt="Important" title="Important" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_important.gif">';
					        else
					     		$importantIcon = "<img src='$image_path/space.gif' width='1' height='1' border='0' />";
					     	
					     	# From / Recipient
					     	if($Folder == $IMap->SentFolder || $Folder == $IMap->DraftFolder){
					     		$ToArray = explode(",",$To);
					     		$fromToTitle = $ToArray[0];
					     	}else{
					     		$fromToTitle = $From;
					     	}
					     	
					     	# Subject
	     					$url  = ($Folder != $IMap->DefaultFolderList['DraftFolder']) ? "viewmail.php" : "compose_email.php";
							$url .= "?uid=".$Uid."&Folder=".urlencode($Folder)."&CurMenu=Search&CurTag=".urlencode($Folder);
							$url .= "&pageNo=".$pagingInfo["pageNo"]."&sort=".$pagingInfo["sort"]."&reverse=".$pagingInfo["reverse"]."&keyword=".$SearchInfo['keyword'];
							$url .= "&KeywordFrom=".$SearchInfo['From']."&KeywordTo=".$SearchInfo['To']."&KeywordCc=".$SearchInfo['Cc'];
							$url .= "&KeywordSubject=".$SearchInfo['Subject']."&FromDate=".$SearchInfo['FromDate']."&ToDate=".$SearchInfo['ToDate'];
							$url .= "&FromSearch=1&IsAdvanceSearch=".$IsAdvanceSearch."&".$params;
							if ($deleted!=1)
								$subjectPrint='<span style="-moz-user-select:text">&nbsp;<a href="'.$url.'" class="'.$LinkStyle.'"> '.$Subject.'</a>&nbsp;</span>';
					     	else
					     		$subjectPrint = $Subject;
					     	$AttachmentIcon = $HaveAttachment?'<img border="0" align="absmiddle" alt="Attachment" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_attachment.gif">':"";
					     	$subjectPrint = $AttachmentIcon.$subjectPrint;
					     		
					     	# Check Box
					        $checkbox = "<input type='checkbox' value='$Uid,$Folder' name='Uid[]'>";
					        
					        #Format Date
					        if($IMap->IsToday($Date))
								$DateFormat = "H:i";
							else
								$DateFormat = "Y-m-d";
							$Date = date($DateFormat, strtotime($Date));
					        
					        $x .= "<tr>\n";
					        //$x .= "<td class='$RowStyle' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>\n";
					        $x.= $this->displayCell($j++,$StatusIcon, "$RowStyle tabletext","nowrap");
					        $x.= $this->displayCell($j++,$starIcon, "$RowStyle tabletext","nowrap");
					        $x.= $this->displayCell($j++,$importantIcon, "$RowStyle tabletext","nowrap");
					        $x.= $this->displayCell($j++,$repliedIcon, "$RowStyle tabletext","nowrap");
							$x.= $this->displayCell($j++,'<span style="-moz-user-select:text">'.$fromToTitle.'</span>', "$RowStyle tabletext $SenderStyle");	 
							$x.= $this->displayCell($j++,$subjectPrint, "$RowStyle tabletext");
							$x.= $this->displayCell($j++,$Date, "$RowStyle tabletext");
							$x.= $this->displayCell($j++,$FolderDisplayName, "$RowStyle tabletext");
							$x.= $this->displayCell($j++,round($Size,0),"$RowStyle tabletext");
							$x.= $this->displayCell($j++,$checkbox, "$RowStyle tabletext","nowrap");
							$x .= "</tr>\n";
		
						}
				
	            	}
	            	
	            	if(isset($IMap->CacheMailToDB) && $IMap->CacheMailToDB == true){
	            		
	            	}else{
	            		$this->n_start = $start;
	            		$this->total_row = $MailList[1];
	            	}
	            	if($this->total_row<>0) $x .= "<tr><td colspan='".($this->no_col)."' class='tablebottom'>".$this->navigation()."</td></tr>";
					$x .= "</table>\n";
	
	                //$this->db_free_result();
					
	                return $x;
	        }
	        /*
	        function displayFormat_iMail($fb="",$fb2="")
	        {
					global $image_path;
					global $i_campusmail_lettertext;
					global $intranet_root;
					include_once($intranet_root."/includes/libcampusmail.php");
					//include_once($intranet_root."/includes/libcampusmail2007a.php");
					$lcampusmail = new libcampusmail();
					
					$i = 0;
					
					$this->rs = $this->db_db_query($this->built_sql());
	
					$n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
					
					$tmp_row = $this->db_fetch_array();
					if($tmp_row[$this->no_col] == 'OUTBOX'){
						$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4' class='tbclip'>";
					}else{
						$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
					}
					$x .= $this->displayColumn();
	
					if ($this->db_num_rows()==0)
					{
							$x .= "<tr>
										<td class='iMailrow tabletext' ><img src='$image_path/space.gif' width='1' height='1' border='0'></td>
	                                    <td class='iMailrow tabletext' align='center' colspan='".($this->no_col-2)."' ><br />".$this->no_msg."<br /><br></td>
										<td class='iMailrow tabletext' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>
									</tr>\n
									";
					} else {
							while($row = $this->db_fetch_array())
	                		{
									$i++;
	
									if($i>$this->page_size) break;
									$x .= "<tr>\n";
	
	                                if (($row[$this->no_col-2] == 1) || ($row[$this->no_col-2] == 2))
	                                {
											$x .= "<td class='iMailrow' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>\n";
									} else {
											$x .= "<td class='iMailrowunread' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>\n";
									}
									for($j=0; $j<$this->no_col-2; $j++)
									{
											if (trim($row[$j]) == "")
											{
													$row[$j] = "&nbsp;";
											}
											if (($row[$this->no_col-2] == 1) || ($row[$this->no_col-2] == 2))
											{
													// read message
													//$x .= $this->displayCell($j,$row[$j], "iMailrow tabletext");
													
													if($row[$this->no_col] == 'OUTBOX'){
															if($j == 2){
																	$arr_internalReceiver = $lcampusmail->getRecipientNames($row[$j]);
																	$arrReceiver = array();
																	for($k=0; $k<sizeof($arr_internalReceiver); $k++){
																			list($ID, $Name) = $arr_internalReceiver[$k];
																			$arrReceiver[] = $Name;
																	}
																	if(sizeof($arrReceiver)>0){
																			$row[$j] = implode(", ",$arrReceiver);
																			$x .= $this->displayCell($j,$row[$j], "iMailrow tabletext", "title='$row[$j]'");
																	}else{
																			$row[$j] = " - ";
																			$x .= $this->displayCell($j,$row[$j], "iMailrow tabletext");
																	}
															}elseif($j == 3){
																	if($row[$j] == "&nbsp;"){
																		$row[$j] = " - ";
																		$x .= $this->displayCell($j,$row[$j], "iMailrow tabletext");
																	}else{
																		$x .= $this->displayCell($j,$row[$j], "iMailrow tabletext", "title='$row[$j]'");
																	}
															}else{
																	$x .= $this->displayCell($j,$row[$j], "iMailrow tabletext");
															}
													}else{
															$x .= $this->displayCell($j,$row[$j], "iMailrow tabletext");
													}
											} else {
													//$x .= $this->displayCell($j,$j,"iMailrowunread tabletext");
													if($row[$this->no_col] == 'OUTBOX'){
															if($j == 2 || $j == 3){
																//echo $row[$j]."<BR>";
															}else{
																$x .= $this->displayCell($j,$row[$j], "iMailrowunread tabletext");
															}
													}else{
															$x .= $this->displayCell($j,$row[$j],"iMailrowunread tabletext");
													}
											}
									}
									
									if (($row[$this->no_col-2] == 1) || ($row[$this->no_col-2] == 2))
									{
											$x .= "<td class='iMailrow' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>\n";
									} else {
											$x .= "<td class='iMailrowunread' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>\n";
									}
									$x .= "</tr>\n";
							}
	            	}
	
					if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".($this->no_col)."' class='tablebottom'>".$this->navigation()."</td></tr>";
					$x .= "</table>\n";
	
	                $this->db_free_result();
	
	                return $x;
	        }
			*/
	        # ------------------------------------------------------------------------------------
	
	        # ------------------------------------------------------------------------------------
	        # Display for iMail USer Selection
	        function displayFormat_iMailSelection($TableType="",$EmailField=1)
	        {
				global $image_path, $LAYOUT_SKIN, $TabID;
				//global $i_campusmail_lettertext;
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
	
				$n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	
	           	if ($TableType == "green")
	           		$x .= '<table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolorlight="#FFFFFF" bordercolordark="#DDEF5D" bgcolor="#F2F8CA" class="body">';
	           	else
	           		$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	            $x .= $this->displayColumn();
	
	           	if ($this->db_num_rows()==0)
				{
					$x .= "<tr>
						<td class='iMailrow tabletext' ><img src='$image_path/space.gif' width='1' height='1' border='0'></td>
						<td class='iMailrow tabletext' align='center' colspan='".($this->no_col-2)."' ><br />".$this->no_msg."<br /><br></td>
						<td class='iMailrow tabletext' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>
						</tr>\n
						";
	            }
	            else
	            {
	            	while($row = $this->db_fetch_array())
	            	{
						$i++;
	
						if($i>$this->page_size) break;
						$x .= "<tr class='tablerow". ( ($i+1)%2 + 1 )."'>\n";
	
						$x .= "<td class='tabletext' valign='top'>". ++$n_start ."</td>\n";
	
						for($j=0; $j<$this->no_col; $j++)
						{
							if (trim($row[$j]) == "")
	                    	{
	                	    	$row[$j] = "&nbsp;";
							}
							if ($j == $EmailField)
							{
								$row[$j] .= "<input type=\"hidden\" name=\"UserEmail_".$row[$j+2]."\" value=\"".$row[$j]."\" />";
								$row[$j] .= "<input type=\"hidden\" name=\"UserName_".$row[$j+2]."\" value=\"".$row[$j-1]."\" />";
							}
	                        $x .= $this->displayCell($j,nl2br($row[$j]), "tablelink","valign='top'");
						}
	
	                    $x .= "</tr>\n";
					}
	            }
	
	           	if ($TableType == "green")
	           	{
		           	$BottomClass = "bgcolor=\"#E8F2A6\"";
	           	}
				if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".($this->no_col+1)."' ".$BottomClass." >".$this->navigation_imailSelection()."</td></tr>";
	
	
				$x .= "</table>\n";
	
				$this->db_free_result();
	
				return $x;
	        }
	
	        # ------------------------------------------------------------------------------------
	
	
	        # ------------------------------------------------------------------------------------
	        # Special display for iMail Address Book
	        function displayFormat_iMailAddressBook($icon_display=1)
	        {
	                global $image_path, $LAYOUT_SKIN, $TabID;
	                //global $i_campusmail_lettertext;
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	
	                    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	                    $x .= $this->displayColumn();
	
	                    if ($this->db_num_rows()==0)
	                {
//	                        $x .= "<tr>
//	                                <td class='iMailrow tabletext' ><img src='$image_path/space.gif' width='1' height='1' border='0'></td>
//	                                <td class='iMailrow tabletext' align='center' colspan='".($this->no_col-2)."' ><br />".$this->no_msg."<br /><br></td>
//	                                <td class='iMailrow tabletext' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>
//	                                </tr>\n
//	                                ";
                            $x .= "<tr><td align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td></tr>\n";
                            $x .= '<tr><td colspan="'.($this->no_col).'" class="dotline"><img src="/images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td></tr>'."\n";

	                    } else {
	
	                        while($row = $this->db_fetch_array())
	                        {
	                                    $i++;
	
	                                    if($i>$this->page_size) break;
	                                $x .= "<tr class='tablerow". ( ($i+1)%2 + 1 )."'>\n";
	
	                                $x .= "<td class='tabletext' valign='top'>". ++$n_start ."</td>\n";
	
	                                if($icon_display)
	                                        $x .= "<td class='tabletext' valign='top'><img src='$image_path/$LAYOUT_SKIN/iMail/icon_ppl_". ($TabID==1?"in_group":($TabID==3?"ex_group":"ex")).".gif' align='absmiddle' /></td>\n";
	
	
	                                for($j=1; $j<$this->no_col-1; $j++)
	                                {
	                                                 if (trim($row[$j]) == "")
	                                                 {
	                                                         $row[$j] = "&nbsp;";
	                                                 }
	
	                                        $x .= $this->displayCell($j,nl2br($row[$j]), "tablelink","valign='top'");
	
	                                }
	
	                                    $x .= "</tr>\n";
	                                }
	                            }
	
	                        if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".($this->no_col)."' class='tablebottom'>".$this->navigation()."</td></tr>";
	                        $x .= "</table>\n";
	
	                        $this->db_free_result();
	
	                        return $x;
	        }
	
	        # ------------------------------------------------------------------------------------
	
  # ------------------------------------------------------------------------------------
  # display normal table without the heading number
  function displayFormat_SmartCardAttandencdList($prompt_msg="")
  {
    global $image_path, $LAYOUT_SKIN, $lcard, $PATH_WRT_ROOT;
    global $i_StudentAttendance_Status_Present, $i_StudentAttendance_Status_Absent, $i_StudentAttendance_Status_Late, $i_StudentAttendance_Status_Outing, $i_StudentAttendance_Status_EarlyLeave, $i_SmartCard_Frontend_Take_Attendance_Waived;

		if ($lcard == "") {
			include_once($PATH_WRT_ROOT.'includes/libcardstudentattend2.php');
			
			$lcard	= new libcardstudentattend2();
			$lcard->retrieveSettings();
		}
    $i = 0;
    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
    $x .= $this->displayColumn();

    if($prompt_msg)
    {
            $x .= "<tr>
                    <td class='tablebluerow2 tabletext' align='center' colspan='".($this->no_col)."' ><br />".$prompt_msg."<br /><br></td>
                    </tr>\n";
    }
    else
    {
      $this->rs = $this->db_db_query($this->built_sql("NoLimit"));
      $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;

      if ($this->db_num_rows()==0)
      {
        $x .= "<tr>
                <td class='tablebluerow2 tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
                </tr>\n
                ";
      } else {
        while($row = $this->db_fetch_array())
        {
          $i++;

          //if($i>$this->page_size) break;
          $x .= "<tr class='tablebluerow". ( ($i+1)%2 + 1 )."'>\n";

          for($j=0; $j<$this->no_col; $j++)
          {
						if (trim($row[$j]) == "")
						{
							$row[$j] = "&nbsp;";
						}
						
						if ($lcard->attendance_mode == 1 || $lcard->attendance_mode == 0) {
							if ($j == 3) {
								switch ($row[$j])
	              {
	                case "--":
	                	$txt_pm = "--"; break;
	                case CARD_STATUS_PRESENT:         	
	                	$txt_pm = $i_StudentAttendance_Status_Present; break;
	                case CARD_STATUS_ABSENT:         	
	                	$txt_pm = $i_StudentAttendance_Status_Absent; 
	                	if ($lcard->attendance_mode == 0) 
	                		$txt_pm .= ($row[6] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	else 
	                		$txt_pm .= ($row[7] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	break;
	                case CARD_STATUS_LATE:
	                	$txt_pm = $i_StudentAttendance_Status_Late; 
	                	if ($lcard->attendance_mode == 0) 
	                		$txt_pm .= ($row[6] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	else 
	                		$txt_pm .= ($row[7] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	break;
	                	break;
	                case CARD_STATUS_OUTING:			
	                	$txt_pm = $i_StudentAttendance_Status_Outing; break;
	                case -1:							
	                	$txt_pm = $i_StudentAttendance_Status_EarlyLeave; 
	                	if ($lcard->attendance_mode == 0) 
	                		$txt_pm .= ($row[8] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	else 
	                		$txt_pm .= ($row[9] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	break;
	                case -2:							
	                	$txt_pm = $i_StudentAttendance_Status_EarlyLeave; 
	                	if ($lcard->attendance_mode == 0) 
	                		$txt_pm .= ($row[8] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	else 
	                		$txt_pm .= ($row[9] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	break;
	                case -3:							
	                	$txt_pm = $i_StudentAttendance_Status_Late;
	                	if ($lcard->attendance_mode == 0) 
	                		$txt_pm .= ($row[6] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	else 
	                		$txt_pm .= ($row[7] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	$txt_pm .= "<BR>".$i_StudentAttendance_Status_EarlyLeave; 
	                	if ($lcard->attendance_mode == 0) 
	                		$txt_pm .= ($row[8] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	else 
	                		$txt_pm .= ($row[9] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	break;
	                case -4:							
	                	$txt_pm = $i_StudentAttendance_Status_Late;
	                	if ($lcard->attendance_mode == 0) 
	                		$txt_pm .= ($row[6] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	else 
	                		$txt_pm .= ($row[7] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	$txt_pm .= "<BR>".$i_StudentAttendance_Status_EarlyLeave; 
	                	if ($lcard->attendance_mode == 0) 
	                		$txt_pm .= ($row[8] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	else 
	                		$txt_pm .= ($row[9] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	break;
	                default:							
	                	$txt_pm = nl2br($row[$j]);
	              }
	
	              $x .= $this->displayCell($j,$txt_pm, "tablelink","valign='top'");
							}
							else {
								$x .= $this->displayCell($j,nl2br($row[$j]), "tablelink","valign='top'");
							}
						}
						else {
							if($j==3 or $j==4)        //AM PM Status
	            {
	              switch ($row[$j])
	              {
	                case "--":
	                	$txt_pm = "--"; break;
	                case CARD_STATUS_PRESENT:         	
	                	$txt_pm = $i_StudentAttendance_Status_Present; break;
	                case CARD_STATUS_ABSENT:         	
	                	$txt_pm = $i_StudentAttendance_Status_Absent; 
	                	$txt_pm .= (($j == 3 && $row[7] == 1) || ($j == 4 && $row[8] == 1))? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	break;
	                case CARD_STATUS_LATE:				
	                	$txt_pm = $i_StudentAttendance_Status_Late; 
	                	$txt_pm .= (($j == 3 && $row[7] == 1) || ($j == 4 && $row[8] == 1))? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	break;
	                case CARD_STATUS_OUTING:			
	                	$txt_pm = $i_StudentAttendance_Status_Outing; break;
	                case -1:							
	                	$txt_pm = $i_StudentAttendance_Status_EarlyLeave; 
	                	$txt_pm .= ($j == 3 && $row[9] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	break;
	                case -2:							
	                	$txt_pm = $i_StudentAttendance_Status_EarlyLeave; 
	                	$txt_pm .= ($j == 4 && $row[10] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	break;
	                case -3:							
	                	$txt_pm = $i_StudentAttendance_Status_Late;
	                	$txt_pm .= ($j == 3 && $row[7] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	$txt_pm .= "<BR>".$i_StudentAttendance_Status_EarlyLeave; 
	                	$txt_pm .= ($j == 3 && $row[9] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	break;
	                case -4:							
	                	$txt_pm = $i_StudentAttendance_Status_Late;
	                	$txt_pm .= ($j == 4 && $row[8] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	$txt_pm .= "<BR>".$i_StudentAttendance_Status_EarlyLeave; 
	                	$txt_pm .= ($j == 4 && $row[10] == 1)? "(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"";
	                	break;
	                default:							
	                	$txt_pm = nl2br($row[$j]);
	              }
	
	              $x .= $this->displayCell($j,$txt_pm, "tablelink","valign='top'");
	            }
	            else
	            {
	            	$x .= $this->displayCell($j,nl2br($row[$j]), "tablelink","valign='top'");
	            }
						}
          }

          $x .= "</tr>\n";
        }
      }
      $this->db_free_result();
  	}
		$x .= "</table>\n";
		
		return $x;
  }
  # ------------------------------------------------------------------------------------
	
	        function displayFormat_SmartCardAttandencdListMonthly($prompt_msg="")
	        {
	                global $image_path, $LAYOUT_SKIN, $results, $year, $month, $lcard, $Lang, $i_SmartCard_Frontend_Take_Attendance_Waived;
	
	                    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	                    $x .= $this->displayColumn();
	
	                if($prompt_msg)
	                {
	                        $x .= "<tr>
	                                <td class='tablebluerow2 tabletext' align='center' colspan='7' ><br />".$prompt_msg."<br /><br></td>
	                                </tr>\n";
	                }
	                else
	                {
	                            if (sizeof($results)==0)
	                        {
	                                $x .= "<tr>
	                                        <td class='tablebluerow2 tabletext' align='center' colspan='7' ><br />".$this->no_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                            } else {
	                                for($i=0;$i<sizeof($results);$i++)
	                                {
	                                        $x .= "<tr class='tablebluerow". ( ($i)%2 + 1 )."'>\n";
	
	                                        list($day_number, $in_school_time, $in_school_station, $am_status, $lunch_out_time, $lunch_out_station, $lunch_back_time, $lunch_back_station, $pm_status, $leave_school_time, $leave_school_status) = $results[$i];
	                                        $date_ts = strtotime("$year-$month-".($day_number < 10?"0":"").$day_number);
	                                        $date_string = date('Y-m-d',$date_ts);
											
	                                        $x .= $this->displayCell($i,$date_string, "tablelink","valign='top'");
											
											$am_waived = $results[$i]['AMWaive'] == 1;
											$pm_waived = $results[$i]['PMWaive'] == 1;
											$el_waived = $results[$i]['EarlyLeaveWaive'] == 1;
											
											if ($lcard->attendance_mode != 1){ // not PM mode
												$display_am_status = '--';
												if($leave_school_status == CARD_LEAVE_AM){
													$display_am_status = $Lang['StudentAttendance']['EarlyLeave'].($el_waived?"(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"");
												}else if($am_status == (string)CARD_STATUS_PRESENT){
													$display_am_status = $Lang['StudentAttendance']['Present'];
												}else if($am_status == CARD_STATUS_ABSENT){
													$display_am_status = $Lang['StudentAttendance']['Absent'].($am_waived?"(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"");
												}else if($am_status == CARD_STATUS_LATE){
													$display_am_status = $Lang['StudentAttendance']['Late'].($am_waived?"(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"");
												}else if($am_status == CARD_STATUS_OUTING){
													$display_am_status = $Lang['StudentAttendance']['Outing'];
												}
												$x .= $this->displayCell($i,$display_am_status, "tablelink","valign='top'");
											}
											if ($lcard->attendance_mode != 0){ // not AM mode
												$display_pm_status = '--';
												if($leave_school_status == CARD_LEAVE_PM){
													$display_pm_status = $Lang['StudentAttendance']['EarlyLeave'].($el_waived?"(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"");
												}else if($pm_status == (string)CARD_STATUS_PRESENT){
													$display_pm_status = $Lang['StudentAttendance']['Present'];
												}else if($pm_status == CARD_STATUS_ABSENT){
													$display_pm_status = $Lang['StudentAttendance']['Absent'].($pm_waived?"(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"");
												}else if($pm_status == CARD_STATUS_LATE){
													$display_pm_status = $Lang['StudentAttendance']['Late'].($pm_waived?"(".$i_SmartCard_Frontend_Take_Attendance_Waived.")":"");
												}else if($pm_status == CARD_STATUS_OUTING){
													$display_pm_status = $Lang['StudentAttendance']['Outing'];
												}
												$x .= $this->displayCell($i,$display_pm_status, "tablelink","valign='top'");
											}
																					
	                                        $in_school_time = ($in_school_time==""?"--":$in_school_time);
	                                        $lunch_out_time = ($lunch_out_time==""?"--":$lunch_out_time);
	                                        $lunch_back_time = ($lunch_back_time==""?"--":$lunch_back_time);
	                                        $leave_school_time = ($leave_school_time==""?"--":$leave_school_time);
	                                        $x .= $this->displayCell($i,$in_school_time, "tablelink","valign='top'");
	                                        if ($lcard->attendance_mode == 2) {
																						if (!$lcard->NoRecordLunchOut) 
																							$x .= $this->displayCell($i,$lunch_out_time, "tablelink","valign='top'");
																						$x .= $this->displayCell($i,$lunch_back_time, "tablelink","valign='top'");
																					}
	                                        $x .= $this->displayCell($i,$leave_school_time, "tablelink","valign='top'");
	
	                                        $x .= "</tr>\n";
	                                 }
	                        }
	                }
	
	                        $x .= "</table>\n";
	
	                        return $x;
	        }
	
	        function displayFormat_SmartCardPaymentBalance($tablecolor)
	        {
	                global $image_path, $LAYOUT_SKIN;
	
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	//debug($this->built_sql());
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	                    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	                    $x .= $this->displayColumn();
	
	                    if ($this->db_num_rows()==0)
	                {
//	                        $x .= "<tr>
//	                                <td class='table".$tablecolor."row2 tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
//	                                </tr>\n
//	                                ";
	                        $x .= "<tr>
	                                <td class='tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
	                                </tr>\n
	                                ";
                        $x .= '<tr><td colspan="'.$this->no_col.'" class="dotline"><img src="/images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td></tr>'."\n";
	                    } else {
	
	                        while($row = $this->db_fetch_array())
	                        {
	                                    $i++;
	
	                                    if($i>$this->page_size) break;
	                                $x .= "<tr class='table".$tablecolor."row". ( ($i+1)%2 + 1 )."'>\n";
	
	                                $x .= "<td class='tabletext' valign='top'>". ++$n_start ."</td>\n";
	
	                                if($icon_display)
	                                        $x .= "<td class='tabletext' valign='top'><img src='$image_path/$LAYOUT_SKIN/iMail/icon_ppl_". ($TabID==1?"in_group":($TabID==3?"ex_group":"ex")).".gif' align='absmiddle' /></td>\n";
	
	                                for($j=1; $j<$this->no_col; $j++)
	                                {
	                                                 if (trim($row[$j]) == "")
	                                                 {
	                                                         $row[$j] = "&nbsp;";
	                                                 }
	
	                                        $x .= $this->displayCell($j,nl2br($row[$j]), "tablelink","valign='top'");
	
	                                }
	
	                                    $x .= "</tr>\n";
	                                }
	                            }
	
	                        if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".($this->no_col)."' class='table".$tablecolor."bottom'>".$this->navigation()."</td></tr>";
	                        $x .= "</table>\n";
	
	                        $this->db_free_result();
	
	                        return $x;
	        }
	
	function displayFormat_SmartCardTakeAttendance($display_period)
	{
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $result, $editAllowed, $reason_js_array, $linterface;
		global $i_StudentAttendance_Status_Present, $i_StudentAttendance_Status_PreAbsent, $i_StudentAttendance_Status_Late, $i_StudentAttendance_Status_Outing, $i_Attendance_Reason, $i_Attendance_Standard;
		global $i_SmartCard_DailyOperation_Preset_Absence, $i_UserName, $i_Attendance_Date, $i_Attendance_DayType, $i_Attendance_Remark, $isStudentHelper;
		global $i_StudentAttendance_Outing, $i_SmartCard_StudentOutingDate, $i_SmartCard_StudentOutingOutTime, $i_SmartCard_StudentOutingBackTime, $i_SmartCard_StudentOutingLocation, $i_SmartCard_StudentOutingReason;
		global $sys_custom;
		global $flag_is_loaded_before, $number_calendar_used_before, $ec_words, $Lang, $IsEditDaysBeforeDisabled;
		if($sys_custom['StudentAttendance']['NoCardPhoto']){
			global $studentIdToNoCardPhotoRecords;
		}
		if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
			global $bodyTemperatureRecords;
		}
		if($sys_custom['SmartCardAttendance_SubmittedProveDocumentForEarlyLeave']){
			global $studentIdToEarlyLeaveRecords;
		}
		$disable_attr = $IsEditDaysBeforeDisabled?' disabled="disabled" ':'';
			
	  $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	  $x .= $this->displayColumn();
	
	  if (sizeof($result)==0)
		{
			$colspan = 17;
			if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
			{
				$colspan += 4;
			}
			if($sys_custom['SmartCardAttendance_StudentAbsentSession2'])
			{
				$colspan += 1;
			}
			if($sys_custom['SmartCardAttendance_SubmittedProveDocumentForEarlyLeave']){
				$colspan += 1;
			}
			
			$x .= "<tr>
			    <td class='tablegreenrow2 tabletext' align='center' colspan='$colspan' ><br />".$this->no_msg."<br /><br></td>
			    </tr>\n
			    ";
		} else {
			# Reason Words
			include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
			$lword = new libwordtemplates();
			$words = $lword->getWordListAttendance();
			$hasWord = sizeof($words)!=0;
			$reason_js_array = "";
			$disable_color="#DFDFDF";  # background color for disabled text field
			$enable_color="#FFFFFF";  # background color for enabled text field
			if($hasWord)
			{
				$words_absence = $lword->getWordListAttendance(1);
				$words_late = $lword->getWordListAttendance(2);
				foreach($words_absence as $key=>$word)
					$words_absence[$key]= htmlspecialchars($word);
				foreach($words_late as $key=>$word)
					$words_late[$key]= htmlspecialchars($word);
		    //$x .= $linterface->CONVERT_TO_JS_ARRAY($words, "jArrayWords", 1);
		    $x .= $linterface->CONVERT_TO_JS_ARRAY($words_absence, "jArrayWordsAbsence", 1);
		    $x .= $linterface->CONVERT_TO_JS_ARRAY($words_late, "jArrayWordsLate", 1);
			}
			
			// Outing Words 
			include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
			$StudentAttend = new libcardstudentattend2();
			if($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']){
				$DefaultNumOfSessionSettings = $StudentAttend->getDefaultNumOfSessionSettings();
			}
			$disable_modify_status = $StudentAttend->iSmartCardDisableModifyStatusAfterConfirmed;
			$status_map = array('0'=>$Lang['StudentAttendance']['Present'],'1'=>$Lang['StudentAttendance']['Absent'],'2'=>$Lang['StudentAttendance']['Late'],'3'=>$Lang['StudentAttendance']['Outing']);
			
			$words_outing = $StudentAttend->Get_Outing_Reason();
			foreach($words_outing as $key=>$word)
				$words_outing[$key]= htmlspecialchars($word);
			$x .= $linterface->CONVERT_TO_JS_ARRAY($words_outing, "jArrayWordsOuting", 1);
			for($i=0;$i<sizeof($result);$i++)
			{
		    list($record_id, $user_id, $name,$class_number, $in_school_time, $in_school_station, 
		    		$attend_status,$attend_reason,$office_remark,$preset_record,$preset_reason,$preset_remark,$preset_waive, 
		    		$outing_record, $outing_date, $outing_time, $back_time, $outing_location, 
		    		$outing_obj, $outing_detail, $student_name, $admin_reason, $absent_session,
		    		$DateModified,$ModifyName,$IsConfirmed,$ConfirmedUser,$IsWaived,$LateSession,
		    		$RequestLeaveSession,$PlayTruantSession,$OfficalLeaveSession,$AbsentSession, $DefaultAbsentReason,
		    		$DefaultLateReason,$DefaultOutingReason, $LeaveStatus,$LeaveSchoolTime, $DocumentStatus, $ConfirmedByAdmin) = $result[$i];
		    $attend_status += 0;
		    $select_status = '';
		    $drop_down_status_style = '';
		    if($disable_modify_status && $ConfirmedByAdmin){
		    	$select_status .= '<span title="'.$Lang['StudentAttendance']['AttendanceRecordConfirmedByAdminAlertMsg'].'">'.$status_map[$attend_status].'</span>';
		    	$drop_down_status_style = ' style="display:none" ';
		    }
		    $select_status .= "<SELECT id=\"drop_down_status_$i\" name=drop_down_status[] onChange='clearReason(".$i.");setEditAllowed(".$i.");ChangeDefaultSessionSelect(".$i.");' ".$disable_attr.$drop_down_status_style.">\n";
		    $select_status .= "<OPTION value='0' ".($attend_status=="0" ? "SELECTED":"").">".$Lang['StudentAttendance']['Present']."</OPTION>\n";
		    $select_status .= "<OPTION value=1 ".($attend_status=="1"? "SELECTED":"").">".$Lang['StudentAttendance']['Absent']."</OPTION>\n";
		    $select_status .= "<OPTION value=2 ".($attend_status=="2"? "SELECTED":"").">".$Lang['StudentAttendance']['Late']."</OPTION>\n";
		    $select_status .= "<OPTION value=3 ".($attend_status=="3"? "SELECTED":"").">".$Lang['StudentAttendance']['Outing']."</OPTION>\n";
		    $select_status .= "</SELECT>\n";
		    
		    
		    $select_status .= "<input name=record_id[$i] type=hidden value=\"$record_id\">\n";
		    $select_status .= "<input name=user_id[$i] type=hidden value=\"$user_id\">\n";
		    $select_status .= "<input name=\"Confirmed[".$i."]\" id=\"Confirmed[".$i."]\" type=hidden value=\"".$IsConfirmed."\">\n";
		    $select_status .= "<input name=\"AbsentDefaultReason[".$i."]\" id=\"AbsentDefaultReason[".$i."]\" type=hidden value=\"".htmlspecialchars($DefaultAbsentReason,ENT_QUOTES)."\">\n";
		    $select_status .= "<input name=\"LateDefaultReason[".$i."]\" id=\"LateDefaultReason[".$i."]\" type=hidden value=\"".htmlspecialchars($DefaultLateReason,ENT_QUOTES)."\">\n";
		    $select_status .= "<input name=\"OutingDefaultReason[".$i."]\" id=\"OutingDefaultReason[".$i."]\" type=hidden value=\"".htmlspecialchars($DefaultOutingReason,ENT_QUOTES)."\">\n";
		    $select_status .= "<input name=\"PrevStatus[".$i."]\" id=\"PrevStatus[".$i."]\" type=hidden value=\"".$attend_status."\">\n";
		
		    # set the reason editable if status is absent or late
		    if($attend_status=="1" || $attend_status=="2"){
		            $editAllowed[$i] = 1;
		    }
		    else{
		            $editAllowed[$i] = 0;
		    }
			
		    if ($hasWord)
		    {
          # Begin of Staff Input Reason Selection List
          $select_input_word = $linterface->GET_PRESET_LIST("getReasons($i)", '_'.$i, "input_reason".$i);
					# End of Staff Input Reason Selection List
		    }
			
		    $hasReason = true;
		    switch ($attend_status){
		            case "1":       $note = "<img src=\"$image_path/{$LAYOUT_SKIN}/smartcard/icon_absent.gif\" title=\"$i_StudentAttendance_Status_PreAbsent\">";
		                            $css = "attendance_norecord";
		                            $hasReason=true;
		                            break;
		            case "2":       $note = "<img src=\"$image_path/{$LAYOUT_SKIN}/smartcard/icon_late.gif\" title=\"$i_StudentAttendance_Status_Late\">";
		                            $css = "attendance_late";
		                            $hasReason = true;
		                            break;
		            case "3":       $note = "<img src=\"$image_path/{$LAYOUT_SKIN}/smartcard/icon_outing.gif\" title=\"$i_StudentAttendance_Status_Outing\">";
		                            $css = "attendance_outing";
		                            break;
		            default:        $note = "<img src=\"$image_path/{$LAYOUT_SKIN}/smartcard/icon_present.gif\" title=\"$i_StudentAttendance_Status_Present\">";
		                            $css = "row_approved";
		                            break;
		    }
		    if($LeaveStatus == 1 || $LeaveStatus == 2){ // AM early leave
		    	$leave_tooltip  = $Lang['StudentAttendance']['EarlyLeave']."(";
		    	$leave_tooltip .= $LeaveStatus==1?$Lang['StudentAttendance']['AM']:$Lang['StudentAttendance']['PM'];
		    	$leave_tooltip .= ") ";
		    	if(trim($LeaveSchoolTime) != "") $leave_tooltip .= $LeaveSchoolTime;
		    	$note .= "<img src=\"$image_path/{$LAYOUT_SKIN}/attendance/icon_early_leave_bw.gif\" width=\"12\" height=\"12\" title=\"$leave_tooltip\" />";
		    }
				// use below style if record not confirmed
				if (!$IsConfirmed) {
					$css = "tablebottom";
				}
				
		    # Preset Absence
	      if($preset_record==1){
	        $preset_table = "<table border=0 cellspacing=0 cellpadding=1 class=attendancestudentphoto><tr><td>";
	        $preset_table .= "<table border=0 cellspacing=0 cellpadding=3>";
	        $preset_table .= "<tr><Td class=\'tablebluetop tabletext\' nowrap><B>[$i_SmartCard_DailyOperation_Preset_Absence]</b></td></tr>";
	        $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_UserName: $student_name ($class_number)</font></td></tr>";
	        $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_Attendance_DayType: $display_period</font></td></tr>";
          $preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_Attendance_Reason: ".str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$preset_reason)."</font></td></tr>";
          $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_Attendance_Remark: ".str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$preset_remark)."</font></td></tr>";
	        $preset_table.="</table></td></tr></table>";
	        $preset_table = htmlspecialchars($preset_table,ENT_QUOTES);
	        $preset_abs_info = "<img onMouseOut='hidePresetAbs();' onMouseOver=\"showPresetAbs(this,'$preset_table')\" src=\"{$image_path}/{$LAYOUT_SKIN}/preset_abs.gif\" align=\"absmiddle\">";
	    	}//else $preset_abs_info ="&nbsp;";
	
		    # show reason
		    $x .= "<tr class='tablerow". ( ($i)%2 + 1 )."'>\n";
			
				#Outing Detail
	   		if($outing_record==1){
	   			$preset_table = "<table border=0 cellspacing=0 cellpadding=1 class=attendancestudentphoto><tr><td>";
	        $preset_table .= "<table border=0 cellspacing=0 cellpadding=3>";
	        $preset_table .= "<tr><Td class=\'tablebluetop tabletext\' nowrap><B>[$i_StudentAttendance_Outing]</b></td></tr>";
	        $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_UserName: $student_name ($class_number)</font></td></tr>";
	        $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingOutTime: $outing_time</font></td></tr>";
	        $preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingBackTime: $back_time</font></td></tr>";
	        $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingLocation: $outing_location</font></td></tr>";
	        $preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingReason: $outing_obj</font></td></tr>";
	        $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_Attendance_Remark: ".str_replace(array("'","\r\n","\n","\r"),array("\'","<br>", "<br>", "<br>"),$outing_detail)."</font></td></tr>";
	        $preset_table .="</table></td></tr></table>";
	        $preset_table = htmlspecialchars($preset_table,ENT_QUOTES);
	        $preset_abs_info = "<img onMouseOut='hidePresetAbs();' onMouseOver=\"showPresetAbs(this,'$preset_table')\" src=\"{$image_path}/{$LAYOUT_SKIN}/preset_abs.gif\" align=\"absmiddle\">";
	      }//else $preset_abs_info ="&nbsp;";
			
	      # show reason
	      $x .= "<tr class='".$css."'>\n";
			
		if($sys_custom['StudentAttendance']['AllowEditTime'] && $in_school_time != '-'){
			$in_school_time_val = $in_school_time;
			$in_school_time = '<span id="InSchoolTime_Display'.$user_id.'">'.$in_school_time_val.'&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Edit'], "button", "onRevealEditTime($user_id,'".$in_school_time_val."')", "editTimeBtn".$user_id, "", "", '').'</span>';
			$in_school_time.= '<span id="InSchoolTime_Edit'.$user_id.'" style="display:none;">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "onCancelEditTime($user_id)", "cancelTimeBtn".$user_id, "", "", '').'</span>';
		}	
		if($sys_custom['StudentAttendance']['NoCardPhoto'] && isset($studentIdToNoCardPhotoRecords[$user_id])){
			$photo_path = '/file/student_attendance/no_card_photo/'.$studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1]['PhotoPath'];
			$take_photo_time = $studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1]['TakePhotoTime'];
			$photo_userinfo = $studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1]['StudentClassName'].'#'.$studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1]['StudentClassNumber'].' '.$studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1][Get_Lang_Selection('StudentChineseName','StudentEnglishName')];
			$in_school_time .= '<a href="'.$photo_path.'" rel="photos" class="tablelink fancybox" title="'.$take_photo_time.' '.$photo_userinfo.' '.$Lang['StudentAttendance']['ForgotBringCard'].'"><img src="'.$photo_path.'" width="80" height="60" border="0" align="absmiddle"></a>';
		}
	      //if(!$isStudentHelper){
      	$attend_reason = "<input type=text class=tabletext name=reason".$i." id=reason".$i." STYLE=\"background:#ffffff\" VALUE=\"".htmlspecialchars($attend_reason,ENT_QUOTES)."\" ".$disable_attr.">".$linterface->GET_PRESET_LIST("getRemarks($i)", $i, "reason$i");
      	//$office_remark_input = "<input type=\"text\" class=\"tabletext\" name=\"OfficeRemark[]\" id=\"OfficeRemark".$i."\" STYLE=\"background:#ffffff\" VALUE=\"".htmlspecialchars($office_remark,ENT_QUOTES)."\">";
      	$office_remark_input = "<span id=\"OfficeRemark".$i."\">".htmlspecialchars($office_remark,ENT_QUOTES)."<input type=\"hidden\" name=\"OfficeRemark[]\" value=\"".htmlspecialchars($office_remark,ENT_QUOTES)."\" / ".$disable_attr."></span>";
        $x .= $this->displayCell($i,$class_number, "tablelink ","valign='top'");
        $x .= $this->displayCell($i,$name, "tablelink ","valign='top'");
        $x .= $this->displayCell($i,$in_school_time, "tablelink ","valign='top'");
        $x .= $this->displayCell($i,$in_school_station, "tablelink ","valign='top'");
        if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
        	$body_temperature = '-';
			if(isset($bodyTemperatureRecords[$user_id])){
				$temperature_status = $bodyTemperatureRecords[$user_id][count($bodyTemperatureRecords[$user_id])-1]['TemperatureStatus'];
				$temperature_value = $bodyTemperatureRecords[$user_id][count($bodyTemperatureRecords[$user_id])-1]['TemperatureValue'];
				$body_temperature = '<span style="color:'.($temperature_status==1?'red':'green').'" title="'.($temperature_status==1?$Lang['StudentAttendance']['BodyTemperatureAbnormal']:$Lang['StudentAttendance']['BodyTemperatureNormal']).'">'.$temperature_value.' &#8451;</span>';
			}
        	$x .= $this->displayCell($i,$body_temperature, "tablelink ","valign='top'");
        }
        $x .= $this->displayCell($i,$note, "tablelink ","valign='top'");
        if(($outing_record==1)||($preset_record==1)){
        	$x .= $this->displayCell($i,$preset_abs_info, "tablelink ","valign='top'");
    	  }else{
      		$x .= $this->displayCell($i,'&nbsp;', "tablelink ","valign='top'");
    	  }

        $x .= $this->displayCell($i,$select_status, "tablelink ","valign='top'");
 
        if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
        {
        	if ($LateSession == "" && $RequestLeaveSession == "" && $PlayTruantSession == "" && $OfficalLeaveSession == "") {
        		if ($attend_status == CARD_STATUS_ABSENT) {
		  			$LateSession = $PlayTruantSession = $OfficalLeaveSession = 0;
		  			$RequestLeaveSession = 1;
		  			if(isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsenteesism']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsenteesism'] != -1){
		  				$PlayTruantSession = $DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsenteesism'];
		  			}
		  			if(isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForLeave']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForLeave'] != -1){
		  				$RequestLeaveSession = $DefaultNumOfSessionSettings['DefaultNumOfSessionForLeave'];
		  			}
		  			
		  		}
		  		else if ($attend_status==CARD_STATUS_LATE) {
		  			$LateSession = 1;
		  			$RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = 0;
		  			if(isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForLate']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForLate'] != -1){
		  				$LateSession = $DefaultNumOfSessionSettings['DefaultNumOfSessionForLate'];
		  			}
		  		}
		  		else {
		  			$LateSession = $RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = 0;
		  		}
        	}
        	
        	$selOfficalLeaveSession = "<select name=\"offical_leave_session[]\" id=\"offical_leave_session$i\"/ ".$disable_attr.">";
					$selLateSession = "<select name=\"late_session[]\" id=\"late_session$i\"/ ".$disable_attr.">";
					$selRequestLeaveSession = "<select name=\"request_leave_session[]\" id=\"request_leave_session$i\"/ ".$disable_attr.">";
					$selPlayTruantSession = "<select name=\"play_truant_session[]\" id=\"play_truant_session$i\"/ ".$disable_attr.">";
					
					$k=0.0;
					while($k<=CARD_STUDENT_MAX_SESSION){
						$LateSelected = ($k == $LateSession)? 'selected':'';
						$OfficalLeaveSelected = ($k == $OfficalLeaveSession)? 'selected':'';
						$RequestLeaveSelected = ($k == $RequestLeaveSession)? 'selected':'';
						$PlayTruantSelected = ($k == $PlayTruantSession)? 'selected':'';
						
						$selOfficalLeaveSession .= "<option value=\"$k\" ".$OfficalLeaveSelected." >".$k."</option>";
						$selLateSession .= "<option value=\"".$k."\" ".$LateSelected." >".$k."</option>";
						$selRequestLeaveSession .= "<option value=\"".$k."\" ".$RequestLeaveSelected." >".$k."</option>";
						$selPlayTruantSession .= "<option value=\"".$k."\" ".$PlayTruantSelected." >".$k."</option>";
						$k+= $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] > 0 ? $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] : 0.5;
					}
					$selOfficalLeaveSession .= "</select>\n";
					$selLateSession .= "</select>\n";
					$selRequestLeaveSession .= "</select>\n";
					$selPlayTruantSession .= "</select>\n";
					
        	$x .= $this->displayCell($i,$selLateSession, "tablelink ","valign='top'");
        	$x .= $this->displayCell($i,$selRequestLeaveSession, "tablelink ","valign='top'");
        	$x .= $this->displayCell($i,$selPlayTruantSession, "tablelink ","valign='top'");
        	$x .= $this->displayCell($i,$selOfficalLeaveSession, "tablelink ","valign='top'");
        }
        if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){
	  		if($AbsentSession == ""){
	  			$AbsentSession = 0;
	  			if ($attend_status == CARD_STATUS_ABSENT) {
		  			if(isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsent']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsent'] != -1){
		  				$AbsentSession = $DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsent'];
		  			}
		  		}
	  		}
	  		$selAbsentSession = "<select name=\"absent2_session[]\" id=\"absent2_session$i\"/ ".$disable_attr.">";
	  		
	  		$k=0.0;
			while($k<=CARD_STUDENT_MAX_SESSION){
				$AbsentSelected = ($k == $AbsentSession)? 'selected':'';
				$selAbsentSession .= "<option value=\"$k\" ".$AbsentSelected." >".$k."</option>";
				$k+= $sys_custom['SmartCardAttendance_StudentAbsentSessionStep']>0 ? $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] : 0.5;
			}
			$selAbsentSession .= "</select>\n";
			$x .= $this->displayCell($i,$selAbsentSession, "tablelink ","valign='top'");
	  	}
        // block the waive options for student
        if (!$isStudentHelper){
        	if($StudentAttend->TeacherCanManageWaiveStatus){
        		$x .= $this->displayCell($i,'<input type="checkbox" class="WaiveBtn" name="Waived'.$i.'" id="Waived'.$i.'" value="1" '.(($IsWaived == 1 || ($attend_status=="1" && $preset_waive == "1"))? 'checked':'').' '.$disable_attr.'>', "tablelink","valign='top'");
        	}
        }
        if($StudentAttend->TeacherCanManageProveDocumentStatus){
        	$x .= $this->displayCell($i,'<span id="HandIn'.$i.'" '.($attend_status==CARD_STATUS_ABSENT?'':'style="display:none"').'><input type="checkbox" class="HandIn" name="HandIn_prove_'.$i.'" id="HandIn_prove_'.$i.'" value="1" '.($DocumentStatus == 1? 'checked':'').' /></span>', "tablelink","valign='top'");
        	if($sys_custom['SmartCardAttendance_SubmittedProveDocumentForEarlyLeave']){
        		$x .= $this->displayCell($i,isset($studentIdToEarlyLeaveRecords[$user_id])? '<span id="HandInEL'.$i.'"><input type="checkbox" class="HandInEL" name="HandInEL_prove_'.$i.'" id="HandInEL_prove_'.$i.'" value="1" '.(isset($studentIdToEarlyLeaveRecords[$user_id]) && $studentIdToEarlyLeaveRecords[$user_id]['DocumentStatus'] == 1? 'checked':'').' /></span>':'&nbsp;', "tablelink","valign='top'");
        	}
        }
        $style_display = $attend_status==CARD_STATUS_PRESENT ? "hidden":"visible";
        $input_reason = "<div id=\"div_reason_$i\" style=\"visibility:$style_display;\">".($IsEditDaysBeforeDisabled?"":$select_input_word)."<input type=text class=tabletext id=\"input_reason$i\" name=\"input_reason$i\" style=\"background:#ffffff\" value=\"".htmlspecialchars($admin_reason,ENT_QUOTES)."\" ".$disable_attr."></div>";
      	if($StudentAttend->TeacherCanManageReason){
      		$x .= $this->displayCell($i,$input_reason, "tablelink ","valign='top' align='right' nowrap");
      	}
      	if($StudentAttend->TeacherCanManageTeacherRemark){
        	$x .= $this->displayCell($i,$attend_reason, "tablelink ","valign='top' align='right' nowrap");
      	}
        $x .= $this->displayCell($i,$office_remark_input, "tablelink ","valign='top' align='left' nowrap");
        $x .= $this->displayCell($i,$DateModified.' ('.$ModifyName.')', "tablelink ","valign='top'");
        $x .= $this->displayCell($i,$ConfirmedUser, "tablelink ","valign='top' align='right' nowrap");
	     
			  $x .= "</tr>\n";
			}
		}
	
		$x .= "</table>\n";
		
		return $x;
	}
	
  function displayFormat_SmartCardTakeGroupAttendance($display_period)
  {
	  global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $result, $editAllowed, $reason_js_array, $linterface;
	  global $i_StudentAttendance_Status_Present, $i_StudentAttendance_Status_PreAbsent, $i_StudentAttendance_Status_Late, $i_StudentAttendance_Status_Outing, $i_Attendance_Reason, $i_Attendance_Standard;
	  global $i_SmartCard_DailyOperation_Preset_Absence, $i_UserName, $i_Attendance_Date, $i_Attendance_DayType, $i_Attendance_Remark, $isStudentHelper;
	  global $i_StudentAttendance_Outing, $i_SmartCard_StudentOutingDate, $i_SmartCard_StudentOutingOutTime, $i_SmartCard_StudentOutingBackTime, $i_SmartCard_StudentOutingLocation, $i_SmartCard_StudentOutingReason;
		global $sys_custom;
		global $flag_is_loaded_before, $number_calendar_used_before, $ec_words, $Lang, $IsEditDaysBeforeDisabled;
		if($sys_custom['StudentAttendance']['NoCardPhoto']){
			global $studentIdToNoCardPhotoRecords;
		}
		if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
			global $bodyTemperatureRecords;
		}
		$disable_attr = $IsEditDaysBeforeDisabled?' disabled="disabled" ':'';
	
    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
    $x .= $this->displayColumn();
		
		if (sizeof($result)==0)
		{
	    $colspan = 12;
	    if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	    {
	    	$colspan += 4;
	    }
	    if($sys_custom['SmartCardAttendance_StudentAbsentSession2'])
	    {
	    	$colspan += 1;
	    }
	    $x .= "<tr>
	            <td class='tablegreenrow2 tabletext' align='center' colspan='$colspan' ><br />".$this->no_msg."<br /><br></td>
	            </tr>\n
	            ";
		} else {
	    # Reason Words
	    include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
	    $lword = new libwordtemplates();
	    $words = $lword->getWordListAttendance();
	    $hasWord = sizeof($words)!=0;
	    $reason_js_array = "";
	    $disable_color="#DFDFDF";  # background color for disabled text field
	    $enable_color="#FFFFFF";  # background color for enabled text field
	    if($hasWord)
	    {
				$words_absence = $lword->getWordListAttendance(1);
				$words_late = $lword->getWordListAttendance(2);
				//$x .= $linterface->CONVERT_TO_JS_ARRAY($words, "jArrayWords", 1);
				$x .= $linterface->CONVERT_TO_JS_ARRAY($words_absence, "jArrayWordsAbsence", 1);
				$x .= $linterface->CONVERT_TO_JS_ARRAY($words_late, "jArrayWordsLate", 1);
	    }
			
			// Outing Words 
			include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
			$StudentAttend = new libcardstudentattend2();
			if($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']){
				$DefaultNumOfSessionSettings = $StudentAttend->getDefaultNumOfSessionSettings();
			}
			
			$disable_modify_status = $StudentAttend->iSmartCardDisableModifyStatusAfterConfirmed;
			$status_map = array('0'=>$Lang['StudentAttendance']['Present'],'1'=>$Lang['StudentAttendance']['Absent'],'2'=>$Lang['StudentAttendance']['Late'],'3'=>$Lang['StudentAttendance']['Outing']);
			
			$words_outing = $StudentAttend->Get_Outing_Reason();
			foreach($words_outing as $key=>$word)
				$words_outing[$key]= htmlspecialchars($word);
			$x .= $linterface->CONVERT_TO_JS_ARRAY($words_outing, "jArrayWordsOuting", 1);
			
	    for($i=0;$i<sizeof($result);$i++)
	    {
        list($record_id, $user_id, $name,$class_number,$class_name, $in_school_time, 
        		$in_school_station, $attend_status,$attend_reason,$office_remark,$preset_record,$preset_reason,
        		$preset_remark,$preset_waive,  $outing_record, $outing_date, $outing_time, $back_time, 
        		$outing_location, $outing_obj, $outing_detail, $student_name, $admin_reason, 
        		$absent_session,$DateModified,$ModifyName,$IsConfirmed,$ConfirmedUser,$IsWaived,
        		$LateSession,$RequestLeaveSession,$PlayTruantSession,$OfficalLeaveSession,$AbsentSession,
        		$DefaultAbsentReason,$DefaultLateReason,$DefaultOutingReason, $LeaveStatus,$LeaveSchoolTime,$DocumentStatus,$ConfirmedByAdmin) = $result[$i];

        $attend_status += 0;
        $select_status = '';
	    $drop_down_status_style = '';
	    if($disable_modify_status && $ConfirmedByAdmin){
	    	$select_status .= '<span title="'.$Lang['StudentAttendance']['AttendanceRecordConfirmedByAdminAlertMsg'].'">'.$status_map[$attend_status].'</span>';
	    	$drop_down_status_style = ' style="display:none" ';
	    }
        $select_status .= "<SELECT id=\"drop_down_status_$i\" name=drop_down_status[] onChange='clearReason($i);setEditAllowed(".$i.");ChangeDefaultSessionSelect(".$i.");' ".$disable_attr.$drop_down_status_style.">\n";
        $present_status=0;
        $select_status .= "<OPTION value='$present_status' ".($attend_status=="0" ? "SELECTED":"").">".$Lang['StudentAttendance']['Present']."</OPTION>\n";
        $select_status .= "<OPTION value=1 ".($attend_status=="1"? "SELECTED":"").">".$Lang['StudentAttendance']['Absent']."</OPTION>\n";
        $select_status .= "<OPTION value=2 ".($attend_status=="2"? "SELECTED":"").">".$Lang['StudentAttendance']['Late']."</OPTION>\n";
        $select_status .= "<OPTION value=3 ".($attend_status=="3"? "SELECTED":"").">".$Lang['StudentAttendance']['Outing']."</OPTION>\n";
        $select_status .= "</SELECT>\n";

        $select_status .= "<input name=record_id[$i] type=hidden value=\"$record_id\">\n";
        $select_status .= "<input name=user_id[$i] type=hidden value=\"$user_id\">\n";
        $select_status .= "<input name=\"Confirmed[".$i."]\" id=\"Confirmed[".$i."]\" type=hidden value=\"".$IsConfirmed."\">\n";
        $select_status .= "<input name=\"AbsentDefaultReason[".$i."]\" id=\"AbsentDefaultReason[".$i."]\" type=hidden value=\"".htmlspecialchars($DefaultAbsentReason,ENT_QUOTES)."\">\n";
		    $select_status .= "<input name=\"LateDefaultReason[".$i."]\" id=\"LateDefaultReason[".$i."]\" type=hidden value=\"".htmlspecialchars($DefaultLateReason,ENT_QUOTES)."\">\n";
		    $select_status .= "<input name=\"OutingDefaultReason[".$i."]\" id=\"OutingDefaultReason[".$i."]\" type=hidden value=\"".htmlspecialchars($DefaultOutingReason,ENT_QUOTES)."\">\n";
		    $select_status .= "<input name=\"PrevStatus[".$i."]\" id=\"PrevStatus[".$i."]\" type=hidden value=\"".$attend_status."\">\n";
	
        # set the reason editable if status is absent or late
        if($attend_status=="1" || $attend_status=="2"){
                $editAllowed[$i] = 1;
        }
        else{
                $editAllowed[$i] = 0;
        }
	
        if ($hasWord)
        {
	        #$select_word = $linterface->GET_PRESET_LIST("jArrayWords", $i, "reason".$i);
	        //$select_word = $linterface->GET_PRESET_LIST("getReasons($i)", $i, "reason".$i);
          # Begin of Staff Input Reason Selection List
          $select_input_word = $linterface->GET_PRESET_LIST("getReasons($i)", '_'.$i, "input_reason".$i);
           
        }
				
        $hasReason = true;
        switch ($attend_status){
                case "1":       $note = "<img src=\"$image_path/{$LAYOUT_SKIN}/smartcard/icon_absent.gif\" title=\"$i_StudentAttendance_Status_PreAbsent\">";
                                $css = "attendance_norecord";
                                $hasReason=true;
                                break;
                case "2":       $note = "<img src=\"$image_path/{$LAYOUT_SKIN}/smartcard/icon_late.gif\" title=\"$i_StudentAttendance_Status_Late\">";
                                $css = "attendance_late";
                                $hasReason = true;
                                break;
                case "3":       $note = "<img src=\"$image_path/{$LAYOUT_SKIN}/smartcard/icon_outing.gif\" title=\"$i_StudentAttendance_Status_Outing\">";
                                $css = "attendance_outing";
                                break;
                default:        $note = "<img src=\"$image_path/{$LAYOUT_SKIN}/smartcard/icon_present.gif\" title=\"$i_StudentAttendance_Status_Present\">";
                                $css = "row_approved";
                                break;
        }
        if($LeaveStatus == 1 || $LeaveStatus == 2){ // AM early leave
	    	$leave_tooltip  = $Lang['StudentAttendance']['EarlyLeave']."(";
	    	$leave_tooltip .= $LeaveStatus==1?$Lang['StudentAttendance']['AM']:$Lang['StudentAttendance']['PM'];
	    	$leave_tooltip .= ") ";
	    	if(trim($LeaveSchoolTime) != "") $leave_tooltip .= $LeaveSchoolTime;
	    	$note .= "<img src=\"$image_path/{$LAYOUT_SKIN}/attendance/icon_early_leave_bw.gif\" width=\"12\" height=\"12\" title=\"$leave_tooltip\" />";
	    }
				// use below style if record not confirmed
				if (!$IsConfirmed) {
					$css = "tablebottom";
				}
				
        # Preset Absence
        if($preset_record==1){
          $preset_table = "<table border=0 cellspacing=0 cellpadding=1 class=attendancestudentphoto><tr><td>";
          $preset_table .= "<table border=0 cellspacing=0 cellpadding=3>";
          $preset_table .= "<tr><Td class=\'tablebluetop tabletext\' nowrap><B>[$i_SmartCard_DailyOperation_Preset_Absence]</b></td></tr>";
          $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_UserName: $student_name ($class_number)</font></td></tr>";
          $preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_Attendance_Date: ".date('Y-m-d')."</font></td></tr>";
          $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_Attendance_DayType: $display_period</font></td></tr>";
          if(!$isStudentHelper){
                  $preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_Attendance_Reason: ".str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$preset_reason)."</font></td></tr>";
                  $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_Attendance_Remark: ".str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$preset_remark)."</font></td></tr>";
          }
          $preset_table.="</table></td></tr></table>";
          $preset_table = htmlspecialchars($preset_table,ENT_QUOTES);
          $preset_abs_info = "<img onMouseOut='hidePresetAbs();' onMouseOver=\"showPresetAbs(this,'$preset_table')\" src=\"{$image_path}/{$LAYOUT_SKIN}/preset_abs.gif\" align=\"absmiddle\">";
        }else $preset_abs_info ="&nbsp;";
	
        # show reason
        $x .= "<tr class='tablerow". ( ($i)%2 + 1 )."'>\n";
	            
        if($outing_record==1){
     			$preset_table = "<table border=0 cellspacing=0 cellpadding=1 class=attendancestudentphoto><tr><td>";
          $preset_table .= "<table border=0 cellspacing=0 cellpadding=3>";
          $preset_table .= "<tr><Td class=\'tablebluetop tabletext\' nowrap><B>[$i_StudentAttendance_Outing]</b></td></tr>";
          $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_UserName: $student_name ($class_number)</font></td></tr>";
          $preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingDate: $outing_date</font></td></tr>";
          $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingOutTime: $outing_time</font></td></tr>";
          $preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingBackTime: $back_time</font></td></tr>";
          $preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingLocation: $outing_location</font></td></tr>";
          $preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingReason: $outing_obj</font></td></tr>";
          if(!$isStudentHelper){
          	$preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_Attendance_Remark: ".str_replace(array("'","\r\n","\n","\r"),array("\'","<br>", "<br>", "<br>"),$outing_detail)."</font></td></tr>";
          }
          $preset_table .="</table></td></tr></table>";
          $preset_table = htmlspecialchars($preset_table,ENT_QUOTES);
          $preset_abs_info = "<img onMouseOut='hidePresetAbs();' onMouseOver=\"showPresetAbs(this,'$preset_table')\" src=\"{$image_path}/{$LAYOUT_SKIN}/preset_abs.gif\" align=\"absmiddle\">";
				}//else $preset_abs_info ="&nbsp;";
	
        # show reason
        $x .= "<tr class='".$css."'>\n";

		if($sys_custom['StudentAttendance']['AllowEditTime'] && $in_school_time != '-'){
			$in_school_time_val = $in_school_time;
			$in_school_time = '<span id="InSchoolTime_Display'.$user_id.'">'.$in_school_time_val.'&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Edit'], "button", "onRevealEditTime($user_id,'".$in_school_time_val."')", "editTimeBtn".$user_id, "", "", '').'</span>';
			$in_school_time.= '<span id="InSchoolTime_Edit'.$user_id.'" style="display:none;">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "onCancelEditTime($user_id)", "cancelTimeBtn".$user_id, "", "", '').'</span>';
		}
		if($sys_custom['StudentAttendance']['NoCardPhoto'] && isset($studentIdToNoCardPhotoRecords[$user_id])){
			$photo_path = '/file/student_attendance/no_card_photo/'.$studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1]['PhotoPath'];
			$take_photo_time = $studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1]['TakePhotoTime'];
			$photo_userinfo = $studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1]['StudentClassName'].'#'.$studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1]['StudentClassNumber'].' '.$studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1][Get_Lang_Selection('StudentChineseName','StudentEnglishName')];
			$in_school_time .= '<a href="'.$photo_path.'" rel="photos" class="tablelink fancybox" title="'.$take_photo_time.' '.$photo_userinfo.' '.$Lang['StudentAttendance']['ForgotBringCard'].'"><img src="'.$photo_path.'" width="80" height="60" border="0" align="absmiddle"></a>';
		}
        //if(!$isStudentHelper){
          $attend_reason = "<input type=text class=tabletext name=reason$i id=reason$i STYLE=\"background:#ffffff\" VALUE=\"".htmlspecialchars($attend_reason,ENT_QUOTES)."\" ".$disable_attr.">".$linterface->GET_PRESET_LIST("getRemarks($i)", $i, "reason$i");
       		//   $x .= $this->displayCell($i,$class_number, "tablelink","valign='top'");
                                                   $displayNameValue = $name." (".$class_name."-".$class_number.")";
          $x .= $this->displayCell($i,$displayNameValue, "tablelink","valign='top'");
          $x .= $this->displayCell($i,$in_school_time, "tablelink","valign='top'");
          $x .= $this->displayCell($i,$in_school_station, "tablelink","valign='top'");
          if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
        	$body_temperature = '-';
			if(isset($bodyTemperatureRecords[$user_id])){
				$temperature_status = $bodyTemperatureRecords[$user_id][count($bodyTemperatureRecords[$user_id])-1]['TemperatureStatus'];
				$temperature_value = $bodyTemperatureRecords[$user_id][count($bodyTemperatureRecords[$user_id])-1]['TemperatureValue'];
				$body_temperature = '<span style="color:'.($temperature_status==1?'red':'green').'" title="'.($temperature_status==1?$Lang['StudentAttendance']['BodyTemperatureAbnormal']:$Lang['StudentAttendance']['BodyTemperatureNormal']).'">'.$temperature_value.' &#8451;</span>';
			}
        	$x .= $this->displayCell($i,$body_temperature, "tablelink ","valign='top'");
          }
          $x .= $this->displayCell($i,$note, "tablelink","valign='top'");
          $x .= $this->displayCell($i,$preset_abs_info, "tablelink","valign='top'");
          $x .= $this->displayCell($i,$select_status, "tablelink","valign='top'");
          if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	      {
	        	if ($LateSession == "" && $RequestLeaveSession == "" && $PlayTruantSession == "" && $OfficalLeaveSession == "") {
	        		if ($attend_status == CARD_STATUS_ABSENT) {
			  			$LateSession = $PlayTruantSession = $OfficalLeaveSession = 0;
			  			$RequestLeaveSession = 1;
			  			if(isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsenteesism']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsenteesism'] != -1){
			  				$PlayTruantSession = $DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsenteesism'];
			  			}
			  			if(isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForLeave']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForLeave'] != -1){
			  				$RequestLeaveSession = $DefaultNumOfSessionSettings['DefaultNumOfSessionForLeave'];
			  			}
			  			
			  		}
			  		else if ($attend_status==CARD_STATUS_LATE) {
			  			$LateSession = 1;
			  			$RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = 0;
			  			if(isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForLate']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForLate'] != -1){
			  				$LateSession = $DefaultNumOfSessionSettings['DefaultNumOfSessionForLate'];
			  			}
			  		}
			  		else {
			  			$LateSession = $RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = 0;
			  		}
	        	}
	        	
	        	$selOfficalLeaveSession = "<select name=\"offical_leave_session[]\" id=\"offical_leave_session$i\"/ ".$disable_attr.">";
						$selLateSession = "<select name=\"late_session[]\" id=\"late_session$i\"/ ".$disable_attr.">";
						$selRequestLeaveSession = "<select name=\"request_leave_session[]\" id=\"request_leave_session$i\"/ ".$disable_attr.">";
						$selPlayTruantSession = "<select name=\"play_truant_session[]\" id=\"play_truant_session$i\"/ ".$disable_attr.">";
						
						$k=0.0;
						while($k<=CARD_STUDENT_MAX_SESSION){
							$LateSelected = ($k == $LateSession)? 'selected':'';
							$OfficalLeaveSelected = ($k == $OfficalLeaveSession)? 'selected':'';
							$RequestLeaveSelected = ($k == $RequestLeaveSession)? 'selected':'';
							$PlayTruantSelected = ($k == $PlayTruantSession)? 'selected':'';
							
							$selOfficalLeaveSession .= "<option value=\"$k\" ".$OfficalLeaveSelected." >".$k."</option>";
							$selLateSession .= "<option value=\"".$k."\" ".$LateSelected." >".$k."</option>";
							$selRequestLeaveSession .= "<option value=\"".$k."\" ".$RequestLeaveSelected." >".$k."</option>";
							$selPlayTruantSession .= "<option value=\"".$k."\" ".$PlayTruantSelected." >".$k."</option>";
							$k+= $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] > 0 ? $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] : 0.5;
						}
						$selOfficalLeaveSession .= "</select>\n";
						$selLateSession .= "</select>\n";
						$selRequestLeaveSession .= "</select>\n";
						$selPlayTruantSession .= "</select>\n";
						
	        	$x .= $this->displayCell($i,$selLateSession, "tablelink ","valign='top'");
	        	$x .= $this->displayCell($i,$selRequestLeaveSession, "tablelink ","valign='top'");
	        	$x .= $this->displayCell($i,$selPlayTruantSession, "tablelink ","valign='top'");
	        	$x .= $this->displayCell($i,$selOfficalLeaveSession, "tablelink ","valign='top'");
	      }
          if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){
	  		if($AbsentSession == ""){
	  			$AbsentSession = 0;
	  			if ($attend_status == CARD_STATUS_ABSENT) {
		  			if(isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsent']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsent'] != -1){
		  				$AbsentSession = $DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsent'];
		  			}
		  		}
	  		}
	  		$selAbsentSession = "<select name=\"absent2_session[]\" id=\"absent2_session$i\"/ ".$disable_attr.">";
	  		
	  		$k=0.0;
			while($k<=CARD_STUDENT_MAX_SESSION){
				$AbsentSelected = ($k == $AbsentSession)? 'selected':'';
				$selAbsentSession .= "<option value=\"$k\" ".$AbsentSelected." >".$k."</option>";
				$k+= $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] > 0? $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] : 0.5;
			}
			$selAbsentSession .= "</select>\n";
			$x .= $this->displayCell($i,$selAbsentSession, "tablelink ","valign='top'");
	  	}
	  	if($StudentAttend->TeacherCanManageWaiveStatus){
          $x .= $this->displayCell($i,'<input type="checkbox" class="WaiveBtn" name="Waived'.$i.'" id="Waived'.$i.'" value="1" '.(($IsWaived == 1 || ($attend_status=="1" && $preset_waive == "1"))? 'checked':'').' '.$disable_attr.'>', "tablelink","valign='top'");
	  	}
	  	if($StudentAttend->TeacherCanManageProveDocumentStatus){
	  		$x .= $this->displayCell($i,'<span id="HandIn'.$i.'" '.($attend_status==CARD_STATUS_ABSENT?'':'style="display:none"').'><input type="checkbox" class="HandIn" name="HandIn_prove_'.$i.'" id="HandIn_prove_'.$i.'" value="1" '.($DocumentStatus == 1? 'checked':'').' /></span>', "tablelink","valign='top'");
	  	}	
        	$input_reason = ($IsEditDaysBeforeDisabled?"":$select_input_word)."<input type=text class=tabletext id=\"input_reason$i\" name=\"input_reason$i\" style=\"background:#ffffff\" value=\"".htmlspecialchars($admin_reason,ENT_QUOTES)."\" ".$disable_attr.">";
        	//$office_remark_input = "<input type=\"text\" class=\"tabletext\" id=\"OfficeRemark$i\" name=\"OfficeRemark[]\" style=\"background:#ffffff\" value=\"".htmlspecialchars($office_remark,ENT_QUOTES)."\">";
        	$office_remark_input = "<span id=\"OfficeRemark$i\">".htmlspecialchars($office_remark,ENT_QUOTES)."<input type=\"hidden\" name=\"OfficeRemark[]\" value=\"".htmlspecialchars($office_remark,ENT_QUOTES)."\"/></span>";
        if($StudentAttend->TeacherCanManageReason)
        {	
        	$x .= $this->displayCell($i,$input_reason, "tablelink","valign='top' align='right' nowrap");
        }
        if($StudentAttend->TeacherCanManageTeacherRemark){
          	$x .= $this->displayCell($i,$attend_reason, "tablelink","valign='top' align='right' nowrap");
        }
          $x .= $this->displayCell($i,$office_remark_input, "tablelink","valign='top' align='left' nowrap");
          $x .= $this->displayCell($i,$DateModified.' ('.$ModifyName.')', "tablelink ","valign='top'");
          $x .= $this->displayCell($i,$ConfirmedUser, "tablelink ","valign='top' align='right' nowrap");
        //}else{
        	//$x .= "<tr class=$css><td>$class_number</td><td>$name</td><td>$in_school_time</td><td>$in_school_station</td><td>$note</td>".($preset_count>0?"<td style='vertical-align:middle;' align=center>$preset_abs_info</td>":"")."<td>$select_status</td></tr>\n";
        //}
	      $x .= "</tr>\n";
	    }
		}

    $x .= "</table>\n";

    return $x;
  }
	
	        function displayFormat_eSportHouseSettings()
	        {
	                global $image_path, $LAYOUT_SKIN, $results, $lsports, $i_Sports_menu_Settings_Not_Setup_Yet;
	
	                    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>";
	                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	                    $x .= $this->displayColumn();
	
	                            if (sizeof($results)==0)
	                        {
	                                $x .= "<tr>
	                                        <td class='tablerow2 tabletext' align='center' colspan='3' ><br />".$this->no_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                            } else {
	                                for($i=0;$i<sizeof($results);$i++)
	                                {
	                                        $x .= "<tr class='tablerow". ( ($i)%2 + 1 )."'>\n";
	
	                                        list ($t_houseID, $t_groupID, $t_eng, $t_chi, $t_color, $t_houseCode, $t_groupName) = $results[$i];
	                                        $x .= $this->displayCell($i,$i+1, "tablelink","valign='top'");
	                                        $x .= $this->displayCell($i,"<a href=\"house_edit.php?GroupID=". $t_groupID."&new=".(!$t_houseID)."\" class=\"tablelink\">".$t_groupName."</a>", "tablelink","valign='top'");
	                                        if(!$t_houseID)
												$t = $i_Sports_menu_Settings_Not_Setup_Yet;
											else
												$t = $t_eng . " / " . $t_chi;
	                                        $x .= $this->displayCell($i,$lsports->house_flag2($t_color, $t), "tablelink","valign='top'");
	                                        $x .= $this->displayCell($i,"<input type=checkbox name='GroupID[]' value='". $t_groupID."'>", "tablelink","valign='top'");
	                                        $x .= "</tr>\n";
	                                 }
	                        }
	
	                        $x .= "</table></td></tr>";
	                        $x .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
	                        $x .= "</table>\n";
	
	                        return $x;
	        }
	
	        function displayFormat_eSportSettings($no_col, $displayNo)
	        {
	                global $image_path, $LAYOUT_SKIN;
	
	                $this->rs = $this->db_db_query($this->built_sql());
	
					$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>";
					$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
					
					$x .= $this->displayColumn();
					if ($this->db_num_rows()==0)
					{
						$x .= "<tr>
						<td class='tablerow2 tabletext' align='center' colspan='". $no_col ."' ><br />".$this->no_msg."<br /><br></td>
						</tr>\n
						";
					} else {
						$i=0;
						
						while($row = $this->db_fetch_array())
						{
							$x .= "<tr class='tablerow". ( ($i)%2 + 1 )."'>\n";
							
							if($displayNo)
								$x .= $this->displayCell($i,$i+1, "tablelink","valign='top'");
							
							for($j=1; $j<$this->no_col; $j++)
							{
								if (trim($row[$j]) == "")
								{
									$row[$j] = "&nbsp;";
								}
								$x .= $this->displayCell($j,nl2br($row[$j]), "tablelink","valign='top'");
							}
							
							$x .= "</tr>\n";
							$i++;
						}
					}
					$x .= "</table></td></tr>";
					$x .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
					$x .= "</table>\n";
					
					return $x;
	        }
	
	        function displayFormat_eSportLaneSetSettings()
	        {
	                global $image_path, $LAYOUT_SKIN, $lanes, $lane_rank;
	                $tablecolor = "green";
	
	                $i = 0;
	
	                    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	                    $x .= $this->displayColumn();
	
	                    if ($lanes==0)
	                {
	                        $x .= "<tr>
	                                <td class='table".$tablecolor."row2 tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
	                                </tr>\n
	                                ";
	                    } else {
	
	                        for($i=1;$i<=$lanes;$i++)
	                        {
	                                $x .= "<tr class='table".$tablecolor."row". ( $i%2 + 1 )."'>\n";
	
	                                $x .= "<td class='tabletext' valign='top'>". $i ."</td>\n";
	
	                                $select_rank = "<SELECT name=lane_rank[]>\n";
	                                for ($j=1; $j<=$lanes; $j++)
	                                        $select_rank .= "<OPTION value=$j ".($lane_rank[$i-1]==$j?"SELECTED":"").">$j</OPTION>\n";
	                                $select_rank .= "</SELECT>\n";
	
	                                $x .= $this->displayCell($j,$select_rank, "tablelink","valign='top'");
	
	                                $x .= "</tr>\n";
	                                }
	                            }
	
	                        $x .= "</table>\n";
	
	                        return $x;
	        }
	
	        function displayFormat_eSportParticipant()
	        {
	                global $image_path, $LAYOUT_SKIN, $results, $i_Sports_Participant_Number_Summary_NoClass;
	                $tablecolor = "blue";
	
	                    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	                    $x .= $this->displayColumn();
	
	                                if (sizeof($results)==0)
	                        {
	                                $x .= "<tr>
	                                        <td class='table{$tablecolor}row2 tabletext' align='center' colspan='8' ><br />".$this->no_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                            } else {
	
	                                    $i = 0;
	                                foreach($results as $class_name => $entry)
	                                {
	                                        list($num_students, $no_classnum, $no_house, $mul_house, $no_gender, $no_dob, $no_athlnum) = $entry;
	
	                                                                                $x .= "<tr class='table{$tablecolor}row". ( ($i)%2 + 1 )."'>\n";
	                                        $i++;
	                                        if ($class_name != EMPTY_ARRAY_NAME)
	                                                                                        $x .= $this->displayCell($i,$class_name, "table{$tablecolor}link","valign='top'");
	                                        else
	                                                                                        $x .= $this->displayCell($i,$i_Sports_Participant_Number_Summary_NoClass, "table{$tablecolor}link","valign='top'");
	
	                                        $x .= $this->displayCell($i,$num_students, "table{$tablecolor}link","valign='top'");
	                                        $x .= $this->displayCell($i,$no_classnum, "table{$tablecolor}link","valign='top'");
	                                        $x .= $this->displayCell($i,$no_house, "table{$tablecolor}link","valign='top'");
	                                        $x .= $this->displayCell($i,$mul_house, "table{$tablecolor}link","valign='top'");
	                                        $x .= $this->displayCell($i,$no_gender, "table{$tablecolor}link","valign='top'");
	                                        $x .= $this->displayCell($i,$no_dob, "table{$tablecolor}link","valign='top'");
	                                        $x .= $this->displayCell($i,$no_athlnum, "table{$tablecolor}link","valign='top'");
	
	                                        $x .= "</tr>\n";
	                                }
	                        }
	
	                        $x .= "</table>\n";
	
	                        return $x;
	        }
	
	        function displayFormat_eSportEnrolUpdateList()
	        {
	                global $image_path, $LAYOUT_SKIN, $results, $i_Sports_Detail, $i_no_record_searched_msg, $i_Sports_total, $lsports;
	                $tablecolor = "blue";
	
	                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>";
	                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	                if (sizeof($results)!=0)
	                $x .= "<tr><td class='tabletext' align='left' colspan='7' >".$i_Sports_total." : ". sizeof($results)."</td></tr>\n";
	                    $x .= $this->displayColumn();
	
	                    	if (sizeof($results)==0)
	                        {
	                                $x .= "<tr>
	                                        <td class='table{$tablecolor}row2 tabletext' align='center' colspan='7' ><br />".$i_no_record_searched_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                        }
	                        else {
	                                for($i=0; $i<sizeof($results); $i++)
	                                {
	                                        $userid         = $results[$i][0];
	                                        $ename          = $results[$i][1];
	                                        $cname          = $results[$i][2];
	                                        $class_name     = $results[$i][3];
	                                        $athletic_num   = $results[$i][6];
	                                        $age_group      = $results[$i][7];
	                                        $house_name     = $results[$i][8];
	                                        $house_color    = $results[$i][9];
	                                        $age_group_id	= $results[$i][10];
											
	                                        $house_tag = $lsports->house_flag2($house_color, $house_name);
											
	                                        $x .= "<tr class='table{$tablecolor}row". ( ($i)%2 + 1 )."'>\n";
	                                        $x .= $this->displayCell($i, $ename, "table{$tablecolor}link", "valign='top'");
	                                        $x .= $this->displayCell($i, $cname, "table{$tablecolor}link", "valign='top'");
	                                        $x .= $this->displayCell($i, $class_name, "table{$tablecolor}link", "valign='top'");
	                                        $x .= $this->displayCell($i, $age_group, "table{$tablecolor}link", "valign='top'");
	                                        $x .= $this->displayCell($i, $house_tag, "table{$tablecolor}link", "valign='top'");
	                                        $x .= $this->displayCell($i, $athletic_num, "table{$tablecolor}link", "valign='top'");
	                                        if($age_group != "N/A")
	                                        	$x .= $this->displayCell($i, "<a class=\"table{$tablecolor}link\" href=\"javascript:newWindow('detail.php?StudentID=$userid&AgeGroup=$age_group_id', 16)\"><img src='{$image_path}/{$LAYOUT_SKIN}/icon_view.gif' border='0'></a>", "table{$tablecolor}link", "valign='top'");
	                                        else
	                                        	$x .= $this->displayCell($i, "&nbsp;", "table{$tablecolor}link", "valign='top'");
	                                        $x .= "</tr>\n";
	                                }
	                        }
	                        $x .= "</table></td></tr>";
	                        $x .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
	                        $x .= "</table>\n";
							
	                        return $x;
	        }
	
	        function displayFormat_eSportExportArrangement()
	        {
	                global $image_path, $LAYOUT_SKIN, $TFEvents, $HREvents, $CREvents, $i_no_record_searched_msg, $lsports, $groupArr, $openGroupList;
	                $tablecolor = "blue";
	
	                    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	
	                    $x .= $this->displayColumn();
	
	                            if (sizeof($TFEvents)==0 and sizeof($HREvents)==0 and sizeof($CREvents)==0)
	                        {
	                                $x .= "<tr>
	                                        <td class='table{$tablecolor}row2 tabletext' align='center' colspan='2' ><br />".$i_no_record_searched_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                            } else {
	
	                                $tr = 0;
	                                ##################### Track and Field Event#############
	                                $typeIDs = array(1,2);
	                                for($i=0; $i<sizeof($TFEvents); $i++)
	                                {
	                                        list($eventID, $groupID, $eventName) = $TFEvents[$i];
	
	                                        $eventGroupInfo = $lsports->retrieveEventGroupID($eventID, $groupID, $typeIDs);
	                                        $egid = $eventGroupInfo[0];
	                                        $eventType = $eventGroupInfo[1];
	
	                                        # Create a array to save the open group events ID
	                                        if($groupID < 0)
	                                        $openGroupArr[] = $egid;
	
	                                        $ext_flag = $lsports->checkExtInfoExist($eventType, $egid);
	
	                                        if($ext_flag==1)
	                                        {
	                                                # Create a array to save the open group events ID
	                                                if($groupID < 0)
	                                                        $openGroupArr[] = $egid;
	
	                                                if($egid != "")
	                                                {
	                                                        $x .= "<tr class='table{$tablecolor}row". ( ($tr)%2 + 1 )."'>\n";
	                                                        $x .= $this->displayCell($i,"<input type='checkbox' name='EventGroupID[]' value='".$egid."'>", "table{$tablecolor}link","valign='top' align='center'");
	                                                        $x .= $this->displayCell($i,$groupArr[$groupID]." ".$eventName, "table{$tablecolor}link","valign='top'");
	                                                        $x .= "</tr>\n";
	                                                        $tr++;
	                                                }
	                                        }
	                                }
	
	                                ##################### House Relay Event#############
	                                $typeIDs = array(3);
	                                for($i=0; $i<sizeof($HREvents); $i++)
	                                {
	                                        list($eventID, $groupID, $eventName) = $HREvents[$i];
	
	                                        if(true || $groupID > 0)
	                                        {
	                                                $eventGroupInfo = $lsports->retrieveEventGroupID($eventID, $groupID, $typeIDs);
	                                                $egid = $eventGroupInfo[0];
	
	                                                               # Create a array to save the open group events ID
	                                          if($groupID < 0)
	                                          $openGroupArr[] = $egid;
	
	                                                $ext_flag = $lsports->checkExtInfoExist(3, $egid);
	
	                                                if($egid != "" && $ext_flag==1)
	                                                {
	                                                        $x .= "<tr class='table{$tablecolor}row". ( ($tr)%2 + 1 )."'>\n";
	                                                        $x .= $this->displayCell($i,"<input type='checkbox' name='EventGroupID[]' value='".$egid."'>", "table{$tablecolor}link","valign='top' align='center'");
	                                                        $x .= $this->displayCell($i,$groupArr[$groupID]." ".$eventName, "table{$tablecolor}link","valign='top'");
	                                                        $x .= "</tr>\n";
	                                                        $tr++;
	                                                }
	                                        }
	                                }
	                                
	                                ##################### Class Relay Event#############
	                                $typeIDs = array(4);
	                                for($i=0; $i<sizeof($CREvents); $i++)
	                                {
	                                        list($eventID, $groupID, $eventName) = $CREvents[$i];
	
	                                        $eventGroupInfo = $lsports->retrieveEventGroupID($eventID, $groupID, $typeIDs);
	                                        $egid = $eventGroupInfo[0];
	
	                                       # Create a array to save the open group events ID
	                                      if($groupID < 0)
	                                      	$openGroupArr[] = $egid;
	
	                                        $ext_flag = $lsports->checkExtInfoExist(4, $egid);
	
	                                        if($egid != "" && $ext_flag==1)
	                                        {
	                                                $x .= "<tr class='table{$tablecolor}row". ( ($tr)%2 + 1 )."'>\n";
	                                                $x .= $this->displayCell($i,"<input type='checkbox' name='EventGroupID[]' value='".$egid."'>", "table{$tablecolor}link","valign='top' align='center'");
	                                                $x .= $this->displayCell($i,$groupArr[$groupID]." ".$eventName, "table{$tablecolor}link","valign='top'");
	                                                $x .= "</tr>\n";
	                                                $tr++;
	                                        }
	                                }
	                                $openGroupList = (is_array($openGroupArr)) ? implode(",", $openGroupArr) : $openGroupArr;
	
	                        }
	
	                        $x .= "</table>\n";
	
	                        return $x;
	        }
	
	        function displayFormat_eSportExportResult()
	        {
	                global $image_path, $LAYOUT_SKIN, $TFEvents, $HREvents, $CREvents, $raceDay, $i_no_record_searched_msg, $lsports, $groupArr, $openGroupList, $i_Sports_First_Round, $i_Sports_Second_Round, $i_Sports_Final_Round;
	                $tablecolor = "blue";
	
	                    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	
	                    $x .= $this->displayColumn();
	
	                            if (sizeof($TFEvents)==0 and sizeof($HREvents)==0 and sizeof($CREvents)==0)
	                        {
	                                $x .= "<tr>
	                                        <td class='table{$tablecolor}row2 tabletext' align='center' colspan='2' ><br />".$i_no_record_searched_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                            } else {
	
	                                $tr = 0;
	                                ##################### Track and Field Event#############
	                                $typeIDs = array(1,2);
	                                for($i=0; $i<sizeof($TFEvents); $i++)
	                                {
	                                        list($eventID, $groupID, $eventName) = $TFEvents[$i];
	
	                                        $eventGroupInfo = $lsports->retrieveEventGroupID($eventID, $groupID, $typeIDs);
	                                        $egid = $eventGroupInfo[0];
	                                        $eventType = $eventGroupInfo[1];
	
	                                        # Create a array to save the open group events ID
	                                        if($groupID < 0)
	                                                $openGroupArr[] = $egid;
	
	                                        $ext_flag = $lsports->checkExtInfoExist($eventType, $egid);
	
	                                        if($egid != "" && $ext_flag==1)
	                                        {
	                                                $ext = $lsports->retrieveEventGroupExtInfo($egid, $eventType);
	
	                                                if($ext["SecondRoundReq"]==1 || $ext["FinalRoundReq"]==1)
	                                                {
	                                                		if($ext["FirstRoundDay"]==$raceDay || $raceDay<0)
	                                                		{
		                                                        $x .= "<tr class='table{$tablecolor}row". ( ($tr)%2 + 1 )."'>\n";
		                                                        $x .= $this->displayCell($i,"<input type='checkbox' name='EventGroupType[]' value='".$egid."_1'>", "table{$tablecolor}link","valign='top' align='center'");
		                                                        $x .= $this->displayCell($i,$groupArr[$groupID]." ".$eventName." [".$i_Sports_First_Round."]", "table{$tablecolor}link","valign='top'");
		                                                        $x .= "</tr>\n";
		                                                        $tr++;
	                                                		}
	
	                                                        if($ext["SecondRoundReq"]==1 )
	                                                        {
	                                                        	if($ext["SecondRoundDay"]==$raceDay || $raceDay<0)
	                                                			{
	                                                                $x .= "<tr class='table{$tablecolor}row". ( ($tr)%2 + 1 )."'>\n";
	                                                                $x .= $this->displayCell($i,"<input type='checkbox' name='EventGroupType[]' value='".$egid."_2'>", "table{$tablecolor}link","valign='top' align='center'");
	                                                                $x .= $this->displayCell($i,$groupArr[$groupID]." ".$eventName." [".$i_Sports_Second_Round."]", "table{$tablecolor}link","valign='top'");
	                                                                $x .= "</tr>\n";
	                                                                $tr++;
	                                                			}
	
	                                                            if($ext["FinalRoundDay"]==$raceDay || $raceDay<0)
	                                                			{
	                                                				$x .= "<tr class='table{$tablecolor}row". ( ($tr)%2 + 1 )."'>\n";
	                                                                $x .= $this->displayCell($i,"<input type='checkbox' name='EventGroupType[]' value='".$egid."_0'>", "table{$tablecolor}link","valign='top' align='center'");
	                                                                $x .= $this->displayCell($i,$groupArr[$groupID]." ".$eventName." [".$i_Sports_Final_Round."]", "table{$tablecolor}link","valign='top'");
	                                                                $x .= "</tr>\n";
	                                                                $tr++;
	                                                			}
	
	                                                        }
	                                                        else
	                                                        {
	                                                         	if($ext["FinalRoundDay"]==$raceDay || $raceDay<0)
	                                                			{
	                                        				        $x .= "<tr class='table{$tablecolor}row". ( ($tr)%2 + 1 )."'>\n";
	                                                                $x .= $this->displayCell($i,"<input type='checkbox' name='EventGroupType[]' value='".$egid."_0'>", "table{$tablecolor}link","valign='top' align='center'");
	                                                                $x .= $this->displayCell($i,$groupArr[$groupID]." ".$eventName." [".$i_Sports_Final_Round."]", "table{$tablecolor}link","valign='top'");
	                                                                $x .= "</tr>\n";
	                                                                $tr++;
	                                                			}
	                                                        }
	                                                }
	                                                else
	                                                {
	                                                	if($ext["FirstRoundDay"]==$raceDay || $raceDay<0)
	                                            		{
	                                                        $x .= "<tr class='table{$tablecolor}row". ( ($tr)%2 + 1 )."'>\n";
	                                                        $x .= $this->displayCell($i,"<input type='checkbox' name='EventGroupType[]' value='".$egid."_1'>", "table{$tablecolor}link","valign='top' align='center'");
	                                                        $x .= $this->displayCell($i,$groupArr[$groupID]." ".$eventName, "table{$tablecolor}link","valign='top'");
	                                                        $x .= "</tr>\n";
	                                                        $tr++;
	                                            		}
	                                                }
	                                        }
	                                }
	
	                                $openGroupList = (is_array($openGroupArr)) ? implode(",", $openGroupArr) : $openGroupArr;
	
	                                ##################### House Relay Event#############
	                                $typeIDs = array(3);
	                                for($i=0; $i<sizeof($HREvents); $i++)
	                                {
	                                        list($eventID, $groupID, $eventName) = $HREvents[$i];
	
	                                        if(true || $groupID > 0)
	                                        {
	                                                $eventGroupInfo = $lsports->retrieveEventGroupID($eventID, $groupID, $typeIDs);
	                                                $egid = $eventGroupInfo[0];
	
	                                                $ext_flag = $lsports->checkExtInfoExist(3, $egid);
	
	                                                if($egid != "" && $ext_flag==1)
	                                                {
	                                                        $x .= "<tr class='table{$tablecolor}row". ( ($tr)%2 + 1 )."'>\n";
	                                                        $x .= $this->displayCell($i,"<input type='checkbox' name='EventGroupType[]' value='".$egid."_1'>", "table{$tablecolor}link","valign='top' align='center'");
	                                                        $x .= $this->displayCell($i,$groupArr[$groupID]." ".$eventName, "table{$tablecolor}link","valign='top'");
	                                                        $x .= "</tr>\n";
	                                                        $tr++;
	                                                }
	                                        }
	                                }
	                                
	                                ##################### Class Relay Event#############
	                                $typeIDs = array(4);
	                                for($i=0; $i<sizeof($CREvents); $i++)
	                                {
	                                        list($eventID, $groupID, $eventName) = $CREvents[$i];
	
	                                        if(true || $groupID > 0)
	                                        {
	                                                $eventGroupInfo = $lsports->retrieveEventGroupID($eventID, $groupID, $typeIDs);
	                                                $egid = $eventGroupInfo[0];
	
	                                                $ext_flag = $lsports->checkExtInfoExist(4, $egid);
	
	                                                if($egid != "" && $ext_flag==1)
	                                                {
	                                                        $x .= "<tr class='table{$tablecolor}row". ( ($tr)%2 + 1 )."'>\n";
	                                                        $x .= $this->displayCell($i,"<input type='checkbox' name='EventGroupType[]' value='".$egid."_1'>", "table{$tablecolor}link","valign='top' align='center'");
	                                                        $x .= $this->displayCell($i,$groupArr[$groupID]." ".$eventName, "table{$tablecolor}link","valign='top'");
	                                                        $x .= "</tr>\n";
	                                                        $tr++;
	                                                }
	                                        }
	                                }
	
	
	                        }
	
	                        $x .= "</table>\n";
	
	                        return $x;
	        }
	
	        function displayFormat_eNoticedisplayMyNotice()
	        {
	        		global $PATH_WRT_ROOT, $sys_custom;
					include_once($PATH_WRT_ROOT."includes/libnotice.php");

	                global $lnotice, $image_path, $LAYOUT_SKIN, $fullRight, $hasIssueRight, $CurrentPageArr;
	
	                ## wordings
	                global $i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual;
	                global $i_Notice_StatusPublished, $i_Notice_StatusSuspended, $i_Notice_StatusTemplate;
	
					### Get All Notice Info first
	                $NoticeArr = $lnotice->returnArray($this->built_sql());
	                $NoticeIDArr = Get_Array_By_Key($NoticeArr, 'NoticeID');
	                $lnotice = new libnotice('', $NoticeIDArr);
	                
	                $this->rs = $this->db_db_query($this->built_sql());
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	
	                if($CurrentPageArr['eServiceNotice'])	$view_table_list = "view_table_list_v30";
	                        	
	                $x .= "<table class='common_table_list $view_table_list'>\n<thead>\n";
	                $x .= $this->displayColumn();
	
	                if ($this->db_num_rows() == 0)
	                {
	                        $x .= " <tr>
                                        <td class='tableContent' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
	                                </tr>\n
	                                ";
	                }
	                else
	                {
	                        $targetType = array("", $i_Notice_RecipientTypeAllStudents,
                                                    $i_Notice_RecipientTypeLevel,
                                                    $i_Notice_RecipientTypeClass,
                                                    $i_Notice_RecipientTypeIndividual);
	                        $noticeStatus = array("", $i_Notice_StatusPublished,
                                                      $i_Notice_StatusSuspended,
                                                      $i_Notice_StatusTemplate);
	                        $totalCounts = $lnotice->returnNoticeTotalCount();
	                        $signedCounts = $lnotice->returnNoticeSignedCount();
	                         
	                        $i = 1;
	                        while($row = $this->db_fetch_array())
	                        {
                                    $i++;
                                    if($i > $this->page_size) {
                                        break;
                                    }

	                                list ($noticeID,$issueDate,$duedate,$noticeNumber,$title,$status,$type) = $row;
	                                
	                                //$lnotice = new libnotice($noticeID);
	                                $lnotice->LoadNoticeData($noticeID);
                                    $hideSpecialNotice = false;
                                    if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice)
                                    {
                                        $title = "<span style='color:red'>* </span>" .$title;
                                        if(($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->isNoticePIC($noticeID))) {
                                            // Allowed
                                        } else {
                                            // Hide buttons
                                            $hideSpecialNotice = true;
                                        }
                                    }

	                              	## edit icon
	                              	if ($lnotice->isNoticeEditable($noticeID) && $CurrentPageArr['eAdminNotice'])
	                              	{
	                                    $editIcon = $lnotice->returnEditLink($noticeID, $TargetType);
	                                  
	                                    if(strtoupper($lnotice->Module) == "PAYMENT")
	                                    {
	                                  	    $title_link = "<a href='javascript:editPaymentNotice($noticeID)' class='tablelink'>$title</a>";
	                                    }
	                                    else
	                                    {
	                                  	    $title_link = "<a href='javascript:editNotice($noticeID)' class='tablelink'>$title</a>";
	                                    }
	                                }
	                              	else
	                              	{
	                              		$editIcon = "";
	                                	$title_link = $title;
	                                	
	                                	// [2016-0907-1010-32206] show link
                                        if(!$CurrentPageArr['eAdminNotice'] && $sys_custom['eNotice']['eServiceShowHyperlink'])
                                        {
                                        	if(strtoupper($lnotice->Module) == "PAYMENT") {
                                        		$title_link = "<a href='javascript:viewPaymentNotice($noticeID)' class='tablelink'>$title</a>";
                                            } else {
                                        		$title_link = "<a href='javascript:viewNotice($noticeID)' class='tablelink'>$title</a>";
                                            }
                                        }
	                                }
	                            	
	                                ## view icon
	                                if(strtoupper($lnotice->Module) == "PAYMENT")
	                                {
	                                	$viewIcon = "<a href='javascript:viewPaymentNotice($noticeID)'><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_edit\" border=0></a>";
	                                }
	                                else
	                                {
	                                	$viewIcon = "<a href='javascript:viewNotice($noticeID)'><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_edit\" border=0></a>";
	                                }

                                    if($hideSpecialNotice)
                                    {
                                        $title_link = $title;
                                        $viewIcon = '';
                                        $editIcon = '';
                                    }
	
	                                $j = 1;
	                              
	                                $trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
	                                $x .= "<tr ".$trClass.">\n";
	                                $x .= $this->displayCell($j++,$issueDate,"tabletext","valign='top'");
	                                $x .= $this->displayCell($j++,$duedate,"tabletext","valign='top'");
	                                $x .= $this->displayCell($j++,$noticeNumber,"tabletext","valign='top'");
	                                $x .= $this->displayCell($j++,$viewIcon." ".$editIcon,"tabletext","valign='top'");
	                                $x .= $this->displayCell($j++,$title_link,"tabletext","valign='top'");
	                                $x .= $this->displayCell($j++,$noticeStatus[$status],"tabletext","valign='top'");
	                                if ($status == 1)
	                                {
	                                    $numSigned = $signedCounts[$noticeID]+0;
	                                    $numTotal = $totalCounts[$noticeID]+0;
	                                    $result_link = "<a href='javascript:viewResult($noticeID)' class='tablelink'><img src=\"$image_path/$LAYOUT_SKIN/eOffice/icon_resultview.gif\" border='0' align='absmiddle'>";
                                        if($hideSpecialNotice) {
                                            $result_link = '';
                                        }
	                                    $x .= $this->displayCell($j++,$result_link." ".$numSigned."/".$numTotal, "tabletext","valign='top'");
	                                }
	                                else
	                                {
	                                    $x .= $this->displayCell($j++,"--","tabletext","valign='top'");
	                                }
	
	                                $x .= $this->displayCell($j++,$targetType[$type],"tabletext","valign='top'");

	                                if(($fullRight || $hasIssueRight) && $CurrentPageArr['eAdminNotice']) {
	                                    $x .= $this->displayCell($i,($hideSpecialNotice ? "&nbsp;" : "<input type='checkbox' name='NoticeIDArr[]' value='". $noticeID."'>"), "tablelink","valign='top'");
                                    }

	                                $x .= "</tr>\n";
                            }
	                }
	
	                $x .= "</thead></table>";
	                
	                if($this->db_num_rows() <> 0) {
	                	$x .= $this->navigation_IP25();
                    }

	                $this->db_free_result();
	                return $x;
	        }
	        
	        # 20090227 yatwoon
	        function displayFormat_eNoticedisplayNoticePrint($getMessageDetails=false)
	        {
		        	global $plugin, $image_path;
	                
	                // [2016-0801-1134-09054] Get message send out time
	                if($plugin['eClassApp'] && $getMessageDetails)
	                {
	                	global $PATH_WRT_ROOT, $Lang;
	                	global $sys_custom;
	                	
						include_once($PATH_WRT_ROOT."includes/libdb.php");
						$libdb = new libdb();
						
						// Get NoticeID
		                $NoticeIDArr = $libdb->returnArray($this->built_sql());
		                $NoticeIDArr = Get_Array_By_Key((array)$NoticeIDArr, 'NoticeID');
		                
		                // Get Notify Message
						$MessageDateSql = "SELECT RelationID, ModuleRecordID, DateInput FROM INTRANET_APP_NOTIFY_MESSAGE_MODULE_RELATION WHERE ModuleName = 'eNotice' AND ModuleRecordID IN ('".implode("','", (array)$NoticeIDArr)."') ORDER BY DateInput DESC";
						$MessageDate = $libdb->returnArray($MessageDateSql);
						$MessageDate = BuildMultiKeyAssoc((array)$MessageDate, array("ModuleRecordID", "RelationID"));

                        // [2020-1009-1753-46096] scheduled mode > Get notify date time from INTRANET_APP_NOTIFY_MESSAGE
						if (isset($sys_custom['eDiscipline']['PushMsgReminder_ScheduledDelay']) && $sys_custom['eDiscipline']['PushMsgReminder_ScheduledDelay'] > 0)
						{
                            $MessageDateSql = "SELECT 
                                                        app_mr.RelationID, app_mr.ModuleRecordID, app.NotifyDateTime as DateInput
                                                FROM 
                                                        INTRANET_APP_NOTIFY_MESSAGE_MODULE_RELATION app_mr 
                                                        INNER JOIN INTRANET_APP_NOTIFY_MESSAGE app ON (app_mr.NotifyMessageID = app.NotifyMessageID AND app.ShowInApp = 1)
                                                WHERE 
                                                        app_mr.ModuleName = 'eNotice' AND 
                                                        app_mr.ModuleRecordID IN ('".implode("','", (array)$NoticeIDArr)."') 
                                                ORDER BY 
                                                        app_mr.DateInput DESC";
                            $MessageDate = $libdb->returnArray($MessageDateSql);
                            $MessageDate = BuildMultiKeyAssoc((array)$MessageDate, array("ModuleRecordID", "RelationID"));
                        }
	                }
	
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $width = ($width!="") ? $width : "100%";
	
	                $n_start = $this->n_start;
	                
	                if ($this->db_num_rows()==0)
	                {
	                        //$x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                        $x .= "<div class='no_record_find'>". $this->no_msg ."</div>";
	                } else
	                {
		                //$x .= "<table class=\"common_table_list view_table_list_v30\">\n";
		                $x .= "<table class=\"common_table_list\">\n";
	                	$x .= $this->displayColumn();
	                
	                        while ($row = $this->db_fetch_array())
	                        {
	                                $i++;
	                                $css = ($i%2) ? "1" : "2";
	                                if ($i>$this->page_size)
	                                {
	                                        break;
	                                }
	                                $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                                for ($j=0; $j<$this->no_col-1; $j++)
	                                {
	                                	// [2016-0801-1134-09054] Display message send out time
	                                	if($plugin['eClassApp'] && $getMessageDetails && $j==5)
	                                	{
	                                		$thisNoticeMessage = $MessageDate[$row[$j]];
	                                		if(!empty($thisNoticeMessage) && count((array)$thisNoticeMessage) > 0){
	                                			$thisNoticeMessage = current($thisNoticeMessage);
	                                        	$x .= $this->displayCell($j, $thisNoticeMessage["DateInput"]);
	                                		}
	                                		else{
	                                			$x .= $this->displayCell($j, $Lang['eNotice']['NonSendMsg']);
	                                		}
	                                		break;
	                                	}
	                                	else
	                                	{
	                                        $x .= $this->displayCell($j, $row[$j]);
	                                	}
	                                }
	                                $x .= $this->displayCell($i,"<input type='checkbox' name='NoticeIDArr[]' value='". $row[$j]."'>", "tablelink","valign='top'");
	                                $x .= "</tr>\n";
	                        }
	                        $x .= "</table>\n";
	                }
	                if($this->db_num_rows()<>0) $x .= $this->navigation_IP25("","view_table_bottom");
	                
	                $this->db_free_result();
	
	                return $x;
	        }
	
	        function displayFormat_displayTeacherView($TargetType="P") 
	        {
	        		global $PATH_WRT_ROOT, $sys_custom;
					include_once($PATH_WRT_ROOT."includes/libnotice.php");

	                global $lnotice, $image_path, $LAYOUT_SKIN, $UserID, $status, $fullRight, $hasIssueRight, $isPIC, $CurrentPageArr;
	                
	                ## wordings
	                global $i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual;
					
					### Get All Notice Info first
	                $NoticeArr = $lnotice->returnArray($this->built_sql());
	                $NoticeIDArr = Get_Array_By_Key($NoticeArr, 'NoticeID');
	                $lnotice = new libnotice('', $NoticeIDArr);
	                $ClassListAssoArr = $lnotice->returnClassList($NoticeIDArr);
					// [2015-0428-1214-11207] get eNotice PIC associative array first, reduce sql number
                	$NoticePICArr = $lnotice->returnNoticePICMapping($NoticeIDArr);

                	### [2020-0604-1821-16170] Get Access Right from notice settings
                	$schoolNoticeFullRight = $lnotice->hasSchoolNoticeFullRight();
                    $paymentNoticeFullRight = $lnotice->hasPaymentNoticeFullRight();
                    $schoolNoticeViewRight = $lnotice->hasSchoolNoticeNormalRight();
                    $paymentNoticeViewRight = $lnotice->hasPaymentNoticeNormalRight();
                    $schoolNoticeIssueRight = $lnotice->hasSchoolNoticeIssueRight();
                    $paymentNoticeIssueRight = $lnotice->hasPaymentNoticeIssueRight();
                    $schoolNoticeApprovalRight = $lnotice->isSchoolNoticeApprovalUser();
                    $paymentNoticeApprovalRight = $lnotice->isPaymentNoticeApprovalUser();
                    $fullRight = $schoolNoticeFullRight || $paymentNoticeFullRight;
                    $viewRight = $schoolNoticeViewRight || $paymentNoticeViewRight;
                    $hasIssueRight = $schoolNoticeIssueRight || $paymentNoticeIssueRight;

	                $this->rs = $this->db_db_query($this->built_sql());
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	
                	// [2015-0323-1602-46073] - add $isPIC to checking
	                if(($fullRight || $hasIssueRight || $isPIC) && $CurrentPageArr['eAdminNotice'])
	                {
		                $view_table = "";
	                }
	                else
	                {
		                $view_table = "view_table_list_v30";
	                }
	                
	                //$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	                $x .= "<table class='common_table_list $view_table'>\n<thead>\n";
					$x .= $this->displayColumn();
					//2016-10-05	Villa #S105745
					$x .= "</thead>";
					
	                if ($this->db_num_rows() == 0)
	                {
	                        // [2015-0323-1602-46073] add $isPIC to checking
							if(($fullRight || $hasIssueRight || $isPIC) && $CurrentPageArr['eAdminNotice'])
							{
								$x .= "	<tr>
											<td class='tableContent' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
										</tr>\n ";
							}
							else
							{
								$x .= "	<tr>
											<td class='tablebluerow2 tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
										</tr>\n ";
							}
	                }
	                else
	                {
	                        global $class, $isClassTeacher, $viewRight, $fullRight, $hasIssueRight;
	
	                        $targetType = array("",
                                                $i_Notice_RecipientTypeAllStudents,
                                                $i_Notice_RecipientTypeLevel,
                                                $i_Notice_RecipientTypeClass,
                                                $i_Notice_RecipientTypeIndividual);

	                        // [2020-0604-1821-16170] added checking of different payment notice settings
                         	// [2015-0323-1602-46073] return eNotice count if user is also an eNotice PIC
	                        //if (($viewRight || $lnotice->staffview) || (($fullRight || $hasIssueRight) && $CurrentPageArr['eAdminNotice']) || $isPIC)
                            //if ($fullRight || $viewRight || $hasIssueRight || $isPIC || $lnotice->staffview)
	                        if ($fullRight || $viewRight || $hasIssueRight || $isPIC || $lnotice->staffview || $lnotice->payment_staffview)
	                        {
	                            $totalCounts = $lnotice->returnNoticeTotalCount();
	                            $signedCounts = $lnotice->returnNoticeSignedCount();
	                        }

	                        $now = mktime(0,0,0,date('m'),date('d'),date('Y'));

	                        $i = 0;
	                        while($row = $this->db_fetch_array())
	                        {
	                              list ($noticeID,$issueDate,$duedate,$noticeNumber,$title,$issuer,$type,$module,$approver,$approveTime,$approvalComment) = $row;

                                  // [2020-0604-1821-16170] Check access right related to this notice
                                  $is_payment_notice    = $module == "Payment";
                                  $noticeFullRight      = $is_payment_notice ? $paymentNoticeFullRight : $schoolNoticeFullRight;
                                  $noticeViewRight      = $is_payment_notice ? $paymentNoticeViewRight : $schoolNoticeViewRight;
                                  $noticeIssueRight     = $is_payment_notice ? $paymentNoticeIssueRight : $schoolNoticeIssueRight;
                                  $noticeStaffView      = $is_payment_notice ? $lnotice->payment_staffview : $lnotice->staffview;
                                  $noticeNeedApproval   = $is_payment_notice ? $lnotice->payment_needApproval : $lnotice->needApproval;
                                  $noticeIsApprovalUser = $is_payment_notice ? $paymentNoticeApprovalRight : $schoolNoticeApprovalRight;

	                              //$lnotice = new libnotice($noticeID);
	                              $lnotice->LoadNoticeData($noticeID);
	                              $hideSpecialNotice = false;
	                              if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice)
	                              {
	                                  $title = "<span style='color:red'>* </span>" .$title;
	                                  if(($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->isNoticePIC($noticeID))) {
	                                      // Allowed
                                      } else {
	                                      // Hide buttons
                                          $hideSpecialNotice = true;
	                                  }
	                              }
	                              
	                              //$thisClassList = $lnotice->returnClassList($noticeID);
	                              $thisClassList = $ClassListAssoArr[$noticeID];
	                              if ($thisClassList == '') {
	                                  $thisClassList = array();
                                  }
	
	                              $control = "";
	                              $start = strtotime($issueDate);
	                              $editLink = ($module == "Payment") ? "payment_notice/paymentNotice_edit.php" : ($TargetType != "S" ? "" : "student_notice/") . "edit.php";
	                              $css = ($i % 2) ? "" : "2";
	                              /*
	                              if (($fullRight || $hasIssueRight) && $CurrentPageArr['eAdminNotice'])
	                              {
	                              		## edit icon
	                                  	if (!($status == 1 && compareDate($start,$now) <= 0))
	                                  	{
											$editIcon = $lnotice->returnEditLink($noticeID);
																					
											$title_link = "<a href='/home/eAdmin/StudentMgmt/notice/$editLink?NoticeID=$noticeID' class='tablelink'>$title</a>";
										}
	                                  	else
	                                  	{
											$editIcon = "";
											$title_link = $title;
										}
	                              }
	                              */
	                              
	                              //$notice_info = $lnotice->returnRecord($noticeID);
	                              
	                              //if($notice_info['IssueUserID'] == $UserID and (!($status == 1 && compareDate($start,$now) <= 0)) && $CurrentPageArr['eAdminNotice'])                // self notice?
	                              //if($notice_info['IssueUserID'] == $UserID && $CurrentPageArr['eAdminNotice'])                // self notice?
	                              
	                              ## School Notice
	                              if($module != "Payment")
	                              {
                                      // [2020-0604-1821-16170] apply checking of this notice settings
	                              	  // Admin can edit notice even if the notice is not created by own
		                              //if(($notice_info['IssueUserID'] == $UserID && $CurrentPageArr['eAdminNotice']) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"])
	                              	  //if($CurrentPageArr['eAdminNotice'] && ($lnotice->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $fullRight || $lnotice->isApprovalUser()))  //$lnotice->isApprovalUser() for those user inside approval user
                                      if($CurrentPageArr['eAdminNotice'] && ($lnotice->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $noticeFullRight || $noticeIsApprovalUser))
		                              {
                                          $editIcon = $lnotice->returnEditLink($noticeID, $TargetType);
                                          $title_link = "<a href='/home/eAdmin/StudentMgmt/notice/$editLink?NoticeID=$noticeID' class='tablelink'>$title</a>";
                                          if($this->forApproval)
                                          {
                                              $title_link = "<a href='javascript:viewNotice($noticeID)' class='tablelink'>$title</a>";

                                              // [2020-0604-1821-16170] apply checking of this notice settings
                                              // [2016-1013-1541-17214] not display edit icon for approval user
                                              //if($lnotice->IssueUserID != $UserID && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] && !$fullRight){
                                              //if(($lnotice->IssueUserID != $UserID && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] && !$fullRight) || $lnotice->isApprovalUser()) {
                                              if(($lnotice->IssueUserID != $UserID && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] && !$noticeFullRight) || $noticeIsApprovalUser) {
                                                  //do nothing
                                              } else {
                                                  $title_link .= $editIcon;
                                              }
                                          }
		                              }
		                              else
		                              {
		                                  $editIcon = "";
		                                  $title_link = $title;

		                                  // [2016-0907-1010-32206] show link
                                          if(!$CurrentPageArr['eAdminNotice'] && $sys_custom['eNotice']['eServiceShowHyperlink'])
                                          {
                                              if(strtoupper($lnotice->Module) == "PAYMENT") {
                                                  $title_link = "<a href='javascript:viewPaymentNotice($noticeID)' class='tablelink'>$title</a>";
                                              } else {
                                                  $title_link = "<a href='javascript:viewNotice($noticeID)' class='tablelink'>$title</a>";
                                              }
                                          }
		                              }
	                              }
	                              # Module Notice
	                              else
	                              {
                                      // [2020-0604-1821-16170] apply checking of this notice settings
	                                  // [2016-0429-0912-09066] allow eNotice Admin edit payment notice even if notice is not created by own
		                              //if(($lnotice->IssueUserID == $UserID && $CurrentPageArr['eAdminNotice']))
                                      //if((($lnotice->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]) && $CurrentPageArr['eAdminNotice']) || $lnotice->isApprovalUser())
		                              if((($lnotice->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]) && $CurrentPageArr['eAdminNotice']) || $noticeIsApprovalUser)
		                              {
		                                  if($lnotice->RecordStatus >= 4 && $module == "Payment") {
		                                      $editIcon = $lnotice->returnEditLink($noticeID, $TargetType);

                                              // [2020-0604-1821-16170] apply checking of this notice settings
		                                      // [2016-1013-1541-17214] not display edit icon for approval user
                                              //if($lnotice->isApprovalUser()) {
                                              if($noticeIsApprovalUser) {
                                                  $editIcon = "";
                                              }
                                              $title_link = "<a href='javascript:viewNotice($noticeID)' class='tablelink'>$title</a>". $editIcon;
		                                  }
		                                  else {
		                                      $editIcon = $lnotice->returnEditLink($noticeID, $TargetType);
		                                      $title_link = "<a href='/home/eAdmin/StudentMgmt/notice/$editLink?NoticeID=$noticeID' class='tablelink'>$title</a>";
		                                  }
		                              }
		                              else
	                                  {
	                                       $editIcon = "";
	                                       $title_link = $title;

	                                       // [2016-0907-1010-32206] show link
                                           if(!$CurrentPageArr['eAdminNotice'] && $sys_custom['eNotice']['eServiceShowHyperlink'])
	                                       {
	                                           if(strtoupper($lnotice->Module) == "PAYMENT") {
	                                               $title_link = "<a href='javascript:viewPaymentNotice($noticeID)' class='tablelink'>$title</a>";
                                               } else {
	                                               $title_link = "<a href='javascript:viewNotice($noticeID)' class='tablelink'>$title</a>";
                                               }
	                                       }
	                                   }
	                              }

	                              // [2020-0604-1821-16170] apply checking of this notice settings
	                              $del_checkbox = true;
	                              //if(!$fullRight && $lnotice->IssueUserID != $UserID && !$lnotice->isApprovalUser())
                                  if(!$noticeFullRight && $lnotice->IssueUserID != $UserID && !$noticeIsApprovalUser)
                                  {
                                      $del_checkbox = false;
                                  }
	                               
	                              // $viewIcon = "<a href='javascript:viewNotice($noticeID)'><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_edit\" border=0></a>";
								  
	                              // eService > eNotice - use viewPaymentNotice for redirect to correct page
	                              if(!$CurrentPageArr['eAdminNotice'] && strtoupper($lnotice->Module) == "PAYMENT") {
	                                  $viewIcon = "<a href='javascript:viewPaymentNotice($noticeID)'><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_edit\" border=0></a>";
                                  } else {
	                                  $viewIcon = "<a href='javascript:viewNotice($noticeID)'><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_edit\" border=0></a>";
                                  }

                                  // [2020-0604-1821-16170] apply checking of this notice settings
								  // [2015-0323-1602-46073] - add $isPIC to checking
                                  //if(($fullRight || $hasIssueRight || $isPIC) && $CurrentPageArr['eAdminNotice'])
                                  if(($noticeFullRight || $noticeIssueRight || $isPIC) && $CurrentPageArr['eAdminNotice'])
								  {
									  $trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
								  	  //$x .= "<tr class='tablerow". ( $i%2 + 1 )."'>";
								  	  $x .= "<tr ".$trClass.">\n";
							  	  }
								  else 
								  {
								  	  $x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>";
								  }
								  
								  # 20200415 (Philips)
								  if($hideSpecialNotice)
								  {
                                      $title_link = $title;
                                      $viewIcon = '';
                                      $editIcon = '';
                                      $del_checkbox = false;
								  }
								  
	                              $j = 1;
	                              $x .= $this->displayCell($j++,$issueDate,"tabletext","valign='top'");
	                              $x .= $this->displayCell($j++,$duedate,"tabletext","valign='top'");
	                              $x .= $this->displayCell($j++,$noticeNumber,"tabletext","valign='top'");
	                              if(!$this->forApproval) {
	                             	  $x .= $this->displayCell($j++,"$viewIcon $editIcon","tabletext","nowrap align='left' valign='top'");
	                              }

	                              $x .= $this->displayCell($j++,$title_link,"tabletext","valign='top'");
	                              $x .= $this->displayCell($j++,$issuer,"tabletext","valign='top'");
	                              
	                              // [2015-0428-1214-11207] display PICs of eNotice in table
	                              $pics = "--";
//	                              $noticePICs = $lnotice->returnNoticePICNames();
	                              // [2015-0428-1214-11207] use associative array instead of returnNoticePICNames() to get PIC
	                              $noticePICs = (array)$NoticePICArr[$noticeID];
	                              if(count($noticePICs) > 0) {
	                              	  $pics = "";
	                              	  $delim = "";
	                              	  foreach((array)$noticePICs as $current_pic) {
	                              		  $pics .= $delim.$current_pic['UserName'];
	                              		  $delim = ", ";
	                              	  }
	                              }
	                              $x .= $this->displayCell($j++,$pics,"tabletext","valign='top'");

	                              // [2020-0604-1821-16170] apply checking of this notice settings
                                  //if($lnotice->needApproval) {
	                              if($noticeNeedApproval) {
		                              if($_GET['status'] == 5) {
		                              	$x .= $this->displayCell($j++,$approver,"tabletext","valign='top'");
		                              	$x .= $this->displayCell($j++,$approveTime,"tabletext","valign='top'");
		                              	$x .= $this->displayCell($j++,$approvalComment,"tabletext","valign='top'");
		                              }
		                              if($status == 1){
		                              	$x .= $this->displayCell($j++,$approver,"tabletext","valign='top'");
		                              }
	                              }
	                              
	                              # check is class teacher 
	                              $isAnyClassTeacher = 0;
	                              foreach($class as $k => $d)
	                              {
		                          	  if(in_array($d[0], $thisClassList)) {
		                          		  $isAnyClassTeacher = 1;
                                      }
	                              }
	                              if ($isClassTeacher && $status == 1)
	                              {
	                                  $class_link = "<a href='javascript:viewNoticeClass($noticeID)' class='tablelink'><img src=\"$image_path/$LAYOUT_SKIN/eOffice/icon_classview.gif\" border='0' align='absmiddle'></a>";
									  //$x .= $this->displayCell($j++, (in_array($class, $thisClassList)? $class_link:""),                 "tabletext","valign='top'");
	                                  # 20200415 (Philips)
	                                  if($hideSpecialNotice) {
	                                  	$class_link =  '';
	                                  }
	                                  $x .= $this->displayCell($j++, ($isAnyClassTeacher? $class_link : ""), "tabletext","valign='top'");
	                              }

	                              // [2020-0604-1821-16170] apply checking of this notice settings
								  // [2015-0323-1602-46073] - display view result link if user is also an PIC of current eNotice
								  // 						- allow display result link to issuer
                                  //if (($viewRight || $lnotice->staffview || $lnotice->IssueUserID == $UserID || $lnotice->isNoticePIC($noticeID)) && $status == 1)
	                              if (($noticeViewRight || $noticeStaffView || $lnotice->IssueUserID == $UserID || $lnotice->isNoticePIC($noticeID)) && $status == 1)
	                              {
	                                  $numSigned = $signedCounts[$noticeID]+0;
	                                  $numTotal = $totalCounts[$noticeID]+0;
	                                  $result_link = "<a href='javascript:viewResult($noticeID)' class='tablelink'><img src=\"$image_path/$LAYOUT_SKIN/eOffice/icon_resultview.gif\" border='0' align='absmiddle'>";
	                                  # 20200415 (Philips)
	                                  if($hideSpecialNotice) {
	                                  	$result_link = '';
	                                  }
	                                  $x .= $this->displayCell($j++,"$result_link $numSigned/$numTotal","tabletext","valign='top'");
	                              }
	                              // [2015-0323-1602-46073] - display empty cell if user is an eNotice PIC (but not a PIC of current eNotice)
	                              // [2016-04-07 updated] - display empty cell if user 
	                              //									1. is an eNotice PIC (but not a PIC of current eNotice)
	                              //									2. can issue notice (but not an issuer of current eNotice)
	                              //else if ($isPIC && $status == 1){
                                  // [2020-0604-1821-16170] apply checking of this notice settings
                                  //else if (($hasIssueRight || $isPIC) && $status == 1){
	                              else if (($noticeIssueRight || $isPIC) && $status == 1){
	                              	  $x .= $this->displayCell($j++,"&nbsp;","tabletext","valign='top'");
	                              }
	                              
	                              $x .= $this->displayCell($j++,$targetType[$type],"tabletext","valign='top'");
								  //$x .= $this->displayCell($j++,$module,"tabletext","valign='top'");

                                  // [2020-0604-1821-16170] apply checking of this notice settings
                             	  // [2015-0323-1602-46073] add $isPIC to checking
                                  //if(($fullRight || $hasIssueRight || $isPIC) && $CurrentPageArr['eAdminNotice']) {
	                              if(($noticeFullRight || $noticeIssueRight || $isPIC) && $CurrentPageArr['eAdminNotice']) {
	                                  $x .= $this->displayCell($i,($del_checkbox && $lnotice->returnDeleteLink($noticeID) ? "<input type='checkbox' name='NoticeIDArr[]' value='". $noticeID."' onClick='unset_checkall(this, this.form);'>" : "&nbsp;"), "tablelink","valign='top'");
                                  }

	                              $x .= "</tr>\n";
	                              $i++;
	                		}
	                }
					$x .= "</table>\n";
					
	                if($this->db_num_rows()<>0)
	                {
                		// [2015-0323-1602-46073] add $isPIC to checking
		                if(($fullRight || $hasIssueRight || $isPIC) && $CurrentPageArr['eAdminNotice'])
		                {
		                	//$x .= "<tr><td colspan='".($this->no_col)."' class='tablebottom'>".$this->navigation()."</td></tr>";
							$x .= $this->navigation_IP25();
						}
						else 
						{
							//$x .= "<tr><td colspan='".($this->no_col)."' class='tablebluebottom'>".$this->navigation()."</td></tr>";
							$x .= $this->navigation_IP25("","view_table_bottom"); 
						}
					}
// 	                $x .= "</table>\n";

					if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin']){
						global $Lang;
						$x .= "<span style='color:red'>* </span><span class='tabletextremark'>".$Lang['eNotice']['SpecialNotice']."</span>";
					}
					
	                $this->db_free_result();
	                return $x;
	        }
	
	        function displayFormat_displayParentView($TargetType='P')
	        {
	        		global $PATH_WRT_ROOT;
	                include_once($PATH_WRT_ROOT."includes/libnotice.php");
	                
	                #wordings
	                global $Lang;
	                global $i_Notice_Unsigned;
	                global $i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual;
	                	
	                ### Get All Notice Info first
	                $NoticeArr = $this->returnArray($this->built_sql());
	                $NoticeIDArr = Get_Array_By_Key($NoticeArr, 'NoticeID');
	                $lnotice = new libnotice('', $NoticeIDArr);
	                
	                $this->rs = $this->db_db_query($this->built_sql());
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	
	                if ($this->db_num_rows()==0)
	                {
		                 $x .= "<div class='no_record_find'>". $this->no_msg ."</div>";
		                 
// 	                        $x .= "<tr>
// 	                                <td class='tablebluerow2 tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
// 	                                </tr>\n
// 	                                ";
	                    }
	                else
	                {
		                $x .= "<table class=\"common_table_list view_table_list_v30\">";
	                    $x .= $this->displayColumn();
	                    
	                        $targetType = array("",$i_Notice_RecipientTypeAllStudents,
	                                        $i_Notice_RecipientTypeLevel,
	                                        $i_Notice_RecipientTypeClass,
	                                        $i_Notice_RecipientTypeIndividual);
	                                        

	
	                        while($row = $this->db_fetch_array())
	                         {
	                              //list($id,$start,$end,$number,$title,$studentID,$name,$noticeType,$replyType,$status,$signer,$signedAt,$module) = $row;
	                              list($id,$start,$end,$number,$title,$studentID,$name,$noticeType,$replyType,$status,$signer,$signedAt,$signer_id) = $row;
	                              //$lnotice = new libnotice($id);
	                              $lnotice->LoadNoticeData($id);
	                              
	                              $css = (($status == 0 || $status == "")? " row_suspend ":"");
	                              if ($replyType==2) $css = " row_waiting ";
	                              if ($signer_id=="")
									{
										$signer = $Lang['General']['EmptySymbol'];
										$signedAt = "$i_Notice_Unsigned";
									}
											
	                              if(strtoupper($lnotice->Module) == "PAYMENT")
	                              {
	                              	$title_link = "<a class='tablebluelink' href='javascript:signPaymentNotice($id,$studentID)'>$title</a>";
	                              	
	                              	//if ($signer=="&nbsp;" || !trim($signer))
	                              	if($signer_id=="")
									{
										$signedAt = "<a class='tablebluelink' href='javascript:signPaymentNotice($id,$studentID)'>$i_Notice_Unsigned</a>";
									}
	                              }
	                              else
	                              {
		                              if($TargetType=="P")
		                              {
	                              			$title_link = "<a class='tablebluelink' href='javascript:sign($id,$studentID)'>$title</a>";
											//if ($signer=="&nbsp;" || !trim($signer))
											if($signer_id=="")
											{
												$signedAt = "<a class='tablebluelink' href='javascript:sign($id,$studentID)'>$i_Notice_Unsigned</a>";
											}
                              			}
	                              		else
	                              		{
	                              			$title_link = "<a class='tablebluelink' href='javascript:viewReply($id,$studentID)'>$title</a>";
	                              			//$title_link = "<a class='tablebluelink' href='javascript:sign($id,$studentID)'>$title</a>";
                              			}
	                              }

									$LateRemark = ($signer!=$Lang['General']['EmptySymbol'] && $signedAt > substr($end,0,10)." 23:59:59") ? "*" : "";

	                              $j = 1;
	                              $x .= "<tr class='$css'>";
	                              $x .= $this->displayCell($j++,$start,                 "$css","valign='top' nowrap");
	                              $x .= $this->displayCell($j++,$end,                 "$css","valign='top' nowrap");
	                              $x .= $this->displayCell($j++,$number,                 "$css","valign='top'");
	                              $x .= $this->displayCell($j++,$title_link,         "$css","valign='top'");
	                              $x .= $this->displayCell($j++,$name,                 "$css","valign='top'");
	                              $x .= $this->displayCell($j++,$targetType[$noticeType],                 "$css","valign='top'");
	                              $x .= $this->displayCell($j++,$signer,                 "$css","valign='top'");
	                              $x .= $this->displayCell($j++,$signedAt . " " . $LateRemark,                 "$css","valign='top'");
	                              //$x .= $this->displayCell($j++,$module,                 "tabletext $css","valign='top'");
	                              $x .= "</tr>\n";
	                         }
						$x .= "</table>\n";
	                }
	
	                if($this->db_num_rows()<>0) $x .= $this->navigation_IP25("","view_table_bottom");
	                
	                $this->db_free_result();
	                return $x;
	        }
	
	        function displayFormat_displayAllNotice()
	        {

	        		global $PATH_WRT_ROOT;
	                include_once($PATH_WRT_ROOT."includes/libnotice.php");
	                
	                #wordings
	                global $i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual;
					
					### Get All Notice Info first
	                $NoticeArr = $this->returnArray($this->built_sql());
	                $NoticeIDArr = Get_Array_By_Key($NoticeArr, 'NoticeID');
	                $lnotice = new libnotice('', $NoticeIDArr);
	                
	                $this->rs = $this->db_db_query($this->built_sql());
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;

	                if ($this->db_num_rows()==0)
	                {
		                 $x .= "<div class='no_record_find'>". $this->no_msg ."</div>";
		                 
// 	                        $x .= "<tr>
// 	                                <td class='tablebluerow2 tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
// 	                                </tr>\n
// 	                                ";
	                    }
	                else
	                {
		                $x .= "<table class=\"common_table_list view_table_list_v30\">";
	                	$x .= $this->displayColumn();
	                
	                        $targetType = array("",$i_Notice_RecipientTypeAllStudents,
	                                        $i_Notice_RecipientTypeLevel,
	                                        $i_Notice_RecipientTypeClass,
	                                        $i_Notice_RecipientTypeIndividual);
	
	                        $i=0;
	                        while($row = $this->db_fetch_array())
	                         {
	                              list ($noticeID,$issueDate,$duedate,$noticeNumber,$title,$type,$module, $studentID) = $row;
	                              
	                              //$lnotice = new libnotice($noticeID);
	                              $lnotice->LoadNoticeData($noticeID);
	                              
	                              
									if(strtoupper($lnotice->Module) == "PAYMENT"){
										$title_link = "<a class='tablebluelink' href='javascript:viewPaymentNotice($noticeID, $studentID)'>$title</a>";
									}
									else
									{
										$title_link = "<a class='tablebluelink' href='javascript:viewNotice($noticeID, $studentID)'>$title</a>";
									}
	                              
	
	                              $j = 1;
	                              $x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>";
	                              $x .= $this->displayCell($j++,$issueDate,         "tabletext","valign='top'");
	                              $x .= $this->displayCell($j++,$duedate,         "tabletext","valign='top'");
	                              $x .= $this->displayCell($j++,$noticeNumber,         "tabletext","valign='top'");
	                              $x .= $this->displayCell($j++,$title_link,         "tabletext","valign='top'");
	                              $x .= $this->displayCell($j++,$targetType[$type],        "tabletext","valign='top'");
	                              //$x .= $this->displayCell($j++,$module,        "tabletext","valign='top'");
	                              
	                              $x .= "</tr>\n";
	                              $i++;
	                         }
							$x .= "</table>\n";
	                }
	
	                if($this->db_num_rows() > 0) $x .= $this->navigation_IP25("","view_table_bottom");
	                
	
	                $this->db_free_result();
	                return $x;
	        }
	
	        function displayFormat_displayParentHistory($TargetType='P')
	        {
					global $PATH_WRT_ROOT;
					include_once($PATH_WRT_ROOT."includes/libnotice.php");

	                #wordings
	                global $i_Notice_Unsigned, $i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual;
	
					### Get All Notice Info first
	                $NoticeArr = $this->returnArray($this->built_sql());
	                $NoticeIDArr = Get_Array_By_Key($NoticeArr, 'NoticeID');
	                $lnotice = new libnotice('', $NoticeIDArr);
	                
	                $this->rs = $this->db_db_query($this->built_sql());
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	
	                $now = date("Y-m-d H:i:s");

	                if ($this->db_num_rows()==0)
	                {
	                        $x .= "<tr>
	                                <td class='tablebluerow2 tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
	                                </tr>\n
	                                ";
	                    }
	                else
	                {
		                $x .= "<table class=\"common_table_list view_table_list_v30\">";
	                    $x .= $this->displayColumn();
	                    
	                        $targetType = array("",$i_Notice_RecipientTypeAllStudents,
	                                        $i_Notice_RecipientTypeLevel,
	                                        $i_Notice_RecipientTypeClass,
	                                        $i_Notice_RecipientTypeIndividual);
	
	                        while($row = $this->db_fetch_array())
	                         {
	                              list($id,$start,$end,$number,$title,$studentID,$name,$noticeType,$replyType,$status,$signer,$signedAt, $signer_id) = $row;
	                              
	                              //$lnotice = new libnotice($id);
	                              $lnotice->LoadNoticeData($id);
	                              
	                              if ($signer_id=="")
	                              {
	                                  $signer = "&nbsp;";
	                                  $signedAt = "$i_Notice_Unsigned";
	                              }
	                              $css = (($status == 0 || $status == "")? "row_suspend":"");
	                              if ($replyType==2) $css = "row_waiting";
	                              
	                              # check enotice expire or not
								  $notExpire = ($now < substr($end,0,10)." 23:59:59");
								  
								  # Payment eNotice
	                              if(strtoupper($lnotice->Module) == "PAYMENT")
	                              {
//	                              	$title_link = "<a href='javascript:viewPaymentReply($id,$studentID)' class='tablebluelink'>$title</a>";
//	                              		if ($now < substr($end,0,10)." 23:59:59"){
//                              				$signedAt = "<a href='javascript:sign($id,$studentID)' class='tablebluelink'>$i_Notice_Unsigned</a>";
//                              			}
									// allow to sign?
									if($lnotice->isLateSignAllow || $notExpire){
										$title_link = "<a class='tablebluelink' href='javascript:signPaymentNotice($id,$studentID)'>$title</a>";
										if($signer_id=="")
										{
											$signedAt = "<a class='tablebluelink' href='javascript:signPaymentNotice($id,$studentID)'>$i_Notice_Unsigned</a>";
										}
									}
									// only view reply if not allow to sign
									else {
										$title_link = "<a href='javascript:viewPaymentReply($id,$studentID)' class='tablebluelink'>$title</a>";
									}
	                              }
	                              # Normal eNotice
	                              else 
	                              {
//		                              # check parent can sign past notice or not
//		                              if($lnotice->isLateSignAllow && $TargetType=="P")
									  // Parent can sign - not expired notice OR can sign past notice
									  if($TargetType=="P" && ($lnotice->isLateSignAllow || $notExpire))
		                              {
				                      	$title_link = "<a href='javascript:sign($id,$studentID)' class='tablebluelink'>$title</a>";
										//if ($signer=="&nbsp;")
										if($signer_id=="")
										{
											$signedAt = "<a href='javascript:sign($id,$studentID)' class='tablebluelink'>$i_Notice_Unsigned</a>";
										}
	                      			}
				                      else
	                              		$title_link = "<a href='javascript:viewReply($id,$studentID)' class='tablebluelink'>$title</a>";
	                              }
	                              
	                              # check the notice is late submission or not
                      			$LateRemark = ($signer!="&nbsp;" && $signedAt > substr($end,0,10)." 23:59:59") ? "*" : "";
	
	                              $x .= "<tr class=\"$css\">";
	                              $x .= $this->displayCell($j++,$start,         "tabletext $css","valign='top' nowrap");
	                              $x .= $this->displayCell($j++,$end,         "tabletext $css","valign='top' nowrap");
	                              $x .= $this->displayCell($j++,$number,         "tabletext $css","valign='top'");
	                              $x .= $this->displayCell($j++,$title_link,         "tabletext $css","valign='top'");
	                              $x .= $this->displayCell($j++,$name,         "tabletext $css","valign='top'");
	                              $x .= $this->displayCell($j++,$targetType[$noticeType],         "tabletext $css","valign='top'");
	                              $x .= $this->displayCell($j++,$signer,         "tabletext $css","valign='top'");
	                              $x .= $this->displayCell($j++,$signedAt . " " . $LateRemark,         "tabletext $css","valign='top'");
	                              $x .= "</tr>\n";
	                         }
	                          $x .= "</table>\n";
	                }
	
	                if($this->db_num_rows()<>0) $x .= $this->navigation_IP25("","view_table_bottom");
	               
	
	                $this->db_free_result();
	                return $x;
	        }
	
	        function displayFormat_displayStudentView($TargetType='P')
	        {
	                global $UserID;
	                global $PATH_WRT_ROOT;
					include_once($PATH_WRT_ROOT."includes/libnotice.php");
	
	                #wordings
	                global $i_Notice_Unsigned, $i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual,$Lang;
					
					global $sys_custom;
					
					### Get All Notice Info first
	                $NoticeArr = $this->returnArray($this->built_sql());
	                $NoticeIDArr = Get_Array_By_Key($NoticeArr, 'NoticeID');
	                $lnotice = new libnotice('', $NoticeIDArr);
	                
	                $this->rs = $this->db_db_query($this->built_sql());
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	
	                //$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	                
	                if ($this->db_num_rows()==0)
	                {
		                $x .= "<div class='no_record_find'>". $this->no_msg ."</div>";
// 	                        $x .= "<tr>
// 	                                <td class='tablebluerow2 tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
// 	                                </tr>\n
// 	                                ";
	                }
	                else
	                {
		                $x .= "<table class=\"common_table_list view_table_list_v30\">";
	                    $x .= $this->displayColumn();
	                    
	                        $targetType = array("",$i_Notice_RecipientTypeAllStudents,
	                                        $i_Notice_RecipientTypeLevel,
	                                        $i_Notice_RecipientTypeClass,
	                                        $i_Notice_RecipientTypeIndividual);
	
	                        while($row = $this->db_fetch_array())
	                         {
	                               list($id,$start,$end,$number,$title,$noticeType,$replyType,$status,$signer,$signedAt,$module) = $row;
	                               
	                               //$lnotice = new libnotice($id);
	                               $lnotice->LoadNoticeData($id);
	
	                              $css = (($status == 0 || $status == "")? "row_suspend":" ");
	                              if ($replyType==2) $css = "row_waiting";
	
	                              if ($signer=="") 
	                              {
	                                  $signer = $Lang['General']['EmptySymbol'];
	                                 $signedAt = "$i_Notice_Unsigned";
	                              }
			                              
									if(strtoupper($lnotice->Module) == "PAYMENT")
									{
										$title_link = "<a class='tablebluelink' href='javascript:viewPaymentReply($id,$UserID)'>$title</a>";
									}
									else
									{
										if($TargetType!="S")
										{
											$title_link = "<a class='tablebluelink' href='javascript:viewReply($id,$UserID)'>$title</a>";
										}
										else
										{
											$title_link = "<a class='tablebluelink' href='javascript:sign($id,$UserID)'>$title</a>";
											if ($signer=="&nbsp;")
				                              {
				                                 $signedAt = "<a class='tablebluelink' href='javascript:sign($id,$UserID)'>$i_Notice_Unsigned</a>";
				                              }
										}
									}
									
									$LateRemark = ($signer!=$Lang['General']['EmptySymbol'] && $signedAt > substr($end,0,10)." 23:59:59") ? "*" : "";
								
	                              $x .= "<tr class='$css'>";
	                              $x .= $this->displayCell($j++,$start,         "tabletext $css","valign='top' nowrap");
	                              $x .= $this->displayCell($j++,$end,         "tabletext $css","valign='top' nowrap");
	                              $x .= $this->displayCell($j++,$number,         "tabletext $css","valign='top'");
	                              $x .= $this->displayCell($j++,$title_link,         "tabletext $css","valign='top'");
	                              if (!$sys_custom['project']['CourseTraining']['IsEnable']) {
	                              	$x .= $this->displayCell($j++,$targetType[$noticeType],         "tabletext $css","valign='top'");
	                              }
	                              $x .= $this->displayCell($j++,$signer,                 "$css","valign='top'");
                              	$x .= $this->displayCell($j++,$signedAt . " " . $LateRemark,                 "$css","valign='top'");
	                              //$x .= $this->displayCell($j++,$module,         "tabletext $css","valign='top'");
	                              $x .= "</tr>\n";
	                         }
	                         $x .= "</table>\n";
	                }
					
	                if($this->db_num_rows()<>0) $x .= $this->navigation_IP25("","view_table_bottom");
	                
	                $this->db_free_result();
	                return $x;
	        }
	
	        function displayFormat_displayeCircularView($past)
	        {
	                global $UserID, $i_Circular_Signed, $i_Circular_Unsigned;
	                $i = 0;
					
					
	                $this->rs = $this->db_db_query($this->built_sql());
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	
	                $x .= "<table width='96%' border='0' cellspacing='0' cellpadding='4'>";
	                    $x .= $this->displayColumn();
	
	                if ($this->db_num_rows()==0)
	                                {
	                                        $x .= "<tr>
	                                                <td class='tablerow tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
	                                                </tr>\n
	                                        ";
	                    }
	                else
	                {
	                        while($row = $this->db_fetch_array())
	                         {
	                                                        list($start,$end,$number,$temp, $title, $owner, $signed,$recordtype, $ID) = $row;
															echo $ID;
	                                                        $css = (($signed == 2 || $past==2)? "attendancepresent":"attendanceabsent");
	                                                        $i++;
	                                                        if ($i>$this->page_size)
	                                                                {
	                                                                        break;
	                                                                }
	                                                        $x .= "<tr>\n";
	                                                        $x .= "<td class='tabletext $css'>".($n_start+$i)."</td>\n";
	                                                        for ($j=0; $j<$this->no_col-1; $j++)
	                                                                {
	                                                                        if($j==6)
	                                                                        {
	                                                                                $displaySigned = $signed==2 ? $i_Circular_Signed : $i_Circular_Unsigned;
	                                                                                if($past!=2)
	                                                                                $x .= $this->displayCell($j, $displaySigned, "tabletext $css");
	                                                                        }
	                                                                        else
	                                                                                $x .= $this->displayCell($j, $row[$j], "tabletext $css");
	                                                                }
	                                                        $x .= "</tr>\n";
	                         }
	                }
	
	                if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".($this->no_col)."' class='tablebottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	
	
	                                $this->db_free_result();
	                return $x;
	        }
	
	        function displayFormat_displayStudentHistory($TargetType='P')
	        {
	        		global $PATH_WRT_ROOT;
					include_once($PATH_WRT_ROOT."includes/libnotice.php");
					
	                global $UserID;
	
	                #wordings
	                global $i_Notice_Unsigned, $i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual;
					global $sys_custom;
					
					### Get All Notice Info first
	                $NoticeArr = $this->returnArray($this->built_sql());
	                $NoticeIDArr = Get_Array_By_Key($NoticeArr, 'NoticeID');
	                $lnotice = new libnotice('', $NoticeIDArr);
	                
	                $this->rs = $this->db_db_query($this->built_sql());
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
					
	                if ($this->db_num_rows()==0)
	                {
		                 $x .= "<div class='no_record_find'>". $this->no_msg ."</div>";
// 	                        $x .= "<tr>
// 	                                <td class='tablebluerow2 tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
// 	                                </tr>\n
// 	                                ";
	                    }
	                else
	                {
		                $x .= "<table class=\"common_table_list view_table_list_v30\">";
	                    $x .= $this->displayColumn();
	                    
	                        $targetType = array("",$i_Notice_RecipientTypeAllStudents,
	                                        $i_Notice_RecipientTypeLevel,
	                                        $i_Notice_RecipientTypeClass,
	                                        $i_Notice_RecipientTypeIndividual);
	
	                        while($row = $this->db_fetch_array())
	                         {
	                               list($id,$start,$end,$number,$title,$noticeType,$replyType,$status,$signed,$signedAt) = $row;
	                               
	                               //$lnotice = new libnotice($id);
	                               $lnotice->LoadNoticeData($id);
	                               
	                              if ($signed=="")
	                              {
	                                  $signed = "&nbsp;";
	                                  $signedAt = "$i_Notice_Unsigned";
	                              }
	                              
	                              # check the notice is late submission or not
                      			$LateRemark = ($signed!="&nbsp;" && $signedAt > substr($end,0,10)." 23:59:59") ? "*" : "";
	
	                              $css = (($status == 0 || $status == "")? "row_suspend":" ");
	                              if ($replyType==2) $css = "row_waiting";
	                              
	                              	if(strtoupper($lnotice->Module) == "PAYMENT")
	                              	{
	                              		$title_link = "<a class='tablebluelink' href='javascript:viewPaymentReply($id,$UserID)'>$title</a>";
									} 
									else
									{
// 										debug_pr($TargetType);
										/*
										if($TargetType!="S")
										{
											$title_link = "<a class='tablebluelink' href='javascript:viewReply($id,$UserID)'>$title</a>";
										}
										else
										{
											$title_link = "<a class='tablebluelink' href='javascript:sign($id,$UserID)'>$title</a>";
											if ($signer=="&nbsp;")
				                              {
				                                 $signedAt = "<a class='tablebluelink' href='javascript:sign($id,$UserID)'>$i_Notice_Unsigned</a>";
				                              }
										}
										*/
										
										if($lnotice->isLateSignAllow && $TargetType=="S")
										{
											$title_link = "<a href='javascript:sign($id,$UserID)' class='tablebluelink'>$title</a>";
											if ($signed=="&nbsp;")
				                              {
				                                  $signedAt = "<a href='javascript:sign($id,$UserID)' class='tablebluelink'>$i_Notice_Unsigned</a>";
				                              }
										}
										else
										{
											$title_link = "<a class='tablebluelink' href='javascript:viewReply($id,$UserID)'>$title</a>";
										}
									}
	                              
	                              $x .= "<tr class='$css'>";
	                              $x .= $this->displayCell($j++,$start,         "tabletext $css","valign='top' nowrap");
	                              $x .= $this->displayCell($j++,$end,         "tabletext $css","valign='top' nowrap");
	                              $x .= $this->displayCell($j++,$number,         "tabletext $css","valign='top'");
	                              $x .= $this->displayCell($j++,$title_link,         "tabletext $css","valign='top'");
	                            if (!$sys_custom['project']['CourseTraining']['IsEnable']) {	                              
	                              $x .= $this->displayCell($j++,$targetType[$noticeType],         "tabletext $css","valign='top'");
								}
	                              $x .= $this->displayCell($j++,$signed,         "tabletext $css","valign='top'");
	                              $x .= $this->displayCell($j++,$signedAt . " " . $LateRemark,         "tabletext $css","valign='top'");
	                              $x .= "</tr>\n";
	                         }
	                          $x .= "</table>\n";
	                }
	
	                if($this->db_num_rows()<>0) $x .= $this->navigation_IP25("","view_table_bottom");
	               
	                $this->db_free_result();
	                return $x;
	        }
	
	        function displayFormat_DetailedRecordByYearWithReasonEditable($studentid, $editable=1)
	        {
	                        global $image_path, $LAYOUT_SKIN, $year, $start_date, $end_date;
	
	                #wordings
	                global $i_Profile_Late, $i_Profile_Absent, $i_Profile_EarlyLeave, $i_DayTypeArray;
	
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	                    $x .= $this->displayColumn();
	
	                if ($this->db_num_rows()==0)
	                                {
	                                        $x .= "<tr>
	                                        <td class='tablegreenrow2 tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                    }
	                else
	                {
	                                $i=1;
	                        while($row = $this->db_fetch_array())
	                         {
	                                 $i++;
	
	                                                        list ($record_id,$attendDate,$type,$dayType,$reason) = $row;
	
	                                                        if($editable)
	                                                                $edit_link = "<a onMouseMove='overhere()' href='view_attendance_edit.php?StudentID=$studentid&Year=$year&Type=$type&RecordID=$record_id&start_date=$start_date&end_date=$end_date'?><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" alt='$button_edit'/></a>";
	                                                        switch ($type)
	                                                        {
	                                                           case 1: $typeStr = $i_Profile_Absent;                 break;
	                                                           case 2: $typeStr = $i_Profile_Late;                         break;
	                                                           case 3: $typeStr = $i_Profile_EarlyLeave;         break;
	                                                           default: $typeStr = "Error";                                 break;
	                                                        }
	                                                        $dayStr = $i_DayTypeArray[$dayType];
	
	                                                        $j = 1;
	                                                        $x .= "<tr class='tablegreenrow". ( $i%2 + 1 )."'>";
	                                                        $x .= $this->displayCell($j++,$attendDate,         "tabletext $css","valign='top'");
	                                                        $x .= $this->displayCell($j++,$typeStr,         "tabletext $css","valign='top'");
	                                                        $x .= $this->displayCell($j++,$dayStr,                 "tabletext $css","valign='top'");
	                                                        $x .= $this->displayCell($j++,$reason,                 "tabletext $css","valign='top'");
	                                                        if($editable)
	                                                        {
	                                                                $x .= $this->displayCell($j++,$edit_link,         "tabletext $css","valign='top'");
	                                                                $x .= "<input name='record_id[$i]' type='hidden' value='$record_id'>";
	                                                        }
	                                                        $x .= "</tr>";
	                         }
	                }
					if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col.">".$this->navigation()."</td></tr>";

					$x .= "</table>\n";
	
	                                $this->db_free_result();
	                return $x;
	        }
	
	        function displayFormat_DetailedRecordByYear($studentid)
	        {
	                        global $image_path, $LAYOUT_SKIN, $year, $start_date, $end_date;
	
	                #wordings
	                global $i_Profile_Late, $i_Profile_Absent, $i_Profile_EarlyLeave, $i_DayTypeArray;

	                $this->rs = $this->db_db_query($this->built_sql());
	
	                                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	                    $x .= $this->displayColumn();
	
	                if ($this->db_num_rows()==0)
	                                {
	                                        $x .= "<tr>
	                                        <td class='tablegreenrow2 tabletext' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                    }
	                else
	                {
	                                $i=1;
	                        while($row = $this->db_fetch_array())
	                         {
	                                 $i++;
	
	                                                        list ($attendDate,$type,$dayType,$reason) = $row;
	
	                                                        switch ($type)
	                                                        {
	                                                           case 1: $typeStr = $i_Profile_Absent;                 break;
	                                                           case 2: $typeStr = $i_Profile_Late;                         break;
	                                                           case 3: $typeStr = $i_Profile_EarlyLeave;         break;
	                                                           default: $typeStr = "Error";                                 break;
	                                                        }
	                                                        $dayStr = $i_DayTypeArray[$dayType];
	
	                                                        $j = 1;
	                                                        $x .= "<tr class='tablegreenrow". ( $i%2 + 1 )."'>";
	                                                        $x .= $this->displayCell($j++,$attendDate,         "tabletext $css","valign='top'");
	                                                        $x .= $this->displayCell($j++,$typeStr,         "tabletext $css","valign='top'");
	                                                        $x .= $this->displayCell($j++,$dayStr,                 "tabletext $css","valign='top'");
	                                                        $x .= $this->displayCell($j++,$reason,                 "tabletext $css","valign='top'");
	                                                        $x .= "</tr>";
	                         }
	                }
					if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col.">".$this->navigation()."</td></tr>";
	                                $x .= "</table>\n";
	
	                                $this->db_free_result();
	                return $x;
	        }
	        function displayFormat_eCommForumList()
	        {
	                global $image_path, $LAYOUT_SKIN;
	                $i=0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                    $x .= "<table id='forumtable' width='100%' border='0' cellspacing='0' cellpadding='4'>\n";
	
	                    $x .= $this->displayColumn();
	                                if ($this->db_num_rows()==0)
	                        {
	                                $x .= "<tr class='forumtablerow '>
	                                        <td class='albumtablerow tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                                }
	
	                                else {
	
	                                        while($row = $this->db_fetch_array())
	                                        {
	                                    $i++;
	                                if($i>$this->page_size) break;
	                                $x .= "<tr class='forumtablerow'>\n";
	
	                                for($j=0; $j<=$this->no_col; $j++)
// 											if($j==1)
// 												$x .= $this->displayCell($j,"<div name='topic' style='overflow:hidden; height:30px;'>".$row[$j]."</div>","  albumtablerow tabletext");
// 											else
												$x .= $this->displayCell($j,$row[$j],"  albumtablerow tabletext");
	
	                                    $x .= "</tr>\n";
	                                        }
	
	                                }
	
	                        if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col.">".$this->navigation()."</td></tr>";
	                        $x .= "</table>\n";
	                        $this->db_free_result();
	                        return $x;
	        }
	                /*function displayFormat_eCommForumReplyList()
	        {
	                global $image_path, $LAYOUT_SKIN;
	
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                    //$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='3'>\n";
	                //$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	
	                    $x .= $this->displayColumn();
	                                if ($this->db_num_rows()==0)
	                        {
	                                $x .= "<tr >
	                                        <td class='tablerow2 tabletext' align='center' colspan='". $no_col ."' ><br />".$this->no_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                                }
	
	                                else {
	
	                                        while($row = $this->db_fetch_array())
	                                        {
	
	                              //  $x .= "<tr>\n";
	
	                                for($j=0; $j<=$this->no_col; $j++)
	                                        $x .= $this->displayCell($j,$row[$j]," albumtablerow tabletext");
	
	                                    $x .= "</tr>\n";
	                                        }
	
	                                }
	
	                        if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col.">".$this->navigation()."</td></tr>";
	                        $x .= "</table>\n";
	                        $this->db_free_result();
	                        return $x;
	        }        */
	
	        function displayFormat_eCommShareList_List($dt='file')
	        {
	                global $PATH_WRT_ROOT, $image_path;
	                include_once($PATH_WRT_ROOT."includes/libfile.php");
	                include_once($PATH_WRT_ROOT."includes/libfolder.php");
	
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $rr = array();
	                if($dt=='file')
	                {
	                        while($row = $this->db_fetch_array())
	                        {
	                        	$FileIDArr[] = $row['FileID'];
	                        	$FileDataArr[$row['FileID']] = $row;
//	                                                $lf                         = new libfile($row['FileID']);
//	                                                if(!$lf->viewFileRight())        continue;
//	                                                array_push($rr, $row);
	                        }
	                        $lf                         = new libfile('',$FileIDArr);
	                        foreach((array)$FileDataArr as $thisFileID => $thisFileData)
                         	{
                         		$lf->LoadFileData($thisFileID);
                         		if(!$lf->viewFileRight())        continue;
                         		 array_push($rr, $thisFileData);
                         	}
	                }
	                else
	                {
	                        while($row = $this->db_fetch_array())
	                        {
	                        	$FolderIDArr[] = $row['FolderID'];
	                        	$FolderDataArr[$row['FolderID']] = $row;
	                        	
//	                                                $lfolder                = new libfolder($row['FolderID']);
//	                                                if(!$lfolder->viewFileRight())        continue;
//	                                                array_push($rr, $row);
	                        }
	                         $lfolder                = new libfolder('',$FolderIDArr);
	                         foreach((array)$FolderDataArr as $thisFolderID => $thisFolderData)
                         	{
                         		$lfolder->LoadFolderData($thisFolderID);
                         		if(!$lfolder->viewFileRight())        continue;
                         		 array_push($rr, $thisFolderData);
                         	}
	                         
	                }
	
	                $x .= "<table width='95%' border='0' cellpadding='4' cellspacing='0'>\n";
	
	                $x .= $this->displayColumn();
	
	                if (sizeof($rr)==0){
	                        $x .= "<tr><td class='albumtablerow tabletext' align='center' colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
	                }else{
	                                                for($i=0;$i<sizeof($rr);$i++)
	                                                {
	                                $x .= "<tr class='forumtablerow'>\n";
	
	                                for($j=0; $j<$this->no_col; $j++)
	                                        $x .= $this->displayCell($j,$rr[$i][$j]," albumtablerow tabletext");
	
	                                $x .= "</tr>\n";
	                                                }
	                }
	
	                $x .= "</table>\n";
	                $this->db_free_result();
	                return $x;
	        }
	
	        function displayFormat_eCommShareList_4($dt='file')
	        {
	                global $image_path, $PATH_WRT_ROOT;
	                include_once($PATH_WRT_ROOT."includes/libfile.php");
	                include_once($PATH_WRT_ROOT."includes/libfolder.php");
	                include_once($PATH_WRT_ROOT."includes/libegroup.php");
	
	                $lg = new libegroup();
	
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $rr = array();
	                if($dt=='file')
	                {
	                        while($row = $this->db_fetch_array())
	                        {
	                        	$FileIDArr[] = $row['FileID'];
	                        	$FileDataArr[$row['FileID']] = $row;
//	                                                $lf                         = new libfile($row['FileID']);
//	                                                if(!$lf->viewFileRight())        continue;
//	                                                array_push($rr, $row);
	                        }
	                        $lf                         = new libfile('',$FileIDArr);
	                        foreach((array)$FileDataArr as $thisFileID => $thisFileData)
                         	{
                         		$lf->LoadFileData($thisFileID);
                         		if(!$lf->viewFileRight())        continue;
                         		
                         		$thisFileData['FileObj'] = $lf;
                         		 array_push($rr, $thisFileData);
                         	}
	                }
	                else
	                {
	                        while($row = $this->db_fetch_array())
	                        {
	                        	$FolderIDArr[] = $row['FolderID'];
	                        	$FolderDataArr[$row['FolderID']] = $row;
	                        	
//	                                                $lfolder                = new libfolder($row['FolderID']);
//	                                                if(!$lfolder->viewFileRight())        continue;
//	                                                array_push($rr, $row);
	                        }
	                         $lfolder                = new libfolder('',$FolderIDArr);
	                         foreach((array)$FolderDataArr as $thisFolderID => $thisFolderData)
                         	{
                         		$lfolder->LoadFolderData($thisFolderID);
                         		if(!$lfolder->viewFileRight())        continue;
                         		
                         		$thisFolderData['FolderObj'] = $lfolder;
                         		 array_push($rr, $thisFolderData);
                         	}
	                         
	                }
	
	                $x .= "<table width='95%' border='0' cellpadding='4' cellspacing='0'>\n";
	
	                if (sizeof($rr)==0){
	                        $x .= "<tr><td class='albumtablerow tabletext' align='center' colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
	                }else{
	                                                for($i=0;$i<sizeof($rr);$i++)
	                                                {
	                                                        $row = $rr[$i];
//	                                                        $li = new libfile($row[0]);
	
	                                                        if($dt=='file')
	                                                        {
	                                                                //$d = "<div id=\"photo_border\" height='165px'><center><a href=\"javascript:viewfile(". $row[0] .")\">". $lg->DisplayPhoto($row['FileObj'], 0, 165) ."</a></center></div>";
	                                                                $d = "<div id=\"photo_border\" height='165px'><center><a href=\"javascript:viewfile(". $row[0] .")\">". $lg->DisplayPhoto($row[0], 0, 165) ."</a></center></div>";
	                                                        }
	                                                        else
	                                                        {
	                                                                $d = $lg->FolderThumbnail($row['FolderObj'], $row['FolderID']);
	                                                        }
	
	                                $x .= ($i%2==0)?"<tr>":"";
	
	                                $x .= "<td widht='50%'>
	                                                <table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
	                                                  <tr>
	                                                    <td>
	
	                                                    $d
	
	                                                    </td>
	                                                    </tr>
	                                                  <tr>
	                                                    <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
	                                                      <tr>
	                                                        <td align=\"left\" class=\"share_file_title\">". $row[1] ."</td>
	                                                      </tr>
	                                                       ";
	                                                  $thisFileObj = $row['FileObj'];
	                               	if($thisFileObj->Approved==1)
	                               	{              
										// comment
		                                $x .= "
												<tr>
													<td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". $row[2] ."</span></td>
												</tr>
												<tr>
													<td align=\"left\" class=\"tabletext\">". $row[3] ."</td>
												</tr>
												";	
									}
									else
									{
										global $eComm;
										$x .= "
		                                                      <tr>
		                                                        <td align=\"left\" class=\"tabletext\">". $eComm['ApprovalStatus'] . ": " . ($thisFileObj->Approved==-1 ? $eComm['Rejected'] : $eComm['WaitingApproval']) ."</td>
		                                                      </tr>
												";	
									}
									$x .= "
	                                                    </table></td>
	                                                    </tr>
	                                                </table>
	                                          </td>
	                                                        ";
	
	                                                        $x .= ($i%2!=0)?"</tr>":"";
	                                                }
	                }
	
	                $x .= "</table>\n";
	                $this->db_free_result();
	                return $x;
	        }
	        function displayFormat_eCommDisplayMessage($BulletinID)
	        {
	                    global $image_path, $PATH_WRT_ROOT, $row, $BulletinID;
	                include_once($PATH_WRT_ROOT."includes/libbulletin.php");
	
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	                $li = new libbulletin($BulletinID);
	
	                $x .= $li->eCommReturnMessageTitle($BulletinID);
	
	                $x .= $li->eCommDisplayOrgMessage();
	
					//comment by marcus 20090729
	                /*$x .= "
	                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
	                                  <tr>
	                                    <td bgcolor=\"#fef9d4\">
	                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
	                                      <tr>
	                                        <td align=\"left\" class=\"tabletext\">". $this->navigation() ."</td>
	                                      </tr>
	                                    </table></td>
	                                  </tr>
	                                        </table>
	                ";*/
	                $x .= $li->eCommDisplayThread();
	               //if($this->total_row<>0)
	                 //$x .= "<tr><td colspan=".$row.">".$this->navigation()."</td></tr>";
	
	                        //$this->db_free_result();
	                                return $x;
	        }
	        function displayFormat_eCommDisplayMessageSetting($BulletinID)
	        {
	                    global $image_path, $PATH_WRT_ROOT, $row, $BulletinID;
	                include_once($PATH_WRT_ROOT."includes/libbulletin.php");
	
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	                $li = new libbulletin($BulletinID);
	
	                $x .= $li->eCommReturnMessageTitleSetting($BulletinID);
	
	                $x .= $li->eCommDisplayOrgMessageSetting();
	
	
	                                return $x;
	        }
	        function displayFormat_eCommForumSearchList()
	        {
	                global $image_path, $PATH_WRT_ROOT;
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	                $x .= "<table width=100% border=0 cellpadding=4 cellspacing=0>\n";
	                $x .= $this->displayColumn();
	               if ($this->db_num_rows()==0)
	                        {
	                                $x .= "<tr class='forumtablerow '>
	                                        <td class='albumtablerow tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                                }
	
	                                else {
	
	                                        while($row = $this->db_fetch_array())
	                                        {
	                                    $i++;
	                                if($i>$this->page_size) break;
	                                $x .= "<tr class='forumtablerow'>\n";
	
	                                for($j=0; $j<=$this->no_col; $j++)
	                                        $x .= $this->displayCell($j,$row[$j]," albumtablerow tabletext");
	
	                                    $x .= "</tr>\n";
	                                        }
	
	                                }
	
	                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tablebottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	                return $x;
	        }
	
	        function displayFormat_eCommAdminMemList()
	        {
	                global $image_path, $PATH_WRT_ROOT;
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
	                $x .= "<table width=100% border=0 cellpadding=4 cellspacing=0>\n";
	                $x .= $this->displayColumn();
	               if ($this->db_num_rows()==0)
	                        {
	                                $x .= "<tr class='forumtablerow '>
	                                        <td class='albumtablerow tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                                }
	
	                                else {
	
	                                        while($row = $this->db_fetch_array())
	                                        {
	                                    $i++;
	                                if($i>$this->page_size) break;
	                                $x .= "<tr class='forumtablerow'>\n";
	
	                                for($j=0; $j<=$this->no_col; $j++)
	                                        $x .= $this->displayCell($j,$row[$j]," albumtablerow tabletext");
	
	                                    $x .= "</tr>\n";
	                                        }
	
	                                }
	
	                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tablebottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	                return $x;
	        }
	
	        function displayFormat_eDisCaseReportList()
	        {
	                global $image_path, $LAYOUT_SKIN;
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	                $n_start = $this->n_start;
	
	                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	                    $x .= $this->displayColumn();
	                                if ($this->db_num_rows()==0)
	                        {
	                                $x .= "<tr>
	                                        <td class='tablerow2 tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
	                                        </tr>\n
	                                        ";
	                                }
	                                else
	                                {
	                                        while($row = $this->db_fetch_array())
	                                        {
	                                                if($i>$this->page_size) break;
	
	                                                $x .= "<tr class='tablerow". ( ($i)%2 + 1 )."'>\n";
	
	                                                $x .= $this->displayCell($i, ++$n_start, "tabletext","valign='top'");
	
	                                                for($j=0; $j< ($this->no_col-1); $j++)
	                                                {
	                                                        if (trim($row[$j]) == "")        $row[$j] = "&nbsp;";
	
	                                                        $x .= $this->displayCell($j,nl2br($row[$j]), "tabletext","valign='top'");
	                                                }
	
	                                                $x .= "</tr>\n";
	                                                $i++;
	                                        }
	
	                                        $x .= "<tr><td colspan=".$this->no_col." class='tablebottom'>".$this->navigation()."</td></tr>";
	                }
	
	                        $x .= "</table>";
	
	                        return $x;
	        }
	        function displayFormat_eInventoryList($width="", $tablecolor=""){
	                global $image_path, $Lang, $linventory;
	
	                $i = 0;
// 	                debug_pr($this->built_sql());
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $width = ($width!="") ? $width : "100%";
	
	                $n_start = $this->n_start;
	                $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	                $x .= $this->displayColumn();
	                if ($this->db_num_rows()==0)
	                {
	                        $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                } else
	                {
	                        while ($row = $this->db_fetch_array())
	                        {
	                                $i++;
	                                if($this->custom_css==true)
	                                	$css = $row[$this->no_col-2];
	                                else
	                                	$css = ($i%2) ? "1" : "2";
	
	                            if ($i>$this->page_size)
	                            {
	                                    break;
	                            }
	                                     $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                                     $x .= "<td class=' tabletext tablerow $css'>".($n_start+$i)."</td>\n";
	                                     for ($j=0; $j<$this->no_col-1; $j++)
	                                     {
	
	                                        if($j!=$this->no_col-2)
	                                        {
		                                        ## replace wordings in Remark fields
		                                        $this_display = $row[$j];
		                                        if(substr($this_display,0,17) == "[CHANGE_LOCATION]")
		                                        {
			                                        $location_str = substr($this_display,17,strlen($this_display));
			                                        list($ori_location, $new_location) = explode(",",$location_str);
			                                        $this_display = $Lang['General']['From'] . " : " . $linventory->returnLocationStr($ori_location) ."<br>";
			                                        $this_display .= $Lang['General']['To'] . " : " . $linventory->returnLocationStr($new_location);
	                                        	}
	                                        	$x .= "<td class =' tabletext tablerow $css' >".$this_display."</td>\n";
	                                    	}
	                                	 }
	                            $x .= "</tr>\n";
	                        }
	                }
	                if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	
	                return $x;
	        }
	        function displayFormat_eInventoryFullList($width="", $tablecolor=""){
	                global $image_path;
	
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $width = ($width!="") ? $width : "100%";
	
	                $n_start = $this->n_start;
	                $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	                $x .= $this->displayColumn();
	                if ($this->db_num_rows()==0)
	                {
	                        $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                } else
	                {
	                        while ($row = $this->db_fetch_array())
	                        {
	                                $i++;
	                                if($this->custom_css==true)
	                                	$css = $row[$this->no_col-2];
	                                else
	                                	$css = ($i%2) ? "1" : "2";
	
	                            if ($i>$this->page_size)
	                            {
	                                    break;
	                            }
	                                     $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                                     $x .= "<td class=' tabletext tablerow $css'>".($n_start+$i)."</td>\n";
	                                     for ($j=0; $j<$this->no_col-1; $j++)
	                                     {
	
	                                        if($j!=$this->no_col-2)
	                                        {
		                                        if($j == 1 || $j == 7 || $j == 8)
	                                        		$x .= "<td class =' tabletext tablerow $css' >".$row[$j]." </td>\n";
	                                        	else
	                                        		$x .= "<td class =' tabletext tablerow $css' >".intranet_htmlspecialchars($row[$j])." </td>\n";
	                                    	}
	                                	 }
	                            $x .= "</tr>\n";
	                        }
	                }
	                if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	
	                return $x;
	        }
	
	        function displayFormat_CurriculumExpectationList()
	        {
	                global $image_path;
	
	                $tablecolor = "";
	
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>";
	                $x .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					$x .= $this->displayColumn();
					if ($this->db_num_rows()==0)
	                {
	                	$x .= "<tr class='forumtablerow '>
	                        <td class='table{$tablecolor}row2 tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
	                        </tr>\n
	                        ";
	                }
	                else
	                {
		                $i=0;
						while($row = $this->db_fetch_array())
						{
		                    $i++;
		                    $css = ($i%2) ? "1" : "2";
	    		            if($i>$this->page_size) break;
	
	    		            $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                		for($j=0; $j<$this->no_col; $j++)
	                        	$x .= $this->displayCell($j,$row[$j]," tabletext", "nowrap");
	                    	$x .= "</tr>\n";

	                    }
	                }
	
	                $x .= "</table></td></tr>";
	                $x .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	                return $x;
	        }
	
	        function displayFormat_PersonalCharacteristicsList()
	        {
	                global $image_path;
	
	                $tablecolor = "";
	
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $n_start = $this->n_start;
	                $x .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					$x .= $this->displayColumn();
					if ($this->db_num_rows()==0)
	                {
	                	$x .= "<tr class='forumtablerow '>
	                        <td class='table{$tablecolor}row2 tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
	                        </tr>\n
	                        ";
	                }
	                else
	                {
		                $i=0;
						while($row = $this->db_fetch_array())
						{	
							if ($i>$this->page_size)
	                        {
	                                break;
	                        }	
		                    $i++;
		                    $css = ($i%2) ? "1" : "2";
	    		            if($i>$this->page_size) break;			
	    		            $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                		for($j=0; $j<$this->no_col; $j++)
	                        	$x .= $this->displayCell($j,$row[$j]," tabletext", "nowrap");       	
	                    	$x .= "</tr>\n";
                  	
	                    }
	                }
	                if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	                return $x;
	        }
	
	        function displayPlain(){
	            global $LAYOUT_SKIN;
			$i=0;
			$row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
			$this->n_start=($this->pageNo-1)*$this->page_size;
			$this->rs=$this->db_db_query($this->built_sql());
			
			
			$total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
			$bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
			$bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
	
			$x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
			$x .= $this->displayPlainColumn(1);
	
			if ($this->db_num_rows()==0)
			{
				$x.="<tr><td $bg_style1 align=center colspan=".$this->no_col." height='80'>".$this->no_msg."</td></tr>\n";
                $x .= '<tr><td colspan="'.$this->no_col.'" class="dotline"><img src="/images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td></tr>'."\n";
			} else
			{
	//			$this->total_row = $this->db_num_rows();
				while($row = $this->db_fetch_array())
				{
					$i++;
					if($i>$this->page_size) break;
					$x.="<tr>\n";
					$bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
					$x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
					for($j=0; $j<$total_col; $j++){
						$t = $row[$j];
						if(isset($this->column_formatter) && is_array($this->column_formatter) && isset($this->column_formatter[$j]) && is_callable($this->column_formatter[$j]) ){
							$t = call_user_func_array($this->column_formatter[$j], array($t,$row));
						}
						$x .= $this->displayPlainCell($j, $t, $bg_style_row, 1);
					}
					$x .= "</tr>\n";
				}
			}
			$x .= "</table>\n";
			if ($this->db_num_rows()>0)
			{
				$this->navigationHTML = $this->navigation(1);
			}
	
			$this->db_free_result();
			echo $x;
		}
	
		function displayPlainColumn($is_plain=0){
			$x = "<tr class='tabletop'>\n";
			$x .= ($is_plain) ? "" : "<td class='tabletext' nowrap background='$this->imagePath/frame/border2_left.gif'>&nbsp;</td>\n";
			$x .= $this->column_list;
			$x .= ($is_plain) ? "" : "<td class='tabletext' nowrap background='$this->imagePath/frame/border2_right.gif'>&nbsp;</td>\n";
			$x .= "</tr>\n";
			return $x;
		}
	
		function displayPlainCell($i, $data, $cn, $is_plain=0){
			$row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
			if (strcmp($data, "") == 0)
			{
				$data="&nbsp;";
			}
			$td_style = ($is_plain) ? $cn : "class=tableContent$cn";
			$row_height = $this->row_height;			
			switch ($this->column_array[$i])
			{
				case 0:
					$x = ($i==0 && $this->noNumber && !$is_plain) ? "<td  height='$row_height' align='center' $td_style $row_valign>$data</td>\n" : "<td height='$row_height' $td_style $row_valign>$data</td>\n";
					break;
				case 1:
					$x = "<td height='$row_height' $td_style $row_valign>".getDisplayContent($data)."</td>\n";
					break;
				case 2:
					$x = "<td height='$row_height' $td_style $row_valign>".stripslashes($data)."</td>\n";
					break;
				case 3:
					$x = "<td height='$row_height' $td_style align='center'>".$data."</td>\n";
					break;
				case 9:
					# Seconds to time (Usage record)
					if ($data)
					{
						if ($data > 3600)
						{
							$hr = intval($data/3600);
							$data = $data % 3600;
						}
						if ($data > 60)
						{
							$min = intval($data/60);
							$data = $data % 60;
						}
						$s_data = "";
						global $usage_hour, $usage_minutes, $usage_seconds;
						if ($hr != "") $s_data .= "$hr $usage_hour ";
						if ($min != "") $s_data .= "$min $usage_minutes ";
						$s_data .= "$data $usage_seconds";
					} else
					{
						$s_data = "&nbsp;";
					}
					$x = "<td height='$row_height' $td_style $row_valign>$s_data</td>\n";
					break;
				case 10:
					// nowrap
					$x = "<td height='$row_height' $td_style $row_valign nowrap>$data</td>\n";
					break;
				case 11:
					// new line to break
					$x = "<td height='$row_height' $td_style $row_valign>".nl2br($data)."</td>\n";
					break;
			}
	
			return $x;
		}
	
		function num_per_page(){
			global $range_all, $list_item_no, $list_display;
	
			$x = $list_display . " <select class='inputfield' name='num_per_page'  onChange='this.form.pageNo.value=1;this.form.page_size_change.value=1;this.form.numPerPage.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
			for($i=1;$i<=10;$i++)
			{
				$j = $i * $this->PageSizeInterval;
				$t=$j;
				if($this->page_size==$j)
				{
					$x.="<option value=$j selected>$t</option>\n";
				} else
				{
					$x.="<option value=$j>$t</option>\n";
				}
			}
			$x.="</select> <span class='cal_day'>".$list_item_no."</span>\n";
			return $x;
		}
	
		/*
		*	display ipf 2.5 school based scheme table
		*/
		function displayFormat_iPortfolioSchoolBasedScheme($Content="", $total_row="")
		{
			global $image_path, $PATH_WRT_ROOT;
			$i = 0;
			$this->rs = $this->db_db_query($this->built_sql());
			$n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
			$x .= "<table width=100% border=0 cellpadding=0 cellspacing=0>\n";
			//$x .= $this->displayColumn();
			if($total_row != "")
			{
				$this->total_row = $total_row;
			}
			//if ($this->db_num_rows()>0)
	/*		
	# Eric Yip (20090218) : Disable page changing since number of scheme should be small		
			if ($this->total_row > 0)
			{
				$x .= "<tr><td colspan=".$this->no_col." class='tablebottom'>".$this->navigation()."</td></tr>";
			}
	*/
			$x .= "<tr><td>&nbsp;</td></tr>";
	
			if ($Content=="")
			{
				$x .= "<tr class='forumtablerow '>
				<td class='albumtablerow tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
	 			</tr>\n
				";
			}
			else
			{
				$x .= "<tr><td>";
				$x .= $Content;
				$x .= "</td></tr>";
			}
	
			$x .= "<tr><td>&nbsp;</td></tr>";
	/*
			if ($this->db_num_rows()>0)
			$x .= "<tr><td colspan=".$this->no_col." class='tablebottom'>".$this->navigation()."</td></tr>";
	*/
			$x .= "</table>\n";
			$this->db_free_result();
			return $x;
		} // end function display ipf 2.5 school based scheme table
	
		function displayFormat_iPortfolioPhaseHandinList($ParArr="")
		{
			global $image_path, $PATH_WRT_ROOT;
	
			$Content = $ParArr["Content"];
	
			$i = 0;
			$n_start = $this->n_start;
			$x .= "<table width=95% border=0 cellpadding=0 cellspacing=0>\n";
	
			if ($this->total_row>0)
			$x .= "<tr class='tabletop'>".$this->column_list."</tr>";
	
			if ($this->total_row == 0)
			{
				$x .= "<tr class='forumtablerow '>
				<td class='albumtablerow tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
	 			</tr>\n
				";
			}
			else
			{
				//$x .= "<tr><td>";
				$x .= $Content;
				//$x .= "</td></tr>";
			}
	
			if ($this->total_row>0)
			$x .= "<tr><td colspan=".$this->no_col." class='tablebottom'>".$this->navigation()."</td></tr>";
			$x .= "</table>\n";
	
			return $x;
	
		} // end function display ipf 2.5 school based scheme handin list
	
	
			function displayFormat_iPortfolioStudentOLE1()
	        {
		        global $PATH_WRT_ROOT, $LAYOUT_SKIN;
	
		        $TickIcon = $PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_tick_green.gif";
		        $TickImage = "<img src='".$TickIcon."' />";
	
				$i=0;
				$row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
				$this->n_start=($this->pageNo-1)*$this->page_size;
				$this->rs=$this->db_db_query($this->built_sql());
	
				$total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
				$bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
				$bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
	
				$x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
				$x .= $this->displayColumnCustom();
	
				if ($this->db_num_rows()==0)
				{
					$x.="<tr class=\"tablerow1\"><td $bg_style1 align=center colspan=".($this->no_col+$this->additionalCols)." height='80'>".$this->no_msg."</td></tr>\n";
				}
				else
				{
					while($row = $this->db_fetch_array())
					{
						$i++;
						if($i>$this->page_size) break;
	
                        $rowClass = ($i%2) ? "tablerow1" : "tablerow2";
						$TmpStatus = $row[0];
						if ($TmpStatus == 1)
						{
							$x.="<tr class=\"{$rowClass} status_waiting\" >\n";
						}
						else if ($TmpStatus == 2)
						{
							$x.="<tr class=\"{$rowClass} status_approved\" >\n";
						}
						else if ($TmpStatus == 3)
						{
							$x.="<tr class=\"{$rowClass} status_reject\" >\n";
						}
						else if ($TmpStatus == 4)
						{
							$x.="<tr class=\"{$rowClass} status_teacher\" >\n";
						}
						else
						{
							$x.="<tr class=\"{$rowClass}\">\n";
						}
						$bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
						//$x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
						$x.= ($this->noNumber) ? "" : "<td  $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
	
						$TmpELECnt = $row[1];
						$TmpELE = trim($row[2]);
						if ($TmpELE != "")
						{
							$TmpELEArray = explode(",",$TmpELE);
						}
						
						$TmpCnt = 3;
						for($j=3; $j<$total_col; $j++)
						{
							$t = $row[$j];
							if ($j == 5)
							{
							//ELE
								if (count($TmpELEArray)>0)
								{
									for ($k=0;$k<count($TmpELEArray);$k++)
									{
										if (strstr($t,$TmpELEArray[$k]))
										{
											$x .= $this->displayCell($TmpCnt, $TickImage, $bg_style_row,"nowrap align='center' onmouseover=\"this.title=document.getElementById('oletitle_".$TmpELEArray[$k]."').title\" ");
										}
										else
										{
											$x .= $this->displayCell($TmpCnt,"&nbsp;", $bg_style_row, "nowrap align='center' onmouseover=\"this.title=document.getElementById('oletitle_".$TmpELEArray[$k]."').title\" ");
										}
										$TmpCnt++;
									}
								}
							}
							else
							{
								$x .= $this->displayCell($TmpCnt, $t, $bg_style_row, "nowrap align='center' valign='middle' ");
								$TmpCnt++;
							}
						}
						$x .= "</tr>\n";
					}
				}
				$x .= "</table>\n";
				if ($this->db_num_rows()>0)
				{
					$this->navigationHTML = $this->navigation(1);
				}
	
				$this->db_free_result();
				echo $x;
	        }
	
	        function displayFormat_iPortfolioStudentOLE1_EXT()
	        {
		        global $PATH_WRT_ROOT, $LAYOUT_SKIN;
	
		        $TickIcon = $PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_tick_green.gif";
		        $TickImage = "<img src='".$TickIcon."' />";
	
				$i=0;
				$row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
				$this->n_start=($this->pageNo-1)*$this->page_size;
				$this->rs=$this->db_db_query($this->built_sql());
	
				$total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
				$bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
				$bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
	
				$x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
				$x .= $this->displayColumnCustom();
	
				if ($this->db_num_rows()==0)
				{
					$x.="<tr class=\"tablerow1\"><td $bg_style1 align=center colspan=".($this->no_col+$this->additionalCols)." height='80'>".$this->no_msg."</td></tr>\n";
				}
				else
				{
					while($row = $this->db_fetch_array())
					{
						$i++;
						if($i>$this->page_size) break;
	
                        $rowClass = ($i%2) ? "tablerow1" : "tablerow2";
						$TmpStatus = $row[0];
						if ($TmpStatus == 1)
						{
							$x.="<tr class=\"{$rowClass} status_waiting\" >\n";
						}
						else if ($TmpStatus == 2)
						{
							$x.="<tr class=\"{$rowClass} status_approved\" >\n";
						}
						else if ($TmpStatus == 3)
						{
							$x.="<tr class=\"{$rowClass} status_reject\" >\n";
						}
						else if ($TmpStatus == 4)
						{
							$x.="<tr class=\"{$rowClass} status_teacher\" >\n";
						}
						else
						{
							$x.="<tr class=\"{$rowClass}\">\n";
						}
						$bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
						//$x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
						$x.= ($this->noNumber) ? "" : "<td  $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
	
						$TmpELECnt = $row[1];
						$TmpELE = trim($row[2]);
						if ($TmpELE != "")
						{
							$TmpELEArray = explode(",",$TmpELE);
						}
	
						$TmpCnt = 3;
						for($j=3; $j<$total_col; $j++)
						{
							$t = $row[$j];
							if ($j == 5)
							{
							//ELE
							/*
								if (count($TmpELEArray)>0)
								{
									for ($k=0;$k<count($TmpELEArray);$k++)
									{
										if (strstr($t,$TmpELEArray[$k]))
										{
											$x .= $this->displayCell($TmpCnt, $TickImage, $bg_style_row,"align='center' ");
										}
										else
										{
											$x .= $this->displayCell($TmpCnt,"&nbsp;", $bg_style_row, "align='center' ");
										}
										$TmpCnt++;
									}
								}
								*/
							}
							else
							{
								$x .= $this->displayCell($TmpCnt, $t, $bg_style_row, "align='center' valign='middle' ");
								$TmpCnt++;
							}
						}
						$x .= "</tr>\n";
					}
				}
				$x .= "</table>\n";
				if ($this->db_num_rows()>0)
				{
					$this->navigationHTML = $this->navigation(1);
				}
	
				$this->db_free_result();
				echo $x;
	    	}
	
			function displayFormat_iPortfolioTeacherOLE1($ProgramCount="")
	        {
		        global $PATH_WRT_ROOT, $LAYOUT_SKIN;
	
		        $TickIcon = $PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_tick_green.gif";
		        $TickImage = "<img src='".$TickIcon."' />";
	
				$i=0;
				$row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
				$this->n_start=($this->pageNo-1)*$this->page_size;
	
				//$ResultArr = $this->returnArray($this->built_sql());
				$ResultArr = $this->returnArray($this->built_sql("noLimit"));
				//debug_r($ResultArr);
	
				//$this->rs=$this->db_db_query($this->built_sql());
	
	
				$total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
				$bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
				$bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
	
				$x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
				$x .= $this->displayColumnCustom();
	
	
				// merge up the ProgramCount to the ResultArr
				$NewResultArr = array();
				for($s = 0; $s < count($ResultArr); $s++)
				{
					$row = $ResultArr[$s];
					$row[8] = $ProgramCount[$row["ProgramID"]]["StudentCnt"];
					$row[9] = $ProgramCount[$row["ProgramID"]]["PendingCnt"];
					//$row[9] = $ProgramCount[$row["ProgramID"]]["StudentCnt"];
					//$row[10] = $ProgramCount[$row["ProgramID"]]["PendingCnt"];
					$NewResultArr[] = $row;
				}
	
				//debug_r($this->field);
				//debug_r($this->order);
				if($this->order == 0)
				$SortOrder = SORT_ASC;
				else
				$SortOrder = SORT_DESC;
	
				if($this->field == 4)
				{
					foreach ($NewResultArr as $key => $row)
					{
	    				$tmpStudentCnt[$key]  = $row['8']; //8 (old)
					}
					// Sort the data with volume descending, edition ascending
					// Add $data as the last parameter, to sort by the common key
					array_multisort($tmpStudentCnt, $SortOrder, $NewResultArr);
				}
	
				if($this->field == 5)
				{
					foreach ($NewResultArr as $key => $row)
					{
	    				$tmpStudentCnt[$key]  = $row['9']; //9 (old)
					}
					// Sort the data with volume descending, edition ascending
					// Add $data as the last parameter, to sort by the common key
					array_multisort($tmpStudentCnt, $SortOrder, $NewResultArr);
				}
	
				//if ($this->db_num_rows()==0)
				if(count($NewResultArr) <= 0)
				{
					$x.="<tr><td $bg_style1 align=center colspan=".($this->no_col+$this->additionalCols)." height='80'>".$this->no_msg."</td></tr>\n";
				}
				else
				{
					//while($row = $this->db_fetch_array())
					for($s = 0; $s < count($NewResultArr); $s++)
					{
						$row = $NewResultArr[$s];
	
						$i++;
						if($i<=$this->n_start) continue;
						if($i>$this->n_start+$this->page_size) break;
	
						$TmpStatus = $row[0];
	
						$x.="<tr>\n";
						$bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
						$x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".$i."&nbsp;</td>\n" ;
	
						$TmpELECnt = $row[1];
						$TmpELE = $row[2];
						$TmpELEArray = explode(",",$TmpELE);
	
						$TmpCnt = 3; 
						for($j=3; $j<$total_col; $j++)
						{
							$t = $row[$j];
	
							if ($j == 6) // 6 (old)
							{
							//ELE
								if (count($TmpELEArray)>0)
								{
									for ($k=0;$k<count($TmpELEArray);$k++)
									{
										//debug_r($TmpELEArray[$k]);
										if (($TmpELEArray[$k] != "") && strstr($t,$TmpELEArray[$k]))
										{
											$x .= $this->displayPlainCell($TmpCnt, $TickImage, $bg_style_row." align='center' ",1);
										}
										else
										{
											$x .= $this->displayPlainCell($TmpCnt,"&nbsp", $bg_style_row." align='center' ", 1);
										}
										$TmpCnt++;
									}
								}
							}
							else
							{
								$x .= $this->displayPlainCell($TmpCnt, $t, $bg_style_row, 1);
								$TmpCnt++;
							}
						}
						$x .= "</tr>\n";
					} // end while
				} // end if
				$x .= "</table>\n";
				//if ($this->db_num_rows()>0)
				if(count($ResultArr) > 0)
				{
					$this->navigationHTML = $this->navigation(1);
				}
	
				//$this->db_free_result();
				echo $x;
	        }
	
	        function displayFormat_iPortfolioTeacherOLE_EXT($ProgramCount="")
	        {
		        global $PATH_WRT_ROOT, $LAYOUT_SKIN;
	
		        $TickIcon = $PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_tick_green.gif";
		        $TickImage = "<img src='".$TickIcon."' />";
	
				$i=0;
				$row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
				$this->n_start=($this->pageNo-1)*$this->page_size;
	
				//$this->rs=$this->db_db_query($this->built_sql());
				$ResultArr = $this->returnArray($this->built_sql("noLimit"));
	
				$total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
				$bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
				$bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
	
				$x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
				$x .= $this->displayColumnCustom();
	
				// merge up the ProgramCount to the ResultArr
				$NewResultArr = array();
				for($s = 0; $s < count($ResultArr); $s++)
				{
					$row = $ResultArr[$s];
					$row[8] = $ProgramCount[$row["ProgramID"]]["StudentCnt"];
					$row[9] = $ProgramCount[$row["ProgramID"]]["PendingCnt"];
					//$row[9] = $ProgramCount[$row["ProgramID"]]["StudentCnt"];
					//$row[10] = $ProgramCount[$row["ProgramID"]]["PendingCnt"];
					$NewResultArr[] = $row;
				}
	
				//debug_r($this->field);
				//debug_r($this->order);
				if($this->order == 0)
				$SortOrder = SORT_ASC;
				else
				$SortOrder = SORT_DESC;
	
				if($this->field == 4)
				{
					foreach ($NewResultArr as $key => $row)
					{
	    				$tmpStudentCnt[$key]  = $row['8']; //8
					}
					// Sort the data with volume descending, edition ascending
					// Add $data as the last parameter, to sort by the common key
					array_multisort($tmpStudentCnt, $SortOrder, $NewResultArr);
				}
	
				if($this->field == 5)
				{
					foreach ($NewResultArr as $key => $row)
					{
	    				$tmpStudentCnt[$key]  = $row['9']; // 9
					}
					// Sort the data with volume descending, edition ascending
					// Add $data as the last parameter, to sort by the common key
					array_multisort($tmpStudentCnt, $SortOrder, $NewResultArr);
				}
	
				//if ($this->db_num_rows()==0)
				if(count($NewResultArr) <= 0)
				{
					$x.="<tr><td $bg_style1 align=center colspan=".($this->no_col+$this->additionalCols)." height='80'>".$this->no_msg."</td></tr>\n";
				}
				else
				{
					//while($row = $this->db_fetch_array())
					for($s = 0; $s < count($NewResultArr); $s++)
					{
						$row = $NewResultArr[$s];
	
						$i++;
						if($i<=$this->n_start) continue;
						if($i>$this->n_start+$this->page_size) break;
	
						$TmpStatus = $row[0];
	
						$x.="<tr>\n";
						$bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
						$x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($i)."&nbsp;</td>\n" ;
	
	
						$TmpELECnt = $row[1];
						$TmpELE = $row[2];
						$TmpELEArray = explode(",",$TmpELE);
	
						$TmpCnt = 3;
						for($j=3; $j<$total_col; $j++)
						{
	
							$t = $row[$j];
							if ($j == 6) //6
							{
							//ELE
							/*
								if (count($TmpELEArray)>0)
								{
									for ($k=0;$k<count($TmpELEArray);$k++)
									{
										//debug_r($TmpELEArray[$k]);
										if (($TmpELEArray[$k] != "") && strstr($t,$TmpELEArray[$k]))
										{
											$x .= $this->displayPlainCell($TmpCnt, $TickImage, $bg_style_row." align='center' ",1);
										}
										else
										{
											$x .= $this->displayPlainCell($TmpCnt,"&nbsp", $bg_style_row." align='center' ", 1);
										}
										$TmpCnt++;
									}
								}
								*/
							}
							else
							{
								$x .= $this->displayPlainCell($TmpCnt, $t, $bg_style_row, 1);
								$TmpCnt++;
							}
						}
						$x .= "</tr>\n";
					}
				}
				$x .= "</table>\n";
	
				if (count($NewResultArr) >0)
				//if ($this->db_num_rows()>0)
				{
					$this->navigationHTML = $this->navigation(1);
				}
	
				//$this->db_free_result();
				echo $x;
	        }
			
			function displayFormat_iPortfolioOLEPool($ParIntExt=0)
	        {
		        global $PATH_WRT_ROOT, $LAYOUT_SKIN;
	
		        $TickIcon = $PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_tick_green.gif";
		        $TickImage = "<img src='".$TickIcon."' />";
	
				$i=0;
				$row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
				$this->n_start=($this->pageNo-1)*$this->page_size;
				$this->rs=$this->db_db_query($this->built_sql());
	
				$total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
                $tdClass = "class=\"tabletext tablerow\"";
	
				$x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
				$x .= $this->displayColumnCustom();
	
				if ($this->db_num_rows()==0)
				{
					$x.="<tr><td align=center colspan=".($this->no_col+$this->additionalCols)." height='80' class='td_no_record'>".$this->no_msg."</td></tr>\n";
                    $x .= '<tr><td colspan="'.($this->no_col+$this->additionalCols).'" class="dotline"><img src="/images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td></tr>'."\n";
				}
				else
				{
					while($row = $this->db_fetch_array())
					{
						$i++;
						if($i>$this->page_size) break;
	
                        $rowClass = ($i%2) ? "tablerow1" : "tablerow2";
                        $x .= "<tr class=\"$rowClass\">\n";
						$x.= ($this->noNumber) ? "" : "<td $tdClass $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
	
						$TmpELECnt = $row[0];
						$TmpELE = trim($row[1]);
						if ($TmpELE != "")
						{
							$TmpELEArray = explode(",",$TmpELE);
						}
	
						$TmpCnt = 2;
						for($j=2; $j<$total_col; $j++)
						{
							$t = $row[$j];
							if ($j == 4)
							{
							//ELE
								if($ParIntExt == 1) continue;
							
								if (count($TmpELEArray)>0)
								{
									for ($k=0;$k<count($TmpELEArray);$k++)
									{
										if (strstr($t,$TmpELEArray[$k]))
										{
											$x .= $this->displayCell($TmpCnt, $TickImage, "", $tdClass." align='center' ");
										}
										else
										{
											$x .= $this->displayCell($TmpCnt,"&nbsp;", "", $tdClass." align='center' ");
										}
										$TmpCnt++;
									}
								}
							}
							else
							{
								$x .= $this->displayCell($TmpCnt, $t, "", $tdClass." align='center' valign='middle' ");
								$TmpCnt++;
							}
						}
						$x .= "</tr>\n";
					}
				}
				$x .= "</table>\n";
				if ($this->db_num_rows()>0)
				{
					$this->navigationHTML = $this->navigation(1);
				}
	
				$this->db_free_result();
				echo $x;
	        }
	
	        function displayFormat_iPortfolioTeacherOLEStudentView(){
		        global $PATH_WRT_ROOT, $LAYOUT_SKIN, $eclass_db, $intranet_db;
	
		        $TickIcon = $PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_tick_green.gif";
		        $TickImage = "<img src='".$TickIcon."' />";
	
				$i=0;
				$row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
				$this->n_start=($this->pageNo-1)*$this->page_size;
				$this->rs=$this->db_db_query($this->built_sql());
	
				$total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
				$bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
				$bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
	
				$x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
				$x .= $this->displayColumnCustom();
	
				if ($this->db_num_rows()==0)
				{
					$x.="<tr><td $bg_style1 align=center colspan=".($this->no_col+$this->additionalCols)." height='80'>".$this->no_msg."</td></tr>\n";
				}
				else
				{
					$liTemp = new libdbtable2007(0,0,0);
					while($row = $this->db_fetch_array())
					{
						$i++;
						if($i>$this->page_size) break;
	
						$studentID = $row[0];
	
						$x.="<tr>\n";
						$bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
						$x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
	
						$TmpELECnt = $row[1];
						$TmpELE = $row[2];
						$TmpELEArray = explode(",",$TmpELE);
	
						$TmpCnt = 3;
						for($j=3; $j<$total_col; $j++)
						{
							$t = $row[$j];
							if ($j == 5)
							{
							//ELE
								if (count($TmpELEArray)>0)
								{
									for ($k=0;$k<count($TmpELEArray);$k++)
									{
										if (($TmpELEArray[$k] != ""))
										{
	
											$sql = "	SELECT
															count(DISTINCT a.RecordID)
														FROM
															{$eclass_db}.OLE_STUDENT as a
														INNER JOIN
															{$intranet_db}.INTRANET_USER as b
														ON
															a.UserID = b.UserID
														LEFT JOIN
															{$eclass_db}.OLE_STUDENT as c
														ON
															a.UserID = c.UserID
															AND c.RecordStatus = 1
											        	WHERE
											        		a.UserID = $studentID
											        		AND
															a.ELE LIKE '%".$TmpELEArray[$k]."%'
														";
	
											$result = $liTemp->returnArray($sql,1);
	
											$x .= $this->displayPlainCell($TmpCnt, $result[0][0], $bg_style_row." align='center' ",1);
										}
										else
										{
											$x .= $this->displayPlainCell($TmpCnt,"0", $bg_style_row." align='center' ", 1);
										}
										$TmpCnt++;
									}
								}
							}
							else
							{
								$x .= $this->displayPlainCell($TmpCnt, $t, $bg_style_row, 1);
								$TmpCnt++;
							}
						}
						$x .= "</tr>\n";
					}
				}
				$x .= "</table>\n";
				if ($this->db_num_rows()>0)
				{
					$this->navigationHTML = $this->navigation(1);
				}
	
				$this->db_free_result();
				echo $x;
	        }
	
			function displayFormat_iPortfolioTeacherOLEStudentView_EXT(){
		        global $PATH_WRT_ROOT, $LAYOUT_SKIN, $eclass_db, $intranet_db;
	
		        $TickIcon = $PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_tick_green.gif";
		        $TickImage = "<img src='".$TickIcon."' />";
	
				$i=0;
				$row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
				$this->n_start=($this->pageNo-1)*$this->page_size;
				$this->rs=$this->db_db_query($this->built_sql());
	
				$total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
				$bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
				$bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
	
				$x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
				$x .= $this->displayColumnCustom();
	
				if ($this->db_num_rows()==0)
				{
					$x.="<tr><td $bg_style1 align=center colspan=".($this->no_col+$this->additionalCols)." height='80'>".$this->no_msg."</td></tr>\n";
				}
				else
				{
					$liTemp = new libdbtable2007(0,0,0);
					while($row = $this->db_fetch_array())
					{
						$i++;
						if($i>$this->page_size) break;
	
						$studentID = $row[0];
	
						$x.="<tr>\n";
						$bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
						$x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
	
						$TmpELECnt = $row[1];
						$TmpELE = $row[2];
						$TmpELEArray = explode(",",$TmpELE);
	
						$TmpCnt = 3;
						for($j=3; $j<$total_col; $j++)
						{
							$t = $row[$j];
							if ($j == 5)
							{
							//ELE
							/*
								if (count($TmpELEArray)>0)
								{
									for ($k=0;$k<count($TmpELEArray);$k++)
									{
										if (($TmpELEArray[$k] != ""))
										{
	
											$sql = "	SELECT
															count(DISTINCT a.RecordID)
														FROM
															{$eclass_db}.OLE_STUDENT as a
														INNER JOIN
															{$intranet_db}.INTRANET_USER as b
														ON
															a.UserID = b.UserID
														LEFT JOIN
															{$eclass_db}.OLE_STUDENT as c
														ON
															a.UserID = c.UserID
															AND c.RecordStatus = 1
											        	WHERE
											        		a.UserID = $studentID
											        		AND
															a.ELE LIKE '%".$TmpELEArray[$k]."%'
														";
	
											$result = $liTemp->returnArray($sql,1);
	
											$x .= $this->displayPlainCell($TmpCnt, $result[0][0], $bg_style_row." align='center' ",1);
										}
										else
										{
											$x .= $this->displayPlainCell($TmpCnt,"0", $bg_style_row." align='center' ", 1);
										}
										$TmpCnt++;
									}
								}
								*/
							}
							else
							{
								$x .= $this->displayPlainCell($TmpCnt, $t, $bg_style_row, 1);
								$TmpCnt++;
							}
						}
						$x .= "</tr>\n";
					}
				}
				$x .= "</table>\n";
				if ($this->db_num_rows()>0)
				{
					$this->navigationHTML = $this->navigation(1);
				}
	
				$this->db_free_result();
				echo $x;
	        }
	        
	        function displayFormat_iPortfolio_DBS_Predicted_Grade()
	        {
	            global $PATH_WRT_ROOT, $image_path, $Lang;
	            
	            $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                
	            $width = ($width!="") ? $width : 560;
	            
	            $n_start = $this->n_start;
	            $x = '';
	            $x .= "<table class='common_table_list_v30 $othercss1'>\n";
	            $x .= "<thead>\n";
	            $x .= $this->displayColumn();
	            $x .= "</thead>";
	            
                $numberOfRows = $this->db_num_rows();
	            if ($numberOfRows==0)
	            {
	                $x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
	            }
	            else
	            {
	                include_once($PATH_WRT_ROOT."includes/libportfolio.php");
	                include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
	                $lpf = new libportfolio();
	                $lpf_dbs = new libpf_dbs_transcript();
	                
	                // Check if Super Admin
	                $isPortfolioSuperAdmin = $lpf->IS_IPF_SUPERADMIN();
	                
	                // Check if Subject Teacher
	                $isSubjectTeacher = !$isPortfolioSuperAdmin && $lpf->isSubjectTeacher();
	                if($isSubjectTeacher)
	                {
	                    $currentDateTime = strtotime(date('Y-m-d 00:00:00'));
	                    
	                    // Adjustment Period: 1st Amendment
    	                $predictPeriod1 = $lpf_dbs->getAdjustmentPeriodSettings('', 2);
    	                $predictPeriod1 = $predictPeriod1[0];
    	                $isAllowEditPeriod1 = $predictPeriod1['PeriodStart'] != '' && $predictPeriod1['PeriodEnd'] != '' && 
    	                                       $currentDateTime >= strtotime($predictPeriod1['PeriodStart']) && $currentDateTime <= strtotime($predictPeriod1['PeriodEnd']);
    	                                       
                        // Adjustment Period: 2nd Amendment
    	                $predictPeriod2 = $lpf_dbs->getAdjustmentPeriodSettings('', 3);
    	                $predictPeriod2 = $predictPeriod2[0];
    	                $isAllowEditPeriod2 = $predictPeriod2['PeriodStart'] != '' && $predictPeriod2['PeriodEnd'] != '' &&
    	                                       $currentDateTime >= strtotime($predictPeriod2['PeriodStart']) && $currentDateTime <= strtotime($predictPeriod2['PeriodEnd']);
	                }
	                
	                // Get all DSE Grades
	                $gradeListArr = $lpf_dbs->getCurriculumGrades();
	                $gradeListArr = $gradeListArr['HKDSE'];
	                foreach((array)$gradeListArr as $thisIndex => $thisGrade) {
	                    $gradeListArr[$thisIndex] = array($thisGrade, $thisGrade);
	                }
	                array_unshift($gradeListArr, array('NC', $Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['NoChange']));
	                
                    while($row = $this->db_fetch_array())
                    {
                        $i++;
                        $css = ($i%2) ? "" : "2";
                        if($i>$this->page_size) break;
                        
                        // Get all Student Adjustment
                        $all_adjustment = array();
                        if($row['UserID'] && $row['SubjectID']) {
                            $all_adjustment = $lpf_dbs->getStudentAdjustment('', $row['UserID'], '', $row['SubjectID'], '', '', $predictedType='', $portfolioAdmin=false, $returnSQL=false);
                            $all_adjustment = BuildMultiKeyAssoc($all_adjustment, 'PredictPhase');
                        }
                        
                        $x .= "<tr>\n";
                        $x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
                        for($j=0; $j<$this->no_col-1; $j++)
                        {
                            $hasOriginalGrade = $all_adjustment[1]['PredictedGrade'] != '';
                            
                            $predictedGrade = '';
                            if(!empty($all_adjustment))
                            {
                                // Original Grade
                                if($j == 3)
                                {
                                    $predictedGrade = $all_adjustment[1]['PredictedGrade'];
                                }
                                // 1st Amendment
                                else if($j == 4)
                                {
                                    $isSavedAsDraft = $all_adjustment[2]['PredictType'] == '1';
                                    
                                    /*
                                     *  Amendment Grade Display
                                     *  1. HAS Original Grade
                                     *  2. NOT Saved as Draft  OR  Super Admin  OR  Subject Teacher
                                     */
                                    if($hasOriginalGrade && (!$isSavedAsDraft || $isPortfolioSuperAdmin || $isSubjectTeacher))
                                    {
                                        $predictedGrade = $all_adjustment[2]['PredictedGrade'];
                                        
                                        // Super Admin + Saved as Draft  >  Not display grade
                                        if($isPortfolioSuperAdmin && $isSavedAsDraft) {
                                            $predictedGrade = '';
                                        }
                                        
                                        //if(!$row['isViewOnly'] && !($isSubjectTeacher && !$isAllowEditPeriod1)) {
                                        if(!$row['isViewOnly'])
                                        {
                                            /*
                                             *  Amendment Grade Input Display
                                             *  1. Super Admin
                                             *  2. Subject Teacher + Adjustment Period: 1st Amendment
                                             */
                                            if($isPortfolioSuperAdmin || ($isSubjectTeacher && $isAllowEditPeriod1))
                                            {
                                                /* 
                                                $thisGradeListArr = $gradeListArr;
                                                if($predictedGrade == '' && $all_adjustment[1]['PredictedGrade'] != '') {
                                                    $thisGradeListArr[] = array($all_adjustment[1]['PredictedGrade'], 'unchanged');
                                                }
                                                
                                                $predictedGrade = '<input type="text" class="predict_inputs" name="predict[2]['.$row[7].']" value="'.$predictedGrade.'" >';
                                                 */
                                                $predictedGrade = getSelectByArray($gradeListArr, ' class="predict_inputs" name="predict[2]['.$row[7].']" ', $predictedGrade);
                                            }
                                            
                                            // Handling for Subject Teacher + Adjustment Period: 1st Amendment
                                            if(!$isPortfolioSuperAdmin && $isSubjectTeacher && $isAllowEditPeriod1)
                                            {
                                                // Saved as Draft - Display Symbol '^'
                                                if($isSavedAsDraft) {
                                                    $predictedGrade .= '<span class="tabletextrequire">^</span>';
                                                }
                                                
                                                // NOT saved before / Saved as Draft
                                                if(empty($all_adjustment[2]) || $isSavedAsDraft) {
                                                    $predictedGrade .= '<input type="hidden" class="save_draft_inputs" name="canSaveDraft[2]['.$row[7].']" value="1" >';
                                                }
                                            }
                                        }
                                    }
                                }
                                // 2nd Amendment
                                else if($j == 5)
                                {
                                    $isSavedAsDraft = $all_adjustment[3]['PredictType'] == '1';
                                    
                                    /*
                                     *  Amendment Grade Display
                                     *  1. HAS Original Grade
                                     *  2. NOT Saved as Draft  OR  Super Admin  OR  Subject Teacher
                                     */
//                                  (($all_adjustment[2]['PredictedGrade'] != '' && $all_adjustment[2]['PredictType'] != '1') || $isPortfolioSuperAdmin) && 
//                                  ($all_adjustment[3]['PredictType'] != '1' || $isSubjectTeacher))
                                    if($hasOriginalGrade && (!$isSavedAsDraft || $isPortfolioSuperAdmin || $isSubjectTeacher))
                                    {
                                        $predictedGrade = $all_adjustment[3]['PredictedGrade'];
                                        
                                        // Super Admin + Saved as Draft  >  Not display grade
                                        if($isPortfolioSuperAdmin && $isSavedAsDraft) {
                                            $predictedGrade = '';
                                        }
                                        
                                        //if(!$row['isViewOnly'] && !($isSubjectTeacher && !$isAllowEditPeriod1)) {
                                        if(!$row['isViewOnly'])
                                        {
                                            /*
                                             *  Amendment Grade Input Display
                                             *  1. Super Admin
                                             *  2. Subject Teacher + Adjustment Period: 2nd Amendment
                                             */
                                            if($isPortfolioSuperAdmin || ($isSubjectTeacher && $isAllowEditPeriod2))
                                            {
                                                /* 
                                                $thisGradeListArr = $gradeListArr;
                                                if($predictedGrade == '')
                                                {
                                                    if($all_adjustment[2]['PredictType'] != '1' && $all_adjustment[2]['PredictedGrade'] != '') {
                                                        $thisGradeListArr[] = array($all_adjustment[2]['PredictedGrade'], 'unchanged');
                                                    } else if($all_adjustment[1]['PredictedGrade'] != '') {
                                                        $thisGradeListArr[] = array($all_adjustment[1]['PredictedGrade'], 'unchanged');
                                                    }
                                                }
                                                
                                                $predictedGrade = '<input type="text" class="predict_inputs" name="predict[3]['.$row[7].']" value="'.$predictedGrade.'" >';
                                                 */
                                                $predictedGrade = getSelectByArray($gradeListArr, ' class="predict_inputs" name="predict[3]['.$row[7].']" ', $predictedGrade);
                                            }
                                            
                                            // Handling for Subject Teacher + Adjustment Period: 2nd Amendment
                                            if(!$isPortfolioSuperAdmin && $isSubjectTeacher && $isAllowEditPeriod2)
                                            {
                                                // Saved as Draft - Display Symbol '^'
                                                if($isSavedAsDraft) {
                                                    $predictedGrade .= '<span class="tabletextrequire">^</span>';
                                                }
                                                
                                                // NOT saved before / Saved as Draft
                                                if(empty($all_adjustment[3]) || $isSavedAsDraft) {
                                                    $predictedGrade .= '<input type="hidden" class="save_draft_inputs" name="canSaveDraft[3]['.$row[7].']" value="1" >';
                                                }
                                            }
                                        }
                                    }
                                }
                                // Final Adjustment Grade
                                else if($j == 6)
                                {
                                    $predictedGrade = $all_adjustment[4]['PredictedGrade'];
                                    if($hasOriginalGrade && $isPortfolioSuperAdmin)
                                    {
                                        /* 
                                        $thisGradeListArr = $gradeListArr;
                                        if($predictedGrade == '')
                                        {
                                            if($all_adjustment[3]['PredictType'] != '1' && $all_adjustment[3]['PredictedGrade'] != '') {
                                                $thisGradeListArr[] = array($all_adjustment[3]['PredictedGrade'], 'unchanged');
                                            } else if($all_adjustment[2]['PredictType'] != '1' && $all_adjustment[2]['PredictedGrade'] != '') {
                                                $thisGradeListArr[] = array($all_adjustment[2]['PredictedGrade'], 'unchanged');
                                            } else if($all_adjustment[1]['PredictedGrade'] != '') {
                                                $thisGradeListArr[] = array($all_adjustment[1]['PredictedGrade'], 'unchanged');
                                            }
                                        }
                                        
                                        $predictedGrade = '<input type="text" class="predict_inputs" name="predict[4]['.$row[7].']" value="'.$predictedGrade.'" >';
                                         */
                                        $predictedGrade = getSelectByArray($gradeListArr, ' class="predict_inputs" name="predict[4]['.$row[7].']" ', $predictedGrade);
                                    }
                                }
                            }
                            
                            if($predictedGrade != '') {
                                $x .= $this->displayCell($j, $predictedGrade, $css);
                            } else {
                                $x .= $this->displayCell($j, $row[$j], $css);
                            }
                        }
                        $x .= "</tr>\n";
                    }
	            }
	            $x .= "</table>";
	            
	            if($numberOfRows<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
                $this->db_free_result();
                
	            return $x;
	        }
	        
	        function displayFormat_iPortfolio_DBS_Predicted_Grade_IB()
	        {
	            global $PATH_WRT_ROOT, $image_path, $Lang;
	            
	            $i = 0;
	            $this->rs = $this->db_db_query($this->built_sql());
	            
	            $width = ($width!="") ? $width : 560;
	            
	            $n_start = $this->n_start;
	            $x = '';
	            $x .= "<table class='common_table_list_v30 $othercss1'>\n";
	            $x .= "<thead>\n";
	            $x .= $this->displayColumn();
	            $x .= "</thead>";
	            
	            $numberOfRows = $this->db_num_rows();
	            if ($numberOfRows==0)
	            {
	                $x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
	            }
	            else
	            {
	                include_once($PATH_WRT_ROOT."includes/libportfolio.php");
	                include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
	                $lpf = new libportfolio();
	                $lpf_dbs = new libpf_dbs_transcript();
	                
	                // Check if Super Admin
	                $isPortfolioSuperAdmin = $lpf->IS_IPF_SUPERADMIN();
	                
	                // Get all IB Grades
	                $gradeListArr = $lpf_dbs->getCurriculumGrades();
	                $gradeListArr = $gradeListArr['IBDB'];
	                foreach((array)$gradeListArr as $thisIndex => $thisGrade) {
	                    $gradeListArr[$thisIndex] = array($thisGrade, $thisGrade);
	                }
	                array_unshift($gradeListArr, array('NC', $Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['NoChange']));
	                
	                while($row = $this->db_fetch_array())
	                {
	                    $i++;
	                    $css = ($i%2) ? "" : "2";
	                    if($i>$this->page_size) break;
	                    
	                    $x .= "<tr>\n";
	                    $x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
	                    for($j=0; $j<$this->no_col-1; $j++)
	                    {
	                        if($j == 5 && $isPortfolioSuperAdmin && $row['PredictedGradeID'] != '') {
	                            $row[$j] = getSelectByArray($gradeListArr, ' class="predict_inputs" name="predict[0]['.$row['UserID'].']" ', $row[$j]);
	                        }
                            $x .= $this->displayCell($j, $row[$j], $css);
	                    }
	                    $x .= "</tr>\n";
	                }
	            }
	            $x .= "</table>";
	            
	            if($numberOfRows<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
	            $this->db_free_result();
	            
	            return $x;
	        }
	        
	        function displayFormat_eEnrolmentGroupEnrolList($width=""){
		        global $image_path, $i_general_last_modified_by,$sys_custom;
	
	            $i = 0;
	            $this->rs = $this->db_db_query($this->built_sql());
	
	            $width = ($width!="") ? $width : "100%";
	
	            $n_start = $this->n_start;
	            $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	            $x .= $this->displayColumn();
	
	            if ($this->db_num_rows()==0)
	            {
	                    $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	            } else
	            {
		            $liTemp = new libdbtable2007(0,0,0);
	                while ($row = $this->db_fetch_array())
	                {
	                        $i++;
	                        $css = ($i%2) ? "1" : "2";
	                        if ($i>$this->page_size)
	                        {
	                                break;
	                        }
	                        $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                        $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                        for ($j=0; $j<$this->no_col-1; $j++)
	                        {
	                        	$ShowTeacherNum = $sys_custom['eEnrolment']['ShowStudentNickName'] == true? 5: 4;
	                        	if($sys_custom['eEnrolment']['ShowStudentGender']) $ShowTeacherNum++;
	                            if ($j == $ShowTeacherNum)	//status column
	                            {
	                               	//add teacher name after status
	                               	$name_field = getNameFieldByLang();
									$tempSQL = "SELECT $name_field FROM INTRANET_USER where UserID = ".$row[$this->no_col-1];
									$result = $liTemp->returnArray($tempSQL,1);
									if ($result[0][0] == "")
									{
										$x .= $this->displayCell($j, $row[$j]);
									}
									else
									{
										$x .= $this->displayCell($j, $row[$j]." (".$i_general_last_modified_by.": ".$result[0][0].")");
									}
	                            }
	                            else
	                            {
	                            	$x .= $this->displayCell($j, $row[$j]);
	                        	}
	                        }
	                        $x .= "</tr>\n";
	                }
	            }
	            if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	            $x .= "</table>\n";
	            $this->db_free_result();
	
	            return $x;
	        }
	
			function displayFormat_EnrolmentTransferOLE($tablecolor='', $navigationBar=1)
			{
	                global $image_path;
	
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $n_start = $this->n_start;
	                $x .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' bgcolor='#CCCCCC'>\n";
	                $x .= $this->displayColumn();
	                if ($this->db_num_rows()==0)
	                {
	                        $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                } else
	                {
	                        while ($row = $this->db_fetch_array())
	                        {
	                                $i++;
	                                $css = ($i%2) ? "1" : "2";
	                                if ($i>$this->page_size)
	                                {
	                                        break;
	                                }
	                                $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                                $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                                for ($j=0; $j<$this->no_col-1; $j++)
	                                {
	                                        $x .= $this->displayCell($j, $row[$j]);
	                                }
	                                $x .= "</tr>\n";
	                        }
	                }
	                if($this->db_num_rows()<>0 and $navigationBar)
	                	$x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	
	                return $x;
	        }
	
			function displayFormat_eDisciplineDetentionSetting()
			{
				$liTemp = new libdbtable2007(0,0,0);
				$ldiscipline = new libdisciplinev12();
				global $image_path;
				$CurrentDay = date("Y-m-d");
				$CurrentTime = date("H:i", time());
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$width = "97%";
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table width=$width border=0 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
				$x .= $this->displayColumn();
	
				if ($this->db_num_rows()==0){
					$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				}else{
					$TempAllForms = $ldiscipline->getAllFormString();
					$name_field = getNameFieldByLang("USR.");
					while($row = $this->db_fetch_array()) {
						$i++;
						$j=0;
						if($i>$this->page_size) break;
						for($j=1; $j<=$this->no_col-1; $j++) {
							if($j==3){
								$TempData[$j] = "<table class=tbclip><tr><td style=\"border-bottom: none\">".intranet_htmlspecialchars($row[$j])."</td></tr></table>";
							}else if($j==4){
								$tempSQL = "SELECT INTR.LevelName FROM DISCIPLINE_DETENTION_SESSION_CLASSLEVEL LVL, INTRANET_CLASSLEVEL INTR WHERE LVL.ClassLevelID = INTR.ClassLevelID AND LVL.DetentionID = '".$row[0]."' ORDER BY INTR.LevelName";
								$result = $liTemp->returnArray($tempSQL,1);
								$TempClassLevel = "";
								for($k=0;$k<sizeof($result);$k++)
									$TempClassLevel[] = $result[$k][0];
								if (is_array($TempClassLevel)){
									$TempData[$j] = implode(", ",$TempClassLevel);
								} else {
									$TempData[$j] = "";
								}
								if($TempData[$j]==$TempAllForms){
									$TempData[$j] = $this->AllFormsData;
								}
								$TempData[$j] = "<table class=tbclip><tr><td style=\"border-bottom: none\">$TempData[$j]</td></tr></table>";
							}else if($j==5){
								$tempSQL = "SELECT $name_field FROM DISCIPLINE_DETENTION_SESSION_PIC PIC, INTRANET_USER USR WHERE PIC.UserID = USR.UserID AND PIC.DetentionID = '".$row[0]."' ORDER BY $name_field";
								$result = $liTemp->returnArray($tempSQL,1);
								$TempPIC = "";
								for($k=0;$k<sizeof($result);$k++)
									$TempPIC[] = $result[$k][0];
								if (is_array($TempPIC)){
									$TempData[$j] = implode(", ",$TempPIC);
								} else {
									$TempData[$j] = "";
								}
								$TempData[$j] = "<table class=tbclip><tr><td style=\"border-bottom: none\">$TempData[$j]</td></tr></table>";
							}else if($j==7){
								// [2015-0528-0910-00207] fixed: display incorrect assign number under "Session Management > Session"
								// 		ensure only valid arrangement will be counted (RecordStatus = 1)
								//$tempSQL = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DetentionID = '".$row[0]."'";
								$tempSQL = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DetentionID = '".$row[0]."' AND RecordStatus = 1";
								$result = $liTemp->returnArray($tempSQL,1);
								$TempAssigned = $result[0][0];
								$TempData[$j] = "<span id=\"SpanAssigned".$row[0]."\">".$TempAssigned."</span>";
							}else{
								$TempData[$j] = $row[$j];
							}
						}
						//if (($row[1] < $CurrentDay) || (($row[1] == $CurrentDay) && (substr($row[2],0,5) < $CurrentTime))) {
						if (($row[1] < $CurrentDay)) {
							$TempCSS = "row_approved";
						} else if ($row[6] <= $TempAssigned) {
							$TempCSS = "row_suspend";
						} else {
							$TempCSS = "row_avaliable";
						}
						$x .= "<tr class=\"$TempCSS\">\n";
						$x .= "<td>".($n_start+$i)."</td>\n";
						for($k=1; $k<=$this->no_col-1; $k++) {
							if (($k==8)&&($TempCSS=="row_approved")){	// old records cannot be selected for edit and delete
								$x .= $this->displayCell($k, "&nbsp;", "");
							}else{
								$x .= $this->displayCell($k, $TempData[$k], "");
							}
						}
						$x .= "</tr>\n";
					}
				}
					if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tableBottom'>".$this->navigation()."</td></tr>";
					$x .= "</table>\n";
					$this->db_free_result();
					return $x;
			}
	
			function displayFormat_eDisciplineDetentionNewRecord()
			{
				global $image_path, $LAYOUT_SKIN;
				
				$ldiscipline = new libdisciplinev12();
				$CurrentDay = date("Y-m-d");
				$CurrentTime = date("H:i", time());
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$width = "100%";
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table width=$width border=0 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
				$x .= $this->displayColumn();
	
				if ($this->db_num_rows()==0){
					$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				}else{
					$TempAllForms = $ldiscipline->getAllFormString();
					while($row = $this->db_fetch_array()) {
						$i++;
						$TempList = $ldiscipline->getDetentionListByDetentionID($row[5]);
						list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
						
						if($i>$this->page_size) break;
						for($j=0; $j<$this->no_col-1; $j++) {
							if($j==2){
								$TempData[$j] = intranet_htmlspecialchars($row[$j]);
								$TempData[$j] .= ($row[9]!="") ? "<a href=javascript:setClickID(".$row[10].");show_ref_list('remark','spanRemark".$row[10]."')><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' width=20 height=20 align=absmiddle title='Remarks' border=0></a><span id='spanRemark".$row[10]."'>&nbsp;</span>" : "";
							}/*else if($j==3){
								$TempData[$j] = intranet_htmlspecialchars($row[$j]);
							}*/else if($j==4){	# PIC							
								$TempData[$j] = "<table class='tbclip'><tr><td style='border-bottom: none'>".$ldiscipline->getPICNameList($row[$j])."</td></tr></table>";
							}else if($j==5){
								
								if ($TmpForm == $TempAllForms) $TmpForm = $this->AllFormsData;
								$TempData[$j] = $TmpDetentionDate;
							}else if($j==6)
							{
								$TempData[$j] = $TmpPeriod;
							}
							else if($j==7)
							{
								$TempData[$j] = $TmpLocation;
							}
							else if($j==8)
							{
								$TempData[$j] = $TmpPIC;
							}
							else{
								$TempData[$j] = $row[$j];
							}
						}
						$x .= "<tr class=\"row_approved\">\n";
						$x .= "<td>".($n_start+$i)."</td>\n";
						for($k=0; $k<$this->no_col-1; $k++) {
							$x .= $this->displayCell($k, $TempData[$k], "");
						}
						$x .= "</tr>\n";
					}
				}
	//				if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tableTitle'>".$this->navigation()."</td></tr>";
					$x .= "</table>\n";
					$this->db_free_result();
					return $x;
			}
	
			function displayFormat_eDisciplineDetentionDetail()
			{
				global $intranet_root, $image_path, $LAYOUT_SKIN, $sys_custom, $i_Discipline_Arranged, $i_Discipline_Released_To_Student, $i_Discipline_Present, $i_Discipline_Absent;
				
				$ldiscipline = new libdisciplinev12();
				$CurrentDay = date("Y-m-d");
				$CurrentTime = date("H:i", time());
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$width = "100%";
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table width=$width border=0 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
				$x .= $this->displayColumn();
	
				if ($this->db_num_rows()==0){
					$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				}else{
					$TempAllForms = $ldiscipline->getAllFormString();
					while($row = $this->db_fetch_array()) {
						$i++;
						if($i>$this->page_size) break;
						for($j=0; $j<$this->no_col-1; $j++)
						{
							// Student Name
							if($j==1) {
								$TempData[$j] = $row[$j];
								
								// [2016-0823-1255-04240] display Student Photo
								if($sys_custom['eDiscipline']['DetentionSessionDisplayStdPhoto'] && $row[$j]!="" && $row[10]!="")
								{
									// Student Info
									include_once("libuser.php");
									$student_userid = $row[10];
									$student_obj = new libuser($student_userid);
									$student_userlogin = $student_obj->UserLogin;
									
									// User Photo - user login
									$user_login_path = $intranet_root."/file/user_photo/".$student_userlogin.".jpg";
			
									// Personal Photo - userid
									$photoname_uid = "p".$student_userid;
									$uid_path1 = $intranet_root."/file/photo/personal/". $photoname_uid.".jpg";
									$uid_path2 = $intranet_root."/file/photo/personal/". $photoname_uid.".JPG";
			
									// Photo - user login
									if(file_exists($user_login_path)) {
										$img = '<img src="/file/user_photo/'.$student_userlogin.'.jpg" width="100px" height="130px">';
									}
									// Personal Photo - userid
									else if(file_exists($uid_path1)) {
										$img = '<img src="/file/photo/personal/p'.$student_userid.'.jpg" width="100px" height="130px">';
									}
									else if(file_exists($uid_path2)) {
										$img = '<img src="/file/photo/personal/p'.$student_userid.'.JPG" width="100px" height="130px">';
									}
									// Default photo
									else {
										$img = '<img src="/images/myaccount_personalinfo/samplephoto.gif" width="100px" height="130px">';
									}
									$TempData[$j] = "<div align=\"center\">".$img."<br>".$row[$j]."</div>";
								}
							}
							else if($j==2) {
								$TempData[$j] = intranet_htmlspecialchars($row[$j]);
								//$TempData[$j] .= ($row[9]!="") ? "<a href=javascript:setClickID(".$row[10].");show_ref_list('remark','spanRemark".$row[10]."')><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' width=20 height=20 align=absmiddle title='Remarks' border=0></a><span id='spanRemark".$row[10]."'>&nbsp;</span>" : "";
								//$TempData[$j] .= ($row[10]!="") ? "<a href=javascript:setClickID(".$row[11].");show_ref_list('remark','spanRemark".$row[11]."')><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' width=20 height=20 align=absmiddle title='Remarks' border=0></a><span id='spanRemark".$row[11]."'>&nbsp;</span>" : "";
								// [2015-0924-1046-51071]
								$TempData[$j] .= ($row[12]!="") ? "<a href=javascript:setClickID(".$row[13].");show_ref_list('remark','spanRemark".$row[13]."')><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' width=20 height=20 align=absmiddle title='Remarks' border=0></a><span id='spanRemark".$row[13]."'>&nbsp;</span>" : "";
							}
							else if($j==4 || $j==5) {
								$TempData[$j] = ($row[$j]!="") ? "<table class='tbclip'><tr><td style='border-bottom: none'>".$ldiscipline->getPICNameList($row[$j])."</td></tr></table>" : "---";
							}
							else if($j==6) {
								//$TmpNoticeID = $ldiscipline->getNoticeIDByStudentIDDetentionID($row[8], $row[9]);
								// [2015-0924-1046-51071]
								$TmpNoticeID = $ldiscipline->getNoticeIDByStudentIDDetentionID($row[10], $row[11]);
								if ($TmpNoticeID[0] == "") {
									//$TmpImage = "<img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">";
									$TmpImage = "---";
								} else {
									$TmpImage = "<img src=\"$image_path/$LAYOUT_SKIN/icon_to_student.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Released_To_Student\">";
								}
								$TempData[$j] = $TmpImage;
							}
							// [2015-0924-1046-51071] Attendance Status
							else if($j==7) {
								$InfoAttendanceStatus = $row[$j];
								if ($InfoAttendanceStatus == "") {
									$InfoAttendanceStatus = "&nbsp;";
								}
								else if ($InfoAttendanceStatus == "PRE") {
									$InfoAttendanceStatus = $i_Discipline_Present;
								}
								else if ($InfoAttendanceStatus == "ABS") {
									$InfoAttendanceStatus = $i_Discipline_Absent;
								}
								$TempData[$j] = $InfoAttendanceStatus;
							}
							else {
								$TempData[$j] = $row[$j];
							}
						}
						$x .= "<tr class=\"row_approved\">\n";
						$x .= "<td>".($n_start+$i)."</td>\n";
						for($k=0; $k<$this->no_col-1; $k++) {
							$x .= $this->displayCell($k, $TempData[$k], "");
						}
						$x .= "</tr>\n";
					}
				}
					if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tableBottom'>".$this->navigation()."</td></tr>";
					$x .= "</table>\n";
					$this->db_free_result();
					return $x;
			}
	
			function displayFormat_eDisciplineDetentionTakeAttendance()
			{
				$ldiscipline = new libdisciplinev12();
				
				global $PATH_WRT_ROOT, $image_path, $linterface, $LAYOUT_SKIN,$plugin;
				
				$CurrentDay = date("Y-m-d");
				$CurrentTime = date("H:i", time());
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$width = "100%";
				$n_start = $this->n_start;
				$x = '';
	
				# Reason Words
				include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
				$lword = new libwordtemplates();
				$words = $lword->getWordListAttendance();
	//			$hasWord = sizeof($words)!=0;
				$reason_js_array = "";
				$disable_color="#DFDFDF";  # background color for disabled text field
				$enable_color="#FFFFFF";  # background color for enabled text field
	//			if($hasWord) {
					$words_absence = $lword->getWordListAttendance(1);
					$x .= $linterface->CONVERT_TO_JS_ARRAY($words_absence, "jArrayWordsAbsence", 1, 1);
	//			}
	
				$x .= "<table width=$width border=0 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
				$x .= $this->displayColumn();
	
				if ($this->db_num_rows()==0){
					$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				}else{
					$TempAllForms = $ldiscipline->getAllFormString();
					while($row = $this->db_fetch_array()) {
						$i++;
	//					if ($hasWord) {
							$select_word = $linterface->GET_PRESET_LIST("getReasons($row[7])", $row[7], "textAttendanceRemark$row[7]");
	//					}
	
					
						if($i>$this->page_size) break;
						for($j=0; $j<$this->no_col-1; $j++) 
						{
							if ($j==2) 	# Detention Reason
							{
								$TempData[$j] = intranet_htmlspecialchars($row[$j])."&nbsp;";
								$TempData[$j] .= ($row['Remark']!="") ? "<a href=javascript:setClickID(".$row['RecordID'].");show_ref_list('remark','spanRemark".$row['RecordID']."')><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' width=20 height=20 align=absmiddle title='Remarks' border=0></a><span id='spanRemark".$row['RecordID']."'>&nbsp;</span>" : "";
							} 
							else if($j==3 || $j==4) # PIC
							{
								$pic_str = $ldiscipline->getPICNameList($row[$j]);
								$TempData[$j] = $pic_str ? $pic_str : "---";
							} 
							else if($j==6) # Remark
							{
								$TempData[$j] = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"layer_table\"><tr><td>".$row[$j]."</td><td>".$select_word."</td></tr></table><input type='hidden' name='sid[]' id='sid[]' value='".$row[7]."'>";
							} 
							else
							{
								$TempData[$j] = $row[$j];
							}
						}
						if($plugin['eClassTeacherApp']){
							$TempData[10] = $row[10];
						}
						if (strpos($row[$i], "icon_present")) {
							$TempCSS = "row_approved";
						} else if ($row[8] <= $TempAssigned) {
							$TempCSS = "row_suspend";
						}
						$x .= "<tr id=\"tr$row[7]\" class=\"$TempCSS\">\n";
						$x .= "<td>".($n_start+$i)."</td>\n";
						
						for($k=0; $k<$this->no_col-1; $k++) {
							$x .= $this->displayCell($k, $TempData[$k]);
						}
						
						if($plugin['eClassTeacherApp']){
							$x .= $this->displayCell($k+3, $TempData[$k+3]);
						}
						
						$x .= "</tr>\n";
					}
				}
					if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tableBottom'>".$this->navigation()."</td></tr>";
					$x .= "</table>\n";
					$this->db_free_result();
					return $x;
			}
	
			function displayFormat_eDisciplineDetentionList()
			{
				$liTemp = new libdbtable2007(0,0,0);
				$ldiscipline = new libdisciplinev12();
				global $image_path, $i_Discipline_Finished, $i_Discipline_Full, $i_Discipline_Available, $image_path, $LAYOUT_SKIN, $i_Discipline_Take, $i_Discipline_Attended, $i_Discipline_Absent;
				$CurrentDay = date("Y-m-d");
				$CurrentTime = date("H:i", time());
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$width = "100%";
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table width=$width border=0 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
				$x .= $this->displayColumn();
	
				if ($this->db_num_rows()==0){
					$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				}else{
					$TempAllForms = $ldiscipline->getAllFormString();
					$name_field = getNameFieldByLang("USR.");
					while($row = $this->db_fetch_array()) {
						$i++;
						$j=0;
						if($i>$this->page_size) break;
	
						for($j=1; $j<=$this->no_col-1; $j++) {
							if($j==3){
								$TempData[$j] = "<table class=tbclip><tr><td style=\"border-bottom: none\">".intranet_htmlspecialchars($row[$j])."</td></tr></table>";
							}else if($j==4){
								$tempSQL = "SELECT INTR.LevelName FROM DISCIPLINE_DETENTION_SESSION_CLASSLEVEL LVL, INTRANET_CLASSLEVEL INTR WHERE LVL.ClassLevelID = INTR.ClassLevelID AND LVL.DetentionID = '".$row[0]."' ORDER BY INTR.LevelName";
								$result = $liTemp->returnArray($tempSQL,1);
								$TempClassLevel = "";
								for($k=0;$k<sizeof($result);$k++)
									$TempClassLevel[] = $result[$k][0];
								if (is_array($TempClassLevel)){
									$TempData[$j] = implode(", ",$TempClassLevel);
								} else {
									$TempData[$j] = "";
								}
								if($TempData[$j]==$TempAllForms){
									$TempData[$j] = $this->AllFormsData;
								}
								$TempData[$j] = "<table class=tbclip><tr><td style=\"border-bottom: none\">$TempData[$j]</td></tr></table>";
							}else if($j==5){
								$tempSQL = "SELECT $name_field FROM DISCIPLINE_DETENTION_SESSION_PIC PIC, INTRANET_USER USR WHERE PIC.UserID = USR.UserID AND PIC.DetentionID = '".$row[0]."' ORDER BY $name_field";
								$result = $liTemp->returnArray($tempSQL,1);
								$TempPIC = "";
								for($k=0;$k<sizeof($result);$k++)
									$TempPIC[] = $result[$k][0];
								if (is_array($TempPIC)){
									$TempData[$j] = implode(", ",$TempPIC);
								} else {
									$TempData[$j] = "";
								}
								$TempData[$j] = "<table class=tbclip><tr><td style=\"border-bottom: none\">$TempData[$j]</td></tr></table>";
							}else if($j==7){
								$TempData[$j] = "<span id=\"SpanAssigned".$row[0]."\">".$row[$j]."</span>";
							}else if($j==8){
								$resultAttendancePreCount = $ldiscipline->getAttendanceCountByDetentionID($row[0], "PRE");
								
								if ($resultAttendancePreCount == "--") {
									$TakeAttendanceFlag = $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-TakeAttendance");
									if ($TakeAttendanceFlag) {
										$TempData[$j] = "<a href=\"take_attendance.php?did=$row[0]\" class=\"tablelink\"><img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_takeattendance.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> $i_Discipline_Take</a>";
									} else {
										$TempData[$j] = $resultAttendancePreCount;
									}
								} else {
									$resultAttendanceAbsCount = $ldiscipline->getAttendanceCountByDetentionID($row[0], "ABS");
									$TempData[$j] = "$resultAttendancePreCount <img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_present.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" title=\"$i_Discipline_Attended\"> , $resultAttendanceAbsCount <img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" title=\"$i_Discipline_Absent\">";
								}

							}else{
								$TempData[$j] = $row[$j];
							}
						}
	
						//if (($row[1] < $CurrentDay) || (($row[1] == $CurrentDay) && (substr($row[2],0,5) < $CurrentTime))) {
						$tempSQL = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DetentionID = '".$row[0]."' AND RecordStatus=1";
						$result = $liTemp->returnVector($tempSQL);
						$TempAssigned = $result[0];
						//echo $TempAssigned;
						if ($row[6] <= $TempAssigned) {
							$TempCSS = "row_suspend";
							$TempData[9] = "$i_Discipline_Full";
						} else if (($row[1] < $CurrentDay)) {
							$TempCSS = "row_approved record_Waived";
							$TempData[9] = "$i_Discipline_Finished";
						} else {
							$TempCSS = "row_avaliable";
							$TempData[9] = "$i_Discipline_Available";
						}
						$x .= "<tr class=\"$TempCSS\">\n";
						$x .= "<td>".($n_start+$i)."</td>\n";
						for($k=1; $k<=$this->no_col-1; $k++) {
							$x .= $this->displayCell($k, $TempData[$k], "");
						}
						$x .= "</tr>\n";
					}
				}
					if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tableBottom'>".$this->navigation()."</td></tr>";
					$x .= "</table>\n";
					$this->db_free_result();
					return $x;
			}
	
			function displayFormat_eDisciplineDetentionStudentRecord()
			{
				$liTemp = new libdbtable2007(0,0,0);
				$ldiscipline = new libdisciplinev12();
				
				global $image_path, $LAYOUT_SKIN, $i_Discipline_Attended, $i_Discipline_Absent, $i_Discipline_Waiting_For_Arrangement, $i_Discipline_Arranged, $i_Discipline_Released_To_Student, $i_Discipline_Not_Yet, $i_Merit_Punishment;
				
				$CurrentDay = date("Y-m-d");
				$CurrentTime = date("H:i", time());
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$width = "97%";
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table id=tableStudentRecord width=$width border=0 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
				$x .= $this->displayColumn();
	
				if ($this->db_num_rows()==0){
					$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				}else{
					$TempAllForms = $ldiscipline->getAllFormString();
					$name_field = getNameFieldByLang("USR.");
					while($row = $this->db_fetch_array()) {
						$i++;
						if($i>$this->page_size) break;
						for($j=0; $j<$this->no_col-1; $j++) {
							if($j==2){
								$TempData[$j] = intranet_htmlspecialchars($row[$j])."&nbsp;";
								$TempData[$j] .= ($row[13]!="") ? "<a href=javascript:setClickID('".$row[11]."');show_ref_list('remark','spanRemark".$row[11]."')><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' width=20 height=20 align=absmiddle title='Remarks' border=0></a><span id='spanRemark".$row[11]."'>&nbsp;</span>" : "";
							}/*else if($j==3){
								if ($row[$j] == "") {
									$TempData[$j] = "--";
								} else {
									$TempData[$j] = intranet_htmlspecialchars($row[$j]);
								}
							}*/
							else if($j==4 || $j==5){	# Record PIC ("RequestedBy" field) & Created by
								$result = ($row[$j]!="") ? "<table class='tbclip'><tr><td style='border-bottom: none'>".$ldiscipline->getPICNameList($row[$j])."</td></tr></table>" : "---";
								$TempData[$j] = $result;
							} else if($j==6){
								if ($row[$j] == "") {
									$TempData[$j] = "<table class=\"tbclip\"><tr><td style=\"border-bottom: none\"><img src=\"$image_path/$LAYOUT_SKIN/icon_waiting.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Waiting_For_Arrangement\"></td></tr></table>";
								}else{
									$TempList = $ldiscipline->getDetentionListByDetentionID($row[$j]);
									list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
									if ($TmpForm == $TempAllForms) $TmpForm = $this->AllFormsData;
									//$SessionString = substr($TmpDetentionDate,2)." (".substr($TmpDetentionDayName,0,3).") | ".$TmpPeriod." | ".intranet_htmlspecialchars($TmpLocation)." | ".$TmpForm." | ".$TmpPIC;
									$StrArr = array($TmpDetentionDate." (".substr($TmpDetentionDayName,0,3).")",$TmpPeriod,intranet_htmlspecialchars($TmpLocation),$TmpPIC);
									$SessionString = $ldiscipline->displayDetentionSessionString($row[$j]);
									$TmpNoticeID = $ldiscipline->getNoticeIDByStudentIDDetentionID($row[9], $TmpDetentionID);
									if ($TmpNoticeID[0] == "") {
										$TmpImage = "<img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" title=\"$i_Discipline_Arranged\">";
									} else {
										$TmpImage = "<img src=\"$image_path/$LAYOUT_SKIN/icon_to_student.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Released_To_Student\">";
									}
									$TempData[$j] = "<table class=\"tbclip\"><tr><td style=\"border-bottom: none\">$TmpImage $SessionString</td></tr></table>";
								}
							}else if($j==7){
								if ($row[$j] == "PRE") {
									$TempData[$j] = "<img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_present.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Attended\">";
								} else if ($row[$j] == "ABS") {
									$TempData[$j] = "<img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Absent\">";
								} else if ($row[6] == "") {		// Waiting for arrangement
									$TempData[$j] = "--";
								} else {
									$TempData[$j] = "$i_Discipline_Not_Yet";
								}
								$TempData[$j] .= $row[14]!="" ? "<a href=\"javascript:show_demerit_list('demerit','spanDemerit".$row[11]."','".$row[14]."')\"><img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_demerit.gif\" width=\"20\" height=\"20\" title=\"$i_Merit_Punishment\" border=0></a><span id='spanDemerit".$row[11]."'></span>" : "";
							}else{
								if ($row[$j] == "") {
									$TempData[$j] = "&nbsp;";
								} else {
									$TempData[$j] = $row[$j];
								}
							}
						}
						if ($row[6] == "") {
							$TempCSS = "row_waiting";
						} else if ($row[7] == "PRE") {
							$TempCSS = "row_approved record_Waived";
						} else if ($row[7] == "ABS") {
							$TempCSS = "row_suspend";
						} else {
							$TempCSS = "row_approved";
						}
						
						$x .= "<tr id='".$row[11]."' class=\"$TempCSS\">\n";
						$x .= "<td>".($n_start+$i)."</td>\n";
						for($k=0; $k<$this->no_col-1; $k++) {
							
							if (($k==6)&&($TempCSS=="row_approved")){	// grey text
								$x .= $this->displayCell($k, $TempData[$k], "", " class=\"tabletextremark\" ");
							}else{
								if($k == 6){
									$x .= $this->displayCell($k, $TempData[$k], "", "");
								} else {
									$x .= $this->displayCell($k, $TempData[$k], "", "");
								}
							}
						}
						$x .= "</tr>\n";
					}
				}
					if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tableBottom'>".$this->navigation()."</td></tr>";
					$x .= "</table>\n";
					$this->db_free_result();
					return $x;
			}
			
			function displayFormat_eDisciplineDetentionStudentRecordInStudentView()
			{
				$liTemp = new libdbtable2007(0,0,0);
				$ldiscipline = new libdisciplinev12();
				
				global $image_path, $LAYOUT_SKIN, $i_Discipline_Attended, $i_Discipline_Absent, $i_Discipline_Waiting_For_Arrangement, $i_Discipline_Arranged, $i_Discipline_Released_To_Student, $i_Discipline_Not_Yet, $i_Merit_Punishment;
				
				$CurrentDay = date("Y-m-d");
				$CurrentTime = date("H:i", time());
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$width = "97%";
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table id=tableStudentRecord width=$width border=0 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
				$x .= $this->displayColumn();
	
				if ($this->db_num_rows()==0){
					$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				}else{
					$TempAllForms = $ldiscipline->getAllFormString();
					$name_field = getNameFieldByLang("USR.");
					while($row = $this->db_fetch_array()) {
						$i++;
						if($i>$this->page_size) break;
						for($j=0; $j<$this->no_col-1; $j++) {
							if($j==2){
								$TempData[$j] = intranet_htmlspecialchars($row[$j])."&nbsp;";
								$TempData[$j] .= ($row[3]!="") ? "<a href=javascript:setClickID('".$row[10]."');show_ref_list('remark','spanRemark".$row[10]."')><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' width=20 height=20 align=absmiddle title='Remarks' border=0></a><span id='spanRemark".$row[10]."'>&nbsp;</span>" : "";
							}
							else if($j==5){
								if ($row[$j] == "") {
									$TempData[$j] = "<table class=\"tbclip\"><tr><td style=\"border-bottom: none\"><img src=\"$image_path/$LAYOUT_SKIN/icon_waiting.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Waiting_For_Arrangement\"></td></tr></table>";
								}else{
									$TempList = $ldiscipline->getDetentionListByDetentionID($row[$j]);
									list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
									if ($TmpForm == $TempAllForms) $TmpForm = $this->AllFormsData;
									
									$StrArr = array($TmpDetentionDate." (".substr($TmpDetentionDayName,0,3).")",$TmpPeriod,intranet_htmlspecialchars($TmpLocation),$TmpPIC);
									$SessionString = $ldiscipline->displayDetentionSessionString($row[$j]);
									$TmpNoticeID = $ldiscipline->getNoticeIDByStudentIDDetentionID($row[9], $TmpDetentionID);
									if ($TmpNoticeID[0] == "") {
										$TmpImage = "<img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" title=\"$i_Discipline_Arranged\">";
									} else {
										$TmpImage = "<img src=\"$image_path/$LAYOUT_SKIN/icon_to_student.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Released_To_Student\">";
									}
									$TempData[$j] = "<table class=\"tbclip\"><tr><td style=\"border-bottom: none\">$TmpImage $SessionString</td></tr></table>";
								}
							}else if($j==6){
								if ($row[$j] == "PRE") {
									$TempData[$j] = "<img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_present.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Attended\">";
								} else if ($row[$j] == "ABS") {
									$TempData[$j] = "<img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Absent\">";
								} else if ($row[6] == "") {		// Waiting for arrangement
									$TempData[$j] = "--";
								} else {
									$TempData[$j] = "$i_Discipline_Not_Yet";
								}
								$TempData[$j] .= $row[14]!="" ? "<a href=\"javascript:show_demerit_list('demerit','spanDemerit".$row[11]."','".$row[14]."')\"><img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_demerit.gif\" width=\"20\" height=\"20\" title=\"$i_Merit_Punishment\" border=0></a><span id='spanDemerit".$row[11]."'></span>" : "";
							}else{
								if ($row[$j] == "") {
									$TempData[$j] = "&nbsp;";
								} else {
									$TempData[$j] = $row[$j];
								}
							}
						}
						if ($row[6] == "") {
							$TempCSS = "row_waiting";
						} else if ($row[7] == "PRE") {
							$TempCSS = "row_approved record_Waived";
						} else if ($row[7] == "ABS") {
							$TempCSS = "row_suspend";
						} else {
							$TempCSS = "row_approved";
						}
						
						$x .= "<tr id='".$row[11]."' class=\"$TempCSS\">\n";
						$x .= "<td>".($n_start+$i)."</td>\n";
						for($k=0; $k<$this->no_col-1; $k++) {
							
							if (($k==6)&&($TempCSS=="row_approved")){	// grey text
								$x .= $this->displayCell($k, $TempData[$k], "", " class=\"tabletextremark\" ");
							}else{
								if($k == 6){
									$x .= $this->displayCell($k, $TempData[$k], "", "");
								} else {
									$x .= $this->displayCell($k, $TempData[$k], "", "");
								}
							}
						}
						$x .= "</tr>\n";
					}
				}
					if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tableBottom'>".$this->navigation()."</td></tr>";
					$x .= "</table>\n";
					$this->db_free_result();
					return $x;
			}
	
	        function displayFormat_meritType($flag, $meritType)
	        {
	            global $image_path, $i_notapplicable;
	            $ldiscipline = new libdisciplinev12();
	            
	            //debug_r($meritType);
	            $i = 0;
	            $this->rs = $this->db_db_query($this->built_sql());
	
	            $width = ($width!="") ? $width : "100%";
	
	            $n_start = $this->n_start;
	            $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	            $x .= $this->displayColumn();
	            
	            $goURL = ($flag == "Demerit") ? "punish_approval_edit.php" : "award_approval_edit.php";
	            
	            if ($this->db_num_rows()==0)
	            {
	                    //$x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                    for($i=0;$i<sizeof($meritType);$i++) {
		                    if($meritType[$i][1] != '') {
								$css = ($i%2) ? "1" : "2";
			                 	$x .= "<tr class='table{$tablecolor}row{$css}'><td>{$i}</td><td><a href={$goURL}?merit={$meritType[$i][2]} class='tablelink'>{$meritType[$i][0]}</a></td><td>0</td><td><input type=checkbox name=merit[] value={$meritType[$i][2]}></td></tr>";   
		                 	}
	                    }
	                    //$x .= "";
	            } else
	            {
	                    //while ($row = $this->db_fetch_array())
	                    //{
	                    $row = $this->db_fetch_array();
	                        $i++;
		                    for($i=0;$i<=sizeof($meritType);$i++) {
			                    if($meritType[$i][1] != '') {
		                            $css = ($i%2) ? "1" : "2";
		                            if ($i>$this->page_size)
		                            {
		                                    break;
		                            }
		                            
		                            $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
		                            $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
		                            for ($j=0; $j<$this->no_col-1; $j++)
		                            {	
			                            if($j==0) {	# 1st column of content
			                                $x .= "<td><a class='tablelink' href='{$goURL}?merit={$meritType[$i][2]}'>{$meritType[$i][0]}</a></td>";
		                                } else {
			                                $m = $meritType[$i][2];
			                                if($j==1) { 
			                                	$sql2 = "SELECT ApproveNum FROM DISCIPLINE_MERIT_TYPE_SETTING WHERE MeritType=$m";
			                                	$temp = $ldiscipline->returnVector($sql2);
			                                	
			                                	$x .= ($temp[0] != '') ? "<td>{$temp[0]}</td>" : "<td>{$i_notapplicable}</td>";	
		                            		}
		                            		else if($j==2) {
		                            			$sql2 = "SELECT g.Name FROM DISCIPLINE_MERIT_TYPE_SETTING s LEFT OUTER JOIN DISCIPLINE_AP_APPROVAL_GROUP g ON (g.GroupID=s.ApprovalGroupID) WHERE s.MeritType=$m";
		                            			$temp2 = $ldiscipline->returnVector($sql2);
		                            			$x .= "<td>".$temp2[0]."</td>";
		                            		}
		                            		else if($j==3) {	# 3rd column (checkbox)
			                            		$x .= "<td><input type=checkbox name=merit[] value={$meritType[$i][2]}></td>";
		                            		} else {
			                            		$x .= $this->displayCell($j, $row[$j]);
		                            		}
		                                }
		                            }
		                            $x .= "</tr>\n";
		                    	}
	                		}
	                    //}
	            }
	            $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>&nbsp;</td></tr>";
	            $x .= "</table>\n";
	            $this->db_free_result();
	
	            return $x;
	        }
	        
	        #marcus 0308
	        function displayFormat_AwardPunishPromotion($fieldAry, $meritType, $mtype)
	        {
	   			$liTemp = new libdbtable2007(0,0,0);
				$ldiscipline = new libdisciplinev12();
				
				$method = $ldiscipline->getPromotionMethod($mtype);
				
				if($mtype==1) {	# merit
					$meritDemerit[] = 1;
					$meritDemerit[] = 2;
					$meritDemerit[] = 3;
					$meritDemerit[] = 4;
					$meritDemerit[] = 5;
				}
				else {			# demerit
					$meritDemerit[] = -1;
					$meritDemerit[] = -2;
					$meritDemerit[] = -3;
					$meritDemerit[] = -4;
					$meritDemerit[] = -5;
				}
				
				if($method=="category") {
					for($i=0;$i<sizeof($meritType);$i++) {
						if($meritType[$i]!=0) {$tempType[] = $meritDemerit[$i];}
					}
				} else {
					for($i=0;$i<sizeof($meritType);$i++) {
						if($meritType[$i]!=0 && $i!=0) {$tempType[] = $meritDemerit[$i];}
					}
				}
				
	            global $image_path;
	            $i = 0;
	            $this->rs = $this->db_db_query($this->built_sql());
	
	            $width = ($width!="") ? $width : "100%";
	
	            $n_start = $this->n_start;
	            $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	            $x .= $this->displayColumn();
	            
	            if ($this->db_num_rows()==0)
	            {
						$x .= "<tr class='table{$tablecolor}row2'>";
						$k = sizeof($this->field_array)+2;
						
						for($i=0;$i<$k;$i++) {
							if($i==0) 
								$x .= "<td>1</td>";
							else if($i==$k-1)
								$x .= "<td><input type=checkbox name='CatID[]' value='9999999999'></td>";
							else 
								$x .= "<td class='tabletext'>-</td>";	
						}
						
						$x .= "</tr>";
	            } else
	            {
	                    while ($row = $this->db_fetch_array())
	                    {
	                            $i++;
	                            $css = ($i%2) ? "1" : "2";
	                            if ($i>$this->page_size)
	                            {
	                                    break;
	                            }
	                            $x .= "<tr class='table{$tablecolor}row{$css}'>\n";		# each <tr>
	                            $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";	
	                            for ($j=0; $j<$this->no_col-1; $j++)					# each <td>
	                            {
		                         	if($method=="category") {
			                        	if($j!=0 & $j!=$fieldAry && $j!=$fieldAry-1) {	# 1st column of content
		                                	$x .= "<td class='tabletext'>";
										
			                                $sql2 = "SELECT UpgradeNum FROM DISCIPLINE_MERIT_PROMOTION_BY_CATEGORY WHERE CatID=$row[$j] AND MeritType=$tempType[$j]";
			                                $temp = $liTemp->returnVector($sql2);
			                                
			                                $x .= ($temp[0]!='') ? $temp[0] : "-";
	                                
		                                	$x .= "</td>";
		
		                                } else {
			                                $x .= $this->displayCell($j, $row[$j]);
		                                }
	                                } else {
	  		                        	if($j!=$fieldAry && $j!=$fieldAry-1) {	# last column of content
										
			                                $x .= "<td class='tabletext'>";
			                            	$sql2 = "SELECT UpgradeNum FROM DISCIPLINE_MERIT_TYPE_SETTING WHERE MeritType=$tempType[$j]";
			                            	$temp = $liTemp->returnVector($sql2);
			                            	$x .= ($temp[0]=='' || $temp[0]==0) ? "-" : $temp[0];
		                                	$x .= "</td>";
		                                } else {
			                                $x .= $this->displayCell($j, $row[$j]);
		                                }
		                            }
	
	                            }
	                            $x .= "</tr>\n";
	                    }
	            }
	   			if($method=="category") {
		            if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
				} else {
		            if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>&nbsp</td></tr>";
	            }
	            $x .= "</table>\n";
	            $this->db_free_result();
	
	            return $x;
	        }
	        
	        function displayFormat_Split_PIC($Section="", $col="", $statusLoc="", $checkbox="", $actionLoc="", $recordIDLoc="", $imgPath, $historyLoc="", $caseIDLocation="", $waivedByLoc="", $noticeIDLocation="", $waiveDayLocation="", $categoryItemLocation="", $remarkLocation="", $newleaf="", $MeritCountLocation="", $conductMarkLocation="", $noticeText="", $pushMsgLocation="", $RehabilLocation="", $NameLocation="", $homeworkMsgLocation="", $SubScoreLocation="", $SubScore2Location="", $FollowUpRemarkLocation="")
	        {
		        global $i_Discipline_System_Discipline_Case_Record_Case, $i_Discipline_System_Award_Punishment_Pending, $i_Discipline_System_Award_Punishment_Waived, $i_general_Days, $iDiscipline, $eDiscipline;
		        global $i_Discipline_System_Award_Punishment_Rejected, $i_Discipline_Released_To_Student, $i_Discipline_System_Award_Punishment_Approved;
		        global $i_Discipline_System_Award_Punishment_Detention, $i_Discipline_System_Award_Punishment_Send_Notice;
		        global $plugin, $sys_custom, $Lang, $ldiscipline;
		        // [2015-0611-1642-26164]
		        global $CurrentPageArr;
		        
	   			$liTemp = new libdbtable2007(0,0,0);
				//$ldiscipline = new libdisciplinev12();
	
				//$layerMove = "";
				$i = 0;
				
	            $this->rs = $this->db_db_query($this->built_sql());
	            
	            $width = ($width!="") ? $width : "100%";
				
				$pendingImg = "<img src='{$imgPath}/icon_waiting.gif' width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Pending\" border=0>";
				$rejectImg = "<img src='{$imgPath}/icon_reject_l.gif' width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Rejected \n[_BY_]\" border=0>";
				$releasedImg = "<img src='{$imgPath}/icon_to_student.gif' width=20 height=20 align=absmiddle title=\"$i_Discipline_Released_To_Student \n[_BY_]\" border=0>";
				$approvedImg = "<img src='{$imgPath}/icon_approve_b.gif' width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Approved \n[_BY_]\" border=0>";
				$waivedImg = "<img src='{$imgPath}/icon_waived.gif' width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Waived \n[_BY_]\" border=0>";
				$remarksImg = "<img src='{$imgPath}/icon_remark.gif' width=20 height=20 align=absmiddle title=\"".$iDiscipline['Remarks']."\" border=0>";
				
				$hasCaseViewRight = $ldiscipline->CHECK_FUNCTION_ACCESS("Discipline-MGMT-Case_Record-View");
				// [2015-0416-1040-06164] Hide eNotice link for Student Prefect
				$notDisplayLink = $sys_custom['eDiscipline']['add_AP_records_student_prefect'] && !$CurrentPageArr['eServiceeDisciplinev12'] && $ldiscipline->isStudentPrefect();
				
	            $n_start = $this->n_start;
	            $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	            $x .= $this->displayColumn("tabletop");
	            if ($this->db_num_rows()==0)
	            {
	                    $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext td_no_record' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	            }
	            else
	            {
	                    while ($row = $this->db_fetch_array())
	                    {
	                            $i++;
								
	                            switch ($row[$statusLoc])
								{
									case 0: $css = "class='row_waiting'"; break;
									case 1: $css = "class='row_approved'"; break;
									case 2: $css = "class='row_approved record_Waived'"; break;
									case -1: $css = "class='row_suspend'"; break;
									default: $css = "class='row_approved'"; break;
								}
								
	                            if ($i>$this->page_size)
	                            {
	                                    break;
	                            }
	                            # $row[12];
	                            $x .= "<tr {$css}>\n";
	                            $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                            
	                            for ($j=0; $j<$this->no_col-1; $j++)
	                            {
		                            if($j==$col) {	# PICID field 
		                                //$x .= "<td><table class='tbclip'><tr><td style='border-bottom: none'>";
	                                    $x .= "<td><table class=''><tr><td style='border-bottom: none'>";
	                                    $picList = $ldiscipline->getPICNameList($row[$j]);
	                            		$x .= $picList ? $picList : "---";
	                                    $x .= "</td></tr></table></td>";
	                                    $pic = $row[$j];
									}
									else if($j==$actionLoc) {		# Action Column 	# Modified by Marcus 14/09/2009 (Add Condition to skip send notice in student mode and parent mode)
		                            	$x .= "<td>";
		                            	if($row[$j] != '' && $row[$j] != 0) # TemplateID != NULL / 0
		                            	{	# NoticeID != NULL / 0
		                            		$lc = new libucc($row[$noticeIDLocation]);
		                            		$result = $lc->getNoticeDetails();
		                            		$url = $result['url']."&StudentID=".$row['UserID'];
		                            		if($noticeText=="") $noticeText = $i_Discipline_System_Award_Punishment_Send_Notice;
											
											// [2015-0416-1040-06164] Hide eNotice link for Student Prefect
		                            		//$x .= ($row[$noticeIDLocation] != '' && $row[$j] != 0) ? "<a href='javascript:;' onClick=\"newWindow('{$url}',1)\">{$noticeText}</a>" : $noticeText;
		                            		$x .= ($row[$noticeIDLocation] != '' && $row[$j] != 0 && !$notDisplayLink) ? "<a href='javascript:;' onClick=\"newWindow('{$url}',1)\">{$noticeText}</a>" : $noticeText;
		                            	}
		                            	else {
			                            	$x .= "";	
		                            	}
		                            	
		                            	// Display "Send Push Message" if PushMessageID is set [2014-1216-1347-28164]
		                            	if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage'] && $pushMsgLocation){
		                            		$withPushMsg = $row[$pushMsgLocation] && $row[$pushMsgLocation] != '' && $row[$pushMsgLocation] != 0 && $row[$pushMsgLocation] != -1;
		                            		if($withPushMsg){
		                            			$x .= ($row[$j] != '' && $row[$j] != 0) ? "<br>" : "";
		                            			$x .= $Lang['eDiscipline']['DetentionMgmt']['SendPushMessage'];
		                            		}
		                            	}
		                            	
		                            	// [2015-0611-1642-26164] Display "Rehabilitation Program" if $withRehabilSetting is set 
		                            	$withRehabilSetting = false;
		                            	if($sys_custom['eDiscipline']['CSCProbation'] && $RehabilLocation!=""){
		                            		// get Rehabilitation PIC
		                            		$withRehabilSetting = isset($row[$RehabilLocation]) && $row[$RehabilLocation]!='';
		                            		
		                            		// with Rehabilitation PIC
		                            		if($withRehabilSetting){
		                            			$x .= (($row[$j] != '' && $row[$j] != 0) || $withPushMsg)? "<br>" : "";
		                            			
		                            			// eService - only show title
		                            			if($CurrentPageArr['eServiceeDisciplinev12']) {
		                            				$x .= $Lang['eDiscipline']['CWCRehabil']['Title'];
		                            			} 
		                            			// eAdmin - allow display info layer
		                            			else {
		                            				$x .= "<a href='javascript:;' onClick=\"changeClickID('r{$row[$recordIDLoc]}');moveObject('show_detailr{$row[$recordIDLoc]}', true, 280);showResult('r{$row[$recordIDLoc]}','rehabil');MM_showHideLayers('show_detailr{$row[$recordIDLoc]}','','show');\">{$Lang['eDiscipline']['CWCRehabil']['Title']}</a><div onload=\"changeClickID('r{$row[$recordIDLoc]}');moveObject('show_detailr{$row[$recordIDLoc]}', true);\" id='show_detailr{$row[$recordIDLoc]}' style='display:none; position:absolute; width:280px; height:70px; z-index:-1;'></div>";
		                            				$y .= "document.onload=moveObject('show_detailr{$row[$recordIDLoc]}', true);";
		                            				$jsRehabilAry .= ($jsRehabilAry != '') ? "\",\"r".$row[$recordIDLoc] : "\"r".$row[$recordIDLoc];
		                            			}
		                            		}
		                            	}
		                            	
		                            	if($Section=="Award_Punishment")
											$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DemeritID=$row[$recordIDLoc] AND RecordStatus=1";
										else 
											$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE GMID=$row[$recordIDLoc] AND RecordStatus=1";
											
										//$gm_sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE GMID=$row[$recordIDLoc]";
											
		                            	$detentionCount = $liTemp->returnVector($sql);
		                            	/*
		                            	if($detentionCount[0]==0)
		                            	{
			                            	$detentionCount = $liTemp->returnVector($gm_sql);
		                            	}
		                            	*/
		                            	if($detentionCount[0]!=0) {
		                            		// [2014-1216-1347-28164]
		                            		// [2015-0611-1642-26164]
		                            		if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage'] && $pushMsgLocation){
		                            			$x .= (($row[$j] != '' && $row[$j] != 0) || $withPushMsg || $withRehabilSetting) ? "<br>" : "";
		                            		} 
		                            		else {
			                            		$x .= (($row[$j] != '' && $row[$j] != 0) || $withRehabilSetting) ? "<br>" : "";
		                            		}
                 	
											if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View")) {
			                            		$x .= "<a href='javascript:;' onClick=\"changeClickID({$row[$recordIDLoc]});moveObject('show_detail{$row[$recordIDLoc]}', true, 280);showResult({$row[$recordIDLoc]},'detention');MM_showHideLayers('show_detail{$row[$recordIDLoc]}','','show');\">{$i_Discipline_System_Award_Punishment_Detention}</a><div onload=\"changeClickID({$row[$recordIDLoc]});moveObject('show_detail{$row[$recordIDLoc]}', true);\" id='show_detail{$row[$recordIDLoc]}' style='display:none; position:absolute; width:280px; height:180px; z-index:-1;'></div>";
		                            		}
		                            		else {
			                            		$x .= $i_Discipline_System_Award_Punishment_Detention;
		                            		}
		                            		$y .= "document.onload=moveObject('show_detail{$row[$recordIDLoc]}', true);";
		                            		$jsDetentionAry .= ($jsDetentionAry != '') ? ",".$row[$recordIDLoc] : $row[$recordIDLoc];
	                            		}
		                                
		                                // [2016-0823-1255-04240] Display Cancel Request message
										if($row[$homeworkMsgLocation] == 1)
										{
											$tableStyle = "";
		                            		if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage'] && $pushMsgLocation){
		                            			if(!(($row[$j] == "" || $row[$j] == 0) && !$withPushMsg && !$withRehabilSetting && $detentionCount[0] == 0))
		                            			{
													$x .= "<br>";
													$tableStyle = " style=\"padding-top: 2px;\"";
		                            			}
		                            		} 
		                            		else if(!(($row[$j] == "" || $row[$j] == 0) && !$withRehabilSetting && $detentionCount[0] == 0)) {
												$x .= "<br>";
												$tableStyle = " style=\"padding-top: 2px;\"";
		                            		}
											
											$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"systemmsg\" $tableStyle>\n";
											$x .= "<tr> \n";
												$x .= "<td nowrap='nowrap' style=\"border: 0\">";
													$x .= "<font color=\"green\">".$Lang['eDiscipline']['CancelRequestMessage']."</font>";
													$x .= "&nbsp;";
													if($row[$homeworkMsgLocation+1] > 0)
													{
														$x .= "<br>";
														$x .= "<font color=\"green\">".$Lang['eDiscipline']['CancelAPNotice']."</font>";
														$x .= "&nbsp;";
													}
												$x .= "</td>\n";
											$x .= "</tr>\n";
											$x .= "</table>\n";
										}

										// [2020-0824-1151-40308]
										$displayedFollowUpRemark = false;
                                        if($sys_custom['eDiscipline']['APFollowUpRemarks'] && $row[$FollowUpRemarkLocation] != '')
                                        {
                                            $tableStyle = "";
                                            if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage'] && $pushMsgLocation){
                                                if(!(($row[$j] == "" || $row[$j] == 0) && !$withPushMsg && !$withRehabilSetting && $detentionCount[0] == 0 && !$row[$homeworkMsgLocation])) {
                                                    $x .= "<br>";
                                                    $tableStyle = " style=\"padding-top: 2px;\"";
                                                }
                                            }
                                            else if(!(($row[$j] == "" || $row[$j] == 0) && !$withRehabilSetting && $detentionCount[0] == 0 && !$row[$homeworkMsgLocation])) {
                                                $x .= "<br>";
                                                $tableStyle = " style=\"padding-top: 2px;\"";
                                            }

                                            $x .= "<a href='javascript:;' onClick=\"changeClickID({$row[$recordIDLoc]});moveObject('show_follow_up_remark{$row[$recordIDLoc]}', true, 280);showResult({$row[$recordIDLoc]},'follow_up_remark');MM_showHideLayers('show_follow_up_remark{$row[$recordIDLoc]}','','show');\">".$Lang['eDiscipline']['FollowUpRemarkShortForm']."</a><div onload=\"changeClickID({$row[$recordIDLoc]});moveObject('show_follow_up_remark{$row[$recordIDLoc]}', true);\" id='show_follow_up_remark{$row[$recordIDLoc]}' style='display:none; position:absolute; width:280px; height:180px; z-index:-1;'></div>";
                                            $y .= "document.onload=moveObject('show_follow_up_remark{$row[$recordIDLoc]}', true);";
                                            $jsFollowUpRemarkAry .= ($jsFollowUpRemarkAry != '') ? ",".$row[$recordIDLoc] : $row[$recordIDLoc];

                                            $displayedFollowUpRemark = true;
                                        }
	                            		
	                            		// Display "Send Push Message" if PushMessageID is set [2014-1216-1347-28164]
	                            		// [2015-0611-1642-26164]
	                            		if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage'] && $pushMsgLocation){
                                            //$x .= (($row[$j] == "" || $row[$j] == 0) && !$withPushMsg && !$withRehabilSetting && $detentionCount[0]==0 && !$row[$homeworkMsgLocation]) ? "---" : "";
	                            			$x .= (($row[$j] == "" || $row[$j] == 0) && !$withPushMsg && !$withRehabilSetting && $detentionCount[0] == 0 && !$row[$homeworkMsgLocation] && !$displayedFollowUpRemark) ? "---" : "";
	                            		} 
	                            		else{
                                            //$x .= (($row[$j] == "" || $row[$j] == 0) && !$withRehabilSetting && $detentionCount[0]==0 && !$row[$homeworkMsgLocation]) ? "---" : "";
		                            		$x .= (($row[$j] == "" || $row[$j] == 0) && !$withRehabilSetting && $detentionCount[0] == 0 && !$row[$homeworkMsgLocation] && !$displayedFollowUpRemark) ? "---" : "";
	                            		}

                                        // [2020-0824-1151-40308]
	                            		// if(!$row[$homeworkMsgLocation]){
                                        if(!$row[$homeworkMsgLocation] && !$displayedFollowUpRemark){
	                            			$x .= "&nbsp";
                                        }
		                            	$x .= "</td>";
	                                }
	                                else if($j==$historyLoc) {	# Reference column
		                                $x .= "<td>";
		                                if($row[$historyLoc]==1) {
			                                $x .= "<a href='javascript:;' onClick=\"changeClickID({$row[$recordIDLoc]});moveObject('show_history{$row[$recordIDLoc]}', true, 300);showResult({$row[$recordIDLoc]},'history');MM_showHideLayers('show_history{$row[$recordIDLoc]}','','show');\">".$eDiscipline["HISTORY"]."</a><div onload=\"changeClickID({$row[$recordIDLoc]});moveObject('show_history{$row[$recordIDLoc]}', true);\" id='show_history{$row[$recordIDLoc]}' style='display:none; position:absolute; width:280px; height:180px; z-index:-1;'></div>";
			                                $jsHistoryAry .= ($jsHistoryAry != '') ? ",".$row[$recordIDLoc] : $row[$recordIDLoc];
		                                }
		                                $x .= ($row[$historyLoc]==1 && $row[$caseIDLocation] != 0) ? "<br>" : "";
		                                
		                                if($row[$caseIDLocation])
		                                {
			                                # check case has access right
			                                if($hasCaseViewRight)
			                                {
				                                # retrieve case number
				                                $case_info = $ldiscipline->getCaseRecordByCaseID($row[$caseIDLocation]);
			                            		$x .= "<a href=/home/eAdmin/StudentMgmt/disciplinev12/management/case_record/view.php?CaseID=".$row[$caseIDLocation].">".$i_Discipline_System_Discipline_Case_Record_Case." ". $case_info[0]['CaseNumber'] ."</a>";
		                            		}
			                            	else
			                            	{
			                            		$x .= $i_Discipline_System_Discipline_Case_Record_Case;
		                            		}
		                            	}
		                                $x .= ($row[$historyLoc]!=1 && $row[$caseIDLocation] == 0) ? "---" : "";										
		                                $x .= "</td>";
	                                }
	                                else if($j==$checkbox) {		# checkbox field
	                                	// PHP 5.4
	                                	// Deprecated: Function split() is deprecated in /home/web/eclass40/intranetIP25/includes/libdbtable2007a.php on line 7017
		                                //$ary = split(',', $pic);
		                                $ary = explode(',', $pic);
		                                $own = 0;
		                                
	                                    $x .= "<td align=center>";
	                                    for($m=0;$m<sizeof($ary);$m++) {
		                                 	$own = ($ary[$m]==$_SESSION['UserID'] || $own==1) ? 1 : 0;
	                                    }
		                                $x .= $row[$j];   
	                                    $x .= "</td>";
	                            	}
	                            	else if($j==$waiveDayLocation && $waiveDayLocation!="") {	# waive day column
	                            		if($newleaf==1) {
		                            		if($sys_custom['eDisciplinev12_Munsang_NewLeafScheme']) {
			                            		# -12 : cannot have minor offence records on following 12 days; -1 : if the waive pass date = today, it should display "0 day(s)", rather than "1 day(s)"
			                            		$x .= "<td>".(($ldiscipline->countSchoolDayDiff($row[15], date('Y-m-d'))-12)-1)." $i_general_Days</td>";
		                            		}
		                            		else {
				                            	$catid = $row[$j];
				                            	$x .= "<td>".$ldiscipline->countWaivePassedDate($catid, $row[4], $row[13], $row[12])."</td>";
			                            	}
		                            	}
	                            	}
	                            	else if($j==$conductMarkLocation && $conductMarkLocation!="") {
		                            	if($ldiscipline->Display_ConductMarkInAP) {
			                            	$conMark = ($row[$j]!='') ? $row[$j] : 0;
//			                            	if($conMark != 0) {
//			                            		$x .= "<td>";
//	                            				$x .= "<a href='javascript:;' onClick=\"changeClickID('c{$row['UserID']}'); moveObject('show_conduct_c{$row['UserID']}', true, 280); showResult('c{$row['UserID']}', 'conduct'); MM_showHideLayers('show_conduct_c{$row['UserID']}', '', 'show');\">{$conMark}</a><div onload=\"changeClickID('c{$row['UserID']}'); moveObject('show_conduct_c{$row['UserID']}', true);\" id='show_conduct_c{$row['UserID']}' style='display:none; position:absolute; width:280px; height:70px; z-index:-1;'></div>";
//	                            				$x .= "</td>";
//	                            				$y .= "document.onload=moveObject('show_conduct_c{$row['UserID']}', true);";
//		                            			$jsConductAry .= ($jsConductAry != '') ? "\",\"c".$row["UserID"] : "\"c".$row["UserID"];
//			                            	}
//			                            	else {
			                            		$x .= "<td>$conMark</td>";
//			                            	}	
		                            	}
	                                }
	                                else if ($j==$SubScoreLocation && $SubScoreLocation!="") {		# Study Score
		                            	if($ldiscipline->UseSubScore) {
			                            	$subMark = ($row[$j]!='') ? my_round($row[$j], 1) : 0;
		                            		$x .= "<td>$subMark</td>";
		                            	}
	                                }
	                                else if ($j==$SubScore2Location && $SubScore2Location!="") {	# Activity Score
		                            	if($ldiscipline->UseActScore) {
			                            	$actMark = ($row[$j]!='') ? my_round($row[$j], 1) : 0;
		                            		$x .= "<td>$actMark</td>";
		                            	}
	                                }
	                                else if($j==$waivedByLoc) {	# status column
	                                	if($waiveDayLocation!="") {		# GM index page
		                                	$sql = "SELECT RecordStatus FROM DISCIPLINE_ACCU_RECORD WHERE RecordID=$row[$recordIDLoc]";
		                                	$result = $liTemp->returnArray($sql,1);
	                                	}
	                                	else {						# AP index page
		                                	$sql = "SELECT RecordStatus, ReleaseStatus FROM DISCIPLINE_MERIT_RECORD WHERE RecordID=$row[$recordIDLoc]";
		                                	$result = $liTemp->returnArray($sql,2);
	                                	}
	                                	
	                                	$x .= "<td>";
	                                	
	                                	# pending
										$x .= ($result[0][0] == DISCIPLINE_STATUS_PENDING) ? $pendingImg : "";
										
										# released
										$ReleasedBy = $row['ReleasedBy']==-1 ? $Lang['eDiscipline']['RecordisAutoReleased'] : $ldiscipline->getPICNameList($row['ReleasedBy']);
										$ReleasedBy .= "\n".$row['ReleasedDate'];
										$x .= ($result[0][1] == DISCIPLINE_STATUS_RELEASED) ? str_replace("[_BY_]",$ReleasedBy,$releasedImg) : "";
										
										# rejected
										$RejectedBy = $ldiscipline->getPICNameList($row['RejectedBy']);
										$RejectedBy .= "\n".$row['RejectedDate'];
										$x .= ($result[0][0] == DISCIPLINE_STATUS_REJECTED) ? str_replace("[_BY_]",$RejectedBy,$rejectImg) : "";
										
										# approved
										$ApprovedBy = $row['ApprovedBy']==-1 ? $Lang['eDiscipline']['RecordisAutoApproved'] : $ldiscipline->getPICNameList($row['ApprovedBy']);
										$ApprovedBy .= "\n".$row['ApprovedDate'];
										$x .= ($result[0][0] == DISCIPLINE_STATUS_APPROVED) ? str_replace("[_BY_]",$ApprovedBy,$approvedImg) : "";
										
										# waived
										if($result[0][0] == DISCIPLINE_STATUS_WAIVED) {
											$WaivedBy = $ldiscipline->getPICNameList($row['WaivedBy']);
											$WaivedBy .= "\n".$row['WaivedDate'];
											$waivedImg_t = str_replace("[_BY_]",$WaivedBy,$waivedImg);
		                            		$x .= "<a href='javascript:;' onClick=\"changeClickID({$row[$recordIDLoc]});moveObject('show_waive{$row[$recordIDLoc]}', true);showResult({$row[$recordIDLoc]},'waived');MM_showHideLayers('show_waive{$row[$recordIDLoc]}','','show');\">{$waivedImg_t}</a><div onload=\"changeClickID({$row[$recordIDLoc]});moveObject('show_waive{$row[$recordIDLoc]}', true);\" id='show_waive{$row[$recordIDLoc]}' style='display:none; position:absolute; width:280px; height:180px; z-index:-1;'></div>";
		                            		$y .= "document.onload=moveObject('show_waive{$row[$recordIDLoc]}', true);";
		                            		$jsWaiveAry .= ($jsWaiveAry != '') ? ",".$row[$recordIDLoc] : $row[$recordIDLoc];
										}
										
										$x .= "</td>";
									}
									else if($j==$categoryItemLocation) {		# category/item column
										$x .= "<td>$row[$categoryItemLocation]";
										if($row[$remarkLocation] != "")
										{
											$x .= "&nbsp;";
											if($row[$waiveDayLocation]==1) {	# "LATE" record
												$x .= "(".$row[$remarkLocation].")";
											}
											else {
												$x .= "<a href='javascript:;' onClick=\"changeClickID({$row[$recordIDLoc]}); moveObject('show_remark".$row[$recordIDLoc]."', true); showResult({$row[$recordIDLoc]},'record_remark'); MM_showHideLayers('show_remark".$row[$recordIDLoc]."','','show');\">";
												$x .= $remarksImg;
												$x .= "</a>";
											}
											$x .= "<div onload=\"changeClickID({$row[$recordIDLoc]}); moveObject('show_remark{$row[$recordIDLoc]}', true);\" id='show_remark{$row[$recordIDLoc]}' style='display:none; position:absolute; width:320px; height:150px; z-index:-1;'></div>";
																
											$jsRemarkAry .= ($jsRemarkAry != '') ? ",".$row[$recordIDLoc] : $row[$recordIDLoc];
										}
										$x .= "&nbsp;</td>";
									}
									else if($j==$MeritCountLocation) {		# Merit/Demrit count
										$x .= "<td>". $ldiscipline->TrimDeMeritNumber_00($row[$j]) ."&nbsp;</td>";
									}
									else if($j===$NameLocation) {	# Name (Cust: added link redirect to Personal Report) [2015-0807-1048-01073]
										// get ClassName and StudentID 
										$currentClass = explode(" - ", $row[0]); 
										$currentClass = $currentClass[0];
										$currentStudentID = $row['UserID'];
										
		                            	$x .= "<td><a href='javascript:;' onClick=\"redirectToReport('$currentClass', '$currentStudentID')\">".$row[$NameLocation]."</a></td>";
									}
									else {
		                                $x .= $this->displayCell($j, $row[$j]);
	                                }
	                            }
	                            $x .= "</tr>\n";
	                    }
	            }
	            if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	
	            $x .= "</table>\n";
	            $this->db_free_result();
	            
	            if($y != "") {
		         	$y = "<script languahe='javascript'>window.onload = pageOnLoad;function pageOnLoad() {".$y."}</script>";
	            }
                //if($jsDetentionAry != '' || $jsWaiveAry != '' || $jsHistoryAry != '' || $jsRemarkAry != '' || ($sys_custom['eDiscipline']['CSCProbation'] && $jsRehabilAry!="")) {
	            if($jsDetentionAry != '' || $jsWaiveAry != '' || $jsHistoryAry != '' || $jsRemarkAry != '' || ($sys_custom['eDiscipline']['CSCProbation'] && $jsRehabilAry != "") || ($sys_custom['eDiscipline']['APFollowUpRemarks'] && $jsFollowUpRemarkAry != "")) {
		         	$y .= "<script language=javascript>";
		         	$y .= ($jsDetentionAry != '') ? "var jsDetention=[{$jsDetentionAry}];" : "";
		         	$y .= ($jsWaiveAry != '') ? "var jsWaive=[{$jsWaiveAry}];" : "";
		         	$y .= ($jsHistoryAry != '') ? "var jsHistory=[{$jsHistoryAry}];" : "";
					$y .= ($jsRemarkAry != '') ? "var jsRemark=[{$jsRemarkAry}];\n" : "";
					// [2015-0611-1642-26164]
					if($sys_custom['eDiscipline']['CSCProbation']){
						$y .= ($jsRehabilAry != "") ? "var jsRehabil=[{$jsRehabilAry}\"];\n" : "";
					}
                    // $y .= ($jsConductAry!="")? "var jsConduct=[{$jsConductAry}\"];\n" : "";
                    // [2020-0824-1151-40308]
                    if($sys_custom['eDiscipline']['APFollowUpRemarks']){
                        $y .= ($jsFollowUpRemarkAry != "") ? "var jsFollowUpRemark=[{$jsFollowUpRemarkAry}];\n" : "";
                    }
		         	$y .= "</script>";
	            }
	            
	            $returnValue = $x.$y;
	
	            return $returnValue;
	        }
	        
			# [Po Leung Kuk C W Chu College] Start
	        function displayFormat_Split_PIC_Rehabilitation($Section="", $col="", $statusLoc="", $imgPath, $categoryItemLocation="", $MeritCountLocation="", $DateLocation="", $ModifiedLocation="", $RemarkLocation="", $RecordIDLocation="")
	        {
				
//		        global $i_Discipline_System_Discipline_Case_Record_Case, $i_Discipline_System_Award_Punishment_Pending, $i_Discipline_System_Award_Punishment_Waived, $i_general_Days, $iDiscipline, $eDiscipline;
//		        global $i_Discipline_System_Award_Punishment_Rejected, $i_Discipline_Released_To_Student, $i_Discipline_System_Award_Punishment_Approved;
//		        global $i_Discipline_System_Award_Punishment_Detention, $i_Discipline_System_Award_Punishment_Send_Notice;
		        global $plugin, $sys_custom, $iDiscipline, $ldiscipline, $Lang;
		        global $CurrentPageArr;
		        
	   			$liTemp = new libdbtable2007(0,0,0);
				//$ldiscipline = new libdisciplinev12();
	
				//$layerMove = "";
				$i = 0;

	            $this->rs = $this->db_db_query($this->built_sql());
	            
	            $width = ($width!="") ? $width : "100%";
				
				$pendingImg = "<img src='{$imgPath}/icon_waiting.gif' width=20 height=20 align=absmiddle title=\"[CONTENT] \n[_BY_]\" border=0>";
				$rejectImg = "<img src='{$imgPath}/icon_reject_l.gif' width=20 height=20 align=absmiddle title=\"[CONTENT] \n[_BY_]\" border=0>";
				$approvedImg = "<img src='{$imgPath}/icon_approve_b.gif' width=20 height=20 align=absmiddle title=\"[CONTENT] \n[_BY_]\" border=0>";
				$remarksImg = "<img src='{$imgPath}/icon_remark.gif' width=20 height=20 align=absmiddle title=\"".$iDiscipline['Remarks']."\" border=0>";
				
//				$hasCaseViewRight = $ldiscipline->CHECK_FUNCTION_ACCESS("Discipline-MGMT-Case_Record-View");
				
	            $n_start = $this->n_start;
	            $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	            $x .= $this->displayColumn("tabletop");
	            
	            if ($this->db_num_rows()==0){
	                    $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext td_no_record' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	            } 
	            else {
	        
                    while ($row = $this->db_fetch_array())
                	{
                        $i++;
						
						// row css
                        switch ($row[$statusLoc])
						{
							case 0: $css = "class='row_waiting'"; break;
							case 1: $css = "class='row_suspend'"; break;
							case 2: $css = "class='row_waiting'"; break;
							case 3: $css = "class='row_approved'"; break;
							default: $css = "class='row_suspend'"; break;
						}
						
                        if ($i>$this->page_size) {
                                break;
                        }
                        
                        $x .= "<tr {$css}>\n";
                        $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
                        
                        for ($j=0; $j<$this->no_col-1; $j++)
                        {
                            if($j==$col) {	# PICID field 
                                $x .= "<td><table class='tbclip'><tr><td style='border-bottom: none'>";
                                $picList = $ldiscipline->getPICNameList($row[$j]);
                        		$x .= $picList ? $picList : "---";
                                $x .= "</td></tr></table></td>";
                                $pic = $row[$j];
							} 
                            else if($j==$statusLoc) {	# status column
                            	
                            	$x .= "<td>";
                            	switch ($row[$statusLoc])
								{
									case 0: 
										$content_str = str_replace("[CONTENT]",$Lang['eDiscipline']['CWCRehabil']['Status']['WaitingApproval'],$pendingImg);
										break;
									case 1: 
										$content_str = str_replace("[CONTENT]",$Lang['eDiscipline']['CWCRehabil']['Status']['Processing'],$rejectImg);
									 	break;
									case 2:
										$content_str = str_replace("[CONTENT]",$Lang['eDiscipline']['CWCRehabil']['Status']['Processing'],$pendingImg);
										break;
									case 3: 
										$content_str = str_replace("[CONTENT]",$Lang['eDiscipline']['CWCRehabil']['Status']['Processing'],$approvedImg);
									 	break;
									default: 
										$content_str = str_replace("[CONTENT]",$Lang['eDiscipline']['CWCRehabil']['Status']['Incomplete'],$rejectImg);
										break;
								}
								
								$currentPICs = $row[$ModifiedLocation]!=""? $ldiscipline->getPICNameList($row[$ModifiedLocation]) : " ";
								$x .= str_replace("[_BY_]", $currentPICs, $content_str);

								$x .= "</td>";
							} 
							else if($j==$categoryItemLocation) {		# category/item column
								$x .= "<td>$row[$categoryItemLocation]";
								if($row[$RemarkLocation] != "") {
									$x .= "&nbsp;";
									$x .= "<a href='javascript:;' onClick=\"changeClickID({$row[$RecordIDLocation]}); moveObject('show_remark".$row[$RecordIDLocation]."', true); showResult({$row[$RecordIDLocation]},'record_remark'); MM_showHideLayers('show_remark".$row[$RecordIDLocation]."','','show');\">";
									$x .= $remarksImg;
									$x .= "</a>";
									$x .= "<div onload=\"changeClickID({$row[$RecordIDLocation]}); moveObject('show_remark{$row[$RecordIDLocation]}', true);\" id='show_remark{$row[$RecordIDLocation]}' style='display:none; position:absolute; width:320px; height:150px; z-index:-1;'></div>";
														
									$jsRemarkAry .= ($jsRemarkAry != '') ? ",".$row[$RecordIDLocation] : $row[$RecordIDLocation];
								}
								$x .= "&nbsp;</td>";
							} 
							else if($j==$MeritCountLocation) {		# Merit/Demrit count
								$x .= "<td>". $ldiscipline->TrimDeMeritNumber_00($row[$j]) ."&nbsp;</td>";
							} 
							else {
                                $x .= $this->displayCell($j, $row[$j]);
                            }
                        }
                        $x .= "</tr>\n";
               		}
	            }
	            if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	
	            $x .= "</table>\n";
	            $this->db_free_result();
	            
	            if($y != "") {
		         	$y = "<script language='javascript'>window.onload = pageOnLoad;function pageOnLoad() {".$y."}</script>";
	            }
	            if($jsRemarkAry != '') {
		         	$y .= "<script language=javascript>";
					$y .= ($jsRemarkAry != '') ? "var jsRemark=[{$jsRemarkAry}];\n" : "";
		         	$y .= "</script>";
	            }
	            
	            $returnValue = $x.$y;
	
	            return $returnValue;
	        }
			# [Po Leung Kuk C W Chu College] End
	        
			function displayFormat_Case_Record($imgPath="", $templateIDLoc="", $noticeIDLoc="", $recordIDLoc="", $statusLoc="", $meritArr="")
	        {	
		        global $i_Discipline_System_Award_Punishment_Pending, $i_Discipline_Released_To_Student, $i_Discipline_System_Award_Punishment_Approved, $i_Discipline_System_Award_Punishment_Rejected,$i_Discipline_System_Award_Punishment_Waived, $eDiscipline;
		        global $i_Discipline_System_Award_Punishment_Detention, $i_Discipline_System_Award_Punishment_Send_Notice,$i_general_na;
		        
	   			$liTemp = new libdbtable2007(0,0,0);
				$ldiscipline = new libdisciplinev12();
	
				$i = 0;
	            $this->rs = $this->db_db_query($this->built_sql());
	
	            $width = ($width!="") ? $width : "100%";
	
				$pendingImg = "<img src={$imgPath}/icon_waiting.gif width=20 height=20 align=absmiddle title=\"". $i_Discipline_System_Award_Punishment_Pending ."\" border=0>";
				$rejectImg = "<img src={$imgPath}/icon_reject_l.gif width=20 height=20 align=absmiddle title=". $i_Discipline_System_Award_Punishment_Rejected ." border=0>";
				$releasedImg = "<img src={$imgPath}/icon_to_student.gif width=20 height=20 align=absmiddle title=\"". $i_Discipline_Released_To_Student."\" border=0>";
				$approvedImg = "<img src={$imgPath}/icon_approve_b.gif width=20 height=20 align=absmiddle title=". $i_Discipline_System_Award_Punishment_Approved ." border=0>";
				$waivedImg = "<img src={$imgPath}/icon_waived.gif width=20 height=20 align=absmiddle title=". $i_Discipline_System_Award_Punishment_Waived ." border=0>";
	
	            $n_start = $this->n_start;
	            $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	            $x .= $this->displayColumn();
	            if ($this->db_num_rows()==0)
	            {
	                    $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	            } else
	            {
	                    while ($row = $this->db_fetch_array())
	                    {
	                            $i++;
	
	                            switch ($row[$statusLoc])
								{
									case 0: $css = "class='row_waiting'"; break;
									case 1: $css = "class='row_approved'"; break;
									case 2: $css = "class='row_approved record_Waived'"; break;
									case -1: $css = "class='row_suspend'"; break;
									default: $css = "class='row_approved'"; break;
								}
								// echo $this->page_size;
	                            if ($i>$this->page_size)
	                            {
	                                    break;
	                            }
	                            # $row[12];
	                            $x .= "<tr {$css}>\n";
	                            $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                            for ($j=0; $j<$this->no_col-1; $j++)
	                            {
		                            if($j==5) {				# Action Column
		                            	$x .= "<td>";
		                            	if($row[$templateIDLoc] != '' && $row[$templateIDLoc] != 0) {	# TemplateID != NULL / 0
			                            	# NoticeID != NULL / 0
											$lc = new libucc($row[$noticeIDLoc]);
		                            		$result = $lc->getNoticeDetails();
		                            		$url = $result['url']."&StudentID=".$row['UserID'];
		                            		//$url = "/home/notice/sign.php?NoticeID={$row[9]}";
	
			                            	$x .= ($row[$noticeIDLoc] != '' && $row[$noticeIDLoc] != 0) ? "<a href='javascript:;' onClick=\"newWindow('{$url}',1)\">". $eDiscipline["SendNotice"] ."</a>" : $eDiscipline["SendNotice"];
		                            	} else {
			                            	$x .= "";	
		                            	}
		                            	
		                            	$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DemeritID=$row[$recordIDLoc]";
		                            	$detentionCount = $liTemp->returnVector($sql);
		                            	if($detentionCount[0]!=0) {
			                            	$x .= ($row[$templateIDLoc] != '' && $row[$templateIDLoc] != 0) ? "<br>" : "";
											if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View")) {
			                            		$x .= "<a href='javascript:;' onClick=\"changeClickID({$row[$recordIDLoc]});moveObject('show_detail{$row[$recordIDLoc]}', true);showResult({$row[$recordIDLoc]},'detention');MM_showHideLayers('show_detail".$row[$recordIDLoc]."','','show');\">{$i_Discipline_System_Award_Punishment_Detention}</a><div id='show_detail".$row[$recordIDLoc]."' style='position:absolute; width:280px; height:180px; z-index:-1;'></div>";
		                            		} else {
			                            		$x .= $i_Discipline_System_Award_Punishment_Detention;
		                            		}
		                            		$y .= "document.onload=moveObject('show_detail{$row[$recordIDLoc]}', true);";
		                            		$jsDetentionAry .= ($jsDetentionAry != '') ? ",".$row[$recordIDLoc] : $row[$recordIDLoc];
		                            		//$x .= "<a href='javascript:;' onClick=\"MM_showHideLayers('show_detail".$row[$recordIDLoc]."','','show')\">Detention</a>";
		                            		//$x .= $ldiscipline->getDetentionLayer($row[$recordIDLoc], $imgPath);
	                            		}
		                            	$x .= ($row[$templateIDLoc] == "" && $row[$templateIDLoc] == 0 && $detentionCount[0]==0) ? "---" : "";
		                            	
		                            	$x .= "&nbsp;</td>";
	                                    
	                                } else if($j==7) {	# status column
	                                	$sql = "SELECT RecordStatus, ReleaseStatus FROM DISCIPLINE_MERIT_RECORD WHERE RecordID=$row[$recordIDLoc]";
	                                	$result = $liTemp->returnArray($sql,2);
	                                	
	                                	$x .= "<td>";
										$x .= ($result[0][0] == DISCIPLINE_STATUS_PENDING) ? $pendingImg : "";
										$x .= ($result[0][0] == DISCIPLINE_STATUS_REJECTED) ? $rejectImg : "";
										$x .= ($result[0][1] == DISCIPLINE_STATUS_RELEASED) ? $releasedImg : "";
										$x .= ($result[0][0] == DISCIPLINE_STATUS_APPROVED) ? $approvedImg : "";
										if($result[0][0] == DISCIPLINE_STATUS_WAIVED) {
		                            		$x .= "<a href='javascript:;' onClick=\"changeClickID({$row[$recordIDLoc]});moveObject('show_waive{$row[$recordIDLoc]}', true);showResult({$row[$recordIDLoc]},'waived');MM_showHideLayers('show_waive".$row[$recordIDLoc]."','','show');\">".$waivedImg."</a><div onload='changeClickID({$row[$recordIDLoc]});moveObject('show_waive{$row[$recordIDLoc]}', true);' id='show_waive".$row[$recordIDLoc]."' style='position:absolute; width:280px; height:180px; z-index:-1;'></div>";
		                            		$y .= "document.onload=moveObject('show_waive{$row[$recordIDLoc]}', true);";
		                            		$jsWaiveAry .= ($jsWaiveAry != '') ? ",".$row[$recordIDLoc] : $row[$recordIDLoc];
	//										$x .= "<a href=javascript:; onClick=\"MM_showHideLayers('show_waive".$row[$recordIDLoc]."','','show')\">".$waivedImg."</a>";
	//										$x .= $ldiscipline->getWaivedInfo($row[$recordIDLoc], $imgPath);
										}
										$x .= "</td>";
									} else if($j==3) {
										$merit = $row[12]+5;
										$thisrow = $ldiscipline->returnDeMeritStringWithNumber($row[13], $meritArr[$merit]);
										/*
										if($row[13]>0)
											$x .= "<td>{$row[13]} {$meritArr[$merit]}</td>";
										else
											$x .= "<td>--</td>";
										*/
										$x .= "<td>". $thisrow ."</td>";
		                            }
		                            else if($j==4) {
			                            $thisRow = $row[$j] ? $row[$j] : "--";
										$x .= $this->displayCell($j, intranet_htmlspecialchars($thisRow)); 
		                            }
		                             else {
		                                $x .= $this->displayCell($j, $row[$j]);
	                                }
	                            }
	                            $x .= "</tr>\n";
	                    }
	            }
	            if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	
	            $x .= "</table>\n";
	            $this->db_free_result();
	
	            if($y != "") {
		         	$y = "<script languahe='javascript'>window.onload = pageOnLoad;function pageOnLoad() {".$y."}</script>";
	            }
	            if($jsDetentionAry != '' || $jsWaiveAry != '') {
		         	$y .= "<script language=javascript>";
		         	$y .= ($jsDetentionAry != '') ? "var jsDetention=[{$jsDetentionAry}];" : "";
		         	$y .= ($jsWaiveAry != '') ? "var jsWaive=[{$jsWaiveAry}];" : "";
		         	$y .= "</script>";
	            }
	            
	            $returnValue = $x.$y;
	
	            return $returnValue;
	        }
	
	        
	        
	        function displayFormat_Number_Item($meritType)
	        {
	            global $image_path, $sys_custom;
	            $ldiscipline = new libdisciplinev12();
	            
	            $i = 0;
	            $this->rs = $this->db_db_query($this->built_sql());
	
	            $width = ($width!="") ? $width : "100%";
	
	            $n_start = $this->n_start;
	            $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	            $x .= $this->displayColumn();
	            if ($this->db_num_rows()==0)
	            {
	                    $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	            } else
	            {
	                    while ($row = $this->db_fetch_array())
	                    {
	                            $i++;
	                            $css = ($i%2) ? "1" : "2";
	                            if ($i>$this->page_size)
	                            {
	                                    break;
	                            }
	                            $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                            $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                            for ($j=0; $j<$this->no_col-1; $j++)
	                            {
		                            $catid = $row[5];		#	CatID
		                            if($j==1) {	# 1st column of content
		                            	$goURL = ($meritType==1) ? "award_category_item_item_list.php" : "punish_category_item_item_list.php";
		                            	
		                            	$sql = "SELECT COUNT(ItemID) FROM DISCIPLINE_MERIT_ITEM WHERE CatID=$catid AND (RecordStatus IS NULL OR RecordStatus!=-1)";
		                            	$result = $ldiscipline->returnVector($sql);
	                                    $x .= "<td><a class='tablelink' href='{$goURL}?CatID={$catid}'>{$result[0]}</a> </td>";
	                                } else if($j==0) {
		                            	if(($sys_custom['sdbnsm_disciplinev12_monthly_ap_report_cust'] && ($catid==3 || $catid==20)) || ($sys_custom['tsk_disciplinev12_class_summary'] && ($catid==20 || $catid==21 || $catid==22 || $catid==13 || $catid==14 || $catid==15 || $catid==17 || $catid==18))) {
			                            	$asterisk = "<span class='tabletextrequire'>*</span>";
		                            	} else {
							$asterisk = "";
										}
		                                $x .= "<td>{$row[$j]} $asterisk</td>";
	                            	}else {
		                                $x .= $this->displayCell($j, $row[$j]);
	                                }
	                            }
	                            $x .= "</tr>\n";
	                    }
	            }
	            if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	            $x .= "</table>\n";
	            $this->db_free_result();
	
	            return $x;
	        }
	        
	        /*function displayFormat_eDisciplineConductMarkAdjust()
			{
	                global $image_path,$linterface,$lf,$i_Discipline_Apply_To_All;
	
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $width = ($width!="") ? $width : "100%";
	
	                $n_start = $this->n_start;
	                $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	                $x .= $this->displayColumnCustom();
	                
	                #prepare reason array for master reason selection
	                $word_array = $lf->word_array;
					$merit_wording = $lf->getWordListMerit();
					$demerit_wording = $lf->getWordListDemerit();
					for($a=0;$a<sizeof($merit_wording);$a++)
					{
						$reasonArr[] = array($merit_wording[$a],$merit_wording[$a]);	
					}
					for($a=0;$a<sizeof($demerit_wording);$a++)
					{
						$reasonArr[] = array($demerit_wording[$a],$demerit_wording[$a]);	
					}
	                $reasonSelection = $linterface->GET_SELECTION_BOX($reasonArr, 'id="master_reason" name="master_reason" onChange="change_reason(this.selectedIndex)"','');
	                # master roll
	                $x .="<tr class=\"tablebottom\">
						<td colspan=\"5\" >$i_Discipline_Apply_To_All &gt;&gt;</td>
						<td class=\"tabletext\"><input type=\"text\" id=\"master_adjust\" name=\"master_adjust\" onChange=\"change_adjust(this.value)\"></td>
						<td>&nbsp;</td>
						<td>$reasonSelection</td>
						</tr>";
					$tablecolor = "green";
	                if ($this->db_num_rows()==0)
	                {
	                        $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                } else
	                {
	                        while ($row = $this->db_fetch_array())
	                        {
	                                $i++;
	                                $css = ($i%2) ? "1" : "2";
	                                if ($i>$this->page_size)
	                                {
	                                        break;
	                                }
	                                $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                                $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                                for ($j=0; $j<$this->no_col-1; $j++)
	                                {
		                                if($j==3)
		                                {
			                                $liTemp = new libdbtable2007(0,0,0);
			                                $sql = "select count(*) from DISCIPLINE_MERIT_RECORD 
			                                		where studentid=".$row['UserID']." and year='".$row['Year']."' and semester='".$row['Semester']."' ";
			                                $result = $liTemp->returnVector($sql,1);
			                             
			                                $semester_data = getSemesters();
											$number = sizeof($semester_data);
											if($number>0)
											{
												for ($i=0; $i<$number; $i++)
												{
													$linedata[]= split("::",$semester_data[$i]);
												}
											}
											
											for($ct=0;$ct<sizeof($linedata);$ct++)
											{
												if($row['Semester']==$linedata[$ct][0])
												{
													$semesterNum = $ct;
													break;
												}
											}
			                                if($result[0]!=0 && $row[$j]!=0)
			                                {
				                                $score = floor($row[$j]/$result[0]);
			                                }
			                                else
			                                {
				                             	$score = $row[$j];
			                                }
			                                if($j==3)
			                                { 
		                                		
		                                		$row[$j] = "<a href=\"javascript:Show_Adj_Window('".$row['UserID']."','".$row['Year']."','".$semesterNum."')\" class=\"tablelink\">
			                                <span id=\"adj_".$row['UserID'].$row['Year'].$semesterNum."\">
			                                ".$score."</a></span>";
		                                	}
			                                
			                             	
		                                }
		                                if($j==5)
		                                {
		                                
			                             	$row[$j] = $row['ConductScore']+$score;
			                             	
		                                }
	                                        $x .= $this->displayCell($j, $row[$j]);
	                                }
	                                $x .= "</tr>\n";
	                                
	                                
	                                
	                        }
	                }
	                
	                $x .= "</table>\n";
	                $this->db_free_result();
	
	                return $x;
	        }*/
	        function displayFormat_eDisciplineConductMarkAdjust()
			{
	                global $image_path,$linterface,$lf,$i_Discipline_Apply_To_All;
	                global $sys_custom;
	                
					$liTemp = new libdbtable2007(0,0,0);
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $width = ($width!="") ? $width : "100%";
	
	                $n_start = $this->n_start;
	                $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	                $x .= $this->displayColumnCustom();
	                
	                #prepare reason array for master reason selection
	                $word_array = $lf->word_array;
					$merit_wording = $lf->getWordListMerit();
					$demerit_wording = $lf->getWordListDemerit();
					for($a=0;$a<sizeof($merit_wording);$a++)
					{
						$reasonArr[] = array($merit_wording[$a],$merit_wording[$a]);	
					}
					for($a=0;$a<sizeof($demerit_wording);$a++)
					{
						$reasonArr[] = array($demerit_wording[$a],$demerit_wording[$a]);	
					}
	                $reasonSelection = $linterface->GET_SELECTION_BOX($reasonArr, 'id="master_reason" name="master_reason" onBlur="change_reason(this.value)" onChange="change_reason(this.value)"','');
	                # master roll
	                $x .="<tr class=\"tablebottom\">
						<td colspan=\"5\" >$i_Discipline_Apply_To_All &gt;&gt;</td>
						<td class=\"tabletext\"><input type=\"text\" id=\"master_adjust\" name=\"master_adjust\" onChange=\"change_adjust(this.value)\"></td>
						<td>&nbsp;</td>
						<td>$reasonSelection</td>
						</tr>";
					$tablecolor = "green";
	                if ($this->db_num_rows()==0)
	                {
	                        $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                } else
	                {
	                        while ($row = $this->db_fetch_array())
	                        {
	                                $i++;
	                                $score=''; 		
	                                $css = ($i%2) ? "1" : "2";
	                                if ($i>$this->page_size)
	                                {
	                                        break;
	                                }
	                                $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                                $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                                for ($j=0; $j<$this->no_col-1; $j++)
	                                {
		                                if($row['YearTermID']=='' || $row['YearTermID']=='0')
		                                {
			                                 $Semester='';
			                                 $cond = ' and IsAnnual = 1';
		                                }
		                                else
		                                {
			                             	$Semester = $row['YearTermID'];
			                             	$cond = ' and YearTermID = \''.$Semester.'\'';
		                                }
		                                /*
		                                $sql = "select ConductScore, GradeChar, Semester from DISCIPLINE_STUDENT_CONDUCT_BALANCE where 
			                             			StudentID = ".$row['UserID']." and Year = '".$row['Year']."'
			                             			and Semester = '".$Semester."' $cond ";
			                             */
			                             $sql = "select ConductScore, GradeChar, YearTermID from DISCIPLINE_STUDENT_CONDUCT_BALANCE where 
			                             			StudentID = ".$row['UserID']." and AcademicYearID = '".$row['AcademicYearID']."'
			                             			$cond ";
			                             	$tmpArr = $liTemp->returnArray($sql,1);
			                             	
			                             	$ConductScore = $tmpArr[0]['ConductScore'];
			                             	$Grade = $tmpArr[0]['GradeChar'];
			                             	
			                            $ldiscipline = new libdisciplinev12();
				                        $BaseConductScore = $ldiscipline->getConductMarkRule('baseMark');
			                             	
				                        /*
			                            $semester_data = getSemesters();
										$number = sizeof($semester_data);
										if($number>0)
										{
											for ($ct=0; $ct<$number; $ct++)
											{
												$linedata[]= split("::",$semester_data[$ct]);
											}
										}
										
										for($ct=0;$ct<sizeof($linedata);$ct++)
										{
											if($row['Semester']==$linedata[$ct][0])
											{
												$semesterNum = $ct;
												break;
											}
										}
										*/
										
										$semesterNum = $row['YearTermID'];
										
										if($j==2)
		                                {
			                             	if($ConductScore!='0' && $ConductScore!='')
			                             	{
				                             	/*
				                             	$sql = "select count(*) from DISCIPLINE_MERIT_RECORD 
			                                		where studentid=".$row['UserID']." and year='".$row['Year']."' and semester='".$row['Semester']."'
			                                		and RecordStatus!= ".DISCIPLINE_STATUS_PENDING;
			                                	*/
			                                	$sql = "select count(*) from DISCIPLINE_MERIT_RECORD 
			                                		where studentid=".$row['UserID']." and AcademicYearID='".$row['AcademicYearID']."' and YearTermID='".$Semester."'
			                                		and RecordStatus= ".DISCIPLINE_STATUS_APPROVED." AND ReleaseStatus=".DISCIPLINE_STATUS_RELEASED;
			                                	$tmpArr = $liTemp->returnVector($sql,1);
			                                	$hasMeritRecord = $tmpArr[0];
			                                	
			                                	// [2016-0808-1526-52240] return misconduct record with conduct score update
			                                	$hasGMConductRecord = false;
			                                	if($sys_custom['eDiscipline']['GMConductMark'])
			                                	{
			                                		$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_RECORD 
															WHERE 
															StudentID=".$row['UserID']." AND AcademicYearID='".$row['AcademicYearID']."' AND YearTermID='".$semesterNum."' AND 
															RecordType=".MISCONDUCT." AND RecordStatus= ".DISCIPLINE_STATUS_APPROVED." AND (GMConductScoreChange > 0 || GMConductScoreChange < 0)";
				                                	$tmpArr = $liTemp->returnVector($sql,1);
				                                	$hasGMConductRecord = $tmpArr[0];
			                                	}
			                                	
			                                	if($hasMeritRecord || ($sys_custom['eDiscipline']['GMConductMark'] && $hasGMConductRecord))
			                                	{
				                             		$sql = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD where StudentID=".$row['UserID']." and AcademicYearID='".$row['AcademicYearID']."' and YearTermID='".$semesterNum."' and RecordStatus= ".DISCIPLINE_STATUS_APPROVED." $cond ";
				                             		$tmpArr = $liTemp->returnVector($sql);
				                 
				                             		$sum = $tmpArr[0];
				                                	
				                             		$row[2] = '<a href="javascript:Show_Merit_Window(\''.$row['UserID'].'\',\''.$row['AcademicYearID'].'\',\''.$semesterNum.'\')" class="tablelink">
				                             				<span id="merit_'.$row['UserID'].$row['AcademicYearID'].$semesterNum.'">'.($ConductScore).'</span></a>';
			                             		}
			                             		else
			                             		{
				                             		$row[2] = $ConductScore;
			                             		}
			                             		
		                             		}
			                             	else
			                             	{
			                             		$row[2] = $BaseConductScore;
		                             		}
		                                }
		                                if($j==3)
		                                {
			                                if($row['YearTermID']=='' || $row['YearTermID']==0)
			                                {
				                                 $Semester='';
				                                 $cond = ' and IsAnnual = 1';
			                                }
			                                else
			                                {
				                             	$Semester = $row['YearTermID'];
				                             	$cond = ' and YearTermID = \''.$Semester.'\'';
			                                }
			                                /*
			                                $sql = "select AdjustMark from DISCIPLINE_CONDUCT_ADJUSTMENT where StudentID = ".$row['UserID']." and Year = '".$row['Year']."' 
			                                and Semester = '".$Semester."' $cond";
			                                */
			                                $sql = "select AdjustMark from DISCIPLINE_CONDUCT_ADJUSTMENT where StudentID = ".$row['UserID']." and AcademicYearID = '".$row['AcademicYearID']."' 
			                                			$cond";
											$tmpArr = $liTemp->returnArray($sql);
	
											if(sizeof($tmpArr)==0)
											{
												$row[$j] = '--';
											}
											else
											{
												
												$score = 0;
												for($y=0;$y<sizeof($tmpArr);$y++)
												{
													$score += $tmpArr[$y]['AdjustMark'];
												}
										        $row[$j] = "<a href=\"javascript:Show_Adj_Window('".$row['UserID']."','".$row['Year']."','".$semesterNum."')\" class=\"tablelink\">
							                                <span id=\"adj_".$row['UserID'].$row['Year'].$semesterNum."\">
							                                ".$score."</a></span>";
						                        
											}
	
		                                }
		                                if($j==5)
		                                {
			                                if($ConductScore)
			                                {
			                                	$row[$j] = $ConductScore + $score;
		                                	}
		                                	else
		                                	{
			                                	$row[$j] = $BaseConductScore + $score;
		                                	}
		                                }
		                                
	                                        $x .= $this->displayCell($j, $row[$j]);
	                                }
	                                $x .= "</tr>\n";
	                                
	                                
	                                
	                        }
	                }
	                
	                $x .= "</table>\n";
	                $this->db_free_result();
	
	                return $x;
	        }
	        function displayFormat_eDisciplineCaseRecord($width="", $tablecolor=""){
	                global $image_path;
	                global $i_Discipline_System_Discipline_Case_Record_days_ago;
	                $ldiscipline = new libdisciplinev12();
					$liTemp = new libdbtable2007(0,0,0);
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $width = ($width!="") ? $width : "100%";
	
	                $n_start = $this->n_start;
	                $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	                $x .= $this->displayColumn();
	                if ($this->db_num_rows()==0)
	                {
	                        $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                } else
	                {
	                        while ($row = $this->db_fetch_array())
	                        {
		                        $row[1] = $row['CaseTitleLink'];
	                                $i++;
	                                //$css = ($i%2) ? "1" : "2";
	                                if($row[8]=='0')
	                                {
		                                $css = "row_waiting";
	                                }
	                                elseif($row[8]=='1')
	                                {
		                                $css = "row_approved";
	                                }
	                                
	                                if ($i>$this->page_size)
	                                {
	                                        break;
	                                }
	                                //$x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                                $x .= "<tr class='".$css."'>\n";
	                                $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                                
	                                for ($j=0; $j<$this->no_col-1; $j++)
	                                {
		                                if($j==5)
		                                {
			                                /*$modifiedDate = strtotime($row[$j]);
											$now = time();
											$dateDiff = $now - $modifiedDate;   
											$fullDays = floor($dateDiff/(60*60*24));
			                             	$row[$j] = $fullDays." $i_Discipline_System_Discipline_Case_Record_days_ago";*/
			                             	$row[$j] = $ldiscipline->convertDatetoDays($row[$j]);
			                             	
		                                }
	                                 	if ($j == 3)	//get PIC user name
			                            {
			                               	//add teacher name after status
			                               	$name_field = getNameFieldByLang();
			                               	
			                               	// [2016-0224-1423-31073]
											// INTRANET_USER
											$tempSQL = "SELECT $name_field as User FROM INTRANET_USER where UserID in (".$row['PICID'].")";
											$result = $liTemp->returnArray($tempSQL,1);
											
											// INTRANET_ARCHIVE_USER
											//$tempSQL = "SELECT $name_field as User FROM INTRANET_ARCHIVE_USER where UserID in (".$row['PICID'].")";
											$tempSQL = "SELECT CONCAT('<span class=\"tabletextrequire\">*</span>', $name_field) as User FROM INTRANET_ARCHIVE_USER where UserID in (".$row['PICID'].")";
											$result2 = $liTemp->returnArray($tempSQL,1);
											
											// Merge user and archive user
											$result = array_merge($result, $result2);
											
											$row[$j] = '';
											for($ct=0;$ct<sizeof($result);$ct++)
											{
												if($ct==0)
												$row[$j] .= $result[$ct]['User'];
												else
												$row[$j] .= ",".$result[$ct]['User'];
											}
										}
										if($j==4)
										{
											$sql = "select count(MR.RecordID) from DISCIPLINE_MERIT_RECORD MR INNER JOIN INTRANET_USER USR ON (USR.UserID=MR.StudentID) INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=".Get_Current_Academic_Year_ID().") where CaseID = ".$row['CaseID']."";
											$count = $liTemp->returnVector($sql);
											$row[$j] = "<a href=\"view_student_involved.php?CaseID=".$row['CaseID']."\">".$count[0]."</a>";
										}
										
	
			                        	$x .= $this->displayCell($j, $row[$j]);
	                             }
	                             $x .= "</tr>\n";
	                        }
	                }
	                if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	
	                return $x;
	        }
	        
	        function displayFormat_eDisciplineConductMarkView($width="", $tablecolor="")
	        {
	                global $image_path,$i_Discipline_System_Discipline_Case_Record_days_ago;
					global $sys_custom;
	                
					$liTemp = new libdbtable2007(0,0,0);
			        $ldiscipline = new libdisciplinev12();
			        
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	                
					$tmp = $liTemp->returnArray($this->sql); 
					$limit = sizeof($tmp);
	                $n_start = $this->n_start;
	                
	                $width = ($width!="") ? $width : "100%";
	                $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	                $x .= $this->displayColumn();
	                if ($this->db_num_rows()==0)
	                {
	                        $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                }
	                else
	                {
	                        while ($row = $this->db_fetch_array())
	                        {
	                                $row_no++;
	                                $score = '';
	                                $css = ($row_no%2) ? "1" : "2";
	                                if ($row_no>$this->page_size)
	                                {
	                                	break;
	                                }
	                                
	                                $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                                $x .= "<td class='tabletext'>".($n_start+$row_no)."</td>\n";
	                                
		                            $yearID = $row['AcademicYearID'];
	   								
	                                for ($j=0; $j<$this->no_col-1; $j++)
	                                {
		                            	$cond = '';
		                                //if($row['Semester']=='')
		                                if($row['YearTermID']=='' || $row['YearTermID']==0)
		                                {
			                                 $Semester='';
			                                 $cond = ' and IsAnnual = 1';
		                                }
		                                else
		                                {
			                             	//$Semester = $row['Semester'];
			                             	//$cond .=" and Semester= '".$row['Semester']."'";
			                             	$cond .=" and YearTermID = '".$row['YearTermID']."'";
		                                }
		                               	
			                           	if($ldiscipline->retriveConductMarkCalculationMethod() == 0)
									   	{
									   		# Get Student Conduct Mark Balance & Grade
			                                $sql = "select ConductScore, GradeChar, YearTermID ";
			                                if($sys_custom['eDiscipline']['yy3']){
			                                	$sql .= ",AdjustedGradeBefore ";
			                                }
											$sql .= "from DISCIPLINE_STUDENT_CONDUCT_BALANCE where 
			                             			StudentID = ".$row['UserID']." and AcademicYearID = '".$row['AcademicYearID']."'
			                             			$cond";
			                             	$tmpArr = $liTemp->returnArray($sql, 1);
			                             	
			                             	$ConductScore = $tmpArr[0]['ConductScore'];
			                             	$Grade = $tmpArr[0]['GradeChar'];
											if($sys_custom['eDiscipline']['yy3'] && $tmpArr[0]['AdjustedGradeBefore']!=''){
												$Grade .= '<span style="color:red">*</span>';
											}
											
											# Get Conduct Base Mark
				                        	$BaseConductScore = $ldiscipline->getConductMarkRule('baseMark');
											
											# Get Student Gain / Deduct Conduct Mark
											if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'])
											{
												$cond2 = ($row['YearTermID']=='' || $row['YearTermID']==0) ? "" : " AND YearTermID=".$row['YearTermID'];
												
												# Total Gain
												$tmpSql1 = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE ReleaseStatus=1 and RecordStatus=1 and MeritType=1 and StudentID=".$row['UserID']." and AcademicYearID = ".$row['AcademicYearID'].$cond2;
												$tmpResult = $ldiscipline->returnVector($tmpSql1);
												$totalGain = $tmpResult[0];
												
												# Total Deduct
												$tmpSql2 = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE ReleaseStatus=1 and RecordStatus=1 and MeritType=-1 and StudentID=".$row['UserID']." and AcademicYearID = ".$row['AcademicYearID'].$cond2;
												$tmpResult = $ldiscipline->returnVector($tmpSql2);
												$totalDeduct = $tmpResult[0];
												
												// [2016-1207-1221-39240]
												# Update Total Gain / Deduct with Manual Adjustment
												if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns_IncludeAdjust'])
												{
													# Total Adjustment Gain
													$tmpSql1 = "SELECT SUM(AdjustMark) FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = ".$row['UserID']." AND AcademicYearID = '".$row['AcademicYearID']."' $cond AND AdjustMark > 0";
													$tmpResult = $ldiscipline->returnVector($tmpSql1);
													$adjustTotalGain = $tmpResult[0];
													$totalGain += $adjustTotalGain;
													
													# Total Adjustment Deduct
													$tmpSql2 = "SELECT SUM(AdjustMark) FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = ".$row['UserID']." AND AcademicYearID = '".$row['AcademicYearID']."' $cond AND AdjustMark < 0";
													$tmpResult = $ldiscipline->returnVector($tmpSql2);
													$adjustTotalDeduct = $tmpResult[0];
													$totalDeduct += $adjustTotalDeduct;
												}
											}
				                        }
				                        else
				                        {
				                        	//$BaseConductScore = $ldiscipline->getConductMarkRule('baseMark');
				                        	$TermStartDate = getStartDateOfAcademicYear($row['AcademicYearID'],$row['YearTermID']);
											$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$row['AcademicYearID']."' AND TermStart <= '$TermStartDate' ORDER BY TermStart DESC";
											$result = $liTemp->returnVector($sql);
											
											if(sizeof($result)>0)
											{
												# Get Student Conduct Mark Balance & Grade for specific term
												for($i=0; $i<sizeof($result); $i++)
												{
													$TermID = $result[$i];
													
													$sql = "SELECT ConductScore, GradeChar, YearTermID ";
													if($sys_custom['eDiscipline']['yy3']){
			                                			$sql.= ",AdjustedGradeBefore ";
			                                		}
													$sql .= " FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE StudentID = ".$row['UserID']." AND AcademicYearID=".$row['AcademicYearID']." AND YearTermID = $TermID";
													$temp_result = $liTemp->returnArray($sql, 1);
													
													if(sizeof($temp_result)>0){
														$ConductScore = $temp_result[0]['ConductScore'];
														$Grade = $temp_result[0]['GradeChar'];
														if($sys_custom['eDiscipline']['yy3'] && $temp_result[0]['AdjustedGradeBefore']!=''){
															$Grade .= '<span style="color:red">*</span>';
														}
														break;
													}
													else{
														$ConductScore = $ldiscipline->getConductMarkRule('baseMark');
														$Grade = "";
													}
												}
												
												# Get Student Gain / Deduct Conduct Mark for specific term
												if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'])
												{
													$cond2 = ($row['YearTermID']=='' || $row['YearTermID']==0) ? "" : " AND YearTermID=".$row['YearTermID'];
													
													# Total Gain
													$tmpSql1 = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE ReleaseStatus=1 and RecordStatus=1 and MeritType=1 and StudentID=".$row['UserID']." and AcademicYearID = ".$row['AcademicYearID']." $cond2";
													$tmpResult = $ldiscipline->returnVector($tmpSql1);
													$totalGain = $tmpResult[0];
													
													# Total Deduct
													$tmpSql2 = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE ReleaseStatus=1 and RecordStatus=1 and MeritType=-1 and StudentID=".$row['UserID']." and AcademicYearID = ".$row['AcademicYearID']." $cond2";
													$tmpResult = $ldiscipline->returnVector($tmpSql2);
													$totalDeduct = $tmpResult[0];
													
													// [2016-1207-1221-39240]
													# Update Total Gain / Deduct with Manual Adjustment
													if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns_IncludeAdjust'])
													{
														$targetTermID = implode(",", $result);
														
														# Total Adjustment Gain
														$tmpSql1 = "SELECT SUM(AdjustMark) FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = ".$row['UserID']." AND AcademicYearID IN ('".$row['AcademicYearID']."') AND YearTermID IN ($targetTermID) AND AdjustMark > 0";
														$tmpResult = $ldiscipline->returnVector($tmpSql1);
														$adjustTotalGain = $tmpResult[0];
														$totalGain += $adjustTotalGain;
														
														# Total Adjustment Deduct
														$tmpSql2 = "SELECT SUM(AdjustMark) FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = ".$row['UserID']." AND AcademicYearID IN ('".$row['AcademicYearID']."') AND YearTermID IN ($targetTermID) AND AdjustMark < 0";
														$tmpResult = $ldiscipline->returnVector($tmpSql2);
														$adjustTotalDeduct = $tmpResult[0];
														$totalDeduct += $adjustTotalDeduct;
													}
												}
											}
				                    	}
				                       	
										$semester_data = getSemesters($yearID);
										$semesterNum = $row['YearTermID'];
										
		                                if($j==1)
		                                {
											if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns']) {
												$a = 11; $b = 15;
											}
											else {
												$a = 9; $b = 13;
											}
											$row[$j] = ($row[$a]==Get_Current_Academic_Year_ID()) ? $row[$j] : $row[$b];
										}
										else if($j==2)
		                                {
			                             	if($ConductScore!='')
			                             	{
				                             	//if($row['Semester']=='')
				                             	if($row['YearTermID']=='' || $row['YearTermID']==0)
												{
													$Semester='';
													//$cond = ' and IsAnnual = 1';	
												}
												else
												{
													//$Semester=$row['Semester'];
													$Semester = $row['YearTermID'];
												}
												
			                                	$sql = "select count(*) from DISCIPLINE_MERIT_RECORD where studentid=".$row['UserID']." and AcademicYearID='".$row['AcademicYearID']."' and YearTermID='".$semesterNum."' and RecordStatus= ".DISCIPLINE_STATUS_APPROVED." AND ReleaseStatus=".DISCIPLINE_STATUS_RELEASED." $cond ";
			                                	$tmpArr = $liTemp->returnVector($sql,1);
			                                	$hasMeritRecord = $tmpArr[0];
			                                	
			                                	// [2016-0808-1526-52240] return misconduct record with conduct score update
			                                	$hasGMConductRecord = false;
			                                	if($sys_custom['eDiscipline']['GMConductMark'])
			                                	{
			                                		$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_RECORD WHERE StudentID=".$row['UserID']." AND AcademicYearID='".$row['AcademicYearID']."' AND YearTermID='".$semesterNum."' AND RecordType=".MISCONDUCT." AND RecordStatus= ".DISCIPLINE_STATUS_APPROVED." AND (GMConductScoreChange > 0 || GMConductScoreChange < 0) $cond ";
				                                	$tmpArr = $liTemp->returnVector($sql,1);
				                                	$hasGMConductRecord = $tmpArr[0];
			                                	}
			                                	
			                                	if($hasMeritRecord || ($sys_custom['eDiscipline']['GMConductMark'] && $hasGMConductRecord))
			                                	{
				                             		$sql = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD where StudentID=".$row['UserID']." and AcademicYearID='".$row['AcademicYearID']."' and YearTermID='".$semesterNum."' and RecordStatus = ".DISCIPLINE_STATUS_APPROVED." AND ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED." $cond ";
			                             			$tmpArr = $liTemp->returnVector($sql);
				                             		$sum = $tmpArr[0];
				                             		
				                             		$row[2] = '<a href="javascript:Show_Merit_Window(\''.$row['UserID'].'\',\''.$row['AcademicYearID'].'\',\''.$semesterNum.'\')" class="tablelink">
				                             				<span id="merit_'.$row['UserID'].$row['AcademicYearID'].$semesterNum.'">'.($ConductScore).'</span></a>';
			                             		}
			                             		else
			                             		{
				                             		$row[2] = $ConductScore;
			                             		}
		                             		}
			                             	else
			                             	{
			                             		$row[2] = $BaseConductScore;
		                             		}
		                                }
		                                
										if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'])
										{
											if($j==3) {		// Display "Total Gain"
												$row[$j] = $totalGain ? $totalGain : '-';
												//$row[$j] .= ", ".$tmpSql1;
												
												// [2017-1004-0907-59206]
				                             	if($row[$j] != '-') {
				                             		$row[$j] = '<a href="javascript:Show_GD_Merit_Window(\''.$row['UserID'].'\',\''.$row['AcademicYearID'].'\',\''.$semesterNum.'\',\'G\')" class="tablelink">
				                             				<span id="merit_gd_'.$row['UserID'].$row['AcademicYearID'].$semesterNum.'G">'.($row[$j]).'</span></a>';
				                             	}
											}
											
											if($j==4) {		// Display "Total Deduct"
												$row[$j] = $totalDeduct ? $totalDeduct : '-';
												//$row[$j] .= ", ".$tmpSql2;
												
												// [2017-1004-0907-59206]
				                             	if($row[$j] != '-') {
				                             		$row[$j] = '<a href="javascript:Show_GD_Merit_Window(\''.$row['UserID'].'\',\''.$row['AcademicYearID'].'\',\''.$semesterNum.'\',\'D\')" class="tablelink">
				                             				<span id="merit_gd_'.$row['UserID'].$row['AcademicYearID'].$semesterNum.'D">'.($row[$j]).'</span></a>';
				                             	}
											}
										}
										
		                                if((!$sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'] && $j==3) || ($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'] && $j==5))
		                                {
			                                $cond = '';
			                                //if($row['Semester']=='')
			                                if($row['YearTermID']=='' || $row['YearTermID']==0)
											{
												$Semester='';
												$cond = ' and IsAnnual = 1';	
											}
											else
											{
												//$Semester=$row['Semester'];
												$Semester = $row['YearTermID'];
												//$cond .= "and Semester = '".$Semester."'";
												$cond .= "and YearTermID = '".$Semester."'";
											}
											
											//$sql = "select AdjustMark from DISCIPLINE_CONDUCT_ADJUSTMENT where StudentID = ".$row['UserID']." and Year = '".$row['Year']."'  $cond";
											if($ldiscipline->retriveConductMarkCalculationMethod() == 0) {
												$sql = "select AdjustMark from DISCIPLINE_CONDUCT_ADJUSTMENT where StudentID = ".$row['UserID']." and AcademicYearID = '".$row['AcademicYearID']."'  $cond";
												$tmpArr = $liTemp->returnArray($sql);
											}
											else{
												$TermStartDate = getStartDateOfAcademicYear($row['AcademicYearID'],$row['YearTermID']);
												$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$row['AcademicYearID']."' AND TermStart <= '$TermStartDate' ORDER BY TermStart DESC";
												$result = $liTemp->returnVector($sql);
												
												if(sizeof($result)>0){
													$targetTermID = implode(",",$result);
													
													$sql = "select AdjustMark from DISCIPLINE_CONDUCT_ADJUSTMENT where StudentID = ".$row['UserID']." and AcademicYearID IN ('".$row['AcademicYearID']."')  AND YearTermID IN ($targetTermID)";
													$tmpArr = $liTemp->returnArray($sql);
												}
											}
											
											if(sizeof($tmpArr)>0)
											{
												$score = 0;
												for($y=0;$y<sizeof($tmpArr);$y++)
												{
													$score += $tmpArr[$y]['AdjustMark'];
												}
												/*
							                     $row[$j] = "<a href=\"javascript:Show_Adj_Window('".$row['UserID']."','".$row['Year']."','".$semesterNum."')\" class=\"tablelink\">
							                                <span id=\"adj_".$row['UserID'].$row['Year'].$semesterNum."\">
							                                ".$score."</a></span>";
							                    */
												$row[$j] = "<a href=\"javascript:Show_Adj_Window('".$row['UserID']."','".$row['AcademicYearID']."','".$semesterNum."')\" class=\"tablelink\">
							                                <span id=\"adj_".$row['UserID'].$row['AcademicYearID'].$semesterNum."\">
							                                ".$score."</a></span>";
											}
											else
											{
												$row[$j] = '--';	
											}
		                                }
		                                
		                                if((!$sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'] && $j==4) || ($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'] && $j==6))
		                                {
			                                if($ConductScore!='')
			                                {
			                                	$row[$j] = $ConductScore + $score;
		                                	}
		                                	else
		                                	{
			                                	$row[$j] = $BaseConductScore + $score;
		                                	}
		                                }
		                                
		                                if((!$sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'] && $j==5) || ($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'] && $j==7))
		                                {
		                                	if($Grade=='')
											{
												$row[$j] = '--';
											}
											else
											{
		                                		$row[$j] = $Grade;
											}
	                                	}
	                                	
		                                if((!$sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'] && $j==6) || ($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'] && $j==8))
		                                {
											if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns']) {
												$a = 5;
											}
											else {
												$a = 3;
											}
											
			                                if(sizeof($tmpArr)==0 || $row[$a]=='--' || trim($row[$a]==''))
			                                {
			                                	$row[$j] = '--';
		                                	}
		                                	else
		                                	{
				                                /*$modifiedDate = strtotime($row[$j]);
												$now = time();
												$dateDiff = $now - $modifiedDate;   
												$fullDays = floor($dateDiff/(60*60*24));
				                             	$row[$j] = $fullDays." $i_Discipline_System_Discipline_Case_Record_days_ago";*/
				                             	$row[$j] = $ldiscipline->convertDatetoDays($row[$j]);
			                             	}
		                                }
		                                
	                                    $x .= $this->displayCell($j, $row[$j]);
	                                }
	                                $x .= "</tr>\n";
	                        }
	                }
	                if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                
	                $this->db_free_result();
					
	                return $x;
	        }
	
	        function displayFormat_eDisciplineGMMisCatSetting($width="", $tablecolor=""){
	                global $image_path, $ldiscipline, $PATH_WRT_ROOT, $i_To, $intranet_root, $sys_custom;
	                global $iDiscipline, $eDiscipline;
	
	                $liTemp = new libdbtable2007(0,0,0);
					$ldiscipline = new libdisciplinev12();
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $width = ($width!="") ? $width : "100%";
	
	                $n_start = $this->n_start;
	                $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	                $x .= $this->displayColumn();
	                if ($this->db_num_rows()==0)
	                {
	                        $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                } else
	                {
	                        include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	                        $SchoolYear = getCurrentAcademicYear();
	                        $lfile = new libfilesystem();
	                        ### Check the semester mode - auto change / manual change ###
	                        $semester_mode = $lfile->file_read($intranet_root."/file/semester_mode.txt");
	                        $currentAcademicYearID = Get_Current_Academic_Year_ID();
	                        $academicStartDate = getStartDateOfAcademicYear($currentAcademicYearID);
	                        $academicEndDate = getEndDateOfAcademicYear($currentAcademicYearID);
							//$academicStartDate = $ldiscipline->getCurrentAcademicStartDate();
							//$academicEndDate = $ldiscipline->getCurrentAcademicStartDate($academicStartDate);
	
	
	                        while ($row = $this->db_fetch_array())
	                        {
	                                $i++;
	                                $css = ($i%2) ? "1" : "2";
	                                if ($i>$this->page_size)
	                                {
	                                        break;
	                                }
	                                $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                                $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                                for ($j=0; $j<$this->no_col-1; $j++)
	                                {
		                                    if ($j == 1)    # No. Of Item
		                                    {
			                                        $CategoryID = $row[5];
			                                        $use_intranet_homework = $ldiscipline->accumulativeUseIntranetSubjectList();
			                                        if ($CategoryID == 2 && $use_intranet_homework)    # homework
			                                        {
			                                                //$sql  = "SELECT COUNT(SubjectName) FROM INTRANET_SUBJECT WHERE RecordStatus = 1";
			                                                $sql = "SELECT COUNT(RecordID) FROM ASSESSMENT_SUBJECT WHERE RecordStatus=1  and (CMP_CODEID is NULL or CMP_CODEID = '') ";
			                                                $result = $liTemp->returnVector($sql);
			                                                $tempData = "<a class=\"tablelink\" href=\"category_item.php?CategoryID=$CategoryID\">".$result[0]."</a>";
			                                                $x .= $this->displayCell($j, $tempData);
			                                        }
			                                        else
			                                        {
				                                            $x .= $this->displayCell($j, $row[$j]);
			                                        }
		                                    }
		                                    elseif ($j == 2)    # Period
		                                    {
		                                    		if($sys_custom['eDiscipline']['PooiToMiddleSchool']) continue;
			                                        $CategoryID = $row[5];
			                                        $PeriodSet = $ldiscipline->returnPeriodSetID($CategoryID);
	
			                                        $defined_field_link = $eDiscipline['Setting_CalculationSchema_Static_Abb'];
	
			                                        $period_field =" CONCAT(a.DateStart,' $i_To ' ,a.DateEnd) ";
	
			                                        $scheme_field = " IF(c.RuleID IS NOT NULL,'".$eDiscipline['Setting_CalculationSchema_Floating_Abb']."',
								                                        IF(b.SettingID IS NOT NULL,'".$defined_field_link."',
								                                        '".$eDiscipline['Setting_CalculationSchema_Undefined']."')) 
							                                        ";
	
	
			                                        if($semester_mode == SEMESTER_MODE_MANUALLY){
			                                                $display_semester = " ,a.TargetSemester ";
			                                                $num_of_column = 3;
			                                        }else{
			                                                $display_semester = "";
			                                                $num_of_column = 2;
			                                        }
	
	#                                                         DISTINCT CONCAT('<a class=\"tablelink\" href=\"category_specific_period_edit.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&Scheme=1&PeriodType=',IF(c.RecordStatus = '' OR c.RecordStatus IS NULL,0,c.RecordStatus),'&PeriodID=',a.PeriodID,'\">',$period_field,'</a>'),
	/*
	                                                        If(c.RuleID IS NOT NULL,CONCAT('<a class=\"tablelink\" href=\'category_period_rule.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&Scheme=1&PeriodType=',IF(c.RecordStatus = '' OR c.RecordStatus IS NULL,0,c.RecordStatus),'&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>'),
	                                                            IF(b.SettingID IS NOT NULL, CONCAT('<a class=\"tablelink\" href=\'category_period_edit.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&Scheme=0&PeriodType=',IF(c.RecordStatus = '' OR c.RecordStatus IS NULL,0,c.RecordStatus),'&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>'),
	                                                            CONCAT('<a class=\"tablelink\" href=\'category_period_new2.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&PeriodType=',IF(c.RecordStatus = '' OR c.RecordStatus IS NULL,0,c.RecordStatus),'&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>'))
	                                                        )
	*/		                                        
	                                                $sql ="SELECT DISTINCT
	                                                	IF(a.SetID=0,
	                                                		$period_field,
	                                                        CONCAT('<a class=\"tablelink\" href=\"category_specific_period_edit.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&Scheme=1&PeriodType=',IF(a.SetID=0,0,1),'&PeriodID=',a.PeriodID,'\">',$period_field,'</a>')
	                                                	)  , 
	                                                        If(c.RuleID IS NOT NULL,CONCAT('<a class=\"tablelink\" href=\'category_period_rule.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&Scheme=1&PeriodType=',IF(a.SetID=0,0,1),'&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>'),
	                                                            IF(b.SettingID IS NOT NULL, CONCAT('<a class=\"tablelink\" href=\'category_period_edit.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&Scheme=0&PeriodType=',IF(a.SetID=0,0,1),'&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>'),
	                                                            CONCAT('<a class=\"tablelink\" href=\'category_period_new2.php?1meritType=".MISCONDUCT."&CategoryID=$CategoryID&PeriodType=',IF(a.SetID=0,0,1),'&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>'))
	                                                        )
	                                                        $display_semester
	                                                       FROM 
	                                                           DISCIPLINE_ACCU_PERIOD AS a LEFT OUTER JOIN 
	                                                           DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING AS b ON (a.PeriodID = b.PeriodID AND b.CategoryID='$CategoryID') LEFT OUTER JOIN 
	                                                           DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE AS c ON (c.CategoryID='$CategoryID' AND c.PeriodID =a.PeriodID ) 
	                                                       WHERE
	                                                           (a.RecordType = ".MISCONDUCT." OR a.RecordType IS NULL) AND
	                                                           a.SetID = '$PeriodSet' AND
	                                                           a.DateStart BETWEEN '$academicStartDate' AND '$academicEndDate'
	                                                       ORDER BY a.DateStart, a.DateEnd
	                                                       ";
	
	                                                $result = $liTemp->returnArray($sql, $num_of_column);
	                                                $TempData = "";
	                                                for($k=0;$k<sizeof($result);$k++) {
	                                                    $TempData .= "<br />".$result[$k][0]." (".$result[$k][1].")";
	                                                }
	
	                                                $x .= $this->displayCell($j, "[".$row[$j]."]".$TempData);
	                                        }
	                                        else
	                                        {
		                                            $x .= $this->displayCell($j, $row[$j]);
	                                        }
	                                }
	                                $x .= "</tr>\n";
	                        }
	                }
	                if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	
	                return $x;
	        }
	
	        function displayFormat_eDisciplineGMGoodCatSetting($width="", $tablecolor=""){
	                global $image_path, $ldiscipline, $PATH_WRT_ROOT, $i_To;
	                global $iDiscipline, $eDiscipline;
	
	                $liTemp = new libdbtable2007(0,0,0);
					$ldiscipline = new libdisciplinev12();
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $width = ($width!="") ? $width : "100%";
	
	                $n_start = $this->n_start;
	                $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	                $x .= $this->displayColumn();
	                if ($this->db_num_rows()==0)
	                {
	                        $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                } else
	                {
	                        include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	                        $SchoolYear = getCurrentAcademicYear();
	                        $lfile = new libfilesystem();
	                        ### Check the semester mode - auto change / manual change ###
	                        $semester_mode = $lfile->file_read($intranet_root."/file/semester_mode.txt");
	                        $currentAcademicYearID = Get_Current_Academic_Year_ID();
	                        $academicStartDate = getStartDateOfAcademicYear($currentAcademicYearID);
	                        $academicEndDate = getEndDateOfAcademicYear($currentAcademicYearID);
							//$academicStartDate = $ldiscipline->getCurrentAcademicStartDate();
							//$academicEndDate = $ldiscipline->getCurrentAcademicStartDate($academicStartDate);
	
	
	                        while ($row = $this->db_fetch_array())
	                        {
	                                $i++;
	                                $css = ($i%2) ? "1" : "2";
	                                if ($i>$this->page_size)
	                                {
	                                        break;
	                                }
	                                $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                                $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                                for ($j=0; $j<$this->no_col-1; $j++)
	                                {
		                                    if ($j == 2)    # Period
		                                    {
			                                        $CategoryID = $row[5];
			                                        $PeriodSet = $ldiscipline->returnPeriodSetID($CategoryID);
			                                        
	
			                                        $defined_field_link = $eDiscipline['Setting_CalculationSchema_Static_Abb'];
	
			                                        $period_field =" CONCAT(a.DateStart,' $i_To ' ,a.DateEnd) ";
	
			                                        $scheme_field = " IF(c.RuleID IS NOT NULL,'".$eDiscipline['Setting_CalculationSchema_Floating_Abb']."',
								                                        IF(b.SettingID IS NOT NULL,'".$defined_field_link."',
								                                        '".$eDiscipline['Setting_CalculationSchema_Undefined']."')) 
							                                        ";
	
	
			                                        if($semester_mode == SEMESTER_MODE_MANUALLY){
			                                                $display_semester = " ,a.TargetSemester ";
			                                                $num_of_column = 3;
			                                        }else{
			                                                $display_semester = "";
			                                                $num_of_column = 2;
			                                        }
	#                                                        DISTINCT CONCAT('<a class=\"tablelink\" href=\"category_specific_period_edit.php?meritType=".GOOD_CONDUCT."&CategoryID=$CategoryID&Scheme=1&PeriodType=',IF(c.RecordStatus = '' OR c.RecordStatus IS NULL,0,c.RecordStatus),'&PeriodID=',a.PeriodID,'\">',$period_field,'</a>'),
	
	                                                $sql ="SELECT DISTINCT
	                                                		IF(a.SetID=0,
	                                                        	CONCAT($period_field),
	                                                        	CONCAT('<a class=\"tablelink\" href=\"category_specific_period_edit.php?meritType=".GOOD_CONDUCT."&CategoryID=$CategoryID&Scheme=1&PeriodType=',IF(a.SetID=0,0,1),'&PeriodID=',a.PeriodID,'\">',$period_field,'</a>')
	                                                    	),
	                                                        If(c.RuleID IS NOT NULL,CONCAT('<a class=\"tablelink\" href=\'category_period_rule.php?meritType=".GOOD_CONDUCT."&CategoryID=$CategoryID&Scheme=1&PeriodType=',IF(c.RecordStatus = '' OR c.RecordStatus IS NULL,0,c.RecordStatus),'&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>'),
	                                                            IF(b.SettingID IS NOT NULL, CONCAT('<a class=\"tablelink\" href=\'category_period_edit.php?meritType=".GOOD_CONDUCT."&CategoryID=$CategoryID&Scheme=0&PeriodType=',IF(c.RecordStatus = '' OR c.RecordStatus IS NULL,0,c.RecordStatus),'&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>'),
	                                                            CONCAT('<a class=\"tablelink\" href=\'category_period_new2.php?meritType=".GOOD_CONDUCT."&CategoryID=$CategoryID&PeriodType=',IF(c.RecordStatus = '' OR c.RecordStatus IS NULL,0,c.RecordStatus),'&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>'))
	                                                        )
	                                                        $display_semester
	                                                       FROM 
	                                                           DISCIPLINE_ACCU_PERIOD AS a LEFT OUTER JOIN 
	                                                           DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING AS b ON (a.PeriodID = b.PeriodID AND b.CategoryID='$CategoryID') LEFT OUTER JOIN 
	                                                           DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE AS c ON (c.CategoryID='$CategoryID' AND c.PeriodID =a.PeriodID )
	                                                       WHERE
	                                                           a.RecordType = ".GOOD_CONDUCT." AND
	                                                           a.SetID = '$PeriodSet' AND
	                                                           a.DateStart BETWEEN '$academicStartDate' AND '$academicEndDate'
	                                                       ORDER BY a.DateStart, a.DateEnd
	                                                       ";
	
	                                                $result = $liTemp->returnArray($sql, $num_of_column);
	                                                
	                                                $TempData = "";
	                                                for($k=0;$k<sizeof($result);$k++) {
														$TempData .= "<br />".$result[$k][0]." (".$result[$k][1].")";
	                                                }
	
	                                                $x .= $this->displayCell($j, "[".$row[$j]."]".$TempData);
	                                        }
	                                        else
	                                        {
		                                            $x .= $this->displayCell($j, $row[$j]);
	                                        }
	                                }
	                                $x .= "</tr>\n";
	                        }
	                }
	                if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	
	                return $x;
	        }
	        
	        function displayFormat_eDisciplineGMMisNewLeafSetting($width="", $tablecolor=""){
		        
	                global $i_To;
	
	                $liTemp = new libdbtable2007(0,0,0);
					$ldiscipline = new libdisciplinev12();
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                $SchoolYear = getCurrentAcademicYear();
	                $currentAcademicYearID = Get_Current_Academic_Year_ID();
	                $academicStartDate = getStartDateOfAcademicYear($currentAcademicYearID);
	                $academicEndDate = getEndDateOfAcademicYear($currentAcademicYearID);
					//$academicStartDate = $ldiscipline->getCurrentAcademicStartDate();
					//$academicEndDate = $ldiscipline->getCurrentAcademicStartDate($academicStartDate);
	                
	                $width = ($width!="") ? $width : "100%";
	
	                $n_start = $this->n_start;
	                $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	                $x .= $this->displayColumn();
	                if ($this->db_num_rows()==0)
	                {
	                        $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                } else
	                {
	                    while ($row = $this->db_fetch_array())
	                    {
	                        $i++;
	                        $css = ($i%2) ? "1" : "2";
	                        if ($i>$this->page_size)
	                        {
	                                break;
	                        }
	                        $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                        $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                        for ($j=0; $j<$this->no_col-1; $j++)
	                        {
	                            if ($j == 1)    # Period
	                            {
	                                $TempData = "";
	                                $CategoryID = $row[1];
	                                
	                                $sql = "SELECT RecordStatus, SetID FROM DISCIPLINE_ACCU_CATEGORY_PERIOD WHERE CategoryID=$CategoryID";
	                                $defaultSpecify = $liTemp->returnVector($sql);
	                                //echo $defaultSpecify[0].'/';
	                                if($defaultSpecify[0]==0 || $CategoryID==1) {		# default period / 'Late'
		                                $sql = "SELECT PeriodID FROM DISCIPLINE_ACCU_PERIOD WHERE SetID=0 AND (RecordType=-1 OR RecordType IS NULL) AND TargetYear='$SchoolYear' AND (DateStart>='$academicStartDate' AND DateEnd<='$academicEndDate')";
		                                $PeriodSet = $liTemp->returnVector($sql);
	                                } else {										# specify period
		                                $sql = "SELECT PeriodID FROM DISCIPLINE_ACCU_PERIOD as a LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_PERIOD as b ON (a.SetID=b.SetID) WHERE b.CategoryID=$CategoryID AND a.TargetYear='$SchoolYear' AND a.DateStart>='$academicStartDate' AND a.DateEnd<='$academicEndDate' ORDER BY DateStart";
		                                $PeriodSet = $liTemp->returnVector($sql);
	                                }
	                                //echo implode(',', $PeriodSet).'<br>';
	                                $sql = "SELECT
	                                			DateStart,
	                                			DateEnd
	                                		FROM
	                                			DISCIPLINE_ACCU_PERIOD
	                                		WHERE
	                                			PeriodID IN (".implode(',',$PeriodSet).")
	                                		ORDER BY DateStart
	                                       ";
	
	                                $result = $liTemp->returnArray($sql, 2);
	                                //debug_r($result);
	                                
	                                for($k=0;$k<sizeof($result);$k++) {
	                                    $TempData .= $result[$k][0]." ".$i_To." ".$result[$k][1];
	                                    $TempData .= (sizeof($result) != $k+1) ? "<br />" : "";
	                                }
	                                if($TempData == "")
	                                	$TempData = "---";
	
	                                $x .= $this->displayCell($j, $TempData);
	                                
	                            } else if($j == 2) {	# Waive Day(s)
	                            	$TempData = "";
	                            	//echo implode(',', $PeriodSet).'<br>';
		                            $sql = "SELECT WaiveDay FROM DISCIPLINE_NEW_LEAF_SCHEME as a LEFT OUTER JOIN DISCIPLINE_ACCU_PERIOD as b ON (a.PeriodID=b.PeriodID) WHERE CategoryID=$CategoryID AND a.PeriodID IN (".implode(',',$PeriodSet).") ORDER BY b.DateStart";
		                            $result = $liTemp->returnVector($sql);
		                            
		                            for($k=0; $k<sizeof($result); $k++) {
			                         	$TempData .= ($result[$k]) ? $result[$k] : "---";
			                         	$TempData .= (sizeof($result)!=$k+1) ? "<br />" : ""; 
		                            }
		                            if(sizeof($result)==0) {
		                            	$TempData = "---";
	                            	}
	                            	$x .= $this->displayCell($j, $TempData);
	                            	
	                            } else if($j == 3) {	# Waive First
	                            	$TempData = "";
		                            $sql = "SELECT WaiveFirst FROM DISCIPLINE_NEW_LEAF_SCHEME as a LEFT OUTER JOIN DISCIPLINE_ACCU_PERIOD as b ON (a.PeriodID=b.PeriodID) WHERE CategoryID=$CategoryID AND a.PeriodID IN (".implode(',',$PeriodSet).") ORDER BY b.DateStart";
		                            $result = $liTemp->returnVector($sql);
		                            
		                            for($k=0; $k<sizeof($result); $k++) {
			                         	$TempData .= ($result[$k]) ? $result[$k] : "---";
			                         	$TempData .= (sizeof($result)!=$k+1) ? "<br />" : ""; 
		                            }
		                            if(sizeof($result)==0) {
		                            	$TempData = "---";
	                            	}
	                            	$x .= $this->displayCell($j, $TempData);
	                            	
	                            }
	                            else
	                            {
	                                $x .= $this->displayCell($j, $row[$j]);
	                            }
	                        }
	                        $x .= "</tr>\n";
	                    }
	                }
	                if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	
	                return $x;
	        }
	        
	        function displayFormat_GeneralDisplay($tablecolor="")
	        {
	                global $image_path, $LAYOUT_SKIN;
	
	                $this->rs = $this->db_db_query($this->built_sql());
	                
					$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>";
					$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4' id='ContentTable'>";
					
					$x .= $this->displayColumn();
	
					if ($this->db_num_rows()==0)
					{
						$x .= "<tr>
						<td class='table{$tablecolor}row2 tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
						</tr>\n
						";
					} else {
						$i=0;
						
						while($row = $this->db_fetch_array())
						{
							$x .= "<tr class='table{$tablecolor}row". ( ($i)%2 + 1 )."'>\n";
							
							for($j=0; $j<$this->no_col; $j++)
							{
								if (trim($row[$j]) == "")
								{
									$row[$j] = "&nbsp;";
								}
								$x .= $this->displayCell($j,nl2br($row[$j]), "table{$tablecolor}link","valign='top'");
							}
							
							$x .= "</tr>\n";
							$i++;
						}
					}
					$x .= "</table>";
					$x .=" </td></tr>";
					$x .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
					$x .= "</table>\n";
					$this->db_free_result();
					return $x;
	        }
	        
	        function displayFormat_GeneralDisplayWithNavigation($tablecolor="", $displayNumber=0)
	        {
	                global $image_path, $LAYOUT_SKIN;
	
	                $this->rs = $this->db_db_query($this->built_sql());
	
	                #### update to IP25 standard
	                #### if cannot display backgroup color of header, pls use 'th' instead of 'td' in $li->column_list
					$x .= "<table class='common_table_list $view_table_list' id='ContentTable'>\n<thead>\n";
					$x .= $this->displayColumn();
					if ($this->db_num_rows()==0)
					{
						$x .= "<tr>
                                <td class='tableContent' align='center' colspan='".($this->no_col)."' ><br />".$this->no_msg."<br /><br></td>
                                </tr>\n
                                ";
					} else {
						
						$n_start = $this->n_start;
						$i=0;
						
						while($row = $this->db_fetch_array())
						{
							$i++;
							$x .= "<tr class='table{$tablecolor}row". ( ($i)%2 + 1 )."'>\n";
							
							if($displayNumber)
							{
								$x .= $this->displayCell(0, ($n_start+$i), "","valign='top'");
								$j_no_col = $this->no_col - 1;
							}
							$j_no_col = $j_no_col ? $j_no_col : $this->no_col;
							for($j=0; $j<$j_no_col; $j++)
							{
								if (trim($row[$j]) == "")
								{
									$row[$j] = "&nbsp;";
								}
								$x .= $this->displayCell($j,nl2br($row[$j]), "table{$tablecolor}link","valign='top'");
							}
							
							$x .= "</tr>\n";
						}
						
					}
					$x .= "</table>";
					
					if($this->db_num_rows()<>0) 
	                	$x .= $this->navigation_IP25();
					$this->db_free_result();
					
					
					return $x;
	        }
	
	        
	        function displayFormat_RepairSystem($tablecolor="")
	        {
	                global $image_path, $LAYOUT_SKIN;
	
	                $this->rs = $this->db_db_query($this->built_sql());
	
					$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>";
					$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4' id='ContentTable'>";
					
					$x .= $this->displayColumn();
	
						$n_start = $this->n_start;
					if ($this->db_num_rows()==0)
					{
						$x .= "<tr>
						<td class='table{$tablecolor}row2 tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
						</tr>\n
						";
					} else {
						$i++;
						
						while($row = $this->db_fetch_array())
						{
							$x .= "<tr class='table{$tablecolor}row". ($i%2 == 0? 2: $i%2)."'>\n";
							$x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++)
							{
								if (trim($row[$j]) == "")
								{
									$row[$j] = "&nbsp;";
								}
								$x .= $this->displayCell($j,nl2br($row[$j]), "table{$tablecolor}link","");
							}
							
							$x .= "</tr>\n";
							$i++;
						}
					}
					$x .= "</table>";
					$x .=" </td></tr>";
					//$x .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
					if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
					$x .= "</table>\n";
					$this->db_free_result();
					return $x;

	        }
	        
	        
	        function displayFormat_eLib_manage_license($aryAssignedQuota, $tablecolor="orange"){
	        	
	        	global $eLib;
	        	
	        	$aryRs = $this->returnArray($this->built_sql()); 
	        		        	
	        	$x .= "<table style='background:#FFF;' width='100%' border='0' cellspacing='0' cellpadding='5'  bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5><tr><td>";
	        	$x .= $this->displayColumn();
	        	if ($aryRs==array())
				{
					$x .= "<tr>
					<td class='table{$tablecolor}row2 tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
					</tr>\n
					";
				} else {
					$i=0;
					$curColor = "";					
					foreach($aryRs as $i=> $row)
					{
						$css = ($i%2) ? "0" : "2";
						$cell_css = ($i%2)? "" : "style='background:#EEE;'";
						$x .= "<tr class=tableContent$css>\n";
						$x .= $this->displayCell($i,($i+1), "table{$tablecolor}link","valign='top' widdth='50px' $cell_css");
						for($j=0; $j<$this->no_col; $j++)
						{
							if (trim($row[$j]) == "")
							{
								$row[$j] = "&nbsp;";
							}
							$x .= $this->displayCell($j,nl2br($row[$j]), "table{$tablecolor}link","valign='top' $cell_css");
						}
						$enabled_quota = $row['quota'];
					//	$assigned_quota = (isset($aryAssignedQuota[$row['BookLicenseID']])) ? $aryAssignedQuota[$row['BookLicenseID']] : 0;
						$assigned_quota = $row['assigned_quota'];
						$available_quota = ($enabled_quota > $assigned_quota)? $enabled_quota-$assigned_quota : 0;						
						
						$assigned_quota = '<a href="admin_book_license_view_assign.php?BookID='.$row['BookID'].'&TB_iframe=true&amp;height=500&amp;width=800" class="thickbox" style="color:#006bbd;" title="'.$eLib['admin']['View_Assign_Student'].'"> '.$assigned_quota.'</a>';
						$available_quota = '<a href="admin_book_license_assign.php?BookID='.$row['BookID'].'&BookLicenseID='.$row['BookLicenseID'].'&TB_iframe=true&amp;height=500&amp;width=800" class="thickbox" style="color:#006bbd;" title="'.$eLib['admin']['Assign_Student'].'"> '.$available_quota.'</a> / '.$enabled_quota;
						
						$x .= $this->displayCell($j,nl2br($assigned_quota), "table{$tablecolor}link","valign='top' $cell_css ");
						$x .= $this->displayCell($j,nl2br($available_quota), "table{$tablecolor}link","valign='top' $cell_css");
						
						
						
						$x .= "</tr>\n";
						
						$i++;
					}
					$colspan = ($this->no_col) + 3;
					$x .= "<tr><td colspan='".$colspan."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
				}
				
				 
					$x .= "</table>\n";
				
					return $x;
	        	
	        }
	        
	        
	        function displayFormat_module_manage_license($tablecolor="orange"){
	        	
	        	global $eLib;
	        	
	        	$aryRs = $this->returnArray($this->built_sql()); 
	        		        	
	        	$x .= "<table style='background:#FFF;' width='100%' border='0' cellspacing='0' cellpadding='5'  bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5><tr><td>";
	        	$x .= $this->displayColumn();
	        	if ($aryRs==array())
				{
					$x .= "<tr>
					<td class='table{$tablecolor}row2 tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
					</tr>\n
					";
				} else {
					$i=0;
					$curColor = "";					
					foreach($aryRs as $i=> $row)
					{
						$css = ($i%2) ? "0" : "2";
						$cell_css = ($i%2)? "" : "style='background:#EEE;'";
						$x .= "<tr class=tableContent$css>\n";
						
						# The Row Number
						$x .= $this->displayCell($i,($i+1), "table{$tablecolor}link","valign='top' widdth='50px' $cell_css");
						
						# The Description
						for($j=0; $j<1; $j++) // only display 1st field, others need to do some checking before display cell
						{
							if (trim($row[$j]) == "")
							{
								$row[$j] = "&nbsp;";
							}
							$x .= $this->displayCell($j,nl2br($row[$j]), "table{$tablecolor}link","valign='top' $cell_css");
						}
						
						#- Calculate the enabled quota and the assigned quota before display
						$enabled_quota = $row['quota'];
						$assigned_quota = $row['assigned_quota'];
						$available_quota = ($enabled_quota > $assigned_quota)? $enabled_quota-$assigned_quota : 0;						
						
						$assigned_quota = '<a href="admin_module_license_view_assign.php?ModuleID='.$row['ModuleID'].'&TB_iframe=true&amp;height=500&amp;width=800" class="thickbox" style="color:#006bbd;" title="'.$eLib['admin']['View_Assign_Student'].'"> '.$assigned_quota.'</a>';
						if (!empty($enabled_quota)) {
							$available_quota = '<a href="admin_module_license_assign.php?ModuleID='.$row['ModuleID'].'&ModuleLicenseID='.$row['ModuleLicenseID'].'&TB_iframe=true&amp;height=520&amp;width=710" class="thickbox" style="color:#006bbd;" title="'.$eLib['admin']['Assign_Student'].'"> '.$available_quota.'</a> / '.$enabled_quota;
						} else {
							$available_quota = '-';
						}
						
						# The Assigned Quota
						$x .= $this->displayCell($j,nl2br($assigned_quota), "table{$tablecolor}link","valign='top' $cell_css ");
						# The Available Quota
						$x .= $this->displayCell($j,nl2br($available_quota), "table{$tablecolor}link","valign='top' $cell_css");
						
						
						
						$x .= "</tr>\n";
						
						$i++;
					}
					$colspan = ($this->no_col) + 3;
					$x .= "<tr><td colspan='".$colspan."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
				}
				
				 
					$x .= "</table>\n";
				
					return $x;
	        	
	        }
	        
	        function displayFormat_eAttendance_ParentLetter($tablecolor="")
	        {
	                global $image_path, $LAYOUT_SKIN, $i_general_Days;
	                
					include_once("libcardstudentattend2.php");
					$lc = new libcardstudentattend2();
					
	                $this->rs = $this->db_db_query($this->built_sql());
	
					$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>";
					$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4' id='ContentTable'>";
					
					$x .= $this->displayColumn();
	
						$n_start = $this->n_start;
					if ($this->db_num_rows()==0)
					{
						$x .= "<tr>
						<td class='table{$tablecolor}row2 tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
						</tr>\n
						";
                        $x .= '<tr><td colspan="'.$this->no_col.'" class="dotline"><img src="/images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td></tr>'."\n";
					} else {
						$i++;
						
						while($row = $this->db_fetch_array())
						{
							$x .= "<tr class='table{$tablecolor}row". ( ($i)%2+2 )."'>\n";
							$x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++)
							{
								if($j==3) {
									$x .= "<td>".$lc->countSchoolDayDiff($row[$j], date("Y-m-d"))." $i_general_Days</td>";	
								}
								else 
								{
									if(trim($row[$j]) == "")
										$row[$j] = "&nbsp;";
									$x .= $this->displayCell($j,nl2br($row[$j]), "table{$tablecolor}link","");
								}
							}
							
							$x .= "</tr>\n";
							$i++;
						}
					}
					$x .= "</table>";
					$x .=" </td></tr>";
					
					if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
					
					$x .= "</table>\n";
					
					$this->db_free_result();
					
					return $x;

	        }	        
	
	        function displayFormat_UserMgmtStaffAccount($tablecolor="")
	        {
	                global $image_path, $LAYOUT_SKIN, $laccount;
	                $this->rs = $this->db_db_query($this->built_sql());
	                $x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn();
					$x .= "</thead>\n";
					$n_start = $this->n_start;
					if ($this->db_num_rows()==0)
					{
						$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						$i++;
						
						while($row = $this->db_fetch_array())
						{
							$x .= "<tr>\n";
							$x .= "<td>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++)
							{
								if($j==0) {
									$x .= "<td><a href='javascript:;' onClick='goEdit(".$row['UserID'].")'>".$row[$j]."</a></td>";
								}
//								else if($j==3) {
//									$temp = $laccount->getTeachingClassList($row[$j]);
//									$x .= "<td>".(($temp!="") ? $temp : "---")."</td>";	
//								}
								else 
								{
									$x .= $this->displayCell($j,nl2br($row[$j]), "","");
								}
							}
							
							$x .= "</tr>\n";
							$i++;
						}
					}
					$x .= "</table>";
					
					if($this->db_num_rows()<>0) $x .= $this->navigation_IP25("",$othercss_bottom);	
					
					$this->db_free_result();
					
					return $x;

	        }
	        function displayFormat_UserMgmtStaffAccountCentennial($tablecolor="")
	        {
	        	//global $image_path, $LAYOUT_SKIN, $laccount;
	        	//debug_r("hi");
	        	$this->rs = $this->db_db_query($this->built_sql());
	        	$x .= "<table>\n<thead>\n";
	        	$x .= $this->displayColumn('');
	        	$x .= "</thead>";
	        	$n_start = $this->n_start;
	        	if ($this->db_num_rows()==0)
	        	{
	        		$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
	        	} else {
	        		$i++;
	        		
	        		while($row = $this->db_fetch_array())
	        		{
	        			$x .= "<tr>\n";
	        			$x .= "<td>".($n_start+$i)."</td>\n";
	        			for($j=0; $j<$this->no_col-1; $j++)
	        			{
	        				if($j==0) {
	        					$x .= "<td><a href='javascript:;' onClick='show_detail_info(".$row['UserID'].")'>".$row[$j]."</a></td>";
	        				}else if ($j== $this->no_col-2){
	        					$x .= "<td><label class='check_box'><input type= 'checkbox' class='check_box' name='user_id[]' id='user_id[]' value=".$row['UserID']."><span class='check_box_text'></span></label></th>\n";
	        				}
	        				else
	        				{
	        					if(trim($row[$j]) == "")
	        						$row[$j] = "&nbsp;";
	        						$x .= $this->displayCell($j,nl2br($row[$j]), "","");
	        				}
	        			}
	        				
	        			$x .= "</tr>\n";
	        			$i++;
	        		}
	        	}
	        	$x .= "</table>";
	        		
	        	if($this->db_num_rows()<>0) $x .= $this->navigation_IP25("",$othercss_bottom);
	        		
	        	$this->db_free_result();
	        		
	        	return $x;
	        
	        }
	        function displayFormat_UserMgmtParentAccount($tablecolor="")
	        {
	                global $image_path, $LAYOUT_SKIN, $laccount, $Lang;
	
	                $this->rs = $this->db_db_query($this->built_sql());
					$n_start = $this->n_start;

					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn();		
					$x .= "</thead>\n";
					if ($this->db_num_rows()==0)
					{
						$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
// 						$x .= "<div class='no_record_find_v30'>". $this->no_msg ."</div>";
					} else {
						$i++;
						
						while($row = $this->db_fetch_array())
						{
							$x .= "<tr class='table{$tablecolor}row". ( ($i)%2+2 )."'>\n";
							$x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++)
							{
								if($j==0) {
									$x .= "<td><a href='javascript:;' onClick='goEdit(".$row[8].")'>".$row[$j]."</a></td>";
								}
//								else if($j==2) {
//									$temp = $laccount->getStudentListByParentID($row[$j]);
//									if($temp=="") $temp = "---";
//									$x .= "<td>$temp</td>";	
//								}
//								else if($j==3) {
//									$sql = "SELECT HomeTelNo, OfficeTelNo, MobileTelNo FROM INTRANET_USER WHERE UserID=".$row[$j];
//									$result = $laccount->returnArray($sql, 3);
//									
//									$temp = "";
//									if($result[0][0]!="") $temp .= ($temp=="") ? $result[0][0] : " / ".$result[0][0];
//									if($result[0][1]!="") $temp .= ($temp=="") ? $result[0][1] : " / ".$result[0][1];
//									if($result[0][2]!="") $temp .= ($temp=="") ? $result[0][2] : " / ".$result[0][2];
//									if($temp=="") $temp = "---";
//									$x .= "<td>$temp</td>";	
//								}
								else 
								{
									if(trim($row[$j]) == "")
										$row[$j] = $Lang['General']['EmptySymbol'];
									$x .= $this->displayCell($j,nl2br($row[$j]), "table{$tablecolor}link","");
								}
							}
							
							$x .= "</tr>\n";
							$i++;
						}
					}
					$x .= "</table>";

					if($this->db_num_rows()<>0) $x .= $this->navigation_IP25("",$othercss_bottom);
					
					$this->db_free_result();
					
					return $x;

	        }	    

	        function displayFormat_UserMgmtStudentAccount($tablecolor="")
	        {
	                global $image_path, $LAYOUT_SKIN, $laccount, $Lang, $sys_custom;
	
	                $this->rs = $this->db_db_query($this->built_sql());
	                $x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn();
					$x .= "</thead>\n";
					
					$n_start = $this->n_start;
					if ($this->db_num_rows()==0)
					{
						$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						$i++;
						
						while($row = $this->db_fetch_array())
						{
							$x .= "<tr>\n";
							$x .= "<td>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++)
							{
								if($j==2) {
									if (isset($row["UserID"]) && $row["UserID"] > 0) {
										$temp = "<a href='javascript:goEdit(".$row["UserID"].")'>".(($row[$j]=="")?"---":$row[$j])."</a>";
									} else {
										$temp = "<a href='javascript:goEdit(".$row[$sys_custom['BIBA_AccountMgmtCust']?10:9 ].")'>".(($row[$j]=="")?"---":$row[$j])."</a>";
									}
									$x .= "<td>$temp</td>";	
								}
//								elseif($j==4){
//									$temp = $laccount->getParentListByStudentID($row[$j]);
//									
//									if($temp=="") $temp = "---";
//									$x .= "<td>$temp</td>";
//								}
								else 
								{
									if(trim($row[$j]) == "")
										$row[$j] = $Lang['General']['EmptySymbol'];
									$x .= $this->displayCell($j,nl2br($row[$j]), "table{$tablecolor}link","");
								}
							}
							
							$x .= "</tr>\n";
							$i++;
						}
					}
					$x .= "</table>";
					if($this->db_num_rows()<>0) $x .= $this->navigation_IP25("",$othercss_bottom);
					
					$this->db_free_result();
					
					return $x;

	        }	    

	        function displayFormat_UserMgmtStudentNoClass($tablecolor="")
	        {
	                global $image_path, $LAYOUT_SKIN, $sys_custom, $laccount, $Lang;
	                
// 	debug_pr($this->built_sql());
	                $this->rs = $this->db_db_query($this->built_sql());
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn();
					$x .= '</thead>';
					
					$column_offset = $sys_custom['BIBA_AccountMgmtCust']? 1:0;
					
					$n_start = $this->n_start;
					if ($this->db_num_rows()==0)
					{
						$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						$i++;
						$x .= "<tbody>";
						while($row = $this->db_fetch_array())
						{
							$x .= "<tr>\n";
							$x .= "<td>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++)
							{
								/*
								if($j==0) {
									$temp = "<a href='javascript:goEdit(".$row[3].")'>".(($row[$j]=="")?"---":$row[$j])."</a>";
									$x .= "<td>$temp</td>";	
								} else 
								*/
								if($j==(3+$column_offset) || $j==(4+$column_offset)) {
									$temp = explode("__SPLIT__",$row[$j]);
									$x .= "<td>".$temp[0]."</td>";
// 									$lastYearData = $laccount->getLastestYearClassOfStudentByID($row[$j]);
// 									$yearName = (count($lastYearData)==0) ? $Lang['General']['EmptySymbol'] : ($lastYearData['AcademicYearID']!="" ? $laccount->getAcademicYearNameByYearID($lastYearData['AcademicYearID']) : $lastYearData['AcademicYear']);
// 									$x .= "<td>".$yearName."</td>";
								}
// 								else if($j==4) {
// 									$className = (count($lastYearData)==0) ? $Lang['General']['EmptySymbol'] : ($lastYearData['YearClassID']!="" ? $laccount->getClassNameByClassID($lastYearData['YearClassID']) : $lastYearData['ClassName']);
// 									$x .= "<td>".$className."</td>";
// 								}
								else 
								{
									if(trim($row[$j]) == "")
										$row[$j] = "&nbsp;";
									$x .= $this->displayCell($j,nl2br($row[$j]), "","");
								}
							}
							
							$x .= "</tr>\n";
							$i++;
						}
					}
					$x .= "</tbody></table>";
					if($this->db_num_rows()<>0) $x .= $this->navigation_IP25("",$othercss_bottom);
					
					$this->db_free_result();
					
					return $x;

	        }	   
	        
	        function displayFormat_GroupSettingGroupList(){	        
	        	
				global $image_path, $lgroup, $libenroll, $PATH_WRT_ROOT,$plugin, $SchoolYear;
	                $i = 0;
	                $this->rs = $this->db_db_query($this->built_sql());
	                $width = ($width!="") ? $width : "100%";
					$ldb = new libdb();
	                $n_start = $this->n_start;

					$AdminNameLang = Get_Lang_Selection("Group_Concat(iu.ChineseName Order By  iu.ChineseName SEPARATOR '<br>') as Name","Group_Concat(iu.EnglishName Order By iu.EnglishName SEPARATOR '<br>') as Name");
					//$AcademicYearID = Get_Current_Academic_Year_ID();
					$AcademicYearID = $SchoolYear;
					$sql = "SELECT
								ig.GroupID,
								$AdminNameLang
							FROM
				                INTRANET_GROUP AS ig
								LEFT OUTER JOIN INTRANET_USERGROUP iug ON (ig.GroupID = iug.GroupID AND iug.RecordType='A')
								LEFT OUTER JOIN INTRANET_USER iu ON (iug.UserID = iu.UserID)
							WHERE
								ig.AcademicYearID = '".$AcademicYearID."' OR ig.AcademicYearID IS NULL
							GROUP BY ig.GroupID
								";
					$AdminNameAry = $ldb->returnResultSet($sql);
					$AdminNameAry = BuildMultiKeyAssoc($AdminNameAry, 'GroupID', 'Name',1,1 );

	                $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	                $x .= $this->displayColumn();
	                if ($this->db_num_rows()==0)
	                {
	                        $x .= "<tr class='table{$tablecolor}row2'><td height='25' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	                } else
	                {
	                        while ($row = $this->db_fetch_array())
	                        {
	                                $i++;
	                                $css = ($i%2) ? "1" : "2";
	                                $Semester = '';
	                                if ($i>$this->page_size)
	                                {
	                                        break;
	                                }
	                                $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
	                                $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
	                                for ($j=0; $j<$this->no_col-1; $j++)
	                                {
										if($j == 4)
										{
											$EnrolIDCond = '';
											$GroupID = $row[$j];
											$GroupType = $row[8];
											if($plugin['eEnrollment'] && $GroupType == 5)
											{
												$GroupInfoArr = $libenroll->Get_Group_Info_By_GroupID($GroupID);
												if(!empty($GroupInfoArr))
												{
													$GroupClubType = $GroupInfoArr['ClubType'];
													
													if($GroupClubType=='S')
													{
														$TermInfoArr = getCurrentAcademicYearAndYearTerm();
														$curYearTermID = $TermInfoArr['YearTermID'];
														
														$InvolvedSem = $libenroll->Get_Group_Involoved_Semester($GroupID);
														if(empty($Semester)) 
															$Semester = $InvolvedSem[0];
														$termfilterbar .= $libenroll->Get_Term_Selection("Semester", '', $Semester, $OnChange='this.form.pageNo.value=1; this.form.submit()',1 ,0 ,0);
														$termfilterbar .= "<input type='hidden' name='filter' value='$filter'>";
														
														$EnrolGroupID = $libenroll->Get_Club_Of_Semester($GroupID, $Semester);
													}
													else
													{
														$EnrolGroupID = $libenroll->Get_Club_Of_Semester($GroupID, '');
													}
													
													$GroupUsersList = $libenroll->Get_Member_List($EnrolGroupID,"club",0);
													$GroupUsers = count($GroupUsersList);
													
													$EnrolIDCond = " EnrolGroupID = '$EnrolGroupID' "; 	
												}
											}
												
											include_once($PATH_WRT_ROOT."includes/libgroup.php");
											$lgroup = new libgroup($GroupID);									
											$noOfGroupUser = $lgroup->getNumberGroupUsers( $EnrolIDCond, $SchoolYear);
											
											$link = "<a class=tablelink href=javascript:user($GroupID)>$noOfGroupUser</a>";
											$x .= "<td class=\"".$this->returnCellClassID()."\" align='center'>".stripslashes($link)."&nbsp;<br></td>\n";
										
										}
										else if ($j==3) {
											$GroupID = $row[4];
											$AdminName = $AdminNameAry["$GroupID"][0];
											$x .= $this->displayCell($j, $AdminName);
										}
										else	
                                        	$x .= $this->displayCell($j, $row[$j]);
	                                }
	                                $x .= "</tr>\n";
	                        }
	                }
	                if($this->db_num_rows()<>0 && $this->with_navigation) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	                $x .= "</table>\n";
	                $this->db_free_result();
	
	                return $x;
        	}

			function displayFormat_IP25_table($width="", $othercss1="", $othercss_bottom=""){
					global $image_path;
					
					$i = 0;
					### Add custDataset & custDataSetArr for eForm by FL 201706
					if ($this->custDataset) {
						$this->rs = $this->custDataSetArr;
					} else {
						$this->rs = $this->db_db_query($this->built_sql());
					}

					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn(); 
// 					2016-10-05 (Villa) #S105745
					$x .= "</thead>";
					if ($this->custDataset) {
						$numberOfRows = count($this->custDataSetArr);
					} else {
						$numberOfRows = $this->db_num_rows();
					}
					if ($numberOfRows==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						if ($this->custDataset) {
							foreach ($this->custDataSetArr as $row) {
								$i++;
								$css = ($i%2) ? "" : "2";
								if($i>$this->page_size) break;
								$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
								$x .= "<tr ".$trClass.">\n";
								$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
								for($j=0; $j<$this->no_col-1; $j++)
									$x .= $this->displayCell($j,$row[$j], $css);
								$x .= "</tr>\n";
							}
						} else {
							while($row = $this->db_fetch_array()) {
								$i++;
								$css = ($i%2) ? "" : "2";
								if($i>$this->page_size) break;
								
								$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
															
								$x .= "<tr ".$trClass.">\n";
								$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
								for($j=0; $j<$this->no_col-1; $j++)
								$x .= $this->displayCell($j,$row[$j], $css);
								$x .= "</tr>\n";
							}
						}
					}
					$x .= "</table>";
					// if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
					if($numberOfRows<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					if (!$this->custDataset) {
						$this->db_free_result();
					}
					return $x;
					
			}
			function displayFormat_Enrolment_Display_Event_ReplySlip_Cust($width="", $othercss1="", $othercss_bottom=""){
				global $image_path,$PATH_WRT_ROOT,$Lang,$eEnrollment, $plugin;
				include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
				include_once($PATH_WRT_ROOT."includes/libinterface.php");
				$libenroll = new libclubsenrol();
				$linterface = new interface_html();
				
				$i = 0;
				$EnrolEventRow = 8;
				if($plugin['payment']){
					$EnrolEventRow = $EnrolEventRow+1;
				}
				$this->rs = $this->db_db_query($this->built_sql());
				$width = ($width!="") ? $width : 560;
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
				$x .= $this->displayColumn();
				// 					2016-10-05 (Villa) #S105745
				$x .= "</thead>";
				
				if ($this->db_num_rows()==0){
					$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				} else {
					while($row = $this->db_fetch_array()) {
						$i++;
						$css = ($i%2) ? "" : "2";
						if($i>$this->page_size) break;
						
						$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
						
						$x .= "<tr ".$trClass.">\n";
						$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
						for($j=0; $j<$this->no_col-1; $j++){
// 							$x .= $this->displayCell($j,$row[$j], $css);
							
							if($j==$EnrolEventRow){
								#ReplySlip Signed/ Total
								$EnrolEventID = $row[$EnrolEventRow];
								$ReplySlipRelation = $libenroll->getReplySlipEnrolGroupRelation('',$EnrolEventID,2);
								$ReplySlipRelation = $ReplySlipRelation[0];
								$ReplySlipGroupMappingID = $ReplySlipRelation['ReplySlipGroupMappingID'];
								$ReplySlipID = $ReplySlipRelation['ReplySlipID'];
								
								if($ReplySlipGroupMappingID){
									$ReplySlipReply = $libenroll->getReplySlipReply('',$ReplySlipGroupMappingID);
									$MemberList = $libenroll->getEventStudentInfo($EnrolGroupID);
									$repliedStudentId = Get_Array_By_Key( $ReplySlipReply, 'StudentID');
									$memberStudentId = Get_Array_By_Key( $MemberList, 'StudentID');
									$repliedStudentIdArr = array_intersect($repliedStudentId, $memberStudentId);
									$NumOfSigned = count($repliedStudentIdArr);
// 									$NumOfSigned = count($ReplySlipReply);
									$NumOfMember = count($memberStudentId);
// 									$ReplySlip_SignTotal = $linterface->Get_Thickbox_Link('820', '1640', "",$eEnrollment['replySlip']['ReplySlipDetail'], "onLoadReplySlipThickBox($ReplySlipGroupMappingID,$EnrolEventID,$ReplySlipID)","FakeLayer",$NumOfSigned.'/'.$NumOfMember);
									$ReplySlip_SignTotal = '<a href="javascript:void(0);" onclick="onLoadThickBox('.$ReplySlipGroupMappingID.','.$EnrolEventID.','.$ReplySlipID.')";>'.$NumOfSigned.'/'.$NumOfMember.'</a>';
								}else{
									$ReplySlipReply = '';
									$NumOfSigned = '0';
									$ReplySlip_SignTotal = '-';
								}
								$x .= $this->displayCell($j,$ReplySlip_SignTotal, $css);
							}else{
								$x .= $this->displayCell($j,$row[$j], $css);
							}
						}
							$x .= "</tr>\n";
					}
				}
				$x .= "</table>";
				if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
				//if($this->db_num_rows()<>0) $x .= $this->navigation();
				$this->db_free_result();
				return $x;
				
			}
			
			function displayFormat_eLibPlus_Circulation_Detail_Table($width="", $othercss1="", $othercss_bottom=""){
					global $image_path;
					
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn(); 
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							
							$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
														
							$x .= "<tr ".$trClass.">\n";
							$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++){
								if($j==5||$j==6||$j==7||$j==8){
									$this_row = explode(',',$row[$j]);
								}else{
									$this_row[0] = $row[$j];
								}
								
								$x .= $this->displayCell($j,$this_row[0], $css);
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}
			
			function displayFormat_StudentRegistry_StudentList($width=""){
					global $image_path, $lsr;
					
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list'>\n<thead>\n";
					$x .= $this->displayColumn();
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							$x .= "<tr>\n";
							$x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++) {
								/*
								$userid = ($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"]) ? $row[10] : $row[9];
								$modifyBy = ($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"]) ? $row[11] : $row[10];
								$modifyByColumn = ($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"]) ? 7 : 6;
								$AcademicYearID = ($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"]) ? $row[12] : $row[11];
								*/
								//$userid = $row[10];
								//$modifyBy = $row[11];
								//$AcademicYearID = $row[12];
								
								if($j==2) {
									$x .= "<td><a href='view.php?AcademicYearID=".$row[12]."&userID=".$row[10]."'>".$row[$j]."</a></td>";
								} else if($j==3) {
									$x .= "<td><a href='view.php?AcademicYearID=".$row[12]."&userID=".$row[10]."'>".$row[$j]."</a></td>";
								} else if($j==6) {
									if($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"])
										$x .= $this->displayCell($j,$row[$j], $css);
								} else if($j==7) {
									$x .= "<td>".$row[$j];
									$x .= ($row[11]!="") ? " (".$lsr->RETRIEVE_MODIFIED_BY_INFO($row[11]).")" : "";
									$x .= "</td>";
								} else {
									$x .= $this->displayCell($j,$row[$j], $css);
								}
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					if($this->db_num_rows()<>0) $x .= $this->navigation_IP25();
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}
			
			function displayFormat_StudentRegistry_StudentList_HK($width=""){
					global $image_path, $lsr;
					
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list'>\n<thead>\n";
					$x .= $this->displayColumn();
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							$x .= "<tr>\n";
							$x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++) {
								if($j==2) {
									$x .= "<td><a href='view.php?AcademicYearID=".$row[8]."&studentID=".$row[6]."'>".$row[$j]."</a></td>";
								} else if($j==3) {
									$x .= "<td><a href='view.php?AcademicYearID=".$row[8]."&studentID=".$row[6]."'>".$row[$j]."</a></td>";
								} else if($j==4) {
									$x .= "<td>".$row[$j];
									$x .= ($row[7]!="") ? " (".$lsr->RETRIEVE_MODIFIED_BY_INFO($row[7]).")" : "";
									$x .= "</td>";
								} else {
									$x .= $this->displayCell($j,$row[$j], $css);
								}
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					if($this->db_num_rows()<>0) $x .= $this->navigation_IP25();
					$this->db_free_result();
					return $x;
					
			}
			
			function displayFormat_StudentRegistry_StudentList_Malaysia($width=""){
					global $image_path, $lsr;
					
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list'>\n<thead>\n";
					$x .= $this->displayColumn();
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							$x .= "<tr>\n";
							$x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++) {

								if($j==2) {
									$x .= "<td><a href='view.php?AcademicYearID=".$row[11]."&userID=".$row[9]."'>".$row[$j]."</a></td>";
								} else if($j==3) {
									$x .= "<td><a href='view.php?AcademicYearID=".$row[11]."&userID=".$row[9]."'>".$row[$j]."</a></td>";
								} else if($j==5) {
									if($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"])
										$x .= $this->displayCell($j,$row[$j], $css);
								} else if($j==6) {
									$x .= "<td>".$row[$j];
									$x .= ($row[10]!="") ? " (".$lsr->RETRIEVE_MODIFIED_BY_INFO($row[10]).")" : "";
									$x .= "</td>";
								} else {
									$x .= $this->displayCell($j,$row[$j], $css);
								}
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					if($this->db_num_rows()<>0) $x .= $this->navigation_IP25();
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}			
			
			function displayFormat_Malaysia_RegistryStatus($width=""){
					global $image_path, $Lang, $lsr;
					
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list'>\n<thead>\n";
					$x .= $this->displayColumn();
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							
							$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
							
							$x .= "<tr ".$trClass.">\n";
							$x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++) {
								if($j==3 || $j==4) {
									$x .= "<td><a href='../information/view.php?AcademicYearID=".$row[13]."&userID=".$row[14]."'>".$row[$j]."</a></td>";
								} else if($j==5) {
									if(is_numeric($row[$j])) { 
										switch($row[$j]) {
											case 1 : $text = $Lang['StudentRegistry']['StatusLeft']; break;
											case 2 : $text = $Lang['StudentRegistry']['StatusSuspended']; break;
											case 3 : $text = $Lang['StudentRegistry']['StatusResume']; break;											
										}
										$x .= "<td>$text</td>";
									} else 
										$x .= $this->displayCell($j,$row[$j], $css);
								} else if($j==6) {
									if(is_numeric($row[$j])) 
										$x .= "<td>".$Lang['StudentRegistry']['ReasonAry'][$row[$j]]."</td>";
									else 
										$x .= $this->displayCell($j,$row[$j], $css);
								} else if($j==9) {
									if($row[$j]!="---") {
										$userinfo = $lsr->getUserInfoByID($row[12]);
										$x .= "<td>".$row[$j]." (".Get_Lang_Selection($userinfo['ChineseName'], $userinfo['EnglishName']).")</td>";						
									} else {
										$x .= "<td>---</td>";
									}
								} 
								else {
									$x .= $this->displayCell($j,$row[$j], $css);
								}
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					if($this->db_num_rows()<>0) $x .= $this->navigation_IP25();
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}
			
	        function displayFormat_ReadingScheme_RecommendBookCoverView($width="", $othercss1="", $othercss_bottom="")
	        {
	        	global $PATH_WRT_ROOT, $image_path, $intranet_root, $LAYOUT_SKIN, $Lang, $ReadingGardenLib, $ReadingGardenUI;
				
				$CategoryAnswerSheet = $ReadingGardenLib->Get_Category_Answer_Sheet_Mapping();
				$CategoryAnswerSheet = BuildMultiKeyAssoc($CategoryAnswerSheet, "CategoryID", "AnswerSheetID", 1, 1);
				
	        	$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$width = ($width!="") ? $width : 560;
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
				$x .= $this->displayColumn(); 
				
				$TBReadingRecordHeight = "400";
				$TBReadingRecordWidth = "500";
				$TBBookReportFileHeight = "250";
				$TBBookReportFileWidth = "500";
				$TBAnswerSheetHeight = "600";
				$TBAnswerSheetWidth = "800";
				$TBOnlineWritingHeight = "350";
				$TBOnlineWritingWidth = "600";
				
				if ($this->db_num_rows()==0){
						$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				} else {
					while($row = $this->db_fetch_array()) {
						
						$i++;
						$css = ($i%2) ? "" : "2";
						if($i>$this->page_size) break;
						
						# Book Info
						$category_id = $row['CategoryID']?$row['CategoryID']:0;
						$book_id = $row['BookID'];
						$book_name = htmlspecialchars($row['BookName'], ENT_QUOTES);
						$author = $row['Author']?htmlspecialchars($row['Author'],ENT_QUOTES):$Lang['General']['EmptySymbol'];
						$call_number = $row['CallNumber']?htmlspecialchars($row['CallNumber'],ENT_QUOTES):$Lang['General']['EmptySymbol'];
						$publisher = $row['Publisher']?htmlspecialchars($row['Publisher'],ENT_QUOTES):$Lang['General']['EmptySymbol'];
						$description = $row['Description']?htmlspecialchars($row['Description'],ENT_QUOTES):$Lang['General']['EmptySymbol'];
//						$cover_image_path = trim($row['BookCoverImage'])==''?($image_path."/".$LAYOUT_SKIN."/10x10.gif"):($ReadingGardenLib->BookCoverImagePath."/".$row['BookCoverImage']);
						$cover_image = $ReadingGardenLib->Get_Book_Cover_Image($row['BookCoverImage'],100,120);
						if( $row['CategoryName'])
						{
							$category_name = htmlspecialchars($row['CategoryName'],ENT_QUOTES);
							$category_code = htmlspecialchars($row['CategoryCode'],ENT_QUOTES);
							$category = $category_code."&nbsp;".$category_name;
							$category = $category;
						}
						else
							$category = $Lang['ReadingGarden']['UnclassifiedCategory'];
						# Reading Record
						$reading_record_id = trim($row['ReadingRecordID']);
						$book_report_id = trim($row['BookReportID']);
						$student_id = trim($row['StudentID']);
						
						# Assigned Reading 
						$assigned_reading_id = trim($row['AssignedReadingID']);
						// need assigned target > 0
						$assigned_reading_target_cnt = $row['NumOfTarget'];
						// if have assigned attachment, need to upload book report
						$assigned_attachment = trim($row['AssignedAttachment']);
						// require file upload
						$book_report_required = trim($row['BookReportRequired']);
						// if have assigned answer sheet, need to do answer sheet 
						$assigned_answer_sheet = trim($row['AssignedAnswerSheet']);
						// if have assigned online writing, need to do online writing 
						$assigned_online_writing = trim($row['AssignedOnlineWriting']);
						# Answer sheet / Report
						// Book default answer sheet
						$default_answer_sheet = trim($row['DefaultAnswerSheet']);
						// book report answer sheet answer
						$report_answer_sheet = trim($row['AnswerSheet']);
						// uploaded book report file
						$book_report_attachment = trim($row['Attachment']);
						// Online Writing
						$online_writing = trim($row['OnlineWriting']);
						# Grade
						$grade = trim($row['Grade']);
						
						$TBAddReadingRecordButton = '';
						$ReadingRecordReaded = '';
						$TBEditReadingRecordButton = '';
						$RemoveReadingRecordButton = '';
						$HandinBookReportButton = '';
						$DoAnswerSheetButton = '';
						$EditAnswerSheetButton = '';
						$ViewAnswerSheetButton = '';
						$DeleteAnswerSheetButton = '';
						$DeleteAnswerSheetButton = '';
						$EditOnlineWritingButton = '';
						$DeleteOnlineWritingButton = '';
						$BookReportFileLink = '';
						$RemoveBookReportFileButton = '';
						$ViewMarkReportBtn = '';
						
						$CallBack = "Get_Recommend_Book_CoverView_Table()";
						if($reading_record_id == ''){// Reading record not submitted yet
							$TBAddReadingRecordButton = $ReadingGardenUI->Get_Thickbox_Link($TBReadingRecordHeight, $TBReadingRecordWidth, "book_status_unread", $Lang['ReadingGarden']['AddReadingRecord'], "Get_Reading_Record_Edit_Form($book_id,1,'','$CallBack');", "FakeLayer", $Lang['ReadingGarden']['AddReadingRecord'], "");
						}else{// Submitted reading record
							$ReadingRecordReaded = "<a class=\"book_status_read\" href=\"javascript:void(0);\" onmouseover=\"View_Reading_Record(this, '$reading_record_id');\" onmouseout=\"Remove_Reading_Record_Layer();\">".$Lang['ReadingGarden']['Readed']."</a>";
							if($grade=="")
							{
								$TBEditReadingRecordButton = $ReadingGardenUI->Get_Thickbox_Link($TBReadingRecordHeight, $TBReadingRecordWidth, "", $Lang['ReadingGarden']['EditReadingRecord'], "Get_Reading_Record_Edit_Form($book_id,0,$reading_record_id,'$CallBack');", "FakeLayer", " [".$Lang['Btn']['Edit']."] ", "");
								$RemoveReadingRecordButton = "<a href=\"javascript:void(0);\" title=\"".$Lang['ReadingGarden']['DeleteReadingRecord']."\" onclick=\"Delete_Reading_Record('$reading_record_id','$CallBack');return false;\">[".$Lang['Btn']['Delete']."]</a>";
								if($book_report_id!=''){
										if($book_report_attachment!=''){
											//$file_link = $ReadingGardenLib->ModuleFilePath."/book_report/u".$student_id."/r".$book_report_id."/".$book_report_attachment;
										//	$file_link = $PATH_WRT_ROOT."home/download_attachment.php?target=".$intranet_root.$ReadingGardenLib->ModuleFilePath."/book_report".urlencode($book_report_attachment);
											$target_e = getEncryptedText($intranet_root.$ReadingGardenLib->ModuleFilePath."/book_report".$book_report_attachment);
											$file_link = $PATH_WRT_ROOT."home/download_attachment.php?target_e=".$target_e;
											$BookReportFileLink = "<a href=\"".$file_link."\" target=\"_blank\" class=\"book_report_edit\">".$Lang['ReadingGarden']['ViewBookReport']."</a>";
											$RemoveBookReportFileButton = "<a href=\"javascript:void(0);\" title=\"".$Lang['ReadingGarden']['DeleteBookReport']."\" onclick=\"Delete_Book_Report_File('$book_report_id','$CallBack');return false;\">[".$Lang['Btn']['Delete']."]</a>";
										}
										else if((trim($assigned_reading_id)!='' && $book_report_required) || (trim($assigned_reading_id)=='' && $ReadingGardenLib->Settings['FileUpload']==1)){
											$HandinBookReportButton = $ReadingGardenUI->Get_Thickbox_Link($TBBookReportFileHeight, $TBBookReportFileWidth, "book_report_edit", $Lang['ReadingGarden']['HandInBookReport'], "Get_Book_Report_Edit_Form(0,1,'$reading_record_id','$book_report_id','$assigned_reading_id','$CallBack');", "FakeLayer", $Lang['ReadingGarden']['HandInBookReport'], "");
										}
										
										if($report_answer_sheet!=''){
											// show answer
		//									if($grade==""){ // not marked by teacher, student can view/edit/delete
												$ViewAnswerSheetButton = "<a href=\"javascript:void(0);\" title=\"".$Lang['ReadingGarden']['ViewAnswerSheet']."\" onclick=\"newWindow('".$PATH_WRT_ROOT."home/eLearning/reading_garden/answer_sheet.php?reading_record_id=".$reading_record_id."&book_report_id=".$book_report_id."&assigned_reading_id=".$assigned_reading_id."',10);\">[".$Lang['Btn']['View']."]</a>";
												$EditAnswerSheetButton = $ReadingGardenUI->Get_Thickbox_Link($TBAnswerSheetHeight, $TBAnswerSheetWidth, "book_report_done", $Lang['ReadingGarden']['EditAnswerSheet'], "Get_Book_Report_Edit_Form(1,0,'$reading_record_id','$book_report_id','$assigned_reading_id','$CallBack','$category_id');", "FakeLayer", $Lang['ReadingGarden']['EditAnswerSheet'], "");
												$DeleteAnswerSheetButton = "<a href=\"javascript:void(0);\" title=\"".$Lang['ReadingGarden']['DeleteAnswerSheet']."\" onclick=\"Delete_Answer_Sheet('$book_report_id', '$CallBack');return false;\">[".$Lang['Btn']['Delete']."]</a>";
		//									}else{ // marked by teacher, student can view only
		//										$EditAnswerSheetButton = $ReadingGardenUI->Get_Thickbox_Link($TBAnswerSheetHeight, $TBAnswerSheetWidth, "book_report_done", $Lang['ReadingGarden']['ViewAnswerSheet'], "Get_Book_Report_Edit_Form(1,0,'$reading_record_id','$book_report_id','$assigned_reading_id','$CallBack','$category_id');", "FakeLayer", $Lang['ReadingGarden']['ViewAnswerSheet'], "");
		//									}
										}else if(!($assigned_reading_id!='' && $assigned_answer_sheet=='') && (($assigned_reading_target_cnt>0 && $assigned_answer_sheet!='') || $default_answer_sheet!='' || $CategoryAnswerSheet[$category_id] )){
											// if either have assigned answer sheet or book default answer sheet or category default answer sheet, do it
											$DoAnswerSheetButton = $ReadingGardenUI->Get_Thickbox_Link($TBAnswerSheetHeight, $TBAnswerSheetWidth, "book_report_done", $Lang['ReadingGarden']['DoAnswerSheet'], "Get_Book_Report_Edit_Form(1,1,'$reading_record_id','$book_report_id','$assigned_reading_id','$CallBack','$category_id');", "FakeLayer", $Lang['ReadingGarden']['DoAnswerSheet'], "");
										}
										
										if($online_writing!=''){
											$EditOnlineWritingButton = $ReadingGardenUI->Get_Thickbox_Link($TBOnlineWritingHeight, $TBOnlineWritingWidth, "book_report_done", $Lang['ReadingGarden']['OnlineWriting'], "Get_Book_Report_Edit_Form(2,0,'$reading_record_id','$book_report_id','$assigned_reading_id','$CallBack');", "FakeLayer", $Lang['ReadingGarden']['EditOnlineWriting'], "");
											$DeleteOnlineWritingButton = "<a href=\"javascript:void(0);\" title=\"".$Lang['ReadingGarden']['DeleteOnlineWriting']."\" onclick=\"Delete_Online_Writing('$book_report_id','$CallBack');return false;\">[".$Lang['Btn']['Delete']."]</a>";
										}else if((trim($assigned_reading_id)!='' && $assigned_online_writing!=-1) || (trim($assigned_reading_id)=='' && $ReadingGardenLib->Settings['OnlineWriting']==1)){
											$EditOnlineWritingButton = $ReadingGardenUI->Get_Thickbox_Link($TBOnlineWritingHeight, $TBOnlineWritingWidth, "book_report_done", $Lang['ReadingGarden']['OnlineWriting'], "Get_Book_Report_Edit_Form(2,1,'$reading_record_id','$book_report_id','$assigned_reading_id','$CallBack');", "FakeLayer", $Lang['ReadingGarden']['OnlineWriting'], "");
										}

								
								}else{
									
									if((trim($assigned_reading_id)!='' && $book_report_required) || (trim($assigned_reading_id)=='' && $ReadingGardenLib->Settings['FileUpload']==1)){
										$HandinBookReportButton = $ReadingGardenUI->Get_Thickbox_Link($TBBookReportFileHeight, $TBBookReportFileWidth, "book_report_edit", $Lang['ReadingGarden']['HandInBookReport'], "Get_Book_Report_Edit_Form(0,1,'$reading_record_id','$book_report_id','$assigned_reading_id','$CallBack');", "FakeLayer", $Lang['ReadingGarden']['HandInBookReport'], "");
									}
									
									if(!($assigned_reading_id!='' && $assigned_answer_sheet=='') && (($assigned_reading_target_cnt>0 && $assigned_answer_sheet!='') || $default_answer_sheet!='' || $CategoryAnswerSheet[$category_id] )){
										// if either have assigned answer sheet or book default answer sheet or category default answer sheet, do it
										$DoAnswerSheetButton = $ReadingGardenUI->Get_Thickbox_Link($TBAnswerSheetHeight, $TBAnswerSheetWidth, "book_report_done", $Lang['ReadingGarden']['DoAnswerSheet'], "Get_Book_Report_Edit_Form(1,1,'$reading_record_id','$book_report_id','$assigned_reading_id','$CallBack','$category_id');", "FakeLayer", $Lang['ReadingGarden']['DoAnswerSheet'], "");
									}
									if((trim($assigned_reading_id)!='' && $assigned_online_writing!=-1) || (trim($assigned_reading_id)=='' && $ReadingGardenLib->Settings['OnlineWriting']==1)){
										$EditOnlineWritingButton = $ReadingGardenUI->Get_Thickbox_Link($TBOnlineWritingHeight, $TBOnlineWritingWidth, "book_report_done", $Lang['ReadingGarden']['OnlineWriting'], "Get_Book_Report_Edit_Form(2,1,'$reading_record_id','$book_report_id','$assigned_reading_id','$CallBack');", "FakeLayer", $Lang['ReadingGarden']['OnlineWriting'], "");
									}
								}
							}
							else
							{
								$ViewMarkReportBtn = "<a class=\"book_report_done\" href=\"javascript:void(0);\" title=\"".$Lang['ReadingGarden']['ViewBookReport']."\" onclick=\"js_View_Mark_Report('$reading_record_id')\">".$Lang['ReadingGarden']['ViewBookReport']."</a>";
							}
								
						}
						
						$x .= "<tr>\n<td>\n";
						$x .= "<div class=\"book_detail_w_cover\">
		                      	<div class=\"book_detail_cover\">
		                      		$cover_image
		                       	</div>";
						$x .= " <div class=\"book_detail\">
                            		<div class=\"book_detail_info\">
                                		<h1>$book_name</h1>
                                		<h2 class=\"book_author\"><em>".$Lang['ReadingGarden']['Author'].":</em><span>$author</span></h2>
                                		<h2 class=\"book_publisher\"><em>".$Lang['ReadingGarden']['Publisher'].":</em><span>$publisher</span></h2>
                                		<h2 class=\"book_ISBN\"><em>".$Lang['ReadingGarden']['CallNumber'].":</em><span>$call_number</span></h2>
                                 		<p class=\"spacer\"></p>
                                	</div>
                                 	<!--<h2 class=\"book_level\"><em>level:</em><span>P6</span></h2>-->
                                	<h2 class=\"book_cat\"><em>".$Lang['ReadingGarden']['Category'].":</em><span>".$category."</span></h2>
                                 	<p class=\"spacer\"></p>
                              		<h2 class=\"book_desc\"><em>".$Lang['ReadingGarden']['Description'].":</em><span>".nl2br($description)."</span></h2>
                                	<p class=\"spacer\"></p>";
                        # Like Function
                        $x .= $ReadingGardenUI->Get_Like_Comment_Div(FUNCTION_NAME_BOOK, $book_id);
                        
                        $x .= '<p class="spacer"></p>';
                        if($ReadingGardenLib->isStudent)
                        {     		
	                        $x .= "<div>";
							//$x .= "	<h2 class=\"book_level\"><em> ".$Lang['ReadingGarden']['MyRecord']." :</em>";
							$x .= "<h2><em> ".$Lang['ReadingGarden']['MyRecord']." :</em></h2>";
							
							//Reading Record
							if($TBAddReadingRecordButton || $ReadingRecordReaded || $TBEditReadingRecordButton || $RemoveReadingRecordButton)
							{
								$x .= "<span style=\"float:left;margin-right:25px;\">";
								$x .= $TBAddReadingRecordButton;
								$x .= $ReadingRecordReaded;
								$x .= "<span style=\"padding-left:20px\">";
								$x .= $TBEditReadingRecordButton;
								$x .= $RemoveReadingRecordButton;
								$x .= "</span>";
								$x .= "</span>";
							}

							// View Mark Report
							if($ViewMarkReportBtn)
							{
								$x .= "<span style=\"float:left;margin-right:25px;\">";
								$x .= $ViewMarkReportBtn;
								$x .= "<span style=\"padding-left:20px\">";
								$x .= "</span>";
								$x .= "</span>";
							}							
							
							// Upload File
							if($HandinBookReportButton || $BookReportFileLink || $RemoveBookReportFileButton)
							{
								$x .= "<span style=\"float:left;margin-right:25px;\">";
								$x .= $HandinBookReportButton;
								$x .= $BookReportFileLink;
								$x .= "<span style=\"padding-left:20px\">";
								$x .= $RemoveBookReportFileButton;
								$x .= "</span>";
								$x .= "</span>";
							}
							
							//Answer Sheet
							if($DoAnswerSheetButton || $EditAnswerSheetButton || $ViewAnswerSheetButton || $DeleteAnswerSheetButton)
							{
								$x .= "<span style=\"float:left;margin-right:25px;\">";
								$x .= $DoAnswerSheetButton;
								$x .= $EditAnswerSheetButton;
								$x .= "<span style=\"padding-left:20px\">";
								$x .= $ViewAnswerSheetButton;
								$x .= $DeleteAnswerSheetButton;
								$x .= "</span>";
								$x .= "</span>";
							}
							
							//Online Writing
							if($EditOnlineWritingButton || $DeleteOnlineWritingButton)
							{
								$x .= "<span style=\"float:left;margin-right:25px;\">";
								$x .= $EditOnlineWritingButton;
								$x .= "<span style=\"padding-left:20px\">";
								$x .= $DeleteOnlineWritingButton;
								$x .= "</span>";
								$x .= "</span>";
							}
							
							$x .= "</div>";
                        }
						$x .= " </div>
				                <p class=\"spacer\"></p>
				             </div>";
						$x .= "</td>\n</tr>\n";
					}
				}
				$x .= "</thead></table>";
				if($this->db_num_rows()<>0) $x .= $this->navigation_IP25("",$othercss_bottom);
				$this->db_free_result();
				return $x;
	        }
	       	
			function displayFormat_ReadingScheme_MyReadingBook()
			{
				global $image_path,$ReadingGardenUI, $ReadingGardenLib, $Lang, $PATH_WRT_ROOT;
				
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table class=' mybook_record_table'>\n<thead>\n";
				$x .= $this->displayColumn(); 
				$x .= "</thead>";
				
				if ($this->db_num_rows()==0){
						$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				} else {
					while($row = $this->db_fetch_array()) {
						$i++;
						if($i>$this->page_size) break;
						
						$thisInfo  = $row;
						$img = $ReadingGardenLib->Get_Book_Cover_Image($thisInfo['BookCoverImage'],45,54);
							
						$Answer = '';
						$ViewReport = '';	
						if($thisInfo['RecordStatus'])
						{
							$trclass = "row_read_book";
							$aclass = "book_status_read";
//							if($thisInfo['Attachment'])
//								$ViewReport = '<a target="_blank" class="book_report_done" href="'.$ReadingGardenLib->BookReportPath.$thisInfo['Attachment'].'">'.$Lang['ReadingGarden']['ViewBookReport'].'</a>';
//							if($thisInfo['AnswerSheet'])
//								$Answer = "<a class=\"book_report_done\" href=\"javascript:void(0);\" title=\"".$Lang['ReadingGarden']['ViewAnswerSheet']."\" onclick=\"newWindow('".$PATH_WRT_ROOT."home/eLearning/reading_garden/answer_sheet.php?reading_record_id=".$thisInfo['ReadingRecordID']."&book_report_id=".$thisInfo['BookReportID']."&assigned_reading_id=".$thisInfo['AssignedReadingID']."',10);\">".$Lang['ReadingGarden']['ViewAnswerSheet']."</a>";
							if($thisInfo['Attachment'] || $thisInfo['AnswerSheet'] || $thisInfo['OnlineWriting'])
							{
								$ViewReport =  (trim($thisInfo['Grade'])!="") ? $thisInfo['Grade'] . "<br />": "";
								$ViewReport .= '<a class="book_report_done" href="javascript:void(0);" onclick="js_View_Mark_Report('.$thisInfo['ReadingRecordID'].');">'.$Lang['ReadingGarden']['ViewBookReport'].'</a>';
							} else
							{
								$ViewReport = $Lang['General']['EmptySymbol'];
							}
							$ReadStatus = $Lang['ReadingGarden']['Read'];
							
						}
						else
						{
							$trclass = "row_unread_book";
							$aclass = "book_status_unread";
							$ViewReport = $Lang['General']['EmptySymbol'];
							$ReadStatus = $Lang['ReadingGarden']['Unread'];
						}

						$FullBookTitle = $ReadingGardenUI->Format_Book_Display($thisInfo);
//						$BookTitle = $FullBookTitle;
						
						$x .= "<tr ".$trclass.">\n";
							$x .= "<td>".($n_start+$i)."</td>\n";
							$x.= '<td>'."\n";
								$x.= '<div class="book_list_item reading_recommend_book_list">'."\n";
									$x.= '<div class="recommend_book_cover " >'."\n";
										$x.= '<a href="javascript:js_View_Book_Detail('.$thisInfo['BookID'].');">'.$img.'</a>'."\n";
									$x.= '</div>'."\n";
									$x.= '<div class="recommend_book_detail">'."\n";
										$x.= '<h2>'.$FullBookTitle.'</h2>'."\n";
										$x .= $ReadingGardenUI->Get_Like_Comment_Div(FUNCTION_NAME_BOOK, $thisInfo['BookID'], '  ');
									$x.= '</div>'."\n";
									$x.= '<p class="spacer"></p>'."\n";
								$x.= '</div>'."\n";
							$x.= '</td>'."\n";
//							$x.= '<td><a class="'.$aclass.'" >'.$ReadStatus.'</a> </td>'."\n";
							$x.= '<td>'.($thisInfo['Author']?intranet_htmlspecialchars($thisInfo['Author']):$Lang['General']['EmptySymbol']).' </td>'."\n";
							$x.= '<td>'.($thisInfo['Publisher']?intranet_htmlspecialchars($thisInfo['Publisher']):$Lang['General']['EmptySymbol']).' </td>'."\n";
							$x.= '<td>'.($thisInfo['CategoryName']?intranet_htmlspecialchars($thisInfo['CategoryName']):$Lang['General']['EmptySymbol']).' </td>'."\n";
							$x.= '<td>'.$thisInfo['StartDate'].' </td>'."\n";
							$x.= '<td>'.$thisInfo['FinishedDate'].' </td>'."\n";
							$x.= '<td>'.$thisInfo['NumberOfHours'].' </td>'."\n";
							$x.= '<td>'.$ViewReport.' </td>'."\n";
							$x.= '<td><input type="checkbox" name="BookIDArr[]" value="'.$thisInfo['BookID'].'"></td>'."\n";
						$x .= "</tr>\n";
					}
				}
				$x .= "</table>";
				if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);

				$this->db_free_result();
				return $x;
					
			}
			
			function displayFormat_OEA_StudentList($width="", $othercss1="", $othercss_bottom=""){
					global $image_path, $oea_cfg, $PATH_WRT_ROOT;
					
					include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");
					$liboea_setting = new liboea_setting();
					include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
					$liboea = new liboea();	
					include_once($PATH_WRT_ROOT."includes/portfolio25/oea/oeaConfig.inc.php");
					$maxOeaItem = $oea_cfg["OEA_MaxOEAItem"];
					
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn(); 
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							
							$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
							
							$x .= "<tr ".$trClass.">\n";
							$x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++) {
								if($j==1) {
									$cat = $liboea_setting->get_OEA_Item($row[$j]);
									//debug_pr($cat);
									//$x .= "<td>".$cat['CatName']."</td>";
									$x .= "<td>".$cat[$row[$j]]['CatName']."</td>";
								}
								else if($j==2) {
									$x .= "<td>".$oea_cfg["OEA_Role_Type"][$row[$j]]."</td>";
								}
								else if($j==3) {
									$x .= "<td>".$oea_cfg["OEA_Achievement_Type"][$row[$j]]."</td>";
								}
								else if($j==6) {
									$x .= "<td>".$liboea->getApprovalStatusImage($liboea->getOEAItemInternalCode(), $row[$j])
									."</td>";	
								}
								else {
									$x .= $this->displayCell($j,$row[$j], $css);
								}
							}
							$x .= "</tr>\n";
						}
						
						for($a=0; $a<($maxOeaItem-$this->db_num_rows()); $a++) {
							$x .= "<tr>
										<td height='30'>".($this->db_num_rows()+$a+1)."</td>
										<td height='30'>&nbsp;</td>
										<td height='30'>&nbsp;</td>
										<td height='30'>&nbsp;</td>
										<td height='30'>&nbsp;</td>
										<td height='30'>&nbsp;</td>
										<td height='30'>&nbsp;</td>
										<td height='30'>&nbsp;</td>
										<td height='30'>&nbsp;</td>
									</tr>";
						}
					}
					$x .= "</thead></table>";
					/*
					if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) 
						$x .= $this->navigation_IP25("",$othercss_bottom);
						*/
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}
			
			function displayFormat_ReadingSchemeReportComment()
			{
				global $image_path,$ReadingGardenUI, $ReadingGardenLib, $Lang, $PATH_WRT_ROOT;
				
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$n_start = $this->n_start;
				$x = '';
				if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
				$x .= "<table width='100%'>\n";
				
				if ($this->db_num_rows()==0){
						$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				} else {
					while($row = $this->db_fetch_array()) {
						$i++;
						if($i>$this->page_size) break;
						
						$thisInfo  = $row;
						$x .= $ReadingGardenUI->Get_Book_Report_Comment_Bubble($thisInfo);

					}
				}
				$x .= "</table>";
				if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);

				$this->db_free_result();
				return $x;
			}
			
			function displayFormat_ReadingScheme_RecommendBookReport($width="", $othercss="", $othercss_bottom="")
			{
				global $image_path,$ReadingGardenUI, $ReadingGardenLib, $Lang, $PATH_WRT_ROOT, $intranet_root;
				
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table class='$othercss'>\n";
				$x .= $this->displayColumn();
				
				if ($this->db_num_rows()==0){
						$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				} else {
					while($row = $this->db_fetch_array()) {
						$i++;
						$css = ($i%2) ? "" : "2";
						if($i>$this->page_size) break;
						
						$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
						
						$x .= "<tr ".$trClass.">\n";
						$x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
						$x .= "<td class=tableContent$css><a href=\"javascript:js_View_Book_Detail(".$row['BookID'].");\">".htmlspecialchars($row['BookInfo'])."</a></td>\n";
						//$x .= "<td class=tableContent$css><a href=\"".$PATH_WRT_ROOT."home/download_attachment.php?target=".$intranet_root.$ReadingGardenLib->BookReportPath.urlencode($row['BookReport'])."\" target=\"_blank\">".htmlspecialchars(basename($row['BookReport']))."</a></td>\n";
//						$x .= "<td class=tableContent$css><a href=\"javascript:newWindow('".$PATH_WRT_ROOT."home/eLearning/reading_garden/book_report_marking.php?RecommendOnly=1&ReadingRecordID=".$row['ReadingRecordID']."',32);\">".$ReadingGardenUI->Get_View_Image()."</a></td>\n";
						$x .= "<td class=tableContent$css><a href=\"javascript:void(0);\" onclick=\"js_View_Mark_Report(".$row['ReadingRecordID'].",1)\">".$ReadingGardenUI->Get_View_Image()."</a></td>\n";
						$x .= "<td class=tableContent$css>".htmlspecialchars($row['StudentName'])."</td>\n";
						$x .= "<td class=tableContent$css>".$ReadingGardenUI->Get_Like_Comment_Div(FUNCTION_NAME_BOOK_REPORT, $row['BookReportID'], '')."</td>\n";
						$x .= "</tr>\n";
					}
				}
				$x .= "</table>";
				if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
				
				$this->db_free_result();
				return $x;
			}
			
			function displayFormat_eDiscipline_GM_Index($width="", $othercss1="", $othercss_bottom=""){
					global $image_path, $LAYOUT_SKIN, $oea_cfg, $PATH_WRT_ROOT;
					global $i_Discipline_System_Award_Punishment_Send_Notice, $i_Discipline_System_Award_Punishment_Detention;
					
					include_once("libdisciplinev12.php");
					$ldiscipline = new libdisciplinev12();

				$pendingImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif' width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Pending\" border=0>";
				$rejectImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif' width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Rejected \n[_BY_]\" border=0>";
				$releasedImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_to_student.gif' width=20 height=20 align=absmiddle title=\"$i_Discipline_Released_To_Student \n[_BY_]\" border=0>";
				$approvedImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif' width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Approved \n[_BY_]\" border=0>";
				$waivedImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waived.gif' width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Waived \n[_BY_]\" border=0>";

				
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : "100%";
					$n_start = $this->n_start;
					$x = '';
					//$x .= "<table class='common_table_list' width='{$width}'>\n";
					$x .= "<table width='{$width}' border='0' cellpadding='2' cellspacing='0'>\n";
					$x .= $this->displayColumn(); 
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							
							//$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
							//$x .= "<tr ".$trClass.">\n";
							switch ($row['RecordStatus'])
							{
								case 0: $css = "class='row_waiting'"; break;
								case 1: $css = "class='row_approved'"; break;
								case 2: $css = "class='row_approved record_Waived'"; break;
								case -1: $css = "class='row_suspend'"; break;
								default: $css = "class='row_approved'"; break;
							}
							
							$x .= "<tr ".$css.">\n";
							$x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++) {
								
								if($j==5) {
									$pic = $ldiscipline->getPICNameList($row['PICID']);
									$pic = ($pic=="") ? "---" : $pic;
									$picData = "<table class='tbclip'><tr><td style='border-bottom: none'>".$pic."</td></tr></table>";
									$x .= "<td>".$pic."</td>";
								}
								else if($j==6) {
									$recordInfo = $ldiscipline->RETRIEVE_CONDUCT_INFO($row['RecordID']);
									$x .= "<td>";
	                            	if($recordInfo[0]['NoticeID'] != '' && $recordInfo[0]['NoticeID'] != 0) # TemplateID != NULL / 0
	                            	{	# NoticeID != NULL / 0
	                            		$lc = new libucc($recordInfo[0]['NoticeID']);
	                            		$result = $lc->getNoticeDetails();
	                            		$url = $result['url']."&StudentID=".$recordInfo[0]['StudentID'];
                            			$noticeText = $i_Discipline_System_Award_Punishment_Send_Notice;

	                            		$x .= ($recordInfo[0]['NoticeID'] != '' && $recordInfo[0]['NoticeID'] != 0) ? "<a href='javascript:;' onClick=\"newWindow('{$url}',1)\">{$noticeText}</a>" : $noticeText;
	                            	} else {
		                            	$x .= "";	
	                            	}
$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE GMID=".$row['RecordID'];
		                            	$detentionCount = $ldiscipline->returnVector($sql);
		                            	
		                            	if($detentionCount[0]!=0) {
			                            	$x .= ($recordInfo[0]['NoticeID'] != '' && $recordInfo[0]['NoticeID'] != 0) ? "<br>" : "";
                 	
											if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View")) {
			                            		$x .= "<a href='javascript:;' onClick=\"changeClickID({$row['RecordID']});moveObject('show_detail{$row['RecordID']}', true, 280);showResult({$row['RecordID']},'detention');MM_showHideLayers('show_detail{$row['RecordID']}','','show');\">{$i_Discipline_System_Award_Punishment_Detention}</a><div onload=\"changeClickID({$row['RecordID']});moveObject('show_detail{$row['RecordID']}', true);\" id='show_detail{$row['RecordID']}' style='clear:both;display:none; position:absolute; width:280px; height:180px; z-index:-1;'></div>";
		                            		} else {
			                            		$x .= $i_Discipline_System_Award_Punishment_Detention;
		                            		}
		                            		$y .= "document.onload=moveObject('show_detail{$row['RecordID']}', true);";
		                            		$jsDetentionAry .= ($jsDetentionAry != '') ? ",".$row['RecordID'] : $row['RecordID'];
	                            		}
		                            	$x .= (($row[$j] == "" || $row[$j] == 0) && $detentionCount[0]==0) ? "---" : "";		                            	
									$x .= "&nbsp;</td>";
								}
								else if($j==8) {
									//$recordInfo = $ldiscipline->RETRIEVE_CONDUCT_INFO($row['RecordID']);
									switch($recordInfo[0]['RecordStatus']) {
										case -1 : $statusImg = $rejectImg; break;
										case 2 : $statusImg = $waivedImg; break;
										default : $statusImg = $approvedImg; break;
									}
									$x .= "<td>".$statusImg."</td>";
								}
								else {
									$x .= $this->displayCell($j,$row[$j], $css);
								}
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}
			
	        function displayFormat_eDiscipline_WebSAMS_NotTransferredIndex($width="", $othercss1="", $othercss_bottom="")
	        {	
				
		        global $i_Discipline_System_Discipline_Case_Record_Case, $iDiscipline, $eDiscipline;
		        global $i_Discipline_System_Award_Punishment_Detention, $i_Discipline_System_Award_Punishment_Send_Notice, $image_path, $LAYOUT_SKIN;
		        global $ldiscipline;
		        
	   			//include_once("libdisciplinev12.php");
	   			include_once("libucc.php");
	   			//$ldiscipline = new libdisciplinev12();
	   			
				$hasCaseViewRight = $ldiscipline->CHECK_FUNCTION_ACCESS("Discipline-MGMT-Case_Record-View");
				$hasGMViewRight = $ldiscipline->CHECK_FUNCTION_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View");

			                            		
				$i = 0;
				
	            $this->rs = $this->db_db_query($this->built_sql());
	            
	            $width = ($width!="") ? $width : "100%";
				
				$remarksImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_remark.gif' width=20 height=20 align=absmiddle title=\"".$iDiscipline['Remarks']."\" border=0>";
				
				$hasCaseViewRight = $ldiscipline->CHECK_FUNCTION_ACCESS("Discipline-MGMT-Case_Record-View");
				
	            $n_start = $this->n_start;
	            $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	            $x .= $this->displayColumn("tabletop");
	            if ($this->db_num_rows()==0)
	            {
	                    $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext td_no_record' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td></tr>\n";
	            } else
	            {
		            
	                    while ($row = $this->db_fetch_array())
	                    {
	                            $i++;
								
	                            if ($i>$this->page_size)
	                            {
	                                    break;
	                            }
	                            # $row[12];
	                            $x .= "<tr class='row_approved'>\n";
	                            $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
if($ldiscipline->Display_ConductMarkInAP) {
						   				$picColumn = 7;	
						   				$referenceColumn = 8;
						   				$actionColumn = 9;
						   			} else {
						   				$picColumn = 6;
						   				$referenceColumn = 7;
						   				$actionColumn = 8;
						   			}	
						   			$recordData = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($row[$referenceColumn]);                            
	                            for ($j=0; $j<$this->no_col-1; $j++)
	                            {
						   			
									if($j==3) {		# Merit/Demrit count
										
										$meritDisplay = $ldiscipline->TrimDeMeritNumber_00($row[$j]);
											
										
										$x .= "<td>$meritDisplay</td>";
										
									} else if($j==5) {
										$remark = "";
										if($recordData[0]['Remark']!="") {
											$remark = "<a href='javascript:;' onClick=\"changeClickID({$row[$referenceColumn]}); moveObject('show_remark".$row[$referenceColumn]."', true); showResult({$row[$referenceColumn]},'record_remark'); MM_showHideLayers('show_remark".$row[$referenceColumn]."','','show');\">$remarksImg</a><div onload=\"changeClickID({$row[$referenceColumn]}); moveObject('show_remark{$row[$referenceColumn]}', true);\" id='show_remark{$row[$referenceColumn]}' style='display:none; position:absolute; width:320px; height:150px; z-index:-1;'></div>";
										}
										$x .= "<td>".$row[$j]." $remark</td>";
										$jsRemarkAry .= ($jsRemarkAry != '') ? ",".$row[$referenceColumn] : $row[$referenceColumn];
										
									} else if($j==$picColumn) {	# PICID field 
	                                    $x .= "<td><table class='tbclip'><tr><td style='border-bottom: none'>";
	                                    $picList = $ldiscipline->getPICNameList($row[$j]);
	                            		$x .= ($picList) ? $picList : "---";
	                                    $x .= "</td></tr></table></td>";

	                                    
	                                   
	                                } else if($j==$referenceColumn) {	# Reference field
	                                	$reference = "";
	                                	 
	                                	
	                                	# history data
	                                	if($recordData[0]['fromConductRecords']) {
											if($hasGMViewRight) {
												$reference .= "<a href='javascript:;' onClick=\"changeClickID({$row[$referenceColumn]});moveObject('show_history{$row[$referenceColumn]}', true, 300);showResult({$row[$referenceColumn]},'history');MM_showHideLayers('show_history{$row[$referenceColumn]}','','show');\">".$eDiscipline["HISTORY"]."</a><div onload=\"changeClickID({$row[$referenceColumn]});moveObject('show_history{$row[$referenceColumn]}', true);\" id='show_history{$row[$referenceColumn]}' style='display:none; position:absolute; width:280px; height:180px; z-index:-1;'></div>";
											} else {
												$reference .= $eDiscipline["History"];
											}
			                                
			                                $jsHistoryAry .= ($jsHistoryAry != '') ? ",".$row[$referenceColumn] : $row[$referenceColumn];
		                                	                                		
	                                		
	                                	}
	                                	
	                                	# case data
	                                	if($recordData[0]['CaseID'] !="") {
	                                		$case_info = $ldiscipline->getCaseRecordByCaseID($recordData[0]['CaseID']);
			                            	if($reference != "") $reference .= "<br>";
	                                		if($hasCaseViewRight) {
	                                			
	                                			$reference .= "<a href=/home/eAdmin/StudentMgmt/disciplinev12/management/case_record/view.php?CaseID=".$recordData[0]['CaseID'].">".$i_Discipline_System_Discipline_Case_Record_Case." ". $case_info[0]['CaseNumber'] ."</a>";
	                                		} else {
	                                			$reference .= $i_Discipline_System_Discipline_Case_Record_Case." ". $case_info[0]['CaseNumber'];
	                                		}
	                                			
	                                	}
	           							if($reference=="") $reference = "---";
	           							
	                                    $x .= "<td>$reference</td>";

	                                } else if($j==$actionColumn) {	# Action field
	                                	$action = "";
	                                	
	                                	# notice data
	           							if($recordData[0]['NoticeID'] != '' && $recordData[0]['NoticeID'] != 0) # TemplateID != NULL / 0
		                            	{	# NoticeID != NULL / 0
		                            		$lc = new libucc($recordData[0]['NoticeID']);
		                            		$result = $lc->getNoticeDetails();
		                            		$url = $result['url']."&StudentID=".$recordData[0]['StudentID'];
		                            		if($noticeText=="") $noticeText = $i_Discipline_System_Award_Punishment_Send_Notice;
	
		                            		$notice = ($recordData[0]['NoticeID'] != '' && $recordData[0]['NoticeID'] != 0) ? "<a href='javascript:;' onClick=\"newWindow('{$url}',1)\">{$noticeText}</a>" : $noticeText;
		                            		$action = "$notice";
		                            	} 
		                            	
		                            	# detention data
										$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DemeritID='".$row[$actionColumn]."'";
										
		                            	$detentionCount = $ldiscipline->returnVector($sql);
		                            	
		                            	if($detentionCount[0]!=0) {
			                            	$action .= ($action!="") ? "<br>" : "";
                 	
											if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View")) {
			                            		$action .= "<a href='javascript:;' onClick=\"changeClickID({$row[$actionColumn]});moveObject('show_detail{$row[$actionColumn]}', true, 280);showResult({$row[$actionColumn]},'detention');MM_showHideLayers('show_detail{$row[$actionColumn]}','','show');\">{$i_Discipline_System_Award_Punishment_Detention}</a><div onload=\"changeClickID({$row[$actionColumn]});moveObject('show_detail{$row[$actionColumn]}', true);\" id='show_detail{$row[$actionColumn]}' style='clear:both;display:none; position:absolute; width:280px; height:180px; z-index:-1;'></div>";
		                            		} else {
			                            		$action .= $i_Discipline_System_Award_Punishment_Detention;
		                            		}
		                            		$y .= "document.onload=moveObject('show_detail{$row[$actionColumn]}', true);";
		                            		$jsDetentionAry .= ($jsDetentionAry != '') ? ",".$row[$actionColumn] : $row[$actionColumn];
	                            		}		                            	
		                            	
		                            	if($action=="") $action = "---";
	                                    $x .= "<td>$action</td>";

	                                } else {
		                                $x .= $this->displayCell($j, $row[$j]);
	                                }
	                            }
	                            $x .= "</tr>\n";
	                    }
	            }
	            if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
	
	            $x .= "</table>\n";
	            $this->db_free_result();
	            
	            
	            if($y != "") {
		         	$y = "<script languahe='javascript'>window.onload = pageOnLoad;function pageOnLoad() {".$y."}</script>";
	            }
	            
	            if($jsDetentionAry != '' || $jsRemarkAry != '' || $jsHistoryAry != '') {
		         	$y .= "<script language=javascript>";
		         	$y .= ($jsDetentionAry != '') ? "var jsDetention=[{$jsDetentionAry}];" : "";
		         	$y .= ($jsHistoryAry != '') ? "var jsHistory=[{$jsHistoryAry}];" : "";
		         	$y .= ($jsRemarkAry != '') ? "var jsRemark=[{$jsRemarkAry}];" : "";
		         	$y .= "</script>";
	            }
	            
	            $returnValue = $x.$y;
	
	            return $returnValue;
	        }	
	        
	        function displayFormat_DigitalArchive_Newest_More_Table($width="", $othercss1="", $othercss_bottom="") {
					global $image_path;
					include_once("libdigitalarchive.php");
					include_once("libdigitalarchive_ui.php");
					$lda = new libdigitalarchive();
					$ldaUI = new libdigitalarchive_ui();
					
					$i = 0;
					
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn(); 
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							
							$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
														
							$x .= "<tr ".$trClass.">\n";
							$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++) {
								if($j==0) {
									$x .= $this->displayCell($j,$row['Supplementary'], $css);
								} else if($j==1) {
									//$x .= $this->displayCell($j,chopString($row[$j],$ldaUI->DiaplayDescriptionLengthLimit,'...'), $css);
									$x .= $this->displayCell($j,nl2br($row[$j]), $css);
								} else if($j==3) {
									$result = $lda->Display_ClassLevel_By_Resource_Record_ID($row[$j], 'text');
									$x .= $this->displayCell($j,$result, $css);
								} else if($j==4) {
									$x .= "<td>";
									//$x .= $row[$j];
									//$x .= '<div class="like_status"><a href="#">Unlike</a><span>&nbsp;|&nbsp;</span></div><span class="like_num">You & <a href="#">30</a> likes</span>';
									$x .= '<span class="Div_More_'.$row['SubjectResourceID'].'" id="Div_More_'.$row['SubjectResourceID'].'">'.$ldaUI->Get_Like_Content("More",$row['SubjectResourceID'], $delimiter="|").'</span>';
									$x .= "</td>";
								} else {
									$x .= $this->displayCell($j,$row[$j], $css);
								}
							}
							$x .= "</tr>\n";
							
						}
					}
					$x .= "</thead></table>";
					
					if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}

			function displayFormat_Study_Score_Management($Subscore1Arr){
					global $image_path;
					
					include_once("libdisciplinev12.php");
					$ldiscipline = new libdisciplinev12();
					
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					
					$x = '';
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn(); 
	
					$baseMark = $ldiscipline->getStudyScoreRule('baseMark');
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					}
					else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
							
							$x .= "<tr ".$trClass.">\n";
							$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++) {
								if($j==2) {
									$subscore = ($Subscore1Arr[$row['StudentID']]) ? $Subscore1Arr[$row['StudentID']] : $baseMark;
									
									if($row['m_count']) {
										//$x .= '<td><a href="#">'.$subscore.'</a></td>';
										$x .= '<td><a href="javascript:Show_Merit_Window(\''.$row['StudentID'].'\',\''.$row['AcademicYearID'].'\',\''.$row['YearTermID'].'\')" class="tablelink">
		                             				<span id="merit_'.$row['StudentID'].$row['AcademicYearID'].$row['YearTermID'].'">'.($subscore).'</span></a></td>';
									}
									else {
										$x .= '<td>'.$subscore.'</td>';
									}
								/*} 
								else if($j==3) {
									$grade = $ldiscipline->getStudyScoreGradeByScore($subscore);
									$x .= '<td>'.$grade.'</td>';*/
								}
								else {
									$x .= $this->displayCell($j,$row[$j], $css);
								}
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					
					if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("", $othercss_bottom);
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
			}

			function displayFormat_Activity_Score_Management($Subscore2Arr){
					global $image_path;
					
					include_once("libdisciplinev12.php");
					$ldiscipline = new libdisciplinev12();
					
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					
					$x = '';
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn(); 
	
					$baseMark = $ldiscipline->getActivityScoreRule('baseMark');
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					}
					else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
							
							$x .= "<tr ".$trClass.">\n";
							$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++) {
								if($j==2) {
									$actscore = ($Subscore2Arr[$row['StudentID']]) ? $Subscore2Arr[$row['StudentID']] : $baseMark;
									
									if($row['m_count']) {
										$x .= '<td><a href="javascript:Show_Merit_Window(\''.$row['StudentID'].'\',\''.$row['AcademicYearID'].'\',\''.$row['YearTermID'].'\')" class="tablelink">
		                             				<span id="merit_'.$row['StudentID'].$row['AcademicYearID'].$row['YearTermID'].'">'.($actscore).'</span></a></td>';
									}
									else {
										$x .= '<td>'.$actscore.'</td>';
									}
								}
								else {
									$x .= $this->displayCell($j,$row[$j], $css);
								}
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					
					if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("", $othercss_bottom);
					$this->db_free_result();
					return $x;
			}
			
			function displayFormat_Digital_Archive_Admin_Search_Result_Table($width="", $othercss1="", $othercss_bottom=""){
					global $image_path;
					
					include_once("libdigitalarchive.php");
					include_once("libdigitalarchive_ui.php");
					$lda = new libdigitalarchive();
					$ldaUI = new libdigitalarchive_ui();
					
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn(); 
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							
							$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
														
							$x .= "<tr ".$trClass.">\n";
							$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++) {
							
								$data = $lda->Get_Admin_Doc_Info($row['DocumentID']);
								
								if($j==0) {		# Title
									if(isset($ldaUI->FileExtension[$data['FileExtension']]) && $ldaUI->FileExtension[$data['FileExtension']]!="") {
										$file_css = " class='doc_file file_".$ldaUI->FileExtension[$data['FileExtension']]."'";
									} else {
										$file_css = " class='doc_file'";
									}
									if($data['IsFolder']) {	# folder
										$link = "<a href='list.php?GroupID=".$data['GroupID']."&FolderID=".$data['DocumentID']."' class='doc_folder'>".$data['Title']."</a>";
									} else {				# file
										$link = "<a href='download.php?fileHashName=".$data['FileHashName']."' $file_css>".$data['Title']."</a>";
									}
									$x .= $this->displayCell($j,$link, $css);
								} else if($j==2) {	# Description
									$x .= $this->displayCell($j,chopString($row[$j], $ldaUI->DiaplayDescriptionLengthLimit,'...'), $css);
								} else if($j==3) {	# Version No
									if($data['IsFolder']) {
										$versionDisplay = "-";
									} else {
										$versionDisplay = '<a href="javascript:newWindow(\'file_version.php?Thread='.$data['Thread'].'\',10)">'.$data['VersionNo'].'</a>';
									}
									$x .= $this->displayCell($j,$versionDisplay, $css);
									
								} else if($j==4) {
									$dirLocation = "<a href='fileList.php?GroupID=".$data['GroupID']."&FolderID=".$data['ParentFolderID']."'>".$ldaUI->Get_Folder_Navigation($row['ParentFolderID'], "&gt;", $isListTable=0, $withGroupName=1)."</a>";
									if($data['ParentFolderID']==0) {
										$groupInfo = $lda->getGroupInfo($row['GroupID']);
										$dirLocation = "<a href='fileList.php?GroupID=".$data['GroupID']."&FolderID=0'>".$groupInfo['GroupTitle']."</a>";
									}
									$x .= $this->displayCell($j,$dirLocation, $css);
								} else if($j==5) {
									if($data['IsFolder']) {
										$x .= $this->displayCell($j,"-", $css);
									} else {
										$x .= $this->displayCell($j,file_size($row[$j]), $css);
									}
								} else if($j==6) {
									$tagDisplay = "";
									$delim = "";
									$tagInDoc = $lda->Get_Tag_By_Admin_Docuemnt_ID($row['DocumentID']);
									for($a=0, $a_max=sizeof($tagInDoc); $a<$a_max; $a++) {
										list($tagid, $tagname) = $tagInDoc[$a];
										$tagDisplay .= $delim.'<a href="displayTag.php?tagid='.$tagid.'">'.$tagname.'</a>';
										$delim = ", ";	
									}
									if($tagDisplay=="")
										$tagDisplay = "-";
								
									$x .= $this->displayCell($j,$tagDisplay, $css);
								} else {
									$x .= $this->displayCell($j,$row[$j], $css);
								}
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}			
			
			function displayFormat_Enrolment_Display_Club($width="", $othercss1="", $othercss_bottom="", $filter=""){
					global $image_path, $libenroll, $EnrolGroupID, $EnrolEventID, $Lang,$sys_custom;
					
					$sql = $this->built_sql();
					$resultSetAry = $this->returnArray($sql); 
					//debug_pr($resultSetAry);
					
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn(); 
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						foreach ($resultSetAry as $id => $row){
							$clsArray[] = "(yc.AcademicYearID='".$row['AcademicYearID']."' AND ycu.UserID='".$row['UserID']."')";
							$clssql = implode(" OR ", $clsArray);
						}
						
						$sql = "SELECT ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")." as ClassName, ycu.ClassNumber, ycu.UserID FROM YEAR_CLASS yc INNER JOIN YEAR_CLASS_USER ycu ON ycu.YearClassID=yc.YearClassID WHERE 1 AND ".$clssql."";
						$clsInfoArr = $libenroll->returnResultSet($sql);
						$numOfData = count($clsInfoArr);
						//debug_pr($clsInfoArr);
						$clsInfoAssoAry = array();
						for ($l=0; $l<$numOfData; $l++) {
							$_userId = $clsInfoArr[$l]['UserID'];
							$_className = $clsInfoArr[$l]['ClassName'];
							$_classNumber = $clsInfoArr[$l]['ClassNumber'];
							
							$clsInfoAssoAry[$_userId]['ClassName'] = $_className;
							$clsInfoAssoAry[$_userId]['ClassNumber'] = $_classNumber;
						}
						
						foreach ($resultSetAry as $id => $row){
							//debug_pr($row);
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
														
							$x .= "<tr ".$trClass.">\n";
							$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
							//debug_pr($sql);
							
//							foreach ($clsInfoArr as $clsInfo){
//								if ($clsInfo['UserID'] == $row['UserID']){
//									list($clsName, $clsNo) = $clsInfo;
//									break;
//								}
//							}
							$clsName = $clsInfoAssoAry[$row['UserID']]['ClassName'];
							$clsNo = $clsInfoAssoAry[$row['UserID']]['ClassNumber'];

							for($j=0; $j<$this->no_col-1; $j++)
							{
//								$sql = "SELECT ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")." as ClassName, ycu.ClassNumber FROM YEAR_CLASS yc INNER JOIN YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID AND yc.AcademicYearID='".$row['AcademicYearID']."' AND ycu.UserID='".$row['UserID']."')";
//								$clsInfo = $libenroll->returnArray($sql);
//								list($clsName, $clsNo) = $clsInfo[0];
								if($clsName == "") $clsName = $Lang['General']['EmptySymbol'];
								if($clsNo == "") $clsNo = $Lang['General']['EmptySymbol'];
								if($filter==2)	// student
								{
									$AttendanceNum = $sys_custom['eEnrolment']['ShowStudentNickName'] == true? 5: 4;
									
									
									if($j==0) {
										$x .= "<td>$clsName</td>";
									} 
									else if($j==1) {
										$x .= "<td>$clsNo</td>";
									} 
									else if($j==$AttendanceNum)	## For attendance column
									{
										$DataArr['StudentID'] = $row['UserID'];
										if(!empty($EnrolGroupID))
										{
											$DataArr['EnrolGroupID'] = $EnrolGroupID;
											$this_attendance = $libenroll->GET_GROUP_STUDENT_ATTENDANCE($DataArr);
										}
										if(!empty($EnrolEventID))
										{
											$DataArr['EnrolEventID'] = $EnrolEventID;
											$this_attendance = $libenroll->GET_EVENT_STUDENT_ATTENDANCE($DataArr);
										}
										
										$x .= $this->displayCell($j, $this_attendance . "%", $css);
									}
									else
									{
										//$k = $j<4 ? $j : ($j>4 ? $j-1 : '');
										$k = $j<$AttendanceNum ? $j : ($j>$AttendanceNum ? $j-1 : '');
										$x .= $this->displayCell($j,$row[$k], $css);
									}
								}
								else	// staff / parent
								{
	                                 $x .= $this->displayCell($j, $row[$j]);
								}
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}
			
			function displayFormat_Invoice_List_Table($width="", $othercss1="", $othercss_bottom="", $filter=""){
					global $image_path, $libenroll, $EnrolGroupID, $EnrolEventID, $Lang;
					
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn(); 
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						include_once("libinvoice.php");
						$linvoice = new libinvoice();
					
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
														
							$x .= "<tr ".$trClass.">\n";
							$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++)
							{
								
								
								if($j==3)	## For attendance column
								{
									$groups = $linvoice->getAllAdminGroupOfInvoice($row[$j]);
									$x .= $this->displayCell($j, $groups, $css);
								} else if($j==6) {
									$sql = "SELECT SUM(Price) FROM INVOICEMGMT_ITEM WHERE InvoiceRecordID='".$row[$j]."' AND RecordStatus=".RECORDSTATUS_ACTIVE;
									$subtotal = $linvoice->returnVector($sql);
									$subtotalValue = ($subtotal[0]==0) ? "-" : "$".$subtotal[0];
									$x .= $this->displayCell($j, $subtotalValue, $css);
								} else if($j==7) {
									$picAry = $linvoice->GetPicByUserId($row[$j]);
									if(count($picAry)>0) {
										$picName = implode(', ', $picAry);
									} else {
										$picName = "-";
									}
									$x .= $this->displayCell($j, $picName, $css);
								} else	
								{
	                                 $x .= $this->displayCell($j, $row[$j]);
								}
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}
			
			function displayFormat_ERC_Rubrics_CommentBank_SubjectDetailsComment()
			{						
				global $image_path,$PATH_WRT_ROOT;
			
				include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
	
			    $lreportcard_topic = new libreportcardrubrics_topic();
					
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$width = ($width!="") ? $width : 560;
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
				$x .= $this->displayColumn(); 
				
				if ($this->db_num_rows()==0){
						$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				} else {
					
					$ldb = new libdb();
					$ResultSetArr = $ldb->returnResultSet($this->sql);
//					$topicID_Array = array();
//					for($i=0,$i_MAX=count($ResultSetArr);$i<$i_MAX;$i++) 
//					{						
//						$display_cell = $ResultSetArr[$i]['TopicID'];	
//						$topicID_Array[] = $display_cell;					
//					}
					$topicID_Array = Get_Array_By_Key($ResultSetArr, 'TopicID');
					$LevelTopicIDArr = $lreportcard_topic->Get_TopicPathArr_by_LastTopicID($topicID_Array ,$Par_basedOnLang=true);
					
					while($row = $this->db_fetch_array()) { 
						$i++;
						$css = ($i%2) ? "" : "2";
						if($i>$this->page_size) break;
						
						$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
													
						$x .= "<tr ".$trClass.">\n";
						$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
						
						$_pathArr = array();
							
				
						for($j=0; $j<$this->no_col-1; $j++)
						{
							$display_cell = $row[$j];
							
							if($j==2)  // CommentID
							{
								$_commentID = $row[$j];
								continue;
							}						
						
							if($j==3)  // TopicID
							{
								$_topicID= $display_cell;				
								
								$_pathArr[] = $LevelTopicIDArr[$_topicID]['Subject']['SubjectName'];
								
								for($k=1,$k_MAX=count($LevelTopicIDArr[$_topicID]['Level']);$k<=$k_MAX;$k++)
								{
									$_pathArr[] = $LevelTopicIDArr[$_topicID]['Level'][$k]['TopicName'];
								}
	
								$display_cell = implode(' > ',(array)$_pathArr);
								
								//$display_cell ='<a class=\'tablelink\' href=\'edit.php?commentType=subject&CommentID='.$_commentID.'\'>'.$display_cell.'</a>';
							}
				
							$x .= $this->displayCell($j,$display_cell, $css);
						}						
						
						$x .= "</tr>\n";
					}
				}
				$x .= "</thead></table>";
				if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
				//if($this->db_num_rows()<>0) $x .= $this->navigation();
				$this->db_free_result();
				return $x;
			}
			
			function displayFormat_ePost_report($section_title){
	  			global $image_path, $Lang;
	  	
	  			$i = 0;
	  			$this->rs = $this->db_db_query($this->built_sql());
				//$width = ($width!="") ? $width : "100%";
	  
			  	$n_start = $this->n_start;
			  	
			  	$x .= "<div class=\"table_board\">\n";
		        $x .= "		<div class=\"table_top_left\">\n";
				$x .= "			<div class=\"table_top_right\">\n";
				$x .= "				<span class=\"sectiontitle\">$section_title</span><br>\n";
				$x .= "			</div>\n";
				$x .= "	  	</div>\n";
				$x .= "		<div class=\"table_left\">\n";
				$x .= "			<div class=\"table_right\">\n";
			  	$x .= "				<table class=\"common_table_list\">\n";
				$x .= "					<col nowrap=\"nowrap\"/>\n";
			  	$x .= "					<tr>".$this->column_list."</tr>";
			 
			 	if ($this->db_num_rows()==0){
					$x .= "				<tr class='normal'>\n";
					$x .= "					<td height='80' class='tabletext' align='center' colspan='".$this->no_col."'>".$this->no_msg."</td>\n";
					$x .= "				</tr>\n";
			  	}
			  	else
			  	{
			      	while($row = $this->db_fetch_array()){
						$i++;
				        if ($i>$this->page_size) break;
				        
				        $x .= "			<tr class=\"normal\">\n";
				        
				        if($this->hide_no_row==false)
				        	$x .= "			<td>".($n_start+$i)."</td>\n";
				        
				        for ($j=0; $j<$this->no_col-1; $j++)
				        {
				        	if(($this->show_first_zone_only && $j==0 && $i>1) || $row[$j]=='')
				        		$row[$j] = "&nbsp";
				        	
				        	$x .= "			<td>".$row[$j]."</td>";
				        }
				        $x .= "			</tr>\n";
					}
				}
				
				$x .= "				</table>\n";
				$x .= "			</div>";
				$x .= "		</div>";
				$x .= "		<div class=\"table_bottom_left\">";
				$x .= "			<div class=\"table_bottom_right\">";
				
				if($this->db_num_rows()<>0 && $this->with_navigation){
					$n_start = $this->n_start+1;
					$n_end   = min($this->total_row,($this->pageNo*$this->page_size));
					$n_total = $this->total_row;
					$n_page  = $this->pageNo;
					$n_t_page= ceil($this->total_row/$this->page_size);
					
					# Generate Previous Button JS
					$prev_js = $n_page==1? "void(0)" : $this->form_name."_gopage(".($n_page-1).",document.".$this->form_name.")";
					
					# Generate Next Button JS
					$next_js = $n_page==$n_t_page? "void(0)" : $this->form_name."_gopage(".($n_page+1).",document.".$this->form_name.")";
					
					# Generate Go Page Drop Down List
					$go_page_html = " <select onChange='this.form.".$this->pageNo_name.".value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
		            for($k=1;$k<=$n_t_page;$k++)
		                $go_page_html .= ($n_page==$k) ? "<option value=$k selected>$k</option>\n" : "<option value=$k>$k</option>\n";
		            $go_page_html .= "</select>\n";
		            
		            # Generate Page Size Drop Down List
		            $page_size_html  = "<select name='num_per_page' onChange='this.form.pageNo.value=1;this.form.page_size_change.value=1;this.form.numPerPage.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
		            for($k=10;$k<=100;$k=$k+10)
		            	$page_size_html .= "<option value=$k ".($this->page_size==$k? "SELECTED":"").">$k</option>\n";
		            $page_size_html .= "</select>\n";
					
					$x .= "			<div class=\"common_table_bottom\">";
					$x .= "				<div class=\"record_no\">".$Lang['ePost']['Records']." $n_start - $n_end, ".$Lang['ePost']['Total']." $n_total</div>";
					$x .= "				<div class=\"page_no\">";
					$x .= "					<a class=\"prev\" href=\"javascript:$prev_js\">&nbsp;</a>";
					$x .= "					<span> ".$Lang['ePost']['Page'][0]." </span>";
					$x .= "					<span>$go_page_html ".$Lang['ePost']['Page'][1]."</span>";
					$x .= "					<a class=\"next\" href=\"javascript:$next_js\">&nbsp;</a>";
					$x .= "					<span> | ".$Lang['ePost']['DisplayPage'][0]." </span>";
					$x .= "					<span>$page_size_html</span>";
					$x .= "					<span>".$Lang['ePost']['DisplayPage'][1]."</span>";
					$x .= "				</div>";
					$x .= "			</div>";
				}
				
				$x .= "			</div>";
				$x .= "		</div>";
				$x .= "</div>";
				$x .= "<script language=javascript>";
				$x .= "		function ".$this->form_name."_gopage(page, obj){";
		        $x .= "			obj.".$this->pageNo_name.".value=page;"; 
		        $x .= "			obj.submit();";
				$x .= "		}";
		        $x .= "</script>";
		        $x .= "<input type=\"hidden\" id=\"pageNo\" name=\"pageNo\" value=\"".$this->pageNo."\">";
		        $x .= "<input type=\"hidden\" id=\"field\" name=\"field\" value=\"".$this->field."\">";
		        $x .= "<input type=\"hidden\" id=\"order\" name=\"order\" value=\"".$this->pageNo."\">";
		        $x .= "<input type=\"hidden\" id=\"page_size_change\" name=\"page_size_change\">";
		        $x .= "<input type=\"hidden\" id=\"numPerPage\" name=\"numPerPage\" value=\"".$this->page_size."\">";
						
				$this->db_free_result();
				
				return $x;
			}
			
			function displayFormat_ePost_portfolio_report(){
			  	global $image_path, $Lang;
			  	
			  	$i = 0;
			  	$this->rs = $this->db_db_query($this->built_sql());
			  	
			  	$n_start = $this->n_start;
			  	
			  	$x .= "<div style=\"min-height:370px\">\n";
			  	$x .= "		<table class=\"common_table_list\">\n";
			  	$x .= "			<tr>".$this->column_list."</tr>";
			 
			 	if ($this->db_num_rows()==0){
					$x .= "		<tr class='normal'>\n";
					$x .= "			<td height='80' class='col_first col_last' style='text-align:center; vertical-align:middle' colspan='".($this->no_col-1)."'>".$this->no_msg."</td>\n";
					$x .= "		</tr>\n";
			  	}
			  	else
			  	{
			      	while($row = $this->db_fetch_array()){
						$i++;
				        if ($i>$this->page_size) break;
				        
				        $x .= "	<tr>\n";
				        
				        if($this->hide_no_row==false)
				        	$x .= "	<td>".($n_start+$i)."</td>\n";
				        
				        for ($j=0; $j<$this->no_col-1; $j++)
				        {
				        	if(($this->show_first_zone_only && $j==0 && $i>1) || $row[$j]=='')
				        		$row[$j] = "&nbsp";
				        	
				        	$x .= "	<td".($j!=0? ($j==$this->no_col-2? " class='col_last'" : "") : " class='col_first'").">".$row[$j]."</td>\n";
				        }
				        $x .= "	</tr>\n";
					}
				}
				
				$x .= "		</table>\n";
				$x .= "</div>";
				$x .= "<div class=\"common_table_bottom\">";
				
				if($this->db_num_rows()<>0 && $this->with_navigation){
					$n_start = $this->n_start+1;
					$n_end   = min($this->total_row,($this->pageNo*$this->page_size));
					$n_total = $this->total_row;
					$n_page  = $this->pageNo;
					$n_t_page= ceil($this->total_row/$this->page_size);
					
					# Generate Previous Button JS
					$prev_js = $n_page==1? "void(0)" : $this->form_name."_gopage(".($n_page-1).",document.".$this->form_name.")";
					
					# Generate Next Button JS
					$next_js = $n_page==$n_t_page? "void(0)" : $this->form_name."_gopage(".($n_page+1).",document.".$this->form_name.")";
					
					# Generate Go Page Drop Down List
					$go_page_html = " <select onChange='this.form.".$this->pageNo_name.".value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
		            for($k=1;$k<=$n_t_page;$k++)
		                $go_page_html .= ($n_page==$k) ? "<option value=$k selected>$k</option>\n" : "<option value=$k>$k</option>\n";
		            $go_page_html .= "</select>\n";
		            
		            # Generate Page Size Drop Down List
		            $page_size_html  = "<select name='num_per_page' onChange='this.form.pageNo.value=1;this.form.page_size_change.value=1;this.form.numPerPage.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
		            for($k=10;$k<=100;$k=$k+10)
		            	$page_size_html .= "<option value=$k ".($this->page_size==$k? "SELECTED":"").">$k</option>\n";
		            $page_size_html .= "</select>\n";
					
					$x .= "	<div class=\"record_no\">".$Lang['ePost']['Records']." $n_start - $n_end, ".$Lang['ePost']['Total']." $n_total</div>";
					$x .= "	<div class=\"page_no\">";
					$x .= "		<a class=\"prev\" href=\"javascript:$prev_js\">&nbsp;</a>";
					$x .= "		<span> ".$Lang['ePost']['Page'][0]." </span>";
					$x .= "		<span>$go_page_html ".$Lang['ePost']['Page'][1]."</span>";
					$x .= "		<a class=\"next\" href=\"javascript:$next_js\">&nbsp;</a>";
					$x .= "		<span> | ".$Lang['ePost']['DisplayPage'][0]." </span>";
					$x .= "		<span>$page_size_html</span>";
					$x .= "		<span>".$Lang['ePost']['DisplayPage'][1]."</span>";
					$x .= "	</div>";
				}
				
				$x .= "</div>";
				$x .= "<script language=javascript>";
				$x .= "		function ".$this->form_name."_gopage(page, obj){";
		        $x .= "			obj.".$this->pageNo_name.".value=page;"; 
		        $x .= "			obj.submit();";
				$x .= "		}";
		        $x .= "</script>";
		        $x .= "<input type=\"hidden\" id=\"pageNo\" name=\"pageNo\" value=\"".$this->pageNo."\">";
		        $x .= "<input type=\"hidden\" id=\"field\" name=\"field\" value=\"".$this->field."\">";
		        $x .= "<input type=\"hidden\" id=\"order\" name=\"order\" value=\"".$this->pageNo."\">";
		        $x .= "<input type=\"hidden\" id=\"page_size_change\" name=\"page_size_change\">";
		        $x .= "<input type=\"hidden\" id=\"numPerPage\" name=\"numPerPage\" value=\"".$this->page_size."\">";
						
				$this->db_free_result();
				
				return $x;
			}
			
			function displayFormat_ePost_article_shelf(){
			  	global $image_path, $Lang;
			  	
			  	$i = 0;
			  	$this->rs = $this->db_db_query($this->built_sql());
			
			  	$n_start = $this->n_start;
			  	
			  	$x .= "<table class=\"common_table_list edit_table_list\">\n";
			  	$x .= "		<col nowrap=\"nowrap\">\n";
			  	$x .= "		<tr>".$this->column_list."</tr>";
			 
			 	if ($this->db_num_rows()==0){
					$x .= "	<tr class='normal'>\n";
					$x .= "		<td height='80' class='col_first col_last' style='text-align:center; vertical-align:middle' colspan='".($this->no_col)."'>".$this->no_msg."</td>\n";
					$x .= "	</tr>\n";
			  	}
			  	else
			  	{
			      	while($row = $this->db_fetch_array()){
						$i++;
				        if ($i>$this->page_size) break;
				        
				        $x .= "<tr id='rowID_".$row['ID']."' class='".($row['IsClassPast']? 'past':'')."'>\n";
				        
				        if($this->hide_no_row==false)
				        	$x .= "<td>".($n_start+$i)."</td>\n";
				        
				        for ($j=0; $j<$this->no_col-1; $j++)
				        {
				        	if($this->field_array[$j]=='ControlButton'){
				        		list($editButton,$delButton) = explode('|=|',$row['ControlButton']);
				        		if(!empty($row['IssueList'])){ //Not Pop Up
				        			$x .= "<td><div class=\"table_row_tool\">";
				        			$x .= $editButton;
				        			if($row['IssueList']=='&nbsp;'){
				        				$x .= $delButton;
				        			}
				        			$x .= "</div></td>\n";
				        		}else{
				        			$x .= "<td>".$row[$j]."</td>\n";
				        		}
				        	}else{
			        			$x .= "<td>".$row[$j]."</td>\n";
			        		}
				        }
				        $x .= "</tr>\n";
				        
				        $x .= "<tr id='rowDisplayID_".$row['ID']."' class='rowDisplay ".($row['IsClassPast']? 'past':'')."' style='display:none'>\n";
				        $x .= "		<td>&nbsp;</td>\n";
				        $x .= "		<td id='rowContent_".$row['ID']."' colspan='".($this->no_col - 1)."'></td>\n";
				        $x .= "</tr>\n";
					}
				}
				
				if($this->db_num_rows()<>0 && $this->with_navigation){
					$x .= "<tr>";
					$x .= "		<td colspan=\"100%\">";
					
					$n_start = $this->n_start+1;
					$n_end   = min($this->total_row,($this->pageNo*$this->page_size));
					$n_total = $this->total_row;
					$n_page  = $this->pageNo;
					$n_t_page= ceil($this->total_row/$this->page_size);
					
					# Generate Previous Button JS
					$prev_js = $n_page==1? "void(0)" : $this->form_name."_gopage(".($n_page-1).",document.".$this->form_name.")";
					
					# Generate Next Button JS
					$next_js = $n_page==$n_t_page? "void(0)" : $this->form_name."_gopage(".($n_page+1).",document.".$this->form_name.")";
					
					# Generate Go Page Drop Down List
					$go_page_html = " <select onChange='this.form.".$this->pageNo_name.".value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
		            for($k=1;$k<=$n_t_page;$k++)
		                $go_page_html .= ($n_page==$k) ? "<option value=$k selected>$k</option>\n" : "<option value=$k>$k</option>\n";
		            $go_page_html .= "</select>\n";
		            
		            # Generate Page Size Drop Down List
		            $page_size_html  = "<select name='num_per_page' onChange='this.form.pageNo.value=1;this.form.page_size_change.value=1;this.form.numPerPage.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
		            for($k=10;$k<=100;$k=$k+10)
		            	$page_size_html .= "<option value=$k ".($this->page_size==$k? "SELECTED":"").">$k</option>\n";
		            $page_size_html .= "</select>\n";
					
					$x .= "			<div class=\"record_no\">".$Lang['ePost']['Records']." $n_start - $n_end, ".$Lang['ePost']['Total']." $n_total</div>";
					$x .= "			<div class=\"page_no\">";
					$x .= "				<a class=\"prev\" href=\"javascript:$prev_js\">&nbsp;</a>";
					$x .= "				<span> ".$Lang['ePost']['Page'][0]." </span>";
					$x .= "				<span>$go_page_html ".$Lang['ePost']['Page'][1]."</span>";
					$x .= "				<a class=\"next\" href=\"javascript:$next_js\">&nbsp;</a>";
					$x .= "				<span> | ".$Lang['ePost']['DisplayPage'][0]." </span>";
					$x .= "				<span>$page_size_html</span>";
					$x .= "				<span>".$Lang['ePost']['DisplayPage'][1]."</span>";
					$x .= "			</div>";
					$x .= "		</td>";
					$x .= "</tr>";
				}
				$x .= "</table>\n";
				
				$x .= "<script language=javascript>";
				$x .= "		function ".$this->form_name."_gopage(page, obj){";
		        $x .= "			obj.".$this->pageNo_name.".value=page;"; 
		        $x .= "			obj.submit();";
				$x .= "		}";
		        $x .= "</script>";
		        $x .= "<input type=\"hidden\" id=\"pageNo\" name=\"pageNo\" value=\"".$this->pageNo."\">";
		        $x .= "<input type=\"hidden\" id=\"field\" name=\"field\" value=\"".$this->field."\">";
		        $x .= "<input type=\"hidden\" id=\"order\" name=\"order\" value=\"".$this->pageNo."\">";
		        $x .= "<input type=\"hidden\" id=\"page_size_change\" name=\"page_size_change\">";
		        $x .= "<input type=\"hidden\" id=\"numPerPage\" name=\"numPerPage\" value=\"".$this->page_size."\">";
						
				$this->db_free_result();
				
				return $x;
			}
			function displayFormat_ePost_portfolio_report_new(){
			  	global $image_path, $Lang;
			  	
			  	$i = 0;
			  	$this->rs = $this->db_db_query($this->built_sql());

			  	$n_start = $this->n_start;
			  	
			  	$x .= "<div style=\"min-height:370px\">\n";
			  	$x .= "		<table class=\"common_table_list\">\n";
			  	$x .= "			<tr>".$this->column_list."</tr>";
			 
			 	if ($this->db_num_rows()==0){
					$x .= "		<tr class='normal'>\n";
					$x .= "			<td height='80' class='col_first col_last' style='text-align:center; vertical-align:middle' colspan='".($this->no_col-1)."'>".$this->no_msg."</td>\n";
					$x .= "		</tr>\n";
			  	}
			  	else
			  	{
			      	while($row = $this->db_fetch_array()){
						$i++;
				        if ($i>$this->page_size) break;
				        
				        $x .= "	<tr>\n";
				        
				        if($this->hide_no_row==false)
				        	$x .= "	<td>".($n_start+$i)."</td>\n";
				        
				        for ($j=0; $j<$this->no_col-1; $j++)
				        {
							if($j==0){
								$row[$j] = '<a href="writer_view.php?RequestID='.$row[5].'&RecordID='.$row[6].'">'.$row[0].'</a>';
								$x .= "	<td>".$row[$j]."</td>\n";
							}elseif($j<5){
								$x .= "	<td>".$row[$j]."</td>\n";
							}
				        }
				        $x .= "	</tr>\n";
					}
				}
				
				$x .= "		</table>\n";
				$x .= "</div>";
				$x .= "<div class=\"common_table_bottom\">";
				
				if($this->db_num_rows()<>0 && $this->with_navigation){
					$n_start = $this->n_start+1;
					$n_end   = min($this->total_row,($this->pageNo*$this->page_size));
					$n_total = $this->total_row;
					$n_page  = $this->pageNo;
					$n_t_page= ceil($this->total_row/$this->page_size);
					
					# Generate Previous Button JS
					$prev_js = $n_page==1? "void(0)" : $this->form_name."_gopage(".($n_page-1).",document.".$this->form_name.")";
					
					# Generate Next Button JS
					$next_js = $n_page==$n_t_page? "void(0)" : $this->form_name."_gopage(".($n_page+1).",document.".$this->form_name.")";
					
					# Generate Go Page Drop Down List
					$go_page_html = " <select onChange='this.form.".$this->pageNo_name.".value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
		            for($k=1;$k<=$n_t_page;$k++)
		                $go_page_html .= ($n_page==$k) ? "<option value=$k selected>$k</option>\n" : "<option value=$k>$k</option>\n";
		            $go_page_html .= "</select>\n";
		            
		            # Generate Page Size Drop Down List
		            $page_size_html  = "<select name='num_per_page' onChange='this.form.pageNo.value=1;this.form.page_size_change.value=1;this.form.numPerPage.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
		            for($k=10;$k<=100;$k=$k+10)
		            	$page_size_html .= "<option value=$k ".($this->page_size==$k? "SELECTED":"").">$k</option>\n";
		            $page_size_html .= "</select>\n";
					
					$x .= "	<div class=\"record_no\">".$Lang['ePost']['Records']." $n_start - $n_end, ".$Lang['ePost']['Total']." $n_total</div>";
					$x .= "	<div class=\"page_no\">";
					$x .= "		<a class=\"prev\" href=\"javascript:$prev_js\">&nbsp;</a>";
					$x .= "		<span> ".$Lang['ePost']['Page'][0]." </span>";
					$x .= "		<span>$go_page_html ".$Lang['ePost']['Page'][1]."</span>";
					$x .= "		<a class=\"next\" href=\"javascript:$next_js\">&nbsp;</a>";
					$x .= "		<span> | ".$Lang['ePost']['DisplayPage'][0]." </span>";
					$x .= "		<span>$page_size_html</span>";
					$x .= "		<span>".$Lang['ePost']['DisplayPage'][1]."</span>";
					$x .= "	</div>";
				}
				
				$x .= "</div><P class=\"spacer\"></P>	";
				$x .= "<script language=javascript>";
				$x .= "		function ".$this->form_name."_gopage(page, obj){";
		        $x .= "			obj.".$this->pageNo_name.".value=page;"; 
		        $x .= "			obj.submit();";
				$x .= "		}";
		        $x .= "</script>";
		        $x .= "<input type=\"hidden\" id=\"pageNo\" name=\"pageNo\" value=\"".$this->pageNo."\">";
		        $x .= "<input type=\"hidden\" id=\"field\" name=\"field\" value=\"".$this->field."\">";
		        $x .= "<input type=\"hidden\" id=\"order\" name=\"order\" value=\"".$this->order."\">";
		        $x .= "<input type=\"hidden\" id=\"page_size_change\" name=\"page_size_change\">";
		        $x .= "<input type=\"hidden\" id=\"numPerPage\" name=\"numPerPage\" value=\"".$this->page_size."\">";
						
				$this->db_free_result();
				
				return $x;
			}			
			
			function displayFormat_MessageCenter_SmsIndex($width="", $othercss1="", $othercss_bottom=""){
					global $image_path, $i_SMS_System_Message;
					
					$i = 0;
					$tempSql = $this->built_sql();
					$this->rs = $this->db_db_query($tempSql);
					
					
					$ldb = new libdb();
					$ResultSetArr = $ldb->returnResultSet($tempSql);
					$userIdAry = Get_Array_By_Key($ResultSetArr, 'UserPIC');
					
					$namefield = getNameFieldByLang();
					$sql = "Select UserID, $namefield as UserDisplayName From INTRANET_USER where UserID in ('".implode("','", (array)$userIdAry)."')";
					$userNameAssoAry = BuildMultiKeyAssoc($ldb->returnResultSet($sql), 'UserID', 'UserDisplayName', $SingleValue=1);
					

					
					
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn(); 
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							
							$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
														
							$x .= "<tr ".$trClass.">\n";
							$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++) {
								
								if ($j==2) {
									$__userPic = $row[$j];
									$__picType = $ResultSetArr[$i-1]['PICType'];
									$__adminPic = $ResultSetArr[$i-1]['AdminPIC'];
									
									//IF (a.PICType=1, a.AdminPIC, if(a.PICType=2, $namefield, '". $i_SMS_System_Message ."')) as PIC,
									if ($__picType == 1) {
										$__display = $__adminPic;
									}
									else {
										if ($__picType == 2) {
											$__display = $userNameAssoAry[$__userPic];
										}
										else {
											$__display = $i_SMS_System_Message;
										}
									}
								}
								else {
									$__display = $row[$j];
								}
								
								$x .= $this->displayCell($j, $__display, $css);
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}
			//Henry Chan Added
			function displayFormat_Digital_Archive_File_Format_Table($width="", $othercss1="", $othercss_bottom=""){
					global $image_path;
					
					include_once("libdigitalarchive.php");
					include_once("libdigitalarchive_ui.php");
					$lda = new libdigitalarchive();
					$ldaUI = new libdigitalarchive_ui();
					
					$i = 0;
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn(); 
					
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						while($row = $this->db_fetch_array()) {
							$i++;
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							
							$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
														
							$x .= "<tr ".$trClass.">\n";
							$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++) {
									$x .= $this->displayCell($j,$row[$j], $css);
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}
			//eLibplus Search User show remark nl2br 
			function displayFormat_eLibPlus_Search_User_Table($width="", $othercss1="", $othercss_bottom=""){
				global $image_path;
				
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$width = ($width!="") ? $width : 560;
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
				$x .= $this->displayColumn(); 
				
				if ($this->db_num_rows()==0){
						$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				} else {
					while($row = $this->db_fetch_array()) {
						$i++;
						$css = ($i%2) ? "" : "2";
						if($i>$this->page_size) break;
						
						$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
													
						$x .= "<tr ".$trClass.">\n";
						$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
						for($j=0; $j<$this->no_col-1; $j++)
						$x .= $this->displayCell($j,nl2br($row[$j]), $css);
						$x .= "</tr>\n";
					}
				}
				$x .= "</thead></table>";
				if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
				$this->db_free_result();
				return $x;
				
			} 	
	
			/*
			 * 	Special needs to show Book cover image
			 */
			function displayFormat_eBook_license_table($width="", $othercss1="", $othercss_bottom="", $is_selecting_books=false){
					global $PATH_WRT_ROOT,$Lang;
					
					$i = 0;
					
					$this->rs = $this->db_db_query($this->built_sql());
					$width = ($width!="") ? $width : 560;
					$n_start = $this->n_start;
					$x = '';
					$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
					$x .= $this->displayColumn();
	
					if ($this->db_num_rows()==0){
							$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
					} else {
						while($row = $this->db_fetch_array()) {
						    $i++;
							$bookID = $row['BookID'];
							$periodTo = array();
							$periodToDisplay = "";
							if($is_selecting_books==true){
								$db2 = new libdb();
								$sql = "SELECT BookID FROM INTRANET_ELIB_BOOK_INSTALLATION i 
										INNER JOIN INTRANET_ELIB_BOOK_QUOTATION q ON i.QuotationID=q.QuotationID AND i.BookID='".$bookID."' AND (i.IsEnable=1 OR i.IsCompulsory=1 OR i.IsSelected=1)
										WHERE  (q.PeriodFrom = '0000-00-00 00:00:00' AND q.PeriodTo = '0000-00-00 00:00:00')  AND q.IsActivated=1";
								$hasOneOff = current($db2->returnVector($sql));
								//$hasOneOffStyle = (empty($hasOneOff))?"":"style='color:grey;'";
								if(empty($hasOneOff)){		
									$sql = "SELECT PeriodTo FROM INTRANET_ELIB_BOOK_INSTALLATION i 
											INNER JOIN INTRANET_ELIB_BOOK_QUOTATION q ON i.QuotationID=q.QuotationID AND i.BookID='".$bookID."' AND (i.IsEnable=1 OR i.IsCompulsory=1 OR i.IsSelected=1)
											WHERE (q.PeriodFrom < NOW() AND q.PeriodTo > NOW()) AND q.IsActivated=1 ORDER BY q.PeriodTo DESC";
									$periodTo = current($db2->returnVector($sql));
									$periodToDisplay = (empty($periodTo))?"":"<br><span class='expiry_date_display'>".$Lang['libms']['bookmanagement']['expiry_date'].":".date("Y-m-d", strtotime($periodTo))."</span>";
								}
							}			
							$image = '/file/elibrary/content/'.$bookID.'/image/cover.jpg';
							if(empty($periodTo) && $is_selecting_books!=true){
								$row[0] = file_exists($PATH_WRT_ROOT.$image) ? $row[0] : $row['TitleOnly'];
							}else{
								if(file_exists($PATH_WRT_ROOT.$image)){
									$resultImgTags = str_replace($row['TitleOnly'], "", $row[0]);
									$resultImgTags = str_replace("style='","style='float:left;margin-right:5px;",$resultImgTags);
									$row[0] = $resultImgTags."<div class='book_purchased_display'>".$row['TitleOnly'].$periodToDisplay."</div>";
								}else{
									$row[0] =$row['TitleOnly'].$periodToDisplay;
								}
							}
							
							$row['Title'] = $row[0];
							
							$css = ($i%2) ? "" : "2";
							if($i>$this->page_size) break;
							
							$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
														
							$x .= "<tr ".$trClass." >\n";
							$x .= "<td class=\"tableContent$css\" >".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++){
								if(!empty($hasOneOff)){
									if($j == $this->no_col-2){
										$row[$j] = $Lang['libms']['bookmanagement']['purchased'];
										//$hasOneOffStyle = "style='color:grey;white-space: nowrap;'";
										$css_oneOff = " book_purchased_display book_purchased";
									}else{
										$css_oneOff = " book_purchased";
									}
								}else{
									$css_oneOff = "";
								}
								$x .= $this->displayCell($j,$row[$j], $css.$css_oneOff);
							}
							$x .= "</tr>\n";
						}
					}
					$x .= "</thead></table>";
					if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
					//if($this->db_num_rows()<>0) $x .= $this->navigation();
					$this->db_free_result();
					return $x;
					
			}
	
			// copy from displayFormat_IP25_table
		    function displayFormat_RecommendBook_table($width="", $othercss1="", $othercss_bottom="", $recommendedGroup=array()){
				global $image_path;
				
				$RecommendedBy_col = array_search('RecommendedBy',$this->field_array);	// column position of RecommendedBy
				$RecommendedGroup_col = array_search('RecommendGroup',$this->field_array);	// column position of RecommendGroup
				
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$width = ($width!="") ? $width : 560;
				$n_start = $this->n_start;
				$x = '';
				$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
				$x .= $this->displayColumn(); 
		// 		2016-10-05 (Villa) #S105745
				$x .= "</thead>";
				if ($this->db_num_rows()==0){
						$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				} else {
					while($row = $this->db_fetch_array()) {
						$i++;
						$css = ($i%2) ? "" : "2";
						if($i>$this->page_size) break;
						
						$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
													
						$x .= "<tr ".$trClass.">\n";
						$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
						for($j=0; $j<$this->no_col-1; $j++) {
							if($RecommendedGroup_col !== false && $j == $RecommendedGroup_col) {
								if (!empty($row['RecommendGroup'])) {
									$recommendGroupID_array = explode('|',$row['RecommendGroup']);
									$recommendGroupName_array = array();
									foreach((array)$recommendGroupID_array as $k=>$v) {
										$recommendGroupName_array[] = $recommendedGroup[$v]['LevelName'];
									}
									sort($recommendGroupName_array);
									$displayValue = implode(', ',$recommendGroupName_array);
									unset($recommendGroupName_array);
								}
								else {
									$displayValue = '--';
								}
							} 
							else {
								$displayValue = $row[$j];
							}
							$x .= $this->displayCell($j,$displayValue, $css);
						}
						$x .= "</tr>\n";
					}
				}
				$x .= "</table>";
				if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
				$this->db_free_result();
				return $x;
				
			} 
			
			/* copy from displayFormat_IP25_table
			 * $multiple_value_column -- associate array, used to lookup values by key, two levels:
			 *  array([column_1] => array([field_11]=>value_11 ...
			 * 							  [field_1n]=>value_1n)
			 * 		  ...
			 * 		  [column_n] => array([field_n1]=>value_n1 ...
			 * 							  [field_nn]=>value_nn)
			 *  e.g. array(	['Teacher'] => array([1]=>'demo teacher 1',[2]=>'demo teacher 2'),
			 * 		 		['Student'] => array([1]=>'demo student 1',[2]=>'demo student 2'));
			 * 	$splitter -- splitter for multiple value column
			 */ 
		    function displayFormat_with_multiple_values_in_a_column($width="", $othercss1="", $othercss_bottom="", $multiple_value_column=array(), $splitter=",", $otherSplitter="^:", $sortByName=true, $subitem_array=array()){
				global $image_path;
				
				$col_pos = array();
				foreach((array)$multiple_value_column as $k=>$v) {
					$col_pos[$k] = array_search($k,$this->field_array);	// find column position
				}
				
				$i = 0;
				$this->rs = $this->db_db_query($this->built_sql());
				$width = ($width!="") ? $width : 560;
				$n_start = $this->n_start;
				$otherLength = strlen($otherSplitter)+4;
				
				$x = '';
				$x .= "<table class='common_table_list_v30 $othercss1'>\n<thead>\n";
				$x .= $this->displayColumn(); 
				$x .= "</thead>";
				if ($this->db_num_rows()==0){
						$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				} else {
					while($row = $this->db_fetch_array()) {
						$i++;
						$css = ($i%2) ? "" : "2";
						if($i>$this->page_size) break;
						
						$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
													
						$x .= "<tr ".$trClass.">\n";
						$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
						for($j=0; $j<$this->no_col-1; $j++) {
							foreach((array)$col_pos as $k=>$p) {
								if($p !== false && $j == $p) {
									if (!empty($row[$k])) {
										$id_array = explode($splitter,$row[$k]);
										$name_array = array();
										
										foreach((array)$id_array as $v) {
											
											if (strpos($v, $otherSplitter) !== false) {
												list($subType,$items) = explode('^:',$v);
												
												if (strpos($items, '^#') !== false) {
													$subitemCodeAry = explode('^#',$items);
													foreach((array)$subitemCodeAry as $sub) {
														$subitem_name[] = $subitem_array[$subType][$sub];	
													}
													$itemName = $multiple_value_column[$k][$subType];
													$name_array[] = $itemName . ':('.implode(',',$subitem_name).')';
												}
												else {
													$itemName = $multiple_value_column[$k][$subType];
													if ($subitem_array[$subType][$items]) {
														$name_array[] = $itemName . ':('.$subitem_array[$subType][$items].')';
													}
													else {
														if ($items) {
															$itemName .= ":".$items;
														}
													}
													$name_array[] = $itemName;
												}
											}
											
//											if (strlen($v)>$otherLength && substr($v,0,$otherLength+1)=="Other".$otherSplitter) {
//												$name_array[] = $multiple_value_column[$k]['Other'].substr($v,$otherLength);
//											}
											else {
												$name_array[] = $multiple_value_column[$k][$v];	
											}											
										}
										if ($sortByName) {
											sort($name_array);	
										}

										$displayValue = implode(', ',$name_array);
										unset($name_array);
									}
									else {
										$displayValue = '-';
									}
								} 
								else {
									$displayValue = $row[$j];
								}
								$x .= $this->displayCell($j,$displayValue, $css);
							}
						}
						$x .= "</tr>\n";
					}
				}
				$x .= "</table>";
				if($this->db_num_rows()<>0 && $this->no_navigation_row!=true) $x .= $this->navigation_IP25("",$othercss_bottom);
				$this->db_free_result();
				return $x;
				
			} 

			
			// ********** start of functions for ajax
			// Following functions show layout only, they are target to use ajax to get data result
			// ajax javascript is not included, user has to write navigation and sort solumn function
			function column_4ajax($field_index, $field_name){
			    
			    global $image_path, $LAYOUT_SKIN;
			    
			    if ($this->IsColOff=="staff")
			    {
			        $image_path="/images/staffattend";
			    }
			    
			    $x = "";
			    if($this->field==$field_index)
			    {
			        //Change arrow display
			        if($this->order==1) $x .= "<a class=\"".$this->returnColumnClassID()."\" href=\"#\" data-order=\"0\" data-fieldIndex=\"$field_index\" onMouseOver=\"window.status='".$this->sortby." $field_name';MM_swapImage('sort_icon','','$image_path/{$LAYOUT_SKIN}/icon_sort_a_on.gif',1);return true;\" onMouseOut=\"window.status='';MM_swapImgRestore();return true;\" title=\"".$this->sortby." $field_name\" >";
			        if($this->order==0) $x .= "<a class=\"".$this->returnColumnClassID()."\" href=\"#\" data-order=\"1\" data-fieldIndex=\"$field_index\" onMouseOver=\"window.status='".$this->sortby." $field_name';MM_swapImage('sort_icon','','$image_path/{$LAYOUT_SKIN}/icon_sort_d_on.gif',1);return true;\" onMouseOut=\"window.status='';MM_swapImgRestore();return true;\" title=\"".$this->sortby." $field_name\" >";
			        
			    } else
			    {
			        $x .= "<a class=\"".$this->returnColumnClassID()."\" href=\"#\" data-order=\"$this->order\" data-fieldIndex=\"$field_index\" onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\" title=\"".$this->sortby." $field_name\" >";
			    }
			    $x .= str_replace("_", " ", $field_name);
			    if ($this->field==$field_index)
			    {
			        //Change arrow display
			        if($this->order==1) $x .= "<img name='sort_icon' id='sort_icon' src='$image_path/{$LAYOUT_SKIN}/icon_sort_a_off.gif' align='absmiddle' border='0' />";
			        if($this->order==0) $x .= "<img name='sort_icon' id='sort_icon' onMouseOver=\"MM_swapImage('sort_asc','','$image_path/{$LAYOUT_SKIN}/icon_sort_d_on.gif',1)\" onMouseOut='MM_swapImgRestore()' src='$image_path/{$LAYOUT_SKIN}/icon_sort_d_off.gif' align='absmiddle'  border='0' />";
			    }
			    $x .= "</a>";
			    return $x;
			}
			
			function navigation_4ajax($bgcolor="")
			{
			    global $image_path, $LAYOUT_SKIN;
			    global $pageSizeChangeEnabled, $viewTypeChangeEnabled;
			    
			    $x  = "<table width='100%' border='0' cellpadding='3' cellspacing='0' class='tabletext'>\n";
			    $x .= "<tr>\n";
			    $x .= "<td width='100%' >\n";
			    $x .= $this->record_range();
			    $x .= "</td><td nowrap='nowrap'>\n";
			    $x .= $this->prev_n_4ajax();
			    $x .= $this->go_page_4ajax();
			    $x .= $this->next_n_4ajax();
			    $x .= "&nbsp; &nbsp; &nbsp;";
			    
			    if ($pageSizeChangeEnabled)
			    {
			        $x .= $this->page_size_input_4ajax();
			    }
			    if ($viewTypeChangeEnabled)
			    {
			        $x .= $this->view_type_input_4ajax();
			    }
			    $x .= "&nbsp;</td>\n";
			    $x .= "</tr>\n";
			    $x .= "</table>\n";
			    return $x;
			}
			
			function prev_n_4ajax()
			{
			    
			    global $image_path, $image_pre, $LAYOUT_SKIN;
			    
			    if ($this->IsColOff=="staff")
			    {
			        $image_path="/images/staffattend";
			        $image_pre = "$image_path/previous_icon.gif";
			    }
			    if ($this->IsColOff == 2 || $this->IsColOff == 5)
			    {
			        $previous_icon = "<img src=$image_path/previous_icon.gif border=0 hspace=2 vspace=0 align=middle>";
			    }
			    else if ($this->IsColOff == "imail_recipient_selection")
			    {
			        $previous_icon = "<img src=$image_path/btn_pre.gif border=0 hspace=2 vspace=0 align=middle title='".$this->pagePrev."'  >";
			    }
			    else
			    {
			        $previous_icon = "<img src=$image_pre border=0 hspace=2 vspace=0 align=middle>";
			    }
			    $n_page = $this->pageNo;
			    $n_total = ceil($this->total_row/$this->page_size);
			    $n_size = $this->page_size;
			    if ($this->IsColOff == "imail_recipient_selection")
			    {
			        $x = ($n_page==1) ? "&nbsp;".$previous_icon."&nbsp;" :
			        "<a href=\"#\" data-targetPage=\"".($n_page-1)."\" class='tablebottomlink' >&nbsp;".$previous_icon."&nbsp;</a> ";
			    }
			    else
			    {
			        $x = ($n_page==1) ? "&nbsp;<img src='{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif' name='page_previous' id='page_previous' width='11' height='10' border='0' hspace='3' align='absmiddle' />&nbsp;" :
			        "<a href=\"#\" data-targetPage=\"".($n_page-1)."\" class='tablebottomlink' onMouseOver=\"MM_swapImage('page_previous','','{$image_path}/{$LAYOUT_SKIN}/icon_prev_on.gif',1)\" onMouseOut='MM_swapImgRestore()'>&nbsp;<img src='{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif' name='page_previous' id='page_previous' width='11' height='10' border='0' align='absmiddle' title='".$this->pagePrev."' />&nbsp;</a> ";
			    }
			    
			    return $x;
			}
			
			function next_n_4ajax(){
			    
			    global $image_path,$image_next, $LAYOUT_SKIN;
			    
			    if ($this->IsColOff=="staff")
			    {
			        $image_path="/images/staffattend";
			        $image_next="$image_path/next_icon.gif";
			    }
			    
			    if ($this->IsColOff == 2 || $this->IsColOff == 5)
			    {
			        $next_icon = "<img src=$image_path/next_icon.gif border=0 hspace=2 vspace=0 align=middle>";
			    }
			    else if ($this->IsColOff == "imail_recipient_selection")
			    {
			        $next_icon = "<img src=$image_path/btn_next.gif border=0 hspace=2 vspace=0 align=middle title='".$this->pageNext."'  >";
			    }
			    else
			    {
			        $next_icon = "<img src=$image_next border=0 hspace=2 vspace=0 align=middle>";
			    }
			    $n_page = $this->pageNo;
			    $n_total = ceil($this->total_row/$this->page_size);
			    
			    $n_size = $this->page_size;
			    
			    if ($this->IsColOff == "imail_recipient_selection")
			    {
			        $x = ($n_page==$n_total) ? "&nbsp;".$next_icon."&nbsp;" :
			        "<a href=\"#\" data-targetPage=\"".($n_page+1)."\" class='tablebottomlink' >&nbsp;".$next_icon."&nbsp;</a> ";
			    }
			    else
			    {
			        $x = ($n_page==$n_total) ? "&nbsp;<img src='{$image_path}/{$LAYOUT_SKIN}/icon_next_off.gif' name='page_next' id='page_next' width='11' height='10' border='0' hspace='3' align='absmiddle' />&nbsp;" :
			        "<a href=\"#\" data-targetPage=\"".($n_page+1)."\" class='tablebottomlink' onMouseOver=\"MM_swapImage('page_next','','{$image_path}/{$LAYOUT_SKIN}/icon_next_on.gif',1)\" onMouseOut='MM_swapImgRestore()'>&nbsp;<img src='{$image_path}/{$LAYOUT_SKIN}/icon_next_off.gif' name='page_next' id='page_next' width='11' height='10' border='0' align='absmiddle' title='".$this->pageNext."' />&nbsp;</a> ";
			    }
			    
			    return $x;
			}
			
			function go_page_4ajax(){
			    $n_page=$this->pageNo;
			    $n_total=ceil($this->total_row/$this->page_size);
			    $x = $this->pagename." <select class='formtextbox pageSelection'>\n";
			    for($i=1;$i<=$n_total;$i++)
			        $x .= ($n_page==$i) ? "<option value=\"$i\" selected>$i</option>\n" : "<option value=\"$i\">$i</option>\n";
			        $x .= "</select>\n";
			        return $x;
			}
			
			function page_size_input_4ajax()
			{
			    global $i_general_EachDisplay ,$i_general_PerPage, $sys_custom;
			    
			    $x = "{$i_general_EachDisplay} <select name='num_per_page' class='formtextbox'>\n";
			    if($sys_custom['ListTableDisplayPageMin'])
			    {
			        $x .= "<option value='". $sys_custom['ListTableDisplayPageMin'] ."' ".($this->page_size== $sys_custom['ListTableDisplayPageMin'] ? "SELECTED":"").">". $sys_custom['ListTableDisplayPageMin'] ."</option>\n";
			    }
			    $x .= "<option value='10' ".($this->page_size==10? "SELECTED":"").">10</option>\n";
			    $x .= "<option value='20' ".($this->page_size==20? "SELECTED":"").">20</option>\n";
			    $x .= "<option value='30' ".($this->page_size==30? "SELECTED":"").">30</option>\n";
			    $x .= "<option value='40' ".($this->page_size==40? "SELECTED":"").">40</option>\n";
			    $x .= "<option value='50' ".($this->page_size==50? "SELECTED":"").">50</option>\n";
			    $x .= "<option value='60' ".($this->page_size==60? "SELECTED":"").">60</option>\n";
			    $x .= "<option value='70' ".($this->page_size==70? "SELECTED":"").">70</option>\n";
			    $x .= "<option value='80' ".($this->page_size==80? "SELECTED":"").">80</option>\n";
			    $x .= "<option value='90' ".($this->page_size==90? "SELECTED":"").">90</option>\n";
			    $x .= "<option value='100' ".($this->page_size==100? "SELECTED":"").">100</option>\n";
			    if($sys_custom['ListTableDisplayPageMax'])
			    {
			        $x .= "<option value='". $sys_custom['ListTableDisplayPageMax'] ."' ".($this->page_size== $sys_custom['ListTableDisplayPageMax'] ? "SELECTED":"").">". $sys_custom['ListTableDisplayPageMax'] ."</option>\n";
			    }
			    $x .= "</select> $i_general_PerPage";
			    return $x;
			}
			
			function view_type_input_4ajax()
			{
			    
			    $x = "<select name='view_type'>\n";
			    $x .= "<option value=1 ".($this->view_type==1? "SELECTED":"").">1 Column</option>\n";
			    $x .= "<option value=2 ".($this->view_type==2? "SELECTED":"").">2 Columns</option>\n";
			    $x .= "<option value=3 ".($this->view_type==3? "SELECTED":"").">4 Columns</option>\n";
			    $x .= "</select>";
			    return $x;
			}
	
			// copy from displayFormat2
			function displayFormat_4ajax($width="", $tablecolor=""){
			    global $image_path;
			    $i = 0;
			    $DisplayIndex = ($this->HideIndex) ? false : true;
			    $TotalColumn = ($DisplayIndex) ? $this->no_col : $this->no_col-1;
			    
			    $this->rs = $this->db_db_query($this->built_sql());
			    $width = ($width!="") ? $width : "100%";
			    
			    $n_start = $this->n_start;
			    $x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
			    $x .= $this->displayColumn();
			    if ($this->db_num_rows()==0)
			    {
			        $x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$TotalColumn."'>".$this->no_msg."</td></tr>\n";
			    } else
			    {
			        while ($row = $this->db_fetch_array())
			        {
			            $i++;
			            $css = ($i%2) ? "1" : "2";
			            if ($i>$this->page_size)
			            {
			                break;
			            }
			            $x .= "<tr class='table{$tablecolor}row{$css}'>\n";
			            if ($DisplayIndex)
			            {
			                $x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
			            }
			            for ($j=0; $j<$this->no_col-1; $j++)
			            {
			                
			                $x .= $this->displayCell($j, $row[$j]);
			            }
			            $x .= "</tr>\n";
			        }
			    }
			    if($this->db_num_rows()<>0 && $this->with_navigation) $x .= "<tr><td colspan='".$TotalColumn."' class='table{$tablecolor}bottom'>".$this->navigation_4ajax()."</td></tr>";
			    $x .= "</table>\n";
			    $this->db_free_result();
			    
			    return $x;
			}
			
			// ********** end of functions for ajax
	}
?>