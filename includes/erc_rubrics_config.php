<?
$eRC_Rubrics_ConfigArr = array();

$eRC_Rubrics_ConfigArr['MaxLevel'] = 3;

$eRC_Rubrics_ConfigArr['MaxLength']['Module']['Code'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['Module']['NameEn'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['Module']['NameCh'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['Rubrics']['Code'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['Rubrics']['NameEn'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['Rubrics']['NameCh'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['RubricsItem']['NameEn'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['RubricsItem']['NameCh'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['RubricsItem']['Desc'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['Topic']['Code'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['Topic']['NameEn'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['Topic']['NameCh'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['TextArea'] = 255;
$eRC_Rubrics_ConfigArr['MaxLength']['Template']['Title'] = 255;
$eRC_Rubrics_ConfigArr['MaxLength']['TemplateSettings']['Code'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['TemplateSettings']['Title'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['ECA_Category']['Code'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['ECA_Category']['Name'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['ECA_Item']['Code'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['ECA_Item']['Name'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['Service']['Code'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['Service']['Name'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['Comment']['Code'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['ReportTemplate']['NameEn'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['ReportTemplate']['NameCh'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['ECACategory']['Code'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['ECACategory']['NameEn'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['ECACategory']['NameCh'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['ClassTeacherComment'] = 255;
$eRC_Rubrics_ConfigArr['MaxLength']['CommentCode'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['Marksheet']['Remarks'] = 10;
$eRC_Rubrics_ConfigArr['MaxLength']['AdminGroup']['Code'] = 128;
$eRC_Rubrics_ConfigArr['MaxLength']['AdminGroup']['Name'] = 128;


// Need to match the customized lang index
$eRC_Rubrics_ConfigArr['SignatureFieldArr'] = array('Principal', 'ClassTeacher', 'Parent', 'Date');
$eRC_Rubrics_ConfigArr['HeaderDataFieldArr'] = array('StudentName', 'ClassName', 'ModuleName', 'ModuleDateRange');

$eRC_Rubrics_ConfigArr['TemplateArr']['CSS'] = 'general.css';

?>