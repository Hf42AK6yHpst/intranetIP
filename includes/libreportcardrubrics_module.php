<?php
#  Editing by Marcus

class libreportcardrubrics_module extends libreportcardrubrics {
	
	/**
	 * Constructor
	 */
	function libreportcardrubrics_module($AcademicYearID='') {
		parent:: libreportcardrubrics($AcademicYearID);
	}
	
	function Get_Module_Info($YearTermID='', $ModuleID='')
	{
		$conds_YearTermID = '';
		if ($YearTermID != '')
			$conds_YearTermID = " And m.YearTermID = '".$YearTermID."' ";

		$conds_ModuleID = '';
		if(!empty($ModuleID))
			$conds_ModuleID = " And m.ModuleID IN ('".implode("','",(array)$ModuleID)."') ";
//		else if ($ModuleID != '')
//			$conds_ModuleID = " And m.ModuleID IN ('".$ModuleID."') ";
		
//		if(trim($AcademicYearID)!='')
//			$conds_AcademicYearID= " And ayt.AcademicYearID = '".$AcademicYearID."'  ";	
		
		$ModuleTable = $this->Get_Table_Name('RC_MODULE');
		$sql = "Select
						m.*
				From
						$ModuleTable as m
						Left Outer Join
						ACADEMIC_YEAR_TERM as ayt
						On (m.YearTermID = ayt.YearTermID)
				Where
						m.RecordStatus = 1
						$conds_YearTermID
						$conds_ModuleID
						$conds_AcademicYearID
				Order By
						ayt.TermStart, m.StartDate, m.DateInput
				";
				
				
		return $this->returnArray($sql);
	}
	
	function Is_Module_Code_Valid($Value, $ExcludedModuleID = '')
	{
		if(trim($Value) == '') return false;
		
		$Value = $this->Get_Safe_Sql_Query(trim($Value));
		
		if(trim($ExcludedModuleID)!='')
			$cond_ExcludedModuleID = " AND ModuleID <> $ExcludedModuleID ";
		
		$ModuleTable = $this->Get_Table_Name('RC_MODULE');
		$sql = "Select
						ModuleID
				From
						$ModuleTable
				Where
						ModuleCode = '$Value'
						And
						RecordStatus = 1
						$cond_ExcludedModuleID
				";
		$ResultArr = $this->returnArray($sql);
		return (count($ResultArr) > 0)? false : true;
	}
	
	function Is_Module_Date_Range_Valid($StartDate, $EndDate, $ExcludedModuleID='')
	{
		
		if(trim($StartDate) == '' && trim($EndDate) == '' )
			return true;
		
		if(trim($ExcludedModuleID)!='')
			$cond_ExcludedModuleID = " AND ModuleID <> $ExcludedModuleID ";
		
		$ModuleTable = $this->Get_Table_Name('RC_MODULE');
		
		$sql = "
			SELECT 
				COUNT(*)
			FROM
				$ModuleTable
			WHERE
				(
					'$StartDate' BETWEEN StartDate AND EndDate
					OR '$EndDate' BETWEEN StartDate AND EndDate
					OR StartDate BETWEEN '$StartDate' AND '$EndDate'
					OR EndDate BETWEEN '$StartDate' AND '$EndDate'
				)
				AND RecordStatus = 1 
				$cond_ExcludedModuleID
		";
		
		$result = $this->returnVector($sql);
		return $result[0]>0?false:true;
	}
	
	function InsertModule($DataArr)
	{
		if(!is_array($DataArr))
			return false;
			
		foreach((array)$DataArr as $key => $data)
		{
			$fieldArr[] = $key;
			if(trim($data)=='')
				$valueArr[] = "NULL";
			else
				$valueArr[] = "'".$data."'";
		}
		
		# Record status
		$fieldArr[] = 'RecordStatus';
		$valueArr[] = '1';
		# DateInput
		$fieldArr[] = 'DateInput';
		$valueArr[] = 'now()';
		# InputBy
		$fieldArr[] = 'InputBy';
		$valueArr[] = "'".$_SESSION['UserID']."'";
		# DateModified
		$fieldArr[] = 'DateModified';
		$valueArr[] = 'now()';
		# LastModifiedBy
		$fieldArr[] = 'LastModifiedBy';
		$valueArr[] = "'".$_SESSION['UserID']."'";
		
		
		$fields = implode(",",$fieldArr);
		$values = implode(",",$valueArr);
		
		$ModuleTable = $this->Get_Table_Name('RC_MODULE');
		$sql = "
			INSERT INTO
				$ModuleTable
				($fields)
			VALUES
				($values)
		";
		$result = $this->db_db_query($sql);
		
		return $result?true:false;	

	}
	
	function UpdateModule($ModuleID, $DataArr, $UpdateLastModified=1)
	{
		if(!is_array($DataArr))
			return false;
			
		foreach((array)$DataArr as $key => $data)
		{
			if(trim($data)=='')
				$valueFieldArr[] = $key." = NULL";
			else
				$valueFieldArr[] = $key." = '".$this->Get_Safe_Sql_Query($data)."'";
		}
		
		if ($UpdateLastModified == 1)
		{
			# DateModified
			$valueFieldArr[] = " DateModified = NOW() ";
			# LastModifiedBy
			$valueFieldArr[] = " LastModifiedBy = '".$_SESSION['UserID']."'";
		}
		
		$valueFields = implode(",",$valueFieldArr);
		
		$ModuleTable = $this->Get_Table_Name('RC_MODULE');
		$sql = "
			UPDATE
				$ModuleTable
			SET
				$valueFields
			WHERE
				ModuleID = '$ModuleID'
		";
		$result = $this->db_db_query($sql);
		
		return $result?true:false;	
	}
}

?>