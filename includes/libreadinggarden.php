<?php
// editing by 

/***************************** Modification Log *************************************
 * 
 * 20190514 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 * 
 * 20180809 Henry
 * 		- fixed sql injection in Get_Book_List()
 * 
 * 20170731 Pun [121046]
 *      - Modified Get_Student_Score(), fixed display error while the setting does not set
 *
 * 20160516 Cameron:
 *		- replace if(trim($array)!='') by if($array) in: Get_Award_Scheme_List(), Get_Student_Recommend_Book_List_DBTable_Sql(),
 *			Get_Book_List(), Remove_Recommend_Book_From_Form(), Get_Category_Answer_Sheet_Mapping(), Get_Recommend_Book_Mappings(),
 *			Get_Category_Answer_Sheet_Mapping()
 *			Remove_Recommend_Book_From_Form()
 *			Get_Recommend_Book_Mappings()
 *			Get_Award_Scheme_Requirement()
 *			Get_Student_Reading_Target_Achievement()
 *			Remove_Announcement_From_Class()			
 *			Get_Class_Announcement()
 *			Get_Announcement_Info()
 *			Settings_Manage_Award_Scheme_Winner()
 *			Get_Award_Scheme_Level_Info()
 *			Remove_Forum_From_Class()
 *			Get_Class_Forum()
 *			Get_Forum_Summary()
 *			Get_Forum_Selection()
 *			Manage_Forum_Post_Record()
 *			Get_Forum_Topic()
 *			Get_Forum_Topic_Post()
 *			Get_Popular_Award_Scheme_Item_Info()
 *			Get_Popular_Award_Scheme_Award_Requirement_Info()
 *			Get_Popular_Award_Scheme_Award_Requirement_Category()
 *		- modify Remove_Announcement_From_Class(), add checking if the imploded string is empty or not	
 * 20160513 Cameron:
 * 		- replace if(trim($array)!='') by if($array) in: Get_Student_Reading_Record_And_Report_Grade(), Settings_Get_Category_Info(),
 * 			Manage_Award_Scheme_Level(), Get_Popular_Award_scheme_Item_Category_Info(), Get_Popular_Award_Scheme_Student_Score(),
 * 			Get_Popular_Award_Scheme_Award_Form_Mapping(), Get_Popular_Award_Scheme_Award_Info()
 * 		- check if $AwardLevelDataArr is empty before retrieve its element in 'remove' action in Manage_Award_Scheme_Level()
 * 		- fix bug: support back slash in StateName and Description in Settings_Insert_State_Info()
 * 20160512 Cameron: 
 * 		- add filter condition BookFormat<>'physical' to Get_ELIB_Book_List(), but there's still problem 
 * 			in showing the list if number of eBook is very large (e.g. 10000), consider breaking it into pages in the future
 * 		- use if($array) to replace if(trim($array)!='') if $array is an array in Get_Assign_Reading_Info(), Get_Class_Assign_Reading_Target(),
 * 			Settings_Remove_Assign_Reading_From_Class()
 * 		- fix bug: save OnlineWritingDesc when assign a book in Settings_Create_Assign_Reading 
 * 20160511 Cameron: modify Remove_Recommend_Book_From_Form(), assigning each execution step result to an array instead of string,
 * 		return true if all results are successful. This supports php5.4. There's no such error before php5.4 because it evaluate
 * 		($YearID != '') to be true if $YearID is an empty array
 * 20140804 Charles Ma: 20140804-overallpercent
 * 			- E64462 - IP25 Training - Reading Scheme
 * 20130927 Yuen  : modified Get_New_My_Reading_Book_Valid_Book_List() to not include student submitted books;
 * 					modified Get_Class_Announcement() to not show the announcement of award scheme which is already deleted!
 * 20130227 Rita  : add  enableReadingSchemeEditISBNByStudentRight()		 
 * 20130206 Rita  : add flag function enableReadingSchemeAddPopularDataTogetherRight()
 * 20121106 Rita  : modified Get_My_Record_My_Reading_Book_DBTable_Sql() AcademicYear Condition
 * 20120925 Rita  : modified Get_Category_List() add $CategoryCode;
 * 					add Delete_Award_Import_Temp_Data() for deleting import award 's temp data
 * 20120924 Rita  : add Delete_Book_Import_Temp_Data() for deleting import book 's temp data
 * 20120427 Marcus: modified Get_Student_Recommend_Book_List_DBTable_Sql, add return value BookReportRequired
 * 20110914 Carlos: fixed Get_Answer_Sheet_From_Assigned_Reading_Or_ReadingRecord() add back missing filter condition by active RecordStatus
 * 20110527 Carlos: add comment bank and sticker to report marking page; add Grant_View_Right_For_Report_Page()
 * 20110526 Carlos: add sticker functions 
 * 20110525 Carlos: add comment bank functions
 * 20110513 Carlos: add award level to award setting
 * 20101221 Marcus:
 * 		- modified Get_Book_List, change bookID condition, allow retrive in batch
 * 20101221 Marcus:
 * 		- modified Get_Book_List, add param CategoryID
 * 		- modified Assign_Recommend_Book_To_Form, Remove_Recommend_Book_From_Form, Get_Recommend_Book_Mappings, from READING_GARDEN_CLASS_RECOMMEND_BOOK_MAPPING to READING_GARDEN_YEAR_RECOMMEND_BOOK_MAPPING
 * 		- modified Get_Recommend_Book_Mappings, default get current academic year
 ************************************************************************************/
include_once("libdb.php");
include_once("libaccessright.php");

define("BOOK_REPORT_SOURCE_BOOK_LIST",1);
define("BOOK_REPORT_SOURCE_EXISTING_BOOK",2);
define("BOOK_REPORT_SOURCE_ANY_BOOK",3);

define("BOOK_SOURCE_INPUT_BY_ADMIN",1);
define("BOOK_SOURCE_INPUT_FROM_ELIB",2);
define("BOOK_SOURCE_INPUT_BY_STUDENT",3);

define("RECORD_STATUS_DELETED",0);
define("RECORD_STATUS_ACTIVE",1);
define("RECORD_STATUS_WAITING_APPROVED",2);
define("RECORD_STATUS_REJECTED",3);

define("CATEGORY_LANGUAGE_CHINESE",1);
define("CATEGORY_LANGUAGE_ENGLISH",2);
define("CATEGORY_LANGUAGE_OTHERS",3);

define("FUNCTION_NAME_BOOK","Book");
define("FUNCTION_NAME_BOOK_REPORT","Report");
define("FUNCTION_NAME_TOPIC","Topic");
define("FUNCTION_NAME_POST","Post");

define("AWARD_SCHEME_TYPE_INDIVIDUAL",1);
define("AWARD_SCHEME_TYPE_GROUP",2);

define("ANNOUNCEMENT_TYPE_ANNOUNCEMENT",1);
define("ANNOUNCEMENT_TYPE_AWARD",2);

define("READING_STATUS_UNREAD",2);
define("READING_STATUS_READ",1);

define("ACTION_SCORE_LOGIN","LoginScore");
define("ACTION_SCORE_LIKE","PressLikeButton");
define("ACTION_SCORE_COMMENT","WriteComment");
define("ACTION_SCORE_COMMENT_LENGTH","CommentLengthScore");
define("ACTION_SCORE_START_THREAD","StartAThreadInForum");
define("ACTION_SCORE_FOLLOW_THREAD","FollowAThreadInForum");
define("ACTION_SCORE_ADD_READING_RECORD","AddReadingRecord");
define("ACTION_SCORE_ADD_BOOK_REPORT","WriteBookReport");
define("ACTION_SCORE_RECOMMEND_REPORT","TeacherRecommendReport");

define("ACTION_PENALTY_WITHOUT_LOGIN","WithoutLoginPenalty");
define("ACTION_PENALTY_COMMENT_DELETED","CommentDeletedByAdmin");
define("ACTION_PENALTY_THREAD_DELETED","ForumThreadDeletedByAdmin");
define("ACTION_PENALTY_POST_DELETED","ForumPostDeletedByAdmin");

define("ACTION_SETTING_COMMENT_LENGTH","CommentMoreThanEvery");
define("ACTION_SETTING_TIMES_OF_LIKE","TimesOfLike");
define("ACTION_SETTING_LOGIN_MORE_THAN","LoginMoreThan");
define("ACTION_SETTING_DAYS_WITHOUT_LOGIN","SuccessiveDaysWithoutLogin");

define("AWARD_TYPE_GENERAL",1);
define("AWARD_TYPE_ADDITIONAL",2);

class libreadinggarden extends libdb
{
	var $Settings = array();
	
	function libreadinggarden($LoginPage=0)
	{
		parent::libdb();
		
		$this->ModulePath = "/home/eLearning/reading_garden";
		$this->ModuleFilePath = "/file/reading_garden";
		$this->BookPath = $this->ModuleFilePath."/books";
		$this->BookReportPath = $this->ModuleFilePath."/book_report";
		$this->BookCoverImagePath = $this->BookPath."/cover_image";
		$this->AssignReadingAttachmentPath = $this->ModuleFilePath."/assign_reading";
		$this->AnouncementAttachmentPath = $this->ModuleFilePath."/announcement";
		$this->AwardSchemeAttachmentPath = $this->ModuleFilePath."/award_scheme";
		$this->ForumAttachmentPath = $this->ModuleFilePath."/forum";
		$this->StateLogoPath = $this->ModuleFilePath."/state";
		$this->StickerPath = $this->ModuleFilePath."/sticker";
		$this->FunctionNameArr = array(FUNCTION_NAME_BOOK,FUNCTION_NAME_BOOK_REPORT, FUNCTION_NAME_TOPIC, FUNCTION_NAME_POST);
		
		$this->Check_Access_Right();
		$this->Load_Settings();
		
		$this->isStudent = $_SESSION['UserType'] == 2?1:0;
		
		if($this->isStudent && $LoginPage!=1 )
			$this->Update_Login_Session();
			
		// function to delete sofed deleted record which were deleted 3 months ago, run once a month only
		$this->Trash_Soft_Deleted_Record();
	}
	
	function db_db_query($sql) // override function db_db_query, add logging logic for sql which delete record 
	{
		if(stristr($sql,"delete from"))
		{
			$this->Log_Delete_Sql($sql);
		}
		return parent::db_db_query($sql);
	}
	
	function Log_Delete_Sql($LogSql) // log any delete sql called by reading scheme, and the page and functions which trigger the delete sql
	{
		$trace = array_pop(debug_backtrace());
		$LogSql = $this->Get_Safe_Sql_Query($LogSql);
		$CallerPage = $this->Get_Safe_Sql_Query($trace['file']);
		$FunctionName = $this->Get_Safe_Sql_Query($trace['function']);

		$sql = "
			INSERT INTO	READING_GARDEN_DELETE_RECORD_LOG
				(DeleteSql, CallerPage, FunctionName, DateDeleted, DeletedBy)
			VALUES
				('$LogSql', '$CallerPage', '$FunctionName', NOW(),'".$_SESSION['UserID']."')
		";
		
		return parent::db_db_query($sql);
	}
	
	function Check_Access_Right()
	{
		global $plugin, $PATH_WRT_ROOT;
		
		if (!$plugin['ReadingScheme'])
		{
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			intranet_closedb();
			exit;
		}
		
		
		if($_SESSION['SSV_USER_ACCESS']['eLearning-ReadingScheme'])
		{
			$this->isAdmin = 1;
			return true; 
		}
		else if(true)
		{
			
		}
		else
		{
			header("location: ".$PATH_WRT_ROOT.$this->ModulePath."?Msg=AccessDenied");
		}
	}
	
	function Update_Login_Session()
	{
		global $PATH_WRT_ROOT;
		
		if(!isset($_SESSION['READING_SCHEME_SESSION']['LoginSessionID'])) //not yet access reading scheme login page yet  
		{
			header("location: ".$PATH_WRT_ROOT."home/eLearning/reading_garden/student_login.php");
			exit;
		}
			
		$sql = "
			UPDATE 
				READING_GARDEN_LOGIN_SESSION_RECORD
			SET
				DateModified = NOW()
			WHERE
				LoginSessionID = '".$_SESSION['READING_SCHEME_SESSION']['LoginSessionID']."'
		";
		$Success = $this->db_db_query($sql);
		
		$this->Scoring_Action_Login_Duration();
	}
	
	function Student_Login_Process()
	{
		$CurTime = time();
		
		// get the most recent login day
		$sql = "
			SELECT
				DATE(StartTime) LastLoginDate,
				MIN(StartTime) StartTime,
				SUM(TIMESTAMPDIFF(SECOND,StartTime,DateModified)) LoginDuration,
				TIMESTAMPDIFF(DAY, DATE(StartTime), CURDATE()) AS DateDiff
			FROM
				READING_GARDEN_LOGIN_SESSION_RECORD
			WHERE
				UserID = '".$_SESSION['UserID']."'
			GROUP BY 
				DATE(StartTime)
			ORDER BY 
				DATE(StartTime) DESC
			LIMIT 1
		";

		$LoginArr = $this->returnArray($sql);
		
		// store num of "successive days without login" if it is more than 1 day.
		$NoOfDaysWithoutLogin = $LoginArr[0]['DateDiff']-1;
		if($NoOfDaysWithoutLogin>0) 
		{
			$PenaltyCount = floor($NoOfDaysWithoutLogin/$this->Settings[ACTION_SETTING_DAYS_WITHOUT_LOGIN]);
			$Success['AddScoreAction'] = $this->Update_State_Score_Action(ACTION_PENALTY_WITHOUT_LOGIN, $PenaltyCount);
		}
			
		$ReceivedLoginScore = $this->Has_Receive_Login_Score_Today();
		if(!$ReceivedLoginScore)
		{
			$LoginDuration = 0;
			if($LoginArr[0]['DateDiff']==0) // if last login date is today, get the total login duration today
			{
				$LoginDuration = $LoginArr[0]['LoginDuration'];
			}	
			
			$_SESSION['READING_SCHEME_SESSION']['LoginDuration'] = $LoginDuration;
			$_SESSION['READING_SCHEME_SESSION']['LoginTime'] = $CurTime;
		}
		else
		{
			$_SESSION['READING_SCHEME_SESSION']['ReceivedLoginScore'] = 1;
		}
		
		$TimeStr = date("Y-m-d H:i:s",$CurTime);
		
		$sql = "
			INSERT INTO READING_GARDEN_LOGIN_SESSION_RECORD
				(UserID, StartTime, DateModified)
			VALUES
				('".$_SESSION['UserID']."','$TimeStr','$TimeStr')
		";
		
		$this->db_db_query($sql);
		$_SESSION['READING_SCHEME_SESSION']['LoginSessionID'] = mysql_insert_id();
		
		$thisClassInfo = $this->Get_Student_ClassInfo($_SESSION['UserID']);
		$_SESSION['READING_SCHEME_SESSION']['StudentClassID'] = $thisClassInfo[0]['YearClassID'];
	}

	// function to delete sofed deleted record which were deleted 3 months ago, run once a month only
	function Trash_Soft_Deleted_Record()
	{
		$TableArr = array(
			"READING_GARDEN_ANNOUNCEMENT",
			"READING_GARDEN_ANSWER_SHEET_TEMPLATE",
			"READING_GARDEN_AWARD_SCHEME",
			"READING_GARDEN_BOOK",
			"READING_GARDEN_BOOK_CATEGORY",
			"READING_GARDEN_BOOK_COMMENT",
			"READING_GARDEN_BOOK_REPORT",
			"READING_GARDEN_BOOK_REPORT_COMMENT",
			"READING_GARDEN_COMMENT_BANK",
			"READING_GARDEN_FORUM",
			"READING_GARDEN_FORUM_POST",
			"READING_GARDEN_FORUM_TOPIC",
			"READING_GARDEN_READING_RECORD",
			"READING_GARDEN_REPORT_STICKER",
			"READING_GARDEN_STATE",
			"READING_GARDEN_STUDENT_ASSIGNED_READING",
			"READING_GARDEN_STUDENT_READING_TARGET"
		);
		
		$LastTrashDate = $this->Settings['LastTrashDate'];
		
		if(Convert_Date_To_DaysAgo($LastTrashDate)>30 || !$LastTrashDate) // trash deleted record every month
		{
			foreach($TableArr as $thisTable)
			{
				$sql = "DELETE FROM $thisTable WHERE RecordStatus = ".RECORD_STATUS_DELETED." AND DATEDIFF(CURDATE(), DATE(DateModified)) > 90 ";
				$SuccessArr[] = $this->db_db_query($sql, 1);
			}
			
			$Now = date("Y-m-d");
			$SettingArr["LastTrashDate"] = $Now;
			$SuccessArr[] = $this->Settings_Update_Settings($SettingArr);	
		}
		
	}
	
	function Has_Receive_Login_Score_Today($StudentID='')
	{
		if(trim($StudentID) =='')
			$StudentID = $_SESSION['UserID'];
			
		$sql = "
			SELECT 
				COUNT(*)
			FROM
				READING_GARDEN_STUDENT_ACTION_HISTORY
			WHERE
				StudentID = '$StudentID'
				AND ActionName = '".ACTION_SCORE_LOGIN."'
				AND DATE(DateInput) = CURDATE()
		";
		
		$ScoringTime = $this->returnVector($sql);
		return 	$ScoringTime[0];
	}

	function Update_State_Score_Action($Action, $Count='', $StudentID='')
	{
		if(trim($StudentID)=='' && !$this->isStudent)
			return false;
		else if(trim($StudentID)=='' && $this->isStudent)
			$StudentID = $_SESSION['UserID'];
		
		if(trim($Count)=='')
			$Count = 1;
		
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$sql = "
			INSERT INTO READING_GARDEN_STUDENT_ACTION_HISTORY
				(StudentID, ActionName, Count, AcademicYearID, DateInput)
			VALUES
				('$StudentID', '$Action', '$Count', '$AcademicYearID', NOW())
		";
		
		$Success = $this->db_db_query($sql);
		return $Success;
	}
	
	function Update_Portal_Class_Session()
	{
		global $ClassID;
		
		if($this->isStudent)
			$ClassID = $_SESSION['READING_SCHEME_SESSION']['StudentClassID']; # init in Student_Login_Process()
		else if(trim($ClassID)!='')
			$_SESSION['READING_SCHEME_SESSION']['TeacherViewSelectClass'] = $ClassID;
		else
			$ClassID = $_SESSION['READING_SCHEME_SESSION']['TeacherViewSelectClass'];
	}
		
	/***********************************Carlos Part***********************************/
	function Get_Book_List($BookID="",$Keyword="", $CategoryID="", $Language="", $AccurateSearch='', $IncludesStudent=1)
	{
		
		$sql = "SELECT 
					b.BookID, b.CallNumber, b.ISBN, b.BookName, b.Author, b.Publisher, 
					b.Description, b.BookCoverImage, b.eBookID, b.Language, b.DefaultAnswerSheet, b.DefaultAnswerSheetID,
					c.CategoryID, c.CategoryCode, c.CategoryName, b.ByStudent,
					b.DisplayOrder, b.DateModified, b.LastModifiedBy 
				FROM READING_GARDEN_BOOK as b ";
		$sql .= "LEFT JOIN READING_GARDEN_BOOK_CATEGORY as c ON c.CategoryID = b.CategoryID AND c.RecordStatus = ".RECORD_STATUS_ACTIVE." ";
		$sql .= "WHERE 
					b.RecordStatus = '".RECORD_STATUS_ACTIVE."' 
				";
		if($BookID!="")
			$sql .= " AND b.BookID IN ('".implode("','",(array)$BookID)."') ";
		if($CategoryID!=='' && $CategoryID==0)
			$sql .= " AND c.CategoryID IS NULL ";
		else if($CategoryID!="")
			$sql .= " AND c.CategoryID IN ('".implode("','",(array)$CategoryID)."') ";
		if($Language!="")
			$sql .= " AND b.Language = '$Language' ";	
		if($IncludesStudent!=1)
			$sql .= " AND (b.ByStudent = '' OR b.ByStudent IS NULL)";	
		if($AccurateSearch)
		{
			foreach((array)$AccurateSearch as $SearchField => $SearchVal)
			{
				if(trim($SearchVal)!='')
					$sql .= " AND $SearchField LIKE '%$SearchVal%' ";
			}
		}	
		else if(trim($Keyword)!=""){
			$Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
			$sql .= " AND ( b.CallNumber LIKE '%".$Keyword."%' OR
							b.ISBN LIKE '%".$Keyword."%' OR 
							b.BookName LIKE '%".$Keyword."%' OR 
							b.Author LIKE '%".$Keyword."%' OR
							b.Publisher LIKE '%".$Keyword."%' OR
							b.Description LIKE '%".$Keyword."%' ) ";
		}
		$sql .= "ORDER BY b.DisplayOrder, b.CallNumber, b.BookName ";
		
		$result = $this->returnArray($sql);
		
		return $result;
	}
	
	function Get_Book_List_DBTable_Sql($Keyword='',$CategoryID='', $BookSourceArr='')
	{
		global $Lang, $image_path,$LAYOUT_SKIN;
		if($CategoryID!=='' && $CategoryID==0)
			$cond_CategoryID = " AND bc.CategoryID IS NULL ";
		else if(trim($CategoryID)!=''){
			$cond_CategoryID = " AND bc.CategoryID = '$CategoryID' ";
		}
		if(trim($Keyword)!='')
		{
			$Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
			$cond_Keyword = "
				AND
				(
					b.BookName LIKE '%".$Keyword."%'
					OR b.CallNumber LIKE '%".$Keyword."%'
					OR b.ISBN LIKE '%".$Keyword."%'
					OR b.Author LIKE '%".$Keyword."%'
					OR b.Publisher LIKE '%".$Keyword."%'
					OR b.Description LIKE '%".$Keyword."%' 
					OR bc.CategoryName LIKE '%".$Keyword."%' 
					OR bc.CategoryCode LIKE '%".$Keyword."%' 
					OR CONCAT(bc.CategoryCode,' ',bc.CategoryName) LIKE '%".$Keyword."%' 
				) ";
		}
		if(!$BookSourceArr){ // default hide books input by student
			$BookSourceArr = array(BOOK_SOURCE_INPUT_BY_ADMIN, BOOK_SOURCE_INPUT_FROM_ELIB);
		}
		
		$cond_BookSource = " AND ( 0";
			if(in_array(BOOK_SOURCE_INPUT_BY_ADMIN,$BookSourceArr))
				$cond_BookSource .= "  OR (ByStudent IS NULL AND eBookID IS NULL) ";
			if(in_array(BOOK_SOURCE_INPUT_FROM_ELIB,$BookSourceArr))
				$cond_BookSource .= " OR b.eBookID IS NOT NULL";
			if(in_array(BOOK_SOURCE_INPUT_BY_STUDENT,$BookSourceArr))
				$cond_BookSource .= " OR b.ByStudent = 1 ";
		$cond_BookSource .= ")";
		
		$cond_ByStudent = "";
		
		$sql = "
			SELECT
				/*CONCAT('<a title=\'".$Lang['Btn']['Edit']." - ',REPLACE(b.BookName,'\'','&#039;'),'\' class=\'edit thickbox\' href=\'#TB_inline?height=600&width=750&inlineId=FakeLayer\' onclick=\'Get_Book_Edit_Form(0,', b.BookID,' )\' >', b.BookName,'</a>') as BookName,
				CONCAT(IF(b.BookCoverImage IS NULL,'<img src=\"".$image_path."/".$LAYOUT_SKIN."/reading_scheme/temp_book_cover.gif',CONCAT('<img src=\"".$this->BookCoverImagePath."/',b.BookCoverImage)), '\" width=\"100px\" height=\"120px\">') as Cover,*/
				CONCAT(
					'<img class=\"preload_image\" src=\"".$image_path."/".$LAYOUT_SKIN."/icon_pic.gif\" path=\"',
					IF(
						b.BookCoverImage IS NOT NULL AND b.BookCoverImage <> '',
						CONCAT('".$this->BookCoverImagePath."/',b.BookCoverImage),
						'".$image_path."/".$LAYOUT_SKIN."/reading_scheme/temp_book_cover.gif'
					),
					'\" width=\"14px\" height=\"14px\" align=\"absmiddle\" valign=\"2\" style=\"float:left\">',
					CONCAT('&nbsp;<a title=\'".$Lang['Btn']['Edit']." - ',REPLACE(b.BookName,'\'','&#039;'),'\' class=\'edit thickbox\' href=\'#TB_inline?height=600&width=750&inlineId=FakeLayer\' onclick=\'Get_Book_Edit_Form(0,', b.BookID,' )\' >', b.BookName,'</a>'),
					CONCAT('&nbsp;<img src=\"".$image_path."/".$LAYOUT_SKIN."/reading_scheme/icon_book.gif\" width=\"14px\" height=\"14px\" align=\"absmiddle\" valign=\"2\" onclick=\"js_View_Book_Detail(',b.BookID,');\" title=\"".$Lang['ReadingGarden']['ViewBookDetail']."\" />')
				) BookName,				
				b.CallNumber,				
				b.Author,
				b.Publisher,
				CONCAT(bc.CategoryCode,' ',bc.CategoryName) CategoryName,
				b.DateModified,
				CONCAT('<input type=\"checkbox\" name=\"BookID[]\" value=\"',b.BookID,'\">') as CheckBox
			FROM
				READING_GARDEN_BOOK as b 
				LEFT JOIN READING_GARDEN_BOOK_CATEGORY as bc ON bc.CategoryID = b.CategoryID AND bc.RecordStatus = ".RECORD_STATUS_ACTIVE." ";
		$sql .= "WHERE 
				b.RecordStatus = '".RECORD_STATUS_ACTIVE."' 
				$cond_CategoryID
				$cond_Keyword
				$cond_ByStudent
				$cond_BookSource
		";	
		
		return $sql;
	}
	
	function Get_ELIB_Book_List($BookID='',$Keyword='')
	{
		$sql = "SELECT DISTINCT
					a.BookID as ELIBBookID,
					a.Title, 
					a.Author,
					a.Category,
					a.SubCategory,
					a.Publisher,
					a.Source,
					b.BookID,
					a.Preface,
					a.Language,
					a.ISBN,
					IF(b.eBookID IS NOT NULL AND b.RecordStatus <> 1, NULL, b.eBookID) as eBookID ,
					a.CallNumber
				FROM 
					INTRANET_ELIB_BOOK a 
					LEFT JOIN READING_GARDEN_BOOK as b ON b.eBookID = a.BookID AND b.RecordStatus = '".RECORD_STATUS_ACTIVE."'
				WHERE a.Publish = 1 
					AND a.BookFormat<>'physical' ";
		if(is_array($BookID) && sizeof($BookID)>0){
			$sql .= " AND a.BookID IN (".implode(",",$BookID).") ";
		}else if(trim($BookID)!=""){
			$sql .= " AND a.BookID = '".$BookID."' ";
		}
		if(trim($Keyword)!=''){
			$Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
			$sql .= " AND (
							a.Title LIKE '%".$Keyword."%'
							OR a.Author LIKE '%".$Keyword."%' 
							OR a.Category LIKE '%".$Keyword."%'
							OR a.Publisher LIKE '%".$Keyword."%' 
							OR a.Source LIKE '%".$Keyword."%' 
							OR a.SubCategory LIKE '%".$Keyword."%' 
						) ";
		}
		$sql .= "ORDER BY a.Title, a.Author";
		
		return $this->returnArray($sql);
	}
	
	// Params: BookInfo (array)
	function Create_Book($BookInfo)
	{
		$CallNumber = "'".$this->Get_Safe_Sql_Query($BookInfo['CallNumber'])."'";
		$ISBN = "'".$this->Get_Safe_Sql_Query($BookInfo['ISBN'])."'";
		$BookName = trim($BookInfo['BookName'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($BookInfo['BookName'])."'";
		$Author = trim($BookInfo['Author'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($BookInfo['Author'])."'";
		$Publisher = trim($BookInfo['Publisher'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($BookInfo['Publisher'])."'";
		$Description = trim($BookInfo['Description'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($BookInfo['Description'])."'";
		$BookCoverImage = trim($BookInfo['BookCoverImage'])==""?"NULL":"'".$BookInfo['BookCoverImage']."'";
		$eBookID = trim($BookInfo['eBookID'])==""?"NULL":$BookInfo['eBookID'];
		$Language = trim($BookInfo['Language'])==""?"NULL":$BookInfo['Language'];
		$CategoryID = !trim($BookInfo['CategoryID'])?"NULL":$BookInfo['CategoryID'];
		$ByStudent = trim($BookInfo['ByStudent'])==""?"NULL":$BookInfo['ByStudent'];
		$DisplayOrder = trim($BookInfo['DisplayOrder'])==""?"NULL":$BookInfo['DisplayOrder'];
		$DefaultAnswerSheetID = trim($BookInfo['AnswerSheet'])==""?"NULL":$BookInfo['AnswerSheet'];
		
		$values = $CallNumber.",".$ISBN.",".$BookName.",".$Author.",".$Publisher.",".$Description.",".$BookCoverImage.",".$eBookID.",".$Language.",".$CategoryID.",".$ByStudent.",".$DefaultAnswerSheetID.",'".RECORD_STATUS_ACTIVE."',".$DisplayOrder;
		$values .= ",NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."'";
		
		$sql = "INSERT INTO READING_GARDEN_BOOK 
					(CallNumber, ISBN, BookName,Author,Publisher,Description,BookCoverImage,eBookID,Language,
					CategoryID,ByStudent,DefaultAnswerSheetID,RecordStatus,DisplayOrder,DateInput,DateModified,InputBy,LastModifiedBy)
				VALUES(".$values.") ";
		
		$result = $this->db_db_query($sql);
		if($result){
			$insert_id = $this->db_insert_id();
			
			$AssignToFormArr = sizeof($BookInfo['YearID'])>0?array_values($BookInfo['YearID']):array();
			$assign_form_result = $this->Assign_Recommend_Book_To_Form(array("BookID"=>$insert_id,"YearID"=>$AssignToFormArr));
			
			if(trim($BookInfo['AnswerSheet'])!='' && $BookInfo['AnswerSheet']>0){
				$AnswerSheetTemplate = $this->Get_Answer_Sheet_Template($BookInfo['AnswerSheet']);
				$this->Set_Book_Default_Answersheet($insert_id,$AnswerSheetTemplate[0]['AnswerSheet']);
			}
			
			return $insert_id;
		}else{
			return false;
		}
	}
	
	// Params: BookInfo (array)
	function Update_Book($BookInfo)
	{
		$BookID = $BookInfo['BookID'];
		$CallNumber = "'".$this->Get_Safe_Sql_Query($BookInfo['CallNumber'])."'";
		$ISBN = "'".$this->Get_Safe_Sql_Query($BookInfo['ISBN'])."'";
		$BookName = trim($BookInfo['BookName'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($BookInfo['BookName'])."'";
		$Author = trim($BookInfo['Author'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($BookInfo['Author'])."'";
		$Publisher = trim($BookInfo['Publisher'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($BookInfo['Publisher'])."'";
		$Description = trim($BookInfo['Description'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($BookInfo['Description'])."'";
		$eBookID = trim($BookInfo['eBookID'])==""?"NULL":$BookInfo['eBookID'];
		$Language = trim($BookInfo['Language'])==""?"NULL":$BookInfo['Language'];
		$CategoryID = !trim($BookInfo['CategoryID'])?"NULL":$BookInfo['CategoryID'];
		$ByStudent = trim($BookInfo['ByStudent'])==""?"NULL":$BookInfo['ByStudent'];
		$DisplayOrder = trim($BookInfo['DisplayOrder'])==""?"NULL":$BookInfo['DisplayOrder'];
		$DefaultAnswerSheetID = trim($BookInfo['AnswerSheet'])==""?"NULL":$BookInfo['AnswerSheet'];
		
		$sql = "UPDATE READING_GARDEN_BOOK SET 
					CallNumber = $CallNumber,
					ISBN = $ISBN,
					BookName = $BookName,
					Author = $Author,
					Publisher = $Publisher,
					Description = $Description,
					eBookID = $eBookID,
					Language = $Language,
					CategoryID = $CategoryID,
					ByStudent = $ByStudent,
					DefaultAnswerSheetID = $DefaultAnswerSheetID,
					DisplayOrder = $DisplayOrder,
					DateModified = NOW(),
					LastModifiedBy = '".$_SESSION['UserID']."' 
				WHERE BookID = '".$BookID."' ";
		$result['UpdateBook'] = $this->db_db_query($sql);
		
		$TempOldMappings = $this->Get_Recommend_Book_Mappings(array('BookID'=>$BookID));
		$OldMappings = array();
		for($i=0;$i<sizeof($TempOldMappings);$i++){
			$OldMappings[] = $TempOldMappings[$i]['YearID'];
		}
		$NewMappings = sizeof($BookInfo['YearID'])>0?$BookInfo['YearID']:array();
		
		$removes = array_values(array_diff($OldMappings,$NewMappings));
		$adds = array_values(array_diff($NewMappings,$OldMappings));
		
		$result['AddForms'] = $this->Assign_Recommend_Book_To_Form(array('BookID'=>$BookID,'YearID'=>$adds));
		$result['RemoveForms'] = $this->Remove_Recommend_Book_From_Form(array('BookID'=>$BookID,'YearID'=>$removes));
		
		if(trim($BookInfo['AnswerSheet'])!='' && $BookInfo['AnswerSheet']>0){
			$AnswerSheetTemplate = $this->Get_Answer_Sheet_Template($BookInfo['AnswerSheet']);
			$result['AssignAnswerSheet'] = $this->Set_Book_Default_Answersheet($BookID,$AnswerSheetTemplate[0]['AnswerSheet']);
		}
		
		return !in_array(false,$result);
	}
	
	// Soft delete
	function Remove_Book($BookID)
	{
		$sql = "UPDATE READING_GARDEN_BOOK SET RecordStatus = ".RECORD_STATUS_DELETED.", LastModifiedBy = '".$_SESSION['UserID']."', DateModified = NOW() ";
		if(is_array($BookID) && sizeof($BookID)>0){
			$sql .= "WHERE BookID IN (".implode(",",$BookID).") ";
		}else{
			$sql .= "WHERE BookID = '".$BookID."' ";
		}
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function Update_Book_Cover_Image($BookID,$ImageLocation,$ImageName){
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$fs = new libfilesystem();
		
		# Create folder if not exist
		$folder_prefix = $intranet_root.$this->ModuleFilePath;
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
		$folder_prefix .= "/books";
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
		$folder_prefix .= "/cover_image";
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
		
		# Rename photo file
		$extention = $fs->file_ext($ImageName);
		$filename = $BookID.'_'.time().$extention;
		
		$Result['CopyFile'] = $fs->file_copy($ImageLocation, $folder_prefix."/".$filename);
		if($Result['CopyFile']){
			$sql = "UPDATE READING_GARDEN_BOOK SET BookCoverImage = '".$this->Get_Safe_Sql_Query($filename)."', DateModified=NOW(), LastModifiedBy='".$_SESSION['UserID']."' 
					WHERE BookID = '".$BookID."' ";
			$Result['SetDBCoverImage'] = $this->db_db_query($sql);
		}
		
		return !in_array(false,$Result);
	}
	
	function Delete_Book_Cover_Image($BookID,$FileName="")
	{
		global $PATH_WRT_ROOT, $intranet_root;
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$fs = new libfilesystem();
		if(trim($FileName)==""){
			$sql = "SELECT BookCoverImage FROM READING_GARDEN_BOOK WHERE BookID='".$BookID."' ";
			$res = $this->returnArray($sql);
			$FileName = $res[0]['BookCoverImage'];
		}
		$Result=array();
		if(trim($FileName)!=""){
			$file = $intranet_root.$this->BookCoverImagePath."/".$FileName;
			$Result['RemoveFile'] = $fs->file_remove($file);
			if($Result['RemoveFile']==true){
				$sql = "UPDATE READING_GARDEN_BOOK SET BookCoverImage=NULL, DateModified=NOW(), LastModifiedBy='".$_SESSION['UserID']."' WHERE BookID='".$BookID."' ";
				$Result['RemoveDBCoverImage'] = $this->db_db_query($sql);
			}
		}
		return !in_array(false,$Result);
	}
	
	function Get_Category_List($Keyword="",$Language="",$CategoryCode="")
	{
		$sql = "SELECT 
					CategoryID,
					CategoryCode,
					CategoryName,
					Language,
					DisplayOrder 
				FROM READING_GARDEN_BOOK_CATEGORY 
				WHERE RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
		if(trim($Keyword)!=""){
			$sql .= " AND CategoryName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' ";
		}
		if(trim($Language)!=""){
			$sql .= " AND Language IN ('".$Language."','".CATEGORY_LANGUAGE_OTHERS."') ";
		}
		if(trim($CategoryCode)!=""){
			$sql .= " AND CategoryCode IN ('".$CategoryCode."')";
		}
		
		$sql .= "ORDER BY DisplayOrder, CategoryCode,CategoryName";
		return $this->returnArray($sql);
	}
	
	function Get_Answer_Sheet_Template($TemplateID="",$TemplateName="",$CategoryID="")
	{
		$sql = "SELECT 
					TemplateID,
					TemplateName,
					AnswerSheet,
					RecordStatus,
					DateInput,
					DateModified,
					InputBy,
					LastModifiedBy 
				FROM READING_GARDEN_ANSWER_SHEET_TEMPLATE 
				WHERE 
					RecordStatus = '".RECORD_STATUS_ACTIVE."' 
				";
		if(is_array($TemplateID) && sizeof($TemplateID)>0){
			$sql .= " AND TemplateID IN (".implode(",",$TemplateID).") ";
		}else if($TemplateID != ""){
			$sql .= " AND TemplateID = '".$TemplateID."' ";
		}
		if(trim($TemplateName)!="")
			$sql .= " AND TemplateName = '".$this->Get_Safe_Sql_Query($TemplateName)."' ";
		$sql .= "ORDER BY TemplateName ";
		
		return $this->returnArray($sql);
	}
	
	function New_Answer_Sheet_Template($ParArr)
	{
		$TemplateName = trim($ParArr['TemplateName'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($ParArr['TemplateName'])."'";
		$AnswerSheet = trim($ParArr['AnswerSheet'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($ParArr['AnswerSheet'])."'";
		
		$sql = "INSERT INTO READING_GARDEN_ANSWER_SHEET_TEMPLATE 
				(TemplateName,AnswerSheet,RecordStatus,DateInput,DateModified,InputBy,LastModifiedBy)
				VALUES ($TemplateName,$AnswerSheet,'".RECORD_STATUS_ACTIVE."',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
		return $this->db_db_query($sql);
	}
	
	function Update_Answer_Sheet_Template($ParArr)
	{
		$TemplateID = $ParArr['TemplateID'];
		$TemplateName = trim($ParArr['TemplateName'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($ParArr['TemplateName'])."'";
		$AnswerSheet = trim($ParArr['AnswerSheet'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($ParArr['AnswerSheet'])."'";
		$sql = "UPDATE READING_GARDEN_ANSWER_SHEET_TEMPLATE SET 
					TemplateName = $TemplateName,
					AnswerSheet = $AnswerSheet, 
					DateModified = NOW(),
					LastModifiedBy = '".$_SESSION['UserID']."'
				WHERE TemplateID = '".$TemplateID."' ";
		return $this->db_db_query($sql);
	}
	
	function Delete_Answer_Sheet_Template($TemplateID)
	{
		$sql = "DELETE FROM READING_GARDEN_ANSWER_SHEET_TEMPLATE  ";
		if(is_array($TemplateID) && sizeof($TemplateID)>0){
			$sql .= "WHERE TemplateID IN (".implode(",",$TemplateID).")";
		}else{
			$sql .= "WHERE TemplateID = '$TemplateID'";
		}
		return $this->db_db_query($sql);
	}
	
	//
	function Update_Answer_Sheet_Template_Category_Mapping($DateArr)
	{
		if(count($DateArr)==0 || !is_array($DateArr)) return false;
		
		$Success['Remove'] = $this->Delete_Answer_Sheet_Template_Category_Mapping($DateArr['TemplateID']);
		$CategoryID = array_remove_empty($DateArr['CategoryID']);		
		if(count($CategoryID)>0)
			$Success['Add'] = $this->Insert_Answer_Sheet_Template_Category_Mapping($DateArr['TemplateID'], $CategoryID);
		
		if(in_array(false, $Success))
		{
			return false;
		}
		else
		{
			return true;
		}
		
	}
 
	function Delete_Answer_Sheet_Template_Category_Mapping($AnswerSheetID)
	{
		if(count($AnswerSheetID)==0) return false;
		
		$AnswerSheetIDStr = "'".implode("','",(array)$AnswerSheetID)."'";
		
		$sql = "
			DELETE FROM
				READING_GARDEN_CATEGORY_ANSWERSHEET_MAPPING	 
			WHERE
				AnswerSheetID IN (".$AnswerSheetIDStr.")
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;
	}

	function Insert_Answer_Sheet_Template_Category_Mapping($AnswerSheetID, $CategoryIDArr)
	{
		if(empty($AnswerSheetID) || count($CategoryIDArr)==0 || !is_array($CategoryIDArr)) return false;
		
		foreach((array)$CategoryIDArr as $CategoryID)
			$InsertSqlArr[] = "('$AnswerSheetID','$CategoryID', ".$_SESSION['UserID'].", NOW())";
			
		$InsertSql = implode(',',$InsertSqlArr);	
		
		$sql = "
			INSERT INTO	READING_GARDEN_CATEGORY_ANSWERSHEET_MAPPING
				(AnswerSheetID, CategoryID, InputBy, DateInput)	 
			VALUES
				$InsertSql
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;		
	}
	
	function Get_Category_Answer_Sheet_Mapping($CategoryID='', $AnswerSheetID='')
	{
		if($CategoryID)
			$cond_CategoryID = " AND cam.CategoryID IN (".implode(",",(array)$CategoryID).") ";
		if($AnswerSheetID)
			$cond_AnswerSheetID = " AND cam.AnswerSheetID IN (".implode(",",(array)$AnswerSheetID).") ";
		
		$sql = "
			SELECT
				cam.CategoryID,
				cam.AnswerSheetID
			FROM
				READING_GARDEN_CATEGORY_ANSWERSHEET_MAPPING cam
				LEFT JOIN READING_GARDEN_ANSWER_SHEET_TEMPLATE ast ON ast.TemplateID = cam.AnswerSheetID AND ast.RecordStatus = ".RECORD_STATUS_ACTIVE."
			WHERE
				1
				$cond_CategoryID
				$cond_AnswerSheetID
		";
		
		$Result = $this->returnArray($sql, NULL, 1);
		return $Result;
	}
	//
	
	function Get_Answer_Sheet_Template_DBTable_Sql($Keyword='')
	{
		global $Lang, $image_path, $LAYOUT_SKIN;
		$NameFieldModifiedBy = getNameFieldByLang("u2.");
		$sql = "SELECT 
					CONCAT('<a class=\'thickbox\' href=\'#TB_inline?height=600&width=750&inlineId=FakeLayer\' onclick=\'Get_Answersheet_Template_Preview_Form(',t.TemplateID,')\'><img border=\"0\" alt=\"\" src=\"".$image_path."/".$LAYOUT_SKIN."/icon_view.gif\"></a>'),
					CONCAT('<a title=\'".$Lang['Btn']['Edit']." - ',REPLACE( REPLACE(t.TemplateName,'\'','&#039;'),'<','&lt;'),'\' class=\'edit thickbox\' href=\'#TB_inline?height=600&width=750&inlineId=FakeLayer\' onclick=\'Get_Answersheet_Template_Form(0,', t.TemplateID,' )\' >', REPLACE(t.TemplateName,'<','&lt;'),'</a>') as TemplateName,
					t.DateModified,
					$NameFieldModifiedBy as LastModifiedBy,
					CONCAT('<input type=\"checkbox\" name=\"TemplateID[]\" value=\"',t.TemplateID,'\">') as CheckBox 
				FROM 
					READING_GARDEN_ANSWER_SHEET_TEMPLATE as t 
					LEFT JOIN INTRANET_USER as u1 ON u1.UserID = t.InputBy 
					LEFT JOIN INTRANET_USER as u2 ON u2.UserID = t.LastModifiedBy 
				WHERE 
					t.RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
		if(trim($Keyword)!='')
		{
			$Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
			$sql .= " AND t.TemplateName LIKE '%".$Keyword."%' ";
		}
		
		return $sql;
	}
	
	function Set_Book_Default_Answersheet($BookID,$AnswerSheet)
	{
		$sql = "UPDATE READING_GARDEN_BOOK SET ";
		$AnswerSheet = trim($AnswerSheet);
		if($AnswerSheet=="")
			$sql .= "DefaultAnswerSheet = NULL ";
		else
			$sql .= "DefaultAnswerSheet = '".$this->Get_Safe_Sql_Query($AnswerSheet)."' ";
		if(is_array($BookID) && sizeof($BookID)>0){
			$sql .= "WHERE BookID IN (".implode(",",$BookID).") ";
		}else{
			$sql .= "WHERE BookID = '$BookID' ";
		}
		return $this->db_db_query($sql);
	}
	
	function Get_Book_Comments($BookID,$UID="")
	{
		$sql = "SELECT 
					BookCommentID, BookID, UserID, Comment, DateInput, DateModified, InputBy, LastModifiedBy 
				FROM READING_GARDEN_BOOK_COMMENT 
				WHERE BookID = '$BookID' AND RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
		if(is_array($UID) && sizeof($UID)>0){
			$sql .= " AND UserID IN (".implode(",",$UID).") ";
		}else if(trim($UID)!=""){
			$sql .= " AND UserID = '$UID' ";
		}
		return $this->returnArray($BookID);
	}
	
	function Manage_Book_Comments($ParArr)
	{
		$action = $ParArr['action'];
		$BookCommentID = $ParArr['BookCommentID'];
		$BookID = $ParArr['BookID'];
		$UID = $ParArr['UserID'];
		$Comment = trim($ParArr['Comment'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($ParArr['Comment'])."'";
		
		$result = true;
		if(strcasecmp($action,"new")==0){
			$sql = "INSERT INTO READING_GARDEN_BOOK_COMMENT (BookID,UserID,Comment,DateInput.DateModified,InputBy,LastModifiedBy) 
					VALUES ('$BookID','$UID',$Comment,NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."') ";
			$result = $this->db_db_query($sql);
		}else if(strcasecmp($action,"update")==0){
			$sql = "UPDATE READING_GARDEN_BOOK_COMMENT SET
						Comment = $Comment,
						DateModified = NOW(),
						LastModifiedBy = '".$_SESSION['UserID']."'  
					WHERE BookCommentID = '$BookCommentID' ";
			$result = $this->db_db_query($sql);
		}else if(strcasecmp($action,"remove")==0 || strcasecmp($action,"delete")==0){
			$sql = "DELETE FROM READING_GARDEN_BOOK_COMMENT WHERE BookCommentID = '$BookCommentID' ";
			$result = $this->db_db_query($sql);
		}
		
		return $result;
	}
	
	function Assign_Recommend_Book_To_Form($ParArr)
	{
		$BookID = $ParArr['BookID'];
		$YearID = $ParArr['YearID'];
		$AcademicYearID = $ParArr['AcademicYearID']?$ParArr['AcademicYearID']:Get_Current_Academic_Year_ID();
		
		$Result=array();
		if(is_array($YearID) && sizeof($YearID)>0){
			for($i=0;$i<sizeof($YearID);$i++){
				$cid = $YearID[$i];
				$sql = "INSERT INTO READING_GARDEN_FORM_RECOMMEND_BOOK_MAPPING 
						(BookID,YearID,AcademicYearID,DateInput,InputBy)
						VALUES ('$BookID','$cid','$AcademicYearID',NOW(),'".$_SESSION['UserID']."') ";
				$Result['InsertToForm'.$cid] = $this->db_db_query($sql);
			}
		}else if(trim($YearID)!=""){
			$sql = "INSERT INTO READING_GARDEN_FORM_RECOMMEND_BOOK_MAPPING 
						(BookID,YearID,AcademicYearID,DateInput,InputBy)
						VALUES ('$BookID','$YearID','$AcademicYearID',NOW(),'".$_SESSION['UserID']."') ";
			$Result['InsertToForm'.$YearID] = $this->db_db_query($sql);
		}
		return !in_array(false,$Result);
	}
	
	function Remove_Recommend_Book_From_Form($ParArr)
	{
		$BookID = $ParArr['BookID'];
		$YearID = $ParArr['YearID'];
		$RecommendBookID = $ParArr['RecommendBookID'];
		$AcademicYearID = $ParArr['AcademicYearID']?$ParArr['AcademicYearID']:Get_Current_Academic_Year_ID();

		$result=array();
		if($RecommendBookID)	{
			$sql = "DELETE FROM READING_GARDEN_FORM_RECOMMEND_BOOK_MAPPING WHERE RecommendBookID IN (".(implode(",",(array)$RecommendBookID)).") ";
			$result[] = $this->db_db_query($sql);
		}
		else if(is_array($YearID) && sizeof($YearID)>0){
			$sql = "DELETE FROM READING_GARDEN_FORM_RECOMMEND_BOOK_MAPPING WHERE BookID = '$BookID' 
					AND YearID IN (".implode(",",$YearID).") AND AcademicYearID = '$AcademicYearID' ";
			$result[] = $this->db_db_query($sql);
		}else if(!is_array($YearID) && trim($YearID)!=""){
			$sql = "DELETE FROM READING_GARDEN_FORM_RECOMMEND_BOOK_MAPPING WHERE BookID = '$BookID' AND YearID = '$YearID' AND AcademicYearID = '$AcademicYearID' ";
			$result[] = $this->db_db_query($sql);
		}
		
		return !in_array(false,$result);
	}
	
	function Get_Recommend_Book_Mappings($ParArr)
	{
		$BookID = $ParArr['BookID'];
		$YearID = $ParArr['YearID'];
//		$AcademicYearID = $ParArr['AcademicYearID']?$ParArr['AcademicYearID']:Get_Current_Academic_Year_ID();
		
		$sql = "SELECT rbm.RecommendBookID, rbm.BookID, rbm.YearID, rbm.AcademicYearID, rbm.DateInput, rbm.InputBy 
				FROM READING_GARDEN_FORM_RECOMMEND_BOOK_MAPPING rbm
				INNER JOIN READING_GARDEN_BOOK b ON rbm.BookID = b.BookID AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
				WHERE 1";
		if($BookID){
			$sql .= " AND rbm.BookID IN (".implode(",",(array)$BookID).") ";
		}
		if(is_array($YearID) && sizeof($YearID)>0){
			$sql .= " AND rbm.YearID IN (".implode(",",$YearID).") ";
		}else if(trim($YearID)){
			$sql .= " AND rbm.YearID = '$YearID' ";
		}
//		$sql .= " AND rbm.AcademicYearID = '$AcademicYearID'  ";
		$sql .= " ORDER BY b.DateInput DESC ";
		
		return $this->returnArray($sql);
	}
	
//	function Get_Award_Scheme_List($AwardSchemeID="",$Keyword="",$Language="")
//	{
//		$Result = $this->Get_Cache_Award_Scheme_List($AwardSchemeID, $Keyword, $Language);
//		$AcademicYearID = $AcademicYearID!=""?$AcademicYearID:Get_Current_Academic_Year_ID();
//		$sql = "SELECT 
//					a.AwardSchemeID, a.AwardName, a.AwardType, a.NumberOfWinners,
//					a.Language, a.Target, a.MinBookRead, a.MinReportSubmit, 
//					DATE_FORMAT(a.StartDate,'%Y-%m-%d') as StartDate, 
//					DATE_FORMAT(a.EndDate,'%Y-%m-%d') as EndDate, 
//					DATE_FORMAT(a.ResultPublishDate,'%Y-%m-%d') as ResultPublishDate  
//				FROM READING_GARDEN_AWARD_SCHEME as a 
//				WHERE a.RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
//		
//		if(trim($AwardSchemeID)!="")
//			$sql .= "AND a.AwardSchemeID = '".$AwardSchemeID."' ";
//		if(trim($Language)!="")
//			$sql .= "AND a.Language = '".$Language."' ";
//		$Keyword = trim($Keyword);
//		if($Keyword!=""){
//			$Keyword = $this->Get_Safe_Sql_Query($Keyword);
//			$sql .= " AND (a.AwardName LIKE '%".$Keyword."%' 
//							OR a.Target LIKE '%".$Keyword."%') ";
//		}
//		$sql .= "ORDER BY a.StartDate, a.AwardName, a.AwardType, a.Language ";
//		
//		$Result = $this->returnArray($sql);
//
//		return $Result;
//	}
	
	//20111013
	function Get_Award_Scheme_List($AwardSchemeID="",$Keyword="",$Language="", $InProgressOnly="", $ParClassID="")
	{
		# get from cached array
		if(trim($AwardSchemeID)!='' && trim($AwardSchemeID)!='0' && trim($Keyword)=='' && trim($Language)=='' && !$InProgressOnly && trim($ParClassID)=='') // Get Info by $AwardSchemeID
		{
			foreach((array)$AwardSchemeID as $thisAwardSchemeID)
			{
				if($this->Cache_Award_Scheme_List_Arr[$thisAwardSchemeID]) // return Cached Award_Scheme info if exist
				{
					$returnArr[] = $this->Cache_Award_Scheme_List_Arr[$thisAwardSchemeID];
				}
				else // if any one do not exist, skip getting from cached array. get from db instead 
				{
					$NoCacheAwardScheme = 1;
					break;
				}
			}
			if(!$NoCacheAwardScheme)
				return $returnArr;
		}
		
		# get from db
		if(trim($ParClassID)!='')
		{
			$AcademicYearID = Get_Current_Academic_Year_ID();
			$INNER_JOIN_READING_GARDEN_AWARD_SCHEME_TARGET_CLASS = "INNER JOIN READING_GARDEN_AWARD_SCHEME_TARGET_CLASS b ON a.AwardSchemeID = b.AwardSchemeID  AND b.ClassID IN (".implode(",",(array)$ParClassID).") AND b.AcademicYearID = '$AcademicYearID' ";
		}
		
		$sql = "SELECT 
					a.AwardSchemeID, a.AwardName, a.AwardType, a.NumberOfWinners,
					a.Language, a.Target, a.MinBookRead, a.MinReportSubmit, 
					DATE_FORMAT(a.StartDate,'%Y-%m-%d') as StartDate, 
					DATE_FORMAT(a.EndDate,'%Y-%m-%d') as EndDate, 
					DATE_FORMAT(a.ResultPublishDate,'%Y-%m-%d') as ResultPublishDate,
					a.Description,
					a.Attachment 
				FROM 
					READING_GARDEN_AWARD_SCHEME as a 
					$INNER_JOIN_READING_GARDEN_AWARD_SCHEME_TARGET_CLASS
				WHERE a.RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
		
		if($AwardSchemeID)
			$sql .= "AND a.AwardSchemeID IN (".implode(",",(array)$AwardSchemeID).") ";
		if($Language)
			$sql .= "AND a.Language IN (".implode(",",(array)$Language).") ";
		$Keyword = trim($Keyword);
		if($Keyword!=""){
			$Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
			$sql .= " AND (a.AwardName LIKE '%".$Keyword."%' 
							OR a.Target LIKE '%".$Keyword."%') ";
		}
		if($InProgressOnly==1){
			$sql .= " AND NOW() BETWEEN a.StartDate AND a.EndDate ";
		}
		
		$sql .= "ORDER BY a.StartDate, a.AwardName, a.AwardType, a.Language ";
		
		$Result = $this->returnArray($sql);
		foreach((array)$Result as $Rec)
		{
			$this->Cache_Award_Scheme_List_Arr[$Rec['AwardSchemeID']] = $Rec;
		} 
		
		return $Result;
	}
	
	function Check_Award_Scheme_Name($AwardName)
	{
		$sql = "SELECT COUNT(*) FROM READING_GARDEN_AWARD_SCHEME WHERE AwardName = '".$this->Get_Safe_Sql_Query($AwardName)."' ";
		$result = $this->returnVector($sql);
		return ($result[0]>0?false:true);
	}
	
	// modified by marcus, also add record to announcement
	function Add_Award_Scheme($ParArr)
	{
		$AwardName = $this->Get_Safe_Sql_Query($ParArr['AwardName']);
		$AwardType = $ParArr['AwardType'];
		$Language = trim($ParArr['Language'])==""?"NULL":$ParArr['Language'];
		$Target = trim($ParArr['Target'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($ParArr['Target'])."'";
	//	$MinBookRead = trim($ParArr['MinBookRead'])==""?"NULL":$ParArr['MinBookRead'];
	//	$MinReportSubmit = trim($ParArr['MinReportSubmit'])==""?"NULL":$ParArr['MinReportSubmit'];
		$StartDate = $ParArr['StartDate'];
		$EndDate = $ParArr['EndDate'];
		$ResultPublishDate = $ParArr['ResultPublishDate'];
		$TargetClassItem = array_values((array)$ParArr['TargetClassItem']);
	//	$RequirementItemCategory = sizeof($ParArr['RequirementItemCategory'])>0?array_values($ParArr['RequirementItemCategory']):array();
	//	$RequirementItemQuantity = sizeof($ParArr['RequirementItemQuantity'])>0?array_values($ParArr['RequirementItemQuantity']):array();
		$Level = $ParArr['Level'];
		$Description = $this->Get_Safe_Sql_Query($ParArr['Description']);
		$Attachment = $ParArr['Attachment'];
		
		$sql = "INSERT INTO READING_GARDEN_AWARD_SCHEME 
				(AwardName,AwardType,Language,Target,StartDate,EndDate,ResultPublishDate,Description,
				Attachment, RecordStatus,DateInput,DateModified,InputBy,LastModifiedBy)
				VALUES ('".$AwardName."',
						'".$AwardType."',
						".$Language.",
						".$Target.",
						'".$StartDate."',
						'".$EndDate."',
						'".$ResultPublishDate."',
						'".$Description."',
						'".$Attachment."',
						".RECORD_STATUS_ACTIVE.",
						NOW(),
						NOW(),
						'".$_SESSION['UserID']."',
						'".$_SESSION['UserID']."'
						)";
		
		$Result['InsertAwardScheme'] = $this->db_db_query($sql);
		if(!in_array(false,$Result)){
			$InsertAwardSchemeID = $this->db_insert_id();
			
			# add announcement record
			$DataArr['AwardSchemeID'] = $InsertAwardSchemeID;
			$DataArr['Title'] = $ParArr['AwardName'];
			$DataArr['StartDate'] = $StartDate;
			$DataArr['ClassID'] = $TargetClassItem;
			
			$Result['InsertAnnouncement'] = $this->Manage_Announcement_Record("add", $DataArr);
			
			//if(sizeof($RequirementItemCategory)>0){
			//	for($i=0;$i<sizeof($RequirementItemCategory);$i++)
			//		$Result['InsertRequirement'.$i] = $this->Manage_Award_Scheme_Requirement("add",$InsertAwardSchemeID,$RequirementItemCategory[$i],$RequirementItemQuantity[$i]);
			//}
			if(sizeof($TargetClassItem)>0){
				//for($i=0;$i<sizeof($TargetClassItem);$i++){
					$Result['InsertTargetClass'] = $this->Manage_Award_Scheme_Target_Class("add",$InsertAwardSchemeID,$TargetClassItem);
				//}
			}
			
			for($i=0;$i<sizeof($Level);$i++){
				$AwardLevelDataArr = $Level[$i];
				$this->Manage_Award_Scheme_Level("add",$InsertAwardSchemeID,'',$AwardLevelDataArr);
			}
			
			return !in_array(false,$Result);
		}else{
			return false;
		}
	}
	
	function Update_Award_Scheme($ParArr)
	{
		$AwardSchemeID = $ParArr['AwardSchemeID'];
		$AwardName = $this->Get_Safe_Sql_Query($ParArr['AwardName']);
		$AwardType = $ParArr['AwardType'];
		$Language = trim($ParArr['Language'])==""?"NULL":$ParArr['Language'];
		$Target = trim($ParArr['Target'])==""?"NULL":"'".$this->Get_Safe_Sql_Query($ParArr['Target'])."'";
		//$MinBookRead = trim($ParArr['MinBookRead'])==""?"NULL":$ParArr['MinBookRead'];
		//$MinReportSubmit = trim($ParArr['MinReportSubmit'])==""?"NULL":$ParArr['MinReportSubmit'];
		$StartDate = $ParArr['StartDate'];
		$EndDate = $ParArr['EndDate'];
		$ResultPublishDate = $ParArr['ResultPublishDate'];
		$TargetClassItem = array_values((array)$ParArr['TargetClassItem']);
		//$RequirementItemCategory = sizeof($ParArr['RequirementItemCategory'])>0?array_values($ParArr['RequirementItemCategory']):array();
		//$RequirementItemQuantity = sizeof($ParArr['RequirementItemQuantity'])>0?array_values($ParArr['RequirementItemQuantity']):array();
		$AcademicYearID = $AcademicYearID!=""?$AcademicYearID:Get_Current_Academic_Year_ID();
		$WinnerGenerated = $ParArr['WinnerGenerated'];
		$Description = $this->Get_Safe_Sql_Query($ParArr['Description']);
		$Attachment = $ParArr['Attachment'];
		
		$sql = "UPDATE READING_GARDEN_AWARD_SCHEME SET
					AwardName = '".$AwardName."',
					AwardType = '".$AwardType."',
					Language = $Language,
					Target = ".$Target.",
					StartDate = '".$StartDate."',
					EndDate = '".$EndDate."',
					ResultPublishDate = '".$ResultPublishDate."',
					Description = '".$Description."',
					Attachment = '".$Attachment."',
					DateModified = NOW(),
					LastModifiedBy = '".$_SESSION['UserID']."' 
				WHERE AwardSchemeID = '".$AwardSchemeID."' ";
		$Result['UpdateAwardScheme'] = $this->db_db_query($sql);
		if($Result['UpdateAwardScheme']){
			/*
			$OldAwardRequirement = $this->Get_Award_Scheme_Requirement($AwardSchemeID);
			$OldRequirementItemCategory = array();
			$OldRequirementItemQuantity = array();
			for($i=0;$i<sizeof($OldAwardRequirement);$i++){
				$OldRequirementItemCategory[] = $OldAwardRequirement[$i]['CategoryID'];
				$OldRequirementItemQuantity[] = $OldAwardRequirement[$i]['MinBookRead'];
			}
			$CategoryItemToAdd = array_diff($RequirementItemCategory,$OldRequirementItemCategory);
			$CategoryItemToRemove = array_diff($OldRequirementItemCategory,$RequirementItemCategory);
			$CategoryItemToUpdate = array_diff($RequirementItemCategory,$CategoryItemToAdd);
			$CategoryItemToUpdate = array_diff($CategoryItemToUpdate,$CategoryItemToRemove);
			if(sizeof($CategoryItemToAdd)>0){
				foreach((array)$CategoryItemToAdd as $index => $cat_id){
					$Result['AddCategory'.$index] = $this->Manage_Award_Scheme_Requirement("add",$AwardSchemeID,$cat_id,$RequirementItemQuantity[$index]);
				}
			}
			if(sizeof($CategoryItemToRemove)>0){
				$Result['RemoveCategory'] = $this->Manage_Award_Scheme_Requirement("remove",$AwardSchemeID,array_values($CategoryItemToRemove),"");
			}
			if(sizeof($CategoryItemToUpdate)>0){
				foreach((array)$CategoryItemToUpdate as $index => $cat_id){
					$Result['UpdateCategory'.$index] = $this->Manage_Award_Scheme_Requirement("update",$AwardSchemeID,$cat_id,$RequirementItemQuantity[$index]);
				}
			}
			*/
			# update award level
			$AwardLevelArr = $ParArr['Level'];
			
			$NewAwardLevelArr = array();
			$ExistAwardLevelArr = array();
			for($i=0;$i<sizeof($AwardLevelArr);$i++){
				$award_level_id = trim($AwardLevelArr[$i]['AwardLevelID']);
				if($award_level_id=='')
					$NewAwardLevelArr[] = $AwardLevelArr[$i];
				else
					$ExistAwardLevelArr[] = $AwardLevelArr[$i];
			}
			$ExistAwardLevelAssocArr = array();
			$ExistAwardLevelIDArr = array();
			for($i=0;$i<sizeof($ExistAwardLevelArr);$i++){
				$ExistAwardLevelAssocArr[$ExistAwardLevelArr[$i]['AwardLevelID']] = $ExistAwardLevelArr[$i];
				$ExistAwardLevelIDArr[] = $ExistAwardLevelArr[$i]['AwardLevelID'];
			}
			$OldAwardLevelArr = $this->Get_Award_Scheme_Level($AwardSchemeID);
			$OldAwardLevelAssocArr = array();
			$OldAwardLevelIDArr = array();
			for($i=0;$i<sizeof($OldAwardLevelArr);$i++){
				$award_requirement = $this->Get_Award_Scheme_Requirement($OldAwardLevelArr[$i]['AwardLevelID']);
				$requirement_categoryid = Get_Array_By_Key($award_requirement,'CategoryID');
				$requirement_minbookread = Get_Array_By_Key($award_requirement,'MinBookRead');
				$OldAwardLevelArr[$i]['RequirementItemCategory'] = $requirement_categoryid;
				$OldAwardLevelArr[$i]['RequirementItemQuantity'] = $requirement_minbookread;
				$OldAwardLevelAssocArr[$OldAwardLevelArr[$i]['AwardLevelID']] = $OldAwardLevelArr[$i];
				$OldAwardLevelIDArr[] = $OldAwardLevelArr[$i]['AwardLevelID'];
			}
			
			$UpdateAwardLevelIDArr = array_values(array_intersect($OldAwardLevelIDArr,$ExistAwardLevelIDArr));
			$RemoveAwardLevelIDArr = array_values(array_diff($OldAwardLevelIDArr,$ExistAwardLevelIDArr));
			
			for($i=0;$i<sizeof($NewAwardLevelArr);$i++){
				$Result['add_award_level_'.$i] = $this->Manage_Award_Scheme_Level("add",$AwardSchemeID,'',$NewAwardLevelArr[$i]);
			}
			for($i=0;$i<sizeof($UpdateAwardLevelIDArr);$i++){
				$Result['update_award_level_'.$i] = $this->Manage_Award_Scheme_Level("update",$AwardSchemeID,$UpdateAwardLevelIDArr[$i],$ExistAwardLevelAssocArr[$UpdateAwardLevelIDArr[$i]]);
			}
			for($i=0;$i<sizeof($RemoveAwardLevelIDArr);$i++){
				$Result['remove_award_level_'.$i] = $this->Manage_Award_Scheme_Level("remove",$AwardSchemeID,$RemoveAwardLevelIDArr[$i],$OldAwardLevelAssocArr[$RemoveAwardLevelIDArr[$i]]);
			}
			
			# update target classes
			$OldTargetClass = $this->Get_Award_Scheme_Target_Class($AwardSchemeID,$AcademicYearID);
			$OldTargetClassItem = array();
			for($i=0;$i<sizeof($OldTargetClass);$i++){
				$OldTargetClassItem[] = $OldTargetClass[$i]['ClassID'];
			}
			$TargetClassItemToAdd = array_diff($TargetClassItem,$OldTargetClassItem);
			$TargetClassItemToRemove = array_diff($OldTargetClassItem,$TargetClassItem);
			if(sizeof($TargetClassItemToAdd)>0)
				$Result['AddTargetClass'] = $this->Manage_Award_Scheme_Target_Class("add",$AwardSchemeID,array_values($TargetClassItemToAdd));
			if(sizeof($TargetClassItemToRemove)>0)
				$Result['RemoveTargetClass'] = $this->Manage_Award_Scheme_Target_Class("remove",$AwardSchemeID,array_values($TargetClassItemToRemove));
			
			# update announcement record
			$DataArr['AwardSchemeID'] = $AwardSchemeID;
			$DataArr['Title'] = $ParArr['AwardName'];
			$DataArr['StartDate'] = $StartDate;
			$DataArr['ClassID'] = $TargetClassItem;
			$Result['EditAnnouncement'] = $this->Manage_Announcement_Record("edit", $DataArr);	
			
			# remove generated winner list.
			if($WinnerGenerated==1)
			{
				$Result['RemoveWinnerList'] = $this->Settings_Manage_Award_Scheme_Winner("unassign", $AwardSchemeID);				
			}
		}
		
		return !in_array(false,$Result);
	}
	
	function Delete_Award_Scheme($AwardSchemeID)
	{
		$this->Start_Trans();
		
		$award_level_list =  $this->Get_Award_Scheme_Level($AwardSchemeID);
		for($i=0;$i<sizeof($award_level_list);$i++){
			$Result['DeleteAwardLevel'.$i] = $this->Manage_Award_Scheme_Level("remove",$award_level_list[$i]['AwardSchemeID'],$award_level_list[$i]['AwardLevelID'],'');
		}
		
		$sql = "DELETE FROM READING_GARDEN_AWARD_SCHEME WHERE ";
		if(is_array($AwardSchemeID) && sizeof($AwardSchemeID) > 0){
			$Cond_AwardSchemeID = "AwardSchemeID IN (".implode(",",$AwardSchemeID).")";
		}else{
			$Cond_AwardSchemeID = "AwardSchemeID = '".$AwardSchemeID."' ";
		}
		$sql .= $Cond_AwardSchemeID;
		$Result['DeleteAwardScheme'] = $this->db_db_query($sql);
		
		$sql = "DELETE FROM READING_GARDEN_AWARD_SCHEME_TARGET_CLASS WHERE ".$Cond_AwardSchemeID;
		$Result['DeleteAwardTarget'] = $this->db_db_query($sql);
		
		//$sql = "DELETE FROM READING_GARDEN_AWARD_SCHEME_REQUIREMENT WHERE ".$Cond_AwardSchemeID;
		//$Result['DeleteAwardRequirement'] = $this->db_db_query($sql);
		
		$sql = "DELETE FROM READING_GARDEN_AWARD_SCHEME_WINNER WHERE ".$Cond_AwardSchemeID;
		$Result['DeleteAwardWinner'] = $this->db_db_query($sql);
		
		if(in_array(false,$Result)){
			$this->Rollback_Trans();
			return false;
		}else{
			$this->Commit_Trans();
			return true;
		}
	}
	
	function Get_Award_Scheme_Target_Class($AwardSchemeID,$AcademicYearID='', $YearID='')
	{
		$AcademicYearID = $AcademicYearID!=""?$AcademicYearID:Get_Current_Academic_Year_ID();
		if ($YearID != '')
			$cond_YearID = " AND y.YearID IN (".implode(",",(array)$YearID).") ";
		$sql = "SELECT 
					t.AwardTargetID,
					t.AwardSchemeID,
					t.ClassID,
					t.AcademicYearID,
					y.YearID,
					y.YearName,
					yc.YearClassID,
					yc.ClassTitleEN,
					yc.ClassTitleB5 
				FROM 
				ACADEMIC_YEAR as ay
				INNER JOIN YEAR_CLASS as yc ON ay.AcademicYearID = yc.AcademicYearID AND ay.AcademicYearID = '".$AcademicYearID."' 
				INNER JOIN YEAR as y ON y.YearID = yc.YearID 
				INNER JOIN READING_GARDEN_AWARD_SCHEME_TARGET_CLASS as t ON t.ClassID = yc.YearClassID 
				WHERE 
					t.AwardSchemeID = '".$AwardSchemeID."' AND t.AcademicYearID = '".$AcademicYearID."' 
					$cond_YearID 
				ORDER BY
					y.sequence, y.YearName, yc.sequence, yc.ClassTitleEN ";
		return $this->returnArray($sql);
	}
	
	function Manage_Award_Scheme_Target_Class($Action,$AwardSchemeID,$ClassID,$AcademicYearID="")
	{
		$AcademicYearID = $AcademicYearID!=""?$AcademicYearID:Get_Current_Academic_Year_ID();
		if($Action=="add"){
			//$this->Start_Trans();
			$Result = array();
			$ClassID = (array)$ClassID;
			for($i=0;$i<sizeof($ClassID);$i++){
				$sql = "INSERT INTO READING_GARDEN_AWARD_SCHEME_TARGET_CLASS
						(AwardSchemeID,ClassID,AcademicYearID,DateInput,InputBy)
						VALUES('".$AwardSchemeID."','".$ClassID[$i]."','".$AcademicYearID."',NOW(),'".$_SESSION['UserID']."')";
				$Result['InsertTarget'.$ClassID] = $this->db_db_query($sql);
			}
			if(in_array(false,$Result)){
				//$this->Rollback_Trans();
				return false;
			}else{
				//$this->Commit_Trans();
				return true;
			}
		}else if($Action=="remove"){
			if(sizeof($ClassID)==0) return true;
			$sql = "DELETE FROM READING_GARDEN_AWARD_SCHEME_TARGET_CLASS WHERE 
					AwardSchemeID = '".$AwardSchemeID."' AND ClassID IN (".implode(",",(array)$ClassID).") AND AcademicYearID = '".$AcademicYearID."' ";
			return $this->db_db_query($sql);
		}
		return true;
	}
	
	function Get_Award_Scheme_Level($AwardSchemeID)
	{
		$award_scheme_ids = implode(",",(array)$AwardSchemeID);
		$sql = "SELECT 
					asl.AwardLevelID,
					asl.AwardSchemeID,
					asl.Level,
					asl.LevelName,
					asl.MinBookRead,
					asl.MinReportSubmit,
					asl.AwardType,
					GROUP_CONCAT(r.RequirementID SEPARATOR ',') as RequirementIDList 
				FROM READING_GARDEN_AWARD_SCHEME_LEVEL as asl 
				LEFT JOIN READING_GARDEN_AWARD_SCHEME_REQUIREMENT as r ON r.AwardLevelID = asl.AwardLevelID 
				WHERE 
					asl.AwardSchemeID IN ($award_scheme_ids) 
				GROUP BY asl.AwardLevelID 
				ORDER BY asl.Level ";
		
		return $this->returnArray($sql);
	}
	
	function Get_Award_Scheme_Requirement($AwardLevelID='', $StrictJoin=true, $AwardSchemeID='')
	{
		if($AwardLevelID)
			$cond_AwardLevelID = " AND asl.AwardLevelID IN (".implode(",",(array)$AwardLevelID).") ";
		if(trim($AwardSchemeID)!='')
			$cond_AwardSchemeID = " AND asl.AwardSchemeID = '$AwardSchemeID' ";
		
		$join = $StrictJoin? " INNER ": " LEFT ";
		$sql = "SELECT 
					r.RequirementID,
					asl.AwardLevelID,
					r.CategoryID,
					r.MinBookRead,
					c.CategoryCode,
					c.CategoryName,
					c.Language,
					asl.AwardSchemeID,
					asl.Level,
					asl.LevelName,
					asl.MinBookRead as LevelMinBookRead,
					asl.MinReportSubmit,
					asl.AwardType
				FROM 
					READING_GARDEN_AWARD_SCHEME_LEVEL as asl 
					$join JOIN READING_GARDEN_AWARD_SCHEME_REQUIREMENT as r ON r.AwardLevelID = asl.AwardLevelID 
					$join JOIN READING_GARDEN_BOOK_CATEGORY as c ON c.CategoryID = r.CategoryID 
				WHERE 
					1
					$cond_AwardSchemeID
					$cond_AwardLevelID
				ORDER BY asl.Level ";
				
		return $this->returnArray($sql);
	}
	
	function Manage_Award_Scheme_Level($Action,$AwardSchemeID,$AwardLevelID='',$AwardLevelDataArr=array())
	{
		$Action = strtolower($Action);
		if($Action == "add"){
			$Level = $AwardLevelDataArr['Level'];
			$LevelName = $this->Get_Safe_Sql_Query($AwardLevelDataArr['LevelName']);
			$MinBookRead = $AwardLevelDataArr['MinBookRead'];
			$MinReportSubmit = $AwardLevelDataArr['MinReportSubmit'];
			$AwardType = $AwardLevelDataArr['AwardType'];
			$sql = "INSERT INTO READING_GARDEN_AWARD_SCHEME_LEVEL (AwardSchemeID,Level,LevelName,MinBookRead,MinReportSubmit,AwardType)
					VALUES ('$AwardSchemeID','$Level','$LevelName','$MinBookRead','$MinReportSubmit','$AwardType')";
			$Result['insert_level'] = $this->db_db_query($sql);	
			if($Result['insert_level'] && sizeof($AwardLevelDataArr['RequirementItemCategory'])>0){
				$AwardLevelID = $this->db_insert_id();
				//add requirements
				for($i=0;$i<sizeof($AwardLevelDataArr['RequirementItemCategory']);$i++){
					$CategoryID = $AwardLevelDataArr['RequirementItemCategory'][$i];
					$MinBookRead = $AwardLevelDataArr['RequirementItemQuantity'][$i];
					$Result['insert_requirement_'.$i] = $this->Manage_Award_Scheme_Requirement("add",$AwardLevelID,$CategoryID,$MinBookRead);
				}
			}
			
			return !in_array(false,$Result);
		}elseif($Action == "remove"){
			$CategoryID = $AwardLevelDataArr ? $AwardLevelDataArr['RequirementItemCategory'] : '';
			$Result['remove_requirement'] = $this->Manage_Award_Scheme_Requirement("remove",$AwardLevelID,$CategoryID,'');
			$sql = "DELETE FROM READING_GARDEN_AWARD_SCHEME_LEVEL WHERE AwardSchemeID = '$AwardSchemeID' ";
			if($AwardLevelID){
				$sql .= " AND AwardLevelID IN (".implode(",",(array)$AwardLevelID).")";
			}
			$Result['remove_level'] = $this->db_db_query($sql);
			
			return !in_array(false,$Result);
		}elseif($Action == "update"){
			$Level = $AwardLevelDataArr['Level'];
			$LevelName = $this->Get_Safe_Sql_Query($AwardLevelDataArr['LevelName']);
			$MinBookRead = $AwardLevelDataArr['MinBookRead'];
			$MinReportSubmit = $AwardLevelDataArr['MinReportSubmit'];
			$RequirementCategoryIDArr = sizeof($AwardLevelDataArr['RequirementItemCategory'])>0?$AwardLevelDataArr['RequirementItemCategory']:array();
			$RequirementQuantityArr = sizeof($AwardLevelDataArr['RequirementItemQuantity'])>0?$AwardLevelDataArr['RequirementItemQuantity']:array();
			$AwardType = $AwardLevelDataArr['AwardType'];
			$RequirementAssocArr = array(); // CategoryID to Requirement data
			for($i=0;$i<sizeof($RequirementCategoryIDArr);$i++){
				$RequirementAssocArr[$RequirementCategoryIDArr[$i]]['CategoryID'] = $RequirementCategoryIDArr[$i];
				$RequirementAssocArr[$RequirementCategoryIDArr[$i]]['MinBookRead'] = $RequirementQuantityArr[$i];
			}
			
			$sql = "UPDATE READING_GARDEN_AWARD_SCHEME_LEVEL SET
						Level = '$Level',
						LevelName = '$LevelName',
						MinBookRead = '$MinBookRead',
						MinReportSubmit = '$MinReportSubmit',
						AwardType = '$AwardType' 
					WHERE AwardLevelID = '$AwardLevelID' ";
			$Result['update_level'] = $this->db_db_query($sql);
			
			$OldRequirement = $this->Get_Award_Scheme_Requirement($AwardLevelID);
			$OldRequirementCategoryIDArr = Get_Array_By_Key($OldRequirement,'CategoryID');
			$OldRequirementAssocArr = array(); // CategoryID to Requirement data
			for($i=0;$i<sizeof($OldRequirementCategoryIDArr);$i++){
				$OldRequirementAssocArr[$OldRequirementCategoryIDArr[$i]] = $OldRequirement[$i];
			}
			
			$NewRequirementCategoryIDArr = array_diff($RequirementCategoryIDArr,$OldRequirementCategoryIDArr);
			$RemoveRequirementCategoryIDArr = array_diff($OldRequirementCategoryIDArr,$RequirementCategoryIDArr);
			$UpdateRequirementCategoryIDArr = array_intersect($RequirementCategoryIDArr,$OldRequirementCategoryIDArr);
			if(sizeof($NewRequirementCategoryIDArr)>0)
			{
				foreach($NewRequirementCategoryIDArr as $i=>$val){
					$CategoryID = $RequirementAssocArr[$val]['CategoryID'];
					$MinBookRead = $RequirementAssocArr[$val]['MinBookRead'];
					$Result['add_requirement_'.$i] = $this->Manage_Award_Scheme_Requirement("add",$AwardLevelID,$CategoryID,$MinBookRead);
				}
			}
			if(sizeof($UpdateRequirementCategoryIDArr)>0)
			{
				foreach($UpdateRequirementCategoryIDArr as $i=>$val){
					$CategoryID = $RequirementAssocArr[$val]['CategoryID'];
					$MinBookRead = $RequirementAssocArr[$val]['MinBookRead'];
					$Result['update_requirement_'.$i] = $this->Manage_Award_Scheme_Requirement("update",$AwardLevelID,$CategoryID,$MinBookRead);
				}
			}
			$CategoryIDToRemove = $RemoveRequirementCategoryIDArr;
			if(sizeof($CategoryIDToRemove)>0)
				$Result['remove_requirement'] = $this->Manage_Award_Scheme_Requirement("remove",$AwardLevelID,$CategoryIDToRemove,'');
			
			return !in_array(false,$Result);
		}
		return true;
	}
	
	function Manage_Award_Scheme_Requirement($Action,$AwardLevelID,$CategoryID,$MinBookRead)
	{
		if($Action=="add"){
			$sql = "INSERT INTO READING_GARDEN_AWARD_SCHEME_REQUIREMENT 
					(AwardLevelID,CategoryID,MinBookRead,DateInput,InputBy)
					VALUES('".$AwardLevelID."','".$CategoryID."','".$MinBookRead."',NOW(),'".$_SESSION['UserID']."')";
			return $this->db_db_query($sql);
		}else if($Action=="remove"){
			$sql = "DELETE FROM READING_GARDEN_AWARD_SCHEME_REQUIREMENT WHERE AwardLevelID = '".$AwardLevelID."' ";
			if(is_array($CategoryID) && sizeof($CategoryID)>0)
				$sql .= "AND CategoryID IN (".implode(",",$CategoryID).")";
			else
				$sql .= "AND CategoryID = '".$CategoryID."' ";
				
			return $this->db_db_query($sql);
		}else if($Action=="update"){
			$sql = "UPDATE READING_GARDEN_AWARD_SCHEME_REQUIREMENT SET 
					MinBookRead = '".$MinBookRead."'
					WHERE AwardLevelID = '".$AwardLevelID."' AND CategoryID = '".$CategoryID."' ";
			return $this->db_db_query($sql);
		}
		return true;
	}
	
	function Get_Student_Reading_Target_List($ReadingTargetID="",$Keyword="", $AcademicYearID="")
	{
		$AcademicYearID = $AcademicYearID!=""?$AcademicYearID:Get_Current_Academic_Year_ID();
		$sql = "SELECT 
					ReadingTargetID, TargetName, StartDate, EndDate, 
					ReadingRequiredCh, BookReportRequiredCh, ReadingRequiredEn, BookReportRequiredEn, ReadingRequiredOthers 
				FROM READING_GARDEN_STUDENT_READING_TARGET 
				WHERE RecordStatus = '".RECORD_STATUS_ACTIVE."' AND AcademicYearID = '".$AcademicYearID."' ";
		if(trim($ReadingTargetID)!="")
			$sql .= " AND ReadingTargetID = '".$ReadingTargetID."' ";
		if(trim($Keyword)!="")
			$sql .= " AND TargetName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' ";
		$sql .= "ORDER BY TargetName, StartDate";	
		
		return $this->returnArray($sql);
	}
	
	function Add_Student_Reading_Target($ParArr)
	{
		$AcademicYearID = trim($ParArr['AcademicYearID'])!=""?$ParArr['AcademicYearID']:Get_Current_Academic_Year_ID();
		$TargetName = $this->Get_Safe_Sql_Query($ParArr['TargetName']);
		$StartDate = $ParArr['StartDate'];
		$EndDate = $ParArr['EndDate'];
		$ReadingRequiredCh = $ParArr['ReadingRequiredCh'];
		$BookReportRequiredCh = $ParArr['BookReportRequiredCh'];
		$ReadingRequiredEn = $ParArr['ReadingRequiredEn'];
		$BookReportRequiredEn = $ParArr['BookReportRequiredEn'];
		$ReadingRequiredOthers = $ParArr['ReadingRequiredOthers'];
		$ReadingTargetClassID = sizeof($ParArr['ClassID'])>0?array_values($ParArr['ClassID']):array();
		
		$sql = "INSERT INTO READING_GARDEN_STUDENT_READING_TARGET 
				(
					TargetName, StartDate, EndDate, ReadingRequiredCh, BookReportRequiredCh, 
					ReadingRequiredEn, BookReportRequiredEn, ReadingRequiredOthers,AcademicYearID,
					RecordStatus, DateInput, DateModified, InputBy, LastModifiedBy 
				 ) 
				VALUES
				(
					'".$TargetName."', '".$StartDate."', '".$EndDate."', '".$ReadingRequiredCh."','".$BookReportRequiredCh."',
					'".$ReadingRequiredEn."','".$BookReportRequiredEn."', '".$ReadingRequiredOthers."','".$AcademicYearID."',
					'".RECORD_STATUS_ACTIVE."',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."' 
				)";
		$Result['InsertReadingTarget'] = $this->db_db_query($sql);
		if($Result['InsertReadingTarget']){
			$ReadingTargetID = $this->db_insert_id();
			if(sizeof($ReadingTargetClassID)>0){
				$Result['InsertClass'] = $this->Manage_Student_Reading_Target_Class("add",$ReadingTargetID,$ReadingTargetClassID);
			}
			return !in_array(false,$Result);
		}else{
			return false;
		}
	}
	
	function Update_Student_Reading_Target($ParArr)
	{
		$AcademicYearID = trim($ParArr['AcademicYearID'])!=""?$ParArr['AcademicYearID']:Get_Current_Academic_Year_ID();
		$ReadingTargetID = $ParArr['ReadingTargetID'];
		$TargetName = $this->Get_Safe_Sql_Query($ParArr['TargetName']);
		$StartDate = $ParArr['StartDate'];
		$EndDate = $ParArr['EndDate'];
		$ReadingRequiredCh = $ParArr['ReadingRequiredCh'];
		$BookReportRequiredCh = $ParArr['BookReportRequiredCh'];
		$ReadingRequiredEn = $ParArr['ReadingRequiredEn'];
		$BookReportRequiredEn = $ParArr['BookReportRequiredEn'];
		$ReadingRequiredOthers = $ParArr['ReadingRequiredOthers'];
		$ReadingTargetClassID = sizeof($ParArr['ClassID'])>0?array_values($ParArr['ClassID']):array();
		
		$sql = "UPDATE READING_GARDEN_STUDENT_READING_TARGET 
				SET
					TargetName = '".$TargetName."',
					StartDate = '".$StartDate."',
					EndDate = '".$EndDate."',
					ReadingRequiredCh = '".$ReadingRequiredCh."',
					BookReportRequiredCh = '".$BookReportRequiredCh."', 
					ReadingRequiredEn = '".$ReadingRequiredEn."',
					BookReportRequiredEn = '".$BookReportRequiredEn."',
					ReadingRequiredOthers = '".$ReadingRequiredOthers."',
					DateModified = NOW(),
					LastModifiedBy = '".$_SESSION['UserID']."'  
				WHERE ReadingTargetID = '".$ReadingTargetID."' ";
		$Result['UpdateReadingTarget'] = $this->db_db_query($sql);
		if($Result['UpdateReadingTarget']){
			$TargetClassMappings = $this->Get_Student_Reading_Target_Class($ReadingTargetID);
			$OldReadingTargetClassID = array();
			for($i=0;$i<sizeof($TargetClassMappings);$i++){
				$OldReadingTargetClassID[] = $TargetClassMappings[$i]['ClassID'];
			}
			$TargetClassToAdd = array_diff($ReadingTargetClassID,$OldReadingTargetClassID);
			$TargetClassToRemove = array_diff($OldReadingTargetClassID,$ReadingTargetClassID);
			if(sizeof($TargetClassToAdd)>0){
				$Result['InsertClassMapping'] = $this->Manage_Student_Reading_Target_Class("add",$ReadingTargetID,array_values($TargetClassToAdd));
			}
			if(sizeof($TargetClassToRemove)>0){
				$Result['RemoveClassMapping'] = $this->Manage_Student_Reading_Target_Class("delete",$ReadingTargetID,array_values($TargetClassToRemove));
			}
			return !in_array(false,$Result);
		}else{
			return false;
		}
	}
	
	function Delete_Student_Reading_Target($ReadingTargetID)
	{
		$sql = "DELETE FROM READING_GARDEN_STUDENT_READING_TARGET WHERE ReadingTargetID IN (".implode(",",(array)$ReadingTargetID).") ";
		$Result['DeleteReadingTarget'] = $this->db_db_query($sql);
		if($Result['DeleteReadingTarget']){
			$sql = "DELETE FROM READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING WHERE ReadingTargetID IN (".implode(",",(array)$ReadingTargetID).") ";
			$Result['DeleteTargetClass'] = $this->db_db_query($sql);
		}
		return !in_array(false,$Result);
	}
	
	function Check_Student_Reading_Target_Name($ReadingTargetName)
	{
		$sql = "SELECT COUNT(*) FROM READING_GARDEN_STUDENT_READING_TARGET WHERE TargetName = '".$this->Get_Safe_Sql_Query($ReadingTargetName)."' ";
		$result = $this->returnVector($sql);
		return ($result[0]>0?false:true);
	}
	
	function Check_Reading_Target_Overlap_Periods($TargetClassID,$StartDate,$EndDate,$AcademicYearID='',$ReadingTargetID='')
	{
		$AcademicYearID = $AcademicYearID!=""?$AcademicYearID:Get_Current_Academic_Year_ID();
		$YearClassName = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
		
		$sql = "SELECT 
					m.ClassID,
					COUNT(m.TargetClassMappingID) as MappingCount,
					$YearClassName as YearClassName  
				FROM READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING as m 
				INNER JOIN READING_GARDEN_STUDENT_READING_TARGET as t ON m.ReadingTargetID = t.ReadingTargetID 
					AND (
						('$StartDate' BETWEEN t.StartDate AND t.EndDate)
						OR ('$EndDate' BETWEEN t.StartDate AND t.EndDate) 
						OR ('$StartDate' < t.StartDate AND '$EndDate' > t.EndDate)
					) 
					AND t.AcademicYearID = '$AcademicYearID' ";
		if($ReadingTargetID != "") 
			$sql .= "AND t.ReadingTargetID <> '$ReadingTargetID' ";
		$sql .= "LEFT JOIN YEAR_CLASS yc ON m.ClassID = yc.YearClassID 
				 LEFT JOIN YEAR y ON y.YearID = yc.YearID ";
		$sql .= "WHERE 
					m.ClassID IN (".implode(",",(array)$TargetClassID).") 
				GROUP BY m.ClassID 
				ORDER BY y.sequence, y.YearName, yc.sequence, YearClassName ";
		
		$result = $this->returnArray($sql);
		$ResultAssoc = array();
		for($i=0;$i<sizeof($result);$i++){
			$ResultAssoc[$result[$i]['ClassID']] = $result[$i]['MappingCount'];
		}
		return $ResultAssoc;
	}
	
	function Get_Student_Reading_Target_Class($ReadingTargetID,$GetDetails=false,$AcademicYearID='', $YearID='')
	{
		if($GetDetails){
			$AcademicYearID = $AcademicYearID!=""?$AcademicYearID:Get_Current_Academic_Year_ID();
			if ($YearID != '')
				$cond_YearID = " AND y.YearID IN (".implode(",",(array)$YearID).") ";
			$sql = "SELECT 
						t.TargetClassMappingID,
						t.ReadingTargetID,
						t.ClassID,
						y.YearID,
						y.YearName,
						yc.YearClassID,
						yc.ClassTitleEN,
						yc.ClassTitleB5 
					FROM ACADEMIC_YEAR as ay
					INNER JOIN YEAR_CLASS as yc ON ay.AcademicYearID = yc.AcademicYearID AND ay.AcademicYearID = '".$AcademicYearID."' 
					INNER JOIN YEAR as y ON y.YearID = yc.YearID 
					INNER JOIN READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING as t ON t.ClassID = yc.YearClassID 
					WHERE 
						t.ReadingTargetID = '".$ReadingTargetID."'  
						$cond_YearID 
					ORDER BY
						y.sequence, y.YearName, yc.sequence, yc.ClassTitleEN ";
		}else{
			$sql = "SELECT 
						TargetClassMappingID, ReadingTargetID, ClassID 
					FROM READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING 
					WHERE ReadingTargetID = '".$ReadingTargetID."' ";
		}
		return $this->returnArray($sql);
	}
	
	function Manage_Student_Reading_Target_Class($Action, $ReadingTargetID, $ClassID)
	{
		if($Action=="add"){
			$TargetClassID = (array)$ClassID;
			$Result = array();
			$this->Start_Trans();
			for($i=0;$i<sizeof($TargetClassID);$i++){
				$sql = "INSERT INTO READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING 
						(
							ReadingTargetID, ClassID, DateInput, InputBy 
						)VALUES
						(
							'".$ReadingTargetID."','".$TargetClassID[$i]."',NOW(),'".$_SESSION['UserID']."'
						)";
				$Result['InsertClass'.$TargetClassID[$i]] = $this->db_db_query($sql);
			}
			if(in_array(false,$Result)){
				$this->Rollback_Trans();
				return false;
			}else{
				$this->Commit_Trans();
				return true;
			}
		}else if($Action=="delete"){
			$sql = "DELETE FROM READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING WHERE ReadingTargetID = '".$ReadingTargetID."' AND ClassID IN (".implode(",",(array)$ClassID).") ";
			return $this->db_db_query($sql);
		}
		return true;
	}
	/*
	function Get_Student_Assigned_Reading_Record()
	{
		$ClassInfo = $this->Get_Student_ClassInfo($_SESSION['UserID']);
		$ClassID = $ClassInfo[0]['YearClassID'];
		
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$sql = "SELECT
					b.BookID,
					b.BookName,
					b.Author,
					b.BookCoverImage,
					ar.AssignedReadingID,
					rt.AssignedReadingTargetID,
					rr.ReadingRecordID,
					rr.Progress,
					IF(as.AnswerSheet != '' AND as.AnswerSheet IS NOT NULL,1,0) as DoAnswerSheet,
					br.BookReportID  
				FROM READING_GARDEN_STUDENT_ASSIGNED_READING_TARGET as rt 
				INNER JOIN READING_GARDEN_STUDENT_ASSIGNED_READING as ar ON rt.AssignedReadingID = ar.AssignedReadingID AND ar.AcademicYearID = '".$AcademicYearID."'
				INNER JOIN READING_GARDEN_BOOK as b ON b.BookID = ar.BookID 
				LEFT JOIN READING_GARDEN_READING_RECORD as rr ON rr.User = '".$_SESSION['UserID']."' AND rr.BookID = b.BookID AND rr.AcademicYearID = '".$AcademicYearID."' 
				LEFT JOIN READING_GARDEN_BOOK_REPORT as br ON br.ReadingRecordID = rr.ReadingRecordID 
				
				WHERE rt.ClassID = '$ClassID' ";
			
		$result = $this->returnArray($sql);
		return $result;
	}
	*/
	function Get_Student_Recommend_Book_List_DBTable_Sql($CategoryID="",$Keyword="", $BookIDArr="",$ClassID="")
	{
		$AcademicYearID = Get_Current_Academic_Year_ID();
		if($ClassID!='')
		{
			include_once("form_class_manage.php");
			$year_class = new year_class($ClassID);
			$YearID = $year_class->YearID;
		}
		else
		{
			$ClassInfo = $this->Get_Student_ClassInfo($_SESSION['UserID']);
			$YearID = $ClassInfo[0]['YearID'];
			$ClassID = $ClassInfo[0]['YearClassID'];
		}
		if($CategoryID!=='' && $CategoryID==0)
			$cond_CategoryID = " AND bc.CategoryID IS NULL ";
		else if(trim($CategoryID)!=''){
			$cond_CategoryID = " AND bc.CategoryID = '$CategoryID' ";
		}
		
		if($BookIDArr)
		{
			$cond_BookID = " AND b.BookID IN (".implode(',',(array)$BookIDArr).") ";
			$SkipRecommendBookJoin = " OR 1 ";
//			$ORByStudent =  " OR b.ByStudent = 1 ";
		}
		
		if(trim($Keyword)!='')
		{
			$Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
			$cond_Keyword = "
				AND
				(
					b.BookName LIKE '%".$Keyword."%'
					OR b.CallNumber LIKE '%".$Keyword."%'
					OR b.Author LIKE '%".$Keyword."%'
					OR b.Publisher LIKE '%".$Keyword."%'
					OR b.Description LIKE '%".$Keyword."%' 
					OR bc.CategoryName LIKE '%".$Keyword."%' 
					OR bc.CategoryCode LIKE '%".$Keyword."%' 
					OR CONCAT(bc.CategoryCode,' ',bc.CategoryName) LIKE '%".$Keyword."%' 
				) ";
		}
		
		$sql = "SELECT 
					b.BookID, 
					b.CallNumber, 
					b.BookName, 
					b.Author, 
					b.Publisher, 
					b.Description, 
					b.BookCoverImage, 
					b.Language, 
					b.DefaultAnswerSheet,
					bc.CategoryID, 
					bc.CategoryCode, 
					bc.CategoryName, 
					rbm.RecommendBookID,
					rr.ReadingRecordID,
					rr.UserID as StudentID,
					br.BookReportID,
					br.AnswerSheet, 
					br.Attachment,
					br.OnlineWriting,
					br.Grade,
					ar.AssignedReadingID,
					ar.BookReportRequired,
					ar.Attachment as AssignedAttachment,
					ar.AnswerSheet as AssignedAnswerSheet,
					ar.WritingLimit as AssignedOnlineWriting,
					COUNT(art.AssignedReadingTargetID) as NumOfTarget 
				FROM READING_GARDEN_FORM_RECOMMEND_BOOK_MAPPING as rbm 
				INNER JOIN READING_GARDEN_BOOK b ON (rbm.BookID = b.BookID /*AND rbm.AcademicYearID = '$AcademicYearID'*/ 
					AND rbm.YearID = '$YearID' $SkipRecommendBookJoin) AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK_CATEGORY bc ON bc.CategoryID = b.CategoryID AND bc.RecordStatus = ".RECORD_STATUS_ACTIVE." 
				LEFT JOIN READING_GARDEN_READING_RECORD as rr ON rr.UserID = '".$_SESSION['UserID']."' AND rr.BookID = b.BookID AND rr.AcademicYearID = '".$AcademicYearID."' AND rr.RecordStatus = ".RECORD_STATUS_ACTIVE." 
				LEFT JOIN READING_GARDEN_BOOK_REPORT as br ON br.ReadingRecordID = rr.ReadingRecordID AND br.RecordStatus = ".RECORD_STATUS_ACTIVE." 
				LEFT JOIN READING_GARDEN_STUDENT_ASSIGNED_READING as ar ON ar.BookID = b.BookID AND ar.AcademicYearID = '".$AcademicYearID."' AND ar.RecordStatus = ".RECORD_STATUS_ACTIVE." 
				LEFT JOIN READING_GARDEN_STUDENT_ASSIGNED_READING_TARGET as art ON art.AssignedReadingID = ar.AssignedReadingID AND art.ClassID = '".$ClassID."' 
				WHERE 
					1
					$cond_BookID					
					$cond_CategoryID 
					$cond_Keyword 
				 GROUP BY 
					b.BookID ";
					
		return $sql;
	}
	
	function Get_Student_Reading_Record($ReadingRecordID="",$BookID="",$AcademicYearID="")
	{
		$ReadingRecordID = trim($ReadingRecordID);
//		$BookID = trim($BookID);
		$AcademicYearID = trim($AcademicYearID);
		$AcademicYearID = $AcademicYearID==""?Get_Current_Academic_Year_ID():$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$sql = "SELECT 
					ReadingRecordID,
					UserID,
					BookID,
					StartDate,
					FinishedDate,
					NumberOfHours,
					Progress,
					AwardSchemeID,
					AcademicYearID  
				FROM READING_GARDEN_READING_RECORD 
				WHERE 
					AcademicYearID = '".$AcademicYearID."' 
					AND UserID = '".$_SESSION['UserID']."' 
					AND RecordStatus = '".RECORD_STATUS_ACTIVE."' 
		";
		if($BookID!="")
			$sql .= "AND BookID IN (".(implode(',',(array)$BookID)).") ";
		if($ReadingRecordID!="")
			$sql .= "AND ReadingRecordID = '".$ReadingRecordID."' ";
		
		return $this->returnArray($sql);
	}	
	
	// modified by marcus, change back to soft delete
	function Manage_Student_Reading_Record($DataArr)
	{
		$action = $DataArr['Action'];
		$AcademicYearID = trim($DataArr['AcademicYearID']);
		$AcademicYearID = $AcademicYearID==""?Get_Current_Academic_Year_ID():$AcademicYearID;
		
		$Result = array();
		if($action=="add"){
			$BookID = $DataArr['BookID'];
			$StartDate = $DataArr['StartDate'];
			$FinishedDate = $DataArr['FinishedDate'];
			$ReadingHours = $DataArr['ReadingHours'];
			$Progress = $DataArr['Progress'];
			$AwardSchemeID = $DataArr['AwardSchemeID'];
			
			$sql = "INSERT INTO READING_GARDEN_READING_RECORD 
					(UserID, BookID, StartDate, FinishedDate, NumberOfHours, Progress, AwardSchemeID, AcademicYearID,
					RecordStatus, DateInput, DateModified, InputBy, LastModifiedBy)
					VALUES (
						'".$_SESSION['UserID']."',
						'".$BookID."',
						'".$StartDate."',
						'".$FinishedDate."',
						'".$ReadingHours."',
						'".$Progress."',
						'".$AwardSchemeID."',
						'".$AcademicYearID."',
						'".RECORD_STATUS_ACTIVE."',
						NOW(),
						NOW(),
						'".$_SESSION['UserID']."',
						'".$_SESSION['UserID']."' 
					)";
			$Result['AddReadingRecord'] = $this->db_db_query($sql);
			if($Result['AddReadingRecord'])
			{
				$Result['AddScoreAction'] = $this->Update_State_Score_Action(ACTION_SCORE_ADD_READING_RECORD);
			}
			
		}else if($action=="update"){
			$ReadingRecordID = $DataArr['ReadingRecordID'];
			//$BookID = $DataArr['BookID'];
			$StartDate = $DataArr['StartDate'];
			$FinishedDate = $DataArr['FinishedDate'];
			$ReadingHours = $DataArr['ReadingHours'];
			$Progress = $DataArr['Progress'];
			$AwardSchemeID = $DataArr['AwardSchemeID'];
			
			$sql = "UPDATE READING_GARDEN_READING_RECORD SET
						StartDate = '".$StartDate."',
						FinishedDate = '".$FinishedDate."',
						NumberOfHours = '".$ReadingHours."',
						Progress = '".$Progress."',
						AwardSchemeID = '".$AwardSchemeID."',
						DateModified = NOW(),
						LastModifiedBy = '".$_SESSION['UserID']."' 
					WHERE ReadingRecordID = '".$ReadingRecordID."' ";
			$Result['UpdateReadingRecord'] = $this->db_db_query($sql);
		}else if($action=="delete"){
			$ReadingRecordID = $DataArr['ReadingRecordID'];
			
			$sql = "SELECT BookReportID, Attachment  
					FROM READING_GARDEN_BOOK_REPORT 
					WHERE ReadingRecordID = '".$ReadingRecordID."' ";
			$temp = $this->returnArray($sql);
			$BookReportID = trim($temp[0]['BookReportID']);
			$Attachment = trim($temp[0]['Attachment']);
			if($BookReportID!=''){
				if($Attachment!='')
					$Result['DeleteBookReportAttachment'] = $this->Delete_Book_Report_Attachment($BookReportID);
//				$sql = "DELETE FROM READING_GARDEN_BOOK_REPORT WHERE BookReportID = '".$BookReportID."' ";
				$sql = "UPDATE READING_GARDEN_BOOK_REPORT SET RecordStatus = ".RECORD_STATUS_DELETED.", DateModified = NOW(), LastModifiedBy = '".$_SESSION['UserID']."' WHERE BookReportID = '".$BookReportID."' ";
				$Result['DeleteBookReport'] = $this->db_db_query($sql);
			}
			
//			$sql = "DELETE FROM READING_GARDEN_READING_RECORD WHERE ReadingRecordID = '$ReadingRecordID' ";
			$sql = "UPDATE READING_GARDEN_READING_RECORD SET RecordStatus = ".RECORD_STATUS_DELETED.", DateModified = NOW(), LastModifiedBy = '".$_SESSION['UserID']."' WHERE ReadingRecordID = '$ReadingRecordID' ";
			$Result['DeleteReadingRecord'] = $this->db_db_query($sql);
			if($Result['DeleteReadingRecord'])
			{
				$Result['AddScoreAction'] = $this->Update_State_Score_Action(ACTION_SCORE_ADD_READING_RECORD,-1);
			}
		}
		
		
		return !in_array(false,$Result);
	}
	
	function Get_Student_Book_Report($BookReportID,$ReadingRecordID='')
	{
		$sql = "SELECT 
					BookReportID,
					ReadingRecordID,
					Attachment,
					AnswerSheet,
					StudentSelectedAnswerSheetID,
					OnlineWriting,
					Grade,
					Mark,
					TeacherComment,
					Recommend,
					IsEmbed  
				FROM READING_GARDEN_BOOK_REPORT 
				WHERE 
					RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
		if($BookReportID!='') 
			$sql .= " AND BookReportID = '".$BookReportID."' "; 
		if($ReadingRecordID!='') 
			$sql .= " AND ReadingRecordID = '".$ReadingRecordID."' ";
			
		return $this->returnArray($sql);
	}
	
	function Upload_Book_Report_Attachment($DataArr)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$fs = new libfilesystem();
		
		$BookReportID = trim($DataArr['BookReportID']);
		$ReadingRecordID = trim($DataArr['ReadingRecordID']);
		$BookReportFileLocation = trim($DataArr['BookReportFile']);
		$BookReportFileName = trim($DataArr['BookReportFile_name']);
		
		$AddStateScore = false;
		$Result=array();
		if($BookReportID==''){
			$sql = "INSERT INTO READING_GARDEN_BOOK_REPORT 
					(ReadingRecordID,RecordStatus,DateInput,DateModified,InputBy,LastModifiedBy)
					VALUES ('".$ReadingRecordID."','".RECORD_STATUS_ACTIVE."',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
			$Result['CreateBookReport'] = $this->db_db_query($sql);
			if($Result['CreateBookReport'])
				$BookReportID = $this->db_insert_id();
			$AddStateScore = true;
		}else{
			$sql = "SELECT Attachment, AnswerSheet FROM READING_GARDEN_BOOK_REPORT WHERE BookReportID = '".$BookReportID."' ";
			$report_result = $this->returnVector($sql);
			if(trim($report_result[0]['AnswerSheet'])=='')// Not did answer sheet yet
				$AddStateScore = true;
		}
		
		if($BookReportID!='' && $BookReportFileName!='' && $BookReportFileLocation!=''){
			# Create folder if not exist
			$folder_prefix = $intranet_root.$this->ModuleFilePath;
			if (!file_exists($folder_prefix))
				$fs->folder_new($folder_prefix);
			$folder_prefix .= "/book_report";
			if (!file_exists($folder_prefix))
				$fs->folder_new($folder_prefix);
			$folder_prefix .= "/u".$_SESSION['UserID'];
			if (!file_exists($folder_prefix))
				$fs->folder_new($folder_prefix);
			$folder_prefix .= "/r".$BookReportID;
				$fs->folder_new($folder_prefix);
			
			$BookReportSavedName = "/u".$_SESSION['UserID']."/r".$BookReportID."/".$BookReportFileName;
			
			$Result['CopyFile'] = $fs->file_copy($BookReportFileLocation, $folder_prefix."/".$BookReportFileName);
			if($Result['CopyFile']){
				$sql = "UPDATE READING_GARDEN_BOOK_REPORT 
						SET Attachment = '".$this->Get_Safe_Sql_Query($BookReportSavedName)."',
							DateModified=NOW(), LastModifiedBy='".$_SESSION['UserID']."'  
						WHERE BookReportID = '".$BookReportID."' ";
				$Result['UpdateAttachment'] = $this->db_db_query($sql);
			}
			if(!in_array(false,$Result) && $AddStateScore){
				$this->Update_State_Score_Action(ACTION_SCORE_ADD_BOOK_REPORT,1,$_SESSION['UserID']);
			}
			
			return !in_array(false,$Result);
		}else{
			return false;
		}
	}
	
	function Delete_Book_Report_Attachment($BookReportID)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$fs = new libfilesystem();
		
		$Result=array();
		$sql = "SELECT 
					br.Attachment,
					br.AnswerSheet,
					br.OnlineWriting,
					rr.UserID 
				FROM 
					READING_GARDEN_BOOK_REPORT as br 
					INNER JOIN READING_GARDEN_READING_RECORD as rr ON rr.ReadingRecordID = br.ReadingRecordID 
				WHERE BookReportID = '".$BookReportID."' ";
		$temp = $this->returnArray($sql);
		$filename = trim($temp[0]['Attachment']);
		$student_id = $temp[0]['UserID'];
		$answer_sheet = trim($temp[0]['AnswerSheet']);
		$OnlineWriting = trim($temp[0]['OnlineWriting']);
		
		if($filename!=''){
			//$filepath = $intranet_root.$this->ModuleFilePath."/book_report/u".$student_id."/r".$BookReportID."/".$filename;
			$filepath = $intranet_root.$this->ModuleFilePath."/book_report".$filename;
			$Result['RemoveFile'] = $fs->file_remove($filepath);
			if($Result['RemoveFile']){
				if($answer_sheet=='' && $OnlineWriting == ''){ // no answer sheet done, remove the record
					$sql = "DELETE FROM READING_GARDEN_BOOK_REPORT WHERE BookReportID = '".$BookReportID."' ";
					$Result['ResetAttachment'] = $this->db_db_query($sql);
					if(!in_array(false,$Result)){
						$this->Update_State_Score_Action(ACTION_SCORE_ADD_BOOK_REPORT,-1,$_SESSION['UserID']);
					}
				}else{
					$sql = "UPDATE READING_GARDEN_BOOK_REPORT 
							SET Attachment = NULL, DateModified = NOW(), LastModifiedBy = '".$_SESSION['UserID']."' 
							WHERE BookReportID = '".$BookReportID."' ";
					$Result['ResetAttachment'] = $this->db_db_query($sql);
					// No need to add score twice since answer sheet already add 1
				}
			}
		}
		return !in_array(false,$Result);
 	}
	
	function Submit_Answer_To_Book_Report($BookReportID,$Answer,$ReadingRecordID='', $StudentSelectedAnswerSheetID='')
	{
		$BookReportID = trim($BookReportID);
		$Answer = $this->Get_Safe_Sql_Query(trim($Answer));
		$StudentSelectedAnswerSheetID = $StudentSelectedAnswerSheetID?"'$StudentSelectedAnswerSheetID'":"NULL";
		
		if($BookReportID==''){
			$sql = "INSERT INTO READING_GARDEN_BOOK_REPORT 
					(ReadingRecordID,AnswerSheet, StudentSelectedAnswerSheetID,RecordStatus,DateInput,DateModified,InputBy,LastModifiedBy)
					VALUES ('".$ReadingRecordID."','".$Answer."',".$StudentSelectedAnswerSheetID.",'".RECORD_STATUS_ACTIVE."',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
			$Success = $this->db_db_query($sql);
			if($Success){
				$this->Update_State_Score_Action(ACTION_SCORE_ADD_BOOK_REPORT,1,$_SESSION['UserID']);
			}
			return $Success;
		}else{
			$sql = "SELECT Attachment, AnswerSheet, OnlineWriting FROM READING_GARDEN_BOOK_REPORT WHERE BookReportID = '".$BookReportID."' ";
			$temp = $this->returnArray($sql);
			$attachment = trim($temp[0]['Attachment']);
			$OnlineWriting = trim($temp[0]['OnlineWriting']);
			
			if($OnlineWriting =='' && $attachment=='' && $Answer==''){// no attachment and going to delete answer,i.e. remove the record
				$sql = "DELETE FROM READING_GARDEN_BOOK_REPORT WHERE BookReportID = '".$BookReportID."' ";
				$Success = $this->db_db_query($sql);
				if($Success){
					$this->Update_State_Score_Action(ACTION_SCORE_ADD_BOOK_REPORT,-1,$_SESSION['UserID']);
				}
			}else{
				$sql = "UPDATE READING_GARDEN_BOOK_REPORT 
						SET AnswerSheet = '".$Answer."', 
							StudentSelectedAnswerSheetID = ".$StudentSelectedAnswerSheetID.", 
							DateModified = NOW(), 
							LastModifiedBy = '".$_SESSION['UserID']."' 
						WHERE BookReportID = '".$BookReportID."' ";
				$Success = $this->db_db_query($sql);
			}
			return $Success;
		}
	}

	function Submit_Writing_To_Book_Report($BookReportID,$OnlineWriting,$ReadingRecordID='')
	{
		$BookReportID = trim($BookReportID);
		$OnlineWriting = $this->Get_Safe_Sql_Query(trim($OnlineWriting));
		if($BookReportID==''){
			$sql = "INSERT INTO READING_GARDEN_BOOK_REPORT 
					(ReadingRecordID,OnlineWriting,RecordStatus,DateInput,DateModified,InputBy,LastModifiedBy)
					VALUES ('".$ReadingRecordID."','".$OnlineWriting."','".RECORD_STATUS_ACTIVE."',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
			$Success = $this->db_db_query($sql);
			if($Success){
				$this->Update_State_Score_Action(ACTION_SCORE_ADD_BOOK_REPORT,1,$_SESSION['UserID']);
			}
			return $Success;
		}else{
			$sql = "SELECT Attachment, AnswerSheet, OnlineWriting FROM READING_GARDEN_BOOK_REPORT WHERE BookReportID = '".$BookReportID."' ";
			$temp = $this->returnArray($sql);
			$attachment = trim($temp[0]['Attachment']);
			$AnswerSheet = trim($temp[0]['AnswerSheet']);
			if($attachment=='' && $AnswerSheet=='' && $OnlineWriting==''){// no attachment and going to delete answer,i.e. remove the record
				$sql = "DELETE FROM READING_GARDEN_BOOK_REPORT WHERE BookReportID = '".$BookReportID."' ";
				$Success = $this->db_db_query($sql);
				if($Success){
					$this->Update_State_Score_Action(ACTION_SCORE_ADD_BOOK_REPORT,-1,$_SESSION['UserID']);
				}
			}else{
				$sql = "UPDATE READING_GARDEN_BOOK_REPORT 
						SET OnlineWriting = '".$OnlineWriting."', 
							DateModified = NOW(), 
							LastModifiedBy = '".$_SESSION['UserID']."' 
						WHERE BookReportID = '".$BookReportID."' ";
				$Success = $this->db_db_query($sql);
			}
			
			return $Success;
		}
	}
	
	function Get_Answer_Sheet_From_Assigned_Reading_Or_ReadingRecord($AssignedReadingID,$ReadingRecordID)
	{
		$AssignedReadingID = trim($AssignedReadingID);
		$ReadingRecordID = trim($ReadingRecordID);
		$AnswerSheet = '';
		$Found = false;
		if($AssignedReadingID!=''){
			// Assigned Reading has higher priority
			$sql = "SELECT AnswerSheet FROM READING_GARDEN_STUDENT_ASSIGNED_READING WHERE AssignedReadingID = '".$AssignedReadingID."' ";
			$temp = $this->returnVector($sql);
			$AnswerSheet = trim($temp[0]);
			if($AnswerSheet!='')
				return $AnswerSheet;
		}else if($ReadingRecordID!=''){
			$sql = "SELECT ast.AnswerSheet 
					FROM READING_GARDEN_READING_RECORD as rr 
					INNER JOIN READING_GARDEN_BOOK as b ON b.BookID = rr.BookID AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
					INNER JOIN READING_GARDEN_ANSWER_SHEET_TEMPLATE ast ON b.DefaultAnswerSheetID = ast.TemplateID AND ast.RecordStatus = ".RECORD_STATUS_ACTIVE."
					WHERE rr.ReadingRecordID = '".$ReadingRecordID."' ";
			$temp = $this->returnVector($sql);
			$AnswerSheet = trim($temp[0]);		
			
//			$AcademicYearID = trim($AcademicYearID)==''?Get_Current_Academic_Year_ID():$AcademicYearID;
//			// Try to find assigned reading answer sheet 
//			$sql = "SELECT ar.AnswerSheet 
//					FROM READING_GARDEN_READING_RECORD as rr 
//					INNER JOIN READING_GARDEN_BOOK as b ON b.BookID = rr.BookID 
//					INNER JOIN READING_GARDEN_STUDENT_ASSIGNED_READING as ar ON ar.BookID = b.BookID AND ar.AcademicYearID = '".$AcademicYearID."' AND ar.RecordStatus = '".RECORD_STATUS_ACTIVE."' 
//					WHERE 
//						rr.ReadingRecordID = '".$ReadingRecordID."' 
//						AND ar.AnswerSheet IS NOT NULL AND ar.AnswerSheet <> '' ";
//			$temp = $this->returnVector($sql);
//			$AnswerSheet = trim($temp[0]);
//			if($AnswerSheet!="")
//				$Found = true;
		}
//		if(!$Found && $ReadingRecordID!=''){
//			$sql = "SELECT b.DefaultAnswerSheet 
//					FROM READING_GARDEN_READING_RECORD as rr 
//					INNER JOIN READING_GARDEN_BOOK as b ON b.BookID = rr.BookID 
//					WHERE rr.ReadingRecordID = '".$ReadingRecordID."' ";
//			$temp = $this->returnVector($sql);
//			$AnswerSheet = trim($temp[0]);
//		}
		
		return $AnswerSheet;
	}
	
	function Get_Student_Recommend_Book_Report_DBTable_Sql($Keyword='',$AcademicYearID='')
	{
		$AcademicYearID = trim($AcademicYearID)==''?Get_Current_Academic_Year_ID():$AcademicYearID;
		$NameField = getNameFieldWithClassNumberByLang('u.');
		
		$sql = "SELECT 
					CONCAT(b.BookName,IF(b.Author IS NOT NULL AND b.Author <> '',CONCAT('[', b.Author ,']'),'')) as BookInfo,
					br.Attachment as BookReport,
					$NameField as StudentName,
					br.BookReportID,
					rr.ReadingRecordID,
					b.BookID,
					u.UserID 
				FROM 
					READING_GARDEN_BOOK_REPORT as br 
					INNER JOIN READING_GARDEN_READING_RECORD as rr ON br.ReadingRecordID = rr.ReadingRecordID AND rr.RecordStatus = ".RECORD_STATUS_ACTIVE."  
					INNER JOIN READING_GARDEN_BOOK as b ON b.BookID = rr.BookID AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
					INNER JOIN INTRANET_USER as u ON u.UserID = rr.UserID AND u.RecordType = 2 AND u.RecordStatus = '".RECORD_STATUS_ACTIVE."' 
				WHERE 
					br.RecordStatus = '".RECORD_STATUS_ACTIVE."' 
					AND br.Recommend IS NOT NULL 
					AND br.Recommend = 1 
					AND (
							(br.Attachment IS NOT NULL AND br.Attachment <> '')
							OR (br.AnswerSheet IS NOT NULL AND br.AnswerSheet <> '') OR
						(br.OnlineWriting IS NOT NULL AND br.OnlineWriting <> '') 
						) ";
					
		$Keyword = trim($Keyword);
		if($Keyword!=""){
			$Keyword = $this->Get_Safe_Sql_Query($Keyword);
			$sql .= " AND (
							b.BookName LIKE '%".$Keyword."%'
							OR b.Author LIKE '%".$Keyword."%' 
							OR u.EnglishName LIKE '%".$Keyword."%' 
							OR u.ChineseName LIKE '%".$Keyword."%' 
							
						) ";
		}
		
		return $sql;
	}
	
	function Manage_Book_Report_Comment($Action,$BookReportID='',$UID='',$Comment='',$BookReportCommentID='', $RecordType='')
	{
		$Action = strtoupper($Action);
		$Result = array();
		$this->Start_Trans();
		
		 if($Action == 'SET'){
			// INSERT
			$sql = "INSERT INTO READING_GARDEN_BOOK_REPORT_COMMENT 
					(BookReportID,UserID,Comment,RecordStatus,
						DateInput,DateModified,InputBy,LastModifiedBy) 
					VALUES ('".$BookReportID."','".$UID."','".$this->Get_Safe_Sql_Query(trim($Comment))."','".RECORD_STATUS_ACTIVE."',
							NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
			$Result["InsertComment"] = $this->db_db_query($sql);
			
			if($this->isStudent) //add state score for student
			{
				$Result["AddScoringAction"] = $this->Update_State_Score_Action(ACTION_SCORE_COMMENT,1,$_SESSION['UserID']);
//				$Result["AddCommentLengthScoringAction"] = $this->Update_State_Score_Action(ACTION_SCORE_COMMENT_LENGTH,'',$_SESSION['UserID']);
			}
			
		}else if($Action == 'DEL'){
			$sql = "UPDATE 
						READING_GARDEN_BOOK_REPORT_COMMENT 
					SET
						RecordStatus = ".RECORD_STATUS_DELETED.",
						LastModifiedBy = ".$_SESSION['UserID'].",
						DateModified = NOW()
					WHERE 
						BookReportCommentID = '$BookReportCommentID'";
						
			$Result["DeleteComment"] = $this->db_db_query($sql);
			
			if($RecordType==2) // give penalty to student
			{
				$Result["AddPenalty"] = $this->Update_State_Score_Action(ACTION_PENALTY_COMMENT_DELETED,1,$UID);
			}
			
		}
		
		if(in_array(false,$Result))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	/***********************************Carlos Part***********************************/
	
	/***********************************Marcus Part Start***********************************/
	function Load_Settings()
	{
		$sql = "
			SELECT
				SettingName,
				SettingValue
			FROM
				READING_GARDEN_SETTING
		";
		
		$Result = $this->returnArray($sql);
		
		foreach((array)$Result as $SettingNameValues)
			$this->Settings[$SettingNameValues[0]] = $SettingNameValues[1];
		
		// assign default value
		if(!isset($this->Settings['FileUpload']))
			$this->Settings['FileUpload'] = 1;
	}
	
	function Settings_Save_Scoring_Rule_Settings($ScoringSettingsArr)
	{
		$Success = $this->Settings_Update_Settings($ScoringSettingsArr);
		return $Success;
	}
		
	function Settings_Update_Settings($SettingsNameValueArr)
	{
		if(count($SettingsNameValueArr)==0 || !is_array($SettingsNameValueArr)) return false;
		
		$this->Start_Trans();
		
		$SettingsNameArr = array_keys($SettingsNameValueArr);
		
		$Success['Remove'] = $this->Settings_Remove_Settings($SettingsNameArr);
		$Success['Add'] = $this->Settings_Add_Settings($SettingsNameValueArr);
		
		if(in_array(false, $Success))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
		
	}
 
	function Settings_Remove_Settings($SettingsNameArr)
	{
		if(count($SettingsNameArr)==0) return false;
		
		$SettingsNameSql = "'".implode("','",(array)$SettingsNameArr)."'";
		
		$sql = "
			DELETE FROM
				READING_GARDEN_SETTING	 
			WHERE
				SettingName IN (".$SettingsNameSql.")
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;
	}

	function Settings_Add_Settings($SettingsNameValueArr)
	{
		if(count($SettingsNameValueArr)==0 || !is_array($SettingsNameValueArr)) return false;
		
		foreach((array)$SettingsNameValueArr as $SettingName => $SettingValue)
			$InsertSqlArr[] = "('$SettingName','$SettingValue', ".$_SESSION['UserID'].", NOW())";
			
		$InsertSql = implode(',',$InsertSqlArr);	
		
		$sql = "
			INSERT INTO	READING_GARDEN_SETTING
				(SettingName, SettingValue, LastModifiedBy, DateModified)	 
			VALUES
				$InsertSql
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;		
	}
	
	function Settings_Save_Class_Book_Source_Settings($ClassBookSourceArr)
	{
		if(!is_array($ClassBookSourceArr) || count($ClassBookSourceArr)==0) return false;
		
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
		foreach((array)$ClassBookSourceArr as $ClassID => $BookSource)
		{
			$sqlArr[] = " ($ClassID,$BookSource,$AcademicYearID,".$_SESSION['UserID'].",NOW()) ";
		}
		$sqlStr = implode(",",$sqlArr);
		
		$sql = "
			REPLACE INTO READING_GARDEN_CLASS_BOOK_SOURCE_SETTING
				(ClassID, BookSource,AcademicYearID, LastModifiedBy, DateModified)
			VALUES
				$sqlStr	
		";
		
		$Success = $this->db_db_query($sql);

		return $Success;
	}
	
	function Settings_Get_Class_Book_Source($ClassID='', $AcademicYearID='')
	{
		if(trim($ClassID)!='')
			$cond_ClassID = " AND ClassID = '$ClassID' ";
		
		if(trim($AcademicYearID)=='')
			$AcademicYearID = Get_Current_Academic_Year_ID();
			
		$sql = "
			SELECT
				*
			FROM
				READING_GARDEN_CLASS_BOOK_SOURCE_SETTING
			WHERE
				AcademicYearID = '$AcademicYearID'
				$cond_ClassID
		";	
		
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Check_Category_Code($CategoryCode, $Language, $CategoryID='')
	{
		if(trim($CategoryID)!='')
			$cond_Code = " AND CategoryID <> '$CategoryID' ";
			
		$sql = "
			SELECT
				1
			FROM
				READING_GARDEN_BOOK_CATEGORY
			WHERE
				CategoryCode = '".$this->Get_Safe_Sql_Query(trim($CategoryCode))."'
				AND Language = '$Language'
				AND RecordStatus = ".RECORD_STATUS_ACTIVE."
				$cond_Code
		";	
		
		$Exist = $this->returnVector($sql);
		return $Exist[0]?1:0;
	}
	 
	function Settings_Update_Category($CategoryInfo, $CategoryID)
	{
		$CategoryName = trim($CategoryInfo['CategoryName']);
		$CategoryCode = trim($CategoryInfo['CategoryCode']);
		
		$sql = "
			UPDATE
				READING_GARDEN_BOOK_CATEGORY
			SET
				CategoryName = '".$this->Get_Safe_Sql_Query($CategoryName)."',
				CategoryCode = '".$this->Get_Safe_Sql_Query($CategoryCode)."',
				Language = '".$CategoryInfo['Language']."',
				LastModifiedBy = '".$_SESSION['UserID']."',
				DateModified = NOW()
			WHERE
				CategoryID = '$CategoryID'
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;
	}
	
	function Settings_Delete_Category($CategoryID)
	{
		$sql = "
			UPDATE 
				READING_GARDEN_BOOK_CATEGORY
			SET
				RecordStatus = ".RECORD_STATUS_DELETED.",
				LastModifiedBy = '".$_SESSION['UserID']."',
				DateModified = NOW()
			WHERE 
				CategoryID = $CategoryID
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;		 
	}

	function Settings_Insert_Category($CategoryInfo)
	{
		$MaxOrder = $this->Settings_Get_Max_Category_Display_Order();
		
		$CategoryName = trim($CategoryInfo['CategoryName']);
		$CategoryCode = trim($CategoryInfo['CategoryCode']);
		
		$sql = "
			INSERT INTO	READING_GARDEN_BOOK_CATEGORY
				(CategoryName, CategoryCode, Language, DisplayOrder, InputBy, DateInput, LastModifiedBy, DateModified)
			VALUES
				(
					'".$this->Get_Safe_Sql_Query($CategoryName)."',
					'".$this->Get_Safe_Sql_Query($CategoryCode)."',
					'".$CategoryInfo['Language']."',
					".($MaxOrder+1).",
					".$_SESSION['UserID'].",
					NOW(),
					".$_SESSION['UserID'].",
					NOW()
				)
		";
		
		$Success = $this->db_db_query($sql);
		if ($Success)
		{
			$Success = $this->db_insert_id();
		}
		
		return $Success;
	}
	
	function Settings_Get_Category_Info($CategoryID = '', $Language = '')
	{
		if($CategoryID!=='' && $CategoryID==0)
			$cond_CategoryID = " AND bc.CategoryID IS NULL ";
		else if($CategoryID){
			$cond_CategoryID = " AND bc.CategoryID IN (".implode(",",(array)$CategoryID).") ";
		}
		if(trim($Language)!= '')
			$cond_Language = " AND Language = $Language ";
			
		$sql = "
			SELECT
				*
			FROM
				READING_GARDEN_BOOK_CATEGORY bc
			WHERE
				1
				$cond_CategoryID
				$cond_Language
				AND RecordStatus = ".RECORD_STATUS_ACTIVE."
			ORDER BY
				DisplayOrder
		";	
		
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
//	function Settings_Get_Category_Book_Number($CategoryID='', $Language = '')
//	{
//		if(trim($CategoryID)!= '')
//			$cond_CategoryID = " AND CategoryID = $CategoryID ";
//		if(trim($Language)!= '')
//			$cond_Language = " AND Language = $Language ";
//			
//		$sql = "
//			SELECT
//				bc.*,
//				Count(b.BookID) NumOfBook
//			FROM
//				READING_GARDEN_BOOK_CATEGORY bc
//				LEFT JOIN READING_GARDEN_BOOK b ON 
//					b.CategoryID = bc.CategoryID 
//					AND b.RecordStatus = ".RECORD_STATUS_ACTIVE." 
//					AND (b.ByStudent = '' OR b.ByStudent IS NULL)
//			WHERE
//				1
//				$cond_CategoryID
//				$cond_Language
//				AND bc.RecordStatus = ".RECORD_STATUS_ACTIVE."
//			GROUP BY 
//				bc.CategoryID
//			ORDER BY
//				bc.DisplayOrder
//		";	
//		debug_pr($sql);
//		$Result = $this->returnArray($sql);
//		
//		return $Result;
//	}
	
	function Settings_Get_Max_Category_Display_Order()
	{
		$sql = "SELECT Max(DisplayOrder) FROM READING_GARDEN_BOOK_CATEGORY";
		
		$Result = $this->returnVector($sql);
		
		return $Result[0];
	}
	
	function Settings_Update_Category_Display_Order($CategoryIDArr)
	{
		$this->Start_Trans();
		$i = 1;
		foreach((array)$CategoryIDArr as $CategoryID)
		{
			$sql = "
				UPDATE
					READING_GARDEN_BOOK_CATEGORY
				SET
					DisplayOrder = $i,
					LastModifiedBy = '".$_SESSION['UserID']."',
					DateModified = NOW()
				WHERE
					CategoryID = '$CategoryID'
			";
			
			$Success[] = $this->db_db_query($sql);
			$i++;
		}
		
		if(!in_array(false,$Success))
		{
			$this->Commit_Trans();
			return true;
		}
		else
		{
			$this->RollBack_Trans();
			return false;
		}
	}
	
	//20101220
	function Get_Recommend_Book_List_DBTable_Sql($YearID='', $Keyword='',$CategoryID='')
	{
		global $image_path,$LAYOUT_SKIN, $Lang;
		
		if(trim($YearID)!='')
			$cond_YearID = " AND rbm.YearID = '$YearID' ";

		if($CategoryID!=='' && $CategoryID==0)
			$cond_CategoryID = " AND bc.CategoryID IS NULL ";
		else if(trim($CategoryID)!='')
			$cond_CategoryID = " AND bc.CategoryID = '$CategoryID' ";

		if(trim($Keyword)!='')
		{
			$Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
			$cond_Keyword = "
				AND
				(
					b.BookName LIKE '%".$Keyword."%'
					OR b.CallNumber LIKE '%".$Keyword."%'
					OR b.Author LIKE '%".$Keyword."%'
					OR b.Publisher LIKE '%".$Keyword."%'
					OR b.Description LIKE '%".$Keyword."%'
					OR bc.CategoryName LIKE '%".$Keyword."%'
					OR bc.CategoryCode LIKE '%".$Keyword."%'
					OR CONCAT(bc.CategoryCode,' ',bc.CategoryName) LIKE '%".$Keyword."%'
				)
			";
		}

		$AcademicYearID = Get_Current_Academic_Year_ID();	

		$sql = "
			SELECT
				CONCAT(
					'<img class=\"preload_image\" src=\"".$image_path."/".$LAYOUT_SKIN."/icon_pic.gif\" path=\"',
					IF(
						b.BookCoverImage IS NOT NULL AND b.BookCoverImage <> '',
						CONCAT('".$this->BookCoverImagePath."/',b.BookCoverImage),
						'".$image_path."/".$LAYOUT_SKIN."/reading_scheme/temp_book_cover.gif'
					),
					'\" width=\"14px\" height=\"14px\" align=\"absmiddle\" valign=\"2\" style=\"float:left\">&nbsp;',
					b.BookName,
					CONCAT('&nbsp;<img src=\"".$image_path."/".$LAYOUT_SKIN."/reading_scheme/icon_book.gif\" width=\"14px\" height=\"14px\" align=\"absmiddle\" valign=\"2\" onclick=\"js_View_Book_Detail(',b.BookID,');\" title=\"".$Lang['ReadingGarden']['ViewBookDetail']."\" />')
				) BookName,
				/*IF(
					b.BookCoverImage IS NOT NULL AND b.BookCoverImage <> '',
					CONCAT('<img src=\"".$this->BookCoverImagePath."/',b.BookCoverImage,'\" width=\"100px\" height=\"120px\">'),
					CONCAT('<img src=\"".$image_path."/".$LAYOUT_SKIN."/reading_scheme/temp_book_cover.gif\" width=\"100px\" height=\"120px\">') 
				)as Cover,*/				
				b.CallNumber,				
				b.Author,
				b.Publisher,
				CONCAT(bc.CategoryCode,' ',bc.CategoryName) CategoryName,
				rbm.DateInput,
				CONCAT('<input type=\"checkbox\" name=\"RecommendBookID[]\" value=\"',rbm.RecommendBookID,'\">') as CheckBox
			FROM
				READING_GARDEN_FORM_RECOMMEND_BOOK_MAPPING rbm
				INNER JOIN READING_GARDEN_BOOK b ON rbm.BookID = b.BookID AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK_CATEGORY bc ON bc.CategoryID = b.CategoryID AND bc.RecordStatus = ".RECORD_STATUS_ACTIVE."
			WHERE
				1
				$cond_YearID
				/*AND rbm.AcademicYearID = $AcademicYearID*/
				$cond_CategoryID
				$cond_Keyword
		";	
		
		return $sql;
	}
	
	function Get_Class_Assign_Reading_Target($ClassIDArr='',$AssignedReadingID='', $AcademicYearID = '', $BookID = '')
	{
		$OrderBy = " b.BookName ASC ";
		
//		if(trim($ClassIDArr)!='')
		if($ClassIDArr)
		{
			$cond_YearClassID = " AND art.ClassID IN (".implode(",",(array)$ClassIDArr).") ";
			if(!is_array($ClassIDArr))
			{
				$OrderBy = " art.DisplayOrder ASC ";
			}
		}

		if($AssignedReadingID)
			$cond_AssignedReadingID = " AND art.AssignedReadingID IN (".implode(",",(array)$AssignedReadingID).") ";
		
		if(trim($AcademicYearID)=='')
			$AcademicYearID = Get_Current_Academic_Year_ID();
		
		if($BookID)
			$cond_BookID = " AND ar.BookID IN (".implode(",",(array)$BookID).") ";
						
		$sql = "
			SELECT
				art.AssignedReadingTargetID, 
				art.ClassID, 
				ar.AssignedReadingID,
				ar.BookID,
				ar.Attachment,
				ar.AnswerSheet,
				ar.Description
			FROM
				READING_GARDEN_STUDENT_ASSIGNED_READING_TARGET art
				INNER JOIN READING_GARDEN_STUDENT_ASSIGNED_READING ar ON 
					ar.AssignedReadingID = art.AssignedReadingID 
					AND ar.AcademicYearID = '$AcademicYearID'					
					AND ar.RecordStatus = ".RECORD_STATUS_ACTIVE." 
				INNER JOIN READING_GARDEN_BOOK b ON 
					b.BookID = ar.BookID 
					AND b.RecordStatus = ".RECORD_STATUS_ACTIVE." 
			WHERE
				1
				$cond_YearClassID
				$cond_AssignedReadingID
				$cond_BookID
			GROUP BY
				art.ClassID, ar.AssignedReadingID
			ORDER BY
				$OrderBy
		";

		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Get_Available_Class_A_Book_Can_Be_Assigned_To($BookID)
	{
		global $PATH_WRT_ROOT;
		
		# Get Recommend Book Mappings
		$ParArr['BookID'] = $BookID;
		$RecommendBookMapping = $this->Get_Recommend_Book_Mappings($ParArr);
		$AvailableYear = Get_Array_By_Key($RecommendBookMapping, "YearID");
		
//		if($isMultiple)
//			$other = " multiple size=10 "; 
//		
//		if($noFirst)
//			$DisplaySelect = 2;
//		else if($isAll)
//			$optionFirst = $Lang['SysMgr']['FormClassMapping']['All']['Class'];
//		else
//			$optionFirst = $Lang['SysMgr']['FormClassMapping']['Select']['Class'];
//		
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		$fcm = new form_class_manage();
		$AcademicYearID = Get_Current_Academic_Year_ID();
		$classArr = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, $AvailableYear, 0);
		$AvailableClass = Get_Array_By_Key($classArr,"YearClassID");
		
		# Get classes that this book has been assigned to
		$AssignedClassArr = $this->Get_Class_Assign_Reading_Target('','','',$BookID);
		$AssignedClassIDArr = Get_Array_By_Key($AssignedClassArr, "ClassID");
		$AvailableClass = array_diff((array)$AvailableClass,(array)$AssignedClassIDArr);
		
		return $AvailableClass;
	}
	
	function Get_Assign_Reading_Info($AssignedReadingID='', $OrderBy='', $AcademicYearID='',$Keyword='')
	{
//		if(trim($AssignedReadingID)!='')
		if($AssignedReadingID)
			$cond_AssignedReadingID= " AND ar.AssignedReadingID IN (".implode(",",(array)$AssignedReadingID).") ";
		
		if(trim($AcademicYearID)=='')
			$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$YearClassName = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEn");
		
		if(trim($OrderBy)=='')
			$OrderBy = "b.BookName ASC";
			
		if(trim($Keyword)!='')
		{
			$Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
			$cond_Keyword = " 
				AND
				(
					b.BookName LIKE '%".$Keyword."%'
					OR ar.Description LIKE '%".$Keyword."%'  
				)
			";
		}	
			
		$sql = "
			SELECT 
				ar.*,
				b.BookID,
				b.BookName,
				b.Author,
				art.ClassID,
				art.DisplayOrder,
				GROUP_CONCAT( $YearClassName ORDER BY y.Sequence, yc.Sequence ASC SEPARATOR ', ') as AssignedClass
			FROM 
				READING_GARDEN_STUDENT_ASSIGNED_READING ar
				INNER JOIN READING_GARDEN_BOOK b ON ar.BookID = b.BookID AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_STUDENT_ASSIGNED_READING_TARGET art ON ar.AssignedReadingID = art.AssignedReadingID
				LEFT JOIN YEAR_CLASS yc ON yc.YearClassID = art.ClassID
				LEFT JOIN YEAR y ON yc.YearID = y.YearID  
			WHERE
				1
				$cond_AssignedReadingID
				AND ar.RecordStatus = ".RECORD_STATUS_ACTIVE."
				AND ar.AcademicYearID = '$AcademicYearID'	
				$cond_Keyword
			GROUP BY
				ar.AssignedReadingID
			ORDER BY 	
				$OrderBy		 

		";
		
		$Result = $this->returnArray($sql);
		
		return $Result;
		
	}
	
	// hide assign reading attached function 
	function Settings_Assign_Reading_Upload_Attachment($AssignedReadingID, $Attachment, $Attachment_name, $TempPath, $old_Attachment)
	{
		if(empty($TempPath)) return false;
		
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$fs = new libfilesystem();
		
		# Create folder if not exist
		$path = $intranet_root.$this->AssignReadingAttachmentPath;
		$tmp_path = $path."/$TempPath";
		$fs->folder_new($tmp_path);
		
		# retrieve list
		$fs->folderlist($tmp_path);
		$ExistFile = $fs->rs;
			
		for($i=0; $i<sizeof($Attachment);$i++)
		{
			$UploadFilePath =  $tmp_path."/".stripslashes($Attachment_name[$i]);
			$UploadFilePathArr[] = $UploadFilePath;
			$Result['CopyFile'][] = $fs->file_copy($Attachment[$i], $UploadFilePath);
		}
		
		for($i=0 ;$i<sizeof($old_Attachment);$i++)
		{
			$UploadFilePath =  $tmp_path."/".stripslashes($old_Attachment[$i]);
			$UploadFilePathArr[] = $UploadFilePath;
		}

		$RemoveFile = array_diff((array)$ExistFile,(array)$UploadFilePathArr);
		$RemoveFile = array_values((array)$RemoveFile);
		
		for($i=0; $i<sizeof($RemoveFile);$i++)
		{
			$Result['CopyFile'][] = $fs->item_remove($RemoveFile[$i]);
		}
		
		if(!in_array(false,(array)$Result['CopyFile']))
			return $TempPath;
		else
		{
//			$fs->folder_remove_recursive($tmp_path);
			return false;
		}
		
	}
	
	function Settings_Create_Assign_Reading($AssignedReadingInfo)
	{
		
		$AssignedReadingName = trim($AssignedReadingInfo['AssignedReadingName']);
		$Description = trim($AssignedReadingInfo['Description']);
		$OnlineWritingDesc = trim($AssignedReadingInfo['OnlineWritingDesc']);
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$sql = "
			INSERT INTO READING_GARDEN_STUDENT_ASSIGNED_READING
				(BookID, Description, AssignedReadingName, Attachment, AnswerSheet, BookReportRequired, WritingLimit, LowerLimit, OnlineWritingDesc, AcademicYearID, RecordStatus, DateInput, DateModified, InputBy, LastModifiedBy)
			VALUES
				(
					".$AssignedReadingInfo['BookID'].",
					'".$this->Get_Safe_Sql_Query($Description)."',
					'".$this->Get_Safe_Sql_Query($AssignedReadingName)."',
					'".$AssignedReadingInfo['Attachment']."',
					'".$this->Get_Safe_Sql_Query($AssignedReadingInfo['AnswerSheet'])."',
					'".$AssignedReadingInfo['BookReportRequired']."',
					'".$AssignedReadingInfo['WritingLimit']."',
					'".$AssignedReadingInfo['LowerLimit']."',
					'".$this->Get_Safe_Sql_Query($OnlineWritingDesc)."',
					'$AcademicYearID',
					".RECORD_STATUS_ACTIVE.",
					NOW(),
					NOW(),
					".$_SESSION['UserID'].",
					".$_SESSION['UserID']."
				)
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;
	}
	
	function Settings_Update_Assign_Reading($AssignedReadingID, $AssignedReadingInfo)
	{
		
		if(empty($AssignedReadingID) || empty($AssignedReadingInfo)) return false;
		
		foreach((array)$AssignedReadingInfo as $Key => $Val)
		{
			if(trim($Val)!=='')
				$UpdateSqlArr[] = " $Key = '".$this->Get_Safe_Sql_Query($Val)."' ";
			else
				$UpdateSqlArr[] = " $Key = NULL ";
		}
		$UpdateSql = implode(",",$UpdateSqlArr);
		
		$sql = "
			UPDATE 
				READING_GARDEN_STUDENT_ASSIGNED_READING
			SET
				$UpdateSql,
				DateModified = NOW(),
				LastModifiedBy = ".$_SESSION['UserID']."
			WHERE
				AssignedReadingID IN (".implode(",",(array)$AssignedReadingID).")
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;
		
	}
	
	function Settings_Delete_Assign_Reading($AssignedReadingID)
	{
		$Par['RecordStatus'] = RECORD_STATUS_DELETED;
		$Success = $this->Settings_Update_Assign_Reading($AssignedReadingID,$Par);
		
		return $Success;
	}
	
	function Settings_Assign_Reading_To_Class($ClassAssignReading)
	{
		$ClassIDArr = $ClassAssignReading['ClassID'];
		$AssignedReadingIDArr = $ClassAssignReading['AssignedReadingID'];
		
		if(empty($ClassIDArr) || empty($AssignedReadingIDArr))	return false;
		
//		$ExistingMapping = $this->Get_Class_Assign_Reading_Target($ClassIDArr, $AssignedReadingIDArr);
//		$ExistingMappingAssoc = BuildMultiKeyAssoc($ExistingMapping, array("ClassID","AssignedReadingID"),"AssignedReadingTargetID",1);
		
		$DisplayOrder = $this->Settings_Get_Class_Max_Assign_Reading_Display_Order();
		
		foreach((array)$ClassIDArr as $ClassID)
		{
			foreach((array)$AssignedReadingIDArr as $AssignedReadingID)
			{
//				if($ExistingMappingAssoc[$ClassID][$AssignedReadingID]) continue;// if mapping already exist, skip insert
				$thisDisplayOrder = ++$DisplayOrder[$ClassID];
				$ValueArr[] = "
					(
						$AssignedReadingID,
						$ClassID,
						$thisDisplayOrder,
						NOW(),
						NOW(),
						".$_SESSION['UserID'].",
						".$_SESSION['UserID']."
					)
				";
			}
			
		}
		if(count($ValueArr)==0) return true;
		
		$ValueSql = implode(",",$ValueArr);
		
		$sql = "
			INSERT INTO READING_GARDEN_STUDENT_ASSIGNED_READING_TARGET
				(AssignedReadingID, ClassID, DisplayOrder, DateInput, DateModified, InputBy, LastModifiedBy)
			VALUES
				$ValueSql
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;

	}
	
	function Settings_Get_Class_Max_Assign_Reading_Display_Order()
	{
		$sql = "SELECT ClassID, Max(DisplayOrder) FROM READING_GARDEN_STUDENT_ASSIGNED_READING_TARGET GROUP BY ClassID";
		
		$Result = $this->returnArray($sql);
		$ReturnArr = build_assoc_array($Result);
		
		return $ReturnArr;
	}
	
	function Settings_Remove_Assign_Reading_From_Class($AssignedReadingTargetID='', $AssignedReadingID='', $ClassID='')
	{
		if(!$AssignedReadingID && !$AssignedReadingTargetID)
			return false;
		
		if($AssignedReadingTargetID)
			$cond_AssginedReadingTargetID = " AND AssignedReadingTargetID IN (".implode(",",(array)$AssignedReadingTargetID).") ";
		if($AssignedReadingID)
			$cond_AssignedReadingID = " AND AssignedReadingID IN (".implode(",",(array)$AssignedReadingID).") ";
		if($ClassID)
			$cond_ClassID = " AND ClassID IN (".implode(",",(array)$ClassID).") ";
		$sql = "
			DELETE FROM
				READING_GARDEN_STUDENT_ASSIGNED_READING_TARGET
			WHERE
				1
				$cond_AssginedReadingTargetID
				$cond_AssignedReadingID
				$cond_ClassID
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;
	}
	
	function Settings_Update_Class_Assign_Reading($AssignedReadingID,$YearClassIDArr)
	{
		$ClassAssignedReadingMapping = $this->Get_Class_Assign_Reading_Target('',$AssignedReadingID);
		$ClassList = Get_Array_By_Key($ClassAssignedReadingMapping,"ClassID");
		
		$DeleteClassList = array_diff((array)$ClassList,(array)$YearClassIDArr);
		$AddClassList = array_diff((array)$YearClassIDArr,(array)$ClassList);
		
		if($DeleteClassList)
			$Success['Delete'] = $this->Settings_Remove_Assign_Reading_From_Class('',$AssignedReadingID, $DeleteClassList);
		
		if($AddClassList)
		{
			$ParArr['AssignedReadingID'] = $AssignedReadingID;
			$ParArr['ClassID'] = $AddClassList;
			$Success["Assign"] = $this->Settings_Assign_Reading_To_Class($ParArr);
		}
		
		return !in_array(false,(array)$Success);
	}
	
	function Settings_Update_Assign_Reading_Display_Order($AssignedReadingIDArr, $ClassID)
	{
		$this->Start_Trans();
		$i = 1;
		foreach((array)$AssignedReadingIDArr as $AssignedReadingID)
		{
			$sql = "
				UPDATE
					READING_GARDEN_STUDENT_ASSIGNED_READING_TARGET
				SET
					DisplayOrder = $i,
					LastModifiedBy = '".$_SESSION['UserID']."',
					DateModified = NOW()
				WHERE
					ClassID = '$ClassID'	
					AND AssignedReadingID = '$AssignedReadingID'
			";
			
			$Success[] = $this->db_db_query($sql);
			$i++;
		}
		
		if(!in_array(false,$Success))
		{
			$this->Commit_Trans();
			return true;
		}
		else
		{
			$this->RollBack_Trans();
			return false;
		}		
	}
	
	# Retrieve assignement only if its book was recommended to the given class(es) 
	function Settings_Get_Class_Available_Assign_Reading($YearClassIDArr)
	{
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
		include_once("form_class_manage.php");
		$year = new year();
		$YearClassList = $year->Get_All_Classes();
		$ClassYearAssoc = BuildMultiKeyAssoc($YearClassList, "YearClassID","YearID",1);
		$YearIDArr = array();
		foreach((array)$YearClassIDArr as $YearClassID)
		{
			$YearID = $ClassYearAssoc[$YearClassID];
			if(!in_array($YearID,(array)$YearIDArr))
				$YearIDArr[] = $YearID;
		}	
		$NumOfYear = sizeof($YearIDArr);
		
		$sql = "
			SELECT
				ar.*
			FROM
				READING_GARDEN_STUDENT_ASSIGNED_READING ar
				INNER JOIN READING_GARDEN_FORM_RECOMMEND_BOOK_MAPPING rbm ON ar.BookID = rbm.BookID 
				INNER JOIN READING_GARDEN_BOOK b ON rbm.BookID = b.BookID AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
			WHERE
				ar.RecordStatus = ".RECORD_STATUS_ACTIVE."
				AND ar.AcademicYearID = '$AcademicYearID'
				AND rbm.YearID IN (".(implode(",",$YearIDArr)).")
			GROUP BY 
				ar.AssignedReadingID
			HAVING
				COUNT(ar.AssignedReadingID) = $NumOfYear
		";
		
		
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Get_Assign_Reading_Attachment($AttPath)
	{
		global $intranet_root;
		
		include_once("libfilesystem.php");
		$fs = new libfilesystem();
		
		$path = $intranet_root.$this->AssignReadingAttachmentPath;
		$tmp_path = $path."/$AttPath";
		
		# retrieve list
		$fs->folderlist($tmp_path);
		$ExistFile = $fs->rs;
		
		return $ExistFile;
	}
	
	
//	function Get_Assigned_Reading_Management_DBTable_Sql($Keyword='')
//	{
//		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $Lang;
//		
////		if(trim($ClassID)!='')
////			$YearClassIDArr = $ClassID;
////		else if(trim($YearID)!='')
////		{
////			include_once($PATH_WRT_ROOT."/includes/form_class_manage.php");
////			$YearLib = new year($YearID);
////			$YearClassList =  $YearLib->Get_All_Classes();
////			$YearClassIDArr = Get_Array_By_Key($YearClassList,"YearClassID");
////		}
////		
////		if(!empty($YearClassIDArr))
////			$cond_ClassID = " AND art.ClassID IN (".implode(",",(array)$YearClassIDArr).") ";
////			
////		$ClassTitle = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
//
//		if(trim($Keyword)!='')
//		{
//			$Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
//			$cond_Keyword = "
//				AND
//				(
//					b.BookName LIKE '%".$Keyword."%'
//					OR ar.Description LIKE '%".$Keyword."%'
//				)
//			";
//		}
//
//		$AcademicYearID = Get_Current_Academic_Year_ID();	
//		$ViewIcon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
//		$sql = "
//			SELECT
//				CONCAT('<a title=\'".$Lang['Btn']['Edit']."\' class=\'edit thickbox\' href=\'#TB_inline?height=600&width=750&inlineId=FakeLayer\' onclick=\'js_Update_Assign_Reading(',ar.AssignedReadingID,')\' >', ar.AssignedReadingName,'</a>') ,
//				ar.Description,
//				b.BookName,
//				IF(
//					TRIM(ar.AnswerSheet) <> '',
//					CONCAT('<a href=\"javascript:void(0);\" onclick=\"js_Preview_AnswerSheet(\'',ar.AnswerSheet,'\')\">','$ViewIcon','</a>'),
//					''				
//				) AS ViewIcon,
//				ar.DateModified,
//				CONCAT('<input type=\"checkbox\" name=\"AssignedReadingID[]\" value=\"',ar.AssignedReadingID,'\">') as CheckBox
//			FROM 
//				READING_GARDEN_STUDENT_ASSIGNED_READING ar
//				INNER JOIN READING_GARDEN_BOOK b ON b.BookID = ar.BookID 
//					AND b.RecordStatus = ".RECORD_STATUS_ACTIVE." 
//				LEFT JOIN READING_GARDEN_STUDENT_ASSIGNED_READING_TARGET art ON ar.AssignedReadingID = art.AssignedReadingID
//				INNER JOIN YEAR_CLASS yc ON yc.YearClassID = art.ClassID
//			WHERE 
//				ar.RecordStatus = ".RECORD_STATUS_ACTIVE."
//				AND ar.AcademicYearID = $AcademicYearID 
//				$cond_Keyword
//			GROUP BY 
//				ar.AssignedReadingID
//		";	
//		
//		return $sql;
//	}
	
	function Get_State_Info($StateID='', $Score='')
	{
		if(trim($StateID!=''))
			$cond_StateID = " AND StateID = '$StateID' "; 
		else if(trim($Score!==''))
		{
			if($Score<=0)
				$cond_ScoreRequired = " AND ScoreRequired = 0 ";
			else
				$cond_ScoreRequired = " AND ScoreRequired <= $Score ";
			$Limit = " LIMIT 1";
		}
		
		$sql = "
			SELECT 
				*
			FROM
				READING_GARDEN_STATE
			WHERE
				1
				$cond_StateID
				$cond_ScoreRequired
				AND RecordStatus = ".RECORD_STATUS_ACTIVE."
			ORDER BY
				ScoreRequired DESC
			$Limit
		";
		
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Settings_Upload_State_Logo($ImagePath, $ImagePath_name)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$fs = new libfilesystem();
		
		if(!exif_imagetype($ImagePath))
			return false;
		
		$ImagePath_name = stripslashes($ImagePath_name);
		
		# Create folder if not exist
		$tmp_path = $intranet_root.$this->StateLogoPath;
		
		$fs->folder_new($tmp_path);
		
		# retrieve list
		$fs->folderlist($tmp_path);
		$ExistFile = $fs->rs;
		
		$UploadFilePath =  $tmp_path."/".$ImagePath_name;
		$i=2;
		while(in_array($UploadFilePath, (array)$ExistFile))
		{
			$tmp_name = build_duplicate_file_name($ImagePath_name,$i);
			$UploadFilePath =  $tmp_path."/".$tmp_name;
			$i++;
		}
		
		$this->Image_Resize($ImagePath, 100, 120);
		
		$Result = $fs->file_copy($ImagePath, $UploadFilePath);
		
		if($Result)
			return $fs->get_file_basename($UploadFilePath);
		else
			return false;
	}
	
	function Settings_Delete_State_Logo($ImagePath)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$fs = new libfilesystem();
		
		$FilePath =  $intranet_root.$this->StateLogoPath."/".$ImagePath;
		
		$Success = $fs->file_remove($FilePath);
		return $Success;
	}
	
	function Settings_Update_State_Info($StateID, $StateInfo)
	{
		if(empty($StateID) || empty($StateInfo)) return false;
		
		foreach((array)$StateInfo as $Key => $Val)
		{
			if(trim($Val)!=='')
				$UpdateSqlArr[] = " $Key = '".$this->Get_Safe_Sql_Query($Val)."' ";
			else
				$UpdateSqlArr[] = " $Key = NULL ";
		}
		$UpdateSql = implode(",",$UpdateSqlArr);
		
		$sql = "
			UPDATE 
				READING_GARDEN_STATE
			SET
				$UpdateSql,
				DateModified = NOW(),
				LastModifiedBy = ".$_SESSION['UserID']."
			WHERE
				StateID IN (".implode(",",(array)$StateID).")
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;
	}
	
	function Settings_Insert_State_Info($StateInfo)
	{
		$StateName = trim($this->Get_Safe_Sql_Query($StateInfo['StateName']));
		$Description = trim($this->Get_Safe_Sql_Query($StateInfo['Description']));
		$ImagePath = trim($this->Get_Safe_Sql_Query($StateInfo['ImagePath']));
		
		$sql = "
			INSERT INTO	READING_GARDEN_STATE
				(StateName, Description, ScoreRequired, ImagePath, InputBy, DateInput, LastModifiedBy, DateModified)
			VALUES
				(
					'".$StateName."',
					'".$Description."',
					'".$StateInfo['ScoreRequired']."',
					'".$ImagePath."',
					".$_SESSION['UserID'].",
					NOW(),
					".$_SESSION['UserID'].",
					NOW()
				)
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;		 
	}
	
	function Settings_Delete_State($StateID)
	{
		$ParArr['RecordStatus'] = RECORD_STATUS_DELETED;
		$Success = $this->Settings_Update_State_Info($StateID, $ParArr);
		
		return $Success;
	}
	
	function Image_Resize($ImagePath, $width, $height)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/SimpleImage.php");
		$SimpleImage = new SimpleImage();
		
		$SimpleImage->load($ImagePath);
		$SimpleImage->resize($width, $height);
		$SimpleImage->save($ImagePath);

		return $ImagePath;		
	}
	
	function Get_Student_State($StudentIDArr='')
	{
		$Score = $this->Get_Student_Score($StudentIDArr);
		$StateArr = $this->Get_State_Info();
		
		$StateInfo = array();
		foreach((array)$Score as $thisStudentID => $StateScore)
		{
			foreach((array)$StateArr as $thisStateInfo)
			{
				$thisStateInfo['Score'] = $StateScore;
				
				if($StateScore >= $thisStateInfo['ScoreRequired'])
				{
					$thisState = $thisStateInfo;
					break;
				}
				$thisState = $thisStateInfo; // return the lowest state instead of return nothing
			}
			$StateInfo[$thisStudentID] = $thisState;
		}
		
		return $StateInfo;
	}

	function Get_Student_Score($StudentIDArr='')
	{
		$Score = array();
		
		if(count($this->Settings)==0)
			return array();
			
		$ActionHistory = $this->Get_Student_Action_History($StudentIDArr);
		$ActionHistoryArr = BuildMultiKeyAssoc($ActionHistory, array("StudentID","ActionName"), "Count", 1, 1);
		
		$LikeRecord = $this->Get_Like_Record($StudentIDArr);
		$LikeRecordAssoc = BuildMultiKeyAssoc($LikeRecord, "UserID", "LikeID",1,1);
		
		// 	calculate score from Action History
		foreach((array)$StudentIDArr as $thisStudentID ) //each student
		{
			$StudentActionHistorArr = $ActionHistoryArr[$thisStudentID];
			
			foreach((array)$StudentActionHistorArr as $Action => $CountArr) //each action type
			{
				foreach((array)$CountArr as $Count) // each action count
				{
					switch($Action)
					{
						// score (no calculation)
						case ACTION_SCORE_LOGIN:
						case ACTION_SCORE_ADD_READING_RECORD:
						case ACTION_SCORE_ADD_BOOK_REPORT:
						case ACTION_SCORE_RECOMMEND_REPORT:
						case ACTION_SCORE_COMMENT:
						case ACTION_SCORE_START_THREAD:
						case ACTION_SCORE_FOLLOW_THREAD:
							$Score[$thisStudentID] += $this->Settings[$Action] * $Count;
	//						debug_pr($Action.": +".($this->Settings[$Action] * $Count));
						break;
	
						// score (need calculation)
						case ACTION_SCORE_COMMENT_LENGTH:
						break;
						
						// penalty
						case ACTION_PENALTY_WITHOUT_LOGIN:
						case ACTION_PENALTY_COMMENT_DELETED:
						case ACTION_PENALTY_THREAD_DELETED:
						case ACTION_PENALTY_POST_DELETED:
							$Score[$thisStudentID] -= $this->Settings[$Action] * $Count;
//							debug_pr($Action.": -".($this->Settings[$Action] * $Count));
						break;
					}
					
				}
			}	
			
			if($this->Settings[ACTION_SETTING_TIMES_OF_LIKE]){
			    $Score[$thisStudentID] += (floor(count($LikeRecordAssoc[$thisStudentID])/$this->Settings[ACTION_SETTING_TIMES_OF_LIKE]))*$this->Settings[ACTION_SCORE_LIKE];
			}else{
			    $Score[$thisStudentID] += 0;
			}
			
//			if($Score[$thisStudentID]<0)
//				$Score[$thisStudentID] = 0;
		}
		
//		debug_pr($Score);
		return $Score;
	}
	
	function Get_Student_Action_History($StudentID='')
	{
		if((is_string($StudentID) && trim($StudentID)!='') || (is_array($StudentID) && count($StudentID)>0))
			$cond_StudentID = " AND StudentID IN (".implode(",",(array)$StudentID).") ";
		
		$sql = "
			SELECT
				*
			FROM
				READING_GARDEN_STUDENT_ACTION_HISTORY
			WHERE
				1
				$cond_StudentID
		";		
		
		$Result = $this->returnArray($sql);
		return $Result;
	}
	
	function Get_Like_Record($ParUserID='', $FunctionName='', $FunctionID='')
	{
		if((is_string($ParUserID) && trim($ParUserID)!='') || (is_array($ParUserID) && count($ParUserID)>0))
			$cond_UserID = " AND UserID IN (".implode(", ",(array)$ParUserID).") ";
			
		if((is_array($FunctionName) && count($FunctionName)>0) || (is_string($FunctionName) && trim($FunctionName)!='') /* trim($FunctionName)!=''*/)
			$cond_FunctionName = " AND FunctionName IN ('".implode("','",(array)$FunctionName)."') ";
			
		if(trim($FunctionID)!='')
			$cond_FunctionID = " AND FunctionID = '$FunctionID' ";
		
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$sql = "
			SELECT
				*
			FROM
				READING_GARDEN_LIKE_RECORD lr
			WHERE
				1
				$cond_UserID
				$cond_FunctionName
				$cond_FunctionID
				AND AcademicYearID = '$AcademicYearID'
		";	
		
		$Result = $this->returnArray($sql);

		return $Result;
	}
	
	function Add_Like_Record($FunctionName, $FunctionID, $ParUserID='')
	{
		if(trim($ParUserID)=='')
			$ParUserID = $_SESSION['UserID'];
			
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$sql = "
			INSERT INTO READING_GARDEN_LIKE_RECORD
				(FunctionName, FunctionID, UserID, AcademicYearID, DateInput)
			VALUES
				('$FunctionName','$FunctionID','$ParUserID','$AcademicYearID',NOW())
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;
	}
	
	function Delete_Like_Record($FunctionName='', $FunctionID='', $ParUserID='',$LikeID='')
	{
		if(trim($LikeID=='') && (trim($FunctionName=='') || trim($FunctionID=='')))
			return false;
					
		if(trim($LikeID!='')){
			$cond_LikeID = " AND LikeID = '$LikeID' ";
		}
		else {
			$cond_FunctionName = " AND FunctionName = '$FunctionName' ";
			$cond_FunctionID = " AND FunctionID = '$FunctionID' ";

			if(trim($ParUserID!=''))
				$cond_UserID = " AND UserID = '$ParUserID' ";
		}
		
		$sql = "
			DELETE FROM
				READING_GARDEN_LIKE_RECORD
			WHERE
				1
				$cond_LikeID
				$cond_FunctionName
				$cond_FunctionID
				$cond_UserID
		";
		
		$Success = $this->db_db_query($sql);
		return $Success;
	}
	
	function Cache_Student_ClassInfo($NewStudent)
	{
		include_once("form_class_manage.php");
		$fcm = new form_class_manage();
		$AcademicYearID = Get_Current_Academic_Year_ID();
		$ClassInfoArr = $fcm->Get_Student_Class_Info_In_AcademicYear($NewStudent,$AcademicYearID);

		foreach((array)$ClassInfoArr as $ClassInfo)
			$this->Cache_Student_ClassInfo_Arr[$ClassInfo['UserID']] = $ClassInfo;

	}
	
	function Get_Student_ClassInfo($StudentIDArr)
	{
		$StudentIDArr = (array)$StudentIDArr;
		# filter out student which has been existing in Cache_Student_ClassInfo
		if(is_array($this->Cache_Student_ClassInfo_Arr) && count($this->Cache_Student_ClassInfo_Arr)>0)
		{
			$ExistingStudent = array_keys($this->Cache_Student_ClassInfo_Arr);
			$NewStudent = array_diff($StudentIDArr, $ExistingStudent);
		}
		else
			$NewStudent = $StudentIDArr;

		# cache non-existing Student ClassInfo in Cache_Student_ClassInfo
		if(count($NewStudent)>0)
		{
			$this->Cache_Student_ClassInfo($NewStudent);
		}
		
		# build Return Array
		foreach((array)$StudentIDArr as $StudentID)
		{
			if($this->Cache_Student_ClassInfo_Arr[$StudentID])
				$ReturnArr[] = $this->Cache_Student_ClassInfo_Arr[$StudentID];
		}
			
		return $ReturnArr;
	}
	
	function Get_Index_Assigned_Reading_List($ParUserID='', $ReadingStatusArr='', $OrderByReadingStatus=1)
	{
		if(trim($ParUserID)=='')
			$ParUserID = $_SESSION['UserID'];
		
		if(trim($ReadingStatusArr)!='')
		{
			$cond_ReadingStatus = " AND ( 0";
			if(in_array(READING_STATUS_READ, $ReadingStatusArr))
				$cond_ReadingStatus .= " OR rr.ReadingRecordID IS NOT NULL ";
			if(in_array(READING_STATUS_UNREAD, $ReadingStatusArr))
				$cond_ReadingStatus .= " OR rr.ReadingRecordID IS NULL ";
			$cond_ReadingStatus .= ") ";
		}
		
		# Get Class Info
		$ClassInfo = $this->Get_Student_ClassInfo($ParUserID);
		$ClassID = $ClassInfo[0]['YearClassID'];

		# Other By
		if($OrderByReadingStatus==1)
			$ByReadingStatus = " rr.RecordStatus, ";
		
		$AcademicYearID = Get_Current_Academic_Year_ID();

		$sql = "
			SELECT
				b.BookID,
				b.BookName,
				b.BookCoverImage,
				b.Author,
				rr.ReadingRecordID,
				rr.RecordStatus,
				br.BookReportID,
				br.Attachment,
				br.AnswerSheet Answer,
				br.OnlineWriting,
				ar.BookReportRequired,
				ar.AnswerSheet,
				ar.WritingLimit,
				art.AssignedReadingID
			FROM
				READING_GARDEN_STUDENT_ASSIGNED_READING_TARGET art
				INNER JOIN READING_GARDEN_STUDENT_ASSIGNED_READING ar ON 
					ar.AssignedReadingID = art.AssignedReadingID
					AND ar.RecordStatus = ".RECORD_STATUS_ACTIVE."
				INNER JOIN READING_GARDEN_BOOK b ON 
					ar.BookID = b.BookID 
					AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_READING_RECORD rr ON 
					rr.AcademicYearID = '".$AcademicYearID."' 
					AND rr.UserID = '".$ParUserID."' 
					AND rr.BookID = b.BookID 
					AND rr.RecordStatus <> ".RECORD_STATUS_DELETED."
				LEFT JOIN READING_GARDEN_BOOK_REPORT br ON 
					br.ReadingRecordID = rr.ReadingRecordID 
					AND br.RecordStatus = ".RECORD_STATUS_ACTIVE."
			WHERE
				art.ClassID = '$ClassID'
				$cond_ReadingStatus
			ORDER BY 
				$ByReadingStatus art.DisplayOrder ASC
		";
		
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Get_Function_Like_Record($FunctionName)
	{
		$this->Cache_Function_Like_Record($FunctionName);
		return $this->Function_Like_List[$FunctionName];
	}
	
	function Cache_Function_Like_Record($FunctionNameArr='')
	{
		if(trim($FunctionNameArr)=='')
			$FunctionNameArr = $this->FunctionNameArr;

		$FunctionNameArr = (array)$FunctionNameArr;
		foreach((array)$FunctionNameArr as $FunctionName) // loop Function, check whether like record was cached
		{
			if(!isset($this->Function_Like_List[$FunctionName])) // if the like records was not cached 
			{
				$this->Function_Like_List[$FunctionName] = array(); // init array for new like records
				$RetrieveFunctionLike[] = $FunctionName; // prepare list of Function which need to retrieve like record 
			}
		}
		
		if(!empty($RetrieveFunctionLike)) 
		{
			$LikeRecord = $this->Get_Like_Record('',$RetrieveFunctionLike);
			
			foreach((array)$LikeRecord as $Rec)
				$this->Function_Like_List[$Rec['FunctionName']][] = $Rec;
		}
			
	}
	
	function Get_Class_Reading_Target_Mapping($ReadingTargetID='', $ClassID='', $CurrentDate='')
	{
		if(trim($ClassID)!='')
			$cond_ClassID = " AND rtcm.ClassID = '$ClassID' ";
		if(trim($ReadingTargetID)!='')
			$cond_ReadingTargetID = " AND rtcm.ReadingTargetID = '$ReadingTargetID' ";
		if(trim($CurrentDate)==1)	
			$cond_CurrentDate = " AND NOW() BETWEEN rt.StartDate AND rt.EndDate ";
		
		$sql = "
			SELECT 
				rt.*
			FROM
				READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING rtcm 
				INNER JOIN READING_GARDEN_STUDENT_READING_TARGET rt ON 
					rtcm.ReadingTargetID = rt.ReadingTargetID 
					AND RecordStatus = ".RECORD_STATUS_ACTIVE." 
			WHERE
				1
				$cond_ClassID
				$cond_ReadingTargetID
				$cond_CurrentDate
		";
		
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Get_Student_Reading_Target_Achievement($ParUserID='', $ClassID='', $ReadingTargetID='', $CurrentDate='')
	{
		if($ParUserID)
			$cond_UserID = " AND ycu.UserID IN (".implode(",",(array)$ParUserID).") ";
		if(trim($ClassID)!='')
			$cond_ClassID = " AND cm.ClassID = '$ClassID' ";
		if(trim($ReadingTargetID)!='')
			$cond_ReadingTargetID = " AND rt.ReadingTargetID = '$ReadingTargetID' ";
		else if(trim($CurrentDate)==1)
			$cond_CurrentDate = " AND NOW() BETWEEN rt.StartDate AND rt.EndDate ";
			
		$AcademicYearID = Get_Current_Academic_Year_ID(); 
		
		$sql = "
			SELECT
				ycu.UserID,
				rt.ReadingTargetID,
				count(IF(b.Language=".CATEGORY_LANGUAGE_ENGLISH.",b.BookID,NULL)) as BookReadEn,
				rt.ReadingRequiredEn,
				count(IF(b.Language=".CATEGORY_LANGUAGE_ENGLISH.",br.BookReportID,NULL)) as ReportEn,
				rt.BookReportRequiredEn,
				count(IF(b.Language=".CATEGORY_LANGUAGE_CHINESE.",b.BookID,NULL)) as BookReadCh,
				rt.ReadingRequiredCh,
				count(IF(b.Language=".CATEGORY_LANGUAGE_CHINESE.",br.BookReportID,NULL)) as ReportCh,
				rt.BookReportRequiredCh,
				count(b.BookID) as BookReadOthers,
				rt.ReadingRequiredOthers
			FROM 
				READING_GARDEN_STUDENT_READING_TARGET rt
				INNER JOIN READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING cm ON 
					cm.ReadingTargetID = rt.ReadingTargetID 
					AND rt.AcademicYearID = '$AcademicYearID'   
				INNER JOIN YEAR_CLASS_USER ycu ON cm.ClassID = ycu.YearClassID
				LEFT JOIN READING_GARDEN_READING_RECORD rr ON  
					rr.AcademicYearID = '$AcademicYearID' 
					AND rr.UserID = ycu.UserID 
					AND rr.FinishedDate BETWEEN rt.StartDate AND rt.EndDate
					AND rr.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK b ON 
					b.BookID = rr.BookID 
					AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK_REPORT br ON 
					br.ReadingRecordID= rr.ReadingRecordID 
					AND br.RecordStatus = ".RECORD_STATUS_ACTIVE."
			WHERE
				1
				$cond_ReadingTargetID
				$cond_ClassID
				$cond_UserID
				$cond_CurrentDate
			GROUP BY 
				ycu.UserID, rt.ReadingTargetID
		";
	
		$Result = $this->returnArray($sql);
		return $Result;
	}
	
	function Get_Student_Reading_Target_Achievement_Percentage($ParUserID='', $ClassID='', $ReadingTargetID='', $CurrentDate='')
	{
		$ReadingTargetAchievement = $this->Get_Student_Reading_Target_Achievement($ParUserID, $ClassID, $ReadingTargetID, $CurrentDate);
		
		foreach((array)$ReadingTargetAchievement as $StudentTarget)
		{
			# reading record
			$Arr['ReadingCountEn'] = min($StudentTarget['BookReadEn'],$StudentTarget['ReadingRequiredEn']);
			$Arr['ReadingRequiredEn'] = $StudentTarget['ReadingRequiredEn'];
			$Arr['ReadingAchievePercentEn'] = $StudentTarget['ReadingRequiredEn']>0?$Arr['ReadingCountEn']/$StudentTarget['ReadingRequiredEn']:0;
			$Arr['ReadingCountCh'] = min($StudentTarget['BookReadCh'],$StudentTarget['ReadingRequiredCh']);
			$Arr['ReadingRequiredCh'] = $StudentTarget['ReadingRequiredCh'];
			$Arr['ReadingAchievePercentCh'] = $StudentTarget['ReadingRequiredCh']>0?$Arr['ReadingCountCh']/$StudentTarget['ReadingRequiredCh']:0;
			$Arr['ReadingCountOthers'] = min($StudentTarget['BookReadOthers'],$StudentTarget['ReadingRequiredOthers']);
			$Arr['ReadingRequiredOthers'] = $StudentTarget['ReadingRequiredOthers'];
			$Arr['ReadingAchievePercentOthers'] = $StudentTarget['ReadingRequiredOthers']>0?$Arr['ReadingCountOthers']/$StudentTarget['ReadingRequiredOthers']:0;

			# book report
			$Arr['ReportCountEn'] = min($StudentTarget['ReportEn'],$StudentTarget['BookReportRequiredEn']);
			$Arr['ReportRequiredEn'] = $StudentTarget['BookReportRequiredEn'];
			$Arr['ReportAchievePercentEn'] = $StudentTarget['BookReportRequiredEn']>0?$Arr['ReportCountEn']/$StudentTarget['BookReportRequiredEn']:0;
			$Arr['ReportCountCh'] = min($StudentTarget['ReportCh'],$StudentTarget['BookReportRequiredCh']);
			$Arr['ReportRequiredCh'] = $StudentTarget['BookReportRequiredCh'];
			$Arr['ReportAchievePercentCh'] = $StudentTarget['BookReportRequiredCh']>0?$Arr['ReportCountCh']/$StudentTarget['BookReportRequiredCh']:0;
			
			# overall
			$Arr['OverallCount'] =  ($Arr['ReadingCountEn']+$Arr['ReadingCountCh']+$Arr['ReportCountEn']+$Arr['ReportCountCh']+$Arr['ReadingCountOthers']);
			$Arr['OverallRequired'] =  $StudentTarget['ReadingRequiredEn']+$StudentTarget['ReadingRequiredCh']+$StudentTarget['BookReportRequiredEn']+$StudentTarget['BookReportRequiredCh']+$StudentTarget['ReadingRequiredOthers'];
			$Arr['OverallAchievePercent'] = $Arr['OverallCount']/$Arr['OverallRequired'];

			$ReturnArr[$StudentTarget['ReadingTargetID']][$StudentTarget['UserID']] = $Arr;			
		}
		
		return $ReturnArr;
	}
	
	function Get_Index_My_Reading_Record_List($ParUserID='')
	{
		if(trim($ParUserID)=='')
			$ParUserID = $_SESSION['UserID'];
		
		$AcademicYearID = Get_Current_Academic_Year_ID();

		$sql = "
			SELECT
				b.BookID,
				b.BookName,
				b.BookCoverImage,
				b.Author,
				rr.ReadingRecordID,
				rr.RecordStatus,
				br.Attachment
			FROM
				READING_GARDEN_READING_RECORD rr
				INNER JOIN READING_GARDEN_BOOK b ON 
					rr.BookID = b.BookID 
					AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK_REPORT br ON 
					br.ReadingRecordID = rr.ReadingRecordID 
					AND br.RecordStatus = ".RECORD_STATUS_ACTIVE."
			WHERE
				rr.AcademicYearID = '".$AcademicYearID."' 
				AND rr.UserID = '".$ParUserID."' 
				AND rr.BookID = b.BookID 
				AND rr.RecordStatus <> ".RECORD_STATUS_DELETED."
			ORDER BY 
				rr.DateModified DESC ,rr.RecordStatus
		";
		
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Cache_Book_Info($BookIDArr='')
	{
		$BookInfoArr = $this->Get_Book_List($BookIDArr);
		
		foreach((array)$BookInfoArr as $thisBookInfo)
			$this->Cache_Book_Info_Arr[$thisBookInfo['BookID']] = $thisBookInfo;
			
	}
	
	function Get_My_Record_Reading_Target_DBTable_Sql($AcademicYearID='')
	{
		global $Lang;
		
		if(trim($AcademicYearID)=='')
			$AcademicYearID = Get_Current_Academic_Year_ID();
		
		#count
		$ReadingEnCountSql = "MINIMUM(count(IF(b.Language=".CATEGORY_LANGUAGE_ENGLISH.",b.BookID,NULL)),rt.ReadingRequiredEn)";
		$ReportEnCountSql = "MINIMUM(count(IF(b.Language=".CATEGORY_LANGUAGE_ENGLISH.",br.BookReportID,NULL)),rt.BookReportRequiredEn)";
		$ReadingChCountSql = "MINIMUM(count(IF(b.Language=".CATEGORY_LANGUAGE_CHINESE.",b.BookID,NULL)),rt.ReadingRequiredCh)";
		$ReportChCountSql = "MINIMUM(count(IF(b.Language=".CATEGORY_LANGUAGE_CHINESE.",br.BookReportID,NULL)),rt.BookReportRequiredCh)";
		$AllReadingCountSql = "MINIMUM(count(b.BookID),rt.ReadingRequiredOthers)";
		
		#percent
		$ReadingEnPercentSql = "ROUND($ReadingEnCountSql/rt.ReadingRequiredEn*100)";
		$ReportEnPercentSql = "ROUND($ReportEnCountSql/rt.BookReportRequiredEn*100)";
		$ReadingChPercentSql = "ROUND($ReadingChCountSql/rt.ReadingRequiredCh*100)";
		$ReportChPercentSql = "ROUND($ReportChCountSql/rt.BookReportRequiredCh*100)";
		$AllReadingPercentSql = "ROUND($AllReadingCountSql/rt.ReadingRequiredOthers*100)";
		
		#overall count
		$OverallCount = " $ReadingEnCountSql + $ReadingChCountSql + $ReportEnCountSql + $ReportChCountSql + $AllReadingCountSql ";
		$OverallRequired = " rt.ReadingRequiredEn + rt.ReadingRequiredCh + rt.BookReportRequiredEn + rt.BookReportRequiredCh + rt.ReadingRequiredOthers ";
		#overall percent
		$OverallPercentSql = "ROUND(($OverallCount)/($OverallRequired)*100)";
				
		$sql = "
			SELECT
				CONCAT(rt.StartDate,' ".$Lang['ReadingGarden']['To']." ',rt.EndDate, '   '),
				IF(rt.ReadingRequiredEn = 0,
					'".$Lang['General']['EmptySymbol']."',
					CONCAT(
						$ReadingEnPercentSql,'%',
						' [',$ReadingEnCountSql,'/',rt.ReadingRequiredEn,']'
					) 
				)AS ReadingEnDetail,
				IF(rt.BookReportRequiredEn = 0,
					'".$Lang['General']['EmptySymbol']."',
					CONCAT(
						$ReportEnPercentSql,'%',
						' [',$ReportEnCountSql,'/',rt.BookReportRequiredEn,']'
					) 
				)AS ReportEnDetail,
				IF(rt.ReadingRequiredCh = 0,
					'".$Lang['General']['EmptySymbol']."',
					CONCAT(
						$ReadingChPercentSql,'%',
						' [',$ReadingChCountSql,'/',rt.ReadingRequiredCh,']'
					) 
				)AS ReadingChDetail,
				IF(rt.BookReportRequiredCh = 0,
					'".$Lang['General']['EmptySymbol']."',
					CONCAT(
						$ReportChPercentSql,'%',
						' [',$ReportChCountSql,'/',rt.BookReportRequiredCh,']'
					) 
				)AS ReportChDetail,
				IF(rt.ReadingRequiredOthers = 0,
					'".$Lang['General']['EmptySymbol']."',
					CONCAT(
						$AllReadingPercentSql,'%',
						' [',$AllReadingCountSql,'/',rt.ReadingRequiredOthers,']'
					) 
				)AS ReadingOtherDetail,
				IF($OverallRequired = 0,
					'".$Lang['General']['EmptySymbol']."',
					CONCAT(
						$OverallPercentSql,'%'
						' [',$OverallCount,'/',$OverallRequired,']'
					) 
				)AS Target,
				$ReadingEnPercentSql AS ReadingEn,
				$ReportEnPercentSql AS ReportEn,
				$ReadingChPercentSql AS ReadingCh,
				$ReportChPercentSql AS ReportCh,
				$AllReadingPercentSql AS ReadingOverall,
				IF(
					NOW() BETWEEN rt.StartDate AND rt.EndDate,
					'row_avaliable',
					''
				)AS trCustClass
			FROM 
				READING_GARDEN_STUDENT_READING_TARGET rt
				INNER JOIN READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING cm ON 
					cm.ReadingTargetID = rt.ReadingTargetID 
					AND rt.AcademicYearID = '$AcademicYearID'   
				INNER JOIN YEAR_CLASS_USER ycu ON cm.ClassID = ycu.YearClassID
				LEFT JOIN READING_GARDEN_READING_RECORD rr ON  
					rr.AcademicYearID = '$AcademicYearID' 
					AND rr.UserID = ycu.UserID 
					AND rr.FinishedDate BETWEEN rt.StartDate AND rt.EndDate
					AND rr.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK b ON 
					b.BookID = rr.BookID 
					AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK_REPORT br ON 
					br.ReadingRecordID= rr.ReadingRecordID 
					AND br.RecordStatus = ".RECORD_STATUS_ACTIVE."
			WHERE
				ycu.UserID = '".$_SESSION['UserID']."'
			GROUP BY 
				ycu.UserID, rt.ReadingTargetID
		";
		
		return $sql;
	} 
	
	function Get_My_Record_My_Reading_Book_DBTable_Sql($AcademicYearID='')
	{
		$ParUserID = $_SESSION['UserID'];
		
//		if(trim($AcademicYearID)=='')
//			$AcademicYearID = Get_Current_Academic_Year_ID();
	
		if($AcademicYearID == 'undefined'){
			$AcademicYearID ='all';
	
		}
		if(trim($AcademicYearID)=='')
		{ // default retrieve current year 
			$AcademicYearID = Get_Current_Academic_Year_ID();
			$cond_AcademicYearID = " AND rr.AcademicYearID IN (".implode(",",(array)$AcademicYearID).") ";
		}
		else if(trim(strtolower($AcademicYearID))=='all')
		{
			 // retrieve all record
			$cond_AcademicYearID = '';
		}
		else{
			$cond_AcademicYearID = " AND rr.AcademicYearID IN (".implode(",",(array)$AcademicYearID).") ";
		}
		
		$sql = "
			SELECT
				b.BookID,
				b.BookName,
				b.BookCoverImage,
				b.Author,
				b.Publisher,
				rr.ReadingRecordID,
				rr.RecordStatus,
				rr.StartDate,
				rr.FinishedDate,
				rr.NumberOfHours,
				br.Attachment,
				br.AnswerSheet,
				br.OnlineWriting,
				br.BookReportID,
				bc.CategoryName,
				br.Grade
			FROM
				READING_GARDEN_READING_RECORD rr
				INNER JOIN READING_GARDEN_BOOK b ON 
					rr.BookID = b.BookID 
					AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK_REPORT br ON 
					br.ReadingRecordID = rr.ReadingRecordID 
					AND br.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK_CATEGORY bc ON
					bc.CategoryID = b.CategoryID
					AND bc.RecordStatus = ".RECORD_STATUS_ACTIVE."
			WHERE
				1
				$cond_AcademicYearID
				AND rr.UserID = '".$ParUserID."' 
				AND rr.BookID = b.BookID 
				AND rr.RecordStatus <> ".RECORD_STATUS_DELETED."			
		";

		return 	$sql;	
	}


	function Get_New_My_Reading_Book_Valid_Book_List($BookSource='', $BookInfo='', $CategoryID='', $Language='')
	{
		if(trim($BookSource)=='')
		{
			$ClassInfo = $this->Get_Student_ClassInfo($_SESSION['UserID']);
			$BookSourceSetting = $this->Settings_Get_Class_Book_Source($ClassInfo[0]['YearClassID']);
			$BookSource = $BookSourceSetting[0]['BookSource'];
		}
		
		if($BookSource==BOOK_REPORT_SOURCE_BOOK_LIST)
		{
			if(!$ClassInfo)
				$ClassInfo = $this->Get_Student_ClassInfo($_SESSION['UserID']);
			$thisRemoccmedBookList = $this->Get_Recommend_Book_Mappings(array('YearID'=>$ClassInfo[0]['YearID']));
			$RemoccmedBookIDArr = Get_Array_By_Key($thisRemoccmedBookList, "BookID");
		}
		else if($BookSource==BOOK_REPORT_SOURCE_EXISTING_BOOK)
		{
			$IncludeStudent = 0;
			$RemoccmedBookIDArr = '';
		}
		else
		{
			# don't include student submitted books
			$IncludeStudent = 0;
			$RemoccmedBookIDArr = '';
		}
		
		$BookList = $this->Get_Book_List($RemoccmedBookIDArr,'',$CategoryID,$Language,$BookInfo,$IncludeStudent);
		
		return $BookList;
		
	}
	
	function Delete_Reading_Record_By_BookID($BookID, $ParUserID='')
	{
		if(trim($ParUserID)=='')
			$ParUserID = $_SESSION['UserID'];
			
		$ReadingRecordArr = $this->Get_Student_Reading_Record('',$BookID);
		
		$ReadingRecordIDArr = Get_Array_By_Key($ReadingRecordArr,"ReadingRecordID");
		
		$this->Start_Trans();
		
		$sql = "
			UPDATE
				READING_GARDEN_READING_RECORD
			SET
				RecordStatus = ".RECORD_STATUS_DELETED.",
				LastModifiedBy = ".$_SESSION['UserID'].",
				DateModified = NOW()
			WHERE
				ReadingRecordID IN (".(implode(',',$ReadingRecordIDArr)).")
				AND RecordStatus = ".RECORD_STATUS_ACTIVE."
		";
		
		$Success["DeleteReadingRecord"] = $this->db_db_query($sql);
		
		if($Success["DeleteReadingRecord"] && mysql_affected_rows()>0)
		{
			$NoOfDeleted = -1*mysql_affected_rows();
			$Success["AddScoreAction"] = $this->Update_State_Score_Action(ACTION_SCORE_ADD_READING_RECORD,$NoOfDeleted);
		}
		
		$sql = "
			UPDATE
				READING_GARDEN_BOOK_REPORT
			SET
				RecordStatus = ".RECORD_STATUS_DELETED.",
				LastModifiedBy = ".$_SESSION['UserID'].",
				DateModified = NOW()
			WHERE
				ReadingRecordID IN (".(implode(',',$ReadingRecordIDArr)).")
				AND RecordStatus = ".RECORD_STATUS_ACTIVE."
		";
		
		$Success["DeleteBookReport"] = $this->db_db_query($sql);
//		if($Success["DeleteBookReport"] && mysql_affected_rows()>0)
//		{
//			$NoOfDeleted = -1*mysql_affected_rows();
//			$this->Update_State_Score_Action(ACTION_SCORE_ADD_BOOK_REPORT,$NoOfDeleted);
//		}
		
		if(in_array(false,$Success))
		{
			$this->RollBack_Trans();
			return false;	
		}		
		else
		{
			$this->Commit_Trans();
			return true;	
		}	
		
	}
	
	function Get_Class_List($ClassID=''){
		global $Lang, $ReadingGardenLib;		
		include_once("form_class_manage.php");
		
		$year_class = new year_class($ClassID,true,false,true);		
		$AcademicYearID = Get_Current_Academic_Year_ID();
		$fcm = new form_class_manage();
		$TeachingOnly = false;
		$AllClassList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, "", $TeachingOnly);
		
		$DisplayClassIDArr = array($year_class->YearID);
		foreach((array)$AllClassList as $thisClassInfo)
		{
			if(in_array($thisClassInfo['YearID'],$DisplayClassIDArr) || $ClassID == "")
				$ClassList[] = $thisClassInfo["YearClassID"];
		}		
		return $ClassList;
	}
	
	function Get_Student_Target_Reading_Count($ClassID='', $ReadingTargetID='',$TargetInProgress='', $AcademicYearID='', &$have_grade=true)
	{
		global $Lang;
//		debug_r(array($ClassID,$ReadingTargetID,$TargetInProgress,$AcademicYearID));
		if(trim($AcademicYearID)=='')
			$AcademicYearID = Get_Current_Academic_Year_ID();
			
		if(is_array($ClassID)){
			$cond_ClassID = " AND cm.ClassID IN ('".implode("','", $ClassID)."') ";			
		}	
		else if(trim($ClassID)!='')
			$cond_ClassID = " AND cm.ClassID = '$ClassID' ";

		if(trim($ReadingTargetID)!='')
			$cond_ReadingTargetID = " AND rt.ReadingTargetID = '$ReadingTargetID' ";
		
		if($TargetInProgress==1)
			$cond_Date = " AND CURDATE() BETWEEN rt.StartDate AND rt.EndDate ";
		
		#Total 
		$ReadingEnTotalSql = "count(IF(b.Language=".CATEGORY_LANGUAGE_ENGLISH.",b.BookID,NULL))";
		$ReportEnTotalSql = "count(IF(b.Language=".CATEGORY_LANGUAGE_ENGLISH.",br.BookReportID,NULL))";
		$ReadingChTotalSql = "count(IF(b.Language=".CATEGORY_LANGUAGE_CHINESE.",b.BookID,NULL))";
		$ReportChTotalSql = "count(IF(b.Language=".CATEGORY_LANGUAGE_CHINESE.",br.BookReportID,NULL))";
		$AllReadingTotalSql = "count(b.BookID)";

		# Number of reading/report counted in target
		$ReadingEnCountSql = "MINIMUM($ReadingEnTotalSql,rt.ReadingRequiredEn)";
		$ReportEnCountSql = "MINIMUM($ReportEnTotalSql,rt.BookReportRequiredEn)";
		$ReadingChCountSql = "MINIMUM($ReadingChTotalSql,rt.ReadingRequiredCh)";
		$ReportChCountSql = "MINIMUM($ReportChTotalSql,rt.BookReportRequiredCh)";
		$AllReadingCountSql = "MINIMUM($AllReadingTotalSql,rt.ReadingRequiredOthers)";
		
		#overall count
		$OverallTotal = " $ReadingEnTotalSql + $ReadingChTotalSql + $ReportEnTotalSql + $ReportChTotalSql + $AllReadingTotalSql ";
		$OverallCount = " $ReadingEnCountSql + $ReadingChCountSql + $ReportEnCountSql + $ReportChCountSql + $AllReadingCountSql ";
		$OverallRequired = " rt.ReadingRequiredEn + rt.ReadingRequiredCh + rt.BookReportRequiredEn + rt.BookReportRequiredCh + rt.ReadingRequiredOthers ";

		#overall percent 20140804-overallpercent
//		$OverallPercentSql = "ROUND(($OverallCount)/($OverallRequired)*100)";
		
		$OverallPercentSql = " ROUND((MAXIMUM((MINIMUM(count(IF(b.Language=2,b.BookID,NULL)),rt.ReadingRequiredEn) 
								+ MINIMUM(count(IF(b.Language=1,b.BookID,NULL)),rt.ReadingRequiredCh)),
								MINIMUM(count(b.BookID),rt.ReadingRequiredOthers))+ 
								MINIMUM(count(IF(b.Language=2,br.BookReportID,NULL)),rt.BookReportRequiredEn)+ 
								MINIMUM(count(IF(b.Language=1,br.BookReportID,NULL)),rt.BookReportRequiredCh))/
								(MAXIMUM(rt.ReadingRequiredEn + rt.ReadingRequiredCh, rt.ReadingRequiredOthers) " .
										"+ rt.BookReportRequiredEn + rt.BookReportRequiredCh )*100)";				
		$sql = "
			SELECT
				ycu.UserID,
				rt.StartDate,
				rt.EndDate,
				$ReadingEnTotalSql as TotalReadingEn,
				$ReadingEnCountSql as ReadingCountEn,
				rt.ReadingRequiredEn,
				$ReportEnTotalSql as TotalReportEn,
				$ReportEnCountSql as ReportCountEn,
				rt.BookReportRequiredEn,
				$ReportChTotalSql as TotalReadingCh,
				$ReadingChCountSql as ReadingCountCh,
				rt.ReadingRequiredCh,
				$ReportChTotalSql as TotalReportCh,
				$ReportChCountSql as ReportCountCh,
				rt.BookReportRequiredCh,
				$AllReadingTotalSql as TotalReading,
				$AllReadingCountSql as ReadingCount,
				rt.ReadingRequiredOthers,
				$OverallPercentSql AS Progress
			FROM 
				READING_GARDEN_STUDENT_READING_TARGET rt
				INNER JOIN READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING cm ON 
					cm.ReadingTargetID = rt.ReadingTargetID 
					AND rt.AcademicYearID = '$AcademicYearID'   
				INNER JOIN YEAR_CLASS_USER ycu ON cm.ClassID = ycu.YearClassID
				LEFT JOIN READING_GARDEN_READING_RECORD rr ON  
					rr.AcademicYearID = '$AcademicYearID' 
					AND rr.UserID = ycu.UserID 
					AND rr.FinishedDate BETWEEN rt.StartDate AND rt.EndDate
					AND rr.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK b ON 
					b.BookID = rr.BookID 
					AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK_REPORT br ON 
					br.ReadingRecordID= rr.ReadingRecordID 
					AND br.RecordStatus = ".RECORD_STATUS_ACTIVE."
			WHERE
				1
				$cond_ReadingTargetID
				$cond_Date
				$cond_ClassID
			GROUP BY 
				ycu.UserID, rt.ReadingTargetID
			ORDER BY
				ycu.ClassNumber ASC
		";
		
		$Result = $this->returnArray($sql);
		
		$sql = "SELECT
				rr.UserID, br.Grade 
				FROM 
				READING_GARDEN_STUDENT_READING_TARGET rt
				INNER JOIN READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING cm ON 
					cm.ReadingTargetID = rt.ReadingTargetID 
					AND rt.AcademicYearID = '$AcademicYearID'   
				INNER JOIN YEAR_CLASS_USER ycu ON cm.ClassID = ycu.YearClassID
				LEFT JOIN READING_GARDEN_READING_RECORD rr ON  
					rr.AcademicYearID = '$AcademicYearID' 
					AND rr.UserID = ycu.UserID 
					AND rr.FinishedDate BETWEEN rt.StartDate AND rt.EndDate
					AND rr.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK b ON 
					b.BookID = rr.BookID 
					AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK_REPORT br ON 
					br.ReadingRecordID= rr.ReadingRecordID 
					AND br.RecordStatus = ".RECORD_STATUS_ACTIVE."
				WHERE 1 $cond_ReadingTargetID	 ORDER BY rr.UserID";
		$Result2 = $this->returnArray($sql);
		$result_arr = array();
		$have_grade=true;
		
		foreach($Result as $item){
			$result_arr[$item["UserID"]] = array();
			$result_arr[$item["UserID"]]["Grade"] = 0;
			foreach($item as $key=> $value){
				$result_arr[$item["UserID"]][$key] = $value;
			}
		}		
				
		foreach($Result2 as $item){				
			if(!is_array($result_arr[$item["UserID"]]) ||  $item["Grade"] == ""){
				continue;
			}
			else if(!is_numeric($item["Grade"]) && $item["Grade"] != ""){
				$have_grade = false;
				break;
			}
			
			if($result_arr[$item["UserID"]]["Grade"] == ""){
				$result_arr[$item["UserID"]]["Grade"] = (float)$item["Grade"];
			}else{
				$result_arr[$item["UserID"]]["Grade"] += (float)$item["Grade"];
			}
		}
		
		if($have_grade == true){
			return $result_arr;			
		}else{
			return $Result;		
		}
	} 

	function Get_Book_Cover_Image($Path, $Width, $Height, $border=0)
	{
		global $image_path,$LAYOUT_SKIN;
		
		if(trim($Path)=='')
			$cover_image_path = $image_path."/".$LAYOUT_SKIN.'/reading_scheme/temp_book_cover.gif';
		else
			$cover_image_path = $this->BookCoverImagePath.'/'.$Path;
		$x = '<img src="'.$cover_image_path.'" width="'.$Width.'px" height="'.$Height.'px"  border='.$border.'/>';
		
		return $x;
	}	
	
	function Is_State_Score_Exist($Score, $StateID='')
	{
		if(trim($Score)!='')
			$cond_StateID = " AND StateID <> '$StateID' ";
		
		$sql = "
			SELECT
				COUNT(*)
			FROM
				READING_GARDEN_STATE
			WHERE	
				RecordStatus = ".RECORD_STATUS_ACTIVE."
				AND ScoreRequired = '$Score'
				$cond_StateID
		";
		
		$Result = $this->returnVector($sql);
		return $Result[0]>0?true:false;
		
	}
	
	function Get_Class_List_From_Form_Class_Option($YearClassID)
	{
		global $PATH_WRT_ROOT;
		
		if(strstr($YearClassID,"::"))
		{
			$YearClassID = str_replace("::",'',$YearClassID);	
		}
		else if($YearClassID==0)
			$YearClassID = '';
		else
		{
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$YearObj = new Year($YearClassID);
			$ClassList = $YearObj->Get_All_Classes();
			$YearClassID = Get_Array_By_Key($ClassList,"YearClassID");
		}
		
		return $YearClassID;
	}
	
	function Get_Management_Announcement_DBTable_Sql($Keyword='')
	{

		$NameFieldModifiedBy = getNameFieldByLang("iu.");

		if($Keyword!='')
		{
			$Keyword = $this->Get_Safe_Sql_Like_Query(trim($Keyword));
			$cond_Keyword = " 
				AND (
					a.Title LIKE '%$Keyword%'
					OR a.Message LIKE '%$Keyword%'
					OR a.StartDate LIKE '%$Keyword%'
					OR a.EndDate LIKE '%$Keyword%'
					OR a.DateModified LIKE '%$Keyword%'
					OR yc.ClassTitleB5 LIKE '%$Keyword%'
					OR yc.ClassTitleEN LIKE '%$Keyword%'
					OR $NameFieldModifiedBy LIKE '%$Keyword%'
				)  
			";
		}
		
		$YearClassName = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
		
		$sql = "
			SELECT
				CONCAT('<a href=\"edit_announcement.php?AnnouncementID=',a.AnnouncementID,'\">',a.Title,'</a>') AS Title,
				DATE(a.StartDate),
				DATE(a.EndDate),
				GROUP_CONCAT( $YearClassName ORDER BY y.Sequence, yc.Sequence ASC SEPARATOR ', ') AS TargetClass,
				$NameFieldModifiedBy AS LastModifiedBy,
				a.DateModified,
				CONCAT('<input type=\"checkbox\" name=\"AnnouncementID[]\" class=\"AnnouncementID\" value=\"',a.AnnouncementID,'\">')
			FROM
				READING_GARDEN_ANNOUNCEMENT a
				LEFT JOIN READING_GARDEN_ANNOUNCEMENT_TARGET_CLASS atc ON a.AnnouncementID = atc.AnnouncementID
				LEFT JOIN YEAR_CLASS yc ON atc.ClassID = yc.YearClassID 
				LEFT JOIN YEAR y ON y.YearID = yc.YearID
				LEFT JOIN INTRANET_USER iu ON iu.UserID = a.LastModifiedBy 
			WHERE
				a.RecordStatus = ".RECORD_STATUS_ACTIVE."
				AND (a.AwardSchemeID = '' OR a.AwardSchemeID IS NULL)
				$cond_Keyword
			GROUP BY 
				a.AnnouncementID
		";
		
		return $sql;
	}
	
	function Manage_Announcement_Record($Action,$DataArr,$AnnouncementID='')
	{
		$this->Start_Trans();
		
		$Success = array();
		$Attachment = $DataArr['Attachment'];
		$Title = $this->Get_Safe_Sql_Query($DataArr['Title']);
		$Message = $this->Get_Safe_Sql_Query($DataArr['Message']);
		$StartDate = $DataArr['StartDate'];
		$EndDate = $DataArr['EndDate'];
		$ClassID = $DataArr['ClassID'];
		$AwardSchemeID = $DataArr['AwardSchemeID'];
		
		switch($Action)
		{
			case "add":
				$sql = "
					INSERT INTO READING_GARDEN_ANNOUNCEMENT
						(Title, Message, StartDate, EndDate, Attachment, AwardSchemeID, RecordStatus, DateInput, DateModified, InputBy, LastModifiedBy)
					VALUES
						('$Title', '$Message','$StartDate','$EndDate','$Attachment','$AwardSchemeID',".RECORD_STATUS_ACTIVE.",NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')
				";
				$Success['AddAnnouncement'] = $this->db_db_query($sql);
				$AnnouncementID = mysql_insert_id();
				
				$ClassAnnouncement['ClassID'] = $ClassID;
				$ClassAnnouncement['AnnouncementID'] = $AnnouncementID;
				$Success['AssignToClass'] = $this->Assign_Announcement_To_Class($ClassAnnouncement);
				
			break;
			
			case "edit":
				if(trim($AwardSchemeID)=='' && trim($AnnouncementID)=='') // avoid update all announcement
					{$this->RollBack_Trans(); return false; }
								
				if(trim($AwardSchemeID)!='')
				{
					$AnnounceInfo = $this->Get_Announcement_Info('',$AwardSchemeID);
					$AnnouncementID = $AnnounceInfo[0]['AnnouncementID'];
					if(trim($AnnouncementID)=='') //if no related announcement was found, ad a new announcement
					{
						$Success['AddAnnouncement'] = $this->Manage_Announcement_Record("add",$DataArr);
					}
				}
				
				if(!$Success['AddAnnouncement'])
				{
					if(trim($AnnouncementID)!='')
						$cond_AnnouncementID = " AND AnnouncementID = '$AnnouncementID' ";
					
					$sql = "
						UPDATE 
							READING_GARDEN_ANNOUNCEMENT
						SET
							Title = '$Title',
							Message = '$Message',
							StartDate = '$StartDate',
							EndDate = '$EndDate',
							Attachment = '$Attachment',
							DateModified = NOW(),
							LastModifiedBy = '".$_SESSION['UserID']."'
						WHERE
							1
							$cond_AnnouncementID
					";
					
					$Success['EditAnnouncement'] = $this->db_db_query($sql);
					$Success['UpdateMapping'] = $this->Update_Announcement_Class_Mapping($AnnouncementID,$ClassID);
				}
			break;
			
			case "delete":
				$sql = "
					UPDATE
						READING_GARDEN_ANNOUNCEMENT
					SET
						RecordStatus = ".RECORD_STATUS_DELETED.",
						LastModifiedBy = '".$_SESSION['UserID']."',
						DateModified = NOW()
					WHERE
						AnnouncementID IN (".implode(",",(array)$AnnouncementID).")
				";
				$Success['DeleteAnnouncement'] = $this->db_db_query($sql);
			break;
		}

		if(in_array(false,$Success))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	function Assign_Announcement_To_Class($ClassAnnouncement)
	{
		$ClassIDArr = $ClassAnnouncement['ClassID'];
		$AnnouncementID = $ClassAnnouncement['AnnouncementID'];
		
		if(empty($ClassIDArr) || empty($AnnouncementID))	return false;
		
		foreach((array)$ClassIDArr as $ClassID)
		{
			$ValueArr[] = "
				(
					'$AnnouncementID',
					'$ClassID',
					NOW(),
					'".$_SESSION['UserID']."'
				)
			";
		}
		if(count($ValueArr)==0) return true;
		
		$ValueSql = implode(",",$ValueArr);
		
		$sql = "
			INSERT INTO READING_GARDEN_ANNOUNCEMENT_TARGET_CLASS
				(AnnouncementID, ClassID, DateInput, InputBy)
			VALUES
				$ValueSql
		";
		
		$Success = $this->db_db_query($sql) or die($sql.mysql_error());
		
		return $Success;

	}
	
	function Remove_Announcement_From_Class($AnnouncementID, $ClassID='')
	{
		$cond_ClassID = '';
		if($ClassID) {
			$classIDs = implode(",",(array)$ClassID);
			if ($classIDs) { 
				$cond_ClassID = " AND ClassID IN (".$classIDs.") ";
			}
		}
		$sql = "
			DELETE FROM
				READING_GARDEN_ANNOUNCEMENT_TARGET_CLASS
			WHERE
				AnnouncementID = '$AnnouncementID'
				$cond_ClassID
		";
		
		$Success = $this->db_db_query($sql);
		return $Success;
	}
	
	function Update_Announcement_Class_Mapping($AnnouncementID,$YearClassIDArr)
	{
		$ClassAnnouncementMapping = $this->Get_Class_Announcement('',$AnnouncementID);
		$ClassList = Get_Array_By_Key($ClassAnnouncementMapping,"ClassID");
		
		$DeleteClassList = array_diff((array)$ClassList,(array)$YearClassIDArr);
		$AddClassList = array_diff((array)$YearClassIDArr,(array)$ClassList);
		if($DeleteClassList)
			$Success['Delete'] = $this->Remove_Announcement_From_Class($AnnouncementID, $DeleteClassList);
		
		if($AddClassList)
		{
			$ParArr['AnnouncementID'] = $AnnouncementID;
			$ParArr['ClassID'] = $AddClassList;
			$Success["Assign"] = $this->Assign_Announcement_To_Class($ParArr);
		}
		return !in_array(false,(array)$Success);
	}
	
	function Settings_Announcement_Upload_Attachment($AnnouncementID, $Attachment, $Attachment_name, $TempPath, $old_Attachment)
	{
		global $PATH_WRT_ROOT, $intranet_root;

		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$fs = new libfilesystem();
		
		# Create folder if not exist
		$path = $intranet_root.$this->AnouncementAttachmentPath;
		$tmp_path = $path."/$TempPath";
		$fs->folder_new($tmp_path);
		
		# retrieve list
		$fs->folderlist($tmp_path);
		$ExistFile = $fs->rs;

		# delete files first
		for($i=0 ;$i<sizeof($old_Attachment);$i++)
		{
			$UploadFilePath =  $tmp_path."/".stripslashes($old_Attachment[$i]);
			$UploadFilePathArr[] = $UploadFilePath;
		}
		$RemoveFile = array_diff((array)$ExistFile,(array)$UploadFilePathArr);
		$RemoveFile = array_values((array)$RemoveFile);
			
		for($i=0; $i<sizeof($Attachment);$i++)
		{
			if(empty($Attachment[$i])) continue;
			
			$ctr = 2;
			$UploadFilePath =  $tmp_path."/".stripslashes($Attachment_name[$i]);
			while(in_array($UploadFilePath, (array)$UploadFilePathArr))
			{
				$tmp_name = build_duplicate_file_name(stripslashes($Attachment_name[$i]),$ctr);
				$UploadFilePath =  $tmp_path."/".$tmp_name;
				$ctr++;
			}
//			$UploadFilePath =  $tmp_path."/".stripslashes($Attachment_name[$i]);
			$Result['CopyFile'][] = $fs->file_copy($Attachment[$i], $UploadFilePath);
			$UploadFilePathArr[] = $UploadFilePath;
		}
		
//		for($i=0 ;$i<sizeof($old_Attachment);$i++)
//		{
//			$UploadFilePath =  $tmp_path."/".stripslashes($old_Attachment[$i]);
//			$UploadFilePathArr[] = $UploadFilePath;
//		}
//				while(in_array($UploadFilePath, (array)$ExistFile))
//		{
//			$tmp_name = build_duplicate_file_name($ImagePath_name,$i);
//			$UploadFilePath =  $tmp_path."/".$tmp_name;
//			$i++;
//		}
		
//		$RemoveFile = array_diff((array)$ExistFile,(array)$UploadFilePathArr);
//		$RemoveFile = array_values((array)$RemoveFile);
		
		for($i=0; $i<sizeof($RemoveFile);$i++)
		{
			$Result['CopyFile'][] = $fs->item_remove($RemoveFile[$i]);
		}
		
		if(!in_array(false,(array)$Result['CopyFile']))
			return $TempPath;
		else
		{
//			$fs->folder_remove_recursive($tmp_path);
			return false;
		}
		
	}

	function Get_Class_Announcement($ClassIDArr='', $AnnouncementIDArr='',$DisplayPeriod='',$AnnouncementType='')
	{
		if($AnnouncementIDArr)
			$cond_AnnouncementID = " AND a.AnnouncementID IN (".implode(",",(array)$AnnouncementIDArr).") ";
			
		if($ClassIDArr)
			$cond_ClassID = " AND atc.ClassID IN (".implode(",",(array)$ClassIDArr).") ";
		
		if($DisplayPeriod== 1)
		{
			$cond_Date = " AND CURDATE() >= DATE(a.StartDate) ";
			$cond_Date .= " AND (CURDATE() <= DATE(a.EndDate) OR a.EndDate = 0) ";
		}
		
		if(trim($AnnouncementType)!='')
		{
			if($AnnouncementType == ANNOUNCEMENT_TYPE_ANNOUNCEMENT)
				$cond_AnnounceType = " AND a.AwardSchemeID <= 0 ";
			else
				$cond_AnnounceType = " AND (a.AwardSchemeID > 0)";
		}
		
		$sql = "
			SELECT
				a.AnnouncementID,
				a.Title,
				a.Message,
				DATE(a.StartDate) StartDate,
				DATE(a.EndDate) EndDate,
				a.Attachment,
				a.RecordStatus,
				a.DateInput,
				a.DateModified,
				a.InputBy,
				a.LastModifiedBy,
				a.AwardSchemeID,
				atc.ClassID
			FROM
				READING_GARDEN_ANNOUNCEMENT a
				LEFT JOIN READING_GARDEN_ANNOUNCEMENT_TARGET_CLASS atc ON a.AnnouncementID = atc.AnnouncementID
				LEFT JOIN READING_GARDEN_AWARD_SCHEME AS rgas ON rgas.AwardSchemeID=a.AwardSchemeID
			WHERE
				a.RecordStatus = ".RECORD_STATUS_ACTIVE."
				$cond_AnnouncementID
				$cond_AnnounceType
				$cond_ClassID
				$cond_Date
				AND ((a.AwardSchemeID=0 AND rgas.AwardSchemeID IS NULL) OR (rgas.AwardSchemeID>0 && a.AwardSchemeID>0))
			ORDER BY
				DATE(a.StartDate) DESC
		";
		
		$Result= $this->returnArray($sql);
		
		return $Result;
	}
	
	function Get_Announcement_Info($AnnouncementID='', $AwardSchemeID='')
	{
		if($AnnouncementID)
			$cond_AnnouncementID = " AND a.AnnouncementID IN (".implode(",",(array)$AnnouncementID).") ";
		if($AwardSchemeID)
			$cond_AwardSchemeID = " AND a.AwardSchemeID IN (".implode(",",(array)$AwardSchemeID).") ";
		
		$NameFieldPostBy = getNameFieldByLang("iu.");
		
		$sql = "
			SELECT 
				a.AnnouncementID,
				a.Title,
				a.Message,
				DATE(a.StartDate) StartDate,
				DATE(a.EndDate) EndDate,
				a.Attachment,
				a.AwardSchemeID,
				a.RecordStatus,
				a.DateInput,
				a.DateModified,
				a.InputBy,
				a.LastModifiedBy,
				$NameFieldPostBy AS PostedBy
			FROM
				READING_GARDEN_ANNOUNCEMENT a 
				LEFT JOIN INTRANET_USER iu ON iu.UserID = a.InputBy
			WHERE
				1
				$cond_AnnouncementID
				$cond_AwardSchemeID
		";
		$Result= $this->returnArray($sql);
		return $Result;
	}
	
	function Get_Announcement_Attachment($AttPath, $ReturnName=0)
	{
		if(trim($AttPath)=='')
			return array();
		global $intranet_root, $fs;
		
		include_once("libfilesystem.php");
		$fs = new libfilesystem();
		
		$path = $intranet_root.$this->AnouncementAttachmentPath;
		$tmp_path = $path."/$AttPath";
		
		# retrieve list
		$fs->folderlist($tmp_path);
		$ExistFile = $fs->rs;
		if($ReturnName==1)
		{
			foreach((array)$ExistFile as $thisFile)
				$ReturnArr[] = $fs->get_file_basename($thisFile);
		}
		else
			$ReturnArr = $ExistFile;
		return $ReturnArr;
	}
	
//	function Get_Student_Reading_Record_And_Report_Grade($StudentID='', $ReadingRecordID='', $Recommend='', $StartDate='',$EndDate='', $Language='', $OrderBy='')
//	{
//		if(trim($StudentID)!='')
//			$cond_Student = " AND UserID IN (".implode(",",(array)$StudentID).") ";
//		if(trim($ReadingRecordID)!='')
//			$cond_ReadingRecordID = " AND rr.ReadingRecordID IN (".implode(",",(array)$ReadingRecordID).") ";
//		if($Recommend==1)
//			$cond_Recommend = " AND br.Recommend = 1 ";	
//		if(trim($StartDate)!='')
//			$cond_StartDate = " AND rr.FinishedDate >= '$StartDate' ";
//		if(trim($EndDate)!='')
//			$cond_EndDate = " AND rr.FinishedDate <= '$EndDate' ";
//		if(trim($Language)!='' && $Language != CATEGORY_LANGUAGE_OTHERS)
//			$cond_Language = " AND b.Language = '$Language' ";
//		if(trim($OrderBy)=='')
//			$OrderBy = " br.DateModified DESC ";
//			
//		
//		$sql = "
//			SELECT
//				rr.UserID,
//				rr.ReadingRecordID,
//				rr.StartDate,
//				rr.FinishedDate,
//				rr.NumberOfHours,
//				b.BookID,
//				b.CallNumber, 
//				b.BookName,
//				b.Author,
//				b.Publisher,
//				b.Description,
//				b.BookCoverImage,
//				b.CategoryID,
//				br.BookReportID,
//				br.Attachment,
//				br.Answersheet,
//				br.OnlineWriting,
//				br.Grade,
//				br.TeacherComment,
//				br.Recommend
//			FROM
//				READING_GARDEN_READING_RECORD rr
//				INNER JOIN READING_GARDEN_BOOK b ON 
//					rr.BookID = b.BookID
//					AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
//				LEFT JOIN READING_GARDEN_BOOK_REPORT br ON 
//					rr.ReadingRecordID = br.ReadingRecordID 
//					AND br.RecordStatus = ".RECORD_STATUS_ACTIVE."
//			WHERE
//				rr.RecordStatus = ".RECORD_STATUS_ACTIVE."
//				$cond_ReadingRecordID
//				$cond_Student
//				$cond_StartDate
//				$cond_EndDate
//				$cond_Language
//				$cond_Recommend
//			ORDER BY
//				$OrderBy
//		";
//		
//		$Result = $this->returnArray($sql);
////		debug_pr($Result);
//		
//		return $Result;
//	}

	function Get_Student_Reading_Record_And_Report_Grade($StudentID='', $ReadingRecordID='', $Recommend='', $StartDate='',$EndDate='', $Language='', $OrderBy='',$AcademicYearID='', $CategoryID='', $AwardSchemeID='')
	{
		
		if($StudentID)
			$cond_Student = " AND UserID IN (".implode(",",(array)$StudentID).") ";
		if($ReadingRecordID)
			$cond_ReadingRecordID = " AND rr.ReadingRecordID IN (".implode(",",(array)$ReadingRecordID).") ";
		if($Recommend==1)
			$cond_Recommend = " AND br.Recommend = 1 ";	
		if(trim($StartDate)!='')
			$cond_StartDate = " AND rr.FinishedDate >= '$StartDate' ";
		if(trim($EndDate)!='')
			$cond_EndDate = " AND rr.FinishedDate <= '$EndDate' ";
		if(trim($Language)!='' && $Language != CATEGORY_LANGUAGE_OTHERS)
			$cond_Language = " AND b.Language = '$Language' ";
		if(trim($OrderBy)=='')
			$OrderBy = " br.DateModified DESC ";
		if($CategoryID)
			$cond_CategoryID = " AND b.CategoryID IN (".implode(",",(array)$CategoryID).") ";
		if (is_array($AcademicYearID)) {
			if ($AcademicYearID)
				$cond_AcademicYearID = " AND rr.AcademicYearID IN (".implode(",",(array)$AcademicYearID).") ";
			else 
				$cond_AcademicYearID = '';
		}
		else {
			if(trim($AcademicYearID)=='') // default retrieve current year 
				$AcademicYearID = Get_Current_Academic_Year_ID();
			else if(trim(strtolower($AcademicYearID))=='all') // retrieve all record
				$cond_AcademicYearID = '';
			else			
				$cond_AcademicYearID = '';
		}	
		if($AwardSchemeID)
			$cond_AwardSchemeID = " AND rr.AwardSchemeID IN (".implode(",",(array)$AwardSchemeID).") ";
		
		$sql = "
			SELECT
				rr.UserID,
				rr.ReadingRecordID,
				rr.StartDate,
				rr.FinishedDate,
				rr.NumberOfHours,
				rr.AcademicYearID,
				b.BookID,
				b.CallNumber, 
				b.BookName,
				b.Author,
				b.Publisher,
				b.Description,
				b.BookCoverImage,
				b.CategoryID,
				b.Language,
				br.BookReportID,
				br.Attachment,
				br.Answersheet,
				br.OnlineWriting,
				br.Grade,
				br.TeacherComment,
				br.Recommend
			FROM
				READING_GARDEN_READING_RECORD rr
				INNER JOIN READING_GARDEN_BOOK b ON 
					rr.BookID = b.BookID
					AND b.RecordStatus = ".RECORD_STATUS_ACTIVE."
				LEFT JOIN READING_GARDEN_BOOK_REPORT br ON 
					rr.ReadingRecordID = br.ReadingRecordID 
					AND br.RecordStatus = ".RECORD_STATUS_ACTIVE."
					AND (
							(br.Attachment IS NOT NULL AND br.Attachment <> '')
							OR (br.AnswerSheet IS NOT NULL AND br.AnswerSheet <> '') OR
						(br.OnlineWriting IS NOT NULL AND br.OnlineWriting <> '') 
						) 
			WHERE
				rr.RecordStatus = ".RECORD_STATUS_ACTIVE."
				$cond_ReadingRecordID
				$cond_AcademicYearID
				$cond_Student
				$cond_StartDate
				$cond_EndDate
				$cond_Language
				$cond_Recommend
				$cond_CategoryID
				$cond_AwardSchemeID
			ORDER BY
				$OrderBy
		";

		$Result = $this->returnArray($sql);
//		debug_pr($Result);
		
		return $Result;
	}
		
	function Get_Teacher_View_Default_Class_ID()
	{
		include_once("form_class_manage.php");
		$fcm = new form_class_manage();
		
		$AcademicYearID = Get_Current_Academic_Year_ID();
		$ClassTeacherClass =  $fcm->Get_Class_Teacher_Class($_SESSION['UserID']);
		
		if(count($ClassTeacherClass)>0)
		{
			$ClassID = $ClassTeacherClass[0]['ClassID'];
		}
		else if($this->isAdmin) // get first class of all classes 
		{
			$AllClassList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);
			$ClassID = $AllClassList[0]['YearClassID'];
		}
		else
		{
			include_once("subject_class_mapping.php");
			$scm = new subject_class_mapping();
			$TeachingClassList = $scm->returnSubjectTeacherClass($_SESSION['UserID']);
			$ClassID = $TeachingClassList[0]['ClassID'];
		}
		
		return $ClassID;
	}
	
	function Get_Book_Report_Comment_DBTable_Sql($BookReportID)
	{
		$NameFieldInputBy = getNameFieldByLang("iu.");
		$sql = "
			SELECT
				brc.BookReportCommentID,
				brc.Comment,
				brc.DateInput,
				brc.DateModified,
				brc.InputBy,
				brc.LastModifiedBy,
				$NameFieldInputBy NameLang,
				iu.UserID, 
				iu.PersonalPhotoLink,
				iu.RecordType
			FROM 
				READING_GARDEN_BOOK_REPORT_COMMENT brc 
				INNER JOIN INTRANET_USER iu ON iu.UserID = brc.UserID
			WHERE
				brc.RecordStatus = ".RECORD_STATUS_ACTIVE."
				AND brc.BookReportID = '$BookReportID'
		";
		
		return $sql;
	
	}
	
	function Get_Book_Report_Comment_List($BookReportID='')
	{
		if($BookReportID)
			$cond_BookReportID = " AND brc.BookReportID IN (".implode(",",(array)$BookReportID).") ";
		
		$sql = "
			SELECT
				brc.BookReportCommentID,
				brc.BookReportID,
				brc.Comment,
				brc.DateInput,
				brc.DateModified,
				brc.InputBy,
				brc.LastModifiedBy
			FROM 
				READING_GARDEN_BOOK_REPORT_COMMENT brc 
			WHERE
				brc.RecordStatus = ".RECORD_STATUS_ACTIVE."
				$cond_BookReportID
		";
		
		$Result= $this->returnArray($sql);
		
		return $Result;
	}
	
	// 20110511
	function Get_Award_Scheme_Winner_List($AwardSchemeID, $AcademicYearID='')
	{
		if(trim($AcademicYearID)=='')
			$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$sql="
			SELECT
				asw.AwardSchemeID,
				asw.WinnerID,
				asw.AwardLevelID,
				asl.LevelName,
				asw.AcademicYearID,
				asw.DateInput,
				asw.InputBy,
				asl.AwardType
			FROM 
				READING_GARDEN_AWARD_SCHEME_WINNER asw
				INNER JOIN READING_GARDEN_AWARD_SCHEME_LEVEL asl ON asw.AwardLevelID = asl.AwardLevelID
			WHERE
				asw.AcademicYearID = '$AcademicYearID'
				AND asw.AwardSchemeID = '$AwardSchemeID'
			ORDER BY
				asl.AwardType ASC, asl.Level ASC
				
		";
		
		$Result = $this->returnArray($sql);

		return $Result;
	}
	
	function Get_Reading_Report_Count_In_Award_Scheme_Period($AwardSchemeID)
	{
		# global val used to save duplicated sql called in Settings_Get_Award_Scheme_Generate_Winner_DBTable_Param
		global $Global_fcm, $Global_AwardScheme, $Global_AwardRequirementList, $Global_AwardTargetClass, $Global_StudentInfoArr,  $Global_ReadingRecordArr;
		if(!$fcm = $Global_fcm)
		{ 
			include_once("form_class_manage.php");
			$fcm = new form_class_manage();
		}
		
		if(!$AwardScheme = $Global_AwardScheme)
		{
			$AwardScheme = $this->Get_Award_Scheme_List($AwardSchemeID);
		}
		
		if(!$AwardRequirementList = $Global_AwardRequirementList)
		{
			$AwardRequirementList = $this->Get_Award_Scheme_Requirement($AwardSchemeID);
		}
		$CategoryIDArr = Get_Array_By_Key($AwardRequirementList,"CategoryID");

		# Target Class
		if(!$AwardTargetClass = $Global_AwardTargetClass)
		{
			$AwardTargetClass = $this->Get_Award_Scheme_Target_Class($AwardSchemeID);
		}
		$ClassIDArr = Get_Array_By_Key($AwardTargetClass,"ClassID");
		
		# Student List in target Class
		if(!$StudentInfoArr = $Global_StudentInfoArr)
		{
			$StudentInfoArr = $fcm->Get_Student_By_Class($ClassIDArr);
		}
		$StudentIDArr = Get_Array_By_Key($StudentInfoArr, "UserID");
		
		# Reading Record of Target Student
		if(!$ReadingRecordArr = $Global_ReadingRecordArr)
		{
			if($this->Settings['RequireSelectAwardScheme'])
			{
				$ReadingRecordAwardSchemeID = $AwardSchemeID;
			}
			else
			{
				$StartDate = $AwardScheme[0]['StartDate'];
				$EndDate = $AwardScheme[0]['EndDate'];
				$Language = $AwardScheme[0]['Language'];
			}			
			$ReadingRecordArr = $this->Get_Student_Reading_Record_And_Report_Grade($StudentIDArr,'','',$StartDate, $EndDate, $Language, "", "", "", $ReadingRecordAwardSchemeID);
		}
		$StudentReadingRecord = BuildMultiKeyAssoc($ReadingRecordArr, "UserID",'','',1); // $ary[$UserID]= array($ReadingRecord1,$ReadingRecord2...)
		
		# Class Student Mapping
		$ClassStudentAssoc = BuildMultiKeyAssoc($StudentInfoArr,array("YearClassID"),"UserID",1,1); //$ClassStudentAssoc[$ClassID] = array($StudentID1, $StudentID2,.... )

		# PartyID is StudentID or ClassID, use to group reading record by studetn or class,  according to the award type
		$PartyID = $AwardScheme[0]['AwardType']== AWARD_SCHEME_TYPE_INDIVIDUAL?"thisStudentID":"thisClassID"; 

		$ReturnArr = array();
		foreach((array)$ClassStudentAssoc as $thisClassID => $thisStudentIDArr) // loop class
		{
			foreach((array)$thisStudentIDArr as $thisStudentID)// loop student
			{
				foreach((array)$StudentReadingRecord[$thisStudentID] as $thisStudentReadingRecord) // count class overall reading record and report 
				{
					//Reading Record Count
					$ReturnArr[${$PartyID}]['RecordCount']++;
					
					//Book Report Count					
					if($thisStudentReadingRecord['BookReportID']>0)
					{
						$ReturnArr[${$PartyID}]['BookReportCount']++;
					}
					
					// Count Reading Record by Category
					$thisCategoryID = $thisStudentReadingRecord['CategoryID']?$thisStudentReadingRecord['CategoryID']:0;
					$ReturnArr[${$PartyID}]['BookCatCount'][$thisCategoryID]++;
				}
			}// end loop student
		}  // end loop class
		
		return $ReturnArr;
	}
	
	function Get_Announcement_List_DBTable_Sql($ClassID, $AnnouncementType='', $Keyword='')
	{
		$NameFieldPostedBy = getNameFieldByLang("iu.");
		
		if(trim($AnnouncementType)!='')
		{
			if($AnnouncementType == ANNOUNCEMENT_TYPE_ANNOUNCEMENT)
				$cond_AnnounceType = " AND a.AwardSchemeID <= 0 ";
			else
				$cond_AnnounceType = " AND (a.AwardSchemeID > 0)";
		}
		
		if(trim($Keyword)!='')
		{
			$cond_Keyword = " 
				AND 
				(
					a.Title LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%'
					OR $NameFieldPostedBy LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%'
					OR StartDate LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' 
				)
			";
			
		}
		
		$sql = "
			SELECT
				IF(
					@IsAward:= IF(a.AwardSchemeID>0,1,0),
					CONCAT('<a class=\"list_title\" href=\"javascript:void(0);\" onclick=\"js_View_Award(\'',a.AwardSchemeID,'\');\">',a.Title,'</a>'),
					CONCAT('<a class=\"list_title\" href=\"javascript:void(0);\" onclick=\"js_View_Announcement(\'',a.AnnouncementID,'\');\">',a.Title,'</a>')
				),
				$NameFieldPostedBy as NameField,
				DATE(a.StartDate) StartDate,
				IF(@IsAward,'award_row','announcement_row') as trCustClass
			FROM
				READING_GARDEN_ANNOUNCEMENT a
				LEFT JOIN READING_GARDEN_ANNOUNCEMENT_TARGET_CLASS atc ON a.AnnouncementID = atc.AnnouncementID
				LEFT JOIN INTRANET_USER iu ON iu.UserID = a.InputBy
			WHERE
				a.RecordStatus = ".RECORD_STATUS_ACTIVE."
				AND atc.ClassID = '$ClassID'
				$cond_AnnounceType
				$cond_Keyword
		";
		
		return $sql;
	}

	//20110511
	function Settings_Get_Award_Scheme_Generate_Winner_DBTable_Param($AwardSchemeID, $WinnerStatus='', $AwardLevelID='')
	{
		global $Global_fcm, $Global_AwardScheme, $Global_AwardRequirementList, $Global_AwardTargetClass, $Global_StudentInfoArr,  $Global_ReadingRecordArr;
		include_once("form_class_manage.php");
		$fcm = new form_class_manage();
		$Global_fcm = $fcm;
		
		$AwardScheme = $this->Get_Award_Scheme_List($AwardSchemeID);
		$Global_AwardScheme = $AwardScheme;
		
		$AwardRequirementList = $this->Get_Award_Scheme_Requirement('', false, $AwardSchemeID);
		
		$Global_AwardRequirementList = $AwardRequirementList;
		$LevelCatRequirementList = BuildMultiKeyAssoc($AwardRequirementList, array("AwardType","AwardLevelID","CategoryID")); //$AwardRequirementList[$AwardLevelID] = ...
		$CategoryIDArr = array_remove_empty(array_unique(Get_Array_By_Key($AwardRequirementList,"CategoryID")));

		# Target Class
		$AwardTargetClass = $this->Get_Award_Scheme_Target_Class($AwardSchemeID);
		$Global_AwardTargetClass = $AwardTargetClass;
		$ClassIDArr = Get_Array_By_Key($AwardTargetClass,"ClassID");
		
		# Student List in target Class
		$StudentInfoArr = $fcm->Get_Student_By_Class($ClassIDArr);
		$Global_StudentInfoArr = $StudentInfoArr;
		$StudentIDArr = Get_Array_By_Key($StudentInfoArr, "UserID");
		
		# Reading Record of Target Student
		if($this->Settings['RequireSelectAwardScheme'])
		{
			$ReadingRecordAwardSchemeID = $AwardSchemeID;
		}
		else
		{
			$StartDate = $AwardScheme[0]['StartDate'];
			$EndDate = $AwardScheme[0]['EndDate'];
			$Language = $AwardScheme[0]['Language'];
		}
		
		$ReadingRecordArr = $this->Get_Student_Reading_Record_And_Report_Grade($StudentIDArr,'','',$StartDate,$EndDate, $Language, "", "", "", $ReadingRecordAwardSchemeID);
		$Global_ReadingRecordArr = $ReadingRecordArr;
		$StudentReadingRecord = BuildMultiKeyAssoc($ReadingRecordArr, "UserID",'','',1); // $ary[$UserID]= array($ReadingRecord1,$ReadingRecord2...)
		
		# Class Student Mapping
		$ReadingReportCount = $this->Get_Reading_Report_Count_In_Award_Scheme_Period($AwardSchemeID);
		
		foreach((array)$ReadingReportCount as $thisPartyID => $thisReadingReportCount) // loop class or student
		{
			// student can get more than 1 additional award
			if(count($LevelCatRequirementList[AWARD_TYPE_ADDITIONAL])>0) 
			{
				foreach($LevelCatRequirementList[AWARD_TYPE_ADDITIONAL] as $thisAwardLevelID => $thisLevelRequirement)
				{
					$ArrFirstElement = current($thisLevelRequirement);
					if($thisReadingReportCount['RecordCount'] < $ArrFirstElement['LevelMinBookRead']) // check MinBookRead 
						continue;	// failed, skip the remaining checking for this level 
					
					if($thisReadingReportCount['BookReportCount'] < $ArrFirstElement['MinReportSubmit']) // check MinReportSubmit
						continue;	// failed, skip the remaining checking for this level
				
					if(!empty($ArrFirstElement['CategoryID'])) // if no category requirement 
					{				
						foreach((array)$thisLevelRequirement as $RequirementDetail)
						{
							$RequireCatID = $RequirementDetail['CategoryID'];
//							$CategoryIDArr[] = $RequireCatID;
							$CatMinBookRead = $RequirementDetail['MinBookRead'];
							
							if($thisReadingReportCount['BookCatCount'][$RequireCatID] < $CatMinBookRead)
							{
								continue 2; // skip the remaining checking for this level
							}
						}
					}
					
					$PartyAwardList[$thisPartyID][] = $thisAwardLevelID;
				
				}
			}
			
			// student can only get 1 of the general award level
			if(count($LevelCatRequirementList[AWARD_TYPE_GENERAL])>0) 
			{
				foreach($LevelCatRequirementList[AWARD_TYPE_GENERAL] as $thisAwardLevelID => $thisLevelRequirement)
				{
					$ArrFirstElement = current($thisLevelRequirement);
					if($thisReadingReportCount['RecordCount'] < $ArrFirstElement['LevelMinBookRead']) // check MinBookRead 
						continue;	// failed, skip the remaining checking for this level 
					
					if($thisReadingReportCount['BookReportCount'] < $ArrFirstElement['MinReportSubmit']) // check MinReportSubmit
						continue;	// failed, skip the remaining checking for this level
				
					if(!empty($ArrFirstElement['CategoryID'])) // if no category requirement 
					{				
						foreach((array)$thisLevelRequirement as $RequirementDetail)
						{
							$RequireCatID = $RequirementDetail['CategoryID'];
//							$CategoryIDArr[] = $RequireCatID;
							$CatMinBookRead = $RequirementDetail['MinBookRead'];
							
							if($thisReadingReportCount['BookCatCount'][$RequireCatID] < $CatMinBookRead)
							{
								continue 2; // skip the remaining checking for this level
							}
						}
					}
					
					// if pass all checking, assign AwardLevelID to the student 
//					$MeetRequirementParty[] = $thisPartyID;
					$PartyAwardList[$thisPartyID][] = $thisAwardLevelID;
//					$CategoryIDArr = array_unique($CategoryIDArr);
					break; // skip checking the remaining level
				
				}
			}
			if(empty($PartyAwardList[$thisPartyID])) $PartyAwardList[$thisPartyID][] = 0;
		}	
		
		$MeetRequirementParty = array_keys($ReadingReportCount);
		
		if($AwardScheme[0]['AwardType']== AWARD_SCHEME_TYPE_INDIVIDUAL)
		{
			$this->Cache_Student_ClassInfo($MeetRequirementParty);
			foreach((array)$MeetRequirementParty as $thisStudentID)
			{
				foreach((array)$PartyAwardList[$thisStudentID] as $thisAwardLevelID)
				{
					$RowArr = array();
					$ClassInfo = $this->Get_Student_ClassInfo($thisStudentID);
					$RowArr['TempID'] = $thisStudentID;
					$RowArr['Class'] = Get_Lang_Selection($ClassInfo[0]['ClassTitleB5'],$ClassInfo[0]['ClassTitleEN']);
					$RowArr['ClassNumber'] = $ClassInfo[0]['ClassNumber'];
					$RowArr['StudentName'] = Get_Lang_Selection($ClassInfo[0]['ChineseName'],$ClassInfo[0]['EnglishName']);
					$RowArr['BookRead'] = $ReadingReportCount[$thisStudentID]['RecordCount'];
					$RowArr['ReportSubmit'] = $ReadingReportCount[$thisStudentID]['BookReportCount'];
					
					foreach((array)$CategoryIDArr as $thisCategoryID)
					{
						$RowArr[$thisCategoryID] = $ReadingReportCount[$thisStudentID]['BookCatCount'][$thisCategoryID];
					}
					
					$RowArr['AwardLevelID'] = $thisAwardLevelID;
	//				$RowArr['AdditionalAwardID'] = $PartyAdditionalAwardList[$thisStudentID];
					$ValueArr[] = $RowArr;
				}
				
			}
		
//			$CategoryIDArr = Get_Array_By_Key($AwardRequirementList,"CategoryID"); 
			$DBTableSql = $this->Create_Temp_Table_For_Generate_Winner_Student($AwardSchemeID, $CategoryIDArr,$ValueArr,$WinnerStatus, $AwardLevelID);
		}
		else
		{
			foreach((array)$MeetRequirementParty as $thisClassID)
			{
				$year_class = new year_class($thisClassID);	
				foreach((array)$PartyAwardList[$thisClassID] as $thisAwardLevelID)
				{
					$RowArr['TempID'] = $thisClassID;
					$RowArr['Class'] = Get_Lang_Selection($year_class->ClassTitleB5,$year_class->ClassTitleEN);
					$RowArr['BookRead'] = $ReadingReportCount[$thisClassID]['RecordCount'];
					$RowArr['ReportSubmit'] = $ReadingReportCount[$thisClassID]['BookReportCount'];
				
					foreach((array)$CategoryIDArr as $thisCategoryID)
					{
						$RowArr[$thisCategoryID] = $ReadingReportCount[$thisClassID]['BookCatCount'][$thisCategoryID];
					}
	
					$RowArr['AwardLevelID'] = $thisAwardLevelID;
	//				$RowArr['AdditionalAwardID'] = $PartyAdditionalAwardList[$thisClassID];
					$ValueArr[] = $RowArr;
				}
			}
			
//			$CategoryIDArr = Get_Array_By_Key($AwardRequirementList,"CategoryID");

			$DBTableSql = $this->Create_Temp_Table_For_Generate_Winner_Class($AwardSchemeID, $CategoryIDArr,$ValueArr,$WinnerStatus, $AwardLevelID);
		}
		
		$DBTableSql['AwardList'] = $PartyAwardList;
		
		return $DBTableSql;
	}
	
	//20110511	
	function Create_Temp_Table_For_Generate_Winner_Student($AwardSchemeID, $CategoryIDArr, $ValueArr,$WinnerStatus='', $AwardLevelID='')
	{
		global $Lang;
		
		if(count($CategoryIDArr)>0)
		{
			foreach((array)$CategoryIDArr as $CategoryID)
			{
				$CreateCatSql .= "C$CategoryID int(11), ";
				$CatFieldArr[] = "C$CategoryID ";
			}
			$CatSql = ", ".implode(',',$CatFieldArr);
		}
		
		$sql = "
			CREATE TEMPORARY TABLE IF NOT EXISTS READING_GARDEN_TEMP_GENERATE_STUDENT_WINNER (
				TempID int(11) AUTO_INCREMENT,
				PartyID int(11),				
				Class text,
				ClassNumber int(11),
				StudentName text,
				BookRead int(11),
				ReportSubmit int(11),
				$CreateCatSql
				AwardLevelID int(11),
				PRIMARY KEY (TempID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		";
		$Success["Create"] = $this->db_db_query($sql);
		
		foreach((array)$ValueArr as $thisFieldArr)
		{
			$ValueSqlArr[] = "( '".(implode("','",$thisFieldArr))."') ";
		}
		$ValueSql = implode(",",(array)$ValueSqlArr);
		
		$InsertSql = " 
			INSERT INTO READING_GARDEN_TEMP_GENERATE_STUDENT_WINNER
				(PartyID, Class, ClassNumber, StudentName, BookRead, ReportSubmit $CatSql, AwardLevelID)
			VALUES
				 $ValueSql
		";
		
		$Success["Insert"] = $this->db_db_query($InsertSql);

		if(trim($WinnerStatus)!='')
		{
			if($WinnerStatus==1)
			{
				$cond_WinnerStatus = " AND WinnerID IS NOT NULL ";	
			}
			else
			{
				$cond_WinnerStatus = " AND WinnerID IS NULL ";
			}
		}
		
		if(trim($AwardLevelID)!='')
			$cond_AwardLevelID = " AND tmp.AwardLevelID = '$AwardLevelID' ";
		
		$DBTableSql = "
			SELECT
				Class, 
				ClassNumber, 
				StudentName, 
				BookRead, 
				ReportSubmit 
				$CatSql,
				l.LevelName as GenerateLevel,
				IF(asw.WinnerID IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as IsWinner,
				CONCAT('<input type=\"checkbox\" name=\"WinnerID[]\" value=\"',PartyID,',',tmp.AwardLevelID,'\">')
			FROM
				READING_GARDEN_TEMP_GENERATE_STUDENT_WINNER tmp
				LEFT JOIN READING_GARDEN_AWARD_SCHEME_WINNER asw ON PartyID = asw.WinnerID AND asw.AwardSchemeID = ".$AwardSchemeID." AND asw.AwardLevelID = tmp.AwardLevelID
				LEFT JOIN READING_GARDEN_AWARD_SCHEME_LEVEL l ON tmp.AwardLevelID = l.AwardLevelID
			WHERE
				1
				$cond_WinnerStatus
				$cond_AwardLevelID
		";

		$Result = $this->returnArray($DBTableSql);

		$DBTablePar['sql'] = $DBTableSql;
		$DBTablePar['CategoryArr'] = $CategoryIDArr;
		$DBTablePar['AwardType'] = AWARD_SCHEME_TYPE_INDIVIDUAL;

		return $DBTablePar;		
	}
	
	//20110511
	function Create_Temp_Table_For_Generate_Winner_Class($AwardSchemeID, $CategoryIDArr, $ValueArr,$WinnerStatus='', $AwardLevelID='')
	{
		global $Lang;
		if(count($CategoryIDArr)>0)
		{
			foreach((array)$CategoryIDArr as $CategoryID)
			{
				$CreateCatSql .= "C$CategoryID int(11), ";
				$CatFieldArr[] = "C$CategoryID ";
			}
			$CatSql = ", ".implode(',',$CatFieldArr);
		}
		
		$sql = "
			CREATE TEMPORARY TABLE IF NOT EXISTS READING_GARDEN_TEMP_GENERATE_CLASS_WINNER (
				TempID int(11) AUTO_INCREMENT,
				PartyID int(11),			
				Class text,
				BookRead int(11),
				ReportSubmit int(11),
				$CreateCatSql
				AwardLevelID int(11),				
				PRIMARY KEY (TempID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		";
		$Success["Create"] = $this->db_db_query($sql);
		
		foreach((array)$ValueArr as $thisFieldArr)
		{
			$ValueSqlArr[] = "( '".(implode("','",$thisFieldArr))."') ";
		}
		$ValueSql = implode(",",(array)$ValueSqlArr);
		
		$InsertSql = " 
			INSERT INTO READING_GARDEN_TEMP_GENERATE_CLASS_WINNER
				(PartyID, Class, BookRead, ReportSubmit $CatSql , AwardLevelID)
			VALUES
				 $ValueSql
		";
		
		$Success["Insert"] = $this->db_db_query($InsertSql);
		
		if(trim($WinnerStatus)!='')
		{
			if($WinnerStatus==1)
			{
				$cond_WinnerStatus = " AND WinnerID IS NOT NULL ";	
			}
			else
			{
				$cond_WinnerStatus = " AND WinnerID IS NULL ";
			}
		}

		if(trim($AwardLevelID)!='')
			$cond_AwardLevelID = " AND tmp.AwardLevelID = '$AwardLevelID' ";
		
		$DBTableSql = "
			SELECT
				Class, 
				BookRead, 
				ReportSubmit 
				$CatSql,
				l.LevelName as GenerateLevel,
				IF(asw.WinnerID IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as IsWinner,
				CONCAT('<input type=\"checkbox\" name=\"WinnerID[]\" value=\"',PartyID,',',tmp.AwardLevelID,'\">')
			FROM
				READING_GARDEN_TEMP_GENERATE_CLASS_WINNER tmp
				LEFT JOIN READING_GARDEN_AWARD_SCHEME_WINNER asw ON PartyID = asw.WinnerID AND asw.AwardSchemeID = ".$AwardSchemeID." AND asw.AwardLevelID = tmp.AwardLevelID 
				LEFT JOIN READING_GARDEN_AWARD_SCHEME_LEVEL l ON tmp.AwardLevelID = l.AwardLevelID
			WHERE
				1
				$cond_WinnerStatus
				$cond_AwardLevelID
		";

		$Result = $this->returnArray($DBTableSql);

		$DBTablePar['sql'] = $DBTableSql;
		$DBTablePar['CategoryArr'] = $CategoryIDArr;
		$DBTablePar['AwardType'] = AWARD_SCHEME_TYPE_GROUP;

		return $DBTablePar;		
	}
	
	//20110511
	function Settings_Manage_Award_Scheme_Winner($Action, $AwardSchemeID, $AwardList='')
	{
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
		switch(strtolower($Action))
		{
			case "assign":
				if(!$AwardList)
					return false;
			
				$ExistingList = $this->Get_Award_Scheme_Winner_List($AwardSchemeID);
				$ExistingList = BuildMultiKeyAssoc($ExistingList, "WinnerID", "AwardLevelID",1 ,1);
				
				foreach((array) $AwardList as $thisWinnerID => $AwardArr)
				{
					foreach((array)$AwardArr as $thisAwardLevelID)
					{
						if(!in_array($thisAwardLevelID, (array)$ExistingList[$thisWinnerID]))
							$ValueSqlArr[] = " ('$AwardSchemeID', '$thisWinnerID', '".$thisAwardLevelID."', '$AcademicYearID', NOW(), '".$_SESSION['UserID']."' ) ";
					}
				}
				if(count($ValueSqlArr)>0)
				{
					$ValueSql = implode(",",$ValueSqlArr);
					$sql = "
						INSERT INTO READING_GARDEN_AWARD_SCHEME_WINNER
							(AwardSchemeID, WinnerID, AwardLevelID, AcademicYearID, DateInput, InputBy)
						VALUES
							$ValueSql
					";
					
					$Success['Insert'] = $this->db_db_query($sql);
				}

			break;
			case "unassign":
				
				if($AwardList)
				{
					foreach((array) $AwardList as $thisWinnerID => $AwardArr)
					{
						$cond_WinnerArr[] = " (WinnerID = '$thisWinnerID' AND AwardLevelID IN (".implode(",",$AwardArr).") ) ";
//						foreach((array)$AwardArr as $thisAwardLevelID)
//						{
//							$cond_WinnerArr[] = " (WinnerID = '$thisWinnerID' AND AwardLevelID = '$thisAwardLevelID' ) ";
//						}
					}
					$cond_Winner = " AND (".implode(" OR ",(array)$cond_WinnerArr).")";
					
				}

				$sql = "
					DELETE FROM
						READING_GARDEN_AWARD_SCHEME_WINNER
					WHERE
						AcademicYearID = '$AcademicYearID'
						AND AwardSchemeID = '$AwardSchemeID'
						$cond_Winner
				";
				
				$Success['Delete'] = $this->db_db_query($sql);
			break;
		} 
		
		if(in_array(false,(array)$Success))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// 20110511
	function Get_Award_Scheme_Level_Info($AwardSchemeID='' ,$AwardLevelID='')
	{
		if($AwardSchemeID)
			$cond_AwardSchemeID = " AND aws.AwardSchemeID IN (".implode(",",(array)$AwardSchemeID).")"; 
		if($AwardLevelID)
			$cond_AwardLevelID = " AND awsl.AwardLevelID IN (".implode(",",(array)$AwardLevelID).")"; 
		
		$sql = "
			SELECT 
				* 
			FROM 
				READING_GARDEN_AWARD_SCHEME aws
				INNER JOIN READING_GARDEN_AWARD_SCHEME_LEVEL  ON aws.AwardSchemeID = awsl.AwardSchemeID
			WHERE
				1
				$cond_AwardSchemeID	
				$cond_AwardLevelID
		";
		
		$Result = $this->returnArray($sql);
		
		return $Result ;
		
	}
	
	/***********************************Marcus Part End ***********************************/
	
	/********************************* Scoring Action Start **********************************/
	function Scoring_Action_Login_Duration()
	{
		if(!$_SESSION['READING_SCHEME_SESSION']['ReceivedLoginScore'] && $this->Settings[ACTION_SETTING_LOGIN_MORE_THAN]) // skip scoring process if Login Score has been given in the same day
		{
			$LoginDuration = $_SESSION['READING_SCHEME_SESSION']['LoginDuration'];
			$LoginDuration += time() - $_SESSION['READING_SCHEME_SESSION']['LoginTime'];
			
			if(floor($LoginDuration/60) >= $this->Settings[ACTION_SETTING_LOGIN_MORE_THAN])
			{
				$Success = $this->Update_State_Score_Action(ACTION_SCORE_LOGIN);
				$_SESSION['READING_SCHEME_SESSION']['ReceivedLoginScore'] = $Success;
			}
		}
	}
	
	/********************************* Scoring Action End **********************************/
	
	/********************************* Book Report Marking ******************************************/
	function Mark_Book_Report($BookReportID,$ReadingRecordID,$UpdateData,$StudentID='',$OldRecommend='')
	{
		// Need either BookReportID or ReadingRecordID is available
		// UpdateData : array - key as DB field name, value as DB field value 
		$Result = array();
		if(sizeof($UpdateData)>0){
			$UpdateSql = "";
			foreach((array)$UpdateData as $FieldName => $FieldValue){
				if($FieldName=='StickerIDToAdd' || $FieldName=='StickerIDToRemove') continue;
				if($UpdateSql != "") $UpdateSql .= ",";
				$UpdateSql .= "$FieldName = '".$this->Get_Safe_Sql_Query($FieldValue)."' ";
			}
			$sql = "UPDATE READING_GARDEN_BOOK_REPORT 
					SET 
						$UpdateSql
						,DateModified = NOW()
						,LastModifiedBy = NOW()
					WHERE 
						RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
			if(trim($BookReportID)!=''){
				$sql .= " AND BookReportID = '".$BookReportID."' ";
			}
			if(trim($ReadingRecordID)!=''){
				$sql .= " AND ReadingRecordID = '".$ReadingRecordID."' ";
			}
			$Result['UpdateReport'] = $this->db_db_query($sql);
			if($Result['UpdateReport'] && sizeof($UpdateData['StickerIDToAdd'])>0){
				$Result['AddSticker'] = $this->Manage_Report_Sticker_Mappings("add",$BookReportID,$UpdateData['StickerIDToAdd']);
			}
			if($Result['UpdateReport'] && sizeof($UpdateData['StickerIDToRemove'])>0){
				$Result['RemoveSticker'] = $this->Manage_Report_Sticker_Mappings("remove",$BookReportID,$UpdateData['StickerIDToRemove']);
			}
			// Update Student State Score for Recommended Report
			if($Result['UpdateReport'] && $StudentID!=''){
				if($OldRecommend==1 && $UpdateData['Recommend']!=1){
					// Recommended before, now undo recommend, -1
					$this->Update_State_Score_Action(ACTION_SCORE_RECOMMEND_REPORT,-1,$StudentID);
				}else if($OldRecommend!=1 && $UpdateData['Recommend']==1){
					// Not recommended before, now recommend, +1
					$this->Update_State_Score_Action(ACTION_SCORE_RECOMMEND_REPORT,'',$StudentID);
				}
			}
			return !in_array(false,$Result);
		}
		
		return false;
	}
	
	function Get_Single_Student_Reading_Record_Book_Report_List($StudentID,$AcademicYearID='',$ShowRecommendOnly=0)
	{
//		$AcademicYearID = $AcademicYearID==""?Get_Current_Academic_Year_ID():$AcademicYearID;
		if(trim($AcademicYearID)!='') 
			$cond_AcademicYearID = " rr.AcademicYearID = '".$AcademicYearID."' ";
		
		$NameField = getNameFieldWithClassNumberByLang('u.');
		
		$sql = "SELECT 
					u.UserID, 
					$NameField as UserName,
					b.BookName,
					rr.ReadingRecordID, 
					br.BookReportID 
				FROM 
					READING_GARDEN_READING_RECORD as rr 
					INNER JOIN READING_GARDEN_BOOK_REPORT as br ON br.ReadingRecordID = rr.ReadingRecordID and br.RecordStatus = '".RECORD_STATUS_ACTIVE."' 
					INNER JOIN READING_GARDEN_BOOK as b ON rr.BookID = b.BookID AND b.RecordStatus = '".RECORD_STATUS_ACTIVE."'
					INNER JOIN INTRANET_USER as u ON u.UserID = rr.UserID 
				WHERE 
					rr.UserID = '".$StudentID."'
					$cond_AcademicYearID 
					AND rr.RecordStatus = '".RECORD_STATUS_ACTIVE."' 
					AND (
						(br.Attachment IS NOT NULL AND br.Attachment <> '') OR
						(br.AnswerSheet IS NOT NULL AND br.AnswerSheet <> '') OR
						(br.OnlineWriting IS NOT NULL AND br.OnlineWriting <> '') 
					)";
		if($ShowRecommendOnly==1){
			$sql .= "AND br.Recommend IS NOT NULL 
					AND br.Recommend = 1 ";
		}
		$sql .= "ORDER BY 
					b.BookName 
				";
		
		return $this->returnArray($sql);
	}
	/************************************* End Book Report Marking **************************************/
	
	/**************************************** Start of Forum Functions ************************************************/
	function Get_Management_Forum_List($ForumID='',$Keyword='')
	{
		$NameFieldModifiedBy = getNameFieldByLang("u.");
		$YearClassName = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
		
		$sql = "SELECT 
					f.ForumID,
					f.ForumName,
					f.Description,
					f.TopicNeedApprove,
					f.AllowAttachment,
					GROUP_CONCAT( $YearClassName ORDER BY y.Sequence, yc.Sequence ASC SEPARATOR ', ') AS TargetClass,
					f.DisplayOrder,
					f.DateModified,
					$NameFieldModifiedBy as LastModifiedBy 
				FROM 
					READING_GARDEN_FORUM as f 
					LEFT JOIN READING_GARDEN_FORUM_TARGET_CLASS as ftc ON ftc.ForumID = f.ForumID 
					LEFT JOIN YEAR_CLASS yc ON ftc.ClassID = yc.YearClassID 
					LEFT JOIN YEAR y ON y.YearID = yc.YearID 
					LEFT JOIN INTRANET_USER as u ON u.UserID = f.LastModifiedBy 
				WHERE 
					f.RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
		if($ForumID!='')
			$sql .= "AND f.ForumID = '$ForumID' ";
		if($Keyword!=''){
			$Keyword = $this->Get_Safe_Sql_Like_Query(trim($Keyword));
			$sql .= "AND (
						f.ForumName LIKE '%$Keyword%'
						OR f.Description LIKE '%$Keyword%'
						OR f.DateModified LIKE '%$Keyword%'
						OR yc.ClassTitleB5 LIKE '%$Keyword%'
						OR yc.ClassTitleEN LIKE '%$Keyword%'
						OR $NameFieldModifiedBy LIKE '%$Keyword%'
					)";
		}
		$sql .= "GROUP BY 
					f.ForumID 
				ORDER BY f.DisplayOrder, f.ForumName ";
		
		return $this->returnArray($sql);
	}
	
	function Manage_Forum_Record($Action,$ForumData)
	{
		$Action = strtolower($Action);
		$ForumID = $ForumData['ForumID'];
		$ForumName = $this->Get_Safe_Sql_Query($ForumData['ForumName']);
		$Description = $this->Get_Safe_Sql_Query($ForumData['Description']);
		$TopicNeedApprove = $ForumData['TopicNeedApprove'];
		$AllowAttachment = $ForumData['AllowAttachment'];
		$ClassIDArr = $ForumData['ClassID'];
		
		$Result = array();
		$this->Start_Trans();
		if($Action=="add")
		{
			$DisplayOrder = $this->Management_Get_Max_Forum_Display_Order() + 1;
			$sql = "INSERT INTO READING_GARDEN_FORUM 
						(ForumName,Description,DisplayOrder,TopicNeedApprove,AllowAttachment,RecordStatus,DateInput,DateModified,InputBy,LastModifiedBy)
						VALUES('$ForumName','$Description','$DisplayOrder','$TopicNeedApprove','$AllowAttachment','".RECORD_STATUS_ACTIVE."',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
						
			$Result['AddForum'] = $this->db_db_query($sql);
			if($Result['AddForum']){
				$ForumID = $this->db_insert_id();
				$ClassForumData['ForumID'] = $ForumID;
				$ClassForumData['ClassID'] = $ClassIDArr;
				$Result['AssignClass'] = $this->Assign_Forum_To_Class($ClassForumData);
			}
		}else if($Action=="edit")
		{
			$sql = "UPDATE READING_GARDEN_FORUM SET 
						ForumName = '$ForumName',
						Description = '$Description',
						TopicNeedApprove = '$TopicNeedApprove',
						AllowAttachment = '$AllowAttachment',
						DateModified = NOW(),
						LastModifiedBy = '".$_SESSION['UserID']."'
					WHERE ForumID = '$ForumID' ";
			$Result['UpdateForum'] = $this->db_db_query($sql);
			if($Result['UpdateForum']){
				$Result['UpdateClass'] = $this->Update_Forum_Class_Mapping($ForumID,$ClassIDArr);
			}
		}else if($Action=="delete")
		{
			$sql = "UPDATE READING_GARDEN_FORUM SET RecordStatus = '".RECORD_STATUS_DELETED."' WHERE ForumID IN (".implode(",",(array)$ForumID).") ";
			$Result['DeleteForum'] = $this->db_db_query($sql);
			if($Result['DeleteForum']){
				$Result['RemoveClass'] = $this->Remove_Forum_From_Class($ForumID);
			}
		}
		
		if(in_array(false,$Result))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	function Assign_Forum_To_Class($ClassForumData)
	{
		$ClassIDArr = $ClassForumData['ClassID'];
		$ForumID = $ClassForumData['ForumID'];
		
		if(empty($ClassIDArr) || empty($ForumID)) return false;
		
		foreach((array)$ClassIDArr as $ClassID)
		{
			$ValueArr[] = "
				(
					'$ForumID',
					'$ClassID',
					NOW(),
					'".$_SESSION['UserID']."'
				)
			";
		}
		if(count($ValueArr)==0) return true;
		
		$ValueSql = implode(",",$ValueArr);
		
		$sql = "
			INSERT INTO READING_GARDEN_FORUM_TARGET_CLASS
				(ForumID, ClassID, DateInput, InputBy)
			VALUES
				$ValueSql
		";
		
		$Success = $this->db_db_query($sql) or die($sql.mysql_error());
		
		return $Success;
	}
	
	function Remove_Forum_From_Class($ForumID, $ClassID='')
	{
		if($ClassID)
			$cond_ClassID = " AND ClassID IN (".implode(",",(array)$ClassID).") ";
		$sql = "
			DELETE FROM
				READING_GARDEN_FORUM_TARGET_CLASS
			WHERE
				ForumID IN (".implode(",",(array)$ForumID).")
				$cond_ClassID
		";
		
		$Success = $this->db_db_query($sql);

		return $Success;
	}
	
	function Update_Forum_Class_Mapping($ForumID,$ClassIDArr)
	{
		$ClassForumMapping = $this->Get_Class_Forum('',$ForumID);
		$ClassList = Get_Array_By_Key($ClassForumMapping,"ClassID");
		
		$DeleteClassList = array_diff((array)$ClassList,(array)$ClassIDArr);
		$AddClassList = array_diff((array)$ClassIDArr,(array)$ClassList);
		if($DeleteClassList)
			$Success['Delete'] = $this->Remove_Forum_From_Class($ForumID, $DeleteClassList);
		
		if($AddClassList)
		{
			$ParArr['ForumID'] = $ForumID;
			$ParArr['ClassID'] = $AddClassList;
			$Success["Assign"] = $this->Assign_Forum_To_Class($ParArr);
		}
		return !in_array(false,(array)$Success);
	}
	
	function Get_Class_Forum($ClassIDArr='', $ForumIDArr='')
	{
//		if(trim($ForumIDArr)!= '' && sizeof((array)$ForumIDArr)>0)
		if($ForumIDArr)
			$cond_ForumID = " AND f.ForumID IN (".implode(",",(array)$ForumIDArr).") ";
			
//		if(trim($ClassIDArr)!= '' && sizeof((array)$ClassIDArr)>0)
		if($ClassIDArr)
			$cond_ClassID = " AND ftc.ClassID IN (".implode(",",(array)$ClassIDArr).") ";
		
		$sql = "
			SELECT
				f.ForumID,
				f.ForumName,
				f.Description,
				f.DisplayOrder,
				f.TopicNeedApprove,
				f.AllowAttachment,
				f.RecordStatus,
				f.DateInput,
				f.DateModified,
				f.InputBy,
				f.LastModifiedBy,
				ftc.ClassID 
			FROM
				READING_GARDEN_FORUM as f 
				LEFT JOIN READING_GARDEN_FORUM_TARGET_CLASS as ftc ON f.ForumID = ftc.ForumID 
			WHERE 
				f.RecordStatus = '".RECORD_STATUS_ACTIVE."' 
				$cond_ForumID
				$cond_ClassID 
			ORDER BY
				f.DisplayOrder  
		";
		
		$Result= $this->returnArray($sql);
		
		return $Result;
	}
	
	function Management_Check_Forum_Name($ForumName,$ForumID='')
	{
		$ForumName = $this->Get_Safe_Sql_Query($ForumName);
		$sql = "SELECT COUNT(*) FROM READING_GARDEN_FORUM WHERE ForumName = '$ForumName' AND RecordStatus = '".RECORD_STATUS_ACTIVE."'";
		if($ForumID!='')
			$sql .= " AND ForumID <> '$ForumID' ";
		$count = $this->returnVector($sql);
		return ($count[0]>0?false:true);
	}
	
	function Management_Get_Max_Forum_Display_Order()
	{
		$sql = "SELECT Max(DisplayOrder) FROM READING_GARDEN_FORUM";
		$Result = $this->returnVector($sql);
		return $Result[0];
	}
	
	function Management_Update_Forum_Display_Order($ForumIDArr)
	{
		$this->Start_Trans();
		$i = 1;
		foreach((array)$ForumIDArr as $ForumID)
		{
			$sql = "
				UPDATE
					READING_GARDEN_FORUM
				SET
					DisplayOrder = $i,
					LastModifiedBy = '".$_SESSION['UserID']."',
					DateModified = NOW()
				WHERE
					ForumID = '$ForumID'
			";
			
			$Success[] = $this->db_db_query($sql);
			$i++;
		}
		
		if(!in_array(false,$Success))
		{
			$this->Commit_Trans();
			return true;
		}
		else
		{
			$this->RollBack_Trans();
			return false;
		}
	}
	
	function Get_Forum_Summary($Keyword='',$ForumIDArr='',$ClassIDArr='')
	{
		if(trim($Keyword)!=''){
			$Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
			$cond_Keyword = " AND (
								f.ForumName LIKE '%$Keyword%' 
								OR f.Description LIKE '%$Keyword%' 
							)";
		}
		
//		if(trim($ForumIDArr)!= '' && sizeof((array)$ForumIDArr)>0)
		if($ForumIDArr)
			$cond_ForumID = " AND f.ForumID IN (".implode(",",(array)$ForumIDArr).") ";
			
//		if(trim($ClassIDArr)!= '' && sizeof((array)$ClassIDArr)>0)
		if($ClassIDArr)
			$cond_ClassID = " AND ftc.ClassID IN (".implode(",",(array)$ClassIDArr).") ";
		
		if($this->isAdmin!=1){
			$join_cond_topic_status = " AND t.RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
		}
		
		$sql = "
			SELECT
				f.ForumID,
				f.ForumName,
				f.Description,
				f.DisplayOrder,
				f.TopicNeedApprove,
				f.AllowAttachment,
				f.RecordStatus,
				f.DateInput,
				f.DateModified,
				f.InputBy,
				f.LastModifiedBy,
				GROUP_CONCAT( ftc.ClassID SEPARATOR ', ') AS TargetClassID,
				COUNT(DISTINCT t.ForumTopicID) as TotalTopic,
				COUNT(DISTINCT p.ForumPostID) as TotalPost,
				MAX(t.DateInput) as LastTopicTime,
				MAX(p.DateInput) as LastPostTime 
			FROM
				READING_GARDEN_FORUM as f 
				LEFT JOIN READING_GARDEN_FORUM_TARGET_CLASS as ftc ON f.ForumID = ftc.ForumID 
				LEFT JOIN READING_GARDEN_FORUM_TOPIC as t ON t.ForumID = f.ForumID $join_cond_topic_status 
				LEFT JOIN READING_GARDEN_FORUM_POST as p ON p.ForumTopicID = t.ForumTopicID AND p.RecordStatus = '".RECORD_STATUS_ACTIVE."'
			WHERE 
				f.RecordStatus = '".RECORD_STATUS_ACTIVE."' 
				$cond_ForumID
				$cond_ClassID 
				$cond_Keyword 
			GROUP BY 
				f.ForumID 
			ORDER BY
				f.DisplayOrder  
		";
		
		$Result= $this->returnArray($sql);
		
		return $Result;
	}
	
	function Get_Forum_Selection($ClassIDArr='')
	{
		if($_SESSION['UserType']==2){// Student
			$StudentInfo = $this->Get_Student_ClassInfo($_SESSION['UserID']);
			$ClassIDArr = $StudentInfo[0]['YearClassID'];
		}
		
//		if(trim($ClassIDArr)!= '' && sizeof((array)$ClassIDArr)>0)
		if($ClassIDArr)
			$cond_ClassID = " AND ftc.ClassID IN (".implode(",",(array)$ClassIDArr).") ";
		
		$sql = "
			SELECT
				f.ForumID,
				f.ForumName 
			FROM
				READING_GARDEN_FORUM as f 
				LEFT JOIN READING_GARDEN_FORUM_TARGET_CLASS as ftc ON f.ForumID = ftc.ForumID 
			WHERE 
				f.RecordStatus = '".RECORD_STATUS_ACTIVE."' 
				$cond_ClassID 
			GROUP BY 
				f.ForumID 
			ORDER BY
				f.DisplayOrder  
		";
		
		$Result= $this->returnArray($sql);
		
		return $Result;
	}
	
	function Manage_Forum_Topic_Record($Action,$TopicData)
	{
		$Action = strtolower($Action);
		$ForumID = $TopicData['ForumID'];
		$ForumTopicID = $TopicData['ForumTopicID'];
		$TopicUserID = $TopicData['UserID'];
		$TopicSubject = $this->Get_Safe_Sql_Query(trim($TopicData['TopicSubject']));
		$Message = $this->Get_Safe_Sql_Query(trim($TopicData['Message']));
		$OnTop = $TopicData['OnTop']==''?0:$TopicData['OnTop'];
		$Attachment = $TopicData['Attachment'];
		
		if($Action=='add')
		{
			$ForumInfo = $this->Get_Class_Forum('', $ForumID);
			$TopicNeedApprove = $ForumInfo[0]['TopicNeedApprove'];
			$RecordStatus = ($_SESSION['UserType']==2 && $TopicNeedApprove==1)?RECORD_STATUS_WAITING_APPROVED:RECORD_STATUS_ACTIVE;
			$InputUserID = $_SESSION['UserID'];
			$sql = "INSERT INTO READING_GARDEN_FORUM_TOPIC 
					(ForumID,UserID,TopicSubject,Message,Attachment,OnTop,RecordStatus,DateInput,DateModified,InputBy,LastModifiedBy)
					VALUES('$ForumID','$InputUserID','$TopicSubject','$Message','$Attachment','$OnTop','$RecordStatus',NOW(),NOW(),'$InputUserID','$InputUserID')";
			$Success = $this->db_db_query($sql);
			if($Success && $_SESSION['UserType']==2){
				$this->Update_State_Score_Action(ACTION_SCORE_START_THREAD,1,$InputUserID);
			}
		}else if($Action=='update')
		{
			$sql = "UPDATE READING_GARDEN_FORUM_TOPIC SET 
						TopicSubject = '$TopicSubject',
						Message = '$Message',
						Attachment = '$Attachment',
						DateModified = NOW(),
						LastModifiedBy = '".$_SESSION['UserID']."'  
					WHERE ForumTopicID = '$ForumTopicID' ";
			$Success = $this->db_db_query($sql);
		}else if($Action=='delete')
		{
			$this->Start_Trans();
			$Result=array();
			# Pre-select students id
			$sql = "SELECT t.UserID FROM READING_GARDEN_FORUM_TOPIC as t INNER JOIN INTRANET_USER as u ON t.UserID = u.UserID AND u.RecordType = 2 WHERE t.ForumTopicID IN ('".implode("','",(array)$ForumTopicID)."') ";
			$StudentsID = $this->returnVector($sql);
			
			$sql = "DELETE FROM READING_GARDEN_FORUM_TOPIC WHERE ForumTopicID IN ('".implode("','",(array)$ForumTopicID)."') ";
			$Result['DeleteTopic'] = $this->db_db_query($sql);
			if($Result['DeleteTopic']){
				$PostData=array();
				$PostData['ForumTopicID'] = $ForumTopicID;
				$Result['DeletePost'] = $this->Manage_Forum_Post_Record("delete",$PostData);
				
				for($i=0;$i<sizeof($StudentsID);$i++){
					$Result['UpdateStateScore_'.$StudentsID[$i]] = $this->Update_State_Score_Action(ACTION_PENALTY_THREAD_DELETED,1,$StudentsID[$i]);
				}
			}
			if(!in_array(false,$Result)){
				$this->Commit_Trans();
				return true;
			}else{
				$this->RollBack_Trans();
				return false;
			}
		}else if($Action=='approve')
		{
			$RecordStatus = RECORD_STATUS_ACTIVE;
			$sql = "UPDATE READING_GARDEN_FORUM_TOPIC SET RecordStatus = '$RecordStatus' WHERE ForumTopicID IN (".implode(",",(array)$ForumTopicID).") ";
			$Success = $this->db_db_query($sql);
		}else if($Action=='reject')
		{
			$RecordStatus = RECORD_STATUS_REJECTED;
			$sql = "UPDATE READING_GARDEN_FORUM_TOPIC SET RecordStatus = '$RecordStatus' WHERE ForumTopicID IN (".implode(",",(array)$ForumTopicID).") ";
			$Success = $this->db_db_query($sql);
		}else if($Action=='markontop')
		{
			$sql = "UPDATE READING_GARDEN_FORUM_TOPIC SET OnTop = '1' WHERE ForumTopicID IN (".implode(",",(array)$ForumTopicID).") ";
			$Success = $this->db_db_query($sql);
		}else if($Action=='removeontop')
		{
			$sql = "UPDATE READING_GARDEN_FORUM_TOPIC SET OnTop = '0' WHERE ForumTopicID IN (".implode(",",(array)$ForumTopicID).") ";
			$Success = $this->db_db_query($sql);
		}
		
		return $Success;
	}
	
	function Manage_Forum_Post_Record($Action,$PostData)
	{
		$Action = strtolower($Action);
		$ForumTopicID = $PostData['ForumTopicID'];
		$ForumPostID = $PostData['ForumPostID'];
		$PostUserID = $PostData['UserID'];
		$PostSubject = $this->Get_Safe_Sql_Query(trim($PostData['PostSubject']));
		$Message = $this->Get_Safe_Sql_Query(trim($PostData['Message']));
		$Attachment = $PostData['Attachment'];
		
		if($Action=="add")
		{
			$RecordStatus = RECORD_STATUS_ACTIVE;
			$InputUserID = $_SESSION['UserID'];
			$sql = "INSERT INTO READING_GARDEN_FORUM_POST 
					(ForumTopicID,UserID,PostSubject,Message,Attachment, RecordStatus,DateInput,DateModified,InputBy,LastModifiedBy)
					VALUES ('$ForumTopicID','$InputUserID','$PostSubject','$Message','$Attachment','$RecordStatus',NOW(),NOW(),'$InputUserID','$InputUserID')";
			$Success = $this->db_db_query($sql);
			
			if($Success && $_SESSION['UserType']==2){
				$this->Update_State_Score_Action(ACTION_SCORE_FOLLOW_THREAD,1,$InputUserID);
			}
		}else if($Action=="update")
		{
			$sql = "UPDATE READING_GARDEN_FORUM_POST SET 
						PostSubject = '$PostSubject',
						Message = '$Message',
						Attachment = '$Attachment',
						DateModified = NOW(),
						LastModifiedBy = '".$_SESSION['UserID']."' 
					WHERE 
						ForumPostID = '$ForumPostID' ";
			$Success = $this->db_db_query($sql);
		}else if($Action=="delete")
		{
			$ValidSqlSelect = false;
			$sqlSelect = "SELECT p.UserID FROM READING_GARDEN_FORUM_POST as p 
							INNER JOIN INTRANET_USER as u ON p.UserID = u.UserID AND u.RecordType = 2 WHERE 1 ";
			
			$ValidSql = false; // Prevent delete all post
			$sql = "DELETE FROM READING_GARDEN_FORUM_POST WHERE 1 ";
//			if(trim($ForumTopicID)!='' && sizeof((array)$ForumTopicID)>0){ 
			if($ForumTopicID){
				$topics_id = implode(",",(array)$ForumTopicID);
				$sql .= " AND ForumTopicID IN ($topics_id) ";
				$ValidSql = true;
				$sqlSelect .= " AND p.ForumTopicID IN ($topics_id) ";
				$ValidSqlSelect = true;
			}
//			if(trim($ForumPostID)!='' && sizeof((array)$ForumPostID)>0){
			if($ForumPostID){
				$posts_id = implode(",",(array)$ForumPostID);
				$sql .= " AND ForumPostID IN ($posts_id) ";
				$ValidSql = true;
				$sqlSelect .= " AND p.ForumPostID IN ($posts_id) ";
				$ValidSqlSelect = true;
			}
			if($ValidSqlSelect) $StudentsID = $this->returnVector($sqlSelect);
			
			if($ValidSql)
				$Success = $this->db_db_query($sql);
			else
				$Success = false;
				
			if($Success){
				if(sizeof($StudentsID)>0){
					for($i=0;$i<sizeof($StudentsID);$i++){
						$Result['UpdateStateScore_'.$StudentsID[$i]] = $this->Update_State_Score_Action(ACTION_PENALTY_POST_DELETED,1,$StudentsID[$i]);
					}
				}
			}
		}else if($Action=="approve")
		{
			$RecordStatus = RECORD_STATUS_ACTIVE;
			$sql = "UPDATE READING_GARDEN_FORUM_POST SET RecordStatus = '$RecordStatus' WHERE ForumPostID IN (".implode(",",(array)$ForumPostID).") ";
			$Success = $this->db_db_query($sql);
		}else if($Action=="reject")
		{
			$RecordStatus = RECORD_STATUS_REJECTED;
			$sql = "UPDATE READING_GARDEN_FORUM_POST SET RecordStatus = '$RecordStatus' WHERE ForumPostID IN (".implode(",",(array)$ForumPostID).") ";
			$Success = $this->db_db_query($sql);
		}
		
		return $Success;
	}
	
	function Get_Forum_Topic_DBTable_Sql($ForumID,$Keyword='',$RecordStatus='')
	{
		global $Lang, $image_path, $LAYOUT_SKIN;
		$NameFieldCreatedBy = getNameFieldWithClassNumberByLang("u1.");
		$NameFieldLastReplyBy = getNameFieldWithClassNumberByLang("u2.");
		
		$RecordStatus = trim($RecordStatus)==''?RECORD_STATUS_ACTIVE:$RecordStatus;
		$cond_RecordStatus = " AND t.RecordStatus = '$RecordStatus' ";
		
		$Keyword = trim($Keyword);
		if($Keyword!=''){
			$Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
			$cond_Keyword = " AND (
								t.TopicSubject LIKE '%$Keyword%' 
								OR t.Message LIKE '%$Keyword%' 
								OR t.DateInput LIKE '%$Keyword%' 
							)";
		}
		
		//if($this->isAdmin==1){
			$CheckboxField = ",IF(t.UserID='".$_SESSION['UserID']."' OR '".$this->isAdmin."'='1',CONCAT('<input type=\"checkbox\" name=\"ForumTopicID[]\" value=\"',t.ForumTopicID,'\">'),'&nbsp;') as CheckBox ";
		//}
		
		//if($this->isAdmin!=1){
		//	$join_cond_post_status = " AND p.RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
		//	$join_cond_inner_post_status = " AND innerP.RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
		//}
		
		$sql = "SELECT 
					CONCAT(
						IF(t.OnTop='1','<img height=\'20\' align=\'absmiddle\' width=\'25\' src=\'".$image_path."/".$LAYOUT_SKIN."/ecomm/icon_ontop.gif\' />',''),
						'<a href=\'post.php?ForumID=$ForumID&ForumTopicID=',t.ForumTopicID,'\'>',t.TopicSubject,'</a>'
					) as TopicSubject,
					t.DateInput,
					$NameFieldCreatedBy as CreatedBy,
					COUNT(p.ForumPostID) as NumOfReply,
					$NameFieldLastReplyBy as LastReplyBy,
					MAX(p.DateInput) as LastPostTime
					$CheckboxField
					,t.ForumTopicID 
				FROM 
					READING_GARDEN_FORUM_TOPIC as t 
					LEFT JOIN READING_GARDEN_FORUM_POST as p ON p.ForumTopicID = t.ForumTopicID AND p.RecordStatus = '".RECORD_STATUS_ACTIVE."'
					LEFT JOIN INTRANET_USER as u1 ON u1.UserID = t.UserID 
					LEFT JOIN INTRANET_USER as u2 ON u2.UserID = (
						SELECT innerP.UserID FROM READING_GARDEN_FORUM_POST as innerP 
						WHERE t.ForumTopicID = innerP.ForumTopicID AND innerP.RecordStatus = '".RECORD_STATUS_ACTIVE."'
						ORDER BY innerP.DateInput DESC LIMIT 1 
					)
				WHERE 
					t.ForumID = '$ForumID' 
					$cond_RecordStatus 
					$cond_Keyword 
				GROUP BY 
					t.ForumTopicID ";
		
		return $sql;
	}
	
	function Get_Forum_Prev_Next_Topic($ForumID,$CurrentTopicID,$ConditionArr=array())
	{
		$SortFieldIndex = $ConditionArr['topic_field'];
		$OrderByIndex = $ConditionArr['topic_order'];
		$Keyword = $ConditionArr['Keyword'];
		$RecordStatus = $ConditionArr['RecordStatus'];
		$fields = array("TopicSubject", "DateInput", "NumOfReply", "CreatedBy", "LastReplyBy", "LastPostTime");
		$orders = array("DESC","ASC");
		
		$sql = $this->Get_Forum_Topic_DBTable_Sql($ForumID,$Keyword,$RecordStatus);
		$sql .= " ORDER BY t.OnTop DESC,";
		if(!isset($fields[$SortFieldIndex]))
			$sql .= $fields[1];
		else
			$sql .= $fields[$SortFieldIndex];
		if(!isset($orders[$OrderByIndex]))
			$sql .= " ".$orders[1];
		else
			$sql .= " ".$orders[$OrderByIndex];
		
		$data = $this->returnArray($sql);
		
		$topics = Get_Array_By_Key($data,"ForumTopicID");
		
		$current_topic_index = array_search($CurrentTopicID,(array)$topics);
		return array("PrevTopicID"=>$topics[$current_topic_index-1],"NextTopicID"=>$topics[$current_topic_index+1]);
	}
	
	function Get_Forum_Topic($ForumID='',$TopicID='',$GetDetails=false, $ClassIDArr='', $countPost='', $ApprovedOnly=0)
	{
//		if(trim($ForumID)!='' && sizeof((array)$ForumID)>0){
		if($ForumID){
			$cond_ForumID = " AND t.ForumID IN (".implode(",",(array)$ForumID).") ";
		}
		
//		if(trim($TopicID)!='' && sizeof((array)$TopicID)>0){
		if($TopicID){
			$cond_Topic = " AND t.ForumTopicID IN (".implode(",",(array)$TopicID).") ";
		}
		
		if($GetDetails){
			$NameFieldCreator = getNameFieldWithClassNumberByLang("u.");
			$NameFieldModifyBy = getNameFieldWithClassNumberByLang("u2.");
			$fields = ",$NameFieldCreator as CreatorName
						,u.PersonalPhotoLink
						,$NameFieldModifyBy as LastModifyName ";
			
			$join_table = "LEFT JOIN INTRANET_USER as u ON u.UserID = t.UserID 
						   LEFT JOIN INTRANET_USER as u2 ON u2.UserID = t.LastModifiedBy ";
		}
		
//		if(trim($ClassIDArr) != '')
		if($ClassIDArr)
		{
			$join_forum_table = "
				LEFT JOIN READING_GARDEN_FORUM f ON f.ForumID = t.ForumID AND f.RecordStatus = ".RECORD_STATUS_ACTIVE."
				INNER JOIN READING_GARDEN_FORUM_TARGET_CLASS as ftc ON f.ForumID = ftc.ForumID  AND ftc.ClassID IN (".implode(",",(array)$ClassIDArr).")
			";
		}
		
		if($countPost ==1)
		{
			$join_post_table = "
				LEFT JOIN READING_GARDEN_FORUM_POST p ON t.ForumTopicID = p.ForumTopicID AND p.RecordStatus='".RECORD_STATUS_ACTIVE."' ";
			$FieldCountPost = " ,COUNT(p.ForumPostID) AS PostCount ";
			$GroupBy = " GROUP BY t.ForumTopicID";
			$sort_by_post = " p.DateInput desc, ";
		}
		
		if($ApprovedOnly==1)
		{
			$cond_RecordStatus = " AND t.RecordStatus=".RECORD_STATUS_ACTIVE." ";
		}
		
		$sql = "SELECT 
					t.ForumTopicID,
					t.ForumID,
					t.UserID,
					t.TopicSubject,
					t.Message,
					t.Attachment,
					t.OnTop,
					t.RecordStatus,
					t.DateInput,
					t.DateModified,
					t.InputBy,
					t.LastModifiedBy 
					$fields 
					$FieldCountPost
				FROM 
					READING_GARDEN_FORUM_TOPIC as t 
					$join_table 
					$join_forum_table
					$join_post_table
				WHERE 
					1
					$cond_RecordStatus
					$cond_ForumID 
					$cond_Topic 
				$GroupBy
				ORDER BY t.OnTop, {$sort_by_post} DateInput DESC ";
		
		return $this->returnArray($sql);
	}
	
	function Get_Forum_Topic_Post_DBTable_Sql($ForumTopicID,$Keyword='',$RecordStatus='')
	{
		global $Lang;
		$NameFieldCreatedBy = getNameFieldWithClassNumberByLang("u1.");
		$NameFieldLastModifiedBy = getNameFieldWithClassNumberByLang("u2.");
		
		$RecordStatus = RECORD_STATUS_ACTIVE;
		$cond_RecordStatus = " AND p.RecordStatus = '$RecordStatus' ";
		
		$Keyword = trim($Keyword);
		if($Keyword!=''){
			$Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
			$cond_Keyword = " AND (
								p.PostSubject LIKE '%$Keyword%' 
								OR p.Message LIKE '%$Keyword%'
								OR p.DateInput LIKE '%$Keyword%' 
							)";
		}
		
		$sql = "SELECT
					p.ForumPostID,
					p.ForumTopicID,
					p.UserID,
					p.PostSubject,
					p.Message,
					p.Attachment,
					p.RecordStatus,
					p.DateInput,
					p.DateModified,
					p.LastModifiedBy,
					$NameFieldCreatedBy as CreatorName,
					u1.PersonalPhotoLink, 
					$NameFieldLastModifiedBy as LastModifiedName 
				FROM 
					READING_GARDEN_FORUM_POST as p 
					LEFT JOIN INTRANET_USER as u1 ON u1.UserID = p.UserID 
					LEFT JOIN INTRANET_USER as u2 ON u2.UserID = p.LastModifiedBy 
				WHERE 
					p.ForumTopicID = '$ForumTopicID' 
					$cond_RecordStatus 
					$cond_Keyword ";
		return $sql;
	}
	
	function Get_Forum_Topic_Post($ForumTopicID='',$ForumPostID='')
	{
//		if(trim($ForumTopicID)!='' && sizeof((array)$ForumTopicID)>0)
		if($ForumTopicID)
			$cond_Topic = " AND p.ForumTopicID IN (".implode(",",(array)$ForumTopicID).") ";
//		if(trim($ForumPostID)!='' && sizeof((array)$ForumPostID)>0)
		if($ForumPostID)
			$cond_Post = " AND p.ForumPostID IN (".implode(",",(array)$ForumPostID).") ";
		
		$NameFieldLastModifiedBy = getNameFieldWithClassNumberByLang("u.");
		$RecordStatus = RECORD_STATUS_ACTIVE;
		
		$sql = "SELECT
					p.ForumPostID,
					p.ForumTopicID,
					p.UserID,
					p.PostSubject,
					p.Message,
					p.Attachment,
					p.RecordStatus,
					p.DateInput,
					p.DateModified,
					$NameFieldLastModifiedBy as LastModifiedByName 
				FROM 
					READING_GARDEN_FORUM_POST as p 
					LEFT JOIN INTRANET_USER as u ON u.UserID = p.LastModifiedBy  
				WHERE 
					p.RecordStatus = '$RecordStatus' 
					$cond_Topic
					$cond_Post ";
		return $this->returnArray($sql);
	}
	
	/******************************************* End of Forum Functions ***********************************************/

	function Settings_Manage_Comment_Bank($Action,$CommentID='',$Comment='')
	{
		$Action = strtolower(trim($Action));
		$Result = false;
		$InputUserID = $_SESSION['UserID'];
		switch($Action)
		{
			case "add":
				$sql = "INSERT INTO READING_GARDEN_COMMENT_BANK (Comment,RecordStatus,DateInput,DateModified,InputBy,LastModifiedBy)
						VALUES ('".$this->Get_Safe_Sql_Query($Comment)."','1',NOW(),NOW(),'$InputUserID','$InputUserID')";
				$Result = $this->db_db_query($sql);
			break;
			case "update":
				$sql = "UPDATE READING_GARDEN_COMMENT_BANK SET Comment = '".$this->Get_Safe_Sql_Query($Comment)."',DateModified = NOW(), LastModifiedBy = '$InputUserID' WHERE CommentID = '$CommentID' ";
				$Result = $this->db_db_query($sql);	
			break;
			case "delete":
				$sql = "UPDATE READING_GARDEN_COMMENT_BANK SET RecordStatus = '".RECORD_STATUS_DELETED."' WHERE CommentID IN (".implode(",",(array)$CommentID).")";
				$Result = $this->db_db_query($sql);
			break;
		}
		
		return $Result;
	}
	
	function Settings_Get_Comment_Bank_DBTable_Sql($Keyword='')
	{
		$NameField = getNameFieldByLang("u.");
		$sql = "SELECT 
					b.Comment,
					b.DateModified,
					$NameField as  LastModifiedBy,
					CONCAT('<input type=\"checkbox\" name=\"CommentID[]\" value=\"',b.CommentID,'\">') as CheckBox  
				FROM 
					READING_GARDEN_COMMENT_BANK as b 
					LEFT JOIN INTRANET_USER as u ON b.LastModifiedBy = u.UserID 
				WHERE 
					b.RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
		if($Keyword != '')
		{
			$sql .= " AND Comment LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' ";
		}
		
		return $sql;
	}
	
	function Get_Comment_Bank_Comments($CommentID='',$Keyword='')
	{
		$sql = "SELECT 
					CommentID,
					Comment,
					DateInput,
					DateModified,
					InputBy,
					LastModifiedBy 
				FROM 
					READING_GARDEN_COMMENT_BANK 
				WHERE 
					RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
		if($CommentID != ''){
			$sql .= " AND CommentID IN (".implode(",",(array)$CommentID).") ";
		}
		if($Keyword != ''){
			$sql .= " AND Comment LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' ";
		}
		$sql .= "ORDER BY Comment ";
		$result = $this->returnArray($sql);
		return $result;
	}
	
	function Settings_Manage_Report_Sticker($Action,$DataArr=array())
	{
		global $PATH_WRT_ROOT, $intranet_root;
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$fs = new libfilesystem();
		
		$Action = strtolower(trim($Action));
		$Result = array();
		$InputUserID = $_SESSION['UserID'];
		if($Action=="add"){
			$sql = "INSERT INTO READING_GARDEN_REPORT_STICKER (RecordStatus,DateInput,DateModified,InputBy,LastModifiedBy)
					VALUES ('".RECORD_STATUS_ACTIVE."',NOW(),NOW(),'$InputUserID','$InputUserID')";
			$Result['InsertSticker'] = $this->db_db_query($sql);
			if($Result['InsertSticker']){
				$StickerID = $this->db_insert_id();
				
				$ImageLocation = $DataArr['ImageLocation'];
				$ImageName = $DataArr['ImageName']; 
				$StickerRootPath = $intranet_root.$this->StickerPath;
				$folder_prefix = $intranet_root.$this->ModuleFilePath;
				if (!file_exists($folder_prefix))
					$fs->folder_new($folder_prefix);
				if (!file_exists($StickerRootPath))
					$fs->folder_new($StickerRootPath);
				
				$extention = $fs->file_ext($ImageName);
				$filename = md5($StickerID.'_'.time()).$extention;
				
				$Result['CopyFile'] = $fs->file_copy($ImageLocation, $StickerRootPath."/".$filename);
				if($Result['CopyFile']){
					$sql = "UPDATE READING_GARDEN_REPORT_STICKER SET ImagePath = '$filename', RecordStatus = '".RECORD_STATUS_ACTIVE."' WHERE StickerID = '$StickerID' ";
					$Result['UpdateSticker'] = $this->db_db_query($sql);
				}else{
					$sql = "DELETE FROM READING_GARDEN_REPORT_STICKER WHERE StickerID = '$StickerID' ";
					$Result['DeleteSticker'] = $this->db_db_query($sql);
				}
			}
		}elseif($Action=="update"){
			$StickerID = $DataArr['StickerID'];
			$ImageLocation = $DataArr['ImageLocation'];
			$ImageName = $DataArr['ImageName'];
			$sql = "SELECT ImagePath FROM READING_GARDEN_REPORT_STICKER WHERE StickerID = '$StickerID' ";
			$tmp_result = $this->returnVector($sql);
			$OldImagePath = trim($tmp_result[0]);
			$OldStickerFullPath = $intranet_root.$this->StickerPath."/".$OldImagePath;
			
			$StickerRootPath = $intranet_root.$this->StickerPath;
			$folder_prefix = $intranet_root.$this->ModuleFilePath;
			if (!file_exists($folder_prefix))
				$fs->folder_new($folder_prefix);
			if (!file_exists($StickerRootPath))
				$fs->folder_new($StickerRootPath);
			
			$extention = $fs->file_ext($ImageName);
			$filename = md5($StickerID.'_'.time()).$extention;
			$Result['CopyNewFile'] = $fs->file_copy($ImageLocation, $StickerRootPath."/".$filename);
			if($Result['CopyNewFile']){
				$sql = "UPDATE READING_GARDEN_REPORT_STICKER SET ImagePath = '$filename' WHERE StickerID = '$StickerID' ";
			 	$Result['UpdateSticker'] = $this->db_db_query($sql);
				if($Result['UpdateSticker'] && $OldImagePath != '' && file_exists($OldStickerFullPath)){
					$Result['RemoveFile'] = $fs->file_remove($OldStickerFullPath);
				}
			}			
		}elseif($Action=="delete"){
			$StickerID = $DataArr['StickerID'];
			$sql = "SELECT StickerID,ImagePath FROM READING_GARDEN_REPORT_STICKER WHERE StickerID IN (".implode(",",(array)$StickerID).") ";
			$sticker_list = $this->returnArray($sql);
			$RemovedStickerID = array();
			for($i=0;$i<sizeof($sticker_list);$i++){
				$ImagePath = trim($sticker_list[$i]['ImagePath']);
				$StickerFullPath = $intranet_root.$this->StickerPath."/".$ImagePath;
				if($ImagePath != '' && file_exists($StickerFullPath)){
					$Result['RemoveFile'.$i] = $fs->file_remove($StickerFullPath);
					if($Result['RemoveFile'.$i]){
						$RemovedStickerID[] = $sticker_list[$i]['StickerID'];
					}
				}
			}
			if(sizeof($RemovedStickerID)>0){
				$sql = "UPDATE READING_GARDEN_REPORT_STICKER SET ImagePath=NULL, RecordStatus='".RECORD_STATUS_DELETED."', DateModified = NOW(), LastModifiedBy = '$InputUserID' WHERE StickerID IN (".implode(",",$RemovedStickerID).")";
				$Result['UpdateStickers'] = $this->db_db_query($sql);
			}
		}
		
		return !in_array(false,$Result);
	}
	
	function Settings_Get_Report_Sticker_DBTable_Sql($Keyword='')
	{
		global $PATH_WRT_ROOT, $intranet_root, $Lang;
		$StickerFullRootPath = $this->StickerPath."/";
		$NameField = getNameFieldByLang("u.");
		$sql = "SELECT 
					CONCAT('<a title=\'".$Lang['Btn']['Edit']."\' class=\'edit thickbox\' href=\'#TB_inline?height=400&width=550&inlineId=FakeLayer\' onclick=\'Get_Sticker_Form(',s.StickerID,',false);\' >',
						'<img src=\"$StickerFullRootPath', s.ImagePath, '\" width=\"120px\" height=\"120px\" /></a>') as ImagePath,
					s.DateModified,
					$NameField as LastModifiedBy,
					CONCAT('<input type=\"checkbox\" name=\"StickerID[]\" value=\"',s.StickerID,'\">') as CheckBox  
				FROM 
					READING_GARDEN_REPORT_STICKER as s 
					LEFT JOIN INTRANET_USER as u ON s.LastModifiedBy = u.UserID 
				WHERE 
					s.RecordStatus = '".RECORD_STATUS_ACTIVE."' ";
		
		return $sql;
	}
	
	function Get_Report_Sticker($StickerID='',$BookReportID='',$StrickMapping=false,$ExcludeStickerID=array())
	{
		if($StickerID!='' && sizeof((array)$StickerID)>0){
			$cond_sticker = " AND s.StickerID IN (".implode(",",(array)$StickerID).") ";
		}
		if($BookReportID!='' && sizeof((array)$BookReportID)>0){
			$cond_book_report = " AND m.BookReportID IN (".implode(",",(array)$BookReportID).") ";
		}
		if(sizeof($ExcludeStickerID)>0){
			$cond_exclude_sticker = " AND s.StickerID NOT IN (".implode(",",$ExcludeStickerID).")";
		}
		$join_by = $StrickMapping?"INNER":"LEFT";		
		
		$sql = "SELECT 
					s.StickerID,
					m.BookReportID,
					m.StickerMappingID,
					s.ImagePath,
					s.DateModified,
					s.LastModifiedby  
				FROM READING_GARDEN_REPORT_STICKER as s 
				$join_by JOIN READING_GARDEN_REPORT_STICKER_MAPPING as m ON m.StickerID = s.StickerID 
				WHERE s.RecordStatus = '".RECORD_STATUS_ACTIVE."' 
					$cond_sticker 
					$cond_book_report 
					$cond_exclude_sticker 
				ORDER BY m.BookReportID,s.DateInput ";
		
		return $this->returnArray($sql);
	}
	
	function Manage_Report_Sticker_Mappings($Action,$BookReportID,$StickerID)
	{
		$Action = strtolower(trim($Action));
		$Result = false;
		if($Action=="add"){
			if($BookReportID == '') return false;
			if($StickerID=='' || (is_array($StickerID) && sizeof($StickerID)==0)) return false;
			$values = array();
			$sticker_list = (array)$StickerID;
			$InputUserID = $_SESSION['UserID'];
			for($i=0;$i<sizeof($sticker_list);$i++){
				$values[] = "('$BookReportID','".$sticker_list[$i]."',NOW(),'$InputUserID')";
			}
			$sql = "INSERT INTO READING_GARDEN_REPORT_STICKER_MAPPING (BookReportID,StickerID,DateInput,InputBy) VALUES ";
			if(sizeof($values)==0) return false;
			$sql .= implode(",",$values);
			$Result = $this->db_db_query($sql);
		}elseif($Action=="remove"){
			if($BookReportID == '') return false;
			if($StickerID=='' || (is_array($StickerID) && sizeof($StickerID)==0)) return false;
			$sql = "DELETE FROM READING_GARDEN_REPORT_STICKER_MAPPING WHERE BookReportID = '$BookReportID' AND StickerID IN (".implode(",",(array)$StickerID).") ";
			$Result = $this->db_db_query($sql);
		}
		
		return $Result;
	}
	
	public static function Grant_View_Right_For_Report_Page($key)
	{
		global $_REQUEST;
		$key = base64_decode($key);
		$key_params = explode("&",$key);
		$key_assoc = array();
		$checksum = 0;
		$checksumstr = '';
		for($i=0;$i<sizeof($key_params);$i++){
			list($k,$v) = explode("=",$key_params[$i]);
			if($k=="checksum"){
				$checksum = $v;
				continue;
			}
			$key_assoc[$k] = $v;
			$checksumstr .= $checksumstr!=''?'&'.$key_params[$i]:$key_params[$i];
		}
		
		if(strlen($checksumstr)!=$checksum)
		{
			// error or invalid report
			return false;
		}else{
			//session_regenerate_id();
			$_SESSION["UserID"] = $key_assoc['UserID'];
			//$_SESSION["iSession_LoginSessionID"] = session_id();
			//$_SESSION["intranet_default_lang"] = $key_assoc['intranet_default_lang'];
			$_SESSION["intranet_session_language"] = $key_assoc['intranet_session_language'];
			if(!isset($_REQUEST['ReadingRecordID'])) $_REQUEST['ReadingRecordID'] = $key_assoc['ReadingRecordID'];
			if(!isset($_REQUEST['RecommendOnly'])) $_REQUEST['RecommendOnly'] = $key_assoc['RecommendOnly'];
		}
		return true;
	}
	
	function Get_Popular_Award_scheme_Item_Category_Info($CategoryID='', $CategoryCode='', $SkipEmptyCategory=0, $StudentSelectItem=0)
	{
		if($CategoryID)
			$cond_CategoryID = " AND ic.CategoryID IN (".implode(",", (array)$CategoryID).")";
		if($CategoryCode)
			$cond_CategoryCode = " AND ic.CategoryCode IN ('".implode("','", (array)$CategoryCode)."')";
		if($SkipEmptyCategory==1)
			$cond_SkipEmptyCategory = " HAVING COUNT(i.ItemID) > 0";
		if($StudentSelectItem==1)
			$cond_StudentSelectItem = " AND i.CanSubmitByStudent = 1 ";
		
		$sql = "
			SELECT
				ic.CategoryID,
				ic.CategoryCode,
				ic.CategoryNameEn,
				IF(ic.CategoryNameCh IS NULL OR ic.CategoryNameCh = '', ic.CategoryNameEn, ic.CategoryNameCh) CategoryNameCh,
				COUNT(i.ItemID) NumOfItem
			FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM_CATEGORY ic
				LEFT JOIN READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM i ON i.CategoryID = ic.CategoryID $cond_StudentSelectItem
			WHERE
				1
				$cond_CategoryID
				$cond_CategoryCode
				AND ic.RecordStatus = ".RECORD_STATUS_ACTIVE."
				
			GROUP BY
				ic.CategoryID
			$cond_SkipEmptyCategory
			ORDER BY
				ic.DisplayOrder ASC

		";
		
		return $this->returnResultSet($sql);
	}
	
//	function Get_Popular_Award_Scheme_Item_Info($ItemID='', $ItemCode='', $CategoryID='')
//	{
//		if(trim($ItemID)!='')
//			$cond_ItemID = " AND i.ItemID IN (".implode(",", (array)$ItemID).")";
//		if(trim($ItemCode)!='')
//			$cond_ItemCode = " AND i.ItemCode IN (".implode(",", (array)$ItemCode).")";
//		if(trim($CategoryID)!='')
//			$cond_CategoryID = " AND ic.CategoryID IN (".implode(",", (array)$CategoryID).")";
//		
//		$sql = "
//			SELECT
//				i.ItemID,
//				i.ItemCode,
//				i.ItemDescEn,
//				IF(i.ItemDescCh IS NULL OR i.ItemDescCh = '', i.ItemDescEn, i.ItemDescCh) ItemDescCh,
//				i.Score,
//				i.CanSubmitByStudent,
//				ic.CategoryID,
//				ic.CategoryCode,
//				ic.CategoryNameEn,
//				IF(ic.CategoryNameCh IS NULL OR ic.CategoryNameCh = '', ic.CategoryNameEn, ic.CategoryNameCh) CategoryNameCh
//			FROM
//				READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM_CATEGORY ic
//				LEFT JOIN READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM i ON i.CategoryID = ic.CategoryID AND i.RecordStatus = 1
//			WHERE
//				1
//				$cond_ItemID
//				$cond_ItemCode
//				$cond_CategoryID
//				AND ic.RecordStatus = 1
//			ORDER BY
//				ic.DisplayOrder, 
//				i.DisplayOrder ASC
//		";
//		
//		return $this->returnResultSet($sql);
//	}

	function Get_Popular_Award_Scheme_Item_Info($ItemID='', $ItemCode='', $CategoryID='', $StudentSelectItem=0)
	{
		if($ItemID)
			$cond_ItemID = " AND i.ItemID IN (".implode(",", (array)$ItemID).")";
		if($ItemCode)
			$cond_ItemCode = " AND i.ItemCode IN ('".implode("','", (array)$ItemCode)."')";
		if($CategoryID)
			$cond_CategoryID = " AND i.CategoryID IN (".implode(",", (array)$CategoryID).")";
		if($StudentSelectItem==1)
			$cond_StudentSelectItem = " AND i.CanSubmitByStudent = 1 ";
		
		$sql = "
			SELECT
				i.ItemID,
				i.ItemCode,
				i.ItemDescEn,
				IF(i.ItemDescCh IS NULL OR i.ItemDescCh = '', i.ItemDescEn, i.ItemDescCh) ItemDescCh,
				i.Score,
				i.CanSubmitByStudent,
				i.CategoryID
			FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM i
			WHERE
				1
				$cond_ItemID
				$cond_ItemCode
				$cond_CategoryID
				$cond_StudentSelectItem
				AND i.RecordStatus = ".RECORD_STATUS_ACTIVE."
			ORDER BY
				i.DisplayOrder ASC
		";
		
		return $this->returnResultSet($sql);
	}	
	
	function Is_Popular_Award_Scheme_Category_Code_Exist($CategoryCode, $CategoryID='')
	{
		if(trim($CategoryID)!='')
			$cond_CategoryID = " AND CategoryID <> '$CategoryID' ";
		
		$sql = "
			SELECT 
				COUNT(*)
			FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM_CATEGORY
			WHERE
				CategoryCode = '".$this->Get_Safe_Sql_Query($CategoryCode)."'
				$cond_CategoryID
				AND RecordStatus = ".RECORD_STATUS_ACTIVE."
		";
		
		$tmp = $this->returnVector($sql);
		
		return $tmp[0]>0?true:false; 
	}
	
	function Get_Max_Popular_Award_Scheme_Category_Display_Order()
	{
		$sql = "
			SELECT
				Max(DisplayOrder)
			FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM_CATEGORY
			WHERE
				RecordStatus = ".RECORD_STATUS_ACTIVE."
		";
		$tmp = $this->returnVector($sql);
		
		return $tmp[0]; 
	}
	
	function Add_Popular_Award_Scheme_Category($CategoryCode, $CategoryNameEn, $CategoryNameCh)
	{
		if($this->Is_Popular_Award_Scheme_Category_Code_Exist($CategoryCode))
			return false;
		
		$MaxDisplayOrder = $this->Get_Max_Popular_Award_Scheme_Category_Display_Order();
		$MaxDisplayOrder = $MaxDisplayOrder?$MaxDisplayOrder+1:1;
			
		$sql = "
		INSERT INTO READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM_CATEGORY
			(CategoryCode, CategoryNameEn, CategoryNameCh, DisplayOrder, DateInput, InputBy)
		VALUES
			(
				'".$this->get_Safe_Sql_Query($CategoryCode)."',
				'".$this->get_Safe_Sql_Query($CategoryNameEn)."',
				'".$this->get_Safe_Sql_Query($CategoryNameCh)."',
				$MaxDisplayOrder,
				NOW(),
				'".$_SESSION['UserID']."'
			)
		";
		
		$Success= $this->db_db_query($sql);
		
		return $Success;
	}
	
	function Delete_Popular_Award_Scheme_Category($CategoryID)
	{
		$UpdateData['RecordStatus'] = RECORD_STATUS_DELETED;
		
		$Success = $this->Update_Popular_Award_Scheme_Category($UpdateData, $CategoryID);
		
		return $Success;
	}
	
	function Update_Popular_Award_Scheme_Category($UpdateData, $CategoryID)
	{
		if($UpdateData['CategoryCode'] && $this->Is_Popular_Award_Scheme_Item_Code_Exist($UpdateData['CategoryCode'], $CategoryID))
			return false;
					
		$SetStr = $this->Get_Update_SQL_Set_String($UpdateData);
		
		$sql = "
			UPDATE
				READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM_CATEGORY
			SET
				$SetStr
			WHERE
				CategoryID IN (".implode(",",(array)$CategoryID).")
		";	
		
		$Success = $this->db_db_query($sql);
		
		return $Success;
	}
	
	function Update_Popular_Award_Scheme_Category_Display_Order($CategoryID)
	{
		$Success = $this->Reorder_Display_Order("READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM_CATEGORY", "CategoryID", "DisplayOrder", $CategoryID);
		
		return $Success;
	}
	
	function Is_Popular_Award_Scheme_Item_Code_Exist($ItemCode, $ItemID='')
	{
		if(trim($ItemID)!='')
			$cond_ItemID = " AND ItemID <> '$ItemID' ";
		
		$sql = "
			SELECT 
				COUNT(*)
			FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM
			WHERE
				ItemCode = '".$this->Get_Safe_Sql_Query($ItemCode)."'
				$cond_ItemID
				AND RecordStatus = ".RECORD_STATUS_ACTIVE."
		";
		
		$tmp = $this->returnVector($sql);
		
		return $tmp[0]>0?true:false; 
	}	

	function Get_Max_Popular_Award_Scheme_Item_Display_Order($CategoryID)
	{
		$sql = "
			SELECT
				Max(DisplayOrder)
			FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM
			WHERE
				RecordStatus = ".RECORD_STATUS_ACTIVE."
				AND CategoryID = '$CategoryID'
		";
		$tmp = $this->returnVector($sql);
		
		return $tmp[0]; 
	}
		
	function Add_Popular_Award_Scheme_Item($ItemCode, $ItemDescEn, $ItemDescCh, $Score, $CanSubmitByStudent, $CategoryID)
	{
		if($this->Is_Popular_Award_Scheme_Item_Code_Exist($ItemCode))
			return false;
		
		$MaxDisplayOrder = $this->Get_Max_Popular_Award_Scheme_Item_Display_Order($CategoryID);
		$MaxDisplayOrder = $MaxDisplayOrder?$MaxDisplayOrder+1:1;
			
		$sql = "
		INSERT INTO READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM
			(ItemCode, ItemDescEn, ItemDescCh, Score, CanSubmitByStudent, CategoryID, DisplayOrder, DateInput, InputBy)
		VALUES
			(
				'".$this->get_Safe_Sql_Query($ItemCode)."',
				'".$this->get_Safe_Sql_Query($ItemDescEn)."',
				'".$this->get_Safe_Sql_Query($ItemDescCh)."',
				'$Score',
				'$CanSubmitByStudent',
				'$CategoryID',
				$MaxDisplayOrder,
				NOW(),
				'".$_SESSION['UserID']."'
			)
		";
		
		$Success= $this->db_db_query($sql);
		
		return $Success;
	}
	
	function Delete_Popular_Award_Scheme_Item($ItemID)
	{
		$UpdateData['RecordStatus'] = RECORD_STATUS_DELETED;
		
		$Success = $this->Update_Popular_Award_Scheme_Item($UpdateData, $ItemID);
		
		return $Success;
	}
	
	function Update_Popular_Award_Scheme_Item($UpdateData, $ItemID)
	{
		if($UpdateData['ItemCode'] && $this->Is_Popular_Award_Scheme_Item_Code_Exist($UpdateData['ItemCode'], $ItemID))
			return false;
		
		$SetStr = $this->Get_Update_SQL_Set_String($UpdateData);
		
		$sql = "
			UPDATE
				READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM
			SET
				$SetStr
			WHERE
				ItemID IN (".implode(",",(array)$ItemID).")
		";	
		
		$Success = $this->db_db_query($sql);
		
		return $Success;
	}

	function Update_Popular_Award_Scheme_Item_Display_Order($ItemID)
	{
		$Success = $this->Reorder_Display_Order("READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM", "ItemID", "DisplayOrder", $ItemID);
		
		return $Success;
	}
	
	function Submit_Popular_Award_Scheme_Record($ItemID, $StudentID='')
	{
		if(trim($StudentID) == '')
			$StudentID = $_SESSION['UserID'];
		
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$sql = "
			INSERT INTO READING_GARDEN_POPULAR_AWARD_SCHEME_STUDENT_ITEM_MAPPING
				(StudentID, ItemID, AcademicYearID, DateInput, InputBy)
			VALUES
				('$StudentID', '$ItemID', '$AcademicYearID', NOW(), '".$_SESSION['UserID']."')
		";
		
		return $this->db_db_query($sql);
	}
	
	function Delete_Popular_Award_Scheme_Record($StudentItemID)
	{
		$sql = "
			DELETE FROM 
				READING_GARDEN_POPULAR_AWARD_SCHEME_STUDENT_ITEM_MAPPING
			WHERE
				StudentItemID IN (".implode(",", (array)$StudentItemID).")
		";
		
		return $this->db_db_query($sql);
	}
	
	function Get_Popular_Award_Scheme_Student_Score($StudentID='', $CategoryID='')
	{
		if($StudentID)
			$cond_StudentID = " AND sim.StudentID IN (".implode(",",(array)$StudentID).")";
		if($CategoryID)
			$cond_CategoryID = " AND i.CategoryID IN (".implode(",",(array)$CategoryID).")";
		
		$sql = "
			SELECT
				sim.StudentID,
				ic.CategoryID,
				SUM(i.Score) Score
			FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_STUDENT_ITEM_MAPPING sim 
				INNER JOIN READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM i ON i.ItemID = sim.ItemID
				INNER JOIN READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM_CATEGORY ic ON ic.CategoryID = i.CategoryID
			WHERE
				1
				$cond_StudentID
				$cond_CategoryID
			GROUP BY
				sim.StudentID, ic.CategoryID 
		";
		
		return $this->returnArray($sql);
	}

	function Get_My_Record_Popular_Award_Scheme_DBTable_Sql()
	{
		$NameLang = getNameFieldByLang("iu.");
		$EnCh = Get_Lang_Selection("Ch", "En");
		
		$sql = "
			SELECT 
				i.ItemDesc$EnCh,
				ic.CategoryName$EnCh,
				$NameLang,
				sim.DateInput,
				IF(sim.InputBy = '".$_SESSION['UserID']."', CONCAT('<input type=\"checkbox\" name=\"StudentItemID[]\" value=\"',sim.StudentItemID,'\" class=\"StudentItemID\">'), '&nbsp;') as CheckBox
			FROM 
				READING_GARDEN_POPULAR_AWARD_SCHEME_STUDENT_ITEM_MAPPING sim 
				INNER JOIN READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM i ON i.ItemID = sim.ItemID
				INNER JOIN READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM_CATEGORY ic ON i.CategoryID = ic.CategoryID
				LEFT JOIN INTRANET_USER iu ON iu.UserID = sim.InputBy
			WHERE
				sim.StudentID = '".$_SESSION['UserID']."'
		";
		
		return $sql;
	}
	

	function Get_Management_Popular_Award_Scheme_DBTable_Sql($StudentID, $CategoryID='')
	{
		if(trim($CategoryID)!='')
			$cond_CategoryID = " AND ic.CategoryID = '$CategoryID' ";
		
		$NameLang = getNameFieldByLang("iu.");
		$EnCh = Get_Lang_Selection("Ch", "En");
		
		$sql = "
			SELECT
				i.ItemCode, 
				i.ItemDesc$EnCh,
				ic.CategoryName$EnCh,
				$NameLang,
				sim.DateInput,
				CONCAT('<input type=\"checkbox\" name=\"StudentItemID[]\" value=\"',sim.StudentItemID,'\" class=\"StudentItemID\">') as CheckBox
			FROM 
				READING_GARDEN_POPULAR_AWARD_SCHEME_STUDENT_ITEM_MAPPING sim 
				INNER JOIN READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM i ON i.ItemID = sim.ItemID
				INNER JOIN READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM_CATEGORY ic ON i.CategoryID = ic.CategoryID
				LEFT JOIN INTRANET_USER iu ON iu.UserID = sim.InputBy
			WHERE
				sim.StudentID = '".$StudentID."'
				$cond_CategoryID
		";
		
		return $sql;
	}

	function Get_Popular_Award_Scheme_Record_Last_Input_Date()
	{
		
		$NameLang = getNameFieldByLang("iu.");
		$sql = "
			SELECT
				ycu.YearClassID, sim.DateInput, $NameLang InputBy
			FROM 
				READING_GARDEN_POPULAR_AWARD_SCHEME_STUDENT_ITEM_MAPPING sim 	
				LEFT JOIN YEAR_CLASS_USER ycu ON sim.StudentID = ycu.UserID 
				INNER JOIN YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
				INNER JOIN INTRANET_USER iu ON sim.InputBy = iu.UserID
			WHERE
				sim.DateInput IN 
				(
					SELECT 
						MAX(sim.DateInput)
					FROM 
						READING_GARDEN_POPULAR_AWARD_SCHEME_STUDENT_ITEM_MAPPING sim 	
						LEFT JOIN YEAR_CLASS_USER ycu ON sim.StudentID = ycu.UserID 
						INNER JOIN YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
					GROUP BY
						ycu.YearClassID
				) 
			
		";
		
		return $this->returnArray($sql);
	}
	
	function Insert_Popular_Award_Scheme_Award($AwardNameEn, $AwardNameCh, $TotalScore)
	{
		$sql = "
			INSERT INTO READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD
				(AwardNameEn, AwardNameCh, Score, DateInput, InputBy, DateModified, LastModifiedBy)
			VALUES
				(
					'".$this->Get_Safe_Sql_Query($AwardNameEn)."', 
					'".$this->Get_Safe_Sql_Query($AwardNameCh)."',
					'$TotalScore',  
					NOW(), 
					".$_SESSION['UserID'].",  
					NOW(), 
					".$_SESSION['UserID']."
				)
		";
		
		return $this->db_db_query($sql);
	}
	
	function Edit_Popular_Award_Scheme_Award($AwardID, $AwardNameEn, $AwardNameCh, $TotalScore)
	{
		$sql = "
			UPDATE 
				READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD
			SET
				AwardNameEn = '".$this->Get_Safe_Sql_Query($AwardNameEn)."',
				AwardNameCh = '".$this->Get_Safe_Sql_Query($AwardNameCh)."',
				Score = '$TotalScore',
				DateModified = NOW(),
				LastModifiedBy = '".$_SESSION['UserID']."'
			WHERE
				AwardID = $AwardID
		";
		
		return $this->db_db_query($sql);
	}	

	function Remove_Popular_Award_Scheme_Award($AwardID)
	{
		$sql = "
			DELETE a, b, c FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD a		
				LEFT JOIN READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_REQUIREMENT b ON a.AwardID = b.AwardID
				LEFT JOIN READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_REQUIREMENT_CATEGORY c ON b.RequirementID = c.RequirementID
			WHERE
				a.AwardID IN (".implode(",", (array)$AwardID).")
		";
		
		$Success['RemoveAward'] = $this->db_db_query($sql);
		
		$Success['RemoveRequirement'] = $this->Remove_Popular_Award_Scheme_Award_Requirement($AwardID);
		$Success['RemoveFormMapping'] = $this->Remove_Popular_Award_Scheme_Form_Mapping($AwardID);
		
		return !in_array(false, $Success);
	}

	
	function Update_Popular_Award_Scheme_Award_Requirement($AwardID, $ScoreArr, $CategoryArr)
	{
		$Success['RemoveRequirement'] = $this->Remove_Popular_Award_Scheme_Award_Requirement($AwardID);
		
		$SizeOfRequirement = sizeof($ScoreArr); 
		if($SizeOfRequirement>0)
		{ 
			for($i=0; $i<$SizeOfRequirement; $i++)
			{
				$Success['InsertRequirement'][] = $this->Insert_Popular_Award_Scheme_Award_Requirement($AwardID, $ScoreArr[$i]);
				$RequirementID = mysql_insert_id();
				
				$Success['InsertRequirementCategory'][] = $this->Insert_Popular_Award_Scheme_Award_Requirement_Category($RequirementID, $CategoryArr[$i]);	
			}
		}
		
		return !in_array(false, array_values_recursive((array)$Success));
		
	}
	
	function Insert_Popular_Award_Scheme_Award_Requirement($AwardID, $Score)
	{
		$sql = "
			INSERT INTO READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_REQUIREMENT
				(AwardID, Score, DateInput, InputBy)
			VALUES
				('$AwardID', '$Score', NOW(), '".$_SESSION['UserID']."')
		";
		
		return $this->db_db_query($sql);
	}

	function Insert_Popular_Award_Scheme_Award_Requirement_Category($RequirementID, $CategoryArr)
	{
		$SizeOfCat = sizeof($CategoryArr);
		for($i=0; $i<$SizeOfCat; $i++)
		{
			$SqlArr[]= "('$RequirementID', '".$CategoryArr[$i]."', NOW(), '".$_SESSION['UserID']."')";
		}
		$ValSql = implode(", ",(array)$SqlArr); 
		
		$sql = "
			INSERT INTO READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_REQUIREMENT_CATEGORY
				(RequirementID, CategoryID, DateInput, InputBy)
			VALUES
				$ValSql
		";
		
		return $this->db_db_query($sql);
	}
		
	function Remove_Popular_Award_Scheme_Award_Requirement($AwardID)
	{
		$sql = "
			DELETE a, b FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_REQUIREMENT a
				INNER JOIN READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_REQUIREMENT_CATEGORY b ON a.RequirementID = b.RequirementID
			WHERE
				a.AwardID = '$AwardID'
		";
		
		$Success = $this->db_db_query($sql);
		
		return $Success;
	}
	
	function Get_Popular_Award_Scheme_Award_Info($AwardID='')
	{
		if($AwardID)
			$cond_AwardID = " AND a.AwardID IN (".implode(",", (array)$AwardID).")";
			
		$LastModifiedBy = getNameFieldByLang("iu.");
		
		$sql = "
			SELECT
				a.AwardID,
				a.AwardNameEn,
				a.AwardNameCh,
				a.Score,
				a.DateModified,
				$LastModifiedBy LastModifiedBy
			FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD a
				LEFT JOIN INTRANET_USER iu ON iu.UserId = a.LastModifiedBy
			WHERE
				1
				$cond_AwardID
			ORDER BY
				a.Score DESC
		";
		
		return $this->returnArray($sql);
	}

	function Get_Popular_Award_Scheme_Award_Form_Mapping($AwardID='', $YearID='')
	{
		if($AwardID)
			$cond_AwardID = " AND atf.AwardID IN (".implode(",", (array)$AwardID).")";
			
		if($YearID)
			$cond_YearID = " AND y.YearID IN (".implode(",", (array)$YearID).")";
			
		$sql = "
			SELECT
				atf.AwardID,
				y.YearName,
				y.YearID
			FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_TARGET_FORM atf
				LEFT JOIN READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD a ON a.AwardID = atf.AwardID
				LEFT JOIN YEAR y ON y.YearID = atf.YearID
			WHERE
				1
				$cond_AwardID
				$cond_YearID
			ORDER BY
				y.Sequence ASC, a.Score	DESC
		";
		
		return $this->returnArray($sql);
	}
	
	function Update_Popular_Award_Scheme_Form_Mapping($AwardID, $YearID)
	{
		if(empty($AwardID) || empty($YearID)) return false;
		
		$Success['Remove'] = $this->Remove_Popular_Award_Scheme_Form_Mapping($AwardID);
		$Success['Insert'] = $this->Insert_Popular_Award_Scheme_Form_Mapping($AwardID, $YearID);
		
		return !in_array(false, $Success);
	}
	
	function Remove_Popular_Award_Scheme_Form_Mapping($AwardID)
	{
		$sql = "
			DELETE FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_TARGET_FORM
			WHERE
				AwardID IN (".implode(",", (array)$AwardID).")
		";	
		
		return $this->db_db_query($sql);
	}
	
	function Insert_Popular_Award_Scheme_Form_Mapping($AwardID, $YearID)
	{
		$SizeOfYear = sizeof($YearID);
		for($i=0; $i<$SizeOfYear; $i++)
		{
			$ValArr []= "('$AwardID', '".$YearID[$i]."', NOW(), ".$_SESSION['UserID'].")";
		}
		
		$ValSql = implode(",", $ValArr);
		
		
		$sql = "
			INSERT INTO READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_TARGET_FORM
				(AwardID, YearID, DateInput, InputBy)
			VALUES
				$ValSql
		";	
		
		return $this->db_db_query($sql);		
	}
	
	function Get_Popular_Award_Scheme_Award_Requirement_Info($RequirementID='', $AwardID='')
	{
		if($AwardID)
			$cond_AwardID = " AND ar.AwardID IN (".implode(",", (array)$AwardID).")";
		if($RequirementID)
			$cond_RequirementID = " AND ar.RequirementID IN (".implode(",", (array)$RequirementID).")";

		$sql = "
			SELECT
				ar.AwardID,
				ar.RequirementID,
				ar.Score
			FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_REQUIREMENT ar
			WHERE
				1
				$cond_RequirementID
				$cond_AwardID
		";
		
		return $this->returnArray($sql);
	}

	function Get_Popular_Award_Scheme_Award_Requirement_Category($RequirementID='')
	{
		if($RequirementID)
			$cond_RequirementID = " AND arc.RequirementID IN (".implode(",", (array)$RequirementID).")";

		$sql = "
			SELECT
				arc.RequirementID,
				arc.CategoryID
			FROM
				READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_REQUIREMENT_CATEGORY arc
			WHERE
				1
				$cond_RequirementID
		";
		
		return $this->returnArray($sql);
	}

	function Get_Popular_Award_Scheme_Student_Award($StudentIDArr='')
	{
		$StudentScore = $this->Get_Popular_Award_Scheme_Student_Score($StudentIDArr);
		$StudentScore = BuildMultiKeyAssoc($StudentScore, array("StudentID", "CategoryID"), "Score", 1);
		
		$RequirementInfo = $this->Get_Popular_Award_Scheme_Award_Requirement_Info();
		$RequirementInfo = BuildMultiKeyAssoc($RequirementInfo, array("AwardID", "RequirementID"));
		
		$RequirementCategory = $this->Get_Popular_Award_Scheme_Award_Requirement_Category();
		$RequirementCategory = BuildMultiKeyAssoc($RequirementCategory, "RequirementID", "CategoryID", 1, 1);
		
		$StudentInfo = $this->Get_Student_ClassInfo($StudentIDArr);
		$YearIDArr = array_unique(Get_Array_By_Key($StudentInfo, "YearID"));
		$StudentInfo = BuildMultiKeyAssoc($StudentInfo, "UserID");
		
		$YearIDInfo = $this->Get_Popular_Award_Scheme_Award_Form_Mapping(NULL, $YearIDArr);
		$AwardYearIDAssoc = BuildMultiKeyAssoc($YearIDInfo, "YearID", "AwardID", 1, 1);
		$AwardIDArr = array_unique((array)Get_Array_By_Key($YearIDInfo, "AwardID"));

		$AwardInfo = $this->Get_Popular_Award_Scheme_Award_Info($AwardIDArr);
		$AwardInfo = BuildMultiKeyAssoc($AwardInfo, "AwardID");

		foreach($StudentScore as $thisStudentID => $CatScoreArr)
		{
			$StuYearID = $StudentInfo[$thisStudentID]['YearID'];
			$thisAwardArr = $AwardYearIDAssoc[$StuYearID];
			$SizeOfAward = sizeof($thisAwardArr);
			
			if($SizeOfAward==0) continue;
			
			$TotalScore = array_sum($CatScoreArr);
			for($j=0; $j<$SizeOfAward; $j++)
			{
				$thisAwardID = $thisAwardArr[$j];
				
				$thisAwardInfo = $AwardInfo[$thisAwardID];
				if($TotalScore < $thisAwardInfo['Score'] ) // fail total score
					continue;
				
				$PassAllRequirement = true;	
				foreach((array)$RequirementInfo[$thisAwardID] as $thisRequirement)
				{
					$thisRequirementID = $thisRequirement['RequirementID'];
					$thisScoreRequired = $thisRequirement['Score'];
				
					$thisRequirementScoreObtained = 0;	
					foreach($RequirementCategory[$thisRequirementID] as $thisCategoryID)
					{
						$thisRequirementScoreObtained += $CatScoreArr[$thisCategoryID]; // loop to sum up Score of Applicable Category
					}
					
					if($thisRequirementScoreObtained < $thisScoreRequired) // fail in any requirement
					{
						$PassAllRequirement = false;
						break; // skip checking the rest requirement
					} 	
				}
				
				if($PassAllRequirement)
				{	
					$StudentAward[$thisStudentID] = $thisAwardInfo;
					break; // skip checking the rest Award
				}
				
			}
		}
	
		return $StudentAward;
	}
	
	
	
	function Delete_Book_Import_Temp_Data()
	{
		
		$sql = "Delete from TEMP_READING_SCHEME_BOOK_IMPORT Where UserID = '".$_SESSION["UserID"]."' ";
			$SuccessArr['DeleteTempImportCsvData'] = $this->db_db_query($sql);
			
		return !in_array(false, $SuccessArr);	
			
	}
	
	function Delete_Award_Import_Temp_Data()
	{
		
		$sql = "Delete from TEMP_READING_SCHEME_AWARD_IMPORT Where UserID = '".$_SESSION["UserID"]."' ";
			$SuccessArr['DeleteTempImportCsvData'] = $this->db_db_query($sql);
			
		return !in_array(false, $SuccessArr);	
			
	}
	
	function enableReadingSchemeAddPopularDataTogetherRight(){
		global $sys_custom;	
		return $sys_custom['ReadingSchemeAddPopularDataTogether'];
	}
	
	
	function enableReadingSchemeEditISBNByStudentRight(){
		global $sys_custom;	
		return $sys_custom['ReadingGarden']['ISBNbyStudent'];	
	}
	
	function IsMadatory($FieldName){
		global $sys_custom;
		if (is_array($sys_custom['ReadingGarden']['Madatory']) && sizeof($sys_custom['ReadingGarden']['Madatory'])>0)
		{
			return in_array($FieldName, $sys_custom['ReadingGarden']['Madatory']);		
		} else
		{
			return false;
		}
		
	}
}

?>