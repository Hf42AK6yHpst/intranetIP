<?php
// Editing by Bill

/*
 * 2020-07-09 (Bill):   [Ho Yu College and Primary School]      [2020-0707-1405-58098]
 *                      - modified getHoYuAnnualDisciplineReport(), for modification of report layout
 * 2020-01-14 (Bill):   [NT Heung Yee Kuk Tai Po District Secondary School]     [2020-0113-1036-14207]
 *                      - modified getHYKPersonalReport(), to handle page break if records with very long remarks
 * 2019-03-12 (Bill):   [NT Heung Yee Kuk Tai Po District Secondary School]     [2018-0710-1351-39240]
 *                      - added getGeneratedNoticeLogSelection(), getHYKDemeritLinkedNoticeListTable(), support Mgmt > Print Punishment Warning Letter
 *                      - modified getHYKClassSummaryReport(), getHYKPersonalReport(), support Export as Word Document
 * 2018-12-18 (Bill):   [Tsuen Wan Public Ho Chuen Yiu Memorial College] - added getHCYClassRecordSummary()
 * 2018-12-11 (Bill):   [Ho Yu College and Primary School] - modified getHoYuAnnualDisciplineReport(), get current principal only
 * 2018-03-02 (Bill):   [Tin Ka Ping Secondary School] - modified getTKPSchoolRecordSummary(), to support Rainbow Scheme stat data display
 * 2018-02-07 (Isaac):  [SKH Tsang Shiu Tim Secondary School] - added table seperation by class to getDetentionPunishmentListTable();
 * 2018-01-25 (Bill):	[NT Heung Yee Kuk Tai Po District Secondary School] - modified getHYKClassSummaryReport() - support cross terms score calculation
 * 2017-03-01 (Bill):	[NT Heung Yee Kuk Tai Po District Secondary School] - added getHYKPersonalReport()
 * 2017-01-16 (Bill):	[HKSYC & IA Chan Nam Chong Memorial College] - added getStudentLateMeritReport()
 * 2017-01-11 (Bill):	[Tung Wah Group Of Hospitals Wong Fut Nam College] - added getWFNDetentionReport(), getWFNConductPunishmentReport()
 * 2016-11-24 (Bill):	[Po Leung Kuk C W Chu College] - modified getCWCPunishmentRecord() - to handle page break
 * 2016-11-01 (Bill):	[NT Heung Yee Kuk Tai Po District Secondary School] - added getHYKMeritStatisticsReport()
 * 2016-09-27 (Bill):   [NT Heung Yee Kuk Tai Po District Secondary School] - added getHYKClassSummaryReport()
 * 2016-09-21 (Anna):   [Escola Secundaria Pui Ching de Macau] - modified getMoPuiChingDailyReport() - add AP remarks after item title when submit report
 * 2016-09-15 (Bill):	[Escola Secundaria Pui Ching de Macau] - modified getMoPuiChingDailyReport() - to adjust row counting for page break
 * 2016-07-04 (Bill):	[Buddhist Fat Ho Memorial College] - modified getIndividualDisciplineRecord(), to fix Chinese character first case
 * 2015-11-11 (Bill):	[Jockey Club Government Secondary School] added getMonthlySummary() - to export Monthly Punishment Summary
 * 2015-09-25 (Bill):	[HKUGA College] added getPresetReasonTableHtml() - to display table of Waive / Redeem Preset Reason
 * 2015-09-16 (Bill):	[Escola Secundaria Pui Ching de Macau] - modified getMoPuiChingDailyReport() - to update report display
 * 2015-07-29 (Bill):	[Po Leung Kuk C W Chu College] - added getClassStudentImageTable(), getCWCPunishmentCount(), getCWCPunishmentRecord()
 * 2015-06-23 (Bill):	[Tin Ka Ping Secondary School] - added getTKPSchoolRecordSummary(), getTKPClassDemeritRecord()
 * 2015-06-02 (Bill):	[Escola Secundaria Pui Ching de Macau] - modified getMoPuiChingDailyReport() - to display content of Report of Award & Punishment Record
 * 2015-03-06 (Bill):	[Ho Yu College And Primary School] - modifed getHoYuAnnualDisciplineReport() - not display redeemed punishment record
 * 2015-02-09 (Bill):	[Ho Yu College And Primary School] - modifed getHoYuAnnualDisciplineReport() - to order AP Item Name
 * 2015-02-03 (Bill):	[Ho Yu College And Primary School] - modifed getHoYuAnnualDisciplineReport() - to group by AP Item Name
 * 2015-01-29 (Carlos): modified getAttendanceAwardPunishmentReport() (A) exclude waived absence records
 * 2015-01-23 (Bill):	[Ho Yu College And Primary School] - modifed getHoYuAnnualDisciplineReport() - to group AP records
 * 2015-01-12 (Bill):	[Ho Yu College And Primary School] - added getHoYuDemeritRecordReport(), getHoYuAnnualDisciplineReport()
 * 2015-01-07 (Carlos): Fix getAttendanceAwardPunishmentReport() Type (D) AP records merit type and merit count
 * 2014-11-12 (Bill):	[Buddhist Fat Ho Memorial College] - added getIndividualDisciplineRecord()
 * 2014-11-06 (Bill):	[Pooi To Middle School] - modified getAttendanceAwardPunishmentReport() to display correct number of Misconduct statistics
 * 2014-06-04 (Carlos): $sys_custom['eDiscipline']['PooiToMiddleSchool'] - added  getAttendanceAwardPunishmentReport(), getEmailNotificationRecordList(), getEmailNotificationDiv()
 * 2013-06-21 (Carlos): [HONG KONG TAOIST ASSOCIATION THE YUEN YUEN INSTITUTE NO.3 SECONDARY SCHOOL] - added displayMaskedMark(), getConductDownGradeSettingTable()
 * 2013-06-03 (Carlos): [TWGHs Chen Zao Men College] - added getDetentionNoticeTable() and getDetentionPunishmentListTable()
 */

include_once("libdisciplinev12_ui.php");

class libdisciplinev12_ui_cust extends libdisciplinev12_ui
{
	function libdisciplinev12_ui_cust()
	{
		parent::libdisciplinev12_ui();
	}
	
	// [TWGH CHEN ZAO MEN COLLEGE] - Start
	function getDetentionNoticeTable($DetentionDate, $YearClassIdAry, $OptionsAry)
	{
		global $Lang, $ldiscipline;
		
		$showSignature = $OptionsAry['Signature'] == 1;
		$records = $ldiscipline->getDetentionNoticeStudents($DetentionDate, $YearClassIdAry);
		//debug_r($records);
		$record_count = count($records);
		$class_count = count($YearClassIdAry);
		
		$classIdToRecords = array();
		for($i=0;$i<$record_count;$i++){
			$class_id = $records[$i]['YearClassID'];
			
			if(!isset($classIdToRecords[$class_id])) {
				$classIdToRecords[$class_id] = array();
			}
			$classIdToRecords[$class_id][] = $records[$i];
		}
		
		$x = '';
		$x .= '<table width="96%" align="center" class="print_hide" border="0">
				<tbody>
					<tr>
						<td align="right">'.$this->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
					</tr>
				</tbody>
				</table>'."\n";
		
		$num = 0;
		for($i=0;$i<$class_count;$i++) {
			$class_id = $YearClassIdAry[$i];
			$student_count = count($classIdToRecords[$class_id]);
			if($student_count==0) {
				continue;
			}
			
			$studentAry = $classIdToRecords[$class_id];
			$class_title = $studentAry[0]['ClassTitle'];
			
			$x .= '<div style="page-break-inside:avoid; page-break-after:auto; display:block;">';
			$x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center" class="OuterTable">';
			$x .= '<tbody>';
			$x .= '<tr>
					<td>'."\n";
			
			$x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tbody>
					 <tr>
						<td style="text-align:center"><h1><b>'.$Lang['eDiscipline']['DetentionNotice'].'</b></h1></td>
					 </tr>
					</tbody>
				   </table>'."\n";
			
			$x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tbody>
					 <tr>
						<td style="text-align:left;font-size:16px;">'.$Lang['General']['Date'].': '.$DetentionDate.'&nbsp;&nbsp;&nbsp;&nbsp;'.$Lang['General']['Class'].': '.$class_title.'</td>
					 </tr>
					</tbody>
				   </table>'."\n";
			$x .= '<br />';
			$x .= '<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tbody>
					 <tr>
						<td style="text-align:left;font-size:16px;">'.$Lang['eDiscipline']['DetentionNoticeMessage'].'</td>
					 </tr>
					</tbody>
				   </table>'."\n";
			$x .= '<br />';
			$x .= '<table width="93%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tbody>
					 <tr>
						<th width="10%" style="text-align:left;font-size:14px;">'.$Lang['General']['ClassNumber'].'</th>
						<th width="20%" style="text-align:left;font-size:14px;">'.$Lang['eDiscipline']['MunSangStudentName'].'</th>
						<th width="70%" style="text-align:left;font-size:14px;">'.$Lang['eDiscipline']['FieldTitle']['DetentionReason'].'</th>
					 </tr>'."\n";
					 
			for($j=0;$j<$student_count;$j++) {
				$class_number = $studentAry[$j]['ClassNumber'];
				$student_name = $studentAry[$j]['StudentName'];
				$reason = trim($studentAry[$j]['Reason']) != '' ? $studentAry[$j]['Reason'] : $studentAry[$j]['Remark'];
				
				$x .= '<tr>';
					$x .= '<td style="text-align:left;font-size:14px;">'.$class_number.'</td>';
					$x .= '<td style="text-align:left;font-size:14px;">'.$student_name.'</td>';
					$x .= '<td style="text-align:left;font-size:14px;">'.$reason.'</td>';
				$x .= '</tr>';
			}
				$x .= '</tbody>';
			$x .= '</table>'."\n";
			$x .= '<br />';
			$x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5" align="right">
					<tbody>
					 <tr>
						<td style="text-align:right;font-size:18px;">'.$Lang['eDiscipline']['DisciplineGroup'].'</td>
					 </tr>
					</tbody>
				   </table>'."\n";
			$x .= '<div style="height:90px;width:100%;display:block;"></div>';
			
			if($showSignature) {
				$x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
						<tbody>
						 <tr>
							<td nowrap style="text-align:left;font-size:16px;">'.$Lang['eDiscipline']['AfterClassTime'].': _______________</td>
							<td nowrap style="text-align:right;font-size:16px;">'.$Lang['eDiscipline']['TeachersSignature'].': __________________________</td>
						 </tr>
						 <tr>
							<td colspan="2">&nbsp;</td>
						 </tr>
						 <tr>
							<td colspan="2" style="text-align:left;font-size:16px;">'.$Lang['eDiscipline']['DetentionNoticeReminder'].'</td>
						 </tr>
						</tbody>
					   </table>'."\n";
				$x .= '';
			}
			
			$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="border-bottom:dashed 1px black;">
					<tbody>
					 <tr>
						<td>&nbsp;</td>
					 </tr>
					 <tr>
						<td>&nbsp;</td>
					 </tr>
					</tbody>
				   </table>'."\n";
			
			$x .= '<div style="height:50px;width:100%;display:block;"></div>'."\n";
			
			
			$x .= '</td>
				</tr>';
			$x .= '</tbody>
				</table>'."\n";
			$x .= '</div>'."\n";
			
			$num++;
		}
		
		return $x;
	}
	
	function getDetentionPunishmentListTable($StartDate, $EndDate, $TargetType, $TargetIdAry, $AttendanceStatusAry, $NumOfTimes, $Format='', $PrintNewPagePerClass="")
	{
		global $Lang, $ldiscipline;
		
		$Format = strtoupper($Format);
		if($Format == 'CSV') {
			$lexport = new libexporttext();
			$headers = array($Lang['General']['Class'],$Lang['General']['ClassNumber'],$Lang['General']['Name'],$Lang['eDiscipline']['DetentionTimes'],$Lang['eDiscipline']['DetentionDateAndEvent']);
			$rows = array();
		}
		
		$records = $ldiscipline->getDetentionPunishmentStudents($StartDate, $EndDate, $TargetType, $TargetIdAry, $AttendanceStatusAry, $NumOfTimes);
		$record_count = count($records);
		
// 		$x = '';
		
// 		$x .= '<table width="100%" align="center" class="print_hide" border="0">
// 				<tr>
// 					<td align="right">'.$this->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
// 				</tr>
// 			  </table>'."\n";
			  
// 		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">';
// 			$x .= '<tbody>';
// 				$x .= '<tr><td style="text-align:center;font-weight:bold;"><h2>'.$Lang['eDiscipline']['DetentionPunishmentList'].'</h2></td></tr>';
// 			$x .= '</tbody>';
// 		$x .= '</table>'."\n";
// 		$x .= '<br />'."\n";
// 		$x .= '<table class="print_table" width="100%" align="center">'."\n";
// 		$x .= '<thead>'."\n";
// 			$x .= '<tr>'."\n";
// 				$x .= '<th width="20%">'.$Lang['General']['Class'].'</th>'."\n";
// 	            $x .= '<th width="10%">'.$Lang['General']['ClassNumber'].'</th>'."\n";
// 	            $x .= '<th width="20%">'.$Lang['General']['Name'].'</th>'."\n";
// 	            $x .= '<th width="10%">'.$Lang['eDiscipline']['DetentionTimes'].'</th>'."\n";
// 	            $x .= '<th width="40%">'.$Lang['eDiscipline']['DetentionDateAndEvent'].'</th>'."\n";
// 	        $x .= '</tr>'."\n";
// 	    $x .= '</thead>'."\n";
	    
// 	    $x .= '<tbody>'."\n";
		
		$tprint .= '<table width="100%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$this->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
				</tr>
			  </table>'."\n";
		$reportHeader = '';
		$reportHeader.= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">';
		$reportHeader .= '<tbody>';
		$reportHeader .= '<tr><td style="text-align:center;font-weight:bold;"><h2>'.$Lang['eDiscipline']['DetentionPunishmentList'].'</h2></td></tr>';
		$reportHeader .= '</tbody>';
		$reportHeader .= '</table>'."\n";
		$reportHeader .= '<br />'."\n";
		$reportHeader .= '<table class="print_table" width="100%" align="center">'."\n";
		$reportHeader .= '<thead>'."\n";
		$reportHeader .= '<tr>'."\n";
		$reportHeader .= '<th width="20%">'.$Lang['General']['Class'].'</th>'."\n";
		$reportHeader .= '<th width="10%">'.$Lang['General']['ClassNumber'].'</th>'."\n";
		$reportHeader .= '<th width="20%">'.$Lang['General']['Name'].'</th>'."\n";
		$reportHeader .= '<th width="10%">'.$Lang['eDiscipline']['DetentionTimes'].'</th>'."\n";
		$reportHeader .= '<th width="40%">'.$Lang['eDiscipline']['DetentionDateAndEvent'].'</th>'."\n";
		$reportHeader .= '</tr>'."\n";
		$reportHeader .= '</thead>'."\n";
		
		$reportHeader .= '<tbody>'."\n";
		$x = $tprint.$reportHeader;
	    	if($record_count == 0){
	    		$x .= '<tr><td colspan="5">'.$Lang['General']['NoRecordFound'].'</td></tr>'."\n";
	    	}else{
	    	    $break = '';
		    	foreach($records as $student_id => $student_records) {
		    	    $endPTag = '';
		    	    #For first record
		    	    if ($PrintNewPagePerClass){
		    	        if($student_records == reset($records)){
		    	            $Class = $student_records[0]['ClassTitle'];  
		    	        #
		    	        }elseif (($student_records[0]['ClassTitle'] != $Class)){
		    	            $Class = $student_records[0]['ClassTitle'];
		    	            $x .= '</tbody>'."\n";
		    	            $x .= '</table>'."\n";
		    	            $x .= '<p class="breakhere"></p>'."\n";
		    	            $x .= $reportHeader;
		    	        }
		    	    }
		    		$rowspan = count($student_records);
		    		$detail = trim($student_records[0]['Reason']) != '' ? $student_records[0]['Reason'] : $student_records[0]['Remark'];
		    		$x .= '<tr>';
		    			$x .= '<td rowspan="'.$rowspan.'">'.$student_records[0]['ClassTitle'].'</td>';
		    			$x .= '<td rowspan="'.$rowspan.'">'.$student_records[0]['ClassNumber'].'</td>';
		    			$x .= '<td rowspan="'.$rowspan.'">'.$student_records[0]['StudentName'].'</td>';
		    			$x .= '<td rowspan="'.$rowspan.'">'.$rowspan.'</td>';
		    			$x .= '<td>'.$student_records[0]['DetentionDate'].' - '.$detail.'</td>';
		    		$x .= '</tr>'."\n";
		    		if($Format == 'CSV') {
		    			$row = array($student_records[0]['ClassTitle'],$student_records[0]['ClassNumber'],$student_records[0]['StudentName'],$rowspan,$student_records[0]['DetentionDate'].' - '.$detail);
		    			$rows[] = $row;
		    		}
		    		for($i=1;$i<$rowspan;$i++){
		    			$detail = trim($student_records[$i]['Reason']) != '' ? $student_records[$i]['Reason'] : $student_records[0]['Remark'];
		    			$x .= '<tr>';
		    				$x .= '<td>'.$student_records[$i]['DetentionDate'].' - '.$detail.'</td>';
		    			$x .= '</tr>'."\n";
		    			
		    			if($Format == 'CSV') {
		    				$row = array(' ',' ',' ',' ',$student_records[0]['DetentionDate'].' - '.$detail);
		    				$rows[] = $row;
		    			}
		    		}
		    	}
	    	}
	    $x .= '</tbody>'."\n";
	    
	    $x .= '</table>'."\n";
	    $x .= '<br />';
	    
	    if($Format == 'CSV'){
	    	$filename = $Lang['eDiscipline']['DetentionPunishmentList'].".csv";
			$export_content = $lexport->GET_EXPORT_TXT($rows, $headers,"","\r\n","",0,"11");
			$lexport->EXPORT_FILE($filename, $export_content);
			return;
	    }
	    
	    return $x;
	}
	// [TWGH CHEN ZAO MEN COLLEGE] - End
	
	// [HONG KONG TAOIST ASSOCIATION THE YUEN YUEN INSTITUTE NO.3 SECONDARY SCHOOL] - Start
	function displayMaskedMark($Mark, $css='')
	{
		$Mark = trim($Mark);
		if($Mark == '') {
			return Get_String_Display($Mark);
		}
		
		if($css != '') {
			$span_open = '<span style="'.$css.'">';
			$span_close = '</span>';
		}
		$maskSymbol = $span_open.'***'.$span_close;
		return $maskSymbol;
	}
	
	function getConductDownGradeSettingTable()
	{
		global $Lang, $ldiscipline;
		global $i_Discipline_System_CategoryName;
		
		$records = $ldiscipline->GetConductDownGradeConditionRecords();
		$record_count = count($records);
		
		$x .= '<table class="common_table_list_v30" width="100%" align="center" border="0" cellspacing="0" cellpadding="4">';
			$x .= '<thead>';
				$x .= '<tr>';
				$x .= '<th width="30%" class="tabletop tabletopnolink">'.$i_Discipline_System_CategoryName.'</th>';
				$x .= '<th width="40%" class="tabletop tabletopnolink">'.$Lang['eDiscipline']['sdbnsm_ap_item'].'</th>';
				$x .= '<th width="20%" class="tabletop tabletopnolink">'.$Lang['eDiscipline']['Quantity'].'</th>';
				//$x .= '<th width="1" class="tabletop tabletopnolink"><input type="checkbox" onclick="(this.checked)?setChecked(1,this.form,\'ItemID[]\'):setChecked(0,this.form,\'ItemID[]\')" name="checkmaster"></th>';
				$x .= '<th width="10%" class="tabletop tabletopnolink">&nbsp;</th>';
				$x .= '</tr>';
			$x .= '</thead>';
		
			$x .= '<tbody>';
			for($i=0;$i<$record_count;$i++) {
				$item_id = $records[$i]['ItemID'];
				
				//$edit_span = '<span class="table_row_tool row_content_tool"><a onclick="jsEditItem(this);return false;" title="'.$Lang['Btn']['Edit'].'" class="edit_dim" href="javascript:void(0);"></a></span>';
				$delete_span = '<span class="table_row_tool row_content_tool"><a onclick="jsDeleteItem('.$item_id.'); return false;" title="'.$Lang['Btn']['Delete'].'" class="delete_dim" href="javascript:void(0);"></a></span>';
				
				$edit_submit_btn = $this->GET_SMALL_BTN($Lang['Btn']['Submit'], "button", $ParOnClick="jsSubmitEditItem($item_id);", $ParName="", $ParOtherAttribute="", $OtherClass="", $ParTitle='').'&nbsp;';
				$edit_cancel_btn = $this->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", $ParOnClick="jsCancelEditItem($item_id);", $ParName="", $ParOtherAttribute="", $OtherClass="", $ParTitle='');
				
				$x .= '<tr>';
					$x .= '<td>'.$records[$i]['CategoryName'].'</td>';
					$x .= '<td>'.$records[$i]['ItemName'].'</td>';
					$x .= '<td>';
						$x .= '<div id="DisplayQuantityDiv_'.$item_id.'" onmouseover="jsShowEditBackground(this);" onmouseout="jsHideEditBackground(this);" title="'.$Lang['Btn']['Edit'].'" onmouseup="jsEditQuantity('.$item_id.');">'.$records[$i]['Quantity'].'</div>';
						$x .= '<div id="EditQuantityDiv_'.$item_id.'" style="display:none;">';
							$x .= '<input type="text" id="Quantity_'.$item_id.'" name="Quantity_'.$item_id.'" value="'.$records[$i]['Quantity'].'" class="textboxnum" size="5" onchange="jsOnChangeQuantity(this);" onkeyup="jsSubmitEditItemByEnterKey(event,'.$item_id.');" />&nbsp;';
							$x .= '<input type="hidden" id="OrigQuantity_'.$item_id.'" name="OrigQuantity_'.$item_id.'" value="'.$records[$i]['Quantity'].'" />';
							//$x .= '<input type="hidden" id="ItemID_'.$item_id.'" name="ItemID_'.$item_id.'" value="'.$item_id.'" />';
							$x .= $edit_submit_btn.$edit_cancel_btn;
							$x .= '<br /><span style="display:none;color:red;">'.$Lang['eDiscipline']['WarningMsgArr']['InvalidQuantity'].'</span>';
						$x .= '</div>';
					$x .= '</td>';
					$x .= '<td>'.$delete_span.'</td>';
				$x .= '</tr>';
			}
				if($record_count == 0) {
					$x .= '<tr><td colspan="4" align="center" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
				}
				$x .= '<tr id="AddToolRow">';
					$x .= '<td colspan="4">';
						$x .= '<div class="table_row_tool row_content_tool"><a title="'.$Lang['Btn']['Add'].'" class="add_dim" onclick="jsAddRow(); return false;" href="javascript:void(0);"></a></div>';
					$x .= '</td>';
				$x .= '</tr>';
			$x .= '</tbody>';
		$x .= '</table>';
		
		return $x;
	}
	
	// [HONG KONG TAOIST ASSOCIATION THE YUEN YUEN INSTITUTE NO.3 SECONDARY SCHOOL] - End
	
	// [Pooi To Middle School] - $sys_custom['eDiscipline']['PooiToMiddleSchool']
	function getAttendanceAwardPunishmentReport($options)
	{
		global $intranet_root, $Lang,  $PATH_WRT_ROOT, $ldiscipline;
		global $i_Merit_NoAwardPunishment, $i_Merit_Warning, $i_Merit_BlackMark, $i_Merit_MinorDemerit, $i_Merit_MajorDemerit, $i_Merit_SuperDemerit, $i_Merit_UltraDemerit;
		
		$format = $options['format'];
		$rankTarget = $options['rankTarget'];
		$radioPeriod = $options['radioPeriod']; // YEAR || DATE
		$selectYear = $options['selectYear']; 
		$selectSemester = $options['selectSemester'];
		$textFromDate = $options['textFromDate'];
		$textToDate = $options['textToDate'];
		
		if ($radioPeriod == "YEAR") 
		{
			$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			$textFromDate = date("Y-m-d", strtotime($SQL_startdate));
			$textToDate = date("Y-m-d", strtotime($SQL_enddate));
		}
		else
		{
			$SQL_startdate = $textFromDate;
			$SQL_enddate = $textToDate;
			/*
			$sql = "SELECT DISTINCT ay.AcademicYearID  
					FROM ACADEMIC_YEAR as ay 
					LEFT JOIN ACADEMIC_YEAR_TERM as ayt ON (ay.AcademicYearID=ayt.AcademicYearID) 
					WHERE ayt.TermStart>='$SQL_startdate 00:00:00' AND ayt.TermEnd<='$SQL_enddate 23:59:59'";
			$tmp = $ldiscipline->returnVector($sql);
			$selectYear = $tmp[0];
			*/
			$selectYear = Get_Current_Academic_Year_ID();
			$selectSemester = 0;
		}
		
		if($rankTarget == 'form'){
			$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
		}else if($rankTarget == 'class'){
			$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
		}else if($rankTarget == 'student'){
			$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
			$studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$options['studentID'])."')";
		}
		
		$misconductToPunishmentItemId = $ldiscipline->MisconductToPunishmentItem;
		$lateToPunishmentItemId = $ldiscipline->LateToPunishmentItem;
		
		$EmptySymbol = $Lang['General']['EmptySymbol'];
		$StylePrefix = '<span class="tabletextrequire">';
		$StyleSuffix = '</span>';
		$AcademicYearNameField = Get_Lang_Selection('ay.YearNameB5','ay.YearNameEN');
		//$SemesterNameField = Get_Lang_Selection('ayt.YearTermNameB5' ,'ayt.YearTermNameEN');
		$YearClassNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
		$StudentNameField = getNameFieldByLang("iu.");
		
		$clsName = $YearClassNameField;
		$sql = "SELECT 
					iu.UserID,
					$YearClassNameField as ClassName,
					ycu.ClassNumber,
					CONCAT(
						IF($clsName IS NULL OR $clsName='' Or ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
						$StudentNameField
					) as StudentName 
				FROM INTRANET_USER as iu 
				LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=iu.UserID 
				LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$selectYear' 
				LEFT JOIN YEAR as y ON y.YearID=yc.YearID 
				WHERE yc.AcademicYearID = '".$selectYear."' 
				$yearIdCond $classIdCond $studentIdCond 
				GROUP BY iu.UserID 
				ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";
		$students = $ldiscipline->returnResultSet($sql);
		$student_count = count($students);
		//debug_r(htmlspecialchars($sql,ENT_QUOTES));
		if($format == 'web'){
			$x = '<div class="content_top_tool">
					<div class="Conntent_tool">
						<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
						<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
					</div>
					<br style="clear:both" />
				  </div><br />';
		}
		
		if($format == 'print'){
			$x .= '<table width="100%" align="center" class="print_hide" border="0">
					<tr>
						<td align="right">'.$this->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print").'</td>
					</tr>
					</table>';
		}
		
		if($format == 'csv'){
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			$lexport = new libexporttext();
				
			$exportContent = '';
		}
		
		if($format == 'print'){
			$school_name = GET_SCHOOL_NAME();
		}
		
		for($i=0;$i<$student_count;$i++)
		{
			$student_id = $students[$i]['UserID'];
			$student_name = $students[$i]['StudentName'];
			$class_name = $students[$i]['ClassName'];
			$class_number = $students[$i]['ClassNumber'];
			if($class_number != ''){
				$student_name .= ' ('.$class_number.')';
			}
			
			if($format == 'print'){
				$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
							<tr>
								<td align="center"><h2>'.$school_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Title'].'</h2></td>
							</tr>
						</table>';
			}
			
			$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td width="30%" align="left">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['StudentName'].' : <u>'.$student_name.'</u></td>
							<td width="20%" align="center">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ClassName'].' : '.$class_name.'</td>
							<td width="50%" align="right">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['StatisticsDate'].' : '.$textFromDate.' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['To'].' '.$textToDate.'</td>
						</tr>
					</table>';
			
			if($format == 'csv'){
				$header = array('',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['StudentName'].' : '.$student_name,$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ClassName'].' : '.$class_name,$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['StatisticsDate'].' : '.$textFromDate.' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['To'].' '.$textToDate,'','');
				$rows = array();
			}
			
			// A. Absence Records
			
			$sql = "SELECT 
						r.RecordID,
						DATE_FORMAT(r.RecordDate,'%d/%m/%Y') as RecordDate,
						r.DayType,
						IF(TRIM(s.AbsentSession)='',0,s.AbsentSession) as AbsentSession,
						r.WaiveAbsent 
					FROM CARD_STUDENT_PROFILE_RECORD_REASON as r 
					LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as s ON s.StudentID=r.StudentID AND s.RecordDate=r.RecordDate AND s.DayType=r.DayType 
					WHERE r.StudentID='$student_id' AND r.RecordType='".PROFILE_TYPE_ABSENT."' 
					 AND r.RecordDate >= '$SQL_startdate' AND r.RecordDate <= '$SQL_enddate' AND (r.RecordStatus IS NULL OR r.RecordStatus='0') 
					ORDER BY r.RecordDate,r.DayType";
			$absent_records = $ldiscipline->returnResultSet($sql);
			$absent_record_count = count($absent_records);
			$date_to_records = array();
			$total_absent_day = 0; // both AM and PM have absent sessions, count as whole day
			$total_session = 0; // non whole day, count the rest
			$total_unwaive = 0; // number of unwaived absent records
			//debug_r($sql);
			for($j=0;$j<$absent_record_count;$j++) {
				$record_date = $absent_records[$j]['RecordDate'];
				$day_type = $absent_records[$j]['DayType'];
				if(!isset($date_to_records[$record_date])){
					$date_to_records[$record_date] = array();
					$date_to_records[$record_date]['daytype'] = array();
					$date_to_records[$record_date]['session'] = 0;
					$date_to_records[$record_date]['waive'] = false;
				}
				//$date_to_records[$record_date]['record'] = $absent_records[$j];
				$date_to_records[$record_date]['daytype'][] = $day_type;
				$date_to_records[$record_date]['session'] += $absent_records[$j]['AbsentSession'];
				$date_to_records[$record_date]['waive'] = $date_to_records[$record_date]['waive'] || $absent_records[$j]['WaiveAbsent']=='1';
			}
			
			$x .= '<table cellspacing="0" cellpadding="5" border="0" width="100%">';
				$x .= '<tr><td width="3%">A. </td><td colspan="3" width="97%">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['AbsenceRecords'].'</td></tr>';
				
				$table = array();
				$table[0] = '<table cellspacing="0" cellpadding="4" border="0" width="100%">
							<tr><td width="10%" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Times'].'</td><td width="80%" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['AbsentDate'].'('.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Sessions'].')</td><td width="10%" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['WaiveMarker'].'</td></tr>';
				$table[1] = '<table cellspacing="0" cellpadding="4" border="0" width="100%">
							<tr><td width="10%" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Times'].'</td><td width="80%" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['AbsentDate'].'('.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Sessions'].')</td><td width="10%" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['WaiveMarker'].'</td></tr>';
				$table[2] = '<table cellspacing="0" cellpadding="4" border="0" width="100%">
							<tr><td width="10%" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Times'].'</td><td width="80%" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['AbsentDate'].'('.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Sessions'].')</td><td width="10%" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['WaiveMarker'].'</td></tr>';			
				
				if($format == 'csv'){
					$row = array('A.',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['AbsenceRecords'],'','','','');
					$rows[] = $row;
					$row = array('',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Times'],$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['AbsentDate'].'('.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Sessions'].')',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['WaiveMarker'],'','');
					$rows[] = $row;
				}
				
				$date_keys = array_keys($date_to_records);
				$date_count = count($date_keys);
				for($d=0;$d<$date_count;$d++){
					$ind = $d % 3;
					$date_key = $date_keys[$d];
					$record = $date_to_records[$date_key];
					$day_session = '0';
					$waive_yn = '0';
					
					if(in_array(PROFILE_DAY_TYPE_AM,$record['daytype']) && in_array(PROFILE_DAY_TYPE_PM,$record['daytype'])){
						$total_absent_day += 1;
						$day_session = 'W';
					}else{
						$total_session += $record['session'];
						$day_session = $record['session'];
					}
					if(!$record['waive']){
						$total_unwaive += 1;
						$waive_yn = '0';
					}else{
						$waive_yn = '1';
					}
					
					$table[$ind] .= '<tr><td align="center">'.count($record['daytype']).'</td><td>'.$date_key.'('.$day_session.')'.'</td><td align="center">'.$waive_yn.'</td></tr>';
				
					if($format == 'csv'){
						$row = array('',count($record['daytype']),$date_key.'('.$day_session.')',$waive_yn,'','');
						$rows[] = $row;
					}
				}
				
				$table[0] .= '</table>';
				$table[1] .= '</table>';
				$table[2] .= '</table>';
				
				$x .= '<tr><td>&nbsp;</td><td valign="top">'.$table[0].'</td><td valign="top">'.$table[1].'</td><td valign="top">'.$table[2].'</td></tr>';
				
				$x .= '<tr><td>&nbsp;</td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['AbsentStatistics'].': </td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['TotalAbsentDaysAndSessions'].' <u>'.$total_absent_day.'</u> '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Day'].' <u>'.$total_session.'</u> '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Session'].'</td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['TotalUnwaived'].' <u>'.$total_unwaive.'</u></td></tr>';
				
				if($format == 'csv'){
					$row = array('',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['AbsentStatistics'].':',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['TotalAbsentDaysAndSessions'].' '.$total_absent_day.' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Day'].' '.$total_session.' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Session'],$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['TotalUnwaived'].' '.$total_unwaive,'','');
					$rows[] = $row;
					$row = array('','','','','','');
					$rows[] = $row;
				}
				
			$x .= '</table>';
			$x .= '<br />';
			
			// find GM records
			$sql ="SELECT a.RecordID, a.ItemID, a.CategoryID, a.UpgradedRecordID, a.StudentAttendanceID, DATE_FORMAT(a.RecordDate,'%d/%m/%Y') as RecordDate, a.ScoreChange, c.Name, c.ItemCode 
					FROM DISCIPLINE_ACCU_RECORD as a 
					LEFT JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as c ON c.ItemID=a.ItemID 
					WHERE a.StudentID='$student_id' AND a.RecordType=-1 AND a.RecordStatus='". DISCIPLINE_STATUS_APPROVED ."' AND a.RecordDate >= '$SQL_startdate' AND a.RecordDate <= '$SQL_enddate' 
					ORDER BY a.RecordDate";
			$gm_records = $ldiscipline->returnResultSet($sql);
			$gm_record_count = count($gm_records);
			
			$late_records = array();
			$total_late = 0;
			$converted_late_ap_ary = array();
			$unconverted_late_ap = 0;
			
			$misconduct_records = array();
			$total_misconduct = 0;
			$converted_ap_ary = array();
			$unconverted_ap_score = 0;
			
			$upgrade_id_to_ary = array();
			
			for($k=0;$k<$gm_record_count;$k++){
				if($gm_records[$k]['CategoryID'] == PRESET_CATEGORY_LATE) {
					$late_records[] = $gm_records[$k];
					$total_late += 1;
					if($gm_records[$k]['UpgradedRecordID']!='' && $gm_records[$k]['UpgradedRecordID'] > 0){
						$converted_late_ap_ary[] = $gm_records[$k]['UpgradedRecordID'];
					}else{
						$unconverted_late_ap += 1;
					}
				}else{
					$misconduct_records[] = $gm_records[$k];
					// Commented: To prevent incorrect misconduct statistics
//					$total_misconduct += 1;
//					if($gm_records[$k]['UpgradedRecordID']!='' && $gm_records[$k]['UpgradedRecordID'] > 0){
//						$converted_ap_ary[] = $gm_records[$k]['UpgradedRecordID'];
//					}else{
//						$unconverted_ap_score += $gm_records[$k]['ScoreChange'];
//					}
				}
				if($gm_records[$k]['UpgradedRecordID']!='' && $gm_records[$k]['UpgradedRecordID'] > 0){
					if(!isset($upgrade_id_to_ary[$gm_records[$k]['UpgradedRecordID']])){
						$upgrade_id_to_ary[$gm_records[$k]['UpgradedRecordID']] = array();
					}
					$upgrade_id_to_ary[$gm_records[$k]['UpgradedRecordID']][] = $gm_records[$k];
				}
			}
			
			$converted_late_ap_ary = array_values(array_unique($converted_late_ap_ary));
//			$converted_ap_ary = array_values(array_unique($converted_ap_ary));
			
			// B. Late Records
			$x .= '<table cellspacing="0" cellpadding="5" border="0" width="100%">';
				$x .= '<tr><td width="3%">B. </td><td colspan="3" width="97%">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['LateRecords'].'</td></tr>';
				
				$table = array();
				$table[0] = '<table cellspacing="0" cellpadding="4" border="0" width="100%">
							<tr><td width="20%" align="left" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Number'].'</td><td width="80%" align="center" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['LateDate'].'</td></tr>';
				$table[1] = '<table cellspacing="0" cellpadding="4" border="0" width="100%">
							<tr><td width="20%" align="left" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Number'].'</td><td width="80%" align="center" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['LateDate'].'</td></tr>';
				$table[2] = '<table cellspacing="0" cellpadding="4" border="0" width="100%">
							<tr><td width="20%" align="left" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Number'].'</td><td width="80%" align="center" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['LateDate'].'</td></tr>';			
				
				if($format == 'csv'){
					$row = array('B.',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['LateRecords'],'','','','');
					$rows[] = $row;
					$row = array('',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Number'],$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['LateDate'],'','','');
					$rows[] = $row;
				}
				
				
				$late_record_count = count($late_records);
				for($d=0;$d<$late_record_count;$d++){
					$ind = $d % 3;
					$table[$ind] .= '<tr><td align="left">'.($d+1).'</td><td align="center">'.$late_records[$d]['RecordDate'].'</td></tr>';
					
					if($format == 'csv'){
						$row = array('',($d+1),$late_records[$d]['RecordDate']);
						$rows[] = $row;
					}
				}
				
				$table[0] .= '</table>';
				$table[1] .= '</table>';
				$table[2] .= '</table>';
				
				$x .= '<tr><td>&nbsp;</td><td valign="top">'.$table[0].'</td><td valign="top">'.$table[1].'</td><td valign="top">'.$table[2].'</td></tr>';
				
				$x .= '<tr><td>&nbsp;</td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['LateStatistics'].': </td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['TotalLate'].' <u>'.$total_late.'</u> '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Time'].' '.'</td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ConvertedDemeritNumber'].' <u>'.count($converted_late_ap_ary).'</u> '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Unit'].'</td></tr>';
				$x .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan="2">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['UnconvertedDemeritNumber'].' <u>'.$unconverted_late_ap.'</u> '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Time'].'</td></tr>';
				
				if($format == 'csv'){
					$row = array('',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['LateStatistics'].':',
								$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['TotalLate'].' '.$total_late.' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Time'],
								$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ConvertedDemeritNumber'].' '.count($converted_late_ap_ary).' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Unit'],
								$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['UnconvertedDemeritNumber'].' '.$unconverted_late_ap.' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Time'], '');
					$rows[] = $row;
					$row = array('','','','','','');
					$rows[] = $row;
				}
				
			$x .= '</table>';
			$x .= '<br />';
			
			// C. GM Records
			$x .= '<table cellspacing="0" cellpadding="5" border="0" width="100%">';
				$x .= '<tr><td width="3%">C. </td><td colspan="4" width="97%">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MisconductRecords'].'</td></tr>';
				
				$table = '<table cellspacing="0" cellpadding="4" border="0" width="100%">
							<tr><td width="10%" align="left" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Number'].'</td><td width="30%" align="center" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['EventDate'].'</td>
								<td width="50%" align="center" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MisconductItem'].'('.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ItemCode'].')</td><td width="10%" align="center" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Deducted'].'</td></tr>';
				
				if($format == 'csv'){
					$row = array('C.',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MisconductRecords'],'','','','');
					$rows[] = $row;
					$row = array('',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Number'],$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['EventDate'],$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MisconductItem'].'('.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ItemCode'].')',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Deducted'],'');
					$rows[] = $row;
				}
				
				$misconduct_count = count($misconduct_records);
				for($k=0;$k<$misconduct_count;$k++)
				{
					if($misconduct_records[$k]['ScoreChange']>0){
						$total_misconduct += 1;
					}
					if($misconduct_records[$k]['UpgradedRecordID']!='' && $misconduct_records[$k]['UpgradedRecordID'] > 0){
						$converted_ap_ary[] = $misconduct_records[$k]['UpgradedRecordID'];
					}else{
						$unconverted_ap_score += $misconduct_records[$k]['ScoreChange'];
					}
					
					$table .= '<tr><td align="left">'.($k+1).'</td><td align="center">'.$misconduct_records[$k]['RecordDate'].'</td>
									<td align="center">'.($misconduct_records[$k]['Name'].'('.$misconduct_records[$k]['ItemCode'].')').'</td><td align="center">'.$misconduct_records[$k]['ScoreChange'].'</td></tr>';
				
					if($format == 'csv'){
						$row = array('',($k+1),$misconduct_records[$k]['RecordDate'],($misconduct_records[$k]['Name'].'('.$misconduct_records[$k]['ItemCode'].')'),$misconduct_records[$k]['ScoreChange'],'');
						$rows[] = $row;
					}
				}
				
				$converted_ap_ary = array_values(array_unique($converted_ap_ary));
				
				$table .= '</table>';
				
				$x .= '<tr><td>&nbsp;</td><td colspan="4">'.$table.'</td></tr>';
				$x .= '<tr><td>&nbsp;</td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MisconductStatistics'].': </td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['TotalNumberOfMisconduct'].' <u>'.$total_misconduct.'</u> '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Time'].' '.'</td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ConvertedDemeritNumber'].' <u>'.count($converted_ap_ary).'</u> '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Unit'].'</td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['UnconvertedMisconductScore'].' <u>'.$unconverted_ap_score.'</u> '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Marks'].'</td></tr>';
				
				if($format == 'csv'){
					$row = array('',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MisconductStatistics'].':',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['TotalNumberOfMisconduct'].' '.$total_misconduct.' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Time'],$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ConvertedDemeritNumber'].' '.count($converted_ap_ary).' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Unit'],$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['UnconvertedMisconductScore'].' '.$unconverted_ap_score.' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Marks'],'');
					$rows[] = $row;
					$row = array('','','','','','');
					$rows[] = $row;
				}
				
			$x .= '</table>';
			$x .= '<br />';
			
			// D. AP Records
			$sql = "SELECT 
						r.RecordID, 
						DATE_FORMAT(r.RecordDate,'%d/%m/%Y') as RecordDate,
						r.ItemText,
						m.ItemName,
						m.ItemCode,
						IF(r.ProfileMeritCount IS NOT NULL,r.ProfileMeritCount,m.NumOfMerit) as NumOfMerit,
						m.RelatedMeritType,
						r.ProfileMeritType,
						r.Remark 
					FROM DISCIPLINE_MERIT_RECORD as r
					LEFT JOIN DISCIPLINE_MERIT_ITEM as m ON m.ItemID=r.ItemID 
					WHERE r.StudentID='$student_id' AND r.MeritType=-1 AND r.RecordDate>='$SQL_startdate' AND r.RecordDate<='$SQL_enddate' AND r.RecordStatus='".DISCIPLINE_STATUS_APPROVED."' 
					ORDER BY r.RecordDate";
			$ap_records = $ldiscipline->returnResultSet($sql);
			$ap_record_count = count($ap_records);
			//debug_r($sql);
			$total_demerit = 0;
			$total_minor_demerit = 0;
			$total_major_demerit = 0;
			
			$x .= '<table cellspacing="0" cellpadding="5" border="0" width="100%">';
				$x .= '<tr><td width="3%">D. </td><td colspan="4" width="97%">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['SeriousPunishmentRecords'].'</td></tr>';
				
				$x .= '<tr><td>&nbsp;</td><td colspan="4">';
				$x .= '<table cellspacing="0" cellpadding="4" border="0" width="100%">
							<tr><td width="5%" align="left" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Number'].'</td><td width="10%" align="center" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ConversionDate'].'</td>
								<td width="40%" align="center" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MisconductItem'].'('.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ItemCode'].')</td><td width="20%" align="center" style="border-bottom:1px solid black;" nowrap="nowrap">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['PunishmentRecords'].'</td>
								<td width="15%" align="center" style="border-bottom:1px solid black;">'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Remark'].'</td></tr>';
				
				if($format == 'csv'){
					$row = array('D.',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['PunishmentRecords'],'','','','');
					$rows[] = $row;
					$row = array('',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Number'],$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ConversionDate'],$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MisconductItem'].'('.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ItemCode'].')',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['PunishmentRecords'],$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Remark']);
					$rows[] = $row;
				}
				
				for($k=0;$k<$ap_record_count;$k++){
					$ap_id = $ap_records[$k]['RecordID'];
					
					$ap_item_name = $ap_records[$k]['ItemName']!=''? $ap_records[$k]['ItemName'] : $ap_records[$k]['ItemText'];
					if($ap_records[$k]['ItemCode']!=''){
						$ap_item_name .= '('.$ap_records[$k]['ItemCode'].')';
					}
					
					$ap_remark = Get_String_Display('');
					if(isset($upgrade_id_to_ary[$ap_id]) && count($upgrade_id_to_ary[$ap_id])>0){
						$ap_remark = '';
						$delim = '';
						$upgrade_id_to_ary_count = count($upgrade_id_to_ary);
						for($n=0;$n<$upgrade_id_to_ary_count;$n++){
							$ap_remark .= $delim.substr($upgrade_id_to_ary[$ap_id][$n]['RecordDate'],0,5).'('.$upgrade_id_to_ary[$ap_id][$n]['ScoreChange'].')';
							$delim = ',';
							if($format != 'csv'){
								$delim = ',<br>';
							}
						}
					}
					
					$ap_punishment = '';
					if($ap_records[$k]['NumOfMerit']>0){
						switch($ap_records[$k]['ProfileMeritType'])
						{
							//case '-999':
							//	$i_Merit_NoAwardPunishment;
							//break;
							case '0':
								$ap_punishment = $Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Warning'];
							break;
							case '-1':
								$ap_punishment = $Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['BlackMark'];
								$total_demerit += $ap_records[$k]['NumOfMerit'];
							break;
							case '-2':
								$ap_punishment = $Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MinorDemerit'];
								$total_minor_demerit += $ap_records[$k]['NumOfMerit'];
							break;
							case '-3':
								$ap_punishment = $Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MajorDemerit'];
								$total_major_demerit += $ap_records[$k]['NumOfMerit'];
							break;
							case '-4':
								$ap_punishment = $Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['SuperDemerit'];
							break;
							case '-5':
								$ap_punishment = $Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['UltraDemerit'];
							break;
						}
						$ap_punishment .= ' '.$ap_records[$k]['NumOfMerit'].' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Time'];
					}
					
					$x .= '<tr>';
						$x .= '<td align="left">'.($k+1).'</td>';
						$x .= '<td align="center">'.$ap_records[$k]['RecordDate'].'</td>';
						$x .= '<td align="center">'.$ap_item_name.'</td>';
						$x .= '<td align="center">'.$ap_punishment.'</td>';
						$x .= '<td align="center">'.$ap_remark.'</td>';
					$x .= '</tr>';
					
					if($format == 'csv'){
						$row = array('',($k+1),$ap_records[$k]['RecordDate'],$ap_item_name,$ap_punishment,$ap_remark);
						$rows[] = $row;
					}
				}
				
				$x .= '</table>'; 
			
				$x .= '</td></tr>';
				
				$x .= '<tr><td>&nbsp;</td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['PunishmentStatistics'].': </td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['DemeritTotal'].' <u>'.$total_demerit.'</u> '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Unit'].' '.'</td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MinorDemeritTotal'].' <u>'.$total_minor_demerit.'</u> '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Unit'].'</td><td>'.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MajorDemeritTotal'].' <u>'.$total_major_demerit.'</u> '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Unit'].'</td></tr>';
				
				if($format == 'csv'){
					$row = array('',$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['PunishmentStatistics'].':',
									$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['DemeritTotal'].' '.$total_demerit.' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Unit'],
									$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MinorDemeritTotal'].' '.$total_minor_demerit.' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Unit'],
									$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MajorDemeritTotal'].' '.$total_major_demerit.' '.$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Unit'],'');
					$rows[] = $row;
					$row = array('','','','','','');
					$rows[] = $row;
				}
				
			$x .= '</table>';
			
			$x .= '<br /><br />';
			
			if($format == 'print'){
				$x .= '<div style="page-break-after:always">&nbsp;</div>';
			}
			
			if($format == 'csv'){
				$rows[] = array('','','','','','');
				if($i > 0){
					$exportContent .= "\r\n";
				}
				$exportContent .= $lexport->GET_EXPORT_TXT($rows, $header);
			}
		}
		
		if($format == 'csv'){
			$lexport->EXPORT_FILE($Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Title'].".csv", $exportContent);
		}else{
			return $x;
		}
	}

	# [Buddhist Fat Ho Memorial College] - $sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']
	function getIndividualDisciplineRecord($options)
	{
		global $intranet_root, $PATH_WRT_ROOT, $intranet_session_language, $Lang, $ldiscipline;
		
		# POST
		$format = $options['format'];
		$rankTarget = $options['rankTarget'];
		$radioPeriod = $options['radioPeriod'];
		$selectYear = $options['selectYear']; 
		$selectSemester = $options['selectSemester'];
		$textFromDate = $options['textFromDate'];
		$textToDate = $options['textToDate'];
		
		# Report Settings
		$maxRow = 48.5;
		$pageOneHeader = 7.5;
		$pageTwoHeader = 1.5;
		
		# Year && Date
		if ($radioPeriod == "YEAR") 
		{
			$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			$textFromDate = date("Y-m-d", strtotime($SQL_startdate));
			$textToDate = date("Y-m-d", strtotime($SQL_enddate));
		}
		else
		{
			$SQL_startdate = $textFromDate;
			$SQL_enddate = $textToDate;
			/*
			$sql = "SELECT DISTINCT ay.AcademicYearID  
					FROM ACADEMIC_YEAR as ay 
					LEFT JOIN ACADEMIC_YEAR_TERM as ayt ON (ay.AcademicYearID=ayt.AcademicYearID) 
					WHERE ayt.TermStart>='$SQL_startdate 00:00:00' AND ayt.TermEnd<='$SQL_enddate 23:59:59'";
			$tmp = $ldiscipline->returnVector($sql);
			$selectYear = $tmp[0];
			*/
			$selectYear = Get_Current_Academic_Year_ID();
			$selectSemester = 0;
		}
		
		# Rank Target
		if($rankTarget == 'form'){
			$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
		}else if($rankTarget == 'class'){
			$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
		}else if($rankTarget == 'student'){
			$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
			$studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$options['studentID'])."')";
		}
		
		$EmptySymbol = $Lang['General']['EmptySymbol'];
		$StylePrefix = '<span class="tabletextrequire">';
		$StyleSuffix = '</span>';
		if($format == 'csv'){
			$StylePrefix = '';
			$StyleSuffix = '';
		}
		
		# Field for Chinese and English Report
		$AcademicYearNameField 	= 'ay.YearNameB5';						$AcademicYearNameOtherField = 'ay.YearNameEN';
		$YearClassNameField 	= 'yc.ClassTitleB5';					$YearClassNameOtherField 	= 'yc.ClassTitleEN';
		$StudentNameField 		= getNameFieldByLang("iu.", "b5");		$StudentNameOtherField 		= getNameFieldByLang("iu.", "en");
		# Sql for fields
		$AcademicYearNameField 	= "IF (rlang.RecordLang IS NULL, ".$AcademicYearNameField.", ".$AcademicYearNameOtherField.")";
		$YearClassNameField 	= "IF (rlang.RecordLang IS NULL, ".$YearClassNameField.", ".$YearClassNameOtherField.")";
		$StudentNameField 		= "IF (rlang.RecordLang IS NULL, ".$StudentNameField.", ".$StudentNameOtherField.")";
		$clsName 				= $YearClassNameField;
		
		# Get Target Students
		$sql = "SELECT 
					iu.UserID,
					$YearClassNameField as ClassName,
					ycu.ClassNumber,
					CONCAT(
						IF($clsName IS NULL OR $clsName='' Or ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
						$StudentNameField
					) as StudentName,
					IF (rlang.RecordLang IS NULL, 'b5', rlang.RecordLang) as Langs
				FROM INTRANET_USER as iu 
				LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=iu.UserID 
				LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$selectYear')
				LEFT JOIN YEAR as y ON y.YearID=yc.YearID 
				LEFT JOIN DISCIPLINE_STUDENT_REPORT_LANG rlang ON (rlang.StudentID = iu.UserID AND rlang.AcademicYearID='$selectYear')
				WHERE yc.AcademicYearID = '".$selectYear."' 
				$yearIdCond $classIdCond $studentIdCond 
				GROUP BY iu.UserID 
				ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";	
		$students = $ldiscipline->returnResultSet($sql);
		$student_count = count($students);
		
		if($format == 'print'){
			$x .= '<table width="100%" align="center" class="print_hide" border="0">
					<tr>
						<td align="right">'.$this->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print").'</td>
					</tr>
				</table>';
		}
		if($format == 'csv'){
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			$lexport = new libexporttext();			
			$exportContent = '';
		}
	
		# Each Students
		for($i=0; $i<$student_count; $i++)
		{
			$currentRow = $maxRow - $pageOneHeader;
				
			# Student Info
			$student_id = $students[$i]['UserID'];
			$student_name = $students[$i]['StudentName'];
			$class_name = $students[$i]['ClassName'];
			$class_number = $students[$i]['ClassNumber'];
			$class_number = ($class_number != '')? ' ('.$class_number.') ' : '&nbsp;';
			$student_lang = $students[$i]['Langs'];
			
			# Header and Info
			$x .= '<div><div class="main_container">
						<table class="table_header" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
							<tr>
								<td align="center"><h2>'.$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['SchoolName'][$student_lang].'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Title'][$student_lang].' ('.getAcademicYearByAcademicYearID($selectYear).')</h2></td>
							</tr>
						</table>
						<br>
						<table class="student_info" cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td width="65%" align="left">'.$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['StudentName'][$student_lang].' : '.$student_name.'</td>
								<td width="10%" align="left">'.$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['ClassNameNumber'][$student_lang].' : </td>
								<td width="5%">&nbsp;</td>
								<td width="10%">'.$class_name.'</td>
								<td width="10%">'.$class_number.'</td>
							</tr>
						</table>
						<br>
						<br>';
			
			if($format == 'csv'){
				$header = array($Lang['eDiscipline']['IndividualDisciplineRecord_cust']['StudentName'][$student_lang].' : ', $student_name, '', $Lang['eDiscipline']['IndividualDisciplineRecord_cust']['ClassNameNumber'][$student_lang].' : ', $class_name.$class_number);
				$rows = array();
			}
			
			# Awards/ Punishment
			$APCaseSql = "CASE (r.ProfileMeritType)
											WHEN 0 THEN '".$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Warning'][$student_lang]."'
											WHEN 1 THEN '".$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Merit'][$student_lang]."'
											WHEN 2 THEN '".$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['MinorCredit'][$student_lang]."'
											WHEN 3 THEN '".$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['MajorCredit'][$student_lang]."'
											WHEN 4 THEN '".$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['SuperCredit'][$student_lang]."'
											WHEN 5 THEN '".$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['UltraCredit'][$student_lang]."'
											WHEN -1 THEN '".$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['BlackMark'][$student_lang]."'
											WHEN -2 THEN '".$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['MinorDemerit'][$student_lang]."'
											WHEN -3 THEN '".$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['MajorDemerit'][$student_lang]."'
											WHEN -4 THEN '".$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['SuperDemerit'][$student_lang]."'
											WHEN -5 THEN '".$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['UltraDemerit'][$student_lang]."'
											ELSE '--' END";
			if($student_lang=="en"){
				$meritDetailsql = "CONCAT(ROUND(r.ProfileMeritCount),' ', ".$APCaseSql.", '(s)')";
			} else {
				$meritDetailsql = "CONCAT(".$APCaseSql.", cast(ROUND(r.ProfileMeritCount) as char), '". $Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Times']['b5'] ."', ' ')";
			}
			
			# Get AP Record
			$sql = "SELECT 
						r.RecordID, 
						DATE_FORMAT(r.RecordDate,'%d/%m/%Y') as RecordDate,
						r.ItemText,
						m.ItemName,
						m.ItemCode,
						m.NumOfMerit,
						".$meritDetailsql."	AS RelatedMeritType,
						r.ProfileMeritType,
						r.Remark 
					FROM DISCIPLINE_MERIT_RECORD as r
					LEFT JOIN DISCIPLINE_MERIT_ITEM as m ON m.ItemID=r.ItemID 
					WHERE r.StudentID='$student_id' AND r.RecordDate>='$SQL_startdate' AND r.RecordDate<='$SQL_enddate' AND r.RecordStatus='".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus='".DISCIPLINE_STATUS_RELEASED."'
					ORDER BY r.RecordDate";
			$ap_records = $ldiscipline->returnResultSet($sql);
			$ap_record_count = count($ap_records);
			
			# Table Header
			$x .= '		<table class="record_table" cellspacing="0" cellpadding="5" border="1" width="100%">
							<tr class="record_table_header">
								<td width="25%" align="center">'.$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Date'][$student_lang].'</td>
								<td width="40%" align="center">'.$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['RecordDetails'][$student_lang].'</td>
								<td width="35%" align="center">'.$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['APRecord'][$student_lang].'</td>
							</tr>';
				
			if($format == 'csv'){
				$row = array('');
				$rows[] = $row;
				$row = array($Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Date'][$student_lang], $Lang['eDiscipline']['IndividualDisciplineRecord_cust']['RecordDetails'][$student_lang], $Lang['eDiscipline']['IndividualDisciplineRecord_cust']['APRecord'][$student_lang]);
				$rows[] = $row;
			}
				
			# Each AP Records
			for($k=0; $k<$ap_record_count; $k++){	
				$currentRow -= 1.5;
				
				# Record Retails
				$ap_item_date 	= $ap_records[$k]['RecordDate'];
				$ap_item_name 	= trim($ap_records[$k]['ItemName']);
				$ap_item_type 	= $ap_records[$k]['RelatedMeritType'];
				if(strpos($ap_item_type, '--')!==false){
					$ap_item_type = "--";
				}
				
				# Get location of first chinese / english character
				$eng_locat = -1;
				$chi_locat = -1;
				$last_chi_locat = -1;
				for($j = 0; $j < strlen($ap_item_name); $j++) {
	        		$value = ord($ap_item_name[$j]);
	        		
	        		// Chinese character
	        		if($value > 223) {
	        			if($chi_locat === -1)
	        				$chi_locat = $j;
	        			
	        			// Skip checking (for English character first)
	        			if($chi_locat !== 0)
	        				break;
	        			// Update last chinese location (for Chinese character first)
	        			else
	        				$last_chi_locat = $j;
	        		}
	        		// English character (for Chinese character first)
	        		else if(($value > 64 && $value < 91 || $value > 96 && $value < 123) && $chi_locat === 0 && $last_chi_locat > $eng_locat){
	        			$eng_locat = $j;
	        		}
				}
				
				# Display record details in chinese or english
				// Normal Case
				if($student_lang == 'en' && $chi_locat > 0){
					$ap_item_name = substr($ap_item_name, 0, $chi_locat);
				}
				else if ($student_lang == 'b5' && $chi_locat > 0){
					$ap_item_name = substr($ap_item_name, $chi_locat);
				}
				// Case: Chinese character first
				else if ($student_lang == 'en' && $chi_locat === 0 && $eng_locat > 0 && $last_chi_locat > 0 && $eng_locat > $last_chi_locat){
					$ap_item_name = substr($ap_item_name, $eng_locat);
				}
				else if ($student_lang == 'b5' && $chi_locat === 0 && $eng_locat > 0 && $last_chi_locat > 0 && $eng_locat > $last_chi_locat){
					$ap_item_name = substr($ap_item_name, 0, $eng_locat);
				}
				$ap_item_name = trim($ap_item_name);
				
				// Extra row for too long item name
				if($student_lang == 'b5' && countString($ap_item_name) > 16){
					$currentRow -= 1.5;
				} else if($student_lang == 'en' && countString($ap_item_name) > 40){
					$currentRow -= 1.5;
				}
				
				# Row style	
				$borderstyle = 'style="border-bottom:0px; border-top:0px;"';
				$borderstyle = $k===0? 'style="border-bottom:0px;"' : $borderstyle;
				$borderstyle = ($k===($ap_record_count-1) && $currentRow < 2)? 'style="border-top:0px;"' : $borderstyle;
					
				$x .= '		<tr>
								<td width="25%" align="right" '.$borderstyle.'>'.$ap_item_date.'</td>
								<td width="40%" align="left" '.$borderstyle.'>'.$ap_item_name.'</td>
								<td width="35%" align="center" '.$borderstyle.'>'.$ap_item_type.'</td>
							</tr>';
					
				if($format == 'csv'){
					$row = array($ap_item_date, $ap_item_name, $ap_item_type);
					$rows[] = $row;
				}
				
				# Line Break if too much records
				if ($currentRow < 3 && $k < ($ap_record_count - 1)){
					$x .= '	
						</table>
						<br /><br />
					</div>
					
					<div style="page-break-after:always">&nbsp;</div>
					
					<div class="main_container">
						<div class="second_page_header">&nbsp;</div>
						<table class="record_table" cellspacing="0" cellpadding="5" border="1" width="100%">
							<tr class="record_table_header">
								<td width="25%" align="center">'.$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Date'][$student_lang].'</td>
								<td width="40%" align="center">'.$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['RecordDetails'][$student_lang].'</td>
								<td width="35%" align="center">'.$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['APRecord'][$student_lang].'</td>
							</tr>';
					
					# Set new row count		
					$currentRow = $maxRow - $pageTwoHeader;
				}
			}
			
			# Empty row if table is not full
			if ($currentRow >= 3){
				for(; $currentRow >= 3; ){
					$currentRow -= 1.5;
					
					$borderstyle = 'style="border-bottom:0px; border-top:0px;"';
					$borderstyle = ($currentRow == ($maxRow - $pageOneHeader - 1.5))? 'style="border-bottom:0px;"' : $borderstyle;
					$borderstyle = ($currentRow < 3)? 'style="border-top:0px;"' : $borderstyle;
					
					# Empty Row
					$x .= '<tr>
							<td width="25%" align="right" '.$borderstyle.'>&nbsp;</td>
							<td width="40%" align="left" '.$borderstyle.'>&nbsp;</td>
							<td width="35%" align="center" '.$borderstyle.'>&nbsp;</td>
						</tr>';
				}
			}
					
			$x .= '		</table>
						<br /><br />';
			
			# Line Break for next student
			if($format == 'print'){
				$x .= '</div>';
				if($i < ($student_count - 1)){
					$x .= '	<div style="page-break-after:always"></div>';
				}
			}
			
			if($format == 'csv'){
				$rows[] = array('','','');
				if($i > 0){
					$exportContent .= "\r\n";
				}
				$exportContent .= $lexport->GET_EXPORT_TXT($rows, $header);
			}
		}
		
		if($format == 'csv'){
			$lexport->EXPORT_FILE("Individual_Discipline_Record.csv", $exportContent);
		}else{
			$x .= '</div>';
			return $x;
		}
	}
	# [Buddhist Fat Ho Memorial College] - End
	
	# [Ho Yu College And Primary School] - $sys_custom['eDiscipline']['HoYuPrimarySchCust']
	function getHoYuDemeritRecordReport($options)
	{
		global $PATH_WRT_ROOT, $intranet_root, $intranet_session_language, $Lang;
		global $ldiscipline;
		
		# POST data
		$format = $options['format'];
		$radioPeriod = $options['radioPeriod'];
		$selectYear = $options['selectYear']; 
		$textFromDate = $options['textFromDate'];
		$textToDate = $options['textToDate'];
		
		# Report Settings
		$maxRow = 48.5;
		$pageOneHeader = 7.5;
		$pageTwoHeader = 1.5;
		
		# Year & Date
		if ($radioPeriod == "YEAR") 
		{
			$SQL_startdate = getStartDateOfAcademicYear($selectYear, '');
			$SQL_enddate = getEndDateOfAcademicYear($selectYear, '');
			$textFromDate = date("Y-m-d", strtotime($SQL_startdate));
			$textToDate = date("Y-m-d", strtotime($SQL_enddate));
		}
		else
		{
			$SQL_startdate = $textFromDate;
			$SQL_enddate = $textToDate;
			$selectYear = Get_Current_Academic_Year_ID();
		}
		
		# Rank Target
		$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
		
		# Style
		$EmptySymbol = $Lang['General']['EmptySymbol'];
		$StylePrefix = '<span class="tabletextrequire">';
		$StyleSuffix = '</span>';
		if($format == 'csv'){
			$StylePrefix = '';
			$StyleSuffix = '';
		}
		
		# Get Target Students
		$clsName = "yc.ClassTitleEN";	
		$sql = "SELECT 
					iu.UserID,
					yc.ClassTitleEN as ClassName,
					ycu.ClassNumber,
					CONCAT(
						IF($clsName IS NULL OR $clsName='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
						iu.EnglishName
					) as StudentName
				FROM INTRANET_USER as iu 
				LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID
				LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear')
				LEFT JOIN YEAR as y ON y.YearID = yc.YearID
				WHERE yc.AcademicYearID = '".$selectYear."' $classIdCond 
				GROUP BY iu.UserID 
				ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";	
				
		$students = $ldiscipline->returnResultSet($sql);
		$student_count = count($students);
		
		$x = '';
		if($format == 'web'){
			$x .= '<div class="content_top_tool">
					<div class="Conntent_tool">
						<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
						<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
					</div>
					<br style="clear:both" />
				  </div><br />';
		}
		if($format == 'print'){
			$x .= '<table width="100%" align="center" class="print_hide" border="0">
					<tr>
						<td align="right">'.$this->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print").'</td>
					</tr>
				</table>';
		}
		if($format == 'csv'){
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			$lexport = new libexporttext();
			$exportContent = '';
		}
	
		# Table Header
		$x .= '<div><div class="main_container">
					<table cellspacing="0" cellpadding="5" border="1" width="100%">
						<tr>
							<td width="3%" align="center"><b><u>CLASS</u></b></td>
							<td width="3%" align="center"><b><u>NO</u></b></td>
							<td width="18%" align="center"><b><u>NAME</u></b></td>
							<td width="3%" align="center"><b><u>年</u></b></td>		
							<td width="2%" align="center"><b><u>月</u></b></td>	
							<td width="2%" align="center"><b><u>日</u></b></td>	
							<td width="8%" align="center"><b><u>CODE</u></b></td>	
							<td width="18%" align="left"><b><u>DESCRIPTION</u></b></td>	
							<td width="3%" align="center"><b><u>警告</u></b></td>		
							<td width="3%" align="center"><b><u>缺點</u></b></td>	
							<td width="3%" align="center"><b><u>小過</u></b></td>	
							<td width="3%" align="center"><b><u>大過</u></b></td>	
							<td width="3%" align="center"><b><u>優點</u></b></td>	
							<td width="3%" align="center"><b><u>小功</u></b></td>		
							<td width="3%" align="center"><b><u>大功</u></b></td>	
							<td width="16%" align="center"><b><u>備註</u></b></td>
						</tr>';	
		if($format == 'csv'){
			$header = array('CLASS', 'NO', 'NAME', '年', '月', '日', 'CODE', 'DESCRIPTION', '警告', '缺點', '小過', '大過', '優點', '小功', '大功', '備註');
			$rows = array();
		}
	
		# Each Students
		for($i=0; $i<$student_count; $i++)
		{
			$currentRow = $maxRow - $pageOneHeader;
				
			# Student Info
			$student_id = $students[$i]['UserID'];
			$student_name = $students[$i]['StudentName'];
			$class_name = $students[$i]['ClassName'];
			$class_number = $students[$i]['ClassNumber'];
			
			# Awards/ Punishment
			$sql = "SELECT 
						r.RecordID, 
						DATE_FORMAT(r.RecordDate,'%d/%m/%Y') as RecordDate,
						m.ItemCode,
						m.ItemName,
						r.ItemText,
						r.ProfileMeritType,
						ROUND(r.ProfileMeritCount) AS RelatedMeritType,
						r.Remark 
					FROM DISCIPLINE_MERIT_RECORD as r
					LEFT JOIN DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID 
					WHERE r.StudentID = '$student_id' AND r.RecordDate >= '$SQL_startdate' AND r.RecordDate <= '$SQL_enddate' AND r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."'
					ORDER BY r.RecordDate";
					
			$ap_records = $ldiscipline->returnResultSet($sql);
			$ap_record_count = count($ap_records);
				
			# Each AP Records
			for($k=0; $k<$ap_record_count; $k++){
				$currentRow -= 1.5;

				# Record Retails
				$ap_item_date 	= $ap_records[$k]['RecordDate'];
				$ap_item_code	= $ap_records[$k]['ItemCode'];
				$ap_item_name 	= $ap_records[$k]['ItemName'];
				$ap_merit_type	= $ap_records[$k]['ProfileMeritType'];
				$ap_item_type 	= $ap_records[$k]['RelatedMeritType'];
				$ap_remarks		= ($ap_records[$k]['Remark'] || $format != 'csv')? $ap_records[$k]['Remark'] : '&nbsp;';
				
				list($ap_item_record_date, $ap_item_month, $ap_item_year) = explode('/', $ap_item_date);
				
				if(strpos($ap_item_type, '--') !== false){
					$ap_item_type = '0';
				}
				
				# Merit type count array
				$ap_records_array = array();
				$ap_records_array[0] = '0';
				$ap_records_array[1] = '0';
				$ap_records_array[2] = '0';
				$ap_records_array[3] = '0';
				$ap_records_array[-2] = '0';
				$ap_records_array[-3] = '0';
				$ap_records_array[-4] = '0';
				
				$ap_merit_type = ($ap_merit_type == '-1')? '0' : $ap_merit_type;
				
				$ap_records_array[$ap_merit_type] = $ap_item_type;
				
//				# Get location of first chinese character
//				$chi_locat = -1;
//				for($j = 0; $j < strlen($ap_item_name); $j++) {
//	        		$value = ord($ap_item_name[$j]); 
//	        		if($value > 223) {
//	        			$chi_locat = $j;
//	        			break;
//	        		}
//				}
//				
//				# Display record details in chinese or english
//				if($student_lang == 'en' && $chi_locat > 0){
//					$ap_item_name = substr($ap_item_name, 0, $chi_locat);
//				} elseif ($student_lang == 'b5' && $chi_locat > 0){
//					$ap_item_name = substr($ap_item_name, $chi_locat);
//				}
//
//				if($student_lang == 'b5' && countString($ap_item_name) > 16){
//					$currentRow -= 1.5;
//				} else if($student_lang == 'en' && countString($ap_item_name) > 40){
//					$currentRow -= 1.5;
//				}
				
				# Row style	
//				$borderstyle = 'style="border-bottom:0px; border-top:0px;"';
//				$borderstyle = $k===0? 'style="border-bottom:0px;"' : $borderstyle;
//				$borderstyle = ($k===($ap_record_count-1) && $currentRow < 2)? 'style="border-top:0px;"' : $borderstyle;

				$x .= '<tr>
							<td align="center" '.$borderstyle.'>'.$class_name.'</td>
							<td align="center" '.$borderstyle.'>'.$class_number.'</td>
							<td align="center" '.$borderstyle.'>'.$student_name.'</td>
							<td align="center" '.$borderstyle.'>'.$ap_item_year.'</td>
							<td align="center" '.$borderstyle.'>'.$ap_item_month.'</td>
							<td align="center" '.$borderstyle.'>'.$ap_item_record_date.'</td>
							<td align="center" '.$borderstyle.'>'.$ap_item_code.'</td>
							<td align="left" '.$borderstyle.'>'.$ap_item_name.'</td>
							<td align="center" '.$borderstyle.'>'.$ap_records_array[0].'</td>
							<td align="center" '.$borderstyle.'>'.$ap_records_array[-2].'</td>
							<td align="center" '.$borderstyle.'>'.$ap_records_array[-3].'</td>
							<td align="center" '.$borderstyle.'>'.$ap_records_array[-4].'</td>
							<td align="center" '.$borderstyle.'>'.$ap_records_array[1].'</td>
							<td align="center" '.$borderstyle.'>'.$ap_records_array[2].'</td>
							<td align="center" '.$borderstyle.'>'.$ap_records_array[3].'</td>
							<td align="center" '.$borderstyle.'>'.$ap_remarks.'</td>
						</tr>';
					
				if($format == 'csv'){
					$row = array($class_name, $class_number, $student_name, $ap_item_year, $ap_item_month, $ap_item_record_date, $ap_item_code, $ap_item_name, $ap_records_array[0], $ap_records_array[-2], $ap_records_array[-3], $ap_records_array[-4], $ap_records_array[1], $ap_records_array[2], $ap_records_array[3], $ap_remarks);
					$rows[] = $row;
				}
			}
//				# Line Break if too much records
//				if ($currentRow < 3 && $k < ($ap_record_count - 1)){
//					$x .= '	
//						</table>
//						<br /><br />
//					</div>
//					
//					<div style="page-break-after:always">&nbsp;</div>
//					
//					<div class="main_container">
//						<div class="second_page_header">&nbsp;</div>
//						<table class="record_table" cellspacing="0" cellpadding="5" border="1" width="100%">
//							<table class="record_table" cellspacing="0" cellpadding="5" border="1" width="100%">
//							<tr class="record_table_header">
//							
//							<th>CLASS</th>
//							<th>NO</th>
//							<th>NAME</th>
//							<th>年</th>		
//							<th>月</th>	
//							<th>日</th>	
//							<th>CODE</th>	
//							<th>DESCRIPTION</th>	
//							<th>警告</th>		
//							<th>缺點</th>	
//							<th>小過</th>	
//							<th>大過</th>	
//							<th>優點</th>	
//							<th>小功</th>		
//							<th>大功</th>	
//							<th>備註</th>
//							</tr>';
//					
//					# Set new row count		
//					$currentRow = $maxRow - $pageTwoHeader;
//				}
//			}
//			
//			# Empty row if table is not full
//			if ($currentRow >= 3){
//				for(; $currentRow >= 3; ){
//					$currentRow -= 1.5;
//					
//					$borderstyle = 'style="border-bottom:0px; border-top:0px;"';
//					$borderstyle = ($currentRow == ($maxRow - $pageOneHeader - 1.5))? 'style="border-bottom:0px;"' : $borderstyle;
//					$borderstyle = ($currentRow < 3)? 'style="border-top:0px;"' : $borderstyle;
//					
//					# Empty Row
//					$x .= '<tr>
//							<td width="25%" align="right" '.$borderstyle.'>&nbsp;</td>
//							<td width="40%" align="left" '.$borderstyle.'>&nbsp;</td>
//							<td width="35%" align="center" '.$borderstyle.'>&nbsp;</td>
//						</tr>';
//				}
//			}
//					
//			$x .= '		</table>
//						<br /><br />';
//			
//			# Line Break for next student
//			if($format == 'print'){
//				$x .= '</div>';
//				if($i < ($student_count - 1)){
//					$x .= '	<div style="page-break-after:always"></div>';
//				}
//			}
//			
//			if($format == 'csv'){
//				$rows[] = array('','','');
//				if($i > 0){
//					$exportContent .= "\r\n";
//				}
//				$exportContent .= $lexport->GET_EXPORT_TXT($rows, $header);
//			}
		}
//		debug_pr($x);
		if($format == 'csv'){
			$exportContent .= $lexport->GET_EXPORT_TXT($rows, $header);
			$lexport->EXPORT_FILE("Individual_Discipline_Record.csv", $exportContent);
		} else {
			$x .= '</table></div></div>';
			return $x;
		}
	}
	
	function getHoYuAnnualDisciplineReport($options)
	{
		global $PATH_WRT_ROOT, $intranet_root, $intranet_session_language, $Lang;
		global $ldiscipline;
		
		# POST
		$format = $options['format'];
		$selectYear = $options['selectYear'];
		$selectSemester = $options['selectSemester']? $options['selectSemester'] : "";
		
		# Report Settings
		$maxRow = 48.5;
		$pageOneHeader = 12.5;
		$pageTwoHeader = 4;
		
		# Year
		//$SQL_startdate = getStartDateOfAcademicYear($selectYear, '');
		//$SQL_enddate = getEndDateOfAcademicYear($selectYear, '');
		$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester);
		$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester);
		$textFromDate = date("Y-m-d", strtotime($SQL_startdate));
		$textToDate = date("Y-m-d", strtotime($SQL_enddate));
		
		# Semester
		$termName = "全年";
		if($selectSemester != "") {
			include_once("form_class_manage.php");
			$objTerm = new academic_year_term($selectSemester, false);
			$termName = $objTerm->YearTermNameB5;
		}
		
		# SQL cond
		$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
		$studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$options['studentID'])."')";
		
		$EmptySymbol = $Lang['General']['EmptySymbol'];
		$StylePrefix = '<span class="tabletextrequire">';
		$StyleSuffix = '</span>';
		if($format == 'csv'){
			$StylePrefix = '';
			$StyleSuffix = '';
		}
		
		# Get all Target Students
		$clsName = 'yc.ClassTitleEN';
		$sql = "SELECT 
					iu.UserID,
					iu.ChineseName as StudentName,
					yc.ClassTitleB5 as ClassName,
					ycu.ClassNumber,
					yc.YearClassID,
					y.WEBSAMSCode as ClassLevelCode
				FROM 
				    INTRANET_USER as iu 
                    LEFT JOIN YEAR_CLASS_USER as ycu ON (ycu.UserID = iu.UserID) 
                    LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear')
                    LEFT JOIN YEAR as y ON (y.YearID = yc.YearID) 
				WHERE 
				    yc.AcademicYearID = '".$selectYear."' 
                    $classIdCond $studentIdCond 
				GROUP BY iu.UserID 
				ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";
		$students = $ldiscipline->returnResultSet($sql);
		$student_count = count($students);
		
		# Get Class Teacher of Target Students
		$sql = "SELECT 
					yct.UserID, 
					CASE 
						WHEN au.UserID IS NOT NULL then au.ChineseName
						ELSE u.ChineseName
					END as TeacherName,
					YearClassID
				FROM 
				    YEAR_CLASS_TEACHER as yct 
                    LEFT JOIN INTRANET_USER as u ON (yct.UserID = u.UserID)
                    LEFT JOIN INTRANET_ARCHIVE_USER as au ON (yct.UserID = au.UserID) 
				WHERE 
				    YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."')";
		$class_teachers = $ldiscipline->returnArray($sql);
		$class_teachers = BuildMultiKeyAssoc($class_teachers, array('YearClassID', 'TeacherName'));

		# Get Principal
		$sql = "SELECT ChineseName FROM INTRANET_USER WHERE TitleChinese = '校長' AND RecordStatus = 1";
		$principal = $ldiscipline->returnArray($sql);
        //$principal = $principal[0]['ChineseName'];
        $secPrincipal = $principal[0]['ChineseName'];
		
		$x = '';
		if($format == 'web')
		{
			$x .= '<div class="content_top_tool">
					<div class="Conntent_tool">
						<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
					</div>
					<br style="clear:both" />
				  </div><br />';
		}
		if($format == 'print')
		{
			$x .= '<table width="100%" align="center" class="print_hide" border="0">
					<tr>
						<td align="right">'.$this->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print").'</td>
					</tr>
				</table>';
		}
//		if($format == 'csv'){
//			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
//			$lexport = new libexporttext();			
//			$exportContent = '';
//		}
	
		# Each Students
		for($i=0; $i<$student_count; $i++)
		{
			$currentPage = 1;
			$currentRow = $maxRow - $pageOneHeader;
				
			# Student Info
			$student_id = $students[$i]['UserID'];
			$student_name = $students[$i]['StudentName'];
			$class_name = $students[$i]['ClassName'];
			$class_number = $students[$i]['ClassNumber'];
			$yearClass_Id = $students[$i]['YearClassID'];
            $formLevel_Code = $students[$i]['ClassLevelCode'];
            $school_type  = substr($formLevel_Code, 0, 1);

            # Principal
            // [2020-0707-1405-58098] 1. 更改小學部份的校長署名
            if($school_type == 'P') {
                $principal = '梁惠芳';
            } else {
                $principal = $secPrincipal;
            }
			
			# Class Teacher
			if(is_array($class_teachers) && count($class_teachers) > 0){
				$teachers = array_keys((array)$class_teachers[$yearClass_Id]);

				// [2020-0707-1405-58098] 6. - 刪除兩位班主任姓名之間的「，」
                //$teachers = implode(', ', $teachers);
                $teachers = implode('　', $teachers);
			} else {
				$teachers = $EmptySymbol;
			}
			
			# Header and Info
			$x .= '<div><div class="main_container">
						<table class="report_sch_header" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
							<tr>
								<td class="sch_logo_td"><img class="hy_sch_logo" src="hoyu_sch_logo.png"></td>
								<td><table cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
									<tr>
										<td><h2 class="chi_sch_name">嗇色園主辦可譽中學暨可譽小學</h2></td>
									</tr>
									<tr>
										<td><h2>HO YU COLLEGE AND PRIMARY SCHOOL</h2></td>
									</tr>
									<tr>
										<td><h2>(SPONSORED BY SIK SIK YUEN)</h2></td>
									</tr>
								</table></td>
								<td class="sch_logo_td"><img class="hy_sch_logo" src="ssy_logo.png"></td>
							</tr>
						</table>
						<br>

						<table class="report_title_header" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
							<tr>
								<td><h2>'.getAcademicYearByAcademicYearID($year=$selectYear,$ParLang='b5').'年度</h2></td>
							</tr>
							<tr>
								<td><h2>'.$termName.'品行表現報告</h2></td>
							</tr>
						</table>
						<br>
						<br>

						<table class="student_info" cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td width="45%">姓名	：	'.$student_name.'</td>
								<td width="35%">班別	：	'.$class_name.'</td>
								<td width="20%">學號	：	'.$class_number.'</td>
							</tr>
						</table>
						<br>
						<br>
						<br>';
//			if($format == 'csv'){
//				$header = array($Lang['eDiscipline']['IndividualDisciplineRecord_cust']['StudentName'][$student_lang].' : ', $student_name, '', $Lang['eDiscipline']['IndividualDisciplineRecord_cust']['ClassNameNumber'][$student_lang].' : ', $class_name.$class_number);
//				$rows = array();
//			}
			
			# Awards / Punishment
			// commented as no need to return whole wording after query
//			$APCaseSql = "CASE (r.ProfileMeritType)
//											WHEN 0 THEN '警告'
//											WHEN 1 THEN '優點'
//											WHEN 2 THEN '小功'
//											WHEN 3 THEN '大功'
//											WHEN -1 THEN '警告'
//											WHEN -2 THEN '缺點'
//											WHEN -3 THEN '小過'
//											WHEN -4 THEN '大過'
//											ELSE '--' END";
//			$meritDetailsql = "CONCAT(".$APCaseSql.", CAST(ROUND(r.ProfileMeritCount) as char), '個')";	
			// get merit count only
			$meritDetailsql = "CAST(ROUND(r.ProfileMeritCount) as char)";
/*
			# Get AP Record
			// 2015-01-23 - Added ItemID for grouping
			$sql = "SELECT 
						r.RecordID,
						m.ItemID,
						m.ItemName,
						IF(r.ProfileMeritType > 0, '1', '0') as MeritType,
						r.ProfileMeritType,
						".$meritDetailsql."	AS RelatedMeritType,
						IF(r.RedeemID IS NOT NULL, '1', '0') as Redeem
					FROM DISCIPLINE_MERIT_RECORD as r
					LEFT JOIN DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID 
					WHERE r.StudentID = '$student_id' AND r.RecordDate >= '$SQL_startdate' AND r.RecordDate <= '$SQL_enddate' AND ((r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."') OR r.RedeemID IS NOT NULL)
					ORDER BY r.RecordDate";
					
			$ap_records = $ldiscipline->returnResultSet($sql);
			
			// Build 3D array for grouping records
			$ap_records = BuildMultiKeyAssoc($ap_records, array('MeritType', 'RecordID'));
			$ap_records = BuildMultiKeyAssoc($ap_records, array('MeritType', 'ItemName', 'RecordID'));

			$p_records = $ap_records[0];
			$a_records = $ap_records[1];
*/
			// 2015-02-09: seperate to 2 sql for AP records
			# Get Award Record
			$sql = "SELECT 
						r.RecordID,
						m.ItemID,
						m.ItemName,
						m.ItemCode,
						r.ProfileMeritType,
						".$meritDetailsql."	AS RelatedMeritType
					FROM 
					    DISCIPLINE_MERIT_RECORD as r
					    LEFT JOIN DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID 
					WHERE 
					    r.StudentID = '$student_id' AND r.MeritType = 1 AND r.RecordDate >= '$SQL_startdate' AND r.RecordDate <= '$SQL_enddate' AND
						r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."' AND r.RedeemID IS NULL
					ORDER BY 
                        r.ProfileMeritType desc, m.ItemCode";
					
			$a_records = $ldiscipline->returnResultSet($sql);
			$a_records = BuildMultiKeyAssoc($a_records, array('ItemName', 'RecordID'));
			
			# Get Punishment Record
//			$sql = "SELECT 
//						r.RecordID,
//						m.ItemID,
//						m.ItemName,
//						IF(r.ProfileMeritType = -999, 999, r.ProfileMeritType) as ProfileMeritType,
//						".$meritDetailsql."	AS RelatedMeritType
//					FROM DISCIPLINE_MERIT_RECORD as r
//					LEFT JOIN DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID 
//					WHERE r.StudentID = '$student_id' AND r.MeritType = -1 AND r.RecordDate >= '$SQL_startdate' AND r.RecordDate <= '$SQL_enddate' 
//						AND ((r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."') OR r.RedeemID IS NOT NULL)
//					ORDER BY ProfileMeritType";
			$sql = "SELECT 
						r.RecordID,
						m.ItemID,
						m.ItemName,
						m.ItemCode,
						IF(r.ProfileMeritType = -999, 999, IF(r.ProfileMeritType = -1, 0, r.ProfileMeritType)) as ProfileMeritType,
						".$meritDetailsql."	AS RelatedMeritType
					FROM 
					    DISCIPLINE_MERIT_RECORD as r
                        LEFT JOIN DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID 
					WHERE 
					    r.StudentID = '$student_id' AND r.MeritType = -1 AND r.RecordDate >= '$SQL_startdate' AND r.RecordDate <= '$SQL_enddate' 
						AND r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."' AND r.RedeemID IS NULL
						AND r.ProfileMeritType != 0 AND r.ProfileMeritType != -1
					ORDER BY ProfileMeritType, m.ItemCode";
			$p_records = $ldiscipline->returnResultSet($sql);
			$p_records = BuildMultiKeyAssoc($p_records, array('ItemName', 'RecordID'));
			
			$p_record_count = count($p_records);
			$a_record_count = count($a_records);
			
			# AP wordings and MeritType linking
			$ap_name_ary = array();
			$ap_name_ary[0] = '警告';
			$ap_name_ary[1] = '優點';
			$ap_name_ary[2] = '小功';
			$ap_name_ary[3] = '大功';
			$ap_name_ary[-1] = '警告';
			$ap_name_ary[-2] = '缺點';
			$ap_name_ary[-3] = '小過';
			$ap_name_ary[-4] = '大過';
			
			# Award Records
			$x .= '<span class="record_title"> 功： </span>';
			$currentRow--;
			
			$award_Table = '';
			$valid_award = false;
			if($a_record_count > 0)
			{
				$award_Table .= '<table class="record_table" cellspacing="0" cellpadding="5" border="1">';

                $award_count = 0;
				foreach($a_records as $current_a_record)
				{
					$award_count++;
					
					if(count($current_a_record) == 0){
						continue;
					}
					
					# Group award records
					$a_item_name = '';
					$a_grouping = array();
					foreach($current_a_record as $grouping_a_record)
					{
						if(!$a_item_name){
							$a_item_name = $grouping_a_record['ItemName'];
						}
							
						# Record Retails
						$a_item_type 	= $grouping_a_record['ProfileMeritType'];
						$a_item_num		= $grouping_a_record['RelatedMeritType'];
//						$a_item_redeem 	= $grouping_a_record['Redeem'];
						
						// 2015-02-09: remove redeem checking
						// Award record not redeem
//						if($a_item_redeem != '1'){
						if(!isset($a_grouping[$a_item_type])){
							$a_grouping[$a_item_type] = $a_item_num;
						} else {
							$a_grouping[$a_item_type] += $a_item_num;
						}
//						}
					}
					
					if(count($a_grouping) == 0){
						continue;
					}
					
//					if($a_item_redeem == '1')	continue;
					$valid_award = true;
					
					# Award details
					$a_item_content = '';
					$delim = '';
					foreach($a_grouping as $a_type => $a_counts){
						if(!$ap_name_ary[$a_type]){
							continue;
						}
						$a_item_content .= $delim.$ap_name_ary[$a_type].$a_counts.'個';
						$delim = '；';
					}
					
					if($a_item_content == ''){
						$a_item_content = $EmptySymbol;
					}
					
					$award_Table .= '<tr>
										<td width="50%" align="center">'.$a_item_name.'</td>
										<td class="record_table_content" width="50%" >'.$a_item_content.'</td>
									</tr>';
					
					$currentRow -= 1.5;
				
					# Get location of first chinese character
					$chi_locat = -1;
					for($j = 0; $j < strlen($a_item_name); $j++) {
		        		$value = ord($a_item_name[$j]);
		        		if($value > 223) {
		        			$chi_locat = $j;
		        			break;
		        		}
					}
					
					if($chi_locat >= 0 && countString($a_item_name) > 10){
						$currentRow -= 1.5;
					} else if($chi_locat == -1 && countString($a_item_name) > 26){
						$currentRow -= 1.5;
					}

					# Line Break if too much records
					if ($format == 'print' && $currentRow < 7.5 && $award_count < $a_record_count)
					{
						$x .= $award_Table;
						$x .= '</table>
							<br />
							<br />
						
							<!--<div class="page_no"> Page '.$currentPage.'/[TOTAL_PAGE_NO] </div>-->
							<div class="page_no"> &nbsp; </div>
						</div>

						<div style="page-break-after:always">&nbsp;</div>

						<div class="main_container"><div class="second_page_header">&nbsp;</div>';
						
						$award_Table = ' 
							<span class="record_title"> 功： </span>
							<table class="record_table" cellspacing="0" cellpadding="5" border="1">';
							
						$valid_award = false;
						$currentRow = $maxRow - $pageTwoHeader;
						$currentPage++;
					}
				}

				$award_Table .= '</table><br/><br/>';
			}
			
			# Add spacing between Award & Punishment Table if no award record
            // [2020-0707-1405-58098] 4. 如沒有任何功過記錄，則顯示 「--」
            //$x .= $valid_award? $award_Table : '<table class="record_table" cellspacing="0" cellpadding="5" border="0">&nbsp;</table><br/><br/>';
			$x .= $valid_award ? $award_Table : '<span class="record_title">--</span><table class="record_table" cellspacing="0" cellpadding="5" border="0">&nbsp;</table><br/><br/>';
			
			# Line Break if too much records
			if ($format == 'print' && $currentRow < 11 && $p_record_count > 0)
			{
				$x .= '
					<!--<div class="page_no"> Page '.$currentPage.'/[TOTAL_PAGE_NO] </div>-->
					<div class="page_no"> &nbsp; </div>
				</div>

				<div style="page-break-after:always">&nbsp;</div>';
						
				$x .= '<div class="main_container"><div class="second_page_header">&nbsp;</div>';
							
				$currentRow = $maxRow - $pageTwoHeader;
				$currentPage++;
			}
			else if ($p_record_count != 0)
			{
				$currentRow -= 3;
			}
		
			# Each Punishment Records
			$x .= '<span class="record_title"> 過： </span>';
			$punish_Table = '';
			if($p_record_count > 0)
			{
				$punish_Table .= '<table class="record_table" cellspacing="0" cellpadding="5" border="1">';

                $punish_count = 0;
				foreach($p_records as $current_p_record)
				{
					$punish_count++;
					
					if(count($current_p_record) == 0){
						continue;
					}
						
					$p_item_name = '';
					$p_grouping = array();
					foreach($current_p_record as $grouping_p_record)
					{
						if(!$p_item_name){
							$p_item_name = $grouping_p_record['ItemName'];
						}
							
						# Record Retails
						$p_item_type = $grouping_p_record['ProfileMeritType'];
						$p_item_num	= $grouping_p_record['RelatedMeritType'];
						if($p_item_type == -1){
							$p_item_type = 0;
						}

						if(!isset($p_grouping[$p_item_type])){
							$p_grouping[$p_item_type] = $p_item_num;
						} else {
							$p_grouping[$p_item_type] += $p_item_num;
						}
					}
					
					if(count($p_grouping) == 0){
						continue;
					}
					
					$p_item_content = '';
					$delim = '';
					foreach($p_grouping as $p_type => $p_counts)
					{
						if(!$ap_name_ary[$p_type]){
							continue;
						}
						$p_item_content .= $delim.$ap_name_ary[$p_type].$p_counts.'個';
						$delim = '；';
					}
					if($p_item_content == ''){
						$p_item_content = $EmptySymbol;
					} 
					
//					# Record Retails
//					$p_item_name 	= $current_p_record['ItemName'];
//					$p_item_contain = $current_p_record['RelatedMeritType'];
											
					$punish_Table .= '<tr>
											<td width="50%" align="center">'.$p_item_name.'</td>
											<td class="record_table_content" width="50%">'.$p_item_content.'</td>
										</tr>';
					
					$currentRow -= 1.5;
				
					# Get location of first chinese character
					$chi_locat = -1;
					for($j = 0; $j < strlen($p_item_name); $j++) {
		        		$value = ord($p_item_name[$j]);
		        		if($value > 223) {
		        			$chi_locat = $j;
		        			break;
		        		}
					}

					if($chi_locat >= 0 && countString($p_item_name) > 10){
						$currentRow -= 1.5;
					} else if($chi_locat == -1 && countString($p_item_name) > 26){
						$currentRow -= 1.5;
					}

					# Line Break if too much records
					if ($format == 'print' && $currentRow < 7.5 && $punish_count < $p_record_count)
					{
						$x .= $punish_Table;
						$x .= '</table>
							<br />
							<br />
						
							<!--<div class="page_no"> Page '.$currentPage.'/[TOTAL_PAGE_NO] </div>-->
							<div class="page_no"> &nbsp; </div>
						</div>

						<div style="page-break-after:always">&nbsp;</div>

						<div class="main_container"><div class="second_page_header">&nbsp;</div>';
						
						$punish_Table = ' 
							<span class="record_title"> 過： </span>
							<table class="record_table" cellspacing="0" cellpadding="5" border="1">';
							
						$currentRow = $maxRow - $pageTwoHeader;
						$currentPage++;
					}
				}
				$punish_Table .= '</table>';
			}

            // [2020-0707-1405-58098] 4. 如沒有任何功過記錄，則顯示 「--」
			if($punish_Table == '')
            {
			    $punish_Table .= '<span class="record_title">--</span>';
            }

			$x .= $punish_Table;
			
			# Line Break if too much records
			if ($format == 'print' && $currentRow < 6)
			{
				$x .= '
					<!--<div class="page_no"> Page '.$currentPage.'/[TOTAL_PAGE_NO] </div>-->
                    <div class="page_no"> &nbsp; </div>
				</div>

				<div style="page-break-after:always">&nbsp;</div>';
						
				$x .= '<div class="main_container"><div class="second_page_header">&nbsp;</div>';

				$currentPage++;
			}
			
//			# Empty row if table is not full
//			if ($currentRow >= 3){
//				for(; $currentRow >= 3; ){
//					$currentRow -= 1.5;
//					
//					$borderstyle = 'style="border-bottom:0px; border-top:0px;"';
//					$borderstyle = ($currentRow == ($maxRow - $pageOneHeader - 1.5))? 'style="border-bottom:0px;"' : $borderstyle;
//					$borderstyle = ($currentRow < 3)? 'style="border-top:0px;"' : $borderstyle;
//					
//					# Empty Row
//					$x .= '<tr>
//							<td width="25%" align="right" '.$borderstyle.'>&nbsp;</td>
//							<td width="40%" align="left" '.$borderstyle.'>&nbsp;</td>
//							<td width="35%" align="center" '.$borderstyle.'>&nbsp;</td>
//						</tr>';
//				}
//			}
//					
//			$x .= '		</table>
//						<br /><br />';
			
			# Line Break for next student
			if($format == 'print')
			{
				$x .= '<div class="page_no"> <!--Page '.$currentPage.'/'.$currentPage.'--> &nbsp;
	       					<span class="class_teacher_field"> 班主任<br> '.$teachers.'</span>
	       					<span class="principal_field"> 校長<br> '.$principal.'</span>
						</div>
	       			</div></div>';
				if($i < ($student_count - 1)){
					$x .= '	<div style="page-break-after:always">&nbsp;</div>';
				}
			}
			else if($format == 'web')
			{
				$x .= '</div></div>';
			}

			// [2020-0707-1405-58098] 3. 頁底 - 刪除 Page 1/1
			//$x = str_replace('[TOTAL_PAGE_NO]', $currentPage, $x);
			
//			if($format == 'csv'){
//				$rows[] = array('','','');
//				if($i > 0){
//					$exportContent .= "\r\n";
//				}
//				$exportContent .= $lexport->GET_EXPORT_TXT($rows, $header);
//			}
		}
	
//		if($format == 'csv'){
//			$lexport->EXPORT_FILE("Individual_Discipline_Record.csv", $exportContent);
//		}else{
		if($format == 'print'){
//			$x .= '</div>';
		}
		return $x;
//		}
	}
	# [Ho Yu College And Primary School] - End
		
	# [Escola Secundaria Pui Ching de Macau] - $sys_custom['eDiscipline']['MoPuiChingDailyReport']
	function getMoPuiChingDailyReport($options)
	{
		global $PATH_WRT_ROOT, $intranet_root, $intranet_session_language, $Lang;
		//global $i_Merit_Merit, $i_Merit_MinorCredit, $i_Merit_MajorCredit, $i_Merit_Warning , $i_Merit_BlackMark, $i_Merit_MinorDemerit, $i_Merit_MajorDemerit, $i_Merit_SuperDemerit;
		global $ldiscipline;
		
		$EmptySymbol = $Lang['General']['EmptySymbol'];
		$StylePrefix = '<span class="tabletextrequire">';
		$StyleSuffix = '</span>';
		
		# POST data
		$targetDate = $options['targetDate'];
		$targetDate = str_replace("-", ".", $targetDate);
		
		# Date
		$YearInfo = getAcademicYearAndYearTermByDate($targetDate);
		$YearID = $YearInfo['AcademicYearID'];
		
		# Rank Target condition
		$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
		
		# Get Class List
		$sql= "Select yc.YearClassID, yc.ClassTitleEN From YEAR_CLASS yc where 1 $classIdCond";
		$class_list = $ldiscipline->returnArray($sql);
		
		# Get Target Students
		$clsName = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
		$stuName = Get_Lang_Selection("iu.ChineseName","iu.EnglishName");
		if ($options["reportType"] == "student" && count($options["UserIDs"]) > 0) {
			
			$sql = "SELECT
			iu.UserID,
			yc.YearClassID,
			$clsName as ClassName,
			ycu.ClassNumber,
			CONCAT(
			IF($clsName IS NULL OR $clsName='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
			$stuName
			) as StudentName,
			iu.ChineseName,
			iu.EnglishName
			FROM INTRANET_USER as iu
			JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID AND ycu.UserID in ('" . implode("', '", $options["UserIDs"]) . "') 
			JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$YearID')
			JOIN YEAR as y ON y.YearID = yc.YearID
			WHERE yc.AcademicYearID = '".$YearID."' AND ycu.UserID in ('" . implode("', '", $options["UserIDs"]) . "') 
			GROUP BY iu.UserID
			ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";

		} else {
			$sql = "SELECT
						iu.UserID,
						yc.YearClassID,
						$clsName as ClassName,
						ycu.ClassNumber,
						CONCAT(
							IF($clsName IS NULL OR $clsName='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
							$stuName
						) as StudentName,
						iu.ChineseName,
						iu.EnglishName
					FROM INTRANET_USER as iu
					LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID
					LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$YearID')
					LEFT JOIN YEAR as y ON y.YearID = yc.YearID
					WHERE yc.AcademicYearID = '".$YearID."' $classIdCond
					GROUP BY iu.UserID
					ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";
		}
		$class_student = $ldiscipline->returnResultSet($sql);
		$student_ids = Get_Array_By_Key($class_student, "UserID");
		$class_student_ary = BuildMultiKeyAssoc($class_student, array("YearClassID", "UserID"));
		
		if ($options["reportType"] == "student" && count($options["UserIDs"]) > 0) {
			$year_class_ids = array_unique(Get_Array_By_Key($class_student, "YearClassID"));
			if (count($year_class_ids) > 0) {
				# Get Class List
				$sql= "Select yc.YearClassID, yc.ClassTitleEN From YEAR_CLASS yc where YearClassID in (" . implode(", ", $year_class_ids) . ")";
				$class_list = $ldiscipline->returnArray($sql);
				$class_list = BuildMultiKeyAssoc($class_list, "YearClassID");
			}
		}

		if ($options["reportType"] == "student" && count($options["UserIDs"]) > 0) {
			$where_date_conditoin = " AND r.RecordDate BETWEEN '" . $options["textFromDate"] . "' AND '" . $options["textToDate"] . "'";
		} else {
			$where_date_conditoin = " AND r.RecordDate = '" . $targetDate . "'";			
		}
		
		# Get Merit Records
		$sql = "SELECT
					r.RecordID,
					r.StudentID,
					r.ItemID,
					m.ItemCode,
					m.ItemName,
					r.ItemText,
					r.ProfileMeritType,
					ROUND(r.ProfileMeritCount) AS RelatedMeritType,
					IF(acci.Name IS NULL, m.ItemName, acci.Name) AS DisplayItemName,
					r.Remark as APRemark,
					ac.Remark as GMRemark,
					ac.CountNo,
					r.RecordDate
				FROM DISCIPLINE_MERIT_RECORD as r
				LEFT JOIN DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID 
				LEFT JOIN DISCIPLINE_ACCU_RECORD as ac ON ac.UpgradedRecordID = r.RecordID
				LEFT JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as acci ON acci.ItemID = ac.ItemID
				WHERE
					r.AcademicYearID = '".$YearID."' AND  
					r.StudentID IN ('".implode("','", (array)$student_ids)."') " . $where_date_conditoin ." AND 
					r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."'
				ORDER BY r.RecordDate";
		$ap_records = $ldiscipline->returnResultSet($sql);
	
		if ($options["reportType"] == "student" && count($options["UserIDs"]) > 0) {
			
			$byRecordDate = true;
			if ($byRecordDate) {
				$ap_records = BuildMultiKeyAssoc($ap_records, array("StudentID", "RecordDate", "RecordID"));
			} else {
				$ap_records = BuildMultiKeyAssoc($ap_records, array("StudentID", "DisplayItemName", "RecordID"));
			}
			# Table Header
			$table_header = "<table class=\"report_content\" style='padding-bottom:0px;' cellspacing=\"0\" cellpadding=\"2\" border=\"0\" align=\"center\" width=\"100%\" >
							<tbody>
							<tr valign=top>
								<td colspan=\"15\" class=\"class_info\"><span class=\"classname\">__CLASSNAME__</span> __CLASSNUMBER__ __STUDENTNAME__</td>
							</tr>
							<tr valign=top>
								<td width=\"15%\" align=\"left\">&nbsp;</td>
								<td width=\"1%\" align=\"center\">&nbsp;</td>
								<td width=\"43%\" align=\"left\">&nbsp;</td>
			
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>口頭</nobr></td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>勸告</nobr></td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>缺點</nobr></td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>小過</nobr></td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>大過</nobr></td>
								<td width=\"1%\" class='thtitle' align=\"center\">&nbsp;</td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>優點</nobr></td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>小功</nobr></td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>大功</nobr></td>
							</tr>
							</tbody>
							</table>";

			/*************************************************************/
			$x = "
			<table width=\"100%\" align=\"center\" class=\"print_hide\" border=\"0\">
				<tr valign=top>
					<td align=\"right\">".$this->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print")."</td>
				</tr>
			</table>";
		
			if (count($class_student_ary) > 0) {
			
				foreach ($class_student_ary as $classID => $classInfo) {
					
					$current_classid = $class_list[$classID]['YearClassID'];
					$current_classname = $class_list[$classID]['ClassTitleEN'];
					/*
					$class_html_header .= "<tr>";
					$class_html_header .= "	<th valign=top colspan=\"15\">";
					// $class_html_header .= str_replace("__CLASSNAME__", $className, $table_header);
					$class_html_header .= str_replace("__CLASSNAME__", "<b>" . $current_classname . "<b>", $table_header);
					$class_html_header .= "	</th>";
					$class_html_header .= "</tr>";
					*/
					if (count($classInfo) > 0) {
						foreach ($classInfo as $studentID => $studentInfo) {
							
							# Student Info
							if (empty($studentInfo['StudentName'])) {
								if (empty($studentInfo['ChineseName'])) {
									$student_name = $studentInfo['EnglishName'];
								} else {
									$student_name = Get_Lang_Selection($studentInfo['ChineseName'], $studentInfo['EnglishName']);
								}
							} else {
								$student_name = $studentInfo['StudentName'];
							}
							$class_name = $studentInfo['ClassName'];
							$class_number = $studentInfo['ClassNumber'];
							$firstLine = " style='border-top:1px solid #000; padding-top:5px;'";
							$otherLine = " style='border-top:1px solid #000; padding-top:5px;'";
							
							$box_html = "<div class=\"main_container student_mode\">";
							$class_html = "";
							$class_html .= "<table cellspacing=\"0\" cellpadding=\"2\" border=\"0\" align=\"center\" width=\"100%\" >";
							
							if ($options["textFromDate"] != $options["textToDate"]) {
								$displayDate = str_replace("-", ".", $options["textFromDate"]) . " - " . str_replace("-", ".", $options["textToDate"]);
							} else {
								$displayDate = str_replace("-", ".", $options["textFromDate"]);
							}
							
							$class_html_header = "<thead>";
							$class_html_header .= "<tr valign=top>";
							$class_html_header .= "	<th colspan=\"15\">";
							$class_html_header .= "		<table class=\"report_header report_topheader\" cellspacing=\"0\" cellpadding=\"2\" border=\"0\" align=\"center\"width=\"100%\" >";
							$class_html_header .= "		<tbody>";
							$class_html_header .= "		<tr class='main_title'>";
							$class_html_header .= "			<td align=\"center\">" . $Lang['eDiscipline']['MoPuiChingDailyReport']['Title'] . "</td>";
							$class_html_header .= "		</tr>";
							$class_html_header .= "		<tr>";
							$class_html_header .= "			<td align=\"center\">" . $displayDate . "</td>";
							$class_html_header .= "		</tr>";
							$class_html_header .= "		</tbody>";
							$class_html_header .= "		</table>";
							$class_html_header .= "	</th>";
							$class_html_header .= "</tr>";
							$class_html_header .= "<tr>";
							$class_html_header .= "	<th valign=top colspan=\"15\">";
							// $class_html_header .= str_replace("__CLASSNAME__", $className, $table_header);
							
							$strRep = array( "__CLASSNAME__" => $current_classname, "__CLASSNUMBER__" => "(" . $class_number . ")", "__STUDENTNAME__" => " " . $student_name . "" );
							
							$class_html_header .= str_replace(array_keys($strRep), array_values($strRep), $table_header);
							$class_html_header .= "	</th>";
							$class_html_header .= "</tr>";
							$class_html_header .= "</thead>";

							$student_records_array[$studentID] = array();
							$student_records_array[$studentID][0] = 0;
							$student_records_array[$studentID][1] = 0;
							$student_records_array[$studentID][2] = 0;
							$student_records_array[$studentID][3] = 0;
							$student_records_array[$studentID][-1] = 0;
							$student_records_array[$studentID][-2] = 0;
							$student_records_array[$studentID][-3] = 0;
							$student_records_array[$studentID][-4] = 0;
							$student_records_array[$studentID][-5] = 0;
							
							$class_html_body = "";
							if (isset($ap_records[$studentID]) && count($ap_records[$studentID])) {
								$TopDisplayCount = 0;
								$displayCount = 0;
								$initDisplayStyle = "";
								$currDisplay = 0;
								foreach ($ap_records[$studentID] as $itemKey => $ap_info) {
									if (count($ap_info) > 0) {
										/*
										$class_html_body .= "<tr valign=top>
															<td  class=\"report_data\"width=\"2%\" align=\"right\" " . $firstLine .">&nbsp;</td>
															<td class=\"report_data\" width=\"1%\" align=\"center\" " . $firstLine .">&nbsp;</td>
															<td class=\"report_data\" width=\"20%\" align=\"left\" " . $firstLine .">&nbsp;</td>
															<td class=\"report_data\" width=\"1%\" align=\"center\" " . $firstLine .">&nbsp;</td>
															<td class=\"report_data\" width=\"43%\" colspan=\"2\" align=\"left\" " . $otherLine .">" . $DisplayName . "</td>
															<td class=\"report_data\" colspan=\"9\" " . $otherLine .">&nbsp;</td>
														<tr>";
										*/
										$firstLine = "";
										$otherLine = " style='border-top:1px solid #afafaf; padding-top:5px;'";
										$class_number = "&nbsp;";
										$student_name = "&nbsp;";

										foreach ($ap_info as $RecordID => $recInfo) {
											$DisplayName = $recInfo["DisplayItemName"];
											# Merit Type Count
											$ap_records_array = array();
											 
											$ap_records_array[0] = 0;
											$ap_records_array[1] = 0;
											$ap_records_array[2] = 0;
											$ap_records_array[3] = 0;
											$ap_records_array[-1] = 0;
											$ap_records_array[-2] = 0;
											$ap_records_array[-3] = 0;
											$ap_records_array[-4] = 0;
											$ap_records_array[-5] = 0;
											# Record Retails
											$ap_merit_type	= $recInfo['ProfileMeritType'];
											$ap_item_type 	= $recInfo['RelatedMeritType'];
											// $item_title 	= $recInfo['ItemName'];
											$item_title 	= "";
											$item_count	   = $recInfo['CountNo']? "<nobr>(".$recInfo['CountNo'].")</nobr>" : "";
											$GMremark       = trim($recInfo['GMRemark'])? $recInfo['GMRemark'] : "";
											$APremark       = trim($recInfo['APRemark'])? $recInfo['APRemark'] : "";
											
											$ap_records_array[$ap_merit_type] += $ap_item_type;
											
											$student_records_array[$studentID][$ap_merit_type] += $ap_item_type; 
											
											$remark = "";
											if($GMremark!=""){
												$remark = "".$GMremark."";
											}
											else if($APremark!="" && !$recInfo['CountNo']){
												$remark = "".$APremark."";
											}
										
											if ($item_title == $DisplayName) {
												$item_title = "";
											}
											$description = empty($item_title) ? "" : $item_title;
											if (!empty($remark)) {
												$description .= " [" . $remark . "]";
											}
											
											$displayCount++;
											if ($TopDisplayCount >= 1) {
												if ($currDisplay != $TopDisplayCount) {
													$currDisplay = $TopDisplayCount;
													if (!$byRecordDate) $initDisplayStyle = " style='border-top:1px solid #dfdfdf;'";
												} else {
													$initDisplayStyle = "";
												}
											} else {
												$initDisplayStyle = "";
											}
											/*
											if (empty($description)) {
												$infoTD = "<td width=\"51%\" colspan=\"2\" align=\"left\" " . $initDisplayStyle . ">" . $recInfo["DisplayItemName"] . $item_count . "</td>";
											} else {
												$infoTD = "<td width=\"16%\" align=\"left\" " . $initDisplayStyle . ">" . $recInfo["DisplayItemName"] . $item_count . "</td>
															<td width=\"35%\" align=\"left\" " . $initDisplayStyle . ">" . $description . "</td>";
											}*/
											$infoTD = "<td width=\"43%\" align=\"left\" " . $initDisplayStyle . ">" . $recInfo["DisplayItemName"] . $item_count . "" . $description . "</td>";
											if (initDisplay )
											$class_html_body .= "<tr valign=top>
															<td width=\"15%\" align=\"left\" " . $initDisplayStyle . "><nobr>" . $recInfo["RecordDate"] . "</nobr></td>
															<td width=\"1%\" align=\"center\" " . $initDisplayStyle . ">&nbsp;</td>" . $infoTD . "
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $initDisplayStyle . ">".$ap_records_array[-1]."</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $initDisplayStyle . ">".$ap_records_array[-2]."</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $initDisplayStyle . ">".$ap_records_array[-3]."</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $initDisplayStyle . ">".$ap_records_array[-4]."</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $initDisplayStyle . ">".$ap_records_array[-5]."</td>
															<td class=\"report_data\" width=\"1%\" align=\"center\" " . $initDisplayStyle . ">&nbsp;</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $initDisplayStyle . ">".$ap_records_array[1]."</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $initDisplayStyle . ">".$ap_records_array[2]."</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $initDisplayStyle . ">".$ap_records_array[3]."</td>
														<tr>";
											
										}
									}
									$TopDisplayCount++;
								}
								$tdStyle = " style='border-top:1px solid #afafaf; padding-top:5px; '";
								$class_html_body .= "<tr valign=top>
															<td width=\"15%\" align=\"left\" " . $tdStyle. ">&nbsp;</td>
															<td width=\"1%\" align=\"center\" " . $tdStyle. ">&nbsp;</td>
															<td width=\"43%\" align=\"right\" " . $tdStyle. ">" . $Lang['General']['Total'] . "</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $tdStyle. ">".$student_records_array[$studentID][-1]."</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $tdStyle. ">".$student_records_array[$studentID][-2]."</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $tdStyle. ">".$student_records_array[$studentID][-3]."</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $tdStyle. ">".$student_records_array[$studentID][-4]."</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $tdStyle. ">".$student_records_array[$studentID][-5]."</td>
															<td class=\"report_data\" width=\"1%\" align=\"center\" " . $tdStyle. ">&nbsp;</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $tdStyle. ">".$student_records_array[$studentID][1]."</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $tdStyle. ">".$student_records_array[$studentID][2]."</td>
															<td class=\"report_data\" width=\"5%\" align=\"center\" " . $tdStyle. ">".$student_records_array[$studentID][3]."</td>
														<tr>";
								$class_html_body .= "<tr>
														<td colspan=\"14\">&nbsp;</td>
													<tr>";
								$class_number = "&nbsp;";
								$student_name = "&nbsp;";
							}
							if (!empty($class_html_body)) {
								$withRecords = true;
								$class_html .= $class_html_header;
								
								$class_html .= "<tbody>" . $class_html_body . "</tbody>";
								$class_html .= "</table>";
								$x .= $box_html;
								$x .= $class_html;
								$x .= "</div>";
							}
						}
					}
					
					
				}
			}
			
			/*************************************************************/
			if(!$withRecords){
				global $i_no_record_exists_msg;
					
				$x .= "<div align='center' style='font-size:20px;'>";
				$x .= "$i_no_record_exists_msg";
				$x .= "</div>";
			}
				
			$x .= "</div>";
			
		} else {
			$ap_records = BuildMultiKeyAssoc($ap_records, array("DisplayItemName", "StudentID", "RecordID"));
			
			# Report Settings
			//$maxRow = 39;			// 680px
			$maxRow = 56.5;			// 1060px
			$headerRow = 2.4; 		// 45px
			$tableHeaderRow = 1.7; 	// 31px
			$tableContentRow = 1.3; // 23px
			$tableFooterRow = 0.6; 	// 12px
			
			# Table Header
			$table_header = "<table class=\"report_content\" cellspacing=\"0\" cellpadding=\"2\" border=\"0\" align=\"center\"width=\"100%\" >
							<tbody>
							<tr>
								<td width=\"3%\" align=\"right\">&nbsp;</td>
								<td width=\"1%\" align=\"center\">&nbsp;</td>
								<td width=\"13%\" align=\"left\">&nbsp;</td>
								<td width=\"1%\" align=\"center\">&nbsp;</td>
								<td width=\"41%\" align=\"left\">&nbsp;</td>
				
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>口頭</nobr></td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>勸告</nobr></td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>缺點</nobr></td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>小過</nobr></td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>大過</nobr></td>
								<td width=\"1%\" class='thtitle' align=\"center\">&nbsp;</td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>優點</nobr></td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>小功</nobr></td>
								<td width=\"5%\" class='thtitle' align=\"center\"><nobr>大功</nobr></td>
							</tr>
							</tbody>";
			
			# Build Content
			$x = "
			<table width=\"100%\" align=\"center\" class=\"print_hide\" border=\"0\">
				<tr>
					<td align=\"right\">".$this->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print")."</td>
				</tr>
			</table>
			<div>";
			
			$withRecords = false;
			
			// display class header without line break
			$isClassHeader = 2;
			
			# Loop Selected Classes
			for($i=0; $i<count($class_list); $i++)
			{
				$current_classid = $class_list[$i]['YearClassID'];
				$current_classname = $class_list[$i]['ClassTitleEN'];
					
				# Report header
				//<div class=\"main_container\" style=\"height:640px\">
				$class_header = "
				<div class=\"main_container\">
				<table class=\"report_header\" cellspacing=\"0\" cellpadding=\"5\" border=\"0\" width=\"100%\">
				<tr class='main_title'>
					<td colspan=\"3\" align=\"center\">" . $Lang['eDiscipline']['MoPuiChingDailyReport']['Title'] . "</td>
				</tr>
				<tr>
				<td width=\"15%\">$current_classname</td>
				<td align=\"center\">$targetDate</td>
				<td width=\"15%\">&nbsp;</td>
				</tr>
				</table>";
					
				//			$withRecords = false;
				if(count($class_student_ary[$current_classid]) > 0)
				{
					$class_students = (array)$class_student_ary[$current_classid];
			
					if(count($ap_records) > 0)
					{
						$withRecords = true;
						$acrossLineBreak = false;
							
						//foreach($ap_records as $ap_item_title => $ap_student_record){
						foreach($ap_records as $item_name => $ap_student_record){
							$isFirst = true;
							$isLineBreak = false;
			
							foreach($class_students as $student_id => $current_students){
								if(count($ap_student_record[$student_id]) > 0)
								{
									# Student Info
									$student_name = $current_students['StudentName'];
									$class_name = $current_students['ClassName'];
									$class_number = $current_students['ClassNumber'];
			
									# Loop AP Records
									foreach($ap_student_record[$student_id] as $recordids => $student_records){
			
										# Merit Type Count
										$ap_records_array = array();
										$ap_records_array[0] = 0;
										$ap_records_array[1] = 0;
										$ap_records_array[2] = 0;
										$ap_records_array[3] = 0;
										$ap_records_array[-1] = 0;
										$ap_records_array[-2] = 0;
										$ap_records_array[-3] = 0;
										$ap_records_array[-4] = 0;
										$ap_records_array[-5] = 0;
			
										# Record Retails
										$ap_merit_type	= $student_records['ProfileMeritType'];
										$ap_item_type 	= $student_records['RelatedMeritType'];
										$item_title 	= $item_name;
										$item_title	   .= $student_records['CountNo']? "(".$student_records['CountNo'].")" : "";
										$GMremark       = trim($student_records['GMRemark'])? $student_records['GMRemark'] : "";
										$APremark       = trim($student_records['APRemark'])? $student_records['APRemark'] : "";
											
										// Remark display
										$remark = "";
										$thisTableContentRow = $tableContentRow;
										$wordLength = mb_strlen($item_name, "UTF-8");
										if($GMremark!=""){
											$remark = " [".$GMremark."]";
											$wordLength += mb_strlen($GMremark, "UTF-8") + 1;
										}
										else if($APremark!="" && !$student_records['CountNo']){
											$remark = " [".$APremark."]";
											$wordLength += mb_strlen($APremark, "UTF-8") + 1;
										}
										$itemLineCount = ceil($wordLength / 19);
										if($itemLineCount > 1)
											$thisTableContentRow += (1 * $itemLineCount);
												
											$ap_records_array[$ap_merit_type] += $ap_item_type;
			
											# Display Report Header
											if($isClassHeader){
												// add line break if not the first class with records
												if($isClassHeader === true){
													$x .= "<div style=\"page-break-after:always\">&nbsp;</div>";
												}
												$x .= $class_header;
			
												$isClassHeader = false;
												$acrossLineBreak = false;
												$countRow = $maxRow - $headerRow;
											}
												
											# Display Table header
											if($isFirst){
												// Line break handling
												if($isLineBreak || $acrossLineBreak){
													$x .= "</table>";
													$x .= "</div>";
													$x .= "<div style=\"page-break-after:always\">&nbsp;</div>";
														
													$x .= $class_header;
														
													$isLineBreak = false;
													$acrossLineBreak = false;
													$countRow = $maxRow - $headerRow;
												}
												else{
													$countRow -= $tableFooterRow;
												}
												$x .= $table_header;
			
												$isFirst = false;
												$countRow -= $tableHeaderRow;
											}
												
											$x .= "<tr>
											<td class=\"report_data\" align=\"right\">$class_number</td>
											<td class=\"report_data\" align=\"center\">&nbsp;</td>
											<td class=\"report_data\" align=\"left\">$student_name</td>
											<td class=\"report_data\" align=\"center\">&nbsp;</td>
											<td class=\"report_data\" align=\"left\" style=\"word-break:break-word\">".$item_title."$remark"."</td>
			
												<td class=\"report_data\" align=\"center\">".$ap_records_array[-1]."</td>
												<td class=\"report_data\" align=\"center\">".$ap_records_array[-2]."</td>
												<td class=\"report_data\" align=\"center\">".$ap_records_array[-3]."</td>
												<td class=\"report_data\" align=\"center\">".$ap_records_array[-4]."</td>
												<td class=\"report_data\" align=\"center\">".$ap_records_array[-5]."</td>
												<td class=\"report_data\" align=\"center\">&nbsp;</td>
												<td class=\"report_data\" align=\"center\">".$ap_records_array[1]."</td>
												<td class=\"report_data\" align=\"center\">".$ap_records_array[2]."</td>
												<td class=\"report_data\" align=\"center\">".$ap_records_array[3]."</td>
											</tr>";
												
											$countRow -= $thisTableContentRow;
											if($countRow < 3){
												$isFirst = true;
												$isLineBreak = true;
											}
									}
								}
							}
			
							if(!$isFirst || $isLineBreak){
								$x .= "</table>";
									
								if($isLineBreak)
									$acrossLineBreak = true;
							}
						}
					}
				}
				//			if(!$withRecords){
				//				global $i_no_record_exists_msg;
				//
				//				if($isClassHeader === true){
				//					$x .= "<div style=\"page-break-after:always\">&nbsp;</div>";
				//				}
				//
				//				$x .= "$class_header";
				//				$x .= "$table_header";
				//				$x .= "	<tr><td colspan='14' style='text-align:center;font-size:14px;'>$i_no_record_exists_msg</td></tr>
				//					</table>";
				//
				//				$isClassHeader = false;
				//				$countRow = $maxRow - $headerRow;
				//
				//			}
				if(!$isClassHeader){
					$x .= "</div>";
			
					$isClassHeader = true;
				}
			}
			
			if(!$withRecords){
				global $i_no_record_exists_msg;
					
				$x .= "<div align='center' style='font-size:20px;'>";
				$x .= "$i_no_record_exists_msg";
				$x .= "</div>";
			}
			
			$x .= "</div>";
		}
		return $x;
	}
	# [Escola Secundaria Pui Ching de Macau]
	
	# [Tin Ka Ping Secondary School] - $sys_custom['eDiscipline']['TKPSchoolRecordSummary']
	function getTKPSchoolRecordSummary($format, $currentYearClass, $student_ary, $semester, $gm_records, $ap_records, $ap_waived_records, $detention_records, $isLast=false, $rows=array())
	{
		global $PATH_WRT_ROOT, $intranet_root, $intranet_session_language, $Lang, $no_record_msg;
		global $ldiscipline;
		
		$x = '';
		
		$student_list = $student_ary[$currentYearClass];
		$first_term = $semester[0];
		$last_term = $semester[1];
		
		# Table Header
		if($format == 'pdf')
		{
			$x = "";
			$x .= "<table class='result_table' cellspacing='0' cellpadding='2' width='100%'>";
			$x .= "<thead>";
				$x .= "<tr>";
					$x .= "<th width='14%' style='border-left:0px solid black' colspan='3'>&nbsp;</th>";
					$x .= "<th width='1'>&nbsp;</th>";
					$x .= "<th width='35%' colspan='10'>全學年累計違規總表</th>";
					$x .= "<th width='1'>&nbsp;</th>";
					$x .= "<th width='36%' colspan='9'>欠交及遲到紀錄</th>";
					$x .= "<th width='1'>&nbsp;</th>";
					$x .= "<th width='16%' colspan='5'>全年好名/優點</th>";
				$x .= "</tr>";
				$x .= "<tr>";
					$x .= "<th width='4%' style='border-left:0px solid black'>班別</th>";
					$x .= "<th width='4%'>學號</th>";
					$x .= "<th width='6%'>中文姓名</th>";
					$x .= "<th width='1'>&nbsp;</th>";
					$x .= "<th width='3%'>留堂</th>";
					$x .= "<th width='3%'>口頭警告</th>";
					$x .= "<th width='3%'>書面警告</th>";
					$x .= "<th width='3%'>記缺點</th>";
					$x .= "<th width='5%'>彩虹<br>計劃<br>(缺點)</th>";
					$x .= "<th width='3%'>小過</th>";
					$x .= "<th width='5%'>彩虹<br>計劃<br>(小過)</th>";
					$x .= "<th width='4%'>全年合計</th>";
					$x .= "<th width='3%'>上學期</th>";
					$x .= "<th width='3%'>下學期</th>";
					$x .= "<th width='1'>&nbsp;</th>";
					$x .= "<th width='4%'>校服儀容<br>(上)</th>";
					$x .= "<th width='4%'>校服儀容<br>(下)</th>";
					$x .= "<th width='4%'>欠功<br>課紀<br>錄<br>(上)</th>";
					$x .= "<th width='4%'>欠功<br>課紀<br>錄<br>(下)</th>";
					$x .= "<th width='4%'>遲到<br>(上)</th>";
					$x .= "<th width='4%'>遲到<br>(下)</th>";
					$x .= "<th width='4%'>欠通告<br>(上)</th>";
					$x .= "<th width='4%'>欠通告<br>(下)</th>";
					$x .= "<th width='4%'>輔委會紀錄總計</th>";
					$x .= "<th width='1'>&nbsp;</th>";
					$x .= "<th width='3%'>優異服務獎</th>";
					$x .= "<th width='3%'>嘉許信</th>";
					$x .= "<th width='3%'>好名</th>";
					$x .= "<th width='3%'>優點</th>";
					$x .= "<th width='4%'>總<br>計</th>";
				$x .= "</tr>";
			$x .= "</thead>";
			$x .= "<tbody>";
		}
		else
		{
			$row = array("");
			$rows[] = $row;
			$row = array("", "", "", "全學年累計違規總表", "", "", "", "", "", "", "", "", "", "欠交及遲到紀錄", "", "", "", "", "", "", "", "", "全年好名/優點", "", "");
			$rows[] = $row;
			$row = array("班別", "學號", "中文姓名", "留堂", "口頭警告", "書面警告", "紀缺點", "刪除缺點_彩虹", "小過", "刪除小過_彩虹", "全年合計", "上學期", "下學期", 
							"校服儀容(上)", "校服儀容(下)", "欠功課紀錄上", "欠功課紀錄下", "遲到上", "遲到下", "欠通告上", "欠通告下", "輔委會紀錄總計", "優異服務獎", "嘉許信", "好名", "優點", "總計");
			$rows[] = $row;
		}
		
		if(count($student_list) > 0){
			foreach((array)$student_list as $currentStudentID => $currentStudent)
			{
				# Student Info
				$student_id = $currentStudent['UserID'];
				$student_name = $currentStudent['StudentName'];
				$class_name = $currentStudent['ClassName'];
				$class_number = $currentStudent['ClassNumber'];
				
				# Record Info
				// Detention
				//$detention_count = $detention_records[$student_id]['DetentionTypeRecordCount']? $detention_records[$student_id]['DetentionTypeRecordCount'] : "";
				// Demerit
				$detention_1st = $ap_records[$student_id][-1][$first_term]['MeritRecordCount']? $ap_records[$student_id][-1][$first_term]['MeritRecordCount'] : "";
				$detention_2nd = $ap_records[$student_id][-1][$last_term]['MeritRecordCount']? $ap_records[$student_id][-1][$last_term]['MeritRecordCount'] : "";
				$warning_1st = $ap_records[$student_id][0][$first_term]['MeritRecordCount']? $ap_records[$student_id][0][$first_term]['MeritRecordCount'] : "";
				$warning_2nd = $ap_records[$student_id][0][$last_term]['MeritRecordCount']? $ap_records[$student_id][0][$last_term]['MeritRecordCount'] : "";
				$warning2_1st = $ap_records[$student_id][-2][$first_term]['MeritRecordCount']? $ap_records[$student_id][-2][$first_term]['MeritRecordCount'] : "";
				$warning2_2nd = $ap_records[$student_id][-2][$last_term]['MeritRecordCount']? $ap_records[$student_id][-2][$last_term]['MeritRecordCount'] : "";
				$demerit_1st = $ap_records[$student_id][-3][$first_term]['MeritRecordCount']? $ap_records[$student_id][-3][$first_term]['MeritRecordCount'] : "";
				$demerit_2nd = $ap_records[$student_id][-3][$last_term]['MeritRecordCount']? $ap_records[$student_id][-3][$last_term]['MeritRecordCount'] : "";
				$minor_demerit_1st = $ap_records[$student_id][-4][$first_term]['MeritRecordCount']? $ap_records[$student_id][-4][$first_term]['MeritRecordCount'] : "";
				$minor_demerit_2nd = $ap_records[$student_id][-4][$last_term]['MeritRecordCount']? $ap_records[$student_id][-4][$last_term]['MeritRecordCount'] : "";
				// Merit
				$good_deed_1st = $ap_records[$student_id][1][$first_term]['MeritRecordCount']? $ap_records[$student_id][1][$first_term]['MeritRecordCount'] : "";
				$good_deed_2nd = $ap_records[$student_id][1][$last_term]['MeritRecordCount']? $ap_records[$student_id][1][$last_term]['MeritRecordCount'] : "";
				$merit_1st = $ap_records[$student_id][2][$first_term]['MeritRecordCount']? $ap_records[$student_id][2][$first_term]['MeritRecordCount'] : "";
				$merit_2nd = $ap_records[$student_id][2][$last_term]['MeritRecordCount']? $ap_records[$student_id][2][$last_term]['MeritRecordCount'] : "";
				$letter_1st = $ap_records[$student_id][3][$first_term]['MeritRecordCount']? $ap_records[$student_id][3][$first_term]['MeritRecordCount'] : "";
				$letter_2nd = $ap_records[$student_id][3][$last_term]['MeritRecordCount']? $ap_records[$student_id][3][$last_term]['MeritRecordCount'] : "";
				$outstanding_1st = $ap_records[$student_id][4][$first_term]['MeritRecordCount']? $ap_records[$student_id][4][$first_term]['MeritRecordCount'] : "";
				$outstanding_2nd = $ap_records[$student_id][4][$last_term]['MeritRecordCount']? $ap_records[$student_id][4][$last_term]['MeritRecordCount'] : "";
				// Waived
				$waived_demerit_1st = $ap_waived_records[$student_id][-3][$first_term]['MeritRecordCount']? $ap_waived_records[$student_id][-3][$first_term]['MeritRecordCount'] : "";
				$waived_demerit_2nd = $ap_waived_records[$student_id][-3][$last_term]['MeritRecordCount']? $ap_waived_records[$student_id][-3][$last_term]['MeritRecordCount'] : "";
				$waived_minor_demerit_1st = $ap_waived_records[$student_id][-4][$first_term]['MeritRecordCount']? $ap_waived_records[$student_id][-4][$first_term]['MeritRecordCount'] : "";
				$waived_minor_demerit_2nd = $ap_waived_records[$student_id][-4][$last_term]['MeritRecordCount']? $ap_waived_records[$student_id][-4][$last_term]['MeritRecordCount'] : "";
				// Conduct
				$dressing_1st = $gm_records[$student_id][16][$first_term]['GMRecordCount']? $gm_records[$student_id][16][$first_term]['GMRecordCount'] : "";
				$dressing_2nd = $gm_records[$student_id][16][$last_term]['GMRecordCount']? $gm_records[$student_id][16][$last_term]['GMRecordCount'] : "";
				$homework_1st = $gm_records[$student_id][2][$first_term]['GMRecordCount']? $gm_records[$student_id][2][$first_term]['GMRecordCount'] : "";
				$homework_2nd = $gm_records[$student_id][2][$last_term]['GMRecordCount']? $gm_records[$student_id][2][$last_term]['GMRecordCount'] : "";
				$late_1st = $gm_records[$student_id][1][$first_term]['GMRecordCount']? $gm_records[$student_id][1][$first_term]['GMRecordCount'] : "";
				$late_2nd = $gm_records[$student_id][1][$last_term]['GMRecordCount']? $gm_records[$student_id][1][$last_term]['GMRecordCount'] : "";
				$notice_1st = $gm_records[$student_id][3][$first_term]['GMRecordCount']? $gm_records[$student_id][3][$first_term]['GMRecordCount'] : "";
				$notice_2nd = $gm_records[$student_id][3][$last_term]['GMRecordCount']? $gm_records[$student_id][3][$last_term]['GMRecordCount'] : "";
				
				# Calculation
				// Demerit
				$detention_count = $detention_1st + $detention_2nd;
				$warning = $warning_1st + $warning_2nd;
				$warning2 = $warning2_1st + $warning2_2nd;
				$demerit = $demerit_1st + $demerit_2nd;
				$minor_demerit = $minor_demerit_1st + $minor_demerit_2nd;
				$waived_demerit = $waived_demerit_1st + $waived_demerit_2nd;
				$waived_minor_demerit = $waived_minor_demerit_1st + $waived_minor_demerit_2nd;
				$total_demerit = $warning + $warning2 + $demerit + $minor_demerit + $detention_count;
				$first_term_demerit = $warning_1st + $warning2_1st + $demerit_1st + $minor_demerit_1st + $detention_1st;
				$second_term_demerit = $warning_2nd + $warning2_2nd + $demerit_2nd + $minor_demerit_2nd + $detention_2nd;
				$detention_count = $detention_count? $detention_count : "";
				$warning = $warning? $warning : "";
				$warning2 = $warning2? $warning2 : "";
				$demerit = $demerit? $demerit : "";
				$minor_demerit = $minor_demerit? $minor_demerit : "";
				$waived_demerit = $waived_demerit? $waived_demerit : "";
				$waived_minor_demerit = $waived_minor_demerit? $waived_minor_demerit : "";
				$total_demerit = $total_demerit? $total_demerit : "";
				$first_term_demerit = $first_term_demerit? $first_term_demerit : "";
				$second_term_demerit = $second_term_demerit? $second_term_demerit : "";
				// Conduct
				$total_conduct = $dressing_1st + $dressing_2nd + $homework_1st + $homework_2nd + $late_1st + $late_2nd + $notice_1st + $notice_2nd;
				$total_conduct = $total_conduct? $total_conduct : "";
				// Merit
				$good_deed = $good_deed_1st + $good_deed_2nd;
				$merit = $merit_1st + $merit_2nd;
				$letter = $letter_1st + $letter_2nd;
				$outstanding = $outstanding_1st + $outstanding_2nd;
				$total_merit = $good_deed + $merit + $letter + $outstanding;
				$good_deed = $good_deed? $good_deed : "";
				$merit = $merit? $merit : "";
				$letter = $letter? $letter : "";
				$outstanding = $outstanding? $outstanding : "";
				$total_merit = $total_merit? $total_merit : "";

				# Student Record
				if($format == 'pdf')
				{
					$x .= "<tr>";
						$x .= "<td style='border-left:0px solid black'>$class_name</td>";
						$x .= "<td>$class_number</td>";
						$x .= "<td>$student_name</td>";
						$x .= "<td>&nbsp;</td>";
						$x .= "<td>$detention_count</td>";
						$x .= "<td>$warning</td>";
						$x .= "<td>$warning2</td>";
						$x .= "<td>$demerit</td>";
						$x .= "<td>$waived_demerit</td>";
						$x .= "<td>$minor_demerit</td>";
						$x .= "<td>$waived_minor_demerit</td>";
						$x .= "<td>$total_demerit</td>";
						$x .= "<td>$first_term_demerit</td>";
						$x .= "<td>$second_term_demerit</td>";
						$x .= "<td>&nbsp;</td>";
						$x .= "<td>$dressing_1st</td>";
						$x .= "<td>$dressing_2nd</td>";
						$x .= "<td>$homework_1st</td>";
						$x .= "<td>$homework_2nd</td>";
						$x .= "<td>$late_1st</td>";
						$x .= "<td>$late_2nd</td>";
						$x .= "<td>$notice_1st</td>";
						$x .= "<td>$notice_2nd</td>";
						$x .= "<td>$total_conduct</td>";
						$x .= "<td>&nbsp;</td>";
						$x .= "<td>$outstanding</td>";
						$x .= "<td>$letter</td>";
						$x .= "<td>$good_deed</td>";
						$x .= "<td>$merit</td>";
						$x .= "<td>$total_merit</td>";
					$x .= "</tr>";
				}
				else
				{
					$row = array("$class_name", "$class_number", "$student_name", "$detention_count", "$warning", "$warning2", "$demerit", "$waived_demerit", "$minor_demerit", "$waived_minor_demerit", 
									"$total_demerit", "$first_term_demerit", "$second_term_demerit", "$dressing_1st", "$dressing_2nd", "$homework_1st", "$homework_2nd", "$late_1st", "$late_2nd", 
									"$notice_1st", "$notice_2nd", "$total_conduct", "$outstanding", "$letter", "$good_deed", "$merit", "$total_merit");
					$rows[] = $row;	
				}
			}
		}
		else {
			if($format == 'pdf') {
				$x .= "<tr>";
					$x .= "<td style='border-left:0px solid black' colspan='30'>$no_record_msg</td>";
				$x .= "</tr>";
			}
			else {
				$row = array("$no_record_msg");
				$rows[] = $row;	
			}
		}
		
		$x .= "</tbody>";
		$x .= "</table>";
		
		if($format == 'pdf' && !$isLast)
		{
			$x .= "<pagebreak />";
		}
		
		if($format == 'csv')
		{
			return $rows;
		} else 
		{
			return $x;
		}
	}
	
	function getTKPClassDemeritRecord($format, $currentYearClass, $student_ary, $pic_ary, $ap_records, $isLast=false, $rows=array()){
		
		global $PATH_WRT_ROOT, $intranet_root, $intranet_session_language, $Lang, $no_record_msg;
		global $ldiscipline, $merit_type_ary;
		
		$x = '';
		
		$current_class_name = $ldiscipline->getClassNameByClassID($currentYearClass);
		$student_list = $student_ary[$currentYearClass];
		
		# Table Header
		if($format == 'pdf')
		{
			$x = "";
			$x .= "<table class='result_table' cellspacing='0' cellpadding='4' border='0' width='100%'>";
			$x .= "<thead>";
				$x .= "<tr>";
					$x .= "<th class='result_table_header' width='5%'>班別</th>";
					$x .= "<th class='result_table_header' width='15%'>中文姓名</th>";
					$x .= "<th class='result_table_header' width='10%'>違規日期</th>";
					$x .= "<th class='result_table_header' width='25%'>犯事詳情</th>";
					$x .= "<th class='result_table_header' width='10%'>懲罰</th>";
					$x .= "<th class='result_table_header' width='5%'>次數</th>";
					$x .= "<th class='result_table_header' width='15%'>備註</th>";
					$x .= "<th class='result_table_header' width='15%'>教師姓名</th>";
				$x .= "</tr>";
				$x .= "<tr>";
					$x .= "<th>$current_class_name</th>";
					$x .= "<th>&nbsp;</th>";
					$x .= "<th>&nbsp;</th>";
					$x .= "<th>&nbsp;</th>";
					$x .= "<th>&nbsp;</th>";
					$x .= "<th>&nbsp;</th>";
					$x .= "<th>&nbsp;</th>";
					$x .= "<th>&nbsp;</th>";
				$x .= "</tr>";
			$x .= "</thead>";
			
			$x .= "<tbody>";
		}
		else
		{
			$row = array("");
			$rows[] = $row;
			$row = array("班別", "中文姓名", "違規日期", "犯事詳情", "懲罰", "次數", "備註", "教師姓名");
			$rows[] = $row;
			$row = array("$current_class_name");
			$rows[] = $row;
		}
		
		$withRecords = false;
		foreach((array)$student_list as $currentStudentID => $currentStudent)
		{
			# Student Info
			$student_id = $currentStudent['UserID'];
			$student_name = $currentStudent['StudentName'];
			$class_name = $currentStudent['ClassName'];
			$class_number = $currentStudent['ClassNumber'];
			
			# Record Info
			$current_ap_record = $ap_records[$currentStudentID];
			
			if(count($current_ap_record) > 0){
				$withRecords = true;
				
				foreach($current_ap_record as $current_record)
				{
					# Record Info
					$record_date = $current_record['RecordDate'];
					$record_item = $current_record['ItemName'];
					$record_merit_type = $current_record['ProfileMeritType'];
					$record_merit_count = $current_record['MeritRecordCount'];
					$record_remark = $current_record['Remark'];
					$record_pics = $current_record['PICID'];
					
					// Convert
					$record_merit_type = $merit_type_ary[$record_merit_type];
					$record_merit_type = $record_merit_type? $record_merit_type : $Lang['General']['EmptySymbol'];
					$record_pics = $pic_ary[$record_pics]['PICName'];
					$record_pics = $record_pics? $record_pics : $Lang['General']['EmptySymbol'];
					
					# Student Record
					if($format == 'pdf')
					{
						$x .= "<tr>";
							$x .= "<td>$class_number</td>";
							$x .= "<td>$student_name</td>";
							$x .= "<td>$record_date</td>";
							$x .= "<td>$record_item</td>";
							$x .= "<td>$record_merit_type</td>";
							$x .= "<td>$record_merit_count</td>";
							$x .= "<td>$record_remark</td>";
							$x .= "<td>$record_pics</td>";
						$x .= "</tr>";
					}
					else
					{
						$row = array("$class_number", "$student_name", "$record_date", "$record_item", "$record_merit_type", "$record_merit_count", "$record_remark", "$record_pics");
						$rows[] = $row;	
					}
				}
			}
		}
		
		if(!$withRecords) {
			if($format == 'pdf') {
				$x .= "<tr>";
					$x .= "<td colspan='8'>$no_record_msg</td>";
				$x .= "</tr>";
			}
			else {
				$row = array("$no_record_msg");
				$rows[] = $row;	
			}
		}
		
		$x .= "</tbody>";
		$x .= "</table>";
		
		if($format == 'pdf' && !$isLast)
		{
			$x .= "<pagebreak />";
		}
		
		if($format == 'csv')
		{
			return $rows;
		} else 
		{
			return $x;
		}
	}
	# [Tin Ka Ping Secondary School]
	
	# [Po Leung Kuk C W Chu College] Start
	function getClassStudentImageTable($YearClassID){
		global $Lang, $PATH_WRT_ROOT, $intranet_root;
		global $laccount;
		
//		$x .= '<table width="100%" cellpadding="5" cellspacing="0" border="0">';
//		$x .= '<tr class="tabletop">';
//			$x .= '<td class="tabletopnolink" width="15%">'.$Lang['AccountMgmt']['ClassNo'].'</td>';
//			$x .= '<td class="tabletopnolink" width="35%">'.$Lang['AccountMgmt']['StudentName'].'</td>';
//			$x .= '<td class="tabletopnolink" width="25%">'.$Lang['AccountMgmt']['LoginID'].'</td>';
//			$x .= '<td class="tabletopnolink" width="25%">'.$Lang['AccountMgmt']['Photo'].'</td>';
//			$x .= '<td class="tabletopnolink" width="10px"><input id="CheckAllPhoto" type="checkbox" onclick="CheckAll(\'UserID[]\')"></td>';
//		$x .= '</tr>';
			
		$count = 0;
		$StudentList = $laccount->getStudentUserInfoListByClassID($YearClassID);

		// layout start
		$x .= "<div class='photo_div'>";
		$x .= "<ul id='selected_photo_list' class='ui-sortable'>";
		
		// loop students
		foreach((array)$StudentList as $StudentInfo)
		{
			// user info
			$student_name = Get_Lang_Selection($StudentInfo["ChineseName"],$StudentInfo["EnglishName"]);
			$student_userid = $StudentInfo['UserID'];
			$student_userlogin = $StudentInfo['UserLogin'];
			$student_classno = $StudentInfo['ClassNumber'];
			
			// user photo - user login
			$user_login_path = $intranet_root."/file/user_photo/".$student_userlogin.".jpg";
			
			// personal photo - user id
			$photoname_uid = "p".$student_userid;
			$uid_path1 = $intranet_root."/file/photo/personal/". $photoname_uid.".jpg";
			$uid_path2 = $intranet_root."/file/photo/personal/". $photoname_uid.".JPG";
			
			// photo - user login
			if(file_exists($user_login_path)){
				$img = '<img src="/file/user_photo/'.$student_userlogin.'.jpg" width="100px" height="130px">';
			}
			// personal photo - user id
			else if(file_exists($uid_path1)){
				$img = '<img src="/file/photo/personal/p'.$student_userid.'.jpg" width="100px" height="130px">';
			}
			else if(file_exists($uid_path2)){
				$img = '<img src="/file/photo/personal/p'.$student_userid.'.JPG" width="100px" height="130px">';
			}
			// default photo
			else {
				$img = '<img src="/images/myaccount_personalinfo/samplephoto.gif" width="100px" height="130px">';
			}
//			$checkBox = '<input type="checkbox" name="UserID[]" value="'.$student_userid.'">';
			
			$x .= "<li class='user_photo' onclick='clickedPhoto(this, $count)'>";
				$x .= "<div>";
					$x .= $img;
					$x .= "<p class='spacer'></p>";
					$x .= "<span>$student_name - $student_classno</span>";
					$x .= "<input type='hidden' id='userid_$count' name='user_id[]' value='$student_userid'>";
					$x .= "<input type='hidden' id='name_$count' name='name_$count' value='$student_name'>";
					$x .= "<input type='hidden' id='classno_$count' name='classno_$count' value='$student_classno'>";
					$x .= "<input type='hidden' id='select_$count' name='select[]' value=''>";
					$x .= "<p class='spacer'></p>";
				$x .= "</div>";
			$x .= "</li>";
				
//			$css = (++$ctr%2==1)?"retablerow1":"retablerow2";
//			$x .= '<tr class="'.$css.'">';
//				$x .= '<td id="class_num_'.$student_userid.'" class="tabletext " valign="top">'.$StudentInfo["ClassNumber"].'</td>';
//				$x .= '<td id="name_'.$student_userid.'" class="tabletext " valign="top">'.Get_Lang_Selection($StudentInfo["ChineseName"],$StudentInfo["EnglishName"]).'</td>';
//				$x .= '<td id="userlogin_'.$student_userid.'"  class="tabletext " valign="top">'.$student_userlogin.'</td>';
//				$x .= '<td class="tabletext " valign="top">'.$img.'</td>';
//				$x .= '<td class="tabletext " valign="top" align="center">'.$checkBox.'</td>';
//			$x .= '</tr>';

			$count++;
		}
		$x .= "</ul>";
		$x .= "</div>";
		
//		$x .= '</table>';

		return $x;
	}
	
	function getCWCPunishmentCount($format, $student_ary, $currentYearClass, $semester, $ap_records, $ap_records_rehabil, $isLast=false, $rows=array())
	{
		global $PATH_WRT_ROOT, $intranet_root, $intranet_session_language;
		global $Lang, $ldiscipline, $i_UserLogin, $no_record_msg, $chi_merit_ary, $eng_merit_ary;
		
		$x = '';
		
		$student_list = $student_ary[$currentYearClass];
		$first_term = $semester[0];
		$last_term = $semester[1];
		
		# Table Header
		if($format == 'web')
		{
			$x = "";
			$x .= "<table class='result_table' cellspacing='0' cellpadding='2' width='100%'>";
			$x .= "<thead>";
				$x .= "<tr>";
					$x .= "<th width='10%'>student_id</th>";
					$x .= "<th width='10%'>$i_UserLogin</th>";
					$x .= "<th width='8%'>{$eng_merit_ary[1]} {$chi_merit_ary[1]}</th>";
					$x .= "<th width='8%'>{$eng_merit_ary[2]} {$chi_merit_ary[2]}</th>";
					$x .= "<th width='8%'>{$eng_merit_ary[3]} {$chi_merit_ary[3]}</th>";
					$x .= "<th width='11%'>{$eng_merit_ary[0]} {$chi_merit_ary[0]}</th>";
					$x .= "<th width='8%'>{$eng_merit_ary[-1]} {$chi_merit_ary[-1]}</th>";
					$x .= "<th width='8%'>{$eng_merit_ary[-2]} {$chi_merit_ary[-2]}</th>";
					$x .= "<th width='8%'>{$eng_merit_ary[-3]} {$chi_merit_ary[-3]}</th>";
					$x .= "<th width='8%'>{$eng_merit_ary[-4]} {$chi_merit_ary[-4]}</th>";
					$x .= "<th width='10%'>Rehabilation 參與自新計劃</th>";
				$x .= "</tr>";
			$x .= "</thead>";
			$x .= "<tbody>";
		}
		else {
			$row = array("");
			$rows[] = $row;
			$row = array("student_id", "$i_UserLogin", "{$eng_merit_ary[1]} {$chi_merit_ary[1]}", "{$eng_merit_ary[2]} {$chi_merit_ary[2]}", "{$eng_merit_ary[3]} {$chi_merit_ary[3]}", 
							"{$eng_merit_ary[0]} {$chi_merit_ary[0]}", "{$eng_merit_ary[-1]} {$chi_merit_ary[-1]}", "{$eng_merit_ary[-2]} {$chi_merit_ary[-2]}", 
							"{$eng_merit_ary[-3]} {$chi_merit_ary[-3]}", "{$eng_merit_ary[-4]} {$chi_merit_ary[-4]}", "Rehabilation 參與自新計劃");
			$rows[] = $row;
		}
		
		// with students
		if(count($student_list)>0)
		{
			foreach((array)$student_list as $currentStudentID => $currentStudent){
				
				# Student Info
				$student_id = $currentStudent['UserID'];
				$user_login = $currentStudent['UserLogin'];
				$student_classname = $currentStudent['ClassName'];
				$student_classnumber = $currentStudent['ClassNumber'];
				$student_classnumber = $student_classnumber>=10 ? $student_classnumber : "0$student_classnumber";
				$student_classid = $student_classname.$student_classnumber;
				
				# Record Info
				// Demerit
				$warning = $ap_records[$student_id][0]['MeritRecordCount'];
				$warning2 = $ap_records[$student_id][-1]['MeritRecordCount'];
				$demerit = $ap_records[$student_id][-2]['MeritRecordCount'];
				$minor_demerit = $ap_records[$student_id][-3]['MeritRecordCount'];
				$major_demerit = $ap_records[$student_id][-4]['MeritRecordCount'];
				$warning = $warning? $warning : 0;
				$warning2 = $warning2? $warning2 : 0;
				$demerit = $demerit? $demerit : 0;
				$minor_demerit = $minor_demerit? $minor_demerit : 0;
				$major_demerit = $major_demerit? $major_demerit : 0;
				// Merit
				$merit = $ap_records[$student_id][1]['MeritRecordCount'];
				$minor_merit = $ap_records[$student_id][2]['MeritRecordCount'];
				$major_merit = $ap_records[$student_id][3]['MeritRecordCount'];
				$merit = $merit? $merit : 0;
				$minor_merit = $minor_merit? $minor_merit : 0;
				$major_merit = $major_merit? $major_merit : 0;
				// Self Record
				$rehabil = $ap_records_rehabil[$student_id]["RehabilCount"];
				$rehabil = $rehabil > 0? "Y" : "N";
				
				# Student Record
				if($format == 'web')
				{
					$x .= "<tr>";
						$x .= "<td>$student_classid</td>";
						$x .= "<td>$user_login</td>";
						$x .= "<td>$merit</td>";
						$x .= "<td>$minor_merit</td>";
						$x .= "<td>$major_merit</td>";
						$x .= "<td>$warning</td>";
						$x .= "<td>$warning2</td>";
						$x .= "<td>$demerit</td>";
						$x .= "<td>$minor_demerit</td>";
						$x .= "<td>$major_demerit</td>";
						$x .= "<td>$rehabil</td>";
					$x .= "</tr>";
				}
				else {
					$row = array("$student_classid", "$user_login", "$merit", "$minor_merit", "$major_merit", "$warning", "$warning2", "$demerit", "$minor_demerit", "$major_demerit", "$rehabil");
					$rows[] = $row;	
				}
			}
		}
		// without records
		else {
			if($format == 'web') {
				$x .= "<tr>";
					$x .= "<td colspan='11'>$no_record_msg</td>";
				$x .= "</tr>";
			}
			else {
				$row = array("$no_record_msg");
				$rows[] = $row;	
			}
		}	
			
		$x .= "</tbody>";
		$x .= "</table>";
		
		if($format == 'csv')
		{
			return $rows;
		} 
		else {
			return $x;
		}
	}
	
	function getCWCPunishmentRecord($format, $currentYearClass, $student_ary, $ap_records, $isFirst=false, $rows=array())
	{
		global $PATH_WRT_ROOT, $intranet_root, $intranet_session_language;
		global $Lang, $iDiscipline, $eDiscipline, $i_UserLogin, $no_record_msg, $i_Discipline_System_Discipline_Category, $i_Merit_Remark;
		global $ldiscipline, $merit_type_ary;
		
		$x = '';
		
		$current_class_name = $ldiscipline->getClassNameByClassID($currentYearClass);
		$student_list = $student_ary[$currentYearClass];
		
		if($format == 'web')
		{
			# Class Report Header
			$header = "<div class=\"container\">";
			
			$header .= "<table class='report_header' cellspacing='0' cellpadding='0' border='0' width='100%' align='center'>";
				$header .= "<tr><td align='left'>{$Lang['eDiscipline']['CWC']['StudentAPRecord']} - ".$ldiscipline->getClassNameByClassID($currentYearClass)."</td></tr>";
			$header .= "</table>";
			
			# Table Header
			$header .= "<table class='result_table' cellspacing='0' cellpadding='4' border='0' width='100%'>";
			$header .= "<thead>";
				$header .= "<tr>";
					$header .= "<th width='10%'>student_id</th>";
					$header .= "<th width='10%'>$i_UserLogin</th>";
					$header .= "<th class='result_table_header' width='17%'>$i_Discipline_System_Discipline_Category</th>";
					$header .= "<th class='result_table_header' width='25%'>{$eDiscipline['Setting_ItemName']}</th>";
					$header .= "<th class='result_table_header' width='8%'>{$iDiscipline['RecordType']}</th>";
					$header .= "<th class='result_table_header' width='25%'>{$i_Merit_Remark}</th>";
				$header .= "</tr>";
			$header .= "</thead>";
			
			$header .= "<tbody>";
			
			$x = $header;
		}
		else {
			$row = array("");
			$rows[] = $row;
			if(!$isFirst){
				$row = array("{$Lang['eDiscipline']['CWC']['StudentAPRecord']} - $current_class_name");
				$rows[] = $row;
			}
			$row = array("student_id", $i_UserLogin, $i_Discipline_System_Discipline_Category, $eDiscipline['Setting_ItemName'], $iDiscipline['RecordType'], $i_Merit_Remark);
			$rows[] = $row;
		}
		
		// Get class ap record count
		$total_ap_count = 0;
		foreach((array)$student_list as $currentStudentID => $currentStudent)
		{
			# Student record count
			$student_ap_count = $ap_records[$currentStudentID];
			$student_ap_count = count((array)$student_ap_count);
			if($student_ap_count > 0)
				$total_ap_count += $student_ap_count; 
		}
		
		// Display ap record details
		$ap_count = 0;
		foreach((array)$student_list as $currentStudentID => $currentStudent)
		{
			# Student Info
			$student_id = $currentStudent['UserID'];
			$student_userlogin = $currentStudent['UserLogin'];
			$student_classname = $currentStudent['ClassName'];
			$student_classnumber = $currentStudent['ClassNumber'];
			$student_classnumber = $student_classnumber>=10 ? $student_classnumber : "0$student_classnumber";
			$student_classid = $student_classname.$student_classnumber;
			
			# Record Info
			$current_ap_record = $ap_records[$currentStudentID];
			
			if(count((array)$current_ap_record) > 0)
			{
				foreach((array)$current_ap_record as $current_record)
				{
					# Record Info
					$record_cat = $current_record['CategoryName'];
					$record_item = $current_record['ItemName'];
					$record_merit_type = $current_record['ProfileMeritType'];
					$record_remark = $current_record['Remark'];
					
					// Convert
					$record_merit_type = $merit_type_ary[$record_merit_type];
					$record_merit_type = $record_merit_type? $record_merit_type : $Lang['General']['EmptySymbol'];
					
					# Student Record
					if($format == 'web')
					{
						$x .= "<tr>";
							$x .= "<td>$student_classid</td>";
							$x .= "<td>$student_userlogin</td>";
							$x .= "<td>$record_cat</td>";
							$x .= "<td>$record_item</td>";
							$x .= "<td>$record_merit_type</td>";
							$x .= "<td>$record_remark</td>";
						$x .= "</tr>";

						$ap_count++;
						
						// add line break
						if(($ap_count % 41 == 0) && $total_ap_count > $ap_count)
						{
							// Report Footer
							$x .= "</tbody>";
							$x .= "</table>";
							$x .= "</div>";
							$x .= "<div style='page-break-after:always;'>&nbsp;</div>";
							
							// Report and Table Header
							$x .= $header;
						}
					}
					else
					{
						$row = array("$student_classid", "$student_userlogin", "$record_cat", "$record_item", "$record_merit_type", "$record_remark");
						$rows[] = $row;	
					}
				}
			}
		}
		
		# No Records
		if($total_ap_count==0){
			if($format == 'web') {
				$x .= "<tr>";
					$x .= "<td colspan='6'>$no_record_msg</td>";
				$x .= "</tr>";
			}
			else {
				$row = array("$no_record_msg");
				$rows[] = $row;	
			}
		}
		
		$x .= "</tbody>";
		$x .= "</table>";
		$x .= "</div>";
		
		if($format == 'csv') {
			return $rows;
		}
		else {
			return $x;
		}
	}
	# [Po Leung Kuk C W Chu College] End
	
	# [HKUGA College] Start
	function getPresetReasonTableHtml($contentAry){
		global $PATH_WRT_ROOT, $Lang;
		
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$linterface = new interface_html();

		$x = "";
		$x .= "<table id='DataTable' class='common_table_list_v30'>\n";
			$x .= "<thead>\n";
				$x .= "<tr>\n";
					$x .= "<th style='width:3%;'>#</th>\n";
					$x .= "<th style='width:94%;'>".$Lang['eDiscipline']['PresetWaiveReason']['Reason']."</th>\n";
					$x .= "<th style='width:3%;'><input type='checkbox' onclick='(this.checked)?setChecked(1,this.form,\"ReasonIDAry[]\"):setChecked(0,this.form,\"ReasonIDAry[]\")' name='checkmaster'></th>\n";
				$x .= "<tr>\n";
			$x .= "</thead>\n";
			
			$x .= "<tbody>\n";
		
			$numOfReason = count($contentAry);
			if ($numOfReason == 0) {
				$x .= "<tr><td colspan='100%' style='text-align:center;'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>\n";
			}
			else {
				for ($i=0; $i<$numOfReason; $i++) {
					$_reason_id = $contentAry[$i]['ReasonID'];
					$_reason_text = $contentAry[$i]['ReasonText'];
					
					$x .= "<tr id='tr_$_reason_id'>\n";
						$x .= "<td><span class='rowNumSpan'>".($i+1)."</td>\n";
						$x .= "<td>".$_reason_text."</td>\n";
						$x .= "<td>\n";
							$x .= "<input type='checkbox' class='ScaleChk' name='ReasonIDAry[]' value='".$_reason_id."'>\n";
						$x .= "</td>\n";
					$x .= "</tr>\n";
				}
			}
			$x .= "</tbody>\n";
		$x .= "</table>\n";
		
		return $x;
	}
	# [HKUGA College] End
	
	# [Jockey Club Government Secondary School] Start 
	function getMonthlySummary($dataAry){
		global $PATH_WRT_ROOT, $ldiscipline, $Lang;
		
		# Get $_POST data
		$selectedYearID = $dataAry["selectYear"];
		$selectedYear = $dataAry["Year"];
		$selectedMonth = $dataAry["Month"];
		if($selectedMonth<10)
			$selectedMonth = "0$selectedMonth";
		//$selectedType = $dataAry["level"];
		//$Targets = $dataAry["rankTargetDetail"];
		
		# Get export data
		// Form and Class Mapping
		$sql = "SELECT
					y.YearName, yc.ClassTitleB5 as ClassName
				FROM YEAR as y
					Inner Join YEAR_CLASS as yc ON (yc.YearID = y.YearID AND yc.AcademicYearID = '$selectedYearID')
				WHERE yc.AcademicYearID = '$selectedYearID'";
		$FormClassMapping = $ldiscipline->returnArray($sql);
		$FormClassMapping = BuildMultiKeyAssoc($FormClassMapping, array("YearName", "ClassName"));		
		
		// Punishment Category
		$catAry = array();
		$catAry[1] = "遲到";
		$catAry[2] = "儀容";
		$catAry[3] = "行為";
		
		// Get Punishment records
		$sql = "Select
					mr.RecordID,
					mr.StudentID,
					y.YearName,
					yc.ClassTitleB5 as ClassName,
					mi.CatID,
					mr.ProfileMeritType,
					mr.ProfileMeritCount
				From DISCIPLINE_MERIT_RECORD as mr
					Inner Join DISCIPLINE_MERIT_ITEM mi on (mi.ItemID = mr.ItemID)
					Inner Join YEAR_CLASS_USER as ycu on (ycu.UserID = mr.StudentID)
					Inner Join YEAR_CLASS as yc on (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectedYearID') 
					Inner Join YEAR as y on (y.YearID = yc.YearID) 
				Where mr.AcademicYearID = '$selectedYearID' AND SUBSTR(mr.RecordDate, 6, 2) = '$selectedMonth' AND
						mr.MeritType = -1 AND mi.CatID IN (1, 2, 3) AND
						mr.RecordStatus = ".DISCIPLINE_STATUS_APPROVED;
		$p_records = $ldiscipline->returnArray($sql);
		
		// Format Punishment records
		$SchoolRecordAry = BuildMultiKeyAssoc($p_records, array("CatID", "ProfileMeritType", "RecordID"));
		$SchoolStudentAry = BuildMultiKeyAssoc($p_records, array("CatID", "StudentID"));
		$FormRecordAry = BuildMultiKeyAssoc($p_records, array("YearName", "CatID", "ProfileMeritType", "RecordID"));
		$FormStudentAry = BuildMultiKeyAssoc($p_records, array("YearName", "CatID", "StudentID"));
		$ClassRecordAry = BuildMultiKeyAssoc($p_records, array("YearName", "ClassName", "CatID", "ProfileMeritType", "RecordID"));
		$ClassStudentAry = BuildMultiKeyAssoc($p_records, array("YearName", "ClassName", "CatID", "StudentID"));

		// Initial
		$returnAry = array();
		$formTotalAry = array();
		$formStudentList = array();
		$schoolTotalAry = array(0, 0, 0, 0, 0, 0);
		$schoolStudentList = array();
		
		# Whole School
		$returnAry[] = array("");
		$returnAry[] = array("全校總覽");
		$returnAry[] = array("");
		$returnAry[] = array("", "人數", "次數", "警告", "缺點", "小過", "大過");
		
		// Whole School - Category count
		foreach($catAry as $CategoryID => $CategoryName){
			// Get records count
			// Student Number
			$currentStudent = intval(count($SchoolStudentAry[$CategoryID]));
			// Record Number
			$currentRecords = $SchoolRecordAry[$CategoryID];
			$currentWarning = intval(count($currentRecords[0]));
			$currentDemerit = intval(count($currentRecords[-1]));
			$currentMinor = intval(count($currentRecords[-2]));
			$currentMajor = intval(count($currentRecords[-3]));
			$currentTotal = $currentWarning + $currentDemerit + $currentMinor + $currentMajor;
			// Merit Number
			$currentWarning = $ldiscipline->returnMeritStatInMonthlySummary($currentRecords[0]);
			$currentDemerit = $ldiscipline->returnMeritStatInMonthlySummary($currentRecords[-1]);
			$currentMinor = $ldiscipline->returnMeritStatInMonthlySummary($currentRecords[-2]);
			$currentMajor = $ldiscipline->returnMeritStatInMonthlySummary($currentRecords[-3]);
			
			// Add count to $schoolTotalAry
			//$schoolTotalAry[0] += $currentStudent;
			$schoolTotalAry[1] += $currentTotal;
			$schoolTotalAry[2] += $currentWarning;
			$schoolTotalAry[3] += $currentDemerit;
			$schoolTotalAry[4] += $currentMinor;
			$schoolTotalAry[5] += $currentMajor;
			
			// Add StudentID to $schoolStudentList
			$schoolStudentList = $schoolStudentList + array_keys((array)$SchoolStudentAry[$CategoryID]);
			
			$returnAry[] = array("$CategoryName", "$currentStudent", "$currentTotal", "$currentWarning", "$currentDemerit", "$currentMinor", "$currentMajor");
		}

		// Whole School - Total count
		$returnAry[] = array("", "".count(array_unique((array)$schoolStudentList))."", "".$schoolTotalAry[1]."", "".$schoolTotalAry[2]."", "".$schoolTotalAry[3]."", "".$schoolTotalAry[4]."", "".$schoolTotalAry[5]."");
		$returnAry[] = array("", "", "", "", "", "", "", "");
		$returnAry[] = array("", "", "", "", "", "", "", "");
		
		unset($SchoolRecordAry);
		unset($SchoolStudentAry);
		unset($schoolStudentList);
		unset($schoolTotalAry);
		
		# Forms
		$returnAry[] = array("全校各級別");
		$returnAry[] = array("", "", "", "", "", "", "", "");
		$returnAry[] = array("級別", "", "人數", "次數", "警告", "缺點", "小過", "大過");
		
		// Loop all forms
		foreach($FormClassMapping as $formName => $formClass){
			$formNameDisplay = $formName;
			$formTotalAry[$formName] = array(0, 0, 0, 0, 0, 0);
			$formStudentList[$formName] = array();
			
			// Forms - Category count
			foreach($catAry as $CategoryID => $CategoryName){
				// Get records count
				// Student Number
				$currentStudent = intval(count($FormStudentAry[$formName][$CategoryID]));
				// Record Number
				$currentRecords = $FormRecordAry[$formName][$CategoryID];
				$currentWarning = intval(count($currentRecords[0]));
				$currentDemerit = intval(count($currentRecords[-1]));
				$currentMinor = intval(count($currentRecords[-2]));
				$currentMajor = intval(count($currentRecords[-3]));
				$currentTotal = $currentWarning + $currentDemerit + $currentMinor + $currentMajor;
				// Merit Number
				$currentWarning = $ldiscipline->returnMeritStatInMonthlySummary($currentRecords[0]);
				$currentDemerit = $ldiscipline->returnMeritStatInMonthlySummary($currentRecords[-1]);
				$currentMinor = $ldiscipline->returnMeritStatInMonthlySummary($currentRecords[-2]);
				$currentMajor = $ldiscipline->returnMeritStatInMonthlySummary($currentRecords[-3]);

				// Add count to $formTotalAry
				//$formTotalAry[$formName][0] += $currentStudent;
				$formTotalAry[$formName][1] += $currentTotal;
				$formTotalAry[$formName][2] += $currentWarning;
				$formTotalAry[$formName][3] += $currentDemerit;
				$formTotalAry[$formName][4] += $currentMinor;
				$formTotalAry[$formName][5] += $currentMajor;
			
				// Add StudentID to $formStudentList
				$formStudentList[$formName] = $formStudentList[$formName] + array_keys((array)$FormStudentAry[$formName][$CategoryID]);
			
				$returnAry[] = array("$formNameDisplay", "$CategoryName", "$currentStudent", "$currentTotal", "$currentWarning", "$currentDemerit", "$currentMinor", "$currentMajor");
				$formNameDisplay = "";
			}
		
			// Forms - Total count
			$currentFormTotalAry = $formTotalAry[$formName];
			$returnAry[] = array("", "", "".count(array_unique((array)$formStudentList[$formName]))."", "".$currentFormTotalAry[1]."", "".$currentFormTotalAry[2]."", "".$currentFormTotalAry[3]."", "".$currentFormTotalAry[4]."", "".$currentFormTotalAry[5]."");
		}
		
		unset($FormRecordAry);
		unset($FormStudentAry);
		unset($currentFormTotalAry);
		
		# Classes
		// Loop all forms
		foreach($FormClassMapping as $formName => $formclass){
			$returnAry[] = array("", "", "", "", "", "", "", "");
			$returnAry[] = array("", "", "", "", "", "", "", "");
		
			$returnAry[] = array($formName." 級");
			$returnAry[] = array("", "", "", "", "", "", "", "");
			$returnAry[] = array("班別", "", "人數", "次數", "警告", "缺點", "小過", "大過");
			
			// Loop all classes
			foreach($formclass as $className => $class){
				$ClassDisplayName = $className;
				$ClassAry = array(0, 0, 0, 0, 0);
				$classStudentList = array();
				
				// Classes - Category count
				foreach($catAry as $CategoryID => $CategoryName){
					// Get records count
					// Student Number
					$currentStudent = intval(count($ClassStudentAry[$formName][$className][$CategoryID]));
					// Record Number
					$currentRecords = $ClassRecordAry[$formName][$className][$CategoryID];
					$currentWarning = intval(count($currentRecords[0]));
					$currentDemerit = intval(count($currentRecords[-1]));
					$currentMinor = intval(count($currentRecords[-2]));
					$currentMajor = intval(count($currentRecords[-3]));
					$currentTotal = $currentWarning + $currentDemerit + $currentMinor + $currentMajor;
					// Merit Number
					$currentWarning = $ldiscipline->returnMeritStatInMonthlySummary($currentRecords[0]);
					$currentDemerit = $ldiscipline->returnMeritStatInMonthlySummary($currentRecords[-1]);
					$currentMinor = $ldiscipline->returnMeritStatInMonthlySummary($currentRecords[-2]);
					$currentMajor = $ldiscipline->returnMeritStatInMonthlySummary($currentRecords[-3]);
			
					// Add count to $ClassAry
					//$ClassAry[0] += $currentStudent;
					$ClassAry[1] += $currentTotal;
					$ClassAry[2] += $currentWarning;
					$ClassAry[3] += $currentDemerit;
					$ClassAry[4] += $currentMinor;
					$ClassAry[5] += $currentMajor;
			
					// Add StudentID to $formStudentList
					$classStudentList = $classStudentList + array_keys((array)$ClassStudentAry[$formName][$className][$CategoryID]);
					
					$returnAry[] = array("$ClassDisplayName", "$CategoryName", "$currentStudent", "$currentTotal", "$currentWarning", "$currentDemerit", "$currentMinor", "$currentMajor");
					$ClassDisplayName = "";
				}
		
				// Clases - Total count
				$returnAry[] = array("", "", "".count(array_unique((array)$classStudentList))."", "".$ClassAry[1]."", "".$ClassAry[2]."", "".$ClassAry[3]."", "".$ClassAry[4]."", "".$ClassAry[5]."");
			}
		
			// Forms - Total count
			$currentFormTotalAry = $formTotalAry[$formName];
			$returnAry[] = array("", "", "".count(array_unique((array)$formStudentList[$formName]))."", "".$currentFormTotalAry[1]."", "".$currentFormTotalAry[2]."", "".$currentFormTotalAry[3]."", "".$currentFormTotalAry[4]."", "".$currentFormTotalAry[5]."");
		}
		
		unset($ClassRecordAry);
		unset($ClassStudentAry);
		unset($classStudentList);
		unset($formTotalAry);
		unset($formStudentList);
		unset($currentFormTotalAry);
		
		return $returnAry;
	}
	# [Jockey Club Government Secondary School] End
	
	// [NT Heung Yee Kuk Tai Po District Secondary School] - Start
	function getHYKClassSummaryReport($dataAry)
	{
		global $PATH_WRT_ROOT, $intranet_root, $sys_custom, $ldiscipline;
		global $intranet_session_language, $Lang, $no_record_msg;
		
		# POST data
		$format = $_POST['format'];
		$yearID = $_POST["YearID"];
		$termID = $_POST["selectSemester"];
		$startDate = $_POST["StartDate"];
		$endDate = $_POST["EndDate"];
		$class = $_POST["rankTargetDetail"];
		$isShowRecordStudentOnly = $_POST["ShowRecordStudentOnly"];
		
		// Special Handling for Whole Year
		$isRangeCrossTerm = false;
		$isWholeYear = $termID == "0";
		if($isWholeYear)
		{
			$startTermInfo = getAcademicYearInfoAndTermInfoByDate($startDate);
			$endTermInfo = getAcademicYearInfoAndTermInfoByDate($endDate);
			$startTermID = $startTermInfo[2];
			$endTermID = $endTermInfo[2];
			
			$isRangeCrossTerm = $startTermID != $endTermID;
			$termID = $isRangeCrossTerm ? "" : $endTermID;
		}
		
		# Year - Date Range
		$SQL_startdate = getStartDateOfAcademicYear($yearID, $termID);
		$SQL_enddate = getEndDateOfAcademicYear($yearID, $termID);
		$yearFromDate = date("Y-m-d", strtotime($SQL_startdate));
		$yearToDate = date("Y-m-d", strtotime($SQL_enddate));
		$targetTermStartDate = $yearFromDate;
		if($isRangeCrossTerm) {
			$targetTermStartDate = getStartDateOfAcademicYear($yearID, $endTermID);
			$targetTermStartDate = date("Y-m-d", strtotime($targetTermStartDate));
		}
		
		# Target End Date
		$endDateField = ($yearToDate > $endDate)? $endDate : $yearToDate;
		
		# Target Classes
		$classIdCond = " yc.YearClassID IN ('".implode("','",(array)$class)."') ";
		
		# Target Term
		$termCond = $termID ? " AND r.YearTermID = '".$termID."' " : "";
		
		# Year and Semester Name
		$sql = "SELECT
						ay.YearNameB5, ayt.YearTermNameB5
				FROM
						ACADEMIC_YEAR_TERM as ayt
						INNER JOIN ACADEMIC_YEAR as ay On (ayt.AcademicYearID = ay.AcademicYearID)
				WHERE
						ayt.AcademicYearID = '$yearID' ";
		$sql .= $termID ? " AND ayt.YearTermID = '".$termID."' " : "";
		$year_term_name = $ldiscipline->returnArray($sql);
		$year_term_name = $year_term_name[0];
		
		# Class Name
		$sql = "SELECT
					YearClassID,
					ClassTitleB5 as ClassName
				FROM
					YEAR_CLASS
				WHERE
					AcademicYearID = '".$yearID."' AND YearClassID IN ('".implode("','",(array)$class)."')";
		$class_name = $ldiscipline->returnArray($sql);
		$class_name = BuildMultiKeyAssoc((array)$class_name, array("YearClassID"));
		
		# Target Students
		$sql = "SELECT
					iu.UserID,
					yc.YearClassID,
					yc.ClassTitleEN as ClassName,
					ycu.ClassNumber,
					CONCAT(
						IF(yc.ClassTitleEN IS NULL OR yc.ClassTitleEN='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".'<span class="tabletextrequire">'."^</span>', ''),
						iu.ChineseName
					) as StudentName
				FROM INTRANET_USER as iu
					LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID
					LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$yearID')
					LEFT JOIN YEAR as y ON y.YearID = yc.YearID
				WHERE
					yc.AcademicYearID = '".$yearID."' AND $classIdCond
				GROUP BY
					iu.UserID
				ORDER BY
					y.Sequence, yc.Sequence, ycu.ClassNumber";
		$students = $ldiscipline->returnResultSet($sql);
		$student_ids = Get_Array_By_Key($students, "UserID");
		$student_ary = BuildMultiKeyAssoc((array)$students, array("YearClassID", "UserID"));
		$student_count = count($students);
		
		# Misconduct - Approved
		$sql = "SELECT
					r.RecordID,
					r.StudentID,
					DATE_FORMAT(r.RecordDate,'%Y-%m-%d') as RecordDate,
					r.YearTermID,
					r.RecordType,
					cat.CategoryID,
					cat.Name,
					IF(r.GMConductScoreChange IS NULL, 0, r.GMConductScoreChange) as GMConductScoreChange,
					IF(r.RecordDate >= '$startDate 00:00:00' AND r.RecordDate <= '$endDate 23:59:59', 1, 0) as InDateRange,
					IF(r.RecordDate >= '$targetTermStartDate 00:00:00' AND r.RecordDate <= '$endDate 23:59:59', 1, 0) as TermDateRange
				FROM
					DISCIPLINE_ACCU_RECORD as r
				INNER JOIN
					DISCIPLINE_ACCU_CATEGORY as cat ON cat.CategoryID = r.CategoryID
				WHERE
					r.AcademicYearID = '$yearID' ".$termCond." AND
					r.RecordDate >= '$yearFromDate 00:00:00' AND r.RecordDate <= '$endDateField 23:59:59' AND
					r.StudentID IN ('".implode("','", (array)$student_ids)."') AND
					r.RecordType = '".MISCONDUCT."' AND r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."'";
		$gm_records = $ldiscipline->returnResultSet($sql);
		$gm_records = BuildMultiKeyAssoc((array)$gm_records, array("StudentID", "RecordID"));
		
		# Awards / Punishment - Approved and Released
		// TEMP - not consider "ProfileMeritType = 0 > Conduct Deduct"
		$sql = "SELECT
					r.RecordID,
					r.StudentID,
					DATE_FORMAT(r.RecordDate,'%Y-%m-%d') as RecordDate,
					r.YearTermID,
					r.ProfileMeritType,
					IF(r.ProfileMeritType < 0 AND r.ProfileMeritType > -999, 1, 0) as ConductRelated,
					r.ProfileMeritCount,
					m.ItemCode,
					m.ItemName,
					IF(r.ConductScoreChange IS NULL, 0, r.ConductScoreChange) as ConductScoreChange,
					IF(r.RecordDate >= '$startDate' AND r.RecordDate <= '$endDate', 1, 0) as InDateRange,
					IF(r.RecordDate >= '$targetTermStartDate' AND r.RecordDate <= '$endDate', 1, 0) as TermDateRange
				FROM
					DISCIPLINE_MERIT_RECORD as r
				INNER JOIN
					DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID
				WHERE
					r.AcademicYearID = '$yearID' ".$termCond." AND
					r.RecordDate >= '$yearFromDate' AND r.RecordDate <= '$endDateField' AND
					r.StudentID IN ('".implode("','", (array)$student_ids)."') AND
					r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."'";
		$ap_records = $ldiscipline->returnResultSet($sql);
		$ap_records = BuildMultiKeyAssoc((array)$ap_records, array("StudentID", "ConductRelated", "RecordID"));
		
		if($format != 'word')
		{
    		$x = "	<table width='100%' align='center' class='print_hide' border='0'>
    					<tr><td align='right'>".$this->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print")."</td></tr>
    				</table>
    				<div>";
		}
		
		// Init
		//$withRecordStudentAry = array();
		
		// loop classes
		$this_count = 0;
		$class_count = count((array)$class);
		foreach((array)$class as $current_classid)
		{
			$x .= " <div class='main_container'>
					<table class='report_header' cellspacing='0' cellpadding='5' border='0' width='100%'>
						<tr><td>班別 : ".$class_name[$current_classid]["ClassName"]."</td></tr>";
			if($termID) {
				$x .= "	<tr><td>學期 : ".$year_term_name[1]."　　　　　學年: ".$year_term_name[0]."</td></tr>
						<tr><td>學期開始日期: ".$yearFromDate."</td></tr>";
			}
			else {
				$x .= "	<tr><td>學年: ".$year_term_name[0]."</td></tr>
						<tr><td>學年開始日期: ".$yearFromDate."</td></tr>";
			}
			$x .= "		<tr><td>開始日期 : ".$startDate."　　　　　　　　終止日期: ".$endDate."</td></tr>
						<tr><td>&nbsp;</td></tr>
						<tr><td>違規事項</td></tr>
					</table>
					<table class='result_table' cellspacing='0' cellpadding='0' width='100%' border='1'>
						<tr>
							<td width='7%'>學號</td>
							<td width='26%'>學生姓名</td>
							<td width='7%'>優點</td>
							<td width='7%'>大功</td>
							<td width='7%'>小功</td>
							<td width='7%'>警告</td>
							<td width='7%'>扣分</td>
							<td width='7%'>缺點</td>
							<td width='7%'>小過</td>
							<td width='7%'>大過</td>
							<td width='11%'>總分</td>
						</tr>";
			
			// loop students
			$student_list = $student_ary[$current_classid];
			if(count($student_list) > 0)
			{
				$classHasRecordStudent = false;
				foreach((array)$student_list as $currentStudentID => $currentStudent)
				{
					$totalDeduct = 0;
					$thisRangeDeduct = 0;
					$thisTargetTermDeduct = 0;
					
					# Student Info
					$student_id = $currentStudent['UserID'];
					$student_name = $currentStudent['StudentName'];
					$class_number = $currentStudent['ClassNumber'];
					
					# AP records not related to Conduct Mark
					$thisStudentData = array();
					$this_ap_non_conduct_score = $ap_records[$currentStudentID][0];
					foreach((array)$this_ap_non_conduct_score as $this_ap)
					{
						if($this_ap["InDateRange"] == 1 && $this_ap["ProfileMeritType"] != -999 && $this_ap["ProfileMeritCount"] > 0)
						{
							$thisStudentData[$this_ap["ProfileMeritType"]] += $this_ap["ProfileMeritCount"];
						}
					}
					
					# AP records related to Conduct Mark
					$this_ap_conduct_score = $ap_records[$currentStudentID][1];
					foreach((array)$this_ap_conduct_score as $this_ap)
					{
						if($this_ap["ConductScoreChange"] < 0)
						{
							$totalDeduct += abs($this_ap["ConductScoreChange"]);
							if($this_ap["InDateRange"] == 1)
							{
								$thisRangeDeduct += abs($this_ap["ConductScoreChange"]);
							}
							if($isRangeCrossTerm && $this_ap["TermDateRange"] == 1)
							{
								$thisTargetTermDeduct += abs($this_ap["ConductScoreChange"]);
							}
						}
					}
					
					# GM records related to Conduct Mark
					$this_gm_conduct_score = $gm_records[$currentStudentID];
					foreach((array)$this_gm_conduct_score as $this_gm)
					{
						if($this_gm["GMConductScoreChange"] < 0)
						{
							$totalDeduct += abs($this_gm["GMConductScoreChange"]);
							if($this_gm["InDateRange"] == 1)
							{
								$thisRangeDeduct += abs($this_gm["GMConductScoreChange"]);
							}
							if($isRangeCrossTerm && $this_gm["TermDateRange"] == 1)
							{
								$thisTargetTermDeduct += abs($this_gm["GMConductScoreChange"]);
							}
						}
					}
					
					# Calculate Demerit Number using Conduct Mark
					$conductMarkData = array();
					if($totalDeduct > 0)
					{
						// Special Case for Whole Year  ( ( 1st Total - Previous ) + ( 2nd Total ) )
						if($isRangeCrossTerm)
						{
							$beforeRangeDeduct = $totalDeduct - $thisRangeDeduct;
							$firstTermTotalDeduct = $totalDeduct - $thisTargetTermDeduct;
							$firstTermRangeDeduct = (floor($firstTermTotalDeduct / 8) - floor($beforeRangeDeduct / 8)) * 8;
							$thisCrossRangeDeduct = $firstTermRangeDeduct + $thisTargetTermDeduct;
							
							$conductMarkData["ConductMark"] = $thisTargetTermDeduct > 0 ? ($thisTargetTermDeduct % 8) : 0;
							$conductMarkData[-1] = floor($thisCrossRangeDeduct / 8) % 3;
							$conductMarkData[-1] = $conductMarkData[-1] >= 0 ? $conductMarkData[-1] : 0;
							$conductMarkData[-2] = floor($thisCrossRangeDeduct / 24) % 3;
							$conductMarkData[-2] = $conductMarkData[-2] >= 0 ? $conductMarkData[-2] : 0;
							$conductMarkData[-3] = floor($thisCrossRangeDeduct / 72) % 3;
							$conductMarkData[-3] = $conductMarkData[-3] >= 0 ? $conductMarkData[-3] : 0;
							
							$totalDeduct = $thisTargetTermDeduct;
						}
						// Normal Case ( Total - Previous )
						else
						{
							$beforeRangeDeduct = $totalDeduct - $thisRangeDeduct;
							$conductMarkData["ConductMark"] = $thisRangeDeduct > 0? ($totalDeduct % 8) : 0;
							$conductMarkData[-1] = (floor($totalDeduct / 8) % 3) - (floor($beforeRangeDeduct / 8) % 3);
							$conductMarkData[-1] = $conductMarkData[-1] >= 0 ? $conductMarkData[-1] : 0;
							$conductMarkData[-2] = (floor($totalDeduct / 24) % 3) - (floor($beforeRangeDeduct / 24) % 3);
							$conductMarkData[-2] = $conductMarkData[-2] >= 0 ? $conductMarkData[-2] : 0;
							$conductMarkData[-3] = floor($totalDeduct / 72) - floor($beforeRangeDeduct / 72);
							$conductMarkData[-3] = $conductMarkData[-3] >= 0 ? $conductMarkData[-3] : 0;
						}
					}
					
					# Calculate related Conduct Grade
					$conductMarkData["ConductGrade"] = $sys_custom['eDiscipline']['HYKReportConductGradeSettings_default'];
					foreach((array)$sys_custom['eDiscipline']['HYKReportConductGradeSettings'] as $thisConductGrade => $thisGradeMaxConduct) {
						if($totalDeduct <= $thisGradeMaxConduct) {
							$conductMarkData["ConductGrade"] = $thisConductGrade;
							break;
						}
					}
					
					# Have Record
					$withDemeritRecord = $conductMarkData["ConductMark"] || $conductMarkData[-1] || $conductMarkData[-2] || $conductMarkData[-3] || ($isRangeCrossTerm && $thisRangeDeduct > 0);
					if($isShowRecordStudentOnly && !$withDemeritRecord) {
						continue;
					}
					$classHasRecordStudent = true;
					
					# Student Record
					$x .= "<tr>";
						$x .= "<td>".$class_number."</td>";
						$x .= "<td>".($withDemeritRecord && !$isShowRecordStudentOnly? "<b>".$student_name."</b>" : $student_name)."</td>";
						$x .= "<td>".($thisStudentData[1]? $thisStudentData[1] : 0)."</td>";
						$x .= "<td>".($thisStudentData[2]? $thisStudentData[2] : 0)."</td>";
						$x .= "<td>".($thisStudentData[3]? $thisStudentData[3] : 0)."</td>";
						$x .= "<td>".($thisStudentData[0]? $thisStudentData[0] : 0)."</td>";
						$x .= "<td>".($conductMarkData["ConductMark"]? "<b>".$conductMarkData["ConductMark"]."</b>" : 0)."</td>";
						$x .= "<td>".($conductMarkData[-1]? "<b>".$conductMarkData[-1]."</b>" : 0)."</td>";
						$x .= "<td>".($conductMarkData[-2]? "<b>".$conductMarkData[-2]."</b>" : 0)."</td>";
						$x .= "<td>".($conductMarkData[-3]? "<b>".$conductMarkData[-3]."</b>" : 0)."</td>";
						$x .= "<td>".$conductMarkData["ConductGrade"]."</td>";
					$x .= "</tr>";
					
//					# Get Student with records
//					if($isShowRecordStudentOnly && ($conductMarkData[-1] || $conductMarkData[-2] || $conductMarkData[-3])) {
//						$withRecordStudentAry[] = $student_name;
//					}
				}
				if(!$classHasRecordStudent) {
					$x .= "<tr>";
						$x .= "<td style='border-left:0px solid black' colspan='30'>$no_record_msg</td>";
					$x .= "</tr>";
				}
			}
			else {
				$x .= "<tr>";
					$x .= "<td style='border-left:0px solid black' colspan='30'>$no_record_msg</td>";
				$x .= "</tr>";
			}
			$x .= "</table>";
			$x .= "</div>";
			
			// Add page break
			if($this_count != ($class_count-1))
			{
			    if($format == 'word') {
			        $x .= '&nbsp; <br style="page-break-before: always"> ';
			    } 
			    else {
			        $x .= "<div style=\"page-break-after:always\">&nbsp;</div>";
			    }
			}
			$this_count++;
		}
		
//		# Display Student List (with Punishment only)
//		if($isShowRecordStudentOnly)
//		{
//			$x = "	<div>
//						<div class='main_container'>
//							<table class='student_list_table' cellspacing='0' cellpadding='0' width='100%' border='0'>";
//			if(count($withRecordStudentAry)) {
//				foreach((array)$withRecordStudentAry as $thisStudentName) {
//					$x .= "		<tr><td>".$thisStudentName."</td></tr>";
//				}
//			}
//			else {
//				$x .= "		<tr><td style='text-align: center'>".$no_record_msg."</td></tr>";
//			}
//			$x .= "			</table>
//						</div>";
//		}
		
		$x .= "</div>";
		return $x;
	}
	
	function getHYKMeritStatisticsReport($dataAry)
	{
		global $PATH_WRT_ROOT, $intranet_root, $ldiscipline;
		global $intranet_session_language, $Lang, $no_record_msg;
		
		# POST data
		$yearID = $_POST['selectYear'];
		$termID = $_POST['selectSemester'];
		$startDate = $_POST['textFromDate'];
		$endDate = $_POST['textToDate'];
		$targetID = $_POST['rankTargetDetail'];
		$periodType = $_POST['radioPeriod'];
		$targetType = $_POST['rankTarget'];
		
		# Year - Date Range
		$termCond = "";
		$termField = " r.YearTermID ";
		$relatedSem = array();
		$allSemList = getSemesters($yearID, 0);
		$termDetails = BuildMultiKeyAssoc($allSemList, "YearTermID");
		// Type: Date Range
		if($periodType=="DATE")
		{
			// Start Term
			$startTerm = getAcademicYearAndYearTermByDate($startDate);
			if($startTerm["AcademicYearID"]==$yearID)
			{
				$startTermID = $startTerm["YearTermID"];
				$relatedSem[] = $startTermID;
				$yearFromDate = date("Y-m-d", strtotime($termDetails[$startTermID]["TermStart"])); // for calculating merit before start date => remove from display
			}
			else
			{
				$startTermID = $allSemList[0]["YearTermID"];
				$yearFromDate = date("Y-m-d", strtotime($allSemList[0]["TermStart"]));
				$relatedSem[] = $startTermID;
			}
			$termField = " '$startTermID' as YearTermID ";
			
			// End Term
			$yearToDate = $endDate;
			
			/*
			$endTerm = getAcademicYearAndYearTermByDate($yearToDate);
			if($endTerm["AcademicYearID"]==$yearID)
			{
				$relatedSem[] = $endTerm["YearTermID"];
			}
			else
			{
				$targetSem = end($allSemList);
				$relatedSem[] = $targetSem["YearTermID"];
			}
			$relatedSem = array_filter(array_unique($relatedSem));
			*/
		}
		// Type: Term
		else
		{
			$singleSemester = ($periodType=="YEAR" && $termID > 0);
			if($singleSemester)
			{
				$relatedSem[] = $termID;
				//$termCond = " r.YearTermID = '$termID' AND ";
			}
			else
			{
				$relatedSem = Get_Array_By_Key($allSemList, "YearTermID");
			}
			
			$SQL_startdate = getStartDateOfAcademicYear($yearID, ($singleSemester? $termID : ""));
			$SQL_enddate = getEndDateOfAcademicYear($yearID, ($singleSemester? $termID : ""));
			$yearFromDate = date("Y-m-d", strtotime($SQL_startdate));
			$yearToDate = date("Y-m-d", strtotime($SQL_enddate));
		}
		
		# Report Target
		// Class
		if($targetType == "class")
		{
			$targetIdCond = " yc.YearClassID IN ('".implode("','", (array)$targetID)."') ";
			$targetField = "YearClassID";
			
			$sql = "SELECT 
						YearClassID,
						ClassTitleB5 as Name
					FROM 
						YEAR_CLASS
					WHERE 
						AcademicYearID = '".$yearID."' AND YearClassID IN ('".implode("','",(array)$targetID)."')";
			$target_name = $ldiscipline->returnArray($sql);
			$target_name = BuildMultiKeyAssoc((array)$target_name, array("YearClassID"));
		}
		// Level
		else
		{
			$targetIdCond = " y.YearID IN ('".implode("','", (array)$targetID)."') ";
			$targetField = "YearID";
			
			$sql = "SELECT 
						YearID,
						YearName as Name
					FROM 
						YEAR
					WHERE 
						YearID IN ('".implode("','", (array)$targetID)."')";
			$target_name = $ldiscipline->returnArray($sql);
			$target_name = BuildMultiKeyAssoc((array)$target_name, array("YearID"));
		}
		
		# Target Students
		$sql = "SELECT 
					iu.UserID,
					y.YearID,
					yc.YearClassID,
					yc.ClassTitleEN as ClassName,
					ycu.ClassNumber,
					CONCAT(
						IF(yc.ClassTitleEN IS NULL OR yc.ClassTitleEN='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".'<span class="tabletextrequire">'."^</span>', ''),
						iu.ChineseName
					) as StudentName
				FROM INTRANET_USER as iu 
					LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID
					LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$yearID')
					LEFT JOIN YEAR as y ON y.YearID = yc.YearID
				WHERE 
					yc.AcademicYearID = '".$yearID."' AND $targetIdCond 
				GROUP BY 
					iu.UserID 
				ORDER BY 
					y.Sequence, yc.Sequence, ycu.ClassNumber";
		$students = $ldiscipline->returnResultSet($sql);
		$student_ids = Get_Array_By_Key($students, "UserID");
		$student_ary = BuildMultiKeyAssoc((array)$students, array($targetField, "UserID"));
		$student_count = count($students);
		
		# Misconduct - Approved
		$sql = "SELECT
					r.RecordID,
					r.StudentID,
					DATE_FORMAT(r.RecordDate,'%Y-%m-%d') as RecordDate,
					$termField,
					r.RecordType,
					cat.CategoryID,
					cat.Name,
					IF(r.GMConductScoreChange IS NULL, 0, r.GMConductScoreChange) as GMConductScoreChange,
					IF(r.RecordDate >= '$startDate 00:00:00' AND r.RecordDate <= '$endDate 23:59:59', 1, 0) as InDateRange
				FROM 
					DISCIPLINE_ACCU_RECORD as r
					INNER JOIN DISCIPLINE_ACCU_CATEGORY as cat ON cat.CategoryID = r.CategoryID
				WHERE 
					r.AcademicYearID = '$yearID' AND $termCond 
		 			r.RecordDate >= '$yearFromDate 00:00:00' AND r.RecordDate <= '$yearToDate 23:59:59' AND
					r.StudentID IN ('".implode("','", (array)$student_ids)."') AND 
					r.RecordType = '".MISCONDUCT."' AND r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."'";
		$gm_records = $ldiscipline->returnResultSet($sql);
		$gm_records = BuildMultiKeyAssoc((array)$gm_records, array("YearTermID", "StudentID", "RecordID"));
		
		# Awards / Punishment - Approved and Released
		$sql = "SELECT 
					r.RecordID,
					r.StudentID,
					DATE_FORMAT(r.RecordDate,'%Y-%m-%d') as RecordDate,
					$termField,
					r.ProfileMeritType,
					IF(r.ProfileMeritType < 0 AND r.ProfileMeritType > -999, 1, 0) as ConductRelated,
					r.ProfileMeritCount,
					m.ItemCode,
					m.ItemName,
					IF(r.ConductScoreChange IS NULL, 0, r.ConductScoreChange) as ConductScoreChange,
					IF(r.RecordDate >= '$startDate' AND r.RecordDate <= '$endDate', 1, 0) as InDateRange
				FROM 
					DISCIPLINE_MERIT_RECORD as r
				INNER JOIN 
					DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID
				WHERE 
					r.AcademicYearID = '$yearID' AND $termCond 
					r.RecordDate >= '$yearFromDate' AND r.RecordDate <= '$yearToDate' AND 
					r.StudentID IN ('".implode("','", (array)$student_ids)."') AND 
					r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."'";
		$ap_records = $ldiscipline->returnResultSet($sql);
		$ap_records = BuildMultiKeyAssoc((array)$ap_records, array("YearTermID", "StudentID", "ConductRelated", "RecordID"));
		
		$x = "	<table width='100%' align='center' class='print_hide' border='0'>
					<tr><td align='right'>".$this->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print")."</td></tr>
				</table>
				<div>";
		$x .= " <div class='main_container'>";
		
		// loop semester
		$isFirstSem = true;
		$TermCount = count((array)$relatedSem);
		foreach((array)$relatedSem as $currentSem)
		{
			// initalize total count
			$totalCountAry = array();
			$totalCountAry["ConductMark"] = 0;
			$totalCountAry[0] = 0;
			$totalCountAry[1] = 0;
			$totalCountAry[2] = 0;
			$totalCountAry[3] = 0;
			$totalCountAry[-1] = 0;
			$totalCountAry[-2] = 0;
			$totalCountAry[-3] = 0;
			
			// Term details
			$thisSemDetails = $termDetails[$currentSem];
			$displayStartDate = $thisSemDetails["TermStart"];
			$displayStartDate = date("Y-m-d", strtotime($displayStartDate));
			$displayEndDate = $thisSemDetails["TermEnd"];
			$displayEndDate = date("Y-m-d", strtotime($displayEndDate));
			
			// Period Type: Date
			if($periodType=="DATE")
			{
				if($isFirstSem || $TermCount==1)
				{
					$displayStartDate = $startDate;
					$displayEndDate = $endDate;
				}
				else
				{
					$displayEndDate = $endDate;
				}
				$isFirstSem = false;
			}
			
			$x .= " <table class='report_header' cellspacing='0' cellpadding='5' border='0' width='100%'>
						<tr><td>開始 日期 : ".$displayStartDate."</td></tr>
						<tr><td>終止 日期: ".$displayEndDate."</td></tr>
					</table>
					<table class='result_table' cellspacing='0' cellpadding='0' width='100%' border='1'>
						<tr>
							<td width='8%'>&nbsp;</td>
							<td width='8%'>優點</td>
							<td width='8%'>大功</td>
							<td width='8%'>小功</td>
							<td width='8%'>警告</td>
							<td width='8%'>扣分</td>
							<td width='8%'>缺點</td>
							<td width='8%'>小過</td>
							<td width='8%'>大過</td>
						</tr>";
			
			// loop classes / levels
			foreach((array)$targetID as $current_targetid)
			{
				// initalize classes / levels count
				$targetCountAry = array();
				$targetCountAry["ConductMark"] = 0;
				$targetCountAry[0] = 0;
				$targetCountAry[1] = 0;
				$targetCountAry[2] = 0;
				$targetCountAry[3] = 0;
				$targetCountAry[-1] = 0;
				$targetCountAry[-2] = 0;
				$targetCountAry[-3] = 0;
			
				// loop students
				$student_list = $student_ary[$current_targetid];
				if(count($student_list) > 0)
				{
					foreach((array)$student_list as $currentStudentID => $currentStudent)
					{
						// initalize student count
						$totalDeduct = 0;
						$thisRangeDeduct = 0;
						
						# AP records not related to Conduct Mark
						$thisStudentData = array();
						$this_ap_non_conduct_score = $ap_records[$currentSem][$currentStudentID][0];
						foreach((array)$this_ap_non_conduct_score as $this_ap)
						{
							if(($periodType!="DATE" || $periodType=="DATE" && $this_ap["InDateRange"] == 1) && $this_ap["ProfileMeritType"] != -999 && $this_ap["ProfileMeritCount"] > 0)
							{
								$thisStudentData[$this_ap["ProfileMeritType"]] += $this_ap["ProfileMeritCount"];
							}
							
						}
						
						// update classes / levels count
						$targetCountAry[0] += ($thisStudentData[0]? $thisStudentData[0] : 0);
						$targetCountAry[1] += ($thisStudentData[1]? $thisStudentData[1] : 0);
						$targetCountAry[2] += ($thisStudentData[2]? $thisStudentData[2] : 0);
						$targetCountAry[3] += ($thisStudentData[3]? $thisStudentData[3] : 0);
						
						# AP records related to Conduct Mark
						$this_ap_conduct_score = $ap_records[$currentSem][$currentStudentID][1];
						foreach((array)$this_ap_conduct_score as $this_ap)
						{
							if($this_ap["ConductScoreChange"] < 0)
							{
								if($periodType!="DATE" || $periodType=="DATE" && $this_ap["RecordDate"] <= $endDate)
									$totalDeduct += abs($this_ap["ConductScoreChange"]);
								if($periodType!="DATE" || $periodType=="DATE" && $this_ap["InDateRange"] == 1)
									$thisRangeDeduct += abs($this_ap["ConductScoreChange"]);
							}
						}
						
						# GM records related to Conduct Mark
						$this_gm_conduct_score = $gm_records[$currentSem][$currentStudentID];
						foreach((array)$this_gm_conduct_score as $this_gm)
						{
							if($this_gm["GMConductScoreChange"] < 0)
							{
								if($periodType!="DATE" || $periodType=="DATE" && $this_gm["RecordDate"] <= "$endDate 23:59:59")
									$totalDeduct += abs($this_gm["GMConductScoreChange"]);
								if($periodType!="DATE" || $periodType=="DATE" && $this_gm["InDateRange"] == 1)
									$thisRangeDeduct += abs($this_gm["GMConductScoreChange"]);
							}
						}
						
						# Calculate demerit number using Conduct Mark
						$conductMarkData = array();
						if($totalDeduct > 0)
						{
							$beforeRangeDeduct = $totalDeduct - $thisRangeDeduct;
							$conductMarkData["ConductMark"] = $thisRangeDeduct > 0? ($totalDeduct % 8) : 0;
							$conductMarkData[-1] = (floor($totalDeduct / 8) % 3) - (floor($beforeRangeDeduct / 8) % 3);
							$conductMarkData[-1] = $conductMarkData[-1] >= 0 ? $conductMarkData[-1] : 0; 
							$conductMarkData[-2] = (floor($totalDeduct / 24) % 3) - (floor($beforeRangeDeduct / 24) % 3);
							$conductMarkData[-2] = $conductMarkData[-2] >= 0 ? $conductMarkData[-2] : 0;
							$conductMarkData[-3] = floor($totalDeduct / 72) - floor($beforeRangeDeduct / 72);
							$conductMarkData[-3] = $conductMarkData[-3] >= 0 ? $conductMarkData[-3] : 0;
							
							// update classes / levels count
							$targetCountAry["ConductMark"] += ($conductMarkData["ConductMark"]? $conductMarkData["ConductMark"] : 0);
							$targetCountAry[-1] += ($conductMarkData[-1]? $conductMarkData[-1] : 0);
							$targetCountAry[-2] += ($conductMarkData[-2]? $conductMarkData[-2] : 0);
							$targetCountAry[-3] += ($conductMarkData[-3]? $conductMarkData[-3] : 0);
						}
					}
				}
				
				$x .= "<tr>";
					$x .= "<td>".$target_name[$current_targetid]["Name"]."</td>";
					$x .= "<td>".($targetCountAry[1]? $targetCountAry[1] : 0)."</td>";
					$x .= "<td>".($targetCountAry[2]? $targetCountAry[2] : 0)."</td>";
					$x .= "<td>".($targetCountAry[3]? $targetCountAry[3] : 0)."</td>";
					$x .= "<td>".($targetCountAry[0]? $targetCountAry[0] : 0)."</td>";
					$x .= "<td>".($targetCountAry["ConductMark"]? $targetCountAry["ConductMark"] : 0)."</td>";
					$x .= "<td>".($targetCountAry[-1]? $targetCountAry[-1] : 0)."</td>";
					$x .= "<td>".($targetCountAry[-2]? $targetCountAry[-2] : 0)."</td>";
					$x .= "<td>".($targetCountAry[-3]? $targetCountAry[-3] : 0)."</td>";
				$x .= "</tr>";
				
				// update total count
				$totalCountAry["ConductMark"] += ($targetCountAry["ConductMark"]? $targetCountAry["ConductMark"] : 0);
				$totalCountAry[0] += ($targetCountAry[0]? $targetCountAry[0] : 0);
				$totalCountAry[1] += ($targetCountAry[1]? $targetCountAry[1] : 0);
				$totalCountAry[2] += ($targetCountAry[2]? $targetCountAry[2] : 0);
				$totalCountAry[3] += ($targetCountAry[3]? $targetCountAry[3] : 0);
				$totalCountAry[-1] += ($targetCountAry[-1]? $targetCountAry[-1] : 0);
				$totalCountAry[-2] += ($targetCountAry[-2]? $targetCountAry[-2] : 0);
				$totalCountAry[-3] += ($targetCountAry[-3]? $targetCountAry[-3] : 0);
			}
			
			$x .= "<tr>";
				$x .= "<td>總數：</td>";
				$x .= "<td>".($totalCountAry[1]? $totalCountAry[1] : 0)."</td>";
				$x .= "<td>".($totalCountAry[2]? $totalCountAry[2] : 0)."</td>";
				$x .= "<td>".($totalCountAry[3]? $totalCountAry[3] : 0)."</td>";
				$x .= "<td>".($totalCountAry[0]? $totalCountAry[0] : 0)."</td>";
				$x .= "<td>".($totalCountAry["ConductMark"]? $totalCountAry["ConductMark"] : 0)."</td>";
				$x .= "<td>".($totalCountAry[-1]? $totalCountAry[-1] : 0)."</td>";
				$x .= "<td>".($totalCountAry[-2]? $totalCountAry[-2] : 0)."</td>";
				$x .= "<td>".($totalCountAry[-3]? $totalCountAry[-3] : 0)."</td>";
			$x .= "</tr>";
			$x .= "</table>";
		}
		
		$x .= "</div>";
		return $x;
	}
	
	function getHYKPersonalReport($dataAry)
	{
		global $PATH_WRT_ROOT, $intranet_root, $ldiscipline;
		global $intranet_session_language, $Lang, $no_record_msg;
		
		# POST data
		$format = $_POST['format'];
		$yearID = $_POST['selectYear'];
		$termID = $_POST['selectSemester'];
		$startDate = $_POST['textFromDate'];
		$endDate = $_POST['textToDate'];
		$target = $_POST['rankTarget'];
		$target_ids = $_POST['rankTargetDetail'];
		$target_sids = $_POST['studentID'];
		$hide_empty_target = $_POST['HideNoRecordStudent'];
		
		# Set parms for "Mgmt > Print Punishment Warning Letter"
		$is_print_notice_mode = isset($_POST['curSubType']);
		$print_notice_mode = $_POST['curSubType'];
		
		# Warning Letter (Single)
		if($print_notice_mode == 'single')
		{
		    // Get searching criteria when generate Notice List Table
		    $yearID = $_POST['periodYear'];
    		$termID = $_POST['periodSemester'];
    		//$startDate = $_POST['periodStart'];
    		$endDate = $_POST['periodEnd'];
    		$target = $_POST['searchType'];
    		$target_ids = $_POST['searchTargetIDArr'];
    		$target_sids = $_POST['searchStudentIDArr'];
    		$target_notice_ids = $_POST['NoticeIDArr'];
    		
    		// Use Term Start
    		$SQL_startdate = getStartDateOfAcademicYear($yearID, $termID);
    		$startDate = date("Y-m-d", strtotime($SQL_startdate));
		}
		# Warning Letter (Monthly)
		else if($print_notice_mode == 'monthly')
		{
		    $notice_generate_log_id = $_POST['GenerateLogID'];
		    $notice_generate_log = $ldiscipline->getGeneratedNoticeLog($yearID, '', '', '', $notice_generate_log_id, $forSelection=false);
		    
		    // Get log info from db
		    $yearID = $notice_generate_log[0]['AcademicYearID'];
		    $termID = $notice_generate_log[0]['SemesterID'];
		    $startDate = $notice_generate_log[0]['StartDate'];
		    $endDate = $notice_generate_log[0]['EndDate'];
		}
		
		# Year - Date Range
		$SQL_startdate = getStartDateOfAcademicYear($yearID, $termID);
		$SQL_enddate = getEndDateOfAcademicYear($yearID, $termID);
		$yearFromDate = date("Y-m-d", strtotime($SQL_startdate));
		$yearToDate = date("Y-m-d", strtotime($SQL_enddate));
		$YearInfo = getAcademicYearInfoAndTermInfoByDate($yearFromDate);
		
		# Warning Letter
		if($is_print_notice_mode)
		{
		    $yearToDate = $endDate;
		}
		
		# Date Target
		$startDateField = ($yearFromDate < $startDate)? $startDate : $yearFromDate;
		$endDateField = ($yearToDate > $endDate)? $endDate : $yearToDate;
		
		# Report Target
		if($target=="form") {
			$targetIdCond = " y.YearID IN ('".implode("','",(array)$target_ids)."') ";
			$classCond = " YearID IN ('".implode("','",(array)$target_ids)."') ";
		}
		else if($target=="class") {
			$targetIdCond = " yc.YearClassID IN ('".implode("','",(array)$target_ids)."') ";
			$classCond = " YearClassID IN ('".implode("','",(array)$target_ids)."') ";
		}
		else if($target=="student") {
			$targetIdCond = " ycu.UserID IN ('".implode("','",(array)$target_sids)."') ";
			$classCond = " YearClassID IN ('".implode("','",(array)$target_ids)."') ";
		}
		else {
			$targetIdCond = " 0 ";
			$classCond = " 0 ";
		}
		
		# Year and Semester Name
		$sql = "SELECT
					ay.YearNameB5, ayt.YearTermNameB5
				FROM
					ACADEMIC_YEAR_TERM as ayt
					INNER JOIN ACADEMIC_YEAR as ay On (ayt.AcademicYearID = ay.AcademicYearID)
				WHERE
					ayt.AcademicYearID = '$yearID' AND ayt.YearTermID = '$termID' ";
		$year_term_name = $ldiscipline->returnArray($sql);
		$year_term_name = $year_term_name[0];
		
		# Class Name
		$sql = "SELECT
					YearClassID,
					ClassTitleB5 as ClassName
				FROM
					YEAR_CLASS
				WHERE
					AcademicYearID = '".$yearID."' AND $classCond ";
		$class_name = $ldiscipline->returnArray($sql);
		$class_name = BuildMultiKeyAssoc((array)$class_name, array("YearClassID"));
		
		# Target Students
		$sql = "SELECT
					iu.UserID,
					yc.YearClassID,
					yc.ClassTitleEN as ClassName,
					ycu.ClassNumber,
					CONCAT(
						IF(yc.ClassTitleEN IS NULL OR yc.ClassTitleEN='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".'<span class="tabletextrequire">'."^</span>', ''),
						iu.ChineseName
					) as StudentName
				FROM INTRANET_USER as iu
					LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID
					LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$yearID')
					LEFT JOIN YEAR as y ON y.YearID = yc.YearID
				WHERE
					yc.AcademicYearID = '".$yearID."' AND $targetIdCond
				GROUP BY
					iu.UserID
				ORDER BY
					y.Sequence, yc.Sequence, ycu.ClassNumber";
		$students = $ldiscipline->returnResultSet($sql);
		$student_ids = Get_Array_By_Key($students, "UserID");
		$student_ary = BuildMultiKeyAssoc((array)$students, array("YearClassID", "UserID"));
		$student_count = count($students);
		
		# Misconduct - Approved
		$sql = "SELECT
					r.RecordID,
					r.StudentID,
					DATE_FORMAT(r.RecordDate,'%Y-%m-%d') as RecordDate,
					DATE_FORMAT(r.DateInput,'%Y-%m-%d') as InputRecordDate,
                    r.DateInput as InputRecordDateTime,
					r.YearTermID,
					r.RecordType,
					cati.ItemID,
					cati.Name as ItemName,
					r.Remark,
					r.PICID,
					r.CreatedBy,
					IF(r.GMConductScoreChange IS NULL, 0, r.GMConductScoreChange) as GMConductScoreChange,
					IF(r.RecordDate >= '$startDate 00:00:00' AND r.RecordDate <= '$endDate 23:59:59', 1, 0) as InDateRange,
					'GM' as ThisRecordType
				FROM
					DISCIPLINE_ACCU_RECORD as r
				    INNER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as cati ON cati.ItemID = r.ItemID
				WHERE
					r.AcademicYearID = '$yearID' AND r.YearTermID = '$termID' AND
					r.RecordDate >= '$yearFromDate 00:00:00' AND r.RecordDate <= '$yearToDate 23:59:59' AND
					r.StudentID IN ('".implode("','", (array)$student_ids)."') AND
					r.RecordType = '".MISCONDUCT."' AND r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."'";
		$gm_records = $ldiscipline->returnResultSet($sql);
		$gm_records_picid = Get_Array_By_Key($gm_records, "PICID");
		$gm_records_creatorid = Get_Array_By_Key($gm_records, "CreatedBy");
		$gm_records = BuildMultiKeyAssoc((array)$gm_records, array("StudentID", "RecordID"));
		
		# Awards / Punishment - Approved and Released
		$sql = "SELECT
					r.RecordID,
					r.StudentID,
					DATE_FORMAT(r.RecordDate,'%Y-%m-%d') as RecordDate,
					DATE_FORMAT(r.DateInput,'%Y-%m-%d') as InputRecordDate,
                    r.DateInput as InputRecordDateTime,
					r.YearTermID,
					r.ProfileMeritType,
					IF(r.ProfileMeritType < 0 AND r.ProfileMeritType > -999, 1, 0) as ConductRelated,
					r.ProfileMeritCount,
					m.ItemCode,
					m.ItemName,
					r.Remark,
					r.PICID,
					r.CreatedBy,
					IF(r.ConductScoreChange IS NULL, 0, r.ConductScoreChange) as ConductScoreChange,
					IF(r.RecordDate >= '$startDate' AND r.RecordDate <= '$endDate', 1, 0) as InDateRange,
					'AP' as ThisRecordType
				FROM
					DISCIPLINE_MERIT_RECORD as r
				    INNER JOIN DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID
				WHERE
					r.AcademicYearID = '$yearID' AND r.YearTermID = '$termID' AND
					r.RecordDate >= '$yearFromDate' AND r.RecordDate <= '$yearToDate' AND
					r.StudentID IN ('".implode("','", (array)$student_ids)."') AND
					r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."'";
		$ap_records = $ldiscipline->returnResultSet($sql);
		$ap_records_picid = Get_Array_By_Key($ap_records, "PICID");
		$ap_records_creatorid = Get_Array_By_Key($ap_records, "CreatedBy");
		$ap_records = BuildMultiKeyAssoc((array)$ap_records, array("StudentID", "ConductRelated", "RecordID"));
		
		# PIC and Input User
		$user_list = array();
		$user_list = array_merge($gm_records_picid, $gm_records_creatorid, $ap_records_picid, $ap_records_creatorid);
		$user_list = array_filter(array_unique($user_list));
		$sql = "SELECT
					iu.UserID,
					CONCAT(
						IF(iu.RecordStatus != 1, '".'<span class="tabletextrequire">'."^</span>', ''),
						iu.ChineseName
					) as StaffName
				FROM INTRANET_USER as iu
				WHERE
					iu.UserID IN (".implode(",", (array)$user_list).")";
		$user_list = $ldiscipline->returnResultSet($sql);
		$user_list = BuildMultiKeyAssoc((array)$user_list, array("UserID"));
		
		# Get Notice Content
		if($is_print_notice_mode)
		{
		    # Discipline Related Notice
    		if($print_notice_mode == 'single' && !empty($target_notice_ids))
    		{
    		    $user_notice_ary = $ldiscipline->getDemeritLinkedNoticeContent($target_notice_ids);
    		    $user_notice_ary = BuildMultiKeyAssoc((array)$user_notice_ary, array("RecipientID", "NoticeID"));
    		}    		
    		# Generated Notice
    		else if($print_notice_mode == 'monthly' && $notice_generate_log_id != '')
    		{
    		    $generated_notice_list = $ldiscipline->getGeneratedNoticeContent($notice_generate_log_id, $student_ids);
        		$generated_notice_list = BuildMultiKeyAssoc((array)$generated_notice_list, 'RecipientID', 'Description', 1, 0);
    		}
		}
		
		# Print Button
		if($format != 'word')
		{
    		$x = "	<table width='100%' align='center' class='print_hide' border='0'>
    					<tr><td align='right'>".$this->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print")."</td></tr>
    				</table>
    				<div>";
		}
		
		$isFirstReport = true;
		if(count($student_ary) > 0)
		{
			// loop classes
			foreach((array)$class_name as $current_classid => $this_class_info)
			{
				// loop students
				$student_list = $student_ary[$current_classid];
				foreach((array)$student_list as $currentStudentID => $currentStudent)
				{
					# Student Info
					$student_id = $currentStudent['UserID'];
					$student_name = $currentStudent['StudentName'];
					$class_number = $currentStudent['ClassNumber'];
					
					# Discipline Related Notice
					if($is_print_notice_mode && $print_notice_mode == 'single')
					{
					    if(!empty($user_notice_ary[$student_id]))
					    {
					        foreach((array)$user_notice_ary[$student_id] as $this_notice_id => $this_single_notice)
					        {
					            # Init
					            $termDeduct = 0;
					            $totalDeduct = 0;
					            $thisRangeDeduct = 0;
					            $thisStudentData = array();
					            $conductMarkData = array();
					            $termConductMarkData = array();
					            $thisTermStudentData = array();
					            $displayMeritRecords = array();
					            
					            # Get Notice Start Date > get report data when notice created
					            $this_start_date = $this_single_notice['DateStart'];
					            
					            # AP records not related to Conduct Mark
					            $this_ap_non_conduct_score = $ap_records[$currentStudentID][0];
					            foreach((array)$this_ap_non_conduct_score as $this_ap)
					            {
					                if($this_ap["ProfileMeritType"] != -999 && $this_ap["ProfileMeritCount"] > 0)
					                {
					                    if($this_ap["InputRecordDateTime"] <= $this_start_date) {
					                        $thisTermStudentData[$this_ap["ProfileMeritType"]] += $this_ap["ProfileMeritCount"];
					                        $thisStudentData[$this_ap["ProfileMeritType"]] += $this_ap["ProfileMeritCount"];
					                    }
					                }
					            }
					            
					            # AP records related to Conduct Mark
					            $this_ap_conduct_score = $ap_records[$currentStudentID][1];
					            foreach((array)$this_ap_conduct_score as $this_ap)
					            {
					                if($this_ap["ConductScoreChange"] < 0)
					                {
					                    if($this_ap["InputRecordDateTime"] <= $this_start_date) {
					                        $termDeduct += abs($this_ap["ConductScoreChange"]);
					                        $totalDeduct += abs($this_ap["ConductScoreChange"]);
					                        $thisRangeDeduct += abs($this_ap["ConductScoreChange"]);
					                        $displayMeritRecords[$this_ap["RecordDate"]][] = $this_ap;
					                    }
					                }
					            }
					            
					            # GM records related to Conduct Mark
					            $this_gm_conduct_score = $gm_records[$currentStudentID];
					            foreach((array)$this_gm_conduct_score as $this_gm)
					            {
					                if($this_gm["GMConductScoreChange"] < 0)
					                {
					                    if($this_gm["InputRecordDateTime"] <= "$this_start_date 23:59:59") {
					                        $termDeduct += abs($this_gm["GMConductScoreChange"]);
					                        $totalDeduct += abs($this_gm["GMConductScoreChange"]);
					                        $thisRangeDeduct += abs($this_gm["GMConductScoreChange"]);
					                        $displayMeritRecords[$this_gm["RecordDate"]][] = $this_gm;
					                    }
					                }
					            }
					            
					            # Calculate Demerit number using Conduct Mark
					            if($termDeduct > 0)
					            {
					                $beforeRangeDeduct = $totalDeduct - $thisRangeDeduct;
					                $conductMarkData["ConductMark"] = $thisRangeDeduct > 0? ($totalDeduct % 8) : 0;
					                $conductMarkData[-1] = (floor($totalDeduct / 8) % 3) - (floor($beforeRangeDeduct / 8) % 3);
					                $conductMarkData[-1] = $conductMarkData[-1] >= 0 ? $conductMarkData[-1] : 0;
					                $conductMarkData[-2] = (floor($totalDeduct / 24) % 3) - (floor($beforeRangeDeduct / 24) % 3);
					                $conductMarkData[-2] = $conductMarkData[-2] >= 0 ? $conductMarkData[-2] : 0;
					                $conductMarkData[-3] = floor($totalDeduct / 72) - floor($beforeRangeDeduct / 72);
					                $conductMarkData[-3] = $conductMarkData[-3] >= 0 ? $conductMarkData[-3] : 0;
					                
					                $termConductMarkData["ConductMark"] = ($termDeduct % 8);
					                $termConductMarkData[-1] = floor($termDeduct / 8) % 3;
					                $termConductMarkData[-1] = $termConductMarkData[-1] >= 0 ? $termConductMarkData[-1] : 0;
					                $termConductMarkData[-2] = floor($termDeduct / 24) % 3;
					                $termConductMarkData[-2] = $termConductMarkData[-2] >= 0 ? $termConductMarkData[-2] : 0;
					                $termConductMarkData[-3] = floor($termDeduct / 72);
					                $termConductMarkData[-3] = $termConductMarkData[-3] >= 0 ? $termConductMarkData[-3] : 0;
					            }
					            
					            # Exclude student if without any records
					            if($hide_empty_target && count((array)$displayMeritRecords)==0) {
					                continue;
					            }
					            
					            # Page Break
					            if(!$isFirstReport) {
					                if($format == 'word') {
					                    $x .= '&nbsp; <br style="page-break-before: always"> ';
					                }
					                else {
					                    $x .= "<div style=\"page-break-after:always\">&nbsp;</div>";
					                }
					            }
					            $isFirstReport = false;
					            
				                $x .= " <div class='main_container'>";
				                    $x .= $this_single_notice['Description'];
				                $x .= " </div>";
				                
				                if($format == 'word') {
				                    $x .= '&nbsp; <br style="page-break-before: always"> ';
				                }
				                else {
				                    $x .= "<div style=\"page-break-after:always\">&nbsp;</div>";
				                }
					            
					            # Report Header
					            $x .= " <div class='main_container'>
        								<table class='report_header' cellspacing='0' cellpadding='5' border='0' width='100%'>
        									<tr><td>學生姓名： ".$student_name."</td></tr>
        									<tr><td>班別： ".$this_class_info["ClassName"]." (".$class_number.")</td></tr>
        									<tr><td>學期： ".$year_term_name[1]."　　　　　　　　學年： ".$year_term_name[0]."</td></tr>";
					            
					            # Warning Letter (Hide 2 rows)
					            if($is_print_notice_mode)
					            {
					                // do nothing
					            }
					            else 
					            {
					               $x .= "  <tr><td>開始 日期： ".$startDate."</td></tr>
        									<tr><td>終止 日期： ".$endDate."</td></tr>";
					            }
					            
					            $x .= "  <tr><td>&nbsp;</td></tr>
        									<tr><td>缺點紀錄：</td></tr>
        								</table>";
					            
					            # Merit Count Table Header
					            $x .= " <table class='result_table' cellspacing='0' cellpadding='0' width='100%' border='1'>
    									<tr>
    										<td width='8%'>&nbsp;</td>
    										<td width='8%'>優點</td>
    										<td width='8%'>大功</td>
    										<td width='8%'>小功</td>
    										<td width='8%'>警告</td>
    										<td width='8%'>扣分</td>
    										<td width='8%'>缺點</td>
    										<td width='8%'>小過</td>
    										<td width='8%'>大過</td>
    									</tr>";
					            
					            # Merit Count Table Content
					            # Warning Letter (Hide 1 row)
					            if($is_print_notice_mode)
					            {
					                // do nothing
					            }
					            else
					            {
    					            $x .= "<tr>";
        					            $x .= "<td>選擇期限</td>";
        					            $x .= "<td>".($thisStudentData[1]? $thisStudentData[1] : 0)."</td>";
        					            $x .= "<td>".($thisStudentData[2]? $thisStudentData[2] : 0)."</td>";
        					            $x .= "<td>".($thisStudentData[3]? $thisStudentData[3] : 0)."</td>";
        					            $x .= "<td>".($thisStudentData[0]? $thisStudentData[0] : 0)."</td>";
        					            $x .= "<td>".($conductMarkData["ConductMark"]? $conductMarkData["ConductMark"] : 0)."</td>";
        					            $x .= "<td>".($conductMarkData[-1]? $conductMarkData[-1] : 0)."</td>";
        					            $x .= "<td>".($conductMarkData[-2]? $conductMarkData[-2] : 0)."</td>";
        					            $x .= "<td>".($conductMarkData[-3]? $conductMarkData[-3] : 0)."</td>";
    					            $x .= "</tr>";
					            }
					            $x .= "<tr>";
    					            $x .= "<td>本學期</td>";
    					            $x .= "<td>".($thisTermStudentData[1]? $thisTermStudentData[1] : 0)."</td>";
    					            $x .= "<td>".($thisTermStudentData[2]? $thisTermStudentData[2] : 0)."</td>";
    					            $x .= "<td>".($thisTermStudentData[3]? $thisTermStudentData[3] : 0)."</td>";
    					            $x .= "<td>".($thisTermStudentData[0]? $thisTermStudentData[0] : 0)."</td>";
    					            $x .= "<td>".($termConductMarkData["ConductMark"]? $termConductMarkData["ConductMark"] : 0)."</td>";
    					            $x .= "<td>".($termConductMarkData[-1]? $termConductMarkData[-1] : 0)."</td>";
    					            $x .= "<td>".($termConductMarkData[-2]? $termConductMarkData[-2] : 0)."</td>";
    					            $x .= "<td>".($termConductMarkData[-3]? $termConductMarkData[-3] : 0)."</td>";
					            $x .= "</tr>";
					            $x .= "</table>";
					            
					            $x .= "<br/>";
					            
					            # Demerit Record Table Header
					            if($format == 'word')
					            {
					                $table_header = "   <table class='demerit_table' cellspacing='0' cellpadding='0' width='100%' border='1'>
                                                        <tr>
                                                            <td colspan='7' style='border: 0'>違規紀錄：</td>
                                                        </tr>
                                                        <tr>
                                                        	<td width='11%'>違事日期</td>
                                                        	<td width='11%'>紀錄日期</td>
                                                        	<td width='25%'>違規事項</td>
                                                        	<td width='15%'>扣分老師</td>
                                                        	<td width='9%'>紀錄老師</td>
                                                        	<td width='9%'>操行紀錄</td>
                                                        	<td width='20%'>備註</td>
                                                        </tr>";
					            }
					            else
					            {
					                $table_header = "   <span class='demerit_table_header'>違規紀錄：</span>";
					                $table_header .= "  <table class='result_table demerit_table' cellspacing='0' cellpadding='0' width='100%' border='1'>
        												<tr>
        													<td width='11%'>違事日期</td>
        													<td width='11%'>紀錄日期</td>
        													<td width='25%'>違規事項</td>
        													<td width='15%'>扣分老師</td>
        													<td width='9%'>紀錄老師</td>
        													<td width='9%'>操行紀錄</td>
        													<td width='20%'>備註</td>
        												</tr>";
					            }
					            $x .= $table_header;
					            
					            // loop Demerit Records
					            if(count((array)$displayMeritRecords) > 0)
					            {
					                $max_row = 38;
					                $row_count = 0;
					                
					                ksort($displayMeritRecords);
					                foreach((array)$displayMeritRecords as $thisDateMeritRecord) {
					                    foreach((array)$thisDateMeritRecord as $thisMeritRecord)
					                    {
					                        // Page Break
					                        if($row_count > $max_row)
					                        {
					                            $x .= "</table>";
					                            $x .= "</div>";
					                            
					                            if($format == 'word') {
					                                $x .= '&nbsp; <br style="page-break-before: always"> ';
					                            }
					                            else {
					                                $x .= "<div style=\"page-break-after:always\">&nbsp;</div>";
					                            }
					                            
					                            $x .= " <div class='main_container'>";
					                            $x .= $table_header;
					                            
					                            $max_row = 48;
					                            $row_count = 0;
					                        }
					                        
					                        // Merit Record Type
					                        $thisMeritRecordType = $thisMeritRecord["ThisRecordType"];
					                        
					                        // Conduct Score
					                        $thisMeritRecordConductScore = $thisMeritRecord["GMConductScoreChange"]? $thisMeritRecord["GMConductScoreChange"] : $thisMeritRecord["ConductScoreChange"];
					                        $thisMeritRecordConductScore = abs($thisMeritRecordConductScore);
					                        
					                        // Remarks
					                        $thisMeritRecordRemarks = $thisMeritRecord["Remark"]? $thisMeritRecord["Remark"] : "---";
					                        
					                        // Creator
					                        $thisMeritRecordCreatedID = $thisMeritRecord["CreatedBy"];
					                        $thisMeritRecordCreatedName = $user_list[$thisMeritRecordCreatedID]["StaffName"];
					                        $thisMeritRecordCreatedName = $thisMeritRecordCreatedName? $thisMeritRecordCreatedName : "---";
					                        
					                        // PIC
					                        $thisMeritRecordPICName = array();
					                        $thisMeritRecordPICID = explode(",", $thisMeritRecord["PICID"]);
					                        foreach($thisMeritRecordPICID as $thisPICID) {
					                            $thisPICName = trim($user_list[$thisPICID]["StaffName"]);
					                            if($thisPICName != "")
					                                $thisMeritRecordPICName[] = $thisPICName;
					                        }
					                        $thisMeritRecordPICName = implode('、', $thisMeritRecordPICName);
					                        
					                        # Demerit Record Table Content
					                        $x .= " <tr>
            											<td>".$thisMeritRecord["RecordDate"]."</td>
            											<td>".$thisMeritRecord["InputRecordDate"]."</td>
            											<td>".$thisMeritRecord["ItemName"]."</td>
            											<td>".$thisMeritRecordPICName."</td>
            											<td>".$thisMeritRecordCreatedName."</td>
            											<td>".$thisMeritRecordConductScore." 扣分</td>
            											<td>".$thisMeritRecordRemarks."</td>
            										</tr>";

					                        ### Page break predict (by row count) [START]
					                        ### Item Name row count
					                        // handle string lang
					                        $item_eng_str = "";
					                        $item_chi_str = $thisMeritRecord["ItemName"];
					                        if($thisMeritRecordType == "GM")
					                        {
					                            $item_eng_str = substr_lang($item_chi_str, "ENG");
					                            $item_chi_str = substr_lang($item_chi_str, "CHI");
					                        }
                                            $item_en_string_length = countString($item_eng_str);
                                            $item_ch_string_length = countString($item_chi_str);
					                        
					                        // predict row count
                                            $item_string_length = $item_ch_string_length;
					                        if($item_en_string_length > 0) {
					                            $item_string_length += ceil($item_en_string_length / 2);
					                        }
					                        // $row_count += ceil($item_string_length / 12);
                                            $item_row_count = ceil($item_string_length / 12);

                                            ### Remarks row count
                                            // handle string lang
                                            $remarks_eng_str = substr_lang($thisMeritRecordRemarks, "ENG");
                                            $remarks_chi_str = substr_lang($thisMeritRecordRemarks, "CHI");
                                            $remarks_en_string_length = countString($remarks_eng_str);
                                            $remarks_ch_string_length = countString($remarks_chi_str);

                                            // predict row count
                                            $remarks_string_length = $remarks_ch_string_length;
                                            if($remarks_en_string_length > 0) {
                                                $remarks_string_length += ceil($remarks_en_string_length / 1.5);
                                            }
                                            $remakrs_row_count = ceil($remarks_string_length / 9);

                                            // apply row count - Item Name or Remarks
                                            $row_count += $remakrs_row_count > $item_row_count ? $remakrs_row_count : $item_row_count;
                                            ### Page break predict (by row count) [END]
					                    }
					                }
                                }
					            else
                                {
					                $x .= " <tr><td colspan='7' style='text-align: center'>$no_record_msg</td></tr>";
					            }
					            $x .= "</table>";
					            $x .= "</div>";
					        }
					    }
					}
					# Personal Report / Generated Notice
					else
					{
    					# Init
    					$termDeduct = 0;
    					$totalDeduct = 0;
    					$thisRangeDeduct = 0;
    					$thisStudentData = array();
    					$conductMarkData = array();
    					$termConductMarkData = array();
    					$thisTermStudentData = array();
    					$displayMeritRecords = array();
    					$displayAllMeritRecords = array();
    					
    					# AP records not related to Conduct Mark
    					$this_ap_non_conduct_score = $ap_records[$currentStudentID][0];
    					foreach((array)$this_ap_non_conduct_score as $this_ap)
    					{
    						if($this_ap["ProfileMeritType"] != -999 && $this_ap["ProfileMeritCount"] > 0)
    						{
    							$thisTermStudentData[$this_ap["ProfileMeritType"]] += $this_ap["ProfileMeritCount"];
    							if($this_ap["InDateRange"] == 1) {
    								$thisStudentData[$this_ap["ProfileMeritType"]] += $this_ap["ProfileMeritCount"];
    							}
    						}
    					}
    					
    					# AP records related to Conduct Mark
    					$this_ap_conduct_score = $ap_records[$currentStudentID][1];
    					foreach((array)$this_ap_conduct_score as $this_ap)
    					{
    						if($this_ap["ConductScoreChange"] < 0)
    						{
    							$termDeduct += abs($this_ap["ConductScoreChange"]);
    							if($this_ap["RecordDate"] <= $endDate) {
    							    $totalDeduct += abs($this_ap["ConductScoreChange"]);
    							    $displayAllMeritRecords[$this_ap["RecordDate"]][] = $this_ap;
    							}
    							if($this_ap["InDateRange"] == 1) {
    								$thisRangeDeduct += abs($this_ap["ConductScoreChange"]);
    								$displayMeritRecords[$this_ap["RecordDate"]][] = $this_ap;
    							}
    						}
    					}
    					
    					# GM records related to Conduct Mark
    					$this_gm_conduct_score = $gm_records[$currentStudentID];
    					foreach((array)$this_gm_conduct_score as $this_gm)
    					{
    						if($this_gm["GMConductScoreChange"] < 0)
    						{
    							$termDeduct += abs($this_gm["GMConductScoreChange"]);
    							if($this_gm["RecordDate"] <= "$endDate 23:59:59") {
    							    $totalDeduct += abs($this_gm["GMConductScoreChange"]);
    							    $displayAllMeritRecords[$this_gm["RecordDate"]][] = $this_gm;
    							}
    							if($this_gm["InDateRange"] == 1) {
    								$thisRangeDeduct += abs($this_gm["GMConductScoreChange"]);
    								$displayMeritRecords[$this_gm["RecordDate"]][] = $this_gm;
    							}
    						}
    					}
    					
    					# Calculate Demerit number using Conduct Mark
    					if($termDeduct > 0)
    					{
    						$beforeRangeDeduct = $totalDeduct - $thisRangeDeduct;
    						$conductMarkData["ConductMark"] = $thisRangeDeduct > 0? ($totalDeduct % 8) : 0;
    						$conductMarkData[-1] = (floor($totalDeduct / 8) % 3) - (floor($beforeRangeDeduct / 8) % 3);
    						$conductMarkData[-1] = $conductMarkData[-1] >= 0 ? $conductMarkData[-1] : 0;
    						$conductMarkData[-2] = (floor($totalDeduct / 24) % 3) - (floor($beforeRangeDeduct / 24) % 3);
    						$conductMarkData[-2] = $conductMarkData[-2] >= 0 ? $conductMarkData[-2] : 0;
    						$conductMarkData[-3] = floor($totalDeduct / 72) - floor($beforeRangeDeduct / 72);
    						$conductMarkData[-3] = $conductMarkData[-3] >= 0 ? $conductMarkData[-3] : 0;
    						
    						$termConductMarkData["ConductMark"] = ($termDeduct % 8);
    						$termConductMarkData[-1] = floor($termDeduct / 8) % 3;
    						$termConductMarkData[-1] = $termConductMarkData[-1] >= 0 ? $termConductMarkData[-1] : 0;
    						$termConductMarkData[-2] = floor($termDeduct / 24) % 3;
    						$termConductMarkData[-2] = $termConductMarkData[-2] >= 0 ? $termConductMarkData[-2] : 0;
    						$termConductMarkData[-3] = floor($termDeduct / 72);
    						$termConductMarkData[-3] = $termConductMarkData[-3] >= 0 ? $termConductMarkData[-3] : 0;
    					}
    					
    					# Exclude student if without any records
    					if($hide_empty_target && count((array)$displayMeritRecords)==0) {
    						continue;
    					}
    					# Exclude student if without any related notice
    					if($print_notice_mode == 'monthly' && !isset($generated_notice_list[$currentStudentID])) {
    					    continue;
    					}
    					
    					# Page Break
    					if(!$isFirstReport) {
    					    if($format == 'word') {
    					        $x .= '&nbsp; <br style="page-break-before: always"> ';
    					    }
    					    else {
    						    $x .= "<div style=\"page-break-after:always\">&nbsp;</div>";
    					    }
    					}
    					
    					# Notice Content
    					if($print_notice_mode == 'monthly')
    					{
    					    if(isset($generated_notice_list[$currentStudentID]))
    					    {
        					    $x .= " <div class='main_container'>";
        					        $x .= $generated_notice_list[$currentStudentID];
        					    $x .= " </div>";
        					    
        					    if($format == 'word') {
        					        $x .= '&nbsp; <br style="page-break-before: always"> ';
        					    }
        					    else {
        					        $x .= "<div style=\"page-break-after:always\">&nbsp;</div>";
        					    }
    					    }
//     					    else 
//     					    {
//     					        continue;
//     					    }
    					}
    					$isFirstReport = false;
    					
    					# Report Header
    					$x .= " <div class='main_container'>
    								<table class='report_header' cellspacing='0' cellpadding='5' border='0' width='100%'>
    									<tr><td>學生姓名： ".$student_name."</td></tr>
    									<tr><td>班別： ".$this_class_info["ClassName"]." (".$class_number.")</td></tr>
    									<tr><td>學期： ".$year_term_name[1]."　　　　　　　　學年： ".$year_term_name[0]."</td></tr>";
			            
			            # Warning Letter (Hide 2 rows)
			            if($is_print_notice_mode)
			            {
			                // do nothing
			            }
			            else 
			            {
                            $x .= "		<tr><td>開始 日期： ".$startDate."</td></tr>
										<tr><td>終止 日期： ".$endDate."</td></tr>";
			            }
			            
			            $x .= "		    <tr><td>&nbsp;</td></tr>
                                        <tr><td>缺點紀錄：</td></tr>
								    </table>";
    					
    					# Merit Count Table Header
    					$x .= " 	<table class='result_table' cellspacing='0' cellpadding='0' width='100%' border='1'>
    									<tr>
    										<td width='8%'>&nbsp;</td>
    										<td width='8%'>優點</td>
    										<td width='8%'>大功</td>
    										<td width='8%'>小功</td>
    										<td width='8%'>警告</td>
    										<td width='8%'>扣分</td>
    										<td width='8%'>缺點</td>
    										<td width='8%'>小過</td>
    										<td width='8%'>大過</td>
    									</tr>";
    					
    					# Merit Count Table Content
    					# Warning Letter (Hide 1 row)
    					if($is_print_notice_mode)
    					{
    					    // do nothing
    					}
    					else
    					{
        					$x .= "<tr>";
        						$x .= "<td>選擇期限</td>";
        						$x .= "<td>".($thisStudentData[1]? $thisStudentData[1] : 0)."</td>";
        						$x .= "<td>".($thisStudentData[2]? $thisStudentData[2] : 0)."</td>";
        						$x .= "<td>".($thisStudentData[3]? $thisStudentData[3] : 0)."</td>";
        						$x .= "<td>".($thisStudentData[0]? $thisStudentData[0] : 0)."</td>";
        						$x .= "<td>".($conductMarkData["ConductMark"]? $conductMarkData["ConductMark"] : 0)."</td>";
        						$x .= "<td>".($conductMarkData[-1]? $conductMarkData[-1] : 0)."</td>";
        						$x .= "<td>".($conductMarkData[-2]? $conductMarkData[-2] : 0)."</td>";
        						$x .= "<td>".($conductMarkData[-3]? $conductMarkData[-3] : 0)."</td>";
        					$x .= "</tr>";
    					}
    					$x .= "<tr>";
    						$x .= "<td>本學期</td>";
    						$x .= "<td>".($thisTermStudentData[1]? $thisTermStudentData[1] : 0)."</td>";
    						$x .= "<td>".($thisTermStudentData[2]? $thisTermStudentData[2] : 0)."</td>";
    						$x .= "<td>".($thisTermStudentData[3]? $thisTermStudentData[3] : 0)."</td>";
    						$x .= "<td>".($thisTermStudentData[0]? $thisTermStudentData[0] : 0)."</td>";
    						$x .= "<td>".($termConductMarkData["ConductMark"]? $termConductMarkData["ConductMark"] : 0)."</td>";
    						$x .= "<td>".($termConductMarkData[-1]? $termConductMarkData[-1] : 0)."</td>";
    						$x .= "<td>".($termConductMarkData[-2]? $termConductMarkData[-2] : 0)."</td>";
    						$x .= "<td>".($termConductMarkData[-3]? $termConductMarkData[-3] : 0)."</td>";
    					$x .= "</tr>";
    					$x .= "</table>";
    					
    					$x .= "<br/>";
    					
    					# Demerit Record Table Header
    					if($format == 'word')
    					{
    					    $table_header = "   <table class='demerit_table' cellspacing='0' cellpadding='0' width='100%' border='1'>
                                                <tr>
                                                    <td colspan='7' style='border: 0'>違規紀錄：</td>
                                                </tr>
    											<tr>
    												<td>違事日期</td>
    												<td>紀錄日期</td>
    												<td>違規事項</td>
    												<td>扣分老師</td>
    												<td>紀錄老師</td>
    												<td>操行紀錄</td>
    												<td>備註</td>
    											</tr>";
    					}
    					else
    					{
        					$table_header = "   <span class='demerit_table_header'>違規紀錄：</span>";
        					$table_header .= "  <table class='result_table demerit_table' cellspacing='0' cellpadding='0' width='100%' border='1'>
    											<tr>
    												<td width='11%'>違事日期</td>
    												<td width='11%'>紀錄日期</td>
    												<td width='25%'>違規事項</td>
    												<td width='15%'>扣分老師</td>
    												<td width='9%'>紀錄老師</td>
    												<td width='9%'>操行紀錄</td>
    												<td width='20%'>備註</td>
    											</tr>";
    					}
    					$x .= $table_header;
    					
    					# Warning Letter (show all in current term)
    					if($is_print_notice_mode)
    					{
    					    $displayMeritRecords = $displayAllMeritRecords;
    					}
    					
    					// loop Demerit Records
    					if(count((array)$displayMeritRecords) > 0)
    					{
    						$max_row = 38;
    						$row_count = 0;
    						
    						ksort($displayMeritRecords);
    						foreach((array)$displayMeritRecords as $thisDateMeritRecord) {
    							foreach((array)$thisDateMeritRecord as $thisMeritRecord)
    							{
    								// Page Break
    								if($row_count > $max_row)
    								{
    									$x .= "</table>";
    									$x .= "</div>";
    									
    									if($format == 'word') {
    									    $x .= '&nbsp; <br style="page-break-before: always"> ';
    									}
    									else {
    									    $x .= "<div style=\"page-break-after:always\">&nbsp;</div>";
    									}
    									
    									$x .= " <div class='main_container'>";
    									$x .= $table_header;
    									
    									$max_row = 48;
    									$row_count = 0;
    								}
    								
    								// Merit Record Type
    								$thisMeritRecordType = $thisMeritRecord["ThisRecordType"];
    								
    								// Conduct Score
    								$thisMeritRecordConductScore = $thisMeritRecord["GMConductScoreChange"]? $thisMeritRecord["GMConductScoreChange"] : $thisMeritRecord["ConductScoreChange"];
    								$thisMeritRecordConductScore = abs($thisMeritRecordConductScore);
    								
    								// Remarks
    								$thisMeritRecordRemarks = $thisMeritRecord["Remark"]? $thisMeritRecord["Remark"] : "---";
    								
    								// Creator
    								$thisMeritRecordCreatedID = $thisMeritRecord["CreatedBy"];
    								$thisMeritRecordCreatedName = $user_list[$thisMeritRecordCreatedID]["StaffName"];
    								$thisMeritRecordCreatedName = $thisMeritRecordCreatedName? $thisMeritRecordCreatedName : "---";
    								
    								// PIC
    								$thisMeritRecordPICName = array();
    								$thisMeritRecordPICID = explode(",", $thisMeritRecord["PICID"]);
    								foreach($thisMeritRecordPICID as $thisPICID) {
    									$thisPICName = trim($user_list[$thisPICID]["StaffName"]);
    									if($thisPICName != "")
    										$thisMeritRecordPICName[] = $thisPICName;
    								}
    								$thisMeritRecordPICName = implode('、', $thisMeritRecordPICName);
    								
    								# Demerit Record Table Content
    								$x .= " <tr>
    											<td>".$thisMeritRecord["RecordDate"]."</td>
    											<td>".$thisMeritRecord["InputRecordDate"]."</td>
    											<td>".$thisMeritRecord["ItemName"]."</td>
    											<td>".$thisMeritRecordPICName."</td>
    											<td>".$thisMeritRecordCreatedName."</td>
    											<td>".$thisMeritRecordConductScore." 扣分</td>
    											<td>".$thisMeritRecordRemarks."</td>
    										</tr>";

                                    ### Page break predict (by row count) [START]
                                    ### Item Name row count
                                    // handle string lang
    								$item_eng_str = "";
    								$item_chi_str = $thisMeritRecord["ItemName"];
    								if($thisMeritRecordType == "GM")
    								{
    									$item_eng_str = substr_lang($item_chi_str, "ENG");
    									$item_chi_str = substr_lang($item_chi_str, "CHI");
    								}
                                    $item_en_string_length = countString($item_eng_str);
                                    $item_ch_string_length = countString($item_chi_str);

                                    // predict row count
                                    $item_string_length = $item_ch_string_length;
    								if($item_en_string_length > 0) {
    									$item_string_length += ceil($item_en_string_length / 2);
    								}
    								// $row_count += ceil($item_string_length / 12);
                                    $item_row_count = ceil($item_string_length / 12);

                                    ### Remarks row count
                                    // handle string lang
                                    $remarks_eng_str = substr_lang($thisMeritRecordRemarks, "ENG");
                                    $remarks_chi_str = substr_lang($thisMeritRecordRemarks, "CHI");
                                    $remarks_en_string_length = countString($remarks_eng_str);
                                    $remarks_ch_string_length = countString($remarks_chi_str);

                                    // predict row count
                                    $remarks_string_length = $remarks_ch_string_length;
                                    if($remarks_en_string_length > 0) {
                                        $remarks_string_length += ceil($remarks_en_string_length / 1.5);
                                    }
                                    $remakrs_row_count = ceil($remarks_string_length / 9);

                                    // apply row count - Item Name or Remarks
                                    $row_count += $remakrs_row_count > $item_row_count ? $remakrs_row_count : $item_row_count;
                                    ### Page break predict (by row count) [END]
    							}
    						}
    					}
    					else
                        {
    						$x .= " <tr><td colspan='7' style='text-align: center'>$no_record_msg</td></tr>";
    					}
    					$x .= "</table>";
    					$x .= "</div>";
					}
				}
			}
		}
		
		if($isFirstReport)
		{
			$x .= "	<table class='report_header' cellspacing='0' cellpadding='5' border='0' width='100%'>
						<tr>
							<td style='border-left: 0px solid black; font-size: 20px;' align='center'>$no_record_msg</td>
						</tr>
					</table>";
		}
		
		$x .= "</div>";
		return $x;
	}
	
	function getGeneratedNoticeLogSelection($AcademicYearID, $SemesterID, $textFromDate, $textToDate)
	{
	    global $Lang, $ldiscipline;
	    
	    $generateLogArr = $ldiscipline->getGeneratedNoticeLog($AcademicYearID, $SemesterID, $textFromDate, $textToDate, '', $forSelection=true);
	    if(!empty($generateLogArr)) {
	        $returnStr = getSelectByArray($generateLogArr, " name='GenerateLogID' id='GenerateLogID' ", "", 0, 1);
	    } else {
	        $returnStr = $Lang['General']['NoRecordFound'];
	    }
	    return $returnStr;
	}
	
	function getHYKDemeritLinkedNoticeListTable()
	{
	    global $PATH_WRT_ROOT, $linterface, $ldiscipline;
	    global $Lang, $i_Notice_Signed, $i_Notice_Unsigned, $i_Notice_DateStart, $i_identity_student, $i_Notice_Title;
	    
	    # POST data
	    $yearID = $_POST['selectYear'];
	    $termID = $_POST['selectSemester'];
	    $StartDate = $_POST['textFromDate'];
	    $EndDate = $_POST['textToDate'];
	    $targetType = $_POST['rankTarget'];
	    $targetIDArr = $_POST['rankTargetDetail'];
	    $studentIDArr = $_POST['studentID'];
	    
	    // Init - Get class student list
	    include_once($PATH_WRT_ROOT."includes/libclass.php");
	    $lclass = new libclass();
	    
	    // Init - Get table checkbox
	    include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	    include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
	    $li = new libdbtable2007(0, 0, 0);
	    
	    // Use time range as eNotice using datetime
	    $StartDate = $StartDate != ""? $StartDate : date("Y-m-d");
	    $StartDateTime = $StartDate." 00:00:00";
	    $EndDate = $EndDate != ""? $EndDate : date("Y-m-d");
	    $EndDateTime = $EndDate." 23:59:59";
	    $TermStartDate = getStartDateOfAcademicYear($yearID, $termID);
	    $TermEndDate = getEndDateOfAcademicYear($yearID, $termID);
	    
	    // Condition
	    if($targetType == 'student') {
	        $student_cond = " AND c.UserID IN (".implode(',', (array)$studentIDArr).")";
	    }
	    else {
	        $classTargetIDArr = $targetIDArr;
	        if($targetType == 'class') {
	            foreach((array)$classTargetIDArr as $thisIndex => $thisTargetID) {
	                $classTargetIDArr[$thisIndex] = '::'.$thisTargetID;
	            }
	        }
	        
	        $studentIDArr = array();
	        foreach((array)$classTargetIDArr as $thisTargetID) {
	            $thisStudentIDArr = $lclass->storeStudent(0, $thisTargetID);
	            $studentIDArr = array_merge($studentIDArr, $thisStudentIDArr);
	        }
	        
	        $student_cond = '';
	        if(count($studentIDArr) > 0) {
	            $student_cond = " AND c.UserID IN (".implode(',', $studentIDArr).")";
	        }
	    }
	    
	    $name_field = getNameFieldWithClassNumberByLang("c.");
	    $sql = "SELECT
            	    LEFT(a.DateStart, 10),
            	    $name_field as studentname,
	                a.Title,
                    IF(b.RecordType IS NOT NULL, '".$i_Notice_Signed."', '".$i_Notice_Unsigned."'),
    				IF(b.PrintDate IS NOT NULL, b.PrintDate, '". $Lang['eNotice']['NonPrint'] ."'),
    				a.NoticeID
				FROM
					INTRANET_NOTICE as a
					INNER JOIN INTRANET_NOTICE_REPLY as b ON (b.NoticeID = a.NoticeID)
                    INNER JOIN INTRANET_USER as c ON (c.UserID = b.StudentID)
                    LEFT JOIN DISCIPLINE_HYK_NOTICE hyk_a ON (hyk_a.NoticeID = a.NoticeID)
				WHERE
					a.DateStart >= '".$StartDateTime."' AND a.DateStart <= '".$EndDateTime."' AND
					a.DateStart >= '".$TermStartDate."' AND a.DateStart <= '".$TermEndDate."' AND
                    a.Module = '".$ldiscipline->Module."' AND a.isModule = 1 AND
                    hyk_a.NoticeID IS NULL
                    $student_cond
                ORDER BY
                    a.DateStart DESC";
        $records = $ldiscipline->returnArray($sql);
        
        // Table Columns
        $x = '';
        $x .= '<div>';
        $x .= '<table class="common_table_list_v30" id="ContentTable" width="90%">';
        $x .= '<thead>';
            $x .= '<tr>';
                $x .= '<th style="width:3%;">#</th>';
                $x .= '<th style="width:12%;">'.$i_Notice_DateStart."</th>\n";
                $x .= '<th style="width:28%;">'.$i_identity_student."</th>\n";
                $x .= '<th style="width:26%;">'.$i_Notice_Title."</th>\n";
                $x .= '<th style="width:8%;">'.$i_Notice_Signed."/".$i_Notice_Unsigned."</th>\n";
                $x .= '<th style="width:20%;">'.$Lang['eNotice']['PrintDate']."</th>\n";
                $x .= '<th style="width:3%;">'.$li->check("NoticeIDArr[]")."</th>\n";
            $x .= '</tr>';
        $x .= '</thead>';
        
        // Table Content
        $x .= '<tbody>';
        if(!empty($records))
        {
            foreach((array)$records as $this_index => $this_record)
            {
                $x .= '<tr>';
                    $x .= '<td>'.($this_index + 1).'</td>';
                    $x .= '<td>'.$this_record[0].'</td>';
                    $x .= '<td>'.$this_record[1].'</td>';
                    $x .= '<td>'.$this_record[2].'</td>';
                    $x .= '<td>'.$this_record[3].'</td>';
                    $x .= '<td>'.$this_record[4].'</td>';
                    $x .= $li->displayCell(5,"<input type='checkbox' name='NoticeIDArr[]' value='". $this_record[5]."'>", "tablelink","valign='top'")."\n";
                $x .= '</tr>';
            }
        }
        else
        {
            $x .= '<tr>';
                $x .= '<td colspan="7" align="center">'.$Lang['General']['NoRecordFound'].'</td>';
            $x .= '</tr>';
        }
        $x .= '</tbody>';
        $x .= '</table>';
        
        $x .= $linterface->Get_Form_Warning_Msg("NoticeWarnDiv","");
        $x .= '</div>';
        
        // Print / Export button
        if(!empty($records))
        {
            $x .= '<div class="edit_bottom_v30">';
                $x .= '<p class="spacer"></p>';
                    $x .= $this->GET_ACTION_BTN($Lang['Btn']['Print'], "button", "submitForm('print')");
                    $x .= '&nbsp';
                    $x .= $this->GET_ACTION_BTN($Lang['eDiscipline']['HYKClassSummaryReport']['ExportToWord'], "button", "submitForm('word')");
                $x .= '<p class="spacer"></p>';
            $x .= '</div>';
            
            $x .= '<input type="hidden" name="periodYear" value="'.$yearID.'" />';
            $x .= '<input type="hidden" name="periodSemester" value="'.$termID.'" />';
            $x .= '<input type="hidden" name="periodStart" value="'.$StartDate.'" />';
            $x .= '<input type="hidden" name="periodEnd" value="'.$EndDate.'" />';
            $x .= '<input type="hidden" name="searchType" value="'.$targetType.'" />';
            foreach((array)$targetIDArr as $thisTargetID) {
                $x .= '<input type="hidden" name="searchTargetIDArr[]" value="'.$thisTargetID.'" />';
            }
            if($targetType == 'student') {
                foreach((array)$studentIDArr as $thisStudentID) {
                    $x .= '<input type="hidden" name="searchStudentIDArr[]" value="'.$thisStudentID.'" />';
                }
            }
        }
        
        return $x;
	}
	// [NT Heung Yee Kuk Tai Po District Secondary School] - End
	
	// [Tung Wah Group Of Hospitals Wong Fut Nam College] - Start
	function getWFNDetentionReport($dataAry)
	{
		global $Lang, $ldiscipline, $no_record_msg;
		
		# POST data
		$targetDate = date("j/n/Y", strtotime($dataAry["textDate"])); 
		$targetYearID = $dataAry["selectYear"]; 
		$studentType = $dataAry["targetType"];
		$classAry = $dataAry["rankTargetDetail"];
		if($studentType=="school")
			$classAry = array(1);
		
		$x = '';
		$x .= '<table width="100%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$this->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
				</tr>
			  </table>'."\n";
		
		$isFirstReport = true;
		$reportCount = 0;
		
		$x .= '<div class="main_container">';
		
		// loop Classes
		$detentionRecords = $ldiscipline->getStudentDetentionByDate($dataAry);
    	foreach($classAry as $thisClassID)
    	{
    		$detentionClasses = (array)$detentionRecords[$thisClassID];
		    $detentionClassCount = count($detentionClasses);
	    	if($detentionClassCount > 0)
	    	{
				// loop Locations
				foreach($detentionClasses as $detentionLocation => $detentionStudents)
				{
					 # Report Header
					$ClassTitle = $Lang['eDiscipline']['TWGHWFNS_StudentDetention']['ClassTitle'];
					if($studentType=="school")
						$ClassTitle = $Lang['eDiscipline']['TWGHWFNS_StudentDetention']['FormTitle'];
					$ClassTitle = str_replace("<!--detentionDate-->", $targetDate, $ClassTitle);
					$ClassTitle = str_replace("<!--detentionLocation-->", $detentionLocation, $ClassTitle);
					
					# Class Info
					if($studentType=="class")
					{
						reset($detentionStudents);
						$firstStudent = current($detentionStudents);
						$studentClassName = $firstStudent["ClassTitle"];
						$ClassTitle = str_replace("<!--studentClass-->", $studentClassName, $ClassTitle);
						$studentClassTeacher = $ldiscipline->getStudentAllClassTeacherName($firstStudent['StudentID'], $targetYearID);
						if(sizeof($studentClassTeacher) > 0)
							$studentClassTeacher = implode("、", $studentClassTeacher);
						else
							$studentClassTeacher = "---";
					}
					
					# Page break
		    		if(!$isFirstReport)
		    		{
		    			if($reportCount % 3 == 0) {
	   						$x .= '</div>'."\n";
							$x .= '<div style="page-break-after:always">&nbsp;</div>';
							$x .= '<div class="main_container">';
		    			}
						else {
							$x .= '<br>';
							$x .= '<br>';
							$x .= '<br>';
						}
		    		}
					
		    		# Table Header
					$x .= '<table class="report_header" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">';
							$x .= '<tr><td class="header_title_1">'.$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['SchoolName'].'</td></tr>';
							$x .= '<tr><td class="header_title_2">'.$ClassTitle.'</td></tr>';
							if($studentType!="school")
								$x .= '<tr><td class="header_title_2">'.$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['ClassTeacher'].': '.$studentClassTeacher.'</td></tr>';
					$x .= '</table>'."\n";
					$x .= '<hr />'."\n";
					$x .= '<table class="report_table" width="100%" align="center">'."\n";
					$x .= '<thead>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<th width="25%">'.($studentType!="school"? "&nbsp;" : $Lang['General']['Class']).'</th>'."\n";
				            $x .= '<th width="25%">'.$Lang['General']['ClassNumber'].'</th>'."\n";
				            $x .= '<th width="50%">'.$Lang['General']['Name'].'</th>'."\n";
				        $x .= '</tr>'."\n";
				    $x .= '</thead>'."\n";
				    $x .= '<tbody>'."\n";
				    
				    // loop Students
			    	foreach((array)$detentionStudents as $studentInfo) {
			    		$x .= '<tr>';
			    			$x .= '<td>'.($studentType!="school"? "&nbsp;" : $studentInfo['ClassTitle']).'</td>';
			    			$x .= '<td>'.$studentInfo['ClassNumber'].'</td>';
			    			$x .= '<td style="text-align:left">'.$studentInfo['StudentName'].'</td>';
			    		$x .= '</tr>'."\n";
			    	}
			    	
	    			$x .= '</tbody>'."\n";
	    		    $x .= '</table>'."\n";
    		    
    		   		$isFirstReport = false;
    		   		$reportCount++;
		    	}
	    	}
		}
		
		if($isFirstReport)
		{
			$x .= "<table class='report_table' cellspacing='0' cellpadding='2' width='97%' align='center'>";
				$x .= "<tr>";
					$x .= "<td style='border-left:0px solid black' align='center'>$no_record_msg</td>";
				$x .= "</tr>";
			$x .= "</table>";
		}
		
	   	$x .= '</div>'."\n";
	    		    
	    return $x;
	}
	
	function getWFNConductPunishmentReport($dataAry)
	{
		global $PATH_WRT_ROOT, $intranet_root, $intranet_session_language, $Lang, $sys_custom, $no_record_msg;
		global $ldiscipline;
		
		# Include library
		include_once("form_class_manage.php");
		
		# POST data
		$targetYearID = $dataAry['selectYear'];
		$targetSemester = $dataAry['selectSemester'];
		$targetClasses = $dataAry['rankTargetDetail'];
		$targetEndDate = date("j/n/Y", strtotime($dataAry['textToDate']));
		
		# Year and Semester Info		
		$targetYearName = getAcademicYearByAcademicYearID($targetYearID, "b5");
		$SemesterAry = (array)getSemesters($targetYearID, 1, "b5");
		$targetSemesterName = $targetSemester==0? $Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['WholeYear'] : $SemesterAry[$targetSemester];
		
		# Get summary data
		$StudentDataAry = $ldiscipline->getStudentConductPunishmentSummaryData($dataAry);
		$StudentList = $StudentDataAry["StudentInfo"];
		$StudentConductList = $StudentDataAry["ConductRecord"];
		$StudentPunishmentList = $StudentDataAry["PunishmentRecord"];
		$StudentPunishmentTypeList = $StudentDataAry["PunishmentRecordType"];

		# Page Height Default Settings
		$x = '';
		$isFirstReport = true;
		$ReportPageCount = 1;
		$PageHeight = 1080;
		$FirstPageHeader = 196;
		$OtherPageHeader = 46;
		$CountRow = 22;
		$APDetailsRow = 18; 
		$APDetailsRowBase = 10;
		
		// loop classes
		foreach((array)$targetClasses as $thisClassID)
		{
			# Get Class and Class Teacher
			$yc = new year_class($thisClassID);
			$targetClassName = $yc->ClassTitleB5;
			$targetClassTeacher = $ldiscipline->getStudentAllClassTeacherName("", $targetYearID, $thisClassID);
			$targetClassTeacher = implode("、", (array)$targetClassTeacher);
			
			# Page break Handling
			if(!$isFirstReport) {
				// loop to add page break
				for($i=0; $i<$ReportPageCount; $i++)
					$x .= "<div style='page-break-after:always'>&nbsp;</div>";
				$ReportPageCount = 1;
			}
			
			# Reset Height Count
			$thisHeight = ($PageHeight - $FirstPageHeader);
			
			# Report Header
			$x .= "<div class='main_container'>";
			$x .= "<table class='report_header' cellspacing='0' cellpadding='2' width='100%'>";
			$x .= "<thead>";
				$x .= "<tr>";
					$x .= "<th class='header_title_1' colspan='4'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['SchoolName']."</th>";
				$x .= "</tr>";
				$x .= "<tr>";
					$x .= "<th class='header_title_2' width='20%'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['EndDate']."&nbsp;&nbsp;&nbsp;&nbsp;<span class='titleSpecifiy'>".$targetEndDate."</span></th>";
					$x .= "<th class='header_title_2' colspan='2' width='50%'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['ReportTitle'].": </th>";
					$x .= "<th class='header_title_2' width='30%'>(".$targetYearName.$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['SchoolYear']."&nbsp;&nbsp;".$targetSemesterName.")</th>";
				$x .= "</tr>";	
				$x .= "<tr>";
					$x .= "<th class='header_title_2 borderSpecifiy' colspan='2' valign='top'><span class='titleSpecifiy'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['ClassName'].": ".$targetClassName."</span></th>";
					$x .= "<th class='header_title_2 borderSpecifiy' colspan='2'><span class='titleSpecifiy'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['ClassTeacher'].": ".$targetClassTeacher."</span></th>";
				$x .= "</tr>";
			$x .= "</thead>";
			$x .= "<tbody>";
			$x .= "</tbody>";
			$x .= "</table>";
			
			# Table Header
			$x .= "<table class='report_table' cellspacing='0' cellpadding='2' width='97%' align='right'>";
			$x .= "<thead>";
				$x .= "<tr>";
					$x .= "<th style='border-left:0px solid black' width='5%'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['ClassNumber']."</th>";
					$x .= "<th style='border-left:0px solid black' width='1%'>&nbsp;</th>";
					$x .= "<th style='border-left:0px solid black; text-align:left' width='15%'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['StudentName']."</th>";
					$x .= "<th style='border-left:0px solid black' colspan='2'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['HWLateSubmission']."</th>";
					$x .= "<th style='border-left:0px solid black' width='5%'>&nbsp;</th>";
					$x .= "<th style='border-left:0px solid black' colspan='2'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['Late']."</th>";
					$x .= "<th style='border-left:0px solid black' width='5%'>&nbsp;</th>";
					$x .= "<th style='border-left:0px solid black' colspan='4'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['Punishment']."</th>";
				$x .= "</tr>";
				$x .= "<tr>";
					$x .= "<th style='border-left:0px solid black'>&nbsp;</th>";
					$x .= "<th style='border-left:0px solid black'>&nbsp;</th>";
					$x .= "<th style='border-left:0px solid black'>&nbsp;</th>";
					$x .= "<th style='border-left:0px solid black' width='5%'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['Times']."</th>";
					$x .= "<th style='border-left:0px solid black' width='5%'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][-1]."</th>";
					$x .= "<th style='border-left:0px solid black'>&nbsp;</th>";
					$x .= "<th style='border-left:0px solid black' width='5%'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['Times']."</th>";
					$x .= "<th style='border-left:0px solid black' width='5%'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][-1]."</th>";
					$x .= "<th style='border-left:0px solid black'>&nbsp;</th>";
					$x .= "<th style='border-left:0px solid black' width='5%'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][0]."</th>";
					$x .= "<th style='border-left:0px solid black' width='5%'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][-1]."</th>";
					$x .= "<th style='border-left:0px solid black' width='5%'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][-2]."</th>";
					$x .= "<th style='border-left:0px solid black' width='5%'>".$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][-3]."</th>";
				$x .= "</tr>";
			$x .= "</thead>";
			$x .= "<tbody>";
			
			// loop students
			$ClassStudentList = $StudentList[$thisClassID];
			if(count($ClassStudentList) > 0){
				foreach((array)$ClassStudentList as $thisStudentID => $thisStudentInfo)
				{
					# Student Info
					$ClassNumber = $thisStudentInfo['ClassNumber'];
					$StudentName = $thisStudentInfo['StudentName'];
					
					# Homework Late Submission misconduct
					$StudentHWSubmitConduct = (array)$StudentConductList[$thisStudentID][$sys_custom['eDiscipline']['BWWTC_WithoutSubmCatID']];
					$StudentHWSubmitUpgradeCount = 0;
					$StudentHWSubmitConductTotal = 0;
					if(count($StudentHWSubmitConduct) > 0)
					{
						// loop misconduct
						foreach($StudentHWSubmitConduct as $thisUpgradeAP => $thisHWSubmitConduct)
						{
							$StudentHWSubmitConductTotal += $thisHWSubmitConduct["GMRecordCount"];
							if($thisUpgradeAP > 0)
								$StudentHWSubmitUpgradeCount += (int)$thisHWSubmitConduct["UpgradeRecordCount"];
						}
					}
					
					# Late misconconduct
					$StudentLateConduct = (array)$StudentConductList[$thisStudentID][PRESET_CATEGORY_LATE];
					$StudentLateUpgradeCount = 0;
					$StudentLateConductTotal = 0;
					if(count($StudentLateConduct) > 0)
					{
						// loop misconduct
						foreach($StudentLateConduct as $thisUpgradeAP => $thisLateConduct)
						{
							$StudentLateConductTotal += $thisLateConduct["GMRecordCount"];
							if($thisUpgradeAP > 0)
								$StudentLateUpgradeCount += (int)$thisLateConduct["UpgradeRecordCount"];
						}
					}
					
					# Punishment (grouped by type)
					$StudentPunishmentCount = array();
					$StudentPunishmentCount["0"] = 0;
					$StudentPunishmentCount["-1"] = 0;
					$StudentPunishmentCount["-2"] = 0;
					$StudentPunishmentCount["-3"] = 0;
					$StudentPunishmentRecord = (array)$StudentPunishmentTypeList[$thisStudentID];
					if(count($StudentPunishmentRecord) > 0)
					{
						// loop punishement type
						foreach($StudentPunishmentCount as $thisPunishmentType => $thisPunishmentNumber)
						{
							$thisPunishmentTypeRecord = (array)$StudentPunishmentRecord[$thisPunishmentType];
							if(count($thisPunishmentTypeRecord) > 0)
							{
								// loop punishment
								foreach($thisPunishmentTypeRecord as $thisPunishmentData)
									$StudentPunishmentCount[$thisPunishmentType] += $thisPunishmentData["MeritRecordCount"];
							}
						}
					}
					
					# Punishment records
					$StudentPunishmentRecords = (array)$StudentPunishmentList[$thisStudentID];
					
					# Table Row
					$x .= "<tr>";
						$x .= "<td style='border-left:0px solid black'>".$ClassNumber."</td>";
						$x .= "<td style='border-left:0px solid black'>&nbsp;</td>";
						$x .= "<td style='border-left:0px solid black; text-align:left'>".$StudentName."</td>";
						$x .= "<td style='border-left:0px solid black'>".$StudentHWSubmitConductTotal."</td>";
						$x .= "<td style='border-left:0px solid black'>".$StudentHWSubmitUpgradeCount."</td>";
						$x .= "<th style='border-left:0px solid black'>&nbsp;</td>";
						$x .= "<td style='border-left:0px solid black'>".$StudentLateConductTotal."</td>";
						$x .= "<td style='border-left:0px solid black'>".$StudentLateUpgradeCount."</td>";
						$x .= "<td style='border-left:0px solid black'>&nbsp;</td>";
						$x .= "<td style='border-left:0px solid black'>".$StudentPunishmentCount["0"]."</td>";
						$x .= "<td style='border-left:0px solid black'>".$StudentPunishmentCount["-1"]."</td>";
						$x .= "<td style='border-left:0px solid black'>".$StudentPunishmentCount["-2"]."</td>";
						$x .= "<td style='border-left:0px solid black'>".$StudentPunishmentCount["-3"]."</td>";
					$x .= "</tr>";
					
					# Punishment Row
					if(count($StudentPunishmentRecords) > 0)
					{
						$x .= "<tr>";
							$x .= "<td style='border-left:0px solid black'>&nbsp;</td>";
							$x .= "<td style='border-left:0px solid black'>&nbsp;</td>";
							$x .= "<td style='border-left:0px solid black'>&nbsp;</td>";
							$x .= "<td style='border-left:0px solid black' colspan='10'>";
							$x .= "<table class='report_table_details' cellspacing='0' cellpadding='2' width='97%' align='right'>";
							
							// loop punishment
							foreach($StudentPunishmentRecords as $thisPunishmentInfo)
							{
								// Merit number display
								$thisMeritType = $thisPunishmentInfo["ProfileMeritType"];
								$thisMeritDisplay = $Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][$thisMeritType];
								$thisMeritDisplay .= $thisPunishmentInfo["MeritRecordCount"];
								$thisMeritDisplay .= $Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['Time'];
								
								$x .= "<tr>";
									$x .= "<td style='border-left:0px solid black' width='20%'>".date("j/n/Y", strtotime($thisPunishmentInfo["RecordDate"]))."</td>";
									$x .= "<td style='border-left:0px solid black' width='65%'>".$thisPunishmentInfo["ItemName"]."</td>";
									$x .= "<td style='border-left:0px solid black' width='15%'>".$thisMeritDisplay."</td>";
								$x .= "</tr>";
								
								# Update Height Count
								if($thisHeight < $OtherPageHeader) {
									$thisHeight = ($PageHeight - $OtherPageHeader);
									$ReportPageCount++;
								}
								$thisHeight -= $APDetailsRow;
							}
							$x .= "</table>";
							$x .= "</td>";
						$x .= "</tr>";
						
						# Update Height Count
						$thisHeight -= $APDetailsRowBase;
					}
					
					# Update Height Count
					if($thisHeight < $OtherPageHeader) {
						$thisHeight = ($PageHeight - $OtherPageHeader);
						$ReportPageCount++;
					}
					$thisHeight -= $CountRow;
				}
			}
			else
			{
				$x .= "<tr>";
					$x .= "<td style='border-left:0px solid black' colspan='13'>$no_record_msg</td>";
				$x .= "</tr>";
			}
			$x .= "</tbody>";
			$x .= "</table>";
			$x .= "</div>";
//			$x .= '<div style="page-break-after:always;"></div>';
//			$x .= '<div style="page-break-after:always;"></div>';
//			$x .= '<div style="page-break-after:always;"></div>';
//			$x .= '<div style="page-break-after:always;"></div>';
			
			$isFirstReport = false;
		}
		return $x;
	}
	// [Tung Wah Group Of Hospitals Wong Fut Nam College] - End
	
	// [HKSYC & IA Chan Nam Chong Memorial College] Start
	function getStudentLateMeritReport($dataAry)
	{
		global $PATH_WRT_ROOT, $intranet_root, $intranet_session_language, $Lang, $sys_custom, $no_record_msg;
		global $ldiscipline;
		
		# POST data
		$targetClasses = $dataAry['rankTargetDetail'];
		$targetYearID = $dataAry['selectYear'];
		$targetEndDate = date("Y-m-d", strtotime($dataAry['textToDate']));
		
		# Get summary data
		$StudentDataAry = $ldiscipline->getStudentUpgradedLateDemeritRecord($dataAry);
		$StudentList = $StudentDataAry["StudentInfo"];
		$StudentPunishmentList = $StudentDataAry["PunishmentRecord"];

		# loop classes
		$x = '';
		$isFirstReport = true;
		foreach((array)$StudentList as $thisYearClassID => $thisClassStudent)
		{
			# Get Class Teacher
			$thisClassTeacher = $ldiscipline->getStudentAllClassTeacherName("", $targetYearID, $thisYearClassID);
			$thisClassTeacher = implode("、", (array)$thisClassTeacher);
			
			# loop students
			foreach((array)$thisClassStudent as $thisStudentID => $thisStudentInfo)
			{
				# Get punishment
				$thisStudentPunishment = $StudentPunishmentList[$thisStudentID];
				
				# skip this student if without punishment
				if(empty($thisStudentPunishment))	continue;
				
				# Get student info
				$thisStudentName = $thisStudentInfo["StudentName"];
				$thisClassName = $thisStudentInfo["ClassName"];
				$thisClassNumber = $thisStudentInfo["ClassNumber"];
					
				# loop punishment
				foreach((array)$thisStudentPunishment as $thisRelatedGMRecords)
				{
					# skip this punishment if no related misconduct
					if(empty($thisRelatedGMRecords))	continue;
					
					# Get target misconduct info 
					$targetGMRecord = reset($thisRelatedGMRecords);
					$thisGMRecordDate = $targetGMRecord["RecordDate"];
					$thisGMRecordTime = $targetGMRecord["Remark"];
					$thisGMUpgradeCount = ((int)$targetGMRecord["CountNo"] * 3);
					
					# skip if upgrade count is zero
					if($thisGMUpgradeCount==0)	continue;
					
					# Page break
					if(!$isFirstReport)
						$x .= "<div style='page-break-after:always'>&nbsp;</div>";
					
					# Report Header
					$x .= "<div class='main_container'>";
						$x .= "<table class='report_header' cellspacing='0' cellpadding='2' width='100%'>";
							$x .= "<tr>";
								$x .= "<td width='20%'>";
									$x .= "<img src='/file/discipline/cnc_main_logo.png' width='90px'>";
								$x .= "</td>";
								$x .= "<td width='80%' style='line-height:30px'>";
									$x .= "香 港 四 邑 商 工 總 會 陳 南 昌 紀 念 中 學";
									$x .= "<br>";
									$x .= "遲到紀錄紙（翌日請交回班主任）";
								$x .= "</td>";
							$x .= "</tr>";
						$x .= "</table>";
						
						# Report Content (with auto fill-in)
						$x .= "<table class='report_table' cellspacing='0' cellpadding='2' width='100%'>";
							$x .= "<tr>";
								$x .= "<td width='7%'>姓名 : </td>";
								$x .= "<td width='40%' class='data_td'>".$thisStudentName."</td>";
								$x .= "<td width='10%'>&nbsp;</td>";
								$x .= "<td width='7%'>班別 :</td>";
								$x .= "<td width='15%' class='data_td'>".$thisClassName."</td>";
								$x .= "<td width='1%'>&nbsp;</td>";
								$x .= "<td width='7%'>學號 : </td>";
								$x .= "<td width='13%' class='data_td'>".$thisClassNumber."</td>";
							$x .= "</tr>";
						$x .= "</table>";
						$x .= "<table class='report_table' cellspacing='0' cellpadding='2' width='100%'>";
							$x .= "<tr>";
								$x .= "<td width='7%'>日期 : </td>";
								$x .= "<td width='40%' class='data_td'>".$thisGMRecordDate."</td>";
								$x .= "<td width='10%'>&nbsp;</td>";
								$x .= "<td width='11%'>到校時間 : </td>";
								$x .= "<td width='32%' class='data_td'>".$thisGMRecordTime."</td>";
							$x .= "</tr>";
						$x .= "</table>";
						$x .= "<table class='report_table' cellspacing='0' cellpadding='2' width='100%'>";
							$x .= "<tr>";
								$x .= "<td width='16%'>累計遲到次數 : </td>";
								$x .= "<td width='31%' class='data_td'>".$thisGMUpgradeCount."</td>";
								$x .= "<td width='10%'>&nbsp;</td>";
								$x .= "<td width='9%'>班主住 : </td>";
								$x .= "<td width='34%' class='data_td'>".$thisClassTeacher."</td>";
							$x .= "</tr>";
						$x .= "</table>";
						
						# Report Content (static)
						$x .= "<table class='report_static' cellspacing='0' cellpadding='2' width='100%'>";
							$x .= "<tr>";
								$x .= "<td width='100%' class='remainder'>＊凡遲到達第三、六及九次各記一缺點，以後遲到記錄每累積三次，均配一小過。</td>";
							$x .= "</tr>";
							$x .= "<tr>";
								$x .= "<td width='100%'>處理方法：　口　須記缺點　　　　口　須記小過</td>";
							$x .= "</tr>";
							$x .= "<tr>";
								$x .= "<td width='100%' class='remainder'>（　如需處分，此紙亦是學生紀錄處分紀錄表，並會蓋上訓導印章及即時發出。）</td>";
							$x .= "</tr>";
						$x .= "</table>";
						$x .= "<table class='report_static2' cellspacing='0' cellpadding='2' width='100%'>";
							$x .= "<tr>";
								$x .= "<td width='50%'>上課室時間：_______________________</td>";
								$x .= "<td width='50%'>當值老師簽署：________________________</td>";
							$x .= "</tr>";
							$x .= "<tr>";
								$x .= "<td>到課室時間：_______________________</td>";
								$x .= "<td>上課老師簽署：<u>第（　）節</u>：_____________</td>";
							$x .= "</tr>";
							$x .= "<tr>";
								$x .= "<td>家長簽署　：_______________________</td>";
								$x .= "<td>日　　期：________________________</td>";
							$x .= "</tr>";
							$x .= "<tr>";
								$x .= "<td>班主任簽署：_______________________</td>";
								$x .= "<td>收紙日期：________________________</td>";
							$x .= "</tr>";
							$x .= "<tr>";
								$x .= "<td colspan='2' class='remainder'>（*班主任必須收回、存檔）</td>";
							$x .= "</tr>";
						$x .= "</table>";
					$x .= "</div>";
					
					$isFirstReport = false;
				}
			}
		}
		
		# No valid data
		if($isFirstReport)
		{
			global $i_no_record_exists_msg;
			
			$x .= "<div align='center' style='font-size:20px;'>";
			$x .= "$i_no_record_exists_msg";
			$x .= "</div>";
		}
		
		return $x;
	}
	// [HKSYC & IA Chan Nam Chong Memorial College] End
	
	# [Tsuen Wan Public Ho Chuen Yiu Memorial College] - $sys_custom['eDiscipline']['HCY_ClassRecordCountReport']
	function getHCYClassRecordSummary($format, $target_ary, $student_ary, $gm_records, $ap_records, $ap_record_count, $absent_records, $header_row='')
	{
	    global $PATH_WRT_ROOT, $no_record_msg;
	    
	    # Init vars
	    $x = '';
	    $rows = array();
	    $target_num = 0;
	    $page_row = 56;
	    
	    # Init libclass obj
	    include_once ("libclass.php");
	    $lclass = new libclass();
	    
	    if($format == 'print')
	    {
    	    # Table Columns row
    	    $table_cols = " <tr>
                                <td width='4%'>班別</td>
                                <td width='4%'>學號</td>
                                <td width='8%'>學生姓名</td>
                                <td width='4%'>操行分</td>
                                <td width='4%'>參考等級</td>
                                <td width='4%'>操行等級</td>
                                <td width='4%'>欠功課</td>
                                <td width='4%'>欠用品</td>
                                <td width='4%'>違規</td>
                                <td width='4%'>遲到</td>
                                <td width='4%'>校服違規</td>
                                <td width='4%'>嘉許</td>
                                <td width='4%'>遲入課室</td>
                                <td width='4%'>醫療室</td>
                                <td width='4%'>不潔記錄</td>
                                <td width='4%'>缺點</td>
                                <td width='4%'>小過</td>
                                <td width='4%'>大過</td>
                                <td width='4%'>缺席日數</td>
                                <td width='20%'>處分記錄</td>
                            </tr>";
    	    
    	    # Page Break div
    	    $report_pagebreak = "</table>
                            </div>
                            <div style='page-break-after:always;'></div>";
	    }
	    
	    // loop targets
	    $target_count = count((array)$target_ary);
	    foreach((array)$target_ary as $class_id)
	    {
	        // Get Class Name
	        $class_name = $lclass->getClassName($class_id);
	        
	        $report_header = '';
    	    if($format == 'print')
    	    {
    	        # Class Name Row
    	        $class_name_row = "<tr><td>$class_name</td></tr>";
    	        
    	        # Report & Class Table Header
    	        $report_header = "  <div id='container'>
                                        <table class='report_header' cellspacing='0' cellpadding='0' border='0' width='100%'>
                                            $header_row
                                            $class_name_row
                                        </table>
                                        <table class='result_table' cellspacing='0' cellpadding='2' width='100%'>
                                            $table_cols ";
    	        $x .= $report_header;
    	    }
    	    else
    	    {
    	        $rows[] = array();
    	        $rows[] = array("班別 (".$class_name.")");
    	        $rows[] = array("班別", "學號", "學生姓名", "操行分", "參考等級", "操行等級", "欠功課", "欠用品", "違規", "遲到", "校服違規", "嘉許", "遲入課室", "醫療室", "不潔記錄", "缺點", "小過", "大過", "缺席日數", "處分記錄");
    	    }
            
    	    // Get Class Student
    	    $student_list = $student_ary[$class_id];
    	    if(count($student_list) > 0)
    	    {
    	        // loop students
    	        $row_count = 0;
    	        $total_data_count = array("", "", "總數", "", "", "", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "");
    	        foreach((array)$student_list as $currentStudent)
    	        {
    	            # Student Info
    	            $student_id = $currentStudent['UserID'];
    	            $class_name = $currentStudent['ClassName'];
    	            $class_number = $currentStudent['ClassNumber'];
    	            $student_name = trim($currentStudent['StudentName']);
    	            $student_name = str_replace('　', '', $student_name);
	            
    	            # Record Info
    	            // Conduct Category Count
    	            $student_gm_record = $gm_records[$student_id];
    	            $homework = $student_gm_record[5]['GMRecordCount']? $student_gm_record[5]['GMRecordCount'] : "0";
    	            $study_item = $student_gm_record[7]['GMRecordCount']? $student_gm_record[7]['GMRecordCount'] : "0";
    	            $break_rules = $student_gm_record[8]['GMRecordCount']? $student_gm_record[8]['GMRecordCount'] : "0";
    	            $late = $student_gm_record[6]['GMRecordCount']? $student_gm_record[6]['GMRecordCount'] : "0";
    	            $uniform = $student_gm_record[3]['GMRecordCount']? $student_gm_record[3]['GMRecordCount'] : "0";
    	            $appreciate = $student_gm_record[13]['GMRecordCount']? $student_gm_record[13]['GMRecordCount'] : "0";
    	            $late_lesson = $student_gm_record[16]['GMRecordCount']? $student_gm_record[16]['GMRecordCount'] : "0";
    	            $medical_room = $student_gm_record[14]['GMRecordCount']? $student_gm_record[14]['GMRecordCount'] : "0";
    	            $not_tidy = $student_gm_record[17]['GMRecordCount']? $student_gm_record[17]['GMRecordCount'] : "0";
                    
    	            // Demerit Type Count
    	            $student_ap_record_count = $ap_record_count[$student_id];
    	            $demerit = $student_ap_record_count[-1]? $student_ap_record_count[-1] : "0";
    	            $minor_demerit = $student_ap_record_count[-2]? $student_ap_record_count[-2] : "0";
    	            $major_demerit = $student_ap_record_count[-3]? $student_ap_record_count[-3] : "0";
    	            
    	            // Demerit Record Details
    	            $student_ap_row_count = 0;
    	            $student_ap_records = $ap_records[$student_id];
    	            foreach((array)$student_ap_records as $this_record_id => $this_ap_records)
    	            {
    	                // Type
    	                $this_ap_merit_type = '';
    	                if($this_ap_records['ProfileMeritType'] == 0) {
    	                    $this_ap_merit_type = '警告 ';
    	                } else if($this_ap_records['ProfileMeritType'] == -1) {
    	                    $this_ap_merit_type = '缺點 ';
    	                } else if($this_ap_records['ProfileMeritType'] == -2) {
    	                    $this_ap_merit_type = '小過 ';
    	                } else if($this_ap_records['ProfileMeritType'] == -3) {
    	                    $this_ap_merit_type = '大過 ';
    	                }
    	                
    	                // Item Name
    	                $this_ap_record_str = $this_ap_records['RecordDate'].' '.$this_ap_merit_type.$this_ap_records['ItemName'];
    	                $student_ap_records[$this_record_id] = $this_ap_record_str;
    	                
    	                // Handle long item name
    	                $itemNameStrLen = strlen($this_ap_records['ItemName']);
    	                if(($this_ap_merit_type == '' && $itemNameStrLen > 28) || ($this_ap_merit_type != '' && $itemNameStrLen > 21)) {
    	                    $student_ap_row_count += 0.882;
    	                }
    	                $student_ap_row_count++;
    	            }
    	            $seperator = $format == 'print'? '<br/>' : ' ';
    	            $student_ap_record_str = implode($seperator, (array)$student_ap_records);
    	            $student_ap_row_count = $student_ap_row_count? $student_ap_row_count : 1;
    	            
    	            # Absent Info
    	            $student_absent = $absent_records[$student_id];
    	            $absent = $student_absent[3]? $student_absent[3]: "0";
    	            
    	            # Student Records
    	            if($format == 'print')
    	            {
    	                // Check if need page break (including total row)
    	                $row_count += ($student_ap_row_count + 1);
    	                if($row_count > $page_row)
    	                {
    	                    $x .= $report_pagebreak;
    	                    $x .= $report_header;
    	                    
    	                    $row_count = $student_ap_row_count;
    	                }
    	                
    	                $x .= "<tr>";
        	                $x .= "<td>$class_name</td>";
        	                $x .= "<td>$class_number</td>";
        	                $x .= "<td>$student_name</td>";
        	                $x .= "<td>&nbsp;</td>";
        	                $x .= "<td>&nbsp;</td>";
        	                $x .= "<td>&nbsp;</td>";
        	                $x .= "<td>$homework</td>";
        	                $x .= "<td>$study_item</td>";
        	                $x .= "<td>$break_rules</td>";
        	                $x .= "<td>$late</td>";
        	                $x .= "<td>$uniform</td>";
        	                $x .= "<td>$appreciate</td>";
        	                $x .= "<td>$late_lesson</td>";
        	                $x .= "<td>$medical_room</td>";
        	                $x .= "<td>$not_tidy</td>";
        	                $x .= "<td>$demerit</td>";
        	                $x .= "<td>$minor_demerit</td>";
        	                $x .= "<td>$major_demerit</td>";
        	                $x .= "<td>$absent</td>";
        	                $x .= "<td style='text-align: left'>$student_ap_record_str</td>";
    	                $x .= "</tr>";
    	            }
    	            else
    	            {
    	                $row = array("$class_name", "$class_number", "$student_name", "", "", "", "$homework", "$study_item", "$break_rules", "$late", "$uniform", "$appreciate", "$late_lesson", "$medical_room", "$not_tidy", 
    	                               "$demerit", "$minor_demerit", "$major_demerit", "$absent", "$student_ap_record_str");
    	                $rows[] = $row;
    	            }
    	            
    	            # Handle Total Count
    	            $total_data_count[6] += $homework;
    	            $total_data_count[7] += $study_item;
    	            $total_data_count[8] += $break_rules;
    	            $total_data_count[9] += $late;
    	            $total_data_count[10] += $uniform;
    	            $total_data_count[11] += $appreciate;
    	            $total_data_count[12] += $late_lesson;
    	            $total_data_count[13] += $medical_room;
    	            $total_data_count[14] += $not_tidy;
    	            $total_data_count[15] += $demerit;
    	            $total_data_count[16] += $minor_demerit;
    	            $total_data_count[17] += $major_demerit;
    	            $total_data_count[18] += $absent;
    	        }
    	        
    	        # Total Records
    	        if($format == 'print')
    	        {
    	            $x .= "<tr>";
        	            $x .= "<td>&nbsp;</td>";
        	            $x .= "<td>&nbsp;</td>";
        	            $x .= "<td>總數</td>";
        	            $x .= "<td>&nbsp;</td>";
        	            $x .= "<td>&nbsp;</td>";
        	            $x .= "<td>&nbsp;</td>";
        	            $x .= "<td>".$total_data_count[6]."</td>";
        	            $x .= "<td>".$total_data_count[7]."</td>";
        	            $x .= "<td>".$total_data_count[8]."</td>";
        	            $x .= "<td>".$total_data_count[9]."</td>";
        	            $x .= "<td>".$total_data_count[10]."</td>";
        	            $x .= "<td>".$total_data_count[11]."</td>";
        	            $x .= "<td>".$total_data_count[12]."</td>";
        	            $x .= "<td>".$total_data_count[13]."</td>";
        	            $x .= "<td>".$total_data_count[14]."</td>";
        	            $x .= "<td>".$total_data_count[15]."</td>";
        	            $x .= "<td>".$total_data_count[16]."</td>";
        	            $x .= "<td>".$total_data_count[17]."</td>";
        	            $x .= "<td>".$total_data_count[18]."</td>";
        	            $x .= "<td>&nbsp;</td>";
    	            $x .= "</tr>";
    	        }
    	        else
    	        {
    	            $rows[] = $total_data_count;
    	        }
    	    }
    	    // No valid students
    	    else
    	    {
    	        if($format == 'print')
    	        {
    	            $x .= "<tr>";
    	               $x .= "<td colspan='20'>暫時仍未有任何紀錄。</td>";
    	            $x .= "</tr>";
    	        }
    	        else
    	        {
    	            $rows[] = array("暫時仍未有任何紀錄。");
    	        }
    	    }
    	    
    	    // Check if need page break
    	    $target_num++;
    	    if($format == 'print' && ($target_num < $target_count))
    	    {
    	        $x .= $report_pagebreak;
    	    }
	    }
	    
	    if($format == 'print')
	    {
	        return $x;
	    }
	    else
	    {
	        return $rows;
	    }
	}
	
	function getEmailNotificationRecordList($options)
	{
		global $Lang, $PATH_WRT_ROOT, $ldiscipline;
		
		/*
		 * TemplateType
		 * 1: 已達5次遲到記錄 (accumulate 5 lates)
		 * 2: 遲到6次轉記缺點 (accumulate 6 lates and to demerit)
		 * 3: 違規記錄已扣5分 (misconduct accumulate 5 scores)
		 * 4: 扣分達6分或以上轉記缺點 (misconduct >= 6 scores)
		 * 5: 違反校規被處分 (punishment)
		 */
		$template_type = $options['TemplateType'];
		$send_status = $options['SendStatus'];
		$rankTarget = $options['rankTarget'];
		$selectYear = Get_Current_Academic_Year_ID();
		
		$list = $ldiscipline->getMisconductAndPunishmentNotificationRecords($options);
		
		$record_count = count($list);
		if(in_array($template_type,array(1,2))){
			$detail_title = $Lang['eDiscipline']['EmailNotifictionCust']['LateNumberRecords'];
		}else if(in_array($template_type,array(3,4))){
			$detail_title = $Lang['eDiscipline']['EmailNotifictionCust']['MisconductDeductionRecords'];
		}else{
			$detail_title = $Lang['eDiscipline']['EmailNotifictionCust']['PunishmentRecords'];
		}
		
		$x = '';
		
		$x .= '<div class="table_board">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tbody>';
			$x .=  '<tr>
						<td>
				  			<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tbody>
									<tr>
										<td valign="bottom">
										<div class="common_table_tool">
											<a href="javascript:void(0);" class="tool_other" title="'.$Lang['eDiscipline']['EmailNotifictionCust']['SendEmails'].'" onclick="SendEmail();">'.$Lang['eDiscipline']['EmailNotifictionCust']['SendEmails'].'</a>
											<a href="javascript:void(0);" class="tool_other" title="'.$Lang['eDiscipline']['EmailNotifictionCust']['PreviewEmails'].'" onclick="PreviewEmail();">'.$Lang['eDiscipline']['EmailNotifictionCust']['PreviewEmails'].'</a>
										</div>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>';
			$x.= '</tbody>
				<table>';
		$x .= '</div>';
		
		$x.= '<table class="common_table_list_v30">'."\n";
			$x.='<thead>';
			$x.= '<tr>';
				$x.='<th class="num_check">#</th>';
				$x.='<th style="width:10%;" nowrap>'.$Lang['eDiscipline']['EmailNotifictionCust']['ClassName'].'</th>';
				$x.='<th style="width:5%;" nowrap>'.$Lang['eDiscipline']['EmailNotifictionCust']['ClassNumber'].'</th>';
				$x.='<th style="width:10%;" nowrap>'.$Lang['eDiscipline']['EmailNotifictionCust']['StudentName'].'</th>';
				$x.='<th style="width:55%;">'.$detail_title.'</th>';
				$x.='<th style="width:20%;" nowrap>'.$Lang['eDiscipline']['EmailNotifictionCust']['SendStatus'].'</th>';
				//$x.='<th style="width:15%;">'.'Last Sent Time'.'</th>';
				//$x.='<th style="width:1px;text-align:center;">'.'<input type="checkbox" id="checkmaster" name="checkmaster" onclick="Set_Checkbox_Value(\'WaiveAbsent[]\',this.checked);" /></th>';
				$x.='<th class="num_check"><input type="checkbox" id="checkmaster" name="checkmaster" onclick="$(\'input[name=RecordID[]]\').attr(\'checked\',this.checked);" /></th>';
			$x.= '</tr>'."\n";
			$x.='</thead>';
			$x.='<tbody>';
			
		if($record_count == 0){
			$x.='<tr>';
				$x.='<td colspan="7" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
			$x.='</tr>'."\n";
		}
		
		for($i=0;$i<$record_count;$i++){
			$x .= '<tr>';
				$x .= '<td>'.($i+1).'</td>';
				$x .= '<td>'.$list[$i]['ClassName'].'</td>';
				$x .= '<td>'.$list[$i]['ClassNumber'].'</td>';
				$x .= '<td>'.$list[$i]['StudentName'].'</td>';
				$x .= '<td>'.$list[$i]['RecordDetail'].'</td>';
				$x .= '<td>'.($list[$i]['SendStatus']=='1'?($list[$i]['SenderName']!=''?$list[$i]['SenderName'].'&nbsp;':'').$Lang['eDiscipline']['EmailNotifictionCust']['SentOn'].$list[$i]['DateModified']:$Lang['General']['EmptySymbol']).'</td>';
				$x .= '<td><input type="checkbox" name="RecordID[]" value="'.$list[$i]['RecordID'].'" /></td>';
			$x .= '</tr>';
		}
		
		$x .= '</tbody>
			</table>';
		$x .= '<input type="hidden" id="hiddenTemplateType" name="hiddenTemplateType" value="'.$template_type.'" />';
		$x .= '<input type="hidden" id="hiddenSendStatus" name="hiddenSendStatus" value="'.$send_status.'" />';
		
		
		return $x;
	}
	
	/*
	 * TemplateType
	 * 1: 已達5次遲到記錄 (accumulate 5 lates)
	 * 2: 遲到6次轉記缺點 (accumulate 6 lates and to demerit)
	 * 3: 違規記錄已扣5分 (misconduct accumulate 5 scores)
	 * 4: 扣分達6分或以上轉記缺點 (misconduct >= 6 scores)
	 * 5: 違反校規被處分 (punishment)
	 */
	function getEmailNotificationDiv($TemplateType, $dataAry, $PageBreak=0)
	{
		global $Lang, $sys_custom, $linterface;
		
		$class_teacher_name = $dataAry['ClassTeacherName'];
		$class_name = $dataAry['ClassName'];
		$class_number = $dataAry['ClassNumber'];
		$student_name = $dataAry['StudentName'];
		$record_detail = $dataAry['RecordDetail'];
		$generated_date = date("d/m/Y");
		
		$title = $Lang['eDiscipline']['EmailNotifictionCust']['HeaderArr'][$TemplateType];
		$table_title1 = $Lang['eDiscipline']['EmailNotifictionCust']['TableTitleArr'][$TemplateType];
		if($TemplateType == 1){
			$table_title2 = $Lang['eDiscipline']['EmailNotifictionCust']['Times'];
			$late_count = 0;
			for($i=0;$i<count($dataAry['RecordAry']);$i++){
				$late_count += $dataAry['RecordAry'][$i][1];	
			}
			$record_detail2 = $late_count;
		}else if($TemplateType == 2){
			$table_title2 = $Lang['eDiscipline']['EmailNotifictionCust']['Times'];
			$late_count = 0;
			for($i=0;$i<count($dataAry['RecordAry']);$i++){
				$late_count += $dataAry['RecordAry'][$i][1];	
			}
			$record_detail2 = $late_count;
		}else if($TemplateType == 3){
			$table_title2 = $Lang['eDiscipline']['EmailNotifictionCust']['Deduction'];
			$total_deducted_mark = 0;
		}else if($TemplateType == 4){
			$table_title2 = $Lang['eDiscipline']['EmailNotifictionCust']['Deduction'];
		}else if($TemplateType == 5){
			$table_title2 = $Lang['eDiscipline']['EmailNotifictionCust']['Punishment'];
		}
		
		$x .= '<div style="display:block; margin:1em;">';
			$x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center" border-collapse="collapse" style="border:1px solid black;">';
				$x .= '<tbody>';
				$x .= '<tr>';
				$x .=  '<td valign="top" width="60%">';
					$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" valign="top" border-collapse="collapse">';
						$x .= '<tbody>';
							$x .= '<tr>';
								$x .= '<td style="text-align:left;vertical-align:top;" colspan="2">'.$class_teacher_name.':</td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td style="text-align:center;" colspan="2">'.$title.'</td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td colspan="2" valign="top">';
									$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" border-collapse="collapse">';
										$x .= '<tbody>';
											$x .= '<tr>';
												$x .= '<td width="10%" style="border-bottom:5px double black;">'.$Lang['eDiscipline']['EmailNotifictionCust']['ClassName'].'</td>';
												$x .= '<td width="10%" style="border-bottom:5px double black;">'.$Lang['eDiscipline']['EmailNotifictionCust']['ClassNumber'].'</td>';
												$x .= '<td width="20%" style="border-bottom:5px double black;">'.$Lang['eDiscipline']['EmailNotifictionCust']['StudentName'].'</td>';
												$x .= '<td width="50%" style="border-bottom:5px double black;">'.$table_title1.'</td>';
												$x .= '<td width="10%" style="border-bottom:5px double black;" align="right" nowrap="nowrap">'.$table_title2.'</td>';
											$x .= '</tr>';
											if(in_array($TemplateType,array(1,2))){
												$x .= '<tr>';
													$x .= '<td width="10%" style="border-bottom:5px double black;" valign="top">'.$class_name.'</td>';
													$x .= '<td width="10%" style="border-bottom:5px double black;" valign="top">'.$class_number.'</td>';
													$x .= '<td width="20%" style="border-bottom:5px double black;" valign="top">'.$student_name.'</td>';
													$x .= '<td width="50%" style="border-bottom:5px double black;" valign="top">'.$record_detail.'</td>';
													$x .= '<td width="10%" style="border-bottom:5px double black;" align="right" valign="top" nowrap="nowrap">'.$record_detail2.'</td>';
												$x .= '</tr>';
											}else {
												$record_ary_count = count($dataAry['RecordAry']);
												for($i=0;$i<$record_ary_count;$i++)
												{
													$css = ($i < ($record_ary_count-1)) || $TemplateType==3? ' style="border-bottom:1px solid black;" ' : ' style="border-bottom:5px double black;" ';
													$left_css = $i == ($record_ary_count-1) && $TemplateType != 3? ' style="border-bottom:5px double black;" ' : '';
													if($TemplateType == 3){
														$total_deducted_mark += $dataAry['RecordAry'][$i][1];
													}
													$x .= '<tr>';
														if($i==0){
															$x .= '<td '.$left_css.' width="10%" valign="top">'.$class_name.'</td>';
															$x .= '<td '.$left_css.' width="10%" valign="top">'.$class_number.'</td>';
															$x .= '<td '.$left_css.' width="20%" valign="top">'.$student_name.'</td>';
														}else{
															$x .= '<td '.$left_css.' width="10%">&nbsp;</td>';
															$x .= '<td '.$left_css.' width="10%">&nbsp;</td>';
															$x .= '<td '.$left_css.' width="20%">&nbsp;</td>';
														}
															$x .= '<td '.$css.' width="50%" valign="top">'.$dataAry['RecordAry'][$i][0].'</td>';
															$x .= '<td '.$css.' align="right" valign="top" nowrap="nowrap" width="10%">'.$dataAry['RecordAry'][$i][1].'</td>';
														
													$x .= '</tr>';
												}
												if($TemplateType == 3){
													$x .= '<tr><td style="border-bottom:5px double black;" align="right" colspan="5">'.$Lang['eDiscipline']['EmailNotifictionCust']['SemesterTotalDeduction'].' '.$total_deducted_mark.'</td></tr>';
												}
											}
										$x .= '</tbody>';
									$x .= '</table>';
								$x .= '</td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td>'.(in_array($TemplateType,array(2,4,5))?$Lang['eDiscipline']['EmailNotifictionCust']['PleaseNoticeParent']:'&nbsp;').'</td>';
								$x .= '<td align="right">'.$Lang['eDiscipline']['EmailNotifictionCust']['GrowthGroup'].'<br>'.$generated_date.'</td>';
							$x .= '</tr>';
						$x .= '</tbody>';
					$x .= '</table>';
				$x .=  '</td>';
				$x .= '</tr>';
				$x .= '</tbody>';
				$x .= '</table>';
			
			$x .= '</div>';
			$x .= '<br>';
		
		if($PageBreak){
			$x .= '<div style="page-break-after:always;"></div>';
		}			
		return $x;
	}
}
?>