<?php 
// ============================== Custom Function ==============================
if (!function_exists("cnvUserArrToSelect"))
{
    function cnvUserArrToSelect($arr){
        $oAry = array();
        for($_i=0; $_i<sizeof($arr); $_i++){
            $oAry[$arr[$_i]["UserID"]] = Get_Lang_Selection($arr[$_i]["ChineseName"],$arr[$_i]["EnglishName"]);
        }
        return $oAry;;
    }
}

if (!function_exists("cnvGeneralArrToSelect"))
{
    function cnvGeneralArrToSelect($arr,$type,$ChineseName,$EnglishName){
        $oAry = array();
        for($_i=0; $_i<sizeof($arr); $_i++){
            $oAry[$arr[$_i][$type]] = Get_Lang_Selection($arr[$_i][$ChineseName],$arr[$_i][$EnglishName]);
        }
        return $oAry;;
    }
}

if (!function_exists("cnvLeaveArrToSelect"))
{
    function cnvLeaveArrToSelect($arr, $Lang){
        $oAry = array();
        for($_i=0; $_i<sizeof($arr); $_i++){
            $name = $arr[$_i]["SettingName"];
            $field = $name."Val";
            $oAry[$Lang['SLRS'][$field]] = $Lang['SLRS'][$name];
        }
        return $oAry;;
    }
}

if (!function_exists("cnvSessionArrToSelect"))
{
    function cnvSessionArrToSelect($Lang){
        $oAry = array();
        for($_i=0; $_i<3; $_i++){
            $oAry[$Lang['SLRS']['SubstitutionSessionVal'][$_i]] = $Lang['SLRS']['SubstitutionSessionDes'][$_i];
        }
        return $oAry;;
    }
}

if (!function_exists("cnvSubstituionArrToSelect"))
{
    function cnvSubstituionArrToSelect($Lang){
        $oAry = array();
        for($_i=0; $_i<2; $_i++){
            $oAry[$Lang['SLRS']['SubstitutionActionVal'][$_i]] = $Lang['SLRS']['SubstitutionActionDes'][$_i];
        }
        return $oAry;;
    }
}

if (!function_exists("convertMultipleRowsIntoOneRow"))
{
    function convertMultipleRowsIntoOneRow($arr,$fieldName){
        $x = "";
        for($i=0; $i<sizeof($arr);$i++){
            if($i==sizeof($arr)-1){
                $x .= $arr[$i][$fieldName];
            }
            else{
                $x .= $arr[$i][$fieldName].",";
            }
        }
        if(sizeof($arr)==0){
            $x = "''";
        }
        return $x;
    }
}

// ============================== Custom Function ==============================