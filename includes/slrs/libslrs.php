<?php
// editing by
/********************
 *
 * Date : 2020-01-13 (Bill) [ip.2.5.11.1.1] [ej.5.0.10.2.1]     [IP DM#3728]
 * Description: modified getLessonTeacher(), to exclude teachers if lesson is cancelled ($includeTeacherCancelledLessons = true)
 *
 * Date : 2019-07-17 (Bill) [ip.2.5.10.10.1] [ej.5.0.9.7.1]
 * Desciption: modified getAllLocationSelection(), to improve location name display
 *
 * Date : 2018-10-23 (Ivan) [ip.2.5.9.11.1] [ej.5.0.8.12.1]
 * Desciption: modified getExchangedLessonByDateTeacher() to change TimeSlot Name encoding for EJ
 * 
 * Date : 2018-09-19 (Frankie)
 * Desciption: added function getAssignedUserByDate to get Assigned Teacher by Date
 *             added function getAssignedTimetableInformation to get Assigned Teacher's TimetableInformation
 *             
 * Date : 2018-03-01 (Frankie)
 * Desciption: add function to retrieve SLRS Available Teacher
 *             added getAllSLRSAvailableTeachers()
 *             
 * Date : 2018-02-28 (Frankie)
 * Desciption: change to default disable Title
 *             modify getNameFieldByLang($prefix="", $isTitleDisabled=true)
 *
 * Desciption: disable to select user when has leave record
 *             modify getLessonTeacher()             
 *             
 * Date : 2018-02-06 (Frankie)
 * Description: fixed weekday timezone
 *              modify getAnotherSubjectGroupByExchangeRecord()
 *              modify getLessonTeacherByAnotherInfo()
 *              
 * Date : 2018-02-05 (Frankie)
 * Desciption: modify getYearStatSubstitutionReportData() for displaying suspended account to Yearly Substitution Statistics
 * 
 * Date : 2018-01-26 (Frankie)
 * Description: Handle Combined Exchange
 *   			add flag - $sys_custom['SLRS']["lessonExchangePromptAnotherLessionForced"]
 * 				modify getExchangeInfoForTimetable
 * 				modify getTableGridData
 * 				modify getAnotherSubjectGroupByExchangeRecord
 * 				modify getExchangedLessonByDateTeacher
 * 				add getLocationInfoByIDs
 *
 * Date : 2017-12-11 (Frankie)
 * Description: add getArrangeDetailsForListing() for Substitution Arrangement detail in Listing page
 * Description: add checkSettingsLimitation() for checking Limitation of General Settings per Substitution Teacher
 * 
 * Date : 2017-11-27 (Ivan) [ip.2.5.9.1.1]
 * Description: modify getSubstituteTimetableInformation() and getLocationList() to return location description as well
 * 
 * Date : 2017-11-21 (Frankie) [ip.2.5.9.1.1]
 * Description: modify for integrated Substitution arrangment and Lesson Exchange
 * 				modified function as below: 
 *	 				getTeachingTeacherDate(), getRoom(), getRoomDetails(), getSubjectName(), getLessonTeacher(), getSubstituteTimetableInformation(),
 * 					getArrangementInfoForTimetable(), isLeaveTimeSlot(), getExchangeInfoForTimetable(), updateSLRSBalance(), getSubstitutionStatisticsRecordByDate(),
 * 					checkLesson(), getLessonTeacherByDate(), getLessonTeacherByAnotherInfo(), getYearStatSubstitutionReportData()
 * 				added function as below:
 * 					buildBalanceSummarySQL(), getExpiredUserIDs(), checkWithSubstitution(), getExchangedTeachersByDate(), getExchangedLessonByDateTeacher(), getTeacherOptionByID()
 *
 * Date : 2017-11-14 (Ivan) [ip.2.5.9.1.1]
 * Description: modify getAllLocation() to return room description as well
 * 				modify getAllLocationSelection() to show room description
 *
 * Date : 2017-10-27 (Frankie Leung)
 * Description: modify getYearStatSubstitutionReportData function for initial Balance Adjustment of Current School Year
 *
 * Date : 2017-09-28 (Frankie Leung)
 * Description: modify getSubjectSameClass function with Subject Term
 *
 * Date : 2017-09-20 (Frankie Leung)
 * Description: add getYearStatSubstitutionReportData function for Year Statistics Teacher Substitution Report
 *
 * Date : 2017-08-14 (Frankie Leung)
 * Description: add function for customization case : J120144 - 保祿六世書院 - SLRS features follow up [Waiting Assessment
 * 				- getAllLocationSelection()
 * 				- checkLesson()
 * 				- getLessonTeacherByDate()
 * 				- getNotAvailableLocationByID()
 * 				- getNotAvailableTeacherRecordByDate()
 * 				- getNotAvailableLocationRecordByDate()
 * 				- getPresetLocationInfo()
 * Date : 2017-02-08 Frankie
 * Description: add getArrangementInfoForTimetable(), isLeaveTimeSlot(), getExchangeInfoForTimetable(), isExchangeTimeSlot(), getLocationInfoByID() for Table Time
 * Description: modify getLocationList() with Building and Floor Relation
 *
 * Date		 : 2017-02-03 (Omas)
 * Descripiton: added getNameFieldByLang() - as EJ cannot use the one in lib.php
 * 				added isEJ() - for EJ , and will convert2unicode for EJ for some output
 * 				added getAdminUser() for EJ login
 * 				modified checkAccessRight() - only Admin can access to SLRS
 * 				replace all INTRANET_USER by $slrsConfig['INTRANET_USER'] and corresponding function global $slrsConfig
 * Date      : 2016-11-05 (Frankie Leung)
 * Description: update getTeachingTeacherDate() for speed up processing time.
 * Date      : 2016-11-04 (Frankie Leung)
 * Description: add convertArrKeyToID(), custFormat(), getStaffNameById(), getNotifyStatusById(), getPushMessageStatusDisplay(),
 * Date      : 2016-11-03 (Frankie Leung)
 * Description: add saveNotifyInfo() and getAppNotifyInfo() for Push Message Log
 * Date      : 2016-11-02 (Frankie Leung)
 * Description: add getSubstituteUserByDate() and getSubstituteTimetableInformation()
 * -- Two function are converted from slrs/controllers/reports/dialy_teacher_substitution_report/ajax_getData.php
 *
 ********************/
if (!defined("LIBSLRS_DEFINED")) {
	define("LIBSLRS_DEFINED", true);
	
	class libslrs extends libdb {
		var $ModuleTitle;
		
		function libslrs() {
			global $slrsConfig;
			$this->libdb();
			$this->ModuleTitle = $slrsConfig['moduleCode'];
		}
		
		function getModuleObjArr() {
			global $PATH_WRT_ROOT, $sys_custom, $intranet_root, $image_path, $LAYOUT_SKIN, $CurrentPage;
			global $plugin, $special_feature, $slrsConfig, $indexVar;
			global $intranet_session_language, $Lang;
			
			$currentPageAry = explode($slrsConfig['taskSeparator'], $CurrentPage);
			$pageFunction = $currentPageAry[0];
			$pageMenu = $currentPageAry[0].$slrsConfig['taskSeparator'].$currentPageAry[1];
			
			
			# Management
			$MenuArr["Management"] = array($Lang['SLRS']['Menu']['Management'], "#", ($pageFunction=='management'));
			
			// Lesson re-arrangement
			/*$menuTask = 'management'.$slrsConfig['taskSeparator'].'lesson_arrange'.$slrsConfig['taskSeparator'].'list';
			 $MenuArr["Management"]["Child"]["LessonRearrangement"] = array($Lang['SLRS']['LessonRearrangement'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			 */
			// Samples
			/*$menuTask = 'management'.$slrsConfig['taskSeparator'].'samples'.$slrsConfig['taskSeparator'].'list';
			 $MenuArr["Management"]["Child"]["Samples"] = array('Samples', '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			 */
			// Substitution arrangement
			$menuTask = 'management'.$slrsConfig['taskSeparator'].'substitute_arrangement'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Management"]["Child"]["SubstitutionArrangement"] = array($Lang['SLRS']['SubstitutionArrangement'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			// Lesson exchange
			$menuTask = 'management'.$slrsConfig['taskSeparator'].'lesson_exchange'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Management"]["Child"]["LessonExchange"] = array($Lang['SLRS']['LessonExchange'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			// Temporary Substitution arrangement
			$menuTask = 'management'.$slrsConfig['taskSeparator'].'temporary_substitute_arrangement'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Management"]["Child"]["TemporarySubstitutionArrangement"] = array($Lang['SLRS']['TemporarySubstitutionArrangement'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			if ($sys_custom['SLRS']["enableCancelLesson"])
			{
			    // Cancel Lesson
			    $menuTask = 'management'.$slrsConfig['taskSeparator'].'cancel_lesson'.$slrsConfig['taskSeparator'].'list';
			    $MenuArr["Management"]["Child"]["CancelLesson"] = array($Lang['SLRS']['CancelLesson'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			}
			
			// Substitute Balance
			$menuTask = 'management'.$slrsConfig['taskSeparator'].'substitute_balance'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Management"]["Child"]["SubstitutionBalance"] = array($Lang['SLRS']['SubstitutionBalance'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			# Reports
			$MenuArr["Reports"] = array($Lang['SLRS']['Menu']['Reports'], "#", ($pageFunction=='reports'));
			
			// Daily Teacher Substituion Report
			$menuTask = 'reports'.$slrsConfig['taskSeparator'].'daily_teacher_substitution_report'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Reports"]["Child"]["DailyTeacherSubReport"] = array($Lang['SLRS']['ReportDailyTeacherSubstituion'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			// Lesson Exchange Summary Report
			$menuTask = 'reports'.$slrsConfig['taskSeparator'].'lesson_exchange_summary_report'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Reports"]["Child"]["LessonExchangeSummaryReport"] = array($Lang['SLRS']['ReportLessonExchangeSummary'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			// Summary Of Classroom Change For Lessons Report
			$menuTask = 'reports'.$slrsConfig['taskSeparator'].'summary_classroom_change_lesson_report'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Reports"]["Child"]["SummaryofClassroomChangeforLessons"] = array($Lang['SLRS']['ReportSummaryofClassroomChangeforLessons'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			// Yearly Statistics Of Teacher Substitution Report
			$menuTask = 'reports'.$slrsConfig['taskSeparator'].'yearly_statistics_teacher_substitution_report'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Reports"]["Child"]["YearlyStatisticsofTeacherSubstitution"] = array($Lang['SLRS']['ReportYearlyStatisticsofTeacherSubstitution'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			// Teachers Absence & Substitution Summary Report
			$menuTask = 'reports'.$slrsConfig['taskSeparator'].'teachers_absence_substitution_summary_report'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Reports"]["Child"]["TeachersAbsenceSubstitutionSummary"] = array($Lang['SLRS']['ReportTeachersAbsenceSubstitutionSummary'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			
			# Settings
			$MenuArr["Settings"] = array($Lang['SLRS']['Menu']['Settings'], "#", ($pageFunction=='settings'));
			
			// System Properties
			/*$menuTask = 'settings'.$slrsConfig['taskSeparator'].'system_properties'.$slrsConfig['taskSeparator'].'list';
			 $MenuArr["Settings"]["Child"]["SystemProperties"] = array($Lang['SLRS']['SystemProperties'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			 */
			// Basic Settings
			$menuTask = 'settings'.$slrsConfig['taskSeparator'].'basic_settings'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Settings"]["Child"]["BasicSettings"] = array($Lang['SLRS']['BasicSettings'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			// Substitue Priority Setting
			$menuTask = 'settings'.$slrsConfig['taskSeparator'].'substitute_priority_settings'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Settings"]["Child"]["SubstitutePrioritySetting"] = array($Lang['SLRS']['SubstitutePrioritySettings'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			// Holiday Setting
			$menuTask = 'settings'.$slrsConfig['taskSeparator'].'holiday_settings'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Settings"]["Child"]["HolidaySetting"] = array($Lang['SLRS']['HolidaySettings'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			// Substitute Teacher
			$menuTask = 'settings'.$slrsConfig['taskSeparator'].'substitute_teacher_settings'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Settings"]["Child"]["SubstituteTeacherSetting"] = array($Lang['SLRS']['SubstituteTeacherSettings'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			
			if ($sys_custom['SLRS']["NoAvailTeachers"]) {
				// List of teachers who can not substitute
				$menuTask = 'settings'.$slrsConfig['taskSeparator'].'not_available_teachers'.$slrsConfig['taskSeparator'].'list';
				$MenuArr["Settings"]["Child"]["NotAvailableTeachers"] = array($Lang['SLRS']['NotAvailableTeachers'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			}
			
			// External Teacher Substitution
			$menuTask = 'settings'.$slrsConfig['taskSeparator'].'external_teacher_arrangement'.$slrsConfig['taskSeparator'].'list';
			$MenuArr["Settings"]["Child"]["ExternalTeacher"] = array($Lang['SLRS']['ExternalTeacher'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			
			### module information
			$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT.$slrsConfig['eAdminPath'];
			$MODULE_OBJ['title'] = $Lang['Header']['Menu']['SLRS'];
			$MODULE_OBJ['title_css'] = "menu_opened";
			$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_doc_routing.png";
			$MODULE_OBJ['menu'] = $MenuArr;
			
			return $MODULE_OBJ;
		}
		function checkAccessRight() {
			global $plugin;
			
			$canAccess = true;
			if (!$plugin['SLRS'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-SLRS"]) {
				// 			if (!$plugin['SLRS'] ) {
				$canAccess = false;
			}
			
			if (!$canAccess) {
				No_Access_Right_Pop_Up();
			}
		}
		function isAdminUser() {
			return $_SESSION["SSV_USER_ACCESS"]["eAdmin-SLRS"]? true : false;
		}
		function isEJ(){
			global $junior_mck;
			return isset($junior_mck) && $junior_mck > 0;
		}
		function getAdminUser(){
			if($this->isEJ()){
				global $intranet_root;
				
				include_once($intranet_root."/includes/libfilesystem.php");
				$lf = new libfilesystem();
				$AdminUser = trim($lf->file_read($intranet_root."/file/SLRS/admin_user.txt"));
				$AdminArray = explode(",", $AdminUser);
				return $AdminArray;
			}else{
				return array();
			}
		}
		function getTeacherLessonByDate($parUserId, $parDate) {
			global $PATH_WRT_ROOT;
			
			if($this->isEJ())
			{
				include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
			}
			else
			{
				include_once($PATH_WRT_ROOT."includes/libtimetable.php");
			}
			$ltimetable = new Timetable();
			
			return $ltimetable->getTeacherLessonByDate($parUserId, $parDate);
		}
		function getFreeLessonTeacher($parDate, $parTimeslotId) {
			global $PATH_WRT_ROOT;
			
			if($this->isEJ())
			{
				include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
			}
			else
			{
				include_once($PATH_WRT_ROOT."includes/libtimetable.php");
			}
			$ltimetable = new Timetable();
			
			return $ltimetable->getFreeLessonTeacher($parDate, $parTimeslotId);
		}
		function getTeachingTeacher() {
			global $PATH_WRT_ROOT, $slrsConfig;
			
			if($this->isEJ())
			{
				include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
			}
			else
			{
				include_once($PATH_WRT_ROOT."includes/libtimetable.php");
			}
			$ltimetable = new Timetable();
			$timetableIdAry = Get_Array_By_Key($ltimetable->Get_All_TimeTable(Get_Current_Academic_Year_ID()), 'TimetableID');
			
			$sql = "SELECT
							iu.UserID, iu.ChineseName, iu.EnglishName
					FROM
							INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
							INNER JOIN SUBJECT_TERM_CLASS_TEACHER as stct ON (itra.SubjectGroupID = stct.SubjectGroupID)
							INNER JOIN ".$slrsConfig['INTRANET_USER']." as iu ON (stct.UserID = iu.UserID)
					WHERE
							itra.TimetableID IN ('".implode("','", (array)$timetableIdAry)."')
					GROUP BY
							iu.UserID
					ORDER BY
							iu.EnglishName
					";
			return $this->returnResultSet($sql);
		}
		
		function getTeachingTeacherDate($date, $isLeaveOnly = false) {
			global $PATH_WRT_ROOT, $slrsConfig;
			/*
			 // $date = "2016-11-01";
			 $time_start = microtime(true);
			 
			 if($date != ""){
			 $dateArr[0]=$date;
			 $sql = "SELECT
			 CASE WHEN SupplyUserID IS NOT NULL THEN SupplyEnglishName ELSE iu.EnglishName END as EnglishName,
			 CASE WHEN SupplyUserID IS NOT NULL THEN SupplyChineseName ELSE iu.ChineseName END as ChineseName,
			 CASE WHEN SupplyUserID IS NOT NULL THEN SupplyUserID ELSE iu.UserID END as UserID
			 FROM
			 INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
			 INNER JOIN SUBJECT_TERM_CLASS_TEACHER as stct ON (itra.SubjectGroupID = stct.SubjectGroupID)
			 INNER JOIN INTRANET_USER as iu ON (stct.UserID = iu.UserID)
			 INNER JOIN INVENTORY_LOCATION as il ON (itra.LocationID = il.LocationID)
			 INNER JOIN (".$this->getDaySQL($dateArr,"").") as icd ON (icd.CycleDay = itra.Day)
			 LEFT JOIN(
			 SELECT SupplyPeriodID, Supply_UserID, TeacherID, DateStart, DateEnd FROM
			 INTRANET_SLRS_SUPPLY_PERIOD WHERE '".$date."' BETWEEN DateStart AND DateEnd
			 ) as issp ON iu.UserID=issp.TeacherID
			 LEFT JOIN(
			 SELECT UserID as SupplyUserID, EnglishName as SupplyEnglishName, ChineseName as SupplyChineseName
			 FROM INTRANET_USER
			 ) as iusec ON issp.Supply_UserID=iusec.SupplyUserID
			 WHERE
			 RecordDate = '".$date."'
			 GROUP BY
			 iu.UserID
			 ORDER BY
			 iu.EnglishName
			 ";
			 }
			 else{
			 $sql = "SELECT NOW() as currentDate";
			 $a = $this->returnResultSet($sql);
			 $currentDate==$a[0]["currentDate"];
			 $currentDateArr[0]=$a[0]["currentDate"];
			 $sql = "SELECT
			 CASE WHEN SupplyUserID IS NOT NULL THEN SupplyEnglishName ELSE iu.EnglishName END as EnglishName,
			 CASE WHEN SupplyUserID IS NOT NULL THEN SupplyChineseName ELSE iu.ChineseName END as ChineseName,
			 CASE WHEN SupplyUserID IS NOT NULL THEN SupplyUserID ELSE iu.UserID END as UserID
			 FROM
			 INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
			 INNER JOIN SUBJECT_TERM_CLASS_TEACHER as stct ON (itra.SubjectGroupID = stct.SubjectGroupID)
			 INNER JOIN INTRANET_USER as iu ON (stct.UserID = iu.UserID)
			 INNER JOIN INVENTORY_LOCATION as il ON (itra.LocationID = il.LocationID)
			 INNER JOIN (".$this->getDaySQL($currentDateArr,"").") as icd ON (icd.CycleDay = itra.Day)
			 LEFT JOIN(
			 SELECT SupplyPeriodID,Supply_UserID,TeacherID,DateStart,DateEnd FROM
			 INTRANET_SLRS_SUPPLY_PERIOD WHERE '".$currentDate."' BETWEEN DateStart AND DateEnd
			 ) as issp ON iu.UserID=issp.TeacherID
			 LEFT JOIN(
			 SELECT UserID as SupplyUserID,EnglishName as SupplyEnglishName,ChineseName as SupplyChineseName
			 FROM INTRANET_USER
			 ) as iusec ON issp.Supply_UserID=iusec.SupplyUserID
			 WHERE
			 RecordDate = DATE_FORMAT(NOW(),'%Y-%m-%dd')
			 GROUP BY
			 iu.UserID
			 ORDER BY
			 iu.EnglishName
			 ";
			 }
			 $time_end = microtime(true);
			 $execution_time = ($time_end - $time_start)/60;
			 echo '<b>Total Execution Time:</b> '.$execution_time.' Mins<br>';
			 return $this->returnResultSet($sql);
			 */
			if($date != ""){
				$currentDate= $date;
				$currentDateArr[0] = $date;
			} else {
				$sql = "SELECT NOW() as currentDate";
				$a = $this->returnResultSet($sql);
				$currentDate = date("Y-m-d", strtotime($a[0]["currentDate"]));
				$currentDateArr[0]=$a[0]["currentDate"];
			}
			$TeacherLists = array();
			if (!$isLeaveOnly) {
				$sql = "SELECT
							SupplyPeriodID, Supply_UserID as UserID, TeacherID, DateStart, DateEnd, EnglishName, ChineseName
						FROM
							INTRANET_SLRS_SUPPLY_PERIOD as issp
						JOIN
							".$slrsConfig['INTRANET_USER']." as iusec ON issp.Supply_UserID=iusec.UserID
						WHERE
							'".$currentDate."' BETWEEN DateStart AND DateEnd
						ORDER BY
							EnglishName asc";
				$supplyUser = $this->returnResultSet($sql);
				if ($supplyUser !== false) {
					foreach ($supplyUser as $kk => $val) {
						$key = $val["EnglishName"] . "_" . $val["UserID"];
						$TeacherLists[$key] = $val;
					}
				}
				$sql = "SELECT
						iu.UserID, EnglishName, ChineseName
						FROM
							INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
							INNER JOIN SUBJECT_TERM_CLASS_TEACHER as stct ON (itra.SubjectGroupID = stct.SubjectGroupID)
							INNER JOIN ".$slrsConfig['INTRANET_USER']." as iu ON (stct.UserID = iu.UserID)
							LEFT JOIN INVENTORY_LOCATION as il ON (itra.LocationID = il.LocationID)
							INNER JOIN (".$this->getDaySQL($currentDateArr,"").") as icd ON (icd.CycleDay = itra.Day)";
				$sql .= " WHERE
							RecordDate = '".$currentDate."'
						GROUP BY
							iu.UserID
						ORDER BY
								iu.EnglishName";
			} else {
				$sql = "SELECT IU.UserID, EnglishName, ChineseName
								FROM INTRANET_SLRS_LEAVE as ISL, ".$slrsConfig['INTRANET_USER']." as IU
								WHERE ISL.UserID=IU.UserID and DateLeave like '".$currentDate."'";
			}
			
			$otherUser = $this->returnResultSet($sql);
			if ($otherUser !== false) {
				foreach ($otherUser as $kk => $val) {
					$key = $val["EnglishName"] . "_" . $val["UserID"];
					$TeacherLists[$key] = $val;
				}
			}
			ksort($TeacherLists);
			$time_end = microtime(true);
			$execution_time = ($time_end - $time_start)/60;
			return array_values($TeacherLists);
			// echo '<b>Total Execution Time:</b> '.$execution_time.' Mins<br>';
		}
		function getRoom($teacher, $date, $checkWithArrangement = true, $ignoreExchangeGroup = array()){
			global $PATH_WRT_ROOT, $slrsConfig;
			if($this->isEJ())
			{
				include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
			}
			else
			{
				include_once($PATH_WRT_ROOT."includes/libtimetable.php");
			}
			$ltimetable = new Timetable();
			$timeTableID = $ltimetable->Get_Current_Timetable($date);
			
			$teacher = $this->getOriginalTeacherPeriod($teacher,$date);
			$dateArr[0]=$date;
			// for tunning the SQL performance, the table of INTRANET_TIMETABLE_ROOM_ALLOCATION & INVENTORY_LOCATION are INNER JOIN in advanced
			/*
			 $sql = "SELECT
			 itra.LocationID,itra.NameChi,itra.NameEng,ClassTitleB5,ClassTitleEN,TimeSlotName,itra.TimeSlotID,itra.SubjectGroupID,itt.StartTime
			 FROM (
			 SELECT YearTermID,AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE '".$date."' BETWEEN TermStart AND TermEnd
			 ) as ayt
			 INNER JOIN(
			 SELECT TimetableID,YearTermID,AcademicYearID FROM INTRANET_SCHOOL_TIMETABLE WHERE TimeTableID=".$timeTableID."
			 ) as ist ON ayt.YearTermID=ist.YearTermID AND ayt.AcademicYearID=ist.AcademicYearID
			 INNER JOIN (
			 SELECT TimeSlotID,TimeTableID FROM INTRANET_TIMETABLE_TIMESLOT_RELATION
			 ) as ittr ON ist.TimeTableID=ittr.TimeTableID
			 INNER JOIN (
			 SELECT RoomAllocationID,Day,TimeSlotID,itra.LocationID,SubjectGroupID,OthersLocation,Code,Barcode,NameChi,NameEng
			 FROM(
			 SELECT RoomAllocationID,Day,TimeSlotID,LocationID,SubjectGroupID,OthersLocation FROM INTRANET_TIMETABLE_ROOM_ALLOCATION
			 ) as itra
			 INNER JOIN(
			 SELECT LocationID,Code,Barcode,NameChi,NameEng FROM INVENTORY_LOCATION
			 ) as il ON itra.LocationID=il.LocationID
			 )as itra ON ittr.TimeSlotID=itra.TimeSlotID
			 INNER JOIN (".$this->getDaySQL($dateArr,"").")as icd ON itra.Day=icd.CycleDay
			 INNER JOIN (
			 SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER
			 )as stct ON itra.SubjectGroupID = stct.SubjectGroupID
			 INNER JOIN(
			 SELECT UserID,EnglishName,ChineseName FROM INTRANET_USER
			 ) as iu ON stct.UserID=iu.UserID
			 INNER JOIN(
			 SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS
			 ) as stc ON itra.SubjectGroupID=stc.SubjectGroupID
			 INNER JOIN(
			 SELECT TimeSlotID,TimeSlotName,StartTime,EndTime FROM INTRANET_TIMETABLE_TIMESLOT
			 ) as itt ON itra.TimeSlotID = itt.TimeSlotID
			 WHERE
			 stct.UserID IN ('".implode("','", (array)$teacher)."')
			 AND RecordDate = '".$date."'
			 ORDER BY
			 itt.StartTime,iu.EnglishName
			 ";
			 */
			$sql = "SELECT
						itra.LocationID, itra.NameChi, itra.NameEng, ClassTitleB5, ClassTitleEN, TimeSlotName, itra.TimeSlotID, itra.SubjectGroupID, itt.StartTime, stct.UserID
					FROM (
						SELECT YearTermID, AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE '".$date."' BETWEEN TermStart AND TermEnd
					) as ayt
					INNER JOIN(
						SELECT TimetableID, YearTermID, AcademicYearID FROM INTRANET_SCHOOL_TIMETABLE WHERE TimeTableID=".$timeTableID."
					) as ist ON ayt.YearTermID=ist.YearTermID AND ayt.AcademicYearID=ist.AcademicYearID
					INNER JOIN (
						SELECT TimeSlotID, TimeTableID FROM INTRANET_TIMETABLE_TIMESLOT_RELATION WHERE TimeTableID=".$timeTableID."
					) as ittr ON ist.TimeTableID=ittr.TimeTableID
					INNER JOIN (
						SELECT RoomAllocationID, Day, TimeSlotID, itra.LocationID, SubjectGroupID, OthersLocation, Code, Barcode, NameChi, NameEng
						FROM(
							SELECT RoomAllocationID, Day, TimeSlotID, LocationID, SubjectGroupID, OthersLocation FROM INTRANET_TIMETABLE_ROOM_ALLOCATION
							WHERE TimeTableID=".$timeTableID."
						) as itra
						LEFT JOIN(
							SELECT LocationID, Code, Barcode, NameChi, NameEng FROM INVENTORY_LOCATION
						) as il ON itra.LocationID=il.LocationID
					)as itra ON ittr.TimeSlotID=itra.TimeSlotID
					INNER JOIN (".$this->getDaySQL($dateArr,"").")as icd ON itra.Day=icd.CycleDay
					INNER JOIN (
						SELECT SubjectClassTeacherID, SubjectGroupID, UserID FROM SUBJECT_TERM_CLASS_TEACHER
					)as stct ON itra.SubjectGroupID = stct.SubjectGroupID
					INNER JOIN(
						SELECT UserID, EnglishName, ChineseName FROM ".$slrsConfig['INTRANET_USER']."
					) as iu ON stct.UserID=iu.UserID and iu.UserID IN ('".implode("','", (array)$teacher) ."')
					INNER JOIN(
						SELECT SubjectGroupID, ClassCode, ClassTitleEN, ClassTitleB5 FROM SUBJECT_TERM_CLASS
					) as stc ON itra.SubjectGroupID=stc.SubjectGroupID
					INNER JOIN(
						SELECT TimeSlotID, TimeSlotName, StartTime, EndTime FROM INTRANET_TIMETABLE_TIMESLOT
					) as itt ON itra.TimeSlotID = itt.TimeSlotID
					WHERE
						stct.UserID IN ('".implode("','", (array)$teacher)."')
						AND RecordDate = '".$date."'
					ORDER BY
						itt.StartTime, iu.EnglishName
				";
			$result = $this->returnResultSet($sql);
			/********************************************************************************/
			/* Disable already Substitution
			 /********************************************************************************/
			if (count($result) > 0)
			{
				foreach ($result as $kk => $vv)
				{
					$sql = "SELECT LessonArrangementID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE UserID='" . $vv["UserID"] . "' AND TimeSlotID='" . $vv["TimeSlotID"] . "' AND DATE(TimeStart)='" . $date . "'";
					$tmp = $this->returnResultSet($sql);
					if (count($tmp) > 0)
					{
						if ($checkWithArrangement) unset($result[$kk]);
						else $result[$kk]["isArranged"] = true;
					}
					$sql = "SELECT LessonExchangeID FROM INTRANET_SLRS_LESSON_EXCHANGE WHERE TeacherA_UserID='" . $vv["UserID"] . "' AND TeacherA_TimeSlotID='" . $vv["TimeSlotID"] . "' AND TeacherA_Date='" . $date . "'";
					if (count($ignoreExchangeGroup) > 0)
					{
						$sql .= " AND GroupID NOT IN (" . implode(",", $ignoreExchangeGroup) . ")";
					}
					$tmp = $this->returnResultSet($sql);
					if (count($tmp) > 0)
					{
						unset($result[$kk]);
					}
				}
				$result = array_values($result);
			}
			if (count($result) > 0 && $this->isEJ()) {
				foreach ($result as $_key => $_valArr) {
					$result[$_key]["NameChi"] = convert2unicode($_valArr["NameChi"]);
					$result[$_key]["ClassTitleB5"] = convert2unicode($_valArr["ClassTitleB5"]);
					$result[$_key]["TimeSlotName"] = convert2unicode($_valArr["TimeSlotName"]);
				}
			}
			return $result;
		}
		
		function getRoomDetails($teacher,$date,$timeSlotID, $SubjectGroupID = ""){
			
			global $slrsConfig;
			$teacher=$this->getOriginalTeacherPeriod($teacher,$date);
			$dateArr[0]=$date;
			
			if ($this->isEJ()) {
				$sql = "SELECT
				il.Code,itra.LocationID, OthersLocation, il.NameChi,il.NameEng,itra.TimeSlotID,LEFT(itt.StartTime,5) as StartTime,LEFT(itt.EndTime,5) as EndTime,itt.TimeSlotName,CONCAT('Day ',itra.Day) as Day,
				st.SubjectID,stc.SubjectGroupID,stc.ClassTitleB5,stc.ClassTitleEN,st.YearTermID,ayt.AcademicYearID,yc.YearID,yc.YearClassID,itra.OthersLocation
				FROM
					INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
					INNER JOIN SUBJECT_TERM_CLASS_TEACHER as stct ON (itra.SubjectGroupID = stct.SubjectGroupID)
					INNER JOIN ".$slrsConfig['INTRANET_USER']." as iu ON (stct.UserID = iu.UserID)
					LEFT JOIN INVENTORY_LOCATION as il ON (itra.LocationID = il.LocationID)
					INNER JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON (itra.TimeSlotID = itt.TimeSlotID)
					INNER JOIN SUBJECT_TERM_CLASS as stc ON (itra.SubjectGroupID = stc.SubjectGroupID)
					INNER JOIN (".$this->getDaySQL($dateArr,"").") as icd ON (icd.CycleDay = itra.Day)
					INNER JOIN SUBJECT_TERM as st ON (stct.SubjectGroupID = st.SubjectGroupID)
					INNER JOIN ACADEMIC_YEAR_TERM as ayt ON (st.YearTermID = ayt.YearTermID)
					INNER JOIN YEAR_CLASS as yc ON (ayt.AcademicYearID=yc.AcademicYearID)
				WHERE
					stct.UserID IN ('".implode("','", (array)$teacher)."')
					AND RecordDate = '".$date."'
					AND itra.TimeSlotID = '".$timeSlotID."'";
				if (!empty($SubjectGroupID)) {
					$sql .= " AND stc.SubjectGroupID='" . $SubjectGroupID . "'";
				}
				$sql .= " GROUP BY
					iu.UserID,il.LocationID
				ORDER BY
					iu.EnglishName
				";
			} else {
				$sql = "SELECT
				il.Code,itra.LocationID,il.NameChi,il.NameEng,itra.TimeSlotID,LEFT(itt.StartTime,5) as StartTime,LEFT(itt.EndTime,5) as EndTime,itt.TimeSlotName,CONCAT('Day ',itra.Day) as Day,
				st.SubjectID,stc.SubjectGroupID,stc.ClassTitleB5,stc.ClassTitleEN,st.YearTermID,ayt.AcademicYearID,syr.YearID,yc.YearClassID,itra.OthersLocation
				FROM
					INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
					INNER JOIN SUBJECT_TERM_CLASS_TEACHER as stct ON (itra.SubjectGroupID = stct.SubjectGroupID)
					INNER JOIN ".$slrsConfig['INTRANET_USER']." as iu ON (stct.UserID = iu.UserID)
					LEFT JOIN INVENTORY_LOCATION as il ON (itra.LocationID = il.LocationID)
					INNER JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON (itra.TimeSlotID = itt.TimeSlotID)
					INNER JOIN SUBJECT_TERM_CLASS as stc ON (itra.SubjectGroupID = stc.SubjectGroupID)
					INNER JOIN (".$this->getDaySQL($dateArr,"").") as icd ON (icd.CycleDay = itra.Day)
					INNER JOIN SUBJECT_TERM as st ON (stct.SubjectGroupID = st.SubjectGroupID)
					INNER JOIN ACADEMIC_YEAR_TERM as ayt ON (st.YearTermID = ayt.YearTermID)
					INNER JOIN SUBJECT_YEAR_RELATION as syr ON (st.SubjectID = syr.SubjectID)
					INNER JOIN YEAR_CLASS as yc ON (syr.YearID=yc.YearID AND ayt.AcademicYearID=yc.AcademicYearID)
				WHERE
					stct.UserID IN ('".implode("','", (array)$teacher)."')
					AND RecordDate = '".$date."'
					AND itra.TimeSlotID = '".$timeSlotID."'";
				if (!empty($SubjectGroupID)) {
					$sql .= " AND stc.SubjectGroupID='" . $SubjectGroupID . "'";
				}
				$sql .= " GROUP BY
					iu.UserID,il.LocationID
				ORDER BY
					iu.EnglishName
				";
			}
			$result = $this->returnResultSet($sql);
			if (count($result) > 0) {
				foreach ($result as $key => $val) {
					$res = $this->getYearClassIDBySubjectGroupID($val["SubjectGroupID"], $date);
					$result[$key]["YearClassID"] = $res[0]["YearClassID"];
					if ($this->isEJ()) {
					    $result[$key]["TimeSlotName"] = convert2unicode($result[$key]["TimeSlotName"]);
					}
				}
			}
			return $result;
		}
		function isTimeslotWithoutLesson($parTimeslotId) {
			$sql = "Select
							count(*) as numOfLesson
					From
							INTRANET_TIMETABLE_ROOM_ALLOCATION
					Where
							TimeSlotID = '".$parTimeslotId."'
					";
			$resultAry = $this->returnResultSet($sql);
			
			return ($resultAry[0]['numOfLesson'] == 0)? true : false;
		}
		function getClassTeacherBySubjectGroup($parSubjectGroupId) {
			$sql = "
					Select
							yct.UserID
					From
							SUBJECT_TERM_CLASS_USER as stcu
							Inner Join YEAR_CLASS_USER as ycu ON (stcu.UserID = ycu.UserID)
							Inner Join YEAR_CLASS_TEACHER as yct ON (ycu.YearClassID = yct.YearClassID)
							Inner Join YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID)
					Where
							stcu.SubjectGroupID = '".$parSubjectGroupId."'
							AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
					Group By
							yct.UserID
					";
			//echo $sql;
			return $this->returnResultSet($sql);
		}
		function getSubjectTeacherTeachingSameSubject($parSubjectGroupId) {
			//same subject
			$sql = "Select
							stct.UserID
					From
							SUBJECT_TERM as stOrg
							Inner Join SUBJECT_TERM as stTarget On (stOrg.SubjectID = stTarget.SubjectID And stOrg.YearTermID = stTarget.YearTermID)
							Inner Join SUBJECT_TERM_CLASS_TEACHER as stct On (stTarget.SubjectGroupID = stct.SubjectGroupID)
					Where
							stOrg.SubjectGroupID = '".$parSubjectGroupId."'
					Group By
							stct.UserID
					";
			//echo $sql."<br/><br/>";
			return $this->returnResultSet($sql);
		}
		function getSubjectTeacherTeachingSameClass($parSubjectGroupId) {
			//has taught those teachers
			$sql = "Select
							stctTarget.UserID
					From
							SUBJECT_TERM_CLASS_USER as stcuOrg
							Inner Join SUBJECT_TERM as stOrg On (stcuOrg.SubjectGroupID = stOrg.SubjectGroupID)
							Inner Join SUBJECT_TERM as stTarget On (stOrg.YearTermID = stTarget.YearTermID)
							Inner Join SUBJECT_TERM_CLASS_USER as stcuTarget On (stTarget.SubjectGroupID = stcuTarget.SubjectGroupID and stcuOrg.UserID = stcuTarget.UserID)
							Inner Join SUBJECT_TERM_CLASS_TEACHER as stctTarget On (stcuTarget.SubjectGroupID = stctTarget.SubjectGroupID)
					Where
							stcuOrg.SubjectGroupID = '".$parSubjectGroupId."'
					Group By
							stctTarget.UserID
					";
			//echo $sql."<br/><br/>";
			return $this->returnResultSet($sql);
		}
		
		function getSupplyTeacher(){
			global $slrsConfig;
			$sql = "SELECT UserID,ChineseName,EnglishName
					FROM ".$slrsConfig['INTRANET_USER']." WHERE Teaching='S'";
			return $this->returnResultSet($sql);
		}
		function getRelatedClassBySubjectGroup($parSubjectGroupId) {
			$sql = "Select
							yc.YearClassID, yc.ClassTitleEN as ClassNameEn, yc.ClassTitleB5 as ClassNameCh
					From
							SUBJECT_TERM_CLASS_USER as stcu
							Inner Join YEAR_CLASS_USER as ycu On (stcu.UserID = ycu.UserID)
							Inner Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
					Where
							stcu.SubjectGroupID = '".$parSubjectGroupId."'
							And yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
					Group By
							yc.YearClassID
					";
			return $this->returnResultSet($sql);
		}
		
		function getAcademicYear($academicID) {
			$academicID=($academicID=="")?"%":$academicID;
			$sql = "SELECT AcademicYearID, YearNameEN, YearNameB5, Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID like '".$academicID."' ORDER BY Sequence";
			$result = $this->returnResultSet($sql);
			if($this->isEJ()){
				foreach((array)$result as $_key => $_infoArr){
					$result[$_key]['YearNameB5'] = convert2unicode($_infoArr['YearNameB5'], 1, 1);
				}
				return $result;
			}else{
				return $result ;
			}
		}
		function getBalanceDisplay(){
			$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE SettingName='SubstitutionBalanceDisplay'";
			$info = $this->returnResultSet($sql);
			$balanceDisplay=$info[0]["SettingValue"];
			$balanceDisplay=($balanceDisplay==0)?1:-1;
			return $balanceDisplay;
		}
		
		function getLocationList(){
			$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='LocationNameDisplay';";
			$a = $this->returnResultSet($sql);
			$mode = $a[0]["SettingValue"];
			/*
			 if($mode == 0){
			 $sql = "SELECT LocationID, NameChi, NameEng FROM INVENTORY_LOCATION ORDER BY LocationLevelID DESC,DisplayOrder";
			 }
			 else if($mode == 1){
			 $sql = "SELECT LocationID, Code as NameChi, Code as NameEng FROM INVENTORY_LOCATION ORDER BY LocationLevelID DESC,DisplayOrder";
			 }
			 */
			if($mode == 0){
				$mode_sql = "r.NameChi,
				r.NameEng";
			} else if($mode == 1){
				$mode_sql = "r.Code as NameChi,
				r.Code as NameEng";
			}
			$sql = "SELECT
							b.BuildingID,
							b.Code as BuildingCode,
							b.NameChi as BuildingNameChi,
							b.NameEng as BuildingNameEng,
							f.LocationLevelID,
							f.Code as FloorCode,
							f.NameChi as FloorNameChi,
							f.NameEng as FloorNameEng,
							r.LocationID,
							" . $mode_sql . ",
							r.Description as LocationDesc
							FROM
							INVENTORY_LOCATION_BUILDING as b
							INNER JOIN INVENTORY_LOCATION_LEVEL as f ON f.BuildingID = b.BuildingID
							INNER JOIN INVENTORY_LOCATION as r ON r.LocationLevelID = f.LocationLevelID
						WHERE
							b.RecordStatus = 1
							AND f.RecordStatus = 1
							AND r.RecordStatus = 1
						Order By
							r.LocationLevelID DESC, b.DisplayOrder, f.DisplayOrder, r.DisplayOrder
				";
			$result = $this->returnResultSet($sql);
			if (count($result) > 0 && $this->isEJ()) {
				foreach ($result as $kk => $vv) {
					$result[$kk]["BuildingNameChi"] = convert2unicode($result[$kk]["BuildingNameChi"]);
					$result[$kk]["FloorNameChi"] = convert2unicode($result[$kk]["FloorNameChi"]);
					$result[$kk]["NameChi"] = convert2unicode($result[$kk]["NameChi"]);
					$result[$kk]["LocationDesc"] = convert2unicode($result[$kk]["LocationDesc"]);
				}
			}
			return $result;
		}
		
		function getAcademicTimeSlot($date){
			global $PATH_WRT_ROOT;
			if($this->isEJ())
			{
				include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
			}
			else
			{
				include_once($PATH_WRT_ROOT."includes/libtimetable.php");
			}
			$ltimetable = new Timetable();
			
			$data_param = "itt.TimeSlotID as TimeSlotID";
			$sql = "
					SELECT " . $data_param . "
					FROM (
						SELECT YearTermID, AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE '".$date."' BETWEEN TermStart AND TermEnd
					) as ayt
					INNER JOIN(
						SELECT TimetableID, YearTermID, AcademicYearID, TimetableName, CycleDays FROM INTRANET_SCHOOL_TIMETABLE
					) as ist ON ayt.YearTermID=ist.YearTermID AND ayt.AcademicYearID=ist.AcademicYearID AND TimeTableID=".IntegerSafe($ltimetable->Get_Current_Timetable($date))."
					INNER JOIN (
						SELECT TimeSlotID,TimeTableID FROM INTRANET_TIMETABLE_TIMESLOT_RELATION
					) as itt ON ist.TimeTableID=itt.TimeTableID
				";
			return $this->returnResultSet($sql);
		}
		
		/**
		 * modify at 2017-09-20 by Frankie Leung
		 *
		 * @param unknown $subjectGroupID
		 * @param unknown $date
		 * @param unknown $tgtUserID
		 * @return string
		 */
		function getSubjectSameClass($subjectGroupID, $date, $tgtUserID){
			/*$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='SubjectDisplay';";
			 $a = $this->returnResultSet($sql);
			 $mode = $a[0]["SettingValue"];*/
			$b = $this->getYearClassID($date);
			$yearClassID = $this->convertMultipleRowsIntoOneRow($b,"YearClassID");
			
			$c = $this->getAcademicYearTerm($date);
			$yearTermID = $this->convertMultipleRowsIntoOneRow($c,"YearTermID");
			$academicID = $this->convertMultipleRowsIntoOneRow($c,"AcademicYearID");
			//Get the class(es) of the students sitting in the same class
			$sql = "SELECT DISTINCT YearClassID FROM YEAR_CLASS_USER
					WHERE YearClassID IN (".$yearClassID.")
					AND USERID IN (SELECT UserID FROM SUBJECT_TERM_CLASS_USER WHERE SubjectGroupID=".IntegerSafe($subjectGroupID).")";
			//echo $sql;
			$a=$this->returnResultSet($sql);
			$YearClassIDList=$this->convertMultipleRowsIntoOneRow($a,"YearClassID");
			//echo $sql."<br/><br/>";
			
			//Get the class(es) of the students sitting in the same class
			$sql = "SELECT DISTINCT SubjectGroupID FROM SUBJECT_TERM_CLASS_USER WHERE SubjectGroupID<>".IntegerSafe($subjectGroupID)."
					AND UserID IN (SELECT UserID FROM YEAR_CLASS_USER WHERE YearClassID IN (".$YearClassIDList."))";
			$a=$this->returnResultSet($sql);
			//echo $sql."<br/><br/>";
			$SubjectGroupIDListOfSameClass=$this->convertMultipleRowsIntoOneRow($a,"SubjectGroupID");
			
			// Check what subjects the target is teaching for the same class
			$sql="
				SELECT ClassTitleEN as ClassTitleENSec, ClassTitleB5 as ClassTitleB5Sec
				FROM SUBJECT_TERM_CLASS A
				INNER JOIN SUBJECT_TERM AS st ON (st.SubjectGroupID=A.SubjectGroupID AND YearTermID='" . $yearTermID . "')
				INNER JOIN SUBJECT_TERM_CLASS_TEACHER B ON A.SubjectGroupID = B.SubjectGroupID AND UserID=".IntegerSafe($tgtUserID)."
				WHERE A.SubjectGroupID IN (".$SubjectGroupIDListOfSameClass.")
				GROUP BY ClassTitleEN,ClassTitleB5
			";
			$b=$this->returnResultSet($sql);
			/*$sql = "
			 SELECT stc.SubjectGroupID,stcsec.SubjectGroupIDSec,ClassTitleENSec,ClassTitleB5Sec
			 FROM(
			 SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID=".IntegerSafe($subjectGroupID)."
			 ) as stc
			 INNER JOIN(
			 SELECT SubjectGroupID,MIN(UserID) as UserID FROM SUBJECT_TERM_CLASS_USER
			 GROUP BY SubjectGroupID
			 ) as stcu ON stc.SubjectGroupID=stcu.SubjectGroupID
			 INNER JOIN(
			 SELECT UserID,YearClassID FROM YEAR_CLASS_USER WHERE YearClassID IN (".$yearClassID.")
			 ) as ycu ON stcu.UserID=ycu.UserID
			 INNER JOIN(
			 SELECT YearClassID,ClassTitleEN as YearClassTitleEN,ClassTitleB5 as YearClassTitleB5,AcademicYearID FROM YEAR_CLASS
			 WHERE AcademicYearID IN (".$academicID.") AND YearClassID IN (".$yearClassID.")
			 ) as yc ON ycu.YearClassID=yc.YearClassID
			 INNER JOIN(
			 SELECT YearTermID,AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE YearTermID IN (".$yearTermID.") AND AcademicYearID IN (".$academicID.")
			 ) as ayt ON yc.AcademicYearID=ayt.AcademicYearID
			 INNER JOIN(
			 SELECT SubjectID,SubjectGroupID,YearTermID FROM SUBJECT_TERM WHERE YearTermID IN (".$yearTermID.")
			 ) as stsec ON ayt.YearTermID=stsec.YearTermID
			 INNER JOIN(
			 SELECT SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER WHERE UserID=".IntegerSafe($tgtUserID)."
			 ) as stct ON stsec.SubjectGroupID=stct.SubjectGroupID
			 INNER JOIN(
			 SELECT SubjectGroupID as SubjectGroupIDSec,ClassTitleEN as ClassTitleENSec,ClassTitleB5 as ClassTitleB5Sec FROM SUBJECT_TERM_CLASS
			 ) as stcsec ON stct.SubjectGroupID=stcsec.SubjectGroupIDSec
			 ";
			 */
			//echo $sql."<br/><br/>";
			//$b = $this->returnResultSet($sql);
			for ($i=0; $i<sizeof($b);$i++){
				if($i==(sizeof($b)-1)){
					if($this->isEJ()){
						$desc .= Get_Lang_Selection(convert2unicode($b[$i]["ClassTitleB5Sec"],1,1), $b[$i]["ClassTitleENSec"]);
					}else{
						$desc .= Get_Lang_Selection($b[$i]["ClassTitleB5Sec"],$b[$i]["ClassTitleENSec"]);
					}
				}
				else{
					if($this->isEJ()){
						$desc = Get_Lang_Selection(convert2unicode($b[$i]["ClassTitleB5Sec"],1,1), $b[$i]["ClassTitleENSec"]).", ".$desc;
					}else{
						$desc = Get_Lang_Selection($b[$i]["ClassTitleB5Sec"],$b[$i]["ClassTitleENSec"]).", ".$desc;
					}
				}
			}
			//echo $desc." ".$tgtUserID."<br/><br/>";
			return $desc;
		}
		function getYearClass($subjectGroupID){
			$sql = "SELECT stc.SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5,RecordID,CODEID,EN_SNAME,CH_SNAME,EN_DES,CH_DES,EN_ABBR,CH_ABBR,
					YearClassTitleEN,YearClassTitleB5
					FROM(
						SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS
					) as stc
					INNER JOIN(
						SELECT SubjectID,SubjectGroupID FROM SUBJECT_TERM
					) as st ON stc.SubjectGroupID=st.SubjectGroupID
					INNER JOIN(
						SELECT RecordID,CODEID,EN_SNAME,CH_SNAME,EN_DES,CH_DES,EN_ABBR,CH_ABBR
						FROM ASSESSMENT_SUBJECT
					) as assub ON st.SubjectID=assub.RecordID
					INNER JOIN(
						SELECT SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_USER
					) as stcu ON stc.SubjectGroupID=stcu.SubjectGroupID
					INNER JOIN(
						SELECT UserID,YearClassID FROM YEAR_CLASS_USER
					) as ycu ON stcu.UserID=ycu.UserID
					INNER JOIN(
						SELECT YearClassID,ClassTitleEN as YearClassTitleEN,ClassTitleB5 as YearClassTitleB5 FROM YEAR_CLASS WHERE AcademicYearID = '".Get_Current_Academic_Year_ID()."'
					) as yc ON ycu.YearClassID=yc.YearClassID
					WHERE stc.SubjectGroupID=".IntegerSafe($subjectGroupID)."
					GROUP BY stc.SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5,RecordID,CODEID,EN_SNAME,CH_SNAME,EN_DES,CH_DES,EN_ABBR,CH_ABBR
					";
			//echo $sql;
			$b = $this->returnResultSet($sql);
			if($this->isEJ()){
				$desc=Get_Lang_Selection(convert2unicode($b[0]["YearClassTitleB5"],1,1), $b[0]["YearClassTitleEN"]);
			}else{
				$desc=Get_Lang_Selection($b[0]["YearClassTitleB5"],$b[0]["YearClassTitleEN"]);
			}
			return $desc;
		}
		function getSubjectName($subjectGroupID,$format){
			// ASSESSMENT_SUBJECT
			$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='SubjectDisplay';";
			$a = $this->returnResultSet($sql);
			$mode = $a[0]["SettingValue"];
			$sql = "SELECT stc.SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5
					FROM(
						SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID=".IntegerSafe($subjectGroupID)."
					) as stc
					";
			$b = $this->returnResultSet($sql);
			
			if($this->isEJ()){
				$desc=Get_Lang_Selection(convert2unicode($b[0]["ClassTitleB5"],1,1), $b[0]["ClassTitleEN"]);
			}else{
				$desc=Get_Lang_Selection($b[0]["ClassTitleB5"],$b[0]["ClassTitleEN"]);
			}
			//echo $desc;
			return $desc;
		}
		function getLocationName($code,$locationChiName,$locationEngName){
			$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='LocationNameDisplay';";
			$a = $this->returnResultSet($sql);
			$mode = $a[0]["SettingValue"];
			
			$desc = "";
			if($mode == 0){
				$desc = Get_Lang_Selection($this->displayChinese($locationChiName),$locationEngName);
			}
			else if($mode == 1){
				$desc = $code;
			}
			return $desc;
		}
		function getYearClassID($date){
			$sql = "SELECT YearClassID,ayt.AcademicYearID
					FROM (
						SELECT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE '".$date."' BETWEEN TermStart AND TermEnd
					) as ayt
					INNER JOIN(
						SELECT YearClassID,AcademicYearID FROM YEAR_CLASS
					) as yc ON ayt.AcademicYearID=yc.AcademicYearID
					";
			//echo $sql;
			return $this->returnResultSet($sql);
		}
		function getAcademicYearTerm($date){
			$sql = "SELECT YearTermID,AcademicYearID
					FROM (
						SELECT YearTermID,AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE '".$date."' BETWEEN TermStart AND TermEnd
					) as ayt
					";
			//echo $sql;
			return $this->returnResultSet($sql);
		}
		function getYearClassIDVal($date){
			$sql = "SELECT YearClassID,ayt.AcademicYearID
					FROM (
						SELECT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE '".$date."' BETWEEN TermStart AND TermEnd
					) as ayt
					INNER JOIN(
						SELECT YearClassID,AcademicYearID FROM YEAR_CLASS
					) as yc ON ayt.AcademicYearID=yc.AcademicYearID
					";
			//echo $sql;
			$a = $this->returnResultSet($sql);
			return $a[0]["YearClassID"];
		}
		function getOthersLocation($timeSlotID,$subjectGroupID,$date){
			$dateArr[0]=$date;
			$sql = "SELECT *
					FROM (".$this->getDaySQL($dateArr,"").") as icd";
			$a = $this->returnResultSet($sql);
			$cycleDay = $a[0]["CycleDay"];
			$sql = "SELECT OthersLocation
					FROM(
						SELECT YearTermID,AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE '".$date."' BETWEEN TermStart AND TermEnd
					) as ayt
					INNER JOIN(
						SELECT TimeTableID,AcademicYearID,YearTermID FROM INTRANET_SCHOOL_TIMETABLE
					) as ist ON ayt.YearTermID=ist.YearTermID AND ayt.AcademicYearID=ist.AcademicYearID
					INNER JOIN(
						SELECT TimeTableID,TimeSlotID,Day,OthersLocation FROM INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day=".$cycleDay." AND SubjectGroupID=".$subjectGroupID."
						AND TimeSlotID=".$timeSlotID."
					) as itra ON ist.TimeTableID=itra.TimeTableID
					";
			//echo $sql."<br/>";
			$b = $this->returnResultSet($sql);
			$desc = $b[0]["OthersLoaction"];
			return $desc;
		}
		
		function setDefaultSettings(){
			$this->initBasicSetting();
			$this->initSubstitutePrioritySetting();
			$this->initHolidaySetting();
		}
		function initBasicSetting(){
			/*$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='CycleWeekMaximunLessons'";
			 $a = $this->returnResultSet($sql);
			 if(sizeof($a)==0){
			 $sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
			 SELECT 'SLRS','CycleWeekMaximunLessons',21,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
			 $a = $this->db_db_query($sql);
			 }*/
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='DailyMaximunLessons'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','DailyMaximunLessons',5,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='DailyMaximunSubstitution'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','DailyMaximunSubstitution',3,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			/*$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='FirstLessonSubstituion'";
			 $a = $this->returnResultSet($sql);
			 if(sizeof($a)==0){
			 $sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
			 SELECT 'SLRS','FirstLessonSubstituion',0,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
			 $a = $this->db_db_query($sql);
			 }*/
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='FormSixTeacherSubstition'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','FormSixTeacherSubstition',0,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='LocationNameDisplay'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','LocationNameDisplay',1,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='SpecialClassroomLocation'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','SpecialClassroomLocation',0,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='SubjectDisplay'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','SubjectDisplay',1,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			/*$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='SubjectGroupDisplay'";
			 $a = $this->returnResultSet($sql);
			 if(sizeof($a)==0){
			 $sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
			 SELECT 'SLRS','SubjectGroupDisplay',1,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
			 $a = $this->db_db_query($sql);
			 }*/
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='SubstitutionAfterSickLeave'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','SubstitutionAfterSickLeave',0,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='SubstitutionBalanceDisplay'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','SubstitutionBalanceDisplay',0,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='VoluntarySubstitute'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','VoluntarySubstitute',1,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
		}
		function initSubstitutePrioritySetting(){
			$sql = "SELECT * FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE ConditionID='1'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO INTRANET_SLRS_SUBSTITUTE_PRIORITY(ConditionID,RowID,ColumnID,DisplayOrder,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)
						SELECT 1,1,1,1,".IntegerSafe($_SESSION['UserID']).",NOW(),NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE ConditionID='2'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO INTRANET_SLRS_SUBSTITUTE_PRIORITY(ConditionID,RowID,ColumnID,DisplayOrder,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)
						SELECT 2,2,1,1,".IntegerSafe($_SESSION['UserID']).",NOW(),NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE ConditionID='3'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO INTRANET_SLRS_SUBSTITUTE_PRIORITY(ConditionID,RowID,ColumnID,DisplayOrder,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)
						SELECT 3,1,2,1,".IntegerSafe($_SESSION['UserID']).",NOW(),NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE ConditionID='4'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO INTRANET_SLRS_SUBSTITUTE_PRIORITY(ConditionID,RowID,ColumnID,DisplayOrder,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)
						SELECT 4,1,2,2,".IntegerSafe($_SESSION['UserID']).",NOW(),NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE ConditionID='5'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO INTRANET_SLRS_SUBSTITUTE_PRIORITY(ConditionID,RowID,ColumnID,DisplayOrder,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)
						SELECT 5,2,2,1,".IntegerSafe($_SESSION['UserID']).",NOW(),NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE ConditionID='6'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO INTRANET_SLRS_SUBSTITUTE_PRIORITY(ConditionID,RowID,ColumnID,DisplayOrder,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)
						SELECT 6,2,2,3,".IntegerSafe($_SESSION['UserID']).",NOW(),NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE ConditionID='7'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO INTRANET_SLRS_SUBSTITUTE_PRIORITY(ConditionID,RowID,ColumnID,DisplayOrder,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)
						SELECT 7,2,2,2,".IntegerSafe($_SESSION['UserID']).",NOW(),NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE ConditionID='8'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO INTRANET_SLRS_SUBSTITUTE_PRIORITY(ConditionID,RowID,ColumnID,DisplayOrder,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)
						SELECT 8,2,2,4,".IntegerSafe($_SESSION['UserID']).",NOW(),NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE ConditionID='9'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO INTRANET_SLRS_SUBSTITUTE_PRIORITY(ConditionID,RowID,ColumnID,DisplayOrder,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)
						SELECT 9,2,2,5,".IntegerSafe($_SESSION['UserID']).",NOW(),NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE ConditionID='10'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO INTRANET_SLRS_SUBSTITUTE_PRIORITY(ConditionID,RowID,ColumnID,DisplayOrder,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)
						SELECT 10,2,2,6,".IntegerSafe($_SESSION['UserID']).",NOW(),NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE ConditionID='11'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO INTRANET_SLRS_SUBSTITUTE_PRIORITY(ConditionID,RowID,ColumnID,DisplayOrder,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)
						SELECT 11,2,2,7,".IntegerSafe($_SESSION['UserID']).",NOW(),NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
		}
		function initHolidaySetting(){
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='OfficialLeavelAO'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','OfficialLeavelAO',0,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='OfficialLeavelPL'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','OfficialLeavelPL',0,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='OfficialLeavelSLP'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','OfficialLeavelSLP',0,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='OfficialLeavelSTL'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','OfficialLeavelSTL',0,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
			$sql = "SELECT * FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='OfficialLeavelWS'";
			$a = $this->returnResultSet($sql);
			if(sizeof($a)==0){
				$sql = "INSERT INTO GENERAL_SETTING(Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy)
						SELECT 'SLRS','OfficialLeavelWS',0,NOW(),".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID'])."";
				$a = $this->db_db_query($sql);
			}
		}
		function getOriginalTeacherPeriod($userID,$date){
			// get the Supply Teacher ID based on the specific date. If yes, return the Supply TeacherID. If no, return the original userID
			$originalTeacherID = "";
			$sql = "SELECT SupplyPeriodID,Supply_UserID,TeacherID FROM INTRANET_SLRS_SUPPLY_PERIOD WHERE '".$date."' BETWEEN DateStart AND DateEnd AND Supply_UserID=".IntegerSafe($userID).";";
			//echo $sql;
			$a = $this->returnResultSet($sql);
			if(sizeof($a)>0){
				$originalTeacherID=$a[0]["TeacherID"];
			}
			else{
				$originalTeacherID=$userID;
			}
			return $originalTeacherID;
		}
		
		
		function getLessonLimitation($leaveID, $lessonArrangementID, $date){
			global $intranet_session_language, $Lang, $slrsConfig;
			$userID = "''";
			if($lessonArrangementID==""){
				$sql = "SELECT LessonArrangementID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LeaveID=".$leaveID.";";
				$a=$this->returnResultSet($sql);
				$lessonArrangementID=$this->convertMultipleRowsIntoOneRow($a,"LessonArrangementID");
			}
			
			// find out Substitution limit per day
			$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='DailyMaximunSubstitution';";
			$a=$this->returnResultSet($sql);
			$dailyMaximunSubstitution=$Lang['SLRS']['DailyMaximunSubstitutionVal'][$a[0]["SettingValue"]];
			
			$sql = "SELECT ArrangedTo_UserID,CountArrangedToUserID
					FROM(
						SELECT ArrangedTo_UserID,COUNT(ArrangedTo_UserID) as CountArrangedToUserID FROM INTRANET_SLRS_LESSON_ARRANGEMENT
						WHERE '".$date."' BETWEEN DATE_FORMAT(TimeStart,'%Y-%m-%d') AND DATE_FORMAT(TimeEnd,'%Y-%m-%d') GROUP BY ArrangedTo_UserID
					) as a
					WHERE CountArrangedToUserID>='".$dailyMaximunSubstitution."' AND ArrangedTo_UserID NOT IN (
						SELECT ArrangedTo_UserID FROM INTRANET_SLRS_LESSON_ARRANGEMENT
						WHERE LessonArrangementID IN (".$lessonArrangementID."))";
			
			$result1=$this->returnResultSet($sql);
			$dateArr[0]=$date;
			
			// find out Limit of teaching loading per day
			$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='DailyMaximunLessons';";
			$x=$this->returnResultSet($sql);
			$dailyMaximunLessons=$Lang['SLRS']['DailyMaximunLessonsVal'][$x[0]["SettingValue"]];
			
			$sql = $this->getDaySQL($dateArr,"");
			$x=$this->returnResultSet($sql);
			$day=$x[0]["CycleDay"];
			
			$sql = "SELECT UserID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LessonArrangementID=".$lessonArrangementID."";
			$x=$this->returnResultSet($sql);
			
			//$userID=$x[0]["UserID"];
			$academicTimeSlot = $this->convertMultipleRowsIntoOneRow($this->getAcademicTimeSlot($date),"TimeSlotID");
			
			/**************************************************/
			$academicYearInfo = getAcademicYearInfoAndTermInfoByDate($date);
			$date_academicYearID = $academicYearInfo[0];
			/**************************************************/
			$sql = "SELECT
						SubjectGroupID FROM INTRANET_TIMETABLE_ROOM_ALLOCATION as s_itra
					JOIN
					INTRANET_SCHOOL_TIMETABLE AS s_it ON (s_it.TimetableID=s_itra.TimetableID AND AcademicYearID='" . $date_academicYearID . "')
					WHERE Day='".$day."' GROUP BY SubjectGroupID";
			$result_subjectgroup = $this->returnResultSet($sql);
			$ls_subjectGroup = $this->convertMultipleRowsIntoOneRow($result_subjectgroup,"SubjectGroupID");
			/**************************************************/
			$sql = "
			SELECT UserID,CountDailyLesson
			FROM(
				SELECT UserID, COUNT(UserID) as CountDailyLesson
				FROM (
					SELECT TimeSlotID, TimeSlotName, StartTime, EndTime, DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT
							WHERE TimeSlotID IN (".$academicTimeSlot.")) AS itt
				LEFT JOIN(
					SELECT RoomAllocationID, Day, itra.TimeSlotID, itra.LocationID, itra.SubjectGroupID, OthersLocation, RecordDate, CycleDay, ClassCode, ClassTitleEN, ClassTitleB5,
					NameChi, NameEng, DisplayOrder, Code, BarCode, SubjectClassTeacherID, stct.UserID, EnglishName, ChineseName,
					SubjectTermID, SubjectID, YearTermID, LessonArrangementID, ArrangedTo_UserID, ArrangedTo_LocationID
					FROM (
						SELECT
							RoomAllocationID,Day,TimeSlotID,LocationID,SubjectGroupID,OthersLocation FROM INTRANET_TIMETABLE_ROOM_ALLOCATION as s_itra
						JOIN
							INTRANET_SCHOOL_TIMETABLE AS s_it ON (s_it.TimetableID=s_itra.TimetableID AND AcademicYearID='" . $date_academicYearID . "')
						WHERE Day='".$day."' AND SubjectGroupID in (" . $ls_subjectGroup . ")) AS itra
						LEFT JOIN (".$this->getDaySQL($dateArr,"").") AS icd ON itra.Day=icd.CycleDay
						LEFT JOIN (SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS ) AS stc ON (itra.SubjectGroupID=stc.SubjectGroupID)
						LEFT JOIN (SELECT LocationID,NameChi,NameEng,DisplayOrder,Code,BarCode FROM INVENTORY_LOCATION) AS il ON (itra.LocationID = il.LocationID)
						LEFT JOIN (SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER) AS stct ON (itra.SubjectGroupID=stct.SubjectGroupID)
						LEFT JOIN (SELECT UserID,EnglishName,ChineseName FROM ".$slrsConfig['INTRANET_USER'].") AS iu ON (stct.UserID=iu.UserID)
						LEFT JOIN (SELECT SubjectTermID,SubjectGroupID,SubjectID,YearTermID FROM SUBJECT_TERM) AS st ON itra.SubjectGroupID = st.SubjectGroupID
						LEFT JOIN (SELECT LessonArrangementID,UserID,TimeSlotID,LocationID,ArrangedTo_UserID,ArrangedTo_LocationID
								FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LessonArrangementID IN (".$lessonArrangementID.")) AS isla ON (iu.UserID=isla.UserID
								AND itra.TimeSlotID=isla.TimeSlotID)
					WHERE stct.UserID IS NOT NULL
				) AS a ON itt.TimeSlotID=a.TimeSlotID
				WHERE UserID IS NOT NULL
				GROUP BY UserID
			) AS a0
			WHERE
				UserID IS NOT NULL AND CountDailyLesson>='".$dailyMaximunLessons."' AND UserID NOT IN (
				SELECT ArrangedTo_UserID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LessonArrangementID IN (".$lessonArrangementID."))";
			/** /
			 $debug_arr = array(
			 "WHERE" => "<br>WHERE<br>&nbsp;&nbsp;&nbsp;",
			 "LEFT JOIN" => "<br>LEFT JOIN<br>&nbsp;&nbsp;&nbsp;",
			 "SELECT" => "<br>SELECT<br>&nbsp;&nbsp;&nbsp;",
			 "FROM" => "<br>FROM<br>&nbsp;&nbsp;&nbsp;",
			 "))" => ")<br>)"
			 );
			 echo str_replace(array_keys($debug_arr), array_values($debug_arr), $sql)."<br/><br/>";
			 
			 /**************************************************/
			// echo $sql . "<br>";
			$result2=$this->returnResultSet($sql);
			/**************************************************/
			
			//如老師上一個工作天曾請病假當天不需代課
			$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='SubstitutionAfterSickLeave';";
			$a=$this->returnResultSet($sql);
			// if $previousDateLeave=0, that teacher allows to substitute other teachers. else, that teacher substitute to other teachers
			$previousDateLeave=$a[0]["SettingValue"];
			$previousDate = date('Y-m-d', strtotime($date.'-1 day'));
			$sql = "SELECT UserID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE '".$previousDate."' BETWEEN DATE_FORMAT(TimeStart,'%Y-%m-%d') AND DATE_FORMAT(TimeEnd,'%Y-%m-%d') GROUP BY UserID";
			//echo $sql."<br/><br/>";
			$result3=$this->returnResultSet($sql);
			
			if(sizeof($result1)>0){
				$userID=$this->convertMultipleRowsIntoOneRow($result1,"ArrangedTo_UserID");
			}
			if(sizeof($result2)>0){
				$userID=$userID.", ".$this->convertMultipleRowsIntoOneRow($result2,"UserID");
			}
			if(sizeof($result3)>0 && $previousDateLeave=="1"){
				$userID=$userID.", ".$this->convertMultipleRowsIntoOneRow($result3,"UserID");
			}
			//echo $userID;
			if($userID==""){
				$userID="''";
			}
			return $userID;
		}
		
		function getLessonTeacher($date, $timeSlotID, $LessonArrangementID='', $withNotAvailTeacher=false, $withDuration=false, $includeTeacherCancelledLessons=false){
		    global $PATH_WRT_ROOT, $sys_custom, $slrsConfig;
			
			$dateArr[0] = $date;
			$sql = $this->getDaySQL($dateArr, "");
			//echo $sql."<br/><br/>";
			
			$a = $this->returnResultSet($sql);
			$day = $a[0]["CycleDay"];
			
			$teachers = array();
			
			$sql = "SELECT UserID
					FROM(
						SELECT ist.UserID, stct.SubjectGroupID
						FROM (
							SELECT UserID, Balance as UpdatedBalance, Priority FROM INTRANET_SLRS_TEACHER
                        ) as ist
						INNER JOIN(
							SELECT UserID, EnglishName, ChineseName, TitleChinese, TitleEnglish FROM ".$slrsConfig['INTRANET_USER']."
						) as iu ON ist.UserID = iu.UserID
					   	INNER JOIN(
						   	SELECT SubjectClassTeacherID, SubjectGroupID, UserID FROM SUBJECT_TERM_CLASS_TEACHER
						) as stct ON ist.UserID = stct.UserID
						INNER JOIN(
						   	SELECT SubjectGroupID, 1 as NumberOfLessons FROM INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day = ".$day." AND TimeSlotID IN (".$timeSlotID.")
						) as itra ON itra.SubjectGroupID = stct.SubjectGroupID
						INNER JOIN(
						   	SELECT SubjectGroupID, ClassCode, ClassTitleEN, ClassTitleB5 FROM SUBJECT_TERM_CLASS
						) as stc ON itra.SubjectGroupID = stc.subjectGroupID
					) as a
					GROUP BY UserID";
			$result1 = $this->returnResultSet($sql);
			
			// [IP DM#3666] Teacher cancelled original lessons
			if ($sys_custom['SLRS']["enableCancelLesson"] && $includeTeacherCancelledLessons)
			{
			    $sql = " SELECT UserID FROM INTRANET_SLRS_CANCEL_LESSON WHERE CancelDate = '$date' AND TimeSlotID = '$timeSlotID' AND DeletedAt IS NULL";
			    $result = $this->returnVector($sql);
			    if(sizeof($result) > 0)
			    {
			        foreach((array)$result1 as $index => $teacherInLesson) {
			            if($teacherInLesson['UserID'] && in_array($teacherInLesson['UserID'], $result)) {
			                unset($result1[$index]);
			            }
			        }
			        $result1 = array_values($result1);
			    }
			}
			if(sizeof($result1) > 0) {
			    $teachers[] = $this->convertMultipleRowsIntoOneRow($result1, "UserID");
			}
			
			// Teacher exchanged lesson
			$sql = "SELECT TeacherA_UserID as UserID FROM INTRANET_SLRS_LESSON_EXCHANGE WHERE TeacherB_TimeSlotID = '".$timeSlotID."' AND TeacherB_Date = '".$date."'";
			$result = $this->returnResultSet($sql);
			if (count($result) > 0)
			{
				$sUserID = $this->convertMultipleRowsIntoOneRow($result, "UserID");
				if (!empty($sUserID))
				{
				    $tmp = explode(",", $sUserID);
				    $teachers = array_merge($teachers, $tmp);
				}
			}
			
			if ($withDuration)
			{
			    ## Leave record 2018-02-28
			    $sql = "SELECT UserID FROM INTRANET_SLRS_LEAVE WHERE DateLeave LIKE '".$date."' AND 
                            (Duration = 'wd' OR Duration IN (SELECT SessionType FROM INTRANET_TIMETABLE_TIMESLOT WHERE TimeSlotID = '".$timeSlotID."'))";
			    $result = $this->returnResultSet($sql);
			    if (count($result) > 0)
			    {
			        $sUserID = $this->convertMultipleRowsIntoOneRow($result, "UserID");
			        if (!empty($sUserID))
			        {
			            $tmp = explode(",", $sUserID);
			            $teachers = array_merge($teachers, $tmp);
			        }
			    }
			    ## end
			}
			
			// Teacher already substitute lessons
			$sql = "SELECT ArrangedTo_UserID as UserID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE TimeSlotID = '".$timeSlotID."' AND DATE(TimeStart) = '".$date."'";
			if ($LessonArrangementID > 0)
			{
				$sql .= " AND LessonArrangementID != '".$LessonArrangementID."'";
			}
			$result = $this->returnResultSet($sql);
			if (count($result) > 0)
			{
				$sUserID = $this->convertMultipleRowsIntoOneRow($result, "UserID");
				if (!empty($sUserID))
				{
				    $tmp = explode(",", $sUserID);
				    $teachers = array_merge($teachers, $tmp);
				}
			}
			
			// Not Available Teacher
			if ($withNotAvailTeacher)
			{
				$sql = "SELECT StartTime, EndTime FROM INTRANET_TIMETABLE_TIMESLOT WHERE TimeSlotID = '".$timeSlotID."' LIMIT 1";
				$result1 = $this->returnResultSet($sql);
				if(sizeof($result1) > 0)
				{
					$StartTime = $date." ".$result1[0]["StartTime"];
					$EndTime = $date." ".$result1[0]["EndTime"];
					
					$strSQL = "SELECT UserID FROM INTRANET_SLRS_NOT_AVAILABLE_TEACHERS WHERE '".$StartTime."' BETWEEN StartDate AND EndDate OR '".$EndTime."' BETWEEN StartDate AND EndDate";
					$result = $this->returnResultSet($strSQL);
					if (count($result) > 0)
					{
						$sUserID = $this->convertMultipleRowsIntoOneRow($result, "UserID");
						if (!empty($sUserID))
						{
						    $tmp = explode(",", $sUserID);
						    $teachers = array_merge($teachers, $tmp);
						}
					}
				}
			}
			
			if(count($teachers) > 0) {
			    $userID = implode(",", $teachers);
			} else {
				$userID = "''";
			}
			
			return $userID;
		}
		
		function convertMultipleRowsIntoOneRow($arr,$fieldName){
			$x = "";
			for($i=0; $i<sizeof($arr);$i++){
				if($i==sizeof($arr)-1){
					$x .= $arr[$i][$fieldName];
				}
				else{
					$x .= $arr[$i][$fieldName].",";
				}
			}
			if(sizeof($arr)==0){
				$x = "''";
			}
			return $x;
		}
		function getDaySQL($date,$alias){
			$sql = "";
			for($i=0;$i<sizeof($date);$i++){
				// check the SQL should use INTRANET_CYCLE_DAYS or weekday
				$sqlTemp = "SELECT PeriodID,PeriodStart,PeriodEnd,PeriodType FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE '".$date[$i]."' BETWEEN PeriodStart AND PeriodEnd;";
				
				$a = $this->returnResultSet($sqlTemp);
				$periodType=$a[0]["PeriodType"];
				// prepare the
				if($periodType==0){
					// week day
					$sql .= "SELECT '".$date[$i]."' as RecordDate".$alias.",WEEKDAY('".$date[$i]."')+1 as CycleDay".$alias." ";
				}
				else{
					// cycle day
					$sql .= "SELECT RecordDate as RecordDate".$alias.",CycleDay as CycleDay".$alias." FROM INTRANET_CYCLE_DAYS ";
					$sql .=	"WHERE RecordDate ='".$date[$i]."'";
				}
				if($i!=sizeof($date)-1){
					$sql .= " UNION ";
				}
			}
			//echo $sql;
			return $sql;
		}
		
		/*
		 * get Substitute User By Date
		 * copied from /home/eAdmin/StaffMgmt/slrs/controller/reports/daily_teacher_substitution_report/ajax_getData.php
		 */
		function getSubstituteUserByDate($date = null, $name_field = null, $filterByUserId = null) {
			global $slrsConfig;
			$result = null;
			if ($date != null && !empty($date) && $name_field != null && !empty($name_field)) {
				$dateArr[0] = $date;
				$sql = "SELECT RecordDate,CycleDay,LeaveID,isl.UserID,DateLeave,ReasonCode,Duration,".$name_field." as userName
						FROM(".$this->getDaySQL($dateArr,"").") as icd
						LEFT JOIN(
							SELECT LeaveID,UserID,DateLeave,ReasonCode,Duration FROM INTRANET_SLRS_LEAVE
						) as isl ON icd.RecordDate=isl.DateLeave
						LEFT JOIN(
							SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM ".$slrsConfig['INTRANET_USER']."
						) as iu ON isl.UserID=iu.UserID	";
				if ($filterByUserId != null) {
					$sql .= " WHERE isl.UserID in (" . $filterByUserId . ")";
				} else {
					$sql .= " WHERE isl.UserID > 0 and isl.UserID is not null";
				}
				$result = $this->returnResultSet($sql);
			}
			return $result;
		}
		
		/*
		 * 20180918
		 * get Assigned User By Date
		 */
		function getAssignedUserByDate($date = null, $name_field = null, $filterByUserId = null) {
		    global $slrsConfig;
		    $result = null;
		    if ($date != null && !empty($date) && $name_field != null && !empty($name_field)) {
		        $dateArr[0] = $date;
		        $sql = "SELECT RecordDate,CycleDay,LeaveID,isl.UserID,DateLeave,ReasonCode,Duration,".$name_field." as userName, isl.leaveUserID
						FROM(".$this->getDaySQL($dateArr,"").") as icd
						LEFT JOIN(
							SELECT 
                                sub_isl.LeaveID, sub_isla.ArrangedTo_UserID as UserID, sub_isl.DateLeave, sub_isl.ReasonCode, sub_isl.Duration, sub_isla.UserID as leaveUserID
                            FROM 
                                INTRANET_SLRS_LEAVE AS sub_isl
                            JOIN
                                INTRANET_SLRS_LESSON_ARRANGEMENT AS sub_isla on (sub_isl.LeaveID=sub_isla.LeaveID)
                            WHERE
                                DateLeave like '%" . $date . "'
                            GROUP BY  
                                sub_isla.ArrangedTo_UserID
						) as isl ON icd.RecordDate=isl.DateLeave
						LEFT JOIN(
							SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM ".$slrsConfig['INTRANET_USER']."
						) as iu ON isl.UserID=iu.UserID	";
		        if ($filterByUserId != null) {
		            $sql .= " WHERE isl.UserID in (" . $filterByUserId . ")";
		        } else {
		            $sql .= " WHERE isl.UserID > 0 and isl.UserID is not null";
		        }
		        $result = $this->returnResultSet($sql);
		    }
		    return $result;
		}
		
		/*
		 * get Substitute Timetable Information
		 * copied from /home/eAdmin/StaffMgmt/slrs/controller/reports/daily_teacher_substitution_report/ajax_getData.php
		 */
		function getSubstituteTimetableInformation($yearClass=null, $date=null, $day=null, $userID=null, $academicTimeSlot=null, $name_field=null, $withNullClass=true)
		{
			global $slrsConfig, $PATH_WRT_ROOT;
			if($this->isEJ())
			{
				include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
			}
			else 
			{
				include_once($PATH_WRT_ROOT."includes/libtimetable.php");
			}
			$ltimetable = new Timetable();
			$curTimetableId = $ltimetable->Get_Current_Timetable($date);
			$result = null;
			if ($yearClass != null && $date != null && $day != null && $userID != null && $academicTimeSlot != null && $name_field != null) {
				$dateArr[0] = $date;
				
				/******* replaced 2017-11-06 * /
				 $sql = "SELECT
				 itt.TimeSlotID, TimeSlotName, LEFT(StartTime,5) as StartTime, LEFT(EndTime,5) as EndTime, itt.DisplayOrder, RoomAllocationID, Day, a.LocationID, a.SubjectGroupID,
				 OthersLocation, RecordDate, CycleDay, ClassCode, ClassTitleEN, ClassTitleB5, NameChi, NameEng, Code, BarCode, a.UserID, a.EnglishName, a.ChineseName, SubjectTermID,
				 a.SubjectID, islUserID, LeaveID, LessonArrangementID, ArrangedTo_UserID, ArrangedTo_LocationID, a.YearTermID, ".$name_field." as ArrangedTo_UserName,
				 YearClassTitleEN, YearClassTitleB5, stcuUser
				 FROM (
				 SELECT
				 TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT WHERE SessionType like '%'
				 AND TimeSlotID IN (".$academicTimeSlot.")
				 ) as itt
				 LEFT JOIN(
				 SELECT
				 RoomAllocationID, Day, itra.TimeSlotID, itra.LocationID, itra.SubjectGroupID, OthersLocation, RecordDate, CycleDay, ClassCode, ClassTitleEN, ClassTitleB5,
				 NameChi, NameEng, DisplayOrder, Code, BarCode, SubjectClassTeacherID, stct.UserID, EnglishName, ChineseName,
				 SubjectTermID, st.SubjectID, st.YearTermID, isl.LeaveID, islUserID, DateLeave, LessonArrangementID, ArrangedTo_UserID, ArrangedTo_LocationID
				 FROM (
				 SELECT
				 RoomAllocationID, Day, TimeSlotID, LocationID, SubjectGroupID, OthersLocation FROM INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day='".$day."' AND TimetableID = '".$curTimetableId."'
				 ) as itra
				 LEFT JOIN (" . $this->getDaySQL($dateArr,"") . ") as icd ON itra.Day=icd.CycleDay
				 LEFT JOIN (
				 SELECT
				 SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS
				 ) as stc ON itra.SubjectGroupID=stc.SubjectGroupID
				 LEFT JOIN (
				 SELECT
				 LocationID,NameChi,NameEng,DisplayOrder,Code,BarCode FROM INVENTORY_LOCATION
				 ) as il ON itra.LocationID = il.LocationID
				 LEFT JOIN (
				 SELECT
				 SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER WHERE UserID=".$userID."
				 ) as stct ON itra.SubjectGroupID=stct.SubjectGroupID
				 LEFT JOIN (
				 SELECT
				 UserID,EnglishName,ChineseName FROM ".$slrsConfig['INTRANET_USER']." WHERE UserID=".$userID."
				 ) as iu ON stct.UserID=iu.UserID
				 LEFT JOIN (
				 SELECT
				 SubjectTermID,SubjectGroupID,SubjectID,YearTermID FROM SUBJECT_TERM
				 ) as st ON itra.SubjectGroupID = st.SubjectGroupID
				 LEFT JOIN (
				 SELECT
				 LeaveID,UserID as islUserID,DateLeave FROM INTRANET_SLRS_LEAVE WHERE DateLeave='".$date."'
				 ) as isl ON stct.UserID=isl.islUserID
				 LEFT JOIN (
				 SELECT
				 LessonArrangementID,UserID,LeaveID,TimeSlotID,LocationID,ArrangedTo_UserID,ArrangedTo_LocationID,SubjectGroupID
				 FROM
				 INTRANET_SLRS_LESSON_ARRANGEMENT
				 ) as isla ON isl.islUserID=isla.UserID AND isl.LeaveID=isla.LeaveID AND itra.TimeSlotID=isla.TimeSlotID	AND isla.SubjectGroupID=st.SubjectGroupID
				 WHERE stct.UserID IS NOT NULL
				 ) as a ON itt.TimeSlotID=a.TimeSlotID
				 LEFT JOIN(
				 SELECT
				 UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM ".$slrsConfig['INTRANET_USER']."
				 ) as iu ON a.ArrangedTo_UserID=iu.UserID
				 LEFT JOIN(
				 SELECT
				 SubjectGroupID,MIN(UserID) as stcuUser FROM SUBJECT_TERM_CLASS_USER GROUP BY SubjectGroupID
				 ) as stcu ON a.SubjectGroupID=stcu.SubjectGroupID ";
				 /* get NULL value */
				
				/******* /
				$sub_sql = "SELECT
							RoomAllocationID, Day, itra.TimeSlotID, itra.LocationID, itra.SubjectGroupID, OthersLocation, RecordDate, CycleDay, ClassCode, ClassTitleEN, ClassTitleB5,
							NameChi, NameEng, DisplayOrder, Code, BarCode, SubjectClassTeacherID, stct.UserID, EnglishName, ChineseName,
							SubjectTermID, st.SubjectID, st.YearTermID, isl.LeaveID, islUserID, DateLeave, LessonArrangementID, ArrangedTo_UserID, ArrangedTo_LocationID, il.Description as LocationDesc
						FROM (
							SELECT
								LessonArrangementID, UserID, LeaveID, TimeSlotID, LocationID, ArrangedTo_UserID, ArrangedTo_LocationID, SubjectGroupID
							FROM
								INTRANET_SLRS_LESSON_ARRANGEMENT
						) as isla
						LEFT JOIN (
							SELECT
								LeaveID,UserID as islUserID,DateLeave
							FROM
								INTRANET_SLRS_LEAVE WHERE DateLeave='".$date."'
						) as isl ON isl.islUserID=isla.UserID AND isl.LeaveID=isla.LeaveID
						LEFT JOIN (
							SELECT
								RoomAllocationID, Day, TimeSlotID, LocationID, SubjectGroupID, OthersLocation
							FROM
								INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day='".$day."' AND TimetableID='".$curTimetableId."'
						) as itra ON (itra.TimeSlotID=isla.TimeSlotID)
						JOIN (" . $this->getDaySQL($dateArr,"") . ") as icd ON itra.Day=icd.CycleDay
						LEFT JOIN (
							SELECT
								SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5
							FROM
								SUBJECT_TERM_CLASS
						) as stc ON itra.SubjectGroupID=stc.SubjectGroupID
						LEFT JOIN (
							SELECT
								LocationID,NameChi,NameEng,DisplayOrder,Code,BarCode,Description
							FROM
								INVENTORY_LOCATION
						) as il ON itra.LocationID = il.LocationID
						LEFT JOIN (
							SELECT
								SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER WHERE UserID='".$userID."'
						) as stct ON itra.SubjectGroupID=stct.SubjectGroupID
						LEFT JOIN (
							SELECT
								UserID,EnglishName,ChineseName
							FROM
								".$slrsConfig['INTRANET_USER']."
							WHERE
								UserID='".$userID."'
						) as iu ON stct.UserID=iu.UserID
						LEFT JOIN (
							SELECT
								SubjectTermID,SubjectGroupID,SubjectID,YearTermID
							FROM
								SUBJECT_TERM
						) as st ON (itra.SubjectGroupID = st.SubjectGroupID and isla.SubjectGroupID=st.SubjectGroupID)";
						
				$sql = "SELECT
							itt.TimeSlotID, TimeSlotName, LEFT(StartTime,5) as StartTime, LEFT(EndTime,5) as EndTime, itt.DisplayOrder, RoomAllocationID, Day, a.LocationID, a.SubjectGroupID,
							OthersLocation, RecordDate, CycleDay, ClassCode, ClassTitleEN, ClassTitleB5, NameChi, NameEng, Code, BarCode, a.UserID, a.EnglishName, a.ChineseName, SubjectTermID,
							a.SubjectID, islUserID, LeaveID, LessonArrangementID, ArrangedTo_UserID, ArrangedTo_LocationID, a.YearTermID, ".$name_field." as ArrangedTo_UserName,
							YearClassTitleEN, YearClassTitleB5, stcuUser, a.LocationDesc
						FROM (
							SELECT
								TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT WHERE SessionType like '%'
								AND TimeSlotID IN (".$academicTimeSlot.")
							) as itt
						LEFT JOIN(
							" . $sub_sql . "
						) as a ON itt.TimeSlotID=a.TimeSlotID
						LEFT JOIN(
							SELECT
								UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM ".$slrsConfig['INTRANET_USER']."
						) as iu ON a.ArrangedTo_UserID=iu.UserID
						LEFT JOIN(
							SELECT
								SubjectGroupID,MIN(UserID) as stcuUser FROM SUBJECT_TERM_CLASS_USER GROUP BY SubjectGroupID
						) as stcu ON a.SubjectGroupID=stcu.SubjectGroupID ";
				*/
				$sub_sql = "SELECT
							RoomAllocationID, Day, isla.TimeSlotID, itra.LocationID, isla.SubjectGroupID, OthersLocation, RecordDate, CycleDay, ClassCode, ClassTitleEN, ClassTitleB5,
							NameChi, NameEng, DisplayOrder, Code, BarCode, SubjectClassTeacherID, stct.UserID, EnglishName, ChineseName,
							SubjectTermID, st.SubjectID, st.YearTermID, isl.LeaveID, islUserID, DateLeave, LessonArrangementID, ArrangedTo_UserID, ArrangedTo_LocationID, il.Description as LocationDesc
						FROM (
							SELECT
								LessonArrangementID, UserID, LeaveID, TimeSlotID, LocationID, ArrangedTo_UserID, ArrangedTo_LocationID, SubjectGroupID
							FROM
								INTRANET_SLRS_LESSON_ARRANGEMENT where UserID='".$userID."' and Date(TimeStart)='".$date."'
						) as isla
						LEFT JOIN (
							SELECT
								LeaveID,UserID as islUserID,DateLeave
							FROM
								INTRANET_SLRS_LEAVE WHERE DateLeave='".$date."'
						) as isl ON isl.islUserID=isla.UserID AND isl.LeaveID=isla.LeaveID
						LEFT JOIN (

							SELECT
								RoomAllocationID, Day, TimeSlotID, LocationID, SubjectGroupID, OthersLocation
							FROM
								INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day='".$day."' AND TimetableID='".$curTimetableId."'

						) as itra ON (itra.TimeSlotID=isla.TimeSlotID AND itra.SubjectGroupID=isla.SubjectGroupID)
						LEFT JOIN (" . $this->getDaySQL($dateArr,"") . ") as icd ON itra.Day=icd.CycleDay
						LEFT JOIN (
							SELECT
								SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5
							FROM
								SUBJECT_TERM_CLASS
						) as stc ON isla.SubjectGroupID=stc.SubjectGroupID
						LEFT JOIN (
							SELECT
								LocationID,NameChi,NameEng,DisplayOrder,Code,BarCode,Description
							FROM
								INVENTORY_LOCATION
						) as il ON itra.LocationID = il.LocationID
						LEFT JOIN (
							SELECT
								SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER WHERE UserID='".$userID."'
						) as stct ON isla.SubjectGroupID=stct.SubjectGroupID
						LEFT JOIN (
							SELECT
								UserID,EnglishName,ChineseName
							FROM
								".$slrsConfig['INTRANET_USER']."
							WHERE
								UserID='".$userID."'
						) as iu ON stct.UserID=iu.UserID
						LEFT JOIN (
							SELECT
								SubjectTermID,SubjectGroupID,SubjectID,YearTermID
							FROM
								SUBJECT_TERM
						) as st ON (isla.SubjectGroupID=st.SubjectGroupID)";
				
				$sql = "SELECT
							itt.TimeSlotID, TimeSlotName, LEFT(StartTime,5) as StartTime, LEFT(EndTime,5) as EndTime, itt.DisplayOrder, RoomAllocationID, Day, a.LocationID, a.SubjectGroupID,
							OthersLocation, RecordDate, CycleDay, ClassCode, ClassTitleEN, ClassTitleB5, NameChi, NameEng, Code, BarCode, a.UserID, a.EnglishName, a.ChineseName, SubjectTermID,
							a.SubjectID, islUserID, LeaveID, LessonArrangementID, ArrangedTo_UserID, ArrangedTo_LocationID, a.YearTermID, ".$name_field." as ArrangedTo_UserName,
							YearClassTitleEN, YearClassTitleB5, stcuUser, a.LocationDesc
						FROM (
							SELECT
								TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT WHERE SessionType like '%'
								AND TimeSlotID IN (".$academicTimeSlot.")
							) as itt
						LEFT JOIN(
							" . $sub_sql . "
						) as a ON itt.TimeSlotID=a.TimeSlotID
						LEFT JOIN(
							SELECT
								UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM ".$slrsConfig['INTRANET_USER']."
						) as iu ON a.ArrangedTo_UserID=iu.UserID
						LEFT JOIN(
							SELECT
								SubjectGroupID,MIN(UserID) as stcuUser FROM SUBJECT_TERM_CLASS_USER GROUP BY SubjectGroupID
						) as stcu ON a.SubjectGroupID=stcu.SubjectGroupID ";
				
				if ($withNullClass) {
					$sql .= " LEFT ";
				}
				$sql .= " JOIN(
							SELECT UserID,YearClassID FROM YEAR_CLASS_USER WHERE YearClassID IN (".$yearClass.")
						) as ycu ON stcu.stcuUser=ycu.UserID
						LEFT JOIN(
							SELECT YearClassID,ClassTitleEN as YearClassTitleEN,ClassTitleB5 as YearClassTitleB5 FROM YEAR_CLASS
						) as yc ON ycu.YearClassID=yc.YearClassID";
				$sql .= " WHERE LeaveID IS NOT NULL ORDER BY DisplayOrder";
				$result = $this->returnResultSet($sql);
			}
			if($this->isEJ()){
				foreach((array) $result as $_key => $_infoArr){
					$result[$_key]['TimeSlotName'] = convert2unicode($_infoArr['TimeSlotName'],1,1);
				}
			}
			return $result;
		}
		
		function getAssignedTimetableInformation($yearClass=null, $date=null, $day=null, $userID=null, $academicTimeSlot=null, $name_field=null, $withNullClass=true)
		{
		    global $slrsConfig, $PATH_WRT_ROOT;
		    if($this->isEJ())
		    {
		        include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
		    }
		    else
		    {
		        include_once($PATH_WRT_ROOT."includes/libtimetable.php");
		    }
		    $ltimetable = new Timetable();
		    $curTimetableId = $ltimetable->Get_Current_Timetable($date);
		    $result = null;
		    if ($yearClass != null && $date != null && $day != null && $userID != null && $academicTimeSlot != null && $name_field != null) {
		        $dateArr[0] = $date;
		        
		        $sub_sql = "SELECT
							RoomAllocationID, Day, isla.TimeSlotID, itra.LocationID, isla.SubjectGroupID, OthersLocation, RecordDate, CycleDay, ClassCode, ClassTitleEN, ClassTitleB5,
							NameChi, NameEng, DisplayOrder, Code, BarCode, SubjectClassTeacherID, stct.UserID, EnglishName, ChineseName,
							SubjectTermID, st.SubjectID, st.YearTermID, isl.LeaveID, islUserID, DateLeave, LessonArrangementID, ArrangedTo_UserID, ArrangedTo_LocationID, il.Description as LocationDesc
						FROM (
							SELECT
								LessonArrangementID, UserID, LeaveID, TimeSlotID, LocationID, ArrangedTo_UserID, ArrangedTo_LocationID, SubjectGroupID
							FROM
								INTRANET_SLRS_LESSON_ARRANGEMENT where ArrangedTo_UserID='".$userID."' and Date(TimeStart)='".$date."'
						) as isla
						LEFT JOIN (
							SELECT
								LeaveID,UserID as islUserID,DateLeave
							FROM
								INTRANET_SLRS_LEAVE WHERE DateLeave='".$date."'
						) as isl ON isl.islUserID=isla.UserID AND isl.LeaveID=isla.LeaveID
						LEFT JOIN (
								    
							SELECT
								RoomAllocationID, Day, TimeSlotID, LocationID, SubjectGroupID, OthersLocation
							FROM
								INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day='".$day."' AND TimetableID='".$curTimetableId."'
								    
						) as itra ON (itra.TimeSlotID=isla.TimeSlotID AND itra.SubjectGroupID=isla.SubjectGroupID)
						LEFT JOIN (" . $this->getDaySQL($dateArr,"") . ") as icd ON itra.Day=icd.CycleDay
						LEFT JOIN (
							SELECT
								SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5
							FROM
								SUBJECT_TERM_CLASS
						) as stc ON isla.SubjectGroupID=stc.SubjectGroupID
						LEFT JOIN (
							SELECT
								LocationID,NameChi,NameEng,DisplayOrder,Code,BarCode,Description
							FROM
								INVENTORY_LOCATION
						) as il ON itra.LocationID = il.LocationID
						LEFT JOIN (
							SELECT
								SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER
						) as stct ON isla.SubjectGroupID=stct.SubjectGroupID and stct.UserID=isl.islUserID
						LEFT JOIN (
							SELECT
								UserID,EnglishName,ChineseName
							FROM
								".$slrsConfig['INTRANET_USER']."
						) as iu ON stct.UserID=iu.UserID
						LEFT JOIN (
							SELECT
								SubjectTermID,SubjectGroupID,SubjectID,YearTermID
							FROM
								SUBJECT_TERM
						) as st ON (isla.SubjectGroupID=st.SubjectGroupID)";
		        $sql = "SELECT
							itt.TimeSlotID, TimeSlotName, LEFT(StartTime,5) as StartTime, LEFT(EndTime,5) as EndTime, itt.DisplayOrder, RoomAllocationID, Day, a.LocationID, a.SubjectGroupID,
							OthersLocation, RecordDate, CycleDay, ClassCode, ClassTitleEN, ClassTitleB5, NameChi, NameEng, Code, BarCode, a.UserID, a.EnglishName, a.ChineseName, SubjectTermID,
							a.SubjectID, islUserID, LeaveID, LessonArrangementID, ArrangedTo_UserID, ArrangedTo_LocationID, a.YearTermID, ".$name_field." as ArrangedTo_UserName,
							YearClassTitleEN, YearClassTitleB5, stcuUser, a.LocationDesc
						FROM (
							SELECT
								TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT WHERE SessionType like '%'
								AND TimeSlotID IN (".$academicTimeSlot.")
							) as itt
						LEFT JOIN(
							" . $sub_sql . "
						) as a ON itt.TimeSlotID=a.TimeSlotID
						LEFT JOIN(
							SELECT
								UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM ".$slrsConfig['INTRANET_USER']."
						) as iu ON a.islUserID=iu.UserID
						LEFT JOIN(
							SELECT
								SubjectGroupID,MIN(UserID) as stcuUser FROM SUBJECT_TERM_CLASS_USER GROUP BY SubjectGroupID
						) as stcu ON a.SubjectGroupID=stcu.SubjectGroupID ";
		        
		        if ($withNullClass) {
		            $sql .= " LEFT ";
		        }
		        $sql .= " JOIN(
							SELECT UserID,YearClassID FROM YEAR_CLASS_USER WHERE YearClassID IN (".$yearClass.")
						) as ycu ON stcu.stcuUser=ycu.UserID
						LEFT JOIN(
							SELECT YearClassID,ClassTitleEN as YearClassTitleEN,ClassTitleB5 as YearClassTitleB5 FROM YEAR_CLASS
						) as yc ON ycu.YearClassID=yc.YearClassID";
		        $sql .= " WHERE LeaveID IS NOT NULL ORDER BY DisplayOrder";
		        $result = $this->returnResultSet($sql);
		    }
		    if($this->isEJ()){
		        foreach((array) $result as $_key => $_infoArr){
		            $result[$_key]['TimeSlotName'] = convert2unicode($_infoArr['TimeSlotName'],1,1);
		        }
		    }
		    return $result;
		}
		
		/*
		 * Convert Array key to useful ID (*** lib.php > BuildMultiKeyAssoc ***)
		 */
		function convertArrKeyToID($assArr, $keyname = "ID") {
			$outputArr = array();
			if (count($assArr) > 0) {
				foreach ($assArr as $key => $_RECORD) {
					if (isset($_RECORD[$keyname])) {
						$outputArr[$_RECORD[$keyname]] = $_RECORD;
					}
				}
			}
			return $outputArr;
		}
		/*
		 * Custom format between Array and String
		 */
		function custFormat($args, $type = "arrToStr") {
			$sptr = "|";
			$val_sptr = ": ";
			switch ($type) {
				case "strToArr":
					$output = array();
					if (is_string($args)) {
						$tmp = explode($sptr, $args);
						if (count($tmp) > 0) {
							foreach ($tmp as $key => $_RECORD) {
								$val_tmp = explode($val_sptr, $_RECORD);
								if (count($val_tmp) == 2) {
									$output[trim($val_tmp[0])] = trim($val_tmp[1]);
								}
							}
							if (count($output) > 0) {
								return $output;
							}
						}
					}
					break;
				default: {
					$output = "";
					if (is_array($args) && count($args) > 0) {
						foreach ($args as $key => $_RECORD) {
							if (!empty($output)) {
								$output .= $sptr;
							}
							$output .= $key . $val_sptr . $_RECORD;
						}
						return $output;
					}
				}
			}
			return false;
		}
		
		/*
		 * Save Push Message Log to MODULE RECORD Update Log Table
		 */
		function saveNotifyInfo($date, $CycleDay, $userId, $notifyMessageId, $recordDetailArr, $section = "StaffMgmtSLRSReportsNotify", $mod = "SLRS") {
			if (!empty($notifyMessageId) && (count($recordDetailArr) > 0) && !empty($section) && !empty($mod)) {
				$sql = "INSERT INTO INTRANET_SLRS_PUSH_MESSAGE_LOG";
				$sql .= " (NotifyMessageId, NotifyDate, CycleDay, Module, Section, LogBy, LogDate) VALUES ";
				$sql .= "('" . $notifyMessageId . "', '" . $date . "', '" . $CycleDay . "', '" . $mod . "', '" . $section . "', '" . $userId . "', NOW())";
				$this->db_db_query($sql);
				$NotifyMessageLogID = $this->db_insert_id();
				if ($NotifyMessageLogID > 0) {
					if (count($recordDetailArr) > 0) {
						foreach($recordDetailArr as $substUserID => $subStUserInfo) {
							if (count($subStUserInfo) > 0) {
								foreach ($subStUserInfo as $leaveTechID => $leaveTechRemark) {
									$sql = "INSERT INTO INTRANET_SLRS_PUSH_MESSAGE_LOG_USER ";
									$sql .= "(NotifyMessageLogID, LeaveTeacherID, SubstituteTeacherID, Remarks) VALUES";
									$sql .= "('" . $NotifyMessageLogID . "', '" . $leaveTechID . "', '" . $substUserID . "', '" . $leaveTechRemark . "')";
									$this->db_db_query($sql);
								}
							}
						}
					}
				}
				return $NotifyMessageLogID;
			}
			return 0;
		}
		
		/*
		 * get Staff Name by UserID (String or Array)
		 */
		function getStaffNameById($args){
			global $slrsConfig;
			$sql = "SELECT UserID, ChineseName, EnglishName, DisplayName, UserLogin
						FROM ".$slrsConfig['INTRANET_USER']."";
			if (is_array($args)) {
				$sql .= " WHERE UserID in (" . implode(",", $args) . ")";
			} else {
				$sql .= " WHERE UserID='" . $args . "'";
			}
			return $this->returnResultSet($sql);
		}
		
		/*
		 * get Notification status by NotifyMessageID, UserID (String or Array)
		 */
		function getNotifyStatusById($notifyMessageID, $userID) {
			$sql = "SELECT mref.MessageStatus, mref.ServiceProvider, mrefto.UserID FROM ";
			$sql .= " INTRANET_APP_NOTIFY_MESSAGE_REFERENCE as mref ";
			$sql .= ", INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO as mrefto ";
			$sql .= " WHERE mref.NotifyMessageID='" . $notifyMessageID . "' and mref.NotifyMessageTargetID=mrefto.NotifyMessageTargetID";
			if (is_array($userID)) {
				$sql .= " AND mrefto.UserID in (" . implode(",", $userID) . ")";
			} else {
				$sql .= " AND mrefto.UserID = '" . $userID . "'";
			}
			return $this->returnResultSet($sql);
		}
		
		/*
		 * get Notification Information
		 * Argument is controlled by Controller)
		 */
		function getAppNotifyInfo($NotifyDate = "", $LeaveTeacher = null, $section = "DailySubstitutionReportNotify", $mod = "SLRS", $reportTarget='LeaveTeacher') {
			$notifyInfo = null;
			if (!empty($NotifyDate)) {
				$sql = "SELECT
							NotifyMessageLogID, NotifyMessageId, NotifyDate, CycleDay, Module, Section, LogBy, LogDate
						FROM
							INTRANET_SLRS_PUSH_MESSAGE_LOG
						WHERE
							Module = '" . $mod . "'
							AND Section = '" . $section . "'
							AND NotifyDate like '" . $NotifyDate . "'
						";
				$sql .= "ORDER BY NotifyDate desc, LogDate desc";
				$notifyData = $this->returnResultSet($sql);
				if (count($notifyData) > 0) {
					foreach ($notifyData as $key => $val) {
						$notifyInfo["logInfo"][$val["NotifyMessageLogID"]] = $val;
						$sql = "SELECT
									NotifyMessageRelateID, LeaveTeacherID, SubstituteTeacherID, Remarks
								FROM
									INTRANET_SLRS_PUSH_MESSAGE_LOG_USER
								WHERE
									NotifyMessageLogID='" . $val["NotifyMessageLogID"] . "'";
						if ($LeaveTeacher != null && $LeaveTeacher != "all") {
						    if ($reportTarget == "AssignedTeacher")
						    {
						        $sql .= " AND SubstituteTeacherID in (" . $LeaveTeacher . ")";
						    } else {
						        $sql .= " AND LeaveTeacherID in (" . $LeaveTeacher . ")";
						    }
							
						}
						$result = $this->returnResultSet($sql);
						if (count($result) > 0) {
							foreach ($result as $teacherIndex => $teacherRelated) {
								$notifyInfo["userArr"][] = $val["LogBy"];
								if (!empty($teacherRelated["LeaveTeacherID"])) $notifyInfo["userArr"][] =  $teacherRelated["LeaveTeacherID"];
								if (!empty($teacherRelated["SubstituteTeacherID"])) $notifyInfo["userArr"][] =  $teacherRelated["SubstituteTeacherID"];
								$notifyInfo["logInfo"][$val["NotifyMessageLogID"]]["SubstInfo"][$teacherRelated["SubstituteTeacherID"]]["LeaveTeacher"][$teacherRelated["LeaveTeacherID"]] = $teacherRelated;
							}
						}
					}
					if (count($notifyInfo["userArr"]) > 0) $notifyInfo["userArr"] = array_unique($notifyInfo["userArr"]);
				}
			}
			return $notifyInfo;
		}
		
		/*
		 * get Push Message Status by Message Status (int)
		 */
		function getPushMessageStatusDisplay($status) {
			global $eclassAppConfig, $Lang;
			
			$statusLang = '';
			if ((string)$status == (string)$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']) {
				$statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['sendFailed'];
			}
			else if ((string)$status == (string)$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess']) {
				$statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['sendSuccess'];
			}
			else if ((string)$status == (string)$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']) {
				$statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['userHasRead'];
			}
			else if ((string)$status == (string)$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend']) {
				$statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['waitingToSend'];
			}
			else if ((string)$status == (string)$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice']) {
				$statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['noRegisteredDevice'];
			}
			else if ((string)$status == (string)$eclassAppConfig['errorCode']['pushMessage']['cannotConnectToServiceProvider'] || (string)$status == (string)$eclassAppConfig['errorCode']['pushMessage']['cannotConnectToCentralServer']) {
				$statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['sendFailed'].' ('.$Lang['MessageCentre']['pushMessageAry']['errorStatusAry']['cannotConnectToServiceProvider'].')';
			}
			
			return $statusLang;
		}
		
		function getNameFieldByLang($prefix="", $isTitleDisabled=true){
			
			global $intranet_session_language;
			
			$displayLang = $displayLang ? $displayLang : $intranet_session_language;
			$chi = ($displayLang =="b5" || $displayLang == "gb");
			$name_field = getNameFieldByLang2($prefix, $displayLang);
			if($isTitleDisabled){
				$field = " TRIM(CONCAT($name_field))";
			}
			else{
				if ($chi)
				{
					$field = "	TRIM(CONCAT($name_field, IF($prefix"."TitleChinese!='', CONCAT('', $prefix"."TitleChinese), '')))";
				}
				else
				{
					$field = "TRIM(CONCAT( IF($prefix"."TitleEnglish!='', CONCAT($prefix"."TitleEnglish,' ') ,  ''),$name_field))";
				}
			}
			return $field;
		}
		
		function displayChinese($chinese){
			if($this->isEJ()){
				return convert2unicode($chinese,1,1);
			}else{
				return $chinese;
			}
		}
		
		function getArrangementInfoForTimetable($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr, $isAssign = false) {
			$SLRS_Arrangeinfo = array();
			
			//  			$strSQL = "SELECT UserID FROM " . $slrsConfig['INTRANET_USER'] . " WHERE UserID='" . $targetUserID . "' AND RecordStatus='1' AND RecordType='2'";
			//  			$result = $this->returnResultSet($strSQL);
			
			$strSQL = "SELECT ArrangedTo_UserID, LocationID, ArrangedTo_LocationID, TimeSlotID, SubjectGroupID";
			$strSQL .= ", ui.EnglishName, ui.ChineseName, ui.UserLogin";
			$strSQL .= ", arrra_ui.EnglishName as ArrEnglishName, arrra_ui.ChineseName as ArrChineseName, arrra_ui.UserLogin as ArrUserLogin";
			$strSQL .= " FROM INTRANET_SLRS_LESSON_ARRANGEMENT AS slrs_arrange";
			$strSQL .= " LEFT JOIN INTRANET_SLRS_LEAVE AS slrs_leave ON (slrs_leave.LeaveID=slrs_arrange.LeaveID)";
			$strSQL .= " JOIN " . $slrsConfig['INTRANET_USER'] . " AS ui ON (ui.UserID=slrs_arrange.UserID)";
			$strSQL .= " LEFT JOIN " . $slrsConfig['INTRANET_USER'] . " AS arrra_ui ON (arrra_ui.UserID=slrs_arrange.ArrangedTo_UserID)";
			
			if ($isAssign && !empty($targetUserID)) {
				$strSQL .= " WHERE TimeSlotID='" . $thisTimeSlotID . "' AND DATE(TimeStart)='" . $accDate . "' AND (slrs_arrange.UserID='" . $targetUserID . "' OR slrs_arrange.ArrangedTo_UserID='" . $targetUserID . "')";
			} else {
				/* is Student */
				$strSQL .= " WHERE TimeSlotID='" . $thisTimeSlotID . "' AND DATE(TimeStart)='" . $accDate . "' AND SubjectGroupID IN ('" . implode("', '", $SubjectGroupID_DisplayFilterArr) . "')";
			}
			$result = $this->returnResultSet($strSQL);
			
			if (count($result) > 0) {
				$SLRS_Arrangeinfo = $result;
				foreach ( $result as $kk => $val) {
					$SLRS_Arrangeinfo[$kk]["locationArr"][] = $val["LocationID"];
					$SLRS_Arrangeinfo[$kk]["locationArr"][] = $val["ArrangedTo_LocationID"];
					$SLRS_Arrangeinfo[$kk]["locationInfo"] = $this->getLocationInfoByID($SLRS_Arrangeinfo[$kk]["locationArr"]);
				}
			}
			return $SLRS_Arrangeinfo;
		}
		
		function isLeaveTimeSlot($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr) {
			$strSQL = "SELECT UserID FROM " . $slrsConfig['INTRANET_USER'] . " WHERE UserID='" . $targetUserID . "' AND RecordStatus='1' AND RecordType='2'";
			$result = $this->returnResultSet($strSQL);
			/* is Student */
			if ($result) {
				$strSQL = "SELECT ArrangedTo_UserID, LocationID, ArrangedTo_LocationID, TimeSlotID, SubjectGroupID";
				$strSQL .= ", ui.EnglishName, ui.ChineseName, ui.UserLogin";
				$strSQL .= ", arrra_ui.EnglishName as ArrEnglishName, arrra_ui.ChineseName as ArrChineseName, arrra_ui.UserLogin as ArrUserLogin";
				$strSQL .= " FROM INTRANET_SLRS_LESSON_ARRANGEMENT AS slrs_arrange";
				$strSQL .= " LEFT JOIN INTRANET_SLRS_LEAVE AS slrs_leave ON (slrs_leave.LeaveID=slrs_arrange.LeaveID)";
				$strSQL .= " JOIN " . $slrsConfig['INTRANET_USER'] . " AS ui ON (ui.UserID=slrs_arrange.UserID)";
				$strSQL .= " JOIN " . $slrsConfig['INTRANET_USER'] . " AS arrra_ui ON (arrra_ui.UserID=slrs_arrange.ArrangedTo_UserID)";
				$strSQL .= " WHERE TimeSlotID='" . $thisTimeSlotID . "' AND DateLeave='" . $accDate . "' AND SubjectGroupID IN ('" . implode("', '", $SubjectGroupID_DisplayFilterArr) . "')";
			} else {
				$strSQL = "SELECT ArrangedTo_UserID, LocationID, ArrangedTo_LocationID, TimeSlotID, SubjectGroupID";
				$strSQL .= ", ui.EnglishName, ui.ChineseName, ui.UserLogin";
				$strSQL .= ", arrra_ui.EnglishName as ArrEnglishName, arrra_ui.ChineseName as ArrChineseName, arrra_ui.UserLogin as ArrUserLogin";
				$strSQL .= " FROM INTRANET_SLRS_LESSON_ARRANGEMENT AS slrs_arrange";
				$strSQL .= " LEFT JOIN INTRANET_SLRS_LEAVE AS slrs_leave ON (slrs_leave.LeaveID=slrs_arrange.LeaveID)";
				$strSQL .= " JOIN " . $slrsConfig['INTRANET_USER'] . " AS ui ON (ui.UserID=slrs_arrange.UserID)";
				$strSQL .= " JOIN " . $slrsConfig['INTRANET_USER'] . " AS arrra_ui ON (arrra_ui.UserID=slrs_arrange.ArrangedTo_UserID)";
				$strSQL .= " WHERE slrs_arrange.UserID='" . $targetUserID . "' AND TimeSlotID='" . $thisTimeSlotID . "' AND DateLeave='" . $accDate . "' LIMIT 1";
			}
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				return true;
			}
			return false;
		}
		
		function getExchangeInfoForTimetable($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr, $exchangeTargetCheck = "B") {
			$SLRS_Arrangeinfo = array();
			
			$strSQL = "SELECT TeacherA_UserID as UserID, TeacherB_LocationID as LocationID, TeacherA_TimeSlotID as TimeSlotID, TeacherA_SubjectGroupID as SubjectGroupID";
			$strSQL .= ", TeacherB_UserID as B_UserID, TeacherB_LocationID as B_LocationID, TeacherB_TimeSlotID as B_TimeSlotID, TeacherB_SubjectGroupID as B_SubjectGroupID";
			$strSQL .= ", ui.EnglishName, ui.ChineseName, ui.UserLogin";
			$strSQL .= ", arrra_ui.EnglishName as ArrEnglishName, arrra_ui.ChineseName as ArrChineseName, arrra_ui.UserLogin as ArrUserLogin";
			$strSQL .= " FROM INTRANET_SLRS_LESSON_EXCHANGE AS slrs_exchange";
			$strSQL .= " JOIN " . $slrsConfig['INTRANET_USER'] . " AS ui ON (ui.UserID=slrs_exchange.TeacherA_UserID)";
			$strSQL .= " JOIN " . $slrsConfig['INTRANET_USER'] . " AS arrra_ui ON (slrs_exchange.TeacherB_UserID=arrra_ui.UserID)";
			// $strSQL .= " WHERE TeacherA_TimeSlotID='" . $thisTimeSlotID . "' AND TeacherB_Date='" . $accDate . "'";
			if ($exchangeTargetCheck == "A") {
				// check orginal class information
				$strSQL .= " WHERE TeacherA_SubjectGroupID in ('" . implode("', '", $SubjectGroupID_DisplayFilterArr) . "')
								   AND TeacherA_TimeSlotID='" . $thisTimeSlotID . "' AND TeacherA_Date='" . $accDate . "'";
			} else {
				// Exchanged Class Information
				$strSQL .= " WHERE TeacherA_SubjectGroupID in ('" . implode("', '", $SubjectGroupID_DisplayFilterArr) . "')
								   AND TeacherB_TimeSlotID='" . $thisTimeSlotID . "' AND TeacherB_Date='" . $accDate . "'";
			}
			$strSQL .= " GROUP BY UserID, LocationID, TimeSlotID, SubjectGroupID ";
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				$SLRS_Arrangeinfo = $result;
				foreach ( $result as $kk => $val) {
					$SLRS_Arrangeinfo[$kk]["locationArr"][] = $val["LocationID"];
					$SLRS_Arrangeinfo[$kk]["locationInfo"] = $this->getLocationInfoByID($SLRS_Arrangeinfo[$kk]["locationArr"]);
				}
			}
			return $SLRS_Arrangeinfo;
		}
		
		function getCancelLessonInfoForTimetable($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr, $isAssign = false)
		{
		    $SLRS_CancelLessonInfo = array();
		    $subjectGroupName = Get_Lang_Selection("ClassTitleB5", "ClassTitleEN");
		    $TeacherName = Get_Lang_Selection("ChineseName", "EnglishName");
		    $SubjectName = Get_Lang_Selection("CH_DES", "EN_DES");
		    $strSQL = "SELECT ";
		    $strSQL .= " CancelDate,
                        " . $TeacherName. " as TeacherName,
                        CONCAT(TimeSlotName, ' (', LEFT(StartTime, 5), '-', LEFT(EndTime, 5), ')') as Timeslot,
                        " . $subjectGroupName . " as LessonInfo,
                        CancelRemark,
                       " . $SubjectName . " as Subject
            ";
		    $strSQL .= " FROM INTRANET_SLRS_CANCEL_LESSON AS iscl ";
		    $strSQL .= " INNER JOIN SUBJECT_TERM_CLASS AS stc ON (stc.SubjectGroupID=iscl.SubjectGroupID)";
		    $strSQL .= " JOIN " . $slrsConfig['INTRANET_USER']. " AS iu ON (iu.UserID=iscl.UserID)";
		    $strSQL .= " JOIN INTRANET_TIMETABLE_TIMESLOT AS itt ON (itt.TimeSlotID=iscl.TimeSlotID)";
		    $strSQL .= " LEFT JOIN ASSESSMENT_SUBJECT AS sj ON (sj.RecordID=iscl.SubjectID)";
		    $strSQL .= " WHERE DeletedAt IS NULL AND 
                            iscl.TimeSlotID='" . $thisTimeSlotID . "' AND
                            CancelDate='" . $accDate . "' AND
                            iscl.SubjectGroupID IN ('" . implode("', '", $SubjectGroupID_DisplayFilterArr) . "')
                        ";
		    $result = $this->returnResultSet($strSQL);
		    if (count($result) > 0) {
		        $SLRS_CancelLessonInfo = $result;
		    }
		    
		    return $SLRS_CancelLessonInfo;
		}
		
		function isExchangeTimeSlot($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr) {
			$SLRS_Arrangeinfo = array();
			
			$strSQL = "SELECT TeacherA_UserID as UserID, TeacherB_LocationID as LocationID, TeacherA_TimeSlotID as TimeSlotID, TeacherA_SubjectGroupID as SubjectGroupID";
			$strSQL .= ", TeacherB_UserID as B_UserID, TeacherB_LocationID as B_LocationID, TeacherB_TimeSlotID as B_TimeSlotID, TeacherB_SubjectGroupID as B_SubjectGroupID";
			$strSQL .= ", ui.EnglishName, ui.ChineseName, ui.UserLogin";
			$strSQL .= ", arrra_ui.EnglishName as ArrEnglishName, arrra_ui.ChineseName as ArrChineseName, arrra_ui.UserLogin as ArrUserLogin";
			$strSQL .= " FROM INTRANET_SLRS_LESSON_EXCHANGE AS slrs_exchange";
			$strSQL .= " JOIN " . $slrsConfig['INTRANET_USER'] . " AS ui ON (ui.UserID=slrs_exchange.TeacherA_UserID)";
			$strSQL .= " JOIN " . $slrsConfig['INTRANET_USER'] . " AS arrra_ui ON (slrs_exchange.TeacherB_UserID=arrra_ui.UserID)";
			// $strSQL .= " WHERE TeacherA_UserID='" . $targetUserID . "' AND TeacherA_TimeSlotID='" . $thisTimeSlotID . "' AND TeacherB_Date='" . $accDate . "'";
			$strSQL .= " WHERE TeacherA_SubjectGroupID IN ('" . implode("', '", $SubjectGroupID_DisplayFilterArr) . "') AND 
                            TeacherA_TimeSlotID='" . $thisTimeSlotID . "' AND 
                            TeacherA_Date='" . $accDate . "'";
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				return true;
			}
			return false;
		}
		
		function getLocationInfoByID($locationIDs = array()) {
			$result = array();
			if (count($locationIDs) > 0) {
				$strSQL = "SELECT
								b.BuildingID,
								b.Code as BuildingCode,
								b.NameChi as BuildingNameChi,
								b.NameEng as BuildingNameEng,
								f.LocationLevelID,
								f.Code as FloorCode,
								f.NameChi as FloorNameChi,
								f.NameEng as FloorNameEng,
								r.LocationID,
								r.Code as RoomCode,
								r.NameChi as RoomNameChi,
								r.NameEng as RoomNameEng
							FROM
								INVENTORY_LOCATION_BUILDING as b
							INNER JOIN 
								INVENTORY_LOCATION_LEVEL as f ON f.BuildingID = b.BuildingID
							INNER JOIN 
								INVENTORY_LOCATION as r ON r.LocationLevelID = f.LocationLevelID
							WHERE
								b.RecordStatus = 1
								AND f.RecordStatus = 1
								AND r.RecordStatus = 1
								AND r.LocationID in ('" . implode("', '", $locationIDs) . "')
							Order By
								b.DisplayOrder, f.DisplayOrder, r.DisplayOrder
				";
				
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0) {
					$result = BuildMultiKeyAssoc($result, "LocationID");
				} else {
					// echo $strSQL;
				}
			}
			return $result;
		}
		
		
		public function getOfficialLeavelSetting($onlyDisableAdjust = false)
		{
			$sql = "SELECT SettingValue, SettingName FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName like 'OfficialLeavel%'";
			if ($onlyDisableAdjust)
			{
				$sql .= " AND SettingValue='1'";
			}
			$settingsVals = $this->returnResultSet($sql);
			if (count($settingsVals) > 0)
			{
				foreach ($settingsVals as $kk=>$vv)
				{
					$settingsArr[str_replace("OfficialLeavel", '', $vv["SettingName"])] = $vv["SettingValue"];
				}
			}
			return $settingsArr;
		}
		
		/**
		 * update Balance Adjustment
		 * @param Int $UserID
		 * @param Int $AcademicYearID
		 * @param String $AdjustedType
		 * @param String $Reason
		 * @param Int $updateBy
		 * @param string $setVal
		 */
		function updateSLRSBalance($UserID, $AcademicYearID, $AdjustedType, $Reason, $updateBy, $setVal = "", $_balInfo = array()) {
			if ($UserID > 0) {
				$strSQL = "SELECT UserID, Balance FROM INTRANET_SLRS_TEACHER WHERE UserID='" . $UserID . "'";
				$result = $this->returnResultSet($strSQL);
				
				$BalanceBeforeAdjust =(isset($result[0]["Balance"])) ? $result[0]["Balance"] : 0;
				
				switch ($AdjustedType) {
					case "ADD":
						$BalanceAdjustTo = $BalanceBeforeAdjust + 1;
						break;
					case "REDUCE":
						$BalanceAdjustTo = $BalanceBeforeAdjust - 1;
						break;
					case "SET":
						if (is_numeric($isetVal)) $BalanceAdjustTo = $setVal;
						else $BalanceAdjustTo = $BalanceBeforeAdjust;
						break;
					default:
						$BalanceAdjustTo = $BalanceBeforeAdjust;
						break;
				}
				
				$adjustParam = array(
						"UserID" => IntegerSafe($UserID),
						"AcademicYearID" => $AcademicYearID,
						"BalanceBeforeAdjust" => $BalanceBeforeAdjust,
						"BalanceAdjustTo" => $BalanceAdjustTo,
						"AdjustedType"  => $AdjustedType,
						"Reason" => $Reason,
						"InputBy_UserID" => $updateBy,
						"ModifiedBy_UserID" => $updateBy,
				);
				if (count($_balInfo) > 0)
				{
					$tmp = $adjustParam + $_balInfo;
					$adjustParam = $tmp;
				}
				$strSQL = "INSERT INTO INTRANET_SLRS_BALANCE_ADJUST (" . implode(", ", array_keys($adjustParam)) . ", DateTimeInput, DateTimeModified)";
				$strSQL .= " VALUES ";
				$strSQL .= " ('" . implode("', '", array_values($adjustParam)) . "', NOW(), NOW())";
				$result = $this->db_db_query($strSQL);
			}
		}
		
		function GetAllAcademicYear(){
			global $intranet_session_language;
			
			$sql = "SELECT AcademicYearID,
			IF('$intranet_session_language'= 'en', YearNameEN, YearNameB5) As YearName
			FROM ACADEMIC_YEAR ORDER BY Sequence";
			$result = $this->returnArray($sql,2);
			
			return $result;
		}
		
		function getCurrentYear($attrib, $result, $selected="", $firstValue="", $displayAllClassValue=true){
			$x = "<SELECT $attrib>\n";
			$empty_selected = ($selected == '')? "SELECTED":"";
			if($displayAllClassValue)
			{
				$x .= "<OPTION value='' $empty_selected>$firstValue</OPTION>\n";
			}
			
			for ($i=0; $i < sizeof($result); $i++)
			{
				list($id, $name) = $result[$i];
				$selected_str = ($id==$selected? "SELECTED":"");
				if ($this->isEJ()) {
					$x .= "<OPTION value='$id' $selected_str>" . convert2unicode($name) . "</OPTION>\n";
				} else {
					$x .= "<OPTION value='$id' $selected_str>$name</OPTION>\n";
				}
			}
			$x .= "</SELECT>\n";
			
			return $x;
		}
		
		function defaultNoSubstitution($getPrefix = false) {
			global $sys_custom;
			if ($getPrefix) {
				$prefix = "NO_SETTING_HERE";
				if (!empty($sys_custom['SLRS']["defaultNoSubstitutionPrefix"]) || !$sys_custom['SLRS']["defaultNoSubstitution"]) {
					$prefix = trim($sys_custom['SLRS']["defaultNoSubstitutionPrefix"]) . " ";
				}
				return $prefix;
			} else {
				return $sys_custom['SLRS']["defaultNoSubstitution"];
			}
		}
		
		function checkSpecialSubjectGroup($subjectGroupID) {
			$sql = "SELECT SubjectGroupID, ClassCode, ClassTitleEN, ClassTitleB5
						FROM SUBJECT_TERM_CLASS
						WHERE SubjectGroupID=".IntegerSafe($subjectGroupID) . "
						 AND ( ClassTitleEN like '" . $this->defaultNoSubstitution($getPrefix=true) . "%'
							 OR ClassTitleB5 like '" . $this->defaultNoSubstitution($getPrefix=true) . "%') limit 1";
			$result = $this->returnResultSet($sql);
			if (count($result) > 0) {
				return true;
			}
			return false;
		}
		
		function getSubstitutionStatisticsRecordByDate($startDate, $endDate) {
			
			global $slrsConfig;
			$name_field = $this->getNameFieldByLang($slrsConfig['INTRANET_USER'].".");
			
			$sql = "SELECT DateLeave FROM INTRANET_SLRS_LEAVE WHERE DateLeave BETWEEN '".$startDate."' AND '".$endDate."'";
			$a = $this->returnResultSet($sql);
			for($i=0;$i<sizeof($a);$i++){
				$dateArr[$i]=$a[$i]["DateLeave"];
			}
			
			$sql ="
				SELECT isl.LeaveID,isl.UserID,DateLeave,ReasonCode,isla.SubjectGroupID,isla.SubjectID,isla.LocationID,ArrangedTo_UserID,ArrangedTo_LocationID,isla.TimeSlotID,
				ClassCode,ClassTitleEN,ClassTitleB5,CycleDay,TimeSlotName,UserName,AssignedUserName,isVolunteer, isla.LessonArrangementID
				FROM(
					SELECT LeaveID,UserID,DateLeave,ReasonCode FROM INTRANET_SLRS_LEAVE WHERE DateLeave BETWEEN '".$startDate."' AND '".$endDate."'
				) as isl
				LEFT JOIN (
					SELECT UserID,LeaveID,SubjectGroupID,SubjectID,LocationID,ArrangedTo_UserID,ArrangedTo_LocationID,TimeSlotID,isVolunteer, LessonArrangementID
					FROM INTRANET_SLRS_LESSON_ARRANGEMENT
				) as isla ON (isl.UserID=isla.UserID AND isl.LeaveID=isla.LeaveID)
				LEFT JOIN (
					SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS
				) as stc ON isla.SubjectGroupID=stc.SubjectGroupID
				LEFT JOIN  (".$this->getDaySQL($dateArr,"").") as icd ON isl.DateLeave=icd.RecordDate
				LEFT JOIN(
					SELECT TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT
				) as itt ON isla.TimeSlotID=itt.TimeSlotID
				LEFT JOIN(
					SELECT SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_USER
				) as stcu ON isla.SubjectGroupID=stcu.SubjectGroupID AND isla.UserID=stcu.UserID
				LEFT JOIN(
					SELECT UserID,".$name_field." as UserName FROM ".$slrsConfig['INTRANET_USER']."
				) as iu ON isl.UserID=iu.UserID
				LEFT JOIN(
					SELECT UserID,".$name_field." as AssignedUserName FROM ".$slrsConfig['INTRANET_USER']."
					) as iua ON isla.ArrangedTo_UserID=iua.UserID
				ORDER BY DateLeave,UserName,TimeSlotName";

			$result = $this->returnResultSet($sql);
			if (count($result) > 0)
			{
				$final_result = array();
				foreach ($result as $key => $val)
				{
				    $val["TimeSlotName"] = $this->isEJ() ? convert2unicode($val["TimeSlotName"]) : $val["TimeSlotName"];
					if (empty($val["LessonArrangementID"]))
					{
						$strSQL = "SELECT
									*
									FROM
										INTRANET_SLRS_LESSON_EXCHANGE as isle
									LEFT JOIN (
										SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS
									) as stc ON isle.TeacherA_SubjectGroupID=stc.SubjectGroupID
									LEFT JOIN  (".$this->getDaySQL(array($val["DateLeave"]),"").") as icd ON TeacherA_Date=icd.RecordDate
									LEFT JOIN(
										SELECT TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT
									) as itt ON isle.TeacherA_TimeSlotID=itt.TimeSlotID
									LEFT JOIN(
										SELECT SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_USER
									) as stcu ON isle.TeacherA_SubjectGroupID=stcu.SubjectGroupID AND isle.TeacherA_UserID=stcu.UserID
									LEFT JOIN(
										SELECT UserID,".$name_field." as UserName FROM ".$slrsConfig['INTRANET_USER']."
									) as iu ON TeacherA_UserID=iu.UserID
									WHERE
										TeacherA_UserID='" . $val["UserID"] . "' AND TeacherA_Date='" . $val["DateLeave"] . "'";
						$tmp = $this->returnResultSet($strSQL);
						if (count($tmp) > 0) {
							foreach ($tmp as $kk => $vv)
							{
								$tmpFull = $val;
								$updateInfo = array(
										"SubjectGroupID" => $vv["TeacherA_SubjectGroupID"],
										"SubjectID" => $vv["TeacherA_SubjectID"],
										"LocationID" => $vv["TeacherA_LocationID"],
										"ArrangedTo_UserID" => "-999",
										"ArrangedTo_LocationID" => "",
										"TimeSlotID" => $vv["TeacherA_TimeSlotID"],
										"ClassCode" => $vv["ClassCode"],
										"ClassTitleEN" => $vv["ClassTitleEN"],
										"ClassTitleB5" => $vv["ClassTitleB5"],
										"CycleDay" => $vv["CycleDay"],
							            "TimeSlotName" => $this->isEJ() ? convert2unicode($vv["TimeSlotName"]) : $vv["TimeSlotName"],
										"UserName" => $vv["UserName"],
										"AssignedUserName" => "",
										"isVolunteer" => "",
										"LessonArrangementID" => "",
										"LessonExchangeID" => $vv["LessonExchangeID"]
								);
								$final_result[] = array_merge($tmpFull, $updateInfo);
							}
						}
					}
					else
					{
						$final_result[] = $val;
					}
				}
			}
			return $final_result;
		}
		
		
		function sqlOpt($sql) {
			$replaceArr = array( "\n", "\t", "\r", "   ", "  ");
			return str_replace($replaceArr, " ", $sql);
		}
		
		function getAllTeachers() {
			global $slrsConfig;
			
			$strSQL = "SELECT UserID, UserLogin, EnglishName, ChineseName FROM " . $slrsConfig['INTRANET_USER'] . " WHERE RecordType='1' AND RecordStatus='1' order by EnglishName ASC";
			$result = $this->returnResultSet($strSQL);
			return $result;
		}
		
		function getAllSLRSAvailableTeachers() {
		    global $slrsConfig;

		    $strSQL = "SELECT a.UserID, UserLogin, EnglishName, ChineseName ";
		    $strSQL .= "From (";
		    $strSQL .= "   SELECT UserID,Balance,Priority,DateTimeInput FROM INTRANET_SLRS_TEACHER WHERE IsActive=1 ";
	        $strSQL .= ") as a ";
	        $strSQL .= " INNER JOIN( SELECT UserID,UserLogin,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM ".$slrsConfig['INTRANET_USER']." WHERE Teaching <> 'S' AND RecordStatus='1' AND RecordType='1' ) as b ON a.UserID=b.UserID";
		    $result = $this->returnResultSet($strSQL);
		    return $result;
		}
		
		function getNotAvailableTeacherByID($rec_id) {
			$strSQL = "SELECT NA_Teacher_ID, UserID, StartDate, EndDate, Reasons FROM INTRANET_SLRS_NOT_AVAILABLE_TEACHERS WHERE NA_Teacher_ID='" . $rec_id . "' LIMIT 1";
			return $this->returnResultSet($strSQL);
		}
		
		####################################################################################################
		## Get Table Grid Information
		## - Fields array for libdbtable
		## - SQL for libdbtable
		####################################################################################################
		function getTableGridInfo($args = array(), $forGrid = true) {
			
			global $slrsConfig, $intranet_session_language;
			
			$name_field = $this->getNameFieldByLang("iu.");
			
			if (count($args) > 0) extract($args);
			
			switch ($args["table"]) {
				case "INTRANET_SLRS_NOT_AVAILABLE_TEACHERS":
					$fieldArr = array('StartDate', 'EndDate', 'UserName', 'Reasons');
					$strSQL = "SELECT " . $name_field . " as UserName, NA_Teacher_ID, StartDate, EndDate, Reasons FROM INTRANET_SLRS_NOT_AVAILABLE_TEACHERS as isnat ";
					$strSQL .= " INNER JOIN " . $slrsConfig['INTRANET_USER'] . " AS iu ON (iu.UserID=isnat.UserID AND iu.RecordType='1' AND iu.RecordStatus='1') ";
					
					if (!empty($keyword)) {
						$strSQL .= " WHERE StartDate like '%" . $keyword . "%' or EndDate like '%" . $keyword . "%'";
						$strSQL .= " or Reasons like '%" . $keyword . "%' or EnglishName like '%" . $keyword . "%' or ChineseName like '%" . $keyword . "%'";
					}
					$order2 = ', EnglishName ASC';
					break;
				case "INTRANET_SLRS_NOT_AVAILABLE_LOCATION":
					if ($intranet_session_language == "en") {
						$name_field = "CONCAT(ilb.NameEng, ' > ', ill.NameEng, ' > ', il.NameEng) as Location";
					} else {
						$name_field = "CONCAT(ilb.NameChi, ' > ', ill.NameChi, ' > ', il.NameChi) as Location";
					}
					
					$fieldArr = array('StartDate', 'EndDate', 'Location', 'Reasons');
					$strSQL = "SELECT isnal.LocationID, " . $name_field . ", NA_Location_ID, StartDate, EndDate, Reasons FROM INTRANET_SLRS_NOT_AVAILABLE_LOCATION as isnal";
					
					$strSQL .= " JOIN
									INVENTORY_LOCATION AS il ON (il.LocationID=isnal.LocationID AND il.RecordStatus='1')
								JOIN
									INVENTORY_LOCATION_LEVEL AS ill ON (ill.LocationLevelID=il.LocationLevelID AND ill.RecordStatus='1')
								JOIN
									INVENTORY_LOCATION_BUILDING AS ilb ON (ilb.BuildingID=ill.BuildingID AND ilb.RecordStatus='1')";
					
					if (!empty($keyword)) {
						$strSQL .= " WHERE StartDate like '%" . $keyword . "%' or EndDate like '%" . $keyword . "%'";
						$strSQL .= " or Reasons like '%" . $keyword . "%'
									 or il.NameEng like '%" . $keyword . "%' or il.NameChi like '%" . $keyword . "%'
									 or ill.NameEng like '%" . $keyword . "%' or ill.NameChi like '%" . $keyword . "%'
									 or ilb.NameEng like '%" . $keyword . "%' or ilb.NameChi like '%" . $keyword . "%'
									";
					}
					$order2 = ', il.NameEng ASC, ill.NameEng ASC, ilb.NameEng ASC';
					break;
				case "custom_sql":
					$strSQL = $sql;
					$order2 .= $fieldorder2;
					$fieldArr = array();
					break;
			}
			$infoArr = array(
					"fields" => $fieldArr,
					"order2"=> $order2,
					"sql" => $this->sqlOpt($strSQL)
			);
			return $infoArr;
		}
		
		####################################################################################################
		## Get Table Grid Data with limit condition
		## - run with SQL
		####################################################################################################
		function getTableGridData($args = array(), $isFromAdmin = true, $forGrid = true) {
			global $Lang, $indexVar, $intranet_session_language, $slrsConfig, $sys_custom;
			
			if (count($args) > 0) extract($args);
			
			$dataArr = array();
			if (isset($sql) && !empty($sql)) {
				$result = $this->returnResultSet($sql);
				if (count($result) > 0) {
					/*********************************************************/
					$kk = 0;
					foreach ($result as $index => $vv) {
						if (isset($girdInfo["fields"]) && count($girdInfo["fields"]) > 0) {
							$thisIsUsed = false;
							#########################
							foreach ($girdInfo["fields"] as $fieldIndx => $fieldLabel) {
								if (!$forGrid) {
									$fieldIndx = $fieldLabel;
								}
								switch ($fieldLabel) {
									case "UserName":
										$userName = $vv["UserName"];
										if ($intranet_session_language != "en" && !empty($vv["ChineseName"])) {
											$userName = $vv["ChineseName"];
										}
										$dataArr[$kk][$fieldIndx] = $userName;
										break;
									case "Reasons":
										if (isset($vv[$fieldLabel])) $dataArr[$kk][$fieldIndx] = nl2br($vv[$fieldLabel]);
										else $dataArr[$kk][$fieldIndx] = "-";
										break;
									case "Location":
										if (isset($vv[$fieldLabel])) $dataArr[$kk][$fieldIndx] = nl2br($vv[$fieldLabel]);
										else $dataArr[$kk][$fieldIndx] = "-";
										break;
									case "AnotherSubjectGroup":
										$dataArr[$kk][$fieldIndx] = "-";
										if ($sys_custom['SLRS']["lessonExchangePromptAnotherLession"])
										{
											$res = $this->getAnotherSubjectGroupByExchangeRecord($vv);
											if (count($res) > 0)
											{
												$dataArr[$kk][$fieldIndx] = "";
												foreach ($res as $resIndex => $resVal)
												{
													$token = md5($resVal["SubjectGroupID"] . "_" . $resVal["TeacherA_TimeSlotID"] . "_" . $resVal["TeacherA_Date"] . "_SLRS_EXC");
													$anotherLink = "?task=management.lesson_exchange.edit&sgid=" . $resVal["SubjectGroupID"] . "&slotid=" . $resVal["TeacherA_TimeSlotID"] . "&date=" . $resVal["TeacherA_Date"] . "&exchangetoken=" . $token;
													$dataArr[$kk][$fieldIndx] .= "<a href='#' rel='" . $anotherLink . "' class='formsmallbutton AnotherRecBtn'>";
													$dataArr[$kk][$fieldIndx] .= ($intranet_session_language == "en") ? $resVal["ClassTitleEN"] : $resVal["ClassTitleB5"];
													$dataArr[$kk][$fieldIndx] .= "</a>";
												}
											}
										} else {
											if (isset($vv[$fieldLabel])) $dataArr[$kk][$fieldIndx] = $vv[$fieldLabel];
											else $dataArr[$kk][$fieldIndx] = "-";
										}
										break;
									default:
										if (isset($vv[$fieldLabel])) $dataArr[$kk][$fieldIndx] = $vv[$fieldLabel];
										else $dataArr[$kk][$fieldIndx] = "-";
										break;
								}
							}
						} else {
							$dataArr[$kk] = array_values($vv);
						}
						if ($isFromAdmin) {
							$dataArr[$kk][] = "<input type=checkbox name='" . $recIDArr . "[]' id='" . $recIDArr . "' value='" . $vv[$recID] . "'>";
						}
						
						$kk++;
					}
				}
			}
			return $dataArr;
		}
		
		
		function reformat_date($date_from_picker) {
			$dateArr = explode(" ", $date_from_picker);
			return  trim($dateArr[0]) . " " . trim($dateArr[1]) . ":00";
		}
		
		function getAllLocation() {
			$strSQL = "SELECT il.LocationID, ilb.Code as BuildingCode, ilb.NameChi as BuildingNameChi, ilb.NameEng as BuildingNameEng,
							ill.Code as LevelCode, ill.NameChi as LevelNameChi, ill.NameEng as LevelNameEng,
							il.Code as LocationCode, il.NameChi as LocationNameChi, il.NameEng as LocationNameEng, il.Description as LocationDesc
						FROM
							INVENTORY_LOCATION_BUILDING AS ilb
						JOIN
							INVENTORY_LOCATION_LEVEL AS ill ON (ill.BuildingID=ilb.BuildingID AND ill.RecordStatus='1')
						JOIN
							INVENTORY_LOCATION AS il ON (il.LocationLevelID=ill.LocationLevelID AND il.RecordStatus='1')
						WHERE
							ilb.RecordStatus='1'
						ORDER BY BuildingNameEng, LevelNameEng, LocationNameEng
					";
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0 && $this->isEJ()) {
				foreach ($result as $_key => $_valArr) {
					$result[$_key]["BuildingNameChi"] = convert2unicode($_valArr["BuildingNameChi"]);
					$result[$_key]["LevelNameChi"] = convert2unicode($_valArr["LevelNameChi"]);
					$result[$_key]["LocationNameChi"] = convert2unicode($_valArr["LocationNameChi"]);
					$result[$_key]["LocationDesc"] = convert2unicode($_valArr["LocationDesc"]);
				}
			}
			$result = BuildMultiKeyAssoc($result, 'LocationID');
			return $result;
		}
		
		function getAllLocationSelection($date = '', $timeSlotID = '', $keepLocationID = '', $withNotAvailableLocation = false) {
			global $intranet_session_language, $sys_custom, $Lang;
			
			$sql = "SELECT SettingName, SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName IN ('LocationNameDisplay', 'PresetLocation');";
			$a = $this->returnResultSet($sql);
			if (count($a) > 0) {
				foreach ($a as $index => $vv) {
					switch ($vv["SettingName"]) {
						case "LocationNameDisplay" :
							$mode = $vv["SettingValue"];
							break;
						case "PresetLocation":
							$PresetLocation = $vv["SettingValue"];
							break;
					}
				}
			} else {
				$mode = 0;
			}
			
			$allLocations = $this->getAllLocation();
			$selection = array();
			if (count($allLocations) > 0) {
				foreach ($allLocations as $kk => $vv) {
					//if (empty($vv["BuildingNameChi"])) $vv["BuildingNameChi"] = $vv["BuildingNameEng"];
					//if (empty($vv["LevelNameChi"])) $vv["LevelNameChi"] = $vv["LevelNameEng"];
					//if (empty($vv["LocationNameChi"])) $vv["LocationNameChi"] = $vv["LocationNameEng"];
                    $buildingName = Get_Lang_Selection($vv["BuildingNameChi"], $vv["BuildingNameEng"]);
                    $levelName = Get_Lang_Selection($vv["LevelNameChi"], $vv["LevelNameEng"]);
                    $locationName = Get_Lang_Selection($vv["LocationNameChi"], $vv["LocationNameEng"]);
					
					if($mode == 0){
 						//if ($intranet_session_language == "en") {
 						//	$selection[$kk] = $vv["BuildingNameEng"] . " > " . $vv["LevelNameEng"] . " > " . $vv["LocationNameEng"];
 						//} else {
 						//	$selection[$kk] = $vv["BuildingNameChi"] . " > " . $vv["LevelNameChi"] . " > " . $vv["LocationNameChi"];
 						//}
						//$selection[$kk] = Get_Lang_Selection($vv["BuildingNameChi"] . " > " . $vv["LevelNameChi"] . " > " . $vv["LocationNameChi"], $vv["BuildingNameEng"] . " > " . $vv["LevelNameEng"] . " > " . $vv["LocationNameEng"]);
                        $selection[$kk] = $buildingName . " > " . $levelName . " > " . $locationName;

						if ($vv["LocationDesc"] != '') {
							$selection[$kk] .= ' ('.$vv["LocationDesc"].')';
						}
					} else if ($mode == 1) {
						$selection[$kk] = $vv["LocationCode"];
					}
					if ($PresetLocation == $vv["LocationID"] && !$withNotAvailableLocation) {
						$selection[$kk] .= " (" . $Lang['SLRS']['PresetSubstitutionLocation'] . ")";
					}
				}
			}
			
			if ($sys_custom['SLRS']["defaultToPresetLocation"] && !$withNotAvailableLocation) {
				$strSQL = "SELECT StartTime, EndTime FROM INTRANET_TIMETABLE_TIMESLOT WHERE TimeSlotID='" . $timeSlotID . "'";
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0) {
					$startTime = $date . " " . $result[0]["StartTime"];
					$endTime = $date . " " . $result[0]["StartTime"];
					$ignored_locationArr = $this->getNotAvailableLocationRecordByDate($startTime, $endTime);
					if (count($ignored_locationArr) > 0) {
						foreach ($ignored_locationArr as $kk => $vv) {
							if (isset($selection[$vv]) && $vv != $keepLocationID) unset($selection[$vv]);
						}
					}
				}
			}
			return $selection;
		}
		
		function checkLesson($date, $userID, $session) {
			global $PATH_WRT_ROOT, $slrsConfig, $Lang;;
			if($this->isEJ())
			{
				include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
			}
			else
			{
				include_once($PATH_WRT_ROOT."includes/libtimetable.php");
			}
			
			$ltimetable = new Timetable();
			$curTimetableId = $ltimetable->Get_Current_Timetable($date);
			
			$session = ($session!= "am" && $session!= "pm")?"%":$session;
			
			//$sql = "SELECT RecordDate,CycleDay FROM INTRANET_CYCLE_DAYS WHERE RecordDate='".$date."'";
			$dateArr[0]=$date;
			$sql = $this->getDaySQL($dateArr,"");
			$a = $this->returnResultSet($sql);
			$day = $a[0]["CycleDay"];
			
			$sql = "SELECT *
				FROM (
					SELECT
						TimeSlotID, TimeSlotName, StartTime, EndTime, DisplayOrder
					FROM
						INTRANET_TIMETABLE_TIMESLOT
					WHERE
						SessionType like '".$session."'
				) as itt
				LEFT JOIN (
					SELECT
						RoomAllocationID, Day, itra.TimeSlotID, itra.LocationID, itra.SubjectGroupID, OthersLocation, RecordDate, CycleDay, ClassCode, ClassTitleEN, ClassTitleB5,
						NameChi, NameEng, DisplayOrder, Code, BarCode, SubjectClassTeacherID, stct.UserID, EnglishName, ChineseName,
						SubjectTermID, SubjectID, YearTermID, isl.LeaveID, islUserID, DateLeave, LessonArrangementID, ArrangedTo_UserID, ArrangedTo_LocationID
					FROM (
						SELECT
							RoomAllocationID,Day, TimeSlotID, LocationID, SubjectGroupID, OthersLocation
						FROM
							INTRANET_TIMETABLE_ROOM_ALLOCATION
						WHERE
							Day=".$day." and TimetableID='".$curTimetableId."') as itra
						LEFT JOIN (".$this->getDaySQL($dateArr,"").") as icd ON itra.Day=icd.CycleDay
						LEFT JOIN (SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS) as stc ON itra.SubjectGroupID=stc.SubjectGroupID
						LEFT JOIN (SELECT LocationID,NameChi,NameEng,DisplayOrder,Code,BarCode FROM INVENTORY_LOCATION) as il ON itra.LocationID = il.LocationID
						LEFT JOIN (SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER WHERE UserID=".IntegerSafe($userID).") as stct ON itra.SubjectGroupID=stct.SubjectGroupID
						LEFT JOIN (SELECT UserID,EnglishName,ChineseName FROM " . $slrsConfig['INTRANET_USER'] . ") as iu ON stct.UserID=iu.UserID
						LEFT JOIN (SELECT SubjectTermID,SubjectGroupID,SubjectID,YearTermID FROM SUBJECT_TERM) as st ON itra.SubjectGroupID = st.SubjectGroupID
						LEFT JOIN (SELECT LeaveID,UserID as islUserID,DateLeave FROM INTRANET_SLRS_LEAVE WHERE DateLeave='".$date."') as isl ON stct.UserID=isl.islUserID
						LEFT JOIN (SELECT LessonArrangementID,UserID,LeaveID,TimeSlotID,LocationID,ArrangedTo_UserID,ArrangedTo_LocationID
								FROM INTRANET_SLRS_LESSON_ARRANGEMENT) as isla ON isl.islUserID=isla.UserID AND isl.LeaveID=isla.LeaveID
								AND itra.TimeSlotID=isla.TimeSlotID
					WHERE stct.UserID IS NOT NULL
				) as a ON itt.TimeSlotID=a.TimeSlotID
				WHERE SubjectGroupID IS NOT NULL;
			";
			$a = $this->returnResultSet($sql);

			if (count($a) == 0)
			{
				$sql = "SELECT
							TimeSlotID, TimeSlotName, StartTime, EndTime
						FROM
							INTRANET_SLRS_LESSON_EXCHANGE AS isle
						INNER JOIN
							INTRANET_TIMETABLE_TIMESLOT AS itt ON (itt.TimeSlotID=isle.TeacherB_TimeSlotID)
						WHERE
							TeacherA_UserID='" . $userID . "' AND TeacherB_Date='" . $date . "' AND SessionType like '".$session."'";
				$a = $this->returnResultSet($sql);
			}
			$check_with_subtc = true;
			if ($check_with_subtc)
			{
				$sql = "SELECT
								isla.TimeSlotID, TimeSlotName, StartTime, EndTime
							FROM 
								INTRANET_SLRS_LESSON_ARRANGEMENT AS isla
							INNER JOIN 
								INTRANET_TIMETABLE_TIMESLOT AS itt ON (itt.TimeSlotID=isla.TimeSlotID)
							WHERE
								ArrangedTo_UserID='" . $userID . "' AND Date(TimeStart)='" . $date . "' AND SessionType like '".$session."'
							";
				$checkSA = $this->returnResultSet($sql);
				if (count($checkSA) > 0) return -199;
			}
			return $a;
		}
		
		
		function getLessonTeacherByDate($date, $fromExchange = false, $ignoreExchangeGroupID=array()) {
			
			global $PATH_WRT_ROOT, $slrsConfig, $Lang;
			if($this->isEJ())
			{
				include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
			}
			else
			{
				include_once($PATH_WRT_ROOT."includes/libtimetable.php");
			}
			
			if (empty($date)) $date = date("Y-m-d");
			$ltimetable = new Timetable();
			$curTimetableId = $ltimetable->Get_Current_Timetable($date);
			
			//$sql = "SELECT RecordDate,CycleDay FROM INTRANET_CYCLE_DAYS WHERE RecordDate='".$date."'";
			$dateArr[0]=$date;
			$sql = $this->getDaySQL($dateArr,"");
			$a = $this->returnResultSet($sql);
			$day = $a[0]["CycleDay"];
			
			$sql = "SELECT
						UserID, EnglishName, ChineseName
					FROM (
						SELECT
							TimeSlotID, TimeSlotName, StartTime, EndTime, DisplayOrder
						FROM
							INTRANET_TIMETABLE_TIMESLOT
					) as itt
					LEFT JOIN(
						SELECT
							RoomAllocationID, Day, itra.TimeSlotID, itra.LocationID, itra.SubjectGroupID, OthersLocation, RecordDate, CycleDay, ClassCode, ClassTitleEN, ClassTitleB5,
							NameChi, NameEng, DisplayOrder, Code, BarCode, SubjectClassTeacherID, stct.UserID, EnglishName, ChineseName,
							SubjectTermID, SubjectID, YearTermID, isl.LeaveID, islUserID, DateLeave, LessonArrangementID, ArrangedTo_UserID, ArrangedTo_LocationID
						FROM(
							SELECT
								RoomAllocationID, Day, TimeSlotID, LocationID, SubjectGroupID, OthersLocation
							FROM
								INTRANET_TIMETABLE_ROOM_ALLOCATION
							WHERE
								Day=".$day." and TimetableID='".$curTimetableId."'
						) as itra
						LEFT JOIN (
							".$this->getDaySQL($dateArr,"")."
						) as icd ON itra.Day=icd.CycleDay
						LEFT JOIN (
							SELECT
								SubjectGroupID, ClassCode, ClassTitleEN, ClassTitleB5
							FROM
								SUBJECT_TERM_CLASS
						) as stc ON itra.SubjectGroupID=stc.SubjectGroupID
						LEFT JOIN (
							SELECT
								LocationID, NameChi, NameEng, DisplayOrder, Code, BarCode
							FROM
								INVENTORY_LOCATION
						) as il ON itra.LocationID = il.LocationID
						LEFT JOIN (
							SELECT
								SubjectClassTeacherID, SubjectGroupID, UserID
							FROM
								SUBJECT_TERM_CLASS_TEACHER
						) as stct ON itra.SubjectGroupID=stct.SubjectGroupID
						LEFT JOIN (
							SELECT
								UserID, EnglishName, ChineseName
							FROM " . $slrsConfig['INTRANET_USER'] . "
						) as iu ON stct.UserID=iu.UserID
						LEFT JOIN (
							SELECT
								SubjectTermID, SubjectGroupID, SubjectID, YearTermID
							FROM
								SUBJECT_TERM
						) as st ON itra.SubjectGroupID = st.SubjectGroupID
						LEFT JOIN (
							SELECT
								LeaveID, UserID as islUserID, DateLeave
							FROM
								INTRANET_SLRS_LEAVE
							WHERE
								DateLeave='".$date."'
						) as isl ON stct.UserID=isl.islUserID
						LEFT JOIN (
							SELECT
								LessonArrangementID, UserID, LeaveID, TimeSlotID, LocationID, ArrangedTo_UserID, ArrangedTo_LocationID, SubjectGroupID
							FROM
								INTRANET_SLRS_LESSON_ARRANGEMENT
						) as isla ON isl.islUserID=isla.UserID AND isl.LeaveID=isla.LeaveID AND itra.TimeSlotID=isla.TimeSlotID AND itra.SubjectGroupID=isla.SubjectGroupID
						WHERE
							stct.UserID IS NOT NULL
					) as a ON itt.TimeSlotID=a.TimeSlotID
					WHERE
						SubjectGroupID IS NOT NULL
					GROUP BY UserID
					ORDER BY EnglishName;
			";
			$a = $this->returnResultSet($sql);
//  			echo $date;
//  			echo "<pre>";
//  			print_r($a);
//  			echo "</pre>";
			
			if (count($a) > 0) {
				foreach ($a as $kk => $vv) {
					if ($this->isEJ())
					{
						// $a[$kk]["ChineseName"] = convert2unicode($vv["ChineseName"]);
					}
					// $sql = "SELECT LessonArrangementID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE DATE(TimeStart) = '" . $date . "' AND (UserID='" . $vv["UserID"] . "' OR ArrangedTo_UserID='" . $vv["UserID"] . "')";
					$sql = "SELECT LessonArrangementID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE DATE(TimeStart) = '" . $date . "' AND UserID='" . $vv["UserID"] . "'";
					$tmp = $this->returnResultSet($sql);
					if (count($tmp) > 0)
					{
						if ($fromExchange)
						{
							$a[$kk]["ChineseName"] .= " (**" . $Lang['SLRS']['SubstitutionArrangementHasRecord'] . ")";
							$a[$kk]["EnglishName"] .= " (**" . $Lang['SLRS']['SubstitutionArrangementHasRecord'] . ")";
						}
						else
						{
							unset($a[$kk]);
						}
					}
					else if ($fromExchange)
					{
						$sql = "SELECT LessonExchangeID FROM INTRANET_SLRS_LESSON_EXCHANGE
								WHERE ( TeacherA_Date='" . $date . "' OR TeacherB_Date='" . $date . "' )
								 AND TeacherA_UserID='" . $vv["UserID"] . "'";
						
						if (count($ignoreExchangeGroupID) > 0)
						{
							$sql .= " AND GroupID NOT IN (". implode(", ", $ignoreExchangeGroupID) . ")";
						}
						$tmp = $this->returnResultSet($sql);
						if (count($tmp) > 0)
						{
							if ($fromExchange)
							{
								$a[$kk]["ChineseName"] .= " (**" . $Lang['SLRS']['SubstitutionArrangementHasExchangeRecord']. ")";
								$a[$kk]["EnglishName"] .= " (**" . $Lang['SLRS']['SubstitutionArrangementHasExchangeRecord']. ")";
							}
						}
					}
				}
			}
// 			echo "<pre>";
// 			print_r($a);
// 			echo "</pre>";
// 			exit;
			return $a;
		}
		
		
		function getNotAvailableLocationByID($rec_id) {
			$strSQL = "SELECT NA_Location_ID, LocationID, StartDate, EndDate, Reasons FROM INTRANET_SLRS_NOT_AVAILABLE_LOCATION WHERE NA_Location_ID='" . $rec_id . "' LIMIT 1";
			return $this->returnResultSet($strSQL);
		}
		
		function getNotAvailableTeacherRecordByDate($startDate, $endDate, $filterByUserId = 'all') {
			global $slrsConfig, $sys_custom;
			$output = array();
			if ($sys_custom['SLRS']["NoAvailTeachers"]) {
				
				if (strlen($endDate) == 10) {
					$endDate = date('Y-m-d', strtotime($endDate . ' +1 day'));
				}
				
				$strSQL = "SELECT UserID, StartDate, EndDate, Reasons ";
				$strSQL .= " FROM INTRANET_SLRS_NOT_AVAILABLE_TEACHERS as isnat";
				$strSQL .= " WHERE (";
				$strSQL .= "'" . $startDate . "' Between isnat.StartDate AND isnat.EndDate or '" . $endDate . "' Between isnat.StartDate AND isnat.EndDate";
				$strSQL .= " or isnat.StartDate Between '" . $startDate . "' AND '" . $endDate . "' or isnat.EndDate Between '" . $startDate . "' AND '" . $endDate . "'";
				$strSQL .= ")";
				
				if ($filterByUserId != "all") {
					$strSQL .= " AND UserID in (" . $filterByUserId. ")";
				}
				$strSQL .= " ORDER BY isnat.StartDate ASC";
				
				$result = $this->returnResultSet($strSQL);
				$result = BuildMultiKeyAssoc($result, 'UserID');
				if (count($result) > 0) {
					$strSQL = "SELECT UserID, EnglishName, ChineseName FROM " . $slrsConfig['INTRANET_USER'] . " WHERE UserID IN ('" . implode("', '", array_keys($result)) . "')";
					$user_result = $this->returnResultSet($strSQL);
					$user_result = BuildMultiKeyAssoc($user_result, 'UserID');
					foreach ($result as $kk => $vv) {
						$output[] = array(
								"LeaveID" => '',
								"UserID" => $kk,
								"DateLeave" => $vv["StartDate"] . " - " . $vv["EndDate"],
								"ReasonCode" => $vv["Reasons"],
								"SubjectGroupID" => '',
								"SubjectID" => '',
								"LocationID" => '',
								"ArrangedTo_UserID" => 'NOT_AVAILABLE',
								"ArrangedTo_LocationID" => '',
								"TimeSlotID" => '',
								"ClassCode" => '',
								"ClassTitleEN" => '',
								"ClassTitleB5" => '',
								"CycleDay" => '',
								"TimeSlotName" => '',
								"UserName" => $user_result[$kk]["EnglishName"],
								"AssignedUserName" => '',
								"isVolunteer" => '',
								"LessonArrangementID" => ''
						);
					}
				}
			}
			return $output;
		}
		
		function getNotAvailableLocationRecordByDate($startDate, $endDate, $filterByLocationId = 'all') {
			global $slrsConfig, $sys_custom;
			$output = array();
			if ($sys_custom['SLRS']["defaultToPresetLocation"]) {
				
				if (strlen($endDate) == 10) {
					$endDate = date('Y-m-d', strtotime($endDate . ' +1 day'));
				}
				
				$strSQL = "SELECT LocationID, StartDate, EndDate, Reasons ";
				$strSQL .= " FROM INTRANET_SLRS_NOT_AVAILABLE_LOCATION as isnal";
				$strSQL .= " WHERE (";
				$strSQL .= "'" . $startDate . "' Between isnal.StartDate AND isnal.EndDate or '" . $endDate . "' Between isnal.StartDate AND isnal.EndDate";
				$strSQL .= " or isnal.StartDate Between '" . $startDate . "' AND '" . $endDate . "' or isnal.EndDate Between '" . $startDate . "' AND '" . $endDate . "'";
				$strSQL .= ")";
				
				if ($filterByLocationId!= "all") {
					$strSQL .= " AND LocationID in (" . $filterByLocationId. ")";
				}
				$strSQL .= " ORDER BY isnal.StartDate ASC";
				
				$result = $this->returnResultSet($strSQL);
				$result = BuildMultiKeyAssoc($result, 'LocationID');
				$output = array_keys($result);
			}
			return $output;
		}
		
		/**
		 * get Preset Location Information
		 * @param int $id
		 * @param date $date
		 * @param string $isTemp
		 * @return boolean|number|string
		 */
		function getPresetLocationInfo($id, $date, $isTemp = false)
		{
			global $sys_custom;
			$PresetLocationInfo["LimitLocation"] = false;
			$PresetLocationInfo["ThisCount"] = 0;
			$PresetLocationInfo["NumOfPresentLocation"] = 0;
			if ($isTemp)
			{
				$PresetLocationInfo["type"] = ($id) ? 'edit' : 'add';
			}
			else
			{
				$strSQL = "SELECT LessonArrangementID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LeaveID='" . $id . "'";
				$res = $this->returnResultSet($strSQL);
				$PresetLocationInfo["type"] = (count($res) > 0) ? 'edit' : 'add';
			}
			
			if ($sys_custom['SLRS']["defaultToPresetLocation"])
			{
				$sql = "SELECT SettingName, SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName IN ('PresetLocation', 'PSSubstitutionLimitPerDay'); ";
				$res = $this->returnResultSet($sql);
				if (count($res) > 0)
				{
					foreach ($res as $ind => $val)
					{
						if ($val["SettingName"] == "PresetLocation")
						{
							$PresetLocationInfo["LocationID"] = $val["SettingValue"];
						}
						else if ($val["SettingName"] == "PSSubstitutionLimitPerDay")
						{
							$PresetLocationInfo["LimitPerDay"] = $val["SettingValue"];
						}
					}
					if ($PresetLocationInfo["LocationID"] > 0 && $PresetLocationInfo["LimitPerDay"] > 0)
					{
						$PresetLocationInfo["LimitLocation"]= true;
						if (!empty($date))
						{
							$sql = "SELECT COUNT(ArrangedTo_LocationID) as NumOfPresentLocation FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE TimeStart like '" . $date . "%' AND ArrangedTo_LocationID='" . $PresetLocationInfo["LocationID"] . "'";
							$res = $this->returnResultSet($sql);
							$PresetLocationInfo["NumOfPresentLocation"] = $res[0]["NumOfPresentLocation"];
							$PresetLocationInfo["ThisCount"] = $PresetLocationInfo["LimitPerDay"] - $PresetLocationInfo["NumOfPresentLocation"];
						}
					}
				}
			}
			return $PresetLocationInfo;
		}
		
		/**
		 * get Year Class IDs By SubjectGroupID
		 * @param Integer $subjectGroupID
		 * @param string $date
		 * @param string $assc
		 * @return Array > YearClassID
		 */
		function getYearClassIDBySubjectGroupID($subjectGroupID, $date = "", $assc = true)
		{
			if (empty($date))
			{
				$date = date("Y-m-d");
			}
			if (!isset($this->AcademicYear[$date]) || empty($this->AcademicYear[$date]))
			{
				$this->AcademicYear[$date] = getAcademicYearAndYearTermByDate($date);
			}
			$yearInfo = $this->AcademicYear[$date];
			$AcademicYearID = $yearInfo["AcademicYearID"];
			
			$strSQL = "SELECT
							distinct ycu.YearClassID
						FROM
							YEAR_CLASS_USER AS ycu
						INNER JOIN
							SUBJECT_TERM_CLASS_USER AS stcu ON (stcu.UserID=ycu.UserID)
						INNER JOIN
							SUBJECT_TERM AS st ON (st.SubjectGroupID=stcu.SubjectGroupID AND YearTermID='" . $yearInfo["YearTermID"] . "')
						INNER JOIN
							YEAR_CLASS AS yc ON (yc.YearClassID=ycu.YearClassID and yc.AcademicYearID='" . $AcademicYearID . "')
						WHERE
							stcu.SubjectGroupID='" . $subjectGroupID . "'";
			if ($assc) {
				$res = $this->returnResultSet($strSQL);
			} else {
				$res = $this->returnVector($strSQL);
			}
			return $res;
		}
		
		/**
		 * get SubjectGroup ID by Year Class IDs
		 * @param Array $thisYearClassIDs
		 * @param Date $date
		 * @param string $assc
		 * @return SubjectGroupIDs|NULL
		 */
		function getSubjectGroupIDByYearClassIDs($thisYearClassIDs, $date, $assc = true)
		{
			if (empty($date))
			{
				$date = date("Y-m-d");
			}
			if (!isset($this->AcademicYear[$date]) || empty($this->AcademicYear[$date]))
			{
				$this->AcademicYear[$date] = getAcademicYearAndYearTermByDate($date);
			}
			$yearInfo = $this->AcademicYear[$date];
			$AcademicYearID = $yearInfo["AcademicYearID"];
			
			if (count($thisYearClassIDs) > 0) {
				$strSQL = "SELECT
								distinct stcu.SubjectGroupID
							FROM
								YEAR_CLASS_USER AS ycu
							INNER JOIN
								SUBJECT_TERM_CLASS_USER AS stcu ON (stcu.UserID=ycu.UserID)
							INNER JOIN
								SUBJECT_TERM AS st ON (st.SubjectGroupID=stcu.SubjectGroupID AND YearTermID='" . $yearInfo["YearTermID"] . "')
							INNER JOIN
								YEAR_CLASS AS yc ON (yc.YearClassID=ycu.YearClassID and yc.AcademicYearID='" . $AcademicYearID . "')
							WHERE
								ycu.YearClassID IN (" . implode(",", $thisYearClassIDs) . ")";
				if ($assc) {
					$res = $this->returnResultSet($strSQL);
				} else {
					$res = $this->returnVector($strSQL);
				}
				return $res;
			}
			return null;
		}
		
		/**
		 * get Another Subject Group by Exchange Record
		 * @param Array $exchangeRecord
		 * @return AnotherSubjectGroup|NULL
		 */
		function getAnotherSubjectGroupByExchangeRecord($exchangeRecords, $getTeacherInfo = false)
		{
			global $slrsConfig;
			
			include_once(dirname(dirname(__FILE__)) . "/libcycleperiods.php");
			$libcycleperiods = new libcycleperiods();
			if (!empty($exchangeRecords["LessonExchangeID"]))
			{
				$exchangeIDs = explode(",", $exchangeRecords["LessonExchangeID"]);
				// $exchangeADate = explode(",", $exchangeRecords["TeacherA_Date"]);
				$anotherGroup = array();
				
				foreach ($exchangeIDs as $kk => $vv)
				{
					
					$strSQL = "SELECT 
									LessonExchangeID, TeacherA_UserID, TeacherA_Date, TeacherA_TimeSlotID, TeacherA_SubjectGroupID, TeacherA_YearClassID, TeacherA_SubjectID, GroupID
								FROM INTRANET_SLRS_LESSON_EXCHANGE
								WHERE LessonExchangeID = '" . $vv . "' 
									and (TeacherA_Date != TeacherB_Date or TeacherA_SubjectGroupID !=TeacherB_SubjectGroupID or TeacherA_UserID != TeacherB_UserID or TeacherA_TimeSlotID != TeacherB_TimeSlotID)";
					$res = $this->returnResultSet($strSQL);
					$exchangeRecord = $res[0];
					
					$date = $exchangeRecord["TeacherA_Date"];
					if (empty($date))
					{
						$date = date("Y-m-d");
					}
					if (!isset($this->AcademicYear[$date]) || empty($this->AcademicYear[$date]))
					{
						$this->AcademicYear[$date] = getAcademicYearAndYearTermByDate($date);
					}
					$yearInfo = $this->AcademicYear[$date];
					$AcademicYearID = $yearInfo["AcademicYearID"];
					
					// $CycleDay = $libcycleperiods->getCycleInfoByDate($exchangeRecord["TeacherA_Date"]);
					
					$dateArr[0] = $exchangeRecord["TeacherA_Date"];
					$sql = $this->getDaySQL($dateArr,"");
					$a1 = $this->returnResultSet($sql);
					$CycleDay = $a1[0];
					
					$thisYearClassIDs = $this->getYearClassIDBySubjectGroupID($exchangeRecord["TeacherA_SubjectGroupID"], $exchangeRecord["TeacherA_Date"], $assc = false);
					$classSubjectGroupIDs = $this->getSubjectGroupIDByYearClassIDs($thisYearClassIDs, $exchangeRecord["TeacherA_Date"], $assc = false);
					
					if (count($classSubjectGroupIDs) > 0) {
						$strSQL = "SELECT
								itra.SubjectGroupID, stct.UserID, ClassTitleEN, ClassTitleB5, EnglishName, ChineseName, LocationID, OthersLocation
							FROM
								INTRANET_TIMETABLE_ROOM_ALLOCATION AS itra
							INNER JOIN
								SUBJECT_TERM_CLASS_TEACHER AS stct ON (stct.SubjectGroupID=itra.SubjectGroupID)
							INNER JOIN
								SUBJECT_TERM_CLASS_YEAR_RELATION AS stcyr ON (stcyr.SubjectGroupID=itra.SubjectGroupID)
							INNER JOIN
								SUBJECT_TERM_CLASS AS stc ON (stc.SubjectGroupID = itra.SubjectGroupID)
							INNER JOIN
								SUBJECT_TERM AS st ON (st.YearTermID='" . $yearInfo["YearTermID"] . "' AND st.SubjectGroupID=itra.SubjectGroupID)
							INNER JOIN
								ACADEMIC_YEAR_TERM AS ayt ON (ayt.YearTermID = st.YearTermID AND AcademicYearID='" . $AcademicYearID . "')
							INNER JOIN
								" . $slrsConfig['INTRANET_USER'] . " AS iu ON (iu.UserID=stct.UserID AND iu.RecordType='1' AND iu.RecordStatus='1')
							WHERE
								Day='" . $CycleDay["CycleDay"] . "' AND TimeSlotID LIKE '" . $exchangeRecord["TeacherA_TimeSlotID"] . "'
								AND itra.SubjectGroupID NOT LIKE '" . $exchangeRecord["TeacherA_SubjectGroupID"] . "'
								AND itra.SubjectGroupID IN (" . implode(",", $classSubjectGroupIDs) . ")
								AND itra.SubjectGroupID NOT IN (
									SELECT
										TeacherA_SubjectGroupID
									FROM
										INTRANET_SLRS_LESSON_EXCHANGE
									WHERE
										TeacherA_TimeSlotID='" . $exchangeRecord["TeacherA_TimeSlotID"] . "' AND TeacherA_Date='" . $exchangeRecord['TeacherA_Date']. "'
								)
							GROUP BY itra.SubjectGroupID, stct.UserID";
						
						$another_res = $this->returnResultSet($strSQL);
						if (count($another_res) > 0)
						{
							foreach ($another_res as $skk => $svv)
							{
								$anotherGroup[$svv["SubjectGroupID"]] = array_merge($exchangeRecord, $svv);
								// $anotherGroup[$svv["SubjectGroupID"]]["master_record"] = $res[0];
								if ($getTeacherInfo)
								{
									$locationInfo = $this->getLocationInfoByIDs(array( $svv["LocationID"] ));
									$anotherGroup[$svv["SubjectGroupID"]]["location"] = $locationInfo[$svv["LocationID"]];
								}
							}
						}
					}
				}
				return $anotherGroup;
				// 
			}
			return null;
		}
		
		/**
		 * get Lesson Teacher by Another Information (TimeSlotID and SubjectGroupID)
		 * @param Array $anotherInfo
		 * @return datasource
		 */
		function getLessonTeacherByAnotherInfo($anotherInfo)
		{
			global $slrsConfig, $Lang;
			
			include_once(dirname(dirname(__FILE__)) . "/libcycleperiods.php");
			$libcycleperiods = new libcycleperiods();
			
			$date = $anotherInfo["ExchangeDate"];
			if (empty($date))
			{
				$date = date("Y-m-d");
			}
			if (!isset($this->AcademicYear[$date]) || empty($this->AcademicYear[$date]))
			{
				$this->AcademicYear[$date] = getAcademicYearAndYearTermByDate($date);
			}
			$yearInfo = $this->AcademicYear[$date];
			$AcademicYearID = $yearInfo["AcademicYearID"];
			
			// $CycleDay = $libcycleperiods->getCycleInfoByDate($date);
			$dateArr[0] = $date;
			$sql = $this->getDaySQL($dateArr,"");
			$a1 = $this->returnResultSet($sql);
			$CycleDay = $a1[0];
			
			$strSQL = "SELECT
							stct.UserID, EnglishName, ChineseName
						FROM
							INTRANET_TIMETABLE_ROOM_ALLOCATION AS itra
						INNER JOIN
							SUBJECT_TERM_CLASS_TEACHER AS stct ON (stct.SubjectGroupID=itra.SubjectGroupID)
						INNER JOIN
							SUBJECT_TERM_CLASS_YEAR_RELATION AS stcyr ON (stcyr.SubjectGroupID=itra.SubjectGroupID)
						INNER JOIN
							SUBJECT_TERM_CLASS AS stc ON (stc.SubjectGroupID = itra.SubjectGroupID)
						INNER JOIN
							SUBJECT_TERM AS st ON (st.YearTermID='" . $yearInfo["YearTermID"] . "' AND st.SubjectGroupID=itra.SubjectGroupID)
						INNER JOIN
							ACADEMIC_YEAR_TERM AS ayt ON (ayt.YearTermID = st.YearTermID AND AcademicYearID='" . $AcademicYearID . "')
						INNER JOIN
							" . $slrsConfig['INTRANET_USER'] . " AS iu ON (iu.UserID=stct.UserID AND iu.RecordType='1' AND iu.RecordStatus='1')
						WHERE
							Day='" . $CycleDay["CycleDay"] . "' AND TimeSlotID LIKE '" . $anotherInfo["TimeSlotID"] . "'
							AND itra.SubjectGroupID IN (" . $anotherInfo["SubjectGroupID"] . ")
						GROUP BY stct.UserID";
			$res = $this->returnResultSet($strSQL);
			if (count($res) > 0)
			{
				foreach ($res as $kk => $vv)
				{
					$strSQL = "SELECT
									LessonArrangementID
								FROM
									INTRANET_SLRS_LESSON_ARRANGEMENT
								WHERE
									TimeSlotID='" . $anotherInfo["TimeSlotID"] . "' AND DATE(TimeStart)='" . $date . "' AND UserID='" . $vv["UserID"] . "'";
					$tmp = $this->returnResultSet($strSQL);
					if (count($tmp) > 0)
					{
						$res[$kk]["EnglishName"] .= " (" . $Lang['SLRS']['SubstitutionArrangementHasRecord'] . ")";
						$res[$kk]["ChineseName"] .= " (" . $Lang['SLRS']['SubstitutionArrangementHasRecord'] . ")";
					}
				}
			}
			return $res;
		}
		
		
		/**
		 * get Year Statistics Teacher Substitution Report
		 * @param int $academicYearID
		 * @param string $semester
		 * @param array $semsterArray
		 * @return array|number
		 */
		public function getYearStatSubstitutionReportData($academicYearID, $semester, $semsterArray, $filterNoAffectLeaveReason=false)
		{
			global $slrsConfig, $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
			$lgeneralsettings = new libgeneralsettings();
			
			$general_settings_SLRS = $lgeneralsettings->Get_General_Setting("SLRS");
			$sql = "SELECT TermStart,TermEnd
				FROM(
					SELECT AcademicYearID FROM ACADEMIC_YEAR WHERE AcademicYearID=".IntegerSafe($academicYearID)."
				)as ay
				INNER JOIN(
					SELECT YearTermID,AcademicYearID,TermStart,TermEnd FROM ACADEMIC_YEAR_TERM WHERE YearTermID IN (".$semester.")
				) as ayt ON ay.AcademicYearID=ayt.AcademicYearID
				ORDER BY TermStart";
			$info = $this->returnResultSet($sql);
			$startDate = $info[0]["TermStart"];
			$endDate = $info[sizeof($info)-1]["TermEnd"];
			
			$name_field = $this->getNameFieldByLang($slrsConfig['INTRANET_USER'].".");
			
			// 	$sql="
			// 		SELECT UserID,UserName,CntLeave,CntSubstitution,CntPreYear,(CntSubstitution-CntLeave+CntPreYear) as Balance
			// 			FROM(
			// 			SELECT ist.UserID,UserName,COALESCE(CntLeave,'--') as CntLeave,COALESCE(CntSubstitution,'--') as CntSubstitution,COALESCE(BalanceAdjustTo,'--') as CntPreYear
			// 			FROM(
			// 				SELECT UserID FROM INTRANET_SLRS_TEACHER
			// 			) as ist
			// 			LEFT JOIN(
			// 				SELECT UserID FROM INTRANET_SLRS_LEAVE WHERE DateLeave BETWEEN '".$startDate."' AND '".$endDate."'
					// 				GROUP BY UserID
					// 			) as isl ON ist.UserID=isl.UserID
					// 			LEFT JOIN(
					// 				SELECT a0.UserID,SubstituteUser as CntSubstitution,CntLeave,BalanceAdjustTo
					// 				FROM(
					// 					SELECT UserID FROM INTRANET_SLRS_LEAVE WHERE DateLeave BETWEEN '".$startDate."' AND '".$endDate."'
					// 					GROUP BY UserID
					// 				) as a0
					// 				INNER JOIN(
					// 					SELECT ArrangedTo_UserID,COUNT(ArrangedTo_UserID) as SubstituteUser
					// 					FROM INTRANET_SLRS_LESSON_ARRANGEMENT GROUP BY ArrangedTo_UserID
					// 				) as a1 ON a0.UserID=a1.ArrangedTo_UserID
					// 				INNER JOIN(
					// 					SELECT UserID,COUNT(TimeSlotID) as CntLeave
					// 					FROM INTRANET_SLRS_LESSON_ARRANGEMENT GROUP BY UserID
					// 				) as a2 ON a0.UserID=a2.UserID
					// 				LEFT JOIN(
					// 					SELECT MIN(DateTimeInput) as DateTimeInput,UserID,BalanceAdjustID,BalanceAdjustTo
					// 					FROM(
					// 						SELECT YearTermID,AcademicYearID,TermStart,TermEnd FROM ACADEMIC_YEAR_TERM WHERE YearTermID IN (".$semsterArray[0].")
					// 					) as ayt
					// 					INNER JOIN(
			// 						SELECT UserID,BalanceAdjustTo,DateTimeInput,BalanceAdjustID FROM INTRANET_SLRS_BALANCE_ADJUST
			// 					) as isba ON isba.DateTimeInput BETWEEN TermStart AND TermEnd
			// 					GROUP BY UserID
			// 				) as a3 ON a0.UserID=a3.UserID
			// 			) as a ON ist.UserID=a.UserID
			// 			LEFT JOIN(
			// 				SELECT UserID,".$name_field." as UserName FROM ".$slrsConfig['INTRANET_USER']."
			// 			) as iu ON ist.UserID =iu.UserID
					// 		) as a0
					// 	";
					// echo $sql;
			// exit;
			// $info=$connection->returnResultSet($sql);
			/********************************************************/
			$balanceDisplay = $this->getBalanceDisplay();
			
			if (count($info) > 0)
			{
			    /*
				$strSQL = "SELECT
								".$slrsConfig['INTRANET_USER'].".UserID,". $name_field . " as UserName
							FROM
								".$slrsConfig['INTRANET_USER']." as ".$slrsConfig['INTRANET_USER']."
							JOIN
								INTRANET_SLRS_TEACHER AS ist ON (".$slrsConfig['INTRANET_USER'].".UserID=ist.UserID)
							WHERE
								RecordType='1' AND Teaching<>'S' AND RecordStatus='1'";
				*/
			    $strSQL = "SELECT
								".$slrsConfig['INTRANET_USER'].".UserID,". $name_field . " as UserName
							FROM
								".$slrsConfig['INTRANET_USER']." as ".$slrsConfig['INTRANET_USER']."
							LEFT JOIN
								INTRANET_SLRS_TEACHER AS ist ON (".$slrsConfig['INTRANET_USER'].".UserID=ist.UserID)
							WHERE
								RecordType='1' AND Teaching<>'S'";
				// Debug only
				// $strSQL .= " AND ".$slrsConfig['INTRANET_USER'].".UserID in (26818, 26819)";
				
				$strSQL .= " ORDER BY EnglishName ASC, ChineseName ASC";
				$data = $this->returnResultSet($strSQL);
				$info = BuildMultiKeyAssoc($data, 'UserID');
				
				$settingsArr = array();
				if ($filterNoAffectLeaveReason)
				{
					$settingsArr = $this->getOfficialLeavelSetting($onlyDisableAdjust= true);
				}
				$strSQL = "SELECT
								LeaveID
							FROM
								INTRANET_SLRS_LEAVE
							WHERE
								DateLeave BETWEEN '".$startDate."' AND '".$endDate."'";
				$leaveData = $this->returnVector($strSQL);
				if (count($settingsArr) > 0)
				{
					$strSQL .= " AND ReasonCode NOT IN ('" . implode("', '", array_keys($settingsArr)) . "')";
				}
				$leaveDataWithoutNoAffected = $this->returnVector($strSQL);
				$leaveData[] = '-1';
				$leaveDataWithoutNoAffected[] = "-1";
				$find_userIDs =  array_keys($info);
				if (count($data) > 0 && count($info) > 0) {
					/*****************************/
					$strSQL = "SELECT isla.UserID, COUNT(isla.UserID) as CntLeave
							FROM
								INTRANET_SLRS_LESSON_ARRANGEMENT AS isla
							LEFT JOIN (
								SELECT
									MAX(DateLeave) as starting_date, UserID
								FROM
									INTRANET_SLRS_BALANCE_ADJUST
								WHERE
									AdjustedType = 'SET_YEAR' AND Deleted_at IS NULL and DateLeave BETWEEN '".$startDate."' AND '" . $endDate . "'
								GROUP By UserID
							) AS adjust ON (adjust.UserID=isla.UserID)
							Where
								isla.LeaveID IN (" . implode(",", $leaveDataWithoutNoAffected) . ")
								AND isla.UserID IN (". implode(',', $find_userIDs) . ")
								AND isla.TimeStart BETWEEN IF (IFNULL(starting_date, '".$startDate."') >= '".$startDate."', IFNULL(starting_date, '".$startDate."'), '".$startDate."') AND '".$endDate."'";
					
					if (isset($general_settings_SLRS['NoSubstitutionCalBalance']) && $general_settings_SLRS['NoSubstitutionCalBalance']=='1')
					{
						$strSQL .= " AND ArrangedTo_UserID != '-999'";
					}
					$strSQL .= " GROUP BY UserID";
					$data = $this->returnResultSet($strSQL);
					$cntLeave= BuildMultiKeyAssoc($data, 'UserID');
					/*****************************/
					$strSQL = "SELECT ArrangedTo_UserID as UserID, COUNT(ArrangedTo_UserID) as SubstituteUser
							FROM
								INTRANET_SLRS_LESSON_ARRANGEMENT AS isla
							LEFT JOIN (
								SELECT
									MAX(DateLeave) as starting_date, UserID
								FROM
									INTRANET_SLRS_BALANCE_ADJUST
								WHERE
									AdjustedType = 'SET_YEAR' AND Deleted_at IS NULL and DateLeave BETWEEN '".$startDate."' AND '" . $endDate . "'
								GROUP By UserID
							) AS adjust ON (adjust.UserID=isla.ArrangedTo_UserID)
							Where
								isla.LeaveID IN (" . implode(",", $leaveData) . ")
								AND isla.ArrangedTo_UserID IN (". implode(',', $find_userIDs) . ")
								AND isla.TimeStart BETWEEN IF (IFNULL(starting_date, '".$startDate."') >= '".$startDate."', IFNULL(starting_date, '".$startDate."'), '".$startDate."') AND '".$endDate."'
							GROUP BY isla.ArrangedTo_UserID";
					$data = $this->returnResultSet($strSQL);
					$substituteUser = BuildMultiKeyAssoc($data, 'UserID');
					/*****************************/
					// 					$strSQL = "SELECT UserID, MIN(DateTimeInput) as DateTimeInput, BalanceAdjustID, BalanceBeforeAdjust, BalanceAdjustTo
					// 							FROM(
					// 								SELECT YearTermID,AcademicYearID,TermStart,TermEnd FROM ACADEMIC_YEAR_TERM WHERE YearTermID IN (". $semsterArray[0] .")
					// 								) as ayt
					// 								INNER JOIN(
							// 								SELECT UserID, BalanceAdjustTo, DateTimeInput, BalanceBeforeAdjust, BalanceAdjustID FROM INTRANET_SLRS_BALANCE_ADJUST WHERE AcademicYearID = $academicYearID
							// 								) as isba ON isba.DateTimeInput BETWEEN TermStart AND TermEnd
					// 								WHERE UserID IN (". implode(',', $find_userIDs) . ")
					// 							GROUP BY UserID";
					
					$strSQL = "SELECT
										ISBA_a.UserID, ISBA_a.BalanceAdjustID, DateTimeModified as DateTimeInput, BalanceBeforeAdjust, BalanceAdjustTo
									FROM
										INTRANET_SLRS_BALANCE_ADJUST as ISBA_a
									JOIN (
										SELECT
											MAX(BalanceAdjustID) as iBalanceAdjustID, UserID as iUserID FROM INTRANET_SLRS_BALANCE_ADJUST
										WHERE
											(DateLeave >= '" . $startDate. "' AND DateLeave <= '" . $endDate. "' AND Deleted_at IS NULL AND ResetType IS NOT NULL)
											OR
											(DateLeave < '" . $startDate. "' AND Deleted_at IS NULL)
										GROUP BY UserID
									) as ISBA_b ON ( ISBA_a.BalanceAdjustID=iBalanceAdjustID and UserID=iUserID)
									Where
										AcademicYearID='" . $academicYearID . "' AND Deleted_at IS NULL
										AND UserID IN (". implode(',', $find_userIDs) . ")
									GROUP BY ISBA_a.UserID";
					/********************************************************************************************/
					$data = $this->returnResultSet($strSQL);
					$Balance = BuildMultiKeyAssoc($data, 'UserID');
				}
			}
			
			$reportData = array();
			if (count($info) > 0)
			{
				foreach ($info as $_UserID => $infoRec)
				{
					$hasRecord = false;
					$reportData[$_UserID]["UserName"] = $infoRec["UserName"];
					if ($cntLeave[$_UserID])
					{
						$reportData[$_UserID]["CntLeave"] = $cntLeave[$_UserID]["CntLeave"];
						$CntLeave = $cntLeave[$_UserID]["CntLeave"];
						$hasRecord = true;
					}
					else
					{
						$reportData[$_UserID]["CntLeave"] = "0";
						$CntLeave = 0;
					}
					if ($substituteUser[$_UserID])
					{
						$reportData[$_UserID]["SubstituteUser"] = $substituteUser[$_UserID]["SubstituteUser"];
						$CntSubstitution = $substituteUser[$_UserID]["SubstituteUser"];
						$hasRecord = true;
					}
					else
					{
						$reportData[$_UserID]["SubstituteUser"] = "0";
						$CntSubstitution = 0;
					}
					if ($Balance[$_UserID])
					{
						if (empty($Balance[$_UserID]["BalanceAdjustTo"])) $Balance[$_UserID]["BalanceAdjustTo"] = 0;
						
						$reportData[$_UserID]["BalanceAdjustTo"] = $Balance[$_UserID]["BalanceAdjustTo"] * $balanceDisplay;
						$CntPreYear = $Balance[$_UserID]["BalanceAdjustTo"];
					}
					else
					{
						if ($hasRecord)
						{
							$reportData[$_UserID]["BalanceAdjustTo"] = "0";
						}
						else
						{
							$reportData[$_UserID]["BalanceAdjustTo"] = "0";
						}
						$CntPreYear = 0;
					}
					$balance = $CntSubstitution - $CntLeave + $CntPreYear;
					$reportData[$_UserID]["balance"] = $balance * $balanceDisplay;
				}
			}
			return $reportData;
		}
		
		public function buildBalanceSummarySQL($args)
		{
			global $slrsConfig, $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
			$lgeneralsettings = new libgeneralsettings();
			
			if (count($args) > 0) extract($args);
			
			$balanceDisplay = $this->getBalanceDisplay();
			$name_field = $this->getNameFieldByLang("b.");
			// echo $name_field;
			
			if ($keyword !== '' && $keyword !== null) {
				// use Get_Safe_Sql_Like_Query for " LIKE "
				// use Get_Safe_Sql_Query for " = "
				$condsKeyword = " WHERE (
							UserName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR Balance LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR YearlyAdd LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR YearlyReduce LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR DateTimeModified LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
						  )
					";
			}
			
			$TermInfoArr = getCurrentAcademicYearAndYearTerm();
			$academicYearID = $TermInfoArr['AcademicYearID'];
			$Year_StartDate = getStartDateOfAcademicYear($academicYearID);
			$Year_EndDate = getEndDateOfAcademicYear($academicYearID);
			$Year_EndDate = date("Y-m-d 23:59:59", strtotime($Year_EndDate));
			$OfficialLeave = $this->getOfficialLeavelSetting($onlyDisableAdjust= true);
			$general_settings_SLRS = $lgeneralsettings->Get_General_Setting("SLRS");
			
			
			/********************************************************************************************/
			$officialLeavelSQL_add = "SELECT
							add_isla.UserID, COUNT(add_isla.UserID) as YearlyReduce
						FROM
							INTRANET_SLRS_LESSON_ARRANGEMENT AS add_isla
						LEFT JOIN
							INTRANET_SLRS_LEAVE AS add_isl ON (add_isla.LeaveID=add_isl.LeaveID)
						LEFT JOIN (
							SELECT MAX(DateLeave) as starting_date, UserID FROM INTRANET_SLRS_BALANCE_ADJUST WHERE AdjustedType = 'SET_YEAR' AND Deleted_at IS NULL GROUP By UserID
						) AS add_adjust ON (add_adjust.UserID=add_isla.UserID)
						WHERE
							add_isla.TimeStart >= IFNULL(starting_date, '" . $Year_StartDate . "') AND add_isla.TimeStart <= '" . $Year_EndDate . "'";
			if (isset($general_settings_SLRS['NoSubstitutionCalBalance']) && $general_settings_SLRS['NoSubstitutionCalBalance'] === '1')
			{
				$officialLeavelSQL_add.= " AND ArrangedTo_UserID != '-999'";
			}
			if (count($OfficialLeave) > 0)
			{
				$officialLeavelSQL_add.= " AND (ReasonCode NOT IN ('" . implode("', '", array_keys($OfficialLeave)) . "') OR ReasonCode IS NULL)";
			}
			$officialLeavelSQL_add .= " GROUP BY add_isla.UserID ORDER BY add_isla.TimeStart";
			/********************************************************************************************/
			/********************************************************************************************/
			$officialLeavelSQL_sub = "SELECT
							sub_isla.ArrangedTo_UserID as UserID, COUNT(sub_isla.ArrangedTo_UserID) as YearlyAdd
						FROM
							INTRANET_SLRS_LESSON_ARRANGEMENT AS sub_isla
						LEFT JOIN
							INTRANET_SLRS_LEAVE AS sub_isl ON (sub_isla.LeaveID=sub_isl.LeaveID)
						LEFT JOIN (
							SELECT MAX(DateLeave) as starting_date, UserID FROM INTRANET_SLRS_BALANCE_ADJUST WHERE AdjustedType = 'SET_YEAR' AND Deleted_at IS NULL GROUP By UserID
						) AS sub_adjust ON (sub_adjust.UserID=sub_isla.UserID)
						WHERE
							sub_isla.TimeStart >= IFNULL(starting_date, '" . $Year_StartDate . "') AND sub_isla.TimeStart <= '" . $Year_EndDate . "'";
			
			$officialLeavelSQL_sub.= " GROUP BY sub_isla.ArrangedTo_UserID ORDER BY sub_isla.TimeStart";
			/********************************************************************************************/
			if ($args['data_only'])
			{
				$checkbox_str = "UserName,Balance * ".$balanceDisplay." as Balance, IFNULL(YearlyAdd, '0') as YearlyAdd, IFNULL(YearlyReduce, '0') as YearlyReduce, IFNULL(DateTimeModified, '-') as DateTimeModified";
				$data_fields = $name_field." as UserName, Balance, YearlyAdd, YearlyReduce,
								c.MaxDateTimeModified as DateTimeModified,
								b.EnglishName";
			}
			else
			{
				$checkbox_str = "UserName,Balance * ".$balanceDisplay." as Balance, IFNULL(YearlyAdd, '0') as YearlyAdd, IFNULL(YearlyReduce, '0') as YearlyReduce, IFNULL(DateTimeModified, '-') as DateTimeModified, checkbox";
				$data_fields = $name_field." as UserName, Balance, YearlyAdd, YearlyReduce,
								CONCAT('<a id=\"adjustedDate_',a.UserID,'\" class=\"tablelink\" href=\"javascript:thickBox(',a.UserID,')\">',c.MaxDateTimeModified,'</a>') as DateTimeModified,
								CONCAT('<input type=checkbox name=userIdAry[] id=userIdAry[] value=\"',a.UserID,'\">') as checkbox,
								b.EnglishName";
			}
			$sql = "
					SELECT
						" . $checkbox_str . "
					FROM (
						SELECT
							" . $data_fields . "
						FROM (
							SELECT
								UserID, Balance
							FROM
								INTRANET_SLRS_TEACHER
						) as a
						INNER JOIN(
							SELECT
								UserID, EnglishName, ChineseName, TitleChinese, TitleEnglish
							FROM
								".$slrsConfig['INTRANET_USER']."
								WHERE
								Teaching<>'S' AND RecordStatus=1 AND RecordType='1'
								) as b ON a.UserID=b.UserID
								LEFT JOIN (
								$officialLeavelSQL_add
								) AS ol_add ON (a.UserID=ol_add.UserID)
								LEFT JOIN (
								$officialLeavelSQL_sub
								) AS ol_sub ON (a.UserID=ol_sub.UserID)
								LEFT JOIN(
								SELECT
								UserID, ISBA_1.BalanceAdjustID, BalanceAdjustTo, DATE_FORMAT(MAX(DateTimeModified),'%Y-%m-%d %H:%i') as MaxDateTimeModified
								FROM
								INTRANET_SLRS_BALANCE_ADJUST as ISBA_1
								JOIN (
								SELECT
								MAX(BalanceAdjustID) as iBalanceAdjustID, UserID as iUserID FROM INTRANET_SLRS_BALANCE_ADJUST
								WHERE
								AcademicYearID='" . $academicYearID . "' AND Deleted_at IS NULL
									GROUP BY UserID
								) as ISBA_2 ON ( ISBA_1.BalanceAdjustID=iBalanceAdjustID and UserID=iUserID)
								Where
									AcademicYearID='" . $academicYearID . "' AND Deleted_at IS NULL
								GROUP BY UserID
						) as c ON a.UserID=c.UserID
					) as a
					".$condsKeyword."";
			return $sql;
		}
		
		public function getExpiredUserIDs()
		{
			global $slrsConfig;
			$strSQL = "SELECT UserID FROM " . $slrsConfig['INTRANET_USER'] . " WHERE RecordStatus='0' and RecordType='1'";
			$data = $this->returnVector($strSQL);
			return $data;
		}
		
		/*
		 * check exchange record has Substitution record
		 */
		public function checkWithSubstitution($data)
		{
			return $data;
		}
		
		public function getExchangedTeachersByDate($date, $teacherList)
		{
			global $slrsConfig;
			$userIDs = array();
			if (count($teacherList) > 0)
			{
				foreach ($teacherList as $key => $val)
				{
					$userIDs[] = $val["UserID"];
				}
			}
			$strSQL = "SELECT
						 iu.UserID, iu.EnglishName, iu.ChineseName
						FROM
							INTRANET_SLRS_LESSON_EXCHANGE as isle
						JOIN
							".$slrsConfig['INTRANET_USER']." AS iu ON (iu.UseriD=TeacherA_UserID)
						WHERE
							TeacherB_Date='" . $date . "'";
			if (count($userIDs) > 0) {
				$strSQL .= " AND TeacherA_UserID NOT IN ('" . implode('", "', $userIDs) . "')";
			}
			$data = $this->returnResultSet($strSQL);
			if (count($data) > 0)
			{
				$index = count($teacherList);
				foreach ($data as $kk => $vv)
				{
					if (!in_array($vv["UserID"], $userIDs))
					{
						$teacherList[$index] = $vv;
					}
				}
			}
			return $teacherList;
		}
		
		public function getExchangedLessonByDateTeacher($date, $teacherID)
		{
			global $slrsConfig, $PATH_WRT_ROOT, $Lang;
			if($this->isEJ())
			{
				include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
			}
			else
			{
				include_once($PATH_WRT_ROOT."includes/libtimetable.php");
			}
			$ltimetable = new Timetable();
			$curTimetableId = $ltimetable->Get_Current_Timetable($date);
			$academicTimeSlot = convertMultipleRowsIntoOneRow($this->getAcademicTimeSlot($date),"TimeSlotID");
			
			## check with Exchanged Course
			$strSQL = "SELECT
				isle.*, isla.*, itt.*, stc.*, isle.LessonExchangeID
			FROM
				INTRANET_SLRS_LESSON_EXCHANGE as isle
			INNER JOIN
				INTRANET_TIMETABLE_TIMESLOT AS itt ON (itt.TimeSlotID IN (".$academicTimeSlot.") AND itt.TimeSlotID=isle.TeacherB_TimeSlotID)
			LEFT JOIN
				INTRANET_SLRS_LESSON_ARRANGEMENT AS isla ON (isle.TeacherB_UserID=isla.UserID AND DATE(TimeStart)='" . $date . "' AND isla.TimeSlotID = isle.TeacherB_TimeSlotID AND DATE(TimeStart)=isle.TeacherB_Date)
			LEFT JOIN (
				SELECT
					LocationID, NameChi, NameEng, DisplayOrder, Code, BarCode
				FROM
					INVENTORY_LOCATION
			) as il ON isle.TeacherB_LocationID = il.LocationID
			LEFT JOIN (
				SELECT
					SubjectGroupID, ClassCode, ClassTitleEN, ClassTitleB5
				FROM
					SUBJECT_TERM_CLASS
			) as stc ON isle.TeacherA_SubjectGroupID=stc.SubjectGroupID
			LEFT JOIN (
				SELECT
					SubjectTermID, SubjectGroupID, SubjectID, YearTermID
				FROM
					SUBJECT_TERM
			) as st ON isle.TeacherA_SubjectGroupID = st.SubjectGroupID
			WHERE
				TeacherB_Date='" . $date . "' AND TeacherA_UserID='" . $teacherID. "'";
			
			$b = array();
			$exchangedRecords = $this->returnResultSet($strSQL);
			if (count($exchangedRecords) > 0)
			{
				foreach ($exchangedRecords as $kk => $vv)
				{
					if ($vv["combined_type"] != "SLAVE" || $vv["combined_type"] == "SLAVE" && $vv["has_another"] !== '0')
					{
					    $vv["TimeSlotName"] = ($this->isEJ())? convert2unicode($vv["TimeSlotName"]) : $vv["TimeSlotName"];
					    
						$exchangedRecord = array(
								"TimeSlotID" => $vv["TeacherB_TimeSlotID"],
								"TimeSlotName" => $vv["TimeSlotName"] . "<br><span style='color:#f00'>(**" . $Lang['SLRS']['SubstitutionArrangementFromExchangeRecord'] . ")</span>",
								"StartTime" => date("H:i", strtotime($vv["TeacherB_TimeStart"])),
								"EndTime" => date("H:i", strtotime($vv["TeacherB_TimeEnd"])),
								"DisplayOrder" => $vv["DisplayOrder"],
								"RoomAllocationID" => "",
								"Day" => $day,
								"LocationID" => $vv["TeacherB_LocationID"],
								"SubjectGroupID" => $vv["TeacherA_SubjectGroupID"],
								"OthersLocation" => "",
								"RecordDate" => $vv["TeacherB_Date"],
								"CycleDay" => $day,
								"ClassCode" => $vv["ClassCode"],
								"ClassTitleEN" => $vv["ClassTitleEN"],
								"ClassTitleB5" => $this->displayChinese($vv["ClassTitleB5"]),
								"NameChi" => $this->displayChinese($vv["NameChi"]),
								"NameEng" => $vv["NameEng"],
								"Code" => "",
								"BarCode" => "",
								"UserID" => $userID,
								"EnglishName" => $userInfo["0"]["EnglishName"],
								"ChineseName" => $userInfo["0"]["ChineseName"],
								"SubjectTermID" => $vv["SubjectTermID"],
								"SubjectID" => $vv["TeacherB_SubjectID"],
								"islUserID" => $userID,
								"LeaveID" => $leaveID,
								"leID" => $vv["LessonExchangeID"],
								"LessonArrangementID" => $vv["LessonArrangementID"],
								"ArrangedTo_UserID" => $vv["ArrangedTo_UserID"],
								"ArrangedTo_LocationID" => $vv["ArrangedTo_LocationID"],
								"isVolunteer" => $vv["isVolunteer"],
								"LessonRemarks" => $vv["LessonRemarks"]
						);
						$b[] = $exchangedRecord;
					}
				}
			}
			return $b;
		}
		
		public function getTeacherOptionByID($userID)
		{
			global $slrsConfig;
			$teacher = array();
			$sql = "SELECT
						UserID, EnglishName, ChineseName
					FROM
						".$slrsConfig['INTRANET_USER']."
					WHERE
						UserID='" . $userID . "' LIMIT 1" 
					;
			$userInfo = $this->returnResultSet($sql);
			if ($userInfo!== false) {
				foreach ($userInfo as $kk => $val) {
					$key = $val["EnglishName"] . "_" . $val["UserID"];
					$teacher = $val;
					$teacher["key"] = $key;
				}
			}
			return $teacher;
		}
		
		/**
		 * Get Substitution Arrangement detail 
		 * @param array() $listData
		 * @param array() $gridInfo
		 * @param boolean $isForLeave
		 * @return JSON string
		 */
		public function getArrangeDetailsForListing($listData, $gridInfo, $isForLeave = true)
		{
			global $slrsConfig, $intranet_session_language, $Lang;
			$name_field = $this->getNameFieldByLang("iu.");
			if (count($listData) > 0)
			{
				if ($isForLeave)
				{
					$conditionKey = array_search("LeaveID", $gridInfo["fields"]);
				}
				else 
				{
					$conditionKey= array_search("LessonArrangementID", $gridInfo["fields"]);
				}
				$lessonInfoKey = array_search("Lesson", $gridInfo["fields"]);
				if ($conditionKey >= 0 && $lessonInfoKey >= 0)
				{
					foreach ($listData as $key => $val)
					{
						if (isset($val[$conditionKey]) && $val[$conditionKey] > 0)
						{
							$strSQL = "SELECT TimeSlotName, LEFT(StartTime, 5) as StartTime, LEFT(EndTime, 5) as EndTime, SessionType,
										ArrangedTo_UserID, " . $name_field . " as UserName, ClassTitleEN, ClassTitleB5, NameChi, NameEng, Description, DATE(TimeStart) as LeaveDate";
							$strSQL .= " FROM INTRANET_SLRS_LESSON_ARRANGEMENT AS isla";
							$strSQL .= " LEFT JOIN INTRANET_TIMETABLE_TIMESLOT AS itt ON (itt.TimeSlotID=isla.TimeSlotID)";
							$strSQL .= " LEFT JOIN INVENTORY_LOCATION AS il ON (il.LocationID=isla.ArrangedTo_LocationID)";
							$strSQL .= " LEFT JOIN SUBJECT_TERM_CLASS AS stc ON (stc.SubjectGroupID=isla.SubjectGroupID)"; 
							$strSQL .= " LEFT JOIN ".$slrsConfig['INTRANET_USER']." AS iu ON (iu.UserID=isla.ArrangedTo_UserID)";
							if ($isForLeave)
							{
								$leaveID = $val[$conditionKey];
								$strSQL .= " WHERE LeaveID = '" . $leaveID . "'";
							}
							else 
							{
								$lessonArrangementID = $val[$conditionKey];
								$strSQL .= " WHERE LessonArrangementID = '" . $lessonArrangementID. "'";
							}
							$strSQL .= " ORDER BY TimeStart ASC";
							$defailInfo = $this->returnResultSet($strSQL);

							if (count($defailInfo) > 0)
							{
								$numOfAffectedLesson = $listData[$key][$lessonInfoKey];
								$listData[$key][$lessonInfoKey] = "";
								$listData[$key][$lessonInfoKey] .= $Lang['SLRS']['SubstitutionArrangementDes']['AffectedLesson'] . " : " . $numOfAffectedLesson;
								$listData[$key][$lessonInfoKey] .= "<div class='slrs-card slrs-card-sl slrs-card-blue'>";
								
								foreach ($defailInfo as $rKey => $rVal)
								{
									$listData[$key][$lessonInfoKey] .= "<div class='slrs-title'>" . $this->displayChinese($rVal["TimeSlotName"]) . " (" . $rVal["StartTime"] . " - " . $this->displayChinese($rVal["EndTime"]) . ")</div>";
									$listData[$key][$lessonInfoKey] .= "<div class='slrs-card-body'>";
									if ($intranet_session_language == "en")
									{
										$listData[$key][$lessonInfoKey] .= $rVal["ClassTitleEN"] . "<br>" . $rVal["NameEng"];
										if (!empty($rVal["Description"]))
										{
											$listData[$key][$lessonInfoKey] .= " (" . $rVal["Description"] . ")";
										}
										$listData[$key][$lessonInfoKey] .= "<br>";
									}
									else 
									{
										if (empty($rVal["ClassTitleB5"])) $rVal["ClassTitleB5"] = $rVal["ClassTitleEN"];
										if (empty($rVal["NameChi"])) $rVal["NameChi"] = $rVal["NameEng"];
										$listData[$key][$lessonInfoKey] .= $this->displayChinese($rVal["ClassTitleB5"]) . "<br>" . $this->displayChinese($rVal["NameChi"]);
										if (!empty($rVal["Description"]))
										{
											$listData[$key][$lessonInfoKey] .= " (" . $this->displayChinese($rVal["Description"]) . ")";
										}
										$listData[$key][$lessonInfoKey] .= "<br>";
									}
									if ($rVal["ArrangedTo_UserID"] > 0)
									{
										$session = $rVal["SessionType"];
										$session = ($session!= "am" && $session!= "pm")?"%":$session;
										if (!isset($checked[$rVal["ArrangedTo_UserID"] . "_" . $session]))
										{
											$strSQL = "SELECT
													LeaveID
											 	FROM
													INTRANET_SLRS_LEAVE
												WHERE UserID='" . $rVal["ArrangedTo_UserID"] . "' and Duration like '" . $session . "' and DateLeave like '" . $rVal["LeaveDate"] . "'
											";
											$leaveInfo = $this->returnResultSet($strSQL);
											$checked[$rVal["ArrangedTo_UserID"] . "_" . $session] = $leaveInfo;
										}
										if (count($checked[$rVal["ArrangedTo_UserID"] . "_" . $session]) > 0)
										{
											$listData[$key][$lessonInfoKey] .= "<span style='color:#ff6868'>" . $rVal["UserName"] . " " . $Lang['SLRS']['SubstitutionArrangementSubTC'];
											$listData[$key][$lessonInfoKey] .= ' <i class="fa fa-warning"></i> ' . $Lang['SLRS']['SubstitutionArrangementDes']['ExistTeacherSubstitutionListArrangeAgain'] . "</span><br>";
										}
										else 
										{
											$listData[$key][$lessonInfoKey] .= $rVal["UserName"] . " " . $Lang['SLRS']['SubstitutionArrangementSubTC'] . "<br>";
										}
									}
									else
									{
										$listData[$key][$lessonInfoKey] .= $Lang['SLRS']['SubstitutionArrangementDes']['ignoreThisRec'] . "<br>";
									}
									$listData[$key][$lessonInfoKey] .= "</div>";
								}
								$listData[$key][$lessonInfoKey] .= "</div>";
							}
							else
							{
								$numOfAffectedLesson = $listData[$key][$lessonInfoKey];
								if (empty($numOfAffectedLesson))
								{
									$listData[$key][$lessonInfoKey] = '-';
								}
							}
						}
						$check_with_subtc = true;
						if ($check_with_subtc)
						{
							$teacherKey = array_search("TeacherName", $gridInfo["fields"]);
							$durationKey = array_search("orgDuration", $gridInfo["fields"]);
							$userKey = array_search("UserID", $gridInfo["fields"]);
							$dateleaveKey = array_search("DateLeave", $gridInfo["fields"]);
							
							$session = $val[$durationKey];
							$session = ($session!= "am" && $session!= "pm")?"%":$session;

							$sql = "SELECT
										isla.TimeSlotID, TimeSlotName, StartTime, EndTime
									FROM
										INTRANET_SLRS_LESSON_ARRANGEMENT AS isla
									INNER JOIN
										INTRANET_TIMETABLE_TIMESLOT AS itt ON (itt.TimeSlotID=isla.TimeSlotID)
									WHERE
										ArrangedTo_UserID='" . $val[$userKey] . "' AND Date(TimeStart)='" . $val[$dateleaveKey] . "' AND SessionType like '".$session."'
									";
							$checkSA = $this->returnResultSet($sql);
							if (count($checkSA) > 0) {
								$listData[$key][$teacherKey] .= "<br><div style='margin-top:5px;color:#ff6868'> <i class='fa fa-info-circle' aria-hidden='true'></i> " . $Lang['SLRS']['SubstitutionArrangementDes']['ExistTeacherSubstitutionList'] . "</div>";
							}
						}
					}
				}
			}
			return $listData;
		}
		
		/**
		 * Check Teacher with Limitation of General Settings
		 * @param unknown $request ($POST value)
		 * @return array() 
		 */
		public function checkSettingsLimitation($request)
		{
			global $slrsConfig, $PATH_WRT_ROOT, $Lang;
			include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
			if($this->isEJ())
			{
				include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
			}
			else
			{
				include_once($PATH_WRT_ROOT."includes/libtimetable.php");
			}
			$ltimetable = new Timetable();
			$lgeneralsettings = new libgeneralsettings();
			
			$output = array('result' => 'success');
			$general_settings_SLRS = $lgeneralsettings->Get_General_Setting("SLRS");
			
			$checking["date"] = $request['datePicker']; 
			$checking["leaveID"] = $request['leaveID'];
			$dateArr[0] = $checking["date"];
			$sql = $this->getDaySQL($dateArr,"");
			$a = $this->returnResultSet($sql);
			$checking["CycleDay"] = $a[0]["CycleDay"];
			$checking["timeTableID"] = $ltimetable->Get_Current_Timetable($checking["date"]);
			$checking["DailyMaximunLessons"] = $Lang['SLRS']['DailyMaximunLessonsVal'][$general_settings_SLRS["DailyMaximunLessons"]];
			$checking["DailyMaximunSubstitution"] = $general_settings_SLRS["DailyMaximunSubstitution"];

			if ($checking["DailyMaximunLessons"] != 999 || $checking["DailyMaximunSubstitution"] > 0)
			{
				if (count($request) > 0)
				{
					foreach ($request as $key => $val)
					{
						if (strpos($key, "teacher_") !== false && $val != "-999")
						{
							if (isset($checking["teacher"][$val]))
							{
								$checking["teacher"][$val]["request_lesson"]++;
							}
							else 
							{
								$checking["teacher"][$val] = array(
										"org_lesson" => 0,
										"leave_lesson" => 0,
										"substitution_lesson" => 0,
										"exchange_lessonA" => 0,
										"exchange_lessonB" => 0,
										"request_lesson" => 1,
										'status' => 'available'
								);
							}
						}
					}
					if (count($checking["teacher"]) > 0)
					{
						########################################
						## get number of Timeslot (Orginal)
						$strSQL = "Select
									UserID, COUNT(TimeSlotID) as num_of_timeslot
								FROM
									INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
								INNER JOIN
									SUBJECT_TERM_CLASS_TEACHER AS stct ON (stct.SubjectGroupID=itra.SubjectGroupID)
								WHERE
									itra.TimeTableID='" . $checking["timeTableID"] . "' and Day='" . $checking["CycleDay"] . "' AND UserID IN ('" . implode("', '", array_keys($checking["teacher"])) . "')
								GROUP BY UserID 
									";
						$query_rec = $this->returnResultSet($strSQL);
						if (count($query_rec) > 0)
						{
							foreach ($query_rec as $kk => $vv)
							{
								if (isset($checking["teacher"][$vv["UserID"]]))
								{
									$checking["teacher"][$vv["UserID"]]["org_lesson"] = $vv["num_of_timeslot"];
								}
							}
						}
						########################################
						## get number of Timeslot (Leave)
						$strSQL = "SELECT
										UserID, COUNT(TimeSlotID) as num_of_timeslot
									FROM
										INTRANET_SLRS_LESSON_ARRANGEMENT
									WHERE
										UserID IN ('" . implode("', '", array_keys($checking["teacher"])) . "')
										AND LeaveID != '" . $checking["leaveID"] . "' AND DATE(TimeStart) = '" . $checking["date"] . "'
									GROUP BY UserID";
						$query_rec = $this->returnResultSet($strSQL);
						if (count($query_rec) > 0)
						{
							foreach ($query_rec as $kk => $vv)
							{
								if (isset($checking["teacher"][$vv["UserID"]]))
								{
									$checking["teacher"][$vv["UserID"]]["leave_lesson"] = $vv["num_of_timeslot"];
								}
							}
						}
						########################################
						## get number of Timeslot (Substitution)
						$strSQL = "SELECT 
										ArrangedTo_UserID as UserID, COUNT(TimeSlotID) as num_of_timeslot 
									FROM
										INTRANET_SLRS_LESSON_ARRANGEMENT
									WHERE
										ArrangedTo_UserID IN ('" . implode("', '", array_keys($checking["teacher"])) . "')
										AND LeaveID != '" . $checking["leaveID"] . "' AND DATE(TimeStart) = '" . $checking["date"] . "'
									GROUP BY UserID";
						$query_rec = $this->returnResultSet($strSQL);
						if (count($query_rec) > 0)
						{
							foreach ($query_rec as $kk => $vv)
							{
								if (isset($checking["teacher"][$vv["UserID"]]))
								{
									$checking["teacher"][$vv["UserID"]]["substitution_lesson"] = $vv["num_of_timeslot"];
								}
							}
						}
						########################################
						## get number of Timeslot (Exchanged out)
						$strSQL = "SELECT
										TeacherA_UserID as UserID, COUNT(TeacherA_TimeSlotID) as num_of_timeslot
									FROM
										INTRANET_SLRS_LESSON_EXCHANGE
									WHERE
										TeacherA_UserID IN ('" . implode("', '", array_keys($checking["teacher"])) . "')
										AND TeacherA_Date = '" . $checking["date"] . "'
									GROUP BY TeacherA_UserID";
						$query_rec = $this->returnResultSet($strSQL);
						if (count($query_rec) > 0)
						{
							foreach ($query_rec as $kk => $vv)
							{
								if (isset($checking["teacher"][$vv["UserID"]]))
								{
									$checking["teacher"][$vv["UserID"]]["exchange_lessonA"] = $vv["num_of_timeslot"];
								}
							}
						}
						########################################
						## get number of Timeslot (Exchanged in)
						$strSQL = "SELECT
										TeacherA_UserID as UserID, COUNT(TeacherB_TimeSlotID) as num_of_timeslot
									FROM
										INTRANET_SLRS_LESSON_EXCHANGE
									WHERE
										TeacherA_UserID IN ('" . implode("', '", array_keys($checking["teacher"])) . "')
										AND TeacherB_Date = '" . $checking["date"] . "'
									GROUP BY TeacherA_UserID";
						$query_rec = $this->returnResultSet($strSQL);
						if (count($query_rec) > 0)
						{
							foreach ($query_rec as $kk => $vv)
							{
								if (isset($checking["teacher"][$vv["UserID"]]))
								{
									$checking["teacher"][$vv["UserID"]]["exchange_lessonB"] = $vv["num_of_timeslot"];
								}
							}
						}
						
						$hasError = false;
						$errUser = array();
						########################################
						## checking for Teacher
						foreach ($checking["teacher"] as $UserID => $val)
						{
							$total_daily_no_of_lesson = $val["org_lesson"] - $val["leave_lesson"] + $val["substitution_lesson"] - $val["exchange_lessonA"] + $val["exchange_lessonB"] + $val["request_lesson"];
							if ($checking["DailyMaximunLessons"] != 999 && $checking["DailyMaximunLessons"] < $total_daily_no_of_lesson)
							{
								$checking["teacher"][$UserID]["status"] = "fail";
								$hasError = true;
								$errUser[$UserID]["msg"]["OutOfMaxLessonLimitation"] = $Lang["SLRS"]["OutOfMaxLessonLimitation"];
							}
							
							$num_of_substitution = $val["substitution_lesson"] + $val["request_lesson"];
							if ($checking["DailyMaximunSubstitution"] > 0 && $checking["DailyMaximunSubstitution"] < $num_of_substitution)
							{
								$checking["teacher"][$UserID]["status"] = "fail";
								$hasError = true;
								$errUser[$UserID]["msg"]["OutOfMaxSubstitutionLimitation"] = $Lang["SLRS"]["OutOfMaxSubstitutionLimitation"];
							}
						}
						
						## has error
						if ($hasError && count($errUser) > 0)
						{
							$output["result"] = "failed";
							$name_field = $this->getNameFieldByLang("");
							$strSQL = "SELECT UserID, " . $name_field . " AS UserName FROM " . $slrsConfig['INTRANET_USER'] . " WHERE UserID IN ('" . implode("', '", array_keys($errUser)) . "')";
							$query_rec = $this->returnResultSet($strSQL);
							if (count($query_rec) > 0)
							{
								foreach ($query_rec as $key => $val)
								{
									$errUser[$val["UserID"]]["name"] = $val["UserName"];
								}
								$output["errmsg"] = array_values($errUser);
							}
						}
					}
				}
			}
			return $output;
		}
		
		public function getClassInfoBySubjectGroupID($subjectGroupID, $classIDs)
		{
			
			$strSQL = "select 
							yc.YearClassID, yc.ClassTitleEN, yc.ClassTitleB5 
						from 
							SUBJECT_TERM_CLASS_USER as stcu
						inner join 
						  	YEAR_CLASS_USER as ycu on (stcu.UserID=ycu.UserID)
						inner join
							YEAR_CLASS as yc on (ycu.YearClassID=yc.YearClassID)
						where 
							stcu.SubjectGroupID='" . $subjectGroupID . "' AND ycu.YearClassID in (" . $classIDs . ")
						group by yc.YearClassID
						order by ClassTitleEN asc
						";
			$query_rec = $this->returnResultSet($strSQL);
			return $query_rec;
			
		}
		
		public function getLocationInfoByIDs($locationIDs)
		{
			$locationInfo = array();
			if (count($locationIDs) > 0)
			{
				$strSQL = "SELECT
							LocationID, il.Code, il.Barcode, il.NameChi, il.NameEng,
							ill.Code as level_Code, ill.Barcode as level_Barcode, ill.NameChi as level_NameChi, ill.NameEng as level_NameEng,
							ilb.Code as Bldg_Code, ilb.Barcode as Bldg_Barcode, ilb.NameChi as Bldg_NameChi, ilb.NameEng as Bldg_NameEng
						FROM
							INVENTORY_LOCATION as il
						LEFT JOIN
							INVENTORY_LOCATION_LEVEL AS ill ON ( ill.LocationLevelID=il.LocationLevelID )
						LEFT JOIN
							INVENTORY_LOCATION_BUILDING AS ilb ON ( ill.BuildingID=ilb.BuildingID )
						WHERE LocationID IN ('" . implode("', '", $locationIDs) . "')";
				
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0)
				{
					foreach ($result as $key => $val)
					{
						$locationInfo[$val['LocationID']] = $val;
						
						$LocationName = Get_Lang_Selection( ($this->isEJ() ? convert2unicode($val["NameChi"],1,1) : $val["NameChi"]), $val["NameEng"]);
						$locationInfo[$val['LocationID']]["LocationName"] = $LocationName;
						
						$LocationName = Get_Lang_Selection( ($this->isEJ() ? convert2unicode($val["level_NameChi"],1,1) : $val["level_NameChi"]), $val["level_NameEng"]);
						$locationInfo[$val['LocationID']]["LevelName"] = $LocationName;
						
						$LocationName = Get_Lang_Selection( ($this->isEJ() ? convert2unicode($val["Bldg_Barcode"],1,1) : $val["Bldg_NameChi"]), $val["Bldg_NameEng"]);
						$locationInfo[$val['LocationID']]["BuildingName"] = $LocationName;
					}
				}
			}
			return $locationInfo;
		}
		
		/**
		 * 
		 * @param unknown $request
		 * example 
		 * $request = array(
		 *                UserID => 26819,
		 *                date => 2018-09-17,
		 *                day => 4,
		 *                TimetableID => 281
		 *                TimeslotIDs => array()
		 *            );
		 * @return unknown[]
		 * 
		 */
		public function getTimeSlotInfoByIds($request)
		{
		    $output = array();
		    if (count($request["TimeslotIDs"]) > 0)
		    {
		        $strTimeslots = implode("', '", $request["TimeslotIDs"]);
		        $strSQL = "SELECT 
                                itt.TimeSlotID, itra.SubjectGroupID, itra.TimetableID, st.SubjectID, itra.LocationID, st.YearTermID,
                                TimeSlotName, LEFT(StartTime,5) as StartTime, LEFT(EndTime,5) as EndTime, 
                                SessionType, ClassCode, ClassTitleEN, ClassTitleB5,
                                EN_DES, CH_DES
                            FROM 
                                INTRANET_TIMETABLE_TIMESLOT AS itt
                            INNER JOIN
                                INTRANET_TIMETABLE_ROOM_ALLOCATION AS itra ON (itt.TimeSlotID=itra.TimeSlotID)
                            INNER JOIN
                                SUBJECT_TERM_CLASS AS stc ON (stc.SubjectGroupID=itra.SubjectGroupID)
                            INNER JOIN
                                SUBJECT_TERM_CLASS_TEACHER AS stct ON (stct.SubjectGroupID=itra.SubjectGroupID)
                            LEFT JOIN (
                				SELECT
                					SubjectTermID, SubjectGroupID, SubjectID, YearTermID
                				FROM
                					SUBJECT_TERM
                			) as st ON itra.SubjectGroupID = st.SubjectGroupID
                            LEFT JOIN
                                ASSESSMENT_SUBJECT AS sj ON (sj.RecordID=st.SubjectID)
                            WHERE
                                itt.TimeSlotID IN ('" . $strTimeslots. "') AND 
                                itra.Day='" . $request["day"] . "' AND 
                                stct.UserID='".IntegerSafe($request["UserID"])."'
                            ORDER BY itt.DisplayOrder";

		        $result = $this->returnResultSet($strSQL);
		        if (count($result) > 0) {
		            foreach ($result as $_record)
		            {
		                $output[$_record["TimeSlotID"]] = array_merge($_record, $request);
		            }

		            if (count($output) > 0)
		            {
		                foreach ($output as $key => $_record)
		                {
		                    // check with Lessage Arrangement
		                    $strSQL = "SELECT
                                    COUNT(LessonArrangementID) as total_record
                                FROM
                                    INTRANET_SLRS_LESSON_ARRANGEMENT
                                WHERE
                                    UserID='".IntegerSafe($request["UserID"])."' AND
                                    TimeSlotID = '" . $_record["TimeSlotID"] . "' AND
                                    SubjectGroupID = '" . $_record["SubjectGroupID"] . "' AND
                                    TimeStart like '" . $request["date"] . "%'
                            ";
		                    $result = $this->returnResultSet($strSQL);
		                    if ($result[0]["total_record"] > 0)
		                    {
		                        $output[$key]["is_arranged"] = true;
		                    } else {
		                        $output[$key]["is_arranged"] = false;
		                    }
		                    // check with Lesson Exchange
		                    $strSQL = "SELECT 
                                            COUNT(LessonExchangeID) as total_record
                                        FROM
                                            INTRANET_SLRS_LESSON_EXCHANGE
                                        WHERE
                                            TeacherA_UserID='".IntegerSafe($request["UserID"])."' AND
                                            TeacherA_Date LIKE '" . $request["date"] . "' AND
                                            TeacherA_SubjectGroupID = '" . $_record["SubjectGroupID"] . "' AND
                                            TeacherA_TimeSlotID = '" . $_record["TimeSlotID"] . "'
                                        ";
		                    $result = $this->returnResultSet($strSQL);
		                    if ($result[0]["total_record"] > 0)
		                    {
		                        $output[$key]["is_exchanged"] = true;
		                    } else {
		                        $output[$key]["is_exchanged"] = false;
		                    }
		                    // check with Cancel Lesson
		                    $strSQL = "SELECT
                                            CancelLessonID, CancelRemark
                                        FROM
                                            INTRANET_SLRS_CANCEL_LESSON
                                        WHERE
                                            UserID='".IntegerSafe($request["UserID"])."' AND
                                            CancelDate LIKE '" . $request["date"] . "' AND
                                            SubjectGroupID = '" . $_record["SubjectGroupID"] . "' AND
                                            TimeSlotID = '" . $_record["TimeSlotID"] . "' AND
                                            DeletedAt IS NULL LIMIT 1
                                        ";
		                    $result = $this->returnResultSet($strSQL);
		                    if (count($result) > 0)
		                    {
		                        $output[$key]["is_cancelled"] = true;
		                        $output[$key]["CancelLessonID"] = $result[0]["CancelLessonID"];
		                        $output[$key]["CancelRemark"] = $result[0]["CancelRemark"];
		                    } else {
		                        $output[$key]["is_cancelled"] = false;
		                    }
		                }
		            }
		        }
		    }
		    return $output;
		}
	}
}
?>