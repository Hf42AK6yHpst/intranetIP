<?php

// ############################################
// Date: 2018-07-10 Ivan [P141793] [ip.2.5.9.7.1]
// Modified Get_Management_Club_Attendance_SC_Info_Table() and Get_Management_Club_Attendance_SC_Info_Table() to ignore tap card records within 5 mins
// Date: 2017-03-24 Frankie
// Extends libclubsenrol_ui for Attendance Schedule Detail (Customization)
// ############################################
class libclubsenrol_ui_cust extends libclubsenrol_ui
{
    
    function __construct()
    {
        parent::interface_html();
    }
    
    function Get_Management_Club_Attendance_SC_Info_Table($EnrolGroupID, $ActivityID, $CanEdit, $PageAction, $ClubAttendanceInfoArr = array(), $DisplayPhoto = 0, $AcademicYearID = '')
    {
        global $eEnrollment, $Lang, $image_path, $LAYOUT_SKIN, $libenroll, $PATH_WRT_ROOT, $DisplayAllPhoto, $HiddenAllPhoto, $sys_custom;
        
        include_once ($PATH_WRT_ROOT . 'includes/libuser.php');
        
        $AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
        $isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
        $isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
        $isClubPIC = $libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolGroupID, "Club");
        $isClubHelper = $libenroll->IS_ENROL_HELPER($_SESSION['UserID'], $EnrolGroupID, "Club");
        
        // ####
        if (empty($ClubAttendanceInfoArr)) {
            // ## Get Club Attendance Records
            $ClubAttendanceInfoArr = $libenroll->Get_Club_Attendance_Info($EnrolGroupID);
            $ClubAttendanceInfoArr = $ClubAttendanceInfoArr[$EnrolGroupID];
        }
        
        // ## Get Club Meeting Dates
        $GroupDateIDArr = array_keys((array) $ClubAttendanceInfoArr['MeetingDateArr']);
        $numOfMeetingDate = count($GroupDateIDArr);
        
        // ## Special checking for HELPER - disable the attendance updating if the
        $disabledArr = array();
        if ($isClubHelper) {
            $StaffRole = "H"; // 'H' for Helper
            
            // Control Attendance Helper's Right
            if ($libenroll->disableHelperModificationRightLimitation != true) {
                // check for each Date
                for ($i = 0; $i < $numOfMeetingDate; $i ++) {
                    $tempGroupDateID = $GroupDateIDArr[$i];
                    
                    // check if there are any attendance records in DB
                    $sql = "SELECT DateModified, LastModifiedRole FROM INTRANET_ENROL_GROUP_ATTENDANCE WHERE EnrolGroupID = '$EnrolGroupID' AND GroupDateID = '$tempGroupDateID'";
                    $result = $libenroll->returnArray($sql, 2);
                    
                    // no attendance record yet => enable checkboxes for edit
                    
                    // otherwise, do checking
                    if (count($result) > 0) {
                        if ($result[0]['LastModifiedRole'] == "A") {
                            $disabledArr[$tempGroupDateID] = "DISABLED";
                        } else {
                            // disable checkboxes if last modified date is not today
                            if (date("Y-m-d") != date("Y-m-d", strtotime($result[0]['DateModified']))) {
                                $disabledArr[$tempGroupDateID] = "DISABLED";
                            }
                        }
                    }
                }
            }
        } else {
            $StaffRole = "A";
        }
        
        // ## Display the Student / Children only if it is in view mode
        $DisplayStudentIDArr = array();
        if ($PageAction == "view") {
            if ($_SESSION['UserType'] == USERTYPE_STUDENT) {
                $DisplayStudentIDArr[] = $_SESSION['UserID'];
            } else if ($_SESSION['UserType'] == USERTYPE_PARENT) {
                $libuser = new libuser($_SESSION['UserID']);
                $ChildArr = $libuser->getChildrenList();
                $DisplayStudentIDArr = Get_Array_By_Key($ChildArr, 'StudentID');
            }
        }
        
        // ## Get Club Members
        $ClubMemberInfoArr = $libenroll->Get_Club_Member_Info($EnrolGroupID, $PersonTypeArr = array(
            2
        ), $AcademicYearID, $IndicatorArr = array(
            'InactiveStudent'
        ), $IndicatorWithStyle = 1, $WithEmptySymbol = 1, $ReturnAsso = 0);
        
        $StudentIDArr = Get_Array_By_Key($ClubMemberInfoArr, 'StudentID');
        if ($libenroll->enableUserJoinDateRange()) {
            $enrolAvailDateArr = $libenroll->Get_Enrol_AvailDate($EnrolGroupID, $StudentIDArr, "Club");
        }
        $numOfMember = count($StudentIDArr);
        $x = '
			<div id="div_Club_AttendanceTable">
			<script language="javascript">
			function toggle_photo()
			{
				Reload_Club_Attendance_Info_Table2("' . $EnrolGroupID . '","' . $CanEdit . '","' . $PageAction . '", "' . ($DisplayPhoto == 1 ? "0" : "1") . '");
			}
			</script>
		';
        // ## Get Member Attendance Records
        $MemberAttendanceInfoArr = $libenroll->Get_Student_Club_Attendance_Info($StudentIDArr, array(
            $EnrolGroupID
        ));
        
        if ($CanEdit && $PageAction != 'view') {
            $x .= '<br />';
            $x .= $this->GET_NAVIGATION2($eEnrollment['take_attendance']);
        }
        
        // ## Navigation
        if ($numOfMeetingDate > 0) {
            for ($i = 0; $i < $numOfMeetingDate; $i ++) {
                $thisGroupDateID = $GroupDateIDArr[$i];
                $thisActivityDateStart = $ClubAttendanceInfoArr['MeetingDateArr'][$thisGroupDateID]['ActivityDateStart'];
                $thisActivityDateEnd = $ClubAttendanceInfoArr['MeetingDateArr'][$thisGroupDateID]['ActivityDateEnd'];
                $DateTimeDisplayArr[$i] = array(
                    $thisGroupDateID,
                    date("Y-m-d", strtotime($thisActivityDateStart)) . ' ' . date("H:i", strtotime($thisActivityDateStart)) . '-' . date("H:i", strtotime($thisActivityDateEnd))
                );
            }
            
            $id = "ActivityID";
            $name = "ActivityID";
            $eventSelection = $this->GET_SELECTION_BOX($DateTimeDisplayArr, " id='" . $id . "' name='" . $name . "' onChange='changeActivity(this)'", "", $ActivityID);
        }
        $x .= "<div style='margin:10px;'>" . $eventSelection . "</div>";
        if ($numOfMember == 0) {
            $x .= '<br />' . "\n";
            $x .= $Lang['eEnrolment']['Warning']['ClubHasNoMember'];
            $x .= '<br />' . "\n";
            $x .= '<br />' . "\n";
        } else {
            $AttendanceLogTimeArr = $this->Get_Attendance_Time_Log_Records($EnrolGroupID, $ActivityID, $MemberAttendanceInfoArr, "Club");
            
            $DisplayPhotoBtn .= "<a href=\"javascript:toggle_photo();\" class=\"tabletool\"><span id=\"DisplayAllPhotoTxt\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo_" . ($DisplayPhoto ? "zoomout" : "zoomin") . ".gif\" border=\"0\" align=\"absmiddle\" alt=\"$DisplayAllPhoto\"></span></a>";
            
            $x .= '<table id="AttendanceTable">' . "\n";
            // ## Table Header
            $x .= '<thead>' . "\n";
            $x .= '<tr class="tablegreentop">' . "\n";
            // Class
            $x .= '<td align="center" class="tableTitle tabletopnolink" width="50">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</td>' . "\n";
            
            // Class No.
            $x .= '<td align="center" class="tableTitle tabletopnolink" width="50">' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</td>' . "\n";
            
            // Student Name
            $x .= '<td class="tableTitle" nowrap style="border-right-width:5px"><span class="tabletopnolink">' . $eEnrollment['student_name'] . '</span> ' . $DisplayPhotoBtn . '</td>' . "\n";
            
            if ($libenroll->enableUserJoinDateRange()) {
                $x .= '<td class="tableTitle" colspan="2" align="center"><span class="tabletopnolink">' . $Lang['eEnrolment']['AvailiableDate'] . '</span></td>' . "\n";
            }
            
            // Attendance Status
            $x .= '<td align="center" class="tableTitle tabletopnolink" width="150">' . $Lang['eEnrolment']['AttendanceStatus'] . '<br />';
            $i = 0;
            $thisSelection = $this->Get_Student_Attendance_Selection("Header_Col" . $i, "Header_Col" . $i, $thisAttendanceStatus = '', "Header_Col" . $i);
            $thisApplyAllIcon = $this->Get_Apply_All_Icon("javascript:CheckAll('" . $i . "');");
            $x .= $thisSelection . "<div style='margin:0px auto; width:20px'>" . $thisApplyAllIcon . "</div>";
            
            $x .= '</td>' . "\n";
            
            if ($libenroll->enableAttendanceScheduleDetail() || $libenroll->enableUserJoinDateRange()) {
                // Time of Arrival
                $x .= '<td align="center" class="tableTitle tabletopnolink" width="150">' . $Lang['eEnrolment']['TimeofArrival'] . '</td>' . "\n";
                
                // Time of Departure
                $x .= '<td align="center" class="tableTitle tabletopnolink" width="150">' . $Lang['eEnrolment']['TimeofDeparture'] . '</td>' . "\n";
            }
            $x .= '</tr>' . "\n";
            $x .= '</thead>' . "\n";
            
            // ## Table Content
            $libuserObj = new libuser("", "", $StudentIDArr);
            $x .= '<tbody>' . "\n";
            // debug_pr($ClubMemberInfoArr);
            for ($i = 0; $i < $numOfMember; $i ++) {
                $thisStudentID = $ClubMemberInfoArr[$i]['StudentID'];
                $thisStudentName = $ClubMemberInfoArr[$i]['StudentName'];
                $thisClassName = $ClubMemberInfoArr[$i]['ClassName'];
                $thisClassNumber = $ClubMemberInfoArr[$i]['ClassNumber'];
                $thisWebSAMSRegNo = $ClubMemberInfoArr[$i]['WebSAMSRegNo'];
                $thisUserId = $ClubMemberInfoArr[$i]['UserID'];
                
                // ## Show user record only in view mode
                if ($PageAction == "view" && ! in_array($thisStudentID, $DisplayStudentIDArr))
                    continue;
                    
                    // ## Get Student Object
                    $libuserObj->LoadUserData($thisStudentID);
                    
                    // ## Student Name is a Link to update Student Performance & Comment for Enrollment Admin, Master and Club PIC
                    $thisStudentNameDisplay = '';
                    $thisStudentNameDisplay .= ($isEnrolAdmin || $isEnrolMaster || $isClubPIC) ? '<a class="tablelink" href="group_student_comment.php?AcademicYearID=' . $AcademicYearID . '&StudentID=' . $thisStudentID . '&EnrolGroupID=' . $EnrolGroupID . '">' : '';
                    $thisStudentNameDisplay .= $thisStudentName;
                    $thisStudentNameDisplay .= ($isEnrolAdmin || $isEnrolMaster || $isClubPIC) ? '</a>' : '';
                    
                    // ## Load Student Photos
                    list ($thisPhotoFilePath, $thisPhotoURL) = $libuserObj->GET_OFFICIAL_PHOTO($thisWebSAMSRegNo);
                    
                    if (file_exists($thisPhotoFilePath)) {
                        if ($DisplayPhoto == 1) {
                            $thisPhotoIconSpan = "";
                            
                            list ($originalWidth, $originalHeight) = getimagesize($thisPhotoFilePath);
                            $thisImgTag = $libenroll->Get_Offical_Photo_Img($thisPhotoURL, $originalWidth, $originalHeight);
                            $thisPhotoSpan = "<div id=\"photo_" . $thisStudentID . "\" style=\"display:inline\">";
                            $thisPhotoSpan .= $thisImgTag;
                            $thisPhotoSpan .= "</div>\n";
                        } else {
                            $thisPhotoIconSpan = "<div id=\"photo_" . $thisStudentID . "_icon\" style=\"display:inline\">";
                            $thisPhotoIconSpan .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo.gif\">";
                            $thisPhotoIconSpan .= "</div>\n";
                            
                            $thisPhotoSpan = "";
                        }
                    } else {
                        $thisPhotoIconSpan = "";
                        $thisPhotoSpan = "";
                    }
                    
                    $thisClass = 'tablegreenrow';
                    $thisClass .= ($i % 2) + 1;
                    
                    $x .= '<tr class="' . $thisClass . '">' . "\n";
                    $x .= '<td align="center" class="tabletext">' . $thisClassName . '</td>' . "\n";
                    $x .= '<td align="center" class="tabletext">' . $thisClassNumber . '</td>' . "\n";
                    $x .= '<td align="left" nowrap style="border-right-width:5px">' . "\n";
                    $x .= '<span class="tabletext">' . $thisPhotoIconSpan . " " . $thisStudentNameDisplay . '</span>' . "\n";
                    $x .= '<br />' . "\n";
                    $x .= $thisPhotoSpan . "\n";
                    $x .= '</td>' . "\n";
                    if ($libenroll->enableUserJoinDateRange()) {
                        if (isset($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateStart"]) && ! empty($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateStart"])) {
                            $AvailDateFrom = date("Y-m-d", strtotime($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateStart"]));
                        } else {
                            $AvailDateFrom = "-";
                        }
                        if (isset($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateEnd"]) && ! empty($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateEnd"])) {
                            $AvailDateTo = date("Y-m-d", strtotime($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateEnd"]));
                        } else {
                            $AvailDateTo = "-";
                        }
                        $x .= '<td align="center">' . $AvailDateFrom . '</td>' . "\n";
                        $x .= '<td align="center" nowrap>' . $AvailDateTo . '</td>' . "\n";
                    }
                    
                    $thisGroupDateID = $ActivityID;
                    $thisActivityDateStart = $ClubAttendanceInfoArr['MeetingDateArr'][$thisGroupDateID]['ActivityDateStart'];
                    $thisAttendanceStatus = $MemberAttendanceInfoArr[$thisStudentID][$EnrolGroupID][$thisGroupDateID]['RecordStatus'];
                    
                    if ($libenroll->enableUserJoinDateRange()) {
                        $isInAvailRange = false;
                        $checkDate = strtotime(date("Y-m-d", strtotime($thisActivityDateStart)));
                        $checkDateStart = strtotime($AvailDateFrom);
                        $checkDateEnd = strtotime($AvailDateTo);
                        if (($checkDate >= $checkDateStart || $AvailDateFrom == "-") && ($checkDate <= $checkDateEnd || $AvailDateTo == "-")) {
                            $isInAvailRange = true;
                        }
                    }
                    
                    // Default Attendance Customization
                    if ($libenroll->enableClubDefaultAttendaceSelectionControl()) {
                        if ($thisAttendanceStatus == '0' || $thisAttendanceStatus == '') {
                            $thisAttendanceStatus = $libenroll->defaultAttendanceStatus;
                        }
                    }
                    
                    if ($libenroll->Is_Future_Date($thisActivityDateStart) == true) {
                        $thisDisplay = $Lang['General']['EmptySymbol'];
                        $x .= '<td align="center" class="tabletext">' . $thisDisplay . '</td>' . "\n";
                    } else {
                        if ($PageAction == 'view')
                            $thisDisplay = $this->Get_Attendance_Icon($thisAttendanceStatus);
                            else {
                                // $thisID = $thisGroupDateID."[".$thisStudentID."]";
                                $thisID = "attendanceArr[" . $thisGroupDateID . "]" . "[" . $thisStudentID . "]";
                                // $thisName = $thisGroupDateID."[".$thisStudentID."]";
                                $thisName = "attendanceArr[" . $thisGroupDateID . "]" . "[" . $thisStudentID . "]";
                                $thisClass = "Col0";
                                $thisDisabled = ($disabledArr[$thisGroupDateID] == "DISABLED") ? true : false;
                                $thisDisplay = $this->Get_Student_Attendance_Selection($thisID, $thisName, $thisAttendanceStatus, $thisClass, $thisDisabled);
                            }
                            $isDisabled = false;
                            if ($libenroll->enableUserJoinDateRange()) {
                                if ($isInAvailRange) {
                                    $x .= '<td align="center" class="tabletext">' . $thisDisplay . '</td>' . "\n";
                                } else {
                                    $isDisabled = true;
                                    // $thisDisplay = $this->Get_Student_Attendance_Selection($thisID, $thisName, $thisAttendanceStatus, $thisClass, $thisDisabled);
                                    switch ($thisAttendanceStatus) {
                                        case ENROL_ATTENDANCE_PRESENT:
                                            $thisDisplay = $Lang['eEnrolment']['Attendance']['Present'];
                                            break;
                                        case ENROL_ATTENDANCE_EXEMPT:
                                            $thisDisplay = $Lang['eEnrolment']['Attendance']['Exempt'];
                                            break;
                                        case ENROL_ATTENDANCE_ABSENT:
                                            $thisDisplay = $Lang['eEnrolment']['Attendance']['Absent'];
                                            break;
                                        case ENROL_ATTENDANCE_LATE:
                                            $thisDisplay = $Lang['eEnrolment']['Attendance']['Late'];
                                            break;
                                        case ENROL_ATTENDANCE_EARLY_LEAVE:
                                            $thisDisplay = $Lang['eEnrolment']['Attendance']['EarlyLeave'];
                                            break;
                                        default:
                                            $thisDisplay = "-";
                                            break;
                                    }
                                    if ($thisDisplay != "-") {
                                        $x .= '<td align="center" class="tabletext" style="background-color:#dfdfdf;"><span class="tabletextrequire">x</span> (' . $thisDisplay . ')</td>' . "\n";
                                    } else {
                                        $x .= '<td align="center" class="tabletext" style="background-color:#dfdfdf;">&nbsp;</td>' . "\n";
                                    }
                                }
                            } else {
                                $x .= '<td align="center" class="tabletext">' . $thisDisplay . '</td>' . "\n";
                            }
                    }
                    if ($libenroll->enableAttendanceScheduleDetail()) {
                        $time_attend = ! empty($AttendanceLogTimeArr[$thisGroupDateID][$thisStudentID]["time_attend"]) ? $AttendanceLogTimeArr[$thisGroupDateID][$thisStudentID]["time_attend"] : "-";
                        $time_leave = ! empty($AttendanceLogTimeArr[$thisGroupDateID][$thisStudentID]["time_leave"]) ? $AttendanceLogTimeArr[$thisGroupDateID][$thisStudentID]["time_leave"] : "-";
                        
                        //P141793
                        $ignoreTapCardSecond = 300;     // in seconds
                        $withinIgnoreRange = false;
                        if (!empty($AttendanceLogTimeArr[$thisGroupDateID][$thisStudentID]["time_attend"]) && !empty($AttendanceLogTimeArr[$thisGroupDateID][$thisStudentID]["time_leave"])) {
                            $timeFirst  = strtotime($AttendanceLogTimeArr[$thisGroupDateID][$thisStudentID]["time_attend"]);
                            $timeSecond = strtotime($AttendanceLogTimeArr[$thisGroupDateID][$thisStudentID]["time_leave"]);
                            $differenceInSeconds = $timeSecond - $timeFirst;
                            if ($differenceInSeconds <= $ignoreTapCardSecond) {
                                $withinIgnoreRange = true;
                            }
                        }
                        
                        if ($time_attend == $time_leave || $withinIgnoreRange) {
                            $time_leave = "-";
                        }
                        if ($isDisabled) {
                            $x .= '<td align="center" class="tabletext" style="background-color:#dfdfdf;">&nbsp;</td>' . "\n";
                            $x .= '<td align="center" class="tabletext" style="background-color:#dfdfdf;">&nbsp;</td>' . "\n";
                        } else {
                            $x .= '<td align="center" class="tabletext">' . $time_attend . '</td>' . "\n";
                            $x .= '<td align="center" class="tabletext">' . $time_leave . '</td>' . "\n";
                        }
                    }
                    $x .= '</tr>' . "\n";
            }
            $x .= '</tbody>' . "\n";
            $x .= '</table>' . "\n";
            
            // ## Footer Remarks
            if ($PageAction == 'view') {
                // $x .= '<div>'."\n";
                // $x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_PRESENT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Present'].'</span>'."\n";
                // $x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
                // $x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_ABSENT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Absent'].'</span>'."\n";
                // $x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
                // $x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_EXEMPT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Exempt'].'</span>'."\n";
                // if ($this->enableAttendanceLateStatusRight()) {
                // $x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
                // $x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_LATE).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Late'].'</span>'."\n";
                // }
                // $x .= '</div>'."\n";
                $x .= $this->Get_Attendance_Icon_Remarks();
            } else {
                // Last Modified Remarks
                $LastModifiedInfoArr = $libenroll->Get_Attendance_Last_Modified_Info($EnrolGroupID, 'Club');
                $LastModifiedDate = $LastModifiedInfoArr[0]['LastDateModified'];
                $LastModifiedUserID = $LastModifiedInfoArr[0]['LastModifiedBy'];
                if (count($LastModifiedInfoArr) > 0)
                    $x .= '<div class="tabletextremark">' . Get_Last_Modified_Remark($LastModifiedDate, '', $LastModifiedUserID) . '</div>';
                    $x .= '<br style="clear:both;" />' . "\n";
                    
                    // Attendance Overall Remarks
                    $x .= '<div class="tabletextremark"><span class="tabletextrequire">*</span> ' . $eEnrollment['act_most_update_avg_attendance'] . '</span></div>' . "\n";
            }
            
            // Deleted Student Remarks
            $x .= $this->Get_Student_Role_And_Status_Remarks(array(
                'InactiveStudent'
            ));
        }
        
        // ## Buttons
        $x .= '<div class="edit_bottom_v30">';
        
        if ($PageAction == 'view') {
            $x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "self.location='" . $PATH_WRT_ROOT . "home/eService/enrollment/'");
        } else {
            $thisDisabled = ($numOfMember == 0) ? true : false;
            if ($libenroll->AllowToEditPreviousYearData) {
                $x .= $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button", ($libenroll->disableUpdate == 1) ? "javascript:alert('" . $Lang['eEnrolment']['disableUpdateAlertMsg'] . "')" : "javascript:this.form.submit();", '', '', $thisDisabled);
                $x .= '&nbsp;' . "\n";
                $x .= $this->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset", '', '', '', $thisDisabled);
                $x .= '&nbsp;' . "\n";
            }
            $x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "javascript:Reload_Club_Attendance_Info_Table();");
        }
        $x .= '</div>';
        
        $x .= '</div>';
        
        // ## Hidden Fields for updating Attendance
        $x .= '<input type="hidden" name="StaffRole" value="' . $StaffRole . '" />' . "\n";
        $x .= '<input type="hidden" name="disabledArr" value="' . rawurlencode(serialize($disabledArr)) . '" />' . "\n";
        
        return $x;
    }
    
    function Get_Management_Activity_Attendance_SC_Info_Table($EnrolEventID, $ActivityID, $CanEdit, $PageAction, $ActivityAttendanceInfoArr = array(), $DisplayPhoto = 0, $AcademicYearID = "")
    {
        global $eEnrollment, $Lang, $image_path, $LAYOUT_SKIN, $libenroll, $PATH_WRT_ROOT, $DisplayAllPhoto, $HiddenAllPhoto, $sys_custom;
        
        // debug_pr($AcademicYearID);
        // debug_pr($EnrolEventID);
        
        include_once ($PATH_WRT_ROOT . 'includes/libuser.php');
        
        $isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
        $isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
        $isActivityPIC = $libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Activity");
        $isAcitivtyHelper = $libenroll->IS_ENROL_HELPER($_SESSION['UserID'], $EnrolEventID, "Activity");
        if ($AcademicYearID == '') {
            $activityInfoAssoAry = $libenroll->Get_Activity_Info_By_Id($EnrolEventID);
            $AcademicYearID = $activityInfoAssoAry[$EnrolEventID]['AcademicYearID'];
        }
        
        if (empty($ActivityAttendanceInfoArr)) {
            // ## Get Activity Attendance Records
            // echo $AcademicYearID.';';
            $ActivityAttendanceInfoArr = $libenroll->Get_Activity_Attendance_Info($EnrolEventID, "", $AcademicYearID);
            // debug_pr($ActivityAttendanceInfoArr);
            $ActivityAttendanceInfoArr = $ActivityAttendanceInfoArr[$EnrolEventID];
        }
        
        // ## Get Activity Meeting Dates
        $EventDateIDArr = array_keys((array) $ActivityAttendanceInfoArr['MeetingDateArr']);
        $numOfMeetingDate = count($EventDateIDArr);
        
        // ## Special checking for HELPER - disable the attendance updating if the
        $disabledArr = array();
        if ($isAcitivtyHelper) {
            $StaffRole = "H";
            
            // Control Attendance Helper's Right
            if ($libenroll->Event_DisableHelperModificationRightLimitation != true) {
                // check for each Date
                for ($i = 0; $i < $numOfMeetingDate; $i ++) {
                    $thisEventDateID = $EventDateIDArr[$i];
                    
                    // check if there are any attendance records in DB
                    $sql = "SELECT DateModified, LastModifiedRole FROM INTRANET_ENROL_EVENT_ATTENDANCE WHERE EnrolEventID = '$EnrolEventID' AND EventDateID = '$thisEventDateID'";
                    $result = $libenroll->returnArray($sql, 2);
                    
                    // no attendance record yet => enable checkboxes for edit
                    // otherwise, do checking
                    if (count($result) > 0) {
                        if ($result[0]['LastModifiedRole'] == "A") {
                            $disabledArr[$thisEventDateID] = "DISABLED";
                        } else {
                            // disable checkboxes if last modified date is not today
                            if (date("Y-m-d") != date("Y-m-d", strtotime($result[0]['DateModified']))) {
                                $disabledArr[$thisEventDateID] = "DISABLED";
                            }
                        }
                    }
                }
            }
        } else {
            $StaffRole = "A";
        }
        
        // ## Display the Student / Children only if it is in view mode
        $DisplayStudentIDArr = array();
        if ($PageAction == "view") {
            if ($_SESSION['UserType'] == USERTYPE_STUDENT) {
                $DisplayStudentIDArr[] = $_SESSION['UserID'];
            } else if ($_SESSION['UserType'] == USERTYPE_PARENT) {
                $libuser = new libuser($_SESSION['UserID']);
                $ChildArr = $libuser->getChildrenList();
                $DisplayStudentIDArr = Get_Array_By_Key($ChildArr, 'StudentID');
            }
        }
        
        // ## Get Club Members
        $ActivityMemberInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($EnrolEventID, $ActivityKeyword = '', $AcademicYearID, $WithNameIndicator = 1, $IndicatorWithStyle = 1, $WithEmptySymbol = 1, $ActiveMemberOnly = 0, $FormIDArr = '');
        $ActivityMemberInfoArr = (array) $ActivityMemberInfoArr[$EnrolEventID]['StatusStudentArr'][2];
        
        $StudentIDArr = Get_Array_By_Key($ActivityMemberInfoArr, 'StudentID');
        if ($libenroll->enableUserJoinDateRange()) {
            $enrolAvailDateArr = $libenroll->Get_Enrol_AvailDate($EnrolEventID, $StudentIDArr, "Activity");
        }
        $numOfMember = count($StudentIDArr);
        
        $x = '
			<div id="div_Activity_AttendanceTable">
			<script language="javascript">
			function toggle_photo()
			{
				Reload_Activity_Attendance_Info_Table2("' . $EnrolEventID . '","' . $CanEdit . '","' . $PageAction . '", "' . ($DisplayPhoto == 1 ? "0" : "1") . '");
			}
			</script>
		';
        
        // ## Get Member Attendance Records
        $MemberAttendanceInfoArr = $libenroll->Get_Student_Activity_Attendance_Info($StudentIDArr, array(
            $EnrolEventID
        ));
        
        // ## Navigation
        if ($CanEdit && $PageAction != 'view') {
            $x .= '<br />';
            $x .= $this->GET_NAVIGATION2($eEnrollment['take_attendance']);
        }
        
        // ## Navigation
        if ($numOfMeetingDate > 0) {
            for ($i = 0; $i < $numOfMeetingDate; $i ++) {
                $thisGroupDateID = $EventDateIDArr[$i];
                $thisActivityDateStart = $ActivityAttendanceInfoArr['MeetingDateArr'][$thisGroupDateID]['ActivityDateStart'];
                $thisActivityDateEnd = $ActivityAttendanceInfoArr['MeetingDateArr'][$thisGroupDateID]['ActivityDateEnd'];
                $DateTimeDisplayArr[$i] = array(
                    $thisGroupDateID,
                    date("Y-m-d", strtotime($thisActivityDateStart)) . ' ' . date("H:i", strtotime($thisActivityDateStart)) . '-' . date("H:i", strtotime($thisActivityDateEnd))
                );
            }
            
            $id = "ActivityID";
            $name = "ActivityID";
            $eventSelection = $this->GET_SELECTION_BOX($DateTimeDisplayArr, " id='" . $id . "' name='" . $name . "' onChange='changeActivity(this)'", "", $ActivityID);
        }
        $x .= "<div style='margin:10px;'>" . $eventSelection . "</div>";
        
        if ($numOfMember == 0) {
            $x .= '<br />' . "\n";
            $x .= $Lang['eEnrolment']['Warning']['ActivityHasNoParticipant'];
            $x .= '<br />' . "\n";
            $x .= '<br />' . "\n";
        } else {
            
            $AttendanceLogTimeArr = $this->Get_Attendance_Time_Log_Records($EnrolEventID, $ActivityID, $MemberAttendanceInfoArr, "Activity");
            
            $DisplayPhotoBtn .= "<a href=\"javascript:toggle_photo();\" class=\"tabletool\"><span id=\"DisplayAllPhotoTxt\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo_" . ($DisplayPhoto ? "zoomout" : "zoomin") . ".gif\" border=\"0\" align=\"absmiddle\" alt=\"$DisplayAllPhoto\"></span></a>";
            
            $x .= '<table id="AttendanceTable">' . "\n";
            // ## Table Header
            $x .= '<thead>' . "\n";
            $x .= '<tr class="tablegreentop">' . "\n";
            // Class
            $x .= '<td align="center" class="tableTitle tabletopnolink" width="50">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</td>' . "\n";
            
            // Class No.
            $x .= '<td align="center" class="tableTitle tabletopnolink" width="50">' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</td>' . "\n";
            
            // Student Name
            $x .= '<td class="tableTitle" nowrap style="border-right-width:5px"><span class="tabletopnolink">' . $eEnrollment['student_name'] . '</span> ' . $DisplayPhotoBtn . '</td>' . "\n";
            
            if ($libenroll->enableUserJoinDateRange()) {
                $x .= '<td class="tableTitle" colspan="2" align="center"><span class="tabletopnolink">' . $Lang['eEnrolment']['AvailiableDate'] . '</span></td>' . "\n";
            }
            
            // Attendance Status
            $x .= '<td align="center" class="tableTitle tabletopnolink" width="150">' . $Lang['eEnrolment']['AttendanceStatus'] . '<br />';
            $i = 0;
            $thisSelection = $this->Get_Student_Attendance_Selection("Header_Col" . $i, "Header_Col" . $i, $thisAttendanceStatus = '', "Header_Col" . $i);
            $thisApplyAllIcon = $this->Get_Apply_All_Icon("javascript:CheckAll('" . $i . "');");
            $x .= $thisSelection . "<div style='margin:0px auto; width:20px'>" . $thisApplyAllIcon . "</div>";
            
            $x .= '</td>' . "\n";
            
            if ($libenroll->enableAttendanceScheduleDetail()) {
                // Time of Arrival
                $x .= '<td align="center" class="tableTitle tabletopnolink" width="150">' . $Lang['eEnrolment']['TimeofArrival'] . '</td>' . "\n";
                
                // Time of Departure
                $x .= '<td align="center" class="tableTitle tabletopnolink" width="150">' . $Lang['eEnrolment']['TimeofDeparture'] . '</td>' . "\n";
            }
            
            $x .= '</tr>' . "\n";
            $x .= '</thead>' . "\n";
            
            // ## Table Content
            $libuserObj = new libuser("", "", $StudentIDArr);
            $x .= '<tbody>' . "\n";
            for ($i = 0; $i < $numOfMember; $i ++) {
                $thisStudentID = $ActivityMemberInfoArr[$i]['StudentID'];
                $thisStudentName = $ActivityMemberInfoArr[$i]['StudentName'];
                $thisClassName = $ActivityMemberInfoArr[$i]['ClassName'];
                $thisClassNumber = $ActivityMemberInfoArr[$i]['ClassNumber'];
                $thisWebSAMSRegNo = $ActivityMemberInfoArr[$i]['WebSAMSRegNo'];
                $thisUserId = $ActivityMemberInfoArr[$i]['UserID'];
                
                // ## Show user record only in view mode
                if ($PageAction == "view" && ! in_array($thisStudentID, $DisplayStudentIDArr))
                    continue;
                    
                    // ## Get Student Object
                    $libuserObj->LoadUserData($thisStudentID);
                    
                    // ## Student Name is a Link to update Student Performance & Comment for Enrollment Admin, Master and Club PIC
                    $thisStudentNameDisplay = '';
                    $thisStudentNameDisplay .= ($isEnrolAdmin || $isEnrolMaster || $isActivityPIC) ? '<a class="tablelink" href="event_student_comment.php?AcademicYearID=' . $AcademicYearID . '&StudentID=' . $thisStudentID . '&EnrolEventID=' . $EnrolEventID . '">' : '';
                    $thisStudentNameDisplay .= $thisStudentName;
                    $thisStudentNameDisplay .= ($isEnrolAdmin || $isEnrolMaster || $isActivityPIC) ? '</a>' : '';
                    
                    // ## Load Student Photos
                    list ($thisPhotoFilePath, $thisPhotoURL) = $libuserObj->GET_OFFICIAL_PHOTO($thisWebSAMSRegNo);
                    
                    /*
                     * if (file_exists($thisPhotoFilePath))
                     * {
                     * list($originalWidth, $originalHeight) = getimagesize($thisPhotoFilePath);
                     * $thisImgTag = $libenroll->Get_Offical_Photo_Img($thisPhotoURL, $originalWidth, $originalHeight);
                     * $thisPhotoSpan = "<div id=\"photo_". $thisStudentID ."\" style=\"display:inline\">";
                     * $thisPhotoSpan .= $thisImgTag;
                     * $thisPhotoSpan .= "</div>\n";
                     *
                     * // $thisPhotoIconSpan = "<div id=\"photo_". $thisStudentID ."_icon\" class=\"tabletext\" style=\"display:inline\">";
                     * // $thisPhotoIconSpan .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo.gif\">";
                     * // $thisPhotoIconSpan .= "</div>\n";
                     * //
                     * // $thisPhotoSpan2 = "<div id=\"photo_". $thisStudentID ."_tmp\" style=\"display:none\">";
                     * // $thisPhotoSpan2 .= $thisImgTag;
                     * // $thisPhotoSpan2 .= "</div>\n";
                     * }
                     * else
                     * {
                     * $thisPhotoSpan = "";
                     * $thisPhotoIconSpan="";
                     * // $thisPhotoSpan2 = "";
                     * }
                     */
                     
                     if (file_exists($thisPhotoFilePath)) {
                         if ($DisplayPhoto == 1) {
                             $thisPhotoIconSpan = "";
                             
                             list ($originalWidth, $originalHeight) = getimagesize($thisPhotoFilePath);
                             $thisImgTag = $libenroll->Get_Offical_Photo_Img($thisPhotoURL, $originalWidth, $originalHeight);
                             $thisPhotoSpan = "<div id=\"photo_" . $thisStudentID . "\" style=\"display:inline\">";
                             $thisPhotoSpan .= $thisImgTag;
                             $thisPhotoSpan .= "</div>\n";
                         } else {
                             $thisPhotoIconSpan = "<div id=\"photo_" . $thisStudentID . "_icon\" style=\"display:inline\">";
                             $thisPhotoIconSpan .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo.gif\">";
                             $thisPhotoIconSpan .= "</div>\n";
                             
                             $thisPhotoSpan = "";
                         }
                     } else {
                         $thisPhotoIconSpan = "";
                         $thisPhotoSpan = "";
                     }
                     
                     $thisClass = 'tablegreenrow';
                     $thisClass .= ($i % 2) + 1;
                     $x .= '<tr class="' . $thisClass . '">' . "\n";
                     $x .= '<td align="center" class="tabletext">' . $thisClassName . '</td>' . "\n";
                     $x .= '<td align="center" class="tabletext">' . $thisClassNumber . '</td>' . "\n";
                     $x .= '<td align="left" nowrap style="border-right-width:5px">' . "\n";
                     $x .= '<span class="tabletext">' . $thisPhotoIconSpan . " " . $thisStudentNameDisplay . '</span>' . "\n";
                     $x .= '<br />' . "\n";
                     $x .= $thisPhotoSpan . "\n";
                     $x .= '</td>' . "\n";
                     if ($libenroll->enableUserJoinDateRange()) {
                         if (isset($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateStart"]) && ! empty($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateStart"])) {
                             $AvailDateFrom = date("Y-m-d", strtotime($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateStart"]));
                         } else {
                             $AvailDateFrom = "-";
                         }
                         if (isset($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateEnd"]) && ! empty($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateEnd"])) {
                             $AvailDateTo = date("Y-m-d", strtotime($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateEnd"]));
                         } else {
                             $AvailDateTo = "-";
                         }
                         $x .= '<td align="center">' . $AvailDateFrom . '</td>' . "\n";
                         $x .= '<td align="center" nowrap>' . $AvailDateTo . '</td>' . "\n";
                     }
                     $thisEventDateID = $ActivityID;
                     $thisActivityDateStart = $ActivityAttendanceInfoArr['MeetingDateArr'][$thisEventDateID]['ActivityDateStart'];
                     $thisAttendanceStatus = $MemberAttendanceInfoArr[$thisStudentID][$EnrolEventID][$thisEventDateID]['RecordStatus'];
                     
                     if ($libenroll->enableUserJoinDateRange()) {
                         $isInAvailRange = false;
                         $checkDate = strtotime(date("Y-m-d", strtotime($thisActivityDateStart)));
                         $checkDateStart = strtotime($AvailDateFrom);
                         $checkDateEnd = strtotime($AvailDateTo);
                         if (($checkDate >= $checkDateStart || $AvailDateFrom == "-") && ($checkDate <= $checkDateEnd || $AvailDateTo == "-")) {
                             $isInAvailRange = true;
                         }
                     }
                     
                     // Default Attendance Customization
                     if ($libenroll->enableActivityDefaultAttendaceSelectionControl()) {
                         if ($thisAttendanceStatus == '0' || $thisAttendanceStatus == '') {
                             $thisAttendanceStatus = $libenroll->Event_DefaultAttendanceStatus;
                         }
                     }
                     
                     if ($libenroll->Is_Future_Date($thisActivityDateStart) == true) {
                         $thisDisplay = $Lang['General']['EmptySymbol'];
                         $x .= '<td align="center" class="tabletext">' . $thisDisplay . '</td>' . "\n";
                     } else {
                         if ($PageAction == 'view') {
                             $thisDisplay = $this->Get_Attendance_Icon($thisAttendanceStatus);
                         } else {
                             $thisID = "attendanceArr[" . $thisEventDateID . "]" . "[" . $thisStudentID . "]";
                             // $thisID = $thisEventDateID."[".$thisStudentID."]";
                             $thisName = "attendanceArr[" . $thisEventDateID . "]" . "[" . $thisStudentID . "]";
                             // $thisName = $thisEventDateID."[".$thisStudentID."]";
                             $thisClass = "Col0";
                             $thisDisabled = ($disabledArr[$thisEventDateID] == "DISABLED") ? true : false;
                             $thisDisplay = $this->Get_Student_Attendance_Selection($thisID, $thisName, $thisAttendanceStatus, $thisClass, $thisDisabled);
                         }
                         $isDisabled = false;
                         if ($libenroll->enableUserJoinDateRange()) {
                             if ($isInAvailRange) {
                                 $x .= '<td align="center" class="tabletext">' . $thisDisplay . '</td>' . "\n";
                             } else {
                                 $isDisabled = true;
                                 // $thisDisplay = $this->Get_Student_Attendance_Selection($thisID, $thisName, $thisAttendanceStatus, $thisClass, $thisDisabled);
                                 switch ($thisAttendanceStatus) {
                                     case ENROL_ATTENDANCE_PRESENT:
                                         $thisDisplay = $Lang['eEnrolment']['Attendance']['Present'];
                                         break;
                                     case ENROL_ATTENDANCE_EXEMPT:
                                         $thisDisplay = $Lang['eEnrolment']['Attendance']['Exempt'];
                                         break;
                                     case ENROL_ATTENDANCE_ABSENT:
                                         $thisDisplay = $Lang['eEnrolment']['Attendance']['Absent'];
                                         break;
                                     case ENROL_ATTENDANCE_LATE:
                                         $thisDisplay = $Lang['eEnrolment']['Attendance']['Late'];
                                         break;
                                     case ENROL_ATTENDANCE_EARLY_LEAVE:
                                         $thisDisplay = $Lang['eEnrolment']['Attendance']['EarlyLeave'];
                                         break;
                                     default:
                                         $thisDisplay = "-";
                                         break;
                                 }
                                 if ($thisDisplay != "-" && ! empty($thisDisplay)) {
                                     $x .= '<td align="center" class="tabletext" style="background-color:#dfdfdf;"><span class="tabletextrequire">x</span> (' . $thisDisplay . ')</td>' . "\n";
                                 } else {
                                     $x .= '<td align="center" class="tabletext" style="background-color:#dfdfdf;">&nbsp;</td>' . "\n";
                                 }
                             }
                         } else {
                             $x .= '<td align="center" class="tabletext">' . $thisDisplay . '</td>' . "\n";
                         }
                     }
                     
                     if ($libenroll->enableAttendanceScheduleDetail()) {
                         $time_attend = ! empty($AttendanceLogTimeArr[$thisEventDateID][$thisStudentID]["time_attend"]) ? $AttendanceLogTimeArr[$thisEventDateID][$thisStudentID]["time_attend"] : "-";
                         $time_leave = ! empty($AttendanceLogTimeArr[$thisEventDateID][$thisStudentID]["time_leave"]) ? $AttendanceLogTimeArr[$thisEventDateID][$thisStudentID]["time_leave"] : "-";
                         
                         //P141793
                         $ignoreTapCardSecond = 300;     // in seconds
                         $withinIgnoreRange = false;
                         if (!empty($AttendanceLogTimeArr[$thisEventDateID][$thisStudentID]["time_attend"]) && !empty($AttendanceLogTimeArr[$thisEventDateID][$thisStudentID]["time_leave"])) {
                             $timeFirst  = strtotime($AttendanceLogTimeArr[$thisEventDateID][$thisStudentID]["time_attend"]);
                             $timeSecond = strtotime($AttendanceLogTimeArr[$thisEventDateID][$thisStudentID]["time_leave"]);
                             $differenceInSeconds = $timeSecond - $timeFirst;
                             if ($differenceInSeconds <= $ignoreTapCardSecond) {
                                 $withinIgnoreRange = true;
                             }
                         }
                         
                         if ($time_attend == $time_leave || $withinIgnoreRange) {
                             $time_leave = "-";
                         }
                         if ($isDisabled) {
                             $x .= '<td align="center" class="tabletext" style="background-color:#dfdfdf;">&nbsp;</td>' . "\n";
                             $x .= '<td align="center" class="tabletext" style="background-color:#dfdfdf;">&nbsp;</td>' . "\n";
                         } else {
                             $x .= '<td align="center" class="tabletext">' . $time_attend . '</td>' . "\n";
                             $x .= '<td align="center" class="tabletext">' . $time_leave . '</td>' . "\n";
                         }
                     }
                     
                     $x .= '</tr>' . "\n";
            }
            $x .= '</tbody>' . "\n";
            $x .= '</table>' . "\n";
            
            // ## Footer Remarks
            if ($PageAction == 'view') {
                // $x .= '<div>'."\n";
                // $x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_PRESENT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Present'].'</span>'."\n";
                // $x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
                // $x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_ABSENT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Absent'].'</span>'."\n";
                // $x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
                // $x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_EXEMPT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Exempt'].'</span>'."\n";
                // $x .= '</div>'."\n";
                $x .= $this->Get_Attendance_Icon_Remarks();
            } else {
                // Last Modified Remarks
                // $LastModifiedInfoArr = $libenroll->Get_Attendance_Last_Modified_Info($EnrolEventID, 'Club');
                $LastModifiedInfoArr = $libenroll->Get_Attendance_Last_Modified_Info($EnrolEventID, 'Event');
                $LastModifiedDate = $LastModifiedInfoArr[0]['LastDateModified'];
                $LastModifiedUserID = $LastModifiedInfoArr[0]['LastModifiedBy'];
                if (count($LastModifiedInfoArr) > 0)
                    $x .= '<div class="tabletextremark">' . Get_Last_Modified_Remark($LastModifiedDate, '', $LastModifiedUserID) . '</div>';
                    $x .= '<br style="clear:both;" />' . "\n";
                    
                    // Attendance Overall Remarks
                    $x .= '<div class="tabletextremark"><span class="tabletextrequire">*</span> ' . $eEnrollment['act_most_update_avg_attendance'] . '</span></div>' . "\n";
            }
            
            // Deleted Student Remarks
            $x .= $this->Get_Student_Role_And_Status_Remarks(array(
                'InactiveStudent'
            ));
        }
        
        // ## Buttons
        $x .= '<div class="edit_bottom_v30">';
        if ($PageAction == 'view') {
            $x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "self.location='" . $PATH_WRT_ROOT . "home/eService/enrollment/event_index.php?AcademicYearID=$AcademicYearID'");
        } else {
            $thisDisabled = ($numOfMember == 0) ? true : false;
            if ($libenroll->AllowToEditPreviousYearData) {
                $x .= $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button", ($libenroll->Event_DisableUpdate == 1) ? "javascript:alert('" . $Lang['eEnrolment']['disableUpdateAlertMsg'] . "')" : "javascript:this.form.submit();", '', '', $thisDisabled);
                $x .= '&nbsp;' . "\n";
                $x .= $this->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset", '', '', '', $thisDisabled);
                $x .= '&nbsp;' . "\n";
            }
            $x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "javascript:Reload_Activity_Attendance_Info_Table();");
        }
        $x .= '</div>';
        $x .= '</div>';
        
        // ## Hidden Fields for updating Attendance
        $x .= '<input type="hidden" name="StaffRole" value="' . $StaffRole . '" />' . "\n";
        $x .= '<input type="hidden" name="disabledArr" value="' . rawurlencode(serialize($disabledArr)) . '" />' . "\n";
        // debug_pr($x);
        return $x;
    }
    
    function Get_Attendance_Time_Log_Records($EventID, $eventDateId, $memberArr = array(), $logType = "Club")
    {
        global $libenroll;
        $logRecordArr[$eventDateId] = array();
        if ($logType == "Club") {
            $log_table = "INTRANET_ENROL_GROUP_ATTENDANCE_LOG";
            $log_table_date_id = "GroupDateID";
        } else {
            $log_table = "INTRANET_ENROL_EVENT_ATTENDANCE_LOG";
            $log_table_date_id = "EventDateID";
        }
        if (count($memberArr) > 0) {
            foreach ($memberArr as $studentID => $memberInfo) {
                $strSQL = "SELECT MIN(CardAttendTime) as time_attend, MAX(CardAttendTime) as time_leave FROM " . $log_table;
                $strSQL .= " WHERE " . $log_table_date_id . " = '" . $eventDateId . "' and StudentID='" . $studentID . "'";
                $strSQL .= " GROUP BY " . $log_table_date_id . ", StudentID";
                $temp = $libenroll->returnArray($strSQL, 2);
                if (count($temp) > 0) {
                    $logRecordArr[$eventDateId][$studentID] = array(
                        "time_attend" => $temp[0]["time_attend"],
                        "time_leave" => $temp[0]["time_leave"]
                    );
                } else {
                    $logRecordArr[$eventDateId][$studentID] = array(
                        "time_attend" => "",
                        "time_leave" => ""
                    );
                }
            }
        }
        return $logRecordArr;
    }
}
?>