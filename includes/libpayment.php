<?php
// page used by:  

####################################### Change Log (Start) #######################################
##  Date    : 2020-11-17 (Ray)    - Added getWechatSettlementData,importPrepareWechatTransactionRecordsToTempTable,importProcessWechatTransactionRecords
##  Date    : 2020-11-09 (Ray)    - Added getENoticePaymentStatus
##  Date    : 2020-10-27 (Ray)    - Added alipaycn, modified importProcessAlipayTransactionRecords
##  Date    : 2020-08-11 (Ray)    - Added wechat
##  Date    : 2020-07-15 (Ray)    - Added GET_MODULE_OBJ_ARR DoublePaidPaymentReport
##  Date    : 2020-07-09 (Ray)    - Added GET_MODULE_OBJ_ARR visamaster
##  Date    : 2020-07-02 (Ray)    - Added getPaymentItemStudentRecords multi payment gateway
##  Date    : 2020-06-29 (Ray)    - Added returnPaymentItemMultiMerchantAccountID
##  Date	: 2020-04-17 (Ray)	  - Modified getWebDisplayAmountFormat format <0 display -$
##  Date    : 2020-04-03 (Ray)    - Modified getWebDisplayAmountFormatDB format
##  Date    : 2020-04-03 (Ray)    - Modified Import_Subsidy_Identity_Record_To_Temp allow $EffectiveEndDate empty
##  Date    : 2020-02-13 (Ray)    - Modified GET_MODULE_OBJ_ARR add fps
##	Date	: 2019-11-12 (Carlos) - Modified getSubsidyIdentityStudents($data), added more filter conditions StartDateWithinEffectiveDates and EndDateWithinEffectiveDates.
##	Date	: 2019-10-29 (Carlos) - [!!!Require schema update!!!] Modified importPrepareAlipayTransactionRecordsToTempTable() and importProcessAlipayTransactionRecords(), added data field SettlementTime. 
##  Date	: 2019-10-23 (Carlos) - Fixed Cancel_Cash_Deposit($TransactionID) wrong balance calculated when cancel several deposit records for the same user all at once.
##  Date	: 2019-10-23 (Carlos) - Log more url paths /api/ and /home/eService/notice/ to class destructor __destruct().
##  Date	: 2019-10-18 (Ray)	  - Added returnPaymentCatsByCatCode,checkPaymentCatCodeValid
##  Date	: 2019-09-27 (Ray)    - Modified returnPaymentItemInfo, added ItemCode
##  Date    : 2019-09-24 (Ray)    - Modified parsePPSLogFile(), added PPSData
##	Date	: 2019-09-09 (Carlos) - Modified getPaymentServiceProviderMapping() to also check top-up flags to enable the service option. 
##								  - Modified GET_MODULE_OBJ_ARR() to also show Merchant Account settings for top-up mode.
##								  - Modified importPrepareAlipayTransactionRecordsToTempTable(), added supplementary step to map PaymentID to PAYMENT_TNG_TRANSACTION.
##								  - Modified importProcessAlipayTransactionRecords(), cater top-up PaymentStatus and PaymentID. 
##  Date	: 2019-09-03 (Ray)    - Added StudentSubsidy_Report at GET_MODULE_OBJ_ARR
##  Date    : 2019-08-27 (Ray)    - Modified Import_Subsidy_Identity_Record_To_Temp, Finalize_Import_Subsidy_Identity add EffectiveStartDate, EffectiveEndDate
##	Date	: 2019-08-23 (Carlos) - Modified getSubsidyIdentityStudents($data) and addSubsidyIdentityStudents($data), added EffectiveStartDate and EffectiveEndDate.
##  Date    : 2019-08-23 (Ray)    - Added tapandgo record page
##  Date	: 2019-08-20 (Carlos) - Modified useEWalletMerchantAccount(), added TNG to support multiple merchant accounts.
##	Date	: 2019-08-12 (Carlos) - Modified getAlipaySettlementData($TargetDate,$MerchantAccount), added param $MerchantAccount to separate handling fee for different account.
##									Added upsertOverallTransactionLog($map) and getOverallTransactionLogs($data) for general use. 
##	Date	: 2019-08-07 (Carlos) - Modified addValue() to add more top-up types, FPS and Tap & Go.
##  Date    : 2019-08-07 (Ray) - Added AlipayHK Handling Fee
##  Date	: 2019-08-02 (Carlos) - Added web service method getAlipaySettlementData($TargetDate) to get AlipayHK daily settlement data from payment gateway server.
##	Date	: 2019-07-31 (Carlos) - Modified GET_MODULE_OBJ_ARR(), add menu [Income Expenses Report].
##  Date    : 2019-06-24 (Ray) - Add Get_Subsidy_Identity_Format_Array, Import_Subsidy_Identity_Record_To_Temp, Finalize_Import_Subsidy_Identity
##  Date    : 2019-06-20 (Ray) - Add TransferMoneyForStudents
##	Date	: 2019-04-08 (Carlos) - Added student subsidy identity.
##	Date	: 2019-03-06 (Carlos) - Consolidate import Alipay settlement records code to importPrepareAlipayTransactionRecordsToTempTable() and importProcessAlipayTransactionRecords().
##	Date	: 2019-01-25 (Carlos) - Copy addValue() from EJ for E-wallet.
##  Date    : 2019-01-17 (Isaac)  - unset $MenuArr["StatisticsReport"] and $MenuArr["BasicSettings"] for menu under App in PowerClass 
##
##	Date	: 2018-08-21 (Carlos) - Modified Create_Notice_Payment_Item() and Edit_Notice_Payment_Item() to support dynamic multiple field values.
##	Date	: 2018-07-17 (Carlos) - Added getMerchantAccounts($data), upsertMerchantAccount($map), deleteMerchantAccount($accountId), getPaymentItemStudentRecords($data), getPaymentServiceProviderMapping().
##									Modified returnPaymentItemInfo($ItemID) added MerchantAccountID for Alipay.
##	Date	: 2018-03-19 (Isaac) - Added Items to menus in PowerClass
##
##	Date	: 2018-03-19 (Carlos) - Modified isPPSLogFileUpdatedBefore($file_type=0,$file_hash='') check file hash value if provided. 
##	Date	: 2017-12-21 (Carlos) - Added getTngQrcode($requestData) for payment notice to get TNG qrcode to pay.
##									Modified Process_Notice_Payment_Item() added optional parameter $AllowNegativeBalance and $DoNotDeductBalance for TNG payment.
## 	Date	: 2017-11-28 (Carlos) - Modified Paid_Payment_Item() and UnPaid_Payment_Item(), added optional parameter to keep balance unchanged for TNG payments.
##	Date	: 2017-09-26 (Carlos) - Added getTngMerchantInfo().
##	Date	: 2017-06-29 (Carlos) - Added log() and __destruct() to log down update / remove / delete actions.
##	Date	: 2017-06-21 (Anna)  - added Push Message Template Settings
##  Date	: 2017-03-22 (Carlos) - modified returnSinglePurchaseStatsByUserType(), deduct voided transaction amounts.
## 	Date	: 2017-01-18 (Carlos) - $sys_custom['ePayment']['TNG'] modified GET_MODULE_OBJ_ARR() added [Import TNG Transaction Records] and [TNG Payment Records].
##  Date	: 2016-08-29 (Carlos) - $sys_custom['ePayment']['CashDepositApproval'] - added isCashDepositNeedApproval() and isCashDepositApprovalAdmin().
##									$sys_custom['ePayment']['CashDepositMethod'] - added getCreditMethodArray(). 
## 	Date	: 2015-12-02 (Carlos) - $sys_custom['ePayment']['ElectronicBilling'] modified getNegativeBalanceRecordsByDateRange(), getNextElectronicBillingNumber(), insertElectronicBillingRecord().
##	Date	: 2015-11-23 (Carlos) - $sys_custom['ePayment']['PaymentItemWithPOS'] modified GET_MODULE_OBJ_ARR(), added menu item [Payment Receipt List].
##									added insertReceiptRecord(), updateReceiptRecord(), getReceiptRecords(), deleteReceiptRecords().
##	Date	: 2015-11-06 (Carlos) - $sys_custom['ePayment']['ElectronicBilling'] modified GET_MODULE_OBJ_ARR(), calculateEPSBarcodeCheckDigit(), getEPSBarcodeNumber(), getEPSBarcodeUrl(), 
##									getEPSBarcodeImage(), getNegativeBalanceRecordsByDateRange(), getElectronicBillingRecords(), getElectronicBillingRecordsByRecordId(), getFormattedElectronicBillingNumber(),
##									insertElectronicBillingRecord(), updateElectronicBillingRecord(), deleteElectronicBillingRecord().
##  Date	: 2015-10-22 (Carlos) - modified Set_Report_Date_Range_Cookies(), set ePOS cookies altogether.
##	Date	: 2015-10-13 (Carlos) - modified Paid_Payment_Item(), added optional parameter $AllowNegativeBalance (default false) to let student without not enough balance to pay.
## 	Date	: 2015-08-06 (Carlos) - added Set_Report_Date_Range_Cookies() and Get_Report_Date_Range_Cookies().
##	Date	: 2015-04-23 (Omas) - add iAccountAccountOverview() to generate Classteacher iAccount > AccountOverview
##	Date	: 2015-01-13 (Carlos) - modified Paid_Payment_Item(), update PAYMENT_PAYMENT_ITEMSTUDENT.ProcessingAdminUser to UserID
##  Date	: 2015-1-7 (Roy)
##				modified GET_MODULE_OBJ_ARR(), add $plugin['eClassApp'] condition to control notification menu
##
##	Date	: 2014-08-19 (Carlos)
##				$sys_custom['ePayment']['PaymentItemChangeLog'] - Added getPaymentItemChangeLogTypeArray(), getPaymentItemChangeLogType(), logPaymentItemChange(), deletePaymentItemChangeLog()
##																- Modified Paid_Payment_Item(), UnPaid_Payment_Item()
##
##	Date	: 2014-04-10 (Carlos)
##				Modified update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG(), added UsedQuota and TotalQuota to PAYMENT_PRINTING_QUOTA_CHANGE_LOG 
##
## 	Date	: 2014-03-28 (Carlos)
##				Modified update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG(), added PAYMENT_PRINTING_QUOTA_CHANGE_LOG.ModifiedBy
##
##	Date	: 2014-03-24 (Carlos)
##				Added getPaymentMethodSelection()
##
##	Date	: 2014-02-12 (Cameron) 
##				Change this line if($balance >= $amount) in Process_Printer_Transactions()
##
##	Date	: 20140204 (Carlos)
##				Added getUnsettledPaymentItemsByStudents()
##
##	Date	: 20131115 (Carlos)
##				Added Process_Printer_Transactions() for Ricoh printer payment
##
##	Date	: 20131101 (Carlos)
##				Modified GET_MODULE_OBJ_ARR(), add menu [eLibrary plus Overdue Payment]
##
##	Date	: 2013-10-28 (Carlos)
##				Modified GET_MODULE_OBJ_ARR(), hide KIS not required functions
##
##	Date	: 20130927 (YatWoon)
##				Modified account_transfer(), makeRefund(), Cancel_Cash_Deposit(), Make_Donation(), Paid_Payment_Item(), UnPaid_Payment_Item(), 
##				add "InputBy" field to store who process the record [Case#2013-0927-0948-06066]
##
##	Date	: 20130917 (Henry)
##				Modifed getReceiptStudentSelectByClass to support multi selected index
##
##	Date	: 20130917 (Henry)
##				added new page nav "$PageStatisticsReport_DailyPaymentReport"
##
##	Date	: 20130830 (Henry)
##				modified the sql in the record_type = 2 and added a parameter "type" of function update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG()
##
##	Date	: 20130823 (Siuwan)
##				- hide some fields in GET_MODULE_OBJ_ARR() if platform = KIS
##
##	Date	: 20120720 (Carlos)
##				- added Balance Report to GET_MODULE_OBJ_ARR()
##
##	Date	: 20111117 (Henry Chow)
##				- update Process_Notice_Payment_Item() & Paid_Payment_Item
##				store PaymentID to "PAYMENT_OVERALL_TRANSACTION_LOG > RelatedTransactionID" (original store ItemID)
##
##	Date	: 20111109 (YatWoon)
##				- update makeRefund(), Cancel_Cash_Deposit(), Make_Donation(), Paid_Payment_Item(), UnPaid_Payment_Item()
##				still using $PHP_AUTH_USER !!!
##				- update makeRefund(), Make_Donation(), auto generate RefCode for refund record [Case#2011-0928-1640-14073]
## 
##	Date	: 20110915 (YatWoon)
##				add getReceiptStudentSelectByClass(), for Print Page > Receipt
##
##	Date	: 20110613 (YatWoon)
##				update parsePPSLogFile(), Improved: auto detect the file type is PPS or Counter Bill 
##
##	Date	: 20110524 (Carlos)
##	Details : Change word "SMS Notification" to "Notification" at GET_MODULE_OBJ_ARR() 
##
##	Date 	: 20100802 (Henry Chow)
##	Details : Modified function checkBalance(), add parameter to check whether need formating on balance
##
##	Date 	: 20100709 (Henry Chow)
##	Details : Modified function checkBalance(), add FORMAT when retrieving Balance
##
##	Date 	: 20100224 (Ronald)
##	Details : Modified function Paid_Payment_Item()
##				changed "if ($Balance > $Amount)" to "if ($Balance >= $Amount)". 
##				so now can process the payment if the account balance === payment item amount.
##
####################################### Change Log (End) #######################################

/*
 * PAYMENT_OVERALL_TRANSACTION_LOG.TransactionType
 * # 1 - Add Value ( Cash Deposit, PPS, Add Value Machine, eWallet top-up e.g. TNG, AlipayHK, etc... )
 * # 2 - Payment Items
 * # 3 - Single Purchase
 * # 4 - Transfer TO
 * # 5 - Transfer FROM
 * # 6 - Cancel Payment
 * # 7 - Refund
 * # 8 - PPS Charges
 * # 9 - Cancel Cash Deposit
 * # 10 - Donation to school
 * # 11 - POS void transaction (refund)
 * # 12 - eWallet handing fee
 */

if (!defined("LIBPAYMENT_DEFINED"))                     // Preprocessor directive
{
define("LIBPAYMENT_DEFINED", true);
define("LIBPAYMENT_AMOUNT_DECIMAIL",2);

class libpayment extends libdb {
  var $pps_file_date;
  var $CreditTransactionType = array(1,5,6,11);
  var $DebitTransactionType = array(2,3,4,7,8,9,10);
  var $IEPSCharge;
  var $CBCharge;
  var $PPSCharge;
  var $pps_file_type;	# 0 = IEPS File (By Phone or Internet) 1 = Counter Bill File (e.g. by Circle K) 
  var $Settings;
  
  function libpayment ()
  {
		$this->libdb();
		$this->pps_file_date = "";
		
		$this->Settings = $this->Retrieve_Setting();
  }

	function __destruct()
	{
		global $___EPAYMENT_LOGGED___; // prevent same page being logged more than one time
		// log down when and who do update or remove action under ePayment module path
		if( !$___EPAYMENT_LOGGED___  && (strpos($_SERVER['REQUEST_URI'],'/home/eAdmin/GeneralMgmt/payment/')!==false || strpos($_SERVER['REQUEST_URI'],'/home/eService/notice/')!==false || strpos($_SERVER['REQUEST_URI'],'/api/')!==false) 
			&& (strpos($_SERVER['REQUEST_URI'],'update')!==false || strpos($_SERVER['REQUEST_URI'],'remove')!==false || strpos($_SERVER['REQUEST_URI'],'delete')!==false || strpos($_SERVER['REQUEST_URI'],'send')!==false) )
		{
			$___EPAYMENT_LOGGED___ = 1;
			$log = date("Y-m-d H:i:s").' '.OsCommandSafe($_SERVER['REQUEST_URI']).' '.(isset($_SESSION['UserID'])?$_SESSION['UserID']:0).' '.(count($_POST)>0?base64_encode(serialize($_POST)):'');
			$this->log($log);
		}
	}

	function log($log_row)
	{
		global $intranet_root, $file_path, $sys_custom;
		
		$num_line_to_keep = isset($sys_custom['ePaymentLogMaxLine'])? $sys_custom['ePaymentLogMaxLine'] : 400000;
		$log_file_path = $file_path."/file/epayment_log";
		$log_file_path_tmp = $file_path."/file/epayment_log_tmp";
		// assume $log_row does not contains single quote that would break the shell command statement
		shell_exec("echo '$log_row' >> $log_file_path"); // append to log file
		$line_count = intval(shell_exec("cat '$log_file_path' | wc -l"));
		if($line_count > $num_line_to_keep){
			shell_exec("tail -n ".($num_line_to_keep/2)." '$log_file_path' > '$log_file_path_tmp'"); // keep the last $num_line_to_keep/2 lines
			shell_exec("rm '$log_file_path'");
			shell_exec("mv '$log_file_path_tmp' '$log_file_path'");
		}
	}

	function Retrieve_Setting() {
		include_once("libgeneralsettings.php");
		$GeneralSetting = new libgeneralsettings();
		
		$SettingList = array();
		$Settings = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);
		$Settings['PPSCounterBillCharge'] = ($Settings['PPSCounterBillCharge'] == "")? 3.2:$Settings['PPSCounterBillCharge'];
		$Settings['PPSIEPSCharge'] = ($Settings['PPSIEPSCharge'] == "")? 2.1:$Settings['PPSIEPSCharge'];
		
		return $Settings;
	}
	
	// Currently only TNG no need to set merchant account
	function useEWalletMerchantAccount()
	{
		global $sys_custom;
		 
		return $sys_custom['ePayment']['Alipay'] || $sys_custom['ePayment']['FPS'] || $sys_custom['ePayment']['TAPANDGO'] || $sys_custom['ePayment']['TNG'] || $sys_custom['ePayment']['VisaMaster'] || $sys_custom['ePayment']['WeChat'] || $sys_custom['ePayment']['AlipayCN'];
	}
	
	function isEWalletDirectPayEnabled()
	{
		global $sys_custom;
		
		return $sys_custom['ePayment']['TNG'] || $sys_custom['ePayment']['Alipay'] || $sys_custom['ePayment']['FPS'] || $sys_custom['ePayment']['TAPANDGO'] || $sys_custom['ePayment']['VisaMaster'] || $sys_custom['ePayment']['WeChat'] || $sys_custom['ePayment']['AlipayCN'];
	}
	
	function isEWalletTopUpEnabled()
	{
		global $sys_custom;
		
		return $sys_custom['ePayment']['TopUpEWallet']['TNG'] || $sys_custom['ePayment']['TopUpEWallet']['Alipay'] || $sys_custom['ePayment']['TopUpEWallet']['FPS'] || $sys_custom['ePayment']['TopUpEWallet']['TAPANDGO'] || $sys_custom['ePayment']['TopUpEWallet']['VisaMaster'] || $sys_custom['ePayment']['TopUpEWallet']['WeChat'] || $sys_custom['ePayment']['TopUpEWallet']['AlipayCN'];
	}
	
	function isTNGDirectPayEnabled()
	{
		global $sys_custom;
		
		return $sys_custom['ePayment']['TNG'];
	}
	
	function isTNGTopUpEnabled()
	{
		global $sys_custom;
		
		return $sys_custom['ePayment']['TopUpEWallet']['TNG'];
	}
	
	function isAlipayDirectPayEnabled()
	{
		global $sys_custom;
		
		return $sys_custom['ePayment']['Alipay'];
	}
	
	function isAlipayTopUpEnabled()
	{
		global $sys_custom;
		
		return $sys_custom['ePayment']['TopUpEWallet']['Alipay'];
	}
	
	function isFPSDirectPayEnabled()
	{
		global $sys_custom;
		
		return $sys_custom['ePayment']['FPS'];
	}
	
	function isVisaMasterTopUpEnabled()
	{
		global $sys_custom;
		
		return $sys_custom['ePayment']['TopUpEWallet']['VisaMaster'];
	}

	function isVisaMasterDirectPayEnabled()
	{
		global $sys_custom;

		return $sys_custom['ePayment']['VisaMaster'];
	}

	function isWeChatTopUpEnabled()
	{
		global $sys_custom;

		return $sys_custom['ePayment']['TopUpEWallet']['WeChat'];
	}

	function isWeChatDirectPayEnabled()
	{
		global $sys_custom;

		return $sys_custom['ePayment']['WeChat'];
	}

	function isAlipayCNTopUpEnabled()
	{
		global $sys_custom;

		return $sys_custom['ePayment']['TopUpEWallet']['AlipayCN'];
	}

	function isAlipayCNDirectPayEnabled()
	{
		global $sys_custom;

		return $sys_custom['ePayment']['AlipayCN'];
	}

	function isFPSTopUpEnabled()
	{
		global $sys_custom;

		return $sys_custom['ePayment']['TopUpEWallet']['FPS'];
	}

	function isTapAndGoDirectPayEnabled()
	{
		global $sys_custom;
		
		return $sys_custom['ePayment']['TAPANDGO'];
	}
	
	function isTapAndGoTopUpEnabled()
	{
		global $sys_custom;
		
		return $sys_custom['ePayment']['TopUpEWallet']['TAPANDGO'];
	}

  function GET_ADMIN_USER()
  {
    global $intranet_root;
		
		include_once($intranet_root."/includes/libfilesystem.php");
    $lf = new libfilesystem();
    $AdminUser = trim($lf->file_read($intranet_root."/file/payment/admin_user.txt"));

    return $AdminUser;
  }

  function IS_ADMIN_USER($ParUserID="")
  {
    global $intranet_root, $plugin, $intranet_version;

		if ($intranet_version == "2.5" || $intranet_version == "3.0") {
			return $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"];
		}
		else {
			if(!$ParUserID) $ParUserID = $_SESSION['UserID'];
	
			$AdminUser = $this->GET_ADMIN_USER();
	    $IsAdmin = 0;
	    if(!empty($AdminUser))
	    {
	      $AdminArray = explode(",", $AdminUser);
	      $IsAdmin = (in_array($ParUserID, $AdminArray)) ? 1 : 0;
	    }
	
	    return $IsAdmin;
		}
  }

  function isPPSLogFileUpdatedBefore($file_type=0,$file_hash='')
  {
		if ($this->pps_file_date != "")
		{
		   $sql = "SELECT COUNT(*) FROM PAYMENT_CREDIT_FILE_LOG WHERE FileDate = '".$this->pps_file_date."' AND FileType = '$file_type' ";
		   if($file_hash != ''){
		   	$sql .= " AND FileHashValue='$file_hash' ";
		   }
		   $temp = $this->returnVector($sql);
		   if ($temp[0]==0) return false;
		   else return true;
		}
		else return false;
  }

  function parsePPSLogFile($content)
  {
		$lines = explode("\n",$content);
		$firstLine = $lines[0];
		$header_prefix = substr($firstLine,0,2);
		if ($header_prefix == "MH")
		{
		   $this->pps_file_date = substr($firstLine, 2, 8);
		   $merchant_number = (substr($firstLine, 10, 5));
		   $this->pps_file_type = $merchant_number == "40000" ? 0 : 1;
		}
		
		for ($i=0; $i<sizeof($lines); $i++)
		{
			$curr = $lines[$i];
			$ppsdata = array_filter(explode(" ", trim($curr)));
			$ppsdata = implode('|', $ppsdata);

			$record_id = substr($curr,0,2);
			if ($record_id != "D1") continue;
			
			$tran_type = substr($curr,14,4);
			if ($tran_type != "PPDR") continue;
			
			$tran_status = substr($curr,18,1);
			if ($tran_status != "A") continue;
			
			$tran_hr = substr($curr,10,2);
			$tran_min = substr($curr,12,2);
			$tran_amount_str = substr($curr,19,8);
			$tran_amount = intval($tran_amount_str)/100.0;
			$tran_bill_ac = substr($curr,27,25);
			$tran_date = substr($curr,54,8);
			$tran_date_counter = substr($curr, 79, 8);
			$tran_hr_counter = substr($curr, 87,2);
			$tran_min_counter = substr($curr, 89,2);
			
			global $special_arrangement;
			if ($special_arrangement['chop_pps_to_6_chars'])
			{
				$record['account'] = substr($tran_bill_ac,0,6);
			}
			else
			{
			  $record['account'] = trim($tran_bill_ac);
			}
			$record['amount'] = $tran_amount;
			$record['date'] = $tran_date;
			$record['hour'] = $tran_hr;
			$record['min'] = $tran_min;
			# New for counter bill
			$record['date_counter'] = $tran_date_counter;
			$record['hr_counter'] = $tran_hr_counter;
			$record['min_counter'] = $tran_min_counter;
			$record['ppsdata'] = $ppsdata;

			$result[] = $record;
		}
		return $result;
  }
  function checkPaymentCatCodeValid($CatCode, $ExcludeItemID = '')
  {
  		if(is_array($ExcludeItemID) == false) {
			$ExcludeItemID = array($ExcludeItemID);
		}
  		$rs = $this->returnPaymentCatsByCatCode($CatCode);
  		foreach($rs as $temp) {
			if(in_array($temp['CatID'], $ExcludeItemID)) {
				continue;
			}
			return false;
		}
  		return true;
  }
  function returnPaymentCatsByCatCode($CatCode)
  {
		$sql = "SELECT CatID FROM PAYMENT_PAYMENT_CATEGORY WHERE CatCode = '$CatCode'";
		$temp = $this->returnArray($sql);
		return $temp;
  }
  function returnPaymentCats()
  {
		$sql = "SELECT CatID,Name FROM PAYMENT_PAYMENT_CATEGORY ORDER BY DisplayOrder";
		return $this->returnArray($sql,2);
  }
  function returnPaymentCatName($CatID)
  {
		$sql = "SELECT Name FROM PAYMENT_PAYMENT_CATEGORY WHERE CatID = '$CatID'";
		$temp = $this->returnVector($sql);
		return $temp[0];
  }
  function returnPaymentCatInfo($CatID)
  {
  		global $sys_custom;
  		$more_fields = "";
		if($sys_custom['ePayment_PaymentCategory_Code']) {
			$more_fields .= ",CatCode";
		}
		$sql = "SELECT Name, DisplayOrder, Description $more_fields FROM PAYMENT_PAYMENT_CATEGORY WHERE CatID = '$CatID'";
		$temp = $this->returnArray($sql,3);
		return $temp[0];
  }

  function getPaymentCatNextDisplayOrder()
  {
		$sql = "SELECT MAX(DisplayOrder) FROM PAYMENT_PAYMENT_CATEGORY";
		$temp = $this->returnVector($sql);
		return $temp[0]+1;
  }
  function getPaymentItemNextDisplayOrder($CatID="")
  {
		$conds = ($CatID==""?"":"WHERE CatID = '$CatID'");
		$sql = "SELECT MAX(DisplayOrder) FROM PAYMENT_PAYMENT_ITEM $conds";
		$temp = $this->returnVector($sql);
		return $temp[0]+1;
  }
  function getPaymentItems($CatID)
  {
		if ($CatID == "") return;
		$sql = "SELECT ItemID, Name FROM PAYMENT_PAYMENT_ITEM WHERE CatID = '$CatID' ORDER BY DisplayOrder";
		return $this->returnArray($sql,2);
  }
  function getPaymentItemsByItemCode($ItemCode)
  {
		$sql = "SELECT ItemID, Name FROM PAYMENT_PAYMENT_ITEM WHERE ItemCode = '$ItemCode' ORDER BY DisplayOrder";
    	return $this->returnArray($sql,2);
  }
  function checkPaymentItemItemCodeValid($ItemCode, $ExcludeItemID='')
  {
	  global $PATH_WRT_ROOT;
	  include_once($PATH_WRT_ROOT."includes/libpos.php");
	  include_once($PATH_WRT_ROOT."includes/libpos_item.php");

	  if(is_array($ExcludeItemID) == false) {
		  $ExcludeItemID = array($ExcludeItemID);
	  }
	  $item_code_valid = true;
	  $libItem = new POS_Item();
	  $pos_item_code_valid = $libItem->Is_Barcode_Valid($ItemCode);
	  if($pos_item_code_valid) {
		  $payment_items = $this->getPaymentItemsByItemCode($ItemCode);
		  if(!empty($payment_items)) {
				foreach($payment_items as $item) {
					if(in_array($item['ItemID'], $ExcludeItemID)){
						continue;
					}
					$item_code_valid = false;
					break;
				}
		  }
	  } else {
		  $item_code_valid = false;
	  }
	  return $item_code_valid;
  }
  function getPaymentItemsInDateRange($CatID)
  {
		if ($CatID == "") return;
		$sql = "SELECT ItemID, Name FROM PAYMENT_PAYMENT_ITEM
		              WHERE CatID = '$CatID' AND StartDate <= CURDATE() AND EndDate >= CURDATE()
		              ORDER BY DisplayOrder";
		return $this->returnArray($sql,2);
  }
  function getPaymentItemsToPay($CatID)
  {
		if ($CatID == "") return;
		$sql = "SELECT DISTINCT a.ItemID, b.Name
		              FROM PAYMENT_PAYMENT_ITEMSTUDENT as a
		                   LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON (a.ItemID = b.ItemID AND b.RecordStatus = 0)
		              WHERE a.RecordStatus = 0 AND b.StartDate <= CURDATE() AND b.CatID = '$CatID'";
		return $this->returnArray($sql,2);
  }
  function returnPaymentItemName ($ItemID)
  {
		$sql = "SELECT Name FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$ItemID'";
		$temp = $this->returnVector($sql);
		return $temp[0];
  }
  function returnPaymentItemStatus($ItemID)
  {
		$sql = "SELECT RecordStatus FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$ItemID'";
		$temp = $this->returnVector($sql);
		return $temp[0];
  }
	function returnPaymentNoticeMultiMerchantAccountID($NoticeID)
	{
		global $sys_custom;
		$temp = array();
		if($sys_custom['ePayment']['MultiPaymentGateway']) {
			$sql = "SELECT MerchantAccountID FROM INTRANET_NOTICE_PAYMENT_GATEWAY WHERE NoticeID = '$NoticeID'";
			$temp = $this->returnVector($sql, 1);
		}
		$sql = "SELECT MerchantAccountID FROM INTRANET_NOTICE WHERE NoticeID = '$NoticeID'";
		$notice_record = $this->returnVector($sql, 1);
		if($notice_record[0] != '') {
			$temp[] = $notice_record[0];
		}
		return array_unique($temp);
	}
	function returnPaymentItemMultiMerchantAccountID($ItemID)
	{
		global $sys_custom;
		$temp = array();
		if($sys_custom['ePayment']['MultiPaymentGateway']) {
			$sql = "SELECT MerchantAccountID FROM PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY WHERE ItemID = '$ItemID'";
			$temp = $this->returnVector($sql, 1);
		}
		$sql = "SELECT MerchantAccountID FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$ItemID'";
		$payment_record = $this->returnVector($sql, 1);
		if($payment_record[0] != '') {
			$temp[] = $payment_record[0];
		}
		return array_unique($temp);
	}
  function returnPaymentItemInfo($ItemID)
  {
  		global $sys_custom;
  		$more_fields = "";
	  	if($sys_custom['ePayment_PaymentItem_ItemCode']) {
			$more_fields .= ",ItemCode";
	  	}
  		if($this->useEWalletMerchantAccount()){
  			$more_fields .= ",MerchantAccountID";
  		}
		$sql = "SELECT Name, CatID, DisplayOrder, Description, StartDate, EndDate,PayPriority,RecordStatus,NoticeID $more_fields FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$ItemID'";
		$temp = $this->returnArray($sql,8);
		return $temp[0];
  }
  function returnDefaultPaymentAmount ($ItemID)
  {
		#$sql = "SELECT Amount FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = '$ItemID' AND RecordType = 0 AND RecordStatus = 0 ORDER BY StudentID LIMIT 0,1";
		$sql = "SELECT DefaultAmount FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$ItemID'";
		$temp = $this->returnVector($sql);
		return $temp[0];
  }
  function returnPaymentItemStudentCount($ItemID)
  {
		$sql = "SELECT RecordStatus,COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = '$ItemID' GROUP BY RecordStatus";
		$temp = $this->returnArray($sql,2);
		$result[0] = 0;
		$result[1] = 0;
		for ($i=0; $i<sizeof($temp); $i++)
		{
		    list($status,$count) = $temp[$i];
		    $result[$status] = $count;
		}
		return $result;
  }
  function returnPaymentInfo($PaymentID)
  {
		$sql = "SELECT RecordStatus, Amount,StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID = '$PaymentID'";
		$result = $this->returnArray($sql,3);
		return $result[0];
  }
  function returnPaymentDetailedInfo($PaymentID)
  {
		$namefield = getNameFieldByLang("c.");
		$sql = "SELECT b.Name, a.Amount,$namefield,c.ClassName,c.ClassNumber,
		              DATE_FORMAT(a.PaidTime,'%Y-%m-%d %H:%i:%s'),a.RecordStatus
		       FROM PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
		            LEFT OUTER JOIN INTRANET_USER as c ON a.StudentID = c.UserID AND c.RecordType = 2
		       WHERE a.PaymentID = '$PaymentID'";
		$result = $this->returnArray($sql,7);
		return $result[0];
  }

  function checkBalance($studentid, $needFormat=0)
  {
		//$sql = "SELECT Balance, LastUpdated FROM PAYMENT_ACCOUNT WHERE StudentID = '$studentid'";
		$sql = "SELECT ".(($needFormat) ? "FORMAT(Balance,".LIBPAYMENT_AMOUNT_DECIMAIL.")" : "Balance").", LastUpdated FROM PAYMENT_ACCOUNT WHERE StudentID = '$studentid'";
		$temp = $this->returnArray($sql,2);
		return $temp[0];
  }
  
  function Check_Balance_Enough($StudentID,$AmountToDeduct) {
  	$BalanceInfo = $this->checkBalance($StudentID);
  	return ($BalanceInfo[0] >= $AmountToDeduct);
  }
  
  function account_transfer($from,$to,$amount)
  {
		global $i_Payment_TransactionType_TransferTo,$i_Payment_TransactionType_TransferFrom, $UserID;
		
		if ($from != $to) { // account transfer cannot be the same guy
			$namefield = getNameFieldWithClassNumberByLang();
			
			$sql = "SELECT $namefield FROM INTRANET_USER WHERE UserID = '$from'";
			$temp = $this->returnVector($sql);
			$from_name = $temp[0];
			$sql = "SELECT $namefield FROM INTRANET_USER WHERE UserID = '$to'";
			$temp = $this->returnVector($sql);
			$to_name = $temp[0];
			# LOCK tables
			$sql = "LOCK TABLES PAYMENT_ACCOUNT WRITE, PAYMENT_OVERALL_TRANSACTION_LOG WRITE";
			$this->db_db_query($sql);
			
			# Check target exists
			$sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$to'";
			$temp = $this->returnVector($sql);
			# Check From balance
			$sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$from'";
			$temp2 = $this->returnVector($sql);
			$to_old_balance = $temp[0];
			$from_old_balance = $temp2[0];
			
			
			$from_balance_after = $from_old_balance - $amount;
			if (abs($from_balance_after) < 0.001)    # Smaller than 0.1 cent
			{
			   $from_balance_after=0;
			}
			
			//if ($to_old_balance=="" || $from_old_balance<$amount)
			if ($to_old_balance=="" || $from_balance_after<0)
			{
			   $result = false;
			}
			else
			{
				$from_balance_after = round($from_balance_after,2);
			
				# Update Account balance of $from
				//$sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance - $amount, LastUpdated = now() WHERE StudentID = $from AND Balance >= $amount";
				$sql = "UPDATE PAYMENT_ACCOUNT SET Balance = '$from_balance_after', LastUpdated = now() WHERE StudentID = '$from'";
				$this->db_db_query($sql);
				if ($this->db_affected_rows() != 1)
				{
				   $result = false;
				}
				else
				{
				 	$to_balance_after = round($to_old_balance + $amount,2);
					# Update Account balance of $to
					//$sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount, LastUpdated = now() WHERE StudentID = $to";
					$sql = "UPDATE PAYMENT_ACCOUNT SET Balance = '$to_balance_after', LastUpdated = now() WHERE StudentID = '$to'";
					$this->db_db_query($sql);
					
					# Insert Transaction Records
					//$to_after = $to_old_balance + $amount;
					//$from_after = $from_old_balance - $amount;
					$to_after = $to_balance_after;
					$from_after = $from_balance_after;
					
					$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
					       (StudentID, Amount,TransactionType, BalanceAfter,TransactionTime,RefCode, InputBy)
					       VALUES
					       ('$to','$amount',5,'$to_after',now(),'ECLASS_TRANSFER', '$UserID'),
					       ('$from','$amount',4,'$from_after',now(),'ECLASS_TRANSFER', '$UserID')
					       ";
					$this->db_db_query($sql);
					$sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG
					              SET RefCode = CONCAT('TRA',LogID), Details = '$i_Payment_TransactionType_TransferFrom $from_name'
					              WHERE RefCode = 'ECLASS_TRANSFER' AND TransactionType = 5 AND StudentID = '$to'";
					$this->db_db_query($sql);
					$sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG
					              SET RefCode = CONCAT('TRA',LogID), Details = '$i_Payment_TransactionType_TransferTo $to_name'
					              WHERE RefCode = 'ECLASS_TRANSFER' AND TransactionType = 4 AND StudentID = '$from'";
					$this->db_db_query($sql);
					$result = true;
				}
			
			}
			
			# UNLOCK tables
			$sql = "UNLOCK TABLES";
			$this->db_db_query($sql);
			return $result;
		}
		else 
			return false;
  }
  function makeRefund($StudentID)
  {
		global $i_Payment_action_refund, $UserID;
		
		# 1. Get current balance
		$sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$StudentID'";
		$temp = $this->returnVector($sql);
		$balance = $temp[0];
		if ($balance == "" || $balance == 0)
		{
		  return true;
		}
		else {
			# 2. Insert to transaction log
			$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
			       (StudentID, TransactionType, Amount, BalanceAfter, TransactionTime, Details, InputBy)
			       VALUES
			       ('$StudentID', 7,'$balance','0',NOW(),'$i_Payment_action_refund', '$UserID')";
			$Result['InsertOverallTransactionLog'] = $this->db_db_query($sql);
			
			# 2a. Auto add RefCode for refund
			$this_TransactionID = $this->db_insert_id();
			$sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RefCode = CONCAT('RF', $this_TransactionID) WHERE LogID='$this_TransactionID'";
			$this->db_db_query($sql);
			
			# 3. Set balance to 0
			$sql = "UPDATE PAYMENT_ACCOUNT SET Balance = 0, LastUpdateByAdmin = '$UserID',
			                      LastUpdateByTerminal = NULL, LastUpdated = NOW() WHERE StudentID = '$StudentID'";
			$Result['UpdateAccountBalance'] = $this->db_db_query($sql);
			
			return !in_array(false,$Result);
		}
  }

  # Deprecated : Use function returnSinglePurchaseStatsByUserType
  # Param:
  # - Start Date of the records
  # - End Date of the records
  # Return:
  # - Detail, Sum of Amount, Count of transaction
  function returnSinglePurchaseStats($start, $end)
  {
		$start_ts = strtotime($start);
		$end_ts = strtotime($end) + 60*60*24 - 1;
		$sql = "SELECT Details, SUM(Amount), COUNT(LogID) FROM PAYMENT_OVERALL_TRANSACTION_LOG
		              WHERE TransactionType = 3
		                    AND UNIX_TIMESTAMP(TransactionTime) BETWEEN $start_ts AND $end_ts
		              GROUP BY Details ORDER BY Details";
		return $this->returnArray($sql,3);
  }

  # Param:
  # - Start Date of the records
  # - End Date of the records
  # - UserType : 1 - Teacher, 2 - Student , '' - ALL
  # Return:
  # - Detail, Sum of Amount, Count of transaction
  function returnSinglePurchaseStatsByUserType($start, $end,$user_type="")
  {
		switch($user_type){
		        case 1 : $user_cond = " AND ( b.RecordType=1 OR ( b.RecordType IS NULL AND c.RecordType IS NULL))";break;
		        case 2 : $user_cond = " AND ( b.RecordType=2 OR c.RecordType=2)"; break;
		        default : $user_cond =""; break;
		}

    	$start_ts = strtotime($start);
		$end_ts = strtotime($end) + 60*60*24 - 1;
		
		$sql = "SELECT a.Details, SUM(a.Amount-IF(t.Amount IS NOT NULL,t.Amount,0)), COUNT(a.LogID) 
				FROM PAYMENT_OVERALL_TRANSACTION_LOG AS a 
				LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.TransactionType=11 AND t.StudentID=a.StudentID AND t.RefCode=a.RefCode 
				LEFT JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID)
		       	LEFT JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID = c.UserID)
		       WHERE a.TransactionType = 3
		          $user_cond
		                    AND UNIX_TIMESTAMP(a.TransactionTime) BETWEEN $start_ts AND $end_ts
		              GROUP BY a.Details ORDER BY a.Details";
		return $this->returnArray($sql,3);

  }

    # Param:
    # = item id
    # Return:
    # - Sum of unpaid amount, Sum of paid amount, Count of unpaid student, Count of paid student
	function returnPaymentPaidSummary($itemID){
		# Grab Summary
		$summary_sql = "SELECT a.Amount, a.RecordStatus FROM
		                         PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
		                 WHERE
		                          a.ItemID = '$itemID'
		 ";
		$count_paid = 0;
		$count_unpaid = 0;
		$sum_paid = 0;
		$sum_unpaid = 0;
		$temp = $this->returnArray($summary_sql,2);
		for ($i=0; $i<sizeof($temp); $i++)
		{
		         list($amount, $status) = $temp[$i];
		         if ($status == 1)
		         {
		                 $count_paid++;
		                 $sum_paid += $amount;
		         }
		         else
		         {
		                 $count_unpaid++;
		                 $sum_unpaid += $amount;
		         }
		}
		$sum_total = $sum_paid + $sum_unpaid;
		$count_total = $count_paid + $count_unpaid;
		$sum_paid = number_format($sum_paid,1,".","");
		$sum_unpaid = number_format($sum_unpaid,1,".","");
		$sum_total = number_format($sum_total,1,".","");
		
		return Array($sum_unpaid, $sum_paid, $count_unpaid, $count_paid);
	}

  # Return an associative array of student balance w/ StudentID as key
  function getBalanceInAssoc()
  {
		$sql = "SELECT StudentID, Balance FROM PAYMENT_ACCOUNT
		              WHERE StudentID IS NOT NULL AND StudentID != 0 AND StudentID != ''";
		$temp = $this->returnArray($sql,2);
		$array = build_assoc_array($temp);
		return $array;
  }

  function getResponseMsg($msg) {
		global $i_con_msg_add, $i_con_msg_update, $i_con_msg_delete, $i_con_msg_email;
		global $i_con_msg_password1, $i_con_msg_password2, $i_con_msg_email1, $i_con_msg_email2;
		global $i_con_msg_suspend, $i_con_msg_add_form_failed, $i_con_msg_delete_part, $i_con_msg_add_failed;
		
		$xmsg = "";
		switch($msg){
			case 1: $xmsg = $i_con_msg_add; break;
			case 2: $xmsg = $i_con_msg_update; break;
			case 3: $xmsg = $i_con_msg_delete; break;
			case 4: $xmsg = $i_con_msg_email; break;
			case 5: $xmsg = $i_con_msg_password1; break;
			case 6: $xmsg = $i_con_msg_password2; break;
			case 7: $xmsg = $i_con_msg_email1; break;
			case 8: $xmsg = $i_con_msg_email2; break;
			case 9: $xmsg = $i_con_msg_suspend; break;
			case 10: $xmsg = $i_con_msg_add_form_failed; break;
			case 11: $xmsg = $i_con_msg_delete_part; break;
			case 12: $xmsg = $i_con_msg_add_failed; break;
		}
		return $xmsg;
	}

    /*
        * Get MODULE_OBJ array
        */
	function GET_MODULE_OBJ_ARR(){
	  global $PATH_WRT_ROOT, $ip20TopMenu, $CurrentPage, $LAYOUT_SKIN,$plugin,$intranet_root, $special_feature, $sys_custom;
	
	  # Menu Item Wordings
	  global $i_Payment_Menu_Settings_PaymentItem;
	  global $i_Payment_Menu_DataImport,$i_Payment_Menu_Import_PPSLink,$i_Payment_Menu_Import_PPSLog,$i_Payment_Menu_Import_CashDeposit;
	  global $i_Payment_Menu_DataBrowsing,$i_Payment_Menu_Browse_TerminalLog,$i_Payment_Menu_Browse_CreditTransaction,$i_Payment_Menu_Browse_StudentBalance,$i_Payment_Menu_Browse_PurchasingRecords,$i_Payment_Menu_Browse_MissedPPSBill;
	  global $i_Payment_Menu_PhotoCopierQuotaSetting,$i_Payment_Menu_PhotoCopier_Package_Setting,$i_Payment_Menu_PhotoCopier_Quota_Management;
	  global $i_Payment_Menu_PrintPage,$i_Payment_Menu_PrintPage_Receipt,$i_Payment_Menu_PrintPage_Outstanding;
	  global $i_Payment_Menu_StatisticsReport,$i_Payment_Menu_PrintPage_AddValueReport,$i_Payment_Menu_Report_SchoolAccount,$i_Payment_Menu_Report_PresetItem,$i_Payment_Menu_Report_TodayTransaction,$i_Payment_Class_Payment_Report;
	  global $i_Payment_Menu_Settings,$i_Payment_Menu_Settings_TerminalIP,$i_Payment_Menu_Settings_TerminalAccount,$i_Payment_Menu_Settings_PaymentCategory,$i_Payment_Menu_Settings_Letter,$i_Payment_Menu_Settings_PPS_Setting, $i_Payment_Menu_Settings_AlipayHK_Handling_Fee;
	  global $i_Payment_Menu_ManualCashDeposit,$i_Payment_Subsidy_Setting,$i_Payment_Menu_Browse_TransactionRecord;
	  global $sms_vendor,$bl_sms_version,$i_SMS_Notification, $i_Payment_Menu_Report_Export_StudentBalance, $i_Payment_Menu_CustomReport, $i_Payment_Menu_Browse_StudentBalance_DirectPay;
	  global $Lang;
	
		$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];
	
	  # Current Page Information init
	  $PagePaymentItemSettings = 0;
	
	  $PageSourceOfSubsidySettings=0;
		
	  $PageeLibraryPlusOverduePayment = 0;	
		
	  $PageAccountDataImport = 0;
	  $PageAccountDataImport_PPSLinkage = 0;
	  $PageAccountDataImport_PPSLog = 0;
	  $PageAccountDataImport_DepositCSV = 0;
	  $PageAccountDateImport_DepositManual=0;
	
	  $PageDataLogBrowsing = 0;
	  $PageDataLogBrowsing_TerminalLog = 0;
	  $PageDataLogBrowsing_CreditTransaction = 0;
	  $PageDataLogBrowsing_ViewUserRecord = 0;
	  $PageDataLogBrowsing_SingleRecord = 0;
	  $PageDataLogBrowsing_UnknownRecord = 0;
	
	  $PagePhotocopierQuota = 0;
	  $PagePhotocopierQuota_PackageSettings = 0;
	  $PagePhotocopierQuota_QuotaManagement = 0;
	
	  $PagePrintPage = 0;
	  $PagePrintPage_Receipt = 0;
	  $PagePrintPage_PaymentReceiptList = 0; // $sys_custom['ePayment']['PaymentItemWithPOS'] for Sin Meng
	  $PagePrintPage_OustandingList = 0;
	  $PagePrintPage_DetailTransactionLog =0;
	
	  $PageStatisticsReport = 0;
	  $PageStatisticsReport_DailyPaymentReport = 0; //Henry
	  $PageStatisticsReport_CreditTransaction = 0;
	  $PageStatisticsReport_SchoolAccount = 0;
	  $PageStatisticsReport_PaymentItem = 0;
	  $PageStatisticsReport_TodayTransaction = 0;
	  $PageStatisticsReport_ClassPayment = 0;
	  $PageStatisticsReport_ExportStudentBalance = 0;
	  $PageStatisticsReport_CancelDeposit = 0;
	  $PageStatisticsReport_Donation = 0;
	  $PageStatisticsReport_Refund = 0;
	  $PageStatisticsReport_BalanceReport = 0;
	  $PageStatisticsReport_DoublePaidPaymentReport = 0;
	  $PageStudentSubsidyIdentity_Report = 0;
	  $PageStudentSubsidy_Report = 0;

	  $PageSmsNotification = 0;
	
	  $PageBasicSettings = 0;
	  $PageBasicSettings_TerminalSettings = 0;
	  $PageBasicSettings_LoginAccount = 0;
	  $PageBasicSettings_PaymentItemCat = 0;
	  $PageBasicSettings_PaymentLetter = 0;
	  $PageBasicSettings_PPS = 0;
	  $PageTransferMoneyForStudents = 0;
	  $PageBasicSettings_AlipayHK_Handling_Fee = 0;

		$PageCustomReport = 0;
		$PageCustomReport_ServiceProductSales = 0;
		$PageCustomReport_SalesDetail = 0;

		if (!is_array($CurrentPage)) {
		  switch ($CurrentPage) {
		    case "PaymentItemSettings":
		      $PagePaymentItemSettings = 1;
		      break;
		    case "eLibraryPlusOverduePayment":
		       $PageeLibraryPlusOverduePayment = 1;
		      break;
		      
		    case "ElectronicBilling":
		    	$PageElectronicBilling = 1;
		    	break;
		    case "TransferMoneyForStudents":
                $PageTransferMoneyForStudents = 1;
                break;
		    case "ImportTNGTransactionRecords":
		    	$PageImportTNGTransactionRecords = 1;
		    	break;
		    case "ImportAlipayTransactionRecords":
		      	$PageImportAlipayTransactionRecords = 1;
		      	break; 
		    case "SourceOfSubsidySettings":
		      $PageSourceOfSubsidySettings=1;
		      break;
		    case "SubsidyIdentitySettings":
				$PageSubsidyIdentitySettings = 1;
			break;
		    case "AccountDataImport_PPSLinkage":
		      $PageAccountDataImport = 1;
		      $PageAccountDataImport_PPSLinkage = 1;
		      break;
		    case "AccountDataImport_PPSLog":
		      $PageAccountDataImport= 1;
		      $PageAccountDataImport_PPSLog = 1;
		      break;
		    case "AccountDataImport_DepositCSV":
		      $PageAccountDataImport = 1;
		      $PageAccountDataImport_DepositCSV = 1;
		      break;
		    case "AccountDataImport_DepositManual":
		      $PageAccountDataImport = 1;
		      $PageAccountDataImport_DepositManual = 1;
		      break;
		    case "PageAccountDataImport_CashLog":
		    	$PageAccountDataImport = 1;
		    	$PageAccountDataImport_CashLog = 1;
		    	break;
		    case "DataLogBrowsing_TerminalLog":
		      $PageDataLogBrowsing = 1;
		      $PageDataLogBrowsing_TerminalLog = 1;
		      break;
		    case "DataLogBrowsing_CreditTransaction":
		      $PageDataLogBrowsing = 1;
		      $PageDataLogBrowsing_CreditTransaction = 1;
		      break;
		    case "DataLogBrowsing_ViewUserRecord":
		      $PageDataLogBrowsing = 1;
		      $PageDataLogBrowsing_ViewUserRecord = 1;
		      break;
		    case "DataLogBrowsing_SingleRecord":
		      $PageDataLogBrowsing = 1;
		      $PageDataLogBrowsing_SingleRecord = 1;
		      break;
		    case "DataLogBrowsing_UnknownRecord":
		      $PageDataLogBrowsing = 1;
		      $PageDataLogBrowsing_UnknownRecord = 1;
		      break;
		    case "TNGPaymentRecords":
		    	$PageDataLogBrowsing = 1;
		    	$PageDataLogBrowsing_TNGPaymentRecords = 1;
		    break;
		    case "AlipayPaymentRecords":
		    	$PageDataLogBrowsing = 1;
		    	$PageDataLogBrowsing_AlipayPaymentRecords = 1;
		    break;
			  case "VisaMasterPaymentRecords":
				  $PageDataLogBrowsing = 1;
				  $PageDataLogBrowsing_VisaMasterPaymentRecords = 1;
				  break;
			  case "WeChatPaymentRecords":
				  $PageDataLogBrowsing = 1;
				  $PageDataLogBrowsing_WeChatPaymentRecords = 1;
				  break;
			  case "AlipayCNPaymentRecords":
				  $PageDataLogBrowsing = 1;
				  $PageDataLogBrowsing_AlipayCNPaymentRecords = 1;
				  break;
			  case "FpsPaymentRecords":
				  $PageDataLogBrowsing = 1;
				  $PageDataLogBrowsing_FpsPaymentRecords = 1;
				  break;
			  case "TapAndGoPaymentRecords":
				  $PageDataLogBrowsing = 1;
				  $PageDataLogBrowsing_TapAndGoPaymentRecords = 1;
				  break;
		    case "PhotocopierQuota_PackageSettings":
		      $PagePhotocopierQuota = 1;
		      $PagePhotocopierQuota_PackageSettings = 1;
		      break;
		    case "PhotocopierQuota_QuotaManagement":
		      $PagePhotocopierQuota = 1;
		      $PagePhotocopierQuota_QuotaManagement = 1;
		      break;
		    case "PrintPage_Receipt":
		      $PagePrintPage = 1;
		      $PagePrintPage_Receipt = 1;
		      break;
		    case "PrintPage_PaymentReceiptList":
		      $PagePrintPage = 1;
		      $PagePrintPage_PaymentReceiptList = 1;
		       break;
		    case "PrintPage_OustandingList":
		      $PagePrintPage = 1;
		      $PagePrintPage_OustandingList = 1;
		      break;
		    case "PrintPage_DetailTransactionLog":
		      $PagePrintPage = 1;
		      $PagePrintPage_DetailTransactionLog = 1;
		      break;
			//Henry
			case "StatisticsReport_DailyPaymentReport":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_DailyPaymentReport = 1;
		      break;
		    case "StatisticsReport_CreditTransaction":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_CreditTransaction = 1;
		      break;
		    case "StatisticsReport_SchoolAccount":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_SchoolAccount = 1;
		      break;
		    case "StatisticsReport_IncomeExpensesReport":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_IncomeExpensesReport = 1;
		    break;
		    case "StatisticsReport_PaymentItem":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_PaymentItem = 1;
		      break;
		    case "StatisticsReport_PaymentItemStatusReport":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_PaymentItemStatusReport = 1;
		      break;
		    case "StatisticsReport_TodayTransaction":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_TodayTransaction = 1;
		      break;
		    case "StatisticsReport_ClassPayment":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_ClassPayment = 1;
		      break;
				case "StatisticsReport_ExportStudentBalance":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_ExportStudentBalance = 1;
		      break;
		    case "StatisticsReport_CancelDeposit":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_CancelDeposit = 1;
		      break;
		    case "StatisticsReport_Donation":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_Donation = 1;
		      break;
		    case "StatisticsReport_Refund":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_Refund = 1;
		      break;
				case "StatisticsReport_POSTransaction":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_POSTransaction = 1;
		      break;
		    case "StatisticsReport_POSItem":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_POSItem = 1;
		      break;
		    case "StatisticsReport_POSStudent":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_POSStudent = 1;
		      break;
		    case "StatisticsReport_BalanceReport":
		      $PageStatisticsReport = 1;
		      $PageStatisticsReport_BalanceReport = 1;
		    break;
			  case "StudentSubsidyIdentity_Report":
			  	$PageStatisticsReport = 1;
			  	$PageStudentSubsidyIdentity_Report = 1;
			  	break;
			  case "StatisticsReport_DoublePaidPaymentReport":
				  $PageStatisticsReport = 1;
				  $PageStatisticsReport_DoublePaidPaymentReport = 1;
				  break;
			  case "StudentSubsidy_Report":
				  $PageStatisticsReport = 1;
				  $PageStudentSubsidy_Report = 1;
				  break;
		    case "SmsNotification":
		      $PageSmsNotification = 1;
		      break;
		    case "BasicSettings_TerminalSettings":
		      $PageBasicSettings = 1;
		      $PageBasicSettings_TerminalSettings = 1;
		      break;
		    case "BasicSettings_LoginAccount":
		      $PageBasicSettings = 1;
		      $PageBasicSettings_LoginAccount = 1;
		      break;
		    case "BasicSettings_PaymentItemCat":
		      $PageBasicSettings = 1;
		      $PageBasicSettings_PaymentItemCat = 1;
		      break;
		    case "BasicSettings_MerchantAccount":
		      $PageBasicSettings = 1;
		      $PageBasicSettings_MerchantAccount = 1;
		      break;
		    case "BasicSettings_PaymentLetter":
		      $PageBasicSettings = 1;
		      $PageBasicSettings_PaymentLetter = 1;
		      break;
		    case "BasicSettings_PPS":
		      $PageBasicSettings = 1;
		      $PageBasicSettings_PPS = 1;
		      break;
			case "BasicSettings_AlipayHK_Handling_Fee":
				$PageBasicSettings = 1;
				$PageBasicSettings_AlipayHK_Handling_Fee = 1;
				break;
		    case "BasicSettings_CashDepositApproval":
		      $PageBasicSettings = 1;
		      $PageBasicSettings_CashDepositApproval = 1;
		     break;
		     case "StudentPaymentMethodSettings":
		      $PageBasicSettings = 1;
		      $PageBasicSettings_StudentPaymentMethodSettings = 1;
		     break;
	
		     case "PushMessageTemplateSettings":
		      $PageBasicSettings = 1;
		      $PageBasicSettings_PushMessageTemplateSettings = 1;
		      break;
			  case "CustomReport_ServiceProductSales":
			  	$PageCustomReport = 1;
			  	$PageCustomReport_ServiceProductSales = 1;
				break;
			  case "CustomReport_SalesDetail":
				  $PageCustomReport = 1;
				  $PageCustomReport_SalesDetail = 1;
				  break;
		    
		  }
		}
	
	  # Menu information
		$PaymentPath = $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/";
	  $MenuArr["PaymentItemSettings"] = array($i_Payment_Menu_Settings_PaymentItem, $PaymentPath."settings/payment_item/", $PagePaymentItemSettings);
	  $MenuArr["SourceOfSubsidySettings"] = array($i_Payment_Subsidy_Setting, $PaymentPath."settings/subsidy_unit/", $PageSourceOfSubsidySettings);
	  if($sys_custom['PowerClass']){
	      $MenuArr["PaymentItemSettings"] = array($i_Payment_Menu_Settings_PaymentItem, "", $PagePaymentItemSettings);
	      $MenuArr["PaymentItemSettings"]["Child"]["PaymentItemSettings"] = array($i_Payment_Menu_Settings_PaymentItem, $PaymentPath."settings/payment_item/", $PagePaymentItemSettings);
	      $MenuArr["SourceOfSubsidySettings"] = array($i_Payment_Subsidy_Setting, "", $PageSourceOfSubsidySettings);
	      $MenuArr["SourceOfSubsidySettings"]["Child"]["SourceOfSubsidySettings"] =array($i_Payment_Subsidy_Setting, $PaymentPath."settings/subsidy_unit/", $PageSourceOfSubsidySettings);
	  }
	
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] && $sys_custom['eLibraryPlus']['ePaymentOverduePayment'] && $plugin['library_management_system']){
	  $MenuArr["eLibraryPlusOverduePayment"] = array($Lang['ePayment']['eLibPlusOverduePayment'], $PaymentPath."management/elibrary_overdue_payment/", $PageeLibraryPlusOverduePayment);
	}
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] && $sys_custom['ePayment']['ElectronicBilling']){
		$MenuArr["ElectronicBilling"] = array($Lang['ePayment']['ElectronicBilling'], $PaymentPath."management/electronic_billing/", $PageElectronicBilling);
		if($sys_custom['PowerClass']){
		    $MenuArr["ElectronicBilling"] = array($Lang['ePayment']['ElectronicBilling'], "", $PageElectronicBilling);
		    $MenuArr["ElectronicBilling"]["Child"]["ElectronicBilling"] = array($Lang['ePayment']['ElectronicBilling'], $PaymentPath."management/electronic_billing/", $PageElectronicBilling);
		}
	}

	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] && ($sys_custom['ePayment']['TNG'] || $sys_custom['ePayment']['TopUpEWallet']['TNG'])){
		$MenuArr["ImportTNGTransactionRecords"] = array($Lang['ePayment']['ImportTNGTransactionRecords'], $PaymentPath."management/import_tng/", $PageImportTNGTransactionRecords);
		if($sys_custom['PowerClass']){
			$MenuArr["ImportTNGTransactionRecords"] = array($Lang['ePayment']['ImportTNGTransactionRecords'], "", $PageImportTNGTransactionRecords);
			$MenuArr["ImportTNGTransactionRecords"]["Child"]["ImportTNGTransactionRecords"] = array($Lang['ePayment']['ImportTNGTransactionRecords'], $PaymentPath."management/import_tng/", $PageImportTNGTransactionRecords);
		}
	}
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] && ($sys_custom['ePayment']['Alipay'] || $sys_custom['ePayment']['TopUpEWallet']['Alipay'])){
		$MenuArr["ImportAlipayTransactionRecords"] = array($Lang['ePayment']['ImportAlipayTransactionRecords'], $PaymentPath."management/import_alipay/", $PageImportAlipayTransactionRecords);
    	if($sys_custom['PowerClass']){
			$MenuArr["ImportAlipayTransactionRecords"] = array($Lang['ePayment']['ImportAlipayTransactionRecords'], "", $PageImportTNGTransactionRecords);
			$MenuArr["ImportAlipayTransactionRecords"]["Child"]["ImportAlipayTransactionRecords"] = array($Lang['ePayment']['ImportAlipayTransactionRecords'], $PaymentPath."management/import_alipay/", $PageImportAlipayTransactionRecords);
		}
    }

    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] && $sys_custom['ePayment']['AdminTransferMoneyForStudents']) {
        $MenuArr["AdminTransferMoneyForStudents"] = array($Lang['ePayment']['AdminTransferMoneyForStudents'], $PaymentPath . "management/transfer_money_for_students/", $PageTransferMoneyForStudents);
    }

	if(!$isKIS) {
	  $MenuArr["AccountDataImport"] = array($i_Payment_Menu_DataImport, "", $PageAccountDataImport);
	  if(!$sys_custom['ePayment']['HidePPS']) $MenuArr["AccountDataImport"]["Child"]["PPSLinkage"] = array($i_Payment_Menu_Import_PPSLink, $PaymentPath."import/pps_account_list.php", $PageAccountDataImport_PPSLinkage);
	  if(!$sys_custom['ePayment']['HidePPS']) $MenuArr["AccountDataImport"]["Child"]["PPSLog"] = array($i_Payment_Menu_Import_PPSLog, $PaymentPath."import/pps_log.php", $PageAccountDataImport_PPSLog);
	  $MenuArr["AccountDataImport"]["Child"]["CashLog"] = array($Lang['Payment']['CashDeposit'],$PaymentPath."import/cash_log/cash_log.php", $PageAccountDataImport_CashLog);
	  /*$MenuArr["AccountDataImport"]["Child"]["DepositCSV"] = array($i_Payment_Menu_Import_CashDeposit, $PaymentPath."import/cash_log.php", $PageAccountDataImport_DepositCSV);
	  $MenuArr["AccountDataImport"]["Child"]["DepositManual"] = array($i_Payment_Menu_ManualCashDeposit, $PaymentPath."import/manual_cash_log.php", $PageAccountDataImport_DepositManual);*/
	}
	  $MenuArr["DataLogBrowsing"] = array($i_Payment_Menu_DataBrowsing, "", $PageDataLogBrowsing);
	if(!$isKIS) {
	  $MenuArr["DataLogBrowsing"]["Child"]["TerminalLog"] = array($i_Payment_Menu_Browse_TerminalLog, $PaymentPath."browse/terminal/", $PageDataLogBrowsing_TerminalLog);
	  $MenuArr["DataLogBrowsing"]["Child"]["CreditTransaction"] = array($i_Payment_Menu_Browse_CreditTransaction, $PaymentPath."browse/credit/", $PageDataLogBrowsing_CreditTransaction);
	}

		if($this->isEWalletDirectPayEnabled()) {
			$text_i_Payment_Menu_Browse_StudentBalance = $i_Payment_Menu_Browse_StudentBalance_DirectPay;
		} else {
			$text_i_Payment_Menu_Browse_StudentBalance = $i_Payment_Menu_Browse_StudentBalance;
		}
	  $MenuArr["DataLogBrowsing"]["Child"]["ViewUserRecord"] = array($text_i_Payment_Menu_Browse_StudentBalance, $PaymentPath."browse/student/", $PageDataLogBrowsing_ViewUserRecord);
	  //if (!$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS']) { // if have pos module hide this item
	  	$MenuArr["DataLogBrowsing"]["Child"]["SingleRecord"] = array($i_Payment_Menu_Browse_PurchasingRecords, $PaymentPath."browse/single_purchase/", $PageDataLogBrowsing_SingleRecord);
	  //}
	if(!$isKIS) {
	  $MenuArr["DataLogBrowsing"]["Child"]["UnknownRecord"] = array($i_Payment_Menu_Browse_MissedPPSBill, $PaymentPath."browse/missed_pps/", $PageDataLogBrowsing_UnknownRecord);
	}
	if($sys_custom['ePayment']['TNG'] || $sys_custom['ePayment']['TopUpEWallet']['TNG']){
		$MenuArr["DataLogBrowsing"]["Child"]["TNGPaymentRecords"] = array($Lang['ePayment']['TNGPaymentRecords'], $PaymentPath."browse/tng/", $PageDataLogBrowsing_TNGPaymentRecords);
	}
	if($sys_custom['ePayment']['Alipay'] || $sys_custom['ePayment']['TopUpEWallet']['Alipay']){
		$MenuArr["DataLogBrowsing"]["Child"]["AlipayPaymentRecords"] = array($Lang['ePayment']['Alipay'], $PaymentPath."browse/alipay/", $PageDataLogBrowsing_AlipayPaymentRecords);
	}

	if($sys_custom['ePayment']['FPS'] || $sys_custom['ePayment']['TopUpEWallet']['FPS']){
		$MenuArr["DataLogBrowsing"]["Child"]["FpsPaymentRecords"] = array($Lang['ePayment']['FPS'], $PaymentPath."browse/fps/", $PageDataLogBrowsing_FpsPaymentRecords);
	}

	if($this->isTapAndGoDirectPayEnabled() || $this->isTapAndGoTopUpEnabled()) {
		$MenuArr["DataLogBrowsing"]["Child"]["TapAndGoPaymentRecords"] = array($Lang['ePayment']['TapAndGo'], $PaymentPath."browse/tapandgo/", $PageDataLogBrowsing_TapAndGoPaymentRecords);
	}

	if($this->isVisaMasterDirectPayEnabled() || $this->isVisaMasterTopUpEnabled()) {
		$MenuArr["DataLogBrowsing"]["Child"]["VisaMasterPaymentRecords"] = array($Lang['ePayment']['VisaMaster'], $PaymentPath."browse/visamaster/", $PageDataLogBrowsing_VisaMasterPaymentRecords);
	}

	if($this->isWeChatDirectPayEnabled() || $this->isWeChatTopUpEnabled()) {
		$MenuArr["DataLogBrowsing"]["Child"]["WeChatPaymentRecords"] = array($Lang['ePayment']['WeChat'], $PaymentPath."browse/wechat/", $PageDataLogBrowsing_WeChatPaymentRecords);
	}

	if($this->isAlipayCNDirectPayEnabled() || $this->isAlipayCNTopUpEnabled()) {
		$MenuArr["DataLogBrowsing"]["Child"]["AlipayCNPaymentRecords"] = array($Lang['ePayment']['AlipayCN'], $PaymentPath."browse/alipaycn/", $PageDataLogBrowsing_AlipayCNPaymentRecords);
	}

	if($_SESSION['platform']!='KIS'){
	  $MenuArr["PhotocopierQuota"] = array($i_Payment_Menu_PhotoCopierQuotaSetting, "", $PagePhotocopierQuota);
	  $MenuArr["PhotocopierQuota"]["Child"]["PackageSettings"] = array($i_Payment_Menu_PhotoCopier_Package_Setting, $PaymentPath."photocopier_quota/package/", $PagePhotocopierQuota_PackageSettings);
	  $MenuArr["PhotocopierQuota"]["Child"]["QuotaManagement"] = array($i_Payment_Menu_PhotoCopier_Quota_Management, $PaymentPath."photocopier_quota/quota/view/", $PagePhotocopierQuota_QuotaManagement);
	}
	  $MenuArr["PrintPage"] = array($i_Payment_Menu_PrintPage, "", $PagePrintPage);
	  $MenuArr["PrintPage"]["Child"]["Receipt"] = array($i_Payment_Menu_PrintPage_Receipt, $PaymentPath."printpages/receipt/paymentlist.php", $PagePrintPage_Receipt);
	  if($sys_custom['ePayment']['PaymentItemWithPOS']){
	  	$MenuArr["PrintPage"]["Child"]["PaymentReceiptList"] = array($Lang['ePayment']['PaymentReceiptList'], $PaymentPath."printpages/payment_receipt_list/index.php", $PagePrintPage_PaymentReceiptList);
	  }
	  $MenuArr["PrintPage"]["Child"]["OustandingList"] = array($i_Payment_Menu_PrintPage_Outstanding, $PaymentPath."printpages/outstanding/", $PagePrintPage_OustandingList);
	  $MenuArr["PrintPage"]["Child"]["DetailTransactionLog"] = array($i_Payment_Menu_Browse_TransactionRecord, $PaymentPath."printpages/transaction_log/", $PagePrintPage_DetailTransactionLog);

	  if($sys_custom['ttmss_custom_report']) {
		  $MenuArr["CustomReport"] = array($i_Payment_Menu_CustomReport, "", $PageCustomReport);
		  $MenuArr["CustomReport"]["Child"]["ServiceProductSales"] = array($Lang['ePayment']['ServiceProductSales'], $PaymentPath . "custom_report/service_product_sales_report/", $PageCustomReport_ServiceProductSales);
		  $MenuArr["CustomReport"]["Child"]["SalesDetail"] = array($Lang['ePayment']['SalesDetail'], $PaymentPath . "custom_report/sales_detail_report/", $PageCustomReport_SalesDetail);
	  }

	  $MenuArr["StatisticsReport"] = array($i_Payment_Menu_StatisticsReport, "", $PageStatisticsReport);
	  $MenuArr["StatisticsReport"]["Child"]["DailyPaymentReport"] = array($Lang['ePayment']['DailyPaymentReport'], $PaymentPath."report/daily_payment_report/", $PageStatisticsReport_DailyPaymentReport);//Henry
	if(!$isKIS){  
	  $MenuArr["StatisticsReport"]["Child"]["CreditTransaction"] = array($i_Payment_Menu_PrintPage_AddValueReport, $PaymentPath."report/credit/", $PageStatisticsReport_CreditTransaction);
	}  
	if($this->isEWalletDirectPayEnabled()){
	  $MenuArr["StatisticsReport"]["Child"]["IncomeExpensesReport"] = array($Lang['ePayment']['IncomeExpensesReport'], $PaymentPath."report/income_expenses_report/", $PageStatisticsReport_IncomeExpensesReport);
	}else{
	  $MenuArr["StatisticsReport"]["Child"]["SchoolAccount"] = array($i_Payment_Menu_Report_SchoolAccount, $PaymentPath."report/schoolaccount/", $PageStatisticsReport_SchoolAccount);
	}
	  $MenuArr["StatisticsReport"]["Child"]["PaymentItem"] = array($i_Payment_Menu_Report_PresetItem, $PaymentPath."report/payment_item/", $PageStatisticsReport_PaymentItem);
	if($sys_custom['ePayment']['HartsPreschool']){
	  $MenuArr["StatisticsReport"]["Child"]["PaymentItemStatusReport"] = array($Lang['ePayment']['PaymentItemStatusReport'], $PaymentPath."report/payment_item_status_report/", $PageStatisticsReport_PaymentItemStatusReport);
	}  
	  $MenuArr["StatisticsReport"]["Child"]["TodayTransaction"] = array($i_Payment_Menu_Report_TodayTransaction, $PaymentPath."report/transaction/", $PageStatisticsReport_TodayTransaction);
	  $MenuArr["StatisticsReport"]["Child"]["ClassPayment"] = array($i_Payment_Class_Payment_Report, $PaymentPath."report/class/", $PageStatisticsReport_ClassPayment);
	  $MenuArr["StatisticsReport"]["Child"]["ExportStudentBalance"] = array($i_Payment_Menu_Report_Export_StudentBalance, $PaymentPath."report/student_balance/", $PageStatisticsReport_ExportStudentBalance);
	if(!$isKIS){  
	  $MenuArr["StatisticsReport"]["Child"]["CancelDeposit"] = array($Lang['Payment']['CancelDepositReport'], $PaymentPath."report/cancel_deposit_report/", $PageStatisticsReport_CancelDeposit);
	  $MenuArr["StatisticsReport"]["Child"]["Donation"] = array($Lang['Payment']['DonationReport'], $PaymentPath."report/donation_report/", $PageStatisticsReport_Donation);
	  if($this->isEWalletDirectPayEnabled() == false) {
		  $refund_report = $Lang['ePayment']['LeaveSchoolRefundReport'];
	  } else {
		  $refund_report = $Lang['ePayment']['RefundReport'];
	  }
	  $MenuArr["StatisticsReport"]["Child"]["Refund"] = array($refund_report, $PaymentPath."report/refund_report/", $PageStatisticsReport_Refund);
	  $MenuArr["StatisticsReport"]["Child"]["BalanceReport"] = array($Lang['ePayment']['BalanceReport'], $PaymentPath."report/balance_report/", $PageStatisticsReport_BalanceReport);
	}

	$MenuArr["StatisticsReport"]["Child"]["StudentSubsidyIdentityReport"] = array($Lang['ePayment']['StudentSubsidyIdentityReport'], $PaymentPath."report/student_subsidy_identity_report/", $PageStudentSubsidyIdentity_Report);

	// Temporary hide this menu until done
	$MenuArr["StatisticsReport"]["Child"]["StudentSubsidyReport"] = array($Lang['ePayment']['StudentSubsidyReport'], $PaymentPath."report/student_subsidy_report/", $PageStudentSubsidy_Report);

	if($sys_custom['ePayment']['DoublePaidPaymentReport']) {
		$MenuArr["StatisticsReport"]["Child"]["DoublePaidPaymentReport"] = array($Lang['ePayment']['DoublePaidPaymentReport'], $PaymentPath . "report/double_paid_payment_report/", $PageStatisticsReport_DoublePaidPaymentReport);
	}

		# SMS Notification
	  if ($bl_sms_version>=2){
			include_once("libsmsv2.php");
			include_once("libkanhansms.php");
			//checkout the status of the payment related template
			$sms_status = 0;
			$lsms = new libsmsv2();
			if($lsms->returnTemplateStatus("","STUDENT_MAKE_PAYMENT") and $lsms->returnSystemMsg("STUDENT_MAKE_PAYMENT"))        
				$sms_status = 1;
			if($lsms->returnTemplateStatus("","BALANCE_REMINDER") and $lsms->returnSystemMsg("BALANCE_REMINDER"))        
				$sms_status = 1;
    }
    else{
			$sms_status = 0;
    }
    if(($plugin['sms'] && $bl_sms_version>=2 && $sms_status || $plugin['eClassApp'])){
    	$MenuArr["SmsNotification"] = array($Lang['ePayment']['Notification'], $PaymentPath."sms_notification/", $PageSmsNotification);
    	if($sys_custom['PowerClass']){
    	    $MenuArr["SmsNotification"]= array($Lang['ePayment']['Notification'], "", $PageSmsNotification);
    	    $MenuArr["SmsNotification"]["Child"]["SmsNotification"] = array($Lang['ePayment']['Notification'], $PaymentPath."sms_notification/", $PageSmsNotification);
    	}
    }
    	
    	
	  $MenuArr["BasicSettings"] = array($i_Payment_Menu_Settings, "", ($PageBasicSettings || (is_array($CurrentPage) && $CurrentPage['SystemProperty']==1) ));
	  if($special_feature['ePaymentNotice'])
	  {
	  		$MenuArr["BasicSettings"]["Child"]["SystemProperties"] = array($Lang['ePayment']['SystemProperties'], $PaymentPath."settings/system_properties/", (is_array($CurrentPage) && $CurrentPage['SystemProperty']==1));
	  }
	if(!$isKIS){
	  $MenuArr["BasicSettings"]["Child"]["TerminalSettings"] = array($i_Payment_Menu_Settings_TerminalIP, $PaymentPath."settings/terminal/", $PageBasicSettings_TerminalSettings);
	  $MenuArr["BasicSettings"]["Child"]["LoginAccount"] = array($i_Payment_Menu_Settings_TerminalAccount, $PaymentPath."settings/account/", $PageBasicSettings_LoginAccount);
	} 
	  $MenuArr["BasicSettings"]["Child"]["PaymentItemCat"] = array($i_Payment_Menu_Settings_PaymentCategory, $PaymentPath."settings/payment_cat/", $PageBasicSettings_PaymentItemCat);
	if($this->useEWalletMerchantAccount() || $this->isEWalletTopUpEnabled()){
		$MenuArr["BasicSettings"]["Child"]["MerchantAccount"] = array($Lang['ePayment']['MerchantAccount'], $PaymentPath."settings/merchant_account/", $PageBasicSettings_MerchantAccount);
	}
	  $MenuArr["BasicSettings"]["Child"]["PaymentLetter"] = array($i_Payment_Menu_Settings_Letter, $PaymentPath."settings/letter/", $PageBasicSettings_PaymentLetter);
	if(!$isKIS){  
	  if(!$sys_custom['ePayment']['HidePPS']) $MenuArr["BasicSettings"]["Child"]["PPS"] = array($i_Payment_Menu_Settings_PPS_Setting, $PaymentPath."settings/pps/", $PageBasicSettings_PPS);
	}
	
 	// This setting not used anymore, has moved handling fee to merchant account settings to support different handling fee for different account
	//if($this->isAlipayDirectPayEnabled() || $this->isAlipayTopUpEnabled()) {
	//	$MenuArr["BasicSettings"]["Child"]["AlipayHKHandlingFee"] = array($i_Payment_Menu_Settings_AlipayHK_Handling_Fee, $PaymentPath . "settings/alipay/", $PageBasicSettings_AlipayHK_Handling_Fee);
	//}

	$MenuArr["BasicSettings"]["Child"]["SubsidyIdentitySettings"] = array($Lang['ePayment']['SubsidyIdentitySettings'], $PaymentPath."settings/subsidy_identity/", $PageSubsidyIdentitySettings);
	if($sys_custom['ePayment']['CashDepositApproval']){
		$MenuArr["BasicSettings"]["Child"]["CashDepositApproval"] = array($Lang['ePayment']['CashDepositApproval'], $PaymentPath."settings/cash_deposit_approval/", $PageBasicSettings_CashDepositApproval);
	}
	
	 if($sys_custom['ePayment']['HartsPreschool']){
	 	$MenuArr["BasicSettings"]["Child"]["StudentPaymentMethodSettings"] = array($Lang['ePayment']['StudentPaymentMethodSettings'], $PaymentPath."settings/payment_method/", $PageBasicSettings_StudentPaymentMethodSettings);
	 }
	 if($plugin['eClassApp'] == true){
	 	$MenuArr["BasicSettings"]["Child"]["PushMessageTemplateSettings"] = array($Lang['eClassApp']['PushMessageTemplate'], $PaymentPath."settings/payment_templates/", $PageBasicSettings_PushMessageTemplateSettings);
	 }

        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass ePayment";'."\n";
        $js.= '</script>'."\n";
	
	  # module information
	  $MODULE_OBJ['title'] = $ip20TopMenu['ePayment'].$js;
	  $MODULE_OBJ['title_css'] = "menu_opened";
	  $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_ePayment.gif";
	  if($sys_custom['PowerClass']){
	  	if(isset($_SESSION['PowerClass_PAGE'])){
		  	if($_SESSION['PowerClass_PAGE']=="reports"){
		  		$MenuArr["StatisticsReport"]["Child"] = array(
		  				"SchoolAccount"		  =>	$MenuArr["StatisticsReport"]["Child"]["SchoolAccount"],
		  				"PaymentItem"		  =>	$MenuArr["StatisticsReport"]["Child"]["PaymentItem"],
		  				"ClassPayment"		  =>	$MenuArr["StatisticsReport"]["Child"]["ClassPayment"],
    		  		    "DailyPaymentReport"  =>	$MenuArr["StatisticsReport"]["Child"]["DailyPaymentReport"],
    		  		    "CreditTransaction"	  =>	$MenuArr["StatisticsReport"]["Child"]["CreditTransaction"],
    		  		    "TodayTransaction"	  =>	$MenuArr["StatisticsReport"]["Child"]["TodayTransaction"],
    		  		    "ExportStudentBalance"=>	$MenuArr["StatisticsReport"]["Child"]["ExportStudentBalance"],
    		  		    "CancelDeposit"	      =>	$MenuArr["StatisticsReport"]["Child"]["CancelDeposit"],
    		  		    "Donation"	          =>	$MenuArr["StatisticsReport"]["Child"]["Donation"] ,
    		  		    "Refund"	          =>	$MenuArr["StatisticsReport"]["Child"]["Refund"] ,
    		  		    "BalanceReport"	      =>	$MenuArr["StatisticsReport"]["Child"]["BalanceReport"]
		  		);
		  		$MenuArr = array("StatisticsReport"=>$MenuArr["StatisticsReport"]);
		  	}elseif($_SESSION['PowerClass_PAGE']=="app"){
// 		  		unset($MenuArr["AccountDataImport"]["Child"]["PPSLinkage"]);
// 		  		unset($MenuArr["AccountDataImport"]["Child"]["PPSLog"]);
		  	    unset($MenuArr["StatisticsReport"]);
		  		unset($MenuArr["PrintPage"]["Child"]["PaymentReceiptList"]);
		  		unset($MenuArr["BasicSettings"]);
// 		  		unset($MenuArr["PrintPage"]["Child"]["DetailTransactionLog"]);
				//$MenuArr = array();
				$MenuArr["PaymentItemSettings"] = $MenuArr["PaymentItemSettings"];
				$MenuArr["SourceOfSubsidySettings"] = $MenuArr["SourceOfSubsidySettings"];
// 				$MenuArr["ElectronicBilling"] = $MenuArr["ElectronicBilling"];
				if(isset($MenuArr["ImportTNGTransactionRecords"])){
					$MenuArr["ImportTNGTransactionRecords"] = $MenuArr["ImportTNGTransactionRecords"];
				}
				$MenuArr["AccountDataImport"] = $MenuArr["AccountDataImport"];
				$MenuArr["DataLogBrowsing"] = $MenuArr["DataLogBrowsing"];
				$MenuArr["PrintPage"] = $MenuArr["PrintPage"];
				$MenuArr["SmsNotification"] = $MenuArr["SmsNotification"];
		  	}
	  	}
	  }
	  $MODULE_OBJ['menu'] =  array_filter($MenuArr);
	  
	  return $MODULE_OBJ;
	}

  function getStudentOutstandingAmt($student_id, $IncludeNonOverDue=0)
  {
		if($IncludeNonOverDue!=1)
		{
		        $cond = " AND b.EndDate < CURDATE() ";
		}else{
		        $cond = " AND b.StartDate <=CURDATE()";
		}
		
		$sql = "SELECT
		                a.Amount
		        FROM
		                PAYMENT_PAYMENT_ITEMSTUDENT AS a
		                LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM AS b ON a.ItemID = b.ItemID
		                LEFT OUTER JOIN INTRANET_USER AS c ON a.StudentID = c.UserID
		        WHERE
		                a.RecordStatus <> 1
		                and c.UserID = '$student_id'
		                $cond
		                ";
		$data = $this->returnArray($sql, 1);
		
		$total_amt = 0;
		for($i=0; $i<sizeof($data); $i++)
		{
		$total_amt += $data[$i][0];
		}
		
		return $total_amt;
  }

  function getWebDisplayAmountFormat($amount){
  		if(!is_numeric($amount)){
  			return '$'.$amount;
  		}
	  if($amount < 0) {
		  return "-$".number_format(abs($amount),LIBPAYMENT_AMOUNT_DECIMAIL);
	  }
	  return "$".number_format($amount,LIBPAYMENT_AMOUNT_DECIMAIL);
	}
	function getWebDisplayAmountFormatDB($field){
		return " IF(ROUND($field,".LIBPAYMENT_AMOUNT_DECIMAIL.") >= 0, CONCAT('$',ROUND($field,".LIBPAYMENT_AMOUNT_DECIMAIL.")), CONCAT('-$',ABS(ROUND($field,".LIBPAYMENT_AMOUNT_DECIMAIL.")))) ";
		//return " CONCAT('$',ROUND($field,".LIBPAYMENT_AMOUNT_DECIMAIL.")) ";
	}
	function getExportAmountFormat($amount){
	  return round($amount,LIBPAYMENT_AMOUNT_DECIMAIL);
	}
	function getExportAmountFormatDB($field){
	  return " ROUND($field,".LIBPAYMENT_AMOUNT_DECIMAIL.") ";
	}
	
	function update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($total_quota,$recordType,$condition,$objDB, $type = "")
	{
		/*        recordType        1 - Add
		                                2 - Reset
		                                3 - Quota deduction from Copier
		                                4 - Purchase
		          type (reset type)	: 0 - total quota, 1 - used quota
		*/
		$sql  = "INSERT INTO PAYMENT_PRINTING_QUOTA_CHANGE_LOG ";
		$sql .= "(UserID,QuotaChange,QuotaAfter,UsedQuota,TotalQuota,RecordType,DateInput,DateModified,ModifiedBy) ";
		$sql .= "select ";
		if($recordType == 2)
		{
				if($type == 0)
		        	$sql .= "userID,0,".$total_quota." - UsedQuota, UsedQuota, TotalQuota, ".$recordType.",Now(),Now(),'".$_SESSION['UserID']."' ";
		        else if($type == 1)
		        	$sql .= "userID,0,TotalQuota - ".$total_quota.", UsedQuota, TotalQuota, ".$recordType.",Now(),Now(),'".$_SESSION['UserID']."' ";
		        else
		        	$sql .= "userID,0,".$total_quota.", UsedQuota, TotalQuota, ".$recordType.",Now(),Now(),'".$_SESSION['UserID']."' ";
		}
		else
		{
		        $sql .= "userID,".$total_quota.",TotalQuota - UsedQuota + ".$total_quota.", UsedQuota, TotalQuota, ".$recordType.",Now(),Now(),'".$_SESSION['UserID']."' ";
		}
		$sql .= "from ";
		$sql .= "PAYMENT_PRINTING_QUOTA ";
		$sql .= $condition;
		
		//        echo "sql [".$sql."]<br>";
		return $objDB->db_db_query($sql);
	}
                
	function Cancel_Cash_Deposit($TransactionID) {
		global $Lang, $UserID, $sys_custom;
		
		// lock tables first		
		/*$sql = "LOCK TABLES
		       PAYMENT_CREDIT_TRANSACTION READ
		       ,INTRANET_USER as b READ
		       ,PAYMENT_ACCOUNT as WRITE
		       ,PAYMENT_OVERALL_TRANSACTION_LOG as WRITE";
		$this->db_db_query($sql);*/
		
		$sql = 'Select 
					a.StudentID, 
					a.Amount, 
					b.balance, 
					a.TransactionID, 
					a.RefCode, 
					c.LogID
				From 
					PAYMENT_CREDIT_TRANSACTION as a
					INNER JOIN 
					PAYMENT_ACCOUNT as b
					on 
					a.TransactionID in ('.implode(',',IntegerSafe((array)$TransactionID)).') and a.StudentID = b.StudentID 
					LEFT OUTER JOIN 
					PAYMENT_OVERALL_TRANSACTION_LOG as c 
					on 
					(a.TransactionID = c.RelatedTransactionID and c.TransactionType=9) 
				order by a.TransactionID ';
		$DepositAmount = $this->returnArray($sql,6);
		/*echo $sql.'<Br>';
		echo '<pre>';
		var_dump($DepositAmount);
		echo '</pre>';
		die;*/
		
		$Result = array();
		
		for ($i= 0; $i< sizeof($DepositAmount); $i++) {
			list($StudentID,$Amount,$Balance,$TransactionID,$RefCode,$LogID) = $DepositAmount[$i];
			// if cancel record has already added, the skip it, prevent case that user double click on cancel button
			if (trim($LogID) == "") {
				//if ($BalanceAfter[$StudentID] > 0)
				//	$Balance = $BalanceAfter[$StudentID];
				
				$sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID='$StudentID'";
				$balance_record = $this->returnVector($sql);
				if(count($balance_record)>0){
					$current_balance = $balance_record[0];
				}else{
					$Result['NoBalanceRecord-UserID-'.$StudentID.'-TransactionID-'.$TransactionID] = false;
					continue;
				}
				if($current_balance < $Amount && !$sys_custom['ePayment']['AllowNegativeBalance']){
					$Result['NotEnoughBalance-UserID-'.$StudentID.'-TransactionID-'.$TransactionID] = false;
					continue;
				}
				
				$after_balance = round($current_balance-$Amount,LIBPAYMENT_AMOUNT_DECIMAIL);
				
				$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
			       			(
			       			StudentID, 
			       			TransactionType, 
			       			Amount, 
			       			RelatedTransactionID, 
			       			BalanceAfter, 
			       			TransactionTime, 
			       			Details, 
			       			RefCode,
			       			InputBy
			       			)
			       		VALUES
			       			(
			       			'".$this->Get_Safe_Sql_Query($StudentID)."', 
			       			9,
			       			'".$this->Get_Safe_Sql_Query($Amount)."',
			       			'".$this->Get_Safe_Sql_Query($TransactionID)."',
			       			'".$this->Get_Safe_Sql_Query($after_balance)."',
			       			NOW(),
			       			'".$Lang['Payment']['CancelDepositDescription']."',
			       			'".$this->Get_Safe_Sql_Query($RefCode)."',
			       			'".$this->Get_Safe_Sql_Query($UserID)."'
			       			)";
				$Result['InsertDetailRecord-UserID:'.$StudentID] = $this->db_db_query($sql);
				//echo $sql.'<br>';
				
				# 3. Set balance
				$sql = "UPDATE PAYMENT_ACCOUNT SET 
									Balance = '".$this->Get_Safe_Sql_Query($after_balance)."', 
									LastUpdateByAdmin = '".$this->Get_Safe_Sql_Query($UserID)."',
									LastUpdateByTerminal = NULL, 
									LastUpdated = NOW() 
								WHERE StudentID = '".$this->Get_Safe_Sql_Query($StudentID)."' ";
				$Result['InsertDetailRecord-UserID:'.$StudentID] = $this->db_db_query($sql);
				
				//$BalanceAfter[$StudentID] = ($Balance-$Amount);
			}
		}

		/*$sql = "UNLOCK TABLES";
		$this->db_db_query($sql);*/
		
		return !in_array(false,$Result);

  }
  
  function Make_Donation($StudentID) {
		global $Lang, $UserID;
		# 1. Get current balance
		$sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$StudentID'";
		$temp = $this->returnVector($sql);
		$balance = $temp[0];
		if ($balance == "" || $balance == 0)
		{
		   return true;
		}
		else {
			# 2. Insert to transaction log
			$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
			       (StudentID, TransactionType, Amount, BalanceAfter, TransactionTime, Details, InputBy)
			       VALUES
			       ('$StudentID', 10,'$balance','0',NOW(),'".$Lang['Payment']['DonateBalanceDescription']."', '$UserID')";
			$Result['InsertTransactionLog'] = $this->db_db_query($sql);
			
			# 2a. Auto add RefCode for donation
			$this_TransactionID = $this->db_insert_id();
			$sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RefCode = CONCAT('DON', $this_TransactionID) WHERE LogID='$this_TransactionID'";
			$this->db_db_query($sql);
			
			# 3. Set balance to 0
			$sql = "UPDATE PAYMENT_ACCOUNT SET Balance = 0, LastUpdateByAdmin = '$UserID',
			                      LastUpdateByTerminal = NULL, LastUpdated = NOW() WHERE StudentID = '$StudentID'";
			$Result['UpdateAccountBalance'] = $this->db_db_query($sql);
			
			return !in_array(false,$Result);
		}
	}
	
	function Get_Next_Receipt_Number() {
		$sql = 'select 
							CASE 
								WHEN ReceiptNumber IS NULL THEN \'0001\' 
								WHEN LENGTH(ReceiptNumber) = 1 THEN CONCAT(\'000\',ReceiptNumber) 
								WHEN LENGTH(ReceiptNumber) = 2 THEN CONCAT(\'00\',ReceiptNumber) 
								WHEN LENGTH(ReceiptNumber) = 3 THEN CONCAT(\'0\',ReceiptNumber) 
								ELSE ReceiptNumber 
							END as NextReceiptNumber
						from 
							PAYMENT_RECEIPT_COUNT 
						where 
							AcademicYear = \''.GET_ACADEMIC_YEAR().'\'';
		// debug_r($sql);
		$Result = $this->returnVector($sql);
		
		if ($Result[0] == "") 
			$Return = '0001';
		else 
			$Return = $Result[0];
		return $Return;
	}
	
	function Save_Receipt_Number($NextReceiptNumber) {
		$sql = 'select 
							count(1) 
						from 
							PAYMENT_RECEIPT_COUNT 
						where 
							AcademicYear = \''.GET_ACADEMIC_YEAR().'\'';
		$Result = $this->returnVector($sql);
		
		if ($Result[0] > 0) {
			$sql = 'update PAYMENT_RECEIPT_COUNT set 
								ReceiptNumber = \''.$NextReceiptNumber.'\' 
							where 
								AcademicYear = \''.GET_ACADEMIC_YEAR().'\'';
			return $this->db_db_query($sql);
		}
		else {
			$sql = 'insert into PAYMENT_RECEIPT_COUNT (
								AcademicYear,
								ReceiptNumber
								)
							values (
								\''.GET_ACADEMIC_YEAR().'\',
								\''.$NextReceiptNumber.'\' 
								) 
							';
			return $this->db_db_query($sql);
		}
	}
	
	// eNotice - ePayment Integration <-- added by kenneth chung on 20100201
	// start of Payment ITem API
	function Create_Notice_Payment_Item($ItemName, $StartDate, $EndDate, $NoticeID, $CategroyID, $KeyValues=array()) {
		$more_fields = '';
		$more_values = '';
		if(count($KeyValues)>0){
			foreach($KeyValues as $key => $val){
				$more_fields .= ','.$key;
				$more_values .= ',\''.$this->Get_Safe_Sql_Query($val).'\'';
			}
		}
		$sql = 'Insert into PAYMENT_PAYMENT_ITEM (
							CatID,
							Name,
							StartDate,
							EndDate,
							NoticeID,
							RecordStatus,
							DateInput '.$more_fields.'
						) 
						values (
							\''.$CategroyID.'\',
							\''.$this->Get_Safe_Sql_Query($ItemName).'\',
							\''.$StartDate.'\',
							\''.$EndDate.'\',
							\''.$NoticeID.'\', 
							\'0\',
							NOW() '.$more_values.'
						)';
		if ($this->db_db_query($sql)) {
			return $this->db_insert_id();
		}
		else 
			return false;
	}
	
	function Edit_Notice_Payment_Item($PaymentItemID, $ItemName, $StartDate, $EndDate, $CategoryID, $KeyValues=array()) {
		$more_fields = '';
		if(count($KeyValues)>0){
			foreach($KeyValues as $key => $val){
				$more_fields .= ','.$key.'=\''.$this->Get_Safe_Sql_Query($val).'\' ';
			}
		}
		$sql = 'update PAYMENT_PAYMENT_ITEM set 
							CatID = \''.$CategoryID.'\',
							Name = \''.$this->Get_Safe_Sql_Query($ItemName).'\',
							StartDate = \''.$StartDate.'\',
							EndDate = \''.$EndDate.'\',
							DateModified = NOW() '.$more_fields.'
						where 
							ItemID = \''.$PaymentItemID.'\'';
		//debug_r($sql);
		return $this->db_db_query($sql);
	}
	
	function Remove_Notice_Payment_Item($PaymentItemID) {
		global $i_Payment_action_cancel_payment, $sys_custom;
		
		$sql = 'select 
							ppi.Name,
							ppis.StudentID, 
              ppis.Amount, 
              a.Balance 
						From 
							PAYMENT_PAYMENT_ITEM as ppi 
							inner join 
							PAYMENT_PAYMENT_ITEMSTUDENT as ppis 
							on 
								ppi.ItemID = \''.$PaymentItemID.'\' 
								and 
								ppi.ItemID = ppis.ItemID 
							inner join 
							PAYMENT_ACCOUNT a 
							on 
								ppis.StudentID = a.StudentID 
								and  
                ppis.RecordStatus = 1 
           ';
    $StudentInvolved = $this->returnArray($sql);
		
		for ($i=0; $i< sizeof($StudentInvolved); $i++) {
			$Result['UnpaidStudent:'.$StudentInvolved[$i]['StudentID']] = $this->UnPaid_Payment_Item($PaymentItemID,$StudentInvolved[$i]['StudentID']);
		}
		
		$sql = 'delete from PAYMENT_PAYMENT_ITEMSTUDENT 
						where 
							ItemID = \''.$PaymentItemID.'\'';
		$Result['DeletePaymentItemStudent'] = $this->db_db_query($sql);
		
		$sql = 'delete from PAYMENT_PAYMENT_ITEM 
						where 
							ItemID = \''.$PaymentItemID.'\'';
		$Result['DeletePaymentItem'] = $this->db_db_query($sql);

		if($sys_custom['ePayment']['MultiPaymentGateway']) {
			$sql = 'delete from PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY 
						where 
							ItemID = \'' . $PaymentItemID . '\'';
			$Result['DeletePaymentItemPaymentGateway'] = $this->db_db_query($sql);
		}
		//debug_r($sql);
		return !in_array(false,$Result);
	}
	// end of Payment Item API
		
	// start of Item Student API (After signing the notice)
	// return 
	function Process_Notice_Payment_Item($PaymentItemID, $StudentID, $Amount, $DebitMethod, $AllowNegativeBalance=false, $DoNotDeductBalance=false) {
		$sql = 'select 
							ItemID,
							StudentID,
							Amount,
							RecordStatus,
							PaymentID
						from 
							PAYMENT_PAYMENT_ITEMSTUDENT 
						where 
							ItemID = \''.$PaymentItemID.'\' 
							and 
							StudentID = \''.$StudentID.'\'';
		$RecordExists = $this->returnArray($sql);
		
		$sql = 'select 
							Name 
						from 
							PAYMENT_PAYMENT_ITEM 
						where 
							ItemId = \''.$PaymentItemID.'\'';
		$Temp = $this->returnVector($sql);
		$ItemName = $Temp[0];
		
		$sql = 'select 
							balance 
						from 
							PAYMENT_ACCOUNT 
						where 
							StudentID = \''.$StudentID.'\'';
		$Temp = $this->returnVector($sql);
		$Balance = $Temp[0];
		
		if (sizeof($RecordExists) == 0) { // create new student relation to item
			$sql = 'insert into PAYMENT_PAYMENT_ITEMSTUDENT (
								ItemID,
								StudentID,
								Amount,
								RecordType,
								RecordStatus,
								DateInput,
								DateModified
							)
							values (
								\''.$PaymentItemID.'\',
								\''.$StudentID.'\',
								\''.$Amount.'\',
								\'0\',
								\'0\',
								NOW(),
								NOW()
							)';
			$Result['InsertItemStudent'] = $this->db_db_query($sql);
			$thisPaymentId = $this->db_insert_id();
			
			if ($DebitMethod == '1') { // debit now
				if (!$this->Paid_Payment_Item($PaymentItemID,$StudentID,$thisPaymentId,$AllowNegativeBalance,false,'',$DoNotDeductBalance) && !in_array(false,$Result)) { //not enough balance, will debit later
					return -1;
				}
				else {
					//debug_r($Result);
					return !in_array(false,$Result);
				}
			}
			else {
				return !in_array(false,$Result);
			}
		}
		else { // edit existing student relation to item
			if ($Amount != $RecordExists[0]['Amount']) { // amount changed
				if ($RecordExists[0]['RecordStatus'] == '1') { // Paid already
					$Result['UnpaidStudent'] = $this->UnPaid_Payment_Item($PaymentItemID,$StudentID,$DoNotDeductBalance);
				}
				
				$sql = 'update PAYMENT_PAYMENT_ITEMSTUDENT set 
									Amount = \''.$Amount.'\' 
								where 
									ItemID = \''.$PaymentItemID.'\' 
									and 
									StudentID = \''.$StudentID.'\'';
				$Result['UpdateAmount'] = $this->db_db_query($sql);
				
				if ($DebitMethod == '1') {
					$Result['PaidStudent'] = $this->Paid_Payment_Item($PaymentItemID,$StudentID,$RecordExists[0]['PaymentID'],$AllowNegativeBalance,false,'',$DoNotDeductBalance);
					
					if (!$Result['PaidStudent'] && !in_array(false,$Result)) 
						return -1;
				}
				
				//debug_r($Result);
				return !in_array(false,$Result);
			}
			else { // amount not changed
				if ($RecordExists[0]['RecordStatus'] == '0' && $DebitMethod == '1') { // currently unpay and now changed to debit now
					if (!$this->Paid_Payment_Item($PaymentItemID,$StudentID,$RecordExists[0]['PaymentID'],$AllowNegativeBalance,false,'',$DoNotDeductBalance)) 
						return -1;
					else 
						return true;
				}
				else 
					return true;
			}
		}
	}
	// end of Item Student API 
	// End of eNotice - ePayment Integration
	
	function Paid_Payment_Item($ItemID,$StudentID,$PaymentID='',$AllowNegativeBalance=false,$ReturnTransactionLogID=false,$PaymentTime='',$DoNotDeductBalance=false) {
		global $UserID, $sys_custom;
		
		$payment_time_val = $PaymentTime != ''? "'".date("Y-m-d H:i:s",strtotime($PaymentTime))."'" : 'NOW()';
		
		$sql = 'select 
							ppis.ItemID,
							ppi.Name,
							ppis.StudentID,
							ppis.Amount,
							pa.Balance, 
							ppis.RecordStatus ';
		if($sys_custom['ePayment']['PaymentItemChangeLog']){
			$name_field = getNameFieldWithClassNumberByLang("u.");
			$sql .= ','.$name_field.' as StudentName ';
		}
		$sql .= ' from 
							PAYMENT_PAYMENT_ITEM ppi 
							inner join 
							PAYMENT_PAYMENT_ITEMSTUDENT ppis 
							on ppi.ItemID = ppis.ItemID 
								and ppi.ItemID = \''.$ItemID.'\' 
								and ppis.StudentID = \''.$StudentID.'\' 
							inner join 
							PAYMENT_ACCOUNT pa 
							on 
								ppis.StudentID = pa.StudentID';
		if($sys_custom['ePayment']['PaymentItemChangeLog']){
			$sql .= ' inner join INTRANET_USER as u ON u.UserID=ppis.StudentID ';	
		}
		$RecordExists = $this->returnResultSet($sql);
		
		if ($RecordExists[0]['RecordStatus'] == '0') { // normal
			$Balance = $RecordExists[0]['Balance'];
			$Amount = $RecordExists[0]['Amount'];
			$ItemName = $RecordExists[0]['Name'];
			
			if ($Balance >= $Amount || $AllowNegativeBalance) {
				if($DoNotDeductBalance){
					$balanceAfter = $Balance;
				}else{
					$balanceAfter = $Balance - $Amount;
					$sql = "UPDATE PAYMENT_ACCOUNT SET 
								Balance = '".$balanceAfter."',
				        LastUpdateByAdmin = '".$UserID."',
				        LastUpdateByTerminal = NULL, 
				        LastUpdated = NOW() 
				      WHERE 
				        StudentID = '".$StudentID."'";
					$Result['Update:PAYMENT_ACCOUNT'] = $this->db_db_query($sql);
				}
				$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG (
									StudentID, 
									TransactionType, 
									Amount, 
									RelatedTransactionID, 
									BalanceAfter, 
									TransactionTime, 
									Details,
									InputBy)
		            VALUES (
		             	'".$StudentID."', 
		             	2,
		             	'".$Amount."',
		             	'".($PaymentID=="" ? $ItemID : $PaymentID)."',
		             	'".$balanceAfter."',
		             	$payment_time_val,
		             	'".$this->Get_Safe_Sql_Query($ItemName)."',
		             	'$UserID')";
				$Result['Insert:PAYMENT_OVERALL_TRANSACTION_LOG'] = $this->db_db_query($sql);
				$logID = $this->db_insert_id();
				$sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RefCode = CONCAT('PAY',LogID) WHERE LogID = '$logID'";
				$Result['Update:PAYMENT_OVERALL_TRANSACTION_LOG'] = $this->db_db_query($sql);
				
				$sql = 'update PAYMENT_PAYMENT_ITEMSTUDENT set 
									RecordStatus = \'1\', 
									PaidTime = '.$payment_time_val.', 
									DateModified = NOW(),
									ProcessingAdminUser=\''.$_SESSION['UserID'].'\' 
								where 
									ItemID = \''.$ItemID.'\' 
									and 
									StudentID = \''.$StudentID.'\'';
				$Result['SetStudentToPaid'] = $this->db_db_query($sql);
				
				if($sys_custom['ePayment']['PaymentItemChangeLog']){
					$logDisplayDetail = '';
					$logHiddenDetail = '';
					for($i=0;$i<count($RecordExists);$i++){
						$logDisplayDetail .= 'Item: '.$RecordExists[$i]['Name'].' Student: '.$RecordExists[$i]['StudentName'].' Balance: $'.$RecordExists[$i]['Balance'].' Amount: $'.$RecordExists[$i]['Amount'] .'<br />';
						$delim = '';
						foreach($RecordExists[$i] as $key => $val){
							$logHiddenDetail .= $delim.$key.': '.$val;
							$delim = ', ';
						}
						$logHiddenDetail .= '<br />';
					}
					$logType = $this->getPaymentItemChangeLogType("Pay");
					$this->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);	
				}
				
				//debug_r($Result);
				return !in_array(false,$Result)?($ReturnTransactionLogID?$logID:true):false;
			}
			else {
				return false;
			}
		}
		else { // previously paid already
			return true;
		}
	}
	
	function UnPaid_Payment_Item($ItemID,$StudentID,$DoNotChangeBalance=false) {
		global $i_Payment_action_cancel_payment, $UserID, $sys_custom;
		
		$sql = 'select 
							ppis.ItemID,
							ppi.Name,
							ppis.StudentID,
							ppis.Amount,
							pa.Balance, 
							ppis.RecordStatus ';
		if($sys_custom['ePayment']['PaymentItemChangeLog']){
			$name_field = getNameFieldWithClassNumberByLang("u.");
			$sql .= ','.$name_field.' as StudentName ';
		}
		$sql.= ' from 
							PAYMENT_PAYMENT_ITEM ppi 
							inner join 
							PAYMENT_PAYMENT_ITEMSTUDENT ppis 
							on ppi.ItemID = ppis.ItemID 
								and ppi.ItemID = \''.$ItemID.'\' 
								and ppis.StudentID = \''.$StudentID.'\' 
							inner join 
							PAYMENT_ACCOUNT pa 
							on 
								ppis.StudentID = pa.StudentID ';
		if($sys_custom['ePayment']['PaymentItemChangeLog']){
			$sql .= ' inner join INTRANET_USER as u ON u.UserID=ppis.StudentID ';	
		}
		$RecordExists = $this->returnResultSet($sql);
		
		if ($RecordExists[0]['RecordStatus'] == '1') {
			if($DoNotChangeBalance){
				$balanceAfter = $RecordExists[0]['Balance'];
			}else{
				$balanceAfter = $RecordExists[0]['Balance'] + $RecordExists[0]['Amount'];
					
				$sql = "UPDATE PAYMENT_ACCOUNT SET 
									Balance = '".$balanceAfter."',
					        LastUpdateByAdmin = '".$UserID."',
					        LastUpdateByTerminal = NULL, 
					        LastUpdated = NOW() 
					      WHERE 
					        StudentID = '".$StudentID."'";
				$Result['Update:PAYMENT_ACCOUNT'] = $this->db_db_query($sql);
			}
			$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG (
								StudentID, 
								TransactionType, 
								Amount, 
								RelatedTransactionID, 
								BalanceAfter, 
								TransactionTime, 
								Details,
								InputBy)
	            VALUES (
	             	'".$StudentID."', 
	             	6,
	             	'".$RecordExists[0]['Amount']."',
	             	'".$ItemID."',
	             	'".$balanceAfter."',
	             	NOW(),
	             	'".$i_Payment_action_cancel_payment." ".$this->Get_Safe_Sql_Query($RecordExists[0]['Name'])."',
	             	'$UserID')";
			$Result['Insert:PAYMENT_OVERALL_TRANSACTION_LOG'] = $this->db_db_query($sql);
			$logID = $this->db_insert_id();
			$sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RefCode = CONCAT('PAY',LogID) WHERE LogID = $logID";
			$Result['Update:PAYMENT_OVERALL_TRANSACTION_LOG'] = $this->db_db_query($sql);
			
			$sql = 'update PAYMENT_PAYMENT_ITEMSTUDENT set 
								RecordStatus = \'0\', 
								PaidTime = NULL, 
								DateModified = NOW() 
							where 
								ItemID = \''.$ItemID.'\' 
								and 
								StudentID = \''.$StudentID.'\'';
			$Result['ResetUnPaidStatus'] = $this->db_db_query($sql);
			
			if($sys_custom['ePayment']['PaymentItemChangeLog']){
				$logDisplayDetail = '';
				$logHiddenDetail = '';
				for($i=0;$i<count($RecordExists);$i++){
					$logDisplayDetail .= 'Item: '.$RecordExists[$i]['Name'].' Student: '.$RecordExists[$i]['StudentName'].' Balance: $'.$RecordExists[$i]['Balance'].' Amount: $'.$RecordExists[$i]['Amount'] .'<br />';
					$delim = '';
					foreach($RecordExists[$i] as $key => $val){
						$logHiddenDetail .= $delim.$key.': '.$val;
						$delim = ', ';
					}
					$logHiddenDetail .= '<br />';
				}
				$logType = $this->getPaymentItemChangeLogType("UndoPay");
				$this->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);	
			}
			
			//debug_r($Result);
			return !in_array(false,$Result);
		}
		else { // unpaid already
			return true;
		}
	}
	
	
	 function getReceiptStudentSelectByClass($class,$tags,$selected="",$recordStatus="0,1,2")
       {
                global $button_select, $button_select_all;
                global $i_ClubsEnrollment_StatusWaiting,$i_ClubsEnrollment_StatusRejected,$i_ClubsEnrollment_StatusApproved;
                
                $list = array();
                            
				$name_field = getNameFieldByLang("a.");
       			$sql = "SELECT 
							a.UserID, 
							CONCAT(
								$name_field, 
								IF(b.ClassNumber IS NOT NULL,
									CONCAT(
										' (', 
										".Get_Lang_Selection('c.ClassTitleB5', 'c.ClassTitleEN').", 
										'-', 
										b.ClassNumber,
										')'
									), 
								'')) as ClassNumber 
 						FROM 
 							INTRANET_USER as a 
 							LEFT OUTER JOIN YEAR_CLASS_USER as b on (a.UserID=b.UserID) 
 							LEFT OUTER JOIN YEAR_CLASS c on (c.YearClassID=b.YearClassID)
 							left join INTRANET_USER_PERSONAL_SETTINGS as d on (d.UserID=a.UserID)
                        WHERE 
                        	a.RecordType = 2 AND 
                        	a.RecordStatus IN (". $recordStatus .") AND 
                        	(c.ClassTitleEN='$class' OR c.ClassTitleB5='$class') AND 
                        	c.AcademicYearID=".Get_Current_Academic_Year_ID()." 
                        	and (d.ePayment_NoNeedReceipt = 0 or d.ePayment_NoNeedReceipt is null)
						ORDER BY b.ClassNumber";
                $list = $this->returnArray($sql,4);
                
                $x = "<SELECT $tags>\n";
                $empty_selected = ($selected == '')? "SELECTED":"";
                
                for ($i=0; $i<sizeof($list); $i++)
                {
                     list($id,$name) = $list[$i];
                     if(!is_array($selected)){
                     	$sel_str = ($selected == $id? "SELECTED":"");
                     }
                     else{
                     	$sel_str = "";
                     	foreach($selected as $tempSelected){
                     		if($tempSelected == $id){
                     			$sel_str = "SELECTED";
                     			break;
                     		}
                     	}
                     }
                     $x .= "<OPTION value=$id $sel_str>$name</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }
	
	/*
	 * @Return bool : true=success, false=fail
	 */
//	function Process_Printer_Transactions()
//	{
//		$sql = "SELECT * FROM RICOH_TRANS_SUMMARY WHERE RecordStatus='0' ORDER BY RicohUserID,TransactionTime";
//		$transRecords = $this->returnResultSet($sql);
//		$transRecords_count = count($transRecords);
//		
//		if($transRecords_count==0){
//			return true;
//		}
//		
//		$sql = "SELECT DISTINCT RicohUserID FROM RICOH_TRANS_SUMMARY WHERE RecordStatus='0'";
//		$userLoginAry = $this->returnVector($sql);
//		
//		$sql = "SELECT 
//					u.UserLogin, a.StudentID, a.Balance 
//				FROM INTRANET_USER as u 
//				INNER JOIN PAYMENT_ACCOUNT as a ON a.StudentID=u.UserID
//				WHERE u.UserLogin IN ('".implode("','",$userLoginAry)."')";
//
//		$balanceRecords = $this->returnResultSet($sql);
//		$balanceRecords_count = count($balanceRecords);
//		
//		$userloginToBalance = array();
//		$userloginToUserId = array();
//		for($i=0;$i<$balanceRecords_count;$i++) {
//			$userloginToBalance[$balanceRecords[$i]['UserLogin']] = $balanceRecords[$i]['Balance'];
//			$userloginToUserId[$balanceRecords[$i]['UserLogin']] = $balanceRecords[$i]['StudentID'];
//		}
//		
//		$inputBy = isset($_SESSION['UserID'])? $_SESSION['UserID'] : 'NULL';
//		
//		$details = "Ricoh Printer Payment";
//		$refCodePrefix = "BATCHID";
//		$resultAry = array();
//		for($i=0;$i<$transRecords_count;$i++) {
//			
//			$user_id = $userloginToUserId[$transRecords[$i]['UserLogin']];
//			$balance = $userloginToUserId[$transRecords[$i]['UserLogin']] + 0.0;			
//			$amount = $transRecords[$i]['Amount'] + 0.0;
//			$transaction_time = $transRecords[$i]['TransactionTime'];
//			$batch_id = $transRecords[$i]['BatchID'];
//			
//			if(($balance - $amount) > 0.001){
//				
//				$balance_after = $balance - $amount;
//				$ref_code = $refCodePrefix.$batch_id;
//				
//				$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG (StudentID,TransactionType,Amount,BalanceAfter,TransactionTime,Details,RefCode,InputBy) 
//						VALUES ('$user_id','3','$amount','$balance_after','$transaction_time','$details','$ref_code',$inputBy)";
//				$resultAry['UserID'.$user_id.'_BatchID'.$batch_id] = $this->db_db_query($sql);
//				if($resultAry['UserID'.$user_id.'_BatchID'.$batch_id]){
//					$sql = "UPDATE PAYMENT_ACCOUNT SET Balance='$balance_after' WHERE StudentID='$user_id'";
//					$this->db_db_query($sql);
//					$userloginToBalance[$transRecords[$i]['UserLogin']] = $balance_after;
//				}
//			}
//		}
//		
//		return !in_array(false,$resultAry);
//	}		
	
	/*
	 * @Return bool : true=success, false=fail
	 */
	function Process_Printer_Transactions()
	{
		$sql = "SELECT * FROM RICOH_TRANS_SUMMARY WHERE RecordStatus='0' AND IPUserID IS NOT NULL ORDER BY RicohUserID,ApiDate";
		$transRecords = $this->returnResultSet($sql);
		$transRecords_count = count($transRecords);
		
		if($transRecords_count==0){
			return true;
		}

		$sql = "SELECT DISTINCT IPUserID FROM RICOH_TRANS_SUMMARY WHERE RecordStatus='0' AND IPUserID IS NOT NULL";
		$userIdAry = $this->returnVector($sql);
		
		$sql = "SELECT 
					u.UserID, u.UserLogin, a.StudentID, a.Balance 
				FROM INTRANET_USER as u 
				INNER JOIN PAYMENT_ACCOUNT as a ON a.StudentID=u.UserID
				WHERE u.UserID IN (".implode(",",$userIdAry).")";
//error_log("\n\npayment user sql -->". $sql."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
		$balanceRecords = $this->returnResultSet($sql);
		$balanceRecords_count = count($balanceRecords);
//error_log("\n\nbalance record -->". var_export($balanceRecords,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");		
		$userIdToBalance = array();
		//$userloginToUserId = array();
		for($i=0;$i<$balanceRecords_count;$i++) {
			$userIdToBalance[$balanceRecords[$i]['UserID']] = $balanceRecords[$i]['Balance'];
		//	$userloginToUserId[$balanceRecords[$i]['UserID']] = $balanceRecords[$i]['StudentID'];
		}
//error_log("\n\nuserloginToBalance -->". var_export($userIdToBalance,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
//error_log("\n\nuserloginToUserId -->". var_export($userloginToUserId,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");		
		$inputBy = isset($_SESSION['UserID'])? $_SESSION['UserID'] : 'NULL';
		
		$details = "Ricoh Printer Payment";
		$refCodePrefix = "BATCHID";
		$resultAry = array();
		for($i=0;$i<$transRecords_count;$i++) {
			$user_id = $transRecords[$i]['IPUserID'];
//			$user_id = $userloginToUserId[$transRecords[$i]['IPUserID']];
//			$balance = $userloginToUserId[$transRecords[$i]['UserLogin']] + 0.0;
			$balance = $userIdToBalance[$user_id] + 0.0;
			$amount = $transRecords[$i]['Amount'] + 0.0;
//			$transaction_time = $transRecords[$i]['ApiDate'];
			$batch_id = $transRecords[$i]['BatchID'];
//error_log("\n\nuser_id = $user_id and amount = $amount and balance = $balance <--".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
//			if(($balance - $amount) > 0.001){			
			if($balance >= $amount){
				
				
				$balance_after = $balance - $amount;
				$ref_code = $refCodePrefix.$batch_id;
				
				$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG (StudentID,TransactionType,Amount,BalanceAfter,TransactionTime,Details,RefCode,InputBy) 
						VALUES ('$user_id','3','$amount','$balance_after',NOW(),'$details','$ref_code',$inputBy)";
				$resultAry['UserID'.$user_id.'_BatchID'.$batch_id] = $this->db_db_query($sql);				
				$logID = $this->db_insert_id();
//error_log("\n\npayment log sql -->". $sql."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");				
				if($resultAry['UserID'.$user_id.'_BatchID'.$batch_id]){
					$sql = "UPDATE PAYMENT_ACCOUNT SET Balance='$balance_after' WHERE StudentID='$user_id'";
					$updated_result = $this->db_db_query($sql);
//error_log("\n\npayment account sql -->". $sql."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");					
					$userIdToBalance[$user_id] = $balance_after;
					// Set record status to Paid
					if ($updated_result)
					{
						$sql = "UPDATE RICOH_TRANS_SUMMARY SET RecordStatus=1, IPPaymentLogID='$logID' WHERE BatchID='$batch_id' AND IPUserID='$user_id'";
						$this->db_db_query($sql);
					}
					else
					{
						error_log("\n\nUpdate RICOH_TRANS_SUMMARY Status fail -->".var_export($sql,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ricoh2_error.txt");						
					}
				}
			}
		}
		
		return !in_array(false,$resultAry);
	}		
	
	function getUnsettledPaymentItemsByStudents($studentIdAry)
	{
		$name_field = getNameFieldWithClassNumberByLang("u.");
		
		$sql = "SELECT 
					u.UserID,
					$name_field as StudentName,
					u.ClassName,
					u.ClassNumber,
					c.CatID,
					c.Name as CategoryName,
					b.ItemID,
					b.Name as ItemName,
					a.Amount,
					b.Description,
					DATE_FORMAT(b.StartDate,'%Y-%m-%d %H:%i:%s') as StartDate,
					DATE_FORMAT(b.EndDate,'%Y-%m-%d %H:%i:%s') as EndDate  
				FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
				INNER JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
				INNER JOIN PAYMENT_PAYMENT_ITEM as b ON b.ItemID=a.ItemID 
				LEFT JOIN PAYMENT_PAYMENT_CATEGORY as c ON c.CatID=b.CatID 
				WHERE a.StudentID IN ('".implode("','",(array)$studentIdAry)."') AND PaidTime IS NULL AND (a.RecordStatus=0 OR a.RecordStatus IS NULL) 
				ORDER BY u.ClassName,u.ClassNumber+0,c.DisplayOrder,b.DisplayOrder ";
		//debug_r($sql);
		$records = $this->returnArray($sql);
		return $records;
	}
	
	function getPaymentMethodSelection($data, $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="")
	{
		global $Lang, $sys_custom;

		if(sizeof($data)==0){
			$data = array();
			foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key_val => $display_val){
				if(isset($sys_custom['ePayment_NotAllowEditPaymentMethodItems']) && is_array($sys_custom['ePayment_NotAllowEditPaymentMethodItems'])) {
					if(in_array($key_val, $sys_custom['ePayment_NotAllowEditPaymentMethodItems'])) {
						if($selected == $key_val) {
							return $display_val;
						}
						continue;
					}
				}
				$data[] = array($key_val, $display_val);
			}
		}
		$x = getSelectByArray($data, $tags, $selected, $all, $noFirst, $FirstTitle, 2);
		return $x;
	}
	
	function getPaymentItemChangeLogType($key)
	{
		$key = strtoupper($key);
		$val = 0;
		switch($key)
		{
			case "EDIT": $val = 1;break;
			case "DELETE": $val = 2;break;
			case "ARCHIVE": $val = 3;break;
			case "UNDOARCHIVE": $val = 4;break;
			case "PAYALL": $val = 5;break;
			case "ADDSTUDENT": $val = 6;break;
			case "EDITSTUDENT": $val = 7;break;
			case "DELETESTUDENT": $val = 8;break;
			case "PAY": $val = 9;break;
			case "UNDOPAY" : $val =	10;break;
		}
		return $val;
	}
	
	function getPaymentItemChangeLogTypeArray()
	{
		return array(1,2,3,4,5,6,7,8,9,10);
	}
	
	function logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail)
	{
		global $sys_custom;
		if(!$sys_custom['ePayment']['PaymentItemChangeLog']) return false;
		if(!in_array($logType,$this->getPaymentItemChangeLogTypeArray())) return false;
		
		// remove last extra <br /> tag if presents
		$br_pos = strrpos($logDisplayDetail,'<br />');
		if($br_pos == strlen($logDisplayDetail)-6){
			$logDisplayDetail = substr($logDisplayDetail,0,$br_pos);
		}
		
		// remove last extra <br /> tag if presents
		$br_pos = strrpos($logHiddenDetail,'<br />');
		if($br_pos == strlen($logHiddenDetail)-6){
			$logHiddenDetail = substr($logHiddenDetail,0,$br_pos);
		}
		
		$logDisplayDetail = $this->Get_Safe_Sql_Query($logDisplayDetail);
		$logHiddenDetail = $this->Get_Safe_Sql_Query($logHiddenDetail);
		
		$sql = "INSERT INTO PAYMENT_PAYMENT_ITEM_CHANGE_LOG (LogType,LogDisplayDetail,LogHiddenDetail,DateInput,InputBy) 
				VALUES ('$logType','".$logDisplayDetail."','".$logHiddenDetail."',NOW(),'".$_SESSION['UserID']."')";
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function deletePaymentItemChangeLog($logIdAry)
	{
		global $sys_custom;
		if(!$sys_custom['ePayment']['PaymentItemChangeLog']) return false;
		if(count($logIdAry) == 0) return false;
		
		$sql = "DELETE FROM PAYMENT_PAYMENT_ITEM_CHANGE_LOG WHERE LogID IN ('".implode("','",$logIdAry)."')";
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function iAccountAccountOverview($ClassId, $FromDate, $ToDate, $format=0,$data_format=1,$display_mode=0,$StudentTypesCURRENT=1){

		if($ClassId == ''){
			return false;
		}
		
		global $i_Payment_PresetPaymentItem_PaidCount,$i_Payment_PresetPaymentItem_UnpaidCount,$i_Payment_PresetPaymentItem_NoNeedToPay;
		global $PATH_WRT_ROOT, $intranet_session_language,$sys_custom,$Lang;
		global $lexport,$lclass;
		//define('MAX_COLUMN',256);  // max. column 256 in excel, minus 2 columns ( username , balance)
		
		//$lpayment = new libpayment();
		//$lexport = new libexporttext();
		//$lclass = new libclass();
		
		## get target students data
		$order_student = array();
		$namefield = getNameFieldWithClassNumberByLang("a.");
		$classNamefield = "IF(a.ClassName IS NOT NULL AND a.ClassName<>'' AND a.ClassNumber IS NOT NULL AND a.ClassNumber>=0,a.ClassName, '".$Lang['General']['EmptySymbol']."')";
		$classNumberfield = "IF(a.ClassName IS NOT NULL AND a.ClassName<>'' AND a.ClassNumber IS NOT NULL AND a.ClassNumber>=0,a.ClassNumber, '".$Lang['General']['EmptySymbol']."')";
		
		if($display_mode!=1){
		
			$namefield = getNameFieldByLang("a.");
		
			if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
				$archive_namefield = "a.ChineseName";
			}else $archive_namefield = "a.EnglishName";
		}else{
			$break = $format==1?" / ":"<BR>";
			$namefield = "CONCAT(IF(a.ClassName IS NOT NULL AND a.ClassName<>'',a.ClassName,''), IF(a.ClassNumber IS NOT NULL AND a.ClassNumber>=0,CONCAT('$break',a.ClassNumber, '$break'),''), ".getNameFieldByLang("a.").")";
			
			if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
				$archive_namefield = "IF(a.ClassNumber IS NOT NULL AND a.ClassNumber>=0,CONCAT(a.ClassName,'$break',a.ClassNumber,'$break',a.ChineseName),a.ChineseName)";
			}else $archive_namefield = "IF(a.ClassNumber IS NOT NULL AND a.ClassNumber>=0,CONCAT(a.ClassName,'$break',a.ClassNumber,'$break',a.EnglishName),a.EnglishName)";
			
		
		}
		
		$ClassName = $lclass->getClassName($ClassId);
		$tempClassName = '"'.$ClassName.'"';
		
		if ($StudentTypesCURRENT)
		{
		
			$sql="SELECT a.UserID, $namefield as DisplayName, $classNamefield, $classNumberfield, b.Balance FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID) LEFT OUTER JOIN YEAR_CLASS_USER as c ON (a.UserID = c.UserID) LEFT OUTER JOIN YEAR_CLASS as d ON (c.YearClassID = d.YearClassID) WHERE a.ClassName IN ($tempClassName) AND  a.RecordType=2 ";
			$sql .= " and a.RecordStatus in (0,1,2) ";
			$sql .= " ORDER BY a.ClassName, a.ClassNumber, DisplayName";
			
			$temp = $this->returnArray($sql,3);
		}
		
		for($i=0;$i<sizeof($temp);$i++){
			list($student_id,$student_name,$class_name,$class_number,$balance) = $temp[$i];
			$student_data[$student_id]['Name'] = $student_name;
			$student_data[$student_id]['ClassName'] = $class_name;
			$student_data[$student_id]['ClassNumber'] = $class_number;
			$student_data[$student_id]['Balance'] = $balance;
			$student_data[$student_id]['archived']=0;
			if(!in_array($student_id,$order_student))
				$order_student[] = $student_id;
		}
		
		$list = implode(",",$order_student);
		
		$sql="
			SELECT
				c.StudentID,
				if(d.NoticeID is null, d.Name, concat(e.Title, ' - ' ,d.Name)),
				d.DefaultAmount,
				c.ItemID,
				IF(c.RecordStatus=1,'$i_Payment_PresetPaymentItem_PaidCount','$i_Payment_PresetPaymentItem_UnpaidCount'),
				IF(c.RecordStatus=1,c.Amount,0)
			FROM 
				PAYMENT_PAYMENT_ITEMSTUDENT AS c LEFT OUTER JOIN
				PAYMENT_PAYMENT_ITEM AS d ON (c.ItemID = d.ItemID )
				left join INTRANET_NOTICE as e on e.NoticeID=d.NoticeID
			WHERE 
				c.StudentID IN ($list) AND 
				d.StartDate<='$ToDate' AND 
				d.EndDate>='$FromDate'
			ORDER BY
				d.StartDate
		";
		
		$temp = $this->returnArray($sql,5);
		
		$order_item = array();
		for($i=0;$i<sizeof($temp);$i++){
			list($student_id,$item_name,$default_amount,$item_id,$paid_status,$paid_amount) = $temp[$i];
			$payment_item[$item_id]['Name'] = $item_name;
			$payment_item[$item_id]['DefaultAmount'] = $default_amount;
			$student_data[$student_id][$item_id]['PaidStatus'] = $paid_status;
			$student_data[$student_id][$item_id]['PaidAmount'] = $paid_amount;
			$student_total_amount[$student_id] += $paid_amount;
			
			if(!in_array($item_id,$order_item))
				$order_item[] = $item_id;
		}
		
		############### Add value data [start]
		$add_value_data = array();
		$total_add_value = array();
		$payment_method_type = array();
		$sql = "select 
					a.StudentID,
					a.Amount,
					a.RecordType,
					a.PPSType,
					b.Amount
				from 
					PAYMENT_CREDIT_TRANSACTION as a
					left join PAYMENT_OVERALL_TRANSACTION_LOG as b on (b.RelatedTransactionID=a.TransactionID and b.TransactionType=8)
				where 
					a.StudentID IN ($list) AND 
					a.TransactionTime >='$FromDate' and a.TransactionTime <='$ToDate' 
				";
		$temp = $this->returnArray($sql);
		for($i=0;$i<sizeof($temp);$i++){
			list($student_id,$add_amount,$add_recordtype,$pps_type,$charge) = $temp[$i];
			if($add_recordtype==1 && $pps_type==0)
			{
				$add_value_data[$student_id][1] += $add_amount;
				$total_add_value[$student_id] += $add_amount;
				$add_value_data_charge[$student_id][1] += $charge;
				$payment_method_type[1]=1;
				$student_total_amount[$student_id]+=$charge;
				
			}
			else if($add_recordtype==1 && $pps_type==1)
			{
				$add_value_data[$student_id][2] += $add_amount;
				$total_add_value[$student_id] += $add_amount;
				$add_value_data_charge[$student_id][2] += $charge;
				$payment_method_type[2]=1;
				$student_total_amount[$student_id]+=$charge;
			}
			else if($add_recordtype==2)
			{
				$add_value_data[$student_id][3] += $add_amount;
				$total_add_value[$student_id] += $add_amount;
				$payment_method_type[3]=1;
			}
			else
			{
				$add_value_data[$student_id][4] += $add_amount;
				$total_add_value[$student_id] += $add_amount;
				$payment_method_type[4]=1;
			}
		}
		############### Add value data [end]
		
		############## Built Display Table / CSV content #############
		
		$header="<table width=90% border=0 cellpadding=0 cellspacing=0 align=center>
			<Tr><Td class='$css_text'><B>".$Lang['General']['Class']."</b>: $ClassName</td></tr>
			<tr><Td class='$css_text'><B>".$Lang['General']['Date']."</b>: $FromDate <span class='$css_text'><b>".$Lang['General']['To']."</b></span> $ToDate</td></tr>
			<tr><Td>&nbsp;</td></tr>
		";
		
		if($data_format==1)
		{
			$header .="
				<tr><td class='$css_text'><font style='font-family:Symbol;'>&Ouml;</font> : ".$Lang['General']['Paid']." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;X : ".$Lang['General']['NotPaid']." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- : ".$Lang['General']['NoNeedPaid']."</td></tr>
				<tr><Td>&nbsp;</td></tr>
			";
		}
		$header .="</table>";		
		
		$csv="\"".$Lang['General']['Class']."\"\t\"$ClassName\"\r\n";
		$csv.="\"".$Lang['General']['Date']."\"\t\"$FromDate ".$Lang['General']['To']." $ToDate\"\r\n";
	
		if($display_mode!=1)		# Y: Student Name / X: Item Name
		{ 
		
			$table="<table class='common_table_list'>";
			
			$csv .="\"".$Lang['General']['Class']."\"\t\"".$Lang['StudentAttendance']['ClassNumber']."\"\t\"".$Lang['StudentAttendance']['StudentName']."\"\t\"".$Lang['ePayment']['CurrentAccountBalance']."\"\t";
				
			$num_item = sizeof($order_item);
			if($num_item >0){
			$itemWidth = (60 / $num_item).'%';
			}
			
			for($i=0;$i< $num_item;$i++){
				$item_id = $order_item[$i];
				$item_name = $payment_item[$item_id]['Name'];
				$default_amount = $payment_item[$item_id]['DefaultAmount'];
				$str_default_amount = $this->getWebDisplayAmountFormat($default_amount);
				$payment_item_table.="<th class=\"tablebluetop tabletopnolink\" width=\"$itemWidth\" style='vertical-align:bottom;horizontal-align:center;text-align: center;'>$item_name<Br>$str_default_amount</th>";
				$csv.="\"$item_name ".$this->getExportAmountFormat($default_amount)."\"";
				if($i< $num_item-1)
					$csv .="\t";
			}
			
			if($num_item == 0){
				$table.="<tr >
							<th width=15% class=\"tablebluetop tabletopnolink\">".$Lang['StudentAttendance']['Class']."</th>
							<th width=15% class=\"tablebluetop tabletopnolink\">".$Lang['StudentAttendance']['ClassNumber']."</th>
							<th width=45% class=\"tablebluetop tabletopnolink\">".$Lang['StudentAttendance']['StudentName']."</th>";
				$table.="<th width=25% class=\"sub_row_top\" style='vertical-align:bottom;horizontal-align:center;text-align: center;'>". $Lang['ePayment']['CurrentAccountBalance'] ."</th>";
				$table.="</tr>";			
			}
			else{
				$table.="<tr >
							<th width=5% class=\"tablebluetop tabletopnolink\">".$Lang['StudentAttendance']['Class']."</th>
							<th width=5% class=\"tablebluetop tabletopnolink\">".$Lang['StudentAttendance']['ClassNumber']."</th>
							<th width=20% class=\"tablebluetop tabletopnolink\">".$Lang['StudentAttendance']['StudentName']."</th>";
				$table.="<th width=10% class=\"sub_row_top\" style='vertical-align:bottom;horizontal-align:center;text-align: center;'>". $Lang['ePayment']['CurrentAccountBalance'] ."</th>";
				$table .= $payment_item_table;
				$table.="</tr>";
			}
			
			$csv.="\r\n";
			
			# table content
			for($i=0;$i<sizeof($order_student);$i++){
				$student_id   = $order_student[$i];
				$student_name = $student_data[$student_id]['Name'];
				$balance      = $student_data[$student_id]['Balance'];
				$str_balance  = $this->getWebDisplayAmountFormat($balance);
				
				$css = $i%2==0?"tablebluerow1":"tablebluerow2";
		
				
				$table.="<tr class='$css'><td class='$css'>".$student_data[$student_id]['ClassName']."</td><td class='$css'>".$student_data[$student_id]['ClassNumber']."</td><td class='$css'>".$student_name."</td>";
				$table.="<td class='roster_off'>$str_balance</td>";
				
				$csv .="\"".$student_data[$student_id]['ClassName']."\"\t\"".$student_data[$student_id]['ClassNumber']."\"\t\"".$student_name."\"\t\"".$this->getExportAmountFormat($balance)."\"\t";
						
				for($j=0;$j<sizeof($order_item);$j++){
					$item_id = $order_item[$j];
					$paid_status  = $student_data[$student_id][$item_id]['PaidStatus'];
					
					if($paid_status == "")
					{
						 $paid_status = $i_Payment_PresetPaymentItem_NoNeedToPay; 
						 $str_paid_status = $paid_status;
					}
					else
					{
						if($data_format==1)
						{
							if($paid_status == $i_Payment_PresetPaymentItem_PaidCount)
								$str_paid_status ="<font style='font-family:Symbol;'>&Ouml;</font>";
							else if($paid_status == $i_Payment_PresetPaymentItem_UnpaidCount)
								$str_paid_status ="X";		
						}
						else
						{
							$str_paid_status = $this->getWebDisplayAmountFormat($student_data[$student_id][$item_id]['PaidAmount']);
						}
					}
					
					$table.="<td align=center class='$css'>$str_paid_status</td>";	
					if($data_format==1)
					{
						$csv .="\"$paid_status\"";
					}
					else
					{
						$csv .="\"". $student_data[$student_id][$item_id]['PaidAmount'] ."\"";
					}
					if($j<sizeof($order_item)-1)
						$csv.="\t";
				
				}
				$table.="</tr>";
				$csv.="\r\n";
			}
			$table.="</table>";
			$display = $header.$table;
		}
	
		if($format!=1){
			
			$result .=  $display;
			
		}else{
			$filename = "class_payment_report_".$ClassName.".csv";
			$lexport->EXPORT_FILE($filename, $csv);		
		}
		
		return $result;
	}
	
	// Remeber current using report date range values for navigating through different reports
	function Set_Report_Date_Range_Cookies($StartDate, $EndDate, $Expire=0)
	{
		$result_startdate = setcookie("ck_epayment_report_start_date", $StartDate, $Expire, '/home/eAdmin/GeneralMgmt/payment', '', 0);
		$result_enddate = setcookie("ck_epayment_report_end_date", $EndDate, $Expire, '/home/eAdmin/GeneralMgmt/payment', '', 0);
		
		setcookie("ck_epos_report_start_date", $StartDate, $Expire, '/home/eAdmin/GeneralMgmt/pos', '', 0);
		setcookie("ck_epos_report_end_date", $EndDate, $Expire, '/home/eAdmin/GeneralMgmt/pos', '', 0);
		
		return $result_startdate && $result_enddate;
	}
	
	function Get_Report_Date_Range_Cookies()
	{
		$start_date = $_COOKIE['ck_epayment_report_start_date'];
		$end_date = $_COOKIE['ck_epayment_report_end_date'];
		
		return array('StartDate'=>$start_date, 'EndDate'=>$end_date);
	}
	
	// calculate the check digit from the incomplete barcode without the check digit
	function calculateEPSBarcodeCheckDigit($barcode_without_checkdigit)
	{
		$length = strlen($barcode_without_checkdigit);
		$sum = 0;
		$weight = 0; // repeat pattern 1,2,1,2,1,2 ...
		// from rightmost digit to left 
		for($i=$length-1;$i>=0;$i-=1){
			$weight = ($weight % 2 + 1);
			$d = substr($barcode_without_checkdigit,$i,1);
			$number = intval($d);
			$product = $number * $weight;
			$digit = $product > 9? $product - 9 : $product;
			$sum += $digit;
			//echo sprintf("%s X %s = %s, %s<br>\n", $number, $weight, $product, $digit);
		}
		
		if( $sum % 10 == 0) return '0';
		return strval(10 - $sum % 10);
	}
	
	function getEPSBarcodeNumber($bill_account_number, $bill_amount='', $fixed_prefix='9999', $merchant_code='659', $bill_type='00')
	{
		$barcode_without_checkdigit = $fixed_prefix.$merchant_code.$bill_type.$bill_account_number.$bill_amount;
		$check_digit = $this->calculateEPSBarcodeCheckDigit($barcode_without_checkdigit);
		return $barcode_without_checkdigit.''.$check_digit;
	}
	
	// get the barcode image on the fly
	function getEPSBarcodeUrl($eps_barcode)
	{
		$url = "http://www.barcodelabelhk.com/barcode-generator/html/image.php?code=code128&o=2&t=50&r=2&text=".$eps_barcode."&f=5&a1=A&a2=";
		return $url;
	}
	
	// download the barcode image
	function getEPSBarcodeImage($eps_barcode, $output_format='img', $save_file_location='')
	{
		global $intranet_root;
		
		$ch = curl_init();
		$url = $this->getEPSBarcodeUrl($eps_barcode);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$file_content = curl_exec($ch);
		curl_close($ch);
		
		$output_format = strtolower($output_format);
		if($output_format == 'img')
		{
			if($save_file_location != ''){
				 $fd = fopen($save_file_location,"w");
				 if($fd){
				 	fputs($fd, $file_content);
				 	fclose($fd);
				 }
				 $http_url = str_replace($intranet_root,'',$save_file_location);
				 $img = '<img src="'.$http_url.'" />';
			}else{
				$base64_data = base64_encode($file_content);
				$src = 'data:image/jpeg;base64,'.$base64_data;
				$img = '<img src="'.$src.'" />';
			}
			return $img;
		}else if($output_format == 'browser'){
			$last_slash = strrpos($save_file_location,'/');
			$file_name = substr($save_file_location,$last_slash+1);
			header("Pragma: public");
			header("Expires: 0"); // set expiration time
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-type: image/jpeg"); 
			header("Content-Length: ".strlen($file_content));
			header('Content-Disposition: attachment; filename="' . $file_name . '"');
			echo $file_content;
		}else if($output_format == 'file'){
			$fd = fopen($save_file_location,"w");
			 if($fd){
			 	fputs($fd, $file_content);
			 	fclose($fd);
			 }
			 return $save_file_location;
		}
	}
	
	function getNegativeBalanceRecordsByDateRange($params)
	{
		$from_date = $params['FromDate'];
		$from_datetime = date("Y-m-d 00:00:00",strtotime($from_date));
		$to_date = $params['ToDate'];
		$to_datetime = date("Y-m-d 23:59:59", strtotime($to_date));
		$student_id_ary = (array)$params['StudentID'];
		$record_type_ary = (array)$params['RecordType'];
		$include_negative_records = in_array('1',$record_type_ary);
		$include_generated_records = in_array('2',$record_type_ary);
		
		$student_filter = '';
		if(count($student_id_ary)>0){
			$student_filter = " AND t.StudentID IN ('".implode("','",$student_id_ary)."') ";
		}
		
		$name_field = getNameFieldByLang2("u.");
		
		$return_maps = array();
		// get existing generated billing records
		if($include_generated_records){
			$generated_records = $this->getElectronicBillingRecords($student_id_ary, $from_date, $to_date);
			$generated_record_count = count($generated_records);
			for($i=0;$i<$generated_record_count;$i++){
				$t_student_id = $generated_records[$i]['UserID'];
				if(!isset($return_maps[$t_student_id])){
					$return_maps[$t_student_id] = array('Transactions'=>array(), 'Billings'=>array(),'SubsidyItems'=>array());
				}
				$return_maps[$t_student_id]['Billings'][] = $generated_records[$i];
			}
		}
		
		// find negative balance students
		if($include_negative_records)
		{
			$sql = "SELECT 
						t.LogID,
						t.StudentID,
						t.TransactionType,
						IF(s.PaymentID IS NOT NULL AND s.SubsidyAmount IS NOT NULL,t.Amount+s.SubsidyAmount,t.Amount) as Amount,
						t.BalanceAfter,
						t.TransactionTime,
						CONCAT(IF(t.TransactionDetails IS NOT NULL,t.TransactionDetails,t.Details),IF(t.TransactionType='1' AND LEFT(t.RefCode,3)<>'CSH',CONCAT(' (',t.RefCode,')'),'')) as Details,
						u.ClassName,
						u.ClassNumber,
						u.EnglishName,
						u.ChineseName,
						REPLACE(u.WebSAMSRegNo,'#','') as RegNo  
					FROM PAYMENT_OVERALL_TRANSACTION_LOG as t 
					INNER JOIN INTRANET_USER as u ON u.UserID=t.StudentID 
					LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON t.TransactionType='2' AND t.RelatedTransactionID=s.PaymentID AND t.StudentID=s.StudentID 
					WHERE u.RecordStatus='1' AND u.RecordType='2' AND (t.TransactionTime BETWEEN '$from_datetime' AND '$to_datetime') $student_filter
					ORDER by t.StudentID,t.TransactionTime,t.LogID ";
			//debug_r($sql);
			$records = $this->returnResultSet($sql);
			$count = count($records);
			$maps = array();
			for($i=0;$i<$count;$i++){
				$t_student_id = $records[$i]['StudentID'];
				if(!isset($maps[$t_student_id])){
					$maps[$t_student_id] = array();
				}
				$maps[$t_student_id][] = $records[$i];
			}
		
			if(count($maps)>0){
				foreach($maps as $sid => $trans_ary)
				{
					$trans_count = count($trans_ary);
					$last_balance = $trans_ary[$trans_count-1]['BalanceAfter'];
					if($last_balance < 0.0){ // check the last transaction and only get the negative balance students
						// count subsidy amount
						$subsidy_records = $this->getElectronicBillingSubsidyItems($sid, $from_date, $to_date);
						$subsidy_record_count = count($subsidy_records);
						for($j=0;$j<$subsidy_record_count;$j++)
						{
							$last_balance += $subsidy_records[$j]['SubsidyAmount'];
						}
						if($last_balance < 0.0)
						{
							// get the remaining balance record before those transactions in date range
							$sql = "SELECT t.BalanceAfter as Amount,'$from_datetime' as TransactionTime,'' as Details FROM PAYMENT_OVERALL_TRANSACTION_LOG as t WHERE t.StudentID='$sid' AND t.TransactionTime < '$from_datetime' ORDER BY t.TransactionTime DESC LIMIT 1";
							$remain_balance_ary = $this->returnResultSet($sql);
							$remain_balance_record = count($remain_balance_ary)>0? $remain_balance_ary[0] : array('Amount'=>0,'TransactionTime'=>$from_datetime,'Details'=>'');
							
							array_unshift($trans_ary, $remain_balance_record);
							
							if(!isset($return_maps[$sid])){
								$return_maps[$sid] = array('Transactions'=>array(), 'Billings'=>array(), 'SubsidyItems'=>array());
							}
							$return_maps[$sid]['Transactions'] = $trans_ary;
							if(count($return_maps[$sid]['Billings'])==0){
								$return_maps[$sid]['Billings'] = $this->getElectronicBillingRecords($sid, $from_date, $to_date);
							}
							$return_maps[$sid]['SubsidyItems'] = $subsidy_records;
							$return_maps[$sid]['Balance'] = $last_balance;
						}
					}
				}
			}
		}
		
		return $return_maps;
	}
	
	function getElectronicBillingSubsidyItems($student_id, $from_date, $to_date)
	{
		$from_datetime = date("Y-m-d 00:00:00",strtotime($from_date));
		$to_datetime = date("Y-m-d 23:59:59", strtotime($to_date));
		$sql = "SELECT 
					t.StudentID, t.TransactionTime, m.Name as ItemName, s.SubsidyAmount 
				FROM PAYMENT_OVERALL_TRANSACTION_LOG as t 
				INNER JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.StudentID=t.StudentID AND t.RelatedTransactionID=s.PaymentID 
				INNER JOIN PAYMENT_PAYMENT_ITEM AS m ON m.ItemID= s.ItemID 
				INNER JOIN INTRANET_USER as u ON u.UserID=t.StudentID 
				WHERE ".(is_array($student_id) && count($student_id)>0?" t.StudentID IN ('".implode("','",$student_id)."') ": " t.StudentID='$student_id' ")." 
					AND u.RecordStatus='1' AND u.RecordType='2' AND t.TransactionType='2' AND (t.TransactionTime BETWEEN '$from_datetime' AND '$to_datetime') 
					AND s.PaidTime IS NOT NULL AND s.SubsidyAmount IS NOT NULL AND s.SubsidyAmount > 0 
				ORDER by t.StudentID,t.TransactionTime,t.LogID ";
		$records = $this->returnResultSet($sql);
		//debug_r($sql);
		return $records;
	}
	
	function getElectronicBillingRecords($student_id, $from_date, $to_date)
	{
		$generated_user = getNameFieldByLang2("u.");
		$sql = "SELECT b.*,s.ClassName,s.ClassNumber,s.EnglishName,s.ChineseName,REPLACE(s.WebSAMSRegNo,'#','') as RegNo, $generated_user as GeneratedUser 
				FROM PAYMENT_ELECTRONIC_BILLING as b 
				INNER JOIN INTRANET_USER as s ON s.UserID=b.UserID 
				LEFT JOIN INTRANET_USER as u ON u.UserID=b.GeneratedBy 
				WHERE ".(is_array($student_id) && count($student_id)>0?" b.UserID IN ('".implode("','",$student_id)."') ": " b.UserID='$student_id' ")." AND ((b.FromDate BETWEEN '$from_date' AND '$to_date') OR (b.ToDate BETWEEN '$from_date' AND '$to_date')) 
				ORDER BY b.UserID,b.GeneratedDate";
		$records = $this->returnResultSet($sql);
		return $records;
	}
	
	function getElectronicBillingRecordsByRecordId($recordIdAry)
	{
		$generated_user = getNameFieldByLang2("u.");
		$sql = "SELECT b.*,$generated_user as GeneratedUser 
				FROM PAYMENT_ELECTRONIC_BILLING as b 
				LEFT JOIN INTRANET_USER as u ON u.UserID=b.GeneratedBy 
				WHERE b.RecordID IN ('".implode("','",(array)$recordIdAry)."')";
		$records = $this->returnResultSet($sql);
		return $records;
	}
	
	function getFormattedElectronicBillingNumber($id)
	{
		$billing_number = sprintf("%06s",$id);
		return $billing_number;
	}
	
	function getNextElectronicBillingNumber($date='')
	{
		if($date == ''){
			$date = date("Y-m-d");
		}
		$date_ts = strtotime($date);
		
		$currentAcademicYearId = Get_Current_Academic_Year_ID();
		$yearStart = substr(date('Y',getStartOfAcademicYear($date_ts)), 2);
		$yearEnd = substr(date('Y',getEndOfAcademicYear($date_ts)), 2);
		
		$sql = "SELECT CurrentNumber FROM PAYMENT_ELECTRONIC_BILLING_NUMBER WHERE AcademicYearID='$currentAcademicYearId'";
		$records = $this->returnVector($sql);
		if(count($records)==0){
			$nextNumber = 1;
			$sql = "INSERT INTO PAYMENT_ELECTRONIC_BILLING_NUMBER (AcademicYearID,CurrentNumber) VALUES ('$currentAcademicYearId','$nextNumber')";
			$this->db_db_query($sql);
		}else{
			$nextNumber = $records[0]+1;
			$sql = "UPDATE PAYMENT_ELECTRONIC_BILLING_NUMBER SET CurrentNumber='$nextNumber' WHERE AcademicYearID='$currentAcademicYearId'";
			$this->db_db_query($sql);
		}
		
		$receiptNumber = $yearStart.$yearEnd.$this->getFormattedElectronicBillingNumber($nextNumber);
		return $receiptNumber;
	}
	
	function insertElectronicBillingRecord($student_id, $from_date, $to_date, $billing_number, $balance_when_generated)
	{
		$sql = "INSERT INTO PAYMENT_ELECTRONIC_BILLING (UserID,FromDate,ToDate,BillingNumber,BalanceWhenGenerated,GeneratedBy,GeneratedDate) VALUES ('$student_id','$from_date','$to_date','".$this->Get_Safe_Sql_Query($billing_number)."','".$balance_when_generated."','".$_SESSION['UserID']."',NOW())";
		$result = $this->db_db_query($sql);
		if($result){
			$record_id = $this->db_insert_id();
			$result = $record_id;
			//$bill_number = $this->getFormattedElectronicBillingNumber($record_id);
			//$this->db_db_query("UPDATE PAYMENT_ELECTRONIC_BILLING SET BillingNumber='$bill_number' WHERE RecordID='$record_id'");
		}
		
		return $result;
	}
	
	function updateElectronicBillingRecord($record_id, $paramMap)
	{
		if(count($paramMap)==0) return false;
		
		$sql = "UPDATE PAYMENT_ELECTRONIC_BILLING SET ";
		$i = -1;
		foreach($paramMap as $fieldName => $value)
		{
			$i += 1;
			if($i > 0) $sql .= ",";
			$sql .= " $fieldName='".$this->Get_Safe_Sql_Query($value)."' ";
		}
		$sql .= " WHERE RecordID='$record_id'";
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function deleteElectronicBillingRecord($record_id)
	{
		$sql = "DELETE FROM PAYMENT_ELECTRONIC_BILLING WHERE ";
		if(is_array($record_id) && count($record_id)>0){
			$sql .= " RecordID IN ('".implode("','",$record_id)."')";
		}else{
			$sql.=" RecordID='$record_id'";
		}
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	// [BEGIN] $sys_custom['ePayment']['PaymentItemWithPOS']
	function insertReceiptRecord($paramMap)
	{
		$fields = '';
		$values = '';
		$i = -1;
		foreach($paramMap as $key => $value){
			$i+=1;
			if($i > 0){
				$fields .= ',';
				$values .= ',';
			}
			$fields .= $key;
			$values .= "'".$this->Get_Safe_Sql_Query($value)."'";
		}
		$sql = "INSERT INTO PAYMENT_RECEIPT ($fields) VALUES ($values)";
		$success = $this->db_db_query($sql);
		if($success){
			$record_id = $this->db_insert_id();
			return $record_id;
		}
		return false;
	}
	
	function updateReceiptRecord($recordId, $paramMap)
	{
		if(count($paramMap)==0) return false;
		
		$sql = "UPDATE PAYMENT_RECEIPT SET ";
		$i = -1;
		foreach($paramMap as $fieldName => $value)
		{
			$i += 1;
			if($i > 0) $sql .= ",";
			$sql .= " $fieldName='".$this->Get_Safe_Sql_Query($value)."' ";
		}
		$sql .= " WHERE RecordID='$recordId'";
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function deleteReceiptRecords($recordIdAry)
	{
		if(!is_array($recordIdAry) || count($recordIdAry)==0) return false;
		
		$sql = "DELETE FROM PAYMENT_RECEIPT WHERE RecordID IN ('".implode("','",$recordIdAry)."')";
		return $this->db_db_query($sql);
	}
	
	function getReceiptRecords($recordIdAry)
	{
		$sql = "SELECT * FROM PAYMENT_RECEIPT WHERE RecordID IN ('".implode("','",$recordIdAry)."')";
		return $this->returnResultSet($sql);
	}
	// [END] $sys_custom['ePayment']['PaymentItemWithPOS']
	
	function getCreditMethodArray()
	{
		global $Lang, $sys_custom, $i_Payment_Credit_TypeCashDeposit;
		
		$credit_types = array();
		$credit_types[] = array('2',$i_Payment_Credit_TypeCashDeposit);
		if($sys_custom['ePayment']['CreditMethodAutoPay']){
			$credit_types[] = array('4',$Lang['ePayment']['AutoPay']);
		}else if($sys_custom['ePayment']['CashDepositMethod']){
			$credit_types[] = array('6',$Lang['ePayment']['BankTransfer']);
			$credit_types[] = array('7',$Lang['ePayment']['ChequeDeposit']);
		}
		
		return $credit_types;
	}
	
	function isCashDepositNeedApproval()
	{
		global $intranet_root, $sys_custom;
		
		if($sys_custom['ePayment']['CashDepositApproval']){
			include_once($intranet_root."/includes/libgeneralsettings.php");
			$GeneralSetting = new libgeneralsettings();
			$SettingList[] = "'CashDepositApproval'";
			$Settings = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);
			
			return $Settings['CashDepositApproval'] == 1;
		}
		return false;
	}
	
	function isCashDepositApprovalAdmin($uid='')
	{
		global $intranet_root, $sys_custom;
		
		if($sys_custom['ePayment']['CashDepositApproval']){
			include_once($intranet_root."/includes/libgeneralsettings.php");
			$GeneralSetting = new libgeneralsettings();
			$SettingList[] = "'CashDepositApproval'";
			$Settings = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);
			
			if($Settings['CashDepositApproval'] == 1){
				if($uid == '') $uid = $_SESSION['UserID'];
				$sql = "SELECT 
							m.UserID 
						FROM ACCESS_RIGHT_GROUP_MEMBER as m 
						INNER JOIN ACCESS_RIGHT_GROUP as g ON g.GroupID=m.GroupID 
						WHERE g.GroupTitle='Cash Deposit Approval' AND m.UserID='$uid'";
				$records = $this->returnVector($sql);
				return count($records)>0;
			}
		}
		return false;
	}
	
	// for $sys_custom['ePayment']['HartsPreschool']
	function getStudentPaymentMethodRecords($studentIdAry=array(),$returnAssocAry=0)
	{
		$sql = "SELECT * FROM PAYMENT_STUDENT_PAYMENT_METHOD";
		if(count($studentIdAry)>0){
			$sql .= " WHERE StudentID IN (".implode(",",$studentIdAry).") ";
		}
		$records = $this->returnResultSet($sql);
		
		if($returnAssocAry){
			$studentIdToAry = array();
			$record_size = count($records);
			for($i=0;$i<$record_size;$i++){
				$studentIdToAry[$records[$i]['StudentID']] = $records[$i];
			}
			return $studentIdToAry;
		}else{
			return $records;
		}
	}
	
	function voidTngTransaction($orderNo, $spTxNo)
	{
		global $intranet_root, $sys_custom, $PATH_WRT_ROOT, $eclassAppConfig, $config_school_code;
		
		include_once($intranet_root.'/includes/libeclassapiauth.php');
		include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
		include_once($intranet_root."/includes/eClassApp/libAES.php");
		include_once($intranet_root."/includes/json.php");

		$lapiAuth = new libeclassapiauth();
		$libaes = new libAES($eclassAppConfig['aesKey']);
		$jsonObj = new JSON_obj();
		

		$platformApiKey = $lapiAuth->GetAPIKeyByProject('IP/EJ platform');
		$apiPath = $eclassAppConfig['paymentGateway']['apiPath'];
		
		$requestAry = array();
		$requestAry['RequestMethod'] = 'voidTransaction';
		$requestAry['APIKey'] = $platformApiKey;
		$requestAry['Request']['SchoolCode'] = $config_school_code;
		$requestAry['Request']['OrderNo'] = $orderNo;
		$requestAry['Request']['SpTxNo'] = $spTxNo;
		$requestJsonString = $jsonObj->encode($requestAry);
		
		
		$jsonAry = array();
		$jsonAry['eClassRequestEncrypted'] = $libaes->encrypt($requestJsonString);
		$postJsonString = $jsonObj->encode($jsonAry);
		

		session_write_close();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_URL, $apiPath);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJsonString);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		$responseJson = curl_exec($ch);
		curl_close($ch);

		$json_ary = $jsonObj->decode($responseJson);
		
		return $json_ary;
	}
	
	function getTngMerchantInfo()
	{
		global $intranet_root, $sys_custom, $PATH_WRT_ROOT, $eclassAppConfig, $config_school_code;
		
		include_once($intranet_root.'/includes/global.php');
		include_once($intranet_root.'/includes/libdb.php');
		include_once($intranet_root.'/includes/libeclassapiauth.php');
		include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
		include_once($intranet_root."/includes/eClassApp/libAES.php");
		include_once($intranet_root."/includes/json.php");
		
		$lapiAuth = new libeclassapiauth();
		$libaes = new libAES($eclassAppConfig['aesKey']);
		$jsonObj = new JSON_obj();
		
		$platformApiKey = $lapiAuth->GetAPIKeyByProject('IP/EJ platform');
		$apiPath = $eclassAppConfig['paymentGateway']['apiPath'];
		
		$requestAry = array();
		$requestAry['RequestMethod'] = 'returnMerchantInfo';
		$requestAry['APIKey'] = $platformApiKey;
		$requestAry['Request']['SchoolCode'] = $config_school_code;
		$requestJsonString = $jsonObj->encode($requestAry);
		
		
		$jsonAry = array();
		$jsonAry['eClassRequestEncrypted'] = $libaes->encrypt($requestJsonString);
		$postJsonString = $jsonObj->encode($jsonAry);
		
		
		session_write_close();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_URL, $apiPath);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJsonString);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		$responseJson = curl_exec($ch);
		curl_close($ch);
		
		$json_ary = $jsonObj->decode($responseJson);
		return $json_ary['MethodResult'];
	}

	function getENoticePaymentStatus($requestData)
	{
		global $intranet_root, $sys_custom, $PATH_WRT_ROOT, $eclassAppConfig, $config_school_code;

		include_once($intranet_root.'/includes/libeclassapiauth.php');
		include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
		include_once($intranet_root."/includes/eClassApp/libAES.php");
		include_once($intranet_root."/includes/json.php");

		$lapiAuth = new libeclassapiauth();
		$libaes = new libAES($eclassAppConfig['aesKey']);
		$jsonObj = new JSON_obj();


		$platformApiKey = $lapiAuth->GetAPIKeyByProject('IP/EJ platform');
		$apiPath = $eclassAppConfig['paymentGateway']['apiPath'];

		$school_url = 'http';
		if (checkHttpsWebProtocol()) {$school_url .= "s";}
		$school_url .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$school_url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
		} else {
			$school_url .= $_SERVER["SERVER_NAME"];
		}

		$requestAry = array();
		$requestAry['RequestMethod'] = 'queryPaymentStatus2';
		$requestAry['APIKey'] = $platformApiKey;
		$requestAry['Request']['SchoolCode'] = $config_school_code;
		$requestAry['Request']['SchoolURL'] = $school_url;

		foreach($requestData as $key => $val){
			$requestAry['Request'][$key] = $val;
		}
		$requestJsonString = $jsonObj->encode($requestAry);


		$jsonAry = array();
		$jsonAry['eClassRequestEncrypted'] = $libaes->encrypt($requestJsonString);
		$postJsonString = $jsonObj->encode($jsonAry);


		//session_write_close();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_URL, $apiPath);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJsonString);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		$responseJson = curl_exec($ch);
		//if($responseJson === false){
		//	debug_pr(curl_error($ch));
		//}
		curl_close($ch);


		return $responseJson;
	}

	function getTngQrcode($requestData)
	{
		global $intranet_root, $sys_custom, $PATH_WRT_ROOT, $eclassAppConfig, $config_school_code;
		
		include_once($intranet_root.'/includes/libeclassapiauth.php');
		include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
		include_once($intranet_root."/includes/eClassApp/libAES.php");
		include_once($intranet_root."/includes/json.php");
		
		$lapiAuth = new libeclassapiauth();
		$libaes = new libAES($eclassAppConfig['aesKey']);
		$jsonObj = new JSON_obj();
		
		
		$platformApiKey = $lapiAuth->GetAPIKeyByProject('IP/EJ platform');
		$apiPath = $eclassAppConfig['paymentGateway']['apiPath'];
		
		$school_url = 'http';
		if (checkHttpsWebProtocol()) {$school_url .= "s";}
		$school_url .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
		  	$school_url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
		} else {
		  	$school_url .= $_SERVER["SERVER_NAME"];
		}
		
		$requestAry = array();
		$requestAry['RequestMethod'] = 'requestQrCode';
		$requestAry['APIKey'] = $platformApiKey;
		$requestAry['Request']['SchoolCode'] = $config_school_code;
		$requestAry['Request']['SchoolURL'] = $school_url;
		/*
		 * PaymentID (for Payment Notice, use the first payment id)
		 * PayerUserID
		 * Amount
		 * PaymentTitle
		 * 
		 * NoticeID (for Payment Notice)
		 * NoticePaymentID (for Payment Notice)
		 * NoticeReplyAnswer (for Payment Notice)
		 */
		foreach($requestData as $key => $val){
			$requestAry['Request'][$key] = $val;
		}
		$requestJsonString = $jsonObj->encode($requestAry);
		
		
		$jsonAry = array();
		$jsonAry['eClassRequestEncrypted'] = $libaes->encrypt($requestJsonString);
		$postJsonString = $jsonObj->encode($jsonAry);
		

		session_write_close();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_URL, $apiPath);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJsonString);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		$responseJson = curl_exec($ch);
		//if($responseJson === false){
		//	debug_pr(curl_error($ch));
		//}
		curl_close($ch);
		
		
		return $responseJson;
	}
	
	function getMerchantAccounts($data)
	{
		$conds = "";
		$order_by = "";
		
		if(isset($data['AccountID'])){
			$conds .= " AND AccountID ";
			if(is_array($data['AccountID'])){
				$conds .= " IN (".implode(",",IntegerSafe($data['AccountID'])).") ";
			}else{
				$conds .= "='".IntegerSafe($data['AccountID'])."' ";
			}
		}
		
		if(isset($data['ExcludeAccountID'])){
			$conds .= " AND AccountID ";
			if(is_array($data['ExcludeAccountID'])){
				$conds .= " NOT IN (".implode(",",IntegerSafe($data['ExcludeAccountID'])).") ";
			}else{
				$conds .= "<>'".IntegerSafe($data['ExcludeAccountID'])."' ";
			}
		}
		
		if(isset($data['RecordStatus'])){
			$conds .= " AND RecordStatus ";
			if(is_array($data['RecordStatus'])){
				$conds .= " IN (".implode(",",IntegerSafe($data['RecordStatus'])).") ";
			}else{
				$conds .= "='".IntegerSafe($data['RecordStatus'])."' ";
			}
		}
		
		if(isset($data['ServiceProvider'])){
			$conds .= " AND ServiceProvider='".$this->Get_Safe_Sql_Query($data['ServiceProvider'])."' ";
		}
		
		if(isset($data['AccountName'])){
			$conds .= " AND AccountName='".$this->Get_Safe_Sql_Query($data['AccountName'])."' ";
		}
		
		if(isset($data['Keyword']) && trim($data['Keyword']) != ''){
			$keyword = $this->Get_Safe_Sql_Like_Query(trim($data['Keyword']));
			$conds .= " AND (AccountName LIKE '%$keyword%' OR MerchantAccount LIKE '%$keyword%' OR MerchantUID LIKE '%$keyword%') ";
		}
		
		if(isset($data['order_by_accountname'])){
			$order_by = " ORDER BY AccountName ";
		}
		
		$sql = "SELECT * FROM PAYMENT_MERCHANT_ACCOUNT WHERE 1 ".$conds.$order_by;
		$records = $this->returnResultSet($sql);
		return $records;
	}
	
	function upsertMerchantAccount($map)
	{
		$fields = array('AccountID','ServiceProvider','AccountName','MerchantAccount','MerchantUID','HandlingFee','RecordStatus','AdministrativeFeeFixAmount','AdministrativeFeePercentageAmount');
		$user_id = $_SESSION['UserID'];
		
		if(count($map) == 0) return false;

		if(isset($map['AccountID']) && $map['AccountID']!='' && $map['AccountID'] > 0)
		{
			$sql = "UPDATE PAYMENT_MERCHANT_ACCOUNT SET ";
			$sep = "";
			foreach($map as $key => $val){
				if(!in_array($key, $fields) || $key == 'AccountID') continue;
				
				$sql .= $sep."$key='".$this->Get_Safe_Sql_Query(trim($val))."'";
				$sep = ",";
			}
			$sql.= " ,ModifiedDate=NOW(),ModifiedBy='$user_id' WHERE AccountID='".$map['AccountID']."'";
			$success = $this->db_db_query($sql);
		}else{
			$keys = '';
			$values = '';
			$sep = "";
			foreach($map as $key => $val){
				if(!in_array($key, $fields)) continue;
				$keys .= $sep."$key";
				$values .= $sep."'".$this->Get_Safe_Sql_Query(trim($val))."'";
				$sep = ",";
			}
			$keys .= ",InputDate,ModifiedDate,InputBy,ModifiedBy";
			$values .= ",NOW(),NOW(),'$user_id','$user_id'";
			
			$sql = "INSERT INTO PAYMENT_MERCHANT_ACCOUNT ($keys) VALUES ($values)";
			$success = $this->db_db_query($sql);
		}
		
		return $success;
	}
	
	function deleteMerchantAccount($accountId)
	{
		$sql = "DELETE FROM PAYMENT_MERCHANT_ACCOUNT WHERE AccountID";
		if(is_array($accountId)){
			$sql .= " IN (".implode(",",IntegerSafe($accountId)).")";
		}else{
			$sql .= "='".IntegerSafe($accountId)."'";
		}
		
		return $this->db_db_query($sql);
	}
	
	function getPaymentItemStudentRecords($data)
	{
		global $sys_custom;
		$conds = "";
		$order_by = "";
		
		if(isset($data['PaymentID'])){
			$conds .= " AND s.PaymentID ";
			if(is_array($data['PaymentID'])){
				$conds .= " IN (".implode(",",IntegerSafe($data['PaymentID'])).") ";
			}else{
				$conds .= "='".IntegerSafe($data['PaymentID'])."' ";
			}
		}
		
		if(isset($data['ItemID'])){
			$conds .= " AND s.ItemID ";
			if(is_array($data['ItemID'])){
				$conds .= " IN (".implode(",",IntegerSafe($data['ItemID'])).") ";
			}else{
				$conds .= "='".IntegerSafe($data['ItemID'])."' ";
			}
		}
		
		if(isset($data['StudentID'])){
			$conds .= " AND s.StudentID ";
			if(is_array($data['StudentID'])){
				$conds .= " IN (".implode(",",IntegerSafe($data['StudentID'])).") ";
			}else{
				$conds .= "='".IntegerSafe($data['StudentID'])."' ";
			}
		}
		
		if(isset($data['RecordStatus'])){
			$conds .= " AND s.RecordStatus ";
			if(is_array($data['RecordStatus'])){
				$conds .= " IN (".implode(",",IntegerSafe($data['RecordStatus'])).") ";
			}else{
				$conds .= "='".IntegerSafe($data['RecordStatus'])."' ";
			}
		}
		
		if(isset($data['MerchantAccountID'])){
			$conds .= " AND i.MerchantAccountID ";
			if(is_array($data['MerchantAccountID'])){
				$conds .= " IN (".implode(",",IntegerSafe($data['MerchantAccountID'])).") ";
			}else{
				$conds .= "='".IntegerSafe($data['MerchantAccountID'])."' ";
			}
		}
		
		
		$select_fields = "s.*,i.Name as ItemName,i.StartDate,i.EndDate,i.NoticeID,i.MerchantAccountID ";
		$more_fields = "";
		$join_tables = "";
		
		if(isset($data['select_merchant_info'])){
			if($sys_custom['ePayment']['MultiPaymentGateway'] && isset($data['ServiceProvider'])) {
				$join_tables .= "LEFT JOIN PAYMENT_MERCHANT_ACCOUNT as a ON a.AccountID=
				(SELECT IFNULL(
					(SELECT MerchantAccountID
						FROM PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY pg
						LEFT JOIN PAYMENT_MERCHANT_ACCOUNT as ma ON pg.MerchantAccountID=ma.AccountID
						WHERE ma.ServiceProvider='".$this->Get_Safe_Sql_Query($data['ServiceProvider'])."' AND pg.ItemID=s.ItemID),
					i.MerchantAccountID)
				)";
			} else {
				$join_tables .= " LEFT JOIN PAYMENT_MERCHANT_ACCOUNT as a ON a.AccountID=i.MerchantAccountID ";
			}
			$more_fields .= ",a.AccountID,a.ServiceProvider,a.AccountName,a.MerchantAccount,a.MerchantUID, a.AdministrativeFeeFixAmount, a.AdministrativeFeePercentageAmount ";
			
			if(isset($data['ServiceProvider'])){
				$conds .= " AND a.ServiceProvider='".$this->Get_Safe_Sql_Query($data['ServiceProvider'])."' ";
			}
			
			if(isset($data['AccountName'])){
				$conds .= " AND a.AccountName='".$this->Get_Safe_Sql_Query($data['AccountName'])."' ";
			}
		}
		
		$sql = "SELECT $select_fields $more_fields FROM PAYMENT_PAYMENT_ITEMSTUDENT as s 
				INNER JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=s.ItemID $join_tables 
				WHERE 1 ".$conds.$order_by;
		$records = $this->returnResultSet($sql);
		return $records;
	}
	
	function getPaymentServiceProviderMapping($returnIndexArray=false,$checkAvailability=false)
	{
		global $Lang, $sys_custom;
		if($checkAvailability){
			$map = array();
			if($sys_custom['ePayment']['TNG'] || $sys_custom['ePayment']['TopUpEWallet']['TNG']){
				$map['TNG'] = $Lang['ePayment']['TNG'];
			}
			if($sys_custom['ePayment']['Alipay'] || $sys_custom['ePayment']['TopUpEWallet']['Alipay']){
				$map['ALIPAY'] = $Lang['ePayment']['Alipay'];
			}
			if($sys_custom['ePayment']['FPS'] || $sys_custom['ePayment']['TopUpEWallet']['FPS']){
				$map['FPS'] = $Lang['ePayment']['FPS'];	
			}
			if($sys_custom['ePayment']['TAPANDGO'] || $sys_custom['ePayment']['TopUpEWallet']['TAPANDGO']){
				$map['TAPANDGO'] = $Lang['ePayment']['TapAndGo'];
			}
			if($sys_custom['ePayment']['VisaMaster'] || $sys_custom['ePayment']['TopUpEWallet']['VisaMaster']) {
				$map['VISAMASTER'] = $Lang['ePayment']['VisaMaster'];
			}
			if($sys_custom['ePayment']['WeChat'] || $sys_custom['ePayment']['TopUpEWallet']['WeChat']) {
				$map['WECHAT'] = $Lang['ePayment']['WeChat'];
			}
			if($sys_custom['ePayment']['AlipayCN'] || $sys_custom['ePayment']['TopUpEWallet']['AlipayCN']) {
				$map['ALIPAYCN'] = $Lang['ePayment']['AlipayCN'];
			}
		}else{
			$map = array('TNG'=>$Lang['ePayment']['TNG'],'ALIPAY'=>$Lang['ePayment']['Alipay'],'FPS'=>$Lang['ePayment']['FPS'],'TAPANDGO'=>$Lang['ePayment']['TapAndGo'],'VISAMASTER'=>$Lang['ePayment']['VisaMaster'],'WECHAT'=>$Lang['ePayment']['WeChat'], 'ALIPAYCN'=>$Lang['ePayment']['AlipayCN']);
		}
		if($returnIndexArray){
			$ary = array();
			foreach($map as $key => $val){
				$ary[] = array($key,$val);
			}
			return $ary;
		}
		return $map;
	}
	
	function addValue($Amount, $StudentID, $TransactionTime, $RefCode = '', $RecordType = 2, $transactionId = '')
	{
		global $i_Payment_TransactionDetailCashDeposit, $Lang;
		if(!is_numeric($Amount) || $Amount <= 0.0 || !is_numeric($StudentID) || $StudentID==''){
			return false;
		}
		
		$TransactionTime = trim(rawurldecode($TransactionTime));
		$RefCode = trim(rawurldecode($RefCode));
		
		$Result = array();
		
		$sql = "INSERT INTO PAYMENT_CREDIT_TRANSACTION (
					StudentID, 
					Amount, 
					RecordType, 
					RecordStatus, 
					TransactionTime, 
					RefCode, 
					AdminInCharge,
					DateInput) 
				VALUES (
					'$StudentID', 
					'$Amount', 
					'$RecordType', 
					0, 
					'$TransactionTime', 
					'".$this->Get_Safe_Sql_Query($RefCode)."', 
					'".$_SESSION['UserID']."',
					NOW())";
		$Result['AddCreditTransaction'] = $this->db_db_query($sql);
		
		if(!$Result['AddCreditTransaction']){
			return false;
		}
		
		$creditTransactionId = $this->db_insert_id();
		
		if($transactionId){
			$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET CreditTransactionID = '".$creditTransactionId."' WHERE RecordID='$transactionId'";
			$Result['UpdateTransaction'] = $this->db_db_query($sql);
		}
		
		switch($RecordType){
	       	case 2 : 
	       		$refCodePrefix = "CSH";
	       		$details = $i_Payment_TransactionDetailCashDeposit;
	        	break;
	        case 8 : 
	        	$refCodePrefix = "TNG";
	        	$details = $Lang['ePayment']['TNGTopUp'];
	        	break;
	        case 9 : 
	        	$refCodePrefix = "ALIPAY";
	        	$details = $Lang['ePayment']['AlipayTopUp'];
	        	break;
	        case 10 :
	        	$refCodePrefix = "FPS";
	        	$details = $Lang['ePayment']['FPSTopUp'];
	        	break;
	        case 11 :
	        	$refCodePrefix = "TAPANDGO";
	        	$details = $Lang['ePayment']['TapAndGoTopUp'];
	        	break;
			case 12 :
				$refCodePrefix = "VISAMASTER";
				$details = $Lang['ePayment']['VisaMasterTopUp'];
				break;
			case 13 :
				$refCodePrefix = "WECHAT";
				$details = $Lang['ePayment']['WeChatTopUp'];
				break;
			case 14 :
				$refCodePrefix = "ALIPAYCN";
				$details = $Lang['ePayment']['AlipayCNTopUp'];
				break;
	        default : 
	        	$refCodePrefix = "";
	        	$details = $i_Payment_TransactionDetailCashDeposit;
	        	break;
		}
		
		// only update RefCode if it is empty
		if($RefCode == ''){
			$RefCode = $refCodePrefix.$creditTransactionId;
			$sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET
			               RefCode = '".$this->Get_Safe_Sql_Query($RefCode)."' 
			        WHERE TransactionID='$creditTransactionId' ";
			$Result['UpdateRefCodeIfNotInput'] = $this->db_db_query($sql);
		}
		
		$sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$StudentID'";
	    $temp = $this->returnVector($sql);
	    if(count($temp)==0){
	     	$sql_create_account = "INSERT INTO PAYMENT_ACCOUNT (StudentID,Balance) VALUES ('$StudentID',0)";
	     	$this->db_db_query($sql_create_account);
	    }
	    $balanceAfter = $temp[0]+$Amount;
	    $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = $balanceAfter, LastUpdateByAdmin = '".$_SESSION['UserID']."', LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = '$StudentID'";
		$Result['UpdateAccountBalance'] = $this->db_db_query($sql);
		
		
		$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
		        (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,TransactionTime,Details,RefCode) VALUES 
				('$StudentID',1,'$Amount','$creditTransactionId','$balanceAfter',now(),'$details','". $this->Get_Safe_Sql_Query($RefCode) ."')";
		$Result['InsertOverallTransactionLogs'] = $this->db_db_query($sql);
		
		return !in_array(false,$Result);
	}
	
	function importPrepareAlipayTransactionRecordsToTempTable($data, $cur_sessionid, $threshold_date, $cur_time, $exclude_fields=array(5,8,9,12,13), $sp_name='alipay')
	{
		global $Lang, $i_Payment_TransactionType_Credit;
		
		$data_size = count($data);
		$column_size = count($data[0]);
		
		// Column 10 (counted from 0) : Type - P: Normal Payment, R: Refund 
		// Column 11 (counted from 0): Status - P: Successful Processed, L: Settled to merchant 
		
		$sql = "CREATE TABLE IF NOT EXISTS PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS (
					RecordID int(11) NOT NULL auto_increment,
					SessionID varchar(300) NOT NULL,
					ImportTime datetime NOT NULL,
					RecordNumber int(11),
					PaymentChannel varchar(255),
					PaymentDateTime datetime,
					OrderNo varchar(255),
					TNGRefNo varchar(255),
					PaymentType varchar(255),
					PaymentAmount decimal(13,2),
					RebateAmount decimal(13,2),
					NetPaymentAmount decimal(13,2),
					Remarks TEXT,
					TransactionRecordID int(11) DEFAULT NULL COMMENT 'PAYMENT_TNG_TRANSACTION.RecordID',
					PaymentID int(11) DEFAULT NULL, 
					PRIMARY KEY(RecordID),
					INDEX IdxSessionID(SessionID),
					INDEX IdxImportTime(ImportTime),
					INDEX IdxTNGRefNo(TNGRefNo) 
				)ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$this->db_db_query($sql);
		
		$sql = "ALTER TABLE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS ADD COLUMN QueueNo varchar(255) DEFAULT NULL AFTER OrderNo";
		$this->db_db_query($sql);
		
		$sql = "ALTER TABLE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS ADD COLUMN TNGNo varchar(255) DEFAULT NULL AFTER TNGRefNo";
		$this->db_db_query($sql);
		
		$sql = "ALTER TABLE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS ADD COLUMN SettleStatus varchar(255) DEFAULT NULL AFTER PaymentType";
		$this->db_db_query($sql);
		
		// added on 2019-10-28
		$sql = "ALTER TABLE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS ADD COLUMN SettlementTime datetime DEFAULT NULL AFTER PaymentDateTime";
		$this->db_db_query($sql);
		
		//$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));
		$sql = "DELETE FROM PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS WHERE SessionID='$cur_sessionid' OR ImportTime>'$threshold_date'";
		$this->db_db_query($sql);
		
		$insert_values = array();
		for($i=0;$i<$data_size;$i++){
			
			$insert_row = "('$cur_sessionid','$cur_time'";
			for($j=0;$j<$column_size;$j++){
				if(in_array($j,$exclude_fields)){
					continue;
				}
				$val = trim($data[$i][$j]);
				if($j == 2){
					$val = abs($val);
				}
				if($j == 3){
					$val = 0;
					if($data[$i][2] < 0){
						$val = $data[$i][2];
					}
				}
				if($j == 6 || $j == 7){ // payment date time or settlement time
					if(preg_match('/^(\d{1,2})\/(\d{1,2})\/(\d\d\d\d)\s(\d{1,2}):(\d{1,2})(:\d{1,2})*$/',$val,$matches)){
						$val = sprintf('%04d-%02d-%02d %02d:%02d:%02d', $matches[3], $matches[2], $matches[1], $matches[4], $matches[5], str_replace(':','',$matches[6]));
					}
					if($val != ''){
						$val = date("Y-m-d H:i:s", strtotime($val));
					}else{
						$val = '';
					}
				}else if($j == 2 || $j == 3 || $j == 4){ // dollar amount
					$val = str_replace(array('$','(',')',',','"','-'),'',$val);
				}
				$insert_row .= $val == ''? ",NULL" : ",'".$val."'";
			}
			$insert_row .= ")";
			$insert_values[] = $insert_row;
		}
		
		$insert_results = array();
		$chunks = array_chunk($insert_values,500);
		//$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS (SessionID,ImportTime,RecordNumber,PaymentChannel,PaymentDateTime,OrderNo,TNGRefNo,PaymentType,PaymentAmount,RebateAmount,NetPaymentAmount,Remarks) VALUES ";
		$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS (SessionID,ImportTime,OrderNo,TNGRefNo,PaymentAmount,RebateAmount,NetPaymentAmount,PaymentDateTime,SettlementTime,PaymentType,SettleStatus) VALUES ";
		for($i=0;$i<count($chunks);$i++){
			$insert_sql = $sql.implode(",", $chunks[$i]);
			$insert_results[] = $this->db_db_query($insert_sql);
		//	debug_pr($insert_sql);
		}
		
		// supplementary step to map PaymentID
		$student_name_field = getNameFieldWithClassNumberByLang("u1.");
		//$archived_student_name_field = getNameFieldWithClassNumberByLang("au1.");
		$archived_student_name_field = getNameFieldByLang2("au1.");
		$payer_name_field = getNameFieldByLang2("u2.");
		$archived_payer_name_field = getNameFieldByLang2("au2.");
		$top_up_student_name_field = getNameFieldWithClassNumberByLang("u3.");
		$top_up_archived_student_name_field = getNameFieldByLang2("au3.");
		
		$sql = "SELECT 
					r.RecordID,
					r.OrderNo,
					r.TNGRefNo,
					r.PaymentAmount,
					r.RebateAmount,
					r.NetPaymentAmount,
					r.PaymentDateTime,
					r.SettlementTime,
					r.PaymentType,
					r.SettleStatus,
					t.RecordID as TNGRecordID,
					t.PaymentID as TNGPaymentID,
					s.RecordStatus as PaidStatus,
					IF(t.PaymentID = -1,'".$i_Payment_TransactionType_Credit."',i.Name) as ItemName,
					IF(u1.UserID IS NOT NULL,$student_name_field,IF(au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',$archived_student_name_field), IF(u3.UserID IS NOT NULL,$top_up_student_name_field,CONCAT('<span class=\"red\">*</span>',$top_up_archived_student_name_field)))) as StudentName,
					t.OrderNo as TNGOrderNo,
					t.SpTxNo,
					".$this->getWebDisplayAmountFormatDB("t.NetPaymentAmount")." as TNGPaymentAmount,
					t.PaymentStatus,
					t.ChargeStatus,
					DATE_FORMAT(t.InputDate,'%Y-%m-%d %H:%i:%s') as InputDate,
					IF(u2.UserID IS NULL AND au2.UserID IS NULL,'".$Lang['General']['EmptySymbol']."',IF(u2.UserID IS NOT NULL,$payer_name_field,CONCAT('<span class=\"red\">*</span>',$archived_payer_name_field))) as PayerName 
				FROM PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS as r 
				LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.SpTxNo=r.TNGRefNo AND t.Sp='$sp_name'
				LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID 
				LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=t.PaymentID 
				LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=s.ItemID 
				LEFT JOIN INTRANET_USER as u1 ON u1.UserID=s.StudentID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au1 ON au1.UserID=s.StudentID 
				LEFT JOIN INTRANET_USER as u2 ON u2.UserID=t.PayerUserID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au2 ON au2.UserID=t.PayerUserID 
				LEFT JOIN INTRANET_USER as u3 ON u3.UserID=c.StudentID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au3 ON au3.UserID=c.StudentID 
				WHERE r.SessionID='$cur_sessionid' 
				ORDER BY r.RecordNumber ";
		$records = $this->returnArray($sql);
		$record_size = count($records);
		
		for($i=0;$i<$record_size;$i++)
		{
			if($records[$i]['TNGRecordID'] == ''){
				// no callback TNG transaction record
			}else if(!in_array($records[$i]['PaymentStatus'],array('1',1,'Success','success'))){
				// TNG transaction fail and not added to ePayment yet
				$sql = "UPDATE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS SET TransactionRecordID='".$records[$i]['TNGRecordID']."',PaymentID='".$records[$i]['TNGPaymentID']."' WHERE RecordID='".$records[$i]['RecordID']."'";
				$this->db_db_query($sql);
			}else{
				// the TNG transaction was added to ePayment successfully
				$sql = "UPDATE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS SET TransactionRecordID='".$records[$i]['TNGRecordID']."',PaymentID='".$records[$i]['TNGPaymentID']."' WHERE RecordID='".$records[$i]['RecordID']."'";
				$this->db_db_query($sql);
			}
		}
		
		
		return !in_array(false, $insert_results) && count($insert_results)>0;
	}
	
	function importProcessAlipayTransactionRecords($cur_sessionid, $sp_name='alipay')
	{
		
		$sql = "SELECT 
					r.*,
					t.RecordID as TNGRecordID,
					t.PaymentID as TNGPaymentID,
					t.RefundStatus,
					s.ItemID,
					IF(t.PaymentID=-1,c.StudentID,s.StudentID) as StudentID,
					t.PaymentStatus,
					t.ChargeStatus,
					IF(t.PaymentID=-1,t.PaymentStatus,s.RecordStatus) as RecordStatus,
					c.TransactionID     
				FROM PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS as r 
				LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.SpTxNo=r.TNGRefNo AND t.Sp='$sp_name'
				LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=r.PaymentID 
				LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID AND t.PaymentID=-1 
				WHERE r.SessionID='$cur_sessionid' 
				ORDER BY r.RecordNumber ";
		$records = $this->returnResultSet($sql);
		$record_size = count($records);
		//debug_pr($sql);
		$updated_by = isset($_SESSION['UserID'])? $_SESSION['UserID'] : 'NULL';
		$resultAry = array();
		for($i=0;$i<$record_size;$i++)
		{
			//$parChargeStatus = in_array($records[$i]['PaymentType'],array('1',1,'Success','success'))?'1': (in_array($records[$i]['PaymentType'],array('3',3,'Void','void'))?'3':'0');
			if(in_array($records[$i]['PaymentType'],array('R','4',4))){
				$parChargeStatus = '4'; // refunded
			}else if(in_array($records[$i]['PaymentType'],array('P','1',1,'Success','success')) && in_array($records[$i]['SettleStatus'],array('L'))){
				$parChargeStatus = '1'; // success
			}else if(in_array($records[$i]['PaymentType'],array('P','1',1,'Success','success')) && in_array($records[$i]['SettleStatus'],array('P'))){
				$parChargeStatus = '2'; // pending
			}else if(in_array($records[$i]['PaymentType'],array('3',3,'Void','void'))){
				$parChargeStatus = '3'; // voided
			}else{
				$parChargeStatus = '0'; // fail
			}

			$exists_record = true;
			if($records[$i]['TNGRecordID'] == ''){
				$exists_record = false;
				// insert a PAYMENT_TNG_TRANSACTION record 
				$parFromServer = $records[$i]['PaymentChannel'];
				$payment_id = $records[$i]['PaymentID'];
				$parSp = $sp_name;
				$parPayerUserId = $records[$i]['StudentID'];
				$parSpTxNo = $records[$i]['TNGRefNo'];
				$parTNGNo = $records[$i]['TNGNo'];
				$parPaymentStatus = 0;
				$parRefundStatus = $records[$i]['RefundStatus'];
				$parUniqueCode = $records[$i]['OrderNo'];
				$payment_amount = $records[$i]['PaymentAmount'];
				$rebate_amount = $records[$i]['RebateAmount'];
				$net_payment_amount = $records[$i]['NetPaymentAmount'];
				$settlement_time = $records[$i]['SettlementTime'];
				
				$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION (FromServer,PaymentID,Sp,PayerUserID,PaymentType,PaymentStatus,ChargeStatus,RefundStatus,OrderNo,SpTxNo,TNGNo,PaymentAmount,RebateAmount,NetPaymentAmount,ErrorCode,InputDate,ModifiedDate,UpdatedBy,SettlementTime) 
						VALUES ('$parFromServer','$payment_id','$parSp','$parPayerUserId','','$parPaymentStatus','$parChargeStatus','$parRefundStatus','$parUniqueCode','$parTNGNo','$parSpTxNo','$payment_amount','$rebate_amount','$net_payment_amount',NULL,'".$records[$i]['PaymentDateTime']."',NOW(),$updated_by,'$settlement_time')";
				$success = $this->db_db_query($sql);
				if($success){
					$tng_record_id = $this->db_insert_id();
					$records[$i]['TNGRecordID'] = $tng_record_id;
				}
			}


			if($records[$i]['TNGRecordID'] != '')
			{
				$settlement_time = $records[$i]['SettlementTime'];

				// paid the payment item 
				if($records[$i]['TNGPaymentID']!='' && $records[$i]['RecordStatus']!=1 && $parChargeStatus != '3' && $parChargeStatus != '4'){
					if($exists_record) {
						if ($records[$i]['RefundStatus'] == 'Success') {
							$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET TNGNo='".$records[$i]['TNGNo']."', ModifiedDate=NOW(),SettlementTime='$settlement_time' WHERE RecordID='".$records[$i]['TNGRecordID']."'";
							$resultAry[] = $this->db_db_query($sql);
						}
					} else {
						/*$log_id = 0;
						if ($records[$i]['TNGPaymentID'] > 0) { // pay payment item
							$log_id = $this->Paid_Payment_Item($records[$i]['ItemID'], $records[$i]['StudentID'], $records[$i]['PaymentID'], $___AllowNegativeBalance = true, $___ReturnTransactionLogID = true, '', true);
						} else if ($records[$i]['TNGPaymentID'] == -1) { // top-up
							if ($records[$i]['TransactionID'] == '') { // no credit transaction id
								$log_id = $this->addValue($net_payment_amount, $records[$i]['StudentID'], date("Y-m-d H:i:s"), '', 9, $records[$i]['TNGRecordID']);
							}
						}*/
						$log_id = 1;
						$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET PaymentStatus='" . ($log_id ? '1' : '0') . "',ChargeStatus='$parChargeStatus',TNGNo='" . $records[$i]['TNGNo'] . "',ModifiedDate=NOW(),SettlementTime='$settlement_time' WHERE RecordID='" . $records[$i]['TNGRecordID'] . "'";
						$resultAry[] = $this->db_db_query($sql);
					}
				}else if($records[$i]['TNGPaymentID'] != '' && ($parChargeStatus == '4' || $parChargeStatus == '3')) // refunded or voided
				{
					/*if($records[$i]['RecordStatus']==1) // paid, then void
					{
						if($records[$i]['TNGPaymentID'] > 0){ // undo payment item
							$this->UnPaid_Payment_Item($records[$i]['ItemID'],$records[$i]['StudentID'],$DoNotChangeBalance=true);
						}else if($records[$i]['TNGPaymentID'] == -1 && $records[$i]['TransactionID'] != ''){ // undo Top-up
							$this->Cancel_Cash_Deposit($records[$i]['TransactionID']);
						}
					}*/
					$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET ChargeStatus='$parChargeStatus',TNGNo='".$records[$i]['TNGNo']."',ModifiedDate=NOW(),SettlementTime='$settlement_time' WHERE RecordID='".$records[$i]['TNGRecordID']."'";
					$resultAry[] = $this->db_db_query($sql);
				}else if($parChargeStatus == '4'){ // refunded
					$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET RefundStatus='$parRefundStatus',TNGNo='".$records[$i]['TNGNo']."',ModifiedDate=NOW(),SettlementTime='$settlement_time' WHERE RecordID='".$records[$i]['TNGRecordID']."'";
					$resultAry[] = $this->db_db_query($sql);
				}
				else if($records[$i]['TNGPaymentID']!='' && $records[$i]['RecordStatus']==1 /*&& $records[$i]['PaymentStatus']!=1*/){
					$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET PaymentStatus='1',ChargeStatus='$parChargeStatus',TNGNo='".$records[$i]['TNGNo']."',ModifiedDate=NOW(),SettlementTime='$settlement_time' WHERE RecordID='".$records[$i]['TNGRecordID']."'";
					$resultAry[] = $this->db_db_query($sql);
				}
			}
		}
		
		return $resultAry;
	}

		function importPrepareWechatTransactionRecordsToTempTable($data, $cur_sessionid, $threshold_date, $cur_time)
		{
			global $Lang, $i_Payment_TransactionType_Credit;

			$data_size = count($data);
			$column_size = count($data[0]);

			$sql = "CREATE TABLE IF NOT EXISTS PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS (
					RecordID int(11) NOT NULL auto_increment,
					SessionID varchar(300) NOT NULL,
					ImportTime datetime NOT NULL,
					RecordNumber int(11),
					PaymentChannel varchar(255),
					PaymentDateTime datetime,
					OrderNo varchar(255),
					TNGRefNo varchar(255),
					PaymentType varchar(255),
					PaymentAmount decimal(13,2),
					RebateAmount decimal(13,2),
					NetPaymentAmount decimal(13,2),
					Remarks TEXT,
					TransactionRecordID int(11) DEFAULT NULL COMMENT 'PAYMENT_TNG_TRANSACTION.RecordID',
					PaymentID int(11) DEFAULT NULL, 
					PRIMARY KEY(RecordID),
					INDEX IdxSessionID(SessionID),
					INDEX IdxImportTime(ImportTime),
					INDEX IdxTNGRefNo(TNGRefNo) 
				)ENGINE=InnoDB DEFAULT CHARSET=utf8";

			$this->db_db_query($sql);

			$sql = "ALTER TABLE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS ADD COLUMN QueueNo varchar(255) DEFAULT NULL AFTER OrderNo";
			$this->db_db_query($sql);

			$sql = "ALTER TABLE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS ADD COLUMN TNGNo varchar(255) DEFAULT NULL AFTER TNGRefNo";
			$this->db_db_query($sql);

			$sql = "ALTER TABLE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS ADD COLUMN SettleStatus varchar(255) DEFAULT NULL AFTER PaymentType";
			$this->db_db_query($sql);

			// added on 2019-10-28
			$sql = "ALTER TABLE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS ADD COLUMN SettlementTime datetime DEFAULT NULL AFTER PaymentDateTime";
			$this->db_db_query($sql);

			//$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));
			$sql = "DELETE FROM PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS WHERE SessionID='$cur_sessionid' OR ImportTime>'$threshold_date'";
			$this->db_db_query($sql);

			$insert_results = array();
			foreach($data as $temp) {
				if($temp['Setllment'] != '1') {
					continue;
				}

				$PaymentType = 'success';
				$OrderNo = $temp['OrderNo'];
				if($temp['RefundOrderNo'] != '') {
					$PaymentType = 'refund';
					$OrderNo = $temp['RefundOrderNo'];
				}

				$target_date = $temp['PayTime'];
				$target_date = substr($target_date,0,4).'-'.substr($target_date,4,2).'-'.substr($target_date,6,2).' '.substr($target_date,8,2).':'.substr($target_date,10,2).':'.substr($target_date,12,2);

				$insert_data = array();
				$insert_data['SessionID'] = $cur_sessionid;
				$insert_data['ImportTime'] = $cur_time;
				$insert_data['OrderNo'] = $temp['MrchtOrderNo'];
				$insert_data['TNGRefNo'] = $OrderNo;
				$amount = $temp['RetReceipts'] / 100;
				$insert_data['PaymentAmount'] = $amount;
				$insert_data['RebateAmount'] = $amount;
				$insert_data['NetPaymentAmount'] = $amount;
				$insert_data['PaymentDateTime'] = $target_date;
				$insert_data['SettlementTime'] = '';
				$insert_data['PaymentType'] = $PaymentType;
				$insert_data['SettleStatus'] = '1';

				$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS SET ";
				$x = '';
				foreach($insert_data as $key=>$insert_value) {
					$sql .= $x.$key."=".($insert_value == ''? "NULL" : "'".$insert_value."'");
					$x = ', ';
				}
				$insert_results[] = $this->db_db_query($sql);
			}

			// supplementary step to map PaymentID
			$student_name_field = getNameFieldWithClassNumberByLang("u1.");
			//$archived_student_name_field = getNameFieldWithClassNumberByLang("au1.");
			$archived_student_name_field = getNameFieldByLang2("au1.");
			$payer_name_field = getNameFieldByLang2("u2.");
			$archived_payer_name_field = getNameFieldByLang2("au2.");
			$top_up_student_name_field = getNameFieldWithClassNumberByLang("u3.");
			$top_up_archived_student_name_field = getNameFieldByLang2("au3.");

			$sql = "SELECT 
					r.RecordID,
					r.OrderNo,
					r.TNGRefNo,
					r.PaymentAmount,
					r.RebateAmount,
					r.NetPaymentAmount,
					r.PaymentDateTime,
					r.SettlementTime,
					r.PaymentType,
					r.SettleStatus,
					t.RecordID as TNGRecordID,
					t.PaymentID as TNGPaymentID,
					s.RecordStatus as PaidStatus,
					IF(t.PaymentID = -1,'".$i_Payment_TransactionType_Credit."',i.Name) as ItemName,
					IF(u1.UserID IS NOT NULL,$student_name_field,IF(au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',$archived_student_name_field), IF(u3.UserID IS NOT NULL,$top_up_student_name_field,CONCAT('<span class=\"red\">*</span>',$top_up_archived_student_name_field)))) as StudentName,
					t.OrderNo as TNGOrderNo,
					t.SpTxNo,
					".$this->getWebDisplayAmountFormatDB("t.NetPaymentAmount")." as TNGPaymentAmount,
					t.PaymentStatus,
					t.ChargeStatus,
					DATE_FORMAT(t.InputDate,'%Y-%m-%d %H:%i:%s') as InputDate,
					IF(u2.UserID IS NULL AND au2.UserID IS NULL,'".$Lang['General']['EmptySymbol']."',IF(u2.UserID IS NOT NULL,$payer_name_field,CONCAT('<span class=\"red\">*</span>',$archived_payer_name_field))) as PayerName 
				FROM PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS as r 
				LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.SpTxNo=r.TNGRefNo AND t.Sp='wechat'
				LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID 
				LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=t.PaymentID 
				LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=s.ItemID 
				LEFT JOIN INTRANET_USER as u1 ON u1.UserID=s.StudentID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au1 ON au1.UserID=s.StudentID 
				LEFT JOIN INTRANET_USER as u2 ON u2.UserID=t.PayerUserID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au2 ON au2.UserID=t.PayerUserID 
				LEFT JOIN INTRANET_USER as u3 ON u3.UserID=c.StudentID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au3 ON au3.UserID=c.StudentID 
				WHERE r.SessionID='$cur_sessionid' 
				ORDER BY r.RecordNumber ";
			$records = $this->returnArray($sql);
			$record_size = count($records);

			for($i=0;$i<$record_size;$i++)
			{
				if($records[$i]['TNGRecordID'] == ''){
					// no callback TNG transaction record
				}else if(!in_array($records[$i]['PaymentStatus'],array('1',1,'Success','success'))){
					// TNG transaction fail and not added to ePayment yet
					$sql = "UPDATE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS SET TransactionRecordID='".$records[$i]['TNGRecordID']."',PaymentID='".$records[$i]['TNGPaymentID']."' WHERE RecordID='".$records[$i]['RecordID']."'";
					$this->db_db_query($sql);
				}else{
					// the TNG transaction was added to ePayment successfully
					$sql = "UPDATE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS SET TransactionRecordID='".$records[$i]['TNGRecordID']."',PaymentID='".$records[$i]['TNGPaymentID']."' WHERE RecordID='".$records[$i]['RecordID']."'";
					$this->db_db_query($sql);
				}
			}


			return !in_array(false, $insert_results) && count($insert_results)>0;
		}

		function importProcessWechatTransactionRecords($cur_sessionid)
		{

			$sql = "SELECT 
					r.*,
					t.RecordID as TNGRecordID,
					t.PaymentID as TNGPaymentID,
					t.RefundStatus,
					s.ItemID,
					IF(t.PaymentID=-1,c.StudentID,s.StudentID) as StudentID,
					t.PaymentStatus,
					t.ChargeStatus,
					IF(t.PaymentID=-1,t.PaymentStatus,s.RecordStatus) as RecordStatus,
					c.TransactionID     
				FROM PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS as r 
				LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.SpTxNo=r.TNGRefNo AND t.Sp='wechat'
				LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=r.PaymentID 
				LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID AND t.PaymentID=-1 
				WHERE r.SessionID='$cur_sessionid' 
				ORDER BY r.RecordNumber ";
			$records = $this->returnResultSet($sql);
			$record_size = count($records);
			//debug_pr($sql);
			$updated_by = isset($_SESSION['UserID'])? $_SESSION['UserID'] : 'NULL';
			$resultAry = array();
			for($i=0;$i<$record_size;$i++)
			{
				//$parChargeStatus = in_array($records[$i]['PaymentType'],array('1',1,'Success','success'))?'1': (in_array($records[$i]['PaymentType'],array('3',3,'Void','void'))?'3':'0');
				if(in_array($records[$i]['PaymentType'],array('refund'))){
					$parChargeStatus = '4'; // refunded
				}else if(in_array($records[$i]['PaymentType'],array('success')) && in_array($records[$i]['SettleStatus'],array('1'))){
					$parChargeStatus = '1'; // success
				}else{
					$parChargeStatus = '0'; // fail
				}

				$exists_record = true;
				if($records[$i]['TNGRecordID'] == ''){
					$exists_record = false;
					// insert a PAYMENT_TNG_TRANSACTION record
					$parFromServer = $records[$i]['PaymentChannel'];
					$payment_id = $records[$i]['PaymentID'];
					$parSp = 'wechat';
					$parPayerUserId = $records[$i]['StudentID'];
					$parSpTxNo = $records[$i]['TNGRefNo'];
					$parTNGNo = $records[$i]['TNGNo'];
					$parPaymentStatus = 0;
					$parRefundStatus = $records[$i]['RefundStatus'];
					$parUniqueCode = $records[$i]['OrderNo'];
					$payment_amount = $records[$i]['PaymentAmount'];
					$rebate_amount = $records[$i]['RebateAmount'];
					$net_payment_amount = $records[$i]['NetPaymentAmount'];
					$settlement_time = $records[$i]['SettlementTime'];

					$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION (FromServer,PaymentID,Sp,PayerUserID,PaymentType,PaymentStatus,ChargeStatus,RefundStatus,OrderNo,SpTxNo,TNGNo,PaymentAmount,RebateAmount,NetPaymentAmount,ErrorCode,InputDate,ModifiedDate,UpdatedBy) 
						VALUES ('$parFromServer','$payment_id','$parSp','$parPayerUserId','','$parPaymentStatus','$parChargeStatus','$parRefundStatus','$parUniqueCode','$parSpTxNo','$parTNGNo','$payment_amount','$rebate_amount','$net_payment_amount',NULL,'".$records[$i]['PaymentDateTime']."',NOW(),$updated_by)";
					$success = $this->db_db_query($sql);
					if($success){
						$tng_record_id = $this->db_insert_id();
						$records[$i]['TNGRecordID'] = $tng_record_id;
					}
				}


				if($records[$i]['TNGRecordID'] != '')
				{
					$settlement_time = $records[$i]['SettlementTime'];

					// paid the payment item
					if($records[$i]['TNGPaymentID']!='' && $records[$i]['RecordStatus']!=1 && $parChargeStatus != '3' && $parChargeStatus != '4'){
						if($exists_record) {
							if ($records[$i]['RefundStatus'] == 'Success') {
								//$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET TNGNo='".$records[$i]['TNGNo']."', ModifiedDate=NOW(),SettlementTime='$settlement_time' WHERE RecordID='".$records[$i]['TNGRecordID']."'";
								//$resultAry[] = $this->db_db_query($sql);
							}
						} else {
							/*$log_id = 0;
							if ($records[$i]['TNGPaymentID'] > 0) { // pay payment item
								$log_id = $this->Paid_Payment_Item($records[$i]['ItemID'], $records[$i]['StudentID'], $records[$i]['PaymentID'], $___AllowNegativeBalance = true, $___ReturnTransactionLogID = true, '', true);
							} else if ($records[$i]['TNGPaymentID'] == -1) { // top-up
								if ($records[$i]['TransactionID'] == '') { // no credit transaction id
									$log_id = $this->addValue($net_payment_amount, $records[$i]['StudentID'], date("Y-m-d H:i:s"), '', 9, $records[$i]['TNGRecordID']);
								}
							}*/
							$log_id = 1;
							$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET PaymentStatus='" . ($log_id ? '1' : '0') . "',ChargeStatus='$parChargeStatus',ModifiedDate=NOW() WHERE RecordID='" . $records[$i]['TNGRecordID'] . "'";
							$resultAry[] = $this->db_db_query($sql);
						}
					}else if($records[$i]['TNGPaymentID'] != '' && ($parChargeStatus == '4' || $parChargeStatus == '3')) // refunded or voided
					{
						/*if($records[$i]['RecordStatus']==1) // paid, then void
						{
							if($records[$i]['TNGPaymentID'] > 0){ // undo payment item
								$this->UnPaid_Payment_Item($records[$i]['ItemID'],$records[$i]['StudentID'],$DoNotChangeBalance=true);
							}else if($records[$i]['TNGPaymentID'] == -1 && $records[$i]['TransactionID'] != ''){ // undo Top-up
								$this->Cancel_Cash_Deposit($records[$i]['TransactionID']);
							}
						}*/
						$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET ChargeStatus='$parChargeStatus',ModifiedDate=NOW() WHERE RecordID='".$records[$i]['TNGRecordID']."'";
						$resultAry[] = $this->db_db_query($sql);
					}else if($parChargeStatus == '4'){ // refunded
						$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET RefundStatus='$parRefundStatus',ModifiedDate=NOW() WHERE RecordID='".$records[$i]['TNGRecordID']."'";
						$resultAry[] = $this->db_db_query($sql);
					}
					else if($records[$i]['TNGPaymentID']!='' && $records[$i]['RecordStatus']==1 /*&& $records[$i]['PaymentStatus']!=1*/){
						$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET PaymentStatus='1',ChargeStatus='$parChargeStatus',ModifiedDate=NOW() WHERE RecordID='".$records[$i]['TNGRecordID']."'";
						$resultAry[] = $this->db_db_query($sql);
					}
				}
			}

			return $resultAry;
		}

	function upsertSubsidyIdentity($map)
	{
		$fields = array('IdentityID','IdentityName','Code','RecordStatus');
		$user_id = $_SESSION['UserID'];
		
		if(count($map) == 0) return false;
		
		if(isset($map['IdentityID']) && $map['IdentityID']!='' && $map['IdentityID'] > 0)
		{
			$sql = "UPDATE PAYMENT_SUBSIDY_IDENTITY SET ";
			$sep = "";
			foreach($map as $key => $val){
				if(!in_array($key, $fields) || $key == 'IdentityID') continue;
				
				$sql .= $sep."$key='".$this->Get_Safe_Sql_Query(trim($val))."'";
				$sep = ",";
			}
			$sql.= " ,ModifiedDate=NOW(),ModifiedBy='$user_id' WHERE IdentityID='".$map['IdentityID']."'";
			$success = $this->db_db_query($sql);
		}else{
			$keys = '';
			$values = '';
			$sep = "";
			foreach($map as $key => $val){
				if(!in_array($key, $fields)) continue;
				$keys .= $sep."$key";
				$values .= $sep."'".$this->Get_Safe_Sql_Query(trim($val))."'";
				$sep = ",";
			}
			$keys .= ",InputDate,ModifiedDate,InputBy,ModifiedBy";
			$values .= ",NOW(),NOW(),'$user_id','$user_id'";
			
			$sql = "INSERT INTO PAYMENT_SUBSIDY_IDENTITY ($keys) VALUES ($values)";
			$success = $this->db_db_query($sql);
		}
		
		return $success;
	}
	
	// soft delete
	function deleteSubsidyIdentity($identityId)
	{
		//$sql = "DELETE FROM PAYMENT_SUBSIDY_IDENTITY WHERE IdentityID ";
		$sql = "UPDATE PAYMENT_SUBSIDY_IDENTITY SET RecordStatus='0' WHERE IdentityID ";
		if(is_array($identityId)){
			$sql .= " IN (".implode(",",IntegerSafe($identityId)).")";
		}else{
			$sql .= "='".IntegerSafe($identityId)."'";
		}
		
		return $this->db_db_query($sql);
	}
	
	function getSubsidyIdentity($data)
	{
		$conds = "";
		$more_fields = "";
		$joins = "";
		$group_by = "";
		$order_by = "";
		
		if(isset($data['IdentityID'])){
			$conds .= " AND i.IdentityID ";
			if(is_array($data['IdentityID'])){
				$conds .= " IN (".implode(",",IntegerSafe($data['IdentityID'])).") ";
			}else{
				$conds .= "='".IntegerSafe($data['IdentityID'])."' ";
			}
		}
		
		if(isset($data['ExcludeIdentityID'])){
			$conds .= " AND i.IdentityID ";
			if(is_array($data['ExcludeIdentityID'])){
				$conds .= " NOT IN (".implode(",",IntegerSafe($data['ExcludeIdentityID'])).") ";
			}else{
				$conds .= "<>'".IntegerSafe($data['ExcludeIdentityID'])."' ";
			}
		}
		
		if(isset($data['IdentityName'])){
			$conds .= " AND i.IdentityName='".$this->Get_Safe_Sql_Query(trim($data['IdentityName']))."' ";
		}
		
		if(isset($data['Code'])){
			$conds .= " AND i.Code='".$this->Get_Safe_Sql_Query(trim($data['Code']))."' ";
		}
		
		if(isset($data['RecordStatus'])){
			$conds .= " AND i.RecordStatus ";
			if(is_array($data['RecordStatus'])){
				$conds .= " IN (".implode(",",IntegerSafe($data['RecordStatus'])).") ";
			}else{
				$conds .= "='".IntegerSafe($data['RecordStatus'])."' ";
			}
		}else{
			$conds .= " AND i.RecordStatus='1' ";
		}
		
		if(isset($data['Keyword']) && trim($data['Keyword']) != ''){
			$keyword = $this->Get_Safe_Sql_Like_Query(trim($data['Keyword']));
			$conds .= " AND (i.IdentityName LIKE '%$keyword%' OR i.Code LIKE '%$keyword%') ";
		}
		
		if(isset($data['OrderByIdentityName'])){
			$order_by = " ORDER BY i.IdentityName ";
		}
		
		if(isset($data['OrderByCode'])){
			$order_by = " ORDER BY i.Code ";
		}
		
		if(isset($data['GetStudentCount']) && $data['GetStudentCount']){
			$more_fields .= ",COUNT(s.StudentID) as StudentCount ";
			$joins .= " LEFT JOIN PAYMENT_SUBSIDY_IDENTITY_STUDENT as s ON s.IdentityID=i.IdentityID ";
			$group_by = " GROUP BY i.IdentityID ";
		}
		
		$sql = "SELECT i.* $more_fields FROM PAYMENT_SUBSIDY_IDENTITY as i $joins WHERE 1 ".$conds.$group_by.$order_by;
		$records = $this->returnResultSet($sql);
		
		if(isset($data['GetStudentIDs']) && $data['GetStudentIDs']){
			$identity_ids = Get_Array_By_Key($records,'IdentityID');
			if(count($identity_ids)>0){
				$sql = "SELECT IdentityID,StudentID FROM PAYMENT_SUBSIDY_IDENTITY_STUDENT WHERE IdentityID IN (".implode(",",$identity_ids).")";
				$identity_students = $this->returnResultSet($sql);
				$identity_id_to_students = array();
				for($i=0;$i<count($identity_students);$i++){
					$tmp_identity_id = $identity_students[$i]['IdentityID'];
					if(!isset($identity_id_to_students[$tmp_identity_id])){
						$identity_id_to_students[$tmp_identity_id] = array();
					}
					$identity_id_to_students[$tmp_identity_id][] = $identity_students[$i]['StudentID'];
				}
				$record_count = count($records);
				for($i=0;$i<$record_count;$i++){
					$records[$i]['StudentIDs'] = isset($identity_id_to_students[$records[$i]['IdentityID']])? $identity_id_to_students[$records[$i]['IdentityID']] : array();
				}
			}
		}
		
		return $records;
	}
	
	function getSubsidyIdentityStudents($data)
	{
		$conds = "";
		$more_fields = "";
		$joins = "";
		$order_by = "";
		
		if(isset($data['IdentityID'])){
			$conds .= " AND s.IdentityID ";
			if(is_array($data['IdentityID'])){
				$conds .= " IN (".implode(",",IntegerSafe($data['IdentityID'])).") ";
			}else{
				$conds .= "='".IntegerSafe($data['IdentityID'])."' ";
			}
		}
		
		if(isset($data['StudentID'])){
			$conds .= " AND s.StudentID ";
			if(is_array($data['StudentID'])){
				$conds .= " IN (".implode(",",IntegerSafe($data['StudentID'])).") ";
			}else{
				$conds .= "='".IntegerSafe($data['StudentID'])."' ";
			}
		}
		
		if(isset($data['DateWithinEffectiveDates']) && $data['DateWithinEffectiveDates'] != ''){
			$conds .= " AND (";
			$conds .= " (s.EffectiveStartDate IS NULL AND s.EffectiveEndDate IS NULL) ";
			$conds .= " OR IF(s.EffectiveEndDate IS NULL,'".$this->Get_Safe_Sql_Query($data['DateWithinEffectiveDates'])."' >= s.EffectiveStartDate,'".$this->Get_Safe_Sql_Query($data['DateWithinEffectiveDates'])."' BETWEEN s.EffectiveStartDate AND s.EffectiveEndDate) ";
			$conds .= ") ";
		}
		
		if(isset($data['StartDateWithinEffectiveDates']) && $data['StartDateWithinEffectiveDates'] != '' && isset($data['EndDateWithinEffectiveDates']) && $data['EndDateWithinEffectiveDates']!='')
		{
			/*
			 * if EffectiveStartDate and EffectiveEndDate are both null, then the subsidy setting is always effective
			 * if EffectiveEndDate is null, then only check EffectiveStartDate
			 */
			$conds .= " AND (";
			$conds .= " (s.EffectiveStartDate IS NULL AND s.EffectiveEndDate IS NULL) ";
			$conds .= " OR IF(s.EffectiveEndDate IS NULL,
							  '".$this->Get_Safe_Sql_Query($data['StartDateWithinEffectiveDates'])."' >= s.EffectiveStartDate,
							  '".$this->Get_Safe_Sql_Query($data['StartDateWithinEffectiveDates'])."' BETWEEN s.EffectiveStartDate AND s.EffectiveEndDate 
							)
						OR IF(s.EffectiveEndDate IS NULL,
							  '".$this->Get_Safe_Sql_Query($data['EndDateWithinEffectiveDates'])."' >= s.EffectiveStartDate,
							  '".$this->Get_Safe_Sql_Query($data['EndDateWithinEffectiveDates'])."' BETWEEN s.EffectiveStartDate AND s.EffectiveEndDate 
							) 
						OR (s.EffectiveStartDate BETWEEN '".$this->Get_Safe_Sql_Query($data['StartDateWithinEffectiveDates'])."' AND '".$this->Get_Safe_Sql_Query($data['EndDateWithinEffectiveDates'])."')
						OR IF(s.EffectiveEndDate IS NOT NULL,s.EffectiveEndDate BETWEEN '".$this->Get_Safe_Sql_Query($data['StartDateWithinEffectiveDates'])."' AND '".$this->Get_Safe_Sql_Query($data['EndDateWithinEffectiveDates'])."',0) ";
			$conds .= ") ";
		}
		
		if(isset($data['GetStudentUser']) && $data['GetStudentUser']){
			$more_fields .= ",u.UserLogin,u.ClassName,u.ClassNumber,u.EnglishName,u.ChineseName ";
			$joins .= " INNER JOIN INTRANET_USER as u ON u.UserID=s.StudentID ";
			$conds .= " AND u.RecordType='".USERTYPE_STUDENT."' ";
			if(isset($data['UserStatus'])){
				$conds .= " AND u.RecordStatus ";
				if(is_array($data['UserStatus'])){
					$conds .= " IN ('".implode("','",($data['UserStatus']))."') ";
				}else{
					$conds .= "='".$data['UserStatus']."' ";
				}
			}
		}
		
		if(isset($data['OrderByClause']) && $data['OrderByClause']!=''){
			$order_by = $data['OrderByClause'];
		}
		
		$sql = "SELECT s.* $more_fields FROM PAYMENT_SUBSIDY_IDENTITY_STUDENT as s $joins WHERE 1 ".$conds.$order_by;
		$records = $this->returnResultSet($sql);
		return $records;
	}
	
	function addSubsidyIdentityStudents($data)
	{
		if(!isset($data['IdentityID']) || !isset($data['StudentID']) || count($data['IdentityID']) != count($data['StudentID'])){
			return false;
		}
		
		$input_user_id = $_SESSION['UserID'];
		
		$sql = "INSERT IGNORE INTO PAYMENT_SUBSIDY_IDENTITY_STUDENT (IdentityID,StudentID,EffectiveStartDate,EffectiveEndDate,InputBy,InputDate) VALUES ";
		$values = array();
		
		$identity_id_ary = IntegerSafe((array)$data['IdentityID']);
		$student_id_ary = IntegerSafe((array)$data['StudentID']);
		$effective_start_date_ary = isset($data['EffectiveStartDate'])? (array)$data['EffectiveStartDate'] : array();
		$effective_end_date_ary = isset($data['EffectiveEndDate'])? (array)$data['EffectiveEndDate'] : array();
		$size = count($student_id_ary);
		
		for($i=0;$i<$size;$i++){
			$identity_id = $identity_id_ary[$i];
			$student_id = $student_id_ary[$i];
			$effective_start_date_value = (isset($effective_start_date_ary[$i]) && !empty($effective_start_date_ary[$i])) ? "'".$this->Get_Safe_Sql_Query($effective_start_date_ary[$i])."'" : 'NULL';
			$effective_end_date_value = (isset($effective_end_date_ary[$i]) && !empty($effective_end_date_ary[$i])) ? "'".$this->Get_Safe_Sql_Query($effective_end_date_ary[$i])."'" : 'NULL';
			$values[] = "('$identity_id','$student_id',$effective_start_date_value,$effective_end_date_value,'$input_user_id',NOW())";
		}
		
		if(count($values)>0){
			$sql .= implode(",",$values);
			return $this->db_db_query($sql);
		}
		
		return false;
	}
	
	function deleteSubsidyIdentityStudents($data)
	{
		if(!(isset($data['IdentityID']) || isset($data['StudentID']))){
			return false;
		}
		
		$sql = "DELETE FROM PAYMENT_SUBSIDY_IDENTITY_STUDENT WHERE 1 ";
		if(isset($data['IdentityID'])){
			$id = $data['IdentityID'];
			if(is_array($id)){
				$sql .= " AND IdentityID IN (".implode(",",IntegerSafe($id)).") ";
			}else{
				$sql .= " AND IdentityID='".IntegerSafe($id)."' ";
			}
		}
		if(isset($data['StudentID'])){
			$id = $data['StudentID'];
			if(is_array($id)){
				$sql .= " AND StudentID IN (".implode(",",IntegerSafe($id)).") ";
			}else{
				$sql .= " AND StudentID='".IntegerSafe($id)."' ";
			}
		}
		
		return $this->db_db_query($sql);
	}

        function Get_Subsidy_Identity_Format_Array($format)
        {
        	global $Lang;
			if($format == '1') {
				return array(
					$Lang['ePayment']['SubsidyIdentityImportHeader_1']['EN'],
					$Lang['ePayment']['SubsidyIdentityImportHeader_1']['B5']
				);
			} else {
				return array(
					$Lang['ePayment']['SubsidyIdentityImportHeader_2']['EN'],
					$Lang['ePayment']['SubsidyIdentityImportHeader_2']['B5']
				);
			}
        }

        function Import_Subsidy_Identity_Record_To_Temp($data, $format)
        {
            global $Lang;
            $Error = array();
            $WarningCount = 0;
            $Result = array();
            if(!is_array($data))
            {
                return array($Error, $WarningCount);
            }

            if(!in_array($format, array('1', '2'))) {
                return array($Error, $WarningCount);
            }


            $sql = 'DROP TABLE TEMP_IMPORT_STUDENT_SUBSIDY_IDENTITY_RECORD';
            $this->db_db_query($sql);
            $sql = 'CREATE TABLE TEMP_IMPORT_STUDENT_SUBSIDY_IDENTITY_RECORD (
                                IdentityID int(11) default NULL,
								UserID int(11) default NULL,
								EffectiveStartDate date default NULL,
  								EffectiveEndDate date default NULL,
								DeleteExisting varchar(1),
							  UNIQUE KEY (IdentityID, UserID, DeleteExisting) 
							)
						 ';
            $this->db_db_query($sql);
            $this->Start_Trans();

            for($i=0;$i<sizeof($data);$i++) {
                $targetUID = '';
                if($format == '1') {
                    list($SubsidyIdentityCode, $ClassName, $ClassNumber, $StudentName, $EffectiveStartDate, $EffectiveEndDate) = $data[$i];
                    $SubsidyIdentityCode = trim($SubsidyIdentityCode);
                    $ClassName = trim($ClassName);
                    $ClassNumber = trim($ClassNumber);
					$EffectiveStartDate = trim($EffectiveStartDate);
					$EffectiveEndDate = trim($EffectiveEndDate);
                } else if($format == '2') {
                    list($SubsidyIdentityCode, $LoginId, $StudentName, $EffectiveStartDate, $EffectiveEndDate) = $data[$i];
                    $SubsidyIdentityCode = trim($SubsidyIdentityCode);
                    $LoginId = trim($LoginId);
					$EffectiveStartDate = trim($EffectiveStartDate);
					$EffectiveEndDate = trim($EffectiveEndDate);
                }

                $records = $this->getSubsidyIdentity(array("Code"=>$SubsidyIdentityCode));
                if(count($records)==0){
                    $Error[] = array("RecordDetail"=>$data[$i],"RowNumber"=>($i+3),"ErrorMsg"=> $Lang['ePayment']['SubsidyIdentityCodeNotFound']);
                    continue;
                } else if(count($records) > 1) {
                    $Error[] = array("RecordDetail"=>$data[$i],"RowNumber"=>($i+3),"ErrorMsg"=> $Lang['ePayment']['ImportMoreThanOneSubsidyIdentityCodeFound']);
                    continue;
                }

                $IdentityID = $records[0]['IdentityID'];

                if($format == '1') {
                    $sql = "SELECT
							a.UserID
						FROM
							INTRANET_USER as a
						WHERE
							a.RecordType = '2'
					  	AND a.RecordStatus = '1'
							AND TRIM(a.ClassName) = '" . $this->Get_Safe_Sql_Query($ClassName) . "' 
							AND TRIM(a.ClassNumber) = '" . $this->Get_Safe_Sql_Query($ClassNumber) . "' ";
                } else if($format == '2') {
                    $sql = "SELECT
							a.UserID
						FROM
							INTRANET_USER as a
						WHERE
							a.RecordType = '2'
					  	AND a.RecordStatus = '1'
							AND a.UserLogin = '" . $this->Get_Safe_Sql_Query($LoginId) . "' ";
                }

                $UserInfo = $this->returnArray($sql);
                if(sizeof($UserInfo) == 0) // no match found
                {
                    $Error[] = array("RecordDetail"=>$data[$i],"RowNumber"=>($i+3),"ErrorMsg"=> $Lang['ePayment']['StudentNotFound']);
                    continue;
                } else if (sizeof($UserInfo) > 1) { // more than one match
                    $Error[] = array("RecordDetail"=>$data[$i],"RowNumber"=>($i+3),"ErrorMsg"=> $Lang['ePayment']['ImportMoreThanOneStudentFound']);
                    continue;
                } else { // only one match
                    $targetUID = $UserInfo[0]['UserID'];
                }

                if($targetUID == '') {
                    $Error[] = array("RecordDetail"=>$data[$i],"RowNumber"=>($i+3),"ErrorMsg"=> $Lang['ePayment']['StudentNotFound']);
                    continue;
                }

                $sql = "select count(1) from TEMP_IMPORT_STUDENT_SUBSIDY_IDENTITY_RECORD where UserID = '".$this->Get_Safe_Sql_Query($targetUID)."'";
                $Temp = $this->returnVector($sql);
                if ($Temp[0] > 0) { // preset already in same file
                    $Error[] = array("RecordDetail"=>$data[$i],"RowNumber"=>($i+3),"ErrorMsg"=> $Lang['ePayment']['PresetSubsidyIdentitySetOnSameFile']);
                    continue;
                }


                if($EffectiveStartDate == '' && $EffectiveEndDate == '') {
					$EffectiveDateValid = true;
				} else if($EffectiveStartDate != '' && $EffectiveEndDate == '') {
					$EffectiveDateValid = true;
				} else if($EffectiveStartDate == '' && $EffectiveEndDate != '') {
					$EffectiveDateValid = false;
				} elseif(!intranet_validateDate($EffectiveStartDate) || !intranet_validateDate($EffectiveEndDate) || strtotime($EffectiveStartDate) > strtotime($EffectiveEndDate)) {
					$EffectiveDateValid = false;
				} else {
					$EffectiveDateValid = true;
				}

                if($EffectiveDateValid == false) {
					$Error[] = array("RecordDetail"=>$data[$i],"RowNumber"=>($i+3),"ErrorMsg"=> $Lang['General']['InvalidDateFormat']);
					continue;
				}

				$EffectiveStartDate_str = empty($EffectiveStartDate) ? 'NULL' : "'".$this->Get_Safe_Sql_Query($EffectiveStartDate)."'";
				$EffectiveEndDate_str = empty($EffectiveEndDate) ? 'NULL' : "'".$this->Get_Safe_Sql_Query($EffectiveEndDate)."'";

                $student_records = $this->getSubsidyIdentityStudents(array("StudentID"=>$targetUID));
                if(count($student_records)!=0) {
					if ($student_records[0]['IdentityID'] != $IdentityID) { // give warning if current identity is not the same as the import identity
						$records = $this->getSubsidyIdentity(array("IdentityID"=>$student_records[0]['IdentityID']));
						if(count($records) > 0) {
							$Error[] = array("RecordDetail" => $data[$i], "RowNumber" => ($i + 3), "WarnMsg" => $Lang['ePayment']['StudentWillRemoveExistingIdentity']);
							$WarningCount++;
						}
					}

                    $sql = 'insert into TEMP_IMPORT_STUDENT_SUBSIDY_IDENTITY_RECORD (
                                  IdentityID,
								  UserID,
								  DeleteExisting,
								  EffectiveStartDate,
								  EffectiveEndDate
									)
								values (
									\''.$this->Get_Safe_Sql_Query($IdentityID).'\',
									\''.$this->Get_Safe_Sql_Query($targetUID).'\',
									\'Y\',
									'.$EffectiveStartDate_str.',
									'.$EffectiveEndDate_str.'
									)';

                    $Result['DeleteTempRecord:'.$targetUID] = $this->db_db_query($sql);
                }

                $sql = 'insert into TEMP_IMPORT_STUDENT_SUBSIDY_IDENTITY_RECORD (
                                  IdentityID,
								  UserID,
								  DeleteExisting,
							      EffectiveStartDate,
								  EffectiveEndDate
									)
								values (
									\''.$this->Get_Safe_Sql_Query($IdentityID).'\',
									\''.$this->Get_Safe_Sql_Query($targetUID).'\',
									\'N\',
									'.$EffectiveStartDate_str.',
									'.$EffectiveEndDate_str.'
									)';

                $Result['InsertTempRecord:'.$targetUID] = $this->db_db_query($sql);
            }

            if (in_array(false,$Result)) {
                $this->RollBack_Trans();
            }
            else {
                $this->Commit_Trans();
            }

            return array($Error, $WarningCount);
        }

        function Finalize_Import_Subsidy_Identity()
        {
			$sql = "SELECT b.IdentityID, b.EffectiveStartDate, b.EffectiveEndDate, a.UserID as StudentID, a.IdentityID as NewIdentityID , a.EffectiveStartDate as NewEffectiveStartDate, a.EffectiveEndDate as NewEffectiveEndDate
					FROM TEMP_IMPORT_STUDENT_SUBSIDY_IDENTITY_RECORD as a
					LEFT JOIN PAYMENT_SUBSIDY_IDENTITY_STUDENT as b ON a.UserID=b.StudentID
					WHERE DeleteExisting='Y'";
			$students_records = $this->returnArray($sql);
			$change_log_data = array();
			$studentid_updated = array();
			foreach($students_records as $record) {
				$studentid_updated[] = $record['StudentID'];
				$data = array();
				$data['StudentID'] = $record['StudentID'];
				$data['OldIdentityID'] = $record['IdentityID'];
				$data['OldEffectiveStartDate'] = $record['EffectiveStartDate'];
				$data['OldEffectiveEndDate'] = $record['EffectiveEndDate'];
				$data['NewIdentityID'] = $record['NewIdentityID'];
				$data['NewEffectiveStartDate'] = $record['NewEffectiveStartDate'];
				$data['NewEffectiveEndDate'] = $record['NewEffectiveEndDate'];
				$change_log_data[] = $data;
			}

			if(count($studentid_updated) > 0) {
				$studentid_exclude = " AND a.UserID NOT IN (" . implode(',', $studentid_updated) . ")";
			}
			$sql = "SELECT b.IdentityID, b.EffectiveStartDate, b.EffectiveEndDate, a.UserID as StudentID, a.IdentityID as NewIdentityID , a.EffectiveStartDate as NewEffectiveStartDate, a.EffectiveEndDate as NewEffectiveEndDate
				FROM TEMP_IMPORT_STUDENT_SUBSIDY_IDENTITY_RECORD as a
				LEFT JOIN PAYMENT_SUBSIDY_IDENTITY_STUDENT as b ON a.UserID=b.StudentID
				WHERE DeleteExisting='N' $studentid_exclude";
			$students_records = $this->returnArray($sql);
			foreach($students_records as $record) {
				$data = array();
				$data['StudentID'] = $record['StudentID'];
				$data['OldIdentityID'] = '';
				$data['OldEffectiveStartDate'] = '';
				$data['OldEffectiveEndDate'] = '';
				$data['NewIdentityID'] = $record['NewIdentityID'];
				$data['NewEffectiveStartDate'] = $record['NewEffectiveStartDate'];
				$data['NewEffectiveEndDate'] = $record['NewEffectiveEndDate'];
				$change_log_data[] = $data;
			}


			if(count($change_log_data) > 0) {
				$this->addSubsidyIdentityStudentChangeLogs($change_log_data);
			}

            $input_user_id = $_SESSION['UserID'];
            $sql = "SELECT a.UserID FROM TEMP_IMPORT_STUDENT_SUBSIDY_IDENTITY_RECORD as a WHERE `DeleteExisting`='Y'";
            $Temp = $this->returnVector($sql);
            if (count($Temp)) {
                $this->deleteSubsidyIdentityStudents(array("StudentID" => $Temp));
            }

            $sql = "INSERT INTO PAYMENT_SUBSIDY_IDENTITY_STUDENT (
    					IdentityID, 
    					StudentID, 
    					InputBy,
    					InputDate,
    					EffectiveStartDate,
    					EffectiveEndDate
    					)
             SELECT
              			a.IdentityID,
              			a.UserID,
              			'$input_user_id', 
              			now(),
              			EffectiveStartDate,
              			EffectiveEndDate
             FROM 
             	TEMP_IMPORT_STUDENT_SUBSIDY_IDENTITY_RECORD as a WHERE `DeleteExisting`='N'";
            $Result = $this->db_db_query($sql);

            if ($Result) {
                $sql = "select count(1) from TEMP_IMPORT_STUDENT_SUBSIDY_IDENTITY_RECORD WHERE `DeleteExisting`='N'";
                $Temp = $this->returnVector($sql);

                $sql = 'drop table TEMP_IMPORT_STUDENT_SUBSIDY_IDENTITY_RECORD';
                $this->db_db_query($sql);

                return $Temp[0];
            }
            else {
                return 0;
            }
        }
		
		// Params: $data is array of array('StudentID','OldIdentityID','OldEffectiveStartDate','OldEffectiveEndDate','NewIdentityID','NewEffectiveStartDate','NewEffectiveEndDate')
		function addSubsidyIdentityStudentChangeLogs($data)
		{
			$input_user_id = $_SESSION['UserID'];
			
			$sql = "INSERT IGNORE INTO PAYMENT_SUBSIDY_IDENTITY_STUDENT_CHANGE_LOG (StudentID,OldIdentityID,OldEffectiveStartDate,OldEffectiveEndDate,NewIdentityID,NewEffectiveStartDate,NewEffectiveEndDate,InputBy,InputDate) VALUES ";
			$values = array();
		
			$data_size = count($data);
			
			for($i=0;$i<$data_size;$i++){
				$d = $data[$i];
				$student_id = $d['StudentID'];
				$old_identity_id = $d['OldIdentityID'] == ''? "NULL" : "'".IntegerSafe($d['OldIdentityID']).".'";
				$old_effective_start_date = $d['OldEffectiveStartDate'] == ''? "NULL" : "'".$this->Get_Safe_Sql_Query($d['OldEffectiveStartDate']).".'";
				$old_effective_end_date = $d['OldEffectiveEndDate'] == ''? "NULL" : "'".$this->Get_Safe_Sql_Query($d['OldEffectiveEndDate']).".'";
				$new_identity_id = $d['NewIdentityID'] == ''? "NULL" : "'".IntegerSafe($d['NewIdentityID']).".'";
				$new_effective_start_date = $d['NewEffectiveStartDate'] == ''? "NULL" : "'".$this->Get_Safe_Sql_Query($d['NewEffectiveStartDate']).".'";
				$new_effective_end_date = $d['NewEffectiveEndDate'] == ''? "NULL" : "'".$this->Get_Safe_Sql_Query($d['NewEffectiveEndDate']).".'";
				if($old_identity_id != $new_identity_id ||
					$old_effective_start_date != $new_effective_start_date ||
					$old_effective_end_date != $new_effective_end_date) {
					$values[] = "('$student_id',$old_identity_id,$old_effective_start_date,$old_effective_end_date,$new_identity_id,$new_effective_start_date,$new_effective_end_date,'$input_user_id',NOW())";
				}
			}
			
			if(count($values)>0){
				$sql .= implode(",",$values);
				return $this->db_db_query($sql);
			}
			
			return false;
		}
		
		// $TargetDate in format YYYYMMDD
		function getAlipaySettlementData($TargetDate,$MerchantAccount, $ServiceProvider)
		{
			global $intranet_root, $sys_custom, $PATH_WRT_ROOT, $eclassAppConfig, $config_school_code;
			
			include_once($intranet_root.'/includes/global.php');
			include_once($intranet_root.'/includes/libdb.php');
			include_once($intranet_root.'/includes/libeclassapiauth.php');
			include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
			include_once($intranet_root."/includes/eClassApp/libAES.php");
			include_once($intranet_root."/includes/json.php");
			
			$lapiAuth = new libeclassapiauth();
			$libaes = new libAES($eclassAppConfig['aesKey']);
			$jsonObj = new JSON_obj();
			
			$platformApiKey = $lapiAuth->GetAPIKeyByProject('IP/EJ platform');
			$apiPath = $eclassAppConfig['paymentGateway']['apiPath'];
			
			$requestAry = array();
			$requestAry['RequestMethod'] = 'getAlipaySettlementData';
			$requestAry['APIKey'] = $platformApiKey;
			$requestAry['Request']['SchoolCode'] = $config_school_code;
			$requestAry['Request']['TargetDate'] = $TargetDate;
			$requestAry['Request']['MerchantAccount'] = $MerchantAccount;
			$requestAry['Request']['ServiceProvider'] = $ServiceProvider;
			$requestJsonString = $jsonObj->encode($requestAry);
			
			
			$jsonAry = array();
			$jsonAry['eClassRequestEncrypted'] = $libaes->encrypt($requestJsonString);
			$postJsonString = $jsonObj->encode($jsonAry);
			
			
			session_write_close();
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
			curl_setopt($ch, CURLOPT_URL, $apiPath);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJsonString);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			$responseJson = curl_exec($ch);
			curl_close($ch);
			
			$json_ary = $jsonObj->decode($responseJson);
			return $json_ary['MethodResult'];
		}


		function getWechatSettlementData($TargetDate,$MerchantAccount, $ServiceProvider)
		{
			global $intranet_root, $sys_custom, $PATH_WRT_ROOT, $eclassAppConfig, $config_school_code;

			include_once($intranet_root.'/includes/global.php');
			include_once($intranet_root.'/includes/libdb.php');
			include_once($intranet_root.'/includes/libeclassapiauth.php');
			include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
			include_once($intranet_root."/includes/eClassApp/libAES.php");
			include_once($intranet_root."/includes/json.php");

			$lapiAuth = new libeclassapiauth();
			$libaes = new libAES($eclassAppConfig['aesKey']);
			$jsonObj = new JSON_obj();

			$platformApiKey = $lapiAuth->GetAPIKeyByProject('IP/EJ platform');
			$apiPath = $eclassAppConfig['paymentGateway']['apiPath'];

			$requestAry = array();
			$requestAry['RequestMethod'] = 'getWechatSettlementData';
			$requestAry['APIKey'] = $platformApiKey;
			$requestAry['Request']['SchoolCode'] = $config_school_code;
			$requestAry['Request']['TargetDate'] = $TargetDate;
			$requestAry['Request']['MerchantAccount'] = $MerchantAccount;
			$requestAry['Request']['ServiceProvider'] = $ServiceProvider;
			$requestJsonString = $jsonObj->encode($requestAry);


			$jsonAry = array();
			$jsonAry['eClassRequestEncrypted'] = $libaes->encrypt($requestJsonString);
			$postJsonString = $jsonObj->encode($jsonAry);


			session_write_close();
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_URL, $apiPath);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJsonString);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			$responseJson = curl_exec($ch);
			curl_close($ch);

			$json_ary = $jsonObj->decode($responseJson);
			return $json_ary['MethodResult'];
		}

		function upsertOverallTransactionLog($map)
		{
			$fields = array('LogID','StudentID','TransactionType','Amount','RelatedTransactionID','BalanceAfter','TransactionTime','Details','TransactionDetails','RefCode','RebateSessionID');
			$input_by_user_id = isset($map['InputBy'])? $map['InputBy'] : $_SESSION['UserID'];
			
			if(count($map) == 0) return false;
			
			if(isset($map['LogID']) && $map['LogID']!='' && $map['LogID'] > 0)
			{
				$sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET ";
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields) || $key == 'LogID') continue;
					
					$sql .= $sep."$key='".$this->Get_Safe_Sql_Query($val)."'";
					$sep = ",";
				}
				$sql.= " ,InputBy='$input_by_user_id' WHERE LogID='".$map['LogID']."'";
				$success = $this->db_db_query($sql);
			}else{
				$keys = '';
				$values = '';
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields)) continue;
					$keys .= $sep."$key";
					$values .= $sep."'".$this->Get_Safe_Sql_Query($val)."'";
					$sep = ",";
				}
				$keys .= ",InputBy";
				$values .= ",'$input_by_user_id'";
				
				$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG ($keys) VALUES ($values)";
				$success = $this->db_db_query($sql);
			}
			
			return $success;
		}
		
		function getOverallTransactionLogs($data)
		{
			$fields = " t.* ";
			$conds = "";
			$joins = "";
			$order_by = " ORDER BY t.LogID ";
			
			if(isset($data['LogID'])){
				$conds .= " AND t.LogID ";
				if(is_array($data['LogID'])){
					$conds .= " IN (".implode(",",IntegerSafe($data['LogID'])).") ";
				}else{
					$conds .= "='".IntegerSafe($data['LogID'])."' ";
				}
			}
			
			if(isset($data['ExcludeLogID'])){
				$conds .= " AND t.LogID ";
				if(is_array($data['ExcludeLogID'])){
					$conds .= " NOT IN (".implode(",",IntegerSafe($data['ExcludeLogID'])).") ";
				}else{
					$conds .= "<>'".IntegerSafe($data['ExcludeLogID'])."' ";
				}
			}
			
			if(isset($data['StudentID'])){
				$conds .= " AND t.StudentID ";
				if(is_array($data['StudentID'])){
					$conds .= " IN (".implode(",",IntegerSafe($data['StudentID'])).") ";
				}else{
					$conds .= "='".IntegerSafe($data['StudentID'])."' ";
				}
			}
			
			if(isset($data['TransactionType'])){
				$conds .= " AND t.TransactionType ";
				if(is_array($data['TransactionType'])){
					$conds .= " IN (".implode(",",IntegerSafe($data['TransactionType'])).") ";
				}else{
					$conds .= "='".IntegerSafe($data['TransactionType'])."' ";
				}
			}
			
			if(isset($data['RelatedTransactionID'])){
				$conds .= " AND t.RelatedTransactionID ";
				if(is_array($data['RelatedTransactionID'])){
					$conds .= " IN (".implode(",",IntegerSafe($data['RelatedTransactionID'])).") ";
				}else{
					$conds .= "='".IntegerSafe($data['RelatedTransactionID'])."' ";
				}
			}
			
			if(isset($data['TransactionStartTime']) && isset($data['TransactionEndTime'])){
				$conds .= " AND (t.TransactionTime BETWEEN '".$this->Get_Safe_Sql_Query($data['TransactionStartTime'])."' AND '".$this->Get_Safe_Sql_Query($data['TransactionEndTime'])."') ";
			}
			
			if(isset($data['GetUserInfo']) && $data['GetUserInfo']){
				$fields .= ",u.UserLogin,u.EnglishName,u.ChineseName,u.RecordType as UserType,u.RecordStatus as UserStatus,u.ClassName,u.ClassNumber ";
				$joins .= " LEFT JOIN INTRANET_USER as u ON u.UserID=t.StudentID ";
			}
			
			if(isset($data['Keyword']) && trim($data['Keyword']) != ''){
				$keyword = $this->Get_Safe_Sql_Like_Query(trim($data['Keyword']));
				$conds .= " AND (t.Details LIKE '%$keyword%' OR t.RefCode LIKE '%$keyword%' OR t.TransactionDetails LIKE '%$keyword%') ";
			}
			
			
			$sql = "SELECT $fields FROM PAYMENT_OVERALL_TRANSACTION_LOG as t $joins WHERE 1 ".$conds.$order_by;
			$records = $this->returnResultSet($sql);
			return $records;
		}

    } // End of Class libpayment

} // End of directive
?>