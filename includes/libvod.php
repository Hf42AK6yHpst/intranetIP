<?php
include_once('json.php');
include_once('settings.php');

Class libvod {

    private $vod = false;
    private $url, $json;

    public function libvod()
    {
        global $dcvod;
        $this->vod = $dcvod; // or others in future

        $this->url = 'https://vod.eclasscloud.hk/api/'.$this->vod.'/';
        $this->json = new JSON_obj();
    }

    public function enabled()
    {
        return (boolean) $this->vod;
    }

    protected function request($action='', $method='POST')
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url.$action,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Authorization: Bearer pFNbCkoSu2yl1Ll9iiYPU9gOj5Ua1oPjrDFRFZkvTiU2pmpyTU9WeWZjjadb64zOS1ikJEDba1oGePit",
                "cache-control: no-cache"
            ),
        ));

        // $response = curl_exec($curl);
        // $err = curl_error($curl);
        //
        // curl_close($curl);
        // $response = $jsonObj->decode($response);

        return $curl;
    }

    public function findOrCreateSchoolFolder($school_code)
    {
        $folders = $this->fetchFolders();
        # Find if given school code is exists in vimeo
        if ( in_array($school_code, array_keys($folders)) ) {
            // Return folder uri

            return $folders[$school_code];
        } else {
            // Create one via api

            return $this->createFolder($school_code);
        }
    }

    public function fetchFolders()
    {
        $ch = $this->request('fetch', 'GET');
        $response = curl_exec($ch);
        curl_close($ch);
        $result =  $this->json->decode($response);

        return $result['data'];
    }

    public function createFolder($school_code)
    {
        $ch = $this->request('folder');
        $post = array('name' => $school_code);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result =  $this->json->decode($response);

        return $result['data'];
    }

    public function upload($file, $title, $description)
    {
        $cFile = '@' . realpath($file);

        $post = array('upload' => $cFile, 'title' => $title, 'description' => $description);
        $ch = $this->request('upload');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result =  $this->json->decode($response);

        return $result['vod'];
    }

    public function move($folder_uri, $vod_uri)
    {
        $ch = $this->request('move-video');
        $post = array('folder' => $folder_uri, 'video' => $vod_uri);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result =  $this->json->decode($response);

        return $result['data'];
    }

    public function setEmbedWhiteList($vod_uri)
    {
        $site = $_SERVER['SERVER_NAME'].(!in_array($_SERVER["SERVER_PORT"],array(80,443))
                ?":".$_SERVER["SERVER_PORT"]:"");

        $ch = $this->request('embed-whitelist');
        $post = array('video' => $vod_uri, 'url' => $site);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result =  $this->json->decode($response);

        return $result['data'];
    }

    public function getEmbedVimeo($vod_uri)
    {
        $ch = $this->request('embedded');
        $post = array('video' => $vod_uri);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        $err = curl_error($ch);

        if(curl_getinfo($ch, CURLINFO_HTTP_CODE) == 404) {
            return false;
        }

        curl_close($ch);
        $result =  $this->json->decode($response);

        return $result['html'];
    }

    public function getVideoLink($vod_uri)
    {
        $ch = $this->request('video');
        $post = array('video' => $vod_uri);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        $err = curl_error($ch);

        if(curl_getinfo($ch, CURLINFO_HTTP_CODE) == 404) {
            return false;
        }

        curl_close($ch);
        $result =  $this->json->decode($response);

        return $result['data'];
    }

    public function deleteVideo($vod_uri)
    {

        $ch = $this->request(ltrim($vod_uri, '/'), 'DELETE');
        $response = curl_exec($ch);
        $err = curl_error($ch);

        if(curl_getinfo($ch, CURLINFO_HTTP_CODE) == 404) {
            return false;
        }

        curl_close($ch);
        $result =  $this->json->decode($response);

        return $result['data'];
    }

    public function videoInfo($vod)
    {
        return explode('|-|', $vod);
    }
}
