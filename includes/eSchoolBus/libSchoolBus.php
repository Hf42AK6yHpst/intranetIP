<?php
// Editing by 
/*
 *  2018-06-22 Cameron
 *      - add function isSchoolBusAdmin()
 *      
 *  2018-06-13 Cameron
 *      - add function checkAppAccessRight()
 */
include_once('schoolBusConfig.php');

class libSchoolBus{
	
	const TEMP_SESSION = 'ESCHOOLBUS_TEMP_SSV';
	var $TmpSessionVar = array();
	
	public function libSchoolBus()
	{
		
		if(isset($_SESSION[self::TEMP_SESSION]) && count($_SESSION[self::TEMP_SESSION])>0){
			$this->TmpSessionVar = array();
			foreach($_SESSION[self::TEMP_SESSION] as $key => $val){
				$this->TmpSessionVar[$key] = $val;
			}
		}
		unset($_SESSION[self::TEMP_SESSION]);
	}
	
	function setTempSession($key, $value)
	{
		if(!isset($_SESSION[self::TEMP_SESSION])){
			$_SESSION[self::TEMP_SESSION] = array();
		}
		$_SESSION[self::TEMP_SESSION][$key] = $value;
	}
	
	function getTempSession($key)
	{
		if(count($this->TmpSessionVar) && isset($this->TmpSessionVar[$key])){
			return $this->TmpSessionVar[$key];
		}
		return '';
	}
	
	public function checkAccessRight(){
		global $CurrentPage, $task, $Lang;
		
		$page = $task;
		
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSchoolBus"] && $_SESSION['SSV_PRIVILEGE']['plugin']['eSchoolBus']){
			
		}else if($_SESSION['UserType'] == USERTYPE_STAFF)
		{
			if(strpos($page, 'settings')!==false){
				No_Access_Right_Pop_Up();
			}
		}else{
			No_Access_Right_Pop_Up();
		}
		
		return true;
	}
	
	public function getCurrentPage($task=''){
		switch($task){
			case 'management/index':
				$CurrentPage = 'testing1';
			break;
			case 'report/index':
				$CurrentPage = 'testing2';
			break;
			case 'settings/index':
				$CurrentPage = 'testing3';
			break;
			default:
				$taskAry = explode('/',$task);
				$CurrentPage = $taskAry[0].'/'.$taskAry[1];
			break;
		}
		return $CurrentPage;
	}
	public function redirect($path){
		header("Location: ".$path);
		exit;
	}
	
	public function checkAppAccessRight()
	{
	    global $Lang, $plugin, $indexVar;
	    
	    $leClassApp = $indexVar['leClassApp'];
	    $ret = true;

	    if ($_SESSION['SSV_PRIVILEGE']['plugin']['eSchoolBus']) {
    	    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSchoolBus"]){
    	        // pass
    	    } else {
    	        if($_SESSION['UserType'] == USERTYPE_STAFF){
        	        if ($plugin['eClassTeacherApp'] && $leClassApp->isSchoolInLicense('T')) {
        	           // pass    
        	        }
        	        else {
        	            $ret = false;
        	        }
    	        } elseif($_SESSION['UserType'] == USERTYPE_PARENT){
        	        if ($plugin['eClassApp'] && $leClassApp->isSchoolInLicense('P')) {
        	            // pass
        	        }
        	        else {
        	            $ret = false;
        	        }
    	        } else {
    	            $ret = false;
    	        }
    	    }
	    }
	    else {
	        $ret = false;
	    }
	    if (!$ret) {
	        No_Access_Right_Pop_Up();
	    }
	    return $ret;
	}
	
	function isSchoolBusAdmin() 
	{
	    return ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSchoolBus"]) ? true : false;
	}
}

?>