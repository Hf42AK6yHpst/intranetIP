<?php
// Editing by 
/*
 *  2020-08-19 Cameron
 *      - modify getStudentListByBus(), apply larger font size and other css requirement [case #Y187484]
 *
 *  2020-06-19 Cameron
 *      - add NeedToTakeAttendance checking in getBusTakeScheduleForParent() so that Saturday and Sunday can also show taking bus if it's set as school working day [case #Y188036]
 *
 *  2020-06-15 Cameron
 *      - add NotNeedToTakeAttendance checking in getBusTakeScheduleForParent(), treat it as not taking school bus if not need to take attendance on that day [case #Y187482]
 *
 *  2019-10-14 Cameron
 *      - don't show RecordStatus if it's not consistent with that in INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT in getHandledApplicationForTeacher()
 *
 *  2019-06-27 Cameron
 *      - show LastModifiedBy and DateModified in getHandledApplicationForTeacher()
 *      
 *  2019-04-16 Cameron
 *      - add parameter hasRouteOrNot in getStudentListByClass() [case #Y159784]
 *      - don't show timeSlot column ( There's only "FromSchool" option for Kentville)
 *      
 *  2019-04-02 Cameron
 *      - also show end date for consecutive dates of leave application in getHandledApplicationForTeacher(), getPendingApplicationForTeacher(), getApplyLeaveResult()
 *      - change variable name from $pendingApplicationAry to $handledApplicationAry in getHandledApplicationForTeacher() 
 *      
 *  2019-03-07 Cameron
 *      - add function getTakingBusClassSelection() [case #A156825]
 *      
 *  2019-03-06 Cameron
 *      - add function getClassGroupSelection(), getStudentListByBus() [case Y156987]
 *      
 *  2019-02-11 Cameron
 *      - fix bug in getBusTakeScheduleForParent()
 *      
 *  2019-02-01 Cameron
 *      - apply wordwrapByLength to event description (holiday) in getBusTakeScheduleForParent() [case #Y152924]
 *      
 *  2019-01-31 Cameron
 *      - add function getRouteTeacherList() [case #Y154206]
 *      
 *  2019-01-30 Cameron
 *      - add function getApplyLeaveStatusSelection() [case #Y154190]
 *      - add parameter $applyLeaveStatus, $startDate and $endDate to getHandledApplicationForTeacher() 
 *      - add parameter $startDate and $endDate to getPendingApplicationForTeacher()
 *      
 *  2019-01-29 Cameron
 *      - apply $includeNullRoute to getStudentBusSchedule() in getBusTakeScheduleForParent()
 *      - hide time and place in schedule of parent app in getScheduleCellDisplay()  [case #Y154194]
 *      - add function getTeacherTypeSelection() [case #Y154206]
 *      
 *  2019-01-28 Cameron
 *      - fix: show "All" for location in getStudentListByBus()
 *      - fix: show pick by parents for special arrangement in getScheduleCellDisplay() if RouteID is empty
 *      
 *  2019-01-25 Cameron
 *      - add parameter $takeOrNot and $printTitle in getStudentListByBus() [case #Y154128]
 *      
 *  2018-11-26 Cameron
 *      - show month in digital format rather than Chinese to save space in getBusTakeScheduleForParent() (case #Y152924)
 *       
 *  2018-11-21 Cameron
 *      - add function getStudentListByClass()
 *      
 *  2018-11-20 Cameron
 *      - add function getStudentListByRoute()
 *      
 *  2018-11-19 Cameron
 *      - add studentListByRoute and studentListByClass in GET_TAGS_OBJ_ARR()
 *      - add function getClassBuilding()
 *       
 *  2018-10-24 Cameron
 *      - set max start date up to academic year end date in getBusTakeScheduleForParent()
 *      - add parameter isShowNoRecord to getApplyLeaveResult(), getPendingApplicationForTeacher(), getHandledApplicationForTeacher()
 *      - handle absent and deleted case in getBusTakeScheduleForParent(), getLeaveApplicationStatusHistoryDisplay()
 *      - add absent case in getLeaveApplicationStatusDisplay() 
 *      
 *  2018-10-18 Cameron
 *      - show $Lang['General']['NoRecordAtThisMoment'] when there's no record in getApplyLeaveResult(), 
 *      getPendingApplicationForTeacher(), getHandledApplicationForTeacher()
 *      
 *  2018-07-05 Cameron
 *      - add function getBusTakeScheduleBodyForParent()
 *      
 *  2018-06-22 Cameron
 *      - add function getPendingApplicationForTeacher(), getHandledApplicationForTeacher() 
 *      
 *  2018-06-19 Cameron
 *      - add function getLeaveApplicationStatusHistoryDisplay(), getApplyLeaveResult()
 *      
 *  2018-06-15 Cameron
 *      - add function getLeaveApplicationStatusDisplay(), getScheduleCellDisplay(), getGotoSchoolDisplay(), getLeaveSchoolDisplay()
 *      
 *  2018-06-14 Cameron
 *      - fix bug on getWeekdayKey() for case 7
 *      - add function getBusTakeScheduleForParent(), getBusTakeScheduleLayoutForParent()
 *      
 *  2018-06-06 Cameron
 *      - add CutoffTime to GET_TAGS_OBJ_ARR()
 */

if(!defined("NEW_LINE")) {
    define("NEW_LINE", "\n");
}

include_once($intranet_root."/includes/libinterface.php");
class libSchoolBus_ui extends interface_html {
	
	public function GET_MODULE_OBJ_ARR($eService=false){
		global $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT, $CurrentPage,$Lang;
		if($eService){
			include $PATH_WRT_ROOT.'includes/eSchoolBus/libSchoolBus_eService_leftmenu.php';
		}else{
			include $PATH_WRT_ROOT.'includes/eSchoolBus/libSchoolBus_leftmenu.php';
		}
		
		$CurrentMenuAry = explode('/',$CurrentPage);
		
		foreach ((array)$MenuArr as $key => $ary){
			$TempMenuArr[$key]= array($MenuGroup[$key],'#',($CurrentMenuAry[0]==$key));
			foreach($ary as $_key => $_ary){
				$TempMenuArr[$key]['Child'][$_key] = array($_ary['Title'],'index.php?task='.$_ary['URL'],($CurrentPage==$_ary['URL']));
			}
		}
		$MenuArr = $TempMenuArr;
	
		### module information
		$MODULE_OBJ['title'] = $Lang['Header']['Menu']['eSchoolBus'];
		$MODULE_OBJ['title_css'] = "menu_opened";
		$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_schoolbus.png";
		$MODULE_OBJ['menu'] = $MenuArr;
	
		return $MODULE_OBJ;
	}
	public function GET_TAGS_OBJ_ARR(){
		global $CurrentPage,$PATH_WRT_ROOT,$Lang;
		include $PATH_WRT_ROOT.'includes/eSchoolBus/libSchoolBus_leftmenu.php';
		include $PATH_WRT_ROOT.'includes/eSchoolBus/libSchoolBus_eService_leftmenu.php';
// 		debug_pr($CurrentPage);
// 		debug_pr($MenuArr);
		$TAGS_OBJ = array();
		
		switch($CurrentPage){
			//Management
			case 'management/attendance':
				$TAGS_OBJ[] = array($MenuArr['management']['Attendance']['Title']);
			break;
			case 'management/specialArrangement':
				$TAGS_OBJ[] = array($MenuArr['management']['SpecialArrangement']['Title']);
			break;
				
			//Report
			case 'report/classList':
				$TAGS_OBJ[] = array($MenuArr['report']['ClassList']['Title']);
			break;
			case 'report/routeArrangement':
				$TAGS_OBJ[] = array($MenuArr['report']['RouteArrangement']['Title']);
			break;
			case 'report/studentList':
				$TAGS_OBJ[] = array($MenuArr['report']['StudentList']['Title']);
			break;
			case 'report/asaContact':
				$TAGS_OBJ[] = array($MenuArr['report']['AsaContact']['Title']);
			break;
			case 'report/pickup':
				$TAGS_OBJ[] = array($MenuArr['report']['PickUp']['Title']);
			break;
			case 'report/studentListByBus':
			    $TAGS_OBJ[] = array($MenuArr['report']['StudentListByBus']['Title']);
			    break;
			case 'report/studentListByClass':
			    $TAGS_OBJ[] = array($MenuArr['report']['StudentListByClass']['Title']);
			    break;
			    
			//Settings
			case 'settings/busStops':
				$TAGS_OBJ[] = array($MenuArr['settings']['BusStops']['Title']);
			break;
			case 'settings/vehicle':
				$TAGS_OBJ[] = array($MenuArr['settings']['Vehicle']['Title']);
			break;
			case 'settings/route':
				$TAGS_OBJ[] = array($MenuArr['settings']['Route']['Title']);
			break;
			case 'settings/students':
				$TAGS_OBJ[] = array($MenuArr['settings']['Students']['Title']);
			break;
			case 'settings/cutoffTime':
			    $TAGS_OBJ[] = array($MenuArr['settings']['CutoffTime']['Title']);
			    break;
			    
			//eService
			case 'view/':
				$TAGS_OBJ[] = array($MenuArr['view']['student']['Title']);
			break;
			
			default:
				$TAGS_OBJ[] = array('set title in ui->GET_TAGS_OBJ_ARR()');
			break;
		}
		return $TAGS_OBJ;
	}
	
	public function INCLUDE_COMMON_CSS_JS()
	{
	    global $LAYOUT_SKIN;
		$x = '<link href="/templates/'.$LAYOUT_SKIN.'/css/eSchoolBus.css" rel="stylesheet" type="text/css">'."\n";
		
		return $x;
	}
	
	public function getDbTable($sql, $columns, $field, $order, $pageNo, $page_size, $checkBox=true, $columnCustArr='', $checkBoxName='targetIdAry[]'){
	
		global $intranet_root, $Lang, $_PAGE;
	
		if(count($columns)>0 && $sql != ''){
			include_once ($intranet_root . "/includes/libdbtable.php");
			include_once ($intranet_root . "/includes/libdbtable2007a.php");
			$li = new libdbtable2007($field, $order, $pageNo);
	
				
			$li->field_array = Get_Array_By_Key($columns,'0');
			if(is_array($columnCustArr)){
				// $columnCustArr = array(1,2,3,4,21)
				$li->column_array = $columnCustArr;
			}
			$li->sql = $sql;
			$li->no_col = sizeof($li->field_array) + 2;
			if(!$checkBox){
				$li->no_col --;
			}
			$li->IsColOff = "IP25_table";
			$li->page_size = $page_size;
				
			$pos = 0;
			$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
			foreach($columns as $_colInfo){
				$_fieldLang = $_colInfo[1];
				$_width = $_colInfo[2];
				$li->column_list .= "<th width='$_width' >".$li->column($pos++, $_fieldLang)."</th>\n";
			}
			if($checkBox){
				$li->column_list .= "<th width='1'>".$li->check($checkBoxName)."</th>\n";
			}
				
			// 			debug_pr($li->built_sql());
			// 			debug_pr($li->rs);
		}
		return $li->display();
	}
	
	public function getBusStopsDbTable($order,$field,$pageNo,$page_size,$keyword){
		global $indexVar,$Lang;
		$sql = $indexVar['db']->getBusStopsDbTableSql($keyword);
		$columns = array();
		$columns[] = array('BusStopsNameChi',$Lang['eSchoolBus']['Settings']['BusStops']['BusStopNameChi'],'50%');
		$columns[] = array('BusStopsName',$Lang['eSchoolBus']['Settings']['BusStops']['BusStopNameEng'],'50%');
		
		
		return $this->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size, $checkBox=true, $columnCustArr='');
	}
	public function getRouteDbTable($order,$field,$pageNo,$page_size,$keyword,$academicYearID){
		global $indexVar,$Lang;
		$sql = $indexVar['db']->getRouteDbTableSql($keyword,$academicYearID);
		$columns = array();
		$columns[] = array('RouteName',$Lang['eSchoolBus']['Settings']['Route']['RouteName'],'12%');
		$columns[] = array('Type',$Lang['eSchoolBus']['Settings']['Route']['Type'],'7%');
		$columns[] = array('NoOfStops',$Lang['eSchoolBus']['Settings']['Route']['NoOfStops'],'6%');
		$columns[] = array('Slot',$Lang['eSchoolBus']['Settings']['Route']['TimeScheduled'],'14%');
		$columns[] = array('Date',$Lang['eSchoolBus']['Settings']['Route']['ValidDates'],'18%');
		$columns[] = array('Vehicle',$Lang['eSchoolBus']['Settings']['Route']['VehicleNum'],'10%');
		$columns[] = array('Vehicle',$Lang['eSchoolBus']['Settings']['Route']['Teacher'],'8%');
		$columns[] = array('Status',$Lang['eSchoolBus']['Settings']['Route']['IsActive'],'6%');
		$columns[] = array('DateModified',$Lang['eSchoolBus']['Settings']['Route']['LastModified'],'19%');
		return $this->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size, $checkBox=true, $columnCustArr='');
	}
	public function getBusStopsSelection($selected="",$tags=""){
		global $indexVar,$Lang;
		$busStopsInfoAry = $indexVar['db']->getBusStopsInfo();
		$data = array();
		$busLangKey = Get_Lang_Selection('BusStopsNameChi','BusStopsName');
		foreach($busStopsInfoAry as $busInfo){
			$data[$busInfo['BusStopsID']] = $busInfo[$busLangKey];
		}
		$sel =  getSelectByAssoArray($data, $tags, $selected, $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);
		return $sel;
	}
	public function getVehicleSelection($tags, $selected="", $all=0, $noFirst=0, $FirstTitle="")
	{
		global $indexVar,$Lang;
		
		$data = $indexVar['db']->getVehicleRecords(array('RecordStatus'=>1));
		$ary = array();
		for($i=0;$i<count($data);$i++){
			$ary[] = array($data[$i]['VehicleID'], $data[$i]['CarNum']);
		}
		
		$selection = getSelectByArray($ary, $tags, $selected, $all, $noFirst, $FirstTitle, $ParQuoteValue=1);
		return $selection;
	}
	
	public function getAcademicYearSelection($tags, $selected="", $all=0, $noFirst=0, $FirstTitle="")
	{
		global $intranet_root, $Lang;
		
		include_once($intranet_root.'/includes/form_class_manage.php');
		//include_once($intranet_root.'/includes/form_class_manage_ui.php');
		
		$fcm = new form_class_manage();
		
		$year_list = $fcm->Get_Academic_Year_List($AcademicYearIDArr='', $OrderBySequence=1, $excludeYearIDArr=array(), $NoPastYear=0, $PastAndCurrentYearOnly=0, $ExcludeCurrentYear=0, $ComparePastYearID='', $SortOrder='');
		$year_list_size = count($year_list);
		$ary = array();
		
		for($i=0;$i<$year_list_size;$i++){
			$ary[] = array($year_list[$i]['AcademicYearID'], Get_Lang_Selection($year_list[$i]['YearNameB5'], $year_list[$i]['YearNameEN']));
		}
		$selection = getSelectByArray($ary, $tags, $selected, $all, $noFirst, $FirstTitle, $ParQuoteValue=1);
		return $selection;
	}
	
	public function getFormSelection($AcademicYearID, $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="")
	{
		global $intranet_root, $Lang;
		
		include_once($intranet_root.'/includes/form_class_manage.php');
		//include_once($intranet_root.'/includes/form_class_manage_ui.php');
		
		$fcm = new form_class_manage();
		
		$list = $fcm->Get_Form_List(false, $ActiveOnly=1, $AcademicYearID, $YearIDArr='', $WebSAMSCodeArr='');
		$list_size = count($list);
		$ary = array();
		
		for($i=0;$i<$list_size;$i++){
			$ary[] = array($list[$i]['YearID'], $list[$i]['YearName']);
		}
		$selection = getSelectByArray($ary, $tags, $selected, $all, $noFirst, $FirstTitle, $ParQuoteValue=1);
		
		return $selection;
	}
	
	public function getClassSelection($AcademicYearID, $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="")
	{
		global $intranet_root, $Lang;
		
		/*
		include_once($intranet_root.'/includes/form_class_manage.php');
		$fcm = new form_class_manage();
		$list = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, $__YearID='', $__TeachingOnly=0, $__YearClassIDArr='');
		
		$list_size = count($list);
		$ary = array();
		
		for($i=0;$i<$list_size;$i++){
			$ary[] = array($list[$i]['YearClassID'], Get_Lang_Selection( $list[$i]['ClassTitleB5'] , $list[$i]['ClassTitleEN']) );
		}
		$selection = getSelectByArray($ary, $tags, $selected, $all, $noFirst, $FirstTitle, $ParQuoteValue=1);
		*/
		include_once($intranet_root.'/includes/libclass.php');
		$lclass = new libclass();
		$selection = $lclass->getSelectClassID($tags, $selected, !$noFirst, $AcademicYearID, $FirstTitle, $DisplayClassIDArr='', $TeachingOnly=0);
		
		return $selection;
	}
	
	public function getStudentSelectionByClass($YearClassID,  $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="", $excludeStudentIdAry=array())
	{
		global $intranet_root, $Lang;
		
		include_once($intranet_root.'/includes/form_class_manage.php');
		$fcm = new form_class_manage();
		
		$list = $fcm->Get_Student_By_Class(array($YearClassID));
		
		$list_size = count($list);
		$ary = array();
		
		for($i=0;$i<$list_size;$i++){
			if(!in_array($list[$i]['UserID'],$excludeStudentIdAry)){
				$ary[] = array($list[$i]['UserID'], Get_Lang_Selection( $list[$i]['ClassTitleB5'] , $list[$i]['ClassTitleEN']). ($list[$i]['ClassNumber']!=''?' ('.$list[$i]['ClassNumber'].') - ':'').$list[$i]['StudentName'] );
			}
		}
		$selection = getSelectByArray($ary, $tags, $selected, $all, $noFirst, $FirstTitle, $ParQuoteValue=1);
		
		return $selection;
	}
	
	public function getRouteSelection($tags,$selected="",$type='',$AmPm='',$weekday='',$date=''){
		global $indexVar,$Lang;
		$db = $indexVar['db'];
		
		
		if($type=='N'){
			$skip_S = true;
		}else if($type=='S'){
			$skip_N = true;
		}else{
			
		}
		
		$additionCond_N = array();
		$additionCond_S = array();
		if(!$AmPm==''){
			$additionCond_N = array('AmPm'=>$AmPm);
		}
		if(!$weekday==''){
			$additionCond_S = array('Weekday'=>$weekday);
		}
		if(!$date=='')$dateCondition = " AND route.StartDate <= '$date' AND route.EndDate >= '$date' ";
		($skip_N==false)?$routeInfo_N = $db->getRouteInfo($routeIDAry='',$isDeleted=0,$type='N',$additionCond_N,$dateCondition,false):'';
		($skip_S==false)?$routeInfo_S = $db->getRouteInfo($routeIDAry='',$isDeleted=0,$type='S',$additionCond_S,$dateCondition,false):'';
		foreach((array)$routeInfo_N as $routeInfo){
			$dataAry[$Lang['eSchoolBus']['Settings']['Route']['NormalRoute']][$routeInfo['RouteID']] = $routeInfo['RouteName'];
		}
		foreach((array)$routeInfo_S as $routeInfo){
			$dataAry[$Lang['eSchoolBus']['Settings']['Route']['SpecialRoute']][$routeInfo['RouteID']] = $routeInfo['RouteName'];
		}
		return getSelectByAssoArray($dataAry, $tags, $selected, $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);
	}
	
	public function getRouteCollectionSelection($academicYearId, $type, $dayType,$tags, $selected="", $all=0, $noFirst=0, $FirstTitle="", $extraOptions=array(),$targetDate='')
	{
		global $indexVar,$Lang;
		
		$params = array('AcademicYearID'=>$academicYearId,'Type'=>$type,'DayType'=>$dayType,'Status'=>1);
		if($targetDate != ''){
			// only get routes that target date fall in route effective date range
			$params['Date'] = $targetDate;
		}
		$list = $indexVar['db']->getRouteCollectionByType($params);
		
		$list_size = count($list);
		$ary = array();
		
		for($i=0;$i<count($extraOptions);$i++){
			$ary[] = $extraOptions[$i];
		}
		for($i=0;$i<$list_size;$i++){
			$ary[] = array($list[$i]['RouteCollectionID'], $list[$i]['RouteName'] );
		}
		$selection = getSelectByArray($ary, $tags, $selected, $all, $noFirst, $FirstTitle, $ParQuoteValue=1);
		
		return $selection;
	}
	
	function getBusStopsSelectionByRouteCollection($routeCollectionIdAry, $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="")
	{
		global $indexVar,$Lang;
	
		$busNameField = Get_Lang_Selection('bs.BusStopsNameChi','bs.BusStopsName');
		
		$list = $indexVar['db']->getBusStopsByRouteCollection($routeCollectionIdAry);
		
		$list_size = count($list);
		$ary = array();
		
		for($i=0;$i<$list_size;$i++){
			$ary[] = array($list[$i]['BusStopsID'], $list[$i]['BusStopsName'] );
		}
		$selection = getSelectByArray($ary, $tags, $selected, $all, $noFirst, $FirstTitle, $ParQuoteValue=1);
		
		return $selection;
	}
	
	public function getActivitySelection($tags, $selected="", $all=0, $noFirst=0, $FirstTitle="")
	{
		global $indexVar, $Lang, $schoolBusConfig;
		
		$selection = getSelectByArray($schoolBusConfig['ActivityTypes'], $tags, $selected, $all, $noFirst, $FirstTitle, $ParQuoteValue=1);
		
		return $selection;
	}
	public function getAttendanceStatusSelection($tags, $selected="", $all=0, $noFirst=0, $FirstTitle=""){
		global $indexVar, $Lang, $schoolBusConfig;
		$selection = getSelectByArray($schoolBusConfig['AttendanceStatus'], $tags, $selected, $all, $noFirst, $FirstTitle, $ParQuoteValue=1);
		return $selection;
	}
	public function getAttendanceForm($routeID,$date,$amPm){
		global $indexVar, $Lang,$PATH_WRT_ROOT, $LAYOUT_SKIN;
		$db = $indexVar['db'];
		$routeInfo = $db->getSingleRouteInfo($routeID,$date);
		$type = $routeInfo['Type'];
		$x = '';
		$x .= '<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tbody>
					<tr>
						<td>'.$this->GET_NAVIGATION2_IP25($routeInfo['RouteName']).'</td>
					</tr>
					</tbody>
				</table>';
		
		//Get Current Route Collection
		if($type=='N'){
			$routeCollectionByAMPM = BuildMultiKeyAssoc($routeInfo['RouteCollection'], "AmPm");
			$currentCollection = $routeCollectionByAMPM[$amPm];
		}else if($type=='S'){
			$routeCollectionByWeekday = BuildMultiKeyAssoc($routeInfo['RouteCollection'], "Weekday");
			$weekday_num = date("N", strtotime($date));
			switch($weekday_num){
				case 1:
					$weekday = 'MON';
					break;
				case 2:
					$weekday = 'TUE';
					break;
				case 3:
					$weekday = 'WED';
					break;
				case 4:
					$weekday = 'THU';
					break;
				case 5:
					$weekday = 'FRI';
					break;
				default:
					echo 'error';
					return;
					break;
			}
			$currentCollection = $routeCollectionByWeekday[$weekday];
		}
// 		debug_pr($currentCollection);
		$busStopsAssoAryByCollectionID = BuildMultiKeyAssoc($routeInfo['BusStops'], 'RouteCollectionID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		$currentBusStops = $busStopsAssoAryByCollectionID[$currentCollection['RouteCollectionID']];
		$userRouteAssoAryByRouteCollection = BuildMultiKeyAssoc($routeInfo['UserRoute'], 'RouteCollectionID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		$currentUserAry = $userRouteAssoAryByRouteCollection[$currentCollection['RouteCollectionID']];
		$currentUserAssoAryByBusStopsID = BuildMultiKeyAssoc($currentUserAry, 'BusStopsID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		$userIDAry = Get_Array_By_Key($currentUserAry,'UserID');
// 		debug_pr($userIDAry);

		//Get Existing Attendance info
		$attendanceInfo = $db->getAttendanceInfo($currentCollection['RouteCollectionID'],$userIDAry,$date);
		$attendanceAssoAryByUser = BuildMultiKeyAssoc($attendanceInfo, 'UserID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);

		if($type=='N'&&$amPm=='PM'){
			//Get Today ASA route, check is anyone takes ASA route rather than this route
			$specialRouteAry = $db->getAllSpecialRouteByDate($date);
			$specialRouteCollectionIDAry = array_unique(Get_Array_By_Key($specialRouteAry,'RouteCollectionID'));
// 			debug_pr($specialRouteAry);
// 			debug_pr($userIDAry);
			$specialUserRouteAry = $db->getUserRouteRecords($filterMap=array('UserID'=>$userIDAry,'RouteCollectionID'=>$specialRouteCollectionIDAry),$additionalCond='',$includeUserName=false);
			$specialUserRouteAssoAryByUser = BuildMultiKeyAssoc($specialUserRouteAry, 'UserID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
// 			debug_pr($specialUserRouteAssoAryByUser);
		}
		
		$specialArrangementAry = $routeInfo['SpecialArrangement'];
		$specialArrangementAssoAryByUser = BuildMultiKeyAssoc($specialArrangementAry, 'UserID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		$specialArrangementAssoAryByRouteCollection = BuildMultiKeyAssoc($specialArrangementAry, 'RouteCollectionID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		//$specialArrangementAssoAryByUser ######For searching is the user getting other route?
		//$specialArrangementAssoAryByRouteCollection #####For searching is anyone get on this route today?
		if(!empty($specialArrangementAssoAryByRouteCollection[$currentCollection['RouteCollectionID']])){
			$currentSpeicalArrangementAssoAryByBusStops = BuildMultiKeyAssoc($specialArrangementAssoAryByRouteCollection[$currentCollection['RouteCollectionID']], 'BusStopsID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		}
		//Loop every stops
// 		debug_pr($specialUserRouteAssoAryByUser);
		foreach((array)$currentBusStops as $busStopInfo){
			$time = date('H:i',strtotime($busStopInfo['Time']));
			$busStopName = $busStopInfo[Get_Lang_Selection('BusStopsNameChi','BusStopsName')];
			$busStopID = $busStopInfo['BusStopsID'];
			
			//add class for tr -> 'row_[busstoppsID]'
			$additionalClassForRow = "row_".$busStopID;
			
			$x .= '<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center"> ';
				$x .= '<colgroup>';
					$x .= '<col width="21%" />';
					$x .= '<col width="20%" />';
					$x .= '<col width="15%" />';
					$x .= '<col width="30%" />';
					$x .= '<col width="15%" />';
				$x .= '</colgroup>';
				$x .= '<tr class="tabletop"> ';
					$x .= '<td>'.$Lang['eSchoolBus']['Management']['Attendance']['TimeAndStopName'].'</td> ';
					$x .= '<td>'.$Lang['eSchoolBus']['Management']['Attendance']['StudentName'].'</td> ';
					$x .= '<td>'.$Lang['eSchoolBus']['Management']['Attendance']['Status'].' ';
						$x.= $this->getAttendanceStatusSelection($tags='onchange="pushToAllStatus(this.value,'.$busStopID.')"', $selected="", $all=0, $noFirst=0, $FirstTitle="");
					$x .= '</td>';
					$x .= '<td>'.$Lang['eSchoolBus']['Management']['Attendance']['Remarks'].'</td>';
					$x .= '<td>'.$Lang['eSchoolBus']['Management']['Attendance']['ContactNumber'].'</td>';
				$x .= '</tr>';
					
				//loop every students in the stops
				$studentCounter = 0;
				$countOfStudents = count($currentUserAssoAryByBusStopsID[$busStopID]);
				
				foreach((array)$currentUserAssoAryByBusStopsID[$busStopID] as $_userRoute){
					$_takeSpecialRoute = false;
					$_haveSpecialArrangment = false;

					if($studentCounter%2==1){
						$_trclass="tablerow2";
					}else{
						$_trclass="tablerow";
					}
					
					//Check if student take ASA Route
					if(!empty($specialUserRouteAssoAryByUser[$_userRoute['UserID']])){
						$_takeSpecialRoute = true;
					}
					
					//Check if student have speical arrangement
					if(!empty($specialArrangementAssoAryByUser[$_userRoute['UserID']])){
						if($type=='S'){
							$amPm = 'PM';	//all ASA is PM
						}
						$_speicalArragmentByAmPm = BuildMultiKeyAssoc($specialArrangementAssoAryByUser[$_userRoute['UserID']], 'DayType', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
						if(!empty($_speicalArragmentByAmPm[$amPm])){
							//HAVE SPECIAL ARRANGEMENT
							$_haveSpecialArrangment = true;
						}
					}
					
					$x .= '<tr class="'.$_trclass.' '.$additionalClassForRow.'">';
					if($studentCounter==0){
						$x .= '<td rowspan="'.$countOfStudents.'"><h4>'.$time.'<br>'.$busStopName.'</h4></td>';
					}
					$x .= '<td class="tabletext" >'.$_userRoute['UserName'].'</td>';
					
					if($_haveSpecialArrangment){
						if($_speicalArragmentByAmPm[$amPm]['RouteCollectionID']==0){
							$_info = $Lang['eSchoolBus']['AttendanceStatusArr']['Parent'];
						}else{
							$_info = $_speicalArragmentByAmPm[$amPm]['RouteName'].' '.$_speicalArragmentByAmPm[$amPm]['BusStopsName'];
						}
						$x .= '<td class="tabletext" colspan="2">';
							$x .= $Lang['eSchoolBus']['Management']['Attendance']['SpecialArrangement'].' ('.$_info.')';
						$x .= '</td>';
					}else if($_takeSpecialRoute){
						
						if($specialUserRouteAssoAryByUser[$_userRoute['UserID']]['RouteCollectionID']==0){
							$_info = $Lang['eSchoolBus']['AttendanceStatusArr']['Parent'];
						}else{
							$_info = $specialUserRouteAssoAryByUser[$_userRoute['UserID']]['RouteName'].' '.$specialUserRouteAssoAryByUser[$_userRoute['UserID']]['BusStopsName'];
						}
						
						$x .= '<td class="tabletext" colspan="2">';
						$x .= $Lang['eSchoolBus']['Management']['Attendance']['HaveASA'].' ('.$_info.')';
						$x .= '</td>';
					}else{
						$x .= '<td class="tabletext">';
						$x .= $this->getAttendanceStatusSelection($tags='name="status['.$_userRoute['UserID'].']"', $selected=$attendanceAssoAryByUser[$_userRoute['UserID']]['Status'], $all=0, $noFirst=0, $FirstTitle="");
						
						$x .= '</td>';
						$x .= '<td class="tabletext">';
						$x .= '<input type="text" name="remarks['.$_userRoute['UserID'].']" id="remarks_'.$_userRoute['UserID'].'" class="textboxnum" maxlength="255" value="'.$attendanceAssoAryByUser[$_userRoute['UserID']]['Remarks'].'">';
						$x .= '</td>';
					}
					$x .= '<td>';
					$x .= '<a href="javascript:void(0);" onclick="showContact(this,'.$_userRoute['UserID'].');"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/icon_view.gif" alt="" border="0"></a>';
					$x .= '</td>';
					$x .= '</tr>';
					
					$studentCounter++;
				}	
				
			//Speical Arrangement => specially take this route
			if($countOfStudents==0){
				if(!empty($currentSpeicalArrangementAssoAryByBusStops[$busStopID])){
					//need rowspan
					$_rowspan = 'rowspan="'.count($currentSpeicalArrangementAssoAryByBusStops[$busStopID]).'" ';
				}
				$additionalClassForRow = "row_".$busStopID;
				$x .= '<tr class="tablerow '.$additionalClassForRow.'">';
				$x .= '<td '.$_rowspan.'><h4>'.$time.'<br>'.$busStopName.'</h4></td>';
			}
			if(!empty($currentSpeicalArrangementAssoAryByBusStops[$busStopID])){
				$_specialArrStudentsCounter = 0; 
				foreach($currentSpeicalArrangementAssoAryByBusStops[$busStopID] as $__data){
					if($_specialArrStudentsCounter>0){
						//add class for tr -> 'row_[busstoppsID]'
						$additionalClassForRow = "row_".$busStopID;
						$x .= '<tr class="tablerow '.$additionalClassForRow.'">';
					}
					$x .= '<td class="tabletext special">'.$__data['UserName'].' '.'<br>('.$Lang['eSchoolBus']['Management']['Attendance']['SpecialArrangement2'].')'.'</td>';
					$x .= '<td class="tabletext special">';
						$x .= $this->getAttendanceStatusSelection($tags='name="status['.$__data['UserID'].']"', $selected=$attendanceAssoAryByUser[$__data['UserID']]['Status'], $all=0, $noFirst=0, $FirstTitle="");
					$x .= '</td>';
					$x .= '<td class="tabletext special">';
					$x .= '<input type="text" name="remarks['.$__data['UserID'].']" id="remarks_'.$__data['UserID'].'" class="textboxnum" maxlength="255" value="'.$attendanceAssoAryByUser[$__data['UserID']]['Remarks'].'">';
					$x .= '</td>';
					$x .= '<td class="tabletext special">';
					$x .= '<a href="javascript:void(0);" onclick="showContact(this,'.$_userRoute['UserID'].');"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/icon_view.gif" alt="" border="0"></a>';
					$x .= '</td>';
					if($_specialArrStudentsCounter>0){
						$x .= '</tr>';
					}
				}
			}
			else if($countOfStudents==0){
				$x .= '<td colspan="3"><center>'.$Lang['eSchoolBus']['Management']['Attendance']['NoStudents'].'</center></td><td></td>';
			}
				
			$x .= '</table>';
			$x .= '<br>';
		}
		
		$x .= '<br style="clear:both;" />';
			$x .= '<div class="edit_bottom_v30">';
				$x .= '<p class="spacer"></p>';
				$x .= $this->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
				
				$x .= '<p class="spacer"></p>';
			$x .= '</div>';
		$x .= '<input type="hidden" name="routeCollectionID" value="'.$currentCollection['RouteCollectionID'].'" />';
		$x .= '<input type="hidden" name="date" value="'.$date.'" />';
		
		return $x;
	}
	public function getStudentViewTableByMonth($studentId,$year,$month){
		global $Lang, $indexVar;
		
		$dateStart = date("Y-m-01",strtotime($year.'-'.$month.'-1'));
		$dateEnd = date("Y-m-t",strtotime($year.'-'.$month.'-1'));
		
		//Get User Route info
		$db = $indexVar['db'];
		$userRouteInfo = $db->getStudentBusSchedule($studentId,$dateStart,$dateEnd);
		
		//Get Holiday
		$sql = "SELECT
		Title, DATE(EventDate) AS Date
		FROM
		INTRANET_EVENT AS a
		WHERE
		a.EventDate BETWEEN '$dateStart' AND '$dateEnd' AND RecordType IN (3,4)
		ORDER By
		a.EventDate, a.RecordType DESC";
		$holidays = $db->returnResultSet($sql);
		$holidays = BuildMultiKeyAssoc($holidays, 'Date', $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 1);
		
		$firstDayWeakDay = date( "N", strtotime($dateStart));
		if($firstDayWeakDay>=2 && $firstDayWeakDay<=5){
			//need to add empty column
			$firstRowEmptyColNum = $firstDayWeakDay-1;
		}else{
			$firstRowEmptyColNum = 0;
		}

		$lastDayWeakDay = date( "N", strtotime($dateEnd));
		if($lastDayWeakDay>=1 && $lastDayWeakDay<=4){
			//need to add empty column
			$lastRowEmptyColNum = 5-$lastDayWeakDay;
		}else{
			$lastRowEmptyColNum = 0;
		}
		
		//Get all dats between two date
		$weekday = date( "N", strtotime($dateStart));
		if($weekday>=1 &&$weekday<=5){
			$dates = array($dateStart);
		}
		$_tmp_date = $dateStart;
		while($_tmp_date < $dateEnd){
			$_date = date('Y-m-d', strtotime($_tmp_date.' +1 day'));
			$_weekday = date( "N", strtotime($_date));
			if($_weekday>=1 &&$_weekday<=5){
				$dates[] = $_date;
			}
			$_tmp_date = $_date;
		}
		//Count no of row is needed
		$numOfRow = (($firstRowEmptyColNum + $lastRowEmptyColNum + count($dates))/5);
		
		//Add empty column into dates
		$dateArry = array();
		for($i=0;$i<$firstRowEmptyColNum;$i++){
			$dateArry[] = '';
		}
		foreach($dates as $_date){
			$dateArry[] = $_date;
		}
		for($i=0;$i<$lastRowEmptyColNum;$i++){
			$dateArry[] = '';
		}
		$x = '';
		//Title
		$x .= '<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tbody>
					<tr>
						<td>'.$this->GET_NAVIGATION2_IP25($Lang['eSchoolBus']['eService']['View']['Student']).'</td>
					</tr>
					</tbody>
				</table>';
		
		//Table header
		$x .= '<table width="96%" border="1" cellpadding="5" cellspacing="0" align="center">';
			for ($row=0;$row<$numOfRow;$row++){
				$x .= '<tr class="tabletop">';
				$x .= '<td>'.$Lang['eSchoolBus']['eService']['StudentView']['Time'].'</td>';
				for($i=0;$i<5;$i++){
					$__date = $dateArry[($i+($row*5))];
					$x .= '<td>'.$Lang['eSchoolBus']['Settings']['Route']['Weekday'][($i+1)];
					$x .= ($__date)?' ('.date('d/m',strtotime($__date)).')':'';
					$x .= '</td>';
				}
				$x .= '</tr>';
				
				// AM schedule
				$x .= '<tr class="tablerow1">';
				$x .= '<td>'.$Lang['eSchoolBus']['Settings']['Student']['AM'].'</td>';
				$_tmp_normal_start_date = '';
				$_tmp_no_of_day = 1;
				$_tmp_array = array();
				for($i=0;$i<5;$i++){
					$__date = $dateArry[($i+($row*5))];
					$__weekday = $this->getWeekdayKey(($i+1));
					$__columnContent = '';
					$__colspan = 1;
					if($__date){
						//check special arragement
						if(!empty($holidays[$__date])){
							// is holiday
							if(empty($_tmp_normal_start_date)){
								// do nothing
							}else{
								//add to tmp array
								$_tmp_array['N'][$_tmp_normal_start_date] = $_tmp_no_of_day;
								//reset tmp value
								$_tmp_normal_start_date = '';
								$_tmp_no_of_day = 1;
							}
							$_tmp_array['Holiday'][$__date] = 1;
						}else if(!empty($userRouteInfo['SpecialArrangement'][$__date]['AM'])){
							//have special arrangement in AP
							if(empty($_tmp_normal_start_date)){
								// do nothing
							}else{
								//add to tmp array
								$_tmp_array['N'][$_tmp_normal_start_date] = $_tmp_no_of_day;
								//reset tmp value
								$_tmp_normal_start_date = '';
								$_tmp_no_of_day = 1;
							}
							$_tmp_array['SpecialArrangement'][$__date] = 1;
						}else if(!empty($userRouteInfo['N']['AM'])){
							//have normal route
							if($userRouteInfo['N']['AM'][0]['DateStart']<=$__date&&$userRouteInfo['N']['AM'][0]['DateEnd']>=$__date){
								if(empty($_tmp_normal_start_date)){
									$_tmp_normal_start_date = $__date;
								}else{
									$_tmp_no_of_day++;
								}
							}
						}
					}
				}
				//add to tmp array
				if(!empty($_tmp_normal_start_date)){
					$_tmp_array['N'][$_tmp_normal_start_date] = $_tmp_no_of_day;
				}
				for($i=0;$i<5;$i++){
					$_date = $dateArry[($i+($row*5))];
					$_weekday = $this->getWeekdayKey(($i+1));
					if($_date){
						if(!empty($_tmp_array['Holiday'][$_date])){
							$x .= '<td bgcolor="#FFBBBB" rowspan="2"><center>';
								$x .= $holidays[$_date][0]['Title'];
							$x .= '</td></center>';
						}else if(!empty($_tmp_array['SpecialArrangement'][$_date])){
							$x .= '<td bgcolor="#99FFBB"><center>';
							if($userRouteInfo['SpecialArrangement'][$_date]['AM'][0]['RouteCollectionID']==0){
								$x .= $Lang['eSchoolBus']['Settings']['Student']['ByParent'];
							}else{
								($userRouteInfo['SpecialArrangement'][$_date]['AM'][0]['Cars'])?$x .= $userRouteInfo['SpecialArrangement'][$_date]['AM'][0]['Cars'].'<br>':'';
								($userRouteInfo['SpecialArrangement'][$_date]['AM'][0]['Time'])?$x .= date('h:i',strtotime($userRouteInfo['SpecialArrangement'][$_date]['AM'][0]['Time'])).'<br>':'';
								$x .= $userRouteInfo['SpecialArrangement'][$_date]['AM'][0]['RouteName'];
							}
							$x .= '</center></td>';
						}else if($_tmp_array['N'][$_date]){
							$userRouteInfo['N']['AM'][0]['RouteName'];
							$x .= '<td colspan="'.$_tmp_array['N'][$_date].'"><center>';
							($userRouteInfo['N']['AM'][0]['Cars'])?$x .= $userRouteInfo['N']['AM'][0]['Cars'].'<br>':'';
							($userRouteInfo['N']['AM'][0]['Time'])?$x .= date('h:i',strtotime($userRouteInfo['N']['AM'][0]['Time'])).'<br>':'';
							$x .= $userRouteInfo['N']['AM'][0]['RouteName'];
							$x .= '</center></td>';
							$i = $i - 1 +$_tmp_array['N'][$_date];
						}else{
							$x .= '<td></td>';
						}
					}else{
						$x .= '<td></td>';
					}
				}
				$x .= '</tr>';
				
				// PM schedule
				$x .= '<tr class="tablerow2">';
				$x .= '<td>'.$Lang['eSchoolBus']['Settings']['Student']['PM'].'</td>';
				$_tmp_normal_start_date = '';
				$_tmp_no_of_day = 1;
				$_tmp_array = array();
				for($i=0;$i<5;$i++){
					$__date = $dateArry[($i+($row*5))];
					$__weekday = $this->getWeekdayKey(($i+1)); 
					$__columnContent = '';
					$__colspan = 1;
					if($__date){
						//check special arragement
						if(!empty($holidays[$__date])){
							// is holiday
							if(empty($_tmp_normal_start_date)){
								// do nothing
							}else{
								//add to tmp array
								$_tmp_array['N'][$_tmp_normal_start_date] = $_tmp_no_of_day;
								//reset tmp value
								$_tmp_normal_start_date = '';
								$_tmp_no_of_day = 1;
							}
							$_tmp_array['Holiday'][$__date] = 1;
						}else if(!empty($userRouteInfo['SpecialArrangement'][$__date]['PM'])){
							//have special arrangement in AP
							if(empty($_tmp_normal_start_date)){
							// do nothing
							}else{
								//add to tmp array
								$_tmp_array['N'][$_tmp_normal_start_date] = $_tmp_no_of_day;
								//reset tmp value
								$_tmp_normal_start_date = '';
								$_tmp_no_of_day = 1;
							}
							$_tmp_array['SpecialArrangement'][$__date] = 1;
						}else if(!empty($userRouteInfo['S'][$__weekday])){
							if($userRouteInfo['S'][$__weekday][0]['DateStart']<=$__date&&$userRouteInfo['S'][$__weekday][0]['DateEnd']>=$__date){
								if(empty($_tmp_normal_start_date)){
								// do nothing
								}else{
									//add to tmp array
									$_tmp_array['N'][$_tmp_normal_start_date] = $_tmp_no_of_day;
									//reset tmp value
									$_tmp_normal_start_date = '';
									$_tmp_no_of_day = 1;
								}
								$_tmp_array['S'][$__date] = 1;
							}
						}else if(!empty($userRouteInfo['N']['PM'])){
							//have normal route
							if($userRouteInfo['N']['PM'][0]['DateStart']<=$__date&&$userRouteInfo['N']['PM'][0]['DateEnd']>=$__date){
								if(empty($_tmp_normal_start_date)){
									$_tmp_normal_start_date = $__date;
								}else{
									$_tmp_no_of_day++;
								}
							}
						}
					}
				}
				//add to tmp array
				if(!empty($_tmp_normal_start_date)){
					$_tmp_array['N'][$_tmp_normal_start_date] = $_tmp_no_of_day;
				}
				for($i=0;$i<5;$i++){
					$_date = $dateArry[($i+($row*5))];
					$_weekday = $this->getWeekdayKey(($i+1));
					if($_date){
						if(!empty($_tmp_array['Holiday'][$_date])){
// 							$x .= '<td bgcolor="#FFBBBB"><center>';
// 								$x .= $holidays[$_date][0]['Title'];
// 							$x .= '</td></center>';
						}else if(!empty($_tmp_array['SpecialArrangement'][$_date])){
							$x .= '<td bgcolor="#99FFBB"><center>';
							if($userRouteInfo['SpecialArrangement'][$_date]['PM'][0]['RouteCollectionID']==0){
								$x .= $Lang['eSchoolBus']['Settings']['Student']['ByParent'];
							}else{
									($userRouteInfo['SpecialArrangement'][$_date]['PM'][0]['Cars'])?$x .= $userRouteInfo['SpecialArrangement'][$_date]['PM'][0]['Cars'].'<br>':'';
									($userRouteInfo['SpecialArrangement'][$_date]['PM'][0]['Time'])?$x .= date('h:i',strtotime($userRouteInfo['SpecialArrangement'][$_date]['PM'][0]['Time'])).'<br>':'';
									$x .= $userRouteInfo['SpecialArrangement'][$_date]['PM'][0]['RouteName'];
							}
							$x .= '</center></td>';
						}else if(!empty($_tmp_array['S'][$_date])){
							
							$x .= '<td bgcolor="#99FFBB"><center>';
								if($userRouteInfo['S'][$_weekday][0]['RouteCollectionID']==0){
									$x .= $Lang['eSchoolBus']['Settings']['Student']['ByParent'];
								}else{
									($userRouteInfo['S'][$_weekday][0]['Cars'])?$x .= $userRouteInfo['S'][$_weekday][0]['Cars'].'<br>':'';
									($userRouteInfo['S'][$_weekday][0]['Time'])?$x .= date('h:i',strtotime($userRouteInfo['S'][$_weekday][0]['Time'])).'<br>':'';
									$x .= $userRouteInfo['S'][$_weekday][0]['RouteName'];
								}
							$x .= '</center></td>';
						}else if($_tmp_array['N'][$_date]){
							$userRouteInfo['N']['PM'][0]['RouteName'];
							$x .= '<td colspan="'.$_tmp_array['N'][$_date].'"><center>';
								($userRouteInfo['N']['PM'][0]['Cars'])?$x .= $userRouteInfo['N']['PM'][0]['Cars'].'<br>':'';
								($userRouteInfo['N']['PM'][0]['Time'])?$x .= date('h:i',strtotime($userRouteInfo['N']['PM'][0]['Time'])).'<br>':'';
								$x .= $userRouteInfo['N']['PM'][0]['RouteName'];
							$x .= '</center></td>';
							$i = $i - 1 +$_tmp_array['N'][$_date];
						}else{
							$x .= '<td></td>';
						}
					}else{
						$x .= '<td></td>';
					}
				}
				$x .= '</tr>';
			}
		
		$x .= '</table>';
		
		return $x;
	}
	
	public function getWeekdayKey($num){
		switch($num){
			case 1:
				$weekday = 'MON';
				break;
			case 2:
				$weekday = 'TUE';
				break;
			case 3:
				$weekday = 'WED';
				break;
			case 4:
				$weekday = 'THU';
				break;
			case 5:
				$weekday = 'FRI';
				break;
			case 6:
				$weekday = 'SAT';
			    break;
			case 7:
				$weekday = 'SUN';
			    break;
			default:
				return array();
				break;
		}
		return $weekday;
	}
	function Get_User_Selection($groupCat,$AddUserID='',$multiple=true) {
		global $Lang,$_PAGE,$indexVar;
		$groupType = substr($groupCat,0,1);
		$groupID = substr($groupCat,1);
	
		switch ($groupType){
			case 'I':
				if($groupID==1){
					$teaching = 1;
				}else if($groupID==3){
					$teaching = 0;
				}
				$db = $indexVar['db'];
				$teachingStaff = $db -> getStaffUserInfo(1,$AddUserID);
	
				$nonTeachingStaff = $db -> getStaffUserInfo(0,$AddUserID);
				if($teaching){
					$UserList = $teachingStaff;
				}else{
					$UserList = $nonTeachingStaff;
				}
				break;
			case 'G':
				$groupObj = new libgroup($groupID);
				$UserList = ($groupObj->returnGroupUser());
				break;
		}
	
		if ($AddUserID== "")
			$AddUserID = array();
	
			if($multiple){
				$isMultiple = 'multiple = "true" ';
			}else{
				$isMultiple = '';
			}
	
			$x .= '<select name="AvalUserList[]" id="AvalUserList[]" size="10" style="width:99%" '.$isMultiple.'>';
	
			for ($i=0; $i< sizeof($UserList); $i++) {
				$x .= '<option value="'.$UserList[$i]['UserID'].'">';
				$x .= Get_Lang_Selection($UserList[$i]['ChineseName'],$UserList[$i]['EnglishName']);
				$x .= '</option>';
			}
			$x .= '</select>';
	
			return $x;
	}
	
	// used in parent app -> bus take schedule
	function getLeaveApplicationStatusDisplay($statusCode)
	{
	    global $Lang, $image_path, $LAYOUT_SKIN;
	    
	    $path = $image_path.'/'.$LAYOUT_SKIN.'/';
	    $x = '';
	    switch($statusCode) {
	        case '1':      // waiting aproval
	            $x = '<span><img title="'.$Lang['eSchoolBus']['App']['Schedule']['Pending'].'" src="'.$path.'icon_waiting.gif" border="0" align="absmiddle"></span><br>';
	            $x .= '<span class="pending">'.$Lang['eSchoolBus']['App']['Schedule']['Pending'].'</span>';
	            break;
	        case '2':      // approved 
	            $x = '<span><img title="'.$Lang['eSchoolBus']['App']['Schedule']['AppliedLeave'].'" src="'.$path.'icon_approve_b.gif" border="0" align="absmiddle"></span><br>';
	            $x .= '<span class="approved">'.$Lang['eSchoolBus']['App']['Schedule']['AppliedLeave'].'</span>';
	            break;
	        case '3':      // rejected
	            $x = '<span><img title="'.$Lang['eSchoolBus']['App']['Schedule']['Rejected'].'" src="'.$path.'icon_reject_l.gif" border="0" align="absmiddle"></span>';
	            $x .= '<span class="rejected">'.$Lang['eSchoolBus']['App']['Schedule']['Rejected'].'</span>';
	            break;
	        case '5':      // absent
	            $x = '<span><img title="'.$Lang['eSchoolBus']['App']['Schedule']['Absent'].'" src="'.$path.'icon_absent.gif" border="0" align="absmiddle"></span>';
	            $x .= '<span class="absent">'.$Lang['eSchoolBus']['App']['Schedule']['Absent'].'</span>';
	            break;
	    }
	    return $x;
	}
	
	// used in parent app -> history application
	function getLeaveApplicationStatusHistoryDisplay($statusCode)
	{
	    global $Lang, $image_path, $LAYOUT_SKIN;
	    
	    $path = $image_path.'/'.$LAYOUT_SKIN.'/';
	    $x = '';
	    switch($statusCode) {
	        case '1':      // waiting aproval
	            $x = '<span><img title="'.$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Pending'].'" src="'.$path.'icon_waiting.gif" border="0" align="absmiddle"></span>';
	            $x .= '<span class="pending">'.$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Pending'].'</span>';
	            break;
	        case '2':      // approved
	            $x = '<span><img title="'.$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Approved'].'" src="'.$path.'icon_approve_b.gif" border="0" align="absmiddle"></span>';
	            $x .= '<span class="approved">'.$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Approved'].'</span>';
	            break;
	        case '3':      // rejected
	            $x = '<span><img title="'.$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Rejected'].'" src="'.$path.'icon_reject_l.gif" border="0" align="absmiddle"></span>';
	            $x .= '<span class="rejected">'.$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Rejected'].'</span>';
	            break;
	        case '4':      // cancelled
	            $x = '<span><img title="'.$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Cancelled'].'" src="'.$path.'icon_cancel.png" border="0" align="absmiddle" class="resizeCancel"></span>';
	            $x .= '<span class="cancelled">'.$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Cancelled'].'</span>';
	            break;
	        case '5':      // absent
	            $x = '<span><img title="'.$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Absent'].'" src="'.$path.'icon_absent.gif" border="0" align="absmiddle"></span>';
	            $x .= '<span class="absent">'.$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Absent'].'</span>';
	            break;
	        case '6':      // deleted
	            $x = '<span><img title="'.$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Deleted'].'" src="'.$path.'icon_delete.gif" border="0" align="absmiddle"></span>';
	            $x .= '<span class="deleted">'.$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Deleted'].'</span>';
	            break;
	    }
	    return $x;
	}
	
	function getScheduleCellDisplay($schedule, $recordStatus='') 
	{
	    global $schoolBusConfig, $Lang, $sys_custom;
	    
 	    if ($schedule['RouteID']) {
    	    $routeName = $schedule['RouteName'];
    	    $x = $routeName;
    	    if (!$sys_custom['eSchoolBus']['parentApp']['schedule']['hideTimeAndPlace']) {
        	    $time = substr($schedule['Time'],0,5);
        	    $busStop = $schedule['BusStopsName'];
        	    $x .= '<br>'.$time.'<br>'.$busStop;
    	    }
    	    if ($recordStatus == $schoolBusConfig['ApplyLeaveStatus']['Rejected']) {
    	        $x .= '<br>'.$this->getLeaveApplicationStatusDisplay($recordStatus);
    	    }
	    }
	    else {
	        $x = '<span>'.$Lang['eSchoolBus']['AttendanceStatusArr']['Parent'].'</span>';
	    }
	    return $x;
	}
	
	function getGotoSchoolDisplay($userRouteInfo, $date, $recordStatus='')
	{
	    global $Lang;
	    
	    // Special arrangement of that date
	    if (!empty($userRouteInfo['SpecialArrangement'][$date]['AM'][0])) {
	        $schedule = $userRouteInfo['SpecialArrangement'][$date]['AM'][0];
	        $x = $this->getScheduleCellDisplay($schedule, $recordStatus);
	    }
	    else {
	        // Normal route
	        if (!empty($userRouteInfo['N']['AM'][0])) {
	            $schedule = $userRouteInfo['N']['AM'][0];
	            
	            // valid date
	            if (($date >= $schedule['DateStart']) && ($date <= $schedule['DateEnd'])) {
	                $x = $this->getScheduleCellDisplay($schedule, $recordStatus);
	            }
	            else {
	                $x = $Lang['eSchoolBus']['App']['Schedule']['NA'];
	            }
	        }
	        else {
	            $x = $Lang['eSchoolBus']['App']['Schedule']['NA'];
	        }
	    }
	    return $x;
	}
	
	function getLeaveSchoolDisplay($userRouteInfo, $date, $weekDayStr, $recordStatus='')
	{
	    global $Lang;
	    
	    if (!empty($userRouteInfo['SpecialArrangement'][$date]['PM'][0])) {
	        $schedule = $userRouteInfo['SpecialArrangement'][$date]['PM'][0];
	        $x = $this->getScheduleCellDisplay($schedule, $recordStatus);
	    }
	    else {
	        if (!empty($userRouteInfo['S'][$weekDayStr][0])) {
	            $schedule = $userRouteInfo['S'][$weekDayStr][0];

	            // Has After School Activity (ASA route)
	            if (($date >= $schedule['DateStart']) && ($date <= $schedule['DateEnd'])) {
	                $x = $this->getScheduleCellDisplay($schedule, $recordStatus);
	            }
	            else {
	                $x = $Lang['eSchoolBus']['App']['Schedule']['NA'];
	            }
	        }
	        else {
	            // Normal route
	            if (!empty($userRouteInfo['N']['PM'][0])) {
	                $schedule = $userRouteInfo['N']['PM'][0];
	                
	                // valid date
	                if (($date >= $schedule['DateStart']) && ($date <= $schedule['DateEnd'])) {
	                    $x = $this->getScheduleCellDisplay($schedule, $recordStatus);
	                }
	                else {
	                    $x = $Lang['eSchoolBus']['App']['Schedule']['NA'];
	                }
	            }
	            else {
	                $x = $Lang['eSchoolBus']['App']['Schedule']['NA'];
	            }
	        }
	    }
	    return $x;
	}
	
	// $dateStart - max up to academic year end date
	function getBusTakeScheduleForParent($studentID, $dateStart, $numberOfDays)
	{
	    global $Lang, $indexVar, $schoolBusConfig, $intranet_root;
        include_once ($intranet_root . "/includes/libcardstudentattend2.php");

	    $ret = array();
	    $currentAcademicYearID = Get_Current_Academic_Year_ID();
	    $academicEndDate = getEndDateOfAcademicYear($currentAcademicYearID);
	    
        $dateStartTs = strtotime($dateStart);   // timestamp
        $dateEnd = date("Y-m-d",mktime(0, 0, 0, date('n',$dateStartTs), date('j',$dateStartTs) + $numberOfDays, date('Y',$dateStartTs)));
        
        if ($dateStart > $academicEndDate) {
            return $ret;
        }
        else if ($dateEnd > $academicEndDate) {
            $dateEnd = $academicEndDate;
            $numberOfDays = intval((strtotime($dateEnd) - strtotime($dateStart))/86400) + 1;
        }

        $lattend = new libcardstudentattend2();
	    $db = $indexVar['db'];

	    // get YearClassID of the student
	    $yearClassID = $db->getYearClassIDByUserID($studentID);

	    //Get User Route info, current academic year only
	    $userRouteInfo = $db->getStudentBusSchedule($studentID, $dateStart, $dateEnd, $isCurrentAcademicYear=true, $includeNullRoute=true);
//debug_pr($userRouteInfo);	    
	    // Get Holiday
	    $holidays = $db->getHoliday($dateStart, $dateEnd);

	    // get Applied Leaves
        $leaveDates = $db->getApplyLeaveRecordByStudent($studentID, $dateStart, $dateEnd);
//debug_pr($leaveDates);        
	    for($i=0;$i<$numberOfDays;$i++) {
	        $_rs = array();    // record set
	        $_rs['isHoliday'] = false;
	        $_rs['mergeCol'] = false;
	        $_rs['mergeContent'] = '';     // empty content for Saturday & Sunday
	        
	        $_timeStamp = mktime(0, 0, 0, date('n',$dateStartTs), date('j',$dateStartTs) + $i, date('Y',$dateStartTs));
	        $_date = date('Y-m-d',$_timeStamp);
	        $_month = date('n', $_timeStamp);
	        //$_dayMonth = date('j', $_timeStamp) . ' ' . $Lang['eSchoolBus']['App']['Month'][$_month];
	        $_dayMonth = date('j', $_timeStamp) . '/' . $_month;
	        $_weekDay = date('N', $_timeStamp);
	        $_displayWweekDay = $Lang['General']['DayType4'][$_weekDay];
	        $_rs['dayMonth'] = $_dayMonth;
	        $_rs['weekDayStr'] = '('.$_displayWweekDay.')';
	        $_weekDayEng = $this->getWeekdayKey($_weekDay);

            $notNeedToTakeAttendanceYearClassIDAry = array();
            $notNeedToTakeAttendanceClassAry = $lattend->getClassListNotTakeAttendanceByDate($_date);
            foreach((array)$notNeedToTakeAttendanceClassAry as $_notNeedToTakeAttendanceClassAry ) {
                $notNeedToTakeAttendanceYearClassIDAry[] = $_notNeedToTakeAttendanceClassAry[0];
            }

            $needToTakeAttendanceYearClassIDAry = array();
            $classListToTakeAttendance = $lattend->getClassListToTakeAttendanceByDate($_date);
            foreach ((array)$classListToTakeAttendance as $_classListToTakeAttendance) {
                $needToTakeAttendanceYearClassIDAry[] = $_classListToTakeAttendance[0];
            }

            // Priority
            // 1. NotNeedToTakeAttendance, 2. Holiday, 3. Applied Leave, 4. Special arrangement of that date, 5. Routine Schedule (normal route and ASA route)
            if (in_array($yearClassID, $notNeedToTakeAttendanceYearClassIDAry)) {
                $_rs['isHoliday'] = true;
            }
            else {

                if (($_weekDay == '6') || ($_weekDay == '7')) {     // Saturday & Sunday
                    if (!in_array($yearClassID, (array)$needToTakeAttendanceYearClassIDAry)) {
                        $_rs['isHoliday'] = true;
                    }
                }

                if (!$_rs['isHoliday']) {
                    if (!empty($holidays[$_date])) {                       // public holiday & school holiday
                        $_rs['isHoliday'] = true;
                        $_rs['mergeContent'] = wordwrapByLength($holidays[$_date], 30);

                    } else {         // Applied Leave
                        if ($leaveDates[$_date]['AM'] && $leaveDates[$_date]['PM']) {        // applied full day
                            if ($leaveDates[$_date]['AM']['RecordStatus'] == $leaveDates[$_date]['PM']['RecordStatus']) {
                                if (($leaveDates[$_date]['AM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Cancelled']) ||
                                    ($leaveDates[$_date]['AM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Deleted'])) {
                                    $_rs['gotoSchool'] = $this->getGotoSchoolDisplay($userRouteInfo, $_date);
                                    $_rs['leaveSchool'] = $this->getLeaveSchoolDisplay($userRouteInfo, $_date, $_weekDayEng);
                                } elseif ($leaveDates[$_date]['AM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Rejected']) {
                                    $_rs['gotoSchool'] = $this->getGotoSchoolDisplay($userRouteInfo, $_date, $leaveDates[$_date]['AM']['RecordStatus']);
                                    $_rs['leaveSchool'] = $this->getLeaveSchoolDisplay($userRouteInfo, $_date, $_weekDayEng, $leaveDates[$_date]['AM']['RecordStatus']);
                                } else {
                                    $_rs['mergeCol'] = true;
                                    $_rs['mergeContent'] = $this->getLeaveApplicationStatusDisplay($leaveDates[$_date]['AM']['RecordStatus']);
                                }
                            } else {
                                if (($leaveDates[$_date]['AM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Cancelled']) ||
                                    ($leaveDates[$_date]['AM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Deleted'])) {
                                    $_rs['gotoSchool'] = $this->getGotoSchoolDisplay($userRouteInfo, $_date);
                                    if (($leaveDates[$_date]['PM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Rejected']) ||
                                        ($leaveDates[$_date]['PM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Cancelled'])) {
                                        $_rs['leaveSchool'] = $this->getLeaveSchoolDisplay($userRouteInfo, $_date, $_weekDayEng, $leaveDates[$_date]['PM']['RecordStatus']);
                                    } else {
                                        $_rs['leaveSchool'] = $this->getLeaveApplicationStatusDisplay($leaveDates[$_date]['PM']['RecordStatus']);
                                    }
                                } elseif ($leaveDates[$_date]['AM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Rejected']) {
                                    $_rs['gotoSchool'] = $this->getGotoSchoolDisplay($userRouteInfo, $_date, $leaveDates[$_date]['AM']['RecordStatus']);
                                    if (($leaveDates[$_date]['PM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Cancelled']) ||
                                        ($leaveDates[$_date]['PM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Deleted'])) {
                                        $_rs['leaveSchool'] = $this->getLeaveSchoolDisplay($userRouteInfo, $_date, $_weekDayEng);
                                    } else {
                                        $_rs['leaveSchool'] = $this->getLeaveApplicationStatusDisplay($leaveDates[$_date]['PM']['RecordStatus']);
                                    }
                                } else {
                                    $_rs['gotoSchool'] = $this->getLeaveApplicationStatusDisplay($leaveDates[$_date]['AM']['RecordStatus']);

                                    // waiting approval, confirmed, absent
                                    if (($leaveDates[$_date]['PM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Waiting']) ||
                                        ($leaveDates[$_date]['PM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Confirmed']) ||
                                        ($leaveDates[$_date]['PM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Absent'])) {
                                        $_rs['leaveSchool'] = $this->getLeaveApplicationStatusDisplay($leaveDates[$_date]['PM']['RecordStatus']);
                                    } elseif ($leaveDates[$_date]['PM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Rejected']) {
                                        $_rs['leaveSchool'] = $this->getLeaveSchoolDisplay($userRouteInfo, $_date, $_weekDayEng, $leaveDates[$_date]['PM']['RecordStatus']);
                                    } else {   // cancelled or deleted, show original schedule
                                        $_rs['leaveSchool'] = $this->getLeaveSchoolDisplay($userRouteInfo, $_date, $_weekDayEng);
                                    }
                                }
                            }
                        } elseif ($leaveDates[$_date]['AM']) {                // applied goto school only

                            // waiting approval, confirmed, absent
                            if (($leaveDates[$_date]['AM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Waiting']) ||
                                ($leaveDates[$_date]['AM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Confirmed']) ||
                                ($leaveDates[$_date]['AM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Absent'])) {
                                $_rs['gotoSchool'] = $this->getLeaveApplicationStatusDisplay($leaveDates[$_date]['AM']['RecordStatus']);
                            } elseif ($leaveDates[$_date]['AM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Rejected']) {
                                $_rs['gotoSchool'] = $this->getGotoSchoolDisplay($userRouteInfo, $_date, $leaveDates[$_date]['AM']['RecordStatus']);
                            } else {   // cancelled or deleted, show original schedule
                                $_rs['gotoSchool'] = $this->getGotoSchoolDisplay($userRouteInfo, $_date);
                            }

                            // leave school
                            $_rs['leaveSchool'] = $this->getLeaveSchoolDisplay($userRouteInfo, $_date, $_weekDayEng);

                        } elseif ($leaveDates[$_date]['PM']) {                // applied leave school only

                            $_rs['gotoSchool'] = $this->getGotoSchoolDisplay($userRouteInfo, $_date);

                            // waiting approval, confirmed, absent
                            if (($leaveDates[$_date]['PM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Waiting']) ||
                                ($leaveDates[$_date]['PM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Confirmed']) ||
                                ($leaveDates[$_date]['PM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Absent'])) {
                                $_rs['leaveSchool'] = $this->getLeaveApplicationStatusDisplay($leaveDates[$_date]['PM']['RecordStatus']);
                            } elseif ($leaveDates[$_date]['PM']['RecordStatus'] == $schoolBusConfig['ApplyLeaveStatus']['Rejected']) {
                                $_rs['leaveSchool'] = $this->getLeaveSchoolDisplay($userRouteInfo, $_date, $_weekDayEng, $leaveDates[$_date]['PM']['RecordStatus']);
                            } else {   // cancelled or deleted, show original schedule
                                $_rs['leaveSchool'] = $this->getLeaveSchoolDisplay($userRouteInfo, $_date, $_weekDayEng);
                            }

                        } else {                                           // didn't apply leave
                            $_rs['gotoSchool'] = $this->getGotoSchoolDisplay($userRouteInfo, $_date);

                            $_rs['leaveSchool'] = $this->getLeaveSchoolDisplay($userRouteInfo, $_date, $_weekDayEng);
                        }
                    }
                }
            }

            if ($_rs['isHoliday']) {
                $_rs['mergeCol'] = true;
            }
	        
	        $ret[] = $_rs;
	        unset($_rs);
	    }  // end for
	    
	    return $ret;
	}
	
	function getBusTakeScheduleBodyForParent($studentID, $offset, $numberOfRecord)
	{
	    $today = date('Y-m-d');
	    $dateStart = $offset ? date('Y-m-d',strtotime("+".$offset." day")) : $today;
	    $x = '';
	    
	    // default get two weeks data
        $scheduleAry = $this->getBusTakeScheduleForParent($studentID, $dateStart, $numberOfRecord);
	    
	    foreach((array)$scheduleAry as $_schedule) {
	        $rowClass = $_schedule['isHoliday'] ? 'class="dim"' : '';
	        if ($_schedule['mergeCol']) {
	            $_scheduleowContent = '<td colspan="2">'.$_schedule['mergeContent'].'</td>'.NEW_LINE;
	        }
	        else {
	            $_scheduleowContent = '<td>'.$_schedule['gotoSchool'].'</td>'.NEW_LINE;
	            $_scheduleowContent .= '<td>'.$_schedule['leaveSchool'].'</td>'.NEW_LINE;
	        }
	        
	        $x .= '<tr '.$rowClass.'>'.NEW_LINE;
	        $x .= '<td>'.$_schedule['dayMonth'].'<br>'.$_schedule['weekDayStr'].'</td>'.NEW_LINE;
	        $x .= $_scheduleowContent;
	        $x .= '</tr>'.NEW_LINE;
	    }
	    return $x;
	}
	
	function getBusTakeScheduleLayoutForParent($studentID, $offset, $numberOfRecord)
	{
	    global $Lang;
	    
	    $x = '<div class="busTakeSchedule col-xs-12">'.NEW_LINE;
	       $x .= '<div class="table-responsive">'.NEW_LINE;
	           $x .= '<table class="table schedule" id="scheduleTable">'.NEW_LINE;
	               $x .= '<thead>'.NEW_LINE;
	                   $x .= '<tr id="scheduleHeader">'.NEW_LINE;
    	                   $x .= '<th>'.$Lang['eSchoolBus']['App']['Date'].'</th>'.NEW_LINE;
    	                   $x .= '<th>'.$Lang['eSchoolBus']['Settings']['ApplyLeave']['GotoSchool'].'</th>'.NEW_LINE;
    	                   $x .= '<th>'.$Lang['eSchoolBus']['Settings']['ApplyLeave']['LeaveSchool'].'</th>'.NEW_LINE;
	                   $x .= '</tr>'.NEW_LINE;
	               $x .= '</thead>'.NEW_LINE;
	               $x .= '<tbody>'.NEW_LINE;
	    
	               $x .= $this->getBusTakeScheduleBodyForParent($studentID, $offset, $numberOfRecord);
	    
	               $x .= '</tbody>'.NEW_LINE;
	           $x .= '</table>'.NEW_LINE;
	       $x .= '</div>'.NEW_LINE;
	    $x .= '</div>'.NEW_LINE;
	    
	    return $x;
	}
	
	// default unlimit number of records
	function getApplyLeaveResult($studentID, $offset=0, $numberOfRecords=0, $isShowNoRecord=false) 
	{
        global $Lang, $indexVar, $schoolBusConfig;
        
        $db = $indexVar['db'];
        
        // exclude deleted record for parent
        $recordStatusAry = array(
            $schoolBusConfig['ApplyLeaveStatus']['Waiting'],
            $schoolBusConfig['ApplyLeaveStatus']['Confirmed'],
            $schoolBusConfig['ApplyLeaveStatus']['Rejected'],
            $schoolBusConfig['ApplyLeaveStatus']['Cancelled'],
            $schoolBusConfig['ApplyLeaveStatus']['Absent']
        );
        
        // get leave applications
        $applications = $db->getApplyLeaveResultByStudent($studentID, $recordStatusAry, $offset, $numberOfRecords);
        
        if (count($applications)) {
            
            $x = '<div class="applicationResult">'.NEW_LINE;
            $x .= '<div class="table-responsive">'.NEW_LINE;
            foreach((array)$applications as $idx => $_application) {
                $_startDateTimeStamp = strtotime($_application['StartDate']);   // timestamp
                $_startWeekDay = date('N', $_startDateTimeStamp);
                $_inputDateDisplay = date('Y-m-d', $_startDateTimeStamp) . '('.$Lang['General']['DayType4'][$_startWeekDay].')';
                if ($_application['StartDate'] != $_application['EndDate']) {
                    $_endDateTimeStamp = strtotime($_application['EndDate']);
                    $_endWeekDay = date('N', $_endDateTimeStamp);
                    $_inputDateDisplay .= ' - ' . date('Y-m-d', $_endDateTimeStamp) . '('.$Lang['General']['DayType4'][$_endWeekDay].')';
                }
                $_recordStatus = $this->getLeaveApplicationStatusHistoryDisplay($_application['RecordStatus']);
                $_url = '?task=parentApp.applyLeaveEdit&ApplicationID='.$_application['ApplicationID'];
                
                $x .= '<a href="'.$_url.'">'.NEW_LINE;
                    $x .= '<li class="event">'.NEW_LINE;
                        $x .= '<div class="eventTitle">'.$_inputDateDisplay.'</div>'.NEW_LINE;
                        $x .= '<div class="eventDate">'.$_application['Reason'].'</div>'.NEW_LINE;
                        $x .= '<div class="">'.$_recordStatus.'</div>'.NEW_LINE;
                    $x .= '</li>';
                $x .= '</a>';
            }
            $x .= '</div>'.NEW_LINE;
            $x .= '</div>'.NEW_LINE;
        }
        else {
            
            if ($isShowNoRecord) {
                $x = '<div class="applicationResult">'.NEW_LINE;
                $x .= '<div class="table-responsive">'.NEW_LINE;
                $x .= '<br><br><div class="text-center">'.$Lang['General']['NoRecordAtThisMoment'].'</div>';
                $x .= '</div>'.NEW_LINE;
                $x .= '</div>'.NEW_LINE;
            }
            else {
                $x = '';   
            }
        }
        
        return $x;
	}
	
	function getPendingApplicationForTeacher($offset, $numberOfRecord, $className='', $studentID='', $inputSearch='', $isShowNoRecord=false, $startDate='', $endDate='')
	{
	    global $Lang, $indexVar, $schoolBusConfig;
	    
	    $db = $indexVar['db'];
	    $isSchoolBusAdmin = $indexVar['libSchoolBus']->isSchoolBusAdmin();
	    if ($isSchoolBusAdmin) {
	        $pendingApplicationAry = $db->getApplyLeaveRecordForTeacher('', $schoolBusConfig['ApplyLeaveStatus']['Waiting'], $offset, $numberOfRecord, $className, $studentID, $inputSearch, $orderBy='DateInput', $startDate, $endDate);
	    }
	    else {
	        $pendingApplicationAry = $db->getApplyLeaveRecordForTeacher($_SESSION['UserID'], $schoolBusConfig['ApplyLeaveStatus']['Waiting'], $offset, $numberOfRecord, $className, $studentID, $inputSearch, $orderBy='DateInput', $startDate, $endDate);
	    }
	    
//	    $x = '<div class="pendingApplication col-xs-12">'.NEW_LINE;
//    	    $x .= '<div class="table-responsive">'.NEW_LINE;
	    $x = '';
	    if (count($pendingApplicationAry) && !empty($pendingApplicationAry)) {
            foreach((array)$pendingApplicationAry as $idx => $_application) {
                $_startDateTimeStamp = strtotime($_application['StartDate']);   // timestamp
                $_startWeekDay = date('N', $_startDateTimeStamp);
                $_inputDateDisplay = date('Y-m-d', $_startDateTimeStamp) . '('.$Lang['General']['DayType4'][$_startWeekDay].')';
                if ($_application['StartDate'] != $_application['EndDate']) {
                    $_endDateTimeStamp = strtotime($_application['EndDate']);
                    $_endWeekDay = date('N', $_endDateTimeStamp);
                    $_inputDateDisplay .= ' - ' . date('Y-m-d', $_endDateTimeStamp) . '('.$Lang['General']['DayType4'][$_endWeekDay].')';
                }
                $_recordStatus = $this->getLeaveApplicationStatusHistoryDisplay($_application['RecordStatus']);
                $_url = '?task=teacherApp.applyLeaveEdit&ApplicationID='.$_application['ApplicationID'];
                $_displayStudent = $_application['StudentName'] . ' ('.$_application['ClassName'].'-'.$_application['ClassNumber'].')';
                $x .= '<a href="'.$_url.'">'.NEW_LINE;
                $x .= '<li class="event">'.NEW_LINE;
                $x .= '<div class="eventTitle">'.$_displayStudent.'</div>'.NEW_LINE;
                $x .= '<div class="">'.$_inputDateDisplay.'</div>'.NEW_LINE;
                $x .= '<div class="">'.$_application['Reason'].'</div>'.NEW_LINE;
                $x .= '<div class="">'.$_recordStatus.'</div>'.NEW_LINE;
                $x .= '</li>';
                $x .= '</a>';
            }
	    }
	    else {
	        if ($isShowNoRecord) {
	           $x .= '<br><br><div class="text-center">'.$Lang['General']['NoRecordAtThisMoment'].'</div>';
	        }
	    }
//            $x .= '</div>'.NEW_LINE;
//        $x .= '</div>'.NEW_LINE;

	    return $x;
	}
	
	function getHandledApplicationForTeacher($offset, $numberOfRecord, $className='', $studentID='', $inputSearch='', $isShowNoRecord=false, $applyLeaveStatus='', $startDate='', $endDate='')
	{
	    global $Lang, $indexVar, $schoolBusConfig;
	    
	    $db = $indexVar['db'];
	    
	    if ($applyLeaveStatus) {
	        $recordStatusAry = array($applyLeaveStatus);
	    }
	    else {
    	    $recordStatusAry = array(
        	    $schoolBusConfig['ApplyLeaveStatus']['Confirmed'],
        	    $schoolBusConfig['ApplyLeaveStatus']['Rejected'],
        	    $schoolBusConfig['ApplyLeaveStatus']['Cancelled'],
    	        $schoolBusConfig['ApplyLeaveStatus']['Absent']
    	    );
	    }
    	    
	    $isSchoolBusAdmin = $indexVar['libSchoolBus']->isSchoolBusAdmin();
	    if ($isSchoolBusAdmin) {
	        $handledApplicationAry = $db->getApplyLeaveRecordForTeacher('', $recordStatusAry, $offset, $numberOfRecord, $className, $studentID, $inputSearch, $orderBy='StartDate', $startDate, $endDate);
	    }
	    else {
	        $handledApplicationAry = $db->getApplyLeaveRecordForTeacher($_SESSION['UserID'], $recordStatusAry, $offset, $numberOfRecord, $className, $studentID, $inputSearch, $orderBy='StartDate', $startDate, $endDate);
	    }
	    
//	    $x = '<div id="handledApplication" class="handledApplication col-xs-12">'.NEW_LINE;
//	    $x .= '<div class="table-responsive">'.NEW_LINE;
	    $x = '';
	    if (count($handledApplicationAry) && !empty($handledApplicationAry)) {
    	    foreach((array)$handledApplicationAry as $idx => $_application) {
    	        $_startDateTimeStamp = strtotime($_application['StartDate']);   // timestamp
    	        $_startWeekDay = date('N', $_startDateTimeStamp);
    	        $_inputDateDisplay = date('Y-m-d', $_startDateTimeStamp) . '('.$Lang['General']['DayType4'][$_startWeekDay].')';
    	        if ($_application['StartDate'] != $_application['EndDate']) {
    	            $_endDateTimeStamp = strtotime($_application['EndDate']);
    	            $_endWeekDay = date('N', $_endDateTimeStamp);
    	            $_inputDateDisplay .= ' - ' . date('Y-m-d', $_endDateTimeStamp) . '('.$Lang['General']['DayType4'][$_endWeekDay].')';
    	        }
                $_applicationID = $_application['ApplicationID'];
    	        $_isStatusConsistent = $db->isRecordStatusConsistent($_applicationID);
                if ($_isStatusConsistent) {
                    $_recordStatus = $this->getLeaveApplicationStatusHistoryDisplay($_application['RecordStatus']);
                }
                else {
                    $_recordStatus = '';
                }
    	        $_url = '?task=teacherApp.applyLeaveView&ApplicationID='.$_applicationID;
    	        $_displayStudent = $_application['StudentName'] . ' ('.$_application['ClassName'].'-'.$_application['ClassNumber'].')';
    	        $x .= '<a href="'.$_url.'">'.NEW_LINE;
    	        $x .= '<li class="event">'.NEW_LINE;
    	        $x .= '<div class="eventTitle">'.$_displayStudent.'</div>'.NEW_LINE;
    	        $x .= '<div class="">'.$_inputDateDisplay.'</div>'.NEW_LINE;
    	        $x .= '<div class=""><span class="fieldTitle">'.$Lang['eSchoolBus']['App']['ApplyLeave']['Reason'].': </span>'.$_application['Reason'].'</div>'.NEW_LINE;
    	        $x .= '<div class=""><span class="fieldTitle">'.$Lang['eSchoolBus']['TeacherApp']['Remark'].': </span>'.$_application['Remark'].'</div>'.NEW_LINE;
    	        $x .= '<div class=""><span class="fieldTitle">'.$Lang['General']['LastModifiedBy'].': </span>'.$_application['LastModifiedBy'].'</div>'.NEW_LINE;
    	        $x .= '<div class=""><span class="fieldTitle">'.$Lang['General']['LastModified'].': </span>'.$_application['DateModified'].'</div>'.NEW_LINE;
    	        $x .= '<div class="">'.$_recordStatus.'</div>'.NEW_LINE;
    	        $x .= '</li>';
    	        $x .= '</a>';
    	    }
	    }
	    else {
	        if ($isShowNoRecord) {
	            $x .= '<br><br><div class="text-center">'.$Lang['General']['NoRecordAtThisMoment'].'</div>';
	        }
	    }
//	    $x .= '</div>'.NEW_LINE;
//	    $x .= '</div>'.NEW_LINE;
	    return $x;
	}
	
	// $date is used to find which academic year it belongs to
	function getClassBuilding($date, $buildingID='')
	{
	    global $indexVar;
	    $db = $indexVar['db'];
	    $buildingAry = $db->getClassBuilding($date);
	    $tags = 'name="BuildingID" id="BuildingID"';    
	    
	    $selection = getSelectByArray($buildingAry, $tags, $buildingID, $all=1);
	    return $selection;
	}

	function getStudentListByBus($date, $timeSlot='PM', $buildingID='', $takeOrNot='2', $printTitle=false, $classGroupIDAry='')
	{
	    global $indexVar, $Lang, $intranet_root, $LAYOUT_SKIN;
	    $db = $indexVar['db'];
	    
	    $map = $db->getStudentListByBus($date, $timeSlot, $buildingID, $takeOrNot, $classGroupIDAry);
	    $presentList = $map['present'];
	    $presentCountAry = $map['presentCount'];
	    $absentList = $map['absent'];
	    $absentCountAry = $map['absentCount'];
	    $vehicleAry = $map['vehicleAssoc'];
	    $nrColumn = count($presentCountAry);
	    if ($nrColumn == 0) {
	        $nrColumn = count($absentCountAry);
	    }
	    $formatDate = date('d/m',strtotime($date));

	    $classGroupAry = $db->getClassGroup();
	    $dispClassGroup = $Lang['eSchoolBus']['Settings']['ApplyLeave']['ClassGroup'] . " : ";
	    if ($classGroupIDAry != '') {
	        if (count($classGroupIDAry) == count($classGroupAry)) {
	            $dispClassGroup .= $Lang['eSchoolBus']['Report']['ByClass']['All'];
	        }
	        else {
	            $dispClassGroupAry = array();
	            foreach((array)$classGroupIDAry as $_classGroupID) {
	                $dispClassGroupAry[] = $classGroupAry[$_classGroupID];
	            }
	            $dispClassGroup .= implode(", ", $dispClassGroupAry);
	            unset($dispClassGroupAry);
	        }
	    }
	    else {
	        $dispClassGroup .= $Lang['eSchoolBus']['Report']['ByClass']['All'];
	    }
	    
        if ($printTitle) {
            $x = '<link href="/templates/'.$LAYOUT_SKIN.'/css/eSchoolBusReportPrint.css" rel="stylesheet" type="text/css">';
        }
        else {
            $x = '<link href="/templates/'.$LAYOUT_SKIN.'/css/eSchoolBusReport.css" rel="stylesheet" type="text/css">';
        }

	    // all or not taking bus
	    if ($takeOrNot == '0' || $takeOrNot == '2' ) {
	        if ($printTitle) {
	            $x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\" align=\"center\">";
	               $x .= "<tr>";
	                   $x .= "<td align=\"center\" class=\"schoolbus_report_title\">";
	                       $x .= $Lang['eSchoolBus']['ReportArr']['StudentListByBusArr']['NotTakeBus'];
	                   $x .= "</td>";
	               $x .= "</tr>";
                $x .= "</table>";
                
                $busNumClass = 'bus_number';
	        }
	        else {
                $busNumClass = 'bus_number bus_number_absent_bg';
	        }
	        
	        $x .= "<table class=\"common_table_list_v30 view_table_list_v30\">";
	        $x .= "<thead>";
    	        $x .= "<tr>";
    	            $x .= "<td colspan=".(count($vehicleAry) + 1)." class=\"class_group\">";
    	                $x .= $dispClassGroup;
    	            $x .= "</td>";
    	        $x .= "</tr>";
    	        
    	        $x .= "<tr>";
        	        $x .= "<td class=\"location\">";
        	           $x .= $Lang['eSchoolBus']['Report']['Campus'];
    	            $x .= "</td>";
        
    	        if ($buildingID) {    
        	        foreach((array)$vehicleAry as $_vehicleID=>$_vehicleAry) {
        	            $x .= "<td class=\"location\">";
        	               $x .= $_vehicleAry['BuildingName'];
        	            $x .= "</td>";
        	        }
    	        }
    	        else {
    	            $x .= "<td colspan=".(count($vehicleAry))."  class=\"location\">";
    	            $x .= $Lang['eSchoolBus']['Report']['ByClass']['All'];
    	            $x .= "</td>";
    	        }
    	        $x .= "</tr>";
        
    	        $x .= "<tr>";
        	        $x .= "<td>&nbsp;</td>";
        
    	        for ($j=0; $j<$nrColumn; $j++) {
    	            $x .= "<td class=\"taking_date\">";
//    	               $x .= "<div>".$Lang['General']['Date'].":</div><div>".$formatDate."</div>";
                        $x .= $Lang['General']['Date'].":".$formatDate;
    	            $x .= "</td>";
    	        }
    	        $x .= "</tr>";
        
    	        $x .= "<tr>";
        	        $x .= "<td class=\"bus\">";
        	            $x .= $Lang['eSchoolBus']['Report']['Bus'];
        	        $x .= "</td>";
        
    	        foreach((array)$vehicleAry as $_vehicleID=>$_vehicleAry) {
    	            $x .= "<th class=\"".$busNumClass."\">";
    	               $x .= $_vehicleAry['CarNum'];
    	            $x .= "</th>";
    	        }
        
                $x .= "</tr>";

                $x .= "<tr>";
                    $x .= "<td class=\"total\">";
                    $x .= $Lang['eSchoolBus']['Report']['Attendance']['TotalAbsent'];
                    $x .= "</td>";
                    foreach((array)$absentCountAry as $_vehicleID=>$_absent) {
                        $x .= "<td class=\"total\">";
                        $x .= $_absent;
                        $x .= "</td>";
                    }
                $x .= "</tr>";

            $x .= "</thead>";
        
            $x .= "<tbody>";
        
	        for ($i=0, $iMax=count($absentList); $i<$iMax; $i++) {
	            $x .= "<tr class=\"report_row\">";
    	            $x .= "<td>&nbsp;</td>";
    	            for ($j=0; $j<$nrColumn; $j++) {
    	                $x .= "<td class=\"student_name\">";
    	                    $x .= $absentList[$i][$j];
    	                $x .= "</td>";
    	            }
	            $x .= "</tr>";
	        }
        
            $x .= "</tbody>";
	        $x .= "</table>";
	    }

	    if ($takeOrNot == '0') {
	       $x .= "<br><br>";    
	    }
	    
	    // all or taking bus
	    if ($takeOrNot == '0' || $takeOrNot == '1' ) {
	        if ($printTitle) {
	            $x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\" align=\"center\">";
    	            $x .= "<tr>";
        	            $x .= "<td align=\"center\" class=\"schoolbus_report_title\">";
        	                $x .= $Lang['eSchoolBus']['ReportArr']['StudentListByBusArr']['MenuTitle'];
        	            $x .= "</td>";
    	            $x .= "</tr>";
	            $x .= "</table>";
                $busNumClass = 'bus_number';
            }
            else {
                $busNumClass = 'bus_number bus_number_present_bg';
            }
	        
    	    $x .= "<table class=\"common_table_list_v30 view_table_list_v30\">";
    	    $x .= "<thead>";
    	    
        	   $x .= "<tr>";
        	       $x .= "<td colspan=".(count($vehicleAry) + 1)." class=\"class_group\">";
        	           $x .= $dispClassGroup;
        	       $x .= "</td>";
        	   $x .= "</tr>";
    	    
    	       $x .= "<tr>";
        	       $x .= "<td class=\"location\">";
        	           $x .= $Lang['eSchoolBus']['Report']['Campus'];
        	       $x .= "</td>";
        	       
        	       if ($buildingID) {
            	       foreach((array)$vehicleAry as $_vehicleID=>$_vehicleAry) {
                	        $x .= "<td class=\"location\">";
                	           $x .= $_vehicleAry['BuildingName'];
                	        $x .= "</td>";
                	   }
        	       }
        	       else {
        	           $x .= "<td colspan=".(count($vehicleAry))." class=\"location\">";
        	               $x .= $Lang['eSchoolBus']['Report']['ByClass']['All'];
        	           $x .= "</td>";
        	       }
        	    $x .= "</tr>";
    
        	    $x .= "<tr>";
            	    $x .= "<td>&nbsp;</td>";
        	    
            	    for ($j=0; $j<$nrColumn; $j++) {
            	        $x .= "<td class=\"taking_date\">";
//            	            $x .= "<div>".$Lang['General']['Date'].":</div><div>".$formatDate."</div>";
                        $x .= $Lang['General']['Date'].":".$formatDate;
            	        $x .= "</td>";
            	    }
        	    $x .= "</tr>";

    	        $x .= "<tr>";
        	        $x .= "<td class=\"bus\">";
        	        $x .= $Lang['eSchoolBus']['Report']['Bus'];
        	        $x .= "</td>";
    	        
            	    foreach((array)$vehicleAry as $_vehicleID=>$_vehicleAry) {
            	        $x .= "<th class=\"".$busNumClass."\">";
            	           $x .= $_vehicleAry['CarNum'];
            	        $x .= "</th>";
            	    }
            	    
    	       $x .= "</tr>";

                $x .= "<tr>";
                    $x .= "<td class=\"total\">";
                    $x .= $Lang['eSchoolBus']['Report']['Attendance']['TotalOnBus'];
                    $x .= "</td>";
                    foreach((array)$presentCountAry as $_vehicleID=>$_present) {
                        $x .= "<td class=\"total\">";
                        $x .= $_present;
                        $x .= "</td>";
                    }
                $x .= "</tr>";

    	    $x .= "</thead>";
    	    
    	    $x .= "<tbody>";
    
    	    for ($i=0, $iMax=count($presentList); $i<$iMax; $i++) {
    	        $x .= "<tr class=\"report_row\">";
    	           $x .= "<td>&nbsp;</td>";
    	           for ($j=0; $j<$nrColumn; $j++) {
    	               $x .= "<td class=\"student_name\">";
    	                   $x .= $presentList[$i][$j];
    	               $x .= "</td>";
    	           }
    	        $x .= "</tr>";
    	    }
    	    
    	    $x .= "</tbody>";
    	    $x .= "</table>";
	    }
	    return $x;
	    
	}
	
	
	function getStudentListByClass($date, $timeSlot='WD', $buildingID='', $takeOrNot='0', $hasRouteOrNot='A', $yearClassIDAry='', $classGroupIDAry='')
	{
	    global $indexVar, $Lang;
	    $db = $indexVar['db'];
	    
	    $studentAry = $db->getStudentListByClass($date, $timeSlot, $buildingID, $takeOrNot, $hasRouteOrNot, $yearClassIDAry, $classGroupIDAry);

	    $x = "<table class=\"common_table_list_v30 view_table_list_v30\">";
	    $x .= "<thead>";
    	    $x .= "<tr>";
        	    $x .= "<th>".$Lang['eSchoolBus']['Report']['ByClass']['Title']['Name']."</th>";
        	    $x .= "<th>".$Lang['eSchoolBus']['Report']['ByClass']['Title']['StudentNumber']."</th>";
        	    $x .= "<th>".$Lang['eSchoolBus']['Report']['ByClass']['Title']['Route']."</th>";
        	    $x .= "<th>".$Lang['eSchoolBus']['Report']['ByClass']['Title']['Class']."</th>";
        	    $x .= "<th>".$Lang['eSchoolBus']['Report']['ByClass']['Title']['Campus']."</th>";
//        	    $x .= "<th>".$Lang['eSchoolBus']['Report']['ByClass']['Title']['TimeSlot']."</th>";
        	    $x .= "<th>".$Lang['eSchoolBus']['Report']['ByClass']['Title']['TakeOrNot']."</th>";
        	    $x .= "<th>".$Lang['eSchoolBus']['Report']['ByClass']['Title']['Remark']."</th>";
    	    $x .= "</tr>";
	    $x .= "</thead>";
	    
	    $x .= "<tbody>";
	    
	    $numberOfRecord=count($studentAry);
	    if ($numberOfRecord == 0) {
	       $x .= '<tr><td colspan="8" style="text-align: center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
	    }
	    else {
    	    for ($i=0; $i<$numberOfRecord; $i++) {
    	        $_studentAry = $studentAry[$i];
    	        
    	        $x .= "<tr>";
    	            $x .= "<td>".$_studentAry['StudentName']."</td>";
    	            $x .= "<td>".$_studentAry['ClassNumber']."</td>";
    	            $x .= "<td>".$_studentAry['RouteName']."</td>";
    	            $x .= "<td>".$_studentAry['ClassName']."</td>";
    	            $x .= "<td>".$_studentAry['BuildingName']."</td>";
//    	            $x .= "<td>".$_studentAry['TimeSlot']."</td>";
    	            $x .= "<td>".$_studentAry['TakeOrNot']."</td>";
    	            $x .= "<td>".$_studentAry['Remark']."</td>";
    	        $x .= "</tr>";
    	    }
	    }
	    $x .= "</tbody>";
	    $x .= "</table>";
	    
	    return $x;
	}
	
	public function getTeacherTypeSelection($selectedType = '', $id = 'TeacherType')
	{
	    global $Lang;
	    
	    $x = '<select name=' . $id . ' id=' . $id . '>';
	    $x .= '<option value="1" ' . (($selectedType == 1) ? 'SELECTED' : '') . '>' . $Lang['SysMgr']['RoleManagement']['TeachingStaff']. '</option>';
	    $x .= '<option value="0" ' . (($selectedType === 0) ? 'SELECTED' : '') . '>' . $Lang['SysMgr']['RoleManagement']['SupportStaff']. '</option>';
	    $x .= '</select>';
	    
	    return $x;
	}
	
	public function getApplyLeaveStatusSelection($tags, $selected="", $all=0, $noFirst=0, $FirstTitle="")
	{
	    global $indexVar, $Lang, $schoolBusConfig;
	    foreach((array)$schoolBusConfig['ApplyLeaveStatus'] as $statusName=>$statusValue) {
	        if ($statusValue != $schoolBusConfig['ApplyLeaveStatus']['Waiting'] && $statusValue != $schoolBusConfig['ApplyLeaveStatus']['Deleted']) {
	            $statusAry[] = array($statusValue, $Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus'][$statusName]);
	        }
	    }
	    $selection = getSelectByArray($statusAry, $tags, $selected, $all, $noFirst, $FirstTitle, $ParQuoteValue=1);
	    return $selection;
	}
	
	public function getRouteTeacherList($routeID)
	{
	    global $indexVar;
	    $db = $indexVar['db'];
	    
	    $routeTeacherAry = $db->getSelectedTeacher($routeID);
	    
	    $x = '';
	    foreach((array)$routeTeacherAry as $_routeTeacherAry) {
	        $_userID = $_routeTeacherAry['UserID'];
	        $_name = $_routeTeacherAry['Name'];
	        $x .= '<div id="TeacherDiv_'.$_userID.'">'.$_name.'<input type="hidden" name="RouteTeacherID[]" value="'.$_userID.'">&nbsp;';
	        $x .= '<a href="javascript:deleteRouteTeacher('.$_userID.');">x</a>';
	        $x .= '</div>';
	    }
	    return $x;
	}
	
	public function getClassGroupSelection()
	{
	    global $indexVar;
	    $db = $indexVar['db'];
	    
	    $classGroupAry = $db->getClassGroup();
	    
	    $x = '<select name="ClassGroupID[]" id="ClassGroupID" size="4" multiple="true">';
	    foreach((array)$classGroupAry as $classGroupID => $groupName) {
	        $x .= '<option value="'.$classGroupID.'">';
	           $x .= $groupName;
	        $x .= '</option>';
	    }
	    $x .= '</select>';
	    
	    return $x;
	}
	
	public function getTakingBusClassSelection($attrib, $selected="", $optionFirst="")
	{
	    global $indexVar, $Lang;
	    $db = $indexVar['db'];
	    
	    $classAry= $db->getTakingBusClassList();
	    $nrClass = count($classAry);
	    
        $x = '<select '.$attrib.'>';
        $x .= '<option value="" '.(($nrClass>0)? "":"selected").'>'.(($optionFirst=="")?'-- '.$Lang['Btn']['Select'].' --':$optionFirst).'</option>';
        for ($i=0; $i< $nrClass; $i++) {
            if ($classAry[$i]['YearID'] != $classAry[$i-1]['YearID']) {
                if ($i == 0) {
                    $x .= '<optgroup label="'.htmlspecialchars($classAry[$i]['YearName'],ENT_QUOTES).'">';
                }
                else {
                    $x .= '</optgroup>';
                    $x .= '<optgroup label="'.htmlspecialchars($classAry[$i]['YearName'],ENT_QUOTES).'">';
                }
            }
            
            unset($Selected);
            $Selected = ($classAry[$i]['ClassTitleEN'] == $selected)? 'selected':'';
            $x .= '<option value="'.$classAry[$i]['ClassTitleEN'].'" '.$Selected.'>'.Get_Lang_Selection($classAry[$i]['ClassTitleB5'],$classAry[$i]['ClassTitleEN']).'</option>';
            
            if ($i == ($nrClass-1)) {
                $x .= '</optgroup>';
            }
        }
        
        $x .= '</select>';
	    return $x;
	}
}

?>