<?php
// Editing by 

/*
 *  2018-06-15 Cameron
 *      - add $schoolBusConfig['ApplyLeaveStatus']
 */
$schoolBusConfig = array();

$schoolBusConfig['ActivityTypes'] = array(
	array(1,$Lang['eSchoolBus']['ActivityArr']['ASA']),
	array(2,$Lang['eSchoolBus']['ActivityArr']['FootballClass']),
	array(3,$Lang['eSchoolBus']['ActivityArr']['SwimmingClass']),
	array(4,$Lang['eSchoolBus']['ActivityArr']['Orchestra']),
	array(5,$Lang['eSchoolBus']['ActivityArr']['MUN']),
	array(6,$Lang['eSchoolBus']['ActivityArr']['Volleyball']) 
);

$schoolBusConfig['AttendanceStatus'] = array(
		array(1,$Lang['eSchoolBus']['AttendanceStatusArr']['OnTime']),	//On Time
		array(2,$Lang['eSchoolBus']['AttendanceStatusArr']['Late']),	//Late
		array(3,$Lang['eSchoolBus']['AttendanceStatusArr']['Absent']),	//Absent
		array(4,$Lang['eSchoolBus']['AttendanceStatusArr']['Activity']),	//Leave for activity
		array(5,$Lang['eSchoolBus']['AttendanceStatusArr']['Parent'])
);

$schoolBusConfig['ApplyLeaveStatus']['Waiting'] = '1';
$schoolBusConfig['ApplyLeaveStatus']['Confirmed'] = '2';
$schoolBusConfig['ApplyLeaveStatus']['Rejected'] = '3';
$schoolBusConfig['ApplyLeaveStatus']['Cancelled'] = '4';
$schoolBusConfig['ApplyLeaveStatus']['Absent'] = '5';
$schoolBusConfig['ApplyLeaveStatus']['Deleted'] = '6';

$schoolBusConfig['ApplyLeaveError']['DuplicateApplication'] = '1';
$schoolBusConfig['ApplyLeaveError']['AfterCutoffTime'] = '2';
$schoolBusConfig['ApplyLeaveError']['InvalidTimeSlot'] = '3';
$schoolBusConfig['ApplyLeaveError']['AfterCutoffDate'] = '4';
?>