<?php
# Editing by 

/***************************************************
 * 	Modification log
 * 20170629 Bill	[2017-0626-1124-47206]
 * 		- modified GET_STUDENT_TERM_SUBJECT_MARK(), to support Vertical > Horizontal Subject Mark Calculation ($ApplySubjectHorizontalCalculation = true)
 * 20170508 Bill	[2017-0508-1128-10164]
 * 		- modified GET_STUDENT_TERM_SUBJECT_MARK(), to display correct Year Report Column Score and support excluding Subject Weight Conversion
 * 20140801 Ryan 
 * 		-  # 20140724 Component Subject Marks calculation 
 * 		     $OtherCondition = "SubjectID = ".$CmpSubjectIDArr[$j]." AND ReportColumnID IS NULL"; 
 * 20101207 Marcus
 * 		- Comment returnReportTemplateSubjectWeightData, same function exist in libreportcard2008.php 
 * 
 * 20091221 Marcus
 * 		- UPDATE_SUBJECT_FORM_GRADING (add LangDisplay & Display SubjectGroup)
 * *************************************************/
 
class libreportcard2008j extends libreportcard {

	function libreportcard2008j(){
		$this->libreportcard();
	}
	
	/*
	function UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $ClassID, $SubjectID, $isComplete, $ParentSubjectID=""){
		$returnVal = true;
		$result = array();
		
		$ReportInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportType = $ReportInfo['Semester'];
		
		$isCmpSubject = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
		$CmpSubjectArr = ($isCmpSubject) ?  $this->GET_COMPONENT_SUBJECT($ParentSubjectID, $ClassLevelID) : $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID); 
		
		# Set to NotCompleted
		if($isComplete == 0){
			# For Term Report - need to consider the complete status of Whole Year Report
			if($ReportType != "F"){
				$ReportIDArr = $this->ReportTemplateList($ClassLevelID, 0);
				
				if(count($ReportIDArr) > 0){
					for($i=0 ; $i<count($ReportIDArr) ; $i++){
						$whole_year_report_id = $ReportIDArr[$i]['ReportID'];
						
						if($ReportIDArr[$i]['Semester'] == "F"){
							$isRelatedReportID = 0;
							$WholeYearReportInfo = $this->returnReportTemplateColumnData($whole_year_report_id);
							if(count($WholeYearReportInfo) > 0){
								for($k=0 ; $k<count($WholeYearReportInfo) ; $k++){
									if($ReportType == $WholeYearReportInfo[$k]['SemesterNum'])
										$isRelatedReportID = 1;
								}
							}
							
							if($isRelatedReportID){
								if($isCmpSubject || (!$isCmpSubject && !empty($CmpSubjectArr))){
									# For Whole Year Report AND Term Report
									# Get Complete Status of Parent Subject 
									if($isCmpSubject)
										$ProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($ParentSubjectID, $whole_year_report_id, $ClassID);
									else 
										$ProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $whole_year_report_id, $ClassID);
									
									if(count($ProgressArr) > 0 && $ProgressArr['IsCompleted'] == 1){
										if(!empty($CmpSubjectArr)){	
											for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
												$cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
												
												# Get any existing record of Component Subject in Marksheet Submission Progress
												$CmpSubmissionProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $whole_year_report_id, $ClassID);
												
												if(count($CmpSubmissionProgressArr) > 0){	
													# UPDATE - if having existing records
													$result['update_cmp_complete_'.$j] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $whole_year_report_id, $ClassID, 0);
												} else {	
													# INSERT - if do not have existing records (first time to update marksheet submission progress of this term subject of a class
													$result['insert_cmp_complete_'.$j] = $this->INSERT_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $whole_year_report_id, $ClassID, 0);
												}
											}
										}
										# UPDATE - parent subject
										if($isCmpSubject)
											$result['update_parent_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($ParentSubjectID, $whole_year_report_id, $ClassID, 0);
										else 
											$result['update_parent_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $whole_year_report_id, $ClassID, 0);
									}
								}
							} 
						}
					}
				}
			}
			
			//if($isCmpSubject || (!$isCmpSubject && !empty($CmpSubjectArr)) ){				
				# For Whole Year Report AND Term Report
				# Get Complete Status of Parent Subject 
				if($isCmpSubject)
					$SubProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($ParentSubjectID, $ReportID, $ClassID);
				else 
					$SubProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID);
				
				# When Complete Status of Parent Subject is 1(Completed), 
				# reset Complete Stuats of itselt and that of All its Component Subject(s) to 0(NotCompleted)
				if(count($SubProgressArr) > 0 && $SubProgressArr['IsCompleted'] == 1){
					if(!empty($CmpSubjectArr)){	
						for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
							$cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
							
							# Get any existing record of Component Subject in Marksheet Submission Progress
							$CmpSubmissionProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID);
							
							if(count($CmpSubmissionProgressArr) > 0){	
								# UPDATE - if having existing records
								$result['update_cmp_complete_'.$j] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID, 0);
							} else {	
								# INSERT - if do not have existing records (first time to update marksheet submission progress of this term subject of a class
								$result['insert_cmp_complete_'.$j] = $this->INSERT_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID, 0);
							}
						}
					}
					# UPDATE - parent subject
					if($isCmpSubject)
						$result['update_parent_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($ParentSubjectID, $ReportID, $ClassID, 0);
					else 
						$result['update_parent_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 0);
				} else {
					# Set its Complete Status to 0(Not Completed)
					$result['update_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 0);
				}
			//}
		} 
		# Set to Completed
		else {
			if(!empty($CmpSubjectArr)){	
				for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
					$cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
					
					# Get any existing record of Component Subject in Marksheet Submission Progress
					$CmpSubmissionProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID);
					
					if(count($CmpSubmissionProgressArr) > 0){	
						# UPDATE - if having existing records
						$result['update_cmp_complete_'.$j] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID, 1);
					} else {	
						# INSERT - if do not have existing records (first time to update marksheet submission progress of this term subject of a class
						$result['insert_cmp_complete_'.$j] = $this->INSERT_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID, 1);
					}
				}
			}
			
			# Get any existing record of Non-Component Subject in Marksheet Submission Progress
			$ProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID);
			
			if(count($ProgressArr) > 0){	
				# UPDATE - if having existing records
				$result['update_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 1);
			} else {	
				# INSERT - if do not have existing records (first time to update marksheet submission progress of this term subject of a class
				$result['insert_complete'] = $this->INSERT_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 1);
			}
			
			//$result['update_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 1);
		}
		
		$returnVal = (!in_array(false, $result)) ? true : false;
		
		return $returnVal;
	}
	*/
	
	/*
	function GET_PARENT_SUBJECT_ID($CmpSubjectID){
		$returnVal = '';
		if($CmpSubjectID != "" && $CmpSubjectID != null){
			$sql = "SELECT 
						b.RecordID 
					FROM 
						{$eclass_db}.ASSESSMENT_SUBJECT a, 
						{$eclass_db}.ASSESSMENT_SUBJECT b, 
					WHERE 
						a.RecordID = $ParSubjectID AND 
						a.CODEID = b.CODEID AND 
						b.CMP_CODEID = NULL 
					";
			$returnArr = $this->returnVector($sql, 1);
			if(count($returnArr) > 0){
				if($returnArr[0] != NULL && $returnArr[0] != "")
					$returnVal = $returnVal[0];
			}
		}
		return $returnVal;				
	}
	*/
	
	# mark		: source of mark
	# ReportType: Term , Whole Year Report
	# type		: SubjectScore , SubjectTotal
	function ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($mark, $ReportType, $type) {
		global $storageDisplaySettings;
		if (!isset($storageDisplaySettings)) {
			$storageDisplaySettings = $this->LOAD_SETTING("Storage&Display");
		}
		
		$mark = $this->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($mark, $storageDisplaySettings[$type]);
		
		return $mark;
	}
	
	################ defined in Yat Woon ##################
	/*
	function GET_SUBJECT_NAME_LANG($SubjectID, $Lang='EN') 
	{
		global $eclass_db;
		
		$select = $Lang."_DES";
		$sql = "SELECT
					{$select}
				FROM
					{$eclass_db}.ASSESSMENT_SUBJECT
				WHERE
					RecordID = '$SubjectID'
				";
		$row = $this->returnVector($sql);
		return $row[0];
	}
	*/
	#######################################################
	 
	/*
	# Get Report Student Selection
	function GET_REPORT_STUDENT_SELECTION($ParClassID)
	{
		$NameField = getNameFieldByLang2("a.");
		$ClassNumField = getClassNumberField("a.");
		$sql = "SELECT
					a.UserID,
					CONCAT('<', a.ClassName, ' - ', $ClassNumField, '> ', $NameField)
				FROM
					INTRANET_USER as a
					LEFT JOIN INTRANET_CLASS as b ON a.ClassName = b.ClassName
				WHERE
					b.ClassID = '$ParClassID'
					AND b.RecordStatus = 1
					AND a.RecordType = 2
					AND a.RecordStatus = 1
				ORDER BY
					$ClassNumField
				";
		$row = $this->returnArray($sql, 2);

		if(!empty($row))
		{
			$ReturnValue = "<SELECT size='21' multiple='multiple' name=\"TargetStudentID[]\">";
			for($i=0; $i<sizeof($row); $i++)
			{
				list($StudentID, $StudentName) = $row[$i];
				$ReturnValue .= "<option value='".$StudentID."' SELECTED>".$StudentName."</option>\n";
			}
			$ReturnValue .= "</SELECT>";
		}

		return $ReturnValue;
	}
	*/
	
	/*
	# Check Whether all column weights are zero or not 
	# corresponding to a Class Level and a Report only
	function IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $SubjectID="", $ReportColumnID=""){
		$returnVal = '';
		$isAllColumnWeightZero = 1;
		
		# Load Settings - Calculation
		$categorySettings = $this->LOAD_SETTING('Calculation');
		
		# Get Report Info
		$ReportInfo = $this->returnReportTemplateColumnData($ReportID);
		$ReportType = $ReportInfo['Semester'];
		
		# Get Existing Report Calculation Method
		$CalculationType = ($ReportType == "F") ? $categorySettings['OrderFullYear'] : $categorySettings['OrderTerm'];
				
		# Get Report Column Weight
		$WeightAry = array();
		if($CalculationType == 2){
			if($ReportColumnID != ""){
				$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$ReportColumnID;
				$weight_data = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
						
				if($weight_data[0]['Weight'] > 0) 
					$isAllColumnWeightZero = 0;
			} else {
				if(count($ReportInfo) > 0){
			    	for($i=0 ; $i<sizeof($ReportInfo) ; $i++){
						$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$ReportInfo[$i]['ReportColumnID'];
						$weight_data = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
						
						if($weight_data[0]['Weight'] > 0){
							$isAllColumnWeightZero = 0;
							continue;
						}
					}
				}
			}
		}
		else {
			$weight_data = $this->returnReportTemplateSubjectWeightData($ReportID);  
			
			for($i=0 ; $i<sizeof($weight_data) ; $i++){
				if($weight_data[$i]['SubjectID'] == $SubjectID && $weight_data[$i]['Weight'] > 0){
					if($ReportColumnID != ""){
						if($weight_data[$i]['ReportColumnID'] == $ReportColumnID){
							$isAllColumnWeightZero = 0;
							continue;
						}
					} else {
						$isAllColumnWeightZero = 0;
						continue;
					}
				}	
			}
		}
		
		$returnVal = $isAllColumnWeightZero;
		return $returnVal;
	}
	*/
	
	/*
	# Copy ClassLevel Settings [SchemeID, ScaleInput, ScaleDisplay]
	function COPY_CLASSLEVEL_SETTINGS($frClassLevelID, $toClassLevelID){
		$resultCopy = array();
		
		# Get Subject Form Grading Data of Source
		$frSubjectDataArr = array();
		$frSubjectDataArr = $this->GET_FROM_SUBJECT_GRADING_SCHEME($frClassLevelID);
		
		# Get Subject Form Grading Data of Target ClassLevel
		$toSubjectDataArr = array();
		$toSubjectDataArr = $this->GET_FROM_SUBJECT_GRADING_SCHEME($toClassLevelID);
		
		if(count($frSubjectDataArr) > 0 && count($toSubjectDataArr) > 0){
			$frSubjectIDArr = array_keys($frSubjectDataArr);
			$toSubjectIDArr = array_keys($toSubjectDataArr);
			
			# Get SubjectID which is matched among Source & Target ClassLevel
			$MatchedSubjectIDArr = array();
			for($i=0 ; $i<count($toSubjectIDArr) ; $i++){
				if(in_array($toSubjectIDArr[$i], $frSubjectIDArr))
					$MatchedSubjectIDArr[] = $toSubjectIDArr[$i];
			}
			
			if(count($MatchedSubjectIDArr) > 0){
				$UpdateSubjectDataArr = array();
				
				# Get Necessary Data for copying,[SchemeID, ScaleInput, ScaleDisplay]
				for($i=0 ; $i<count($MatchedSubjectIDArr) ; $i++){
					$SubjectID = $MatchedSubjectIDArr[$i];
					
					# Copy operates only when 
					# The Target ClassLevel Subject does not have assigned any Scheme AND 
					# The Source ClassLevel Subject contains a Grading Scheme
					if( $frSubjectDataArr[$SubjectID]['schemeID'] != "" && $frSubjectDataArr[$SubjectID]['schemeID'] != null && 
						($toSubjectDataArr[$SubjectID]['schemeID'] == "" || $toSubjectDataArr[$SubjectID]['schemeID'] == null) ){
							$UpdateSubjectDataArr[$SubjectID]['schemeID'] = $frSubjectDataArr[$SubjectID]['schemeID'];
						
						# Get Grading Scheme Info
						$SchemeMainInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($frSubjectDataArr[$SubjectID]['schemeID']);
						$SchemeType = $SchemeMainInfo['SchemeType'];
						
						if($SchemeType == "H"){
							if( $frSubjectDataArr[$SubjectID]['scaleInput'] != "" && $frSubjectDataArr[$SubjectID]['scaleInput'] != null &&
								($toSubjectDataArr[$SubjectID]['scaleInput'] == "" || $toSubjectDataArr[$SubjectID]['scaleInput'] == null) )
								$UpdateSubjectDataArr[$SubjectID]['scaleInput'] = $frSubjectDataArr[$SubjectID]['scaleInput'];
								
							if( $frSubjectDataArr[$SubjectID]['scaleDisplay'] != "" && $frSubjectDataArr[$SubjectID]['scaleDisplay'] != null &&
								($toSubjectDataArr[$SubjectID]['scaleDisplay'] == "" || $toSubjectDataArr[$SubjectID]['scaleDisplay'] == null) )
								$UpdateSubjectDataArr[$SubjectID]['scaleDisplay'] = $frSubjectDataArr[$SubjectID]['scaleDisplay']; 
						} else {
							# If SchemeType is PassFail(PF), set scaleInput & scaleDisplay for default value
							$UpdateSubjectDataArr[$SubjectID]['scaleInput'] = "";
							$UpdateSubjectDataArr[$SubjectID]['scaleDisplay'] = "";
						}
					} else {
						$resultCopy[$SubjectID] = 0;
					}
				}
				
				# Update ClassLevel Settings Info of Target ClassLevel - [Copy To Operation]
				if(count($UpdateSubjectDataArr) > 0){
					foreach($UpdateSubjectDataArr as $UpdateID => $data){
						$UpdateDataArr = array();
						
						foreach($data as $field => $value){
							$UpdateDataArr[$field] = $value;
						}
						$UpdateDataArr['subjectID'] = $UpdateID;
						$UpdateDataArr['classLevelID'] = $toClassLevelID;
						
						$resultCopy[$UpdateID] = $this->UPDATE_SUBJECT_FORM_GRADING($UpdateDataArr);
					}
				}
			} 
		}
		
		return $resultCopy;
	}
	*/
	
	/*
	# Get Last Modified Info of Parent Subject having Component Subjects
	function GET_CLASS_PARENT_MARKSHEET_LAST_MODIFIED($ParentSubjectID, $CmpSubjectIDArr, $ReportColumnIDList, $ClassID){
		$returnArr = array();
		
		if($ParentSubjectID != "" && count($CmpSubjectIDArr) > 0){
			$ParentLastModifiedArr = array();
			$ParentLastModifiedArr = $this->GET_CLASS_MARKSHEET_LAST_MODIFIED($ParentSubjectID, $ReportColumnIDList);
			
			$CmpLastModifiedArr = array();
			for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
				$CmpLastModifiedArr[$CmpSubjectIDArr[$i]] = $this->GET_CLASS_MARKSHEET_LAST_MODIFIED($CmpSubjectIDArr[$i], $ReportColumnIDList);
			}
			
			
			# Compare the Last Modified Time among Component Subjects
			$tmpLastModifiedTime = array();
			$CmpLastModifiedTime = '';
			for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
				if(isset($CmpLastModifiedArr[$CmpSubjectIDArr[$i]][$ClassID])){
					$tmpTime = $CmpLastModifiedArr[$CmpSubjectIDArr[$i]][$ClassID][0];
					if($i != 0 && strtotime($CmpLastModifiedTime) - strtotime($tmpTime) < 0){
						$CmpLastModifiedTime = $tmpTime;
						$tmpLastModifiedTime = $CmpLastModifiedArr[$CmpSubjectIDArr[$i]][$ClassID];
					} 
					if($i == 0){
						$CmpLastModifiedTime = $tmpTime;
						$tmpLastModifiedTime = $CmpLastModifiedArr[$CmpSubjectIDArr[$i]][$ClassID];
					}
				}
			}
			
			if(isset($ParentLastModifiedArr[$ClassID]) && count($ParentLastModifiedArr[$ClassID]) > 0){
				$returnArr[$ClassID] = $ParentLastModifiedArr[$ClassID];
				$LastModifiedTime = $ParentLastModifiedArr[$ClassID][0];
					
				# Compare the Last Modified Time between Parent Subject & the lated Modified Time of Component Subject
				if($CmpLastModifiedTime != ""){
					if(strtotime($CmpLastModifiedTime) - strtotime($LastModifiedTime) > 0){
						$returnArr[$ClassID] = $tmpLastModifiedTime;
					}
				}
			} else if(isset($tmpLastModifiedTime)){
				$returnArr[$ClassID] = $tmpLastModifiedTime;
			}
		}
		
		return $returnArr;
	}
	*/
	
	# To Check whether the Full Mark of input special case is count for total full mark or not
	# $scString : string of special case
	function IS_COUNT_FULLMARK_OF_SPECIALCASE($scString, $absentExemptSettings){
		$isCount = 1;
		if($scString == "-"){
	    	$isCount = ($absentExemptSettings['Absent'] == "ExFullMark") ? 0 : 1;
		} else if($scString == "/"){
			$isCount = ($absentExemptSettings['Exempt'] == "ExFullMark") ? 0 : 1;
		} else if($scString == "*" || $scString == "N.A."){
    		$isCount = 0;
		}
		return $isCount;
	}
	
	# Get value For Absent('-') & Exempt('/') special case Determined by Settings of System
	# $Type : Absent or Exemt
	function GET_VALUE_FROM_ABSENT_EXEMPT_SPECIAL_CASE($Type, $absentExemptSettings){
		$returnVal = '';
		if($absentExemptSettings[$Type] == "ExWeight"){
			$returnVal = '';
		} else if($absentExemptSettings[$Type] == "ExFullMark"){
			$returnVal = '';
		} else if($absentExemptSettings[$Type] == "Mark0"){
			$returnVal = 0;
		} 
		return $returnVal;
	}
	
	// overiding the function in libeRc2008
	# Case 1 occurs when $isOverallScore = 0  
	# Check if the score of (all students in a class of) an Assessment Column of an subject have been filled in
	#
	# Case 2 occurs when $isOverallScore = 1 
	# Check if the overall score of (all students in a class of) an overall term subject mark OR 
	# overall subject mark Column of an subject have been filled in when 
	# the scheme type is honor-based with Input Grade(G) OR The scheme type is PassFail(PF)
	# the Input variable $ReportColumnID regarded as $ReportID 
	#
	# Return "all" if no scores have been filled in the column
	# Return the number of filled scores otherwise
//	function IS_MARKSHEET_SCORE_EMPTY($StudentIDList, $SubjectID, $ReportColumnID, $isOverallScore=0) {
//		$StudentSize = sizeof($StudentIDList);
//		if ($StudentSize == 0)
//			return true;
//		$StudentIDList = implode(",", $StudentIDList);
//		$table = (!$isOverallScore) ? $this->DBName.".RC_MARKSHEET_SCORE" : $this->DBName.".RC_MARKSHEET_OVERALL_SCORE";
//		$sql = "SELECT ";
//		$sql .= (!$isOverallScore) ? "MarksheetScoreID " : "MarksheetOverallScoreID ";
//		$sql .= "FROM $table WHERE ";
//		$sql .= "SubjectID = '$SubjectID' AND ";
//		$sql .= (!$isOverallScore) ? "ReportColumnID = '$ReportColumnID' AND " : "ReportID = '$ReportColumnID' AND ";
//		$sql .= (!$isOverallScore) ? "(MarkRaw <> '-1' OR MarkNonNum <> '') AND " : "MarkNonNum <> '' AND ";
//		$sql .= "StudentID IN ($StudentIDList)";
//		$result = $this->returnArray($sql, 1);
//		
//		if (sizeof($result) == 0) {
//			return "all";
//		} else if (sizeof($result) > 0) {
//			return sizeof($result);
//		}
//	}
	
	# INSERT marksheet overall scores without raw mark record for the cases
	# when the scheme type is Honor-Based (H) AND its scaleinput is Grade(G) OR
	# when the scheme type is PassFail-Based (PF)
	function INSERT_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $ReportID, $LastModifiedUserID="") {
		if ($LastModifiedUserID == "")
			$LastModifiedUserID = $this->uid;
		$existStudentID = array_keys($this->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $ReportID));
		$StudentIDArr = array_diff($StudentIDArr, $existStudentID);
		sort($StudentIDArr);
		if (sizeof($StudentIDArr) > 0) {
			$table = $this->DBName.".RC_MARKSHEET_OVERALL_SCORE";
			$fields = "StudentID, SubjectID, ReportID, LastModifiedUserID, DateInput, DateModified";
			$sql = "INSERT Ignore INTO $table ($fields) VALUES ";
			for($i=0; $i<sizeof($StudentIDArr); $i++) {
				$entries[] = "(".$StudentIDArr[$i].", '$SubjectID', '$ReportID',  
								'$LastModifiedUserID', NOW(), NOW())";
			}
			$sql .= implode(",", $entries);
			$success = $this->db_db_query($sql);
			
			return $success;
		} else {
			return true;
		}
	}
	
	# UPDATE marksheet overall scores
	function UPDATE_MARKSHEET_OVERALL_SCORE($MarksheetOverallScoreArr) {
		$success = array();
		$table = $this->DBName.".RC_MARKSHEET_OVERALL_SCORE";
		$sqlUpdate = "UPDATE $table SET ";
		for($i=0; $i<sizeof($MarksheetOverallScoreArr); $i++) {
			$sql = "SchemeID = '".$MarksheetOverallScoreArr[$i]["SchemeID"]."', ";
			$sql .= "MarkType = '".$MarksheetOverallScoreArr[$i]["MarkType"]."', ";
			$sql .= "MarkNonNum = '".$MarksheetOverallScoreArr[$i]["MarkNonNum"]."', ";
			$sql .= "IsEstimated = '".$MarksheetOverallScoreArr[$i]["IsEstimated"]."', ";
			$sql .= "LastModifiedUserID = '".$MarksheetOverallScoreArr[$i]["LastModifiedUserID"]."', ";
			$sql .= "DateModified = NOW() ";
			$sql .= "WHERE ";
			if (isset($MarksheetOverallScoreArr[$i]["MarksheetOverallScoreID"]) &&  $MarksheetOverallScoreArr[$i]["MarksheetOverallScoreID"] != "") {
				$sql .= "MarksheetOverallScoreID = '".$MarksheetOverallScoreArr[$i]["MarksheetOverallScoreID"]."'";
			} else {
				$sql .= "StudentID = '".$MarksheetOverallScoreArr[$i]["StudentID"]."' AND ";
				$sql .= "SubjectID = '".$MarksheetOverallScoreArr[$i]["SubjectID"]."' AND ";
				$sql .= "ReportID = '".$MarksheetOverallScoreArr[$i]["ReportID"]."'";
			}
			$sql = $sqlUpdate.$sql;
			
			$success[] = $this->db_db_query($sql);
		}
		return !in_array(false, $success);
	}
	
	/* Copied to libreportcard2008.php
	# Filling NA in the empty scores of Marksheet
	function FILL_EMPTY_SCORE_WITH_NA($StudentIDList, $SubjectID, $ReportColumnID, $isOverallScore=0) {
		$success = true;
		$StudentSize = sizeof($StudentIDList);
		if ($StudentSize == 0)
			return true;
		$StudentIDList = implode(",", $StudentIDList);
		$table = (!$isOverallScore) ? $this->DBName.".RC_MARKSHEET_SCORE" : $this->DBName.".RC_MARKSHEET_OVERALL_SCORE";
		$sql = "SELECT ";
		$sql .= (!$isOverallScore) ? "MarksheetScoreID " : "MarksheetOverallScoreID ";
		$sql .= "FROM $table WHERE ";
		$sql .= "SubjectID = '$SubjectID' AND ";
		$sql .= (!$isOverallScore) ? "ReportColumnID = '$ReportColumnID' AND " : "ReportID = '$ReportColumnID' AND ";
		$sql .= (!$isOverallScore) ? "MarkRaw = '-1' AND " : "";
		$sql .= "MarkNonNum = '' AND ";
		$sql .= "StudentID IN ($StudentIDList)";
		$MarksheetScoreIDArr = $this->returnVector($sql, 1);
		
		if(count($MarksheetScoreIDArr) > 0){
			# Fill in All Empty Scores in Marksheet By N.A.
			$MarksheetScoreIDList = implode(",", $MarksheetScoreIDArr);
			$sql = "UPDATE $table SET 
						MarkType = 'SC', 
						MarkNonNum = 'N.A.', 
						LastModifiedUserID = ".$this->uid.", 
						DateModified = NOW() 
					WHERE 
						".((!$isOverallScore) ? "MarksheetScoreID" : "MarksheetOverallScoreID")." IN ($MarksheetScoreIDList)";
			
			$success = $this->db_db_query($sql);
		}
		
		return $success;
	}
	*/
	
	# Use for the whole year report for calculating term subject mark of subject without term report but having component subjects
	# ReportID: Whole Year ReportID / TermReportID
	function GET_STUDENT_TERM_SUBJECT_MARK_WITHOUT_TEMPLATE($StudentIDArr, $ReportID, $ReportColumnID, $SubjectID, $CmpSubjectIDArr=array()){
		$returnArr = array();
		$ReportInfo = array();			// WholeYear Report Info 	  / Term Report Info
		$ReportColumnInfo = array();	// WholeYear Term Column Info / Term Assessment Column Info
		$MarksheetScoreInfoArr = array();
		$ParentSubMarksheetScoreInfoArr = array();
		
		# Get Absent&Exempt Settings
		$absentExemptSettings = $this->LOAD_SETTING('Absent&Exempt');
		
		# Get Report Template Info
		$ReportInfo = $this->returnReportTemplateBasicInfo($ReportID);				// Basic Data
		$ClassLevelID = $ReportInfo['ClassLevelID'];
		
		# Get Full Mark of Parent Subject
		$ParentFullMark = $this->GET_SUBJECT_FULL_MARK($SubjectID, $ClassLevelID, $ReportID);
		
		if($ReportID != "" && $SubjectID != ""){
			$ReportInfo = $this->returnReportTemplateBasicInfo($ReportID);			// Basic Data
			$ReportColumnInfo = $this->returnReportTemplateColumnData($ReportID); 	// Column Data;
				   
		    if(count($CmpSubjectIDArr) == 0){	
			    # Case 1 - common subject
			} 
			else {
				$TotalCmpSubjectWeight = 0;
				# Case 2 - subject contains component subjects
				for($j=0 ; $j<sizeof($CmpSubjectIDArr) ; $j++){
					$CmpSubjectID = $CmpSubjectIDArr[$j];
					$ParentSubMarksheetScoreInfoArr[$CmpSubjectID] = $this->GET_MARKSHEET_SCORE($StudentIDArr, $CmpSubjectID, $ReportColumnID);
				}
			}
			
			if(!empty($StudentIDArr)){
				foreach($StudentIDArr as $StudentID){
					$notCompletedFlag = 0;
					$termMark = '';
					$Count = 0;
					
					if(count($CmpSubjectIDArr) == 0){
						# Case 1: Common Subject without any Component Subjects
					}
					else if(!empty($CmpSubjectIDArr)){	
						# Check for Combination of Special Case of Input Mark
				        $SpecialCaseArr = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
				        foreach($SpecialCaseArr as $key){
				        	$SpecialCaseCountArr[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
			        	}
						
						# Case 2: Subject having Component Subjects & Calculation Type is 2 (Down-Right Method)
						for($i=0 ; $i<sizeof($CmpSubjectIDArr) ; $i++){	
							$CmpSubjectID = $CmpSubjectIDArr[$i];
							$TotalCmpSubjectWeight = 0;
							$TotalCmpFullMark = 0;
							
							if(count($ParentSubMarksheetScoreInfoArr[$CmpSubjectID]) > 0){
								# Get the Total Component Subject Full Mark
								for($j=0 ; $j<count($CmpSubjectIDArr) ; $j++){							
									# Get Mark Details for Temporary use only
									$tmpType = $ParentSubMarksheetScoreInfoArr[$CmpSubjectIDArr[$j]][$StudentID]['MarkType'];
									$tmpNonNum = $ParentSubMarksheetScoreInfoArr[$CmpSubjectIDArr[$j]][$StudentID]['MarkNonNum'];
									
									$isCountWeight = ($tmpType == "SC") ? $this->IS_COUNT_WEIGHT_OF_SPECIALCASE($tmpNonNum, $absentExemptSettings) : 1;
									
//									$OtherCondition = "SubjectID = ".$CmpSubjectIDArr[$j]." AND ReportColumnID = $ReportColumnID";
									# 20140724 Component Subject Marks calculation 
									$OtherCondition = "SubjectID = ".$CmpSubjectIDArr[$j]." AND ReportColumnID IS NULL";
									$CmpSubjectWeight[$CmpSubjectIDArr[$j]] = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
					    			$TotalCmpSubjectWeight += ($isCountWeight) ? $CmpSubjectWeight[$CmpSubjectIDArr[$j]][0]['Weight'] : 0;
									
									# Get Total Full Mark of All Component Subjects
									$CmpFullMark = $this->GET_SUBJECT_FULL_MARK($CmpSubjectIDArr[$j], $ClassLevelID, $ReportID);
									$tmpCmpFullMark = ($CmpFullMark != "") ? $CmpFullMark * $CmpSubjectWeight[$CmpSubjectIDArr[$j]][0]['Weight'] : 0;
									$TotalCmpFullMark += ($isCountWeight) ? (($tmpCmpFullMark != "") ? $tmpCmpFullMark : 0) : 0;
								}
								
								# Get Full Mark Factor for calculating the mark of Parent Subject 
								$Factor = $this->GET_FULL_MARK_FACTOR($ParentFullMark, $TotalCmpFullMark);
								
								# Main
								$MarkType = $ParentSubMarksheetScoreInfoArr[$CmpSubjectID][$StudentID]['MarkType'];
								$MarkNonNum = $ParentSubMarksheetScoreInfoArr[$CmpSubjectID][$StudentID]['MarkNonNum'];
								
								if($MarkType == "M"){	// term weighted mark
									$Value = $ParentSubMarksheetScoreInfoArr[$CmpSubjectID][$StudentID]['MarkRaw'];
									
									if($Value == "" || $Value < 0){
										$notCompletedFlag = 1;
									} else {
										//$tmpMark = ($TotalCmpSubjectWeight > 0) ? ($Value * $CmpSubjectWeight[$CmpSubjectID][0]['Weight']) / $TotalCmpSubjectWeight : "";
										$tmpMark = ($Factor > 0) ? ($Value * $CmpSubjectWeight[$CmpSubjectID][0]['Weight']) * $Factor : "";
										$termMark += $tmpMark;
									}
								}
								else if($MarkType == "SC"){	// special case
									$SpecialCaseCountArr = $this->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCountArr, $MarkNonNum, $absentExemptSettings);
									$scMark = $SpecialCaseCountArr[$MarkNonNum]['Value'];
									$Count = $SpecialCaseCountArr[$MarkNonNum]['Count'];
									$isCount = $SpecialCaseCountArr[$MarkNonNum]['isCount'];
										
									if($isCount) $termMark += $scMark;	// scMark : SpecialCaseMark
								}
							}
							else {
								$notCompletedFlag = 1;
							}
						}
						
						# Checking for any combination of special cases without any Mark after Step 1 (Right)
						$setSpecialCase = 0;
						$setSpecialCaseValue = '';
						foreach($SpecialCaseCountArr as $key => $scData){	// scData : SpecialCaseData
							if($scData['Count'] == count($CmpSubjectIDArr)){
								$setSpecialCase = 1;
								$setSpecialCaseValue = $key;
							}
						}
					}
					
					# Control Display Value
					if($notCompleteFlag){
						$returnArr[$StudentID] = "NOTCOMPLETED";
					} else {
						if($setSpecialCase){
							$returnArr[$StudentID] = $setSpecialCaseValue;
						} else {
							$returnArr[$StudentID] = $termMark;
						}
					}
					
				}
			}
		}	
		return $returnArr;
	}
	
	# Check whether the grading scheme range info is valid or not for particular nature (Distinction-D, Pass-P, Fail-F)
	# Status of Return Value (-1: no any record , 0: not completed record , 1: valid record)
	function IS_GRADING_SCHEME_RANGE_INFO_VALID($ParSchemeID, $Nature="ALL", $ScaleDisplay){
		$returnVal = -1;
		if($ParSchemeID != ""){
			$schemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($ParSchemeID);
			$gradeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($ParSchemeID, $Nature);
			
			if(count($gradeRangeInfo) > 0){
				for($i=0 ; $i<count($gradeRangeInfo) ; $i++){
					if($schemeInfo['TopPercentage'] == 0 ){		// Mark Range
						// if both values are null, ignore this record
						if($gradeRangeInfo[$i]['LowerLimit'] == "" && $gradeRangeInfo[$i]['Grade'] == "")
							continue;
							
						if($ScaleDisplay == "G"){		// Check Grade when ScaleDisplay is Grade
							if($gradeRangeInfo[$i]['Grade'] == "")
								$returnVal = 0;
						}
						else {			// Check Mark when ScaleDisplay is Mark
							if($gradeRangeInfo[$i]['LowerLimit'] == "")
								$returnVal = 0;
						}
					}
					else {		// Percentage Range
						if($gradeRangeInfo[$i]['UpperLimit'] == "" && $gradeRangeInfo[$i]['Grade'] == "")
							continue;
						
						if($ScaleDisplay == "G"){		
							if($gradeRangeInfo[$i]['Grade'] == "")
								$returnVal = 0;
						}
						else {
							if($gradeRangeInfo[$i]['UpperLimit'] == "")
								$returnVal = 0;
						}
					}
				}
				
				if($returnVal != 0)
					$returnVal = 1;
			}
		}
		return $returnVal;
	}
	
	// override from andy
	function UPDATE_GRADING_SCHEME_AND_RANGE($SchemeInfo, $RangeInfo) {
		$table = $this->DBName.".RC_GRADING_SCHEME";
		# update scheme info first
		$sqlScheme = "UPDATE $table SET 
						SchemeTitle = '".$SchemeInfo["SchemeTitle"]."',
						SchemeType = '".$SchemeInfo["SchemeType"]."',
						FullMark = ".$SchemeInfo["FullMark"].",
						PassMark = ".$SchemeInfo["PassMark"].",
						TopPercentage = ".$SchemeInfo["TopPercentage"].",
						Pass = '".$SchemeInfo["Pass"]."',
						Fail = '".$SchemeInfo["Fail"]."', 
						DateModified = NOW() 
					WHERE SchemeID = '".$SchemeInfo["SchemeID"]."'";
		$success = $this->db_db_query($sqlScheme);
		if (!success) return false;
		
		# delete all ranges of the scheme if SchemeType is set to "PF" (Pass/Fail)
		if ($SchemeInfo["SchemeType"] == "PF") {
			$success = $this->DELETE_GRADING_SCHEME_RANGE($SchemeInfo["SchemeID"]);
			return $success;
			# updated END Here for this case
		}
		
		# get the ID of existing ranges for counter checking
		$existRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($SchemeInfo["SchemeID"]);
		if (sizeof($existRangeInfo) > 0) {
			for($i=0; $i<sizeof($existRangeInfo); $i++) {
				$existRangeID[] = $existRangeInfo[$i]["GradingSchemeRangeID"];
			}
		} else {
			$existRangeID = array();
		}
		
		# for error checking
		$success = array();
		
		# check the GradingSchemeRangeID to divide the RangeInfo array for DB operation 
		if (sizeof($RangeInfo) > 0) {
			for($i=0; $i<sizeof($RangeInfo); $i++) {
				$GradingSchemeRangeID = $RangeInfo[$i]["GradingSchemeRangeID"];
				# No GradingSchemeRangeID is provided, new range => INSERT
				if (empty($GradingSchemeRangeID)) {
					$newRangeInfo[] = $RangeInfo[$i];
				} #  GradingSchemeRangeID is exist in DB, edit range => UPDATE
				else if (in_array($GradingSchemeRangeID, $existRangeID)) {
					# remove the matched GradingSchemeRangeID from existRangeID,
					# it can be used to detemine which ranges need to be DELETE
					$keyMatch = array_search($GradingSchemeRangeID, $existRangeID);
					unset($existRangeID[$keyMatch]);
					#### UPDATE matched range ####
					$success[] = $this->UPDATE_GRADING_SCHEME_RANGE($GradingSchemeRangeID, $RangeInfo[$i]);
				}
			}
			
			####  INSERT new ranges ####
			$success[] = $this->INSERT_GRADING_SCHEME_RANGE($newRangeInfo);
		}
		
		#### DELETE removed ranges ####
		if (sizeof($existRangeID) > 0) {
			foreach($existRangeID as $value) {
				$success[] = $this->DELETE_GRADING_SCHEME_RANGE("", $value);
			}
		}
		return in_array(false, $success);
	}
	
	/*
	# Check Marksheet Status 
	# Status Mode: 1-Edit, 0-View
	# $ParCmpSubjectIDArr can be used for checking Subject which contains component subjects	
	function CHECK_MARKSHEET_STATUS($ParSubjectID, $ParClassLevelID, $ParReportID, $ParCmpSubjectIDArr=array()){
		$returnVal = 1;				// default as Edit Mode
		$hasDirectInputMark = 1;	// default as Edit Status
		$ColumnHasTemplateAry = array();
		
		if($ParSubjectID != "" && $ParClassLevelID != "" && $ParReportID != ""){
			# Get Grading Scheme Info of Parent / Common Subject
			$CommonSubjectFormGradingArr = array();
			$CommonSubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ParClassLevelID, $ParSubjectID);
			if(count($CommonSubjectFormGradingArr) > 0){
				$SchemeID = $CommonSubjectFormGradingArr['SchemeID'];
				$ScaleInput = $CommonSubjectFormGradingArr['ScaleInput'];
				$ScaleDisplay = $CommonSubjectFormGradingArr['ScaleDisplay'];
				
				$SchemeMainInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$SchemeType = $SchemeMainInfo['SchemeType'];
			}
			
			# Check Whether the Scale Input of parent subject is Grade(G) / PassFail(PF) or not
			if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
				$hasDirectInputMark = 1;
			}
			else {
				# Case for The Scale Input of parent subject is Mark(M)
				$basic_data = $this->returnReportTemplateBasicInfo($ParReportID);			// Basic  Data
				$column_data = $this->returnReportTemplateColumnData($ParReportID); 		// Column Data
				$ReportType = (count($basic_data) > 0) ?  $basic_data['Semester'] : "";
				
				if(count($column_data) > 0){	
					for($i=0 ; $i<sizeof($column_data) ; $i++){
						$ReportColumnID = $column_data[$i]['ReportColumnID'];
						$SemesterNum = $column_data[$i]['SemesterNum'];
						
						# Whole Year Report Template use
						if($ReportType == "F"){
							# Case 1a: Check for any term report template is defined for the use of whole year report
							$ColumnHasTemplateAry[$ReportColumnID] = $this->CHECK_REPORT_TEMPLATE_FROM_COLUMN($SemesterNum, $ParClassLevelID);
	
							if($ColumnHasTemplateAry[$ReportColumnID]){
								$hasDirectInputMark = 0;
							} else if(!$ColumnHasTemplateAry[$ReportColumnID]){
								$hasDirectInputMark = 1;
								continue;
							}
							
							# Case 1b: When Terms in Whole Year report have not defined, check whether the component subjects can calculate mark or not 
						}
					}
					
					# Check for Any subject having component subjects &
					# Case 2: if all ScaleInput of Component Subjects are "G", allow to edit ($hasDirectInputMark = 1)
					# Case 3: if anyone ScaleInput of Component Subjects is "M", do not allow to edit ($hasDirectInputMark = 1)
					if(!empty($ParCmpSubjectIDArr)){
						$ScaleInputArr = array();
						for($j=0 ; $j<count($ParCmpSubjectIDArr) ; $j++){
							$SubjectFormGradingArr = array();
							$SubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ParClassLevelID, $ParCmpSubjectIDArr[$j]);
							
							if(count($SubjectFormGradingArr) > 0){
								$ScaleInputArr[] = $SubjectFormGradingArr['ScaleInput'];
							}
						}
						
						if(count($ScaleInputArr) > 0){
							if(in_array("M", $ScaleInputArr)){	// Set to "0" as at least one ScaleInput of Component Subject is Mark(M)
								$hasDirectInputMark = 0;
							}
							else {			// Set to "1" as no any ScaleInput of Component Subject is Mark(M)
								$hasDirectInputMark = 1;
							}
						}
					}
				}
			}
		}
		
		if(!$hasDirectInputMark)
			$returnVal = 0;
			
		return $returnVal;
	}
	*/
	
	# Delete Report Template subject weight By ClassLevelID with SubjectID
	# this function is not gerenal use for Deleting Subject Weight & use for Subject Settings
	function DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT_BY_CLASSLEVELID($ParSubjectID, $ParClassLevelID){
		$result = array();
		if($ParSubjectID != "" && $ParClassLevelID != ""){
			$ReportTemplateArr= array();
			$table = $this->DBName.".RC_REPORT_TEMPLATE";
			$sql = "SELECT ReportID FROM $table WHERE ClassLevelID = '$ParClassLevelID'";
			$ReportTemplateArr = $this->returnArray($sql, 1);
			
			if(count($ReportTemplateArr) > 0){
				$table = $this->DBName.".RC_REPORT_TEMPLATE_SUBJECT_WEIGHT";
				for($i=0 ; $i<count($ReportTemplateArr) ; $i++){
					$sql = "DELETE FROM $table WHERE ReportID = '".$ReportTemplateArr[$i]['ReportID']."' AND SubjectID = '$ParSubjectID'";
					$result['delete_'.$i] = $this->db_db_query($sql);
				}
			}
		}
		if(!in_array(false, $result))
			return true;
		else 
			return false;
	}
	
	/*
	# Get Parent Subject Marksheet Score calculated by Marksheet Scores of Component Subjects 
	# Use when it contains Term Report Template (defined)
	# This function is used for Calculation Down-Right Method only
	# Pass StudentID Array, CmpSubjectID Array, ReportID, ReportColumnID
	# For each mark (M) of assessment(ReportColumn) of Parent Subject 
	# 	Formula: M = (M1 x W1 x G1) + (M2 x W2 x G2) + ... + (Mn x Wn x Gn) 
	# 	M1: Mark of Assessment of 1st Component Subject
	# 	W1: Weight of Assessment
	# 	G1: Full Mark Factor which is calculated by  Pf / total of (Cf x Wn)
	# 	Pf: Full Mark of Parent Subject
	# 	Cf: Full Mark of Component Subject
	function GET_PARENT_SUBJECT_MARKSHEET_SCORE($StudentIDArr, $ParentSubjectID, $CmpSubjectIDArr, $ReportID, $ReportColumnID){
		$returnArr = array();
		$MarksheetScoreArr = array();
		
		# Get Absent & Exempt Settings
		$absentExemptSettings = $this->LOAD_SETTING('Absent&Exempt');
		
		# Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
		$categorySettings = $this->LOAD_SETTING('Calculation');
		$TermCalculationType = $categorySettings['OrderTerm'];
		
		# Get Report Template Info
		$ReportInfo = $this->returnReportTemplateBasicInfo($ReportID);				// Basic Data
		$ClassLevelID = $ReportInfo['ClassLevelID'];
		
		# Get Full Mark of Parent Subject
		$ParentFullMark = $this->GET_SUBJECT_FULL_MARK($ParentSubjectID, $ClassLevelID);
		
		if(count($StudentIDArr) > 0 && count($CmpSubjectIDArr) > 0 && $ReportColumnID != ""){			
			for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
				# Get MarkSheet Score Data by Column in Student 
				$MarksheetScoreArr[$CmpSubjectIDArr[$i]] = $this->GET_MARKSHEET_SCORE($StudentIDArr, $CmpSubjectIDArr[$i], $ReportColumnID);
			}
			
			# Loop Students
			foreach($StudentIDArr as $StudentID){
				$TotalAssSubMark = 0;		// store the Total Assessment Subject Mark
				$notCount = 0;				// count for the happeness of Special Case for (/, *, N.A.)
				$notYetMark = 0;			// count for the Mark which is still not given
				
				# Check for Combination of Special Case of Input Mark Among Component Subjects 
		        $SpecialCaseArr = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		        foreach($SpecialCaseArr as $key){
		        	$SpecialCaseCountArr[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
	        	}
	        	
				# Loop The Component Subjects
				for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
					$TotalCmpSubjectWeight = 0;
					$TotalCmpFullMark = 0;
					
					if(count($MarksheetScoreArr[$CmpSubjectIDArr[$i]]) > 0){
						for($j=0 ; $j<count($CmpSubjectIDArr) ; $j++){							
							# Get Mark Details for Temporary use only
							$tmpType = $MarksheetScoreArr[$CmpSubjectIDArr[$j]][$StudentID]['MarkType'];
							$tmpNonNum = $MarksheetScoreArr[$CmpSubjectIDArr[$j]][$StudentID]['MarkNonNum'];
							
							$isCountWeight = ($tmpType == "SC") ? $this->IS_COUNT_WEIGHT_OF_SPECIALCASE($tmpNonNum, $absentExemptSettings) : 1;
																				
							# Get Personal Weight among Component Subjects of each student
							if($TermCalculationType == 1){
								$OtherCondition = "SubjectID = ".$CmpSubjectIDArr[$j]." AND ReportColumnID IS NULL ";
								$CmpSubjectWeight[$CmpSubjectIDArr[$j]] = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
							} else {
								$OtherCondition = "SubjectID = ".$CmpSubjectIDArr[$j]." AND ReportColumnID = $ReportColumnID";
								$CmpSubjectWeight[$CmpSubjectIDArr[$j]] = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
							}
					    	$TotalCmpSubjectWeight += ($isCountWeight) ? $CmpSubjectWeight[$CmpSubjectIDArr[$j]][0]['Weight'] : 0;
					    	
					    	# Get Total Full Mark of All Component Subjects
							$CmpFullMark = $this->GET_SUBJECT_FULL_MARK($CmpSubjectIDArr[$j], $ClassLevelID);
							$tmpCmpFullMark = ($CmpFullMark != "") ? $CmpFullMark * $CmpSubjectWeight[$CmpSubjectIDArr[$j]][0]['Weight'] : 0;
							$TotalCmpFullMark += ($isCountWeight) ? (($tmpCmpFullMark != "") ? $tmpCmpFullMark : 0) : 0;
						}
						
						#Get Full Mark Factor for calculating the mark of Parent Subject 
						$Factor = $this->GET_FULL_MARK_FACTOR($ParentFullMark, $TotalCmpFullMark);
						
						# Get Mark Details
						$MarkType = $MarksheetScoreArr[$CmpSubjectIDArr[$i]][$StudentID]['MarkType'];
						$MarkRaw = $MarksheetScoreArr[$CmpSubjectIDArr[$i]][$StudentID]['MarkRaw'];
						$MarkNonNum = $MarksheetScoreArr[$CmpSubjectIDArr[$i]][$StudentID]['MarkNonNum'];
						
						# Calculate the Weighted Mark of Parent Subject From the Mark of Component Subjects
						if($MarkType == "M" && $MarkRaw != "" && $MarkRaw >= 0){
							//$TotalAssSubMark += ($TotalCmpSubjectWeight > 0) ? ($MarkRaw * $CmpSubjectWeight[$CmpSubjectIDArr[$i]][0]['Weight']) / $TotalCmpSubjectWeight : "";
							$TotalAssSubMark += ($Factor != "" && $Factor > 0) ? $MarkRaw * $CmpSubjectWeight[$CmpSubjectIDArr[$i]][0]['Weight'] * $Factor : 0;
						} else if($MarkType == "M" && ($MarkRaw == "" || $MarkRaw == -1) ){
							$notYetMark++;
						} else if($MarkType == "SC"){					// Special Case checking
							$SpecialCaseCountArr = $this->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCountArr, $MarkNonNum, $absentExemptSettings);
							$scMark = $SpecialCaseCountArr[$MarkNonNum]['Value'];
							$notCount = $SpecialCaseCountArr[$MarkNonNum]['Count'];
							$isCount = $SpecialCaseCountArr[$MarkNonNum]['isCount'];
							
							if($isCount) $TotalAssSubMark += $scMark;	// scMark : SpecialCaseMark
						} else if($MarkType == "G"){
							if($MarkNonNum != "")
								$notCount++;
							else 
								$notYetMark++;
						}
					} else {
						$notYetMark++;
					}
				}
				
				# Checking for any combination of special cases without any Mark
				$isSetSpecialCase = 0;
				foreach($SpecialCaseCountArr as $key => $scData){	// scData : SpecialCaseData
					if($scData['Count'] == count($CmpSubjectIDArr)){
						$isSetSpecialCase = 1;
						$setSpecialCaseValue = $key;
					}
				}
				
				# Control the output of mark of each student
				if($notYetMark > 0)
					$returnArr[$StudentID] = "";
				else if($isSetSpecialCase)
					$returnArr[$StudentID] = $setSpecialCaseValue;
				else if($notCount == count($CmpSubjectIDArr))
					$returnArr[$StudentID] = "N.A.";
				else 
					$returnArr[$StudentID] = $TotalAssSubMark;
			}
		}
		return $returnArr;		
	}
	*/
	
	# Get the Term Subject Mark for each Student 
	# Case 1: From Common Subject without any Component Subjects
	# Case 2: From Subject having Component Subjects
	# Calculation Type (1:Right-Down , 2:Down-Right) Use for Component Subject Only
	# Special Handling - Support Vertical > Horizontal Calculation ($ApplySubjectHorizontalCalculation = true)
	function GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr=array(), $CalculationType=1, $CurrentReportID='', $ParTermReportColumnID='', $excludedTermReportColumnWeight=false, $ApplySubjectHorizontalCalculation=false){
		$returnArr = array();
		$TermReportInfo = array();
		$TermReportColumnInfo = array();
		$MarksheetScoreInfoArr = array();
		$ParentSubMarksheetScoreInfoArr = array();
		
		# Get Absent&Exempt Settings
		$absentExemptSettings = $this->LOAD_SETTING('Absent&Exempt');
		
		# Main
		if($TermReportID != "" && $SubjectID != ""){
			# Get Report Template Info
			$ReportInfo = $this->returnReportTemplateBasicInfo($TermReportID);				// Basic Data
			$ClassLevelID = $ReportInfo['ClassLevelID'];
			
			# Get Full Mark of Parent Subject
			$ParentFullMark = $this->GET_SUBJECT_FULL_MARK($SubjectID, $ClassLevelID, $TermReportID);

			# Get Current Report Basie Info if Current Report ID is set
			if($CurrentReportID != "")
				$CurrentReportInfo = $this->returnReportTemplateBasicInfo($CurrentReportID);
			
			# Get Term Report Column Info			
			$TermReportInfo = $this->returnReportTemplateBasicInfo($TermReportID);				// Basic Data
			$TermReportColumnInfo = $this->returnReportTemplateColumnData($TermReportID); 		// Column Data;
			if($ParTermReportColumnID > 0)
				$TermReportColumnInfo = $this->returnReportTemplateColumnData($TermReportID, "", $ParTermReportColumnID);
			
			if(count($TermReportColumnInfo) > 0){
			    for($i=0 ; $i<sizeof($TermReportColumnInfo) ; $i++){
				    $TermReportColumnID = $TermReportColumnInfo[$i]['ReportColumnID'];
				    
				    if ($ParTermReportColumnID != '' && $TermReportColumnID != $ParTermReportColumnID) {
				    	continue;
				    }
				    
				    if(count($CmpSubjectIDArr) == 0){  # Case 1
						$MarksheetScoreInfoArr[$TermReportColumnID] = $this->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $TermReportColumnID);
					} 
					else {  # Case 2
						if($CalculationType == 2){	
							# Down-Right Method - Get & Manipulate Data in First Step (Down)
							$ParentSubMarksheetScoreInfoArr[$TermReportColumnID] = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $CmpSubjectIDArr, $TermReportID, $TermReportColumnInfo[$i]['ReportColumnID']);
						} 
						else {				
							$TotalCmpSubjectWeight = 0;
							# Right-Down Method - Get & prepare Data for the First Step (Right)
							for($j=0 ; $j<sizeof($CmpSubjectIDArr) ; $j++){
								$CmpSubjectID = $CmpSubjectIDArr[$j];
								
								# Get Marksheet Score
								$ParentSubMarksheetScoreInfoArr[$CmpSubjectID][$TermReportColumnID] = $this->GET_MARKSHEET_SCORE($StudentIDArr, $CmpSubjectID, $TermReportColumnID);
								
								# Get Weight among Component Subjects
								$OtherCondition = "SubjectID = $CmpSubjectID AND ReportColumnID IS NULL ";
								$CmpSubjectWeight[$CmpSubjectID] = $this->returnReportTemplateSubjectWeightData($TermReportID, $OtherCondition);
								$TotalCmpSubjectWeight += $CmpSubjectWeight[$CmpSubjectID][0]['Weight'];
							}
						}
					}					
				}
			}
			
			if(!empty($StudentIDArr)){
				foreach($StudentIDArr as $StudentID){
					$notCompletedFlag = 0;
					$weightedMark = '';
					
					# Check for Combination of Special Case of Input Mark
			        $SpecialCaseArr = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
			        foreach($SpecialCaseArr as $key){
			        	$SpecialCaseCountArr[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		        	}
			        	
					# Case 1: Common Subject without any Component Subjects
					# Case 2: Subject having Component Subjects & Calculation Type is 2 (Down-Right Method)
					if(count($CmpSubjectIDArr) == 0 || (!empty($CmpSubjectIDArr) && $CalculationType == 2) ){
						# Prepare for getting valid total weight
						if($CurrentReportID != "" && $CurrentReportInfo['Semester'] == "F"){					
							$AllColumnMarkArr = array();
							for($i=0 ; $i<sizeof($TermReportColumnInfo) ; $i++){
								list($AssessmentColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $TermReportColumnInfo[$i];
								if($MarksheetScoreInfoArr[$AssessmentColumnID][$StudentID]['MarkType'] == "M")
									$AllColumnMarkArr[$AssessmentColumnID] = $MarksheetScoreInfoArr[$AssessmentColumnID][$StudentID]['MarkRaw'];
								else 
									$AllColumnMarkArr[$AssessmentColumnID] = $MarksheetScoreInfoArr[$AssessmentColumnID][$StudentID]['MarkNonNum'];
							}
						}
						
			        	# Loop Report Column
						for($i=0 ; $i<sizeof($TermReportColumnInfo) ; $i++){
							$isCount = 1;
							
							# AssessmentColumnID --> ReportColumnID in the view of Term Report
							list($AssessmentColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $TermReportColumnInfo[$i];
							
							# Case 2: Down-Right Method - Manipulate Data in Second Step (Right) 
							if(count($ParentSubMarksheetScoreInfoArr) > 0){		// Down-Right Method - Manipulate Data in second Step (Right)
								$tmpRaw = $ParentSubMarksheetScoreInfoArr[$AssessmentColumnID][$StudentID];
									    
							    if(!strcmp($tmpRaw, 0) || ($tmpRaw != "" && $tmpRaw != "N.A.") ){
									// [2017-0626-1124-47206] Consolidate Report Calculation : Vertical > Horizontal
									if($ApplySubjectHorizontalCalculation) {
							    		$tmpVal = $this->CONVERT_MARK_TO_WEIGHTED_MARK($TermReportID, $AssessmentColumnID, $SubjectID, $tmpRaw, 2, array(), true);
							    		$weightedMark += $tmpVal;
									}
									// Normal
									else {
									    // Since it is the 2nd Step, CalculationType = 1
							    		$tmpVal = $this->CONVERT_MARK_TO_WEIGHTED_MARK($TermReportID, $AssessmentColumnID, $SubjectID, $tmpRaw);
							    		$weightedMark += $tmpVal;
									}
					    		}
					    		else if($tmpRaw == ""){
						    		$Value = "";
						    		$notCompletedFlag = 1;
					    		}
							}
							else {
								# Case 1								
								if(count($MarksheetScoreInfoArr[$AssessmentColumnID]) > 0){
									$MarkType = $MarksheetScoreInfoArr[$AssessmentColumnID][$StudentID]['MarkType'];
									$MarkNonNum = $MarksheetScoreInfoArr[$AssessmentColumnID][$StudentID]['MarkNonNum'];
									
									if($MarkType == "M"){		// term weighted mark
										$Value = $MarksheetScoreInfoArr[$AssessmentColumnID][$StudentID]['MarkRaw'];
										
										if($Value == "" || $Value == -1){
											$notCompletedFlag = 1;
											if($notCompletedFlag) continue;
										}
											
										// Since it is the 2nd Step, CalculationType = 1
										if($excludedTermReportColumnWeight) {
											$weightedMark += $Value;
										}
										else {
											if($CurrentReportID != "" && $CurrentReportInfo['Semester'] == "F"){
												// [2017-0626-1124-47206] Consolidate Report Calculation : Vertical > Horizontal
												if($ApplySubjectHorizontalCalculation) {
										    		$tmpMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($TermReportID, $AssessmentColumnID, $SubjectID, $Value, 2, $AllColumnMarkArr, true);
												}
												// Normal
												else {
													$tmpMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($TermReportID, $AssessmentColumnID, $SubjectID, $Value, 1, $AllColumnMarkArr);
												}
											}
											else {
												$tmpMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($TermReportID, $AssessmentColumnID, $SubjectID, $Value);
											}
											$weightedMark += $tmpMark;
										}
									}
									else if($MarkType == "SC"){	// Special case
										$SpecialCaseCountArr = $this->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCountArr, $MarkNonNum, $absentExemptSettings);
										$Value = $SpecialCaseCountArr[$MarkNonNum]['Value'];
										$isCount = $SpecialCaseCountArr[$MarkNonNum]['isCount'];
										
										if($isCount){
											$tmpMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($TermReportID, $AssessmentColumnID, $SubjectID, $Value);
											$weightedMark += $tmpMark;
										}
									}
								}
								else {
									$notCompletedFlag = 1;
								}
							}
						}
						
						$setSpecialCase = 0;
						$setSpecialCaseValue = '';
			      		foreach($SpecialCaseCountArr as $key1 => $scData1){
				      		if($scData1['Count'] == count($TermReportColumnInfo)){
				      			$setSpecialCase = 1;
				      			$setSpecialCaseValue = $key1;
			      			}
			      		}
					}
					else {	# Case 3: Subject having Component Subjects & Calculation Type is 1 (Start from Right then Down)
						$tmpWeightedMarkArr	= array();
						for($i=0 ; $i<sizeof($CmpSubjectIDArr) ; $i++){
							$CmpSubjectID = $CmpSubjectIDArr[$i];
							$tmpWeightedMark = '';
							
							# Check for Combination of Special Case of Input Mark Among Component Subjects 
					        $SpecialCaseArr = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
					        foreach($SpecialCaseArr as $key){
					        	$SpecialCaseCountArr[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
				        	}
				        	
				        	# Prepare an Array for converting mark to weighted mark with considering valid weight only
							# AllColumnMarkArr is refer the all Column Marks of each student
				        	$AllColumnMarkArr = array();
				        	for($j=0 ; $j<sizeof($TermReportColumnInfo) ; $j++){
					        	$AssessmentColumnID = $TermReportColumnInfo[$j]['ReportColumnID'];
					        	
					        	if($ParentSubMarksheetScoreInfoArr[$CmpSubjectID][$AssessmentColumnID][$StudentID]['MarkType'] == "SC")
					    			$AllColumnMarkArr[$AssessmentColumnID] = $ParentSubMarksheetScoreInfoArr[$CmpSubjectID][$AssessmentColumnID][$StudentID]['MarkNonNum'];
					    		else 
									$AllColumnMarkArr[$AssessmentColumnID] = $ParentSubMarksheetScoreInfoArr[$CmpSubjectID][$AssessmentColumnID][$StudentID]['MarkRaw'];
				        	}
				        	
							# Right-Down Method - Manipulate Data in First Step (Right)
							for($j=0 ; $j<sizeof($TermReportColumnInfo) ; $j++){
								$isCount = 1;
								list($AssessmentColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $TermReportColumnInfo[$j];
								
								if(count($ParentSubMarksheetScoreInfoArr) > 0){
									# Get Marksheet Score Details
									$MarkType = $ParentSubMarksheetScoreInfoArr[$CmpSubjectID][$AssessmentColumnID][$StudentID]['MarkType'];
									$MarkNonNum = $ParentSubMarksheetScoreInfoArr[$CmpSubjectID][$AssessmentColumnID][$StudentID]['MarkNonNum'];
										    
								    if($MarkType == "M"){	// term weighted mark
										$Value = $ParentSubMarksheetScoreInfoArr[$CmpSubjectID][$AssessmentColumnID][$StudentID]['MarkRaw'];
										
										if($Value == "" || $Value < 0)
											$notCompletedFlag = 1;
										
										if(!$notCompleteFlag){
											$tmpMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($TermReportID, $AssessmentColumnID, $CmpSubjectID, $Value, 1, $AllColumnMarkArr);
											$tmpWeightedMark += $tmpMark;
										}
									}
									else if($MarkType == "SC"){	// special case
										$SpecialCaseCountArr = $this->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCountArr, $MarkNonNum, $absentExemptSettings);
										$scMark = $SpecialCaseCountArr[$MarkNonNum]['Value'];
										$Count = $SpecialCaseCountArr[$MarkNonNum]['Count'];
										$isCount = $SpecialCaseCountArr[$MarkNonNum]['isCount'];
											
										if($isCount) $tmpWeightedMark += $scMark;	// scMark : SpecialCaseMark
									}
								}
								else {
									$notCompletedFlag = 1;
								}
							}
							
							# Checking for any combination of special cases without any Mark after Step 1 (Right)
							$setSpecialCase = 0;
							foreach($SpecialCaseCountArr as $key => $scData){	// scData : SpecialCaseData
								if($scData['Count'] == count($TermReportColumnInfo)){
									$setSpecialCase = 1;
									$setSpecialCaseValue = $key;
								}
							}
							
							# Store the Data into an Array for preparation of Step 2 Down
							if($notCompletedFlag)
								$tmpWeightedMarkArr[$CmpSubjectID]['Mark'] = "";
							else if($setSpecialCase)
								$tmpWeightedMarkArr[$CmpSubjectID]['Mark'] = $setSpecialCaseValue;
							else 
								$tmpWeightedMarkArr[$CmpSubjectID]['Mark'] = $tmpWeightedMark;	
								
							$tmpWeightedMarkArr[$CmpSubjectID]['setSpecialCase'] = $setSpecialCase;
							$tmpWeightedMarkArr[$CmpSubjectID]['setSpecialCaseValue'] = ($setSpecialCase) ? $setSpecialCaseValue : "";					
							
							# Right-Down Method - Manipulate Data in Seoncd Step (Down)
							//$weightedMark += ($TotalCmpSubjectWeight > 0) ? ($tmpWeightedMark * $CmpSubjectWeight[$CmpSubjectID][0]['Weight']) / $TotalCmpSubjectWeight : "";
						}
						# End of Step 1 (Right)
						
						# Start of Step 2 (Down)
						foreach($SpecialCaseArr as $key){
				        	$SpecialCaseCount2Arr[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
			        	}
			        	
						# Get Mark of Parent Subject For Step 2 (Down)
						for($i=0 ; $i<sizeof($CmpSubjectIDArr) ; $i++){
							# Get Total Component Subject Full Mark with excluding unnecessary full mark
							$TotalCmpFullMark = 0;
							$isCount = 1;
							for($j=0 ; $j<sizeof($CmpSubjectIDArr) ; $j++){
								$isCountWeight = 1;
								# Control the output of mark of each student
								if($tmpWeightedMarkArr[$CmpSubjectIDArr[$j]]['setSpecialCase']){
									$isCountWeight = $this->IS_COUNT_WEIGHT_OF_SPECIALCASE($tmpWeightedMarkArr[$CmpSubjectIDArr[$j]]['setSpecialCaseValue'], $absentExemptSettings);
								}
								# Get Total Full Mark of All Component Subjects
								$CmpFullMark = $this->GET_SUBJECT_FULL_MARK($CmpSubjectIDArr[$j], $ClassLevelID, $TermReportID);
								$tmpCmpFullMark = ($CmpFullMark != "") ? $CmpFullMark * $CmpSubjectWeight[$CmpSubjectIDArr[$j]][0]['Weight'] : 0;
								$TotalCmpFullMark += ($isCountWeight) ? (($tmpCmpFullMark != "") ? $tmpCmpFullMark : 0) : 0;
							}
							
							#Get Full Mark Factor for calculating the mark of Parent Subject 
							$Factor = $this->GET_FULL_MARK_FACTOR($ParentFullMark, $TotalCmpFullMark);
							
							$tmpScType = $tmpWeightedMarkArr[$CmpSubjectIDArr[$i]]['setSpecialCase'];
							$tmpScNonNum = $tmpWeightedMarkArr[$CmpSubjectIDArr[$i]]['setSpecialCaseValue'];
							if($tmpScType){
								$SpecialCaseCount2Arr = $this->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCount2Arr, $tmpScNonNum, $absentExemptSettings);
								$Value = $SpecialCaseCount2Arr[$tmpScNonNum]['Value'];
								$isCount = $SpecialCaseCount2Arr[$tmpScNonNum]['isCount'];
								$Count = $SpecialCaseCount2Arr[$tmpScNonNum]['Count'];
							}
							
							# Right-Down Method - Manipulate Data in Seoncd Step (Down)
							if($isCount && $Factor != "" && $Factor > 0){
								$weightedMark += $tmpWeightedMarkArr[$CmpSubjectIDArr[$i]]['Mark'] * $CmpSubjectWeight[$CmpSubjectIDArr[$i]][0]['Weight'] * $Factor;
								
							} else {
								$weightedMark += 0;	
							}
							# End of Step 2 (Down)
						}
						
						# Checking for any combination of special cases without any Mark after Step 1 (Right)
						$setSpecialCase = 0;
						$setSpecialCaseValue = '';
						foreach($SpecialCaseCount2Arr as $key2 => $scData2){	// scData : SpecialCaseData
							if($scData2['Count'] == count($CmpSubjectIDArr)){
								$setSpecialCase = 1;
								$setSpecialCaseValue = $key2;
							}
						}
					}
					
					# Control Display Value
					if($notCompleteFlag){
						$returnArr[$StudentID] = "NOTCOMPLETED";
					} else {
						if($setSpecialCase){
							$returnArr[$StudentID] = $setSpecialCaseValue;
						} else {
							$returnArr[$StudentID] = $weightedMark;
						}
					}
				}
			}
		}
		
		return $returnArr;
	}
	
	
	/*
	* Convert Mark to Corresponding Decimal places by providing Mark(Value) and Number of decimal places
	*/
	function CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($Mark, $NoOfDecimal){
		$returnVal = '';
		
		if($Mark != "" && $NoOfDecimal > 0){
			$len = strlen($Mark);
			$pos = strpos($Mark, ".");
			if(!$pos){	// Input Value is integer
				$returnVal = str_pad($Mark.".", $len+1+$NoOfDecimal, "0", STR_PAD_RIGHT);
			}
			else {	// Input Value is in decimal places
				//if($len - ($pos+1) != $NoOfDecimal){
				$returnVal = str_pad($Mark, $len+($NoOfDecimal-($len-($pos+1))), "0", STR_PAD_RIGHT);
			}
		}
		else {
			$returnVal = $Mark;
		}
		return $returnVal;
	}
	
	
	/*
	* Convert Raw Mark to Weighted Mark of Assessments by 
	* case 1. percentage
	* case 2. ratio (weight) [assessment's weight / total assessment's weights]
	* Full Form Formula: 
	* 						  weight * mark / origninal total weight
	* 						------------------------------------------- (over)
	* 						 new total weight / origninal total weight
	*
	* original total weight: excludes the weight which special case is set to ExFullMark
	*/
	/*
	function CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $Mark, $CalculationType=1, $AllColumnMarkArr=array()){
		$returnVal = '';
		$ReportInfo = array();
		$ReportWeightInfo = array();
		$WeightArr = array();
		$WeightTotal = 0;
		
		# Get Absent&Exempt Settings
		$absentExemptSettings = $this->LOAD_SETTING('Absent&Exempt');
		
		if($ReportID != "" && $ReportColumnID != ""){
			$ReportInfo = $this->returnReportTemplateBasicInfo($ReportID);
			$column_data = $this->returnReportTemplateColumnData($ReportID); 		// Column Data
			
			# Get Report Column Weight
			if($CalculationType == 2){
				if(count($ReportInfo) > 0){
			    	for($i=0 ; $i<sizeof($column_data) ; $i++){
						$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
						$ReportWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
						
						$WeightArr[$ReportWeightInfo[0]['ReportColumnID']] = $ReportWeightInfo[0]['Weight'];
						
						# Check for any weight will not be used to as a Total Weight
						$isCountWeight = 1;
						$isCountFullMark = 1;
				    	if(count($AllColumnMarkArr) > 0 && $AllColumnMarkArr[$column_data[$i]['ReportColumnID']] != ""){
					    	$tmpVal = $AllColumnMarkArr[$column_data[$i]['ReportColumnID']];
					    	
					    	$isSpecialCase = $this->CHECK_MARKSHEET_SPECIAL_CASE_SET1($tmpVal);
					    	if($isSpecialCase){
						    	# Check whether it counts the weight or not if excludes full mark 
						    	$isCountFullMark = $this->IS_COUNT_FULLMARK_OF_SPECIALCASE($tmpVal, $absentExemptSettings);
						    	
						    	# Check whether it counts the weight or not if excludes weight 
						    	$isCountWeight = $this->IS_COUNT_WEIGHT_OF_SPECIALCASE($tmpVal, $absentExemptSettings);
					    	}
				    	}
				    	$WeightTotal += ($isCountWeight == 0 || $isCountFullMark == 0) ? 0 : $ReportWeightInfo[$i]['Weight'];
					}
				}
			}
			else {
				$ReportWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, "SubjectID = ".$SubjectID);
			
				# Subject Weight Array
				if(count($ReportWeightInfo) > 0){
					for($i=0 ; $i<sizeof($ReportWeightInfo) ; $i++){
						$WeightArr[$ReportWeightInfo[$i]['ReportColumnID']] = $ReportWeightInfo[$i]['Weight'];
						
						if($ReportWeightInfo[$i]['ReportColumnID'] != ""){
							# Check for any weight will not be used to as a Total Weight
							$isCountWeight = 1;
							$isCountFullMark = 1;
					    	if(count($AllColumnMarkArr) > 0 && $AllColumnMarkArr[$ReportWeightInfo[$i]['ReportColumnID']] != ""){
						    	$tmpVal = $AllColumnMarkArr[$ReportWeightInfo[$i]['ReportColumnID']];
						    	$isSpecialCase = $this->CHECK_MARKSHEET_SPECIAL_CASE_SET1($tmpVal);
						    	
						    	if($isSpecialCase){
							    	# Check whether it counts the weight or not if excludes full mark 
							    	$isCountFullMark = $this->IS_COUNT_FULLMARK_OF_SPECIALCASE($tmpVal, $absentExemptSettings);
							    	
							    	# Check whether it counts the weight or not if excludes weight 
							    	$isCountWeight = $this->IS_COUNT_WEIGHT_OF_SPECIALCASE($tmpVal, $absentExemptSettings);
						    	}
					    	}
							
					    	$WeightTotal += ($isCountWeight == 0 || $isCountFullMark == 0) ? 0 : $ReportWeightInfo[$i]['Weight'];
						}
					}
				}
			}
			
			if(count($ReportInfo) > 0){
				if($Mark != "" && $Mark >= 0){
					if($ReportInfo['PercentageOnColumnWeight']){	// By Percentage						
						# Formula: Raw Mark(Input Mark) * Percentage / Total Percentage
						$returnVal = ($WeightTotal > 0) ? ($Mark * $WeightArr[$ReportColumnID]) / $WeightTotal : $Mark;
						
					}
					else {	// By Ratio (weight)
						# Formula: Raw Mark (Input Mark) * Weight(Ratio) / Total Weight
						$returnVal = ($WeightTotal > 0) ? ($Mark * $WeightArr[$ReportColumnID]) / $WeightTotal : $Mark;
					}
				}
			}
		}
		
		return $returnVal;
	}
	*/
	
	/*
	# Insert a new record for each subject term report of a class in Marksheet Submission Progress on 1 Apr
	function INSERT_MARKSHEET_SUBMISSION_PROGRESS($ParSubjectID, $ParReportID, $ParClassID, $ParIsCompleted){
		$success = false;
		if($ParSubjectID != "" && $ParReportID != "" && $ParClassID != ""){
			$table = $this->DBName.".RC_MARKSHEET_SUBMISSION_PROGRESS";
			$sql = "
				INSERT INTO $table 
					(TeacherID, SubjectID, ClassID, IsCompleted, 
					 ReportID, DateInput, DateModified) 
				VALUES 
					($this->uid, $ParSubjectID, $ParClassID, $ParIsCompleted, 
					 $ParReportID, NOW(), NOW())
			";
			$success = $this->db_db_query($sql);
		}
		return $success;
	}
	*/
	
	/*
	# Update the Flag [IsCompleted] to state the status of Marksheet in Marksheet Submission Progress  on 25 Mar 
	function UPDATE_MARKSHEET_SUBMISSION_PROGRESS($ParSubjectID, $ParReportID, $ParClassID, $ParIsCompleted){
		$success = false;
		if($ParSubjectID != "" && $ParReportID != "" && $ParClassID != ""){
			$table = $this->DBName.".RC_MARKSHEET_SUBMISSION_PROGRESS";
			$sql = "
				UPDATE 
					$table 
				SET 
					IsCompleted = $ParIsCompleted, 
					DateModified = NOW() 
				WHERE 
					SubjectID = $ParSubjectID AND 
					ReportID = $ParReportID AND 
					ClassID = $ParClassID
			";
			$success = $this->db_db_query($sql);
		}
		return $success;
	}
	*/
	
	/*
	# Check Any Report Template exists for those terms defined in Whole year Report
	function CHECK_REPORT_TEMPLATE_FROM_COLUMN($ReportType, $ClassLevelID){
		$returnArr = array();
		$table1 = $this->DBName.".RC_REPORT_TEMPLATE";
		$sql = "	
			SELECT 
				ReportID 
			FROM 
				$table1 
			WHERE 
				Semester = '$ReportType' AND 
				ClassLevelID = $ClassLevelID 
		";
		$returnArr = $this->returnVector($sql);
		if(count($returnArr) > 0)
			return $returnArr[0];
		else 
			return false;
	}
	*/
	
	# Check Any Report Template exists for those terms defined in Whole year Report
	# $ReportID  	: Current ReportID
	# $ReportType 	: The Report To be found
	# $ClassLevelID : Current Class Level
	# Known Issue   : for non-final Whole Year Report will find the report column of Final Whole Year Report as Valid Column
	function CHECK_WHOLE_YEAR_REPORT_TEMPLATE_FROM_COLUMN($ReportID, $ReportType, $ClassLevelID){
		$returnArr = array();
		$table1 = $this->DBName.".RC_REPORT_TEMPLATE";
		$table2 = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		$sql = "	
			SELECT 
				a.ReportID 
			FROM 
				$table1 AS a
			INNER JOIN $table2 AS b ON
				b.ReportID = a.ReportID AND 
				b.ReportID != '$ReportID'
			WHERE 
				b.SemesterNum = '$ReportType' AND 
				a.ClassLevelID = '$ClassLevelID' AND 
				a.ReportID 
		";
		$returnArr = $this->returnVector($sql);
		if(count($returnArr) > 0)
			return $returnArr[0];
		else 
			return false;
	}
	
	
	/*
	* Get Grade & GradingSchemeRangeID From Grading Scheme Range Table
	* Default to return an Array in format of $returnArr[][] so as to generate a Selection Box
	*/
	/*
	function GET_GRADE_FROM_GRADING_SCHEME_RANGE($SchemeID) {
		$returnArr = array();
		$gradeRangeInfo = array();
		
		$table = $this->DBName.".RC_GRADING_SCHEME_RANGE";
		$sql  = "SELECT GradingSchemeRangeID, Grade FROM $table WHERE SchemeID = '$SchemeID' ORDER BY GradingSchemeRangeID";
		$gradeRangeInfo = $this->returnArray($sql, 2);
		
		if(count($gradeRangeInfo) > 0){
			for($i=0 ; $i<count($gradeRangeInfo) ; $i++)
				$returnArr[$gradeRangeInfo[$i]['GradingSchemeRangeID']] = $gradeRangeInfo[$i]['Grade'];
		}
		return $returnArr;
	}
	*/
	/* ********************* already defined by Andy ********************** */
	
	// But this needs to be updated the code by adding a DELETE function of Report Template Subject Weight
	/*
	function CHANGE_FROM_SUBJECT_GRADING($ClassLevelID, $updatedArr) {
		$existArr = $this->CHECK_FROM_SUBJECT_GRADING($ClassLevelID);
		# Difference of $updatedArr against $existArr
		$diffSubj = array_diff_assoc($updatedArr, $existArr);
		
		# Get default display order of the subjects
		$subjects = array_keys($diffSubj);
		$defaultOrder = $this->GET_SUBJECTS_DISPLAY_ORDER($subjects);
		
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$insertSQL = "INSERT INTO $table VALUES ";
		$result = array();
		foreach ($diffSubj as $key => $value) {
			if ($value == 1) {
				$insertItem[] = "(NULL, $key, $ClassLevelID, ".$defaultOrder[$key].", NULL, NULL, NULL, NOW(), NOW())";
			} else if ($value == 0) {
				$deleteSQL = "DELETE FROM $table WHERE ClassLevelID = $ClassLevelID AND SubjectID = $key";
				$result[] = $this->db_db_query($deleteSQL);
				if(!in_array(false, $result)){
					$result[] = $this->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT_BY_CLASSLEVELID($key, $ClassLevelID);
				}
			}
		}
		if (isset($insertItem) && sizeof($insertItem) > 0) {
			$insertSQL .= implode(",", $insertItem);
			$result[] = $this->db_db_query($insertSQL);
		}
		
		# return false if any error encountered during SQL query
		return !in_array(false, $result);
	}
	*/
	/* ******************************************* */
	
	
	/* ******************************************* */
	// already defined by Yat Woon
	
	/*function returnReportTemplateColumnData($ReportID)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		$sql = "	
			SELECT 
				ReportColumnID,
				ColumnTitle,
				DefaultWeight,
				SemesterNum
			FROM 
				$table 
			WHERE
				ReportID = $ReportID
			ORDER BY
				DisplayOrder
		";
		$x = $this->returnArray($sql);
		return $x;
	}*/
//	function returnReportTemplateColumnData($ReportID, $other="")
//	{
//		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
//		
//		$sql = "	
//			SELECT 
//				ReportColumnID,
//				ColumnTitle,
//				DefaultWeight,
//				SemesterNum,
//				IsDetails,
//				ShowPosition,
//				PositionRange,
//				BlockMarksheet,
//				DisplayOrder		
//			FROM 
//				$table 
//			WHERE
//				ReportID = $ReportID
//		";
//		if($other) $sql .= " and $other ";
//		$sql .="		
//			ORDER BY
//				DisplayOrder
//		";
//		$x = $this->returnArray($sql);
//		return $x;
//	}
	
//	function returnReportTemplateSubjectWeightData($ReportID, $other_condition="")
//	{
//		$table = $this->DBName.".RC_REPORT_TEMPLATE_SUBJECT_WEIGHT";
//		
//		$sql = "	
//			SELECT 
//				ReportColumnID,
//				SubjectID,
//				Weight
//			FROM 
//				$table 
//			WHERE
//				ReportID = $ReportID
//		";
//		if($other_condition)	$sql .= " AND " . $other_condition;
//		$x = $this->returnArray($sql);
//		return $x;
//	}
//	function returnReportColoumnTitle($ReportID)
//	{
//		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
//		
//		$sql = "SELECT ReportColumnID, ColumnTitle, SemesterNum FROM $table where ReportID = '$ReportID' ORDER BY DisplayOrder";
//		$result = $this->returnArray($sql);
//		foreach($result as $key => $val)
//		{
//			list($ReportColumnID, $ColumnTitle, $SemesterNum) = $val;
//			$x[$ReportColumnID] = $ColumnTitle ? $ColumnTitle : $this->GET_ALL_SEMESTERS($SemesterNum);
//		}
//		
//		return $x;
//	}
	/* ******************************************* */
	
	/*
	* Get Subject Name
	*/
	/*
	function GET_SUBJECT_NAME($ParSubjectID)
	{
		global $eclass_db;
		$sql = "SELECT
					a.EN_DES,
					a.CH_DES,
					b.EN_DES,
					b.CH_DES
				FROM
					{$eclass_db}.ASSESSMENT_SUBJECT as a
					LEFT JOIN {$eclass_db}.ASSESSMENT_SUBJECT as b ON a.CODEID = b.CODEID AND b.CMP_CODEID IS NULL
				WHERE
					a.RecordID = '$ParSubjectID'
				";
		$row = $this->returnArray($sql, 4);

		list($SubjectEngName, $SubjectChiName, $ParentSubjectEngName, $ParentSubjectChiName) = $row[0];
		$SubjectName = ($SubjectEngName==$ParentSubjectEngName) ? $SubjectEngName."&nbsp;".$SubjectChiName : $ParentSubjectEngName." - ".$SubjectEngName."&nbsp;".$ParentSubjectChiName." - ".$SubjectChiName;

		return $SubjectName;
	}
	*/
	
	
	// defined in Yat Woon Lib
	/*
	function returnSubjectwOrder($ClassLevelID, $ParForSelection=0)
	{
		global $eclass_db;
		
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		
		$SubjectField = ($ParForSelection==1) ? "CONCAT(a.EN_DES, ' (', a.CH_DES, ')')" : "CONCAT(a.EN_DES, '<br />', a.CH_DES)";
		$CmpSubjectField = ($ParForSelection==1) ? "CONCAT(b.EN_DES, ' (', b.CH_DES, ')')" : "CONCAT(b.EN_DES, '<br />', b.CH_DES)";
		$sql = "SELECT DISTINCT
					a.RecordID,
					$SubjectField,
					b.RecordID,
					$CmpSubjectField
				FROM
					ASSESSMENT_SUBJECT AS a
					LEFT JOIN ASSESSMENT_SUBJECT AS b ON a.CODEID = b.CODEID
					LEFT JOIN $table as c on c.SubjectID=b.RecordID 
				WHERE
					a.EN_SNAME IS NOT NULL
					AND a.CMP_CODEID IS NULL
					and c.ClassLevelID=$ClassLevelID 
				ORDER BY
					a.DisplayOrder,
					b.DisplayOrder
			";
		$SubjectArr = $this->returnArray($sql);	
		
		for($i=0; $i<sizeof($SubjectArr); $i++)
		{
			list($SubjectID, $SubjectName, $CmpSubjectID, $CmpSubjectName) = $SubjectArr[$i];
			
			if($SubjectID==$CmpSubjectID)
			{
				$CmpSubjectID = 0;
			}
			else
			{
				$SubjectName = $CmpSubjectName;
			}
			$ReturnArr[$SubjectID][$CmpSubjectID] = $SubjectName;
			ksort($ReturnArr[$SubjectID]);
		}
		return $ReturnArr;
	}
	*/
	
/*
	function GET_FROM_SUBJECT_GRADING_SCHEME_1($ClassLevelID){
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$sql  = "SELECT SubjectID, SchemeID, DisplayOrder, ScaleInput, ScaleDisplay ";
		$sql .= "FROM $table ";
		$sql .= "WHERE ClassLevelID = $ClassLevelID ";
		$sql .= "ORDER BY DisplayOrder";
		$result = $this->returnArray($sql, 2);
		
		$allSubjects = $this->GET_ALL_SUBJECTS();
		
		$returnArr = array();
		foreach($allSubjects as $subjectID => $data){
			if(is_array($data)){
				$parentSubjectID = $subjectID;
				
				foreach($data as $cmpSubjectID => $subjectName){
					$subjectID = ($cmpSubjectID==0) ? $subjectID : $cmpSubjectID;
					$is_cmpSubject = ($cmpSubjectID==0) ? 0 : 1;
				
					for($i=0 ; $i<count($result) ; $i++){
						if($result[$i]['SubjectID'] == $subjectID){
							$returnArr[$subjectID]['schemeID'] = $result[$i]['SchemeID'];
							$returnArr[$subjectID]['displayOrder'] = $result[$i]['DisplayOrder'];
							$returnArr[$subjectID]['scaleInput'] = $result[$i]['ScaleInput'];
							$returnArr[$subjectID]['scaleDisplay'] = $result[$i]['ScaleDisplay'];
							$returnArr[$subjectID]['is_cmpSubject'] = $is_cmpSubject;
							$returnArr[$subjectID]['subjectName'] = $subjectName;
							$returnArr[$subjectID]['parentSubjectID'] = $parentSubjectID;
							continue;
						}
					}
				}
			}
		}
		return $returnArr;
	}
*/
	// added
	/*
	function GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID){
		global $eclass_db;
		
		$tmpResult = array();
		$result = array();
		
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$subjectTable = $eclass_db.".ASSESSMENT_SUBJECT";
		$sql  = " SELECT a.SubjectID, a.SchemeID, a.DisplayOrder, a.ScaleInput, a.ScaleDisplay ";
		$sql .= " FROM $table as a INNER JOIN $subjectTable as b ON a.SubjectID = b.RecordID ";
		$sql .= " WHERE a.ClassLevelID = $ClassLevelID ";
		$sql .= " ORDER BY a.DisplayOrder";
		$tmpResult = $this->returnArray($sql, 2);
				
		// re-generate the order including parent subject and its subjects in proper format
		if(count($tmpResult) > 0)
			$result = $this->CONSTRUCT_FROM_SUBJECT_GRADING_SCHEME($tmpResult, 1);
		
		$allSubjects = $this->GET_ALL_SUBJECTS();
		
		$returnArr = array();
		$is_cmpSubject = 0;
		$subjectName = '';
		$parentSubjectID = '';
		if(count($result) > 0){
			for($i=0 ; $i<count($result) ; $i++){
				$flag = 0;
				foreach($allSubjects as $subjectID => $data){
					if(is_array($data)){
						$parentSubjectID = $subjectID;
						foreach($data as $cmpSubjectID => $subjectName){
							if($result[$i]['SubjectID'] == $cmpSubjectID || $result[$i]['SubjectID'] == $subjectID){
								$subjectID = ($cmpSubjectID==0) ? $subjectID : $cmpSubjectID;
								$is_cmpSubject = ($cmpSubjectID==0) ? 0 : 1;
								$flag = 1;
								break;
							}
						}
					}
					if($flag)
						break;
				}
				$returnArr[$result[$i]['SubjectID']]['schemeID'] = $result[$i]['SchemeID'];
				$returnArr[$result[$i]['SubjectID']]['displayOrder'] = $result[$i]['DisplayOrder'];
				$returnArr[$result[$i]['SubjectID']]['scaleInput'] = $result[$i]['ScaleInput'];
				$returnArr[$result[$i]['SubjectID']]['scaleDisplay'] = $result[$i]['ScaleDisplay'];
				$returnArr[$result[$i]['SubjectID']]['is_cmpSubject'] = $is_cmpSubject;
				$returnArr[$result[$i]['SubjectID']]['subjectName'] = $subjectName;
				$returnArr[$result[$i]['SubjectID']]['parentSubjectID'] = $parentSubjectID;
			}
		}
		return $returnArr;
	}
	*/
	
	// override - added
	/*
	function GET_GRADING_SCHEME_RANGE_INFO($SchemeID, $Nature="ALL") {
		$returnArr = array();
		$gradeRangeInfo = array();
		
		# Get grade mark Info
		$table = $this->DBName.".RC_GRADING_SCHEME_RANGE";
		$sql  = "SELECT GradingSchemeRangeID, SchemeID, Nature, LowerLimit, UpperLimit, Grade, GradePoint ";
		$sql .= "FROM $table ";
		$sql .= "WHERE SchemeID = $SchemeID ";
		$sql .= "ORDER BY GradingSchemeRangeID";
		$gradeRangeInfo = $this->returnArray($sql, 7);
		
		# Get grade mark Info by Nature
		if($Nature != "ALL"){
			if(count($gradeRangeInfo) > 0){
				for($i=0 ; $i<count($gradeRangeInfo) ; $i++){
					if($gradeRangeInfo[$i]['Nature'] == $Nature)
						$returnArr[] = $gradeRangeInfo[$i];
				}
			}
		}
		else {
			$returnArr = $gradeRangeInfo;
		}
		
		return $returnArr;
	}
	*/
	
	/*
	// new - added
	function GET_GRADING_SCHEME_ID() {
		# Get the main info of the grading scheme
		$table = $this->DBName.".RC_GRADING_SCHEME";
		$sql  = "SELECT SchemeID FROM $table ORDER BY SchemeID";
		
		$result = $this->returnVector($sql);
		return $result;
	}
	*/
	
	// new - added
//	function UPDATE_SUBJECT_FORM_GRADING($subjectInfo){
//		$success = false;
//		$ReportID = trim($subjectInfo["ReportID"])==''?0:$subjectInfo["ReportID"];
//		
//		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
//		$sql  = "UPDATE $table SET  
//					".((isset($subjectInfo["displayOrder"]) && $subjectInfo["displayOrder"] != "") ? "DisplayOrder = ".$subjectInfo["displayOrder"].", " : "")."  
//					".(($subjectInfo["schemeID"] != "") ? "SchemeID = ".$subjectInfo["schemeID"].", " : "SchemeID = NULL, ")." 
//					".((isset($subjectInfo["scaleInput"])) ? "ScaleInput = '".$subjectInfo["scaleInput"]."', " : "")." 
//					".((isset($subjectInfo["scaleDisplay"])) ? "ScaleDisplay = '".$subjectInfo["scaleDisplay"]."', " : "")."
//					".((isset($subjectInfo["LangDisplay"])) ? "LangDisplay = '".$subjectInfo["LangDisplay"]."', " : "")." 
//					".((isset($subjectInfo["DisplaySubjectGroup"])) ? "DisplaySubjectGroup = '".$subjectInfo["DisplaySubjectGroup"]."', " : "")." 
//					DateModified = NOW()
//				WHERE
//					ClassLevelID = ".$subjectInfo["classLevelID"]." AND
//					ReportID = ".$ReportID." AND  
//					SubjectID = ".$subjectInfo["subjectID"];
//							
//		/*$success = false;
//		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
//		$sql  = "UPDATE $table SET  
//					".((isset($subjectInfo["displayOrder"]) && $subjectInfo["displayOrder"] != "") ? "DisplayOrder = ".$subjectInfo["displayOrder"].", " : "")."  
//					".(($subjectInfo["schemeID"] != "") ? "SchemeID = ".$subjectInfo["schemeID"].", " : "SchemeID = NULL, ")." 
//					".((isset($subjectInfo["scaleInput"])) ? "ScaleInput = '".$subjectInfo["scaleInput"]."', " : "")." 
//					".((isset($subjectInfo["scaleDisplay"])) ? "ScaleDisplay = '".$subjectInfo["scaleDisplay"]."', " : "")."
//					".((isset($subjectInfo["LangDisplay"])) ? "LangDisplay = '".$subjectInfo["LangDisplay"]."', " : "")."
//					".((isset($subjectInfo["DisplaySubjectGroup"])) ? "DisplaySubjectGroup = '".$subjectInfo["DisplaySubjectGroup"]."', " : "")." 
//					DateModified = NOW()
//				WHERE
//					ClassLevelID = ".$subjectInfo["classLevelID"]." AND 
//					SubjectID = ".$subjectInfo["subjectID"];*/
//		
//		$success = $this->db_db_query($sql);
//		
//		
//		return $success;
//	}
	
	// override
//	function DELETE_GRADING_SCHEME_AND_RANGE($SchemeID){
//		$table = $this->DBName.".RC_GRADING_SCHEME";
//		$sql = "DELETE FROM $table WHERE SchemeID = $SchemeID";
//		$success[] = $this->db_db_query($sql);
//		
//		$success[] = $this->DELETE_GRADING_SCHEME_RANGE($SchemeID);
//		
//		// update the subject form info if SchemeID no longer exists
//		if(!in_array(false, $success)){
//			$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
//			$sql = "UPDATE $table SET SchemeID = '' WHERE SchemeID = $SchemeID";
//			$success[] = $this->db_db_query($sql);
//		}
//		
//		return in_array(false, $success);
//	}


}

?>