<?php
// using: Pun

#################################################
#
#	Date:	2017-07-10 (Pun)
#			modified ReturnAllPicAry(), added filter AcademicYear
#
#	Date:	2012-03-28 (Henry Chow)
#			modified libinvoice() & GET_MODULE_OBJ_ARR(), added isAllowViewBudgetReport()
#			display "Reports > Budget Report" for "Resources Mgmt Group" leader 
#
#	Date:	2012-03-28 (Henry Chow)
#			added getUserNameByID() & ReturnAllPicAry(), display "PIC filter" in index page  
#
#	Date:	2011-12-15 (Henry Chow)
#			modified getAllAdminGroupOfInvoice(), only return groups which item's recordstatus is active
#
#	Date:	2011-08-26 (Henry Chow)
#			added getAllAdminGroupOfInvoice(), display groups in mgmt > invoice
#
#	Date:	2011-08-04	YatWoon
#			Customization for Fukien Secondary School [Case#2011-0718-0958-20126]
#
#################################################

if (!defined("LIBINVOICE_DEFINED"))                  // Preprocessor directives
{
	define("LIBINVOICE_DEFINED",true);
	define("RECORDSTATUS_ACTIVE",	1);
	define("RECORDSTATUS_DELETED",	-1);
	
	include_once("libinventory.php");      
	class libinvoice extends libinventory 
	{
	
        function libinvoice()
        {
        	global $PATH_WRT_ROOT, $UserID;
	        $this->Module = "InvoiceMgmtSystem";
            $this->libdb();
            $this->filePath = $PATH_WRT_ROOT."file/InvoiceMgmtSysAtt/";
            $this->isViewerGroupMember = $this->isViewerGroupMember($UserID);
            $this->allowViewBudgetReport = $this->isAllowViewBudgetReport($UserID);
        }
        
        function GET_MODULE_OBJ_ARR()
        {
	        global $Lang, $CurrentPage, $i_InventorySystem, $PATH_WRT_ROOT, $_SESSION, $LAYOUT_SKIN;

                # Current Page Information init
                $PageManagement_InvoiceList = 0;
                $PageReports_DeletionLog = 0;
				$PageSettings_GroupBudget = 0;
				$PageSettings_Category = 0;
				$PageSettings_ViewerGroup = 0;
				
                switch ($CurrentPage) 
                {
	                	case "Management_InvoiceList":
	                			$PageManagement = 1;
	                			$PageManagement_InvoiceList = 1;
	                			break;
	                	case "Reports_BudgetReport":
	                			$PageReports = 1;
	                			$PageReports_BudgetReport = 1;
	                			break;
	                	case "Reports_DeletionLog":
	                			$PageReports = 1;
	                			$PageReports_DeletionLog = 1;
	                			break;		
	                	case "Settings_GroupBudget":
	                			$PageSettings = 1;
	                			$PageSettings_GroupBudget = 1;
	                			break;
	                	case "Settings_Category":
	                			$PageSettings = 1;
	                			$PageSettings_Category = 1;
	                			break;	
	                	case "Settings_ViewerGroup":
	                			$PageSettings = 1;
	                			$PageSettings_ViewerGroup = 1;
	                			break;		
	                	
                }

                #### Management
                $MenuArr["Management"] = array($i_InventorySystem['PageManagement'], "", $PageManagement);
                $MenuArr["Management"]["Child"]["View"] = array($Lang['Invoice']['Invoice'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/InvoiceMgmtSystem/management/invoice/index.php?clearCoo=1", $PageManagement_InvoiceList);
                
                if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] || $this->allowViewBudgetReport) {
                #### Reports
                	$MenuArr["Reports"] = array($Lang['Invoice']['Reports'], "", $PageReports);
	               	$MenuArr["Reports"]["Child"]["BudgetReport"] = array($Lang['Invoice']['BudgetReport'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/InvoiceMgmtSystem/reports/budget_report/index.php", $PageReports_BudgetReport);
	               	if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem']) {
	                	$MenuArr["Reports"]["Child"]["DeletionLog"] = array($Lang['Invoice']['DeletionLog'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/InvoiceMgmtSystem/reports/del_log/index.php", $PageReports_DeletionLog);
	               	
                	
	                #### Settings
						$MenuArr["Settings"] = array($i_InventorySystem['Settings'], "", $PageSettings);
						$MenuArr["Settings"]["Child"]["Category"] = array($Lang['Invoice']['Category'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/InvoiceMgmtSystem/settings/category/index.php?clearCoo=1", $PageSettings_Category);
						$MenuArr["Settings"]["Child"]["GroupBudget"] = array($Lang['Invoice']['GroupBudget'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/InvoiceMgmtSystem/settings/group_budget/", $PageSettings_GroupBudget);
						$MenuArr["Settings"]["Child"]["ViewerGroup"] = array($Lang['Invoice']['ViewerGroup'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/InvoiceMgmtSystem/settings/viewer_group/", $PageSettings_ViewerGroup);
	               	}
                }
                
                # module information
                $MODULE_OBJ['title'] = $Lang['Header']['Menu']['InvoiceMgmtSystem'];
                $MODULE_OBJ['title_css'] = "menu_opened";
                $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_eInvoice.png";
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/InvoiceMgmtSystem/index.php";
                $MODULE_OBJ['menu'] = $MenuArr;
                $MODULE_OBJ['TitleBar_Style'] = 2;

                return $MODULE_OBJ;
        }

        function hasAccessRight($level=0)
        {
                global $intranet_inventory_admin, $intranet_inventory_usertype;
                
                if ($_SESSION["inventory"]["role"] != "")
                {
                    return true;
                }
                return false;
        } 
        
        function returnAdminGroupName($AdminGroupID, $withLang=1)
        {
	     	if($AdminGroupID)
	     	{
		     	if($withLang)
		     	{
			     	$sql = "SELECT ".$this->getInventoryNameByLang()." FROM INVENTORY_ADMIN_GROUP where AdminGroupID=$AdminGroupID";
					$result = $this->returnVector($sql);   
					return $result[0];
				}
				else
				{
					$sql = "SELECT NameChi, NameEng FROM INVENTORY_ADMIN_GROUP where AdminGroupID=$AdminGroupID";
					$result = $this->returnArray($sql);   
					return $result;
				}
	     	}
        }
        
        function returnGroupCategoriesBudget($YearID, $AdminGroupID)
        {
	        $sql = "select 
						a.CategoryID, 
						".$this->getInventoryNameByLang('a.').",
						IFNULL(b.Budget, '0.00')
					from 
						INVOICEMGMT_ITEM_CATEGORY as a
						left join INVOICEMGMT_GROUP_BUDGET as b on (b.CategoryID=a.CategoryID and b.AdminGroupID=$AdminGroupID and b.AcademicYearID=$YearID)
	        		";
	        $result = $this->returnArray($sql);   
			return $result;
        }
        
        function Get_Category($categoryId)
        {
        	$sql = "SELECT * FROM INVOICEMGMT_ITEM_CATEGORY WHERE CategoryID='$categoryId'";
        	$result = $this->returnArray($sql);
        	
        	return $result[0];	
        }
        
        function Get_All_Category()
        {
        	$sql = "SELECT CategoryID, NameChi, NameEng, Left(DateModified,10) as DateModified FROM INVOICEMGMT_ITEM_CATEGORY WHERE RecordStatus=".RECORDSTATUS_ACTIVE;	
        	
        	return $this->returnArray($sql);
        }
        
        function New_Category($dataAry=array())
        {
        	global $UserID;
        	
        	if(count($dataAry)==0) return 0;
        	
        	$sql = "SELECT COUNT(*) FROM INVOICEMGMT_ITEM_CATEGORY WHERE NameChi='".$this->Get_Safe_Sql_Query($dataAry['NameChi'])."' OR NameEng='".$this->Get_Safe_Sql_Query($dataAry['NameEng'])."'";
        	$ary = $this->returnVector($sql);
        	
        	if($ary > 0) {	# name duplicated
        		return 0;
        	}
        	
        	$delim = "";
        	foreach($dataAry as $_key=>$_val) {
        		$fields .= $delim.$_key;	
        		$values .= $delim.$_val;
        		$delim = ", ";
        	}
        	
        	$sql = "INSERT INTO INVOICEMGMT_ITEM_CATEGORY ($fields, DateInput, InputBy, DateModified, ModifyBy) VALUES $values, NOW(), '$UserID', NOW(), '$UserID'";
        	$this->db_db_query($sql);
        	
        	return $this->db_insert_id();        	
        }
        
        function GenerateInvoiceNumber($InvoiceDate)
        {
	     	include_once("libgeneralsettings.php");
			$lgs = new libgeneralsettings();
			
			$InvoiceYear = substr($InvoiceDate,0,4);
		 	$InvoiceYearName = "MaxInvNo_".$InvoiceYear;
		 	$data[] = $InvoiceYearName;
		 	$result = $lgs->Get_General_Setting($this->Module,array("'$InvoiceYearName'"));
		 	$ExistsInvoiceNo = $result[$InvoiceYearName];
			$NewInvoiceNo = $ExistsInvoiceNo + 1;
			
			# update/insert to GENERAL_SETTING
			$dataAry = array();
			$dataAry[$InvoiceYearName] = $NewInvoiceNo;
			$lgs->Save_General_Setting($this->Module, $dataAry);
			
			$tempNNNNN = "0000".$NewInvoiceNo;
			$NNNNN = substr($tempNNNNN,strlen($tempNNNNN)-5,5);
			
			return $InvoiceYear.$NNNNN;
        }
        
        function RetrieveInvoiceBasicInfo($RecordID)
        {
	     	$sql = "select
	     				  AcademicYearID, InvoiceDate, Company, InvoiceNo, FundingSource
	     			from
	     				INVOICEMGMT_INVOICE
	     			where 
	     				RecordID=$RecordID";
	     	$result = $this->returnArray($sql,5,1);
	     	return $result;
        }
        
        function RetrieveInvoiceInfo($RecordID)
        {
	     	$sql = "select
	     				  AcademicYearID, InvoiceDate, Company, InvoiceNo, 
	     				  InvoiceDescription, DiscountAmount, TotalAmount, 
	     				  AccountDate, PIC, FundingSource, Remarks
	     			from
	     				INVOICEMGMT_INVOICE
	     			where 
	     				RecordID=$RecordID";
	     	$result = $this->returnArray($sql,11,1);
	     	return $result;
        }
        
        function RetrieveInvoiceItemInfo($ItemID)
        {
	     	$sql = "select
	     				  NameChi, NameEng, Price, Quantity, 
	     				  ResourceMgmtGroup, CategoryID, IsAssetItem
	     			from
	     				INVOICEMGMT_ITEM
	     			where 
	     				ItemID=$ItemID";
	     	$result = $this->returnArray($sql,7,1); 
	     	return $result;
        }
        
        function RetrieveInvoiceAllInfo($RecordID) {
        	$sql = "SELECT * FROM INVOICEMGMT_INVOICE WHERE RecordID='$RecordID' AND RecordStatus=".RECORDSTATUS_ACTIVE;
        	$result = $this->returnArray($sql);
        	return $result[0];	
        }
        
        function getCategoryName($RecordStatus=1)
		{
			$sql = "SELECT 
							DISTINCT CategoryID,
							".$this->getInventoryNameByLang("")." as CategoryName
					FROM
							INVOICEMGMT_ITEM_CATEGORY
					where
						RecordStatus in ($RecordStatus)
					order by 
						NameEng
					";
			return $this->returnArray($sql,2,2);
		}
		
		function getCategoryNameById($CategoryID)
		{
			
			$sql = "SELECT 
							DISTINCT CategoryID,
							NameChi, 
							NameEng
					FROM
							INVOICEMGMT_ITEM_CATEGORY
					WHERE
						CategoryID='$CategoryID'
					";
			$temp = $this->returnArray($sql,3);
			return $temp[0];
		}
		
		function GetCategoryIdByTitle($titleEng="", $titleChi="", $CategoryID="")
		{
			if($titleChi=="" && $titleEng=="") return;
			
			if($titleEng!="") {
				$conds .= " NameEng='".$this->Get_Safe_Sql_Query($titleEng)."'";
				$or = " OR";
			}
			if($titleEng!="") $conds .= $or." NameChi='".$this->Get_Safe_Sql_Query($titleEng)."'";
			if($CategoryID!="") $cond2 = " AND CategoryID!='$CategoryID'";
			
			$sql = "SELECT CategoryID FROM INVOICEMGMT_ITEM_CATEGORY WHERE RecordStatus=".RECORDSTATUS_ACTIVE." ($conds) $cond2";
			$temp = $this->returnVector($sql);
			
			return $temp[0];
		}
		
		function NewCategory($titleEng, $titleChi) 
		{
			global $UserID;
			
			$sql = "INSERT INTO INVOICEMGMT_ITEM_CATEGORY SET NameChi='".$this->Get_Safe_Sql_Query($titleChi)."', NameEng='".$this->Get_Safe_Sql_Query($titleEng)."', DateInput=NOW(), InputBy='$UserID', DateModified=NOW(), ModifyBy='$UserID'";
			return $this->db_db_query($sql);
		}
		
		function DeleteCategory($CategoryIdAry=array())
		{
			global $UserID;
			
			include_once("liblog.php");
			$llog = new liblog();
			
			$NoOfCategory = count($CategoryIdAry);
			
			for($i=0; $i<$NoOfCategory; $i++) {
				# check any item using this category
				$catId = $CategoryIdAry[$i];
				$sql = "SELECT COUNT(*) FROM INVOICEMGMT_ITEM WHERE CategoryID='$catId' AND RecordStatus=".RECORDSTATUS_ACTIVE;
				$count = $this->returnVector($sql);
				
				if($count[0]==0) {
					# soft delete CATEGORY
					$sql = "UPDATE INVOICEMGMT_ITEM_CATEGORY SET RecordStatus=".RECORDSTATUS_DELETED." WHERE CategoryID='$catId'";
					$tempResult = $this->db_db_query($sql);
					
					# add deletion log
					$catInfo = $this->getCategoryNameById($catId);
					list($catId, $catNameChi, $catNameEng) = $catInfo;
					$sql = "SELECT EnglishName FROM INTRANET_USER where UserID='$UserID'";
					$temp = $this->returnVector($sql);
					$detail = "Delete category : $catNameChi/$catNameEng, deleted by (".$temp[0]." ($UserID) ".date('Y-m-d H:i:s').")";
					$llog->INSERT_LOG($this->Module, 'Category', $detail, 'INVOICEMGMT_ITEM_CATEGORY', $catId);
					
					$Result[] = $tempResult? TRUE : FALSE;
				} else {
					$Result[] = FALSE;	
				}
			}
			return $Result;
		}
		
		function EditCategory($titleEng, $titleChi, $CategoryId)
		{
			global $UserID;
			
			$sql = "UPDATE INVOICEMGMT_ITEM_CATEGORY SET NameChi='".$this->Get_Safe_Sql_Query($titleChi)."', NameEng='".$this->Get_Safe_Sql_Query($titleEng)."', DateModified=NOW(), ModifyBy='$UserID' WHERE CategoryID='$CategoryId'";
			return $this->db_db_query($sql);
		}
		
		function Get_Item_By_ItemID($ItemID)
		{
			$sql = "SELECT * FROM INVOICEMGMT_ITEM WHERE ItemID='$ItemID'";
			$result = $this->returnArray($sql);
			return $result[0];
		}
		
		function DeleteInvoice($RecordIdAry=array())
		{
			global $UserID;
			
			$NoOfRecord = count($RecordIdAry);
			
			if($NoOfRecord==0) return; 
			
			include_once("liblog.php");
			$llog = new liblog();
			
			# deleted by 
			$sql = "SELECT EnglishName FROM INTRANET_USER where UserID='$UserID'";
			$temp = $this->returnVector($sql);
			$deletedBy = $temp[0]." ($UserID)";
			$time = date('Y-m-d H:i:s');
			
			for($i=0; $i<$NoOfRecord; $i++) {
				$recId = $RecordIdAry[$i];
				
				# soft delete Invoice
				$sql = "UPDATE INVOICEMGMT_INVOICE SET RecordStatus=".RECORDSTATUS_DELETED." WHERE RecordID='".$recId."'";
				$result[] = $this->db_db_query($sql);
				
				# deletion log of INVOICE
				$recInfo = $this->RetrieveInvoiceBasicInfo($recId);
				
				$detail = "Delete Invoice : ".$recInfo[0]['InvoiceNo']." (".$recInfo[0]['InvoiceDate']."), deleted by (".$deletedBy.") on ".$time;
				$llog->INSERT_LOG($this->Module, 'Invoice', $detail, 'INVOICEMGMT_INVOICE', $recId);
				
				# soft delete Invoice Item (one by one)
				$sql = "SELECT ItemID FROM INVOICEMGMT_ITEM WHERE InvoiceRecordID='$recId' AND RecordStatus=".RECORDSTATUS_ACTIVE;
				$affectedRows = $this->returnVector($sql);
				
				$NoOfItems = count($affectedRows);
				for($a=0; $a<$NoOfItems; $a++) {
					$itemId = $affectedRows[$a];
					/*
					$sql = "UPDATE INVOICEMGMT_ITEM SET RecordStatus=".RECORDSTATUS_DELETED." WHERE InvoiceRecordID='$recId' AND ItemID='$itemId'";
					$this->db_db_query($sql);
					
					# deletion log of ITEM
					$itemInfo = $this->Get_Item_By_ItemID($itemId);
					$detail = "Delete Item : ".$itemInfo['ItemName']." (triggered by Invoice deletion), deleted by (".$deletedBy.") on ".$time;
					$llog->INSERT_LOG($this->Module, 'Item', $detail, 'INVOICEMGMT_ITEM', $itemId);
					*/
					$this->DeleteItem(array($itemId));
				}
				
			}
			
					
			return $result;
		}
		
		function DeleteItem($ItemIdAry=array()) 
		{
			global $UserID;
			
			# deleted by 
			$sql = "SELECT EnglishName FROM INTRANET_USER where UserID='$UserID'";
			$temp = $this->returnVector($sql);
			$deletedBy = $temp[0]." ($UserID)";
			$time = date('Y-m-d H:i:s');
			
			for($i=0; $i<count($ItemIdAry); $i++) {
			
				$itemId = $ItemIdAry[$i];
				
				# update RecordStatus 
				$sql = "UPDATE INVOICEMGMT_ITEM SET RecordStatus=".RECORDSTATUS_DELETED." WHERE ItemID='$itemId'";
				$result[] = $this->db_db_query($sql);
				
				if($result) {
					$itemInfo = $this->Get_Item_Budget_Amount_By_ItemID($itemId);
					list($academic_year_id, $catId, $resource_mgmt_group, $price, $qty) = $itemInfo;
					
					# update budget used
					$sql = "UPDATE INVOICEMGMT_GROUP_BUDGET_USED SET BudgetUsed=BudgetUsed-$price WHERE AcademicYearID='$academic_year_id' AND AdminGroupID='$resource_mgmt_group' AND CategoryID='$catId'";
					$this->db_db_query($sql);
					
					include_once("liblog.php");
					$llog = new liblog();
					
					# deletion log of ITEM
					$itemInfo = $this->Get_Item_By_ItemID($itemId);
					$detail = "Delete Item : ".$itemInfo['NameEng']." (item deletion), deleted by (".$deletedBy.") on ".$time;
					$llog->INSERT_LOG($this->Module, 'Item', $detail, 'INVOICEMGMT_ITEM', $itemId);
				}
			}
			return $result;
		}
		
		function GetPicByUserId($UserIdList="")
		{
			$name_field = getNameFieldByLang("");
			$sql = "SELECT $name_field as name FROM INTRANET_USER WHERE UserID IN ($UserIdList) ORDER BY name";
			return $this->returnVector($sql);	
		}
		
		function returnAdminGroup($thisUserID='', $leader=1)
		{
			if($leader=="") $leader = 1;
			if($leader==1) $conds = " AND b.RecordType=1";
			
			
			$group_namefield = $this->getInventoryNameByLang();
			
			if(!empty($thisUserID))
			{
				$sql = "
					SELECT 
						a.AdminGroupID, 
						$group_namefield as GroupName 
					FROM 
						INVENTORY_ADMIN_GROUP as a 
						inner join INVENTORY_ADMIN_GROUP_MEMBER as b on (b.AdminGroupID=a.AdminGroupID $conds)
					WHERE 
						b.UserID =  $thisUserID
					 ORDER BY 
					 	a.DisplayOrder
					";
					
			}
			else
			{
				$sql = "SELECT AdminGroupID, ".$group_namefield." FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
			}
			return $this->returnArray($sql,'',2);   
			
		}
		
		function INSERT_ITEM_RECORD($dataAry = array())
		{
			global $UserID;
	
			foreach($dataAry as $fields[]=>$values[]);
	
			$sql = "INSERT INTO INVOICEMGMT_ITEM (";
			foreach($fields as $field)	$sql .= $field .", ";
			$sql .= "DateInput, InputBy) values (";
			foreach($values as $value)	$sql .= "\"". $value ."\", ";
			$sql .= "now(), \"$UserID\")";
			$this->db_db_query($sql) or die(mysql_error());
			$new_id = $this->db_insert_id();
			
			## Update Budget used
			$this->UpdateBugetUsed($new_id);
			
			return $new_id;
		}
		
		function MODIFY_ITEM_RECORD($ItemID, $dataAry = array())
		{
			global $UserID;
	
			## retrieve original group, category, price
			$sql = "select 
						a.Price, 
						a.CategoryID, 
						a.ResourceMgmtGroup,
						b.AcademicYearID
					from
						INVOICEMGMT_ITEM as a
						inner join INVOICEMGMT_INVOICE as b on b.RecordID=a.InvoiceRecordID
					where 
						a.ItemID=$ItemID
					";
			$result = $this->returnArray($sql,4,1);
			
			## deduct budget used
			$ori_price = $result[0]['Price'];
			$ori_group = $result[0]['ResourceMgmtGroup'];
			$ori_category = $result[0]['CategoryID'];
			$ori_year = $result[0]['AcademicYearID'];
			$sql = "update INVOICEMGMT_GROUP_BUDGET_USED set 
						BudgetUsed = BudgetUsed - $ori_price,
						DateModified = now()
						where 
						AcademicYearID=$ori_year and 
						AdminGroupID = $ori_group and 
						CategoryID = $ori_category
						";
			$this->db_db_query($sql) or die(mysql_error());
						
			## update item record		
			$sql = "update INVOICEMGMT_ITEM set ";
			foreach($dataAry as $field=>$value)
				$sql .= $field . "=\"". $value."\", ";
			$sql .= "DateModified=now(), ";
			$sql .= "ModifyBy='$UserID' ";
			$sql .= "where ItemID=$ItemID";
			$this->db_db_query($sql);
			
			## Update Budget used
			$this->UpdateBugetUsed($ItemID);
			
		}
		
		function UpdateBugetUsed($ItemID)
		{
			# retrieve item data
			$sql = "select 
						a.Price, 
						a.CategoryID, 
						a.ResourceMgmtGroup,
						b.AcademicYearID
					from
						INVOICEMGMT_ITEM as a
						inner join INVOICEMGMT_INVOICE as b on b.RecordID=a.InvoiceRecordID
					where 
						a.ItemID=$ItemID
					";
			$result = $this->returnArray($sql,4,1);
			
			$Price = $result[0]['Price'];
			$CategoryID = $result[0]['CategoryID'];
			$ResourceMgmtGroup = $result[0]['ResourceMgmtGroup'];
			$AcademicYearID = $result[0]['AcademicYearID'];
			
			$sql = "select RecordID from INVOICEMGMT_GROUP_BUDGET_USED where AdminGroupID=$ResourceMgmtGroup and CategoryID=$CategoryID and AcademicYearID=$AcademicYearID";
			$result = $this->returnVector($sql);
			
			# insert or edit to INVOICEMGMT_GROUP_BUDGET_USED
			if(empty($result))	# insert
			{
				$sql = "insert into INVOICEMGMT_GROUP_BUDGET_USED 
						(AcademicYearID, AdminGroupID, CategoryID, BudgetUsed, DateModified)
						values
						($AcademicYearID, $ResourceMgmtGroup, $CategoryID, '$Price', now())
						";
			}
			else				# update
			{
				$sql = "update INVOICEMGMT_GROUP_BUDGET_USED set 
						BudgetUsed = BudgetUsed + $Price,
						DateModified = now()
						where RecordID=$result[0]";
			}
			
			$this->db_db_query($sql) or die(mysql_error());
		}
		
		
		function INSERT_ITEMID_RELATIONSHIP($InvoiceItemID, $eInventoryItemID)
		{
			$sql = "insert into INVOICEMGMT_ITEMID_RELATIONSHIP	values ($InvoiceItemID, $eInventoryItemID)";
			$this->db_db_query($sql) or die(mysql_error());
		}
		
		function GetAttachmentListByRecordID($RecordID)
		{
			include_once("libfilesystem.php");
			$lfs = new libfilesystem();
			
			$fileList = $lfs->return_folderlist($this->filePath.$RecordID);
			
			return $fileList;
		}
		
		function Display_Invoice_Item_Table($RecordID) 
		{
			global $intranet_session_Language;
			
			$orderBy = ($intranet_session_Language=="en") ? "NameEng" : "NameChi";
			
			$sql = "SELECT * FROM INVOICEMGMT_ITEM WHERE InvoiceRecordID='$RecordID' AND RecordStatus=".RECORDSTATUS_ACTIVE." ORDER BY $orderBy";
			
			return $this->returnArray($sql);
		}	
		
		function Get_Item_Info($ItemId)
		{
			$sql = "SELECT * from INVOICEMGMT_ITEM WHERE ItemID='$ItemId' AND RecordStatus=".RECORDSTATUS_ACTIVE;
			$result = $this->returnArray($sql);
			return $result[0];	
		}
		
		function Get_Item_Budget_Amount_By_ItemID($itemId)
		{
			$sql = "SELECT v.AcademicYearID, i.CategoryID, i.ResourceMgmtGroup, i.Price, i.Quantity FROM INVOICEMGMT_INVOICE v INNER JOIN INVOICEMGMT_ITEM i ON (i.InvoiceRecordID=v.RecordID AND i.ItemID='$itemId')";	
			$result = $this->returnArray($sql);
			return $result[0];
		}
		
		function Check_Allow_View_Invoice_Record($RecordID="")
		{
			global $UserID;
			$groupAry = array();
			
			if($this->isInvoiceCreator($RecordID, $UserID)) return 1;
			
			$group_arr = $this->returnAdminGroup($UserID, $leader=2);	// $leader=2 : return all admin group of current user
			for($i=0; $i<count($group_arr); $i++) {
				$groupAry[] = $group_arr[$i][0];	
			}
		
			$sql = "SELECT COUNT(DISTINCT InvoiceRecordID) FROM INVOICEMGMT_ITEM WHERE InvoiceRecordID='$RecordID' AND RecordStatus=".RECORDSTATUS_ACTIVE." AND ResourceMgmtGroup IN (".implode(',',$groupAry).") GROUP BY InvoiceRecordID";
			$count = $this->returnVector($sql);
			
			return ($count[0]>0) ? 1 : 0;
		}
		
		function isInvoiceCreator($InvoiceID, $userId)
		{
			$sql = "SELECT COUNT(*) FROM INVOICEMGMT_INVOICE WHERE RecordID='$InvoiceID' AND InputBy='$userId'";
			$count = $this->returnVector($sql);
			
			return ($count[0]==0) ? 0 : 1; 	
		}
		
		function memberInViewerGroup() 
		{
			$sql = "SELECT UserID FROM INVOICEMGMT_VIEWER_GROUP_MEMBER";
			return $this->returnVector($sql);	
		}
		
		function addViewerGroupMember($userAry=array())
		{
			if(sizeof($userAry)>0) {
				for($i=0, $i_max=sizeof($userAry); $i<$i_max; $i++) {
					$sql = "SELECT COUNT(*) FROM INVOICEMGMT_VIEWER_GROUP_MEMBER WHERE UserID='".$userAry[$i]."'";
					$count = $this->returnVector($sql);
					if($count[0]==0) {
						$sql = "INSERT INTO INVOICEMGMT_VIEWER_GROUP_MEMBER SET UserID='".$userAry[$i]."', DateInput=NOW()";	
						$result[] = $this->db_db_query($sql);
					}
				}
				return $result;
			}	
		}
		
		function removeViewerGroupMember($userAry=array())
		{
			if(sizeof($userAry)>0) {
				$sql = "DELETE FROM INVOICEMGMT_VIEWER_GROUP_MEMBER WHERE UserID IN (".implode(',', $userAry).")";
				$result[] = $this->db_db_query($sql);
				return $result;	
			}	
		}
		
		function isViewerGroupMember($userid="")
		{
			$sql = "SELECT COUNT(*) FROM INVOICEMGMT_VIEWER_GROUP_MEMBER WHERE UserID='$userid'";
			$count = $this->returnVector($sql);
			
			return $count[0]>0 ? 1 : 0;	
		}
		
		function Load_Budget_Data($YearAry=array(), $GroupAry=array()) 
		{
			$sql = "SELECT 
						b.AcademicYearID, b.AdminGroupID, b.CategoryID, b.Budget, u.BudgetUsed 
					FROM 
						INVOICEMGMT_GROUP_BUDGET b 
						LEFT OUTER JOIN INVOICEMGMT_GROUP_BUDGET_USED u ON (b.AcademicYearID=u.AcademicYearID AND b.AdminGroupID=u.AdminGroupID AND b.CategoryID=u.CategoryID)
					WHERE
						b.AcademicYearID IN (".implode(',',$YearAry).") AND b.AdminGroupID IN (".implode(',',$GroupAry).") 
					GROUP BY 
						b.AcademicYearID, b.AdminGroupID, b.CategoryID";
			$result = $this->returnArray($sql);
			
			$NoOfResult = count($result);
			$dataAry = array();
			for($i=0; $i<$NoOfResult; $i++) {
				list($academic_year_id, $admin_group_id, $category_id, $budget, $used) = $result[$i];
				$dataAry[$academic_year_id][$admin_group_id][$category_id]['Budget'] = ($budget ? $budget : 0);
				$dataAry[$academic_year_id][$admin_group_id][$category_id]['BudgetUsed'] = ($used ? $used : 0);
			}
			
			return $dataAry;	
		}
		
		function Get_Budget_Detail($AcademicYearID='', $GroupID='', $CategoryID='')
		{
			global $intranet_session_Language;
			
			if($CategoryID != "") $cat_conds = " AND u.CategoryID='$CategoryID'";
			$orderBy = ($intranet_session_Language=="en") ? "a.NameEng" : "a.NameChi";
			
			$sql = "SELECT 
						i.InvoiceNo, a.* 
					FROM 
						INVOICEMGMT_ITEM a INNER JOIN 
						INVOICEMGMT_GROUP_BUDGET_USED u ON (u.AdminGroupID=a.ResourceMgmtGroup AND u.CategoryID=a.CategoryID AND a.RecordStatus=".RECORDSTATUS_ACTIVE." $cat_conds AND u.AdminGroupID='$GroupID') INNER JOIN 
						INVOICEMGMT_INVOICE i ON (i.recordStatus=".RECORDSTATUS_ACTIVE." AND i.RecordID=a.InvoiceRecordID AND i.AcademicYearID='$AcademicYearID') INNER JOIN  
						INVOICEMGMT_ITEM_CATEGORY c ON (c.CategoryID=a.CategoryID AND c.RecordStatus=".RECORDSTATUS_ACTIVE.")
					ORDER BY 
						i.InvoiceNo, $orderBy";
			//echo $sql;
			return $this->returnArray($sql);
		}
		
		function getAllAdminGroupOfInvoice($InvoiceRecordID="") 
		{
			global $Lang;
			
			$sql  = "SELECT distinct ".Get_Lang_Selection("g.NameChi", "g.NameEng")." as AdminGroupName FROM INVOICEMGMT_ITEM i INNER JOIN INVENTORY_ADMIN_GROUP g ON (g.AdminGroupID=i.ResourceMgmtGroup) WHERE i.InvoiceRecordID='$InvoiceRecordID' AND i.RecordStatus=".RECORDSTATUS_ACTIVE;
			$data = $this->returnVector($sql);
			//echo $sql.'<br>';
			$returnValue = count($data)==0 ? $Lang['General']['EmptySymbol'] : implode(', ', $data);
			return $returnValue;
		}
		
		function getAllAdminGroupIDOfInvoice($InvoiceRecordID="") 
		{
			global $Lang;
			
			$sql  = "SELECT distinct(g.AdminGroupID) FROM INVOICEMGMT_ITEM i INNER JOIN INVENTORY_ADMIN_GROUP g ON (g.AdminGroupID=i.ResourceMgmtGroup) WHERE i.InvoiceRecordID='$InvoiceRecordID'";
			$data = $this->returnVector($sql);
			return $data;
		}
		
		function CheckOverBudget($YearID, $GroupIDAry)
		{
			global $Lang, $intranet_root;
			/* The lang is stored in lang.cust file */
			
			$CurrentYearID = Get_Current_Academic_Year_ID();
			
			if($YearID == $CurrentYearID)
			{ 
				foreach($GroupIDAry as $k=>$GroupID)
				{
					# Total Used
					$sql = "select sum(BudgetUsed) from INVOICEMGMT_GROUP_BUDGET_USED where AcademicYearID=$CurrentYearID and AdminGroupID=$GroupID";
					$temp = $this->returnVector($sql);
					$TotalUsed = $temp[0];
					
					# year budget
					$sql = "select sum(Budget)from INVOICEMGMT_GROUP_BUDGET where AcademicYearID=$CurrentYearID and AdminGroupID=$GroupID";
					$temp = $this->returnVector($sql);
					$TotalBudget = $temp[0];
					
					if($TotalUsed > $TotalBudget)
					{
						$excess = number_format($TotalUsed - $TotalBudget,2);
						
						# retrieve group name
						$GroupName = $this->returnAdminGroupName($GroupID, 0);
						
						$ContentEn = $Lang['Invoice']['OverBudgetEmail']['Content']['Eng'];
						$ContentEn = str_replace("<!--GroupName//-->", $GroupName[0]['NameEng'], $ContentEn);
						$ContentEn = str_replace("<!--TotalUsed//-->", $TotalUsed, $ContentEn);
						$ContentEn = str_replace("<!--Excess//-->", $excess, $ContentEn);
						
						$ContentChi = $Lang['Invoice']['OverBudgetEmail']['Content']['Chi'];
						$ContentChi = str_replace("<!--GroupName//-->", $GroupName[0]['NameChi'], $ContentChi);
						$ContentChi = str_replace("<!--TotalUsed//-->", $TotalUsed, $ContentChi);
						$ContentChi = str_replace("<!--Budget//-->", $TotalBudget, $ContentChi);
						
						$Subject = $Lang['Invoice']['OverBudgetEmail']['Subject'];
						$Content = $ContentChi . "<br><br><br><br>" . $ContentEn;
						
						include("libwebmail.php");
						$lwebmail = new libwebmail();
						$receiver = $this->GET_ADMIN_USER();
						$exmail_success = $lwebmail->sendModuleMail($receiver, $Subject, $Content, 1);
					}

				}
			}
		}
		
		function retrieveGroupAdminID($GroupID)
		{
			$sql = "select UserID from INVENTORY_ADMIN_GROUP_MEMBER where AdminGroupID=$GroupID and RecordType=1";
			$result = $this->returnVector($sql);
			return $result;
		}
		
		function GET_ADMIN_USER()
		{
			$sql = "SELECT DISTINCT rm.UserID FROM ROLE_RIGHT rr LEFT OUTER JOIN ROLE_MEMBER rm ON (rm.RoleID=rr.RoleID) WHERE FunctionName='eAdmin-eInventory' AND rm.UserID IS NOT NULL";
			$AdminUser = $this->returnVector($sql);
			return $AdminUser;
		}
		
		function ReturnAllPicAry($filterAcademicYearID='')
		{
			$sql = "SELECT DISTINCT PIC FROM INVOICEMGMT_INVOICE WHERE RecordStatus=".RECORDSTATUS_ACTIVE;	
			
			if($filterAcademicYearID){
			    $filterAcademicYearIdSql = implode("','", (array)$filterAcademicYearID);
			    $sql .= " AND AcademicYearID IN ('{$filterAcademicYearIdSql}')";
			}
			
			$picList = $this->returnVector($sql);
			$noOfResult = count($picList);
			
			if($noOfResult==0) {
				return array();	
			} else {
				$PICArray = array();
				
				$PICArrayTemp = array();
				$PICName = array();
				$i = 0;
				foreach($picList as $p) {
					$temp = explode(',', $p);
					if(sizeof($temp)>=1) {
						foreach($temp as $m) {
							if($m!='' && $m!="0" && !isset($PICArrayTemp[$m])) {
								$name = $this->getUserNameByID($m);
								if($name[0] != "") {
									$PICArrayTemp[$m] = $name[0];	# ID
									$PICName[$name[0]] = $m;
									$i++;
								} else {
									$PICArrayTemp[$m] = $m;	# ID
									$PICName[$m] = $m;
								}
							}
						}
					}
				}
				
				$PICArrayTemp = asorti($PICArrayTemp);
				$i = 0;
				foreach($PICArrayTemp as $key=>$val) {
					$PICArray[$i][] = $PICName[$val];
					$PICArray[$i][] = $val;
					$i++;
				}
	
				return $PICArray;
			}
		}
		
		function getUserNameByID($thisUserID)
		{
			$name_field = getNameFieldByLang();
			$sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '$thisUserID'";
			return $this->returnVector($sql);
		}
		
		function isAllowViewBudgetReport($thisUserID)
		{
			# allow "Resource Mgmt Group" leader to view "Budget Report"
			$sql = "SELECT COUNT(g.AdminGroupID) FROM INVENTORY_ADMIN_GROUP g INNER JOIN INVENTORY_ADMIN_GROUP_MEMBER m ON (m.AdminGroupID=g.AdminGroupID AND m.UserID='$thisUserID' AND m.RecordType='1')";	
			$result = $this->returnVector($sql);
			//echo $sql;
			return ($result[0]==0) ? 0 : 1;
		}
		
		
		
	}  
}	        
?>