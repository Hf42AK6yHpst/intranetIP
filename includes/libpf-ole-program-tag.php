<?php

include_once($intranet_root."/includes/libdb.php");
//include_once($intranet_root."/includes/portfolio25/iportfolio.inc.php");

class libpf_ole_program_tag{

  var $tag_id;
  var $chi_title;
  var $eng_title;
  var $record_status;
  var $program_id;
  var $record_id;
  
  # iportfolio20 function
  function GET_OLE_TAG_SELECTION($tag, $programTag, $all="", $ParShowAllCat=false)
	{
		global $ec_iPortfolio, $range_all, $intranet_session_language;

		$TagArray = $this->GET_PROGRAM_TAG_LIST();

		$TagSelection = "<SELECT $tag>";
		$TagSelection .= ($all==1) ? "<OPTION value=''>".$ec_iPortfolio['all_tag']."</OPTION>" : "";
		
		$titlefield = ($intranet_session_language=="en") ? "EngTitle" : "ChiTitle";
		
		if(!empty($TagArray))
		{
			foreach($TagArray as $index => $values)
			{
				$id = $values["TagID"];
        $TagSelection .= "<OPTION value='".$id."' ".($programTag==$id?'SELECTED':"").">".$values[$titlefield]."</OPTION>";
			}
		}
		$TagSelection .= "</SELECT>";

		return $TagSelection;
	}
	
	
  function SET_PROGRAM_TAG_PROPERTY(){
    global $lpf_gDB, $eclass_db;
    
    $t_tag_id = $this->GET_CLASS_VARIABLE("tag_id");
    $t_program_id = $this->GET_CLASS_VARIABLE("program_id");
    $lpf_gDB = new libdb();
    
    if($tag_id == "" && $t_program_id == "") return false;
    
    if($tag_id !="")
    {
      $sql =  "
                SELECT DISTINCT
                  TagID,
                  ChiTitle,
                  EngTitle,
                  RecordStatus
                FROM
                  {$eclass_db}.OLE_PROGRAM_TAG
                WHERE
                  TagID = ".$t_tag_id."
              ";
    }
    else
    {
      $sql = " SELECT DISTINCT
                    a.TagID,
                    a.ChiTitle,
                    a.EngTitle,
                    a.RecordStatus
                  FROM
                    {$eclass_db}.OLE_PROGRAM_TAG as a
                    INNER JOIN {$eclass_db}.OLE_PROGRAM_MAPPING as b ON a.TagID = b.TagID  
                  WHERE 
                     b.ProgramID = ".$t_program_id." 
                     "; 
    }    
    
    $program_tag_arr = $lpf_gDB->returnArray($sql);
    $program_tag_arr = $program_tag_arr[0];

    if(!empty($program_tag_arr))
    {
      $this->SET_CLASS_VARIABLE("tag_id", $program_tag_arr["TagID"]);
      $this->SET_CLASS_VARIABLE("chi_title", $program_tag_arr["ChiTitle"]);
      $this->SET_CLASS_VARIABLE("eng_title", $program_tag_arr["EngTitle"]);
      $this->SET_CLASS_VARIABLE("record_status", $program_tag_arr["RecordStatus"]);
    }
  }
  
  function GET_PROGRAM_TAG_LIST($ParShowInactive=false)
  {
		global $lpf_gDB, $eclass_db;
    
    $lpf_gDB = new libdb();
    
		$sql =  "
              SELECT
                TagID,
                EngTitle,
                ChiTitle,
                RecordStatus
              FROM
                {$eclass_db}.OLE_PROGRAM_TAG
            ";
		if(!$ParShowInactive)
		{
			$sql .= " WHERE RecordStatus = 1 OR RecordStatus = 2";
		}
		$returnArray = $lpf_gDB->returnArray($sql);

		return $returnArray;
  }
  
  function ADD_PROGRAM_MAPPING()
  {
    global $lpf_gDB, $eclass_db;
    
    $lpf_gDB = new libdb();
    
    $t_program_id = $this->GET_CLASS_VARIABLE("program_id");
    $t_tag_id = $this->GET_CLASS_VARIABLE("tag_id");
    
    $fields = "(ProgramID,TagID,InputDate,ModifiedDate)";
    $values = "(".$t_program_id.", ".$t_tag_id.", NOW(), NOW())";
    
    $sql = "INSERT INTO {$eclass_db}.OLE_PROGRAM_MAPPING $fields VALUES $values";
    $lpf_gDB->db_db_query($sql);
    $t_recordID = $lpf_gDB->db_insert_id();
    
    $this->SET_CLASS_VARIABLE("record_id", $t_recordID);

  }
  
  function DELETE_PROGRAM_MAPPING()
  {
    global $lpf_gDB, $eclass_db;
    
    $lpf_gDB = new libdb();
    
    $t_program_id = $this->GET_CLASS_VARIABLE("program_id");
    if($t_program_id == "") return false; 
    
    $sql = "DELETE FROM {$eclass_db}.OLE_PROGRAM_MAPPING 
              WHERE ProgramID = ".$t_program_id." ";
    $returnVal = $lpf_gDB->db_db_query($sql);
    
    return $returnVal;
  }
  
  function SET_CLASS_VARIABLE($ParVariableName, $ParValue)
  {
    $this->{$ParVariableName} = $ParValue;
  }
  
  function GET_CLASS_VARIABLE($ParVariableName)
  {
    return $this->{$ParVariableName};
  }
}

?>