<?php
# using: 

/********************** Change Log ***********************/
#
#   Date    :   2019-05-24 (Bill)
#               modified displayAttachmentEdit(), to apply download_attachment.php
#
#   Date    :   2019-05-14 (Bill)
#               modified retrieveTeachingData(), returnPosterID(), returnClassID(), returnClassName(), returnTeacherName(), returnSubjectName(), importHomework(), 
#                   displayHandinList(), displayIndex(), displayStudentView(), displayCalendarStudent(), displayCalendarClass(), displayWeekViewClass(), displayWeekViewStudent(), 
#                   displaySmallStudentView(), displaySmallTable(), displayDetail(), to prevent SQL Injection
#
#   Date    :   2019-05-07 (Bill)   [2019-0507-0951-59254]
#               modified returnRecord(), to prevent SQL Injection
#
# 	Date	:	2012-02-10 (YatWoon)
#				update libhomework(), missing check with $plugin['eHomework']
#
#	Date	:	2011-06-02 (Henry Chow)
#				modified libhomework(), commented un-use coding
#
#	Date	:	2010-10-28 (YatWoon)
#				update returnRecord(), add CreatedBy field
#
#	Date	:	20010-02-08 (Henry)
#	Detail	:	modifed libhomework(), add "$this->useHomeworkType".
#
#	Date	:	2009-12-15 (Henry)
#	Detail	:	modifed libhomework(), add "$this->useStartDateToGenerateList".
#
/******************* End Of Change Log *******************/

if (!defined("LIBHOMEWORK_DEFINED"))                     // Preprocessor directive
{
define("LIBHOMEWORK_DEFINED", true);


$homework_eclass_showicon = "<img src=\"$image_path/icon_eclass.gif\" alt=\"eClass\">";
class libhomework extends libdb {
        var $HomeworkID;
        var $no_msg;
        var $field;
        var $order;
        var $pageNo;
        var $filter;
        var $page_size;
        var $groupsRetrieved;
        var $groupList;
        var $disabled;
        var $teacherSearchDisabled;
        var $subjectSearchDisabled;
        var $startFixed;
        var $parentAllowed;
        var $nonteachingAllowed;
        var $subjectLeaderAllowed;
        var $exportAllowed;
        var $calData;
        var $pastInputAllowed;
        var $useHomeworkType;
        var $useHomeworkHandin;
		
        var $isTeacher;
        var $teachingData;      # 20031215    Add for checking data of teaching

        var $importSettingType; # added by Kelvin Ho 2008-10-16
        var $file_format;
        var $DeafultHandinRequired;
        var $useStartDateToGenerateList;	# add by Henry on 20091215
        

        function libhomework ()
        {
                global $i_no_record_exists_msg, $plugin;
                global $list_sortby,$list_total,$list_page,$list_prev,$list_next;
                global $sys_custom,$intranet_session_language;
                global $i_general_class,$i_admintitle_subjects,$i_Homework_loading_csv_header,$i_frontpage_campusmail_subject,$i_SmartCard_StudentOutingPIC,$i_Circular_DateStart,$i_Homework_duedate,$i_EventDescription,$i_Homework_HomeworkType,$i_Homework_Handin_Required,$i_Homework_Collect_Required;
                $this->libdb();
                $this->no_msg = $i_no_record_exists_msg;
                $this->groupRetrieved = false;
                $this->groupList = array();
                $this->page_size = 50;
                $this->pageNo = 1;
                $this->sortby = (trim($list_sortby) == "") ? "Sort By" : $list_sortby;
                $this->total = (trim($list_total) == "") ? "Total" : $list_total;
                $this->pagename = (trim($list_page) == "") ? "Page" : $list_page;
                $this->pagePrev = (trim($list_prev) == "") ? "Prev" : $list_prev;
                $this->pageNext = (trim($list_next) == "") ? "Next" : $list_next;

				$this->Module = "eHomework";
				if (!isset($_SESSION["SSV_PRIVILEGE"]["eHomework"]) && $plugin['eHomework'])
				{
					include_once("libgeneralsettings.php");
					$lgeneralsettings = new libgeneralsettings();
					
					$settings_ary = $lgeneralsettings->Get_General_Setting($this->Module);
					if(sizeof($settings_ary)!=0){
						foreach($settings_ary as $key=>$data)
						{
							$_SESSION["SSV_PRIVILEGE"]["homework"][$key] = $data;
							$this->$key = $data; 
						}
					}
				}
				else
				{
					$this->disabled = $_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"];
					$this->teacherSearchDisabled = $_SESSION["SSV_PRIVILEGE"]["homework"]["teacherSearchDisabled"];
					$this->subjectSearchDisabled = $_SESSION["SSV_PRIVILEGE"]["homework"]["subjectSearchDisabled"];
					$this->startFixed = $_SESSION["SSV_PRIVILEGE"]["homework"]["startFixed"];
					$this->parentAllowed = $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"];
					$this->nonteachingAllowed = $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"];
					$this->subjectLeaderAllowed = $_SESSION["SSV_PRIVILEGE"]["homework"]["subjectLeaderAllowed"];
					$this->exportAllowed = $_SESSION["SSV_PRIVILEGE"]["homework"]["exportAllowed"];
					$this->pastInputAllowed = $_SESSION["SSV_PRIVILEGE"]["homework"]["pastInputAllowed"];
					$this->useHomeworkType = $_SESSION["SSV_PRIVILEGE"]["homework"]["useHomeworkType"];
					$this->useHomeworkHandin = $_SESSION["SSV_PRIVILEGE"]["homework"]["useHomeworkHandin"];
					$this->DeafultHandinRequired = $_SESSION["SSV_PRIVILEGE"]["homework"]["DeafultHandinRequired"];
					$this->useHomeworkCollect = $_SESSION["SSV_PRIVILEGE"]["homework"]["useHomeworkCollect"];
					$this->useHomeworkCollect = $_SESSION["SSV_PRIVILEGE"]["homework"]["useHomeworkCollect"];
					$this->useStartDateToGenerateList = $_SESSION["SSV_PRIVILEGE"]["homework"]["useStartDateToGenerateList"];
				 }
					
				// commented by Henry Chow on 2011-06-02 (the tables do not use any more)
				/*	 
                global $UserID;
                if ($UserID != "")
                {
                    $this->retrieveTeachingData();
                }
                else
                {
                    $this->teachingData = "";
                    $this->isTeacher = false;
                }
                */
                
                /* added by Kelvin Ho 2008-10-16
                  for classifying the number of column in csv during import 
                   each setting can be modified in admin console setting->homework list settings
                */
                if ( !$this->useHomeworkType && !$this->useHomeworkHandin && !$this->useHomeworkCollect)
				{
					if($intranet_session_language=='en')
					{
						$this->file_format = array ("Class","Subject","Topic","Loading","Pic","Start Date","Due Date","Description");  
					}
					elseif($intranet_session_language=='b5')
					{
						$this->file_format = array ($i_general_class,$i_admintitle_subjects,$i_frontpage_campusmail_subject,$i_Homework_loading_csv_header,$i_SmartCard_StudentOutingPIC,$i_Circular_DateStart,$i_Homework_duedate,$i_EventDescription);
					}
					$this->importSettingType = 1;
				}
				//homeworktype setting
				elseif ($this->useHomeworkType && !$this->useHomeworkHandin && !$this->useHomeworkCollect)
				{
					if($intranet_session_language=='en')
					{
						$this->file_format = array ("Class","Subject","Topic","Loading","Pic","Start Date","Due Date","Description","Homework Type");
					}
					elseif($intranet_session_language=='b5')
					{
						$this->file_format = array ($i_general_class,$i_admintitle_subjects,$i_frontpage_campusmail_subject,$i_Homework_loading_csv_header,$i_SmartCard_StudentOutingPIC,$i_Circular_DateStart,$i_Homework_duedate,$i_EventDescription,$i_Homework_HomeworkType);
					}
					$this->importSettingType = 2;
				}
				//handin setting
				elseif (!$this->useHomeworkType && $this->useHomeworkHandin && !$this->useHomeworkCollect)
				{
					
					if($intranet_session_language=='en')
					{
						$this->file_format = array ("Class","Subject","Topic","Loading","Pic","Start Date","Due Date","Description","Handin Required");
					}
					elseif($intranet_session_language=='b5')
					{
						$this->file_format = array ($i_general_class,$i_admintitle_subjects,$i_frontpage_campusmail_subject,$i_Homework_loading_csv_header,$i_SmartCard_StudentOutingPIC,$i_Circular_DateStart,$i_Homework_duedate,$i_EventDescription,$i_Homework_Handin_Required);
					}
					$this->importSettingType = 3;
				}
				//collect setting
				elseif (!$this->useHomeworkType && !$this->useHomeworkHandin && $this->useHomeworkCollect)
				{
					if($intranet_session_language=='en')
					{
						$this->file_format = array ("Class","Subject","Topic","Loading","Pic","Start Date","Due Date","Description","Collected by class teacher");
					}
					elseif($intranet_session_language=='b5')
					{
						$this->file_format = array ($i_general_class,$i_admintitle_subjects,$i_frontpage_campusmail_subject,$i_Homework_loading_csv_header,$i_SmartCard_StudentOutingPIC,$i_Circular_DateStart,$i_Homework_duedate,$i_EventDescription,$i_Homework_Collect_Required);
					}
					$this->importSettingType = 4;
				}
				//homeworktype + handin setting
				elseif ($this->useHomeworkType && $this->useHomeworkHandin && !$this->useHomeworkCollect)
				{
					if($intranet_session_language=='en')
					{
						$this->file_format = array ("Class","Subject","Topic","Loading","Pic","Start Date","Due Date","Description","Homework Type","Handin Required");
					}
					elseif($intranet_session_language=='b5')
					{
						$this->file_format = array ($i_general_class,$i_admintitle_subjects,$i_frontpage_campusmail_subject,$i_Homework_loading_csv_header,$i_SmartCard_StudentOutingPIC,$i_Circular_DateStart,$i_Homework_duedate,$i_EventDescription,$i_Homework_HomeworkType,$i_Homework_Handin_Required);
					}
					$this->importSettingType = 5;
				}
				//homeworktype + collect setting
				elseif ($this->useHomeworkType && !$this->useHomeworkHandin && $this->useHomeworkCollect)
				{
					if($intranet_session_language=='en')
					{
						$this->file_format = array ("Class","Subject","Topic","Loading","Pic","Start Date","Due Date","Description","Homework Type","Collected by class teacher");
					}
					elseif($intranet_session_language=='b5')
					{
						$this->file_format = array ($i_general_class,$i_admintitle_subjects,$i_frontpage_campusmail_subject,$i_Homework_loading_csv_header,$i_SmartCard_StudentOutingPIC,$i_Circular_DateStart,$i_Homework_duedate,$i_EventDescription,$i_Homework_HomeworkType,$i_Homework_Collect_Required);
					}
					$this->importSettingType = 6;
				}
				//homeworktype + handin + collect setting
				elseif ($this->useHomeworkType && $this->useHomeworkHandin && $this->useHomeworkCollect)
				{
					if($intranet_session_language=='en')
					{
						$this->file_format = array ("Class","Subject","Topic","Loading","Pic","Start Date","Due Date","Description","Homework Type","Handin Required","Collected by class teacher");  
					}
					elseif($intranet_session_language=='b5')
					{
						$this->file_format = array ($i_general_class,$i_admintitle_subjects,$i_frontpage_campusmail_subject,$i_Homework_loading_csv_header,$i_SmartCard_StudentOutingPIC,$i_Circular_DateStart,$i_Homework_duedate,$i_EventDescription,$i_Homework_HomeworkType,$i_Homework_Handin_Required,$i_Homework_Collect_Required);
					}
					$this->importSettingType = 7;
				}
				  //handin + collect setting
				elseif (!$this->useHomeworkType && $this->useHomeworkHandin && $this->useHomeworkCollect)
				{
					if($intranet_session_language=='en')
					{
						$this->file_format = array ("Class","Subject","Topic","Loading","Pic","Start Date","Due Date","Description","Handin Required","Collected by class teacher");    
					}
					elseif($intranet_session_language=='b5')
					{
						$this->file_format = array ($i_general_class,$i_admintitle_subjects,$i_frontpage_campusmail_subject,$i_Homework_loading_csv_header,$i_SmartCard_StudentOutingPIC,$i_Circular_DateStart,$i_Homework_duedate,$i_EventDescription,$i_Homework_Handin_Required,$i_Homework_Collect_Required);
					}
					$this->importSettingType = 8;
				 }
        }
        function returneClassIcon()
        {
                 global $homework_eclass_showicon;
                 return $homework_eclass_showicon;
        }
        function retrieveTeachingData()
        {
                 global $UserID;
                 $sql = "SELECT Teaching FROM INTRANET_USER WHERE UserID = '$UserID' AND RecordType = 1";
                 $temp = $this->returnVector($sql);
                 if ($temp[0]==1)
                 {
                     $this->isTeacher = true;
                     
                     #$sql = "SELECT b.GroupID, a.SubjectID FROM INTRANET_SUBJECT_TEACHER as a, INTRANET_CLASS as b WHERE a.GroupID = b.GroupID AND a.UserID = '$UserID'";
                     $sql = "SELECT b.GroupID, a.SubjectID FROM INTRANET_SUBJECT_TEACHER as a, INTRANET_CLASS as b WHERE a.ClassID = b.ClassID AND a.UserID = '$UserID'";
                     $this->teachingData = $this->returnArray($sql,2);
                 }
                 else
                 {
                     $this->isTeacher = false;
                 }
        }
        function hasTeachingSubject($ClassID,$SubjectID)
        {
                 global $UserID;
                 if ($UserID=="") return false;
                 if ($this->teachingData == "")
                 {
                     return false;
                 }
                 for ($i=0; $i<sizeof($this->teachingData); $i++)
                 {
                      list($cid, $sid) = $this->teachingData[$i];
                      if ($cid==$ClassID && $sid==$SubjectID)
                      {
                          return true;
                      }
                 }
                 return false;
        }

        /*********  Handin List *******/
        function hasAccessHandinListRight($targetUserID,$homeworkID){
                global $UserID;
                if(!$this->useHomeworkHandin) return false;
                if($targetUserID=="")$targetUserID=$UserID;
                $lu = new libuser($targetUserID);
                $li = new libdb();
                # check teacher
                if ($lu->isTeacherStaff()){
                        $sql = "SELECT HomeworkID, ClassGroupID, SubjectID, PosterUserID FROM INTRANET_HOMEWORK WHERE HomeworkID='$homeworkID' ";
                        $tmp = $li->returnArray($sql,4);
                        list($hid,$classGroupID,$subjectID,$posterID)= $tmp[0];

                        $hasRight = false;
                        # non-teaching staff is allowed to access
                               if($lu->teaching!=1 && $this->nonteachingAllowed){ $hasRight = true; }

                        # the staff is the poster
                        else if($posterID == $targetUserID){ $hasRight = true; }

                        # the staff is teaching that subject
                          else if($this->hasTeachingSubject($classGroupID,$subjectID)) {$hasRight = true; }
                            return $hasRight;
                        }

                        # check student
                        else if($lu->isStudent()){
                                # if the student is the subject leader
                        $sql ="SELECT b.LeaderID FROM INTRANET_HOMEWORK AS a, INTRANET_SUBJECT_LEADER AS b WHERE ";
                        $sql .="a.HomeworkID='$homeworkID' AND b.UserID='$targetUserID' AND a.SubjectID=b.SubjectID AND a.ClassGroupID=b.ClassID";
                        $sql .=" AND a.HandinRequired=1 ";
                        $temp = $li->returnVector($sql,1);
                              if($temp[0]!="") {
                                      return true;
                              }

                              # if the student is the poster
                        $sql = "SELECT HomeworkID, ClassGroupID, SubjectID, PosterUserID FROM INTRANET_HOMEWORK WHERE HomeworkID='$homeworkID' ";
                        $tmp = $li->returnArray($sql,4);
                        list($hid,$classGroupID,$subjectID,$posterID)= $tmp[0];
                                if($posterID == $targetUserID){
                                         return true;
                                }
                    }
                return false;
            }

        # REMOVE records FROM INTRANET_HOMEWORK_HANDIN_LIST
        function removeHandinList($hid,$classID){
                    $removeSql="DELETE FROM INTRANET_HOMEWORK_HANDIN_LIST WHERE HomeworkID='$hid'";
                    $removeConfirm="UPDATE INTRANET_HOMEWORK SET HandinConfirmUserID=NULL, HandinConfirmTime=NULL WHERE HomeworkID='$hid'";
                    $this->db_db_query($removeSql);
                    $this->db_db_query($removeConfirm);
                }

        # INSERT records into INTRANET_HOMEWORK_HANDIN_LIST
        function createHandinList($hid,$classID){
                        $studentList = $this->returnStudentsNeedToHandin($hid,$classID);
                        $insertSql="INSERT INTO INTRANET_HOMEWORK_HANDIN_LIST (HomeworkID,StudentID,RecordStatus,DateInput,DateModified) VALUES ";
                        for($i=0;$i<sizeof($studentList);$i++){
                                        $student_id = $studentList[$i];
                                        $insertSql.="('$hid','$student_id',-1,NOW(),NOW()),";
                        }
                        if(sizeof($studentList)>0){
                                $insertSql = substr($insertSql,0,strrpos($insertSql,","));
                                $this->db_db_query($insertSql);
                        }
                }
                
		function returnStudentsNoNeedToHandin($hid,$classID){
                        $sql = "SELECT StudentID FROM INTRANET_HOMEWORK_HANDIN_LIST WHERE HomeworkID='$hid' AND RecordStatus=2";
                        $result = $this->returnVector($sql);
                        return $result;
                    }
                
		function returnStudentsNotHandin($hid,$classID){
                        $studentsNeedToHandin = $this->returnStudentsNeedToHandin($hid,$classID);
                        $noNeedHandin = $this->returnStudentsNoNeedToHandin($hid,$classID);
                        $notHandinStudents = array();
                        if(is_array($studentsNeedToHandin) && sizeof($studentsNeedToHandin)>0){
                                        $sql = "SELECT StudentID FROM INTRANET_HOMEWORK_HANDIN_LIST WHERE HomeworkID='$hid' AND RecordStatus=1";
                                        $handinStudents = $this->returnVector($sql,1);
                                        //if(sizeof($handinStudents)<=0) return $studentsNeedToHandin;
                                        # get students not handin
                                        $k=0;
                                        for($i=0;$i<sizeof($studentsNeedToHandin);$i++){
                                                if(!in_array($studentsNeedToHandin[$i],$handinStudents) && !in_array($studentsNeedToHandin[$i],$noNeedHandin)){
                                                        $notHandinStudents[$k++] = $studentsNeedToHandin[$i];
                                                }
                                        }
                               }

                               return $notHandinStudents;
                    }

        # return Array of studentID who needs to handin (regardless the handin status)
        function returnStudentsNeedToHandin($hid,$classID=""){
                        $studentList=array();
                            if($hid=="" || $classID=="")return $studentList;


                        $sql = "SELECT a.HomeworkID FROM INTRANET_HOMEWORK_SPECIAL AS a,INTRANET_HOMEWORK AS b WHERE a.HomeworkID='$hid' AND a.HomeworkID=b.HomeworkID AND b.HandinRequired=1";
                            $result = $this->returnVector($sql,1);

                        $groupID=$classID;
                            # Special Homework
                                if($result[0]!=""){
                                        # get Students who needs to handin
                                    $sql ="SELECT a.UserID FROM INTRANET_USER AS a, INTRANET_USERGROUP AS b WHERE b.GroupID='$groupID' AND a.RecordType=2 AND a.RecordStatus IN(0,1,2) AND a.UserID=b.UserID";
                                    $studentList = $this->returnVector($sql,1);

                        }
                        # Normal Homework
                        else{
                                $sql = "SELECT a.HomeworkID FROM INTRANET_HOMEWORK AS a WHERE a.HomeworkID='$hid' AND a.HandinRequired=1";
                                    $result = $this->returnVector($sql,1);
                                if($result[0]!=""){
                                        $sql = "SELECT a.UserID FROM INTRANET_USER AS a, INTRANET_CLASS AS b WHERE b.GroupID='$groupID' AND a.RecordType=2 AND a.RecordStatus IN(0,1,2) AND a.ClassName = b.ClassName ";
                                        $studentList = $this->returnVector($sql,1);
                                }

                        }
                        return $studentList;
                }


        #  $uid : any user id
        # given any userID, return true if the user has homework overdue , false otherwise
        function hasHomeworkOverDue($uid){
                        $luser= new libuser($uid);
                        # if Student
                        if($luser->isStudent()){
                        /*            $sql ="SELECT count(DISTINCT c.HomeworkID) FROM
                                 INTRANET_USERGROUP as a ,
                                 INTRANET_HOMEWORK as c LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST as d ON( (c.HomeworkID=d.HomeworkID and d.StudentID='$uid'))
                                 WHERE a.UserID='$uid' AND a.GroupID=c.ClassGroupID and c.HandinRequired=1 AND c.DueDate<CURDATE() AND (d.RecordStatus IS NULL OR d.RecordStatus=-1) ";
                                 $result = $this->returnVector($sql,1);
                                 if($result[0]>0)
                                         return true;
           */
                                                           return $this->countOverDueHomework($uid);
                        }
                        # if Parent
                        else if($luser->isParent()){
                                 # Get children List
                 $parentID = $uid;
                 $sql = "SELECT a.StudentID FROM INTRANET_PARENTRELATION as a, INTRANET_USER as b
                         WHERE a.StudentID = b.UserID AND a.ParentID = $parentID";
                 $children = $this->returnVector($sql,1);
                 for($i=0;$i<sizeof($children);$i++){
                         //if($this->hasHomeworkOverDue($children[$i])) return true;
                         if($this->countOverDueHomework($children[$i])>0) return true;
                     }
                        }
                        return false;
                }

        function countOverDueHomework($studentID){
                                $count = 0;
                            $luser= new libuser($studentID);
                        # if Student
                        if($luser->isStudent()){
                                $sql ="SELECT count(DISTINCT c.HomeworkID) FROM
                                 INTRANET_USERGROUP as a ,
                                 INTRANET_HOMEWORK as c LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST as d ON( (c.HomeworkID=d.HomeworkID and d.StudentID='$studentID'))
                                 WHERE a.UserID='$studentID' AND a.GroupID=c.ClassGroupID and c.HandinRequired=1 AND c.DueDate<CURDATE() AND (d.RecordStatus IS NULL OR d.RecordStatus=-1) ";
                                 $result = $this->returnVector($sql,1);
                                 $count = $result[0];
                                 //echo "<p>$sql</p>";
                        }

                        # if Parent
                    /*    else if($luser->isParent()){
                                 # Get children List
                                                 $parentID = $studentID;
                                                 $sql = "SELECT a.StudentID FROM INTRANET_PARENTRELATION as a, INTRANET_USER as b
                                                         WHERE a.StudentID = b.UserID AND a.ParentID = $parentID";
                                                 $children = $this->returnVector($sql,1);
                                                 for($i=0;$i<sizeof($children);$i++){
                                                 $count+=$this->countOverDueHomework($children[$i]);
                                             }
                        }
                     */
                        return $count;
                    }

        # given any userID, return the HomeworkOverDue Link if he has Homework OverDue
        # $anchorOnly : return full url or anchor only.
        function returnHomeworkOverDueLink($uid,$anchorOnly=""){
                        $libuser = new libuser($uid);
                        $link="<table border=0>";

                        if(!$this->hasHomeworkOverDue($uid))
                                return "";

                        # Parent
                        if($libuser->isParent()){
                                 # Get children List
                                         $parentID = $uid;
                                                  $namefield = getNameFieldWithClassNumberByLang("b.");
                                         $sql = "SELECT a.StudentID, $namefield as StudentName,b.ClassName,b.ClassNumber FROM INTRANET_PARENTRELATION as a, INTRANET_USER as b
                                                 WHERE a.StudentID = b.UserID AND a.ParentID = $parentID ORDER BY b.ClassName,b.ClassNumber ";
                                         $children = $this->returnArray($sql,2);
                                         for($i=0;$i<sizeof($children);$i++){
                                                $child_id = $children[$i][0];
                                                $child_name=$children[$i][1];
                                                $url = $anchorOnly==""?"/home/homework/homework_overdue.php?uid=$child_id#$child_id":"#$child_id";
                                                $countOD = $this->countOverDueHomework($child_id);
                                                if($countOD>0)
                                                        $link .="<tr><td><a class=functionlink href='$url'>$child_name</a> ($countOD OverDue Homework)</td></tr>";
                                         }
                                }
                                # Student
                                else if($libuser->isStudent()){
                                    $namefield = getNameFieldWithClassNumberByLang("b.");
                                    $sql = "SELECT $namefield FROM INTRANET_USER as b WHERE UserID='$uid'";
                                    $result = $this->returnVector($sql,1);
                                    $sName= $result[0];
                                    $countOD = $this->countOverDueHomework($uid);
                                    $url = $anchorOnly==""?"/home/homework/homework_overdue.php?uid=$uid#$uid":"#$uid";
                                    if($countOD>0)
                                            $link .="<tr><td><a class=functionlink href='$url'>$sName</a> ($countOD OverDue Homework)</td></tr>";
                            }
                                $link.="<tr><td><br><br></td></tr></table>";
                                return $link;

                }

        function displayHomeworkOverDueList($uid){
			global $i_ClassName,$i_Homework_title,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_handin_current_status;
			global $i_Homework_handin_not_handin,$i_Homework_handin_no_need_to_handin,$i_Homework_handin_handin,$i_Homework_handin_not_handin;
			global $i_Subject_name,$i_Homework_overdue_list,$image_cancel;
			global $order,$field,$image_path,$referer;

			$displayContent="<a name='t1'>".$this->displayLargeTitleToolBar2("","$i_Homework_overdue_list","","")."</a>";
			$displayContent.=$this->returnHomeworkOverDueLink($uid,"1");

			$lu = new libuser($uid);
			if($lu->isParent()){
					$studentList = $lu->getChildren();
			}else if($lu->isStudent()){
					$studentList = Array($uid);
			}

			$order_str = $order=="1"?" DESC ":" ASC ";
			switch($field){
					case 1 : $field_str = " c.Title "; break;
					case 2 : $field_str = " e.SubjectName "; break;
					case 3 : $field_str = " c.StartDate "; break;
					case 4 : $field_str = " c.DueDate "; break;
					//case 5 : $field_str = " d.RecordStatus "; break;
					default: $field_str = " c.DueDate ";
			}
			$order_img = "<img src='".$image_path."/".($order=="1"?"asc":"desc").".gif' hspace=2 border=0>";
			for($j=0;$j<sizeof($studentList);$j++){
					$student_id = $studentList[$j];
					$namefield = getNameFieldWithClassNumberByLang("f.");
					$sql ="SELECT c.HomeworkID,
											IF(b.Title IS NULL,'&nbsp;',b.Title),
											IF(c.Title IS NULL,'&nbsp;',c.Title),
											IF(e.SubjectName IS NULL,'&nbsp;',e.SubjectName),
											c.HandinRequired,
											IF(c.StartDate IS NULL,'&nbsp;',c.StartDate),
											IF(c.Duedate IS NULL,'&nbsp;',c.DueDate),
											d.RecordStatus,
											$namefield,
											g.TypeName
					FROM
					 INTRANET_USERGROUP as a ,
					 INTRANET_GROUP as b,
					 INTRANET_HOMEWORK as c LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST as d ON( (c.HomeworkID=d.HomeworkID and d.StudentID='$student_id'))
					 LEFT OUTER JOIN INTRANET_SUBJECT as e ON (c.SubjectID = e.SubjectID)
					 LEFT OUTER JOIN INTRANET_USER as f ON (f.UserID='$student_id')
					 LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as g ON (c.TypeID=g.TypeID)
					 WHERE a.UserID='$student_id' AND a.GroupID=b.GroupID and a.GroupID=c.ClassGroupID and c.HandinRequired=1 AND c.DueDate<CURDATE() AND (d.RecordStatus IS NULL OR d.RecordStatus=-1)
					 ORDER BY $field_str $order_str
					 ";
					 $result = $this->returnArray($sql,10);
					 $x="";
					 for($i=0;$i<sizeof($result);$i++){
							 list($hid,$className,$hw_title,$subjectName,$handinRequired,$startDate,$dueDate,$status,$studentName,$typeName) = $result[$i];
							 $strStatus="&nbsp;";

							 switch($status){
									 case -1 : $strStatus = $i_Homework_handin_not_handin; break;
									 case  2 : $strStatus = $i_Homework_handin_no_need_to_handin; break;
									 case  1 : $strStatus = $i_Homework_handin_handin; break;
									 default : $strStatus = $i_Homework_handin_not_handin;
							 }
							 $x.="<Tr><td>".($i+1)."</td>";
							 //$x.="<td>$className</td>";
							 if($this->useHomeworkType && $typeName!="")
									 $hw_title = "[".$typeName."] ".$hw_title;
							 $x.="<td><a href=javascript:fe_view_homework_detail($hid)>$hw_title</a></td>";
							 $x.="<td>$subjectName</td>";
							 $x.="<td>$startDate</td>";
							 $x.="<td>$dueDate</td>";
							 $x.="<td>$strStatus</td>";
							 $x.="</tr>";
					}
					if(sizeof($result)<=0){
							$x.="<tr><td colspan=6 align=center>&nbsp;<br>".$this->no_msg."&nbsp;<br></td></tr>";
					}
					$displayContent .="<br><br><table width=717 border=0 cellspacing=0 cellpadding=1 background=/images/homeworklist/notebooktbbg.gif class=h1>\n";
					$displayContent .= "<tr><td></td><td colspan=2><a name=$student_id><b>$studentName</b></a> <a class=functionlink href='#t1'>Top</a><br>&nbsp;</td></tr>";
						$displayContent.="<tr><td colspan=3 valign=top><table width=99% border=1 cellspacing=0 cellpadding=4 bordercolordark=#EFFFF7 bordercolorlight=#D0DBC4>\n";
						$displayContent .= "<tr>";
						$displayContent .= "<td width=1% class=homework_tableTitle><b>#</b></td>\n";
						/*$displayContent .= "<td width=20%><b>$i_ClassName</b></td>\n";*/
                                    $displayContent .= "<td width=35%><b><a href=javascript:orderBy(1,'$student_id') onMouseOver=\"window.status='"."Sort by"." $i_Homework_title';return true;\" onMouseOut=\"window.status='';return true;\">$i_Homework_title".($field==1?$order_img:"")."</a></b></td>\n";
                                    $displayContent .= "<td width=20%><b><a href=javascript:orderBy(2,'$student_id') onMouseOver=\"window.status='"."Sort by"." $i_Subject_name';return true;\" onMouseOut=\"window.status='';return true;\">$i_Subject_name".($field==2?$order_img:"")."</a></b></td>\n";
                                    $displayContent .= "<td width=15%><b><a href=javascript:orderBy(3,'$student_id') onMouseOver=\"window.status='"."Sort by"." $i_Homework_startdate';return true;\" onMouseOut=\"window.status='';return true;\">$i_Homework_startdate".($field==3?$order_img:"")."</a></b></td>\n";
                                    $displayContent .= "<td width=15%><b><a href=javascript:orderBy(4,'$student_id') onMouseOver=\"window.status='"."Sort by"." $i_Homework_duedate';return true;\" onMouseOut=\"window.status='';return true;\">$i_Homework_duedate".($field==4?$order_img:"")."</a></b></td>\n";
                                    /*$displayContent .= "<td width=15%><b><a href=javascript:orderBy(5,'$student_id') onMouseOver=\"window.status='"."Sort by"." $i_Homework_duedate';return true;\" onMouseOut=\"window.status='';return true;\">$i_Homework_handin_current_status".($field==5?$order_img:"")."</a></b><br>&nbsp;</td>\n";*/
                                                                $displayContent .= "<td width=15%><b>$i_Homework_handin_current_status</b>&nbsp;<br></td>\n";

                                    $displayContent .= "</tr>";
                                    $displayContent .= $x;

                                    $displayContent .="</table>";

                                # cancel button
                                $displayContent.="<tr><td colspan=3><table border=0 width=100% cellspacing=0 cellpadding=0>
                                  <tr><td>
                                                <table border=0 align=center width=100%>
                                                    <tr>
                                                    <td align=center valign=top><br><BR>
                                                           <a href='$referer'><img src='$image_cancel' border=0></a>
                                                                </td>
                                                        </tr>
                                                </table>
                                  </td></tr>
                                  </table></td></tr>";
                                #end cancel button

                                $displayContent .="</table>";
                        }
                        return $displayContent;
                }
                
                function displayHandinList($hid)
                {
                                global $i_Homework_title,$i_ClassName,$i_LastModified,$image_submit,$image_reset,$image_cancel,$i_ClassNameNumber,$i_UserStudentName;
                                global $i_Homework_handin_current_status,$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record,$i_Homework_handin_list;
                                global $order_field,$order,$i_Homework_handin_change_status;
                                global $image_path,$i_Homework_handin_not_handin,$i_Homework_handin_handin,$i_Homework_handin_no_need_to_handin;
                                global $i_Homework_handin_set_NotHandin_to_NoNeedHandin,$i_Homework_handin_set_All_to_Handin,$i_Homework_handin_set_All_to_NotHandin;

                                $order_img="<img src='".$image_path."/".($order==0?"desc":"asc").".gif' hspace=2 border=0>";
                                $order_str="";

                                $hid = IntegerSafe($hid);
                                $sql = "SELECT ClassGroupID FROM INTRANET_HOMEWORK_SPECIAL WHERE HomeworkID = '$hid'";
                                $r = $this->returnVector($sql,1);
                                $classGroupID = $r[0];

                                // $order_str = $order=="0"?" ASC ": " DESC ";  NO SORTING AVAILABLE
                                # NO SORTING
                                $order_str="ASC";
                                $order_field=1;

                                # IF SPECIAL CLASS HOMEWORK
                                if($classGroupID!=""){

                                        $name_field = getNameFieldByLang("B.");
                                        $sql = "SELECT a.ClassGroupID,b.Title FROM INTRANET_HOMEWORK AS a, INTRANET_GROUP AS b WHERE HomeworkID = '$hid' AND a.ClassGroupID=b.GroupID";
                                        $r = $this->returnArray($sql,3);
                                        $classGroupID = $r[0][0];
                                        $classGroupTitle = $r[0][1];

                                        # select from INTRANET_GROUP
                                        $sql = "SELECT B.UserID, $name_field AS StudentName, B.ClassName, B.ClassNumber,C.RecordStatus, C.RecordID, E.HandinConfirmUserID, E.HandinConfirmTime,E.Title,F.TypeName FROM INTRANET_USERGROUP AS A, INTRANET_USER AS B LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS C ON(C.HomeworkID = '$hid' AND C.StudentID = A.UserID), INTRANET_GROUP AS D,INTRANET_HOMEWORK AS E LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE AS F ON(E.TypeID=F.TypeID)
                                                WHERE A.GroupID = '$classGroupID' AND A.UserID = B.UserID AND B.RecordType=2 And B.RecordStatus IN(0,1,2) AND D.GroupID=E.ClassGroupID AND E.HomeworkID = '$hid' AND E.HandinRequired=1";
                                        
                                        switch($order_field){
                                                case 1 : $sort_str = " ORDER BY B.ClassName $order_str,B.ClassNumber ";break;
                                                case 2 : $sort_str = " ORDER BY StudentName $order_str"; break;
                                                case 3 : $sort_str = " ORDER BY C.RecordStatus $order_str"; break;
                                                default: $sort_str = " ORDER BY B.ClassName $order_str,B.ClassNumber ";
                                        }
                                        //$sort_str.=$order_str;
                                        $sql.=$sort_str;
                                        $result = $this->returnArray($sql,10);

                                }
                                # IF NOT SPECIAL CLASS HOMEWORK
                                else{

                                        $name_field = getNameFieldByLang("A.");

                                        # select from INTRANET_CLASS
                                        $sql = "SELECT A.UserID, $name_field AS StudentName, A.ClassName, A.ClassNumber,E.RecordStatus, E.RecordID, D.HandinConfirmUserID, D.HandinConfirmTime,D.Title,F.TypeName FROM INTRANET_USER AS A, INTRANET_CLASS AS B,INTRANET_HOMEWORK AS D LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS E ON( E.HomeworkID = '$hid' AND A.UserID=E.StudentID) LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE AS F ON (D.TypeID=F.TypeID)
                                        WHERE D.HomeworkID = '$hid' AND B.GroupID=D.ClassGroupID AND A.ClassName=B.ClassName AND A.RecordType=2 AND A.RecordStatus IN (0,1,2) AND D.HandinRequired=1";

                                        switch($order_field){
                                                case 1 : $sort_str = " ORDER BY A.ClassName,A.ClassNumber ";break;
                                                case 2 : $sort_str = " ORDER BY StudentName "; break;
                                                case 3 : $sort_str = " ORDER BY E.RecordStatus "; break;
                                                default: $sort_str = " ORDER BY A.ClassName , A.ClassNumber ";
                                        }

                                        $sort_str.=$order_str;

                                        $sql.=$sort_str;
                                        $result = $this->returnArray($sql,10);
                                        $classGroupTitle = $result[0][2];


                                }
                                $insertSql = "INSERT INTO INTRANET_HOMEWORK_HANDIN_LIST (HomeworkID,StudentID,RecordStatus,DateInput,DateModified) VALUES";
                                $values = "";

                                $displayContent ="";
                                 $displayContent = $this->displayLargeTitleToolBar2("","$i_Homework_handin_list","","");
                                $x="";
                                # build content table
                                if(sizeof($result)<=0){
                                        //$x.=$this->getLineSeparation(5);
                                        $x.="<tr><td colspan=5 align=center>&nbsp;<br>".$this->no_msg."<br>&nbsp;</td></tr>";
                                }
                                for($i=0;$i<sizeof($result);$i++){
                                        list($uid,$studentName,$className,$classNumber,$handinStatus,$handinID,$confirmUserID,$confirmTime,$hw_title,$hw_type) = $result[$i];

                                        $hw_title = ($this->useHomeworkType && $hw_type != "") ? "[".$hw_type."] ".$hw_title : $hw_title;
                                        $str_status="";
                                        $css="";
                                        $handinStatus=$handinStatus==""?-1:$handinStatus;
                                        switch($handinStatus){
                                                case 1 : $str_status="$i_Homework_handin_handin"; $css="homework_handin";break;
                                                case -1: $str_status="$i_Homework_handin_not_handin";$css="homework_no_handin";break;
                                                case 2 : $str_status="$i_Homework_handin_no_need_to_handin"; $css="homework_no_need_handin";break;
                                                default : $str_status="$i_Homework_handin_handin";$css="homework_handin";
                                        }

                                        $select_status="<SELECT name='handin_status[]'>";
                                        $select_status.="<OPTION value='1'".($handinStatus==1?"SELECTED":"").">$i_Homework_handin_handin</OPTION>";
                                        $select_status.="<OPTION value='-1'".($handinStatus==-1?"SELECTED":"").">$i_Homework_handin_not_handin</OPTION>";
                                        $select_status.="<OPTION value='2'".($handinStatus==2?"SELECTED":"").">$i_Homework_handin_no_need_to_handin</OPTION>";
                                        $select_status.="<input type=hidden name='handin_user_id[]' value='$uid'>";
                                        $select_status.="</SELECT>\n";

                                //        $x .=$this->getLineSeparation(5);
                                        $x .="<tr><td class=$css>".($i+1)."</td><td class=$css>$className($classNumber)</td><td class=$css>$studentName</td><td class=$css>$str_status</td><td class=$css>$select_status</td></tr>";
                                        if($handinID==""){
                                                $y ="($hid,$uid,-1,NOW(),NOW()),";
                                                $values .= $y;
                                        }
                                }
                                //$x.=$this->getLineSeparation(5);

                                # actions
                                $action_links="<table width=717 border=0 cellspacing=0 cellpadding=5 background=/images/homeworklist/notebooktbbg.gif><tr><Td align=right>";
                                $action_links.="<a href='javascript:setNotHandinToNoNeedHandin()' class=functionlink>$i_Homework_handin_set_NotHandin_to_NoNeedHandin</a><br><br>";
                                $action_links.="<a href='javascript:setAllToHandin()' class=functionlink>$i_Homework_handin_set_All_to_Handin</a><Br><br>";
                                $action_links.="<a href='javascript:setAllToNotHandin()' class=functionlink>$i_Homework_handin_set_All_to_NotHandin</a><br>";
                                $action_links.="</td><td>&nbsp;</td></tr>";
                                //$action_links.=$this->getLineSeparation(2);
                                $action_links.="</table>";

                                # confirmation detail
                            $confirm_table="<br><br><Table border=0 align=center width=30% cellpadding=3 cellspacing=0>";
                            $confirm_table.="<tr><td align=right><b>$i_Homework_title : </b></td><td><A HREF='javascript:fe_view_homework_detail($hid)'>$hw_title</A></td></tr>".$this->getSmallLineSeparation(2);
                            $confirm_table.="<tr><td align=right><b>$i_ClassName : </b></td><td>$classGroupTitle</td></tr>".$this->getSmallLineSeparation(2);

                            if($confirmUserID!=""){
                                $confirm_user_name_field = getNameFieldByLang();
                                    $temp = $this->returnVector("SELECT $confirm_user_name_field FROM INTRANET_USER WHERE UserID='$confirmUserID'",1);
                                    $confirm_user_name = $temp[0];
                                    $confirm_table.="<tr><td align=right><b>$i_LastModified : </b></td><td>$confirmTime ($confirm_user_name)</td></tr>";
                                }else{
                                        $confirm_table .="<tr><td>&nbsp;</td><td>$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record</td></tr>";
                                }
                            $confirm_table.="</table><br><br>";

                            $displayContent .= $confirm_table;
                            $displayContent .= $action_links;

                            $displayContent .= "<table width=717 border=0 cellspacing=0 cellpadding=5 background=/images/homeworklist/notebooktbbg.gif class=h1>\n";
                            $displayContent .= "<tr><td colspan=3><table width=99% border=1 cellspacing=0 cellpadding=4 bordercolordark=#EFFFF7 bordercolorlight=#D0DBC4>\n";
                            $displayContent .= "<tr>";
                            $displayContent .= "<td width=5% class=homework_tableTitle><b>#</b></td>\n";

                        # DISABLE SORTING
                        /*
                            $displayContent .= "<td width=25%><b><a href='javascript:orderBy(1)' onMouseOver=\"window.status='"."Sort by"." $i_ClassNameNumber';return true;\" onMouseOut=\"window.status='';return true;\">$i_ClassNameNumber".($order_field==1?$order_img:"")."</a></b></td>\n";
                            $displayContent .= "<td width=25%><b><a href='javascript:orderBy(2)' onMouseOver=\"window.status='"."Sort by"." $i_UserStudentName';return true;\" onMouseOut=\"window.status='';return true;\">$i_UserStudentName".($order_field==2?$order_img:"")."</a></b></td>\n";
                            $displayContent .= "<td width=20%><b><a href='javascript:orderBy(3)' onMouseOver=\"window.status='"."Sort by"." $i_Homework_handin_current_status';return true;\" onMouseOut=\"window.status='';return true;\">$i_Homework_handin_current_status".($order_field==3?$order_img:"")."</a></b></td>\n";
                        */

                            $displayContent .= "<td width=25%><b>$i_ClassNameNumber</b></td>\n";
                            $displayContent .= "<td width=25%><b>$i_UserStudentName</b></td>\n";
                            $displayContent .= "<td width=20%><b>$i_Homework_handin_current_status</b></td>\n";

                              $displayContent .= "<td width=25%><b>$i_Homework_handin_change_status</b><br>&nbsp;</td>\n";
                            $displayContent .= "</tr>";
                                $displayContent .= $x;
                                $displayContent .="</table>";
                                $displayContent .="</td></tr></table>";
                                # insert if records not exists
                                if($values!=""){
                                        $values = substr($values,0,strrpos($values,","));
                                        $insertSql .=$values;
                                        $this->db_db_query($insertSql);
                                }
                                return $displayContent;
                }

        function navigation(){
                global $image_path;
                $x  = "<table width=100% border=0 cellpadding=0 cellspacing=0>\n";
                $x .= "<tr>\n";
                $x .= "<td align=right style='vertical-align:middle'>\n";
                $x .= $this->record_range();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->prev_n();
                $x .= "<img src=$image_path/space.gif width=5 height=10 border=0>\n";
                $x .= $this->next_n();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->go_page();
                $x .= "</td>\n";
                $x .= "</tr>\n";
                $x .= "</table>\n";
                return $x;
        }

        function record_range(){
                $n_title = $this->title;
                $n_start = ($this->pageNo-1)*$this->page_size+1;
                $n_end = min($this->db_num_rows(),($this->pageNo*$this->page_size));
                $n_total = $this->db_num_rows();
                $x = "$n_title $n_start - $n_end, ".$this->total." $n_total\n";
                return $x;
        }

        function prev_n(){
                global $image_path,$image_pre;
//                $previous_icon = "<img src=$image_path/previous_icon.gif border=0 hspace=2 vspace=0 align=middle>";
                $previous_icon = "<img src=$image_pre border=0 hspace=2 vspace=0 align=middle>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->db_num_rows()/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==1) ? $previous_icon.$this->pagePrev."\n" : "<a href=javascript:gopage(".($n_page-1).",document.form1) onMouseOver=\"window.status='".$this->pagePrev."';return true;\" onMouseOut=\"window.status='';return true;\">".$previous_icon.$this->pagePrev."</a>\n";
                return $x;
        }

        function next_n(){
                global $image_path,$image_next;
//                $next_icon = "<img src=$image_path/next_icon.gif border=0 hspace=2 vspace=0 align=middle>";
                $next_icon = "<img src=$image_next border=0 hspace=2 vspace=0 align=middle>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->db_num_rows()/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==$n_total) ? $this->pageNext.$next_icon."\n" : "<a href=javascript:gopage(".($n_page+1).",document.form1) onMouseOver=\"window.status='".$this->pageNext."';return true;\" onMouseOut=\"window.status='';return true;\">".$this->pageNext.$next_icon."</a>\n";
                return $x;
        }

        function go_page(){
                $n_page=$this->pageNo;
                $n_total=ceil($this->db_num_rows()/$this->page_size);
                $x = $this->pagename." <select onChange='this.form.pageNo.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
                for($i=1;$i<=$n_total;$i++)
                $x .= ($n_page==$i) ? "<option value=$i selected>$i</option>\n" : "<option value=$i>$i</option>\n";
                $x .= "</select>\n";
                return $x;
        }

        function returnEditDeleteLink ($hid)
        {
                 global $image_path, $intranet_session_language, $button_edit,$button_remove;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 return "<A HREF=/home/homework/edit.php?hid=$hid><img src=\"$hwImagePath/btn_edit.gif\" alt=\"$button_edit\" border=0></A> <a HREF=/home/homework/remove.php?hid=$hid onClick=\"if(confirm(globalAlertMsg3)){return true;}else{return false;}\"><img alt=\"$button_remove\" border=0 src=\"$hwImagePath/btn_erase.gif\"></a>";
        }
        function returnHandinListLink($hid){
                 global $image_path, $intranet_session_language,$i_Homework_handin_list;
                 global $UserID;

                 # get class group ID FROM INTRANET_HOMEWORK
                                $sql = "SELECT ClassGroupID FROM INTRANET_HOMEWORK WHERE HomeworkID='$hid' ";
                                $result = $this->returnVector($sql);
                                $classID=$result[0];

                                $numNotHandin=0;
                                $numTotal=0;
                                $numNoNeedHandin=0;
                                if($classID!=""){
                         $notHandin = $this->returnStudentsNotHandin($hid,$classID);
                         $total = $this->returnStudentsNeedToHandin($hid,$classID);
                         $noNeedHandin = $this->returnStudentsNoNeedToHandin($hid,$classID);
                                         $numNotHandin = sizeof($notHandin);
                                         $numTotal = sizeof($total) - sizeof($noNeedHandin);
                                         $numTotal = $numTotal>=0 ? $numTotal:0;

                 }
                $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                return " <A HREF=/home/homework/handin_list.php?hid=$hid><img src=\"$hwImagePath/btn_handin_list.gif\" alt=\"".$numNotHandin."/".$numTotal."\" border=0></A> ";
            }


        function returnRecord ($HomeworkID)
        {
                 $HomeworkID = IntegerSafe($HomeworkID);
                 
                 $fields ="HomeworkID, ClassGroupID, SubjectID, StartDate, DueDate, Loading, Title, Description, LastModified,RecordType,PosterName, TypeID,AttachmentPath,HandinRequired,CollectRequired,PosterUserID, CreatedBy";
                 $dbtables = "INTRANET_HOMEWORK";
                 $conds = "HomeworkID = '$HomeworkID' ";
                 $sql = "SELECT $fields FROM $dbtables WHERE $conds";
                 $array = $this->returnArray($sql);
                 if (sizeof($array) < 1) {
                     return "";
                 }
                 else {
                     return $array[0];
                 }
        }
        function returnPosterID ($HomeworkID)
        {
                 $HomeworkID = IntegerSafe($HomeworkID);
                 $sql = "SELECT PosterUserID FROM INTRANET_HOMEWORK WHERE HomeworkID = '$HomeworkID'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }
        function returnClassID($UserID)
        {
                 $UserID = IntegerSafe($UserID);
                 $sql = "SELECT a.GroupID FROM INTRANET_CLASS as a, INTRANET_USER as b WHERE b.ClassName = a.ClassName AND b.UserID = '$UserID'";
                 $result = $this->returnVector($sql);
                 return ($result[0]==""? 0 : $result[0]);
        }

        function ClassName2ClassID ($classname)
        {
                 $sql = "SELECT GroupID FROM INTRANET_GROUP WHERE Title = '$classname'";
                 $array = $this->returnVector($sql);
                 if (sizeof($array)<1)
                        return "";
                 else return $array[0];
        }

        function returnClassName($ClassID)
        {
                $ClassID = IntegerSafe($ClassID);
                $sql ="SELECT Title FROM INTRANET_GROUP WHERE GroupID = '$ClassID'";
                $array = $this->returnVector($sql);
                if (sizeof($array)==0) return "";
                return $array[0];
        }

        function returnTeacherName($TeacherID)
        {
                 $username_field = getNameFieldWithClassNumberByLang();

                 $TeacherID = IntegerSafe($TeacherID);
                 $sql = "SELECT $username_field FROM INTRANET_USER WHERE UserID = '$TeacherID'";
                 $array = $this->returnVector($sql);
                 return $array[0];
        }

        function returnSubjectName($subjectID)
        {
                 $subjectID = IntegerSafe($subjectID);
                 $sql = "SELECT SubjectName FROM INTRANET_SUBJECT WHERE SubjectID = '$subjectID'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }

        function returnTeacherClassList($teacherID)
        {
                 $fields = "distinct a.GroupID, a.Title";
                 $dbtables = "INTRANET_GROUP as a, INTRANET_HOMEWORK as b";
                 $join_conds = "a.GroupID = b.ClassGroupID";
                 $conds = "$join_conds AND b.PosterUserID = $teacherID";
                 $sql = "SELECT $fields from $dbtables where $conds order by a.Title ASC";
                 return $this->returnArray($sql,2);
        }

        function UserLogin2UserID ($UserLogin)
        {
                 $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$UserLogin'";
                 $array = $this->returnVector($sql);
                 if (sizeof($array)<1)
                        return "";
                 else return $array[0];
        }

        function returnSpecialGroups($HomeworkID="")
        {
                 $HomeworkID = ($HomeworkID==""? $this->HomeworkID: $HomeworkID);
                 $sql = "SELECT a.Title FROM INTRANET_GROUP as a, INTRANET_HOMEWORK_SPECIAL as b
                         WHERE a.GroupID = b.ClassGroupID AND b.HomeworkID = $HomeworkID";
                 $result = $this->returnVector($sql);
                 return implode(",",$result);
        }

        
		# added by Kelvin Ho 2008-10-16
		function header_error($format_wrong)
		{
			global $i_Homework_Error_Header,$error_css,$i_Profile_Import_NoMatch_Entry;
			
			$i_Homework_Error_Header = $i_Profile_Import_NoMatch_Entry[1];
            $err = $i_Homework_Error_Header;
            $css_pic = $error_css;
            
                 /*$x = "<TR><td></td><TD>$i</TD><TD $css_class>$class</TD><TD $css_subj>$subject</TD>";
                 $x .= "<TD $css_title>$title</TD><TD $css_load>$loading</TD>";
                 $x .="<TD $css_pic>$pic</TD>";
                 $x .= "<TD $css_start>$start</TD><TD $css_due>$due</TD><TD>$desp</TD>";
                 $x .= ($this->useHomeworkType) ? "<TD $css_homeworktype>$homeworktype</TD>" : "";
                 $x .= "<td>$err</td><TD></TD></TR>\n";*/
                 $x = "<tr><td></td>";
                 $x .= "<td colspan=\"$format_wrong\">$i_Homework_Error_Header</td><TD>&nbsp;</TD></TR>\n";
                 
                 return $x;
                 
		}
        function con_error($row, $i, $errno=0)
        {
                 global $i_Homework_Error_Empty,$i_Homework_Error_NoSubject,$i_Homework_Error_NoClass,$i_Homework_Error_Date, $i_Homework_Error_NoHomeworkType;
                 global $i_Homework_Error_StartDate,$i_Homework_Error_EndDate,$i_Homework_Error_NoAccessRight,$i_Homework_Error_HandIn,$i_Homework_Error_Collect;
                 
                 
                 $i++;
                 list ($class,$subject,$title,$loading,$pic,$start,$due,$desp) = $row;
                 
                 /*
                 $importSettingType
                 1: the basic setting
                 2: homeworktype setting
                 3: handin setting
                 4: collect setting
		         5: homeworktype + handin setting
                 6: homeworktype + collect setting
                 7: homeworktype + handin + collect setting
                 8: handin + collect setting*/
                      	  
                 switch($this->importSettingType)
                 {
	                 case 2:
	                 $homeworktype = $row[8];
	                 break;
	                 case 3:
	                 $handin = $row[8];
	                 break;
	                 case 4:
	                 $collect = $row[8];
	                 break;
	                 case 5:
	                 $homeworktype = $row[8];
	                 $handin = $row[9];
	                 break;
	                 case 6:
	                 $homeworktype = $row[8];
	                 $collect = $row[9];
	                 break;
	                 case 7:
	                 $homeworktype = $row[8];
	                 $handin = $row[9];
	                 $collect = $row[10];
	                 break;
	                 case 8:
	                 $handin = $row[8];
	                 $collect = $row[9];
	                 break;
                 }
                 
                 
                 
                 
                 $error_css = "bgcolor=#FFD4A4";
                 
                 switch ($errno)
                 {
                         case 1:
                              $err = $i_Homework_Error_Empty;
                              $css_class = ($class==""?$error_css:"");
                              $css_subj = ($subject==""?$error_css:"");
                              $css_title = ($title==""?$error_css:"");
                              $css_load = ($loading==""?$error_css:"");
                              $css_start = ($start==""?$error_css:"");
                              $css_due = ($due==""?$error_css:"");
                              ($this->useHomeworkType) ? $css_homeworktype = ($homeworktype==""?$error_css:"") : "";
                              break;
                         case 2:
                              $err = $i_Homework_Error_NoSubject;
                              $css_subj = $error_css;
                              break;
                         case 3:
                              $err = $i_Homework_Error_NoClass;
                              $css_class = $error_css;
                              break;
                         case 4:
                              $err = $i_Homework_Error_StartDate;
                              $css_start = $error_css;
                              break;
                         case 5:
                              $err = $i_Homework_Error_EndDate;
                              $css_start = $error_css;
                              $css_due = $error_css;
                              break;
                         case 6:
                              $err = $i_Homework_Error_EndDate;
                              $css_class = $error_css;
                              break;
                         case 7:
                              $err = $i_Homework_Error_NoHomeworkType;
                              $css_homeworktype = $error_css;
                              break;
                         case 8:
                         	  $err = $i_Homework_Error_NoAccessRight;
                         	  $css_pic = $error_css;	
                         	  break;
                         case 9:
                         	  $err = $i_Homework_Error_HandIn;
                         	  $css_handin = $error_css;	
                         	  break;
                          case 10:
                         	  $err = $i_Homework_Error_Collect;
                         	  $css_collect = $error_css;	
                         	  break;	  	  
                         default:
                                 $err = "&nbsp;";
                                 break;
                 }
                 $x = "<TR><td></td><TD>$i</TD><TD $css_class>$class</TD><TD $css_subj>$subject</TD>";
                 $x .= "<TD $css_title>$title</TD><TD $css_load>$loading</TD>";
                 $x .="<TD $css_pic>$pic</TD>";
                 $x .= "<TD $css_start>$start</TD><TD $css_due>$due</TD><TD>$desp</TD>";
                 $x .= ($this->useHomeworkType) ? "<TD $css_homeworktype>$homeworktype</TD>" : "";
                 $x .= ($this->useHomeworkHandin) ? "<TD $css_handin>$handin</TD>" : "";
                 $x .= ($this->useHomeworkCollect) ? "<TD $css_collect>$collect</TD>" : "";
                 $x .= "<td>$err</td><TD></TD></TR>\n";
                 return $x;
        }

        function returnSubjectTable()
        {
                 $sql = "SELECT SubjectID, SubjectName FROM INTRANET_SUBJECT WHERE RecordStatus = 1 order by SubjectName";
                 return $this->returnArray($sql,2);
        }
        function returnSubjectList()
        {
                 $sql = "SELECT SubjectName FROM INTRANET_SUBJECT WHERE RecordStatus = 1 ORDER BY SubjectName";
                 return $this->returnVector($sql);
        }
        function returnHomeworkTypeList()
        {
                 $sql = "SELECT TypeName FROM INTRANET_HOMEWORK_TYPE WHERE RecordStatus = 1 ORDER BY TypeName";
                 return $this->returnVector($sql);
        }
        /*
        function returnClassTable()
        {
                 $sql = "SELECT GroupID, Title FROM INTRANET_GROUP WHERE RecordType = 3 AND LOCATE('*',Title)=0 AND LOCATE('-',Title)=0 ORDER BY Title";
                 return $this->returnArray($sql,2);
        }
        */
        function returnClassList()
        {
                 $sql = "SELECT ClassName FROM INTRANET_CLASS WHERE GroupID IS NOT NULL ORDER BY ClassName";
                 return $this->returnVector($sql);
        }
        /*
        function importHomework($currentUserID, $data)
        {
                 $subjects = $this->returnSubjectList();
                 $classes = $this->returnClassList();
                 $homeworktypes = $this->returnHomeworkTypeList();

                 $delim = "";
                 $no_fields = ($this->useHomeworkType) ? 7 : 6;
                 for($i=0;$i<sizeof($data);$i++)
                 {
                         $failed = false;
                         for ($j=0; $j < sizeof ($data[$i]); $j++)
                         {
                              if ($data[$i][$j] =="" && $j!=$no_fields)
                              {
                                  $x .= $this->con_error($data[$i],$i,1);
                                  $failed = true;
                                  break;
                              }
                         }
                         if ($failed) continue;

                         if ($this->useHomeworkType)
                         {
                                 list ($class,$subject,$title,$loading,$start,$due,$description,$HomeworkType) = $data[$i];
                              }
                              else
                              {
                                      list ($class,$subject,$title,$loading,$start,$due,$description) = $data[$i];
                              }
                             # added by peterho 2006-11-07
                                   $class = intranet_htmlspecialchars($class);
                                                $subject = intranet_htmlspecialchars($subject);
                                                $title = intranet_htmlspecialchars($title);
                                                $description = intranet_htmlspecialchars($description);
                                                $HomeworkType = intranet_htmlspecialchars($HomeworkType);
                                                # end
                         if (!in_array($subject,$subjects))
                         {
                              $x .= $this->con_error($data[$i],$i,2);
                              continue;
                         }
                         if (!in_array($class,$classes))
                         {
                              $x .= $this->con_error($data[$i],$i,3);
                              continue;
                         }

                         if ($this->useHomeworkType && $HomeworkType != "" && !in_array($HomeworkType, $homeworktypes))
                         {
                                 $x .= $this->con_error($data[$i], $i, 7);
                                 continue;
                         }
                         # Fix date
                         if ($this->startFixed ==1)
                         {
                             $start = date('Y-m-d');
                         }
                         # Check date
                         # Start date >= curdate() && Start date <= due date
                         $start_stamp = strtotime($start);
                         $due_stamp = strtotime($due);
                         $today = time();

                         if (!$this->pastInputAllowed && compareDate($start_stamp,$today) < 0)  # start date eariler than today
                         {
                             $x .= $this->con_error($data[$i],$i,4);
                             continue;
                         }
                         if (compareDate($due_stamp,$start_stamp)<0)  # End Date eariler than start date
                         {
                             $x .= $this->con_error($data[$i],$i,5);
                             continue;
                         }
                         ($this->useHomeworkType) ? list($class,$subject,$title,$loading,$start,$due,$description, $HomeworkType) = $data[$i] : list($class,$subject,$title,$loading,$start,$due,$description) = $data[$i];
                         #list ($class,$subject,$title,$loading,$start,$due,$description, $HomeworkType) = $data[$i];
                         # added by peterho 2006-11-07
                                   $class = intranet_htmlspecialchars($class);
                                                $subject = intranet_htmlspecialchars($subject);
                                                $title = intranet_htmlspecialchars($title);
                                                $description = intranet_htmlspecialchars($description);
                                                $HomeworkType = intranet_htmlspecialchars($HomeworkType);
                                                # end
                         $class = addslashes($class);
                         $subject = addslashes($subject);
                         $title = addslashes($title);
                         $description = addslashes($description);
                         $TypeName = addslashes($HomeworkType);
                         $TypeID = $this->getHomeworkType($TypeName);
                         $values .= "$delim ('$class','$subject','$start','$due','$loading','$title','$description', '".$TypeID."', NULL)";
                         $delim = ",";
                 }
                 # Create Temp table
                 $sql = "CREATE TEMPORARY TABLE TEMP_IMPORT_HOMEWORK (
                          ClassName varchar(255),
                          SubjectName varchar(255),
                          StartDate datetime,
                          EndDate datetime,
                          Loading int,
                          Title varchar(150),
                          Description text,
                          TypeID int,
                          AttachmentPath varchar(255)
                         )";
                 $this->db_db_query($sql);

                 $sql = "INSERT INTO TEMP_IMPORT_HOMEWORK (ClassName, SubjectName, StartDate, EndDate,Loading,Title,Description, TypeID, AttachmentPath) VALUES $values";
                 $this->db_db_query($sql);

                 # Grab name of this user
                 $username_field = getNameFieldEng("");
                 $sql = "SELECT $username_field FROM INTRANET_USER WHERE UserID = '$currentUserID'";
                 $temp = $this->returnVector($sql);
                 $username = $temp[0];

                 # Insert records to Live table
                 $fields = "ClassGroupID,SubjectID,PosterUserID,PosterName,StartDate,DueDate,Loading,Title,Description,RecordType, TypeID,AttachmentPath";
                 $sql = "INSERT INTO INTRANET_HOMEWORK ($fields)
                                SELECT
                                      b.GroupID, c.SubjectID, $currentUserID,'$username',a.StartDate,a.EndDate,
                                      a.Loading, a.Title, a.Description,1, a.TypeID,a.AttachmentPath
                                FROM TEMP_IMPORT_HOMEWORK as a,
                                     INTRANET_GROUP as b,
                                     INTRANET_SUBJECT as c
                                WHERE
                                     a.ClassName = b.Title
                                     AND a.SubjectName = c.SubjectName
                                     AND b.RecordType = 3
                                     AND c.RecordStatus = 1
                                ";
                 $this->db_db_query($sql);
                 return $x;
        }
        */

        function importHomework($currentUserID, $data)
        {
                 $subjects = $this->returnSubjectList();
                 $classes = $this->returnClassList();
                 $homeworktypes = $this->returnHomeworkTypeList();
				 $requiredArray = array('YES','NO');
                 $delim = "";
                 $allowed_empty_fields = array(1,4,7,9); // Description, Pic and Handin are allowed to be empty
                 
                 $header = $data[0];
                 
                 for ($a=0; $a<sizeof($this->file_format); $a++)
				{
					if ($header[$a]!=$this->file_format[$a])
					{
					    $format_wrong = sizeof($this->file_format);
					    break;
					}
				}
                 array_shift($data);		# drop the title bar
                 for($i=0;$i<sizeof($data);$i++)
                 {
	                    if(!is_array($data[$i]) || trim(implode("",$data[$i]))=="") 
	                    	continue;  // skip empty line	                 
                         $failed = false;
                         
                         ## check empty fields
                         for ($j=0; $j < sizeof ($data[$i]); $j++)  
                         {
                              if ($data[$i][$j] =="" && !in_array($j,$allowed_empty_fields))
                              {
                                  $x .= $this->con_error($data[$i],$i,1);
                                  $failed = true;
                                  break;
	                          }
                         }
                         
                         
                         if ($failed) continue;

                         /*if ($this->useHomeworkType)
                         {
                              list ($class,$subject,$title,$loading,$pic,$start,$due,$description,$HomeworkType,$handin_required,$collect_required) = $data[$i];
                         }
                         else
                         {
                                  list ($class,$subject,$title,$loading,$pic,$start,$due,$description) = $data[$i];
                          }*/
                          
                          switch($this->importSettingType)
                          {
	                          case 1:
									list ($class,$subject,$title,$loading,$pic,$start,$due,$description) = $data[$i];
		                      break;
		                      case 2:
									list ($class,$subject,$title,$loading,$pic,$start,$due,$description,$HomeworkType) = $data[$i]; 	   
							  break;
							  case 3:
									list ($class,$subject,$title,$loading,$pic,$start,$due,$description,$handin_required) = $data[$i];  
							  break;
							  case 4:
									list ($class,$subject,$title,$loading,$pic,$start,$due,$description,$collect_required) = $data[$i];  
							  break;
							  case 5:
									list ($class,$subject,$title,$loading,$pic,$start,$due,$description,$HomeworkType,$handin_required) = $data[$i];
							  break;
							  case 6:
									list ($class,$subject,$title,$loading,$pic,$start,$due,$description,$HomeworkType,$collect_required) = $data[$i];
							  break;
							  case 7:
									list ($class,$subject,$title,$loading,$pic,$start,$due,$description,$HomeworkType,$handin_required,$collect_required) = $data[$i];
							  break;
							  case 8:
									list ($class,$subject,$title,$loading,$pic,$start,$due,$description,$handin_required,$collect_required) = $data[$i];   
							  break;
                          }

							
							
                         # added by peterho 2006-11-07
                               $class = intranet_htmlspecialchars($class);
                                $subject = intranet_htmlspecialchars($subject);
                                $title = intranet_htmlspecialchars($title);
                                $description = intranet_htmlspecialchars($description);
                                $HomeworkType = intranet_htmlspecialchars($HomeworkType);
                                $handin_required = intranet_htmlspecialchars($handin_required);
                                $collect_required = intranet_htmlspecialchars($collect_required);
                                # end
                         if($format_wrong)       
                         {
                              $x .= $this->header_error($format_wrong);
                              break;
                         }      
                         if(trim($subject)!='')
                         {
	                         if (!in_array($subject,$subjects))
	                         {
	                              $x .= $this->con_error($data[$i],$i,2);
	                              continue;
	                         }
                     	}
                         if (!in_array($class,$classes))
                         {
                              $x .= $this->con_error($data[$i],$i,3);
                              continue;
                         }
                         

                         if ($this->useHomeworkType && $HomeworkType != "" && !in_array($HomeworkType, $homeworktypes))
                         {
	                             $x .= $this->con_error($data[$i], $i, 7);
                                 continue;
                         }
                         if($this->useHomeworkHandin)
                         {
	                         
	                         if (trim($handin_required)!='' && !in_array(strtoupper($handin_required),$requiredArray))
	                         {
	                                 $x .= $this->con_error($data[$i], $i, 9);
	                                 continue;
	                         }
                         }
                         if($this->useHomeworkCollect)
                         {
	                         if (trim($collect_required)!='' && !in_array(strtoupper($collect_required),$requiredArray))
	                         {
	                                 $x .= $this->con_error($data[$i], $i, 10);
	                                 continue;
	                         }
	                          	
                     	 }
                     	 if(strtoupper($collect_required)=='YES')
	                         	$collect_required=1;
	                         else
	                         	$collect_required=0;
                         if(strtoupper($handin_required)=='YES')
                         	$handin_required=1;
                         else
                         	$handin_required=0;
                        
                         	
                         
                         # check owner
                         if($pic!=""){
	                         $sql ="SELECT UserID FROM INTRANET_USER WHERE UserLogin='$pic'";
	                         $temp = $this->returnVector($sql);
	                         $owner_id = $temp[0];
	                         
	                         $hasAccessRight = false;
	                         
	                         if($owner_id!=""){
	 	                        $lu = new libuser($owner_id);
	                         	if($lu->isTeacherStaff()){
								    if ($this->nonteachingAllowed || $lu->teaching == 1)
								    {
	 								    $hasAccessRight = true;
								    }
		                        }
		                     }
		                     if(!$hasAccessRight){
			                     # error
                                 $x .= $this->con_error($data[$i], $i, 8);
                                 continue;
			                 }
	                     }
                         
                         # Fix date
                         if ($this->startFixed ==1)
                         {
                             $start = date('Y-m-d');
                         }
                         # Check date
                         # Start date >= curdate() && Start date <= due date
                         $start_stamp = strtotime($start);
                         $due_stamp = strtotime($due);
                         $today = time();

                         if (!$this->pastInputAllowed && compareDate($start_stamp,$today) < 0)  # start date eariler than today
                         {
                             $x .= $this->con_error($data[$i],$i,4);
                             continue;
                         }
                         if (compareDate($due_stamp,$start_stamp)<0)  # End Date eariler than start date
                         {
                             $x .= $this->con_error($data[$i],$i,5);
                             continue;
                         }
                         ($this->useHomeworkType) ? list($class,$subject,$title,$loading,$pic,$start,$due,$description, $HomeworkType) = $data[$i] : list($class,$subject,$title,$loading,$pic,$start,$due,$description) = $data[$i];
                         #list ($class,$subject,$title,$loading,$start,$due,$description, $HomeworkType) = $data[$i];
                         # added by peterho 2006-11-07
                                   $class = intranet_htmlspecialchars($class);
                                                $subject = intranet_htmlspecialchars($subject);
                                                $title = intranet_htmlspecialchars($title);
                                                $description = intranet_htmlspecialchars($description);
                                                $HomeworkType = intranet_htmlspecialchars($HomeworkType);
                                                # end
                         $class = addslashes($class);
                         $subject = addslashes($subject);
                         $title = addslashes($title);
                         $description = addslashes($description);
                         $TypeName = addslashes($HomeworkType);
                         $TypeID = $this->getHomeworkType($TypeName);
                         $values .= "$delim ('$class','$subject','$start','$due','$loading','$pic','$title','$description', '".$TypeID."', NULL,'$handin_required','$collect_required')";
                         $delim = ",";
                 }

                 # Create Temp table
                 $sql = "CREATE TEMPORARY TABLE TEMP_IMPORT_HOMEWORK (
                          ClassName varchar(255),
                          SubjectName varchar(255),
                          StartDate datetime,
                          EndDate datetime,
                          Loading float,
                          Pic varchar(255),
                          Title varchar(150),
                          Description text,
                          TypeID int,
                          AttachmentPath varchar(255),
                          HandinRequired int(11),
                          CollectRequired int(11)
                         )";
                 $this->db_db_query($sql);

                 $sql = "INSERT INTO TEMP_IMPORT_HOMEWORK (ClassName, SubjectName, StartDate, EndDate,Loading,Pic,Title,Description, TypeID, AttachmentPath, HandinRequired, CollectRequired) VALUES $values";
                 $this->db_db_query($sql);

                 # Grab name of this user
                 $username_field = getNameFieldEng("");
                 
                 $currentUserID = IntegerSafe($currentUserID);
                 $sql = "SELECT $username_field FROM INTRANET_USER WHERE UserID = '$currentUserID'";
                 $temp = $this->returnVector($sql);
                 $username = $temp[0];
                 
                 $username_field2 = getNameFieldEng("d.");
                 
                 # Insert records to Live table
                 
	             $fields = "ClassGroupID,SubjectID,PosterUserID,PosterName,StartDate,DueDate,Loading,Title,Description,RecordType, TypeID,AttachmentPath, HandinRequired, CollectRequired";
	             /*$sql = "INSERT INTO INTRANET_HOMEWORK ($fields)
                            SELECT
                                  b.GroupID, c.SubjectID, 
                                  IF(a.Pic IS NULL OR TRIM(a.Pic)='','$currentUserID',d.UserID),
                                  IF(a.Pic IS NULL OR TRIM(a.Pic)='','$username',$username_field2),
                                  a.StartDate,a.EndDate,
                                  a.Loading, a.Title, a.Description,1, a.TypeID,a.AttachmentPath,
                                  HandinRequired, CollectRequired
                            FROM TEMP_IMPORT_HOMEWORK as a,
                                 INTRANET_GROUP as b,
                                 INTRANET_SUBJECT as c
                                 LEFT OUTER JOIN INTRANET_USER as d ON (d.UserLogin = a.Pic)
                                 
                            WHERE
                                 a.ClassName = b.Title
                                 AND a.SubjectName = c.SubjectName
                                 AND b.RecordType = 3
                                 AND c.RecordStatus = 1
                            ";*/
                 $sql = "INSERT INTO INTRANET_HOMEWORK ($fields)
                            SELECT
                                  b.GroupID, c.SubjectID, 
                                  IF(a.Pic IS NULL OR TRIM(a.Pic)='','$currentUserID',d.UserID),
                                  IF(a.Pic IS NULL OR TRIM(a.Pic)='','$username',$username_field2),
                                  a.StartDate,a.EndDate,
                                  a.Loading, a.Title, a.Description,1, a.TypeID,a.AttachmentPath,
                                  HandinRequired, CollectRequired
                            FROM TEMP_IMPORT_HOMEWORK as a left join INTRANET_GROUP as b on a.ClassName = b.Title
							left join INTRANET_SUBJECT as c on a.SubjectName = c.SubjectName AND c.RecordStatus = 1
							LEFT OUTER JOIN INTRANET_USER as d ON (d.UserLogin = a.Pic)
                                 
                            WHERE
                                 b.RecordType = 3
                            
                            ";           
								

                 $this->db_db_query($sql);
                 return $x;
        }
        function column($field_index, $field_name){
                global $image_path;
                $x = "";
                if($this->field==$field_index){
                        if($this->order==1) $x .= "<a href=javascript:sortPage(0,$field_index,document.form1) onMouseOver=\"window.status='"."Sort by"." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                        if($this->order==0) $x .= "<a href=javascript:sortPage(1,$field_index,document.form1) onMouseOver=\"window.status='"."Sort by"." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }else{
                        $x .= "<a href=javascript:sortPage($this->order,$field_index,document.form1) onMouseOver=\"window.status='"."Sort by"." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }
                $x .= str_replace("_", " ", $field_name);
                if($this->field==$field_index){
                        if($this->order==1) $x .= "<img src='$image_path/desc.gif' hspace=2 border=0>";
                        if($this->order==0) $x .= "<img src='$image_path/asc.gif' hspace=2  border=0>";
                }
                $x .= "</a>";
                $x = "<B>$x</B>";
                return $x;
        }

        function displayIndex($classID, $UserID)
        {
                 $UserID = IntegerSafe($UserID);
                 $sql = "SELECT RecordType, Teaching FROM INTRANET_USER WHERE UserID = '$UserID'";
                 $temp = $this->returnArray($sql,2);
                 list($type,$teaching) = $temp[0];
				 
                 if ($type==1 && $teaching==1)
                 {
                          $classList = $this->returnTeacherClassList($UserID);
                          if (sizeof($classList)==0)                       // No Homework posted, show all class view
                          {
                              $view = 1;
                          }
                          else $view = 2;
                 }
                 elseif ($type==1)
                 {
                         if ($this->nonteachingAllowed)
                             $view = 1;
                         else $view = 0;
                 }
                 elseif ($type==2)
                 {
                         $view = 3;
                 }
                 elseif ($type==3)
                 {
                         $view = 4;
                 }
                 
                 switch ($view)
                 {
                         case 1: return $this->allClass();
                         case 2: return $this->tabsView($classID,$UserID,$classList);
                         case 3: return $this->studentView($UserID);
                         case 4: return $this->parentView();
                         case 0:
                         default: return "";
                 }
        }

        function allClass()
        {
             global $i_Homework_Import,$image_path,$intranet_session_language,$i_Homework_New,$i_Homework_SearchSubject,$i_Homework_SearchTeacher,$i_Homework_WholeSchool,$i_Homework_SearchTeaching;
             $hwImagePath = "$image_path/homework/$intranet_session_language/hw";

             $hwBlock = "";
             $hwToolbar =
             "<img src=\"$hwImagePath/btn_new.gif\" width=19 height=19><font size=2><a href=\"homework/add.php\"> $i_Homework_New </a></font><img src=\"$hwImagePath/btn_import.gif\" width=16 height=15>
             <font size=2><a href=\"homework/import.php\"> $i_Homework_Import </a></font>
             <a href=\"homework/index.php\"><img border=0 src=\"$hwImagePath/btn_myrecord.gif\" width=115 height=22></a>";
             $hwBlock .= $this->displaySmallTitleToolbar($i_Homework_WholeSchool,$hwToolbar);
             $hwBlock .= $this->displaySmallAllClasses();
             $teacherSel = $this->getSearchTeacherSelect("name=teacherID onChange=\"this.form.submit()\"");
             $subjectSel = $this->getSearchSubjectSelect("name=subjectID onChange=\"this.form.submit()\"");
             $teachingSel = $this->getSearchTeachingSelect("name=teacherID onChange=\"this.form.submit()\"");
                 $hwBlock .= "
           <table width=517 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg.gif\">
             <tr><td colspan=3>&nbsp;</td></tr>\n";
                 if ($teacherSel != "")
                     $hwBlock .= "
             <tr>
               <td width=358 align=right><font size=2>$i_Homework_SearchTeacher  </font></td>
               <td width=10> &nbsp;</td>
               <td width=382 align=left>
               <FORM name=teacher METHOD=GET ACTION=\"homework/showteacher.php\">
               $teacherSel
               </FORM>
               </td>
             </tr>\n";
                 if ($subjectSel != "")
                     $hwBlock .= "
             <tr>
               <td width=358 align=right><font size=2>$i_Homework_SearchSubject  </font></td>
               <td width=10> &nbsp;</td>
               <td width=382 align=left>
               <form name=subject method=get action=\"homework/showsubject.php\">
               $subjectSel
               </form>
               </td>
             </tr>\n";
                 if ($teachingSel != "")
                     $hwBlock .= "
             <tr>
               <td width=358 align=right><font size=2>$i_Homework_SearchTeaching  </font></td>
               <td width=10> &nbsp;</td>
               <td width=382 align=left>
               <form name=teaching method=get action=\"homework/showteaching.php\">
               $teachingSel
               </form>
               </td>
             </tr>\n";
           $hwBlock .= "</table>\n";
           $hwBlock .= $this->displaySmallAllBottomBar();
           return $hwBlock;
        }

        function tabsView($classID, $UserID, $cList)
        {
             global $i_Homework_Import,$image_path,$intranet_session_language,$i_Homework_New,$i_Homework_SearchSubject,$i_Homework_subject,$i_Homework_myrecords;
             $hwImagePath = "$image_path/homework/$intranet_session_language/hw";

             if ($classID =="") $classID = $cList[0][0];

             $hwToolbar =
             "<img src=\"$hwImagePath/btn_new.gif\" width=19 height=19><font size=2><a href=\"homework/add.php?classID=$classID\"> $i_Homework_New </a></font><img src=\"$hwImagePath/btn_import.gif\" width=16 height=15>
             <font size=2><a href=\"homework/import.php\"> $i_Homework_Import </a></font>
             <a href=\"homework/allclass.php\"><img border=0 src=\"$hwImagePath/btn_schoolsrecord.gif\"></a>";
             $hwBlock = "";
             $hwBlock .= "<FORM name=form1 method=get action=\"#homework\">\n";
             $hwBlock .= $this->displaySmallTitleToolbar($i_Homework_myrecords,$hwToolbar);
             $hwBlock .= $this->displaySmallTabView($classID, $UserID, $cList);
             $hwBlock .= $this->displaySmallTeacherClass($classID, $UserID);
             $hwBlock .=
             "<table width=517 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg2.gif\">
             <tr><td width=26>&nbsp;</td>
             <td width=468 align=right><a href=\"homework/index.php?classID=$classID\"><img src=\"$hwImagePath/btn_more.gif\" border=0></a></td>
             <td width=23>&nbsp;</td>
             </tr>
             </table>\n";
             $hwBlock .= $this->displaySmallBottomBar();
             $hwBlock .= "</form>\n";
             return $hwBlock;
        }

        function studentView($uid)
        {
                 global $image_path,$intranet_session_language;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 if ($this->filter!=1) $this->filter=0;
                 $flag = $this->filter;
                 $x = "<FORM name=form1 method=get action=\"#homework\">\n";
                 $x .= $this->displaySmallTitleBar();
                 $x .= $this->showSmallTab($flag);
                 $cid = $this->returnClassID($uid);

                 if ($cid == "") return "";

                 $x .= $this->displaySmallStudentView($flag,$uid);

             $x .= "<table width=517 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg2.gif\">
             <tr><td width=26>&nbsp;</td>
             <td width=468 align=right><a href=\"homework/studentview.php?classID=$cid&flag=$flag\"><img src=\"$hwImagePath/btn_all.gif\" width=75 height=22 border=0></a></td>
             <td width=23>&nbsp;</td>
             </tr>
             </table>\n";

                 $x .= $this->displaySmallBottomBar();
                 $x .= "<input type=hidden name=filter value=\"$flag\">";
                 $x .= "</form>\n";
                 return $x;
        }

        function parentView()
        {
                 global $image_path,$intranet_session_language,$UserID,$i_Homework_ChildrenList;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";

                 # Get children List
                 $parentID = $UserID;
                 $namefield = getNameFieldWithClassNumberByLang("b.");
                 $sql = "SELECT a.StudentID, $namefield
                         FROM INTRANET_PARENTRELATION as a, INTRANET_USER as b
                         WHERE a.StudentID = b.UserID AND a.ParentID = $parentID";
                 $children = $this->returnArray($sql,2);

                 if (sizeof($children)==0)
                 {
                     if ($this->parentAllowed)
                     {
                         return $this->parentViewAllClass();
                     }
                     else return "";
                 }

                 $x .= $this->displaySmallTitleToolbar($i_Homework_ChildrenList,"");
                 $x .= "<table width=517 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg2.gif\">\n";
                 $x .= "<tr><td>&nbsp;</td></tr>\n";
                 $x .= "<tr><td width=500 align=center><table align=center width=90% border=1 cellspacing=0 cellpadding=4 bordercolordark=#EFFFF7 bordercolorlight=#D0DBC4>\n";

                 for ($i=0; $i<sizeof($children); $i++)
                 {
                      list($id,$name) = $children[$i];
                      $link = "<a href=/home/homework/studentview.php?studentID=$id>$name</a>";
                      $x .= "<tr><td>$link</td></tr>\n";
                 }
                 $x .= "</table>\n";
                 $x .= "</td></tr></table>\n";
                 $x .= "<table width=517 border=0 cellspacing=0 cellpadding=0><tr><td><img src=\"$hwImagePath/bar_bottom2.gif\" width=517 height=20></td></tr></table>\n";
                 $x .= $this->displaySmallAllBottomBar();
                 return $x;
        }
        function parentViewAllClass()
        {
             global $i_Homework_Import,$image_path,$intranet_session_language,$i_Homework_New,$i_Homework_SearchSubject,$i_Homework_SearchTeacher,$i_Homework_WholeSchool,$i_Homework_SearchTeaching;
             $hwImagePath = "$image_path/homework/$intranet_session_language/hw";

             $hwBlock = "";
             $hwBlock .= $this->displaySmallTitleToolbar($i_Homework_WholeSchool,"");
             $hwBlock .= $this->displaySmallAllClasses();
             $teacherSel = $this->getSearchTeacherSelect("name=teacherID onChange=\"this.form.submit()\"");
             $subjectSel = $this->getSearchSubjectSelect("name=subjectID onChange=\"this.form.submit()\"");
             $teachingSel = $this->getSearchTeachingSelect("name=teacherID onChange=\"this.form.submit()\"");
                 $hwBlock .= "
           <table width=517 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg.gif\">
             <tr><td colspan=3>&nbsp;</td></tr>";
                 if ($teacherSel != "")
                     $hwBlock .= "
             <tr>
               <td width=358 align=right><font size=2>$i_Homework_SearchTeacher  </font></td>
               <td width=10> &nbsp;</td>
               <td width=382 align=left>
               <FORM name=teacher METHOD=GET ACTION=\"homework/showteacher.php\">
               $teacherSel
               </FORM>
               </td>
             </tr>";
                 if ($subjectSel != "")
                     $hwBlock .= "
             <tr>
               <td width=358 align=right><font size=2>$i_Homework_SearchSubject  </font></td>
               <td width=10> &nbsp;</td>
               <td width=382 align=left>
               <form name=subject method=get action=\"homework/showsubject.php\">
               $subjectSel
               </form>
               </td>
             </tr>\n";
                 if ($teachingSel != "")
                     $hwBlock .= "
             <tr>
               <td width=358 align=right><font size=2>$i_Homework_SearchTeaching  </font></td>
               <td width=10> &nbsp;</td>
               <td width=382 align=left>
               <form name=teaching method=get action=\"homework/showteaching.php\">
               $teachingSel
               </form>
               </td>
             </tr>\n";
           $hwBlock .= "</table>\n";
           $hwBlock .= $this->displaySmallAllBottomBar();
           return $hwBlock;
        }

        function displayClassTodo ($ClassID,$currentID)
        {
                 $username_field = getNameFieldWithClassNumberByLang("c.");
                 # Get ClassID
                 $ClassID = $this->returnClassID($currentID);
                 if ($ClassID == "" || $ClassID == 0) return "";

                 $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading, a.Title, a.Description,
                            IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field),c.Teaching,a.ClassGroupID,a.SubjectID, d.CourseID, d.CourseAssignmentID";
                 $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                              LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                              LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as d ON a.HomeworkID = d.IntranetHomeworkID";
                 $conds = "a.ClassGroupID = $ClassID and a.StartDate <= CURDATE() and a.DueDate >= CURDATE()";
                 $sql = "SELECT $fields FROM $dbtables WHERE $conds";
                 return $this->displayTable($ClassID, $currentID, $sql);
        }

        function displayHistory ($ClassID,$currentID)
        {
                 $username_field = getNameFieldWithClassNumberByLang("c.");
                 # Get ClassID
                 $ClassID = $this->returnClassID($currentID);
                 if ($ClassID == "" || $ClassID == 0) return "";

                 $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading, a.Title, a.Description,
                            IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field),c.Teaching,a.ClassGroupID,a.SubjectID, d.CourseID, d.CourseAssignmentID";
                 $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                              LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                              LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as d ON a.HomeworkID = d.IntranetHomeworkID";
                 $conds = "a.PosterUserID = c.UserID AND a.SubjectID = b.SubjectID AND a.ClassGroupID = $ClassID and a.DueDate < CURDATE()";
                 $sql = "SELECT $fields FROM $dbtables WHERE $conds";
                 return $this->displayTable($ClassID, $currentID, $sql);
        }

        function displayStudentView($flag, $currentID)
        {
                 global $i_Homework_Only;
                 global $UserID;
                 $lu = new libuser($UserID);

                 if ($flag==0)
                 {
                     $date_conds = "AND a.StartDate <= CURDATE() AND a.DueDate >= CURDATE()";
                 }
                 else if ($flag==1)
                 {
                      $date_conds = "AND a.DueDate < CURDATE()";
                 }
                 $username_field = getNameFieldWithClassNumberByLang("c.");
                 
                 # Get groups
                 $currentID = IntegerSafe($currentID);
                 $sql = "SELECT DISTINCT a.GroupID FROM INTRANET_USERGROUP as a, INTRANET_GROUP as b WHERE a.GroupID = b.GroupID AND a.UserID = '$currentID' AND b.RecordType = 3";
                 $groups = $this->returnVector($sql);
                 if(sizeof($groups)>0)
                         $group_list = implode(",",$groups);
                 else $group_list="";

                 $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading,
                            IF(a.RecordType=1,a.Title,CONCAT(a.Title,' <I>(',d.Title,' $i_Homework_Only)</I>')), a.Description,
                            IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field), c.Teaching, a.ClassGroupID, a.SubjectID, e.CourseID, e.CourseAssignmentID, a.AttachmentPath,a.HandinRequired,a.CollectRequired,h.TypeName";
                 $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                              LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                              LEFT OUTER JOIN INTRANET_GROUP as d ON a.ClassGroupID = d.GroupID
                              LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                              LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE AS h ON a.TypeID = h.TypeID";
                 if($group_list!="")
                         $conds = " a.ClassGroupID IN ($group_list) $date_conds";
                 else $conds=" a.ClassGroupID='$group_list' $date_conds";

                 $sql = "SELECT $fields FROM $dbtables WHERE $conds";

                 # if is parent
                 if($lu->isParent())
                         return $this->displayTable($ClassID,"",$sql);
                 return $this->displayTable($ClassID, $currentID, $sql);
        }

        function displayClassView($flag, $ClassID, $currentID="")
        {
                 global $UserID,$i_Homework_Only;
                 
                 $currentID = IntegerSafe($currentID);
                 if ($currentID == "") $currentID = $UserID;
                 
                 $ClassID = IntegerSafe($ClassID);
                 $sql = "SELECT GroupID FROM INTRANET_CLASS WHERE GroupID = '$ClassID'";
                 $temp = $this->returnVector($sql);
                 $isClass = ($temp[0]!="");
                 
                 if ($flag==0)
                 {
                     $date_conds = "AND a.StartDate <= CURDATE() AND a.DueDate >= CURDATE()";
                 }
                 else if ($flag==1)
                 {
                      $date_conds = "AND a.DueDate < CURDATE()";
                 }
                 $username_field = getNameFieldWithClassNumberByLang("c.");

                 if ($isClass)
                 {
                     $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading,
                                IF(a.RecordType=1,a.Title,CONCAT(a.Title,' <I>(',d.Title,' $i_Homework_Only)</I>')), a.Description,
                                IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field), c.Teaching, a.ClassGroupID, a.SubjectID, e1.CourseID, e1.CourseAssignmentID, a.AttachmentPath,a.HandinRequired,a.CollectRequired,h.TypeName";
                     $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                                  LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                                  LEFT OUTER JOIN INTRANET_HOMEWORK_SPECIAL as e ON a.HomeworkID = e.HomeworkID
                                  LEFT OUTER JOIN INTRANET_GROUP as d ON d.GroupID = a.ClassGroupID
                                  LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e1 ON a.HomeworkID = e1.IntranetHomeworkID
                                  LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as h ON a.TypeID = h.TypeID";
                     $conds = "((a.ClassGroupID = '$ClassID' AND e.HomeworkID IS NULL AND a.RecordType = 1) OR (e.ClassGroupID = '$ClassID' AND a.HomeworkID = e.HomeworkID AND a.RecordType = 2))
                                 $date_conds";
                 }
                 else
                 {
                     $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading,
                                a.Title, a.Description,
                                IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field), c.Teaching, a.ClassGroupID, a.SubjectID, e.CourseID, e.CourseAssignmentID, a.AttachmentPath,a.HandinRequired,a.CollectRequired,h.TypeName";
                     $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                                  LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                                  LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                                  LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as h ON a.TypeID = h.TypeID";
                     $conds = "a.ClassGroupID = '$ClassID'  $date_conds";
                 }
                 $sql = "SELECT $fields FROM $dbtables WHERE $conds";
                 return $this->displayTable($ClassID, $currentID, $sql);
        }
        function displayCalendarStudent($currentID,$ts )
        {
                 global $UserID,$i_Homework_Only;
                 
                 $currentID = IntegerSafe($currentID);
                 if ($currentID == "") $currentID = $UserID;
                 
                 $month = date('m',$ts);
                 $year = date('Y',$ts);
                 $start = $ts;
                 $end = mktime(0,0,0,$month+1,1,$year);
                 $date_conds = "UNIX_TIMESTAMP(a.DueDate) >= $start AND UNIX_TIMESTAMP(a.DueDate)<$end";
                 $username_field = getNameFieldWithClassNumberByLang("c.");
                 # Get groups
                 $sql = "SELECT DISTINCT a.GroupID FROM INTRANET_USERGROUP as a, INTRANET_GROUP as b WHERE a.GroupID = b.GroupID AND a.UserID = '$currentID' AND b.RecordType = 3";
                 $groups = $this->returnVector($sql);
                 if (sizeof($groups)!=0)
                     $group_list = implode(",",$groups);

                 $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading,
                            IF(a.RecordType=1,a.Title,CONCAT(a.Title,' <I>(',d.Title,' $i_Homework_Only)</I>')), a.Description,
                            IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field), c.Teaching, a.ClassGroupID, a.SubjectID, e.CourseID, e.CourseAssignmentID,a.AttachmentPath,a.HandinRequired, h.TypeName";
                 $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                              LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                              LEFT OUTER JOIN INTRANET_GROUP as d ON a.ClassGroupID = d.GroupID
                              LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                              LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as h ON a.TypeID = h.TypeID";
                 $conds = " $date_conds";
                 if ($group_list != "")
                     $conds .= " AND (a.ClassGroupID IN ($group_list) )";
                 $sql = "SELECT $fields FROM $dbtables WHERE $conds ORDER BY a.DueDate";
                 return $this->displayCalendarView($sql,$month,$year);
        }
        function displayCalendarClass($ClassID, $ts)
        {
                 global $UserID,$i_Homework_Only;
                 
                 $ClassID = IntegerSafe($ClassID);
                 $sql = "SELECT GroupID FROM INTRANET_CLASS WHERE GroupID = '$ClassID'";
                 $temp = $this->returnVector($sql);
                 $isClass = ($temp[0]!="");
                 
                 $month = date('m',$ts);
                 $year = date('Y',$ts);
                 $start = $ts;
                 $end = mktime(0,0,0,$month+1,1,$year);
                 $date_conds = "AND UNIX_TIMESTAMP(a.DueDate) >= '$start' AND UNIX_TIMESTAMP(a.DueDate) < '$end'";
                 $username_field = getNameFieldWithClassNumberByLang("c.");

                 if ($isClass)
                 {
                     $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading,
                                IF(a.RecordType=1,a.Title,CONCAT(a.Title,' <I>(',d.Title,' $i_Homework_Only)</I>')), a.Description,
                                IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field), c.Teaching, a.ClassGroupID, a.SubjectID, e1.CourseID, e1.CourseAssignmentID, a.AttachmentPath,a.HandinRequired,h.TypeName";
                     $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                                  LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                                  LEFT OUTER JOIN INTRANET_HOMEWORK_SPECIAL as e ON a.HomeworkID = e.HomeworkID
                                  LEFT OUTER JOIN INTRANET_GROUP as d ON d.GroupID = a.ClassGroupID
                                  LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e1 ON a.HomeworkID = e1.IntranetHomeworkID
                                  LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as h ON a.TypeID = h.TypeID";
                     $conds = "((a.ClassGroupID = '$ClassID' AND e.HomeworkID IS NULL AND a.RecordType = 1) OR (e.ClassGroupID = '$ClassID' AND a.HomeworkID = e.HomeworkID AND a.RecordType = 2))
                                 $date_conds";
                 }
                 else
                 {
                     $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading,
                                a.Title, a.Description,
                                IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field), c.Teaching, a.ClassGroupID, a.SubjectID, e.CourseID, e.CourseAssignmentID, a.AttachmentPath,a.HandinRequired,h.TypeName";
                     $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                                  LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                                  LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                                  LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as h ON a.TypeID = h.TypeID";
                     $conds = "a.ClassGroupID = '$ClassID'  $date_conds";
                 }
                 $sql = "SELECT $fields FROM $dbtables WHERE $conds ORDER BY a.DueDate";
                 return $this->displayCalendarView($sql,$month,$year);
        }
        function displayWeekViewClass($ClassID,$ts)
        {
                 global $UserID,$i_Homework_Only;
                 
                 $ClassID = IntegerSafe($ClassID);
                 $sql = "SELECT GroupID FROM INTRANET_CLASS WHERE GroupID = '$ClassID'";
                 $temp = $this->returnVector($sql);
                 $isClass = ($temp[0]!="");
                 
                 $start = $ts;
                 $end = $ts+604800;
                 $date_conds = "AND UNIX_TIMESTAMP(a.DueDate) >= '$start' AND UNIX_TIMESTAMP(a.DueDate) < '$end'";
                 $username_field = getNameFieldWithClassNumberByLang("c.");

                 if ($isClass)
                 {
                     $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading,
                                IF(a.RecordType=1,a.Title,CONCAT(a.Title,' <I>(',d.Title,' $i_Homework_Only)</I>')), a.Description,
                                IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field), c.Teaching, a.ClassGroupID, a.SubjectID, f.CourseID, f.CourseAssignmentID,a.AttachmentPath, a.HandinRequired,h.TypeName";
                     $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                                  LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                                  LEFT OUTER JOIN INTRANET_HOMEWORK_SPECIAL as e ON a.HomeworkID = e.HomeworkID
                                  LEFT OUTER JOIN INTRANET_GROUP as d ON d.GroupID = a.ClassGroupID
                                  LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as f ON a.HomeworkID = f.IntranetHomeworkID
                                  LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as h ON a.TypeID = h.TypeID";
                     $conds = "((a.ClassGroupID = '$ClassID' AND e.HomeworkID IS NULL AND a.RecordType = 1) OR (e.ClassGroupID = '$ClassID' AND a.HomeworkID = e.HomeworkID AND a.RecordType = 2))
                                 $date_conds";
                 }
                 else
                 {
                     $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading,
                                a.Title, a.Description,
                                IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field), c.Teaching, a.ClassGroupID, a.SubjectID, e.CourseID, e.CourseAssignmentID, a.AttachmentPath,a.HandinRequired,h.TypeName";
                     $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                                  LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                                  LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                                  LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as h ON a.TypeID = h.TypeID";
                     $conds = "a.ClassGroupID = '$ClassID' $date_conds";
                 }
                 $sql = "SELECT $fields FROM $dbtables WHERE $conds ORDER BY a.DueDate";
                 return $this->displayWeekView($sql,$ts);
        }
        function displayWeekViewStudent($currentID,$ts )
        {
                 global $UserID,$i_Homework_Only;
                 
                 $currentID = IntegerSafe($currentID);
                 if ($currentID == "") $currentID = $UserID;
                 
                 $start = $ts;
                 $end = $ts+604800;
                 $date_conds = "UNIX_TIMESTAMP(a.DueDate) >= $start AND UNIX_TIMESTAMP(a.DueDate)<$end";
                 $username_field = getNameFieldWithClassNumberByLang("c.");
                 # Get groups
                 $sql = "SELECT DISTINCT a.GroupID FROM INTRANET_USERGROUP as a, INTRANET_GROUP as b WHERE a.GroupID = b.GroupID AND a.UserID = '$currentID' AND b.RecordType = 3";
                 $groups = $this->returnVector($sql);
                 if (sizeof($groups)!=0)
                     $group_list = implode(",",$groups);

                 $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading,
                            IF(a.RecordType=1,a.Title,CONCAT(a.Title,' <I>(',d.Title,' $i_Homework_Only)</I>')), a.Description,
                            IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field)
                            , c.Teaching, a.ClassGroupID, a.SubjectID, e.CourseID, e.CourseAssignmentID, a.AttachmentPath,a.HandinRequired, h.TypeName";
                 $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                              LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                              LEFT OUTER JOIN INTRANET_GROUP as d ON a.ClassGroupID = d.GroupID
                              LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                              LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as h ON a.TypeID = h.TypeID";
                 $conds = " $date_conds";
                 if ($group_list != "")
                     $conds .= " AND (a.ClassGroupID IN ($group_list) )";
                 $sql = "SELECT $fields FROM $dbtables WHERE $conds ORDER BY a.DueDate";
                 return $this->displayWeekView($sql,$ts);
        }
        function displayCalendarView($sql,$month,$year)
        {
	        
                 global $image_path,$intranet_session_language,$UserID,$i_frontpage_day;
                 #global $i_DayType0,$i_DayType1,$i_DayType2,$i_DayType3;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x = "";
                 $start = mktime(0,0,0,$month,1,$year);
                 $end = mktime(0,0,0,$month+1,1,$year);
                 $lcal = new libcalevent();
                 $row = $lcal->returnCalendarArray($month,$year);
                 $lcycle = new libcycleperiods();
                                 $cycle_days = $lcycle->getCycleInfoByDateRange(date('Y-m-d',$start),date('Y-m-d',$end));

                # Homework data
                 $data = $this->returnArray($sql,17);
                 $this->calData = $data;
                 $x .= "<table width=714 border=0 cellpadding=0 cellspacing=0 bordercolordark=#31BC9B class=body bodercolorlight=#BBEAD5>
                          <tr valign=top>
                            <td width=102 align=left class=homework_cal_month_tab_bg_cell1><img src=\"$image_path/homeworklist/month_tab_sun_$intranet_session_language.gif\" width=50 height=43></td>
                            <td width=102 align=left class=homework_cal_month_tab_bg_cell2><img src=\"$image_path/homeworklist/month_tab_mon_$intranet_session_language.gif\" width=81 height=43></td>
                            <td width=102 align=left class=homework_cal_month_tab_bg_cell1><img src=\"$image_path/homeworklist/month_tab_tue_$intranet_session_language.gif\" width=81 height=43></td>
                            <td width=102 align=left class=homework_cal_month_tab_bg_cell2><img src=\"$image_path/homeworklist/month_tab_wed_$intranet_session_language.gif\" width=81 height=43></td>
                            <td width=102 align=left class=homework_cal_month_tab_bg_cell1><img src=\"$image_path/homeworklist/month_tab_thu_$intranet_session_language.gif\" width=81 height=43></td>
                            <td width=102 align=left class=homework_cal_month_tab_bg_cell2><img src=\"$image_path/homeworklist/month_tab_fri_$intranet_session_language.gif\" width=81 height=43></td>
                            <td width=102 align=left class=homework_cal_month_tab_bg_cell1><img src=\"$image_path/homeworklist/month_tab_sat_$intranet_session_language.gif\" width=81 height=43></td>
                          </tr>
                        </table>\n";
                 $x .= "<table width=714 border=1 cellpadding=1 cellspacing=0 bordercolordark=#31BC9B class=body bodercolorlight=#BBEAD5>\n";
                 for($i=0; $i<sizeof($row); $i++)
                 {
                     $pos = $i%7;
                     $x .= ($pos==0) ? "</tr>\n<tr height=50>\n" : "";
                     $lang = $intranet_session_language=="b5"?1:0;
                     $recordDate = date('Y-m-d',$row[$i]);
                     $cycle = $cycle_days[$recordDate][$lang];

                     if($cycle===-1 || $cycle==="") $display="";
                     else $display=$cycle;
                     $display = ($display==""?"":"(".$display.")");

                     $odd = $pos%2;
                     $x .= $this->displayDayBlock($row[$i],$display,$odd);
                 }
                 $x .= "</tr></table>\n";
                 return $x;
        }
        function displayWeekView($sql,$ts)
        {
                 global $image_path,$intranet_session_language,$UserID,$i_frontpage_day;
                 global $i_Homework_minimum,$i_Homework_morethan,$i_Homework_hours,$i_Homework_subject,$i_Homework_loading;
                 global $i_AnnouncementAttachment;
                 $currentID = $UserID;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 # Get Homework data
                 $data = $this->returnArray($sql,17);
                 $this->calData = $data;
                 # Set $ts to sunday of the week
                 $ts = mktime(0,0,0,date('m',$ts),date('d',$ts)-date('w',$ts),date('Y',$ts));
                 $today = date('Y-m-d');


                 # Get Cycle days
                 $b = new libcycleperiods();
                 $cStart = date('Y-m-d',$ts);
                 $cEnd = date('Y-m-d',$ts+6*86400);
                 $cycle_days = $b->getCycleInfoByDateRange($cStart,$cEnd);


                 for ($i=0; $i<7; $i++)
                 {
                      $week_ts[] = $ts+$i*86400;

                 }

                 $date_image = array("sun","mon","tue","wed","thu","fri","sat");
                 $x .= "<table width=646 border=0 align=center cellpadding=0 cellspacing=0>";
                 for ($i=0; $i<7; $i++)
                 {
                      $current = date('Y-m-d',$week_ts[$i]);  #date('Y-m-d',$ts+$i*86400);
                      if ($today == $current)
                      {
                          $font_tag_o = "<font color=#0000FF>";
                          $font_tag_e = "</font>";
                      }
                      else
                      {
                          $font_tag_o = "";
                          $font_tag_e = "";
                      }

                      list($hid,$subject,$posterID,$startdate,$duedate,$loading,$title,$description,$teachername,$posterTeaching,$recordClassID,$recordSubjectID,$courseID, $courseAssignmentID, $AttachmentPath,$handinRequired,$HomeworkType) = $this->calData[0];
                                 $cDayString =$intranet_session_language=="b5"?$cycle_days[$current][1]:$cycle_days[$current][0];
                      if ($cDayString != "") $cDayString = "($cDayString)";
                      if ($i!=0)
                      {
                          $cell_4_class = "3";
                          $cell_5_suf = "2";
                      }
                      else
                      {
                          $cell_4_class = "2";
                          $cell_5_suf = "";
                      }
                      $x .= "<tr class=h1><td width=48><img src=\"$image_path/homeworklist/cal_date_".$date_image[$i]."_$intranet_session_language.gif\" width=48 height=43></td>
                               <td width=200 class=homework_cal_week_date_bg_cell>$font_tag_o $current $cDayString $font_tag_e</td>
                               <td width=9><img src=\"$image_path/homeworklist/cal_date_tab.gif\" width=9 height=43></td>
                               <td width=347 class=homework_cal_week_date_bg_cell$cell_4_class>&nbsp;</td>
                               <td width=42><img src=\"$image_path/homeworklist/cal_date_tab_right$cell_5_suf.gif\" width=42 height=43></td>
                             </tr>\n";
                      $x .= "<tr>
                               <td class=homework_cal_week_bg_left>&nbsp;</td>
                               <td bgcolor=\"#F2FFF9\">&nbsp;</td>
                               <td bgcolor=\"#F2FFF9\">&nbsp;</td>
                               <td bgcolor=\"#F2FFF9\">&nbsp;</td>
                               <td class=homework_cal_week_bg_right>&nbsp;</td>
                             </tr>\n";
                      $x .= "<tr>
                               <td class=homework_cal_week_bg_left>&nbsp;</td>
                               <td colspan=3 bgcolor=\"#F2FFF9\"\n>";
                      #$x .= "<tr><td width=100% align=left>$current (".$i_frontpage_day[$i].")</td><tr>\n<tr><td width=100% height=50>";
                      $count = 0;
                      $cell = "";
                     while ($current == $duedate)
                     {
                            #$text = "$title";
                            if ($loading==0)
                            {
                                $tload = $i_Homework_minimum;
                            }
                            else if ($loading <= 16)
                            {
                                 $tload = $loading/2;
                            }
                            else
                            {
                                $tload = "$i_Homework_morethan 8";
                            }

                            $title = ($this->useHomeworkType && $HomeworkType != "") ? "[".$HomeworkType."] ".$title : $title;
                            $text = "<a href=javascript:fe_view_homework_detail($hid)>$title</a><br>$i_Homework_subject: $subject<br>$i_Homework_loading: $tload $i_Homework_hours<br>";

                            if ($description != "")
                               $text .= " <img src=\"$hwImagePath/icon_alt.gif\" alt=\"$description\"> ";
                            if ($AttachmentPath!="")
                                    $text .=" <img src=\"$image_path/icon_attachment.gif\" alt=\"$i_AnnouncementAttachment\"> ";

                            # handin list link
                                                        if ($this->useHomeworkHandin && $handinRequired && $this->hasAccessHandinListRight($UserID,$hid)){
                                                                        $text .= $this->returnHandinListLink($hid);
                                                        }
                            $editAllowed = false;
                            if ($courseID!="" && $courseAssignmentID!="")
                            {
                            }
                            if ($currentID == $posterID)
                            {
                                $editAllowed = true;
                            }
                            else if ($posterTeaching != 1 && $this->hasTeachingSubject($recordClassID,$recordSubjectID))
                            {
                                 $editAllowed = true;
                            }

                            if ($editAllowed)
                            {
                                $text .= $this->returnEditDeleteLink($hid);
                            }
                            if ($courseID!="" && $courseAssignmentID!="")
                            {
                                $text .= $this->returneClassIcon();
                            }

                            array_shift($this->calData);
                            list($hid,$subject,$posterID,$startdate,$duedate,$loading,$title,$description,$teachername,$posterTeaching, $recordClassID, $recordSubjectID, $courseID, $courseAssignmentID,$AttachmentPath) = $this->calData[0];
                            $count++;

                            $cell .= "<table width=556 border=0 cellpadding=2 cellspacing=0 class=bodylink>
                                        <tr align=left valign=top>
                                          <td width=15><img src=\"$image_path/homeworklist/cal_bullet.gif\" width=13 height=13></td>
                                          <td width=533>$text</td>
                                        </tr>
                                      </table>\n";
                     }
                     if ($count==0)
                     {
                         $cell = "&nbsp;";
                     }
                     $x .= "$cell</td><td class=homework_cal_week_bg_right>&nbsp;</td></tr>\n";
                     $x .= "<tr>
                             <td class=homework_cal_week_bg_left>&nbsp;</td>
                             <td bgcolor=\"#F2FFF9\">&nbsp;</td>
                             <td bgcolor=\"#F2FFF9\">&nbsp;</td>
                             <td bgcolor=\"#F2FFF9\">&nbsp;</td>
                             <td class=homework_cal_week_bg_right>&nbsp;</td>
                            </tr>\n";

                 }
                 $x .= "</table>\n";
                 #$x .= "</tr></table></td></tr></table>\n";
                 return $x;
        }
        function displayDayBlock($ts, $cycle,$odd)
        {
                 global $i_Homework_minimum,$i_Homework_morethan,$i_Homework_hours,$image_path,$intranet_session_language;
                 global $i_Homework_loading,$i_Homework_subject;
                 global $UserID;
                 global $i_AnnouncementAttachment;
                 
                 $currentID = $UserID;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $cell_bg = ($odd==1? "class=homework_cal_month_cell_bg1":"class=homework_cal_month_cell_bg0");
                 $today = mktime(0,0,0,date('m'),date('d'),date('Y'));
                 if ($today == $ts)
                 {
                     $cell_bg = "class=homework_cal_month_cell_today";
                     $cell_bgcolor = "#EBCF87";
                     $dayfontcolor = "#0000FF";
                 }
                 else
                 {
                     $cell_bgcolor = "#87EBE0";
                     $dayfontcolor = "black";
                 }

                 if($ts=="")
                 {
                    $x = "<td width=99 $cell_bg align=center><br></td>";
                 }
                 else
                 {	
                     if ($cycle=="") $cycle = "&nbsp;";
                     $i = 0;
                     list($hid,$subject,$posterID,$startdate,$duedate,$loading,$title,$description,$teachername,$posterTeaching,$recordClassID,$recordSubjectID,$courseID, $courseAssignmentID, $AttachmentPath,$handinRequired,$HomeworkType) = $this->calData[0];
                     #$cell = "<table width=100% border=0 cellpadding=0 cellspacing=0 class=body>\n";
                     $cell = "";
                     $blockDate = date('Y-m-d',$ts);
                     $day = date('j',$ts);
                     
                     while ($blockDate == $duedate)
                     {
                            if ($loading==0)
                            {
                                $tload = $i_Homework_minimum;
                            }
                            else if ($loading <= 16)
                            {
                                 $tload = $loading/2;
                            }
                            else
                            {
                                $tload = "$i_Homework_morethan 8";
                            }
                            $title = ($this->useHomeworkType && $HomeworkType != "") ? "[$HomeworkType] ".$title : $title;
                            $text = "<a href=javascript:fe_view_homework_detail($hid)>$title</a><br>$i_Homework_subject: $subject<br>$i_Homework_loading: $tload $i_Homework_hours<br>";
                            if ($description != "")
                               $text .= " <img src=\"$hwImagePath/icon_alt.gif\" alt=\"$description\"> ";
                            if ($AttachmentPath !="")
                                    $text .="<img src=\"$image_path/icon_attachment.gif\" alt=\"$i_AnnouncementAttachment\"> ";
                                                        if ($this->useHomeworkHandin && $handinRequired && $this->hasAccessHandinListRight($UserID,$hid)){
                                          $text .= $this->returnHandinListLink($hid);
                                      }
                            $editAllowed = false;
                            if ($courseID != "" && $courseAssignmentID!="")
                            {
                            }
                            else if ($currentID == $posterID)
                            {
                                $editAllowed = true;
                            }
                            else if ($posterTeaching != 1 && $this->hasTeachingSubject($recordClassID,$recordSubjectID))
                            {
                                 $editAllowed = true;
                            }

                            if ($editAllowed)
                            {
                                $text .= $this->returnEditDeleteLink($hid);
                            }
                            if ($courseID != "" && $courseAssignmentID !="")
                            {
                                $text .= $this->returneClassIcon();
                            }

                            $cell .= "<table width=100% border=0 cellpadding=0 cellspacing=0 class=body>\n
                                        <tr>
                                          <td width=10 align=left valign=top><img src=\"$image_path/homeworklist/month_bullet.gif\" width=8 height=12></td>
                                          <td align=left valign=top>$text</td>
                                        </tr>
                                        <tr>
                                          <td>&nbsp;</td>
                                          <td>&nbsp;</td>
                                        </tr>
                                      </table>\n";
                            array_shift($this->calData);
                            list($hid,$subject,$posterID,$startdate,$duedate,$loading,$title,$description,$teachername,$posterTeaching,$recordClassID,$recordSubjectID,$courseID, $courseAssignmentID,$AttachmentPath,$handinRequired,$HomeworkType) = $this->calData[0];
                     }

                     $x  = "<td $cell_bg width=99 align=center>";
                     $x .= "<table width=100% border=0 cellpadding=0 cellspacing=0 bgcolor=$cell_bgcolor class=body>
                                  <tr><td><font color=$dayfontcolor><strong>$day</strong> <font size=1>$cycle</font></font></td></tr>
                            </table>
                            $cell";
                     $x .= "</td>\n";
                 }
                 return $x;
        }        

        function displaySmallStudentView($flag,$currentID)
        {
                 global $i_Homework_today;
                 global $UserID;
                 if ($flag==0)
                 {
                     $date_conds = "a.StartDate <= CURDATE() AND a.DueDate >= CURDATE()";
                 }
                 else if ($flag==1)
                 {
                      $date_conds = "a.DueDate < CURDATE()";
                 }
                 $username_field = getNameFieldWithClassNumberByLang("c.");
                 
                 # Get groups
                 $currentID = IntegerSafe($currentID);
                 $sql = "SELECT DISTINCT a.GroupID FROM INTRANET_USERGROUP as a, INTRANET_GROUP as b WHERE a.GroupID = b.GroupID AND a.UserID = '$currentID' AND b.RecordType = 3";
                 $groups = $this->returnVector($sql);
                 if (sizeof($groups)==0)
                 {
                     return;
                 }
                 $group_list = implode(",",$groups);

                 $fields = "CONCAT(IF(a.RecordType=1,a.Title,CONCAT(a.Title,' <I>(',d.Title,')</I>')), if (a.StartDate=CURDATE(),' $i_Homework_today',''))
                            , a.DueDate, b.SubjectName, a.Loading, a.HomeworkID, a.PosterUserID,a.Description
                            ,c.Teaching, a.ClassGroupID, a.SubjectID, e.CourseID, e.CourseAssignmentID,a.AttachmentPath,a.HandinRequired,f.TypeName";
                 $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                              LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                              LEFT OUTER JOIN INTRANET_GROUP as d ON a.ClassGroupID = d.GroupID
                              LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                              LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as f ON (a.TypeID=f.TypeID)
                              ";
                 $conds = " $date_conds";
                 $conds .= " AND (a.ClassGroupID IN ($group_list) )";
                 $sql = "SELECT $fields FROM $dbtables WHERE $conds";
                 return $this->displaySmallTable($classID, $currentID, $sql);
        }

        function displayTable($ClassID,$currentID, $sql)
        {
                global $i_Homework_morethan,$i_Homework_hours,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_lastModified;
                 global $image_path, $intranet_session_language,$i_Homework_teacher_name,$i_Homework_minimum;
                 global $i_Homework_Poster;
                 global $i_AnnouncementAttachment,$i_Homework_Collected_By_Class_Teacher,$i_general_yes,$i_general_no;
                 $username_field = getNameFieldWithClassNumberByLang("c.");

                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x = "";

                $sorting_fields = array("a.Title","b.SubjectName","$username_field","a.StartDate","a.DueDate","a.Loading","a.collectRequired");
                if ($this->field=="" || $this->field >= count($sorting_fields))
                    $this->field = 4;

                if ($this->order=="" || ($this->order!=1))
                    $this->order = 0;
                if ($this->pageNo==""|| ($this->pageNo==0))
                    $this->pageNo =1;

                $order_str = ($this->order==0) ? " ASC" : " DESC";
                $sql .= " ORDER BY " . $sorting_fields[$this->field] . $order_str;
                $n_start = ($this->pageNo-1)*$this->page_size;
                $this->rs = $this->db_db_query($sql);
                $x .= "<table width=717 border=0 cellspacing=0 cellpadding=5 background=/images/homeworklist/notebooktbbg.gif class=h1>\n";
                $x .= "<tr><td colspan=3><table width=100%>\n";
                $x .= "<tr><td width=30>&nbsp;</td>\n<td width=212>&nbsp;</td>\n";
                $x .= "<td width=90>&nbsp;</td>\n<td width=130>&nbsp;</td>\n<td width=90>&nbsp;</td>\n";
                $x .= "<td width=90>&nbsp;</td>\n<td width=75>&nbsp;</td>\n</tr>";
                $i =0;

                if($this->rs && $this->db_num_rows()!=0){
                        $x .= "<tr><td>#</td>\n";
                        $x .= "<TD>".$this->column(0,$i_Homework_title)."</TD>\n";
                        $x .= "<TD>".$this->column(1,$i_Homework_subject)."</TD>\n";
                        $x .= "<TD>".$this->column(2,$i_Homework_Poster)."</TD>\n";
                        $x .= "<TD>".$this->column(3,$i_Homework_startdate)."</TD>\n";
                        $x .= "<TD>".$this->column(4,$i_Homework_duedate)."</TD>\n";
                        $x .= "<TD>".$this->column(5,"$i_Homework_loading ($i_Homework_hours)")."</TD>";
                        if($this->useHomeworkCollect)
                        	$x .= "<TD>".$this->column(6,$i_Homework_Collected_By_Class_Teacher)."</TD>";
                        $x.="</TR>\n";
                        if($this->useHomeworkCollect)
                        	$x .=$this->getLineSeparation(8);
                        else
                        	$x .=$this->getLineSeparation(7);
                        if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){


                           while($row = $this->db_fetch_array()){
                                  $i++;
                                  if($i>$this->page_size) break;
                                  $count = $n_start+$i;
                                  list($hid,$subject,$posterID,$startdate,$duedate,$loading,$title,$description,$teachername,$posterTeaching,$recordClassID,$recordSubjectID, $courseID, $courseAssignmentID, $AttachmentPath,$handinRequired,$collectRequired,$HomeworkType) = $row;
                                  $title = ($this->useHomeworkType && $HomeworkType != "") ? "[".$HomeworkType."] ".$title : $title;
                                  $title = intranet_wordwrap($title,20,"\n",1);
                                  $subject = intranet_wordwrap ($subject,14,"\n",1);
                                  $teachername = intranet_wordwrap ($teachername,16,"\n",1);
                                  $x .= "<TR><TD>$count</td><td><A HREF='javascript:fe_view_homework_detail($hid)'>$title</A>\n";
                                  if ($description != "")
                                      $x .= "<img src=\"$hwImagePath/icon_alt.gif\" alt=\"$description\"> ";

                                  if ($AttachmentPath != "")
                                            $x .= "<img src=\"$image_path/icon_attachment.gif\" alt=\"$i_AnnouncementAttachment\"> ";

                                  if ($this->useHomeworkHandin && $handinRequired && $this->hasAccessHandinListRight($currentID,$hid)){
                                          $temp = $this->rs;
                                          $x .= $this->returnHandinListLink($hid);
                                          $this->rs = $temp;
                                      }

                                  if ($courseID != "" && $courseAssignmentID != "")
                                  {
                                      $x .= $this->returneClassIcon();
                                  }
                                  $editAllowed = false;
                                  if ($courseID != "" && $courseAssignmentID != "")
                                  {
                                  }
                                  else if ($currentID == $posterID)
                                  {
                                      $editAllowed = true;
                                  }
                                  else if ($posterTeaching != 1 && $this->hasTeachingSubject($recordClassID,$recordSubjectID))
                                  {
                                       $editAllowed = true;
                                  }

                                  if ($editAllowed)
                                  {
                                          $x .= $this->returnEditDeleteLink($hid);
                                  }
                                  $x .= "</TD>\n";
                                  $x .= "<TD>$subject</TD>\n";
                                  $x .= "<TD>$teachername</TD>\n";
                                  $x .= "<td>$startdate</td>\n";
                                  $x .= "<td>$duedate</td>\n";
                                  if ($loading==0)
                                  {
                                      $tload = $i_Homework_minimum;
                                  }
                                  else if ($loading <= 16)
                                  {
                                       $tload = $loading/2;
                                  }
                                  else
                                  {
                                       $tload = "$i_Homework_morethan 8";
                                  }
                                  $x .= "<TD>$tload</TD>";
                                  if($this->useHomeworkCollect)
                                  	$x.="<td>".($collectRequired?$i_general_yes:$i_general_no)."</td>";
                                  $x .= "</TR>\n";
                                  if($this->useHomeworkCollect)
                                  	$x .= $this->getLineSeparation(8);
                                  else
                                  	$x .= $this->getLineSeparation(7);
                            }

                        }
                }
                else
                {
                        $x .= "<tr><td align=center colspan=7>".$this->no_msg."</td></tr>";
                }
                $x .= "</TABLE></td></tr>";
                if($this->db_num_rows()<>0)
                {
                   $inside = "<table width=525><tr><td width=* bgcolor=#CEECE6 style='vertical-align:middle' align=center>".$this->navigation()."</td></tr></table>\n";
                   $x .= "<td width=126></td><td align=center colspan=".$this->no_col.">$inside</td><td width=66></td></tr>\n";
                }
                $x .= "</table>";

                $x.= "<input type=hidden name=field value=".$this->field.">\n";
                $x.= "<input type=hidden name=order value=".$this->order.">\n";
                $x.= "<input type=hidden name=pageNo value=1>\n";
                $x.= "<input type=hidden name=classID value=$ClassID>\n";
                $this->db_free_result();
                return $x;
        }

        function displaySmallTable($cid, $uid,$sql)
        {
                global $i_Homework_morethan,$i_Homework_hours,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_lastModified;
                 global $image_path, $intranet_session_language,$i_Homework_teacher_name,$i_Homework_minimum;
                 global $i_Homework_Poster;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x = "";

                 $uid = IntegerSafe($uid);
                 $usertypesql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '$uid'";
                 $temp = $this->returnVector($usertypesql);
                 $type = $temp[0];

                $edit_box = ($type==1);
                $sorting_fields = array("a.Title","a.DueDate","b.SubjectName","a.Loading");
                if ($this->field=="" || $this->field >= count($sorting_fields))
                    $this->field = 1;
                if ($this->order=="" || ($this->order!=1))
                    $this->order = 0;
                $order_str = ($this->order==0) ? " ASC" : " DESC";

                $sql .= " ORDER BY " . $sorting_fields[$this->field] . $order_str;
                $sql .= " LIMIT 20";
                $this->rs = $this->db_db_query($sql);


                $x .= "<table width=517 border=0 cellspacing=2 cellpadding=0 background=\"$hwImagePath/table_bg2.gif\">\n";
                $x .= "<tr><td width=26>&nbsp;</td>\n<td width=26>&nbsp;</td>\n";
                if ($edit_box)
                    $x .= "<td width=157>&nbsp;</td>\n";
                else
                    $x .= "<td width=197>&nbsp;</td>\n";
                $x .= "<td width=90>&nbsp;</td>\n<td width=60>&nbsp;</td>\n<td width=95>&nbsp;</td>\n";

                if ($edit_box)
                {
                    $x .= "<td width=50> &nbsp; </td>\n";
                    $colsp = 8;
                }
                else $colsp = 7;

                $x .= "<td width=23>&nbsp;</td></tr>";
                $i =0;
                if($this->rs && $this->db_num_rows()!=0){
                        $x .= "<tr><td></td><td>#</td>\n";
                        $x .= "<TD>".$this->column(0,$i_Homework_title)."</TD>\n";
                        $x .= "<TD>".$this->column(1,$i_Homework_duedate)."</TD>\n";
                        $x .= "<TD>".$this->column(2,$i_Homework_subject)."</TD>\n";
                        $x .= "<TD>".$this->column(3,"$i_Homework_loading ($i_Homework_hours)")."</TD>\n";
                        if ($edit_box)
                        {
                            $x .= "<td> &nbsp; </td>\n";
                            $colsp = 8;
                        }
                        else $colsp = 7;

                        $x .= "<td> &nbsp; </td></tr>\n";
                        $x .= $this->getSmallLineSeparation($colsp);
                        $fields = "a.HomeworkID, a.Title, b.SubjectName, a.DueDate, a.Loading, a.Description";

                        while($row = $this->db_fetch_array()){
                                  $i++;
                                  if($i>20) break;
                                  list($title,$duedate,$subject,$loading,$hid,$posterID, $description,$posterTeaching,$recordClassID,$recordSubjectID, $courseID, $courseAssignmentID,$attachmentPath,$handinRequired,$typeName) = $row;
                                  $title = intranet_wordwrap($title,20,"\n",1);
                                  if($typeName!="" && $this->useHomeworkType)
                                          $title = "[$typeName] $title";
                                  $subject = intranet_wordwrap($subject,14,"\n",1);

                                  $x .= "<TR><td></td><TD>$i</td><td><A HREF='javascript:fe_view_homework_detail($hid)'>$title</A>\n";
                                  if ($description!="")
                                      $x .= "<img src=\"$hwImagePath/icon_alt.gif\" alt=\"$description\"> ";
                                                                    if($attachmentPath!=""){
                                  $x .=" <img src=\"$image_path/icon_attachment.gif\" alt=\"$i_AnnouncementAttachment\"> ";
                                      }

                                  if ($courseID != "" && $courseAssignmentID != "")
                                  {
                                      $x .= $this->returneClassIcon();
                                  }
                                  $x .= "</TD>\n";
                                  $x .= "<td>$duedate</td>\n";
                                  $x .= "<TD>$subject</TD>\n";
                                  if ($loading == 0)
                                  {
                                      $tload = $i_Homework_minimum;
                                  }
                                  else if ($loading <= 16)
                                   {
                                           $tload = $loading/2;
                                   }
                                   else
                                   {
                                           $tload = "$i_Homework_morethan 8";
                                   }
                                  $x .= "<TD align=center>$tload</TD>";
                                  $x .= "<td>";
                                  if ($this->useHomeworkHandin && $handinRequired && $this->hasAccessHandinListRight($uid,$hid)){
                                                  $temp = $this->rs;
                                          $x .= $this->returnHandinListLink($hid);
                                          $this->rs = $temp;
                                      }

                                  if ($edit_box)
                                  {
                                      $editAllowed = false;
                                      if ($courseID != "" && $courseAssignmentID != "")
                                      {
                                      }
                                      else if ($uid == $posterID)
                                      {
                                          $editAllowed = true;
                                      }
                                      else if ($posterTeaching != 1 && $this->hasTeachingSubject($recordClassID,$recordSubjectID))
                                      {
                                           $editAllowed = true;
                                      }

                                      if ($editAllowed)
                                      {
                                          $x .= $this->returnEditDeleteLink($hid);
                                      }
                                      $x .= "</td><td>\n";
                                  }
                                  $x .= "</td></TR>\n";
                                  $x .= $this->getSmallLineSeparation($colsp);
                        }
                }
                else
                {
                        $x .= "<tr><td align=center colspan=$colsp>".$this->no_msg."</td></tr>";
                }
                $x .= "</TABLE>";

                $x.= "<input type=hidden name=field value=".$this->field.">\n";
                $x.= "<input type=hidden name=order value=".$this->order.">\n";
                $x.= "<input type=hidden name=pageNo value=1>\n";
                $x.= "<input type=hidden name=classID value=$cid>\n";
                $this->db_free_result();
                return $x;
        }

        function displayTeacher($tid, $uid)
        {
                global $i_Homework_morethan,$i_Homework_hours,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_lastModified;
                 global $image_path, $intranet_session_language,$i_Homework_teacher_name,$i_Homework_minimum,$i_Homework_Group,$i_general_no,$i_general_yes,$i_Homework_Collected_By_Class_Teacher;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x = "";

                $sorting_fields = array("a.Title","b.SubjectName","d.Title","a.StartDate","a.DueDate","a.Loading","a.collectrequired");
                if ($this->field=="" || $this->field >= count($sorting_fields))
                    $this->field = 4;

//                $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading, a.Title, a.Description, CONCAT(c.LastName, ' ' ,c.FirstName)";

                if ($this->order=="" || ($this->order!=0))
                    $this->order = 1;
                if ($this->pageNo==""|| ($this->pageNo==0))
                    $this->pageNo =1;
                $order_str = ($this->order==0) ? " ASC" : " DESC";
                $fields = "a.HomeworkID, a.Title, b.SubjectName, d.Title, a.StartDate, a.DueDate, a.Loading, a.Description
                           ,c.Teaching, a.ClassGroupID, a.SubjectID, e.CourseID, e.CourseAssignmentID, a.AttachmentPath, a.HandinRequired,a.CollectRequired,f.TypeName";
                $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                             LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                             LEFT OUTER JOIN INTRANET_GROUP as d ON a.ClassGroupID = d.GroupID
                             LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                             LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as f ON (a.TypeID=f.TypeID) ";
                $conds = "a.PosterUserID = $tid";
                $sql = "SELECT $fields FROM $dbtables WHERE $conds";
                $sql .= " ORDER BY " . $sorting_fields[$this->field] . $order_str;
                $n_start = ($this->pageNo-1)*$this->page_size;
                $this->rs = $this->db_db_query($sql);
                $x .= "<table width=717 border=0 cellspacing=0 cellpadding=5 background=/images/homeworklist/notebooktbbg.gif class=h1>\n";
                $x .= "<tr><td colspan=3><table width=100%>\n";
                $x .= "<tr><td width=30>&nbsp;</td>\n<td width=212>&nbsp;</td>\n";
                $x .= "<td width=90>&nbsp;</td>\n<td width=130>&nbsp;</td>\n<td width=90>&nbsp;</td>\n";
                $x .= "<td width=90>&nbsp;</td>\n<td width=75>&nbsp;</td>\n</tr>";
                $i =0;

                if($this->rs && $this->db_num_rows()!=0){
                        $x .= "<tr><td>#</td>\n";
                        $x .= "<TD>".$this->column(0,$i_Homework_title)."</TD>\n";
                        $x .= "<TD>".$this->column(1,$i_Homework_subject)."</TD>\n";
                        $x .= "<TD>".$this->column(2,"$i_Homework_class/$i_Homework_Group")."</TD>\n";
                        $x .= "<TD>".$this->column(3,$i_Homework_startdate)."</TD>\n";
                        $x .= "<TD>".$this->column(4,$i_Homework_duedate)."</TD>\n";
                        $x .= "<TD>".$this->column(5,"$i_Homework_loading ($i_Homework_hours)")."</TD>\n";
                        if($this->useHomeworkCollect)
                        {
                     	   $x .= "<TD>".$this->column(6,"$i_Homework_Collected_By_Class_Teacher")."</TD>\n";
                    	}
                    	$x.="</TR>";
                        if($this->useHomeworkCollect)
                        {
                        	$x .= $this->getLineSeparation(8);
                    	}
                        else
                        {
                        	$x .= $this->getLineSeparation(7);
                    	}
                        if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                           $match = ($tid==$uid);

                           while($row = $this->db_fetch_array()){
                                  $i++;
                                  if($i>$this->page_size) break;
                                  $count = $n_start+$i;
                                  list($hid,$title,$subject,$class,$startdate,$duedate,$loading,$description,$posterTeaching,$recordClassID,$recordSubjectID, $courseID, $courseAssignmentID,$AttachmentPath,$handinRequired,$collectRequired,$typeName) = $row;
                                  $title = intranet_wordwrap($title,20,"\n",1);
                                  if($typeName!="" && $this->useHomeworkType) $title = "[".$typeName."] ".$title;
                                  $x .= "<TR><TD>$count</td><td><A HREF='javascript:fe_view_homework_detail($hid)'>$title</A>\n";
                                  if ($description!="")
                                      $x .= "<img src=\"$hwImagePath/icon_alt.gif\" alt=\"$description\"> ";
                                  if ($AttachmentPath!="")
                                                  $x .=" <img src=\"$image_path/icon_attachment.gif\" alt=\"$i_AnnouncementAttachment\"> ";

                                  if ($this->useHomeworkHandin && $handinRequired && $this->hasAccessHandinListRight($uid,$hid)){
                                          $temp = $this->rs;
                                          $x .= $this->returnHandinListLink($hid);
                                          $this->rs = $temp;
                                      }

                                  if ($courseID != "" && $courseAssignmentID != "")
                                  {
                                      $x .= $this->returneClassIcon();
                                  }

                                  $editAllowed = false;
                                  if ($courseID != "" && $courseAssignmentID != "")
                                  {
                                  }
                                  else if ($match)
                                  {
                                      $editAllowed = true;
                                  }
                                  else if ($posterTeaching != 1 && $this->hasTeachingSubject($recordClassID,$recordSubjectID))
                                  {
                                       $editAllowed = true;
                                  }

                                  if ($editAllowed)
                                  {
                                      $x .= $this->returnEditDeleteLink($hid);
                                  }
                                  $x .= "</TD>\n";
                                  $subject = intranet_wordwrap($subject,15,"\n",1);
                                  $x .= "<TD>$subject</TD>\n";
                                  $x .= "<TD>$class</TD>\n";
                                  $x .= "<td>$startdate</td>\n";
                                  $x .= "<td>$duedate</td>\n";
                                  if ($loading ==0)
                                  {
                                      $tload = $i_Homework_minimum;
                                  }
                                  else if ($loading <= 16)
                                   {
                                           $tload = $loading/2;
                                   }
                                   else
                                   {
                                           $tload = "$i_Homework_morethan 8";
                                   }
                                  $x .= "<TD>$tload</TD>";
                                  if($this->useHomeworkCollect)
                                  {
	                                  $x .= "<TD>".($collectRequired==1?$i_general_yes:$i_general_no)."</TD>";
                                  }
                                  $x .= "</TR>\n";
									
                                  if($this->useHomeworkCollect)
                                  {
	                                  $x .= $this->getLineSeparation(8);
                                  }
                                  else
                                  {
	                                  $x .= $this->getLineSeparation(7);
                                  }
                            }

                        }
                }
                else
                {
                        $x .= "<tr><td align=center colspan=7>".$this->no_msg."</td></tr>";
                }
                $x .= "</TABLE></td></tr>";
                if($this->db_num_rows()<>0)
                {
                   $inside = "<table width=525><tr><td width=* bgcolor=#CEECE6 style='vertical-align:middle' align=center>".$this->navigation()."</td></tr></table>\n";
                   $x .= "<td width=126></td><td align=center colspan=".$this->no_col.">$inside</td><td width=66></td></tr>\n";
                }
                $x .= "</table>";

                $x.= "<input type=hidden name=field value=".$this->field.">\n";
                $x.= "<input type=hidden name=order value=".$this->order.">\n";
                $x.= "<input type=hidden name=pageNo value=1>\n";
                $x.= "<input type=hidden name=teacherID value=$tid>\n";
                $this->db_free_result();
                return $x;
        }
        function displayTeaching($teaching,$tid, $uid)
        {
                global $i_Homework_morethan,$i_Homework_hours,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_lastModified;
                 global $image_path, $intranet_session_language,$i_Homework_teacher_name,$i_Homework_minimum,$i_Homework_Group;
                 global $i_Homework_Poster,$i_general_no,$i_general_yes,$i_Homework_Collected_By_Class_Teacher;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x = "";

                $sorting_fields = array("a.Title","b.SubjectName","d.Title","c.EnglishName","a.StartDate","a.DueDate","a.Loading","a.CollectRequired");
                if ($this->field=="" || $this->field >= count($sorting_fields))
                    $this->field = 4;

//                $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading, a.Title, a.Description, CONCAT(c.LastName, ' ' ,c.FirstName)";

                if ($this->order=="" || ($this->order!=0))
                    $this->order = 1;
                if ($this->pageNo==""|| ($this->pageNo==0))
                    $this->pageNo =1;
                $order_str = ($this->order==0) ? " ASC" : " DESC";
                $name_field = getNameFieldWithClassNumberByLang("c.");
                $fields = "a.HomeworkID, a.Title, b.SubjectName, d.Title,$name_field, a.StartDate,
                           a.DueDate, a.Loading, a.Description,a.PosterUserID, c.Teaching, a.ClassGroupID, a.SubjectID
                           , e.CourseID, e.CourseAssignmentID,a.AttachmentPath,a.HandinRequired,a.CollectRequired,f.TypeName";
                $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                             LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                             LEFT OUTER JOIN INTRANET_GROUP as d ON a.ClassGroupID = d.GroupID
                             LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                             LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as f ON a.TypeID = f.TypeID
                             ";
                $conds = "";
                $delim = "";
                for ($i=0; $i<sizeof($teaching); $i++)
                {
                     list($sid, $subject, $cid, $class, $gid) = $teaching[$i];
                     if ($gid != "")
                     {
                         $conds .= "$delim (a.SubjectID = $sid AND a.ClassGroupID = $gid)";
                         $delim = " OR";
                     }
                }
                #$conds = "a.PosterUserID = $tid";
                $sql = "SELECT $fields FROM $dbtables WHERE $conds";
                $sql .= " ORDER BY " . $sorting_fields[$this->field] . $order_str;
                $n_start = ($this->pageNo-1)*$this->page_size;
                $this->rs = $this->db_db_query($sql);
                $x .= "<table width=717 border=0 cellspacing=0 cellpadding=5 background=/images/homeworklist/notebooktbbg.gif class=h1>\n";
                $x .= "<tr><td colspan=3><table width=100%>\n";
                $x .= "<tr><td width=30>&nbsp;</td>\n<td width=162>&nbsp;</td>\n";
                $x .= "<td width=90>&nbsp;</td>\n<td width=100>&nbsp;</td>\n<td width=80>&nbsp;</td><td width=90>&nbsp;</td>\n";
                $x .= "<td width=90>&nbsp;</td>\n<td width=75>&nbsp;</td>\n</tr>";
                $i =0;

                if($this->rs && $this->db_num_rows()!=0){
                        $x .= "<tr><td>#</td>\n";
                        $x .= "<TD>".$this->column(0,$i_Homework_title)."</TD>\n";
                        $x .= "<TD>".$this->column(1,$i_Homework_subject)."</TD>\n";
                        $x .= "<TD>".$this->column(2,"$i_Homework_class/$i_Homework_Group")."</TD>\n";
                        $x .= "<TD>".$this->column(3,$i_Homework_Poster)."</TD>\n";
                        $x .= "<TD>".$this->column(4,$i_Homework_startdate)."</TD>\n";
                        $x .= "<TD>".$this->column(5,$i_Homework_duedate)."</TD>\n";
                        $x .= "<TD>".$this->column(6,"$i_Homework_loading ($i_Homework_hours)")."</TD>\n";
                        
                        if($this->useHomeworkCollect)
                        {
	                        $x .= "<TD>".$this->column(7,"$i_Homework_Collected_By_Class_Teacher")."</TD>\n";
	                        $x .= $this->getLineSeparation(9);
                        }
                        else
                        {
                        	$x .= $this->getLineSeparation(8);
                    	}
                    	$x.="</TR>";
                        if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                           #$match = ($tid==$uid);

                           while($row = $this->db_fetch_array()){
                                  $i++;
                                  if($i>$this->page_size) break;
                                  $count = $n_start+$i;
                                  list($hid,$title,$subject,$class,$name,$startdate,$duedate,$loading,$description,$posterID,$posterTeaching,$recordClassID,$recordSubjectID, $courseID, $courseAssignmentID,$AttachmentPath,$handinRequired,$collectRequired,$typeName) = $row;
                                  $title = intranet_wordwrap($title,20,"\n",1);
                                  if($typeName!="" && $this->useHomeworkType)
                                          $title="[$typeName] $title";
                                  $x .= "<TR><TD>$count</td><td><A HREF='javascript:fe_view_homework_detail($hid)'>$title</A>\n";
                                  if ($description!="")
                                      $x .= "<img src=\"$hwImagePath/icon_alt.gif\" alt=\"$description\"> ";

                                  if ($AttachmentPath!="")
                                               $x .=" <img src=\"$image_path/icon_attachment.gif\" alt=\"$i_AnnouncementAttachment\"> ";
                                                                  if ($this->useHomeworkHandin && $handinRequired && $this->hasAccessHandinListRight($uid,$hid)){
                                                                          $temp = $this->rs;
                                          $x .= $this->returnHandinListLink($hid);
                                          $this->rs = $temp;
                                      }
                                  if ($courseID != "" && $courseAssignmentID != "")
                                  {
                                      $x .= $this->returneClassIcon();
                                  }
                                  $editAllowed = false;
                                  if ($courseID != "" && $courseAssignmentID != "")
                                  {
                                  }
                                  else if ($uid == $posterID)
                                  {
                                      $editAllowed = true;
                                  }
                                  else if ($posterTeaching != 1 && $this->hasTeachingSubject($recordClassID,$recordSubjectID))
                                  {
                                       $editAllowed = true;
                                  }

                                  if ($editAllowed)
                                  {
                                          $x .= $this->returnEditDeleteLink($hid);
                                  }
                                  $x .= "</TD>\n";
                                  $subject = intranet_wordwrap($subject,15,"\n",1);
                                  $x .= "<TD>$subject</TD>\n";
                                  $x .= "<TD>$class</TD>\n";
                                  $x .= "<TD>$name</TD>\n";
                                  $x .= "<td>$startdate</td>\n";
                                  $x .= "<td>$duedate</td>\n";
                                  if ($loading ==0)
                                  {
                                      $tload = $i_Homework_minimum;
                                  }
                                  else if ($loading <= 16)
                                   {
                                           $tload = $loading/2;
                                   }
                                   else
                                   {
                                           $tload = "$i_Homework_morethan 8";
                                   }
                                  $x .= "<TD>$tload</TD>";
                                  if($this->useHomeworkCollect)
                                  {
	                              	$x .= "<TD>".($collectRequired==1?$i_general_yes:$i_general_no)."</TD>"; 	   
                                  }
                                  $x .= "</TR>\n";

                                  if($this->useHomeworkCollect)
                                  {
	                                  $x .= $this->getLineSeparation(9);
                                  }
                                  else
                                  {
                                  	$x .= $this->getLineSeparation(8);
                              	  }
                            }

                        }
                }
                else
                {
                        $x .= "<tr><td align=center colspan=9>".$this->no_msg."</td></tr>";
                }
                $x .= "</TABLE></td></tr>";
                if($this->db_num_rows()<>0)
                {
                   $inside = "<table width=525><tr><td width=* bgcolor=#CEECE6 style='vertical-align:middle' align=center>".$this->navigation()."</td></tr></table>\n";
                   $x .= "<td width=126></td><td align=center colspan=".$this->no_col.">$inside</td><td width=66></td></tr>\n";
                }
                $x .= "</table>";

                $x.= "<input type=hidden name=field value=".$this->field.">\n";
                $x.= "<input type=hidden name=order value=".$this->order.">\n";
                $x.= "<input type=hidden name=pageNo value=1>\n";
                $x.= "<input type=hidden name=teacherID value=$tid>\n";
                $this->db_free_result();
                return $x;
        }
		function displayCollect($collect,$tid, $uid)
        {
                global $i_Homework_morethan,$i_Homework_hours,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_lastModified;
                 global $image_path, $intranet_session_language,$i_Homework_teacher_name,$i_Homework_minimum,$i_Homework_Group;
                 global $i_Homework_Poster,$i_Homework_Collected_By_Class_Teacher,$i_general_no,$i_general_yes;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x = "";
				
                $sorting_fields = array("a.Title","b.SubjectName","d.Title","c.EnglishName","a.StartDate","a.DueDate","a.Loading");
                if ($this->field=="" || $this->field >= count($sorting_fields))
                    $this->field = 4;

//                $fields = "a.HomeworkID, b.SubjectName, a.PosterUserID,a.StartDate, a.DueDate, a.Loading, a.Title, a.Description, CONCAT(c.LastName, ' ' ,c.FirstName)";

                if ($this->order=="" || ($this->order!=0))
                    $this->order = 1;
                if ($this->pageNo==""|| ($this->pageNo==0))
                    $this->pageNo =1;
                $order_str = ($this->order==0) ? " ASC" : " DESC";
                $name_field = getNameFieldWithClassNumberByLang("c.");
                $fields = "a.HomeworkID, a.Title, b.SubjectName, d.Title,$name_field, a.StartDate,
                           a.DueDate, a.Loading, a.Description,a.PosterUserID, c.Teaching, a.ClassGroupID, a.SubjectID
                           , e.CourseID, e.CourseAssignmentID,a.AttachmentPath,a.HandinRequired,a.CollectRequired,f.TypeName";
                $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                             LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                             LEFT OUTER JOIN INTRANET_GROUP as d ON a.ClassGroupID = d.GroupID
                             LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                             LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as f ON a.TypeID = f.TypeID
                             ";
                $conds = "";
                $delim = "";
                
                if($collect==0)
                	$conds = "(a.CollectRequired=$collect or a.CollectRequired='' or a.CollectRequired is null)";
                $conds = "(a.CollectRequired=$collect)";
                #$conds = "a.PosterUserID = $tid";
                $sql = "SELECT $fields FROM $dbtables WHERE $conds";
                $sql .= " ORDER BY " . $sorting_fields[$this->field] . $order_str;
                
                $n_start = ($this->pageNo-1)*$this->page_size;
                $this->rs = $this->db_db_query($sql);
                $x .= "<table width=717 border=0 cellspacing=0 cellpadding=5 background=/images/homeworklist/notebooktbbg.gif class=h1>\n";
                $x .= "<tr><td colspan=3><table width=100%>\n";
                $x .= "<tr><td width=30>&nbsp;</td>\n<td width=162>&nbsp;</td>\n";
                $x .= "<td width=90>&nbsp;</td>\n<td width=100>&nbsp;</td>\n<td width=80>&nbsp;</td><td width=90>&nbsp;</td>\n";
                $x .= "<td width=90>&nbsp;</td>\n<td width=75>&nbsp;</td>\n</tr>";
                $i =0;

                if($this->rs && $this->db_num_rows()!=0){
                        $x .= "<tr><td>#</td>\n";
                        $x .= "<TD>".$this->column(0,$i_Homework_title)."</TD>\n";
                        $x .= "<TD>".$this->column(1,$i_Homework_subject)."</TD>\n";
                        $x .= "<TD>".$this->column(2,"$i_Homework_class/$i_Homework_Group")."</TD>\n";
                        $x .= "<TD>".$this->column(3,$i_Homework_Poster)."</TD>\n";
                        $x .= "<TD>".$this->column(4,$i_Homework_startdate)."</TD>\n";
                        $x .= "<TD>".$this->column(5,$i_Homework_duedate)."</TD>\n";
                        $x .= "<TD>".$this->column(6,"$i_Homework_loading ($i_Homework_hours)")."</TD>";
                        $x .= "<TD>".$this->column(7,"$i_Homework_Collected_By_Class_Teacher")."</TD>";
                        $x.="</TR>\n";
                        
                        $x .= $this->getLineSeparation(9);
                        if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                           #$match = ($tid==$uid);

                           while($row = $this->db_fetch_array()){
                                  $i++;
                                  if($i>$this->page_size) break;
                                  $count = $n_start+$i;
                                  list($hid,$title,$subject,$class,$name,$startdate,$duedate,$loading,$description,$posterID,$posterTeaching,$recordClassID,$recordSubjectID, $courseID, $courseAssignmentID,$AttachmentPath,$handinRequired,$collectRequired,$typeName) = $row;
                                  $title = intranet_wordwrap($title,20,"\n",1);
                                  if($typeName!="" && $this->useHomeworkType)
                                          $title="[$typeName] $title";
                                  $x .= "<TR><TD>$count</td><td><A HREF='javascript:fe_view_homework_detail($hid)'>$title</A>\n";
                                  if ($description!="")
                                      $x .= "<img src=\"$hwImagePath/icon_alt.gif\" alt=\"$description\"> ";

                                  if ($AttachmentPath!="")
                                               $x .=" <img src=\"$image_path/icon_attachment.gif\" alt=\"$i_AnnouncementAttachment\"> ";
                                                                  if ($this->useHomeworkHandin && $handinRequired && $this->hasAccessHandinListRight($uid,$hid)){
                                                                          $temp = $this->rs;
                                          $x .= $this->returnHandinListLink($hid);
                                          $this->rs = $temp;
                                      }
                                  if ($courseID != "" && $courseAssignmentID != "")
                                  {
                                      $x .= $this->returneClassIcon();
                                  }
                                  $editAllowed = false;
                                  if ($courseID != "" && $courseAssignmentID != "")
                                  {
                                  }
                                  else if ($uid == $posterID)
                                  {
                                      $editAllowed = true;
                                  }
                                  else if ($posterTeaching != 1 && $this->hasTeachingSubject($recordClassID,$recordSubjectID))
                                  {
                                       $editAllowed = true;
                                  }

                                  if ($editAllowed)
                                  {
                                          $x .= $this->returnEditDeleteLink($hid);
                                  }
                                  $x .= "</TD>\n";
                                  $subject = intranet_wordwrap($subject,15,"\n",1);
                                  $x .= "<TD>$subject</TD>\n";
                                  $x .= "<TD>$class</TD>\n";
                                  $x .= "<TD>$name</TD>\n";
                                  $x .= "<td>$startdate</td>\n";
                                  $x .= "<td>$duedate</td>\n";
                                  if ($loading ==0)
                                  {
                                      $tload = $i_Homework_minimum;
                                  }
                                  else if ($loading <= 16)
                                   {
                                           $tload = $loading/2;
                                   }
                                   else
                                   {
                                           $tload = "$i_Homework_morethan 8";
                                   }
                                  $x .= "<TD>$tload</TD>";
                                  $x .= "<td>".($collectRequired==1?$i_general_yes:$i_general_no)."</td>\n";
                                  $x .= "</TR>\n";

                                  $x .= $this->getLineSeparation(9);
                            }

                        }
                }
                else
                {
                        $x .= "<tr><td align=center colspan=9>".$this->no_msg."</td></tr>";
                }
                $x .= "</TABLE></td></tr>";
                if($this->db_num_rows()<>0)
                {
                   $inside = "<table width=525><tr><td width=* bgcolor=#CEECE6 style='vertical-align:middle' align=center>".$this->navigation()."</td></tr></table>\n";
                   $x .= "<td width=126></td><td align=center colspan=".$this->no_col.">$inside</td><td width=66></td></tr>\n";
                }
                $x .= "</table>";

                $x.= "<input type=hidden name=field value=".$this->field.">\n";
                $x.= "<input type=hidden name=order value=".$this->order.">\n";
                $x.= "<input type=hidden name=pageNo value=1>\n";
                $x.= "<input type=hidden name=teacherID value=$tid>\n";
                $this->db_free_result();
                return $x;
        }
        
        function displayDetail($homeworkID)
        {
                global $intranet_session_language,$image_path,$i_Homework_unauthorized,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_hours,$i_Homework_morethan, $i_Homework_lastModified,$i_Homework_404;
                global $i_Homework_list, $i_Homework_detail,$i_Homework_teacher_name,$i_Homework_minimum,$i_Homework_Type;
                global $i_Homework_Group,$i_Homework_IncludingClass,$i_Homework_Poster;
                global $file_path, $i_Homework_Collect_Required,$i_Homework_Handin_Required, $ec_iPortfolio;

                $username_field = getNameFieldWithClassNumberByLang("c.");

                $hwImagePath = "$image_path/homework/$intranet_session_language/hw";

                $fields = "a.Title, a.Description,b.SubjectName,d.Title,
                           IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field),
                           a.Loading,a.StartDate, a.DueDate, a.ClassGroupID,a.RecordType
                           ,e.CourseID, e.CourseAssignmentID, a.AttachmentPath,h.TypeName, a.HandinRequired, a.CollectRequired ";
                $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                             LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                             LEFT OUTER JOIN INTRANET_GROUP as d ON a.ClassGroupID = d.GroupID
                             LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                             LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as h ON a.TypeID = h.TypeID";
                
                $homeworkID = IntegerSafe($homeworkID);
                $conds = "HomeworkID = '$homeworkID'";
                
                $sql = "SELECT $fields FROM $dbtables where $conds";

                $x = "";
                $x .= $this->displayViewTitleBar($i_Homework_detail);
                $x .= "<table width=\"517\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" background=\"$hwImagePath/table_bg2.gif\">\n";


                if ($homeworkID!="")
                {
                    $row = $this->returnArray($sql,14);
                }

                if($row)
                {
                   $x .= "<tr><td width=\"26\">&nbsp;</td>
                   <td width=\"100\" align=right valign=top>&nbsp;</td>
                   <td width=\"20\" valign=top align=left>&nbsp;</td>
                   <td width=\"345\" align=left valign=top>&nbsp;</td>
                   <td width=\"26\">&nbsp;</td>
                   </tr>\n";

                        list($title,$desp,$subject,$classname,$teacher,$loading,$start,$due,$groupID,$type, $courseID, $courseAssignmentID, $AttachmentPath,$TypeName,$handinRequired, $collectRequired) = $row[0];
                                                $title = ($this->useHomeworkType && $TypeName != "") ? "[".$TypeName."] ".$title : $title;
                        $title = intranet_wordwrap ($title,30,"\n",1);
                        $subject = intranet_wordwrap ($subject,30,"\n",1);
                        $teacher = intranet_wordwrap ($teacher,30,"\n",1);
                        if ($classID == 0 || $classID ==$groupID)
                        {
                            if ($courseID!="" && $courseAssignmentID!="")
                            {
                                $str_eclass = $this->returneClassIcon();
                            }
                                $x .= "<tr>
                                    <td width=\"26\">&nbsp;</td>
                                    <td width=\"100\" align=right valign=top><font size=2>$i_Homework_title</font></td>
                                    <td width=\"20\" valign=top align=left><font size=2>:</font></td>
                                    <td width=\"345\" align=left valign=top>$title $str_eclass</td>
                                    <td width=\"26\">&nbsp;</td>
                                  </tr><tr><td colspan=5></td></tr>\n";
/*
                                # Check it is eClass Assignment or not
                                global $intranet_db, $eclass_db;
                                $sql = "SELECT a.CourseID, b.course_code,b.course_name
                                        FROM $intranet_db.INTRANET_COURSE_HOMEWORK as a
                                             LEFT OUTER JOIN $eclass_db.course as b
                                             ON a.CourseID = b.course_id
                                        WHERE a.IntranetHomeworkID = $homeworkID";
                                $temp = $this->returnArray($sql,3);
                                list ($courseid,$coursecode,$coursename) = $temp[0];
                                if ($courseid == "")
                                {
                                    $desp = $this->convertAllLinks2($desp,30);
                                    $desp = nl2br($desp);
                                }
                                else
                                {
                                    global $i_Homework_eClassAssignment;
                                    $desp = "$i_Homework_eClassAssignment"."<a href=javascript:fe_eclass($courseid)>$coursecode $coursename</a>";
                                }
                                */
                                //$desp = $this->convertAllLinks2(nl2br($desp),40);
                                $desp = nl2br($desp);

                                $x .= "<tr>
                                    <td width=\"26\">&nbsp;</td>
                                    <td width=\"100\" align=right valign=top><font size=2>$i_Homework_description</font></td>
                                    <td width=\"20\" valign=top align=left><font size=2>:</font></td>
                                    <td width=\"345\" align=left valign=top>$desp</td>
                                    <td width=\"26\">&nbsp;</td>
                                  </tr><tr><td colspan=5></td></tr>\n";

                                $x .= "<tr>
                                    <td width=\"26\">&nbsp;</td>
                                    <td width=\"100\" align=right valign=top><font size=2>$i_Homework_subject</font></td>
                                    <td width=\"20\" valign=top align=left><font size=2>:</font></td>
                                    <td width=\"345\" align=left valign=top>$subject</td>
                                    <td width=\"26\">&nbsp;</td>
                                  </tr><tr><td colspan=5></td></tr>\n";
								if (trim($TypeName)!="")
								{
	                                $x .= "<tr>
	                                    <td width=\"26\">&nbsp;</td>
	                                    <td width=\"100\" align=right valign=top><font size=2>$i_Homework_Type</font></td>
	                                    <td width=\"20\" valign=top align=left><font size=2>:</font></td>
	                                    <td width=\"345\" align=left valign=top>$TypeName</td>
	                                    <td width=\"26\">&nbsp;</td>
	                                  </tr><tr><td colspan=5></td></tr>\n";
								}
                                if ($type == 1)
                                {
                                    $x .= "<tr>
                                            <td width=\"26\">&nbsp;</td>
                                            <td width=\"100\" align=right valign=top><font size=2>$i_Homework_class</font></td>
                                            <td width=\"20\" valign=top align=left><font size=2>:</font></td>
                                            <td width=\"345\" align=left valign=top>$classname</td>
                                            <td width=\"26\">&nbsp;</td>
                                           </tr><tr><td colspan=5></td></tr>\n";
                                }
                                else
                                {
                                    $x .= "<tr>
                                            <td width=\"26\">&nbsp;</td>
                                            <td width=\"100\" align=right valign=top><font size=2>$i_Homework_Group</font></td>
                                            <td width=\"20\" valign=top align=left><font size=2>:</font></td>
                                            <td width=\"345\" align=left valign=top>$classname</td>
                                            <td width=\"26\">&nbsp;</td>
                                           </tr><tr><td colspan=5></td></tr>\n";
                                    $sql = "SELECT a.Title FROM INTRANET_GROUP as a, INTRANET_HOMEWORK_SPECIAL as b WHERE a.GroupID = b.ClassGroupID AND b.HomeworkID = '$homeworkID'";
                                    $groups = $this->returnVector($sql);
                                    $groupList = implode(", ",$groups);
                                    $x .= "<tr>
                                            <td width=\"26\">&nbsp;</td>
                                            <td width=\"100\" align=right valign=top><font size=2>$i_Homework_IncludingClass</font></td>
                                            <td width=\"20\" valign=top align=left><font size=2>:</font></td>
                                            <td width=\"345\" align=left valign=top>$groupList</td>
                                            <td width=\"26\">&nbsp;</td>
                                           </tr><tr><td colspan=5></td></tr>\n";
                                }

                                $x .= "<tr>
                                    <td width=\"26\">&nbsp;</td>
                                    <td width=\"100\" align=right valign=top><font size=2>$i_Homework_Poster</font></td>
                                    <td width=\"20\" valign=top align=left><font size=2>:</font></td>
                                    <td width=\"345\" align=left valign=top>$teacher</td>
                                    <td width=\"26\">&nbsp;</td>
                                  </tr><tr><td colspan=5></td></tr>\n";
                                if ($loading ==0)
                                {
                                    $tload = $i_Homework_minimum;
                                }
                                else if ($loading <= 16)
                                 {
                                         $tload = $loading/2;
                                 }
                                 else
                                 {
                                         $tload = "$i_Homework_morethan 8";
                                 }

                                $x .= "<tr>
                                    <td width=\"26\">&nbsp;</td>
                                    <td width=\"100\" align=right valign=top><font size=2>$i_Homework_loading</font></td>
                                    <td width=\"20\" valign=top align=left><font size=2>:</font></td>
                                    <td width=\"345\" align=left valign=top>$tload $i_Homework_hours</td>
                                    <td width=\"26\">&nbsp;</td>
                                  </tr><tr><td colspan=5></td></tr>\n";
                                $x .= "<tr>
                                    <td width=\"26\">&nbsp;</td>
                                    <td width=\"100\" align=right valign=top><font size=2>$i_Homework_startdate</font></td>
                                    <td width=\"20\" valign=top align=left><font size=2>:</font></td>
                                    <td width=\"345\" align=left valign=top>$start</td>
                                    <td width=\"26\">&nbsp;</td>
                                  </tr><tr><td colspan=5></td></tr>\n";
                                $x .= "<tr>
                                    <td width=\"26\">&nbsp;</td>
                                    <td width=\"100\" align=right valign=top><font size=2>$i_Homework_duedate</font></td>
                                    <td width=\"20\" valign=top align=left><font size=2>:</font></td>
                                    <td width=\"345\" align=left valign=top>$due</td>
                                    <td width=\"26\">&nbsp;</td>
                                  </tr><tr><td colspan=5></td></tr>\n";
								if($handinRequired)
								{
	                                $x .= "<tr>
	                                    <td width=\"26\">&nbsp;</td>
	                                    <td width=\"100\" align=right valign=top><font size=2>$i_Homework_Handin_Required</font></td>
	                                    <td width=\"20\" valign=top align=left><font size=2>:</font></td>
	                                    <td width=\"345\" align=left valign=top>".$ec_iPortfolio['yes']."</td>
	                                    <td width=\"26\">&nbsp;</td>
	                                  </tr><tr><td colspan=5></td></tr>\n";
                              	}
                              	if($collectRequired)
                              	{
	                                 $x .= "<tr>
	                                    <td width=\"26\">&nbsp;</td>
	                                    <td width=\"100\" align=right valign=top><font size=2>$i_Homework_Collect_Required</font></td>
	                                    <td width=\"20\" valign=top align=left><font size=2>:</font></td>
	                                    <td width=\"345\" align=left valign=top>".$ec_iPortfolio['yes']."</td>
	                                    <td width=\"26\">&nbsp;</td>
	                                  </tr><tr><td colspan=5>&nbsp;</td></tr>\n"; 
	                           }   
                                # show attachments:
                               if($AttachmentPath!=""){
                                                               $hasImage=false;
                                                               $hasOther=false;
                                                               $displayOther = "<tr><td width=\"26\">&nbsp;</td><td colspan=4>";
                                                $displayImage = "<Tr><td width=\"26\">&nbsp;</td><Td colspan=4>";
                                                $path = "$file_path/file/homework/".$AttachmentPath.$homeworkID;
                                                include_once("libfiletable.php");
                                                            $templf = new libfiletable("", $path,0,0,"");
                                                            $files = $templf->files;
                                                            while (list($key, $value) = each($files)) {
                                                                   $target_filepath = "$path/".$files[$key][0];
                                                                   if (isImage($target_filepath))
                                                                   {
                                                                       $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                                                                       $url = intranet_handle_url($url);

                                                                                $size = GetImageSize($target_filepath);
                                                                                list($width, $height, $type, $attr) = $size;
                                                                                if ($width > 412 || $height > 550){
                                                                                    $image_html_tag = "width=412 height=550";
                                                                                }
                                                                                else{
                                                                                    $image_html_tag = "";
                                                                                }
                                                                                $displayImage .= "<a href=\"$intranet_httppath$url\" target=_blank onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\"><img border=0 src=\"$intranet_httppath$url\" $image_html_tag></a>";
                                                                                $displayImage .= "<br>\n";
                                                                                $hasImage = true;
                                                                       }else{
                                                                                $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                                                                               $url = intranet_handle_url($url);
                                                                   $displayOther .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                                                                                $displayOther .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                                                                                $displayOther .= " (".ceil($files[$key][1]/1000)."Kb)";
                                                                                $displayOther .= "<br>\n";
                                                                                $hasOther = true;
                                                                           }
                                                            }
                                                                                $displayImage .="</td></tr>";
                                                                                $displayOther .="</td></tr>";
                                    }

                                # end of attachments
                                if($hasImage)
                                        $x.= $displayImage;
                                if($hasOther)
                                        $x.= $displayOther;
                                $x .= "<tr><td colspan=5></td></tr>\n";
                        }
                        else
                        {
                                $x .= "<TR><TD colspan=5 align=center>$i_Homework_unauthorized</TD></TR>";
                        }
                }
                else
                {
                        $x .= "<TR><TD align=center>$i_Homework_404</TD></TR>";
                }
                $x .= "</table>";
                $x .= "<table width=\"517\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" background=\"$hwImagePath/table_bg2.gif\">\n";
                $x .= "<tr><td>&nbsp;</td></tr><tr><td><img src=\"$hwImagePath/bar_bottom.gif\" width=\"517\" height=\"50\"></td>\n";
                $x .= "</tr></table>\n";

                return $x;
        }

        function in_arrayField ($needle, $array, $atField)
        {
                 for ($i=0; $i<sizeof($array); $i++)
                 {
                      if ($needle == $array[$i][$atField])
                          return true;
                 }
                 return false;
        }


        function displayAllClasses($flag)
        {
                 global $image_path,$intranet_session_language;
                 # 0 - normal class, 1 - special
                 $sql = "SELECT GroupID FROM INTRANET_CLASS WHERE GroupID IS NOT NULL";
                 $groups = $this->returnVector($sql);
                 if (sizeof($groups)!=0)
                 {
                     $group_list = implode(",",$groups);
                     if ($flag == 1)
                     {
                         $conds = "AND GroupID NOT IN ($group_list)";
                     }
                     else if ($flag == 0)
                     {
                         $conds = "AND GroupID IN ($group_list)";
                     }

                 }
                 $sql = "SELECT GroupID, Title FROM INTRANET_GROUP WHERE RecordType = 3 $conds order by Title asc";
                 $array = $this->returnArray($sql,2);
                 $length = $this->getMaxTitleLength($array);
                 if ($length > 15)
                 {
                     $num = 2;
                 }
                 else
                 {
                     $num = 5;
                 }
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x = "<table width=717 border=0 cellspacing=0 cellpadding=10 background=\"/images/homeworklist/notebooktbbg.gif\">\n";
                 $x .= "<tr><td>&nbsp;</td><td width=700 align=center>\n";
                 $x .= "<table width=90% border=1 cellspacing=0 cellpadding=4 bordercolordark=#EFFFF7 bordercolorlight=#D0DBC4>\n";
                 $width = 100/$num;
                 for ($i=0; $i<sizeof($array); $i++)
                 {
                      list($id,$name) = $array[$i];
                      $chopped_name = intranet_wordwrap ($name,60/$num,"\n",1);
                      $alt = " title='$name'";
                      if ($i % $num == 0)
                      {
                          if ($i !=0) $x .= "</tr>";
                          $x .= "<tr valign=middle>\n";
                      }
                      $x .= "<td width=$width%><a $alt href=\"classview.php?classID=$id\">$chopped_name</a></td>\n";
                 }
                 $last = sizeof($array) % $num;
                 $empty = ($last==0? 0 : $num - $last);
                 for ($j=0; $j<$empty; $j++)
                 {
                      $x .= "<td>&nbsp;</td>";
                 }
                 $x .= "</tr>\n";
//                 $x .= "<tr><td colspan=7><img src=\"$hwImagePath/bar_bottom2_750.gif\" width=750 height=20></td></tr>\n";
                 $x .= "</table>\n";
                 $x .= "</td></tr></table>\n";
                 return $x;
        }

        function displaySmallAllClasses()
        {
                 global $image_path,$intranet_session_language;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 # Fix to normal class
                 $sql = "SELECT GroupID FROM INTRANET_CLASS WHERE GroupID IS NOT NULL";
                 $groups = $this->returnVector($sql);
                 if (sizeof($groups)!=0)
                 {
                     $group_list = implode(",",$groups);
                     $conds = "AND GroupID IN ($group_list)";
                 }

                 $sql = "SELECT GroupID, Title FROM INTRANET_GROUP WHERE RecordType = 3 $conds order by Title asc";
                 $array = $this->returnArray($sql,2);
                 $length = $this->getMaxTitleLength($array);
                 if ($length > 15)
                 {
                     $num = 2;
                 }
                 else
                 {
                     $num = 5;
                 }
                 $x = "<table width=517 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg2.gif\">\n";
                 $x .= "<tr><td>&nbsp;</td></tr>\n";
                 $x .= "<tr><td width=500 align=center><table align=center width=90% border=1 cellspacing=0 cellpadding=4 bordercolordark=#EFFFF7 bordercolorlight=#D0DBC4>\n";
                 $width = 100/$num;
                 for ($i=0; $i<sizeof($array); $i++)
                 {
                      list($id,$name) = $array[$i];
                      $chopped_name = intranet_wordwrap ($name,50/$num,"\n",1);
                          $alt = " title='$name'";

                      if ($i % $num == 0)
                      {
                          if ($i !=0) $x .= "</tr>\n";
                          $x .= "<tr valign=middle>\n";
                      }
                      $x .= "<td width=$width%><a $alt href=\"/home/homework/classview.php?classID=$id\">$chopped_name</a></td>\n";
                 }

                 $last = sizeof($array) % $num;
                 $empty = ($last==0? 0 : $num - $last);
                 for ($j=0; $j<$empty; $j++)
                 {
                      $x .= "<td>&nbsp;</td>";
                 }
                 $x .= "</tr>\n";
                 $x .= "</table></td></tr>";
                 $x .= "</table>\n";
                 $x .= "<table width=517 border=0 cellspacing=0 cellpadding=0><tr><td><img src=\"$hwImagePath/bar_bottom2.gif\" width=517 height=20></td></tr></table>\n";
                 return $x;
        }

        function displaySubject($sid,$uid)
        {
                global $i_Homework_morethan,$i_Homework_hours,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_lastModified;
                 global $image_path, $intranet_session_language,$i_Homework_teacher_name,$i_Homework_minimum,$i_Homework_Group;
                 global $i_Homework_Poster,$i_Homework_Collected_By_Class_Teacher,$i_general_yes,$i_general_no;
                 
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $username_field = getNameFieldWithClassNumberByLang("c.");
                 $x = "";

                $sorting_fields = array("a.Title","d.Title","c.EnglishName","a.StartDate","a.DueDate","a.Loading","a.CollectRequired");
                if ($this->field=="" || $this->field >= count($sorting_fields))
                    $this->field = 4;

                if ($this->order=="" || ($this->order!=0))
                    $this->order = 1;
                if ($this->pageNo==""|| ($this->pageNo==0))
                    $this->pageNo =1;
                $order_str = ($this->order==0) ? " ASC" : " DESC";
                $fields = "a.HomeworkID, a.Title,
                           IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$username_field),
                           d.Title, a.StartDate, a.DueDate, a.Loading, a.Description,
                           a.PosterUserID,c.Teaching, a.ClassGroupID, a.SubjectID,
                           e.CourseID, e.CourseAssignmentID,a.AttachmentPath,a.HandinRequired,a.CollectRequired,f.TypeName";
                $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                             LEFT OUTER JOIN INTRANET_USER as c ON a.PosterUserID = c.UserID
                             LEFT OUTER JOIN INTRANET_GROUP as d ON a.ClassGroupID = d.GroupID
                             LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                             LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as f ON a.TypeID = f.TypeID
                             ";
                $conds = "a.SubjectID = $sid";
                $sql = "SELECT $fields FROM $dbtables WHERE $conds";
                $sql .= " ORDER BY " . $sorting_fields[$this->field] . $order_str;
                $n_start = ($this->pageNo-1)*$this->page_size;
                $this->rs = $this->db_db_query($sql);
                $x .= "<table width='717' border=0 cellspacing=0 cellpadding=5 background=/images/homeworklist/notebooktbbg.gif class=h1>\n";
                $x .= "<tr><td colspan=3><table width='100%' border='0'>\n";
                $x .= "<tr><td >&nbsp;</td>\n<td>&nbsp;</td>\n";
                $x .= "<td >&nbsp;</td>\n<td >&nbsp;</td>\n<td >&nbsp;</td>\n";
                $x .= "<td >&nbsp;</td>\n<td >&nbsp;</td>\n";

                if($this->useHomeworkCollect)
                {
	                $x .= "<td >&nbsp;</td>\n";
                }
                $x .= "</tr>";
                /*$x .= "<tr><td width=30>&nbsp;</td>\n<td width=182>&nbsp;</td>\n";
                $x .= "<td width=90>&nbsp;</td>\n<td width=160>&nbsp;</td>\n<td width=90>&nbsp;</td>\n";
                $x .= "<td width=90>&nbsp;</td>\n<td width=75>&nbsp;</td>\n</tr>";
                */
                $i =0;
                if($this->rs && $this->db_num_rows()!=0){
                        $x .= "<tr><td>#</td>\n";
                        $x .= "<TD>".$this->column(0,$i_Homework_title)."</TD>\n";
                        $x .= "<TD>".$this->column(1,"$i_Homework_class/$i_Homework_Group")."</TD>\n";
                        $x .= "<TD>".$this->column(2,$i_Homework_Poster)."</TD>\n";
                        $x .= "<TD>".$this->column(3,$i_Homework_startdate)."</TD>\n";
                        $x .= "<TD>".$this->column(4,$i_Homework_duedate)."</TD>\n";
                        $x .= "<TD>".$this->column(5,"$i_Homework_loading ($i_Homework_hours)")."</TD>";
                        if($this->useHomeworkCollect)
                        {
                        	$x .= "<TD>".$this->column(6,$i_Homework_Collected_By_Class_Teacher)."</TD>";
                    	}
                    	$x.="</TR>\n";
                    	if($this->useHomeworkCollect)
                    	{
	                    	$x .= $this->getLineSeparation(8);
                    	}
                    	else
                    	{
                        	$x .= $this->getLineSeparation(7);
                    	}
                        if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                           while($row = $this->db_fetch_array()){
                                  $i++;
                                  if($i>$this->page_size) break;
                                  $count = $n_start+$i;
                                  list($hid,$title,$name,$class,$startdate,$duedate,$loading,$description,$posterID,$posterTeaching,$recordClassID,$recordSubjectID, $courseID, $courseAssignmentID,$AttachmentPath,$handinRequired,$collectRequired,$typeName) = $row;
                                  $title = intranet_wordwrap($title,20,"\n",1);
                                  if($typeName!="" && $this->useHomeworkType)
                                          $title="[$typeName] $title";
                                  $x .= "<TR><TD>$count</td><td><A HREF='javascript:fe_view_homework_detail($hid)'>$title</A>\n";
                                  if ($description != "")
                                      $x .= "<img src=\"$hwImagePath/icon_alt.gif\" alt=\"$description\"> ";
                                  if ($AttachmentPath!="")
                                               $x .=" <img src=\"$image_path/icon_attachment.gif\" alt=\"$i_AnnouncementAttachment\"> ";
                                                                  if ($this->useHomeworkHandin && $handinRequired && $this->hasAccessHandinListRight($uid,$hid)){
                                                                          $temp =$this->rs;
                                          $x .= $this->returnHandinListLink($hid);
                                          $this->rs = $temp;
                                      }
                                  if ($courseID!="" && $courseAssignmentID!="")
                                  {
                                      $x .= $this->returneClassIcon();
                                  }

                                  $editAllowed = false;
                                  if ($courseID!="" && $courseAssignmentID!="")
                                  {
                                  }
                                  else if ($uid == $posterID)
                                  {
                                      $editAllowed = true;
                                  }
                                  else if ($posterTeaching != 1 && $this->hasTeachingSubject($recordClassID,$recordSubjectID))
                                  {
                                       $editAllowed = true;
                                  }

                                  if ($editAllowed)
                                  {
                                          $x .= $this->returnEditDeleteLink($hid);
                                  }
                                  $x .= "</TD>\n";
                                  $x .= "<TD>$class</TD>\n";
                                  $name = intranet_wordwrap($name,15,"\n",1);
                                  $x .= "<TD>$name</TD>\n";
                                  $x .= "<td>$startdate</td>\n";
                                  $x .= "<td>$duedate</td>\n";
                                  if ($loading ==0)
                                  {
                                      $tload = $i_Homework_minimum;
                                  }
                                  else if ($loading <= 16)
                                   {
                                           $tload = $loading/2;
                                   }
                                   else
                                   {
                                           $tload = "$i_Homework_morethan 8";
                                   }
                                  $x .= "<TD>$tload</TD>";
                                  if($this->useHomeworkCollect)
                                  {
	                                  $x .= "<TD>".($collectRequired==1?$i_general_yes:$i_general_no)."</TD>";
                                  }
                                  $x .= "</TR>\n";
                                  if($this->useHomeworkCollect)
                                  {
                                  	$x .= $this->getLineSeparation(8);
                              	  }
                                  else
                                  {
	                               $x .= $this->getLineSeparation(7);   
                                  }
                            }

                        }
                }
                else
                {
                        $x .= "<tr><td align=center colspan=7>".$this->no_msg."</td></tr>";
                }
                $x .= "</TABLE></td></tr>";
                if($this->db_num_rows()<>0)
                {
                   $inside = "<table width=525><tr><td width=* bgcolor=#CEECE6 style='vertical-align:middle' align=center>".$this->navigation()."</td></tr></table>\n";
                   $x .= "<td width=126></td><td align=center colspan=".$this->no_col.">$inside</td><td width=66></td></tr>\n";
                }
                $x .= "</table>";

                $x.= "<input type=hidden name=field value=".$this->field.">\n";
                $x.= "<input type=hidden name=order value=".$this->order.">\n";
                $x.= "<input type=hidden name=pageNo value=1>\n";
                $x.= "<input type=hidden name=subjectID value=$sid>\n";
                $this->db_free_result();
                return $x;
        }

        function displayTeacherTabView($classID, $uid, $cList)
        {
                 global $image_path, $intranet_session_language;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $temp = "";
                 for ($i=0; $i<sizeof($cList); $i++)
                 {
                      list($id, $name) = $cList[$i];
                      $s = (($id==$classID || ($i==0 && $classID==""))? "1":"2");
                      if ($i % 4 ==0)
                      {
                          if ($i!=0) $temp .= "</tr>\n";
                          $x = "$temp$x";
                          $temp = "<tr>";
                      }
                      $latt = ($i%4==0? "width=15 align=right": "width=5");
                      $ratt = ($i%4==3? "width=18 align=left": "width=5");
                      $cwidth = ($i%4==3? 169: 166);

                      $temp .= "<td $latt><img src=\"$hwImagePath/tab_$s"."l.gif\" width=5 height=26></td>
    <td width=$cwidth align=center valign=middle STYLE=\"background-image: url($hwImagePath/tab_$s"."cell.gif);\"><a href=?classID=$id>$name</a></td>
    <td $ratt><img src=\"$hwImagePath/tab_$s"."r.gif\" width=5 height=26></td>\n";
                 }
                 if (sizeof($cList)%4!=0)
                        for ($j=sizeof($cList)%4; $j<4; $j++)
                        {
                              $cwidth = ($j%4==3? 169: 166);
                              $ratt = ($j==3? "width=18 align=left": "width=5");
                              $temp .= "<td width=5></td><td width=$cwidth></td><td $ratt></td>\n";
                        }
                 $temp .= "</tr>\n";
                 $x = "<table width=750 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg_popup.gif\">$temp$x</table>";
                 return $x;
        }

        function displaySmallTabView($classID, $uid, $cList)
        {
                 global $image_path, $intranet_session_language;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $temp = "";
                 for ($i=0; $i<sizeof($cList); $i++)
                 {
                      list($id, $name) = $cList[$i];
                      $chopped_name = chopword($name,10);
                      if ($name != $chopped_name)
                          $alt = " title='$name'";
                      else $alt = "";

                      $s = (($id==$classID || ($i==0 && $classID==""))? "1":"2");
                      if ($i % 4 ==0)
                      {
                          if ($i!=0) $temp .= "</tr>\n";
                          $x = "$temp$x";
                          $temp = "<tr>";
                      }
                      $latt = ($i%4==0? "width=12 align=right": "width=5");
                      $ratt = ($i%4==3? "width=18 align=left": "width=5");
                      $cwidth = ($i%4==3? 115: 114);

                      $temp .= "<td $latt><img src=\"$hwImagePath/tab_$s"."l.gif\" width=5 height=26></td>
    <td width=$cwidth align=center valign=middle STYLE=\"background-image: url($hwImagePath/tab_$s"."cell.gif);\"><a href=?classID=$id#homework$alt>$chopped_name</a></td>
    <td $ratt><img src=\"$hwImagePath/tab_$s"."r.gif\" width=5 height=26></td>\n";
                 }
                 if (sizeof($cList)%4!=0)
                        for ($j=sizeof($cList)%4; $j<4; $j++)
                        {
                              $ratt = ($j==3? "width=18 align=left": "width=5");
                              $cwidth = ($j==3? 115: 114);
                              $temp .= "<td width=5></td><td width=$cwidth></td><td $ratt></td>\n";
                        }
                 $temp .= "</tr>\n";
                 $x = "<table width=517 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg.gif\">$temp$x</table>";
                 return $x;
        }

        function displayTeacherClass($cid, $uid)
        {
                global $i_Homework_morethan,$i_Homework_hours,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_lastModified;
                 global $image_path, $intranet_session_language,$i_Homework_teacher_name,$i_Homework_today,$i_Homework_minimum;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x = "";

                $sorting_fields = array("a.Title","a.StartDate","a.DueDate","b.SubjectName","a.Loading");
                if ($this->field=="" || $this->field >= count($sorting_fields))
                    $this->field = 1;
                if ($this->order=="" || ($this->order!=1))
                    $this->order = 0;
                if ($this->pageNo==""|| ($this->pageNo==0))
                    $this->pageNo =1;
                $order_str = ($this->order==0) ? " ASC" : " DESC";

                $fields = "CONCAT(a.Title, if (a.StartDate=CURDATE(),' $i_Homework_today','')),
                                  a.StartDate, a.DueDate, b.SubjectName, a.Loading, a.HomeworkID,
                                  a.PosterUserID,a.Description, e.CourseID, e.CourseAssignmentID,a.AttachmentPath,f.TypeName";
                $dbtables = "INTRANET_HOMEWORK as a
                               LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                               LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID,
                               INTRANET_HOMEWORK_TYPE AS f

                             ";
                $conds = "a.ClassGroupID = $cid AND a.PosterUserID = $uid AND a.DueDate >= CURDATE() AND a.TypeID=f.TypeID";
                $sql = "SELECT $fields FROM $dbtables WHERE $conds";
                $sql .= " ORDER BY " . $sorting_fields[$this->field] . $order_str;
                $this->rs = $this->db_db_query($sql);

                $x .= "<table width=750 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg_popup2.gif\">\n";
                $x .= "<tr><td colspan=3><table width=100%>\n";
                $x .= "<tr><td width=38>&nbsp;</td>\n<td width=38>&nbsp;</td>\n<td width=230>&nbsp;</td>\n";
                $x .= "<td width=93>&nbsp;</td>\n<td width=93>&nbsp;</td>\n<td width=87>&nbsp;</td>\n";
                $x .= "<td width=80>&nbsp;</td>\n<td width=33>&nbsp;</td>\n<td width=33>&nbsp;</td></tr>";
                $i =0;
                $n_start = ($this->pageNo-1)*$this->page_size;
                if($this->rs && $this->db_num_rows()!=0){
                        $x .= "<tr><td></td><td>#</td>\n";
                        $x .= "<TD>".$this->column(0,$i_Homework_title)."</TD>\n";
                        $x .= "<TD>".$this->column(1,$i_Homework_startdate)."</TD>\n";
                        $x .= "<TD>".$this->column(2,$i_Homework_duedate)."</TD>\n";
                        $x .= "<TD>".$this->column(3,$i_Homework_subject)."</TD>\n";
                        $x .= "<TD>".$this->column(4,"$i_Homework_loading ($i_Homework_hours)")."</TD>\n";
                        $x .= "<td> &nbsp; </td><td> &nbsp; </td></tr>";
                        if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){

                           while($row = $this->db_fetch_array()){
                                  $i++;
                                  if($i>$this->page_size) break;
                                  $count = $n_start+$i;
                                  list($title,$startdate, $duedate,$subject,$loading,$hid,$posterID, $description, $courseID, $courseAssignmentID,$attachmentPath,$typeName) = $row;
                                  $x .= "<TR><td></td><TD>$count</td><td><A HREF='javascript:fe_view_homework_detail($hid)'>$title</A>\n";
                                  if ($description != "")
                                      $x .= "<img src=\"$hwImagePath/icon_alt.gif\" alt=\"$description\"> ";
                                  if ($courseID!="" && $courseAssignmentID!="")
                                  {
                                      $x .= $this->returneClassIcon();
                                  }
                                  $x .= "</TD>\n";
                                  $x .= "<td>$startdate</td>\n";
                                  $x .= "<td>$duedate</td>\n";
                                  $x .= "<TD>$subject</TD>\n";
                                  if ($loading ==0)
                                  {
                                      $tload = $i_Homework_minimum;
                                  }
                                  else if ($loading <= 16)
                                   {
                                           $tload = $loading/2;
                                   }
                                   else
                                   {
                                           $tload = "$i_Homework_morethan 8";
                                   }
                                  $x .= "<TD>$tload</TD>";
                                  $x .= "<td>";
                                  if($attachmentPath!=""){
                                         $x .=" <img src=\"$image_path/icon_attachment.gif\" alt=\"$i_AnnouncementAttachment\"> ";
                                      }
                                  if ($courseID!="" && $courseAssignmentID)
                                  {
                                  }
                                  else if ($uid==$posterID)
                                  {
                                          $x .= $this->returnEditDeleteLink($hid);
                                  }
                                  "</td><td></td></TR>\n";
                            }
                         }
                }
                else
                {
                        $x .= "<tr><td align=center colspan=9>".$this->no_msg."</td></tr>";
                }
                $x .= "</TABLE></td></tr>";
                if($this->db_num_rows()<>0)
                {
                        $x .= "<tr><td width=26>&nbsp;</td>";
                        $x .= "<tr><td>".$this->navigation()."</td>";
                        $x .= "<td width=10>&nbsp;</td>\n</tr>";
                }
                $x .= "</TABLE>";

                $x.= "<input type=hidden name=field value=".$this->field.">\n";
                $x.= "<input type=hidden name=order value=".$this->order.">\n";
                $x.= "<input type=hidden name=pageNo value=1>\n";
                $x.= "<input type=hidden name=classID value=$cid>\n";
                $this->db_free_result();
                return $x;
        }

        function displaySmallTeacherClass($cid, $uid)
        {
                 global $i_Homework_today;
                $fields = "CONCAT(a.Title, if (a.StartDate=CURDATE(),' $i_Homework_today','')),
                           DATE_FORMAT(a.DueDate,'%Y-%m-%d'), b.SubjectName, a.Loading, a.HomeworkID, a.PosterUserID,
                           a.Description,1 as PosterTeaching,'' as RecordClassID,a.SubjectID,
                           e.CourseID, e.CourseAssignmentID,a.AttachmentPath,a.HandinRequired,f.TypeName";
                $dbtables = "INTRANET_HOMEWORK as a
                               LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                               LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as e ON a.HomeworkID = e.IntranetHomeworkID
                               LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE as f ON (a.TypeID=f.TypeID)
                               ";
                $conds = "ClassGroupID = $cid AND a.PosterUserID = $uid AND a.DueDate >= CURDATE()";
                $sql = "SELECT $fields FROM $dbtables WHERE $conds";
                return $this->displaySmallTable($cid,$uid,$sql);
        }
        /*
        function getSelectClasses($atrib, $match)
        {
                 global $button_select;
                // Select all classes
                $sql = "SELECT GroupID, Title FROM INTRANET_GROUP WHERE RecordType = 3 AND LOCATE('*',Title)=0 AND LOCATE('-',Title)=0 order by Title";
                $classes = $this->returnArray($sql, 2);
                $x = "<SELECT $atrib >\n";
                //$x .= "<OPTION VALUE=0> -- $button_select -- </OPTION>";
                for ($i=0; $i<sizeof($classes); $i++)
                {
                        $GroupID = $classes[$i][0];
                        $Title = $classes[$i][1];
                        $x .= "<OPTION VALUE=$GroupID";
                        if ($GroupID == $match)
                        {
                            $x .= " selected";
                        }
                        $x .= "> $Title </OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
        }
        */

        function getSelectTeacherClasses($tags, $selected, $teaching="",$noFirst=0)
        {
                 global $UserID;
                 if ($this->teachingRestricted && $teaching == 1)
                 {
                     $sql = "SELECT DISTINCT c.GroupID, c.Title FROM INTRANET_SUBJECT_TEACHER as a
                             LEFT OUTER JOIN INTRANET_CLASS as b ON a.ClassID = b.ClassID
                             LEFT OUTER JOIN INTRANET_GROUP as c ON b.GroupID = c.GroupID
                             WHERE a.UserID = $UserID AND c.RecordType = 3 ORDER BY c.Title";
                 }
                 else
                 {
                     $sql = "SELECT a.GroupID, a.Title FROM INTRANET_GROUP as a LEFT OUTER JOIN INTRANET_CLASS as b ON a.GroupID = b.GroupID
                             WHERE a.RecordType = 3 AND b.GroupID IS NOT NULL ORDER BY a.Title";
                 }
                 $result = $this->returnArray($sql,2);
                 return getSelectByArray($result,$tags,$selected,0,$noFirst);
        }
        function getSelectGroups($tags, $selected)
        {
                 $sql = "SELECT a.GroupID, a.Title FROM INTRANET_GROUP as a
                         LEFT OUTER JOIN INTRANET_CLASS as b ON a.GroupID = b.GroupID
                         WHERE b.ClassID IS NULL AND a.RecordType = 3 ORDER BY a.Title";
                 $result = $this->returnArray($sql,2);
                 return getSelectByArray($result,$tags, $selected,0,1);
        }
        function getSelectActivatedSubjects($tags, $selected="", $classGroup = "",$teaching="")
        {
                 global $UserID;
                 if ($classGroup != "")
                 {
                     if (!$this->teachingRestricted || ($teaching!=1 && $this->nonteachingAllowed))
                     {
                          $conds = "";
                     }
                     else
                     {
                         $conds = " AND b.UserID = $UserID";
                     }
                          $sql = "SELECT DISTINCT a.SubjectID,a.SubjectName FROM INTRANET_SUBJECT as a
                                  , INTRANET_SUBJECT_TEACHER as b, INTRANET_CLASS as c
                                  WHERE a.SubjectID = b.SubjectID AND a.RecordStatus = 1
                                  AND b.ClassID = c.ClassID AND c.GroupID = $classGroup $conds
                                  ORDER BY a.SubjectName";
                 }
                 else
                 {
                     $sql = "SELECT SubjectID, SubjectName FROM INTRANET_SUBJECT WHERE RecordStatus = 1 ORDER BY SubjectName";
                 }
                 $result = $this->returnArray($sql,2);
                 return getSelectByArray($result,$tags,$selected);
        }

        function getSelectActivatedSuspendedSubjects($atrib, $target="")
        {
                // Select all classes
                $sql = "SELECT SubjectID, SubjectName FROM INTRANET_SUBJECT WHERE RecordStatus IN (0,1) ORDER BY SubjectName";
                $subjects = $this->returnArray($sql,2);
                if (sizeof($subjects)==0) return "";
                $x .= "<SELECT $atrib>\n";
                //$x .= "<OPTION value=0> -- Select -- </OPTION>\n";
                for($i=0; $i<sizeof($subjects); $i++){
                        list($SubjectID,$SubjectName) = $subjects[$i];
                        $x .= "<OPTION VALUE=\"$SubjectID\"";
                        if ($target==$SubjectID)
                            $x .= " SELECTED";
                        $x .= "> $SubjectName </OPTION>\n";
                }
                $x .= "</SELECT>";
                return $x;
        }

        /*function getSelectLoading($atrib)
        {
                 global $i_Homework_morethan,$i_Homework_minimum;
                $max = 17;
                $i = 1;
                $x = "<SELECT $atrib >\n";
                $x .= "<OPTION VALUE=0>$i_Homework_minimum</OPTION>";

                while ($i < $max)
                {
                        $x .= "<OPTION VALUE=$i>". ($i*0.5) ."</OPTION>\n";
                        $i++;
                }
                $x .= "<option VALUE=$max> $i_Homework_morethan ". (($max-1)*0.5) ."</OPTION>\n";
                $x .= "</SELECT>\n";
                return $x;
        }*/
        function getSelectLoading($atrib)
        {
                 global $i_Homework_morethan,$i_Homework_minimum;
                $max = 33;
                $i = 1;
                $x = "<SELECT $atrib >\n";
                $x .= "<OPTION VALUE=0>$i_Homework_minimum</OPTION>";

                while ($i < $max)
                {
                        $x .= "<OPTION VALUE=".($i/2).">". ($i/4) ."</OPTION>\n";
                        $i++;
                }
                $x .= "<option VALUE=".round($max/2)."> $i_Homework_morethan ". (($max-1)/4) ."</OPTION>\n";
                $x .= "</SELECT>\n";
                return $x;
        }

        /*function getSelectedLoadingSelect($atrib,$tload)
        {
                 global $i_Homework_morethan,$i_Homework_minimum;
                $max = 17;
                $i = 1;
                $x = "<SELECT $atrib >\n";
                $x .= "<OPTION VALUE=0";
                if (0==$tload) $x .= " selected ";
                $x .= ">$i_Homework_minimum</OPTION>";

                while ($i < $max)
                {
                        $x .= "<OPTION VALUE=$i";
                        if ($i==$tload) $x .= " selected ";
                        $x .= ">". ($i*0.5) ."</OPTION>\n";
                        $i++;
                }
                $x .= "<option VALUE=$max";
                if ($max==$tload) $x .= "selected";
                $x .= "> $i_Homework_morethan ". (($max-1)*0.5) ."</OPTION>\n";
                $x .= "</SELECT>\n";
                return $x;
        }*/
        function getSelectedLoadingSelect($atrib,$tload)
        {
                 global $i_Homework_morethan,$i_Homework_minimum;
                $max = 33;
                $i = 1;
                $x = "<SELECT $atrib >\n";
                $x .= "<OPTION VALUE=0";
                if (0==$tload) $x .= " selected ";
                $x .= ">$i_Homework_minimum</OPTION>";

                while ($i < $max)
                {
                        $x .= "<OPTION VALUE=".($i/2);
                        if ($i/2==$tload) $x .= " selected ";
                        $x .= ">". ($i/4) ."</OPTION>\n";
                        $i++;
                }
                $x .= "<option VALUE=".(round($max/2))."";
                if (round($max/2)==$tload) $x .= " selected";
                $x .= "> $i_Homework_morethan ". (($max-1)/4) ."</OPTION>\n";
                $x .= "</SELECT>\n";
                return $x;
        }

        function getSearchTeacherSelect($atrib)
        {
                 if ($this->teacherSearchDisabled) return "";
                global $button_select;
                $username_field = getNameFieldWithClassNumberByLang("");

                #$sql = "select a.UserID, CONCAT($username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) ) from INTRANET_USER as a, INTRANET_USERGROUP as b where a.UserID = b.UserID and b.GroupID = 1 ORDER BY CONCAT(LastName,' ',FirstName)";
                $sql = "SELECT UserID, $username_field from INTRANET_USER where RecordType = 1 AND Teaching = 1 ORDER BY EnglishName";
                $teachers = $this->returnArray($sql,2);
                $x = "<SELECT $atrib>\n";
                $x .= "<OPTION> -- $button_select -- </OPTION>\n";
                for($i=0; $i<sizeof($teachers); $i++){
                        $TeacherID = $teachers[$i][0];
                        $TeacherName = $teachers[$i][1];
                        $x .= "<OPTION VALUE=$TeacherID> $TeacherName </OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
        }

        function getSearchSubjectSelect($atrib)
        {
                 if ($this->subjectSearchDisabled) return "";
                 global $button_select;
                 $sql = "SELECT SubjectID, SubjectName FROM INTRANET_SUBJECT WHERE RecordStatus =1 OR RecordStatus=0 ORDER BY SubjectName";
                 $subjects = $this->returnArray($sql,2);
                 $x = "<SELECT $atrib>\n";
                 $x .= "<OPTION> -- $button_select -- </OPTION>\n";
                 for ($i=0; $i<sizeof($subjects); $i++)
                 {
                      list ($id,$name) = $subjects[$i];
                      $x .= "<OPTION VALUE=$id> $name </OPTION>\n";
                 }
                 $x .= "</SELECT>\n";
                 return $x;
        }
        function getSearchTeachingSelect($atrib)
        {
                 if ($this->subjectsTaughtSearchDisabled) return "";
                 global $button_select;
                 $name_field = getNameFieldByLang("b.");
                 $sql = "SELECT DISTINCT a.UserID, $name_field FROM INTRANET_SUBJECT_TEACHER as a
                         LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                         WHERE b.RecordStatus = 1 ORDER BY b.EnglishName";
                 $teachers = $this->returnArray($sql,2);
                 $x = getSelectByArray($teachers,$atrib);
                 return $x;
        }
        
        function getSearchCollectSelect($atrib)
        {
                 if ($this->subjectsTaughtSearchDisabled) return "";
                 $options = array(array(1,'Yes'),array(-1,'No'));
                 $x = getSelectByArray($options,$atrib);
                 
                 return $x;
        }

        function getSelectHomeworkList($tags, $selected="")
        {
                $sql = "SELECT TypeID, TypeName FROM INTRANET_HOMEWORK_TYPE WHERE RecordStatus = 1 ORDER BY DisplayOrder";
                $result = $this->returnArray($sql, 2);
                return getSelectByArray($result, $tags, $selected);
        }

        # Design display
        function displayLargeTitleBar ($msg1, $msg2="")
        {
                global $intranet_session_language,$image_path,$i_Homework_unauthorized,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_hours,$i_Homework_morethan, $i_Homework_lastModified,$i_Homework_404;
                global $i_Homework_list, $i_Homework_detail, $i_Homework_ToDoList, $i_Homework_AllRecords;

                $hwImagePath = "$image_path/homework/$intranet_session_language/hw";

                $x = "<table width=\"750\" border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg_popup.gif\">\n";
                $x .="<tr><td colspan=3><a href=\"/home/homework/\"><img border=0 src=\"$hwImagePath/bar_popup.gif\" width=750 height=28></a></td></tr>\n";
                $x .= "<tr><td colspan=3><img src=\"$hwImagePath/spacer.gif\" width=1 height=10></td></tr>\n";
                $x .= "<tr><td width=15>&nbsp;</td><td width=716><img src=\"$hwImagePath/buttlet_sq.gif\" width=13 height=13>\n";
                $x .= "<b>$msg1 </b>";
                if ($msg2 != "")
                    $x .= "<img src=\"$hwImagePath/arrow.gif\" width=9 height=15> <b> $msg2</b>";
                $x .= "</td>\n";
                $x .= "<td width=19>&nbsp;</td></tr>\n";
                $x .= "<tr><td colspan=3><img src=\"$hwImagePath/spacer.gif\" width=1 height=10></td></tr>\n";
                $x .= "</table>\n";
                return $x;
        }
        
        function displayAttachmentEdit($homeworkID,$name)
        {
                 global $file_path, $image_path;
                 
                 $homeworkID = IntegerSafe($homeworkID);
                 $sql =  "SELECT AttachmentPath FROM INTRANET_HOMEWORK WHERE HomeworkID = '$homeworkID'";
                 $tmp = $this->returnVector($sql,1);
                 $folder = $tmp[0];
                 
                 $x = "";
                 if($folder=="") return $x;
                 
                 $path = "$file_path/file/homework/".$folder.$homeworkID;
                 include_once("libfiletable.php");
                 $templf = new libfiletable("", $path,0,0,"");
                 $files = $templf->files;
                 
                 $x = "<table border=0 cellpadding=0 cellspacing=0>";
                 while (list($key, $value) = each($files))
                 {
                        $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                        // $url = intranet_handle_url($url);
                        
                        $x .= "<tr><td width=1><input type=checkbox name=$name value=\"".urlencode($files[$key][0])."\" checked onClick=\"setRemoveFile($key,this.checked)\"></td>";
                        $x .= "<td id='a_".$key."'><img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                        // $x .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\" >".$files[$key][0]."</a>";
                        
                        $x .= "<a href=\"/home/download_attachment.php?target_e=".getEncryptedText($file_path.$url)."\" target=\"_blank\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\" >".$files[$key][0]."</a>";
                        $x .= " (".ceil($files[$key][1]/1000)."Kb)</td>";
                        $x .= "</tr>\n";
                 }
                 $x .= "</table>";
                 return $x;
        }
        
        function displaySmallTitleBar ()
        {
                global $intranet_session_language,$image_path,$i_Homework_unauthorized,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_hours,$i_Homework_morethan, $i_Homework_lastModified,$i_Homework_404;
                global $i_Homework_list, $i_Homework_detail, $i_Homework_ToDoList, $i_Homework_AllRecords;

                $hwImagePath = "$image_path/homework/$intranet_session_language/hw";

                $x = "<table width=\"517\" border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg.gif\">\n";
                $x .="<tr><td colspan=3><a href=\"/home/homework/\"><img border=0 src=\"$hwImagePath/bar.gif\" width=517 height=30></a></td></tr>\n";
                $x .= "<tr><td colspan=3><img src=\"$hwImagePath/spacer.gif\" width=1 height=10></td></tr>\n";
                $x .= "</table>\n";
                return $x;
        }

        function displayViewTitleBar ()
        {
                global $intranet_session_language,$image_path,$i_Homework_unauthorized,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_hours,$i_Homework_morethan, $i_Homework_lastModified,$i_Homework_404;
                global $i_Homework_list, $i_Homework_detail, $i_Homework_ToDoList, $i_Homework_AllRecords;

                $hwImagePath = "$image_path/homework/$intranet_session_language/hw";

                $x = "<table width=\"517\" border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg.gif\">\n";
                $x .="<tr><td colspan=3><img border=0 src=\"$hwImagePath/bar.gif\" width=517 height=30></td></tr>\n";
                $x .= "<tr><td colspan=3><img src=\"$hwImagePath/spacer.gif\" width=1 height=10></td></tr>\n";
                $x .= "</table>\n";
                return $x;
        }

        function displayLargeTitleToolbar ($msg1, $toolbar)
        {
                global $intranet_session_language,$image_path,$i_Homework_unauthorized,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_hours,$i_Homework_morethan, $i_Homework_lastModified,$i_Homework_404;
                global $i_Homework_list, $i_Homework_detail, $i_Homework_ToDoList, $i_Homework_AllRecords;

                $hwImagePath = "$image_path/homework/$intranet_session_language/hw";

                $x  = "<table width=\"750\" border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg_popup.gif\">\n";
                $x .= "<tr><td colspan=4><a href=\"/home/homework/\"><img border=0 src=\"$hwImagePath/bar_popup.gif\" width=750 height=28></a></td></tr>\n";
                $x .= "<tr><td colspan=4><img src=\"$hwImagePath/spacer.gif\" width=1 height=10></td></tr>\n";
                $x .= "<tr><td width=22>&nbsp;</td><td width=174><img src=\"$hwImagePath/buttlet_sq.gif\" width=13 height=13>\n";
                $x .= "<b>$msg1</b>";
                $x .= "</td>\n";
                $x .= "<td width=532 align=right valign=middle>$toolbar</td>\n";
                $x .= "<td width=22>&nbsp;</td></tr>";
                $x .= "<tr><td colspan=4><img src=\"$hwImagePath/spacer.gif\" width=1 height=10></td></tr>\n";
                $x .= "</table>\n";
                return $x;
        }

        function displaySmallTitleToolbar ($msg, $toolbar)
        {
                global $intranet_session_language,$image_path,$i_Homework_unauthorized,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_hours,$i_Homework_morethan, $i_Homework_lastModified,$i_Homework_404;
                global $i_Homework_list, $i_Homework_detail, $i_Homework_ToDoList, $i_Homework_AllRecords,$i_Homework_myrecords;

                $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                $x = "<table width=517 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg.gif\">
  <tr>
    <td colspan=4><a href=\"/home/homework/\"><img border=0 src=\"$hwImagePath/bar.gif\" width=517 height=30></a></td>
  </tr>
  <tr>
    <td colspan=4><img src=\"$hwImagePath/spacer.gif\" width=1 height=10></td>
  </tr>
  <tr>
    <td width=15>&nbsp;</td>
    <td width=120><img src=\"$hwImagePath/buttlet_sq.gif\" width=13 height=13>
      <b>$msg</b></td>
    <td align=center width=367>$toolbar</td>
    <td width=15 align=center>&nbsp;</td>
  </tr>
</table>\n";
              return $x;
        }

        function displayLargeBottomBar()
        {
                 global $image_path, $intranet_session_language;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x = "<table width=750 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg_popup2.gif\">\n";
                 $x .= "<tr><td>&nbsp;</td></tr><tr><td><img src=\"$hwImagePath/bar_bottom_popup.gif\" width=750 height=50></td>\n";
                 $x .= "</tr></table>\n";
                 return $x;
        }

        function displaySmallBottomBar()
        {
                 global $image_path, $intranet_session_language;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x = "<table width=517 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg2.gif\">
                   <tr>
                     <td><img src=\"$hwImagePath/bar_bottom.gif\" width=517 height=46></td>
                   </tr>
                 </table>\n";
                 return $x;
        }

        function displaySmallAllBottomBar()
        {
                 global $image_path, $intranet_session_language;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x .= "<table width=517 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg2.gif\">
     <tr><td><img src=\"$hwImagePath/bar_bottom3.gif\" width=517 height=35></td>
    </tr></table>\n";
                 return $x;
        }

        function showtab($TabID)
        {
                 global $image_path,$intranet_session_language,$classID;

                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x  = "<table width=750 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg_popup.gif\">\n";
                 $x .= "<tr>\n";

                 $todo_path = "showtodo.php?classID=$classID";
                 $history_path = "showhistory.php?classID=$classID";

                 if ($TabID ==0)
                 {
                     $todo_status = "on";
                     $history_status = "off";
                     $todo_tab = 1;
                     $history_tab = 2;
                     $todo_bg = "#EEFCF6";
                     $history_bg = "#789E8D";
                 }
                 else
                 {
                     $todo_status = "off";
                     $history_status = "on";
                     $todo_tab = 2;
                     $history_tab = 1;
                     $todo_bg = "#789E8D";
                     $history_bg = "#EEFCF6";
                 }
                 $x .= "<td width=14 align=\"right\"><img src=\"$hwImagePath/tab_$todo_tab"."l.gif\" width=5 height=26></td>\n";
                 $x .= "<td width=134 align=center bgcolor=$todo_bg><a href=\"$todo_path\"><img border=0 src=\"$hwImagePath/icon_todolist_$todo_status.gif\" width=26 height=22><img border=0 src=\"$hwImagePath/todolist_$todo_status.gif\" width=71 height=15></a></td>\n";
                 $x .= "<td width=5><img src=\"$hwImagePath/tab_$todo_tab"."r.gif\" width=5 height=26></td>\n";
                 $x .= "<td width=5><img src=\"$hwImagePath/tab_$history_tab"."l.gif\" width=5 height=26></td>\n";
                 $x .= "<td width=150 align=center bgcolor=$history_bg><a href=\"$history_path\"><img border=0 src=\"$hwImagePath/icon_history_$history_status.gif\" width=31 height=22><img border=0 src=\"$hwImagePath/history_$history_status.gif\" width=64 height=15></a></td>\n";
                 $x .= "<td width=5><img src=\"$hwImagePath/tab_$history_tab"."r.gif\" width=5 height=26></td>\n";
                 $x .= "<td width=437 valign=bottom>&nbsp;</td>\n";
                 $x .= "</tr></table>\n";

                 return $x;
        }

        function showSmallTab($TabID)
        {
                 global $image_path,$intranet_session_language,$classID;

                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x  = "<table width=517 border=0 cellspacing=0 cellpadding=0 background=\"$hwImagePath/table_bg.gif\">\n";
                 $x .= "<tr>\n";

                 $todo_path = "?filter=0#homework";
                 $history_path = "?filter=1#homework";

                 if ($TabID ==0)
                 {
                     $todo_status = "on";
                     $history_status = "off";
                     $todo_tab = 1;
                     $history_tab = 2;
                     $todo_bg = "#EEFCF6";
                     $history_bg = "#789E8D";
                 }
                 else
                 {
                     $todo_status = "off";
                     $history_status = "on";
                     $todo_tab = 2;
                     $history_tab = 1;
                     $todo_bg = "#789E8D";
                     $history_bg = "#EEFCF6";
                 }
                 $x .= "<td width=14 align=\"right\"><img src=\"$hwImagePath/tab_$todo_tab"."l.gif\" width=5 height=26></td>\n";
                 $x .= "<td width=134 align=center bgcolor=$todo_bg><a href=\"$todo_path\"><img border=0 src=\"/images/homeworklist/icon_todolist_$todo_status.gif\" width=26 height=22><img border=0 src=\"$hwImagePath/todolist_$todo_status.gif\" width=71 height=15></a></td>\n";
                 $x .= "<td width=5><img src=\"$hwImagePath/tab_$todo_tab"."r.gif\" width=5 height=26></td>\n";
                 $x .= "<td width=5><img src=\"$hwImagePath/tab_$history_tab"."l.gif\" width=5 height=26></td>\n";
                 $x .= "<td width=150 align=center bgcolor=$history_bg><a href=\"$history_path\"><img border=0 src=\"/images/homeworklist/icon_history_$history_status.gif\" width=31 height=22><img border=0 src=\"$hwImagePath/history_$history_status.gif\" width=64 height=15></a></td>\n";
                 $x .= "<td width=5><img src=\"$hwImagePath/tab_$history_tab"."r.gif\" width=5 height=26></td>\n";
                 $x .= "<td width=204 STYLE=\"vertical-align: BOTTOM\"><img src=\"$hwImagePath/little_bar.gif\" width=204 height=4></td>\n";
                 $x .= "</tr></table>\n";

                 return $x;
        }

        function displayLargeTitleToolbar2($icon, $msg, $toolbar, $toolbar2="")
        {
                 global $i_Homework_list;
                 global $image_path;
                 if ($icon == "")
                 {
                     $width = 240;
                     $lwidth = 335;
                     $cell = $msg;
                     $col = 3;
                 }
                 else
                 {
                     $width = 40;
                     $lwidth = 535;
                     $cell = "$icon </td><td class=td_middle_h1 nowrap>$msg";
                     $col = 4;
                 }

                 $x = "<table width=717 border=0 cellspacing=0 cellpadding=0>
                         <tr>
                      <td class=td_middle_h1 align=left width=142 nowrap><img src=$image_path/homeworklist/sq_palegreen.gif>
                        $i_Homework_list <img src=$image_path/homeworklist/arrow.gif></td>
                      <td class=td_middle_h1 align=left width=$width>$cell </td>
                      <td align=right width=$lwidth>$toolbar &nbsp;</td>
                    </tr>\n";
                 if ($toolbar2!="")
                 {
                    $x .= "<tr>
                      <td colspan=$col align=left class=functionlink><img src=$image_path/spacer.gif width=13> $toolbar2 &nbsp;</td>
                    </tr>
                  ";
                 }
                    $x .= "<tr>
                      <td colspan=$col align=left class=functionlink><img src=$image_path/spacer.gif width=13 height=5> &nbsp;</td>
                    </tr>
                  </table>";
                  return $x;
        }

        function displayTeacherTabView2($classID, $uid, $cList)
        {
                 global $image_path, $intranet_session_language;
                 $hwImagePath = "$image_path/homeworklist";
                 $temp = "";
                 for ($i=0; $i<sizeof($cList); $i++)
                 {
                      list($id, $name) = $cList[$i];
                      $chopped_name = chopword($name,15);
                      if ($name != $chopped_name)
                          $alt = " title='$name'";
                      else $alt = "";
                      $s = (($id==$classID || ($i==0 && $classID==""))? "light":"dark");
                      if ($i % 4 ==0)
                      {
                          if ($i!=0) $temp .= "</tr>\n";
                          $x = "$temp$x";
                          $temp = "<tr>";
                      }
                      $latt = "width=5"; //($i%4==0? "width=15 align=right": "width=5");
                      $ratt = "width=5"; //($i%4==3? "width=18 align=left": "width=5");
                      $cwidth = ($i%4==3? 169: 170);
                      $temp .= "<td $latt><img src=\"$hwImagePath/tab_$s"."left.gif\" width=5 height=26></td>
    <td width=$cwidth align=center class=homework_$s"."_class><a $alt href=?classID=$id>$chopped_name</a></td>
    <td $ratt><img src=\"$hwImagePath/tab_$s"."right.gif\" width=5 height=26></td>\n";
                 }
                 if (sizeof($cList)%4!=0)
                        for ($j=sizeof($cList)%4; $j<4; $j++)
                        {
                              $cwidth = ($j%4==3? 169: 170);
                              $ratt = "width=5"; //($j==3? "width=18 align=left": "width=5");
                              $temp .= "<td width=5></td><td width=$cwidth></td><td $ratt></td>\n";
                        }
                 $temp .= "</tr>\n";
                 $x = "<table width=717 border=0 cellspacing=0 cellpadding=0 class=h1>$temp$x</table>";
                 return $x;
        }

        function displayTeacherClass2($cid, $uid)
        {
                 global $i_Homework_morethan,$i_Homework_hours,$i_Homework_class,$i_Homework_subject,$i_Homework_title,$i_Homework_description,$i_Homework_loading,$i_Homework_startdate,$i_Homework_duedate,$i_Homework_lastModified;
                 global $image_path, $intranet_session_language,$i_Homework_teacher_name,$i_Homework_today,$i_Homework_minimum;
                 global $i_AnnouncementAttachment,$i_Homework_Collected_By_Class_Teacher,$i_general_yes,$i_general_no;
                 $hwImagePath = "$image_path/homework/$intranet_session_language/hw";
                 $x = "";

                $sorting_fields = array("a.Title","a.StartDate","a.DueDate","b.SubjectName","a.Loading","a.CollectRequired");
                if ($this->field=="" || $this->field >= count($sorting_fields))
                    $this->field = 1;
                if ($this->order=="" || ($this->order!=1))
                    $this->order = 0;
                if ($this->pageNo==""|| ($this->pageNo==0))
                    $this->pageNo =1;
                $order_str = ($this->order==0) ? " ASC" : " DESC";

                #$fields = "CONCAT(a.Title, if (a.StartDate=CURDATE(),' $i_Homework_today','')), a.StartDate, a.DueDate, b.SubjectName, a.Loading, a.HomeworkID, a.PosterUserID,a.Description";
                #$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID";
                #$conds = "ClassGroupID = $cid AND a.PosterUserID = $uid AND a.DueDate >= CURDATE()";
                $fields = "CONCAT(a.Title, if (a.StartDate=CURDATE(),' $i_Homework_today','')), a.StartDate, a.DueDate, b.SubjectName, a.Loading, a.HomeworkID, a.PosterUserID,a.Description, c.CourseID, c.CourseAssignmentID, a.AttachmentPath,a.HandinRequired,a.CollectRequired,h.TypeName";
                $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID LEFT OUTER JOIN INTRANET_COURSE_HOMEWORK as c ON a.HomeworkID = c.IntranetHomeworkID LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE As h ON a.TypeID = h.TypeID";
                $conds = "ClassGroupID = $cid AND a.PosterUserID = $uid AND a.DueDate >= CURDATE()";
                $sql = "SELECT $fields FROM $dbtables WHERE $conds";
                $sql .= " ORDER BY " . $sorting_fields[$this->field] . $order_str;
                $this->rs = $this->db_db_query($sql);
                $x .= "<table width=717 border=0 cellspacing=0 cellpadding=5 background=/images/homeworklist/notebooktbbg.gif class=h1>\n";
                $x .= "<tr><td colspan=3>";
                
                if($this->useHomeworkCollect)
                {
	                $x.="<table width=100% border=0>\n";
	                $x .= "<tr><td width='30'>&nbsp;</td>\n<td>&nbsp;</td>\n";
	                $x .= "<td width='100'>&nbsp;</td>\n<td width='50'>&nbsp;</td>\n<td>&nbsp;</td>\n";
	                $x .= "<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>";
                }
                else
                {
	                $x.="<table width=100% border=0 cellspacing=5 cellpadding=0>\n";
	                $x .= "<tr><td width=30>&nbsp;</td>\n<td width=172>&nbsp;</td>\n";
	                $x .= "<td width=50>&nbsp;</td>\n<td width=50>&nbsp;</td>\n<td width=20>&nbsp;</td>\n";
	                $x .= "<td width=55>&nbsp;</td>\n<td width=50>&nbsp;</td>\n</tr>";
            	}
                $i =0;
                $n_start = ($this->pageNo-1)*$this->page_size;
                if($this->rs && $this->db_num_rows()!=0){
                        $x .= "<tr><td>#</td>\n";
                        $x .= "<TD>".$this->column(0,$i_Homework_title)."</TD>\n";
                        $x .= "<TD>".$this->column(1,$i_Homework_startdate)."</TD>\n";
                        $x .= "<TD>".$this->column(2,$i_Homework_duedate)."</TD>\n";
                        $x .= "<TD>".$this->column(3,$i_Homework_subject)."</TD>\n";
                        $x .= "<TD>".$this->column(4,"$i_Homework_loading ($i_Homework_hours)")."</TD>\n";
                        if($this->useHomeworkCollect)
                        {
                        	$x .= "<TD>".$this->column(5,$i_Homework_Collected_By_Class_Teacher)."</TD>";
                    	}
                        $x .= "<td> &nbsp; </td></tr>";
                        if($this->useHomeworkCollect)
                        {
	                        $x .= $this->getLineSeparation(8);
                        }
                        else
                        {
	                     	$x .= $this->getLineSeparation(7);   
                        }
                        if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){

                           while($row = $this->db_fetch_array()){
                                  $i++;
                                  if($i>$this->page_size) break;
                                  $count = $n_start+$i;
                                  list($title,$startdate, $duedate,$subject,$loading,$hid,$posterID, $description,$eclassCourseID, $eclassAssignmentID, $AttachmentPath,$handinRequired,$collectRequired,$HomeworkType) = $row;
                                  $title = ($this->useHomeworkType && $HomeworkType != "") ? "[".$HomeworkType."] ".$title : $title;
                                  $title = intranet_wordwrap($title,20,"\n",1);
                                  $subject = intranet_wordwrap($subject,15,"\n",1);

                                  $x .= "<TR><TD  align='center'>$count</td><td><A HREF='javascript:fe_view_homework_detail($hid)'>$title</A>\n";
                                  if ($description != "")
                                      $x .= "<img src=\"$hwImagePath/icon_alt.gif\" alt=\"$description\"> ";
                                  if ($AttachmentPath != "")
                                            $x .= "<img src=\"$image_path/icon_attachment.gif\" alt=\"$i_AnnouncementAttachment\"> ";

                                  if ($eclassAssignmentID != "" && $eclassCourseID != "")
                                  {
                                      $x .= $this->returneClassIcon();
                                  }
                                  $x .= "</TD>\n";
                                  $x .= "<td>$startdate</td>\n";
                                  $x .= "<td>$duedate</td>\n";
                                  $x .= "<TD>$subject</TD>\n";
                                  if ($loading ==0)
                                  {
                                      $tload = $i_Homework_minimum;
                                  }
                                  else if ($loading <= 16)
                                  {
                                      $tload = $loading/2;
                                  }
                                  else
                                  {
                                      $tload = "$i_Homework_morethan 8";
                                  }
                                  $x .= "<TD>$tload</TD>";
                                  if($this->useHomeworkCollect)
                                  {
                                  		$x.="<td>".($collectRequired==1?$i_general_yes:$i_general_no)."</td>";
                              	  }
                                  $x .= "<td>";

                                  if ($this->useHomeworkHandin && $handinRequired && $this->hasAccessHandinListRight($uid,$hid)){
                                                  $temp = $this->rs;
                                          $x .= $this->returnHandinListLink($hid);
                                          $this->rs = $temp;
                                      }
                                  if ($uid==$posterID && $eclassAssignmentID == "" && $eclassCourseID == "")
                                  {
                                      $x .= $this->returnEditDeleteLink($hid);
                                  }
                                  $x .= "</td></TR>\n";
                                  
                                  if($this->useHomeworkCollect)
			                        {
				                        $x .= $this->getLineSeparation(8);
			                        }
			                        else
			                        {
				                     	$x .= $this->getLineSeparation(7);   
			                        }
                            }
                         }
                }
                else
                {
                        $x .= "<tr><td align=center colspan=7>".$this->no_msg."</td></tr>";
                }
                $x .= "</TABLE></td></tr>";
                if($this->db_num_rows()<>0)
                {
                        $x .= "<tr><td width=26>&nbsp;</td>";
                        $x .= "<tr><td>".$this->navigation()."</td>";
                        $x .= "<td width=10>&nbsp;</td>\n</tr>";
                }
                $x .= "</TABLE>";

                $x.= "<input type=hidden name=field value=".$this->field.">\n";
                $x.= "<input type=hidden name=order value=".$this->order.">\n";
                $x.= "<input type=hidden name=pageNo value=1>\n";
                $x.= "<input type=hidden name=classID value=$cid>\n";
                $this->db_free_result();
                return $x;
        }

        function displayLargeBottomBar2()
        {
                 $x = "<table width=717 border=0 cellspacing=0 cellpadding=5 background=/images/homeworklist/notebooktbbg.gif class=h1>\n";
                 $x .= "<tr>
                <td>dsf</td>
              </tr>";
                 $x .= "</table>\n";
                 return $x;
        }

        function getLineSeparation ($cols, $height="")
        {
                 $height = ($height==""? 6: $height);

              $x .= "<tr>
                    <td height=$height colspan=$cols align=center><img src=/images/line_long.gif></td>
                 </tr>";
              return $x;
        }

        function getSmallLineSeparation ($cols, $height="")
        {
                 $height = ($height==""? 6: $height);

              $x .= "<tr>
                    <td height=$height colspan=$cols align=center><img src=/images/index/line.gif></td>
                 </tr>";
              return $x;
        }

        function getMaxTitleLength ($array)
        {
                 $max = 0;
                 for ($i=0; $i<sizeof($array); $i++)
                 {
                      $length = strlen($array[$i][1]);
                      if ($length > $max) $max = $length;
                 }
                 return $max;
        }
        function getSubjectLeaderInfo($targetUserID)
        {
	        
	        # Assumption: current year, current term
	        $TermInfoArr = getCurrentAcademicYearAndYearTerm();
			$curYearID = $TermInfoArr['AcademicYearID'];
			$curYearTermID = $TermInfoArr['YearTermID'];
	
	        $sql = "
	        		SELECT 
	        			c.ClassID,
	        			c.SubjectID
					FROM 
						ASSESSMENT_SUBJECT AS a
						INNER JOIN SUBJECT_TERM AS b ON b.SubjectID = a.RecordID 
						INNER JOIN INTRANET_SUBJECT_LEADER AS c ON  b.SubjectGroupID = c.ClassID
						INNER JOIN ACADEMIC_YEAR_TERM AS d ON d.YearTermID = b.YearTermID 
						INNER JOIN ACADEMIC_YEAR AS e ON e.AcademicYearID = d.AcademicYearID 
					WHERE 
						c.UserID = $targetUserID AND 
						e.AcademicYearID = $curYearID AND
						d.YearTermID = $curYearTermID AND
						c.RecordStatus = 1 
					";
					
			return $this->returnArray($sql,2);
			
			/*
                 $sql = "SELECT ClassID, SubjectID FROM INTRANET_SUBJECT_LEADER WHERE UserID = '$targetUserID' AND RecordStatus = 1";
                 return $this->returnArray($sql,2);
           */
        }
        function isSubjectLeader($targetUserID)
        {
                 $info = $this->getSubjectLeaderInfo($targetUserID);
                 return (sizeof($info)!=0);
        }
        function getLeaderClass($targetUserID)
        {
                 $sql = "SELECT DISTINCT a.ClassID, b.Title
                         FROM INTRANET_SUBJECT_LEADER as a LEFT OUTER JOIN INTRANET_GROUP as b ON a.ClassID = b.GroupID
                         WHERE a.UserID = $targetUserID";
                 return $this->returnArray($sql,2);
        }
        function getLeaderSubject($targetUserID, $ClassID)
        {
                 $sql = "SELECT a.SubjectID, b.SubjectName
                         FROM INTRANET_SUBJECT_LEADER as a LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID
                         WHERE a.ClassID = $ClassID AND a.UserID = $targetUserID";
                 return $this->returnArray($sql,2);
        }

        function getHomeworkType($TypeName)
        {
                        $sql = "SELECT TypeID FROM INTRANET_HOMEWORK_TYPE WHERE TypeName = '".$TypeName."'";
                        $data = $this->returnVector($sql);
                        return $data[0];
        }
}


}        // End of directive
?>
