<?php
# using: 

############ Change Log [Start]
#	Date:	2014-08-15  Carlos
#			modified returnLatestTopic(), simplified the query and make it faster
#
#	Date:	2012-09-26	YatWoon
#			update returnThread(), 
#			- fixed: incorrect sql statment that some message will be failed to retrieve data
#			- improved: display English/Chinese name if nickname is empty
#					
#	Date:	2012-08-15	YatWoon
#			update returnLatestTopic(), remove "Private" icon
#
#	Date:	2010-12-13	YatWoon
#			update replyTable(), fixed: incorrect GroupID (cancel button)
#
##############################
class libbulletin extends libdb {

        var $Thread;
        var $ThreadID;
        var $BulletinID;
        var $ParentID;
        var $GroupID;
        var $UserID;
        var $UserName;
        var $UserEmail;
        var $Subject;
        var $Message;
        var $ReadFlag;
        var $RecordType;
        var $RecordStatus;
        var $DateInput;
        var $DateModified;
        var $PhotoLink;
        var $Attachment;
        var $PublicStatus;

        # ------------------------------------------------------------------------------------

        function libbulletin($BulletinID=0, $ThreadID=0){
                $this->libdb();
                $this->Thread = $this->returnThread($BulletinID);
                $this->ThreadID = $ThreadID;
                $this->BulletinID=$this->Thread[0][0];
                $this->ParentID=$this->Thread[0][1];
                $this->GroupID=$this->Thread[0][2];
                $this->UserID=$this->Thread[0][3];
                $this->UserName=$this->Thread[0][4];
                $this->UserEmail=$this->Thread[0][5];
                $this->Subject=$this->Thread[0][6];
                $this->Message=$this->Thread[0][7];
                $this->ReadFlag=$this->Thread[0][8];
                $this->RecordType=$this->Thread[0][9];
                $this->RecordStatus=$this->Thread[0][10];
                $this->DateInput=$this->Thread[0][11];
                $this->DateModified=$this->Thread[0][12];
                $this->Photolink=$this->Thread[0][13];
                $this->Attachment=$this->Thread[0][14];
                $this->PublicStatus=$this->Thread[0][15];
                $this->ReplyFlag=$this->Thread[0][16];
                $this->OnTop=$this->Thread[0][17];
        }

        function returnThread($BulletinID){
                $name_field = getNameFieldByLang("b.");
                //$sql = "SELECT a.BulletinID, a.ParentID, a.GroupID, a.UserID, a.UserName, a.UserEmail, a.Subject, a.Message, a.ReadFlag, a.RecordType, a.RecordStatus, a.DateInput, a.DateModified, b.PersonalPhotolink, a.Attachment, a.PublicStatus, a.ReplyFlag, a.OnTop FROM INTRANET_BULLETIN a , INTRANET_USER b WHERE (a.ParentID = '$BulletinID' OR a.BulletinID = '$BulletinID') and a.UserID = b.UserID ORDER BY a.BulletinID";
               $sql = "SELECT 
                			a.BulletinID, a.ParentID, a.GroupID, a.UserID, 
                			if(a.UserName='', $name_field, a.UserName), a.UserEmail, a.Subject, a.Message, a.ReadFlag, a.RecordType, a.RecordStatus, a.DateInput, a.DateModified, b.PersonalPhotolink, a.Attachment, a.PublicStatus, a.ReplyFlag, a.OnTop 
                		FROM 
                			INTRANET_BULLETIN a 
                			left join INTRANET_USER b on a.UserID=b.UserID
                		WHERE 
                			a.ParentID = '$BulletinID' OR a.BulletinID = '$BulletinID'
                		ORDER 
                			BY a.BulletinID";

                return $this->returnArray($sql,13);
                
        }

        function returnBulletin($ThreadID){
	        
	        	$Thread = $this->Thread;
	        	$this->BulletinID = $Thread[$ThreadID][0];
                $this->ParentID = $Thread[$ThreadID][1];
                $this->GroupID = $Thread[$ThreadID][2];
                $this->UserID = $Thread[$ThreadID][3];
                $this->UserName = $Thread[$ThreadID][4];
                $this->UserEmail = $Thread[$ThreadID][5];
                $this->Subject = $Thread[$ThreadID][6];
                $this->Message = $Thread[$ThreadID][7];
                $this->ReadFlag = $Thread[$ThreadID][8];
                $this->RecordType = $Thread[$ThreadID][9];
                $this->RecordStatus = $Thread[$ThreadID][10];
                $this->DateInput = $Thread[$ThreadID][11];
                $this->DateModified = $Thread[$ThreadID][12];
                if ($Thread[$ThreadID][13] =="")
                	$this->PhotoLink = "/images/myaccount_personalinfo/samplephoto.gif";
                else
                	$this->PhotoLink = $Thread[$ThreadID][13];
                $this->Attachment = $Thread[$ThreadID][14];	
                $this->PublicStatus = $Thread[$ThreadID][15];	
                $this->ReplyFlag = $Thread[$ThreadID][16];
                $this->OnTop = $Thread[$ThreadID][17];	
        }	

        # ------------------------------------------------------------------------------------

        function check($id){
                return "<input type=checkbox onClick=(this.checked)?setChecked(1,document.form1,'$id'):setChecked(0,document.form1,'$id')>";
        }

        function updateReadFlag(){
                global $UserID;
                $sql = "UPDATE INTRANET_BULLETIN SET ReadFlag = CONCAT(ReadFlag,';$UserID;') WHERE LOCATE(';$UserID;',ReadFlag)=0 AND BulletinID = ".$this->BulletinID;
                $this->db_db_query($sql);
        }
        
        function updateAttachFlag($AllowAtt, $GroupID){
                	
            $sql = "UPDATE INTRANET_GROUP SET ForumAttStatus ='".$this->Get_Safe_Sql_Query($AllowAtt)."' WHERE GroupID = '".$GroupID."'";

                $this->db_db_query($sql);
        }

        function returnReference(){
                $Thread = $this->Thread;
                for ($i=0; $i<sizeof($Thread); $i++)
                $x .= ($this->ThreadID == $Thread[$i][0]) ? ($i+1).",\n" : "<a href=javascript:bulletin_thread(".$i.")>".($i+1)."</a>,\n";
                $x .= ($this->ThreadID == -1) ? "All\n" : "<a href=javascript:bulletin_thread(-1)>All</a>\n";
                return $x;
        }

        # ------------------------------------------------------------------------------------

        function returnMessage($ThreadID){
                global $i_frontpage_bulletin_date, $i_frontpage_bulletin_subject, $i_frontpage_bulletin_author, $i_frontpage_bulletin_reference;
                
                $this->returnBulletin($ThreadID);
                $this->updateReadFlag();
              
                $x  = "<table width=95% border=0 cellpadding=2 cellspacing=1>\n";
                $x .= "<tr><td class=tableContent align=right width=20%>$i_frontpage_bulletin_subject:</td><td width=80%><b>".$this->Subject."</b></td></tr>\n";
                $x .= "<tr><td class=tableContent align=right>$i_frontpage_bulletin_date:</td><td>".$this->DateModified."</td></tr>\n";
                $x .= "<tr><td class=tableContent align=right>$i_frontpage_bulletin_author:</td><td>".$this->UserName." &lt; <a href=mailto:".$this->UserEmail.">".$this->UserEmail."</a> &gt;</td></tr>\n";
                $x .= "<tr><td class=tableContent align=right>$i_frontpage_bulletin_reference:</td><td>".$this->returnReference()."</td></tr>\n";
                $x .= "<tr><td class=tableContent align=right><br></td><td>".intranet_wordwrap($this->convertAllLinks(nl2br($this->Message)),25,"\n",1)."</td></tr>\n";
                $x .= "</table><br>\n";
                return $x;
        }
        function eCommReturnMessageTitle($ThreadID){
			 global $image_path, $LAYOUT_SKIN , $eComm, $ThreadID;
                
			 $this->returnBulletin($ThreadID);
             $this->updateReadFlag();
			 
			 $wrapped_subject = intranet_wordwrap($this->Subject,120,"\n",1);
                
                $x .="<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		        <tr>
		          <td width=\"4\" height=\"4\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_title_01.gif\" width=\"4\" height=\"4\"></td>
		          <td height=\"4\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_title_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_title_02.gif\" width=\"4\" height=\"4\"></td>
		          <td width=\"7\" height=\"4\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_title_03.gif\" width=\"7\" height=\"4\"></td>
		        </tr>
		        <tr>
		          <td width=\"4\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_title_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_title_04.gif\" width=\"4\" height=\"5\"></td>
		          <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">
		            <tr class=\"forumtabletop forumtabletoptext\">
		              <td align=\"left\" class=\"tabletext\"><strong>".$eComm['Title']." : ".$wrapped_subject."</strong> <span class=\"forumtablerow tabletext\">";
		              if ($this->PublicStatus!=1)
		              $x .="<img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/icon_private.gif\" width=\"18\" height=\"18\" align=\"absmiddle\">";
		              
		              $x .="</span></td>
		              <td width=\"100\">&nbsp;</td>
		            </tr>
		          </table></td>
		          <td width=\"7\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_title_06.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_title_06.gif\" width=\"7\" height=\"4\"></td>
		        </tr>
		        <tr>
		          <td width=\"4\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_title_07.gif\" width=\"4\" height=\"5\"></td>
		          <td height=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_title_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_title_08.gif\" width=\"5\" height=\"5\"></td>
		          <td width=\"7\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_title_09.gif\" width=\"7\" height=\"5\"></td>
		        </tr>
		      </table>
                
                ";
                return $x;
            }
            
            function eCommReturnMessageTitleSetting($ThreadID)
            {
			 global $image_path, $LAYOUT_SKIN , $eComm, $ThreadID;
                
			 $this->returnBulletin($ThreadID);
             $this->updateReadFlag();

                $x .="<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		        
		        <tr>
		          
		          <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">
		            <tr class=\"tablebottom\">
		              <td align=\"left\" class=\"tabletext\"><strong>".$eComm['Title']." : ".$this->Thread[0]['Subject']."</strong> <span class=\"forumtablerow tabletext\">";
		              if ($this->Thread[0]['PublicStatus']!=1)
		              $x .="<img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/icon_private.gif\" width=\"18\" height=\"18\" align=\"absmiddle\">";
		              
		              $x .="</span></td>
		              <td width=\"100\">&nbsp;</td>
		            </tr>
		          </table></td>
		          
		        </tr>
		        
		      </table>
                
                ";
                return $x;
            }
        function eCommReturnMessage($ThreadID)
        {
	        global $image_path, $LAYOUT_SKIN , $eComm;
	        
	        $this->returnBulletin($ThreadID);
            $this->updateReadFlag();
                
                
                #original post
                $x .= "
                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		        <tr>
		          <td width=\"100\" valign=\"top\"><table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">
		              <tr>
		                <td width=\"120\" align=\"center\"><table width=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		                    <tr>
		                      <td><div id=\"photo_border_nolink\"><span><img src=\"".$this->PhotoLink."\" width=\"60\" height=\"80\" border=\"0\"></span> </div></td>
		                    </tr>
		                </table></td>
		              </tr>
		              <tr>
		                <td width=\"120\" align=\"center\"><span class=\"share_file_title\"><strong><a href=mailto:".$this->UserEmail.">".$this->UserName."</a></strong></span></td>
		              </tr>
		          </table></td>
		          <td align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		              <tr>
		                <td width=\"25\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_01.gif\" width=\"25\" height=\"7\"></td>
		                <td height=\"7\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_02.gif\" width=\"12\" height=\"7\"></td>
		                <td width=\"9\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_03.gif\" width=\"9\" height=\"7\"></td>
		              </tr>
		              <tr>
		                <td width=\"25\" valign=\"top\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_04b.gif\" width=\"25\" height=\"40\"></td>
		                <td valign=\"top\" bgcolor=\"#FFFFFF\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		                    <tr>
		                      <td align=\"right\" class=\"tabletext  forumtablerow\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		                        <tr >
		                          <td align=\"left\" class=\"tabletext\"><strong>".$this->Subject."</strong></td>
		                          <td align=\"right\" class=\"tabletext\">".$this->DateModified."</td>
		                        </tr>
		                      </table></td>
		                    </tr>
		                    <tr>
		                    	<td>".$this->displayAttachment()."</td>
		                    </tr>
		                    <tr>
		                      <td class=\"tabletext\">".intranet_wordwrap($this->convertAllLinks(nl2br($this->Message)),25,"\n",1)."</td>
		                    </tr>
		                    <tr>
		                      <td height=\"10\" class=\"tabletext\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"10\"></td>
		                    </tr>
		                  </table>
		                  <br></td>
		                <td width=\"9\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_06.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_06.gif\" width=\"9\" height=\"80\"></td>
		              </tr>
		              <tr>
		                <td width=\"25\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_07.gif\" width=\"25\" height=\"10\"></td>
		                <td height=\"10\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_08.gif\" width=\"9\" height=\"10\"></td>
		                <td width=\"9\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_09.gif\" width=\"9\" height=\"10\"></td>
		              </tr>
		          	</table>
		            <br>
		            </td>
		       	 </tr>
		    	</table>";
                
                return $x;
        }
        function eCommReturnMessageSetting($ThreadID)
        {
	        global $image_path, $LAYOUT_SKIN , $eComm;
                $this->returnBulletin($ThreadID);
                $this->updateReadFlag();
                
                
                #original post
                $x .= "
                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		        <tr>
		          <td width=\"100\" valign=\"top\"><table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">
		              <tr>
		                <td width=\"120\" align=\"center\"><table width=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		                    <tr>
		                      <td><div id=\"photo_border_nolink\"><span><img src=\"".$this->PhotoLink."\" width=\"60\" height=\"80\" border=\"0\"></span> </div></td>
		                    </tr>
		                </table></td>
		              </tr>
		              <tr>
		                <td width=\"120\" align=\"center\"><span class=\"share_file_title\"><strong><a href=mailto:".$this->UserEmail.">".$this->UserName."</a></strong></span></td>
		              </tr>
		          </table></td>
		          <td align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		              <tr>
		                <td width=\"25\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_01.gif\" width=\"25\" height=\"7\"></td>
		                <td height=\"7\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_02.gif\" width=\"12\" height=\"7\"></td>
		                <td width=\"9\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_03.gif\" width=\"9\" height=\"7\"></td>
		              </tr>
		              <tr>
		                <td width=\"25\" valign=\"top\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_04b.gif\" width=\"25\" height=\"40\"></td>
		                <td valign=\"top\" bgcolor=\"#FFFFFF\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		                    <tr>
		                      <td align=\"right\" class=\"tabletext  forumtablerow\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		                        <tr >
		                          <td align=\"left\" class=\"tabletext\"><strong>".$this->Subject."</strong></td>
		                          <td align=\"right\" class=\"tabletext\">".$this->DateModified."</td>
		                        </tr>
		                      </table></td>
		                    </tr>
		                    <tr>
		                    	<td>".$this->displayAttachmentSetting()."</td>
		                    </tr>
		                    <tr>
		                      <td class=\"tabletext\">".intranet_wordwrap($this->convertAllLinks(nl2br($this->Message)),25,"\n",1)."</td>
		                    </tr>
		                    <tr>
		                      <td height=\"10\" class=\"tabletext\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"10\"></td>
		                    </tr>
		                  </table>
		                  <br></td>
		                <td width=\"9\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_06.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_06.gif\" width=\"9\" height=\"80\"></td>
		              </tr>
		              <tr>
		                <td width=\"25\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_07.gif\" width=\"25\" height=\"10\"></td>
		                <td height=\"10\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_08.gif\" width=\"9\" height=\"10\"></td>
		                <td width=\"9\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_09.gif\" width=\"9\" height=\"10\"></td>
		              </tr>
		          	</table>
		            <br>
		            </td>
		       	 </tr>
		    	</table>";
                
                return $x;
        }
        function displayMessage(){
                for($i=0; $i<sizeof($this->Thread); $i++){
                        if($this->ThreadID == -1 || $this->ThreadID == $i)
                        $x .= $this->returnMessage($i);
                }
                return $x;
        }
        
        function eCommDisplayOrgMessage(){
	        
            for($i=0; $i<sizeof($this->Thread); $i++)
            {
                    if($this->ThreadID == -1 || $this->ThreadID == $i)
                    {
						$wrapped_message = intranet_wordwrap($this->eCommReturnMessage($i),100,"\n",1);
                    	//$x .= $this->eCommReturnMessage($i);
                    	$x .= $wrapped_message;
                	}
            }
            return $x;
        }
        function eCommDisplayOrgMessageSetting(){
	        
            for($i=0; $i<sizeof($this->Thread); $i++)
            {
                    if($this->ThreadID == -1 || $this->ThreadID == $i)
                    {
                    	$x .= $this->eCommReturnMessageSetting($i);
                	}
            }
            return $x;
        }

        function displayThread($isAdmin=false){
                global $UserID, $image_path, $i_frontpage_bulletin_date, $i_frontpage_bulletin_subject, $i_frontpage_bulletin_author,$button_remove;
                $x .= "<table width=95% border=0 cellpadding=2 cellspacing=1>\n";
                $x .= "<tr>\n";
                $x .= "<td class=tableContent width=1><br></td>\n";
                $x .= "<td class=tableContent width=20%>$i_frontpage_bulletin_date</td>\n";
                $x .= "<td class=tableContent width=50%>$i_frontpage_bulletin_subject</td>\n";
                $x .= "<td class=tableContent width=30%>$i_frontpage_bulletin_author</td>\n";
                $x .= "</tr>\n";
                for($i=0; $i<sizeof($this->Thread); $i++){
                        $this->returnBulletin($i);
                        $IsNew = (!strstr($this->ReadFlag,";$UserID;")) ? "<img src='$image_path/new.gif' border=0 hspace=2>" : "";
                        $x .= "<tr>\n";
                        $x .= ($i == 0) ? "<td><img src='$image_path/bulletin-msg.gif' border=0></td>\n" : "<td><img src='$image_path/bulletin-link.gif' border=0></td>\n";
                        $x .= "<td>".$this->DateModified."</td>\n";
                        $x .= ($i == $this->ThreadID) ? "<td><b>".$this->Subject."</b>$IsNew\n" : "<td><a href=javascript:bulletin_thread(".$i.")>".intranet_wordwrap($this->Subject,17,"\n",1)."</a>$IsNew";
                        if ($isAdmin && $this->ParentID != 0)
                            $x .= "<a href=javascript:del(".$this->BulletinID.")><img src=../../../images/eraser_icon.gif alt=\"$button_remove\" hspace=20 vspace=2 border=0 align=absmiddle></a>";
                        $x .= "</td>\n";
                        $x .= "<td><a href=mailto:".$this->UserEmail.">".intranet_wordwrap($this->UserName,15,"\n",1)."</a></td>\n";
                        $x .= "</tr>\n";
                }
                $x .= "</table>\n";
                return $x;
        }
		
        function eCommDisplayThread($isAdmin=false){
                global $UserID, $image_path, $LAYOUT_SKIN,$i_no_record_exists_msg,$button_remove;
                global $pageNo, $num_per_page;
				
	        
                if (sizeof($this->Thread)<=1)
                {
	             
                        $x .="<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
                  <tr class=\"forumtabletoptext\">
                    <td width=\"100\"><table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">
                        <tr>
                          <td width=\"120\" align=\"center\"><table width=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
                              <tr>
                                <td ></td>
                              </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td width=\"120\" align=\"center\" class=\"tabletext\"></td>
                        </tr>
                    </table></td>
                    <td align=\"left\" valign=\"top\">".$i_no_record_exists_msg."</td>
                  </tr>
                </table>";   
                }
            
            $pageNo = $pageNo ? $pageNo : 1;
            $num_per_page = $num_per_page ? $num_per_page : 50;
			$start = ($pageNo-1)*$num_per_page;
			
	        if($pageNo==1)
	        {
				$start = 1;
				$end = $start+$num_per_page-1;
			}
			else
			{
				$end = $start+$num_per_page+1;
			}
                
                for($i=$start; ($i<sizeof($this->Thread) and $i<$end); $i++)
                {
	                
                        $this->returnBulletin($i);
                 
                        $x .="<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
                  <tr>
                    <td width=\"100\"><table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">
                        <tr>
                          <td width=\"120\" align=\"center\"><table width=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
                              <tr>
                                <td><div id=\"div\"><span><img src=\"".$this->PhotoLink."\" width=\"45\" height=\"60\" border=\"0\"></span> </div></td>
                              </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td width=\"120\" align=\"center\"><span class=\"share_file_title\"><strong><a href=mailto:".$this->UserEmail.">".$this->UserName."</a></strong></span></td>
                        </tr>
                    </table></td>
                    <td align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                        <tr>
                          <td width=\"25\" height=\"7\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_01.gif\" width=\"25\" height=\"7\"></td>
                          <td height=\"7\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_02.gif\" width=\"12\" height=\"7\"></td>
                          <td width=\"9\" height=\"7\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_03.gif\" width=\"9\" height=\"7\"></td>
                        </tr>
                        <tr>
                          <td width=\"25\" valign=\"top\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_04b.gif\" width=\"25\" height=\"40\"></td>
                          <td valign=\"top\" bgcolor=\"#FFFFFF\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
                              <tr>
                                <td align=\"right\" class=\"tabletext forumtablerow\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
                                    <tr >
                                      <td align=\"left\" class=\"tabletext\"><strong>".intranet_wordwrap($this->Subject,17,"\n",1)."</strong></td>
                                      <td align=\"right\" class=\"tabletext\">".$this->DateModified."</td>
                                    </tr>
                                </table></td>
                              </tr>
                              <tr>
                                <td class=\"tabletext\">".intranet_wordwrap($this->convertAllLinks(nl2br($this->Message)),25,"\n",1)."</td>
                              </tr>
                              <tr>
                                <td height=\"10\" class=\"tabletext\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"10\"></td>
                              </tr>
                          </table></td>
                          <td width=\"9\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_06.gif\" width=\"9\" height=\"80\"></td>
                        </tr>
                        <tr>
                          <td width=\"25\" height=\"10\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_07.gif\" width=\"25\" height=\"10\"></td>
                          <td height=\"10\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_08.gif\" width=\"9\" height=\"10\"></td>
                          <td width=\"9\" height=\"10\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_09.gif\" width=\"9\" height=\"10\"></td>
                        </tr>
                    </table></td>
                  </tr>
                </table>";
                }
                
               
                
                return $x;
        }
        function eCommDisplayThreadSetting($isAdmin=false)
        {
                global $UserID, $image_path, $LAYOUT_SKIN,$i_no_record_exists_msg,$button_remove;
                global $pageNo, $num_per_page;
				
	        
                if (sizeof($this->Thread)<=1)
                {
	             
                        $x .="<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
                  <tr class=\"forumtabletoptext\">
                    <td width=\"100\"><table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">
                        <tr>
                          <td width=\"120\" align=\"center\"><table width=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
                              <tr>
                                <td ></td>
                              </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td width=\"120\" align=\"center\" class=\"tabletext\"></td>
                        </tr>
                    </table></td>
                    <td align=\"left\" valign=\"top\">".$i_no_record_exists_msg."</td>
                  </tr>
                </table>";   
                }
            
            $pageNo = $pageNo ? $pageNo : 1;
            $num_per_page = $num_per_page ? $num_per_page : 50;
			$start = ($pageNo-1)*$num_per_page;
			
	        if($pageNo==1)
	        {
				$start = 1;
				$end = $start+$num_per_page-1;
			}
			else
			{
				$end = $start+$num_per_page+1;
			}
                
                for($i=$start; ($i<sizeof($this->Thread) and $i<$end); $i++)
                {
	                
                  $this->returnBulletin($i);
                  
                 $del_btn="<a href=\"javascript:RemoveThread(".$this->Thread[$i]['BulletinID'].",'removeThread.php')\" class=\"tabletool\"><img src=$image_path/$LAYOUT_SKIN/icon_delete.gif width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">".$button_remove."</a>";
                        $x .="<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
                  <tr>
                  	<td width=\"2\">".$del_btn."</td>
                    <td width=\"100\"><table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">
                        <tr>
                          <td width=\"120\" align=\"center\"><table width=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
                              <tr>
                                <td><div id=\"div\"><span><img src=\"".$this->PhotoLink."\" width=\"45\" height=\"60\" border=\"0\"></span> </div></td>
                              </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td width=\"120\" align=\"center\"><span class=\"share_file_title\"><strong><a href=mailto:".$this->UserEmail.">".$this->UserName."</a></strong></span></td>
                        </tr>
                    </table></td>
                    <td align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                        <tr>
                          <td width=\"25\" height=\"7\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_01.gif\" width=\"25\" height=\"7\"></td>
                          <td height=\"7\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_02.gif\" width=\"12\" height=\"7\"></td>
                          <td width=\"9\" height=\"7\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_03.gif\" width=\"9\" height=\"7\"></td>
                        </tr>
                        <tr>
                          <td width=\"25\" valign=\"top\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_04b.gif\" width=\"25\" height=\"40\"></td>
                          <td valign=\"top\" bgcolor=\"#FFFFFF\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
                              <tr>
                                <td align=\"right\" class=\"tabletext forumtablerow\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
                                    <tr >
                                      <td align=\"left\" class=\"tabletext\"><strong>".intranet_wordwrap($this->Subject,17,"\n",1)."</strong></td>
                                      <td align=\"right\" class=\"tabletext\">".$this->DateModified."</td>
                                    </tr>
                                </table></td>
                              </tr>
                              <tr>
                                <td class=\"tabletext\">".intranet_wordwrap($this->convertAllLinks(nl2br($this->Message)),25,"\n",1)."</td>
                              </tr>
                              <tr>
                                <td height=\"10\" class=\"tabletext\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"10\"></td>
                              </tr>
                          </table></td>
                          <td width=\"9\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_06.gif\" width=\"9\" height=\"80\"></td>
                        </tr>
                        <tr>
                          <td width=\"25\" height=\"10\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_07.gif\" width=\"25\" height=\"10\"></td>
                          <td height=\"10\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_08.gif\" width=\"9\" height=\"10\"></td>
                          <td width=\"9\" height=\"10\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_bubble_re_09.gif\" width=\"9\" height=\"10\"></td>
                        </tr>
                    </table></td>
                  </tr>
                </table>";
                }
                
               
                
                return $x;
        }
		# for alumni 
        function returnMessage2($ThreadID){
                global $i_frontpage_bulletin_date, $i_frontpage_bulletin_subject, $i_frontpage_bulletin_author, $i_frontpage_bulletin_reference, $i_frontpage_bulletin_message;
                $this->returnBulletin($ThreadID);
                $this->updateReadFlag();
                $x  = "<table width=95% border='1' cellpadding='2' cellspacing='0' bordercolorlight='#5DA5C9' bordercolordark='#FFFFFF' bgcolor='#FFFFFF' class='13-black'>\n";
                $x .= "<tr><td bgcolor='#E3DB9C' align='left' valign='top' class='13-black-bold' width=20%>$i_frontpage_bulletin_subject:</td><td width=80%><b>".$this->Subject."</b></td></tr>\n";
                $x .= "<tr><td bgcolor='#E3DB9C' align='left' valign='top' class='13-black-bold'>$i_frontpage_bulletin_date:</td><td>".$this->DateModified."</td></tr>\n";
                $x .= "<tr><td bgcolor='#E3DB9C' align='left' valign='top' class='13-black-bold'>$i_frontpage_bulletin_author:</td><td>".$this->UserName." &lt; <a href=mailto:".$this->UserEmail.">".$this->UserEmail."</a> &gt;</td></tr>\n";
                $x .= "<tr><td bgcolor='#E3DB9C' align='left' valign='top' class='13-black-bold'>$i_frontpage_bulletin_reference:</td><td>".$this->returnReference()."</td></tr>\n";
                $x .= "<tr><td bgcolor='#E3DB9C'  align='left' valign='top' class='13-black-bold'>$i_frontpage_bulletin_message</td><td>".intranet_wordwrap($this->convertAllLinks(nl2br($this->Message)),25,"\n",1)."</td></tr>\n";
                $x .= "</table><br>\n";
                return $x;
        }

		# for alumni
        function displayMessage2(){
                for($i=0; $i<sizeof($this->Thread); $i++){
                        if($this->ThreadID == -1 || $this->ThreadID == $i)
                        $x .= $this->returnMessage2($i);
                }
                return $x;
        }

		# for alumni
        function displayThread2($isAdmin=false){
                global $UserID, $image_path, $i_frontpage_bulletin_date, $i_frontpage_bulletin_subject, $i_frontpage_bulletin_author,$button_remove;
                $x .= "<table width=95% border='1' cellpadding='2' cellspacing='0' bordercolorlight='#5DA5C9' bordercolordark='#FFFFFF' bgcolor='#FFFFFF' class='13-black'>\n";
                $x .= "<tr bgcolor='#E3DB9C'>\n";
                $x .= "<td align='left' valign='top' class='13-black-bold' width=1><br></td>\n";
                $x .= "<td align='left' valign='top' class='13-black-bold' width=20%>$i_frontpage_bulletin_date</td>\n";
                $x .= "<td align='left' valign='top' class='13-black-bold' width=50%>$i_frontpage_bulletin_subject</td>\n";
                $x .= "<td align='left' valign='top' class='13-black-bold' width=30%>$i_frontpage_bulletin_author</td>\n";
                $x .= "</tr>\n";
                for($i=0; $i<sizeof($this->Thread); $i++){
                        $this->returnBulletin($i);
                        $IsNew = (!strstr($this->ReadFlag,";$UserID;")) ? "<img src='$image_path/new.gif' border=0 hspace=2>" : "";
                        $x .= "<tr>\n";
                        $x .= ($i == 0) ? "<td><img src='$image_path/bulletin-msg.gif' border=0></td>\n" : "<td><img src='$image_path/bulletin-link.gif' border=0></td>\n";
                        $x .= "<td>".$this->DateModified."</td>\n";
                        $x .= ($i == $this->ThreadID) ? "<td><b>".$this->Subject."</b>$IsNew\n" : "<td><a href=javascript:bulletin_thread(".$i.")>".intranet_wordwrap($this->Subject,17,"\n",1)."</a>$IsNew";
                        if ($isAdmin && $this->ParentID != 0)
                            $x .= "<a href=javascript:del(".$this->BulletinID.")><img src=../../../images/eraser_icon.gif alt=\"$button_remove\" hspace=20 vspace=2 border=0 align=absmiddle></a>";
                        $x .= "</td>\n";
                        $x .= "<td><a href=mailto:".$this->UserEmail.">".intranet_wordwrap($this->UserName,15,"\n",1)."</a></td>\n";
                        $x .= "</tr>\n";
                }
                $x .= "</table>\n";
                return $x;
        }

		function replySubject(){
                $id = ($this->ThreadID == -1) ? sizeof($this->Thread)-1 : $this->ThreadID;
                $this->returnBulletin($id);
                return "RE:".$this->Subject;
        }
        
        function replyTable()
        {
	     if ($this->ReplyFlag==1)
	     {
		     global $image_path, $LAYOUT_SKIN, $eComm, $button_submit, $button_reset, $button_cancel;
		  $x="
          <tr ><td align=\"left\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"><span class=\"tabletext\"><strong>".$eComm['YourReply']."</strong></span></td></tr>
          <tr ><td align=\"left\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
          <tr class=\"forumtabletoptext\"><td width=\"100\">".$eComm['Title']."</td>  
              <td><input name=\"Subject\" type=\"text\" class=\"textboxtext2\" value=\"";
              $x.=$this->replySubject();
              $x .="\"></td>  
          </tr>
          <tr class=\"forumtabletoptext\"><td>".$eComm['Content']."</td>
              <td><textarea class=\"textboxtext\" name=\"Message\" cols=\"70\" rows=\"2\" wrap=\"virtual\" onFocus=\"this.rows=5; \"></textarea></td>   
          </tr>
              </table></td>
          </tr>
		  <tr>
            <td align=\"right\" class=\"dotline\">&nbsp;</td>
          </tr>
          <tr>
            <td align=\"center\">
            <input type=\"button\" class=\"formbutton print_hide\" onClick=\"javascript:bulletin_post(document.form1)\" name=\"submit2\" id=\"submit2\" value=\"". $button_submit ."\"   class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"/>
			<input type=\"reset\" class=\"formbutton print_hide\"  name=\"reset2\" id=\"reset2\" value=\"". $button_reset ."\"   class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"/>
			<input type=\"button\" class=\"formbutton print_hide\" onClick=\"window.location='index.php?GroupID=". $this->GroupID ."'\" name=\"cancelbtn\" id=\"cancelbtn\" value=\"". $button_cancel ."\"   class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"/>
            </td></tr>
          ";
      		
          
	     }
	     return $x;   
        }

        function replyContent(){
                $id = ($this->ThreadID == -1) ? sizeof($this->Thread)-1 : $this->ThreadID;
                $this->returnBulletin($id);
                $x .= "\n\n &gt;";
                $x .= str_replace("\n", "\n &gt; ", $this->Message);
                $x .= "\n ";
                return $x;
        }

        function replyEmail(){
                for ($i=0; $i<sizeof($this->Thread); $i++){
                        $this->returnBulletin($i);
                        $x .= $this->UserEmail.",";
                }
                $x = substr($x, 0, strlen($x)-1);
                return $x;
        }

        function replyThreadSize(){
                return sizeof($this->Thread);
        }
        
        function displayAttachment()
        {
                 global $file_path, $image_path, $intranet_httppath, $PATH_WRT_ROOT,$button_remove,$LAYOUT_SKIN;
                 include_once($PATH_WRT_ROOT."includes/libfiletable.php");
                 $path = "$file_path/file/eComm/bulletin/". $this->Attachment;
                 
                 $a = new libfiletable("", $path, 0, 0, "");
                 $files = $a->files;
                 while (list($key, $value) = each($files))
                 {
	                 if($this->UserID==$_SESSION['UserID'])
						$del_btn="<a href=\"javascript:checkRemoveThis(document.form1,'BulletinID','removeAtt.php','".$files[$key][0]."')\" class=\"tabletool\"><img src=$image_path/$LAYOUT_SKIN/icon_delete.gif width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">".$button_remove."</a>";				 
					//$url = str_replace(" ", "%20", str_replace($file_path, "", $path)."/". $files[$key][0]);
					$url = str_replace(" ", "%20", str_replace($file_path, "", $path)."/". urlencode($files[$key][0]));
					$url = str_replace("+", " ", $url);
					
					$x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
					$x .= "<a class='tablelink' target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='". str_replace("'","''",$files[$key][0]) ."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
					$x .= "<span class=\"forumtabletoptext\"> (".ceil($files[$key][1]/1000)."Kb)</span> ".$del_btn;
                        
                        
                 }
                 return $x;
        }
        function displayAttachmentSetting()
        {
                 global $file_path, $image_path, $intranet_httppath, $PATH_WRT_ROOT,$button_remove,$LAYOUT_SKIN;
                 include_once($PATH_WRT_ROOT."includes/libfiletable.php");
                 $path = "$file_path/file/eComm/bulletin/".$this->Attachment;
                 $a = new libfiletable("", $path, 0, 0, "");
                 $files = $a->files;
                 
                 
                 $del_btn="<a href=\"javascript:checkRemoveThis(document.form1,'BulletinID','removeAtt.php')\" class=\"tabletool\"><img src=$image_path/$LAYOUT_SKIN/icon_delete.gif width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">".$button_remove."</a>";
                 while (list($key, $value) = each($files))
                 {
                        $url = str_replace(" ", "%20", str_replace($file_path, "", $path)."/".$files[$key][0]);
                        $x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                        $x .= "<a class='tablelink' target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $x .= "<span class=\"forumtabletoptext\"> (".ceil($files[$key][1]/1000)."Kb)</span> ".$del_btn;
                        
                        
                 }
                 return $x;
        }
        function returnLatestTopic($GroupID, $limit=999)
        {
	        global $image_path, $LAYOUT_SKIN, $UserID, $PATH_WRT_ROOT;
			//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");	        
			include_once($PATH_WRT_ROOT."includes/libuser2007a.php");	        
			
			$lu2007 = new libuser2007($UserID);
	        if($lu2007->isInGroup($GroupID))
			{
				//$cond1 = "if (c.PublicStatus=1 ,'&nbsp;', CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif\">')) ,";
				$cond1 = "if (a.PublicStatus=1 ,'&nbsp;', CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif\">')) ,";
			}
			else{
			$cond2 = "AND a.PublicStatus=1";}
	        
			/*
			CONCAT(if (c.PublicStatus=1 ,'&nbsp;', CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif\">')),
	 							if (c.onTop=1 ,CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_ontop.gif\">'),'&nbsp;' )),
	 							*/
	        // Original query before 2014-08-15
	        /*
	        $sql = "
	        	SELECT					CONCAT(if (c.onTop=1 ,CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_ontop.gif\">'),'&nbsp;' )),
                        CONCAT('<a href=javascript:bulletin_view(', a.BulletinID, ') class=\"tablelink\">', a.Subject, '</a>',if(locate(';$UserID;',a.ReadFlag)=0, '<img src=$image_path/{$LAYOUT_SKIN}/alert_new.gif border=0 hspace=2>', '') $deleteIcon) as topic,
                        if (e.PersonalPhotoLink!=\"\", CONCAT('<img src=\"',e.PersonalPhotoLink,'\" width=\"25\" height=\"30\">'), CONCAT('<img src=\"/images/myaccount_personalinfo/samplephoto.gif\" width=\"25\" height=\"30\">')),
                        a.UserName AS author, c.onTop,
                        if(COUNT(d.BulletinID)=0, CONCAT(COUNT(c.BulletinID)-1), CONCAT(COUNT(c.BulletinID)-1 )),
                        DATE_FORMAT(MAX(c.DateModified), '%Y-%m-%d %H:%i') AS postdate
                FROM INTRANET_BULLETIN AS a
                LEFT OUTER JOIN INTRANET_USER AS e ON (e.UserID = a.UserID)
                LEFT OUTER JOIN INTRANET_BULLETIN AS c ON (a.BulletinID = c.BulletinID OR a.BulletinID = c.ParentID)
                LEFT OUTER JOIN INTRANET_BULLETIN AS d ON
                        (a.BulletinID = d.BulletinID OR a.BulletinID = d.ParentID) AND
                        c.BulletinID = d.BulletinID AND
                        d.ParentID<>0 AND
                        d.ReadFlag not like '%;$UserID;%'
                WHERE a.GroupID = $GroupID AND a.ParentID = 0
                ".$cond2."
                GROUP BY a.BulletinID
                order by postdate desc 
                limit $limit
            ";
            */
             $sql = "SELECT	CONCAT(if (a.onTop=1 ,CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_ontop.gif\">'),'&nbsp;' )),
	                        CONCAT('<a href=javascript:bulletin_view(', a.BulletinID, ') class=\"tablelink\">', a.Subject, '</a>',if(locate(';$UserID;',a.ReadFlag)=0, '<img src=$image_path/{$LAYOUT_SKIN}/alert_new.gif border=0 hspace=2>', '') $deleteIcon) as topic,
	                        if (e.PersonalPhotoLink!=\"\", CONCAT('<img src=\"',e.PersonalPhotoLink,'\" width=\"25\" height=\"30\">'), CONCAT('<img src=\"/images/myaccount_personalinfo/samplephoto.gif\" width=\"25\" height=\"30\">')),
	                        a.UserName AS author, d.onTop,
	                        COUNT(d.BulletinID),
	                        DATE_FORMAT(IF(d.BulletinID IS NULL, a.DateModified, MAX(d.DateModified)), '%Y-%m-%d %H:%i') AS postdate
	                FROM INTRANET_BULLETIN AS a
	                LEFT JOIN INTRANET_USER AS e ON (e.UserID = a.UserID)
	                LEFT JOIN INTRANET_BULLETIN AS d ON
	                        a.BulletinID = d.ParentID 
	                        AND d.ParentID<>0 ";
	          //$sql .= " AND d.ReadFlag not like '%;$UserID;%' ";
	          $sql.=" WHERE a.GroupID = '$GroupID' AND a.ParentID = 0
	                ".$cond2."
	                GROUP BY a.BulletinID
	                order by postdate desc 
	                limit $limit
	            ";
            
            $x = $this->returnArray($sql,7);
            
			return $x;
		
			
        }
        
        function returnMyTopic($GroupID, $limit=999)
        {
	        global $image_path, $LAYOUT_SKIN, $UserID, $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");	        
			
			$lu2007 = new libuser2007($UserID);
	        if($lu2007->isInGroup($GroupID))
			{
				$cond1 = "if (c.PublicStatus=1 ,'&nbsp;', CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif\">')) ,";
			}
			else{
			$cond2 = "AND a.PublicStatus=1";}
	        
	        $sql = "
	        	SELECT					CONCAT(if (c.PublicStatus=1 ,'&nbsp;', CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif\">')),
	 							if (c.onTop=1 ,CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_ontop.gif\">'),'&nbsp;' )),
                        CONCAT('<a href=javascript:bulletin_view(', a.BulletinID, ') class=\"tablelink\">', a.Subject, '</a>',if(locate(';$UserID;',a.ReadFlag)=0, '<img src=$image_path/{$LAYOUT_SKIN}/alert_new.gif border=0 hspace=2>', '') $deleteIcon) as topic,
                        if (e.PersonalPhotoLink!=\"\", CONCAT('<img src=\"',e.PersonalPhotoLink,'\" width=\"25\" height=\"30\">'), CONCAT('<img src=\"/images/myaccount_personalinfo/samplephoto.gif\" width=\"25\" height=\"30\">')),
                        a.UserName AS author, c.onTop,
                        if(COUNT(d.BulletinID)=0, CONCAT(COUNT(c.BulletinID)-1), CONCAT(COUNT(c.BulletinID)-1 )),
                        DATE_FORMAT(MAX(c.DateModified), '%Y-%m-%d %H:%i') AS postdate
                FROM INTRANET_BULLETIN AS a
                LEFT OUTER JOIN INTRANET_USER AS e ON (e.UserID = a.UserID)
                LEFT OUTER JOIN INTRANET_BULLETIN AS c ON (a.BulletinID = c.BulletinID OR a.BulletinID = c.ParentID)
                LEFT OUTER JOIN INTRANET_BULLETIN AS d ON
                        (a.BulletinID = d.BulletinID OR a.BulletinID = d.ParentID) AND
                        c.BulletinID = d.BulletinID AND
                        d.ParentID<>0 AND
                        d.ReadFlag not like '%;$UserID;%'
                WHERE a.GroupID = '$GroupID' AND a.ParentID = 0
                AND c.UserID = '$UserID'
                ".$cond2."
                GROUP BY a.BulletinID
                order by postdate desc 
                limit $limit
            ";
            
            $x = $this->returnArray($sql,7);
            
			return $x;
		
			
        }
        
        function CheckForumAttStatus($GroupID)
        {
	        $sql = "Select ForumAttStatus from INTRANET_GROUP where GroupID='$GroupID'";
	        
	        $x = $this->returnArray($sql,1);
	        
	        return $x;
        }
        
}
?>