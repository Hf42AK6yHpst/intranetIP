<?php
# Modifing by 

# Under Development : 
/** 
*
* Date      : 2020-09-25 Cameron
*             fix to avoid divided by zero in built_sql() [case #R197957]
*
* Date      : 2020-03-17 Tommy
*             modified built_sql(), check $this->db_db_query is null or not to check sql error
* 
* Date		: 2019-07-30 Paul
* 			  Modified page_size_input_ip25() , page_size_input() for using numPerPage_name & pageNo_name in the script instead of fixed name
* 
* Date		: 2019-04-30 Carlos
*			  Added replaceAllOnEventHandlers($html) for replace all onXXXXXX expression with random id string.
*			  Modified displayCell() to apply replaceAllOnEventHandlers($html).
*
* Date      : 2019-04-29 (Anna)
*             Modified displayCell() - added cleanHtmlJavascript when display data in case 0  
* 
* Date      : 2019-04-01 (Bill)
*             add iPortfolio_DBS_Predicted_Grade_IB in display()
*
* Date      : 2018-10-31 (Bill)
*             add iPortfolio_DBS_Predicted_Grade in display()
* 
* Date      : 2018-04-04 (Cameron)
*             add list4ajax in display()
* 
* Date		: 2017-09-13 (Omas)
* 			  modified displayCell(), displayFormat6() - replace split by explode
* 
* Date		: 2017-04-24 (Villa)
* 			  modified display(): add case Enrolment_Display_Event_ReplySlip_Cust for Enrol ReplySlip Cust
* 
* Date		: 2017-04-12 (Cameron)
* 			  add case 23 to handle special characters in displayCell() [DM#3171]
* 
* Date		: 2016-08-17 (Bill)		[2016-0801-1134-09054]
* 			  modified display(), to pass 1 parm to displayFormat_eNoticedisplayNoticePrint()
* Date		: 2016-08-16 (Ronald)
* 				modified  display(), add case UserMgmtStaffAccountCentennial
* Date		: 2016-04-19 (Omas)
* 				modified displayCell(), add case 21 - for formated number , case 22 for Get_String_Display 
* Date 	    : 2015-02-09 (Ryan) 
*				add eLibplus Search User display format Case eLibPlus_Search_User_Table
* 				displayFormat_eLibPlus_Search_User_Table() @2007a  
* Date		: 2013-03-04 (YatWoon)
* Description	: comment out built_sql() 2013-02-26 (Ivan) coding first [Case#K59414]
* 
* Date		: 2013-02-26 (Ivan)
* Description	: modified built_sql(), added $specialCount condition for $this->total_row. Otherwise, total number of record counting will be wrong if there are only 1 row of returned data
* 
* Date		: 2013-09-10 (Siuwan)
* Description 	: add display() IsColOff case eLibPlus_Circulation_Detail_Table
* 
* Date		: 2013-07-11 (Henry)
* Description 	: add display() IsColOff case Digital_Archive_File_Format_Table
* 
* Date		: 2013-03-25 (YatWoon)
* Descipriont	: updated display(), add displayFormat_StudentRegistry_StudentList_HK
* Date		: 2013-03-21 (Ivan)
* Description 	: add display IsColOff case "MessageCenter_SmsIndex"
* 
* Date		: 2013-02-06 (Siuwan)
* Description 	: add display IsColOff case ePost_portfolio_report_new

* Date		: 2012-06-07 (Ivan) [2012-0607-0959-33073]
* Description	: updated built_sql(), added "$this->IsColOff =='UserMgmtStudentNoClass'" to the special row counting method logic
* 
* Date		: 2012-06-06 (Thomas)
* Description 	: add display IsColOff case ePost_report, ePost_portfolio_report, ePost_article_shelf
* 
* Date		: 2012-02-16 (YatWoon)
* Description	: updated display(), add parameter to displayFormat_displayStudentHistory()
*
* Date      : 2011-12-09 (Henry Chow)
* Description   : add display() IsColOff eDisciplineDetentionStudentRecordInStudentView (for eService > eDis >  Detention)
* 
* Date      : 2011-12-02 (Connie)
* Description   : add display() IsColOff ERC_rubrics_CommentBank_SubjectDetailsComment 
* 
* Date		: 2011-09-26 (Henry Chow)
* Description	: add display IsColOff case Invoice_List_Table
*
* Date		: 2011-05-24 (YatWoon)
* Description	: add display IsColOff case Enrolment_Display_Club
*
* Date		: 2011-05-16 (Henry Chow)
* Description	: add display() IsColOff case Digital_Archive_Admin_Search_Result_Table
* 
* Date		: 2011-05-11 (Henry Chow)
* Description	: add display() IsColOff case Study_Score_Management
* 
* Date		: 2011-05-09 (Henry Chow)
* Description	: add display() IsColOff case DigitalArchive_Newest_More_Table
* 
* Date		: 2011-02-24 (Henry Chow)
* Description	: add display() IsColOff case eDiscipline_WebSAMS_NotTransferredIndex
* 
* Date		: 2011-02-23 (Henry Chow)
* Description	: add display() IsColOff case eDiscipline_GM_Index
* 
* Date		: 2011-02-23 (Carlos)
* Description	: add display() IsColOff case ReadingScheme_RecommendReportComment
* 
* Date		: 2011-02-23 (Marcus)
* Description	: add display() IsColOff case ReadingSchemeReportComment
* 
* Date		: 2011-02-21 (Henry Chow)
* Description	: add display() IsColOff case OEA_StudentList
* 
* Date		: 2011-01-11 (Marcus)
* Description	: add display() IsColOff case ReadingScheme_MyReadingBook
* 
* Date		: 2011-01-10 (Carlos)
* Description	: add display() IsColOff case ReadingScheme_RecommendBookCoverView
* 
* Date      : 2010-08-06 (Carlos)
* Description   : add count_mode, count the size of return array of sql when built_sql 
* 
* Date 		: 2010-08-05 (Henry Chow)
* Description	: add display(), add 1 more case "Malaysia_RegistryStatus"
*
* Date 		: 2010-07-29 (Henry Chow)
* Description	: add column_IP25(), for new IP25 layout format
*
* Date 		: 2010-07-20 (Henry Chow)
* Description	: modified display(), add 1 case "StudentRegistry_StudentList"
*
* Date 		: 2010-07-20 (Henry Chow)
* Description	: added navigation_IP25(), record_range_ip25(), prev_n_ip25(), go_page_ip25(), next_n_ip25(), page_size_input_ip25(), for new IP25 layout format
*
* Date 		: 2010-07-20 (Henry Chow)
* Description	: modified display(), add 1 case "IP25_table"
*
* Date 		: 2010-07-19 (Marcus)
* Description	: modified display()
* 				  add 1 more case "GroupSettingGroupLis"
* 
* Date 		: 2010-06-21 (Henry)
* Description	: modified display()
* 				  add 1 more case "UserMgmtStudentAccount"
*
* Date			: 2010-06-02 (Henry Chow)
* Description	: Added Case for display() "UserMgmtStudentNoClass""
* 
* Date		: 2010-05-18 Max (201005181005)
* Description	: Added Case for display() "module_manage_license""
* 
* Date 		: 2010-04-27 (Henry)
* Description	: modified display()
* 				  add 1 more case "UserMgmtParentAccount"
*
* Date		: 2010-04-30 (Ronald)
* Description	: modified function libdbtable(), if the flag $pageSizeChangeEnabled is empty, default set it to true. 
*
* Date 		: 2010-04-27 (Henry)
* Description	: modified display()
* 				  add 1 more case "UserMgmtStaffAccount"
*
* Date 		: 2010-04-20 (Henry)
* Description	: modified display()
* 				  add 1 more case "eAttendance_ParentLetter"
*
* Type			: Enhancement
* Date 		: 200911230911
* Description	: Teacher role
* 				  1) Add fields [Allow student to join], [Join period]
* 				  Student role
* 				  2C) Allow students to see the list of Student Learning Profile(SLP) to join, show within editable period and type == 1
* By			: Max Wong
* Case Number	: 200911230911MaxWong
* C=CurrentIssue 
*/
class libdbtable extends libdb{

        var $field;
        var $order;
        var $pageNo;
        var $custom_css;
        var $field_array;
        var $column_array;
        var $column_list;
        var $sql;
        var $result;
        var $db;
        var $rs;
        var $page_size;
        var $view_type;
        var $title;
        var $no_col;
        var $no_msg;
        var $bgcolor=array("#EEEEEE", "#DDDDDD");
        var $tableColor = "#FFFFFF";
        var $sortby;
        var $checkall;
        var $clearall;
        var $total;
        var $pagename;
        var $pagePrev;
        var $pageNext;
        var $IsColOff;
        var $wrap_array;
//        var $PageSizeInterval = 10;

        var $n_start;

     var $noNumber;          //3.0 use index from 1 to n?
     var $total_row;          //3.0 select only the data inside the range and this field is the total no. of data fullfilled the query
     var $total_row2;          // from result array
     var $QueType;              // QB added
     var $imagePath;
     var $form_name;
     var $pageNo_name;
     var $numPerPage_name;
     var $order_name;
     var $form_field_name;

     	var $fieldorder2;
     	var $record_num_mode = ""; // for intranet iportfolio 2.5 school based scheme (updated built_sql function)
      var $with_navigation = ''; // table with 'record / total' row or not
      var $count_mode = "";
      
      var $custDataset = false;
      var $custDatasetArr = array();
        # ------------------------------------------------------------------------------------
        

        function libdbtable($f=0, $o=0, $p=0 ,$c=false){
                global $list_sortby, $button_check_all, $button_clear_all, $list_total, $list_page, $list_prev, $list_next;
                global $intranet_db, $page_size, $keyword, $i_no_record_exists_msg, $i_no_search_result_msg, $view_type;
                global $image_path,$pageSizeChangeEnabled;
                
                if($pageSizeChangeEnabled == "")
                	$pageSizeChangeEnabled = true;

                $f+=0;
                $o+=0;
                $p+=0;
                $this->field = ($f==0) ? 0 : $f;
                $this->order = ($o==0) ? 0 : $o;
                $this->pageNo = ($p==0) ? 1 : $p;
                $this->custom_css = ($c==true) ? true:false;
                $this->sortby = (trim($list_sortby) == "") ? "Sort By" : $list_sortby;
                $this->checkall = (trim($button_check_all) == "") ? "Check All" : $button_check_all;
                $this->clearall = (trim($button_clear_all) == "") ? "Clear All" : $button_clear_all;
                $this->total = (trim($list_total) == "") ? "Total" : $list_total;
                $this->pagename = (trim($list_page) == "") ? "Page" : $list_page;
                $this->pagePrev = (trim($list_prev) == "") ? "Prev" : $list_prev;
                $this->pageNext = (trim($list_next) == "") ? "Next" : $list_next;
                $this->db = $intranet_db;
                $this->page_size = $page_size;
                $this->view_type = $view_type;
                $this->no_msg = ($keyword=="") ? $i_no_record_exists_msg : $i_no_search_result_msg;
                $this->wrap_array = array();
                $this->imagePath = $image_path;
                $this->form_name = "form1";
                $this->pageNo_name = "pageNo";    
                $this->numPerPage_name = "numPerPage";    
                $this->with_navigation = true; 
                $this->custDataset = false;
                $this->custDatasetArr= array();
                /*
                $this->order_name = "order";
                $this->form_field_name = "field";*/
        }

        # ------------------------------------------------------------------------------------

        /*
        * Modified by Key (2008-10-17): Add a filter to check to use Limit query or not (For ipf2.5 OLE page)
        */ 
        function built_sql($isLimit="Limit"){

                 $x = $this->sql;
                 
                 /*
                $rest = stristr($x, " FROM\n");
                $rest2 = stristr($x, " FROM ");
                $pos = strpos($x,"FROM");
                echo ord(substr($x,670,1));
                echo "|";
                echo ord(substr($x,675,1));
                echo "|";
                echo $pos;
                if ($rest != "") $y = "SELECT COUNT(*) $rest";
                else $y = "SELECT COUNT(*) $rest2";

                echo "<br>1: $rest\n";
                echo "2: $rest2\n";
                echo $y;
                */
                
                 $x = str_replace("\n"," ",$x);
                 $x = str_replace("\r"," ",$x);
                 $x = str_replace("\t"," ",$x);
                 
                 // Notes: if SQL contains " HAVING 'alias'", error msg will show as SQL below didn't define alias

                  $y = "SELECT COUNT(*) " . stristr($x, " FROM ");
                 
                $isSpecialCount = false;
                if($this->IsColOff =='eDisciplineConductMarkView' || $this->IsColOff =='UserMgmtStudentNoClass' || $this->IsColOff =='ePost_article_shelf')
                {
                	$tmp = $this->returnArray($x);
                	$size = sizeof($tmp);
                	$y = "SELECT $size " . stristr($x, " FROM ");
                	
                	$isSpecialCount = true;
            	}
                  if($this->count_mode != "")
                  {
                  	$tmp = $this->returnArray($x);
                  	$size = sizeof($tmp);
                  	$y = "SELECT $size";
                  	
                  	$isSpecialCount = true;
                  }
                 
                 $sql_result = $this->db_db_query($y);
                 if($sql_result == ''){
                     $fromCount = substr_count(strtoupper($x), " FROM ");
                     if($fromCount > 1){
                         $test = strripos(strtoupper($x), " FROM ", 1);
                         $y = substr_replace($x, "SELECT COUNT(*) ", 0, $test);
                     }else{
                         $y = "SELECT COUNT(*) " . stristr($x, " FROM ");
                     }
                     
                     $isSpecialCount = false;
                     if($this->IsColOff =='eDisciplineConductMarkView' || $this->IsColOff =='UserMgmtStudentNoClass' || $this->IsColOff =='ePost_article_shelf')
                     {
                         $tmp = $this->returnArray($x);
                         $size = sizeof($tmp);
                         $y = "SELECT $size " . stristr($x, " FROM ");
                         
                         $isSpecialCount = true;
                     }
                     if($this->count_mode != "")
                     {
                         $tmp = $this->returnArray($x);
                         $size = sizeof($tmp);
                         $y = "SELECT $size";
                         
                         $isSpecialCount = true;
                     }
                 }

                 $this->rs=$this->db_db_query($y);
                 
                 if ($this->db_num_rows()!=0)
                 {
                     $row = $this->db_fetch_array();
                     
                     # Modified by key (2008-07-22) for iportfolio 2.5 shcool based scheme
                     if($this->record_num_mode == "iportfolio") { 
                     	$this->total_row = ($this->db_num_rows() >= 1) ? $this->db_num_rows() : 0;
                     }
                     else {
                     	// 2014-0226-1014-31184 
                     	$this->total_row = ($this->db_num_rows()!=1) ? $this->db_num_rows() : $row[0];
                     	/*if ($isSpecialCount) {
                     		$this->total_row = ($this->db_num_rows()!=1) ? $this->db_num_rows() : $row[0];
                     	}
                     	else {
                     		$this->total_row = $this->db_num_rows();
                     	}
                     	*/
                     }
                 }

				$max_page = ($this->page_size > 0) ? ceil($this->total_row/$this->page_size) : 0;
                if ($this->pageNo > $max_page && $max_page != 0)
                {
                    $this->pageNo = $max_page;
                }
                 $this->n_start=($this->pageNo-1)*$this->page_size;
                 
                # added by Kelvin on 11 Feb 09 for admin console->Admin Mgmt->School Calendar->School Events CRM Ref No.: 2009-0210-1044
                if($this->IsColOff=='adminMgtSchoolCalendarEvents')
				{
					$x .= " ORDER BY ";
					if($this->field==0)
					{
						$x .= $this->field_array[$this->field];
						$x .= ($this->order==0) ? " DESC," : " ASC,";
						$x .= $this->field_array[($this->field+1)];
						$x .=  " ASC";
						$x .= " ".$this->fieldorder2;
					}
					else
					{
	                	$x .= (count($this->field_array)<=$this->field) ? $this->field_array[0] : $this->field_array[$this->field];
	                	$x .= ($this->order==0) ? " DESC" : " ASC";
	                	$x .= " ".$this->fieldorder2;
                	}
				}
				else
				{
	                $x .= " ORDER BY ";
	                $x .= (count($this->field_array)<=$this->field) ? $this->field_array[0] : $this->field_array[$this->field];
	                $x .= ($this->order==0) ? " DESC" : " ASC";
	                $x .= " ".$this->fieldorder2;
	                
            	}
            	
                if($isLimit == "Limit")
               	$x .= " LIMIT ".$this->n_start.", ".$this->page_size;
                return $x;
        }

        
        # ------------------------------------------------------------------------------------

        function returnCellClassID(){
                switch ($this->IsColOff){
                        case 1:  $x = "nothing"; break;
                        case 2:  $x = "tableContent"; break;
                        case 3:  $x = "group_bulletin"; break;
                        case 4:  $x = "body"; break;
                        case 5:  $x = "nothing"; break;
                        case 6:  $x = "nothing"; break;
                        case 7:  $x = "admin_center_announcement_tableContent"; break;
                        case 8:  $x = "nothing"; break;
                        case 9:  $x = "nothing"; break;
                        case 10: $x = "13-black"; break;
                        case 11: $x = "nothing"; break;
                        case "imail": $x = "nothing"; break;
                        case "staff": $x = "tableContent";break;
                        case "imail_gamma": $x = "nothing";break;
                        case "imail_gamma_search": $x = "nothing";break;
                        default: $x = "tableContent"; break;
                }
                return $x;
        }

        function returnColumnClassID(){
                switch ($this->IsColOff){
                        case 1:  $x = "nothing"; break;
                        case 2:  $x = "tableTitle"; break;
                        case 3:  $x = "group_bulletin"; break;
                        case 4:  $x = "nothing"; break;
                        case 5:  $x = "nothing"; break;
                        case 6:  $x = "qb_tableheader"; break;
                        case 7:  $x = "nothing"; break;
                        case 8:  $x = "nothing"; break;
                        case 9:  $x = "nothing"; break;
                        case 10: $x = "13-black-bold"; break;
                        case 11: $x = "nothing"; break;
                        case "imail": $x = "nothing"; break;
                        case "imail_gamma": $x = "nothing"; break;
                        case "imail_gamma_search": $x = "nothing";break;
                        default: $x = "tableTitle"; break;
                }
                return $x;
        }

        function returnColumnTableWidth(){
                switch ($this->IsColOff){
                        case 1:  $x = "100%"; break;
                        case 2:  $x = "100%"; break;
                        case 3:  $x = "93%"; break;
                        case 11: $x = "100%"; break;
                        default: $x = "95%"; break;
                }
                return $x;
        }

        # ------------------------------------------------------------------------------------

        function column($field_index, $field_name){
                global $image_path;
                if($this->IsColOff=="staff") $image_path="/images/staffattend";
                $x = "";
                if($this->field==$field_index){
                        if($this->order==1) $x .= "<a class=".$this->returnColumnClassID()." href=javascript:sortPage(0,$field_index,document.".$this->form_name.") onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                        if($this->order==0) $x .= "<a class=".$this->returnColumnClassID()." href=javascript:sortPage(1,$field_index,document.".$this->form_name.") onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }else{
                        $x .= "<a class=".$this->returnColumnClassID()." href=javascript:sortPage($this->order,$field_index,document.".$this->form_name.") onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }
                $x .= str_replace("_", " ", $field_name);
                if($this->field==$field_index)
                {
					//if($this->order==1) $x .= "<img src='$image_path/desc.gif' hspace=2 border=0>";
					//if($this->order==0) $x .= "<img src='$image_path/asc.gif' hspace=2  border=0>";
	                
					//Change arrow display    	                                        
					if($this->order==1) $x .= "<img src='$image_path/asc.gif' hspace='2' border='0' />";
					if($this->order==0) $x .= "<img src='$image_path/desc.gif' hspace='2'  border='0' />";
                }
                $x .= "</a>";
                return $x;
        }
		

		
        function column_IP25($field_index, $field_name){
                global $image_path;
                if($this->IsColOff=="staff") $image_path="/images/staffattend";
                $x = "";
                if($this->field==$field_index){
                        if($this->order==1) $x .= "<a class=sort_asc href=javascript:sortPage(0,$field_index,document.".$this->form_name.") onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                        if($this->order==0) $x .= "<a class=sort_dec href=javascript:sortPage(1,$field_index,document.".$this->form_name.") onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }else{
                        $x .= "<a class=".$this->returnColumnClassID()." href=javascript:sortPage($this->order,$field_index,document.".$this->form_name.") onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }
                $x .= str_replace("_", " ", $field_name);
                if($this->field==$field_index)
                {
					//if($this->order==1) $x .= "<img src='$image_path/desc.gif' hspace=2 border=0>";
					//if($this->order==0) $x .= "<img src='$image_path/asc.gif' hspace=2  border=0>";
	                
					//Change arrow display    	                                        
					//if($this->order==1) $x .= "<img src='$image_path/asc.gif' hspace='2' border='0' />";
					//if($this->order==0) $x .= "<img src='$image_path/desc.gif' hspace='2'  border='0' />";
                }
                $x .= "</a>";
                return $x;
        }
		

        function column_image($field_index, $field_name, $field_image)
        {
                global $image_path;
                $x = "";
                if($this->field==$field_index){
                        if($this->order==1) $x .= "<a class=".$this->returnColumnClassID()." href=javascript:sortPage(0,$field_index,document.".$this->form_name.") onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                        if($this->order==0) $x .= "<a class=".$this->returnColumnClassID()." href=javascript:sortPage(1,$field_index,document.".$this->form_name.") onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }else{
                        $x .= "<a class=".$this->returnColumnClassID()." href=javascript:sortPage($this->order,$field_index,document.".$this->form_name.") onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }
                $x .= $field_image;
                if($this->field==$field_index)
                {
					//if($this->order==1) $x .= "<img src='$image_path/desc.gif' hspace=2 border=0>";
                    //if($this->order==0) $x .= "<img src='$image_path/asc.gif' hspace=2  border=0>";
	                
	                //Change arrow display
					if($this->order==1) $x .= "<img src='$image_path/asc.gif' hspace='2' border='0' />";
					if($this->order==0) $x .= "<img src='$image_path/desc.gif' hspace='2'  border='0' />";
                }
                $x .= "</a>";
                return $x;
        }

        # ------------------------------------------------------------------------------------

        function navigation($bgcolor=""){
                global $image_path;
                $bgcolor_att_string = ($bgcolor!="")? "bgcolor=#$bgcolor":"";
                $x  = "<table width=100% border=0 cellpadding=0 cellspacing=0 $bgcolor_att_string>\n";
                $x .= "<tr >\n";
                $x .= "<td align=right style='vertical-align:middle'>\n";
                $x .= $this->record_range();
                
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->prev_n();
                $x .= "<img src=$image_path/space.gif width=5 height=10 border=0>\n";
                $x .= $this->next_n();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->go_page();
                global $pageSizeChangeEnabled, $viewTypeChangeEnabled;
                if ($pageSizeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                    $x .= $this->page_size_input();
                }
                if ($viewTypeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                    $x .= $this->view_type_input();
                }                               
                $x .= "&nbsp;</td>\n";
                $x .= "</tr>\n";
                $x .= "</table>\n";
                $x .= "<SCRIPT LANGUAGE=JAVASCRIPT>
function ".$this->form_name."_gopage(page, obj){
        obj.".$this->pageNo_name.".value=page;        
        obj.submit();
}
                </SCRIPT>\n";
                return $x;
        }
        
       function navigation_staff($bgcolor=""){
                global $image_path;
                if($intranet_session_language=="b5") $lang="chi";
								else $lang="eng";
								if($this->IsColOff=="staff"){
										$image_path="/images/staffattend";
								}
                $bgcolor_att_string = ($bgcolor!="")? "bgcolor=#$bgcolor":"";
                $x  = "<table width=100% border=0 cellpadding=0 cellspacing=0 $bgcolor_att_string>\n";
                $x .= "<tr >\n";
                $x .= "<td align=right style='vertical-align:middle'><span class=".$lang."_staff_menu_heading>\n";
                $x .= $this->record_range();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->prev_n();
                $x .= "<img src=$image_path/space.gif width=5 height=10 border=0>\n";
                $x .= $this->next_n();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->go_page();
                global $pageSizeChangeEnabled, $viewTypeChangeEnabled;
                if ($pageSizeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                    $x .= $this->page_size_input();
                }
                if ($viewTypeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                    $x .= $this->view_type_input();
                }                               
                $x .= "&nbsp;</span></td>\n";
                $x .= "</tr>\n";
                $x .= "</table>\n";
                $x .= "<SCRIPT LANGUAGE=JAVASCRIPT>
function ".$this->form_name."_gopage(page, obj){
        obj.".$this->pageNo_name.".value=page;        
        obj.submit();
}
                </SCRIPT>\n";
                return $x;
        }

        
                # for alumni

        function navigation2(){
                global $image_path;

                                $x = "<table width='90%' border='0' cellspacing='0' cellpadding='0'>";
                                $x .= "<tr>";
                $x .= "<td width='10'><img src='$image_path/page_l.gif' width='10' height='34'></td>";
                $x .= "<td align='center' class='alumni_next_page_cellbg' valign='middle'>";
                $x .= $this->record_range();
                $x .= $this->prev_n2();
                $x .= $this->go_page();
                $x .= $this->next_n2();

                global $pageSizeChangeEnabled;
                if ($pageSizeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                    $x .= $this->page_size_input();
                }

                                $x .= "</td>";
                $x .= "<td width='10'><img src='$image_path/page_r.gif' width='10' height='34'></td></tr>";
                                $x .= "</table>";

                return $x;
        }

        function navigation_IP25($bgcolor="", $css2=""){
                global $image_path, $pageSizeChangeEnabled, $viewTypeChangeEnabled;
				$x = "
					<div class='common_table_bottom $css2'>
					<div class='record_no'>".$this->record_range_ip25()."</div>
					<div class='page_no'>";
                $x .= "<span>".$this->prev_n_ip25()."</span>";
                $x .= "<span>".$this->go_page_ip25()."</span>";
                $x .= "<span>".$this->next_n_ip25()."</span><span>";

				if($pageSizeChangeEnabled || $viewTypeChangeEnabled)
					$x .= "|";
                if ($pageSizeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=2 height=10 border=0>\n";
                    $x .= $this->page_size_input_ip25();
                }
                if ($viewTypeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=2 height=10 border=0>\n";
                    $x .= $this->view_type_input();
                }                               
				$x .= "	</span>
					</div>";			
                $x .= "<SCRIPT LANGUAGE=JAVASCRIPT>
function ".$this->form_name."_gopage(page, obj){
        obj.".$this->pageNo_name.".value=page;        
        obj.submit();
}
                </SCRIPT></div>\n";
                return $x;
        }
		
        function page_size_input ()
        {
                 global $i_general_EachDisplay ,$i_general_PerPage;

                 $x = "$i_general_EachDisplay <select name='num_per_page' onChange='this.form.".$this->pageNo_name.".value=1;this.form.page_size_change.value=1;this.form.".$this->numPerPage_name.".value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
                 $x .= "<option value=10 ".($this->page_size==10? "SELECTED":"").">10</option>\n";
                 $x .= "<option value=20 ".($this->page_size==20? "SELECTED":"").">20</option>\n";
                 $x .= "<option value=30 ".($this->page_size==30? "SELECTED":"").">30</option>\n";
                 $x .= "<option value=40 ".($this->page_size==40? "SELECTED":"").">40</option>\n";
                 $x .= "<option value=50 ".($this->page_size==50? "SELECTED":"").">50</option>\n";
                 $x .= "<option value=60 ".($this->page_size==60? "SELECTED":"").">60</option>\n";
                 $x .= "<option value=70 ".($this->page_size==70? "SELECTED":"").">70</option>\n";
                 $x .= "<option value=80 ".($this->page_size==80? "SELECTED":"").">80</option>\n";
                 $x .= "<option value=90 ".($this->page_size==90? "SELECTED":"").">90</option>\n";
                 $x .= "<option value=100 ".($this->page_size==100? "SELECTED":"").">100</option>\n";
                 $x .= "</select>$i_general_PerPage\n";
                 return $x;
        }
         
		# for IP25
        function page_size_input_ip25 ()
        {
                 global $i_general_EachDisplay, $i_general_PerPage;

                 $x = "$i_general_EachDisplay <select name='num_per_page' onChange='this.form.".$this->pageNo_name.".value=1;this.form.page_size_change.value=1;this.form.".$this->numPerPage_name.".value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
//					for($i=1;$i<=10;$i++)
//					{
//						$pageNoSelect = $this->PageSizeInterval*$i;
//						$x .= "<option value=$pageNoSelect ".($this->page_size==$pageNoSelect? "SELECTED":"").">$pageNoSelect</option>\n";
//					}
                 $x .= "<option value=10 ".($this->page_size==10? "SELECTED":"").">10</option>\n";
                 $x .= "<option value=20 ".($this->page_size==20? "SELECTED":"").">20</option>\n";
                 $x .= "<option value=30 ".($this->page_size==30? "SELECTED":"").">30</option>\n";
                 $x .= "<option value=40 ".($this->page_size==40? "SELECTED":"").">40</option>\n";
                 $x .= "<option value=50 ".($this->page_size==50? "SELECTED":"").">50</option>\n";
                 $x .= "<option value=60 ".($this->page_size==60? "SELECTED":"").">60</option>\n";
                 $x .= "<option value=70 ".($this->page_size==70? "SELECTED":"").">70</option>\n";
                 $x .= "<option value=80 ".($this->page_size==80? "SELECTED":"").">80</option>\n";
                 $x .= "<option value=90 ".($this->page_size==90? "SELECTED":"").">90</option>\n";
                 $x .= "<option value=100 ".($this->page_size==100? "SELECTED":"").">100</option>\n";
                 $x .= "</select> $i_general_PerPage\n";
                 return $x;
        }
		 
        function view_type_input ()
        {        

                 $x = "<select name='view_type' onChange='this.form.pageNo.value=1;this.form.view_type_change.value=1;this.form.viewType.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
                 $x .= "<option value=1 ".($this->view_type==1? "SELECTED":"").">1 Column</option>\n";
                 $x .= "<option value=2 ".($this->view_type==2? "SELECTED":"").">2 Columns</option>\n";
                 $x .= "<option value=3 ".($this->view_type==3? "SELECTED":"").">4 Columns</option>\n";         
                 $x .= "</select>";
                 return $x;
        }
                
        function record_range(){
	        	global $Lang;
	        	
                $n_title = ($this->title == '')? $Lang['General']['Record'] : $this->title;
                #$n_start = ($this->pageNo-1)*$this->page_size+1;
                $n_start = $this->n_start+1;
                $n_end = min($this->total_row,($this->pageNo*$this->page_size));
                $n_total = $this->total_row;
                $x = "$n_title $n_start - $n_end, ".$this->total." $n_total\n";
                return $x;
        }

        function record_range_ip25(){
	        	global $Lang;
	        	$n_title = ($this->title == '')? $Lang['General']['Record'] : $this->title;
                $n_start = $this->n_start+1;
                $n_end = min($this->total_row,($this->pageNo*$this->page_size));
                $n_total = $this->total_row;
                
                $x = "$n_title $n_start - $n_end, ".$Lang['StudentRegistry']['TotalRecord']." $n_total\n";
                return $x;
        }
		
        function prev_n(){
                global $image_path,$image_pre;
				
				 if($this->IsColOff=="staff"){
							$image_path="/images/staffattend";
							$image_pre = "$image_path/previous_icon.gif";
					}
				 if ($this->IsColOff == 2 || $this->IsColOff == 5)
					$previous_icon = "<img src=$image_path/previous_icon.gif border=0 hspace=2 vspace=0 align=middle>";
				else
					$previous_icon = "<img src=$image_pre border=0 hspace=2 vspace=0 align=middle>";
					
                $n_page = $this->pageNo;
                $n_total = ceil($this->total_row/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==1) ? "<span class=iconLink>".$previous_icon.$this->pagePrev."</span>\n" : "<a class=iconLink href=javascript:".$this->form_name."_gopage(".($n_page-1).",document.".$this->form_name.") onMouseOver=\"window.status='".$this->pagePrev."';return true;\" onMouseOut=\"window.status='';return true;\">".$previous_icon.$this->pagePrev."</a>\n";
                return $x;
        }
        

                # for alumni
        function prev_n2(){
                global $image_path,$image_pre;
                 if($this->IsColOff=="staff") $image_path="/images/staffattend";
                if ($this->IsColOff == 2 || $this->IsColOff == 5)
                    $previous_icon = "<img border=0 hspace=2 vspace=0 align=middle src='$image_path/arrow_l.gif' width='13' height='13' align='absmiddle'>";
                else
                    $previous_icon = "<img border=0 hspace=2 vspace=0 align=middle src='$image_path/arrow_l.gif' width='13' height='13' align='absmiddle'>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->total_row/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==1) ? "<span >".$previous_icon.$this->pagePrev."</span>\n" : "<a  href=javascript:"."gopage(".($n_page-1).",document.".$this->form_name.") onMouseOver=\"window.status='".$this->pagePrev."';return true;\" onMouseOut=\"window.status='';return true;\">".$previous_icon.$this->pagePrev."</a>\n";
                return $x;
        }
		
		# for IP25_table 
        function prev_n_ip25(){
			global $image_path, $image_pre;
								
			$n_page = $this->pageNo;
			$n_total = ceil($this->total_row/$this->page_size);
			$n_size = $this->page_size;
			$x = ($n_page==1) ? "<a class='prev'>&nbsp;</a>\n" : "<a href='javascript:".$this->form_name."_gopage(".($n_page-1).",document.".$this->form_name.")' class='prev'>".$previous_icon."</a>\n";
			return $x;
        }
		
		# for IP25_table 
        function next_n_ip25(){
			global $image_path,$image_next;
			
			$n_page = $this->pageNo;
			$n_total = ceil($this->total_row/$this->page_size);
			$n_size = $this->page_size;
			$x = ($n_page==$n_total) ? "<a class='next'>&nbsp;</a>\n" : "<a class='next' href=javascript:".$this->form_name."_gopage(".($n_page+1).",document.".$this->form_name.")>".$next_icon."</a>\n";
			return $x;
        }
		
		
                # for alumni
        function next_n2(){
                global $image_path,$image_next;
                if ($this->IsColOff == 2 || $this->IsColOff == 5)
                    $next_icon = "<img border=0 hspace=2 vspace=0 align=middle src='$image_path/arrow_r.gif' width='13' height='13' align='absmiddle'>";
                else
                    $next_icon = "<img border=0 hspace=2 vspace=0 align=middle src='$image_path/arrow_r.gif' width='13' height='13' align='absmiddle'>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->total_row/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==$n_total) ? "<span >".$this->pageNext.$next_icon."</span>\n" : "<a href=javascript:"."gopage(".($n_page+1).",document.".$this->form_name.") onMouseOver=\"window.status='".$this->pageNext."';return true;\" onMouseOut=\"window.status='';return true;\">".$this->pageNext.$next_icon."</a>\n";
                return $x;
        }
        
        function next_n(){
                global $image_path,$image_next;
                 if($this->IsColOff=="staff"){
                 			$image_path="/images/staffattend";
                 			$image_next="$image_path/next_icon.gif";
                 }

                if ($this->IsColOff == 2 || $this->IsColOff == 5)
                    $next_icon = "<img src=$image_path/next_icon.gif border=0 hspace=2 vspace=0 align=middle>";
                else
                    $next_icon = "<img src=$image_next border=0 hspace=2 vspace=0 align=middle>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->total_row/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==$n_total) ? "<span class=iconLink>".$this->pageNext.$next_icon."</span>\n" : "<a class=iconLink href=javascript:".$this->form_name."_gopage(".($n_page+1).",document.".$this->form_name.") onMouseOver=\"window.status='".$this->pageNext."';return true;\" onMouseOut=\"window.status='';return true;\">".$this->pageNext.$next_icon."</a>\n";
                return $x;
        }

        function go_page(){
                $n_page=$this->pageNo;
                $n_total=ceil($this->total_row/$this->page_size);
                $x = $this->pagename." <select onChange='this.form.".$this->pageNo_name.".value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
                for($i=1;$i<=$n_total;$i++)
                $x .= ($n_page==$i) ? "<option value=$i selected>$i</option>\n" : "<option value=$i>$i</option>\n";
                $x .= "</select>\n";
                return $x;
        }

		# for IP25
        function go_page_ip25(){
			global $Lang;
			
			$n_page=$this->pageNo;
			$n_total=ceil($this->total_row/$this->page_size);
			$x = $Lang['StudentRegistry']['PageName1'];
			$x .= " <select onChange='this.form.".$this->pageNo_name.".value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
			for($i=1;$i<=$n_total;$i++)
			$x .= ($n_page==$i) ? "<option value=$i selected>$i</option>\n" : "<option value=$i>$i</option>\n";
			$x .= "</select>\n";
			$x .= $this->pagename;
			return $x;
        }
		
        function check($id){
	        
	        	global $checkmaster;
	        	
                $x = "<input type=checkbox name='checkmaster' onClick=(this.checked)?setChecked(1,this.form,'$id'):setChecked(0,this.form,'$id')>";

                if($checkmaster==true)
                {
	             		$x="<input type=checkbox name='checkmaster' onClick=(this.checked)?setChecked(1,document.form1,'$id'):setChecked(0,document.form1,'$id')>";   
                }
                return $x;
        }

        # ------------------------------------------------------------------------------------

        function functionbar($n, $m, $color, $a){
                $x = "";
                if(strstr($m , $n)){
                $x .= "<table width=95% border=0 cellpadding=1 cellspacing=0>\n";
                $x .= "<tr><td class=tableTitle align=right>$a</td></tr>\n";
                $x .= "</table>\n";
                }
                return $x;
        }

        function displayFunctionbar($n, $m, $a, $b){
                $x = "";
                if(strstr($m , $n)){
                        $x .= "<table width=".$this->returnColumnTableWidth()." border=0 cellpadding=1 cellspacing=0 align=center>\n";
                        $x .= "<tr>\n";
                        $x .= "<td>$a</td>\n";
                        $x .= "<td align=right>$b</td>\n";
                        $x .= "</tr>\n";
                        $x .= "</table>\n";
                }
                return $x;
        }
		
		// @Description: This function find out all onXXXXX event handlers in html and replace with a random id for replace back at later time
		// @Return: array of the replaced html text and array of replaced pairs(onXXXXXX expresson, random id)		
		function replaceAllOnEventHandlers($html)
		{
			$max_iteration = 10000000;
			
			$start_pos = 0;
			$end_pos = 0;
			$open_bracket = '<';
			$close_bracket = '>';
			$onevent_pattern = '/\s*on\w+\s*=\s*("[^"]+"|\'[^\']+\')\s*/';
			$whole_str_length = strlen($html);
		
			$all_matches = array();
			
			$iteration_count = 0;
			do{
				$iteration_count++;
				$start_pos = stripos($html,$open_bracket,$start_pos); // find next < position
				if($start_pos !== false){ // if find next <, find > position
					//echo '$start_pos='.$start_pos.'<br />';
					$end_pos = stripos($html,$close_bracket,$start_pos);
					if($end_pos !== false){ // found >
						//echo '$end_pos='.$end_pos.'<br />';
						$str_length = $end_pos + 1 - $start_pos;
						$substr = substr($html,$start_pos,$str_length); // extract string in between < and >
						//echo '$substr='.htmlspecialchars($substr).'<br />';
						//$cleaned_str = preg_replace($onevent_pattern,' ',$substr); // replace all onxxxx="" attributes
						$matches = array();
						$has_match = false;
						$cleaned_str = $substr;
						if(preg_match_all($onevent_pattern,$substr,$matches)!==false){
							$has_match = true;
							if(count($matches)>0){
								//debug_pr($matches);
								for($i=0;$i<count($matches[0]);$i++){
									$random_id = uniqid();
									//echo htmlspecialchars($matches[$i])."<br />";
									$all_matches[] = array($matches[0][$i],$random_id);
									$cleaned_str = str_replace($matches[0][$i],$random_id,$cleaned_str);
								}
							}
						}
						$cleaned_str_length = strlen($cleaned_str);
						if($has_match){ // if has replaced onxxx attributes
							//echo '$cleaned_str='.htmlspecialchars($cleaned_str).'<br />';
							$head_str = substr($html,0,$start_pos);
							$tail_str = substr($html,$end_pos+1);
							$html = $head_str.$cleaned_str.$tail_str; // construct the new html text with the attributes cleaned string
						}
						$start_pos = $start_pos + $cleaned_str_length;
					}else{ // no > found, can end the finding
						$start_pos = false;
					}
				}
				if($iteration_count > $max_iteration){ // avoid dead loop
					break;
				}
			}while($start_pos !== false);
			
			return array('html'=>$html,'replaces'=>$all_matches);
		}		

        function displayCell($i,$data, $css="", $other=""){
                global $default_wrap_length;
                switch ($this->column_array[$i]){
                case 0:
                        //$data = str_replace(array('onclick=','onClick=','OnClick='),'##!dgsjlpait@#$%',$data);
                        $replaced_data = $this->replaceAllOnEventHandlers($data);// replace all onXXXX expression to random id strings
                        $safe_data = cleanHtmlJavascript($replaced_data['html']); // clean <script> tags and onXXXX events in data value 
                        //$data = str_replace('##!dgsjlpait@#$%','onclick=',$data);
                        // replace back the real onXXXXXX events
                        for($i=0;$i<count($replaced_data['replaces']);$i++){
                        	$safe_data = str_replace($replaced_data['replaces'][$i][1],$replaced_data['replaces'][$i][0],$safe_data);
                        }
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>$safe_data&nbsp;</td>\n";
                        break;
                case 1:
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".$this->convertAllLinks(nl2br($data))."&nbsp;<br></td>\n";
                        break;
                case 2:
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".stripslashes($data)."&nbsp;<br></td>\n";
                        break;
                case 3:
                        $lr = new librb();
                        $row = explode(":",$data);
                        $slots_str = $lr->returnSlotsString($row[1],$row[0]);
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".$slots_str."&nbsp;<br></td>\n";
                        break;

                case 4:
                        $row = explode(":",$data);
                        list($type,$value,$start,$end) = $row;
                        if ($type!=3)
                        {
                            $lr = new librb();
                            $type_str = $lr->returnTypeString($type,$value);
                        }
                        else
                        {
                            $lc = new libcycleperiods();
                         	$type_str = $lc->returnCriteriaString($value,$start,$end);
                        }
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".$type_str."&nbsp;<br></td>\n";
                        break;
                case 5:
                        # Wrap long words
                        
                        ### Comment (By Ronald - 20090914)
                        ### as now is using UTF8, so the old method for detecting string length cannot use as it may have error
                        ### when checking the length which contain UTF8 chinese word
                        
                        //$wrap_length =  ($this->wrap_array[$i] == "")? $default_wrap_length : $this->wrap_array[$i];
                        $wrap_length = mb_strlen($data,"UTF-8");
                        
                        $s_data = intranet_wordwrap($data,$wrap_length,"\n",1);
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".$s_data."&nbsp;</td>";
                        break;
                case 6:
                        # Wrap long words inside hyperlink anchor tag
                        $wrap_length =  ($this->wrap_array[$i] == "")? $default_wrap_length : $this->wrap_array[$i];
                        $newlink = wraplink($data,$wrap_length);
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".$newlink."&nbsp;</td>";
                        break;
                case 7:
                        # Process link then wrap
                        $wrap_length =  ($this->wrap_array[$i] == "")? $default_wrap_length : $this->wrap_array[$i];
                        $s_data = intranet_wordwrap($this->convertAllLinks(nl2br($data)),$wrap_length,"\n",1);
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".$s_data."&nbsp;</td>";
                        break;
                case 8:

                        # Count replies of campusmail outbox
                        $li = new libdb();
                        $id = $data;
                        $sql = "SELECT COUNT(CampusMailReplyID) FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID = '$id'";
                        $result = $li->returnVector($sql);
                        $total = $result[0];
                        $sql = "SELECT COUNT(CampusMailReplyID) FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID = '$id' AND IsRead = '1'";
                        $result = $li->returnVector($sql);
                        $read = $result[0];
                        $s_data = "$read/$total";
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".$s_data."&nbsp;</td>";
                        break;
                case 9:
                        # Seconds to time (Usage record)
                        if ($data > 3600)
                        {
                            $hr = intval($data/3600);
                            $data = $data % 3600;
                        }
                        if ($data > 60)
                        {
                            $min = intval($data/60);
                            $data = $data % 60;
                        }
                        $s_data = "";
                        global $i_Usage_Hour, $i_Usage_Min, $i_Usage_Sec;
                        if ($hr != "") $s_data .= "$hr $i_Usage_Hour";
                        if ($min != "") $s_data .= "$min $i_Usage_Min";
                        $s_data .= "$data $i_Usage_Sec";

                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".$s_data."&nbsp;</td>";

                        break;
				case 10:
						# alumni
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".$data."&nbsp;</td>\n";
                        break;
                case 11:
                		# no wrap
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other nowrap='nowrap' >".$data."&nbsp;<br></td>\n";
                        break;
                case 12:
                		# For UTF-8 text (eLib)
                        //$x = "<td class=\"".$this->returnCellClassID()."$css\" $other nowrap='nowrap' >".iconv("UTF-8", "big5"."//IGNORE", $data)."&nbsp;<br></td>\n";
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other nowrap='nowrap' >". $data."&nbsp;<br></td>\n";
                        break;
                case 13:
                        $x = "<td class=\"".$this->returnCellClassID()." $css\" $other align=\"right\">".$data."&nbsp;</td>\n";
                        break;
                case 14:
                        $x = "<td $other>".$data."</td>\n";
                        break;
                case 15:
						$li = new libdb();
						$ldiscipline = new libdisciplinev12();
                		$x = "<td $other><table class='tbclip' cellspacing='0' cellpadding='0'><tr><td style='border-bottom: none'>";
		                $name_field = getNameFieldByLang();
                        if($data != '') {
                        	$sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ($data)";
                        	$temp = $li->returnArray($sql,1);
                        	if(sizeof($temp)!=0) {
                            	for($k=0;$k<sizeof($temp);$k++) {
	                            	$x .= $temp[$k][0];
	                            	$x .= ($k!=(sizeof($temp)-1)) ? ", " : "";
                            	}
                    		} else {
                        		$x .= "---";
                    		}
                		} else {
                    		$x .= "---";	
                		}
                        $x .= "</td></tr></table></td>";

                		break;
                case 16:
                		## added by Ronald on 20090312 - for iMail use (show subject in Inbox)
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>$data</td>\n";
                        break;
                case 17:
						# added by marcus 20090803 - for School Setting -> Group (align number of member to center)
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other align=\"center\">$data</td>\n";
                        break;	
                case 18:
                		# nl2br 
                		global $Lang;
                		
                		$x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".(trim($data)?nl2br($data):$Lang['General']['EmptySymbol'])."</td>\n";
//                 		$x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".(Get_String_Display($data))."</td>\n";
                        break;
                case 21:
                		// number format turn 10000 to 10,000.00 
	                	$x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".number_format($data,2)."&nbsp;</td>\n";
	                	break;
                case 22:
                		// Get String Display
                		$x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".Get_String_Display($data)."&nbsp;</td>\n";
                		break;
                case 23:
                		// handle special characters like <a>&b to &lt;a&gt;&amp;b
                        $x = "<td class=\"".$this->returnCellClassID()."$css\" $other>".intranet_htmlspecialchars($data)."&nbsp;</td>\n";
                        break;
                		
				}
                return $x;
        }

        function displayColumn(){
                global $image_path;
				$x = '';
                $x .= "<tr>\n";
//                $x .= ($this->IsColOff==1) ? "<td ><img src=$image_path/space.gif width=14 height=26 border=0></td>\n" : "";
                $x .= ($this->IsColOff==1 || $this->IsColOff==4 || $this->IsColOff==8 || $this->IsColOff=="imail") ? "<td width=15>\n" :"";
                $x .= $this->column_list;                
                $x .= ($this->IsColOff == 12) ? $this->column_list : "" ; 
//                $x .= ($this->IsColOff==1) ? "<td><img src=$image_path/space.gif width=21 height=26 border=0></td>\n" : "";
                $x .= ($this->IsColOff==1 || $this->IsColOff==4 || $this->IsColOff==8 || $this->IsColOff=="imail") ? "<td width=15>\n":"";
                $x .= "</tr>\n";
                return $x;
        }

                # for alumni
        function displayColumn2(){
                global $image_path;
                $x .= "<tr align=left bgcolor='#E3DB9C'>\n";

                $x .= ($this->IsColOff==1 || $this->IsColOff==4 || $this->IsColOff==8) ? "<td td align='left' valign='top' class=13-black-bold>\n" :"";
                $x .= $this->column_list;

                $x .= ($this->IsColOff==1 || $this->IsColOff==4 || $this->IsColOff==8) ? "<td td align='left' valign='top' class=13-black-bold>\n":"";
                $x .= "</tr>\n";
                return $x;
        }
			

        # ------------------------------------------------------------------------------------

        function display($fb="",$fb2="",$fb3="",$fb4="",$fb5=""){
	        	switch ($this->IsColOff){
                        case 1:  $x = $this->displayFormat1($fb,$fb2); break;
                        case 2:  $x = $this->displayFormat2($fb, $fb2); break;
                        case 3:  $x = $this->displayFormat3(); break;
                        case 4:  $x = $this->displayFormat4(); break;
                        case 5:  $x = $this->displayFormat5(); break;
                        case 6:  $x = $this->displayFormat6(); break;
                        case 7:  $x = $this->displayFormat7(); break;
                        case 8:  $x = $this->displayFormat8($fb,$fb2); break;
                        case 9:  $x = $this->displayFormat9($fb); break;
                        case 10: $x = $this->displayFormat10(); break;
                        case 11: $x = $this->displayFormat11($fb,$fb2,$fb3,$fb4,$fb5); break;
                        case 12: $x = $this->displayFormat12($fb); break;
                        case "staff": $x = $this->displayFormat_Staff($fb);break;
                        case "imail": $x = $this->displayFormat_iMail($fb,$fb2); break;
                        case "imail_gamma": 
							$x = $this->displayFormat_iMail_Gamma($fb,$fb2); break;
                        case "imail_gamma_search": 
							$x = $this->displayFormat_iMail_Gamma_Search($fb,$fb2); break;
                        case "imail_addressbook": 
                        case "imail_addressbook_list":
                        case "imail_addressbook_externalRecipient":
                        	$x = $this->displayFormat_iMailAddressBook(); break;
                        	
						case "imail_addressbook_list_externalRecipient":
                        	$x = $this->displayFormat_iMailAddressBook(0); break;
						case "SmartCardAttandencdList":     
                        	$x = $this->displayFormat_SmartCardAttandencdList($fb); break;
						case "SmartCardAttandencdListMonthly":
                        	$x = $this->displayFormat_SmartCardAttandencdListMonthly($fb);break;
						case "SmartCardPaymentBalance":
                        case "SmartCardPaymentRecord":
                        case "SmartCardPaymentAddValue":
                        	$x = $this->displayFormat_SmartCardPaymentBalance("blue"); break;
						case "SmartCardReminder":
                        	$x = $this->displayFormat_SmartCardPaymentBalance(""); break;
						case "SmartCardTakeAttendance":
                        	$x = $this->displayFormat_SmartCardTakeAttendance($fb); break;
						case "SmartCardTakeGroupAttendance":
                        	$x = $this->displayFormat_SmartCardTakeGroupAttendance($fb); break;
						case "eSportHouseSettings":
                                $x = $this->displayFormat_eSportHouseSettings();break;
						case "eSportSettings":
                        	$x = $this->displayFormat_eSportSettings($fb, $fb2);break;
                        case "eSportLaneSetSettings":
                        	$x = $this->displayFormat_eSportLaneSetSettings();break;
						case "eSportParticipant":
                        	$x = $this->displayFormat_eSportParticipant();break;
						case "eSportEnrolUpdateList":
                        	$x = $this->displayFormat_eSportEnrolUpdateList();break;
						case "eSportExportArrangement":
                        	$x = $this->displayFormat_eSportExportArrangement();break;
						case "eSportExportResult":
                        	$x = $this->displayFormat_eSportExportResult();break;
						case "eSportAdmin":
                        	$x = $this->displayFormat_eSportAdmin($fb, $fb2);break;
						case "IP25_table":
							$x = $this->displayFormat_IP25_table($fb, $fb2, $fb3);break;						
						case "StudentRegistry_StudentList":
							$x = $this->displayFormat_StudentRegistry_StudentList($fb, $fb2);break;
						case "StudentRegistry_StudentList_Malaysia":
							$x = $this->displayFormat_StudentRegistry_StudentList_Malaysia($fb, $fb2);break;
						case "StudentRegistry_StudentList_HK":
							$x = $this->displayFormat_StudentRegistry_StudentList_HK($fb, $fb2);break;
						case "eLibPlus_Circulation_Detail_Table":
							$x = $this->displayFormat_eLibPlus_Circulation_Detail_Table($fb, $fb2, $fb3);break;		
						case "eLibPlus_Search_User_Table":
							$x = $this->displayFormat_eLibPlus_Search_User_Table($fb, $fb2, $fb3);break;						
                        	
            ######## eOffice / eNotice
            case "eNoticedisplayMyNotice":
            	$x = $this->displayFormat_eNoticedisplayMyNotice();break;
            
            case "eNoticedisplayNoticePrint":		# 20090227 yatwoon
            	$x = $this->displayFormat_eNoticedisplayNoticePrint($fb);break;

			case "displayTeacherView":
                        	$x = $this->displayFormat_displayTeacherView($fb);break;
			case "displayParentView":
                        	$x = $this->displayFormat_displayParentView($fb);break;
			case "displayAllNotice":
                        	$x = $this->displayFormat_displayAllNotice();break;
			case "displayParentHistory":
                        	$x = $this->displayFormat_displayParentHistory($fb);break;
			case "displayStudentView":
                        	$x = $this->displayFormat_displayStudentView($fb);break;
			case "displayStudentHistory":
                        	$x = $this->displayFormat_displayStudentHistory($fb);break;

			###### eCircular
            case "displayeCircularView":
                        	$x = $this->displayFormat_displayeCircularView($fb);break;
                        	
			###### eFilling
            case "displayeFillingPoll":
            	$x = $this->displayFormat_SmartCardPaymentBalance($fb);break;
            	
			### student attendance
			case "DetailedRecordByYearWithReasonEditable":
				$x = $this->displayFormat_DetailedRecordByYearWithReasonEditable($fb, $fb2);break;
			case "DetailedRecordByYear":
				$x = $this->displayFormat_DetailedRecordByYear($fb);break;							
 
			case "eCommShareList_List":
				$x = $this->displayFormat_eCommShareList_List($fb);break;							
			case "eCommShareList_4":
				$x = $this->displayFormat_eCommShareList_4($fb);break;							
			case "eCommShareList_1":
				$x = $this->displayFormat_eCommShareList_1();break;							
			
			case "eCommForumList":
				$x = $this->displayFormat_eCommForumList($fb, $fb2);break;
            case "eCommDisplayMessage":
			    $x = $this->displayFormat_eCommDisplayMessage($fb);break;
			case "eCommDisplayMessageSetting":
			    $x = $this->displayFormat_eCommDisplayMessageSetting($fb);break;            	
			case "eCommForumSearchList":
				$x = $this->displayFormat_eCommForumSearchList($fb);break;
			case "eCommAdminMemList":
				$x = $this->displayFormat_eCommAdminMemList();break;
			case "eDisCaseReportList":
				$x = $this->displayFormat_eDisCaseReportList();break;
			### inventory
			case "eInventoryList":
				$x = $this->displayFormat_eInventoryList($fb);break;	
			case "eInventoryFullList":
				$x = $this->displayFormat_eInventoryFullList($fb);break;	
			
			### eRC > Curriculum Expectation
			case "CurriculumExpectationList":
				$x = $this->displayFormat_CurriculumExpectationList();break;	
			### eRC > Curriculum Expectation > Personal Characteristics Pool
			case "PersonalCharacteristicsList":
				$x = $this->displayFormat_PersonalCharacteristicsList();break;	
		
			### ipf 2.5 > School based scheme
			case "iPortfolioSchoolBasedScheme":
				$x = $this->displayFormat_iPortfolioSchoolBasedScheme($fb, $fb2);break;	
			case "iPortfolioPhaseHandinList":
				$x = $this->displayFormat_iPortfolioPhaseHandinList($fb);break;	
			case "iPortfolioStudentOLE1":
				$x = $this->displayFormat_iPortfolioStudentOLE1();break;	
			case "iPortfolioTeacherOLE1":
				$x = $this->displayFormat_iPortfolioTeacherOLE1($fb);break;	
			case "iPortfolioTeacherOLEStudentView":
				$x = $this->displayFormat_iPortfolioTeacherOLEStudentView();break;
			case "iPortfolioTeacherOLE_EXT":
				$x = $this->displayFormat_iPortfolioTeacherOLE_EXT($fb);break;
			case "iPortfolioTeacherOLEStudentView_EXT":
				$x = $this->displayFormat_iPortfolioTeacherOLEStudentView_EXT();break;
			case "iPortfolioStudentOLE1_EXT":
				$x = $this->displayFormat_iPortfolioStudentOLE1_EXT();break;
			case "iPortfolioOLEPool":
				$x = $this->displayFormat_iPortfolioOLEPool($fb);break;
			case "iPortfolio_OLE_Join":
			    $x = $this->displayFormat_iPortfolio_OLE_Join();break;
			case "iPortfolio_DBS_Predicted_Grade":
			    $x = $this->displayFormat_iPortfolio_DBS_Predicted_Grade();break;
			case "iPortfolio_DBS_Predicted_Grade_IB":
			    $x = $this->displayFormat_iPortfolio_DBS_Predicted_Grade_IB();break;
			case "eEnrolmentGroupEnrolList":
				$x = $this->displayFormat_eEnrolmentGroupEnrolList($fb);break;	
			case "displayEnrolmentTransferOLE":
				$x = $this->displayFormat_EnrolmentTransferOLE($fb, $fb2);break;
			case "eDisciplineDetentionSetting":
				$x = $this->displayFormat_eDisciplineDetentionSetting();break;
			case "eDisciplineDetentionNewRecord":
				$x = $this->displayFormat_eDisciplineDetentionNewRecord();break;
			case "eDisciplineDetentionDetail":
				$x = $this->displayFormat_eDisciplineDetentionDetail();break;
			case "eDisciplineDetentionTakeAttendance":
				$x = $this->displayFormat_eDisciplineDetentionTakeAttendance();break;
			case "eDisciplineDetentionList":
				$x = $this->displayFormat_eDisciplineDetentionList();break;
			case "eDisciplineDetentionStudentRecord":
				$x = $this->displayFormat_eDisciplineDetentionStudentRecord();break;
			case "eDisciplineDetentionStudentRecordInStudentView":
				$x = $this->displayFormat_eDisciplineDetentionStudentRecordInStudentView();break;
			case "eDisciplineConductMarkAdjust":
				$x = $this->displayFormat_eDisciplineConductMarkAdjust();break;
			case "eDisciplineCaseRecord":
				$x = $this->displayFormat_eDisciplineCaseRecord();break;	
			case "eDisciplineConductMarkView":
				$x = $this->displayFormat_eDisciplineConductMarkView();break;	
			case "eDisciplineGMMisCatSetting":
				$x = $this->displayFormat_eDisciplineGMMisCatSetting();break;
			case "eDisciplineGMMisNewLeafSetting":
				$x = $this->displayFormat_eDisciplineGMMisNewLeafSetting();break;
			case "eDisciplineGMGoodCatSetting":
				$x = $this->displayFormat_eDisciplineGMGoodCatSetting();break;
			# added by Kelvin on 11 Feb 09 for admin console->Admin Mgmt->School Calendar->School Events	
			case "adminMgtSchoolCalendarEvents":
				$x = $this->displayFormat_adminMgtSchoolCalendarEvents();break;
			
			# 20090714 yatwoon	
			case "GeneralDisplay":
				$x = $this->displayFormat_GeneralDisplay($fb); break;
			case "GeneralDisplayWithNavigation":
				$x = $this->displayFormat_GeneralDisplayWithNavigation($fb, $fb2); break;
			
			# 20091005 Ivan: Admin Console > SMS > View Sent Records
			case "adminSMSViewSentRecords":
				$x = $this->displayFormat_adminSMSViewSentRecords();break;
				
				
			# Repair System
			case "RepairSystem":
				$x = $this->displayFormat_RepairSystem();break;
				
			case "eLib_manage_license":
				$x = $this->displayFormat_eLib_manage_license($fb, $fb2);break;
			
			# Module License
			case "module_manage_license":
				$x = $this->displayFormat_module_manage_license($fb2);break;
				

			case "eAttendance_ParentLetter":
				$x = $this->displayFormat_eAttendance_ParentLetter();break;
			case "UserMgmtStaffAccountCentennial":
				$x = $this->displayFormat_UserMgmtStaffAccountCentennial();break;
			case "UserMgmtStaffAccount":
				$x = $this->displayFormat_UserMgmtStaffAccount();break;
			case "UserMgmtParentAccount":
				$x = $this->displayFormat_UserMgmtParentAccount();break;
			case "UserMgmtStudentAccount":
				$x = $this->displayFormat_UserMgmtStudentAccount();break;
			case "UserMgmtStudentNoClass":
				$x = $this->displayFormat_UserMgmtStudentNoClass();break;
			case "GroupSettingGroupList":
				$x = $this->displayFormat_GroupSettingGroupList();break;
			case "Malaysia_RegistryStatus":
				$x = $this->displayFormat_Malaysia_RegistryStatus();break;
			case "ReadingScheme_RecommendBookCoverView":
				$x = $this->displayFormat_ReadingScheme_RecommendBookCoverView();break;
			case "ReadingScheme_MyReadingBook":
				$x = $this->displayFormat_ReadingScheme_MyReadingBook();break;
			case "OEA_StudentList":
				$x = $this->displayFormat_OEA_StudentList(); break;
			case "ReadingSchemeReportComment":
				$x = $this->displayFormat_ReadingSchemeReportComment(); break;
			case "ReadingScheme_RecommendBookReport":
				$x = $this->displayFormat_ReadingScheme_RecommendBookReport($fb, $fb2, $fb3); break;
			case "eDiscipline_GM_Index":
				$x = $this->displayFormat_eDiscipline_GM_Index($fb, $fb2, $fb3); break;
			case "eDiscipline_WebSAMS_NotTransferredIndex":
				$x = $this->displayFormat_eDiscipline_WebSAMS_NotTransferredIndex($fb, $fb2, $fb3); break;
			case "DigitalArchive_Newest_More_Table":
				$x = $this->displayFormat_DigitalArchive_Newest_More_Table($fb, $fb2, $fb3); break;
			case "Study_Score_Management":
				$x = $this->displayFormat_Study_Score_Management($fb, $fb2, $fb3); break;
			case "Digital_Archive_Admin_Search_Result_Table":
				$x = $this->displayFormat_Digital_Archive_Admin_Search_Result_Table($fb, $fb2, $fb3); break;
			case "Digital_Archive_File_Format_Table":
				$x = $this->displayFormat_Digital_Archive_File_Format_Table($fb, $fb2, $fb3); break;
			
			### eEnrolment	
			case "Enrolment_Display_Club":
				$x = $this->displayFormat_Enrolment_Display_Club($fb, $fb2, $fb3, $fb4); break;
			### eEnrolment ReplySlip #P110324 
			case "Enrolment_Display_Event_ReplySlip_Cust":
				$x = $this->displayFormat_Enrolment_Display_Event_ReplySlip_Cust($fb, $fb2); break;
			### invoice mgmt system
			case "Invoice_List_Table"	:
				$x = $this->displayFormat_Invoice_List_Table($fb, $fb2, $fb3, $fb4); break;
				
			### ERC Rubrics Comment Bank Subject Details Comment
			case "ERC_rubrics_CommentBank_SubjectDetailsComment":
				$x = $this->displayFormat_ERC_Rubrics_CommentBank_SubjectDetailsComment();break;
			
			### ePost
			case "ePost_report":
				$x = $this->displayFormat_ePost_report($fb);break;
			case "ePost_portfolio_report":
				$x = $this->displayFormat_ePost_portfolio_report();break;
            case "ePost_article_shelf":
            	$x = $this->displayFormat_ePost_article_shelf();break;
			case "ePost_portfolio_report_new":
				$x = $this->displayFormat_ePost_portfolio_report_new();break;
			case "MessageCenter_SmsIndex":
				$x = $this->displayFormat_MessageCenter_SmsIndex();break;
			case "list4ajax":  
			    $x = $this->displayFormat_4ajax($fb, $fb2); break;
			default: $x = $this->displayFormat(); break;
                }
                return $x;
        }

        # ------------------------------------------------------------------------------------

        function displayFormat(){	        
                global $image_path;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $n_start = $this->n_start; #($this->pageNo-1)*$this->page_size;
                $x .= "<table width=95% border=0 cellpadding=2 cellspacing=1>\n";
                $x .= "<tr><td>\n";
                $x .= "<table width=100% border=0 cellpadding=2 cellspacing=1>\n";
                $x .= $this->displayColumn();
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td class=tableContent>".($n_start+$i)."</td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j]);
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                $x .= "</table>\n";
                $x .= "</td></tr>\n";
                if($this->db_num_rows()<>0) $x .= "<tr><td>".$this->navigation()."</td></tr>";
                $x .= "</table>\n";
                $this->db_free_result();
                return $x;
        }

        function displayFormat1($fb="",$fb2=""){
                global $image_path;
                global $i_campusmail_lettertext;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
                $x .= "<img src=$image_path/space.gif width=1 height=3 border=0><br>\n";
                $x .= "<table width=794 border=0 cellspacing=0 cellpadding=0>
                         <tr>
                           <td width=66 valign=top>
                             <p><br>$i_campusmail_lettertext<br>
                             <br><br></p>
                             <p>&nbsp;</p>
<p>&nbsp;</p>
<p><br>
<br>
<br>
<br>
<br>
<br>
<br>
<img src=/images/campusmail/letterbottom.gif> </p>
</td>
<td width=728 align=left valign=top>
<table width=685 border=0 cellspacing=0 cellpadding=0>
<tr>
<td><img src=/images/campusmail/papertop.gif width=685 height=10></td>
</tr>
</table>";
                $x .= "<table width=685 border=0 cellspacing=0 cellpadding=5 background=/images/campusmail/paperbg1.gif>
                        <tr><td width=655 align=right valign=top>$fb</td><td width=30></td></tr>
                        <tr><td width=655 align=right valign=top>$fb2</td><td width=30></td></tr>
                        </table>\n";

                $x .= "<table height=200 width=685 border=0 cellspacing=0 cellpadding=0 background=/images/campusmail/paperbg2.gif class=body>\n";
                $x .= "<tr><td><table width=100% border=0 cellpadding=0 cellspacing=0>\n";
                $x .= $this->displayColumn();
                if ($this->db_num_rows()==0){
                        $x .= "<tr>
                                        <td ><img src=$image_path/space.gif width=1 height=1 border=0></td>
                                        <td  align=center colspan=".($this->no_col-1)."><br>".$this->no_msg."<br><br></td>
                                        <td ><img src=$image_path/space.gif width=1 height=1 border=0></td>
                                        </tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td ><img src=$image_path/space.gif width=1 height=1 border=0></td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j]);
                                        $x .= "<td ><img src=$image_path/space.gif width=1 height=1 border=0></td>\n";
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                $x .= "<tr>\n";
                $x .= "<td ><img src=$image_path/space.gif width=14 height=26 border=0></td>\n";
                $x .= "<td  colspan=".($this->no_col-1)."><img src=$image_path/space.gif width=1 height=1 border=0></td>\n";
                $x .= "<td ><img src=$image_path/space.gif width=21 height=26 border=0></td>\n";
                $x .= "</tr>\n";

                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=4>&nbsp;</td><td bgcolor=#FFE6BC colspan=".($this->no_col-4).">".$this->navigation()."</td><td></td></tr>\n";

                $x .= "</table></td></tr></table>\n";

                $x .= "<table width=685 border=0 cellspacing=0 cellpadding=0>
                <tr>
                  <td><img src=/images/campusmail/paperbottom.gif width=685 height=70></td>
                </tr>
              </table>
              </td>
        </tr>
      </table>
";
                $this->db_free_result();
                return $x;
        }
        function displayFormat8($fb="",$fb2=""){
                global $image_path;
                global $i_campusmail_lettertext;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
                $x .= "<img src=$image_path/space.gif width=1 height=3 border=0><br>\n";
                $x .= "<table width=794 border=0 cellspacing=0 cellpadding=0>
                         <tr>
                           <td width=66 valign=top>
                             <p><br><br>
                             <br><br></p>
                             <p>&nbsp;</p>
<p>&nbsp;</p>
<p><br>
<br>
<br>
<br>
<br>
<br>
<br>
 </p>
</td>
<td width=728 align=left valign=top>
<table width=685 border=0 cellspacing=0 cellpadding=0>
<tr>
<td><img src=/images/campusmail/papertop.gif width=685 height=10></td>
</tr>
</table>";
                $x .= "<table width=685 border=0 cellspacing=0 cellpadding=5 background=/images/campusmail/paperbg1.gif>
                        <tr><td width=655 align=right valign=top>$fb</td><td width=30></td></tr>
                        <tr><td width=655 align=right valign=top>$fb2</td><td width=30></td></tr>
                        </table>\n";

                $x .= "<table height=200 width=685 border=0 cellspacing=0 cellpadding=0 background=/images/campusmail/paperbg2.gif class=body>\n";
                $x .= "<tr><td><table width=100% border=0 cellpadding=0 cellspacing=0>\n";
                $x .= $this->displayColumn();
                if ($this->db_num_rows()==0){
                        $x .= "<tr>
                                        <td ><img src=$image_path/space.gif width=1 height=1 border=0></td>
                                        <td  align=center colspan=".($this->no_col-1)."><br>".$this->no_msg."<br><br></td>
                                        <td ><img src=$image_path/space.gif width=1 height=1 border=0></td>
                                        </tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td ><img src=$image_path/space.gif width=1 height=1 border=0></td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j]);
                                        $x .= "<td ><img src=$image_path/space.gif width=1 height=1 border=0></td>\n";
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                $x .= "<tr>\n";
                $x .= "<td ><img src=$image_path/space.gif width=14 height=26 border=0></td>\n";
                $x .= "<td  colspan=".($this->no_col-1)."><img src=$image_path/space.gif width=1 height=1 border=0></td>\n";
                $x .= "<td ><img src=$image_path/space.gif width=21 height=26 border=0></td>\n";
                $x .= "</tr>\n";

                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=4>&nbsp;</td><td bgcolor=#FFE6BC colspan=".($this->no_col-4).">".$this->navigation()."</td><td></td></tr>\n";

                $x .= "</table></td></tr></table>\n";

                $x .= "<table width=685 border=0 cellspacing=0 cellpadding=0>
                <tr>
                  <td><img src=/images/campusmail/paperbottom.gif width=685 height=70></td>
                </tr>
              </table>
              </td>
        </tr>
      </table>
";
                $this->db_free_result();
                return $x;
        }
        function displayFormat2($width=""){
                global $image_path;
                $i = 0;
//                                debug_pr($this->built_sql());

                $this->rs = $this->db_db_query($this->built_sql());
                $width = ($width!="") ? $width : 560;
                $n_start = $this->n_start;
		$x = '';
                $x .= "<table width=$width border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
                $x .= $this->displayColumn();
                
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        $css = ($i%2) ? "" : "2";
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j], $css);
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tableTitle'>".$this->navigation()."</td></tr>";
                $x .= "</table>\n";
                $this->db_free_result();
                return $x;
        }

        function displayFormat_Staff($width=""){
        				global $image_path;
                $i = 0;
				//echo "<p>".$this->built_sql()."</p>";
                $this->rs = $this->db_db_query($this->built_sql());
                $width = ($width!="") ? $width : 560;
                $n_start = $this->n_start;
                $x .= "<table width=$width border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
                $x .= $this->displayColumn();
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        $css = ($i%2) ? "" : "2";
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j], $css);
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." bgcolor='#88c4d7'>".$this->navigation_staff()."</td></tr>";
                $x .= "</table>\n";
                $this->db_free_result();
                return $x;
        }

        function displayFormat3(){
                global $image_path;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
                $x .= "<table width=93% border=0 cellpadding=2 cellspacing=0>\n";
                $x .= $this->displayColumn();
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td class=group_bulletin align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
	                                    $i++;
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td class=group_bulletin>".($n_start+$i)."</td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j]);
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col.">".$this->navigation()."</td></tr>";
                $x .= "</table>\n";
                $this->db_free_result();
                return $x;
        }

        # Front end group info admin settings display announcement and events
        function displayFormat4()
        {
                global $image_path;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $n_start = $this->n_start;  # ($this->pageNo-1)*$this->page_size;
                $x .= "<table width=750 border=0 cellspacing=0 cellpadding=2 background=/images/groupinfo/popup_tbbg_large.gif>\n";
                $x .= $this->displayColumn();
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td>&nbsp;</td><td bgcolor=#FFE6BC style='vertical-align:middle' align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td><td>&nbsp;</td></tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td >&nbsp;</td>\n";
                                        $x .= "<td class=body>".($n_start+$i)."</td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j]);
                                        $x .= "<td >&nbsp;</td>\n";
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                if($this->db_num_rows()<>0)
                {
                   $inside = "<table><tr><td width=35></td><td width=680 bgcolor=#FFE6BC style='vertical-align:middle' align=center>".$this->navigation()."</td><td width=35>&nbsp;</td></tr></table>\n";
                   $x .= "<td></td><td colspan=".$this->no_col.">$inside</td><td></td></tr>\n";
                }
                $x .= "</table>\n";
                $this->db_free_result();
                return $x;
        }

        function displayFormat4_old()
        {
                global $image_path;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $n_start = $this->n_start;  # ($this->pageNo-1)*$this->page_size;
                $x .= "<table width=553 border=0 cellspacing=0 cellpadding=2 background=/images/groupinfo/popup_tbbg.gif>\n";
                $x .= $this->displayColumn();
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td>&nbsp;</td><td bgcolor=#FFE6BC style='vertical-align:middle' align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td><td>&nbsp;</td></tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td >&nbsp;</td>\n";
                                        $x .= "<td class=body>".($n_start+$i)."</td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j]);
                                        $x .= "<td >&nbsp;</td>\n";
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                if($this->db_num_rows()<>0)
                {
                   $inside = "<table><tr><td width=35></td><td width=453 bgcolor=#FFE6BC style='vertical-align:middle' align=center>".$this->navigation()."</td><td width=35>&nbsp;</td></tr></table>\n";
                   $x .= "<td></td><td colspan=".$this->no_col.">$inside</td><td></td></tr>\n";
                }
                $x .= "</table>\n";
                $this->db_free_result();
                return $x;
        }

        function displayFormat5(){
                global $image_path;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $n_start = $this->n_start;  # ($this->pageNo-1)*$this->page_size;
                $x .= "<table width='100%' border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=5 cellspacing=3>\n";
                $x .= $this->displayColumn();
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        if($i>$this->page_size) break;
                                        $css = ($i%2) ? "" : "2";
                                        $x .= "<tr>\n";
                                        $x .= "<td class=tableCortent$css>".($n_start+$i)."</td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= "<td class=tableContent$css>".$row[$j]."</td>\n"; //$this->displayCell($j,$row[$j]);
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col.">".$this->navigation()."</td></tr>";
                $x .= "</table>\n";
                $this->db_free_result();
                return $x;
        }

        # Question Bank console table view
        # First 7 fields must be : QuestionID, QuestionCode, QuestionChi,QuestionEng, Level, Category, Difficulty
        function displayFormat6()
        {
          $i=0;

          #$this->n_start=($this->pageNo-1)*$this->page_size;

          $this->rs=$this->db_db_query($this->built_sql());
          $n_start = $this->n_start;  # ($this->pageNo-1)*$this->page_size;

          $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
          //$x.="<table width='96%' border='1' align='center' cellpadding='0' cellspacing='0'>\n";
          $x .= "<table width=100% border=1 cellpadding=5 cellspacing=0 bordercolorlight=#DDEEFA bordercolordark=#AAB9D4 class=body>\n";
/*
          $x.="<tr>\n";
          $x.="<td width='1'><img src='$this->imagePath/frame/border2_up_left.gif' width='9' height='5'></td>\n";
          $x.="<td colspan='".$this->no_col."' nowrap background='$this->imagePath/frame/border2_bottom.gif'>\n";
          $x.="<img src='$this->imagePath/space.gif' width='1' height='5'></td>\n";
          $x.="<td width='1'><img src='$this->imagePath/frame/border2_up_right.gif' width='9' height='5'></td>\n";
          $x.="</tr>\n";
*/
          $x.=$this->displayColumn();

          if ($this->db_num_rows()==0){
               $x.="<tr><td nowrap>&nbsp;</td><td class=tableContent2 align=center colspan=".$this->no_col."><br><br>".$this->no_msg."<br><br><br></td></tr>\n";

          }else{

               #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){

               while($row = $this->db_fetch_array()) {

                    $i++;

                    if($i>$this->page_size) break;

                    //$x.="<tr><td background='$this->imagePath/frame/border2_left.gif'>&nbsp;</td>\n";
                    $x .= "<tr>";

//                    $cn = ( $i%2 == 1) ? "1" : "2";
//                    $x.= ($this->noNumber) ? "" : "<td class=tableContent$cn height='35' align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
                    $x.= ($this->noNumber) ? "" : "<td>".($this->n_start+$i)."</td>\n" ;

                    $QuestionID = $row[0];
                    $QuestionCode = $row[1];
                    $QuestionEng = $row[2];
                    $QuestionChi = $row[3];
                    $QuestionLvl = $row[4];
                    $QuestionCat = $row[5];
                    $QuestionDiff = $row[6];
                    $QuestionScore = $row[7];

                    $dataStartCol = 8;
                    for($j=$dataStartCol; $j<$dataStartCol+$this->no_col-1; $j++){

                         $t=$row[$j];

                         if ($j==$dataStartCol) {
                              if ($this->QueType==2) {
                                   $question = getQuestion($t, $QuestionID);
                                   $Que_Title_full = $question[0];
                                   $Que_Title_cut = $question[1];
                                   $question_en = getQuestion($QuestionEng, $QuestionID);
                                   $Que_Title_full_en = $question_en[0];
                                   $Que_Title_cut_en = $question_en[1];
                                   $question_b5 = getQuestion($QuestionChi, $QuestionID);
                                   $Que_Title_full_b5 = $question_b5[0];
                                   $Que_Title_cut_b5 = $question_b5[1];
                                   $tipContent = makeQuestionToolTipContent($QuestionCode,$QuestionLvl,$QuestionCat,$QuestionDiff,$Que_Title_full_en,$Que_Title_full_b5,$QuestionScore);
                                   $t = "<a href='javascript:view($QuestionID)' onMouseOut='hideTip()' onBlur='hideTip()' onMouseMove='overhere()' \n onMouseOver='tipsNow(\"$tipContent\")'>".nl2br($Que_Title_cut)."</a>\n <script language='javascript'>var str$QuestionID = \"".addslashes(preg_replace("/(\015\012)|(\015)|(\012)/","&nbsp;<br />", $Que_Title_full))."\"</script>\n";
                              } else {
                                   $separator = "===@@@===";
                                   $question_bulletin_split = "===###===";
                                   $temp = explode($question_bulletin_split,$t);
                                   //$display_string = (strlen($temp[0])>50? substr($temp[0],0,50)."..." : $temp[0]);
                                   //$t = $display_string.$temp[1];
                                   $question = explode($separator, $temp[0]);
                                   $q = $question[0];
                                   if (strlen($q)>50)
                                   {
                                       $q = htmlspecialchars(substr($q,0,50)." ...");
                                   }
                                   else
                                   {
                                       $q = htmlspecialchars($q);
                                   }
                                   $q .= $temp[1];
                                   $question[0] = $q;
                                   $Que_HTML = 1;
                                   $Que_HTML_en = 1;
                                   $Que_HTML_b5 = 1;
                                   $Que_Title = $question[0];
                                   //$Que_Title = str_replace(">","&gt",$Que_Title);
                                   //$Que_Title = str_replace("<","&lt",$Que_Title);

                                   $Que_HTML = $question[1];
                                   $Que_Title_Tip = ($Que_HTML==1) ? undo_htmlspecialchars($Que_Title) : $Que_Title;
                                   $question_en = explode($separator, $QuestionEng);
                                   $Que_Title_en = $question_en[0];
                                   $Que_HTML_en = $question_en[1];
                                   $Que_Title_Tip_en = ($Que_HTML_en==1) ? undo_htmlspecialchars($Que_Title_en) : $Que_Title_en;
                                   $question_b5 = explode($separator, $QuestionChi);
                                   $Que_Title_b5 = $question_b5[0];
                                   $Que_HTML_b5 = $question_b5[1];
                                   $Que_Title_Tip_b5 = ($Que_HTML_b5==1) ? undo_htmlspecialchars($Que_Title_b5) : $Que_Title_b5;
                                   $tipContent = makeQuestionToolTipContent($QuestionCode,$QuestionLvl,$QuestionCat,$QuestionDiff,$Que_Title_Tip_en,$Que_Title_Tip_b5,$QuestionScore);
                                   $t = "<a href='javascript:view($QuestionID)' onMouseOut='hideTip()' onBlur='hideTip()' onMouseMove='overhere()' \n onMouseOver='tipsNow(\"$tipContent\")'>".nl2br($Que_Title)."</a>\n <script language='javascript'>var str$QuestionID = \"".addslashes(preg_replace("/(\015\012)|(\015)|(\012)/","&nbsp;<br />", $Que_Title_Tip))."\"</script>\n";
                              }
                         }
                         $x.=$this->displayCell($j,$t); //, $cn);

                    }

                    //$x.="<td background='$this->imagePath/frame/border2_right.gif'>&nbsp;</td></tr>\n";
                    $x .= "</tr>\n";

               }
               #}

          }
          if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col.">".$this->navigation()."</td></tr>";

          $x .= "</table>\n";
          $this->db_free_result();
          return $x;

/*
          $x.="<tr><td><img src='$this->imagePath/frame/border2_bt_left.gif' width='9' height='5'></td>";
          $x.="<td colspan='".$this->no_col."' background='$this->imagePath/frame/border2_bottom.gif'>";
          $x.="<img src='$this->imagePath/space.gif' width='5' height='5'></td>";
          $x.="<td><img src='$this->imagePath/frame/border2_bt_right.gif' width='9' height='5'></td></tr>\n";

          if($this->db_num_rows()<>0) $x.= "<tr><td>&nbsp;</td><td colspan='".($this->no_col)."'>".$this->navigation()."</td><td>&nbsp;</td></tr>";
          $x.="</table>\n";
          if ($this->db_num_rows()==0) $x .= "<br>
&nbsp;\n";
          $this->db_free_result();

          return $x;
*/
        }
        function displayFormat7($width=""){
                global $image_path;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $width = ($width!="") ? $width : "100%";
                $n_start = $this->n_start;
                $x .= "<table width=$width border=1 bordercolordark=#D0C68F bordercolorlight=#D0C68F cellpadding=3 cellspacing=0 class=body>\n";
                $x .= $this->displayColumn();
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td class=admin_center_announcement_tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        $css = ($i%2) ? "" : "2";
                                        if($i>$this->page_size) break;
                                        $x .= "<tr >\n";
                                        $x .= "<td class=admin_center_announcement_tableContent$css>".($n_start+$i)."</td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j], $css);
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                $x .= "</table>\n";

                if($this->db_num_rows()<>0)
                {
                   $x .= "<br><table width=$width border=0 cellspacing=0 cellpadding=0>\n";
                   $x .= "<tr><td width=5 align><img src=\"$image_path/admin_center/page_l.gif\" width=5 height=27></td>\n";
                   $x .= "<td class=td_bottom bgcolor=#FCDF7E colspan=".($this->no_col-2).">".$this->navigation()."</td>\n";
                   $x .= "<td width=5><img src=\"$image_path/admin_center/page_r.gif\" width=5 height=27></td></tr>\n";
                   $x .= "</table>\n";
                }
                $this->db_free_result();
                return $x;
        }
        function displayFormat9($width=""){
	        	
                global $image_path;
                global $i_campusmail_lettertext;
                $i = 0;
                	
                $this->rs = $this->db_db_query($this->built_sql());
                
                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;

                if ($width == "") $width = "100%";
                $x = "";
                $x .= "<table width=$width border=0 cellpadding=1 cellspacing=1>\n";
                $x .= $this->displayColumn();
                if ($this->db_num_rows()==0){
                        $x .= "<tr>
                                        <td ><img src=$image_path/space.gif width=1 height=1 border=0></td>
                                        <td  align=center colspan=".($this->no_col-1)."><br>".$this->no_msg."<br><br></td>
                                        <td ><img src=$image_path/space.gif width=1 height=1 border=0></td>
                                        </tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td class=nothing>".($n_start+$i)."</td>\n";
                                        #$x .= "<td ><img src=$image_path/space.gif width=1 height=1 border=0></td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j]);
                                        #$x .= "<td ><img src=$image_path/space.gif width=1 height=1 border=0></td>\n";
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                $x .= "<tr>\n";
                $x .= "<td ><img src=$image_path/space.gif width=14 height=26 border=0></td>\n";
                $x .= "<td  colspan=".($this->no_col-1)."><img src=$image_path/space.gif width=1 height=1 border=0></td>\n";
                $x .= "<td ><img src=$image_path/space.gif width=21 height=26 border=0></td>\n";
                $x .= "</tr>\n";

                if($this->db_num_rows()<>0) $x .= "<tr><td>&nbsp;</td><td bgcolor=#FFE6BC colspan=".($this->no_col-1).">".$this->navigation()."</td><td></td></tr>\n";

                $x .= "</table>\n";

                $this->db_free_result();
                return $x;
        }

                # for alumni survey
        function displayFormat10(){
                global $image_path;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
                $x .= "<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolorlight='#5DA5C9' bordercolordark='#FFFFFF' bgcolor='#FFFFFF' class='13-black'>\n";

                $x .= $this->displayColumn2();
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td class='13-black'>".($n_start+$i)."</td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j]);
                                        $x .= "</tr>\n";
                                }
                        #}
                }
//                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col.">".$this->navigation2()."</td></tr>";
                $x .= "</table>\n";
                $this->db_free_result();
                return $x;
        }
        function displayFormat11($width="",$borderlight="",$borderdark="",$bgcolor="",$navbgcolor=""){
                global $image_path;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $width = ($width!="") ? $width : "100%";
                $borderlight = ($borderlight!="")? $borderlight : "FFFFFF";
                $borderdark = ($borderdark!="")? $borderdark : "DDEF5D";
                $bgcolor = ($bgcolor!="")? $bgcolor : "F2F8CA";
                $navbgcolor = ($navbgcolor!="")? $navbgcolor : "E8F2A6";

                $n_start = $this->n_start;
                $x .= "<table width=$width border=1 bordercolordark=#$borderdark bordercolorlight=#$borderlight cellpadding=2 cellspacing=0 align='center' bgcolor=#$bgcolor>\n";
                $x .= $this->displayColumn();
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td class=atableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        $css = ($i%2) ? "" : "2";
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td class=atableContent$css>".($n_start+$i)."</td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j], $css);
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='atableTitle'>".$this->navigation($navbgcolor)."</td></tr>";
                $x .= "</table>\n";
                $this->db_free_result();
                return $x;
        }
        
        #---------------------------------------------------------------------------------------------------
        # /admin/photoalbum : index.php - 2 Columns View Type
        #---------------------------------------------------------------------------------------------------
        function displayFormat12($width=""){
                global $image_path;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $width = ($width!="") ? $width : 560;
                $n_start = $this->n_start;
                $x .= "<table width=$width border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
                $x .= $this->displayColumn();                
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
                }else{                        
                        while($row = $this->db_fetch_array()) {
                                $i++;
                                $css = ($i%2) ? "" : "2";
                                if($i>$this->page_size) break;
                                $x .= ($i%2) ? "<tr>\n" : "";
                                //$x .= "<tr>\n";
                                $x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
                                for($j=0; $j<$this->no_col-1; $j++)
                                $x .= $this->displayCell($j,$row[$j], $css);                                
                                $x .= ($i%2) ? "" : "</tr>\n";
                                //$x .= "</tr>\n";
                        }                        
                        
                }
                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tableTitle'>".$this->navigation()."</td></tr>";
                $x .= "</table>\n";
                $this->db_free_result();
                return $x;
        }        

        # ------------------------------------------------------------------------------------
        # Special display for iMail
        function displayFormat_iMail($fb="",$fb2=""){
                global $image_path;
                global $i_campusmail_lettertext;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                
                $n_start = $this->n_start;  #($this->pageNo-1)*$this->page_size;
                $x .= "<img src=$image_path/space.gif width=1 height=3 border=0><br>\n";
                $x .= "<table width=794 border=0 cellspacing=0 cellpadding=0>
                         <tr>
                           <td width=66 valign=top>
                             <p><br><br>
                             <br><br></p>
                             <p>&nbsp;</p>
<p>&nbsp;</p>
<p><br>
<br>
<br>
<br>
<br>
<br>
<br>
 </p>
</td>
<td width=728 align=left valign=top>
<table width=685 border=0 cellspacing=0 cellpadding=0>
<tr>
<td><img src=/images/campusmail/papertop.gif width=685 height=10></td>
</tr>
</table>";
                $x .= "<table width=685 border=0 cellspacing=0 cellpadding=5 background=/images/campusmail/paperbg1.gif>
                        <tr><td width=655 align=right valign=top>$fb</td><td width=30></td></tr>
                        <tr><td width=655 align=right valign=top>$fb2</td><td width=30></td></tr>
                        </table>\n";

                $x .= "<table height=200 width=685 border=0 cellspacing=0 cellpadding=0 background=/images/campusmail/paperbg2.gif class=body>\n";
                $x .= "<tr><td><table width=100% border=0 cellpadding=0 cellspacing=0>\n";
                $x .= $this->displayColumn();
                if ($this->db_num_rows()==0){
                        $x .= "<tr>
                                        <td ><img src=$image_path/space.gif width=1 height=1 border=0></td>
                                        <td  align=center colspan=".($this->no_col-1)."><br>".$this->no_msg."<br><br></td>
                                        <td ><img src=$image_path/space.gif width=1 height=1 border=0></td>
                                        </tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td ><img src=$image_path/space.gif width=1 height=1 border=0></td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j]);
                                        $x .= "<td ><img src=$image_path/space.gif width=1 height=1 border=0></td>\n";
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                $x .= "<tr>\n";
                $x .= "<td ><img src=$image_path/space.gif width=14 height=26 border=0></td>\n";
                $x .= "<td  colspan=".($this->no_col-1)."><img src=$image_path/space.gif width=1 height=1 border=0></td>\n";
                $x .= "<td ><img src=$image_path/space.gif width=21 height=26 border=0></td>\n";
                $x .= "</tr>\n";

                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=4>&nbsp;</td><td bgcolor=#FFE6BC colspan=".($this->no_col-4).">".$this->navigation()."</td><td></td></tr>\n";

                $x .= "</table></td></tr></table>\n";

                $x .= "<table width=685 border=0 cellspacing=0 cellpadding=0>
                <tr>
                  <td><img src=/images/campusmail/paperbottom.gif width=685 height=70></td>
                </tr>
              </table>
              </td>
        </tr>
      </table>
";
                $this->db_free_result();
                return $x;
        }

        # ------------------------------------------------------------------------------------

        function displayFormat_eSportAdmin($width=""){
                global $image_path;
                global $i_Sports_AdminConsole_UserType;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $width = ($width!="") ? $width : 560;
                $n_start = $this->n_start;
                $x = '';
                $x .= "<table width=$width border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
                $x .= $this->displayColumn();
                
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
                }else{
                        while($row = $this->db_fetch_array()) {
                                $i++;
                                $css = ($i%2) ? "" : "2";
                                if($i>$this->page_size) break;
                                $x .= "<tr>\n";
                                $x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
                                for($j=0; $j<$this->no_col-1; $j++) {
                                        if ($j == 2) {
                                                $x .= $this->displayCell($j,$i_Sports_AdminConsole_UserType[$row[$j]], $css);
                                        } else {
                                                $x .= $this->displayCell($j,$row[$j], $css);
                                        }
                                }
                                $x .= "</tr>\n";
                        }
                }
                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tableTitle'>".$this->navigation()."</td></tr>";
                $x .= "</table>\n";
                $this->db_free_result();
                return $x;
        }
        
        function displayFormat_adminMgtSchoolCalendarEvents($width="", $tablecolor=""){
                global $image_path;
                $i = 0;
                $this->rs = $this->db_db_query($this->built_sql());
                $width = ($width!="") ? $width : 560;
                $n_start = $this->n_start;
		$x = '';
                $x .= "<table width=$width border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
                $x .= $this->displayColumn();
                
                if ($this->db_num_rows()==0){
                        $x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
                }else{
                        #if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
                                while($row = $this->db_fetch_array()) {
                                        $i++;
                                        $css = ($i%2) ? "" : "2";
                                        if($i>$this->page_size) break;
                                        $x .= "<tr>\n";
                                        $x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
                                        for($j=0; $j<$this->no_col-1; $j++)
                                        $x .= $this->displayCell($j,$row[$j], $css);
                                        $x .= "</tr>\n";
                                }
                        #}
                }
                if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tableTitle'>".$this->navigation()."</td></tr>";
                $x .= "</table>\n";
                $this->db_free_result();
                return $x;
        }
        
        
        function GeneralDisplayWithNavigation()
        {
                global $image_path, $LAYOUT_SKIN;

                $this->rs = $this->db_db_query($this->built_sql());

				$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>";
				$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
				
				$x .= $this->displayColumn();

				if ($this->db_num_rows()==0)
				{
					$x .= "<tr>
					<td class='tablerow2 tabletext' align='center' colspan='". $this->no_col ."' ><br />".$this->no_msg."<br /><br></td>
					</tr>\n
					";
				} else {
					$i=0;
					
					while($row = $this->db_fetch_array())
					{
						$x .= "<tr class='tablerow". ( ($i)%2 + 1 )."'>\n";
						
						for($j=0; $j<$this->no_col; $j++)
						{
							if (trim($row[$j]) == "")
							{
								$row[$j] = "&nbsp;";
							}
							$x .= $this->displayCell($j,nl2br($row[$j]), "tablelink","valign='top'");
						}
						
						$x .= "</tr>\n";
						$i++;
					}
				}
				$x .= "</table>";
				$x .=" </td></tr>";
				//$x .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
				if($this->db_num_rows()<>0) $x .= "<tr><td colspan='".$this->no_col."' class='table{$tablecolor}bottom'>".$this->navigation()."</td></tr>";
				$x .= "</table>\n";
				$this->db_free_result();
				return $x;
        }
        
        function displayFormat_adminSMSViewSentRecords($width="", $tablecolor=""){
			global $image_path, $sms_vendor, $i_SMS;
			
			$i = 0;
			$this->rs = $this->db_db_query($this->built_sql());
			$width = ($width!="") ? $width : 560;
			$n_start = $this->n_start;
			$x = '';
			$x .= "<table width=$width border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
				$x .= $this->displayColumn();
			
				if ($this->db_num_rows()==0)
				{
					$x .= "<tr><td class=tableContent align=center colspan=".$this->no_col."><br>".$this->no_msg."<br><br></td></tr>\n";
				}
				else
				{
					#if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
					while($row = $this->db_fetch_array()) {
						$i++;
						$css = ($i%2) ? "" : "2";
						
						if($i > $this->page_size) break;
						
						$x .= "<tr>\n";
							$x .= "<td class=tableContent$css>".($n_start+$i)."</td>\n";
							for($j=0; $j<$this->no_col-1; $j++)
							{
								# Distinct(a.SourceID)
								if ($j == 0)
									continue;
									
								$x .= $this->displayCell($j,$row[$j], $css);
							}
						$x .= "</tr>\n";
					}
					#}
				}
			if($this->db_num_rows()<>0) $x .= "<tr><td colspan=".$this->no_col." class='tableTitle'>".$this->navigation()."</td></tr>";
			$x .= "</table>\n";
			$this->db_free_result();
			return $x;
		}
		
		

}
?>
