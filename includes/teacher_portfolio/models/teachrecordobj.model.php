<?php
if (defined('TEACHER_PORTFOLIO')) {
	if (!class_exists("CommonLibdb")) include_once(dirname(__FILE__) . "/commonlibdb.model.php");
	if (!class_exists("TeachRecordObj")) {
		class TeachRecordObj extends CommonLibdb {
			public function __construct() {
				parent::__construct();
				$this->userTable = $this->defaultUserTable;
			}
			
			public function getDataTableArray($UserID=0, $postData = array()) {
				$data = array();
				$jsonData = array(
					"recordsTotal" => 0,
					"recordsFiltered" => 0,
					"data" => $data
				);
				$strCondition = "";
				if (!empty($postData["selyear"]) && $postData["selyear"] != "all") {
					$selectedYear = $postData["selyear"];
					$strCondition = " AND ay.AcademicYearID='" . $this->Get_Safe_Sql_Query($postData["selyear"]) . "'";
				} else {
					$selectedYear = 'all';
				}
				$strSQL_main = 'SELECT count(*) as totalrecord FROM ';
				$strSQL_body = "";
				if (!isset($postData["tableType"])) $postData["tableType"] = "class";
				switch ($postData["tableType"]) {
					case "subject":
						$strSQL_body .= ' SUBJECT_TERM_CLASS_TEACHER AS stct ';
						$strSQL_body .= ' INNER JOIN SUBJECT_TERM_CLASS AS stc ON (stct.SubjectGroupID = stc.SubjectGroupID) ';
						$strSQL_body .= ' INNER JOIN SUBJECT_TERM AS st ON (stc.SubjectGroupID = st.SubjectGroupID) ';
						$strSQL_body .= ' INNER JOIN ACADEMIC_YEAR_TERM as ayt ON (st.YearTermID = ayt.YearTermID) ';
						$strSQL_body .= ' INNER JOIN ACADEMIC_YEAR as ay ON (ayt.AcademicYearID = ay.AcademicYearID) ';
						$strSQL_body .= ' INNER JOIN ' . $this->userTable . ' as IU on (IU.UserID=stct.UserID and IU.UserID="' . $this->Get_Safe_Sql_Query($UserID) . '")';
						$strSQL_body .= ' WHERE ';
						$strSQL_body .= ' stct.UserID="' . $this->Get_Safe_Sql_Query($UserID) . '"' . $strCondition;
						/************************************************************************/
						$totalRec = $this->returnDBResultSet($strSQL_main . $strSQL_body);
						
						$order_field = $postData["order"][0]["column"];
						switch ($order_field) {
							case "1" :
							case "course" :
								$order_field = Get_Lang_Selection("ClassTitleB5", "ClassTitleEN");
								break;
							default:
								$order_field = "ay.Sequence";
								break;
						}
						$strOrder = "";
						if ($order_field == "ay.Sequence") {
							$strOrder = " ORDER BY ay.Sequence " . $postData["order"][0]["dir"] . ", YearNameEN " . $postData["order"][0]["dir"];
						} else {
							if (!empty($order_field)) {
								$strOrder = " ORDER BY " . $order_field . " " . $postData["order"][0]["dir"];
							}
						}
						$strSQL = "SELECT IU.UserID, " . Get_Lang_Selection("ChineseName", "EnglishName") . " AS TeacherName, ClassTitleEN, ClassTitleB5, YearNameEN, YearNameB5, ay.Sequence FROM";
						break;
					default:
						$strSQL_body .= ' YEAR_CLASS_TEACHER as yct';
						$strSQL_body .= ' INNER JOIN YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID)';
						$strSQL_body .= ' INNER JOIN ACADEMIC_YEAR as ay ON (yc.AcademicYearID = ay.AcademicYearID)';
						$strSQL_body .= ' INNER JOIN ' . $this->userTable . ' as IU on (IU.UserID=yct.UserID and IU.UserID="' . $this->Get_Safe_Sql_Query($UserID) . '")';
						$strSQL_body .= ' WHERE ';
						$strSQL_body .= ' yct.UserID="' . $this->Get_Safe_Sql_Query($UserID) . '"' . $strCondition;
						/************************************************************************/
						$totalRec = $this->returnDBResultSet($strSQL_main . $strSQL_body);
						$order_field = $postData["order"][0]["column"];
						switch ($order_field) {
							case "1" :
							case "course" :
								$order_field = "ClassTitleEN";
								break;
							default:
								$order_field = "ay.Sequence";
								break;
						}
						$strOrder = "";
						if ($order_field == "ay.Sequence") {
							$strOrder = " ORDER BY ay.Sequence " . $postData["order"][0]["dir"] . ", YearNameEN " . $postData["order"][0]["dir"];
						} else {
							if (!empty($order_field)) {
								$strOrder = " ORDER BY " . $order_field . " " . $postData["order"][0]["dir"];
							}
						}
						$strSQL = "SELECT IU.UserID, " . Get_Lang_Selection("ChineseName", "EnglishName") . " AS TeacherName, ClassTitleEN, ClassTitleB5, YearNameEN, YearNameB5, ay.Sequence FROM";
						break;
				}
				$recordsTotal = isset($totalRec[0]["totalrecord"]) ? $totalRec[0]["totalrecord"] : 0;
				$recordsFiltered = isset($totalRec[0]["totalrecord"]) ? $totalRec[0]["totalrecord"] : 0;
				
				$strSQL .= $strSQL_body . $strOrder;
				/************************************************************************/
				$start = $postData["start"] ? $postData["start"] : 0;
				$pagelimit = $postData["length"] > 0 ? $postData["length"] : 0;
				$strLimit = " LIMIT " . $start . ", " . $pagelimit;
				if ($pagelimit > 0) $strSQL .= $strLimit;
				/************************************************************************/
				$queryData = $this->returnDBResultSet($strSQL);
				if (count($queryData) > 0) {
					foreach ($queryData as $kk => $RECORD) {
						$queryData[$kk]["academicYear"] = Get_Lang_Selection($RECORD["YearNameB5"], $RECORD["YearNameEN"]); 
						$queryData[$kk]["course"] = Get_Lang_Selection($RECORD["ClassTitleB5"], $RECORD["ClassTitleEN"]);
						$queryData[$kk]["subject"] = "";
					}
				}

				$jsonData = array(
					"recordsTotal" => $recordsTotal,
					"recordsFiltered" => $recordsFiltered,
					"allowExpand" => false,
					"acdyear" => $selectedYear,
					"data" => $queryData
				);
				return $jsonData;
			}
			
			public function exportAllData($CSVObj, $UserID, $secType) {
				$strSQL_body = "";
				$strCondition = "";
				if (!isset($secType)) $secType = "class";
				switch ($secType) {
					case "subject":
						$secType = "subject";
						$strSQL_body .= ' SUBJECT_TERM_CLASS_TEACHER AS stct ';
						$strSQL_body .= ' INNER JOIN SUBJECT_TERM_CLASS AS stc ON (stct.SubjectGroupID = stc.SubjectGroupID) ';
						$strSQL_body .= ' INNER JOIN SUBJECT_TERM AS st ON (stc.SubjectGroupID = st.SubjectGroupID) ';
						$strSQL_body .= ' INNER JOIN ACADEMIC_YEAR_TERM as ayt ON (st.YearTermID = ayt.YearTermID) ';
						$strSQL_body .= ' INNER JOIN ACADEMIC_YEAR as ay ON (ayt.AcademicYearID = ay.AcademicYearID) ';
						$strSQL_body .= ' INNER JOIN ' . $this->userTable . ' as IU on (IU.UserID=stct.UserID and IU.UserID="' . $this->Get_Safe_Sql_Query($UserID) . '")';
						$strSQL_body .= ' WHERE ';
						$strSQL_body .= ' stct.UserID="' . $this->Get_Safe_Sql_Query($UserID) . '"' . $strCondition;
						/************************************************************************/
						$strSQL = "SELECT stct.UserID, IU.EnglishName, IU.ChineseName, ClassTitleEN, ClassTitleB5, YearNameEN, YearNameB5, ay.Sequence FROM";
						break;
					default:
						$secType = "class";
						$strSQL_body .= ' YEAR_CLASS_TEACHER as yct';
						$strSQL_body .= ' INNER JOIN ' . $this->userTable . ' as IU on (IU.UserID=yct.UserID and IU.UserID="' . $this->Get_Safe_Sql_Query($UserID) . '")';
						$strSQL_body .= ' INNER JOIN YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID)';
						$strSQL_body .= ' INNER JOIN ACADEMIC_YEAR as ay ON (yc.AcademicYearID = ay.AcademicYearID)';
						$strSQL_body .= ' WHERE ';
						$strSQL_body .= ' yct.UserID="' . $this->Get_Safe_Sql_Query($UserID) . '"' . $strCondition;
						/************************************************************************/
						$strSQL = "SELECT yct.UserID, IU.EnglishName, IU.ChineseName, ClassTitleEN, ClassTitleB5, YearNameEN, YearNameB5, ay.Sequence FROM";
						break;
				}
				$strSQL .= $strSQL_body;
				$queryData = $this->returnDBResultSet($strSQL);
				if (count($queryData) > 0) {
					$rowData = array();
					$strSQL .= $strSQL_body;
					$i=0;
					$j=0;
					$rowData[$i] = array();
					$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["TeacherName"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
					$j++;
					$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["TeacherName"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
					$j++;
					$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["SchoolYear"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
					$j++;
					$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["SchoolYear"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
					$j++;
					if ($secType == "class") {
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Course"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
						$j++;
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Course"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
						$j++;
					} else {
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Subject"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
						$j++;
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Subject"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
						$j++;
					}
					$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Remarks"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
					$j++;
					$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Remarks"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
					$j++;
					$i++;
					foreach ($queryData as $qkk => $qvv) {
						$j = 0;
						$rowData[$i][$j] = $qvv["ChineseName"];
						$j++;
						$rowData[$i][$j] = $qvv["EnglishName"];
						$j++;
						$rowData[$i][$j] = $qvv["YearNameB5"];
						$j++;
						$rowData[$i][$j] = $qvv["YearNameEN"];
						$j++;
						$rowData[$i][$j] = $qvv["ClassTitleB5"];
						$j++;
						$rowData[$i][$j] = $qvv["ClassTitleEN"];
						$j++;
						$i++;
					}
					if (count($rowData) > 0) {
						$CSVObj->outputData($rowData, strtolower($this->core->data["section"] . "_" . $secType));
						exit;
					}
				}

				header('Content-type: text/plain; charset=utf-8');
				echo "Data not found";
				exit;
			}
		}
	}
}