<?php
if (defined('TEACHER_PORTFOLIO')) {
	if (!class_exists("CommonLibdb")) include_once(dirname(__FILE__) . "/commonlibdb.model.php");
	if (!class_exists("LogObj")) {
		class LogObj extends CommonLibdb {
			public function __construct() {
				parent::__construct();
				$this->tableName = 'INTRANET_TEACHER_PORTFOLIO_LOG';
				$this->userName = $this->defaultUserTable;
			}
			
			public function addRecord($userID, $logType = "INFO", $logDetail = "") {
				$cpdParam = array();
				$cpdParam[] = "TPLogType";
				$cpdParam[] = "TPLogDetail";
				$cpdParam[] = "InputBy";
				$cpdParam[] = "DateInput";
				$data = array();
				if ($logType == "CERTIFICATION") {
					$logType = "CERTIFICATE";
				}
				$data["TPLogType"] = array( "field" => "TPLogType", "data" => $logType);
				$data["TPLogDetail"] = array( "field" => "TPLogDetail", "data" => $logDetail);
				$data["InputBy"] = array( "field" => "InputBy", "data" => $userID);
				$data["DateInput"] = array( "field" => "DateInput", "data" => "__dbnow__");
				$strSQL = $this->buildInsertSQL($this->tableName, $cpdParam, $data);
				$result = $this->DBdb_query($strSQL);
			}
			
			public function getDataTableArray($postData) {
				$data = array();
				$jsonData = array(
					"recordsTotal" => 0,
					"recordsFiltered" => 0,
					"allowExpand" => false,
					"data" => $data
				);
				$recordsTotal = 0;
				$recordsFiltered = 0;
				
				$sortField = $postData["columns"][$postData["order"][0]["column"]]["data"];
				switch ($sortField) {
					case "UserName":
						$order_field = Get_Lang_Selection("ChineseName", "EnglishName");
						break;
					default:
						$order_field = $sortField;
				}
				$order_dir = "ASC";
				if (isset($postData["order"][0]["dir"])) {
					switch (strtoupper($postData["order"][0]["dir"])) {
						case "DESC":
							$order_dir = strtoupper($postData["order"][0]["dir"]);
							break;
					}
				}
				$queryData = array();
				
				$strSQL = "SELECT COUNT(TPLogID) AS totalrecord";
				$strSQL_st = " FROM " . $this->tableName . " AS lg";
				$strSQL_st .= " JOIN " . $this->userName . " AS ut ON (ut.UserID=lg.InputBy)";
				
				if (isset($postData["search"]["value"]) && !empty($postData["search"]["value"])) {
					$strSQL_st .= " WHERE " . Get_Lang_Selection("ChineseName", "EnglishName") . " LIKE '%" . $this->Get_Safe_Sql_Query($postData["search"]["value"]) . "%' OR TPLogType LIKE '%" . $this->Get_Safe_Sql_Query($postData["search"]["value"]) . "%' OR TPLogDetail like '%" . $this->Get_Safe_Sql_Query($postData["search"]["value"]) . "%' OR lg.DateInput like '%" . $this->Get_Safe_Sql_Query($postData["search"]["value"]) . "%'";
				}
				
				$strSQL .= $strSQL_st;
				$totalRec = $this->returnDBResultSet($strSQL);
				if ($totalRec != NULL && count($totalRec) > 0) {
					$recordsTotal = $totalRec[0]["totalrecord"] ? $totalRec[0]["totalrecord"] : 0;
					$recordsFiltered = $totalRec[0]["totalrecord"] ? $totalRec[0]["totalrecord"] : 0;

					$strSQL = "SELECT TPLogID, lg.InputBy, TPLogType as LogType, TPLogDetail as LogDetail, lg.DateInput as LogDate, " . Get_Lang_Selection("ChineseName", "EnglishName") . " as UserName";
					$strSQL .= $strSQL_st;
					$strOrder = " ORDER BY " . $order_field . " " . $order_dir;
					$strSQL .= $strOrder;
					/************************************************************************/
					$start = $postData["start"] ? $postData["start"] : 0;
					$pagelimit = $postData["length"] > 0 ? $postData["length"] : 0;
					$strLimit = " LIMIT " . $start . ", " . $pagelimit;
					if ($pagelimit > 0) $strSQL .= $strLimit;
					$result = $this->returnDBResultSet($strSQL);
					if (count($result) > 0) {
						
						$repArr = array(
							"Delete Record:" => "<h6 class='label label-danger'>DELETE RECORD</h6>",
							"Delete file:" => "<h6 class='label label-danger'>DELETE FILE</h6>",
							"Delete File:" => "<h6 class='label label-danger'>DELETE FILE</h6>",
							"Delete User(s) :" => "<h6 class='label label-danger'>DELETE USER</h6><br>",
							"Add Record:" => "<h6 class='label label-info'>ADD RECORD</h6>",
							"Add User(s) :" => "<h6 class='label label-info'>ADD USER</h6><br>",
							"Upload File:" => "<h6 class='label label-info'>UPLOAD FILE</h6>",
							"Update Record:" => "<h6 class='label label-success'>UPDATE RECORD</h6>",
							"Data Import:" => "<h6 class='label label-success'>DATA IMPORT</h6><br>",
							"Update Configs:" => "<h6 class='label label-success'>UPDATE CONFIGS</h6><br>",
							"Data Export:" => "<h6 class='label label-default'>DATA EXPORT</h6><br>",
							"Print Data:" => "<h6 class='label label-default'>PRINT DATA</h6><br>"
						);
						
						foreach ($result aS $pkey => $RECORD) {
							$queryData[$pkey] = $RECORD;
							$queryData[$pkey]["LogDetail"] = str_replace(array_keys($repArr), array_values($repArr), $queryData[$pkey]["LogDetail"]);
						}
					}
				}
				$jsonData = array(
					"recordsTotal" => $recordsTotal,
					"recordsFiltered" => $recordsFiltered,
					"allowExpand" => false,
					"data" => $queryData
				);
				return $jsonData;
			}
			
			public function getAllData($strip_tags = false) {
				$i = 0;
				$rowData[$i] = array(
						$this->core->data["lang"]["TeacherPortfolio"]["manage_logDate"],
						$this->core->data["lang"]["TeacherPortfolio"]["manage_userName"],
						$this->core->data["lang"]["TeacherPortfolio"]["manage_logType"],
						$this->core->data["lang"]["TeacherPortfolio"]["manage_logDetail"]
				);
				$i++;
				$strSQL = "SELECT TPLogType, TPLogDetail, EnglishName, ChineseName, lg.DateInput";
				$strSQL_st = " FROM " . $this->tableName . " AS lg";
				$strSQL_st .= " JOIN " . $this->userName . " AS ut ON (ut.UserID=lg.InputBy)";
				$strSQL .= $strSQL_st . " ORDER BY lg.DateInput DESC";
				$result = $this->returnDBResultSet($strSQL);
				
				$regArr = array("&gt;" => ">", "&lt;" => "<");
				
				if (count($result) > 0) {
					foreach ($result as $kk => $vv) {
						$rowData[$i] = array(
							$vv["DateInput"],
							Get_Lang_Selection($vv["ChineseName"], $vv["EnglishName"]),
							$vv["TPLogType"],
							($strip_tags) ? str_replace(array_keys($regArr), array_values($regArr), strip_tags($vv["TPLogDetail"])) : $vv["TPLogDetail"]
						);
						$i++;
					}
				}
				return $rowData;
			}
			
			public function exportAllData($CSVObj) {
				$rowData = $this->getAllData(true);
				if (count($rowData) > 0) {
					$CSVObj->outputData($rowData, strtolower("log_record"));
					exit;
				}
			}
		}
	}
}
