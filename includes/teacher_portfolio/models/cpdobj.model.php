<?php
if (defined('TEACHER_PORTFOLIO')) {
	if (!class_exists("CommonLibdb")) include_once(dirname(__FILE__) . "/commonlibdb.model.php");
	if (!class_exists("CpdObj")) {
		class CpdObj extends CommonLibdb {
			public function __construct() {
				parent::__construct();
				$this->optionsArr = array();
				$this->mainTable = "INTRANET_TEACHER_PORTFOLIO_CPD";
				$this->userTable = $this->defaultUserTable;
				$this->primaryKey = "CpdID";
				
				$this->relationCategoryTable = "INTRANET_TEACHER_PORTFOLIO_CPD_CATEGORY_RELATION";
				$this->optionsInfo = array(
					"category" => array(
						"table" => "INTRANET_TEACHER_PORTFOLIO_CPD_CATEGORY",
						"param" => "Category",
						"relation" => "MN"
					),
					"mode" => array(
						"table" => "INTRANET_TEACHER_PORTFOLIO_CPD_MODE",
						"param" => "Mode",
						"relation" => "1N"
					),
					"nature" => array(
						"table" => "INTRANET_TEACHER_PORTFOLIO_CPD_NATURE",
						"param" => "Nature",
						"relation" => "1N"
					)
				);
				/****************************************************************************/
				/* data field for generating SQL */
				/****************************************************************************/
				$this->dataField = array(
						"UserID", "AcademicYearID", "ActivityNameChi", "ActivityNameEn", "StartDate", "EndDate",
						"IsDevDay", "CPDNatureID", "ContentChi", "ContentEn", "CPDModeID", "TotalHours", "OrganizerNameChi", "OrganizerNameEn",
						"CertificationNameChi", "CertificationNameEn", "AwardingInstitutionChi", "AwardingInstitutionEn" );
				
				/****************************************************************************/
				/* data mapping for making diffenret fields name on Front end */
				/****************************************************************************/
				$this->dataMapping = array(
					"academicYear" => array( "field" => "AcademicYearID", "required" => true, "type" => "int" ),
					"chiName" => array( "field" => "ActivityNameChi", "required" => true, "type" => "text" ),
					"engName" => array( "field" => "ActivityNameEn", "required" => true, "type" => "text" ),
					"StartDate" => array( "field" => "StartDate", "required" => true, "type" => "date" ),
					"EndDate" => array( "field" => "EndDate", "required" => false, "type" => "date" ),
					"isTPDD" => array( "field" => "IsDevDay", "required" => false, "type" => "int" ),
					"nature" => array( "field" => "CPDNatureID", "required" => true, "type" => "int" ),
					"chiContent" => array( "field" => "ContentChi", "required" => false, "type" => "text" ),
					"engContent" => array( "field" => "ContentEn", "required" => false, "type" => "text" ),
					"mode" => array( "field" => "CPDModeID", "required" => true, "type" => "int" ),
					"hour" => array( "field" => "TotalHours", "required" => false, "type" => "float" ),
					"chiOrganizer" => array( "field" => "OrganizerNameChi", "required" => true, "type" => "text" ),
					"engOrganizer" => array( "field" => "OrganizerNameEn", "required" => true, "type" => "text" ),
					"chiCert" => array( "field" => "CertificationNameChi", "required" => false, "type" => "text" ),
					"engCert" => array( "field" => "CertificationNameEn", "required" => false, "type" => "text" ),
					"chiInstitution" => array( "field" => "AwardingInstitutionChi", "required" => false, "type" => "text" ),
					"engInstitution" => array( "field" => "AwardingInstitutionEn", "required" => false, "type" => "text" ),
					"Category" =>  array( "field" => "CPDCategoryID", "required" => true, "type" => "intArray" ),
				);
			}
			
			public function getOptions($args = array(), $searchByPrimaryKey = null)  {
				$optionsArr = array();
				if (count($args) > 0) {
					foreach ($args as $kk => $RECORD) {
						if (isset($this->optionsInfo[$RECORD])) {
							$sqlParam = array(
								"param" => "CPD" . $this->optionsInfo[$RECORD]["param"] . "ID, CPD" . $this->optionsInfo[$RECORD]["param"] . "NameEn, CPD" . $this->optionsInfo[$RECORD]["param"] . "NameChi",
								"table" => $this->optionsInfo[$RECORD]["table"],
								"condition" => !empty($searchByPrimaryKey) ? "CPD" . $this->optionsInfo[$RECORD]["param"] . "ID='" . $searchByPrimaryKey . "'" : null,
								"order" => "priority ASC, CPD" . $this->optionsInfo[$RECORD]["param"]. "NameEn ASC",
								"limit" => null
							);
							if ($sqlParam["condition"] != null) {
								$sqlParam["condition"] .= " AND isDeleted='N'";
							} else {
								$sqlParam["condition"] = "isDeleted='N'";
							}
							$strSQL = $this->buildSelectSQL($sqlParam);
							if (!empty($strSQL)) {
								$result = $this->returnDBResultSet($strSQL);
								$optionsArrp[$RECORD . "Arr"] = $this->convertArrKeyToID($result, "CPD" . $this->optionsInfo[$RECORD]["param"] . "ID");
							}
						}
					}
				}
				$this->optionsArr =  $optionsArrp;
				return $optionsArrp;
			}
			
			public function getCategory($CPDCategoryID = "") {
				$type = "cateory";
				return $this->getItem($CPDNatureID, $type);
			}
			
			public function getMode($CPDModeID = "") {
				$type = "mode";
				return $this->getItem($CPDNatureID, $type);
			}
			
			public function getNature($CPDNatureID = "") {
				$type = "nature";
				return $this->getItem($CPDNatureID, $type);
			}
			
			public function getItem($CPDOptionID, $type, $withDelete = false) {
				$resultArr = array();
				switch (strtolower($type)) {
					case "category":
					case "mode":
					case "nature":
						$typeLabel = ucfirst(strtolower($type));
						$tablename = "INTRANET_TEACHER_PORTFOLIO_CPD_" . strtoupper($type);
						$sqlParam = array(
							"param" => "CPD" . $typeLabel . "ID, CPD" . $typeLabel . "NameEn, CPD" . $typeLabel . "NameChi, isDeleted, priority",
							"table" => $tablename,
							"condition" => !empty($CPDOptionID) ? "CPD" . $typeLabel . "ID='" . $this->Get_Safe_Sql_Query($CPDOptionID) . "'" : null . " AND isDeleted='N'",
							"order" => "priority ASC, CPD" . $typeLabel . "NameEn ASC",
							"limit" => null
						);
						$strSQL = $this->buildSelectSQL($sqlParam);
						if (!empty($strSQL)) {
							$result = $this->returnDBResultSet($strSQL);
							$resultArr = $this->convertArrKeyToID($result, "CPD" . $typeLabel . "ID");
						}
						break;
					default:
						return false;
						break;
				}
				return $resultArr;
			}
			
			public function dataHandler($userInfo, $data, $action, $id) {
				$output = array(
					"result" => "fail",
					"data" => "Input Error! Please check"
				);
				$isError = false;
				
				if ($data["hasError"] == "N") {
					$data["dataInfo"]["UserID"] = array( "field" => "UserID", "data" => $userInfo["info"]["SelectedUserID"]);
					$cpdParam = $this->dataField;
					$cpdParam[] = "DateModified";
					$cpdParam[] = "ModifyBy";
					$data["dataInfo"]["DateModified"] = array( "field" => "DateInput", "data" => "__dbnow__");
					$data["dataInfo"]["ModifyBy"] = array( "field" => "InputBy", "data" => $userInfo["info"]["UserID"]);
					switch ($action) {
						case "INSERT":
							/********* extra field for Insert ***********/
							$cpdParam[] = "DateInput";
							$cpdParam[] = "InputBy";
							$data["dataInfo"]["InputBy"] = array( "field" => "InputBy", "data" => $userInfo["info"]["UserID"]);
							$data["dataInfo"]["DateInput"] = array( "field" => "DateInput", "data" => "__dbnow__");
							/********* extra field for Insert ***********/
							
							$strSQL = $this->buildInsertSQL($this->mainTable, $cpdParam, $data["dataInfo"]);
							$result = $this->DBdb_query($strSQL);
							if (!$result) {
								$isError = true;
							} else {
								$id = $this->db_insert_id();
							}
							$msg = $this->core->data["lang"]["TeacherPortfolio"]["InsertSuccess"];
							break;
						case "UPDATE":
							$condition = $this->primaryKey . "='" . $id . "' AND UserID='" . $userInfo["info"]["SelectedUserID"] . "'";
							$strSQL = $this->buildUpdateSQL($this->mainTable, $cpdParam, $data["dataInfo"], $condition);
							$result = $this->DBdb_query($strSQL);
							if (!$result) {
								$isError = true;
							}
							$msg = $this->core->data["lang"]["TeacherPortfolio"]["UpdateSuccess"];
							break;
					}
					if (!$isError && $id > 0 && count($data["dataInfo"]["CPDCategoryID"]["data"]) > 0) {
						$strSQL = "DELETE FROM " . $this->optionsInfo["category"]["table"] . "_RELATION WHERE " . $this->primaryKey .  "='" . $id . "'";
						$result = $this->DBdb_query($strSQL);
						foreach($data["dataInfo"]["CPDCategoryID"]["data"] as $kk => $vv) {
							$cpdParam = array( "CpdID", "CPDCategoryID" );
							$related_data = array(
								"CpdID" => array( "data" => $id ),
								"CPDCategoryID" => array( "data" => $vv )
							);
							$strSQL = $this->buildInsertSQL($this->optionsInfo["category"]["table"] . "_RELATION", $cpdParam, $related_data);
							$result = $this->DBdb_query($strSQL);
						}
					}
					if (!$isError) {
						$output = array(
							"result" => "success",
							"data" => array( "id" => $id, "msg" => $msg )
						);
					}
				}
				return $output;
			}
			
			public function getRecordByID($id) {
				$result = false;
				if ($id > 0) {
					$strSQL = "SELECT * FROM " . $this->mainTable;
					$strSQL .= " WHERE " . $this->primaryKey . "='" . $this->Get_Safe_Sql_Query($id) . "' limit 1";
					$result = $this->returnDBResultSet($strSQL);
					if (count($result) == 1) {
						$result = $result[0];
						$category = $this->getCateRelation($result["CpdID"]);
						if (count($category) > 0) {
							foreach ($category as $ky => $kv) {
								$result["category"][] = $kv["CPDCategoryID"];
							}
						}
					}
				}
				return $result;
			}
			
			public function getCateRelation($argsID) {
				$result = false;
				if (!empty($argsID)) {
					$strSQL = "SELECT CpdID, CPDCategoryID FROM " . $this->relationCategoryTable;
					if (is_array($argsID) && count($argsID) > 0) {
						$strSQL .= " WHERE CpdID in (" . implode(", ", $argsID) . ")";
					} else {
						$strSQL .= " WHERE CpdID='" . $this->Get_Safe_Sql_Query($argsID) . "'";
					}
					$strSQL .= " ORDER BY CpdID ASC, CPDCategoryID ASC ";
					$result = $this->returnDBResultSet($strSQL);
				}
				return $result;
			}
			
			public function getDataTableArray($UserID=0, $postData = array()) {
				$data = array();
				$jsonData = array(
					"recordsTotal" => 0,
					"recordsFiltered" => 0,
					"data" => $data
				);
				$recordsTotal = 0;
				$recordsFiltered = 0;
				if ($UserID > 0 && isset($postData["columns"]) && count($postData["columns"]) > 0) {
					$main_prefix = "mt.";
					$cpdParam = $this->dataField;
					$strParam = $main_prefix . "" . $this->primaryKey . ", EnglishName, ChineseName, " . $main_prefix . implode(", mt.", $cpdParam);
					$strTable = $this->mainTable . " AS mt";
					$strTable .= " JOIN " . $this->userTable . " AS ut ON (mt.UserID=ut.UserID AND ut.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "') "; 
					$strCondition = " WHERE mt.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "'";
					if (!empty($postData["selyear"]) && $postData["selyear"] != "all") {
						$selectedYear = $postData["selyear"];
						$strCondition .= " AND " . $main_prefix . "AcademicYearID='" . $this->Get_Safe_Sql_Query($postData["selyear"]) . "'";
					} else {
						$selectedYear = 'all';
					}
					
					$strTable .= ", " . $this->core->data["yearTable"] . " AS yeartbl";
					$strParam .= ", yeartbl.YearNameEN, yeartbl.YearNameB5";
					$strCondition .= " AND " . $main_prefix . "AcademicYearID=yeartbl.AcademicYearID ";

					foreach ($this->optionsInfo as $reKey => $reVal) {
						if ($reVal["relation"] == "1N") {
							$strTable .= ", " . $reVal["table"] . " AS rel_" . $reVal["param"];
							$strParam .= ", rel_" . $reVal["param"] . ".CPD" . $reVal["param"] . "NameEn, rel_" . $reVal["param"] . ".CPD" . $reVal["param"] . "NameChi";
							$strCondition .= " AND ".$main_prefix."CPD" . $reVal["param"] . "ID=rel_" . $reVal["param"] . ".CPD" . $reVal["param"] . "ID";
						}
					}
					$strSQL = "SELECT COUNT(" . $main_prefix . $this->primaryKey . ") as totalrecord" ;
					$strSQL .= " FROM " . $strTable;
					$strSQL .= $strCondition;
					/************************************************************************/
					$totalRec = $this->returnDBResultSet($strSQL);
					if ($totalRec !=  NULL && count($totalRec) > 0) {
						$recordsTotal = isset($totalRec[0]["totalrecord"]) ? $totalRec[0]["totalrecord"] : 0;
						$recordsFiltered = isset($totalRec[0]["totalrecord"]) ? $totalRec[0]["totalrecord"] : 0;
						/************************************************************************/
						$strSQL = "SELECT " . $main_prefix . $this->primaryKey . ", EnglishName, ChineseName, " . $strParam;
						$strSQL .= " FROM " . $strTable;
						$strSQL .= $strCondition;
						/************************************************************************/
						if ($postData["columns"][$postData["order"][0]["column"]]["data"] == "TeacherName") {
							$order_field = "TeacherName";
						} else {
							switch ($postData["order"][0]["column"]) {
								case "ActivityName" :
									$map_field = Get_Lang_Selection("ActivityNameChi", "ActivityNameEn");
									break;
								default:
									$map_field = $postData["order"][0]["column"];
									break;
							}
							$order_field = $this->dataMapping[$postData["columns"][$postData["order"][0]["column"]]["data"]]["field"];
							if (empty($order_field)) {
								$order_field = $postData["columns"][$postData["order"][0]["column"]]["data"];
							}
						}
						switch ($order_field) {
							case "TeacherName" :
								$order_field = "mt.UserID";
								break;
							case "AcademicYearID" :
								$order_field = "yeartbl.Sequence";
								break;
							case "Nature" :
								$order_field = Get_Lang_Selection("CPDNatureNameChi", "CPDNatureNameEn");
								break;
							case "ActivityName" :
								$order_field = Get_Lang_Selection("ActivityNameChi", "ActivityNameEn");
								break;
						}
						$order_dir = "DESC";
						if (isset($postData["order"][0]["dir"])) {
							switch (strtoupper($postData["order"][0]["dir"])) {
								case "ASC":
									$order_dir = strtoupper($postData["order"][0]["dir"]);
									break;
							}
						}
						$strOrder = " ORDER BY " . $order_field . " " . $order_dir;
						$strSQL .= $strOrder;
						/************************************************************************/
						$start = $postData["start"] ? $postData["start"] : 0;
						$pagelimit = $postData["length"] > 0 ? $postData["length"] : 0;
						$strLimit = " LIMIT " . $start . ", " . $pagelimit;
						if ($pagelimit > 0) $strSQL .= $strLimit;
						/************************************************************************/
						$queryData = $this->returnDBResultSet($strSQL);
						if ($queryData !=  NULL) {
							$queryData = $this->convertArrKeyToID($queryData, $this->primaryKey);
							foreach ($queryData aS $pkey => $RECORD) {
								$data[$pkey] = $RECORD;
								foreach ($postData["columns"] as $kk => $vv) {
									switch ($vv["data"]) {
										case "func":
											$record_data = "<div class='text-center'>";
											if ($this->core->isOwner()) {
												$record_data .= '<a href="#" class="btn-del" rel="' . $this->core->data["section"] . '"><i class="fa fa-trash"></i></a>';
												$record_data .= '<a href="#" class="btn-edit" rel="' . $this->core->data["section"] . '"><i class="fa fa-pencil"></i></a>';
											}
											$record_data .= '<a href="#" class="btn-detail"><i class="fa fa-caret-up"></i><i class="fa fa-caret-down"></i></a></div>';
											break;
										case "academicYear":
											$record_data_chi = "-";
											$record_data_eng = "-";
											if (isset($RECORD["YearNameB5"])) $record_data_chi = $RECORD["YearNameB5"];
											if (isset($RECORD["YearNameEN"])) $record_data_eng = $RECORD["YearNameEN"];
											$record_data = Get_Lang_Selection($record_data_chi, $record_data_eng);
											break;
										case "TeacherName":
											$record_data_chi = "-";
											$record_data_eng = "-";
											if (isset($RECORD["ChineseName"])) $record_data_chi = $RECORD["ChineseName"];
											if (isset($RECORD["EnglishName"])) $record_data_eng = $RECORD["EnglishName"];
											$record_data = Get_Lang_Selection($record_data_chi, $record_data_eng);
											$data[$pkey]["TeacherName"] = $record_data;
											break;
										case "ActivityName":
											$record_data_chi = "-";
											$record_data_eng = "-";
											if (isset($RECORD["ActivityNameChi"])) $record_data_chi = $RECORD["ActivityNameChi"];
											if (isset($RECORD["ActivityNameEn"])) $record_data_eng = $RECORD["ActivityNameEn"];
											$record_data = Get_Lang_Selection($record_data_chi, $record_data_eng);
											$data[$pkey]["listTitle"] = $record_data;
											break;
										case "Nature":
											$record_data_chi = "-";
											$record_data_eng = "-";
											if (isset($RECORD["CPDNatureNameChi"])) $record_data_chi = $RECORD["CPDNatureNameChi"];
											if (isset($RECORD["CPDNatureNameEn"])) $record_data_eng = $RECORD["CPDNatureNameEn"];
											$record_data = Get_Lang_Selection($record_data_chi, $record_data_eng);
											break;
										default:
											if (isset($this->dataMapping[$vv["data"]]["field"])) {
												$org_name = $this->dataMapping[$vv["data"]]["field"];
												switch ($org_name) {
													case "IsDevDay":
														if ($RECORD[$org_name] == 1) {
															$record_data ='<div class="text-center"><i class="fa fa-check text-warning"></i></div>';
														} else {
															$record_data ='<div class="text-center"><i class="fa fa-close text-warning"></i></div>';
														}
														break;
													default:
														$record_data = $RECORD[$org_name];
														break;
												}
											} else {
												$record_data = "";
											}
											break;
									}
									$data[$pkey][$vv["data"]] = $record_data;
								}
								if (empty($data[$pkey]["EndDate"])) $data[$pkey]["EndDate"] = '-';
								if (!isset($data[$pkey]["Content"])) $data[$pkey]["Content"] = nl2br(Get_Lang_Selection($data[$pkey]["ContentChi"], $data[$pkey]["ContentEn"]));
								if (!isset($data[$pkey]["CPDModeName"])) $data[$pkey]["CPDModeName"] = Get_Lang_Selection($data[$pkey]["CPDModeNameChi"], $data[$pkey]["CPDModeNameEn"]);
								if (!isset($data[$pkey]["ActivityName"])) $data[$pkey]["ActivityName"] = Get_Lang_Selection($data[$pkey]["ActivityNameChi"], $data[$pkey]["ActivityNameEn"]);
								if (!isset($data[$pkey]["CertificationName"])) $data[$pkey]["CertificationName"] = Get_Lang_Selection($data[$pkey]["CertificationNameChi"], $data[$pkey]["CertificationNameEn"]);
								if (!isset($data[$pkey]["OrganizerName"])) $data[$pkey]["OrganizerName"] = Get_Lang_Selection($data[$pkey]["OrganizerNameChi"], $data[$pkey]["OrganizerNameEn"]);
								if (!isset($data[$pkey]["AwardingInstitution"])) $data[$pkey]["AwardingInstitution"] = Get_Lang_Selection($data[$pkey]["AwardingInstitutionChi"], $data[$pkey]["AwardingInstitutionEn"]);
							}
							
							/***************************************************************/
							if (count($data) > 0) {
								$allCPDID = array_keys($data);
								$result = $this->getCateRelation($allCPDID);
								if (count($result) > 0) {
									foreach ($result as $kk => $vv) {
										if (isset($data[$vv["CpdID"]])) {
											$record_chi = $this->optionsArr["categoryArr"][$vv["CPDCategoryID"]]["CPDCategoryNameChi"];
											$record_eng = $this->optionsArr["categoryArr"][$vv["CPDCategoryID"]]["CPDCategoryNameEn"];
											if (!isset($data[$vv["CpdID"]]["Category"])) $data[$vv["CpdID"]]["Category"] = "";
											if (!empty($data[$vv["CpdID"]]["Category"])) {
												$data[$vv["CpdID"]]["Category"] .= "<br>";
											}
											$data[$vv["CpdID"]]["Category"] .= Get_Lang_Selection($record_chi, $record_eng);
										}
									}
								}
							}
							/***************************************************************/
						}
					}
				}
				$jsonData = array(
					"recordsTotal" => $recordsTotal,
					"recordsFiltered" => $recordsFiltered,
					"allowExpand" => true,
					"acdyear" => $selectedYear,
					"data" => array_values($data)
				);
				return $jsonData;
			}
			
			public function deleteRecord($id) {
				if ($id > 0) {
					$strSQL = "DELETE FROM " . $this->mainTable;
					$strSQL .= " WHERE " . $this->primaryKey . "='" . $this->Get_Safe_Sql_Query($id) . "'";
					$result = $this->DBdb_query($strSQL);
					$strSQL = "DELETE FROM " . $this->relationCategoryTable;
					$strSQL .= " WHERE " . $this->primaryKey . "='" . $this->Get_Safe_Sql_Query($id) . "'";
					$result = $this->DBdb_query($strSQL);
					return true;
				}
				return false;
			}
			
			public function exportAllData($CSVObj, $UserID) {
				$rowData = array();
				$main_prefix = "mt.";
				$cpdParam = $this->dataField;
				$strParam = $main_prefix . "" . $this->primaryKey . ", " . $main_prefix . implode(", mt.", $cpdParam) . ", EnglishName, ChineseName";
				$strTable = $this->mainTable . " AS mt";
				$strCondition = " WHERE IU.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "'";
				if (!empty($postData["selyear"]) && $postData["selyear"] != "all") {
					$selectedYear = $postData["selyear"];
				} else {
					$selectedYear = 'all';
				}
				$strParam .= ", yeartbl.YearNameEN, yeartbl.YearNameB5";
				// $strCondition .= " AND " . $main_prefix . "AcademicYearID=yeartbl.AcademicYearID ";
				$joinTable = " JOIN " . $this->core->data["yearTable"] . " AS yeartbl ON (" . $main_prefix . "AcademicYearID=yeartbl.AcademicYearID )";
				$joinTable .= " JOIN " . $this->userTable . " AS IU ON (" . $main_prefix . "UserID=IU.UserID )";
				foreach ($this->optionsInfo as $reKey => $reVal) {
					if ($reVal["relation"] == "1N") {
						// $strTable .= ", " . $reVal["table"] . " AS rel_" . $reVal["param"];
						$strParam .= ", rel_" . $reVal["param"] . ".CPD" . $reVal["param"] . "NameEn, rel_" . $reVal["param"] . ".CPD" . $reVal["param"] . "NameChi";
						$joinTable .= " JOIN " . $reVal["table"] . " AS rel_" . $reVal["param"] . " ON (" . $main_prefix . "CPD" . $reVal["param"] . "ID=rel_" . $reVal["param"] . ".CPD" . $reVal["param"] . "ID )";
					}
				}
				$strSQL = "SELECT " . $main_prefix . $this->primaryKey . ", " . $strParam . ", combinedCategory, numOfCategory";
				$strSQL .= " FROM " . $strTable;
				$strSQL .= $joinTable;
				$strSQL .= " JOIN (SELECT s.CpdID, GROUP_CONCAT(s.CPDCategoryID) AS combinedCategory, COUNT(CPDCategoryID) as numOfCategory FROM " . $this->relationCategoryTable . " AS s GROUP BY s.CpdID) AS X ON (X.CpdID=" . $main_prefix . "CpdID )";
				$strSQL .= $strCondition;
				$queryData = $this->returnDBResultSet($strSQL);
				if (count($queryData) > 0) {
					$rowData[0]= array(
						$this->core->data["lang"]["TeacherPortfolio"]["TeacherName"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["TeacherName"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["SchoolYear"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["SchoolYear"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["StartDate"],
						$this->core->data["lang"]["TeacherPortfolio"]["EndDate"],
						$this->core->data["lang"]["TeacherPortfolio"]["TPDD"],
						$this->core->data["lang"]["TeacherPortfolio"]["Nature"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["Nature"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["Content"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["Content"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["Mode"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["Mode"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["Hour"],
						$this->core->data["lang"]["TeacherPortfolio"]["Organizer"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["Organizer"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["CertificateName"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["CertificateName"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["AwardingInstitution"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["AwardingInstitution"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")"
						/*$this->core->data["lang"]["TeacherPortfolio"]["Category"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")",
						$this->core->data["lang"]["TeacherPortfolio"]["Category"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")"*/
					);
					foreach ($this->optionsArr["categoryArr"] as $catekk => $catevv) {
						$rowData[0][] = Get_Lang_Selection($catevv["CPDCategoryNameChi"], $catevv["CPDCategoryNameEn"]);
					}
					$i = 1;
					foreach ($queryData as $kk => $val) {
						$category = explode(",", $val["combinedCategory"]);
						$strCategoryChi = "";
						$strCategoryEn = "";
						if (count($category) > 0) {
							foreach ($category as $catekk => $catevv) {
								if (!empty($strCategoryChi)) $strCategoryChi .= "\n";
								if (!empty($strCategoryEn)) $strCategoryEn .= "\n";
								$strCategoryChi .= $this->optionsArr["categoryArr"][$catevv]["CPDCategoryNameChi"];
								$strCategoryEn .= $this->optionsArr["categoryArr"][$catevv]["CPDCategoryNameEn"];
							}
						}
						$rowData[$i] = array(
							$val["ChineseName"],
							$val["EnglishName"],
							$val["YearNameB5"],
							$val["YearNameEN"],
							$val["StartDate"],
							$val["EndDate"],
							($val["IsDevDay"]) ? "Y":"N",
							$val["CPDNatureNameChi"],
							$val["CPDNatureNameEn"],
							$val["ContentChi"],
							$val["ContentEn"],
							$val["CPDModeNameChi"],
							$val["CPDModeNameEn"],
							$val["TotalHours"],
							$val["OrganizerNameChi"],
							$val["OrganizerNameEn"],
							$val["CertificationNameChi"],
							$val["CertificationNameEn"],
							$val["AwardingInstitutionChi"],
							$val["AwardingInstitutionEn"]
							/*$strCategoryChi,
							$strCategoryEn*/
						);
						foreach ($this->optionsArr["categoryArr"] as $catekk => $catevv) {
							if (in_array($catekk, $category)) {
								$rowData[$i][] = "Y";
							} else {
								$rowData[$i][] = "N";
							}
						}
						$i++;
					}
					if (count($rowData) > 0) {
						$CSVObj->outputData($rowData, strtolower($this->core->data["section"]));
						exit;
					}
				}
				header('Content-type: text/plain; charset=utf-8');
				echo "File not found";
				exit;
			}
			
			public function updateSortRecords($userInfo, $ids, $otype) {
				if (isset($this->optionsInfo[$otype])) {
					$optInfo = $this->optionsInfo[$otype];
					$idArr = explode(",", $ids);
					if (count($idArr) > 0) {
						$i = 1;
						foreach ($idArr as $kk => $vv) {
							$typeLabel = ucfirst(strtolower($otype));
							$tablename = "INTRANET_TEACHER_PORTFOLIO_CPD_" . strtoupper($otype);
							$strSQL = "UPDATE " . $tablename . " SET priority='" . $i . "', DateModified=NOW(), ModifyBy='" . $userInfo["info"]["UserID"] . "' WHERE CPD" . $typeLabel . "ID='" . $vv . "' AND isDeleted='N'";
							$result = $this->DBdb_query($strSQL);
							$i++;
						}
						return true;
					}
				}
				return false;
			}
			
			public function optionsManage($userInfo, $postData) {
				$param = array();
				$param["DateModified"] = array( "field" => "DateInput", "data" => "__dbnow__");
				$param["ModifyBy"] = array( "field" => "InputBy", "data" => $userInfo["info"]["UserID"]);
				$existData = array();
				if (isset($postData["action"]) && $postData["action"] == "delete") {
					$type = "delete";
				} else {
					if (isset($postData["recid"]) && $postData["recid"] > 0) {
						$type = "edit";
					} else {
						$type = "add";
					}
				}
				$label = "CPD" . ucfirst(strtolower($postData["otype"]));
				$param[$label . "NameEn"] = array( "field" => $label . "NameEn", "data" => $this->Get_Safe_Sql_Query($postData["nameEN"]) );
				$param[$label . "NameChi"] = array( "field" => $label . "NameChi", "data" => $this->Get_Safe_Sql_Query($postData["nameCN"]) );
				$condition = $label . 'ID="' . $postData["recid"] . '"';
				
				if ($type != "add") {
					$existData = $this->getItem($postData["recid"], $postData["otype"], true);
					if (isset($existData[$postData["recid"]][$label . "ID"])) {
						if ($type == "delete") {
							unset($param[$label . "NameEn"]);
							unset($param[$label . "NameChi"]);
							$param["isDeleted"] = array( "field" => "isDeleted", "data" => "Y" );
						}
						$strSQL = $this->buildUpdateSQL($this->optionsInfo[$postData["otype"]]["table"], array_keys($param), $param, $condition);
					}
				} else {
					$param["priority"] = array( "field" => "DateInput", "data" => "0");
					$strSQL = "SELECT " . $label . "ID FROM " . $this->optionsInfo[$postData["otype"]]["table"] . " WHERE isDeleted='Y' AND " . $label . "NameEn" . "='" . $this->Get_Safe_Sql_Query($postData["nameEN"]) . "' AND " . $label . "NameChi='" . $this->Get_Safe_Sql_Query($postData["nameCN"]) . "'";
					
					$result = $this->returnDBResultSet($strSQL);
					if ($result !=  NULL && count($result) > 0) {
						$param["isDeleted"] = array( "field" => "isDeleted", "data" => "N" );
						$condition = $label . 'ID="' . $result[0][$label . "ID"] . '"';
						$strSQL = $this->buildUpdateSQL($this->optionsInfo[$postData["otype"]]["table"], array_keys($param), $param, $condition);
					} else {
						$param["DateInput"] = array( "field" => "DateInput", "data" => "__dbnow__");
						$param["InputBy"] = array( "field" => "InputBy", "data" => $userInfo["info"]["UserID"]);
						$strSQL = $this->buildInsertSQL($this->optionsInfo[$postData["otype"]]["table"], array_keys($param), $param, $condition);
					}
				}

				$isError = false;
				$result = false;
				if (!empty($strSQL)) {
					$result = $this->DBdb_query($strSQL);
					if (!$result) {
						$isError = true;
					} else {
						if ($type == "add") {
							$id = $this->db_insert_id();
						} else {
							$id = $postData["recid"];
						}
						$succData = array(
							"recid" => $id,
							"nameEN" => $postData["nameEN"],
							"nameCN" => $postData["nameCN"],
							"sec" => $postData["otype"],
							"status" => $type,
							"msg" => $this->core->data["lang"]["TeacherPortfolio"]["UpdateSuccess"]
						);
					}
				}
				if (!$isError) {
					$output = array(
						"result" => "success",
						"data" => $succData
					);
					return $output;
				}
				return false;
			}
		}
	}
}