<?php
/*
 * 2019-02-22 Paul
 * 		- modify to handle the issue of missing ordering parameters
 * 
 * 2018-12-10 Cameron
 *      - modify getChirldByTplID() to filter out customized fields if customized flag is not set
 *        
 * 2018-11-12 Paul
 * 		- fix the issue of not showing alert details and insufficient checking 
 * 
 * 2018-08-13 Henry
 * 		- fixed sql injection
 * 
 * 2018-01-15 Paul
 * 		- add dynamic approach for the tv display	
 * 
 * 2017-11-09 Cameron
 * 		- add Edit button (pencil) for cpd in getGridDataTableArray()
 */
if (defined('TEACHER_PORTFOLIO')) {
	if (!class_exists("CommonLibdb")) include_once(dirname(__FILE__) . "/commonlibdb.model.php");
	if (!class_exists("FormTemplateObj")) {
		class FormTemplateObj extends CommonLibdb {
			public function __construct() {
				parent::__construct();
				$this->mainTable = "INTRANET_TEACHER_PORTFOLIO_FORM_TEMPLATE";
				$this->userTable = $this->defaultUserTable;
				$this->mainParam = array( "FormTemplateID", "TemplateName" );
				$this->mainPriKey = "FormTemplateID";
				$this->fieldTable = "INTRANET_TEACHER_PORTFOLIO_FORM_TEMPLATE_TV";
				$this->relationTable = "INTRANET_TEACHER_PORTFOLIO_FORM_TEMPLATE_RELATION";
				$this->dataGridTable = "INTRANET_TEACHER_PORTFOLIO_FORM_DATA_GRID";
				$this->dataTable = "INTRANET_TEACHER_PORTFOLIO_FORM_DATA";
				$this->yearTable = "ACADEMIC_YEAR";
				$this->yearTermTable = "ACADEMIC_YEAR_TERM";
				
				$this->fieldStatusOption = array(
					"required" => "isRequired",
					"reference" => "isReferenceOnly",
					"reference" => "normal"
				);
			}
			
			public function setCoreData($data) {
				$this->coreData = $data;
				if (isset($this->coreData["TemplateType"]) && $this->coreData["TemplateType"] == "GRID") {
					$this->setDataGridTable($this->coreData["TemplateGridTable"]);
				}
			}
			
			private function setDataGridTable($tableName) {
				$this->dataGridTable = $tableName;
			}
			
			public function initTemplate($templateName, $predefined = array()) {
				$this->coreData["formTplInfo"] = $this->getTempInfoByName($templateName);
				
				if ($this->coreData["formTplInfo"] == NULL) {
					$data = array();
					$data["TemplateName"] = array( "field" => "TemplateName", "data" => strtoupper($templateName));
					$data["DateInput"] = array( "field" => "DateInput", "data" => "__dbnow__");
					$data["InputBy"] = array( "field" => "InputBy", "data" => $this->coreData["userInfo"]["info"]["UserID"]);
					$data["DateModified"] = array( "field" => "DateModified", "data" => "__dbnow__");
					$data["ModifyBy"] = array( "field" => "ModifyBy", "data" => $this->coreData["userInfo"]["info"]["UserID"]);
					$strSQL = $this->buildInsertSQL($this->mainTable, array_keys($data), $data);
					$result = $this->DBdb_query($strSQL);
					if (!$result) {
						return FALSE;
					} else {
						$templateID = $this->db_insert_id();
						if (count($predefined) > 0) {
							foreach ($predefined as $kk => $val) {
								$strSQL = "SELECT FieldID FROM " . $this->fieldTable . " WHERE FieldLabel='" . $kk . "' LIMIT 1";
								$result = $this->returnDBResultSet($strSQL);
								if (count($result) > 0) {
									$fieldID = $result[0]["FieldID"];
								} else {
									$data = array();
									$tv_array = array( "FieldLabel", "FieldNameChi", "FieldNameEn", "isMultiLang", "SampleDataChi", "SampleData", "FieldType" );
									// $data = $val;
									foreach ($tv_array as $_field) {
										$data[$_field] = array( "field" => $_field, "data" => $val[$_field]);
									}
									$data["DateInput"] = array( "field" => "DateInput", "data" => "__dbnow__");
									$data["InputBy"] = array( "field" => "InputBy", "data" => $this->coreData["userInfo"]["info"]["UserID"]);
									$data["DateModified"] = array( "field" => "DateModified", "data" => "__dbnow__");
									$data["ModifyBy"] = array( "field" => "ModifyBy", "data" => $this->coreData["userInfo"]["info"]["UserID"]);
									$strSQL = $this->buildInsertSQL($this->fieldTable, array_keys($data), $data);
									$result = $this->DBdb_query($strSQL);
									if (!$result) {
										$fieldID = 0;
									} else {
										$fieldID = $this->db_insert_id();
									}
								}
								if ($fieldID > 0) {
									$strSQL = "SELECT RelationID FROM " . $this->relationTable . " WHERE FormTemplateID='" . $templateID . "' AND FieldID='" . $fieldID . "'";
									$result = $this->returnDBResultSet($strSQL);
									if (!(count($result) > 0)) {
										$data = array();
										$data["FormTemplateID"] = array( "field" => "FormTemplateID", "data" => $templateID . "");
										$data["FieldID"] = array( "field" => "FormTemplateID", "data" => $fieldID);
										$tv_array = array( "FixedItem", "FieldStatus", "showInGrid", "isUsed", "Priority" );
										foreach ($tv_array as $_field) {
											$data[$_field] = array( "field" => $_field, "data" => $val[$_field]);
										}
										$data["DateInput"] = array( "field" => "DateInput", "data" => "__dbnow__");
										$data["InputBy"] = array( "field" => "InputBy", "data" => $this->coreData["userInfo"]["info"]["UserID"]);
										$data["DateModified"] = array( "field" => "DateModified", "data" => "__dbnow__");
										$data["ModifyBy"] = array( "field" => "ModifyBy", "data" => $this->coreData["userInfo"]["info"]["UserID"]);
										$strSQL = $this->buildInsertSQL($this->relationTable, array_keys($data), $data);
										$result = $this->DBdb_query($strSQL);
									}
								}
							}
						}
						$this->coreData["formTplInfo"] = $this->getTempInfoByName($templateName);
						return $this->coreData["formTplInfo"];
					}
				} else {
					$newHeader = array_diff_key($predefined, $this->coreData["formTplInfo"]["childs"]);
					if(!empty($newHeader)){ // new fields have not entered the DB
						$templateID = $this->coreData["formTplInfo"]["FormTemplateID"];
						foreach($newHeader as $kk=>$val){
							$strSQL = "SELECT FieldID FROM " . $this->fieldTable . " WHERE FieldLabel='" . $kk . "' LIMIT 1";
							$result = $this->returnDBResultSet($strSQL);
							if (count($result) > 0) {
								$fieldID = $result[0]["FieldID"];
							} else {
								$data = array();
								$tv_array = array( "FieldLabel", "FieldNameChi", "FieldNameEn", "isMultiLang", "SampleDataChi", "SampleData", "FieldType" );
								// $data = $val;
								foreach ($tv_array as $_field) {
									$data[$_field] = array( "field" => $_field, "data" => $val[$_field]);
								}
								$data["DateInput"] = array( "field" => "DateInput", "data" => "__dbnow__");
								$data["InputBy"] = array( "field" => "InputBy", "data" => $this->coreData["userInfo"]["info"]["UserID"]);
								$data["DateModified"] = array( "field" => "DateModified", "data" => "__dbnow__");
								$data["ModifyBy"] = array( "field" => "ModifyBy", "data" => $this->coreData["userInfo"]["info"]["UserID"]);
								$strSQL = $this->buildInsertSQL($this->fieldTable, array_keys($data), $data);
								$result = $this->DBdb_query($strSQL);
								if (!$result) {
									$fieldID = 0;
								} else {
									$fieldID = $this->db_insert_id();
								}
							}
							if ($fieldID > 0) {
								$strSQL = "SELECT RelationID FROM " . $this->relationTable . " WHERE FormTemplateID='" . $templateID . "' AND FieldID='" . $fieldID . "'";
								$result = $this->returnDBResultSet($strSQL);
								if (!(count($result) > 0)) {
									$data = array();
									$data["FormTemplateID"] = array( "field" => "FormTemplateID", "data" => $templateID . "");
									$data["FieldID"] = array( "field" => "FormTemplateID", "data" => $fieldID);
									$tv_array = array( "FixedItem", "FieldStatus", "showInGrid", "isUsed", "Priority" );
									foreach ($tv_array as $_field) {
										$data[$_field] = array( "field" => $_field, "data" => $val[$_field]);
									}
									$data["DateInput"] = array( "field" => "DateInput", "data" => "__dbnow__");
									$data["InputBy"] = array( "field" => "InputBy", "data" => $this->coreData["userInfo"]["info"]["UserID"]);
									$data["DateModified"] = array( "field" => "DateModified", "data" => "__dbnow__");
									$data["ModifyBy"] = array( "field" => "ModifyBy", "data" => $this->coreData["userInfo"]["info"]["UserID"]);
									$strSQL = $this->buildInsertSQL($this->relationTable, array_keys($data), $data);
									$result = $this->DBdb_query($strSQL);
								}
							}
						}
						$this->coreData["formTplInfo"] = $this->getTempInfoByName($templateName);
					}else{
						$pickedHeader = array_diff_key($this->coreData["formTplInfo"]["childs"],$predefined);
						if(!empty($pickedHeader)){ // Hide some customized fields if not set to be displayed
							foreach($this->coreData["formTplInfo"]["childs"] as $k=>$v){
								if(isset($pickedHeader[$k])){
									unset($this->coreData["formTplInfo"]["childs"][$k]);
								}
							}
						}
					}
					return $this->coreData["formTplInfo"];
				}
				return FALSE;
			}
			
			public function getAcademicYear($formTPLID = "",$checkAcademicYearTable=false,$showAcademicYearPeriod=false) {
				$templateID = $this->coreData["formTplInfo"]["FormTemplateID"];
				if($checkAcademicYearTable == true){
					if($showAcademicYearPeriod==true){
						$strSQL = "SELECT year.AcademicYearID,year.YearNameEN,year.YearNameB5, MIN(term.TermStart) YearStart, MAX(term.TermEnd) YearEnd FROM ".$this->yearTable." year ";
						$strSQL .= " INNER JOIN ".$this->yearTermTable." term ON year.AcademicYearID = term.AcademicYearID ";
						if (!empty($formTPLID)) {
							$strSQL .= " WHERE year.YearNameEN like '%".$formTPLID."%' OR year.YearNameB5 like '%".$formTPLID."%' OR year.AcademicYearID='".$formTPLID."'";
						}
						$strSQL .= " GROUP BY term.AcademicYearID ";
						$strSQL .= " ORDER BY year.Sequence";
					}else{
						$strSQL = "SELECT AcademicYearID,YearNameEN,YearNameB5 FROM ".$this->yearTable;
						if (!empty($formTPLID)) {
							$strSQL .= " WHERE YearNameEN like '%".$formTPLID."%' OR YearNameB5 like '%".$formTPLID."%' OR AcademicYearID='".$formTPLID."'";
						}
						$strSQL .= " ORDER BY Sequence";
					}
					
					$result = $this->returnDBResultSet($strSQL);
					$yearArr = array();
					if (count($result) > 0) {
						foreach ($result as $kk => $vv) {
							$yearArr[$vv["YearNameEN"]] = array();
							foreach($vv as $fieldKey=>$fieldContent){
								$yearArr[$vv["YearNameEN"]][$fieldKey] = $fieldContent;
							}
						}
						return $yearArr;
					}
					return NULL;
				}else{
					if ($this->coreData["TemplateType"] == "GRID") {
						$strSQL = "SELECT AcademicYear FROM " . $this->dataGridTable;
						if (!empty($formTPLID)) {
							$strSQL .= " WHERE AcademicYear like '" . $formTPLID . "'";
						}
						$strSQL .= " GROUP BY AcademicYear ORDER BY AcademicYear DESC";
						$result = $this->returnDBResultSet($strSQL);
						
						if (count($result) > 0) {
							foreach ($result as $kk => $vv) {
								$yearArr[$vv["AcademicYear"]] = array(
									"FTDataRowID" => $vv["AcademicYear"],
									"YearNameEN" => $vv["AcademicYear"],	
									"YearNameB5" => $vv["AcademicYear"]
								);
							}
							return $yearArr;
						}
					} else {
						$strSQL = "SELECT FTDataRowID, FTData as YearNameEN, FTDataChi as YearNameB5 FROM " . $this->relationTable . " AS RL";
						$strSQL .= " JOIN " . $this->fieldTable . " AS FT ON (RL.FieldID=FT.FieldID)";
						$strSQL .= " JOIN " . $this->dataTable . " AS DT ON (RL.RelationID=DT.RelationID)";
						$strSQL .= " WHERE RL.FormTemplateID='" . $this->Get_Safe_Sql_Query($templateID) . "' AND isUsed='Y' AND FieldLabel='AcademicYear'";
						if (!empty($formTPLID)) {
							$strSQL .= " AND FTDataRowID in ('" . $formTPLID . "')"; 
						}
						$strSQL .= "  GROUP BY YearNameEN, FTDataChi ORDER BY YearNameEN desc";
						$result = $this->returnDBResultSet($strSQL);
						if (count($result) > 0) {
							$result = $this->convertArrKeyToID($result, "FTDataRowID");
							return $result;
						}
					}
					return NULL;
				}
			}
			
			public function getSampleData() {
				$row = array();
				if (count($this->coreData["formTplInfo"]["childs"]) > 0) {
					$i = 0;
					
					if($this->coreData["template"]=="CPD" && $this->coreData["userInfo"]["customization"]['useEDBCPD']){
						$this->coreData["formTplInfo"]["childs"]["NoOfSection"]["FieldNameChi"] = "持續專業發展時數";
						$this->coreData["formTplInfo"]["childs"]["NoOfSection"]["FieldNameEn"] = "CPD Hour(s)";
					}
					
					foreach ($this->coreData["formTplInfo"]["childs"] as $kk => $vv) {
						if ($vv["isMultiLang"] == "Y") {
							// $row[0][$i] = $vv["FieldNameEn"] . " (" . $this->coreData["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
							$row[0][$i] = $vv["FieldNameEn"] . " (" . $this->coreData["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
							if ($vv["FieldStatus"] == "reference") {
								$row[0][$i] .= "[ref]";
							}
							$row[1][$i] = $vv["SampleData"];
							$i++;
							$row[0][$i] = $vv["FieldNameEn"] . " (" . $this->coreData["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
							if ($vv["FieldStatus"] == "reference") {
								$row[0][$i] .= "[ref]";
							}
							$row[1][$i] = $vv["SampleDataChi"];
						} else {
							$row[0][$i] = $vv["FieldNameEn"];
							if ($vv["FieldStatus"] == "reference") {
								$row[0][$i] .= "[ref]";
							}
							$row[1][$i] = $vv["SampleData"];
						}
						$i++;
					}
				}
				return $row;
			}
			
			public function getCSVHeader($childs) {
				$header = array();
				if (count($childs) > 0) {
					$i = 0;
					foreach ($childs as $kk => $vv) {
						if ($vv["isMultiLang"] == "Y") {
							$header[$i] = $vv["FieldNameEn"] . " (" . $this->coreData["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
							$i++;
							$header[$i] = $vv["FieldNameEn"] . " (" . $this->coreData["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
						} else {
							$header[$i] = $vv["FieldNameEn"];
						}
						$i++;
					}
				}
				return $header;
			}
			
			public function getGridDataTableArray($UserID=0, $postData = array(), $isGroupTable = FALSE) {
				$templateID = $this->coreData["formTplInfo"]["FormTemplateID"];
				
				if (!empty($postData["selyear"]) && $postData["selyear"] != "all") {
					$selectedYear = $postData["selyear"];
				} else {
					$selectedYear = 'all';
				}
				$finalData = array();
				$jsonData = array(
						"recordsTotal" => 0,
						"recordsFiltered" => 0,
						"allowExpand" => false,
						"acdyear" => $selectedYear,
						"data" => $finalData
				);
				
				if (!$isGroupTable) {
					if (empty($postData["s"])) {
						return $jsonData;
					} else {
						$searchArr = explode("||", base64_decode($postData["s"]));
						if (isset($searchArr[0])) {
							$selectedUser = $searchArr[0];
						}
						if (isset($searchArr[1])) {
							$selectedYear = $searchArr[1];
						}
					}
				}
				$recordsTotal = 0;
				$recordsFiltered = 0;
				if ($UserID > 0 && isset($postData["columns"]) && count($postData["columns"]) > 0) {
					if ($selectedYear != "all") {
						$yearRec = $this->getAcademicYear($selectedYear);
						if (isset($yearRec[$selectedYear]["YearNameEN"])) {
							$yearInfoTitle = $yearRec[$selectedYear]["YearNameEN"];
						}
					}
					$strSQL_param = " iu.UserID, iu.EnglishName, iu.ChineseName, iu.UserLogin";
					if (count($this->coreData["TemplateGridTableStructure"]) > 0) {
						$strSQL_param .= ", gt.FTDataRowID";
						$strs_group_param = "";
						foreach ($this->coreData["TemplateGridTableStructure"] as $fieldName => $fieldRec) {
							if (!empty($strSQL_param)) {
								$strSQL_param .= ", ";
							}
							if ($isGroupTable) {
								if ($fieldRec["groupBY"]) {
									$searchGroup[] = $fieldRec["field"];
									if (!empty($strs_group_param)) {
										$strs_group_param .= ", ";
									}
									$strs_group_param .= "gt." . $fieldRec["field"];
								}
								switch (strtoupper($fieldRec["action"])) {
									case "EMPTY":
										$strSQL_param .= "NULL AS " . $fieldRec["field"];
										break;
									case "SUM":
										$strSQL_param .= "SUM(gt." . $fieldRec["field"] . ") AS " . $fieldRec["field"];
										break;
									case "MIN":
										$strSQL_param .= "MIN(gt." . $fieldRec["field"] . ") AS " . $fieldRec["field"];
										break;
									case "MAX":
										$strSQL_param .= "MAX(gt." . $fieldRec["field"] . ") AS " . $fieldRec["field"];
										break;
									case "COUNT":
										$strSQL_param .= "COUNT(gt." . $fieldRec["field"] . ") AS " . $fieldRec["field"];
										break;
									case "CONCAT":
										$strSQL_param .= "GROUP_CONCAT(DISTINCT gt." . $fieldRec["field"] . " ORDER BY gt." . $fieldRec["field"] . " SEPARATOR ', ') AS " . $fieldRec["field"];
										break;
									default:
										if ($fieldRec["field"] == "DateModified") {
											$strSQL_param .= "MAX(gt." . $fieldRec["field"] . ") AS " . $fieldRec["field"];
										} else {
											$strSQL_param .= "gt." . $fieldRec["field"];
										}
										break;
								}
							} else {
								$strSQL_param .= "gt." . $fieldRec["field"];
							}
						}
					}
					$strSQL_from = " FROM " . $this->dataGridTable . " AS gt";
					/************************************************************************/
					
					$start = $postData["start"] ? $postData["start"] : 0;
					$pagelimit = $postData["length"] > 0 ? $postData["length"] : 10;
					$strSQL_limit = " LIMIT " . $start . ", " . $pagelimit;
					
					if ($this->coreData["userInfo"]["Role"]["isAdminMode"]) {
						$strSQL_join = " JOIN " . $this->userTable . " AS iu ON (iu.UserID=gt.UserID and Teaching='1' AND RecordType='1'";
						if (isset($selectedUser) && !empty($selectedUser)) {
							$strSQL_join .= " AND gt.UserID='" . $selectedUser . "'";
						}
						if (!empty($postData["search"]["value"])) {
							$strSQL_join .= " AND (iu.EnglishName LIKE '%" . $this->Get_Safe_Sql_Query($postData["search"]["value"]) . "%' OR iu.ChineseName LIKE '%" . $this->Get_Safe_Sql_Query($postData["search"]["value"]) . "%' OR iu.UserLogin LIKE '%" . $this->Get_Safe_Sql_Query($postData["search"]["value"]) . "%')";
						}
						$strSQL_join .= " )";
					} else {
						$strSQL_join = " JOIN " . $this->userTable . " AS iu ON (iu.UserID=gt.UserID and iu.Teaching='1' AND iu.RecordType='1' AND iu.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "')";
					}
					
					if (!empty($yearInfoTitle)) {
						$strSQL_where = " WHERE gt.AcademicYear like '" . $yearInfoTitle . "'"; 
					}
					
					$sortField = $postData["columns"][$postData["order"][0]["column"]]["data"];
					switch ($sortField) {
						case "TeacherName": $order_sort = "iu.EnglishName"; break;
						case "AcademicYear": $order_sort = "gt.AcademicYear"; break;
						case "Semester": $order_sort = "gt.SemesterEn"; break;
						case "ClassName": $order_sort = "gt.ClassNameEn"; break;
						case "Subject": $order_sort = "gt.SubjectEn"; break;
						case "NoOfSection": $order_sort = ($isGroupTable)?"SUM(gt.NoOfSection)":"gt.NoOfSection"; break;
						case "EventName": $order_sort = "gt.EventNameEn"; break;
						case "Remarks": $order_sort = "gt.RemarksEn"; break;
						case "StartDate": $order_sort = ($isGroupTable)?"MIN(gt.StartDate)":"gt.StartDate"; break;
						case "EndDate": $order_sort = ($isGroupTable)?"MAX(gt.EndDate)":"gt.EndDate"; break;
						case "DateModified": $order_sort = ($isGroupTable)?"MAX(gt.DateModified)":"gt.DateModified"; break;
						default:
							$order_sort = "gt.AcademicYear"; break;
							break;
					}
					$order_dir = "DESC";
					if (isset($postData["order"][0]["dir"])) {
						switch (strtoupper($postData["order"][0]["dir"])) {
							case "ASC":
								$order_dir = strtoupper($postData["order"][0]["dir"]);
								break;
						}
					}
					if (empty($order_sort)) {
						$strSQL_distinct = " DISTINCT gt.AcademicYear";
						$strSQL_order = " ORDER BY gt.AcademicYear DESC";
					} else {
						$strSQL_distinct = " DISTINCT " . $order_sort;
						$strSQL_order = " ORDER BY " . $order_sort . " " . $order_dir;
					}
					if ($isGroupTable) {
						$strSQL_distinct = " DISTINCT " . $strs_group_param;
						$strSQL_group = " GROUP BY " . $strs_group_param;
					}
					
					$strSQL = "SELECT COUNT(" . $strSQL_distinct . ") as totalrecord " . $strSQL_from . " " . $strSQL_join . " " . $strSQL_where;
					$totalRec = $this->returnDBResultSet($strSQL);

					if ($totalRec != NULL && count($totalRec) > 0) {
						$recordsTotal = $totalRec[0]["totalrecord"] ? $totalRec[0]["totalrecord"] : 0;
						$recordsFiltered = $totalRec[0]["totalrecord"] ? $totalRec[0]["totalrecord"] : 0;
						unset($totalRec);
						
						$strSQL = "SELECT " . $strSQL_param . " " . $strSQL_from . " " . $strSQL_join . " " . $strSQL_where . " " . $strSQL_group;
						$strSQL .= " " . $strSQL_order;
						if ($isGroupTable) {
							$strSQL .= " " . $strSQL_limit;
						}
						$data = $this->returnDBResultSet($strSQL);
						$allowCenter = false;
						if ($data != null && count($data) > 0) {
							foreach ($data as $kk => $vv) {
								$finalData[$vv["FTDataRowID"]] = $vv;
								$finalData[$vv["FTDataRowID"]]["TeacherName"] = Get_Lang_Selection($vv["ChineseName"], $vv["EnglishName"]) ? Get_Lang_Selection($vv["ChineseName"], $vv["EnglishName"]) : "-";
								switch ($this->coreData["template"]) {
									case "ACTIVITIES" :
										$finalData[$vv["FTDataRowID"]]["Semester"] = Get_Lang_Selection($vv["SemesterChi"], $vv["SemesterEn"]) ? Get_Lang_Selection($vv["SemesterChi"], $vv["SemesterEn"]) : "-";
										$finalData[$vv["FTDataRowID"]]["EventName"] = Get_Lang_Selection($vv["EventNameChi"], $vv["EventNameEn"]) ? Get_Lang_Selection($vv["EventNameChi"], $vv["EventNameEn"]) : "-";
										$finalData[$vv["FTDataRowID"]]["Hours"] = $vv["Hours"] ? $vv["Hours"] : 0;
										if ($allowCenter) $finalData[$vv["FTDataRowID"]]["Hours"] = "<div class='text-center'>" . $finalData[$vv["FTDataRowID"]]["Hours"] . "</div>";
										$finalData[$vv["FTDataRowID"]]["Remarks"] = Get_Lang_Selection($vv["RemarksChi"], $vv["RemarksEn"]) ? Get_Lang_Selection($vv["RemarksChi"], $vv["RemarksEn"]) : "-";
										break;
									case "TEACHING_RECORD" :
										$finalData[$vv["FTDataRowID"]]["ClassName"] = Get_Lang_Selection($vv["ClassNameChi"], $vv["ClassNameEn"]) ? Get_Lang_Selection($vv["ClassNameChi"], $vv["ClassNameEn"]) : "0";
										$finalData[$vv["FTDataRowID"]]["Subject"] = Get_Lang_Selection($vv["SubjectChi"], $vv["SubjectEn"]) ? Get_Lang_Selection($vv["SubjectChi"], $vv["SubjectEn"]) : "-";
										$finalData[$vv["FTDataRowID"]]["NoOfSection"] = $vv["NoOfSection"] ? $vv["NoOfSection"] : 0;
										if ($allowCenter) $finalData[$vv["FTDataRowID"]]["NoOfSection"] = "<div class='text-center'>" . $finalData[$vv["FTDataRowID"]]["NoOfSection"] . "</div>";
										$finalData[$vv["FTDataRowID"]]["Remarks"] = Get_Lang_Selection($vv["RemarksChi"], $vv["RemarksEn"]) ? Get_Lang_Selection($vv["RemarksChi"], $vv["RemarksEn"]) : "-";
										break;
									case "CPD" :
										$finalData[$vv["FTDataRowID"]]["StartDate"] = $vv["StartDate"] ? $vv["StartDate"] : "-";
										$finalData[$vv["FTDataRowID"]]["EndDate"] = $vv["EndDate"] ? $vv["EndDate"] : "-";
										$finalData[$vv["FTDataRowID"]]["NoOfSection"] = $vv["NoOfSection"] ? $vv["NoOfSection"] : 0;
										if ($allowCenter) $finalData[$vv["FTDataRowID"]]["NoOfSection"] = "<div class='text-center'>" . $finalData[$vv["FTDataRowID"]]["NoOfSection"] . "</div>";
										$finalData[$vv["FTDataRowID"]]["EventName"] = Get_Lang_Selection($vv["EventNameChi"], $vv["EventNameEn"]) ? Get_Lang_Selection($vv["EventNameChi"], $vv["EventNameEn"]) : "-";
										$finalData[$vv["FTDataRowID"]]["EventType"] = Get_Lang_Selection($vv["EventTypeChi"], $vv["EventTypeEn"]) ? Get_Lang_Selection($vv["EventTypeChi"], $vv["EventTypeEn"]) : "-";
										$finalData[$vv["FTDataRowID"]]["Remarks"] = Get_Lang_Selection($vv["RemarksChi"], $vv["RemarksEn"]) ? Get_Lang_Selection($vv["RemarksChi"], $vv["RemarksEn"]) : "-";
										if($this->coreData["userInfo"]["customization"]['useEDBCPD']){
											$finalData[$vv["FTDataRowID"]]["Content"] = $vv["Content"] ? $vv["Content"] : "-";
											$finalData[$vv["FTDataRowID"]]["Organization"] = $vv["Organization"] ? $vv["Organization"] : "-";
											$finalData[$vv["FTDataRowID"]]["CPDMode"] = ($vv["CPDMode"]==='1' || $vv["CPDMode"]==='0')?(($vv["CPDMode"]==0)?$this->coreData["lang"]['TeacherPortfolio']["edit_cpd_mode_systematic_scheme"]:$this->coreData["lang"]['TeacherPortfolio']["edit_cpd_mode_others"]):"-" ;
											if($vv["CPDDomain"]){
												$finalData[$vv["FTDataRowID"]]["CPDDomain"] = "";
												$domainArr = explode(",",$vv["CPDDomain"]);
												for($idx=0;$idx < 6; $idx++){
													if($domainArr[$idx]=="1"){
														$finalData[$vv["FTDataRowID"]]["CPDDomain"] .= $this->coreData["lang"]['TeacherPortfolio']['edit_cpd_domain'][$idx]."<br>\n\r";
													}
												}
											}else{
												$finalData[$vv["FTDataRowID"]]["CPDDomain"] = "-";
											}
											if($vv["SubjectRelated"]){
												$finalData[$vv["FTDataRowID"]]["SubjectRelated"] = "";
												$domainArr = explode(",",$vv["SubjectRelated"]);
												for($idx=0;$idx < count($this->coreData["lang"]['TeacherPortfolio']['edit_cpd_subject']); $idx++){
													if($domainArr[$idx]=="1"){
														$finalData[$vv["FTDataRowID"]]["SubjectRelated"] .= $this->coreData["lang"]['TeacherPortfolio']['edit_cpd_subject'][$idx]."<br>\n\r";
													}
												}
											}else{												
												$finalData[$vv["FTDataRowID"]]["SubjectRelated"] = "-";
											}
											$finalData[$vv["FTDataRowID"]]["BasicLawRelated"] = $vv["BasicLawRelated"] ? $vv["BasicLawRelated"] : 0;
										}
										break;
									case "ATTENDANCE" :
										$finalData[$vv["FTDataRowID"]]["Month"] = $vv["Month"] ? $vv["Month"] : "-";
										if ($allowCenter) $finalData[$vv["FTDataRowID"]]["Month"] = "<div class='text-center'>" . $finalData[$vv["FTDataRowID"]]["Month"] . "</div>"; 
										$finalData[$vv["FTDataRowID"]]["Attend"] = $vv["Attend"] ? $vv["Attend"] : "0";
										if ($allowCenter) $finalData[$vv["FTDataRowID"]]["Attend"] = "<div class='text-center'>" . $finalData[$vv["FTDataRowID"]]["Attend"] . "</div>";
										$finalData[$vv["FTDataRowID"]]["Late"] = $vv["Late"] ? $vv["Late"] : "0";
										if ($allowCenter) $finalData[$vv["FTDataRowID"]]["Late"] = "<div class='text-center'>" . $finalData[$vv["FTDataRowID"]]["Late"] . "</div>";
										$finalData[$vv["FTDataRowID"]]["SickLeave"] = $vv["SickLeave"] ? $vv["SickLeave"] : "0";
										if ($allowCenter) $finalData[$vv["FTDataRowID"]]["SickLeave"] = "<div class='text-center'>" . $finalData[$vv["FTDataRowID"]]["SickLeave"] . "</div>";
										$finalData[$vv["FTDataRowID"]]["CasualLeave"] = $vv["CasualLeave"] ? $vv["CasualLeave"] : "0";
										if ($allowCenter) $finalData[$vv["FTDataRowID"]]["CasualLeave"] = "<div class='text-center'>" . $finalData[$vv["FTDataRowID"]]["CasualLeave"] . "</div>";
										$finalData[$vv["FTDataRowID"]]["AnnualLeave"] = $vv["AnnualLeave"] ? $vv["AnnualLeave"] : "0";
										if ($allowCenter) $finalData[$vv["FTDataRowID"]]["AnnualLeave"] = "<div class='text-center'>" . $finalData[$vv["FTDataRowID"]]["AnnualLeave"] . "</div>";
										break;
								}
								if ($isGroupTable) {
									if (count($searchGroup)) {
										$str_search = "";
										foreach ($searchGroup as $groupKK => $groupVV) {
											if (!empty($str_search)) {
												$str_search .= "||";
											}
											$str_search .= $finalData[$vv["FTDataRowID"]][$groupVV];
										}
										$searchParam = base64_encode($str_search);
									} else {
										$searchParam = "";
									}
									$childType = $this->coreData["childType"];
									switch ($childType) {
										case "Modal":
										    $finalData[$vv["FTDataRowID"]]["rel"] = '?task=' . $this->coreData["section"] . '.childlist&s=' . $searchParam. '';
											$finalData[$vv["FTDataRowID"]]["func"] = '<div class="text-center">';
											$finalData[$vv["FTDataRowID"]]["func"] .= '<a href="#" class="btnModal" rel="?task=' . $this->coreData["section"] . '.childlist&s=' . $searchParam. '"';
											$finalData[$vv["FTDataRowID"]]["func"] .= ' title="' . $this->coreData["lang"]["TeacherPortfolio"]["sec_" . $this->coreData["section"]] . " / " . $finalData[$vv["FTDataRowID"]]["TeacherName"] . ' / ' . $finalData[$vv["FTDataRowID"]]["AcademicYear"] . '"><span class="glyphicon glyphicon-zoom-in"></span></a></div>';
											break;
										case "Expand":
										    $finalData[$vv["FTDataRowID"]]["rel"] = '?task=' . $this->coreData["section"] . '.childlist&s=' . $searchParam. '';
											$finalData[$vv["FTDataRowID"]]["func"] = '<div class="text-center">';
											$finalData[$vv["FTDataRowID"]]["func"] .= '<a href="#" class="btn-detail" rel="?task=' . $this->coreData["section"] . '.childlist&s=' . $searchParam. '"><i class="fa fa-caret-up"></i><i class="fa fa-caret-down"></i></a>';
											$finalData[$vv["FTDataRowID"]]["func"] .= '</div>';
											break;
									}
								} else {
									/********************************************************************/
									/* Customization
									/********************************************************************/
									if ($this->coreData["section"]=="cpd")
									{
										if ( $this->coreData["userInfo"]["customization"]["allowImportCPDByIndividualTeacher"] &&
												$this->coreData["userInfo"]["Role"]["isOwnerMode"] ) {
											$finalData[$vv["FTDataRowID"]]["func"] = '<div class="text-center"><label class="text-left"><input type="checkbox" class="form-control" name="dataid[]" value="' . $vv["FTDataRowID"] . '"><span class="custom-check"></span></label><a href="#" class="btnAjax" rel="' . $this->coreData["section"] . '/editrecord/' . $vv["FTDataRowID"] . '"><i class="fa fa-pencil"></i></a></div>';
										}
										else if (($this->coreData["userInfo"]["currMode"] == "sys") &&
											($this->coreData["userInfo"]["info"]["isSystemAdmin"] == "Y" || $this->coreData["userInfo"]["info"]["isModuleAdmin"] == "Y"))
										{
											$finalData[$vv["FTDataRowID"]]["func"] = '<div class="text-center"><label class="text-left"><input type="checkbox" class="form-control" name="dataid[]" value="' . $vv["FTDataRowID"] . '"><span class="custom-check"></span></label><a href="#" class="btnAjax" rel="' . $this->coreData["section"] . '/editrecord/' . $vv["FTDataRowID"] . '"><i class="fa fa-pencil"></i></a></div>';
										}
									}
									else 
									{
										if (($this->coreData["userInfo"]["currMode"] == "sys") &&
											($this->coreData["userInfo"]["info"]["isSystemAdmin"] == "Y" || $this->coreData["userInfo"]["info"]["isModuleAdmin"] == "Y"))
										{
											$finalData[$vv["FTDataRowID"]]["func"] = '<div class="text-center"><label class="text-left"><input type="checkbox" class="form-control" name="dataid[]" value="' . $vv["FTDataRowID"] . '"><span class="custom-check"></span></label></div>';
										}
									}
								}
							} // end of for loop
						}
					}
				}
				
				$jsonData = array(
						"recordsTotal" => $recordsTotal,
						"recordsFiltered" => $recordsFiltered,
						"allowExpand" => true,
						"acdyear" => $selectedYear,
						"data" => array_values($finalData)
				);
				return $jsonData;
			}
			
			public function getDataTableArray($UserID=0, $postData = array()) {
				$templateID = $this->coreData["formTplInfo"]["FormTemplateID"];
				if (!empty($postData["selyear"]) && $postData["selyear"] != "all") {
					$selectedYear = $postData["selyear"];
				} else {
					$selectedYear = 'all';
				}
				$finalData = array();
				$jsonData = array(
					"recordsTotal" => 0,
					"recordsFiltered" => 0,
					"allowExpand" => false,
					"acdyear" => $selectedYear,
					"data" => $finalData
				);
				$recordsTotal = 0;
				$recordsFiltered = 0;
				if ($UserID > 0 && isset($postData["columns"]) && count($postData["columns"]) > 0) {
					$sortField = $postData["columns"][$postData["order"][0]["column"]]["data"];
					switch ($sortField) {
						case "TeacherName":
							$found_field = "AcademicYear";
							break;
						default: 
							$found_field = $sortField;
							break;
					}
					
					if ($selectedYear != "all") {
						$yearRec = $this->getAcademicYear($selectedYear);
						if (isset($yearRec[$selectedYear]["YearNameEN"])) {
							$yearInfoTitle = $yearRec[$selectedYear]["YearNameEN"];
						}
					}

					$strSQL = "SELECT RelationID FROM " . $this->relationTable . " AS RL";
					$strSQL .= " JOIN " . $this->fieldTable . " AS FT ON (RL.FieldID=FT.FieldID)";
					$strSQL .= " WHERE RL.FormTemplateID='" . $this->Get_Safe_Sql_Query($templateID) . "' AND FieldLabel='" . $found_field . "'";
					$strSQL .= " AND isUsed='Y'";
					$strSQL .= " LIMIT 1";

					$result = $this->returnDBResultSet($strSQL);
					if ($result != NULL) {
						if (!isset($result[0]["RelationID"])) {
							$jsonData["error"] = 'NO SORT DATA';
							return $jsonData;
						}
						$RelationID = $result[0]["RelationID"];
						$strSQL_st = " LEFT JOIN " . $this->dataTable . " as DT on (DT.FTDataRowID = DGT.FTDataRowID AND (DT.RelationID='" . $RelationID . "' OR DT.RelationID is null))";
						$strSQL_st .= " JOIN " . $this->relationTable . " as RL on (RL.RelationID = DT.RelationID AND RL.FormTemplateID='" . $this->Get_Safe_Sql_Query($templateID) . "')";

						if ($this->coreData["userInfo"]["Role"]["isAdminMode"]) {
							$strSQL_st .= " JOIN " . $this->userTable . " as UT on (UT.UserID = DGT.UserID and Teaching='1' AND RecordType='1'";
							if (!empty($postData["search"]["value"])) {
								$strSQL_st .= " AND (EnglishName like '%" . $this->Get_Safe_Sql_Query($postData["search"]["value"]) . "%' or ChineseName like '%" . $this->Get_Safe_Sql_Query($postData["search"]["value"]) . "%' OR UserLogin like '%" . $this->Get_Safe_Sql_Query($postData["search"]["value"]) . "%')";
							}
							$strSQL_st .= " )";
						} else {
							$strSQL_st .= " JOIN " . $this->userTable . " as UT on (UT.UserID = DGT.UserID and UT.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "' AND Teaching='1' and RecordType='1')";
							$strSQL_st .= " WHERE DGT.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "'";
						}

						if ($selectedYear != "all" && !empty($yearInfoTitle)) {
							$subSubSQL = "SELECT sDGT.FTDataRowID FROM " . $this->dataGridTable . " as sDGT";
							$subSubSQL .= " JOIN " . $this->dataTable . " as sDT on (sDT.FTDataRowID = sDGT.FTDataRowID AND (sDT.RelationID='" . $RelationID . "'))";
							$subSubSQL .= " JOIN " . $this->userTable . " as sUT on (sUT.UserID=sDGT.UserID)";
							$subSubSQL .= " WHERE sDGT.FormTemplateID='" . $this->Get_Safe_Sql_Query($templateID) . "' AND sDT.FTData like '" . $yearInfoTitle . "'";
							if ($this->coreData["userInfo"]["Role"]["isAdminMode"]) {
								$strSQL_st .= " WHERE DGT.FTDataRowID in (" . $subSubSQL . ")";
							} else {
								$subSubSQL .= " AND sDGT.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "'";
								$strSQL_st .= " AND DGT.FTDataRowID in (" . $subSubSQL . ")";
							}
						}
						$strSQL = "SELECT COUNT(DGT.FTDataRowID) as totalrecord FROM " . $this->dataGridTable . " as DGT";
						$strSQL .= $strSQL_st;
						/************************************************************************/
						$totalRec = $this->returnDBResultSet($strSQL);
						if ($totalRec != NULL && count($totalRec) > 0) {
							$recordsTotal = $totalRec[0]["totalrecord"] ? $totalRec[0]["totalrecord"] : 0;
							$recordsFiltered = $totalRec[0]["totalrecord"] ? $totalRec[0]["totalrecord"] : 0;
							unset($totalRec);
							/************************************************************************/

							$strSQL = "SELECT DGT.FTDataRowID FROM " . $this->dataGridTable . " as DGT";
							$strSQL .= $strSQL_st;
							$order_dir = "DESC";
							if (isset($postData["order"][0]["dir"])) {
								switch (strtoupper($postData["order"][0]["dir"])) {
									case "ASC":
										$order_dir = strtoupper($postData["order"][0]["dir"]);
										break;
								}
							}
							switch ($sortField) {
								case "TeacherName":	
									$order_field = Get_Lang_Selection("ChineseName", "EnglishName");
									break;
								default:
									$order_field = Get_Lang_Selection("FTDataChi", "FTData");
									// $order_field = "FTData";
									break;
							}
							$strOrder = " ORDER BY " . $order_field . " " . $order_dir;
							$strSQL .= $strOrder;
							
							/************************************************************************/
							$start = $postData["start"] ? $postData["start"] : 0;
							$pagelimit = $postData["length"] > 0 ? $postData["length"] : 0;
							$strLimit = " LIMIT " . $start . ", " . $pagelimit;
							if ($pagelimit > 0) $strSQL .= $strLimit;
							$id_result = $this->returnDBResultSet($strSQL);
							
							if ($id_result != null && count($id_result) > 0) {
								$id_result = $this->convertArrKeyToID($id_result, "FTDataRowID");
								
								if (isset($this->coreData["formTplInfo"]["childs"]) && count($this->coreData["formTplInfo"]["childs"]) > 0) {
									$array_structure = array_flip(array_keys($this->coreData["formTplInfo"]["childs"]));
									if (isset($array_structure["UserID"])) unset($array_structure["UserID"]);
									foreach ($array_structure as $skk => $svv) {
										switch ($this->coreData["formTplInfo"]["childs"][$skk]["FieldType"]) {
											case "INT":
												$array_structure[$skk] = "0";
												break;
											default:
												$array_structure[$skk] = "-";
												break;
										}
									}
								}
								$finalData = array();
								foreach ($id_result as $idKK => $idVV) {
									$finalData[$idKK] = $array_structure;
									$finalData[$idKK]["FTDataRowID"] = $idKK;
									if (($this->coreData["userInfo"]["currMode"] == "sys") && ($this->coreData["userInfo"]["info"]["isSystemAdmin"] == "Y" || $this->coreData["userInfo"]["info"]["isModuleAdmin"] == "Y")) {
										$finalData[$idKK]["func"] = '<div class="text-center"><label class="text-left"><input type="checkbox" class="form-control" name="dataid[]" value="' . $idKK . '"><span class="custom-check"></span></label></div>';
									}
								}
								$strSQL = "SELECT EnglishName, ChineseName, FTDataID, DT.FTDataRowID, FTData, FTDataChi, FT.FieldLabel, FT.FieldNameChi, FT.FieldNameEn, FT.FieldType, FT.isMultiLang";
								$strSQL .= " FROM " . $this->dataTable . " as DT";
								$strSQL .= " JOIN " . $this->dataGridTable . " as DGT on (DGT.FTDataRowID in (" . implode(", ", array_keys($id_result)) . ") and DGT.FTDataRowID=DT.FTDataRowID) ";
								$strSQL .= " JOIN " . $this->relationTable . " as RL on (DT.RelationID=RL.RelationID AND RL.FormTemplateID='" . $this->Get_Safe_Sql_Query($templateID) . "')";
								$strSQL .= " JOIN " . $this->fieldTable . " as FT on (FT.FieldID=RL.FieldID)";
								if ($this->coreData["userInfo"]["currMode"] == "owner") {
									$strSQL .= " JOIN " . $this->userTable . " as UT on (UT.UserID = DGT.UserID and UT.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "' AND Teaching='1' and RecordType='1')";
								} else {
									$strSQL .= " JOIN " . $this->userTable . " as UT on (UT.UserID = DGT.UserID and Teaching='1' and RecordType='1')";
								}
								$strSQL .= " WHERE DT.FTDataRowID in (" . implode(", ", array_keys($id_result)) . ") and DGT.FTDataRowID=DT.FTDataRowID";

								$data = $this->returnDBResultSet($strSQL);
								if ($data != null && count($data) > 0) {
									foreach ($data as $kk => $vv) {
										if (!isset($finalData[$vv["FTDataRowID"]])) {
											$finalData[$vv["FTDataRowID"]] = $array_structure;
											$finalData[$vv["FTDataRowID"]]["FTDataRowID"] = $vv["FTDataRowID"];
										}
										$finalData[$vv["FTDataRowID"]][$vv["FieldLabel"]] = $vv["FTData"];
										// if ($vv["isMultiLang"] == "Y" && !empty($vv["FTDataChi"])) {
										if ($vv["isMultiLang"] == "Y") {
											$finalData[$vv["FTDataRowID"]][$vv["FieldLabel"]] = $vv["FTDataChi"];
										}
										if (empty($finalData[$vv["FTDataRowID"]][$vv["FieldLabel"]])) $finalData[$vv["FTDataRowID"]][$vv["FieldLabel"]] = "-";
										if (isset($finalData[$vv["FTDataRowID"]]["TeacherName"])) {
											$finalData[$vv["FTDataRowID"]]["TeacherName"] = Get_Lang_Selection($vv["ChineseName"], $vv["EnglishName"]);
										}
									}
									
								}
								unset($data);
							}
							unset($id_result);
						}
					}
				}
				$jsonData = array(
					"recordsTotal" => $recordsTotal,
					"recordsFiltered" => $recordsFiltered,
					"allowExpand" => false,
					"acdyear" => $selectedYear,
					"data" => array_values($finalData)
				);
				return $jsonData;
			}

			public function getAllData($UserID, $fullFieldData = FALSE) {
				$templateID = $this->coreData["formTplInfo"]["FormTemplateID"];
				$currLang = Get_Lang_Selection("chi", "eng");
				$row = array();
				if (count($this->coreData["formTplInfo"]["childs"]) > 0) {
					$tmp_dataStructure = array_flip(array_keys($this->coreData["formTplInfo"]["childs"]));
					foreach ($tmp_dataStructure as $skk => $svv) {
						if ($this->coreData["formTplInfo"]["childs"][$skk]["isMultiLang"] == "Y") {
							$dataStructure[$skk . "_eng"] = $tmp_dataStructure[$skk];
							$dataStructure[$skk . "_chi"] = $tmp_dataStructure[$skk];
						} else {
							$dataStructure[$skk] = $tmp_dataStructure[$skk];
						}
						switch ($this->coreData["formTplInfo"]["childs"][$skk]["FieldType"]) {
							case "INT":
								$dataStructure[$skk] = "0";
								break;
							default:
								if ($this->coreData["formTplInfo"]["childs"][$skk]["isMultiLang"] == "Y") {
									if ($fullFieldData) {
										$dataStructure[$skk . "_eng"] = "-";
										$dataStructure[$skk . "_chi"] = "-";
									} else {
										if ($currLang == "eng") {
											$dataStructure[$skk . "_eng"] = "-";
										} else {
											$dataStructure[$skk . "_chi"] = "-";
										}
									}
								} else {
									$dataStructure[$skk] = "-";
								}
								break;
						}
				
					}
					unset($tmp_dataStructure);
					if ($this->coreData["TemplateType"] == "GRID") {
						$strSQL_param = " iu.UserLogin, iu.EnglishName, iu.ChineseName";
						if (count($this->coreData["TemplateGridTableStructure"]) > 0) {
							$strSQL_param .= ", gt.FTDataRowID";
							$strs_group_param = "";
							foreach ($this->coreData["TemplateGridTableStructure"] as $fieldName => $fieldRec) {
								if($fieldRec["field"]=="BasicLawRelated"){continue;}
								if (!empty($strSQL_param)) {
									$strSQL_param .= ", ";
								}								
								$strSQL_param .= "gt." . $fieldRec["field"];
							}
						}
						$strSQL_from = " FROM " . $this->dataGridTable . " AS gt";
						if ($this->coreData["userInfo"]["Role"]["isAdminMode"]) {
							$strSQL_join = " JOIN " . $this->userTable . " AS iu ON (iu.UserID=gt.UserID and Teaching='1' AND RecordType='1'";
							$strSQL_join .= " )";
						} else {
							$strSQL_join = " JOIN " . $this->userTable . " AS iu ON (iu.UserID=gt.UserID and iu.Teaching='1' AND iu.RecordType='1' AND iu.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "')";
						}
						$strSQL = "SELECT " . $strSQL_param . " " . $strSQL_from . " " . $strSQL_join . " ORDER BY EnglishName ASC, gt.AcademicYear ASC";
						$data = $this->returnDBResultSet($strSQL);
						if ($data != null && count($data) > 0) {
							$j = 0;
							$i = 0;
							foreach ($this->coreData["formTplInfo"]["childs"] as $kk => $vv) {
								if($kk=="BasicLawRelated"){continue;}
								if (!$fullFieldData && $kk == 'UserID') {
									continue;								
								} else {
									if ($vv["isMultiLang"] == "Y") {
										if ($fullFieldData) {
											$rowData[$i][$j] = $vv["FieldNameEn"] . " (" . $this->coreData["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
											$j++;
											$rowData[$i][$j] = $vv["FieldNameEn"] . " (" . $this->coreData["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
										} else {
											if ($currLang == "eng") {
												$rowData[$i][$j] = $vv["FieldNameEn"];
											} else {
												$rowData[$i][$j] = $vv["FieldNameChi"];
											}
										}
									} else {
										if ($fullFieldData) {
											$rowData[$i][$j] = $vv["FieldNameEn"];
										} else {
											if ($currLang == "eng") {
												$rowData[$i][$j] = $vv["FieldNameEn"];
											} else {
												$rowData[$i][$j] = $vv["FieldNameChi"];
											}
										}
									}
									$j++;
								}
							}
							$i++;
							$j = 0;
							foreach ($data as $kk => $vv) {
								$j = 0;
								if ($fullFieldData) {
									$rowData[$i][$j] = $vv["UserLogin"];
									$j++;
									$rowData[$i][$j] = $vv["EnglishName"];
									$j++;
									$rowData[$i][$j] = $vv["ChineseName"];
								} else {
									if ($currLang == "eng") {
										$rowData[$i][$j] = $vv["EnglishName"];
									} else {
										$rowData[$i][$j] = $vv["ChineseName"];
									}
								}
								$j++;
								foreach ($this->coreData["TemplateGridTableStructure"] as $fieldName => $fieldRec) {
									if ($fieldRec["output"]) {
										if ($fullFieldData) {											
											if($this->coreData["userInfo"]["customization"]['useEDBCPD']){
												if($fieldRec["field"] == "CPDMode"){
													$rowData[$i][$j] = ($vv[$fieldRec["field"]]==='1' || $vv[$fieldRec["field"]]==='0')?(($vv[$fieldRec["field"]]==0)?$this->coreData["lang"]['TeacherPortfolio']["edit_cpd_mode_systematic_scheme"]:$this->coreData["lang"]['TeacherPortfolio']["edit_cpd_mode_others"]):"-" ;
												}else
												if($fieldRec["field"] == "CPDDomain"){
													$rowData[$i][$j] = "";
													$domainArr = explode(",",$vv[$fieldRec["field"]]);
													for($idx=0;$idx < 6; $idx++){
														if($domainArr[$idx]=="1"){
															$rowData[$i][$j] .= $this->coreData["lang"]['TeacherPortfolio']['edit_cpd_domain'][$idx]."\n\r";
														}
													}
												}elseif($fieldRec["field"] == "SubjectRelated"){
													$rowData[$i][$j] = "";
													$domainArr = explode(",",$vv[$fieldRec["field"]]);
													for($idx=0;$idx < count($this->coreData["lang"]['TeacherPortfolio']['edit_cpd_subject']); $idx++){
														if($domainArr[$idx]=="1"){
															$rowData[$i][$j] .= $this->coreData["lang"]['TeacherPortfolio']['edit_cpd_subject'][$idx]."\n\r";
														}
													}
												}elseif($fieldRec["field"] == "BasicLawRelated"){
													$rowData[$i][$j] = "";
												}else{
													$rowData[$i][$j] = $vv[$fieldRec["field"]];
												}
											}else{
												$rowData[$i][$j] = $vv[$fieldRec["field"]];
											}
											$j++;
										} else {
											if ($fieldRec["field"] != "UserID") {
												if (substr($fieldRec["field"], -3) != "Chi" && substr($fieldRec["field"], -2) != "En") {
													if($this->coreData["userInfo"]["customization"]['useEDBCPD']){
														if($fieldRec["field"] == "CPDMode"){
															$rowData[$i][$j] = ($vv[$fieldRec["field"]]==='1' || $vv[$fieldRec["field"]]==='0')?(($vv[$fieldRec["field"]]==0)?$this->coreData["lang"]['TeacherPortfolio']["edit_cpd_mode_systematic_scheme"]:$this->coreData["lang"]['TeacherPortfolio']["edit_cpd_mode_others"]):"-" ;
														}else
														if($fieldRec["field"] == "CPDDomain"){
															$rowData[$i][$j] = "";
															$domainArr = explode(",",$vv[$fieldRec["field"]]);
															for($idx=0;$idx < 6; $idx++){
																if($domainArr[$idx]=="1"){
																	$rowData[$i][$j] .= $this->coreData["lang"]['TeacherPortfolio']['edit_cpd_domain'][$idx]."\n\r";
																}
															}
														}elseif($fieldRec["field"] == "SubjectRelated"){
															$rowData[$i][$j] = "";
															$domainArr = explode(",",$vv[$fieldRec["field"]]);
															for($idx=0;$idx < count($this->coreData["lang"]['TeacherPortfolio']['edit_cpd_subject']); $idx++){
																if($domainArr[$idx]=="1"){
																	$rowData[$i][$j] .= $this->coreData["lang"]['TeacherPortfolio']['edit_cpd_subject'][$idx]."\n\r";
																}
															}
														}else{
															$rowData[$i][$j] = $vv[$fieldRec["field"]];
														}
													}else{
														$rowData[$i][$j] = $vv[$fieldRec["field"]];
													}
													$j++;
												} else if ($currLang == "eng" && substr($fieldRec["field"], -2) == "En") {
													$rowData[$i][$j] = $vv[$fieldRec["field"]];
													$j++;
												} else if ($currLang == "chi" && substr($fieldRec["field"], -3) == "Chi") {
													$rowData[$i][$j] = $vv[$fieldRec["field"]];
													$j++;
												}
											}
										}										
									}
								}
								$i++;
							}
						}
					} else {
						$strSQL = "SELECT UT.UserID, UserLogin, EnglishName, ChineseName, FTDataID, DT.FTDataRowID, FTData, FTDataChi, FT.FieldLabel, FT.FieldNameChi, FT.FieldNameEn, FT.FieldType, FT.isMultiLang";
						$strSQL .= " FROM " . $this->dataTable . " as DT";
						$strSQL .= " JOIN " . $this->dataGridTable . " as DGT on (DGT.FTDataRowID=DT.FTDataRowID)";
						$strSQL .= " JOIN " . $this->userTable . " as UT on (UT.UserID=DGT.UserID)";
						$strSQL .= " JOIN " . $this->relationTable . " as RL on (DT.RelationID=RL.RelationID AND RL.FormTemplateID='" . $this->Get_Safe_Sql_Query($templateID) . "')";
						$strSQL .= " JOIN " . $this->fieldTable . " as FT on (FT.FieldID=RL.FieldID)";
						if ($UserID != "all" && $UserID > 0 && !$this->coreData["userInfo"]["Role"]["isAdminMode"]) {
							$strSQL .= " WHERE UT.UserID='" . $UserID . "'";
						}
						$strSQL .= " ORDER BY UT.UserID, DT.FTDataRowID ASC";
						$data = $this->returnDBResultSet($strSQL);
						$rowData = array();
						if ($data != null && count($data) > 0) {
							$j = 0;
							$i = 0;
							foreach ($this->coreData["formTplInfo"]["childs"] as $kk => $vv) {
								if ($vv["isMultiLang"] == "Y") {
									$rowData[$i][$j] = $vv["FieldNameEn"] . " (" . $this->coreData["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
									$j++;
									$rowData[$i][$j] = $vv["FieldNameEn"] . " (" . $this->coreData["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
								} else {
									$rowData[$i][$j] = $vv["FieldNameEn"];
								}
								$j++;
							}
							$i++;
							$currRowID = 0;
							$finalData = array();
							foreach ($data as $kk => $vv) {
								if ($currRowID != $vv["FTDataRowID"]) {
									if ($currRowID > 0) {
										$rowData[$i] = array_values($finalData);
										$finalData = array();
										$i++;
									}
									$currRowID = $vv["FTDataRowID"];
								}
								if (count($finalData) == 0) $finalData = $dataStructure;
								if (isset($finalData["UserID"])) $finalData["UserID"] = $vv["UserLogin"];
								if (isset($finalData["TeacherName_eng"])) $finalData["TeacherName_eng"] = $vv["EnglishName"];
								if (isset($finalData["TeacherName_chi"])) $finalData["TeacherName_chi"] = $vv["ChineseName"];
								if (isset($finalData["FTDataRowID"])) $finalData["FTDataRowID"] = $vv["FTDataRowID"];
								if (isset($finalData[$vv["FieldLabel"]])) $finalData[$vv["FieldLabel"]] = $vv["FTData"];
								if ($vv["isMultiLang"] == "Y" && !empty($vv["FTDataChi"])) {
									if (isset($finalData[$vv["FieldLabel"]."_eng"])) $finalData[$vv["FieldLabel"]."_eng"] = $vv["FTData"];
									if (isset($finalData[$vv["FieldLabel"]."_chi"])) $finalData[$vv["FieldLabel"]."_chi"] = $vv["FTDataChi"];
								}
							}
							$rowData[$i] = array_values($finalData);
							$i++;
						}
					}
					
					return $rowData; 
				}
				return array();
				exit;
			}
			
			public function exportAllData($CSVObj, $UserID) {
				$templateID = $this->coreData["formTplInfo"]["FormTemplateID"];
				$row = array();
				$rowData = $this->getAllData($UserID, TRUE);
				if (count($rowData) > 0) {
					$total = count($rowData) - 1;
					if ($total > 0) {
						$logDetail = "Data Export: ";
						$logDetail .= "<small>Total " . $total . " Record(s)</small>";
						$this->core->load_model('LogObj');
						$this->core->LogObj->addRecord($UserID,  str_replace("_", " ", strtoupper($this->coreData["template"])), $logDetail);
					}
					$CSVObj->outputData($rowData, strtolower($this->coreData["formTplInfo"]["TemplateName"]), $UserID);
					exit;
				} else {
					header('Content-type: text/plain; charset=utf-8');
					echo $this->coreData["lang"]["TeacherPortfolio"]["downloadNoRecord"];
					exit;
				}
				
/*
				if (count($this->coreData["formTplInfo"]["childs"]) > 0) {
					$tmp_dataStructure = array_flip(array_keys($this->coreData["formTplInfo"]["childs"]));
					foreach ($tmp_dataStructure as $skk => $svv) {
						if ($this->coreData["formTplInfo"]["childs"][$skk]["isMultiLang"] == "Y") {
							$dataStructure[$skk . "_eng"] = $tmp_dataStructure[$skk];
							$dataStructure[$skk . "_chi"] = $tmp_dataStructure[$skk];
						} else {
							$dataStructure[$skk] = $tmp_dataStructure[$skk];
						}
						switch ($this->coreData["formTplInfo"]["childs"][$skk]["FieldType"]) {
							case "INT":
								$dataStructure[$skk] = "0";
								break;
							default:
								if ($this->coreData["formTplInfo"]["childs"][$skk]["isMultiLang"] == "Y") {
									$dataStructure[$skk . "_eng"] = "-";
									$dataStructure[$skk . "_chi"] = "-";
								} else {
									$dataStructure[$skk] = "-";
								}
								break;
						}
						
					}
					unset($tmp_dataStructure);
					$strSQL = "SELECT UT.UserID, UserLogin, EnglishName, ChineseName, FTDataID, DT.FTDataRowID, FTData, FTDataChi, FT.FieldLabel, FT.FieldNameChi, FT.FieldNameEn, FT.FieldType, FT.isMultiLang";
					$strSQL .= " FROM " . $this->dataTable . " as DT";
					$strSQL .= " JOIN " . $this->dataGridTable . " as DGT on (DGT.FTDataRowID=DT.FTDataRowID)";
					$strSQL .= " JOIN " . $this->userTable . " as UT on (UT.UserID=DGT.UserID)";
					$strSQL .= " JOIN " . $this->relationTable . " as RL on (DT.RelationID=RL.RelationID AND RL.FormTemplateID='" . $this->Get_Safe_Sql_Query($templateID) . "')";
					$strSQL .= " JOIN " . $this->fieldTable . " as FT on (FT.FieldID=RL.FieldID)";
					if ($UserID != "all" && $UserID > 0 && !$this->coreData["userInfo"]["Role"]["isAdminMode"]) {
						$strSQL .= " WHERE UT.UserID='" . $UserID . "'";
					}
					$strSQL .= " ORDER BY UT.UserID, DT.FTDataRowID ASC";
					$data = $this->returnDBResultSet($strSQL);
					$rowData = array();
					if ($data != null && count($data) > 0) {
						$j = 0;
						$i = 0;
						foreach ($this->coreData["formTplInfo"]["childs"] as $kk => $vv) {
							if ($vv["isMultiLang"] == "Y") {
								$rowData[$i][$j] = $vv["FieldNameEn"] . " (" . $this->coreData["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
								$j++;
								$rowData[$i][$j] = $vv["FieldNameEn"] . " (" . $this->coreData["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
							} else {
								$rowData[$i][$j] = $vv["FieldNameEn"];
							}
							$j++;
						}
						$i++;
						$currRowID = 0;
						$finalData = array();
						foreach ($data as $kk => $vv) {
							if ($currRowID != $vv["FTDataRowID"]) {
								if ($currRowID > 0) {
									$rowData[$i] = array_values($finalData);
									$finalData = array();
									$i++;
								}
								$currRowID = $vv["FTDataRowID"];
							}
							if (count($finalData) == 0) $finalData = $dataStructure;
							if (isset($finalData["UserID"])) $finalData["UserID"] = $vv["UserLogin"];
							if (isset($finalData["TeacherName_eng"])) $finalData["TeacherName_eng"] = $vv["EnglishName"];
							if (isset($finalData["TeacherName_chi"])) $finalData["TeacherName_chi"] = $vv["ChineseName"];
							if (isset($finalData["FTDataRowID"])) $finalData["FTDataRowID"] = $vv["FTDataRowID"];
							if (isset($finalData[$vv["FieldLabel"]])) $finalData[$vv["FieldLabel"]] = $vv["FTData"];
							if ($vv["isMultiLang"] == "Y" && !empty($vv["FTDataChi"])) {
								if (isset($finalData[$vv["FieldLabel"]."_eng"])) $finalData[$vv["FieldLabel"]."_eng"] = $vv["FTData"];
								if (isset($finalData[$vv["FieldLabel"]."_chi"])) $finalData[$vv["FieldLabel"]."_chi"] = $vv["FTDataChi"];
							}
						}
						$rowData[$i] = array_values($finalData);
						$i++;
					}
					if (count($rowData) > 0) {
						$CSVObj->outputData($rowData, strtolower($this->coreData["formTplInfo"]["TemplateName"]));
						exit;
					} else {
						header('Content-type: text/plain; charset=utf-8');
						echo $this->coreData["lang"]["TeacherPortfolio"]["downloadNoRecord"];
						exit;
					}
				}
*/
				header('Content-type: text/plain; charset=utf-8');
				echo $this->coreData["lang"]["TeacherPortfolio"]["downloadNoRecord"];
				exit;
			}
			
			public function printAllData($UserID) {
				
			}

			public function getTempInfoByName($templateName) {
				$result = $this->getTemplateInfo($templateName, "TemplateName");
				return $result;
			}

			public function getTempInfoByID($id) {
				$result = $this->getTemplateInfo($id, "FormTemplateID");
				return $result;
			}

			public function getTemplateInfo($args, $searchBy ='TemplateName') {
				$strSQL = "SELECT " . implode(",", $this->mainParam) . " FROM " . $this->mainTable . " WHERE " . $searchBy . "='" . $this->Get_Safe_Sql_Query($args) . "' LIMIT 1";
				$result = $this->returnDBResultSet($strSQL);
				
				if (count($result) > 0) {
					$result = $result[0];
					$result["childs"] = $this->getChirldByTplID($result["FormTemplateID"]);
					$result["csvHeader"] = $this->getCSVHeader($result["childs"]);
					if (count($result["childs"]) > 0) {
						$result["extChilds"] = $this->getAllFields($result["FormTemplateID"], array_keys($result["childs"]));
					}					
					return $result;
				}
				return null;
			}

			public function getChirldByTplID($id) {
				$param = array( "RL.FieldID", "RelationID", "FieldLabel", "FieldNameChi", "FieldNameEn", "FieldType", "SampleData", "SampleDataChi", "isMultiLang", "showInGrid", "isUsed", "FixedItem", "FieldStatus", "Priority");
				$strSQL = "SELECT " . implode(", ", $param) . " FROM " . $this->relationTable . " AS RL";
				$strSQL .= " JOIN " . $this->fieldTable . " AS FT ON (RL.FieldID=FT.FieldID)";
				$strSQL .= " WHERE RL.FormTemplateID='" . $this->Get_Safe_Sql_Query($id) . "' AND isUsed='Y'";
				
				if ($this->coreData["template"] == 'CPD') {
				    if(!$this->coreData["userInfo"]["customization"]['useEDBCPD']){
				        $strSQL .= " AND FT.FieldLabel NOT IN ('Content', 'Organization', 'CPDMode', 'CPDDomain', 'SubjectRelated')";
				    }
				    else if (!$this->coreData['userInfo']["customization"]["cpdSubjectTag"] ){
				        $strSQL .= " AND FT.FieldLabel NOT IN ('SubjectRelated')";
				    }
				}
				$strSQL .= " ORDER BY FixedItem DESC, Priority ASC";
				
				$result = $this->returnDBResultSet($strSQL);
				if (count($result) > 0) {
					$result = $this->convertArrKeyToID($result, "FieldLabel");					
					return $result;
				}
				return NULL;
			}

			public function getAllFields($id, $fieldIDs = array()) {
				$param = array( "RL.FieldID", "RelationID", "FieldLabel", "FieldNameChi", "FieldNameEn", "FieldType", "SampleData", "SampleDataChi", "isMultiLang", "showInGrid", "isUsed", "FixedItem", "FieldStatus", "Priority");
				$strSQL = "SELECT " . implode(", ", $param) . " FROM " . $this->relationTable . " AS RL";
				$strSQL .= " JOIN " . $this->fieldTable . " AS FT ON (RL.FieldID=FT.FieldID)";
				$strSQL .= " WHERE FormTemplateID='" . $this->Get_Safe_Sql_Query($id) . "'";
				if (count($fieldIDs) > 0) {
					$strSQL .= " AND FieldLabel NOT IN ('" . implode("', '", $fieldIDs) . "')";
				}
				$strSQL .= " ORDER BY FixedItem DESC, Priority ASC";
				$result = $this->returnDBResultSet($strSQL);
				if (count($result) > 0) {
					$result = $this->convertArrKeyToID($result, "FieldLabel");
					return $result;
				}
				return NULL;
			}

			public function validImportData($userArr, $csvData, $fieldsArr) {
				$duplicateMD5 = array();
				$resData = array();
				if (count($csvData) > 0) {
					if (count($csvData) > 0) {
						foreach ($csvData as $kk => $rowData) {
							if (count($rowData) > 0) {
								$single_duplicateMD5 = md5(implode(", ", $rowData));
								if (isset($duplicateMD5[$single_duplicateMD5])) {
									$duplicateMD5[$single_duplicateMD5]++;
								} else {
									$duplicateMD5[$single_duplicateMD5] = 1;
								}
								$i = 0;
								$hasError = false;
								$errTd = array();
								$errTdMsg = array();
								foreach ($rowData as $colid => $colData) {
									$tdError = false;
									$resData[$kk][$fieldsArr[$i]["Label"]] = $colData;									
									$tdData = trim($colData);
																		
									if($fieldsArr[$i]["Label"] == "TeacherName_chi"){
										$queryData = array(
											"param" => "ChineseName",
											"table" => $this->defaultUserTable,
											"condition" => " UserID='".IntegerSafe($userArr[$resData[$kk]["UserID"]])."' "
										);
										$userName = current($this->returnVector($this->buildSelectSQL($queryData)));
										if($userName != $colData){
											$resData[$kk][$fieldsArr[$i]["Label"]] = $userName;
										}
										
									} elseif($fieldsArr[$i]["Label"] == "TeacherName_eng"){
										$queryData = array(
											"param" => "EnglishName",
											"table" => $this->defaultUserTable,
											"condition" => " UserID='".IntegerSafe($userArr[$resData[$kk]["UserID"]])."' "
										);
										$userName = current($this->returnVector($this->buildSelectSQL($queryData)));
										if($userName != $colData){
											$resData[$kk][$fieldsArr[$i]["Label"]] = $userName;
										}
									}
									
									if ($fieldsArr[$i]["Label"] == "UserID") {
										if (empty($tdData) || !isset($userArr[$tdData])) {
											$tdError = true;
											$hasError = true;
											$errorCode = 3;
											$errTd[] = $i;
											$errTdMsg[] = "";
										}
									}elseif($fieldsArr[$i]["Label"] == "AcademicYear"){										
										$yearData = $this->getAcademicYear($tdData,true,true);
										if(empty($yearData) || $tdData==""){
											$tdError = true;
											$hasError = true;
											$errorCode = 1;
											$errTd[] = $i;
											$errTdMsg[] = $this->coreData["lang"]["TeacherPortfolio"]["noThisRecord"];
										}
									}elseif($fieldsArr[$i]["Label"] == "StartDate"){
										$idxAcademicYear = -1;
										$idxEndDate = -1;
										foreach($fieldsArr as $fieldKey=>$fieldContent){
											if($fieldContent["Label"]=="AcademicYear"){
												$idxAcademicYear = $fieldKey;
											}
											if($fieldContent["Label"]=="EndDate"){
												$idxEndDate = $fieldKey;
											}
											if($idxAcademicYear >= 0 && $idxEndDate >=0){
												break;
											}
										}
										$academciYear = $rowData[$fieldsArr[$idxAcademicYear]["FieldNameEn"]];
										$startDate = getDefaultDateFormat($tdData);
										$endDate = getDefaultDateFormat($rowData[$fieldsArr[$idxEndDate]["FieldNameEn"]]);
										$yearData = $this->getAcademicYear($academciYear,true,true);
										if(strtotime($yearData[$academciYear]['YearStart']) > strtotime($startDate) || strtotime($yearData[$academciYear]['YearEnd']) < strtotime($startDate)){
											$tdError = true;
											$hasError = true;
											$errorCode = 1;
											$errTd[] = $i;
											$errTdMsg[] = $this->coreData["lang"]["TeacherPortfolio"]["dateInYear"];
										}
									}elseif($fieldsArr[$i]["Label"] == "EndDate"){
										$idxAcademicYear = -1;
										$idxStartDate = -1;
										foreach($fieldsArr as $fieldKey=>$fieldContent){
											if($fieldContent["Label"]=="AcademicYear"){
												$idxAcademicYear = $fieldKey;
											}
											if($fieldContent["Label"]=="StartDate"){
												$idxStartDate = $fieldKey;
											}
											if($idxAcademicYear >= 0 && $idxStartDate >=0){
												break;
											}
										}
										$academciYear = $rowData[$fieldsArr[$idxAcademicYear]["FieldNameEn"]];
										$endDate = getDefaultDateFormat($tdData);
										$startDate = getDefaultDateFormat($rowData[$fieldsArr[$idxStartDate]["FieldNameEn"]]);
										$yearData = $this->getAcademicYear($academciYear,true,true);
										if(strtotime($yearData[$academciYear]['YearStart']) > strtotime($endDate) || strtotime($yearData[$academciYear]['YearEnd']) < strtotime($endDate)){
											$tdError = true;
											$hasError = true;
											$errorCode = 1;
											$errTd[] = $i;
											$errTdMsg[] = $this->coreData["lang"]["TeacherPortfolio"]["dateInYear"];
										}else if(strtotime($endDate) <= strtotime($startDate)){
											$tdError = true;
											$hasError = true;
											$errorCode = 1;
											$errTd[] = $i;
											$errTdMsg[] = $this->coreData["lang"]["TeacherPortfolio"]["dateRange"];
										}
									}elseif($fieldsArr[$i]["Label"] == "NoOfSection"){
										if (floatval($tdData) <=0 ) {
											$tdError = true;
											$hasError = true;
											$errorCode = 1;
											$errTd[] = $i;
											$errTdMsg[] = $this->coreData["lang"]["TeacherPortfolio"]["err_cpdhour_negative"];
										}else{
											$idxEndDate = -1;
											$idxStartDate = -1;
											foreach($fieldsArr as $fieldKey=>$fieldContent){
												if($fieldContent["Label"]=="EndDate"){
													$idxEndDate = $fieldKey;
												}
												if($fieldContent["Label"]=="StartDate"){
													$idxStartDate = $fieldKey;
												}
												if($idxEndDate >= 0 && $idxStartDate >=0){
													break;
												}
											}
											$endDate = getDefaultDateFormat($rowData[$fieldsArr[$idxStartDate]["FieldNameEn"]]);
											$startDate = getDefaultDateFormat($rowData[$fieldsArr[$idxStartDate]["FieldNameEn"]]);
											$hourDiff = round((strtotime($endDate." 23:59:59") - strtotime($startDate." 00:00:00"))/3600, 1);
											if($hourDiff < floatval($tdData)){
												$tdError = true;
												$hasError = true;
												$errorCode = 1;
												$errTd[] = $i;
												$errTdMsg[] = $this->coreData["lang"]["TeacherPortfolio"]["err_cpdhour_exceed"];
											}
										}
										
									}elseif ($fieldsArr[$i]["Label"] == "CPDMode") {
										if (empty($tdData)) $tdData = 0;
										else if (!is_numeric($tdData)) {
											$tdError = true;
											$hasError = true;
											$errorCode = 1;
											$errTd[] = $i;
											$errTdMsg[] = $this->coreData["lang"]["TeacherPortfolio"]["cpd_mode_import_error"];
										}
										else if (intval($tdData)!=0 && intval($tdData)!=1) {
											$tdError = true;
											$hasError = true;
											$errorCode = 1;
											$errTd[] = $i;
											$errTdMsg[] = $this->coreData["lang"]["TeacherPortfolio"]["cpd_mode_import_error"];
										}
									} elseif ($fieldsArr[$i]["Label"] == "CPDDomain") {
										if(!empty($tdData)){
											$error = false;
											$domain = explode(",",$tdData);
											foreach($domain as $d){
												if($d!=0 && $d!=1){
													$tdError = true;
													$hasError = true;
													$errorCode = 1;
													$errTd[] = $i;
													$errTdMsg[] = $this->coreData["lang"]["TeacherPortfolio"]["cpd_domain_import_error"];
													break;
												}
											}
										}
									} elseif ($fieldsArr[$i]["Label"] == "SubjectRelated") {
										if(!empty($tdData)){
											$error = false;
											$domain = explode(",",$tdData);
											foreach($domain as $d){
												if($d!=0 && $d!=1){
													$tdError = true;
													$hasError = true;
													$errorCode = 1;
													$errTd[] = $i;
													$errTdMsg[] = $this->coreData["lang"]["TeacherPortfolio"]["cpd_subject_import_error"];
													break;
												}
											}
										}
									} else {
										switch (strtoupper($fieldsArr[$i]["FieldType"])) {
											case "FLOAT" :
											case "INT" :
												if (empty($tdData)) $tdData = 0;
												else if (!is_numeric($tdData)) {
													$tdError = true;
													$hasError = true;
													$errorCode = 1;
													$errTd[] = $i;
													$errTdMsg[] = "";
												}
												break;
											default:
												// if (empty($tdData)) $tdData = "-";
												break;
										}
									}
									if ($fieldsArr[$i]["FieldStatus"] == "required" && empty($tdData) && $tdData !== 0) {
										$hasError = true;
										$tdError = true;
										$errorCode = 2;
										$errTd[] = $i;
										$errTdMsg[] = "";
									}
									$i++;
								}
								$resData[$kk]["_cust_"] = array();
								$resData[$kk]["_cust_"]["md5"] = $single_duplicateMD5;
								$resData[$kk]["_cust_"]["md5_norec"] = $duplicateMD5[$single_duplicateMD5];
								$resData[$kk]["_cust_"]["index"] = $kk;
								if ($hasError) {
									$resData[$kk]["_cust_"]["valid"] = "N";
									$resData[$kk]["_cust_"]["error"] = $errTd;
									if(!isset($resData[$kk]["_cust_"]["errorMsg"])){
										$resData[$kk]["_cust_"]["errorMsg"] = array();
									}
									$resData[$kk]["_cust_"]["errorMsg"] = $errTdMsg;
								} else {
									$resData[$kk]["_cust_"]["valid"] = "Y";
									$resData[$kk]["_cust_"]["error"] = "NULL";
								}
							}
						}
					}
				}
				return $resData;
			}

			public function dataImport($processUserID, $userIDs, $ImportData = array()) {
				$totalImported = 0;
				$loginID = array();
				$fieldArr = $this->coreData["formTplInfo"]["childs"];
				if (count($ImportData) > 0) {
					foreach ($ImportData as $kk => $rowData) {
						$selectedUserID = $userIDs[$rowData["UserID"]];
						if (isset($this->coreData["TemplateGridTableStructure"])) {
							$data_param = $this->coreData["TemplateGridTableStructure"];
							$data_param["FormTemplateID"]["data"] = $this->coreData["formTplInfo"]["FormTemplateID"];
							$data_param["UserID"]["data"] = $selectedUserID;
							switch ($this->coreData["template"]) {
								case "ACTIVITIES" :
									$data_param["AcademicYear"]["data"] = $rowData["AcademicYear"];
									$data_param["SemesterEn"]["data"] = $rowData["Semester_eng"];
									$data_param["SemesterChi"]["data"] = $rowData["Semester_chi"];
									$data_param["Hours"]["data"] = $rowData["Hours"];
									$data_param["EventNameEn"]["data"] = $rowData["EventName_eng"];
									$data_param["EventNameChi"]["data"] = $rowData["EventName_chi"];
									$data_param["RemarksEn"]["data"] = $rowData["Remarks_eng"];
									$data_param["RemarksChi"]["data"] = $rowData["Remarks_chi"];
									break;
								case "TEACHING_RECORD":
									$data_param["AcademicYear"]["data"] = $rowData["AcademicYear"];
									$data_param["ClassNameEn"]["data"] = $rowData["ClassName_eng"];
									$data_param["ClassNameChi"]["data"] = $rowData["ClassName_chi"];
									$data_param["SubjectEn"]["data"] = $rowData["Subject_eng"];
									$data_param["SubjectChi"]["data"] = $rowData["Subject_chi"];
									$data_param["NoOfSection"]["data"] = $rowData["NoOfSection"];
									$data_param["RemarksEn"]["data"] = $rowData["Remarks_eng"];
									$data_param["RemarksChi"]["data"] = $rowData["Remarks_chi"];
									break;
								case "CPD":
									if(strstr($rowData["StartDate"],'/') || strstr($rowData["EndDate"],'/')){
										// handle case of importing date in format dd/mm/yy
										$startDateArr = explode('/',$rowData["StartDate"]);
										$startDate = $startDateArr[2]."-".$startDateArr[1]."-".$startDateArr[0];
										$endDateArr = explode('/',$rowData["EndDate"]);
										$endDate = $endDateArr[2]."-".$endDateArr[1]."-".$endDateArr[0];
									}else{
										$startDate = $rowData["StartDate"];
										$endDate = $rowData["EndDate"];
									}	
									
									$data_param["AcademicYear"]["data"] = $rowData["AcademicYear"];									
									$data_param["StartDate"]["data"] = date("Y-m-d",strtotime($startDate));
									$data_param["EndDate"]["data"] = date("Y-m-d",strtotime($endDate));
									$data_param["NoOfSection"]["data"] = $rowData["NoOfSection"];
									$data_param["EventNameEn"]["data"] = $rowData["EventName_eng"];
									$data_param["EventNameChi"]["data"] = $rowData["EventName_chi"];
									$data_param["EventTypeEn"]["data"] = $rowData["EventType_eng"];
									$data_param["EventTypeChi"]["data"] = $rowData["EventType_chi"];
									$data_param["RemarksEn"]["data"] = $rowData["Remarks_eng"];
									$data_param["RemarksChi"]["data"] = $rowData["Remarks_chi"];
									if($this->coreData["userInfo"]["customization"]['useEDBCPD']){
										$data_param["Content"]["data"] = $rowData["Content"];									
										$data_param["Organization"]["data"] = $rowData["Organization"];
										$data_param["CPDMode"]["data"] = $rowData["CPDMode"];
										$data_param["CPDDomain"]["data"] = $rowData["CPDDomain"];
										if($this->coreData['userInfo']["customization"]["cpdSubjectTag"] ){
											$data_param["SubjectRelated"]["data"] = $rowData["SubjectRelated"];
										}
									}
									break;
								case "ATTENDANCE":
									$data_param["AcademicYear"]["data"] = $rowData["AcademicYear"];
									$data_param["Month"]["data"] = $rowData["Month"];
									$data_param["Attend"]["data"] = $rowData["Attend"];
									$data_param["Late"]["data"] = $rowData["Late"];
									$data_param["SickLeave"]["data"] = $rowData["SickLeave"];
									$data_param["CasualLeave"]["data"] = $rowData["CasualLeave"];
									$data_param["AnnualLeave"]["data"] = $rowData["AnnualLeave"];
									break;
							}
							$data_param["DateInput"]["data"] = "__dbnow__";
							$data_param["InputBy"]["data"] =$processUserID;
							$data_param["DateModified"]["data"] = "__dbnow__";
							$data_param["ModifyBy"]["data"] = $processUserID;
							
						} else {
							$data_param = array(
								"FormTemplateID" => array( "field" => "FormTemplateID", "data" => $this->coreData["formTplInfo"]["FormTemplateID"]),
								"UserID" => array( "field" => "UserID", "data" => $selectedUserID),
								"DateInput" => array( "field" => "DateInput", "data" => "__dbnow__"),
								"InputBy" => array( "field" => "InputBy", "data" => $processUserID),
								"DateModified" => array( "field" => "DateModified", "data" => "__dbnow__"),
								"ModifyBy" => array( "field" => "ModifyBy", "data" => $processUserID)
							);
						}
						$strSQL = $this->buildInsertSQL($this->dataGridTable, array_keys($data_param), $data_param);
						$result = $this->DBdb_query($strSQL);
						if (!$result) {
							$isError = true;
							$FTDataRowID = 0;
						} else {
							$FTDataRowID = $this->db_insert_id();
							$totalImported++;
						}
						if (!isset($this->coreData["TemplateType"])) {
							$data_param = array();
							if ($FTDataRowID > 0) {
								foreach ($rowData as $colID => $colData) {
									$isChinese = false;
									$keyObj = $colID;
									$FTData = $colData;
									if ($colID == "UserID") {
										$FTData = $userIDs[$colData];
									}
									if (!isset($fieldArr[$colID])) {
										$keyObj = str_lreplace("_eng", "", $colID);
										if ($keyObj == $colID) {
											$keyObj = str_lreplace("_chi", "", $colID);
											if ($keyObj != $colID) {
												// is Chinese
												$isChinese = true;
											}
										} else {
											// is English
										}
									}
									if ($keyObj != "_cust_") {
										$RelationID = $fieldArr[$keyObj]["RelationID"];
										if ($RelationID > 0 && $fieldArr[$keyObj]["FieldStatus"] != "reference") {
											if (!isset($data_param[$RelationID])) {
												$data_param[$RelationID] = array(
													"FTDataRowID" => array( "field" => "FTDataRowID", "data" => $FTDataRowID),
													"RelationID" => array( "field" => "RelationID", "data" => $RelationID),
													"FTData" => array( "field" => "RelationID", "data" => ""),
													"FTDataChi" => array( "field" => "FTDataChi", "data" => ""),
													"DateInput" => array( "field" => "DateInput", "data" => "__dbnow__"),
													"InputBy" => array( "field" => "InputBy", "data" => $processUserID),
													"DateModified" => array( "field" => "DateModified", "data" => "__dbnow__"),
													"ModifyBy" => array( "field" => "ModifyBy", "data" => $processUserID)
												);
											}
											if ($fieldArr[$keyObj]["isMultiLang"] == "N"){
												$data_param[$RelationID]["FTDataChi"] = array( "field" => "FTDataChi", "data" => $FTData );
												$data_param[$RelationID]["FTData"] = array( "field" => "FTData", "data" => $FTData );
											} else {
												if ($isChinese) {
													$data_param[$RelationID]["FTDataChi"] = array( "field" => "FTDataChi", "data" => $FTData );
												} else {
													$data_param[$RelationID]["FTData"] = array( "field" => "FTData", "data" => $FTData );
												}
											}
										}
									}
								}
								if (count($data_param) > 0) {
									foreach ($data_param as $dkk => $dvv) {
										$strSQL = $this->buildInsertSQL($this->dataTable, array_keys($dvv), $dvv);
										$result = $this->DBdb_query($strSQL);
									}
								}
							}
						}
					}
				}
				if ($totalImported > 0) {
					$logDetail = "Data Import: ";
					$logDetail .= "Total " . $totalImported . " Record(s)";
					$this->core->load_model('LogObj');
					$this->core->LogObj->addRecord($processUserID, str_replace("_", " ", strtoupper($this->coreData["template"])), $logDetail);
				}
				return $totalImported;
			}

			public function deleteRecord($ids, $userID = 0) {
				$condition = "";
				if (is_array($ids)) {
					if (count($ids) > 0) {
						$condition = "FTDataRowID IN ('" . implode("', '", $ids) . "')";
					}
				} else if ($ids > 0) {
					$condition = "FTDataRowID='" . $this->Get_Safe_Sql_Query($ids) . "'";
				}
				if ($condition != "") {
					$strSQL = "SELECT dgt.*, iu.EnglishName, iu.ChineseName, iu.UserLogin FROM " . $this->dataGridTable . " AS dgt";
					$strSQL .= " JOIN " . $this->userTable . " AS iu ON (dgt.UserID=iu.UserID)";
					$strSQL .= " WHERE " . $condition;
					$result = $this->returnDBResultSet($strSQL);
					if (count($result) > 0) {
						$logDetail = "Delete Record:<div class='log_detail'>";
						foreach ($result as $kk => $vv) {
							switch (strtoupper($this->coreData["template"])) {
								case "TEACHING_RECORD":
									$logDetail_con .= "<small>" . $vv["EnglishName"] . " / " . $vv["AcademicYear"] . " / " . $vv["ClassNameEn"] . " / " . $vv["SubjectEn"] . "</small><br>";
									break;
								case "ATTENDANCE":
									$logDetail_con .= "<small>" . $vv["EnglishName"] . " / " . $vv["AcademicYear"] . " / " . $vv["Month"] . "</small><br>";
									break;
								case "ACTIVITIES":
									$logDetail_con .= "<small>" . $vv["EnglishName"] . " / " . $vv["AcademicYear"] . " / " . $vv["SemesterEn"] . " / " . $vv["EventNameEn"] . "</small><br>";
									break;
								case "CPD":
									$logDetail_con .= "<small>" . $vv["EnglishName"] . " / " . $vv["AcademicYear"] . " / " . $vv["EventNameEn"] . " / " . $vv["EventTypeEn"] . " / " . $vv["StartDate"] . " - " . $vv["EndDate"] . "</small><br>";
									break;
							}
					
						}
						$logDetail .= $logDetail_con . "</div>";
						$this->core->load_model('LogObj');
						$this->core->LogObj->addRecord($userID, str_replace("_", " ", strtoupper($this->coreData["template"])), $logDetail);
					}
					
					$strSQL = "DELETE FROM " . $this->dataGridTable;
					$strSQL .= " WHERE " . $condition;
					$result = $this->DBdb_query($strSQL);

					$strSQL = "DELETE FROM " . $this->dataTable;
					$strSQL .= " WHERE " . $condition;
					$result = $this->DBdb_query($strSQL);
					
					if ($this->coreData["TemplateType"] != "GRID") {
						$strSQL = "DELETE FROM " . $this->dataTable;
						$strSQL .= " WHERE FTDataRowID NOT IN (SELECT FTDataRowID FROM " . $this->dataGridTable . ")";
						$result = $this->DBdb_query($strSQL);
						
						$strSQL = "DELETE FROM " . $this->dataGridTable;
						$strSQL .= " WHERE FTDataRowID NOT IN (SELECT FTDataRowID FROM " . $this->dataTable . ")";
						$result = $this->DBdb_query($strSQL);
					}
					return true;
				}
				return false;
			}

			public function updateSortRecords($ids) {
				if ($ids != "") {
					$formTplInfo = $this->coreData["formTplInfo"];
					$idArr = explode(",", $ids);
					if (count($idArr) > 0) {
						$strSQL = "UPDATE " . $this->relationTable . " SET isUsed='N' WHERE FormTemplateID='" . $formTplInfo["FormTemplateID"] . "' AND RelationID NOT IN (" . $ids . ")";
						$result = $this->DBdb_query($strSQL);
						$Priority = 1;
						foreach ($idArr as $index => $valID) {
							$strSQL = "UPDATE " . $this->relationTable . " SET isUsed='Y', Priority='" . $Priority . "' WHERE FormTemplateID='" . $formTplInfo["FormTemplateID"] . "' AND RelationID='" . $this->Get_Safe_Sql_Query($valID) . "'";
							$result = $this->DBdb_query($strSQL);
							$Priority++;
						}
						return true;
					}
				}
				return false;
			}
		}
	}
}