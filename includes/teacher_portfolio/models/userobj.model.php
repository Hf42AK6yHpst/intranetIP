<?php
/*
 *  2020-05-19 Cameron
 *      - retrieve PersonalPhotoLink by call function in libuser instead of directly retrieved from SQL, mainly cater for default image
 */
if (defined('TEACHER_PORTFOLIO')) {
	if (!class_exists("CommonLibdb")) include_once(dirname(__FILE__) . "/commonlibdb.model.php");
	if (!class_exists("UserObj")) {
		class UserObj extends CommonLibdb {
			public function __construct() {
				parent::__construct();
				$this->primaryKey = "UserID";
				$this->tableName = $this->defaultUserTable;
				$this->userPermissionTable = "INTRANET_TEACHER_PORTFOLIO_USER";
				$this->userKey = "!$%^&:986yh";
			}
			
			public function getProfile($UserInfo) {
			    global $intranet_root;
				$profileArr = array();
				$sqlFieldParam = $this->primaryKey . ", EnglishName, ChineseName, Gender, DateOfBirth, UserEmail, Address, HomeTelNo, MobileTelNo";
				$personal_photo_link = '';
				if(!$this->isEJ){
//					$sqlFieldParam .= ", PersonalPhotoLink";

				    include_once($intranet_root."/includes/libuser.php");
				    $li = new libuser($UserInfo["info"]["SelectedUserID"]);
					$personal_photo_link = $li->returnDefaultPersonalPhotoPath();
					$personal_photo_delete = "";
					if ($li->PersonalPhotoLink !="")
					{
					    if (is_file($intranet_root.$li->PersonalPhotoLink))
					    {
					        $personal_photo_link = $li->PersonalPhotoLink;
					    }
					}

				}
				$sqlParam = array(
					"param" => $sqlFieldParam,
					"table" => $this->tableName,
					"condition" => $this->primaryKey . "='" . $this->Get_Safe_Sql_Query($UserInfo["info"]["SelectedUserID"]) . "' AND Teaching='1' and RecordType='1'",
					"order" => null,
					"limit" => 1
				);
				$strSQL = $this->buildSelectSQL($sqlParam);
				if (!empty($strSQL)) {
					$result = $this->returnDBResultSet($strSQL);
					if ($result != null) {
						$profileArr = $result[0];
						$profileArr["DateOfEntry"] = "-";
						$profileArr["Position"] = "-";
						$profileArr["PersonalPhotoLink"] = $personal_photo_link;
					}
				}
				return $profileArr;
			}
			
			public function getUserIDByNames($userArr) {
				$UserIDs = array();
				if (count($userArr) > 0) {
					$sqlParam = array(
						"param" => $this->primaryKey . ", UserLogin",
						"table" => $this->tableName,
						"condition" => "UserLogin in ('" . implode("', '", array_keys($userArr)) . "') AND Teaching='1' and RecordType='1'"
					);
					$strSQL = $this->buildSelectSQL($sqlParam);
					if (!empty($strSQL)) {
						$result = $this->returnDBResultSet($strSQL);
						if (count($result) > 0) {
							foreach ($result as $index => $REC) {
								$UserIDs[$REC["UserLogin"]] = $REC["UserID"];
							}
						}
					}
				}
				return $UserIDs;
			}
			
			public function getInfoByID($id) {
				$TeachersInfo = NULL;
				$sqlParam = array(
						"param" => $this->primaryKey . ", UserLogin, EnglishName, ChineseName",
						"table" => $this->tableName,
						"condition" => "Teaching='1' and RecordType='1' and UserID='" . $this->Get_Safe_Sql_Query($id) . "'"
					);
				$strSQL = $this->buildSelectSQL($sqlParam);
				if (!empty($strSQL)) {
					$result = $this->returnDBResultSet($strSQL);
					if (count($result) > 0) {
						foreach ($result as $index => $REC) {
							$TeachersInfo[$REC[$this->primaryKey]] = $REC;
							$TeachersInfo[$REC[$this->primaryKey]]["cs"] =  $this->user_eyt($REC); // decrypt
						}
					}
				}
				return $TeachersInfo;
			}
			
			public function exportAllData($CSVObj, $userInfo) {
				$i = 0;
				$rowData[$i] = array(
					$this->core->data["lang"]["TeacherPortfolio"]["info_ChineseName"],
					$this->core->data["lang"]["TeacherPortfolio"]["info_EnglishName"],
					$this->core->data["lang"]["TeacherPortfolio"]["info_Gender"],
					$this->core->data["lang"]["TeacherPortfolio"]["info_DateOfEntry"],
					$this->core->data["lang"]["TeacherPortfolio"]["info_Position"],
					$this->core->data["lang"]["TeacherPortfolio"]["info_MobileTel"],
					$this->core->data["lang"]["TeacherPortfolio"]["info_HomeTel"],
					$this->core->data["lang"]["TeacherPortfolio"]["info_Email"],
					$this->core->data["lang"]["TeacherPortfolio"]["info_Address"]
				);
				$i++;
				$profile = $this->getProfile($userInfo);
				if (count($profile) > 0) {
					$rowData[$i] = array(
						$profile["ChineseName"],
						$profile["EnglishName"],
						$this->core->data["lang"]['TeacherPortfolio']["info_GenderOpt"][$profile["Gender"]],
						"",
						"",
						$profile["MobileTelNo"],
						$profile["HomeTelNo"],
						$profile["UserEmail"],
						$profile["Address"]
					);
				}
				if (count($rowData) > 0) {
					$CSVObj->outputData($rowData, strtolower("profile"));
					exit;
				}
			}
			
			public function getAllTeacher() {
				$TeachersInfo = NULL;
				$sqlParam = array(
						"param" => $this->primaryKey . ", UserLogin, EnglishName, ChineseName",
						"table" => $this->tableName,
						"condition" => "Teaching='1' and RecordType='1'",
						"order" => "EnglishName ASC"
					);
				$strSQL = $this->buildSelectSQL($sqlParam);
				if (!empty($strSQL)) {
					$result = $this->returnDBResultSet($strSQL);
					if (count($result) > 0) {
						foreach ($result as $index => $REC) {
							$TeachersInfo[$REC[$this->primaryKey]] = $REC;
							$TeachersInfo[$REC[$this->primaryKey]]["cs"] =  $this->user_eyt($REC); // decrypt
						}
					}
				}
				return $TeachersInfo;
			}
			
			public function user_eyt($info) {
				if (is_array($info) && count($info) > 0) {
					return md5(implode("_", $info) . "_" . $this->userKey);
				}
				return NULL;
			}
			
			public function getDataTableArray($postData) {
				$data = array();
				$jsonData = array(
					"recordsTotal" => 0,
					"recordsFiltered" => 0,
					"allowExpand" => false,
					"data" => $data
				);
				$recordsTotal = 0;
				$recordsFiltered = 0;
				
				$sortField = $postData["columns"][$postData["order"][0]["column"]]["data"];
				switch ($sortField) {
					default: 
						$order_field = Get_Lang_Selection("ChineseName", "EnglishName");
						break;
				}
				$order_dir = "ASC";
				if (isset($postData["order"][0]["dir"])) {
					switch (strtoupper($postData["order"][0]["dir"])) {
						case "DESC":
							$order_dir = strtoupper($postData["order"][0]["dir"]);
							break;
					}
				}
				$queryData = array();
				
				$strSQL = "SELECT COUNT(TPUserID) AS totalrecord";
				$strSQL_st = " FROM " . $this->userPermissionTable . " AS upt";
				$strSQL_st .= " JOIN " . $this->tableName . " AS ut ON (ut.UserID=upt.UserID)";
				$strSQL .= $strSQL_st;
				$totalRec = $this->returnDBResultSet($strSQL);
				if ($totalRec != NULL && count($totalRec) > 0) {
					$recordsTotal = $totalRec[0]["totalrecord"] ? $totalRec[0]["totalrecord"] : 0;
					$recordsFiltered = $totalRec[0]["totalrecord"] ? $totalRec[0]["totalrecord"] : 0;

					$strSQL = "SELECT TPUserID, upt.UserID, TPUserType, " . Get_Lang_Selection("ChineseName", "EnglishName") . " as TeacherName";
					$strSQL .= $strSQL_st;
					$strOrder = " ORDER BY " . $order_field . " " . $order_dir;
					$strSQL .= $strOrder;
					/************************************************************************/
					$start = $postData["start"] ? $postData["start"] : 0;
					$pagelimit = $postData["length"] > 0 ? $postData["length"] : 0;
					$strLimit = " LIMIT " . $start . ", " . $pagelimit;
					if ($pagelimit > 0) $strSQL .= $strLimit;
					$result = $this->returnDBResultSet($strSQL);
					if (count($result) > 0) {
						foreach ($result aS $pkey => $RECORD) {
							$queryData[$pkey] = $RECORD;
							$queryData[$pkey]["func"] = '<div class="text-center"><label class="text-left"><input type="checkbox" class="form-control" name="dataid[]" value="' . $RECORD["UserID"] . '"><span class="custom-check"></span></label></div>';
						}
					}
				}

				$jsonData = array(
					"recordsTotal" => $recordsTotal,
					"recordsFiltered" => $recordsFiltered,
					"allowExpand" => false,
					"data" => $queryData
				);
				return $jsonData;
			}
			
			public function getSuggestionUser($userID, $postData) {
				$jsonData = array(
					"result" => "0"
				);
				if (isset($postData["q"]) && !empty($postData["q"])) {
					$sqlParam = array(
							"param" => $this->primaryKey . ", UserLogin, EnglishName, ChineseName",
							"table" => $this->tableName,
							"condition" => "Teaching='1' AND RecordType='1' AND UserLogin like '" . $this->Get_Safe_Sql_Query($postData["q"]) . "%' AND UserID NOT IN (SELECT UserID FROM " . $this->userPermissionTable . ") AND UserID != '" . $userID . "'",
							"order" => "UserLogin ASC"
						);
					$strSQL = $this->buildSelectSQL($sqlParam);
					$result = $this->returnDBResultSet($strSQL);
					if (count($result) > 0) {
						$jsonData["result"] = count($result);
						foreach ($result as $index => $REC) {
							$jsonData["data"][$REC[$this->primaryKey]] = array();
							$jsonData["data"][$REC[$this->primaryKey]]["id"] = $REC["UserID"];
							$jsonData["data"][$REC[$this->primaryKey]]["name"] = Get_Lang_Selection($REC["ChineseName"], $REC["EnglishName"]);
							$jsonData["data"][$REC[$this->primaryKey]]["ul"] = $REC["UserLogin"];
						}
					}
				}
				return $jsonData;
			}
			
			public function userUpdate($userID, $postData) {
				$UserIDs = array();
				if (isset($postData["sUser"]) > 0) {
					$UserIDs = Get_User_Array_From_Common_Choose($postData["sUser"]);
					$UserIDs = array_filter($UserIDs);
				}
				if (count($UserIDs) > 0) {
					$sqlParam = array(
						"param" => $this->primaryKey . ", UserLogin, EnglishName, ChineseName",
						"table" => $this->tableName,
						"condition" => "UserID in (" . implode(', ', $UserIDs) . ")"
					);
					$strSQL = $this->buildSelectSQL($sqlParam);
					$result = $this->returnDBResultSet($strSQL);
					if (count($result) > 0) {
						$logDetail = "Add User(s) : ";
						$logDetail_con = "";
						foreach ($result as $kk => $vv) {
							if (!empty($logDetail_con)) {
								$logDetail_con .= ", ";
							}
							$logDetail_con .= $vv["EnglishName"] . " (" . $vv["UserLogin"] . ")";
						}
						$logDetail .= "<small>" . $logDetail_con . "</small>";
						$this->core->load_model('LogObj');
						$this->core->LogObj->addRecord($userID, "USER ACTION", $logDetail);
					}

					/********* extra field for Insert ***********/
					$cpdParam = array();
					$cpdParam[] = "UserID";
					$cpdParam[] = "TPUserType";
					$cpdParam[] = "DateInput";
					$cpdParam[] = "InputBy";
					$cpdParam[] = "DateModified";
					$cpdParam[] = "ModifyBy";
					
					foreach ( $UserIDs as $uIndex => $uUserID ) {
						$data["UserID"] = array( "field" => "TPUserType", "data" => $uUserID );
						$data["TPUserType"] = array( "field" => "TPUserType", "data" => "viewer");
						$data["DateModified"] = array( "field" => "DateInput", "data" => "__dbnow__");
						$data["ModifyBy"] = array( "field" => "InputBy", "data" => $userID);
						$sqlParam = array(
							"param" => $this->primaryKey,
							"table" => $this->userPermissionTable,
							"condition" => " UserID='" . $this->Get_Safe_Sql_Query($uUserID) . "'"
						);
						$strSQL = $this->buildSelectSQL($sqlParam);
						$result = $this->returnDBResultSet($strSQL);
						if (!(count($result) > 0)) {
							$data["DateInput"] = array( "field" => "DateInput", "data" => "__dbnow__");
							$data["InputBy"] = array( "field" => "InputBy", "data" => $userID);
							$strSQL = $this->buildInsertSQL($this->userPermissionTable, $cpdParam, $data);
						} else {
							$condition = " UserID ='" . $this->Get_Safe_Sql_Query($uUserID) . "'";
							$strSQL = $this->buildUpdateSQL($this->userPermissionTable, $cpdParam, $data, $condition);
						}
					/********* extra field for Insert ***********/
						$result = $this->DBdb_query($strSQL);
					}
				}
			}
			
			public function deleteRecord($userID, $ids) {
				$condition = "";
				if (is_array($ids)) {
					if (count($ids) > 0) {
						$condition = "UserID IN ('" . implode("', '", $ids) . "')";
					}
				} else if ($ids > 0) {
					$condition = "UserID='" . $this->Get_Safe_Sql_Query($id) . "'";
				}
				
				$sqlParam = array(
					"param" => $this->primaryKey . ", UserLogin, EnglishName, ChineseName",
					"table" => $this->tableName,
					"condition" => $condition
				);
				$strSQL = $this->buildSelectSQL($sqlParam);
				$result = $this->returnDBResultSet($strSQL);
				if (count($result) > 0) {
					$logDetail = "Delete User(s) : ";
					$logDetail_con = "";
					foreach ($result as $kk => $vv) {
						if (!empty($logDetail_con)) {
							$logDetail_con .= ", ";
						}
						$logDetail_con .= $vv["EnglishName"] . " (" . $vv["UserLogin"] . ")";
					}
					$logDetail .= "<small>" . $logDetail_con . "</small>";
					$this->core->load_model('LogObj');
					$this->core->LogObj->addRecord($userID, "USER ACTION", $logDetail);
				}
				
				if ($condition != "") {
					$strSQL = "DELETE FROM " . $this->userPermissionTable;
					$strSQL .= " WHERE " . $condition;
					$result = $this->DBdb_query($strSQL);
					return true;
				}
				return false;
			}
		}
	}
}