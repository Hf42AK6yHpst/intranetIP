<?php
if (defined('TEACHER_PORTFOLIO')) {
	if (!class_exists("CommonLibdb")) {
		class CommonLibdb extends libdb {
			public function __construct() {
				global $junior_mck;
				$this->libdb();
				$this->isEJ = $junior_mck;				
				if($this->isEJ){
					$this->defaultUserTable = "INTRANET_USER_UTF8";
				}else{
					$this->defaultUserTable = "INTRANET_USER";
				}
			}

			public function setCore($core) {
				$this->core = $core;
			}

			public function buildSelectSQL($sqlParam) {
				$strSQL = "";
				if (!empty($sqlParam["param"]) && !empty($sqlParam["table"])) {
					$strSQL = "SELECT";
					$strSQL .= " " . $sqlParam["param"];
					$strSQL .= " FROM " . $sqlParam["table"];
					if (isset($sqlParam["condition"]) && $sqlParam["condition"] != null && !empty($sqlParam["condition"])) $strSQL .= " WHERE " . $sqlParam["condition"];
					if (isset($sqlParam["order"]) && $sqlParam["order"] != null && !empty($sqlParam["order"])) $strSQL .= " ORDER BY " . $sqlParam["order"];
					if (isset($sqlParam["limit"]) && $sqlParam["limit"] != null && !empty($sqlParam["limit"])) $strSQL .= " LIMIT " . $sqlParam["limit"];
				}
				return $strSQL;
			}
			
			public function getMappingInfo() {
				return $this->dataMapping;
			}
			
			public function buildInsertSQL($table, $param, $data) {
				$strSQL = "";
				if (!empty($table) && count($param) > 0 && count($data) > 0) {
					$strSQL = "INSERT INTO " . $table . " (" . implode(", ", $param) . ") VALUES (";
					$valueSQL = "";
					
					foreach ($param as $kk => $vv) {
						if (!empty($valueSQL)) {
							$valueSQL .= ", ";
						}
						if ($data[$vv]["data"] == "__dbnow__") {
							$valueSQL .= "NOW()";
						} else {
							$valueSQL .= "'" . $this->Get_Safe_Sql_Query($data[$vv]["data"]) . "'";
						}
					}
					$strSQL .= $valueSQL . ")";
				}
				return $strSQL;
			}
			
			public function buildUpdateSQL($table, $param, $data, $condition) {
				$strSQL = "";
				if (!empty($table) && count($param) > 0 && count($data) > 0 && !empty($condition)) {
					$strSQL = "UPDATE " . $table . " set ";
					$valueSQL = "";
					foreach ($param as $kk => $vv) {
						if (!empty($valueSQL)) {
							$valueSQL .= ", ";
						}
						if ($data[$vv]["data"] == "__dbnow__") {
							$valueSQL .= $vv . "=NOW()";
						} else {
							$valueSQL .= $vv . "='" . $this->Get_Safe_Sql_Query($data[$vv]["data"]) . "'";
						}
					}
					$strSQL .= $valueSQL . "";
					$strSQL .= " WHERE " . $condition;
				}
				return $strSQL;
			}
			
			public function convertArrKeyToID($assArr, $keyname = "ID") {
				$outputArr = array();
				if (count($assArr) > 0) {
					foreach ($assArr as $key => $_RECORD) {
						if (isset($_RECORD[$keyname])) {
							$outputArr[$_RECORD[$keyname]] = $_RECORD;
						}
					}
				}
				return $outputArr;
			}
			
			public function returnDBResultSet($strSQL) {
				$this->core->addQuerySQL($strSQL);
				$result = $this->returnResultSet($strSQL);
				$result = $this->specialHandle($result);
				return $result;
			}
			
			public function specialHandle($result) {
				if (count($result) > 0) {
					foreach($result as $kk => $vv) {
						if (is_array($vv)) {
							$result[$kk] = $this->specialHandle($vv);
						} else {
							$result[$kk] = $this->specialChar($vv);
						}
					}
				}
				return $result;
			}
			
			public function specialChar($vv) {
				$replaceArr = array( "'" => "&#39;", '"' => "&#34;" );
				return str_replace(array_keys($replaceArr), array_values($replaceArr), $vv);
			}

			public function DBdb_query($strSQL) {
				$this->core->addQuerySQL($strSQL);
				return $this->db_db_query($strSQL);
			}
		}
	}
}