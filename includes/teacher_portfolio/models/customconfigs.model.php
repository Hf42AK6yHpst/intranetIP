<?php
/*
 * 	2017-11-10 Cameron
 * 		- add default config: allowOwnerAddOrEditCpd
 */
if (defined('TEACHER_PORTFOLIO')) {
	if (!class_exists("CommonLibdb")) include_once(dirname(__FILE__) . "/commonlibdb.model.php");
	if (!class_exists("CustomConfigs")) {
		class CustomConfigs extends CommonLibdb {
			public function __construct() {
				parent::__construct();
				$this->mainTable = "INTRANET_TEACHER_PORTFOLIO_CUSTOM_CONFIGS";
				$this->defaultConfigs = array(
					"allowOwnerAddOrEditCpd" => "N",
					"allowOwnerAddOrEditPerformance" => "N",
					"allowOwnerAddOrEditCertificate" => "N",
					"allowOwnerAddOrEditOthers" => "N",
				);
			}
			
			public function getDefaultConfigs() {
				return $this->defaultConfigs;
			}
			
			public function getConfigs() {
				$UserID = $this->core->userInfo["info"]["UserID"];
				$CurrDateTime = date("Y-m-d H:i:s");
				$strSQL = "SELECT TPConfigs_Label, TPConfigs_Value FROM " . $this->mainTable;
				$result = $this->returnDBResultSet($strSQL);
				if (count($result) > 0) {
					foreach ($result as $kk => $vv) {
						$TPCustomConfig[$vv["TPConfigs_Label"]] = $vv["TPConfigs_Value"];
					}
				} else {
					$TPCustomConfig = $this->defaultConfigs;
					foreach ($TPCustomConfig as $label => $value) {
						$strSQL = "INSERT INTO " . $this->mainTable . " (TPConfigs_Label, TPConfigs_Value, DateInput, InputBy, DateModified, ModifyBy) VALUES ( ";
						$strSQL .= "'" . $label . "', '" . $value . "', '" . $CurrDateTime . "', '" . $UserID . "', '" . $CurrDateTime . "', '" . $UserID . "'";
						$strSQL .= " ) ";
						$result = $this->DBdb_query($strSQL);
					}
				}
				return $TPCustomConfig;
			}
			
			public function saveConfig($postData) {
				$output = array(
					"result" => "success",
					"data" => array( "msg" => $this->core->data['lang']['TeacherPortfolio']["UpdateSuccess"], "noaction" => "Y" ),
					"updateConfig" => array()
				);
				$UserID = $this->core->userInfo["info"]["UserID"];
				$CurrDateTime = date("Y-m-d H:i:s");
				
				$default = $this->getDefaultConfigs();
				unset($postData["k"]);
				unset($postData["tp"]);
				$diff1 = array_diff_key($default, $postData);
				if (count($diff1) > 0) {
					$update_params = array_merge($diff1, $postData);
				} else {
					$update_params = $postData;
				}
				if (count($update_params) > 0) {
					$str_up = "";
					foreach ($update_params as $label => $val) {
						if ($val != "Y") {
							$update_params[$label] = "N";
							$val = "N";
							$str_val = "NO";
						} else {
							$str_val = "YES";
						}
						if (!empty($str_up)) {
							$str_up .= ", ";
						}
						$str_up .= strtoupper(str_replace("allowOwnerAddOrEdit", "", $label)) . " =&gt; " . $str_val;
						$strSQL = "SELECT TPConfigsID FROM " . $this->mainTable . " WHERE TPConfigs_Label like '" . $label . "' LIMIT 1";
						$result = $this->returnDBResultSet($strSQL);
						if (count($result) > 0) {
							$strSQL = "UPDATE " . $this->mainTable . " set TPConfigs_Value='" . $val . "', DateModified='" . $CurrDateTime . "', ModifyBy='" . $UserID . "'";
							$strSQL .= " WHERE TPConfigs_Label like '" . $label . "' AND TPConfigs_Value != '" . $val . "'";
						} else {
							$strSQL = "INSERT INTO " . $this->mainTable . " (TPConfigs_Label, TPConfigs_Value, DateInput, InputBy, DateModified, ModifyBy) VALUES ( ";
							$strSQL .= "'" . $label . "', '" . $val . "', '" . $CurrDateTime . "', '" . $UserID . "', '" . $CurrDateTime . "', '" . $UserID . "'";
							$strSQL .= " ) ";
						}
						$this->DBdb_query($strSQL);
					}
				}
				$logDetail = "Update Configs: ";
				$logDetail .= "<small>" . $str_up . "</small>";
				$this->core->load_model('LogObj');
				$this->core->LogObj->addRecord($UserID, "BASIC SETTINGS", $logDetail);
				$output["updateConfig"] = $update_params;
				return $output;
			}
		}
	}
}