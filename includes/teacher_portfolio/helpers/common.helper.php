<?php
if (defined('TEACHER_PORTFOLIO')) {
	if (!function_exists('get_magic_quotes_gpc')) {
		function get_magic_quotes_gpc() {
			return false;
		}
	}
	if (!function_exists('Get_Lang_Selection')) {
		function Get_Lang_Selection($chinese, $english) {
			global $intranet_session_language;
			switch ($intranet_session_language) {
				case "en":
					return $english;
					break;
				default:
					if (empty($chinese)) $chinese = $english;
					return $chinese;
					break;
			}
		}
	}
	if (!function_exists('checkEmpty')) {
		function checkEmpty($val) {
			$val = trim($val);
			if (empty($val)) {
				$val = "-";
			}
			return $val;
		}
	}

	if (!function_exists('isInteger')) {
		function isInteger($input){
			return(ctype_digit(strval($input)));
		}
	}
	if (!function_exists('isFloat')) {
		function isFloat($input){
			return(is_numeric($input));
		}
	}
	
	if (!function_exists('No_Access_Right_Pop_Up')) {
		function No_Access_Right_Pop_Up(){
			echo "Access Denied";
			exit;
		}
	}
	
	if (!function_exists("array_depth")) {
		function array_depth($ary)
		{
			$hasArray = 0;
			$maxDepth = $depth = 0;
			foreach((array)$ary as $item)
			{
				if(is_array($item))	
				{
					$depth = max(array_depth($item),$maxDepth);
					$hasArray = 1;
				}
			}
			if($hasArray)
				return $depth+1;
			else
				return 1;
		}
	}
	if (!function_exists("str_lreplace")) {
		function str_lreplace($search, $replace, $subject) {
			$pos = strrpos($subject, $search);
			if($pos !== false)
			{
				$subject = substr_replace($subject, $replace, $pos, strlen($search));
			}
			return $subject;
		}
	}
	if (!function_exists("Get_User_Array_From_Common_Choose")) {
		function Get_User_Array_From_Common_Choose($SelectionValueArr, $UserTypeArr='') {
			$UserIDArr = array();
			if (count($SelectionValueArr) > 0) {
				foreach ($SelectionValueArr as $key => $val) {
					$UserIDArr[] = substr($val, 1);
				}
			}
			return $UserIDArr;
		}
	}
}