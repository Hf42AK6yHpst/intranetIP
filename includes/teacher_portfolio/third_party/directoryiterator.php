<?php
if (!class_exists('DirectoryIterator')) {
	class DirectoryIterator {
		var $key;
		var $current;
		var $valid = true;
		var $entry;
		var $path;
		var $handle;
		/** Construct a directory iterator from a path-string.
		 *
		 * \param $path directory to iterate.
		 */
		function DirectoryIterator($path) {
			if (!in_array(substr($path, strlen($path) - 1, 1), array('\\', '/'))) {
				$path = $path . DIRECTORY_SEPARATOR;
			}
			$this->handle = opendir($path);
			$this->path = $path;
		}
		/** \return The opened path.
		 */
		function getPath() {
			return $this->path;
		}
		/** \return The current file name.
		 */
		function getFilename() {
			return $this->entry;
		}
		/** \return The current entries path and file name.
		 */
		function getPathname() {
			return $this->getPath() . $this->getFilename();
		}
		/** \return The current entry's permissions.
		 */
		function getPerms() {
			return fileperms($this->getPathname());
		}
		/** \return The current entry's inode.
		 */
		function getInode() {
			return fileinode($this->getPathname());
		}
		/** \return The current entry's size in bytes .
		 */
		function getSize() {
			return filesize($this->getPathname());
		}
		/** \return The current entry's owner name.
		 */
		function getOwner() {
			return fileowner($this->getPathname());
		}
		/** \return The current entry's group name.
		 */
		function getGroup() {
			return filegroup($this->getPathname());
		}
		/** \return The current entry's last access time.
		 */
		function getATime() {
			return fileatime($this->getPathname());
		}
		/** \return The current entry's last modification time.
		 */
		function getMTime() {
			return filemtime($this->getPathname());
		}
		/** \return The current entry's last change time.
		 */
		function getCTime() {
			return filectime($this->getPathname());
		}
		/** \return The current entry's size in bytes .
		 */
		function getType() {
			return filetype($this->getPathname());
		}
		/** \return The current entry's extension.
		 */
		function getExtension() {
			$path_parts = pathinfo($this->getPathname());
			if (isset($path_parts['extension'])) {
				return $path_parts['extension'];
			} else {
				return "";
			}
		}
		/** \return The current entry's basename.
		 */
		function getBasename($suffix = '') {
			return basename($this->getPathname(), $suffix);
		}
		/** \return Whether the current entry is writeable.
		 */
		function isWritable() {
			return is_writable($this->getPathname());
		}
		/** \return Whether the current entry is readable.
		 */
		function isReadable() {
			return is_readable($this->getPathname());
		}
		/** \return Whether the current entry is executable.
		 */
		function isExecutable() {
			if (function_exists('is_executable')) {
				return is_executable($this->getPathname());
			}
		}
		/** \return Whether the current entry is .
		 */
		function isFile() {
			return is_file($this->getPathname());
		}
		/** \return Whether the current entry is a directory.
		 */
		function isDir() {
			return is_dir($this->getPathname());
		}
		/** \return Whether the current entry is either '.' or '..'.
		 */
		function isDot() {
			return $this->isDir() && ($this->entry == '.' || $this->entry == '..');
		}
		/** \return whether the current entry is a link.
		 */
		function isLink() {
			return is_link($this->getPathname());
		}
		/** \Move to next entry.
		 */
		function next() {
			$this->valid = $this->getFile();
			$this->key++;
		}
		/** \Rewind dir back to the start.
		 */
		function rewind() {
			$this->key = 0;
			rewinddir($this->handle);
			$this->valid = $this->getFile();
		}
		/** \Check whether dir contains more entries.
		 */
		function valid() {
			if ($this->valid === false) {
				$this->close();
			}
			return $this->valid;
		}
		/** \Return current dir entry.
		 */
		function key() {
			return $this->key;
		}
		/** \Return this.
		 */
		function current() {
			return $this;
		}
		/** \Close dir.
		 */
		function close() {
			closedir($this->handle);
		}
		function getFile() {
			if ( false !== ($file = readdir($this->handle)) ) {
				$this->entry = $file;
				return true;
			} else {
				return false;
			}
		}
	}
}