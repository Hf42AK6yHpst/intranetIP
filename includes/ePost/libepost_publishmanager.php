<?php
# using : Paul

/* Modification Log:
 * 
 */

if (!defined("LIBEPOSTPUBLISHMANAGER_DEFINED"))         // Preprocessor directives
{
	define("LIBEPOSTPUBLISHMANAGER_DEFINED",true);
	include_once($intranet_root."/includes/ePost/config/config.php");
	
	class libepost_publishmanagers extends libepost
	{
		var $PublishManagers;
		
		function libepost_publishmanagers(){
			# Initialize ePost Object
			$this->libepost();
			
			$publishmanagers_raw_ary = $this->get_publishmanagers_raw_ary();
			
			for($i=0;$i<count($publishmanagers_raw_ary);$i++){
				$this->PublishManagers[$i] = new libepost_publishmanager(0, $publishmanagers_raw_ary[$i]);
			}
		}
		
		function get_publishmanagers_raw_ary(){
			global $cfg_ePost;
			
			$sql = "SELECT
						epm.ManagerID,
						epm.UserID,
						epm.Status,
						epm.InputDate,
						epm.ModifiedBy,
						epm.ModifiedDate,
						".getNameFieldByLang('eiu.')." AS UserName,
						eiu.EnglishName,
						eiu.ClassName,
						eiu.ClassNumber,
						".getNameFieldByLang('miu.')." AS Modified_User,
						miu.EnglishName AS Modified_EnglishName,
						miu.ClassName AS Modified_ClassName,
						miu.ClassNumber AS Modified_ClassNumber
					FROM
						EPOST_PUBLISH_MANAGER AS epm INNER JOIN
						INTRANET_USER AS eiu ON epm.UserID = eiu.UserID INNER JOIN
						INTRANET_USER AS miu ON epm.ModifiedBy = miu.UserID
					WHERE
						epm.Status = '".$cfg_ePost['BigNewspaper']['PublushManager_status']['exist']."'
					ORDER BY
						eiu.ClassName , eiu.ClassNumber, eiu.EnglishName";
			$manager_ary = $this->returnArray($sql);
			
			return $manager_ary;
		}
	}
	
	class libepost_publishmanager extends libepost_publishmanagers
	{
		var $ManagerID;
		var $UserID;
		var $Status;
		var $InputDate;
		var $ModifiedBy;
		var $ModifiedDate;
		var $UserName;
		var $EnglishName;
		var $ClassName;
		var $ClassNumber;
		var $Modified_User;
		var $Modified_EnglishName;
		var $Modified_ClassName;
		var $Modified_ClassNumber;
		
		function libepost_publishmanager($ManagerID=0, $PublishManager_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $ManagerID is inputed or not
			$PublishManager_Data_ary = $ManagerID? $this->get_publishmanager_raw_ary($ManagerID) : $PublishManager_Data_ary;
			
			# Initialize Publish Manager Object
			$this->initialize_publishmanager_obj($PublishManager_Data_ary);
		}
		
		function initialize_publishmanager_obj($PublishManager_Data_ary){
			$this->ManagerID 			= $PublishManager_Data_ary['ManagerID'];
			$this->UserID 				= $PublishManager_Data_ary['UserID'];
			$this->Status 				= $PublishManager_Data_ary['Status'];
			$this->InputDate 			= $PublishManager_Data_ary['InputDate'];
			$this->ModifiedBy 			= $PublishManager_Data_ary['ModifiedBy'];
			$this->ModifiedDate 		= $PublishManager_Data_ary['ModifiedDate'];
			$this->UserName		 		= $PublishManager_Data_ary['UserName'];
			$this->EnglishName 			= $PublishManager_Data_ary['EnglishName'];
			$this->ClassName 			= $PublishManager_Data_ary['ClassName'];
			$this->ClassNumber 			= $PublishManager_Data_ary['ClassNumber'];
			$this->Modified_User		= $PublishManager_Data_ary['Modified_User'];
			$this->Modified_EnglishName = $PublishManager_Data_ary['Modified_EnglishName'];
			$this->Modified_ClassName 	= $PublishManager_Data_ary['Modified_ClassName'];
			$this->Modified_ClassNumber = $PublishManager_Data_ary['Modified_ClassNumber'];
		}
		
		function get_publishmanager_raw_ary($ManagerID){
			global $cfg_ePost;
			
			$sql = "SELECT
						epm.ManagerID,
						epm.UserID,
						epm.Status,
						epm.InputDate,
						epm.ModifiedBy,
						epm.ModifiedDate,
						".getNameFieldByLang('eiu.')." AS UserName,
						eiu.EnglishName,
						eiu.ClassName,
						eiu.ClassNumber,
						".getNameFieldByLang('miu.')." AS Modified_User,
						miu.EnglishName AS Modified_EnglishName,
						miu.ClassName AS Modified_ClassName,
						miu.ClassNumber AS Modified_ClassNumber
					FROM
						EPOST_PUBLISH_MANAGER AS epm INNER JOIN
						INTRANET_USER AS eiu ON epm.UserID = eiu.UserID INNER JOIN
						INTRANET_USER AS miu ON epm.ModifiedBy = miu.UserID
					WHERE
						epm.ManagerID = '$ManagerID'";
			$manager_ary = current($this->returnArray($sql));
			
			return $manager_ary;
		}
		
		function update_publishmanager_db(){
			global $UserID, $cfg_ePost;
			
			if($this->ManagerID){
				$ManagerID 	 = $this->ManagerID;
				$InputDate   = $this->InputDate;
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "UPDATE
							EPOST_PUBLISH_MANAGER
						SET
							UserID = '".$this->UserID."',
							Status = '".$this->Status."',
							ModifiedBy = '$UserID',
							ModifiedDate = '$ModifedDate'
						WHERE
							ManagerID = '".$this->ManagerID."'";
				$result = $this->db_db_query($sql);
			}
			else{
				$InputDate   = date('Y-m-d H:i:s');
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "INSERT INTO
							EPOST_PUBLISH_MANAGER
							(
								UserID, Status, InputDate, ModifiedBy, ModifiedDate
							)
						VALUES
							(
								'".$this->UserID."', '".$this->Status."', '$InputDate', '$UserID', '$ModifedDate'
							)";
				$result = $this->db_db_query($sql);
				$ManagerID = $this->db_insert_id();
			}
			
			if($result){
				$sql = "SELECT ".getNameFieldByLang('iu.')." AS UserName, iu.EnglishName, iu.ClassName, iu.ClassNumber FROM INTRANET_USER AS iu WHERE iu.UserID = '".$this->UserID."'";
				$Manager_ary = current($this->returnArray($sql));
				
				$sql = "SELECT ".getNameFieldByLang('iu.')." AS Modified_User, iu.EnglishName, iu.ClassName, iu.ClassNumber FROM INTRANET_USER AS iu WHERE iu.UserID = '$UserID'";
				$ModifiedBy_ary = current($this->returnArray($sql));
				
				$this->ManagerID	    	 = $ManagerID;
				$this->UserName		 		 = $Manager_ary['UserName'];
				$this->EnglishName			 = $Manager_ary['EnglishName'];
				$this->ClassName			 = $Manager_ary['ClassName'];
				$this->ClassNumber			 = $Manager_ary['ClassNumber'];
				$this->InputDate			 = $InputDate;
				$this->ModifiedBy   		 = $UserID;
				$this->ModifiedDate 		 = $ModifedDate;
				$this->Modified_User		 = $ModifiedBy_ary['Modified_User'];
				$this->Modified_EnglishName  = $ModifiedBy_ary['EnglishName'];
				$this->Modified_ClassName 	 = $ModifiedBy_ary['ClassName'];
				$this->Modified_ClassNumber  = $ModifiedBy_ary['ClassNumber'];
			}
			
			return $result;
		}
		
		function delete_publishmanager(){
			global $cfg_ePost, $UserID;
			
			$sql = "UPDATE
						EPOST_PUBLISH_MANAGER
					SET
						Status = '".$cfg_ePost['BigNewspaper']['PublushManager_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						ManagerID = '".$this->ManagerID."'";
			$result = $this->db_db_query($sql);
			
			return $result;
		}
	}
}