<?
class kis_studentregistry extends libdb implements kis_apps {
    private $user_id;
    
    public static function getAvailability($user_id, $user_type, $student_id)
    {
        global $plugin, $sys_custom;
        
        if(!$plugin['AccountMgmt_StudentRegistry'] || !$sys_custom['StudentRegistry']['Kentville'] || !$_SESSION['ParentFillOnlineReg']) {
			return array();
		}
		
		# eService > student registry
        if($user_type==kis::$user_types["parent"]) {
		    return array("studentregistry", "btn_studentregistry", "", "/home/eService/StudentRegistry/online_reg/index.php");
		}
		return array();
    }
            
    public static function getAdminStatus($user_id, $user_type, $student_id)
    {
        if($_SESSION['SSV_USER_ACCESS']["eAdmin-AccountMgmt_StudentRegistry"]) {
    		return array("/home/eAdmin/AccountMgmt/StudentRegistry/management/hk/information/view.php");
    	}
		return array();
    }
    
    public static function getNotificationCount($user_id, $user_type, $student_id)
    {
        return 0;
    }
    
    public function __construct($user_id, $user_type, $student_id, $params)
    {
    	// do nothing
    }
}
?>