<?php

include_once($intranet_root."/includes/libdigitalarchive.php");
class kis_digitalarchive extends libdb implements kis_apps 
{
	private $user_id, $user_type, $student_id;
	
	public static function getAvailability($user_id, $user_type, $student_id)
	{
		global $plugin;
		
		$lda = new libdigitalarchive();
		if($plugin['digital_archive'] && ($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] || $lda->UserInGroupList()>0)){
			return array('digitalarchive','btn_digitalarchive','', '/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/');
		}
		return array();
	}
    
    public static function getAdminStatus($user_id, $user_type, $student_id)
    {
    	if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']){
    		return array('/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/');
    	}
    	return array();
    }
    
    public static function getNotificationCount($user_id, $user_type, $student_id)
    {
    	return 0;
    }
    
    public function __construct($user_id, $user_type, $student_id, $params)
    {
    	global $intranet_db;

		$this->db = $intranet_db;
		$this->user_id = $user_id;
		$this->user_type = $user_type;
		$this->student_id = $user_type == kis::$user_types['teacher']? $user_id: $student_id;
    }
}
?>