<?
/*
 *  2020-02-10 Cameron
 *      - disable digital channel for student and parent for temp solution. case ref: #Y179425
 */
class kis_digitalchannels extends libdb implements kis_apps {
    private $user_id;
    
    public static function getAvailability($user_id, $user_type, $student_id)
    {
        global $plugin, $sys_custom;
        
    	if(!$plugin['DigitalChannels']) {
			return array();
		}
		
		# eService > Digital Channels
		if(($user_type==kis::$user_types["student"]) || ($user_type==kis::$user_types["parent"])) {
		    if (!$sys_custom['DigitalChannels']['HideForParentInKis']) {
		        return array("digitalchannels", "btn_digitalchannels", "", "/home/eAdmin/ResourcesMgmt/DigitalChannels/index.php?From_eService=1");
		    }
		    else {
		        return array();
		    }
		}
		# eAdmin > Digital Channels
		else {
// 			if ($_SESSION['SSV_USER_ACCESS']["eAdmin-DigitalChannels"]) {
				return array("digitalchannels", "btn_digitalchannels", "", "/home/eAdmin/ResourcesMgmt/DigitalChannels/index.php");
// 			}
		}

		return array();
    }
            
    public static function getAdminStatus($user_id, $user_type, $student_id)
    {
        if($_SESSION['SSV_USER_ACCESS']["eAdmin-DigitalChannels"]) {
    		return array("/home/eAdmin/ResourcesMgmt/DigitalChannels/index.php");
    	}
		return array();
    }

    public static function getNotificationCount($user_id, $user_type, $student_id)
    {
		return 0;
    }
    
    public function __construct($user_id, $user_type, $student_id, $params)
    {
    	// do nothing
    }
}
?>