<?
include_once("$intranet_root/includes/libelibrary_plus.php");

class kis_elibrary extends libdb implements kis_apps {
        
    private $user_id;
    
    public static function getAvailability($user_id, $user_type, $student_id){
    
	global $plugin;
    
	$permission = elibrary_plus::getUserPermission($user_id);
	
	if (($permission['setting'] && $permission['circulation']) || $permission['admin']){
	    return array(($plugin['eLib_Lite']?'elibrary_lite':'elibrary'), 'btn_elib', '', '/home/eLearning/elibplus2/');
	   	    
	}else if($permission['circulation']){
		return array(($plugin['eLib_Lite']?'elibrary_lite':'elibrary'), 'btn_elib', '', '/home/library_sys/management/circulation/');
	}
	else if ($plugin['library_management_system'] && $permission['elibplus_opened']){
	    return array(($plugin['eLib_Lite']?'elibrary_lite':'elibrary'), 'btn_elib', '', '/home/eLearning/elibplus2/');
	}
	
	return array();
	
    }
            
    public static function getAdminStatus($user_id, $user_type, $student_id){
    
	
	return array();
    }

    public static function getNotificationCount($user_id, $user_type, $student_id){
    
	return 0;
	
    }
    public function __construct($user_id, $user_type, $student_id, $params){

	
	
    }
    
}
?>