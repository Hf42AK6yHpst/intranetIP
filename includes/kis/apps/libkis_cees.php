<?
# using:

####################################
#
#	Date:	2020-04-16	Philips
#			create this file
#
####################################

class kis_cees extends libdb implements kis_apps {
        
    private $user_id;
    
    public static function getAvailability($user_id, $user_type, $student_id){
    	global $plugin, $sys_custom, $PATH_WRT_ROOT;
    	include_once($PATH_WRT_ROOT.'includes/cust/student_data_analysis_system_kis/libSDAS.php');
    	$libSDAS = new libSDAS();
    	$isAdmin = $libSDAS->checkAccessRight();
    	$isMonthlyReportPIC = $libSDAS->isMonthlyReportPIC($_SESSION['UserID']);

        if(!$plugin['StudentDataAnalysisSystem'] || $plugin['StudentDataAnalysisSystem_Style'] != "tungwah" || !$plugin['SDAS_module']['KISMode']) {
            return array();
        }
        else if($user_type != kis::$user_types['teacher']) {
            return array();
        }
        else {
            $module_path = "/home/student_data_analysis_system_kis/";

            if ($isAdmin){
                return array('cees', 'btn_cees', '_blank', $module_path.'?t=settings.assessmentStatReport.accessRightConfigMonthlyReport');
            }
            else if($isMonthlyReportPIC){
                return array('cees', 'btn_cees', '_blank', '/home/cees_kis/');
            } else {
            	return array();
            }
        }
    }

    public static function getAdminStatus($user_id, $user_type, $student_id){
	    return array();
    }

    public static function getNotificationCount($user_id, $user_type, $student_id){
	    return 0;
    }

    public function __construct($user_id, $user_type, $student_id, $params){
	
    }
}
?>