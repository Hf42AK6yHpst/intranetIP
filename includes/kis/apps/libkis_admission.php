<? //using:
/*
 * 2018-01-26 (Pun):    Modified replaceEmailVariables(), added preReplaceEmailVariables, postReplaceEmailVariables for override by libadmission_cust
 * 2018-01-26 (Pun):    Modified __construct(), changed direct call parent class name for constructor
 * 2017-06-27 (Henry):  Modified replaceEmailVariables() for HKUGAPS cust
 * 2016-01-25 (Omas):	Added getPaymentStatusSelection()
 * 2015-11-09 (Henry):	Modified sendMails() and sendEmailToReceivers()
 * 2015-11-04 (Henry):	Modified updateInterviewFormResultByIds() to fixed duplicate record insert
 * 						[developing] Modified getInterviewFormQuestionAnswerAry to change to <!--SEPARATOR-->
 * 2015-10-27 (Henry):	Modified function updateInterviewFormResultByIds(), getInterviewFormReport()
 * 2015-10-09 (Henry):	Added function getInterviewUserSettings(), updateInterviewUserSettings() and deleteInterviewUserSettings()
 * 2015-10-02 (Henry):	Modified resendNotificationEmailByIds()
 * 2015-09-25 (Henry):  Modified getInterviewSettingSelection() and replaceEmailVariables()
 * 2015-09-23 (Henry):  Added resendNotificationEmailByIds()
 * 2015-08-18 (Henry):	Added getInterviewFormQuestionSelection() and getInterviewFormAnswerSelection()
 * 2015-08-17 (Henry):	Added getInterviewFormReport() and getInterviewFormQuestionAry()
 * 2015-08-13 (Henry):	modified replaceEmailVariables(), getEmailReceivers(), sendEmailToReceivers(), sendMails(), prepareSendMails()
 * 2015-08-11 (Henry):	Added updateInterviewFormBuilterEditByIds()
 * 2015-08-05 (Henry):  Added getUnassignedInterviewApplicantReocrd() and assignApplicantToInterviewSetting()
 * 2015-08-04 (Henry):	Modified function getInterviewSettingAry() to add para $round
 * 						Added removeApplicationInterviewRecord()
 * 2015-08-03 (Henry):	Added function getInterviewDateSelection() and getInterviewFromInputListAry()
 * 2015-07-31 (Henry):	Modified updateInterviewSettingAry() to set GroupName
 * 						Added para $isMultiSelect on getStatusSelection()
 * 						Added getNumberOfApplicant() and getNumberOfApplicant()
 * 2015-07-21 (Henry):	Modified getInterviewSettingAry(), getInterviewSettingSelection() to get GroupName
 * 2015-07-20 (Henry):	Added getInterviewArrangementRecords(), updateInterviewArrangementRecord(), updateInterviewArrangementSessionRecord(), deleteInterviewArrangementRecord(), reorderInterviewArrangementRecords()
 * 2015-07-16 (Henry):	Modified getEmailReceivers() for CHIUCHUNKG Cust
 * 2014-11-04 (Carlos): Added prepareSendMails(), sendEmailToReceivers(), modified getEmailReceivers() to send emails in chunks by ajax mode
 * 2014-10-28 (Henry):	Modified getEmailReceivers(), replaceEmailVariables(), sendMails()
 * 2014-08-13 (Henry):	Modified getInterviewSettingAry()
 * 2014-08-11 (Carlos): Added getEmailAttachmentBasePath(), getEmailTemplateType(), getEmailAttachmentType(), addEmailAttachment(),
 * 						deleteEmailAttachment(), getEmailAttachmentRecords(), getEmailTemplateRecords(), updateEmailTemplateRecord(),
 * 						deleteEmailTemplateRecord(), reorderEmailTemplateRecords(), uploadEmailAttachmentsToTempFolder(), getAttachmentDisplaySize()
 * 2014-08-07 (Carlos): Modified sendMails(), getSendMailResultTable() added attachment function
 * 2014-07-25 (Carlos): modified sendMails(), set return email to the webmaster email
 * 2014-07-18 (Carlos): Modified getEmailReceivers(), get ADMISSION_PG_INFO.Email in priority of ADMISSION_STU_INFO.Email
 */

include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
class kis_admission extends admission_cust implements kis_apps {

    private $user_id;
    private $EMAIL_TEMPLATE_TYPE = array('Active'=>1,'Draft'=>2); // ADMISSION_EMAIL_TEMPLATE.RecordType
    private $EMAIL_ATTACHMENT_TYPE = array('Email'=>1,'EmailTemplate'=>2); // ADMISSION_EMAIL_ATTACHMENT.LinkRecordType

	//not using now
    public static function getAvailability($user_id, $user_type, $student_id){
		global $plugin;

		$li = new libdb();
		$sql = "
			SELECT 
				s.UserID
			FROM 
				ADMISSION_INTERVIEW_USER_SETTING as s 
			LEFT JOIN 
				INTRANET_USER as u On u.UserID=s.UserID 
			WHERE s.UserID IN ('".$user_id."')";

		$isInterviewAdmin = count($li->returnArray($sql));
		if($isInterviewAdmin>0){
			$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission-interview-marker"] = true;
		}
	    if (($_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission-interview-marker"]) && $plugin['eAdmission']){
		return array('admission', 'btn_admission', 'wood', '');
    }

	return array();
    }

    public static function getAdminStatus($user_id, $user_type, $student_id){
		return array();
    }

    public static function getNotificationCount($user_id, $user_type, $student_id){
		return 0;
    }
    public function __construct($user_id, $user_type, $student_id, $params){
		global $intranet_db;
		parent::__construct();
		$this->user_type = $user_type;
/*		$libadmission = new admission_cust();
		$this->db = $intranet_db;
		$this->user_id = $user_id;
		$this->user_type = $user_type;
		$this->schoolYearID = $libadmission->getNextSchoolYearID(); //Display next year info
		$this->classLevelAry = $libadmission->getClassLevel();*/

    }

    public function getInterviewStatusSelection($name='selectInterviewStatus',$status=''){
		global $kis_lang;

    	$x = '<select name="'.$name.'" id="'.$name.'" class="auto_submit">';
		foreach($kis_lang['Admission']['notifiedtointerview'] as $_key => $_value){
			$x .= '<option value="'.$_key.'"'.($_key==$status?' selected="selected"':'').'>';
			$x .= $_value ;
			$x .= '</option>';
		}
		$x .= '</select>';
		return $x;
    }

    public function getAcademicYearSelection($schoolYearID='',$name='selectSchoolYearID'){
    	global $intranet_session_language;
    	$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$SchoolYearArr = kis_utility::getAcademicYears(array("order"=>"Sequence"));

    	$schoolYearCnt = count($SchoolYearArr);
    	$x = '<select name="'.$name.'" id="'.$name.'" class="auto_submit">';
		for($i=0;$i<$schoolYearCnt;$i++){
			$x .= '<option value="'.$SchoolYearArr[$i]['academic_year_id'].'"'.($SchoolYearArr[$i]['academic_year_id']==$schoolYearID?' selected="selected"':'').'>';
			$x .= $SchoolYearArr[$i]['academic_year_name_'.$intranet_session_language] ;
			$x .= '</option>';
		}
		$x .= '</select>';
		return $x;
    }

	function getNavigationBar($NavArr){
		$x = '<div class="navigation_bar">';
		foreach((array)$NavArr as $key=>$arr){
			list($path,$name) = (array)$arr;
			if(!empty($path))
				$x .= '<a href="#/apps/admission/'.$path.'">'.$name.'</a>';
			else
				$x .= '<span>'.$name.'</span>';
			$NavCnt--;
		}
		$x .= '</div>';
		return $x;
	}


	function getApplicationCountByClassLevel($schoolYearID,$ClassLevelID=''){
		$cond = !empty($ClassLevelID)?" AND oi.ApplyLevel='".$ClassLevelID."'":"";
		$sql = "
			SELECT
     			o.ApplyLevel,s.Status,IFNULL(COUNT(DISTINCT o.ApplicationID),0) Cnt
			FROM
				ADMISSION_APPLICATION_STATUS s	
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID	
			INNER JOIN 
				ADMISSION_PG_INFO pg ON s.ApplicationID = pg.ApplicationID
			INNER JOIN
				ADMISSION_STU_INFO stu ON s.ApplicationID = stu.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
			GROUP BY
				o.ApplyLevel,s.Status		
    	";
		$applicationAry = $this->returnArray($sql);
		$record_cnt = count($applicationAry);
		$applicationCountAry = array();
		for($i=0;$i<$record_cnt;$i++){
			$_classLevelId = $applicationAry[$i]['ApplyLevel'];
			$_status = $applicationAry[$i]['Status'];
			$_cnt = $applicationAry[$i]['Cnt'];
			$applicationCountAry[$_classLevelId][$_status] = $_cnt;
			$applicationCountAry[$_classLevelId]['total'] += $_cnt;
		}
		return $applicationCountAry;
	}
	function getPaymentStatusSelection($status='',$name="paymentStatus",$auto_submit=true,$isAll=true, $isMultiSelect=false){
		global $admission_cfg,$kis_lang;
    	$x = '<select name="'.$name.'" id="'.$name.'" '.($auto_submit?'class="auto_submit"':'').' '.($isMultiSelect?'multiple':'').'>';
    	$x .= ($isAll)?'<option value=""'.($status==''?' selected="selected"':'').'>'.$kis_lang['allstatus'].'</option>':'';
		foreach($admission_cfg['PaymentStatus'] as $_key => $_status){
			$x .= '<option value="'.$_status.'"'.($status==$_status?' selected="selected"':'').'>';
			$x .= $kis_lang['Admission']['PaymentStatus'][$_key];
			$x .= '</option>';
		}
		$x .= '</select>';
		return $x;
	}
	function getStatusSelection($status='',$name="selectStatus",$auto_submit=true,$isAll=true, $isMultiSelect=false){
		global $admission_cfg,$kis_lang;
    	$x = '<select name="'.$name.'" id="'.$name.'" '.($auto_submit?'class="auto_submit"':'').' '.($isMultiSelect?'multiple':'').'>';
    	$x .= ($isAll)?'<option value=""'.($status==''?' selected="selected"':'').'>'.$kis_lang['allstatus'].'</option>':'';
		foreach($admission_cfg['Status'] as $_key => $_status){
			$x .= '<option value="'.$_status.'"'.($status==$_status?' selected="selected"':'').'>';
			$x .= $kis_lang['Admission']['Status'][$_key];
			$x .= '</option>';
		}
		$x .= '</select>';
		return $x;
	}
	function getClassLevelSelection($classLevelID='',$name='selectClassLevelID',$firstTitle=false,$firstTitleText='', $isMultiSelect=false, $otherTag=''){
		global $kis_lang;

		if(!is_array($classLevelID)){
			$classLevelID = (array)$classLevelID;
		}

    	$x = '<select name="'.$name.'" id="'.$name.'" '.($isMultiSelect?'multiple':'').' '.$otherTag.' >';
    	$x .= ($firstTitle)?'<option value=""'.(!$classLevelID?' selected="selected"':'').'>- '.($firstTitleText!=''?$firstTitleText:$kis_lang['select']).' -</option>':'';
		foreach($this->classLevelAry as $_classLevelId => $_classLevelName){
			$x .= '<option value="'.$_classLevelId.'"'.(in_array($_classLevelId,$classLevelID)?' selected="selected"':'').'>';
			$x .= $_classLevelName;
			$x .= '</option>';
		}
		$x .= '</select>';
		return $x;
	}
	function getInterviewFormQuestionSelection($formID, $questionID='',$name='selectFormQuestionID',$firstTitle=false,$firstTitleText='', $isMultiSelect=false, $otherTag=''){
		global $kis_lang;

		if(!is_array($questionID)){
			$questionID = (array)$questionID;
		}

    	$x = '<select name="'.$name.'" id="'.$name.'" '.($isMultiSelect?'multiple':'').' '.$otherTag.' >';
    	$x .= ($firstTitle)?'<option value=""'.(!$questionID?' selected="selected"':'').'>- '.($firstTitleText!=''?$firstTitleText:$kis_lang['select']).' -</option>':'';

		$sql = "SELECT QuestionID, Title 
				FROM ADMISSION_INTERVIEW_QUESTION 
				WHERE FormID = '".$formID."'";
		$result = $this->returnArray($sql);

		foreach($result as $_classLevelId => $_classLevelName){
			$x .= '<option value="'.$_classLevelName['QuestionID'].'"'.(in_array($_classLevelName['QuestionID'],$questionID)?' selected="selected"':'').'>';
			$x .= $_classLevelName['Title'];
			$x .= '</option>';
		}
		$x .= '</select>';
		return $x;
	}
	function getInterviewFormAnswerSelection($questionID, $answerID='',$name='selectFormAnswerID',$firstTitle=false,$firstTitleText='', $isMultiSelect=false, $otherTag=''){
		global $kis_lang;

		if(!is_array($answerID)){
			$answerID = (array)$answerID;
		}

    	$x = '<select name="'.$name.'" id="'.$name.'" '.($isMultiSelect?'multiple':'').' '.$otherTag.' >';
    	$x .= ($firstTitle)?'<option value=""'.(!$answerID?' selected="selected"':'').'>- '.($firstTitleText!=''?$firstTitleText:$kis_lang['select']).' -</option>':'';

		$sql = "SELECT OptionID, Name 
				FROM ADMISSION_INTERVIEW_QUESTION_OPTION 
				WHERE QuestionID = '".$questionID."'";
		$result = $this->returnArray($sql);

		foreach($result as $_classLevelId => $_classLevelName){
			$x .= '<option value="'.$_classLevelName['Name'].'"'.(in_array($_classLevelName['Name'],$answerID)?' selected="selected"':'').'>';
			$x .= $_classLevelName['Name'];
			$x .= '</option>';
		}
		$x .= '</select>';
		return $x;
	}
	public function getApplicationListAry($schoolYearID=''){
		global $admission_cfg;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$applicationCountAry = $this->getApplicationCountByClassLevel($schoolYearID);
		$applicationPeriodAry = $this->getApplicationSetting($schoolYearID);
		$applicationListAry = array();
		foreach($this->classLevelAry as $_classLevelId => $_classLevelName){
			$applicationListAry[$_classLevelId] = array(
													'ClassLevelName'=>$_classLevelName,
													'StartDate'=>$applicationPeriodAry[$_classLevelId]['StartDate'],
													'EndDate'=>$applicationPeriodAry[$_classLevelId]['EndDate'],
													'TotalApplicant'=>($applicationCountAry[$_classLevelId]['total']?$applicationCountAry[$_classLevelId]['total']:0)
												);

			foreach($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
				$applicationListAry[$_classLevelId]['Status'][$_key] = $applicationCountAry[$_classLevelId][$_status]?$applicationCountAry[$_classLevelId][$_status]:0;
			}
		}

		return $applicationListAry;
	}
	public function getEmailListAry($schoolYearID='', $keyword='', $classLevelID=''){
		global $admission_cfg, $Lang;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;

		if($classLevelID != ''){
			$cond = ' AND e.YearID=\''.$classLevelID.'\' ';
		}
		if($keyword != ''){
			$search_cond = ' AND (y.YearName LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\' 
							OR e.Subject LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR LastSentBy LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR e.LastSentDate LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR e.RecordID IN (SELECT r2.EmailRecordID FROM ADMISSION_EMAIL_RECEIVER as r2 JOIN ADMISSION_OTHERS_INFO as o ON r2.UserID = o.RecordID AND o.ApplicationID LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'))';
		}
		$name_field = getNameFieldByLang("u.");
		$sql = 'SELECT e.YearID as ApplyLevel, y.YearName as ClassLevelName, e.Subject, SUM(r.SentStatus) as NoOfSuccessSent, COUNT(r.ReceiverID) AS NoOfRecipient, '.$name_field.' as LastSentBy, e.LastSentDate, e.RecordID, SUM(IF(r.AcknowledgeDate<>"",1,0)) as NoOfAcknowledged, e.IsAcknowledge
				FROM ADMISSION_EMAIL_RECORD AS e 
				LEFT JOIN ADMISSION_EMAIL_RECEIVER AS r ON e.RecordID = r.EmailRecordID 
				LEFT JOIN YEAR as y ON y.YearID=e.YearID 
				LEFT JOIN INTRANET_USER as u On u.UserID=e.LastSentBy 
				WHERE e.SchoolYearID = \''.$schoolYearID.'\' '.$cond.' '.$search_cond.'
				GROUP BY e.RecordID 
				ORDER BY e.LastSentDate DESC';
		//debug_r($sql);
		$emailAry = $this->returnArray($sql);

		return $emailAry;
		/*
		$emailListAry = array();
		for($i=0; $i<count($emailAry);$i++){
			if(!isset($emailListAry[$emailAry[$i]['ApplyLevel']])){
				$emailListAry[$emailAry[$i]['ApplyLevel']] = array();
			}
			$emailListAry[$emailAry[$i]['ApplyLevel']][] = array(
															'ClassLevelName'=>$emailAry[$i]['YearName'],
															'Subject'=>$emailAry[$i]['Subject'],
															'NoOfSuccessSent'=>$emailAry[$i]['NoOfSuccessSent'],
															'NoOfRecipient'=>$emailAry[$i]['NoOfRecipient'],
															'LastSentBy'=>$emailAry[$i]['LastSentBy'],
															'LastSentDate'=>$emailAry[$i]['LastSentDate'],
															'RecordID'=>$emailAry[$i]['RecordID']
														);
		}
//		$applicationListAry = array();
//		foreach($this->classLevelAry as $_classLevelId => $_classLevelName){
//			$applicationListAry[$_classLevelId] = $emailListAry[$_classLevelId];
//		}
		return $emailListAry;
		*/
	}

	public function getInterviewSettingAry($recordID='',  $date='', $startTime='', $endTime='', $keyword='', $schoolYearID='', $classLevelID='', $round=''){
		global $admission_cfg, $Lang, $sys_custom;
		//$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;

//		if(!$round){
//			$round=1;
//		}

		if($schoolYearID != ''){
			$cond .= ' AND i.SchoolYearID = \''.$schoolYearID.'\' ';
		}
		if($classLevelID != ''){
			$cond .= ' AND i.ClassLevelID = \''.$classLevelID.'\' ';
		}
		if($recordID != ''){
			$cond .= ' AND i.RecordID = \''.$recordID.'\' ';
		}
		if($date != ''){
			$cond .= ' AND i.Date = \''.$date.'\' ';
		}
		if($startTime != ''){
			$cond .= ' AND i.StartTime >= \''.$startTime.'\' ';
		}
		if($endTime != ''){
			$cond .= ' AND i.EndTime <= \''.$endTime.'\' ';
		}
		if($round != ''){
			$cond .= ' AND i.Round = \''.$round.'\' ';
		}
		if($keyword != ''){
			$search_cond = ' AND (i.Date LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\' 
							OR i.StartTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR i.EndTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\')';
		}
		$sql = 'SELECT i.RecordID as RecordID, i.SchoolYearID as SchoolYearID, i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.Quota as Quota, i.GroupName as GroupName, i.Round as Round, COUNT(o.InterviewSettingID) as Applied, i.Extra 
				FROM ADMISSION_INTERVIEW_SETTING AS i
				LEFT JOIN ADMISSION_OTHERS_INFO AS o ON i.RecordID = o.InterviewSettingID'.($round>1?$round:'').' 
				WHERE 1 '.$cond.' '.$search_cond.' 
				GROUP BY i.RecordID 
				ORDER BY '.($sys_custom['KIS_Admission']['InterviewArrangement']?'':'ClassLevelID,').' Date, StartTime, EndTime, GroupName';
		//debug_r($sql);
		$interviewAry = $this->returnArray($sql);

		return $interviewAry;
	}

	public function getNumberOfApplicantAssignedTointerview($schoolYearID='', $round = 1){
		$sql = "Select StatusSelect 
				From ADMISSION_INTERVIEW_ARRANGEMENT_LOG 
				where SchoolYearID = '".$schoolYearID."' AND Round = '".($round?$round:1)."' order by DateInput desc";
		$statusResult = $this->returnArray($sql);

		$statusSelect = $statusResult[0]['StatusSelect'];

		$cond = " AND s.Status IN (".$statusSelect.") ";

		if($schoolYearID != ''){
			$cond .= ' AND i.SchoolYearID = \''.$schoolYearID.'\' ';
		}
		$sql = 'SELECT COUNT(*)
				FROM ADMISSION_INTERVIEW_SETTING AS i
				JOIN ADMISSION_OTHERS_INFO AS o ON i.RecordID = o.InterviewSettingID'.($round>1?$round:'').'
				JOIN ADMISSION_APPLICATION_STATUS as s ON o.ApplicationID = s.ApplicationID
				WHERE 1 '.$cond;
		//debug_r($sql);
		$interviewAry = $this->returnArray($sql);

		return $interviewAry[0][0];
	}

	public function getNumberOfApplicant($schoolYearID='', $round = 1){

		$sql = "Select StatusSelect 
				From ADMISSION_INTERVIEW_ARRANGEMENT_LOG 
				where SchoolYearID = '".$schoolYearID."' AND Round = '".($round?$round:1)."' order by DateInput desc";
		$statusResult = $this->returnArray($sql);

		$statusSelect = $statusResult[0]['StatusSelect'];

		$cond = " AND s.Status IN (".$statusSelect.") ";

		if($schoolYearID != ''){
			$cond .= ' AND s.SchoolYearID = \''.$schoolYearID.'\' ';
		}
		$sql = 'SELECT COUNT(*)
				FROM ADMISSION_OTHERS_INFO as o
				JOIN ADMISSION_APPLICATION_STATUS as s
				ON o.ApplicationID = s.ApplicationID 
				WHERE 1 '.$cond;
		//debug_r($sql);
		$applicantAry = $this->returnArray($sql);

		return $applicantAry[0][0];
	}

	public function removeInterviewSettingByIds($recordIds){
		if($recordIds=="") return false;

		//$applicationIdsSql = "'".implode("','",(array)$applicationIds)."'";
		$recordIds = is_array($recordIds)? "'".implode("','", $recordIds)."'" : "'".implode("','",explode(",",$recordIds))."'";

		$sql = "
			DELETE FROM
				ADMISSION_INTERVIEW_SETTING
			WHERE
				RecordID IN (".$recordIds.")
		";
		$success[] = $this->db_db_query($sql);

		if(in_array(false, $success))
			return false;
		else
			return true;

	}
//	public function getInterviewListAry($recordID='',  $date='', $startTime='', $endTime='', $keyword=''){
//		global $admission_cfg, $Lang;
//		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
//
//		if($recordID != ''){
//			$cond = ' AND i.RecordID = \''.$recordID.'\' ';
//		}
//		if($date != ''){
//			$cond .= ' AND i.Date >= \''.$date.'\' ';
//		}
//		if($startTime != ''){
//			$cond .= ' AND i.StartTime >= \''.$startTime.'\' ';
//		}
//		if($endTime != ''){
//			$cond .= ' AND i.EndTime <= \''.$endTime.'\' ';
//		}
//		if($keyword != ''){
//			$search_cond = ' AND (i.Date LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
//							OR i.StartTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
//							OR i.EndTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\')';
//		}
//		$sql = 'SELECT i.RecordID as RecordID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.Quota as Quota
//				FROM ADMISSION_INTERVIEW_SETTING AS i
//				LEFT JOIN ADMISSION_OTHERS_INFO AS o ON i.RecordID = o.InterviewSettingID
//				WHERE 1 '.$cond.' '.$search_cond.'
//				ORDER BY Date, StartTime, EndTime';
//		//debug_r($sql);
//		$interviewAry = $this->returnArray($sql);
//
//		return $interviewAry;
//	}

	public function updateInterviewSettingAry($recordID='',  $date='', $startTime='', $endTime='', $quota='', $schoolYearID='', $classLevelID='', $groupName='', $round=1){
		global $admission_cfg, $Lang, $UserID;
		//$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;

		if($recordID == ''){
			$sql = "INSERT INTO ADMISSION_INTERVIEW_SETTING (SchoolYearID, ClassLevelID, Date, StartTime, EndTime, GroupName, Round, Quota, InputBy, DateModified ) Values ('".$schoolYearID."','".$classLevelID."','".$date."','".$startTime."','".$endTime."','".$groupName."','".$round."','".$quota."','".$UserID."',NOW())";
			$success = $this->db_db_query($sql);
		}
		else{
			$sql = "UPDATE ADMISSION_INTERVIEW_SETTING 
					Set 
						SchoolYearID = '".$schoolYearID."', 
						ClassLevelID = '".$classLevelID."', 
						Date = '".$date."', 
						StartTime = '".$startTime."', 
						EndTime = '".$endTime."', 
						GroupName = '".$groupName."', 
						Quota = '".$quota."', 
						DateModified = NOW(),
      					ModifiedBy = '".$UserID."'
					Where RecordID = '".$recordID."' ";
			$success = $this->db_db_query($sql);
		}
		return $success;
	}

	function getInterviewSettingSelection($recordID='',$name='InterviewSettingID',$firstTitle=true,$firstTitleText='', $schoolYearID='', $classLevelID='', $round=''){
		global $kis_lang, $admission_cfg;
    	$x = '<select name="'.$name.'" id="'.$name.'">';
    	$x .= ($firstTitle)?'<option value="0"'.($recordID==''?' selected="selected"':'').'>- '.($firstTitleText!=''?$firstTitleText:$kis_lang['select']).' -</option>':'';
		foreach($this->getInterviewSettingAry('',  '', '', '', '', $schoolYearID, $classLevelID, $round) as $_classLevelId => $_classLevelName){
			if($_classLevelName['Quota'] > $_classLevelName['Applied'] || $recordID==$_classLevelName['RecordID']){
				$x .= '<option value="'.$_classLevelName['RecordID'].'"'.($recordID==$_classLevelName['RecordID']?' selected="selected"':'').'>';
				$x .= $_classLevelName['Date'].' ('.substr($_classLevelName['StartTime'], 0, -3).' ~ '.substr($_classLevelName['EndTime'], 0, -3).') '.($_classLevelName['GroupName']?($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$_classLevelName['GroupName']:'');
				$x .= '</option>';
			}
		}
		$x .= '</select>';
		return $x;
	}

	public function getInterviewDateSelection($date='',$name='selectInterviewDate',$firstTitle=true,$firstTitleText='', $schoolYearID='', $classLevelID='', $round='', $otherTag=''){
		global $kis_lang;
    	$x = '<select name="'.$name.'" id="'.$name.'" '.$otherTag.'>';
    	$x .= ($firstTitle)?'<option value="0"'.($date==''?' selected="selected"':'').'>- '.($firstTitleText!=''?$firstTitleText:$kis_lang['select']).' -</option>':'';

		$tempAssignedDate = array();
		foreach($this->getInterviewSettingAry('',  '', '', '', '', $schoolYearID, $classLevelID, $round) as $_classLevelId => $_classLevelName){
			if(!in_array($_classLevelName['Date'], $tempAssignedDate)){
				$x .= '<option value="'.$_classLevelName['Date'].'"'.($date==$_classLevelName['Date']?' selected="selected"':'').'>';
				$x .= $_classLevelName['Date'];
				$x .= '</option>';
				$tempAssignedDate[] = $_classLevelName['Date'];
			}
		}
		$x .= '</select>';
		return $x;
	}

	public function removeEmailListByIds($recordIds){
		if($recordIds=="") return false;
		$recordIdAry = (array)$recordIds;

		//$applicationIdsSql = "'".implode("','",(array)$applicationIds)."'";
		$recordIds = is_array($recordIds)? "'".implode("','", $recordIds)."'" : "'".implode("','",explode(",",$recordIds))."'";

		$sql = "
			DELETE FROM
				ADMISSION_EMAIL_RECEIVER
			WHERE
				EmailRecordID IN (".$recordIds.")
		";
		$success['DeleteReciever'] = $this->db_db_query($sql);

		$sql = "
			DELETE FROM
				ADMISSION_EMAIL_RECORD
			WHERE
				RecordID IN (".$recordIds.")
		";
		$success['DeleteEmail'] = $this->db_db_query($sql);

		$attachment_type = $this->getEmailAttachmentType('Email');
		if(count($recordIdAry)>0){
			foreach($recordIdAry as $k => $id){
				$attachments = $this->getEmailAttachmentRecords($attachment_type, $id);
				if(count($attachments)>0){
					foreach($attachments as $k2 => $attachment){
						$success['DeleteAttachment_'.$id."_".$attachment['AttachmentID']] = $this->deleteEmailAttachment($attachment['AttachmentID']);
					}
				}
			}
		}

		if(in_array(false, $success))
			return false;
		else
			return true;

	}

	public function getEmailRecordDetail($recordId)
	{
		$name_field = getNameFieldByLang("u.");
		$sql = "SELECT 
					r.*, $name_field as SenderName 
				FROM ADMISSION_EMAIL_RECORD as r 
				LEFT JOIN INTRANET_USER as u ON u.UserID=r.LastSentBy 
				WHERE r.RecordID='$recordId' ";
		$record = $this->returnResultSet($sql);
		$emailDetail = $record[0];

		$sql = "SELECT UserID FROM ADMISSION_EMAIL_RECEIVER WHERE EmailRecordID='$recordId'";
		$receiver_ary = $this->returnVector($sql);
		$emailDetail['NumberOfReceiver'] = count($receiver_ary);
		$emailDetail['applicationIds'] = $receiver_ary;

		return $emailDetail;
	}

	/*
	 * $applicationIdAry : ADMISSION_OTHERS_INFO.RecordID
	 * $recordId : ADMISSION_EMAIL_RECEIVER.EmailRecordID or ADMISSION_EMAIL_RECORD.RecordID
	 * $receiverIdAry: ADMISSION_EMAIL_RECEIVER.ReceiverID
	 */
	public function getEmailReceivers($applicationIdAry, $recordId='', $receiverIdAry='')
	{
		global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language, $sys_custom;

		$cond = "";
		$join_receiver = "LEFT";
		$applicationIdCsv = is_array($applicationIdAry)? "'".implode("','",$applicationIdAry)."'" : "'".implode("','",explode(",",$applicationIdAry))."'";
		if($recordId != ''){
			if((count($applicationIdAry)>0 && $applicationIdAry!='') || trim($applicationIdAry) != ''){
				$cond .= " AND f.RecordID IN (".$applicationIdCsv.") ";
			}
			$cond .= " AND c.EmailRecordID='$recordId' ";
		}else{
			$join_cond = " AND c.ReceiverID IS NULL ";
			$cond = " AND f.RecordID IN (".$applicationIdCsv.") ";
		}

		if($receiverIdAry != '' && is_array($receiverIdAry) && count($receiverIdAry)>0){
			$cond .= " AND c.ReceiverID IN ('".implode("','",$receiverIdAry)."') ";
			$join_receiver = "INNER";
		}

		if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
			$email_field = "IF(p.PG_TYPE = 'F',IF(a.Email IS NOT NULL AND a.Email<>'',a.Email,''),IF(p.PG_TYPE = 'M',IF(a.Email2 IS NOT NULL AND a.Email2<>'',a.Email2,''),''))";
		}else if($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SSGC']['Settings']){
			$email_field = "IF(a.Email IS NOT NULL AND a.Email<>'',a.Email,p.Email)";
		}else{
			$email_field = "IF(p.Email IS NOT NULL AND p.Email<>'',p.Email,a.Email)";
		}

		$pg_cond = '';
		if($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings'] || $sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SSGC']['Settings']){
			$pg_cond = " AND p.PG_TYPE = 'G' ";
		}

		if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
			if($recordId){
				$pg_cond = " AND p.PG_TYPE = 'F' ";
			}
			else {
				$pg_cond = " OR p.ApplicationID=a.ApplicationID AND (p.PG_TYPE = 'F' AND a.Email IS NOT NULL AND a.Email<>'' OR p.PG_TYPE = 'M' AND a.Email2 IS NOT NULL AND a.Email2<>'') ";
			}
		}

		$add_column = '';
		if($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings']){
			$add_column = "b.BriefingDate, b.BriefingInfo,";
		}

		if($sys_custom['KIS_Admission']['FH']['Settings'] || $sys_custom['KIS_Admission']['MUNSANG']['Settings']){
			if($recordId){
				$pg_cond = " AND p.PG_TYPE = 'G' ";
			}
			$orderBy = "p.RecordID";
		}else{
			$orderBy = "a.ApplicationID";
		}

		$sql = "SELECT 
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					IF(c.SentEmail IS NULL OR c.SentEmail='',$email_field, c.SentEmail) as Email,
					c.SentStatus,
					c.AcknowledgeDate,
					CASE b.Status 
						WHEN 2 THEN '".$kis_lang['Admission']['Status']['paymentsettled']."' 
						WHEN 3 THEN '".$kis_lang['Admission']['Status']['waitingforinterview']."' 
						WHEN 4 THEN '".$kis_lang['Admission']['Status']['confirmed']."' 
						WHEN 5 THEN '".$kis_lang['Admission']['Status']['cancelled']."' 
						ELSE '".$kis_lang['Admission']['Status']['pending']."' 
					END as Status,
					b.InterviewDate,
					b.InterviewLocation, 
					$add_column
					c.ReceiverID,
					d.ApplyYear as schoolYearID, 
					d.ApplyLevel as classLevelID,
					c.DateModified,
					d.InterviewSettingID,
					d.InterviewSettingID2,
					d.InterviewSettingID3,
					a.LangSpokenAtHome
				FROM ADMISSION_OTHERS_INFO as f 
				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID 
				LEFT JOIN ADMISSION_PG_INFO as p ON p.ApplicationID=a.ApplicationID AND p.Email IS NOT NULL AND p.Email<>'' $pg_cond
				LEFT JOIN ADMISSION_OTHERS_INFO as d ON d.ApplicationID=a.ApplicationID 
				LEFT JOIN ADMISSION_APPLICATION_STATUS as b ON b.ApplicationID=a.ApplicationID 
				$join_receiver JOIN ADMISSION_EMAIL_RECEIVER as c ON c.UserID=f.RecordID $join_cond 
				WHERE 1 $cond 
				ORDER BY $orderBy";

		$records = $this->returnArray($sql);
		//debug_r($records);
		return $records;
	}

	public function convertToDateWithLang($date, $lang){
		if($lang == 'b5'){
			$weekDayArr = array('日','一','二','三','四','五','六');
			$weekDay = $weekDayArr[date('w', strtotime($date))];
			$date = date('Y年n月j日 (星期%1)', strtotime($date));
			$date = str_replace('%1', $weekDay, $date);
		}
		else if($lang == 'en'){
			$date = date('j F Y (l)', strtotime($date));
		}
		return $date;
	}

	public function convertToTimeWithLang($time, $lang){
		if($lang == 'b5'){
			$meridiemArr = array('AM' => '上午', 'PM' => '下午');
			$meridiem = $meridiemArr[ date('A', strtotime($time)) ];
			$time = date('%1 g時i分', strtotime($time));
			$time = str_replace('%1', $meridiem, $time);
		}
		else if($lang == 'en'){
			$time = date('g:i A', strtotime($time));
		}
		return $time;
	}

	public function replaceEmailVariables($text, $chineseName, $englishName, $applicationNo, $applicationStatus, $interviewDateTime, $interviewLocation, $interviewDateTime1='', $interviewDateTime2='', $interviewDateTime3='',$briefingdate='',$briefinginfo='', $langSpokenAtHome='', $printEmailLink='')
	{
		global $kis_lang, $admission_cfg, $sys_custom;


		if(method_exists($this, 'preReplaceEmailVariables')){
		    $text = $this->preReplaceEmailVariables($text, $chineseName, $englishName, $applicationNo, $applicationStatus, $interviewDateTime, $interviewLocation, $interviewDateTime1, $interviewDateTime2, $interviewDateTime3, $briefingdate, $briefinginfo, $langSpokenAtHome, $printEmailLink);
		}

		if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
			$replaced_text = $text;
			$replaced_text = str_replace("[=StudentNameE=]",$englishName,$replaced_text);
			$replaced_text = str_replace("[=StudentNameC=]",$chineseName,$replaced_text);
			$replaced_text = str_replace("[=ApplicationNo=]",$applicationNo,$replaced_text);
			$replaced_text = str_replace("[=ApplicationStatus=]",$applicationStatus,$replaced_text);
			$replaced_text = str_replace("[=InterviewDateTime=]",substr($interviewDateTime, 0, -3),$replaced_text);
			$replaced_text = str_replace("[=InterviewLocation=]",$interviewLocation,$replaced_text);
			$replaced_text = str_replace("[=BriefingDateTime=]",substr($briefingdate, 0, -3),$replaced_text);
			$replaced_text = str_replace("[=BriefingInfo=]",$briefinginfo,$replaced_text);
			$replaced_text = str_replace("[=InterviewLanguageC=]",$langSpokenAtHome=='Cantonese'?'廣東話':($langSpokenAtHome=='English'?'英文':($langSpokenAtHome=='Putonghua'?'普通話':'--')),$replaced_text);
			$replaced_text = str_replace("[=InterviewLanguageE=]",$langSpokenAtHome?$langSpokenAtHome:'--',$replaced_text);

			$interviewInfo = current($this->getInterviewListAry($interviewDateTime1, '', '', '', '', '', '', 1));
		 	$interviewInfo2 = current($this->getInterviewListAry($interviewDateTime2, '', '', '', '', '', '', 2));
		 	$interviewInfo3 = current($this->getInterviewListAry($interviewDateTime3, '', '', '', '', '', '', 3));

			$interviewTimeslotName1 = $this->getInterviewTimeslotName($interviewDateTime1, 1);
			$interviewTimeslotName2 = $this->getInterviewTimeslotName($interviewDateTime2, 2);
			$interviewTimeslotName3 = $this->getInterviewTimeslotName($interviewDateTime3, 3);

			$interviewDateTime1 = ($interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'--');
	        $interviewDateTime2 = ($interviewInfo2?$interviewInfo2['Date'].' ('.substr($interviewInfo2['StartTime'], 0, -3).' ~ '.substr($interviewInfo2['EndTime'], 0, -3).')'.($interviewInfo2['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo2['GroupName']:''):'--');
	        $interviewDateTime3 = ($interviewInfo3?$interviewInfo3['Date'].' ('.substr($interviewInfo3['StartTime'], 0, -3).' ~ '.substr($interviewInfo3['EndTime'], 0, -3).')'.($interviewInfo3['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo3['GroupName']:''):'--');
			//debug_pr($interviewInfo);
			$replaced_text = str_replace("[=FirstRoundInterviewGroup=]",$interviewTimeslotName1.'-'.$interviewInfo['GroupName'],$replaced_text);
			$replaced_text = str_replace("[=FirstRoundInterviewDateC=]",$this->convertToDateWithLang($interviewInfo['Date'], 'b5'),$replaced_text);
			$replaced_text = str_replace("[=FirstRoundInterviewStartTimeC=]",$this->convertToTimeWithLang($interviewInfo['StartTime'], 'b5'),$replaced_text);
			$replaced_text = str_replace("[=FirstRoundInterviewEndTimeC=]",$this->convertToTimeWithLang($interviewInfo['EndTime'], 'b5'),$replaced_text);
			$replaced_text = str_replace("[=FirstRoundInterviewRegistationTimeC=]",$this->convertToTimeWithLang(date('H:i:s', strtotime($interviewInfo['StartTime'].' - 15 minute')), 'b5'),$replaced_text);

			$replaced_text = str_replace("[=SecondRoundInterviewGroup=]",$interviewTimeslotName2.'-'.$interviewInfo2['GroupName'],$replaced_text);
			$replaced_text = str_replace("[=SecondRoundInterviewDateC=]",$this->convertToDateWithLang($interviewInfo2['Date'], 'b5'),$replaced_text);
			$replaced_text = str_replace("[=SecondRoundInterviewStartTimeC=]",$this->convertToTimeWithLang($interviewInfo2['StartTime'], 'b5'),$replaced_text);
			$replaced_text = str_replace("[=SecondRoundInterviewEndTimeC=]",$this->convertToTimeWithLang($interviewInfo2['EndTime'], 'b5'),$replaced_text);
			$replaced_text = str_replace("[=SecondRoundInterviewRegistationTimeC=]",$this->convertToTimeWithLang(date('H:i:s', strtotime($interviewInfo2['StartTime'].' - 15 minute')), 'b5'),$replaced_text);

			$replaced_text = str_replace("[=ThirdRoundInterviewGroup=]",$interviewTimeslotName3.'-'.$interviewInfo3['GroupName'],$replaced_text);
			$replaced_text = str_replace("[=ThirdRoundInterviewDateC=]",$this->convertToDateWithLang($interviewInfo3['Date'], 'b5'),$replaced_text);
			$replaced_text = str_replace("[=ThirdRoundInterviewStartTimeC=]",$this->convertToTimeWithLang($interviewInfo3['StartTime'], 'b5'),$replaced_text);
			$replaced_text = str_replace("[=ThirdRoundInterviewEndTimeC=]",$this->convertToTimeWithLang($interviewInfo3['EndTime'], 'b5'),$replaced_text);
			$replaced_text = str_replace("[=ThirdRoundInterviewRegistationTimeC=]",$this->convertToTimeWithLang(date('H:i:s', strtotime($interviewInfo3['StartTime'].' - 15 minute')), 'b5'),$replaced_text);

			$replaced_text = str_replace("[=FirstRoundInterviewDateE=]",$this->convertToDateWithLang($interviewInfo['Date'], 'en'),$replaced_text);
			$replaced_text = str_replace("[=FirstRoundInterviewStartTimeE=]",$this->convertToTimeWithLang($interviewInfo['StartTime'], 'en'),$replaced_text);
			$replaced_text = str_replace("[=FirstRoundInterviewEndTimeE=]",$this->convertToTimeWithLang($interviewInfo['EndTime'], 'en'),$replaced_text);
			$replaced_text = str_replace("[=FirstRoundInterviewRegistationTimeE=]",$this->convertToTimeWithLang(date('H:i:s', strtotime($interviewInfo['StartTime'].' - 15 minute')), 'en'),$replaced_text);

			$replaced_text = str_replace("[=SecondRoundInterviewDateE=]",$this->convertToDateWithLang($interviewInfo2['Date'], 'en'),$replaced_text);
			$replaced_text = str_replace("[=SecondRoundInterviewStartTimeE=]",$this->convertToTimeWithLang($interviewInfo2['StartTime'], 'en'),$replaced_text);
			$replaced_text = str_replace("[=SecondRoundInterviewEndTimeE=]",$this->convertToTimeWithLang($interviewInfo2['EndTime'], 'en'),$replaced_text);
			$replaced_text = str_replace("[=SecondRoundInterviewRegistationTimeE=]",$this->convertToTimeWithLang(date('H:i:s', strtotime($interviewInfo2['StartTime'].' - 15 minute')), 'en'),$replaced_text);

			$replaced_text = str_replace("[=ThirdRoundInterviewDateE=]",$this->convertToDateWithLang($interviewInfo3['Date'], 'en'),$replaced_text);
			$replaced_text = str_replace("[=ThirdRoundInterviewStartTimeE=]",$this->convertToTimeWithLang($interviewInfo3['StartTime'], 'en'),$replaced_text);
			$replaced_text = str_replace("[=ThirdRoundInterviewEndTimeE=]",$this->convertToTimeWithLang($interviewInfo3['EndTime'], 'en'),$replaced_text);
			$replaced_text = str_replace("[=ThirdRoundInterviewRegistationTimeE=]",$this->convertToTimeWithLang(date('H:i:s', strtotime($interviewInfo3['StartTime'].' - 15 minute')), 'en'),$replaced_text);

			$replaced_text = str_replace("[=Barcode=]",'<img src="'.($_SERVER['SERVER_PORT'] == 443?'https://':'http://').$_SERVER['HTTP_HOST'].'/kis/admission_form/barcode.php?barcode='.rawurlencode($applicationNo).'"/>',$replaced_text);
			$replaced_text = str_replace("[=PrintEmailLink=]",'<a target="_blank" href="'.$printEmailLink.'">'.$printEmailLink.'</a>',$replaced_text);
		}
		else{
			$replaced_text = $text;
			$replaced_text = str_replace("[=StudentEngName=]",$englishName,$replaced_text);
			$replaced_text = str_replace("[=StudentChiName=]",$chineseName,$replaced_text);
			$replaced_text = str_replace("[=ApplicationNo=]",$applicationNo,$replaced_text);
			$replaced_text = str_replace("[=ApplicationStatus=]",$applicationStatus,$replaced_text);
			$replaced_text = str_replace("[=InterviewDateTime=]",substr($interviewDateTime, 0, -3),$replaced_text);
			$replaced_text = str_replace("[=InterviewLocation=]",$interviewLocation,$replaced_text);
			$replaced_text = str_replace("[=BriefingDateTime=]",substr($briefingdate, 0, -3),$replaced_text);
			$replaced_text = str_replace("[=BriefingInfo=]",$briefinginfo,$replaced_text);

			//if($interviewDateTime1 || $interviewDateTime2 || $interviewDateTime3){

				$interviewInfo = current($this->getInterviewListAry($interviewDateTime1, '', '', '', '', '', '', 1));
			 	$interviewInfo2 = current($this->getInterviewListAry($interviewDateTime2, '', '', '', '', '', '', 2));
			 	$interviewInfo3 = current($this->getInterviewListAry($interviewDateTime3, '', '', '', '', '', '', 3));

				$interviewDateTime1_zh = $interviewDateTime1_en = $interviewDateTime2_zh = $interviewDateTime2_en = $interviewDateTime3_zh = $interviewDateTime3_en = '';
				if($sys_custom['KIS_Admission']['YLSYK']['Settings'] || $sys_custom['KIS_Admission']['SSGC']['Settings']){
					$interviewDateTime1 = ($interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).')':'--');
		            $interviewDateTime2 = ($interviewInfo2?$interviewInfo2['Date'].' ('.substr($interviewInfo2['StartTime'], 0, -3).')':'--');
		            $interviewDateTime3 = ($interviewInfo3?$interviewInfo3['Date'].' ('.substr($interviewInfo3['StartTime'], 0, -3).')':'--');
				}
				else if($sys_custom['KIS_Admission']['SHCK']['Settings']){
					$interviewDateTime1 = ($interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')':'--');
		            $interviewDateTime2 = ($interviewInfo2?$interviewInfo2['Date'].' ('.substr($interviewInfo2['StartTime'], 0, -3).' ~ '.substr($interviewInfo2['EndTime'], 0, -3).')':'--');
		            $interviewDateTime3 = ($interviewInfo3?$interviewInfo3['Date'].' ('.substr($interviewInfo3['StartTime'], 0, -3).' ~ '.substr($interviewInfo3['EndTime'], 0, -3).')':'--');
				}
				else{
					$interviewDateTime1 = ($interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'--');
					$interviewDateTime1_zh = ($interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom_zh']:$kis_lang['sessiongroup_zh']).' '.$interviewInfo['GroupName']:''):'--');
					$interviewDateTime1_en = ($interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom_en']:$kis_lang['sessiongroup_en']).' '.$interviewInfo['GroupName']:''):'--');

		            $interviewDateTime2 = ($interviewInfo2?$interviewInfo2['Date'].' ('.substr($interviewInfo2['StartTime'], 0, -3).' ~ '.substr($interviewInfo2['EndTime'], 0, -3).')'.($interviewInfo2['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo2['GroupName']:''):'--');
		            $interviewDateTime2_zh = ($interviewInfo2?$interviewInfo2['Date'].' ('.substr($interviewInfo2['StartTime'], 0, -3).' ~ '.substr($interviewInfo2['EndTime'], 0, -3).')'.($interviewInfo2['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom_zh']:$kis_lang['sessiongroup_zh']).' '.$interviewInfo2['GroupName']:''):'--');
		            $interviewDateTime2_en = ($interviewInfo2?$interviewInfo2['Date'].' ('.substr($interviewInfo2['StartTime'], 0, -3).' ~ '.substr($interviewInfo2['EndTime'], 0, -3).')'.($interviewInfo2['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom_en']:$kis_lang['sessiongroup_en']).' '.$interviewInfo2['GroupName']:''):'--');

		            $interviewDateTime3 = ($interviewInfo3?$interviewInfo3['Date'].' ('.substr($interviewInfo3['StartTime'], 0, -3).' ~ '.substr($interviewInfo3['EndTime'], 0, -3).')'.($interviewInfo3['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo3['GroupName']:''):'--');
		            $interviewDateTime3_zh = ($interviewInfo3?$interviewInfo3['Date'].' ('.substr($interviewInfo3['StartTime'], 0, -3).' ~ '.substr($interviewInfo3['EndTime'], 0, -3).')'.($interviewInfo3['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom_zh']:$kis_lang['sessiongroup_zh']).' '.$interviewInfo3['GroupName']:''):'--');
		            $interviewDateTime3_en = ($interviewInfo3?$interviewInfo3['Date'].' ('.substr($interviewInfo3['StartTime'], 0, -3).' ~ '.substr($interviewInfo3['EndTime'], 0, -3).')'.($interviewInfo3['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom_en']:$kis_lang['sessiongroup_en']).' '.$interviewInfo3['GroupName']:''):'--');
				}
				//debug_pr($interviewInfo);
				$interviewTime1 = ($interviewInfo?substr($interviewInfo['StartTime'], 0, -3):'--');
				$interviewTime2 = ($interviewInfo2?substr($interviewInfo2['StartTime'], 0, -3):'--');
				$interviewTime3 = ($interviewInfo3?substr($interviewInfo3['StartTime'], 0, -3):'--');

				$replaced_text = str_replace("[=FirstRoundInterviewDateTime=]",$interviewDateTime1,$replaced_text);
				$replaced_text = str_replace("[=FirstRoundInterviewDateTimeChi=]",$interviewDateTime1_zh,$replaced_text);
				$replaced_text = str_replace("[=FirstRoundInterviewDateTimeEng=]",$interviewDateTime1_en,$replaced_text);

				$replaced_text = str_replace("[=SecondRoundInterviewDateTime=]",$interviewDateTime2,$replaced_text);
				$replaced_text = str_replace("[=SecondRoundInterviewDateTimeChi=]",$interviewDateTime2_zh,$replaced_text);
				$replaced_text = str_replace("[=SecondRoundInterviewDateTimeEng=]",$interviewDateTime2_en,$replaced_text);

				$replaced_text = str_replace("[=ThirdRoundInterviewDateTime=]",$interviewDateTime3,$replaced_text);
				$replaced_text = str_replace("[=ThirdRoundInterviewDateTimeChi=]",$interviewDateTime3_zh,$replaced_text);
				$replaced_text = str_replace("[=ThirdRoundInterviewDateTimeEng=]",$interviewDateTime3_en,$replaced_text);

				$replaced_text = str_replace("[=FirstRoundInterviewTime=]",$interviewTime1,$replaced_text);
				$replaced_text = str_replace("[=SecondRoundInterviewTime=]",$interviewTime2,$replaced_text);
				$replaced_text = str_replace("[=ThirdRoundInterviewTime=]",$interviewTime3,$replaced_text);
			//}
		}

		if(method_exists($this, 'postReplaceEmailVariables')){
		    $replaced_text = $this->postReplaceEmailVariables($replaced_text, $chineseName, $englishName, $applicationNo, $applicationStatus, $interviewDateTime, $interviewLocation, $interviewDateTime1, $interviewDateTime2, $interviewDateTime3, $briefingdate, $briefinginfo, $langSpokenAtHome, $printEmailLink);
		}


		return $replaced_text;
	}

	/*
	 * @param $sendTarget : 1 - send to all , 2 - send to those success, 3 - send to those failed, 4 - send to those have not acknowledged
	 * @param $tmpAttachmentFolder : a temp folder where stores all attachments to be sent, empty means no attachment
	 * @param $attachmentIdAry : ADMISSION_EMAIL_ATTACHMENT.AttachmentID array may contains email template attachments or email record;s own attachments
	 */
	public function sendMails($applicationIdAry,$subject,$message,$isAcknowledge,$acknowledgeMessage,$recordId='',$schoolYearId='',$yearId='', $sendTarget=1, $tmpAttachmentFolder='', $attachmentIdAry=array())
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT, $file_path;
		include_once($intranet_root."/includes/libwebmail.php");
		include_once($intranet_root."/includes/libfilesystem.php");
		include_once($PATH_WRT_ROOT."includes/libfiletable.php");
		$libwebmail = new libwebmail();
		$lfs = new libfilesystem();

		$webmaster_email = get_webmaster();
		$from = $libwebmail->GetWebmasterMailAddress();
		$inputby = $_SESSION['UserID'];
		$result = array();

		$resend = $recordId != '';

		if($resend){
			$emailDetail = $this->getEmailRecordDetail($recordId);
			$applicationIdAry = $emailDetail['applicationIds'];
		}
		$tmpReceiverAry = $this->getEmailReceivers($applicationIdAry,$recordId);
		$receiverAry = array();
		for($i=0;$i<count($tmpReceiverAry);$i++){
			if($sendTarget == 1 || ($sendTarget == 2 && $tmpReceiverAry[$i]['SentStatus']=='1')
				 || ($sendTarget == 3 && $tmpReceiverAry[$i]['SentStatus']=='0') || ($sendTarget == 4 && trim($tmpReceiverAry[$i]['AcknowledgeDate'])==''))
			{
				$receiverAry[] = $tmpReceiverAry[$i];
			}
		}
		$receiverCount = count($receiverAry);

		if(!$resend){
			$sql = "INSERT INTO ADMISSION_EMAIL_RECORD (SchoolYearID,YearID,Subject,Message,IsAcknowledge,AcknowledgeMessage,InputBy,DateInput,LastSentBy,LastSentDate) 
					VALUES ('$schoolYearId','$yearId','".$this->Get_Safe_Sql_Query($subject)."','".$this->Get_Safe_Sql_Query($message)."',
					'".($isAcknowledge==1?'1':'0')."','".$this->Get_Safe_Sql_Query($acknowledgeMessage)."','".$inputby."',NOW(),'".$inputby."',NOW())";
			$result['InsertEmailRecord'] = $this->db_db_query($sql);
			$recordId = $this->db_insert_id();
		}else{
			$sql = "UPDATE ADMISSION_EMAIL_RECORD SET 
					Subject='".$this->Get_Safe_Sql_Query($subject)."',
					Message='".$this->Get_Safe_Sql_Query($message)."',IsAcknowledge='".($isAcknowledge==1?'1':'0')."',
					AcknowledgeMessage='".$this->Get_Safe_Sql_Query($acknowledgeMessage)."',
					LastSentBy='".$inputby."',LastSentDate=NOW() WHERE RecordID='$recordId'";
			$result['UpdateEmailRecord'] = $this->db_db_query($sql);
		}

		$base_dir = $this->getEmailAttachmentBasePath();
		$email_attachment_type = $this->getEmailAttachmentType('Email');

		$existAttachmentAry = $this->getEmailAttachmentRecords($email_attachment_type, $recordId);
		$existAttachmentIdAry = Get_Array_By_Key($existAttachmentAry,'AttachmentID');
		$attachmentIdToRemove = array_values(array_diff($existAttachmentIdAry, $attachmentIdAry));
		$attachmentIdToAdd = array_values(array_diff($attachmentIdAry,$existAttachmentIdAry));

		if(count($attachmentIdToAdd)>0){
			$newAttachmentAry = $this->getEmailAttachmentRecords('', '', $attachmentIdToAdd);
		}

		if(count($attachmentIdToRemove)>0){
			foreach($attachmentIdToRemove as $k => $attachment_id){
				$this->deleteEmailAttachment($attachment_id);
			}
		}

		$new_files = array();
		if(count($newAttachmentAry)>0){
			foreach($newAttachmentAry as $k => $ary){
				$new_files[] = array('tmp_name'=>$file_path."/".$ary['FilePath'],'name'=>$ary['FileName']);
			}
			if(count($new_files)>0){
				$this->addEmailAttachment($email_attachment_type, $recordId, $new_files);
			}
		}
		$new_files2 = array();
		if($tmpAttachmentFolder != ''){ // there are new uploaded attachments
			$lft = new libfiletable("", $base_dir."/".$tmpAttachmentFolder, 0, 0, "");
			$tmp_files = $lft->files;

			while (list($key, $value) = each($tmp_files))
			{
				$tmp_full_path = $base_dir."/".$tmpAttachmentFolder."/".$tmp_files[$key][0];
				$new_files2[] = array('tmp_name'=>$tmp_full_path,'name'=>$tmp_files[$key][0]);
			}
			if(count($new_files2)>0){
				$this->addEmailAttachment($email_attachment_type, $recordId, $new_files2);
			}
			$lfs->deleteDirectory($base_dir."/".$tmpAttachmentFolder);
		}

		$allAttachmentAry = $this->getEmailAttachmentRecords($email_attachment_type, $recordId);
		$email_files = array();
		for($i=0;$i<count($allAttachmentAry);$i++){
			$email_files[] = array('tmp_name'=>$file_path."/".$allAttachmentAry[$i]['FilePath'],'name'=>$allAttachmentAry[$i]['FileName']);
		}
		$tmp_attachment_folder = $this->uploadEmailAttachmentsToTempFolder($email_files);
		if($tmp_attachment_folder != ''){
			$attachment_folder_path = $base_dir."/".$tmp_attachment_folder;
		}

		for($i=0;$i<$receiverCount;$i++){
			$email_message = '';
			if($isAcknowledge == 1){
				$encrypted_data = getEncryptedText('receiverid='.$receiverAry[$i]['ReceiverID']);
				$link = 'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_email_acknowledgement.php?q='.$encrypted_data;
				$acknowledge_link = '<div><a style="background-color: #FFFF00" href="'.$link.'" target="_blank">[ '.($acknowledgeMessage==''?$kis_lang['acknowledgementlinktext']:$acknowledgeMessage).' ]</a></div><br />';
				$email_message = $acknowledge_link;
			}

			$to_email = trim($receiverAry[$i]['Email']);

			$email_subject = $this->replaceEmailVariables($subject, $receiverAry[$i]['ChineseName'], $receiverAry[$i]['EnglishName'], $receiverAry[$i]['ApplicationNo'], $receiverAry[$i]['Status'], $receiverAry[$i]['InterviewDate'], $receiverAry[$i]['InterviewLocation'],$receiverAry[$i]['InterviewSettingID'], $receiverAry[$i]['InterviewSettingID2'], $receiverAry[$i]['InterviewSettingID3'], $receiverAry[$i]['BriefingDate'], $receiverAry[$i]['BriefingInfo'], $receiverAry[$i]['LangSpokenAtHome'], $this->getPrintLink($recordId, $receiverAry[$i]['UserID']));
			$email_message .= $this->replaceEmailVariables($message, $receiverAry[$i]['ChineseName'], $receiverAry[$i]['EnglishName'], $receiverAry[$i]['ApplicationNo'], $receiverAry[$i]['Status'], $receiverAry[$i]['InterviewDate'], $receiverAry[$i]['InterviewLocation'],$receiverAry[$i]['InterviewSettingID'], $receiverAry[$i]['InterviewSettingID2'], $receiverAry[$i]['InterviewSettingID3'], $receiverAry[$i]['BriefingDate'], $receiverAry[$i]['BriefingInfo'], $receiverAry[$i]['LangSpokenAtHome'], $this->getPrintLink($recordId, $receiverAry[$i]['UserID']));

			if($resend)
			{
				$sql = "UPDATE ADMISSION_EMAIL_RECEIVER SET SentEmail='$to_email',AcknowledgeDate=NULL,ModifiedBy='".$inputby."',DateModified=NOW() WHERE ReceiverID='".$receiverAry[$i]['ReceiverID']."'";
				$result['UpdateReceiver_'.$receiverAry[$i]['UserID']] = $this->db_db_query($sql);
			}else{
				$sql = "INSERT INTO ADMISSION_EMAIL_RECEIVER (EmailRecordID,UserID,SentEmail,AcknowledgeDate,InputBy,DateInput,ModifiedBy,DateModified) 
						VALUES ('$recordId','".$receiverAry[$i]['UserID']."','$to_email',NULL,'$inputby',NOW(),'$inputby',NOW())";
				$result['InsertReceiver_'.$receiverAry[$i]['UserID']] = $this->db_db_query($sql);
				$receiverAry[$i]['ReceiverID']= $this->db_insert_id();
			}

			if($isAcknowledge == 1){
				$encrypted_data = getEncryptedText('receiverid='.$receiverAry[$i]['ReceiverID']);
				$link = 'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_email_acknowledgement.php?q='.$encrypted_data;
				$acknowledge_link = '<br /><div><a style="background-color: #FFFF00" href="'.$link.'" target="_blank">[ '.($acknowledgeMessage==''?$kis_lang['acknowledgementlinktext']:$acknowledgeMessage).' ]</a></div>';
				$email_message .= $acknowledge_link;
			}

			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$result['SendMail_'.$receiverAry[$i]['UserID']] = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),$attachment_folder_path,$IsImportant="", $webmaster_email, $reply_address="",$isMulti=null,$nl2br=0);
				$sent_ok = $result['SendMail_'.$receiverAry[$i]['UserID']];
			}else{
				$sent_ok = false;
			}
			if(!$sent_ok){
				$sql = "UPDATE ADMISSION_EMAIL_RECEIVER SET SentStatus='0' WHERE ReceiverID='".$receiverAry[$i]['ReceiverID']."'";
				$this->db_db_query($sql);
			}
		}

		if($attachment_folder_path != ''){
			$lfs->deleteDirectory($attachment_folder_path);
		}

		return array('recordId'=>$recordId , 'result'=>!in_array(false, $result));
	}

	/*
	 * @param $sendTarget : 1 - send to all , 2 - send to those success, 3 - send to those failed, 4 - send to those have not acknowledged
	 * @param $tmpAttachmentFolder : a temp folder where stores all attachments to be sent, empty means no attachment
	 * @param $attachmentIdAry : ADMISSION_EMAIL_ATTACHMENT.AttachmentID array may contains email template attachments or email record;s own attachments
	 */
	public function prepareSendMails($applicationIdAry,$subject,$message,$isAcknowledge,$acknowledgeMessage,$recordId='',$schoolYearId='',$yearId='', $sendTarget=1, $tmpAttachmentFolder='', $attachmentIdAry=array())
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT, $file_path;
		include_once($intranet_root."/includes/libwebmail.php");
		include_once($intranet_root."/includes/libfilesystem.php");
		include_once($PATH_WRT_ROOT."includes/libfiletable.php");

		$libwebmail = new libwebmail();
		$lfs = new libfilesystem();

		$webmaster_email = get_webmaster();
		$from = $libwebmail->GetWebmasterMailAddress();
		$inputby = $_SESSION['UserID'];
		$result = array();

		$resend = $recordId != '';

		if($resend){
			$emailDetail = $this->getEmailRecordDetail($recordId);
			if($sendTarget != 5){
				$applicationIdAry = $emailDetail['applicationIds'];
			}
			else{
				$applicationIdAry = explode(',',$applicationIdAry);
			}
		}
		$tmpReceiverAry = $this->getEmailReceivers($applicationIdAry,$recordId);
		$receiverAry = array();
		for($i=0;$i<count($tmpReceiverAry);$i++){
			if($sendTarget == 1 || $sendTarget == 5 || ($sendTarget == 2 && $tmpReceiverAry[$i]['SentStatus']=='1')
				 || ($sendTarget == 3 && $tmpReceiverAry[$i]['SentStatus']=='0') || ($sendTarget == 4 && trim($tmpReceiverAry[$i]['AcknowledgeDate'])==''))
			{
				$receiverAry[] = $tmpReceiverAry[$i];
			}
		}
		$receiverCount = count($receiverAry);

		if(!$resend){
			$sql = "INSERT INTO ADMISSION_EMAIL_RECORD (SchoolYearID,YearID,Subject,Message,IsAcknowledge,AcknowledgeMessage,InputBy,DateInput,LastSentBy,LastSentDate) 
					VALUES ('$schoolYearId','$yearId','".$this->Get_Safe_Sql_Query($subject)."','".$this->Get_Safe_Sql_Query($message)."',
					'".($isAcknowledge==1?'1':'0')."','".$this->Get_Safe_Sql_Query($acknowledgeMessage)."','".$inputby."',NOW(),'".$inputby."',NOW())";
			$result['InsertEmailRecord'] = $this->db_db_query($sql);
			$recordId = $this->db_insert_id();
		}else{
			$sql = "UPDATE ADMISSION_EMAIL_RECORD SET 
					Subject='".$this->Get_Safe_Sql_Query($subject)."',
					Message='".$this->Get_Safe_Sql_Query($message)."',IsAcknowledge='".($isAcknowledge==1?'1':'0')."',
					AcknowledgeMessage='".$this->Get_Safe_Sql_Query($acknowledgeMessage)."',
					LastSentBy='".$inputby."',LastSentDate=NOW() WHERE RecordID='$recordId'";
			$result['UpdateEmailRecord'] = $this->db_db_query($sql);
		}

		$base_dir = $this->getEmailAttachmentBasePath();
		$email_attachment_type = $this->getEmailAttachmentType('Email');

		$existAttachmentAry = $this->getEmailAttachmentRecords($email_attachment_type, $recordId);
		$existAttachmentIdAry = Get_Array_By_Key($existAttachmentAry,'AttachmentID');
		$attachmentIdToRemove = array_values(array_diff($existAttachmentIdAry, $attachmentIdAry));
		$attachmentIdToAdd = array_values(array_diff($attachmentIdAry,$existAttachmentIdAry));

		if(count($attachmentIdToAdd)>0){
			$newAttachmentAry = $this->getEmailAttachmentRecords('', '', $attachmentIdToAdd);
		}

		if(count($attachmentIdToRemove)>0){
			foreach($attachmentIdToRemove as $k => $attachment_id){
				$this->deleteEmailAttachment($attachment_id);
			}
		}

		$new_files = array();
		if(count($newAttachmentAry)>0){
			foreach($newAttachmentAry as $k => $ary){
				$new_files[] = array('tmp_name'=>$file_path."/".$ary['FilePath'],'name'=>$ary['FileName']);
			}
			if(count($new_files)>0){
				$this->addEmailAttachment($email_attachment_type, $recordId, $new_files);
			}
		}
		$new_files2 = array();
		if($tmpAttachmentFolder != ''){ // there are new uploaded attachments
			$lft = new libfiletable("", $base_dir."/".$tmpAttachmentFolder, 0, 0, "");
			$tmp_files = $lft->files;

			while (list($key, $value) = each($tmp_files))
			{
				$tmp_full_path = $base_dir."/".$tmpAttachmentFolder."/".$tmp_files[$key][0];
				$new_files2[] = array('tmp_name'=>$tmp_full_path,'name'=>$tmp_files[$key][0]);
			}
			if(count($new_files2)>0){
				$this->addEmailAttachment($email_attachment_type, $recordId, $new_files2);
			}
			$lfs->deleteDirectory($base_dir."/".$tmpAttachmentFolder);
		}

		$allAttachmentAry = $this->getEmailAttachmentRecords($email_attachment_type, $recordId);
		$email_files = array();
		for($i=0;$i<count($allAttachmentAry);$i++){
			$email_files[] = array('tmp_name'=>$file_path."/".$allAttachmentAry[$i]['FilePath'],'name'=>$allAttachmentAry[$i]['FileName']);
		}
		$tmp_attachment_folder = $this->uploadEmailAttachmentsToTempFolder($email_files);
		if($tmp_attachment_folder != ''){
			$attachment_folder_path = $base_dir."/".$tmp_attachment_folder;
		}
		$returnReceiverId = array();
		// pre-insert the reciever records
		for($i=0;$i<$receiverCount;$i++){

			$to_email = trim($receiverAry[$i]['Email']);
			//$to_email = "carlos_t1@testmail2.broadlearning.com"; // for test
			$email_subject = $this->replaceEmailVariables($subject, $receiverAry[$i]['ChineseName'], $receiverAry[$i]['EnglishName'], $receiverAry[$i]['ApplicationNo'], $receiverAry[$i]['Status'], $receiverAry[$i]['InterviewDate'], $receiverAry[$i]['InterviewLocation'],$receiverAry[$i]['InterviewSettingID'], $receiverAry[$i]['InterviewSettingID2'], $receiverAry[$i]['InterviewSettingID3'], $receiverAry[$i]['BriefingDate'], $receiverAry[$i]['BriefingInfo'], $receiverAry[$i]['LangSpokenAtHome'], $this->getPrintLink($recordId, $receiverAry[$i]['UserID']));
			$email_message = $this->replaceEmailVariables($message, $receiverAry[$i]['ChineseName'], $receiverAry[$i]['EnglishName'], $receiverAry[$i]['ApplicationNo'], $receiverAry[$i]['Status'], $receiverAry[$i]['InterviewDate'], $receiverAry[$i]['InterviewLocation'],$receiverAry[$i]['InterviewSettingID'], $receiverAry[$i]['InterviewSettingID2'], $receiverAry[$i]['InterviewSettingID3'], $receiverAry[$i]['BriefingDate'], $receiverAry[$i]['BriefingInfo'], $receiverAry[$i]['LangSpokenAtHome'], $this->getPrintLink($recordId, $receiverAry[$i]['UserID']));

			if($resend)
			{
				$sql = "UPDATE ADMISSION_EMAIL_RECEIVER SET SentEmail='$to_email',AcknowledgeDate=NULL,ModifiedBy='".$inputby."',DateModified=NOW() WHERE ReceiverID='".$receiverAry[$i]['ReceiverID']."'";
				$result['UpdateReceiver_'.$receiverAry[$i]['UserID']] = $this->db_db_query($sql);
			}else{
				$sql = "INSERT INTO ADMISSION_EMAIL_RECEIVER (EmailRecordID,UserID,SentEmail,SentStatus,AcknowledgeDate,InputBy,DateInput,ModifiedBy,DateModified) 
						VALUES ('$recordId','".$receiverAry[$i]['UserID']."','$to_email','0',NULL,'$inputby',NOW(),'$inputby',NOW())";
				$result['InsertReceiver_'.$receiverAry[$i]['UserID']] = $this->db_db_query($sql);
				$receiverAry[$i]['ReceiverID']= $this->db_insert_id();
			}

			$returnReceiverId[] = $receiverAry[$i]['ReceiverID'];
		}

		return array('recordId'=>$recordId,'attachmentFolder'=>$tmp_attachment_folder,'receiverId'=>$returnReceiverId);
	}

	public function sendEmailToReceivers($recordId,$receiverIdAry,$attachmentFolderPath, $sendTarget=1, $deleteTmpAttachmentFolder=0)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT, $file_path;
		include_once($intranet_root."/includes/libwebmail.php");
		include_once($intranet_root."/includes/libfilesystem.php");
		include_once($PATH_WRT_ROOT."includes/libfiletable.php");

		$libwebmail = new libwebmail();
		$lfs = new libfilesystem();

		$webmaster_email = get_webmaster();
		$from = $libwebmail->GetWebmasterMailAddress();
		$inputby = $_SESSION['UserID'];

		$emailDetail = $this->getEmailRecordDetail($recordId);
		$subject = $emailDetail['Subject'];
		$message = $emailDetail['Message'];
		$isAcknowledge = $emailDetail['IsAcknowledge']=='1';
		$acknowledgeMessage = $emailDetail['AcknowledgeMessage'];

		$tmpReceiverAry = $this->getEmailReceivers('', $recordId, $receiverIdAry);
		$receiverAry = array();
		for($i=0;$i<count($tmpReceiverAry);$i++){
			if($sendTarget == 1 || $sendTarget == 5 || ($sendTarget == 2 && $tmpReceiverAry[$i]['SentStatus']=='1')
				 || ($sendTarget == 3 && $tmpReceiverAry[$i]['SentStatus']=='0') || ($sendTarget == 4 && trim($tmpReceiverAry[$i]['AcknowledgeDate'])==''))
			{
				$receiverAry[] = $tmpReceiverAry[$i];
			}
		}
		$receiverCount = count($receiverAry);

		$attachment_folder_path = '';
		$base_dir = $this->getEmailAttachmentBasePath();
		if($attachmentFolderPath != ''){
			$attachment_folder_path = $base_dir.'/'.$attachmentFolderPath;
		}
		$result = array();
		for($i=0;$i<$receiverCount;$i++){
			$email_message = '';
			if($isAcknowledge == 1){
				$encrypted_data = getEncryptedText('receiverid='.$receiverAry[$i]['ReceiverID']);
				$link = 'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_email_acknowledgement.php?q='.$encrypted_data;
				$acknowledge_link = '<div><a style="background-color: #FFFF00" href="'.$link.'" target="_blank">[ '.($acknowledgeMessage==''?$kis_lang['acknowledgementlinktext']:$acknowledgeMessage).' ]</a></div><br />';
				$email_message = $acknowledge_link;
			}

			$to_email = trim($receiverAry[$i]['Email']);
			$email_subject = $this->replaceEmailVariables($subject, $receiverAry[$i]['ChineseName'], $receiverAry[$i]['EnglishName'], $receiverAry[$i]['ApplicationNo'], $receiverAry[$i]['Status'], $receiverAry[$i]['InterviewDate'], $receiverAry[$i]['InterviewLocation'],$receiverAry[$i]['InterviewSettingID'], $receiverAry[$i]['InterviewSettingID2'], $receiverAry[$i]['InterviewSettingID3'], $receiverAry[$i]['BriefingDate'], $receiverAry[$i]['BriefingInfo'], $receiverAry[$i]['LangSpokenAtHome'], $this->getPrintLink($recordId, $receiverAry[$i]['UserID']));
			$email_message .= $this->replaceEmailVariables($message, $receiverAry[$i]['ChineseName'], $receiverAry[$i]['EnglishName'], $receiverAry[$i]['ApplicationNo'], $receiverAry[$i]['Status'], $receiverAry[$i]['InterviewDate'], $receiverAry[$i]['InterviewLocation'],$receiverAry[$i]['InterviewSettingID'], $receiverAry[$i]['InterviewSettingID2'], $receiverAry[$i]['InterviewSettingID3'], $receiverAry[$i]['BriefingDate'], $receiverAry[$i]['BriefingInfo'], $receiverAry[$i]['LangSpokenAtHome'], $this->getPrintLink($recordId, $receiverAry[$i]['UserID']));

			if($isAcknowledge == 1){
				$encrypted_data = getEncryptedText('receiverid='.$receiverAry[$i]['ReceiverID']);
				$link = 'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_email_acknowledgement.php?q='.$encrypted_data;
				$acknowledge_link = '<br /><div><a style="background-color: #FFFF00" href="'.$link.'" target="_blank">[ '.($acknowledgeMessage==''?$kis_lang['acknowledgementlinktext']:$acknowledgeMessage).' ]</a></div>';
				$email_message .= $acknowledge_link;
			}

			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$result['SendMail_'.$receiverAry[$i]['UserID']] = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),$attachment_folder_path,$IsImportant="", $webmaster_email, $reply_address="",$isMulti=null,$nl2br=0);
				$sent_ok = $result['SendMail_'.$receiverAry[$i]['UserID']];
			}else{
				$sent_ok = false;
			}
			if($sent_ok){
				$sql = "UPDATE ADMISSION_EMAIL_RECEIVER SET SentStatus='1',DateModified=NOW() WHERE ReceiverID='".$receiverAry[$i]['ReceiverID']."'";
				$this->db_db_query($sql);
			}
		}

		if($deleteTmpAttachmentFolder=='1'){
			if($attachment_folder_path != ''){
				$lfs->deleteDirectory($attachment_folder_path);
			}
		}

		return !in_array(false,$result);
	}

	public function getSendMailResultTable($recordId, $tableId='')
	{
		global $Lang, $kis_lang;

		$recordDetail = $this->getEmailRecordDetail($recordId);
		$receiverAry = $this->getEmailReceivers($recordDetail['applicationIds'],$recordId);
		$attachment_type = $this->getEmailAttachmentType('Email');
		$attachmentAry = $this->getEmailAttachmentRecords($attachment_type, $recordId);
		$attachment_count = count($attachmentAry);

		$numOfSuccessSent = 0;
		for($i=0;$i<count($receiverAry);$i++){
			if($receiverAry[$i]['SentStatus']=='1'){
				$numOfSuccessSent += 1;
			}
		}

		$x = '';
		$x.= '<table id="'.($tableId!=''?$tableId:'ComposeResultTable').'" class="form_table">
	        	<tbody>
	            	<tr> 
	                	<td class="field_title" style="width:30%;">'.$kis_lang['subject'].'</td>
	                	<td style="width:70%;">'.$recordDetail['Subject'].'</td>
	                </tr>
	                <tr> 
	                	<td class="field_title" style="width:30%;">'.$kis_lang['mailcontents'].'</td>
	                	<td style="width:70%;border:1px solid black;">'.$recordDetail['Message'].'</td>
	                </tr>
					<tr>
						<td class="field_title" style="width:30%;">'.$kis_lang['email_template_attachment'].'</td>
	                	<td style="width:70%;">';

	            if($attachment_count>0){
	            	for($i=0;$i<$attachment_count;$i++){
		            	$display_size = '&nbsp;('.$this->getAttachmentDisplaySize($attachmentAry[$i]['SizeInBytes']).')';
	    				$x .= '<div>';
						$x .= '<a class="btn_attachment" href="/kis/apps/admission/ajax.php?action=viewEmailAttachment&AttachmentID='.$attachmentAry[$i]['AttachmentID'].'" id="AttachmentFile_'.$attachmentAry[$i]['AttachmentID'].'" name="AttachmentFile[]">'.$attachmentAry[$i]['FileName'].$display_size.'</a>';
						$x .= '<input type="hidden" name="AttachmentID[]" value="'.$attachmentAry[$i]['AttachmentID'].'" />';
						$x .= '</div>';
	            	}
	            }else{
	            	$x .= '-';
	            }

				$x .=  '</td>
					</tr>
					<tr> 
	                	<td class="field_title" style="width:30%;">'.$kis_lang['noofrecipients'].'</td>
	                	<td style="width:70%;"><a href="javascript:void(0);" id="ViewResultBtn">'.$numOfSuccessSent.' / '.count($receiverAry).'</a></td>
	                </tr>
					<tr> 
	                	<td class="field_title" style="width:30%;">'.$kis_lang['lastsenton'].'</td>
	                	<td style="width:70%;">'.$recordDetail['LastSentDate'].'</td>
	                </tr>
	            </tbody>
	        </table>';

	    $x .= '<script type="text/javascript" language="JavaScript">';
	    $x .= '$("a#ViewResultBtn").click(function(){
				var recordID = "'.$recordId.'";
				var applicantionIds ="";
				$.post(
					\'apps/admission/ajax.php?action=getEmailReceiversLayer\',
					{
						\'recordID\' : recordID,
						\'applicantionIds\' : applicantionIds
					},
					function(data){
						$.fancybox(data, {
							\'fitToView\' : true,
							\'autoDimensions\' : true,
							\'autoScale\' : false,
							\'autoSize\' : true,
							\'padding\' : 20,
							\'transitionIn\' : \'elastic\',
							\'transitionOut\' : \'elastic\'
						});
					}
				);						
			});';
	    $x .= '</script>';

	    return $x;
	}

	public function getSelectHandler($params, $exclude_list=false){
    	global $intranet_root;

    	include_once($intranet_root."/includes/role_manage.php");
    	include_once($intranet_root."/includes/form_class_manage.php");

    	$lrole = new role_manage();
    	$fcm = new form_class_manage();
    	//$academic_year_id = $_SESSION['CurrentSchoolYearID'];
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

    	extract($params);

    	// $user_type: selected user type - 1=Teaching staff, 2=Student, 3=Non teaching staff, 4=Parent
    	$where_cond = "";
    	if($user_type == 1) {
    		$where_cond .= " AND u.RecordType='".USERTYPE_STAFF."' AND u.Teaching='1' ";
    	}else if($user_type == 3) {
    		$where_cond .= " AND u.RecordType='".USERTYPE_STAFF."' AND (u.Teaching='0' OR u.Teaching IS NULL) ";
    	}else if($user_type == 2) {
    		$where_cond .= " AND u.RecordType='".USERTYPE_STUDENT."' ";
    	}else if($user_type == 4) {
    		$where_cond .= " AND u.RecordType='".USERTYPE_PARENT."' ";
    	}

    	// $user_group : selected GroupID, '' or single integer
    	if($user_group != '') {
    		$where_cond .= " AND ug.GroupID='$user_group' ";
    	}

    	$keyword = $this->Get_Safe_Sql_Like_Query($keyword);


//    	if(!$_SESSION['SSV_USER_TARGET']['All-Yes'])
//    	{
//    		$filterUserIdAry = array();
//    		$identity = $lrole->Get_Identity_Type($this->user_id); // 'Student' | 'Parent' | 'Teaching' | 'NonTeaching' | 'Alumni'
//
//    		if($identity == 'Teaching' || $identity == 'NonTeaching' || $identity == 'Student' || $identity == 'Parent') {
//    			if($identity == 'Teaching' || $identity == 'NonTeaching') {
//	    			// My class by teacher type
//	    			$sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON a.YearClassID = b.YearClassID WHERE a.UserID = '".$this->user_id."' AND b.AcademicYearID='$CurrentAcademicYearID'";
//					$arrTargetYearClassID = $this->returnVector($sql);
//					$targetYearClassID = count($arrTargetYearClassID)>0? implode(",",$arrTargetYearClassID):"-1";
//
//	    			// My subject group by teacher type
//	    			$sql = "SELECT DISTINCT a.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON a.SubjectGroupID = b.SubjectGroupID WHERE a.UserID='".$this->user_id."' AND b.YearTermID='$CurrentTermID'";
//					$arrTargetSubjectGroupID = $this->returnVector($sql);
//					$targetSubjectGroupID = count($arrTargetSubjectGroupID)>0? implode(",",$arrTargetSubjectGroupID):"-1";
//    			}else if($identity == 'Student'){
//    				// My class by student type
//	    			$sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON a.YearClassID = b.YearClassID WHERE a.UserID = '".$this->user_id."' AND b.AcademicYearID='$CurrentAcademicYearID'";
//					$arrTargetYearClassID = $this->returnVector($sql);
//					$targetYearClassID = count($arrTargetYearClassID)>0? implode(",",$arrTargetYearClassID):"-1";
//
//	    			// My subject group by student type
//	    			$sql = "SELECT DISTINCT a.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON a.SubjectGroupID = b.SubjectGroupID WHERE a.UserID='".$this->user_id."' AND b.YearTermID='$CurrentTermID'";
//					$arrTargetSubjectGroupID = $this->returnVector($sql);
//					$targetSubjectGroupID = count($arrTargetSubjectGroupID)>0? implode(",",$arrTargetSubjectGroupID):"-1";
//    			}else if($identity == 'Parent') {
//    				// My class by parent type, get my children's class
//	    			$sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON a.YearClassID = b.YearClassID INNER JOIN INTRANET_PARENTRELATION as r ON r.StudentID=a.UserID WHERE r.ParentID = '".$this->user_id."' AND b.AcademicYearID='$CurrentAcademicYearID'";
//					$arrTargetYearClassID = $this->returnVector($sql);
//					$targetYearClassID = count($arrTargetYearClassID)>0? implode(",",$arrTargetYearClassID):"-1";
//
//	    			// My subject group by parent type, get my children's class
//	    			$sql = "SELECT DISTINCT a.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON a.SubjectGroupID = b.SubjectGroupID INNER JOIN INTRANET_PARENTRELATION as r ON r.StudentID=a.UserID WHERE r.ParentID='".$this->user_id."' AND b.YearTermID='$CurrentTermID'";
//					$arrTargetSubjectGroupID = $this->returnVector($sql);
//					$targetSubjectGroupID = count($arrTargetSubjectGroupID)>0? implode(",",$arrTargetSubjectGroupID):"-1";
//    			}
//
//    			// Same form teachers or same class teachers
//    			if(!$_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'] && ($_SESSION['SSV_USER_TARGET']['Staff-MyForm'] || $_SESSION['SSV_USER_TARGET']['Staff-MyClass'])) {
//					$sql = "SELECT DISTINCT UserID FROM YEAR_CLASS_TEACHER WHERE YearClassID IN ($targetYearClassID)";
//    				$teacherUserIdAry = $this->returnVector($sql);
//    				$filterUserIdAry = array_merge($filterUserIdAry,$teacherUserIdAry);
//    			}
//    			// Same subject group teachers
//    			if(!$_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'] && ($_SESSION['SSV_USER_TARGET']['Staff-MySubject'] || $_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'])) {
//					$sql = "SELECT DISTINCT UserID FROM SUBJECT_TERM_CLASS_TEACHER WHERE SubjectGroupID IN ($targetSubjectGroupID)";
//					$teacherUserIdAry = $this->returnVector($sql);
//    				$filterUserIdAry = array_merge($filterUserIdAry,$teacherUserIdAry);
//    			}
//    			// Same form students or same class students
//    			if(!$_SESSION['SSV_USER_TARGET']['Student-All'] && ($_SESSION['SSV_USER_TARGET']['Student-MyForm'] || $_SESSION['SSV_USER_TARGET']['Student-MyClass'])){
//    				$sql = "SELECT DISTINCT UserID FROM YEAR_CLASS_USER WHERE YearClassID IN ($targetYearClassID)";
//    				$classStudentUserIdAry = $this->returnVector($sql);
//    				$filterUserIdAry = array_merge($filterUserIdAry,$classStudentUserIdAry);
//    			}
//    			// Same subject group students
//    			if(!$_SESSION['SSV_USER_TARGET']['Student-All'] && ($_SESSION['SSV_USER_TARGET']['Student-MySubject'] || $_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'])){
//    				$sql = "SELECT DISTINCT UserID FROM SUBJECT_TERM_CLASS_USER WHERE SubjectGroupID IN ($targetSubjectGroupID)";
//					$subjectStudentUserIdAry = $this->returnVector($sql);
//    				$filterUserIdAry = array_merge($filterUserIdAry,$subjectStudentUserIdAry);
//    			}
//    			// Same form student's parents or same class student's parents
//    			if(!$_SESSION['SSV_USER_TARGET']['Parent-All'] && ($_SESSION['SSV_USER_TARGET']['Parent-MyForm'] ||$_SESSION['SSV_USER_TARGET']['Parent-MyClass'])){
//    				$sql = "SELECT DISTINCT r.ParentID FROM YEAR_CLASS_USER as ycu INNER JOIN INTRANET_PARENTRELATION as r ON r.StudentID=ycu.UserID WHERE ycu.YearClassID IN ($targetYearClassID)";
//    				$classParentUserIdAry = $this->returnVector($sql);
//    				$filterUserIdAry = array_merge($filterUserIdAry,$classParentUserIdAry);
//    			}
//    			// Same subject group student's parents
//    			if(!$_SESSION['SSV_USER_TARGET']['Parent-All'] && ($_SESSION['SSV_USER_TARGET']['Parent-MySubject'] || $_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'])){
//    				$sql = "SELECT DISTINCT r.ParentID FROM SUBJECT_TERM_CLASS_USER as a INNER JOIN INTRANET_PARENTRELATION as r ON r.StudentID=a.UserID WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
//					$subjectParentUserIdAry = $this->returnVector($sql);
//    				$filterUserIdAry = array_merge($filterUserIdAry,$subjectParentUserIdAry);
//    			}
//    		}
//
//			// Same group teaching-teachers, or non-teaching teachers, or students, or parents
//			if((!$_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'] && $_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])
//				 || (!$_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching'] && $_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])
//				 || (!$_SESSION['SSV_USER_TARGET']['Student-All'] && $_SESSION['SSV_USER_TARGET']['Student-MyGroup'])
//				 || (!$_SESSION['SSV_USER_TARGET']['Parent-All'] && $_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])) {
//
//				$allow_user_types = array(0);
//				if($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'] || $_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) {
//					$allow_user_types[] = USERTYPE_STAFF;
//				}
//				if($_SESSION['SSV_USER_TARGET']['Student-All']){
//					$allow_user_types[] = USERTYPE_STUDENT;
//				}
//				if($_SESSION['SSV_USER_TARGET']['Parent-All']){
//					$allow_user_types[] = USERTYPE_PARENT;
//				}
//
//				// My Groups
//				$sql = "SELECT DISTINCT b.GroupID FROM INTRANET_USERGROUP as a INNER JOIN INTRANET_GROUP as b ON b.GroupID=a.GroupID WHERE a.UserID='".$this->user_id."' AND b.AcademicYearID='$CurrentAcademicYearID'";
//				$arrTargetGroupID = $this->returnVector($sql);
//				$targetGroupID = count($arrTargetGroupID)>0? implode(",",$arrTargetGroupID):"-1";
//
//				$sql = "SELECT DISTINCT a.UserID FROM INTRANET_USER as a INNER JOIN INTRANET_USERGROUP as b ON a.UserID=b.UserID WHERE (b.GroupID IN ($targetGroupID) AND a.RecordType IN (".implode(",",$allow_user_types).")) ";
//				if($_SESSION['SSV_USER_TARGET']['Staff-MyGroup']) {
//					$sql .= " OR (b.GroupID IN ($targetGroupID) AND a.RecordType='".USERTYPE_STAFF."' AND a.Teaching='1') ";
//				}
//				if($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup']) {
//					$sql .= " OR (b.GroupID IN ($targetGroupID) AND a.RecordType='".USERTYPE_STAFF."' AND (a.Teaching!='1' OR a.Teaching IS NULL)) ";
//				}
//				if($_SESSION['SSV_USER_TARGET']['Student-MyGroup']) {
//					$sql .= " OR (b.GroupID IN ($targetGroupID) AND a.RecordType='".USERTYPE_STUDENT."') ";
//				}
//				if($_SESSION['SSV_USER_TARGET']['Parent-MyGroup']) {
//					$sql .= " OR (b.GroupID IN ($targetGroupID) AND a.RecordType='".USERTYPE_PARENT."') ";
//				}
//
//				$groupUserIdAry = $this->returnVector($sql);
//				$filterUserIdAry = array_merge($filterUserIdAry,$groupUserIdAry);
//			}
//
//    		if(!$_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'] && !$_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']
//    			&& !$_SESSION['SSV_USER_TARGET']['Student-All'] && !$_SESSION['SSV_USER_TARGET']['Parent-All']
//    			 && count($filterUserIdAry)==0) {
//    			$filterUserIdAry[] = -1;
//    		}
//    		$where_cond .= " AND u.UserID IN (".implode(",",$filterUserIdAry).") ";
//    	} // End of filtering mail target

    	$exclude_list = $exclude_list? " AND u.UserID NOT IN ($exclude_list) ":"";

    	$sql = "SELECT DISTINCT u.UserID as user_id,
				    u.EnglishName as user_name_en,
				    u.ChineseName as user_name_b5,
				    u.ClassName as user_class_name,
				    u.PhotoLink as user_photo,
				    u.RecordType as user_type
				FROM INTRANET_USER as u 
				INNER JOIN INTRANET_USERGROUP as ug ON u.UserID = ug.UserID 
				INNER JOIN INTRANET_GROUP as g ON g.GroupID=ug.GroupID AND (g.AcademicYearID='$CurrentAcademicYearID' OR ug.GroupID <= 4)
				WHERE u.RecordStatus='1' $where_cond $exclude_list 
					AND (g.AcademicYearID='$CurrentAcademicYearID' OR ug.GroupID <= 4)
					AND (u.UserLogin LIKE '%$keyword%' OR u.EnglishName LIKE '%$keyword%' OR u.ChineseName LIKE '%$keyword%' OR u.ClassName LIKE '%$keyword%')
				ORDER BY u.EnglishName";

    	return $this->returnArray($sql);
    }

    public function getUpdateStatusAry($schoolYearID=''){
		global $admission_cfg;

     	$from_table = "		    
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN 
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
			LEFT JOIN YEAR as y ON y.YearID=o.ApplyLevel
			LEFT JOIN INTRANET_USER as u ON u.UserID=s.ModifiedBy	
			WHERE s.ModifiedBy IS NOT NULL 
			
    	";
    	$sql = "SELECT DISTINCT o.ApplicationID ".$from_table." $limit";
    	$applicationIDAry = $this->returnVector($sql);
    	$sql = "
			SELECT
				o.RecordID AS record_id,
     			stu.ApplicationID AS application_id,
				y.YearName as ClassLevelName,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status, ".getNameFieldByLang2("u.")." AS modified_by, s.DateModified AS date_modified ".$from_table."  ORDER BY date_modified desc limit 100";

    	$applicationAry = $this->returnArray($sql);
    	return $applicationAry;
	}

	public function updateApplicationStatusByApplicationNos($applicationIds,$status){
		global $UserID;

		$applicationIdAry = explode(',',$applicationIds);
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID 
			SET	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."'
     		WHERE 
				o.ApplicationID IN ('".implode("','", $applicationIdAry)."')
    	";
    	return $this->db_db_query($sql);
	}

	public function getEmailAttachmentBasePath()
	{
		global $intranet_root, $file_path;

		$base_dir = $file_path."/file/kis";

		if(!file_exists($base_dir) || !is_dir($base_dir)){
			mkdir($base_dir, 0777, true);
		}

		$base_dir = $file_path."/file/kis/admission";
		if(!file_exists($base_dir) || !is_dir($base_dir)){
			mkdir($base_dir, 0777, true);
		}

		$base_dir = $file_path."/file/kis/admission/email";
		if(!file_exists($base_dir) || !is_dir($base_dir)){
			mkdir($base_dir, 0777, true);
		}

		return $base_dir;
	}

	public function getEmailTemplateType($key)
	{
		return $this->EMAIL_TEMPLATE_TYPE[$key];
	}

	public function getEmailAttachmentType($key)
	{
		return $this->EMAIL_ATTACHMENT_TYPE[$key];
	}

	/*
	 * $linkRecordType: 1=Email attachment, 2=Email template attachment
	 * $linkRecordID: ADMISSION_EMAIL_TEMPLATE.TemplateID or ADMISSION_EMAIL_RECORD.RecordID
	 * $fileAry: array(array('tmp_name'=> temp file path, 'name'=> original file name), ...)
	 */
	public function addEmailAttachment($linkRecordType, $linkRecordID, $fileAry)
	{
		global $intranet_root, $file_path;

		$base_dir = $this->getEmailAttachmentBasePath();

		$input_by = $this->user_id != '' ? $this->user_id : $_SESSION['UserID'];

		$resultAry = array();
		if(count($fileAry) > 0){

			foreach($fileAry as $fileKey => $fileInfoAry)
			{
				$tmp_path = $fileInfoAry['tmp_name'];
				$file_name = stripslashes($fileInfoAry['name']);

				if($file_name == '' || $tmp_path == '') continue;

				$size_bytes = filesize($tmp_path);
				$sql = "INSERT INTO ADMISSION_EMAIL_ATTACHMENT (LinkRecordType,LinkRecordID,FileName,FilePath,SizeInBytes,DateInput,InputBy) 
						VALUES ('$linkRecordType','$linkRecordID','".$this->Get_Safe_Sql_Query($file_name)."','','$size_bytes',NOW(),'$input_by')";
				$resultAry['InsertRecord'.$fileKey] = $this->db_db_query($sql);
				if($resultAry['InsertRecord'.$fileKey]){
					$attachment_id = $this->db_insert_id();

					$chunk_number = ceil($attachment_id / 1500);
					$target_dir = $base_dir."/".$chunk_number;
					if(!file_exists($target_dir) || !is_dir($target_dir)){
						mkdir($target_dir, 0777, true);
					}
					$target_path = $target_dir."/".$attachment_id;
					$copy_success = copy($tmp_path, $target_path);
					chmod($target_path,"0777");
					$save_path = str_replace($file_path, "" ,$target_path);

					$sql = "UPDATE ADMISSION_EMAIL_ATTACHMENT SET FilePath='$save_path' WHERE AttachmentID='$attachment_id'";
					$resultAry['UpdateRecord'.$fileKey] = $this->db_db_query($sql);
				}
			}
		}

		return !in_array(false,$resultAry);
	}

	public function deleteEmailAttachment($attachmentId)
	{
		global $intranet_root, $file_path;

		$sql = "SELECT FilePath FROM ADMISSION_EMAIL_ATTACHMENT WHERE AttachmentID='$attachmentId'";
		$record = $this->returnVector($sql);
		$path = $record[0];

		$resultAry = array();
		if($path != ''){
			$full_path = $file_path.$path;

			if(file_exists($full_path)){
				$resultAry['UnlinkFile'] = unlink($full_path);
			}
		}
		$sql = "DELETE FROM ADMISSION_EMAIL_ATTACHMENT WHERE AttachmentID='$attachmentId'";
		$resultAry['DeleteRecord'] = $this->db_db_query($sql);

		return !in_array(false,$resultAry);
	}

	public function getEmailAttachmentRecords($linkRecordType='', $linkRecordID='', $attachmentIdAry=array())
	{
		$sql = "SELECT * FROM ADMISSION_EMAIL_ATTACHMENT WHERE 1 ";
		if($linkRecordType != ''){
			$sql.= " AND LinkRecordType='$linkRecordType' ";
		}
		if($linkRecordID != ''){
			$sql.= " AND LinkRecordID='$linkRecordID' ";
		}
		if(count($attachmentIdAry)>0){
			$sql .= " AND AttachmentID IN ('".implode("','",$attachmentIdAry)."')";
		}
		$sql .= " ORDER BY DateInput";

		$records = $this->returnArray($sql);
		return $records;
	}

	public function getEmailTemplateRecords($recordType='',$templateIdAry='')
	{
		global $PATH_WRT_ROOT, $Lang;
		$name_field = getNameFieldByLang("u.");
		$sql = "SELECT 
					t.TemplateID, t.Title, t.Content, t.RecordType, t.Sequence, COUNT(a.AttachmentID) as NumberOfAttachment, t.DateModified, $name_field as ModifiedBy, t.TemplateType, t.TemplateClassLevel
				FROM ADMISSION_EMAIL_TEMPLATE as t 
				LEFT JOIN ADMISSION_EMAIL_ATTACHMENT as a ON a.LinkRecordType='2' AND a.LinkRecordID=t.TemplateID 
				LEFT JOIN INTRANET_USER as u ON u.UserID=t.ModifiedBy 
				WHERE 1 ";
		if($templateIdAry != '' && count($templateIdAry)>0){
			$sql .= " AND t.TemplateID IN ('".implode("','",(array)$templateIdAry)."')";
		}
		if($recordType != ''){
			$sql .= " AND t.RecordType='$recordType' ";
		}
		$sql.= "GROUP BY t.TemplateID ORDER BY t.Sequence";

		$records = $this->returnArray($sql);
		return $records;
	}

	// return TemplateID for success, 0 for failure
	public function updateEmailTemplateRecord($title,$content,$recordType,$templateID='', $TemplateType='0', $TemplateClassLevel=array())
	{
		$title_value = $this->Get_Safe_Sql_Query($title);
		$content_value = $this->Get_Safe_Sql_Query($content);
	    $TemplateClassLevelSql = implode(',', (array)$TemplateClassLevel);

		$input_by = $this->user_id != '' ? $this->user_id : $_SESSION['UserID'];

		$success = true;
		if($templateID != ''){
			$sql = "UPDATE ADMISSION_EMAIL_TEMPLATE SET Title='$title_value',Content='$content_value',RecordType='$recordType',DateModified=NOW(),ModifiedBy='$input_by',TemplateType='$TemplateType',TemplateClassLevel='{$TemplateClassLevelSql}' WHERE TemplateID='$templateID'";
			$success = $this->db_db_query($sql);
		}else{
			$sql = "SELECT MAX(Sequence) FROM ADMISSION_EMAIL_TEMPLATE;";
			$maxRecord = $this->returnVector($sql);
			$sequence = $maxRecord[0]+1;
			$sql = "INSERT INTO ADMISSION_EMAIL_TEMPLATE (Title,Content,RecordType,Sequence,DateInput,DateModified,InputBy,ModifiedBy, TemplateType, TemplateClassLevel)
					VALUES ('$title_value','$content_value','$recordType','$sequence',NOW(),NOW(),'$input_by','$input_by', '$TemplateType', '{$TemplateClassLevelSql}')";
			$success = $this->db_db_query($sql);
			$templateID = $this->db_insert_id();
		}

		return $success? $templateID : 0;
	}

	public function deleteEmailTemplateRecord($templateIdAry)
	{
		 $templateIdAry =(array)$templateIdAry;
		 $resultAry = array();
		 $attachment_type = $this->getEmailAttachmentType('EmailTemplate');
		 if(count($templateIdAry)>0){
		 	foreach($templateIdAry as $k => $id)
		 	{
		 		$sql = "SELECT AttachmentID FROM ADMISSION_EMAIL_ATTACHMENT WHERE LinkRecordType='$attachment_type' AND LinkRecordID='$id'";
		 		$attachmentIds = $this->returnVector($sql);
		 		$attachment_count = count($attachmentIds);
		 		for($i=0;$i<$attachment_count;$i++){
		 			$resultAry['DeleteAttachment_'.$id."_".$i] = $this->deleteEmailAttachment($attachmentIds[$i]);
		 		}
		 	}

		 	$sql = "DELETE FROM ADMISSION_EMAIL_TEMPLATE WHERE TemplateID IN (".implode(",",$templateIdAry).")";
		 	$resultAry['DeleteTemplateRecords'] = $this->db_db_query($sql);
		 }
		 return !in_array(false,$resultAry);
	}

	public function reorderEmailTemplateRecords($templateIdAry)
	{
		$result = array();
		for($i=0;$i<sizeof($templateIdAry);$i++){
			$sql = "UPDATE ADMISSION_EMAIL_TEMPLATE SET Sequence='".($i+1)."' WHERE TemplateID='".$templateIdAry[$i]."'";
			 $result[] = $this->db_db_query($sql);
		}
		return !in_array(false,$result);
	}

	/*
	 * $fileAry: array(array('tmp_name'=> temp file path, 'name'=> original file name), ...)
	 */
	public function uploadEmailAttachmentsToTempFolder($fileAry)
	{
		global $intranet_root, $file_path;

		$file_count = 0;
		if(count($fileAry)>0){
			foreach($fileAry as $fileKey => $fileInfoAry)
			{
				$tmp_path = $fileInfoAry['tmp_name'];
				$file_name = $fileInfoAry['name'];

				if($file_name == '' || $tmp_path == '') continue;
				$file_count+=1;
			}
		}

		if($file_count == 0) return '';

		$base_dir = $this->getEmailAttachmentBasePath();
		$user_id = $this->user_id != '' ? $this->user_id : $_SESSION['UserID'];
		$tmp_folder_name = "TMP".MD5(time()."U".$user_id);

		$target_dir = $base_dir."/".$tmp_folder_name;
		if(!file_exists($target_dir) || !is_dir($target_dir)){
			mkdir($target_dir, 0777, true);
		}

		if(count($fileAry)>0){
			foreach($fileAry as $fileKey => $fileInfoAry)
			{
				$tmp_path = $fileInfoAry['tmp_name'];
				$file_name = $fileInfoAry['name'];

				if($file_name == '' || $tmp_path == '') continue;

				$target_path = $target_dir."/".$file_name;
				$copy_success = copy($tmp_path, $target_path);
			}
		}

		return $tmp_folder_name;
	}

	public function getAttachmentDisplaySize($size_bytes)
	{
		if($size_bytes < 1024){
			$display_size = $size_bytes.' bytes';
		}else if($size_bytes < 1024 * 1024){
			$display_size = sprintf("%.2f",$size_bytes/1024).' KB';
		}else{
			$display_size = sprintf("%.2f",($size_bytes/(1024*1024))).' MB';
		}
		return $display_size;
	}

	// for Interview Arrangement Settings [Start]
	public function getInterviewArrangementRecords($recordIdAry='', $round=1)
	{
		global $PATH_WRT_ROOT, $Lang;

		$sql = "SELECT a.RecordID, a.Date, a.NumOfGroup, a.Quota , s.StartTime, s.EndTime, s.Group, a.Extra
				FROM ADMISSION_INTERVIEW_ARRANGEMENT as a
				JOIN ADMISSION_INTERVIEW_ARRANGEMENT_SESSION as s ON a.RecordID = s.RecordID
				WHERE 1 ";
		if($recordIdAry != '' && count($recordIdAry)>0){
			$sql .= " AND a.RecordID IN ('".implode("','",(array)$recordIdAry)."') ";
		}

		if($round != ''){
			$sql .= " AND a.Round = '".$round."' ";
		}

		$sql .=" ORDER BY a.Sequence, a.Date, s.StartTime";

		$records = $this->returnArray($sql);

		$recordById = array();
		foreach($records as $aRecord){
			$recordById[$aRecord['RecordID']]['RecordID'] = $aRecord['RecordID'];
			$recordById[$aRecord['RecordID']]['Date'] = $aRecord['Date'];
			$recordById[$aRecord['RecordID']]['NumOfGroup'] = $aRecord['NumOfGroup'];
			$recordById[$aRecord['RecordID']]['Quota'] = $aRecord['Quota'];
			$recordById[$aRecord['RecordID']]['StartTime'][] = $aRecord['StartTime'];
			$recordById[$aRecord['RecordID']]['EndTime'][] = $aRecord['EndTime'];
			$recordById[$aRecord['RecordID']]['Group'] = $aRecord['Group'];
			$recordById[$aRecord['RecordID']]['Extra'] = $aRecord['Extra'];
		}
		return $recordById;
	}

	public function updateInterviewArrangementRecord($date,$numOfGroup,$quota,$sessionAry,$recordID='',$round=1,$selectGroups='', $extra='')
	{
		$input_by = $this->user_id != '' ? $this->user_id : $_SESSION['UserID'];

		$success = true;
		if($recordID != ''){
			$sql = "UPDATE ADMISSION_INTERVIEW_ARRANGEMENT SET Date='$date',NumOfGroup='$numOfGroup',Quota='$quota',DateModified=NOW(),ModifiedBy='$input_by',Round='$round', Extra='$extra' WHERE RecordID='$recordID'";
			$success = $this->db_db_query($sql);
		}else{
			$sql = "SELECT MAX(Sequence) FROM ADMISSION_INTERVIEW_ARRANGEMENT";
			$maxRecord = $this->returnVector($sql);
			$sequence = $maxRecord[0]+1;
			$sql = "INSERT INTO ADMISSION_INTERVIEW_ARRANGEMENT (Date,NumOfGroup,Quota,Sequence,DateInput,DateModified,InputBy,ModifiedBy,Round, Extra)
					VALUES ('$date','$numOfGroup','$quota','$sequence',NOW(),NOW(),'$input_by','$input_by','$round', '$extra')";
			$success = $this->db_db_query($sql);
			$recordID = $this->db_insert_id();
		}

		if($success){
			$sql = "DELETE FROM ADMISSION_INTERVIEW_ARRANGEMENT_SESSION WHERE RecordID = '".$recordID."'";
			$success = $this->db_db_query($sql);
		}

		foreach($sessionAry as $aSessionAry){
			if($success)
				$success = $this->updateInterviewArrangementSessionRecord($aSessionAry['StartTime'], $aSessionAry['EndTime'], $recordID, $selectGroups);
		}

		return $success? $recordID : 0;
	}

	public function updateInterviewArrangementSessionRecord($startTime, $endTime, $recordID, $selectGroups)
	{
		$input_by = $this->user_id != '' ? $this->user_id : $_SESSION['UserID'];

		$selectGroupSql = implode(',', $selectGroups);

		$success = true;
		if($success){
			$sql = "INSERT INTO ADMISSION_INTERVIEW_ARRANGEMENT_SESSION (RecordID, StartTime, EndTime, DateInput,DateModified,InputBy,ModifiedBy,`Group`)
				VALUES ('$recordID', '$startTime', '$endTime', NOW(), NOW(), '$input_by', '$input_by', '$selectGroupSql')";
		}
		$success = $this->db_db_query($sql);

		return $success;
	}

	public function deleteInterviewArrangementRecord($recordIdAry)
	{
		 $recordIdAry =(array)$recordIdAry;
		 $resultAry = array();

		 $sql = "DELETE FROM ADMISSION_INTERVIEW_ARRANGEMENT_SESSION WHERE RecordID IN (".implode(",",$recordIdAry).")";
	 	 $resultAry[] = $this->db_db_query($sql);
	 	 $sql = "DELETE FROM ADMISSION_INTERVIEW_ARRANGEMENT WHERE RecordID IN (".implode(",",$recordIdAry).")";
	 	 $resultAry[] = $this->db_db_query($sql);

		 return !in_array(false,$resultAry);
	}

	public function reorderInterviewArrangementRecords($recordIdAry)
	{
		$result = array();
		for($i=0;$i<sizeof($recordIdAry);$i++){
			$sql = "UPDATE ADMISSION_INTERVIEW_ARRANGEMENT SET Sequence='".($i+1)."' WHERE RecordID='".$recordIdAry[$i]."'";
			 $result[] = $this->db_db_query($sql);
		}
		return !in_array(false,$result);
	}
	// for Interview Arrangement Settings [End]

	function getInterviewFromInputListAry($recordID='',  $date='', $startTime='', $endTime='', $keyword='', $order='', $sortby='', $round=1, $donotassignteacherresult=0){
		global $admission_cfg;

		//$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;

		if($recordID != ''){
			$cond = " AND i.RecordID IN ('".implode("','",(array)($recordID))."') ";
		}
		if($date != ''){
			$cond .= ' AND i.Date >= \''.$date.'\' ';
		}
		if($startTime != ''){
			$cond .= ' AND i.StartTime >= \''.$startTime.'\' ';
		}
		if($endTime != ''){
			$cond .= ' AND i.EndTime <= \''.$endTime.'\' ';
		}
		if($keyword != ''){
			$search_cond = ' AND (i.Date LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\' 
							OR i.StartTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR i.EndTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\')';
		}
		$sort = $sortby? "$sortby $order":"application_id";

//		if(!empty($keyword)){
//			$cond .= "
//				AND (
//					stu.EnglishName LIKE '%".$keyword."%'
//					OR stu.ChineseName LIKE '%".$keyword."%'
//					OR stu.ApplicationID LIKE '%".$keyword."%'
//					OR pg.EnglishName LIKE '%".$keyword."%'
//					OR pg.ChineseName LIKE '%".$keyword."%'
//				)
//			";
//		}

     	$from_table = "
			FROM 
				ADMISSION_INTERVIEW_SETTING AS i		    
			LEFT JOIN
				ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID".($round>1?$round:'')." 
			INNER JOIN 
				ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID 
			WHERE
				1 	
			".$cond." 
			order by ".$sort."
			
    	";
    	$sql = "SELECT o.ApplicationID as ApplicationID, i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.Quota as Quota,
				i.RecordID AS record_id, o.RecordID AS other_record_id,
     			stu.ApplicationID AS application_id,
				".getNameFieldByLang2("stu.")." AS student_name,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." ";
     	//debug_r($sql);
     	if($donotassignteacherresult==1){
     		$sql = "SELECT o.RecordID AS other_record_id, o.ApplicationID AS application_id FROM 
				ADMISSION_INTERVIEW_SETTING AS i		    
			LEFT JOIN
				ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID".($round>1?$round:'')." 
			WHERE
				1 	
			".$cond." 
			order by ".$sort." ";
     	}
    	$applicationAry = $this->returnArray($sql);

    	if($donotassignteacherresult==0){
    	// assign teacher result to array [start]
    	$name_field = getNameFieldByLang("");
    	for($i=0; $i < sizeof($applicationAry); $i++){
    		$sql = "SELECT '.$name_field.' as InputBy, DateModified 
					FROM ADMISSION_INTERVIEW_FORM
					WHERE Status = '1' AND ApplicationID = '".$applicationAry[$i]['ApplicationID']."' AND Round = '".$round."'";
			$result = $this->returnArray($sql);

			foreach($result as $aResult){
				$applicationAry[$i]['InputBy'][] = $aResult['InputBy'];
				$applicationAry[$i]['DateModified'][] = $aResult['DateModified'];
			}

    	}

    	// assign teacher result to array [end]
    	}
    	return $applicationAry;
	}

	public function removeApplicationInterviewRecord($recordIds, $round=1){
		if($recordIds=="") return false;

		//$applicationIdsSql = "'".implode("','",(array)$applicationIds)."'";
		$recordIds = is_array($recordIds)? "'".implode("','", $recordIds)."'" : "'".implode("','",explode(",",$recordIds))."'";

		if($round == 1){
			$round = '';
		}

		$sql = "
			UPDATE 
				ADMISSION_OTHERS_INFO Set InterviewSettingID".$round." = '0' 
			WHERE
				RecordID IN (".$recordIds.")
		";
		$success[] = $this->db_db_query($sql);

		if(in_array(false, $success))
			return false;
		else
			return true;

	}

	public function getUnassignedInterviewApplicantReocrd($interviewSettingID, $data=array()){
		global $admission_cfg;
		extract($data);

		$sql = "Select SchoolYearID, ClassLevelID, Round 
				From ADMISSION_INTERVIEW_SETTING 
				Where RecordID = '".$interviewSettingID."'";
		$interviewSettingResult = current($this->returnArray($sql));
		//debug_pr($interviewSettingResult);
		$sql = "Select StatusSelect 
				From ADMISSION_INTERVIEW_ARRANGEMENT_LOG 
				where SchoolYearID = '".$interviewSettingResult['SchoolYearID']."' AND Round = '".($interviewSettingResult['Round']?$interviewSettingResult['Round']:1)."' order by DateInput desc";
		$statusResult = $this->returnArray($sql);

		$statusSelect = $statusResult[0]['StatusSelect'];

		$cond = '';
		if($statusSelect){
			$cond .= " AND s.Status IN (".$statusSelect.") ";
		}

		if($interviewSettingResult['SchoolYearID'] != ''){
			$cond .= ' AND s.SchoolYearID = \''.$interviewSettingResult['SchoolYearID'].'\' ';
		}

		$round = ($interviewSettingResult['Round'] > 1?$interviewSettingResult['Round']:'');

		$sort = $sortby? "$sortby $order":"application_id";
		$limit = $page? " LIMIT ".(($page-1)*$amount).", $amount": "";
		$cond .= !empty($interviewSettingResult['ClassLevelID'])?" AND o.ApplyLevel='".$interviewSettingResult['ClassLevelID']."'":"";
		$cond .= !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= $status?" AND s.Status='".$status."'":"";
		if($status==$admission_cfg['Status']['waitingforinterview']){
			if($interviewStatus==1){
				$cond .= " AND s.isNotified = 1";
			}else{
				$cond .= " AND (s.isNotified = 0 OR s.isNotified IS NULL)";
			}
		}
		if(!empty($keyword)){
			$cond .= "
				AND ( 
					stu.EnglishName LIKE '%".$keyword."%'
					OR stu.ChineseName LIKE '%".$keyword."%'
					OR stu.ApplicationID LIKE '%".$keyword."%'					
					OR pg.EnglishName LIKE '%".$keyword."%'
					OR pg.ChineseName LIKE '%".$keyword."%'
					OR pg.Mobile LIKE '%".$keyword."%'
					OR stu.BirthCertNo LIKE '%".$keyword."%' 				
				)
			";
		}

     	$from_table = "		    
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN 
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID	
			WHERE
				o.ApplyYear = '".$interviewSettingResult['SchoolYearID']."'	
				AND o.InterviewSettingID".$round." <= 0 
			".$cond." 
			
    	";
    	$sql = "SELECT DISTINCT o.ApplicationID ".$from_table;
    	$applicationIDAry = $this->returnVector($sql);
//    	$sql2 = "SELECT DISTINCT o.ApplicationID ".$from_table;
//    	$applicationIDCount = $this->returnVector($sql2);
    	$sql = "
			SELECT
				o.RecordID AS record_id,
     			stu.ApplicationID AS application_id,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." AND o.ApplicationID IN ('".implode("','",$applicationIDAry)."') ORDER BY $sort ";
     	//debug_pr($sql);
    	$applicationAry = $this->returnArray($sql);

    	return array(count($applicationIDAry),$applicationAry);
	}

	public function assignApplicantToInterviewSetting($recordIds, $round=1, $interviewSettingID){
		if($recordIds=="") return false;

		//$applicationIdsSql = "'".implode("','",(array)$applicationIds)."'";
		$recordIds = is_array($recordIds)? "'".implode("','", $recordIds)."'" : "'".implode("','",explode(",",$recordIds))."'";
		if($round == 1){
			$round = '';
		}

		if($interviewSettingID=="") return false;

		$sql = "
			UPDATE 
				ADMISSION_OTHERS_INFO Set InterviewSettingID".$round." = '".$interviewSettingID."' 
			WHERE
				RecordID IN (".$recordIds.")
		";
		$success[] = $this->db_db_query($sql);

		if(in_array(false, $success))
			return false;
		else
			return true;
	}

	function getInterviewFormSetting($recordID='',  $startDate='', $endDate='', $keyword='', $schoolYearID='', $classLevelID=''){
    	global $admission_cfg, $Lang, $sys_custom;
		//$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;

		if($schoolYearID != ''){
			$cond .= ' AND i.SchoolYearID = \''.$schoolYearID.'\' ';
		}
		if($classLevelID != ''){
			$cond .= ' AND c.ClassLevelID = \''.$classLevelID.'\' ';
		}
		if($recordID != ''){
			$cond .= ' AND i.FormID = \''.$recordID.'\' ';
		}
		if($startDate != ''){
			$cond .= ' AND i.StartDate <= \''.$startDate.'\' ';
		}
		if($endDate != ''){
			$cond .= ' AND i.EndDate >= \''.$endDate.'\' ';
		}
		/*if($round != ''){
			$cond .= ' AND i.Round = \''.$round.'\' ';
		}*/
		if($keyword != ''){
			$search_cond = ' AND (i.Date LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\' 
							OR i.StartTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR i.EndTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\')';
		}
		$sql = 'SELECT i.FormID as RecordID, i.SchoolYearID as SchoolYearID, GROUP_CONCAT(distinct c.ClassLevelID SEPARATOR ",") as ClassLevelID, i.StartDate as StartDate, i.EndDate as EndDate, i.Title as Title, i.Remarks as Remarks, count(distinct r.ResultID) as ResultCount 
				FROM ADMISSION_INTERVIEW_FORM AS i
				JOIN ADMISSION_INTERVIEW_FORM_CLASS_LEVEL AS c
				ON i.FormID = c.FormID
				LEFT JOIN ADMISSION_INTERVIEW_RESULT r
				ON i.FormID = r.FormID
				WHERE i.Status = 1 '.$cond.' '.$search_cond.' 
				GROUP BY i.FormID 
				ORDER BY c.ClassLevelID, i.FormID';
		//debug_r($sql);
		$interviewAry = $this->returnArray($sql);

		return $interviewAry;
    }

    function getInterviewFormBuilterAry($recordID=''){
    	global $admission_cfg, $Lang, $sys_custom;

		//if($recordID != ''){
			$cond .= ' AND q.FormID = \''.$recordID.'\' ';
		//}

		$sql = "SELECT q.QuestionID, FormID, Type, Title, Remarks, Required, Marked, GROUP_CONCAT(o.Name SEPARATOR ',') as Name, GROUP_CONCAT(o.Value SEPARATOR ',') as Value FROM ADMISSION_INTERVIEW_QUESTION q 
				JOIN ADMISSION_INTERVIEW_QUESTION_OPTION o 
				ON q.QuestionID = o.QuestionID
				WHERE 1 ".$cond."
				GROUP BY q.QuestionID
				ORDER BY q.Sequence, o.Sequence";
		//debug_r($sql);
		$interviewAry = $this->returnArray($sql);

		return $interviewAry;
    }

    function updateInterviewFormBuilterEditByIds($data=array()){
    	global $admission_cfg, $Lang, $UserID;
    	extract($data);

    	$success = array();

		if($recordID == ''){
			$sql = "INSERT INTO ADMISSION_INTERVIEW_FORM 
						   (SchoolYearID, StartDate, EndDate, Title, Remarks, DateInput, InputBy, DateModified, ModifiedBy) 
					Values ('".$schoolYearID."','".$startDate."','".$endDate."','".$title."','',NOW(),'".$UserID."',NOW(),'".$UserID."')";
			$success[] = $this->db_db_query($sql);

			$recordID = mysql_insert_id();

			for($i=0; $i<sizeof($selectClassLevelID); $i++){
				$sql = "INSERT INTO ADMISSION_INTERVIEW_FORM_CLASS_LEVEL 
							   (FormID, ClassLevelID, DateInput, InputBy, DateModified, ModifiedBy) 
						Values ('".$recordID."','".$selectClassLevelID[$i]."',NOW(),'".$UserID."',NOW(),'".$UserID."')";
				$success[] = $this->db_db_query($sql);
			}

			for($i=0; $i<sizeof($questions_type); $i++){
				$sql = "INSERT INTO ADMISSION_INTERVIEW_QUESTION 
							   (FormID, Type, Title, Remarks, Required, Marked, Sequence, DateInput, InputBy, DateModified, ModifiedBy) 
						Values ('".$recordID."','".$questions_type[$i]."','".$text_title[$i]."',  '".$text_remarks[$i]."','".$text_required[$i]."','".$text_mark[$i]."', '".($i+1)."', NOW(),'".$UserID."',NOW(),'".$UserID."')";
				$success[] = $this->db_db_query($sql);

				$questionID = mysql_insert_id();

				$ans_arr = explode("\n",$text_input[$i]);

				for($j=0; $j<sizeof($ans_arr); $j++){
					$sql = "INSERT INTO ADMISSION_INTERVIEW_QUESTION_OPTION 
							   (QuestionID, Name, Value, IsAnswer, Sequence, DateInput, InputBy, DateModified, ModifiedBy) 
						Values ('".$questionID."','".trim($ans_arr[$j])."','".trim($ans_arr[$j])."', '0', '".($j+1)."', NOW(),'".$UserID."',NOW(),'".$UserID."')";
					$success[] = $this->db_db_query($sql);
				}
			}
		}
		else{

				// -- update interview form
				$sql = "UPDATE ADMISSION_INTERVIEW_FORM 
						SET
							SchoolYearID = '".$schoolYearID."',
							StartDate = '".$startDate."',
							EndDate = '".$endDate."',
							Title = '".$title."',
							Remarks = '',
							DateModified = NOW(),
							ModifiedBy = '".$UserID."'
						WHERE
							FormID = '".$recordID."' ";
				$success[] = $this->db_db_query($sql);

				// -- update interview form class
				$sql = "delete from ADMISSION_INTERVIEW_FORM_CLASS_LEVEL where FormID = '".$recordID."' ";
				$success[] = $this->db_db_query($sql);

				for($i=0; $i<sizeof($selectClassLevelID); $i++){
					$sql = "INSERT INTO ADMISSION_INTERVIEW_FORM_CLASS_LEVEL 
								   (FormID, ClassLevelID, DateInput, InputBy, DateModified, ModifiedBy) 
							Values ('".$recordID."','".$selectClassLevelID[$i]."',NOW(),'".$UserID."',NOW(),'".$UserID."')";
					$success[] = $this->db_db_query($sql);
				}
				if($needUpdateQuestion > 0){
				// -- update interview question
				$sql = "delete q, o from ADMISSION_INTERVIEW_QUESTION q 
						join ADMISSION_INTERVIEW_QUESTION_OPTION o on q.QuestionID = o.QuestionID 
						where FormID = '".$recordID."' ";
				$success[] = $this->db_db_query($sql);

				for($i=0; $i<sizeof($questions_type); $i++){
					$sql = "INSERT INTO ADMISSION_INTERVIEW_QUESTION 
								   (FormID, Type, Title, Remarks, Required, Marked, Sequence, DateInput, InputBy, DateModified, ModifiedBy) 
							Values ('".$recordID."','".$questions_type[$i]."','".$text_title[$i]."',  '".$text_remarks[$i]."','".$text_required[$i]."','".$text_mark[$i]."', '".($i+1)."', NOW(),'".$UserID."',NOW(),'".$UserID."')";
					$success[] = $this->db_db_query($sql);

					$questionID = mysql_insert_id();

					$ans_arr = explode("\n",$text_input[$i]);

					for($j=0; $j<sizeof($ans_arr); $j++){
						$sql = "INSERT INTO ADMISSION_INTERVIEW_QUESTION_OPTION 
								   (QuestionID, Name, Value, IsAnswer, Sequence, DateInput, InputBy, DateModified, ModifiedBy) 
							Values ('".$questionID."','".trim($ans_arr[$j])."','".trim($ans_arr[$j])."', '0', '".($j+1)."', NOW(),'".$UserID."',NOW(),'".$UserID."')";
						$success[] = $this->db_db_query($sql);
					}
				}
			}
		}
		return !in_array(false,$success);
    	/*recordID
		schoolYearID

		selectClassLevelID
		start_date
		end_date
		title

		text_title
		text_input
		text_remarks
		text_required
		text_mark
		questions_type*/
    }

    function removeInterviewFormBuilterEditByIds($recordIds){
    	if($recordIds=="") return false;

		$recordIds = is_array($recordIds)? "'".implode("','", $recordIds)."'" : "'".implode("','",explode(",",$recordIds))."'";

		$sql = "UPDATE ADMISSION_INTERVIEW_FORM Set Status = '0' where FormID IN (".$recordIds.")";
		$success[] = $this->db_db_query($sql);

		/*$sql = "delete from ADMISSION_INTERVIEW_FORM where FormID IN (".$recordIds.")";
		$success[] = $this->db_db_query($sql);


		$sql = "delete from ADMISSION_INTERVIEW_FORM_CLASS_LEVEL where FormID IN (".$recordIds.")";
		$success[] = $this->db_db_query($sql);

		$sql = "delete q, o from ADMISSION_INTERVIEW_QUESTION q
						join ADMISSION_INTERVIEW_QUESTION_OPTION o on q.QuestionID = o.QuestionID
						where FormID IN (".$recordIds.")";
				$success[] = $this->db_db_query($sql);

		$sql = "delete r, a from ADMISSION_INTERVIEW_RESULT r
						join ADMISSION_INTERVIEW_RESULT_ANSWER a on r.ResultID = a.ResultID
						where r.FormID IN (".$recordIds.")";
				$success[] = $this->db_db_query($sql);
		*/
		if(in_array(false, $success))
			return false;
		else
			return true;
    }

    function getInterviewFormQuestionAnswerAry($recordID='', $ApplicationID='', $InterviewSettingID='', $createBy=''){
    	global $admission_cfg, $Lang, $sys_custom;

		if($recordID != ''){
			$cond .= ' AND r.FormID = \''.$recordID.'\' ';
		}

		if($createBy != ''){
			$cond .= ' AND r.InputBy = \''.$createBy.'\' ';
		}

		if($ApplicationID != ''){
			$cond .= ' AND r.ApplicationID = \''.$ApplicationID.'\' ';
		}

		if($InterviewSettingID != ''){
			$cond .= ' AND r.InterviewSettingID = \''.$InterviewSettingID.'\' ';
		}

//		$name_field = getNameFieldByLang("r.");
//		$sql = "SELECT r.ApplicationID, r.InterviewSettingID, q.QuestionID, q.FormID, Type, q.Title, q.Remarks as QRemarks, Required, Marked, GROUP_CONCAT(distinct o.Name SEPARATOR ',') as Name, GROUP_CONCAT(distinct o.Value SEPARATOR ',') as Value, a.Answer, ".$name_field." as InputBy
//				FROM ADMISSION_INTERVIEW_QUESTION as q
//				JOIN ADMISSION_INTERVIEW_QUESTION_OPTION as o
//				ON q.QuestionID = o.QuestionID
//				JOIN ADMISSION_INTERVIEW_RESULT_ANSWER a
//				ON q.QuestionID = a.QuestionID
//				JOIN ADMISSION_INTERVIEW_RESULT as r
//				ON q.FormID = r.FormID
//				JOIN ADMISSION_INTERVIEW_FORM as f
//				ON q.FormID = f.FormID
//				where f.Status = 1 ".$cond."
//				GROUP BY q.QuestionID
//				ORDER BY q.Sequence, o.Sequence";

		$name_field = getNameFieldByLang("u.");
		$sql = "SELECT r.ApplicationID, r.InterviewSettingID, q.FormID, GROUP_CONCAT(q.Sequence SEPARATOR ',') as QuestionsSeq, GROUP_CONCAT(q.QuestionID SEPARATOR ',') as QuestionsID, GROUP_CONCAT(q.Title SEPARATOR '<!--SEPARATOR-->') as Questions, GROUP_CONCAT(a.Answer SEPARATOR '<!--SEPARATOR-->') as Answers, ".$name_field." as InputByName, r.InputBy	
				FROM ADMISSION_INTERVIEW_QUESTION as q 
				JOIN ADMISSION_INTERVIEW_RESULT_ANSWER as a 
				ON q.QuestionID = a.QuestionID 
				JOIN ADMISSION_INTERVIEW_RESULT as r 
				ON a.ResultID= r.ResultID 
				JOIN ADMISSION_INTERVIEW_FORM as f
				ON q.FormID = f.FormID 
				LEFT JOIN INTRANET_USER as u On u.UserID=r.InputBy 
				WHERE f.Status = 1 ".$cond." 
				GROUP BY r.ResultID 
				ORDER BY q.Sequence";
		//debug_pr($sql);
		$interviewAry = $this->returnArray($sql);

		$tempInterviewAry = array();

		foreach($interviewAry as $aInterview){
			$QuestionArr = explode('<!--SEPARATOR-->',$aInterview['Questions']);
			$QuestionsSeqArr = explode(',',$aInterview['QuestionsSeq']);
			$QuestionIDArr = explode(',',$aInterview['QuestionsID']);
			for($i=0; $i <sizeof($QuestionArr); $i++){
				$tempInterviewAry[$aInterview['ApplicationID']][$aInterview['InterviewSettingID']][$aInterview['InputBy']]['Questions'][$QuestionsSeqArr[$i]-1] = $QuestionArr[$i];
				$tempInterviewAry[$aInterview['ApplicationID']][$aInterview['InterviewSettingID']][$aInterview['InputBy']]['QuestionsID'][$QuestionsSeqArr[$i]-1] = $QuestionIDArr[$i];

			}
			$AnswerArr = explode('<!--SEPARATOR-->',$aInterview['Answers']);
			for($i=0; $i <sizeof($AnswerArr); $i++){
				$tempInterviewAry[$aInterview['ApplicationID']][$aInterview['InterviewSettingID']] [$aInterview['InputBy']]['Answers'][$QuestionsSeqArr[$i]-1] = $AnswerArr[$i];
			}
			$tempInterviewAry[$aInterview['ApplicationID']][$aInterview['InterviewSettingID']] [$aInterview['InputBy']]['InputByName'] = $aInterview['InputByName'];
		}

		$maxNumOfInputBy = 0;
		foreach($tempInterviewAry as $atempInterviewAry){
			foreach($atempInterviewAry as $_atempInterviewAry){
				if(sizeof($_atempInterviewAry) > $maxNumOfInputBy){
					$maxNumOfInputBy = sizeof($_atempInterviewAry);
				}
			}
		}
		$tempInterviewAry['maxNumOfInputBy'] = $maxNumOfInputBy;

		return $tempInterviewAry;
    }

    function getInterviewFormAnswerAry($recordID='', $ApplicationID='', $createBy=''){
    	global $admission_cfg, $Lang, $sys_custom;

		//if($recordID != ''){
			$cond .= ' AND r.FormID = \''.$recordID.'\' ';
		//}

		if($createBy != ''){
			$cond .= ' AND r.InputBy = \''.$createBy.'\' ';
		}

		if($ApplicationID != ''){
			$cond .= ' AND r.ApplicationID = \''.$ApplicationID.'\' ';
		}

		$sql = "select r.ResultID, QuestionID, Answer from ADMISSION_INTERVIEW_RESULT as r 
				join ADMISSION_INTERVIEW_RESULT_ANSWER a 
				on r.ResultID = a.ResultID
				where 1 ".$cond;

//		$sql = "SELECT q.QuestionID, q.FormID, Type, Title, Remarks, Required, Marked, GROUP_CONCAT(o.Name SEPARATOR ',') as Name, GROUP_CONCAT(o.Value SEPARATOR ',') as Value, a.Answer FROM ADMISSION_INTERVIEW_QUESTION q
//				JOIN ADMISSION_INTERVIEW_QUESTION_OPTION o
//				ON q.QuestionID = o.QuestionID
//				LEFT JOIN ADMISSION_INTERVIEW_RESULT_ANSWER a
//				ON q.QuestionID = a.QuestionID ".$cond2."
//				WHERE 1 ".$cond."
//				GROUP BY q.QuestionID
//				ORDER BY q.Sequence, o.Sequence";
		//debug_r($sql);
		$interviewAry = $this->returnArray($sql);

		$tempInterviewAry = array();

		foreach($interviewAry as $aInterview){
			$tempInterviewAry['ResultID'] = $aInterview['ResultID'];
			$tempInterviewAry[$aInterview['QuestionID']] = $aInterview['Answer'];
		}
		return $tempInterviewAry;
    }

    function getInterviewFormResultAry($recordID, $ApplicationID='', $createBy='', $interviewSettingID=''){
    	global $admission_cfg, $Lang, $sys_custom;

		//if($recordID != ''){
			$cond .= ' AND (r.FormID = \''.$recordID.'\' OR r.InterviewSettingID = \''.$interviewSettingID.'\')';
		//}

		if($createBy != ''){
			$cond .= ' AND r.InputBy = \''.$createBy.'\' ';
		}

		if($ApplicationID != ''){
			$cond .= ' AND r.ApplicationID = \''.$ApplicationID.'\' ';
		}
		$name_field = getNameFieldByLang("u.");
		$sql = "select Marks, r.FormID, ApplicationID, GROUP_CONCAT(r.InputBy SEPARATOR ',') as InputByID, GROUP_CONCAT(".$name_field." SEPARATOR ',') as InputBy, GROUP_CONCAT(r.DateModified SEPARATOR ',') as DateModified from ADMISSION_INTERVIEW_RESULT as r 
				JOIN ADMISSION_INTERVIEW_FORM as f ON f.FormID = r.FormID
				LEFT JOIN INTRANET_USER as u On u.UserID=r.InputBy 
				where f.Status = '1' ".$cond."
				group by ApplicationID
				order by r.InputBy";

//		$sql = "SELECT q.QuestionID, q.FormID, Type, Title, Remarks, Required, Marked, GROUP_CONCAT(o.Name SEPARATOR ',') as Name, GROUP_CONCAT(o.Value SEPARATOR ',') as Value, a.Answer FROM ADMISSION_INTERVIEW_QUESTION q
//				JOIN ADMISSION_INTERVIEW_QUESTION_OPTION o
//				ON q.QuestionID = o.QuestionID
//				LEFT JOIN ADMISSION_INTERVIEW_RESULT_ANSWER a
//				ON q.QuestionID = a.QuestionID ".$cond2."
//				WHERE 1 ".$cond."
//				GROUP BY q.QuestionID
//				ORDER BY q.Sequence, o.Sequence";
		//debug_r($sql);
		$interviewAry = $this->returnArray($sql);

		$tempInterviewAry = array();
		foreach($interviewAry as $aInterview){
			$tempInterviewAry[$aInterview['ApplicationID']]['id'] = $aInterview['InputByID'];
			$tempInterviewAry[$aInterview['ApplicationID']]['name'] = $aInterview['InputBy'];
			$tempInterviewAry[$aInterview['ApplicationID']]['datemodified'] = $aInterview['DateModified'];
			$tempInterviewAry[$aInterview['ApplicationID']]['formid'] = $aInterview['FormID'];
			$tempInterviewAry[$aInterview['ApplicationID']]['marks'] = $aInterview['Marks'];
		}
		return $tempInterviewAry;
    }

    function updateInterviewFormResultByIds($data=array()){
    	global $admission_cfg, $Lang, $UserID;
    	extract($data);

    	$success = array();

		if($answerID == ''){
			$sql = "SELECT ResultID 
				FROM ADMISSION_INTERVIEW_RESULT 
				WHERE InterviewSettingID = '".$interviewSettingID."' 
				AND FormID = '".$formID."' 
				AND ApplicationID = '".$applicationID."' 
				AND InputBy = '".$UserID."'";
			$result = current($this->returnArray($sql));
			$answerID = $result['ResultID'];
		}

		if($answerID == ''){
			$sql = "INSERT INTO ADMISSION_INTERVIEW_RESULT 
						   (InterviewSettingID, FormID, ApplicationID, Marks, Remarks, DateInput, InputBy, DateModified, ModifiedBy) 
					Values ('".$interviewSettingID."','".$formID."','".$applicationID."','','',NOW(),'".$UserID."',NOW(),'".$UserID."')";
			$success[] = $this->db_db_query($sql);
			//debug_pr($sql);
			$answerID = mysql_insert_id();

			for($i=0; $i<sizeof($questionID); $i++){
				$sql = "INSERT INTO ADMISSION_INTERVIEW_RESULT_ANSWER 
							   (ResultID, QuestionID, Answer, DateInput, InputBy, DateModified, ModifiedBy) 
						Values ('".$answerID."','".$questionID[$i]."','".$answer[$questionID[$i]]."',NOW(),'".$UserID."',NOW(),'".$UserID."')";
				$success[] = $this->db_db_query($sql);
			}
		}
		else{
			for($i=0; $i<sizeof($questionID); $i++){
				$sql = "UPDATE ADMISSION_INTERVIEW_RESULT_ANSWER 
						SET
							Answer = '".$answer[$questionID[$i]]."',
							DateModified = NOW(),
							ModifiedBy = '".$UserID."'
						WHERE
							ResultID = '".$answerID."' AND QuestionID ='".$questionID[$i]."'";
				$success[] = $this->db_db_query($sql);
			}
		}

		// -- update the mark
		$sql = "select SUM(Answer) as TotalMark
				from ADMISSION_INTERVIEW_RESULT as r 
				JOIN ADMISSION_INTERVIEW_RESULT_ANSWER as a 
				ON r.ResultID = a.ResultID 
				JOIN ADMISSION_INTERVIEW_QUESTION as q 
				ON q.QuestionID = a.QuestionID where Marked = 1 AND r.FormID = '".$formID."' AND r.InterviewSettingID = '".$interviewSettingID."' AND r.ApplicationID = '".$applicationID."' AND r.InputBy = '".$UserID."' 
				GROUP BY r.FormID";
		$totalMark = current($this->returnArray($sql));

		$sql = "UPDATE ADMISSION_INTERVIEW_RESULT Set Marks = '".$totalMark['TotalMark']."', DateModified = NOW() Where FormID= '".$formID."' AND InterviewSettingID = '".$interviewSettingID."' AND ApplicationID = '".$applicationID."' AND InputBy = '".$UserID."'";
		$success[] = $this->db_db_query($sql);

		return !in_array(false,$success);
    	/*'recordID' => $recordID,
		'questionID' => $question_id,
		'applicationID' => $applicationID,
		'answer' => $answer*/
    }

    function getInterviewFormReport($schoolYearID,$data=array()){
		global $admission_cfg;
		extract($data);
		$sort = $sortby? "$sortby $order, application_id ASC":"application_id";
		$limit = $page? " LIMIT ".(($page-1)*$amount).", $amount": "";
		$cond = !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($applicationID)?" AND o.RecordID IN ('".implode("','",(array)$applicationID)."')":"";
		$cond .= !empty($interviewDate)?" AND ais.Date='{$interviewDate}'":"";
		$cond .= $status?" AND s.Status='".$status."'":"";
		if($status==$admission_cfg['Status']['waitingforinterview']){
			if($interviewStatus==1){
				$cond .= " AND s.isNotified = 1";
			}else{
				$cond .= " AND (s.isNotified = 0 OR s.isNotified IS NULL)";
			}
		}

		switch ($round) {
		    case 1:
		        $select = " o.InterviewSettingID ";
		        break;
		    case 2:
		        $select = " o.InterviewSettingID2 ";
		        break;
		    case 3:
		        $select = " o.InterviewSettingID3 ";
		        break;
		    default:
		    	 $select = " o.InterviewSettingID ";
		}

		if(!empty($keyword)){
			$cond .= "
				AND ( 
					stu.EnglishName LIKE '%".$keyword."%'
					OR stu.ChineseName LIKE '%".$keyword."%'
					OR stu.ApplicationID LIKE '%".$keyword."%'					
					OR pg.EnglishName LIKE '%".$keyword."%'
					OR pg.ChineseName LIKE '%".$keyword."%'
					OR pg.Mobile LIKE '%".$keyword."%'
					OR stu.BirthCertNo LIKE '%".$keyword."%'				
				)
			";
		}

     	$from_table = "		    
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN 
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID	
			LEFT JOIN 
				ADMISSION_INTERVIEW_SETTING ais ON {$select} = ais.RecordID
			LEFT JOIN 
				ADMISSION_INTERVIEW_RESULT r ON o.InterviewSettingID = r.InterviewSettingID  AND o.RecordID = r.ApplicationID
			LEFT JOIN 
				ADMISSION_INTERVIEW_FORM f ON f.FormID = r.FormID
			LEFT JOIN 
				ADMISSION_INTERVIEW_SETTING s2 ON s2.RecordID = r.InterviewSettingID AND s2.Round = '".$round."'
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			
    	";
    	$sql = "SELECT DISTINCT o.ApplicationID ".$from_table;
    	$applicationIDAry = $this->returnVector($sql);
//    	$sql2 = "SELECT DISTINCT o.ApplicationID ".$from_table;
//    	$applicationIDCount = $this->returnVector($sql2);
    	$sql = "
			SELECT
				o.RecordID AS record_id,
     			stu.ApplicationID AS application_id,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status,	".$select." as InterviewSettingID, r.Marks as marks, ais.Date as InterviewDate ".$from_table." AND o.ApplicationID IN ('".implode("','",$applicationIDAry)."') ORDER BY $sort";

    	$applicationAry = $this->returnArray($sql);

    	// assign teacher result to array [start]
    	//$name_field = getNameFieldByLang("");

    	for($i=0; $i < sizeof($applicationAry); $i++){
    		$sql = "SELECT AVG(Marks) as Marks, f.FormID, a.QuestionID, a.Answer
					FROM ADMISSION_INTERVIEW_RESULT r
					JOIN ADMISSION_INTERVIEW_FORM f
					ON f.FormID = r.FormID
					JOIN ADMISSION_INTERVIEW_SETTING s
					ON s.RecordID = r.InterviewSettingID
					LEFT JOIN ADMISSION_INTERVIEW_RESULT_ANSWER a
					ON r.ResultID = a.ResultID
					WHERE Status = '1' AND r.ApplicationID = '".$applicationAry[$i]['record_id']."' AND s.Round = '".$round."' 
					GROUP BY r.FormID
					";
			//debug_pr($sql);
			$result = $this->returnArray($sql);

			$applicationAry[$i]['Marks'] = round($result[0]['Marks'], 2);
			$applicationAry[$i]['FormID'] = $result[0]['FormID'];

//			for($j=0; $j < sizeof($result); $j++){
//				if($formQuestionID!='' && $formAnswer!=''){
//					if($result[0]['QuestionID'] == $formQuestionID && $result[0]['Answer'] == $formAnswer){
//						$tempApplicationAry[] = $applicationAry[$i];
//					}
//				}
//				else if($formQuestionID!='' && $result[0]['QuestionID'] == $formQuestionID){
//	    			$tempApplicationAry[] = $applicationAry[$i];
//		    	}
//			}
    	}
    	// assign teacher result to array [end]
    	//$applicationAry = ($tempApplicationAry?$tempApplicationAry:$applicationAry);
//    	if($formQuestionID!='' && $formAnswer!=''){
//    		$applicationAry = $tempApplicationAry;
//    	}
    	return array(count($applicationIDAry),$applicationAry);
	}

	function resendNotificationEmailByIds($recordIds, $lauc, $applicationIDs=''){
		global $sys_custom, $PATH_WRT_ROOT, $setting_path_ip_rel;

		if($recordIds=="" && $applicationIDs=='') return false;

		$recordIds = is_array($recordIds)? "'".implode("','", $recordIds)."'" : "'".implode("','",explode(",",$recordIds))."'";
		$applicationIDs = is_array($applicationIDs)? "'".implode("','", $applicationIDs)."'" : "'".implode("','",explode(",",$applicationIDs))."'";

		$sql = "
			SELECT 
				ApplicationID applicationID,
				ApplyYear schoolYearId,
				ApplyLevel classLevelID
			FROM
				ADMISSION_OTHERS_INFO
			WHERE
				RecordID IN (".$recordIds.")
			OR
				ApplicationID IN (".$applicationIDs.")
    	";
    	$applicantInfoArr = $this->returnArray($sql);
		//debug_pr($sql);

		foreach($applicantInfoArr as $aApplicantInfo){
			$applicationSetting = $this->getApplicationSetting($aApplicantInfo['schoolYearId']);
			$lastContent = (trim($applicationSetting[$aApplicantInfo['classLevelID']]['EmailContent']) && trim($applicationSetting[$aApplicantInfo['classLevelID']]['EmailContent']) != '<br />')?$applicationSetting[$aApplicantInfo['classLevelID']]['EmailContent']:$applicationSetting[$aApplicantInfo['classLevelID']]['LastPageContent'];
			if($sys_custom['KIS_Admission']['MGF']['Settings'])
				$mail_content = $lauc->getFinishPageEmailContent($aApplicantInfo['applicationID'], $lastContent, $aApplicantInfo['schoolYearId'], $aApplicantInfo['classLevelID']);
			else
				$mail_content = $lauc->getFinishPageEmailContent($aApplicantInfo['applicationID'], $lastContent, $aApplicantInfo['schoolYearId']);

			$success[] = $this->sendMailToNewApplicant($aApplicantInfo['applicationID'],'',$mail_content, true);
		}
		if(in_array(false, $success))
			return false;
		else
			return true;
	}

	function getInterviewUserSettings($userId=""){
		if($userId){
			$userId = (is_array($userId)?implode("','",$userId):$userId);
			$cond = " AND s.UserID IN ('".$userId."')";
		}

		$name_field = getNameFieldByLang("u.");
		$sql = "
			SELECT 
				s.UserID, ".$name_field." as UserName 
			FROM 
				ADMISSION_INTERVIEW_USER_SETTING as s 
			LEFT JOIN 
				INTRANET_USER as u On u.UserID=s.UserID 
			WHERE 1  
				$cond";
		return $this->returnArray($sql);
	}

	function updateInterviewUserSettings($userId){
		global $UserID;
		$result = array();
		if($userId){
			$userId = (is_array($userId)?$userId:explode(',',$userId));
			for($i=0; $i<count($userId); $i++){
				if(count($this->getInterviewUserSettings($userId[$i])) <= 0){
					$sql = "
						INSERT INTO 
							ADMISSION_INTERVIEW_USER_SETTING 
							(UserID,  DateInput, InputBy, DateModified, ModifiedBy) 
						Values
							('".$userId[$i]."', now(),'".$UserID."', now(),'".$UserID."')
					";
					$result[] = $this->db_db_query($sql);
				}
			}
		}
		return !in_array(false,$result);
	}

	function deleteInterviewUserSettings($userId){
		if($userId){
			$userId = is_array($userId)? "'".implode("','", $userId)."'" : "'".implode("','",explode(",",$userId))."'";

			$cond = " AND UserID IN (".$userId.")";
			$sql = "DELETE FROM ADMISSION_INTERVIEW_USER_SETTING WHERE 1 $cond";
			return $this->db_db_query($sql);
		}
		return true;
	}

	function getPrintLink($recordID, $applicationID, $templateID = '', $sequence=''){
		global $admission_cfg;
		if($sequence){
			$sql = "SELECT TemplateID FROM ADMISSION_EMAIL_TEMPLATE where Sequence = '".$sequence."'";
			$result = $this->returnVector($sql);
			$templateID = $result[0];
		}
		$id = urlencode(getEncryptedText('ApplicationID='.$applicationID.'&RecordID='.$recordID.'&TemplateID='.$templateID, $admission_cfg['FilePathKey']));
		return ($_SERVER['SERVER_PORT'] == 443?'https://':'http://').$_SERVER['HTTP_HOST'].'/kis/admission_form/print_email.php?id='.$id;
	}
}
?>
