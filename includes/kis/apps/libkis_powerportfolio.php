<?
# using:

####################################
#
#	Date:	2020-08-27	Philips
#			create this file
#
####################################

class kis_powerportfolio extends libdb implements kis_apps {
        
    private $user_id;
    
    public static function getAvailability($user_id, $user_type, $student_id){

    	global $plugin, $PATH_WRT_ROOT;
    	include_once($PATH_WRT_ROOT.'includes/PowerPortfolio/libpowerportfolio.php');
    	$libPP = new libpowerportfolio();
    	//$isAdminGroupMember = $libPP->IS_POWER_PORTFOLIO_ADMIN_GROUP_MEMBER();
    	$isAdmin = $libPP->IS_POWER_PORTFOLIO_ADMIN_USER();
    	$isClassTeacher = $libPP->IS_KG_CLASS_TEACHER();
    	//$isSubjectTeacher = $libPP->IS_KG_SUBJECT_TEACHER();

        if(!$plugin['PowerPortfolio']) {
            return array();
        }
        else if($user_type != kis::$user_types['teacher']) {
            return array();
        }
        else {
            if ($isAdmin){
                $module_path = "/home/eAdmin/StudentMgmt/PowerPortfolio/";
                return array('power_portfolio', 'btn_power_portfolio', '_blank', $module_path.'?task=settings.rubrics.list');
            } else if($isClassTeacher) {
            	$module_path = "/home/eAdmin/StudentMgmt/PowerPortfolio/";
            	return array('power_portfolio', 'btn_power_portfolio', '_blank', $module_path.'?task=mgmt.input_score.index_class');
            } else {
            	return array();
            }
        }
    }

    public static function getAdminStatus($user_id, $user_type, $student_id){
	    return array();
    }

    public static function getNotificationCount($user_id, $user_type, $student_id){
	    return 0;
    }

    public function __construct($user_id, $user_type, $student_id, $params){
	
    }
}
?>