<?
include_once("$intranet_root/includes/libelibrary_plus.php");
include_once("$intranet_root/home/library_sys/management/circulation/TimeManager.php");

class kis_borrowrecord extends libdb implements kis_apps {
        
    private $user_id;
    
    public static function getAvailability($user_id, $user_type, $student_id){
    
	global $plugin, $sys_custom;
    
	$permission = elibrary_plus::getUserPermission($user_id);
	
	if ($user_type == kis::$user_types['parent'] && $sys_custom['eLibrary_plus_kis_parent']){
		return array('borrowrecord', 'btn_borrow', 'wood', '');
	}
	/*if ($permission['admin'] || $permission['circulation']){
	    return array('borrowrecord', 'btn_borrow', 'wood', '');
	   	    
	}else if ($plugin['library_management_system'] && $permission['elibplus_opened']){
	    return array('elibrary', 'btn_elib', '', '/home/eLearning/elibplus/');
	}*/
	
	return array();
	
    }
            
    public static function getAdminStatus($user_id, $user_type, $student_id){
    
	
	return array();
    }

    public static function getNotificationCount($user_id, $user_type, $student_id){
    
	return 0;
	
    }
    public function __construct($user_id, $user_type, $student_id, $params){
		global $intranet_db;
		$this->db = $intranet_db;
		$this->user_id = $user_id;
		$this->student_id = $student_id;
		$this->students = array($this->student_id);
    }
    public function getBorrowRecords($params, $sortby, $order, $amount, $page){
	
	global $eclass_prefix;
	extract($params);
	
	$sort = $sortby? "$sortby $order,":"";
	$limit = $amount? " LIMIT ".(($page-1)*$amount).", $amount": "";
	$cond = $status? "AND a.RecordStatus = '".$status."'": "";
	if($status == 'RETURNED'){
		$cond = "AND (a.RecordStatus = 'RETURNED' OR a.RecordStatus ='RENEWED')";
	}
	
//	$sql = "SELECT SQL_CALC_FOUND_ROWS
//		    a.PaymentID as id,
//		    b.Name as item_name,
//		    c.Name as category_name,
//		    a.Amount as amount,
//		    a.SubsidyAmount as subsidy,
//		    d.UnitName as unit_name,
//		    b.EndDate as deadline,
//		    a.RecordStatus as status,
//		    a.PaidTime as time
//		FROM PAYMENT_PAYMENT_ITEMSTUDENT as a
//		LEFT JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
//		LEFT JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID
//		LEFT JOIN PAYMENT_SUBSIDY_UNIT as d  ON a.SubsidyUnitID = d.UnitID
//		WHERE
//                    a.StudentID = ".$this->user_id."
//		    AND (b.Name LIKE '%$keyword%' OR c.Name LIKE '%$keyword%') 
//		    $cond
//                ORDER BY $sort
//		    deadline desc,
//		    time desc,
//		    item_name asc,
//		    category_name asc,
//		    unit_name asc,
//		    amount asc,
//		    subsidy asc
//		$limit";
//	
//	$sql = " SELECT lrl.*
//					FROM 
//						LIBMS_BORROW_LOG lrl
//						LEFT JOIN LIBMS_USER lu ON lu.UserID = lrl.UserID
//					WHERE 1 AND lrl.RecordStatus = 'BORROWED'
//						 AND lrl.UserID = '".$this->user_id."' 
//				";
	$libmsDB = $eclass_prefix."eClass_LIBMS";
	$libms_tm = new TimeManager(new liblms());
    $sql = "SELECT SQL_CALC_FOUND_ROWS
			CONCAT(b.BookTitle,' [',c.BarCode,'] ') as title,
            a.BorrowLogID as borrow_id,
            a.ReturnedTime as return_date,
            a.BorrowTime as borrow_date,
            a.DueDate as due_date,
            if (CURDATE() > a.DueDate, 1, 0) as is_overdue,
			if (a.RecordStatus = 'BORROWED',if (CURDATE() > a.DueDate, 0, 1) , 2) as borrow_status,
			UNIX_TIMESTAMP(CURDATE()) as current_date_ts,
			UNIX_TIMESTAMP(a.DueDate) as due_date_ts,
            a.RenewalTime as renew_count,
            a.UniqueID
            from ".$libmsDB.".LIBMS_BORROW_LOG a
            JOIN ".$libmsDB.".LIBMS_BOOK b ON a.BookID=b.BookID
            JOIN ".$libmsDB.".LIBMS_BOOK_UNIQUE c ON a.UniqueID=c.UniqueID		
            where (a.UserID='".$this->student_id."' or (a.UserID=3066 and 0)) $cond
            ORDER BY $sort borrow_status asc, return_date desc, title asc, due_date desc, borrow_date desc $limit";
			      
	$records = $this->returnArray($sql);
	$total   = current($this->returnVector('SELECT FOUND_ROWS();'));
	
	foreach ($records as $i=>$book){
	    if ($book['is_overdue']){
		$records[$i]['overdue_days'] = $libms_tm->dayForPenalty($book['due_date_ts'], $book['current_date_ts']);
	    }
	}
	
	return array($total, $records);
    }
    
}
?>