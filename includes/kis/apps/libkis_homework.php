<?

class kis_homework extends libdb implements kis_apps {
    private $user_id;
    
    public static function getAvailability($user_id, $user_type, $student_id)
    {
    	global $sys_custom;
    	
		if(!$sys_custom['KIS_SchoolSettings']['EnableSubjectSetting'] || !$sys_custom['KIS_eHomework']['EnableModule'] || ($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])) {
			return array();
		}
		
		# eService > eHomework
		if($user_type==kis::$user_types["student"] || ($user_type==kis::$user_types["parent"] && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])) {
		    return array("homework", "btn_homework", "", "/home/eService/homework/index.php");
		}
		# eAdmin > eHomework
		else {
			global $intranet_root;
			include_once($intranet_root."/includes/libhomework.php");
			include_once($intranet_root."/includes/libhomework2007a.php");
			$lhomework = new libhomework2007();
			
			if (
					$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || 
					$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ||
					$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] || 
					$_SESSION["isTeaching"] ||
					($user_type==kis::$user_types["staff"] && !$_SESSION["isTeaching"] && $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"]) || 
					$lhomework->isViewerGroupMember($user_id)
				)
			{
				return array("homework", "btn_homework", "", "/home/eAdmin/StudentMgmt/homework/index.php");
			}
		}

		return array();
    }
            
    public static function getAdminStatus($user_id, $user_type, $student_id)
    {
    	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"]) {
    		return array("/home/eAdmin/StudentMgmt/homework/index.php");
    	}
		return array();
    }

    public static function getNotificationCount($user_id, $user_type, $student_id)
    {
		return 0;
    }
    
    public function __construct($user_id, $user_type, $student_id, $params)
    {
    	// do nothing
    }
}
?>