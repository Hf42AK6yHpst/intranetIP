<?php 
/* using: 
 */

include_once($intranet_root."/includes/libcircular.php");

class kis_ecircular extends libdb implements kis_apps{
    private $user_id;
    
    public function __construct($user_id, $user_type, $student_id, $params){
        global $intranet_db;
        
        $this->db = $intranet_db;
        $this->user_id = $user_id;
    }
    
    public static function getAvailability($user_id, $user_type, $student_id){
        global $plugin;
        
        $permission = elibrary_plus::getUserPermission($user_id);
        if($user_type == kis::$user_types['teacher']){
            return array('ecircular','btn_ecircular','', '/home/eService/circular/');
        }
        return array();
    }
    
    public static function getAdminStatus($user_id, $user_type, $student_id){
        
        
        return array();
    }
    
    public static function getNotificationCount($user_id, $user_type, $student_id){
        $libkis_ecircular = new self($user_id, $user_type, $student_id, array());
        
        $temp = $libkis_ecircular->getNumOfCircular($user_id);
        
        return $temp; 
        
    }
    
    function getNumOfCircular ($user_id, $Ended = 0){
        global $sys_custom;
        
        global $PATH_WRT_ROOT;
        include_once($PATH_WRT_ROOT."includes/libcircular.php");
        $lcircular = new libcircular();
        
        if ($lcircular->isLateSignAllow && !$sys_custom['eCircular']['IconUnsignedCountApplyOldLogic']){
            
        }
        else{
            $conds = " AND b.DateEnd >= CURDATE()";
        }
        
        if($Ended){
            $conds = " AND b.DateEnd >= CURDATE()";
        }
        
        $sql = 'SELECT
         			COUNT(a.CircularReplyID)
				FROM
					INTRANET_CIRCULAR_REPLY as a
					LEFT OUTER JOIN INTRANET_CIRCULAR as b ON a.CircularID = b.CircularID
				WHERE
					a.UserID = '. $user_id .' AND a.RecordStatus != 2 AND
					b.DateStart <= CURDATE() and b.RecordStatus=1 '. $conds .'
                ';
        
        $temp = $this->returnVector($sql);
        return $temp[0];
    }

}





?>