<?php

include_once($intranet_root."/includes/DocRouting/libDocRouting.php");
class kis_digitalrouting extends libdb implements kis_apps 
{
	private $user_id, $user_type, $student_id;
	
	public static function getAvailability($user_id, $user_type, $student_id)
	{
		global $plugin;
		
		if($plugin['DocRouting'] && ($_SESSION['SSV_USER_ACCESS']['eAdmin-DocRouting'] || $user_type == kis::$user_types['teacher'])){
			return array('digitalrouting','btn_digitalrouting','', '/home/eAdmin/ResourcesMgmt/DocRouting/');
		}
		return array();
	}
    
    public static function getAdminStatus($user_id, $user_type, $student_id)
    {
    	if($_SESSION['SSV_USER_ACCESS']['eAdmin-DocRouting']){
    		return array('/home/eAdmin/ResourcesMgmt/DocRouting/');
    	}
    	return array();
    }
    
    public static function getNotificationCount($user_id, $user_type, $student_id)
    {
    	global $intranet_root, $docRoutingConfig;
    	include_once($intranet_root."/includes/DocRouting/docRoutingConfig.inc.php");
    	include_once($intranet_root."/includes/libportal.php");
    	$lportal = new libportal();
    	return $lportal->getDrUserCurrentInvolvedEntryCount();
    }
    
    public function __construct($user_id, $user_type, $student_id, $params)
    {
    	global $intranet_db;

		$this->db = $intranet_db;
		$this->user_id = $user_id;
		$this->user_type = $user_type;
		$this->student_id = $user_type == kis::$user_types['teacher']? $user_id: $student_id;
    }
}
?>