<?php
// editing by

/*
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!                                                                                    !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!   Modification of this file may affect different modules.                          !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!   Please check carefully and inform elearning team leader after modifying it.      !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!                                                                                    !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

/********************
 * change log:
 * 20170317 Roy:
 * 		- add function getSchoolCustFlag()
 *      - move the flags in getUserExtraFlag() to getSchoolCustFlag()
 * 20170314 Roy:
 * 		- add function getUserExtraFlag(), return flags for power lesson 2 app login
 ********************/
include_once($intranet_root.'/includes/elearning/elearningConfig.inc.php');

if (!defined("LIBELEARNING_DEFINED")) {
	define("LIBELEARNING_DEFINED", true);
	
	class libelearning extends libdb {
		var $settingsObj;
		
		function libelearning() {
			$this->libdb();
			$this->settingsObj = null;
		}
		
		function getUserObj($parUserId, $parUserLogin='') {
			$funcParamAry = get_defined_vars();
			$cacheResultAry = $this->getCacheResult('getUserObj', $funcParamAry);
	    	if ($cacheResultAry !== null) {
	    		return $cacheResultAry;
	    	}
	    	
			global $intranet_root;
			
			include_once($intranet_root.'/includes/libuser.php');
			$userObj = new libuser($parUserId, $parUserLogin);
			
			$this->setCacheResult('getUserObj', $funcParamAry, $userObj);
        	return $userObj;
		}
		
		function logRequest($requestJsonEncryptedString, $requestJsonDecryptedString, $responseJsonEncryptedString, $responseJsonDecryptedString) {
			$sql = "Insert Into APP_REQUEST_LOG 
						(RequestTextEncrypted, RequestText, ResponseTextEncrypted, ResponseText, DateInput) 
					Values 
						('".$this->Get_Safe_Sql_Query($requestJsonEncryptedString)."', '".$this->Get_Safe_Sql_Query($requestJsonDecryptedString)."', '".$this->Get_Safe_Sql_Query($responseJsonEncryptedString)."', '".$this->Get_Safe_Sql_Query($responseJsonDecryptedString)."', now()) ";
			return $this->db_db_query($sql);
		}
		
		function getAppSettingsObj() {
			global $intranet_root;
			
			include_once($intranet_root.'/includes/eClassApp/libeClassApp_settings.php');
			if ($this->settingsObj == null) {
				$this->settingsObj = new libeClassApp_settings();
				$this->settingsObj->loadSettings();
			}
			
			return $this->settingsObj;
		}
				
		function isSchoolInLicense($appType) {
			$funcParamAry = get_defined_vars();
			$cacheResultAry = $this->getCacheResult('isSchoolInLicense', $funcParamAry);
	    	if ($cacheResultAry !== null) {
	    		return $cacheResultAry;
	    	}
	    	
			global $intranet_root, $config_school_code, $elearningConfig;
			
			$settingsName = 'appInLicense_'.$appType;
			$settingsObj = $this->getAppSettingsObj();
			$licenseLastModified = $settingsObj->getSettingsLastModifiedDate($settingsName);
			
			$nowTs = time();
			$licenseTs = strtotime($licenseLastModified);
			$timeDiff = $nowTs - $licenseTs;
			if ($licenseLastModified=='' || $timeDiff > $elearningConfig['refreshLicenseInterval']) {
				include_once($intranet_root."/includes/json.php");
				$jsonObj = new JSON_obj();
				
				$postParamAry = array();
				$postParamAry['RequestMethod'] = 'isSchoolInLicense';
				$postParamAry['SchoolCode'] = $config_school_code;
				$postParamAry['ModuleName'] = $elearningConfig['appLicenseModuleName'][$appType];
				$postJsonString = $jsonObj->encode($postParamAry);
				
				$headers = array('Content-Type: application/json');
				$centralServerUrl = $this->getCurCentralServerUrl();
				
				session_write_close();
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
				curl_setopt($ch, CURLOPT_URL, $centralServerUrl);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
				$responseJson = curl_exec($ch);
				curl_close($ch);
				
				$responseJson = standardizeFormPostValue($responseJson);
				$responseJsonAry = $jsonObj->decode($responseJson);
				$isSchoolInAppLicense = $responseJsonAry['MethodResult']['InLicense'];
				
				$updateDb = false;
				$dbValue = '';
				if ($isSchoolInAppLicense == 'Y') {
					$updateDb = true;
					$dbValue = 1;
				}
				else if ($isSchoolInAppLicense == 'N') {
					$updateDb = true;
					$dbValue = 0;
				}
				
				if ($updateDb) {
					$tmpSettingsAssoAry = array();
					$tmpSettingsAssoAry[$settingsName] = $dbValue;
					$settingsObj->saveSettings($tmpSettingsAssoAry);
					$settingsObj->loadSettings($forceReload=true);
				}
			}
			
			$result = ($settingsObj->getSettingsValue($settingsName) == 1)? true : false;
			$this->setCacheResult('isSchoolInLicense', $funcParamAry, $result);
			return $result;
		}
		
		function getCurCentralServerUrl($parFunction='') {
			global $elearningConfig;
			
			return $elearningConfig['pushMessage']['apiPath'];
		}
		
		function getUserExtraFlag($parUserLogin='', $parUserId='') {
			$flagAry = array();
			
			return $flagAry;
		}

        function getSchoolCustFlag() {
            global $eclass40_httppath, $web_protocol, $session_expiry_time;

            $flagAry = array();
            $flagCount = 0;

            ### session expiry time
            $flagAry[$flagCount]['key'] = 'sessionExpiryTime';
            $flagAry[$flagCount]['value'] = (string) $session_expiry_time;
            $flagCount++;

            ### eclass40 path
            if (strpos($eclass40_httppath, '://') !== false) {
                $hasPrefix = true;
            }
            if (!$hasPrefix) {
                if ($web_protocol == 'https') {
                    $prefix = 'https://';
                } else {
                    $prefix = 'http://' ;
                }
            } else {
                $prefix = '';
            }

            $fullPath = $prefix . $eclass40_httppath;

            $flagAry[$flagCount]['key'] = 'eclassPath';
            $flagAry[$flagCount]['value'] = $fullPath;
            $flagCount++;

            return $flagAry;
        }
	}
}
?>