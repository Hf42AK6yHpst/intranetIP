<?
# modifying : henry chow

##### Change Log [Start] #####
#
#	Date	:	2012-03-28 (Henry Chow)
#				modified Load_Year_Budget_Report() & Load_Group_Budget_Report()
#				display "Total" column at the beginning of "Budget Report"
#
###### Change Log [End] ######

include_once("libinvoice.php");		
include_once("libinterface.php");		

class libinvoice_ui extends interface_html {
	
	function libinvoice_ui() 
	{
		// nothing
	}
	/*
	function Get_Category_Table() 
	{
		global $Lang, $button_new, $button_edit, $button_delete;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		

		$BtnArr = array();
		$BtnArr[] = array("edit", "#", $button_edit);
		$BtnArr[] = array("delete", "#", $button_delete);
		
		$li = new libdbtable2007($field, $order, $pageNo);

		$x = '
			<div class="Conntent_tool">
				<a href="new.php" class="new"> '.$button_new.'</a>
			</div>
			<br style="clear:both">
			'.$this->Get_DBTable_Action_Button_IP25($BtnArr).'

			<table class="common_table_list">
			<thead>
				<tr>
					<th class="num_check">#</th>
					<th><a href="#" class="sort_asc">Group Name</a></th>
					<th><a href="#" class="sort_dec">Description</a></th>
					<th><a href="#">No. of Members</a></th>
					<th class="num_check"><input type="checkbox" name="checkbox" id="checkbox" /></th>
				</tr>
			</thead>
			</table>

		';
		return $x;
	}
	*/
	
	function Display_Invoice_Item_Table($RecordID)
	{
		global $Lang, $intranet_session_language, $UserID, $button_edit, $button_delete;
		global $_SESSION, $i_no_record_exists_msg, $i_general_no, $i_general_yes;
		
		$linvoice = new libinvoice();
		//$thisUserID = $linvoice->IS_ADMIN_USER($UserID) ? "" : $UserID;
		$group_arr = $linvoice->returnAdminGroup();
		$arr_category = $linvoice->getCategoryName();
		
		$groupAry = array();
		for($i=0; $i<count($group_arr); $i++) {
			$groupAry[$group_arr[$i][0]] = $group_arr[$i][1];	
		}
		
		$catAry = array();
		for($i=0; $i<count($arr_category); $i++) {
			$catAry[$arr_category[$i][0]] = $arr_category[$i][1];	
		}
		
		# item list
		$data = $linvoice->Display_Invoice_Item_Table($RecordID);
		$NoOfItem = count($data);
		
		# table action button
		$BtnArr = array();
		$BtnArr[] = array("edit", "javascript:checkEdit(document.form1,'ItemID[]','edit_item.php')", $button_edit);
		$BtnArr[] = array("delete", "javascript:checkRemove(document.form1,'ItemID[]','remove_item_update.php')", $button_delete);
		
		$checkAll = "(this.checked)?setChecked(1,this.form,'ItemID[]'):setChecked(0,this.form,'ItemID[]')";
		
		$admin_group_leader_arr = $linvoice->returnAdminGroup($UserID, $leader=1); // is leader of admin group
		$group_leader = array();
		for($a=0; $a<count($admin_group_leader_arr); $a++) 
			$group_leader[] = $admin_group_leader_arr[$a][0];
		
		if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] || count($admin_group_leader_arr) > 0) {
			$table = $this->Get_DBTable_Action_Button_IP25($BtnArr);
		}
		
		$table .= '
			<br style="clear:both">
			<table class="common_table_list">
			<thead>
				<tr>
					<th class="num_check" width="1">#</th>
					<th width="20%">'.$Lang['Invoice']['ItemName'].'</th>
					<th width="10%">'.$Lang['Invoice']['SubTotalAmount'].'</th>
					<th width="10%">'.$Lang['Invoice']['Quantity'].'</th>
					<!--<th width="10%">'.$Lang['Invoice']['TotalPrice'].'</th>-->
					<th width="25%">'.$Lang['Invoice']['ResourceMgmtGroup'].'</th>
					<th width="25%">'.$Lang['Invoice']['Category'].'</th>
					<th width="10%">'.$Lang['Invoice']['IsAsset'].'</th>
					<th class="num_check" width="1">'.$this->Get_Checkbox("ItemID[]", "ItemID[]", '','','','',$checkAll).'</th>
				</tr>
			</thead>';
		
		for($i=0; $i<$NoOfItem; $i++) {
			$name = ($intranet_session_language=="en") ? $data[$i]['NameEng'] : $data[$i]['NameChi'];
			//$totalPrice = $data[$i]['Price'] * $data[$i]['Quantity'];
			$isAsset = $data[$i]['IsAssetItem'] ? $i_general_yes : $i_general_no;
			$checkbox = ($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] || in_array($data[$i]['ResourceMgmtGroup'], $group_leader)) ? $this->Get_Checkbox("ItemID[]", "ItemID[]", $data[$i]['ItemID']) : "-";
			
			$table .= '
				<tr>
					<td>'.($i+1).'</td>
					<td>'.$name.'</td>
					<td>$'.$data[$i]['Price'].'</td>
					<td>'.$data[$i]['Quantity'].'</td>
					<td>'.$groupAry[$data[$i]['ResourceMgmtGroup']].'</td>
					<td>'.$catAry[$data[$i]['CategoryID']].'</td>
					<td>'.$isAsset.'</td>
					<td>'.$checkbox.'</td>
				</tr>	
			';	
		}	
		if(count($data)==0) {
			$table .= '<tr><td colspan="100%" align="center" height="40">'.$i_no_record_exists_msg.'</td></tr>';	
		}
			
		$table .= '	
			</table>
		';
		
		return $table;
	} 
	
	function Load_Year_Budget_Report($dataAry=array(), $AcademicYearID, $Groups, $flag=1)
	{
		# $flag : 1=view, 2=export
		
		global $Lang, $allCategory, $adminGroup, $linterface, $LAYOUT_SKIN, $image_path;
		
		include_once("form_class_manage.php");
		$fcm = new academic_year($AcademicYearID);
		
		$adminGroupAry = array();
		for($i=0; $i<count($adminGroup); $i++) {
			list($_id, $_name) = $adminGroup[$i];
			$adminGroupAry[$_id] = $_name;
		}
		
		$NoOfCategory = count($allCategory);
		$NoOfGroup = count($Groups);
		
		
		$table .= $linterface->GET_NAVIGATION2(Get_Lang_Selection($fcm->YearNameB5, $fcm->YearNameEN));
		$table .= '<br style="clear:both">';
		$exportContent .= Get_Lang_Selection($fcm->YearNameB5, $fcm->YearNameEN)."\n\n";				
		
		$table .= '<table class="common_table_list_v30 view_table_list_v30">';
		
		# table header
		$table .= '
				<thead>
					<tr>
						<th rowspan="2">&nbsp;</th>';
		$exportContent .= "\t";				
						
		$table .= '<th colspan="2">'.$Lang['General']['Total'].'</th>';
		$exportContent .= $Lang['General']['Total']."\t\t";
		
		for($i=0; $i<$NoOfCategory; $i++) {
			$table .= '<th colspan="2">'.Get_Lang_Selection($allCategory[$i][1], $allCategory[$i][2]).'</th>';
			$exportContent .= Get_Lang_Selection($allCategory[$i][1], $allCategory[$i][2])."\t\t";
		}
		$table .= '</tr><tr>';
		$exportContent .= "\n";
		
		$exportContent .= "\t";				
		for($i=0; $i<$NoOfCategory+1; $i++) { 
			$table .= '<th>'.$Lang['Invoice']['Budget'].'</th>';
			$table .= '<th>'.$Lang['Invoice']['AccumulatedExpense'].'</th>';
			$exportContent .= $Lang['Invoice']['Budget']."\t";
			$exportContent .= $Lang['Invoice']['AccumulatedExpense']."\t";
		}
		$table .= '</tr>';
		$table .= '</thead>';
		$exportContent .= "\n";
		
		$total = array();
		# content 
		for($i=0; $i<$NoOfGroup; $i++) {
			
			$tempTableContent = "";
			$tempExportContent = "";
			
			$rowBudgetTotal = 0;
			$rowBudgetUsed = 0;
			
			$table .= '<tr>';
			$table .= '<td>'.$adminGroupAry[$Groups[$i]].'</td>';	
			$exportContent .= $adminGroupAry[$Groups[$i]]."\t";
			
			for($j=0; $j<$NoOfCategory; $j++) {
				$_cat_id = $allCategory[$j][0];
				$thisBudget = !isset($dataAry[$AcademicYearID][$Groups[$i]][$_cat_id]['Budget']) ? 0 : $dataAry[$AcademicYearID][$Groups[$i]][$_cat_id]['Budget'];
				$thisBudgetUsed = !isset($dataAry[$AcademicYearID][$Groups[$i]][$_cat_id]['BudgetUsed']) ? 0 : $dataAry[$AcademicYearID][$Groups[$i]][$_cat_id]['BudgetUsed'];
				
				$BudgetUsedDisplay = number_format($thisBudgetUsed,2)=="0.00" ? number_format($thisBudgetUsed,2) : "<a href='#TB_inline?height=500&width=750&inlineId=FakeLayer' onClick=Display_Detail('".$AcademicYearID."','".$Groups[$i]."','".$_cat_id."');return false; class='new_folder setting_row thickbox' title='".$Lang['Btn']['View']."'>".number_format($thisBudgetUsed,2)."</a>";
				# cell content 
				$tempTableContent .= '<td>'.number_format($thisBudget,2).'</td><td>'.$BudgetUsedDisplay.'</td>';
				$tempExportContent .= $thisBudget."\t";
				$tempExportContent .= $thisBudgetUsed."\t";
				//$exportContent .= "\n";
				
				# calculate row total
				$rowBudgetTotal += $thisBudget;
				$rowBudgetUsed += $thisBudgetUsed;	
				
				# calculate column total
				$total[$_cat_id]['Budget'] += $thisBudget;
				$total[$_cat_id]['BudgetUsed'] += $thisBudgetUsed;
			}
			
			$rowBudgetUsedDisplay = number_format($rowBudgetUsed,2)=="0.00" ? number_format($rowBudgetUsed,2) : "<a href='#TB_inline?height=500&width=750&inlineId=FakeLayer' onClick=Display_Detail('".$AcademicYearID."','".$Groups[$i]."','');return false; class='new_folder setting_row thickbox' title='".$Lang['Btn']['View']."'>".number_format($rowBudgetUsed,2)."</a>";
			
			$table .= '<td>'.number_format($rowBudgetTotal,2).'</td><td>'.$rowBudgetUsedDisplay.'</td>';
			$table .= $tempTableContent;
			$table .= '</tr>';
			$exportContent .= $rowBudgetTotal."\t";
			$exportContent .= $rowBudgetUsed."\t";
			//$exportContent .= "\n";
			$exportContent .= "$tempExportContent\n";
			
			$total['Total']['Budget'] += $rowBudgetTotal;
			$total['Total']['BudgetUsed'] += $rowBudgetUsed;
		}
		
		# display column total in last row
		/*
		$table .= '<tr><td colspan="100%" height="1">';
		$table .= '<table cellspacing=0 cellpadding=0 width=100%><tr><td><img src="/'.$image_path.'/'.$LAYOUT_SKIN.'"/10x10.gif" width="100%" height="1"></td></tr></table>';
		$table .= '</td></tr>';
		*/
		$table .= '<tr>';
		$table .= '<td>'.$Lang['General']['Total'].'</td>';
		$exportContent .= $Lang['General']['Total']."\t";
		
		$table .= '<td>'.number_format($total['Total']['Budget'],2).'</td>';
		$table .= '<td>'.number_format($total['Total']['BudgetUsed'],2).'</td>';
		$exportContent .= $total['Total']['Budget']."\t";
		$exportContent .= $total['Total']['BudgetUsed']."\t";
		
		for($i=0; $i<$NoOfCategory; $i++) { 
			$_cat_id = $allCategory[$i][0];
			$table .= '<td>'.number_format($total[$_cat_id]['Budget'],2).'</td>';
			$table .= '<td>'.number_format($total[$_cat_id]['BudgetUsed'],2).'</td>';
			$exportContent .= $total[$_cat_id]['Budget']."\t";
			$exportContent .= $total[$_cat_id]['BudgetUsed']."\t";
		}
		
		$table .= '</tr>';

		
		$table .= '</table>';
		
		
		$table .= '<div class="edit_bottom_v30">'.$linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "goExport()").'</div>';
		
		return ($flag==1) ? $table : $exportContent;
	}
	
	function Load_Group_Budget_Report($dataAry=array(), $Groups, $AcademicYearID, $flag=1)
	{
		# $flag : 1=view, 2=export
		
		global $Lang, $allCategory, $adminGroup, $linterface, $linvoice;
		
		$group_name = $linvoice->returnAdminGroupName($Groups);
		
		include_once("form_class_manage.php");
		$fcm = new academic_year();
		$year = $fcm->Get_All_Year_List();
		$yearAry = array();
		for($i=0; $i<count($year); $i++) {
			list($_id, $_name) = $year[$i];
			$yearAry[$_id] = $_name;	
		}		

		$adminGroupAry = array();
		for($i=0; $i<count($adminGroup); $i++) {
			list($_id, $_name) = $adminGroup[$i];
			$adminGroupAry[$_id] = $_name;
		}
		
		$NoOfCategory = count($allCategory);
		$NoOfYear = count($AcademicYearID);
		
		
		$table .= $linterface->GET_NAVIGATION2($group_name);
		$table .= '<br style="clear:both">';
		$exportContent .= $group_name."\n\n";	
		
		$table .= '<table class="common_table_list_v30 view_table_list_v30">';
		$exportContent .= "\t";		
		
		# table header
		$table .= '
				<thead>
					<tr>
						<th rowspan="2">&nbsp;</th>';
		$table .= '<th colspan="2">'.$Lang['General']['Total'].'</th>';
		$exportContent .= $Lang['General']['Total']."\t\t";
						
		for($i=0; $i<$NoOfCategory; $i++) {
			$table .= '<th colspan="2">'.Get_Lang_Selection($allCategory[$i][1], $allCategory[$i][2]).'</th>';
			$exportContent .= Get_Lang_Selection($allCategory[$i][1], $allCategory[$i][2])."\t\t";
		}
		
		$table .= '</tr><tr>';
		
				
		$exportContent .= "\n";
		$exportContent .= "\t";				
		
		for($i=0; $i<$NoOfCategory+1; $i++) { 
			$table .= '<th>'.$Lang['Invoice']['Budget'].'</th>';
			$table .= '<th>'.$Lang['Invoice']['AccumulatedExpense'].'</th>';
			$exportContent .= $Lang['Invoice']['Budget']."\t";
			$exportContent .= $Lang['Invoice']['AccumulatedExpense']."\t";
		}
		
		$table .= '</tr>';
		$exportContent .= "\n";
		
		# content 
		for($i=0; $i<$NoOfYear; $i++) {
			
			$tempTableContent = "";
			$tempExportContent = "";
			
			$rowBudgetTotal = 0;
			$rowBudgetUsed = 0;
			
			$table .= '<tr>';
			$table .= '<td nowrap>'.$yearAry[$AcademicYearID[$i]].'</td>';	
			$exportContent .= $yearAry[$AcademicYearID[$i]]."\t";
			
			for($j=0; $j<$NoOfCategory; $j++) {
				$_cat_id = $allCategory[$j][0];
				$thisBudget = !isset($dataAry[$AcademicYearID[$i]][$Groups][$_cat_id]['Budget']) ? 0 : $dataAry[$AcademicYearID[$i]][$Groups][$_cat_id]['Budget'];
				$thisBudgetUsed = !isset($dataAry[$AcademicYearID[$i]][$Groups][$_cat_id]['BudgetUsed']) ? 0 : $dataAry[$AcademicYearID[$i]][$Groups][$_cat_id]['BudgetUsed'];
				
				$BudgetUsedDisplay = number_format($thisBudgetUsed,2)=="0.00" ? number_format($thisBudgetUsed,2) : "<a href='#TB_inline?height=500&width=750&inlineId=FakeLayer' onClick=Display_Detail('".$AcademicYearID[$i]."','".$Groups."','".$_cat_id."');return false; class='new_folder setting_row thickbox' title='".$Lang['Btn']['View']."'>".number_format($thisBudgetUsed,2)."</a>";
				
				# cell content 
				$tempTableContent .= '<td>'.number_format($thisBudget,2).'</td><td>'.$BudgetUsedDisplay.'</td>';
				$tempExportContent .= $thisBudget."\t";
				$tempExportContent .= $thisBudgetUsed."\t";
				
				# calculate row total
				$rowBudgetTotal += $thisBudget;
				$rowBudgetUsed += $thisBudgetUsed;	
			}
			
			$rowBudgetUsedDisplay = number_format($rowBudgetUsed,2)=="0.00" ? number_format($rowBudgetUsed,2) : "<a href='#TB_inline?height=500&width=750&inlineId=FakeLayer' onClick=Display_Detail('".$AcademicYearID[$i]."','".$Groups."','');return false; class='new_folder setting_row thickbox' title='".$Lang['Btn']['View']."'>".number_format($rowBudgetUsed,2)."</a>";
			
			$table .= '<td>'.number_format($rowBudgetTotal,2).'</td><td>'.$rowBudgetUsedDisplay.'</td>';
			$table .= $tempTableContent;
			$table .= '</tr>';
			$exportContent .= $rowBudgetTotal."\t";
			$exportContent .= $rowBudgetUsed."\t";
			$exportContent .= $tempExportContent."\t";
			$exportContent .= "\n";
		}
		
		$table .= '</thead>
		';
		$table .= '</table>';
		
		
		$table .= '<div class="edit_bottom_v30">'.$linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "goExport()").'</div>';
		
		return ($flag==1) ? $table : $exportContent;
	}
	
	function Display_Budget_Detail($AcademicYearID='', $GroupID='', $CategoryID='', $flag=1) 
	{
		# $flag : 1=view, 2=export
		
		global $Lang, $linterface, $linvoice, $i_no_record_exists_msg, $i_general_yes, $i_general_no;
		
		# admin group
		$adminGroup = $linvoice->returnAdminGroup();
		$groupAry = array();
		for($i=0; $i<count($adminGroup); $i++) {
			list($_id, $_name) = $adminGroup[$i];
			$groupAry[$_id] = $_name;
		}
		
		# item category
		$cat = $linvoice->Get_All_Category();
		$catAry = array();
		for($i=0; $i<count($cat); $i++) {
			list($_id, $_name_b5, $_name_en) = $cat[$i];
			$catAry[$_id] = Get_Lang_Selection($_name_b5, $_name_en);
		}
		
		# Budget Description
		$table .= '<br style="clear:both">';
		# year info
		include_once("form_class_manage.php");
		$fcm = new academic_year($AcademicYearID);
		$selectedYear = Get_Lang_Selection($fcm->YearNameB5, $fcm->YearNameEN);
		$table .= '<table class="form_table_v30">';
		$table .= '
			<tr>
				<td class="field_title">'.$Lang['General']['AcademicYear'].'</td>
				<td class="row_content">'.$selectedYear.'</td>
			</tr>
		';
		$exportContent .= $Lang['General']['AcademicYear'].":\t";
		$exportContent .= $selectedYear."\t";
		$exportContent .= "\n";
		
		# admin group info
		$sql = "SELECT ".Get_Lang_Selection("NameChi", "NameEng")." as groupName FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID='$GroupID'";
		$result = $linvoice->returnVector($sql);
		$groupName = $result[0];
		$table .= '
			<tr>
				<td class="field_title">'.$Lang['Invoice']['Group'].'</td>
				<td class="row_content">'.$groupName.'</td>
			</tr>
		';
		$exportContent .= $Lang['Invoice']['Group'].":\t";
		$exportContent .= $groupName."\t";
		$exportContent .= "\n";
		
		if($CategoryID != "") {
			$catInfo = $linvoice->Get_Category($CategoryID);
			$table .= '
				<tr>
					<td class="field_title">'.$Lang['Invoice']['CategoryName'].'</td>
					<td class="row_content">'.Get_Lang_Selection($catInfo['NameChi'],$catInfo['NameEng']).'</td>
				</tr>
			';	
			$exportContent .= $Lang['Invoice']['CategoryName'].":\t";
			$exportContent .= Get_Lang_Selection($catInfo['NameChi'],$catInfo['NameEng'])."\t";
			$exportContent .= "\n";
		}
		
		$table .= '</table>';
		$exportContent .= "\n";
		
		# Item detail
		if($CategoryID=="") $width = "20";
		$table .= '
			<br style="clear:both">
			<table class="common_table_list">
			<thead>
				<tr>
					<th class="num_check" width="1">#</th>
					<th width="20%">'.$Lang['Invoice']['InvoiceNo'].'</th>
					<th width="'.(40-$width).'%">'.$Lang['Invoice']['ItemName'].'</th>
					<th width="20%">'.$Lang['Invoice']['SubTotalAmount'].'</th>
					<th width="10%">'.$Lang['Invoice']['Quantity'].'</th>';
		if($CategoryID == "") {
			$table .= '	<th width="'.$width.'%">'.$Lang['Invoice']['Category'].'</th>';
		}
		$table .= ' <th width="10%">'.$Lang['Invoice']['IsAsset'].'</th>
				</tr>
			</thead>';
		$exportContent .= "#\t";
		$exportContent .= $Lang['Invoice']['InvoiceNo']."\t";
		$exportContent .= $Lang['Invoice']['ItemName']."\t";
		$exportContent .= $Lang['Invoice']['SubTotalAmount']."\t";	
		$exportContent .= $Lang['Invoice']['Quantity']."\t";	
		if($CategoryID == "") {
			$exportContent .= $Lang['Invoice']['Category']."\t";
		}	
		$exportContent .= $Lang['Invoice']['IsAsset']."\t";	
		$exportContent .= "\n";
			
			
		$data = $linvoice->Get_Budget_Detail($AcademicYearID, $GroupID, $CategoryID);
		
		$NoOfRecord = count($data);
		
		$total = 0;
		for($i=0; $i<$NoOfRecord; $i++) {
			# table content
			$table .= '<tr>';
			$table .= '<td>'.($i+1).'</td>';
			$table .= '<td>'.$data[$i]['InvoiceNo'].'</td>';
			$table .= '<td>'.Get_Lang_Selection($data[$i]['NameChi'], $data[$i]['NameEng']).'</td>';
			$table .= '<td>'.$data[$i]['Price'].'</td>';
			$table .= '<td>'.$data[$i]['Quantity'].'</td>';
			if($CategoryID == "") {
				$table .= '<td>'.$catAry[$data[$i]['CategoryID']].'</td>';
			}
			$table .= '<td>'.($data[$i]['IsAssetItem']==1 ? $i_general_yes : $i_general_no).'</td>';
			$table .= '</tr>';
			
			# export content
			$exportContent .= ($i+1)."\t";
			$exportContent .= $data[$i]['InvoiceNo']."\t";
			$exportContent .= Get_Lang_Selection($data[$i]['NameChi'], $data[$i]['NameEng'])."\t";
			$exportContent .= $data[$i]['Price']."\t";
			$exportContent .= $data[$i]['Quantity']."\t";
			if($CategoryID == "") {
				$exportContent .= $catAry[$data[$i]['CategoryID']]."\t";
			}
			$exportContent .= ($data[$i]['Quantity']==1 ? $i_general_yes : $i_general_no)."\t";
			$exportContent .= "\n";
			
			$total += $data[$i]['Price'];
		} 
		
		if($NoOfRecord==0) {
			$table .= '<tr><td colspan="100%">'.$i_no_record_exists_msg.'</td></tr>';
			$exportContent .= $i_no_record_exists_msg."\n";	
		} else {
			$table .= '<tr>';
			$table .= '<td colspan="3" align="right">'.$Lang['General']['Total'].' : </td>';
			$table .= '<td colspan="3">'.number_format($total,2).'</td>';
			$table .= '</tr>';	
			$exportContent .= "\t\t";
			$exportContent .= $Lang['General']['Total']." : \t";
			$exportContent .= $total."\t";
		}
		
		
		$table .= '</table>';
		
		$table .= '<div class="edit_bottom_v30">';
		$table .= $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "goExportDetail()");
		$table .= '&nbsp;';
		$table .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.tb_remove();");
		$table .= '</div>';
		
		return ($flag==1) ? $table : $exportContent;
	}
	
}
