<?php
// Editing by 

/*
 * 2018-09-05 (Anna): MODIFIED returnEvent(), display titleEng/nature/location according to UI
 * 2018-08-08 (Henry): fixed sql injection 
 */
class libevent extends libdb{

        var $Event;
        var $EventID;
        var $Title;
        var $Description;
        var $EventDate;
        var $EventVenue;
        var $EventNature;
        var $RecordType;
        var $RecordStatus;
        var $DateInput;
        var $DateModified;
        var $OwnerGroupID;
        var $isSkipCycle;
        var $Internal;

        function libevent($EventID=""){
                $this->libdb();
                if($EventID<>""){
                        $this->Event = $this->returnEvent($EventID);
                        $this->EventID = $this->Event[0][0];
                        $this->Title = $this->Event[0][1];
                        $this->Description = $this->Event[0][2];
                        $this->EventDate = $this->Event[0][3];
                        $this->EventVenue = $this->Event[0][4];
                        $this->EventNature = $this->Event[0][5];
                        $this->RecordType = $this->Event[0][6];
                        $this->RecordStatus = $this->Event[0][7];
                        $this->DateInput = $this->Event[0][8];
                        $this->DateModified = $this->Event[0][9];
                        $this->OwnerGroupID = $this->Event[0][10];
                        $this->isSkipCycle = $this->Event[0][11];
                        $this->Internal = $this->Event[0][12];
                        $this->EventLocationID = $this->Event[0][13];
                        $this->InputBy = $this->Event[0][14];
                        
                        # check if EventVenue is empty, retrieve data from EventLocationID
                        if(empty($this->EventVenue) && $this->EventLocationID)
                        {
	                        include_once("liblocation.php");
	                        $objRoom = new Room($this->EventLocationID);
							$this->EventVenue = $objRoom->BuildingName ." > " . $objRoom->FloorName ." > " . $objRoom->RoomName;
                    	}
                }
        }

        function returnEvent($EventID){
                $sql = "SELECT EventID, 
                        If (TitleEng IS NULL Or TitleEng = '',Title, ".Get_Lang_Selection('Title','TitleEng').") as Title,  
                        If (DescriptionEng IS NULL Or DescriptionEng = '',Description, ".Get_Lang_Selection('Description','DescriptionEng').")  as Description,  
                        DATE_FORMAT(EventDate, '%Y-%m-%d'), 
                        If (EventVenueEng IS NULL Or EventVenueEng = '', EventVenue, ".Get_Lang_Selection('EventVenue','EventVenueEng').")  as EventVenue,   
                        If (EventNatureEng IS NULL Or EventNatureEng = '', EventNature, ".Get_Lang_Selection('EventNature','EventNatureEng').") as EventNature,   
                        RecordType, RecordStatus, DateInput, DateModified, OwnerGroupID, isSkipCycle, Internal, EventLocationID, InputBy FROM INTRANET_EVENT WHERE EventID = '".$EventID."'";
                return $this->returnArray($sql);
        }
        function returnPosterName()
        {
                 global $i_general_sysadmin;
                 /*
                 if ($this->OwnerUserID == "") return $i_general_sysadmin;
                 $name_field = getNameFieldWithClassNumberByLang();
                 $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '".$this->OwnerUserID."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
                 */
                 
                 if ($this->InputBy == "") return $i_general_sysadmin;
                 $name_field = getNameFieldWithClassNumberByLang();
                 $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '".$this->InputBy."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }
        
        function returnOwnerGroup()
        {
                 if ($this->OwnerGroupID == "") return "";
                 $sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID = '".$this->OwnerGroupID."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }
        function returnTargetGroups()
        {
                 if ($this->EventID == "") return array();
                 $sql = "SELECT DISTINCT a.Title
                         FROM INTRANET_GROUPEVENT as b
                              LEFT OUTER JOIN INTRANET_GROUP as a ON b.GroupID = a.GroupID
                         WHERE EventID = '".$this->EventID."'";
                 return $this->returnVector($sql);
        }  
  
        function display(){
                global $i_EventDescription, $i_EventDate, $i_EventVenue, $i_EventNature, $i_EventRecordType, $i_EventDateModified;
                global $i_EventTypeSchool, $i_EventTypeAcademic, $i_EventTypeHoliday, $i_EventTypeGroup;
                global $i_general_WholeSchool,$i_general_TargetGroup,$i_EventPoster, $Lang;
                switch($this->RecordType){
                        case 0: $i_EventType = $Lang['EventType']['School']; 				$c=2;	$css="indexpopsubtitleschevent";	break;
                        case 1: $i_EventType = $Lang['EventType']['Academic']; 				$c=3;	$css="indexpopsubtitleacevent";		break;
                        case 2: $i_EventType = $Lang['EventType']['Group']; 				$c=1;	$css="indexpopsubtitlegroupevent";	break;
                        case 3: $i_EventType = $Lang['EventType']['PublicHoliday']; 		$c=4;	$css="indexpopsubtitlehday";		break;
                        case 4: $i_EventType = $Lang['EventType']['SchoolHoliday']; 		$c=9;	$css="indexpopsubtitleschoolholiday";	break;
                }   
                $poster = $this->returnPosterName();
                  
                $ownerGroup = $this->returnOwnerGroup();
                if ($this->RecordType == 2)
                    $targetGroups = $this->returnTargetGroups();
                if ($ownerGroup == "")
                {
                    $displayName = $poster;
                }
                else
                {
                    $displayName = "$poster ($ownerGroup)";
                }
                if ($this->RecordType == 2)
                {
                    if (sizeof($targetGroups)==0)
                        $target = $i_general_WholeSchool;
                    else $target = implode(", ",$targetGroups);
                }
                
                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='10'>";
                $x .= "<tr>";
                $x .= "<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                $x .= "<tr>";
                $x .= "<td class='indexpopsubtitle $css'>". intranet_wordwrap($this->Title,30,"\n",1) ."</td>";
                $x .= "</tr>";
                $x .= "</table></td></tr>";
                $x .= "<tr>";
                $x .= "<td><table width='100%' border='0' cellspacing='4' cellpadding='3'>";
                $x .= "<tr>";
                $x .= "<td width='30%' valign='top' nowrap class='tabletext'>$i_EventDate</td>";
                $x .= "<td bgcolor='#FFFFFF' class='tabletext'>". $this->EventDate ."</td>";
                $x .= "</tr>";
                $x .= "<tr>";
                $x .= "<td valign='top' nowrap class='tabletext'>$i_EventRecordType</td>";
                $x .= "<td bgcolor='#FFFFFF' class='tabletext'><span class='indexeventlink$c'>$i_EventType</span></td>";
                $x .= "</tr>";

                $x .= "<tr>";
                $x .= "<td valign='top' nowrap class='tabletext'>$i_EventVenue</td>";
                $x .= "<td bgcolor='#FFFFFF' class='tabletext'>". (trim($this->EventVenue) ? intranet_wordwrap($this->EventVenue,20,"\n",1) : "--") ."</td>";
                $x .= "</tr>";

		$x .= "<tr>";
                $x .= "<td valign='top' nowrap class='tabletext'>$i_EventNature</td>";
                $x .= "<td bgcolor='#FFFFFF' class='tabletext'>". (trim($this->EventNature) ? intranet_wordwrap($this->EventNature,20,"\n",1) : "--") ."</td>";
                $x .= "</tr>";
		
                $x .= "<tr>";
                $x .= "<td valign='top' nowrap class='tabletext'>$i_EventPoster<br></td>";
                $x .= "<td bgcolor='#FFFFFF' class='tabletext'>".intranet_wordwrap($displayName,30,"\n",1)."</td>";
                $x .= "</tr>";
                if ($this->RecordType == 2)
                {
                        $x .= "<tr>";
                        $x .= "<td valign='top' nowrap class='tabletext'>$i_general_TargetGroup<br></td>";
                        $x .= "<td bgcolor='#FFFFFF' class='tabletext'>".intranet_wordwrap($target,30,"\n",1)."</td>";
                        $x .= "</tr>";
		}

		$x .= "<tr>";
                $x .= "<td valign='top' nowrap class='tabletext'>$i_EventDescription</td>";
                $x .= "<td bgcolor='#FFFFFF' class='tabletext'>". (trim($this->Description) ? nl2br($this->Description) : "--") ."</td>";
                $x .= "</tr>";
		
                $x .= "</table></td>";
                $x .= "</tr>";
                $x .= "</table>";

                return $x;
        }

                # for alumni
                # --- index.php
                function returnGroupEvent($GroupID){
                         global $i_general_sysadmin;
                         $user_field = getNameFieldWithClassNumberByLang("c.");
                         $sql  = "SELECT
                                                                                DATE_FORMAT(a.EventDate, '%Y-%m-%d'),
                                                                                CONCAT('<a href=javascript:alumni_view_event(', a.EventID, ') ><span class=13-blue>', a.Title, '</span></a>'),
                                                                                if (a.UserID IS NOT NULL AND a.UserID != 0,$user_field,'$i_general_sysadmin') as Username
                                                                FROM
                                                                         INTRANET_EVENT as a LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = a.UserID
                                                                                        LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID AND b.GroupID = $GroupID
                                                                WHERE
                                                                                CURDATE() <= a.EventDate AND
                                                                         a.RecordStatus = 1 AND
                                                                         a.RecordType = 3 AND
                                                                         b.GroupEventID IS NOT NULL
                                                                ORDER BY a.EventDate DESC";
                        return $this->returnArray($sql,3);
                }
                # for alumni
                # --- moreevent.php
                function returnGroupAllEvent($GroupID){
						global $i_general_sysadmin;
                         $user_field = getNameFieldWithClassNumberByLang("c.");
						
                         $sql  = "SELECT DATE_FORMAT(a.EventDate, '%Y-%m-%d'),
													CONCAT('<a href=javascript:alumni_view_event(', a.EventID, ') ><span class=13-blue>', a.Title, '</span></a>'),
                                                    if (a.UserID IS NOT NULL AND a.UserID != 0,$user_field,'$i_general_sysadmin') as Username
                                      FROM INTRANET_EVENT as a 
												LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = a.UserID
                                                LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID AND b.GroupID = $GroupID
                                    WHERE a.RecordStatus = 1 AND
                                                 a.RecordType = 3 AND
                                                 b.GroupEventID IS NOT NULL
                                  ORDER BY a.EventDate DESC";

                        return $this->returnArray($sql,3);
                }

                # for alumni
                # --- index.php
        function displaySchoolEvent($UserID){
                global $image_path,$intranet_session_language;
                $row = $this->returnGroupEvent($UserID);

                                $x ="";

                                if(sizeof($row)==0){
                                                        global $i_no_record_exists_msg;
                                                        $x .= "<span class='13-blue'>$i_no_record_exists_msg</span>\n";
                                }
                                else{
                                for($i=0; $i<sizeof($row); $i++){
                                $AnnouncementDate = $row[$i][0];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][1],40,"\n",1);
                                $announcer = $row[$i][2];

                                                                $x .=        "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
                                                                $x .=        "<tr>\n";
                                                                $x .=        "<td width='25' valign='top'><img src='$image_path/bullet.gif' width='25' height='17' align='absmiddle'></td>\n";
                                                                $x .=         "<td class='13-blue'>" . $AnnouncementTitle ."</td>\n";
                                                                $x .=        "</tr>\n";
                                                                $x .=        "<tr>\n";
                                                                $x .=        "<td>&nbsp;</td>\n";
                                                                $x .=        "<td class='11-grey'>" . $AnnouncementDate . " " . $announcer  ."</td>\n";
                                                                $x .=        "</tr>\n";
                                                                $x .=        "<tr>\n";
                                                                $x .=        "<td>&nbsp;</td>\n";
                                                                $x .=        "<td class='11-grey'>&nbsp;</td>\n";
                                                                $x .=        "</tr>\n";
                                                                $x .=        "</table>\n";
                                                        }
                                }
                return $x;
        }
                # for alumni
        function display2(){
                global $i_EventDescription, $i_EventDate, $i_EventVenue, $i_EventNature, $i_EventRecordType, $i_EventDateModified;
                global $i_EventTypeSchool, $i_EventTypeAcademic, $i_EventTypeHoliday, $i_EventTypeGroup;
                global $i_general_WholeSchool,$i_general_TargetGroup,$i_EventPoster;
                switch($this->RecordType){
                        case 0: $i_EventType = $i_EventTypeSchool; break;
                        case 1: $i_EventType = $i_EventTypeAcademic; break;
                        case 2: $i_EventType = $i_EventTypeHoliday; break;
                        case 3: $i_EventType = $i_EventTypeGroup; break;
                }
                $poster = $this->returnPosterName();
                $ownerGroup = $this->returnOwnerGroup();
                if ($this->RecordType == 3)
                    $targetGroups = $this->returnTargetGroups();
                if ($ownerGroup == "")
                {
                    $displayName = $poster;
                }
                else
                {
                    $displayName = "$poster ($ownerGroup)";
                }
                if ($this->RecordType == 3)
                {
                    if (sizeof($targetGroups)==0)
                        $target = $i_general_WholeSchool;
                    else $target = implode(", ",$targetGroups);
                }

                $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 $bgcolortag class='13-black' bordercolordark=#FFFFFF bordercolorlight=#5DA5C9>\n";
                $x .= "<tr bgcolor=#E3DB9C><td colspan=2 class='13-black-bold' align=center>".intranet_wordwrap($this->Title,30,"\n",1)."</td></tr>\n";
                $x .= "<tr><td align=right width=30%>$i_EventDate:</td><td width=70% >".$this->EventDate."</td></tr>\n";
                $x .= "<tr><td align=right >$i_EventRecordType:</td><td >".$i_EventType."</td></tr>\n";
                $x .= "<tr><td align=right >$i_EventVenue:</td><td >".intranet_wordwrap($this->EventVenue,20,"\n",1)."&nbsp;</td></tr>\n";
                $x .= "<tr><td align=right >$i_EventNature:</td><td >".intranet_wordwrap($this->EventNature,20,"\n",1)."&nbsp;</td></tr>\n";
                $x .= "<tr><td align=right >$i_EventPoster:</td><td >".intranet_wordwrap($displayName,30,"\n",1)."&nbsp;</td></tr>\n";
                if ($this->RecordType == 3)
                    $x .= "<tr><td align=right >$i_general_TargetGroup:</td><td >".intranet_wordwrap($target,30,"\n",1)."&nbsp;</td></tr>\n";
                $x .= "<tr><td align=right   >$i_EventDescription:</td><td >".nl2br($this->convertAllLinks2($this->Description,30))."&nbsp;</td></tr>\n";
                $x .= "</table>\n";
                return $x;
        }
        function returnTargetGroupsID()
        {
                 if ($this->EventID == "") return array();
                 $sql = "SELECT DISTINCT a.GroupID
                         FROM INTRANET_GROUPEVENT as b
                              LEFT OUTER JOIN INTRANET_GROUP as a ON b.GroupID = a.GroupID
                         WHERE EventID = ".$this->EventID;
                 return $this->returnVector($sql);
        }
        function hasRightToView()
        {
                 if ($this->EventID == "" || $this->EventID == 0) return false;
                 if ($this->RecordType==0 || $this->RecordType==1 || $this->RecordType==2 || $this->RecordType==4)
                 {
                     return true;
                 }
                 if ($this->RecordType==3)
                 {
                     $targetGroups = $this->returnTargetGroupsID();
                     if (sizeof($targetGroups)==0) return true;
                     global $UserID;
                     $sql = "SELECT DISTINCT GroupID FROM INTRANET_USERGROUP WHERE UserID = '$UserID'";
                     $groups = $this->returnVector($sql);

                     for ($i=0; $i<sizeof($groups); $i++)
                     {
                          $target_gID = $groups[$i];
                          if (in_array($target_gID,$targetGroups))
                          {
                              return true;
                          }
                     }
                     return false;
                 }
                 return false;
        }

                # for alumni
        function displayAllEventList($GroupID){
                global $image_path,$intranet_session_language;
                global $i_EventDate,$i_EventTitle,$i_EventPoster;

                $row = $this->returnGroupAllEvent($GroupID);

               $x = "";
                $x .= "<table width=495 border=1 cellpadding=2 cellspacing=0 bordercolorlight=#5DA5C9 bordercolordark=#FFFFFF bgcolor=#FFFFFF class=13-black>
                    <tr bgcolor=#E3DB9C>
                      <td width=120 align=left  class=13-black-bold><strong>$i_EventDate</strong></td>
                      <td align=left class=13-black-bold><strong>$i_EventTitle</strong></td>
                      <td width=120 align=left  class=13-black-bold><strong>$i_EventPoster</strong></td>
                    </tr>";
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td align=center colspan=3 class=body>$i_no_record_exists_msg</td></tr>\n";
                }else{
                        for($i=0; $i<sizeof($row); $i++){

/*
                                $AnnouncementID = $row[$i][0];
                                $AnnouncementDate = $row[$i][1];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);

                                $IsNew = $row[$i][3];
                                $Attachment = $row[$i][4];
                                $announcer = $row[$i][5];
                                $ownerGroup = $row[$i][6];
*/

                                $AnnouncementDate = $row[$i][0];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][1],40,"\n",1);
                                $announcer = $row[$i][2];

/*
                                if ($ownerGroup == "")
                                {
                                    $displayName = "$i_AnnouncementSystemAdmin";
                                }
                                else
                                {
                                    $displayName = "$announcer <br> $ownerGroup";
                                }
*/

                                $clip = ($Attachment == ""? "&nbsp;": " $i_iconAttachment");
                                $newFlag = (($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "&nbsp;");
                                $x .= "<tr>\n";
                                $x .= "<td valign=top class=body>
                                         <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                           <tr>
                                             <td colspan=2>$AnnouncementDate</td>
                                           </tr>
                                         </table>
                                       </td>
                                       <td align=left valign=top class=bodylink><a class=bodylink href=javascript:alumni_view_announcement($AnnouncementID)>$AnnouncementTitle</a></td>
                                       <td valign=top class=bodylink>
                                         <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                           <tr>
                                             <td>$announcer</td>
                                             <td width=18 align=right>$clip</td>
                                           </tr>
                                         </table>
                                       </td>\n";
                                $x .= "</tr>\n";
                        }
                }
                $x .= "</table>\n";
                return $x;
        }

}
?>
