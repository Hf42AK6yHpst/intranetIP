<?php
/*
 * upload.php
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under GPL License.
 *
 * License: http://www.plupload.com/license
 * Contributing: http://www.plupload.com/contributing
 *
 * variables to define before including this file:
 * - $pluploadPar['targetDir']
 * 	 the folder you want to place the file during upload, such as $eclass_filepath.'/files/tmp'
 *   
 */
 
/*
 * Changes:
 * 2020-05-01 (Bill): Added file formats checking
 * 2019-07-19 (Carlos): Added explicit content-type header as some firewall may block due to no content-type specified. 
 * 2016-09-30 (Carlos): Removed the get_magic_quotes_gpc() checking in order to make compatible with PHP5.4+
 */

// HTTP headers for no cache etc
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Content-type: application/json");

// Settings
//$pluploadPar['targetDir'] = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";

$cleanupTargetDir = true; 	// Remove old files
$maxFileAge = 5 * 3600; 	// Temp file age in seconds

// 5 minutes execution time
@set_time_limit(5 * 60);

// Uncomment this one to fake upload time
// usleep(5000);

// Get parameters
$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

# check if a file is uploaded (avoid displaying chuck info when echo out $filePath)
$pluploadPar['singleFileUploaded'] = false;

//if(get_magic_quotes_gpc() ==1) 
$fileName = stripslashes($fileName);
############################################################

// Clean the fileName for security reasons
//$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

// Make sure the fileName is unique but only if chunking is disabled
if ($chunks < 2 && file_exists($pluploadPar['targetDir'] . DIRECTORY_SEPARATOR . $fileName)) {
	$ext = strrpos($fileName, '.');
	$fileName_a = substr($fileName, 0, $ext);
	$fileName_b = substr($fileName, $ext);

	$count = 1;
	while (file_exists($pluploadPar['targetDir'] . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b)) {
		$count++;
    }
	$fileName = $fileName_a . '_' . $count . $fileName_b;
}
$filePath = $pluploadPar['targetDir'] . DIRECTORY_SEPARATOR . $fileName;

// Create target dir
if (!file_exists($pluploadPar['targetDir'])) {
	@mkdir($pluploadPar['targetDir']);
}

// Remove old temp files	
if ($cleanupTargetDir && is_dir($pluploadPar['targetDir']) && ($dir = opendir($pluploadPar['targetDir']))) {
	while (($file = readdir($dir)) !== false)
	{
		$tmpfilePath = $pluploadPar['targetDir'] . DIRECTORY_SEPARATOR . $file;

		// Remove temp file if it is older than the max age and is not the current file
		if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
			@unlink($tmpfilePath);
		}
	}

	closedir($dir);
} else {
	die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
}

// Look for the content type header
if (isset($_SERVER["HTTP_CONTENT_TYPE"])) {
	$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
}
if (isset($_SERVER["CONTENT_TYPE"])) {
	$contentType = $_SERVER["CONTENT_TYPE"];
}

# Block invalid file formats
if (isset($filePath) && $filePath != '')
{
    $file = get_file_basename($filePath);
    $fileExt = strtolower(substr($file, strrpos($file,".")));
	if(in_array($fileExt, $____block_file_upload_extensions____)) {
        logBlockFileUploadExtensions($filePath);
        die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "'.$Lang['plupload']['move_uploaded_file_failed'].'"}, "id" : "id"}');
    }
}

// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
if (strpos($contentType, "multipart") !== false)
{
	if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
		// Open temp file
		$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
		if ($out) {
			// Read binary input stream and append it to temp file
			$in = fopen($_FILES['file']['tmp_name'], "rb");

			if ($in) {
				while ($buff = fread($in, 4096)) {
					fwrite($out, $buff);
                }
			} else {
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "'.$Lang['plupload']['open_input_stream_failed'].'"}, "id" : "id"}');
            }
			fclose($in);
			fclose($out);
			@unlink($_FILES['file']['tmp_name']);
		} else {
			die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "'.$Lang['plupload']['open_output_stream_failed'].'"}, "id" : "id"}');
        }
	} else {
		die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "'.$Lang['plupload']['move_uploaded_file_failed'].'"}, "id" : "id"}');
    }
}
else
{
	// Open temp file
	$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
	if ($out) {
		// Read binary input stream and append it to temp file
		$in = fopen("php://input", "rb");

		if ($in) {
			while ($buff = fread($in, 4096))
				fwrite($out, $buff);
		} else {
			die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "'.$Lang['plupload']['open_input_stream_failed'].'"}, "id" : "id"}');
        }

		fclose($in);
		fclose($out);
	} else {
		die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "'.$Lang['plupload']['open_output_stream_failed'].'"}, "id" : "id"}');
    }
}

// Check if file has been uploaded
if (!$chunks || $chunk == $chunks - 1) {
	// Strip the temp .part suffix off 
	rename("{$filePath}.part", $filePath);
	$pluploadPar['uploadedFilePath'] = $filePath;
	$pluploadPar['singleFileUploaded'] = true;
}

// Return JSON-RPC response
//die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
echo '{"jsonrpc" : "2.0", "result" : null, "id" : "id"}';
?>
