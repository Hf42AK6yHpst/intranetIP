<?php
### Note: If you update this file (libattendance.php) to JUNIOR site, please make sure you have already copied the function displayStudentRecordByYear_jr() from the JUNIOR site first. ###

// Modifing by 

# 2020-04-21 (Tommy): modified displayStudentRecordByYear(), only show in class record
# 2013-08-05 by Henry Chan: added function returnDaysBeforeToOpenData()

if (!defined("LIBATTENDANCE_DEFINED"))         // Preprocessor directives
{

 define("LIBATTENDANCE_DEFINED",true);

 class libattendance extends libclass{
       var $StudentAttendanceID;
       var $UserID;
       var $AttendanceDate;
       var $Year;
       var $Semester;
       var $RecordType;
       var $DayType;
       var $Reason;
       var $type_array;
       var $YearDescending = true;   # Eric Yip : Boolean controling sorting order of year

       function libattendance($StudentAttendanceID="")
       {
                $this->libclass();
                $this->type_array = array(1,2,3);
                if ($StudentAttendanceID != "")
                {
                    $this->StudentAttendanceID = $StudentAttendanceID;
                    $this->retrieveRecord($this->StudentAttendanceID);
                }
       }
       function retrieveRecord($id)
       {
                $fields = "UserID, DATE_FORMAT(AttendanceDate,'%Y-%m-%d'), Year,RecordType, DayType, Reason,Semester";
                $conds = "StudentAttendanceID = '$id'";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_ATTENDANCE WHERE $conds";
                $result = $this->returnArray($sql,7);
                list(
                $this->UserID, $this->AttendanceDate,$this->Year, $this->RecordType,
                $this->DayType, $this->Reason, $this->Semester) = $result[0];
                return $result;
       }
       function returnDateConditions($start, $end)
       {
                if ($start == "" || $end == "")
                {
                    $date_conds = "";
                }
                else
                {
                    $date_conds = "AND UNIX_TIMESTAMP(AttendanceDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')";
                }
                return $date_conds;
       }
       function returnReasonConditions($reason,$prefix="")
       {
                return ($reason==""? "" : " AND ".$prefix."Reason = '$reason'");
       }

       function returnStudentRecord($uid, $start, $end, $returnSQL =0)
       {
                $fields = "DATE_FORMAT(AttendanceDate,'%Y-%m-%d'), RecordType, DayType, Reason";
                $conds = "UserID = '$uid' AND UNIX_TIMESTAMP(AttendanceDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')";
                $order = "AttendanceDate DESC";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_ATTENDANCE WHERE $conds ";
                if(!$returnSQL)	$sql .= "ORDER BY $order";
                return ($returnSQL ? $sql : $this->returnArray($sql,4));
       }
       function getCountByClassName ($class, $start, $end)
       {
                $sql = "SELECT a.RecordType, COUNT(a.StudentAttendanceID)
                        FROM PROFILE_STUDENT_ATTENDANCE as a
                        LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        WHERE b.ClassName = '$class' AND
                        UNIX_TIMESTAMP(a.AttendanceDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')
                        Group BY a.RecordType";
                $data = $this->returnArray($sql,2);
                $pos = 0;
                for ($i=0; $i<3; $i++)
                {
                     list($type,$count) = $data[$pos];
                     if ($type==$i+1)
                     {
                         $result[$i] = $count;
                         $pos++;
                     }
                     else
                     {
                         $result[$i] = 0;
                     }
                }
                return $result;


       }
       /*
       function getClassAttendanceList ($start, $end)
       {
                $classList = $this->getClassList();
                if (sizeof($classList)==0) return array();
                for ($i=0; $i<sizeof($classList); $i++)
                {
                     list($id, $name,$lvlID) = $classList[$i];
                     $temp = $this->getCountByClassName($name,$start,$end);
                     $result[] = array($id,$name,$temp[0],$temp[1],$temp[2],$lvlID);
                }
                return $result;
       }
       */
       function getClassAttendanceList($start, $end, $reason="")
       {
                               global $intranet_root;
                               include("$intranet_root/includes/libstudentprofile.php");
                               $lstudentprofile = new libstudentprofile();

                $dateConds = $this->returnDateConditions($start,$end);
                $reasonConds = $this->returnReasonConditions($reason);
                //$count_sql = "IF(a.DayType = 1, COUNT(a.StudentAttendanceID), COUNT(a.StudentAttendanceID))";
                //$count_sql = ($lstudentprofile->attendance_count_method == 1) ? "IF(a.DayType=1, COUNT(a.StudentAttendanceID), COUNT(a.StudentAttendanceID)*0.5)" : "COUNT(a.StudentAttendanceID) ";
                $counts = array();
                
                for ($i=0; $i<sizeof($this->type_array); $i++)
                {
                     $type = $this->type_array[$i];
                     $count_sql = ($lstudentprofile->attendance_count_method == 1 && $type == 1) ? "IF(a.DayType = 1, COUNT(a.StudentAttendanceID), COUNT(a.StudentAttendanceID)*0.5)" : "COUNT(a.StudentAttendanceID)";
                     $sql = "SELECT c.YearClassID, $count_sql
                             FROM 
	                             PROFILE_STUDENT_ATTENDANCE as a 
	                             LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
	                             LEFT OUTER JOIN YEAR_CLASS as c ON b.ClassName = c.ClassTitleEN
                             WHERE b.RecordStatus IN (0,1,2) AND a.RecordType = '$type' $dateConds $reasonConds
                             GROUP BY c.YearClassID, a.DayType";
                     //debug_r($sql);
                     $result = $this->returnArray($sql,2);

                     for ($j=0; $j<sizeof($result); $j++)
                     {
                          list ($ClassID,$count) = $result[$j];
                          $counts[$i][$ClassID] += $count;
                     }
                }

                $classList = $this->getClassList();
                for ($i=0; $i<sizeof($classList); $i++)
                {
                     list($ClassID, $ClassName, $LvlID) = $classList[$i];
                     $returnResult[$i][0] = $ClassID;
                     $returnResult[$i][1] = $ClassName;
                     $returnResult[$i][2] = $LvlID;
                     for ($j=0; $j<sizeof($this->type_array); $j++)
                     {
                          $count = $counts[$j][$ClassID]+0;
                          $returnResult[$i][3+$j] = $count;
                     }
                }
                return $returnResult;

       }
       function getCountByStudent($studentid,$start,$end)
       {
                $sql = "SELECT COUNT(StudentAttendanceID) FROM PROFILE_STUDENT_ATTENDANCE
                        WHERE UserID=$studentid AND UNIX_TIMESTAMP(AttendanceDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end') AND RecordType = 1";
                $absence = $this->returnVector($sql);
                $sql = "SELECT COUNT(StudentAttendanceID) FROM PROFILE_STUDENT_ATTENDANCE
                        WHERE UserID=$studentid AND UNIX_TIMESTAMP(AttendanceDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end') AND RecordType = 2";
                $late = $this->returnVector($sql);
                $sql = "SELECT COUNT(StudentAttendanceID) FROM PROFILE_STUDENT_ATTENDANCE
                        WHERE UserID=$studentid AND UNIX_TIMESTAMP(AttendanceDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end') AND RecordType = 3";
                $earlyleave = $this->returnVector($sql);
                $entry = array($absence[0]+0,$late[0]+0,$earlyleave[0]+0);
                return $entry;
       }
       /*
       function getAttendanceListByClass($classid, $start, $end)
       {
                $studentList = $this->getClassStudentNameList($classid);
                if (sizeof($studentList)==0) return array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($id, $name, $classnumber) = $studentList[$i];
                     $temp = $this->getCountByStudent($id,$start,$end);
                     $result[] = array($id,$name,$classnumber,$temp[0],$temp[1],$temp[2]);
                }
                return $result;
       }
       */
//       function getAttendanceListByClass($classname,$start,$end,$reason,$classid='')
//       {
//                               global $intranet_root;
//                               include("$intranet_root/includes/libstudentprofile.php");
//                               $lstudentprofile = new libstudentprofile();
//
//                $dateConds = $this->returnDateConditions($start,$end);
//                $reasonConds = $this->returnReasonConditions($reason);
//                //$count_sql = ($lstudentprofile->attendance_count_method == 1) ? "IF(a.DayType=1, COUNT(a.StudentAttendanceID), COUNT(a.StudentAttendanceID)*0.5)" : "COUNT(a.StudentAttendanceID) ";
//                $counts = array();
//                for ($i=0; $i<sizeof($this->type_array); $i++)
//                {
//                     $type = $this->type_array[$i];
//                     $count_sql = ($lstudentprofile->attendance_count_method == 1 && $type == 1) ? "IF(a.DayType = 1, COUNT(a.StudentAttendanceID), COUNT(a.StudentAttendanceID)*0.5)" : "COUNT(a.StudentAttendanceID)";
//                     $sql = "SELECT b.UserID,$count_sql
//                             FROM PROFILE_STUDENT_ATTENDANCE as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
//                             WHERE b.ClassName = '$classname' AND b.RecordStatus IN (0,1,2) AND a.RecordType = '$type' $dateConds $reasonConds
//                             GROUP BY b.UserID, a.DayType";
//                     //debug_r($sql);
//                     $result = $this->returnArray($sql,2);
//                     for ($j=0; $j<sizeof($result); $j++)
//                     {
//                          list ($StudentID,$count) = $result[$j];
//                          $counts[$i][$StudentID] += $count;
//                     }
//                }
//                
//                if ($classid == '') {
//                	$studentList = $this->getStudentNameListByClassName($classname, "1");
//                }
//                else {
//                	$studentList = $this->getStudentByClassId($classid);
//                }
//                
//                for ($i=0; $i<sizeof($studentList); $i++)
//                {
//                	 if ($classid == '') {
//                     	list($StudentID, $StudentName, $classNumber) = $studentList[$i];
//                     	$returnResult[$i][0] = $StudentID;
//	                     $returnResult[$i][1] = $StudentName;
//	                     $returnResult[$i][2] = $classNumber;
//                	 }
//                	 else {
//                	 	$returnResult[$i][0] = $studentList[$i]['UserID'];
//                	 	$returnResult[$i][1] = '';
//                	 	$returnResult[$i][2] = $studentList[$i]['ClassNumber'];
//                	 }
//                     
//                     for ($j=0; $j<sizeof($this->type_array); $j++)
//                     {
//                          $count = $counts[$j][$StudentID]+0;
//                          $returnResult[$i][3+$j] = $count;
//                     }
//                }
//                return $returnResult;
//       }
	   function getAttendanceListByClass($classname,$start,$end,$reason,$parUserIdAry='')
       {
                               global $intranet_root;
                               include("$intranet_root/includes/libstudentprofile.php");
                               $lstudentprofile = new libstudentprofile();
                               
                               if ($parUserIdAry !== '') {
                               		$userIdConds = " And b.UserID IN ('".implode("','", (array)$parUserIdAry)."') ";
                               }
                               else {
                               		$classNameConds = " And b.ClassName = '".$classname."' ";
                               }

                $dateConds = $this->returnDateConditions($start,$end);
                $reasonConds = $this->returnReasonConditions($reason);
                //$count_sql = ($lstudentprofile->attendance_count_method == 1) ? "IF(a.DayType=1, COUNT(a.StudentAttendanceID), COUNT(a.StudentAttendanceID)*0.5)" : "COUNT(a.StudentAttendanceID) ";
                $counts = array();
                for ($i=0; $i<sizeof($this->type_array); $i++)
                {
                     $type = $this->type_array[$i];
                     $count_sql = ($lstudentprofile->attendance_count_method == 1 && $type == 1) ? "IF(a.DayType = 1, COUNT(a.StudentAttendanceID), COUNT(a.StudentAttendanceID)*0.5)" : "COUNT(a.StudentAttendanceID)";
                     $sql = "SELECT b.UserID,$count_sql
                             FROM PROFILE_STUDENT_ATTENDANCE as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                             WHERE b.RecordStatus IN (0,1,2) AND a.RecordType = '$type' $dateConds $reasonConds $userIdConds $classNameConds
                             GROUP BY b.UserID, a.DayType";
                     //debug_r($sql);
                     $result = $this->returnArray($sql,2);
                     for ($j=0; $j<sizeof($result); $j++)
                     {
                          list ($StudentID,$count) = $result[$j];
                          $counts[$i][$StudentID] += $count;
                     }
                }
                
                if ($parUserIdAry !== '') {
                	$name_field = getNameFieldByLang();
                	$sql = "SELECT UserID, $name_field, ClassNumber FROM INTRANET_USER WHERE UserID In ('".implode("','", (array)$parUserIdAry)."') ";
                	$studentList = $this->returnArray($sql);
                }
                else {
                	$studentList = $this->getStudentNameListByClassName($classname, "1");
                }
                
                
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                	 list($StudentID, $StudentName, $classNumber) = $studentList[$i];
                     $returnResult[$i][0] = $StudentID;
	                 $returnResult[$i][1] = $StudentName;
	                 $returnResult[$i][2] = $classNumber;
                     
                     for ($j=0; $j<sizeof($this->type_array); $j++)
                     {
                          $count = $counts[$j][$StudentID]+0;
                          $returnResult[$i][3+$j] = $count;
                     }
                }
                return $returnResult;
       }
       function getSelectType($tags, $selected="")
       {
                global $i_AttendanceTypeArray;
                $x = "<SELECT $tags>\n";
                for ($i=1; $i<sizeof($i_AttendanceTypeArray); $i++)
                {
                     $selected_tag = ($i==$selected? "SELECTED":"");
                     $x .= "<OPTION value=$i $selected_tag>".$i_AttendanceTypeArray[$i]."</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }
       function displayStudentRecord($studentid,$start,$end)
       {
                global $i_Attendance_Date,$i_Attendance_Type,$i_Attendance_DayType,$i_Attendance_Reason;
                global $i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave;
                global $i_DayTypeArray;
                global $i_no_record_exists_msg;

                $no_msg = $i_no_record_exists_msg;

                $result = $this->returnStudentRecord($studentid,$start,$end);
              $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>
        <tr>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Date</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Type</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_DayType</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Reason</td>
        </tr>\n";
              for ($i=0; $i<sizeof($result); $i++)
              {
                   list ($attendDate,$type,$dayType,$reason) = $result[$i];
                   switch ($type)
                   {
                           case 1: $typeStr = $i_Profile_Absent; break;
                           case 2: $typeStr = $i_Profile_Late; break;
                           case 3: $typeStr = $i_Profile_EarlyLeave; break;
                           default: $typeStr = "Error"; break;
                   }
                   $dayStr = $i_DayTypeArray[$dayType];
                   $x .= "<tr>
          <td class=td_center_middle bgcolor=#E8FBFC>$attendDate</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$typeStr</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$dayStr</td>
          <td class=td_center_middle bgcolor=#E8FBFC>&nbsp;$reason</td>
        </tr>";
              }
              if (sizeof($result)==0)
              {
                  $x .= "<tr><td colspan=4 class=td_center_middle bgcolor=#E8FBFC>".$no_msg."</td></tr>\n";
              }
              $x .= "</table>";
              return $x;

       }
       function returnDaysBeforeToOpenData(){
       	//Henry added
       			include_once("libgeneralsettings.php");
       			$GeneralSetting = new libgeneralsettings();
       			$SettingList[] = "'EnablePSLAD'";
				$SettingList[] = "'DaysBeforeToOpenData'";
				$Settings = $GeneralSetting->Get_General_Setting('LessonAttendance',$SettingList);
				$conds = "";
				if($Settings['EnablePSLAD'] == 1){
					$add_days = $Settings['DaysBeforeToOpenData'];
					$date = date('Y-m-d',strtotime($date) - (24*3600*$add_days));
                	 //$conds =' AND UNIX_TIMESTAMP(AttendanceDate) < UNIX_TIMESTAMP("'.$date.'")';
                	 $conds =' AND UNIX_TIMESTAMP(AttendanceDate) <= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL '.$add_days.' DAY))';
				}
				return $conds;
       }
       function returnDetailedRecordByYear($studentid, $year, $returnSQL=0)
       {
       			$fields = "DATE_FORMAT(AttendanceDate,'%Y-%m-%d'), RecordType, DayType, Reason";
                $conds = "UserID = '$studentid' AND Year = '$year'";
                $order = "AttendanceDate DESC";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_ATTENDANCE WHERE $conds ";
                if(!$returnSQL)	$sql .= "ORDER BY $order";
                return ($returnSQL ? $sql : $this->returnArray($sql,4));
       }
       function returnDetailedRecordByYear2($studentid, $year, $returnSQL=0)
       {
                $fields = "StudentAttendanceID, DATE_FORMAT(AttendanceDate,'%Y-%m-%d'), RecordType, DayType, Reason";
                $conds = "UserID = '$studentid' AND Year = '$year'";
                $order = "AttendanceDate DESC";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_ATTENDANCE WHERE $conds ";
                if(!$returnSQL)	$sql .= "ORDER BY $order";
                return ($returnSQL ? $sql : $this->returnArray($sql,5));
       }
       function returnDetailedRecordByDate1($studentid,$start_date,$end_date, $returnSQL = 0)
       {
                $fields = "StudentAttendanceID, DATE_FORMAT(AttendanceDate,'%Y-%m-%d'), RecordType, DayType, Reason";
                $conds = "UserID = '$studentid' AND UNIX_TIMESTAMP(AttendanceDate) BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')";
                $order = "AttendanceDate DESC";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_ATTENDANCE WHERE $conds ";
                if(!$returnSQL)	$sql .= "ORDER BY $order";
                return ($returnSQL ? $sql : $this->returnArray($sql,5));
       }
       function returnDetailedRecordByDate2($studentid,$start_date,$end_date)
       {
                $fields = "StudentAttendanceID, DATE_FORMAT(AttendanceDate,'%Y-%m-%d'), RecordType, DayType, Reason";
                $conds = "UserID = '$studentid' AND UNIX_TIMESTAMP(AttendanceDate) BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')";
                $order = "AttendanceDate DESC";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_ATTENDANCE WHERE $conds ORDER BY $order";
                return $this->returnArray($sql,5);
       }
       function displayDetailedRecordByYear($studentid,$year)
       {
                global $i_Attendance_Date,$i_Attendance_Type,$i_Attendance_DayType,$i_Attendance_Reason;
                global $i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave;
                global $i_DayTypeArray;
                global $i_no_record_exists_msg;

                $no_msg = $i_no_record_exists_msg;

                $result = $this->returnDetailedRecordByYear($studentid,$year);
		$x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>
        <tr>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Date</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Type</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_DayType</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Reason</td>
        </tr>\n";
              for ($i=0; $i<sizeof($result); $i++)
              {
                   list ($attendDate,$type,$dayType,$reason) = $result[$i];
                   switch ($type)
                   {
                           case 1: $typeStr = $i_Profile_Absent; break;
                           case 2: $typeStr = $i_Profile_Late; break;
                           case 3: $typeStr = $i_Profile_EarlyLeave; break;
                           default: $typeStr = "Error"; break;
                   }
                   $dayStr = $i_DayTypeArray[$dayType];
                   $x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>
          <td class=td_center_middle bgcolor=#E8FBFC>$attendDate</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$typeStr</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$dayStr</td>
          <td class=td_center_middle bgcolor=#E8FBFC>&nbsp;$reason</td>
        </tr>";
              }
              if (sizeof($result)==0)
              {
                  $x .= "<tr><td colspan='4' class=td_center_middle bgcolor=#E8FBFC>".$no_msg."</td></tr>\n";
              }
              $x .= "</table>";
              return $x;

       }

       function displayDetailedRecordByYearWithReasonEditable($studentid,$year)
       {
			global $i_Attendance_Date,$i_Attendance_Type,$i_Attendance_DayType,$i_Attendance_Reason;
			global $i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave;
			global $i_DayTypeArray;
			global $i_no_record_exists_msg;
			global $type,$image_path,$button_edit;
			
			$no_msg = $i_no_record_exists_msg;

			$result = $this->returnDetailedRecordByYear2($studentid,$year);
			
			$x .= "<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>
	        <tr>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Date</td>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Type</td>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_DayType</td>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Reason</td>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$button_edit</td>
	        </tr>\n";
			for ($i=0; $i<sizeof($result); $i++)
			{
				list ($record_id,$attendDate,$type,$dayType,$reason) = $result[$i];
				//$select_default_reason = "<a onMouseMove='overhere()' href=javascript:retrieveReason($type,$record_id)><img src=\"$image_path/icon_alt.gif\" border=0 alt='$button_select'></a>";
				$edit_link = "<a onMouseMove='overhere()' href='view_attendance_edit.php?StudentID=$studentid&Year=$year&Type=$type&RecordID=$record_id'?><img src=\"$image_path/icon_edit.gif\" border=0 alt='$button_select'></a>";
				switch ($type)
				{
				   case 1: $typeStr = $i_Profile_Absent; break;
				   case 2: $typeStr = $i_Profile_Late; break;
				   case 3: $typeStr = $i_Profile_EarlyLeave; break;
				   default: $typeStr = "Error"; break;
				}
				$dayStr = $i_DayTypeArray[$dayType];
				$x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>
				<td class=td_center_middle bgcolor=#E8FBFC>$attendDate</td>
				<td class=td_center_middle bgcolor=#E8FBFC>$typeStr</td>
				<td class=td_center_middle bgcolor=#E8FBFC>$dayStr</td>
				<td class=td_center_middle bgcolor=#E8FBFC>$reason</td>
				<td class=td_center_middle bgcolor=#E8FBFC>$edit_link</td>
				<input name=record_id[$i] type=hidden value=$record_id>
				</tr>";
			}
			if (sizeof($result)==0)
			{
			  $x .= "<tr><td colspan='4' class=td_center_middle bgcolor=#E8FBFC>".$no_msg."</td></tr>\n";
			}
			$x .= "</table>";
			return $x;
       }
      
       
       function displayDetailedRecordByDate($studentid,$start,$end)
       {
			global $i_Attendance_Date,$i_Attendance_Type,$i_Attendance_DayType,$i_Attendance_Reason;
			global $i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave;
			global $i_DayTypeArray;
			global $i_no_record_exists_msg;
			global $type,$image_path,$button_edit;
			
			$no_msg = $i_no_record_exists_msg;

			$result = $this->returnDetailedRecordByDate1($studentid,$start,$end);
			$x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>
	        <tr>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Date</td>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Type</td>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_DayType</td>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Reason</td>
	        </tr>\n";
			for ($i=0; $i<sizeof($result); $i++)
			{
				list ($record_id,$attendDate,$type,$dayType,$reason) = $result[$i];
				//$select_default_reason = "<a onMouseMove='overhere()' href=javascript:retrieveReason($type,$record_id)><img src=\"$image_path/icon_alt.gif\" border=0 alt='$button_select'></a>";
				switch ($type)
				{
				   case 1: $typeStr = $i_Profile_Absent; break;
				   case 2: $typeStr = $i_Profile_Late; break;
				   case 3: $typeStr = $i_Profile_EarlyLeave; break;
				   default: $typeStr = "Error"; break;
				}
				$dayStr = $i_DayTypeArray[$dayType];
				$x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>
				<td class=td_center_middle bgcolor=#E8FBFC>$attendDate</td>
				<td class=td_center_middle bgcolor=#E8FBFC>$typeStr</td>
				<td class=td_center_middle bgcolor=#E8FBFC>$dayStr</td>
				<td class=td_center_middle bgcolor=#E8FBFC>$reason</td>
				</tr>";
			}
			if (sizeof($result)==0)
			{
			  $x .= "<tr><td colspan='4' class=td_center_middle bgcolor=#E8FBFC>".$no_msg."</td></tr>\n";
			}
			$x .= "</table>";
			return $x;
       }
       
       function displayDetailedRecordByDateWithReasonEditable($studentid,$start,$end)
       {
			global $i_Attendance_Date,$i_Attendance_Type,$i_Attendance_DayType,$i_Attendance_Reason;
			global $i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave;
			global $i_DayTypeArray;
			global $i_no_record_exists_msg;
			global $type,$image_path,$button_edit;
			
			$no_msg = $i_no_record_exists_msg;

			$result = $this->returnDetailedRecordByDate2($studentid,$start,$end);
			$x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>
	        <tr>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Date</td>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Type</td>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_DayType</td>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Reason</td>
	          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$button_edit</td>
	        </tr>\n";
			for ($i=0; $i<sizeof($result); $i++)
			{
				list ($record_id,$attendDate,$type,$dayType,$reason) = $result[$i];
				//$select_default_reason = "<a onMouseMove='overhere()' href=javascript:retrieveReason($type,$record_id)><img src=\"$image_path/icon_alt.gif\" border=0 alt='$button_select'></a>";
				$edit_link = "<a onMouseMove='overhere()' href='view_attendance_edit.php?StudentID=$studentid&start_date=$start&end_date=$end&Type=$type&RecordID=$record_id'?><img src=\"$image_path/icon_edit.gif\" border=0 alt='$button_select'></a>";
				switch ($type)
				{
				   case 1: $typeStr = $i_Profile_Absent; break;
				   case 2: $typeStr = $i_Profile_Late; break;
				   case 3: $typeStr = $i_Profile_EarlyLeave; break;
				   default: $typeStr = "Error"; break;
				}
				$dayStr = $i_DayTypeArray[$dayType];
				$x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>
				<td class=td_center_middle bgcolor=#E8FBFC>$attendDate</td>
				<td class=td_center_middle bgcolor=#E8FBFC>$typeStr</td>
				<td class=td_center_middle bgcolor=#E8FBFC>$dayStr</td>
				<td class=td_center_middle bgcolor=#E8FBFC>$reason</td>
				<td class=td_center_middle bgcolor=#E8FBFC>$edit_link</td>
				<input name=record_id[$i] type=hidden value=$record_id>
				</tr>";
			}
			if (sizeof($result)==0)
			{
			  $x .= "<tr><td colspan='5' class=td_center_middle bgcolor=#E8FBFC>".$no_msg."</td></tr>\n";
			}
			$x .= "</table>";
			return $x;
       }
       
       function returnYears($studentid="")
       {
                if ($studentid != "")
                {
                    $conds = " and UserID = '$studentid' ";
                }
                $sql = "SELECT DISTINCT Year FROM PROFILE_STUDENT_ATTENDANCE where Year !='' $conds ORDER BY Year DESC";
                return $this->returnVector($sql);
       }

       function returnStudentRecordByYear($studentid,$year,$sem="")
       {
				global $intranet_root, $intranet_session_language;
				
				include("$intranet_root/includes/libstudentprofile.php");
                $lstudentprofile = new libstudentprofile();

                # Grab information in DB
                $count_sql = ($lstudentprofile->attendance_count_method == 1) ? "IF(DayType=1, COUNT(StudentAttendanceID), COUNT(StudentAttendanceID)*0.5)" : "COUNT(StudentAttendanceID) ";
                $conds = ($year != ""? " AND Year = '$year'":"");
                $conds .= ($sem != ""? " AND Semester = '$sem'":"");
                $sql = "SELECT Year, $count_sql FROM PROFILE_STUDENT_ATTENDANCE
                        WHERE UserID = '$studentid' AND RecordType = 1 $conds and Year!=''
                        GROUP BY Year, DayType";
                $data[] = $this->returnArray($sql,2);

				$sql = "SELECT Year, COUNT(StudentAttendanceID) FROM PROFILE_STUDENT_ATTENDANCE
                        WHERE UserID = '$studentid' AND RecordType = 2 $conds and Year!=''
                        GROUP BY Year, DayType";
                $data[] = $this->returnArray($sql,2);
 
                $sql = "SELECT Year, COUNT(StudentAttendanceID) FROM PROFILE_STUDENT_ATTENDANCE
                        WHERE UserID = '$studentid' AND RecordType = 3 $conds and Year!=''
                        GROUP BY Year, DayType";
                $data[] = $this->returnArray($sql,2);

                # Build the whole array
                if ($year == "")
                {
	                /*
					$years = array();
                    $AcademicYearInfo = GetAllAcademicYearInfo();
                    for($i=0;$i<sizeof($AcademicYearInfo);$i++)
						$years[] = $AcademicYearInfo[$i]["YearName".(strtoupper($intranet_session_language))];
					*/
						
	                ## $all_disintct_years, refer to /admin/academic/studentview.php
  	                global $all_distinct_years;
	                if(isset($all_distinct_years) && sizeof($all_distinct_years)>0)
	                	$years = $all_distinct_years;
                    else 
                    	$years = $this->returnYears($studentid);
                }
                else
                {
                    $years = array($year);
                }

//                 if(is_array($years) && $this->YearDescending)
//                   rsort($years);    # Eric Yip : sort years
                
                $result = array();
                for ($i=0; $i<sizeof($years); $i++)
                {
                     $result[$i][0] = $years[$i];
                }
                
                for ($i=0; $i<3; $i++)
                {
                     for ($j=0; $j<sizeof($years); $j++)
                     {
                          //$pos = -1;
                          for ($k=0; $k<sizeof($data[$i]); $k++)
                          {
                               if ($data[$i][$k][0] == $years[$j])
                               {
                                       $result[$j][$i+1] += $data[$i][$k][1];
                                   //$pos = $k;
                                   //break;
                               }
                          }
                          $result[$j][$i+1] += 0;
                          /*
                          if ($pos == -1)
                          {
                              $count = 0;
                          }
                          else
                          {
                              $count = $data[$i][$k][1];
                          }
                          */
                          #$result[$i+1][$j] = $count;
                          //$result[$j][$i+1] = $count;
                     }
                }
                return $result;

       }


       function displayStudentRecordByYearAdmin($studentid,$year="",$sem="", $previewMode=false)
       {
                global $i_no_record_exists_msg;
                $result = $this->returnStudentRecordByYear($studentid,$year,$sem);
                $x = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
                if (sizeof($result)==0)
                {
                    return "$x<tr><td>$i_no_record_exists_msg</td></tr></table>\n";
                }
                global $i_Attendance_Year,$i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave;
                $title_array = array($i_Attendance_Year,$i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave);
                $size = sizeof($result[0]) + 1;
                $width = 100/$size;
                # ================== Changes from here
                $x .= "<tr>\n";
                for ($i=0; $i<sizeof($title_array); $i++)
                {
                     $x .= "<td class=tableTitle_new>".$title_array[$i]."</td>";
                }
                $x .= "</tr>\n";
                $col = sizeof($result[0]);
                $row = sizeof($result);
                for ($i=0; $i<sizeof($result); $i++)
                {
                     $x .= "<tr>\n";
                     for ($j=0; $j<sizeof($result[$i]); $j++)
                     {
                          $data = $result[$i][$j];
                          
													# Eric Yip : hide javascript link for print preview
                          if ($j == 0 && !$previewMode)
                          {
                               $data = "<a href=\"javascript:viewAttendByYear($studentid,'$data')\">$data</a>";
                          }
                          $x .= "<td>$data</td>";
                     }
                     $x .= "</tr>\n";
                }
                $x .= "</table>\n";
/*
                for ($i=0; $i<sizeof($title_array); $i++)
                {
                     $x .= "<tr><td>".$title_array[$i]."</td>";
                     for ($j=0; $j<sizeof($result[$i]); $j++)
                     {
                          $data = $result[$i][$j];
                          if ($i == 0)
                          {
                              $data = "<a href=javascript:viewAttendByYear($studentid,$data)>$data</a>";
                          }
                          $x .= "<td width=$width%>$data</td>";
                     }
                     $x .= "</tr>\n";
                }
                $x .= "</table>\n";
*/
                return $x;

       }

       function displayStudentRecordByYear($studentid,$year="",$student_class=array())
       {
                global $i_no_record_exists_msg;
                global $image_path,$intranet_session_language;
                global $i_Attendance_Year,$i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave;
                $result = $this->returnStudentRecordByYear($studentid,$year);
                
                $x = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>\n";
                
                global $i_Attendance_Year,$i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave;
                $title_array = array($i_Attendance_Year,$i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave);

                $size = sizeof($result[0]) + 1;
                $width = 100/$size;
                # ================== Changes from here
                $x .= "<tr>\n";
                for ($i=0; $i<sizeof($title_array); $i++)
                {
                     $x .= "<td class='tablebluetop tabletopnolink' align='left'>".$title_array[$i]."</td>";
                }
                $x .= "</tr>\n";
                $col = sizeof($result[0]);
                
                $class = array();
                for($i = 0; $i < sizeof($student_class); $i++){
                    $class[] = $student_class[$i][0];
                }
                
                $in_class = array();
                # get record is in class
                for($i = 0; $i < sizeof($result); $i++){
                    if(in_array($result[$i][0], $class)){
                        $in_class[] = $result[$i];
                    }
                }
                
                /*
                //$row = sizeof($result);
                # check includes record or not
                if(sizeof($result)==1)
                {
	                for ($i=1; $i<$col; $i++)
	                {
		             	$data_no += $result[0][$i];
	                }
            	}
            	if($data_no)
				*/
            	if(sizeof($in_class))
            	{
            	    for ($i=0; $i<sizeof($in_class); $i++)
	                {
	                    if(in_array($in_class[$i][0], $class)){
    	                   $x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>";
    	                   for ($j=0; $j<sizeof($in_class[$i]); $j++)
        	               {
        	                   $data = $in_class[$i][$j];
        	                   if ($j == 0)
        	                   {
        	                     $data = "<a class=\"tablebluelink\" href=\"javascript:viewAttendByYear($studentid,'$data')\">$data</a>";
        	                   }
                               $x .= "<td class='tabletext'>$data</td>";
        	               }
        	               $x .= "</tr>\n";
    	               }
	                }
                }
                else		
                //if (sizeof($result)==0)
                {
                    $x .= "<tr><td class='tabletext' colspan='".sizeof($title_array)."' align='center'>$i_no_record_exists_msg</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;

       }

       // ++++++ for archive student ++++++ \\
       function displayArchiveStudentRecordByYearAdmin($studentid,$year="",$sem="")
       {
                global $i_no_record_exists_msg;
                $result = $this->returnArchiveStudentRecordByYear($studentid,$year,$sem);
                $x = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
                if (sizeof($result)==0)
                {
                    return "$x<tr><td>$i_no_record_exists_msg</td></tr></table>\n";
                }
                global $i_Attendance_Year,$i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave;
                $title_array = array($i_Attendance_Year,$i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave);
                $size = sizeof($result[0]) + 1;
                $width = 100/$size;
                # ================== Changes from here
                $x .= "<tr>\n";
                for ($i=0; $i<sizeof($title_array); $i++)
                {
                     $x .= "<td class=tableTitle_new>".$title_array[$i]."</td>";
                }
                $x .= "</tr>\n";
                $col = sizeof($result[0]);
                $row = sizeof($result);
                for ($i=0; $i<sizeof($result); $i++)
                {
                     $x .= "<tr>\n";
                     for ($j=0; $j<sizeof($result[$i]); $j++)
                     {
                          $data = $result[$i][$j];
                          if ($j == 0)
                          {
                              $data = "<a href=\"javascript:viewAttendByYear($studentid,'$data')\">$data</a>";
                          }
                          $x .= "<td>$data</td>";
                     }
                     $x .= "</tr>\n";
                }
                $x .= "</table>\n";
                return $x;

       }
        function returnArchiveStudentRecordByYear($studentid,$year,$sem="")
       { 
                               global $intranet_root;
                               include("$intranet_root/includes/libstudentprofile.php");
                               $lstudentprofile = new libstudentprofile();

                # Grab information in DB
                $count_sql = ($lstudentprofile->attendance_count_method == 1) ? "IF(DayType='WD', COUNT(RecordID), COUNT(RecordID)*0.5) " : "COUNT(RecordID) ";
                $conds = ($year != ""? " AND Year = '$year'":"");
                $conds .= ($sem != ""? " AND Semester = '$sem'":"");
                $sql = "SELECT Year, $count_sql FROM PROFILE_ARCHIVE_ATTENDANCE
                        WHERE UserID = '$studentid' AND RecordType = 'ABSENT' $conds
                        GROUP BY Year, DayType";

                $data[] = $this->returnArray($sql,2);
                $sql = "SELECT Year, COUNT(RecordID) FROM PROFILE_ARCHIVE_ATTENDANCE
                        WHERE UserID = '$studentid' AND RecordType = 'LATE' $conds
                        GROUP BY Year, DayType";
                $data[] = $this->returnArray($sql,2);
                $sql = "SELECT Year, COUNT(RecordID) FROM PROFILE_ARCHIVE_ATTENDANCE
                        WHERE UserID = '$studentid' AND RecordType = 'EARLY LEAVE' $conds
                        GROUP BY Year, DayType";
                $data[] = $this->returnArray($sql,2);
                
                # Build the whole array
                if ($year == "")
                {
	                global $all_distinct_years;
	                if($all_distinct_years==""){
                    	$years = $this->returnArchiveYears($studentid);
                    }else{
                    	$years = $all_distinct_years;
                    }
                }
                else
                {
                    $years = array($year);
                }
                $result = array();
                for ($i=0; $i<sizeof($years); $i++)
                {
                     $result[$i][0] = $years[$i];
                }
                for ($i=0; $i<3; $i++)
                {
                     //$result[$i] = array();

                     for ($j=0; $j<sizeof($years); $j++)
                     {
                          //$pos = -1;
                          for ($k=0; $k<sizeof($data[$i]); $k++)
                          {
                               if ($data[$i][$k][0] == $years[$j])
                               {
                                       $result[$j][$i+1] += $data[$i][$k][1];
                                   //$pos = $k;
                                   //break;
                               }
                          }
                          $result[$j][$i+1] += 0;
                          /*
                          if ($pos == -1)
                          {
                              $count = 0;
                          }
                          else
                          {
                              $count = $data[$i][$k][1];
                          }
                          #$result[$i+1][$j] = $count;
                          $result[$j][$i+1] = $count;
                          */
                     }
                }
                return $result;

       }
       function returnArchiveYears($studentid)
       {
                $sql = "SELECT DISTINCT Year FROM PROFILE_ARCHIVE_ATTENDANCE WHERE UserID = '$studentid' ORDER BY Year DESC";
                return $this->returnVector($sql);
       }
       function displayArchiveDetailedRecordByYear($studentid,$year)
       {
                global $i_Attendance_Date,$i_Attendance_Type,$i_Attendance_DayType,$i_Attendance_Reason;
                global $i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave;
                global $i_DayTypeArray;
                global $i_no_record_exists_msg;

                $no_msg = $i_no_record_exists_msg;

                $result = $this->returnArchiveDetailedRecordByYear($studentid,$year);
              $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>
        <tr>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Date</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Type</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_DayType</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Attendance_Reason</td>
        </tr>\n";
              for ($i=0; $i<sizeof($result); $i++)
              {
                   list ($attendDate,$type,$dayType,$reason) = $result[$i];

                   switch ($type)
                   {
                           case "ABSENT": $typeStr = $i_Profile_Absent; break;
                           case "LATE": $typeStr = $i_Profile_Late; break;
                           case "EARLY LEAVE": $typeStr = $i_Profile_EarlyLeave; break;
                           default: $typeStr = "Error"; break;
                   }

                   switch ($dayType)
                   {
                                      case "WD":        $dayStr = $i_DayTypeArray[1]; break;
                                      case "AM":        $dayStr = $i_DayTypeArray[2]; break;
                                      case "PM":        $dayStr = $i_DayTypeArray[3]; break;
                           default: $dayStr = "Error"; break;
                   }

                   $x .= "<tr>
          <td class=td_center_middle bgcolor=#E8FBFC>$attendDate</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$typeStr</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$dayStr</td>
          <td class=td_center_middle bgcolor=#E8FBFC>&nbsp;$reason</td>
        </tr>";
              }
              if (sizeof($result)==0)
              {
                  $x .= "<tr><td colspan=4 class=td_center_middle bgcolor=#E8FBFC>".$no_msg."</td></tr>\n";
              }
              $x .= "</table>";
              return $x;

       }
       function returnArchiveDetailedRecordByYear($studentid, $year)
       {
                $fields = "DATE_FORMAT(RecordDate,'%Y-%m-%d'), RecordType, DayType, Reason";
                $conds = "UserID = '".$studentid."' AND Year = '$year'";
                $order = "RecordDate DESC";
                $sql = "SELECT $fields FROM PROFILE_ARCHIVE_ATTENDANCE WHERE $conds ORDER BY $order";
                return $this->returnArray($sql,4);
       }
       // +++++ end of archive student +++++ \\

 }


} // End of directives
?>
