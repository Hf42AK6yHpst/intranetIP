<?php
// editing by Paul

/* 2015-12-22 Paul [ip.2.5.7.1.1]
 * replace $_FILES in function Upload_EPUB_Book_TEST, Upload_EPUB_Book_New, UPLOAD_EPUB_Book by $filedata to resolve error " cannot re-assign auto-global variable _FILES "
 * 
 * 2012-09-12 CharlesMa
 * add column for function of forcing to one page mode & one page width---- Upload_EPUB_Book
 * 
 * 
 * 2012-05-24 Carlos
 * add testing code for learning object, not finish, do not deploy this file
 * 
 * 2012-02-27 Yuen
 * modified Upload_EPUB_Book to follow BookID of INTRANET_ELIB_BOOK
 * 
 */
 
include_once("libdb.php");
include_once("libxml.php");


function cmp($a, $b)
{
	$a_file = explode("/",$a);
	$b_file = explode("/",$b);
	$a_name = explode(".txt",$a_file[count($a_file)-1]);
	$b_name = explode(".txt",$b_file[count($b_file)-1]);
	$a_index = (int)$a_name[0];
	$b_index = (int)$b_name[0];

    return ($a_index < $b_index) ? -1 : 1;
}

class libebookreader extends libdb
{
	var $libxml= null; // LibXML reference
	var $container_xml_array = array(); // raw container.xml xml data
	var $content_opf_xml_array = array(); // raw content.opf xml data
	var $toc_xml_array = array(); // raw toc.ncx xml data
	var $opf_items = array(); // {id,href,media-type}
	var $opf_spine = array(); // a list of idref
	var $metadata_array = array(); // book info: dc:title, dc:publisher, dc:rights, dc:date, dc:language, dc:subject, dc:creator etc.
	var $table_of_content = array(); // contains (1)[meta] book info, (2) [docTitle] book title (3) [navPoint] dir link list
	var $ebook_doc_path; // is which the folder where html files are located 
	var $ebook_subfolder_path; // is the middle subfolder(s) where container.xml specified in full-path
	var $BookID; // temp BookID
	var $CoverImagePath; // /file/eBook/id/..subfolder../*cover*.*
	var $CoverImageName; 
	
	var $xml_attr_subfix = '_attr';
	var $ENCRYPT_KEY = '6ed954b0';
	var $EBOOK_FILE_PATH = '/file/eBook';
	
	function libebookreader()
	{
		parent::libdb();
	}
	
	function Get_LibXML()
	{
		if(!isset($this->libxml) || $this->libxml === null){
			$this->libxml = new LibXML();
		}
		return $this->libxml;
	}
	
	/*
	 * Parse /META-INF/container.xml
	 * 		 /OEBPS/content.opf
	 * 		 /OEBPS/toc.ncx
	 * 		 where /OEBPS is not a fix subfolder, can be any path
	 */
	function Parse_EPUB_XML($epub_root_path)
	{
		$libxml = $this->Get_LibXML();
		
		if(!file_exists($epub_root_path)){
			return false;
		}
		if($epub_root_path[strlen($epub_root_path)-1]!='/') $epub_root_path .= '/';
		// META-INF xml
		$container_xml_path = $epub_root_path."/META-INF/container.xml";
		$container_xml = file_get_contents($container_xml_path);
		$this->container_xml_array = $libxml->XML_ToArray($container_xml);
		
		if(!empty($this->container_xml_array['container']['rootfiles']['rootfile'.$this->xml_attr_subfix]['full-path']))
		{
			// find any sub-folder for locating epub docoument files
			$last_slash_pos = strrpos($this->container_xml_array['container']['rootfiles']['rootfile'.$this->xml_attr_subfix]['full-path'],'/');
			$tmp_subfolder = trim(substr($this->container_xml_array['container']['rootfiles']['rootfile'.$this->xml_attr_subfix]['full-path'],0,$last_slash_pos));
			$subfolder = $tmp_subfolder==""? "": $tmp_subfolder."/";
			$this->ebook_subfolder_path = $subfolder;
			$ebook_document_path = $epub_root_path.$subfolder;
			$this->ebook_doc_path = $ebook_document_path;
			// OPF
			$content_opf_path = $epub_root_path.$this->container_xml_array['container']['rootfiles']['rootfile'.$this->xml_attr_subfix]['full-path'];
		
			if(file_exists($content_opf_path))
			{
				$content_opf_xml = file_get_contents($content_opf_path);
				$this->content_opf_xml_array = $libxml->XML_ToArray($content_opf_xml);
				
				$metadata = $this->content_opf_xml_array['package']['metadata'];
				$_attr_len = strlen($this->xml_attr_subfix);
				foreach($metadata as $key => $val){
					$pos = strrpos($key, $this->xml_attr_subfix);
					if($pos !==FALSE && ($pos + $_attr_len == strlen($key))){
						// it is a xml attribute, skip 
					}elseif(!empty($val)){
						// it is a tag value
						$this->metadata_array[$key] = $val;
					}
				}
				
				$toc_path = '';
				if(count($this->content_opf_xml_array['package']['manifest'])==2 && !isset($this->content_opf_xml_array['package']['manifest']['item'][0]))
				{
					// only one item
					$items = array();
					$items[0] = $this->content_opf_xml_array['package']['manifest']['item'];
					$items['0'.$this->xml_attr_subfix] = $this->content_opf_xml_array['package']['manifest']['item'.$this->xml_attr_subfix];
					$item_size = 1;
				}else{
					$items = $this->content_opf_xml_array['package']['manifest']['item'];
					$items_size = count($items)/2;
				}
				
				// find toc.ncx path and other opf items
				for($i=0;$i<$items_size;$i++){
					$this->opf_items[$items[$i.$this->xml_attr_subfix]['id']] = array('id' => $items[$i.$this->xml_attr_subfix]['id'], 
											  'href' => $items[$i.$this->xml_attr_subfix]['href'],
											  'media-type' => $items[$i.$this->xml_attr_subfix]['media-type']);
					// find toc file path
					if($items[$i.$this->xml_attr_subfix]['media-type']=='application/x-dtbncx+xml'){
						$toc_path = $items[$i.$this->xml_attr_subfix]['href'];
					}
					// find cover image path
					if(substr($items[$i.$this->xml_attr_subfix]['media-type'],0,6)=='image/' && preg_match('/.*cover.*/i',$items[$i.$this->xml_attr_subfix]['id']) ){
						$this->CoverImagePath = substr($this->ebook_doc_path, stripos($this->ebook_doc_path,$this->EBOOK_FILE_PATH)).$items[$i.$this->xml_attr_subfix]['href'];
						$this->CoverImageName = $items[$i.$this->xml_attr_subfix]['href'];
					}
				}
				
				if(count($this->content_opf_xml_array['package']['spine']) == 2){ // only 1 itemref and itemref_attr, put to 0
					$spine_itemref = array();
					$spine_itemref[0] = $this->content_opf_xml_array['package']['spine']['itemref'];
					$spine_itemref['0'.$this->xml_attr_subfix] = $this->content_opf_xml_array['package']['spine']['itemref'.$this->xml_attr_subfix];
					$itemref_size = 1;
				}else{
					$spine_itemref = $this->content_opf_xml_array['package']['spine']['itemref'];
					$itemref_size = count($spine_itemref)/2;
				}
				
				for($i=0;$i<$itemref_size;$i++){
					if(isset($spine_itemref[$i.$this->xml_attr_subfix]) && sizeof($spine_itemref[$i.$this->xml_attr_subfix])>0){
						$this->opf_spine[] = array('idref' => $spine_itemref[$i.$this->xml_attr_subfix]['idref']);
					}
				}
				
				// TOC - table of content
				if($toc_path != '')
				{
					$toc_full_path = $ebook_document_path.$toc_path;
					if(file_exists($toc_full_path)){
						 $toc_xml = file_get_contents($toc_full_path);
						 $this->toc_xml_array = $libxml->XML_ToArray($toc_xml);
					}
					
					if(count($this->toc_xml_array)>0)
					{
						$meta_array = $this->toc_xml_array['ncx']['head']['meta'];
						$meta_array_size = count($meta_array)/2;
						$this->table_of_content['meta'] = array();
						for($i=0;$i<$meta_array_size;$i++){
							$this->table_of_content['meta'][$meta_array[$i.$this->xml_attr_subfix]['name']] = $meta_array[$i.$this->xml_attr_subfix]['content'];
						}
						$this->table_of_content['docTitle'] = $this->toc_xml_array['ncx']['docTitle']['text'];
						
						$navmap_array = $this->toc_xml_array['ncx']['navMap'];
						//debug_pr($this->toc_xml_array);
						if(count($navmap_array)>0)
						{
							if(isset($navmap_array['navPoint'])){
								$nav_points = $this->parseNavPoint($navmap_array['navPoint']);
								$this->table_of_content['navPoint'] = $nav_points;
							}
						}
						//debug_pr($this->table_of_content);
					}
				}
			}
		}
		
		return true;
	}
	
	// parse navPoint recursively
	function parseNavPoint($node)
	{
		/*
		 * entry->navLabel // optional attribute
		 * entry->navContent // optional attribute
		 * entry->navPointList  //numerical index based array list 
		 */
		$entry = array();
		// a list of navPoint
		if(isset($node[0]) && count($node)>0)
		{
			if(isset($node['navLabel'])){
				$entry['navLabel'] = $node['navLabel']['text'];
			}
			if(isset($node['content'])){
				$entry['content'] = $node['content'.$this->xml_attr_subfix]['src'];;
			}
			//$entry['navPointList'] = array();
			//$node_size = count($node)/2;
			$node_size = 0;
			while(isset($node[$node_size])){
				$node_size++;
			}
			for($i=0;$i<$node_size;$i++){
			 	$entry[] = $this->parseNavPoint($node[$i]);
			}
		}else{ // single navPoint
			//$entry['navLabel'] = $node['navLabel']['text'];
			//$entry['content'] = $node['content'.$this->xml_attr_subfix]['src'];
			if(isset($node['navLabel'])){
				$entry['navLabel'] = $node['navLabel']['text'];
			}
			if(isset($node['content'])){
				$entry['content'] = $node['content'.$this->xml_attr_subfix]['src'];;
			}
			if(isset($node['navPoint']) && count($node['navPoint'])>0)
				$entry['navPointList'] = $this->parseNavPoint($node['navPoint']);
			else
				$entry['navPointList'] = array();
		}
		
		return $entry;
	}
	
	function PrintTableOfContent()
	{
		$x = '';
		$title = $this->table_of_content['docTitle'];
		$navPoint = $this->table_of_content['navPoint'];
		
		$indent = 0;
		$indent_size = 4;
		if(trim($title)!=''){
			$x .= $title."\n<br>";
			$indent++;
		}
		if(isset($navPoint['navLabel']))
		{
			$x .= $this->PrintTableOfContentEntry($navPoint, $indent);
		}
		return $x;
	}
	
	// print one line table of content recursively
	function PrintTableOfContentEntry($node, $level=0, $indent_size=4)
	{
		$x = '';
		$indent = $level;
		if(isset($node['navLabel']))
		{
			$indent++;
			$x .= str_repeat('&nbsp;', $indent * $indent_size);
			$x .= '<a href="'.$node['content'].'" >'.$node['navLabel'].'</a>';
			$x .= "\n<br>";
			
			for($i=0;$i<count($node['navPointList']);$i++){
				$x .= $this->PrintTableOfContentEntry($node['navPointList'][$i], $indent, $indent_size);
			}
		}
		return $x;
	}
	
	// Extract usefull html elements
	function GetHtmlContent($html)
	{
		$x = '';
		// extract head inner html
		$head_start = stripos($html,'<head',0);
		$head_start_close = stripos($html,'>',$head_start);
		$head_end = stripos($html,'</head>',0);
		$inner_head = substr($html, $head_start_close+1, $head_end -1 - $head_start_close);
		// extract <link> tags
		$tmp_inner_head = $inner_head;
		while( ($link_start = stripos($tmp_inner_head, '<link', 0)) !== FALSE )
		{
			$link_end = stripos($tmp_inner_head,'>',$link_start);
			$link_tag = substr($tmp_inner_head, $link_start, $link_end + 1 - $link_start);
			$tmp_inner_head = substr($tmp_inner_head, $link_end + 1, strlen($tmp_inner_head) - $link_end);
			$x .= $link_tag;
		}
		// extract <style></style> tags 
		$tmp_inner_head = $inner_head;
		while( ($tag_start = stripos($tmp_inner_head, '<style', 0)) !== FALSE )
		{
			$tag_end = stripos($tmp_inner_head,'</style>',$tag_start) + 8;
			$tag_tag = substr($tmp_inner_head, $tag_start, $tag_end + 1 - $tag_start);
			$tmp_inner_head = substr($tmp_inner_head, $tag_end + 1, strlen($tmp_inner_head) - $tag_end);
			$x .= $tag_tag;
		}
		// extract <script></script> tags
		$tmp_inner_head = $inner_head;
		while( ($tag_start = stripos($tmp_inner_head, '<script', 0)) !== FALSE )
		{
			$tag_end = stripos($tmp_inner_head,'</script>',$tag_start) + 9;
			$tag_tag = substr($tmp_inner_head, $tag_start, $tag_end + 1 - $tag_start);
			$tmp_inner_head = substr($tmp_inner_head, $tag_end + 1, strlen($tmp_inner_head) - $tag_end);
			$x .= $tag_tag;
		}
		// extract body inner html
		$body_start = stripos($html,'<body',0);
		$body_start_close = stripos($html,'>',$body_start);
		$body_end = stripos($html,'</body',0);
		$inner_body = substr($html, $body_start_close+1, $body_end -1 - $body_start_close);
		//$x .= $inner_body;
		$x = $inner_body.$x;
		
		return $x;
	}
	/*
	function Get_LearningObject_Tag($html)
	{
		//$start_tag = '<bl:learningobject';
		//$end_tag = '</bl:learningobject>';
		$start_tag = '<div class="eClass_Learning_Object"';
		$end_tag = '</div>';
		
		$tmp_html = $html;
		$LearningObjTags = array();
		while( ($tag_start = stripos($tmp_html, $start_tag, 0)) !== FALSE )
		{
			$tag_startend = stripos($tmp_html, '>', $tag_start);
			$tag_end = stripos($tmp_html, $end_tag, $tag_start) + strlen($end_tag);
			
			$tag = substr($tmp_html, $tag_start, $tag_end + 1 - $tag_start);
			$attributes = substr($tmp_html, $tag_start + strlen($start_tag), $tag_startend - $tag_start - strlen($start_tag));
			$attr_arr = explode(" ",$attributes);
			$attr_pairs = array();
			for($i=0;$i<count($attr_arr);$i++){
				if(trim($attr_arr[$i])!='' && strpos($attr_arr[$i],'=')!==FALSE){
					$key_val = explode("=",$attr_arr[$i]);
					$attr_pairs[$key_val[0]] = str_replace(array('"',"'"),'',$key_val[1]);
				}
			}
			
			$tmp_html = substr($tmp_html, $tag_end + 1);
			$LearningObjTags[] = array($tag,$attr_pairs);
		}
		
		return $LearningObjTags;
	}
	*/
	function Get_LearningObject_Tag($html)
	{
		$start_tag = '<div class="eClass_Learning_Object"';
		$end_tag = '</div>';
		
		$tmp_html = $html;
		$LearningObjTags = array();
		while( ($tag_start = stripos($tmp_html, $start_tag, 0)) !== FALSE )
		{
			$tag_startend = stripos($tmp_html, '>', $tag_start);
			$tag_end = stripos($tmp_html, $end_tag, $tag_start) + strlen($end_tag);
			
			$tag = substr($tmp_html, $tag_start, $tag_end + 1 - $tag_start);
			$tag_content = trim(substr($tmp_html, $tag_startend+1, $tag_end - strlen($end_tag) - $tag_startend - 1));
		//	$attributes = strtolower(substr($tmp_html, $tag_start + strlen($start_tag), $tag_startend - $tag_start - strlen($start_tag)));
			$attributes = substr($tmp_html, $tag_start + strlen($start_tag), $tag_startend - $tag_start - strlen($start_tag));
			//$attributes = preg_replace('/\s*=\s*/',"=",$attributes);
			//$attr_arr = explode(" ",$attributes);
			$pattern = '/(\\w+)\s*=\\s*("[^"]*"|\'[^\']*\'|[^"\'\\s>]*)/';
			$matches = array();
			preg_match_all($pattern, $attributes, $matches, PREG_SET_ORDER);
			$attr_arr = array();
			for($i=0;$i<count($matches);$i++){
				$attr_arr[] = str_replace(' ','',$matches[$i][0]);
			}
			$attr_pairs = array();
			$attr_pairs['src'] = $tag_content;
			for($i=0;$i<count($attr_arr);$i++){
				if(trim($attr_arr[$i])!='' && strpos($attr_arr[$i],'=')!==FALSE){
					$key_val = explode("=",$attr_arr[$i]);
					if($key_val[0]=='title'){
						// e.g. title="type:flash;width:480;height:320" 
						$tmp_style = str_replace(array('"',"'"),'',$key_val[1]);
						$tmp_style_arr = explode(";",$tmp_style);
						for($j=0;$j<count($tmp_style_arr);$j++){
							if($tmp_style_arr[$j]!='' && strpos($tmp_style_arr[$j],':')!==FALSE){
								$tmp_style_key_val = explode(":",$tmp_style_arr[$j]);
								$tmp_key = trim($tmp_style_key_val[0]);
								$tmp_val = trim($tmp_style_key_val[1]);
								$attr_pairs[$tmp_key] = str_replace(array('"',"'"),'',$tmp_val);
							}
						}
					}else{
						$attr_pairs[$key_val[0]] = str_replace(array('"',"'"),'',$key_val[1]);
					}
				}
			}
			
			$tmp_html = substr($tmp_html, $tag_end + 1);
			$LearningObjTags[] = array($tag,$attr_pairs);
		}
		
		return $LearningObjTags;
	}
	
	function Get_Flash_Tag($swfPath,$id,$params=array(),$width='457',$height='320')
	{
		$param_tags = '';
		for($i=0;$i<count($params);$i++){
			$param_tags .= '<param name="'.$params[$i][0].'" value="'.htmlspecialchars($params[$i][1],ENT_QUOTES).'" />'."\n";
		}
		if($width==''){
			$width = '457';
		}
		if($height==''){
			$height = '320';
		}
		$x = '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="'.$width.'" height="'.$height.'" id="'.$id.'" align="middle">'."\n";
		$x.= '<param name="movie" value="'.$swfPath.'"/>'."\n";
		$x.= $param_tags;
		$x.= '  <!--[if !IE]>-->
			    <object type="application/x-shockwave-flash" data="'.$swfPath.'" width="'.$width.'" height="'.$height.'">'."\n";
		$x.= '<param name="movie" value="'.$swfPath.'"/>'."\n";
		$x.= $param_tags;
		$x.= '  <!--<![endif]-->
			        <a href="http://www.adobe.com/go/getflash">
			            <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/>
			        </a>
			    <!--[if !IE]>-->
			    </object>
			    <!--<![endif]-->
			</object>'."\n";
		
		return $x;
	}
	
	function Get_iFrame_Tag($src,$id,$width='457',$height='320')
	{
		if($width==''){
			$width = '457';
		}
		if($height==''){
			$height = '320';
		}
		$x = '<iframe id="'.$id.'" width="'.$width.'" height="'.$height.'" src="'.$src.'" frameborder="0"></iframe>';
		return $x;
	}
	
	function Get_Thickbox_Link($id,$text,$title='',$width="420",$height="300",$more_class='',$onclick_handler='')
	{
		if(trim($more_class) != ''){
			$more_class = ' '.$more_class;
		}
		if($title != ''){
			$title_attr = 'title="'.htmlspecialchars($title,ENT_QUOTES).'"';
		}
		if($height==''){
			$height = '420';
		}
		if($width==''){
			$width = '300';
		}
		if($onclick_handler!=''){
			$onclick_event = ' onclick="'.$onclick_handler.'" ';
		}
		$x = '<a class="thickbox'.$more_class.'" id="'.$id.'" href="#TB_inline?height='.$height.'&width='.$width.'&inlineId=FakeLayer" '.$onclick_event.' '.$title_attr.'>';
		$x.= "<p style='color:blue' class='bodytext paragraph'>";
			$x.= $text;	
			$x.= "</p>";	
		$x.= '</a>';
		
		return $x;
	}
	
	function Get_Audio_Control($src, $width='', $height='')
	{
		global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang, $userBrowser;
		
		$version = explode('.',$userBrowser->version);
		if(($userBrowser->browsertype == 'Firefox' && $version[0] <= 8) || $userBrowser->browsertype == 'MSIE'){
			$x = $this->Get_Audio_Object_Control($src,$width,$height);
		}else{
			$width_css = $width != '' ? 'width:'.$width.'px;' : '';
			$height_css = $height != '' ? 'height:'.$height.'px;' : '';
			$style = ($width_css!='' || $height_css!='') ? 'style="'.$width_css.$height_css.'"': '';
			$x = '<audio id="audio_control" controls="controls" autoplay="autoplay" '.$style.'>
					<source src="'.$src.'" type="audio/mp3" />
					Your browser does not support the audio tag.
				  </audio>';
		}
		return $x;
	}
	
	function Get_Audio_Object_Control($src, $width='', $height='')
	{
		global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;
		
		$width_attr = $width != '' ? 'width="'.$width.'px"' : '';
		$height_attr = $height != '' ? 'height="'.$height.'px"':  '';
		$x = '<object '.$width_attr.' '.$height_attr.' data="'.$src.'">';
		$x.= '<param name="url" value="'.$src.'">';
		$x.= '<param name="filename" value="'.$src.'">';
		$x.= '<param name="autoplay" value="true">';
		$x.= '<param name="autoStart" value="1">';
		$x.= '<param name="uiMode" value="full" />';
		$x.= '<param name="autosize" value="1">';
		$x.= '<param name="playcount" value="1">';
		$x.= '<embed type="application/x-mplayer2" src="'.$src.'" width="100%" height="100%" autostart="true" showcontrols="true" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/"></embed>';
		$x.= '</object>';
		
		return $x;
	}
	
	/*
	function Generate_LearningObject($html)
	{
		$learningobj_tags = $this->Get_LearningObject_Tag($html);
		$x = $html;
		$flash_param_names = array();
		for($i=0;$i<count($learningobj_tags);$i++){
			$resxType = strtolower($learningobj_tags[$i][1]['resxType']);
			if($resxType=='flash'){
				$flash_params = array();
				foreach($flash_param_names as $key => $param_name){
					if(isset($learningobj_tags[$i][1][$param_name])){
						$flash_params[] = array($param_name,$learningobj_tags[$i][1][$param_name]);
					}
				}
				
				$flash_tag = $this->Get_Flash_Tag($learningobj_tags[$i][1]['resxPath'],'learningobject'.$learningobj_tags[$i][1]['resxID'],$flash_params,$learningobj_tags[$i][1]['resxWidth'],$learningobj_tags[$i][1]['resxHeight']);
				
				$x = str_replace($learningobj_tags[$i][0],$flash_tag,$x);
			}else if($resxType=='httplink'){
				$iframe_tag = $this->Get_iFrame_Tag($learningobj_tags[$i][1]['resxPath'],'learningobject'.$learningobj_tags[$i][1]['resxID'],$learningobj_tags[$i][1]['resxWidth'],$learningobj_tags[$i][1]['resxHeight']);
				$x = str_replace($learningobj_tags[$i][0],$iframe_tag,$x);
			}
		}
		
		return $x;
	}
	*/
	
	function Get_Slider_Tag2($src,$id,$effect=" ", $pauseTime =3000)	{
		//$img_string = $src.split(" ");		
		$token = strtok($src, "|=|");	 		
		
		if($effect == " ")$effect = 'random';
		if($pauseTime == " ")$pauseTime = 3000;		
		$x = '<div class="slider-wrapper theme-default" style="width: 600px" ><div id="slider'.$id.'" class="nivoSlider">';
		while ($token != false)
		  {
		  $x .= '<img src="'.trim($token).'" alt="" style="width:600px;" />';
		  $token = strtok("|=|");
		  }
		$x .= '</div></div>';
		
		$x .= '<script type="text/javascript"> $(\'#slider'.$id.'\').nivoSlider({effect:"'.$effect.'", pauseTime:'.$pauseTime.', pauseOnHover: true});</script>';
		return $x;
	}
	
	function Get_Slider_Tag($src,$id,$effect=" ", $pauseTime =3000)	{
		//$img_string = $src.split(" ");		
		$token = strtok($src, "|=|");	 		
		
		if($effect == " ")$effect = 'random';
		if($pauseTime == " ")$pauseTime = 3000;		
		
		$x = '<div style="width: 600px; height: 260px;background-repeat:no-repeat;background-image:url(\''.$token.'\');background-size:contain;" id="slider'.$id.'" >';
		
		$x .= '</div>';
		
		//while ($token != false)
		//  {
		//  $x .= '<img src="'.trim($token).'" alt="" style="width:600px;" />';
		//  $token = strtok("|=|");
		//  }
		
		$x .= '<script type="text/javascript"> var images = []; images.push("'.$token.'");';
		while ($token != false)
		  {
		  $x .= 'images.push("'.$token.'");';
		  $token = strtok("|=|");
		  }
		
		$x .='var currentImage = 0;' .
				'function nextImage() { currentImage++; currentImage =  currentImage % 3;
					if(currentImage == 0) document.getElementById("slider'.$id.'").style.backgroundImage = "url(\'../Images/Photo_slider1.jpg\')"; ' .
					'else if(currentImage == 1)document.getElementById("slider'.$id.'").style.backgroundImage = "url(\'../Images/Photo_slider2.jpg\')";' .
					'else if(currentImage == 2)document.getElementById("slider'.$id.'").style.backgroundImage = "url(\'../Images/Photo_slider3.jpg\')";
				}' .
				'$("#slider'.$id.'").bind("click touch",function(){nextImage();});' .
				'</script>';
		return $x;
	}
	
	function Get_ThickboxNew_Link($id,$src,$title='',$width="420",$height="300", $mode='')
	{	
		
		if($height==''){
			$height = '420';
		}
		if($width==''){
			$width = '300';
		}
		if($mode==""){
			$mode = "text";
		}		
		
		$token = strtok($title, "|");
		$text = "";
		$img = "";
		$dest ="";
		while ($token != false)
		  {		  	
		  	 if($token == "=img="){
		  	 	$img = strtok("|");
		  	 }
		  	 else if ($token == "=text="){
		  	 	$text = strtok("|");	  	 	
		  	 }
		  	 else if ($token == "=dest=")
			 	$dest = strtok("|");	
			 
			 $token = strtok("|");
		  }
		$onclick_handler = 'onclick="jsDisplayLearningObjectInTB(this,\''.$width.'\',\''.$height.'\',\''.$text.'\')"';
		$title_attr = 'title="'.htmlspecialchars($dest,ENT_QUOTES).'"';
		

		$x = '<a class="thickbox" id="'.$id;
		$x.= '" href="#TB_inline?height='.$height.'&width='.$width.'&inlineId=FakeLayer"';
		$x.= $onclick_handler.' '.$title_attr.'>';
		if ($mode == "image" || $mode == "all" )		
			$x.= '<img src="'.$img.'"/>';
		if($mode == "text" || $mode == "all"){
			$x.= "<p style='color:blue' class='bodytext paragraph'>";
			$x.= $text;	
			$x.= "</p>";	
		}	
		$x.= '</a>';		
		
		return $x;
	}
	
	function Get_Draggable_Tool($x, $draggable, $id, $draggableHandler="")
	{	
		if($draggable){
			$x = '<div id="draggable'.$id.'"> <img onclick=" displayControl(document.getElementById(\'tool'.$id.'\')); " src="'.$draggableHandler.'"/><div id="tool'.$id.'" style="display:none">'.$x;
			$x .= '</div></div>';
			$x .= '<script type="text/javascript">' .
					'function displayControl (element){ if(element.style.display == "inline")element.style.display ="none";' .
					' else element.style.display ="inline"; }' .
					'$(function(){$("#draggable'.$id.'").draggable({handle:"img", containment: "#book_content"});})</script>';
		}		
		return $x;
	}
	
	function Get_Concept_Object_Tag($src,$id, $imgNumber, $destDiv, $clickShape, $clickCoordinate)	{
		//$img_string = $src.split(" ");	
		
		$token = strtok($src, "|");
		$x = '<script type="text/javascript">' .
				'function destDivControl(element, text){' .
				'var texts =text.split("\n");' .
				' document.getElementById(element).innerHTML = "";' .
				'for(var i =0; i < texts.length; i++) document.getElementById(element).innerHTML += "<p class=\'bodytext paragraph\'>" + texts[i] + "</p>"}</script>';
		$index = 0;
		while ($token != false)
		  {		  	
		  	 if($token == "=img="){
		  	 	$img = strtok("|");
		  	 	$x .= '<img src="'.$img.'"' ;
		  	 }
		  	 else if ($token == "=text="){
		  	 	$text = strtok("|");	  
		  	 	$x .= 'id = "'.$id.$index.'"   usemap="#map'.$id.$index.'" border="0">' .
		  	 			'<map name="map'.$id.$index.'" id="map'.$id.$index.'">' .
  	 					'<area target="iframe_noUse" href="http://www.eclass.com.hk/templates/bl_theme/images/sub_menu_icon.gif" id="area'.$id.$index.'" coords="'.$clickCoordinate.'" shape="'.$clickShape.'">' .
						'</map>' .
		  	 			'<script type="text/javascript">$("#map'.$id.$index.'").bind("click touch", function(){destDivControl(\''.$destDiv.'\',\''.$text.'\' )});</script>';		  	 			
		  	 	$index++;	 	
		  	 }			 
			 $token = strtok("|");
		  }	
		  
		$x .="<iframe name='iframe_noUse' height='0' width = '0' frameborder='0' style='display: none;'></iframe>";
		return $x;
	}
	
	function Get_Answer_Object_Tag($src,$id, $showTime, $destDiv, $paragraphNumber,$auto, $clickShape, $clickCoordinate, $imgWidth, $imgHeight)	{
		//$img_string = $src.split(" ");	
		
		$token = strtok($src, "|");
		$x = '<script type="text/javascript">' .
				'function answerDivControl(element){var showTime = '.$showTime.';';
		if($auto == "true"){
			for($i = 0; $i < $paragraphNumber; $i++){
			$x .= 'setTimeout(function() {$("#'.$id.$i.'").show(990);}  , showTime*1000*'.$i.');';
			}
		}
		else{
			$x .= 'paragraphNumber++; if(paragraphNumber >= '.$paragraphNumber.' ) return;' .
					'setTimeout(function() {$("#'.$id.'" + paragraphNumber).show(990);}  , 0);';
		}			
		
		$x .= '}; function answerDivInsertParagraph(element, text, paraID){' .
				'document.getElementById(element).innerHTML += "<p class=\'bodytext paragraph \' id=\'"+ paraID+"\'>" + text + "</p>" ;}</script>';
		$index = 0;
		while ($token != false)
		  {		  	
		  	 if($token == "=img="){
		  	 	$img = strtok("|");
		  	 	$x .= '<img style="float:left; width: ' . $imgWidth . 'px" src="'.$img.'" id = "'.$id.'" usemap="#map'.$id.'" border="0"/>'.
		  	 	'<map name="map'.$id.'" id="map'.$id.'">' .
  	 					'<area target="iframe_noUse" href="http://www.eclass.com.hk/templates/bl_theme/images/sub_menu_icon.gif" id="area'.$id.'" coords="'.$clickCoordinate.'" shape="'.$clickShape.'">' .
						'</map>';
		  	 	$x .= '<script type="text/javascript">var paragraphNumber = -1;' .
		  	 			' $("#map'.$id.'").bind("click touch", function(){answerDivControl(\''.$destDiv.'\')});</script>';
		  	 }
		  	 else if ($token == "=text="){
		  	 	$text = strtok("|");	  
		  	 	$x .= '<script type="text/javascript">answerDivInsertParagraph(\''.$destDiv.'\',\''.$text.'\',\''.$id.$index.'\' );' .
		  	 			'$("#'.$id.$index.'").hide();</script>';
				$index++;		  	 			  	  	
		  	 }			 
			 $token = strtok("|");
		  }	
		 $x .="<iframe name='iframe_noUse' height='0' width = '0' frameborder='0' style='display: none;'></iframe>";
		return $x;
	}
	
	function Get_Power_Board_Tag($id,$src, $file_path,$title='',$width="800",$height="660")
	{	
		global $eclass40_httppath;
		
		$info = explode("/", $file_path);
		$bookID = $info[7];
		if($height==''){
			$height = '800';
		}
		if($width==''){
			$width = '660';
		}
		$token = strtok($title, "|");
		$text = "Powerboard";
		$dest = "http://$eclass40_httppath/src/tool/powerboard5/pb5_intranet.php?width=700&height=630&formula=http://".$_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"]."/home/eLearning/ebook_reader/$bookID/reader/$src";	
			 
		$onclick_handler = 'onclick="jsDisplayLearningObjectInTB(this,\''.$width.'\',\''.$height.'\',\''.$text.'\')"';
		$title_attr = 'title="'.htmlspecialchars($dest,ENT_QUOTES).'"';
		

		$x = '<a style="float:left" class="thickbox" id="'.$id;
		$x.= '" href="#TB_inline?height='.$height.'&width='.$width.'&inlineId=FakeLayer"';
		$x.= $onclick_handler.' '.$title_attr.'>';
		
		$x.= '<img alt="" class="icon" id="opener" src="../Images/Writing_Icon.png" />';
		
		$x.= '</a>';		
		
		return $x;
	}
	
	function Generate_LearningObject($html, $file_path)
	{
		$learningobj_tags = $this->Get_LearningObject_Tag($html);
		$x = $html;
		$flash_param_names = array();
		for($i=0;$i<count($learningobj_tags);$i++){
			$resxType = strtolower($learningobj_tags[$i][1]['type']);
			if($resxType=='flash'){
				$flash_params = array();
				foreach($flash_param_names as $key => $param_name){
					if(isset($learningobj_tags[$i][1][$param_name])){
						$flash_params[] = array($param_name,$learningobj_tags[$i][1][$param_name]);
					}
				}
				
				$flash_tag = $this->Get_Flash_Tag($learningobj_tags[$i][1]['src'],$learningobj_tags[$i][1]['id'],$flash_params,$learningobj_tags[$i][1]['width'],$learningobj_tags[$i][1]['height']);
				
				$x = str_replace($learningobj_tags[$i][0],$flash_tag,$x);
			}else if($resxType=='httplink'){
				$iframe_tag = $this->Get_iFrame_Tag($learningobj_tags[$i][1]['src'],$learningobj_tags[$i][1]['id'],$learningobj_tags[$i][1]['width'],$learningobj_tags[$i][1]['height']);
				$x = str_replace($learningobj_tags[$i][0],$iframe_tag,$x);
			}else if($resxType=='thickbox'){
				$desc = ucfirst(rawurldecode($learningobj_tags[$i][1]['description']));
				$onclick_handler = 'jsDisplayLearningObjectInTB(this,\''.$learningobj_tags[$i][1]['width'].'\',\''.$learningobj_tags[$i][1]['height'].'\',\''.rawurlencode($desc).'\')';
				$anchor_tag = $this->Get_Thickbox_Link($learningobj_tags[$i][1]['id'],$desc,$learningobj_tags[$i][1]['src'],$learningobj_tags[$i][1]['width'],$learningobj_tags[$i][1]['height'],'',$onclick_handler);
				$x = str_replace($learningobj_tags[$i][0],$anchor_tag,$x);
			}else if($resxType=='audio'){
				$audio_tag = $this->Get_Audio_Control($learningobj_tags[$i][1]['src'],$learningobj_tags[$i][1]['width'],$learningobj_tags[$i][1]['height']);
				$x = str_replace($learningobj_tags[$i][0],$audio_tag,$x);
			}else if($resxType=='js'){
				$js_html = base64_decode(trim($learningobj_tags[$i][1]['src']));
				$js_html =$this->Get_Draggable_Tool($js_html, $learningobj_tags[$i][1]['draggable'],$learningobj_tags[$i][1]['id'],$learningobj_tags[$i][1]['draggable-handler']);
				$x = str_replace($learningobj_tags[$i][0],$js_html,$x);
			}
			///////////////////////////// 2012-9-13 CharlesMa
			else if($resxType=='image-slider'){
				$slider_tag = $this->Get_Slider_Tag($learningobj_tags[$i][1]['src'],$learningobj_tags[$i][1]['id'], $learningobj_tags[$i][1]['effect'], $learningobj_tags[$i][1]['pauseTime']);
				$x = str_replace($learningobj_tags[$i][0],$slider_tag,$x);
			}
			else if($resxType=='thickbox-new'){
				$desc = ucfirst(rawurldecode($learningobj_tags[$i][1]['description']));
				//$onclick_handler = 'jsDisplayLearningObjectInTB(this,\''.$learningobj_tags[$i][1]['width'].'\',\''.$learningobj_tags[$i][1]['height'].'\',\''.rawurlencode($desc).'\')';
				$anchor_tag = $this->Get_ThickboxNew_Link($learningobj_tags[$i][1]['id'],$desc,$learningobj_tags[$i][1]['src'],$learningobj_tags[$i][1]['width'],$learningobj_tags[$i][1]['height'],$learningobj_tags[$i][1]['mode']);
				$anchor_tag =$this->Get_Draggable_Tool($anchor_tag, $learningobj_tags[$i][1]['draggable'],$learningobj_tags[$i][1]['id'],$learningobj_tags[$i][1]['draggable-handler']);
				$x = str_replace($learningobj_tags[$i][0],$anchor_tag,$x);				
			}	
			else if($resxType=='concept-object'){
				$concept_tag = $this->Get_Concept_Object_Tag($learningobj_tags[$i][1]['src'],$learningobj_tags[$i][1]['id'], $learningobj_tags[$i][1]['imgNumber'], $learningobj_tags[$i][1]['destDiv'], $learningobj_tags[$i][1]['clickShape'], $learningobj_tags[$i][1]['clickCoordinate']);
				$x = str_replace($learningobj_tags[$i][0],$concept_tag,$x);				
			}	
			else if($resxType=='answer-object'){
				$answer_tag = $this->Get_Answer_Object_Tag($learningobj_tags[$i][1]['src'],$learningobj_tags[$i][1]['id'], $learningobj_tags[$i][1]['showTime'], $learningobj_tags[$i][1]['destDiv'], $learningobj_tags[$i][1]['paragraphNumber'], $learningobj_tags[$i][1]['auto'], $learningobj_tags[$i][1]['clickShape'], $learningobj_tags[$i][1]['clickCoordinate'], $learningobj_tags[$i][1]['width'], $learningobj_tags[$i][1]['height']);
				$x = str_replace($learningobj_tags[$i][0],$answer_tag,$x);				
			}	
			else if($resxType=='power-board'){
				$answer_tag =  $this->Get_Power_Board_Tag($learningobj_tags[$i][1]['id'], $learningobj_tags[$i][1]['src'],$file_path);
				$x = str_replace($learningobj_tags[$i][0],$answer_tag,$x);				
			}	
		}
		
		return $x;
	}
	
	function Get_Img_Src($html,$getAllImgTags=false)
	{
		$img_arr = array();
		$tmp_inner_html = $html;
		while( ($img_start = stripos($tmp_inner_html, '<img', 0)) !== FALSE )
		{
			$img_end = stripos($tmp_inner_html,'>',$img_start);
			$img_tag = substr($tmp_inner_html, $img_start, $img_end + 1 - $img_start);
			$tmp_inner_html = substr($tmp_inner_html, $img_end + 1, strlen($tmp_inner_html) - $img_end);
			$img_arr[] = $img_tag;
		}
		
		$img_src_arr = array();
		for($i=0;$i<count($img_arr);$i++){
			if(preg_match("/src\s*=\s*['|\"]([^'\"]+)['|\"]/",$img_arr[$i],$matches))
			{
				$img_src_arr[] = $matches[1];
			}
		}
		
		if($getAllImgTags){
			return $img_src_arr;
		}else{
			return $img_src_arr[0];
		}
	}
	
	function Update_Image_Width($BookID, $ImageWidth){
		$sql = " UPDATE INTRANET_EBOOK_BOOK Set ImageWidth = '$ImageWidth' Where BookID = '$BookID'  ";
		return $this->db_db_query($sql);		
	}
	
	function Get_HTML_Image_Srcs($BookID,$RequestPaths,$extra)
	{
		global $intranet_root;
		
		$book_info = $this->Get_Book_List($BookID);		
		
		//2013-10-17 Charles Ma
		$book_elibinfo =  $this->Get_eLib_Book_Info($BookID);		
		$IsFirstPageBlank = $book_elibinfo[0]['IsFirstPageBlank'];		
		//2013-10-17 Charles Ma END
		
		$BookPath = $book_info[0]['BookPath'];
		$SubFolder = trim($book_info[0]['BookSubFolder']);
		$InputFormat = $book_info[0]['InputFormat'];
		
		$base_path = $intranet_root.$BookPath.($SubFolder!=''?("/".$SubFolder) : "");
		if($base_path[strlen($base_path)-1] != '/'){
			$base_path .= '/';
		}
		
		$img_src_arr = array();
		for($i=0;$i<count($RequestPaths);$i++){
			$full_path = $base_path.$RequestPaths[$i];
			if(file_exists($full_path)){				
				//// Charles Ma 2013-07-16
				if($InputFormat==4){					
					$txt_link = explode("text/",$RequestPaths[$i]);
					$page = explode(".txt",$txt_link[1]);				
										
					// 20140516-page999 Charles Ma
					$page_number = $IsFirstPageBlank && $extra != $i + 1 ? (int)$page[0] - 1 : $page[0];
					$page_number = sprintf("%04d", $page_number);
					$page_name = str_replace("/", "",$SubFolder)."_".$page_number;
					if($IsFirstPageBlank && $page_number == 1)
						$img_src_arr[] = "";
					else $img_src_arr[] = "../images/".$page_name.".jpg";
					//2013-10-17 Charles Ma END
				}else{					
					$html = file_get_contents($full_path);
					$img_src_arr[] = $this->Get_Img_Src($html);
				}
			}
		}
		
		return $img_src_arr;
	}
	
	function Get_Rewrite_File_Content($file_path, $is_upload = false, $page_mode = 2, $is_debug = false)
	{
		global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		if (file_exists($file_path))
		{
			$fileext = substr($file_path, strrpos($file_path, ".")+1);
			$isHtml = false;
			switch (strtolower($fileext))
			{
				case "ogg":
					header("Content-type: audio/ogg");
					break;
				case "js":
					header("Content-type: text/javascript");
					break;
				case "css":
					header("Content-type: text/css");
					break;
				case "txt":
					header("Content-type: text/plain");
					break;
				case "htm":
				case "xhtml":
				case "html":
					header("Content-type: text/html");
					$isHtml = true;
					break;
				case "jpg":
					header("Content-type: image/jpeg");
					break;
				case "gif":
					header("Content-type: image/gif");
					break;
				case "png":
					header("Content-type: image/png");
					break;
				case "swf":
					header("Content-type: application/x-shockwave-flash");
					break;
				case "wma":
					header("Content-type: audio/x-ms-wma");
					break;
				case "mp3":
					header("Content-type: audio/mp3");
					break;
				case "wav":
					header("Content-type: audio/wav");
					break;
				case "mpeg":
					header("Content-type: audio/mpeg");
					break;
				case "midi":
				case "mid":
					header("Content-type: audio/midi");
					break;
				default:
					header("Content-type: text/plain");
					break;
			}
			$x = file_get_contents($file_path);
			if($isHtml){
				$x = $this->GetHtmlContent($x);
				$x = $this->Generate_LearningObject($x, $file_path);
				if($is_upload){
					if($is_debug){
						$y = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						  		<link rel="stylesheet" href="/templates/'.$LAYOUT_SKIN.'/css/ebook_reader.css" type="text/css" />
						  	</head>
							<body onload="window.parent.jsCountPage();">
							<div id="book_content" class="book_'.$page_mode.'page_content">
								<div class="book_content_container" style="overflow:visible">
									<div id="book_content_main" class="book_content_main book_content_main_for_upload" style="overflow:visible">'.$x.'</div>
								</div>
						  	</div>
							</body>
						  </html>';
					}else{
						$y = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							  <html xmlns="http://www.w3.org/1999/xhtml">
								<head>
									<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
							  		<link rel="stylesheet" href="/templates/'.$LAYOUT_SKIN.'/css/ebook_reader.css" type="text/css" />
							  	</head>
								<body onload="window.parent.jsCountPage();">
								<div id="book_content" class="book_'.$page_mode.'page_content">
									<div class="book_content_container" style="overflow:visible">
										<div id="book_content_main" class="book_content_main book_content_main_for_upload" style="overflow:visible">'.$x.'</div>
									</div>
							  	</div>
								</body>
							  </html>';
					}
					
					$x = $y;
				}
			}
		}
		else
		{
			$x = "file: " . substr($file_path, strrpos($file_path, "/")+1) . " not found"; 
		}
		
		return $x;
	}
	
	function Encrypt($string, $key) {
	  	$result = '';
	  	for($i=0; $i<strlen($string); $i++) {
	   		$char = substr($string, $i, 1);
	    	$keychar = substr($key, ($i % strlen($key))-1, 1);
	    	$char = chr(ord($char)+ord($keychar));
	    	$result.=$char;
	  	}
		
	  	return base64_encode($result);
	}
	
	function Decrypt($string, $key) {
		$result = '';
	  	$string = base64_decode($string);
	
	  	for($i=0; $i<strlen($string); $i++) {
	    	$char = substr($string, $i, 1);
	    	$keychar = substr($key, ($i % strlen($key))-1, 1);
	    	$char = chr(ord($char)-ord($keychar));
	    	$result.=$char;
	  	}
		
	  	return $result;
	}	
	
	function Upload_EPUB_Book_TEST($filedata, $field_name, $bookIDGiven=null, $ImageBookSetting=array(), $onePageModeSetting=array(),$InputArray)
	{
		
		///////////////////Temp
		$book_id = 1913;
		///////////////////Temp
		
		global $intranet_root, $PATH_WRT_ROOT;	
		include_once("libfilesystem.php");
	
		$lf = new libfilesystem();
		
		$tmp_path = $intranet_root.$this->EBOOK_FILE_PATH;
		if(!file_exists($tmp_path) || !is_dir($tmp_path)){
			$lf->createFolder($tmp_path);
			echo("B");
		}
		$success = array();
		if(isset($filedata[$field_name])){
			if($filedata[$field_name]['error'] > 0)
			{
				$success['UploadError'] = false;
			}else{
				$fileext = strtolower(substr($filedata[$field_name]["name"],strripos($filedata[$field_name]["name"],'.',0)+1));
				if($fileext != 'zip') return false;
				
				$inputby = $_SESSION['UserID'];
				$imageBook = trim($ImageBookSetting['ImageBook']);
				$imageWidth = trim($ImageBookSetting['ImageWidth']);
				$ForceToOnePageMode = trim($onePageModeSetting['ForceToOnePageMode']);
				$OnePageModeWidth = trim($onePageModeSetting['OnePageModeWidth']);
				$OnePageModeHeight = trim($onePageModeSetting['OnePageModeHeight']);
				
				$referenceID = "";
				
//				if ($bookIDGiven!=null && $bookIDGiven>0)
//				{
//					$sql = "INSERT INTO INTRANET_EBOOK_BOOK (BookID, ImageBook, ImageWidth, RecordStatus,DateInput,InputBy,ForceToOnePageMode,OnePageModeWidth,OnePageModeHeight) " .
//							"VALUES({$bookIDGiven},'".$imageBook."','".$imageWidth."', '0',NOW(),'".$inputby."','".$ForceToOnePageMode."', '".$OnePageModeWidth."', '".$OnePageModeHeight."')";
//					$success['InsertDB'] = $this->db_db_query($sql);
//					if(!$success['InsertDB']) return false;
//					$book_id = $bookIDGiven;
//				} else
//				{
//					$sql = "INSERT INTO INTRANET_EBOOK_BOOK (ImageBook, ImageWidth, RecordStatus,DateInput,InputBy,ForceToOnePageMode,OnePageModeWidth,OnePageModeHeight)" .
//							" VALUES('".$imageBook."','".$imageWidth."','0',NOW(),'".$inputby."','".$ForceToOnePageMode."', '".$OnePageModeWidth."', '".$OnePageModeHeight."')";
//					$success['InsertDB'] = $this->db_db_query($sql);
//					if(!$success['InsertDB']) return false;
//					$book_id = $this->db_insert_id();
//				}
				
				$tmp_path = $intranet_root.$this->EBOOK_FILE_PATH."/test_".$book_id;
				if(!file_exists($tmp_path) || !is_dir($tmp_path)){
					$lf->createFolder($tmp_path);
				}
				if(file_exists($tmp_path)){
					$file_name = $filedata[$field_name]["name"];
					$full_path = $tmp_path.'/'.$file_name;
					$db_book_path = $this->EBOOK_FILE_PATH.'/'.$book_id;
					if(!file_exists($full_path) || !is_file($full_path)){
						$lf->lfs_copy($filedata[$field_name]["tmp_name"], $full_path);
					}
					if(file_exists($full_path)){
						$success['UnzipEPUB'] = $lf->unzipFile($full_path,$tmp_path);
						$file = glob($tmp_path."/". "*");
						for($a=1;$a <=count($file);$a++){
							$current_file = explode("/",$file[$a-1]);
							$current_file_name = $current_file[count($current_file)-1];
							
							if(!in_array("zip",explode(".",$current_file_name))){
								$referenceID = $current_file_name;
								break;
							}
						}			
						//$text_path = $tmp_path.'/'.$referenceID.'/'.$referenceID.'.zip';							
						//$text_tmp_path = $tmp_path.'/'.$referenceID.'/'.$referenceID;	
						//if(!file_exists($text_tmp_path) || !is_dir($text_tmp_path)) $lf->createFolder($text_tmp_path);						
						//$success['UnzipText'] = $lf->unzipFile($text_path,$text_tmp_path);
						
						rename($tmp_path.'/'.$referenceID.'/'.$referenceID, $tmp_path.'/'.$referenceID.'/Text');		
					}					
					$text_files = glob($tmp_path.'/'.$referenceID.'/Text/' . "*");
					usort($text_files, "cmp");
					
					
					$success['UnzipEPUB'] = false;
					if(!in_array(false,$success)){						
						$book_title = $InputArray["Title"];
						$subfolder = "";
						$sql = "UPDATE INTRANET_EBOOK_BOOK SET 
									BookTitle = '".$book_title."', BookPath = '".$db_book_path."', BookSubFolder = '".$subfolder."', RecordStatus = '1' 
								WHERE BookID = '".$book_id."' ";
						$success['UpdateDB'] = $this->db_db_query($sql);
						
						if($success['UpdateDB']){							
							$this->BookID = $book_id;
							$text = glob($tmp_path."/Text/" . "*");
							//$count_images = count(glob($tmp_path."/Images/" . "*"));
							$count_text = count(glob($tmp_path."/Text/" . "*"));
							$page_sql = "INSERT INTO INTRANET_EBOOK_PAGE_INFO (BookID,RefID,FileName".($imageBook==1?",Pages1PageMode,Pages2PageMode":"").") VALUES ";
							$page_values = "";
							$delimiter = "";
							if($count_text>0){
								for($k =1;$k <=$count_text ; $k++ ){
									$current_file = explode("/",$text[$k-1]);
									$refid = $current_file[count($current_file)-1];
									$href = $current_file[count($current_file)-2]."/".$current_file[count($current_file)-1];
									$page_values .= $delimiter."('".$book_id."','".$refid."','".$href."'";
									$page_values .= $imageBook == 1? ",'1','1'" : "";
									$page_values .= ")";
									$delimiter = ",";
								}
								$page_sql .= $page_values;
								$success['InsertPages'] = $this->db_db_query($page_sql);
							}
							$this->CoverImagePath = $this->EBOOK_FILE_PATH."/".$book_id."/Images/cover.jpg";
							$this->CoverImageName = "Images/cover.jpg";
						}
					}
				}
			}
		}
		
		return !in_array(false,$success);
	}
	
	function Search_KeyWord($BookID, $KeyWord){
		
		//2013-10-17 Charles Ma
		$book_elibinfo =  $this->Get_eLib_Book_Info($BookID);		
		$IsFirstPageBlank = $book_elibinfo[0]['IsFirstPageBlank'];		
		//2013-10-17 Charles Ma END
		
		$KeyWord = addslashes($KeyWord);
		$sql = "Select RefID, Content From INTRANET_EBOOK_PAGE_INFO WHERE BookID = $BookID AND Content LIKE '%$KeyWord%'";
		$result = $this->returnArray($sql);
		
		$json_arr = "{";
		$delimiter = "";
		foreach($result as $page_result){
			$refID = $IsFirstPageBlank ? (int)(str_replace(".txt","",$page_result["RefID"])) :(int)(str_replace(".txt","",$page_result["RefID"])) - 1;
						
			$json_arr.= $delimiter.'"'.$refID.'":"'.rawurlencode(addslashes($page_result["Content"])).'"';
			$delimiter = ",";
		}
		$json_arr.="}";
		return $json_arr;
	}
	
  	function GET_NEW_TABLE_OF_CONTENT($book_id, $sub_folder){
	    global $intranet_root;
	    $tmp_path = $intranet_root.$this->EBOOK_FILE_PATH."/".$book_id."/".$sub_folder."/".$sub_folder."_outline.txt";
	    
	    $file = file($tmp_path);
	    $current_content = "";
	    $z = 0;
	    $this->table_of_content['navPoint'] = array();
	    
	    //2013-10-17 Charles Ma
	    $book_elibinfo =  $this->Get_eLib_Book_Info($book_id);    
	    $IsFirstPageBlank = $book_elibinfo[0]['IsFirstPageBlank'];  
	    
	    $book_info = $this->Get_Book_List($book_id);    
	    
	    $InputFormat = $book_info[0]['InputFormat'];  
	    //2013-10-17 Charles Ma END
	    
	    foreach ($file as $line){
	      if($z > 1000) break;  
	      $z++;
	      if($z == 1) continue;
	      $parts = explode("][",$line);
	      $pos = str_replace("[","",$parts[0]) ;
	      $name = $parts[1] ;
	      $ref = (int)str_replace("]","",$parts[2]) ;
	      
	      if($IsFirstPageBlank)
	        $ref = (int)$ref + 1;
	      
	      $key = $z - 1;
	      //$this->table_of_content['navPoint'] = array($key=>array("navLabel" => $name,"content"=>"Text/$ref.txt", "navPointList"=>array()));
	      if($InputFormat == 4){
	        array_push($this->table_of_content['navPoint'],array("navLabel" => $name,"content"=>"text/$ref.txt", "navPointList"=>array()));           
	      }else{
	        array_push($this->table_of_content['navPoint'],array("navLabel" => $name,"content"=>"Text/$ref.txt", "navPointList"=>array()));       
	      }	      
	    }
  	}
	
	function Upload_EPUB_Book_New($filedata, $field_name, $bookIDGiven=null, $ImageBookSetting=array(), $onePageModeSetting=array(),$InputArray)
	{		
		///////////////////Temp
		//$book_id = 1913;
		///////////////////Temp
		
		global $intranet_root, $PATH_WRT_ROOT;	
		include_once("libfilesystem.php");
	
		$lf = new libfilesystem();
		
		$tmp_path = $intranet_root.$this->EBOOK_FILE_PATH;
		if(!file_exists($tmp_path) || !is_dir($tmp_path)){
			$lf->createFolder($tmp_path);
		}
		$success = array();
		if(isset($filedata[$field_name])){
			if($filedata[$field_name]['error'] > 0)
			{
				$success['UploadError'] = false;
			}else{
				$fileext = strtolower(substr($filedata[$field_name]["name"],strripos($filedata[$field_name]["name"],'.',0)+1));
				if($fileext != 'zip') return false;
				
				$inputby = $_SESSION['UserID'];
				$imageBook = trim($ImageBookSetting['ImageBook']);
				$imageWidth = trim($ImageBookSetting['ImageWidth']);
				$ForceToOnePageMode = trim($onePageModeSetting['ForceToOnePageMode']);
				$OnePageModeWidth = trim($onePageModeSetting['OnePageModeWidth']);
				$OnePageModeHeight = trim($onePageModeSetting['OnePageModeHeight']);
				
				
				if ($bookIDGiven!=null && $bookIDGiven>0)
				{
					$sql = "INSERT INTO INTRANET_EBOOK_BOOK (BookID, ImageBook, ImageWidth, RecordStatus,DateInput,InputBy,ForceToOnePageMode,OnePageModeWidth,OnePageModeHeight) " .
							"VALUES({$bookIDGiven},'".$imageBook."','".$imageWidth."', '0',NOW(),'".$inputby."','".$ForceToOnePageMode."', '".$OnePageModeWidth."', '".$OnePageModeHeight."')";
					$success['InsertDB'] = $this->db_db_query($sql);
					if(!$success['InsertDB']) return false;
					$book_id = $bookIDGiven;
				} else
				{
					$sql = "INSERT INTO INTRANET_EBOOK_BOOK (ImageBook, ImageWidth, RecordStatus,DateInput,InputBy,ForceToOnePageMode,OnePageModeWidth,OnePageModeHeight)" .
							" VALUES('".$imageBook."','".$imageWidth."','0',NOW(),'".$inputby."','".$ForceToOnePageMode."', '".$OnePageModeWidth."', '".$OnePageModeHeight."')";
					$success['InsertDB'] = $this->db_db_query($sql);
					if(!$success['InsertDB']) return false;
					$book_id = $this->db_insert_id();
				}
				// TODO
				$tmp_path = $intranet_root.$this->EBOOK_FILE_PATH."/".$book_id;
				if(!file_exists($tmp_path) || !is_dir($tmp_path)){
					$lf->createFolder($tmp_path);
				}
				if(file_exists($tmp_path)){
					$file_name = $filedata[$field_name]["name"];
					$full_path = $tmp_path.'/'.$file_name;
					$db_book_path = $this->EBOOK_FILE_PATH.'/'.$book_id;
					if(!file_exists($full_path) || !is_file($full_path)){
						$lf->lfs_copy($filedata[$field_name]["tmp_name"], $full_path);
					}
					if(file_exists($full_path)){
						$success['UnzipEPUB'] = $lf->unzipFile($full_path,$tmp_path);
						$file = glob($tmp_path."/". "*");
						for($a=1;$a <=count($file);$a++){
							$current_file = explode("/",$file[$a-1]);
							$current_file_name = $current_file[count($current_file)-1];
							
							$file_arr = explode(".",$current_file_name); 
							
							if(!in_array("zip",$file_arr)){
								$referenceID = $current_file_name;
								break;
							}
							if(!in_array("txt",$file_arr)){
								$referenceID_arr =  explode("_",$file_arr[0]); 
								$referenceID = $referenceID_arr[0];
								break;
							}
						}			
						//$text_path = $tmp_path.'/'.$referenceID.'/'.$referenceID.'.zip';							
						//$text_tmp_path = $tmp_path.'/'.$referenceID.'/'.$referenceID;	
						//if(!file_exists($text_tmp_path) || !is_dir($text_tmp_path)) $lf->createFolder($text_tmp_path);						
						//$success['UnzipText'] = $lf->unzipFile($text_path,$text_tmp_path);
						
						if(!file_exists($tmp_path.'/'.$referenceID.'/Text') && file_exists($tmp_path.'/'.$referenceID.'/'.$referenceID))
							rename($tmp_path.'/'.$referenceID.'/'.$referenceID, $tmp_path.'/'.$referenceID.'/Text');
					}	
					
					
					if(!in_array(false,$success)){						
						$book_title = $InputArray["Title"];
						$input_format = $InputArray["InputFormat"];
						$subfolder = "$referenceID/";
						$this->GET_NEW_TABLE_OF_CONTENT($book_id, $referenceID);
						
//						$this->Parse_EPUB_XML($tmp_path);
//						$subfolder = $this->Get_Safe_Sql_Query($this->ebook_subfolder_path);
//						$book_title = $this->Get_Safe_Sql_Query($this->metadata_array['dc:title']);
						$sql = "UPDATE INTRANET_EBOOK_BOOK SET 
									BookTitle = '".$book_title."', BookPath = '".$db_book_path."', BookSubFolder = '".$subfolder."', RecordStatus = '1' ,  InputFormat = '".$input_format."'
								WHERE BookID = '".$book_id."' ";
						$success['UpdateDB'] = $this->db_db_query($sql);
//						
						if($success['UpdateDB']){							
							$this->BookID = $book_id;
							$text = glob($tmp_path.'/'.$referenceID.'/Text/' . "*");
							usort($text, "cmp");							
							//$count_images = count(glob($tmp_path."/Images/" . "*"));
							$count_text = count($text);
							$page_sql = "INSERT INTO INTRANET_EBOOK_PAGE_INFO (Content,BookID,RefID,FileName".($imageBook==1?",Pages1PageMode,Pages2PageMode":"").") VALUES ";
							$page_values = "";
							$delimiter = "";
							if($count_text>0){
								for($k =2;$k <=$count_text ; $k++ ){
									$current_file = explode("/",$text[$k-1]);
									$file = file($text[$k-1]);
									$current_content = "";
									$z = 0;
									foreach ($file as $line){
										$z++; if($z > 1000) break;
										$line = trim($line);
										$current_content .= $line;
									}
									$current_content = addslashes($current_content);
									
									$refid = $current_file[count($current_file)-1];
									$href = $current_file[count($current_file)-2]."/".$current_file[count($current_file)-1];
									$page_values .= $delimiter."('".$current_content."','".$book_id."','".$refid."','".$href."'";
									$page_values .= $imageBook == 1? ",'1','1'" : "";
									$page_values .= ")";
									$delimiter = ",";
								}
								$page_sql .= $page_values;
								$success['InsertPages'] = $this->db_db_query($page_sql);
							}
							$this->CoverImagePath = $this->EBOOK_FILE_PATH."/".$book_id."/Images/cover.jpg";
							$this->CoverImageName = "Images/cover.jpg";
						}
					}
				}
			}
		}
		
		return !in_array(false,$success);
	}
	
	// Upload an epub file, unzip it and record in DB
	function Upload_EPUB_Book($filedata, $field_name, $bookIDGiven=null, $ImageBookSetting=array(), $onePageModeSetting=array(),$InputArray)
	{
		global $intranet_root, $PATH_WRT_ROOT;	
		include_once("libfilesystem.php");
	
		$lf = new libfilesystem();
		
		$tmp_path = $intranet_root.$this->EBOOK_FILE_PATH;
		if(!file_exists($tmp_path) || !is_dir($tmp_path)){
			$lf->createFolder($tmp_path);
		}
		$success = array();
		if(isset($filedata[$field_name])){
			if($filedata[$field_name]['error'] > 0)
			{
				$success['UploadError'] = false;
			}else{
				$fileext = strtolower(substr($filedata[$field_name]["name"],strripos($filedata[$field_name]["name"],'.',0)+1));
				if($fileext != 'epub') return false;
				
				$inputby = $_SESSION['UserID'];
				$imageBook = trim($ImageBookSetting['ImageBook']);
				$imageWidth = trim($ImageBookSetting['ImageWidth']);
				$ForceToOnePageMode = trim($onePageModeSetting['ForceToOnePageMode']);
				$OnePageModeWidth = trim($onePageModeSetting['OnePageModeWidth']);
				$OnePageModeHeight = trim($onePageModeSetting['OnePageModeHeight']);
				
				
				if ($bookIDGiven!=null && $bookIDGiven>0)
				{	
					$sql = "INSERT INTO INTRANET_EBOOK_BOOK (BookID, ImageBook, ImageWidth, RecordStatus,DateInput,InputBy,ForceToOnePageMode,OnePageModeWidth,OnePageModeHeight) " .
							"VALUES({$bookIDGiven},'".$imageBook."','".$imageWidth."', '0',NOW(),'".$inputby."','".$ForceToOnePageMode."', '".$OnePageModeWidth."', '".$OnePageModeHeight."')";
					$success['InsertDB'] = $this->db_db_query($sql);
					if(!$success['InsertDB']) return false;
					$book_id = $bookIDGiven;
				} else
				{
					$sql = "INSERT INTO INTRANET_EBOOK_BOOK (ImageBook, ImageWidth, RecordStatus,DateInput,InputBy,ForceToOnePageMode,OnePageModeWidth,OnePageModeHeight)" .
							" VALUES('".$imageBook."','".$imageWidth."','0',NOW(),'".$inputby."','".$ForceToOnePageMode."', '".$OnePageModeWidth."', '".$OnePageModeHeight."')";
					$success['InsertDB'] = $this->db_db_query($sql);
					if(!$success['InsertDB']) return false;
					$book_id = $this->db_insert_id();
				}
				
				$tmp_path = $intranet_root.$this->EBOOK_FILE_PATH."/".$book_id;
				if(!file_exists($tmp_path) || !is_dir($tmp_path)){
					$lf->createFolder($tmp_path);
				}
				
				if(file_exists($tmp_path)){
					$file_name = $filedata[$field_name]["name"];
					$full_path = $tmp_path.'/'.$file_name;
					$db_book_path = $this->EBOOK_FILE_PATH.'/'.$book_id;
					if(!file_exists($full_path) || !is_file($full_path)){
						$lf->lfs_copy($filedata[$field_name]["tmp_name"], $full_path);
					}
					
					if(file_exists($full_path)){
						$success['UnzipEPUB'] = $lf->unzipFile($full_path,$tmp_path);
					}
					
					if(!in_array(false,$success)){
						$input_format = $InputArray["InputFormat"];
						$this->Parse_EPUB_XML($tmp_path);
						$subfolder = $this->Get_Safe_Sql_Query($this->ebook_subfolder_path);
						$book_title = $this->Get_Safe_Sql_Query($this->metadata_array['dc:title']);
						$sql = "UPDATE INTRANET_EBOOK_BOOK SET 
									BookTitle = '".$book_title."', BookPath = '".$db_book_path."', BookSubFolder = '".$subfolder."', RecordStatus = '1' , InputFormat = '".$input_format."'
								WHERE BookID = '".$book_id."' ";
						$success['UpdateDB'] = $this->db_db_query($sql);
						
						if($success['UpdateDB']){
							$this->BookID = $book_id;
							$items = $this->opf_items;
							$spines = $this->opf_spine;
							$page_sql = "INSERT INTO INTRANET_EBOOK_PAGE_INFO (BookID,RefID,FileName".($imageBook==1?",Pages1PageMode,Pages2PageMode":"").") VALUES ";
							$page_values = "";
							$delimiter = "";
						/*	if(count($items)>0){
								foreach($items as $key => $val){
									$refid = $this->Get_Safe_Sql_Query($val['id']);
									$href = $this->Get_Safe_Sql_Query($val['href']);
									$page_values .= $delimiter."('".$book_id."','".$refid."','".$href."')";
									$delimiter = ",";
								}
								$page_sql .= $page_values;
								$success['InsertPages'] = $this->db_db_query($page_sql);
							} */
							if(count($spines)>0){
								foreach($spines as $key => $val){
									if(!isset($items[$val['idref']])) continue;
									$tmp_item = $items[$val['idref']];
									$refid = $this->Get_Safe_Sql_Query($tmp_item['id']);
									$href = $this->Get_Safe_Sql_Query($tmp_item['href']);
									$page_values .= $delimiter."('".$book_id."','".$refid."','".$href."'";
									$page_values .= $imageBook == 1? ",'1','1'" : "";
									$page_values .= ")";
									$delimiter = ",";
								}
								$page_sql .= $page_values;
								$success['InsertPages'] = $this->db_db_query($page_sql);
							}
						}
					}
				}
			}
		}
		
		return !in_array(false,$success);
	}
	
	function Get_Book_List($book_id='')
	{
		if(is_array($book_id) && count($book_id)>0){
			$cond_book_id = " AND BookID IN (".implode(",",$book_id).") ";
		}else{
			$cond_book_id = $book_id != ''? " AND BookID = '".$book_id."' " : "";
		}
		$sql = "SELECT * FROM INTRANET_EBOOK_BOOK WHERE RecordStatus = '1' $cond_book_id ORDER BY BookTitle";
		
		return $this->returnArray($sql);
	}
	
	function Delete_Book($book_id)
	{
		global $intranet_root, $PATH_WRT_ROOT;	
		include_once("libfilesystem.php");
	
		$lf = new libfilesystem();
		
		$success = array();
		$booklist = $this->Get_Book_List($book_id);
		for($i=0;$i<count($booklist);$i++)
		{
			$book_path = trim($booklist[$i]['BookPath']);
			if($book_path == ''){
				$success['DeleteBookID'.$booklist[$i]['BookID']] = false;
				continue;
			}
			
			$full_path = $intranet_root.$book_path;
			if(file_exists($full_path)){
				$lf->deleteDirectory($full_path);
				
				$sql = "DELETE FROM INTRANET_EBOOK_BOOK WHERE BookID = '".$booklist[$i]['BookID']."' ";
				$success['DeleteDB'.$booklist[$i]['BookID']] = $this->db_db_query($sql);
				
				$sql = "DELETE FROM INTRANET_EBOOK_PAGE_INFO WHERE BookID = '".$booklist[$i]['BookID']."' ";
				$succss['DeletePageInfo'.$booklist[$i]['BookID']] = $this->db_db_query($sql);
			}
		}
		return !in_array(false,$success);
	}
	
	function Get_Book_Page_Info($BookID)
	{
		$sql = "SELECT RecordID, RefID, FileName, Pages1PageMode, Pages2PageMode 
				FROM INTRANET_EBOOK_PAGE_INFO 
				WHERE BookID = '".$BookID."' AND Pages1PageMode IS NOT NULL AND Pages2PageMode IS NOT NULL";
		return $this->returnArray($sql);
	}
	
	function Update_Book_Page_Count($BookID,$RefID,$PageMode,$PageCount)
	{
		$field = "Pages".$PageMode."PageMode";
		$sql = "UPDATE INTRANET_EBOOK_PAGE_INFO SET ".$field." = '".$PageCount."' WHERE BookID = '".$BookID."' AND RefID = '".$RefID."' ";
		return $this->db_db_query($sql);
	}
	
	function Get_Page_Index($BookID,$PageMode)
	{
		$book_elibinfo = $this->Get_eLib_Book_Info($BookID);	
		$Publisher = $book_elibinfo[0]['Publisher'];
		$SubCategory = $book_elibinfo[0]['SubCategory'];			
		$engclassicMode = $Publisher=="English Classics"||$SubCategory=="English Classics";		
		
		$page_info_list = $this->Get_Book_Page_Info($BookID);
		
		$pageModeField = $engclassicMode ? 'Pages2PageMode': 'Pages'.$PageMode.'PageMode';
		
		$page_index = array();
		$page_index[] = array('idref'=>'','href'=>'','session'=>''); // pad 0 with dummy item since page count is start from page 1
		
		for($i=0;$i<count($page_info_list);$i++){
			$refID = $page_info_list[$i]['RefID'];
			$href = $page_info_list[$i]['FileName'];
			
			$pageCount = $PageMode == 2 && $engclassicMode && (int)$page_info_list[$i][$pageModeField] >= 10 ? $page_info_list[$i][$pageModeField] : $page_info_list[$i][$pageModeField];
			
			for($j=1;$j<=$pageCount;$j++){
				$page_index[] = array('idref'=> $refID, 'href' => $href, 'session' => $j);
			}
			
		}
		return $page_index;
	}
	
	function Get_Book_User_Highlight_Notes($pBookID,$pUserID,$pRefID='',$pUniqueID='')
	{
		$sql = "SELECT RecordID, BookID, UserID, RefID, UniqueID, Highlight, DATE_FORMAT(DateModified,'%Y-%m-%d %H:%i:%s') FROM INTRANET_EBOOK_HIGHLIGHT_NOTES WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' ";
		if($pRefID != ''){
			$sql .= " AND RefID = '".$this->Get_Safe_Sql_Query($pRefID)."' ";
		}
		if($pUniqueID != ''){
			$sql .= " AND UniqueID = '".$pUniqueID."' ";
		}
		$resultList = $this->returnArray($sql);
		return $resultList;
	}
	
	function Get_Page_Highlights($pBookID,$pUserID,$pRefID)
	{
		$sql = "SELECT Highlight FROM INTRANET_EBOOK_HIGHLIGHT_NOTES WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' AND RefID = '".$this->Get_Safe_Sql_Query($pRefID)."' ";
		return $this->returnVector($sql);
	}
	
	function Update_Book_User_Highlight_Notes($DataArray)
	{
		$userid = $_SESSION['UserID'];
		$success = array();
		for($i=0;$i<count($DataArray);$i++){
			$BookID = $DataArray[$i]['BookID'];
			$RefID = $this->Get_Safe_Sql_Query($DataArray[$i]['RefID']);
			$UniqueID = $DataArray[$i]['UniqueID'];
			$Highlight = $this->Get_Safe_Sql_Query($DataArray[$i]['Highlight']);
			//$Notes = $this->Get_Safe_Sql_Query($DataArray[$i]['Notes']);
			$sql = "INSERT IGNORE INTO INTRANET_EBOOK_HIGHLIGHT_NOTES (BookID, UserID, RefID, UniqueID, Highlight, DateInput, DateModified) 
					VALUES ('".$BookID."','".$userid."','".$RefID."','".$UniqueID."','".$Highlight."',NOW(),NOW())";
			$success[$i.'_insert'] = $this->db_db_query($sql);
			$affected_rows = $this->db_affected_rows();
			if($affected_rows == 0){
				$sql = "UPDATE INTRANET_EBOOK_HIGHLIGHT_NOTES SET Highlight = '".$Highlight."', DateModified = NOW()  
						WHERE BookID = '".$BookID."' AND RefID = '".$RefID."' AND UniqueID = '".$UniqueID."' ";
				$success[$i.'_update'] = $this->db_db_query($sql);
			}
		}
		return !in_array(false,$success);
	}
	
	function Update_Book_User_Highlight_Notes_By_ID($pBookID, $pUserID, $pUniqueID, $pHighlightNotes)
	{
		$Highlight = $this->Get_Safe_Sql_Query($pHighlightNotes);
		$sql = "UPDATE INTRANET_EBOOK_HIGHLIGHT_NOTES SET Highlight = '".$Highlight."', DateModified = NOW() WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' AND UniqueID = '".$pUniqueID."' ";
		return $this->db_db_query($sql);
	}
	
	function Delete_Book_User_Highlight_Notes($pBookID,$pUserID,$pRefID='',$pUniqueID='')
	{
		$sql = "DELETE FROM INTRANET_EBOOK_HIGHLIGHT_NOTES WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' ";
		if($pRefID != ''){
			$sql .= " AND RefID = '".$this->Get_Safe_Sql_Query($pRefID)."' ";
		}
		if($pUniqueID != ''){
			$sql .= " AND UniqueID = '".$pUniqueID."' ";
		}
		return $this->db_db_query($sql);
	}
	
	function Get_Book_User_Bookmarks($pBookID, $pUserID, $pPageMode)
	{
		$pageField = "PageNum".$pPageMode."PageMode";
		$sql = "SELECT ".$pageField." as PageNum, DATE_FORMAT(DateInput,'%Y-%m-%d %H:%i:%s') as DateInput FROM INTRANET_EBOOK_BOOKMARKS WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' ";
		
		return $this->returnArray($sql);
	}
	
	function Add_Book_User_Bookmark($pBookID, $pUserID, $pPageMode, $pPageNum)
	{
		if($pPageMode == 2){
			$pageNum1PageMode = $this->Get_PageNumber_Mapping_By_PageMode($pBookID, $pPageMode, $pPageNum);
			$pageNum2PageMode = $pPageNum;
		}else{
			$pageNum1PageMode = $pPageNum;
			$pageNum2PageMode = $this->Get_PageNumber_Mapping_By_PageMode($pBookID, $pPageMode, $pPageNum);
		}
		$sql = "INSERT IGNORE INTO INTRANET_EBOOK_BOOKMARKS (BookID, UserID, PageNum1PageMode, PageNum2PageMode, DateInput) VALUES ('".$pBookID."','".$pUserID."','".$pageNum1PageMode."','".$pageNum2PageMode."',NOW())";
		return $this->db_db_query($sql);
	}
	
	function Delete_Book_User_Bookmark($pBookID, $pUserID, $pPageMode, $PageNum)
	{
		if($pPageMode == 2){
			$pageNumField = "PageNum2PageMode";
		}else{
			$pageNumField = "PageNum1PageMode";
		}
		$sql = "DELETE FROM INTRANET_EBOOK_BOOKMARKS WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' AND ".$pageNumField." = '".$PageNum."' ";
		return $this->db_db_query($sql);
	}
	
	function Get_PageNumber_Mapping_By_PageMode($pBookID, $pPageMode, $PageNum, $isImageBook=false) // 2013-06-11 Charles Ma
	{
		$book_elibinfo = $this->Get_eLib_Book_Info($pBookID);	
			
		$SubCategory = $book_elibinfo[0]['SubCategory']; //2013-09-18 Charles Ma
		$Publisher = $book_elibinfo[0]['Publisher']; 
		$engclassicMode = $Publisher=="English Classics"||$SubCategory=="English Classics";
		
		$mulFactor = $isImageBook? "":"*2";
		
		$sql = "SELECT SUM(Pages1PageMode) as OnePageModeTotal, SUM(Pages2PageMode ".$mulFactor.") as TwoPageModeTotal FROM INTRANET_EBOOK_PAGE_INFO
				WHERE BookID = '".$pBookID."' GROUP BY BookID";
			
		$pageTotals = $this->returnArray($sql);
		$total1PageMode = $pageTotals[0]['OnePageModeTotal'];
		$total2PageMode = $pageTotals[0]['TwoPageModeTotal'];
		
		if($engclassicMode){
			$outputPage = $pPageMode == 1 ? $PageNum * 2 : round ($PageNum / 2);
		}
		else if($pPageMode == 1){
			$outputPage = round($total1PageMode * ($PageNum*2 / $total2PageMode));
		}else{
			$outputPage = round(($total2PageMode * ($PageNum / $total1PageMode))/2);
		}
		return $outputPage;
	}
	
	function Get_Book_User_Notes($pBookID, $pUserID, $pPageMode, $pPageNum)
	{
		$pageModeField = "PageNum".$pPageMode."PageMode";
		$sql = "SELECT Notes, DATE_FORMAT(DateModified,'%Y-%m-%d %H:%i:%s') as DateModified FROM INTRANET_EBOOK_PAGE_NOTES WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' AND ".$pageModeField." = '".$pPageNum."' ";
		
		return $this->returnArray($sql);
	}
	
	function Get_Book_User_Page_Notes_List($pBookID, $pUserID, $pPageMode, $pPageNum='')
	{
		$pageModeField = "PageNum".$pPageMode."PageMode";
		if($pPageNum != ''){
			$page_num_cond = " AND ".$pageModeField." = '".$pPageNum."' ";
		}
		$sql = "SELECT RecordID, Notes, ".$pageModeField." as PageNumber, DATE_FORMAT(DateModified,'%Y-%m-%d %H:%i:%s') as DateModified FROM INTRANET_EBOOK_PAGE_NOTES WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' $page_num_cond";
		
		return $this->returnArray($sql);
	}
	
	function Update_Book_User_Notes($pBookID, $pUserID, $pPageMode, $pPageNum, $pNotes)
	{
		$pageModeField = "PageNum".$pPageMode."PageMode";
		
		$notes = $this->Get_Safe_Sql_Query($pNotes);
		$sql = "UPDATE INTRANET_EBOOK_PAGE_NOTES SET Notes = '".$notes."', DateModified = NOW() 
				WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' AND ".$pageModeField." = '".$pPageNum."' ";
		$success = $this->db_db_query($sql);		
		
		if($this->db_affected_rows() == 0){
			if($pPageMode == 2){
				$pageNum1PageMode = $this->Get_PageNumber_Mapping_By_PageMode($pBookID, $pPageMode, $pPageNum);
				$pageNum2PageMode = $pPageNum;
			}else{
				$pageNum1PageMode = $pPageNum;
				$pageNum2PageMode = $this->Get_PageNumber_Mapping_By_PageMode($pBookID, $pPageMode, $pPageNum);
			}
			$sql = "INSERT IGNORE INTO INTRANET_EBOOK_PAGE_NOTES (BookID, UserID, PageNum1PageMode, PageNum2PageMode, Notes, DateInput, DateModified) 
					VALUES ('".$pBookID."', '".$pUserID."', '".$pageNum1PageMode."','".$pageNum2PageMode."','".$notes."',NOW(),NOW())";
			$success = $this->db_db_query($sql);
		}
		return $success;
	}
	
	function Delete_Book_User_Notes($pBookID, $pUserID, $pPageMode, $pPageNum)
	{
		$pageModeField = "PageNum".$pPageMode."PageMode";
		$sql = "DELETE FROM INTRANET_EBOOK_PAGE_NOTES WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' AND ".$pageModeField." = '".$pPageNum."' ";
		return $this->db_db_query($sql);
	}
	
	function Update_Book_User_Notes_By_RecordID($pRecordID, $pNotes)
	{	
		$notes = $this->Get_Safe_Sql_Query($pNotes);
		$sql = "UPDATE INTRANET_EBOOK_PAGE_NOTES SET Notes = '".$notes."', DateModified = NOW() 
				WHERE RecordID = '".$pRecordID."' ";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function Delete_Book_User_Notes_By_RecordID($pRecordID)
	{
		$sql = "DELETE FROM INTRANET_EBOOK_PAGE_NOTES WHERE RecordID = '".$pRecordID."' ";
		return $this->db_db_query($sql);
	}
	
	function TransformTruePageNumberToPageIndex($PageMode, $PageNumber, $isImageBook=false)
	{
		$page_index = $PageNumber; // 1 page mode is 1 to 1 mapping
		if($PageMode == 2){
			// 2 page mode
			if(!$isImageBook){
				$PageNumber % 2;
				$page_index = floor($PageNumber / 2) + (($PageNumber % 2)==0?0:1);
			}
		}
		return $page_index;
	}
	
	function FormatDatetime($dateString)
	{
		global $Lang;
		
		$ts = strtotime($dateString);
		$format_date = date("Y-m-d",$ts);
		$format_time = date("H:i:s",$ts);
		$date_arr = explode("-",$format_date);
		$format_datetime = $format_date." ".$format_time;
		
		$today = date("Y-m-d");
		$today_arr = explode("-",$today);
		if($format_date == $today){
			$format_datetime = $Lang['eBookReader']['today']." ".$format_time;
		}else if($date_arr[0] == $today_arr[0] && $date_arr[1] == $today_arr[1] && ($today_arr[2] - $date_arr[2])==1){
			$format_datetime = $Lang['eBookReader']['yesterday']." ".$format_time;
		}
		
		return $format_datetime;
	}
	
	function Get_User_Progress($pBookID, $pUserID)
	{
		$sql = "SELECT ProgressStatus, Percentage, DATE_FORMAT(DateModified,'%Y-%m-%d %H:%i:%s') as DateModified FROM INTRANET_EBOOK_USER_PROGRESS WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' ";
		return $this->returnArray($sql);
	}
	
	function Update_User_Process($pBookID, $pUserID, $pProgressStatus, $pPercentage='')
	{
		$percentageField = ($pPercentage != '' ? ", Percentage = '".$pPercentage."' ": "");
		$sql = "UPDATE INTRANET_EBOOK_USER_PROGRESS SET ProgressStatus = '".$pProgressStatus."'".$percentageField.",DateModified = NOW() WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' ";
		$success = $this->db_db_query($sql);
		$affected_rows = $this->db_affected_rows();
		
		if($pPercentage != ''){
			// sync elib INTRANET_ELIB_USER_PROGRESS here
			$sql = "UPDATE INTRANET_ELIB_USER_PROGRESS SET Percentage = '".$pPercentage."', DateModified = NOW() WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' ";
			$elib_success = $this->db_db_query($sql);
			$elib_affected_rows = $this->db_affected_rows();
			
			if($elib_affected_rows == 0){
				$sql = "INSERT INTO INTRANET_ELIB_USER_PROGRESS (BookID,UserID,Percentage,DateModified) 
						VALUES ('".$pBookID."','".$pUserID."','".$pPercentage."',NOW())";
				$elib_success = $this->db_db_query($sql);
			}
		}
		if($affected_rows == 0){
			$percentageFieldName = $pPercentage != '' ? ",Percentage" : "";
			$percentageFieldValue = $pPercentage != '' ? ",'".$pPercentage."'" : "";
			$sql = "INSERT INTO INTRANET_EBOOK_USER_PROGRESS (BookID,UserID,ProgressStatus ".$percentageFieldName.",DateModified) 
					VALUES ('".$pBookID."','".$pUserID."','".$pProgressStatus."'".$percentageFieldValue.",NOW())";
			$success = $this->db_db_query($sql);
		}
		
		return $success;
	}
	
	// probably not used
	function Delete_User_Progress($pBookID,$pUserID)
	{
		$sql = "DELETE FROM INTRANET_EBOOK_USER_PROGRESS WHERE BookID = '".$pBookID."' AND UserID = '".$pUserID."' ";
		return $this->db_db_query($sql);
	}
	
	function Send_Recommend_Email($To, $Content, $BookID)
	{
		global $Lang, $intranet_session_language;
		include_once("libuser.php");
		include_once("imap_gamma.php");
		
		$libuser = new libuser($_SESSION['UserID']);
		$imap = new imap_gamma(true);
		
		$user_name = ($intranet_session_language =="b5" || $intranet_session_language == "gb")? $libuser->ChineseName : $libuser->EnglishName;
		$book_info = $this->Get_Book_List($BookID);
		$book_title = $book_info[0]['BookTitle'];
		
		$ToArray = explode(";",$To);
		for($i=0;$i<count($ToArray);$i++){
			$ToArray[$i] = trim($ToArray[$i]);
		}
		$Subject = $user_name.$Lang['eBookReader']['RecommendYouToRead'].'"'.$book_title.'"';
		$From = $libuser->UserEmail;
		$Content = trim($Content);
		
		$MimeMessage = $imap->Build_Mime_Text($Subject, $Content, $From, $ToArray);
		
		$Result['SendMail'] = mail($MimeMessage['to'], $MimeMessage['subject'], $MimeMessage['body'], $MimeMessage['headers2'], "-f $From");
		
		return $Result['SendMail'];
	}
	
	function Get_Book_Reviews($BookID)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		include_once("libelibrary.php");
		$libelibrary = new elibrary();
		
		$dataArr = array('BookID' => $BookID);
		$reviews = $libelibrary->getReview($dataArr);
		
		return $reviews;
	}
	
	function Add_Book_Review($pBookID, $pUserID, $pRating, $pContent)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		include_once("libelibrary.php");
		$libelibrary = new elibrary();
		
		$dataArr = array('BookID' => $pBookID,
						'UserID' => $pUserID,
						'Rating' => $pRating,
						'Content' => $this->Get_Safe_Sql_Query($pContent));
		
		$reviewID = $libelibrary->addBookReview($dataArr);
		
		return $reviewID > 0 ? true: false;
	}
	
	function Add_Book_Review_Helpful($pBookID,$pUserID,$pReviewID,$pChoose)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		include_once("libelibrary.php");
		$libelibrary = new elibrary();
		
		$dataArr = array('BookID' => $pBookID,
						'UserID' => $pUserID,
						'ReviewID' => $pReviewID,
						'Choose' => $pChoose);
		
		$reviewHelpfulID = $libelibrary->addBookReviewHelpful($dataArr);
		
		return $reviewHelpfulID > 0? true:false;
	}
	
	function Get_Book_Review_Helpful_Check($pReviewID)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		include_once("libelibrary.php");
		$libelibrary = new elibrary();
		
		return $libelibrary->getHelpfulCheck($pReviewID);
	}
	
	function Get_Book_Review_Helpful_Vote($pReviewID)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		include_once("libelibrary.php");
		$libelibrary = new elibrary();
		
		return $libelibrary->getHelpfulVote($pReviewID);
	}
	
	function Get_eLib_Book_Info($BookID)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		$sql = "SELECT * FROM INTRANET_ELIB_BOOK WHERE BookID = '".$BookID."' ";
		
		return $this->returnArray($sql);
	}
	
	function Get_eLib_TagName_By_BookID($BookID)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		include_once("libelibrary.php");
		$libelibrary = new elibrary();
		
		$BookTags = '';
		$tagName = $libelibrary->returnTagNameByBook($BookID);
		if(is_array($tagName) && sizeof($tagName)>0)
		{
			$BookTags = implode(', ', $tagName);
		}
		return $BookTags;
	}
	
	// 20131213-eBookTW Charles Ma
	function Validate_CSV_ImportEPUB($filepath, $status){
		global $intranet_root;
		include_once('libimporttext.php');
		
		$tmp_path = $intranet_root.$this->EBOOK_FILE_PATH."/upload_files";
		$dest = $tmp_path."/upload.csv";		
		
		if(!file_exists($tmp_path) || !is_dir($tmp_path)){		
			mkdir($tmp_path);
		}
		chmod($tmp_path, 0777); 
		
		if(!file_exists($tmp_path) || !is_dir($tmp_path)){		
			return -1;
		}else if ($filepath){			
			//file_put_contents($tmp_path."/upload.csv", $filepath);
			exec("mv ".OsCommandSafe($filepath)." ".OsCommandSafe($dest)); 
			chmod($dest, 0777); 			
		}		
				
		if(!file_exists($dest)){		
			return -1;
		}else if ((!$filepath && $status == "new")){
			return -3;
		}
		$limport = new libimporttext();
		$data = @$limport->GET_IMPORT_TXT($dest);	
			
		
		// Step 1a : Pop out the first row to validate the CSV file
		$col_array = array_shift($data);			
		$del = "|";
		$file_format = "BookID $del BookTitle $del Author $del" .
				" SeriesEditor $del Category $del SubCategory $del" .
				" Tags1 $del Tags2 $del Level $del Publisher $del" .
				" Language $del FirstPublished $del Edition $del" .
				" ePublisher $del Preface $del ISBN $del IsFirstPageBlank $del IsRightToLeft";
		$file_format = str_replace(" ", "", $file_format);
		
		if(strcmp(current($col_array),$file_format) != 0){
			
			return -2;
		}
		
		//	Step 1b : remove the chinese row of CSV file
		$col_array_chi = array_shift($data);
		//$result = array();
		
		$temp_sql = "";
		foreach($data as $key => $row){
			
			$current_array = explode("|" , current($row)); 
			foreach($current_array as $key=>$value){
				$current_array[$key] = addslashes($value);
			}
			list($BookID,$BookTitle,$Author,$SeriesEditor,$Category,$SubCategory,$Tag1,$Tag2,$Level,$Publisher,$Language,$FirstPublished,$Edition,$ePublisher,$Preface,$ISBN,$IsFirstPageBlank,$IsRightToLeft)	= $current_array;
			$BookID = str_replace("Y","",$BookID);
			$BookID = $BookID."/";
			
			$ISBN = strlen($ISBN) == 10 ? $ISBN : (string) floatval($ISBN);
						
			if($temp_sql != "") $temp_sql .= " , ";
			
			$temp_sql .= " ( '$BookID', '$BookTitle', '$Author', '$SeriesEditor'" .
					" , '$Category', '$SubCategory', '$Tag1', '$Tag2', '$Level'" .
					" , '$Publisher', '$Language', '$FirstPublished'" .
					" , '$Edition', '$ePublisher', '$Preface', '$ISBN', '$IsFirstPageBlank' , '$IsRightToLeft'  ) ";			
		}
		
		if(sizeof($data) > 0){
			$sql = " CREATE TEMPORARY TABLE INTRANET_TEMP_EBOOK_IMPORT (" .
					" BookSubFolder varchar(255) default NULL, " .
					" BookTitle varchar(255) default NULL, " .
					" Author varchar(50) default NULL, " .
					" SeriesEditor varchar(50) default NULL, " .
					" Publisher varchar(100) default NULL, " .
					" ISBN varchar(100) default NULL, " .
					" FirstPublished varchar(50) default NULL, " .
					" PublishVersion varchar(50) default NULL, " .
					" Language varchar(5) default NULL, " .
					" Preface text, " .
					" Category varchar(100) default NULL, " .
					" SubCategory varchar(100) default NULL, " .
					" Tag1 varchar(50) default NULL, " .
					" Tag2 varchar(50) default NULL, " .
					" Level varchar(5) default NULL, " .
					" ePublisher varchar(100) default NULL, " .
					" IsFirstPageBlank char(2) default '0', " .
					" IsRightToLeft char(2) default '0' " .
					")  ENGINE=InnoDB DEFAULT CHARSET=utf8 ";
			$success = $this->db_db_query($sql);
			if($success){
				$sql = " INSERT INTO INTRANET_TEMP_EBOOK_IMPORT ( BookSubFolder,BookTitle" .
						",Author,SeriesEditor,Category,SubCategory,Tag1," .
						" Tag2,Level,Publisher,Language,FirstPublished,PublishVersion,ePublisher,Preface,ISBN, IsFirstPageBlank, IsRightToLeft ) VALUES ";
				$sql .= $temp_sql;
								
				$success = $this->db_db_query($sql);
				return 1;
			}
		}
		return 0;
	}
	
	
  function Update_ImportEPUB()
  {     
    global $intranet_root, $PATH_WRT_ROOT, $Lang; 
    include_once("libfilesystem.php");
    include_once($PATH_WRT_ROOT."includes/libimage.php");
    include_once($PATH_WRT_ROOT."includes/libinterface.php");
  
    $lf = new libfilesystem();
    
    
    $tmp_path = $intranet_root.$this->EBOOK_FILE_PATH;
    
    $success = array();
    
    $success["testing"] = false;
    $msg = "";
    
    $msg .= 'Start:       '. date('Y-m-d h:i:s') ."<br/><br/>";
    
    // Step 1 : insert and update items
    
    $inputby = $_SESSION['UserID'];
    
    // Step 1.a : Retrieve all subFolder code of new and existed ebook
    $sql = " SELECT CONCAT('\'', tei.BookSubFolder, '\'') FROM INTRANET_TEMP_EBOOK_IMPORT as tei " .
        " LEFT JOIN INTRANET_EBOOK_BOOK as eb ON tei.BookSubFolder = eb.BookSubFolder WHERE eb.BookID != ''  ";
    
    $update_items = $this->returnVector($sql);
    $update_items_str = implode(",", $update_items);
        
    
    $sql = " SELECT CONCAT('\'', tei.BookSubFolder, '\'') FROM INTRANET_TEMP_EBOOK_IMPORT as tei " .
        " LEFT JOIN INTRANET_EBOOK_BOOK as eb ON tei.BookSubFolder = eb.BookSubFolder WHERE eb.BookID = ''  ";
    
    $new_items = $this->returnVector($sql);
    $new_items_str = implode(",", $new_items);    
    
    // Step 1.b : insert into INTRANET_EBOOK_BOOK 
    
    $sql = " INSERT INTO INTRANET_EBOOK_BOOK (BookTitle,BookSubFolder,RecordStatus," .
        " DateInput,InputBy,ForceToOnePageMode,OnePageModeWidth,OnePageModeHeight,InputFormat,ImageBook,ImageWidth) " .
        " SELECT tei.BookTitle, tei.BookSubFolder , '1' as RecordStatus, " .
        " NOW() as DateInput, '".$inputby."' as InputBy," .
        " '0' as ForceToOnePageMode, '0' as OnePageModeWidth, '0' as OnePageModeHeight," .
        " 4 as InputFormat, 1 as ImageBook, 0 as ImageWidth  FROM INTRANET_TEMP_EBOOK_IMPORT as tei " .
        " LEFT JOIN INTRANET_EBOOK_BOOK as eb ON tei.BookSubFolder = eb.BookSubFolder " .
        " WHERE eb.BookID IS NULL " ;   
        
    if(!$success["testing"])$success["insert_item"] = array($this->db_db_query($sql), mysql_affected_rows());   
        
    // Step 1.c : update BookPath for new items
    $sql = " UPDATE INTRANET_EBOOK_BOOK Set BookPath = CONCAT('/file/eBook/', BookID) " .
        " Where ( BookPath = '' OR BookPath IS NULL ) AND InputFormat = '4' ";
    if(!$success["testing"])$success["update_bookpath"] = array($this->db_db_query($sql), mysql_affected_rows());
    
    // Step 1.d : insert into INTRANET_ELIB_BOOK_AUTHOR  
    $sql = " INSERT INTO INTRANET_ELIB_BOOK_AUTHOR (Author, DateModified, Description, InputBy) " .
        " SELECT tei.Author, NOW() as DateModified, '' as Description, '".$inputby."' as InputBy FROM INTRANET_TEMP_EBOOK_IMPORT as tei " .
        " LEFT JOIN INTRANET_ELIB_BOOK_AUTHOR as eba ON tei.Author = eba.Author " .
        " WHERE AuthorID IS NULL ";   
    
    if(!$success["testing"])$success["insert_author"] = array($this->db_db_query($sql), mysql_affected_rows());
    
    // Step 1.e : update INTRANET_ELIB_TAG  and insert into INTRANET_ELIB_BOOK_TAG  
    
    $sql = " INSERT INTO INTRANET_ELIB_TAG (TagName, DateInput, InputBy) " .
        " SELECT DISTINCT tei.Tag1 as TagName, NOW() as DateInput, '".$inputby."' as InputBy FROM INTRANET_TEMP_EBOOK_IMPORT as tei " .
        " LEFT JOIN INTRANET_ELIB_TAG as et ON tei.Tag1 = et.TagName " .
        " WHERE TagID IS NULL AND tei.Tag1 != '' " ;  
      if(!$success["testing"])$success["insert_tag1"] = array($this->db_db_query($sql), mysql_affected_rows());   
    
    $sql =  " INSERT INTO INTRANET_ELIB_BOOK_TAG (BookID, TagID, DateInput, InputBy) " . 
          " SELECT eb.BookID as BookID, et.TagID as TagID, NOW() as DateInput, '".$inputby."'as InputBy FROM INTRANET_TEMP_EBOOK_IMPORT as tei " .
          " INNER JOIN INTRANET_ELIB_TAG as et ON tei.Tag1 = et.TagName " .
          " INNER JOIN INTRANET_EBOOK_BOOK as eb ON tei.BookSubFolder = eb.BookSubFolder " .
          " ON DUPLICATE KEY UPDATE BookID = VALUES (BookID)" ;
    
    if(!$success["testing"])$success["insert_book_tag1"] = array($this->db_db_query($sql), mysql_affected_rows());  
    
    $sql = " INSERT INTO INTRANET_ELIB_TAG (TagName, DateInput, InputBy) " .
        " SELECT DISTINCT tei.Tag2 as TagName, NOW() as DateInput, '".$inputby."' as InputBy FROM INTRANET_TEMP_EBOOK_IMPORT as tei " .
        " LEFT JOIN INTRANET_ELIB_TAG as et ON tei.Tag2 = et.TagName " .
        " WHERE TagID IS NULL AND tei.Tag2 != '' " ;        
    
    if(!$success["testing"])$success["insert_tag2"] = array($this->db_db_query($sql), mysql_affected_rows());   
    
    $sql =  " INSERT INTO INTRANET_ELIB_BOOK_TAG (BookID, TagID, DateInput, InputBy) " . 
          " SELECT eb.BookID as BookID, et.TagID as TagID, NOW() as DateInput, '".$inputby."'as InputBy FROM INTRANET_TEMP_EBOOK_IMPORT as tei " .
          " INNER JOIN INTRANET_ELIB_TAG as et ON tei.Tag2 = et.TagName " .
          " INNER JOIN INTRANET_EBOOK_BOOK as eb ON tei.BookSubFolder = eb.BookSubFolder " .
          " ON DUPLICATE KEY UPDATE BookID = VALUES (BookID)" ;
    
    if(!$success["testing"])$success["insert_book_tag2"] = array($this->db_db_query($sql), mysql_affected_rows());  
    
    
    //Step 1.f : insert / update INTRANET_ELIB_BOOK     
    
    $sql = " INSERT INTO INTRANET_ELIB_BOOK " .
        " (BookID, Title,Author,AuthorID,Publisher,Preface,InputBy,DateModified," .
        " Language,Category,Level,SubCategory,SeriesEditor,Publish,ISBN,ePublisher,Edition, BookFormat, IsFirstPageBlank, IsRightToLeft ) ";
    
      $sql .= " SELECT eb.BookID, tei.BookTitle as Title, tei.Author as Author, " .
        " eba.AuthorID as AuthorID, tei.Publisher as Publisher, " .
        " tei.Preface as Preface, '".$inputby."' as InputBy, " .
        " NOW() as DateModified," .
        " CASE tei.Language WHEN '繁體中文' THEN 'chi' WHEN '英文' THEN 'eng' END AS Language, " .
        " tei.Category as Category, tei.Level as Level, tei.SubCategory, tei.SeriesEditor, " .
        " 1 as Publish,tei.ISBN as ISBN, tei.ePublisher as ePublisher, tei.PublishVersion as Edition , 'ePub2' as BookFormat, IsFirstPageBlank, IsRightToLeft " .
        " FROM INTRANET_TEMP_EBOOK_IMPORT as tei " .
        " INNER JOIN INTRANET_EBOOK_BOOK as eb ON tei.BookSubFolder = eb.BookSubFolder " .
        " LEFT JOIN INTRANET_ELIB_BOOK_AUTHOR as eba ON tei.Author = eba.Author ";
    
    $sql .= " ON DUPLICATE KEY UPDATE Title = VALUES (Title), " .
        " Author = VALUES (Author),AuthorID = VALUES (AuthorID),Publisher = VALUES (Publisher)," .
        " Preface = VALUES (Preface),InputBy = VALUES (InputBy)," . //DateModified = VALUES (DateModified),
        " Language = VALUES (Language),Category = VALUES (Category),Level = VALUES (Level),SubCategory = VALUES (SubCategory)," .
        " SeriesEditor = VALUES (SeriesEditor),ISBN = VALUES (ISBN),ePublisher = VALUES (ePublisher),Edition = VALUES (Edition), BookFormat = 'ePub2', " .
        " IsFirstPageBlank = VALUES (IsFirstPageBlank), IsRightToLeft = VALUES (IsRightToLeft) ";
    
    if(!$success["testing"])$success["insert_elib_book"] = array($this->db_db_query($sql), mysql_affected_rows());  
    
    //Step 2.a : retrieve current imported book id
    
    $sql = " SELECT eb.BookID as BookID, eb.BookSubFolder as BookSubFolder, tei.BookTitle as BookTitle " .
        " FROM INTRANET_TEMP_EBOOK_IMPORT as tei " .
        " INNER JOIN INTRANET_EBOOK_BOOK as eb ON tei.BookSubFolder = eb.BookSubFolder ";
    
    $items = $this->returnArray($sql);  
    
    //Step 2.b : move file to corresponding book file 
          
    $ebook_file_path = $intranet_root.$this->EBOOK_FILE_PATH;
    
    
    foreach($items as $i => $value){
      $BookID = $value['BookID'];
      $RefID = str_replace("/","", $value['BookSubFolder']);        
      
      $success[$value['BookID']]['testing'] = true;     
      $book_zip_path = $ebook_file_path."/upload_files/".$RefID.".zip";
      $upload_files_path = $ebook_file_path."/upload_files";
      $unzip_book_file_path = $ebook_file_path."/upload_files/".$RefID."";
      $book_file_path = $ebook_file_path."/".$BookID."/".$RefID;
      $book_path = $ebook_file_path."/".$BookID;
      
//      debug_r(array($book_zip_path, $upload_files_path, $unzip_book_file_path, $book_file_path, $book_path));
      
      if(file_exists($book_file_path) && is_dir($book_file_path) && $BookID){
        //Step 2.c : delete previous directory
        if(!$success["testing"])$success[$value['BookID']]['delete_previous_directory'] =$lf->deleteDirectory($book_file_path);
      }
      if(!file_exists($book_file_path) || !is_dir($book_file_path)){
        //Step 2.d : create new directory
        if(!$success["testing"])$success[$value['BookID']]['create_new_directory'] =$lf->createFolder($book_file_path);       
      }
      
      if(!file_exists($book_zip_path)){
        if(!$success["testing"])$success[$value['BookID']]['zip_file_exist'] = false; 
      }
      
      if(file_exists($book_file_path) && file_exists($book_zip_path)){
        $success[$value['BookID']]['zip_file_exist'] = true;  
        //Step 2.e : unzip file complete
        if(!$success["testing"]){         
          $dirNow = getcwd(); 
          $save_arr  = array();           
          chdir($upload_files_path);
          exec("7za x ".OsCommandSafe($RefID).".zip -o".OsCommandSafe($book_file_path)."/images images* ", $save_arr);  
          exec("7za x ".OsCommandSafe($RefID).".zip -o".OsCommandSafe($book_file_path)."/mp3 mp3* ", $save_arr);    
          exec("7za x ".OsCommandSafe($RefID).".zip -o".OsCommandSafe($book_file_path)."/text text* ", $save_arr);  
                        
          chdir($book_file_path."/images/");
          chmod($book_file_path."/images/", 0777); 
          $c_images = glob("*");    
          if(current($c_images) == "images"){
            chdir($book_file_path."/images/images/");
            $c_images = glob("*");
            foreach($c_images as $key=>$value){
              exec("mv ".OsCommandSafe($value)." ../");
            }
          }else{
            foreach($c_images as $key=>$value){
              $new_value = str_replace("images\\", "", $value);
              rename($value, $new_value);
            }
          }
          
          
          if(file_exists($book_file_path."/mp3/")){           
            chdir($book_file_path."/mp3/");   
            chmod($book_file_path."/mp3/", 0777);         
            $c_mp3 = glob("*");
            if(current($c_mp3) == "mp3"){
              chdir($book_file_path."/mp3/mp3/");
              $c_mp3 = glob("*");
              foreach($c_mp3 as $key=>$value){
                exec("mv ".OsCommandSafe($value)." ../");
              }
            }
            else{   
              foreach($c_mp3 as $key=>$value){
                $new_value = str_replace("mp3\\", "", $value);
                rename($value, $new_value);
              }             
            } 
          }
          
          chdir($book_file_path."/text/");
          chmod($book_file_path."/text/", 0777); 
          $c_text = glob("*");
          
          if(current($c_text) == "text"){
            chdir($book_file_path."/text/text/");
            $c_text = glob("*");
            foreach($c_text as $key=>$value){
              exec("mv ".OsCommandSafe($value)." ../");
            }
          }else{
            foreach($c_text as $key=>$value){
              $new_value = str_replace("text\\", "", $value);
              rename($value, $new_value);
            }
          }
          
          chdir($dirNow);
        }
        //$success[$value['BookID']]['unzip_file_complete'] =  $lf->unzipFile($book_zip_path,$book_file_path);
        
        $outline_path = $ebook_file_path."/".$BookID."/".$RefID."/text/".$RefID."_outline.txt";
        $content_path = $ebook_file_path."/".$BookID."/".$RefID."/text/".$RefID."_content.txt";
        $new_outline_path = $ebook_file_path."/".$BookID."/".$RefID."/".$RefID."_outline.txt";
        $new_content_path = $ebook_file_path."/".$BookID."/".$RefID."/".$RefID."_content.txt";
        
        if( file_exists($outline_path)){  
          
          $success[$value['BookID']]['move_outline'] = $lf->lfs_move($outline_path, $new_outline_path);         
          $success[$value['BookID']]['move_content'] = $lf->lfs_move($content_path, $new_content_path);
          
          if($success[$value['BookID']]['move_outline'] && file_exists($new_outline_path))          
            $this->GET_NEW_TABLE_OF_CONTENT($BookID, $RefID);
          
          $text_zip_path = $ebook_file_path."/".$BookID."/".$RefID."/text/".$RefID.".zip";  
          $new_text_zip_path = $ebook_file_path."/".$BookID."/".$RefID."/".$RefID.".zip";   
          $text_file_path = $ebook_file_path."/".$BookID."/".$RefID."/text/"; 
          $text_zip_name = $RefID.".zip";
          
          if(file_exists($text_zip_path)){               
             $dirNow = getcwd();  
             $save_arr  = array();  
             chdir($text_file_path);             
             $success[$value['BookID']]['text_file_change_mode'] = chmod($text_zip_name,0777);             
             exec("7za x ".OsCommandSafe($text_zip_name), $save_arr);               
             chdir($dirNow);             
             $success[$value['BookID']]['move_zip'] = $lf->lfs_move($text_zip_path, $new_text_zip_path);            
          }
          
          $text = glob($ebook_file_path."/".$BookID."/".$RefID."/text/" . "*");
          usort($text, "cmp");      
          $count_text = count($text);
          
          $sql = " DELETE FROM INTRANET_EBOOK_PAGE_INFO WHERE BookID = '$BookID'  ";
          // Step 2.f : delete prev pages
          if(!$success["testing"])$success[$value['BookID']]['delete_prev_pages'] = $this->db_db_query($sql);
                    
          $page_sql = "INSERT INTO INTRANET_EBOOK_PAGE_INFO (Content,BookID,RefID,FileName,Pages1PageMode,Pages2PageMode) VALUES ";
          $page_values = "";
          $delimiter = "";
          
          if($count_text>0 && !$success["testing"]){
            for($k =2;$k <=$count_text ; $k++ ){
              $current_file = explode("/",$text[$k-1]);
              $file = file($text[$k-1]);
              $current_content = "";
              $z = 0;
              foreach ($file as $line){
                $z++; if($z > 1000) break;
                $line = trim($line);
                $current_content .= $line;
              }
              $current_content = addslashes($current_content);
              
              $refid = $current_file[count($current_file)-1];
              $href = $current_file[count($current_file)-2]."/".$current_file[count($current_file)-1];
              $page_values .= $delimiter."('".$current_content."','".$BookID."','".$refid."','".$href."'";
              $page_values .= ",'1','1'";             
              $page_values .= ")";
              $delimiter = ",";
            }
            $page_sql .= $page_values;
            // Step 2.g : insert pages
            if(!$success["testing"])$success[$value['BookID']]['insert_pages'] = $this->db_db_query($page_sql);
          }
          $this->CoverImagePath = $this->EBOOK_FILE_PATH."/".$BookID."/Images/cover.jpg";
          $this->CoverImageName = "Images/cover.jpg"; 
          
          $filepath_abs = $intranet_root.$this->EBOOK_FILE_PATH."/".$BookID."/".$RefID."/images/".$RefID."_0001.jpg";
          
          $image_obj = new SimpleImage();
          if (is_file($filepath_abs))
          {
            $imagesize = getimagesize($filepath_abs);         
            
            # copy and resize 
            $photo_des = $intranet_root."/file/elibrary/content/".$BookID."/image";
            if (!is_file($photo_des))
              $lf->createFolder($photo_des);
            
            $image_obj->load($filepath_abs);
            
            $height = $imagesize[1];
            $width = $imagesize[0];
            $ImageWidth = (int)(($imagesize[0]*650) / $imagesize[1]) ;
            
            if ($imagesize[1]>220 && $image_obj->IsGDReady())
            {
               $image_obj->resizeToMax(163, 220);       
            }           
            $image_obj->save($photo_des."/cover.jpg");
            
            $sql = " UPDATE INTRANET_EBOOK_BOOK SET RecordStatus = 1, ImageWidth = '$ImageWidth' WHERE BookID = '$BookID' ";
            if(!$success["testing"])$success[$value['BookID']]['update_ebook_book'] = $this->db_db_query($sql);
          }           
                    
          // Step 2.h : resize image and rename mp3
          if(!$success["testing"]){         
//            $image_arr = glob($intranet_root.$this->EBOOK_FILE_PATH."/".$BookID."/".$RefID."/images/" . "*"); 
//            foreach($image_arr as $index => $value){            
//              $imagesize = getimagesize($value);
//              $width = $imagesize[0];
//              $height = $imagesize[1];
//              $newwidth = 1000;
//              if($width > $newwidth){   
//                $newheight = ($imagesize[1] * $newwidth) / $width;          
//                $image = imagecreatefromjpeg($value);
//                $imagenew = imagecreatetruecolor($newwidth, $newheight);
//                imagecopyresampled($imagenew, $image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
//                imagejpeg($imagenew, $value);
//                imagedestroy($image);
//                imagedestroy($imagenew);
//              }     
//            }
            $mp3_arr = glob($intranet_root.$this->EBOOK_FILE_PATH."/".$BookID."/".$RefID."/mp3/" . "*");            
            foreach($mp3_arr as $index => $value){            
              $file_name_encoding = DetectDataEncoding($value);
              $new_file_name = iconv($file_name_encoding, 'UTF8', $value);
              if ($new_file_name != '') {
                rename($value, $new_file_name);
              }       
            }
          }       
        }       
      }
    }
        
    $br = "<br/><br/>";
    
    $msg .= $success["insert_item"] && $success["update_bookpath"] && $success["insert_elib_book"] ? " Update Database : success ".$br : " Update Database : fail ".$br;
    
    $linterface = new interface_html();
    foreach($items as $k=>$value){
      $BookID = $value['BookID'];
      $BookTitle = $value['BookTitle'];
      $book_success = !in_array(false,$success[$BookID]);
      
      $msg .= $BookTitle." : ";
      if($book_success){
        $msg .= " Import success. ".$br;
      }else{        
        $msg .= !$success[$value['BookID']]['zip_file_exist'] ? " ZIP Folder Fail; " : "";
        $msg .= !$success[$value['BookID']]['unzip_file_complete'] ? " File Decompression Fail; " : "";
        $msg .= !$success[$value['BookID']]['insert_pages'] ? " Page Insertion Fail; " : "";
        $msg .= !$success[$value['BookID']]['update_ebook_book'] ? " Data Update Fail; " : "";
      }
      $msg .= $br ;
    }
    $msg .= 'END:       '. date('Y-m-d h:i:s') ."<br/>";
    echo $linterface->Get_Warning_Message_Box($Lang['StaffAttendance']['ImportResult'], $msg);
    
    return !in_array(false,$success);
  }
	
	function retrieveReportBook(){
		global $eLib;	
		
		$publisher_html .= "<option value =''>All publishers</option>";
		
		foreach($eLib['Source'] as $key => $value){
			if($key != "-")
			$publisher_html .= "<option value ='$key'>$value</option>";
		}		
		
		$html = "<select id='publisher'>$publisher_html</select>";
		
		$html .= "<button id='generate_btn' type='button'>Generate</button>";
		
		return $html;
	}
	
  function retrieveCompressBook(){
    global $intranet_root;  
    $sql = " SELECT BookID, BookSubFolder, BookTitle FROM INTRANET_EBOOK_BOOK WHERE InputFormat = 4 ";
    
    $items = $this->returnArray($sql);  
    
    $html = '';
    
    $html .= '<tr>
        <td class="formfieldtitle" align="left"></td>
        <td class="tabletext"><?=$csv_format?></td>
      </tr>';
    
    foreach( $items as $index => $item ){
      $BookID = $item["BookID"];
      $RefID = $item["BookSubFolder"];
      $BookTitle = $item["BookTitle"];
      
      $html .= '<tr><td class="formfieldtitle" align="left">'.$BookID.' : '.$BookTitle.'</td>';
      
      $image_arr = glob($intranet_root.$this->EBOOK_FILE_PATH."/".$BookID."/".$RefID."images/" . "*");  
      
      $random_page = rand(2,sizeof($image_arr)-1);
      
      $imagesize = getimagesize($image_arr[$random_page]);
      
      $width = $imagesize[0];
      
      $bookclass =  $width > 1000 ? "Compress" : "Finish";
      $status = $width > 1000 ? '<a href="javascript: void(0)" >Compress</a>' : "Finish";
      
      $html .= '<td  style="min-width: 120px;" id="book_'.$BookID.'" class="book'.$bookclass.'" value="'.$BookID.'">'.$status.'</td>';
      $html .= '</tr>';
    } 
    
    return $html;
  }
	
  function compressBookImage($BookID){
    global $intranet_root;  
    $sql = " SELECT BookID, BookSubFolder FROM INTRANET_EBOOK_BOOK WHERE InputFormat = 4 AND BookID = '$BookID' ";
    
    $items = $this->returnArray($sql);  
    
    $item = current($items);
    
      $BookID = $item["BookID"];
      $RefID = $item["BookSubFolder"];
      
      $image_arr = glob($intranet_root.$this->EBOOK_FILE_PATH."/".$BookID."/".$RefID."images/" . "*");  
      
      if(sizeof($image_arr) < 0) return array($BookID,-1);
      
      foreach($image_arr as $index => $value){  
        if(!is_file($value)) continue;          
        $imagesize = getimagesize($value);
        $width = $imagesize[0];
        $height = $imagesize[1];
        $newwidth = 1000;
        if($width > $newwidth){   
          $newheight = ($imagesize[1] * $newwidth) / $width;          
          $image = imagecreatefromjpeg($value);
          $imagenew = imagecreatetruecolor($newwidth, $newheight);
          imagecopyresampled($imagenew, $image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
          imagejpeg($imagenew, $value);
          imagedestroy($image);
          imagedestroy($imagenew);
        }     
      }
      
      return array($BookID,1);    
  }
						
	function chineseToUnicode($str){
	    //split word
	    preg_match_all('/./u',$str,$matches);
	
	    $c = "";
	    foreach($matches[0] as $m){
	    	
	            $c .= "&#".base_convert(bin2hex(iconv('UTF-8',"UCS-4",$m)),16,10);
	    }
	    return $c;
	}
	
		
	function retrieveMP3Path($BookID){
		global $intranet_root;
		$sql = " SELECT BookSubFolder FROM INTRANET_EBOOK_BOOK WHERE BookID = '$BookID' ";
		$RefID = current($this->returnVector($sql));
		
		if($RefID){
			$book_info = $this->Get_Book_List($BookID);			
			$book_elibinfo =  $this->Get_eLib_Book_Info($BookID);		
			$ISBN = $book_elibinfo[0]['ISBN'];		
			$BookPath = $book_info[0]['BookPath'];
			$SubFolder = trim($book_info[0]['BookSubFolder']);
			$Title = $book_elibinfo[0]['Title'];
			$SubFolder = str_replace("/", "",$SubFolder);
			
			$base_path = $intranet_root.$BookPath.($SubFolder!=''?("/".$SubFolder) : "");
			
			$mp3_src_arr = glob($base_path."/mp3/" . "*");
			
			$html = "";			
			$html .= "<li><audio id='mp3_player' controls> <source id='mp3_src' type='audio/mp3' />Your browser does not support the audio element.</audio></li>";
			
			$replace_arr = array($SubFolder, $ISBN,$Title,"-", ".mp3");
			
			for($i=0;$i<sizeof($mp3_src_arr);$i++){
				$index = $i + 1;
				
				$filename = explode("/", $mp3_src_arr[$i]);
				$filename =($filename[sizeof($filename)-1]) ;	//rawurlencode
				
				$T_html = "/file/eBook/".$BookID."/".$SubFolder."/mp3/$filename";
				
				if(stripos($filename,".mp3") > 0)			
					$html .= "<li class='mp3_item' src='".$T_html."'><div><a href='javascript:void(0)'>".str_replace($replace_arr,"",$filename)."</a></div></li>"; 
				
			}
			
			return $html;
		}		
		return "";
		
	}
}

?>