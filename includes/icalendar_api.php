<?php
#editing : 
/********************************** NOTES ************************************
 * CALENDAR_CALENDAR.CalType: 
 * 		0 - Personal/General Calendar, 
 * 		1 - School Calendar, 
 * 		2 - Group Calendar, 
 * 		3 - (eClass DB) Course Calendar, 
 * 		4 - Homework, 
 * 		5 - Enrollment User Activity , 
 * 		6 - Enrollment Group Activity, 
 * 		7 - Detention
 * 		8 - Timetable
 * CALENDAR_CALENDAR_VIEWER.Access: Access level 
 * 		A : owner all permission, W : read and write, R : Read only
 * 
 * IP25 INTRANET_EVENT RecordType
 * 		0 - School Events
 * 		1 - Academic Events
 * 		2 - Group Events
 * 		3 - Public Holidays
 * 		4 - School Holidays
 ****************************************************************************/
/*
 * 2019-05-09 (Carlos): Added class attribute $CurrentUserID and its getter/setter, constructor can assign the UserID to make all methods stick to the current user id.
 * 2019-01-16 (Anna): modified getAllSchoolEvent Display event english title/location/nature if have [#N145816]
 * 2017-02-15 (Villa): #U113092 getRelatedUserActivity- fix wrong field in sql
 * 2017-01-24 (Carlos): Modified GetCalenderEventsForCheckingAvailability() only check participant user list. 
 * 2016-09-22 (Carlos): Added GetCalenderEventsForCheckingAvailability(), GetEnrolmentEventsForCheckingAvailability(), GetDetentionEventsForCheckingAvailability(), GetTimetableLessonsForCheckingAvailability().
 * 2016-06-24 (Catherine): Replace split() with explode() for PHP 5.4
 * 2015-06-24 (Carlos): Added DateModified to getAllSchoolEvent(), getRelatedHomeworkEvent(), getRelatedAllEnrolActivity(), getAllDetentionEvent(), 
 * 						CreateTempEventTable(), getRelatedGroupActivity(), getRelatedUserActivity(), formatExternalEvent()
 * 2015-02-27 (Carlos): Fix getAllSchoolEvent() use temp table approach fail to insert bug
 * 2014-10-28 (carlos) [ip2.5.5.10.1]: Modified getSchoolEventDetail(), use INTRANET_EVENT.InputBy as creator id, if null use '0'
 * 2014-10-08 (Carlos) [ip2.5.5.10.1]: Modified getAllSchoolEvent(), add INTRANET_EVENT.ModifyBy as UserID
 */
include_once("libdb.php");
include_once("lib.php");
include_once('libcal.php');
include_once('libcalevent.php');
include_once('libcalevent2007a.php');
include_once('liblocation.php');
include_once('libhomework.php');
include_once('libhomework2007a.php');
if (!isset($plugin["iCalendarEditByOwner"])){
	$plugin["iCalendarEditByOwner"]  = true;
}
class icalendar_api extends libdb{
	var $Public_Calendar;
	var $Group_Event;
	var $School_Event;
	var $School_Holiday;
	var $Academic_Event;
	var $Homework;
	var $Enrollment;
	var $Detention;
	
	var $CurrentUserID;
	
	function icalendar_api($parUserID=null){
		global $Lang, $eEnrollmentMenu, $i_Discipline_System_Access_Right_Detention ;
		$this->libdb();
		$this->Public_Calendar["color"] = "b00408";
		$this->Group_Event["color"] = "e97e0b";
		$this->School_Event["color"] = "8ad00f";
		$this->School_Holiday["color"] = "8c66d9";
		$this->Academic_Event["color"] = "3ea6ff";
		$this->Homework["color"] = "09759d";
		$this->Enrollment["color"] = "7f4b21";
		$this->Detention["color"] = "65979b";
		
		$this->Public_Calendar["name"] = $Lang['SysMgr']['SchoolCalendar']['EventType'][3];
		$this->Group_Event["name"] = $Lang['SysMgr']['SchoolCalendar']['EventType'][2];
		$this->School_Event["name"] = $Lang['SysMgr']['SchoolCalendar']['EventType'][0];
		$this->School_Holiday["name"] = $Lang['SysMgr']['SchoolCalendar']['EventType'][4];
		$this->Academic_Event["name"] = $Lang['SysMgr']['SchoolCalendar']['EventType'][1];
		$this->Homework["name"] = $Lang['SysMgr']['Homework']['HomeworkList'];
		$this->Enrollment["name"] = $eEnrollmentMenu['activity'];
		$this->Detention["name"] = $i_Discipline_System_Access_Right_Detention;
		
		$uid = $parUserID? $parUserID : $_SESSION["UserID"];
		
		$this->setCurrentUserID($uid);
		
		$this->Public_Calendar["setting"] = "PublicCalVisible";
		$this->Group_Event["setting"] = "GroupCalVisible";
		$this->School_Event["setting"] = "SchoolEventCalVisible";
		$this->School_Holiday["setting"] = "SchoolHolidayCalVisible";
		$this->Academic_Event["setting"] = "AcademicCalVisible";
		$this->Homework["setting"] = "HomeworkCalVisible";
		$this->Enrollment["setting"] = "EnrollmentCalVisible";
		$this->Detention["setting"] = "DetentionCalVisible";
		
		$this->Public_Calendar["DivClass"] = "PublicCal";
		$this->Group_Event["DivClass"] = "GroupCal";
		$this->School_Event["DivClass"] = "SchoolEventCal";
		$this->School_Holiday["DivClass"] = "SchoolHolidayCal";
		$this->Academic_Event["DivClass"] = "AcademicCal";
		$this->Homework["DivClass"] = "HomeworkCal";
		$this->Enrollment["DivClass"] = "EnrollmentCal";
		$this->Detention["DivClass"] = "DetentionCal";
		
		$sql = "select Setting,Value 
				from CALENDAR_USER_PREF 
				where UserID = '$uid' And Setting in (
				'".$this->Public_Calendar["setting"]."',
				'".$this->Group_Event["setting"]."',
				'".$this->School_Event["setting"]."',
				'".$this->Academic_Event["setting"]."',
				'".$this->School_Holiday["setting"]."',				
				'".$this->Homework["setting"]."',
				'".$this->Enrollment["setting"]."',
				'".$this->Detention["setting"]."'
				)";
		$result = $this->returnArray($sql);
		
		foreach($result as $r){
			switch (trim($r['Setting'])){
				case $this->Public_Calendar["setting"]: 
				$this->Public_Calendar["visible"] = $r["Value"];
				break;
				case $this->Group_Event["setting"]: 
				$this->Group_Event["visible"] = $r["Value"];
				break;
				case $this->School_Event["setting"]: 
				$this->School_Event["visible"] = $r["Value"];
				break;
				case $this->School_Holiday["setting"]: 
				$this->School_Holiday["visible"] = $r["Value"];
				break;
				case $this->Academic_Event["setting"]:
				$this->Academic_Event["visible"] = $r["Value"];
				break;	
				case $this->Homework["setting"]:
				$this->Homework["visible"] = $r["Value"];
				break;	
				case $this->Enrollment["setting"]:
				$this->Enrollment["visible"] = $r["Value"];
				break;
				case $this->Detention["setting"]:
				$this->Detention["visible"] = $r["Value"];
				break;
			};
		}			
		$this->Public_Calendar["recordType"] = 3;
		$this->Group_Event["recordType"] = 2;
		$this->School_Event["recordType"] = 0;
		$this->School_Holiday["recordType"] = 4;
		$this->Academic_Event["recordType"] = 1;
		
		$this->Public_Calendar["CalType"] = 1;
		$this->Group_Event["CalType"] = 1;
		$this->School_Event["CalType"] = 1;
		$this->School_Holiday["CalType"] = 1;
		$this->Academic_Event["CalType"] = 1;
		$this->Enrollment["CalType"]["Act"] = 5;
		$this->Enrollment["CalType"]["Group"] = 6;
		$this->Homework["CalType"] = 4;
		$this->Detention["CalType"] = 7;
	}
	
	function setCurrentUserID($parUserID)
	{
		$this->CurrentUserID = $parUserID;
	}
	
	function getCurrentUserID()
	{
		return $this->CurrentUserID? $this->CurrentUserID : $_SESSION['UserID'];
	}
	
	##### Control panel for Calendar left menu #####
	function getSchoolEventControlPanel(){
		$controlSchool  = "<li><span style='background-color:#".$this->School_Event["color"].";color:white;width: 140px'>";
		$controlSchool .= "<input type='checkbox' id='toggleSchoolEventVisible' name='toggleSchoolEventVisible' onClick='toggleSchoolEventVisibility(\"toggleSchoolEventVisible\",\"".$this->School_Event["DivClass"]."\",0)' ".($this->School_Event["visible"]=="1"?"checked='checked'":"")." />";
		$controlSchool.= "&nbsp;".$this->School_Event["name"];
		$controlSchool .= "</span>
		<input type='hidden' value='toggleSchoolEventVisible' id='V".$this->School_Event["DivClass"]."' name='V".$this->School_Event["DivClass"]."'>
		</li>";
		return $controlSchool;
	}
	function getAcademicEventControlPanel(){
		$controlSchool  = "<li><span style='background-color:#".$this->Academic_Event["color"].";color:white;width: 140px'>";
		$controlSchool .= "<input type='checkbox' id='toggleAcademicVisible' name='toggleAcademicVisible' onClick='toggleSchoolEventVisibility(\"toggleAcademicVisible\",\"".$this->Academic_Event["DivClass"]."\",1)' ".($this->Academic_Event["visible"]=="1"?"checked='checked'":"")." />";
		$controlSchool.= "&nbsp;".$this->Academic_Event["name"];
		$controlSchool .= "</span>
		<input type='hidden' value='toggleAcademicVisible' id='V".$this->Academic_Event["DivClass"]."' name='V".$this->Academic_Event["DivClass"]."'>
		</li>";
		return $controlSchool;
	}
	
	function getGroupEventControlPanel(){
		$controlSchool  = "<li><span style='background-color:#".$this->Group_Event["color"].";color:white;width: 140px'>";
		$controlSchool .= "<input type='checkbox' id='toggleGroupVisible' name='toggleGroupVisible' onClick='toggleSchoolEventVisibility(\"toggleGroupVisible\",\"".$this->Group_Event["DivClass"]."\",2)' ".($this->Group_Event["visible"]=="1"?"checked='checked'":"")." />";
		$controlSchool.= "&nbsp;".$this->Group_Event["name"];
		$controlSchool .= "</span>
		<input type='hidden' value='toggleGroupVisible' id='V".$this->Group_Event["DivClass"]."' name='V".$this->Group_Event["DivClass"]."'>
		</li>";
		return $controlSchool;
	}
	function getPublicHolidayControlPanel(){
		$controlSchool  = "<li><span style='background-color:#".$this->Public_Calendar["color"].";color:white;width: 140px'>";
		$controlSchool .= "<input type='checkbox' id='togglePublicVisible' name='togglePublicVisible' onClick='toggleSchoolEventVisibility(\"togglePublicVisible\",\"".$this->Public_Calendar["DivClass"]."\",3)' ".($this->Public_Calendar["visible"]=="1"?"checked='checked'":"")." />";
		$controlSchool.= "&nbsp;".$this->Public_Calendar["name"];
		$controlSchool .= "</span>
		<input type='hidden' value='togglePublicVisible' id='V".$this->Public_Calendar["DivClass"]."' name='V".$this->Public_Calendar["DivClass"]."'>
		</li>";
		return $controlSchool;
	}
	function getSchoolHolidayControlPanel(){
		$controlSchool  = "<li><span style='background-color:#".$this->School_Holiday["color"].";color:white;width: 140px'>";
		$controlSchool .= "<input type='checkbox' id='toggleSchoolHolidayVisible' name='toggleSchoolHolidayVisible' onClick='toggleSchoolEventVisibility(\"toggleSchoolHolidayVisible\",\"".$this->School_Holiday["DivClass"]."\",4)' ".($this->School_Holiday["visible"]=="1"?"checked='checked'":"")." />";
		$controlSchool.= "&nbsp;".$this->School_Holiday["name"];
		$controlSchool .= "</span>
		<input type='hidden' value='toggleSchoolHolidayVisible' id='V".$this->School_Holiday["DivClass"]."' name='V".$this->School_Holiday["DivClass"]."'>
		</li>";
		return $controlSchool;
	}
	function getHomeworkControlPanel(){
		$controlSchool  = "<li><span style='background-color:#".$this->Homework["color"].";color:white;width: 140px'>";
		$controlSchool .= "<input type='checkbox' id='toggleHomeworkVisible' name='toggleHomeworkVisible' 
onClick='toggleExternalCalVisibility(\"toggleHomeworkVisible\",\"".$this->Homework["DivClass"]."\",4)' ".($this->Homework["visible"]=="1"?"checked='checked'":"")." />";
		$controlSchool.= "&nbsp;".$this->Homework["name"];
		$controlSchool .= "</span>
		<input type='hidden' value='toggleHomeworkVisible' id='V".$this->Homework["DivClass"]."' name='V".$this->Homework["DivClass"]."'>
		</li>";
		return $controlSchool;
	}
	function getEnrollmentControlPanel(){
		$controlSchool  = "<li><span style='background-color:#".$this->Enrollment["color"].";color:white;width: 140px'>";
		$controlSchool .= "<input type='checkbox' id='toggleEnrollmentVisible' name='toggleEnrollmentVisible' 
onClick='toggleExternalCalVisibility(\"toggleEnrollmentVisible\",\"".$this->Enrollment["DivClass"]."\",5)' ".($this->Enrollment["visible"]=="1"?"checked='checked'":"")." />";
		$controlSchool.= "&nbsp;".$this->Enrollment["name"];
		$controlSchool .= "</span>
		<input type='hidden' value='toggleEnrollmentVisible' id='V".$this->Enrollment["DivClass"]."' name='V".$this->Enrollment["DivClass"]."'>
		</li>";
		return $controlSchool;
	}
	function getDetentionControlPanel(){
		$controlSchool  = "<li><span style='background-color:#".$this->Detention["color"].";color:white;width: 140px'>";
		$controlSchool .= "<input type='checkbox' id='toggleDetentionVisible' name='toggleDetentionVisible' 
onClick='toggleExternalCalVisibility(\"toggleDetentionVisible\",\"".$this->Detention["DivClass"]."\",7)' ".($this->Detention["visible"]=="1"?"checked='checked'":"")." />";
		$controlSchool.= "&nbsp;".$this->Detention["name"];
		$controlSchool .= "</span>
		<input type='hidden' value='toggleDetentionVisible' id='V".$this->Detention["DivClass"]."' name='V".$this->Detention["DivClass"]."'>
		</li>";
		return $controlSchool;
	}
	##### End of control panel for Calendar left menu #####
	
	
	
	function externalCalendarChecking(){
		$uid = $this->getCurrentUserID();
		$sql = "select Setting from CALENDAR_USER_PREF where UserID = '$uid' And Setting in (
		'".$this->Public_Calendar["setting"]."',
		'".$this->Group_Event["setting"]."',
		'".$this->School_Event["setting"]."',
		'".$this->School_Holiday["setting"]."',
		'".$this->Academic_Event["setting"]."',
		'".$this->Homework["setting"]."',
		'".$this->Enrollment["setting"]."',
		'".$this->Detention["setting"]."'
		)";
		$result = $this->returnVector($sql);
		if (count($result)<8){
			$sql = "insert into CALENDAR_USER_PREF(UserID,Setting,Value) values ";
			if (!in_array($this->Public_Calendar["setting"],$result))
				$sql .= "('$uid','".$this->Public_Calendar["setting"]."','1'), ";
			if (!in_array($this->Group_Event["setting"],$result))
				$sql .= "('$uid','".$this->Group_Event["setting"]."','1'), ";
			if (!in_array($this->School_Event["setting"],$result))
				$sql .= "('$uid','".$this->School_Event["setting"]."','1'), ";
			if (!in_array($this->School_Holiday["setting"],$result))
				$sql .= "('$uid','".$this->School_Holiday["setting"]."','1'), ";
			if (!in_array($this->Academic_Event["setting"],$result))	
				$sql .= "('$uid','".$this->Academic_Event["setting"]."','1'), ";
			if (!in_array($this->Homework["setting"],$result))
				$sql .= "('$uid','".$this->Homework["setting"]."','1'), ";
			if (!in_array($this->Enrollment["setting"],$result))
				$sql .= "('$uid','".$this->Enrollment["setting"]."','1'), ";
			if (!in_array($this->Detention["setting"],$result))
				$sql .= "('$uid','".$this->Detention["setting"]."','1'), ";
				
			$sql = rtrim($sql,", ");
			$this->db_db_query($sql);
		}
	}
	
	function getSchoolEventDetail($eventID){
		$uid = $this->getCurrentUserID();
		$fieldname  = "a.EventID, IF(a.InputBy IS NULL,'0',a.InputBy) as UserID, '0' as CalID, '' as RepeatID, date_format(a.EventDate, '%Y-%m-%d %H:%i:%s') AS EventDate, ";
		$fieldname .= "
            If (a.TitleEng IS NULL Or a.TitleEng = '', a.Title, ".Get_Lang_Selection('a.Title','a.TitleEng').") as Title,           
            If (a.DescriptionEng IS NULL Or a.DescriptionEng = '', a.Description, ".Get_Lang_Selection('a.Description','a.DescriptionEng').") as Description,          
            '1440' as Duration, '0' as IsImportant, '1' as IsAllDay, 'P' as Access, a.EventVenue as Location, a.EventLocationID as LocationID, ";
		$fieldname .= " pn.PersonalNote ";
		
		$sql = "SELECT $fieldname 
				FROM INTRANET_EVENT AS a 
				LEFT JOIN CALENDAR_EVENT_PERSONAL_NOTE AS pn ON 
					a.EventID = pn.EventID and 
					pn.UserID = '".$uid."'
					and pn.CalType = 1
					";		
		$sql .= "WHERE a.EventID=$eventID";
		$detail = $this->returnArray($sql);
		if ($detail[0]["LocationID"]!=0){
			$detail[0]["Location"] = $this->returnFullAddr($detail[0]["LocationID"]);
		}
		$detail[0]["Title"] = intranet_undo_htmlspecialchars($detail[0]["Title"]);
		$detail[0]["PersonalNote"] = intranet_undo_htmlspecialchars($detail[0]["PersonalNote"]);
		$detail[0]["Description"] = intranet_undo_htmlspecialchars($detail[0]["Description"]);
		return $detail;
	}
	
	function returnFullAddr($locationID){
		if ($locationID!=0){
			$room = new Room($locationID);
			$location = $room->RoomName.", ".$room->FloorName.", ".$room->BuildingName;
			return $location;
		}
		return $locationID;
	}
	
	
	function getAllSchoolEvent($lowBound="", $upBound="", $searchCase="",$useTempTable=false){
		$uid = $this->getCurrentUserID();		
		$libcal = new libcalevent2007();
		$allEvent = $libcal->returnAllEvents($lowBound,$upBound);	
		$eventIDSql = "";
		foreach ($allEvent as $event){
			$eventIDSql .=  "'".$event["EventID"]."',";
		}
		
		
		$eventIDSql = empty($eventIDSql)?"''":rtrim($eventIDSql,",");
		
		$colArray = Array('e.EventVenue','pn.PersonalNote','e.Title','EventDate','e.Description');
		$keySearchSql = $this->SearchSqlString($searchCase,$colArray);	
		$title = Get_Lang_Selection('e.Title','e.TitleEng');
		$Title_field = "IF ( $title Is Null Or $title = '',e.Title , $title ) ";
		
		$Description= Get_Lang_Selection('e.Description','e.DescriptionEng');
		$Description_field = "IF ( $Description Is Null Or $Description= '',e.Description , $Description) ";
		
		$EventVenue_field = "  If (e.EventVenueEng IS NULL Or e.EventVenueEng = '', e.EventVenue, ".Get_Lang_Selection('e.EventVenue','e.EventVenueEng').") ";
		
		$sql = "select pn.PersonalNote, $Description_field as Description , $EventVenue_field as Location,
				e.EventLocationID as LocationID,
				$Title_field as Title, e.EventID, e.RecordType, 
				DATE_FORMAT(e.EventDate,'%Y-%m-%d') AS EventDate,
				'1' as CalType, 1440 as Duration, '1' as IsAllDay, e.ModifyBy as ".($useTempTable?"ModifyBy":"UserID").",
				e.DateModified 
				From INTRANET_EVENT as e 
				Left join CALENDAR_EVENT_PERSONAL_NOTE as pn on
				e.EventID = pn.EventID And
				pn.UserID = '".$uid."' and
				pn.CalType = 1
				where
				e.EventID in ($eventIDSql)
				$keySearchSql
			";
		
		if ($useTempTable){
			$insertSql = "insert into TempEvent (PersonalNote,Description,Location,LocationID,Title,EventID,RecordType,EventDate,CalType,Duration,IsAllDay,ModifyBy,DateModified) ".$sql;
			$this->db_db_query($insertSql);
			return;
		}
		else
			$detailedEvent = $this->returnArray($sql);
		// debug_r($detailedEvent);
		return $this->formatExternalEvent($detailedEvent,$lowBound);
	}
	
	function getExternalCalConfig($calType, $recordType=""){
		switch($calType){
			case 1:
			return $this->getCorrespondingSchoolCalConfig($recordType);
			case 4:
			return $this->Homework;
			case 5:
			return $this->Enrollment; #activity
			case 6:
			return $this->Enrollment; #group
			case 7:
			return $this->Detention;
		}
	}
	
	function getCorrespondingSchoolCalConfig($recordType){
		switch($recordType){
			case 0:
			return $this->School_Event;
			case 1:
			return $this->Academic_Event;
			case 2:
			return $this->Group_Event;
			case 3:
			return $this->Public_Calendar;
			case 4:
			return $this->School_Holiday;		
		}
	}
	
	# use for export event
//	function returnFormattedSchoolExportEvent($recordType){
//		$sql = "select 
//					Title, '' as ExtraIcalInfo, Description, 
//					DateInput as InputDate,
//					DateModified as ModifiedDate,
//					'1440' as Duration,
//					Date_Format(EventDate,'%Y-%m-%d') as EventDate,
//					EventVenue,
//					EventLocationID,
//					concat(EventID,'@SchoolEvent.eclass') as UID,
//					Case RecordType 
//						When 0 Then '".$this->School_Event["name"]."'
//						When 1 Then '".$this->Academic_Event["name"]."'
//						When 2 Then '".$this->Group_Event["name"]."'
//						When 3 Then '".$this->Public_Calendar["name"]."'
//						When 4 Then '".$this->School_Holiday["name"]."'
//					END as Name,
//					DATE_FORMAT(EventDate,'%Y-%m-%d') as StartDate,
//					'00:00:00' AS StartTime,
//					DATE_FORMAT(EventDate,'%Y-%m-%d') as EndDate,
//					'00:00:00' AS EndTime,
//					'Yes' as AllDayEvent,
//					'1' as IsAllDay,
//					'' as url
//					From INTRANET_EVENT where
//					RecordType = $recordType
//					ORDER By EventDate
//				";				
//		$result = $this->returnArray($sql);
//		for ($i = 0; $i<count($result); $i++){
//			if ($result[$i]['EventLocationID']!=0)
//				$result[$i]['Location'] = $this->returnFullAddr($result[$i]['EventLocationID']);
//			else
//				$result[$i]['Location'] = $result[$i]['EventVenue'];
//			$result[$i]["Title"] = intranet_undo_htmlspecialchars($result[$i]["Title"]);
//			$result[$i]["PersonalNote"] = intranet_undo_htmlspecialchars($result[$i]["PersonalNote"]);
//			$result[$i]["Description"] = intranet_undo_htmlspecialchars($result[$i]["Description"]);
//		}		
//		return $result;
//	}

	function returnFormattedSchoolExportEvent($recordTypeAry, $StartDate='', $EndDate=''){
		if ($StartDate !== '') {
			$condsStartDate = " AND ('".$StartDate."' <= DATE_FORMAT(EventDate,'%Y-%m-%d')) ";
		}
		if ($EndDate !== '') {
			$condsEndDate = " AND (DATE_FORMAT(EventDate,'%Y-%m-%d') <= '".$EndDate."') ";
		}
		
		$sql = "select 
					Title, '' as ExtraIcalInfo, Description, 
					DateInput as InputDate,
					DateModified as ModifiedDate,
					'1440' as Duration,
					Date_Format(EventDate,'%Y-%m-%d') as EventDate,
					EventVenue,
					EventLocationID,
					concat(EventID,'@SchoolEvent.eclass') as UID,
					Case RecordType 
						When 0 Then '".$this->School_Event["name"]."'
						When 1 Then '".$this->Academic_Event["name"]."'
						When 2 Then '".$this->Group_Event["name"]."'
						When 3 Then '".$this->Public_Calendar["name"]."'
						When 4 Then '".$this->School_Holiday["name"]."'
					END as Name,
					DATE_FORMAT(EventDate,'%Y-%m-%d') as StartDate,
					'00:00:00' AS StartTime,
					DATE_FORMAT(EventDate,'%Y-%m-%d') as EndDate,
					'00:00:00' AS EndTime,
					'Yes' as AllDayEvent,
					'1' as IsAllDay,
					'' as url
					From INTRANET_EVENT where
					RecordType IN ('".implode("','", (array)$recordTypeAry)."')
					$condsStartDate
					$condsEndDate
					ORDER By EventDate
				";				
		$result = $this->returnArray($sql);
		for ($i = 0; $i<count($result); $i++){
			if ($result[$i]['EventLocationID']!=0)
				$result[$i]['Location'] = $this->returnFullAddr($result[$i]['EventLocationID']);
			else
				$result[$i]['Location'] = $result[$i]['EventVenue'];
			$result[$i]["Title"] = intranet_undo_htmlspecialchars($result[$i]["Title"]);
			$result[$i]["PersonalNote"] = intranet_undo_htmlspecialchars($result[$i]["PersonalNote"]);
			$result[$i]["Description"] = intranet_undo_htmlspecialchars($result[$i]["Description"]);
		}		
		return $result;
	}
	
	function getRelatedHomeworkEvent($lowBound="",$upBound="",$searchCase="",$useTempTable=false){
		$uid = $this->getCurrentUserID();		
		$subjAndGroup = $this->getUserRelatedSubjAndGroup();
		if (!empty($subjAndGroup["Subject"])&&!empty($subjAndGroup["SubjectGroup"])){
			$conds = "";
			if ($lowBound!=""&&$upBound!="")
			$conds = " AND UNIX_TIMESTAMP(h.StartDate) BETWEEN $lowBound AND $upBound";
			$subjListSql = implode("','",$subjAndGroup["Subject"]);
			$subjGroupListSql = implode("','",$subjAndGroup["SubjectGroup"]);
			
			$colArray = Array('h.StartDate','h.DueDate','h.Description','c.PersonalNote','h.Title','c.PersonalNote');
			$keySearchSql = $this->SearchSqlString($searchCase,$colArray);		
			
			$sql = "Select 
					h.HomeworkID as EventID,h.StartDate as EventDate,h.DueDate,
					h.Title,h.Description,c.PersonalNote,
					'4' as CalType,'1440' as Duration, '1' as IsAllDay,
					h.LastModified as DateModified 
					From INTRANET_HOMEWORK as h
					Left outer join CALENDAR_EVENT_PERSONAL_NOTE as c on 
					c.EventID = h.HomeworkID and
					c.UserID = '".$uid."' and
					c.CalType = 4 
					where h.ClassGroupID in ('$subjGroupListSql') and
						h.SubjectID in ('$subjListSql')
						$conds 
						$keySearchSql
				"; //echo $sql;
			if ($useTempTable){
				$insertSql = "insert into TempEvent (EventID, EventDate, DueDate, Title, Description, PersonalNote,CalType,Duration,IsAllDay,DateModified) ".$sql;
				$this->db_db_query($insertSql);
				return;
			}
			else
				$result = $this->returnArray($sql);		
			
			return $this->formatExternalEvent($result,$lowBound);
		
		}
		return Array();
	}
	
	function getHomeworkEventDetail($eventID){
		$uid = $this->getCurrentUserID();
		$fieldname  = "a.HomeworkID as EventID, '0' as UserID, '0' as CalID, '' as RepeatID, date_format(a.StartDate, '%Y-%m-%d') AS EventDate, 
		date_format(a.DueDate, '%Y-%m-%d') AS DueDate, ";
		$fieldname .= "a.Title, a.Description, '1440' as Duration, '0' as IsImportant, '1' as IsAllDay, 'P' as Access, '' as Location, ";
		$fieldname .= " pn.PersonalNote ";
		
		$sql = "SELECT $fieldname 
				FROM INTRANET_HOMEWORK AS a 
				LEFT JOIN CALENDAR_EVENT_PERSONAL_NOTE AS pn ON 
					a.HomeworkID = pn.EventID and 
					pn.UserID = '".$uid."' 
					and pn.CalType = 4
				";		
		$sql .= "WHERE a.HomeworkID=$eventID";		
		$detail = $this->returnArray($sql);		
		$detail[0]["Title"] = intranet_undo_htmlspecialchars($detail[0]["Title"]);
		$detail[0]["PersonalNote"] = intranet_undo_htmlspecialchars($detail[0]["PersonalNote"]);
		$detail[0]["Description"] = intranet_undo_htmlspecialchars($detail[0]["Description"]);
		return $detail;
	}
	
	# use for export event
	function returnFormattedHomeworkExportEvent(){
		$subjAndGroup = $this->getUserRelatedSubjAndGroup();
		if (!empty($subjAndGroup["Subject"])&&!empty($subjAndGroup["SubjectGroup"])){
			$subjListSql = implode("','",$subjAndGroup["Subject"]);
			$subjGroupListSql = implode("','",$subjAndGroup["SubjectGroup"]);
			$sql = "select 
						Title, '' as ExtraIcalInfo, Description, 
						LastModified as InputDate,
						LastModified as ModifiedDate,
						(TIMESTAMPDIFF(MINUTE,StartDate,DueDate)+1440) as Duration,
						Date_Format(StartDate,'%Y-%m-%d') as EventDate,
						'' as Location,
						concat(HomeworkID,'@HomeworkEvent.eclass') as UID,
						'".$this->Homework['name']."' as Name,
						DATE_FORMAT(StartDate,'%Y-%m-%d') as StartDate,
						'00:00:00' AS StartTime,
						DATE_FORMAT(DueDate,'%Y-%m-%d') as EndDate,
						'00:00:00' AS EndTime,
						'Yes' as AllDayEvent,
						'1' as IsAllDay,
						'' as url
						From INTRANET_HOMEWORK where
						ClassGroupID in ('$subjGroupListSql') and
						SubjectID in ('$subjListSql')
						ORDER By EventDate
					";				
			$result = $this->returnArray($sql);
			for ($i = 0; $i<count($result); $i++){
				
				$result[$i]["Title"] = intranet_undo_htmlspecialchars($result[$i]["Title"]);
				$result[$i]["PersonalNote"] = intranet_undo_htmlspecialchars($result[$i]["PersonalNote"]);
				$result[$i]["Description"] = intranet_undo_htmlspecialchars($result[$i]["Description"]);
			}		
			return $result;
		}
	}
	
	
	function getUserRelatedSubjAndGroup(){
		$uid = $this->getCurrentUserID();
		$tableName = "";
		$userList = Array();
		$libhomework = new libhomework2007();
		if ($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching']){
			$tableName ='SUBJECT_TERM_CLASS_TEACHER';
			$userList[] =$uid;			
		}
		else if($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']){
			$tableName ='SUBJECT_TERM_CLASS_TEACHER';
			$userList[] =$uid;
		}
		else if($_SESSION['UserType']==USERTYPE_STUDENT){
			$tableName ='SUBJECT_TERM_CLASS_USER';
			$userList[] =$uid;
		}
		else if($_SESSION['UserType']==USERTYPE_PARENT){
			$tableName ='SUBJECT_TERM_CLASS_USER';
			$userList = $libhomework->getChildrenList($uid);
		}
		if (count($userList)>0 && !empty($tableName)){
			$userListSql = implode("','",$userList);
			$sql = "
				SELECT a.RecordID
				FROM ASSESSMENT_SUBJECT AS a
				LEFT OUTER JOIN SUBJECT_TERM AS b ON b.SubjectID = a.RecordID 
				LEFT OUTER JOIN $tableName AS c ON 
				c.SubjectGroupID = b.SubjectGroupID 
				WHERE c.UserID in ('$userListSql')
				GROUP BY a.RecordID";
			$subjList = $this->returnVector($sql);
			$subjListSql = implode("','",$subjList);
			$sql = "SELECT a.SubjectGroupID 
					From SUBJECT_TERM_CLASS As a 
					LEFT OUTER JOIN $tableName As b ON
					b.SubjectGroupID = a.SubjectGroupID
					LEFT OUTER JOIN SUBJECT_TERM As c ON 
					c.SubjectGroupID = a.SubjectGroupID
					WHERE b.UserID in ('$userListSql') 
					AND c.SubjectID in('$subjListSql')
					";
			$subjGroupList =  $this->returnVector($sql);
			return Array("Subject"=>$subjList,"SubjectGroup"=>$subjGroupList);
		}	
		return Array();
	}
	
	function getRelatedAllEnrolActivity($lowBound="",$upBound="",$searchValue="",$useTempTable=false){
		$groupActivity = $this->getRelatedGroupActivity($lowBound,$upBound,$searchValue,$useTempTable);
		$userActivity = $this->getRelatedUserActivity($lowBound,$upBound,$searchValue,$useTempTable);
		if ($useTempTable)
			return;
		$allActivity = array_merge($groupActivity,$userActivity);
		return $this->formatExternalEvent($allActivity,$lowBound);
	}
	
	function returnFormattedAllActivityExportEvent(){			
		$groupActivity = $this->returnFormattedGroupActivityExportEvent();
		$userActivity = $this->returnFormattedUserActivityExportEvent();		
		return array_merge($groupActivity,$userActivity);
	}
	
	# use for export event
	function returnFormattedGroupActivityExportEvent(){
		$uid = $this->getCurrentUserID();		
		$sql = "select 
					g.Title as Title, '' as ExtraIcalInfo, i.Description, 
					i.DateInput as InputDate,
					i.DateModified as ModifiedDate,	
					Case When			
					Date_Format(d.ActivityDateStart,'%T') = '00:00:00' and
						Date_Format(d.ActivityDateEnd,'%T') = '00:00:00'
					Then	
					TIMESTAMPDIFF(MINUTE,d.ActivityDateStart,d.ActivityDateEnd)+1440
					Else 
					TIMESTAMPDIFF(MINUTE,d.ActivityDateStart,d.ActivityDateEnd)
					END	as Duration,
					Date_Format(d.ActivityDateStart,'%Y-%m-%d') as EventDate,
					'' as Location,
					concat(d.GroupDateID,'@EnrolGroupEvent.eclass') as UID,
					'".$this->Enrollment['name']."' as Name,
					DATE_FORMAT(d.ActivityDateStart,'%Y-%m-%d') as StartDate,
					DATE_FORMAT(d.ActivityDateStart,'%T') AS StartTime,
					DATE_FORMAT(d.ActivityDateEnd,'%Y-%m-%d') as EndDate,
					DATE_FORMAT(d.ActivityDateEnd,'%T') AS EndTime,
					Case When 
						Date_Format(d.ActivityDateStart,'%T') = '00:00:00' and
						Date_Format(d.ActivityDateEnd,'%T') = '00:00:00'
					Then 'Yes' 
					Else 'No'
					End as AllDayEvent,
					Case When 
						Date_Format(d.ActivityDateStart,'%T') = '00:00:00' and
						Date_Format(d.ActivityDateEnd,'%T') = '00:00:00'
					Then '1' 
					Else '0'
					End as IsAllDay,
					'' as url
					From INTRANET_USERGROUP as u Inner join INTRANET_GROUP as g on
					u.GroupID = g.GroupID and 
					g.RecordType = 5
				Inner join INTRANET_ENROL_GROUPINFO as i on
					g.GroupID = i.GroupID
				Inner join INTRANET_ENROL_GROUP_DATE as d on
					i.EnrolGroupID = d.EnrolGroupID and
					d.RecordStatus = 1
				where u.UserID = '".$uid."'
					ORDER By EventDate
				";		
		$result = $this->returnArray($sql);
		for ($i = 0; $i<count($result); $i++){
			
			$result[$i]["Title"] = intranet_undo_htmlspecialchars($result[$i]["Title"]);
			$result[$i]["PersonalNote"] = intranet_undo_htmlspecialchars($result[$i]["PersonalNote"]);
			$result[$i]["Description"] = intranet_undo_htmlspecialchars($result[$i]["Description"]);
		}		
		return $result;
		
	}
	
	
	function getRelatedGroupActivity($lowBound="",$upBound="",$searchCase="",$useTempTable=false){
		$cord = "";
		
		$uid = $this->getCurrentUserID();
		$colArray = Array('d.ActivityDateStart','d.ActivityDateEnd','i.Description','g.Title','c.PersonalNote');
		$keySearchSql = $this->SearchSqlString($searchCase,$colArray);			
		
		if ($lowBound!=""&&$upBound!="")
			$cord = " 
				And ((UNIX_TIMESTAMP(d.ActivityDateStart) between $lowBound and $upBound 
				OR UNIX_TIMESTAMP(d.ActivityDateEnd) between $lowBound and $upBound)
				OR (UNIX_TIMESTAMP(d.ActivityDateStart) <= $lowBound and 
				UNIX_TIMESTAMP(d.ActivityDateEnd) >= $upBound)
				)";
		$sql = "select 				
				d.ActivityDateStart as EventDate, 
				d.ActivityDateEnd as EndDate, 
				d.GroupDateID as EventID,
				i.Description,
				'6' as CalType,
				TIMESTAMPDIFF(MINUTE,d.ActivityDateStart,d.ActivityDateEnd) as Duration,
				g.Title,
				c.PersonalNote,
				Case When 
					Date_Format(d.ActivityDateStart,'%T') = '00:00:00' and
					Date_Format(d.ActivityDateEnd,'%T') = '00:00:00'
				Then '1'
				Else '0'
				End as IsAllDay,
				i.DateModified 
				From INTRANET_USERGROUP as u Inner join INTRANET_GROUP as g on
					u.GroupID = g.GroupID and 
					g.RecordType = 5
				Inner join INTRANET_ENROL_GROUPINFO as i on
					g.GroupID = i.GroupID
				Inner join INTRANET_ENROL_GROUP_DATE as d on
					i.EnrolGroupID = d.EnrolGroupID and
					d.RecordStatus = 1
				Left outer join CALENDAR_EVENT_PERSONAL_NOTE as c on 
					c.EventID = d.GroupDateID and
					c.UserID = '".$uid."' and
					c.CalType = 6
				where u.UserID = '".$uid."'
				$cord
				$keySearchSql
			";
		if ($useTempTable){
			$insertSql = "insert into TempEvent (EventDate,EndDate,EventID,Description,CalType,Duration,Title,PersonalNote,IsAllDay,DateModified) ".$sql;
			$this->db_db_query($insertSql);
			return;
		}
		else
			return $this->returnArray($sql);		
	}
	
	# use for export event
	function returnFormattedUserActivityExportEvent(){
		$uid = $this->getCurrentUserID();
		$tableName = "";
		if ($_SESSION['UserType']==USERTYPE_STAFF){
			$tableName = "INTRANET_ENROL_EVENTSTAFF";
		}
		else if($_SESSION['UserType']==USERTYPE_STUDENT){
			$tableName = "INTRANET_ENROL_EVENTSTUDENT";
		}
		if ($tableName == "")
			return Array();
		$sql = "select 
					e.EventTitle as Title, '' as ExtraIcalInfo, e.Description, 
					e.DateInput as InputDate,
					e.DateModified as ModifiedDate,		
					Case When			
					Date_Format(d.ActivityDateStart,'%T') = '00:00:00' and
						Date_Format(d.ActivityDateEnd,'%T') = '00:00:00'
					Then	
					TIMESTAMPDIFF(MINUTE,d.ActivityDateStart,d.ActivityDateEnd)+1440
					Else 
					TIMESTAMPDIFF(MINUTE,d.ActivityDateStart,d.ActivityDateEnd)
					END	as Duration,
					Date_Format(d.ActivityDateStart,'%Y-%m-%d') as EventDate,
					'' as Location,
					concat(d.EventDateID,'@EnrolActEvent.eclass') as UID,
					'".$this->Enrollment['name']."' as Name,
					DATE_FORMAT(d.ActivityDateStart,'%Y-%m-%d') as StartDate,
					DATE_FORMAT(d.ActivityDateStart,'%T') AS StartTime,
					DATE_FORMAT(d.ActivityDateEnd,'%Y-%m-%d') as EndDate,
					DATE_FORMAT(d.ActivityDateEnd,'%T') AS EndTime,
					Case When 
						Date_Format(d.ActivityDateStart,'%T') = '00:00:00' and
						Date_Format(d.ActivityDateEnd,'%T') = '00:00:00'
					Then 'Yes' 
					Else 'No'
					End as AllDayEvent,
					Case When 
						Date_Format(d.ActivityDateStart,'%T') = '00:00:00' and
						Date_Format(d.ActivityDateEnd,'%T') = '00:00:00'
					Then '1' 
					Else '0'
					End as AllDayEvent,
					'' as url
					From $tableName as u inner join INTRANET_ENROL_EVENTINFO as e on
					e.EnrolEventID = u.EnrolEventID
				inner join INTRANET_ENROL_EVENT_DATE as d on
					e.EnrolEventID = d.EnrolEventID and 
					d.RecordStatus = 1
				where u.UserID = '".$uid."'
					ORDER By EventDate
				";				
		$result = $this->returnArray($sql);
		for ($i = 0; $i<count($result); $i++){
			
			$result[$i]["Title"] = intranet_undo_htmlspecialchars($result[$i]["Title"]);
			$result[$i]["PersonalNote"] = intranet_undo_htmlspecialchars($result[$i]["PersonalNote"]);
			$result[$i]["Description"] = intranet_undo_htmlspecialchars($result[$i]["Description"]);
		}		
		return $result;
		
	}
	
	
	function getRelatedUserActivity($lowBound="",$upBound="",$searchCase="",$useTempTable=false){
		$uid = $this->getCurrentUserID();
		$tableName = "";
		if ($_SESSION['UserType']==USERTYPE_STAFF){
			$tableName = "INTRANET_ENROL_EVENTSTAFF";
			$UserIDName = "UserID";
		}
		else if($_SESSION['UserType']==USERTYPE_STUDENT){
			$tableName = "INTRANET_ENROL_EVENTSTUDENT";
			$UserIDName = "StudentID";
		}
		if ($tableName == "")
			return Array();
		$cord = "";
		if ($lowBound!=""&&$upBound!="")
			$cord = " 
				And ((UNIX_TIMESTAMP(d.ActivityDateStart) between $lowBound and $upBound 
				OR UNIX_TIMESTAMP(d.ActivityDateEnd) between $lowBound and $upBound)
				OR (UNIX_TIMESTAMP(d.ActivityDateStart) <= $lowBound and 
				UNIX_TIMESTAMP(d.ActivityDateEnd) >= $upBound)
				)";		
				
		$colArray = Array('dd.ActivityDateStart','d.ActivityDateEnd','e.EventTitle','e.Description','c.PersonalNote');
		$keySearchSql = $this->SearchSqlString($searchCase,$colArray);		
		
		$sql = "
				Select 
				d.ActivityDateStart as EventDate, 
				d.ActivityDateEnd as EndDate, 
				d.EventDateID as EventID,
				e.EventTitle as Title,
				e.Description,
				c.PersonalNote,
				'5' as CalType,
				TIMESTAMPDIFF(MINUTE,d.ActivityDateStart,d.ActivityDateEnd) as Duration,
				Case When 
				Date_Format(d.ActivityDateStart,'%T') = '00:00:00' and
				Date_Format(d.ActivityDateEnd,'%T') = '00:00:00'
				Then '1'
				Else '0'
				End as IsAllDay,
				e.DateModified 
				From $tableName as u inner join INTRANET_ENROL_EVENTINFO as e on
					e.EnrolEventID = u.EnrolEventID
				inner join INTRANET_ENROL_EVENT_DATE as d on
					e.EnrolEventID = d.EnrolEventID and 
					d.RecordStatus = 1
				Left outer join CALENDAR_EVENT_PERSONAL_NOTE as c on 
					c.EventID = d.EventDateID and
					c.UserID = '".$uid."' and
					c.CalType = 5
				where u.$UserIDName = '".$uid."'
				$cord
				$keySearchSql
				";
		if ($useTempTable){
			$insertSql = "insert into TempEvent (EventDate,EndDate,Title,Description,PersonalNote,CalType,Duration,IsAllDay,DateModified) ".$sql;
			$this->db_db_query($insertSql);
			return;

		}
		else
			return $this->returnArray($sql);
	}
	
	function getEnrolActivityEventDetail($eventID){
		$uid = $this->getCurrentUserID();
		$tableName = "";
		if ($_SESSION['UserType']==USERTYPE_STAFF){
			$tableName = "INTRANET_ENROL_EVENTSTAFF";
		}
		else if($_SESSION['UserType']==USERTYPE_STUDENT){
			$tableName = "INTRANET_ENROL_EVENTSTUDENT";
		}
		if ($tableName == "")
			return Array();
		$fieldname  = "d.EventDateID as EventID, '0' as UserID, '0' as CalID, '' as RepeatID, date_format(d.ActivityDateStart, '%Y-%m-%d %T') AS EventDate, 
		date_format(d.ActivityDateEnd, '%Y-%m-%d %T') AS EndDate, ";
		$fieldname .= "e.EventTitle as Title, e.Description, 
		TIMESTAMPDIFF(MINUTE,d.ActivityDateStart,d.ActivityDateEnd) as Duration, 
		'0' as IsImportant, Case When 
					Date_Format(d.ActivityDateStart,'%T') = '00:00:00' and
					Date_Format(d.ActivityDateEnd,'%T') = '00:00:00'
					Then '1'
					Else '0'
				End as IsAllDay, 'P' as Access, '' as Location, ";
		$fieldname .= " pn.PersonalNote ";
		
		$sql = "SELECT $fieldname 				
				From $tableName as u inner join INTRANET_ENROL_EVENTINFO as e on
					e.EnrolEventID = u.EnrolEventID
				inner join INTRANET_ENROL_EVENT_DATE as d on
					e.EnrolEventID = d.EnrolEventID and 
					d.RecordStatus = 1
				Left outer join CALENDAR_EVENT_PERSONAL_NOTE as pn on 
					pn.EventID = d.EventDateID and
					pn.UserID = '".$uid."' and
					pn.CalType = 5
				where d.EventDateID = '$eventID'
				";		
		$detail = $this->returnArray($sql);		
		$detail[0]["Title"] = intranet_undo_htmlspecialchars($detail[0]["Title"]);
		$detail[0]["PersonalNote"] = intranet_undo_htmlspecialchars($detail[0]["PersonalNote"]);
		$detail[0]["Description"] = intranet_undo_htmlspecialchars($detail[0]["Description"]);
		return $detail;
	}
	
	
	function getEnrolGroupEventDetail($eventID){
		$uid = $this->getCurrentUserID();
		$fieldname  = "d.GroupDateID as EventID, '0' as UserID, '0' as CalID, '' as RepeatID, date_format(d.ActivityDateStart, '%Y-%m-%d %T') AS EventDate, 
		date_format(d.ActivityDateEnd, '%Y-%m-%d %T') AS EndDate, ";
		$fieldname .= "g.Title, i.Description, 
		TIMESTAMPDIFF(MINUTE,d.ActivityDateStart,d.ActivityDateEnd) as Duration, 
		'0' as IsImportant, Case When 
					Date_Format(d.ActivityDateStart,'%T') = '00:00:00' and
					Date_Format(d.ActivityDateEnd,'%T') = '00:00:00'
					Then '1'
					Else '0'
				End as IsAllDay, 'P' as Access, '' as Location, ";
		$fieldname .= " pn.PersonalNote ";
		
		$sql = "SELECT $fieldname 
				From INTRANET_USERGROUP as u Inner join INTRANET_GROUP as g on
					u.GroupID = g.GroupID and 
					g.RecordType = 5
				Inner join INTRANET_ENROL_GROUPINFO as i on
					g.GroupID = i.GroupID
				Inner join INTRANET_ENROL_GROUP_DATE as d on
					i.EnrolGroupID = d.EnrolGroupID and
					d.RecordStatus = 1
				Left outer join CALENDAR_EVENT_PERSONAL_NOTE as pn on 
					pn.EventID = d.GroupDateID and
					pn.UserID = '".$uid."' and
					pn.CalType = 6
				where d.GroupDateID = $eventID
				";		
		$detail = $this->returnArray($sql);		
		$detail[0]["Title"] = intranet_undo_htmlspecialchars($detail[0]["Title"]);
		$detail[0]["PersonalNote"] = intranet_undo_htmlspecialchars($detail[0]["PersonalNote"]);
		$detail[0]["Description"] = intranet_undo_htmlspecialchars($detail[0]["Description"]);
		return $detail;
	}
	
	function getAllDetentionEvent($lowBound="",$upBound="",$searchCase="",$useTempTable=false){
		$colName = "";
		$userID = $this->getCurrentUserID();
		$libhomework = new libhomework2007();
		if ($_SESSION['UserType']==USERTYPE_STAFF){
			$colName = "stud.RequestedBy";
		}
		else if($_SESSION['UserType']==USERTYPE_STUDENT){
			$colName = "stud.StudentID";
		}
		else if($_SESSION['UserType']==USERTYPE_PARENT){
			$tableName ='stud.StudentID';
			$studentList = $libhomework->getChildrenList($userID);
			$userID = implode("','",$studentList);
		}
		if ($colName == "")
			return Array();
		$cord = "";
		global $intranet_session_language;
		if ($intranet_session_language=='en')
			$langCol = "u.EnglishName";
		else
			$langCol = "u.ChineseName";
		
		if ($lowBound!=""&&$upBound!="")
			$cord = " 
				And UNIX_TIMESTAMP(sess.DetentionDate) between $lowBound and $upBound ";
				
		$colArray = Array("Concat(sess.DetentionDate,' ',sess.StartTime)",'sess.Location','c.PersonalNote','stud.Reason',"concat($langCol,' (',u.ClassName,' - ',u.ClassNumber,') ','".$this->Detention["name"]."')");
		$keySearchSql = $this->SearchSqlString($searchCase,$colArray);
		
		$sql = "
				Select stud.StudentID, stud.RecordID as EventID,
				concat($langCol,' (',u.ClassName,' - ',u.ClassNumber,') ','".$this->Detention["name"]."') as Title,
				stud.Reason as Description, stud.RequestedBy as UserID,
				Concat(sess.DetentionDate,' ',sess.StartTime) as EventDate,
				TIMESTAMPDIFF(Minute,Concat(sess.DetentionDate,' ',sess.StartTime),Concat(sess.DetentionDate,' ',sess.EndTime)) as Duration,
				sess.Location, '7' as CalType, '0' as IsAllDay,
				c.PersonalNote,
				stud.DateModified 
				From DISCIPLINE_DETENTION_STUDENT_SESSION as stud 
				inner join DISCIPLINE_DETENTION_SESSION as sess on
					stud.DetentionID = sess.DetentionID
				inner join INTRANET_USER as u on
						stud.StudentID = u.UserID
				Left outer join CALENDAR_EVENT_PERSONAL_NOTE as c on 
					c.EventID = stud.RecordID and
					c.UserID = '".$userID."' and
					c.CalType = 7
				where $colName in ('$userID') $cord
					$keySearchSql
				";		
		if ($useTempTable){
			$insertSql = "insert into TempEvent (StudentID,EventID,Title,Description,UserID,EventDate,Duration,Location,CalType,IsAllDay,PersonalNote,DateModified) ".$sql;		
			$this->db_db_query($insertSql);
			return;
		}
		else
			$result = $this->returnArray($sql);
		return $this->formatExternalEvent($result,$lowBound);
	}
	
	# use for export event
	function returnFormattedDetentionExportEvent(){
		$colName = "";
		$userID = $this->getCurrentUserID();
		$libhomework = new libhomework2007();
		if ($_SESSION['UserType']==USERTYPE_STAFF){
			$colName = "stud.RequestedBy";
		}
		else if($_SESSION['UserType']==USERTYPE_STUDENT){
			$colName = "stud.StudentID";
		}
		else if($_SESSION['UserType']==USERTYPE_PARENT){
			$tableName ='stud.StudentID';
			$studentList = $libhomework->getChildrenList($userID);
			$userID = implode("','",$studentList);
		}
		if ($colName == "")
			return Array();
			
		global $intranet_session_language;
		if ($intranet_session_language=='en')
			$langCol = "u.EnglishName";
		else
			$langCol = "u.ChineseName";
			
		$sql = "select 
					concat($langCol,' (',u.ClassName,' - ',u.ClassNumber,') ','".$this->Detention["name"]."') as Title, 
					'' as ExtraIcalInfo, stud.Reason as Description, 
					stud.DateInput as InputDate,
					stud.DateModified as ModifiedDate,
					TIMESTAMPDIFF(Minute,Concat(sess.DetentionDate,' ',sess.StartTime),Concat(sess.DetentionDate,' ',sess.EndTime)) as Duration,
					Concat(sess.DetentionDate,' ',sess.StartTime) as EventDate,
					sess.Location as Location,
					concat(stud.RecordID,'@DetentionEvent.eclass') as UID,
					'".$this->Detention['name']."' as Name,
					sess.DetentionDate as StartDate,
					sess.StartTime AS StartTime,
					sess.DetentionDate as EndDate,
					sess.EndTime AS EndTime,
					'No' as AllDayEvent,
					'0' as IsAllDay,
					'' as url
					From DISCIPLINE_DETENTION_STUDENT_SESSION as stud 
					inner join DISCIPLINE_DETENTION_SESSION as sess on
						stud.DetentionID = sess.DetentionID
					inner join INTRANET_USER as u on
						stud.StudentID = u.UserID
					where $colName in ('$userID')
					ORDER By EventDate
				";				
		$result = $this->returnArray($sql);
		for ($i = 0; $i<count($result); $i++){
			
			$result[$i]["Title"] = intranet_undo_htmlspecialchars($result[$i]["Title"]);
			$result[$i]["PersonalNote"] = intranet_undo_htmlspecialchars($result[$i]["PersonalNote"]);
			$result[$i]["Description"] = intranet_undo_htmlspecialchars($result[$i]["Description"]);
		}		
		return $result;
		
	}
	
	
	function getDetentionEventDetail($eventID){
		global $intranet_session_language;
		$uid = $this->getCurrentUserID();
		if ($intranet_session_language=='en')
			$langCol = "u.EnglishName";
		else
			$langCol = "u.ChineseName";
		$fieldname  = "stud.RecordID as EventID, stud.RequestedBy as UserID,
		'0' as CalID, '' as RepeatID, 
		Concat(sess.DetentionDate,' ',sess.StartTime) AS EventDate, 
		Concat(sess.DetentionDate,' ',sess.EndTime) AS EndDate, ";
		$fieldname .= " stud.Reason as Description, 
		TIMESTAMPDIFF(Minute,Concat(sess.DetentionDate,' ',sess.StartTime),Concat(sess.DetentionDate,' ',sess.EndTime)) as Duration, 
		stud.StudentID,
		'0' as IsImportant,'0' as IsAllDay, 'P' as Access, sess.Location, 
		concat($langCol,' (',u.ClassName,' - ',u.ClassNumber,') ','".$this->Detention["name"]."') as Title,
		";
		$fieldname .= " c.PersonalNote ";
		
		$sql = "SELECT $fieldname 
				From DISCIPLINE_DETENTION_STUDENT_SESSION as stud 
				inner join DISCIPLINE_DETENTION_SESSION as sess on
					stud.DetentionID = sess.DetentionID
				inner join INTRANET_USER as u on
				stud.StudentID = u.UserID
				Left outer join CALENDAR_EVENT_PERSONAL_NOTE as c on 
					c.EventID = stud.RecordID and
					c.UserID = '".$uid."' and
					c.CalType = 7
				where stud.RecordID = '$eventID'
				";		
		$detail = $this->returnArray($sql);		
		$detail[0]["Title"] = intranet_undo_htmlspecialchars($detail[0]["Title"]);
		$detail[0]["PersonalNote"] = intranet_undo_htmlspecialchars($detail[0]["PersonalNote"]);
		$detail[0]["Description"] = intranet_undo_htmlspecialchars($detail[0]["Description"]);
		return $detail;
	}
	
	function getExternalEventDivPrefix($calType){
		switch($calType){
			case 1:
			return "eventSchool";
			case 4:
			return "eventHomework";
			case 5:
			return "eventEnrolAct";
			case 6:
			return "eventEnrolGroup";
			case 7:
			return "eventDetention";
		}
	} 
	
	function getExternalEventDetail($calType,$eventID){
		switch($calType){
			case 1:
			return $this->getSchoolEventDetail($eventID);
			case 4:
			return $this->getHomeworkEventDetail($eventID);
			case 5:
			return $this->getEnrolActivityEventDetail($eventID);
			case 6:
			return $this->getEnrolGroupEventDetail($eventID);
			case 7:
			return $this->getDetentionEventDetail($eventID);
		}
	}
	
	function getFormattedExternalExportEvent($calType,$CalID="",$StartDate='',$EndDate=''){
		switch($calType){
			case 1:
			return $this->returnFormattedSchoolExportEvent($CalID,$StartDate,$EndDate);
			case 4:
			return $this->returnFormattedHomeworkExportEvent();
			case '5-6':
			return $this->returnFormattedAllActivityExportEvent();
			case 5:
			return $this->returnFormattedUserActivityExportEvent();
			case 6:
			return $this->returnFormattedGroupActivityExportEvent();
			case 7:
			return $this->returnFormattedDetentionExportEvent();
		}
	}

	function getAllExternalRelatedEvent($lowBound="",$upBound="",$searchValue="",$useTempTable=false){
		global $plugin;
		$events = Array();
		$schoolEvent = $this->getAllSchoolEvent($lowBound,$upBound,$searchValue,$useTempTable);
		if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF || $plugin["iCalendarFullToAllUsers"]){
			$homeworkEvent = $this->getRelatedHomeworkEvent($lowBound,$upBound,$searchValue,$useTempTable);
			$activityEvent = $this->getRelatedAllEnrolActivity($lowBound,$upBound,$searchValue,$useTempTable);
			$detentionEvent = $this->getAllDetentionEvent($lowBound,$upBound,$searchValue,$useTempTable);
			if ($useTempTable)
				return;
			$events = array_merge_recursive($events, $homeworkEvent);
			$events = array_merge_recursive($events, $activityEvent);
			$events = array_merge_recursive($events, $detentionEvent);
		}
		if ($useTempTable)
				return;
		$events = array_merge_recursive($events, $schoolEvent);
		
		return $events;
	}
	
	function isExternalEvent($calType){
		return (isset($calType)&&($calType==1||$calType>3)&&!is_null($calType));
	}
	
	function formatExternalEvent($event,$startTs){
		$formattedEvent = Array();
		
		foreach($event as $evt){	
			$isExternal = $this->isExternalEvent($evt["CalType"]);
			$eventDate = date("Y-m-d",strtotime($evt["EventDate"]));
			if (strtotime($evt["EventDate"]) < $startTs){
				$eventDate = date("Y-m-d",$startTs);
			}
			$i = count($formattedEvent[$eventDate]);
			$formattedEvent[$eventDate][$i]["CalType"] = $evt["CalType"];
			$formattedEvent[$eventDate][$i]["EventID"] = $evt["EventID"];
			$formattedEvent[$eventDate][$i]["UserID"] = isset($evt["UserID"])?$evt["UserID"]:0;
			$formattedEvent[$eventDate][$i]["StudentID"] = $evt["StudentID"];
			$formattedEvent[$eventDate][$i]["EventDate"]=$evt["EventDate"];
			$formattedEvent[$eventDate][$i]["Duration"]=$evt["Duration"];
			$formattedEvent[$eventDate][$i]["EndDate"]=$evt["EndDate"];
			$formattedEvent[$eventDate][$i]["DueDate"]=$evt["DueDate"];
			$formattedEvent[$eventDate][$i]["RecordType"] = $evt["RecordType"];
			$formattedEvent[$eventDate][$i]["Title"]=intranet_undo_htmlspecialchars($evt["Title"]);		
			$formattedEvent[$eventDate][$i]["Description"] = $evt["Description"];
			$formattedEvent[$eventDate][$i]["IsAllDay"] = $evt["IsAllDay"];
			$formattedEvent[$eventDate][$i]["Access"] = $isExternal?"P":$evt["Access"];
			$formattedEvent[$eventDate][$i]["DateModified"] = $evt["DateModified"];
			
			if (isset($evt["LocationID"]) && $evt["LocationID"]!=0){ 
				$formattedEvent[$eventDate][$i]["Location"]=$this->returnFullAddr($evt["LocationID"]);
			}
			else{
				$formattedEvent[$eventDate][$i]["Location"]=isset($evt["Location"])?$evt["Location"]:"";
			}
			$formattedEvent[$eventDate][$i]["PersonalNote"]=$evt["PersonalNote"];
			$formattedEvent[$eventDate][$i]["CalAccess"]=$evt["CalAccess"];
			$formattedEvent[$eventDate][$i]["CalID"]=$evt["CalID"];
			$formattedEvent[$eventDate][$i]["RepeatID"]=$evt["RepeatID"];
			$formattedEvent[$eventDate][$i]["IsImportant"]=$evt["IsImportant"];
			$formattedEvent[$eventDate][$i]["Url"]=$evt["Url"];
			$formattedEvent[$eventDate][$i]["Status"]=$isExternal?"A":$evt["Status"];
			$formattedEvent[$eventDate][$i]["Permission"]=$isExternal?"R":$evt["Permission"];
			if ($isExternal){
				$Config = $this->getExternalCalConfig($evt["CalType"],$evt["RecordType"]);
				$formattedEvent[$eventDate][$i]["Color"]=$Config["color"];
				$formattedEvent[$eventDate][$i]["Visible"]=$Config["visible"];
				$formattedEvent[$eventDate][$i]["DivClass"]=$Config["DivClass"];
				$formattedEvent[$eventDate][$i]["DivPrefix"]= $this->getExternalEventDivPrefix($evt["CalType"]);
			}
			else{
				$formattedEvent[$eventDate][$i]["Color"]=$evt["Color"];
				$formattedEvent[$eventDate][$i]["Visible"]=$evt["Visible"];
			}
		}
		
		return $formattedEvent;
	}
	
	function searchCaseParser($searchCase){
		$result = Array();
		if (is_array($searchCase)){
			foreach($searchCase as $keyword)
				$result[] = $this->keywordParser($keyword);
		}
		else{
			$result[] = $this->keywordParser($searchCase);
		}
		return $result;
	} 
	
	function keywordParser($keyword){
		//$pos = stripos($keyword,'SchoolName:');
		/*$schoolName = '';
		if ($pos !== false){
			$schoolName = substr($keyword,$pos+11);
			$keyword=substr($keyword,0,$pos);
		}*/
		$quoteNum = substr_count($keyword,'"');
		$keyword = stripslashes($keyword);		
		$keyword = trim($keyword);
		$subCase = Array();
		if ($quoteNum>1){
			$subCase = explode('"',$keyword);
		} 
		else
			$subCase[] = $keyword;
		$toggle = false;
		if ($keyword{0}=='"')
			$toggle == true;
		$result = $this->subCaseParser($subCase,$toggle);
		//$result[] = $schoolName;
		return $result;
	}
	
	function subCaseParser($subCase,$toggle){
		$result = Array();
		$quoteCase = $toggle?0:1;		
		if (count($subCase)%2==0){
			$last_ele = array_pop($subCase);
			$subCase[count($subCase)-1] .= ('"'.$last_ele);
		}
		for($i=0; $i<count($subCase); $i++){
			$value =  trim($subCase[$i]);			
			if ($i%2==$quoteCase){
				if ($value!='' && $value != '"'){
					$result[][0] = urldecode(trim(stripslashes($value)));
				}
			}
			else{				
				if ($value=='' || $value == '"')
					continue;
				$str = strtok($value,',');
				while($str){
					$str = trim($str);
					if ($str!='')
						$result[] =$this->getORCase($str);
					$str = strtok(',');
				}
			}
		}
		return $result;
	}
	
	function getORCase($value){
		$result = Array();
		$str = explode(' ',$value);
		foreach($str as $s){
			$temp = trim($s);
			if ($s != ''){
				$result[]=urldecode(trim(stripslashes($temp)));
			}			
		}
		return $result;
	}
	
	function formatSearchValueString($searchValue){
		$str = '';
		foreach($searchValue as $sv){
			$str .= "'".$sv."', ";
		}
		return rtrim($str,', ');
	}
	
	function SearchSqlString($searchCase,$tableCol){
		$keySearchSql = ''; 
		if (!empty($searchCase)){
			foreach($searchCase as $s){
				if (empty($s))
					continue;
				//$schoolName = array_pop($s);
				$keySearchSql .= ' And ( ';
				foreach ($s as $key){
					foreach($key as $andCase){
						$keySearchSql .= ' ( ';
						foreach($tableCol as $col){
							$keySearchSql .=" $col like '%".$andCase."%' OR ";
						}
						$keySearchSql = rtrim($keySearchSql,'OR ');
						$keySearchSql .= ' ) OR ';
					}
					$keySearchSql = rtrim($keySearchSql,'OR ');
					$keySearchSql .= ' AND ';
				}
				$keySearchSql = rtrim($keySearchSql,'AND ');
				//$keySearchSql .= " And SchoolName"
				$keySearchSql .= ' ) ';
			}
		} 
		return $keySearchSql;
	}
	
	function CreateTempEventTable(){
		$sql = "create temporary table TempEvent (
			EventID int(8),
			UserID int(8),
			CalID int(8),
			RepeatID int(8),
			EventDate datetime,
			Title varchar(255),
			Duration int(8),
			Description mediumtext,
			IsImportant char(1),
			IsAllDay char(1),
			Access char(1),
			Location varchar(255),
			Url mediumtext,
			Status char(1),
			PersonalNote mediumtext,
			Permission char(1),
			Color char(6),
			Visible char(1),
			CalAccess char(1),
			CalType tinyint(4),
			EndDate datetime,
			DueDate datetime,
			RecordType char(1),
			LocationID int(8),
			StudentID int(8),
			ModifyBy int(8),
			DateModified datetime  
		)ENGINE=InnoDB DEFAULT CHARSET=utf8";
		$this->db_db_query($sql); 
	}
	
	function GetCalenderEventsForCheckingAvailability($userIdAry, $startTimestamp, $endTimestamp, $calType=0)
	{
		global $Lang, $sys_custom;
		
		$name_field = getNameFieldByLang2("u.");
		// calender viewer events
		/*
		$sql = "select e.EventID,v.UserID, $name_field as UserName, e.CalID,DATE_FORMAT(e.EventDate,'%Y-%m-%d %H:%i:%s') as StartTime, DATE_FORMAT(TIMESTAMPADD(MINUTE,e.duration,e.EventDate),'%Y-%m-%d %H:%i:%s') as EndTime, UNIX_TIMESTAMP(e.EventDate) as StartTimestamp, (UNIX_TIMESTAMP(TIMESTAMPADD(MINUTE,e.duration,e.EventDate))-1) as EndTimestamp, e.Title 
				from CALENDAR_EVENT_ENTRY as e 
				inner join CALENDAR_CALENDAR_VIEWER as v on v.CalID=e.CalID 
				inner join CALENDAR_CALENDAR as c on c.CalID = e.CalID 
				inner join INTRANET_USER as u ON (u.UserID=v.UserID) 
				where c.CalType ".(is_array($calType)?" IN ('".implode("','",$calType)."')" : "='$calType'")." AND v.UserID in ('".implode("','",$userIdAry)."') and 
				(UNIX_TIMESTAMP(e.EventDate) between $startTimestamp and $endTimestamp OR
				UNIX_TIMESTAMP(TIMESTAMPADD(Minute,e.Duration,e.EventDate)) between $startTimestamp and $endTimestamp OR
				(UNIX_TIMESTAMP(e.EventDate) <= $startTimestamp AND
				UNIX_TIMESTAMP(TIMESTAMPADD(Minute,e.Duration,e.EventDate)) >= $endTimestamp) 
				) 
				order by e.EventDate";
		*/
		/*
		$sql = "select e.EventID,e.UserID, $name_field as UserName, e.CalID,DATE_FORMAT(e.EventDate,'%Y-%m-%d %H:%i:%s') as StartTime, DATE_FORMAT(TIMESTAMPADD(MINUTE,e.duration,e.EventDate),'%Y-%m-%d %H:%i:%s') as EndTime, UNIX_TIMESTAMP(e.EventDate) as StartTimestamp, (UNIX_TIMESTAMP(TIMESTAMPADD(MINUTE,e.duration,e.EventDate))-1) as EndTimestamp, e.Title 
				from CALENDAR_EVENT_ENTRY as e 
				inner join CALENDAR_CALENDAR as c on c.CalID = e.CalID 
				inner join INTRANET_USER as u ON (u.UserID=e.UserID) 
				where c.CalType ".(is_array($calType)?" IN ('".implode("','",$calType)."')" : "='$calType'")." AND e.UserID in ('".implode("','",$userIdAry)."') and 
				(UNIX_TIMESTAMP(e.EventDate) between $startTimestamp and $endTimestamp OR
				UNIX_TIMESTAMP(TIMESTAMPADD(Minute,e.Duration,e.EventDate)) between $startTimestamp and $endTimestamp OR
				(UNIX_TIMESTAMP(e.EventDate) <= $startTimestamp AND
				UNIX_TIMESTAMP(TIMESTAMPADD(Minute,e.Duration,e.EventDate)) >= $endTimestamp) 
				) 
				order by e.EventDate";
		$events = $this->returnResultSet($sql);
		*/
		$title_field = $sys_custom['iCalendar']['EventTitleWithParticipantNames']? "REPLACE(e.Title,IFNULL(e.TitleExtraInfo,''),'')" : "e.Title";
		// invited events
		$sql = "select e.EventID,ceu.UserID, $name_field as UserName, e.CalID,DATE_FORMAT(e.EventDate,'%Y-%m-%d %H:%i:%s') as StartTime, DATE_FORMAT(TIMESTAMPADD(MINUTE,e.duration,e.EventDate),'%Y-%m-%d %H:%i:%s') as EndTime, UNIX_TIMESTAMP(e.EventDate) as StartTimestamp, (UNIX_TIMESTAMP(TIMESTAMPADD(MINUTE,e.duration,e.EventDate))-1) as EndTimestamp, IF(c.CalType='0','".$Lang['iCalendar']['PersonalEvent']."',$title_field) as Title 
				from CALENDAR_EVENT_ENTRY as e 
				inner join CALENDAR_EVENT_USER as ceu on ceu.EventID=e.EventID 
				inner join CALENDAR_CALENDAR as c on c.CalID = e.CalID 
				inner join INTRANET_USER as u ON u.UserID=ceu.UserID
				where c.CalType ".(is_array($calType)?" IN ('".implode("','",$calType)."')" : "='$calType'")." AND ceu.UserID in ('".implode("','",$userIdAry)."') and 
				(UNIX_TIMESTAMP(e.EventDate) between $startTimestamp and $endTimestamp OR
				UNIX_TIMESTAMP(TIMESTAMPADD(Minute,e.Duration,e.EventDate)) between $startTimestamp and $endTimestamp OR
				(UNIX_TIMESTAMP(e.EventDate) <= $startTimestamp AND
				UNIX_TIMESTAMP(TIMESTAMPADD(Minute,e.Duration,e.EventDate)) >= $endTimestamp) 
				) 
				order by e.EventDate";
		$events = $this->returnResultSet($sql);
		
		//if(count($events2)>0){
		//	$events = array_merge($events, $events2);
		//}
		
		return $events;
	}
	
	function GetEnrolmentEventsForCheckingAvailability($userIdAry, $startTimestamp, $endTimestamp)
	{
		global $Lang;
		
		$name_field = getNameFieldByLang2("u.");
		$sql = "SELECT 
					e.EnrolEventID as EventID, s.UserID, $name_field as UserName, IF(e.GroupID=0,5,6) as CalID, DATE_FORMAT(d.ActivityDateStart,'%Y-%m-%d %H:%i:%s') as StartTime, DATE_FORMAT(d.ActivityDateEnd,'%Y-%m-%d %H:%i:%s') as EndTime, UNIX_TIMESTAMP(d.ActivityDateStart) as StartTimestamp, UNIX_TIMESTAMP(d.ActivityDateEnd) as EndTimestamp, e.EventTitle as Title
				FROM INTRANET_ENROL_EVENTINFO as e  
				INNER JOIN INTRANET_ENROL_EVENT_DATE as d on e.EnrolEventID = d.EnrolEventID 
				INNER JOIN INTRANET_ENROL_EVENTSTAFF as s ON s.EnrolEventID=e.EnrolEventID 
				INNER JOIN INTRANET_USER as u ON u.UserID=s.UserID 
				WHERE d.RecordStatus=1 AND s.UserID IN ('".implode("','",$userIdAry)."') AND 
				(UNIX_TIMESTAMP(d.ActivityDateStart) between $startTimestamp and $endTimestamp OR
				UNIX_TIMESTAMP(d.ActivityDateEnd) between $startTimestamp and $endTimestamp OR
				(UNIX_TIMESTAMP(d.ActivityDateStart) <= $startTimestamp AND
				UNIX_TIMESTAMP(d.ActivityDateEnd) >= $endTimestamp))
				ORDER BY d.ActivityDateStart";
		$enrol_events = $this->returnResultSet($sql);
		
		return $enrol_events;
	}
	
	function GetDetentionEventsForCheckingAvailability($userIdAry, $startTimestamp, $endTimestamp)
	{
		global $Lang;
		
		$name_field = getNameFieldByLang2("u.");
		$EventDate = date("Y-m-d",$startTimestamp);
		
		$sql = "SELECT 
					s.DetentionID as EventID,p.UserID, $name_field as UserName, 7 as CalID, CONCAT(s.DetentionDate,' ',s.StartTime) as StartTime,  CONCAT(s.DetentionDate,' ',s.EndTime) as EndTime, UNIX_TIMESTAMP(CONCAT(s.DetentionDate,' ',s.StartTime)) as StartTimestamp, UNIX_TIMESTAMP(CONCAT(s.DetentionDate,' ',s.EndTime)) as EndTimestamp, CONCAT('Detention @ ',s.Location) as Title 
				FROM DISCIPLINE_DETENTION_SESSION as s 
				INNER JOIN DISCIPLINE_DETENTION_SESSION_PIC as p ON p.DetentionID=s.DetentionID 
				INNER JOIN INTRANET_USER as u ON u.UserID=p.UserID 
				WHERE s.DetentionDate='$EventDate' AND p.UserID IN ('".implode("','",$userIdAry)."') AND
				(UNIX_TIMESTAMP(CONCAT(s.DetentionDate,' ',s.StartTime)) between $startTimestamp and $endTimestamp OR
				UNIX_TIMESTAMP(CONCAT(s.DetentionDate,' ',s.EndTime)) between $startTimestamp and $endTimestamp OR
				(UNIX_TIMESTAMP(CONCAT(s.DetentionDate,' ',s.StartTime)) <= $startTimestamp AND
				UNIX_TIMESTAMP(CONCAT(s.DetentionDate,' ',s.EndTime)) >= $endTimestamp)) ";
		$detention_events = $this->returnResultSet($sql);
		
		return $detention_events;
	}
	
	// for the teachers only
	function GetTimetableLessonsForCheckingAvailability($userIdAry, $startTimestamp, $endTimestamp)
	{
		global $intranet_root, $PATH_WRT_ROOT, $Lang;
		
		include_once($intranet_root."/includes/libtimetable.php");
		include_once($intranet_root.'/includes/libcardstudentattend2.php');
		
		$lc = new libcardstudentattend2();
		$libtimetable = new Timetable();
		
		
		$TargetDate = date("Y-m-d",$startTimestamp);
	  	$timetableID = $libtimetable->Get_Current_Timetable($TargetDate);
	  	
	  	if(trim($timetableID)!='' && $timetableID > 0){
	  		$CycleDayInfo = $lc->Get_Attendance_Cycle_Day_Info($TargetDate);
			if ($CycleDayInfo[0] == 'Cycle') {
				$DayNumber = $lc->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
				$DayCond = " and itra.Day='$DayNumber' ";
			}else{
				$DayCond = " and itra.Day=DAYOFWEEK('".$TargetDate."')-1 ";
			}
	  		
	  		$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
			list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
	  		if( $AcademicYearID == "") {
	  			$AcademicYearID = Get_Current_Academic_Year_ID();
	  		}
	  		
	  		$name_field = getNameFieldByLang2("u.");
	  		$subject_field = Get_Lang_Selection('s.CH_DES','s.EN_DES');
	  		$subject_class_field = Get_Lang_Selection('stc.ClassTitleB5','stc.ClassTitleEN');
	  		
	  		$sql = "SELECT 
						  stc.SubjectGroupID as EventID,
						  stct.UserID,
						  $name_field as UserName,
						  8 as CalID,
						  CONCAT('$TargetDate ',itt.StartTime) as StartTime,
						  CONCAT('$TargetDate ',itt.EndTime) as EndTime,
						  UNIX_TIMESTAMP(CONCAT('$TargetDate ',itt.StartTime)) as StartTimestamp,
						  UNIX_TIMESTAMP(CONCAT('$TargetDate ',itt.EndTime)) as EndTimestamp,
						  CONCAT($subject_field,' - ',$subject_class_field) as Title  
					FROM ACADEMIC_YEAR_TERM as ayt
					INNER JOIN SUBJECT_TERM as st ON ayt.YearTermID = st.YearTermID AND ayt.AcademicYearID = '".$AcademicYearID."' 
					INNER JOIN ASSESSMENT_SUBJECT as s ON s.RecordID = st.SubjectID 
					INNER JOIN SUBJECT_TERM_CLASS as stc ON st.SubjectGroupID = stc.SubjectGroupID 
				 	INNER JOIN SUBJECT_TERM_CLASS_TEACHER as stct ON stc.SubjectGroupID = stct.SubjectGroupID 
					INNER JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION as itra ON stc.SubjectGroupId = itra.SubjectGroupID and itra.TimetableID = '".$timetableID."' $DayCond 
					INNER JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON itra.TimeSlotID = itt.TimeSlotID 
					LEFT JOIN INTRANET_USER as u ON u.UserID=stct.UserID 
					WHERE stct.UserID IN ('".implode("','",$userIdAry)."') AND 
						(UNIX_TIMESTAMP(CONCAT('$TargetDate ',itt.StartTime)) between $startTimestamp and $endTimestamp OR
						UNIX_TIMESTAMP(CONCAT('$TargetDate ',itt.EndTime)) between $startTimestamp and $endTimestamp OR
						(UNIX_TIMESTAMP(CONCAT('$TargetDate ',itt.StartTime)) <= $startTimestamp AND
						UNIX_TIMESTAMP(CONCAT('$TargetDate ',itt.EndTime)) >= $endTimestamp))
					ORDER BY itt.StartTime";
			
			return $this->returnResultSet($sql);
	  	}
		
		return array();
	}
}
?>