<?php
// page editing by: 
/********************************** Change Log ***********************************
 * 2011-03-04 (Carlos): add createTable_Card_Staff_Attendance2_Raw_Log()
 *********************************************************************************/
if (!defined("LIBSTAFFATTEND2_DEFINED"))                // Preprocessor directives
{
  # Constant definition
  define("LIBSTAFFATTEND2_DEFINED",true);
  define("CARD_STATUS_NOSETTING",-1);
  define("CARD_STATUS_NORMAL",0);
  define("CARD_STATUS_ABSENT",1);
  define("CARD_STATUS_LATE",2);
  define("CARD_STATUS_EARLYLEAVE", 3);
  define("CARD_STATUS_HOLIDAY",4);
  define("CARD_STATUS_OUTGOING",5);

class libstaffattend2 extends libdb
{
  function libstaffattend2() {
  	$this->libdb();
  }
	
	// code added on 20080305, by kenneth chung to allow the user to select the year and month
	function getExistingAttendanceYearMonth() {
		$sql = 'show tables like \'CARD_STAFF_ATTENDANCE2_DAILY_LOG_%\'';

    return $this->returnArray($sql);
	}
	
	// code added on 20080305, by kenneth chung to allow the user to select the year and month
	function getYearMonthSelector($SelectYear, $SelectMonth) {
		$AllAttendTables = $this->getExistingAttendanceYearMonth();
		sort($AllAttendTables);
		for ($i=0; $i< sizeof($AllAttendTables); $i++) {
			$tempArray = explode("_",$AllAttendTables[$i][0]);
			
			if (sizeof($yearList) > 0) {
				if ($tempArray[sizeof($tempArray)-2] == $yearList[sizeof($yearList)-1]) {
					$yearList[sizeof($yearList)-1][] = $tempArray[sizeof($tempArray)-1];
				}
				else {
					$yearList[$tempArray[sizeof($tempArray)-2]][] = $tempArray[sizeof($tempArray)-1];
				}
			}
			else {
				$yearList[$tempArray[sizeof($tempArray)-2]][] = $tempArray[sizeof($tempArray)-1];
			}
			
		}
		/*echo '<pre>';
		var_dump($yearList);
		echo '</pre>';*/
		
		// build the Year selection structure
		$YearSelector = "<select name=\"SelectYear\">";
		for ($i=0; $i< sizeof($yearList); $i++) {
			$selected = ($SelectYear == key($yearList)) ? "selected":"";
			$YearSelector .= "<option value=\"".key($yearList)."\" ".$selected.">".key($yearList)."</option>";
			next($yearList);
		}
		$YearSelector .= "</select>";
		
		// build the Month selection structure
		reset($yearList);
		$MonthSelector = "<select name=\"SelectMonth\" >";
		for ($i=1; $i< 13; $i++) {
			$selected = ($SelectMonth == $i) ? " selected ":"";
			$MonthSelector .= "<option value=\"";
			$MonthSelector .= ($i<10) ? '0'.$i : $i; 
			$MonthSelector .= "\" ".$selected.">";
			$MonthSelector .= ($i<10) ? '0'.$i : $i; 
			$MonthSelector .= "</option>";
		}
		$MonthSelector .= "</select>";
		
		$Output = $YearSelector."- ".$MonthSelector;
		return $Output;
	}
	
  function createTable_Card_Staff_Attendance2_Daily_Log($year="", $month="") {
    $year = ($year == "") ? date("Y") : $year;
    $month = ($month == "") ? date("m") : $month;

    $card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$year."_".$month;
    $sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name (
              RecordID int(11) NOT NULL auto_increment,
              StaffID int(11) NOT NULL,
              DayNumber int(11) NOT NULL,
              Duty int,
              DutyStart time,
              DutyEnd time,
              StaffPresent int,
              InTime time,
              InSchoolStatus int,
              InSchoolStation varchar(100),
              InWaived int,
              InAttendanceRecordID int,
              OutTime time,
              OutSchoolStatus int,
              OutSchoolStation varchar(100),
              OutWaived int,
              OutAttendanceRecordID int,
              MinLate int,
              MinEarlyLeave int,
              RecordType int,
              RecordStatus int,
              DateInput datetime,
              DateModified datetime,
              PRIMARY KEY (RecordID),
              UNIQUE StaffDay (StaffID, DayNumber),
              INDEX DayNumber (DayNumber),
              INDEX StaffID (StaffID)
    			) ENGINE=InnoDB charset=utf8";
    $this->db_db_query($sql);
    return $card_log_table_name;
  }
  	
  	function createTable_Card_Staff_Attendance2_Raw_Log($year="",$month="")
  	{
  		$year = ($year == "") ? date("Y") : $year;
    	$month = ($month == "") ? date("m") : $month;
    	$raw_log_table_name = "CARD_STAFF_ATTENDANCE2_RAW_LOG_".$year."_".$month;
        $sql = "CREATE TABLE IF NOT EXISTS $raw_log_table_name (
		          RecordID int(11) NOT NULL auto_increment,
		          UserID int(11) NOT NULL,
		          DayNumber int(11) NOT NULL,
		          RecordTime time,
		          RecordStation varchar(100),
		          RecordType int,
		          RecordStatus int,
		          DateInput datetime,
		          DateModified datetime,
		          PRIMARY KEY (RecordID),
		          INDEX DayNumber (DayNumber),
		          INDEX UserID (UserID)
		         ) ENGINE=InnoDB charset=utf8";
        $this->db_db_query($sql);
        return $raw_log_table_name;
  	}
  	
  	# created by ronald on 13 Dec 2007 #
	# create staff patrol log table is not exist #
	function createTable_Staff_Patrol_Log_Month_Table($year="", $month="")
	{
			$year = ($year == "") ? date("Y") : $year;
			$month = ($month == "") ? date("m") : $month;
			
	     	$tablename = "CARD_STAFF_PATROL_LOG_".$year."_".$month;
	     	$sql = "CREATE TABLE IF NOT EXISTS $tablename (
					RecordID int(11) NOT NULL auto_increment,
					StaffID int(11) NOT NULL, 
					CardID varchar(255) NOT NULL, 
					DayNumber int(11) NOT NULL, 
					SiteName varchar(20) NOT NULL, 
					RecordTime time, 
					RecordType int(11), 
					RecordStatus int(11), 
					DateInput datetime, 
					DateModified datetime, 
					PRIMARY KEY (RecordID), 
					INDEX ByStaff (StaffID), 
					INDEX ByDay (DayNumber), 
					INDEX BySite (SiteName)) ENGINE=InnoDB charset=utf8";
	    	$this->db_db_query($sql);
	}

  function retrieveGroupDailyRecord ($StaffIDs, $year, $month, $day, $SortOrder=NULL)
  {
    global $i_StaffAttendance_Result_Absent, $i_StaffAttendance_Result_Late, $i_StaffAttendance_Result_EarlyLeave, $i_StaffAttendance_Result_Normal, $i_StaffAttendance_Result_Waived, $i_StaffAttendance_Result_NotWaived;
    $this->createTable_Card_Staff_Attendance2_Daily_Log($year, $month);
    $list = (is_array($StaffIDs)) ? implode(",", $StaffIDs) : "";

    $card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$year."_".$month;
    $sql = "SELECT
		          RecordID,
		          StaffID,
		          Duty,
		          DutyStart,
		          DutyEnd,
		          IF(InTime IS NULL, '--', InTime) as \"InTime\",
		          InSchoolStatus,
		          IF(InWaived IS NULL, '--', IF(InWaived=0, '$i_StaffAttendance_Result_NotWaived', '$i_StaffAttendance_Result_Waived')),
		          IF(OutTime IS NULL, '--', OutTime) as \"OutTime\",
		          OutSchoolStatus,
		          IF(OutWaived IS NULL, '--', IF(OutWaived=0, '$i_StaffAttendance_Result_NotWaived', '$i_StaffAttendance_Result_Waived')),
		          StaffPresent ,
		          InSchoolStation,
		          OutSchoolStation
          FROM
              $card_log_table_name
          WHERE
              DayNumber = $day
              AND StaffID IN ($list)
          ";
    // code add on 2007-12-19, add sort order to function from HTML form
    if ($SortOrder) {
    	$sql .= "Order by ".$SortOrder;
  	}
  	
    $temp = $this->returnArray($sql, 14);
    # Process Array
    for ($i=0; $i<sizeof($temp); $i++)
    {
		  list($record_id, $staff_id, $duty, $dutystart, $dutyend, $intime, $inschoolstatus, $inwaived, $outtime, $outschoolstatus, $outwaived,$staffpresent,$inschoolstation,$outschoolstation) = $temp[$i];
		  if($dutystart=="00:00:00"|| $dutystart==""){
		  	if($dutyend=="00:00:00" || $dutyend==""){
		  		$dutystart="0";
		  		$dutyend="0";
		  	}
      }
        	
      $Record[$staff_id]['Record'] = $record_id;
      $Record[$staff_id]['Duty'] = $duty;
      $Record[$staff_id]['DutyStart'] = $dutystart;
      $Record[$staff_id]['DutyEnd'] = $dutyend;
      $Record[$staff_id]['InTime'] = $intime;
      $Record[$staff_id]['InSchoolStatus'] = $inschoolstatus;
      $Record[$staff_id]['InWaived'] = $inwaived;
      $Record[$staff_id]['OutTime'] = $outtime;
      $Record[$staff_id]['OutSchoolStatus'] = $outschoolstatus;
      $Record[$staff_id]['OutWaived'] = $outwaived;
      $Record[$staff_id]['StaffPresent']=$staffpresent;
      $Record[$staff_id]['InSchoolStation']=$inschoolstation;
      $Record[$staff_id]['OutSchoolStation']=$outschoolstation;
    }
    return $Record;
  }
	
	/*---------------------------------- 
		Code Ended : 2007-12-19
		Coded by   : Kenneth Chung
		Description: This method replace "retrieveGroupDailyRecord", 
								 edited the sql structure to allow sort by "Status" 
								 and "Time"
	  ----------------------------------*/
	
	function retrieveGroupDailyRecordV2 ($UserNameField, $GroupID, $year, $month, $day, $SortOrder=NULL, $SortType=NULL) {
    global $i_StaffAttendance_Result_Absent, $i_StaffAttendance_Result_Late, $i_StaffAttendance_Result_EarlyLeave, $i_StaffAttendance_Result_Normal, $i_StaffAttendance_Result_Waived, $i_StaffAttendance_Result_NotWaived;
    $this->createTable_Card_Staff_Attendance2_Daily_Log($year, $month);

    $card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$year."_".$month;
    $sql = "SELECT
    					a.UserID as \"UserID\",
    					".$UserNameField." as \"Name\", 
    					a.GroupID as \"GroupID\", 
    					c.RecordID as \"Record\",
	            c.StaffID as \"StaffID\",
	            c.Duty as \"Duty\",
	            c.DutyStart as \"DutyStart\",
	            c.DutyEnd as \"DutyEnd\",
	            IF(c.InTime IS NULL, '--', c.InTime) as \"InTime\",
	            c.InSchoolStatus as \"InSchoolStatus\",
	            IF(c.InWaived IS NULL, '--', IF(c.InWaived=0, '".$i_StaffAttendance_Result_NotWaived."', '".$i_StaffAttendance_Result_Waived."')),
	            IF(c.OutTime IS NULL, '--', c.OutTime) as \"OutTime\",
	            c.OutSchoolStatus as \"OutSchoolStatus\",
	            IF(c.OutWaived IS NULL, '--', IF(c.OutWaived=0, '".$i_StaffAttendance_Result_NotWaived."', '".$i_StaffAttendance_Result_Waived."')),
	            c.StaffPresent as \"StaffPresent\",
	            c.InSchoolStation as \"InSchoolStation\",
	            c.OutSchoolStation as \"OutSchoolStation\"
    				FROM
				      CARD_STAFF_ATTENDANCE_USERGROUP AS a
				      inner join
				      INTRANET_USER AS b on a.UserID=b.UserID
				      left join
				      ".$card_log_table_name." as c on a.UserID=c.StaffID
				      and 
				    	c.DayNumber = ".$day."
				    where 
				    	a.GroupID in (".$GroupID.")"
				    ;

    // code add on 2007-12-19, add sort order to function from HTML form
    if ($SortOrder) {
    	$sql .= " Order by ".$SortOrder." ".$SortType;
  	}
  	//echo $sql."<br>";
    $temp = $this->returnArray($sql, 17);
    # Process Array
    for ($i=0; $i<sizeof($temp); $i++)
    {
    		$UserID = $temp[$i]['UserID'];
        if($dutystart=="00:00:00"|| $dutystart==""){
          	if($dutyend=="00:00:00" || $dutyend==""){
          		$dutystart="0";
          		$dutyend="0";
          	}
      	}
    		$Record[$i]['StaffID'] = $temp[$i][0];
    		$Record[$i]['Name'] = $temp[$i][1];
    		$Record[$i]['GroupID'] = $temp[$i][2];
        $Record[$i]['Record'] = $temp[$i][3];
        $Record[$i]['Duty'] = $temp[$i][5];
        $Record[$i]['DutyStart'] = $temp[$i][6];
        $Record[$i]['DutyEnd'] = $temp[$i][7];
        $Record[$i]['InTime'] = $temp[$i][8];
        $Record[$i]['InSchoolStatus'] = $temp[$i][9];
        $Record[$i]['InWaived'] = $temp[$i][10];
        $Record[$i]['OutTime'] = $temp[$i][11];
        $Record[$i]['OutSchoolStatus'] = $temp[$i][12];
        $Record[$i]['OutWaived'] = $temp[$i][13];
        $Record[$i]['StaffPresent']= $temp[$i][14];
        $Record[$i]['InSchoolStation']= $temp[$i][15];
        $Record[$i]['OutSchoolStation']= $temp[$i][16];
    }

    return $Record;
  }

/*---------------------------------- 
		Code Ended : 2007-12-24
		Coded by   : Kenneth Chung
		Description: This method get all staff absent/ earily Leave record from DB 
								 and return these record value by array, 
								 for the report of "Staff Absent Report" function
	  ----------------------------------*/
function getStaffDateRangeAbsRecord($FieldName, $FromDate, $EndDate, $Spacer="&nbsp;", $StaffID) {
	$sql ="SELECT
			  ".$FieldName." as Name,
			  A.RecordDate as Date,
			  '1' as NumberOfDate,
			  if (B.ReasonTypeName is NULL, '".$Spacer."', B.ReasonTypeName) as ReasonType,
			  if (A.Reason = '', '".$Spacer."', A.Reason) as Reason,
			  '".$Spacer."' AS Remark
			FROM
			  CARD_STAFF_ATTENDANCE2_PROFILE AS A
			  LEFT JOIN
			  CARD_STAFF_ATTENDANCE2_REASON_TYPE AS B
			  on A.ReasonType = B.TypeID
			  inner join
			  INTRANET_USER as c
			  on A.StaffID = c.UserID
			where
			  A.RecordType in (1, 3)
			  and
			  A.RecordDate between '".$FromDate."' and '".$EndDate."' ";
	
	if ($StaffID != '') {
		$sql .= "and A.StaffID='".$StaffID."' ";
	}

	$sql .= "order by 
			  c.EnglishName, 
			  A.RecordDate";
	$result = $this->returnArray($sql,6);
	
	for($i=0;$i< sizeof($result);$i++) {
		$ReturnResult[$i]['Name'] = $result[$i][0];
		$ReturnResult[$i]['Date'] = $result[$i][1];
		$ReturnResult[$i]['NumberOfDate'] = $result[$i][2];
		$ReturnResult[$i]['ReasonType'] = $result[$i][3];
		$ReturnResult[$i]['Reason'] = $result[$i][4];
		$ReturnResult[$i]['Remark'] = $result[$i][5];
	}
	
	return $ReturnResult;
}

/*---------------------------------- 
		Code Ended : 2007-12-24
		Coded by   : Kenneth Chung
		Description: Method that get all absent 
								 record and summarized them 
								 into count by StaffID
	  ----------------------------------*/
function getStaffDateRangeAbsByNameSummaryRecord($FieldName, $FromDate, $EndDate, $StaffID=NULL, $DisplayWaive) {
	$sql ="SELECT
					c.UserID as 'UserID', 
				  ".$FieldName." as 'Name',
				  B.TypeID as 'TypeID',
				  count(*) as 'NumberOfDay',
				  IF (B.ReasonTypeName IS NULL, 'Undefined', B.ReasonTypeName) as 'ReasonDesc' 
				FROM
				  CARD_STAFF_ATTENDANCE2_PROFILE AS A
				  LEFT JOIN
				  CARD_STAFF_ATTENDANCE2_REASON_TYPE AS B
				  on A.ReasonType = B.TypeID
				  inner join INTRANET_USER as c
				  on A.StaffID = c.UserID
				where
				  A.RecordType in (1, 3)
				  and
				  A.RecordDate between '".$FromDate."' and '".$EndDate."'
				";
	if (!is_null($StaffID)) {
		$sql .= "and 
						c.UserID = '".$StaffID."' ";
	}	
	if ($DisplayWaive == 2) {
		$sql .= "and
						A.Waived = '0' ";
	}
	$sql .= "Group by
				  A.StaffID,
				  B.TypeID
				order by  
					B.TypeID";
	
	$result = $this->returnArray($sql,6);
	
	for ($i=0; $i< sizeof($result); $i++) {
		$ReturnResult[$i]['UserID'] = $result[$i][0];
		$ReturnResult[$i]['Name'] = $result[$i][1];
		$ReturnResult[$i]['TypeID'] = $result[$i][2];
		$ReturnResult[$i]['NumberOfDay'] = $result[$i][3];
		$ReturnResult[$i]['ReasonDesc'] = $result[$i][4];
	}
	
	return $ReturnResult;
}

/*---------------------------------- 
		Code Ended : 2007-12-24
		Coded by   : Kenneth Chung
		Description: Method that get all absent record in
								 detail by staff ID
	  ----------------------------------*/
function getStaffDateRangeAbsByNameDetailRecord($FieldName, $FromDate, $EndDate, $StaffID=NULL, $DisplayWaive) {
	$sql ="SELECT
				  ".$FieldName." as 'Name',
				  B.TypeID as 'TypeID',
				  A.RecordDate as 'RecordDate', 
				  A.Waived as 'Waived'
				FROM
				  CARD_STAFF_ATTENDANCE2_PROFILE AS A
				  LEFT JOIN
				  CARD_STAFF_ATTENDANCE2_REASON_TYPE AS B
				  on A.ReasonType = B.TypeID 
				  inner join INTRANET_USER as c
				  on A.StaffID = c.UserID
				where
				  A.RecordType in (1, 3)
				  and 
				  A.RecordDate between '".$FromDate."' and '".$EndDate."' 
				";
	if (!is_null($StaffID)) {
		$sql .= "and 
						c.UserID = '".$StaffID."' ";
	}
	if ($DisplayWaive == 2) {
		$sql .= "and
						A.Waived = '0' ";
	}	  
	$sql .= "order by  
					B.TypeID, 
					A.RecordDate
				";
	$result = $this->returnArray($sql,6);
	
	for ($i=0;$i< sizeof($result);$i++) {
		$ReturnResult[$i]['Name'] = $result[$i][0];
		$ReturnResult[$i]['TypeID'] = $result[$i][1];
		$ReturnResult[$i]['RecordDate'] = $result[$i][2];
		$ReturnResult[$i]['Waived'] = $result[$i][3];
	}
	
	return $ReturnResult;
}

/*---------------------------------- 
		Code Ended : 2007-12-27
		Coded by   : Kenneth Chung
		Description: Method that get all Active/Inactive absent reason type record from db
	  ----------------------------------*/
function getAllAbsReasonType($Status) {
	$sql ="SELECT 
					TypeID, 
					ReasonTypeName 
				FROM 
					CARD_STAFF_ATTENDANCE2_REASON_TYPE C
				WHERE 
					RecordStatus = ".$Status;
	$result = $this->returnArray($sql,2);
	
	$returnArray = Array();
	for ($i=0;$i< sizeof($result); $i++) {
		$returnArray[$i]['TypeID'] = $result[$i][0];
		$returnArray[$i]['ReasonTypeName'] = $result[$i][1];
	}
	
	return $returnArray;
}

/*---------------------------------- 
		Code Ended : 2007-12-27
		Coded by   : Kenneth Chung
		Description: Method that get all Inactive absent reason type record Used in a time range from db
	  ----------------------------------*/
function getUsedInActiveReasonTypeByTimeRange($FromDate,$EndDate) {
	$sql = "SELECT DISTINCT
						B.TypeID, 
						B.ReasonTypeName 
					FROM 
						CARD_STAFF_ATTENDANCE2_PROFILE AS A
				  	INNER JOIN
				  	CARD_STAFF_ATTENDANCE2_REASON_TYPE AS B
				  	on A.ReasonType = B.TypeID and B.RecordStatus = 0
				  WHERE
				  	A.RecordType in (1, 3)
					  and 
					  A.RecordDate between '".$FromDate."' and '".$EndDate."'";
	$result = $this->returnArray($sql,2);
				  	
	$returnArray = Array();
	for ($i=0;$i< sizeof($result); $i++) {
		$returnArray[$i]['TypeID'] = $result[$i][0];
		$returnArray[$i]['ReasonTypeName'] = $result[$i][1];
	}
	
	return $returnArray;
}

function createTable_Card_Staff_Attendance2_Inter_Log($year="", $month="")
{
        $year = ($year == "") ? date("Y") : $year;
        $month = ($month == "") ? date("m") : $month;

        $card_log_table_name = "CARD_STAFF_ATTENDANCE2_INTER_LOG_".$year."_".$month;
        $sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name (
                        RecordID int(11) NOT NULL auto_increment,
                        StaffID int(11) NOT NULL,
                        DayNumber int(11) NOT NULL,
                        RecordTime time,
                        RecordType int,
                        RecordStatus int,
                        DateInput datetime,
                        DateModified datetime,
                        PRIMARY KEY (RecordID),
                        INDEX StaffDay (StaffID, DayNumber),
                        INDEX DayNumber (DayNumber),
                        INDEX StaffID (StaffID)
        ) ENGINE=InnoDB charset=utf8";
        $this->db_db_query($sql);
        return $card_log_table_name;
}

# studentid  : student id or array of student id
# target_date: attendance date
# timestamp  : page load time 
# return  : true - expired ,  false - not expired
function isDataOutdated1($staff_id,$target_date,$timestamp){
	if($staff_id=="") return true;
    if(!is_array($staff_id)){
        $staff_id = array($staff_id);
    }
    $staff_list = implode(",",$staff_id);
    $target_ts = strtotime($target_date);
    $year = date('Y',$target_ts);
    $month = date('m',$target_ts);
    $day = date('j',$target_ts);
    $sql="SELECT 
    				COUNT(*) 
    			FROM 
    				CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$year."_".$month." 
    			WHERE 
    				StaffID IN ($staff_list) 
    				AND DayNumber = $day 
    				AND UNIX_TIMESTAMP(DateModified) > $timestamp";
    //echo $sql."<BR>";
    $temp = $this->returnVector($sql);
    return $temp[0]>0;
}

function isDataOutdated2($staff_id,$target_date,$timestamp){
	if($staff_id=="") return true;
    if(!is_array($staff_id)){
        $staff_id = array($staff_id);
    }
    $staff_list = implode(",",$staff_id);
    $target_ts = strtotime($target_date);
    $year = date('Y',$target_ts);
    $month = date('m',$target_ts);
    $day = date('j',$target_ts);
    
    $sql="SELECT COUNT(*) FROM CARD_STAFF_ATTENDANCE2_PROFILE WHERE StaffID IN ($staff_list) AND RecordDate = '$target_date' AND UNIX_TIMESTAMP(DateModified) > $timestamp";
    $temp = $this->returnVector($sql);
    return $temp[0]>0;
}

function Get_Attendenance_Data($name_field, $TargetGroup, $TargetDate, $Day, $year, $month, $day)
{
	//global $i_StaffAttendance_Result_Absent, $i_StaffAttendance_Result_Late, $i_StaffAttendance_Result_EarlyLeave, $i_StaffAttendance_Result_Normal, $i_StaffAttendance_Result_Waived, $i_StaffAttendance_Result_NotWaived;
	//global $i_StaffAttendance_Status_Absent, $i_StaffAttendance_Status_Normal, $i_StaffAttendance_Status_Late, $i_StaffAttendance_Status_Holiday, $i_StaffAttendance_Status_Outgoing;
	//global $i_StaffAttendance_Report_Staff_Off, $i_StaffAttendance_DataManagmenet_OTRecord;
	$sql = "Select 
						a.UserID, a.GroupID , $name_field AS 'Name'
					FROM
			      CARD_STAFF_ATTENDANCE_USERGROUP AS a
			      inner join
			      INTRANET_USER AS b 
			      on a.GroupID in ( $TargetGroup ) AND a.UserID=b.UserID";
	$StaffList = $this->returnArray($sql);
	$attend_array = array();
	
	for($i=0;$i<sizeof($StaffList);$i++)
	{
		$StaffID = $StaffList[$i]['UserID'];
		$GroupID = $StaffList[$i]['GroupID'];
		$StaffName = $StaffList[$i]['Name'];
		$roster_retrieved = false;
		$attend_array[$StaffID]['StaffID'] = $StaffID;
		$attend_array[$StaffID]['GroupID'] = $GroupID;
		$attend_array[$StaffID]['Name'] = $StaffList[$i]['Name'];
		# check duty		
		# check for holiday
		$sql = "SELECT RecordID, RecordType, OutgoingType, ReasonType
		                        FROM CARD_STAFF_ATTENDANCE2_LEAVE_RECORD
		                        WHERE StaffID = '$StaffID' AND RecordDate = '$TargetDate'";
		$temp = $this->returnArray($sql,4);
		list ($t_LeaveRecordID, $t_LeaveRecordType, $t_LeaveOutgoingType, $t_LeaveReasonType) = $temp[0];
		$attend_array[$StaffID]['Record'] = $t_LeaveRecordID;
		$attend_array[$StaffID]['RecordType'] = $t_LeaveRecordType;
		$attend_array[$StaffID]['OutgoingType'] = $t_LeaveOutgoingType;
		$attend_array[$StaffID]['ReasonType'] = $t_LeaveReasonType;
		if ($t_LeaveRecordID != "")
		{
			if ($t_LeaveRecordType == 1)
			{
				$attend_array[$StaffID]['InSchoolStatus']= CARD_STATUS_HOLIDAY;
				$roster_retrieved = true;
			}
			else if ($t_LeaveRecordType == 2)
			{
				$dutyInWaived = ($t_LeaveOutgoingType == 1 || $t_LeaveOutgoingType == 2);
				$dutyOutWaived = ($t_LeaveOutgoingType == 1 || $t_LeaveOutgoingType == 3);
				$attend_array[$StaffID]['InWaived']=$dutyInWaived?$i_StaffAttendance_Result_Waived:$i_StaffAttendance_Result_NotWaived;
				//$attend_array[$j]['OutWaived']=$dutyOutWaived?1:0;
				if($dutyInWaived)
				{
					$attend_array[$StaffID]['InSchoolStatus']=CARD_STATUS_OUTGOING;
					//$attend_array[$StaffID]['Duty']=1;
				  //$roster_retrieved = true;
				}
			}
		}
		# end check for holiday			        
		        
		
		if (!$roster_retrieved)
		{
		  // Check User Special Duty : CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY
		  $sql = "SELECT Duty, DutyStart, DutyEnd FROM CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY WHERE UserID = '".$StaffID."' AND DutyDate = '".$TargetDate."'";
		  $temp = $this->returnArray($sql, 3);
		  
		  if (sizeof($temp) != 0)
		  {
		    $attend_array[$StaffID]['Duty'] = $temp[0][0];
		    if ($temp[0][0] == 1)
		    {
		      $attend_array[$StaffID]['DutyStart'] = $temp[0][1];
		      $attend_array[$StaffID]['DutyEnd'] = $temp[0][2];
		      if(!$dutyInWaived)
		      	$attend_array[$StaffID]['InSchoolStatus']=CARD_STATUS_ABSENT;
		    }
		    $roster_retrieved = true;
		  }
		}
		
		if (!$roster_retrieved)
		{
		  // Check Group Special Duty : CARD_STAFF_ATTENDANCE2_GROUP_DATE_DUTY
		  $sql = "SELECT Duty, DutyStart, DutyEnd FROM CARD_STAFF_ATTENDANCE2_GROUP_DATE_DUTY WHERE GroupID = '".$GroupID."' AND DutyDate = '".$TargetDate."'";
		  $group_special = $this->returnArray($sql, 3);
		  if (sizeof($group_special) != 0)
		  {
		    $attend_array[$StaffID]['Duty'] = $group_special[0][0];
		    if ($group_special[0][0] == 1)
		    {
				$attend_array[$StaffID]['DutyStart'] = $group_special[0][1];
				$attend_array[$StaffID]['DutyEnd'] = $group_special[0][2];
				if(!$dutyInWaived)
					$attend_array[$StaffID]['InSchoolStatus']=CARD_STATUS_ABSENT;
		    }
		    $roster_retrieved = true;
		  }
		}
		
		if (!$roster_retrieved)
		{
			switch ($Day)
			{
		        case 0: $field_ext = "Sun"; break;
		        case 1: $field_ext = "Mon"; break;
		        case 2: $field_ext = "Tue"; break;
		        case 3: $field_ext = "Wed"; break;
		        case 4: $field_ext = "Thur"; break;
		        case 5: $field_ext = "Fri"; break;
		        case 6: $field_ext = "Sat"; break;
		        default: $field_ext = "Mon";
			}
			
			// Check Group Duty
			$sql = "SELECT Duty".$field_ext.", DutyStart".$field_ext.", DutyEnd".$field_ext." FROM CARD_STAFF_ATTENDANCE2_GROUP WHERE GroupID = '".$GroupID."'";
			$temp = $this->returnArray($sql, 3);
			
			if (sizeof($temp) != 0)
			{
			  $attend_array[$StaffID]['Duty'] = $temp[0][0];
			  if ($temp[0][0] == 1)
			  {
			    $attend_array[$StaffID]['DutyStart'] = $temp[0][1];
			    $attend_array[$StaffID]['DutyEnd'] = $temp[0][2];
			    if(!$dutyInWaived)
			        $attend_array[$StaffID]['InSchoolStatus']=CARD_STATUS_ABSENT;
			    $roster_retrieved = true;
			  }
			}
		}
	
	  	$off_duty = (empty($attend_array[$StaffID]['Duty'])) ? true:false;
	  
	  	if($attend_array[$StaffID]['InSchoolStatus']==CARD_STATUS_HOLIDAY)
	  	{
	  		
		}
		else if($off_duty)
		{
			// code added on 20080304, by kenneth chung, to add the OT information.
			$sql = "SELECT 
								StartTime,
								EndTime,
								Waived
							from 
								CARD_STAFF_ATTENDANCE2_OT_RECORD 
							WHERE 
								StaffID='".$StaffID."'
								AND 
								RecordDate='".$year."-".$month."-".$day."' 
							";
			
			$OTDuty = $this->returnArray($sql, 3);
			if (sizeof($OTDuty)>0)
			{
				$attend_array[$StaffID]['StartTime'] = $OTDuty[0]['StartTime'];
				$attend_array[$StaffID]['EndTime'] = $OTDuty[0]['EndTime'];
				$attend_array[$StaffID]['Waived'] = $OTDuty[0]['Waived'];
			}
			//code  end
		}else
		{
			$default_in_time = isset($attend_array[$StaffID]['InTime']) && $attend_array[$StaffID]['InTime'] != "" ? $attend_array[$StaffID]['InTime'] : "";
			if($default_in_time=="--") $default_in_time="";
			$attend_array[$StaffID]['InTime'] = $default_in_time;
			$default_in_station =isset($attend_array[$StaffID]['InSchoolStation']) && $attend_array[$StaffID]['InSchoolStation'] != "" ? $attend_array[$StaffID]['InSchoolStation'] : "";
			$attend_array[$StaffID]['InSchoolStation'] = $default_in_station;
		}
		
		if($attend_array[$StaffID]['DutyStart']=="") $attend_array[$StaffID]['DutyStart'] = "00:00:00";
		if($attend_array[$StaffID]['DutyEnd']=="") $attend_array[$StaffID]['DutyEnd'] = "00:00:00";
	}# End for loop
	
	return $attend_array;
}
        }




} // End of directives
?>
