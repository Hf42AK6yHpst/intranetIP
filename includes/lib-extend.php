<?php
# modify : 
/*
 * 	2017-04-12 [Cameron]
 * 		- fix bug to support php5.4, should use $_COOKIE instead of $variable [DM#3178]
 */
if (!$BLOCK_LIB_LOADING)
{
	function updateGetCookies($arrCookies)
	{
		if(!empty($arrCookies) && count($arrCookies) > 0)
		{
			for($i=0 ; $i<count($arrCookies) ; $i++)
			{
				global ${$arrCookies[$i][0]}, ${$arrCookies[$i][1]};
				
				//if(isset(${$arrCookies[$i][1]}) && ${$arrCookies[$i][1]} != '')
				if(isset(${$arrCookies[$i][1]}))
				{
					# e.g. setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
					setcookie($arrCookies[$i][0], ${$arrCookies[$i][1]}, 0, "", "", 0);
					
					# e.g. $ck_page_size = $numPerPage;
					${$arrCookies[$i][0]} = ${$arrCookies[$i][1]};
				}
				else 
				{		
					# e.g. $numPerPage = $ck_page_size; // added stripslashes as setcookie automatically added slashes to quotes or slashes
					${$arrCookies[$i][1]} = stripslashes($_COOKIE[$arrCookies[$i][0]]);		// use $_COOKIE to support php5.4
				}
			}
		}
		
		return;
	}
		
	function clearCookies($arrCookies)
	{
		# clear cookies process...
		if(!empty($arrCookies) && count($arrCookies) > 0)
		{
			for($i=0 ; $i<count($arrCookies) ; $i++)
			{
				global ${$arrCookies[$i][0]};
				
				if(isset(${$arrCookies[$i][0]}))
				{
					# e.g. setcookie("ck_page_size", null, time()-999999, "", "", 0);
					setcookie($arrCookies[$i][0], null, time()-999999, "", "", 0);
					
					# e.g. $ck_page_size = $numPerPage;
					${$arrCookies[$i][0]} = null;
				}
			}
		}
		return;
	}
}	
?>