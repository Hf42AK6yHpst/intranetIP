<?php
// Using : 
/************************************ Change Log ***************************************************
 * 
 * 2019-05-14 (Cameron) : fix potential sql injection problem by enclosing var with apostrophe
 * 2017-03-23 (Villa)#W114642  : add allowbookingindependence, modified Get_All_Item() add AllowBookingIndependence
 * 2011-02-16 (Carlos) : added Attachment field, Upload_Item_Attachment(), Delete_Item_Attachment
 ***************************************************************************************************/

class eBooking_Item extends libebooking {
	var $TableName;
	var $ID_FieldName;
	var $ObjectID;
	
	var $ItemType;
	var $CategoryID;
	var $Category2ID;
	var $NameChi;
	var $NameEng;
	var $DescriptionChi;
	var $DescriptionEng;
	var $ItemCode;
	var $LocationID;
	var $BookingDayBeforehand;
	var $AllowBooking;
	var $eInventoryItemID;
	var $Attachment;
	var $RecordType;
	var $RecordStatus;
	var $DateInput;
	var $InputBy;
	var $DateModified;
	var $ModifiedBy;
	var $AllowBookingIndependence; #W114642 
	
	function eBooking_Item($ObjectID='', $GetInfo=true){
		parent::libebooking();
		
		$this->TableName = 'INTRANET_EBOOKING_ITEM';
		$this->ID_FieldName = 'ItemID';
		
		$this->MaxLength['ItemCode'] = 100;
	    $this->MaxLength['ItemName'] = 255;
	    $this->MaxLength['ItemBookingDayBeforehand'] = 3;
		
		if ($ObjectID != '')
		{
			$this->ObjectID = $ObjectID;
			
			if ($GetInfo == true)
				$this->Get_Self_Info();
		}
	}
	
	function Get_Self_Info()
	{
		$sql = "";
		$sql .= " SELECT * FROM ".$this->TableName;
		$sql .= " WHERE ".$this->ID_FieldName." = '".$this->ObjectID."'";
		
		$resultSet = $this->returnArray($sql);
		$infoArr = $resultSet[0];
		
		foreach ($infoArr as $key => $value)
			$this->{$key} = $value;
	}
	
	function Insert_Object($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
		
		# set field and value string
		$fieldArr = array();
		$valueArr = array();
		foreach ($DataArr as $field => $value)
		{
			$fieldArr[] = $field;
			$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
		}
		
		## set others fields
		$fieldArr[] = 'DateInput';
		$valueArr[] = 'now()';
		# InputBy
		$fieldArr[] = 'InputBy';
		$valueArr[] = $_SESSION['UserID'];
		# DateModify
		$fieldArr[] = 'DateModified';
		$valueArr[] = 'now()';
		# ModifyBy
		$fieldArr[] = 'ModifiedBy';
		$valueArr[] = $_SESSION['UserID'];
		
		$fieldText = implode(", ", $fieldArr);
		$valueText = implode(", ", $valueArr);
		
		# Insert Record
		$this->Start_Trans();
		
		$sql = '';
		$sql .= ' INSERT INTO '.$this->TableName;
		$sql .= ' ( '.$fieldText.' ) ';
		$sql .= ' VALUES ';
		$sql .= ' ( '.$valueText.' ) ';
		$success = $this->db_db_query($sql);
		
		
		if ($success == false)
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$insertedID = $this->db_insert_id();
			$this->Commit_Trans();
			
			return $insertedID;
		}
	}
	
	function Update_Object($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
		
		# Build field update values string
		$valueFieldText = '';
		foreach ($DataArr as $field => $value)
		{
			$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
		}
		$valueFieldText .= ' DateModified = now(), ModifiedBy = \''.$_SESSION['UserID'].'\' ';
		
		$sql = '';
		$sql .= ' UPDATE '.$this->TableName;
		$sql .= ' SET '.$valueFieldText;
		$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$this->ObjectID.'\' ';
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Delete_Object($ObjectIDArr='')
	{
		$SuccessArr = array();
		
		if ($ObjectIDArr=='')
			$ObjectIDArr = array($this->ObjectID);
		$ObjectIDList = implode(',', $ObjectIDArr);
		
		$this->Start_Trans();
		
		/* Changed to soft delete
		# Delete Item
		$sql = "Delete From ".$this->TableName." Where ".$this->ID_FieldName." In ($ObjectIDList) ";
		$SuccessArr['DeleteItem'] = $this->db_db_query($sql);
		
		# Delete Item Management Group
		$sql = "Delete From INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP Where ".$this->ID_FieldName." In ($ObjectIDList) ";
		$SuccessArr['DeleteItemManagementGroup'] = $this->db_db_query($sql);
		
		# Delete Item Follow-up Group
		$sql = "Delete From INTRANET_EBOOKING_ITEM_FOLLOWUP_GROUP Where ".$this->ID_FieldName." In ($ObjectIDList) ";
		$SuccessArr['DeleteItemFollowUpGroup'] = $this->db_db_query($sql);
		*/
		
		$sql = "Update ".$this->TableName." Set RecordStatus = '0' Where ".$this->ID_FieldName." In ($ObjectIDList) ";
		$SuccessArr['UpdateItemStatus'] = $this->db_db_query($sql);
		
		if($SuccessArr['UpdateItemStatus']){
			for($i=0;$i<sizeof($ObjectIDArr);$i++){
				$SuccessArr['RemoveItemAttachment'.$ObjectIDArr[$i]] = $this->Delete_Item_Attachment($ObjectIDArr[$i]);
			}
		}
		
		if (in_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$this->Commit_Trans();
			return 1;
		}
	}
	
	function Update_Item_Management_Group($GroupIDArr=array())
	{
		### Delete old records
		$sql = "Delete From INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP Where ".$this->ID_FieldName." = '".$this->ObjectID."'";
		$SuccessArr['DeleteItemManagementGroup'] = $this->db_db_query($sql);
		
		
		### Add new linkage
		$numOfMgmtGroup = count($GroupIDArr);
		if ($SuccessArr['DeleteItemManagementGroup'] && $numOfMgmtGroup > 0)
		{
			$InsertArr = array();
			for ($i=0; $i<$numOfMgmtGroup; $i++)
			{
				$thisGroupID = $GroupIDArr[$i];
				$InsertArr[] = " ('".$this->ObjectID."', $thisGroupID, now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
			}
			
			$InsertList = implode(',', $InsertArr);
			
			$sql = "Insert Into INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP
						(ItemID, GroupID, DateInput, InputBy, DateModified, ModifiedBy)
					Values
						$InsertList
					";
			$SuccessArr['AddItemManagementGroupMapping'] = $this->db_db_query($sql);
		}
		
		if (in_array(false, $SuccessArr) == false)
			return true;
		else
			return false;
	}
	
	function Update_Item_FollowUp_Group($GroupIDArr=array())
	{
		### Delete old records
		$sql = "Delete From INTRANET_EBOOKING_ITEM_FOLLOWUP_GROUP Where ".$this->ID_FieldName." = '".$this->ObjectID."'";
		$SuccessArr['DeleteItemFollowUpGroup'] = $this->db_db_query($sql);
		
		### Add new linkage
		$numOfFollowUpGroup = count($GroupIDArr);
				
		if ($SuccessArr['DeleteItemFollowUpGroup'] && $numOfFollowUpGroup > 0)
		{
			$InsertArr = array();
			for ($i=0; $i<$numOfFollowUpGroup; $i++)
			{
				$thisGroupID = $GroupIDArr[$i];
				$InsertArr[] = " ('".$this->ObjectID."', '$thisGroupID', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
			}
			
			$InsertList = implode(',', $InsertArr);
			
			$sql = "Insert Into INTRANET_EBOOKING_ITEM_FOLLOWUP_GROUP
						(ItemID, GroupID, DateInput, InputBy, DateModified, ModifiedBy)
					Values
						$InsertList
					";
			$SuccessArr['AddItemFollowUpGroupMapping'] = $this->db_db_query($sql);
		}
		
		if (in_array(false, $SuccessArr) == false)
			return true;
		else
			return false;
	}
	
	function Update_Item_User_Booking_Rule($RuleIDArr=array())
	{
		### Delete old records
		$sql = "Delete From INTRANET_EBOOKING_ITEM_USER_BOOKING_RULE Where ".$this->ID_FieldName." = '".$this->ObjectID."'";
		$SuccessArr['DeleteItemUserBookingRule'] = $this->db_db_query($sql);
		
		
		### Add new linkage
		$numOfRule = count($RuleIDArr);
		if ($SuccessArr['DeleteItemUserBookingRule'] && $numOfRule > 0)
		{
			$InsertArr = array();
			for ($i=0; $i<$numOfRule; $i++)
			{
				$thisRuleID = $RuleIDArr[$i];
				$InsertArr[] = " ('".$this->ObjectID."', '$thisRuleID', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
			}
			
			$InsertList = implode(',', $InsertArr);
			
			$sql = "Insert Into INTRANET_EBOOKING_ITEM_USER_BOOKING_RULE
						(ItemID, RuleID, DateInput, InputBy, DateModified, ModifiedBy)
					Values
						$InsertList
					";
			$SuccessArr['AddItemUserBookingRuleMapping'] = $this->db_db_query($sql);
		}
		
		if (in_array(false, $SuccessArr) == false)
			return true;
		else
			return false;
	}
	
	function Get_All_Item($CategoryID='', $Category2ID='', $itemIdAry='')
	{
		$conds_categoryID = '';
    	if ($CategoryID != '')
    		$conds_categoryID = " And item.CategoryID = '$CategoryID' ";
    		
    	$conds_category2ID = '';
    	if ($Category2ID != '')
    		$conds_category2ID = " And item.Category2ID = '$Category2ID' ";
    		
    	if ($itemIdAry !== '') {
    		$conds_itemId = " And item.ItemID IN ('".implode("','", (array)$itemIdAry)."') ";
    	}
		
		$nameField = Get_Lang_Selection('item.NameChi', 'item.NameEng');
		$descriptionField = Get_Lang_Selection('item.DescriptionChi', 'item.DescriptionEng');
		$locationField = $this->Get_Room_Full_Location_NameField('building', 'floor', 'room');
		
		$sql = "Select
						item.ItemID,
						$nameField as ItemName,
						$descriptionField as ItemDescription,
						$locationField as LocationName,
						item.AllowBooking,
						item.BookingDayBeforehand,
						item.Attachment,
						item.eInventoryItemID,
						item.AllowBookingIndependence
				From
						INTRANET_EBOOKING_ITEM as item
						LEFT Join
						INVENTORY_LOCATION as room
						On (item.LocationID = room.LocationID)
						LEFT Join
						INVENTORY_LOCATION_LEVEL as floor
						On (room.LocationLevelID = floor.LocationLevelID)
						LEFT Join
						INVENTORY_LOCATION_BUILDING as building
						On (floor.BuildingID = building.BuildingID)
				Where
						item.RecordStatus = 1
						$conds_categoryID
						$conds_category2ID
						$conds_itemId
				Order By
						item.NameEng
				";
		$returnArr = $this->returnArray($sql);
		return $returnArr;
	}
	
	function Get_Item_Management_Group()
	{
		$sql = "Select
						MgmtGroup.GroupID,
						MgmtGroup.GroupName,
						MgmtGroup.Description
				From
						INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP as mapping
						Inner Join
						INTRANET_EBOOKING_MANAGEMENT_GROUP as MgmtGroup
						On (mapping.GroupID = MgmtGroup.GroupID)
				Where
						mapping.ItemID = '".$this->ObjectID."'
				Order By
						MgmtGroup.GroupName
				";
		$returnArr = $this->returnArray($sql);
		
		return $returnArr;
	}
	
	function Get_Item_FollowUp_Group()
	{
		$sql = "SELECT 
					* 
				FROM 
					INTRANET_EBOOKING_ITEM_FOLLOWUP_GROUP as mapping 
					INNER JOIN INTRANET_EBOOKING_FOLLOWUP_GROUP as follow_up ON (mapping.GroupID = follow_up.GroupID) 
				WHERE 
					mapping.ItemID = '".$this->ObjectID."' ORDER BY follow_up.GroupName";
		$returnArr = $this->returnArray($sql);
		
		return $returnArr;
	}
	
	function Get_Item_User_Booking_Rule()
	{
		$sql = "Select
						rule.RuleID,
						rule.RuleName
				From
						INTRANET_EBOOKING_ITEM_USER_BOOKING_RULE as mapping
						Inner Join
						INTRANET_EBOOKING_USER_BOOKING_RULE as rule
						On (mapping.RuleID = rule.RuleID)
				Where
						mapping.ItemID = '".$this->ObjectID."'
				Order By
						rule.RuleName
				";
		$returnArr = $this->returnArray($sql);
		
		return $returnArr;
	}
	
	function Get_Item_Category_Info()
	{
		$sql = "Select
						cat.NameChi as CategoryNameChi,
						cat.NameEng as CategoryNameEng,
						subcat.NameChi as SubCategoryNameChi,
						subcat.NameEng as SubCategoryNameEng
				From
						INTRANET_EBOOKING_ITEM as item
						Inner Join
						INVENTORY_CATEGORY as cat
						On (item.CategoryID = cat.CategoryID)
						Inner Join
						INVENTORY_CATEGORY_LEVEL2 as subcat
						On (item.Category2ID = subcat.Category2ID)
				Where
						item.ItemID = '".$this->ObjectID."'
				";
		$returnArr = $this->returnArray($sql);
		
		return $returnArr;
	}
	
	function Get_Item_Location_Info()
	{
		$locationField = $this->Get_Room_Full_Location_NameField('building', 'floor', 'room');
		$sql = "Select
						room.NameChi as RoomNameChi,
						room.NameEng as RoomNameEng,
						$locationField as RoomLocationName
				From
						INTRANET_EBOOKING_ITEM as item
						Inner Join
						INVENTORY_LOCATION as room
						On (item.LocationID = room.LocationID)
						Inner Join
						INVENTORY_LOCATION_LEVEL as floor
						On (room.LocationLevelID = floor.LocationLevelID)
						Inner Join
						INVENTORY_LOCATION_BUILDING as building
						On (floor.BuildingID = building.BuildingID)
				Where
						item.ItemID = '".$this->ObjectID."'
				";
		$returnArr = $this->returnArray($sql);
		
		return $returnArr;
	}
	
	function Is_Code_Valid($Code, $ExcludeObjectID='')
	{
		$cond_excludeObjectID = '';
		if ($ExcludeObjectID != '')
			$cond_excludeObjectID = " And ".$this->ID_FieldName." != '".$ExcludeObjectID."' ";
			
		$sql = "Select
						".$this->ID_FieldName."
				From
						".$this->TableName."
				Where
						ItemCode = '".$this->Get_Safe_Sql_Query($Code)."'
						And
						RecordStatus = 1
						$cond_excludeObjectID
				";
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) == 0)
			return 1;
		else
			return 0;
	}
	
	function Get_Room_Full_Location_NameField($BuildingPrefix='', $FloorPrefix='', $RoomPrefix='')
	{
		$BuildingFieldEn = ($BuildingPrefix=='')? 'NameEng' : $BuildingPrefix.'.NameEng';
		$BuildingFieldCh = ($BuildingPrefix=='')? 'NameChi' : $BuildingPrefix.'.NameChi';
		$BuildingField = Get_Lang_Selection($BuildingFieldCh, $BuildingFieldEn);
		
		$FloorFieldEn = ($FloorPrefix=='')? 'NameEng' : $FloorPrefix.'.NameEng';
		$FloorFieldCh = ($FloorPrefix=='')? 'NameChi' : $FloorPrefix.'.NameChi';
		$FloorField = Get_Lang_Selection($FloorFieldCh, $FloorFieldEn);
		
		$RoomFieldEn = ($RoomPrefix=='')? 'NameEng' : $RoomPrefix.'.NameEng';
		$RoomFieldCh = ($RoomPrefix=='')? 'NameChi' : $RoomPrefix.'.NameChi';
		$RoomField = Get_Lang_Selection($RoomFieldCh, $RoomFieldEn);
		
		$LocationField = " Concat($BuildingField, ' > ', $FloorField, ' > ', $RoomField) ";
		
		return $LocationField;
	}
	
	function Is_Item_Exist_In_Panding_BookingRequest($ItemID)
	{
		$sql = "
			SELECT 
				COUNT(*)
			FROM 
				INTRANET_EBOOKING_RECORD r 
				LEFT JOIN INTRANET_EBOOKING_BOOKING_DETAILS fbd ON r.BookingID = fbd.BookingID AND r.RecordStatus=1 AND fbd.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." 
			WHERE 
				fbd.RecordStatus = 1 
				AND fbd.BookingStatus = 0 
				AND fbd.ItemID = '$ItemID'
		";
//		$sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingStatus = 0 AND ItemID = $ItemID";
		$arrResult = $this->returnVector($sql);
		if($arrResult[0] > 0)
		{
			// Some panding request contain the target item
			// return false
			return 0;
		}
		else
		{
			// return true
			return 1;
		}
	}
	
	function Upload_Item_Attachment($ItemID,$AttachmentLocation,$AttachmentName)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$fs = new libfilesystem();
		
		# Create folder if not exist
		$folder_prefix = $intranet_root."/file/ebooking";
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
		$folder_prefix .= "/item";
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
		$folder_prefix .= "/i".$ItemID;
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
			
		$Result['CopyFile'] = $fs->file_copy($AttachmentLocation, $folder_prefix."/".$AttachmentName);
		if($Result['CopyFile']){
			$sql = "UPDATE INTRANET_EBOOKING_ITEM 
					SET Attachment = '".$this->Get_Safe_Sql_Query($AttachmentName)."'  
					WHERE ItemID = '".$ItemID."' ";
			$Result['UpdateDB'] = $this->db_db_query($sql);
		}
		return !in_array(false,$Result);
	}
	
	function Delete_Item_Attachment($ItemID)
	{
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$fs = new libfilesystem();
		
		$sql = "SELECT Attachment FROM INTRANET_EBOOKING_ITEM WHERE ItemID = '".$ItemID."' ";
		$res_vec = $this->returnVector($sql);
		
		if(trim($res_vec[0])!=''){
			$Attachment = $res_vec[0];
			$location = $intranet_root."/file/ebooking/item/i".$ItemID."/".$Attachment;
			$Result['RemoveFile'] = $fs->file_remove($location);
			if($Result['RemoveFile']){
				$sql = "UPDATE INTRANET_EBOOKING_ITEM SET Attachment = NULL WHERE ItemID = '".$ItemID."' ";
				$Result['UpdateDB'] = $this->db_db_query($sql);
			}
			return !in_array(false,$Result);
		}
		return true;
	}
	
	function Import_Item_From_eInventory()
	{
		$sql = "
			INSERT INTO INTRANET_EBOOKING_ITEM
			(
				eInventoryItemID,
				ItemType,
				CategoryID,
				Category2ID,
				NameChi,
				NameEng,
				DescriptionChi,
				DescriptionEng,
				ItemCode,
				BarCode,
				LocationID,
				AllowBooking,
				RecordStatus,
				DateInput,
				InputBy,
				DateModified,
				ModifiedBy		
			)
			SELECT
				a.ItemID, 
				a.ItemType,
				a.CategoryID,
				a.Category2ID,
				a.NameChi,
				a.NameEng,
				a.DescriptionChi,
				a.DescriptionEng,
				a.ItemCode,
				b.TagCode,
				b.LocationID,
				0,
				1,
				NOW(),
				'".$_SESSION['UserID']."',
				NOW(),
				'".$_SESSION['UserID']."'			
			FROM
				INVENTORY_ITEM a
				INNER JOIN INVENTORY_ITEM_SINGLE_EXT b ON a.ItemID = b.ItemID
			WHERE
				a.ItemType = 1
				AND a.TransferredToEbooking = 0
		";
		$Success['CopyFromInventory'] = $this->db_db_query($sql);
		
		$sql = "
			UPDATE 
				INVENTORY_ITEM 
			SET 
				TransferredToEbooking = 1 
			WHERE 
				ItemType = 1 
				AND TransferredToEbooking = 0
		";
		$Success['UpdateTransferredToEbooking'] = $this->db_db_query($sql);
		
		return !in_array(false,$Success)?1:0;
		
	}
}

?>