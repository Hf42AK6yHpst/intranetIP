<?php
// using by 

/* Change log
 *  Date: 2020-08-03 (Ray)    - modified Get_Duty_By_Date
 *  Date: 2019-02-04 (Carlos) - modified Record_Tap_Card(), added optional parameter $UpdatedByUserID for explicitly asigning the update user. 
 *  Date: 2018-02-14 (Carlos) - modified Get_Duty_By_Date(), apply preset leave Remark to daily log records.
 *  Date: 2017-07-12 (Carlos) - Get_Duty_By_Date() check leave user list before generating time slot daily log records.
 *  Date: 2017-06-01 (Carlos) - modified Get_Daily_Log_Record() added optional parameter for returning staff id as key associative array.
 *  Date: 2017-04-21 (Carlos) - added cleanFullDayRecords($year, $month) for removing any duplicated/wrong full day records.
 *  Date: 2017-03-29 (Carlos) - modified Record_Tap_Card(), record raw log even within ignore tap card period.
 *  Date: 2016-12-13 (Carlos) - $sys_custom['StaffAttendance']['WongFutNamCollege'] - modified Get_Daily_Log_Record(), Get_Duty_By_Date(), Record_Tap_Card()
 * 																					- added clearOtherDutyRecordsForCustomization()
 *  Date: 2016-04-06 (Carlos) - modified Record_Tap_Card(), for door access integration $sys_custom['StaffAttendance']['RemoveEarlyLeaveBeforeLastTimeSlot'], apply absolute ignore tap card in for waive in time slots, avoid being late.
 *  Date: 2015-12-07 (Carlos) - modified Record_Tap_Card().
 * 								added processOnDutyTimeSlotAfterOutingOrHoliday() for handling on duty time slots after come back from outing/holiday.
 * 								added isLeftBeforeCurrentTimeSlot() for handling tap card in (present/late) after left in previous time slots.
 * 								added hasPreviousTimeSlotsRecordedTime() for handling today's first time card when in between no need to tap card in time slots. 
 *  Date: 2015-09-30 (Carlos) - modified Record_Tap_Card(), ignore today's first time slot that is in waived(no need to tap card in), should continue process in status. 
 *  Date: 2015-08-27 (Carlos) - $sys_custom['StaffAttendance']['RemoveEarlyLeaveBeforeLastTimeSlot'] added Remove_EarlyLeave_Records_Before_LastTimeSlot(), modified Record_Tap_Card() t ocater door access integration early leave issue.
 *  Date: 2015-07-20 (Carlos) - added Get_Libcycleperiods(), Get_Cycle_Periods_Within_Date_Range($start_date, $end_date), Get_Cycle_Day_By_Date($target_date) for getting cycle day data.
 * 							  - modified Get_Duty_By_Date() to handle cycle day.
 *  Date: 2015-04-02 (Carlos) - modified Record_Tap_Card(), fix tap card leave update in status as present forgot to clear InAttendanceRecordID issue.
 *  Date: 2015-01-19 (Carlos) - modified Record_Tap_Card(), added parameter $IsEditPastRecord=false for editing past overnight duty leave time that can skip next day's tap card times after the first tap card time.
 *  Date: 2014-12-04 (Carlos) - fix Record_Tap_Card(), over night duty tap card leave, if today no duty then should process yesterday duty out
 *  Date: 2014-10-30 (Carlos) - added class member var $DataCache for preventing repeatly call createTable_Card_Staff_Attendance2_Daily_Log() to show tables/create table
 *  Date: 2013-03-21 (Carlos) - modified Get_Duty_By_Date(), add cust $sys_custom['StaffAttendance']['NoSpecialDutyUseRegularDuty'] to force get regular duty for no special duty group member
 *  Date: 2013-01-25 (Carlos) - fix Get_Duty_By_Date(), 1)init variable $CardLogDetail for past dates and today; 
 * 								2) for individual user, if it is a group member and other group members have special duty, it should not get regular duty.
 *  Date: 2012-11-06 (Carlos) - fix Record_Tap_Card(), do not update in status for InWavie=1 time slot, update out status or traverse to next time slot
 *  Date: 2012-04-05 (Carlos) - modified Get_Duty_By_Date(), set Reason DefaultWavie to PROFILE Waived and DAILYLOG InWaived/OutWaived when generating new records
 *  Date: 2011-09-07 (Carlos) - modified Get_Duty_By_Date(), add checking on existing full day holiday/outgoing records when inserting new full day leave daily log records 
 * 	Date: 2011-08-15 (Carlos) - fix fail to record first time slot in time problem
 *  Date: 2011-08-04 (Carlos) - fix tap card after overnight time slot, added checking of absolute tap card time region for today's first time slot
 *  Date: 2011-07-13 (Carlos)
 *  Detail: Modified Record_Tap_Card() - fix overnight time slot record out time failure problem when today do not have any duty slot 
	Date: 2010-11-10 (Kenneth Chung)
	detail: 
		Modified function Get_Duty_By_Date, add two parameter:
		1. $FreezeDuty - system will not retrieve the final duty from cardlog for return if set to true
		2. $CacheResult - system will cache the duty result to global array to reduce reducdant query

	Date: 2010-10-25 (Kenneth Chung)
	detail: changed Record_Tap_Card, Apply check of Absolute timeslot start time setting

	Date: 2010-10-18 (Kenneth Chung)
	detail: changed Get_Duty_By_Date, to fix the problem on case "2010-1015-1158-067"

*/

// class for staff attendance API calling only, to reduce the library size for faster performance
include_once("libdb.php");
include_once("libstaffattend2.php");
include_once("libstaffattend3_staff_attend_group.php");

define("SCHOOLEVENT","0");
define("ACADEMICEVENT","1");
define("PUBLICHOLIDAY","3");
define("SCHOOLHOLIDAY","4");

class libstaffattend3_api extends libstaffattend2 {
	var $ModuleName;
	var $MaxSlotPerSetting;
	
	var $DataCache = array();
	
	var $libcycleperiods = null;
	
	function libstaffattend3_api() {
		parent::libstaffattend2();
		
		$this->ModuleName = 'StaffAttendance3';
		$this->MaxSlotPerSetting = 5;
	}
	
	function log($log_row)
	{
		global $intranet_root, $file_path, $sys_custom;
		
		$num_line_to_keep = isset($sys_custom['StaffAttendanceLogMaxLine'])? $sys_custom['StaffAttendanceLogMaxLine'] : 200000;
		$log_file_path = $file_path."/file/staff_attendance_log";
		$log_file_path_tmp = $file_path."/file/staff_attendance_log_tmp";
		// assume $log_row does not contains single quote that would break the shell command statement
		shell_exec("echo '$log_row' >> $log_file_path"); // append to log file
		$line_count = intval(shell_exec("cat '$log_file_path' | wc -l"));
		if($line_count > $num_line_to_keep){
			shell_exec("tail -n ".($num_line_to_keep/2)." '$log_file_path' > '$log_file_path_tmp'"); // keep the last $num_line_to_keep/2 lines
			shell_exec("rm '$log_file_path'");
			shell_exec("mv '$log_file_path_tmp' '$log_file_path'");
		}
	}
	
	function Record_Tap_Card($StaffID,$current_time,$SiteName,$CreateRawLog=true, $IsEditPastRecord=false, $UpdatedByUserID='') {
		global $sys_custom;
		
		include_once("libgeneralsettings.php");
		
		$GeneralSetting = new libgeneralsettings();
		$SettingList[] = "'IgnorePeriod'"; // Ignore Period for card tapping
		$SettingList[] = "'AbsMinsForTimeSlot'"; // Ignore Period for card tapping
		$Settings = $GeneralSetting->Get_General_Setting('StaffAttendance',$SettingList);
		//debug_pr($Settings);
		// get absolute seconds that need to count for card tapping
		$AbsSecForTimeSlot = $Settings["AbsMinsForTimeSlot"]*60;
		
		$Month = date('m',$current_time);
    $Year = date('Y',$current_time);
    $Day = date('j',$current_time);
    $Today = date('Y-m-d',$current_time);
    $Yesterday = date('Y-m-d',mktime(1,1,1,($Month+0),($Day-1),$Year));
    $YesterdayYear = date('Y',mktime(1,1,1,($Month+0),($Day-1),$Year));
    $YesterdayMonth = date('m',mktime(1,1,1,($Month+0),($Day-1),$Year));
    $YesterdayDay = date('j',mktime(1,1,1,($Month+0),($Day-1),$Year));
    $Tmr = date('Y-m-d',mktime(1,1,1,($Month+0),($Day+1),$Year));
    $time_string = date('H:i:s',$current_time);
    
    $ts_now = $current_time - strtotime($Today);
  	
  	if($UpdatedByUserID == ''){
  		$UpdatedByUserID = $StaffID;
  	}
  	
  	/* Add Raw Log to Staff Attendance */
    $RawLogTable = $this->buildStaffRawLogMonthTable($Year, $Month);
    $TodayDailyLogTable = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
    $YesterdayDailyLogTable = $this->createTable_Card_Staff_Attendance2_Daily_Log($YesterdayYear, $YesterdayMonth);
    
    if(!$IsEditPastRecord)
    {
	    // check for Ignore Period for card tapping
	    $sql = 'select 
	    						MAX(TIME_TO_SEC(RecordTime))
	    				From 
	    					'.$RawLogTable.' 
	    				Where 
	    					DayNumber = \''.$Day.'\' 
	    					and 
	    					UserID = \''.$StaffID.'\'';
	    $LastTapCardTime = $this->returnVector($sql);
	    // is within ignore period
	    if ((($ts_now - $LastTapCardTime[0])/60) < $Settings['IgnorePeriod'] && $LastTapCardTime[0] != "") {
	    	if ($CreateRawLog) {
			    $sql = "INSERT IGNORE INTO $RawLogTable
			                   (UserID, DayNumber, RecordTime, RecordStation, DateInput, DateModified)
			                   VALUES
			                   ('$StaffID','$Day','$time_string','".$this->Get_Safe_Sql_Query($SiteName)."', NOW(),NOW())";
			    $Result['CreateRawLog'] = $this->db_db_query($sql);
			}
	    	
	    	return array('',CARD_RESP_IGNORE_WITHIN_PERIOD);
	    }
    }
  	
  	if ($CreateRawLog) {
	    $sql = "INSERT IGNORE INTO $RawLogTable
	                   (UserID, DayNumber, RecordTime, RecordStation, DateInput, DateModified)
	                   VALUES
	                   ('$StaffID','$Day','$time_string','".$this->Get_Safe_Sql_Query($SiteName)."', NOW(),NOW())";
	    $Result['CreateRawLog'] = $this->db_db_query($sql);
	}
  	
  	// get Today Duty
  	$Duty = $this->Get_Duty_By_Date($StaffID,$Today,true); 
  	//debug_r($Duty);
  	$i = 0;
  	$FirstTimeSlotName = '';
  	foreach ($Duty['CardSlotDetail'] as $SlotName => $SlotDetail) {
  		$TodaySlot[] = $SlotDetail;
  		if ($i == 0) {
  			$FirstStartTime = $SlotDetail['SlotStart'];
  			$FirstTimeSlotName = $SlotName;
  		}
  		$SlotTimeArray[] = array('SlotName'=> $SlotName,'Timestamp'=> strtotime($Today." ".$SlotDetail['SlotStart']));
  		
  		if ($SlotDetail['SlotEnd'] < $FirstStartTime) // have overnight shift
  			$SlotTimeArray[] = array('SlotName'=> $SlotName,'Timestamp'=> strtotime($Tmr." ".$SlotDetail['SlotEnd']));
  		else 
  			$SlotTimeArray[] = array('SlotName'=> $SlotName,'Timestamp'=> strtotime($Today." ".$SlotDetail['SlotEnd']));
  		$i++;
  	}
	//debug_pr($SlotTimeArray);
  	// may be yesterday out record
  	if (sizeof($Duty['CardFullHoliday']) > 0 || 
  			sizeof($Duty['CardFullOutgoing']) > 0 || 
  			!in_array($StaffID,$Duty['CardStaffWithSetting']) || 
  			strtotime($Today." ".$TodaySlot[0]['SlotStart']) >= $current_time) {
  		$NotWithinTodaysFirstSlotTapCardRegion = ((strtotime($Today." ".$TodaySlot[0]['SlotStart']) - $AbsSecForTimeSlot) >= $current_time);
  		if ($NotWithinTodaysFirstSlotTapCardRegion || !in_array($StaffID,$Duty['CardStaffWithSetting'])) { // tap card not falls to today first time slot's absolute counting mins  or  no duty time slot today 
	  		// get yesterday Duty
	  		$YesterdayDuty = $this->Get_Duty_By_Date($StaffID,$Yesterday,true); 
	  		
	  		// check if there is overnight duty and need to check last out tap card record
		  	$NeedRecordYesterdayOut = false;
		  	foreach ($YesterdayDuty['CardSlotDetail'] as $SlotName => $SlotDetail) {
		  		if ($SlotDetail['SlotStart'] > $SlotDetail['SlotEnd'] && 
		  			($NotWithinTodaysFirstSlotTapCardRegion || !in_array($StaffID,$Duty['CardStaffWithSetting'])) && 
		  				($YesterdayDuty['CardSlotToDuty'][$SlotName][0]['OutSchoolStatus'] == "" 
		  				|| $YesterdayDuty['CardSlotToDuty'][$SlotName][0]['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE
		  				|| (!in_array($StaffID,$Duty['CardStaffWithSetting']) && $YesterdayDuty['CardSlotToDuty'][$SlotName][0]['OutSchoolStatus'] == CARD_STATUS_NORMAL))) {
		  			$NeedRecordYesterdayOut = true;
		  			
		  			// update yesterday duty record
	  				$SlotEndTime = strtotime($Today." ".$SlotDetail['SlotEnd']);
			  		if ($SlotEndTime > $current_time) { // early leave
			  			$MinEarlyLeave = floor(($YesterdayDuty['CardSlotToDuty'][$SlotName][0]['DutyEndSec'] - $ts_now)/60);
			  			
			  			$Result['ClearYesterDayLastProfile'] = $this->Remove_Profile($YesterdayDuty['CardSlotToDuty'][$SlotName][0]['OutAttendanceRecordID']);
			  			$ProfileID = $this->Create_Profile($Yesterday,$StaffID,"",$SlotDetail['DutyCount'],CARD_STATUS_EARLYLEAVE);
			  			
			  			$sql = 'update '.$YesterdayDailyLogTable.' set 
			  								OutSchoolStatus = \''.CARD_STATUS_EARLYLEAVE.'\', 
			  								OutSchoolStation = \''.$this->Get_Safe_Sql_Query($SiteName).'\', 
			  								OutTime = \''.$time_string.'\', 
			  								MinEarlyLeave = \''.$MinEarlyLeave.'\', 
			  								OutAttendanceRecordID = \''.$ProfileID.'\', 
			  								DateModified = NOW(), 
			  								ModifyBy = \''.$UpdatedByUserID.'\' 
			  							where 
			  								RecordID = \''.$YesterdayDuty['CardSlotToDuty'][$SlotName][0]['RecordID'].'\' 
			  						 ';
							$Result['YesterdayEarlyleave'] = $this->db_db_query($sql);
							
							if($sys_custom['StaffAttendance']['WongFutNamCollege']){
								$this->clearOtherDutyRecordsForCustomization($StaffID, $Today);
							}
							
							// debug_r($Result);
							return array($SlotName,CARD_RESP_STAFF_LEAVE_SCHOOL_EARLY);
			  		}
			  		else { // Normal
			  			$Result['ClearYesterDayLastProfile'] = $this->Remove_Profile($YesterdayDuty['CardSlotToDuty'][$SlotName][0]['OutAttendanceRecordID']);
			  			
			  			$sql = 'update '.$YesterdayDailyLogTable.' set 
			  								OutSchoolStatus = \''.CARD_STATUS_NORMAL.'\', 
			  								OutSchoolStation = \''.$this->Get_Safe_Sql_Query($SiteName).'\', 
			  								OutTime = \''.$time_string.'\', 
			  								MinEarlyLeave = \'\', 
			  								OutAttendanceRecordID = \'\', 
			  								DateModified = NOW(), 
			  								ModifyBy = \''.$UpdatedByUserID.'\'  
			  							where 
			  								RecordID = \''.$YesterdayDuty['CardSlotToDuty'][$SlotName][0]['RecordID'].'\'
			  						 ';
							$Result['YesterdayNormalLeave'] = $this->db_db_query($sql);
							
							if($sys_custom['StaffAttendance']['WongFutNamCollege']){
								$this->clearOtherDutyRecordsForCustomization($StaffID, $Today);
							}
							
							// debug_r($Result);
							return array($SlotName,CARD_RESP_STAFF_LEAVE_SCHOOL_NORMAL);
			  		}
		  			break;
		  		}
		  	}
	  	}
	  	else {
	  		$NeedRecordYesterdayOut = false;
	  	}
	  	// today in
		  if (!$NeedRecordYesterdayOut) {
		  	$SlotDuty = $Duty['CardSlotToDuty'][$TodaySlot[0]['SlotName']][0];
		  	if (sizeof($Duty['CardFullHoliday']) > 0 || 
  			sizeof($Duty['CardFullOutgoing']) > 0 || 
  			!in_array($StaffID,$Duty['CardStaffWithSetting'])) { // full day off
  				$Result['ProcessOT'] = $this->Process_OT(false,$StaffID,$Today,$current_time);
  				return array('',CARD_RESP_NO_NEED_TO_TAKE);
  			}
  			else if ($SlotDuty['InSchoolStatus'] == CARD_STATUS_OUTGOING) { // no duty for that time slot
  					// find the first on duty time slot after this outing time slot 
  					$return_result = $this->processOnDutyTimeSlotAfterOutingOrHoliday($StaffID, $Duty, $TodaySlot, $SlotDuty['SlotName'], $current_time, $SiteName);
  					if($return_result !== false && is_array($return_result)){
  						return $return_result;
  					}
					return array($SlotDuty['SlotName'],STAFF_LEAVE_TYPE_OUTGOING);
				}
				else if ($SlotDuty['InSchoolStatus'] == CARD_STATUS_HOLIDAY) { // no duty for that time slot
					// find the first on duty time slot after this holiday time slot
					$return_result = $this->processOnDutyTimeSlotAfterOutingOrHoliday($StaffID, $Duty, $TodaySlot, $SlotDuty['SlotName'], $current_time, $SiteName);
  					if($return_result !== false && is_array($return_result)){
  						return $return_result;
  					}
					return array($SlotDuty['SlotName'],STAFF_LEAVE_TYPE_HOLIDAY);
				}
  			else {
  				// Check if yesterday duty has over night duty, if yes and not within today first tap card region, ignore tap card
  				$YesterdayDuty = $this->Get_Duty_By_Date($StaffID,$Yesterday,true);
  				$hasYesterdayOvernightDuty = false;
  				if(count($YesterdayDuty)>0){
  					foreach ($YesterdayDuty['CardSlotDetail'] as $SlotName => $SlotDetail) {
		  				if ($SlotDetail['SlotStart'] > $SlotDetail['SlotEnd']){
		  					$hasYesterdayOvernightDuty = true;
		  				}
  					}
  				}
  				
  				if($hasYesterdayOvernightDuty && $AbsSecForTimeSlot > 0 && $NotWithinTodaysFirstSlotTapCardRegion){
  					return array('',CARD_RESP_IGNORE_WITHIN_PERIOD);
  				}
  				if ($SlotDuty['InSchoolStatus'] != CARD_STATUS_NORMAL."") {
	  				// clear profile record for time slot
			  		$Result['ClearFirstProfile'] = $this->Remove_Profile($SlotDuty['InAttendanceRecordID']);
	  				
	  				// update daily log status
				  	$sql = 'update '.$TodayDailyLogTable.' set 
			  							InSchoolStatus = \''.CARD_STATUS_NORMAL.'\', 
			  							InSchoolStation = \''.$this->Get_Safe_Sql_Query($SiteName).'\', 
			  							InTime = \''.$time_string.'\', 
										MinLate = \'\',
			  							InAttendanceRecordID = \'\',';
			  		if ($TodaySlot[0]['OutWavie'] == "1") {
			  			$sql .= 'OutSchoolStatus = \''.CARD_STATUS_NORMAL.'\',
								 MinEarlyLeave = \'\',';
			  		}
			  		$sql .= '
			  							DateModified = NOW(), 
				  						ModifyBy = \''.$UpdatedByUserID.'\'  
			  						Where 
			  							RecordID = \''.$SlotDuty['RecordID'].'\'
			  					 ';
			  		//debug_pr($sql);
			  		$Result['TodayFirstNormal'] = $this->db_db_query($sql);
			  		
			  		// traverse and update status through today time slot
			  		if ($TodaySlot[0]['OutWavie'] == "1") {
			  			$Result['TraverseWaviedTimeSlot'] = $this->Traverse_Wavied_Time_Slot($StaffID,$TodaySlot,$Duty,$SlotDuty['SlotName'],CARD_STATUS_NORMAL,$Today);
			  		}
			  	}
			  	
  				if($sys_custom['StaffAttendance']['WongFutNamCollege']){
					$this->clearOtherDutyRecordsForCustomization($StaffID, $Today);
				}
			  	
		  		// debug_r($Result);
		  		return array($SlotDuty['SlotName'],CARD_RESP_STAFF_IN_SCHOOL_ONTIME);
		  	}
		  }
  	}
  	else { // today duty 
  		// loop for suitable time slot for tap card time
  		for ($i=0; $i< sizeof($SlotTimeArray); $i++) {
  			if ($SlotTimeArray[$i]['Timestamp'] < $current_time && $SlotTimeArray[($i+1)]['Timestamp'] >= $current_time) {
  				if ($SlotTimeArray[$i]['SlotName'] == $SlotTimeArray[($i+1)]['SlotName']) { // inner tap card record within one time slot
  					$SlotDuty = $Duty['CardSlotToDuty'][$SlotTimeArray[$i]['SlotName']][0];
  					if ($SlotDuty['InSchoolStatus'] ==  CARD_STATUS_OUTGOING) { // no duty for that time slot
  						// find the first on duty time slot after this outing time slot
  						$return_result = $this->processOnDutyTimeSlotAfterOutingOrHoliday($StaffID, $Duty, $TodaySlot, $SlotDuty['SlotName'], $current_time, $SiteName);
	  					if($return_result !== false && is_array($return_result)){
	  						return $return_result;
	  					}
  						return array($SlotDuty['SlotName'],STAFF_LEAVE_TYPE_OUTGOING);
  					}
  					else if ($SlotDuty['InSchoolStatus'] ==  CARD_STATUS_HOLIDAY) { // no duty for that time slot
  						// find the first on duty time slot after this holiday time slot
  						$return_result = $this->processOnDutyTimeSlotAfterOutingOrHoliday($StaffID, $Duty, $TodaySlot, $SlotDuty['SlotName'], $current_time, $SiteName);
	  					if($return_result !== false && is_array($return_result)){
	  						return $return_result;
	  					}
  						return array($SlotDuty['SlotName'],STAFF_LEAVE_TYPE_HOLIDAY);
  					}
  					else {
  						// find normal leave or early leave time tims lot just before this time slot
  						$is_left_before = $this->isLeftBeforeCurrentTimeSlot($StaffID, $Duty, $SlotDuty['SlotName']);
  						$has_recorded_time_before = $this->hasPreviousTimeSlotsRecordedTime($StaffID, $Duty, $SlotDuty['SlotName']); // if no recorded any time before, this is today's first tap card, record it
  						$absolute_ignore_tap_card_in = $Duty['CardSlotDetail'][$SlotDuty['SlotName']]['InWavie'] == "1" && $sys_custom['StaffAttendance']['RemoveEarlyLeaveBeforeLastTimeSlot']; // if time slot does not need tap card in and has door access integration, must ignore tap card in for this case
  						if (trim($SlotDuty['InTime']) == "" && ($SlotDuty['InSchoolStatus'] == "" || $SlotDuty['InSchoolStatus'] == CARD_STATUS_ABSENT) && ($SlotDuty['SlotName']==$FirstTimeSlotName || $Duty['CardSlotDetail'][$SlotDuty['SlotName']]['InWavie'] != "1" || ($is_left_before && !$absolute_ignore_tap_card_in) || (!$has_recorded_time_before && !$absolute_ignore_tap_card_in)) ) { // late
  							// clear profile record for time slot
					  		$Result['ClearProfile:'.$SlotDuty['SlotName']] = $this->Remove_Profile($SlotDuty['InAttendanceRecordID']);
					  		
					  		// create late profile
							  $ProfileID = $this->Create_Profile($Today,$StaffID,"",$SlotDuty['DutyCount'],CARD_STATUS_LATE);
							  
							  $MinLate = floor(($ts_now - $SlotDuty['DutyStartSec'])/60);
  							// daily log
  							$sql = 'update '.$TodayDailyLogTable.' set 
					  							InSchoolStatus = \''.CARD_STATUS_LATE.'\', 
					  							InSchoolStation = \''.$this->Get_Safe_Sql_Query($SiteName).'\', 
					  							InTime = \''.$time_string.'\', 
					  							InAttendanceRecordID = \''.$ProfileID.'\', 
					  							MinLate = \''.$MinLate.'\', ';
					  		if ($Duty['CardSlotDetail'][$SlotDuty['SlotName']]['OutWavie'] == "1") {
					  			$sql .= 'OutSchoolStatus = \''.CARD_STATUS_NORMAL.'\',
										 MinEarlyLeave = \'\',';
					  		}
					  		$sql .= '
					  							DateModified = NOW(), 
			  									ModifyBy = \''.$UpdatedByUserID.'\'  
					  						Where 
					  							RecordID = \''.$SlotDuty['RecordID'].'\'
					  					 ';
					  		$Result['TodayLate:'.$SlotDuty['SlotName']] = $this->db_db_query($sql);
					  		
					  		// traverse and update status through today time slot
					  		if ($Duty['CardSlotDetail'][$SlotDuty['SlotName']]['OutWavie'] == "1") {
					  			$Result['TraverseWaviedTimeSlot'] = $this->Traverse_Wavied_Time_Slot($StaffID,$TodaySlot,$Duty,$SlotDuty['SlotName'],CARD_STATUS_NORMAL,$Today);
					  		}
					  		
					  		// special handler for door access integration client - remove all early leave status before the last time slot
	  						if($sys_custom['StaffAttendance']['RemoveEarlyLeaveBeforeLastTimeSlot']){
	  							$tmp_start_time = date("H:i:s", $SlotTimeArray[$i]['Timestamp']);
	  							$this->Remove_EarlyLeave_Records_Before_LastTimeSlot($StaffID, $Today, $tmp_start_time);
	  						}
					  		
					  		if($sys_custom['StaffAttendance']['WongFutNamCollege']){
								$this->clearOtherDutyRecordsForCustomization($StaffID, $Today);
							}
					  							  		
					  		// debug_r($Result);
					  		return array($SlotDuty['SlotName'],CARD_RESP_STAFF_IN_SCHOOL_LATE);
  						}
  						else { 
  							if ($SlotTimeArray[($i+1)]['Timestamp'] == $current_time) { // special handling for tap card out time exactly the same as time slot end time, treat as normal out
  								// clear profile record for time slot
		  						$Result['ClearProfile:'.$SlotDuty['SlotName']] = $this->Remove_Profile($SlotDuty['OutAttendanceRecordID']);
							
		  						$sql = 'update '.$TodayDailyLogTable.' set 
		  											OutSchoolStatus = \''.CARD_STATUS_NORMAL.'\', 
		  											OutSchoolStation = \''.$this->Get_Safe_Sql_Query($SiteName).'\', 
						  							OutTime = \''.$time_string.'\', 
						  							OutAttendanceRecordID = \'\', 
						  							MinEarlyLeave = \'\', 
						  							DateModified = NOW(), 
			  										ModifyBy = \''.$UpdatedByUserID.'\'  
						  						where 
						  							RecordID = \''.$SlotDuty['RecordID'].'\'';
		  						$Result['TodayNormalOut:'.$SlotDuty['SlotName']] = $this->db_db_query($sql);
		  						
		  						// traverse and update status through today time slot
						  		$Result['TraverseWaviedTimeSlot'] = $this->Traverse_Wavied_Time_Slot($StaffID,$TodaySlot,$Duty,$SlotDuty['SlotName'],CARD_STATUS_ABSENT,$Today);
		  						
		  						// special handler for door access integration client - remove all early leave status before the last time slot
		  						if($sys_custom['StaffAttendance']['RemoveEarlyLeaveBeforeLastTimeSlot']){
		  							$tmp_start_time = date("H:i:s", $SlotTimeArray[$i]['Timestamp']);
		  							$this->Remove_EarlyLeave_Records_Before_LastTimeSlot($StaffID, $Today, $tmp_start_time);
		  						}
		  						
		  						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
									$this->clearOtherDutyRecordsForCustomization($StaffID, $Today);
								}
		  						
		  						// debug_r($Result);
		  						return array($SlotDuty['SlotName'],CARD_RESP_STAFF_LEAVE_SCHOOL_NORMAL);
  							}
  							else {// early leave
	  							// clear profile record for time slot
	  							$Result['ClearProfile:'.$SlotDuty['SlotName']] = $this->Remove_Profile($SlotDuty['OutAttendanceRecordID']);
	  							
	  							// create Early Leave profile
								  $ProfileID = $this->Create_Profile($Today,$StaffID,"",$SlotDuty['DutyCount'],CARD_STATUS_EARLYLEAVE);
	  							
	  							$MinEarlyLeave = floor(($SlotDuty['DutyEndSec'] - $ts_now)/60);
	  							$sql = 'update '.$TodayDailyLogTable.' set 
						  							OutSchoolStatus = \''.CARD_STATUS_EARLYLEAVE.'\', 
						  							OutSchoolStation = \''.$this->Get_Safe_Sql_Query($SiteName).'\', 
						  							OutTime = \''.$time_string.'\', 
						  							OutAttendanceRecordID = \''.$ProfileID.'\', 
						  							MinEarlyLeave = \''.$MinEarlyLeave.'\', 
						  							DateModified = NOW(), 
			  										ModifyBy = \''.$UpdatedByUserID.'\'  
						  						Where 
						  							RecordID = \''.$SlotDuty['RecordID'].'\'
						  					 ';
						  		$Result['TodayEarlyLeave:'.$SlotDuty['SlotName']] = $this->db_db_query($sql);
						  		
						  		// traverse and update status through today time slot
						  		$Result['TraverseWaviedTimeSlot'] = $this->Traverse_Wavied_Time_Slot($StaffID,$TodaySlot,$Duty,$SlotDuty['SlotName'],CARD_STATUS_ABSENT,$Today);
						  		
						  		// special handler for door access integration client - remove all early leave status before the last time slot
		  						if($sys_custom['StaffAttendance']['RemoveEarlyLeaveBeforeLastTimeSlot']){
		  							$tmp_start_time = date("H:i:s", $SlotTimeArray[$i]['Timestamp']);
		  							$this->Remove_EarlyLeave_Records_Before_LastTimeSlot($StaffID, $Today, $tmp_start_time);
		  						}
						  		
						  		if($sys_custom['StaffAttendance']['WongFutNamCollege']){
									$this->clearOtherDutyRecordsForCustomization($StaffID, $Today);
								}
						  		
						  		// debug_r($Result);
						  		return array($SlotDuty['SlotName'],CARD_RESP_STAFF_LEAVE_SCHOOL_EARLY);
						  	}
  						}
  					}
  				}
  				else { // tap card record between two time slot
  					$PrevSlotDuty = $Duty['CardSlotToDuty'][$SlotTimeArray[$i]['SlotName']][0];
  					$NextSlotDuty = $Duty['CardSlotToDuty'][$SlotTimeArray[($i+1)]['SlotName']][0];
  					// time slot 1 normal out
  					if (((trim($PrevSlotDuty['OutSchoolStatus']) == '' && trim($PrevSlotDuty['InSchoolStatus']) != '') || 
  							 $PrevSlotDuty['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE) && 
  							 ((strtotime($Today." ".$NextSlotDuty['DutyStart']) - $AbsSecForTimeSlot) >= $current_time) // tap card not falls to next time slot's absolute counting mins
  						 ) {
  						// clear profile record for time slot
  						$Result['ClearProfile:'.$PrevSlotDuty['SlotName']] = $this->Remove_Profile($PrevSlotDuty['OutAttendanceRecordID']);
					
  						$sql = 'update '.$TodayDailyLogTable.' set 
  											OutSchoolStatus = \''.CARD_STATUS_NORMAL.'\', 
  											OutSchoolStation = \''.$this->Get_Safe_Sql_Query($SiteName).'\', 
				  							OutTime = \''.$time_string.'\', 
				  							OutAttendanceRecordID = \'\', 
				  							MinEarlyLeave = \'\', 
				  							DateModified = NOW(), 
			  								ModifyBy = \''.$UpdatedByUserID.'\'  
				  						where 
				  							RecordID = \''.$PrevSlotDuty['RecordID'].'\'';
  						$Result['TodayNormalOut:'.$PrevSlotDuty['SlotName']] = $this->db_db_query($sql);
  						
  						// traverse and update status through today time slot
						  $Result['TraverseWaviedTimeSlot'] = $this->Traverse_Wavied_Time_Slot($StaffID,$TodaySlot,$Duty,$PrevSlotDuty['SlotName'],CARD_STATUS_ABSENT,$Today);
  						
  						// special handler for door access integration client - remove all early leave status before the last time slot
  						if($sys_custom['StaffAttendance']['RemoveEarlyLeaveBeforeLastTimeSlot']){
  							$tmp_start_time = date("H:i:s", $SlotTimeArray[$i]['Timestamp']);
  							$this->Remove_EarlyLeave_Records_Before_LastTimeSlot($StaffID, $Today, $tmp_start_time);
  						}
  						
  						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
							$this->clearOtherDutyRecordsForCustomization($StaffID, $Today);
						}
  						
  						// debug_r($Result);
  						return array($PrevSlotDuty['SlotName'],CARD_RESP_STAFF_LEAVE_SCHOOL_NORMAL);
  					}
  					// time slot 2 on time
  					else {
  						if ($NextSlotDuty['InSchoolStatus'] ==  CARD_STATUS_OUTGOING) { // no duty for that time slot
  							// find the first on duty time slot after this outing time slot
  							$return_result = $this->processOnDutyTimeSlotAfterOutingOrHoliday($StaffID, $Duty, $TodaySlot, $NextSlotDuty['SlotName'], $current_time, $SiteName);
		  					if($return_result !== false && is_array($return_result)){
		  						return $return_result;
		  					}
	  						return array($NextSlotDuty['SlotName'],STAFF_LEAVE_TYPE_OUTGOING);
	  					}
	  					else if ($NextSlotDuty['InSchoolStatus'] ==  CARD_STATUS_HOLIDAY) { // no duty for that time slot
	  						// find the first on duty time slot just after this holiday time slot 
	  						$return_result = $this->processOnDutyTimeSlotAfterOutingOrHoliday($StaffID, $Duty, $TodaySlot, $NextSlotDuty['SlotName'], $current_time, $SiteName);
		  					if($return_result !== false && is_array($return_result)){
		  						return $return_result;
		  					}
	  						return array($NextSlotDuty['SlotName'],STAFF_LEAVE_TYPE_HOLIDAY);
	  					}
	  					else if($AbsSecForTimeSlot > 0 && ((strtotime($Today." ".$NextSlotDuty['DutyStart']) - $AbsSecForTimeSlot) >= $current_time)){ // ignore tap card action
	  						return array($NextSlotDuty['SlotName'],CARD_RESP_IGNORE_WITHIN_PERIOD);
	  					}
	  					else{ // within that time slot's absolute counting mins
	  						if ($NextSlotDuty['InSchoolStatus'] != CARD_STATUS_NORMAL."") {
		  						// clear profile record for time slot
	  							$Result['ClearProfile:'.$NextSlotDuty['SlotName']] = $this->Remove_Profile($NextSlotDuty['InAttendanceRecordID']);
		  						
		  						$sql = 'update '.$TodayDailyLogTable.' set 
		  											InSchoolStatus = \''.CARD_STATUS_NORMAL.'\', 
		  											InSchoolStation = \''.$this->Get_Safe_Sql_Query($SiteName).'\', 
						  							InTime = \''.$time_string.'\', 
						  							InAttendanceRecordID = \'\', 
						  							MinLate = \'\',';
						  		if ($NextSlotDuty['OutWavie'] == "1") {
						  			$sql .= 'OutSchoolStatus = \''.CARD_STATUS_NORMAL.'\',
											 MinEarlyLeave = \'\', ';
						  		}
						  		$sql .= '
						  							DateModified = NOW(), 
				  									ModifyBy = \''.$UpdatedByUserID.'\'  
						  						where 
						  							RecordID = \''.$NextSlotDuty['RecordID'].'\'';
						  		$Result['TodayNormalIn:'.$NextSlotDuty['SlotName']] = $this->db_db_query($sql);
						  		
						  		// traverse and update status through today time slot
						  		if ($NextSlotDuty['OutWavie'] == "1") {
						  			$Result['TraverseWaviedTimeSlot'] = $this->Traverse_Wavied_Time_Slot($StaffID,$TodaySlot,$Duty,$NextSlotDuty['SlotName'],CARD_STATUS_NORMAL,$Today);
						  		}
						  	}
					  		
					  		// special handler for door access integration client - remove all early leave status before the last time slot
	  						if($sys_custom['StaffAttendance']['RemoveEarlyLeaveBeforeLastTimeSlot']){
	  							$tmp_start_time = $NextSlotDuty['DutyStart'];
	  							$this->Remove_EarlyLeave_Records_Before_LastTimeSlot($StaffID, $Today, $tmp_start_time);
	  						}
					  		
					  		if($sys_custom['StaffAttendance']['WongFutNamCollege']){
								$this->clearOtherDutyRecordsForCustomization($StaffID, $Today);
							}
					  		
					  		// debug_r($Result);
					  		return array($NextSlotDuty['SlotName'],CARD_RESP_STAFF_IN_SCHOOL_ONTIME);
					  	}
  					}
  				}
  			}
  		}
  		
  		// if below code ran, i means that the tap card is after the last out time
  		// update last time slot out status to NORMAL
  		// clear out profile record for time slot
  		$LastDuty = $Duty['CardSlotToDuty'][$SlotTimeArray[(sizeof($SlotTimeArray)-1)]['SlotName']][0];
		  if ($LastDuty['InSchoolStatus'] ==  CARD_STATUS_OUTGOING) { // no duty for that time slot
				// handle OTs
				$Result['ProcessOT'] = $this->Process_OT(true,$StaffID,$Today,$current_time,$LastDuty);
				return array($LastDuty['SlotName'],STAFF_LEAVE_TYPE_OUTGOING);
			}
			else if ($LastDuty['InSchoolStatus'] ==  CARD_STATUS_HOLIDAY) { // no duty for that time slot
				// handle OTs
				$Result['ProcessOT'] = $this->Process_OT(true,$StaffID,$Today,$current_time,$LastDuty);
				return array($LastDuty['SlotName'],STAFF_LEAVE_TYPE_HOLIDAY);
			}
			else {
				$Result['ClearOutProfile'] = $this->Remove_Profile($LastDuty['OutAttendanceRecordID']);
				
				// insert to daily record
	  		$sql = 'update '.$TodayDailyLogTable.' set ';
	  		if ($LastDuty['InSchoolStatus'] == CARD_STATUS_ABSENT || $LastDuty['InSchoolStatus'] == "") {
	  			$Result['ClearInProfile'] = $this->Remove_Profile($LastDuty['InAttendanceRecordID']);
	  			$sql .= 'InSchoolStatus = \''.CARD_STATUS_NORMAL.'\',
						 InAttendanceRecordID = NULL,
						 MinLate = \'\', ';
	  		}
	  		$sql .= '
									OutSchoolStatus = \''.CARD_STATUS_NORMAL.'\', 
									OutSchoolStation = \''.$this->Get_Safe_Sql_Query($SiteName).'\', 
	  							OutTime = \''.$time_string.'\', 
	  							OutAttendanceRecordID = \'\', 
	  							MinEarlyLeave = \'\', 
	  							DateModified = NOW(), 
			  					ModifyBy = \''.$UpdatedByUserID.'\'  
	  						where 
	  							RecordID = \''.$LastDuty['RecordID'].'\'';
				$Result['TodayNormalOut:'.$LastDuty['SlotName']] = $this->db_db_query($sql);
				//debug_r($sql);
				// debug_r($Result);
				
				// handle OTs
				$Result['ProcessOT'] = $this->Process_OT(true,$StaffID,$Today,$current_time,$LastDuty);
				
				// special handler for door access integration client - remove all early leave status before the last time slot
				if($sys_custom['StaffAttendance']['RemoveEarlyLeaveBeforeLastTimeSlot']){
					$tmp_start_time = $LastDuty['DutyStart'];
					$this->Remove_EarlyLeave_Records_Before_LastTimeSlot($StaffID, $Today, $tmp_start_time);
				}
				
				if($sys_custom['StaffAttendance']['WongFutNamCollege']){
					$this->clearOtherDutyRecordsForCustomization($StaffID, $Today);
				}
				
				//debug_r($Result);
				return array($LastDuty['SlotName'],CARD_RESP_STAFF_LEAVE_SCHOOL_NORMAL);
			}
  	}
	}
	
	// if DutyEnd is not "" then it is not normal OT, otherwise is Non-School day OT
	function Process_OT($IsNormalOT,$StaffID,$TargetDate,$current_time,$LastDuty="") {
		include_once("libgeneralsettings.php");
		
		$GeneralSetting = new libgeneralsettings();
		$SettingList[] = "'IgnoreOTTime'"; // time to ignore OT
		$SettingList[] = "'CountAsOT'"; // non school day count for OT
		$Settings = $GeneralSetting->Get_General_Setting('StaffAttendance',$SettingList);
		
		$Month = date('m',$current_time);
    $Year = date('Y',$current_time);
    $Day = date('j',$current_time);
		$time_string = date('H:i:s',$current_time);
		$Today = date('Y-m-d',$current_time);
    $ts_now = $current_time - strtotime($Today);
    
		if ($IsNormalOT) {
			$MinOT = floor(($ts_now - $LastDuty['DutyEndSec'])/60);
			if ($MinOT > $Settings['IgnoreOTTime'] && $MinOT>0) {
				$sql = 'insert into CARD_STAFF_ATTENDANCE2_OT_RECORD (
									StaffID,
									RecordDate,
									StartTime,
									EndTime,
									OTmins,
									DateInput,
									DateModified
									)
								values (
									\''.$StaffID.'\',
									\''.$TargetDate.'\',
									\''.$LastDuty['DutyEnd'].'\',
									\''.$time_string.'\',
									\''.$MinOT.'\',
									NOW(),
									NOW()
									)
								On Duplicate Key Update 
									EndTime = \''.$time_string.'\', 
									OTmins = \''.$MinOT.'\',
									DateModified = NOW()';
				//debug_r($sql);
				return $this->db_db_query($sql);
			}
			else 
				return true;
		}
		else {
			if ($Settings['CountAsOT'] == "1") {
				$RawLogTable = $this->buildStaffRawLogMonthTable($Year, $Month);
				
				$sql = 'select 
									MIN(TIME_TO_SEC(RecordTime)) as OTStartSec,
									MAX(TIME_TO_SEC(RecordTime)) as OTEndSec,
									MIN(RecordTime) as OTStart,
									MAX(RecordTime) as OTEnd
								From 
									'.$RawLogTable.' 
								where 
									DayNumber = \''.$Day.'\' 
									and 
									UserID = \''.$StaffID.'\'';
				$RawLog = $this->returnArray($sql);

				if ($RawLog[0]['OTStartSec'] != $RawLog[0]['OTEndSec'] && floor(($RawLog[0]['OTEndSec'] - $RawLog[0]['OTStartSec'])/60)>0) {
					$sql = 'insert into CARD_STAFF_ATTENDANCE2_OT_RECORD (
									StaffID,
									RecordDate,
									StartTime,
									EndTime,
									OTmins,
									DateInput,
									DateModified
									)
								values (
									\''.$StaffID.'\',
									\''.$TargetDate.'\',
									\''.$RawLog[0]['OTStart'].'\',
									\''.$RawLog[0]['OTEnd'].'\',
									\''.floor(($RawLog[0]['OTEndSec'] - $RawLog[0]['OTStartSec'])/60).'\',
									NOW(),
									NOW()
									)
								On Duplicate Key Update 
									EndTime = \''.$RawLog[0]['OTEnd'].'\', 
									OTmins = \''.floor(($RawLog[0]['OTEndSec'] - $RawLog[0]['OTStartSec'])/60).'\',
									DateModified = NOW()';
					//debug_r($sql);
					return $this->db_db_query($sql);
				}
				else 
					return true;
			}
			else 
				return true;
		}
	}
	
		
	function buildStaffRawLogMonthTable($year, $month) {
		$tablename = "CARD_STAFF_ATTENDANCE2_RAW_LOG_".$year."_".$month;
		
		$sql = 'show tables like \''.$tablename.'\'';
		$Exist = $this->returnArray($sql);
		
		if (sizeof($Exist) == 0) {
			$sql = "CREATE TABLE IF NOT EXISTS $tablename (
	      RecordID int(11) NOT NULL auto_increment,
	      UserID int(11) NOT NULL,
	      DayNumber int(11) NOT NULL,
	      RecordTime time,
	      RecordStation varchar(100),
	      RecordType int,
	      RecordStatus int,
	      DateInput datetime,
	      DateModified datetime,
	      PRIMARY KEY (RecordID),
	      INDEX DayNumber (DayNumber),
	      INDEX UserID (UserID)
	     ) ENGINE=InnoDB charset=utf8";
		  $this->db_db_query($sql);
		}
	  
    return $tablename;
	}
	
	function Remove_Profile($ProfileID) {
		$sql = 'delete from CARD_STAFF_ATTENDANCE2_PROFILE 
						where 
							RecordID = \''.$this->Get_Safe_Sql_Query($ProfileID).'\'';
		//debug_r($sql);
		return $this->db_db_query($sql);
	}
	
	function Create_Profile($TargetDate,$StaffID,$Wavie,$ProfileCount,$Status) {
		$sql = 'insert into CARD_STAFF_ATTENDANCE2_PROFILE 
							(
							StaffID,
							RecordDate,
							Waived,
							ProfileCountFor,
							RecordType,
							DateInput,
							DateModified
							)
						values 
							(
							\''.$this->Get_Safe_Sql_Query($StaffID).'\',
							\''.$this->Get_Safe_Sql_Query($TargetDate).'\',
							\''.$this->Get_Safe_Sql_Query($Wavie).'\',
							\''.$this->Get_Safe_Sql_Query($ProfileCount).'\',
							\''.$this->Get_Safe_Sql_Query($Status).'\',
							NOW(),
							NOW()
							)';
		//debug_r($sql);
		$Result = $this->db_db_query($sql);
		
		return $this->db_insert_id();
	}
	
	function Traverse_Wavied_Time_Slot($StaffID,$TimeSlotArray,$Duty,$SlotName,$Status,$TargetDate) {
		$DateArray = explode('-',$TargetDate);
		$Year = $DateArray[0];
		$Month = $DateArray[1];
		$DailyLogTable = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
		
		$Process = false;
		$Result = array();
		for ($i=0; $i< sizeof($TimeSlotArray); $i++) {
			$ProfileID = '';
			$CurrentSlot = $TimeSlotArray[$i];
			$CurrentDuty = $Duty['CardSlotToDuty'][$CurrentSlot['SlotName']][0];
			//debug_r($CurrentDuty);
			if ($Process) {
				if ($CurrentDuty['InSchoolStatus'] != CARD_STATUS_OUTGOING && $CurrentDuty['InSchoolStatus'] != CARD_STATUS_HOLIDAY) {
					if ($CurrentSlot['InWavie'] == "1" || $Status == CARD_STATUS_ABSENT) {
						// clear Profile
						$Result['ClearProfileIn'] = $this->Remove_Profile($CurrentDuty['InAttendanceRecordID']);
						$Result['ClearProfileOut'] = $this->Remove_Profile($CurrentDuty['OutAttendanceRecordID']);
						
						if ($Status == CARD_STATUS_ABSENT) { // insert profile
							$ProfileID = $this->Create_Profile($TargetDate,$StaffID,"",$CurrentSlot['DutyCount'],$Status);
						}
						$sql = 'update '.$DailyLogTable.' set 
			  							InSchoolStatus = \''.$Status.'\', 
			  							InAttendanceRecordID = \''.$ProfileID.'\', ';
			  		if ($CurrentSlot['OutWavie'] == "1" && $Status == CARD_STATUS_NORMAL) {
			  			$sql .= 'OutSchoolStatus = \''.CARD_STATUS_NORMAL.'\', ';
			  		}
			  		else {
			  			$sql .= 'OutSchoolStatus = NULL, ';
			  		}
			  		$sql .= '
			  							OutAttendanceRecordID = NULL
			  						Where 
			  							RecordID = \''.$CurrentDuty['RecordID'].'\'';
						$Result['Traverse:'.$CurrentSlot['SlotName']] = $this->db_db_query($sql);
						
						if ($CurrentSlot['OutWavie'] != "1") {
							if ($Status == CARD_STATUS_NORMAL) 
								break; // end the loop
						}
					}
					else {
						if ($Status == CARD_STATUS_NORMAL) 
							break; // end the loop
					}
				}
				else {
					if ($Status == CARD_STATUS_NORMAL) 
						break; // end the loop
				}
			}
			
			if ($SlotName == $CurrentSlot['SlotName']) 
				$Process = true;
		}
		
		// debug_r($Result);
		return !in_array(false,$Result);
	}
	
	function createTable_Card_Staff_Attendance2_Daily_Log($year="", $month="") {
		global $sys_custom;
	  $year = ($year == "") ? date("Y") : $year;
	  $month = ($month == "") ? date("m") : $month;
	
	  $card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$year."_".$month;
	  
	  if(!isset($this->DataCache[$card_log_table_name]))
	  {
	  	$sql = 'show tables like \''.$card_log_table_name.'\'';
		$Exist = $this->returnArray($sql);
		
		if (sizeof($Exist) == 0) {
		  $sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name (
		            RecordID int(11) NOT NULL auto_increment,
		            StaffID int(11) NOT NULL,
		            DayNumber int(11) NOT NULL,
					SlotName varchar(255),
					DutyCount double,
					InWavie int(1),
					OutWavie int(1),
		            Duty int,
		            DutyStart time,
		            DutyEnd time,
		            StaffPresent int,
		            InTime time,
		            InSchoolStatus int,
		            InSchoolStation varchar(100),
		            InWaived int,
		            InAttendanceRecordID int,
		            OutTime time,
		            OutSchoolStatus int,
		            OutSchoolStation varchar(100),
		            OutWaived int,
		            OutAttendanceRecordID int,
		            MinLate int,
		            MinEarlyLeave int,
		            RecordType int,
		            RecordStatus int,
				  	Remark varchar(255),
					AttendOne varchar(1) DEFAULT '0',
		            DateInput datetime,
		            InputBy int(11),
		            DateModified datetime,
					ModifyBy int(11),
					DateConfirmed datetime,
					ConfirmBy int(11),
		            PRIMARY KEY (RecordID),
		            UNIQUE KEY CompositeKey (StaffID,DayNumber,SlotName),
		            INDEX DayNumber (DayNumber),
		            INDEX StaffID (StaffID)
		  			) ENGINE=InnoDB charset=utf8";
		  //UNIQUE StaffDay (StaffID, DayNumber),
		  $this->db_db_query($sql);
		}else if($sys_custom['StaffAttendance']['WongFutNamCollege']){
			$sql = "ALTER TABLE $card_log_table_name ADD COLUMN AttendOne varchar(1) DEFAULT '0' AFTER REMARK";
			$this->db_db_query($sql);
		}
		
		$this->DataCache[$card_log_table_name] = 1;
	  }
	  return $card_log_table_name;
	}
	
	function Get_Duty_By_Date($Obj,$TargetDate,$CreateCardLog=true,$IgnoreFullDayHoliday=false,$FreezeDuty=false,$CacheResult=false) {
		global $sys_custom;
		if ($CacheResult) {
			global $ProcessedUserID, $ProcessedGroupID;
			global $ProcessedUserData, $ProcessedGroupData;
		}
		
		$DateArray = explode('-',$TargetDate);
		$Year = $DateArray[0];
		$Month = $DateArray[1];
		$Day = $DateArray[2];
		$IsPastOrTodayDuty = strtotime($TargetDate) <= strtotime(date('Y-m-d'));
		$CreateCardLog = ($CreateCardLog && $IsPastOrTodayDuty);
		
		if ($Obj instanceof staff_attend_group /* get_class($Obj) == "staff_attend_group"*/) { // group
			$GroupOrIndividual = "G";
			$ObjID = $Obj->GroupID;
			$UserList = ((sizeof($Obj->MemberList) > 0)? implode(',',$Obj->MemberList):'-1');
			$NormalCond = 'GroupID = \''.$Obj->GroupID.'\' ';
			$UserListInArray = $Obj->MemberList;
			$NonSchoolDayTableName = "CARD_STAFF_ATTENDANCE2_GROUP_DATE_DUTY";
			$IDField = "GroupID";
		}
		else { // individual
			$sql = 'select 
								GroupID 
							From 
								CARD_STAFF_ATTENDANCE_USERGROUP 
							Where 
								UserID = \''.$Obj.'\'';
			$IsGroupMember = $this->returnArray($sql);
			if (sizeof($IsGroupMember) > 0) {
				$GroupOrIndividual = "G";
				$ObjID = $IsGroupMember[0]['GroupID'];
				$NormalCond = 'GroupID = \''.$IsGroupMember[0]['GroupID'].'\' ';
				$WeeklyCond = 'and uwd.UserID = \''.$Obj.'\' ';
				$NonSchoolDayTableName = "CARD_STAFF_ATTENDANCE2_GROUP_DATE_DUTY";
				$IDField = "GroupID";
				
				$GroupObj = new staff_attend_group($ObjID,true);
				$GroupMemberList = $GroupObj->MemberList;
				$OtherGroupMemberList = array_values(array_diff($GroupMemberList,array($Obj))); // exclude the individual user itself
				if ($ProcessedGroupID[$TargetDate][$ObjID] != "Processed") {
					$this->Get_Duty_By_Date($GroupObj,$TargetDate,$CreateCardLog,$IgnoreFullDayHoliday,$FreezeDuty,$CacheResult);
				}
			}
			else {
				$GroupOrIndividual = "I";
				$ObjID = $Obj;
				$NormalCond = 'UserID = \''.$Obj.'\' ';
				$NonSchoolDayTableName = "CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY";
				$IDField = "UserID";
			}

			$UserList = $Obj;
			$UserListInArray[] = $Obj;
		}
		
		// is the same group
		if ((($Obj instanceof staff_attend_group) /* get_class($Obj) == "staff_attend_group"*/ && $ProcessedGroupID[$TargetDate][$ObjID] != "Processed") || 
				(!($Obj instanceof staff_attend_group) /*get_class($Obj) != "staff_attend_group"*/ && $ProcessedUserID[$TargetDate][$Obj] != "Processed")) {
			// get staff that is without daily log before load from settings
			if ($IsPastOrTodayDuty) {
				$StaffWithOutSetting = $this->Get_Staff_With_Out_Daily_Log($UserList,$Year,$Month,$Day);
				
				$StaffWithOutSetting = ((sizeof($StaffWithOutSetting) > 0)? implode(',',$StaffWithOutSetting):'-1');
			}
			else {
				$StaffWithOutSetting = $UserList;
			}
			
			// get avaliable staff (get staffs that is within employment period) and without daily log records
			$sql = 'select 
								wp.UserID 
							From 
								CARD_STAFF_ATTENDANCE3_WORKING_PERIOD wp 
								inner join 
								INTRANET_USER u 
								on wp.UserID = u.UserID 
							where 
								\''.$TargetDate.'\' between wp.PeriodStart and wp.PeriodEnd 
								and 
								wp.UserID in ('.$UserList.') 
							order by 
								u.EnglishName';
			$AvaliableStaff = $this->returnVector($sql);
			// get staff list not in employment period
			if (($Obj instanceof staff_attend_group)/* get_class($Obj) == "staff_attend_group"*/) { // group
				$UnAvaliableStaff = array_values(array_diff($Obj->MemberList,$AvaliableStaff));
			}
			else { // individual
				$UnAvaliableStaff = (sizeof($AvaliableStaff) > 0)? array():array($StaffWithOutSetting);
			}
			
			// get daily log records
			if(!isset($CardLogDetail)){
				$CardLogDetail = array();
			}
			if (!$FreezeDuty) {
				// the date is today/ past date, get records from Daily Log
				if ($IsPastOrTodayDuty) {
					// Get Daily Log Records
					$CardLogDetail = $this->Get_Daily_Log_Record($UserList,$Year,$Month,$Day,$UnAvaliableStaff);
				}
			}

			// Group - Slots Stat
			$Total = array(); // $Total[SlotID] = N;
			$ConfirmedTotal = array(); // $ConfirmedTotal[SlotID] = N;
			$NotConfirmedTotal = array(); // $NotConfirmedTotal[SlotID] = N;
			$LeaveTotal = array(); // $LeaveTotal[SlotID] = N;
			$UnassignedTotal = array(); // $UnassignedTotal[SlotID] = N;

			// get leave setting from db
			$HolidaySlotLeaveUserToSlot = array();
			$HolidaySlotLeaveSlotToUser = array();
			$HolidaySlotReasonSlotToUser = array(); // SlotID,StaffID to ReasonID
			$HolidaySlotStaffToRemark = array(); // SlotID,StaffID to Remark
			$OutgoingSlotLeaveUserToSlot = array();
			$OutgoingSlotLeaveSlotToUser = array();
			$OutgoingSlotReasonSlotToUser = array(); // SlotID,StaffID to ReasonID
			$OutgoingSlotStaffToRemark = array(); // SlotID,StaffID to Remark
			$FullDayLeave = array();
			$FullDayLeaveRemark = array(); // StaffID to Remark
			$FullHolidayLeave = array();
			$FullOutgoingLeave = array();
			$SlotInvolved = array();

			// get Super Special day duty -  schoolwide special setting
			$FinalDutyUserToSlot = array();
			$FinalDutySlotToUser = array();
			$StaffWithoutDuty = array();

			// if there is someone who without daily log (missed created daily log past record or future date)
			if ($StaffWithOutSetting != '-1') {

				$sql = 'select 
									lr.StaffID,
									lr.SlotID, 
									lr.RecordType,
									lr.ReasonID,
									lr.Remark 
								from
									CARD_STAFF_ATTENDANCE2_LEAVE_RECORD lr 
									inner join 
									INTRANET_USER u 
									on lr.StaffID = u.UserID 
								where 
									lr.RecordDate = \''.$TargetDate.'\' 
									and 
									lr.StaffID in ('.((sizeof($AvaliableStaff) > 0)? implode(',',$AvaliableStaff):'-1').') 
								order by 
									lr.RecordType, lr.SlotID, u.EnglishName';
				$LeaveList = $this->returnArray($sql);
				for ($i=0; $i< sizeof($LeaveList); $i++) {
					if ($LeaveList[$i]['SlotID'] == "") {
						$FullDayLeave[] = $LeaveList[$i]['StaffID'];
						$FullDayLeaveRemark[$LeaveList[$i]['StaffID']] = $LeaveList[$i]['Remark'];
						if ($LeaveList[$i]['RecordType'] == CARD_STATUS_HOLIDAY) { // holiday
							$FullHolidayLeave[] = $LeaveList[$i]['StaffID'];
							$FullHolidayLeaveReason[$LeaveList[$i]['StaffID']] = $LeaveList[$i]['ReasonID'];
						}
						else { // outgoing
							$FullOutgoingLeave[] = $LeaveList[$i]['StaffID'];
							$FullOutgoingLeaveReason[$LeaveList[$i]['StaffID']] = $LeaveList[$i]['ReasonID'];
						}
					}
					else {
						if ($LeaveList[$i]['RecordType'] == CARD_STATUS_HOLIDAY) { // holiday
							$HolidaySlotLeaveUserToSlot[$LeaveList[$i]['StaffID']][] = $LeaveList[$i]['SlotID'];
							$HolidaySlotLeaveSlotToUser[$LeaveList[$i]['SlotID']][] = $LeaveList[$i]['StaffID'];
							$HolidaySlotReasonSlotToUser[$LeaveList[$i]['SlotID']][$LeaveList[$i]['StaffID']] = $LeaveList[$i]['ReasonID'];
							$HolidaySlotStaffToRemark[$LeaveList[$i]['SlotID']][$LeaveList[$i]['StaffID']] = $LeaveList[$i]['Remark'];
						}
						else { // outgoing
							$OutgoingSlotLeaveUserToSlot[$LeaveList[$i]['StaffID']][] = $LeaveList[$i]['SlotID'];
							$OutgoingSlotLeaveSlotToUser[$LeaveList[$i]['SlotID']][] = $LeaveList[$i]['StaffID'];
							$OutgoingSlotReasonSlotToUser[$LeaveList[$i]['SlotID']][$LeaveList[$i]['StaffID']] = $LeaveList[$i]['ReasonID'];
							$OutgoingSlotStaffToRemark[$LeaveList[$i]['SlotID']][$LeaveList[$i]['StaffID']] = $LeaveList[$i]['Remark'];
						}
					}
				}
				$FullDayLeave = array_values(array_unique($FullDayLeave));
				$FullHolidayLeave = array_values(array_unique($FullHolidayLeave));
				$FullOutgoingLeave = array_values(array_unique($FullOutgoingLeave));
				// remove staff with full day leave or outgoing
				$StaffWithoutFullDayLeave = (sizeof($FullDayLeave) > 0 && !$IgnoreFullDayHoliday)? array_values(array_diff($AvaliableStaff,$FullDayLeave)):$AvaliableStaff;
				//debug_r($StaffWithoutFullDayLeave);
				

				$sql = 'select 
									s.SlotID 
								from 
									CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_DATE_RELATION sddr
									inner join 
									CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_TEMPLATE sdt 
									on 
										sddr.TargetDate = \''.$TargetDate.'\' 
										and 
										sddr.TemplateID = sdt.TemplateID 
									inner join 
									CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET sdst 
									on 
										sdt.TemplateID = sdst.TemplateID 
										and 
										sdst.GroupOrIndividual = \''.$GroupOrIndividual.'\' 
										and 
										sdst.ObjID = \''.$ObjID.'\' 
									inner join 
									CARD_STAFF_ATTENDANCE3_SLOT s 
									on 
										sdst.SlotID = s.SlotID';
				$SuperSpecialDutyArrangement = $this->returnVector($sql);
				for ($i=0; $i< sizeof($SuperSpecialDutyArrangement); $i++) {
					for ($j=0; $j< sizeof($StaffWithoutFullDayLeave); $j++) {
						$FinalDutySlotToUser[$SuperSpecialDutyArrangement[$i]][] = $StaffWithoutFullDayLeave[$j];
						$FinalDutyUserToSlot[$StaffWithoutFullDayLeave[$j]][] = $SuperSpecialDutyArrangement[$i];
						$SlotInvolved[$SuperSpecialDutyArrangement[$i]] = $SuperSpecialDutyArrangement[$i];
					}
				}
				
				if (sizeof($SuperSpecialDutyArrangement) > 0) {// if there is super special day setting exist, ignore special and normal duty setting
					if (sizeof($FinalDutySlotToUser) == 0) { // have school setting but staff not in employment period
						$DutyType = "Normal";
					}
					else {
						$DutyType = "SuperSpecial";
					}
				}
				else { 
					// get Special day duty
					// first check if is non school day
					$sql = "select 
										RecordID 
									from 
										".$NonSchoolDayTableName." 
									where 
										".$IDField." = '".$ObjID."' 
										and 
										DutyDate = '".$TargetDate."' 
										and 
										Duty = '0'";
					$IsNonSchoolDay = $this->returnVector($sql);
					if (sizeof($IsNonSchoolDay) > 0) {
						$StaffWithoutDuty = $StaffWithoutFullDayLeave;
						$DutyType = "Special";
						$HaveNonSchoolDay = 1;
					}
					else {				
						// else then check for duty
						$SpecialDateDutyUserList = array();
						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
							$SpecialDateDutyAttendOneUserList = array(); // customized to let staff only attend one duty time slot only on the same day no matter how many time slots are set on that day 
						}
						$sql = 'select 
											sdd.UserID,
											sdd.SlotID ';
						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
							$sql .= ',sdd.AttendOne ';
						}
							$sql .=' From 
											CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY sdd 
											inner join 
											INTRANET_USER u 
											on sdd.UserID = u.UserID
										where 
											sdd.DutyDate = \''.$TargetDate.'\' 
											and sdd.UserID in ('.((sizeof($StaffWithoutFullDayLeave) > 0)? implode(',',$StaffWithoutFullDayLeave):'-1').') 
										order by 
											sdd.SlotID, u.EnglishName
									';
						//debug_r($sql);
						$Result = $this->returnArray($sql);
						for($i=0; $i< sizeof($Result); $i++) {
							$FinalDutySlotToUser[$Result[$i]['SlotID']][] = $Result[$i]['UserID'];
							$FinalDutyUserToSlot[$Result[$i]['UserID']][] = $Result[$i]['SlotID'];
							$SpecialDateDutyUserList[$Result[$i]['UserID']] = $Result[$i]['UserID'];
							$SlotInvolved[$Result[$i]['SlotID']] = $Result[$i]['SlotID'];
							
							if($sys_custom['StaffAttendance']['WongFutNamCollege']){
								$SpecialDateDutyAttendOneUserList[$Result[$i]['UserID']] = $Result[$i]['AttendOne'];
							}
						}
						//debug_r($SpecialDateDutySlotToUser);
						// remove staff with special day duty
						$StaffWithoutSpecialDayDuty = (sizeof($SpecialDateDutyUserList) > 0)? array_values(array_diff($StaffWithoutFullDayLeave,$SpecialDateDutyUserList)):$StaffWithoutFullDayLeave;
						
						// check if any other group members have special duty
						$SpecailDateDutyOtherGroupMember = array();
						if (sizeof($IsGroupMember) > 0) {
							$sql = 'select 
										sdd.UserID 
									From 
										CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY sdd 
										inner join 
										INTRANET_USER u 
										on sdd.UserID = u.UserID
									where 
										sdd.DutyDate = \''.$TargetDate.'\' 
										and sdd.UserID in ('.((sizeof($OtherGroupMemberList) > 0)? implode(',',$OtherGroupMemberList):'-1').')';
							$SpecailDateDutyOtherGroupMember = $this->returnVector($sql);
						}
						
						// if there is special day setting, ignore normal duty period setting
						if ((sizeof($SpecialDateDutyUserList) == 0 && sizeof($SpecailDateDutyOtherGroupMember)==0)
							 || ($sys_custom['StaffAttendance']['NoSpecialDutyUseRegularDuty'] && count($StaffWithoutSpecialDayDuty)>0) ) {
							// get General duty setting
							$sql = 'select 
												GroupSlotPeriodID,
												SkipSchoolHoliday,
												SkipSchoolEvent,
												SkipPublicHoliday, 
												SkipAcademicEvent,
												DayType 
											From 
												CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD 
											where 
												'.$NormalCond.' 
												and 
												\''.$TargetDate.'\' between EffectiveStart and EffectiveEnd';
							//debug_r($sql);
							$Result = $this->returnArray($sql);
							
							// have set regular duty period
							if (sizeof($Result) > 0) {
								$DutyType = "Normal";
								if($sys_custom['StaffAttendance']['NoSpecialDutyUseRegularDuty'] && (sizeof($SpecialDateDutyUserList)>0 || sizeof($SpecailDateDutyOtherGroupMember)>0)) {
									$DutyType = "Special";
								}
								$RecordType = array();
								if ($Result[0]['SkipSchoolHoliday']) {
									$RecordType[] = SCHOOLHOLIDAY;
								}
								if ($Result[0]['SkipSchoolEvent']) {
									$RecordType[] = SCHOOLEVENT;
								}
								if ($Result[0]['SkipPublicHoliday']) {
									$RecordType[] = PUBLICHOLIDAY;
								}
								if ($Result[0]['SkipAcademicEvent']) {
									$RecordType[] = ACADEMICEVENT;
								}
								$sql = 'select 
													Title,
													Description 
												from 
													INTRANET_EVENT 
												where 
													EventDate = \''.$TargetDate.'\' 
													and 
													RecordType in ('.((sizeof($RecordType) > 0)? implode(',',$RecordType):'-1').') 
												order by 
													Title';
								//debug_r($sql);
								$EventList = $this->returnArray($sql);
								// have school event to skip attendance
								if (sizeof($EventList) > 0) {
									$StaffWithoutDuty = $StaffWithoutSpecialDayDuty;
								}
								else {
									// check for regular duty
									$DayType = $Result[0]['DayType'];
									if($DayType == 2){ // cycle day
										// find $TargetDate is which cycle day
										$DayOfWeek = $this->Get_Cycle_Day_By_Date($TargetDate);
									}else{ // weekday
										$DayOfWeek = date('w',strtotime($TargetDate));
									}
									$sql = 'select 
														uwd.UserID,
														uwds.SlotID
													from 
														CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY uwd 
														inner join 
														CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT uwds 
														on 
															uwd.DayOfWeek = \''.$DayOfWeek.'\' 
															and uwd.GroupSlotPeriodID = \''.$Result[0]['GroupSlotPeriodID'].'\' 
															and uwd.WeekDutyID = uwds.WeekDutyID 
															and uwd.UserID in ('.((sizeof($StaffWithoutSpecialDayDuty) > 0)? implode(',',$StaffWithoutSpecialDayDuty):'-1').') 
															'.$WeeklyCond.' 
														inner join 
														INTRANET_USER u 
														on uwd.UserID = u.UserID 
													order by
														uwds.SlotID, u.EnglishName
													';
									//debug_r($sql);
									$RegularResult = $this->returnArray($sql);
									for($i=0; $i< sizeof($RegularResult); $i++) {
										$FinalDutySlotToUser[$RegularResult[$i]['SlotID']][] = $RegularResult[$i]['UserID'];
										$FinalDutyUserToSlot[$RegularResult[$i]['UserID']][] = $RegularResult[$i]['SlotID'];
										$RegularDateDutyUserList[$RegularResult[$i]['UserID']] = $RegularResult[$i]['UserID'];
										$SlotInvolved[$RegularResult[$i]['SlotID']] = $RegularResult[$i]['SlotID'];
									}
									//debug_r($RegularDateDutySlotToUser);
									// Get User that is with out duty at the end
									$StaffWithoutDuty = (sizeof($RegularDateDutyUserList) > 0)? array_values(array_diff($StaffWithoutSpecialDayDuty,$RegularDateDutyUserList)):$StaffWithoutSpecialDayDuty;
								}
							}
							else {
								$StaffWithoutDuty = $StaffWithoutSpecialDayDuty;
							}
						}
						else {
							$DutyType = "Special";
							$StaffWithoutDuty = $StaffWithoutSpecialDayDuty;
						}
					}
				}
				
				// get slot detail 
				$SlotDetailAssociate = array();
				$sql = 'select 
									SlotID,
									SlotName,
									SlotStart,
									SlotEnd,
									DutyCount,
									InWavie,
									OutWavie 
								From 
									CARD_STAFF_ATTENDANCE3_SLOT 
								where 
									SlotID in ('.((sizeof($SlotInvolved) > 0)? implode(',',$SlotInvolved):'-1').') 
								order by 
									SlotStart';
				$SlotDetail = $this->returnArray($sql);
				// prepare slot assosiative array for create card log process
				for ($i=0; $i< sizeof($SlotDetail); $i++) {
					$SlotDetailAssociate[$SlotDetail[$i]['SlotID']] = $SlotDetail[$i];
				}
				
				/*$FinalDutySlotToUser = $RegularDateDutySlotToUser + $SpecialDateDutySlotToUser;
				$FinalDutyUserToSlot = $RegularDateDutyUserToSlot + $SpecialDateDutyUserToSlot;*/
				
				// create card log if 
				// 1. there is current no record in cardlog
				// 2. $Create CardLog is true (parameter value and is today/past date)
				if($CreateCardLog)
				{
					// Prepare a ReasonID to DefaultWavie mapping array (Holiday & Outing reasons)
					$sql = "SELECT ReasonID, DefaultWavie FROM CARD_STAFF_ATTENDANCE3_REASON WHERE ReasonActive = 1 AND ReasonType IN (".CARD_STATUS_HOLIDAY.",".CARD_STATUS_OUTGOING.")";
					$ReasonIDDefaultWavieArray = $this->returnArray($sql);
					$ReasonIDToDefaultWavieArray = array();
					for($tt=0;$tt<count($ReasonIDDefaultWavieArray);$tt++){
						$ReasonIDToDefaultWavieArray[$ReasonIDDefaultWavieArray[$tt]['ReasonID']] = $ReasonIDDefaultWavieArray[$tt]['DefaultWavie'];
					}
					
					# ==================================== Start Preparing Daily Log Records ==================================== #
					// if there is setting
					if ((sizeof($FullOutgoingLeave) > 0 || sizeof($FullHolidayLeave) > 0 || sizeof($FinalDutyUserToSlot) > 0)) {
						$LoggedUser = array();
						for ($i=0; $i< sizeof($CardLogDetail); $i++) {
							$LoggedUser[] = $CardLogDetail[$i]['StaffID'];
						}
						$CardLogTable = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year,$Month);
						
						// #1. -- Start Preparing CardLog for Full Outgoing Users --
						$FullOutgoingUserToCardLog = array();
						if (sizeof($FullOutgoingLeave) > 0) {
							// Find existing full day outgoing daily log records to prevent duplicated records (because NULL SlotName does not treat as UNIQUE KEY) 
							$sql = "SELECT StaffID FROM $CardLogTable WHERE StaffID IN (".implode(",",$FullOutgoingLeave).") AND DayNumber = '$Day' AND SlotName IS NULL AND RecordType = '".CARD_STATUS_OUTGOING."' ";
							$ExistingFullDayOutgoingStaffID = $this->returnVector($sql);
							
							for ($i=0; $i< sizeof($FullOutgoingLeave); $i++) {
								if (!in_array($FullOutgoingLeave[$i],$LoggedUser) && !in_array($FullOutgoingLeave[$i],$ExistingFullDayOutgoingStaffID)) {
									$sql = "SELECT StaffID FROM $CardLogTable WHERE StaffID='".$FullOutgoingLeave[$i]."' AND DayNumber = '$Day' AND SlotName IS NULL AND RecordType = '".CARD_STATUS_OUTGOING."' ";
									$TmpFullDayOutingDailyLog = $this->returnVector($sql);
									if(count($TmpFullDayOutingDailyLog)==0)
									{
										$LeaveReasonID = $FullOutgoingLeaveReason[$FullOutgoingLeave[$i]];
										$ReasonDefaultWavie = $ReasonIDToDefaultWavieArray[$LeaveReasonID]==1? "1":"NULL";
										$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
														(
															StaffID, 
															ReasonID,
															RecordDate,
															RecordType,
															Waived,
															DateInput,
															DateModified
														)
														VALUES
														(
															'".$FullOutgoingLeave[$i]."',
															'$LeaveReasonID',
															'$TargetDate',
															'".CARD_STATUS_OUTGOING."',
															".$ReasonDefaultWavie.",
															NOW(),
															NOW()
														)";
											
										$this->db_db_query($sql);
										$ProfileID = $this->db_insert_id();
										
										$sql = "INSERT INTO ".$CardLogTable."
														(
															StaffID, 
															DayNumber,
															Duty,
															InSchoolStatus,
															OutSchoolStatus,
															RecordType,
															InAttendanceRecordID,
															InWaived,
															Remark,
															DateInput,
															DateModified
														)
														VALUES
														(
															'".$FullOutgoingLeave[$i]."',
															'$Day',
															'0',
															'".CARD_STATUS_OUTGOING."',
															'".CARD_STATUS_OUTGOING."',
															'".CARD_STATUS_OUTGOING."',
															'$ProfileID',
															".$ReasonDefaultWavie.",
															".(isset($FullDayLeaveRemark[$FullOutgoingLeave[$i]]) && $FullDayLeaveRemark[$FullOutgoingLeave[$i]]!=''? "'".$this->Get_Safe_Sql_Query($FullDayLeaveRemark[$FullOutgoingLeave[$i]])."'" :"NULL").",
															NOW(),
															NOW()
														)";
										$this->db_db_query($sql);
										$CardLogID = $this->db_insert_id();
										$ExistingFullDayOutgoingStaffID[] = $FullOutgoingLeave[$i];
									}
								}
							}
						}
						// #1. -- End of Preparing CardLog for Full Outgoing Users --
						
						// #2. -- Start Preparing CardLog for Full Holiday Leave Users --
						$FullHolidayUserToCardLog = array();
						if (sizeof($FullHolidayLeave) > 0) {
							// Find existing full day holiday daily log records to prevent duplicated records (because NULL SlotName does not treat as UNIQUE KEY) 
							$sql = "SELECT StaffID FROM $CardLogTable WHERE StaffID IN (".implode(",",$FullHolidayLeave).") AND DayNumber = '$Day' AND SlotName IS NULL AND RecordType = '".CARD_STATUS_HOLIDAY."' ";
							$ExistingFullDayHolidayStaffID = $this->returnVector($sql);
							
							for ($i=0; $i< sizeof($FullHolidayLeave); $i++) {
								if (!in_array($FullHolidayLeave[$i],$LoggedUser) && !in_array($FullHolidayLeave[$i],$ExistingFullDayHolidayStaffID)) {
									$sql = "SELECT StaffID FROM $CardLogTable WHERE StaffID='".$FullHolidayLeave[$i]."' AND DayNumber = '$Day' AND SlotName IS NULL AND RecordType = '".CARD_STATUS_HOLIDAY."' ";
									$TmpFullDayHolidayDailyLog = $this->returnVector($sql);
									if(count($TmpFullDayHolidayDailyLog)==0)
									{
										$LeaveReasonID = $FullHolidayLeaveReason[$FullHolidayLeave[$i]];
										$ReasonDefaultWavie = $ReasonIDToDefaultWavieArray[$LeaveReasonID]==1? "1":"NULL";
										$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
														(
															StaffID, 
															ReasonID,
															RecordDate,
															RecordType,
															Waived,
															DateInput,
															DateModified
														)
														VALUES
														(
															'".$FullHolidayLeave[$i]."',
															'$LeaveReasonID',
															'$TargetDate',
															'".CARD_STATUS_HOLIDAY."',
															".$ReasonDefaultWavie.",
															NOW(),
															NOW()
														)";
											
										$this->db_db_query($sql);
										$ProfileID = $this->db_insert_id();
										
										$sql = "INSERT INTO ".$CardLogTable."
														(
															StaffID, 
															DayNumber,
															Duty,
															InSchoolStatus,
															OutSchoolStatus,
															RecordType,
															InAttendanceRecordID,
															InWaived,
															Remark,
															DateInput,
															DateModified
														)
														VALUES
														(
															'".$FullHolidayLeave[$i]."',
															'$Day',
															'0',
															'".CARD_STATUS_HOLIDAY."',
															'".CARD_STATUS_HOLIDAY."',
															'".CARD_STATUS_HOLIDAY."',
															'$ProfileID',
															".$ReasonDefaultWavie.",
															".(isset($FullDayLeaveRemark[$FullHolidayLeave[$i]]) && $FullDayLeaveRemark[$FullHolidayLeave[$i]]!=''? "'".$this->Get_Safe_Sql_Query($FullDayLeaveRemark[$FullHolidayLeave[$i]])."'" :"NULL").",
															NOW(),
															NOW()
														)";
										$this->db_db_query($sql);
										$CardLogID = $this->db_insert_id();
										$ExistingFullDayHolidayStaffID[] = $FullHolidayLeave[$i];
									}
								}
							}
						}
						// #2. -- End of Preparing CardLog for Full Holiday Leave Users --
						
						$sql = "SELECT StaffID FROM CARD_STAFF_ATTENDANCE2_LEAVE_RECORD WHERE RecordDate='".$TargetDate."' AND SlotID IS NULL"; // find full day leave only
						$leave_userid_ary = $this->returnVector($sql);
						// #3. -- Start Preparing CardLog for On Duty Users --
						$UserToCardLog = array(); // Store CardLog RecordIDs
						foreach($FinalDutyUserToSlot as $StaffID => $SlotArray)
						{
							if (!in_array($StaffID,$LoggedUser) && !in_array($StaffID,$FullDayLeave) && !in_array($StaffID,$leave_userid_ary)) {
								for($i=0;$i<sizeof($SlotArray);$i++)
								{	
									$SlotID = $SlotArray[$i];
									$SlotName = $SlotDetailAssociate[$SlotID]['SlotName'];
									$SlotStart = $SlotDetailAssociate[$SlotID]['SlotStart'];
									$SlotEnd = $SlotDetailAssociate[$SlotID]['SlotEnd'];
									$SlotDutyCount = $SlotDetailAssociate[$SlotID]['DutyCount'];
									$SlotInWavie = $SlotDetailAssociate[$SlotID]['InWavie'];
									$SlotOutWavie = $SlotDetailAssociate[$SlotID]['OutWavie'];
									
									if((isset($HolidaySlotLeaveUserToSlot[$StaffID]) && in_array($SlotID,$HolidaySlotLeaveUserToSlot[$StaffID])) 
										|| (isset($OutgoingSlotLeaveUserToSlot[$StaffID]) && in_array($SlotID,$OutgoingSlotLeaveUserToSlot[$StaffID]) )){
										// has set on leave or outing for that time slot, skip
										continue;
									}
									
									$sql = "INSERT INTO ".$CardLogTable."
													(
														StaffID, 
														DayNumber,
														SlotName,
														Duty,
														DutyStart,
														DutyEnd,
														DutyCount,
														InWavie,
														OutWavie,";
											if($sys_custom['StaffAttendance']['WongFutNamCollege']){
												$sql .= " AttendOne, ";
											}
											  $sql .= " DateInput,
														DateModified
													)
													VALUES
													(
														'$StaffID',
														'$Day',
														'".$this->Get_Safe_Sql_Query($SlotName)."',
														'1',
														'$SlotStart',
														'$SlotEnd',
														'$SlotDutyCount',
														'$SlotInWavie',
														'$SlotOutWavie',";
											if($sys_custom['StaffAttendance']['WongFutNamCollege']){
												$sql .= " '".($SpecialDateDutyAttendOneUserList[$StaffID])."', ";
											}
											  $sql .= " NOW(),
														NOW()
													)";
									//debug_r($sql);
									$this->db_db_query($sql);
									$CardLogID = $this->db_insert_id();
									$UserToCardLog[] = $CardLogID;
								}
							}
						}
						// #3. -- End of Preparing CardLog for On Duty Users --
						
						// #4. -- Start Preparing CardLog for Outgoing Users --
						$OutgoingUserToCardLog = array();
						foreach($OutgoingSlotLeaveUserToSlot as $StaffID => $SlotArray)
						{
							if (!in_array($StaffID,$LoggedUser) && !in_array($StaffID, $FullDayLeave)) {
								for($i=0;$i<sizeof($SlotArray);$i++)
								{	
									$SlotID = $SlotArray[$i];
									$SlotName = $SlotDetailAssociate[$SlotID]['SlotName'];
									$SlotStart = $SlotDetailAssociate[$SlotID]['SlotStart'];
									$SlotEnd = $SlotDetailAssociate[$SlotID]['SlotEnd'];
									$SlotDutyCount = $SlotDetailAssociate[$SlotID]['DutyCount'];
									$SlotInWavie = $SlotDetailAssociate[$SlotID]['InWavie'];
									$SlotOutWavie = $SlotDetailAssociate[$SlotID]['OutWavie'];
									$SlotRemark = isset($OutgoingSlotStaffToRemark[$SlotID]) && isset($OutgoingSlotStaffToRemark[$SlotID][$StaffID])? $OutgoingSlotStaffToRemark[$SlotID][$StaffID] : '';
									
									$LeaveReasonID = $OutgoingSlotReasonSlotToUser[$SlotID][$StaffID];
									$ReasonDefaultWavie = $ReasonIDToDefaultWavieArray[$LeaveReasonID]==1? "1":"NULL";
									
									$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
													(
														StaffID, 
														ReasonID,
														RecordDate,
														RecordType,
														Waived,
														DateInput,
														DateModified
													)
													VALUES
													(
														'$StaffID',
														'$LeaveReasonID',
														'$TargetDate',
														'".CARD_STATUS_OUTGOING."',
														".$ReasonDefaultWavie.",
														NOW(),
														NOW()
													)";
									$this->db_db_query($sql);
									$ProfileID = $this->db_insert_id();
									
									$sql = "INSERT INTO ".$CardLogTable."
												(
													StaffID, 
													DayNumber,
													SlotName,
													Duty,
													DutyStart,
													DutyEnd,
													DutyCount,
													InWavie,
													OutWavie,
													InSchoolStatus,
													OutSchoolStatus,
													RecordType,
													InAttendanceRecordID,
													InWaived,
													Remark,
													DateInput,
													DateModified
												)
												VALUES
												(
													'$StaffID',
													'$Day',
													'".$this->Get_Safe_Sql_Query($SlotName)."',
													'0',
													'$SlotStart',
													'$SlotEnd',
													'$SlotDutyCount',
													'$SlotInWavie',
													'$SlotOutWavie',
													'".CARD_STATUS_OUTGOING."',
													'".CARD_STATUS_OUTGOING."',
													'".CARD_STATUS_OUTGOING."',
													'$ProfileID',
													".$ReasonDefaultWavie.",
													'".$this->Get_Safe_Sql_Query($SlotRemark)."',
													NOW(),
													NOW()
												) 
												On Duplicate Key Update 
													RecordID = LAST_INSERT_ID(RecordID), 
													Duty = '0', 
													InSchoolStatus = '".CARD_STATUS_OUTGOING."',
													OutSchoolStatus = '".CARD_STATUS_OUTGOING."',
													RecordType = '".CARD_STATUS_OUTGOING."',
													InAttendanceRecordID = '$ProfileID',
													InWaived = ".$ReasonDefaultWavie.",
													Remark = '".$this->Get_Safe_Sql_Query($SlotRemark)."',
													DateModified = NOW()
													";
									$this->db_db_query($sql);
									$CardLogID = $this->db_insert_id();
									$OutgoingUserToCardLog[] = $CardLogID;
								}
							}
						}
						// #4. -- End of Preparing CardLog for Outgoing Users --
						
						// #5. -- Start Preparing CardLog for On Holiday Users --
						$HolidayUserToCardLog = array();
						foreach($HolidaySlotLeaveUserToSlot as $StaffID => $SlotArray)
						{
							if (!in_array($StaffID,$LoggedUser) && !in_array($StaffID, $FullDayLeave)) {
								for($i=0;$i<sizeof($SlotArray);$i++)
								{	
									$SlotID = $SlotArray[$i];
									$SlotName = $SlotDetailAssociate[$SlotID]['SlotName'];
									$SlotStart = $SlotDetailAssociate[$SlotID]['SlotStart'];
									$SlotEnd = $SlotDetailAssociate[$SlotID]['SlotEnd'];
									$SlotDutyCount = $SlotDetailAssociate[$SlotID]['DutyCount'];
									$SlotInWavie = $SlotDetailAssociate[$SlotID]['InWavie'];
									$SlotOutWavie = $SlotDetailAssociate[$SlotID]['OutWavie'];
									$SlotRemark = isset($HolidaySlotStaffToRemark[$SlotID]) && isset($HolidaySlotStaffToRemark[$SlotID][$StaffID])? $HolidaySlotStaffToRemark[$SlotID][$StaffID] : '';
									
									$LeaveReasonID = $HolidaySlotReasonSlotToUser[$SlotID][$StaffID];
									$ReasonDefaultWavie = $ReasonIDToDefaultWavieArray[$LeaveReasonID]==1? "1":"NULL";
									
									$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
													(
														StaffID, 
														ReasonID,
														RecordDate,
														RecordType,
														Waived,
														DateInput,
														DateModified
													)
													VALUES
													(
														'$StaffID',
														'$LeaveReasonID',
														'$TargetDate',
														'".CARD_STATUS_HOLIDAY."',
														".$ReasonDefaultWavie.",
														NOW(),
														NOW()
													)";
									$this->db_db_query($sql);
									$ProfileID = $this->db_insert_id();
									
									$sql = "INSERT INTO ".$CardLogTable."
													(
														StaffID, 
														DayNumber,
														SlotName,
														Duty,
														DutyStart,
														DutyEnd,
														DutyCount,
														InWavie,
														OutWavie,
														InSchoolStatus,
														OutSchoolStatus,
														RecordType,
														InAttendanceRecordID,
														InWaived,
														Remark,
														DateInput,
														DateModified
													)
													VALUES
													(
														'$StaffID',
														'$Day',
														'".$this->Get_Safe_Sql_Query($SlotName)."',
														'0',
														'$SlotStart',
														'$SlotEnd',
														'$SlotDutyCount',
														'$SlotInWavie',
														'$SlotOutWavie',
														'".CARD_STATUS_HOLIDAY."',
														'".CARD_STATUS_HOLIDAY."',
														'".CARD_STATUS_HOLIDAY."',
														'$ProfileID',
														".$ReasonDefaultWavie.",
														'".$this->Get_Safe_Sql_Query($SlotRemark)."',
														NOW(),
														NOW()
													)
													On Duplicate Key Update 
														RecordID = LAST_INSERT_ID(RecordID), 
														Duty = '0', 
														InSchoolStatus = '".CARD_STATUS_HOLIDAY."',
														OutSchoolStatus = '".CARD_STATUS_HOLIDAY."',
														RecordType = '".CARD_STATUS_HOLIDAY."',
														InAttendanceRecordID = '$ProfileID',
														InWaived = ".$ReasonDefaultWavie.",
														Remark = '".$this->Get_Safe_Sql_Query($SlotRemark)."',
														DateModified = NOW()
													";
									$this->db_db_query($sql);
									$CardLogID = $this->db_insert_id();
									$HolidayUserToCardLog[] = $CardLogID;
								}
							}
						}
						// #5. -- End of Preparing CardLog for On Holiday Users --
						
						// #6. -- Start Preparing CardLog for No Duty User (No Setting/ Not in employment period)
						if (sizeof($UnAvaliableStaff) > 0) {
							foreach ($UnAvaliableStaff as $Key => $Val) {
								if(!in_array($Val,$FullDayLeave)){
									$sql = "insert into ".$CardLogTable."
													(
														StaffID, 
														DayNumber,
														Duty,
														InSchoolStatus,
														OutSchoolStatus,
														RecordType,
														DateInput,
														DateModified
													)
													VALUES
													(
														'".$Val."',
														'$Day',
														'0',
														'".CARD_STATUS_NOSETTING."',
														'".CARD_STATUS_NOSETTING."',
														'".CARD_STATUS_NOSETTING."',
														NOW(),
														NOW()
													)";
									$this->db_db_query($sql);
								}
							}
						}
						
						if (sizeof($StaffWithoutDuty) > 0) {
							foreach ($StaffWithoutDuty as $Key => $Val) {
								if(!in_array($Val,$FullDayLeave)){
									$sql = "insert into ".$CardLogTable."
													(
														StaffID, 
														DayNumber,
														Duty,
														InSchoolStatus,
														OutSchoolStatus,
														RecordType,
														DateInput,
														DateModified
													)
													VALUES
													(
														'".$Val."',
														'$Day',
														'0',
														'".CARD_STATUS_NOSETTING."',
														'".CARD_STATUS_NOSETTING."',
														'".CARD_STATUS_NOSETTING."',
														NOW(),
														NOW()
													)";
									//debug_r($sql);
									$this->db_db_query($sql);
								}
							}
						}
						// #6. -- End of Preparing CardLog for No Duty User (No Setting/ Not in employment period)
					}
					else { // if there is no any setting in that day
						if (sizeof($CardLogDetail) == 0) {
							$CardLogTable = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year,$Month);
							for ($i=0; $i< sizeof($UserListInArray); $i++) {
								$sql = "insert into ".$CardLogTable."
												(
													StaffID, 
													DayNumber,
													Duty,
													InSchoolStatus,
													OutSchoolStatus,
													RecordType,
													DateInput,
													DateModified
												)
												VALUES
												(
													'".$UserListInArray[$i]."',
													'$Day',
													'0',
													'".CARD_STATUS_NOSETTING."',
													'".CARD_STATUS_NOSETTING."',
													'".CARD_STATUS_NOSETTING."',
													NOW(),
													NOW()
												)";
								$this->db_db_query($sql);
							}
						}
					}
					
					$this->cleanFullDayRecords($Year, $Month);
					# =================================== End of Preparing Daily Log Records ================================== #
				}
			}
			
			// regular get setting
			if (!$FreezeDuty) {
				// the date is today/ past date, get records from Daily Log
				if ($IsPastOrTodayDuty) {
					// Get Daily Log Records
					$CardLogDetail = $this->Get_Daily_Log_Record($UserList,$Year,$Month,$Day,$UnAvaliableStaff);
					//debug_r($CardLogDetail);
						
					$HaveDailyLog = (sizeof($CardLogDetail) > 0);
					$CardFullHoliday = array();
					$CardFullOutgoing = array();
					$CardFullHolidayReason = array();
					$CardFullOutgoingReason = array();
					$CardHolidaySlotToUser = array();
					$CardHolidayUserToSlot = array();
					$CardOutgoingSlotToUser = array();
					$CardOutgoingUserToSlot = array();
					$CardOnDutySlotToUser = array();
					$CardOnDutyUserToSlot = array();
					$CardSlotToDuty = array();
					$CardUserToDuty = array();
					$CardSlotDetail = array();
					$CardStaffWithSetting = array();
					$CardSlotHolidayReasonSlotToUser = array();
					$CardSlotOutgoingReasonSlotToUser = array();
					for ($i=0; $i< sizeof($CardLogDetail); $i++) {
						
						if($sys_custom['StaffAttendance']['WongFutNamCollege'] && $CardLogDetail[$i]['AttendOne'] == 1){
							$AttendOne = 1;
						}
						
						if ($CardLogDetail[$i]['RecordType'] != CARD_STATUS_NOSETTING) {
							if (trim($CardLogDetail[$i]['SlotName']) != "") {
								$CardSlotDetail[$CardLogDetail[$i]['SlotName']] = array('SlotName'=>$CardLogDetail[$i]['SlotName'],
																																				'SlotStart'=>$CardLogDetail[$i]['DutyStart'],
																																				'SlotEnd'=>$CardLogDetail[$i]['DutyEnd'],
																																				'InWavie'=>$CardLogDetail[$i]['InWavie'],
																																				'OutWavie'=>$CardLogDetail[$i]['OutWavie'],
																																				'DutyCount'=>$CardLogDetail[$i]['DutyCount']);
							}
							$CardStaffWithSetting[$CardLogDetail[$i]['StaffID']] = $CardLogDetail[$i]['StaffID'];
							$CardSlotToDuty[$CardLogDetail[$i]['SlotName']][] = $CardLogDetail[$i];
							$CardUserToDuty[$CardLogDetail[$i]['StaffID']][] = $CardLogDetail[$i];
							$CardUserToDutyStartToDuty[$CardLogDetail[$i]['StaffID']][$CardLogDetail[$i]['DutyStart']] = $CardLogDetail[$i];
							$CardUserToDutyEndToDuty[$CardLogDetail[$i]['StaffID']][$CardLogDetail[$i]['DutyEnd']] = $CardLogDetail[$i];
							
							// full outgoing
							if ($CardLogDetail[$i]['InSchoolStatus'] == CARD_STATUS_OUTGOING 
								&& trim($CardLogDetail[$i]['SlotName']) == "" 
								&& trim($CardLogDetail[$i]['DutyStart']) == "" 
								&& trim($CardLogDetail[$i]['DutyEnd']) == "") {
								$CardFullOutgoing[] = $CardLogDetail[$i]['StaffID'];
								$CardFullOutgoingReason[$CardLogDetail[$i]['StaffID']] = $CardLogDetail[$i]['InReasonID'];
							}
							// full holiday
							else if ($CardLogDetail[$i]['InSchoolStatus'] == CARD_STATUS_HOLIDAY 
								&& trim($CardLogDetail[$i]['SlotName']) == "" 
								&& trim($CardLogDetail[$i]['DutyStart']) == "" 
								&& trim($CardLogDetail[$i]['DutyEnd']) == "") {
								$CardFullHoliday[] = $CardLogDetail[$i]['StaffID'];
								$CardFullHolidayReason[$CardLogDetail[$i]['StaffID']] = $CardLogDetail[$i]['InReasonID'];
							}
							// slot outgoing
							else if ($CardLogDetail[$i]['InSchoolStatus'] == CARD_STATUS_OUTGOING 
								&& trim($CardLogDetail[$i]['SlotName']) != "" 
								&& trim($CardLogDetail[$i]['DutyStart']) != "") {
								$CardOutgoingSlotToUser[$CardLogDetail[$i]['SlotName']][] = $CardLogDetail[$i]['StaffID'];
								$CardOutgoingUserToSlot[$CardLogDetail[$i]['StaffID']][] = $CardLogDetail[$i]['SlotName'];
								$CardOnDutySlotToUser[$CardLogDetail[$i]['SlotName']][] = $CardLogDetail[$i]['StaffID'];
								$CardOnDutyUserToSlot[$CardLogDetail[$i]['StaffID']][] = $CardLogDetail[$i]['SlotName'];
								$CardSlotOutgoingReasonSlotToUser[$CardLogDetail[$i]['SlotName']][$CardLogDetail[$i]['StaffID']] = $CardLogDetail[$i]['InReasonID'];
							}
							// slot holiday
							else if ($CardLogDetail[$i]['InSchoolStatus'] == CARD_STATUS_HOLIDAY 
								&& trim($CardLogDetail[$i]['SlotName']) != "" 
								&& trim($CardLogDetail[$i]['DutyStart']) != "") {
								$CardHolidaySlotToUser[$CardLogDetail[$i]['SlotName']][] = $CardLogDetail[$i]['StaffID'];
								$CardHolidayUserToSlot[$CardLogDetail[$i]['StaffID']][] = $CardLogDetail[$i]['SlotName'];
								$CardOnDutySlotToUser[$CardLogDetail[$i]['SlotName']][] = $CardLogDetail[$i]['StaffID'];
								$CardOnDutyUserToSlot[$CardLogDetail[$i]['StaffID']][] = $CardLogDetail[$i]['SlotName'];
								$CardSlotHolidayReasonSlotToUser[$CardLogDetail[$i]['SlotName']][$CardLogDetail[$i]['StaffID']] = $CardLogDetail[$i]['InReasonID'];
							}
							// slot on duty
							else if ($CardLogDetail[$i]['InSchoolStatus'] != CARD_STATUS_HOLIDAY 
								&& $CardLogDetail[$i]['InSchoolStatus'] != CARD_STATUS_OUTGOING 
								&& trim($CardLogDetail[$i]['SlotName']) != "" 
								&& trim($CardLogDetail[$i]['DutyStart']) != "" 
								&& trim($CardLogDetail[$i]['Duty']) == "1" ) {
								$CardOnDutySlotToUser[$CardLogDetail[$i]['SlotName']][] = $CardLogDetail[$i]['StaffID'];
								$CardOnDutyUserToSlot[$CardLogDetail[$i]['StaffID']][] = $CardLogDetail[$i]['SlotName'];
							}
							// others?? should not be others!!
							else {
							}
						}
					}
				}
				
				// get avaliable staff (get staffs that is within employment period) and without daily log records
				$sql = 'select 
									wp.UserID 
								From 
									CARD_STAFF_ATTENDANCE3_WORKING_PERIOD wp 
									inner join 
									INTRANET_USER u 
									on wp.UserID = u.UserID 
								where 
									\''.$TargetDate.'\' between wp.PeriodStart and wp.PeriodEnd 
									and 
									wp.UserID in ('.$UserList.') 
								order by 
									u.EnglishName';
				//debug_r($sql);
				$OverallAvaliableStaff = $this->returnVector($sql);
				// get staff list not in employment period
				if (($Obj instanceof staff_attend_group)/*get_class($Obj) == "staff_attend_group"*/) { // group
					$OverallUnAvaliableStaff = array_values(array_diff($Obj->MemberList,$OverallAvaliableStaff));
				}
				else { // individual
					$OverallUnAvaliableStaff = (sizeof($OverallAvaliableStaff) > 0)? array():array($UserList);
				}
				
				// get user detail
				$NameField = getNameFieldByLang('u.');
				$UserDetail = array();
				$sql = 'select 
									u.UserID, 
									'.$NameField.' as UserName 
								from 
									INTRANET_USER u 
								where 
									u.UserID in (-1,'.implode(',',$OverallAvaliableStaff).') 
								order by 
									u.EnglishName';
				$Result = $this->returnArray($sql);
				for ($i=0; $i< sizeof($Result); $i++) {
					$UserDetail[$Result[$i]['UserID']] = $Result[$i]['UserName'];
				}
				
				$Return = array(
										'UserDetail'=> $UserDetail,
										'SlotDetail'=> $SlotDetail,
										'SlotIDList'=> $SlotInvolved,
										'FullOutgoing'=> $FullOutgoingLeave,
										'FullHoliday'=> $FullHolidayLeave,
										'UserToSlotHoliday'=> $HolidaySlotLeaveUserToSlot,
										'UserToSlotOutgoing'=> $OutgoingSlotLeaveUserToSlot,
										'SlotToUserHoliday'=> $HolidaySlotLeaveSlotToUser,
										'HolidaySlotReason'=> $HolidaySlotReasonSlotToUser,
										'OutgoingSlotReason'=> $OutgoingSlotReasonSlotToUser,
										'HolidayFullReason'=> $FullHolidayLeaveReason,
										'OutgoingFullReason'=> $FullOutgoingLeaveReason,
										'SlotToUserOutgoing'=> $OutgoingSlotLeaveSlotToUser,
										'SlotToUserDuty'=> $FinalDutySlotToUser,
										'UserToSlotDuty'=> $FinalDutyUserToSlot,
										'StaffWithoutSetting'=> $StaffWithoutDuty,
										'UnAvaliableStaff'=> $OverallUnAvaliableStaff,
										'EventList'=> $EventList, 
										'HaveDailyLog'=> $HaveDailyLog,
										'CardSlotDetail'=> $CardSlotDetail,
										'CardFullHoliday'=> $CardFullHoliday,
										'CardFullOutgoing'=> $CardFullOutgoing,
										'CardFullHolidayReason'=> $CardFullHolidayReason,
										'CardFullOutgoingReason'=> $CardFullOutgoingReason,
										'CardHolidaySlotToUser'=> $CardHolidaySlotToUser,
										'CardHolidayUserToSlot'=> $CardHolidayUserToSlot,
										'CardOutgoingSlotToUser' => $CardOutgoingSlotToUser,
										'CardOutgoingUserToSlot' => $CardOutgoingUserToSlot,
										'CardSlotOutgoingReason' => $CardSlotOutgoingReasonSlotToUser,
										'CardSlotHolidayReason' => $CardSlotHolidayReasonSlotToUser,
										'CardOnDutySlotToUser' => $CardOnDutySlotToUser,
										'CardOnDutyUserToSlot' => $CardOnDutyUserToSlot, 
										'CardStaffWithSetting' => $CardStaffWithSetting,
										'CardSlotToDuty' => $CardSlotToDuty,
										'CardUserToDuty' => $CardUserToDuty,
										'CardUserToDutyStartToDuty' => $CardUserToDutyStartToDuty,
										'CardUserToDutyEndToDuty' => $CardUserToDutyEndToDuty,
										'DutyType' => $DutyType,
										'HaveSpecialNonSchoolDay' => $HaveNonSchoolDay
										//'UserToCardLog' => $UserToCardLog,
										//'FullHolidayUserToCardLog' => $FullHolidayUserToCardLog,
										//'FullOutgoingUserToCardLog' => $FullOutgoingUserToCardLog,
										//'HolidayUserToCardLog' => $HolidayUserToCardLog,
										//'OutgoingUserToCardLog' => $OutgoingUserToCardLog
									);
				if($sys_custom['StaffAttendance']['WongFutNamCollege']){
					$Return['SpecialDateDutyAttendOneUser'] = $SpecialDateDutyAttendOneUserList;
					$Return['AttendOne'] = $AttendOne;
				}
				// save result to cache
				if ($CacheResult) {
					if (!($Obj instanceof staff_attend_group)/* get_class($Obj) != "staff_attend_group"*/) {
						$ProcessedUserData[$TargetDate][$Obj] = $Return;
						$ProcessedUserID[$TargetDate][$Obj] = "Processed";
					}
					else {
						$ProcessedGroupData[$TargetDate][$ObjID] = $Return;
						$ProcessedGroupID[$TargetDate][$ObjID] = "Processed";
					}
				}
				if($sys_custom['StaffAttendance']['LogGetDutyByDate']){
					$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.(isset($_SESSION['UserID'])?$_SESSION['UserID']:0).' '.(serialize($Return));
					$this->log($log_row);
				}
				//if ($TargetDate == "2010-10-21") 
				//	debug_r($Return);
				return $Return; 
			}
			// just freezing duty, no need to retrieve setting
			else {
				if ($CacheResult) {
					if (!($Obj instanceof staff_attend_group)/* get_class($Obj) != "staff_attend_group"*/) {
						$ProcessedUserID[$TargetDate][$Obj] = "Processed";
					}
					else {
						$ProcessedGroupID[$TargetDate][$ObjID] = "Processed";
					}
				}
				
				return "";
			}
		}
		else {
			if (!($Obj instanceof staff_attend_group) /*get_class($Obj) != "staff_attend_group"*/) 
				return $ProcessedUserData[$TargetDate][$Obj];
			else 
				return $ProcessedGroupData[$TargetDate][$ObjID];
		}
	}
	
	function Get_Daily_Log_Record($UserList,$Year,$Month,$Day,$UnAvaliableStaff,$StaffIdToRecords=0) {
		global $sys_custom;
		// Get Daily Log Records
		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
		$sql = 'select 
							log.RecordID, 
							log.StaffID,
							log.SlotName,
							log.DutyCount,
							log.Duty,
							log.DutyStart,
							TIME_TO_SEC(log.DutyStart) as DutyStartSec,
							log.DutyEnd,
							TIME_TO_SEC(log.DutyEnd) as DutyEndSec,
							log.InWavie,
							log.InTime,
							log.InSchoolStatus,
							log.OutWavie,
							log.OutTime,
							log.OutSchoolStatus, 
							log.InAttendanceRecordID,
							log.OutAttendanceRecordID,
							log.RecordType,
							ip.ReasonID as InReasonID, 
							op.ReasonID as OutReasonID, 
							ip.Waived as InWavied, 
							op.Waived as OutWavied, 
							log.MinLate,
							log.MinEarlyLeave, 
							log.InSchoolStation,
							log.OutSchoolStation, 
							r.ReasonText as InReason, 
							r1.ReasonText as OutReason,
							log.DateModified ';
		if($sys_custom['StaffAttendance']['WongFutNamCollege']){
			$sql .= ',log.AttendOne ';
		}
					$sql .=' From 
							'.$card_log_table_name.' as log 
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE2_PROFILE as ip 
							on log.InAttendanceRecordID = ip.RecordID 
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE3_REASON as r 
							on ip.ReasonID = r.ReasonID 
              LEFT JOIN
							CARD_STAFF_ATTENDANCE2_PROFILE as op
							on log.OutAttendanceRecordID = op.RecordID
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE3_REASON as r1 
							on op.ReasonID = r1.ReasonID 
						where 
							log.DayNumber = \''.$Day.'\' 
							and 
							log.StaffId in ('.$UserList.') ';
		if (sizeof($UnAvaliableStaff) > 0) {
			$sql .= 'and 
							log.StaffID not in ('.implode(',',$UnAvaliableStaff).') ';
		}
		$sql .= '
						Order by 
							DutyStart, SlotName 
					 ';
		$CardLogDetail = $this->returnArray($sql);
		if($StaffIdToRecords)
		{
			$staffIdToRecordAry = array();
			$record_size = count($CardLogDetail);
			for($i=0;$i<$record_size;$i++){
				if(!isset($staffIdToRecordAry[$CardLogDetail[$i]['StaffID']])){
					$staffIdToRecordAry[$CardLogDetail[$i]['StaffID']] = array();
				}
				$staffIdToRecordAry[$CardLogDetail[$i]['StaffID']][] = $CardLogDetail[$i];
			}
			return $staffIdToRecordAry;
		}
		//if ($Year == "2010" && $Month == 8 && $Day == 18)
		//debug_r($sql);
		return $CardLogDetail;
	}
	
	function Get_Staff_With_Out_Daily_Log($UserList,$Year,$Month,$Day) {
		// Get Daily Log Records
		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
		
		$sql = 'select 
							StaffID 
						from 
							'.$card_log_table_name.' 
						where 
							DayNumber = \''.$Day.'\' 
							and 
							StaffID in ('.$UserList.') 
						group by 
							StaffID';
		//debug_r($sql);
		$UserFound = $this->returnVector($sql);
		
		$UserList = explode(',',$UserList);
		
		return array_values(array_diff($UserList,$UserFound));
	}
	
	function Get_Libcycleperiods()
	{
		global $intranet_root;
		
		if(!$this->libcycleperiods)
		{
			include_once($intranet_root."/includes/libcycleperiods.php");
			$this->libcycleperiods = new libcycleperiods();
		}
		return $this->libcycleperiods;
	}
	
	function Get_Cycle_Periods_Within_Date_Range($start_date, $end_date)
	{
		/*
		 * PeriodType : 0=non-cycleday, 1=cycleday, 2=imported cycleday
		 * CycleType : 0=numeric, 1=alphabetic, 2=roman
		 * PeriodDays : number of days in one cycle
		 */
		//$sql = "SELECT * FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE PeriodType!='0' AND ('$start_date' BETWEEN PeriodStart AND PeriodEnd) AND ('$end_date' BETWEEN PeriodStart AND PeriodEnd) ORDER BY PeriodStart";
		// find all cycle periods that intersect the date range
		$sql = "SELECT * FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE PeriodType!='0' 
				AND (('$start_date' BETWEEN PeriodStart AND PeriodEnd) OR ('$end_date' BETWEEN PeriodStart AND PeriodEnd) OR (PeriodStart BETWEEN '$start_date' AND '$end_date') OR (PeriodEnd BETWEEN '$start_date' AND '$end_date')) 
				ORDER BY PeriodStart";
		$records = $this->returnArray($sql);
		$record_count = count($records);
		if($record_count == 0){
			return array();
		}
		$largest_period_day = 0;
		$return_record_index = 0;
		// get the longest cycle day period 
		for($i=0;$i<$record_count;$i++){
			$period_day = $records[$i]['PeriodDays'];
			if($period_day > $largest_period_day){
				$largest_period_day = $period_day;
				$return_record_index = $i;
			}
		}
		
		return array($records[$return_record_index]);
	}
	
	function Get_Cycle_Day_By_Date($target_date)
	{
		$sql = "SELECT CycleDay FROM INTRANET_CYCLE_DAYS WHERE RecordDate='$target_date'";
		$record = $this->returnVector($sql);
		return $record[0];
	}
	
	function Remove_EarlyLeave_Records_Before_LastTimeSlot($staff_id, $target_date, $before_duty_time)
	{
		$ts = strtotime($target_date);
		$year = date("Y", $ts);
		$month = date("m", $ts);
		$day = date("d",$ts);
		$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$year."_".$month;
		
		$sql = "SELECT RecordID, InSchoolStatus, InAttendanceRecordID, OutAttendanceRecordID FROM $card_log_table_name WHERE StaffID='$staff_id' AND DayNumber='$day' AND DutyStart IS NOT NULL AND DutyStart < '$before_duty_time' AND OutSchoolStatus IS NOT NULL AND OutSchoolStatus='".CARD_STATUS_EARLYLEAVE."'";
		$records = $this->returnResultSet($sql);
		$record_count = count($records);
		
		$result = array();
		for($i=0;$i<$record_count;$i++){
			$record_id = $records[$i]['RecordID'];
			$in_status = $records[$i]['InSchoolStatus'];
			$in_profile_id = $records[$i]['InAttendanceRecordID'];
			$el_profile_id = $records[$i]['OutAttendanceRecordID'];
			
			if($el_profile_id > 0){
				$this->Remove_Profile($el_profile_id);
			}
			if($in_status == CARD_STATUS_ABSENT && $in_profile_id > 0){
				$this->Remove_Profile($in_profile_id);
			}
			$update_in_status = $in_status == '' || $in_status == CARD_STATUS_ABSENT? ",InSchoolStatus='".CARD_STATUS_NORMAL."',InAttendanceRecordID=NULL " : "";
			
			$sql = "UPDATE $card_log_table_name SET OutSchoolStatus='".CARD_STATUS_NORMAL."',OutWaived=NULL,OutAttendanceRecordID=NULL,MinEarlyLeave=NULL $update_in_status WHERE RecordID='$record_id'";
			$result['RemoveELRecord_'.$record_id] = $this->db_db_query($sql);
		}
		
		return !in_array(false, $result);
	}
	
	/*
		$Duty is the array from Get_Duty_By_Date()
		$TimeSlotArray is today's time slot details
		$ThisTimeSlotName is the outing or holiday time slot that the time falls in 
		$CurrentTime is timestamp
	*/
	function processOnDutyTimeSlotAfterOutingOrHoliday($StaffID, $Duty, $TimeSlotArray, $ThisTimeSlotName, $CurrentTime, $SiteName)
	{
		//find the first on duty time slot after $Duty["CardSlotToDuty"][$ThisTimeSlotName]
		if(count($Duty["CardSlotToDuty"]) == 0 || !isset($Duty["CardSlotToDuty"][$ThisTimeSlotName])) return false;
		
		$Month = date('m',$CurrentTime);
	    $Year = date('Y',$CurrentTime);
		$Today = date('Y-m-d',$CurrentTime);
		$time_string = date('H:i:s',$CurrentTime);
		
		$TodayDailyLogTable = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
		
		$this_duty_start = $Duty["CardSlotToDuty"][$ThisTimeSlotName][0]["DutyStart"];
		$found_time_slot = array();
		foreach($Duty["CardSlotToDuty"] as $time_slot_name => $ary)
		{
			if($time_slot_name == $ThisTimeSlotName) continue;
			if($ary[0]["Duty"]=="1" && $ary[0]["SlotName"]!='' && $ary[0]["RecordType"]!=CARD_STATUS_OUTGOING && $ary[0]["RecordType"]!=CARD_STATUS_HOLIDAY && $ary[0]["DutyStart"] > $this_duty_start )
			{
				if($ary[0]["InTime"]=="" && ($ary[0]["InSchoolStatus"]=="" || $ary[0]["InSchoolStatus"]==CARD_STATUS_ABSENT) && $ary[0]["OutTime"]=="" && $ary[0]["OutSchoolStatus"]=="")
				{
					// found !!!
					$found_time_slot = $ary;
				}
				// exit anyway if find a on duty time slot 
				break;
			}
		}
		if(count($found_time_slot)==0) return false;
		
		$Result = array();
		// set on time for the first on duty time slot after the outing/holiday time slot
		$sql = 'update '.$TodayDailyLogTable.' set 
							InSchoolStatus = \''.CARD_STATUS_NORMAL.'\', 
							InSchoolStation = \''.$this->Get_Safe_Sql_Query($SiteName).'\', 
							InTime = \''.$time_string.'\', 
							MinLate = \'\',
							InAttendanceRecordID = \'\',';
		if ($found_time_slot[0]['OutWavie'] == "1") {
			$sql .= 'OutSchoolStatus = \''.CARD_STATUS_NORMAL.'\',
					 MinEarlyLeave = \'\',';
		}
		$sql .= '
							DateModified = NOW(), 
							ModifyBy = \''.$StaffID.'\'  
						Where 
							RecordID = \''.$found_time_slot[0]['RecordID'].'\'
					 ';
		$Result['FirstNormalAfterOutingHoliday'] = $this->db_db_query($sql);
		//debug_r($sql);
		// traverse and update status through today time slot
		if ($found_time_slot[0]['OutWavie'] == "1") {
			$Result['TraverseWaviedTimeSlot'] = $this->Traverse_Wavied_Time_Slot($StaffID,$TimeSlotArray,$Duty,$found_time_slot[0]['SlotName'],CARD_STATUS_NORMAL,$Today);
		}
		
		if(!in_array(false, $Result)){
			return array($found_time_slot[0]['SlotName'],CARD_RESP_STAFF_IN_SCHOOL_ONTIME);
		}else{
			return false;
		}
	}
	
	/*
	 * check if a staff has left before current time slot
	 */
	function isLeftBeforeCurrentTimeSlot($StaffID, $Duty, $ThisTimeSlotName)
	{
		if(count($Duty["CardSlotToDuty"]) == 0 || !isset($Duty["CardSlotToDuty"][$ThisTimeSlotName])) return false;
		
		$this_duty_start = $Duty["CardSlotToDuty"][$ThisTimeSlotName][0]["DutyStart"];
		$out_status_before_current_time_slot = '';
		foreach($Duty["CardSlotToDuty"] as $time_slot_name => $ary)
		{
			if($time_slot_name == $ThisTimeSlotName) continue;
			if($ary[0]["Duty"]=="1" && $ary[0]["SlotName"]!='' && $ary[0]["RecordType"]!=CARD_STATUS_OUTGOING && $ary[0]["RecordType"]!=CARD_STATUS_HOLIDAY && $ary[0]["DutyStart"] < $this_duty_start && $ary[0]["OutSchoolStatus"]!="")
			{
				$out_status_before_current_time_slot = $ary[0]["OutSchoolStatus"];
			}
		}
		
		// if out status has been set, it would be either normal leave or early leave
		return $out_status_before_current_time_slot!='';
	}
	
	/*
	 * check if time slots before current time slot has recorded tap card time, i.e. staff at least has come back or left 
	 */
	function hasPreviousTimeSlotsRecordedTime($StaffID, $Duty, $ThisTimeSlotName)
	{
		if(count($Duty["CardSlotToDuty"]) == 0 || !isset($Duty["CardSlotToDuty"][$ThisTimeSlotName])) return false;
		
		$this_duty_start = $Duty["CardSlotToDuty"][$ThisTimeSlotName][0]["DutyStart"];
		$in_time = '';
		$out_time = '';
		
		foreach($Duty["CardSlotToDuty"] as $time_slot_name => $ary)
		{
			if($time_slot_name == $ThisTimeSlotName || ($ary[0]["DutyStart"]!='' && $ary[0]["DutyStart"] >= $this_duty_start)) continue;
			if($ary[0]["Duty"]=="1" && $ary[0]["SlotName"]!='' && $ary[0]["RecordType"]!=CARD_STATUS_OUTGOING && $ary[0]["RecordType"]!=CARD_STATUS_HOLIDAY && $ary[0]["DutyStart"] < $this_duty_start)
			{
				if($ary[0]['InTime'] != ''){
					$in_time = $ary[0]['InTime'];
				}
				if($ary[0]['OutTime'] != ''){
					$out_time = $ary[0]['OutTime'];
				}
			}
		}
		
		return $in_time != '' || $out_time != '';
	}
	
	// $sys_custom['StaffAttendance']['WongFutNamCollege']
	function clearOtherDutyRecordsForCustomization($StaffID, $RecordDate)
	{
		$ts = strtotime($RecordDate);
		$year = date("Y",$ts);
		$month = date("m",$ts);
		$day = date("d",$ts);
		
		$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$year."_".$month;
		
		$sql = "SELECT * FROM ".$card_log_table_name." WHERE DayNumber='$day' AND StaffID='$StaffID' AND SlotName IS NOT NULL AND Duty='1' AND AttendOne='1' ORDER BY DutyStart";
		$records = $this->returnResultSet($sql);
		$record_size = count($records);
		
		$recordToRemove = array();
		for($i=0;$i<$record_size;$i++){
			if($records[$i]['InTime'] == '' && $records[$i]['OutTime'] == ''){ // no in/out time implies that this attendance record is not touched, tap card must fall into other time slots
				$recordToRemove[] = $records[$i];
			}
		}
		
		$resultAry = array();
		$remove_count = count($recordToRemove);
		// prevent clear all attendance time slots for the staff, must at least have one attendance record left there
		if($remove_count < $record_size && $remove_count>0){
			for($i=0;$i<$remove_count;$i++){
				$record_id = $recordToRemove[$i]['RecordID'];
				$in_profile_id = trim($recordToRemove[$i]['InAttendanceRecordID']);
				$out_profile_id = trim($recordToRemove[$i]['OutAttendanceRecordID']);
				
				if($in_profile_id != ''){
					$resultAry['DeleteInProfile_'.$in_profile_id] = $this->Remove_Profile($in_profile_id);
				}
				if($out_profile_id != ''){
					$resultAry['DeleteOutProfile_'.$out_profile_id] = $this->Remove_Profile($out_profile_id);
				}
				$sql = "DELETE FROM $card_log_table_name WHERE RecordID='$record_id'";
				$resultAry['DeleteDailylog_'.$record_id] = $this->db_db_query($sql);
			}
		}
		
		return !in_array(false, $resultAry);
	}
	
	function cleanFullDayRecords($year, $month)
	{
		$card_log_table_name = sprintf("CARD_STAFF_ATTENDANCE2_DAILY_LOG_%04d_%02d",$year,$month);
		
		$sql = "SELECT StaffID,DayNumber,RecordType,GROUP_CONCAT(RecordID) as RecordIdCsv,GROUP_CONCAT(InAttendanceRecordID) as ProfileIdCsv 
				FROM $card_log_table_name WHERE RecordType IN (".CARD_STATUS_OUTGOING.",".CARD_STATUS_HOLIDAY.") AND SlotName IS NULL GROUP BY StaffID,DayNumber,RecordType HAVING COUNT(RecordID)>1";
		$records = $this->returnResultSet($sql);
		$record_size = count($records);
		
		$to_delete_record_id_ary = array();
		$to_delete_profile_id_ary = array();
		
		for($i=0;$i<$record_size;$i++){
			$record_id_ary = explode(",",$records[$i]['RecordIdCsv']);
			$profile_id_ary = explode(",",$records[$i]['ProfileIdCsv']);
			
			if(count($record_id_ary)>1){
				array_shift($record_id_ary);
				$to_delete_record_id_ary = array_merge($to_delete_record_id_ary,$record_id_ary);
			}
			
			if(count($profile_id_ary)>1){
				array_shift($profile_id_ary);
				$to_delete_profile_id_ary = array_merge($to_delete_profile_id_ary,$profile_id_ary);
			}
		}
		
		if(count($to_delete_record_id_ary)>0){
			$to_delete_record_id_ary = array_values($to_delete_record_id_ary);
			$sql = "DELETE FROM $card_log_table_name WHERE RecordID IN (".implode(",",$to_delete_record_id_ary).")";
			$this->db_db_query($sql);
		}
		
		if(count($to_delete_profile_id_ary)>0){
			$to_delete_profile_id_ary = array_values($to_delete_profile_id_ary);
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE WHERE RecordID IN (".implode(",",$to_delete_profile_id_ary).")";
			$this->db_db_query($sql);
		}
	}
	/*
	function getSpecialDutyRecords($params)
	{
		$conds = "";
		if(isset($params['StartDate']) && $params['StartDate']!=''){
			$conds .= " AND DutyDate >= '".$params['StartDate']."' ";
		}
		if(isset($params['EndDate']) && $params['EndDate']!=''){
			$conds .= " AND DutyDate <= '".$params['EndDate']."' ";
		}
		if(isset($params['DutyDate']) && $params['DutyDate']!=''){
			$conds .= " AND DutyDate = '".$params['DutyDate']."' ";
		}
		if(isset($params['UserID'])){
			$conds .= " AND UserID ";
			if(is_array($params['UserID'])){
				$conds .= " IN (".implode(",",IntegerSafe($params['UserID'])).") ";
			}else{
				$conds .= " = '".IntegerSafe($params['UserID'])."' ";
			}
		}
		
		$sql = "SELECT * FROM CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY WHERE 1 ".$conds." ORDER BY DutyDate";
		$records = $this->returnResultSet($sql);
		$record_size = count($records);
		//debug_pr($sql);
		$ary = array();
		if(isset($params['DateToRecords'])){
			for($i=0;$i<$record_size;$i++){
				if(!isset($ary[$records[$i]['DutyDate']])){
					$ary[$records[$i]['DutyDate']] = array();
				}
				$ary[$records[$i]['DutyDate']][] = $records[$i];
			}
			return $ary;
		}
		if(isset($params['DateUserIDToRecords'])){
			for($i=0;$i<$record_size;$i++){
				if(!isset($ary[$records[$i]['DutyDate']])){
					$ary[$records[$i]['DutyDate']] = array();
				}
				if(!isset($ary[$records[$i]['DutyDate']][$records[$i]['UserID']])){
					$ary[$records[$i]['DutyDate']][$records[$i]['UserID']] = array();
				}
				$ary[$records[$i]['DutyDate']][$records[$i]['UserID']][] = $records[$i];
			}
			return $ary;
		}
		
		return $records;
	}
	*/
}
?>