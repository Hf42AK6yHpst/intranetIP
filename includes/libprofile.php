<?php
# using:  
/**
 * 2020-08-28 Bill  [2020-0828-0957-37073]
 * - modified Print_Student_photo(), to ensure school name and year at the top of printing page
 *
 * 2020-06-17 Ray
 * - modified GET_MODULE_OBJ_ARR add $sys_custom['HideIndividualTimetable'] & check attendancelesson
 *
 * 2019-08-08 Bill  [2019-0806-1405-49206]
 * - modified GET_MODULE_OBJ_ARR(), fixed non-admin cannot access "Student Profile > Student Performance"
 *
 * 2018-11-12 Cameron
 * - consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
 * 
 * 2017-12-22 Cameron
 * - Use the same menu like Amway for Oaks by modifying GET_MODULE_OBJ_ARR()
 *  
 * Change Log:
 * 2017-12-01 Isaac
 * - added Print_Student_photo()
 * 
 * 2017-06-07 Pun
 * - modified GET_MODULE_OBJ_ARR(), $sys_custom['iAccount']['editPasswordOnly'] - show edit password only.
 * 
 * 2017-04-24 Carlos 
 * - modified GET_MODULE_OBJ_ARR(), $sys_custom['DHL'] - hide unnecessary menus for DHL.
 * 
 * 2016-09-23 Ivan [ip.2.5.7.10.1] [M104724]
 * - modified GET_MODULE_OBJ_ARR() added $sys_custom['StudentProfile']['hideParentiAccount'] to hide student profile in iaccount for parent account
 * 
 * 2016-05-09 Carlos [ip2.5.7.7.1]
 * - modified GET_MODULE_OBJ_ARR(), show menus under [Student Profile] for student and parent users. 
 * 
 * 20151211 Cameron
 * - modify GET_MODULE_OBJ_ARR() to hide all other menu except "Personal Info" and "Login Password" 
 * - add link to portal if user do not change password / after change password
 * 
 * 20151207 Ivan [ip.2.5.7.1.1.0]
 * - added Get_StudentProformance_Top_Tabs_Menu()
 * 
 * 20150930 Ivan [ip.2.5.6.10.1.0]
 * - modified GET_MODULE_OBJ_ARR() to add HKEdCity menu
 * 
 * 20150423	Omas 
 * -Add $MenuArr["StudentProfile"]["Child"]["PaymentAccountOverView"]
 * 
 * 20110824 YatWoon
 * -add checking for Alumni
 *
 * 20101215 Ivan
 * - added function Get_Timetable_Top_Tabs_Menu()
 */

if (!defined("LIBPROFILE_DEFINED"))                     // Preprocessor directive
{
define("LIBPROFILE_DEFINED", true);

class libprofile extends libdb
{
	function libprofile()
	{
		
	}

    /*
    * Get MODULE_OBJ array
    */
    function GET_MODULE_OBJ_ARR()
    {
        global $plugin, $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr;
        global $mapping_file_path, $UserID, $Lang, $sys_custom, $FromLogin;
        
        ### wordings
        global $i_UserProfilePersonal, $i_UserProfileContact, $i_UserProfileMessage, $i_UserProfileLogin, $i_MyAccount_Usage, $i_myac_title, $iAccount_Account, $iAccount;
        global $i_adminmenu_adm_academic_record, $i_Profile_Attendance, $i_Profile_Merit, $i_Profile_Activity, $i_Profile_Service, $i_Profile_Award, $i_MyAccount_Library, $i_sls_library_search, $i_sls_library_info, $i_Payment_DetailTransactionLog, $i_Payment_Menu_PrintPage_Outstanding;
        
        //$CurrentPageArr['eOffice'] = 1;
        
        # Current Page Information init
        $PageAccount 		= 0;
        $PagePersonalInfo 	= 0;
        $PageContactInfo 	= 0;
        $PageMessage 		= 0;
        $PageLoginPassword 	= 0;
        $PageLoginRecord 	= 0;
        
        $PageStudentProfile	= 0;
        $PageAttendanceRecord = 0;
	    $PageMeritDemerit 	= 0;
        $PageActivity		= 0;
        $PageService		= 0;
        $PageAward		    = 0;
        $PageTransactionLog = 0;
        $PageTransactionOutstanding = 0;
        
        $PageLibraryRecord	= 0;
        $PageTimetable 	= 0;
        
        $PageStudentPerformanceView = 0;
        $PageStudentOfficialPhotoView = 0;
        
        switch ($CurrentPage) {
        	case "PagePersonalInfo":
            	$PageAccount		= 1; 
                $PagePersonalInfo 	= 1;
            	break;
            case "PageContactInfo":
            	$PageAccount		= 1;
            	$PageContactInfo 	= 1;
            	break;
            case "PageMessage":
            	$PageAccount		= 1;
            	$PageMessage 		= 1;
            	break;
            case "PageLoginPassword":
            	$PageAccount		= 1;
            	$PageLoginPassword 	= 1;
            	break;
            case "PageHKEdCityLogin":
            	$PageAccount		= 1;
            	$PageHKEdCityLogin 	= 1;
            	break;
            case "PageLoginRecord":
            	$PageLoginRecord 	= 1;
            	break;
                
            case "PageStudentProfile":
               	$PageStudentProfile = 1;
			break;
	                
			case "PageAttendanceRecord":
				$PageStudentProfile = 1;
				$PageAttendanceRecord = 1;
			break;
	                                
			case "PageMeritDemerit":
				$PageStudentProfile = 1;
				$PageMeritDemerit = 1;
			break;
	                                
			case "PageActivity":
				$PageStudentProfile = 1;
				$PageActivity = 1;
			break;
	                                
			case "PageService":
				$PageStudentProfile = 1;
				$PageService = 1;
			break;
				
			case "PageAward":
				$PageStudentProfile = 1;
				$PageAward = 1;
			break;	
			
			case "PageTransactionLog":
				$PageStudentProfile = 1;
				$PageTransactionLog = 1;
			break;	
			
			case "PageTransactionOutstanding":
				$PageStudentProfile = 1;
				$PageTransactionOutstanding = 1;
			break;
			case "PageAccountOverView":
				$PageStudentProfile = 1;
				$PageAccountOverView = 1;
			break;	
			case "PageEnrolmentAttendanceView":
				$PageStudentProfile = 1;
				$PageEnrolmentAttendanceView = 1;
			break;
			//Isaac added
			case "PageStudentOfficialPhotoView":
			    $PageStudentProfile = 1;
			    $PageStudentOfficialPhotoView = 1;
			    break;
			case "PageStudentPerformanceView":
			    $PageStudentProfile = 1;
			    $PageStudentPerformanceView = 1;
			    break;
			case "PageTimetable":
				$PageTimetable = 1;
			break;
			//Henry Added
			case "PageLessonAttendance":
				//$PageTimetable = 1;
				$PageLessonAttendance = 1;
			break;
        }
        
        include_once("libhkedcity.php");
		$lhkedcity = new libhkedcity();

        # Menu information
        $MenuArr["Account"] = array($iAccount_Account, "", $PageAccount);
		if ($sys_custom['iAccount']['editPasswordOnly']) 
		{
			$MenuArr["Account"]["Child"]["LoginPassword"] = array($i_UserProfileLogin, $PATH_WRT_ROOT."home/iaccount/account/login_password.php", $PageLoginPassword);
		}
		else if ($sys_custom['project']['CourseTraining']['IsEnable'])
		{
            $MenuArr["Account"]["Child"]["PersonalInfo"] = array($i_UserProfilePersonal, $PATH_WRT_ROOT."home/iaccount/account/", $PagePersonalInfo);
			$MenuArr["Account"]["Child"]["LoginPassword"] = array($i_UserProfileLogin, $PATH_WRT_ROOT."home/iaccount/account/login_password.php", $PageLoginPassword);
			if ($FromLogin) {
				$MenuArr["Account"]["Child"]["Portal"] = array($Lang['iAccount']['Portal'], $PATH_WRT_ROOT."home/portal.php", '');	
			}
		} 
		else 
		{
            $MenuArr["Account"]["Child"]["PersonalInfo"] = array($i_UserProfilePersonal, $PATH_WRT_ROOT."home/iaccount/account/", $PagePersonalInfo);
	        $MenuArr["Account"]["Child"]["ContactInfo"] = array($i_UserProfileContact, $PATH_WRT_ROOT."home/iaccount/account/contact_info.php", $PageContactInfo);
	        if(!$sys_custom['DHL']) $MenuArr["Account"]["Child"]["Message"] = array($i_UserProfileMessage, $PATH_WRT_ROOT."home/iaccount/account/message.php", $PageMessage);
	        $MenuArr["Account"]["Child"]["LoginPassword"] = array($i_UserProfileLogin, $PATH_WRT_ROOT."home/iaccount/account/login_password.php", $PageLoginPassword);
	        if ($lhkedcity->enabledSingleSignOn() && !$sys_custom['DHL']) {
	        	$MenuArr["Account"]["Child"]["HKEdCityLogin"] = array($Lang['HKEdCity']['HKEdCityLogin'], $PATH_WRT_ROOT."home/iaccount/account/hkedcity.php", $PageHKEdCityLogin);
	        }
	        
	        
	        $MenuArr["LoginRecord"] = array($i_MyAccount_Usage, $PATH_WRT_ROOT."home/iaccount/login_record/usage.php", $PageLoginRecord);
	        
	        if($_SESSION['UserType']!=USERTYPE_ALUMNI && !$sys_custom['DHL'])
	        {
		        ###########################
				### Student Profile 
				###########################
				$lu = new libuser($UserID);
				$hasStudentProfile = isset($plugin['studentprofile']) && $plugin['studentprofile'];
				
				if($hasStudentProfile && $lu->isTeacherStaff())
				{
					include_once("libteaching.php");
				    $li_teaching = new libteaching();
			    	
//			    	$temp_class= $li_teaching->returnTeacherClass($UserID);
//			    	$classname = $temp_class[0][1];
		    	
			    	$isClassTeacher = $li_teaching->Is_Class_Teacher($_SESSION['UserID']);
			    	$isSubjectTeacher = $li_teaching->Is_Subject_Teacher($_SESSION['UserID']);
			    	$isTeacherAppAdmin = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"];
			    		
			    	$hasStudentProfile = ($isClassTeacher || $isSubjectTeacher || $isTeacherAppAdmin)!=""? true : false;
				}
				if ($sys_custom['StudentProfile']['hideParentiAccount'] && $_SESSION['UserType']==USERTYPE_PARENT) {
					$hasStudentProfile = false;
				}
				
			    if($hasStudentProfile)
			    {     
				    include_once("libstudentprofile.php");
				    $lstudentprofile = new libstudentprofile();
				    
				    if(!$lstudentprofile->is_frontend_attendance_hidden || !$lstudentprofile->is_frontend_merit_hidden || !$lstudentprofile->is_frontend_activity_hidden || !$lstudentprofile->is_frontend_service_hidden || !$lstudentprofile->is_frontend_award_hidden || $lu->RecordType==1)
				    {
				        if ($isClassTeacher /* || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"] */ || in_array($_SESSION['UserType'],array(USERTYPE_STUDENT,USERTYPE_PARENT)))
				        {
				            //if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"]) {
				                $MenuArr["StudentProfile"] 					= array($i_adminmenu_adm_academic_record, "", $PageStudentProfile);
				                
				                if(!$lstudentprofile->is_frontend_attendance_hidden)
				                    $MenuArr["StudentProfile"]["Child"]["AttendanceRecord"] 	= array($i_Profile_Attendance, $PATH_WRT_ROOT."home/iaccount/student_profile/attendance_record.php", $PageAttendanceRecord);
				                if(!$lstudentprofile->is_frontend_merit_hidden)
				                    $MenuArr["StudentProfile"]["Child"]["MeritDemerit"] 		= array($i_Profile_Merit, $PATH_WRT_ROOT."home/iaccount/student_profile/merit_demerit.php", $PageMeritDemerit);
				                if(!$lstudentprofile->is_frontend_activity_hidden)
		                            $MenuArr["StudentProfile"]["Child"]["Activity"] 		= array($i_Profile_Activity, $PATH_WRT_ROOT."home/iaccount/student_profile/activity.php", $PageActivity);
				                if(!$lstudentprofile->is_frontend_service_hidden)
				                    $MenuArr["StudentProfile"]["Child"]["Service"] 			= array($i_Profile_Service, $PATH_WRT_ROOT."home/iaccount/student_profile/service.php", $PageService);
				                if(!$lstudentprofile->is_frontend_award_hidden)
				                    $MenuArr["StudentProfile"]["Child"]["Award"] 			= array($i_Profile_Award, $PATH_WRT_ROOT."home/iaccount/student_profile/award.php", $PageAward);
				                                    
                                if($lu->RecordType == USERTYPE_STAFF && $plugin['payment'])
                                {
                                    $MenuArr["StudentProfile"]["Child"]["PaymentTransactionLog"]	= array($i_Payment_DetailTransactionLog, $PATH_WRT_ROOT."home/iaccount/student_profile/payment_transaction_log.php", $PageTransactionLog);
                                    $MenuArr["StudentProfile"]["Child"]["PaymentTransactionOutstanding"]	= array($i_Payment_Menu_PrintPage_Outstanding, $PATH_WRT_ROOT."home/iaccount/student_profile/payment_outstanding.php", $PageTransactionOutstanding);
                                    $MenuArr["StudentProfile"]["Child"]["PaymentAccountOverView"]	= array($Lang['iAccount']['AccountOverview'], $PATH_WRT_ROOT."home/iaccount/student_profile/payment_info.php", $PageAccountOverView);
                                }
                                if($lu->RecordType == USERTYPE_STAFF && $plugin['eEnrollment']){
                                    $MenuArr["StudentProfile"]["Child"]["EnrolmentAttendanceView"] = array($Lang['iAccount']['EnrolmentAttendanceOverview'],$PATH_WRT_ROOT."home/iaccount/student_profile/activity_attendance.php", $PageEnrolmentAttendanceView);
                                }
				            //}
					    	/*
				            if($lu->RecordType == USERTYPE_STAFF && $plugin['eClassTeacherApp']) {
							     if(!isset($MenuArr["StudentProfile"])){
								    $MenuArr["StudentProfile"] 					= array($i_adminmenu_adm_academic_record, "", $PageStudentProfile);
							     }
							     $MenuArr["StudentProfile"]["Child"]["StudentPerformance"] = array($Lang['iAccount']['StudentPerformance'],$PATH_WRT_ROOT."home/iaccount/student_profile/studentPerformance/index.php", $PageStudentPerformanceView); 
							
							     if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"]) {
							         $MenuArr["StudentProfile"]["Child"]["StudentOfficialPhoto"] = array($Lang['iAccount']['StudentOfficialPhoto'],$PATH_WRT_ROOT."home/iaccount/student_profile/studentOfficialPhoto/index.php", $PageStudentOfficialPhotoView);
							     }
						    }
						    */
					    }

                        // if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"] && $lu->RecordType == USERTYPE_STAFF && $plugin['eClassTeacherApp']){
                        if($plugin['eClassTeacherApp'] && $lu->RecordType == USERTYPE_STAFF)
					    {
					    	if(!isset($MenuArr["StudentProfile"])){
							    $MenuArr["StudentProfile"] = array($i_adminmenu_adm_academic_record, "", $PageStudentProfile);
						     }
						     $MenuArr["StudentProfile"]["Child"]["StudentPerformance"] = array($Lang['iAccount']['StudentPerformance'],$PATH_WRT_ROOT."home/iaccount/student_profile/studentPerformance/index.php", $PageStudentPerformanceView);

						     //if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"]) {
						     //    $MenuArr["StudentProfile"]["Child"]["StudentOfficialPhoto"] = array($Lang['iAccount']['StudentOfficialPhoto'],$PATH_WRT_ROOT."home/iaccount/student_profile/studentOfficialPhoto/index.php", $PageStudentOfficialPhotoView);
						     //}
					    }
					}
				}

				###########################
		        ### Library Record
		        ###########################
		        if (isset($plugin['library']) && $plugin['library'])
		        {
					$lu 	= new libuser($UserID);                
		
		            $content = get_file_content("$mapping_file_path");
		            
		            $records = explode("\n",$content);
		            $login = $lu->UserLogin;
		            for ($i=0; $i<sizeof($records); $i++)
		            {
		                 $data = explode("::",$records[$i]);
		                 if (strtolower($login) == strtolower($data[0]))
		                 {
		                     $MenuArr["LibraryRecord"] = array($i_MyAccount_Library, "", $PageLibraryRecord);
		                     $MenuArr["LibraryRecord"]["Child"]["Search"] 	= array($i_sls_library_search, "javascript:newWindow('". $PATH_WRT_ROOT ."home/plugin/sls/library.php',8);");
		                     $MenuArr["LibraryRecord"]["Child"]["Info"] 	= array($i_sls_library_info, "javascript:newWindow('". $PATH_WRT_ROOT ."home/plugin/sls/library_info.php',8);");
		                 }
		            }
		        }
		        
		        $disableTimeTable = false;
		        if ($plugin['DisableTimeTable']) {
		        	$disableTimeTable = true;
		        } else {
		        	// required hidden Timetable for Living Homeopathy's student account
		        	if ($sys_custom['LivingHomeopathy'] && $_SESSION["UserType"] == USERTYPE_STUDENT) {
		        		$disableTimeTable = true;
		        	}
		        }
		        ### Timetable
		        if (!$disableTimeTable) {
		        	$MenuArr["Timetable"] = array($Lang['SysMgr']['Timetable']['Timetable'], $PATH_WRT_ROOT."home/iaccount/timetable/personal_view.php", $PageTimetable);
			        //Henry Added
					if(!($sys_custom['HideIndividualTimetable'] == true)) {
						$MenuArr["Timetable"]["Child"]["Timetable"] = array($Lang['iAccount']['Timetable']['IndividualTimetable'], $PATH_WRT_ROOT . "home/iaccount/timetable/personal_view.php", $PageTimetable);
					}

			        if ($lu->RecordType == 2 || $lu->RecordType == 3) {
			        	if($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson']) {
							$MenuArr["Timetable"]["Child"]["LessonAttendance"] = array($Lang['LessonAttendance']['DailyLessonStatus'], $PATH_WRT_ROOT . "home/iaccount/timetable/lesson_attendance.php", $PageLessonAttendance);
						}
					}

			        if(count($MenuArr["Timetable"]["Child"]) == 0) {
						unset($MenuArr["Timetable"]);
					}
		        }
	        }
    	}
    	
        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass iAccount";'."\n";
        $js.= '</script>'."\n";
    	
        # module information
        $MODULE_OBJ['title'] = $iAccount.$js;
        $MODULE_OBJ['title_css'] = "menu_opened";
        $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_studentprofile.gif";
        $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/iaccount/account/";
        $MODULE_OBJ['menu'] = $MenuArr;
		
        return $MODULE_OBJ;
    }
	
	function Get_Timetable_Top_Tabs_Menu($CurrentView)
	{
		global $PATH_WRT_ROOT, $Lang;
		
		if ($_SESSION['UserType'] == USERTYPE_STAFF)
		{
			include_once($PATH_WRT_ROOT."includes/libteaching.php");
			$libteaching = new libteaching();
			$isClassTeacher = $libteaching->Is_Class_Teacher($_SESSION['UserID']);
			//$isSubjectTeacher = $libteaching->Is_Subject_Teacher($_SESSION['UserID']);
		}
		
		$TabsArr = array();
		$TabsArr[] = array($Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Personal'], 'personal_view.php', ($CurrentView=='Personal')? 1 : 0);
		if ($isClassTeacher)
			$TabsArr[] = array($Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Class'], 'class_view.php', ($CurrentView=='Class')? 1 : 0);
		//if ($isSubjectTeacher)
		//	$TabsArr[] = array($Lang['SysMgr']['Timetable']['View']['ViewModeOption']['SubjectGroup'],'subject_group_view.php', ($CurrentView=='SubjectGroup')? 1 : 0);
			
		return $TabsArr;
	}
	
	function Print_Student_photo($thisClassID, $subjectGroupIdAry)
    {
        global $Lang;

	    $laccount = new libaccountmgmt();
	    $libfs = new libfilesystem();
	    $fcm = new form_class_manage();
	    $liuser = new libuser();
	    
	    if($thisClassID == "" && empty($subjectGroupIdAry))
	    {
	        $StudentInfoAry = $laccount->getStudentUserInfoListByClassID("", "");
 	        // debug_pr($StudentInfoAry);

	        # get the current year name
	        $currentAcademicYearArr = $fcm->Get_Current_Academic_Year_And_Year_Term();
	        $currentYearName = $currentAcademicYearArr[0]['YearNameEN'];
	        
	        # get school name
	        $schoolName = GET_SCHOOL_NAME();
	        
	        $BottomTitleArr = array($schoolName, $Lang['General']['StudentPhotos'], $currentYearName);
	        
	        $html .= "<table border=\"0\" cellpadding=\"4\" width=\"100%\" cellspacing=\"0\">
                        <tr>
                            <td colspan=\"8\" align=\"right\" class=\"print_hide\"><input type=\"button\" class=\" formsubbutton\" onClick=\"javascript:window.print();\" name=\"submit\" id=\"submit\" value=\"Print\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=\"8\" align=\"center\">".$BottomTitleArr[0] . "<br />" . $BottomTitleArr[1]. " " . $BottomTitleArr[2]."
                            </td>
                        </tr>
					";
	        $colcount = 0;
	        $colMax = 5;

// 	        foreach ($StudentInfoAry as $i => $studentInfo) {
// 	            $name[$i]  = $studentInfo['name'];
// 	        }
// 	        debug_pr($name);
// 	        array_multisort($name, SORT_ASC, $StudentInfoAry);
// 	        debug_pr($StudentInfoAry);
// 	        debug_pr($_SESSION);

	        foreach($StudentInfoAry as $i => $studentInfo)
	        {
	            // $webSamsNo = $studentInfo['WebSAMSRegNo'];
	            // $classNo = $studentInfo['ClassNumber'];
	            $studentName = Get_Lang_Selection($studentInfo['ChineseName'], $studentInfo['EnglishName']);
	            $userLogin = $studentInfo['UserLogin'];
	            // $className = $studentInfo['ClassTitleEN'];
	            $gender = $studentInfo['Gender'];
	            $id = $studentInfo['UserID'];
	            $studentPhoto = $liuser->GET_OFFICIAL_PHOTO_BY_USER_ID($id);
	            // if($_SESSION['UserID'] == '1'){
	            //      debug_pr($studentName);
	            // 		debug_pr($webSamsNo);
	            // 		debug_pr($studentPhoto);
	            // }
	            
	            if(file_exists($studentPhoto[0])){
	                $image = "<img src=\"".$studentPhoto[2]."\" border=0 width=\"100px\" height=\"130px\" />";
	            }else{
	                $image = "<img src=\"".$studentPhoto[0]."\" border=0 width=\"100px\" height=\"130px\" />";
	            }
	            
	            $imageFrame = "<table border=\"0\" cellpadding='0' cellspacing='1' width=\"100px\" height=\"130px\" bgcolor=\"#000\">";
	            $imageFrame .= "<tr bgcolor=\"#FFFFFF\"><td align=\"center\">".$image."</td></tr>";
	            $imageFrame .= "</table>";

	            # student info
	            // $studentInfo = $className." (" .$classNo. ") ". $studentName . "<br />" . $chineseName . " [" . $gender . "]" . "<br />" . $studentName;
	            $studentInfo = $studentName . " [" . $gender . "]<br>".$userLogin;

	            # display Image
                $colcount = ($colcount == $colMax)? 0:$colcount;
	            $html .= ($colcount == 0)?"<tr>":"";
	            $html .= "<td valign=\"top\" align=\"center\" width='20%'>".$imageFrame." ".$studentInfo."</td>";
	            $html .= ($colcount == ($colMax-1))?"</tr>\n":"";
	            
	            $colcount++;
	        }

	        $html .= "</table>";
	    }
	    else if($thisClassID != "" && empty($subjectGroupIdAry))
	    {
	        $StudentInfoAry = $laccount->printStudentListWithPhoto($thisClassID);

	        # get the class name
	        $className = $StudentInfoAry[0]['ClassTitleEN'];
	        
	        # get the current year name
	        $currentAcademicYearArr = $fcm->Get_Current_Academic_Year_And_Year_Term();
	        $currentYearName = $currentAcademicYearArr[0]['YearNameEN'];
	        
	        # get school name
	        $schoolName = GET_SCHOOL_NAME();
	        
	        $BottomTitleArr = array($schoolName, $Lang['General']['StudentPhotos'], $currentYearName);
	        
	        # get class teacher
	        $NameField = getNameFieldByLang('u.');
	        $ArchiveNameField = getNameFieldByLang2('au.');
	        
	        $sql = 'SELECT
                        yct.UserID,
                        CASE
                            WHEN au.UserID IS NOT NULL THEN CONCAT('.$ArchiveNameField.')
                            ELSE '.$NameField.'
                        END as TeacherName
                    FROM
                        YEAR_CLASS_TEACHER as yct
                        LEFT JOIN
                        INTRANET_USER as u ON (yct.UserID = u.UserID)
                        LEFT JOIN
                        INTRANET_ARCHIVE_USER as au ON (yct.UserID = au.UserID)
                    WHERE
                        YearClassID = \''.$thisClassID.'\'';
	        $result = $laccount->returnResultSet($sql);
	        
	        $teacherAry = array();
	        foreach($result as $teacherInfo){
	            $teacherAry[] = $teacherInfo['TeacherName'];
	        }
	        $implodeTeacherName = implode(', ', $teacherAry);
	        
	        $html .= "<table border=\"0\" cellpadding=\"4\" width=\"100%\" cellspacing=\"0\">
                        <tr>
                            <td colspan=\"8\" align=\"right\" class=\"print_hide\"><input type=\"button\" class=\" formsubbutton\" onClick=\"javascript:window.print();\" name=\"submit2\" id=\"submit2\" value=\"Print\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=\"8\" align=\"center\">".$BottomTitleArr[0] . "<br />" . $BottomTitleArr[1]. " " . $BottomTitleArr[2]."
                            </td>
                        </tr>
                        <tr>
                            <td colspan=\"4\" align=\"left\">".$Lang['General']['Class']." : $className</td>
                            <td colspan=\"4\" align=\"right\">".$Lang['General']['Teacher']." : $implodeTeacherName</td>
                        </tr>
                    ";

	        $colcount = 0;
	        $colMax = 5;

	        foreach($StudentInfoAry as $studentInfo)
	        {
	            $classNo = $studentInfo['ClassNumber'];
	            $studentName = $studentInfo['name'];
	            $chineseName = $studentInfo['ChineseName'];
	            $userLogin = $studentInfo['UserLogin'];
	            $className = $studentInfo['ClassTitleEN'];
	            $gender = $studentInfo['Gender'];
	            $webSamsNo = $studentInfo['WebSAMSRegNo'];

	            # use the libuser GET_OFFICIAL_PHOTO to get the photo instead of directly access the PhotoLink db field.
	            $id = $studentInfo['UserID'];
	            $studentPhoto = $liuser->GET_OFFICIAL_PHOTO_BY_USER_ID($id);
	            
	            if(file_exists($studentPhoto[0])){
	                $image = "<img src=\"".$studentPhoto[2]."\" border=0 width=\"100px\" height=\"130px\" />";
	            }else{
	                $image = "<img src=\"".$studentPhoto[0]."\" border=0 width=\"100px\" height=\"130px\" />";
	            }
	            
	            $imageFrame = "<table border=\"0\" cellpadding='0' cellspacing='1' width=\"100px\" height=\"130px\" bgcolor=\"#000\">";
	            $imageFrame .= "<tr bgcolor=\"#FFFFFF\"><td align=\"center\">".$image."</td></tr>";
	            $imageFrame .= "</table>";
	            
	            # student info
	            $studentInfo = $className." (" .$classNo. ") " . "<br />" . $studentName . " [" . $gender . "]";
	            // $studentInfo = $studentName . "<br />" . $chineseName . " [" . $gender . "]" . "<br />" . $studentName;

	            # display Image
                $colcount = ($colcount == $colMax)? 0:$colcount;
	            $html.= ($colcount == 0)?"<tr>":"";
	            $html.= "<td valign=\"top\" align=\"center\" width='20%'>".$imageFrame." ".$studentInfo."</td>";
	            $html.= ($colcount == ($colMax-1))?"</tr>\n":"";
	            
	            $colcount++;
	        }
	        
	        // $html .= "<tr><td colspan=\"8\" align=\"center\">".$BottomTitleArr[0] . "<br />" . $BottomTitleArr[1]. " " . $BottomTitleArr[2]."</td></tr>";
	        
	        $html .="</table>";
	    }
	    else if($thisClassID == "" && !empty($subjectGroupIdAry))
	    {
	        $stuPhoto = new libeClassApp_stuPhoto();
	        
	        $headerAry = array();
	        $dataAry = array();
	        $startDate = standardizeFormPostValue($_POST['startDate']);
	        $endDate = standardizeFormPostValue($_POST['endDate']);

	        $StudentInfoArys = $stuPhoto->getSubjectViewReportDataAry($viewType, $startDate, $endDate, $subjectGroupIdAry);
	        foreach($StudentInfoArys as $i => $StudentInfoAry)
	        {
	            // debug_pr($StudentInfoArys);
	            
	            # get the class name
	            // $_subjectName = Get_Lang_Selection($subjectAry[$i]['SubjectDescB5'], $StudentInfoArys[$i]['SubjectDescEN']);
	            $subjectGroupName = $StudentInfoAry[0]['SubjectGroupTitle'];
	            $subjectGroupID = $StudentInfoAry[0]['SubjectGroupID'];
	            // debug_pr($subjectName);

	            # get the current year name
	            $currentAcademicYearArr = $fcm->Get_Current_Academic_Year_And_Year_Term();
	            $currentYearName = $currentAcademicYearArr[0]['YearNameEN'];
	            
	            # get school name
	            $schoolName = GET_SCHOOL_NAME();
	            
	            $BottomTitleArr = array($schoolName, $Lang['General']['StudentPhotos'], $currentYearName);
	            
	            // $subjectNames = Get_Subject_Desc();
	            // debug_pr($subjectNames);

	            # get subject group teacher
	            $NameField = getNameFieldByLang('u.');
	            $ArchiveNameField = getNameFieldByLang2('au.');

	            // debug_pr($subjectClass);
	            $result = $stuPhoto->getTeacherlistAry($subjectGroupID);
	            // debug_pr($result);
	            
	            $teacherAry = array();
	            foreach($result as $teacherInfo){
	                $teacherAry[] = $teacherInfo['TeacherName'];
	            }
	            $implodeTeacherName = implode(', ', $teacherAry);
	            
	            if ($i == 0)
	            {
	                $printPBreak = "";
	                $printBtn = "<tr>
                                    <td colspan=\"8\" align=\"right\" class=\"print_hide\"><input type=\"button\" class=\"formsubbutton\" onClick=\"javascript:window.print();\" name=\"submit2\" id=\"submit2\" value=\"Print\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\"/></span>
                                    </td>
                                </tr>";
	            }
	            else
                {
	                $printPBreak = "breakhere";
	                $printBtn = "<tr>
                                    <td colspan=\"8\" align=\"right\">
                                    </td>
                                </tr>";
	            }
	            $html .= "<p class=\"$printPBreak\"><table border=\"0\" cellpadding=\"4\" width=\"100%\" cellspacing=\"0\">
                            $printBtn
                            <tr>
                                <td colspan=\"8\" align=\"center\">".$BottomTitleArr[0] . "<br />" . $BottomTitleArr[1]. " " . $BottomTitleArr[2]."
                                </td>
                            </tr>
                            <tr>
                                <td colspan=\"4\" align=\"left\">".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']." : $subjectGroupName</td>
                                <td colspan=\"4\" align=\"right\">".$Lang['General']['Teacher']." : $implodeTeacherName</td>
                            </tr>
                           </p>";

	            $colcount = 0;
	            $colMax = 5;
	            
                foreach($StudentInfoAry as $i => $studentInfo)
                {
                    $classNo = $studentInfo['ClassNumber'];
                    $studentName = $studentInfo['StudentName'];;
                    $userLogin = $studentInfo['UserLogin'];
                    $className = $studentInfo['ClassTitleEN'];
                    $gender = $studentInfo['Gender'];
                    $webSamsNo = $studentInfo['WebSAMSRegNo'];

                    # use the libuser GET_OFFICIAL_PHOTO to get the photo instead of directly access the PhotoLink db field.
                    $id = $studentInfo['UserID'];
                    $studentPhoto = $liuser->GET_OFFICIAL_PHOTO_BY_USER_ID($id);

                    if(file_exists($studentPhoto[0])){
                        $image = "<img src=\"".$studentPhoto[2]."\" border=0 width=\"100px\" height=\"130px\" />";
                    }else{
                        $image = "<img src=\"".$studentPhoto[0]."\" border=0 width=\"100px\" height=\"130px\" />";
                    }

                    $imageFrame = "<table border=\"0\" cellpadding='0' cellspacing='1' width=\"100px\" height=\"130px\" bgcolor=\"#000\">";
                    $imageFrame .= "<tr bgcolor=\"#FFFFFF\"><td align=\"center\">".$image."</td></tr>";
                    $imageFrame .= "</table>";

                    # student info
                    $studentInfo = $className." (" .$classNo. ") " . "<br />" . $studentName . " [" . $gender . "]";
                    // $studentInfo = $studentName . "<br />" . $chineseName . " [" . $gender . "]" . "<br />" . $studentName;

                    # display Image
                    $colcount = ($colcount == $colMax)? 0:$colcount;
                    $html .= ($colcount == 0)?"<tr>":"";
                    $html .= "<td valign=\"top\" align=\"center\" width='20%'>".$imageFrame." ".$studentInfo."</td>";
                    $html .= ($colcount == ($colMax-1))?"</tr>\n":"";

                    $colcount++;
                }

                // $html .= "<tr><td colspan=\"8\" align=\"center\">".$BottomTitleArr[0] . "<br />" . $BottomTitleArr[1]. " " . $BottomTitleArr[2]."</td></tr>";

                $html .= "</table>";
            }
        }

	    return $html;
   }
}
}        // End of directive
?>