<?php
// Using: ivan

if (!defined("LIBFORMSTAGE_DEFINED"))         // Preprocessor directives
{
	define("LIBFORMSTAGE_DEFINED", true);
	
	class Form_Stage extends libdb {
		var $FormStageID;
		var $Code;
		var $TitleEn;
		var $TitleCh;
		var $DisplayOrder;
		var $RecordStatus;
		var $DateInput;
		var $DateModified;
		var $LastModifiedBy;
		
		function Form_Stage($ClassGroupID=''){
			parent:: libdb();
			$this->TableName = 'FORM_STAGE';
			$this->ID_FieldName = "FormStageID";
			
			if ($ClassGroupID != '')
			{
				$this->RecordID = $ClassGroupID;
				$this->Get_Self_Info();
			}
		}
		
		function Get_Self_Info()
		{
			$sql = "";
			$sql .= " SELECT * FROM ".$this->TableName;
			$sql .= " WHERE ".$this->ID_FieldName." = '".$this->RecordID."'";
			
			$resultSet = $this->returnArray($sql);
			$infoArr = $resultSet[0];
			
			if (count($infoArr > 0))
			{
				foreach ($infoArr as $key => $value)
					$this->{$key} = $value;
			}
		}
		
		function Get_Name($Lang='')
		{
			$returnName = '';
			if ($Lang == '')
			{
				$returnName = $this->TitleEn.' ('.$this->TitleCh.')';
			}
			else if ($Lang == 'en')
			{
				$returnName = $this->TitleEn;
			}
			else if ($Lang == 'b5')
			{
				$returnName = $this->TitleCh;
			}
			
			return $returnName;
		}
		
		function Get_All_Form_Stage($Active='', $FormStageIDArr='')
		{
			$recordstatus_conds = '';
			if ($Active != '')
				$recordstatus_conds = " And RecordStatus = '".$Active."' ";
				
			$formStageID_conds = '';
			if ($FormStageIDArr != '')
				$formStageID_conds = " And ".$this->ID_FieldName." In (".implode(',', (array)$FormStageIDArr).") ";
				
			$sql = "Select
							*
					From
							".$this->TableName."
					Where
							1
							$recordstatus_conds
							$formStageID_conds
					Order By
							DisplayOrder
					";
			$InfoArr = $this->returnArray($sql);
			return $InfoArr;
		}
		
		function Get_Form_List($FormStageIDArr='')
		{
			if ($FormStageIDArr == '' && $this->FormStageID != '')
				$FormStageIDArr = array($this->FormStageID);
				
			$conds_FormStageID = '';
			if ($FormStageIDArr != '')
				$conds_FormStageID = " And FormStageID = '".implode(',', (array)$FormStageIDArr)."' ";
				
			$sql = "Select
							*
					From
							YEAR
					Where
							1
							$conds_FormStageID
					Order By
							Sequence
					";
			$subjectInfoArr = $this->returnArray($sql);
			return $subjectInfoArr;
		}
		
		function Get_Last_Modified_Info()
		{
			if ($this->FormStageID != '')
			{
				# self last modified info
				$sql = "Select
								*
						From
								".$this->TableName."
						Where
								".$this->ID_FieldName." = '".$this->RecordID."'
						";
			}
			else
			{
				# all class group last modified info
				$sql = "Select
								*
						From
								".$this->TableName."
						Where
								DateModified IN (
													SELECT 
															MAX(DateModified) 
													FROM ".$this->TableName."
												)
						";
			}
			
			$returnArr = $this->returnArray($sql);
			return $returnArr;
		}
		
		function Insert_Record($DataArr=array())
		{
			if (count($DataArr) == 0)
				return false;
				
			## insert data
			# DisplayOrder
			$thisDisplayOrder = $this->Get_Max_Display_Order() + 1;
			
			# Last Modified By
			$LastModifiedBy = ($_SESSION['UserID'])? "'".$_SESSION['UserID']."'" : 'NULL';
			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = "'".$this->Get_Safe_Sql_Query($value)."'";
			}
			
			## set others fields
			# DisplayOrder
			$fieldArr[] = 'DisplayOrder';
			$valueArr[] = $thisDisplayOrder;
			# Last Modified By
			$fieldArr[] = 'LastModifiedBy';
			$valueArr[] = $LastModifiedBy;
			# DateInput
			$fieldArr[] = 'DateInput';
			$valueArr[] = 'now()';
			# DateModified
			$fieldArr[] = 'DateModified';
			$valueArr[] = 'now()';
			# Set RecordStatus = active
			$fieldArr[] = 'RecordStatus';
			$valueArr[] = 1;
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$sql = '';
			$sql .= ' INSERT INTO '.$this->TableName;
			$sql .= ' ( '.$fieldText.' ) ';
			$sql .= ' VALUES ';
			$sql .= ' ( '.$valueText.' ) ';
			
			$success = $this->db_db_query($sql);
			if ($success == false) 
				$this->RollBack_Trans();
			else
				$this->Commit_Trans();
			
			return $success;
		}
		
		function Update_Record($DataArr=array())
		{
			if (count($DataArr) == 0)
				return false;
				
			
			# Build field update values string
			$valueFieldText = '';
			foreach ($DataArr as $field => $value)
			{
				$valueFieldText .= $field." = '".$this->Get_Safe_Sql_Query($value)."', ";
			}
			$valueFieldText .= ' DateModified = now(), ';
			$LastModifiedBy = ($_SESSION['UserID'])? "'".$_SESSION['UserID']."'" : 'NULL';
			$valueFieldText .= ' LastModifiedBy = '.$LastModifiedBy.' ';
			
			$sql = "Update ".$this->TableName."
						Set ".$valueFieldText."
					Where 
							".$this->ID_FieldName." = '".$this->RecordID."'
					";
			$success = $this->db_db_query($sql);
			
			return $success;
		}
		
		function Delete_Record($SoftDelete=1)
		{
			if ($SoftDelete == 1)
			{
				$success = $this->Inactivate_Record();
			}
			else
			{
				$sql = "Delete From ".$this->TableName." Where ".$this->ID_FieldName." = '".$this->RecordID."'";
				$success = $this->db_db_query($sql);
			}
			
			return $success;
		}
		
		function Inactivate_Record()
		{
			$DataArr['RecordStatus'] = 0;
			$success = $this->Update_Record($DataArr);
			
			return $success;
		}
		
		function Activate_Record()
		{
			$DataArr['RecordStatus'] = 1;
			
			# assign to the last display for activated records
			$thisDisplayOrder = $this->Get_Max_Display_Order() + 1;
			$DataArr['DisplayOrder'] = $thisDisplayOrder;
			
			$success = $this->Update_Record($DataArr);
			return $success;
		}
		
		function Get_Max_Display_Order()
		{
			$sql = '';
			$sql .= 'SELECT max(DisplayOrder) FROM '.$this->TableName;
			$MaxDisplayOrder = $this->returnVector($sql);
			
			return $MaxDisplayOrder[0];
		}
		
		function Is_Code_Existed($TargetCode, $ExcludeID='')
		{
			$TargetCode = $this->Get_Safe_Sql_Query(trim($TargetCode));
			
			$cond_ExcludeUniqueKey = '';
			if ($ExcludeID != '')
				$cond_ExcludeUniqueKey = " AND ".$this->ID_FieldName." != '$ExcludeID' ";
				
			# Check Form Stage Code
			$sql = "SELECT 
							DISTINCT(".$this->ID_FieldName.") 
					FROM 
							".$this->TableName." 
					WHERE 
							Code = '".$TargetCode."'
							$cond_ExcludeUniqueKey
					";
			$SameCodeObjectArr = $this->returnVector($sql);
			$isCodeExisted = (count($SameCodeObjectArr)==0)? 0 : 1;
			
			return $isCodeExisted;
		}
		
		function Get_Update_Display_Order_Arr($OrderText, $Separator=",", $Prefix="")
		{
			if ($OrderText == "")
				return false;
				
			$orderArr = explode($Separator, $OrderText);
			$orderArr = array_remove_empty($orderArr);
			$orderArr = Array_Trim($orderArr);
			
			$numOfOrder = count($orderArr);
			# display order starts from 1
			$counter = 1;
			$newOrderArr = array();
			for ($i=0; $i<$numOfOrder; $i++)
			{
				$thisID = str_replace($Prefix, "", $orderArr[$i]);
				if (is_numeric($thisID))
				{
					$newOrderArr[$counter] = $thisID;
					$counter++;
				}
			}
			
			return $newOrderArr;
		}
		
		function Update_DisplayOrder($DisplayOrderArr=array())
		{
			if (count($DisplayOrderArr) == 0)
				return false;
				
			$this->Start_Trans();
			for ($i=1; $i<=sizeof($DisplayOrderArr); $i++) {
				$thisObjectID = $DisplayOrderArr[$i];
				
				$sql = "UPDATE ".$this->TableName." SET 
									DisplayOrder = '".$i."' 
								WHERE 
									".$this->ID_FieldName." = '".$thisObjectID."' ";
				$Result['ReorderResult'.$i] = $this->db_db_query($sql);
			}
			
			if (in_array(false, $Result)) 
			{
				$this->RollBack_Trans();
				return false;
			}
			else
			{
				$this->Commit_Trans();
				return true;
			}
		}
		
		function Get_Form_Stage_Selection($ID_Name, $SelectedFormStageID='', $Onchange='', $NoFirst=0, $IsMultiple=0)
		{
			global $Lang;
			
			$FormStageArr = $this->Get_All_Form_Stage($active=1);
			$numOfFormStage = count($FormStageArr);
			
			$selectArr = array();
			for ($i=0; $i<$numOfFormStage; $i++)
			{
				$thisFormStageID = $FormStageArr[$i]['FormStageID'];
				$thisName = Get_Lang_Selection($FormStageArr[$i]['TitleCh'], $FormStageArr[$i]['TitleEn']);
				
				$selectArr[$thisFormStageID] = $thisName;
			}
			
			$onchange = '';
			if ($Onchange != "")
				$onchange = ' onchange="'.$Onchange.'" ';
			
			$multiple = '';
			if ($IsMultiple == 1)
				$multiple = ' multiple size=5 ';
				
			$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$multiple;
			
			if ($NoFirst == 0)
				$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['FormStage']);
			
			$classGroupSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedFormStageID, $isAll=0, $NoFirst, $firstTitle);
			
			return $classGroupSelection;
		}
		
		function Get_ID_By_Code($ParCode)
		{
			$sql = "SELECT 
						".$this->ID_FieldName."
					FROM 
						".$this->TableName."
					WHERE
						Code = '".$this->Get_Safe_Sql_Query($ParCode)."'
					";
			$tmp = $this->returnVector($sql);
			return empty($tmp)? '' : $tmp[0];
		}
		
		function Get_Name_By_ID($ParID)
		{
			$sql = "SELECT 
						TitleEn,
						TitleCh
					FROM 
						".$this->TableName."
					WHERE
						".$this->ID_FieldName." = '$ParID'
					";
			return $this->returnArray($sql);
		}
	}
} // End of directives
?>