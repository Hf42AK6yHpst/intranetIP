<?php
//modifying by:
/*
 * the ASL Parent App class, responsible for sending request to the ASL server
 * for ASL to push the message to their parent Apps
 * 
 * Ref doc: eClass Web Services Specification v1.0 by ASL 
 * 
 * Methods supported:
 * -- AWS_NotifyStudentStatus --
 * Sample parameter:
 * $param = array(
				"SchID" => "junior20-b5-demo2.eclass.com.hk",
				"UserName" => "asl_s1",
				"Status" => "A",
				"AccessDateTime" => "2011-06-01 08:40:10",
				"StudentName" => "ASL S1",
				"NotifyUser" => "ASL P1",
				"TargetUser" => "asl_p1"
	);
 * 
 * -- AWS_NotifyMessage --
 * Sample parameter:
 * $param = array(
				"SchID" => "junior20-b5-demo2.eclass.com.hk",
				"MsgNotifyDateTime" => "2011-07-29 12:00:00",
				"MessageTitle" => "Test Title",
				"MessageContent" => "Test Content",
				"NotifyFor" => "Class 1A notice",
				"IsPublic" => "N",
				"NotifyUser" => "Ms Wong",
				"TargetUsers" => array(
					"TargetUser" => array("asl_p1", "kelvin_p")
				)
	);
 */
include_once "libxml.php";
class ASLParentApp extends LibXML
{
	var $host; // 202.140.232.198
	var $path; // /NotifyProc
	var $timeout;
	var $postData;
	
	function ASLParentApp($host, $path, $timeout=10)
	{
		$this->host = $host;
		$this->path = $path;
		$this->timeout = $timeout;
		$this->postData = "";
	}
	
	# public
	# build and send the xml request for the AWS_NotifyStudentStatus web service and return the result
	function NotifyStudentStatus($paramArray, &$ReturnMessage)
	{
		$this->BuildXML("AWS_NotifyStudentStatus", $paramArray);
		return $this->SendRequest($ReturnMessage);
	}

	# public
	# build and send the xml request for the AWS_NotifyMessage web service and return the result
	function NotifyMessage($paramArray, &$ReturnMessage)
	{
		$this->BuildXML("AWS_NotifyMessage", $paramArray);
		return $this->SendRequest($ReturnMessage);
	}
		
	# private
	# to build the xml content that will be sent to ASL
	function BuildXML($method, $paramArray)
	{
		list($usec, $sec) = explode(" ", microtime());
		$t = time().substr($usec, 2,3);
		$requestID = "req" . $t . $_SERVER['SERVER_NAME'];
		$this->postData = '<?xml version="1.0" encoding="UTF-8"?>
						   <awsRequest xmlns:xsd="http://www.example.org/NewXMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
		$this->postData .= $this->XML_CreateElement("RequestID", NULL, $requestID);
		$this->postData .= $this->XML_CreateElement("RequestMethod", NULL, $method);
		$this->postData .= $this->XML_OpenElement("Request");
		$this->postData .= $this->Array_To_XML($paramArray, true);
		$this->postData .= $this->XML_CloseElement("Request");
  		$this->postData .= '</awsRequest>';
	}
	
	# private
	# send the request to ASL server, need to call BuildXML to build the xml content first
	function SendRequest(&$ReturnMessage)
	{
		$headers = array(
			"POST ".$this->path." HTTP/1.1",
			"Host: ".$this->host,
			"Content-Length: ". strlen($this->postData),
			"Content-Type: text/xml; charset=utf-8",
			"Connection: close"
		);
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "https://".$this->host.$this->path);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->timeout);
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers); 
		curl_setopt($curl, CURLOPT_POSTFIELDS, $this->postData);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		
		$ReturnMessage = curl_exec($curl);
		if (curl_error($curl))
		{
			$ReturnResult = false;
			$ReturnMessage = curl_errno($curl) . "|"  . curl_error($curl);
		   //printf("Error %s: %s", curl_errno($curl), curl_error($curl));
		}
		else
		{
			//parse the returned xml message and see if any error returned by ASL 
			
			
			$resultArray = $this->XML_ToArray($ReturnMessage);
			//$p = xml_parser_create();
			//xml_parse_into_struct($p, $ReturnMessage, $vals, $index);
			//xml_parser_free($p);
			
			if ($resultArray["awsResponse"]["ReturnResult"])
			{
				$ReturnResult = ($resultArray["awsResponse"]["ReturnResult"] == "Y");
			}
			else
			{
				$ReturnResult = false; 
			}
		}
		curl_close($curl); 
		
		return $ReturnResult;
	}
}
?>