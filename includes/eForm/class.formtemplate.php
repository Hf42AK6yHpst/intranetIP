<?php
// editing by
/********************
 * Date : 2017-05-31 Frankie
 ********************/
if (defined("LIBEFORM_DEFINED")) {
	class FormTemplate extends FormCommon {
		####################################################################################################
		function __construct($ModuleTitle = 'eForm') {
			parent::__construct($ModuleTitle);
		}

		####################################################################################################
		## Get Table Grid Information
		## - Fields array for libdbtable
		## - SQL for libdbtable
		####################################################################################################
		function getTableGridInfo($args = array()) {
			if (count($args) > 0) extract($args);
			
			$strSQL = "SELECT TemplateID, TemplateID, TemplateTitle, Editable, TemplateType, DateModified as LastModified
				FROM " . $this->formTemplateTable . " WHERE ModuleTitle='" . $this->ModuleTitle . "' AND " . $this->isDeletedCondition() . " AND TemplateType NOT LIKE 'Form'
			";
			if (isset($keyword) && !empty($keyword))
				$strSQL .= " AND (TemplateTitle LIKE '%" . $keyword . "%' OR DateModified  LIKE '%" . $keyword . "%')";
				$infoArr = array(
						"fields" => array("TemplateTitle", "TemplateType", "ViewForm", "LastModified", "TemplateStatus"),
						"order2"=> ", TemplateTitle ASC",
						"sql" => $this->sqlOpt($strSQL)
				);
				return $infoArr;
		}
		####################################################################################################
		## check TemplateIDs is used
		####################################################################################################
		function getIsUsedTableTemplateID($templateIDs = array(), $checkSubmitedGrid = false) {
			$inUsedTable = array();
			
			$strSQL = "SELECT ftv.TableTemplateID FROM " . $this->formTemplateTVTable . " AS ftv ";
			$strSQL .= " JOIN " . $this->formTemplateTable. " AS ftt ON (ftt.TemplateID=ftv.TemplateID AND isDeleted != '1') ";
			if (!$checkSubmitedGrid) {
				$strSQL .= " JOIN " . $this->formGridTable . " AS fgt ON (fgt.TemplateID=ftv.TemplateID AND isDeleted !='1')";
			}
			$strSQL .= " WHERE ftv.TableTemplateID IN ('" . implode("', '", $templateIDs) . "') GROUP BY TableTemplateID";
			
			$result = $this->returnResultSet($strSQL);
			$inUsedTable = array();
			if (count($result) > 0) {
				$inUsedTable = BuildMultiKeyAssoc($result, "TableTemplateID");
			}
			return $inUsedTable;
		}

		####################################################################################################
		## Get Table Grid Data with limit condition
		## - run with SQL
		####################################################################################################
		function getTableGridData($args = array()) {
			global $indexVar, $LAYOUT_SKIN;
			if (count($args) > 0) extract($args);
			$dataArr = array();
			if (isset($sql) && !empty($sql)) {
				$result = $this->returnResultSet($sql);
				if (count($result) > 0) {
					/*********************************************************/
					$formArr = BuildMultiKeyAssoc($result, "TemplateID");
					$templateIDs = array_keys($formArr);
					$inSubmiittedTable = $this->getIsUsedTableTemplateID($templateIDs);
					$inUsedTable = $this->getIsUsedTableTemplateID($templateIDs, true);
					/*********************************************************/
					$kk = 0;
					foreach ($formArr as $formID => $vv) {
						if (isset($girdInfo["fields"]) && count($girdInfo["fields"]) > 0) {
							foreach ($girdInfo["fields"] as $fieldIndx => $fieldLabel) {
								switch ($fieldLabel) {
									case "TemplateTitle":
										$dataArr[$kk][$fieldIndx] = "<a href='#'>" . $vv[$fieldLabel] . "</a>";
										if (isset($vv[$fieldLabel])) {
											/*
											if (!isset($inUsedTable[$vv["TemplateID"]])) {
												$dataArr[$kk][$fieldIndx] = " <a href=\"?task=settings/table_template/edit&tplid=" . $vv["TemplateID"] . "&ctk=" . $this->getToken($vv["TemplateID"]) . "\"><img src=\"/images/".$LAYOUT_SKIN."/icon_edit_b.gif\" height=\"20\" width=\"20\" align=\"absmiddle\" border=0> " . $vv[$fieldLabel] . "</a>";
											} else {
												$dataArr[$kk][$fieldIndx] = $vv[$fieldLabel];
											}*/
											$dataArr[$kk][$fieldIndx] = $vv[$fieldLabel];
										} else {
											$dataArr[$kk][$fieldIndx] = "-";
										}
										break;
									case "TemplateType" :
										$dataArr[$kk][$fieldIndx] = $this->formLang[$vv[$fieldLabel]]; 
										break;
									case "TemplateStatus":
										if (isset($inUsedTable[$vv["TemplateID"]])) $dataArr[$kk][$fieldIndx] = $this->formLang["isUsed"];
										else $dataArr[$kk][$fieldIndx] = "-";
										break;
									case "ViewForm":
										if (isset($inSubmiittedTable[$vv["TemplateID"]])) {
											$dataArr[$kk][$fieldIndx] = " <a href=\"?task=settings/table_template/edit&tplid=" . $vv["TemplateID"] . "&ctk=" . $this->getToken($vv["TemplateID"]) . "\"><img src=\"/images/".$LAYOUT_SKIN."/icon_view.gif\" align=\"absmiddle\" style='max-width:20px;' alt='" . $Lang["eForm"]["img_alt_view"] . "' border=0></a>";
										} else {
											$dataArr[$kk][$fieldIndx] = " <a href=\"?task=settings/table_template/edit&tplid=" . $vv["TemplateID"] . "&ctk=" . $this->getToken($vv["TemplateID"]) . "\"><img src=\"/images/".$LAYOUT_SKIN."/icon_edit_b.gif\" align=\"absmiddle\" style='max-width:20px;' alt='" . $Lang["eForm"]["img_alt_edit"] . "' border=0></a>";
										}
										$dataArr[$kk][$fieldIndx] .= " <a href=\"?task=settings/table_template/edit&cid=" . $vv["TemplateID"] . "&ctk=" . $this->getToken($vv["TemplateID"] . "-copy") . "\"><img src=\"/images/".$LAYOUT_SKIN."/icon_copy_b.gif\" align=\"absmiddle\" style='max-width:20px;' alt='" . $Lang["eForm"]["img_alt_copy"] . "' border=0></a>";
										break;
									default:
										if (isset($vv[$fieldLabel])) $dataArr[$kk][$fieldIndx] = $vv[$fieldLabel];
										else $dataArr[$kk][$fieldIndx] = "-";
										break;
								}
							}
						} else {
							$dataArr[$kk] = array_values($vv);
						}
						if (isset($inUsedTable[$vv["TemplateID"]])) {
							$dataArr[$kk][] = "<center>-</center>";
						} else {
							$dataArr[$kk][] = "<input type=checkbox name=groupIdAry[] id=groupIdAry[] value='" . $vv["TemplateID"] . "'>";
						}
						$kk++;
					}
				}
			}
			return $dataArr;
		}

		####################################################################################################
		function getFormTemplateInfoById($templateID, $getFullDetail = false, $isString = true) {
			// echo $templateID . "<br>";
			if ($getFullDetail) {
				$isString = false;
			}
			$templateInfo = array();
			$strSQL = "SELECT TemplateID, TemplateTitle, FormElementsJSON, Editable, TemplateType, TemplateSettingsJSON FROM " . $this->formTemplateTable
			. " WHERE ModuleTitle='" . $this->ModuleTitle . "' AND " . $this->isDeletedCondition() . " AND TemplateID='" . $templateID . "' LIMIT 1";
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				$templateInfo = $result[0];
				unset($result);
				
				if ($getFullDetail) {
					$strSQL = "SELECT FieldID, ElementStrID, FieldType, Priority FROM " . $this->formTemplateTVTable . " WHERE TemplateID='" . $templateInfo["TemplateID"] . "' ORDER BY Priority ASC";
					$result = $this->returnResultSet($strSQL);
					$tvIDs = array();
					if (count($result) > 0) {
						// $tvIDs = BuildMultiKeyAssoc($result, "Priority");
						foreach ($result as $kk => $val) {
							$tvIDs[$kk] = $val;
						}
					}
					$templateInfo["ElementsInfo"] = $tvIDs;
				}
						
				if (!empty($templateInfo["FormElementsJSON"])) {
					$templateInfo["FormElementsJSON"] = $this->dbToJsonFormat($templateInfo["FormElementsJSON"], $isString);
					// array_walk_recursive($templateInfo["FormElementsJSON"], "retrivDBValue");
					if (count($templateInfo["FormElementsJSON"]) > 0 && !$isString) {
						foreach ($templateInfo["FormElementsJSON"] as $kk => $val) {
							$templateInfo["FormElementsJSON"][$kk]["TemplateID"] = $templateInfo["TemplateID"];
							if (!isset($val["name"]) || empty($val["name"])) {
								if ($templateInfo["ElementsInfo"][$kk]["FieldType"] == $val["type"]) {
									if (isset($this->elmTypeArr[$val["type"]])) {
										// $templateInfo["FormElementsJSON"][$kk]["custom_name"] = $templateID . "_" . $templateInfo["ElementsInfo"][$kk]["FieldID"] . "_" . $templateInfo["ElementsInfo"][$kk]["ElementStrID"];
										$templateInfo["FormElementsJSON"][$kk]["custom_name"] = $this->elmTypeArr[$val["type"]] . "_" . $templateID . "_" . $templateInfo["ElementsInfo"][$kk]["FieldID"] . "_" . $templateInfo["ElementsInfo"][$kk]["ElementStrID"];
										$templateInfo["FormElementsJSON"][$kk]["FieldID"] = $templateInfo["ElementsInfo"][$kk]["FieldID"];
									}
								}
							} else {
								if ($templateInfo["ElementsInfo"][$kk]["FieldType"] == $val["type"]) {
									$priority = $kk + 1;
									$customID = $val["name"] . "-" . $priority;
									if ($templateInfo["ElementsInfo"][$kk]["ElementStrID"] == $customID) {
										if (isset($this->elmTypeArr[$val["type"]])) {
											$templateInfo["FormElementsJSON"][$kk]["custom_name"] = $this->elmTypeArr[$val["type"]] . "_" . $templateID . "_" . $templateInfo["ElementsInfo"][$kk]["FieldID"] . "_" . $templateInfo["ElementsInfo"][$kk]["ElementStrID"];
											$templateInfo["FormElementsJSON"][$kk]["FieldID"] = $templateInfo["ElementsInfo"][$kk]["FieldID"];
										}
									}
								}
							}
							switch ($val["type"]) {
								case "fixedTableElm":
								case "dynamicTableElm":
									if (!isset($templateInfo["TableInfo"][$val["value"]])) {
										$table_templateInfo = $this->getFormTemplateInfoById($val["value"], $getFullDetail, $isString);
										$templateInfo["TableInfo"][$val["value"]] = $table_templateInfo;
									}
									break;
							}
						}
					}
				}
				if (!empty($templateInfo["TemplateSettingsJSON"])) {
					$templateInfo["TemplateSettingsJSON"] = $this->dbToJsonFormat($templateInfo["TemplateSettingsJSON"], $isString);
					if (!$isString) {
						if (is_string($templateInfo["TemplateSettingsJSON"]["SettingsJSON"])) {
							$templateInfo["TemplateSettingsJSON"]["SettingsJSON"] = $this->strToJSON($templateInfo["TemplateSettingsJSON"]["SettingsJSON"]);
						} else {
							$templateInfo["TemplateSettingsJSON"]["SettingsJSON"] = $this->jsonToStr($templateInfo["TemplateSettingsJSON"]["SettingsJSON"]);
						}
					}
				}
			}
			$inUsedTable = $this->getIsUsedTableTemplateID(array( $templateInfo["TemplateID"] ));
			if (isset($inUsedTable[$templateInfo["TemplateID"]])) {
				$templateInfo["isUsed"] = true;
			} else {
				$templateInfo["isUsed"] = false;
			}
			return $templateInfo;
		}
		
		####################################################################################################
		function getTableTemplateOption() {
			$specArr = array("'" => "&#39;" );
			$idsOpt = array(
				"FixedTable" => array(),
				"DynamicTable" => array()
			);
			$strSQL = "SELECT TemplateID, TemplateTitle, TemplateType FROM " . $this->formTemplateTable . " WHERE TemplateType in ('" . implode("', '", array_keys($idsOpt)) . "') AND " . $this->isDeletedCondition() . " ORDER BY TemplateTitle";
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				foreach ($result as $kk => $vv) {
					$idsOpt[$vv["TemplateType"]][$vv["TemplateID"]] = str_replace(array_keys($specArr), array_values($specArr), $vv["TemplateTitle"]);
				}
			}
			return $idsOpt;
		}
		
		####################################################################################################
		function FormRelation($FormID, $TemplateID) {
			$strSQL = "UPDATE " . $this->formInfoTable . " SET TemplateID='" . $TemplateID ."' WHERE FormID='" . $FormID . "'";
			$result = $this->db_db_query($strSQL);
		}
		
		####################################################################################################
		function editFormTemplate($FormID, $args = array(), $TemplateType = "Form", $isUsed = false) {
			if (count($args) > 0) {
				unset($args["FormID"]);
				extract($args);
			}
			if (!isset($TemplateTitle)) {
				$TemplateTitle = $FormTitle;
			}
			$current_time = date("Y-m-d H:i:s");
			$param = array(
				"ModuleTitle" => $this->ModuleTitle,
				"TemplateTitle" => $TemplateTitle,
				"TemplateType" => $TemplateType
			);
			
			$isUpdate = false;
			if (isset($TemplateID) && $TemplateID > 0) {
				$strSQL = "SELECT TemplateID, isDeleted FROM " . $this->formTemplateTable . " WHERE TemplateID='" . $TemplateID . "'";
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0) {
					if ($result[0]["isDeleted"] == "1") {
						$templateRec = array( "result" => "isRemoved", "fid" => $FormID, "templateid" => $TemplateID );
						return $templateRec;
						exit;
					}
					$isUpdate = true;
				} else {
					$param["InputBy"] = $_SESSION["UserID"];
					$param["DateInput"] = $current_time;
					$param["TemplateID"] = $TemplateID;
				}
			}
			if (!$isUpdate) {
				$param["InputBy"] = $_SESSION["UserID"];
				$param["DateInput"] = $current_time;
			}
			$param["ModifyBy"] = $_SESSION["UserID"];
			$param["DateModified"] = $current_time;
			$templateRec = array( "result" => false );
			if ($isUsed) {
				$templateRec = array( "result" => true, "fid" => $FormID, "templateid" => $TemplateID );
			} else if (count($param) > 0) {
				if ($isUpdate) {
					$strSQL = "UPDATE " . $this->formTemplateTable . " SET ";
					$update_field = "";
					foreach ($param as $kk => $vv) {
						if (!empty($update_field)) {
							$update_field .= ", ";
						}
						$update_field .= " " . $kk . "='" . $vv . "' ";
					}
					$strSQL .= $update_field . " WHERE TemplateID='" . $TemplateID . "'";
				} else {
					$strSQL = "INSERT INTO " . $this->formTemplateTable . " (" . implode(", ", array_keys($param)) . ") VALUES ('" . implode("', '", array_values($param)) . "');";
				}
				$tp_result = $this->db_db_query($strSQL);
				if (!$isUpdate) {
					if (isset($param["TemplateID"])) {
						$TemplateID = $param["TemplateID"];
					} else {
						$TemplateID = $this->db_insert_id();
					}
				}
				$update_status = false;
				if ($tp_result) {
					$update_status = true;
					$updateParam["FormElementsJSON"] = "";
					$updateParam["TemplateSettingsJSON"] = "";
					if ($TemplateType == "Form") {
						if (!empty($FormSchemeJSON)) {
							$updateParam["FormElementsJSON"] = $this->jsonToDBFormat($FormSchemeJSON);
							$templateObj = $this->strToJson($FormSchemeJSON);
						}
						
						if (!empty($TemplateSettingsJSON)) {
							$updateParam["TemplateSettingsJSON"] = $this->jsonToDBFormat($TemplateSettingsJSON);
						}
					} else {
						if (!empty($SchemeJSON)) {
							$updateParam["FormElementsJSON"] = $this->jsonToDBFormat($SchemeJSON);
							$templateObj = $this->strToJson($SchemeJSON);
						}
						$TemplateSettingsJSON = array(
								"settings_maxrows" => $args["settings_maxrows"],
								"settings_noofrows" => $args["settings_noofrows"],
								"SettingsJSON" => $args["SettingsJSON"]
						);
						
						$updateParam["TemplateSettingsJSON"] = $this->jsonToDBFormat($TemplateSettingsJSON, false);
					}
					if (count($updateParam) > 0) {
						
						$strSQL = "SELECT FormElementsJSON, TemplateSettingsJSON FROM " . $this->formTemplateTable;
						$strSQL .= " WHERE TemplateID='" . $TemplateID . "' AND (FormElementsJSON != '" . $updateParam["FormElementsJSON"] . "' OR FormElementsJSON IS NULL OR TemplateSettingsJSON != '" . $updateParam["TemplateSettingsJSON"] . "' OR TemplateSettingsJSON IS NULL)";
						$result = $this->returnResultSet($strSQL);
						if ($result) {
							$strSQL = "UPDATE " . $this->formTemplateTable . " SET ";
							$update_field = "";
							if (empty($updateParam["FormElementsJSON"])) {
								$update_field .= "FormElementsJSON =''";
							} else {
								$update_field .= "FormElementsJSON ='" . $updateParam["FormElementsJSON"] . "'";
							}
							if (!empty($update_field)) {
								$update_field .= ",";
							}
							if (empty($updateParam["TemplateSettingsJSON"])) {
								$update_field .= "TemplateSettingsJSON =''";
							} else {
								$update_field .= "TemplateSettingsJSON ='" . $updateParam["TemplateSettingsJSON"] . "'";
							}
							$strSQL .= $update_field . " WHERE TemplateID='" . $TemplateID . "'";
							$result = $this->db_db_query($strSQL);
							if ($TemplateType == "Form") {
								if (!empty($FormID)) {
									$this->FormRelation($FormID, $TemplateID);
								}
							}
							if ($tp_result) {
								$i = 1;
								$strSQL = "DELETE FROM " . $this->formTemplateTVTable . " WHERE TemplateID='" . $TemplateID . "'";
								$result = $this->db_db_query($strSQL);
								
								if ($this->allowResetAutoIncID) {
									$strSQL = "SELECT MAX(FieldID) as latestID FROM " . $this->formTemplateTVTable;
									$result = $this->returnResultSet($strSQL);
									if ($result) {
										$latestID = $result[0]["latestID"] + 1;
									} else {
										$latestID = 1;
									}
									$strSQL = "ALTER TABLE " . $this->formTemplateTVTable . " AUTO_INCREMENT = " . $latestID;
									$result = $this->db_db_query($strSQL);
								}
								if (count($templateObj) > 0) {
									foreach ($templateObj as $elmKey => $elm) {
										if (empty($elm["name"])) $elm["name"] = $elm["type"];
										if (empty($elm["label"])) $elm["label"] = ucfirst($elm["type"]);
										$tvArr = array(
											"TemplateID" => $TemplateID,
											"ElementStrID" => $elm["name"] . "-" . $i,
											"ElementLabel" => $this->retrivDBValue($elm["label"]),
											"FieldType" => $elm["type"],
											"isRequired" => (isset($elm["required"]) && !empty($elm["required"])) ? 1: 0,
											"TableTemplateID" => "",
											"AttributeJSON" => $this->jsonToDBFormat($elm, false),
											"OptionsJSON" => $this->jsonToDBFormat($elm["values"], false),
											"Priority" => $i,
											"InputBy" => $_SESSION["UserID"],
											"DateInput" => $current_time,
											"ModifyBy" => $_SESSION["UserID"],
											"DateModified" => $current_time
										);
										if ($elm["type"] == "fixedTableElm" ||  $elm["type"] == "dynamicTableElm") {
											$tvArr["TableTemplateID"] = $elm["value"]; 
										}
										$strSQL = "INSERT INTO " . $this->formTemplateTVTable . " (" . implode(", ", array_keys($tvArr)) . ") VALUES ('" . implode("', '", array_values($tvArr)) . "')";
										$result = $this->db_db_query($strSQL);
										$i++;
									}
								}
							}
						}
					}
				}
				$templateRec = array( "result" => $update_status, "fid" => $FormID, "templateid" => $TemplateID );
			}
			return $templateRec;
		}
		
		####################################################################################################
		function retrieveAllowRemoteIDs($groupID) {
			$diff = array();
			if (count($groupID) > 0) {
				$inUsedTable = $this->getIsUsedTableTemplateID($groupID);
				if (count($inUsedTable) > 0) {
					$ids = array_keys($inUsedTable);
					$diff = array_diff($groupID, $ids);
				} else {
					$diff = $groupID;
				}
			}
			return $diff;
		}
		
		####################################################################################################
		function removeTableTemplate($groupID) {
			/*************************/
			$allowRevIDs = $this->retrieveAllowRemoteIDs($groupID);
			
			if ($this->allowDirectDelete) {

				$strSQL = "DELETE FROM " . $this->formTemplateTable . " WHERE TemplateID IN ('" . implode("', '", $allowRevIDs) . "')";
				$this->db_db_query($strSQL);
				
				$strSQL = "DELETE FROM " . $this->formTemplateTVTable. " WHERE TemplateID IN ('" . implode("', '", $allowRevIDs) . "')";
				$this->db_db_query($strSQL);
				
			} else {
				$strSQL = "UPDATE " . $this->formTemplateTable . " SET isDeleted='1', DeletedDate=NOW() WHERE TemplateID IN ('" . implode("', '", $allowRevIDs) . "') AND " . $this->isDeletedCondition();
				$this->db_db_query($strSQL);
			}
			return $allowRevIDs;
		}
		

		function getDataHeaderScheme($formInfo = array()) {
			$headerScheme = array();
			if (count($formInfo["TemplateInfo"]["FormElementsJSON"]) == count($formInfo["TemplateInfo"]["ElementsInfo"])) {
				$headerIndex = 0;
				foreach ($formInfo["TemplateInfo"]["FormElementsJSON"] as $kk => $val) {
					if (!in_array($val["type"], array("header", "paragraph"))) {
						if (in_array($val["type"], array("dynamicTableElm", "fixedTableElm"))) {
							if (isset($formInfo["TemplateInfo"]["TableInfo"][$val["value"]])) {
								$tableInfo = $formInfo["TemplateInfo"]["TableInfo"][$val["value"]];
								if (count($tableInfo["FormElementsJSON"]) == count($tableInfo["ElementsInfo"])) {
									$tableHasRow = false;
									if ($val["type"] == "dynamicTableElm") {
										$strSQL = "SELECT MAX(TotalRow) as maxNoTable FROM " . $this->formGridDataTable . "_TABLE ";
										$strSQL .= " WHERE FormID='" . $formInfo["FormID"] . "' AND TemplateID='" . $val["TemplateID"] . "' AND FieldID='" . $val["FieldID"] . "' AND FieldType='DT'";
										$result = $this->returnResultSet($strSQL);
										if ($result) {
											$headerScheme[$headerIndex]["maxNoTable"] = $result[0]["maxNoTable"];
										} else {
											$headerScheme[$headerIndex]["maxNoTable"] = 0;
										}
									} else {
										$headerScheme[$headerIndex]["presetValue"] = $tableInfo["TemplateSettingsJSON"]["SettingsJSON"]["tddata"];
									}
									$headerScheme[$headerIndex]["FormJSON"] = $val;
									$headerScheme[$headerIndex]["ElementJSON"] = $formInfo["TemplateInfo"]["ElementsInfo"][$kk];
									$headerScheme[$headerIndex]["settings_noofrows"] = $tableInfo["TemplateSettingsJSON"]["settings_noofrows"];
									$headerScheme[$headerIndex]["settings_maxrows"] = $tableInfo["TemplateSettingsJSON"]["settings_maxrows"];
									$tableIndex = 0;
									foreach ($tableInfo["FormElementsJSON"] as $t_kk => $t_val) {
										if (!in_array($t_val["type"], array("header", "paragraph"))) {
											$headerScheme[$headerIndex]["childElm"][$tableIndex]["FormJSON"] = $t_val;
											$headerScheme[$headerIndex]["childElm"][$tableIndex]["ElementJSON"] = $tableInfo["ElementsInfo"][$kk];
											$tableIndex++;
										}
									}
								}
							}
						} else {
							$headerScheme[$headerIndex]["FormJSON"] = $val;
							$headerScheme[$headerIndex]["ElementJSON"] = $formInfo["TemplateInfo"]["ElementsInfo"][$kk];
						}
						$headerIndex++;
					}
				}
			}
			return $headerScheme;
		}
		
		function retrieveHeaderArray($headerScheme, $withUserName = true) {
			$headerArr = array();
			if (count($headerScheme) > 0) {
				if ($withUserName) {
					$headerArr[] = $this->formLang["Export_UserLogin"];
					$headerArr[] = $this->formLang["Export_EnglishName"];
					$headerArr[] = $this->formLang["Export_ChineseName"];
					$headerArr[] = $this->formLang["Export_SubmitDate"];
				}
				foreach ($headerScheme as $kk => $val) {
					if (!in_array($val["FormJSON"]["type"], array("dynamicTableElm", "fixedTableElm"))) {
						if (isset($val["FormJSON"]["label"])) {
							$headerArr[] = str_replace(array("\n", "\r"), "", intranet_undo_htmlspecialchars($val["FormJSON"]["label"]));
						}
					} else {
						if ($val["FormJSON"]["type"] == "dynamicTableElm") {
							$noOfRow = $val["maxNoTable"];
							if ($val["settings_noofrows"] > $noOfRow) {
								$noOfRow = $val["settings_noofrows"];
							}
							if ($noOfRow > $val["settings_maxrows"] && $val["settings_maxrows"] > 0) {
								$noOfRow = $val["settings_maxrows"];
							}
						} else {
							$noOfRow = $val["settings_noofrows"];
						}
						if ($noOfRow < 1) $noOfRow = 1;
						for($i=1; $i <= $noOfRow; $i++) {
							foreach ($val["childElm"] as $chd_kk => $chd_val) {
								$headerArr[] = str_replace(array("\n", "\r"), "", intranet_undo_htmlspecialchars($val["FormJSON"]["label"])) . " " . str_replace(array("\n", "\r"), "", intranet_undo_htmlspecialchars($chd_val["FormJSON"]["label"])) . " " . $i;
							}
						}
					}
				}
			}
			return $headerArr;
		}
	}
}
?>