<?php
// editing by
/********************
 * Date : 2017-05-31 Frankie
 ********************/
if (defined("LIBEFORM_DEFINED")) {
	class FormGrid extends FormCommon {
		function __construct($ModuleTitle = 'eForm') {
			parent::__construct($ModuleTitle);
			// $this->entityCheck();			
		}

		function entityCheck() {
			// echo count($this->elmTypeArr);
		}
		####################################################################################################
		## Get Table Grid Information
		## - Fields array for libdbtable
		## - SQL for libdbtable
		####################################################################################################
		function getTableGridInfo($args = array(), $isFromAdmin = true, $forGrid = true) {

			if (count($args) > 0) extract($args);
			if ($forGrid) {
				if ($this->currLang == "en") {
					$lang_cond = "EnglishName as UserName, ChineseName,";
				} else {
					$lang_cond = "EnglishName, ChineseName as UserName,";
				}
			} else {
				$lang_cond = "UserLogin, EnglishName, ChineseName, ";
			}
			$strSQL = "SELECT TargetUserID, iu.UserID, " . $lang_cond . " UserLogin, gt.GridID, fti.FormID, fti.FormTitle, fti.StartDate, fti.DueDate, fti.TemplateID, gt.Draft, gt.GridStatus, gt.GridStatus as Status, gt.DateModified as FormSubmitDate
				FROM " . $this->formInfoTargetUserTable . " AS fitut";
			$strSQL .= " JOIN " . $this->userTable . " AS iu ON (iu.UserID=fitut.UserID) ";
			$strSQL .= " JOIN " . $this->formInfoTable. " AS fti ON (fitut.FormID=fti.FormID AND fti.ModuleTitle='" . $this->ModuleTitle . "') ";
			$strSQL .= " LEFT";
			$strSQL .= " JOIN " . $this->formGridTable . " AS gt ON (gt.FormID=fitut.FormID AND gt.ModuleTitle='" . $this->ModuleTitle . "'";
			$strSQL .= " AND fitut.UserID=gt.UserID AND iu.RecordStatus='1' AND gt.isDeleted='0'";
			if ($completed || $Status=="forminfo_submittedlist") {
				$strSQL .= " AND (gt.GridStatus='" . $this->GridStatusArr["COMPLETED"] . "') AND gt.Draft='0'";
			}
			$strSQL .= " )";
			$strSQL .= " WHERE iu.RecordStatus='1'";
			if (isset($FormID)) {
				$strSQL .= " AND fitut.FormID='" . $FormID . "'";
				if ($forGrid) {
					$fieldArr = array("UserName", "Status", "FormSubmitDate");
				} else {
					$fieldArr = array("GridID", "UserLogin", "EnglishName", "ChineseName", "FormSubmitDate");
				}
				$order2 = ", EnglishName ASC";
				if (isset($keyword) && !empty($keyword)) {
					$strSQL .= " AND (EnglishName LIKE '%" . $keyword . "%' OR ChineseName  LIKE '%" . $keyword . "%')";
				}
			} else {
				$strSQL .= " AND fitut.UserID='" . $UserID . "'";
				$fieldArr = array("FormTitle", "StartDate", "DueDate", "Status", "FormSubmitDate");
				$order2 = ", FormTitle ASC, StartDate ASC, DueDate ASC";
				if (isset($keyword) && !empty($keyword)) {
					$strSQL .= " AND (EnglishName LIKE '%" . $keyword . "%' OR ChineseName  LIKE '%" . $keyword . "%' OR FormTitle like '%" . $keyword . "%')";
				}
			}
			if ($completed || $Status=="forminfo_submittedlist") {
				$strSQL .= " AND (gt.GridStatus='" . $this->GridStatusArr["COMPLETED"] . "')";
			} else {
				if ($Status=="forminfo_list") {
					$strSQL .= " AND (gt.GridStatus NOT IN ('" . $this->GridStatusArr["COMPLETED"] . "') OR gt.GridID IS NULL) AND StartDate <= '" . date("Y-m-d") . "'";
				} else if ($Status == "eform_expired") {
					$strSQL .= " AND (gt.GridStatus NOT IN ('" . $this->GridStatusArr["COMPLETED"] . "') OR gt.GridID IS NULL) AND DueDate < '" . date("Y-m-d") . "'";
				}
			}
			$infoArr = array(
					"fields" => $fieldArr,
					"order2"=> $order2,
					"sql" => $this->sqlOpt($strSQL)
			);
			return $infoArr;
		}
		
		####################################################################################################
		## Get Table Grid Data with limit condition
		## - run with SQL
		####################################################################################################
		function getTableGridData($args = array(), $isFromAdmin = true, $forGrid = true) {
			global $Lang, $indexVar, $intranet_session_language;
			if (count($args) > 0) extract($args);
			
			#########################
			$CurrDate = date("Y-m-d");
			$Compare_currDate = strtotime($CurrDate);
			#########################
			
			$dataArr = array();
			if (isset($sql) && !empty($sql)) {
				$result = $this->returnResultSet($sql);
				if (count($result) > 0) {
					/*********************************************************/
					$kk = 0;
					foreach ($result as $index => $vv) {
						if (isset($girdInfo["fields"]) && count($girdInfo["fields"]) > 0) {
							$thisIsUsed = false;
							if (empty($vv["GridID"])) {
								$vv["GridStatus"] = $this->GridStatusArr["READY"];
							} else {
								if (!in_array($vv["GridStatus"], array($this->GridStatusArr["READY"], $this->GridStatusArr["EXPIRED"]))) {
									$thisIsUsed = true;
								}
							}
							#########################
							$compare_DueDate = strtotime($vv["DueDate"]);
							if (!in_array($vv["GridStatus"], array($this->GridStatusArr["COMPLETED"], $this->GridStatusArr["SUPPLEMENTARY_REQUEST"]))) {
								if ($compare_DueDate < $Compare_currDate) {
									$vv["GridStatus"] = $this->GridStatusArr["EXPIRED"];
								}
							}
							#########################
							foreach ($girdInfo["fields"] as $fieldIndx => $fieldLabel) {
								if (!$forGrid) {
									$fieldIndx = $fieldLabel;
								}
								switch ($fieldLabel) {
									case "UserName":
										$userName = $vv["UserName"];
										if ($intranet_session_language != "en" && !empty($vv["ChineseName"])) {
											$userName = $vv["ChineseName"];
										}
										if (!empty($vv["GridID"]) && in_array($vv["GridStatus"], array($this->GridStatusArr["COMPLETED"], $this->GridStatusArr["SUPPLEMENTARY_REQUEST"]))) {
											$dataArr[$kk][$fieldIndx] = "<a href=\"?task=management/form_management/formdetail&fid=" . $vv["FormID"] . "&gid=" . $vv["GridID"]. "&gid=" . $vv["GridID"] . "&ctk=" . $this->getToken($vv["FormID"] . "_" . $vv["GridID"]) . "\"><img src=\"/images/2009a/icon_view.gif\" height=\"20\" width=\"20\" align=\"absmiddle\" border=0> " . $userName . "</a>";
										} else {
											$dataArr[$kk][$fieldIndx] = $userName;
										}
										break;
									case "Status":
										$label = array_search($vv["GridStatus"], $this->GridStatusArr);
										if (empty($label)) $label = "READY";
										if ($vv["GridStatus"] != $this->GridStatusArr["COMPLETED"]) {
											$now = time(); // or your date as well
											$your_date = strtotime($vv["DueDate"]);
											$datediff = $now - $your_date;
											$NoOfDate = floor($datediff / (60 * 60 * 24));
											if ($NoOfDate > 3) {
												$dataArr[$kk][$fieldIndx] = "<span style='padding:4px; padding-left:10px; padding-right:10px; line-height:20px; color:#fff; background-color:#e14040;-webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px;'>" . $Lang["eForm"]["GStatus_EXPIRED_MoreThanThreeDays"]. "</span>";
											} else if ($NoOfDate == 3) {
												$dataArr[$kk][$fieldIndx] = "<span style='padding:4px; padding-left:10px; padding-right:10px; line-height:20px; color:#000; background-color:#ffae45;-webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px;'>" . $Lang["eForm"]["GStatus_EXPIRED_ThreeDaysLate"]. "</span>";
											} else if ($NoOfDate == 2) {
												$dataArr[$kk][$fieldIndx] = "<span style='padding:4px; padding-left:10px; padding-right:10px; line-height:20px; color:#000; background-color:#f1de23;-webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px;'>" . $Lang["eForm"]["GStatus_EXPIRED_TwoDaysLate"] . "</span>";
											} else if ($NoOfDate == 1) {
												$dataArr[$kk][$fieldIndx] = "<span style='padding:4px; padding-left:10px; padding-right:10px; line-height:20px; color:#fff; background-color:#32b123;-webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px;'>" . $Lang["eForm"]["GStatus_EXPIRED_OneDayLate"]. "</span>";
											} else {
												$dataArr[$kk][$fieldIndx] = $Lang["eForm"]["GStatus_" . $label];
											}
										} else {
											$dataArr[$kk][$fieldIndx] = $Lang["eForm"]["GStatus_" . $label];
										}
																				
										break;
									case "FormTitle":
										$formTitle = $vv[$fieldLabel];
										if ($vv["GridStatus"] != $this->GridStatusArr["COMPLETED"]) {
											$dataArr[$kk][$fieldIndx] = "<a href=\"?task=forminfo/editform&fid=" . $vv["FormID"] . "&gid=" . $vv["GridID"] . "&ctk=" . $this->getToken($vv["FormID"] . "_" . $vv["GridID"]) . "\"><img src=\"/images/2020a/icon_edit_b.gif\" height=\"20\" width=\"20\" align=\"absmiddle\" border=0> " . $formTitle. "</a>";
										} else {
											$dataArr[$kk][$fieldIndx] = "<a href=\"?task=forminfo/viewform&fid=" . $vv["FormID"] . "&gid=" . $vv["GridID"] . "&ctk=" . $this->getToken($vv["FormID"] . "_" . $vv["GridID"]) . "\"><img src=\"/images/2020a/icon_view.gif\" height=\"20\" width=\"20\" align=\"absmiddle\" border=0> " . $formTitle. "</a>";
										}
										break;
									case "FormSubmitDate":
										if (isset($vv[$fieldLabel])) $dataArr[$kk][$fieldIndx] = $vv[$fieldLabel];
										else $dataArr[$kk][$fieldIndx] = "-";
										
										if (!$thisIsUsed) {
											$dataArr[$kk][$fieldIndx] = "-";
										}
										break;
									default:
										if (isset($vv[$fieldLabel])) $dataArr[$kk][$fieldIndx] = $vv[$fieldLabel];
										else $dataArr[$kk][$fieldIndx] = "-";
										break;
								}
							}
						} else {
							$dataArr[$kk] = array_values($vv);
						}
						if ($isFromAdmin) {
							$dataArr[$kk][] = "<input type=checkbox name=groupIdAry[] id=groupIdAry[] value='" . $vv["UserID"] . "'>";
						}
						$kk++;
					}
				}
			}
			return $dataArr;
		}
		
		###########################################################################
		function removeGridData($FormID = array()) {
			if (count($FormID) > 0) {
				
				$strSQL = "UPDATE " . $this->formGridTable . " SET isDeleted='1', DeletedDate=NOW(), ModifyBy='" . $_SESSION["UserID"] . "' WHERE FormID IN ('" . implode("', '", $FormID) . "') AND isDeleted != '1'";
				$this->db_db_query($strSQL);
			}
			return $FormID;
		}
		
		###########################################################################
		function initGridInfo($formInfo = array()) {
			if (isset($formInfo["FormID"]) && !empty($formInfo["FormID"]) && isset($formInfo["TemplateID"]) && !empty($formInfo["TemplateID"])) {
				$currDateTime = date("Y-m-d H:i:s");
				$gridInitParam = array(
					"FormID" => $formInfo["FormID"],
					"ModuleTitle" => $this->ModuleTitle,
					"TemplateID" => $formInfo["TemplateID"],
					"UserID" => $_SESSION["UserID"],
					"GridType" => "Form",
					"GridStatus" => $this->GridStatusArr["READY"],
					"Draft" => 1,
					"isDeleted" => 0,
					"InputBy" => $_SESSION["UserID"],
					"DateInput" => date("Y-m-d H:i:s"),
					"ModifyBy" => $_SESSION["UserID"],
					"DateModified" => $currDateTime,
					"DeletedDate" => ""
				);
				$strSQL = "SELECT TargetUserID FROM " . $this->formInfoTargetUserTable . " WHERE FormID='" . $formInfo["FormID"] . "' AND UserID='" . $_SESSION["UserID"] . "'";
				$result = $this->returnResultSet($strSQL);
				if ($result) {
					$strSQL = "SELECT GridID FROM " . $this->formGridTable . " WHERE ModuleTitle='" . $this->ModuleTitle . "' AND UserID='" . $_SESSION["UserID"] . "' AND FormID='" . $formInfo["FormID"] . "' AND TemplateID='" . $formInfo["TemplateID"] . "' LIMIT 1";
					$result = $this->returnResultSet($strSQL);
					if (count($result) > 0) {
						$GridID = $result[0]["GridID"];
					} else {
						$strSQL = 'INSERT INTO ' . $this->formGridTable . ' (' . implode(', ', array_keys($gridInitParam)) . ') VALUES ("' . implode('", "', array_values($gridInitParam)) . '")';
						$result = $this->db_db_query($strSQL);
						if ($result) {
							$GridID = $this->db_insert_id();
						}
					}
					if ($GridID > 0) {
						$gridInfo = $this->getGridInfoByID($GridID, $formInfo);
						return $gridInfo;
					}
				}
			}
			return null;
		}
		
		###########################################################################
		function getGridInfoByID($GridID = "", $formInfo) {
			$strSQL = "SELECT GridID, FormID, TemplateID, UserID, GridType, GridStatus, Draft, isDeleted, DateModified FROM " . $this->formGridTable;
			$strSQL .= " WHERE GridID='" . $GridID . "' AND FormID='" . $formInfo["FormID"] . "' LIMIT 1";
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				return $result[0];
			}
			return NULL;
		}
		
		###########################################################################
		function getGridDataByID($GridID = "") {
			
			$dataTypeArr = $this->elmTypeArr;
			
			$dataTypeArr["table"] = "DT";
			
			foreach ($dataTypeArr as $fieldType => $fieldCode) {
				if (!in_array($fieldType, array("fixedTableElm", "dynamicTableElm"))) {
					$tplName = strtoupper($fieldType);
					$dbTBLName = $this->formGridDataTable . "_" . str_replace("-", "", $tplName);
					$fieldArr = array("DataID", "GridID", "FormID", "UserID", "TemplateID", "FieldID", "FieldType", "FromTable", "RowID", "OrgFieldName", "FieldName", "FieldValue");
					$strSQL = "SELECT " . implode(", ", array_values($fieldArr)) . " FROM " .  $dbTBLName . " WHERE GridID='" . $GridID . "'";
					$result = $this->returnResultSet($strSQL);
					$dataArr[$fieldType] = array();
					if (count($result) > 0) {
						foreach ($result as $kk => $vv) {
							if ($vv["FromTable"] == 1) {
								$dataArr[$fieldType][$vv["OrgFieldName"]][$vv["RowID"]] = $vv;
								if ($vv["FieldType"] == "NO") {
									if (!empty($dataArr[$fieldType][$vv["OrgFieldName"]][$vv["RowID"]]["FieldValue"])) {
										$dataArr[$fieldType][$vv["OrgFieldName"]][$vv["RowID"]]["FieldValue"] = $dataArr[$fieldType][$vv["OrgFieldName"]][$vv["RowID"]]["FieldValue"] * 1;
									}
								} else if (in_array($vv["FieldType"], array("TA", "TX"))) {
									// $dataArr[$fieldType][$vv["OrgFieldName"]][$vv["RowID"]]["disFieldValue"] = $this->retrivDBValue($vv["FieldValue"]);
									$dataArr[$fieldType][$vv["OrgFieldName"]][$vv["RowID"]]["disFieldValue"] = $vv["FieldValue"];
								}
							} else {
								$dataArr[$fieldType][$vv["OrgFieldName"]] = $vv;
								if ($vv["FieldType"] == "NO") {
									if (!empty($dataArr[$fieldType][$vv["OrgFieldName"]]["FieldValue"])) {
										$dataArr[$fieldType][$vv["OrgFieldName"]]["FieldValue"] = $dataArr[$fieldType][$vv["OrgFieldName"]]["FieldValue"] * 1;
									} else {
										$dataArr[$fieldType][$vv["OrgFieldName"]]["FieldValue"] = "";
									}
								} else if (in_array($vv["FieldType"], array("TA", "TX"))) {
									$dataArr[$fieldType][$vv["OrgFieldName"]]["disFieldValue"] = $this->retrivDBValue($dataArr[$fieldType][$vv["OrgFieldName"]]["FieldValue"]);
								}
							}
							if (in_array($fieldType, array("select", "checkbox-group", "radio-group", "table", 'studentElm', 'teacherElm', 'subjectGroupElm'))) {
								if ($vv["FromTable"] == 1) {
									if ($fieldType == "table") {
										$dataArr[$fieldType][$vv["OrgFieldName"]][$vv["RowID"]]["arrFieldValue"] = array_values($this->strToJSON($dataArr[$fieldType][$vv["OrgFieldName"]][$vv["RowID"]]["FieldValue"]));
									} else {
										$dataArr[$fieldType][$vv["OrgFieldName"]][$vv["RowID"]]["arrFieldValue"] = $this->strToJSON($dataArr[$fieldType][$vv["OrgFieldName"]][$vv["RowID"]]["FieldValue"]);
									}
									$arrFieldValue = $dataArr[$fieldType][$vv["OrgFieldName"]][$vv["RowID"]]["arrFieldValue"];
								} else {
									$dataArr[$fieldType][$vv["OrgFieldName"]]["arrFieldValue"] = $this->strToJSON($dataArr[$fieldType][$vv["OrgFieldName"]]["FieldValue"]);
									$arrFieldValue = $this->strToJSON($dataArr[$fieldType][$vv["OrgFieldName"]]["FieldValue"]);
								}
								if (in_array($fieldType, array('studentElm', 'teacherElm', 'subjectGroupElm'))) {
								    $meaningData = $this->getDBSelectionData($fieldType, $arrFieldValue);
								    if ($vv["FromTable"] == 1) {
                                        $dataArr[$fieldType][$vv["OrgFieldName"]][$vv["RowID"]]["meaningFieldValue"] = $meaningData;
								    } else {
								        $dataArr[$fieldType][$vv["OrgFieldName"]]["meaningFieldValue"] = $meaningData;
								    }
								}
							}
						}
					}
				}
			}
			
			return $dataArr;			
		}
		
		####################################################################################################
		function removeUserData($FormID, $targetUser = array()) {
			if ($FormID> 0 && count($targetUser) > 0) {
				$strSQL = "SELECT GridID FROM " . $this->formGridTable. " WHERE FormID='" . $FormID . "' AND UserID IN ('" . implode("', '", $targetUser) . "')";
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0) {
					$GridIDs = BuildMultiKeyAssoc($result, "GridID");
					foreach ($this->elmTypeArr as $kk => $vv) {
						if (!in_array($kk, array("fixedTableElm", "dynamicTableElm"))) {
							$strSQL = "DELETE FROM " . $this->formGridDataTable . "_" . str_replace("-", "", strtoupper($kk)) . " WHERE FormID='" . $FormID . "' AND UserID IN ('" . implode("', '", $targetUser) . "') AND GridID IN ('" . implode("', '", array_keys($GridIDs)) . "')";
							$this->db_db_query($strSQL);
							
							if ($this->allowResetAutoIncID) {
								$this->resetNextAutoIncreID($this->formGridDataTable. "_" . str_replace("-", "", strtoupper($kk)), "DataID");
							}
						}
					}
					$strSQL = "DELETE FROM " . $this->formGridDataTable. "_TABLE" . " WHERE FormID='" . $FormID . "' AND UserID IN ('" . implode("', '", $targetUser) . "') AND GridID IN ('" . implode("', '", array_keys($GridIDs)) . "')";
					$this->db_db_query($strSQL);
					
					if ($this->allowResetAutoIncID) {
						$this->resetNextAutoIncreID($this->formGridDataTable . "_TABLE", "DataID");
					}
					
					$strSQL = "DELETE FROM " . $this->formSuppleRequestTable . " WHERE FormID='" . $FormID . "' AND UserID IN ('" . implode("', '", $targetUser) . "') AND GridID IN ('" . implode("', '", array_keys($GridIDs)) . "')";
					$this->db_db_query($strSQL);
					
					if ($this->allowResetAutoIncID) {
						$this->resetNextAutoIncreID($this->formSuppleRequestTable, "SupplementalID");
					}

					$strSQL = "DELETE FROM " . $this->formGridTable. " WHERE FormID='" . $FormID . "' AND UserID IN ('" . implode("', '", $targetUser) . "') AND GridID IN ('" . implode("', '", array_keys($GridIDs)) . "')";
					$this->db_db_query($strSQL);
					
					if ($this->allowResetAutoIncID) {
						$this->resetNextAutoIncreID($this->formGridTable, "GridID");
					}
				}
				$strSQL = "DELETE FROM " . $this->formInfoTargetUserTable . " WHERE FormID='" . $FormID . "' AND UserID IN ('" . implode("', '", $targetUser) . "')";
				$this->db_db_query($strSQL);
				
				if ($this->allowResetAutoIncID) {
					$this->resetNextAutoIncreID($this->formInfoTargetUserTable, "TargetUserID");
				}
				
			}
		}
		
		function retrivPostData($orgname, $name, $elm, $isFromTable = false, $rowid = 0) {
			$nameArr = explode("_", $name);
			$dataArr = array(
					"OrgName" => $orgname,
					"FieldType" => $nameArr[0],
					"TemplateID" => $nameArr[1],
					"FieldID" => $nameArr[2],
					"name" => $nameArr[3],
					"value" => "",
					"rowID" => $rowid,
					"isFromTable" => $isFromTable
			);
			if (in_array($dataArr["FieldType"], $this->elmTypeArr)) {
				$dataArr["FieldTypeStr"] = array_search($dataArr["FieldType"], $this->elmTypeArr);
			}
			switch ($dataArr["FieldTypeStr"]) {
				case "dynamicTableElm":
				case "fixedTableElm":
					if (substr($dataArr["name"], -5) == "-RWL4") {
						$dataArr["value"] = $elm;
						$this->UpdateArr["TABLE"][] = $dataArr;
					} else {
						$tableID = implode("_", array($dataArr["FieldType"], $dataArr["TemplateID"], $dataArr["FieldID"], $dataArr["name"])) . "_";
						$sub_name = str_replace($tableID, "", $orgname);
						if (count($elm) > 0 && is_array($elm)) {
							foreach ($elm as $rowid => $vv) {
								###################################################################
								if ($rowid !== "_TPL_") {
									$this->retrivPostData($orgname, $sub_name, $vv, true, $rowid);
								}
								###################################################################
							}
							
						}
					}
					break;
				default:
					if ($dataArr != NULL && !empty($dataArr["FieldTypeStr"]) && !empty($dataArr["TemplateID"]) && !empty($dataArr["FieldID"])) {
						$dataArr["value"] = $elm;
						if (!empty($dataArr["value"])) {
							$this->UpdateArr[strtoupper($dataArr["FieldTypeStr"])][] = $dataArr;
						}
					}
					break;
			}
		}
		
		####################################################################################################
		function editGridData($postdata = array(), $saveAsDraft = true) {
			
			$this->UpdateArr = array();
			$res = array( "result" => false );

			$strSQL = "SELECT GridID FROM " . $this->formGridTable . " WHERE GridID='" . $postdata["gid"] . "'";
			$strSQL .= " AND GridStatus NOT IN ('" . $this->GridStatusArr["COMPLETED"] . "', '" . $this->GridStatusArr["EXPIRED"] . "') AND isDeleted != '1'";
			$result = $this->returnResultSet($strSQL);
			if (count($result) == 0) {
				return $res;
			}
			
			if (count($postdata) > 0) {
				array_walk_recursive($postdata, 'retrivPostValue');
				foreach ($postdata as $itemKey => $itemVal) {
					$this->retrivPostData($itemKey, $itemKey, $itemVal, false);
				}
			}
			
			if (count($this->UpdateArr) > 0) {
				foreach ($this->elmTypeArr as $type => $code) {
					$dbType = strtoupper($type);
					$dbTBLName = $this->formGridDataTable . "_" . str_replace("-", "", $dbType);
					$dbData[$dbTBLName] = array();
					if (isset($this->UpdateArr[$dbType]) && count($this->UpdateArr[$dbType]) > 0) {
						foreach ($this->UpdateArr[$dbType] as $kk => $vv) {
							switch (str_replace("-", "", $dbType)) {
								case "RADIOGROUP":
								case "CHECKBOXGROUP":
								case "SELECT":
									$val = $this->jsonToStr($vv["value"]);
									break;
								default:
									$val = $vv["value"];
									break;
							}
							
							$dbData[$dbTBLName][$kk] = array(
								"GridID" => $postdata["gid"],
								"FormID" => $postdata["fid"],
								"UserID" => $_SESSION["UserID"],
								"TemplateID" => $vv["TemplateID"],
								"FieldID" => $vv["FieldID"],
								"FieldType" => $vv["FieldType"],
								"FieldName" => $vv["name"],
								"OrgFieldName" => $vv["OrgName"],
								"RowID" => $vv["rowID"],
								"FromTable" => ($vv["isFromTable"]) ? 1 : 0,
									"FieldValue" => $this->Get_Safe_Sql_Query($val)
							);
						}
					}
				}
				if (isset($this->UpdateArr["TABLE"]) && count($this->UpdateArr["TABLE"])) {
					$dbTBLName = $this->formGridDataTable . "_TABLE";
					$dbData[$dbTBLName] = array();
					foreach ($this->UpdateArr["TABLE"]  as $kk => $vv) {
						$totalRow = count($vv["value"]) - 1;
						if ($totalRow < 0) $totalRow = 0;
						$dbData[$dbTBLName][$kk] = array(
								"GridID" => $postdata["gid"],
								"FormID" => $postdata["fid"],
								"UserID" => $_SESSION["UserID"],
								"TemplateID" => $vv["TemplateID"],
								"FieldID" => $vv["FieldID"],
								"FieldType" => $vv["FieldType"],
								"FieldName" => $vv["name"],
								"OrgFieldName" => $vv["OrgName"],
								"RowID" => $vv["rowID"],
								"TotalRow" => $totalRow,
								"FromTable" => 1,
								"FieldValue" => $this->Get_Safe_Sql_Query($this->jsonToStr(array_values($vv["value"])))
						);
					}
				}
				
				if (count($dbData) > 0) {
					foreach ($dbData as $dbTblName => $updateArr) {
						$strSQL = "DELETE FROM " . $dbTblName . " WHERE FormID='" . $postdata["fid"] . "' AND GridID='" . $postdata["gid"] . "'";
						$result = $this->db_db_query($strSQL);
						if ($this->allowResetAutoIncID) {
		
							$this->resetNextAutoIncreID($dbTblName, "DataID");
						}
						if (count($updateArr) > 0) {
							foreach ($updateArr as $kk => $param) {
								$strSQL = "INSERT INTO " . $dbTblName . " (" . implode(", ", array_keys($param)) . ", InputBy, DateInput, ModifyBy, DateModified)";
								$strSQL .= " VALUES ('" . implode("', '", array_values($param)) . "', '" . $_SESSION["UserID"] . "', NOW(), '" . $_SESSION["UserID"] . "', NOW())";
								$result = $this->db_db_query($strSQL);
							}
						}
						
					}
					$res = array( "result" => true );
					if ($saveAsDraft) {
						$strSQL = "UPDATE " . $this->formGridTable . " SET Draft='1', DateModified=NOW(), ModifyBy='" . $_SESSION["UserID"] . "' WHERE GridID='" . $postdata["gid"] . "'";
						$strSQL .= " AND GridStatus NOT IN ('" . $this->GridStatusArr["COMPLETED"] . "', '" . $this->GridStatusArr["EXPIRED"] . "') AND isDeleted != '1'";
						$result = $this->db_db_query($strSQL);
					} else {
						$strSQL = "UPDATE " . $this->formGridTable . " SET Draft='0', GridStatus='" . $this->GridStatusArr["COMPLETED"] . "', DateModified=NOW(), ModifyBy='" . $_SESSION["UserID"] . "' WHERE GridID='" . $postdata["gid"] . "'";
						$strSQL .= " AND isDeleted != '1'";
						$result = $this->db_db_query($strSQL);
					}
				}
				
			}
			return $res;
		}
		
		function addSuppRequest($param) {
			$res = array( "result" => "Failed" );
			if (count($param) > 0) {
				$strSQL = "INSERT INTO " . $this->formSuppleRequestTable . "  (" . implode(", ", array_keys($param)) . " ) VALUES ";
				$strSQL .= " ('" . implode("', '", array_values($param)) . "');";
				$result = $this->db_db_query($strSQL);
				if ($result) {
					$SupplementalID = $this->db_insert_id();
					
					$strSQL = "UPDATE " . $this->formGridTable . " SET GridStatus='" . $this->GridStatusArr["SUPPLEMENTARY_REQUEST"] . "' WHERE GridID='" . $param["GridID"] . "'";
					$result = $this->db_db_query($strSQL);
					
					return array("result" => "SUCCESS", "suppid" => $SupplementalID );
				}
			}
			return $res;
		}
		
		function getGridLatestSuppInfo($GridID) {
		    $strSQL = "SELECT SupplementalID, FormID, GridID, UserID, SuppStatus, SuppMsg, SuppReadDate FROM " . $this->formSuppleRequestTable . " WHERE GridID='" . $GridID . "' ORDER BY DateInput DESC LIMIT 1";
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				$suppRec = $result[0];
				return $suppRec;
			}
			return NULL;
		}
		
		function retrieveForExport($gridInitData, $gridData, $headerScheme) {
		    $dataSelectSplitter = ", ";
			$dataArr = array();
			if (count($gridInitData) > 0 && count($headerScheme) > 0) {
				$dataArr[] = $gridInitData["UserLogin"];
				$dataArr[] = $gridInitData["EnglishName"];
				$dataArr[] = $gridInitData["ChineseName"];
				$dataArr[] = $gridInitData["FormSubmitDate"];
				$index = count($dataArr);
				foreach ($headerScheme as $kk => $val) {
					switch ($val["FormJSON"]["type"]) {
						case "number":
							$dataArr[$index] = $gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["FieldValue"] * 1;
							$index++;
							break;
						case "textarea":
							$dataArr[$index] = str_replace("<br>", "\n", $gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["FieldValue"]);
							$index++;
							break;
						case "select":
							if (is_array($gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["arrFieldValue"]) && count($gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["arrFieldValue"]) > 0) {
								$dataArr[$index] = implode(", ", $gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["arrFieldValue"]);
							} else {
								$dataArr[$index] = "";
							}
							$index++;
							break;
						case "radio-group":
						case "checkbox-group":
							$hasOther = false;
							$dataArr[$index] = "";
							if (count($gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["arrFieldValue"]) > 0) {
								foreach ($gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["arrFieldValue"] as $groupIndex => $groupVal) {
									if (!is_array($groupVal)) {
										if (!empty($dataArr[$index])) $dataArr[$index] .= $dataSelectSplitter;
										if ($groupVal == "other") {
											$hasOther = true;
											$dataArr[$index] .= $this->formLang["other"];
										} else {
											$already_assigned = false;
											foreach ($val["FormJSON"]["values"] as $optKk => $optVal) {
												if ($optVal["value"] == $groupVal && !$already_assigned) {
													$already_assigned = true;
													// $dataArr[$index] .= $optVal["label"];
													$dataArr[$index] .= $optVal["value"];
												}
											}
										}
									} else if ($hasOther) {
										$dataArr[$index] .= ": " . $groupVal["other"];
									}
								}
							}
							$index++;
							break;
						case "fixedTableElm" :
						case "dynamicTableElm" :
							$tableName = $val["FormJSON"]["custom_name"];
							if ($val["FormJSON"]["type"] == "dynamicTableElm") {
								$noOfRow = $val["maxNoTable"];
								if ($val["settings_noofrows"] > $noOfRow) {
									$noOfRow = $val["settings_noofrows"];
								}
								if ($noOfRow > $val["settings_maxrows"] && $val["settings_maxrows"] > 0) {
									$noOfRow = $val["settings_maxrows"];
								}
							} else {
								$noOfRow = $val["settings_noofrows"];
							}
							if ($noOfRow < 1) $noOfRow = 1;
							for($i=1; $i <= $noOfRow; $i++) {
								foreach ($val["childElm"] as $chd_kk => $chd_val) {
									$dataName = $tableName . "_" . $chd_val["FormJSON"]["custom_name"];
									$dataArr[$index] = "";
									if ($val["FormJSON"]["type"] == "dynamicTableElm") {
										if (isset($gridData["table"][$tableName. "-RWL4"][0]["arrFieldValue"][$i])) {
											$dataTableIndex = $gridData["table"][$tableName . "-RWL4"][0]["arrFieldValue"][$i];
										}
										$presetValue = "";
									} else {
										$dataTableIndex = $i - 1;
										$presetValue = $val["presetValue"][$dataTableIndex][$chd_kk];
									}
									if (!empty($presetValue)) {
										switch ($chd_val["FormJSON"]["type"]) {
											case "select":
											case "checkbox-group":
											case "radio-group":
												foreach ($chd_val["FormJSON"]["values"] as $optKk => $optVal) {
													if ($optVal["value"] == $presetValue) {
														$dataArr[$index] .= $optVal["label"];
													}
												}
												break;
											default:
												$dataArr[$index] = $presetValue;
												break;
										}
										$index++;
									} else {
										switch ($chd_val["FormJSON"]["type"]) {
											case "select":
											case "radio-group":
											case "checkbox-group":
												$hasOther = false;
												$dataArr[$index] = "";
												if (isset($gridData[$chd_val["FormJSON"]["type"]][$dataName][$dataTableIndex])) {
													if (count($gridData[$chd_val["FormJSON"]["type"]][$dataName][$dataTableIndex]["arrFieldValue"]) > 0) {
														foreach ($gridData[$chd_val["FormJSON"]["type"]][$dataName][$dataTableIndex]["arrFieldValue"] as $groupIndex => $groupVal) {
															if (!is_array($groupVal)) {
															    if (!empty($dataArr[$index])) $dataArr[$index] .= $dataSelectSplitter;
																if ($groupVal == "other") {
																	$hasOther = true;
																	$dataArr[$index] .= $this->formLang["other"];
																} else {
																	$already_assigned = false;
																	foreach ($chd_val["FormJSON"]["values"] as $optKk => $optVal) {
																		if ($optVal["value"] == $groupVal && !$already_assigned) {
																			$already_assigned = true;
																			// $dataArr[$index] .= $optVal["label"];
																			$dataArr[$index] .= $optVal["value"];
																		}
																	}
																}
															} else if ($hasOther) {
																$dataArr[$index] .= ": " . $groupVal["other"];
															}
														}
													}
												}
												if (empty($dataArr[$index])) $dataArr[$index] = "-";
												$index++;
												break;
											case "textarea":
												$dataArr[$index] = str_replace("<br>", "\n", $gridData[$chd_val["FormJSON"]["type"]][$dataName][$dataTableIndex]["FieldValue"]);
												if (empty($dataArr[$index])) $dataArr[$index] = "-";
												$index++;
												break;
											case "number":
											case "text":
											case "dateFieldElm":
											case "timeFieldElm":
												$dataArr[$index] = $gridData[$chd_val["FormJSON"]["type"]][$dataName][$dataTableIndex]["FieldValue"];
												if (empty($dataArr[$index])) $dataArr[$index] = "-";
												$index++;
												break;
											case "studentElm":
											    $extraData = "";
											    if (isset($gridData[$chd_val["FormJSON"]["type"]][$dataName][$dataTableIndex]["meaningFieldValue"])) {
											        $meaningFieldValue = $gridData[$chd_val["FormJSON"]["type"]][$dataName][$dataTableIndex]["meaningFieldValue"];
											        if (isset($meaningFieldValue["dataArr"]) && count($meaningFieldValue["dataArr"])) {
											            $extraData = "[" . Get_Lang_Selection((empty($meaningFieldValue["YearInfo"]["YearNameB5"]) ? $meaningFieldValue["YearInfo"]["YearNameEN"] : $meaningFieldValue["YearInfo"]["YearNameB5"]), $meaningFieldValue["YearInfo"]["YearNameEN"]) . "] ";
											            $extraLoop = "";
											            foreach ($meaningFieldValue["dataArr"] as $m_stk => $m_stv) {
											                if (!empty($extraLoop)) $extraLoop .= $dataSelectSplitter;
											                $extraLoop .= ""  . Get_Lang_Selection((empty($m_stv["ClassTitleB5"]) ? $m_stv["ClassTitleEN"] : $m_stv["ClassTitleB5"]), $m_stv["ClassTitleEN"]) . " (" . $m_stv['ClassNumber'] . ") - ";
											                $extraLoop .= Get_Lang_Selection((empty($m_stv["ChineseName"]) ? $m_stv["EnglishName"] : $m_stv["ChineseName"]), $m_stv["EnglishName"]);
											            }
											            $extraData .= $extraLoop;
											        }
											    }
											    $dataArr[$index] = $extraData;
											    if (empty($dataArr[$index])) $dataArr[$index] = "-";
											    $index++;
											    break;
											case "teacherElm":
											    $extraData = "";
											    if (isset($gridData[$chd_val["FormJSON"]["type"]][$dataName][$dataTableIndex]["meaningFieldValue"])) {
											        $meaningFieldValue = $gridData[$chd_val["FormJSON"]["type"]][$dataName][$dataTableIndex]["meaningFieldValue"];
											        if (isset($meaningFieldValue["dataArr"]) && count($meaningFieldValue["dataArr"])) {
											            $extraLoop = "";
											            foreach ($meaningFieldValue["dataArr"] as $m_stk => $m_stv) {
											                if (!empty($extraLoop)) $extraLoop .= $dataSelectSplitter;
											                $extraLoop .= Get_Lang_Selection((empty($m_stv["ChineseName"]) ? $m_stv["EnglishName"] : $m_stv["ChineseName"]), $m_stv["EnglishName"]);
											            }
											            $extraData .= $extraLoop;
											        }
											    }
											    $dataArr[$index] = $extraData;
											    if (empty($dataArr[$index])) $dataArr[$index] = "-";
											    $index++;
											    break;
											case "subjectGroupElm":
											    $extraData = "";
											    if (isset($gridData[$chd_val["FormJSON"]["type"]][$dataName][$dataTableIndex]["meaningFieldValue"])) {
											        $meaningFieldValue = $gridData[$chd_val["FormJSON"]["type"]][$dataName][$dataTableIndex]["meaningFieldValue"];
											        if (isset($meaningFieldValue["dataArr"]) && count($meaningFieldValue["dataArr"])) {
											            $extraData = "[" . Get_Lang_Selection((empty($meaningFieldValue["YearInfo"]["YearNameB5"]) ? $meaningFieldValue["YearInfo"]["YearNameEN"] : $meaningFieldValue["YearInfo"]["YearNameB5"]), $meaningFieldValue["YearInfo"]["YearNameEN"]) . "] ";
											            $extraLoop = "";
											            foreach ($meaningFieldValue["dataArr"] as $m_stk => $m_stv) {
											                if (!empty($extraLoop)) $extraLoop .= $dataSelectSplitter;
											                $extraLoop .= Get_Lang_Selection((empty($m_stv["ClassTitleB5"]) ? $m_stv["ClassTitleEN"] : $m_stv["ClassTitleB5"]), $m_stv["ClassTitleEN"]);
											            }
											            $extraData .= $extraLoop;
											        }
											    }
											    $dataArr[$index] = $extraData;
											    if (empty($dataArr[$index])) $dataArr[$index] = "-";
											    $index++;
											    break;
										}
									}
								}
							}
							break;
						case "text":
						case "dateFieldElm":
						case "timeFieldElm":
							$dataArr[$index] = $gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["FieldValue"];
							$index++;
							break;
						case "studentElm":
						    $extraData = "";
						    if (isset($gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["meaningFieldValue"])) {
						        $meaningFieldValue = $gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["meaningFieldValue"];
						        if (isset($meaningFieldValue["dataArr"]) && count($meaningFieldValue["dataArr"])) {
						            $extraData = "[" . Get_Lang_Selection((empty($meaningFieldValue["YearInfo"]["YearNameB5"]) ? $meaningFieldValue["YearInfo"]["YearNameEN"] : $meaningFieldValue["YearInfo"]["YearNameB5"]), $meaningFieldValue["YearInfo"]["YearNameEN"]) . "] ";
						            $extraLoop = "";
						            foreach ($meaningFieldValue["dataArr"] as $m_stk => $m_stv) {
						                if (!empty($extraLoop)) $extraLoop .= $dataSelectSplitter;
						                $extraLoop .= ""  . Get_Lang_Selection((empty($m_stv["ClassTitleB5"]) ? $m_stv["ClassTitleEN"] : $m_stv["ClassTitleB5"]), $m_stv["ClassTitleEN"]) . " (" . $m_stv['ClassNumber'] . ") - ";
						                $extraLoop .= Get_Lang_Selection((empty($m_stv["ChineseName"]) ? $m_stv["EnglishName"] : $m_stv["ChineseName"]), $m_stv["EnglishName"]);
						            }
						            $extraData .= $extraLoop;
						        }
						    }
						    $dataArr[$index] = $extraData;
						    if (empty($dataArr[$index])) $dataArr[$index] = "-";
						    $index++;
						    break;
						case "teacherElm":
						    $extraData = "";
						    if (isset($gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["meaningFieldValue"])) {
						        $meaningFieldValue = $gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["meaningFieldValue"];
						        if (isset($meaningFieldValue["dataArr"]) && count($meaningFieldValue["dataArr"])) {
						            $extraLoop = "";
						            foreach ($meaningFieldValue["dataArr"] as $m_stk => $m_stv) {
						                if (!empty($extraLoop)) $extraLoop .= $dataSelectSplitter;
						                $extraLoop .= Get_Lang_Selection((empty($m_stv["ChineseName"]) ? $m_stv["EnglishName"] : $m_stv["ChineseName"]), $m_stv["EnglishName"]);
						            }
						            $extraData .= $extraLoop;
						        }
						    }
						    $dataArr[$index] = $extraData;
						    if (empty($dataArr[$index])) $dataArr[$index] = "-";
						    $index++;
						    break;
						case "subjectGroupElm":
						    $extraData = "";
						    if (isset($gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["meaningFieldValue"])) {
						        $meaningFieldValue = $gridData[$val["FormJSON"]["type"]][$val["FormJSON"]["custom_name"]]["meaningFieldValue"];
						        if (isset($meaningFieldValue["dataArr"]) && count($meaningFieldValue["dataArr"])) {
						            $extraData = "[" . Get_Lang_Selection((empty($meaningFieldValue["YearInfo"]["YearNameB5"]) ? $meaningFieldValue["YearInfo"]["YearNameEN"] : $meaningFieldValue["YearInfo"]["YearNameB5"]), $meaningFieldValue["YearInfo"]["YearNameEN"]) . "] ";
						            $extraLoop = "";
						            foreach ($meaningFieldValue["dataArr"] as $m_stk => $m_stv) {
						                if (!empty($extraLoop)) $extraLoop .= $dataSelectSplitter;
						                $extraLoop .= Get_Lang_Selection((empty($m_stv["ClassTitleB5"]) ? $m_stv["ClassTitleEN"] : $m_stv["ClassTitleB5"]), $m_stv["ClassTitleEN"]);
						            }
						            $extraData .= $extraLoop;
						        }
						    }
						    $dataArr[$index] = $extraData;
						    if (empty($dataArr[$index])) $dataArr[$index] = "-";
						    $index++;
						    break;
					}
				}
			}
			return $dataArr;
		}
	}
}
?>