<?php
// Using :

########################### Change Log ###########################
/*   
 *  
 */
##################################################################

include_once("json.php");

class libcloudreport
{
    var $json;
    var $loginUsername;
    var $loginPassword;
    var $apiDomain;
    var $apiVersion;
    var $accessToken;
    var $schoolCode;
    var $startDate;
    var $EndDate;
    
    function libcloudreport($username, $password, $APIver, $schoolCode){
        $this->loginUsername = $username;
        $this->loginPassword = $password;
        $this->apiDomain = "https://admin:WVVZqmG96N@ecapi.eclasscloud.hk";
        $this->apiVersion = $APIver;
        $this->json = new JSON_obj();
        $this->schoolCode = $schoolCode;
    }
    
    function getAuthorizationCode(){
        $url = $this->apiDomain."/".$this->apiVersion."/authorize";
        $ch = curl_init();
        $data =array("username"=>$this->loginUsername, "password"=>$this->loginPassword);
        $data_string = $this->json->encode($data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
            );
        $output = $this->json->decode(curl_exec($ch));
        curl_close($ch);
        if($output['status']){
            $authorizationCode = $output['data']['authorization_code']; 
        } 
        return $authorizationCode;
    }
    
    function getAccesstoken(){
        $url = $this->apiDomain."/".$this->apiVersion."/accesstoken";
        $ch = curl_init();
        $data =array("authorization_code"=>$this->getAuthorizationCode());
        $data_string = $this->json->encode($data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
            );
        $output = $this->json->decode(curl_exec($ch));
        curl_close($ch);
        if($output['status']){
            $accessToken = $output['data']['access_token'];
        }
        
        return $accessToken;
    }

    
    function getClientRecordAry($accessToken){
        $url = $this->apiDomain."/".$this->apiVersion."/clients/view/".$this->schoolCode;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-Access-Token:'.$accessToken)
            );
        $output = $this->json->decode(curl_exec($ch));
        curl_close($ch);
        if($output['status']){
            $clientRecordAry = $output['data'];
            $clientRecordAry['system_used']=unserialize($clientRecordAry['system_used']);
            unset($clientRecordAry['system_used']['disk_used']);
        }
        
        return $clientRecordAry;
    }
    
    function logoutUser($accessToken){
        $url = $this->apiDomain."/".$this->apiVersion."/logout";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-Access-Token:'.$accessToken)
            );
        $output = $this->json->decode(curl_exec($ch));
        curl_close($ch);
    }
    
}
?>
