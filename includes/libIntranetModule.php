<?php

/** [Modification Log] Modifying By: Paul
 * *******************************************
 * *******************************************
 * 2019-05-13 Paul
 * 		Add quote to query having variable not quoted
 * 2015-08-05 Siuwan
 * 		modified function add_module_quota(), add field $isReturnID to return ModuleLicenseID instead of ModuleCode
 * 2014-08-30 Siuwan
 * 		add $showExpiryDateModuleAry to store module that display expiry date
 * 2013-10-08 Jason
 * 		- edit get_year_class_user() to fix the user list which is not order in class and class number 
 * 2013-04-18 Mick
 * 		- Fix Wrong field Student ID in SQL in check_module_student_exists
 * 2010-12-20 Henry Yuen
 * 		- added function get_deleted_users_count() to count number of deleted users, whose account are deleted from intranet
 * 2010-07-30 Max (201007301439)
 * 		- Added function updateModuleUserUsageStatus()
 * *******************************************
 */

if (!defined("LIBINTRANETMODULE_DEFINED")) // Preprocessor directives
	{
	define("LIBINTRANETMODULE_DEFINED", true);

	include_once("imConfig.inc.php");	// include the configuration file
	class libintranetmodule extends libdb {
		var $moduleId = NULL;
		var $showExpiryDateModuleAry = array("poemsandsongs","gvlistening","gvlistening2","gvlistening3","rsreading1","rsreading2","rsreading3","grammartouch1","grammartouch2","grammartouch3","grammartouch_s_2015","kisworksheets");
		function __construct() {
			$this->libdb();
		}
		function isModuleUser($ParModuleCode, $ParUserId) {
			$sql = "
			SELECT COUNT(*) AS NUMOFUSER FROM INTRANET_MODULE_USER IMU 
			INNER JOIN INTRANET_MODULE IM ON IMU.MODULEID = IM.MODULEID
			WHERE IM.CODE = '".$ParModuleCode."' AND IMU.USERID = '".$ParUserId."'
			";
			$resultVector = $this->returnVector($sql);
			return (($resultVector[0]>0)?true:false);
		}
		function GET_ADMIN_USER()
		{
			global $intranet_root;
	
			$lf = new libfilesystem();
			$AdminUser = trim($lf->file_read($intranet_root."/file/elibrary/admin_user.txt"));
	
			return $AdminUser;
		}
		function IS_ADMIN_USER($ParUserID)
		{
			global $intranet_root;
	
			$AdminUser = $this->GET_ADMIN_USER();
			
			$IsAdmin = 0;
			if(!empty($AdminUser))
			{
				$AdminArray = explode(",", $AdminUser);
				$IsAdmin = (in_array($ParUserID, $AdminArray)) ? 1 : 0;
			}
	
			return $IsAdmin;
		}
		function check_module_student_exists($ModuleID, $StudentID){
			$sql = "SELECT ModuleStudentID FROM INTRANET_MODULE_USER WHERE ModuleID='".$ModuleID."' AND UserID='".$StudentID."'";
			$rs = $this->returnVector($sql);
			
			if($rs[0]!="")
				return true;
			else
				return false;
		}		
		function add_module_student($ModuleID, $StudentID){
			
			if($ModuleID == "" ||$StudentID=="" )
				return;
				
			## Check student book record exits already ?
			if($this->check_module_student_exists($ModuleID, $StudentID))
				return true;
			
			## Insert book student record
			$sql = "INSERT INTO 
					INTRANET_MODULE_USER (ModuleID, UserID, InputDate)
					VALUES('$ModuleID','$StudentID', NOW())";				

			if ($this->db_db_query($sql)) {
				$insertedId = $this->db_insert_id();
				$sql = "SELECT IM.CODE, IM.DESCRIPTION FROM INTRANET_MODULE IM 
						INNER JOIN INTRANET_MODULE_USER IMU ON IM.MODULEID = IMU.MODULEID
						WHERE IMU.MODULESTUDENTID = '".$insertedId."'";

				$returnArray = $this->returnArray($sql);
				return $returnArray[0];
			} else {
				return;
			}
			
		}
		
		function delete_module_student($strModuleStudentID){
			if($strModuleStudentID=="")
				return;
				
			$sql = "DELETE FROM INTRANET_MODULE_USER WHERE ModuleStudentID IN (".$strModuleStudentID.")";		
			return $this->db_db_query($sql);
		}	
				
		function get_student_license_list($ModuleID){				
			$sql = "SELECT * FROM INTRANET_MODULE_USER WHERE ModuleID = '".$ModuleID."' ORDER BY InputDate";		
			return $this->returnArray($sql);
		}
		/**
		 * check wheather any Module license entry has been modified or not 
		 */
		function check_module_quota_is_original($ModuleID){
			$aryModule = $this->check_module_license_quota_is_original($ModuleID);
			$isValid = $aryModule[0];
			$quota = $aryModule[1];
			
			if(in_array(false, $aryModule)){
				
				return array(false, "");
			}
			
			return array(true, $quota);
		}
		/**
		 * check whether any quota has been compromised 
		 */
		function check_module_license_quota_is_original($ModuleID){
			$aryRs_isValid 	= array();
			$quota 	= 0;
			
			$sql = "SELECT * FROM INTRANET_MODULE_LICENSE WHERE MODULEID = '".$ModuleID."'";
			$aryBook = $this->returnArray($sql);
			
			foreach($aryBook as $i=>$data){
				$key = md5($data['ModuleID']."_".$data['NumberOfCopy']."_".$data['InputDate']);
				$aryRs_isValid[$data['ModuleLicenseID']] =($key == $data['CheckKey'])? "TRUE" : (($data['NumberOfCopy'] > 0)? $data['NumberOfCopy'] : "FALSE");
				
				$quota +=($key == $data['CheckKey'])? 0  : (($data['NumberOfCopy'] > 0)? $data['NumberOfCopy'] : 0);
			}
			
			return array($aryRs_isValid,$quota);
		}
		
			/**
			 * Get Available quota 
			 */
			function get_available_quota($ModuleID) {
	
				## Get Quota
				$quota = $this->get_module_quota($ModuleID);
	
				## Get number of students assigned to book
				$sql = "SELECT count(*) FROM INTRANET_MODULE_USER WHERE MODULEID = '" . $ModuleID . "' GROUP BY MODULEID";
				$aryRs2 = $this->returnVector($sql);
				$assigned = $aryRs2[0];
	
				$available = $quota - $assigned;
	
				$aryOriginal = $this->check_module_license_quota_is_original($ModuleID);
				foreach ($aryOriginal as $i => $orig_quota) {
					if ($orig_quota != "TRUE" && is_numeric($orig_quota)) {
						$invalid_quota += $orig_quota;
					}
				}
	
				$available = ($invalid_quota > $available) ? 0 : $available - $invalid_quota;
	
				return $available;
			}
		
		
			function get_student_with_name_license_list($ModuleID, $getSqlOnly="true", $ParSearchText=null){
				global $im_cfg,$Lang,$intranet_session_language;
			$name = getNameFieldWithLoginByLang("u.");
			
			$cond = " WHERE 
						imu.ModuleID = '".$ModuleID."' ";
			if (isset($ParSearchText)) {
				$ParSearchText = trim(mysql_real_escape_string($ParSearchText));
				if ($intranet_session_language == "en") {
					$searchName = "EnglishName";
				} else {
					$searchName = "ChineseName";
				}
				$cond .= " AND (imu.InputDate LIKE '%{$ParSearchText}%' OR 
							CONCAT(u.ClassName,' - ',u.ClassNumber) LIKE '%{$ParSearchText}%' OR
							u.$searchName LIKE '%{$ParSearchText}%')";
			}
			
			$sql = "SELECT 
						imu.InputDate,
						CONCAT(
							IF(u.ClassName IS NULL OR TRIM(u.ClassName) = '', '--', u.ClassName),
							' - ',
							IF(u.ClassNumber IS NULL OR TRIM(u.ClassNumber)= '', '--', u.ClassNumber)
						) as ClassInfo,					
						".$name." as Name,
						IF (
							DATE_ADD(imu.InputDate, INTERVAL 2 DAY) > NOW(),
							IF (
								imu.UsageStatus = {$im_cfg["DB_INTRANET_MODULE_USER_UsageStatus"]["Used"]},
								'{$Lang["IntranetModule"]["Using"]}',
								CONCAT('<input type=\"checkbox\" name=\"ModuleStudentID[]\" value=\"',imu.ModuleStudentID,'\"')
							),
							''
						)
					FROM 
						INTRANET_MODULE_USER as imu
					INNER JOIN 
						INTRANET_USER as u
					ON 
						imu.UserID = u.UserID 
					$cond
					";
			return ($getSqlOnly)? $sql : $this->returnArray($sql);
		}
		/**
		 * Get the maximum quota allowd for module
		 */
		function get_module_quota($ModuleID){
			
			$sql = "select
						SUM( iml.NumberOfLicense) as quota					
					FROM
						INTRANET_MODULE_LICENSE as iml
					WHERE 
						iml.ModuleID = '".$ModuleID."'";
			$aryRs = $this->returnVector($sql);
			$quota = $aryRs[0];
			
			return $quota;	
		}
		function get_year_class_user($YearClassID, $strUserID = "", $notIn = true) {
			global $intranet_session_language;
			if ($notIn)
				$condition = (!empty ($strUserID)) ? 'AND u.UserID NOT IN (' . $strUserID . ')' : "";
			else
				$condition = (!empty ($strUserID)) ? 'AND u.UserID IN (' . $strUserID . ')' : "";

			$sql = 'select
					  ycu.UserID,
					  yc.ClassTitleEN,
					  yc.ClassTitleB5,
					  ycu.ClassNumber 
					From
					  YEAR_CLASS_USER as ycu 
					  inner join 
					  INTRANET_USER as u 
					  on ycu.YearClassID = \'' . $YearClassID . '\' AND ycu.UserID = u.UserID ' . $condition . '
					 inner join 
					  YEAR_CLASS as yc 
					  on ycu.YearClassID = yc.YearClassID 
					order by ';
			if ($intranet_session_language == "en") {
				$sql .= "yc.ClassTitleEN, ycu.ClassNumber ";
			} else {
				$sql .= "yc.ClassTitleB5, ycu.ClassNumber ";
			}

			return $this->returnArray($sql);
		}
		function gen_year_class_user_ui($select_name = "StudentID", $YearClassID, $strUserID = "") {
			$hidden_list = "";
			$UserList = $this->get_year_class_user($YearClassID, $strUserID, true);
			$x .= '<select name="' . $select_name . '" id="' . $select_name . '" size="18" style="height:height:250px;width:280px" multiple="true">';
			for ($i = 0; $i < sizeof($UserList); $i++) {
				$TempUser = new libuser($UserList[$i]['UserID']);
				$tmpUsername = $TempUser->UserNameLang() . ' (' . Get_Lang_Selection($UserList[$i]['ClassTitleB5'], $UserList[$i]['ClassTitleEN']) . '-' . $UserList[$i]['ClassNumber'] . ')';

				$x .= '<option value="' . $UserList[$i]['UserID'] . '">';
				$x .= $tmpUsername;
				$x .= '</option>';
				$hidden_list .= '<input type="hidden" id="hidden_user_list_' . $UserList[$i]['UserID'] . '" value="' . $tmpUsername . '" />';
				unset ($TempUser);
			}
			$x .= '</select>';
			$x .= $hidden_list;
			return $x;
		}

		function get_studentID_license_list($ModuleID) {
			$sql = "SELECT UserID FROM INTRANET_MODULE_USER WHERE ModuleID = '" . $ModuleID . "' ORDER BY InputDate";
			return $this->returnVector($sql);
		}
		function add_module_quota_to_grammartouch($course_code, $module_license_id, $quota, $numberOfUnit=0, $numberOfStudents=0, $expiry_date='') {
			global $config_school_code,$plugin;
			if(isset($plugin['iTextbook_dev']) && $plugin['iTextbook_dev']){
				$api_path = "http://gt-dev.eclass.com.hk/";
			}
			elseif (isset($plugin['iTextbook']) && $plugin['iTextbook'])
			{
				$api_path = "http://gt.eclass.com.hk/";
			}
			$gt_ts = time();
			$gt_action = "add_module_quota";
			$gt_para = "ts=".$gt_ts."&action=".$gt_action."&school_code=".$config_school_code;
			$gt_para .= "&course_code=".$course_code."&module_license_id=".$module_license_id."&module_quota=".$quota."&number_of_unit=".$numberOfUnit."&number_of_students=".$numberOfStudents."&expiry_date=".strtotime($expiry_date);
			$gt_enc = encrypt_string($gt_para);
			$gt_ch = curl_init($api_path."gt_access.php?enc=".$gt_enc);
			curl_setopt($gt_ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($gt_ch, CURLOPT_HEADER, false);
			curl_setopt($gt_ch, CURLOPT_USERAGENT, "eClass");
			curl_setopt($gt_ch, CURLOPT_TIMEOUT_MS, 5000);
			curl_setopt($gt_ch, CURLOPT_FOLLOWLOCATION, true);
			$result = curl_exec($gt_ch);
			curl_close($gt_ch);
		}
		function add_module_quota($ModuleId, $quota, $numberOfUnit=0, $numberOfStudents=0, $expiryDate='', $isReturnID=false) {

			if ($ModuleId == "")
				return;

			$timestamp = date("Y-m-d H:i:s");
			$CheckKey = md5($ModuleId . "_" . $quota . "_" . $timestamp);
			$sql = "INSERT INTO 
									INTRANET_MODULE_LICENSE (MODULEID, NUMBEROFLICENSE, NUMBEROFUNIT, NUMBEROFSTUDENTS, EXPIRYDATE, CHECKKEY, INPUTDATE, DATEMODIFIED)
								VALUES 
									('$ModuleId', '$quota', '$numberOfUnit', '$numberOfStudents', ".(empty($expiryDate)? "NULL":"'$expiryDate'").", '$CheckKey', '$timestamp', '$timestamp')";

			if ($this->db_db_query($sql)) {
				$insertedId = $this->db_insert_id();
				$sql = "SELECT IM.CODE FROM INTRANET_MODULE IM 
							INNER JOIN INTRANET_MODULE_LICENSE IML ON IM.MODULEID = IML.MODULEID
							WHERE IML.MODULELICENSEID = '".$insertedId."'";
	
				$returnVector = $this->returnVector($sql);
					
				if($isReturnID){
					return array($insertedId,$returnVector[0]);
				}else{	
					return $returnVector[0];
				}
			} else {
				return;
			}
		}
		/**
		 * Given the list of success & failed updates/inserts , display the results.
		 * 
		 * $extra_info_header : set this field to display extra information. 
		 * $extra_info : must have $extra_info_header set to display extra info.
		 * 
		 *  Called in files:
		 * install_quota_import_update.php
		 * install_status_import_update.php
		 * ajax.php
		 * admin_book_license_assign_update.php
		 */
		function gen_result_ui($arySucc, $aryFail, $reload_parent = true, $tb_width = "100%") {
			global $eLib;

			$rs = "";

			$rs .= '<table width="'.$tb_width.(strpos($tb_width, '%')===false? 'px':'').'" height="490px"><tr><td>';
			//$rs .='<table width="100%" height="490px"><tr><td>';
			$rs .= '<div style="height:490px;background:white;">';

			########################################
			### Result UI
			$rs .= '<div style="height:464px;overflow-y:auto;">';
			//			debug_r($arySucc);
			## Success list UI
			if (count($arySucc) > 0) {
				$rs .= $this->get_ary_result_ui("Success", $arySucc);
			}

			## Fail list UI
			if (count($aryFail) > 0) {
				$rs .= $this->get_ary_result_ui("Fail", $aryFail);
			}

			$rs .= '</div>';

			########################################
			## Button panel UI
			$rs .= '<div id="btn_panel" style="height:40px;background:#EEE;text-align:center;">';
			$rs .= '<table width="100%" height="100%"><tr><td align="center" valign="center">';

			## Reload parent page + close thickbox OR Close thickbox only
			$close_js = ($reload_parent) ? 'window.parent.location.reload(); window.parent.tb_remove();' : 'window.parent.tb_remove();';

			$rs .= '<input type="button" name="close_btn" id="close_btn" value="' . $eLib["html"]["Close"] . '" onClick="' . $close_js . '" />';
			$rs .= '</td></tr><table>';
			$rs .= '</div>';
			########################################		

			$rs .= '</div>';
			$rs .= '</td></tr></table>';
			return $rs;
		}
		/**
		 *
		 */
		//function get_ary_result_ui($type, $FirstHeading, $aryRs, $extra_info_header, $extra_info){
		function get_ary_result_ui($type, $aryRs) {
			global $eLib;

			$bgColor = "#EEE";
			$rs = "";
			$curBg = "";

			$header_color = ($type == "Success") ? "green" : "red";
			$status = ($type == "Success") ? $eLib["html"]["Success"] : $eLib["html"]["Fail"];

			## Heading
			//$rs .= '<BR />';			
			$rs .= '<div style="height:45px;">';
			$rs .= '<table width="100%">';
			$rs .= '<tr style="font-weight:bold;"><td align="right">';
			$rs .= '<div class="systemmsg" style="width:100px;text-align:center;color:' . $header_color . ';">' . $status . '<div>';
			$rs .= '</td></tr>';
			$rs .= '<tr><td align="right">' . $eLib["html"]['Total'] . ': ' . count($aryRs) . '</td></tr></table>';
			$rs .= '</div>';

			## Main 
			$rs .= '<div id="display_content" style="">';
			$rs .= '<table width="100%" cellpadding="2" cellspacing="0">';

			$rs .= '<tr style="font-weight:bold;height:30px;background:#CCCCCC;">';
			$rs .= '<td width="5%">#</td>';

			$aryHeading = array_keys($aryRs[0]);
			$wdth = 70 / count($aryHeading);
			foreach ($aryHeading as $e => $info) {
				$rs .= '<td width="' . $wdth . '%">' . $info . '</td>';
			}
			$rs .= '</tr>';

			################################
			## List success		
			foreach ($aryRs as $i => $data) {
				$index = $i +1;
				/*$rs .= '<tr style="background:'.$curBg.';"><td>'.$index.'</td><td>'.$default_data.'</td>';
				
				## Display extra info , if any
				if(!empty($extra_info_header) && is_array($extra_info_header) && $extra_info_header != array()){					
					foreach($extra_info_header as $e=> $info){					
						$rs .= '<td>'.$extra_info[$e][$i].'</td>';
					}
				}*/
				$rs .= '<tr style="background:' . $curBg . ';">';
				$rs .= '<td>' . $index . '</td>';

				foreach ($data as $j => $info) {
					$rs .= '<td>' . $info . '</td>';
				}

				$rs .= '</tr>';
				$curBg = $curBg == "" ? $bgColor : "";
			}
			$rs .= '</table>';
			$rs .= '</div>';

			return $rs;
		}

		## GET FUNCTIONS
		function get_module_license_list($currUserID, $ParModuleCode="") {
			global $eLib, $i_QB_LangSelectEnglish, $i_QB_LangSelectChinese;
			
			
			$cond = !empty($ParModuleCode)?" WHERE im.Code = '$ParModuleCode' ":"";
			$sql = "
					SELECT
						im.Description,
						IF(tmp_iml.ct = '' or tmp_iml.ct IS NULL, '-', tmp_iml.ct) as assigned_quota,
						SUM( iml.NumberOfLicense) as quota,										
						iml.ModuleLicenseID,
						im.ModuleID				
					FROM
						INTRANET_MODULE as im
					LEFT JOIN 
						INTRANET_MODULE_LICENSE as iml
					ON
						im.ModuleID = iml.ModuleID
					LEFT JOIN 
						INTRANET_TEMP_ModuleLicense_{$currUserID} as tmp_iml
					ON
						iml.ModuleID = tmp_iml.ModuleID
					$cond
					GROUP BY 
					    im.ModuleID
				  ";
			return $sql;

		}
		
		# Henry Yuen 2010-12-21: get number of deleted users
		function get_deleted_users_count($ModuleID){
			$sql = "SELECT COUNT(*) from INTRANET_MODULE_USER M LEFT JOIN INTRANET_USER U ON M.UserID = U.UserID where ModuleID = '$ModuleID' AND U.UserID IS NULL";
			$r = $this->returnVector($sql);
			return $r[0];
		}		

		function create_tmp_module_license_count($currUserID) {
			$aryCount = array ();

			$sql = "CREATE TABLE INTRANET_TEMP_ModuleLicense_{$currUserID}
								SELECT MODULEID, count(*) as ct FROM INTRANET_MODULE_USER GROUP BY MODULEID";
			$this->db_db_query($sql);
		}

		function drop_tmp_module_license_count($currUserID) {
			$sql = "drop table IF EXISTS INTRANET_TEMP_ModuleLicense_{$currUserID}";
			$this->db_db_query($sql);
		}

		/**
		 * Count the number of students assigned to each book (Assigned Quota)
		 */
		function get_module_license_count() {
			$aryCount = array ();

			$sql = "SELECT ModuleLicenseID, count(*) as ct FROM INTRANET_BOOK_STUDENT GROUP BY ModuleLicenseID";
			$aryRs = $this->returnArray($sql);

				if ($aryRs == array ()) {
				return;
			}

			foreach ($aryRs as $i => $info) {
				$aryCount[$info['ModuleLicenseID']] = $info['ct'];
			}
			return $aryCount;
		}

		/**
		* GET MODULE ID BY CODE
		*
		* @param : String $code the code of the module , the code should be unique in INTRANET_MODULE 
		* @return : int , the Module ID of the request module code
		* 
		*/
		function getModuleID($code)
		{
			$sql = "select ModuleID from INTRANET_MODULE where Code = '{$code}'";
			$resultRs = $this->returnArray($sql);
			$moduleID = -9;
			if(sizeof($resultRs) == 1 && is_array($resultRs))
			{
				$moduleID = $resultRs[0]["ModuleID"];
			}
			return $moduleID;

		}
		
		/**
		 * Check the access right to module licence
		 */
		static public function checkModuleLicenceAccess($ModuleCode = '') {
			//suppose $ModuleCode must have value , but for compatible with old version, for safe , allow empty
			global $eLib;
			$isAllowAccess = false;

			if(strtolower($ModuleCode) == 'hidden'){
				//module code == 'hidden' , super admin , do not consider the code , allow to acces
				$isAllowAccess = true;
			}else{
				if($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"]) {
					if(strtolower($ModuleCode) == 'ies'){
						$isAllowAccess = true;	
					}		
				}

				if($_SESSION["SSV_USER_ACCESS"]["eLearning-SBA"]){
					if(strtolower($ModuleCode) == strtolower('SBA_IES')){
						$isAllowAccess = true;			
					}
				}
			}
			if(!$isAllowAccess)
			{
				//not allow access
				echo "<div style=\"padding: 30px 0 0 50px;height:100%;\">";
				echo "<BR /><BR /><BR /><BR />";
				echo $eLib['SystemMsg']['AccessDenied'];
				echo "<BR /><BR /><BR /><BR />";
				exit();
			}
		}
		
		static public function checkModuleLicenceAccessByModuleId($moduleId){
			global $intranet_db;

			$moduleCode = 'MODULE_NOT_EXIST';  // a dummy string to ensure the code does not exist
			if(intval($moduleId) > 0){
				$sql = "select Code as `Code` from ".$intranet_db.".INTRANET_MODULE where ModuleID = '".$moduleId."'";

				$objDB = new libdb();
				$rs = $objDB->returnResultSet($sql);
				if(is_array($rs) && count($rs) == 1){
					$rs = current($rs);
					$moduleCode = $rs['Code'];

				}
			}
			self::checkModuleLicenceAccess($moduleCode);
		}

		/**
		 * Update a user's usage status for intranet module
		 * @param String $ParModuleCode - the module code e.g. ies that for user to change usage status
		 * @param Array/INT $ParUserIdArray - User ID array or User ID that hope to change usage status
		 * @param INT $ParUsageStatus - the usage status that wish to be changed to, can see in config file in "includes"
		 * @return boolean - the result for the update
		 */
		public function updateModuleUserUsageStatus($ParModuleCode,$ParUserIdArray,$ParUsageStatus) {
			global $intranet_db;
			if (is_array($ParUserIdArray)) {
				// do nothing
			} else {
				$ParUserIdArray = array($ParUserIdArray);
			}
			$sql = "UPDATE {$intranet_db}.INTRANET_MODULE_USER IMU
					INNER JOIN {$intranet_db}.INTRANET_MODULE IM
					ON IMU.MODULEID = IM.MODULEID
					SET IMU.USAGESTATUS = '$ParUsageStatus'
					WHERE IMU.USERID IN ('".implode("','",$ParUserIdArray)."')
					AND IM.CODE = '$ParModuleCode'";
			$q_result = $this->db_db_query($sql);
			return $q_result;
		}

	}
}
?>