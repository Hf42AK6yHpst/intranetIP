<?php
// using: 
/**
 * Log :
 *
 * Date: 2019-07-30 [Tommy]
 * added export function in Get_Group_List_Table()
 * 
 * Date: 2019-07-08 [Tommy]
 *       add export function in Get_Group_Member_Form
 *
 * Date: 2018-02-22 [Cameron] hide not_allow_class_management_circulation [case #E129637]
 *
 * Date: 2015-06-05 [Henry]
 * added Get_Add_Class_Mgt_Group_Member_Form(), Get_Class_Mgt_User_Selection() and Get_Class_Mgt_Top_Management_Form()
 *
 * Date: 2014-11-27 [Ryan]
 * Modified Modified() to support option users return book at the same day
 *
 * Date: 2014-02-19 [Yuen]
 * Added Get_Group_Period() to support optional period for patron
 *
 * Date: 2013-08-29 [Ivan]
 * Modified Get_Group_Edit_Form() to add GroupCode edit function
 *
 * Date: 2013-08-22 [Henry]
 * Modified the Get_User_Selection() to fix bug of "Search User"
 *
 * Date: 2013-04-18 [Cameron]
 * Retrieve BarCode in function Get_Group_Member_Form($Group), show '--' if the field is empty
 */
if (! defined("LIBLIBRARYMGMT_GROUP_UI_DEFINED")) // Preprocessor directive
{
    define("LIBLIBRARYMGMT_GROUP_UI_DEFINED", true);
    
    if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

        function magicQuotes_awStripslashes(&$value, $key)
        {
            $value = stripslashes($value);
        }
        $gpc = array(
            &$_GET,
            &$_POST,
            &$_COOKIE,
            &$_REQUEST
        );
        if (function_exists("array_walk_recursive")) {
            array_walk_recursive($gpc, "magicQuotes_awStripslashes");
        }
    }
    
    include_once ("{$_SERVER['DOCUMENT_ROOT']}/includes/liblibrarymgmt.php");
    include_once ("{$_SERVER['DOCUMENT_ROOT']}/includes/liblibrarymgmt_group_manage.php");
    include_once ("{$_SERVER['DOCUMENT_ROOT']}/includes/form_class_manage.php");
    
    include_once ("{$_SERVER['DOCUMENT_ROOT']}/includes/pdump/pdump.php");
    include_once ("{$_SERVER['DOCUMENT_ROOT']}/lang/lang.$intranet_session_language.php");
    include_once ("{$_SERVER['DOCUMENT_ROOT']}/lang/libms.lang.$intranet_session_language.php");
    include_once ("{$_SERVER['DOCUMENT_ROOT']}/home/library_sys/default.inc.php");

    class liblibrarymgmt_group_manage_ui
    {

        function liblibrarymgmt_group_manage_ui()
        {}

        function Get_Identity_Manage()
        {
            global $Lang, $PATH_WRT_ROOT;
            
            // $GroupManage = new group_manage();
            $x = '<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.js"></script>
				    <script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.blockUI.js"></script>
				    <script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.jeditable.js"></script>
				    <script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.autocomplete.js"></script>

				    <link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.autocomplete.css" type="text/css" media="screen" />
				    <link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.css" type="text/css" media="screen" />';
            
            $x .= '<div id="IdentityManageLayer">';
            $x .= $this->Get_Group_List_Table_All();
            $x .= '</div>';
            
            $x .= '<div class="FakeLayer"></div>';
            
            return $x;
        }

        function Get_Group_List_Table_All()
        {
            global $Lang, $PATH_WRT_ROOT;
            
            include_once ($PATH_WRT_ROOT . '/includes/libinterface.php');
            $linterface = new interface_html();
            $x = '';
            $x = '<table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="main_content"><div class="content_top_tool">
		<div class="Conntent_search">
	        		<!--<input name="" type="text"/>-->
        		</div>
           	<br style="clear:both" />
		<div class="table_board">';
            
            $x .= $this->Get_Group_List_Table();
            $x .= '	</div>
			</td>
			</tr>
			</table>';
            
            return $x;
        }

        function Get_Group_Form()
        {
            global $Lang;
            
            $x = '<div class="edit_pop_board edit_pop_board_simple" style="height:180px;">
						<form id="GroupForm" name"GroupForm" onsubmit="return false;">
						<h1> ' . $Lang['libms']['GroupManagement']['ModuleTitle'] . ' &gt; <span>' . $Lang['libms']['GroupManagement']['NewGroup'] . ' </span> </h1>
						<div class="edit_pop_board_write" style="height:90px;">
							<table class="form_table">
							  <tr>
					        <td>' . $Lang['libms']['GroupManagement']['Identity'] . '</td>
							    <td>:</td>
							    <td >
							    	<select name="GroupType" id="GroupType">
								      <option value="">' . $Lang['libms']['GroupManagement']['SelectIdentity'] . '</option>
								      <option value="Teaching">' . $Lang['libms']['GroupManagement']['TeachingStaff'] . '</option>
								      <option value="NonTeaching">' . $Lang['libms']['GroupManagement']['SupportStaff'] . '</option>
								      <option value="Student">' . $Lang['libms']['GroupManagement']['Student'] . '</option>
								      <option value="Parent">' . $Lang['libms']['GroupManagement']['Parent'] . '</option>
					          </select>
					          <div id="IdentityWarningLayer" name="IdentityWarningLayer" style="display: none; color:red;"></div>
					         </td>
						      </tr>
								<col class="field_title" />
								<col  class="field_c" />
								<tr>
									<td>' . $Lang['libms']['GroupManagement']['GroupTitle'] . '</td>
									<td>:</td>
									<td >
										<input name="GroupName" type="text" id="GroupName" class="textbox" onkeyup="Check_Group_Name();"/>
										<div id="GroupNameWarningLayer" name="GroupNameWarningLayer" style="display: none; color:red;"></div>
									</td>
								</tr>
							</table>
						  <br />
						</div>
						<div class="edit_bottom">
							<span> </span>
							<p class="spacer"></p>
							<input id="SubmitGroupBtn" name="SubmitGroupBtn" type="button" class="formbutton" onclick="Save_Group(false);" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Submit'] . '" />
							<input id="SubmitEditGroupBtn" name="SubmitEditGroupBtn" type="button" class="formbutton" onclick="Save_Group(true);" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['libms']['GroupManagement']['SubmitAndEdit'] . '" />
						  <input id="CancelGroupBtn" name="CancelGroupBtn" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Cancel'] . '" />
						  <p class="spacer"></p>
						</div>
						</form>
					</div>';
            
            return $x;
        }

        function Get_Group_List_Table()
        {
            global $Lang;
            
            $GroupManage = new liblibrarymgmt_group_manage();
            
            $GroupList = $GroupManage->Get_Group_List();
            
            $x = '<form name="form1" method="post" action="">
            <div class="Conntent_tool">
            <a href="javascript:click_export_all();" class="export">'. $Lang['Btn']['Export'] . '</a></div></form>';
            
            $x .='<table class="common_table_list" id="GroupListTable">
						<thead>
							<tr>
								<th>' . $Lang['libms']['GroupManagement']['GroupTitle'] . '</th>
								<th>' . $Lang['libms']['GroupManagement']['GroupCode'] . '</th>
								<th>' . $Lang["libms"]["GroupManagement"]["UserList"] . '</th>
																<th>&nbsp;</th>
							 </tr>
						</thead>
						<tbody>';
            
            for ($i = 0; $i < sizeof($GroupList); $i ++) {
                $x .= '<tr>
					<td>
						<a href="#" onclick="Get_Group_Detail(\'' . $GroupList[$i]['GroupID'] . '\',\'Member\'); return false;">
						' . $GroupList[$i]['GroupTitle'] . '
						</a>
					</td>
					<td>
						' . $GroupList[$i]['GroupCode'] . '
					</td>
					<td>
						' . $GroupList[$i]['GroupMembers'] . '
					</td>
					<td>
						<span class="table_row_tool row_content_tool">
						<a href="#" class="edit_dim" title="' . $Lang['Btn']['Edit'] . '" onclick="Get_Group_Detail(\'' . $GroupList[$i]['GroupID'] . '\',\'Member\'); return false;"></a>
						<a href="#" class="delete_dim" title="' . $Lang['Btn']['Delete'] . '" onclick="Remove_Group(\'' . $GroupList[$i]['GroupID'] . '\'); return false;"></a>
						</span>
					</td>
				</tr>';
            }
            $x .= '
					<tr id="AddGroupRow">
						<td colspan="6">
							<div class="table_row_tool row_content_tool">
								<a href="#" class="add_dim" title="' . $Lang['libms']['GroupManagement']['NewGroup'] . '" onclick="Add_Form_Row(); return false;"></a>
							</div>
						</td>
					';
            $x .= '</tbody></table>';
            
            $x .= '<span class="tabletextrequire">*</span>' . $Lang["libms"]["GroupManagement"]["index"]["remark"];
            
            return $x;
        }

        // $Show: "RightTarget" or "Member"
        function Get_Group_Edit_Form($GroupID, $Show = "Member")
        {
            global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
            
            $Group = new liblibrarymgmt_group($GroupID, true, true, true);
            // dump($Group);
            // $GLOBALS['DEBUG'][] = $GroupID;
            // $GLOBALS['DEBUG'][] = $Group;
            // $GLOBALS['DEBUG'][] = $Lang['libms'];
            if ($Show == "")
                $Show = "TopManagement";
            
            $x = '<table width="99%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td class="main_content">
				  <div class="navigation">
				  	<img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/nav_arrow.gif" width="15" height="15" align="absmiddle" />
				  	<a href="#" onclick="Get_Group_List_Table(); return false;">' . $Lang['libms']['GroupManagement']['GroupList'] . '</a>
				  	<img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/nav_arrow.gif" width="15" height="15" align="absmiddle" />
				  	<span id="GroupNameNavLayer">' . $Group->GroupTitle . '</span>
				  	<br />
				  </div>
          <div class="detail_title_box">
          	<table width="100%">
          		<tr>
          			<td width="10%" valign="top">' . $Lang['libms']['GroupManagement']['GroupTitle'] . ':</td>
          			<td width="90%" align="left">
          			<strong>
          			<div id="' . $GroupID . '" onmouseover="Show_Edit_Icon(this);" onmouseout="Hide_Edit_Icon(this);" class="jEditInput">' . $Group->GroupTitle . '</div>
		          	</strong>
		          	<div id="GroupNameWarningLayer" style="display:none; color:red;"></div>
          			</td>
          		</tr>
				<tr>
          			<td width="20%" valign="top">' . $Lang['libms']['GroupManagement']['GroupCode'] . ':</td>
          			<td width="80%" align="left">
          			<strong>
          			<div id="groupCode_' . $GroupID . '" onmouseover="Show_Edit_Icon(this);" onmouseout="Hide_Edit_Icon(this);" class="jEditInputCode">' . $Group->GroupCode . '</div>
		          	</strong>
		          	<div id="GroupCodeWarningLayer" style="display:none; color:red;"></div>
          			</td>
          		</tr>
          	</table>
          </div>
					<br style="clear:both" />
					<div class="shadetabs">
					 <ul>';
            $Selected = ($Show == "Member") ? 'selected' : '';
            $x .= '  <li class="' . $Selected . ' SubMenu">
					   	<a href="#" onclick="$(\'li.selected\').removeClass(\'selected\'); this.parentNode.className=\'selected SubMenu\'; Switch_Layer(\'Member\'); return false;">' . $Lang['libms']['GroupManagement']['UserList'] . '</a>
					   </li>';
            $Selected = ($Show == "TopManagement") ? 'selected' : '';
            $x .= '   <li class="' . $Selected . ' SubMenu">
					   	<a href="#" onclick="$(\'li.selected\').removeClass(\'selected\'); this.parentNode.className=\'selected SubMenu\'; Switch_Layer(\'TopManagement\'); return false;">' . $Lang['libms']['GroupManagement']['TopManagement'] . '</a>
					   </li>';
            $Selected = ($Show == "GroupRule") ? 'selected' : '';
            $x .= '   <li class="' . $Selected . ' SubMenu">
					   	<a href="#" onclick="$(\'li.selected\').removeClass(\'selected\'); this.parentNode.className=\'selected SubMenu\'; Switch_Layer(\'GroupRule\'); return false;">' . $Lang['libms']['GroupManagement']['GroupRule'] . '</a>
					   </li>';
            $Selected = ($Show == "GroupPeriod") ? 'selected' : '';
            $x .= '   <li class="' . $Selected . ' SubMenu">
					   	<a href="#" onclick="$(\'li.selected\').removeClass(\'selected\'); this.parentNode.className=\'selected SubMenu\'; Switch_Layer(\'GroupPeriod\'); return false;">' . $Lang['libms']['GroupManagement']['GroupPeriod'] . '</a>
					   </li>';
            /*
             * $Selected = ($Show=="Target")? 'selected':'';
             * $x .= ' <li class="'.$Selected.' SubMenu">
             * <a href="#" onclick="$(\'li.selected\').removeClass(\'selected\'); this.parentNode.className=\'selected SubMenu\'; Switch_Layer(\'Target\'); return false;">'.$Lang['libms']['GroupManagement']['Targeting'].'</a>
             * </li>';
             * $x .= ' </ul>
             * //
             */
            $x .= '</div><br style="clear:both" />';
            
            // Member Layer
            $Display = ($Show == "Member") ? '' : 'style="display:none;"';
            $x .= '<div id="MemberListSubLayer" ' . $Display . '>';
            $x .= $this->Get_Group_Member_Form($Group);
            $x .= '</div>';
            
            // Top Management Layer
            $Display = ($Show == "TopManagement") ? '' : 'style="display:none;"';
            $x .= '<div id="TopManagementDetailSubLayer" ' . $Display . '>';
            $x .= $this->Get_Top_Management_Form($Group);
            $x .= '</div>';
            
            // Rulelayer
            
            $Display = ($Show == "GroupRule") ? '' : 'style="display:none;"';
            $x .= '<div id="RuleDetailSubLayer" ' . $Display . '>';
            $x .= $this->Get_Group_Rule($Group);
            $x .= '</div>';
            
            // PeriodLayer
            
            $Display = ($Show == "GroupPeriod") ? '' : 'style="display:none;"';
            $x .= '<div id="PeriodDetailSubLayer" ' . $Display . '>';
            $x .= $this->Get_Group_Period($Group);
            $x .= '</div>';
            
            $x .= '</td>
				</tr>
			</table>';
            // */
            
            return $x;
        }

        function Get_Add_Group_Member_Form($GroupID)
        {
            global $Lang;
            
            $GroupManage = new liblibrarymgmt_group_manage();
            $Group = new liblibrarymgmt_group($GroupID, false, true);
            $x = '<input type="hidden" name="GroupID" id="GroupID" value="' . $GroupID . '">
					<div class="edit_pop_board" style="height:440px;">
						<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
					    <table class="form_table">
					    	<tr><td>' . $Lang['libms']['GroupManagement']['Users'] . ': </td></tr>
							<tr>

							    <td >
							    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
				              <tr>
				              	<td>
				              		<table width="100%" border="0" cellspacing="0" cellpadding="5">
													<tr>
														<td width="50%" bgcolor="#EEEEEE">
														' . $Lang['libms']['GroupManagement']['UserType'] . ':
														<select name="IdentityType" id="IdentityType" onchange="Get_User_List(' . $GroupID . ');">
															<option value="Teaching">' . $Lang['libms']['GroupManagement']['TeachingStaff'] . '</option>
															<option value="NonTeaching">' . $Lang['libms']['GroupManagement']['SupportStaff'] . '</option>
															<option value="Student">' . $Lang['libms']['GroupManagement']['Student'] . '</option>
															<option value="Parent">' . $Lang['libms']['GroupManagement']['Parent'] . '</option>
														</select>
				';
            
            // get class list
            $CurrentYearTermID = getCurrentAcademicYearAndYearTerm();
            $CurrentAcademicYearID = $CurrentYearTermID['AcademicYearID'];
            $fcm = new form_class_manage();
            $YearClassList = $fcm->Get_Class_List_By_Academic_Year($CurrentAcademicYearID);
            $x .= '<select name="YearClassSelect" id="YearClassSelect" class="formtextbox" onchange="Get_User_List(' . $GroupID . ');" style="display:none;">';
            $x .= '<option value="">' . $Lang['libms']['SubjectClassMapping']['SelectClass'] . '</option>';
            for ($i = 0; $i < sizeof($YearClassList); $i ++) {
                if ($YearClassList[$i]['YearID'] != $YearClassList[$i - 1]['YearID']) {
                    if ($i == 0)
                        $x .= '<optgroup label="' . $YearClassList[$i]['YearName'] . '">';
                    else {
                        $x .= '</optgroup>';
                        $x .= '<optgroup label="' . $YearClassList[$i]['YearName'] . '">';
                    }
                }
                
                // $x .= '<option value="'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'">'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'</option>';
                $x .= '<option value="' . $YearClassList[$i]['ClassTitleEN'] . '">' . Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'], $YearClassList[$i]['ClassTitleEN']) . '</option>';
                
                if ($i == (sizeof($YearClassList) - 1))
                    $x .= '</optgroup>';
            }
            $x .= '</select>';
            
            $x .= '<div id="ParentStudentLayer" name="ParentStudentLayer">';
            $x .= '</div>';
            $x .= '
														</td>
													<td width="40"></td>
													<td width="50%" bgcolor="#EFFEE2" class="steptitletext">' . $Lang['libms']['GroupManagement']['SelectedUser'] . ' </td>
												</tr>
												<tr>
												<td bgcolor="#EEEEEE" align="center">';
            // get user list with selected criteria
            $x .= '							<div id="AvalUserLayer">';
            for ($i = 0; $i < sizeof($Group->MemberList); $i ++) {
                $AddUserID[] = $Group->MemberList[$i]['UserID'];
            }
            $x .= $this->Get_User_Selection($AddUserID, "Teaching", "", "", $GroupID);
            $x .= '
												</div>';
            
            $x .= '
												<span class="tabletextremark">' . $Lang['libms']['FormClassMapping']['CtrlMultiSelectMessage'] . ' </span>
												</td>
												<td><input name="AddAll" onclick="Add_All_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;&gt;" style="width:40px;" title="' . $Lang['Btn']['AddAll'] . '"/>
													<br />
													<input name="Add" onclick="Add_Selected_Class_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;" style="width:40px;" title="' . $Lang['Btn']['AddSelected'] . '"/>
													<br /><br />
													<input name="Remove" onclick="Remove_Selected_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;" style="width:40px;" title="' . $Lang['Btn']['RemoveSelected'] . '"/>
													<br />
													<input name="RemoveAll" onclick="Remove_All_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;&lt;" style="width:40px;" title="' . $Lang['Btn']['RemoveAll'] . '"/>
												</td>
												<td bgcolor="#EFFEE2" id="SelectedUserCell">';
            
            // selected User list
            $x .= '<select name="AddUserID[]" id="AddUserID[]" class="AddUserID" size="10" style="width:99%" multiple="true">';
            $x .= '</select>';
            $x .= '						</td>
											</tr>
											<tr>
												<td width="50%" bgcolor="#EEEEEE">';
            $x .= $Lang['libms']['FormClassMapping']['Or'] . '<Br>';
            $x .= $Lang['libms']['GroupManagement']['SearchUser'] . '<br>';
            
            $x .= '
												<!--<div class="Conntent_search" style="float:left;">-->
												<div style="float:left;">
													<input type="text" id="UserSearch" name="UserSearch" value=""/>
												</div>
											</td>
											<td>&nbsp;</td>
										</tr>
									</table>
									<p class="spacer"></p>
	              	</td>
	              </tr>
							</table>
						  <br />
						</td>
						</tr>
						</table>
						</div>
					  <div class="edit_bottom">
							<span> </span>
							<p class="spacer"></p>
							<input name="AddMemberSubmitBtn" id="AddMemberSubmitBtn" type="button" class="formbutton" onclick="Add_Group_Member(\'' . $Group->GroupID . '\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Add'] . '" />
						  <input name="AddMemberCancelBtn" id="AddMemberCancelBtn" type="button" class="formbutton" onclick="window.top.tb_remove(); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Cancel'] . '" />
					    <p class="spacer"></p>
						</div>
					</div>';
            
            return $x;
        }

        function Get_Add_Class_Mgt_Group_Member_Form($GroupID)
        {
            global $Lang;
            
            $GroupManage = new liblibrarymgmt_group_manage();
            
            // $Group = new liblibrarymgmt_group($GroupID,false,true);
            // -- get group member list
            $NameField = $Lang['libms']['SQL']['UserNameFeild'];
            
            // $ParentNameField = getParentNameWithStudentInfo("r.","u.","",1);
            // $StudentNameField = getNameFieldWithClassNumberByLang("u.");
            
            $sql = 'Select
						u.UserID,						
						u.' . $NameField . ' as MemberName,
						u.BarCode,
						u.UserType,
						u.ClassNumber,
						u.ClassName
						
					From
						LIBMS_CLASS_MANAGEMENT_GROUP_USER gu
					INNER JOIN
					    LIBMS_USER u
					ON
					    u.UserID = gu.UserID
					WHERE 
						GroupID = \'' . $GroupID . '\'
					AND
						u.RecordStatus = 1	
					ORDER BY u.ClassLevel, u.ClassName, u.ClassNumber
					';
            // debug_r($sql);
            $GroupMemberList = $GroupManage->returnArray($sql);
            // -- get group member list
            
            $x = '<input type="hidden" name="GroupID" id="GroupID" value="' . $GroupID . '">
					<div class="edit_pop_board" style="height:440px;">
						<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
					    <table class="form_table">
					    	<tr><td>' . $Lang['libms']['GroupManagement']['Users'] . ': </td></tr>
							<tr>

							    <td >
							    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
				              <tr>
				              	<td>
				              		<table width="100%" border="0" cellspacing="0" cellpadding="5">
													<tr>
														<td width="50%" bgcolor="#EEEEEE">
														' . $Lang['libms']['GroupManagement']['UserType'] . ':
														<select name="IdentityType" id="IdentityType" onchange="Get_User_List(' . $GroupID . ');">
															<option value="Teaching">' . $Lang['libms']['GroupManagement']['TeachingStaff'] . '</option>
															<option value="NonTeaching">' . $Lang['libms']['GroupManagement']['SupportStaff'] . '</option>
															<option value="Student">' . $Lang['libms']['GroupManagement']['Student'] . '</option>
															<option value="Parent">' . $Lang['libms']['GroupManagement']['Parent'] . '</option>
														</select>
				';
            
            // get class list
            $CurrentYearTermID = getCurrentAcademicYearAndYearTerm();
            $CurrentAcademicYearID = $CurrentYearTermID['AcademicYearID'];
            $fcm = new form_class_manage();
            $YearClassList = $fcm->Get_Class_List_By_Academic_Year($CurrentAcademicYearID);
            $x .= '<select name="YearClassSelect" id="YearClassSelect" class="formtextbox" onchange="Get_User_List(' . $GroupID . ');" style="display:none;">';
            $x .= '<option value="">' . $Lang['libms']['SubjectClassMapping']['SelectClass'] . '</option>';
            for ($i = 0; $i < sizeof($YearClassList); $i ++) {
                if ($YearClassList[$i]['YearID'] != $YearClassList[$i - 1]['YearID']) {
                    if ($i == 0)
                        $x .= '<optgroup label="' . $YearClassList[$i]['YearName'] . '">';
                    else {
                        $x .= '</optgroup>';
                        $x .= '<optgroup label="' . $YearClassList[$i]['YearName'] . '">';
                    }
                }
                
                // $x .= '<option value="'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'">'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'</option>';
                $x .= '<option value="' . $YearClassList[$i]['ClassTitleEN'] . '">' . Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'], $YearClassList[$i]['ClassTitleEN']) . '</option>';
                
                if ($i == (sizeof($YearClassList) - 1))
                    $x .= '</optgroup>';
            }
            $x .= '</select>';
            
            $x .= '<div id="ParentStudentLayer" name="ParentStudentLayer">';
            $x .= '</div>';
            $x .= '
														</td>
													<td width="40"></td>
													<td width="50%" bgcolor="#EFFEE2" class="steptitletext">' . $Lang['libms']['GroupManagement']['SelectedUser'] . ' </td>
												</tr>
												<tr>
												<td bgcolor="#EEEEEE" align="center">';
            // get user list with selected criteria
            $x .= '							<div id="AvalUserLayer">';
            for ($i = 0; $i < sizeof($GroupMemberList); $i ++) {
                $AddUserID[] = $GroupMemberList[$i]['UserID'];
            }
            $x .= $this->Get_Class_Mgt_User_Selection($AddUserID, "Teaching", "", "", $GroupID);
            $x .= '
												</div>';
            
            $x .= '
												<span class="tabletextremark">' . $Lang['libms']['FormClassMapping']['CtrlMultiSelectMessage'] . ' </span>
												</td>
												<td><input name="AddAll" onclick="Add_All_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;&gt;" style="width:40px;" title="' . $Lang['Btn']['AddAll'] . '"/>
													<br />
													<input name="Add" onclick="Add_Selected_Class_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;" style="width:40px;" title="' . $Lang['Btn']['AddSelected'] . '"/>
													<br /><br />
													<input name="Remove" onclick="Remove_Selected_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;" style="width:40px;" title="' . $Lang['Btn']['RemoveSelected'] . '"/>
													<br />
													<input name="RemoveAll" onclick="Remove_All_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;&lt;" style="width:40px;" title="' . $Lang['Btn']['RemoveAll'] . '"/>
												</td>
												<td bgcolor="#EFFEE2" id="SelectedUserCell">';
            
            // selected User list
            $x .= '<select name="AddUserID[]" id="AddUserID[]" class="AddUserID" size="10" style="width:99%" multiple="true">';
            
            for ($i = 0; $i < sizeof($GroupMemberList); $i ++) {
                $class_info = ($GroupMemberList[$i]['ClassName'] != "" && $GroupMemberList[$i]['ClassNumber'] != "") ? $GroupMemberList[$i]['ClassName'] . "-" . $GroupMemberList[$i]['ClassNumber'] . " " : "";
                $x .= '<option value="' . $GroupMemberList[$i]['UserID'] . '" >';
                $x .= $class_info . $GroupMemberList[$i]['MemberName'];
                $x .= '</option>';
            }
            
            $x .= '</select>';
            $x .= '						</td>
											</tr>
											<tr>
												<td width="50%" bgcolor="#EEEEEE">';
            $x .= $Lang['libms']['FormClassMapping']['Or'] . '<Br>';
            $x .= $Lang['libms']['GroupManagement']['SearchUser'] . '<br>';
            
            $x .= '
												<!--<div class="Conntent_search" style="float:left;">-->
												<div style="float:left;">
													<input type="text" id="UserSearch_' . $GroupID . '" name="UserSearch_' . $GroupID . '" value=""/>
												</div>
											</td>
											<td>&nbsp;</td>
										</tr>
									</table>
									<p class="spacer"></p>
	              	</td>
	              </tr>
							</table>
						  <br />
						</td>
						</tr>
						</table>
						</div>
					  <div class="edit_bottom">
							<span> </span>
							<p class="spacer"></p>
							<input name="AddMemberSubmitBtn" id="AddMemberSubmitBtn" type="button" class="formbutton" onclick="Add_Group_Member(\'' . $GroupID . '\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Submit'] . '" />
						  <input name="AddMemberCancelBtn" id="AddMemberCancelBtn" type="button" class="formbutton" onclick="window.top.tb_remove(); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Cancel'] . '" />
					    <p class="spacer"></p>
						</div>
					</div>';
            
            return $x;
        }

        function Get_Module_Manage()
        {}

        function Get_Group_Member_Form($Group)
        {
            global $Lang;
            
            $rm = new liblibrarymgmt_group_manage();
            
            $x = '<form name="form1" method="post" action="">
            <div class="content_top_tool">
            <div class="Conntent_tool">
            	<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Group_Add_Member_Form(\'' . $Group->GroupID . '\'); return false;" class="new thickbox" title="' . $Lang['libms']['GroupManagement']['AddUser'] . '"> ' . $Lang['libms']['GroupManagement']['New'] . ' </a>
            	<!--<a href="#" class="import"> Import</a> -->
            	<a href="javascript:click_export();" class="export">'. $Lang['Btn']['Export'] . '</a>
            </div>
            <div class="Conntent_search">
              <!--<input name="input" type="text"/>-->
            </div>
            <br style="clear:both" />
          </div>
          <div class="table_board">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td valign="bottom"><div class="table_filter"></div></td>
					    <td valign="bottom">
					    	<div class="common_table_tool">
					    		<a href="#" class="tool_delete" title="' . $Lang['libms']['GroupManagement']['Delete'] . '" onclick="Delete_Group_Member(\'' . $Group->GroupID . '\'); return false;"></a>
					    	</div>
					    </td>
					  </tr>
					</table>
					<table class="common_table_list">
					  <thead>
					    <tr>
					      <th class="num_check">#</th>
					      <th> ' . $Lang['libms']['GroupManagement']['Name'] . '</th> 
					      <th> ' . $Lang['libms']['CirculationManagement']['user_barcode'] . '</th>					      		
					      <th> ' . $Lang['libms']['GroupManagement']['ClassName'] . '</th>
					      <th> ' . $Lang['libms']['GroupManagement']['ClassNumber'] . '</th>
					      <th> ' . $Lang['libms']['GroupManagement']['UserType'] . '</th>
					      <th class="num_check" ><input type="checkbox" name="CheckAll" id="CheckAll" onclick="Set_Checkbox_Value(\'UserIDs[]\',this.checked);"/></th>
					      </tr>
					  </thead>
					  <tbody>';
            for ($i = 0; $i < sizeof($Group->MemberList); $i ++) {
                $MemberClassInfo = $rm->Get_Member_Class_Info($Group->MemberList[$i]['UserID'], $Group->MemberList[$i]['RecordType']);
                
                // // --> Updated by Cameron
                $x .= '		<tr>
						      <td>' . ($i + 1) . '</td> 
						      <td>' . ($Group->MemberList[$i]['MemberName'] ? $Group->MemberList[$i]['MemberName'] : '--') . '</td> 
						      <td>' . ($Group->MemberList[$i]['BarCode'] ? $Group->MemberList[$i]['BarCode'] : '--') . '</td>
						      <td>' . ($Group->MemberList[$i]['ClassName'] ? $Group->MemberList[$i]['ClassName'] : '--') . '</td>
						      <td>' . ($Group->MemberList[$i]['ClassNumber'] ? $Group->MemberList[$i]['ClassNumber'] : '--') . '</td>
						      <td>' . ($Group->MemberList[$i]['UserType'] ? $Group->MemberList[$i]['UserType'] : '--') . '</td>
						      <td><input type="checkbox" name="UserIDs[]" id="UserIDs[]" value="' . $Group->MemberList[$i]['UserID'] . '"/></td>
                              <input type="hidden" name="group" id="group" value="'. $Group->GroupID .'"/>
						    </tr>';
                
            }
            $x .= ' </tbody>
					</table>
					<p class="spacer"></p>
					  </div>
					<div class="edit_bottom">
						<!--<span> ' . Get_Last_Modified_Remark($Group->DateModified, $Group->ModifiedByName) . '</span>-->
						<p class="spacer"></p>
						<!--<input name="submit2" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Save" />-->
						<!--<input name="submit3" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Save As new" />-->
						<!--<input name="RemoveGroupBtn" id="RemoveGroupBtn" type="button" class="formbutton" onclick="Remove_Group(\'' . $Group->GroupID . '\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['libms']['GroupManagement']['RemoveThisGroup'] . '" />-->
						<!--<input name="submit2" type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'school_setting_subject_class.htm\');return document.MM_returnValue" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Cancel" />-->
						<p class="spacer"></p>
					</div></form>';
            
            return $x;
        }

        function Get_Group_Target_Form($Group)
        {
            global $Lang, $special_feature;
            
            $TargetList = $Group->TargetList;
            $NotSetBefore = (! isset($TargetList['All-Yes']) && ! isset($TargetList['All-No']));
            
            $x = '<div class="table_board">
					' . $Lang['libms']['GroupManagement']['ToAllUser'];
            
            $DetailTargetUI .= '     <tr class="sub_row">
						      								<td align="left">' . $Lang['libms']['GroupManagement']['ToAll'] . '</td>';
            $TeachingAllChecked = ($TargetList['Staff-AllTeaching']['RightFlag'] || $NotSetBefore) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-AllTeaching" id="TeachingAll" onclick="Toggle_Teaching_Options(\'Teaching\',this.checked);" ' . $TeachingAllChecked . '></td>';
            $NonTeachingAllChecked = ($TargetList['Staff-AllNonTeaching']['RightFlag'] || $NotSetBefore) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-AllNonTeaching" id="NonTeachingAll" onclick="Toggle_Teaching_Options(\'NonTeaching\',this.checked);" ' . $NonTeachingAllChecked . '></td>';
            $StudentAllChecked = ($TargetList['Student-All']['RightFlag'] || $NotSetBefore) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Student-All" id="StudentAll" onclick="Toggle_Teaching_Options(\'Student\',this.checked);" ' . $StudentAllChecked . '></td>';
            $ParentAllChecked = ($TargetList['Parent-All']['RightFlag'] || $NotSetBefore) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Parent-All" id="ParentAll" onclick="Toggle_Teaching_Options(\'Parent\',this.checked);" ' . $ParentAllChecked . '></td>';
            if ($special_feature['alumni']) {
                $AlumniAllChecked = ($TargetList['Alumni-All']['RightFlag'] || $NotSetBefore) ? 'checked="checked"' : '';
                $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Alumni-All" id="AlumniAll" onclick="Toggle_Teaching_Options(\'Alumni\',this.checked);" ' . $AlumniAllChecked . '></td>';
                
                $DetailTargetDisplay = ($TeachingAllChecked != '' && $StudentAllChecked != '' && $ParentAllChecked != '' && $AlumniAllChecked != '') ? 'style="display:none;"' : '';
            } else {
                $DetailTargetDisplay = ($TeachingAllChecked != '' && $StudentAllChecked != '' && $ParentAllChecked != '') ? 'style="display:none;"' : '';
            }
            $TeachingDetailDisplay = ($TeachingAllChecked != '') ? 'style="display:none;"' : '';
            $NonTeachingDetailDisplay = ($NonTeachingAllChecked != '') ? 'style="display:none;"' : '';
            $StudentDetailDisplay = ($StudentAllChecked != '') ? 'style="display:none;"' : '';
            $ParentDetailDisplay = ($ParentAllChecked != '') ? 'style="display:none;"' : '';
            if ($special_feature['alumni'])
                $AlumniDetailDisplay = ($AlumniAllChecked != '') ? 'style="display:none;"' : '';
            
            $DetailTargetUI .= '     </tr>
					      <tbody id="SubTargetRow" ' . $DetailTargetDisplay . '>
					      <tr class="sub_row">
					      	<td align="left">' . $Lang['libms']['GroupManagement']['BelongToSameForm'] . '</td>';
            $StaffMyFormChecked = ($TargetList['Staff-MyForm']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-MyForm" class="TeachingTargetOption" ' . $TeachingDetailDisplay . ' ' . $StaffMyFormChecked . '>&nbsp;</td>
					      	<td>&nbsp;</td>';
            $StudentMyFormChecked = ($TargetList['Student-MyForm']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Student-MyForm" class="StudentTargetOption" ' . $StudentDetailDisplay . ' ' . $StudentMyFormChecked . '>&nbsp;</td>';
            $ParentMyFormChecked = ($TargetList['Parent-MyForm']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Parent-MyForm" class="ParentTargetOption" ' . $ParentDetailDisplay . ' ' . $ParentMyFormChecked . '>&nbsp;</td>';
            if ($special_feature['alumni'])
                $DetailTargetUI .= '<td>&nbsp;</td>';
            $DetailTargetUI .= '</tr>';
            $DetailTargetUI .= '<tr class="sub_row">
					      	<td align="left">' . $Lang['libms']['GroupManagement']['BelongToSameFormClass'] . '</td>';
            $StaffMyClassChecked = ($TargetList['Staff-MyClass']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-MyClass" class="TeachingTargetOption" ' . $TeachingDetailDisplay . ' ' . $StaffMyClassChecked . '>&nbsp;</td>
					      	<td>&nbsp;</td>';
            $StudentMyClassChecked = ($TargetList['Student-MyClass']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Student-MyClass" class="StudentTargetOption" ' . $StudentDetailDisplay . ' ' . $StudentMyClassChecked . '>&nbsp;</td>';
            $ParentMyClassChecked = ($TargetList['Parent-MyClass']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Parent-MyClass" class="ParentTargetOption" ' . $ParentDetailDisplay . ' ' . $ParentMyClassChecked . '>&nbsp;</td>';
            if ($special_feature['alumni'])
                $DetailTargetUI .= '<td>&nbsp;</td>';
            $DetailTargetUI .= '</tr>
					      <tr class="sub_row">
					      	<td align="left">' . $Lang['libms']['GroupManagement']['BelongToSameSubject'] . '</td>';
            $StaffMySubjectChecked = ($TargetList['Staff-MySubject']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-MySubject" class="TeachingTargetOption" ' . $TeachingDetailDisplay . ' ' . $StaffMySubjectChecked . '>&nbsp;</td>
					      	<td>&nbsp;</td>';
            $StudentMySubjectChecked = ($TargetList['Student-MySubject']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Student-MySubject" class="StudentTargetOption" ' . $StudentDetailDisplay . ' ' . $StudentMySubjectChecked . '>&nbsp;</td>';
            $ParentMySubjectChecked = ($TargetList['Parent-MySubject']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Parent-MySubject" class="ParentTargetOption" ' . $ParentDetailDisplay . ' ' . $ParentMySubjectChecked . '>&nbsp;</td>';
            if ($special_feature['alumni'])
                $DetailTargetUI .= '<td>&nbsp;</td>';
            $DetailTargetUI .= '</tr>
					      <tr class="sub_row">
					      	<td align="left">' . $Lang['libms']['GroupManagement']['BelongToSameSubjectGroup'] . '</td>';
            $StaffMySubjectGroupChecked = ($TargetList['Staff-MySubjectGroup']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-MySubjectGroup" class="TeachingTargetOption" ' . $TeachingDetailDisplay . ' ' . $StaffMySubjectGroupChecked . '>&nbsp;</td>
					      	<td>&nbsp;</td>';
            $StudentMySubjectGroupChecked = ($TargetList['Student-MySubjectGroup']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Student-MySubjectGroup" class="StudentTargetOption" ' . $StudentDetailDisplay . ' ' . $StudentMySubjectGroupChecked . '>&nbsp;</td>';
            $ParentMySubjectGroupChecked = ($TargetList['Parent-MySubjectGroup']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Parent-MySubjectGroup" class="ParentTargetOption" ' . $ParentDetailDisplay . ' ' . $ParentMySubjectGroupChecked . '>&nbsp;</td>';
            if ($special_feature['alumni'])
                $DetailTargetUI .= '<td>&nbsp;</td>';
            // $AlumniMySubjectGroupChecked = ($TargetList['Alumni-MySubjectGroup']['RightFlag'])? 'checked="checked"':'';
            // $DetailTargetUI .= ' <td><input type="checkbox" name="Target[]" value="Alumni-MySubjectGroup" class="AlumniTargetOption" '.$AlumniDetailDisplay.' '.$AlumniMySubjectGroupChecked.'>&nbsp;</td>';
            $DetailTargetUI .= '</tr>
					      <tr class="sub_row">
					      	<td align="left">' . $Lang['libms']['GroupManagement']['BelongToSameGroup'] . '</td>';
            $StaffMyGroupChecked = ($TargetList['Staff-MyGroup']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-MyGroup" class="TeachingTargetOption" ' . $TeachingDetailDisplay . ' ' . $StaffMyGroupChecked . '>&nbsp;</td>';
            $NonTeachingMyGroupChecked = ($TargetList['NonTeaching-MyGroup']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="NonTeaching-MyGroup" class="NonTeachingTargetOption" ' . $NonTeachingDetailDisplay . ' ' . $NonTeachingMyGroupChecked . '>&nbsp;</td>';
            $StudentMyGroupChecked = ($TargetList['Student-MyGroup']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Student-MyGroup" class="StudentTargetOption" ' . $StudentDetailDisplay . ' ' . $StudentMyGroupChecked . '>&nbsp;</td>';
            $ParentMyGroupChecked = ($TargetList['Parent-MyGroup']['RightFlag']) ? 'checked="checked"' : '';
            $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Parent-MyGroup" class="ParentTargetOption" ' . $ParentDetailDisplay . ' ' . $ParentMyGroupChecked . '>&nbsp;</td>';
            if ($special_feature['alumni']) {
                $AlumniMyGroupChecked = ($TargetList['Alumni-MyGroup']['RightFlag']) ? 'checked="checked"' : '';
                $DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Alumni-MyGroup" class="AlumniTargetOption" ' . $AlumniDetailDisplay . ' ' . $AlumniMyGroupChecked . '>&nbsp;</td>';
            }
            $DetailTargetUI .= '</tr>
					      ';
            
            $AlumniMyGroupCheckedNotNULL = true;
            if ($special_feature['alumni']) {
                $AlumniMyGroupCheckedNotNULL = $AlumniMyGroupChecked != '';
            }
            $AllChecked = (($TeachingAllChecked != '' && $NonTeachingAllChecked != '' && $StudentAllChecked != '' && $ParentAllChecked != '' && $AlumniAllChecked != '' && $StaffMyFormChecked != '' && $StudentMyFormChecked != '' && $ParentMyFormChecked != '' && 
            // $AlumniMyFormChecked != '' &&
            $StaffMyClassChecked != '' && $StudentMyClassChecked != '' && $ParentMyClassChecked != '' && 
            // $AlumniMyClassChecked != '' &&
            $StaffMySubjectChecked != '' && $StudentMySubjectChecked != '' && $ParentMySubjectChecked != '' && 
            // $AlumniMySubjectChecked != '' &&
            $StaffMySubjectGroupChecked != '' && $StudentMySubjectGroupChecked != '' && $ParentMySubjectGroupChecked != '' && 
            // $AlumniMySubjectGroupChecked != '' &&
            $StaffMyGroupChecked != '' && $NonTeachingMyGroupChecked != '' && $StudentMyGroupChecked != '' && $ParentMyGroupChecked != '' && $AlumniMyGroupCheckedNotNULL) || $NotSetBefore || $TargetList['All-Yes']['RightFlag']) ? 'checked="checked"' : '';
            $AllNoChecked = ($TargetList['All-No']['RightFlag']) ? 'checked="checked"' : '';
            $HeaderUI .= '	<input type="Radio" name="Target[]" id="TargetAll" value="All-Yes" ' . $AllChecked . ' onclick="$(\'div#TargetTable\').hide(\'slow\');"> <label for="TargetAll">' . $Lang['General']['Yes'] . '</label>
						<input type="Radio" name="Target[]" id="TargetNone" value="All-No" ' . $AllNoChecked . ' onclick="$(\'div#TargetTable\').show(\'slow\');"> <label for="TargetNone">' . $Lang['General']['No'] . '</label>';
            
            $TableHeaderDisplay = ($AllChecked != '') ? 'display:none;' : '';
            $HeaderUI .= '	<div id="TargetTable" style="width:100%; ' . $TableHeaderDisplay . '">
					 <table width="100%" border="0" id="TargetTable">
					  <tr>
					    <td width="75%" valign="top"><table class="common_table_list">
					    <col /><col class="num_check" />
					      <tr>
					        <th width="65%">' . $Lang['libms']['GroupManagement']['CanSendTo'] . '</th>
					        <th width="7%">' . $Lang['libms']['GroupManagement']['TeachingStaff'] . '</th>
									<th width="7%">' . $Lang['libms']['GroupManagement']['SupportStaff'] . '</th>
									<th width="7%">' . $Lang['libms']['GroupManagement']['Student'] . '</th>
									<th width="7%">' . $Lang['libms']['GroupManagement']['Parent'] . '</th>';
            if ($special_feature['alumni'])
                $HeaderUI .= '<th width="7%">' . $Lang['libms']['GroupManagement']['Alumni'] . '</th>';
            $HeaderUI .= '</tr>';
            
            $x .= $HeaderUI . $DetailTargetUI;
            
            $x .= '
					      </tbody>
					    </table></td>
					  </tr>
					 </table>
					 </div>
					<p class="spacer"></p>
					</div>
					<div class="edit_bottom">
						<!--<span> ' . Get_Last_Modified_Remark($Group->DateModified, $Group->ModifiedByName) . '</span>-->
						<p class="spacer" style="text-align:left"></p>
						<div class="tabletext" style="color:red; float:left;">*</div><div class="tabletext" style="float:left;">' . $Lang['libms']['GroupManagement']['UncheckForMoreOptions'] . '</div>
						<input name="SaveGroupBtn" id="SaveGroupBtn" type="button" class="formbutton" onclick="Save_Group_Right_Target(\'' . $Group->GroupID . '\',\'Target\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Save'] . '" />
						<!--<input name="submit3" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Save As new" />-->
						<!--<input name="RemoveGroupBtn" id="RemoveGroupBtn" type="button" class="formbutton" onclick="Remove_Group(\'' . $Group->GroupID . '\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['libms']['GroupManagement']['RemoveThisGroup'] . '" />-->
						<!--<input name="GroupDetailCancelBtn" id="GroupDetailCancelBtn" type="button" class="formbutton" onclick="Get_Group_List_Table();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Cancel'] . '" />-->
						<p class="spacer"></p>
					</div>';
            
            return $x;
        }

        function Get_Group_Period($Group)
        {
            global $Lang, $libms_default, $linterface, $PATH_WRT_ROOT;
            
            $GroupManage = new liblibrarymgmt_group_manage();
            
            $GroupPeriodObj = $GroupManage->Get_Group_Period_Date($Group->GroupID);
            
            if (! $linterface) {
                include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
                $linterface = new interface_html();
            }
            
            $checked = $GroupPeriodObj['SameDayReturnNotAllowed'] ? 'checked' : '';
            // $UserRightTarget = new user_right_target();
            $x = '<div class="table_board"><table width="100%" border="0">
		<tr>
			<td width="85%"><table class="form_table_v30">
				<tr valign="top">
					<td class="field_title">' . $Lang['libms']['S_OpenTime']['SameDayReturn'] . '</td>
					<td><input type="checkbox" name="SameDayReturn" id="SameDayReturn" ' . $checked . '> </td>
				</tr>
				<tr valign="top">
					<td class="field_title">' . $Lang['libms']['S_OpenTime']['DateFrom'] . '</td>
					<td >' . $linterface->GET_DATE_PICKER("PeriodStartDate", $GroupPeriodObj["StartDate"], $OtherMember = "", $DateFormat = "yy-mm-dd", $MaskFunction = "", $ExtWarningLayerID = "", $ExtWarningLayerContainer = "", $OnDatePickSelectedFunction = "", $ID = "", $SkipIncludeJS = 0, $CanEmptyField = 1) . '</td>
				</tr>
				<tr valign="top">
					<td class="field_title">' . $Lang['libms']['S_OpenTime']['DateEnd'] . '</td>
					<td>' . $linterface->GET_DATE_PICKER("PeriodEndDate", $GroupPeriodObj["EndDate"], $OtherMember = "", $DateFormat = "yy-mm-dd", $MaskFunction = "", $ExtWarningLayerID = "", $ExtWarningLayerContainer = "", $OnDatePickSelectedFunction = "", $ID = "", $SkipIncludeJS = 0, $CanEmptyField = 1) . '</td>
				</tr>
				</table>
					<span class="tabletextrequire">*</span>' . $Lang["libms"]["open_time"]["system_period_remark"] . '
			</td>
		</tr>
		</table>
		<p class="spacer"></p>
				</div>
				<div class="edit_bottom">
					<p class="spacer"></p>
					<input name="SaveGroupBtn" id="SaveGroupBtn" type="button" class="formbutton" onclick="Save_Group_Period(\'' . $Group->GroupID . '\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Save'] . '" />					
					<p class="spacer"></p>
				</div>';
            return $x;
        }

        function Get_Group_Rule($Group)
        {
            global $Lang, $libms_default;
            
            // $UserRightTarget = new user_right_target();
            $x = '<div class="table_board">
					 <table width="100%" border="0">
					  <tr>
					    <td width="75%" valign="top">
					    <!--' . $Lang['libms']['GroupManagement']['GroupRule'] . '-->' . '<p>' . $Lang["libms"]["GroupManagement"]["Rule"]["policy_remark"] . '</p>' . '</td>
					  </tr>
					  
					  
					  
					 <tr>
					    <td width="75%" valign="top">
					    <table class="common_table_list">
					      <tr>
					      <th>' . $Lang['libms']['GroupManagement']['Rule']['CirDescription'] . '</th>
					      <th>' . $Lang['libms']['GroupManagement']['Rule']['LimitBorrow'] . '</th>
					      <th>' . $Lang['libms']['GroupManagement']['Rule']['LimitRenew'] . '</th>
					      <th>' . $Lang['libms']['GroupManagement']['Rule']['LimitReserve'] . '</th>
					      <th>' . $Lang['libms']['GroupManagement']['Rule']['ReturnDuration'] . '</th>
					      <th>' . $Lang['libms']['GroupManagement']['Rule']['OverDueCharge'] . '</th>
					      <th>' . $Lang['libms']['GroupManagement']['Rule']['MaxFine'] . '</th>
					      </tr>
					      <tbody>';
            $FontStyle = '';
            
            foreach ($Group->RuleList as $row) {
                if ($row['CirculationTypeCode'] = '-1Def')
                    $default_rule = $row;
                break;
            }
            // $x .= "<tr><td colspan=99></td></tr>";
            $x .= "<tr>
			<td>{$Lang['libms']['GroupManagement']['DefaultRule']}<input name='rule[-1Def][isset]' value='1' type='hidden'/></td>
			<td><input name='rule[-1Def][LimitBorrow]' value='{$default_rule['LimitBorrow']}' type='text' /></td>
			<td><input name='rule[-1Def][LimitRenew]' value='{$default_rule['LimitRenew']}' type='text' /></td>
			<td><input name='rule[-1Def][LimitReserve]' value='{$default_rule['LimitReserve']}' type='text' /></td>
			<td><input name='rule[-1Def][ReturnDuration]' value='{$default_rule['ReturnDuration']}' type='text' /></td>
			<td><input name='rule[-1Def][OverDueCharge]' value='{$default_rule['OverDueCharge']}' type='text' /></td>
			<td><input name='rule[-1Def][MaxFine]' value='{$default_rule['MaxFine']}' type='text' /></td>
			</tr>";
            $x .= "<tr><td colspan=99></td></tr>";
            
            if (! empty($Group->RuleList))
                foreach ($Group->RuleList as $row) {
                    if ($row['CirculationTypeCode'] != '-1Def') {
                        $x .= "<tr>
				<td>{$row['CirDescription']}<input  name='rule[{$row['CirculationTypeCode']}][isset]' type='checkbox' onClick=\"toggleRuleRow(this,'{$row['CirculationTypeCode']}')\" checked /></td>
				<td class='toggleable{$row['CirculationTypeCode']}' ><input name='rule[{$row['CirculationTypeCode']}][LimitBorrow]' value='{$row['LimitBorrow']}' type='text' /></td>
				<td class='toggleable{$row['CirculationTypeCode']}' ><input name='rule[{$row['CirculationTypeCode']}][LimitRenew]' value='{$row['LimitRenew']}' type='text' /></td>
				<td class='toggleable{$row['CirculationTypeCode']}' ><input name='rule[{$row['CirculationTypeCode']}][LimitReserve]' value='{$row['LimitReserve']}' type='text' /></td>
				<td class='toggleable{$row['CirculationTypeCode']}' ><input name='rule[{$row['CirculationTypeCode']}][ReturnDuration]' value='{$row['ReturnDuration']}' type='text' /></td>
				<td class='toggleable{$row['CirculationTypeCode']}' ><input name='rule[{$row['CirculationTypeCode']}][OverDueCharge]' value='{$row['OverDueCharge']}' type='text' /></td>
				<td class='toggleable{$row['CirculationTypeCode']}' ><input name='rule[{$row['CirculationTypeCode']}][MaxFine]' value='{$row['MaxFine']}' type='text' /></td>
				</tr>";
                        $included[] = $row['CirculationTypeCode'];
                    }
                }
            $libms = new liblms();
            $cir_list = $libms->GET_CIRCULATION_TYPE_LIST($included);
            
            if (! empty($cir_list))
                foreach ($cir_list as $row) {
                    $x .= "<tr>
			    <td>{$row['CirDescription']}<input name='rule[{$row['CirculationTypeCode']}][isset]' value='1' type='checkbox' onClick=\"toggleRuleRow(this,'{$row['CirculationTypeCode']}')\" /></td>
			    <td class='toggleable{$row['CirculationTypeCode']}' style='display :none;'><input name='rule[{$row['CirculationTypeCode']}][LimitBorrow]' value='' type='text' /></td>
			    <td class='toggleable{$row['CirculationTypeCode']}' style='display :none;'><input name='rule[{$row['CirculationTypeCode']}][LimitRenew]' value='' type='text' /></td>
			    <td class='toggleable{$row['CirculationTypeCode']}' style='display :none;'><input name='rule[{$row['CirculationTypeCode']}][LimitReserve]' value='' type='text' /></td>
			    <td class='toggleable{$row['CirculationTypeCode']}' style='display :none;'><input name='rule[{$row['CirculationTypeCode']}][ReturnDuration]' value='' type='text' /></td>
			    <td class='toggleable{$row['CirculationTypeCode']}' style='display :none;'><input name='rule[{$row['CirculationTypeCode']}][OverDueCharge]' value='' type='text' /></td>
			    <td class='toggleable{$row['CirculationTypeCode']}' style='display :none;'><input name='rule[{$row['CirculationTypeCode']}][MaxFine]' value='' type='text' /></td>
			    </tr>";
                }
            
            $x .= '   </tbody>
					    </table>
					   </td>
					 </tr>
					</table>
					<p class="spacer"></p>
				</div>
				<div class="edit_bottom">
					<!--<span> ' . Get_Last_Modified_Remark($Group->DateModified, $Group->ModifiedByName) . '</span>-->
					<p class="spacer"></p>
					<input name="SaveGroupBtn" id="SaveGroupBtn" type="button" class="formbutton" onclick="Save_Group_Rule(\'' . $Group->GroupID . '\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Save'] . '" />
					<!--<input name="submit3" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Save As new" />-->
					<!--<input name="RemoveGroupBtn" id="RemoveGroupBtn" type="button" class="formbutton" onclick="Remove_Group(\'' . $Group->GroupID . '\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['libms']['GroupManagement']['RemoveThisGroup'] . '" />-->
					<!--<input name="GroupDetailCancelBtn" id="GroupDetailCancelBtn" type="button" class="formbutton" onclick="Get_Group_List_Table();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Cancel'] . '" />-->
					<p class="spacer"></p>
				</div>';
            return $x;
        }

        function Get_Top_Management_Form($Group)
        {
            global $Lang, $libms_default;
            
            // $UserRightTarget = new user_right_target();
            $x = '<div class="table_board">
					 <table width="100%" border="0">
					  <tr>
					    <td width="75%" valign="top">
					    <table class="common_table_list">
					    	<col /><col class="num_check" />
					      <tr>
					        <th>' . $Lang['libms']['GroupManagement']['AccessRight'] . '</th>
					        <th>&nbsp;</th>
					      </tr>
					      <tbody>';
            // $EffectiveRightList = $libms_default['group_right'];
            $DBRightRightList = $Group->RightList;
            foreach ($libms_default['group_right'] as $section => $Functions)
                foreach ($Functions as $function => $default_rights) {
                    // If(!isset($EffectiveRightList[$section][$function])){
                    $EffectiveRightList[$section][$function] = false;
                }
            
            foreach ($DBRightRightList as $Right)
                $EffectiveRightList[$Right['Section']][$Right['Function']] = $Right['IsAllow'];
            
            $FontStyle = '';
            foreach ($EffectiveRightList as $section => $Functions) {
                $x .= "<tr><td colspan=99 style='background-color:#ffff00'>{$Lang['libms']['GroupManagement']['Right'][$section]['name']}</td></tr>";
                foreach ($Functions as $function => $right) {
                    $checked = ($right) ? 'checked="checked"' : '';
                    if ($Lang['libms']['GroupManagement']['Right'][$section][$function]) {
                        $x .= '<tr class=\'group_right\'>
						  <td align="left" class="sub_function_row" ' . $FontStyle . '><label for="' . $function . '">' . $Lang['libms']['GroupManagement']['Right'][$section][$function] . '</label></td>
						  <td><input onclick="Swap_Style(this);" name="Right[]" id="' . $section . '/' . $function . '" value="' . $section . '/' . $function . '" type="checkbox" ' . $checked . ' /></td>
						</tr>';
                    }
                }
            }
            
            $x .= '   </tbody>
					    </table>
					   </td>
					 </tr>
					</table>
					<p class="spacer"></p>
				</div>
				<div class="edit_bottom">
					<!--<span> ' . Get_Last_Modified_Remark($Group->DateModified, $Group->ModifiedByName) . '</span>-->
					<p class="spacer"></p>
					<input name="SaveGroupBtn" id="SaveGroupBtn" type="button" class="formbutton" onclick="Save_Group_Right_Target(\'' . $Group->GroupID . '\',\'Right\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Save'] . '" />
					<!--<input name="submit3" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Save As new" />-->
					<!--<input name="RemoveGroupBtn" id="RemoveGroupBtn" type="button" class="formbutton" onclick="Remove_Group(\'' . $Group->GroupID . '\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['libms']['GroupManagement']['RemoveThisGroup'] . '" />-->
					<!--<input name="GroupDetailCancelBtn" id="GroupDetailCancelBtn" type="button" class="formbutton" onclick="Get_Group_List_Table();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Cancel'] . '" />-->
					<p class="spacer"></p>
				</div>';
            return $x;
        }

        function Get_Class_Mgt_Top_Management_Form($Group)
        {
            global $Lang, $libms_default;
            
            // $UserRightTarget = new user_right_target();
            $x = '<div class="table_board">
					 <table width="100%" border="0">
					  <tr>
					    <td width="75%" valign="top">
					    <table class="common_table_list">
					    	<col /><col class="num_check" />
					      <tr>
					        <th>' . $Lang['libms']['GroupManagement']['AccessRight'] . '</th>
					        <th>&nbsp;</th>
					      </tr>
					      <tbody>';
            // $EffectiveRightList = $libms_default['group_right'];
            // $DBRightRightList=$Group->RightList;
            $GroupManage = new liblibrarymgmt_group_manage();
            $sql = 'Select
						Section,
						Function,
						IsAllow
					From
						LIBMS_CLASS_MANAGEMENT_GROUP_RIGHT
					WHERE
						GroupID = \'1\'
					';
            $DBRightRightList = $GroupManage->returnArray($sql);
            
            foreach ($libms_default['group_right'] as $section => $Functions)
                foreach ($Functions as $function => $default_rights) {
                    if ($section == 'circulation management') {
                        if ($function == 'not_allow_class_management_circulation') {
                            continue;
                        }
                        $EffectiveRightList[$section][$function] = false;
                    }
                }
            
            foreach ($DBRightRightList as $Right)
                $EffectiveRightList[$Right['Section']][$Right['Function']] = $Right['IsAllow'];
            
            $FontStyle = '';
            foreach ($EffectiveRightList as $section => $Functions) {
                $x .= "<tr><td colspan=99 style='background-color:#ffff00'>{$Lang['libms']['GroupManagement']['Right'][$section]['name']}</td></tr>";
                foreach ($Functions as $function => $right) {
                    $checked = ($right) ? 'checked="checked"' : '';
                    if ($Lang['libms']['GroupManagement']['Right'][$section][$function]) {
                        $x .= '<tr class=\'group_right\'>
						  <td align="left" class="sub_function_row" ' . $FontStyle . '><label for="' . $function . '">' . $Lang['libms']['GroupManagement']['Right'][$section][$function] . '</label></td>
						  <td><input onclick="Swap_Style(this);" name="Right[]" id="' . $section . '/' . $function . '" value="' . $section . '/' . $function . '" type="checkbox" ' . $checked . ' /></td>
						</tr>';
                    }
                }
            }
            
            $x .= '   </tbody>
					    </table>
					   </td>
					 </tr>
					</table>
					<p class="spacer"></p>
				</div>
				<div class="edit_bottom">
					<!--<span> ' . Get_Last_Modified_Remark($Group->DateModified, $Group->ModifiedByName) . '</span>-->
					<p class="spacer"></p>
					<input name="SaveGroupBtn" id="SaveGroupBtn" type="button" class="formbutton" onclick="Save_Group_Right_Target(\'1\',\'Right\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Save'] . '" />
					<!--<input name="submit3" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Save As new" />-->
					<!--<input name="RemoveGroupBtn" id="RemoveGroupBtn" type="button" class="formbutton" onclick="Remove_Group(\'' . $Group->GroupID . '\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['libms']['GroupManagement']['RemoveThisGroup'] . '" />-->
					<!--<input name="GroupDetailCancelBtn" id="GroupDetailCancelBtn" type="button" class="formbutton" onclick="Get_Group_List_Table();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Cancel'] . '" />-->
					<p class="spacer"></p>
				</div>';
            return $x;
        }

        function Get_User_Selection($AddUserID, $IdentityType, $YearClassSelect, $ParentStudentID, $GroupID)
        {
            global $Lang;
            
            if ($AddUserID[0] == "")
                $AddUserID = array();
            
            // choose the user that already in group and filter it
            $Group = new liblibrarymgmt_group($GroupID, false, true);
            for ($i = 0; $i < sizeof($Group->MemberList); $i ++) {
                $AddUserID1[] = $Group->MemberList[$i]['UserID'];
            }
            $AddUserID = array_merge((array) $AddUserID, (array) $AddUserID1);
            
            $GroupManage = new liblibrarymgmt_group_manage();
            
            $UserList = $GroupManage->Get_Avaliable_User_List($AddUserID, $IdentityType, $YearClassSelect, $ParentStudentID, $GroupID);
            
            if ($IdentityType == "Parent" && $YearClassSelect != "" && trim($ParentStudentID) == "") {
                $x .= '<select name="ParentStudentID" id="ParentStudentID" style="width:99%" onchange="Get_User_List(' . $GroupID . ');">';
                $x .= '<option value="all">' . $Lang['libms']['GroupManagement']['SelectAll'] . '</option>';
            } else {
                $x .= '<select name="AvalUserList[]" class="AvalUserList" id="AvalUserList[]" size="10" style="width:99%" multiple="true">';
            }
            for ($i = 0; $i < sizeof($UserList); $i ++) {
                if (! empty($UserList[$i]['GroupID'])) {
                    $has_group = 'has_group';
                } else {
                    $has_group = '';
                }
                $class_info = ($UserList[$i]['ClassName'] != "" && $UserList[$i]['ClassNumber'] != "") ? $UserList[$i]['ClassName'] . "-" . $UserList[$i]['ClassNumber'] . " " : "";
                $x .= '<option value="' . $UserList[$i]['UserID'] . '" ' . $has_group . ' >';
                $x .= $class_info . $UserList[$i]['Name'];
                $x .= '</option>';
            }
            $x .= '</select>';
            
            return $x;
        }

        function Get_Class_Mgt_User_Selection($AddUserID, $IdentityType, $YearClassSelect, $ParentStudentID, $GroupID)
        {
            global $Lang;
            
            if ($AddUserID[0] == "")
                $AddUserID = array();
            
            // choose the user that already in group and filter it
            // $Group = new liblibrarymgmt_group($GroupID,false,true);
            // for ($i=0; $i< sizeof($Group->MemberList); $i++) {
            // $AddUserID1[] = $Group->MemberList[$i]['UserID'];
            // }
            $GroupManage = new liblibrarymgmt_group_manage();
            
            // -- get group member list
            $NameField = $Lang['libms']['SQL']['UserNameFeild'];
            
            // $ParentNameField = getParentNameWithStudentInfo("r.","u.","",1);
            // $StudentNameField = getNameFieldWithClassNumberByLang("u.");
            
            $sql = 'Select
						u.UserID,						
						u.' . $NameField . ' as MemberName,
						u.BarCode,
						u.UserType,
						u.ClassNumber,
						u.ClassName
						
					From
						LIBMS_CLASS_MANAGEMENT_GROUP_USER gu
					INNER JOIN
					    LIBMS_USER u
					ON
					    u.UserID = gu.UserID
					WHERE 
						GroupID = \'' . $GroupID . '\'
					AND
						u.RecordStatus = 1	
					ORDER BY u.ClassLevel, u.ClassName, u.ClassNumber
					';
            // debug_r($sql);
            $GroupMemberList = $GroupManage->returnArray($sql);
            for ($i = 0; $i < sizeof($GroupMemberList); $i ++) {
                $AddUserID1[] = $GroupMemberList[$i]['UserID'];
            }
            // -- get group member list
            
            $AddUserID = array_merge((array) $AddUserID, (array) $AddUserID1);
            
            $UserList = $GroupManage->Get_Avaliable_User_List($AddUserID, $IdentityType, $YearClassSelect, $ParentStudentID, $GroupID);
            
            if ($IdentityType == "Parent" && $YearClassSelect != "" && trim($ParentStudentID) == "") {
                $x .= '<select name="ParentStudentID" id="ParentStudentID" style="width:99%" onchange="Get_User_List(' . $GroupID . ');">';
                $x .= '<option value="all">' . $Lang['libms']['GroupManagement']['SelectAll'] . '</option>';
            } else {
                $x .= '<select name="AvalUserList[]" class="AvalUserList" id="AvalUserList[]" size="10" style="width:99%" multiple="true">';
            }
            for ($i = 0; $i < sizeof($UserList); $i ++) {
                if (! empty($UserList[$i]['GroupID'])) {
                    $has_group = 'has_group';
                } else {
                    $has_group = '';
                }
                $class_info = ($UserList[$i]['ClassName'] != "" && $UserList[$i]['ClassNumber'] != "") ? $UserList[$i]['ClassName'] . "-" . $UserList[$i]['ClassNumber'] . " " : "";
                $x .= '<option value="' . $UserList[$i]['UserID'] . '" ' . $has_group . ' >';
                $x .= $class_info . $UserList[$i]['Name'];
                $x .= '</option>';
            }
            $x .= '</select>';
            
            return $x;
        }

        function Get_Group_Detail($Mode = "Print")
        {
            global $Lang, $PATH_WRT_ROOT, $MODULE_OBJ, $LAYOUT_SKIN;
            
            include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
            $linterface = new interface_html();
            
            include_once ($PATH_WRT_ROOT . "includes/group_manage.php");
            include_once ($PATH_WRT_ROOT . "includes/user_right_target.php");
            $GroupManage = new group_manage();
            $UserRightTarget = new user_right_target();
            
            $GroupList = $GroupManage->Get_Group_List(false);
            
            $GroupCount = sizeof($GroupList);
            for ($i = 0; $i < $GroupCount; $i ++) {
                $Group = new group($GroupList[$i]['GroupID'], true, true, true);
                $GroupName = $Group->GroupName;
                
                $table_content .= "<div style=\"width:100%; text-align:left;\"><b>" . $Lang['libms']['GroupManagement']['GroupName'] . ": </b>" . $GroupName . '</div>';
                $table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"eSporttableborder\" align='center'>\n";
                $table_content .= "<tr class=\"eSporttdborder eSportprinttabletitle\">";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" width=\"33%\"><b>" . $Lang['libms']['GroupManagement']['TopManagement'] . "</b></td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" width=\"33%\"><b>" . $Lang['libms']['GroupManagement']['Targeting'] . "</b></td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" width=\"34%\"><b>" . $Lang['libms']['GroupManagement']['UserList'] . "</b></td>";
                $table_content .= "</tr>";
                
                // Right List
                $table_content .= "<tr>
												<td class=\"eSporttdborder\" valign=\"top\">
												<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align='center'>";
                for ($j = 0; $j < sizeof($Group->RightList); $j ++) {
                    $RightArray = explode('-', $Group->RightList[$j]['FunctionName']);
                    if (sizeof($RightArray) == 2)
                        $RightName = $UserRightTarget->SysFunctionList[$RightArray[0]][$RightArray[1]][0];
                    else
                        $RightName = "";
                    
                    $table_content .= "<tr>";
                    $table_content .= "<td class='eSportprinttext'>" . $RightName . "&nbsp;</td>";
                    $table_content .= "</tr>";
                }
                $table_content .= "</table>";
                $table_content .= "</td>";
                
                // Target List
                $TargetList = (is_array($Group->TargetList)) ? array_keys($Group->TargetList) : array();
                $AllTeachingEnabled = in_array("Staff-AllTeaching", $TargetList);
                $AllNonTeachingEnabled = in_array("Staff-AllNonTeaching", $TargetList);
                $AllParentEnabled = in_array("Student-All", $TargetList);
                $AllStudentEnabled = in_array("Parent-All", $TargetList);
                $table_content .= "<td class=\"eSporttdborder\" valign=\"top\">
												<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align='center'>";
                for ($j = 0; $j < sizeof($TargetList); $j ++) {
                    if ($TargetList[$j] == "All-No")
                        continue;
                    
                    $TargetArray = explode('-', $TargetList[$j]);
                    if ($TargetArray[0] == "Staff" && $AllTeachingEnabled && $TargetList[$j] != "Staff-AllNonTeaching" && $TargetList[$j] != "Staff-AllTeaching")
                        continue;
                    if ($TargetArray[0] == "NonTeaching" && $AllNonTeachingEnabled && $TargetList[$j] != "Staff-AllNonTeaching")
                        continue;
                    if ($TargetArray[0] == "Student" && $AllStudentEnabled && $TargetList[$j] != "Student-All")
                        continue;
                    if ($TargetArray[0] == "Parent" && $AllParentEnabled && $TargetList[$j] != "Parent-All")
                        continue;
                    switch ($TargetArray[0]) {
                        case "Staff":
                            $Target = $Lang['libms']['GroupManagement']['TeachingStaff'];
                            break;
                        case "NonTeaching":
                            $Target = $Lang['libms']['GroupManagement']['SupportStaff'];
                            break;
                        case "Student":
                            $Target = $Lang['libms']['GroupManagement']['Student'];
                            break;
                        case "Parent":
                            $Target = $Lang['libms']['GroupManagement']['Parent'];
                            break;
                        default:
                            $Target = "";
                            break;
                    }
                    
                    switch ($TargetArray[1]) {
                        case 'AllTeaching':
                            $TargetName = str_replace("...", $Lang['libms']['GroupManagement']['TeachingStaff'], $Lang['libms']['GroupManagement']['ToAll']);
                            break;
                        case 'AllNonTeaching':
                            $TargetName = str_replace("...", $Lang['libms']['GroupManagement']['SupportStaff'], $Lang['libms']['GroupManagement']['ToAll']);
                            break;
                        case 'MyForm':
                            $TargetName = str_replace("...", $Target, $Lang['libms']['GroupManagement']['BelongToSameForm']);
                            break;
                        case 'MyClass':
                            $TargetName = str_replace("...", $Target, $Lang['libms']['GroupManagement']['BelongToSameFormClass']);
                            break;
                        case 'MySubject':
                            $TargetName = str_replace("...", $Target, $Lang['libms']['GroupManagement']['BelongToSameSubject']);
                            break;
                        case 'MySubjectGroup':
                            $TargetName = str_replace("...", $Target, $Lang['libms']['GroupManagement']['BelongToSameSubjectGroup']);
                            break;
                        case 'MyGroup':
                            $TargetName = str_replace("...", $Target, $Lang['libms']['GroupManagement']['BelongToSameGroup']);
                            break;
                        default:
                            $TargetName = str_replace("...", $Target, $Lang['libms']['GroupManagement']['ToAll']);
                            break;
                    }
                    $table_content .= "<tr>";
                    $table_content .= "<td class='eSportprinttext'>" . $TargetName . "&nbsp;</td>";
                    $table_content .= "</tr>";
                    
                    if ($TargetList[$j] == "All-Yes")
                        break;
                }
                $table_content .= "</table>";
                $table_content .= "</td>";
                
                // Member List
                $table_content .= "<td class=\"eSporttdborder\" valign=\"top\">
													<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align='center'>";
                for ($j = 0; $j < sizeof($Group->MemberList); $j ++) {
                    if ($Group->MemberList[$j]['RecordType'] == "2") {
                        $MemberName = $Group->MemberList[$j]['StudentMemberName'];
                    } else {
                        if ($Group->MemberList[$j]['RecordType'] == "3") {
                            $MemberName = $Group->MemberList[$j]['ParentMemberName'];
                        } else {
                            $MemberName = $Group->MemberList[$j]['MemberName'];
                        }
                    }
                    
                    $table_content .= "<tr>";
                    $table_content .= "<td class='eSportprinttext'>" . $MemberName . "&nbsp;</td>";
                    $table_content .= "</tr>";
                }
                $table_content .= "</table>
											</td>
										</tr>
									</table>
									<br><br><br>";
            }
            
            $print_btn_table = "<table width=\"100%\" align=\"center\" class=\"print_hide\" border=\"0\"><tr><td align=\"right\">" . $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2") . "</td></tr></table><BR>";
            
            include_once ($PATH_WRT_ROOT . "/templates/" . $LAYOUT_SKIN . "/layout/print_header.php");
            
            echo $print_btn_table;
            echo $table_content;
        }
    }
}
?>