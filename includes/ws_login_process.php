<?php
include_once("../includes/libxml.php");
include_once("../includes/libmethod.php");
include_once("../includes/libdb.php");
include_once("../includes/libauth.php");
include_once("../includes/global.php");
include_once("../includes/libwebservice.php");

$data = $HTTP_RAW_POST_DATA;

$xml = new LibXML();	
$arr = $xml->XML_ToArray($data, 0);

intranet_opendb();

if(is_array($arr)) {
	if($arr['eClassRequest']) {
		$SessionID = $arr['eClassRequest']['SessionID'];
		$RequestID = $arr['eClassRequest']['RequestID'];
		$RequestMethod = $arr['eClassRequest']['RequestMethod'];
		
		# Checking before call method
		if(trim($SessionID) != '') {
			$WebService = new WebService();
			$tUserID = $WebService->GetUserIDFromSessionKey(trim($SessionID));
			
			$method = new LibMethod();
			
			# Check SessionKey valid or not
			if($tUserID != '') {				
				$ReturnContent = $method->callMethod($RequestMethod, $arr);						
			}else{
				$ReturnContent = $method->getErrorArray('5');	
			}
		}
		
		$returnContent = $xml->Array_To_XML($ReturnContent);		
	}
}

echo $returnContent;
?>