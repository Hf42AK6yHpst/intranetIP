<?php
//modifying By Paul

include_once($intranet_root."/includes/iTextbook/iTextbookConfig.inc.php");

class libitextbook extends libdb {
	
	public function libitextbook(){
		$this->libdb();
	}
	
	/* Get MODULE_OBJ array for generation of left menu */
	public function GET_MODULE_OBJ_ARR(){
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $iTextbook_cfg, $Current_ModuleID, $Lang;
		
		$MenuArr["Settings"] = array($Lang['General']['Settings'], "", 1);
		
		$iTextbook_ary = $this->get_enabled_iTextbook();
		
		for($i=0;$i<count($iTextbook_ary);$i++){
			list($ModuleID, $Code) = $iTextbook_ary[$i];
			$MenuArr["Settings"]["Child"][$Code] = array($iTextbook_cfg[$Code]['book_name'],
														 $PATH_WRT_ROOT."home/eLearning/iTextbook/index.php?Current_ModuleID=$ModuleID", 
														 ($ModuleID==$Current_ModuleID? 1:0));
		}

        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass iTextBook";'."\n";
        $js.= '</script>'."\n";
        $MODULE_OBJ['title'] .= $js;
						
		/* Module information */
		$MODULE_OBJ['title'] 	 = $Lang['itextbook']['itextbook'].$js;
		$MODULE_OBJ['title_css'] = "menu_opened";
		$MODULE_OBJ['logo'] 	 = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_IES_setting.png";
		$MODULE_OBJ['root_path'] = "/home/eLearning/iTextbook/index.php";
		$MODULE_OBJ['menu'] 	 = $MenuArr;
		
		return $MODULE_OBJ;
	}
	
	/* Get iTextbook enabled */
	public function get_enabled_iTextbook(){
		global $iTextbook_cfg;
		
		$sql = "SELECT
					im.ModuleID,
					im.Code
				FROM
					INTRANET_MODULE AS im INNER JOIN
					INTRANET_MODULE_LICENSE AS iml ON im.ModuleID = iml.ModuleID
				WHERE
					im.Code IN ('".implode("','", $iTextbook_cfg['iTextbook_module_code_ary'])."')
				ORDER BY
					im.Code";
		
		return $this->returnArray($sql);
	}
	
	public function is_iTextbook_enabled($ModuleID){
		$enabled_ary = $this->get_enabled_iTextbook();
		$result = false;
		
		for($i=0;$i<count($enabled_ary);$i++){
			if($ModuleID==$enabled_ary[$i]['ModuleID']){
				$result = true;
				break;
			}
		}
		
		return $result;
	}
	
	public function admin_auth($ModuleID){
		global $plugin, $iTextbook_cfg;
		
		$result = false;
		
		if($plugin['iTextbook'] && $_SESSION["SSV_USER_ACCESS"]["eLearning-iTextbook"] && $this->is_iTextbook_enabled($ModuleID)){
			$result = true;
		}
		
		if(!$result)
			header("location:/");
	}
	
	public function admin_allow_create_book($ModuleCode){
		global $iTextbook_cfg;
		
		$result = false;
		
		if($iTextbook_cfg[$ModuleCode]['AllowCreateBook']){
			$result = true; 
		}
		
		if(!$result)
			header("Location:/home/eLearning/iTextbook");
	}
	
	public function get_iTextbook_total_quota($ModuleID){
		$sql = "SELECT
					SUM(NumberOfLicense)
				FROM
					INTRANET_MODULE_LICENSE
				WHERE
					ModuleID = '$ModuleID'";
		$quota = current($this->returnVector($sql));
		$quota = $quota? $quota : 0;
		
		return $quota;
	}
	
	public function get_iTextbook_total_used($ModuleID, $IsCountSpecialUnit=false){
		global $iTextbook_cfg;
		
		$code = $this->get_iTextbook_code($ModuleID);
		
		$cond = $IsCountSpecialUnit &&
				is_array($iTextbook_cfg[$code]['SpecialUnit']) &&
				count($iTextbook_cfg[$code]['SpecialUnit'])>0? "" : "AND Unit NOT IN (".implode(',', $iTextbook_cfg[$code]['SpecialUnit']).")";
		
		$sql = "SELECT
					COUNT(*)
				FROM
					INTRANET_MODULE_USER
				WHERE
					ModuleID = '$ModuleID'
					$cond";
		$quota_used = current($this->returnVector($sql));
		$quota_used = $quota_used? $quota_used : 0;
		
		return $quota_used;
	}
	
	public function get_iTextbook_code($ModuleID){
		$sql = "SELECT
					Code
				FROM
					INTRANET_MODULE
				WHERE
					ModuleID = '$ModuleID'";
					
		return current($this->returnVector($sql));
	}
	
	public function get_iTextbook_ModuleID($code){
		$sql = "SELECT
					ModuleID
				FROM
					INTRANET_MODULE
				WHERE
					Code = '$code'";
		
		return current($this->returnVector($sql));
	}
	
	public function get_class_drop_down_list($id, $name, $selected_item='', $other_attr=''){
		$class_list = $this->get_class_list();
		
		$html = "<select id='$id' name='$name' $other_attr>";
		
		foreach($class_list as $YearID=>$year_ary){
			$YearName  = $year_ary['YearName'];
			$ClassList = $year_ary['ClassList'];
			
			$html .= "<optgroup label=\"$YearName\">";
			
			foreach($ClassList as $YearClassID=>$ClassName){
				$html .= "<option value='$YearClassID' ".($YearClassID==$selected_item? 'selected' : '').">$ClassName</option>";
			}
			
			$html .= "</optgroup>";
		}
		
		$html .= "</select>";
		
		return $html;
	}
	
	private function get_class_list(){
		global $CurrentSchoolYearID, $intranet_session_language;
		
		$sql = "SELECT
					y.YearID AS YearID,
					y.YearName AS YearName,
					yc.YearClassID AS YearClassID,
					".($intranet_session_language=='en'? 'yc.ClassTitleEN':'yc.ClassTitleB5')." AS ClassName
				FROM
					YEAR AS y INNER JOIN
					YEAR_CLASS AS yc ON y.YearID = yc.YearID
				WHERE
					yc.AcademicYearID = '$CurrentSchoolYearID' AND
					y.RecordStatus = 1
				ORDER BY
					y.Sequence";
		$year_ary = $this->returnArray($sql);
		
		for($i=0;$i<count($year_ary);$i++){
			list($YearID, $YearName, $YearClassID, $ClassName) = $year_ary[$i];
			$class_list[$YearID]['YearName'] = $YearName;
			$class_list[$YearID]['ClassList'][$YearClassID] = $ClassName;
		}
		
		return $class_list;
	}
	
	
	public function get_default_class(){
		$class_list = $this->get_class_list();
		
		$default_year  = current($class_list);
		$default_class = current(array_keys($default_year['ClassList']));
		
		return $default_class;
	}
	
	public function get_class_student_with_assigned_module($ModuleID, $ClassID){
		global $intranet_session_language;
		
		$sql = "SELECT
					iu.UserID,
					ycu.ClassNumber,
					".getNameFieldByLang('iu.',$intranet_session_language).",
					imu.ModuleStudentID,
					imu.Unit,
					imu.InputDate
				FROM
					YEAR_CLASS AS yc INNER JOIN
					YEAR_CLASS_USER AS ycu ON yc.YearClassID = ycu.YearClassID INNER JOIN
					INTRANET_USER AS iu ON ycu.UserID = iu.UserID LEFT JOIN
					(
					 SELECT
						UserID,
						ModuleStudentID,
						Unit,
						InputDate
					 FROM
						INTRANET_MODULE_USER
					 WHERE
						ModuleID = '$ModuleID'
					)AS imu ON iu.UserID = imu.UserID
				WHERE
					yc.YearClassID = '$ClassID' AND
					iu.RecordType = 2 AND
					iu.RecordStatus = 1
				ORDER BY
					ycu.ClassNumber";
		
		$tmp_ary = $this->returnArray($sql);
		
		# Format Result Array
		for($i=0;$i<count($tmp_ary);$i++){
			list($user_id, $ClassNumber, $Name, $ModuleStudentID, $Unit, $InputDate) = $tmp_ary[$i];
			$result_ary[$user_id]['ClassNumber'] = $ClassNumber;
			$result_ary[$user_id]['Name'] = $Name;
			if($Unit && $InputDate) $result_ary[$user_id]['Unit'][$Unit] = array("RecordID"=>$ModuleStudentID, "InputDate"=>$InputDate);
		}
		
		return $result_ary;
	}
	
	public function gen_assign_student_table($ModuleID, $ClassID){
		global $iTextbook_cfg, $image_path, $i_no_record_exists_msg;
		
		$code 	  = $this->get_iTextbook_code($ModuleID);
		$std_list = $this->get_class_student_with_assigned_module($ModuleID, $ClassID);
		
		$table .= "<table class='common_table_list_v30'>";
		
		$table .= $this->gen_assign_studnet_table_header($code);
		
		$i = 1;
		
		if(count($std_list)>0){
			foreach($std_list as $std_user_id=>$std_user_ary){
				$ClassNumber = $std_user_ary['ClassNumber'];
				$Name		 = $std_user_ary['Name'];
				$UnitAry	 = is_array($std_user_ary['Unit'])? $std_user_ary['Unit'] : array();
				
				$table .= "<tr id='row_$i'>";
				$table .= "		<td class='start'>$i</td>";
				$table .= " 	<td class='row'>$ClassNumber</td>";
				$table .= " 	<td class='row'>$Name</td>";
				$table .= " 	<td class='row'><input type='checkbox' id='check_row_$i' class='check_row' onclick='check_row($i)'/></td>";
				
				for($j=1;$j<=$iTextbook_cfg[$code]['TotalNoOfUnit'];$j++){
					$IsSpecialUnit = in_array($j, $iTextbook_cfg[$code]['SpecialUnit']);
					$table .= "	<td class='".($j==$iTextbook_cfg[$code]['TotalNoOfUnit']? 'end' : 'row')."'>";
					
					if(isset($std_user_ary['Unit'][$j]) && is_array($std_user_ary['Unit'][$j])){
						if($this->Is_Cooldown_passed($iTextbook_cfg[$code]['cooldown'], $std_user_ary['Unit'][$j]['InputDate'])){
							$table .= "<img src='$image_path/2009a/icon_tick_green.gif' />";
							$table .= "<input type='checkbox' class='".($IsSpecialUnit? "SpecialUnit" : "NormalUnit")." row_$i col_$j lock' name='std_unit[$std_user_id][$j]' value='1' checked style='display:none'/>";
						}
						else{
							$table .= "<input type='checkbox' class='".($IsSpecialUnit? "SpecialUnit" : "NormalUnit")." row_$i col_$j free' name='std_unit[$std_user_id][$j]' value='1' checked onclick='".($IsSpecialUnit? "return false;" : "check_row_column($i,$j)")."'/>";
						}
					}
					else{
						$table .= "<input type='checkbox' class='".($IsSpecialUnit? "SpecialUnit" : "NormalUnit")." row_$i col_$j free' name='std_unit[$std_user_id][$j]' value='1' onclick='".($IsSpecialUnit? "return false;" : "check_row_column($i,$j)")."'/>";
					}
					
					$table .= "	</td>";
				}
				
				$table .= "</tr>";
				
				$i++;
			}
		}
		else{
			$colspan = $iTextbook_cfg[$code]['TotalNoOfUnit'] + 4;
			$table  .= "<tr>";
			$table  .= "	<td colspan='$colspan' align='center'>$i_no_record_exists_msg</td>";
			$table  .= "</tr>";
		}
		
		$table .= "</table>";
		$table .= "<input type='hidden' id='NoOfRecord' name='NoOfRecord' value='".count($std_list)."'/>";
		
		return $table;
	}
	
	public function Is_Cooldown_passed($Cooldown, $Inputdate){
		$result = false;
		
		if($Inputdate){
			$Cooldown_in_sec = $Cooldown * 24 * 60 * 60;
			$result = strtotime($Inputdate) < (time() - $Cooldown_in_sec)? true : false;
		}
		
		return $result;
	}
	
	private function gen_assign_studnet_table_header($code){
		global $Lang, $iTextbook_cfg;
		
		$table_header .= "<tr class='tabletop'>";
		$table_header .= "	<th width='3%'>#</th>";
		$table_header .= "	<th width='20%'>".$Lang['itextbook']['ClassNo']."</th>";
		$table_header .= "	<th width='20%'>".$Lang['itextbook']['Name']."</th>";
		$table_header .= "	<th><input type='checkbox' style='visibility:hidden'/></th>";
		
		for($i=1;$i<=$iTextbook_cfg[$code]['TotalNoOfUnit'];$i++){
			$table_header .= "<th style='text-align:center; vertical-align:top'>";
			$table_header .= "	$i<br/>";
			$table_header .= in_array($i, $iTextbook_cfg[$code]['SpecialUnit'])? "<input type='checkbox' style='visibility:hidden'/>" : "<input type='checkbox' id='check_column_$i' class='check_col' onclick='check_column($i)'/>";
			$tbale_header .= "</th>";
		}
		
		$table_header .= "</tr>";
		
		return $table_header;
	}
	
	public function gen_instruction_table($ModuleID, $code, $quota_left, $total_quota){
		global $Lang, $iTextbook_cfg;
		
		if($ModuleID)
			$code = $this->get_iTextbook_code($ModuleID);
		
		$table = "<table width=\"100%\">
					<col width='13%'/>
					<col width='1%'>
					<col width='*'/>";
		
		if(isset($iTextbook_cfg[$code]['instruction']) && $iTextbook_cfg[$code]['instruction']!='') { 				
		$table .= "	<tr>
						<td><span style=\"font-size:1em;\"><strong>".$Lang['itextbook']['Instruction']."</strong></span></td>
						<td>:</td>
						<td>".$iTextbook_cfg[$code]['instruction']."</td>
					</tr>";
		}
		
		if(isset($iTextbook_cfg[$code]['remarks']) && $iTextbook_cfg[$code]['remarks']!='') {
		$table .= "	<tr>
						<td><span style=\"font-size:1em;\"><strong>".$Lang['itextbook']['Remarks']."</strong></span></td>
						<td>:</td>
						<td>".$iTextbook_cfg[$code]['remarks']."</td>
					</tr>";
		}
		
		if(isset($iTextbook_cfg[$code]['cooldown']) && $iTextbook_cfg[$code]['cooldown']!='') {
		$table .= "	<tr>
						<td><span style=\"font-size:1em;\"><strong>".$Lang['itextbook']['Cooldown']."</strong></span></td>
						<td>:</td>
						<td>".$iTextbook_cfg[$code]['cooldown']." day(s)</td>
					</tr>";
		}
		
		$table .= "	<tr>
						<td>
							<span style=\"font-size:1em;\"><strong>".$Lang['itextbook']['UserQuotaLeft']."</strong></span>
						</td>
						<td>:</td>
						<td>
							<span style=\"font-size:1em;\"><span id='quota_left'>$quota_left</span> / $total_quota</span>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span style=\"font-size:1em;\"><strong>".$Lang['itextbook']['Quota_Released_and_Unassigned']."</strong></span> :
							<span style=\"font-size:1em;\"><span id='quota_released'>0</span></span>
						</td>
					</tr>
					<tr><td colspan='6'>&nbsp;</td></tr>
				  </table>";
		
		return $table;
	}
	
	public function gen_admin_nav($page_ary){
		global $image_path;
		
		$html  = "<div class=\"navigation\">";
		
		for($i=0;$i<count($page_ary);$i++){
			$display = $page_ary[$i]['display'];
			$link    = $page_ary[$i]['link'];
			
			$html .= "&nbsp; <img height=\"15\" align=\"absmiddle\" width=\"15\" src=\"$image_path/2009a/nav_arrow.gif\">";
			$html .= $link? "<a class=\"navigation\" href=\"$link\">" : "<span class=\"navigation\">";
			$html .= $display;
			$html .= $link? "</a>" : "</span>";
			
		}

		$html .= "</div>";
		
		return $html;
	}
	
	public function format_user_session_array($par_user_id, $allow_all=false){
		global $iTextbook_cfg;
		
		$result_ary = array();
		
		for($i=0;$i<count($iTextbook_cfg['iTextbook_module_code_ary']);$i++){
			$code = $iTextbook_cfg['iTextbook_module_code_ary'][$i];
			
			if(!$this->check_itextbook_expiried($code, $par_user_id)){
				if($allow_all){
					for($j=1;$j<=$iTextbook_cfg[$code]['TotalNoOfUnit'];$j++){
						if(!in_array($j, $iTextbook_cfg[$code]['DisableUnit']))
							$result_ary[$code][] = $j;
					}
				}
				else{
					$enabled_iTextbook_ary = $this->get_enable_iTextbook_by_UserID($code, $par_user_id);
					if(count($enabled_iTextbook_ary))
						$result_ary[$code] = $enabled_iTextbook_ary;
				}
			}
		}
		
		return $result_ary;
	}
	
	/*
	public function get_enable_iTextbook_by_UserID($ModuleID, $code, $par_user_id){
		$ModuleID = $ModuleID? $ModuleID : $this->get_iTextbook_ModuleID($code);
		
		$sql = "SELECT
					Unit
				FROM
					INTRANET_MODULE_USER
				WHERE
					ModuleID = $ModuleID AND
					UserID = $par_user_id
				ORDER BY
					Unit";
		
		return $this->returnVector($sql);
	}
	*/
	
	public function get_enable_iTextbook_by_UserID($code, $par_user_id){
		global $iTextbook_cfg;
		
		$sql = "SELECT
    				ib.Chapters
				FROM
					ITEXTBOOK_BOOK AS ib INNER JOIN
    				ITEXTBOOK_DISTRIBUTION AS id ON ib.BookID = id.BookID INNER JOIN
    				ITEXTBOOK_DISTRIBUTION_USER AS idu ON id.DistributionID = idu.DistributionID
				WHERE
					ib.BookType = '$code' AND
					ib.Status = '".$iTextbook_cfg['db_setting']['book']['status']['not_deleted']."' AND
					idu.UserID = '$par_user_id'";
		$Raw_Chapters = $this->returnVector($sql);
		
		$Chapters = array();
		for($i=0;$i<count($Raw_Chapters);$i++){
			$Chapter_ary = explode($iTextbook_cfg['db_setting']['book']['chapter']['dilimeter'], $Raw_Chapters[$i]);
			for($j=0;$j<count($Chapter_ary);$j++){
				if(!in_array($Chapter_ary[$j], $Chapters) && !in_array($Chapter_ary[$j], $iTextbook_cfg[$code]['DisableUnit']))
					$Chapters[] = $Chapter_ary[$j];
			}
		}
		sort($Chapters, SORT_NUMERIC);
		
		return $Chapters;
	}
	
	public function format_user_book_session_array($par_user_id, $allow_all=false){
		global $iTextbook_cfg;
		
		$result_ary = array();
		
		for($i=0;$i<count($iTextbook_cfg['iTextbook_module_code_ary']);$i++){
			$code = $iTextbook_cfg['iTextbook_module_code_ary'][$i];
			
			if(!$this->check_itextbook_expiried($code, $par_user_id)){
				$result_ary[$code] = $this->get_user_iTextbook_Book($code, $par_user_id, $allow_all);
			}
		}
		return $result_ary;
	}
	
	public function get_user_iTextbook_Book($code, $par_user_id, $allow_all=false){
		global $iTextbook_cfg;
		
		$sql = "SELECT
	    			ib.BookID, ib.BookName, ib.BookLang, ib.Chapters
				FROM
					ITEXTBOOK_BOOK AS ib ".(!$allow_all? "INNER JOIN
    				ITEXTBOOK_DISTRIBUTION AS id ON ib.BookID = id.BookID INNER JOIN
    				ITEXTBOOK_DISTRIBUTION_USER AS idu ON id.DistributionID = idu.DistributionID":"")."
				WHERE
					ib.BookType = '$code' AND
					ib.Status = '".$iTextbook_cfg['db_setting']['book']['status']['not_deleted']."'
					".(!$allow_all? "AND idu.UserID = '$par_user_id'" : "")."
				ORDER BY
					ib.BookName";
		$Book_ary = $this->returnArray($sql);
		
		$result_ary = array();
		for($i=0;$i<count($Book_ary);$i++){
			$result_ary[$Book_ary[$i]['BookID']]['BookName'] = $Book_ary[$i]['BookName'];
			$result_ary[$Book_ary[$i]['BookID']]['BookLang'] = $Book_ary[$i]['BookLang'];
			
			$Chapter_ary = explode($iTextbook_cfg['db_setting']['book']['chapter']['dilimeter'], $Book_ary[$i]['Chapters']);
			for($j=0;$j<count($Chapter_ary);$j++){
				if(!in_array($Chapter_ary[$j], $iTextbook_cfg[$code]['DisableUnit']))
					$result_ary[$Book_ary[$i]['BookID']]['Chapters'][] = $Chapter_ary[$j];
			}
		}
		
		return $result_ary;
	} 
	
	public function gen_tab_menu($selected_tab=1){
		global $intranet_httppath, $Current_ModuleID, $Lang, $iTextbook_cfg;
		
		$code = $this->get_iTextbook_code($Current_ModuleID);
		
		$html = "<form name=\"header_form\" method=\"post\">
					<input type=\"hidden\" name=\"Current_ModuleID\" value=\"$Current_ModuleID\">
				 </form>
				<script>
					function go_tab(tab_no){
						switch(tab_no){
							case 1:
								document.header_form.action = '$intranet_httppath/home/eLearning/iTextbook/index.php';
								break;
							case 2:
								document.header_form.action = '$intranet_httppath/home/eLearning/iTextbook/book_list.php';
								break;
							case 3:
								document.header_form.action = '$intranet_httppath/home/eLearning/iTextbook/assign_teacher.php';
								break;
							case 4:
								document.header_form.action = '$intranet_httppath/home/eLearning/iTextbook/statistic.php';
								break;
						}
						document.header_form.submit();
					}
				 </script>
				 <div class=\"shadetabs\">
					<ul>
						<li class=\"".($selected_tab==1? "selected" : "")."\">
							<a href=\"javascript:go_tab(1)\"><strong>".$Lang['itextbook']['PurchaseRecord']."</strong></a>
						</li>";
		
		if($iTextbook_cfg[$code]['AllowCreateBook']){
			$html .= "	<li class=\"".($selected_tab==2? "selected" : "")."\">
							<a href=\"javascript:go_tab(2)\"><strong>".$Lang['itextbook']['BookList']."</strong></a>
						</li>";
		}
		
		$html .= "		<li class=\"".($selected_tab==3? "selected" : "")."\">
							<a href=\"javascript:go_tab(3)\"><strong>".$Lang['itextbook']['AssignTeacherInCharge']."</strong></a>
						</li>";
		if(strpos($code,'rsreading') !== false&&strpos($code,'gvlistening') !== false&&strpos($code,'grammartouch') !== false)
		$html .= "
						<li class=\"".($selected_tab==4? "selected" : "")."\">
							<a href=\"javascript:go_tab(4)\"><strong>".$Lang['itextbook']['Statistic']."</strong></a>
						</li>
					</ul>
				 </div>";
		
		return $html;
	}
	
	public function gen_chapter_selection_list($code, $selected_chapter_list=array(), $IsLock=false, $IsSelectedListOnly=false, $IsShowSpecialChapter=false, $BookLang="en"){
		global $iTextbook_cfg, $Lang;
		
		foreach($iTextbook_cfg[$code]['ChapterName'] as $chapter=>$chapter_name_ary){
			if(!in_array($chapter, $selected_chapter_list) &&
			   !in_array($chapter, $iTextbook_cfg[$code]['DisableUnit']) &&
			   ($IsShowSpecialChapter || !in_array($chapter, $iTextbook_cfg[$code]['SpecialUnit']))){
			   	
				$chapter_list_html .= "<div class=\"chapter_list_chapter ".($IsLock? "fixed" : "")."\">";
				
				foreach($chapter_name_ary as $BookLangCode=>$chapter_name){
					$chapter_list_html .= "<a href=\"javascript:void(0)\" class=\"book_lang book_lang_$BookLangCode\" style=\"".($BookLangCode!=$BookLang? "display:none":"")."\">".$Lang['itextbook']['Chapter'][$BookLangCode]."$chapter - $chapter_name</a>";
				}
										
										
				$chapter_list_html .= "		<input type=\"hidden\" id=\"IsSelected_$chapter\" name=\"IsSelected[$chapter]\" class=\"IsSelected\" value=\"0\"/>
											<input type=\"hidden\" id=\"Order_$chapter\" name=\"Order[]\" value=\"$chapter\"/>
									   </div>";
			}
		}
		
		if(count($selected_chapter_list)>0){
			for($i=0;$i<count($selected_chapter_list);$i++){
				$chapter = $selected_chapter_list[$i];
				
				if(!in_array($chapter, $iTextbook_cfg[$code]['DisableUnit']) &&
				   ($IsShowSpecialChapter || !in_array($chapter, $iTextbook_cfg[$code]['SpecialUnit']))){
				   	
					$chapter_name_ary = $iTextbook_cfg[$code]['ChapterName'][$chapter];
					$selected_chapter_list_html .= "<div class=\"chapter_list_chapter\">";
					
					foreach($chapter_name_ary as $BookLangCode=>$chapter_name){
						$selected_chapter_list_html .= "<a href=\"javascript:void(0)\" class=\"book_lang book_lang_$BookLangCode\" style=\"".($BookLangCode!=$BookLang? "display:none":"")."\">".$Lang['itextbook']['Chapter'][$BookLangCode]."$chapter - $chapter_name</a>";
					}
					
					$selected_chapter_list_html .= "	<input type=\"hidden\" id=\"IsSelected_$chapter\" name=\"IsSelected[$chapter]\" class=\"IsSelected\" value=\"1\"/>
														<input type=\"hidden\" id=\"Order_$chapter\" name=\"Order[]\" value=\"$chapter\"/>
													</div>";
				}
			}
		}
		
		$html = "<table class='peer_within_group_board'>
					<tr>";
		
		if(!$IsSelectedListOnly){
			$html .= "	<td id=\"chapter_list\" class=\"within_column ui-sortable\" width=\"50%\" style=\"vertical-align:top; border: 3px solid #D0D0D0;\">
							<div class=\"chapter_list_title fixed\">
								<span class=\"chapter_list_title_name\">".$Lang['itextbook']['ChapterList']."</span> 
								<p class=\"spacer\"></p>
							</div>
							$chapter_list_html
						</td>";
		}
		
		$html .= "		<td id=\"selected_chapter_list\" class=\"within_column ui-sortable\" width=\"".($IsSelectedListOnly? "100%":"50%")."\" style=\"vertical-align:top; border: 3px solid #D0D0D0;\">
							<div class=\"chapter_list_title fixed\">
								<span class=\"chapter_list_title_name\">".$Lang['itextbook']['SelectedChapters']."</span> 
								<p class=\"spacer\"></p>
							</div>
							$selected_chapter_list_html
						</td>
					</tr>
				 </table>";
		
		return $html;
	}
	
	/*
	public function get_distribution_info($DistributionID){
		$sql = "SELECT
					DistributionID, ModuleLicenseID, BookID,
					DateInput, DateModified, ModifiedBy
				FROM
					ITEXTBOOK_DISTRIBUTION
				WHERE
					DistributionID = $DistributionID";
		
		return current($this->returnArray($sql));
	}
	*/
	
	public function get_distributions_user_info($ModuleLicenseID, $DistributionID=''){
		global $intranet_session_language, $CurrentSchoolYearID;
		
		/*
		$sql = "SELECT
					idu.DistributionUserID,
					id.DistributionID,
					id.BookID,
					id.DateInput,
					id.DateModified,
					id.ModifiedBy,
					".getNameFieldByLang('iu.',$intranet_session_language)." AS UserName,
					ycu.ClassNumber,
					".($intranet_session_language=='en'? 'yc.ClassTitleEN':'yc.ClassTitleB5')." AS ClassName
				FROM
				    YEAR AS y INNER JOIN
				    YEAR_CLASS AS yc ON y.RecordStatus = 1 AND y.YearID = yc.YearID INNER JOIN
				    YEAR_CLASS_USER AS ycu ON yc.AcademicYearID = $CurrentSchoolYearID AND yc.YearClassID = ycu.YearClassID INNER JOIN
				    INTRANET_USER AS iu ON ycu.UserID = iu.UserID INNER JOIN
				    ITEXTBOOK_DISTRIBUTION_USER AS idu ON iu.UserID = idu.UserID RIGHT JOIN
				    ITEXTBOOK_DISTRIBUTION AS id ON idu.DistributionID = id.DistributionID
				WHERE
				    id.ModuleLicenseID = $ModuleLicenseID
					".($DistributionID? "AND id.DistributionID = $DistributionID":"")."
				ORDER BY
					id.DateInput";
		*/
		$sql = "SELECT
					idu.DistributionUserID,
					id.DistributionID,
					id.BookID,
					id.DateInput,
					id.DateModified,
					id.ModifiedBy,
					".getNameFieldByLang('iu.',$intranet_session_language)." AS UserName,
					iu.ClassNumber,
					iu.ClassName
				FROM
				    INTRANET_USER AS iu INNER JOIN
				    ITEXTBOOK_DISTRIBUTION_USER AS idu ON iu.UserID = idu.UserID RIGHT JOIN
				    ITEXTBOOK_DISTRIBUTION AS id ON idu.DistributionID = id.DistributionID
				WHERE
				    id.ModuleLicenseID = '$ModuleLicenseID'
					".($DistributionID? "AND id.DistributionID = '$DistributionID'":"")."
				ORDER BY
					id.DateInput, iu.ClassName, iu.ClassNumber, UserName";
		$UserInfo = $this->returnArray($sql);
		
		$UserInfo_ary = array();
		for($i=0;$i<count($UserInfo);$i++){
			$DistributionID = $UserInfo[$i]['DistributionID'];
			
			$UserInfo_ary[$DistributionID]['BookID']		= $UserInfo[$i]['BookID'];
			$UserInfo_ary[$DistributionID]['DateInput']		= $UserInfo[$i]['DateInput'];
			$UserInfo_ary[$DistributionID]['DateModified'] 	= $UserInfo[$i]['DateModified'];
			
			if($UserInfo[$i]['UserName'] || $UserInfo[$i]['ClassNumber'] || $UserInfo[$i]['ClassName']){
				$UserInfo_ary[$DistributionID]['UserInfo'][] 	= array("DistributionUserID" => $UserInfo[$i]['DistributionUserID'],
																		"UserName"	  		 =>$UserInfo[$i]['UserName'],
																		"ClassNumber" 		 =>$UserInfo[$i]['ClassNumber'],
																		"ClassName"	  		 =>$UserInfo[$i]['ClassName']);
			}
		}
		
		return $UserInfo_ary;
	}
	
	public function gen_distribution_info_table($ModuleCode, $DistributionNo=1, $DistributionUserAry=array(), $sys_msg=''){
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $ec_iPortfolio, $iTextbook_cfg;
		
		$BookAry = $this->get_books_info($ModuleCode);
		
		if(count($DistributionUserAry)>0){
			foreach($DistributionUserAry as $DistributionID=>$DistributionAry){
				$cooldown_end  = date('Y-m-d H:i:s', strtotime($DistributionAry['DateInput']) + ($iTextbook_cfg[$ModuleCode]['cooldown'] * 24 * 60 * 60));
				$IsCooldownEnd = $this->Is_Cooldown_passed($iTextbook_cfg[$ModuleCode]['cooldown'], $DistributionAry['DateInput']);
				$UserInfo	   = $DistributionAry['UserInfo'];
				$sys_msg 	   = $sys_msg? $sys_msg : "";
				
				if(!$IsCooldownEnd){
					# Generate Book Select List
					$Book_html  = "<select id=\"BookID_$DistributionID\" name=\"BookID_$DistributionID\" onchange=\"update_book($DistributionID, $DistributionNo, this.value)\">";
					foreach($BookAry as $BookID=>$BookName){
						$Book_html .= "<option value=\"".$BookID."\" ".($DistributionAry['BookID']==$BookID? "selected":"").">".$BookName."</option>";
					}
					$Book_html .= "</select>";
				}
				else
					$Book_html = $BookAry[$DistributionAry['BookID']];
			
				$html .= $this->gen_distribution_info_table_ui($DistributionID, $DistributionNo, $Book_html, $cooldown_end, $IsCooldownEnd, $UserInfo, $sys_msg);
				$DistributionNo++;
			}
		}
		else{
			$cooldown_end  = '';
			$IsCooldownEnd = false;
			$UserInfo	   = array();
			$sys_msg 	   = $sys_msg? $sys_msg : "";
			
			# Generate Book Select List
			$Book_html  = "<select id=\"BookID_\" name=\"BookID_\">";
			foreach($BookAry as $BookID=>$BookName){
				$Book_html .= "<option value=\"$BookID\">$BookName</option>";
			}
			$Book_html .= "</select>";
			
			$html .= $this->gen_distribution_info_table_ui(0, $DistributionNo, $Book_html, $cooldown_end, $IsCooldownEnd, $UserInfo, $sys_msg);
		}
		
		return $html;
	}
	
	private function gen_distribution_info_table_ui($DistributionID=0, $DistributionNo=1, $Book_html='', $cooldown_end='', $IsCooldownEnd=false, $UserInfo=array(), $sys_msg=""){
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $ec_iPortfolio, $iTextbook_cfg, $Lang;
		
		$html = "<div id=\"Distribution_$DistributionNo\">
					 <table width=\"85%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
						<tr>
							<td colspan=\"100%\">
								<table width=\"100%\">
									<tr height=\"25px\">
										<td>
											<div id=\"sys_msg_$DistributionNo\" style=\"float:right\">$sys_msg</div>
										</td>
									</tr>
									<tr>
										<td class=\"tabletext\">
											<div style=\"float:left\">
												<img height=\"20\" src=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/icon_section.gif\" width=\"20\" align=\"absMiddle\"/>
												<span class=\"sectiontitle\">".$Lang['itextbook']['Distribution']." $DistributionNo</span>
											</div>";
		
		if(!$IsCooldownEnd){
			$html .= "						<div style=\"float:right\">
												<a class=\"tabletool\" href=\"javascript:void(0)\" onclick=\"delete_distribution($DistributionID)\">
													<img src=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif\" name=\"imgDelete\" align=\"absmiddle\" border=\"0\" >".$Lang['itextbook']['DeleteDistribution']."
												</a>
											</div>";
		}
		
		$html .= "						</td>
									</tr>
									<tr>
										<td>
											<div style=\"float:left\">
												".$Lang['itextbook']['Book']." : $Book_html
											</div>";
		
		if($cooldown_end){
			$html .= "						<div style=\"float:right\">
												".$Lang['itextbook']['CoolDownPeriodEndAt']." : $cooldown_end
											</div>";
		}
										
		$html .= "						</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan=\"100%\">&nbsp;</td>
						</tr>
						<tr>
							<td align=\"left\">";
							
		if(!$IsCooldownEnd){
			$html .= "			<div class=\"content_top_tool\">
								  	<div class=\"Conntent_tool\">
								  		<a title=\"".$Lang['itextbook']['AddStudentToDistribution']."\" href=\"javascript:add_student($DistributionID, $DistributionNo)\">".$Lang['itextbook']['AddStudentToDistribution']."</a>
								  	</div>
								</div>";
		}
		
		$html .= "			</td>
							<td align=\"right\" valign=\"bottom\">
								<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
					          		<tr>
					            		<td width=\"21\"><img src=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/management/table_tool_01.gif\" height=\"23\" width=\"21\"></td>
					            		<td background=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/management/table_tool_02.gif\">
					              			<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\">
												<tr>
					                    			<td><img src=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/management/10x10.gif\" width=\"5\"></td>
					                    			<td nowrap=\"nowrap\">
														<a class=\"tabletool\" href=\"javascript:void(0)\" onclick=\"".($IsCooldownEnd? "alert('Distribution is Locked! No editing is allowed.')" : "delete_distribution_user($DistributionID, $DistributionNo)")."\">
															<img src=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif\" name=\"imgDelete\" align=\"absmiddle\" border=\"0\" >".$ec_iPortfolio['delete']."
														</a>
													</td>
					                  			</tr>
					              			</table>
					            		</td>
					            		<td width=\"6\"><img src=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/management/table_tool_03.gif\" height=\"23\" width=\"6\"></td>
					          		</tr>
					        	</table>
							</td>
						</tr>
						<tr>
							<td colspan=\"100%\">
								".$this->gen_distribution_user_table($DistributionNo, $UserInfo, $IsCooldownEnd)."
							</td>
						</tr>
					</table>
					<div id=\"distribution_separator\">
						<br /><p class=\"spacer\"></p><hr/>
				  	</div>
				</div>";
		
		return $html;
	}
	
	private function gen_distribution_user_table($DistributionNo, $UserInfo=array(), $IsCooldownEnd=false){
		global $intranet_session_language, $Lang;
		
		if(count($UserInfo)<=1){
			$html .= "<table class=\"common_table_list_v30\">
						<tr class=\"tabletop\">
							<th width=\"1\" style=\"vertical-align: middle;\" class=\"tabletop tabletopnolink\">&nbsp;#&nbsp;</th>
							<th width=\"20%\" style=\"vertical-align: middle;\" class=\"tabletop\">".$Lang['itextbook']['Class']."</th>
							<th width=\"80%\" style=\"vertical-align: middle;\" class=\"tabletop\">".$Lang['itextbook']['Name']."</th>
							<th width=\"1\" style=\"vertical-align: middle;\" class=\"tabletop\">
								<input type=\"checkbox\" id=\"L_check_all_$DistributionNo\" name=\"L_check_all_$DistributionNo\" onclick=\"check_all_row('L', $DistributionNo)\"/>
							</th>
						</tr>	
						<tr>";
			
			if(count($UserInfo)==0){
				$html .= "<td class=\"tableContent\" colspan=\"100%\" align=\"center\">".$Lang['General']['NoRecordAtThisMoment']."</td>";
			}
			else{
				$html .= "<td class=\"tableContent\">&nbsp;1&nbsp;</td>
						  <td class=\"tabletext tablerow\">".$UserInfo[0]['ClassName']." - ".$UserInfo[0]['ClassNumber']."</td>
					      <td class=\"tabletext tablerow\">".$UserInfo[0]['UserName']."</td>
						  <td class=\"tabletext tablerow\">
							<input type=\"checkbox\" class=\"L_row_$DistributionNo\" name=\"row_".$DistributionNo."_[]\" value=\"".$UserInfo[0]['DistributionUserID']."\"/>
						  </td>";
			}
							  
			$html .= "	</tr>
					  </table>";
		}
		else{
			$html .= "<table class=\"common_table_list_v30\">
						<tr class=\"tabletop\">
							<th width=\"1\" style=\"vertical-align: middle;\" class=\"tabletop tabletopnolink\">&nbsp;#&nbsp;</th>
							<th width=\"20%\" style=\"vertical-align: middle;\" class=\"tabletop\">".$Lang['itextbook']['Class']."</th>
							<th width=\"30%\" style=\"vertical-align: middle;\" class=\"tabletop\">".$Lang['itextbook']['Name']."</th>
							<th width=\"1\" style=\"vertical-align: middle; border-right-width: 3px\" class=\"tabletop\">
								<input type=\"checkbox\" id=\"L_check_all_$DistributionNo\" name=\"L_check_all_$DistributionNo\" onclick=\"check_all_row('L', $DistributionNo)\"/>
							</th>
							<th width=\"1\" style=\"vertical-align: middle;\" class=\"tabletop tabletopnolink\">&nbsp;#&nbsp;</th>
							<th width=\"20%\" style=\"vertical-align: middle;\" class=\"tabletop\">".$Lang['itextbook']['Class']."</th>
							<th width=\"30%\" style=\"vertical-align: middle;\" class=\"tabletop\">".$Lang['itextbook']['Name']."</th>
							<th width=\"1\" style=\"vertical-align: middle;\" class=\"tabletop\">
								<input type=\"checkbox\" id=\"R_check_all_$DistributionNo\" name=\"R_check_all_$DistributionNo\" onclick=\"check_all_row('R', $DistributionNo)\"/>
							</th>
						</tr>";
			
			for($i=0;$i<count($UserInfo);$i+=2){
				$L_no 		 	= $i+1;
				$L_UserName  	= $UserInfo[$i]['UserName'];
				$L_ClassNumber 	= $UserInfo[$i]['ClassNumber'];
				$L_ClassName 	= $UserInfo[$i]['ClassName'];
				$L_Checkbox		= "<input type=\"checkbox\" class=\"L_row_$DistributionNo\" name=\"row_".$DistributionNo."_[]\" value=\"".$UserInfo[$i]['DistributionUserID']."\"/>";
				$R_no 		 	= ($i+1)<count($UserInfo)? $i+2 : "";
				$R_UserName  	= ($i+1)<count($UserInfo)? $UserInfo[$i+1]['UserName'] : "";
				$R_ClassNumber 	= ($i+1)<count($UserInfo)? $UserInfo[$i+1]['ClassNumber'] : "";
				$R_ClassName 	= ($i+1)<count($UserInfo)? $UserInfo[$i+1]['ClassName'] : "";
				$R_Checkbox		= ($i+1)<count($UserInfo)? "<input type=\"checkbox\" class=\"R_row_$DistributionNo\" name=\"row_".$DistributionNo."_[]\" value=\"".$UserInfo[$i+1]['DistributionUserID']."\"/>" : "";
				
				$html .= "<tr>
							<td class=\"tableContent\">&nbsp;$L_no&nbsp;</td>
					      	<td class=\"tabletext tablerow\">$L_ClassName - $L_ClassNumber</td>
						  	<td class=\"tabletext tablerow\">$L_UserName</td>
						  	<td class=\"tabletext tablerow\" style=\"border-right-width: 3px\">$L_Checkbox</td>
						  	<td class=\"tableContent\">".($R_no? "&nbsp;$R_no&nbsp;" : "&nbsp;")."</td>
					      	<td class=\"tabletext tablerow\">".($R_ClassName || $R_ClassNumber? "$R_ClassName - $R_ClassNumber" : "&nbsp")."</td>
						  	<td class=\"tabletext tablerow\">".($R_UserName? $R_UserName:"&nbsp")."</td>
						  	<td class=\"tabletext tablerow\">".($R_Checkbox? $R_Checkbox:"&nbsp")."</td>
						  </tr>";
			}
						
			$html .= "</table>";
		}
		
		return $html;
	}
	
	/*
	public function gen_distribution_info_table_bak($ModuleCode, $DistributionID, $DistributionNo=1){
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $ec_iPortfolio, $iTextbook_cfg;
		
		$Distribution_Info = $this->get_distribution_info($DistributionID);
		
		$cooldown_end  = date('Y-m-d H:i:s', strtotime($Distribution_Info['DateInput']) + ($iTextbook_cfg[$ModuleCode]['cooldown'] * 24 * 60 * 60));
		$IsCooldownEnd = $this->Is_Cooldown_passed($iTextbook_cfg[$ModuleCode]['cooldown'], $Distribution_Info['DateInput']);
		$BookInfo	   = $this->get_book_info($Distribution_Info['BookID'], false);
		$Book_html	   = $IsCooldownEnd? $BookInfo['BookName'] : $this->gen_book_select($ModuleCode, $Distribution_Info['BookID']);
		
		$html = "<table width=\"85%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
					<tr>
						<td>
							<table width=\"100%\">
								<col width=\"20%\"/>
								<col width=\"1%\"/>
								<col width=\"*\"/>
								<tr>
									<td colspan=\"3\" class=\"tabletext\">
										<img height=\"20\" src=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/icon_section.gif\" width=\"20\" align=\"absMiddle\"/>
										<span class=\"sectiontitle\">Distribution $DistributionNo</SPAN>
									</td>
								</tr>
								<tr>
									<td>Cooldown period end at</td>
									<td>:</td>
									<td>$cooldown_end</td>
								</tr>
								<tr>
									<td>Book</td>
									<td>:</td>
									<td>$Book_html</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align=\"right\">
							<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
				          		<tr>
				            		<td width=\"21\"><img src=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/management/table_tool_01.gif\" height=\"23\" width=\"21\"></td>
				            		<td background=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/management/table_tool_02.gif\">
				              			<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\">
											<tr>
				                    			<td><img src=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/management/10x10.gif\" width=\"5\"></td>
				                    			<td nowrap=\"nowrap\">
													<a class=\"tabletool\" href=\"javascript:void(0)\" onclick=\"javascript:void(0)\">
														<img src=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif\" name=\"imgDelete\" align=\"absmiddle\" border=\"0\" >".$ec_iPortfolio['delete']."
													</a>
												</td>
				                  			</tr>
				              			</table>
				            		</td>
				            		<td width=\"6\"><img src=\"".$PATH_WRT_ROOT."/".$image_path."/".$LAYOUT_SKIN."/management/table_tool_03.gif\" height=\"23\" width=\"6\"></td>
				          		</tr>
				        	</table>
						</td>
					</tr>
					<tr>
						<td>
							".$this->gen_distribution_user_table($DistributionID)."
							<br />
				  			<p class=\"spacer\"></p>
							<div class=\"content_top_tool\">
				  				<div class=\"Conntent_tool\">
				  					<a title=\"Add New Book\" href=\"javascript:add_student($DistributionID)\">Add Student to this Distribution</a>
				  				</div>
							</div>
						</td>
					</tr>
				</table>
				<div id=\"distribution_separator\">
					<hr/><br /><p class=\"spacer\"></p>
			  	</div>";
		
		return $html;
	}
	*/
	
	/*
	public function gen_book_select($BookType, $selected_BookID="", $id="BookID", $name="BookID", $other_attr=""){
		global $iTextbook_cfg;
		
		$sql = "SELECT
					BookID, BookName
				FROM
					ITEXTBOOK_BOOK
				WHERE
					BookType = '$BookType' AND Status = ".$iTextbook_cfg['db_setting']['book']['status']['not_deleted'];
		$BookAry = $this->returnArray($sql);
		
		$html .= "<select id=\"$id\" name=\"$name\" $other_attr>";
		for($i=0;$i<count($BookAry);$i++){
			list($BookID, $BookName) = $BookAry[$i];
			$html .= "<option value=\"$BookID\" ".($selected_BookID==$BookID? "selected":"").">$BookName</option>";
		}
		$html .= "</select>";
		
		return $html;
	}
	*/
	
	public function get_books_info($BookType){
		global $iTextbook_cfg;
		
		$sql = "SELECT
					BookID, BookName
				FROM
					ITEXTBOOK_BOOK
				WHERE
					BookType = '$BookType' AND Status = '".$iTextbook_cfg['db_setting']['book']['status']['not_deleted']."'
				ORDER BY
					BookName";
		$BookAry = $this->returnArray($sql);
		
		$result_ary = array();
		for($i=0;$i<count($BookAry);$i++){
			$result_ary[$BookAry[$i]['BookID']] = $BookAry[$i]['BookName'];
		}
		
		return $result_ary;
	}
	
	public function get_book_info($BookID, $withUserNo=false){
		global $iTextbook_cfg;
		
		$field = "ib.BookID, ib.BookType, ib.BookName, ib.BookLang, ib.Chapters, ib.Status,
				  ib.DateInput, ib.DateModified, ".getNameFieldByLang("iu.")." AS ModifiedBy";
		
		if($withUserNo){
			$field     .= ", IFNULL(tmp_d.UserCount, 0) AS UserCount";
			$join_table = " LEFT JOIN
							(
								SELECT
									id.BookID,
									COUNT(*) AS UserCount
								FROM
									ITEXTBOOK_DISTRIBUTION AS id INNER JOIN
									ITEXTBOOK_DISTRIBUTION_USER AS idu ON id.DistributionID = idu.DistributionID
								WHERE
									id.BookID = '$BookID'
								GROUP BY
									id.BookID
							) AS tmp_d ON ib.BookID = tmp_d.BookID";
		}
		
		$sql = "SELECT
					$field
				FROM
					ITEXTBOOK_BOOK AS ib INNER JOIN
					INTRANET_USER AS iu ON ib.ModifiedBy = iu.UserID
					$join_table
				WHERE
					ib.BookID = $BookID";
		
		$BookInfo = current($this->returnArray($sql));
		$BookInfo['Chapters'] = explode($iTextbook_cfg['db_setting']['book']['chapter']['dilimeter'], $BookInfo['Chapters']); 
		
		return $BookInfo;
	}
	
	public function get_distributions_used_quota($ModuleLicenseID, $Module_Code){
		global $iTextbook_cfg;
		
		$sql = "SELECT
					COUNT(*)*".$iTextbook_cfg[$Module_Code]['MaxChapters']."
				FROM
					ITEXTBOOK_DISTRIBUTION AS id INNER JOIN
					ITEXTBOOK_DISTRIBUTION_USER AS idu ON id.DistributionID = idu.DistributionID
				WHERE
					ModuleLicenseID = '$ModuleLicenseID'";
		
		return current($this->returnVector($sql));
	}
	
	public function get_distributions_total_quota($ModuleLicenseID){
		$sql = "SELECT
					NumberOfLicense
				FROM
					INTRANET_MODULE_LICENSE
				WHERE
					ModuleLicenseID = '$ModuleLicenseID'";
		return current($this->returnVector($sql));
	}
	
	public function get_distributions_quota_left($ModuleLicenseID, $Module_Code){
		$total_quota = $this->get_distributions_total_quota($ModuleLicenseID);
		$used_quota  = $this->get_distributions_used_quota($ModuleLicenseID, $Module_Code);
		
		return ($total_quota - $used_quota);
	}
	
	public function get_license_expiry_date($ModuleLicenseID){
		$sql = "SELECT
					ExpiryDate
				FROM
					INTRANET_MODULE_LICENSE
				WHERE
					ModuleLicenseID = '$ModuleLicenseID'";
		return current($this->returnVector($sql));
	}
	
	public function check_itextbook_expiried($code, $par_user_id){
		global $intranet_root, $iTextbook_cfg;
		
		if($iTextbook_cfg[$code]['LicenseType']['HaveExpiryDate']){
			include_once($intranet_root."/includes/libuser.php");
			$lu = new libuser($par_user_id);
			
			if($lu->isTeacherStaff()){
				$sql = "SELECT
							DISTINCT iml.ExpiryDate
						FROM
							INTRANET_MODULE_LICENSE AS iml INNER JOIN
							INTRANET_MODULE AS im ON iml.ModuleID = im.ModuleID
						WHERE
							im.Code = '$code'";
			}else{
				$sql = "SELECT
							DISTINCT iml.ExpiryDate
						FROM
    						INTRANET_MODULE AS im INNER JOIN
    						INTRANET_MODULE_LICENSE AS iml ON im.ModuleID = iml.ModuleID INNER JOIN 
    						ITEXTBOOK_DISTRIBUTION AS id ON iml.ModuleLicenseID = id.ModuleLicenseID INNER JOIN 
    						ITEXTBOOK_DISTRIBUTION_USER AS idu ON id.DistributionID = idu.DistributionID
						WHERE
    						im.Code = '$code' AND
							idu.UserID = '$par_user_id'";
			}
			$expiry_date_ary = $this->returnVector($sql);
			if(in_array(NULL,$expiry_date_ary)){
				return false;
			}else{
				$most_recent_date = 0;
				foreach($expiry_date_ary as $_date){
				  $_cur_date = strtotime($_date);
				  if ($_cur_date > $most_recent_date) {
				     $most_recent_date = $_cur_date;
				  }
				}
			}
			return time() > $most_recent_date;
		}
		
		return false;
	}
	
	public function get_chapter_page_ary($itextbook_code){
		global $eclass40_filepath;
		
		include($eclass40_filepath.'/src/plugin/'.$itextbook_code.'/config.php');
		
		$chapter_page_ary = array();
		
		switch($itextbook_code){
			case 'ambook':
				foreach($ab_cfg['chapter'] as $sub=>$sub_obj){
					foreach($sub_obj as $unit_code=>$unit_obj){
						foreach($unit_obj as $chapter_code=>$chapter_obj){
							$chapter_page_ary[] = $chapter_obj;
						}
					}
				}
				break;
			case 'poemsandsongs':
				foreach($poemsandsongs_cfg['chapter'] as $chapter_key=>$chapter_ary){
					$chapter_page_ary[] = $chapter_ary;
				}
				break;
		}
		
		return $chapter_page_ary;
	}
	
	public function get_chapter_page_view_ary($itextbook_code){
		global $intranet_root, $eclass_db;
		
		include_once($intranet_root."/includes/libeclass40.php");
		
		$sql = "SELECT course_id FROM $eclass_db.course WHERE course_code = '$itextbook_code' AND RoomType = 7";
		$course_id = current($this->returnVector($sql));
		
		$sql = "SELECT
					a_no, b_no, readflag
				FROM
					".classNamingDB($course_id).".notes
				WHERE
					a_no != 0 AND a_no IS NOT NULL AND
					b_no != 0 AND b_no IS NOT NULL AND
					c_no IS NULL AND
					d_no IS NULL AND
					e_no IS NULL
				ORDER BY
					a_no, b_no";
		$tmp_notes_ary = $this->returnArray($sql);
		
		$notes_ary = array();
		$notes_ary_x = 1;
		$notes_ary_y = 0;
		$a_no = $tmp_notes_ary[0]['a_no'];
		$b_no = $tmp_notes_ary[0]['b_no'];
		
		for($i=0;$i<count($tmp_notes_ary);$i++){
			list($tmp_a_no, $tmp_b_no, $readflag_str) = $tmp_notes_ary[$i];
			
			if($a_no!=$tmp_a_no){
				$a_no = $tmp_a_no; 
				$notes_ary_x++;
				$notes_ary_y = 0;
			}
			else{
				if($b_no!=$tmp_b_no){
					$b_no = $tmp_b_no; 
					$notes_ary_y++;
				}
			}
			
			$readflag_ary = $readflag_str? unserialize($readflag_str):array();
			$readflag_ary = is_array($readflag_ary)? $readflag_ary : array();
			
			$notes_ary[$notes_ary_x][$notes_ary_y] = $readflag_ary;
		}
		
		return $notes_ary;
	}
	
	public function get_chapter_page_view_ary_with_filter($itextbook_code, $selected_YearClassID=0, $selected_chapter=0){
		global $intranet_root, $intranet_db, $eclass_db, $CurrentSchoolYearInfo;
		
		$filtered_ary = array();
		
		$notes_ary = $this->get_chapter_page_view_ary($itextbook_code);
		
		# Handle the selected user
		include_once($intranet_root."/includes/libeclass40.php");
		
		$sql = "SELECT course_id FROM $eclass_db.course WHERE course_code = '$itextbook_code' AND RoomType = 7";
		$course_id = current($this->returnVector($sql));
		
		$sql = "SELECT
					u.user_id
				FROM
					".classNamingDB($course_id).".usermaster AS u INNER JOIN
					".$intranet_db.".INTRANET_USER AS iu ON u.user_email = iu.UserEmail AND u.status IS NULL AND iu.RecordStatus IN (0, 1, 2) AND iu.RecordType = '".USERTYPE_STUDENT."' INNER JOIN
					".$intranet_db.".YEAR_CLASS_USER AS ycu ON iu.UserID = ycu.UserID AND ycu.YearClassID";
		
		if($selected_YearClassID){
			$sql .= " = ".$selected_YearClassID;
		}
		else{
			$sql .= " IN (SELECT yc.YearClassID FROM YEAR_CLASS AS yc WHERE yc.AcademicYearID = '".$CurrentSchoolYearInfo['AcademicYearID']."')";
		}
		$user_id_ary = $this->returnVector($sql);
		
		foreach($notes_ary as $unit=>$page_ary){
			foreach($page_ary as $page=>$page_view_count){
				if(is_array($page_view_count['user_view_count']) && count($page_view_count['user_view_count'])){
					$page_view_count_user_id = array_keys($page_view_count['user_view_count']);
					
					for($i=0;$i<count($page_view_count_user_id);$i++){
						if(!is_array($user_id_ary) || !in_array($page_view_count_user_id[$i], $user_id_ary))
							unset($notes_ary[$unit][$page]['user_view_count'][$page_view_count_user_id[$i]]);
					}
				}
			}
		}
		
		# Handle the view count
		if($selected_chapter){
			foreach($notes_ary[$selected_chapter] as $page=>$page_view_count){
				$filtered_ary[$selected_chapter][$page] = is_array($page_view_count['user_view_count']) && count($page_view_count['user_view_count'])? array_sum($page_view_count['user_view_count']):0;
			}
		}
		else{
			foreach($notes_ary as $unit=>$page_ary){
				$view_count = 0;
				foreach($page_ary as $page=>$page_view_count){
					$view_count += is_array($page_view_count['user_view_count']) && count($page_view_count['user_view_count'])? array_sum($page_view_count['user_view_count']):0;
				}
				$filtered_ary[$unit] = $view_count;
			}
		}
		
		return $filtered_ary;
	}
	function sync_grammartouch_license($studentAry,$course_code,$distribution_id,$module_license_id){
		global $config_school_code,$plugin,$UserID;
		if(isset($plugin['iTextbook_dev']) && $plugin['iTextbook_dev']){
			$api_path = "http://gt-dev.eclass.com.hk/";
		}
		elseif (isset($plugin['iTextbook']) && $plugin['iTextbook'])
		{
			$api_path = "http://gt.eclass.com.hk/";
		}
			
		$gt_ts = time();
		$gt_action = "add_user_to_distribution";
		$gt_para = "ts=".$gt_ts."&action=".$gt_action."&school_code=".$config_school_code."&distribution_id=".$distribution_id."&module_license_id=".$module_license_id."&input_by=".$UserID;
		for($i=0;$i<count($studentAry);$i++){
			$gt_para .= "&student_ids[]=".$studentAry[$i];
		}
		$gt_enc = encrypt_string($gt_para);
		$gt_ch = curl_init($api_path."gt_access.php?enc=".$gt_enc); 
		curl_setopt($gt_ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($gt_ch, CURLOPT_HEADER, false);
		curl_setopt($gt_ch, CURLOPT_USERAGENT, "eClass");
		curl_setopt($gt_ch, CURLOPT_TIMEOUT_MS, 5000);
		curl_setopt($gt_ch, CURLOPT_FOLLOWLOCATION, true);
		$result = curl_exec($gt_ch);
		curl_close($gt_ch); 
	}	
	function delete_grammartouch_user_from_distribution($studentAry,$distribution_id){
		global $config_school_code,$plugin,$UserID;
		if(isset($plugin['iTextbook_dev']) && $plugin['iTextbook_dev']){
			$api_path = "http://gt-dev.eclass.com.hk/";
		}
		elseif (isset($plugin['iTextbook']) && $plugin['iTextbook'])
		{
			$api_path = "http://gt.eclass.com.hk/";
		}
			
		$gt_ts = time();
		$gt_action = "delete_user_from_distribution";
		$gt_para = "ts=".$gt_ts."&action=".$gt_action."&school_code=".$config_school_code."&distribution_id=".$distribution_id."&delete_by=".$UserID;
		for($i=0;$i<count($studentAry);$i++){
			$gt_para .= "&student_ids[]=".$studentAry[$i];
		}
		$gt_enc = encrypt_string($gt_para);
		$gt_ch = curl_init($api_path."gt_access.php?enc=".$gt_enc);
		curl_setopt($gt_ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($gt_ch, CURLOPT_HEADER, false);
		curl_setopt($gt_ch, CURLOPT_USERAGENT, "eClass");
		curl_setopt($gt_ch, CURLOPT_TIMEOUT_MS, 5000);
		curl_setopt($gt_ch, CURLOPT_FOLLOWLOCATION, true);
		$result = curl_exec($gt_ch);
		curl_close($gt_ch); 
	}
	function delete_grammartouch_distribution($distribution_id){
		global $config_school_code,$plugin,$UserID;
		if(isset($plugin['iTextbook_dev']) && $plugin['iTextbook_dev']){
			$api_path = "http://gt-dev.eclass.com.hk/";
		}
		elseif (isset($plugin['iTextbook']) && $plugin['iTextbook'])
		{
			$api_path = "http://gt.eclass.com.hk/";
		}
			
		$gt_ts = time();
		$gt_action = "delete_distribution";
		$gt_para = "ts=".$gt_ts."&action=".$gt_action."&school_code=".$config_school_code."&distribution_id=".$distribution_id."&delete_by=".$UserID;

		$gt_enc = encrypt_string($gt_para);
		$gt_ch = curl_init($api_path."gt_access.php?enc=".$gt_enc);
		curl_setopt($gt_ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($gt_ch, CURLOPT_HEADER, false);
		curl_setopt($gt_ch, CURLOPT_USERAGENT, "eClass");
		curl_setopt($gt_ch, CURLOPT_TIMEOUT_MS, 5000);
		curl_setopt($gt_ch, CURLOPT_FOLLOWLOCATION, true);
		$result = curl_exec($gt_ch);
		curl_close($gt_ch); 
	}	
}

?>