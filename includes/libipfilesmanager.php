<?php
# Using By: Paul
class libipfilesmanager {
	private $userId;
	private $moduleCode;
	private $fileList;	// array of (Object) PortalFile
	
	private $libdb;
	private $order;
	private $sortBy;
	private $pageNum;
	private $numPerPage;
	private $search;
	
	public function __construct($ParUserId, $ParModuleCode=null) {
		$this->userId = $ParUserId;
		$this->moduleCode = $ParModuleCode;
		$this->fileList = array();
		$this->libdb = new libdb();
		
		$this->sortBy = " DATEINPUT ";
		$this->order = " DESC ";
	}
	
	/**
	 * Get the file extenstion,
	 * this is a static function that can be called independently without initializing this object.
	 * the calling syntax is like libipfilesmanager::getFileExtention
	 * @param String $myFile - the file name e.g.File.doc
	 * @param boolean $isLower - true, return the file extension in lower case, vice versa
	 * @return String - a string of file extension e.g. doc
	 */
	static public function getFileExtention($myFile, $isLower=false) {
		$filetmp = explode(".", $myFile);
		$rx = $filetmp[sizeof($filetmp)-1];
		$rx = ($isLower) ? strtolower($rx) : strtoupper($rx);

		return $rx;
	}

	
	/**
	 * Get the file icon for differenct type of file,
	 * this is a static function that can be called independently without initializing this object.
	 * the calling syntax is like libipfilesmanager::getFileIcon
	 * @param String $filename - the file name that the file required to have an icon e.g.File.doc
	 * @param boolean $isFolder - true, the file require an icon is a folder
	 * @return String - html icon string e.g.<img src="/images/2009a/ies/student/file/doc.gif" align="absmiddle" border="0" hspace="3" vspace="1">
	 */
	static public function getFileIcon($filename, $isFolder=false){
		global $image_path;

		if ($isFolder)
		{
			$icon = "folder.gif";
		} else
		{
			if (true)
			{
				$extension = self::getFileExtention($filename, true);
				switch ($extension)
				{
					case "htm":
					case "html":
					case "shtml":
					case "mht":
									$icon = "htm.gif";
									break;
					case "doc":
					case "docx":
					case "rtf":
									$icon = "doc.gif";
									break;
					case "epc":
									$icon = "epc.gif";
									break;
					case "epb":
									$icon = "epb.gif";
									break;
					case "gif":
					case "jpg":
					case "jpe":
					case "jpeg":
					case "png":
					case "tif":
					case "bmp":
									$icon = "jpg.gif";
									break;
					case "ppt":
									$icon = "ppt.gif";
									break;
					case "pdf":
									$icon = "pdf.gif";
									break;
					case "csv":
					case "xls":
					case "dif":
									$icon = "xls.gif";
									break;
					case "swf":
									$icon = "swf.gif";
									break;
					case "zip":
					case "rar":
					case "ace":
					case "arc":
					case "arj":
					case "gz":
					case "iso":
					case "lha":
					case "lzh":
					case "tar":
					case "tz":
									$icon = "zip.gif";
									break;
					case "fla":
									$icon = "fla.gif";
									break;
					case "mid":
					case "midi":
					case "mp3":
					case "ra":
					case "wav":
					case "wma":
									$icon = "mp3.gif";
									break;
					case "avi":
					case "wmv":
					case "rm":
					case "mov":
					case "mpg":
					case "mp4":
					case "mpe":
					case "mpeg":
					case "ram":
					case "asf":
									$icon = "avi.gif";
									break;
					case "eml":
									$icon = "eml.gif";
									break;
					default:
									$icon = "txt.gif";
									break;
				}
			} else
			{
				$icon = "file.gif";
			}
		}
		$rx = "<img src=\"$image_path/2009a/file/$icon\" border='0' hspace='3' vspace='1' align='absmiddle'/>";

		return $rx;
	}


	/**
	 * Get the total size of file usage in iportal files for this user
	 * @return INT - the total size in kb
	 */
	public function getUserFileUsage() {
		global $intranet_db;
		$sql = <<<SQLEND
		SELECT SUM(SIZE) FROM {$intranet_db}.IPORTAL_FILE WHERE USERID = {$this->userId}
SQLEND;
		$result = current($this->libdb->returnVector($sql));
		return $result;
	}
	/**
	 * Get the total size of file usage in iportal files for this user for specified module
	 * @return INT/boolean - the total size in kb, if does not specify module for this Object will return false
	 */
	public function getUserModuleFileUsage() {
		global $intranet_db;
		if (isset($this->moduleCode)) {
			$sql = <<<SQLEND
			SELECT SUM(SIZE) FROM {$intranet_db}.IPORTAL_FILE WHERE USERID = {$this->userId} AND CODE = '{$this->moduleCode}'
SQLEND;
			$result = current($this->libdb->returnVector($sql));
		} else {
			$result = false;
		}
		return $result;
	}
	/**
	 * Get the total stored size for iportal file
	 * @return INT - total stored size in kb
	 */
	static public function getTotalStoredSize() {
		global $intranet_db;
		$sql = <<<SQLEND
			SELECT SUM(SIZE) FROM {$intranet_db}.IPORTAL_FILE;
SQLEND;
		$libdb = new libdb();
		$result = current($libdb->returnVector($sql));
		return $result;
	}
	
	/**
	 * Get the Maximum Storage Size for a iportal file
	 * @return INT/boolean - storage size in kb, false if does not set the max storage size
	 */
	static public function getMaxStorageSize() {
		// TODO: not implemented yet until DB schema is decided
		return false;
	}
	
	/**
	 * Get the Free space left in iportal file
	 * @return INT/boolean - free space in kb, false if does not set the max storage size
	 */
	static public function getTotalFreeSpace() {
		if (!self::getMaxStorageSize()) {
			$result = false;
		} else {
			$result = self::getMaxStorageSize() - self::getTotalStoredSize();
		}
		return $result;
	}
	
	/**
	 * Get the Maximum Storage Size for a module
	 * @param String $ParModuleCode - the module code
	 * @return INT/boolean - storage size in kb, false if does not set the max storage size
	 */
	static public function getModuleMaxStorageSize($ParModuleCode) {
		global $intranet_db;
		$sql = <<<SQLEND
			SELECT MODULEMAXSIZE FROM {$intranet_db}.IPORTAL_FILE_MODULE WHERE CODE = '{$ParModuleCode}'
SQLEND;
		$libdb = new libdb();
		$result = current($libdb->returnVector($sql));
		if (!isset($result)) {
			$result = false;
		}		
		return $result;
	}
	
	/**
	 * Get the user's total number of files
	 * @return INT - number of files stored in iportal file
	 */
	public function getTotalNumberOfFiles() {
		global $intranet_db;
		$sql = <<<SQLEND
			SELECT COUNT(*) FROM {$intranet_db}.IPORTAL_FILE WHERE USERID = $this->userId
SQLEND;
		$libdb = new libdb();
		$result = current($libdb->returnVector($sql));
		return $result;
	}
	
	/**
	 * Get the user's total number of files
	 * @return INT - number of files stored in iportal file
	 */
	public function getTotalNumberOfModuleFiles() {
		global $intranet_db;
		$sql = <<<SQLEND
			SELECT COUNT(*) FROM {$intranet_db}.IPORTAL_FILE WHERE USERID = $this->userId AND CODE = '{$this->moduleCode}'
SQLEND;
		$libdb = new libdb();
		$result = current($libdb->returnVector($sql));
		return $result;
	}
	
	/**
	 * @param String $ParPageToProcessUpload - a php path that the php process the upload
	 * @return String - the HTML string for an upload box
	 */
	static public function getUploadFileFieldHtml($ParPageToProcessUpload) {
		global $Lang;
		$HTML = <<<HTMLEND
			<form target="upload_target" name="upload_form" action="{$ParPageToProcessUpload}" method="post" enctype="multipart/form-data" >
		        <input type="file" name="submitFiles[]" onchange="javascript: document.upload_form.submit();"/><br/>
	        </form>
	        <iframe id="upload_target" name="upload_target" src="#" style="width:400;height:500;border:1px solid #fff;"></iframe>
HTMLEND;
		return $HTML;
	}
	/**
	 * Get files information
	 * @return Array - a list of PortalFile
	 */
	private function getFileListFromDB() {
		$fileIds = $this->getFileIds();
		foreach($fileIds as $key => $fileId) {
			$PortalFiles[] = new PortalFile($fileId);
		}
		return $PortalFiles;
	}
	
	/**
	 * Get File Ids by a user id
	 * @return Array Vector Array of file ids
	 */
	public function getFileIds() {
		global $intranet_db;
		
		# WHERE
		$where = " USERID = $this->userId ";		
		if (isset($this->moduleCode)) {
			$where .= " AND CODE = '{$this->moduleCode}' ";
		} else {
			// do nothing
		}
		
		# ORDER BY , sortBy and order had default value assigned in constructor
		if (trim($this->sortBy) == "DOCTYPE") {
			$order = " REVERSE(
								SUBSTRING(
									REVERSE(FILENAME),1,LOCATE('.', REVERSE(FILENAME)) - 1
								)
							)";
		} else {
			$order = " $this->sortBy ";
		}
		$order .= " $this->order ";
		
		# MATCH
		if (isset($this->search)) {
			$like = " AND (FILENAME LIKE '%{$this->search}%' || SIZE LIKE '%{$this->search}%' || DATEINPUT LIKE '%{$this->search}%') ";
		} else {
			// do nothing
		}
		
		# LIMIT
		if (!empty($this->pageNum) && !empty($this->numPerPage)) {
			$limit = " LIMIT ".($this->pageNum-1)*$this->numPerPage.",".$this->numPerPage;
		} else {
			// do nothing
		}
		
		
		$cond = " WHERE $where $like ORDER BY $order $limit";
		$sql = "SELECT FILEID FROM $intranet_db.IPORTAL_FILE $cond";
		$fileIds = $this->libdb->returnVector($sql);
		return $fileIds;
	}
	
	/**
	 * Upload a file that stored in session parameter $_FILES by calling the PortfalFile's Upload
	 * @return Array	- a list of upload result
	 */
	public function upload() {
		$uploadResult = array();
		$isUpload = true;
		if (isset($this->moduleCode)) {
			$isUpload = !$this->isReachMaxSize();
		}
		if ($isUpload) {
			foreach($_FILES as $fileSetName => $oneSetFile) {
				$uploadResult = $this->uploadOneSetFiles($oneSetFile);
			}
		}
		return $uploadResult;
	}
	
	/**
	 * Check if reaching the max size for a module if uploaded
	 * @return boolean true, if reached the max size, vice versa
	 */
	private function isReachMaxSize() {
		global $intranet_db;
		#######################################
		## Calculate total size if uploaded this batch of files
		foreach($_FILES as $fileSetName => $oneSetFile) {
			$numOfFiles = count($oneSetFile["name"]);
			for($i=0;$i<$numOfFiles;$i++) {
				$fileTmpName = $oneSetFile["tmp_name"][$i];
				$totalUploadFileSize += round(filesize($fileTmpName)/1024+1,PHP_ROUND_HALF_DOWN);	// calculate in kb
			}
		}
		$totalSizeIfUpload = $totalUploadFileSize + $this->getUserModuleFileUsage();
		#######################################
		
		
		#######################################
		## Get Max Size can be uploaded
		$availableSize = self::getModuleMaxStorageSize($this->moduleCode);
		#######################################
		
		if (isset($availableSize) && $availableSize<$totalSizeIfUpload) {
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * Upload a set of files
	 * @param Array $ParOneSetFiles - Information Array of a set of files from $_FILE["_setName_"]
	 * @return Array - a list of upload result
	 */
	private function uploadOneSetFiles($ParOneSetFiles) {
		$numOfFiles = count($ParOneSetFiles["name"]);
		for($i=0;$i<$numOfFiles;$i++) {
			$PortalFile = new PortalFile();
			$PortalFile->setUserId($this->userId);
			$PortalFile->setCode($this->moduleCode);
			
			$fileTmpName = $ParOneSetFiles["tmp_name"][$i];
			$fileName = $ParOneSetFiles["name"][$i];

			if (!empty($fileName)) {
				$fileUploadResult = $PortalFile->upload($fileTmpName,$fileName);
				if ($fileUploadResult) {
					$res[] = array("FILENAME"=>$fileName, "UPLOAD_RESULT"=>"1", "FILEID"=>$fileUploadResult);
				} else {
					$res[] = array("FILENAME"=>$fileName, "UPLOAD_RESULT"=>"0");
				}
			} 
		}
		return $res;
	}

	######################################
	##	Start: Accessors
	######################################
	public function getUserId() {
		return $this->userId;
	}
	public function getModuleCode() {
		return $this->moduleCode;
	}
	public function getFileList() {
		if (count($this->fileList)>0) {
			$returnArray = $this->fileList;
		} else {
			$returnArray = $this->getFileListFromDB();
		}
		return $returnArray;
	}
	public function getLibdb() {
		return $this->libdb;
	}
	public function getOrder() {
		return $this->order;
	}
	public function getSortBy() {
		return $this->sortBy;
	}
	public function getPageNum() {
		return $this->pageNum;
	}
	public function getNumPerPage() {
		return $this->numPerPage;
	}
	public function getSearch() {
		return $this->search;
	}
	######################################
	##	End: Accessors
	######################################
	
	######################################
	##	Start: Mutators
	######################################
	public function setUserId($value) {
		$this->userId=$value;
	}
	public function setModuleCode($value) {
		$this->moduleCode=$value;
	}
	public function setFileList($value) {
		if (is_array($value)) {
			$this->fileList=$value;
		} else {
			$this->fileList=array($value);
		}
	}
	public function setLibdb($value) {
		$this->libdb=$value;
	}
	public function setOrder($value) {
		$this->order=$value;
	}
	public function setSortBy($value) {
		$this->sortBy=$value;
	}
	public function setPageNum($value) {
		$this->pageNum=$value;
	}
	public function setNumPerPage($value) {
		$this->numPerPage=$value;
	}
	public function setSearch($value) {
		$this->search=$value;
	}
	######################################
	##	End: Mutators
	######################################
}


class PortalFile extends libfilesystem {
	
	private $fileId;
	private $fileName;
	private $folderPath;
	private $fileHashName;
	private $userId;
	private $dateInput;
	private $inputBy;
	private $dateModified;
	private $modifyBy;
	private $size; // save in KB
	private $code;
		
	private $FOLDER_PATH_PREFIX;
	private $DB_TABLE;
	
	private $sessionFile;
	
	private $libdb;
	
	######################################
	##	Start: Constructors Group
	######################################
	public function __construct($ParFileId=null) {
		global $intranet_db;
		$this->FOLDER_PATH_PREFIX = "/file/ipfiles/";
		$this->DB_TABLE = $intranet_db.".IPORTAL_FILE";
		$this->libdb = new libdb();
		if ($ParFileId) {
			$this->fileId = $ParFileId;
			$this->initPortalFile();
		} else {
			// do nothing
		}
	}
	######################################
	##	End: Constructors Group
	######################################
	
	######################################
	##	Start: Functions
	######################################
	/**
	 * Initialize this file's information
	 */
	private function initPortalFile() {
		$fileInfo = $this->getFileInfoFromDB();
		$this->fileId = $fileInfo["FILEID"];
		$this->fileName = $fileInfo["FILENAME"];
		$this->folderPath = $fileInfo["FOLDERPATH"];
		$this->fileHashName = $fileInfo["FILEHASHNAME"];
		$this->userId = $fileInfo["USERID"];
		$this->dateInput = $fileInfo["DATEINPUT"];
		$this->inputBy = $fileInfo["INPUTBY"];
		$this->dateModified = $fileInfo["DATEMODIFIED"];
		$this->modifyBy = $fileInfo["MODIFYBY"];
		$this->size = $fileInfo["SIZE"];
		$this->code = $fileInfo["CODE"];
	}
	/**
	 * Get this file information from the database
	 * @return array - file information
	 */
	private function getFileInfoFromDB() {
		$sql = "SELECT 
					FILEID, 
					FILENAME, 
					FOLDERPATH, 
					FILEHASHNAME, 
					USERID, 
					DATEINPUT, 
					INPUTBY, 
					DATEMODIFIED, 
					MODIFYBY,
					SIZE,
					CODE
				FROM
					{$this->DB_TABLE}
				WHERE
					FILEID = $this->fileId";
		$returnArray = array();
		$returnArray = current($this->libdb->returnArray($sql));
		return $returnArray;
	}
	/**
	 * Perform File Upload, both physically and set data to DB
	 * @param String $ParFileTmpName - the tmp_name in $_FILE
	 * @param String $ParFileName - the name in $_FILE
	 * @return boolean true if successfully uploaded file physically and inserted record to database, vice versa
	 */
	public function upload($ParFileTmpName,$ParFileName) {
		global $intranet_root;
//		1. upload file physically
//		2. if success uploaded, update DB
//			- if update DB failed, rollback AND delete file
		# get file name
		$sourceFile = $ParFileTmpName;
		$this->fileName = $ParFileName;

		# create folder if necessary
		if ($this->folderPath) {
			// do nothing
		} else {
			$this->folderPath = str_replace("//","/",$this->FOLDER_PATH_PREFIX."u{$this->userId}/");
			if ( file_exists($intranet_root.$this->folderPath) ) {
				$resultCreatedFolder = true;
			} else {
				$resultCreatedFolder = $this->folder_new($intranet_root.$this->folderPath);
			}
			chmod($intranet_root.$this->folderPath, 0755);
		}
		if ($resultCreatedFolder) {
			# encode file name
			$this->fileHashName = sha1($this->userId.microtime());
			$destinationFile = $intranet_root.$this->folderPath.$this->fileHashName;

			if ($this->uploadFile($sourceFile,$destinationFile)) {
				$this->inputBy = $this->userId;
				$this->modifyBy = $this->userId;
				$this->size = round(filesize($destinationFile)/1024+1,PHP_ROUND_HALF_DOWN); // save in KB, +1 is used to cater 0kb after rounding
				$updateResult = $this->addToDB();
				if ($updateResult) {
					return $updateResult;
				} else {
					$this->removeFile();
				}
			}
		}
		
		return false;
	}
	/**
	 * Call to download this file
	 */
	public function download() {
		global $intranet_root;
		$fullPath = $intranet_root.$this->folderPath.$this->fileHashName;
		// Check browser agent
		$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
		if (preg_match('/opera/', $userAgent)) { $agentName = 'opera'; } 
		elseif (preg_match('/webkit/', $userAgent)) { $agentName = 'safari'; } 
		elseif (preg_match('/msie/', $userAgent)) { $agentName = 'msie'; } 
		elseif (preg_match('/mozilla/', $userAgent) && !preg_match('/compatible/', $userAgent)) { $agentName = 'mozilla'; } 
		else { $agentName = 'unrecognized'; }
		
		// Output file to browser
		$mime_type = mime_content_type($fullPath);
		$file_content = file_get_contents($fullPath);
//		$file_name = ($agentName == "mozilla") ? $this->fileName : iconv("UTF-8", "Big5", $this->fileName);
		switch ($agentName) {
			default:
			case "msie":
				$file_name = urlencode($this->fileName);
//$file_name = iconv("UTF-8", "Big5", $this->fileName);
				break;
			case "mozilla":
				$file_name = $this->fileName;
				break;
		}
		
		# header to output
		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-type: $mime_type");
		header("Content-Length: ".strlen($file_content));
		header("Content-Disposition: attachment; filename=\"".$file_name."\"");
		
		echo $file_content;
//output2browser($file_content,$this->fileName);
	}
	
	/**
	 * Remove a file in DB
	 * @return boolean - the result of the deletion
	 */
	protected function removeFromDB() {
		$sql = "DELETE FROM {$this->DB_TABLE} WHERE FILEID = {$this->fileId}";
		$q_result = $this->libdb->db_db_query($sql);
		return $q_result;		
	}
	
	/**
	 * Remove current file physically
	 * 
	 */
	protected function removeFile() {
		global $intranet_root;
		$libfilesystem = new libfilesystem();
		$removeTarget = $intranet_root.$this->folderPath.$this->fileHashName;
		$removeResult = false;
		if (file_exists($removeTarget)) {
			$removeResult = $libfilesystem->file_remove($removeTarget);
		} else {
			$removeResult = true;
		}
		return $removeResult;
	}
	
	/**
	 * Remove a file physically and remove data from DB
	 * @return boolean - true, if success, vice versa
	 */
	public function delete() {
//		1. remove record in DB && remove file physical
//		2. if failed in either one
//			rollback DB and return false
		$removeResult = $this->removeFromDB() && $this->removeFile();
		if ($removeResult) {
			// do nothing
		} else {
			$this->handleUpdateResult($removeResult);
		}
		return true;
	}
	
	/**
	 * Upload file physically
	 * @param String $ParSrcFile - The source address of the file to be uploaded
	 * @param String $ParDestFile - The destination address of the file to be uploaded
	 * @return boolean true, if upload success, vice versa
	 */
	protected function uploadFile($ParSrcFile, $ParDestFile) {
		$uploadResult = false;
		$uploadResult = move_uploaded_file($ParSrcFile, $ParDestFile);
		return $uploadResult;
	}
	
	/**
	 * Insert the current file record to the database
	 * @return boolean true, if successfullly insert this file record to the database
	 */
	protected function addToDB() {
		$sql = "
		INSERT INTO {$this->DB_TABLE} SET
			FILENAME = '{$this->fileName}',
			FOLDERPATH = '{$this->folderPath}',
			FILEHASHNAME = '{$this->fileHashName}',
			USERID = '{$this->userId}',
			DATEINPUT = NOW(),
			INPUTBY = '{$this->inputBy}',
			MODIFYBY = '{$this->modifyBy}',
			SIZE = '{$this->size}',
			CODE = '{$this->code}'
		";
		$returnResult = 0;
		$q_result = $this->libdb->db_db_query($sql);
		
		if ($q_result) {
			$returnResult = mysql_insert_id();
		}
		$this->handleUpdateResult($q_result);
		return $returnResult;
	}
	
	/**
	 * A function that will roll back the DB if false
	 * @param	boolean	$ParDBUpdateResult
	 */
	private function handleUpdateResult($ParDBUpdateResult) {
		if($ParDBUpdateResult) {
		  $this->libdb->Commit_Trans();
		} else {
		  $this->libdb->RollBack_Trans();
		}
	}
	/** 
	 * << get file id by file hash name >>
	 * Please note that the result maybe more than 1,
	 * FILEHASHNAME has a probabilty to be the same for 2 files despite very low,
	 * so, limited the result get to 1
	 * @return INT - fileid
	 */
	static public function GetFileIdByFileHashName($ParFileHashName) {
		global $intranet_db;
		$sql = <<<SQLEND
			SELECT FILEID FROM {$intranet_db}.IPORTAL_FILE WHERE FILEHASHNAME = '{$ParFileHashName}' LIMIT 1
SQLEND;
		$libdb = new libdb();
		$fileId = current($libdb->returnVector($sql));
		return $fileId;
	}
	######################################
	##	End: Functions
	######################################
	
	######################################
	##	Start: Accessors
	######################################
	public function getFileId() {
		return $this->fileId;
	}
	public function getFileName() {
		return $this->fileName;
	}
	public function getFolderPath() {
		return $this->folderPath;
	}
	public function getFileHashName() {
		return $this->fileHashName;
	}
	public function getUserId() {
		return $this->userId;
	}
	public function getDateInput() {
		return $this->dateInput;
	}
	public function getInputBy() {
		return $this->inputBy;
	}
	public function getDateModified() {
		return $this->dateModified;
	}
	public function getModifyBy() {
		return $this->modifyBy;
	}
	public function getSize() {
		return $this->size;
	}
	public function getCode() {
		return $this->code;
	}
	
	######################################
	##	End: Accessors
	######################################
	
	######################################
	##	Start: Mutators
	######################################
	public function setFileId($value) {
		$this->fileId=$value;
	}
	public function setFileName($value) {
		$this->fileName=$value;
	}
	/**
	 * The folder path is not supposed to change
	 */
//	public function setFolderPath($value) {
//		$this->folderPath=$value;
//	}
	public function setFileHashName($value) {
		$this->fileHashName=$value;
	}
	public function setUserId($value) {
		$this->userId=$value;
	}
	/**
	 * The date input is not supposed to change
	 */
//	public function setDateInput($value) {
//		$this->dateInput=$value;
//	}
	public function setInputBy($value) {
		$this->inputBy=$value;
	}
	public function setDateModified($value) {
		$this->dateModified=$value;
	}
	public function setModifyBy($value) {
		$this->modifyBy=$value;
	}
	public function setSize($value) {
		$this->size=$value;
	}
	public function setCode($value) {
		$this->code=$value;
	}
	######################################
	##	End: Mutators
	######################################
}

?>