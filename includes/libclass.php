<?php
// modifying by: 

############ Change Log [start] #################
#
#   Date    :   2020-04-21 (Tommy)
#               modified returnStudentClassHistory(), for ordering AcademicYear DESC
#
#   Date    :   2020-01-06 (Bill)   [2019-1220-1119-59235]
#               modified getStudentListByClassID(), support left student
#
#   Date    :   2018-08-27 (Ivan) 
#               added GetClassOfClassTeacher(), copy from EJ but convert to school settings
#
#   Date    :   2018-03-15 (Omas) #L136904 
#               modified returnStudentClassHistory() sync logic with iPo class histroy only show  PROFILE_CLASS_HISTORY
#
#	Date	:	2017-05-23 (Anna)
#				modified getStudentNameListByClassName() to add nickname condition 
#
#	Date	:	2017-04-10  (Cameron)
#				add $excludeUserIdAry to getStudentNameListByClassName()
#
#	Date	:	2017-03-08  (Cameron)
#				add $excludeUserIdAry to getStudentNameListWClassNumberByClassName()
#
#	Date	:	2017-02-23	(Omas)
#				modified getStudentByClassId() - add field YearClassID
#
#	Date	:	2016-05-12  (Cameron)
#				modify getSelectClassID, use if($var) to replace if(trim($var)!='') if $var is an array to support php 5.4
#
#	Date	:	2016-01-27	(Carlos)
#				modified returnStudentListByClass(), append [Left] to student name for left students. 
#
#	Date	: 	2015-07-21	(Shan)
#				add  getStudentIDByClassName()
#
#	Date	: 	2014-10-24	(Roy)
#				updated getSelectClass(), add parameter $yearClassIDArr to do filtering
#
#	Date	: 	2014-07-30	(Ivan)
#				updated getStudentNameListWClassNumberByClassName() added param $includeUserIdAry to return specific user only
#
#	Date	:	2014-02-26	(YatWoon)
#				updated returnHeadingClass(), add ordering statement if display more then 1 class
#
#	Date	:	2013-12-23	(Carlos)
#				modified getSelectClass(), add optional parameter $OptionNoClassStudent=false to display an option for Students that do not belong to any class
#
#	Date	:	2013-08-06	(YatWoon)
#				copy getStudentListByClassID() from libdisciplinev12.php
#
#	Date 	: 	2013-03-19	(Yuen)
#				add getSelectAlumniYears() 
#
#	Date 	: 	2012-10-24	(Rita)
#				add getStudentByClassId() 
#
#	Date	:	2012-09-19 (fai)
#				modified getClassStudentListOrderByClassNo, support with select which recordstatus of student

#	Date	:	2011-11-21 (Marcus)
#				modified getSelectClassWithWholeForm, added $ClassIDArr to display certain class only in the selection
#
#	Date	:	2011-08-09 (Henry Chow)
#				modified getSelectClassID(), add paramter $TeachingOnly
#
#	Date	:	2011-08-03 (Ivan)
#				modified getSelectClassWithWholeForm(), added para $WebSAMSCodeArr to show Forms with specific WebSAMS Code only
#
#	Date	:	2011-05-20 (Marcus)
#				add returnClassTeacherByClassID
#
#	Date	:	2011-02-01 (Marcus)
#				modified getSelectClassID, allow multiply selected, add logic to hide first option if $DisplaySelect=2
#
#	Date	:	2011-01-13 (Marcus)
#				modified getSelectClassID, add Param $optionFirst
#
#	Date	:	2011-01-13 (Marcus)
#				modified returnClassTeacher, add return values ChineseName,EnglishName
#
#	Date	:	2010-12-20	(YatWoon)
#				update getStudentNameListByClassName(), set RecordStatus as parameter
#
#	Date	:	2010-12-14 (Marcus)
#				modified returnClassTeacher, add return values UserID, UserLogin
#
#	Date	:	2010-12-07 (Marcus)
#				modified returnClassTeacher, add return values CTeacherEn, CTeacherCh
#
#	Date	:	2010-12-06 (Henry Chow)
#				added getSelectClassWithoutWholeForm(), get class list (without "Form" selection)
#
#	Date	:	2010-12-01 (Marcus)
#				modified getSelectClassWithWholeForm, performance tuning. get classes of form by batch instead of 1 by 1
#
#	Date	:	2010-11-22 (YAtWoon)
#				update getSelectLevel(), allow set the first select option name and pass AcademicYear parameter
#
#	Date	:	2010-11-03 (Ivan)
#				modified getSelectClassWithWholeForm(), added para $FormIDArr to display specific Form in the Selection only
#
#	Date	:	2010-09-08 (Henry Chow)
#				modified getClassStudentList(), change the parameter $class_name as optional 
#
#	Date	:	2010-09-06 (Henry Chow)
#				added storeStudent(), return student ID array by YearClassID or YearID 
#
#	Date	:	2010-08-12 (Henry Chow)
#				update returnStudentListByClass(), 
#				select the class name & class number from "YEAR_CLASS" & "YEAR_CLASS_USER" tables since getNameFieldWithClassNumberByLang() is not correct if "Class Promotion" not yet take effect  
#
#	Date	:	2010-05-28 YatWoon
#				update returnStudentListByClass2(), add class name data
#				copy getSelectClassWithWholeForm() from libdisciplinev12.php 
#
#	Date 	:	2010-04-24 (Marcus)
#				add function getClassNameByLang in order to select ClassName by Lang
#
#	Date 	:	2010-04-20 (Henry)
#				update getTeacherAndStudent() and getTeacher(), add $condition in functions
#
#	Date 	:	2010-04-15 (Henry)
#				add getTeacherTypeSelection(), add "Teacher/Non-teaching staff" selection menu 
#
#	Date 	:	2010-04-15 (Henry)
#				update getTeacherAndStudent() and getTeacher(), add condition of Teaching / Non-teaching staff
#
#	Date	:	2009-12-30 YatWoon
#				update returnClassTeacherID() 
#				change to IP25 logic (not use INTRANET_CLASS, use YEAR_CLASS instead)
#
############ Change Log [end] #################


include_once('form_class_manage.php');
include_once('form_class_manage_ui.php');

if (!defined("LIBCLASS_DEFINED"))         // Preprocessor directives
{

 define("LIBCLASS_DEFINED",true);

 class libclass extends libdb{

       function libclass()
       {
                $this->libdb();
       }
       function getClassName($classid)
       {
       	$YearClass = new Year_Class($classid,false,false,false);
       	
       	return $YearClass->ClassTitleEN;
                /*$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 AND ClassID = $classid";
                $result = $this->returnVector($sql);
                return $result[0];*/
       }
       function getClassNameByLang($classid)
       {
       	$YearClass = new Year_Class($classid,false,false,false);
       	
       	return Get_Lang_Selection($YearClass->ClassTitleB5,$YearClass->ClassTitleEN);
       }       
       function getLevelList()
       {
            //$sql = "SELECT ClassLevelID, LevelName FROM INTRANET_CLASSLEVEL WHERE RecordStatus = 1";
            $sql = "SELECT YearID, YearName FROM YEAR order by Sequence";
            $temp = $this->returnArray($sql,2);
            $result = array();
            for ($i=0; $i<sizeof($temp); $i++)
            {
                 list($id,$name)= $temp[$i];
                 $result[$id] = $name;
            }
            return $result;
       }
       function getLevelArray()
       {
	       $sql = 'Select YearID as ClassLevelID, YearName as LevelName From YEAR Order By Sequence ';
							
               // $sql = "SELECT ClassLevelID, LevelName FROM INTRANET_CLASSLEVEL WHERE RecordStatus = 1 ORDER BY LevelName";
           return $this->returnArray($sql,2);
       }

       function isClassExist($class)
       {
                //$sql = "SELECT COUNT(*) FROM INTRANET_CLASS WHERE RecordStatus = 1 AND ClassName = '$class'";
                $sql = "SELECT COUNT(*) FROM YEAR_CLASS WHERE ClassTitleEN = '$class'";
                $result = $this->returnVector($sql);
                return ($result[0]!=0);
       }

       function isLevelExist($level)
       {
                //$sql = "SELECT COUNT(*) FROM INTRANET_CLASSLEVEL WHERE RecordStatus = 1 AND LevelName = '$level'";
                $sql = "SELECT COUNT(*) FROM YEAR WHERE YearName = '$level'";
                $result = $this->returnVector($sql);
                return ($result[0]!=0);
       }

       function getLevelID ($level)
       {
       		$sql = 'select 
       							YearID 
       						from 
       							YEAR 
       						where 
       							YearName = \''.$this->Get_Safe_Sql_Query($level).'\'';
          //$sql = "SELECT ClassLevelID FROM INTRANET_CLASSLEVEL WHERE LevelName = '$level'";
          $result = $this->returnVector($sql);
          return $result[0];
       }
       
       function getLevelName ($ClassLevelID)
       {
	       $sql = "SELECT YearName FROM YEAR WHERE YearID = '$ClassLevelID'";
                //$sql = "SELECT LevelName FROM INTRANET_CLASSLEVEL WHERE ClassLevelID = '$ClassLevelID'";
                $result = $this->returnVector($sql);
                return $result[0];
       }
       
       function getClassID ($class, $AcademicYearID='', $YearIDArr='')
       {
       		if ($AcademicYearID=='')
	       		$AcademicYearID = Get_Current_Academic_Year_ID();
	       		
	       	if ($YearIDArr != '') {
       			$conds_YearID = " And YearID In ('".implode("','", IntegerSafe((array)$YearIDArr))."') ";
       		}
	       		
       		$sql = "select 
       							YearClassID 
       						From 
       							YEAR_CLASS 
       						where 
       							ClassTitleEN = '".$this->Get_Safe_Sql_Query($class)."' 
       							And AcademicYearID = '".$this->Get_Safe_Sql_Query($AcademicYearID)."'
								$conds_YearID
					";
	        //$sql = "SELECT ClassID FROM INTRANET_CLASS WHERE ClassName = '$class'";
	        $result = $this->returnVector($sql);
	        return $result[0];
       }

       function getClassList($AcademicYearID='')
       {
			if ($AcademicYearID == '')
				$AcademicYearID = Get_Current_Academic_Year_ID();
				
			$fcm = new form_class_manage();
			$ClassList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);
			
			for ($i=0; $i< sizeof($ClassList); $i++) {
				$Return[] = array(	$ClassList[$i]['YearClassID'],
									$ClassList[$i]['ClassTitleEN'],
									$ClassList[$i]['YearID'],
									"ClassID" => $ClassList[$i]['YearClassID'],
									"ClassName" => $ClassList[$i]['ClassTitleEN'],
									"ClassLevelID" => $ClassList[$i]['YearID']
								);
			}
			return $Return;
        /*$sql = "SELECT ClassID, ClassName, ClassLevelID FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
        return $this->returnArray($sql,3);*/
       }
       function getClassStudentList ($class_name="")
       {
       	if($class_name!="")
       		 $conds = " AND ClassName = '$class_name'";
       	
        $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN(0,1,2) $conds ORDER BY ClassName, ClassNumber";
        return $this->returnVector($sql);
       }
       
       function getStudentIDByClassName($classNameArr)
       {
         	
       	 $year = Get_Current_Academic_Year_ID();
       	 $sql = " select
     	 		  ycu.UserID	from
				  YEAR_CLASS as yc
				  INNER JOIN YEAR_CLASS_USER as ycu on yc.YearClassID = ycu.YearClassID
				  INNER JOIN INTRANET_USER as iu on ycu.UserID = iu.UserID and iu.RecordStatus IN(0,1,2) and iu.RecordType = 2
				  AND yc.ClassTitleEN IN ('".implode("','",$classNameArr)."') 
				  AND yc.AcademicYearID = ".$year."
				  order by yc.ClassTitleEN asc, ycu.ClassNumber	";
       
              return $this->returnVector($sql);
       }
       
       function getClassStudentListOrderByClassNo ($class_name, $RecordStatus = '0,1,2')
       {
                $sql = "SELECT UserID FROM INTRANET_USER 
                WHERE RecordType = 2 AND RecordStatus IN({$RecordStatus}) AND ClassName = '$class_name' 
                ORDER BY ClassNumber";
				//error_log($sql."    e:[".mysql_error()."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
                return $this->returnVector($sql);
       }
       function getClassNoByStudentID ($studentID)
       {
                $sql = "SELECT ClassNumber FROM INTRANET_USER
                WHERE UserID=$studentID";
                return $this->returnVector($sql);
       }
       function getClassByStudentID ($studentID)
       {
                $sql = "SELECT ClassName FROM INTRANET_USER
                WHERE UserID=$studentID";
                return $this->returnVector($sql);
       }
     
       function getStudentNameByStudentID ($studentID)
       {
                $sql = "SELECT EnglishName, ChineseName FROM INTRANET_USER
                WHERE UserID=$studentID";
                return $this->returnArray($sql,2);
       }
       function getGenderByStudentID ($studentID)
       {
                $sql = "SELECT Gender FROM INTRANET_USER
                WHERE UserID=$studentID";
                return $this->returnVector($sql);
       }
       function getStudentNameListByClassName ($class_name, $recordStatus="0,1,2",$excludeUserIdAry=array())
       {
    		   	global $sys_custom;
    		   	if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
    		      	$NickName = ",NickName";
    		   	}else{
    		   		$NickName = "";
    		   	}
    		   	$name_field = getNameFieldByLang();
               
                $sql = "SELECT UserID, $name_field, ClassNumber $NickName FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (". $recordStatus .") AND ClassName = '$class_name'";
		      
		        if (!empty($excludeUserIdAry)) {
		       		$conds_userId = " AND UserID NOT IN ('".implode("','", (array)$excludeUserIdAry)."')";
		        }                
                $sql .= $conds_userId." ORDER BY ClassNumber";
                return $this->returnArray($sql,3);
       }
       function getStudentNameListWClassNumberByClassName($class_name, $recordstatus='', $includeUserIdAry='', $excludeUserIdAry='')
       {
       		global $sys_custom;
	       $recordstatus = $recordstatus ? $recordstatus : "0,1,2";
// 	       if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
// 	       		$NickName = ",NickName";
// 	       }else{
// 	      	 	$NickName = "";
// 	       }
	       
	       $conds_userId = '';
	       if ($includeUserIdAry != '') {
	       		$conds_userId = " AND UserID IN ('".implode("','", (array)$includeUserIdAry)."') ";
	       }

	       if ($excludeUserIdAry != '') {
	       		$conds_userId = " AND UserID NOT IN ('".implode("','", (array)$excludeUserIdAry)."') ";
	       }
	       
            $name_field = getNameFieldWithClassNumberByLang();
            $sql = "SELECT UserID, $name_field  FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN ($recordstatus) AND ClassName = '$class_name' $conds_userId ORDER BY ClassNumber";
            return $this->returnArray($sql,3);
       }
       function getClassStudentNameList($classid)
       {
                $name_field = getNameFieldByLang();
                //$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE RecordStatus IN (0,1,2) AND ClassID=$classid";
                $sql = "SELECT ClassTitleEN FROM YEAR_CLASS WHERE YearClassID=$classid";
                $class = $this->returnVector($sql);
                $sql = "SELECT UserID, $name_field, ClassNumber FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName = '".$class[0]."' ORDER BY ClassNumber";
                return $this->returnArray($sql,3);
       }
       function returnClassListByLevel($lvlID)
       {
       		$fcm = new form_class_manage();
       		return $fcm->Get_Class_List_By_YearID($lvlID);
          /*$sql = "SELECT ClassID, ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 AND ClassLevelID = $lvlID";
          return $this->returnArray($sql,2);*/
       }
    
    /*
     * $DisplaySelect = 2 means do not show the "-- select --" option
     */
	function getSelectClassID($attrib, $selected="", $DisplaySelect=0, $AcademicYearID='',$optionFirst='', $DisplayClassIDArr='', $TeachingOnly=0)
	{
	  global $button_select, $i_general_all_classes;
	  
	  if ($AcademicYearID == '')
			$AcademicYearID = Get_Current_Academic_Year_ID();
			
	  $fcm = new form_class_manage();
		$AllClassList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, "", $TeachingOnly);
	
//		if(trim($DisplayClassIDArr)!='')
		if($DisplayClassIDArr)
		{
			foreach((array)$AllClassList as $thisClassInfo)
			{
				if(in_array($thisClassInfo['YearClassID'],$DisplayClassIDArr))
					$ClassList[] = $thisClassInfo;
			}	
		}
		else
		{
			$ClassList = $AllClassList;
		}	
		
	  $x = "<SELECT $attrib>\n";
	  $empty_selected = ($selected == '')? "SELECTED":"";
	  if (($selected == "" || $DisplaySelect==1) && $DisplaySelect!=2)
	  {
	      $x .= "<OPTION value='' $empty_selected>". (($optionFirst=="")? "-- ".$button_select." --":$optionFirst) ."</OPTION>\n";
      }
      
	  for ($i=0; $i< sizeof($ClassList); $i++) {
	  	if ($ClassList[$i]['YearID'] != $ClassList[$i-1]['YearID']) {
	  		if ($i == 0) 
	  			$x .= '<optgroup label="'.$ClassList[$i]['YearName'].'">';
	  		else {
	  			$x .= '</optgroup>';
	  			$x .= '<optgroup label="'.$ClassList[$i]['YearName'].'">';
	  		}
	  	}
	  	
	  	unset($Selected);
	  	$Selected = (in_array($ClassList[$i]['YearClassID'],(array)$selected))? 'selected':'';
	  	$x .= '<option value="'.$ClassList[$i]['YearClassID'].'" '.$Selected.'>&nbsp;&nbsp;&nbsp;&nbsp;'.Get_Lang_Selection($ClassList[$i]['ClassTitleB5'],$ClassList[$i]['ClassTitleEN']).'</option>';
	  	
	  	if ($i == (sizeof($ClassList)-1)) 
	  		$x .= '</optgroup>';
	  }
	  $x .= '</select>';
	  
	  return $x;
	}
	
	function getSelectClass($attrib,$selected="",$optionSelect="", $optionFirst="", $AcademicYearID='', $OptionNoClassStudent=false, $YearClassIDArr='')
	{
		global $button_select, $Lang;
		
		if ($AcademicYearID == '')
			$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$fcm = new form_class_manage();
		$ClassList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, $YearID='', $TeachingOnly=0, $YearClassIDArr);
		
		$x .= '<select '.$attrib.'>';
		if($optionSelect != 1)
		{
			$x .= "<OPTION value='' ".((sizeof($ClassList) > 0)? "":"selected").">".(($optionFirst=="")? "-- ".$button_select." --":$optionFirst)."</OPTION>\n";
		}
	  for ($i=0; $i< sizeof($ClassList); $i++) {
	  	if ($ClassList[$i]['YearID'] != $ClassList[$i-1]['YearID']) {
	  		if ($i == 0) 
	  			$x .= '<optgroup label="'.htmlspecialchars($ClassList[$i]['YearName'],ENT_QUOTES).'">';
	  		else {
	  			$x .= '</optgroup>';
	  			$x .= '<optgroup label="'.htmlspecialchars($ClassList[$i]['YearName'],ENT_QUOTES).'">';
	  		}
	  	}
	  	
	  	unset($Selected);
	  	$Selected = ($ClassList[$i]['ClassTitleEN'] == $selected)? 'selected':'';
	  	$x .= '<option value="'.$ClassList[$i]['ClassTitleEN'].'" '.$Selected.'>'.Get_Lang_Selection($ClassList[$i]['ClassTitleB5'],$ClassList[$i]['ClassTitleEN']).'</option>';
	  	
	  	if ($i == (sizeof($ClassList)-1)) 
	  		$x .= '</optgroup>';
	  }
	  if($OptionNoClassStudent) {
	  	$x .= '<option value="-2" '.($selected=='-2'?'selected':'').'>'.$Lang['AccountMgmt']['StudentNotInClass'].'</option>';
	  }
	  
	  $x .= '
	                  </select>';
	
		/*
		$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
		$result = $this->returnVector($sql);
		$x = "<SELECT $attrib>\n";
		$empty_selected = ($selected == '')? "SELECTED":"";
		
		if($optionSelect != 1)
		{
			$x .= "<OPTION value='' $empty_selected>".(($optionFirst=="")? "-- ".$button_select." --":$optionFirst)."</OPTION>\n";
		}
		
	  for ($i=0; $i < sizeof($result); $i++)
	  {
	       $name = $result[$i];
	       $selected_str = ($name==$selected? "SELECTED":"");
	       $x .= "<OPTION value='$name' $selected_str>$name</OPTION>\n";
	  }
	  $x .= "</SELECT>\n";*/
	  return $x;
	}
           function getClassListByLevel($lvlID,$attrib,$selected="")
           {
                global $button_select;
                //$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassLevelID = $lvlID AND RecordStatus = 1 ORDER BY ClassName";
                $sql = "SELECT 
                			ClassTitleEN 
                		FROM 
                			YEAR_CLASS 
                		WHERE 
                			YearID = $lvlID 
                		ORDER BY 
                			ClassTitleEN";
                
                $result = $this->returnVector($sql);
                $x = "<SELECT $attrib>\n";
                $empty_selected = ($selected == '')? "SELECTED":"";

                $x .= "<OPTION value='' $empty_selected>{$button_select}</OPTION>\n";
                for ($i=0; $i < sizeof($result); $i++)
                {
                     $name = $result[$i];
                     $selected_str = ($name==$selected? "SELECTED":"");
                     $x .= "<OPTION value='$name' $selected_str>$name</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
           }
           
           /*
       function getSelectLevel($attrib,$selected="",$ValueIsID=false, $firstOptionName='')
       {
       		global $button_select;
//          echo $selected;
          $fcm = new form_class_manage();
          
          $result = $fcm->Get_Form_List(false);
          
                $x = "<SELECT $attrib>\n";
                $empty_selected = ($selected == '')? "SELECTED":"";
                $firstName = $firstOptionName ? $firstOptionName : $button_select;
                $x .= "<OPTION value='' $empty_selected>{$firstName}</OPTION>\n";
                for ($i=0; $i < sizeof($result); $i++)
                {
// 	                debug_pr($result[$i]['MappedClass']);
// 	                if(!$result[$i]['MappedClass'])	continue;
	                
                     $name = $result[$i]['YearName'];
                     if ($ValueIsID) {
                     	$Val = $result[$i]['YearID'];
                     	$selected_str = ($Val==$selected? "SELECTED":"");
                     }
                     else {
                     	$Val = $name;
                     	$selected_str = ($Val==$selected? "SELECTED":"");
                     }
                     	
                     $x .= "<OPTION value='$Val' $selected_str>$name</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }
       */
       
       function getSelectLevel($attrib,$selected="",$ValueIsID=false, $firstOptionName='', $AcademicYearID='')
       {
       		global $button_select;
          $fcm = new form_class_manage();
          
          $result = $fcm->Get_Form_List(false,"",$AcademicYearID);
                $x = "<SELECT $attrib>\n";
                $empty_selected = ($selected == '')? "SELECTED":"";
                $firstName = $firstOptionName ? $firstOptionName : $button_select;
                $x .= "<OPTION value='' $empty_selected>{$firstName}</OPTION>\n";
                for ($i=0; $i < sizeof($result); $i++)
                {
                     $name = $result[$i]['YearName'];
                     if ($ValueIsID) {
                     	$Val = $result[$i]['YearID'];
                     	$selected_str = ($Val==$selected? "SELECTED":"");
                     }
                     else {
                     	$Val = $name;
                     	$selected_str = ($Val==$selected? "SELECTED":"");
                     }
                     	
                     $x .= "<OPTION value='$Val' $selected_str>$name</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }
       
       # student name output format: CHAN KA YU (1A-02) 
       function returnStudentListByClass($class_name, $filter="", $recordStatus="0,1,2")
       {
       		global $Lang; 
       			$name_field = getNameFieldByLang("a.");
       			$sql = "SELECT 
							a.UserID, 
							CONCAT(
								$name_field, 
								IF(b.ClassNumber IS NOT NULL,
									CONCAT(
										' (', 
										".Get_Lang_Selection('c.ClassTitleB5', 'c.ClassTitleEN').", 
										'-', 
										b.ClassNumber,
										')'
									), 
								''),IF(a.RecordStatus='3','[".$Lang['Status']['Left']."]','')) as ClassNumber 
 						FROM 
 							INTRANET_USER as a LEFT OUTER JOIN 
 							YEAR_CLASS_USER as b on (a.UserID=b.UserID) 
 							LEFT OUTER JOIN YEAR_CLASS c on (c.YearClassID=b.YearClassID)
                        WHERE 
                        	a.RecordType = 2 AND 
                        	a.RecordStatus IN (". $recordStatus .") AND 
                        	(c.ClassTitleEN='$class_name' OR c.ClassTitleB5='$class_name') AND 
                        	c.AcademicYearID=".Get_Current_Academic_Year_ID()." 
                        	$filter 
						ORDER BY b.ClassNumber";
						
                /*
                $name_field = getNameFieldWithClassNumberByLang("a.");

 				$sql = "SELECT a.UserID, $name_field  
 						FROM 
 							INTRANET_USER as a LEFT OUTER JOIN 
 							YEAR_CLASS_USER as b on (a.UserID=b.UserID) 
 							LEFT OUTER JOIN YEAR_CLASS c on (c.YearClassID=b.YearClassID)
                        WHERE 
                        	a.RecordType = 2 AND 
                        	a.RecordStatus IN (". $recordStatus .") AND 
                        	(c.ClassTitleEN='$class_name' OR c.ClassTitleB5='$class_name') AND 
                        	c.AcademicYearID=".Get_Current_Academic_Year_ID()." 
                        	$filter 
						ORDER BY b.ClassNumber";
				*/                
				/*$sql = "SELECT a.UserID, $name_field FROM INTRANET_USER as a
                        WHERE a.RecordType = 2 AND a.RecordStatus IN (". $recordStatus .") AND a.ClassName = '$class_name' $filter ORDER BY a.ClassNumber";
				*/
                //$sql = "SELECT a.UserID, $name_field FROM INTRANET_USER as a
                //        WHERE a.RecordType = 2 AND a.RecordStatus != 3 AND a.ClassName = '$class_name' ORDER BY a.ClassNumber";

                return $this->returnArray($sql,2);
       }
       
       # seperate student name, class number, className
       function returnStudentListByClass2($class_name, $filter="")
       {
				$name_field = getNameFieldByLang();
				$sql = "SELECT a.UserID, $name_field,a.ClassNumber, a.ClassName FROM INTRANET_USER as a
                        WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND a.ClassName = '$class_name' $filter ORDER BY a.ClassName, a.ClassNumber";
                		
                return $this->returnArray($sql,4);
       }
       
       function getStudentSelectByClass($class,$tags,$selected="", $all=0, $type="", $typeID=-1, $recordStatus="0,1,2", $includeUserIdAry='')
       {
                global $button_select, $button_select_all;
                global $i_ClubsEnrollment_StatusWaiting,$i_ClubsEnrollment_StatusRejected,$i_ClubsEnrollment_StatusApproved;
                
                $list = array();
                                
                // edited by Ivan (24 July 08) adding $GroupID to hide existing member in the list
                if ($type==""){	//no specific type => list all students in the class
                	$list = $this->returnStudentListByClass($class,"",$recordStatus);
	            }
	            else{
		            if ($typeID==-1){	//no ID number => show all students in the class
			            $list = $this->returnStudentListByClass($class,"",$recordStatus);
		            }
		            else if ($type==0 || $type==2){	//filter group member and student in enrol list
	            		$EnrolGroupID = $typeID;
	            	
	                	//get all UserID of members
						$sql = "SELECT b.UserID 
								FROM INTRANET_USERGROUP as a INNER JOIN INTRANET_USER as b ON a.UserID = b.UserID 
								WHERE (a.EnrolGroupID = $EnrolGroupID AND b.ClassName = '$class' AND b.RecordType = 2 AND b.RecordStatus IN (". $recordStatus ."))
								";
						$memberIDArr1 = $this->returnArray($sql,1);
						
						//get all UserID of students in enrol list
						$sql = "SELECT b.UserID 
								FROM INTRANET_ENROL_GROUPSTUDENT as a INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID 
								WHERE (a.EnrolGroupID = $EnrolGroupID AND b.ClassName = '$class' AND b.RecordType = 2 AND b.RecordStatus IN (". $recordStatus .") AND a.RecordStatus IN (0,2))
								";
						$memberIDArr2 = $this->returnArray($sql,1);
						
						$memberIDArr = array_merge($memberIDArr1, $memberIDArr2);
						
						if (sizeof($memberIDArr)==0){	//no one is member => show all
							$list = $this->returnStudentListByClass($class,"",$recordStatus);	
						}
						else{	//hide the member in the list
							
							//contruct a string of NOT IN list for sql
							$filterList = "";
							for ($i=0; $i<sizeof($memberIDArr); $i++){
								$filterList .= $memberIDArr[$i][0];
								if ($i != sizeof($memberIDArr)-1){
									$filterList .= ", ";
								}
							}
							
							$filterList = "AND a.UserID NOT IN (".$filterList.")";
							$list = $this->returnStudentListByClass($class, $filterList,$recordStatus);
						}
					}
					else if ($type==1 || $type==3){	//filter activity joiner
				        $EnrolEventID = $typeID;
	            	
	                	//get all UserID of members
						$sql = "SELECT b.UserID 
								FROM INTRANET_ENROL_EVENTSTUDENT as a INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID 
								WHERE (a.EnrolEventID = $EnrolEventID AND b.ClassName = '$class' AND b.RecordType = 2 AND b.RecordStatus IN (". $recordStatus .") AND a.RecordStatus IN (0,2))
								";
						$memberIDArr = $this->returnArray($sql,1);
						
						if (sizeof($memberIDArr)==0){	//no one joined yet => show all
							$list = $this->returnStudentListByClass($class,"",$recordStatus);	
						}
						else{	//hide the member in the list
							
							//contruct a string of NOT IN list for sql
							$filterList = "";
							for ($i=0; $i<sizeof($memberIDArr); $i++){
								$filterList .= $memberIDArr[$i][0];
								if ($i != sizeof($memberIDArr)-1){
									$filterList .= ", ";
								}
							}
							$filterList = "AND a.UserID NOT IN (".$filterList.")";
							$list = $this->returnStudentListByClass($class, $filterList,$recordStatus);
						}
			        }
			        else{	//unknown type => show all students in the class
			            $list = $this->returnStudentListByClass($class,"",$recordStatus);
		            }
	            }
                
                $x = "<SELECT $tags>\n";
                $empty_selected = ($selected == '')? "SELECTED":"";
                //$x .= "<OPTION value='' $empty_selected>{$button_select}</OPTION>\n";
                
                if($all)
                $x .= "<OPTION value='-1'>{$button_select_all}</OPTION>\n";
                
                for ($i=0; $i<sizeof($list); $i++)
                {
                     list($id,$name) = $list[$i];
                     $status = "";
                     
                     if ($includeUserIdAry != '' && !in_array($id, (array)$includeUserIdAry)) {
                     	continue;
                     }
                     
                     # show student record status
                     if ($typeID==-1)
                     {
	                  	$status = "";   
                     }
                     else if ($type==0 || $type==2)
                     {
	                     $sql = "SELECT 
	                     				CASE RecordStatus
							                   WHEN 0 THEN '$i_ClubsEnrollment_StatusWaiting'
							                   WHEN 1 THEN '$i_ClubsEnrollment_StatusRejected'
							                   WHEN 2 THEN '$i_ClubsEnrollment_StatusApproved'
							            END
							     FROM INTRANET_ENROL_GROUPSTUDENT
							     WHERE EnrolGroupID = $typeID AND StudentID = $id
							     ";
						 $result = $this->returnVector($sql);
						 if ($result[0]!="")
						 {
						 	$status = "[".$result[0]."]";
					 	 }
                     }
                     else if ($type==1 || $type==3)
                     {
	                    $sql = "SELECT 
	                     				CASE RecordStatus
							                   WHEN 0 THEN '$i_ClubsEnrollment_StatusWaiting'
							                   WHEN 1 THEN '$i_ClubsEnrollment_StatusRejected'
							                   WHEN 2 THEN '$i_ClubsEnrollment_StatusApproved'
							            END
							     FROM INTRANET_ENROL_EVENTSTUDENT
							     WHERE EnrolEventID = $typeID AND StudentID = $id
							     ";
						 $result = $this->returnVector($sql);
						 if ($result[0]!="")
						 {
						 	$status = "[".$result[0]."]";
					 	 }
                     }
                     
                     $sel_str = ($selected == $id? "SELECTED":"");
                     $x .= "<OPTION value=$id $sel_str>$name $status</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }

       ##### Modified by Henry on 14 Nov 2008 ###
       
              function getTeacher($tag, $teacherType="", $conds="")
       			{	
                global $button_select, $button_select_all;
                global $i_ClubsEnrollment_StatusWaiting,$i_ClubsEnrollment_StatusRejected,$i_ClubsEnrollment_StatusApproved;

       			$ldiscipline = new libdisciplinev12();
                
                $AdminUser = $ldiscipline->GET_ADMIN_USER();
                $conds .= ($AdminUser != "") ? " AND UserID NOT IN ($AdminUser)" : "";
				//$AssignedUser = $ldiscipline->getAssignedGroupUser();
				if($teacherType==1)
					$conds .= " AND Teaching=1";
				else if($teacherType==0)
					$conds .= " AND (Teaching IS NULL OR Teaching=0 OR Teaching='')";

                $list = array();
                $name_field = getNameFieldByLang();
                
                $sql = "SELECT UserID, $name_field FROM INTRANET_USER WHERE RecordType=1 $conds ORDER BY $name_field";
                $list = $this->returnArray($sql,2);
                
                $x = "<SELECT ".$tag.">";
                
                for ($i=0; $i<sizeof($list); $i++)
                {
                     list($id,$name) = $list[$i];
                     $status = "";
					
                     $sel_str = ($selected == $id? "SELECTED":"");
                     $x .= "<OPTION value=$id $sel_str>$name</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }
       
       function getTeacherForRepairSystem($tag, $teacherType="", $conds="")
       {	
                global $button_select, $button_select_all;
                global $i_ClubsEnrollment_StatusWaiting,$i_ClubsEnrollment_StatusRejected,$i_ClubsEnrollment_StatusApproved;

				if($teacherType==1)
					$conds .= " AND Teaching=1";
				else if($teacherType==0)
					$conds .= " AND (Teaching IS NULL OR Teaching=0 OR Teaching='')";

                $list = array();
                $name_field = getNameFieldByLang();
                
                $sql = "SELECT UserID, $name_field FROM INTRANET_USER WHERE RecordType=1 $conds ORDER BY $name_field";
                $list = $this->returnArray($sql,2);
                hdebug_r($sql);
                $x = "<SELECT ".$tag.">";
                
                for ($i=0; $i<sizeof($list); $i++)
                {
                     list($id,$name) = $list[$i];
                     $status = "";
					
                     $sel_str = ($selected == $id? "SELECTED":"");
                     $x .= "<OPTION value=$id $sel_str>$name</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }

       function getTeacherAndStudent($tag, $teacherType="", $conds="")
       			{	
                global $button_select, $button_select_all;
                global $i_ClubsEnrollment_StatusWaiting,$i_ClubsEnrollment_StatusRejected,$i_ClubsEnrollment_StatusApproved;

       			$ldiscipline = new libdisciplinev12();
                
                $AdminUser = $ldiscipline->GET_ADMIN_USER();
                $conds .= ($AdminUser != "") ? " AND UserID NOT IN ($AdminUser)" : "";
				//$AssignedUser = $ldiscipline->getAssignedGroupUser();

				if($teacherType==1)
					$conds .= " AND Teaching=1";
				else if($teacherType==0)
					$conds .= " AND (Teaching IS NULL OR Teaching=0 OR Teaching='')";
				
                $list = array();
                $name_field = getNameFieldByLang();
                
                $sql = "SELECT UserID, $name_field FROM INTRANET_USER WHERE RecordType IN (1,2) $conds GROUP BY UserID ORDER BY $name_field";
                $list = $this->returnArray($sql,2);
                                
                $x = "<SELECT ".$tag.">";
                
                for ($i=0; $i<sizeof($list); $i++)
                {
                     list($id,$name) = $list[$i];
                     $status = "";
					
                     $sel_str = ($selected == $id? "SELECTED":"");
                     $x .= "<OPTION value=$id $sel_str>$name</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }
      
       ##########################################
       
       /*
       For Homework list use
       */
       function fixClassGroupRelation()
       {
                $sql = "SELECT GroupID, Title FROM INTRANET_GROUP where RecordType = 3";
                $result = $this->returnArray($sql,2);

                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($GroupID,$name) = $result[$i];
                     $groups[$name] = $GroupID;
                }
                $sql = "UPDATE INTRANET_CLASS SET GroupID = NULL";
                $this->db_db_query($sql);

                $sql = "SELECT ClassID, ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1";
                $result = $this->returnArray($sql,2);
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list($cid,$name) = $result[$i];
                     $gid = $groups[$name];
                     if ($gid != "" && $gid != 0)
                     {
                         $sql = "UPDATE INTRANET_CLASS SET GroupID = $gid WHERE ClassID = $cid";
                         $this->db_db_query($sql);
                     }
                }
       }
                # For removed student
                function isRemovedArchiveStudentExist($studentName){
                                $sql = "SELECT COUNT(*)
                                                                FROM INTRANET_ARCHIVE_USER
                                                                        WHERE
                                                                                (EnglishName like '%$studentName%' OR ChineseName like '%$studentName%')";
                $result = $this->returnVector($sql);
                return ($result[0]!=0);
                }
                function returnRemovedStudentID($studentName){

                                $sql = "SELECT DISTINCT UserID, EnglishName, ChineseName
                                                                FROM INTRANET_ARCHIVE_USER
                                                                        WHERE
                                                                                (EnglishName like '%$studentName%' OR ChineseName like '%$studentName%') ORDER BY EnglishName";
                $result = $this->returnArray($sql,3);
                return ($result);
                }

                function returnRemovedStudentArchiveClassHistory($UserID){
                                $sql = "SELECT EnglishName, ChineseName, AcademicYear, ClassName,ClassNumber, UserID
                                                                FROM PROFILE_ARCHIVE_CLASS_HISTORY
                                                                        WHERE UserID=$UserID
                                                                                ORDER BY AcademicYear";
                $result = $this->returnArray($sql,6);
                return ($result);
                }

       function returnStudentClassHistory($studentid, $order="ASC")
       {
					global $eclass_db;
       
					$sql = "SELECT DISTINCT AcademicYear, ClassName, ClassNumber, RecordId FROM PROFILE_CLASS_HISTORY
					               WHERE UserID = $studentid ORDER BY AcademicYear ". $order;
					$row1 = $this->returnArray($sql, 3);

                    // 20180315 Omas sync logic with iPo class histroy only show  PROFILE_CLASS_HISTORY
// 					# Eric Yip : another table to get class history
// 					$ClassNumberField = getClassNumberField();
// 					$sql = "SELECT DISTINCT Year, ClassName, $ClassNumberField as ClassNumber FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
// 												WHERE UserID = $studentid ORDER BY Year";				
// 					$row2 = $this->returnArray($sql, 3);
					
// 					return (sizeof($row1)>sizeof($row2))? $row1 : $row2;
                    return $row1;
       }
       function returnLeftStudentClassHistory($studentid, $recordyear="")
       {
                $sql = "SELECT DISTINCT AcademicYear, ClassName, ClassNumber FROM PROFILE_ARCHIVE_CLASS_HISTORY
                               WHERE UserID = '$studentid'";
								$sql .= ($recordyear!="") ? " AND AcademicYear = '$recordyear'" : "";
								$sql .= " ORDER BY DateInput";
                return $this->returnArray($sql,3);
       }
       function displayClassHistoryAdmin($studentid)
       {
                $data = $this->returnStudentClassHistory($studentid);
                
                # Eric Yip : display class history together with current class
                # Modified by Key 25-09-2008: Check the YearOfLeft when getting the current class
								$IsCurrentIncluded = false;
								$currentYr = getCurrentAcademicYear();
								foreach($data as $value)
								{
									if($value['AcademicYear'] == $currentYr)
										$IsCurrentIncluded = true;
								}
								if(!$IsCurrentIncluded)
								{
									$TmpClass = $this->returnCurrentClass($studentid);
									//check the class & classnumber are existing
									if($TmpClass[0][1] != "" && $TmpClass[0][2] != "")
									$data = array_merge($data, $TmpClass);
								}
                
                return $this->displayClassHistoryTable($data);
       }
       function displayLeftClassHistoryAdmin($studentid, $recordyear="")
       {
                $data = $this->returnLeftStudentClassHistory($studentid, $recordyear);
                return $this->displayClassHistoryTable($data);
       }
       function displayClassHistoryTable($data)
       {
                global $i_no_record_exists_msg;

                $x = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
                if (sizeof($data)==0)
                {
                    return "$x<tr><td>$i_no_record_exists_msg</td></tr></table>\n";
                }
                $title_row = "<tr>";
                $data_row = "<tr>";
                for ($i=0; $i<sizeof($data); $i++)
                {
                     list($year, $class, $classnum) = $data[$i];
                     $title_row .= "<td class=tableTitle_new align=center>$year</td>";
                     $data_row .= "<td align=center>$class-$classnum</td>";
                }
                $x .= "$title_row\n$data_row\n";
                $x .= "</table>\n";
                return $x;
       }
       
       function returnClassTeacher($ClassName, $AcademicYearID='')
       {
			if ($AcademicYearID == '')
				$AcademicYearID = Get_Current_Academic_Year_ID();
	       $name_field = getNameFieldByLang('u.');
	       $name_field_en = getNameFieldByLang('u.','en');
	       $name_field_b5 = getNameFieldByLang('u.','b5');
         
         $ClassID = $this->getClassID($ClassName, $AcademicYearID);
         
         $sql = 'select 
         					'.$name_field.' as CTeacher,
							u.TitleEnglish,
							u.TitleChinese,
							u.NickName,
							'.$name_field_en.' as CTeacherEn,
							'.$name_field_b5.' as CTeacherCh,
							u.UserID,
							u.UserLogin,
							u.ChineseName,
							u.EnglishName
         				From 
         					YEAR_CLASS_TEACHER as yct 
         					inner join 
         					INTRANET_USER as u 
         					on yct.UserID = u.UserID 
         				where 
         					YearClassID = \''.$ClassID.'\'';
            /*$sql = "SELECT $name_field as CTeacher
                    FROM 
                    INTRANET_CLASSTEACHER as b 
                    LEFT OUTER JOIN INTRANET_CLASS as a ON a.ClassID = b.ClassID
                    LEFT JOIN INTRANET_USER as c on c.UserID=b.UserID
                    WHERE a.ClassName = '$ClassName'";*/
            return $this->returnArray($sql);
       }
       
       /*
       function returnClassTeacherID($ClassName)
       {
            $sql = "SELECT b.UserID 
                    FROM 
                    INTRANET_CLASSTEACHER as b 
                    LEFT OUTER JOIN INTRANET_CLASS as a ON a.ClassID = b.ClassID
                    LEFT JOIN INTRANET_USER as c on c.UserID=b.UserID
                    WHERE a.ClassName = '$ClassName'";
            return $this->returnArray($sql);
       }
       */
       
       function returnClassTeacherID($ClassName, $AcademicYearID='')
       {
			if ($AcademicYearID == '')
				$AcademicYearID = Get_Current_Academic_Year_ID();
         	$ClassID = $this->getClassID($ClassName, $AcademicYearID);
         
			$sql = "select 
						yct.UserID 
					From 
						YEAR_CLASS_TEACHER as yct
						Inner Join
						INTRANET_USER as iu
						On (yct.UserID = iu.UserID)
					where 
						yct.YearClassID = $ClassID";
						
			 return $this->returnArray($sql);
       }
       
       function returnClassTeacherByClassID($ClassID)
       {
	       $name_field = getNameFieldByLang('u.');
	       $name_field_en = getNameFieldByLang('u.','en');
	       $name_field_b5 = getNameFieldByLang('u.','b5');
         
         $sql = "select 
         					".$name_field." as CTeacher,
							u.TitleEnglish,
							u.TitleChinese,
							u.NickName,
							".$name_field_en." as CTeacherEn,
							".$name_field_b5." as CTeacherCh,
							u.UserID,
							u.UserLogin,
							u.ChineseName,
							u.EnglishName,
							YearClassID							
         				From 
         					YEAR_CLASS_TEACHER as yct 
         					inner join 
         					INTRANET_USER as u 
         					on yct.UserID = u.UserID 
         				where 
         					YearClassID IN  (".implode(",",(array)$ClassID).") ";
         					
            return $this->returnArray($sql);
       }
       
       function GetClassOfClassTeacher($ParUserID)
       {
//            $sql = "
//            SELECT
//            a.ClassID, a.ClassName, a.GroupID
//            FROM
//            INTRANET_CLASSTEACHER as b
//            LEFT JOIN INTRANET_CLASS as a ON a.ClassID = b.ClassID
//            WHERE
//            b.UserID = '$ParUserID'";
//            return $this->returnArray($sql);
           
           $sql = " SELECT
                        yc.YearClassID as ClassID, yc.ClassTitleEN as ClassName, yc.GroupID
                    FROM
                        YEAR_CLASS as yc
                        INNER JOIN YEAR_CLASS_TEACHER as yct ON (yc.YearClassID = yct.YearClassID)
                    WHERE
                        yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
                        AND yct.UserID = '".$ParUserID."'
                  ";
           return $this->returnResultSet($sql);
       }
			
			// return teacher being �Z�D�� classes
			function returnHeadingClass($uid, $displayAll=0)
      {
      	
      	$sql = 'select 
      						yc.ClassTitleEN 
      					From 
      						YEAR_CLASS as yc 
      						inner join 
      						YEAR_CLASS_TEACHER yct 
      						on 
      						yc.YearClassID = yct.YearClassID and 
      						yc.AcademicYearID = \''.Get_Current_Academic_Year_ID().'\' and 
      						yct.UserID = \''.$uid.'\'';
      			if($displayAll)
      			{
      				$sql .= ' left join YEAR as y on y.YearID=yc.YearID' .
      						' order by y.Sequence, yc.Sequence';
      			}
      			
				/*$sql = "SELECT a.ClassName FROM INTRANET_CLASS as a, INTRANET_CLASSTEACHER as b
				       WHERE a.ClassID = b.ClassID AND b.UserID = $uid";*/
				if($displayAll)
				{
					$result = $this->returnArray($sql);
					return $result;
				}
				else
				{
					$result = $this->returnVector($sql);
					return $result[0];
				}
      }
			
			# Eric Yip : function to add current class to class history
			# Modified by Key 25-09-2008: Check the YearOfLeft when getting the current class
			function returnCurrentClass($studentid)
			{
				$sql =	"
									SELECT
										ClassName, ClassNumber
									FROM
										INTRANET_USER
									WHERE
										UserID = $studentid
								";
				$sql .= " AND  (YearOfLeft = '' OR YearOfLeft IS NULL)";
				$Year = array();
				$Year[] = getCurrentAcademicYear();
				$returnArr = $this->returnArray($sql,2);
				$returnArr[0] = count($returnArr)>0 ? array_merge($Year, $returnArr[0]) : array($Year,'','');
				
				return $returnArr;
			}
			
			function getClassLevelByClassName($ParClassName)
			{
			    $sql = "SELECT icl.ClassLevelID, icl.LevelName FROM INTRANET_CLASS as ic LEFT JOIN INTRANET_CLASSLEVEL as icl ON icl.ClassLevelID = ic.ClassLevelID WHERE ic.RecordStatus = 1 AND icl.RecordStatus = 1 AND ic.ClassName='".$this->Get_Safe_Sql_Query($ParClassName)."'";
				$result = $this->returnArray($sql,2);
				return $result[0];
			}
			
			/*
			# 20090727 yat - select student list from classid array
				function getClassStudentNameListByClassID($classid)
				{
					if(!empty($classid))
					{
						if(is_array($classid))
						{
								$classid_str = implode(",",$classid);
						}
						else
						{
							
						}
						
						$name_field = getNameFieldByLang();
						
						$sql = "
								select 
	       							ClassTitleEN 
	       						From 
	       							YEAR_CLASS 
	       						where 
	       							YearClassID in ($classid_str)
	       							and 
	       							AcademicYearID = '".Get_Current_Academic_Year_ID()."'
	       						";
						$class = $this->returnVector($sql);
						
						$sql = "SELECT UserID, $name_field, ClassNumber FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName = '".$class[0]."' ORDER BY ClassNumber";
						return $this->returnArray($sql,3);
					}
				}
				*/
				
				function getTeacherTypeSelection($tag="", $teacherType="")
				{
					global $i_alert_pleaseselect, $i_nonteachingStaff, $i_teachingStaff;
					
					$teacherTypeSelection = "<SELECT name='teacherType' id='teacherType' $tag>";
					$teacherTypeSelection .= "<option value=''".(($teacherType=="") ? " selected" : "").">$i_alert_pleaseselect</option>";
					$teacherTypeSelection .= "<option value='1'".(($teacherType=="1") ? " selected" : "").">$i_teachingStaff</option>";
					$teacherTypeSelection .= "<option value='0'".(($teacherType=="0") ? " selected" : "").">$i_nonteachingStaff</option>";
					$teacherTypeSelection .= "</select>";
					
					return $teacherTypeSelection;
				}
				
		function getSelectClassWithWholeForm($attrib, $selected="", $firstValue="", $pleaseSelect="", $year="", $FormIDArr='', $TeachingClassOnly=0, $HideNoClassForm=0, $WebSAMSCodeArr='', $ClassIDArr='')
		{
			global $i_general_all;
			$year = ($year=="") ? Get_Current_Academic_Year_ID() : $year;
			
			$TeachingFormIDArr = '';
			$TeachingYearClassIDArr = '';
			if ($TeachingClassOnly == 1)
			{
				$fcm = new form_class_manage();
				$TeachingClassInfoArr = $fcm->Get_Class_List_By_Academic_Year($year, $FormIDArr, $TeachingOnly=1);
				$TeachingFormIDArr = Get_Array_By_Key($TeachingClassInfoArr, 'YearID');
				$TeachingYearClassIDArr = Get_Array_By_Key($TeachingClassInfoArr, 'YearClassID');
			}
			

			//$sql = "SELECT LVL.ClassLevelID, LVL.LevelName FROM INTRANET_CLASSLEVEL LVL left outer join INTRANET_CLASS CLS on (LVL.ClassLevelID=CLS.ClassLevelID) GROUP BY LVL.ClassLevelID";
			if ($FormIDArr != '') {
				$conds_YearID = " And YearID In (".implode(',', (array)$FormIDArr).") ";
			}
			if ($TeachingFormIDArr != '' && count((array)$TeachingFormIDArr) > 0) {
				$conds_TeachingYearID = " And YearID In (".implode(',', (array)$TeachingFormIDArr).") ";
			}
			if ($WebSAMSCodeArr != '') {
				$conds_WebSAMSCode = " And WEBSAMSCode In ('".implode("','", (array)$WebSAMSCodeArr)."')";
			}
				
			$sql = "SELECT YearID, YearName FROM YEAR Where 1 $conds_YearID $conds_TeachingYearID $conds_WebSAMSCode ORDER BY Sequence";
			$form = $this->returnArray($sql,3);

			$x = "<SELECT $attrib>\n";
			$empty_selected = ($selected == '')? "SELECTED":"";
			
			if($pleaseSelect!="")
				$x .= "<OPTION value='#' $empty_selected> -- $pleaseSelect -- </OPTION>\n";
				
			if($firstValue!="") {
				$x .= "<OPTION value='0'";
				$x .= ($selected=='0') ? " selected" : "";
				$x .=">$firstValue</OPTION>\n";
			}

			if(!is_numeric($selected)) $selectedClass = substr($selected, 2);

			$conds_TeachingYearClassID = '';
			if (is_array($TeachingYearClassIDArr)) {
				$conds_TeachingYearClassID = " And YearClassID In (".implode(',', (array)$TeachingYearClassIDArr).") ";
			}
			if(!empty($ClassIDArr))
			{
				$conds_ClassID = " And YearClassID In (".implode(',', (array)$ClassIDArr).") ";
			}
				
			$sql2 = "SELECT ClassTitleB5, ClassTitleEN, YearClassID, YearID FROM YEAR_CLASS WHERE AcademicYearID=$year $conds_TeachingYearClassID $conds_ClassID ORDER BY Sequence, ".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
			$Batch_result = BuildMultiKeyAssoc((array)$this->returnArray($sql2,3),array("YearID","YearClassID"));
			

			for($i=0;$i<sizeof($form);$i++) {
				list($temp, $tempLvlName) = $form[$i];

				//$sql2 = "SELECT ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 AND ClassLevelID=$temp ORDER BY ClassName";
//				$sql2 = "SELECT ClassTitleB5, ClassTitleEN, YearClassID FROM YEAR_CLASS WHERE AcademicYearID=$year AND YearID=$temp ORDER BY Sequence, ".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
//				$result = $this->returnArray($sql2,3);
				$result = array_values((array)$Batch_result[$temp]);
				
				if ($HideNoClassForm==1 && count((array)$result) == 0) {
					continue;
				}

				$selected_str = ($selected == "{$temp}")? "SELECTED":"";
				$x .= "<option value=\"{$temp}\" {$selected_str}>{$i_general_all} {$tempLvlName}</option>";

				for($j=0;$j<sizeof($result);$j++) {
					$tempName = Get_Lang_Selection($result[$j][0], $result[$j][1]);
					$clsID = $result[$j][2];
					$selected_str = ($clsID==$selectedClass? "SELECTED":"");
					$x .= "<OPTION value='::{$clsID}' {$selected_str}>&nbsp;&nbsp;&nbsp;{$tempName}</OPTION>\n";
				}
			}
			$x .= "</select>";
			return $x;
		}
		
		function storeStudent($stdid, $class, $academicYearID="")	# all student (all classes / whole form / each class)
		{
			$dataAry = array();

			$academicYearID = ($academicYearID=="") ? Get_Current_Academic_Year_ID() : $academicYearID;
			
			if($stdid != '0') {								# specific student
				$sql = "SELECT UserID FROM INTRANET_USER WHERE UserID=$stdid AND RecordType = 2 AND RecordStatus IN (0,1,2,3)";
				$result = $this->returnArray($sql,1);

				for($i=0;$i<sizeof($result);$i++) {
					$dataAry[] = $result[$i][0];
				}

			} else if($class == '0') {						# all classes all students
				$class = substr($class, 2);
				//$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) ORDER BY ClassName, ClassNumber";
				$sql = "SELECT ycu.UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$academicYearID) LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID) INNER JOIN INTRANET_USER USR ON (USR.UserID=ycu.UserID AND USR.RecordStatus IN (0,1,2,3)) WHERE yc.AcademicYearID=$academicYearID ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";
				$result = $this->returnArray($sql,1);

				for($j=0;$j<sizeof($result);$j++) {
					$dataAry[] = $result[$j][0];
				}

			} else if(is_numeric($class)) {			# all students of specific form
				$sql = "SELECT USR.UserID FROM INTRANET_USER USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (USR.UserID=ycu.UserID) LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID) WHERE y.YearID=$class AND yc.AcademicYearID=$academicYearID AND USR.RecordStatus IN (0,1,2,3) ORDER BY yc.Sequence, ycu.ClassNumber";
				$result = $this->returnArray($sql,1);
				
				for($i=0;$i<sizeof($result);$i++) {
					$dataAry[] = $result[$i][0];
				}

			} else {										# specific class , all students
				$class = substr($class, 2);
				$sql = "SELECT USR.UserID FROM INTRANET_USER USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (USR.UserID=ycu.UserID) LEFT OUTER JOIN YEAR_CLASS yc ON (ycu.YearClassID=yc.YearClassID) WHERE yc.YearClassID=$class AND USR.RecordStatus IN (0,1,2,3) ORDER BY ycu.ClassNumber";
				$result = $this->returnArray($sql,1);

				for($j=0;$j<sizeof($result);$j++) {
					$dataAry[] = $result[$j][0];
				}

			}

			return $dataAry;
		}
		
		function getSelectClassWithoutWholeForm($academocYearID="") 
		{
			$sql = "SELECT yc.YearClassID, ".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN")." FROM YEAR_CLASS yc LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID) WHERE yc.AcademicYearID='".Get_Current_Academic_Year_ID()."' ORDER BY y.Sequence, yc.Sequence";
			$ary = $this->returnArray($sql,2);
			
			return $ary;			
		}
		
		
		function getStudentByClassId($classIdAry) {
					
					 $sql = "SELECT 
								UserID, 
								ClassNumber,
					 			YearClassID
							FROM 							
								YEAR_CLASS_USER 					
							WHERE 
								YearClassID IN ('".implode("','", (array)$classIdAry)."')  
							ORDER BY
								ClassNumber
							";
				
				//debug_pr($classIdAry);
				//	 debug_pr($sql);
							    
			return $this->returnResultSet($sql);
		}
		
	function getSelectAlumniYears()
	{
		global $button_select, $linterface;
		
		$sql = "SELECT DISTINCT YearOfLeft, YearOfLeft FROM INTRANET_USER where RecordStatus='1' and RecordType=4 AND YearOfLeft IS NOT NULL AND YearOfLeft<>'' ORDER BY YearOfLeft" ;
		$rows = $this->returnArray($sql);
		
		return $rows;
	}
	
	####################
	# copy from libdisciplinev12.php
	####################
	function getStudentListByClassID($classIDAry, $selectFlag=1, $includeLeftStudent=0)
	{
		if(!is_array($classIDAry)) {
			$clsIDAry[0] = $classIDAry;
        } else {
			$clsIDAry = $classIDAry;
        }
			
		$academicYearID = Get_Current_Academic_Year_ID();
		
		if($selectFlag==1) {
			$name_field = getNameFieldWithClassNumberByLang("USR.");
        } else if($selectFlag==2) {
			$name_field = "CONCAT(".Get_Lang_Selection("yc.ClassTitleB5", "ClassTitleEN").",IF(ycu.ClassNumber=NULL,'',(CONCAT('-',ycu.ClassNumber))),' ',".getNameFieldByLang("USR.").")";
        } else {
			$name_field = getNameFieldWithClassNumberByLang("USR.");
        }
		
		if(sizeof($clsIDAry) > 0) {
			$clsIDs = implode(',',$clsIDAry);
        }

        // [2019-1220-1119-59235]
		$stuStatusList = '0,1,2';
		if($includeLeftStudent){
            $stuStatusList .= ',3';
        }
		
		$sql = "SELECT ycu.UserID, $name_field FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) INNER JOIN INTRANET_USER USR ON (USR.UserID=ycu.UserID) WHERE USR.RecordType=2 AND USR.RecordStatus IN ($stuStatusList) AND yc.YearClassID IN ($clsIDs) AND yc.AcademicYearID=$academicYearID ORDER BY yc.Sequence, ycu.ClassNumber";
		return $this->returnArray($sql,2);
	}
		
 }

} // End of directives
?>
