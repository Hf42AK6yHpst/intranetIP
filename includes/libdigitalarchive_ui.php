<?php

# modifying :      

##### Change Log [Start] #####
#   Date    :   2019-11-12 (Sam)
#               Modified DisplayDocument_Icon() and DisplayDocument() to make description redirectable if it is a URL
#
#   Date    :   2019-06-27 (Ray)
#               added Get_AccessRight_Import_confirm, Get_AccessRight_Import_Finish_Page
#
#	Date	:	2017-07-10 (Anna)
#				added checkCurrentIP, check computer IP access right
#
#	Date	:	2014-11-13 	(Carlos) [ip2.5.5.12.1]
#				Modified Get_Right_Element(), add enter key up event to submit the search on the text input
#
#	Date	:	2014-10-03 	(Carlos) [ip2.5.5.10.1]
#				Modified GET_MODULE_OBJ_ARR(), added [Access Setting] to admin menu
#
#	Date	:	2014-05-29	(Carlos)
#				Modified DisplayDocument_Icon() to display jpg file with thumbnail image
#
#   Date    :   2014-05-12  (Tiffany)
#               Modified the advance search in Get_Right_Element()
#
#   Date    :   2014-04-04  (Tiffany)
#               Modified DisplayDocument_Icon()
#
#   Date    :   2014-02-21  (Tiffany)
#               Modified Get_My_Favourite_Resource_More_Record(),Get_New_Resource_Record_Item(),
#               Get_My_Favourite_Resource_Record_Table(),Get_Resource_Record_Item_By_TagID(),
#               Get_Favourite_Resource_Record_Table() to handle the campus tv
#
#	Date	:	2014-02-18	(Yuen)
#				Modified Get_Display_More_Table() to support filter by class level

#	Date	:	2013-11-11	(Carlos)
#				Modified DisplayDocument(), change folder path to icon list mode
#
#	Date	:	2013-07-31	(Henry)
#				Modified Get_Folder_Navigation() that support new layout
#				-The hyper link of the filename
#
#	Date	:	2013-07-29	(Henry)
#				Modified Get_Tag_Record_Table() that support new layout
#
#	Date	:	2013-07-25	(Henry)
#				added function Display_File_List_Table_Icon(), DisplayDocument_Icon()
#
#	Date	:	2013-07-25	(Carlos)
#				added Get_Quickview_Folder_Div() and Get_Quickview_Folder_Tree() 
#
#	Date	:	2013-07-24	(Henry)
#				added function Get_My_Group_Table2(), Display_File_List_Table2() and modified function PageTagObj()
#
#	Date	:	2013-07-24	(Carlos)
#				modified Include_JS_CSS(), include jquery.fancybox-1.3.4
#
#	Date	:	2013-07-08	(Henry Chan)
#				modified Get_Folder_Navigation to display the group name of the file path
#
#	Date	:	2013-07-04	(Henry Chan)
#				added Get_My_Group_Select_Menu() to support searching with group (completed)
#
#	Date	:	2013-07-03	(Henry Chan)
#				added the textfield at line to support searching with tag independently
#				added Get_My_Group_Select_Menu() to support searching with group (incomplete)
#
#	Date	:	2013-04-25	(yuen)
#				modified Get_Display_AdvanceSearchResult_Table() to support searching with tag
#
#	Date	:	2013-04-10	(Rita)
#				modified Get_My_Subject_Table() add style margin-top and <br /> to fix display issue
#
#	Date	:	2013-04-08	(Rita)
#				modified Get_Like_Content() fix display problem
#
#	Date	:	2013-04-02	(Rita)		
#				modified Get_Like_Content()
#
#	Date	:	2013-03-27 (Rita)
#				amend GET_MODULE_OBJ_ARR() add admin menu for Subject eResources 
#				add Get_Subject_eResources_Group_DBTable()
#
#	Date	:	2013-02-14 (Rita)
#				amend GET_MODULE_OBJ_ARR(), change admin menu checking
#
#	Date	:	2013-01-30 (Rita)
#				amend GET_MODULE_OBJ_ARR(), change admin menu checking
#
#	Date	:	2013-01-17 (Carlos) (Restored on 2013-02-19)
#				modified Get_Group_Access_Right_Settings_Index() - check and disable [Member View Own] and [Member Manage Own] access right, replace with hidden values
#
#	Date	: 	2013-01-08 (Rita)
#				modifeid GET_MODULE_OBJ_ARR() - add menu, Display_Name_Of_Last_Modified() - include time
#
#	Date	:	2013-01-07 (Carlos)
#				modified Get_Right_Element() - add date checking for js submitSearch(1)
#
#	Date	:	2013-01-02 (Carlos)
#				modified Get_Green_Tab() - only load first level of groups, do load sub folders by ajax
#
#	Date	:	2012-05-31 (Henry Chow)
#				modified Get_Access_Right_Group_Table(), add keyword search feature
#
#	Date	:	2012-03-26 (Henry Chow)
#				modified printLeftMenu(), display different icon for "Subject Reference" and "Admin Doc" respectively.
#
#	Date	:	2012-03-15 (Henry Chow)
#				modified printLeftMenu(), display different hyperlink in icon on left top corner for "Subject Reference" and "Admin Doc" respectively.
#
#	Date	:	2012-03-12 (Henry Chow)
#				edit javascript checking on GroupTitle and GroupCode (fields cannot be blanked)
#
###### Change Log [End] ######

//include_once("libdigitalarchive.php");
include_once ($intranet_root . "/includes/libdigitalarchive.php");

define("FUNCTION_NEWEST", "ResourceNewest");
define("FUNCTION_FAVOURITE", "ResourceFavourite");
define("FUNCTION_MY_FAVOURITE", "ResourceMyFavourite");
define("FUNCTION_MY_SUBJECT", "ResourceMySubject");
define("FUNCTION_ADVANCE_SEARCH", "AdvanceSearch");
define("FUNCTION_TAG", "Tag");
define("FUNCTION_MORE", "More");

//class libdigitalarchive_ui extends interface_html {
class libdigitalarchive_ui extends interface_html {

	var $Module;
	var $File_text;
	var $File_doc;
	var $File_xls;
	var $File_ppt;
	var $File_pdf;
	var $File_vdo;
	var $File_image;
	var $File_sound;
	var $File_zip;
	var $HighlightLimit;
	var $DiaplayDescriptionLengthLimit;
	var $right_element;
	var $isFromeAdmin;
	
	function libdigitalarchive_ui() {
		$this->Module = "DigitalArchive";

		$this->DiaplayDescriptionLengthLimit = 100;

		$this->HighlightLimit = 10;

		# for file extension checking
		$this->FileExtension = array (
			"txt" => "text",
			"doc" => "doc",
			"docx" => "doc",
			"csv" => "xls",
			"xls" => "xls",
			"xlsx" => "xls",
			"ppt" => "ppt",
			"pptx" => "ppt",
			"pdf" => "pdf",
			"rmvb" => "vdo",
			"wmv" => "vdo",
			"avi" => "vdo",
			"mov" => "vdo",
			"mp4" => "vdo",
			"flv" => "vdo",
			"gif" => "image",
			"jpg" => "image",
			"png" => "image",
			"wav" => "sound",
			"wma" => "sound",
			"mp3" => "sound",
			"rar" => "zip",
			"xml" => "text",
			"zip" => "zip"
		);
		$this->isFromeAdmin = strpos($_SERVER["SCRIPT_NAME"], "/admin_doc/");
	}

	function GET_MODULE_OBJ_ARR() {
		global $Lang, $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT, $PrinterIPLincense, $sys_custom, $CurrentPageArr;
		global $CurrentPage;
		parent :: interface_html();
		
		$lda = new libdigitalarchive();
		$allowed_IPs = trim($lda->GetDigitalArchiveGeneralSetting("AllowAccessIP"));
		$ip_addresses = explode("\n", $allowed_IPs);
		checkCurrentIP($ip_addresses, $Lang['Header']['Menu']['DigitalArchive']);
		

		if ($CurrentPageArr['DigitalArchive_SubjectReference']) { # Subject Reference
			$MODULE_OBJ['title'] = "<a href='".$PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/'>".$Lang["DigitalArchive"]["SubjectReference"] . "</a> ";
		} else { # Admin Document
			$MODULE_OBJ['title'] = "<a href='".$PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/'>".$Lang["DigitalArchive"]["DigitalArchiveName"] . "</a> ";

			# check whether the scanner to DA service is established
			$scannerReady = "";
			if (file_exists($PATH_WRT_ROOT . "rifolder/global.php")) {
				include_once ($PATH_WRT_ROOT . "rifolder/global.php");
				if (sizeof($PrinterIPLincense) > 0) {
					$scannerReady = 1;
				}
			}
			if ($scannerReady) {
				if ($sys_custom['digital_archive_general_scan_icon']) {
					$MODULE_OBJ['title'] .= " <img src=\"{$image_path}/{$LAYOUT_SKIN}/digital_archive/scan_ready.png\" width=\"105\" height=\"32\" align=\"absmiddle\">";
				} else {
					$MODULE_OBJ['title'] .= " <img src=\"{$image_path}/{$LAYOUT_SKIN}/digital_archive/Ricoh_ready.png\" width=\"105\" height=\"32\" align=\"absmiddle\">";
				}
			}
		}

        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass Digital Archive";'."\n";
        $js.= '</script>'."\n";
        $MODULE_OBJ['title'] .= $js;

		//$MODULE_OBJ['title_css'] = "menu_opened";
		//$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_digital_archive.png";

		# Check if not Admin Page, show logo only
		if ($CurrentPageArr['DigitalArchiveAdminMenu'] == true) { # Show admin menu
			$MODULE_OBJ['title_css'] = "menu_opened";
			//			if($CurrentPageArr['DigitalArchive_SubjectReference']) {
			//				$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_subject_resources.png";
			//				$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/DigitalArchive/";
			//			}else{
			$MODULE_OBJ['logo'] = $PATH_WRT_ROOT . "images/{$LAYOUT_SKIN}/leftmenu/icon_digital_archive.png";
			$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/";
			//			}

			##### Menu #####
			# Current Page Information init		
			$PageReports_DeleteLog = 0;
			$PageSettings_AccessRight = 0;
			$PageReports = 0;
			$PageSettings = 0;
			//Henry added
			//$MaxFileSize = 0;
			//$FileFormatSettings = 0;
			$UploadPolicy = 0;
			//End Henry added

			# Current Page Information init
			switch ($CurrentPage) {
				#-------------- Overview
				case "Overview" :
					$PageOverview = 1;
					break;

					#-------------- Reports
				case "Reports_UsageSummary" :
					$PageReports = 1;
					$PageReports_Usage = 1;
					break;
					#-------------- Reports
				case "Reports_SystemReport" :
					$PageReports = 1;
					$PageReports_DeletionLog = 1;
					break;

					#-------------- group and access right
				case "Settings_AccessRight" :
					$PageSettings = 1;
					$PageSettings_AccessRight = 1;
					break;

					#-------------- group category
				case "Settings_GroupCategory" :
					$PageSettings = 1;
					$PageSettings_GroupCategory = 1;
					break;

					//Henry added
					#-------------- maximum file size
					/*case "Settings_MaxFileSize":
						$PageSettings = 1;
						$MaxFileSize = 1;
						break;*/

					#-------------- file format settings
					/*case "Settings_FileFormatSettings":
						$PageSettings = 1;
						$FileFormatSettings = 1;
						break;*/

				case "Settings_UploadPolicy":
					$PageSettings = 1;
					$UploadPolicy = 1;
					break;
					//Henry end added
					
				case "Settings_AccessSetting":
					$PageSettings = 1;
					$PageSettings_AccessSetting = 1;
					break;
					
				case "Settings_AllowAccessIP":
					$PageSettings = 1;
					$PageSettings_AllowAccessIP = 1;
					break;
			}

			# Menu information
			# Management
			//$MenuArr["Reports"]["Child"]["AdministrativeDocument"] = array($Lang['Menu']['DigitalArchive']['Settings']['AdministrativeDocument'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/reports/del_log/", $PageReports_DeleteLog);

			# Reports
			$MenuArr["Reports"] = array (
				$Lang['Menu']['DigitalArchive']['Reports']['Title'],
				$PATH_WRT_ROOT . "",
				$PageReports
			);
			if ($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
				$MenuArr["Reports"]["Child"]["UsageReport"] = array (
					$Lang['DigitalArchive']['ReportUsage'],
					$PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/reports/system_report/usage_summary.php",
					$PageReports_Usage
				);
				$MenuArr["Reports"]["Child"]["DeletionLog"] = array (
					$Lang['Menu']['DigitalArchive']['Reports']['DeletionLog'],
					$PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/reports/system_report/del_log/index.php?clearCoo=1",
					$PageReports_DeletionLog
				);
			}

			# Settings
			$MenuArr["Settings"] = array (
				$Lang['General']['Settings'],
				"",
				$PageSettings
			);
			//$Lang['General']['Settings'] 
			if ($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
				$MenuArr["Settings"]["Child"]["GroupCategory"] = array (
					$Lang["DigitalArchive"]["GroupCategory"],
					$PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/group_category.php",
					$PageSettings_GroupCategory
				);
				$MenuArr["Settings"]["Child"]["Access_Right"] = array (
					$Lang['Menu']['DigitalArchive']['Settings']['AccessRight'],
					$PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/access_right.php",
					$PageSettings_AccessRight
				);
				//Add Max. File Size and File format code here...  by Henry Chan
				//$MenuArr["Settings"]["Child"]["MaxFileSize"] = array($Lang['DigitalArchive']['MaxFileSize'] , $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/maximum_file_size.php", $MaxFileSize);
				//$MenuArr["Settings"]["Child"]["FileFormatSettings"] = array( $Lang['DigitalArchive']['FileFormatSettings'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/file_format_settings.php?clearCoo=1", $FileFormatSettings);
				$MenuArr["Settings"]["Child"]["UploadPolicy"] = array (
					$Lang['DigitalArchive']['UploadPolicy'],
					$PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/maximum_file_size.php",
					$UploadPolicy
				);
				
				$MenuArr["Settings"]["Child"]["AccessSetting"] = array(
					$Lang['DigitalArchive']['AccessSetting'],
					$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/access_setting.php',
					$PageSettings_AccessSetting
				);
				$MenuArr["Settings"]["Child"]["Settings_AllowAccessIP"] = array(
					$Lang['Menu']['DigitalArchive']['Settings']['Settings_AllowAccessIP'],
					$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/allow_access_ip_index.php',
					$PageSettings_AllowAccessIP
				);
			}

			$MODULE_OBJ['menu'] = $MenuArr;

			# Check if Subject eResources Admin Page
		}
		elseif ($CurrentPageArr['DigitalArchiveSubjecteResourcesAdminMenu'] == true) {
			$MODULE_OBJ['title_css'] = "menu_opened";
			$MODULE_OBJ['logo'] = $PATH_WRT_ROOT . "images/{$LAYOUT_SKIN}/leftmenu/icon_digital_archive.png";
			$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/";

			##### Menu Start #####
			$PageSettings = 0;

			switch ($CurrentPage) {

				#-------------- group and access right
				case "Settings_Group" :
					$PageSettings = 1;
					$PageSettings_Group = 1;
					break;

			}

			# Settings
			$MenuArr["Settings"] = array (
				$Lang['General']['Settings'],
				"",
				$PageSettings
			);
			//if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive_SubjecteResources']){
			$MenuArr["Settings"]["Child"]["Group"] = array (
				$Lang['Menu']['DigitalArchive']['Settings']['AccessRight'],
				$PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/subject_eResources_settings/group/index.php?clearCoo=1",
				$PageSettings_Group
			);

			//}

			$MODULE_OBJ['menu'] = $MenuArr;
			#####  Menu End  #####

		} else { # Show logo only		
//			parent :: interface_html("digital_archive_default.html");
//			$MODULE_OBJ['title_css'] = "menu_small";
//			$MODULE_OBJ['CustomLogo'] = $this->printLeftMenu();

		}

		return $MODULE_OBJ;
	}

	function Include_JS_CSS() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;

		//include_once($PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/common_js_lang.php');
		$x = '
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.blockUI.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/ui.core.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/ui.draggable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/ui.droppable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/ui.selectable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.jeditable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.pack.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/script.js"></script>
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.css"  type="text/css" media="screen">
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.css" type="text/css" media="screen" />
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.ui.all.css" type="text/css" media="all" />
					';
		return $x;
	}

	function Get_Right_Element($__GET = array ()) {
		global $Lang, $linterface, $i_To, $PATH_WRT_ROOT, $LAYOUT_SKIN;
        $lda = new libdigitalarchive();
		//get the string of the groupid(s)
		if ($__GET["GroupIDArr"] != '')
			$groupIds = implode(",", $__GET["GroupIDArr"]);
		else
			if ($_REQUEST["selectedGroups"] != '')
				$groupIds = $_REQUEST["selectedGroups"];

		if ($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
			$AdminButton = '|  <a href="access_right.php" ><img src="/images/'.$LAYOUT_SKIN.'/icon_manage.gif" border=0 align="absmiddle"><font size="3">' . $Lang['DigitalArchive']['AdminSettings'] . '</font></a>';
		}
		
        $tagAry = returnModuleAvailableTag($lda->AdminModule, 1);
        $tagNameAry = array_values($tagAry);    
       	foreach ($tagNameAry as $value) $Taglist .="'".addslashes($value)."', ";
        $Taglist = rtrim($Taglist,', ');
        $SelectedTaglist = trim($__GET['tag']);
        if($SelectedTaglist!=""){
	       $SelectedTaglist_handle = explode(",", $SelectedTaglist);
           for($i=0;$i<count($SelectedTaglist_handle);$i++){
            	$SelectedTaglist_handle_new[$i] = trim($SelectedTaglist_handle[$i],'[]"\\');
            	$SelectedTaglist_handle_new[$i] = "'".$SelectedTaglist_handle_new[$i]."'";
           }
        	$SelectedTaglist = implode(",", $SelectedTaglist_handle_new);		
        }
		$right_element .= '<form name="SearchForm" id="SearchForm" method="GET"> ';
		$right_element .= '<table ><tr><td nowrap><div class="DA_search" ><input name="keyword" id="keyword" type="text"  class="keywords" value="' . intranet_htmlspecialchars($__GET['keyword']) . '" onkeyup="if(event.which==13 || event.keyCode == 13) submitSearch(0);" />
			                    	  <input type="button" value="' . $Lang['Btn']['Search'] . '" class="formsmallbutton" onclick="submitSearch(0)"/> | <a href="advance_search.php?TB_iframe=true&width=700&height=500" class="thickbox" title="'.$Lang['DigitalArchive']['Advanced'].'">' . $Lang['DigitalArchive']['Advanced'] . '</a> ' . $AdminButton . '</div></td></tr></table>
				';
		$right_element .= '
						<script language="javascript">		
						function submitSearch(advSearch) {
							//$(\'#advanceSearchFlag\').val(advSearch);
							$(\'#advance\').val(advSearch);
							if(advSearch==1 && $(\'#byDateRange\').is(\':checked\')){
								var isValid = true; 
								var startdateObj = document.getElementById(\'StartDate\');
								var enddateObj = document.getElementById(\'EndDate\');
								if(!check_date_without_return_msg(startdateObj)) {
									$(\'#DPWL-StartDate\').html(\'' . $Lang['General']['InvalidDateFormat'] . '\').show();
									startdateObj.focus();
									isValid = false;
								}
								if(!check_date_without_return_msg(enddateObj)) {
									$(\'#DPWL-EndDate\').html(\'' . $Lang['General']['InvalidDateFormat'] . '\').show();
									enddateObj.focus();
									isValid = false;
								}
								if(isValid && startdateObj.value.Trim() > enddateObj.value.Trim()) {
									$(\'#DPWL-StartDate\').html(\'' . $Lang['General']['JS_warning']['InvalidDateRange'] . '\').show();
									startdateObj.focus();
									isValid = false;
								}
								if(!isValid) return;
							}
							document.SearchForm.action = "searchResult.php";
							document.SearchForm.submit();
								
						}
						
                        </script>                                              		
				';
		$right_element .= '</form>';

		return $right_element;
	}

	function PageTagObj($selected = 1) {
		global $Lang, $_SESSION, $sys_custom;
		$lda = new libdigitalarchive();

		$ary[$Lang["DigitalArchive"]["SubjectReference"]] = "/home/eAdmin/ResourcesMgmt/DigitalArchive/";
		if (($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] || sizeof($lda->UserInGroupList()) > 0) && ($_SERVER["SCRIPT_NAME"] != "/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/index2.php" && $_SERVER["SCRIPT_NAME"] != "/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/index.php") && $_SERVER["SCRIPT_NAME"] != "/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/fileList.php" && $_SERVER["SCRIPT_NAME"] != "/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/fileList2.php" && $_SERVER["SCRIPT_NAME"] != "/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/fileList_Icon.php") {
			$ary[$Lang['Menu']['DigitalArchive']['Settings']['AccessRight']] = "/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/";
		}
		elseif ($sys_custom['digital_archive']['hide_groupmenu'] && $this->isFromeAdmin) {
			//var_dump('<div class="DA_tab">'.$this->Get_Blue_Tab(3).'</div>');
			return array (
				array (
					'<div class="DA_tab" >' . $this->Get_Blue_Tab(3) . '</div>'
				)
			);

			/*$TAGS_OBJ[] = array($Lang['DigitalArchive']['MyGroup'], "javascript:Display_Blue_Content(3)", 1);
			$TAGS_OBJ[] = array($Lang['DigitalArchive']['Latest'], "javascript:Display_Blue_Content(1)", 0);
			$TAGS_OBJ[] = array($Lang['DigitalArchive']['LastViewed'], "javascript:Display_Blue_Content(2)", 0);
			
			return $TAGS_OBJ;
			*/
		}
		if (is_array($ary) && sizeof($ary) > 0) {
			$i = 1;
			foreach ($ary as $_name => $_link) {
				$displayFlag = ($i == $selected) ? 1 : 0;
				$TAGS_OBJ[] = array (
					$_name,
					$_link,
					$displayFlag
				);
				$i++;
			}
		}
		// commented by Henry Chow on 2011-10-14, since split the page of "Subject Reference" & "Admin Doc"
		//return $TAGS_OBJ;

		//return ($selected==1) ? array($TAGS_OBJ[0]) : array($TAGS_OBJ[1]); 
		return ($selected == 1) ? array (
			""
		) : array (
			$TAGS_OBJ[1]
		);
	}

	function PageSubTagObj($GroupID = "", $selected = 1) {
		global $Lang;
		$ary[] = array (
			$Lang["DigitalArchive"]["Directory"],
			"list.php?GroupID=" . $GroupID,
			 (($selected == 1) ? 1 : 0)
		);
		//$ary[] = array($Lang["DigitalArchive"]["MyFavourite"],"myFavourite.php?GroupID=".$GroupID,(($selected==2)?1:0));

		return $this->GET_SUBTAGS($ary);
	}

	function PageAccessRightSubTagObj($GroupID = "", $selected = 1) {
		global $Lang;
		$ary[] = array (
			$Lang["DigitalArchive"]["AccessRight"],
			"access_right_detail.php?GroupID=" . $GroupID,
			 (($selected == 1) ? 1 : 0)
		);
		if ($GroupID != "") {
			$ary[] = array (
				$Lang["DigitalArchive"]["UserList"],
				"access_right_user.php?GroupID=" . $GroupID,
				 (($selected == 2) ? 1 : 0)
			);
		}

		return $this->GET_SUBTAGS($ary);
	}

	function printLeftMenu() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $CurrentPageArr;

		if ($CurrentPageArr['DigitalArchive_SubjectReference']) {
			$img = $PATH_WRT_ROOT . "images/{$LAYOUT_SKIN}/leftmenu/icon_subject_resources.png";
		} else {
			$img = $PATH_WRT_ROOT . "images/{$LAYOUT_SKIN}/leftmenu/icon_digital_archive.png";
		}
		$x .= '<span><div id="Reading_Scheme_logo">';
		if ($CurrentPageArr['DigitalArchive_SubjectReference']) {
			$x .= "<a href='" . $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/'><img border=\"0\" width=\"110\" height=\"100\" src=\"$img\" /></a>";
		} else {
			$x .= "<a href='" . $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/'><img border=\"0\" width=\"110\" height=\"100\" src=\"$img\" /></a>";
		}
		$x .= '</div></span>';

		return $x;
	}

	function Get_Newest_Resource_Record_Table($YearID = "") {
		global $Lang;
		$x = '
					<div class="reading_board_top_left">
						<div class="reading_board_top_right">
							<h1><span>' . $Lang["DigitalArchive"]["Newest"] . '</span></h1>
							<div class="thumb_list_tab">
								' . $this->Get_Form_Display($YearID, "Get_Resource_Newest_Table") . '
							</div>
							<a class="reading_more" href="displayMore.php?flag=' . FUNCTION_NEWEST . '">' . $Lang["DigitalArchive"]["More"] . '..</a>
						</div>
					</div>
					<div class="reading_board_left">
						<div class="reading_board_right">';
		$x .= $this->Get_New_Resource_Record_Item($YearID, $this->HighlightLimit);
		$x .= '
						</div>
					</div>
					<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
				';

		return $x;
	}

	function Get_Form_Display($YearID = "", $jsFunctionName) {
		global $Lang, $UserID;

		$lda = new libdigitalarchive();

		$AcademicYearID = Get_Current_Academic_Year_ID();

		if ($_SESSION['UserType'] == USERTYPE_STAFF) {

			$sql = "SELECT yc.YearID, y.YearName FROM YEAR_CLASS_TEACHER yct INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=yct.YearClassID AND yc.AcademicYearID='$AcademicYearID') INNER JOIN YEAR y ON (y.YearID=yc.YearID) WHERE yct.UserID='$UserID' GROUP BY y.YearID ORDER BY y.Sequence";

		} else
			if ($_SESSION['UserType'] == USERTYPE_STUDENT) {

				$sql = "SELECT yc.YearID, y.YearName FROM YEAR_CLASS yc INNER JOIN YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID AND ycu.UserID='$UserID' AND yc.AcademicYearID='$AcademicYearID') INNER JOIN YEAR y ON (y.YearID=yc.YearID) GROUP BY y.YearID ORDER BY y.Sequence";

			}
		$formAry = $lda->returnArray($sql, 2);

		if ($YearID == "") {
			$allClassSelected = ' class="thumb_list_tab_on"';
		} else {
			$classSelected = ' class="thumb_list_tab_on"';
		}

		$x .= '<a href="javascript:;" onClick="js_Hide_ClassLevel();resetOtherFlag(\'\');' . $jsFunctionName . '(\'\')" ' . $allClassSelected . ';><span>' . $Lang["DigitalArchive"]["AllForms"] . '</span></a>';
		$delim = " <em> | </em> ";

		$x .= $delim . '<a href="javascript:;" onClick="js_Show_ClassLevel(this, \'' . $jsFunctionName . '\', \'' . $YearID . '\'); checkDisplayFlag(\'' . $jsFunctionName . '\')" ' . $classSelected . '><span>' . $Lang["DigitalArchive"]["Level"] . '</span></a>';
		/*
		for($i=0, $i_max=sizeof($formAry); $i<$i_max; $i++) {
			list($yID, $yName) = $formAry[$i];
			$classSelected = ($YearID==$yID) ? ' class="thumb_list_tab_on"' : '';
		
			$x .= $delim.'<a href="javascript:;" onClick="'.$jsFunctionName.'('.$yID.')" '.$classSelected.'><span>'.$yName.'</span></a>';	
		}
		*/

		return $x;
	}

	function Get_New_Resource_Record_Item($YearID, $limit = "") {
		global $Lang, $i_no_record_exists_msg;

		$lda = new libdigitalarchive();

		if ($YearID != "") {
			$conds = " WHERE lvl.ClassLevelID='$YearID'";
		}

		$data = $lda->Get_Newest_More_SQL($conds, $returnSqlOnly = 0, $limit);

		for ($i = 0, $i_max = sizeof($data); $i < $i_max; $i++) {
			list ($recordID, $title, $description, $dateInput, $url, $fileName, $fileHashName, $sourceExtension, $likeCount) = $data[$i];

			$description = chopString($description, $this->DiaplayDescriptionLengthLimit, '...');

			if ($sourceExtension=="url") {
				$ext = "file_htm";
				$link = $url;
				$link_style = ' target="_blank"';
				$img = '<a href="' . $url . '" ' . $link_style . ' class="' . $ext . '">&nbsp;</a>';
				$img = '<span onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$recordID.',0,1);">'.$img.'</span>';
				$h2_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$recordID.',0,1);"';
				$link_onClick = '';
			}
			elseif($sourceExtension=="tvurl"){
		    	$ext = "file_vdo";
				$link = 'javascript:void(0);';
				$link_style = '';
				$link_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$recordID.',\'download.php?FileHashName='.$fileHashName.'\',1);loadCampusTV(\''.$recordID.'\')"';
				$img = '<a href="'.$link.'" class="'.$ext.'"'.$link_onClick.'>&nbsp;</a>';
				$h2_onClick = '';			}	
			else {
				$ext = (isset ($this->FileExtension[$sourceExtension]) && $this->FileExtension[$sourceExtension] != "") ? "file_" . $this->FileExtension[$sourceExtension] : "file_unknown";
				//$link = 'download.php?FileHashName=' . $fileHashName;
				$link = 'javascript:void(0);';
				$link_style = '';
				$link_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$recordID.',\'download.php?FileHashName='.$fileHashName.'\',1);"';
				$img = '<a href="'.$link.'" class="'.$ext.'"'.$link_onClick.'>&nbsp;</a>';
				$h2_onClick = '';
			}

			$x .= '
							<div class="new_item">
								<div class="new_item_top_left"><div class="new_item_top_right"></div></div>
									<div class="new_item_left"><div class="new_item_right">
										<div class="reading_recommend_book_list">
											<div class="recommend_book_cover">' . $img . '</div>
												<div class="recommend_book_detail">
													<h2'.$h2_onClick.'><a href="'.$link.'" '.$link_style.$link_onClick.'>' . $title . ' <span class="date_time">' . $this->Get_Date_Diff($dateInput, $compareWith = "") . '</span>
													<br />
													<span>' . nl2br($description) . '</span>
													</a></h2>
													<div class="tag_list">' . $lda->Display_Tag_By_Resource_Record_ID($recordID, $returnFlag = 'text') . '</div>
													<div class="like_comment Div_'.FUNCTION_NEWEST.'_'.$recordID.'" id="Div_'.FUNCTION_NEWEST.'_'.$recordID . '">
							';
			$x .= $this->Get_Like_Content(FUNCTION_NEWEST, $recordID, '', 1);
			$x .= '
													</div>
													<p class="spacer"></p>
													<p class="spacer"></p>
												</div>  
											</div>
										<p class="spacer"></p>
										</div>
									</div>
								<div class="new_item_bottom_left"><div class="new_item_bottom_right"></div></div>
							</div>
						';

		}
		if (sizeof($data) == 0) {
			$x .= '
							<div class="new_item">
								<div class="new_item_top_left"><div class="new_item_top_right"></div></div>
									<div class="new_item_left"><div class="new_item_right">
										<div class="reading_recommend_book_list">
										' . $i_no_record_exists_msg . '
										<p class="spacer"></p>
										 
										</div>
									</div></div>
								<div class="new_item_bottom_left"><div class="new_item_bottom_right"></div></div>
							</div>
						';

		}
		return $x;
	}
	function Get_Like_Content($FunctionName, $RecordID, $delimiter = "", $RefreshTable=0){
		global $Lang, $UserID;

		$lda = new libdigitalarchive();
		list($likeButton,$likeBubble) = $this->Get_Like_Button($FunctionName, $RecordID);
		list($rateButton,$rateBubble) = $this->Get_Rating_Button($FunctionName, $RecordID, $RefreshTable);
		$viewButton = $this->Get_View_Button($FunctionName, $RecordID);
		$x = '';
		$x .= '<div class="doc_comment_group">';
	    	$x .= '<ul>';
	        	$x .= '<li>'.$likeButton.'</li>';
	            $x .= '<li class="seperator"></li>';
	            $x .= '<li>'.$rateButton.'</li>';
	            $x .= '<li class="seperator"></li>';
	            $x .= '<li>'.$viewButton.'</li>';
	        
	        $x .= '</ul>';
	        
	        $x .= $likeBubble;
	        $x .= $rateBubble;        
	    $x .= '</div>';
		

		

		return $x;		
	}
	function Get_Rating_Button($FunctionName, $RecordID, $RefreshTable=1) {
		global $Lang, $UserID;

		$lda = new libdigitalarchive();

		$iRate = false;
		$rateInfo = $lda->Get_Rating_Detail_By_Resource_ID($RecordID);
		$RateUserAry = BuildMultiKeyAssoc($rateInfo,'UserID',array('Rating'));
		$RatingAry = Get_Array_By_Key($rateInfo,'Rating');
		$rateCount = sizeof($rateInfo);
		$AvgRating = $rateCount>0?my_round(array_sum($RatingAry)/$rateCount,1):0;
		$rateText = $Lang['DigitalArchive']['Rating'];
		$userRating = $RateUserAry[$UserID]['Rating'];
		if (!empty($userRating)) {
			$iRate = true;
		}
		if($AvgRating>0){
			$rateText .= '('.$Lang['DigitalArchive']['Average'].':'.$AvgRating.')';
		}
		$rateButton = '<a href="javascript:void(0);" class="btn_comment_option a_Rate_'.$RecordID.'" onClick="js_Show_Rating_Status(\''.$FunctionName.'\','.$RecordID.')">'.$rateText.'</a>';
		$rateBubble = '<div id="Div_'.$FunctionName.'_Rate_'.$RecordID.'" class="board_comment_detail board_comment_rate" style="display:none;">';
		$rateBubble .= '<em></em>';
		$rateBubble .= '<span>';
		if($iRate){
			$rateBubble .= $Lang['DigitalArchive']['MyReview'].': ';
	        $rateBubble .= '<u>'.$userRating.'</u>';
		}else{
			$rateBubble .= $Lang['DigitalArchive']['ReviewNow'].':';
			$rateBubble .= $this->Get_Number_Selection('Rating_'.$FunctionName.'_'.$RecordID,0,10,$SelectedValue='', $Onchange='', $noFirst=1);	
			$rateBubble .= $this->GET_BTN($Lang['Btn']['Submit'],'button','js_Update_Rating(\''.$FunctionName.'\','.$RecordID.','.$RefreshTable.')');		
		}
		$rateBubble .= '<a class="btn_view_stat" href="javascript:void(0);" onClick="js_View_Details(\''.$RecordID.'\',\'gen_rating_stat\')" title="'.$Lang['DigitalArchive']['ViewStatistics'].'">&nbsp;</a>';	
		$rateBubble .= '</span>';
		$rateBubble .= '</div>';


		
		return array($rateButton,$rateBubble);
	}

	function Get_Like_Button($FunctionName, $RecordID) {
		global $Lang, $UserID;

		$lda = new libdigitalarchive();

		##Like
		$iLike = false;
		$likeInfo = $lda->Get_Like_Detail_By_Resource_ID($RecordID);
		$likeCount = sizeof($likeInfo);
		$likeText = '';
		
		if (in_array($UserID, $likeInfo)) {
			$iLike = true;
			$likeCount--;
		}
		if ($iLike) {
			$text = $Lang["DigitalArchive"]["Unlike"];
			$action = 0;
			$you = $Lang["DigitalArchive"]["You"];
		} else {
			$text = $Lang["DigitalArchive"]["Like"];
			$action = 1;
			$you = "";
		}
		$likeButton = '<a href="javascript:void(0);" class="btn_comment_option" onClick="Update_Like_Status(\''.$FunctionName.'\','.$RecordID.','.$action.')">'.$text.'</a>';
		if ($likeCount > 0) {
			if ($iLike) { //&
				$likeText .= $Lang["DigitalArchive"]["AndSign"];
			} // 50 ppl
			$likeText .= '<a href="javascript:void(0);" onClick="js_View_Details(\''.$RecordID.'\',\'view_like_list\')">' . $likeCount . $Lang["DigitalArchive"]["People"] . '</a>';
		}
		if ($iLike || $likeCount > 0) { //50 ppl Like
			$likeText .= $Lang["DigitalArchive"]["ChooseLike"];
			$likeBubble = '<div id="Div_'.$FunctionName.'_Like_'.$RecordID.'" class="board_comment_detail board_comment_like">';
		    $likeBubble .= '<em></em>';
			$likeBubble .= '<span>'.$you.$likeText.'</span>';
			$likeBubble .= '</div>'; 
		}else{
			$likeBubble = '<div id="Div_'.$FunctionName.'_Like_'.$RecordID.'" class="board_comment_detail board_comment_like">';
			$likeBubble .= '</div>'; 			
		}
		return array($likeButton,$likeBubble);
	}
	function Get_View_Button($FunctionName, $RecordID) {
		global $Lang, $UserID;

		$lda = new libdigitalarchive();

		$viewCount = $lda->Get_View_Detail_By_Resource_ID($RecordID);
		$viewText = $Lang['DigitalArchive']['View'].'('.$viewCount.')';
		
		$viewButton = $viewCount>0?'<a href="javascript:void(0);" onClick="js_View_Details(\''.$RecordID.'\',\'view_hitrate_list\')">':'<a href="javascript:void(0);">';
		$viewButton .= $viewText.'</a>';
		
		
		return $viewButton;
	}	
	/*function Get_Like_Content($FunctionName, $RecordID, $delimiter = "") {
		global $Lang, $UserID;

		$lda = new libdigitalarchive();

		$iLike = false;
		$likeInfo = $lda->Get_Like_Detail_By_Resource_ID($RecordID);

		$likeCount = sizeof($likeInfo);
		if (in_array($UserID, $likeInfo)) {
			$iLike = true;
			$likeCount--;
		}

		if ($likeCount > 0) {
			if ($iLike) {
				$likeText = $Lang["DigitalArchive"]["AndSign"];
			}
			$likeText .= '<a href="javascript:void(0);" onmouseover="js_Open_Like_List(this,\'' . $FunctionName . '\',\'' . $RecordID . '\')" onmouseout="js_Hide_Like_List()">' . $likeCount . $Lang["DigitalArchive"]["People"] . '</a>';
		}

		if ($iLike || $likeCount > 0) {
			$likeText .= $Lang["DigitalArchive"]["ChooseLike"];
		}

		$x = '';
		if (sizeof($likeInfo) == 0) { # no one LIKE 
			$x = ' <a href="javascript:void(0);" onClick="Update_Like_Status(\'' . $FunctionName . '\',' . $RecordID . ',1)">' . $Lang["DigitalArchive"]["Like"] . '</a>';
		} else { # with LIKE record
			
			if($iLike) {
				$x = '<a href="javascript:;" onClick="Update_Like_Status(\''.$FunctionName.'\','.$RecordID.',0)">'.$Lang["DigitalArchive"]["Unlike"].'</a>';
				$x .= '<span class="like_num">'.$Lang["DigitalArchive"]["You"].$likeText.'</span>';
			} else {
				$x = '<a href="javascript:;" onClick="Update_Like_Status(\''.$FunctionName.'\','.$RecordID.',1)">'.$Lang["DigitalArchive"]["Like"].'</a>';
				$x .= '<span class="like_num">'.$likeText.'</span>';
			}
			
			if ($iLike) {
				$text = $Lang["DigitalArchive"]["Unlike"];
				$action = 0;
				$you = $Lang["DigitalArchive"]["You"];
			} else {
				$text = $Lang["DigitalArchive"]["Like"];
				$action = 1;
				$you = "";
			}

			if ($delimiter == "") {

				$x .= '<span class="like_num" id="like_num_' . $RecordID . '">' . $you . $likeText . '</span>';
				$x .= '<span style="float: left; margin-left: 10px;"> <a href="javascript:;" style="height:19px;line-height:19px;" onClick="Update_Like_Status(\'' . $FunctionName . '\',' . $RecordID . ',' . $action . ')">' . $text . '</a></span>';

			} else {

				$x .= '<div class="like_status" >
									   <a href="javascript:void(0);"  onClick="Update_Like_Status(\'' . $FunctionName . '\',' . $RecordID . ',' . $action . ')">' . $text . '</a>
									   <span>&nbsp;' . $delimiter . '&nbsp;</span>
									   </div><span class="like_num" id="like_num_' . $RecordID . '">' . $you . $likeText . '</span>';
			}
			$x .= '
							<p class="spacer"></p>
							<p class="spacer"></p>			
						';

		}

		return $x;
	}*/
	
	function Get_Date_Diff($Date, $compareWith = "") {
		global $Lang;
		$OneDayTimestamp = 86400;

		if ($compareWith == "") {
			$compareWith = date('Y-m-d');
		}
		$Date = substr($Date, 0, 10);

		$dateDiff = intranet_time_diff($compareWith, $Date);

		if ($dateDiff == 0) {
			$returnValue = $Lang['StaffAttendance']['Today'];
		} else
			if ($dateDiff <= 7) {
				$returnValue = $dateDiff . $Lang["DigitalArchive"]["DaysAgo"];
			} else {
				$returnValue = $Date;
			}

		return $returnValue;
	}

	function Get_My_Favourite_Resource_Record_Table($YearID = "", $limitAmount = "") {
		global $Lang, $UserID, $i_no_record_exists_msg;
		$lda = new libdigitalarchive();

		$data = $lda->Get_My_Favourite_Resource_Record($limitAmount);
		$x = '
					<div class="reading_board_top_left"><div class="reading_board_top_right">
					<h1><span>' . $Lang["DigitalArchive"]["MyLike"] . '</span></h1>
					</div></div>
					   
					<div class="reading_board_left"><div class="reading_board_right">
					';
		if (sizeof($data) == 0) {
			$x .= '<div align="center"><br style="clear:both">' . $i_no_record_exists_msg . '</div>';
		} else {
			$x .= '<a href="myLikeMore.php" class="reading_more">' . $Lang["DigitalArchive"]["More"] . '..</a>
						<p class="spacer"></p>';
			$x .= '<ul>';
			for ($i = 0, $i_max = sizeof($data); $i < $i_max; $i++) {
				list ($id, $title, $url, $fileHashName, $sourceExtension) = $data[$i];

				if ($sourceExtension=="url") {
					$ext = "file_htm";
					$link = $url;
					$link_style = " target='_blank'";
					$span_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_MY_FAVOURITE.'\','.$id.',0,1);"';
					$link_onClick = '';
				}elseif($sourceExtension=="tvurl") {
				    $ext = "file_vdo";
				    $link = 'javascript:void(0);';
				    $link_style = '';
					$link_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_MY_FAVOURITE.'\','.$id.',\'download.php?FileHashName='.$fileHashName.'\',1);loadCampusTV(\''.$id.'\')"';
				    $span_onClick = '';
				}
				else {
					$ext = (isset ($this->FileExtension[$sourceExtension]) && $this->FileExtension[$sourceExtension] != "") ? "file_" . $this->FileExtension[$sourceExtension] : "file_unknown";
					//$link = 'download.php?FileHashName=' . $fileHashName;
					$link = 'javascript:void(0);';
					$link_style = "";
					$link_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_MY_FAVOURITE.'\','.$id.',\'download.php?FileHashName='.$fileHashName.'\',1);"';
					$span_onClick = '';
				}
				$x .= '<li><span'.$span_onClick.'><a href="'.$link.'" '.$link_style.' class="'.$ext.'"'.$link_onClick.'>'.$title.'</a></span> <a href="javascript:;" onClick="Update_Like_Status(\'' . FUNCTION_MY_FAVOURITE . '\',' . $id . ',0);" class="unlike">' . $Lang["DigitalArchive"]["Unlike"] . '</a><p class="spacer"></p></li>';
				//$x .= '<li><span><a href="' . $link . '" ' . $link_style . ' class="' . $ext . '">' . $title . '</a></span> <a href="javascript:;" onClick="Update_Like_Status(\'' . FUNCTION_MY_FAVOURITE . '\',' . $id . ',0);" class="unlike">' . $Lang["DigitalArchive"]["Unlike"] . '</a><p class="spacer"></p></li>';
			}
			$x .= '</ul>';
		}
		$x .= '
					</div></div>
					<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
					<p class="spacer"></p>
				';
		return $x;
	}

	function Get_My_Subject_Table($AcademicYearID = "", $YearID = "") {
		global $Lang, $UserID, $i_no_record_exists_msg;

		include_once ("subject_class_mapping.php");
		include_once ("libdigitalarchive.php");
		$subject = new subject();
		$lda = new libdigitalarchive();

		if ($AcademicYearID == "")
			$AcademicYearID = Get_Current_Academic_Year_ID();

		if ($YearID == "") {
			$subjectList = $subject->Get_Subject_List($returnAssociAry = 0, $withLearningCategory = 0);
		} else {
			$subjectList = $subject->Get_Subject_By_Form($YearID);
		}

		$x = '
					<div class="reading_board_top_left"><div class="reading_board_top_right">
						<h1><span>' . $Lang["DigitalArchive"]["Subject"] . '</span></h1>
						<div class="thumb_list_tab">
								' . $this->Get_Form_Display($YearID, "Get_My_Subject_Table") . '
							</div>
					</div></div>
					<div class="reading_board_left"><div class="reading_board_right">';

		if ($YearID == "") {
			$classSelected = ' class="thumb_list_tab_on"';
		}

		/*
		$x .= '<div class="thumb_list_tab">
						'.$this->Get_Form_Display($YearID, "Get_My_Subject_Table").'
					</div>';
		*/
		$x .= '<br /><div id="elib_cata_body" style="margin-top:20px;">';

		for ($i = 0, $i_max = sizeof($subjectList); $i < $i_max; $i++) {
			$x .= '<p><a href="displayMore.php?flag=' . FUNCTION_MY_SUBJECT . '&SubjectID=' . $subjectList[$i]['RecordID'] . '">' . Get_Lang_Selection($subjectList[$i]['CH_DES'], $subjectList[$i]['EN_DES']) . '</a></p>';
		}
		if (sizeof($subjectList) == 0) {
			$x .= '<br style="clear:both" /><p align="center">' . $i_no_record_exists_msg . '</p>';
		}

		$x .= '			
						</div>
					        			
					</div></div>
					<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
					<p class="spacer"></p>
				';
		return $x;
	}

	function Get_Favourite_Resource_Record_Table($YearID = "", $limit = "") {
		global $Lang, $UserID, $i_no_record_exists_msg;

		$lda = new libdigitalarchive();

		$x = '
					<div class="reading_board_top_left"><div class="reading_board_top_right">
						<h1><span>' . $Lang["DigitalArchive"]["Favourite"] . '</span></h1>
						<div class="thumb_list_tab">
							';

		$x .= $this->Get_Form_Display($YearID, "Get_Resource_Favourite_Table");

		$x .= '
						</div>
						<a href="displayMore.php?flag=' . FUNCTION_FAVOURITE . '" class="reading_more" >' . $Lang["DigitalArchive"]["More"] . '..</a>
					</div></div>
				
					<div class="reading_board_left"><div class="reading_board_right">
					';
		if ($YearID != "") {
			$conds = " WHERE lvl.ClassLevelID='$YearID'";
		}
		if ($limit != "")
			$limitValue = " LIMIT $limit";

		$sql = "SELECT
							rec.SubjectResourceID,
							rec.Title,
							rec.Description,
							rec.DateInput,
							rec.Url,
							rec.FileName,
							rec.FileHashName,
							rec.SourceExtension,
							rec.LikeCount
						FROM 
							DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD rec 
							INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_AND_CLASSLEVEL_RELATION lvl ON (lvl.SubjectResourceID=rec.SubjectResourceID AND rec.RecordStatus=1) 
							LEFT JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD lk ON (lk.SubjectResourceID=lk.SubjectResourceID)
						$conds
						GROUP BY 
							rec.SubjectResourceID
						ORDER BY 
							LikeCount DESC, SubjectResourceID DESC
						$limitValue
						";

		$data = $lda->returnArray($sql);

		for ($i = 0, $i_max = sizeof($data); $i < $i_max; $i++) {
			list ($recordID, $title, $description, $dateInput, $url, $fileName, $fileHashName, $sourceExtension, $likeCount) = $data[$i];

			if ($sourceExtension=="url") {
				$ext = "file_htm";
				$link = $url;
				$link_style = ' target="_blank"';
				$img = '<a href="' . $url . '" ' . $link_style . ' class="' . $ext . '">&nbsp;</a>';
				$img = '<span onClick="js_Update_HitRate(\''.FUNCTION_FAVOURITE.'\','.$recordID.',0,1);">'.$img.'</span>';
				$h2_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_FAVOURITE.'\','.$recordID.',0,1);"';
				$link_onClick = '';
			} elseif($sourceExtension=="tvurl"){
				$ext = "file_vdo";
				$link = 'javascript:void(0);';
				$link_style = '';
				$link_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_FAVOURITE.'\','.$recordID.',\'download.php?FileHashName='.$fileHashName.'\',1);loadCampusTV(\''.$recordID.'\')"';
				$img = '<a href="'.$link.'" class="'.$ext.'"'.$link_onClick.'>&nbsp;</a>';
				$h2_onClick = '';
			}
			else {
				$ext = (isset ($this->FileExtension[$sourceExtension]) && $this->FileExtension[$sourceExtension] != "") ? "file_" . $this->FileExtension[$sourceExtension] : "file_unknown";
				//$link = 'download.php?FileHashName=' . $fileHashName;
				$link = 'javascript:void(0);';
				$link_style = '';
				$link_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_FAVOURITE.'\','.$recordID.',\'download.php?FileHashName='.$fileHashName.'\',1);"';
				$img = '<a href="'.$link.'" class="'.$ext.'"'.$link_onClick.'>&nbsp;</a>';
				$h2_onClick = '';
			}
			
			//$description = substr($description, 0,60);

			$description = chopString($description, $this->DiaplayDescriptionLengthLimit, '...');
			$x .= '
							<div class="new_item">
								<div class="popular_item_top_left"><div class="popular_item_top_right"></div></div>
									<div class="popular_item_left"><div class="popular_item_right">
										<div class="reading_recommend_book_list">
											<div class="recommend_book_cover">' . $img . '</div>
												<div class="recommend_book_detail">
													<h2'.$h2_onClick.'><a href="'.$link.'" '.$link_style.$link_onClick.'>' . $title . ' <span class="date_time">' . $this->Get_Date_Diff($dateInput, $compareWith = "") . '</span>
													<br />
													<span>' . nl2br($description) . '</span>
													</a></h2>
													<div class="tag_list">' . $lda->Display_Tag_By_Resource_Record_ID($recordID, $returnFlag = 'text') . '</div>
											</div>
										<p class="spacer"></p>
													<div class="like_comment Div_'.FUNCTION_FAVOURITE.'_'.$recordID.'" id="Div_'.FUNCTION_FAVOURITE.'_'.$recordID.'">
							';
			$x .= $this->Get_Like_Content(FUNCTION_FAVOURITE, $recordID, '', 1);
			$x .= '
													</div>
													<p class="spacer"></p>
													<p class="spacer"></p>
												</div> 
										 
										</div>
									</div>
								<div class="popular_item_top_left"><div class="popular_item_bottom_right"></div></div>
							</div>
						';

		}
		if (sizeof($data) == 0) {
			$x .= '
							<div class="new_item">
								<div class="new_item_top_left"><div class="new_item_top_right"></div></div>
									<div class="new_item_left"><div class="new_item_right">
										<div class="reading_recommend_book_list">
										' . $i_no_record_exists_msg . '
										<p class="spacer"></p>
										 
										</div>
									</div></div>
								<div class="new_item_bottom_left"><div class="new_item_bottom_right"></div></div>
							</div>
						';

		}
		$x .= '
					</div></div>
					<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
				';
		return $x;
	}

	function Get_Directory_List_Table($FolderID = "", $GroupID = "") {
		global $Lang, $button_remove, $button_edit, $button_download, $i_no_record_exists_msg;

		$lda = new libdigitalarchive();

		$directoryList = $lda->getDirectoryList($GroupID, $FolderID);

		$directoryContent = "";
		$folderCount = 0;
		$fileCount = 0;
		$countText = "";

		$editableRecordIdByGroupID = $lda->Get_Editable_Record_ID_By_GroupID($GroupID, $FolderID);

		# table content [Start]
		for ($i = 0, $i_max = sizeof($directoryList); $i < $i_max; $i++) {
			list ($docID, $title, $fileFolderName, $hashName, $fileExtension, $description, $versionNo, $isFolder, $dateInput, $inputBy, $sizeInBytes, $thread, $dateModifed) = $directoryList[$i];

			$tagDisplay = "";
			$delim = "";
			$tagInDoc = $lda->Get_Tag_By_Admin_Docuemnt_ID($docID);
			for ($j = 0, $j_max = sizeof($tagInDoc); $j < $j_max; $j++) {
				list ($tagid, $tagname) = $tagInDoc[$j];
				$tagDisplay .= $delim . '<a href="displayTag.php?tagid=' . $tagid . '">' . $tagname . '</a>';
				$delim = ", ";
			}
			if ($tagDisplay == "")
				$tagDisplay = "-";

			if ($isFolder) {
				$tr_css = ' class="row_waiting"';
				$file_css = ' class="doc_folder"';
				$link = "javascript:;";
				$onClick = ' onClick="document.form1.FolderID.value=' . $docID . ';Get_Directory_List_Table();"' . $docID;
				$sizeDisplay = "-";
				$tag = "-";
				$versionDisplay = "-";
				$folderCount++;
			} else {
				$tr_css = "";
				if (isset ($this->FileExtension[$fileExtension]) && $this->FileExtension[$fileExtension] != "") {
					$file_css = ' class="doc_file file_' . $this->FileExtension[$fileExtension] . '"';
				} else {
					$file_css = ' class="doc_file file_unknown"';
				}
				$link = "download.php?fileHashName=$hashName";
				$onClick = "";

				$sizeDisplay = file_size($sizeInBytes);
				$tag = "-";
				$versionDisplay = '<a href="javascript:newWindow(\'file_version.php?Thread=' . $thread . '\',10)">' . $versionNo . '</a>';
				$fileCount++;
			}
			if ($description == "")
				$description = "-";

			$directoryContent .= '
							<tr ' . $tr_css . '>
								<td><a href="' . $link . '" ' . $file_css . ' ' . $onClick . '>' . $title . '</a></td>
								<td>' . ($isFolder == 0 ? $fileFolderName : "-") . '</td>
								<td>' . nl2br($description) . '</td>
								<td>' . $versionDisplay . '</td>
								<td>' . $sizeDisplay . '</td>
								<td>' . $tagDisplay . '</td>
								<td>' . $inputBy . '</td>
								<td>' . ($dateModifed == "0000-00-00 00:00:00" ? $dateInput : $dateModifed) . '</td>
								<td><input type="checkbox" name="DocumentID[]" id="DocumentID[]" value="' . $docID . '"/></td>
							</tr>';
		}
		if (sizeof($directoryList) == 0) {
			$directoryContent = '<tr><td colspan="8" align="center" height="40">' . $i_no_record_exists_msg . '</td></tr>';
		}
		# table content [End]

		//$folderHierarchy = $this->Get_Folder_Hierarchy($GroupID);
		$array = $lda->Get_Folder_Hierarchy_Array($GroupID);

		$folderHierarchySwap = $this->Display_Folder_Hierarchy_For_Swap($array, 0, $FolderID);
		$folderHierarchyMoveTo = $this->Display_Folder_Hierarchy_For_MoveTo($array);
		$folderHierarchyMoveTo2 = $this->Display_Folder_Hierarchy_For_Swap($array, 0, $FolderID, $NoReload = 0, $MoveTo = 1);

		if ($folderCount != 0) {
			$countText .= $folderCount . $Lang["DigitalArchive"]["SubFolders2"];
		}
		if ($fileCount != 0) {
			if ($countText != "")
				$countText .= ", ";
			$countText .= $fileCount . $Lang["DigitalArchive"]["Files"];
		}
		if ($countText != "")
			$countText = $Lang["DigitalArchive"]["SubFolders1"] . $countText;

		# check whether can manage file
		$canManage = 0;
		$adminInGroup = $this->IsAdminInGroup;
		if ($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
			$canManage = 1;
		} else
			if ($adminInGroup[$GroupID]) {
				if ($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP" . $GroupID . "-ADMIN_OWN-MANAGE") || $lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP" . $GroupID . "-ADMIN_OTHERS-MANAGE")) {
					$canManage = 1;
				}
			} else {
				if ($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP" . $GroupID . "-MEMBER_OWN-MANAGE") || $lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP" . $GroupID . "-MEMBER_OTHERS-MANAGE")) {
					$canManage = 1;
				}
			}

		$CurrentFolderInfo = $lda->Get_Admin_Doc_Info($FolderID);
		$currentDirectoryName = (sizeof($CurrentFolderInfo) == 0) ? "/" : $CurrentFolderInfo['Title'];

		$x .= '<script language="javascript">';
		$x .= 'var editableAry = new Array();';

		for ($i = 0; $i < sizeof($editableRecordIdByGroupID); $i++) {

			$x .= 'editableAry[' . $i . '] = "' . $editableRecordIdByGroupID[$i] . '"' . "\n";
			error_log('editableAry[' . $i . '] = "' . $editableRecordIdByGroupID[$i] . '"' . "\n", 3, "/tmp/aaa.txt");
		}
		$x .= '</script>';

		$x .= '
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="2">
								<div class="navigation_v30">
									' . ($this->Get_Folder_Navigation($FolderID, "", $IsListTable = 1)) . '
								</div>
							</td>
						</tr>
						<tr>
							<td valign="bottom">
		
								<div class="table_filter">
									<div class="selectbox_group selectbox_group_filter">
										<a href="javascript:;" onClick="checkSpanDisplay(\'status_option\');document.getElementById(\'status_option2_value\').value = 0;document.getElementById(\'status_option2_value\').style.visibility=\'hidden\';">' . $currentDirectoryName . '</a>
									</div>
									<p class="spacer"></p>
									<div id="status_option" class="selectbox_layer select_folder_tree">
										' . $folderHierarchySwap . '
									</div>
								</div>
							</td>
							<td valign="bottom">
							<div class="common_table_tool">			
							';

		if ($canManage) {
			/*
			$x .= '
					<div class="table_filter">
							<div class="selectbox_group_v30 selectbox_group_filter">
								<a href="javascript:;" onClick="checkSpanDisplay(\'status_option2\')">'.$currentDirectoryName.'</a>
							</div>
							<p class="spacer"></p>
							<div id="status_option2" class="selectbox_layer select_folder_tree">
								'.$folderHierarchyMoveTo2.'
							</div>
					</div>
				';		
			*/
			$x .= '
							<div class="common_table_tool_movefolder">
								<div class="selectbox_group selectbox_group_filter" style="height:20px; line-height:20px;">
									<a href="javascript:;" onClick="checkSpanDisplay(\'status_option2\')">-- ' . $Lang['DigitalArchive']['MoveTo'] . ' --</a>
								</div>
								<div id="status_option2" class="selectbox_layer select_folder_tree">
									' . $folderHierarchyMoveTo2 . '                                      
									<p class="spacer"></p>
								</div>
							</div>
						';

			$x .= '			
									
									<!--
										<span class="selection_box">
											<select name="TargetFolderID" id="TargetFolderID" onChange="if(this.value!=\'\') {Move_To_Action(this.value);}">
												<option selected="selected">-- ' . $Lang['DigitalArchive']['MoveTo'] . ' --</option>
												' . $folderHierarchyMoveTo . '
											</select>
										</span>
									-->
			
										<span>|</span>
										
								';
		}
		$x .= '
									<a href="javascript:checkDownload()" class="tool_set" title="' . $Lang['Button']['Download'] . '">' . $Lang['Button']['Download'] . '</a>';
		if ($canManage) {
			$x .= '				
										<a href="javascript:checkFileExtract()" class="tool_undo" title="' . $Lang['Button']['Extract'] . '">' . $Lang['Button']['Extract'] . '</a>  
										<a href="javascript:;" onClick="Display_Edit();" class="tool_edit setting_row" title="' . $button_edit . '">' . $button_edit . '</a>
									
										<a href="javascript:removeAction()" class="tool_delete" title="' . $button_remove . '">' . $button_remove . '</a> ';
		}
		$x .= '				
								
								</div>
								<a href="#TB_inline?height=500&width=750&inlineId=FakeLayer;" onClick="Display_Edit_Layer();" class="setting_row thickbox" title="' . $button_edit . '" id="editLink2">&nbsp;</a>
							</td>
						</tr>
					</table>
					
					<table class="common_table_list">
						<thead>
							<tr>
								<th>' . $Lang["DigitalArchive"]["Folder"] . '/' . $Lang["DigitalArchive"]["File"] . '</th>
								<th>' . $Lang["DigitalArchive"]["FileName"] . '</th>
								<th>' . $Lang["DigitalArchive"]["Description"] . '</th>
								<th>' . $Lang["DigitalArchive"]["Version"] . '</th>
								<th>' . $Lang["DigitalArchive"]["Size"] . '</th>
								<th>' . $Lang["DigitalArchive"]["Tag"] . '</th>
								<th>' . $Lang["DigitalArchive"]["CreatedBy"] . '</th>
								<th>' . $Lang['General']['LastModified'] . '</th>
								<th class="num_check" ><input type="checkbox" name="checkbox3" id="checkbox3" onClick="Set_Checkbox_Value(\'DocumentID[]\',this.checked)"/></th>
							</tr>
						</thead>
						<tbody>
						' . $directoryContent . '
						</tbody>
					</table>
					<input type="hidden" name="xx" id="xx" value="xx">
					<p class="spacer"></p>
					<div class="edit_bottom"> 
						<span> ' . $countText . '</span>
						<p class="spacer"></p>
					</div>';

		return $x;
	}

	function Get_Folder_Navigation($FolderID = "", $delimitor = "", $IsListTable = "", $withGroupName = 1, $GroupID = "") {
		global $Lang, $GroupID, $FirstCall;

		$lda = new libdigitalarchive();
		//added that support to display the group name of the file path [Start]
		if ($withGroupName && !$IsListTable) {
			$FirstCall = 1;
			$recInfo = $lda->Get_Admin_Doc_Info($FolderID);
			$groupInfo1 = $lda->getGroupInfo($recInfo['GroupID']);
			$groupTitle = $groupInfo1['GroupTitle'];
		}
		//added that support to display the group name of the file path [End]
		if ($FolderID == 0) {

			if ($withGroupName) {
				$groupInfo = $lda->getGroupInfo($GroupID);
			}
			$group_name = $groupInfo["GroupTitle"];
			if ($delimitor == "") {
				//return "<a href='javascript:;' onClick='document.form1.FolderID.value=0;Get_Directory_List_Table();'>".$groupInfo['GroupTitle']." [".($IsListTable ? $Lang['DigitalArchive']['Root'] : ".")."]</a>";
				//".$_SERVER['PATH_INFO']."?GroupID=".$GroupID."&FolderID=".$FolderID."&folderTreeSelect=".$_REQUEST['folderTreeSelect']."
				return "<a href='javascript:file_folder_onclick(\"".$GroupID."\",\"".$FolderID."\");' onClick='document.form1.FolderID.value=0;Get_Directory_List_Table();'><em class='folder_group'>" . $groupInfo['GroupTitle'] . " [" . ($IsListTable ? $Lang['DigitalArchive']['Root'] : ".") . "]</em></a>";
			} else {
				return ($IsListTable ? $delimitor . $Lang['DigitalArchive']['Root'] : "");
			}
		}

		$sql = "SELECT DocumentID, ParentFolderID, Title, GroupID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID='$FolderID' AND IsFolder=1";
		$result = $lda->returnArray($sql, 4);

		//if($result[0]['ParentFolderID']!=0) {

		$list = $this->Get_Folder_Navigation($result[0]['ParentFolderID'], $delimitor, $IsListTable);
		//}
		if ($result[0]['Title'] != "") {
			if ($delimitor == "") {
				//$endLink .= "<a href='javascript:;' onClick='document.form1.FolderID.value=".$result[0]['DocumentID'].";Get_Directory_List_Table();'>".$result[0]['Title']."</a>";
				$endLink .= "<a href='javascript:file_folder_onclick(\"".$GroupID."\",\"".$FolderID."\");' onClick='document.form1.FolderID.value=" . $result[0]['DocumentID'] . ";Get_Directory_List_Table();'><em class='folder_close'>" . $result[0]['Title'] . "</em></a>";
				//$endLink .= "<a href='javascript:file_folder_onclick(\'".$GroupID."\', \'".$result[0]['DocumentID']."\');' onClick='document.form1.FolderID.value=" . $result[0]['DocumentID'] . ";Get_Directory_List_Table();'><em class='folder_close'>" . $result[0]['Title'] . "</em></a>";
			} else {
				$endLink .= $delimitor . $result[0]['Title'];
			}

		}
		//added that support to display the group name of the file path [Start]
		if ($FirstCall) {
			$FirstCall = 0;
			$link = $groupTitle . $list . $endLink;
		} else
			//added that support to display the group name of the file path [End]
			$link = $list . $endLink;
		/*
		if($withGroupName) {
			$recInfo = $lda->Get_Admin_Doc_Info($FolderID);
			$groupInfo = $lda->getGroupInfo($recInfo['GroupID']);
			$link = $groupInfo['GroupTitle'].$link;				
		}
		*/

		return $link;

	}

	function Get_My_Group() {
		global $image_path, $LAYOUT_SKIN, $UserID, $Lang, $_SESSION;

		$lda = new libdigitalarchive();

		//$data = $lda->GetMyGroupList();

		$data = $lda->GetMyGroup();

		$x = '<p class="spacer"></p>';

		$today = date('Y-m-d');

		for ($i = 0, $i_max = sizeof($data); $i < $i_max; $i++) {

			list ($id, $title, $docTotal, $lastUpdateDoc) = $data[$i];

			$newDoc = $lda->getNewDocTotalSinceLastLogin($id);

			$memberInGroup = $lda->memberInMgmtGroup($id);

			if ($lda->IsAdminInGroup[$id]) {
				//$leaderIcon = '<a class="group_leader"></a>';
				$css = ' class="group_leader" title="' . $Lang['DigitalArchive']['Admin'] . '"';
			} else {
				//$leaderIcon = '';	
				$css = '';
			}

			$x .= '
							<div class="group_item">
								<div class="group_item_top_left"><div class="group_item_top_right"></div></div>
			
			
								<div class="group_item_left">
									<div class="group_item_right">
										
										<a href="list.php?GroupID=' . $id . '" ' . $css . '><h1>
										<span>' . $title . '</span></h1><em></em></a><p class="spacer"></p>
										<div class="group_item_detail">
											<a href="list.php?GroupID=' . $id . '" title="' . $Lang['DigitalArchive']['NoOfDoc'] . '"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/reading_scheme/icon_file.gif" align="absmiddle" border="0" id="doc_icon_' . $id . '" />' . $docTotal . '  </a>';

			if ($newDoc > 0) {
				$x .= '					
												<a href="displayResult.php?GroupID=' . $id . '&advanceSearchFlag=1&byDateRange=1&StartDate=' . $_SESSION['DigitalArchive']['LastGroupViewTime'][$id] . '&EndDate=' . date('Y-m-d H:i:s') . '" title="' . $Lang['DigitalArchive']['NoOfNewDoc'] . '"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/reading_scheme/icon_file_new.gif" align="absmiddle" border="0" />' . $newDoc . '  </a>';
			} else {
				$x .= '<a title="' . $Lang['DigitalArchive']['NoOfNewDoc'] . '"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/reading_scheme/icon_file_new_dimmed.gif" align="absmiddle" border="0" /><span class="tabletextremark">' . $newDoc . '</span></a>';

			}

			if (sizeof($memberInGroup) == 0) {
				$x .= '<a title="' . $Lang['DigitalArchive']['MemberList'] . '"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/reading_scheme/icon_member.gif" align="absmiddle" border="0" /><span class="tabletextremark">' . sizeof($memberInGroup) . '</span></a>';
			} else {
				$x .= ' 
												<a href="javascript:show_membere_list(\'member_list_click\', \'' . $id . '\')" id="group_item_' . $id . '" title="' . $Lang['DigitalArchive']['MemberList'] . '"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/reading_scheme/icon_member.gif" align="absmiddle" border="0" />' . sizeof($memberInGroup) . '  </a> <span id=\"member_list_click\" style=\"border:1px border\">&nbsp;</span> ';
			}
			$x .= '					
										</div> 
										
										<p class="spacer"></p>
									</div>
								</div>
			
			
								<div class="group_item_bottom_left"><div class="group_item_bottom_right"></div></div>
							</div>
						';
			if ($i % 3 == 2) {
				$x .= '<p class="spacer"></p><br />';
			}
		}

		return $x;
	}

	function Get_Access_Right_Group_Table($keyword = "", $CategoryID = 0) {
		global $Lang, $i_no_record_exists_msg;

		$lda = new libdigitalarchive();

		$data = $lda->getAllAccessRightGroup($keyword, $CategoryID);

		if (sizeof($data) == 0)
			$initialSetting = '<a title="' . $Lang['DigitalArchive']['InitializeFromGroup'] . '" class="setting_row" href="import.php" /></a>';

		$x = '
					<table class="common_table_list">
					<thead>
					<tr>
					<th class="num_check">#</th>
					<th width="10%">' . $Lang['DigitalArchive']['GroupCode'] . '</th>
					<th width="25%">' . $Lang['DigitalArchive']['GroupName'] . '</th>
					<th width="30%">' . $Lang['DigitalArchive']['Description'] . '</th>
					<th width="10%">' . $Lang['DigitalArchive']['NoOfMembers'] . '</th>
					<th width="20%">' . $Lang['DigitalArchive']['Admin'] . '</th>
					<th width="5%"><div class="table_row_tool row_content_tool">' . $initialSetting . '<a title="' . $Lang['DigitalArchive']['NewGroup'] . '" class="add" href="access_right_detail.php" /></a></div></th>
					</tr>';
		for ($i = 0, $i_max = sizeof($data); $i < $i_max; $i++) {
			$GroupObj = $data[$i];

			$id = $GroupObj['GroupID'];
			$title = $GroupObj['GroupTitle'];
			$description = $GroupObj['Description'];
			$total = $GroupObj['totalMember'];
			$adminName = $GroupObj['AdminName'];
			$grp_code = $GroupObj["Code"];
			if ($description == "")
				$description = "-";
			if ($adminName == "")
				$adminName = "-";

			$x .= '
							<tr>
								<td>' . ($i +1) . '</td>
								<td>' . $grp_code . '</a></td>
								<td><a href="access_right_detail.php?GroupID=' . $id . '">' . $title . '</a></td>
								<td>' . nl2br($description) . '</td>
								<td><a href="access_right_user.php?GroupID=' . $id . '">' . $total . '</a></td>
								<td>' . $adminName . '</td>
								<td>
									<div class="table_row_tool">
										<a href="access_right_detail.php?GroupID=' . $id . '" class="edit_dim" title="' . $Lang['Button']['Edit'] . '"></a>
										<a href="javascript:;" onClick="goRemove(' . $id . ')" class="delete_dim" title="' . $Lang['Button']['Delete'] . '"></a>
									</div>
								</td>
							</tr>';
		}
		if (sizeof($data) == 0) {
			$x .= '<tr><td colspan="6">' . $i_no_record_exists_msg . '</td></tr>';
		}

		$x .= '
						</tbody>
						</table>
					';
		return $x;
	}

	function Get_Usage_Summary_Table() {
		global $Lang, $i_no_record_exists_msg;

		$lda = new libdigitalarchive();

		$UsageArr = $lda->getUsageData();

		$GroupArr = $lda->getAllAccessRightGroup();

		# combine data
		for ($i = 0; $i < sizeof($UsageArr['Files']); $i++) {
			$GroupFileObj = $UsageArr['Files'][$i];
			$GroupUsageArr[$GroupFileObj['GroupID']] = $GroupFileObj;
		}

		for ($i = 0; $i < sizeof($UsageArr['Folders']); $i++) {
			$GroupFolderObj = $UsageArr['Folders'][$i];
			$GroupUsageArr[$GroupFolderObj['GroupID']]['GroupID'] = $GroupFolderObj['GroupID'];
			$GroupUsageArr[$GroupFolderObj['GroupID']]['NoOfFolder'] = $GroupFolderObj['NoOfFolder'];
		}

		for ($i = 0; $i < sizeof($GroupArr); $i++) {
			$GroupObj = $GroupArr[$i];
			$GroupUsageArr[$GroupObj['GroupID']]['GroupID'] = $GroupObj['GroupID'];
			$GroupUsageArr[$GroupObj['GroupID']]['GroupTitle'] = $GroupObj['GroupTitle'];
			$GroupUsageArr[$GroupObj['GroupID']]['Description'] = $GroupObj['Description'];
			$GroupUsageArr[$GroupObj['GroupID']]['TotalMember'] = $GroupObj['totalMember'];
			$GroupUsageArr[$GroupObj['GroupID']]['GroupCode'] = $GroupObj['Code'];
			$GroupUsageArr[$GroupObj['GroupID']]['AdminName'] = $GroupObj['AdminName'];
		}

		# list from big

		$x = '
					<table class="common_table_list">
					<thead>
					<tr>
					<th class="num_check">#</th>
					<th width="10%">' . $Lang['DigitalArchive']['GroupCode'] . '</th>
					<th width="25%">' . $Lang['DigitalArchive']['GroupName'] . '</th>
					<th width="15%">' . $Lang['DigitalArchive']['NoOfMembers'] . '</th>
					<th width="15%">' . $Lang['DigitalArchive']['NoOfFolders'] . '</th>
					<th width="15%">' . $Lang['DigitalArchive']['NoOfFiles'] . '</th>
					<th width="20%">' . $Lang['DigitalArchive']['SpaceUsed'] . '</th>
					</tr>';
		if (is_array($GroupUsageArr) && sizeof($GroupUsageArr) > 0) {
			$counter = 0;
			$TotalUsageByte = 0;
			$TotalNoOfFolder = 0;
			$TotalNoOfFile = 0;
			foreach ($GroupUsageArr AS $GroupID => $GroupObj) {
				if ($GroupObj['GroupTitle'] != "") {
					$counter++;
					$TotalUsageByte += $GroupObj['FileSizeByte'];
					$TotalNoOfFolder += $GroupObj['NoOfFolder'];
					$TotalNoOfFile += $GroupObj['NoOfFile'];
					$x .= '
											<tr>
												<td>' . $counter . '</td>
												<td>' . $GroupObj['GroupCode'] . '</td>
												<td>' . $GroupObj['GroupTitle'] . '</td>
												<td>' . $GroupObj['TotalMember'] . '</td>
												<td>' . (($GroupObj['NoOfFolder'] != "") ? number_format($GroupObj['NoOfFolder']) : "0") . '</td>
												<td>' . (($GroupObj['NoOfFile'] != "") ? number_format($GroupObj['NoOfFile']) : "0") . '</td>
												<td>' . file_size($GroupObj['FileSizeByte']) . '</td>
											</tr>';
				}
			}
			$x .= '
								<tr>
									<td colspan="3">&nbsp;</td>
									<td align="right">' . $Lang['DigitalArchive']['TotalAmount'] . ':</td>
									<td>' . (($TotalNoOfFolder != "") ? number_format($TotalNoOfFolder) : "0") . '</td>
									<td>' . (($TotalNoOfFile != "") ? number_format($TotalNoOfFile) : "0") . '</td>
									<td>' . file_size($TotalUsageByte) . '</td>
								</tr>';
		}
		if (sizeof($GroupUsageArr) == 0) {
			$x .= '<tr><td colspan="7">' . $i_no_record_exists_msg . '</td></tr>';
		}

		$x .= '
						</tbody>
						</table>
					';
		return $x;
	}

	function Get_Access_Right_Group_Category_Table($keyword = "") {
		global $Lang, $i_no_record_exists_msg;

		$lda = new libdigitalarchive();

		$data = $lda->getAllAccessRightGroupCategory($keyword);

		$x = '
					<table class="common_table_list">
					<thead>
					<tr>
					<th width="5%">' . $Lang['DigitalArchive']['DisplayOrder'] . '</th>
					<th width="20%">' . $Lang['DigitalArchive']['GroupCategoryCode'] . '</th>
					<th width="40%">' . $Lang['DigitalArchive']['GroupCategoryName'] . '</th>
					<th width="20%">' . $Lang['DigitalArchive']['GroupTotal'] . '</th>
					<th width="10%"><div class="table_row_tool row_content_tool"><a title="' . $Lang['DigitalArchive']['NewGroupCategory'] . '" class="add" href="access_right_category_detail.php" /></a></div></th>
					</tr>';
		for ($i = 0, $i_max = sizeof($data); $i < $i_max; $i++) {
			$CategoryObj = $data[$i];

			$x .= '
							<tr>
								<td>' . $CategoryObj['DisplayOrder'] . '</td>
								<td>' . $CategoryObj['CategoryCode'] . '</td>
								<td><a href="access_right_category_detail.php?GroupCategoryID=' . $CategoryObj['CategoryID'] . '">' . $CategoryObj['CategoryTitle'] . '</a></td>
								<td><a href="access_right.php?CategoryID=' . $CategoryObj['CategoryID'] . '">' . $CategoryObj['TotalGroups'] . '</a></td>
								<td>
									<div class="table_row_tool">
										<a href="access_right_category_detail.php?GroupCategoryID=' . $CategoryObj['CategoryID'] . '" class="edit_dim" title="' . $Lang['Button']['Edit'] . '"></a>
										<a href="javascript:;" onClick="goRemove(' . $CategoryObj['CategoryID'] . ')" class="delete_dim" title="' . $Lang['Button']['Delete'] . '"></a>
									</div>
								</td>
							</tr>';
		}
		if (sizeof($data) == 0) {
			$x .= '<tr><td colspan="5">' . $i_no_record_exists_msg . '</td></tr>';
		}

		$x .= '
						</tbody>
						</table>
					';
		return $x;
	}

	function Get_Group_Category_Edit_Form($GroupCategoryID = 0) {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;

		$x .= $this->Include_JS_CSS();

		if ($GroupCategoryID > 0) {
			$lda = new libdigitalarchive();
			$CategoryObj = $lda->Get_Category_Info($GroupCategoryID);
			$GroupTitle = $CategoryObj['CategoryTitle'];
		} else {
			$GroupTitle = $Lang['DigitalArchive']['NewGroupCategory'];
		}

		$x .= '<form id="form1" name="form1" method="post" action="group_category_update.php"><div id="ReportDiv">';
		$x .= '<br>
					  <div class="navigation_v30 navigation_2">
							
							<span id="GroupTitleNavLayer">' . htmlspecialchars($GroupTitle, ENT_QUOTES) . '</span>
							
					  	<input type="hidden" id="GroupCategoryID" name="GroupCategoryID" value="' . $GroupCategoryID . '" />
					  </div>
				<br style="clear:both" />
				<p class="spacer"></p><div class="table_board">
							<table class="form_table">
								<tbody>
									<tr>
										<td class="field_title">' . $Lang['DigitalArchive']['GroupCategoryCode'] . ' <span class="tabletextrequire">*</span></td>
										<td><input name="CategoryCode" type="text" id="CategoryCode" class="textboxnum" value="' . htmlspecialchars($CategoryObj['CategoryCode'], ENT_QUOTES) . '" maxlength="16" /></td>
									</tr>
									<tr>
										<td class="field_title">' . $Lang['DigitalArchive']['GroupCategoryName'] . ' <span class="tabletextrequire">*</span></td>
										<td><input name="CategoryTitle" type="text" id="CategoryTitle" class="textbox_name" value="' . htmlspecialchars($CategoryObj['CategoryTitle'], ENT_QUOTES) . '" /></td>
									</tr>
									<tr>
										<td class="field_title">' . $Lang['DigitalArchive']['DisplayOrder'] . ' <span class="tabletextrequire">*</span></td>
										<td><input name="DisplayOrder" type="text" id="DisplayOrder" class="textboxnum" value="' . htmlspecialchars($CategoryObj['DisplayOrder'], ENT_QUOTES) . '" maxlength="3" /></td>
									</tr>
								    </tbody>
									<col class="field_title"/>
								    <col class="field_c"/>
								</table>
								<p class="spacer"/>
							</div>' . "\n";

		$x .= '<div class="tabletextremark">&nbsp; ' . $Lang['General']['RequiredField'] . '</div><div class="edit_bottom_v30" style="width:95%;">
								<p class="spacer"/>
									<input type="button" value="' . $Lang['Btn']['Submit'] . '" onmouseout="this.className=\'formbutton\'" onmouseover="this.className=\'formbuttonon\'" onclick="CheckForm();" class="formbutton" name="submit2"/>
									<input type="button" value="' . $Lang['Btn']['Cancel'] . '" onmouseout="this.className=\'formbutton\'" onmouseover="this.className=\'formbuttonon\'" onclick="window.location=\'group_category.php\'" class="formbutton" name="submit3"/>
								<p class="spacer"/>
							</div>';

		$x .= '<div id="ReportLayer">';

		$x .= '</div>';
		$x .= '</div></form>';

		//$libclassdiary->Get_Class_Diary_Student_Lessons('1711',$start_date,$end_date);

		return $x;
	}

	function Get_Category_Filter($GroupCategoryID) {
		global $PATH_WRT_ROOT, $Lang;

		$lda = new libdigitalarchive();
		$AllCategory = $lda->getAllAccessRightGroupCategory();
		$CategorySelection = "<select name='CategoryID' id='CategoryID' onChange=\"this.form.submit();\" >\n";
		$CategorySelection .= "<option value=\"\">--- " . $Lang['DigitalArchive']['AllCategories'] . " ---</option>\n";
		for ($i = 0; $i < sizeof($AllCategory); $i++) {
			$ID = $AllCategory[$i]['CategoryID'];
			$Name = $AllCategory[$i]['CategoryTitle'];
			$SelectedStr = ($GroupCategoryID == $ID) ? "SELECTED" : "";
			$CategorySelection .= "<option value=\"{$ID}\" {$SelectedStr}>{$Name}</option>\n";
		}
		$CategorySelection .= "</select>\n";

		return $CategorySelection;
	}

	function Get_Group_Access_Right_Settings_Index($GroupID) {
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $Lang, $intranet_session_language, $linterface, $iDiscipline, $plugin;

		$lda = new libdigitalarchive();

		if ($GroupID != null && trim($GroupID) != '' && $GroupID > 0) {
			$ResultArr = $lda->Get_Group_Info($GroupID);

			$GroupTitle = $ResultArr[0]['GroupTitle'];
			$GroupTitleEdit = $GroupTitle;
			$GroupDescription = $ResultArr[0]['Description'];
			$GroupCode = $ResultArr[0]['Code'];
			$name = $lda->Get_Name_By_ID($ResultArr[0]['ModifiedBy']);

			if ($ResultArr[0]['ModifiedBy'] != "")
				$LastUpdate = ($intranet_session_language == "en") ? " " . $iDiscipline['By'] . " " . $name : " ($name)";

			$LastUpdateDate = $ResultArr[0]['DateModified'] . $LastUpdate;

			$GroupCategoryID = $lda->GetGroupCategoryID($GroupID);
			$GroupCategoryUpdateJS = "UpdateCategoryLinkage()";
		} else {
			$GroupTitle = $Lang["DigitalArchive"]["New"];
			$GroupDescription = "";
			$GroupCode = "";
			$LastUpdateDate = "";
		}

		$AllCategory = $lda->getAllAccessRightGroupCategory();
		$CategorySelection = "<select name='CategoryID' id='CategoryID' onChange=\"document.GroupRightForm.GroupCategoryID.value=this.value;{$GroupCategoryUpdateJS}\" >\n";
		$CategorySelection .= "<option value=\"\">--- " . $Lang['Btn']['Select'] . " ---</option>\n";
		for ($i = 0; $i < sizeof($AllCategory); $i++) {
			$ID = $AllCategory[$i]['CategoryID'];
			$Name = $AllCategory[$i]['CategoryTitle'];
			$SelectedStr = ($GroupCategoryID == $ID) ? "SELECTED" : "";
			$CategorySelection .= "<option value=\"{$ID}\" {$SelectedStr}>{$Name}</option>\n";
		}
		$CategorySelection .= "</select>\n";

		$RightList = $lda->retrieveAccessRight($GroupID);

		$Right = array ();
		for ($i = 0; $i < sizeof($RightList); $i++) {
			$Right[$RightList[$i][0]][$RightList[$i][1]][$RightList[$i][2]] = 1;
		}

		$x = $this->Include_JS_CSS();
		$x .= '
					<br>
					  <div class="navigation_v30 navigation_2">
							
							<!--<a href="index.php">' . $Lang['DigitalArchive']['SchoolAdminDoc'] . '</a>
							
							<a href="javascript:goBack();">' . $Lang['Menu']['DigitalArchive']['Settings']['AccessRight'] . '</a>-->
							
							<span id="GroupTitleNavLayer">' . htmlspecialchars($GroupTitle, ENT_QUOTES) . '</span>
							
					  	<input type="hidden" id="GroupID" name="GroupID" value="' . $GroupID . '" />
					  </div>
				<br style="clear:both" />
				<p class="spacer"></p>';

		$x .= '<div class="detail_title_box">
							<span style="width:100%">
								<span style="width:10%;"><span class="tabletextrequire">* </span>' . $Lang['AccountMgmt']['Settings']['GroupName'] . ': </span>
								<strong>
									<span style="text-align:left;" id="EditableGroupTitle" onmouseover=Show_Edit_Icon("GroupTitleEditIcon") onmouseout=Hide_Edit_Icon("GroupTitleEditIcon") class="jEditInput">' . htmlspecialchars($GroupTitleEdit, ENT_QUOTES) . '</span><span class="table_row_tool"><a id="GroupTitleEditIcon" class="edit_dim" href="javascript:jsClickEdit();"></a></span>
								</strong>
							</span>
							<div id="GroupTitleWarningLayer" style="display:none; color:red;" ></div>
		          			<br style="clear: both;" />
							<span style="width:100%">
				                <span style="width:10%"><span class="tabletextrequire">* </span>' . $Lang['DigitalArchive']['GroupCode'] . ': </span>
								<strong>
									<span style="text-align:left;" id="EditableGroupCode" onmouseover=Show_Edit_Icon("GroupCodeEditIcon") onmouseout=Hide_Edit_Icon("GroupCodeEditIcon") class="jEditInput3">' . htmlspecialchars($GroupCode, ENT_QUOTES) . '</span><span class="table_row_tool"><a id="GroupCodeEditIcon" class="edit_dim" href="javascript:jsClickEdit3();"></a></span>
								</strong>
							</span>
							<div id="GroupCodeWarningLayer" style="display:none; color:red;" ></div>
		          			<br style="clear: both;" />
							<span style="width:100%">
				                <span style="width:10%">' . $Lang['DigitalArchive']['GroupCategory'] . ': </span>
								<strong>
									' . $CategorySelection . '
								</strong>
							</span>
							<div id="GroupCategoryWarningLayer" style="display:none; color:red;" ></div>
							<br style="clear: both;" />
							<span style="width:100%">
				                <span style="width:10%">' . $Lang['AccountMgmt']['Settings']['Description'] . ': </span>
								<strong>
									<span style="text-align:left;" id="EditableGroupDescription" onmouseover=Show_Edit_Icon("GroupDescEditIcon") onmouseout=Hide_Edit_Icon("GroupDescEditIcon") class="jEditInput2">' . htmlspecialchars($GroupDescription, ENT_QUOTES) . '</span><span class="table_row_tool"><a id="GroupDescEditIcon" class="edit_dim" href="javascript:jsClickEdit2();"></a></span>
								</strong>
							</span>
							<br style="clear: both;" />
		      		 </div>';

		$x .= '
						<br style="clear: both;"/>
				' . $this->PageAccessRightSubTagObj($GroupID, 1) . '
		        <br style="clear: both;"/>
					  <form name="GroupRightForm" id="GroupRightForm" method="POST" action="group_access_update.php">
					  <input type="hidden" id="GroupTitle" name="GroupTitle" value="' . htmlspecialchars($GroupTitle, ENT_QUOTES) . '" />
					  <input type="hidden" id="GroupDescription" name="GroupDescription" value="' . htmlspecialchars($GroupDescription, ENT_QUOTES) . '" />
					  <input type="hidden" id="GroupCode" name="GroupCode" value="' . htmlspecialchars($GroupCode, ENT_QUOTES) . '" />
					  <input type="hidden" id="GroupCategoryID" name="GroupCategoryID" value="' . htmlspecialchars($GroupCategoryID, ENT_QUOTES) . '" />
		        <div class="table_board">';

		// ADMIN

		$x .= '
						<table class="common_table_list rights_table_list">
							<tbody>
							<tr class="seperate_row">
								<th class="sub_row_top" colspan="2">
									<input type="checkbox" id="ADMIN" name="ADMIN" onclick="Check_Group(this)"/>
									<label for="ADMIN">' . $Lang['DigitalArchive']['Admin'] . ' <img src="' . $image_path . '/' . $LAYOUT_SKIN . '/reading_scheme/icon_leader.gif" border="0" width="20" height="20" align="bottom" /></label> 
								</th>
						 	</tr>
						  	<tr class="tabletop">
							    <th width="50%">' . $Lang['DigitalArchive']['OwnFiles'] . '</th>
								<th width="50%">' . $Lang['DigitalArchive']['OthersFiles'] . '</th>
						    </tr>
							<tr>
								<td class="ADMIN-Cell ' . (($Right['ADMIN']['OWN']['VIEW'] == 1) ? 'rights_selected' : 'rights_not_select') . '">
					     		<input class="ADMIN-Check" type="checkbox" ' . (($Right['ADMIN']['OWN']['VIEW'] == 1) ? 'checked' : '') . ' id="ADMIN_OWN_VIEW" value="ADMIN_OWN_VIEW" name="Right[]" onclick="Check_Single(this);"/>
					     		<label for="ADMIN_OWN_VIEW">' . $Lang['AccountMgmt']['Settings']['View'] . '</label>
					   			</td>
								<td class="ADMIN-Cell ' . (($Right['ADMIN']['OTHERS']['VIEW'] == 1) ? 'rights_selected' : 'rights_not_select') . '">
					     		<input class="ADMIN-Check" type="checkbox" ' . (($Right['ADMIN']['OTHERS']['VIEW'] == 1) ? 'checked' : '') . ' id="ADMIN_OTHERS_VIEW" value="ADMIN_OTHERS_VIEW" name="Right[]" onclick="Check_Single(this);"/>
					     		<label for="ADMIN_OTHERS_VIEW">' . $Lang['AccountMgmt']['Settings']['View'] . '</label>
					   			</td>
							</tr>
							<tr>
						    	<td class="ADMIN-Cell ' . (($Right['ADMIN']['OWN']['MANAGE'] == 1) ? 'rights_selected' : 'rights_not_select') . '">
						    	<input class="ADMIN-Check" type="checkbox" ' . (($Right['ADMIN']['OWN']['MANAGE'] == 1) ? 'checked' : '') . ' id="ADMIN_OWN_MANAGE" value="ADMIN_OWN_MANAGE" name="Right[]" onclick="Check_Single(this);" />
						      <label for="ADMIN_OWN_MANAGE">' . $Lang['AccountMgmt']['Settings']['Manage'] . '</label></td>
								<td class="ADMIN-Cell ' . (($Right['ADMIN']['OTHERS']['MANAGE'] == 1) ? 'rights_selected' : 'rights_not_select') . '">
						    	<input class="ADMIN-Check" type="checkbox" ' . (($Right['ADMIN']['OTHERS']['MANAGE'] == 1) ? 'checked' : '') . ' id="ADMIN_OTHERS_MANAGE" value="ADMIN_OTHERS_MANAGE" name="Right[]" onclick="Check_Single(this)" />
						      <label for="ADMIN_OTHERS_MANAGE">' . $Lang['AccountMgmt']['Settings']['Manage'] . '</label></td>
				
						    </tr>
						  	<tr>
						    	<td class="rights_not_select" colspan="2">&nbsp;</td>
						  	</tr>';

		// Group Members
		$x .= '
							<tr class="seperate_row">
								<th class="sub_row_top" colspan="2">
									<input type="checkbox" id="MEMBER" name="MEMBER" onclick="Check_Group(this)"/>
									<label for="MEMBER">' . $Lang['DigitalArchive']['GroupMembers'] . '</label> 
								</th>
						 	</tr>
						  	<tr class="tabletop">
							    <th>' . $Lang['DigitalArchive']['OwnFiles'] . '</th>
								<th>' . $Lang['DigitalArchive']['OthersFiles'] . '</th>
						    </tr>
							<tr>';

		$x .= '<td class="MEMBER-Cell ' . (($Right['MEMBER']['OWN']['VIEW'] == 1) ? 'rights_selected' : 'rights_not_select') . '">
					     		<input class="MEMBER-Check" type="checkbox" ' . (($Right['MEMBER']['OWN']['VIEW'] == 1) ? 'checked' : '') . ' id="MEMBER_OWN_VIEW" value="MEMBER_OWN_VIEW" name="Right[]" onclick="Check_Single(this);"/>
					     		<label for="MEMBER_OWN_VIEW">' . $Lang['AccountMgmt']['Settings']['View'] . '</label>
					   			</td>';
		/*
		$x .= '<td class="rights_selected">
		 		<input type="checkbox" checked="checked" id="MEMBER_OWN_VIEW" value="MEMBER_OWN_VIEW" name="Right[]" onclick="Check_Single(this);" disabled="disabled" />
		 		<input type="hidden" name="Right[]" value="MEMBER_OWN_VIEW" />
				<label for="MEMBER_OWN_VIEW">'.$Lang['AccountMgmt']['Settings']['View'].'</label>
					</td>';
			*/
		$x .= '<td class="MEMBER-Cell ' . (($Right['MEMBER']['OTHERS']['VIEW'] == 1) ? 'rights_selected' : 'rights_not_select') . '">
					     		<input class="MEMBER-Check" type="checkbox" ' . (($Right['MEMBER']['OTHERS']['VIEW'] == 1) ? 'checked' : '') . ' id="MEMBER_OTHERS_VIEW" value="MEMBER_OTHERS_VIEW" name="Right[]" onclick="Check_Single(this);"/>
					     		<label for="MEMBER_OTHERS_VIEW">' . $Lang['AccountMgmt']['Settings']['View'] . '</label>
					   			</td>
							</tr>
							<tr>';

		$x .= '<td class="MEMBER-Cell ' . (($Right['MEMBER']['OWN']['MANAGE'] == 1) ? 'rights_selected' : 'rights_not_select') . '">
						    	<input class="MEMBER-Check" type="checkbox" ' . (($Right['MEMBER']['OWN']['MANAGE'] == 1) ? 'checked' : '') . ' id="MEMBER_OWN_MANAGE" value="MEMBER_OWN_MANAGE" name="Right[]" onclick="Check_Single(this);Check_Single(document.GroupRightForm.MEMBER_OWN_VIEW);" />
						      <label for="MEMBER_OWN_MANAGE">' . $Lang['AccountMgmt']['Settings']['Manage'] . '</label></td>';
		/*
		$x .= '<td class="rights_selected">
		    	<input type="checkbox" checked="checked" id="MEMBER_OWN_MANAGE" value="MEMBER_OWN_MANAGE" name="Right[]" onclick="Check_Single(this);Check_Single(document.GroupRightForm.MEMBER_OWN_VIEW);" disabled="disabled" />
		        <input type="hidden" name="Right[]" value="MEMBER_OWN_MANAGE" />
				<label for="MEMBER_OWN_MANAGE">'.$Lang['AccountMgmt']['Settings']['Manage'].'</label></td>';
		*/
		$x .= '<td class="MEMBER-Cell ' . (($Right['MEMBER']['OTHERS']['MANAGE'] == 1) ? 'rights_selected' : 'rights_not_select') . '">
						    	<input class="MEMBER-Check" type="checkbox" ' . (($Right['MEMBER']['OTHERS']['MANAGE'] == 1) ? 'checked' : '') . ' id="MEMBER_OTHERS_MANAGE" value="MEMBER_OTHERS_MANAGE" name="Right[]" onclick="Check_Single(this)" />
						      <label for="MEMBER_OTHERS_MANAGE">' . $Lang['AccountMgmt']['Settings']['Manage'] . '</label></td>
						    </tr>
							</tbody>
						</table>';

		$x .= '
					<br/>
					<p class="spacer"/>
		      </div>
				
		      <div class="edit_bottom">';
		if ($LastUpdateDate != '')
			$x .= '<span>' . $Lang['StaffAttendance']['LastUpdateOn'] . $LastUpdateDate . '</span>';
		$x .= '<p class="spacer"/>';
		$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "document.getElementById('SaveBtn').focus();setTimeout('goSubmit()',500);", "SaveBtn") . "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goBack();");
		$x .= '<p class="spacer"/>
		      </div>
				</form>';

		return $x;
	}

	function Get_Group_Access_Right_Users_Index($GroupID, $xmsg = "") {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $linterface, $button_new, $button_import, $button_export;

		$lda = new libdigitalarchive();

		if ($GroupID != null && trim($GroupID) != '' && $GroupID > 0) {
			$ResultArr = $lda->Get_Group_Info($GroupID);
			$GroupTitle = $ResultArr[0]['GroupTitle'];
			$GroupDescription = $ResultArr[0]['Description'];
			$GroupCode = $ResultArr[0]['Code'];
			//$LastUpdateDate = $ResultArr[0]['DateModified'];

			$GroupCategoryID = $lda->GetGroupCategoryID($GroupID);
			$GroupCategoryUpdateJS = "UpdateCategoryLinkage()";
		} else {
			$GroupTitle = "";
			$GroupDescription = "";
			//$LastUpdateDate = "";
		}

		$AllCategory = $lda->getAllAccessRightGroupCategory();
		$CategorySelection = "<select name='CategoryID' id='CategoryID' onChange=\"document.form1.GroupCategoryID.value=this.value;{$GroupCategoryUpdateJS}\" >\n";
		$CategorySelection .= "<option value=\"\">--- " . $Lang['Btn']['Select'] . " ---</option>\n";
		for ($i = 0; $i < sizeof($AllCategory); $i++) {
			$ID = $AllCategory[$i]['CategoryID'];
			$Name = $AllCategory[$i]['CategoryTitle'];
			$SelectedStr = ($GroupCategoryID == $ID) ? "SELECTED" : "";
			$CategorySelection .= "<option value=\"{$ID}\" {$SelectedStr}>{$Name}</option>\n";
		}
		$CategorySelection .= "</select>\n";

		$x .= $this->Include_JS_CSS();
		//$x .= "<div align='right'>".$this->GET_SYS_MSG($xmsg)."</div>";
		$x .= '
					
					<br style="clear:both">
					   <div class="navigation_v30 navigation_2">
							
							<!--<a href="index.php">' . $Lang['DigitalArchive']['SchoolAdminDoc'] . '</a>
							
							<a href="javascript:goBack();">' . $Lang['DigitalArchive']['AccessRightSettings'] . '</a>-->
							
		          			<span id="GroupTitleNavLayer">' . htmlspecialchars($GroupTitle, ENT_QUOTES) . '</span><br />
					  	<input type="hidden" id="GroupID" name="GroupID" value="' . $GroupID . '" />
					  </div>
		
				<div class="detail_title_box">
							<span style="width:100%">
								<span style="width:10%;"><span class="tabletextrequire">* </span>' . $Lang['AccountMgmt']['Settings']['GroupName'] . ': </span>
								<strong>
									<span style="text-align:left;" id="EditableGroupTitle" onmouseover=Show_Edit_Icon("GroupTitleEditIcon") onmouseout=Hide_Edit_Icon("GroupTitleEditIcon") class="jEditInput">' . htmlspecialchars($GroupTitle, ENT_QUOTES) . '</span><span class="table_row_tool"><a id="GroupTitleEditIcon" class="edit_dim" href="javascript:jsClickEdit();"></a></span>
								</strong>
							</span>
							<div id="GroupTitleWarningLayer" style="display:none; color:red;" ></div>
		          			<br style="clear: both;" />
							<span style="width:100%">
				                <span style="width:10%"><span class="tabletextrequire">* </span>' . $Lang['DigitalArchive']['GroupCode'] . ': </span>
								<strong>
									<span style="text-align:left;" id="EditableGroupCode" onmouseover=Show_Edit_Icon("GroupCodeEditIcon") onmouseout=Hide_Edit_Icon("GroupCodeEditIcon") class="jEditInput3">' . htmlspecialchars($GroupCode, ENT_QUOTES) . '</span><span class="table_row_tool"><a id="GroupCodeEditIcon" class="edit_dim" href="javascript:jsClickEdit3();"></a></span>
								</strong>
							</span>
							<div id="GroupCodeWarningLayer" style="display:none; color:red;" ></div>
		          			<br style="clear: both;" />
							<span style="width:100%">
				                <span style="width:10%">' . $Lang['DigitalArchive']['GroupCategory'] . ': </span>
								<strong>
									' . $CategorySelection . '
								</strong>
							</span>
							<div id="GroupCategoryWarningLayer" style="display:none; color:red;" ></div>
							<br style="clear: both;" />
							<span style="width:100%">
				                <span style="width:10%">' . $Lang['AccountMgmt']['Settings']['Description'] . ': </span>
								<strong>
									<span style="text-align:left;" id="EditableGroupDescription" onmouseover=Show_Edit_Icon("GroupDescEditIcon") onmouseout=Hide_Edit_Icon("GroupDescEditIcon") class="jEditInput2">' . htmlspecialchars($GroupDescription, ENT_QUOTES) . '</span><span class="table_row_tool"><a id="GroupDescEditIcon" class="edit_dim" href="javascript:jsClickEdit2();"></a></span>
								</strong>
							</span>
							<br style="clear: both;" />
		      		 </div>
		              <br style="clear: both;"/>
		             ' . $this->PageAccessRightSubTagObj($GroupID, 2) . '
		              <br style="clear: both;"/>
		              <div class="content_top_tool">
		                ';

		$x .= '<div class="Conntent_tool">
						<a title="' . $Lang['StaffAttendance']['AddUser'] . '" href="access_right_user_add.php?GroupID=' . $GroupID . '" class="new">' . $Lang['StudentRegistry']['Add'] . '</a>
						<!--
						<a title="' . $button_import . '" href="group_access_users_import.php?GroupID=' . $GroupID . '" class="import">' . $button_import . '</a>
						<a title="' . $button_export . '" href="group_access_users_export.php?GroupID=' . $GroupID . '" class="export">' . $button_export . '</a>
						-->
					  </div>';

		$x .= '		
		                <div class="Conntent_search" id="SearchInputLayer">
		                    <input type="text" name="Keyword" id="Keyword" onkeyup="Check_Go_Search(event);" />
		                </div>
		                <br style="clear: both;"/>
		              </div>
				<form name="form1" id="form1" method="POST">
					  <input type="hidden" id="GroupID" name="GroupID" value="' . $GroupID . '" /> <input type="hidden" id="GroupCategoryID" name="GroupCategoryID" value="' . $GroupCategoryID . '" />';

		$x .= '<table width="100%" cellspacing="0" cellpadding="0" border="0">
						  <tbody>
							<tr>
						    <td valign="bottom"><div class="table_filter"/></td>
						    <td valign="bottom"><div class="common_table_tool"> 
								<a title="' . $Lang['DigitalArchive']['SetAsAdmin'] . '" class="tool_set" href="javascript:void(0);" onclick="Set_Role(1);" />' . $Lang['DigitalArchive']['SetAsAdmin'] . '</a> 
								<a title="' . $Lang['DigitalArchive']['SetAsMember'] . '" class="tool_set" href="javascript:void(0);" onclick="Set_Role(0);" />' . $Lang['DigitalArchive']['SetAsMember'] . '</a>
								<a title="' . $Lang['Btn']['Delete'] . '" class="tool_delete" href="javascript:void(0);" onclick="Delete_Member();" />' . $Lang['Btn']['Delete'] . '</a> 
							</div></td>
						  	</tr>
						  </tbody>
						</table>';
		$x .= '      <div class="table_board" id="UserListLayer">';
		$x .= $this->Get_Group_Access_Right_Users_List($GroupID);
		/*		    
		if($LastUpdateDate != '') {
			$x .= '<br><p class="spacer"/>';
			    $x .= '<div class="edit_bottom" style="width:100%;">';
			$x .= '<span>'.$Lang['StaffAttendance']['LastUpdateOn'].$LastUpdateDate.'</span>';
			$x .= '</div>';
		}
		*/
		$x .= '<p class="spacer"/>
				  	</div>
		            <br/>
		            <br/>
					</form>';

		return $x;
	}

	function Get_Group_Access_Right_Users_List($GroupID, $Keyword = "") {
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $Lang, $i_no_record_exists_msg;

		$lda = new libdigitalarchive();

		$UserList = $lda->Get_Group_Access_Right_Users($GroupID, $Keyword);

		$x .= '
						
						<table class="common_table_list">	
						  <thead>
						    <tr>
						      <th class="num_check" width="1">#</th>
						      <th width="60%">' . $Lang['StaffAttendance']['StaffName'] . '</th>
						      <th width="40%">' . $Lang['StaffAttendance']['UserType'] . '</th>
						      <th class="num_check" width="1"><input type="checkbox" id="checkall" name="checkall" onclick="Set_Checkbox_Value(\'StaffID[]\',this.checked)" /></th>
						      </tr>
						  </thead>
						  <tbody>';
		for ($i = 0; $i < sizeof($UserList); $i++) {
			if ($UserList[$i]['IsAdmin']) {
				$css = ' class="row_avaliable"';
				$icon = $Lang['DigitalArchive']['Admin'] . ' <img src="' . $image_path . '/' . $LAYOUT_SKIN . '/reading_scheme/icon_leader.gif" border="0" width="20" height="20" align="bottom" />';
			} else {
				$css = '';
				$icon = $Lang['DigitalArchive']['Member'];
			}

			$x .= '<tr ' . $css . '>
							      <td>' . ($i +1) . '</td>
							      <td>' . $UserList[$i]['UserName'] . '</td>
							      <td>' . $icon . '</td>
							      <td><input type="checkbox" id="StaffID[]" name="StaffID[]" value="' . $UserList[$i]['UserID'] . '" /></td>
							   </tr>';
		}
		if (sizeof($UserList) == 0) {
			$x .= '<tr><td colspan="4" height="40" align="center">' . $i_no_record_exists_msg . '</td></tr>';
		}

		$x .= '</tbody>
					  </table>';
		return $x;
	}

	function Get_Folder_Hierarchy($GroupID) {
		$lda = new libdigitalarchive();
		$array = $lda->Get_Folder_Hierarchy_Array($GroupID);

		$returnAry = $this->Display_Folder_Hierarchy_For_Swap($array);
		//debug_pr($returnAry);
		return $returnAry;
	}

	function Display_Folder_Hierarchy_For_Swap($array = array (), $levelCount = 0, $FolderID = "", $NoReload = "", $moveTo = "") {

		global $Lang;

		if ($moveTo == 1)
			$onClickAction = 'onClick="document.getElementById(\'TargetFolderID2\').value=0;Move_To_Action2(0);"';
		else
			$onClickAction = 'onClick="batchUpdate(this, 0)"';

		if ($levelCount == 0) {
			if ($FolderID == 0) {
				$returnText = '<a class="doc_folder folder_level_1" ' . $onClickAction . ' alt="/">/</a>' . "\n\r";
			} else {
				if ($NoReload) {

					$returnText = '<a href="javascript:;" onClick="batchUpdate(this, this.id)" class="doc_folder folder_level_1" alt="/" id="0">/</a>' . "\n\r";
				} else
					if ($moveTo) {

						$returnText = '<a href="javascript:;" onClick="document.getElementById(\'TargetFolderID2\').value=0;Move_To_Action2(0)" class="doc_folder folder_level_1" alt="/">/</a>' . "\n\r";
					} else {
						$returnText = '<a href="javascript:;" onClick="document.form1.FolderID.value=0;Get_Directory_List_Table();" class="doc_folder folder_level_1" alt="/">/</a>' . "\n\r";
					}
			}
		}

		if ($NoReload) {
			$moreAction = " onClick=\"batchUpdate(this, this.id);\"";
		} else
			if ($moveTo) {
				$moreAction = " onClick=\"document.getElementById('TargetFolderID2').value=this.id;Move_To_Action2(this.id)\"";
			}

		if (sizeof($array) > 0) {
			foreach ($array as $_id => $_title) {
				for ($i = 0, $i_max = sizeof($_title); $i < $i_max; $i++) {
					$nameAry = $_title[$i];
					$folder_css = ($_id == $FolderID) ? "doc_folder" : "doc_folder";

					if (!is_array($nameAry)) {
						if ($_id == $FolderID || $NoReload || $moveTo) {

							$returnText .= '<a class="' . $folder_css . ' folder_level_' . ($levelCount +2) . '"' . $moreAction . ' alt="' . $nameAry . '" id="' . $_id . '">' . $nameAry . '</a>' . "\n\r";
						} else {
							$returnText .= '<a href="javascript:;" onClick="document.getElementById(\'status_option_value\').value=0;document.form1.FolderID.value=' . $_id . ';Get_Directory_List_Table();" class="' . $folder_css . ' folder_level_' . ($levelCount +2) . '">' . $nameAry . '</a>' . "\n\r";
						}
					} else {
						$returnText .= $this->Display_Folder_Hierarchy_For_Swap($nameAry, $levelCount +1, $FolderID, $NoReload, $moveTo);
					}
				}
			}
		}

		return $returnText;
	}

	function Display_Folder_For_Swap($array = array (), $levelCount = 0, $FolderID = "", $NoReload = "", $moveTo = "", $GroupID = "", $selectedGroupID = "") {

		global $Lang;

		if ($levelCount == 0) {
			if ($FolderID == 0) {
				$returnText = '<a class="doc_folder folder_level_1" href="javascript:void(0);" onClick="' . (($GroupID == $selectedGroupID) ? "document.form1.GroupID.value=$GroupID;document.form1.FolderID.value=0;Get_Directory_List_Table();" : "self.location.href='fileList.php?GroupID=$GroupID&FolderID=0'") . '">/</a>' . "\n\r";
			} else {
				if ($NoReload) {

					$returnText = '<a href="javascript:void(0);" onClick="batchUpdate(this, this.id)" class="doc_folder folder_level_1" alt="/" id="0">/</a>' . "";
				} else
					if ($moveTo) {

						$returnText = '<a href="javascript:void(0);" onClick="' . (($GroupID == $selectedGroupID) ? "document.getElementById(\'TargetFolderID2\').value=0;Move_To_Action2(0)" : "self.location.href='fileList.php?GroupID=$GroupID&FolderID=0'") . '" class="doc_folder folder_level_1">/</a>' . "";
					} else {
						$returnText = '<a href="javascript:void(0);" onClick="' . (($GroupID == $selectedGroupID) ? "document.form1.GroupID.value='.$GroupID.';document.form1.FolderID.value=0;Get_Directory_List_Table();" : "self.location.href='fileList.php?GroupID=$GroupID&FolderID=0'") . '" class="doc_folder folder_level_1">/</a>' . "";
					}
			}
		}

		if ($NoReload) {
			$moreAction = " onClick=\"batchUpdate(this, this.id);\"";
		} else
			if ($moveTo) {
				$moreAction = " onClick=\"document.getElementById('TargetFolderID2').value=this.id;Move_To_Action2(this.id)\"";
			}

		if (sizeof($array) > 0) {
			foreach ($array as $_id => $_title) {
				for ($i = 0, $i_max = sizeof($_title); $i < $i_max; $i++) {
					$nameAry = $_title[$i];
					$folder_css = ($_id == $FolderID) ? "doc_folder" : "doc_folder";

					if (!is_array($nameAry)) {
						if ($_id == $FolderID || $NoReload || $moveTo) {

							$returnText .= '<a class="' . $folder_css . ' folder_level_' . ($levelCount +2) . '"' . $moreAction . ' alt="' . $nameAry . '" id="' . $_id . '">' . $nameAry . '</a>' . "";
						} else {
							$returnText .= '<a href="javascript:void(0);" onClick="' . (($GroupID == $selectedGroupID) ? "document.getElementById('status_option_value').value=0;document.form1.GroupID.value=" . $GroupID . ";document.form1.FolderID.value=" . $_id . ";Get_Directory_List_Table();" : "self.location.href='fileList.php?GroupID=$GroupID&FolderID=$_id'") . '" class="' . $folder_css . ' folder_level_' . ($levelCount +2) . '">' . $nameAry . '</a>' . "";
						}
					} else {
						$returnText .= $this->Display_Folder_For_Swap($nameAry, $levelCount +1, $FolderID, $NoReload, $moveTo, $GroupID, $selectedGroupID);
					}
				}
			}
		}

		return $returnText;
	}

	function Display_Folder_Hierarchy_For_MoveTo($array = array (), $levelCount = 0) {
		global $Lang;

		if ($levelCount == 0) {
			$returnText = '<option value="0">/</a>';
		}
		if (sizeof($array) > 0) {
			for ($i = 0; $i <= $levelCount; $i++) {
				$space .= "&nbsp;&nbsp;&nbsp;&nbsp;";
			}

			foreach ($array as $_id => $_title) {
				for ($i = 0, $i_max = sizeof($_title); $i < $i_max; $i++) {
					$nameAry = $_title[$i];
					if (!is_array($nameAry)) {
						$returnText .= '<option value="' . $_id . '">' . $space . $nameAry . '</option>';
					} else {
						$returnText .= $this->Display_Folder_Hierarchy_For_MoveTo($nameAry, $levelCount +1);
					}
				}
			}
		}

		return $returnText;
	}

	function Get_File_Version_List($Thread) {
		global $Lang, $i_no_record_exists_msg, $intranet_session_language;
		global $image_path, $LAYOUT_SKIN, $iDiscipline, $UserID, $_SESSION;

		$lda = new libdigitalarchive();

		$data = $lda->Get_File_List_With_All_Version($Thread);

		//$titleOfCurrentThread = $lda->Get_Title_Of_Current_Thread($data[0]['Thread']);

		//$returnContent = '<table width="100%"><tr><td><b>'.$Lang['DigitalArchive']['Title'].' : </b> '.$titleOfCurrentThread.'</td></tr></table><br style="clear:both" />';

		$returnContent .= '
					<table class="common_table_list">	
						<thead>
							<tr>
								<th width="15%">' . $Lang["DigitalArchive"]["Version"] . '</th>
								<th width="30%">' . $Lang["DigitalArchive"]["FileName"] . '</th>
								<!--<th width="40%">' . $Lang["DigitalArchive"]["Description"] . '</th>-->
								<th width="20%">' . $Lang["DigitalArchive"]["CreatedBy"] . '</th>
								<th width="20%">' . $Lang["DigitalArchive"]["UploadDate"] . '</th>
								<th width="10%">' . $Lang["DigitalArchive"]["Size"] . '</th>
								<th width="5%">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
								';

		$i_max = sizeof($data);
		for ($i = 0; $i < $i_max; $i++) {
			list ($docID, $title, $hashFileName, $fileExtension, $description, $versionNo, $sizeInBytes, $recordStatus, $dateModified, $groupID, $modifyBy, $fileName, $inputBy, $dateInput, $thread, $inputByID) = $data[$i];
			if ($recordStatus == 0) {

				$link = $title;

				if ($description != "") {
					$description .= "<br>";
				}
				if ($intranet_session_language == "en") {
					$description .= "<span class='tabletextremark'>" . $Lang['DigitalArchive']['FileIsDeleted_1'] . $modifyBy . $Lang['DigitalArchive']['FileIsDeleted_2'] . $dateModified . $Lang['DigitalArchive']['FileIsDeleted_3'] . "</span>";
				} else {
					$description .= "<span class='tabletextremark'>" . $Lang['DigitalArchive']['FileIsDeleted_1'] . $dateModified . $Lang['DigitalArchive']['FileIsDeleted_2'] . $modifyBy . $Lang['DigitalArchive']['FileIsDeleted_3'] . "</span>";
				}

			} else {

				$link = '<a href="download.php?fileHashName=' . $hashFileName . '" class="doc_file file_' . $this->FileExtension[$fileExtension] . '">' . $fileName . '</a>';

				if ($description == "") {
					$description = "-";
				}
			}

			$returnContent .= '<tr>';
			$returnContent .= '<td>' . $versionNo . '</td>';
			$returnContent .= '<td>' . $link;
			if ($description != "-") {
				$returnContent .= '<a href="javascript:;" onClick="js_Show_Version_Description(this, ' . $thread . ', ' . $versionNo . ')"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_remark.gif" width="20" height="20" align="absmiddle" title="' . $iDiscipline['Remarks'] . '" border="0"></a>';
			}
			$returnContent .= '</td>';
			//$returnContent .= '<td>'.$description.'</td>';
			$returnContent .= '<td>' . $inputBy . '</td>';
			$returnContent .= '<td>' . ($dateModified == "0000-00-00 00:00:00" ? $dateInput : $dateModified) . '</td>';
			$returnContent .= '<td>' . file_size($sizeInBytes) . '</td>';

			$canManage = 0;

			if ($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
				$canManage = 1;
			} else
				if ($inputByID == $UserID) { # own file
					if (($this->IsAdminInGroup[$groupID] && $lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP" . $groupID . "-ADMIN_OWN-MANAGE")) || (!$lda->IsAdminInGroup[$groupID] && $lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP" . $groupID . "-MEMBER_OWN-MANAGE"))) {
						$canManage = 1;
					}
				} else { # others file
					if (($this->IsAdminInGroup[$groupID] && $lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP" . $groupID . "-ADMIN_OTHERS-MANAGE")) || (!$lda->IsAdminInGroup[$groupID] && $lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP" . $groupID . "-MEMBER_OTHERS-MANAGE"))) {
						$canManage = 1;
					}
				}
			if ($canManage) {
				$returnContent .= '<td class="table_row_tool">' . (($recordStatus == 0) ? '&nbsp;' : '<a class="delete_dim" href="javascript:;" onClick="removeAction(' . $docID . ')"></a>') . '</td>';
			} else {
				$returnContent .= '<td class="table_row_tool">&nbsp;</td>';
			}
			$returnContent .= '</tr>';
		}
		if ($i_max == 0) {
			$returnContent .= '<td colspan="5">' . $i_no_record_exists_msg . '</td>';
		}
		$returnContent .= '
						</tbody>
					</table>
								';
		$returnContent .= '<input type="hidden" name="GroupID" id="GroupID" value="' . $groupID . '">';
		$returnContent .= '<input type="hidden" name="removeThisID[]" id="removeThisID[]" value="">';

		return $returnContent;
	}

	function Get_Resource_Record_By_Tag_ID($tagid) {
		global $Lang, $i_no_record_exists_msg;

		$lda = new libdigitalarchive();
		$data = $lda->getResourceInfoByTagID($tagid);
		/*
		$x = '
			<div class="reading_board_top_left">
				<div class="reading_board_top_right">
				</div>
			</div>
			<div class="reading_board_left">
				<div class="reading_board_right">';
		*/
		$x .= $this->Get_Resource_Record_Item_By_TagID($tagid);
		/*
		$x .= '
				</div>
			</div>
			<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
		
		';
		*/
		return $x;
	}

	function Get_Resource_Record_Item_By_TagID($tagid) {
		global $i_no_record_exists_msg, $Lang;

		$lda = new libdigitalarchive();

		$data = $lda->getResourceInfoByTagID($tagid);
		//$data = array();
		for ($i = 0, $i_max = sizeof($data); $i < $i_max; $i++) {
			list ($recordID, $title, $description, $dateInput, $url, $fileName, $fileHashName, $sourceExtension, $likeCount) = $data[$i];

			if ($sourceExtension=="url") {
				$ext = "file_htm";
				$link = $url;
				$link_style = ' target="_blank"';
				$span_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$recordID.',0,0);"';
				$link_onClick = '';
				//$img = '<a href="'.$url.'" '.$link_style.' class="'.$ext.'">&nbsp;</a>';
			} elseif($sourceExtension=="tvurl"){
		    	$ext = "file_vdo";
			    $link = 'javascript:void(0);';
				$link_style = '';
				$link_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$recordID.',\'download.php?FileHashName='.$fileHashName.'\',0);loadCampusTV(\''.$recordID.'\')"';
				$span_onClick = '';
			}
			else {
				$ext = (isset ($this->FileExtension[$sourceExtension]) && $this->FileExtension[$sourceExtension] != "") ? "file_" . $this->FileExtension[$sourceExtension] : "file_unknown";
				//$link = 'download.php?FileHashName=' . $fileHashName;
				$link = 'javascript:void(0);';
				$link_style = '';
				$link_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$recordID.',\'download.php?FileHashName='.$fileHashName.'\',0);"';
				$span_onClick = '';
				//$img = '<a href="'.$link.'" class="'.$ext.'">&nbsp;</a>';
			}

			//$description = substr($description, 0,60);
			$description = chopString($description, $this->DiaplayDescriptionLengthLimit, '...');

			/*
			$x .= '
				<div class="resource_detail">
					<div class="book_detail_cover">
						<a href="'.$link.'" '.$link_style.'><span class="'.$ext.'">&nbsp;</span></a>
					</div>
					<div class="book_detail">
						<div class="book_detail_info">
							<h1><a href="'.$link.'" '.$link_style.'>'.$title.'</a></h1>
							<p class="spacer"></p>
						</div>
						<h2 class="book_level"><em>'.$Lang['DigitalArchive']['Level'].': </em><span>'.$lda->Display_ClassLevel_By_Resource_Record_ID($recordID, $returnFlag='text').'</span></h2>
						<h2 class="book_cat"><em>'.$Lang["DigitalArchive"]["Subject"].': </em><span>'.$lda->Display_Subject_By_Resource_Record_ID($recordID, $returnFlag='text').'</span></h2>
						<p class="spacer"></p>
						<h2 class="book_desc"><em>'.$Lang["DigitalArchive"]["Description"].': </em><div style="height: 30px; overflow: visible; overflow-x: none; overflow-y: auto; position: relative; width: 100%;"><span>'.nl2br($description).'</span></div></h2>
						<p class="spacer"></p>
						<div class="like_comment" id="Div_'.FUNCTION_NEWEST.'_'.$recordID.'">
							
							';
			$x .= $this->Get_Like_Content(FUNCTION_NEWEST,$recordID);				
			$x .= '
						</div>
						<p class="spacer"></p> 
					</div>
					
				</div>
				<p class="spacer"></p>
				<br style="clear:both">
			';		
			*/
			$x .= '
							<div class="resource_detail">
								<div class="book_detail_cover">
									<a href="' . $link . '" ' . $link_style . ' '.$link_onClick.'><span class="' . $ext . '"'.$span_onClick.'>&nbsp;</span></a>
								</div>
								<div class="book_detail">
									<div class="book_detail_info">
										<h1'.$span_onClick.'><a href="' . $link . '" ' . $link_style . ' '.$link_onClick.'>' . $title . '</a></h1>
										<p class="spacer"></p>
									</div>
									<h2 class="book_level"><em>' . $Lang['DigitalArchive']['Level'] . ': </em><span>' . $lda->Display_ClassLevel_By_Resource_Record_ID($recordID, $returnFlag = 'text') . '</span></h2>
									<h2 class="book_cat"><em>' . $Lang["DigitalArchive"]["Subject"] . ': </em><span>' . $lda->Display_Subject_By_Resource_Record_ID($recordID, $returnFlag = 'text') . '</span></h2>
									<p class="spacer"></p>
									<h2 class="book_desc"><em>' . $Lang["DigitalArchive"]["Description"] . ': </em><span>' . (trim($description) != "" ? nl2br($description) : "--") . '</span></h2>
									<p class="spacer"></p>
								<div class="tag_list">' . $lda->Display_Tag_By_Resource_Record_ID($recordID, $returnFlag = 'text') . '</div>
									<p class="spacer"></p> 
								</div>
									<div class="like_comment Div_'.FUNCTION_NEWEST.'_'.$recordID.'" id="Div_'.FUNCTION_NEWEST.'_'.$recordID.'">
										
										';
			$x .= $this->Get_Like_Content(FUNCTION_NEWEST, $recordID);
			$x .= '
									</div>
							</div>
							<p class="spacer"></p>
							<br style="clear:both">
						';

		}

		if (sizeof($data) == 0) {
			$x .= '
							<div class="popular_item">
								<div class="reading_recommend_book_list">
								' . $i_no_record_exists_msg . '
								<p class="spacer"></p>
								</div>
							</div>
						';

		}
		return $x;
	}

	function Get_My_Favourite_Resource_More_Record() {
		global $Lang, $i_no_record_exists_msg;

		$lda = new libdigitalarchive();

		$data = $lda->Get_My_Favourite_Resource_Record();

		for ($i = 0, $i_max = sizeof($data); $i < $i_max; $i++) {
			list ($id, $title, $url, $fileHashName, $sourceExtension, $description) = $data[$i];

            if ($sourceExtension=="url"){
                $ext = "file_htm";
				$link = $url;
				$link_style = " target='_blank'";
				$link_onClick = '';
				$span_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$id.',0,0);"';
				//$img = '<a href="'.$url.'" '.$link_style.' class="'.$ext.'">&nbsp;</a>';
            }elseif($sourceExtension=="tvurl"){
                $ext = "file_vdo";
				//$link = '#TB_inline?height=450&width=750&inlineId=FakeLayer';
				$link = 'javascript:void(0);';
				$link_style = " title='".$title."' class='thickbox'";
				$link_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$id.',0,0);loadCampusTV(\''.$id.'\')"';
				//$span_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$id.',0,0);"';
				//$img = '<a href="'.$url.'" '.$link_style.' class="'.$ext.'">&nbsp;</a>';
            }else{
                $ext = (isset ($this->FileExtension[$sourceExtension]) && $this->FileExtension[$sourceExtension] != "") ? "file_" . $this->FileExtension[$sourceExtension] : "file_unknown";
		  	//	$link = 'download.php?FileHashName=' . $fileHashName;
				$link = 'javascript:void(0);';
				$link_style = "";
				$link_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$id.',\'download.php?FileHashName='.$fileHashName.'\',0);"';
				$span_onClick = '';
				//$img = '<a href="'.$link.'" class="'.$ext.'">&nbsp;</a>';
            }
//			if ($url != "") {
//				$ext = "file_htm";
//				$link = $url;
//				$link_style = " target='_blank'";
//				$link_onClick = '';
//				$span_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$id.',0,0);"';
//				//$img = '<a href="'.$url.'" '.$link_style.' class="'.$ext.'">&nbsp;</a>';
//			} else {
//				$ext = (isset ($this->FileExtension[$sourceExtension]) && $this->FileExtension[$sourceExtension] != "") ? "file_" . $this->FileExtension[$sourceExtension] : "file_unknown";
//			//	$link = 'download.php?FileHashName=' . $fileHashName;
//				$link = 'javascript:void(0);';
//				$link_style = "";
//				$link_onClick = ' onClick="js_Update_HitRate(\''.FUNCTION_NEWEST.'\','.$id.',\'download.php?FileHashName='.$fileHashName.'\',0);"';
//				$span_onClick = '';
//				//$img = '<a href="'.$link.'" class="'.$ext.'">&nbsp;</a>';
//			}

			$x .= '
							<div class="resource_detail">
								<div class="book_detail_cover">
									<a href="'.$link.'" ' . $link_style . ' '.$link_onClick.'><span class="' . $ext . '"'.$span_onClick.'>&nbsp;</span></a>
								</div>
								<div class="book_detail">
									<div class="book_detail_info">
										<h1'.$span_onClick.'><a href="'.$link.'" ' . $link_style . ' '.$link_onClick.'>' . $title . '</a></h1>
										<p class="spacer"></p>
									</div>
									<h2 class="book_level"><em>' . $Lang['DigitalArchive']['Level'] . ': </em><span>' . $lda->Display_ClassLevel_By_Resource_Record_ID($id, $returnFlag = 'text') . '</span></h2>
									<h2 class="book_cat"><em>' . $Lang["DigitalArchive"]["Subject"] . ': </em><span>' . $lda->Display_Subject_By_Resource_Record_ID($id, $returnFlag = 'text') . '</span></h2>
									<p class="spacer"></p>
									<h2 class="book_desc"><em>' . $Lang["DigitalArchive"]["Description"] . ': </em><span>' . (trim($description) != "" ? nl2br($description) : "--") . '</span></h2>
									<p class="spacer"></p>
									<div class="like_comment Div_'.FUNCTION_NEWEST.'_'.$id.'" id="Div_'.FUNCTION_NEWEST.'_'.$id.'">
										
										';
			$x .= $this->Get_Like_Content(FUNCTION_NEWEST, $id);
			$x .= '
									</div>
									<p class="spacer"></p> 
								</div>
								<div class="tag_list">' . $lda->Display_Tag_By_Resource_Record_ID($id, $returnFlag = 'text') . '</div>
							</div>
							<p class="spacer"></p>
							<br style="clear:both">
						';
		}
		if ($i_max == 0) {
			$x = '<div class="resource_detail" align="center"><p class="spacer"></p>' . $i_no_record_exists_msg . '</div>';
		}

		return $x;
	}

	function Get_Display_More_Table($field = "", $order = "", $pageNo = "", $flag, $conds = "") {
		$lda = new libdigitalarchive();

		switch ($flag) {
			case FUNCTION_NEWEST :
				$sql = $lda->Get_Newest_More_SQL($conds, $returnSqlOnly = 1);
				$data = $this->Display_Table_More($field, $order, $pageNo, $sql);
				break;
			case FUNCTION_FAVOURITE :
				$sql = $lda->Get_Favourite_More_SQL($conds, $returnSqlOnly = 1);
				$data = $this->Display_Table_More($field, $order, $pageNo, $sql);
				//$data = $this->Display_Favourite_More($field, $order, $pageNo, $sql);
				break;
			case FUNCTION_MY_SUBJECT :
				$sql = $lda->Get_My_Subject_More_SQL($conds, $returnSqlOnly = 1);
				$data = $this->Display_Table_More($field, $order, $pageNo, $sql);
				//$data = $this->Display_My_Subject_More($field, $order, $pageNo, $conds, $sql);
				break;
			case FUNCTION_TAG :
				$data = $this->Display_Tag_More($field, $order, $pageNo, $lda->Module);
				break;

			case FUNCTION_ADVANCE_SEARCH :
				$sql = $lda->Get_My_Subject_More_SQL($conds, $returnSqlOnly = 1);
				$data = $this->Display_Table_More($field, $order, $pageNo, $sql);
				break;
		}
		return $data;
	}

	function Display_Table_More($field = "", $order = "", $pageNo = "", $sql) {
		global $Lang, $_SESSION;

		include_once ("libdbtable.php");
		include_once ("libdbtable2007a.php");

		$li = new libdbtable2007($field, $order, $pageNo);
		$lda = new libdigitalarchive();

		//$sql = $lda->Get_Newest_More_SQL($conds="", $returnSqlOnly=1);

		$additionColumn = ($_SESSION['UserType'] == USERTYPE_STAFF) ? 2 : 1;

		$li->sql = $sql;
		$li->field_array = array (
			"Title",
			"Description",
			"Subject",
			"ClassLevel",
			"LikeCount",
			"DateInput"
		);
		$li->no_col = sizeof($li->field_array) + $additionColumn;
		$li->IsColOff = "DigitalArchive_Newest_More_Table";
		$li->column_array = array (
			0,
			0,
			0,
			0,
			0,
			0
		);
		$li->wrap_array = array (
			0,
			0,
			0,
			0,
			0,
			0
		);
		$li->count_mode = 1;
		$pos = 0;
		$li->column_list .= "<th class='num_check'>#</th>\n";
		$li->column_list .= "<th width='20%' >" . $li->column_IP25($pos++, $Lang["DigitalArchive"]["Title"]) . "</th>\n";
		$li->column_list .= "<th width='20%' >" . $li->column_IP25($pos++, $Lang['DigitalArchive']['Description']) . "</th>\n";
		$li->column_list .= "<th width='10%' >" . $li->column_IP25($pos++, $Lang["DigitalArchive"]["Subject"]) . "</th>\n";
		$li->column_list .= "<th width='20%' >" . $li->column_IP25($pos++, $Lang["DigitalArchive"]["Level"]) . "</th>\n";
		$li->column_list .= "<th width='20%' >" . $li->column_IP25($pos++, $Lang['DigitalArchive']['Like']) . "</th>\n";
		$li->column_list .= "<th width='10%' >" . $li->column_IP25($pos++, $Lang['DigitalArchive']['UploadDate']) . "</th>\n";
		if ($_SESSION['UserType'] == USERTYPE_STAFF) {
			$li->column_list .= "<th width='1'>" . $li->check("RecordID[]") . "</th>\n";
		}

		$x = $li->display();

		$x .= "
					<input type='hidden' name='page_size_change' id='page_size_change' value='' />
					<input type='hidden' name='pageNo' id='pageNo' value='" . $li->pageNo . "' />
					<input type='hidden' name='order' id='order' value='" . $li->order . "' />
					<input type='hidden' name='field' id='field' value='" . $li->field . "' />
					<input type='hidden' name='numPerPage' id='numPerPage' value='" . $li->page_size . "' />
				";
		return $x;

	}

	function Get_Display_AdvanceSearchResult_Table($field = "", $order = "", $pageNo = "", $flag, $conds = "", $GroupID, $tagIDs = -1) {
		global $Lang;

		include_once ("libdbtable.php");
		include_once ("libdbtable2007a.php");
		$lda = new libdigitalarchive();
		$li = new libdbtable2007($field, $order, $pageNo);

		if ($flag == FUNCTION_TAG) {

			$x = $this->Display_Tag_More($field, $order, $pageNo, $lda->AdminModule);

		} else {

			$sql = $lda->Get_Admin_Doc_AdvanceSearchResult_SQL($conds, $GroupID, $returnSqlOnly = 1, $tagIDs);

			$li->sql = $sql;
			$li->field_array = array (
				"Title",
				"FileFolderName",
				"Description",
				"VersionNo",
				"ParentFolderID",
				"SizeInBytes",
				"DocumentID",
				"InputBy",
				"DateModified"
			);
			$li->no_col = sizeof($li->field_array) + 2;
			$li->IsColOff = "Digital_Archive_Admin_Search_Result_Table";
			$li->column_array = array (
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0
			);
			$li->wrap_array = array (
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0
			);

			$pos = 0;
			$li->column_list .= "<th class='num_check'>#</th>\n";
			$li->column_list .= "<th width='15%' >" . $li->column_IP25($pos++, $Lang["DigitalArchive"]["File"]) . "</th>\n";
			$li->column_list .= "<th width='10%' >" . $li->column_IP25($pos++, $Lang["DigitalArchive"]["FileName"]) . "</th>\n";
			$li->column_list .= "<th width='10%' >" . $li->column_IP25($pos++, $Lang['DigitalArchive']['Description']) . "</th>\n";
			$li->column_list .= "<th width='5%' >" . $Lang["DigitalArchive"]["Version"] . "</th>\n";
			$pos++;
			$li->column_list .= "<th width='20%' >" . $Lang["DigitalArchive"]["Folder"] . "</th>\n";
			$pos++;
			$li->column_list .= "<th width='10%' >" . $li->column_IP25($pos++, $Lang['DigitalArchive']['Size']) . "</th>\n";
			$li->column_list .= "<th width='10%' >" . $Lang['DigitalArchive']['Tag'] . "</th>\n";
			$pos++;
			$li->column_list .= "<th width='10%' >" . $li->column_IP25($pos++, $Lang['DigitalArchive']['CreatedBy']) . "</th>\n";
			$li->column_list .= "<th width='10%' >" . $li->column_IP25($pos++, $Lang['General']['LastModified']) . "</th>\n";
			$li->column_list .= "<th width='1'>" . $li->check("DocumentID[]") . "</th>\n";

			$x = $li->display();

			$x .= "
							<input type='hidden' name='page_size_change' id='page_size_change' value='' />
							<input type='hidden' name='pageNo' id='pageNo' value='" . $li->pageNo . "' />
							<input type='hidden' name='order' id='order' value='" . $li->order . "' />
							<input type='hidden' name='field' id='field' value='" . $li->field . "' />
							<input type='hidden' name='numPerPage' id='numPerPage' value='" . $li->page_size . "' />
						";
		}

		return $x;

	}

	function Display_Tag_More($field = "", $order = "", $pageNo = "", $Module = "") {
		global $Lang, $_SESSION;
		//echo $field.'/'.$order;

		include_once ("libdbtable.php");
		include_once ("libdbtable2007a.php");

		$li = new libdbtable2007($field, $order, $pageNo);
		$lda = new libdigitalarchive();

		$additionColumn = ($_SESSION['UserType'] == USERTYPE_STAFF) ? 2 : 1;

		if ($Module == $this->Module) {
			$sql = $lda->Get_Tag_More_SQL($conds = "", $returnSqlOnly = 1, $Module);
		} else {
			$sql = $lda->returnTagRankingArray("", $Module, $returnSQL = 1);
		}

		$li->sql = $sql;
		$li->field_array = array (
			"Tag",
			"DateInput"
		);
		$li->no_col = sizeof($li->field_array) + $additionColumn;
		$li->IsColOff = "IP25_table";
		$li->column_array = array (
			0,
			0
		);
		$li->wrap_array = array (
			0,
			0
		);

		$pos = 0;
		$li->column_list .= "<th class='num_check'>#</th>\n";
		$li->column_list .= "<th width='80%' >" . $li->column_IP25($pos++, $Lang["DigitalArchive"]["Tag"]) . "</th>\n";
		$li->column_list .= "<th width='20%' >" . $li->column_IP25($pos++, $Lang['DigitalArchive']['UploadDate']) . "</th>\n";
		if ($_SESSION['UserType'] == USERTYPE_STAFF) {
			$li->column_list .= "<th width='1'>" . $li->check("TagID[]") . "</th>\n";
		}

		$x = $li->display();

		$x .= "
					<input type='hidden' name='page_size_change' id='page_size_change' value='' />
					<input type='hidden' name='pageNo' id='pageNo' value='$pageNo' />
					<input type='hidden' name='order' id='order' value='$order' />
					<input type='hidden' name='field' id='field' value='$field' />
					<input type='hidden' name='numPerPage' id='numPerPage' value='" . $li->page_size . "' />
				";

		return $x;

	}

	function Get_Admin_Document_By_Tag_ID($tagid) {
		global $Lang, $i_no_record_exists_msg;

		$lda = new libdigitalarchive();
		//$data = $lda->getAdminDocInfoByTagID($tagid);	

		$x .= $this->Get_Admin_Doc_Item_By_TagID($tagid);

		return $x;
	}

	function Get_Admin_Doc_Item_By_TagID($tagid) {
		global $i_no_record_exists_msg, $Lang;

		$lda = new libdigitalarchive();

		$data = $lda->getAdminDocInfoByTagID($tagid);

		for ($i = 0, $i_max = sizeof($data); $i < $i_max; $i++) {
			list ($recordID, $title, $description, $dateInput, $fileName, $fileHashName, $fileExtension, $groupID, $parentFolderID) = $data[$i];

			$groupInfo = $lda->getGroupInfo($groupID);
			$GroupName = $groupInfo['GroupTitle'];

			$ext = (isset ($this->FileExtension[$fileExtension]) && $this->FileExtension[$fileExtension] != "") ? "file_" . $this->FileExtension[$fileExtension] : "file_unknown";
			$link = 'download.php?fileHashName=' . $fileHashName;
			$link_style = '';

			//$description = substr($description, 0,60);
			$description = chopString($description, $this->DiaplayDescriptionLengthLimit, '...');

			$x .= '
							<div class="resource_detail">
								<div class="book_detail_cover">
									<a href="' . $link . '" ' . $link_style . '><span class="' . $ext . '">&nbsp;</span></a>
								</div>
								<div class="book_detail">
									<div class="book_detail_info">
										<h1><a href="' . $link . '" ' . $link_style . '>' . $title . '</a></h1>
										<p class="spacer"></p>
									</div>
									<span><em>' . $Lang['DigitalArchive']['GroupName'] . ' : </em><span><a href="list.php?GroupID=' . $groupID . '">' . $GroupName . '</a></span></span>
									
									<p class="spacer"></p>
									<span><em>' . $Lang["DigitalArchive"]["Description"] . ' : </em><span>' . nl2br($description) . '</span></span>
								
									<p class="spacer"></p> 
									<span><em>' . $Lang["DigitalArchive"]["Folder"] . ' : </em><span><a href="list.php?GroupID=' . $groupID . '&FolderID=' . $parentFolderID . '">' . $this->Get_Folder_Navigation($parentFolderID, "/") . '</a></span></span>
								
									<p class="spacer"></p> 
			
								</div>
								
							</div>
							<p class="spacer"></p>
							<br style="clear:both">
						';

		}

		if (sizeof($data) == 0) {
			$x .= '
							<div class="popular_item">
								<div class="reading_recommend_book_list" align="center">
								' . $i_no_record_exists_msg . '
								<p class="spacer"></p>
								 
								</div>
							</div>
						';

		}
		return $x;
	}

	function Get_Intranet_Group_Table($AcademicYearID = "") {
		global $Lang, $i_no_record_exists_msg, $button_submit, $button_cancel;

		include_once ("libgroup.php");
		$lg = new libgroup();
		$lda = new libdigitalarchive();
		if ($AcademicYearID == "")
			$AcademicYearID = Get_Current_Academic_Year_ID();

		$GroupCategoryList = $lg->returnAllCategory();

		$sql = $lda->getIntranetGroupByYearID($AcademicYearID);
		$result = $lda->returnArray($sql);

		$table = '
					<table width="90%">
						<tr>
							<td>
								<div id="GroupTableDiv">
									<table border="0" cellpadding="0" cellspacing="0" class="common_table_list">
									<thead>
										<tr>
											<th width="35%">' . $Lang['Group']['Category'] . '</td>
											<th width="35%">' . $Lang['Group']['Group'] . '</td>
											<th width="15%" style="text-align:center"><label for="CheckAllGroup">' . $Lang['Group']['Copy'] . '</label><input type="checkbox" value="411" id="CheckAllGroup" name=""  onclick="CheckAll(\'Group\');UpdateMemberCheckBox();" checked  /></th>
											<th width="15%" style="text-align:center"><label for="CheckAllMember">' . $Lang['Group']['CopyMember'] . '</label><input type="checkbox" value="411" id="CheckAllMember" name="" class="Member" onclick="CheckAll(\'Member\'); " checked  /></th>
										</tr>
									</thead>
									<tbody>
										';

		for ($i = 0, $i_max = sizeof($result); $i < $i_max; $i++) {
			list ($groupid, $group_b5Title, $group_enTitle, $catid, $catName) = $result[$i];
			$table .= '	<tr>
										<td>' . $GroupCategoryList[$catid] . '</td>
										<td>' . Get_Lang_Selection($group_b5Title, $group_enTitle) . '</td>
										<td style="text-align:center"><input type="checkbox" id="Group' . $groupid . '" name="CopyGroup[]" value="' . $groupid . '" class="Group" onclick="UpdateMemberCheckBox(' . $groupid . ')" checked></td>
										<td style="text-align:center"><input type="checkbox" id="Member' . $groupid . '" name="CopyGroupMember[]" value="' . $groupid . '" class="Member" checked></td>	
									</tr>';
		}

		if (sizeof($result) == 0) {
			$table .= '<tr><td colspan="4" align="center" height="40"> ' . $i_no_record_exists_msg . ' </td></tr>';
		}

		$table .= '
									</tbody>
									</table>
								</div>
							</td>
						</tr>
					</table>
				';

		if (sizeof($result) > 0) {
			$table .= '<div class="edit_bottom">';
			$table .= $this->GET_ACTION_BTN($button_submit, "submit") . "&nbsp;" . $this->GET_ACTION_BTN($button_cancel, "button", "goBack()");
			$table .= '</div>';
		}

		return $table;
	}

	function Get_Blue_Tab($selected = "") {
		global $Lang, $sys_custom;

		if ($sys_custom['digital_archive']['hide_groupmenu']) {
			$ajust_position = "style=\"margin-top:15px; width:350px;\"";
		}
		$tag = '<div class="DA_tab_A" ' . $ajust_position . '>
					<ul>
					<li class="' . ($selected == 3 ? "current_tab_a" : "") . '" id="tab_a3"><a href="javascript:void(0)" onClick="Display_Blue_Content(3)"><span>' . $Lang['DigitalArchive']['MyGroup'] . '</span></a></li>
					<li class="' . ($selected == 1 ? "current_tab_a" : "") . '" id="tab_a1"><a href="javascript:void(0)" onClick="Display_Blue_Content(1)"><span>' . $Lang['DigitalArchive']['Latest'] . '</span></a></li>
					<li class="' . ($selected == 2 ? "current_tab_a" : "") . '" id="tab_a2"><a href="javascript:void(0)" onClick="Display_Blue_Content(2)"><span>' . $Lang['DigitalArchive']['LastViewed'] . '</span></a></li>
					</ul>
					</div>';
		return $tag;
	}

	function Get_Green_Tab($selected = "") {
		global $Lang;

		$lda = new libdigitalarchive();
		$myGroup = $lda->GetMyGroup();

		$tag .= '	<ul style="float:left;">';

		$NoOfGroups = count($myGroup);

		for ($i = 0; $i < $NoOfGroups; $i++) {
			list ($groupId, $groupName) = $myGroup[$i];

			//$array = $lda->Get_Folder_Hierarchy_Array($groupId);
			//$folderHierarchySwap = $this->Display_Folder_For_Swap($array, 0, $FolderID=0, "", "", $groupId, $selected);

			$tag .= '
							<li class="' . ($selected == $groupId ? "current_tab_b" : "") . '" onmouseover="Load_Folder_Tree(\'tab_b_board_' . ($i +1) . '\');Get_Folder_Hierarchy(\'FolderTreeContentDiv_' . ($i +1) . '\',' . $groupId . ');DisplayGroupHierachy(\'green_tab_' . $groupId . '\', \'tab_b_board_' . ($i +1) . '\');" onmouseout="Hide_Folder_Tree(\'tab_b_board_' . ($i +1) . '\');">
								<a href="javascript:void(0);" onClick="Display_Green_Content(' . $groupId . ',0)"  id="green_tab_' . $groupId . '"><span id="start_group_' . ($i +1) . '">' . $groupName . '</span></a>
			
								<div class="tab_b_board" id="tab_b_board_' . ($i +1) . '">
									<div class="tab_b_board_top_left"><div class="tab_b_board_top_right"></div></div>
									<div class="tab_b_board_left"><div class="tab_b_board_right">
										<div class="select_folder_tree" id="FolderTreeContentDiv_' . ($i +1) . '" style="height:200px;width:320px;overflow:auto;">
											' . $folderHierarchySwap . '                                                
											<p class="spacer"></p>
										</div>
									</div></div><!--<table bgcolor="red" width="300" height="300 border="1"><tr><td>&nbsp;</td></tr></table>-->
									<div class="tab_b_board_bottom_left"><div class="tab_b_board_bottom_right"></div></div>
								</div>
								
								<p class="spacer"></p>
								<span id="end_group_' . ($i +1) . '"></span>
							</li>';
		}

		$tag .= '</ul>';

		return $tag;
	}

	//Henry Added
	function Get_My_Group_Select_Menu($ID_Name, $Selected = '', $OnChange = '', $OnFocus = '', $IsMultiple = 0) {
		global $Lang;

		if ($OnFocus != '')
			$onfocus = 'onfocus="' . $OnFocus . '"';

		if ($OnChange != '')
			$onchange = 'onchange="' . $OnChange . '"';

		if ($IsMultiple == 1)
			$multiple = 'multiple="1" size="10"';

		$lda = new libdigitalarchive();

		$GroupCategoryData = $lda->getAllCategoryAndGroup();
		//debug_r($GroupCategoryData);

		$data = $lda->GetMyGroupList();

		for ($i = 0; $i < sizeof($data); $i++) {
			$groupObj = $data[$i];
			$GroupArr[$groupObj['GroupID']] = array (
				'GroupID' => $groupObj['GroupID'],
				'GroupTitle' => $groupObj['GroupTitle'],
				'DocTotal' => $groupObj['docTotal'],
				'LastUpload' => date("Y-m-d H:i", strtotime($groupObj[3]))
			);
			$DisplayLogCategory[$groupObj['GroupID']] = 0;
		}
		$NoOfRecord = count($data);

		//$content .= '<p class="spacer"></p>';
		//debug_r($GroupArr);

		# determine if there is any group for current in each category
		$CategoryGroupCount = array ();

		$select = '';
		$select .= '<select name="' . $ID_Name . '" id="' . $ID_Name . '" class="formtextbox" ' . $onchange . ' ' . $onfocus . ' ' . $multiple . '>';
		$select .= '<option value="-1">N/A</option>';
		$TempCategoryObjID = '';

		$SelectedArr = explode(',', $Selected);

		for ($i = 0; $i < sizeof($GroupCategoryData); $i++) {
			$CategoryObj = $GroupCategoryData[$i];
			$GroupObj = $GroupArr[$CategoryObj['GroupID']];

			if (is_array($GroupObj)) {
				//get the selected group from the selected group array
				$select_tag = "";
				foreach ($SelectedArr as $aSelected) {
					if ($GroupObj['GroupID'] == $aSelected) {
						$select_tag = "selected";
					}
				}
				$DisplayLogCategory[$CategoryObj['GroupID']] = $CategoryObj['CategoryID'];

				if ($TempCategoryObjID == '' || $TempCategoryObjID != $CategoryObj['CategoryID']) {
					$select .= '<optgroup label="' . $CategoryObj['CategoryTitle'] . '">';
				}
				$select .= '<option value="' . $GroupObj['GroupID'] . '" ' . $select_tag . '>' . $GroupObj['GroupTitle'] . '</option>';
				if ($TempCategoryObjID != '' && $TempCategoryObjID != $CategoryObj['CategoryID']) {
					$select .= '</optgroup>';
				}
				$TempCategoryObjID = $CategoryObj['CategoryID'];
			}
		}

		# list unclassified groups
		if (is_array($DisplayLogCategory) && sizeof($DisplayLogCategory) > 0) {
			$GroupsLeft = array ();
			foreach ($DisplayLogCategory AS $GroupID => $CategoryID) {
				if ($CategoryID == 0) {
					$GroupsLeft[] = $GroupID;
				}
			}
			if (sizeof($GroupsLeft) > 0) {
				$ItemsInCategory = 0;

				# open for current new category
				$select .= '<optgroup label="(' . $Lang['DigitalArchive']['Unclassified'] . ')">';

				for ($i = 0; $i < sizeof($GroupsLeft); $i++) {
					$GroupObj = $GroupArr[$GroupsLeft[$i]];
					$select .= '<option value="' . $GroupObj['GroupID'] . '">' . $GroupObj['GroupTitle'] . '</option>';
				}

				$select .= '</optgroup>';
			}
		}

		$select .= '</select>';

		return $select;

	}
	//End Henry Added

	function Get_My_Group_Table() {
		global $Lang, $LAYOUT_SKIN;

		$lda = new libdigitalarchive();

		$GroupCategoryData = $lda->getAllCategoryAndGroup();
		//debug_r($GroupCategoryData);

		$data = $lda->GetMyGroupList();

		for ($i = 0; $i < sizeof($data); $i++) {
			$groupObj = $data[$i];
			$GroupArr[$groupObj['GroupID']] = array (
				'GroupID' => $groupObj['GroupID'],
				'GroupTitle' => $groupObj['GroupTitle'],
				'DocTotal' => $groupObj['docTotal'],
				'LastUpload' => date("Y-m-d H:i", strtotime($groupObj[3]))
			);
			$DisplayLogCategory[$groupObj['GroupID']] = 0;
		}
		$NoOfRecord = count($data);

		//$content .= '<p class="spacer"></p>';
		//debug_r($GroupArr);

		# determine if there is any group for current in each category
		$CategoryGroupCount = array ();
		for ($i = 0; $i < sizeof($GroupCategoryData); $i++) {
			$CategoryObj = $GroupCategoryData[$i];
			$GroupObj = $GroupArr[$CategoryObj['GroupID']];
			if (is_array($GroupObj)) {
				$CategoryGroupCount[$CategoryObj['CategoryID']]++;
			}
		}

		$LastCategoryID = 0;
		$ItemsInCategory = 0;
		for ($i = 0; $i < sizeof($GroupCategoryData); $i++) {
			$CategoryObj = $GroupCategoryData[$i];
			if ($CategoryGroupCount[$CategoryObj['CategoryID']] <= 0) {
				continue;
			}
			if ($LastCategoryID != $CategoryObj['CategoryID']) {

				if ($ItemsInCategory > 0) {
					# close last category
					$content .= '                        
					                                              
					                                             <p class="spacer"></p><br />
					                                  </div></div>
					                                  <div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
					                                   <p class="spacer"></p>
					                             </div><p class="spacer"></p><br />
										';
				}

				$ItemsInCategory = 0;

				# open for current new category
				$content .= '
									<div class="reading_board" id="resource_admin_doc_group">
				                                      <div class="reading_board_top_left"><div class="reading_board_top_right">
				                                                <h1><span>' . $CategoryObj['CategoryTitle'] . '</span></h1>
				                                              
				                                      </div></div>
				                                        
				                                      <div class="reading_board_left"><div class="reading_board_right">
				                                     ';
			}
			$GroupObj = $GroupArr[$CategoryObj['GroupID']];

			if (is_array($GroupObj)) {
				$DisplayLogCategory[$CategoryObj['GroupID']] = $CategoryObj['CategoryID'];
				$FileTotal = ($GroupObj['DocTotal'] != "" && $GroupObj['DocTotal'] > 0) ? '<img src="/images/'.$LAYOUT_SKIN.'/reading_scheme/icon_file.gif" align="absmiddle" />' . $GroupObj['DocTotal'] . ' ' . $Lang["DigitalArchive"]["Files"] : "&nbsp;";
				$content .= '								
					                                               <div class="group_item">
					                                                <div class="group_item_top_left"><div class="group_item_top_right"></div></div>
					                                                <div class="group_item_left">
					                                                  <div class="group_item_right">
					                                                  <h1><a href="fileList.php?GroupID=' . $GroupObj['GroupID'] . '&FolderID=0"><img src="/images/'.$LAYOUT_SKIN.'/reading_scheme/icon_member.gif" align="absmiddle" border="0" /><font color="#1338b2">' . $GroupObj['GroupTitle'] . '</font> <!--[<img src="/images/'.$LAYOUT_SKIN.'/reading_scheme/icon_file.gif" align="absmiddle" />' . $GroupObj['DocTotal'] . ']<img src="/images/'.$LAYOUT_SKIN.'/alert_new.gif" align="absmiddle" />--></a></h1><p class="spacer"></p>
					                                           ';

				if ($GroupObj['DocTotal'] != "" && $GroupObj['DocTotal'] > 0) {
					$GroupInfo = $FileTotal . ', ' . $Lang['DigitalArchive']['LastUpload'] . ' ' . $GroupObj['LastUpload'];
				} else {
					$GroupInfo = '<font color="grey">' . $Lang['DigitalArchive']['NoFileInside'] . "</font>";
				}
				$content .= '
				                                                <div class="group_item_detail">
				                                                &nbsp; &nbsp; ' . $GroupInfo . '
				                                                </div>';

				$content .= '
					           		                             		 <p class="spacer"></p>
					                                   		 			 
					                                                </div></div>
					                                         	    <div class="group_item_bottom_left"><div class="group_item_bottom_right"></div></div>
					                                           </div>';
				$ItemsInCategory++;
				# break for 3 items per row
				if ($ItemsInCategory % 3 == 0) {
					$content .= '<p class="spacer"></p><br />';
				}
			}
			$LastCategoryID = $CategoryObj['CategoryID'];

		}
		if ($ItemsInCategory > 0) {
			# close last category
			$content .= '                        
			                                      
			                                     <p class="spacer"></p><br />
			                          </div></div>
			                          <div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
			                           <p class="spacer"></p>
			                     </div><p class="spacer"></p><br />
						';
		}

		# list unclassified groups
		if (is_array($DisplayLogCategory) && sizeof($DisplayLogCategory) > 0) {
			$GroupsLeft = array ();
			foreach ($DisplayLogCategory AS $GroupID => $CategoryID) {
				if ($CategoryID == 0) {
					$GroupsLeft[] = $GroupID;
				}
			}
			if (sizeof($GroupsLeft) > 0) {
				$ItemsInCategory = 0;

				# open for current new category
				$content .= '
									<div class="reading_board" id="resource_admin_doc_group">
				                                      <div class="reading_board_top_left"><div class="reading_board_top_right">
				                                                <h1><span>(' . $Lang['DigitalArchive']['Unclassified'] . ')</span></h1>
				                                              
				                                      </div></div>
				                                        
				                                      <div class="reading_board_left"><div class="reading_board_right">
				                                     ';
				for ($i = 0; $i < sizeof($GroupsLeft); $i++) {
					$GroupObj = $GroupArr[$GroupsLeft[$i]];
					$FileTotal = ($GroupObj['DocTotal'] != "" && $GroupObj['DocTotal'] > 0) ? '<img src="/images/'.$LAYOUT_SKIN.'/reading_scheme/icon_file.gif" align="absmiddle" />' . $GroupObj['DocTotal'] . ' ' . $Lang["DigitalArchive"]["Files"] : "&nbsp;";
					$content .= '								
							                                               <div class="group_item">
							                                                <div class="group_item_top_left"><div class="group_item_top_right"></div></div>
							                                                <div class="group_item_left">
							                                                  <div class="group_item_right">
							                                                  <h1><a href="fileList.php?GroupID=' . $GroupObj['GroupID'] . '&FolderID=0"><img src="/images/'.$LAYOUT_SKIN.'/reading_scheme/icon_member.gif" align="absmiddle" border="0" /><font color="#1338b2">' . $GroupObj['GroupTitle'] . '</font> <!--[<img src="/images/'.$LAYOUT_SKIN.'/reading_scheme/icon_file.gif" align="absmiddle" />' . $GroupObj['DocTotal'] . ']<img src="/images/'.$LAYOUT_SKIN.'/alert_new.gif" align="absmiddle" />--></a></h1><p class="spacer"></p>
							                                           ';
					if ($GroupObj['DocTotal'] != "" && $GroupObj['DocTotal'] > 0) {
						$GroupInfo = $FileTotal . ', ' . $Lang['DigitalArchive']['LastUpload'] . ' ' . $GroupObj['LastUpload'];
					} else {
						$GroupInfo = '<font color="grey">' . $Lang['DigitalArchive']['NoFileInside'] . "</font>";
					}
					$content .= '
						                                                <div class="group_item_detail">
						                                                &nbsp; &nbsp; ' . $GroupInfo . '
						                                                </div>';
					$content .= '
							           		                             		 <p class="spacer"></p>
							                                   		 			 
							                                                </div></div>
							                                         	    <div class="group_item_bottom_left"><div class="group_item_bottom_right"></div></div>
							                                           </div>';
					$ItemsInCategory++;
					# break for 3 items per row
					if ($ItemsInCategory % 3 == 0) {
						$content .= '<p class="spacer"></p><br />';
					}
				}
				# close last category
				$content .= '                        
				                                              
				                                             <p class="spacer"></p><br />
				                                  </div></div>
				                                  <div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
				                                   <p class="spacer"></p>
				                             </div><p class="spacer"></p><br />
									';
			}
		}

		$content .= "<script language='javascript'>\n \$('#FilterDiv').hide();\n</script>\n";

		if ($NoOfRecord == 0) {
			$content .= $this->DisplayDocument("", $displayLocation = 0, $displayCheckbox = 0);
		}
		/*
		$content .= '
			<div class="edit_bottom">
				<span> '.$Lang['DigitalArchive']['Latest'].' '.count($data).' '.$Lang['DigitalArchive']['Files'].'</span>
				<p class="spacer"></p>
				<p class="spacer"></p>
			</div>
			<p class="spacer"></p>
		';
		*/

		$cloud = new wordCloud();

		$tagUsageArray = $lda->returnTagRankingArray($returnAmount = 30, $lda->AdminModule);

		# find the max amount of usage 
		$max = 0;
		for ($i = 0, $i_max = sizeof($tagUsageArray); $i < $i_max; $i++) {
			$max = ($tagUsageArray[$i]['Total'] > $max) ? $tagUsageArray[$i]['Total'] : $max;
		}

		for ($i = 0, $i_max = sizeof($tagUsageArray); $i < $i_max; $i++) {
			list ($tagid, $tagname, $total) = $tagUsageArray[$i];

			# define tag class 
			if ($max == 0 || $total / $max < 0.4) {
				$tagclass = "tag_small";
			} else
				if ($total / $max > 0.8) {
					$tagclass = "tag_big";
				} else {
					$tagclass = "tag_normal";
				}

			$cloud->addWord(array (
				'word' => intranet_htmlspecialchars($tagname),
				'size' => 1,
				'url' => 'displayTag.php?tagid=' . $tagid,
				'class' => $tagclass
			));

		}

		$content .= '
				<!-- TAG Content [Start] -->
			<div class="reading_board" id="resource_tag">
		        <div class="reading_board_top_left"><div class="reading_board_top_right">
		        	<h1><span>' . $Lang["DigitalArchive"]["CommonUseTag"] . '</span></h1><a class="reading_more" href="displayResult.php?flag=' . FUNCTION_TAG . '">' . $Lang["DigitalArchive"]["More"] . '...</a>
				</div></div>
		                        
		        <div class="reading_board_left">
		        	<div class="reading_board_right">
						<div class="tag_list">' . $cloud->showCloud() . '</div>
			            <p class="spacer"></p>
					</div>
				</div>
				<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
			</div>';

		return $content;
	}

	function Get_My_Group_Table2() {
		global $Lang, $LAYOUT_SKIN;
		
		$lda = new libdigitalarchive();

		$GroupCategoryData = $lda->getAllCategoryAndGroup();
		//debug_r($GroupCategoryData);

		$data = $lda->GetMyGroupList();

		for ($i = 0; $i < sizeof($data); $i++) {
			$groupObj = $data[$i];
			$GroupArr[$groupObj['GroupID']] = array (
				'GroupID' => $groupObj['GroupID'],
				'GroupTitle' => $groupObj['GroupTitle'],
				'DocTotal' => $groupObj['docTotal'],
				'LastUpload' => date("Y-m-d H:i", strtotime($groupObj[3]))
			);
			$DisplayLogCategory[$groupObj['GroupID']] = 0;
		}
		$NoOfRecord = count($data);

		//$content .= '<p class="spacer"></p>';
		//debug_r($GroupArr);

		# determine if there is any group for current in each category
		$CategoryGroupCount = array ();
		for ($i = 0; $i < sizeof($GroupCategoryData); $i++) {
			$CategoryObj = $GroupCategoryData[$i];
			$GroupObj = $GroupArr[$CategoryObj['GroupID']];
			if (is_array($GroupObj)) {
				$CategoryGroupCount[$CategoryObj['CategoryID']]++;
			}
		}

		$LastCategoryID = 0;
		$ItemsInCategory = 0;
		$rightGroup = 0;
		for ($i = 0; $i < sizeof($GroupCategoryData); $i++) {
			$CategoryObj = $GroupCategoryData[$i];
			if ($CategoryGroupCount[$CategoryObj['CategoryID']] <= 0) {
				continue;
			}
			if ($LastCategoryID != $CategoryObj['CategoryID']) {

				if ($ItemsInCategory > 0) {
					# close last category
					//					$content .= '                        
					//                                              
					//                                             <p class="spacer"></p><br />
					//                                  </div></div>
					//                                  <div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
					//                                   <p class="spacer"></p>
					//                             </div><p class="spacer"></p><br />
					//					';
					$content .= '</ul><p class="spacer"></p></div></div></div><div class="group_list_board_bottom"><div class="group_list_board_bottom_right"><div class="group_list_board_bottom_bg">
					                        
					                        </div></div></div></div>';
					# break for 2 Group per row
					if ($rightGroup == 1) {
						$content .= '<p class="spacer"></p>';
						$rightGroup = 0;
					} else
						$rightGroup = 1;
				}

				$ItemsInCategory = 0;

				# open for current new category
				//				$content .= '
				//					<div class="reading_board" id="resource_admin_doc_group">
				//                                      <div class="reading_board_top_left"><div class="reading_board_top_right">
				//                                                <h1><span>'.$CategoryObj['CategoryTitle'].'</span></h1>
				//                                              
				//                                      </div></div>
				//                                        
				//                                      <div class="reading_board_left"><div class="reading_board_right">
				//                                     ';
				$content .= '<div class="group_list_board">
										<div class="group_list_board_top"><div class="group_list_board_top_right"><div class="group_list_board_top_bg">
				                        	<h1>' . $CategoryObj['CategoryTitle'] . '</h1>
				                        </div></div></div>
				                        <div class="group_list_board_content"><div class="group_list_board_content_right"><div class="group_list_board_content_bg"><ul>';
			}
			$GroupObj = $GroupArr[$CategoryObj['GroupID']];

			if (is_array($GroupObj)) {
				$DisplayLogCategory[$CategoryObj['GroupID']] = $CategoryObj['CategoryID'];
				$FileTotal = ($GroupObj['DocTotal'] != "" && $GroupObj['DocTotal'] > 0) ? '<img src="/images/'.$LAYOUT_SKIN.'/reading_scheme/icon_file.gif" align="absmiddle" />' . $GroupObj['DocTotal'] . ' ' . $Lang["DigitalArchive"]["Files"] : "&nbsp;";
				//				$content .= '								
				//	                                               <div class="group_item">
				//	                                                <div class="group_item_top_left"><div class="group_item_top_right"></div></div>
				//	                                                <div class="group_item_left">
				//	                                                  <div class="group_item_right">
				//	                                                  <h1><a href="fileList.php?GroupID='.$GroupObj['GroupID'].'&FolderID=0"><img src="/images/'.$LAYOUT_SKIN.'/reading_scheme/icon_member.gif" align="absmiddle" border="0" /><font color="#1338b2">'.$GroupObj['GroupTitle'].'</font> <!--[<img src="/images/'.$LAYOUT_SKIN.'/reading_scheme/icon_file.gif" align="absmiddle" />'.$GroupObj['DocTotal'].']<img src="/images/'.$LAYOUT_SKIN.'/alert_new.gif" align="absmiddle" />--></a></h1><p class="spacer"></p>
				//	                                           ';
				if($lda->Has_Folder_In_Group($GroupObj['GroupID'], 0) == FALSE){
					//'<a href="fileList2.php?GroupID=' . $GroupObj['GroupID'] . '&FolderID=0" title="'.$Lang['DigitalArchive']['OpenFolder'].'"><li><div class="group_name">' . $GroupObj['GroupTitle'] . '</div></li></a><p class="spacer"></p>'
					$content .= ' 
				                                	<li><a href="fileList_Icon.php?GroupID=' . $GroupObj['GroupID'] . '&FolderID=0" class="group_name" title="'.$Lang['DigitalArchive']['OpenFolder'].'">' . $GroupObj['GroupTitle'] . '</a><p class="spacer"></p></li>
				                                   
				                                ';
				}
				else{
					$content .= ' 
				                                	<li><a href="javascript:displayQuickviewFolder(' . $GroupObj['GroupID'] . ');" class="group_name" title="'.$Lang['DigitalArchive']['QuickViewFolders'].'">' . $GroupObj['GroupTitle'] . '</a><p class="spacer"></p></li>
				                                   
				                                ';
				}

				//	           if ($GroupObj['DocTotal']!="" && $GroupObj['DocTotal']>0)
				//	           {
				//	           		$GroupInfo = $FileTotal.', '.$Lang['DigitalArchive']['LastUpload'].' '.$GroupObj['LastUpload'];
				//	           } else
				//	           {
				//	           		$GroupInfo = '<font color="grey">'.$Lang['DigitalArchive']['NoFileInside']."</font>";
				//	           }                           
				//           		$content .= '
				//                                                <div class="group_item_detail">
				//                                                &nbsp; &nbsp; '.$GroupInfo.'
				//                                                </div>';

				//	           $content .= '
				//	           		                             		 <p class="spacer"></p>
				//	                                   		 			 
				//	                                                </div></div></div>
				//	                                         	    <div class="group_list_board_bottom"><div class="group_list_board_bottom_right"><div class="group_list_board_bottom_bg">
				//                        
				//                        </div></div></div>
				//	                                           </div>';
				$ItemsInCategory++;
				# break for 3 items per row
				//				if ($ItemsInCategory%3==0)
				//				{
				//					$content .= '<p class="spacer"></p><br />';
				//				}
			}
			$LastCategoryID = $CategoryObj['CategoryID'];

		}
		if ($ItemsInCategory > 0) {
			# close last category
			//			$content .= '                        
			//                                      
			//                                     <p class="spacer"></p><br />
			//                          </div></div>
			//                          <div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
			//                           <p class="spacer"></p>
			//                     </div><p class="spacer"></p><br />
			//			';

			$content .= '
				           		                             		</ul><p class="spacer"></p>
				                                   		 			 
				                                                </div></div></div>
				                                         	    <div class="group_list_board_bottom"><div class="group_list_board_bottom_right"><div class="group_list_board_bottom_bg">
			                        
			                        </div></div></div>
				                                           </div>';
			# break for 2 Group per row
			if ($rightGroup == 1) {
				$content .= '<p class="spacer"></p>';
				$rightGroup = 0;
			} else
				$rightGroup = 1;
		}

		# list unclassified groups
		if (is_array($DisplayLogCategory) && sizeof($DisplayLogCategory) > 0) {
			$GroupsLeft = array ();
			foreach ($DisplayLogCategory AS $GroupID => $CategoryID) {
				if ($CategoryID == 0) {
					$GroupsLeft[] = $GroupID;
				}
			}
			if (sizeof($GroupsLeft) > 0) {
				$ItemsInCategory = 0;

				# open for current new category
				//				$content .= '
				//					<div class="reading_board" id="resource_admin_doc_group">
				//                                      <div class="reading_board_top_left"><div class="reading_board_top_right">
				//                                                <h1><span>('.$Lang['DigitalArchive']['Unclassified'].')</span></h1>
				//                                              
				//                                      </div></div>
				//                                        
				//                                      <div class="reading_board_left"><div class="reading_board_right">
				//                                     ';

				$content .= '<div class="group_list_board">
										<div class="group_list_board_top"><div class="group_list_board_top_right"><div class="group_list_board_top_bg">
				                        	<h1>(' . $Lang['DigitalArchive']['Unclassified'] . ')</h1>
				                        </div></div></div>
				                        <div class="group_list_board_content"><div class="group_list_board_content_right"><div class="group_list_board_content_bg">';

				for ($i = 0; $i < sizeof($GroupsLeft); $i++) {
					$GroupObj = $GroupArr[$GroupsLeft[$i]];
					$FileTotal = ($GroupObj['DocTotal'] != "" && $GroupObj['DocTotal'] > 0) ? '<img src="/images/'.$LAYOUT_SKIN.'/reading_scheme/icon_file.gif" align="absmiddle" />' . $GroupObj['DocTotal'] . ' ' . $Lang["DigitalArchive"]["Files"] : "&nbsp;";
					//					$content .= '								
					//		                                               <div class="group_item">
					//		                                                <div class="group_item_top_left"><div class="group_item_top_right"></div></div>
					//		                                                <div class="group_item_left">
					//		                                                  <div class="group_item_right">
					//		                                                  <h1><a href="fileList.php?GroupID='.$GroupObj['GroupID'].'&FolderID=0"><img src="/images/'.$LAYOUT_SKIN.'/reading_scheme/icon_member.gif" align="absmiddle" border="0" /><font color="#1338b2">'.$GroupObj['GroupTitle'].'</font> <!--[<img src="/images/'.$LAYOUT_SKIN.'/reading_scheme/icon_file.gif" align="absmiddle" />'.$GroupObj['DocTotal'].']<img src="/images/'.$LAYOUT_SKIN.'/alert_new.gif" align="absmiddle" />--></a></h1><p class="spacer"></p>
					//		                                           ';
					$content .= ' <ul>';
					                                	//<li><a href="fileList2.php?GroupID=' . $GroupObj['GroupID'] . '&FolderID=0" class="group_name">' . $GroupObj['GroupTitle'] . '</a><a href="javascript:displayQuickviewFolder(' . $GroupObj['GroupID'] . ');" class="btn_quickview_folder" title="Quick View Folders"></a> <p class="spacer"></p></li>
					if($lda->Has_Folder_In_Group($GroupObj['GroupID'], 0) == FALSE){
						$content .= ' 
					                                	<li><a href="fileList_Icon.php?GroupID=' . $GroupObj['GroupID'] . '&FolderID=0" class="group_name" title="'.$Lang['DigitalArchive']['OpenFolder'].'">' . $GroupObj['GroupTitle'] . '</a><p class="spacer"></p></li>
					                                   
					                                ';
					}
					else{
						$content .= ' 
					                                	<li><a href="javascript:displayQuickviewFolder(' . $GroupObj['GroupID'] . ');" class="group_name" title="'.$Lang['DigitalArchive']['QuickViewFolders'].'">' . $GroupObj['GroupTitle'] . '</a><p class="spacer"></p></li>
					                                   
					                                ';
					}                                   
					$content .= '</ul> ';
					if ($GroupObj['DocTotal'] != "" && $GroupObj['DocTotal'] > 0) {
						$GroupInfo = $FileTotal . ', ' . $Lang['DigitalArchive']['LastUpload'] . ' ' . $GroupObj['LastUpload'];
					} else {
						$GroupInfo = '<font color="grey">' . $Lang['DigitalArchive']['NoFileInside'] . "</font>";
					}
					//	           		$content .= '
					//	                                                <div class="group_item_detail">
					//	                                                &nbsp; &nbsp; '.$GroupInfo.'
					//	                                                </div>';
					//		           $content .= '
					//		           		                             		 <p class="spacer"></p>
					//		                                   		 			 
					//		                                                </div></div>
					//		                                         	    <div class="group_item_bottom_left"><div class="group_item_bottom_right"></div></div>
					//		                                           </div>';
					$ItemsInCategory++;
					# break for 3 items per row
					//					if ($ItemsInCategory%3==0)
					//					{
					//						$content .= '<p class="spacer"></p><br />';
					//					}
				}
				# close last category
				//				$content .= '                        
				//                                              
				//                                             <p class="spacer"></p><br />
				//                                  </div></div>
				//                                  <div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
				//                                   <p class="spacer"></p>
				//                             </div><p class="spacer"></p><br />
				//					';
				$content .= '
					           		                             		 <p class="spacer"></p>
					                                   		 			 
					                                                </div></div></div>
					                                         	    <div class="group_list_board_bottom"><div class="group_list_board_bottom_right"><div class="group_list_board_bottom_bg">
				                        
				                        </div></div></div>
					                                           </div>';
				# break for 2 Group per row
				if ($rightGroup == 1) {
					$content .= '<p class="spacer"></p>';
					$rightGroup = 0;
				} else
					$rightGroup = 1;
			}
		}
		$content .= "<script language='javascript'>\n \$('#FilterDiv').hide();\n</script>\n";

		if ($NoOfRecord == 0) {
			$content .= $this->DisplayDocument("", $displayLocation = 0, $displayCheckbox = 0);
		}
		/*
		$content .= '
			<div class="edit_bottom">
				<span> '.$Lang['DigitalArchive']['Latest'].' '.count($data).' '.$Lang['DigitalArchive']['Files'].'</span>
				<p class="spacer"></p>
				<p class="spacer"></p>
			</div>
			<p class="spacer"></p>
		';
		*/

		$cloud = new wordCloud();

		$tagUsageArray = $lda->returnTagRankingArray($returnAmount = 30, $lda->AdminModule);

		# find the max amount of usage 
		$max = 0;
		for ($i = 0, $i_max = sizeof($tagUsageArray); $i < $i_max; $i++) {
			$max = ($tagUsageArray[$i]['Total'] > $max) ? $tagUsageArray[$i]['Total'] : $max;
		}

		for ($i = 0, $i_max = sizeof($tagUsageArray); $i < $i_max; $i++) {
			list ($tagid, $tagname, $total) = $tagUsageArray[$i];

			# define tag class 
			if ($max == 0 || $total / $max < 0.4) {
				$tagclass = "tag_small";
			} else
				if ($total / $max > 0.8) {
					$tagclass = "tag_big";
				} else {
					$tagclass = "tag_normal";
				}

			$cloud->addWord(array (
				'word' => intranet_htmlspecialchars($tagname),
				'size' => 1,
				'url' => 'displayTag.php?tagid=' . $tagid,
				'class' => $tagclass
			));

		}

		//		$content .= '
		//		<!-- TAG Content [Start] -->
		//	<div class="reading_board" id="resource_tag">
		//        <div class="reading_board_top_left"><div class="reading_board_top_right">
		//        	<h1><span>'.$Lang["DigitalArchive"]["CommonUseTag"].'</span></h1><a class="reading_more" href="displayResult.php?flag='.FUNCTION_TAG.'">'.$Lang["DigitalArchive"]["More"].'...</a>
		//		</div></div>
		//                        
		//        <div class="reading_board_left">
		//        	<div class="reading_board_right">
		//				<div class="tag_list">'.$cloud->showCloud().'</div>
		//	            <p class="spacer"></p>
		//			</div>
		//		</div>
		//		<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
		//	</div>';

		return $content;
	}

	function Get_Latest_Table($ViewAmount = 5, $OrderBy, $OrderBy2) {
		global $Lang;

		$lda = new libdigitalarchive();

		$data = $lda->Get_Latest_Document($ViewAmount, $OrderBy, $OrderBy2);

		$NoOfRecord = count($data);

		$content .= '<p class="spacer"></p>';

		for ($i = 0; $i < $NoOfRecord; $i++) {
			list ($docId, $title, $hashName, $versionNo, $fileExt, $description, $size, $isFolder, $groupId, $parentFollderId) = $data[$i];
			$content .= '
							<!-- log start -->
							<div class="DA_file_log">
								<div class="DA_file_num">
									<span>' . ($i +1) . '.</span>
								</div>
								' . $this->DisplayDocument($docId, $displayLocation = 1, $displayCheckbox = 0) . '
							</div>
							<p class="spacer"></p>
							<!-- log end -->
						';
		}
		if ($NoOfRecord == 0) {
			$content .= $this->DisplayDocument("", $displayLocation = 0, $displayCheckbox = 0);
		}

		$content .= '
					<div class="edit_bottom">
						<span> ' . $Lang['DigitalArchive']['Latest'] . ' ' . count($data) . ' ' . $Lang['DigitalArchive']['Files'] . '</span>
						<p class="spacer"></p>
						<p class="spacer"></p>
					</div>
					<p class="spacer"></p>
				';

		$content .= "<script language='javascript'>\n \$('#FilterDiv').show();\n</script>\n";

		return $content;
	}

	function Get_Last_View_Table($ViewAmount = 5, $OrderBy, $OrderBy2) {
		global $Lang;

		$lda = new libdigitalarchive();

		$data = $lda->Get_Last_View_Document($ViewAmount, $OrderBy, $OrderBy2);

		$NoOfRecord = count($data);

		$content .= '<p class="spacer"></p>';

		for ($i = 0; $i < $NoOfRecord; $i++) {
			list ($docId, $title, $hashName, $versionNo, $fileExt, $description, $size, $isFolder, $groupId, $parentFollderId) = $data[$i];
			$content .= '
							<!-- log start -->
							<div class="DA_file_log">
								<div class="DA_file_num">
									<span>' . ($i +1) . '.</span>
								</div>
								' . $this->DisplayDocument($docId, $displayLocation = 1, $displayCheckbox = 0) . '
							</div>
							<p class="spacer"></p>
							<!-- log end -->
						';
		}
		if ($NoOfRecord == 0) {
			$content .= $this->DisplayDocument("", $displayLocation = 0, $displayCheckbox = 0);
		}

		$content .= '
					<div class="edit_bottom">
						<span> ' . $Lang['DigitalArchive']['Latest'] . ' ' . count($data) . ' ' . $Lang['DigitalArchive']['Files'] . '</span>
						<p class="spacer"></p>
						<p class="spacer"></p>
					</div>
					<p class="spacer"></p>
				';
		$content .= "<script language='javascript'>\n \$('#FilterDiv').show();\n</script>\n";
		return $content;
	}

	function DisplayDocument($DocumentID = "", $displayLocation = 1, $displayCheckbox = 1) {
		global $Lang, $userBrowser;

		$lda = new libdigitalarchive();

		$recInfo = $lda->Get_Admin_Doc_Info($DocumentID);

		# display Icon according to the file extension
		if ($recInfo['FileExtension'] != "") {
			$ext = (isset ($this->FileExtension[$recInfo['FileExtension']]) && $this->FileExtension[$recInfo['FileExtension']] != "") ? "file_" . $this->FileExtension[$recInfo['FileExtension']] : "file_unknown";
		}
		/*
		if(in_array(strtolower($recInfo['FileExtension']),array('jpg','jpeg','jpe'))){
			$file_path = getEncryptedText("../intranetdata/digital_archive/admin_doc/".$recInfo['FileHashName']);
			$thumbnail_image = '<img src="/home/plugin/imagethumbnail.php?image_e='.$file_path.'" width="85px" height="56px" border="0" />';
			if (!$userBrowser->isMSIE()){
				$thumbnail_css = ' style="background-image:url(\'/home/plugin/imagethumbnail.php?image_e='.$file_path.'\');background-size:85px 53px;background-position:center top;"';
			}else{
				$thumbnail_css = ' style="background-image:url(\'/home/plugin/imagethumbnail.php?image_e='.$file_path.'\');background-size:85px 53px;background-position:center top;';
				$thumbnail_css.= 'filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/home/plugin/imagethumbnail.php?image_e='.$file_path.'\',sizingMethod=\'scale\');';
				$thumbnail_css.= '-ms-filter: \"progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/home/plugin/imagethumbnail.php?image_e='.$file_path.'\',sizingMethod=\'scale\')\";"';
			}
		}
		*/
		$content = '
							<div class="DA_file_board">
								<div class="DA_file_board_top">	<div class="DA_file_board_top_right">	<div class="DA_file_board_top_bg"></div></div></div>
								<div class="DA_file_board_content"><div class="DA_file_board_content_right"><div class="DA_file_board_content_bg">
							
									<div class="DA_file_detail">';
		if ($displayCheckbox) {
			$content .= '		<div class="DA_file_checkbox"> <input type="checkbox" name="DocumentID[]" id="DocumentID[]" value="' . $DocumentID . '"/> </div>';
		}

		$size = ($recInfo['SizeInBytes'] != "") ? "(" . file_size($recInfo['SizeInBytes']) . ")" : "";

		# file title & size
		//if($DocumentID != ""){
		if ($DocumentID != "") {
			$content .= '
											<div class="DA_file_type">
						<a href="download.php?fileHashName=' . $recInfo['FileHashName'] . '" class="' . $ext . '" title="' . intranet_htmlspecialchars($recInfo['Title']) . '" '.$thumbnail_css.'>' . chopString($recInfo['Title'], 16, '...') . ' <em>' . $size . '</em></a>
					</div>';
		}
		$content .= '<div class="DA_file_info"> 
											<div class="DA_file_header"> 
												<a href="download.php?fileHashName=' . $recInfo['FileHashName'] . '" class="DA_file_title">' . $recInfo['Title'] . '</a> ';
		//}	
		# version	
		if ($recInfo['VersionNo'] != "") {
			$content .= '
													<div class="DA_file_version">' . $Lang["DigitalArchive"]["Version"] . ' <a href="javascript:newWindow(\'file_version.php?Thread=' . $recInfo['Thread'] . '\',10)">' . $recInfo['VersionNo'] . '</a></div> ';
		} else {
			$content .= "<div align='center'>" . $Lang['DigitalArchive']['NoFileInThisFolder'] . "</div>";
		}
		# location
		if ($displayLocation) {
			$groupInfo = $lda->getGroupInfo($recInfo['GroupID']);
			if ($recInfo['ParentFolderID'] == 0) {
				//$dirLocation = "<a href='list.php?GroupID=".$recInfo['GroupID']."&FolderID=0'> > </a>";
			//	$dirLocation = "<a href='".$_SERVER['PATH_INFO']."?GroupID=" . $recInfo['GroupID'] . "'>" . $groupInfo['GroupTitle'] . "</a>";
				$dirLocation = '<a href="javascript:self.location.href=\'fileList_Icon.php?GroupID='.$recInfo['GroupID'].'&FolderID=0\'">'.$groupInfo['GroupTitle'].'</a>';
			} else {
			//	$dirLocation = "<a href='".$_SERVER['PATH_INFO']."?GroupID=" . $recInfo['GroupID'] . "&FolderID=" . $recInfo['ParentFolderID'] . "'>" . $this->Get_Folder_Navigation($recInfo['ParentFolderID'], "> ", $isListTable = 0, $withGroupName = 1) . "</a>";
				$dirLocation = '<a href="javascript:self.location.href=\'fileList_Icon.php?GroupID='.$recInfo['GroupID'].'&FolderID='.$recInfo['ParentFolderID'].'\'">'. $this->Get_Folder_Navigation($recInfo['ParentFolderID'], "> ", $isListTable = 0, $withGroupName = 1) . '</a>';
			}

			$content .= '
													<div class="DA_file_location">In : ' . $dirLocation . '</div>';
		}

		# description
		$description = nl2br(preg_replace('/(https?:\/\/\S+)/is', '<a href="$1" target="_blank">$1</a>', $recInfo['Description']));
		$content .= '
												<p class="spacer">&nbsp;</p>
											</div>
										 
											<div class="DA_desc">' . $description . '</div>
											<div class="DA_tag">';
		# display "Tag"
		if ($recInfo['DocumentID'] <> "") {
			$tagInDoc = $lda->Get_Tag_By_Admin_Docuemnt_ID($recInfo['DocumentID']);
			for ($a = 0, $a_max = sizeof($tagInDoc); $a < $a_max; $a++) {
				list ($tagid, $tagname) = $tagInDoc[$a];
				$tagDisplay .= '<a href="fileList.php?TagId=' . $tagid . '">' . $tagname . '</a> ';
			}

			$content .= $tagDisplay;
		}

		# display "Last Modified"
		if ($recInfo['ModifyBy'] == "" || $recInfo['ModifyBy'] == 0) {
			$ById = $recInfo['InputBy'];
			$ByTime = $recInfo['DateInput'];
		} else {
			$ById = $recInfo['ModifyBy'];
			$ByTime = $recInfo['DateModified'];
		}
		$content .= '
											</div>
											<p class="spacer">&nbsp;</p>
											<p class="spacer"></p>
											<div class="DA_modifiyby">' . $this->Display_Name_Of_Last_Modified($ById, $ByTime) . '</div> 
											<p class="spacer"></p>
										</div>
									</div>
									<p class="spacer"></p>
								</div></div></div>
								<div class="DA_file_board_bottom"><div class="DA_file_board_bottom_right"><div class="DA_file_board_bottom_bg"></div></div></div>
							</div>
				';
		return $content;
	}

	function DisplayDocument_Icon($DocumentID = "", $displayLocation = 1, $displayCheckbox = 1) {
		global $Lang, $userBrowser;

		$lda = new libdigitalarchive();

		$recInfo = $lda->Get_Admin_Doc_Info($DocumentID);

		# display Icon according to the file extension
		if ($recInfo['FileExtension'] != "") {
			$ext = (isset ($this->FileExtension[$recInfo['FileExtension']]) && $this->FileExtension[$recInfo['FileExtension']] != "") ? "file_" . $this->FileExtension[$recInfo['FileExtension']] : "file_unknown";
		} else
			return;

		//		$content = '
		//					<div class="DA_file_board">
		//						<div class="DA_file_board_top">	<div class="DA_file_board_top_right">	<div class="DA_file_board_top_bg"></div></div></div>
		//						<div class="DA_file_board_content"><div class="DA_file_board_content_right"><div class="DA_file_board_content_bg">
		//					
		//							<div class="DA_file_detail">';
		if ($recInfo['DocumentID'] <> "") {
			$tagInDoc = $lda->Get_Tag_By_Admin_Docuemnt_ID($recInfo['DocumentID']);
			for ($a = 0, $a_max = sizeof($tagInDoc); $a < $a_max; $a++) {
				list ($tagid, $tagname) = $tagInDoc[$a];
				$tagDisplay .= '<a href="fileList.php?TagId=' . $tagid . '">' . $tagname . '</a> ';
			}

		}

		if ($recInfo['ModifyBy'] == "" || $recInfo['ModifyBy'] == 0) {
			$ById = $recInfo['InputBy'];
			$ByTime = $recInfo['DateInput'];
		} else {
			$ById = $recInfo['ModifyBy'];
			$ByTime = $recInfo['DateModified'];
		}

		//if($userId=="") return;

		global $intranet_session_language, $Lang, $PATH_WRT_ROOT;

		$lda = new libdigitalarchive();
	
		if(in_array(strtolower($recInfo['FileExtension']),array('jpg','jpeg','jpe'))){
			$file_path = getEncryptedText("../intranetdata/digital_archive/admin_doc/".$recInfo['FileHashName']);
			$thumbnail_image = '<img src="/home/plugin/imagethumbnail.php?image_e='.$file_path.'" width="85px" height="56px" border="0" />';
			$thumbnail_css = ' style="height:56px;background-image:url(\'/home/plugin/imagethumbnail.php?image_e='.$file_path.'\');background-size:85px 56px;background-position:center top;"';
			$IE_thumbnail_css = 'height:56px;background-image:url(\'/home/plugin/imagethumbnail.php?image_e='.$file_path.'\');background-size:85px 56px;background-position:center top;';
			$IE_thumbnail_css.= 'filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/home/plugin/imagethumbnail.php?image_e='.$file_path.'\',sizingMethod=\'scale\');';
			$IE_thumbnail_css.= '-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/home/plugin/imagethumbnail.php?image_e='.$file_path.'\',sizingMethod=\'scale\')";';
			$paddingtop_css = 'style="padding-top:56px;"';
		}
		
		$name_field = getNameFieldByLang();
		$name = $lda->Get_User_Name($ById, $name_field);

		if ($intranet_session_language == "en") {
			//$str = substr($datetime,0,10)." ".$Lang['General']['LastModifiedBy']." ".$name;
			$str = $ByTime . " " . $Lang['RepairSystem']['by'] . " " . $name;
		} else {
			//	$str = $name." ".$Lang['RepairSystem']['on'].substr($datetime,0,10)." ".$Lang['General']['LastModified2'];
			$str = $name . " " . $Lang['RepairSystem']['on'] . $ByTime;
		}
		$size = ($recInfo['SizeInBytes'] != "") ? file_size($recInfo['SizeInBytes']) : "";
		$content = '<li id="fderid' . $DocumentID . '">
						<span class="icon_' . $ext . '" '.$paddingtop_css.'>';
		if (!$userBrowser->isMSIE())
		    $content .= '<a href="javascript:void(0);" title="'.$Lang['DigitalArchive']['FileInfo'].'" class="file_icon" onclick="$(\'.file_info_layer\').hide();$(\'#d'.$DocumentID.'\').show();MM_showHideLayers(\'d' . $DocumentID . '\',\'\',\'show\')" '.$thumbnail_css.'></a>';
		else
		    $content .= '<a href="javascript:void(0);" title="'.$Lang['DigitalArchive']['FileInfo'].'" class="file_icon" onclick="$(\'.file_info_layer\').hide();$(\'#d'.$DocumentID.'\').show();MM_showHideLayers(\'d' . $DocumentID . '\',\'\',\'show\')" style="left:0px;'.$IE_thumbnail_css.'"></a>';
		    $content .= '<a href="download.php?fileHashName=' . $recInfo['FileHashName'] . '" class="file_name">' . chopString($recInfo['Title'], 16, '...') . '</a>';
		if ($displayCheckbox) {
			$content .= '<em><input type="checkbox" name="DocumentID[]" id="DocumentID[]" onClick="(this.checked)?($(\'#d' . $DocumentID . ' em input:checkbox\').attr(\'checked\',\'checked\'), $(\'#d' . $DocumentID . ' .file_info_icon_title .icon_' . $ext . '\').addClass(\'selected\'), $(\'#fderid' . $DocumentID . '\').addClass(\'selected\')):($(\'#d' . $DocumentID . ' em input:checkbox\').removeAttr(\'checked\'), $(\'#d' . $DocumentID . ' .file_info_icon_title .icon_' . $ext . '\').removeClass(\'selected\'), $(\'#fderid' . $DocumentID . '\').removeClass(\'selected\'))" value="' . $DocumentID . '"/></em>';
			//$content .= '<em><input type="checkbox" name="DocumentID[]" id="DocumentID[]" onClick="(this.checked)?$(\'#fderid' . $DocumentID . '\').addClass(\'selected\'):$(\'#fderid' . $DocumentID . '\').removeClass(\'selected\')" value="' . $DocumentID . '"/></em>';
		
		}
		$content .= '</span>
					 <p class="spacer"></p>
		             <div id="d' . $DocumentID . '" class="file_info_layer">
		             	<div class="file_info_icon_title">
		               		<span class="icon_' . $ext . '">';
		if (!$userBrowser->isMSIE())
			$content .= '<span class="file_icon"><a href="javascript:void(0);" title="'.$Lang['DigitalArchive']['FileInfo'].'" class="file_icon" onclick="MM_showHideLayers(\'d' . $DocumentID . '\',\'\',\'hide\')" '.$thumbnail_css.'></a></span>';
		else
			$content .= '<span class="file_icon" style="left:5px;'.$IE_thumbnail_css.'"></span>';
		$content .= '<a href="javascript:void(0);" class="file_name">' . chopString($recInfo['Title'], 16, '...') . '</a>';
		if ($displayCheckbox) {
			$content .= '<em><input type="checkbox" name="DocumentID1[]" id="DocumentID1[]" onClick="(this.checked)?($(\'#fderid' . $DocumentID . ' .icon_'.$ext.' em input:checkbox\').attr(\'checked\',\'checked\'), $(\'#d' . $DocumentID . ' .file_info_icon_title .icon_' . $ext . '\').addClass(\'selected\'), $(\'#fderid' . $DocumentID . '\').addClass(\'selected\')):($(\'#fderid' . $DocumentID . ' .icon_'.$ext.' em input:checkbox\').removeAttr(\'checked\'), $(\'#d' . $DocumentID . ' .file_info_icon_title .icon_' . $ext . '\').removeClass(\'selected\'), $(\'#fderid' . $DocumentID . '\').removeClass(\'selected\'))" value="' . $DocumentID . '"/></em>';
			//$content .= '<em><input type="checkbox" name="DocumentID[]" id="DocumentID[]" value="' . $DocumentID . '"/></em>';
		}
			$content .= '</span>
		             	 <div class="file_info_title">
			             	<h1><a href="download.php?fileHashName=' . $recInfo['FileHashName'] . '" class="file_name">' . $recInfo['Title'] . '</a></h1>
			                <p class="spacer"></p>';
			if ($recInfo['VersionNo'] != "") {
				$content .= '<em>' . $Lang["DigitalArchive"]["Version"] . '</em><span>: ' . $recInfo['VersionNo'] . '</span>
				             <p class="spacer"></p>';
			}
				$content .= '<em>' . $Lang["DigitalArchive"]["Size"] . '</em><span> : ' . $size . '</span>                                                                     
			             </div>
			             <p class="spacer"></p>                                           
		             	</div>';
		if($recInfo['Description'] !="" || $tagDisplay !=""){
		    $content .= '<div class="file_info_icon_detail">';
			if($recInfo['Description'] !=""){
			    $description = nl2br(preg_replace('/(https?:\/\/\S+)/is', '<a href="$1" target="_blank">$1</a>', $recInfo['Description']));
			    $content .= '<h2>' . $Lang["DigitalArchive"]["Description"] . ' :</h2>' . $description;
			}
			if($tagDisplay !="")
				$content .= '<h2>' . $Lang["DigitalArchive"]["Tag"] . ' :</h2><div class="tag">' . $tagDisplay.'</div>'; 
			$content .='<p class="spacer"></p>
			            </div>
			            <p class="spacer"></p>';
		}
		    $content .='<div class="file_info_icon_detail">
		                  <h2>' . $Lang['eDiscipline']['FieldTitle']['LastUpdated'] . ' : </h2>' .
						$str .
						'</div>
                         <p align="center" class="spacer"></p>
                         
                         <div class="edit_bottom">
                           <input type="button" class="formsmallbutton" value="Close" onclick="MM_showHideLayers(\'d' . $DocumentID . '\',\'\',\'hide\')"/>
                         </div>
		               </div>
		               </li>';
		/*if($displayCheckbox) {
			$content .= '		<div class="DA_file_checkbox"> <input type="checkbox" name="DocumentID[]" id="DocumentID[]" value="'.$DocumentID.'"/> </div>';
		}
		
		$size = ($recInfo['SizeInBytes']!="") ? "(".file_size($recInfo['SizeInBytes']).")" : "";
		
		# file title & size
		$content .= '
								<div class="DA_file_type">
									<a href="download.php?fileHashName='.$recInfo['FileHashName'].'" class="'.$ext.'" title="'.intranet_htmlspecialchars($recInfo['Title']).'">'.chopString($recInfo['Title'],16,'...').' <em>'.$size.'</em></a>
								</div>
								<div class="DA_file_info"> 
									<div class="DA_file_header"> 
										<a href="download.php?fileHashName='.$recInfo['FileHashName'].'" class="DA_file_title">'.$recInfo['Title'].'</a> ';
									
		# version	
		if($recInfo['VersionNo']!="") {								
			$content .= '
										<div class="DA_file_version">'.$Lang["DigitalArchive"]["Version"].' <a href="javascript:newWindow(\'file_version.php?Thread='.$recInfo['Thread'].'\',10)">'.$recInfo['VersionNo'].'</a></div> ';
		} else {
			$content .= "<div align='center'>".$Lang['DigitalArchive']['NoFileInThisFolder']."</div>";	
		}
		# location
		if($displayLocation) {
			$groupInfo = $lda->getGroupInfo($recInfo['GroupID']);
			if($recInfo['ParentFolderID']==0) {
				//$dirLocation = "<a href='list.php?GroupID=".$recInfo['GroupID']."&FolderID=0'> > </a>";
				$dirLocation = "<a href='fileList.php?GroupID=".$recInfo['GroupID']."'>".$groupInfo['GroupTitle']."</a>";
			} else {
				$dirLocation = "<a href='fileList.php?GroupID=".$recInfo['GroupID']."&FolderID=".$recInfo['ParentFolderID']."'>".$this->Get_Folder_Navigation($recInfo['ParentFolderID'], "> ", $isListTable=0, $withGroupName=1)."</a>";
			}
			
			$content .= '
										<div class="DA_file_location">In : '.$dirLocation.'</div>';
		}
		$content .= '
										<p class="spacer">&nbsp;</p>
									</div>
								 
									<div class="DA_desc">'.nl2br($recInfo['Description']).'</div>
									<div class="DA_tag">';
		# display "Tag"
		if ($recInfo['DocumentID']<>"")
		{
			$tagInDoc = $lda->Get_Tag_By_Admin_Docuemnt_ID($recInfo['DocumentID']);
			for($a=0, $a_max=sizeof($tagInDoc); $a<$a_max; $a++) {
				list($tagid, $tagname) = $tagInDoc[$a];
				$tagDisplay .= '<a href="fileList.php?TagId='.$tagid.'">'.$tagname.'</a> ';
			}
			
			$content .= $tagDisplay;
		}
		
		# display "Last Modified"
		if($recInfo['ModifyBy']=="" || $recInfo['ModifyBy']==0) {
			$ById = $recInfo['InputBy'];
			$ByTime = $recInfo['DateInput'];
		} else {
			$ById = $recInfo['ModifyBy'];
			$ByTime = $recInfo['DateModified'];
		}
		$content .= '
									</div>
									<p class="spacer">&nbsp;</p>
									<p class="spacer"></p>
									<div class="DA_modifiyby">'.$this->Display_Name_Of_Last_Modified($ById,$ByTime).'</div> 
									<p class="spacer"></p>
								</div>
							</div>
							<p class="spacer"></p>
						</div></div></div>
						<div class="DA_file_board_bottom"><div class="DA_file_board_bottom_right"><div class="DA_file_board_bottom_bg"></div></div></div>
					</div>
		</div>
		<p class="spacer"></p>';	*/
		return $content;
	}

	function Display_Name_Of_Last_Modified($userId = "", $datetime = "") {
		if ($userId == "")
			return;

		global $intranet_session_language, $Lang;

		$lda = new libdigitalarchive();

		$name_field = getNameFieldByLang();
		$name = $lda->Get_User_Name($userId, $name_field);

		if ($intranet_session_language == "en") {
			//$str = substr($datetime,0,10)." ".$Lang['General']['LastModifiedBy']." ".$name;
			$str = $datetime . " " . $Lang['General']['LastModifiedBy'] . " " . $name;
		} else {
			//	$str = $name." ".$Lang['RepairSystem']['on'].substr($datetime,0,10)." ".$Lang['General']['LastModified2'];
			$str = $name . " " . $Lang['RepairSystem']['on'] . $datetime . " " . $Lang['General']['LastModified2'];
		}

		return $str;

	}

	function Display_File_List_Table($GroupID, $FolderID = 0, $skipFolder = 0, $OrderBy = "filename", $OrderBy2 = "ASC") {
		global $Lang, $userBrowser;

		$lda = new libdigitalarchive();

		$directoryList = $lda->getDirectoryList($GroupID, $FolderID, $skipFolder);
		//debug_r($directoryList);

		$NoOfRecord = count($directoryList);

		$folder_rx = "";

		for ($i = 0; $i < $NoOfRecord; $i++) {
			$filename = $directoryList[$i]['Title'];
			$date = ($directoryList[$i]['ModifyBy'] == 0) ? $directoryList[$i]['DateInput'] : $directoryList[$i]['DateModifed'];
			if ($directoryList[$i]['IsFolder']) {
				//$folder_rx .= "&nbsp; <a href=\"javascript:void()\" onClick=\"self.location.href='fileList.php?GroupID={$GroupID}&FolderID=".$directoryList[$i]['DocumentID'] ."'\" ><img src=\"/images/".$LAYOUT_SKIN."/icon_files/folder.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" />". $directoryList[$i]['Title'] . "</a> <input type=\"checkbox\" name=\"DocumentID[]\" id=\"DocumentID2[]\" value=\"".$directoryList[$i]['DocumentID'] ."\"/> &nbsp; ";
				$folder_rx .= '<li id="fderid' . $directoryList[$i]['DocumentID'] . '">';
				if (!$userBrowser->isMSIE()) {
					$folder_rx .= '<span>';
				}
				$folder_rx .= '<input name="DocumentID[]" id="DocumentID[]" type="checkbox" value="' . $directoryList[$i]['DocumentID'] . '" onClick="(this.checked)?$(\'#fderid' . $directoryList[$i]['DocumentID'] . '\').addClass(\'selected\'):$(\'#fderid' . $directoryList[$i]['DocumentID'] . '\').removeClass(\'selected\');" />';
				if (!$userBrowser->isMSIE()) {
					$folder_rx .= '</span>';
				}
				$folder_rx .= '<a href="javascript:void()" onClick="self.location.href=\'fileList.php?GroupID=' . $GroupID . '&FolderID=' . $directoryList[$i]['DocumentID'] . '\'" >' . $directoryList[$i]['Title'] . '</a></li>';

			} else {
				if ($OrderBy == "filename") {
					$tempAry[$filename][] = $directoryList[$i];
				} else
					if ($OrderBy == "date") {
						$tempAry[$date][] = $directoryList[$i];
					}
			}

			//$content .= $this->DisplayDocument($docId, $displayLocation=0, $displayCheckbox=1);
		}

		if ($folder_rx != "") {
			if (!$userBrowser->isMSIE()) {
				# add more style for better display in other browsers
				$content .= '<style type="text/css">
												ul.da_folder_list li {position:relative;}
											</style>
											';
			}
			$content .= '
								<div class="reading_board" id="resource_admin_doc_group">
			                                      <div class="reading_board_top_left"><div class="reading_board_top_right">
			                                                <h1><span>' . $Lang["DigitalArchive"]["Folder"] . '</span></h1>
			                                      </div></div>
			                                        
			                                      <div class="reading_board_left"><div class="reading_board_right">
			                                     			<ul class="da_folder_list">
			                                                	' . $folder_rx . '                                                 
			                                                 </ul>
															 <p class="spacer"></p>
			                                      </div></div>
			                                      <div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
			                                       <p class="spacer"></p>
			                                 </div>      ';
		}

		//$content = $folder_rx;
		$returnAry = array ();

		if ($NoOfRecord > 0) {
			if ($OrderBy2 == "ASC") {
				if (count($tempAry) > 0)
					ksort($tempAry);
			} else {
				if (count($tempAry) > 0)
					krsort($tempAry);
			}

			if (count($tempAry) > 0) {
				foreach ($tempAry as $_ary) {
					for ($a = 0; $a < count($_ary); $a++) {
						$docId = $_ary[$a]['DocumentID'];
						$content .= $this->DisplayDocument($docId, $displayLocation = 0, $displayCheckbox = 1);
					}
				}
			} else {
				$content .= $this->DisplayDocument("", $displayLocation = 0, $displayCheckbox = 0);
			}
		}
		if ($NoOfRecord == 0) {
			$content .= $this->DisplayDocument("", $displayLocation = 0, $displayCheckbox = 0);
		}

		$editableRecordIdByGroupID = $lda->Get_Editable_Record_ID_By_GroupID($GroupID, $FolderID);

		$x .= '<script language="javascript">';
		$x .= 'var editableAry = new Array();';

		for ($i = 0; $i < sizeof($editableRecordIdByGroupID); $i++) {

			$x .= 'editableAry[' . $i . '] = "' . $editableRecordIdByGroupID[$i] . '"' . "\n";
		}
		$x .= '</script>';

		return $content . $x;
	}

	function Display_File_List_Table2($GroupID, $FolderID = 0, $skipFolder = 0, $OrderBy = "filename", $OrderBy2 = "ASC") {
		global $Lang, $userBrowser, $fileNo;

		$lda = new libdigitalarchive();

		$directoryList = $lda->getDirectoryList($GroupID, $FolderID, $skipFolder);
		//debug_r($directoryList);

		$NoOfRecord = count($directoryList);

		$folder_rx = "";

		for ($i = 0; $i < $NoOfRecord; $i++) {
			$filename = $directoryList[$i]['Title'];
			$date = ($directoryList[$i]['ModifyBy'] == 0) ? $directoryList[$i]['DateInput'] : $directoryList[$i]['DateModifed'];
			if ($directoryList[$i]['IsFolder']) {
				$folder_rx .= '
								<!-- folder log start -->
				                        	<div class="DA_file_log DA_file_log_folder">
				                            
				                            	<div class="DA_file_board">
				                                	<div class="DA_file_board_top">	<div class="DA_file_board_top_right">	<div class="DA_file_board_top_bg"></div></div></div>
				                                    <div class="DA_file_board_content">	<div class="DA_file_board_content_right">	<div class="DA_file_board_content_bg">
				                                    	
				                                        <div class="DA_file_detail">';
				//                                          <div class="DA_file_checkbox"> <input type="checkbox" /> </div>
				//                                        	<div class="DA_file_type"><a href="#" class="file_folder_l"> Folder 1</a></div>

				// $folder_rx .= '<li id="fderid'.$directoryList[$i]['DocumentID'].'">';
				//				if (!$userBrowser->isMSIE())
				//				{
				//						$folder_rx .= '<span>';
				//				}
				$folder_rx .= '<div class="DA_file_checkbox"><input name="DocumentID[]" id="DocumentID[]" type="checkbox" value="' . $directoryList[$i]['DocumentID'] . '" onClick="(this.checked)?$(\'#fderid' . $directoryList[$i]['DocumentID'] . '\').addClass(\'selected\'):$(\'#fderid' . $directoryList[$i]['DocumentID'] . '\').removeClass(\'selected\');" /></div>';
				//				if (!$userBrowser->isMSIE())
				//				{
				//						$folder_rx .= '</span>';
				//				}
				//$folder_rx .= '<div class="DA_file_type"><a href="javascript:void()" class="file_folder_l" onClick="self.location.href=\'fileList2.php?GroupID=' . $GroupID . '&FolderID=' . $directoryList[$i]['DocumentID'] . '\'" >' . $directoryList[$i]['Title'] . '</a></div>';
				$folder_rx .= '<div class="DA_file_type"><a href="javascript:void()" class="file_folder_l" onClick="file_folder_onclick(\''.$GroupID.'\', \''.$directoryList[$i]['DocumentID'].'\')" >' . $directoryList[$i]['Title'] . '</a></div>';
				
				$folder_rx .= '</div> 
				                                       
				                                    <p class="spacer"></p>
				                                    
				                                    </div></div></div>
				                                	<div class="DA_file_board_bottom">	<div class="DA_file_board_bottom_right">	<div class="DA_file_board_bottom_bg"></div></div></div>
				                                </div>
				                            </div>
				                        	<p class="spacer"></p>
				                        <!-- folder log end -->';
				//$folder_rx .= "&nbsp; <a href=\"javascript:void()\" onClick=\"self.location.href='fileList.php?GroupID={$GroupID}&FolderID=".$directoryList[$i]['DocumentID'] ."'\" ><img src=\"/images/".$LAYOUT_SKIN."/icon_files/folder.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" />". $directoryList[$i]['Title'] . "</a> <input type=\"checkbox\" name=\"DocumentID[]\" id=\"DocumentID2[]\" value=\"".$directoryList[$i]['DocumentID'] ."\"/> &nbsp; ";
				//				$folder_rx .= '<li id="fderid'.$directoryList[$i]['DocumentID'].'">';
				//				if (!$userBrowser->isMSIE())
				//				{
				//						$folder_rx .= '<span>';
				//				}
				//				$folder_rx .= '<input name="DocumentID[]" id="DocumentID[]" type="checkbox" value="'.$directoryList[$i]['DocumentID'] .'" onClick="(this.checked)?$(\'#fderid'.$directoryList[$i]['DocumentID'].'\').addClass(\'selected\'):$(\'#fderid'.$directoryList[$i]['DocumentID'].'\').removeClass(\'selected\');" />';
				//				if (!$userBrowser->isMSIE())
				//				{
				//						$folder_rx .= '</span>';
				//				}
				//				$folder_rx .= '<a href="javascript:void()" onClick="self.location.href=\'fileList.php?GroupID='.$GroupID.'&FolderID='.$directoryList[$i]['DocumentID'] .'\'" >'. $directoryList[$i]['Title'] . '</a></li>';
				//								
			} else {
				if ($OrderBy == "filename") {
					$tempAry[$filename][] = $directoryList[$i];
				} else
					if ($OrderBy == "date") {
						$tempAry[$date][] = $directoryList[$i];
					}
			}

			//$content .= $this->DisplayDocument($docId, $displayLocation=0, $displayCheckbox=1);
		}

		if ($folder_rx != "" || TRUE) {
			if (!$userBrowser->isMSIE()) {
				# add more style for better display in other browsers
				//				$content .= '<style type="text/css">
				//								ul.da_folder_list li {position:relative;}
				//							</style>
				//							';
			}
			//			$content .= '
			//					<div class="reading_board" id="resource_admin_doc_group">
			//                                      <div class="reading_board_top_left"><div class="reading_board_top_right">
			//                                                <h1><span>'.$Lang["DigitalArchive"]["Folder"].'</span></h1>
			//                                      </div></div>
			//                                        
			//                                      <div class="reading_board_left"><div class="reading_board_right">
			//                                     			<ul class="da_folder_list">
			//                                                	'.$folder_rx.'                                                 
			//                                                 </ul>
			//												 <p class="spacer"></p>
			//                                      </div></div>
			//                                      <div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
			//                                       <p class="spacer"></p>
			//                                 </div>      ';
			
			$GroupID = $_REQUEST['GroupID'];
			$SelectedGroupID = $_REQUEST['SelectedGroupID'];
			$array = $lda->Get_Folder_Hierarchy_File_Count_Array($GroupID);
			
			if($_REQUEST['folderTreeSelect'] == ""){
				//$tree_style = 'style="visibility:hidden;"';
				//$file_style = 'style="position: relative; left: 0px; width:100%"';
				//$tempContent = "</div>";
				$tree_style = 'style="display:none;width:0;visibility:hidden;"';
				$file_style = 'style="width:100%"';
			}
			else{
				
				//$file_style = 'style="position: relative; left: 200px;"';
				//$tree_style = 'style="position: absolute; left:0px;"';
				//$tempContent = "</div>";
				$tempClass = 'class="tree_open"';
			}
				
			if (sizeof($array) > 0) {
				$content .= '<div id="DA_file_list" '.$tempClass.'><div class="tree_list" id="tree_list" '.$tree_style.'>' . "\n";

				$content .= $this->Get_Quickview_Folder_Div($array, 0, $FolderID = 0, "", "", $GroupID, $SelectedGroupID, FALSE);

				$content .= '</div>'.$tempContent. "\n";
			}
			
			$content .= '<div class="file_list" id="file_list" '.$file_style.'>
								<!--<div class="DA_tool_selectall"><input name="" type="checkbox" value="" />All</div>-->
			                	<div class="DA_tool_selectall">
			                			<span id="checkboxAllDiv"><label for="CheckAll">' . $Lang['General']['All'] . '</label><input type="checkbox" id="CheckAll" onClick = "(this.checked)?setChecked(1,this.form,\'DocumentID[]\',1):setChecked(0,this.form,\'DocumentID[]\',1)"/></span>
			                    </div>     
			                                    <p class="spacer"></p>
			                    <!-- Log list start-->
			                    ' . $folder_rx . '
			                   
								';
		}

		//$content = $folder_rx;
		$returnAry = array ();
		$fileNo = 0;

		if ($NoOfRecord > 0) {
			if ($OrderBy2 == "ASC") {
				if (count($tempAry) > 0)
					ksort($tempAry);
			} else {
				if (count($tempAry) > 0)
					krsort($tempAry);
			}

			if (count($tempAry) > 0) {
				foreach ($tempAry as $_ary) {

					for ($a = 0; $a < count($_ary); $a++) {
						$docId = $_ary[$a]['DocumentID'];
						$fileNo++;
						$content .= '<div class="DA_file_log">
											<div class="DA_file_num"><span>' . $fileNo . '.</span></div>';
						$content .= $this->DisplayDocument($docId, $displayLocation = 0, $displayCheckbox = 1);
						$content .= '</div><p class="spacer"></p>';
					}
				}
			} else {
				$fileNo++;
				//				$content .= '<div class="DA_file_log">
				//					<div class="DA_file_num"><span>'.$fileNo.'.</span></div>';
				$content .= $this->DisplayDocument("", $displayLocation = 0, $displayCheckbox = 0);
				//				$content .='</div><p class="spacer"></p>';
			}
		}
		if ($NoOfRecord == 0) {
			$fileNo++;
			//			$content .= '<div class="DA_file_log">
			//					<div class="DA_file_num"><span>'.$fileNo.'.</span></div>';
			$content .= $this->DisplayDocument("", $displayLocation = 0, $displayCheckbox = 0);
			//			$content .='</div><p class="spacer"></p>';
		}
		$content .='</div>';
		$editableRecordIdByGroupID = $lda->Get_Editable_Record_ID_By_GroupID($GroupID, $FolderID);

		$x .= '<script language="javascript">';
		$x .= 'var editableAry = new Array();';

		for ($i = 0; $i < sizeof($editableRecordIdByGroupID); $i++) {

			$x .= 'editableAry[' . $i . '] = "' . $editableRecordIdByGroupID[$i] . '"' . "\n";
		}
		$x .= '</script>';

		return $content . $x;
	}

	function Display_File_List_Table_Icon($GroupID, $FolderID = 0, $skipFolder = 0, $OrderBy = "filename", $OrderBy2 = "ASC") {
		global $Lang, $userBrowser;

		$lda = new libdigitalarchive();

		$directoryList = $lda->getDirectoryList($GroupID, $FolderID, $skipFolder);
		//debug_r($directoryList);

		$NoOfRecord = count($directoryList);
		$NoOfFolder = 0;
		$folder_rx = "";
		$folder_rx .= '<ul class="da_folder_list">';
		for ($i = 0; $i < $NoOfRecord; $i++) {
			$filename = $directoryList[$i]['Title'];
			$date = ($directoryList[$i]['ModifyBy'] == 0) ? $directoryList[$i]['DateInput'] : $directoryList[$i]['DateModifed'];
			if ($directoryList[$i]['IsFolder']) {
				$NoOfFolder++;
				//$folder_rx .= "&nbsp; <a href=\"javascript:void()\" onClick=\"self.location.href='fileList.php?GroupID={$GroupID}&FolderID=".$directoryList[$i]['DocumentID'] ."'\" ><img src=\"/images/".$LAYOUT_SKIN."/icon_files/folder.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" />". $directoryList[$i]['Title'] . "</a> <input type=\"checkbox\" name=\"DocumentID[]\" id=\"DocumentID2[]\" value=\"".$directoryList[$i]['DocumentID'] ."\"/> &nbsp; ";
				$folder_rx .= '<li id="fderid' . $directoryList[$i]['DocumentID'] . '">';
				//if (!$userBrowser->isMSIE())
				//{
				$folder_rx .= '<span class="file_folder">';
				//}
				$folder_rx .= '<em><input name="DocumentID[]" id="DocumentID[]" type="checkbox" value="' . $directoryList[$i]['DocumentID'] . '" onClick="(this.checked)?$(\'#fderid' . $directoryList[$i]['DocumentID'] . '\').addClass(\'selected\'):$(\'#fderid' . $directoryList[$i]['DocumentID'] . '\').removeClass(\'selected\');" /></em>';

				//$folder_rx .= '<a href="javascript:void()" onClick="self.location.href=\'fileList_Icon.php?folderTreeSelect=&GroupID=' . $GroupID . '&FolderID=' . $directoryList[$i]['DocumentID'] . '\'" >' . $directoryList[$i]['Title'] . '</a>';
				$folder_rx .= '<a href="javascript:void()" onClick="file_folder_onclick(\''.$GroupID.'\', \''.$directoryList[$i]['DocumentID'].'\')" >' . $directoryList[$i]['Title'] . '</a>';
				
				//if (!$userBrowser->isMSIE())
				//{
				$folder_rx .= '</span>';
				//}
				$folder_rx .= '<p class="spacer"></p></li>';
				//				$folder_rx1 .='<li class="selected"><span class="file_folder"><em><input name="" type="checkbox" value="" checked="checked" />
				//                                                   </em><a href="#" class="file_name">Public Folder 000001 abcdefg </a></span><p class="spacer"></p></li>';
				//				$folder_rx2 .='
				//                    <ul class="da_folder_list">
				//                                                	
				//                                                   <li class="selected"><span class="file_folder"><em><input name="" type="checkbox" value="" checked="checked" />
				//                                                   </em><a href="#" class="file_name">Public Folder 000001 abcdefg </a></span><p class="spacer"></p></li>
				//                                                   <li><span class="file_folder"><a href="#" class="file_name">Public Folder 000001 abcdefg </a><em><input name="" type="checkbox" value="" checked="checked" /></em></span><p class="spacer"></p></li>
				//                                                   <li><span class="file_folder"><em><input name="" type="checkbox" value="" checked="checked" /></em><a href="#" class="file_name">Public Folder 000001 abcdefg </a></span><p class="spacer"></p></li>
				//                                                   <li><span class="file_folder"><em><input name="" type="checkbox" value="" checked="checked" /></em><a href="#" class="file_name">Public Folder 000001 abcdefg </a></span><p class="spacer"></p></li>
				//                                                   <li><span class="file_folder"><em><input name="" type="checkbox" value="" checked="checked" /></em><a href="#" class="file_name">Public Folder 000001 abcdefg </a></span><p class="spacer"></p></li>
				//                                                   <li><span class="file_folder"><em><input name="" type="checkbox" value="" checked="checked" /></em><a href="#" class="file_name">Public Folder 000001 abcdefg </a></span><p class="spacer"></p></li>
				//                                                   <li><span class="file_folder"><em><input name="" type="checkbox" value="" checked="checked" /></em><a href="#" class="file_name">Public Folder 000001 abcdefg</a> </span><p class="spacer"></p></li>
				//                                                    <li><span class="icon_file_doc">
				//                                                    	<a href="#" title="File Infomation" class="file_icon" onclick="MM_showHideLayers(\'file_01\',\'\',\'show\')"></a>
				//                                                        <a href="#" class="file_name">Public Folder 000001 abcdefg</a> <em><input name="" type="checkbox" value="" checked="checked" /></em>
				//                                                        </span><p class="spacer"></p>
				//                                                        
				//                                                        <div id="file_01" class="file_info_layer">
				//                                                        	<div class="file_info_icon_title">
				//                                                        			<span class="icon_file_doc">
				//                                                    					<span class="file_icon"></span>
				//                                                       					 <a href="#" class="file_name">Public Folder 000001 abcdefg</a> <em><input name="" type="checkbox" value="" checked="checked" /></em>                                                       				 </span>
				//                                                                     <div class="file_info_title">
				//                                                                     	<h1>My Doc </h1>
				//                                                                        <p class="spacer"></p>
				//                                                                        <em>Version</em><span>: 5</span>
				//                                                                        <p class="spacer"></p>
				//                                                                        <em>size</em><span> : 239kb</span>                                                                     
				//                                                                     </div>
				//                                                                     <p class="spacer"></p>
				//                                                                   
				//                                                      </div>
				//                                                        <div class="file_info_icon_detail">
				//                                                                         <h2>description :</h2>
				//                                                                         text text text text text text 
				//                                                                         
				//                                                                         <h2>Tag :</h2>
				//                                                                     		<a href="#">Tag1</a><a href="#">Tag2</a><a href="#">Tag3</a>
				//                                                                             <p class="spacer"></p>
				//                                                          </div>
				//                                                                      <p class="spacer"></p>
				//                                                                      <div class="file_info_icon_detail">
				//                                                                         <h2>Last Updated : </h2>
				//                                                                         	2013-03-04 by Miss Chan
				//                                                          </div>
				//                                                                             <p align="center" class="spacer"></p>
				//                                                                             
				//                                                                             <div class="edit_bottom">
				//                                                                               <input type="button" class="formsmallbutton" value="Close" onclick="MM_showHideLayers(\'file_01\',\'\',\'hide\')"/>
				//                                                                                                  </div>
				//                                                        </div>
				//                                                    </li>
				//                                                     <li><span class="icon_file_xls">
				//                                                    	<a href="#" class="file_icon"></a>
				//                                                        <a href="#" class="file_name">Public Folder 000001 abcdefg</a>
				//                                                         <em><input name="" type="checkbox" value="" checked="checked" /></em>
				//                                                         </span><p class="spacer"></p>
				//                                                    </li>
				//                                                     <li><span class="icon_file_ppt">
				//                                                    	<a href="#" class="file_icon"></a>
				//                                                        <a href="#" class="file_name">Public Folder 000001 abcdefg</a> 
				//                                                        <em><input name="" type="checkbox" value="" checked="checked" /></em>
				//                                                        </span><p class="spacer"></p>
				//                                                    </li>
				//                                                     <li><span class="icon_file_pdf">
				//                                                    	<a href="#" class="file_icon"></a>
				//                                                        <a href="#" class="file_name">Public Folder 000001 abcdefg</a>\
				//                                                         <em><input name="" type="checkbox" value="" checked="checked" /></em>
				//                                                         </span><p class="spacer"></p>
				//                                                    </li>
				//                                                     <li><span class="icon_file_video">
				//                                                    	<a href="#" class="file_icon"></a>
				//                                                        <a href="#" class="file_name">Public Folder 000001 abcdefg</a>
				//                                                         <em><input name="" type="checkbox" value="" checked="checked" /></em>
				//                                                         </span><p class="spacer"></p>
				//                                                    </li>
				//                                                     <li><span class="icon_file_sound">
				//                                                    	<a href="#" class="file_icon"></a>
				//                                                        <a href="#" class="file_name">Public Folder 000001 abcdefg</a>
				//                                                         <em><input name="" type="checkbox" value="" checked="checked" /></em>
				//                                                         </span><p class="spacer"></p>
				//                                                    </li>
				//                                                     <li><span class="icon_file_image">
				//                                                    	<a href="#" class="file_icon"></a>
				//                                                        <a href="#" class="file_name">Public Folder 000001 abcdefg</a> 
				//                                                        <em><input name="" type="checkbox" value="" checked="checked" /></em>
				//                                                        </span><p class="spacer"></p>
				//                                                    </li>
				//                                                     <li><span class="icon_file_text">
				//                                                    	<a href="#" class="file_icon"></a>
				//                                                        <a href="#" class="file_name">Public Folder 000001 abcdefg</a> 
				//                                                        <em><input name="" type="checkbox" value="" checked="checked" /></em>
				//                                                        </span><p class="spacer"></p>
				//                                                    </li>
				//                                                     <li><span class="icon_file_htm">
				//                                                    	<a href="#" class="file_icon"></a>
				//                                                        <a href="#" class="file_name">Public Folder 000001 abcdefg</a> 
				//                                                        <em><input name="" type="checkbox" value="" checked="checked" /></em>
				//                                                        </span><p class="spacer"></p>
				//                                                    </li>
				//                                                     <li><span class="icon_file_zip">
				//                                                    	<a href="#" class="file_icon"></a>
				//                                                        <a href="#" class="file_name">Public Folder 000001 abcdefg</a>
				//                                                        <em><input name="" type="checkbox" value="" checked="checked" /></em> 
				//                                                        </span><p class="spacer"></p>
				//                                                    </li>
				//                          </ul>
				//                          <p class="spacer"></p>
				//                         <div class="file_list_bottom">
				//                    10 folders, 20 files
				//                    </div> 
				//                    </div>';
				/*$folder_rx .= '
				<!-- folder log start -->
				        	<div class="DA_file_log DA_file_log_folder">
				            
				            	<div class="DA_file_board">
				                	<div class="DA_file_board_top">	<div class="DA_file_board_top_right">	<div class="DA_file_board_top_bg"></div></div></div>
				                    <div class="DA_file_board_content">	<div class="DA_file_board_content_right">	<div class="DA_file_board_content_bg">
				                    	
				                        <div class="DA_file_detail">';*/
				//                                          <div class="DA_file_checkbox"> <input type="checkbox" /> </div>
				//                                        	<div class="DA_file_type"><a href="#" class="file_folder_l"> Folder 1</a></div>

				// $folder_rx .= '<li id="fderid'.$directoryList[$i]['DocumentID'].'">';
				//				if (!$userBrowser->isMSIE())
				//				{
				//						$folder_rx .= '<span>';
				//				}
				/*$folder_rx .= '<div class="DA_file_checkbox"><input name="DocumentID[]" id="DocumentID[]" type="checkbox" value="'.$directoryList[$i]['DocumentID'] .'" onClick="(this.checked)?$(\'#fderid'.$directoryList[$i]['DocumentID'].'\').addClass(\'selected\'):$(\'#fderid'.$directoryList[$i]['DocumentID'].'\').removeClass(\'selected\');" /></div>';
				*/
				//				if (!$userBrowser->isMSIE())
				//				{
				//						$folder_rx .= '</span>';
				//				}
				/*$folder_rx .= '<div class="DA_file_type"><a href="javascript:void()" class="file_folder_l" onClick="self.location.href=\'fileList2.php?GroupID='.$GroupID.'&FolderID='.$directoryList[$i]['DocumentID'] .'\'" >'. $directoryList[$i]['Title'] . '</a></div>';
				                       
				                            
				                        
					$folder_rx .= '</div>
				                       
				                    <p class="spacer"></p>
				                    
				                    </div></div></div>
				                	<div class="DA_file_board_bottom">	<div class="DA_file_board_bottom_right">	<div class="DA_file_board_bottom_bg"></div></div></div>
				                </div>
				            </div>
				        	<p class="spacer"></p>
				        <!-- folder log end -->';*/
				//$folder_rx .= "&nbsp; <a href=\"javascript:void()\" onClick=\"self.location.href='fileList.php?GroupID={$GroupID}&FolderID=".$directoryList[$i]['DocumentID'] ."'\" ><img src=\"/images/".$LAYOUT_SKIN."/icon_files/folder.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" />". $directoryList[$i]['Title'] . "</a> <input type=\"checkbox\" name=\"DocumentID[]\" id=\"DocumentID2[]\" value=\"".$directoryList[$i]['DocumentID'] ."\"/> &nbsp; ";
				//				$folder_rx .= '<li id="fderid'.$directoryList[$i]['DocumentID'].'">';
				//				if (!$userBrowser->isMSIE())
				//				{
				//						$folder_rx .= '<span>';
				//				}
				//				$folder_rx .= '<input name="DocumentID[]" id="DocumentID[]" type="checkbox" value="'.$directoryList[$i]['DocumentID'] .'" onClick="(this.checked)?$(\'#fderid'.$directoryList[$i]['DocumentID'].'\').addClass(\'selected\'):$(\'#fderid'.$directoryList[$i]['DocumentID'].'\').removeClass(\'selected\');" />';
				//				if (!$userBrowser->isMSIE())
				//				{
				//						$folder_rx .= '</span>';
				//				}
				//				$folder_rx .= '<a href="javascript:void()" onClick="self.location.href=\'fileList.php?GroupID='.$GroupID.'&FolderID='.$directoryList[$i]['DocumentID'] .'\'" >'. $directoryList[$i]['Title'] . '</a></li>';
				//								
			} else {
				if ($OrderBy == "filename") {
					$tempAry[$filename][] = $directoryList[$i];
				} else
					if ($OrderBy == "date") {
						$tempAry[$date][] = $directoryList[$i];
					}
			}

			//$content .= $this->DisplayDocument($docId, $displayLocation=0, $displayCheckbox=1);
		}

		if ($folder_rx != "") {
			//			if (!$userBrowser->isMSIE())
			//			{
			//				# add more style for better display in other browsers
			//				$content .= '<style type="text/css">
			//								ul.da_folder_list li {position:relative;}
			//							</style>
			//							';
			//			}
			//			$content .= '
			//					<div class="reading_board" id="resource_admin_doc_group">
			//                                      <div class="reading_board_top_left"><div class="reading_board_top_right">
			//                                                <h1><span>'.$Lang["DigitalArchive"]["Folder"].'</span></h1>
			//                                      </div></div>
			//                                        
			//                                      <div class="reading_board_left"><div class="reading_board_right">
			//                                     			<ul class="da_folder_list">
			//                                                	'.$folder_rx.'                                                 
			//                                                 </ul>
			//												 <p class="spacer"></p>
			//                                      </div></div>
			//                                      <div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
			//                                       <p class="spacer"></p>
			//                                 </div>      ';
			
			if($_REQUEST['folderTreeSelect'] == ""){
				//$tree_style = 'style="visibility:hidden;"';
				//$file_style = 'style="position: relative; left: 0px; width:100%"';
				//$tempContent = "</div>";
				$tree_style = 'style="display:none;width:0;visibility:hidden;"';
				$file_style = 'style="width:100%"';
			}
			else{
				
				//$file_style = 'style="position: relative; left: 200px;"';
				//$tree_style = 'style="position: absolute; left:0px;"';
				//$tempContent = "</div>";
				$tempClass = 'class="tree_open"';
			}
			$content .= '<div id="DA_file_list" '.$tempClass.'>
								<!--<div class="DA_tool_selectall"><input name="" type="checkbox" value="" />All</div>-->';

			$GroupID = $_REQUEST['GroupID'];
			$SelectedGroupID = $_REQUEST['SelectedGroupID'];
			$array = $lda->Get_Folder_Hierarchy_File_Count_Array($GroupID);
			
			
			
			if (sizeof($array) > 0) {
				$content .= '<div class="tree_list" id="tree_list" '.$tree_style.'>' . "\n";

				$content .= $this->Get_Quickview_Folder_Div($array, 0, $FolderID = 0, "", "", $GroupID, $SelectedGroupID, FALSE);

				$content .= '</div>'.$tempContent . "\n";
			}

			$content .= '<div class="file_list" id="file_list" '.$file_style.'>
			                			<div class="DA_tool_selectall">
			                			<span id="checkboxAllDiv"><label for="CheckAll">' . $Lang['General']['All'] . '</label><input type="checkbox" id="CheckAll" onClick = "(this.checked)?(setChecked(1,this.form,\'DocumentID[]\',1), setChecked(1,this.form,\'DocumentID1[]\',1)):(setChecked(0,this.form,\'DocumentID[]\',1), setChecked(0,this.form,\'DocumentID1[]\',1)); (this.checked)?($(\'.da_folder_list li\').addClass(\'selected\'), $(\'.file_info_icon_title span\').addClass(\'selected\')):($(\'.da_folder_list li\').removeClass(\'selected\'), $(\'.file_info_icon_title span\').removeClass(\'selected\'));"/></span>
			                    </div>
			                    <!-- Log list start-->
			                    		
			                    		
			                    ' . $folder_rx;
		}

		//$content = $folder_rx;
		$returnAry = array ();

		if ($NoOfRecord > 0) {
			if ($OrderBy2 == "ASC") {
				if (count($tempAry) > 0)
					ksort($tempAry);
			} else {
				if (count($tempAry) > 0)
					krsort($tempAry);
			}

			if (count($tempAry) > 0) {
				foreach ($tempAry as $_ary) {
					for ($a = 0; $a < count($_ary); $a++) {
						$docId = $_ary[$a]['DocumentID'];

						//						$content .= '<div class="DA_file_log">
						//					<div class="DA_file_num"><span>'.$fileNo.'.</span></div>';
						$content .= $this->DisplayDocument_Icon($docId, $displayLocation = 0, $displayCheckbox = 1);
					}
				}
			} else {

				$content .= $this->DisplayDocument_Icon("", $displayLocation = 0, $displayCheckbox = 0);
			}
		}
		if ($NoOfRecord == 0) {

			$content .= $this->DisplayDocument_Icon("", $displayLocation = 0, $displayCheckbox = 0);
		}

		$content .= '</ul>
		                          <p class="spacer"></p>
		                         <div class="file_list_bottom">
		                    ' . $NoOfFolder . ' ' . $Lang["DigitalArchive"]["SubFolders2"] . ', ' . ($NoOfRecord - $NoOfFolder) . ' ' . $Lang["DigitalArchive"]["Files"] . '
		                    </div> 
		                    </div>';

		$editableRecordIdByGroupID = $lda->Get_Editable_Record_ID_By_GroupID($GroupID, $FolderID);

		$x .= '<script language="javascript">';
		$x .= 'var editableAry = new Array();';

		for ($i = 0; $i < sizeof($editableRecordIdByGroupID); $i++) {

			$x .= 'editableAry[' . $i . '] = "' . $editableRecordIdByGroupID[$i] . '"' . "\n";
		}
		$x .= '</script>';

		return $content . $x;
	}

	function Get_Tag_Record_Table($TagID = "", $OrderBy = "", $OrderBy2 = "") {
		if (!isset ($TagID) || $TagID == "")
			return;

		$lda = new libdigitalarchive();
		$data = $lda->getAdminDocInfoByTagID($TagID);

		$NoOfRecord = count($data);
		$tempAry = array ();

		# prepare $tempAry for sorting
		for ($i = 0; $i < $NoOfRecord; $i++) {
			if ($OrderBy == "filename") {
				$tempAry[STRTOUPPER($data[$i]['Title'])][] = $data[$i];
			} else
				if ($OrderBy == "date") {
					$date = ($data[$i]['DateModified'] == "0000-00-00 00:00:00") ? $data[$i]['DateInput'] : $data[$i]['DateModified'];

					$tempAry[$date][] = $data[$i];
				} else
					if ($OrderBy == "location") {
						$path = $this->Get_Folder_Navigation($data[$i]['ParentFolderID'], " > ", $isListTable = 0, $withGroupName = 1, $data[$i]['GroupID']);
						$tempAry[$path][] = $data[$i];
					}
		}
		$url = preg_replace('/\?.*/', '', basename($_SERVER["HTTP_REFERER"]));//Modified for new layout
		
		if ($NoOfRecord > 0) {
			if ($OrderBy2 == "ASC") {
				ksort($tempAry);
			} else {
				krsort($tempAry);
			}

			foreach ($tempAry as $_ary) {
				for ($a = 0; $a < count($_ary); $a++) {
					$docId = $_ary[$a]['DocumentID'];
					
					if($url != "fileList_Icon.php"){//Modified for new layout
						$content .= $this->DisplayDocument($docId, $displayLocation = 1, $displayCheckbox = 1);
					}
					else{//Modified for new layout
						$content .= '<ul class="da_folder_list">';
						$content .= $this->DisplayDocument_Icon($docId, $displayLocation = 0, $displayCheckbox = 1);
						$content .= '</ul>';
					}
						
				}
			}
		}
		if ($NoOfRecord == 0) {
			if($url != "fileList_Icon.php"){//Modified for new layout
				$content .= $this->DisplayDocument("", $displayLocation = 0, $displayCheckbox = 0);
			}
			else{//Modified for new layout
				$content .= '<ul class="da_folder_list">';
				$content .= $this->DisplayDocument_Icon("", $displayLocation = 0, $displayCheckbox = 0);
				$content .= '</ul>';
			}
		}

		return $content;
	}

	function Get_Subject_eResources_Group_DBTable($keyword = '') {

		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $image_path;
		global $ck_Digital_Archive_Subject_eResources_Group_page_size, $ck_Digital_Archive_Subject_eResources_Group_page_number;
		global $ck_Digital_Archive_Subject_eResources_Group_order, $ck_Digital_Archive_Subject_eResources_Group_page_field;

		include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
		include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
		include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

		$lda = new libdigitalarchive();

		$linterface = new interface_html();

		if (isset ($ck_Digital_Archive_Subject_eResources_Group_page_size) && $ck_Digital_Archive_Subject_eResources_Group_page_size != '') {
			$PageSize = $ck_Digital_Archive_Subject_eResources_Group_page_size;
		}

		if (isset ($ck_Digital_Archive_Subject_eResources_Group_page_number) && $ck_Digital_Archive_Subject_eResources_Group_page_number != '') {
			$PageNumber = $ck_Digital_Archive_Subject_eResources_Group_page_number;
		}

		if (isset ($ck_Digital_Archive_Subject_eResources_Group_page_order) && $ck_Digital_Archive_Subject_eResources_Group_page_order != '') {
			$Order = $ck_Digital_Archive_Subject_eResources_Group_page_order;
		}

		if (isset ($ck_Digital_Archive_Subject_eResources_Group_page_field) && $ck_Digital_Archive_Subject_eResources_Group_page_field != '') {
			$SortField = $ck_Digital_Archive_Subject_eResources_Group_page_field;
		}

		# Default Table Settings
		$PageNumber = ($PageNumber == '') ? 1 : $PageNumber;
		$PageSize = ($PageSize == '') ? 20 : $PageSize;
		$Order = ($Order == '') ? 1 : $Order;
		$SortField = ($SortField == '') ? 0 : $SortField;

		# TABLE INFO
		$libdbtable = new libdbtable2007($SortField, $Order, $PageNumber);
		$libdbtable->sql = $lda->Get_Subject_eResources_Group_Sql('', '', '', $keyword);
		$libdbtable->IsColOff = "IP25_table";
		$libdbtable->field_array = array (
			"GroupCode",
			"GroupTitle",
			"Description",
			"Subject",
			"Form",
			"NoOfMember",
			"CheckBox"
		);
		$libdbtable->no_col = sizeof($libdbtable->field_array) + 2; // 2 means column #, Checkbox
		$libdbtable->title = "";
		$libdbtable->column_array = array (
			0,
			0,
			0,
			0,
			0,
			0,
			0
		);
		$libdbtable->wrap_array = array (
			0,
			0,
			0,
			0,
			0,
			0,
			0
		);

		$libdbtable->page_size = $PageSize;
		$libdbtable->fieldorder2 = " ,GroupCode, GroupTitle";

		# Table Action Button
		$BtnArr = array ();
		$BtnArr[] = array (
			'edit',
			'javascript:checkEdit(document.form1,\'GroupID[]\',\'new.php\');'
		);
		$BtnArr[] = array (
			'delete',
			'javascript:checkRemove(document.form1,\'GroupID[]\',\'remove.php\');'
		);
		$table_tool = $linterface->Get_DBTable_Action_Button_IP25($BtnArr);

		# Table Column
		$pos = 0;
		$libdbtable->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
		$libdbtable->column_list .= "<th width='10%'>" . $libdbtable->column_IP25($pos++, $Lang['DigitalArchive']['GroupCode']) . "</td>\n";
		$libdbtable->column_list .= "<th width='15%'>" . $libdbtable->column_IP25($pos++, $Lang['DigitalArchive']['GroupName']) . "</td>\n";
		$libdbtable->column_list .= "<th width='20%'>" . $libdbtable->column_IP25($pos++, $Lang["DigitalArchive"]["Description"]) . "</td>\n";
		$libdbtable->column_list .= "<th width='15%'>" . $libdbtable->column_IP25($pos++, $Lang["DigitalArchive"]["Subject"]) . "</td>\n";
		$libdbtable->column_list .= "<th width='20%'>" . $libdbtable->column_IP25($pos++, $Lang['General']['Form']) . "</td>\n";
		$libdbtable->column_list .= "<th width='15%'>" . $libdbtable->column_IP25($pos++, $Lang['DigitalArchive']['NoOfMembers']) . "</td>\n";
		$libdbtable->column_list .= "<th class='tableTitle'>" . $libdbtable->check("GroupID[]") . "</th>\n";
		$libdbtable->no_col = $pos +2;

		$DBTable = '';
		$DBTable .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">' . "\n";
		$DBTable .= '<tr>' . "\n";
		$DBTable .= '	<td align="right" valign="bottom">' . $table_tool . '</td>' . "\n";
		$DBTable .= '</tr>' . "\n";
		$DBTable .= '<tr>' . "\n";
		$DBTable .= '	<td align="center" valign="top">' . $libdbtable->display() . '</td>' . "\n";
		$DBTable .= '</tr>' . "\n";
		$DBTable .= '</table>' . "\n";

		$DBTable .= '<input type="hidden" name="pageNo" value="' . $libdbtable->pageNo . '" />';
		$DBTable .= '<input type="hidden" name="order" value="' . $libdbtable->order . '" />';
		$DBTable .= '<input type="hidden" name="field" value="' . $libdbtable->field . '" />';
		$DBTable .= '<input type="hidden" name="page_size_change" value="" />';
		$DBTable .= '<input type="hidden" name="numPerPage" value="' . $libdbtable->page_size . '" />';

		return $DBTable;

	}

	function Get_Subject_eResources_Group_Member_DBTable($groupID, $keyword = '') {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $image_path;
		global $ck_Digital_Archive_Subject_eResources_Group_Member_page_size, $ck_Digital_Archive_Subject_eResources_Group_Member_page_number;
		global $ck_Digital_Archive_Subject_eResources_Group_Member_page_order, $ck_Digital_Archive_Subject_eResources_Group_Member_page_field;

		include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
		include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
		include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

		$lda = new libdigitalarchive();

		$linterface = new interface_html();
		$Keyword = trim($Keyword);

		if (isset ($ck_Digital_Archive_Subject_eResources_Group_Member_page_size) && $ck_Digital_Archive_Subject_eResources_Group_Member_page_size != '') {
			$PageSize = $ck_Digital_Archive_Subject_eResources_Group_Member_page_size;
		}

		if (isset ($ck_Digital_Archive_Subject_eResources_Group_Member_page_number) && $ck_Digital_Archive_Subject_eResources_Group_Member_page_number != '') {
			$PageNumber = $ck_Digital_Archive_Subject_eResources_Group_Member_page_number;
		}

		if (isset ($ck_Digital_Archive_Subject_eResources_Group_Member_page_order) && $ck_Digital_Archive_Subject_eResources_Group_Member_page_order != '') {
			$Order = $ck_Digital_Archive_Subject_eResources_Group_Member_page_order;
		}

		if (isset ($ck_Digital_Archive_Subject_eResources_Group_Member_page_field) && $ck_Digital_Archive_Subject_eResources_Group_Member_page_field != '') {
			$SortField = $ck_Digital_Archive_Subject_eResources_Group_Member_page_field;
		}

		# Default Table Settings
		$PageNumber = ($PageNumber == '') ? 1 : $PageNumber;
		$PageSize = ($PageSize == '') ? 20 : $PageSize;
		$Order = ($Order == '') ? 1 : $Order;
		$SortField = ($SortField == '') ? 0 : $SortField;

		# TABLE INFO
		$libdbtable = new libdbtable2007($SortField, $Order, $PageNumber);
		$libdbtable->sql = $lda->Get_Subject_eResources_Group_Member_Sql($groupID, '', $keyword);
		$libdbtable->IsColOff = "IP25_table";

		$libdbtable->field_array = array (
			"UserName",
			"CheckBox"
		);
		$libdbtable->no_col = sizeof($libdbtable->field_array) + 2; // 2 means column #, Checkbox
		$libdbtable->title = "";
		$libdbtable->column_array = array (
			0,
			0
		);
		$libdbtable->wrap_array = array (
			0,
			0
		);

		$libdbtable->page_size = $PageSize;
		$libdbtable->fieldorder2 = " ,UserName";

		# Table Action Button
		$BtnArr = array ();
		//$BtnArr[] = array('edit', 'javascript:checkEdit(document.form1,\'user_id[]\',\'new_member.php?groupId='.$groupID.'\');');
		$BtnArr[] = array (
			'delete',
			'javascript:checkRemove(document.form1,\'user_id[]\',\'remove_member.php?groupId=' . $groupID . '\');'
		);

		$table_tool = $linterface->Get_DBTable_Action_Button_IP25($BtnArr);

		# Table Column
		$pos = 0;

		$libdbtable->column_list .= "<th width='3%' class='tabletoplink'>#</td>\n";
		$libdbtable->column_list .= "<th width='92%'>" . $libdbtable->column_IP25($pos++, $Lang['General']['Name']) . "</td>\n";
		$libdbtable->column_list .= "<th width='5%' class='tableTitle'>" . $libdbtable->check("user_id[]") . "</th>\n";
		$libdbtable->no_col = $pos +2;

		$DBTable = '';
		$DBTable = '<table width="100%" border="0" cellspacing="0" cellpadding="0">' . "\n";
		$DBTable .= '<tr>';
		$DBTable .= '	<td align="right" valign="bottom">' . $table_tool . '</td>' . "\n";
		$DBTable .= '</tr>';
		$DBTable .= '<tr>';
		$DBTable .= '	<td align="center" valign="top">' . $libdbtable->display() . '</td>';
		$DBTable .= '</tr>';
		$DBTable .= '</table>';

		$DBTable .= '<input type="hidden" name="pageNo" value="' . $libdbtable->pageNo . '" />';
		$DBTable .= '<input type="hidden" name="order" value="' . $libdbtable->order . '" />';
		$DBTable .= '<input type="hidden" name="field" value="' . $libdbtable->field . '" />';
		$DBTable .= '<input type="hidden" name="page_size_change" value="" />';
		$DBTable .= '<input type="hidden" name="GroupID" value="' . $groupID . '" />';
		$DBTable .= '<input type="hidden" name="numPerPage" value="' . $libdbtable->page_size . '" />';

		return $DBTable;

	}

	function Get_Quickview_Folder_Div($array = array (), $levelCount = 0, $FolderID = "", $NoReload = "", $moveTo = "", $GroupID = "", $selectedGroupID = "", $showTitle=true) {
		global $Lang, $lda;
		
		if($showTitle) {
			$groupInfo = $lda->getGroupInfo($GroupID);
	
			$returnText = '<div class="pop_folder">' . "\n";
			$returnText .= '<div class="pop_title">
									    	<h1><a href="javascript:void(0);" onclick="' . (($GroupID == $selectedGroupID) ? "document.form1.GroupID.value=$GroupID;document.form1.FolderID.value=0;Get_Directory_List_Table();" : "self.location.href='fileList_Icon.php?GroupID=$GroupID&FolderID=0'") . '">' . $groupInfo['GroupTitle'] . '</a>&nbsp;(' . $array[0]['FileCount'] .' '.$Lang["DigitalArchive"]["Files"] .')</h1>
									        <div class="folder_tool">
									        	<a class="btn_folder_expand" href="javascript:void(0);" onclick="expandCollapseAllFolders(\'expandAll\');">' . $Lang['DigitalArchive']['ExpandAll'] . '</a>
									            <a class="btn_folder_collapse " href="javascript:void(0);" onclick="expandCollapseAllFolders(\'collapseAll\');">' . $Lang['DigitalArchive']['CollapseAll'] . '</a>
									        </div>
									    </div>' . "\n";
		}
		if (sizeof($array) > 0) {
			$returnText .= '<div class="folder_list">' . "\n";
			
			//$tempReturnText = $this->Get_Quickview_Folder_Tree($array, $levelCount +1, $FolderID, $NoReload, $moveTo, $GroupID, $selectedGroupID);
			//debug_pr($tempReturnText);
			if($lda-> Has_Folder_In_Group($GroupID, 0) == FALSE)
				$returnText .= "* ".$Lang['DigitalArchive']['NoSubfolder'];
			else
				$returnText .= $this->Get_Quickview_Folder_Tree($array, $levelCount +1, $FolderID, $NoReload, $moveTo, $GroupID, $selectedGroupID,$showTitle);
			
			$returnText .= '</div>' . "\n";
		}
		
		if($showTitle)
			$returnText .= '</div>' . "\n";

		$returnText .= '<script type="text/javascript" language="JavaScript">' . "\n";
		$returnText .= 'function expandCollapseFolder(obj)
									{
										var thisObj = $(obj);
										var curClassName = thisObj.attr(\'className\');
										
										if(curClassName == \'btn_folder_control_collapse\') {
											thisObj.siblings(\'ul\').hide();
											thisObj.attr(\'className\',\'btn_folder_control_expand\');
											if(thisObj.siblings(\'a.folder_selected\').length == 0){	
													thisObj.siblings(\'a.folder_open\').attr(\'className\',\'folder_close\');
												}
												else{
													thisObj.siblings(\'a.folder_selected\').attr(\'className\',\'folder_close folder_selected\');
												}	
										}else if(curClassName == \'btn_folder_control_expand\') {
											thisObj.siblings(\'ul\').show();
											thisObj.attr(\'className\',\'btn_folder_control_collapse\');
											if(thisObj.siblings(\'a.folder_selected\').length == 0){		
													thisObj.siblings(\'a.folder_close\').attr(\'className\',\'folder_open\');
												}
												else{	
													thisObj.siblings(\'a.folder_selected\').attr(\'className\',\'folder_open folder_selected\');
												}
										}
									}' . "\n";
		$returnText .= 'function expandCollapseAllFolders(action)
									{
										if(action == \'collapseAll\'){
											var objs = $(\'ul.folder_tree a.btn_folder_control_collapse\');
											objs.each(function(i){
												var thisObj = $(this);
												thisObj.siblings(\'ul\').hide();
												thisObj.attr(\'className\',\'btn_folder_control_expand\');
												thisObj.siblings(\'a.folder_open\').attr(\'className\',\'folder_close\');	
											});
										}else if(action == \'expandAll\') {
											var objs = $(\'ul.folder_tree a.btn_folder_control_expand\');
											objs.each(function(i){
												var thisObj = $(this);
												thisObj.siblings(\'ul\').show();
												thisObj.attr(\'className\',\'btn_folder_control_collapse\');	
												thisObj.siblings(\'a.folder_close\').attr(\'className\',\'folder_open\');				
											});
										}
									}' . "\n";
		$returnText .= '</script>' . "\n";

		return $returnText;
	}

	function Get_Quickview_Folder_Tree($array = array (), $levelCount = 0, $FolderID = "", $NoReload = "", $moveTo = "", $GroupID = "", $selectedGroupID = "", $showTitle=true) {
		global $Lang;

		if (count($array) > 0) {
			if ($levelCount == 1) {
				$returnText .= '<ul ' . ($levelCount == 1 ? 'class="folder_tree"' : '') . '>' . "\n";
			}
			foreach ($array as $folder_id => $folder_ary) {

				$parent_folder_id = $folder_ary['ParentFolderID'];
				$title = $folder_ary['Title'];
				$file_count = $folder_ary['FileCount'];
				$subfolders = $folder_ary['Subfolders'];
				$subfolder_count = count($subfolders);

				$onclick = (($GroupID == $selectedGroupID) ? "document.getElementById('status_option_value').value=0;document.form1.GroupID.value=" . $GroupID . ";document.form1.FolderID.value=" . $folder_id . ";Get_Directory_List_Table();" : "self.location.href='".(($showTitle==TRUE)?"fileList_Icon.php": $_SERVER["PATH_INFO"])."?GroupID=$GroupID&FolderID=$folder_id&folderTreeSelect=".(($showTitle==TRUE)?"":"selected")."'");
				$is_last_folder = false;

				if ($folder_id != 0) {
					$returnText .= '<li ' . ($is_last_folder ? 'class="last"' : '') . '>' . "\n";
					if ($subfolder_count > 0) {
						$returnText .= '<a class="btn_folder_control_collapse" href="javascript:void(0);" onclick="expandCollapseFolder(this);"></a>' . "\n";
					}
					if($_POST['FolderID'] == $folder_id){
						$returnText .= '<a class="' . ($subfolder_count == 0 ? 'folder_close folder_selected' : 'folder_open folder_selected') . '" href="javascript:void(0);" onclick="' . $onclick . '">' . $title . '<span>&nbsp;(' . $file_count . ')</span></a>' . "\n";
					}
					else
						$returnText .= '<a class="' . ($subfolder_count == 0 ? 'folder_close' : 'folder_open') . '" href="javascript:void(0);" onclick="' . $onclick . '">' . $title . '<span>&nbsp;(' . $file_count . ')</span></a>' . "\n";
				}
				if ($subfolder_count > 0) {
					if ($folder_id != 0) {
						$returnText .= '<ul>' . "\n";
					}
					for ($i = 0; $i < $subfolder_count; $i++) {
						$returnText .= $this->Get_Quickview_Folder_Tree($subfolders[$i], $levelCount +1, $FolderID, $NoReload, $moveTo, $GroupID, $selectedGroupID, $showTitle);
					}
					if ($folder_id != 0) {
						$returnText .= '</ul>' . "\n";
					}
				}
				if ($folder_id != 0) {
					$returnText .= '</li>' . "\n";
				}

			}
			if ($levelCount == 1) {
				$returnText .= '</ul>' . "\n";
			}
		}

		return $returnText;
	}


    function Get_AccessRight_Import_confirm($data)
    {
        global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $lda;

        $PAGE_NAVIGATION[] = array($Lang['Btn']['Import']);

        # step information
        $STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
        $STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 1);
        $STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);

        $x = '<br/>
            <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>'.$this->GET_NAVIGATION($PAGE_NAVIGATION).'</td>
                </tr>
                <tr>
                    <td>'.$this->GET_STEPS($STEPS_OBJ).'</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <table width="90%" border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td class="formfieldtitle" width="30%" align="left">'.$Lang['General']['TotalRecord'].'</td>
                    <td class="tabletext">'.sizeof($data).'</td>
                </tr>';
        if (sizeof($data) > 0) {
			list($SuccessArray, $ErrorArray) = $lda->Import_AccessRight_Record_To_Temp($data);

            $x .= '
					<tr>
						<td class="formfieldtitle" width="30%" align="left">'.$Lang['General']['SuccessfulRecord'].'</td>
						<td class="tabletext">'.(sizeof($data)-sizeof($ErrorArray)).'</td>
					</tr>
					<tr>
						<td class="formfieldtitle" width="30%" align="left">'.$Lang['General']['FailureRecord'].'</td>
						<td class="tabletext">'.sizeof($ErrorArray).'</td>
					</tr>
				</table>
				';

            $x .= '<table width="95%" border="0" cellpadding="3" cellspacing="0">
						<tr>
							<td class="tablebluetop tabletopnolink" width="10">#</td>
							<td class="tablebluetop tabletopnolink">'.$Lang['DigitalArchive']['GroupCode'].'</td>
							<td class="tablebluetop tabletopnolink">'.$Lang['AccountMgmt']['LoginID'].'</td>
							<td class="tablebluetop tabletopnolink">'.$Lang['StaffAttendance']['StaffName'].'</td>
				            <td class="tablebluetop tabletopnolink">'.$Lang['DigitalArchive']['Admin'].'</td>';


            $x .= '<td class="tablebluetop tabletopnolink">'.$Lang['General']['Error'].'</td>
						</tr>';

			$count = 0;
            for ($i=0; $i< sizeof($ErrorArray); $i++) {
				$css_i = ($count % 2) ? "2" : "";
				$count++;
                $x .= '
							<tr>
								<td valign="top" class="tablebluerow'.$css_i.'" width="10">
									'.$ErrorArray[$i]['RowNumber'].'
								</td>
								<td valign="top" class="tablebluerow'.$css_i.'">
									'.$ErrorArray[$i]['RecordDetail'][0].'
								</td>
								<td valign="top" class="tablebluerow'.$css_i.'">
									'.$ErrorArray[$i]['RecordDetail'][1].'
								</td>
								<td valign="top" class="tablebluerow'.$css_i.'">
									'.$ErrorArray[$i]['RecordDetail'][2].'
								</td>
								<td valign="top" class="tablebluerow'.$css_i.'">
									'.$ErrorArray[$i]['RecordDetail'][3].'
								</td>';

                $x .= '<td valign="top" class="tablebluerow'.$css_i.'">
									<font style="color:red;">'.$ErrorArray[$i]['ErrorMsg'].'</font>
								</td>
							</tr>';
            }

			for ($i=0; $i< sizeof($SuccessArray); $i++) {
				$css_i = ($count % 2) ? "2" : "";
				$count++;
				$x .= '
							<tr>
								<td valign="top" class="tablebluerow'.$css_i.'" width="10">
									'.$SuccessArray[$i]['RowNumber'].'
								</td>
								<td valign="top" class="tablebluerow'.$css_i.'">
									'.$SuccessArray[$i]['RecordDetail'][0].'
								</td>
								<td valign="top" class="tablebluerow'.$css_i.'">
									'.$SuccessArray[$i]['RecordDetail'][1].'
								</td>
								<td valign="top" class="tablebluerow'.$css_i.'">
									'.$SuccessArray[$i]['RecordDetail'][2].'
								</td>
								<td valign="top" class="tablebluerow'.$css_i.'">
									'.$SuccessArray[$i]['RecordDetail'][3].'
								</td>';

				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">
									
								</td>
							</tr>';
			}

            $x .= '</table>';

            $x .= '
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
					<td class="dotline"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td align="center">';
            if (sizeof($ErrorArray)== 0)
                $x .= $this->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "window.location='access_right_import_update.php';");

            $x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='access_right_import.php';");
            $x .= '</td>
				</tr>
				</table>';
        } else {
            $x .= '</table>';

            $x .= '
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
					<td class="dotline"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td align="center">';
            $x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='access_right_import.php';");
            $x .= '</td>
				</tr>
				</table>';
        }

        return $x;
    }


    function Get_AccessRight_Import_Finish_Page()
    {
        global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $lda;

        $Result = $lda->Finalize_Import_AccessRight();

        $PAGE_NAVIGATION[] = array($Lang['Btn']['Import']);

        # step information
        $STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
        $STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
        $STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 1);


        $x .= '<br />
					<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>'.$this->GET_NAVIGATION($PAGE_NAVIGATION).'</td>
						</tr>
						<tr>
							<td>'.$this->GET_STEPS($STEPS_OBJ).'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="center">
							'.$Result.' '.$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully'].'
							</td>
						</tr>
						<tr>
							<td class="dotline">
								<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" />
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="center">
								'.$this->GET_ACTION_BTN($Lang['Btn']['Back'],"button","window.location='access_right.php';").'
							</td>
						</tr>
					</table>';

        return $x;
    }
}
?>