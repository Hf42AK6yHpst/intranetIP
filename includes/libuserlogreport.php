<?php 
// Modifying by: Philips

// #################################### !!! IMPORTANT !!! #####################################
// #### If upload this file to client site before ip.2.5.6.10.1, #####
// #### please upload with the addon schema and update the schema in client site #####
// #################################### !!! IMPORTANT !!! #####################################

// ########### Change Log [start] ###############

// ########### Change Log [end] #################
class libuserlogreport extends libuser{
    var $type = array(
        '0' => 'login',
        '1' => 'account',
        '2' => 'report',
        'login' => '0',
        'account' => '1',
        'report' => '2'
    );
    var $action = array(
        '0' => 'export',
        '1' => 'html',
        '2' => 'pdf',
        'export' => '0',
        'html' => '1',
        'pdf' => '2'
    );
    function getAllLogReport($filterOpts = array(), $paging = array(), $orderArr = array()){
        $cols = "ilr.UserID, iu.EnglishName, iu.ChineseName, iu.UserLogin, ilr.Rtype,
                     ilr.Action, ilr.LogDate, ilr.LogTime, ilr.IPAddress";
        $table = "INTRANET_LOG_REPORT ilr";
        $users = "INTRANET_USER iu ON ilr.UserID = iu.UserID";
        
        // Fitering
        $cond = array();
        if($filterOpts['DateStart']!=''&& $filterOpts['DateEnd']){
            $cond['daterange'] = "AND (ilr.LogDate >= '$filterOpts[DateStart]' AND  ilr.LogDate <= '$filterOpts[DateEnd]') ";
        }
        if($filterOpts['UserName']!=''){
            $cond['name'] = "AND (iu.ChineseName LIKE '%$filterOpts[UserName]%' OR iu.EnglishName LIKE '%$filterOpts[UserName]%') ";
        }
        if($filterOpts['Rtype'] != '' && $filterOpts['Rtype'] != -1){
            $frtype = is_numeric($filterOpts['Rtype']) ? $filterOpts['Rtype'] : $this->getReportType($filterOpts['Rtype']);
            $cond['type'] = "AND ilr.Rtype = '$frtype' ";
        }
        if($filterOpts['Action'] != '' && $filterOpts['Action'] != -1){
            $faction = is_numeric($filterOpts['Action']) ? $filterOpts['Action'] : $this->getReportAction($filterOpts['Action']);
            $cond['action'] = "AND ilr.Action = '$faction' ";
        }
        
        // Order
        if($orderArr['Field']!=''){
            $order = $orderArr['Order'] == 0 ? 'asc' : 'desc';
            $orderCond = 'ORDER BY ' . $orderArr['Field'] . ' ' . $order;
        }
        
        // Paging
        if(empty($paging)){
            $paging['pageNo'] = 1;
            $paging['pageSize'] = 50;
        }
        if(!$paging['all']){
            $pn = $paging['pageNo'];
            $ps = $paging['pageSize'];
            $limitCond = 'LIMIT ' . (($pn-1) * $ps ) . ', ' . $ps;
        } else {
            $limitCond = '';
        }
        $sql = "SELECT $cols
                FROM $table 
                INNER JOIN $users
                WHERE 1
                $cond[daterange]
                $cond[name]
                $cond[type]
                $cond[action]
                $orderCond
                $limitCond
                ";
        $result = $this->returnArray($sql);
        //debug_pr($sql);die();
        for($i=0; $i<sizeof($result);$i++){
            $result[$i]['Rtype'] = $this->getReportType($result[$i]['Rtype']);
            $result[$i]['Action'] = $this->getReportAction($result[$i]['Action']);
        }
        $sql = "SELECT COUNT(*)
                FROM $table
                INNER JOIN $users
                WHERE 1
                $cond[daterange]
                $cond[name]
                $cond[type]
                $cond[action]
                ";
                $rowResult = $this->returnArray($sql);
                $result['PAGEMAX'] = $rowResult[0][0];
        return $result;
    }
    function insertLogReport($dataArr){
        $cols = "UserID, Rtype, Action, LogDate, LogTime, IPAddress";
        
        $IPAddress = $_SERVER['HTTP_CLIENT_IP']?$_SERVER['HTTP_CLIENT_IP']:($_SERVER['HTTP_X_FORWARDE‌​D_FOR']?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']);
        
        $drtype = $this->getReportType($dataArr['Rtype']);
        $daction = $this->getReportAction($dataArr['Action']);
        
        $values = "'$_SESSION[UserID]', '$drtype',  '$daction', '$dataArr[Date]', '$dataArr[Time]', '$IPAddress'";
        
        $table = "INTRANET_LOG_REPORT";
        
        $sql = "INSERT INTO $table ($cols) VALUES($values)";
        
        $result = $this->db_db_query($sql);
        return $result;
    }
    function getReportType($str){
        return $this->type[strtolower($str)];
    }
    function getReportAction($str){
        return $this->action[strtolower($str)];
    }
    function getReportTypeSelection($select = -1, $name = '', $onchange = ''){
        global $Lang;
        $count = sizeof($this->type) / 2;
        $selectionbox = "<select name='$name' onchange='$onchange'>";
        $selectionbox .= "<option value='-1' " . ($select == -1 ? 'selected' : '') . ">" . $Lang['SystemSetting']['AccountLog']['All'] . "</option>";
        for($i=0;$i<$count;$i++){
            $selected  = ($select == $i) ? 'selected' : '';
            $selectionbox .= "<option value='$i' $selected>".$Lang['SystemSetting']['ReportLog']['Rtype'][$this->type[$i]]."</option>";
        }
        $selectionbox .= "</select>";
        return $selectionbox;
    }
    function getReportActionSelection($select = -1, $name = '', $onchange = ''){
        global $Lang;
        $count = sizeof($this->type) / 2;
        $selectionbox = "<select name='$name' onchange='$onchange'>";
        $selectionbox .= "<option value='-1' " . ($select == -1 ? 'selected' : '') . ">" . $Lang['SystemSetting']['AccountLog']['All'] . "</option>";
        for($i=0;$i<$count;$i++){
            $selected  = ($select == $i) ? 'selected' : '';
            $selectionbox .= "<option value='$i' $selected>".$Lang['SystemSetting']['ReportLog']['Action'][$this->action[$i]]."</option>";
        }
        $selectionbox .= "</select>";
        return $selectionbox;
    }
}
?>