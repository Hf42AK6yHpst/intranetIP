<?php
# using: 

######### Change Log [Start] ###########
#	Date:	2020-11-16 [Philips]
#			modified getDumpDataWithSeparator_utf(), added KISClassType
#
#	Date:	2017-08-22 [Carlos]
#			modified getDumpDataWithSeparator_utf(), get children class name and class number from year class info, not INTRANET_USER.ClassName,ClassNumber, to avoid left students with same class info with coming year students.
#
#	Date:	2015-12-11 [Omas] [ip2.5.7.1.1]
#			modified getDumpDataWithSeparator_utf(),getDumpDataWithSeparator() - replace deprecated split() by explode() for PHP 5.4
#
#	Date:	2014-11-20 [Bill]
#			modified getDumpDataWithBouns() to return UserID of matched students, getDumpDataWithSeparator_utf(), to export Guardian Info
#
#	Date:	2013-12-13 [Fai]
#			Add a cust attribute "stayOverNight" with $plugin['medical']
#
#	Date:	2013-04-26 [Rita]
#			amend getDumpDataWithSeparator_utf() - fix export line-break issue
#
#	Date:	2013-03-22 [Rita]
#			modified getDumpDataWithBouns(), DOB remove time
#
#	Date:	2012-08-02 [Bill Mak]
#			Add getDumpDataWithBouns(), present the data from INTRANET_USER join custom table on UserID
#
#	Date:	2012-06-12 [YatWoon]
#			update getDumpDataWithSeparator_utf(), for parent export, if user don't select UserLogin, then no student info is retrieved.
#
#	Date:	2011-01-13 [YatWoon]
#			update getDumpDataWithSeparator_utf(), add "Student Info" to parent export csv
#
#	Date:	2010-09-14 [Henry Chow]
#			update getDumpDataWithSeparator_utf(), perform Array_merge only if $row2 is not empty
#
#	Date:	2010-04-19 YatWoon
#			update getDumpDataWithSeparator_utf()
#			for "LaiShan" project, add parent/guardian name to student export csv
#
######### Change Log [End] ###########


include_once($intranet_root."/includes/libgrouping.php");
include_once($intranet_root."/includes/librole.php");

class libdbdump extends libdb{
	
	var $FieldArray;
	var $Table;
	var $FilterSQL;
	var $FieldsTerminator;
	
	# ------------------------------------------------------------------------------------
	
	function libdbdump(){
		$this->libdb();
	}
	
	# ------------------------------------------------------------------------------------
	
	function undo_htmlspecialchars($string){
		$string = str_replace("&amp;", "&", $string);
		$string = str_replace("&quot;", "\"", $string);
		$string = str_replace("\"", "\"\"", $string);
		$string = str_replace("&lt;", "<", $string);
		$string = str_replace("&gt;", ">", $string);
		return $string;
	}
	
	function setFieldArray($FieldArray){
		$this->FieldArray = $FieldArray;
	}
	
	function setBounsFieldArray($BounsFieldArray){
		$this->BounsFieldArray = $BounsFieldArray;
	}
	
	function setTable($Table){
		$this->Table = $Table;
	}
	
	function setFilterSQL($FilterSQL){
		$this->FilterSQL = $FilterSQL;
	}
	
	function setFieldsTerminator($FieldsTerminator){
		$this->FieldsTerminator = $FieldsTerminator;
	}
	
	# ------------------------------------------------------------------------------------
	
	/* Old
	 function getDumpData(){
	 $sql = "SELECT ".implode(",", $this->FieldArray)." FROM ".$this->Table." ".$this->FilterSQL;
	 return $this->returnArray($sql,sizeof($this->FieldArray));
	 }
	 function getDumpDataWithSeparator(){
	 $x  = implode(",", $this->FieldArray)."\n";
	 $row = $this->getDumpData();
	 for($i=0; $i<sizeof($row); $i++){
	 # $x .= implode($this->FieldsTerminator, $row[$i])."\n";
	 # $x .= "\"".implode("\",\"", $row[$i])."\"\n";
	 for($j=0; $j<sizeof($row[$i]); $j++){
	 $row[$i][$j] = "\"".$this->undo_htmlspecialchars($row[$i][$j])."\"";
	 }
	 $x .= implode(",", $row[$i])."\n";
	 }
	 return $x;
	 }
	 */
	
	function getDumpData()
	{
		$sql = stripslashes("SELECT ".implode(",", $this->FieldArray)." FROM ".$this->Table." ".$this->FilterSQL);
		
		return $this->returnArray($sql,sizeof($this->FieldArray));
	}
	
	//Get Information in other table
	function getDumpDataWithBouns($target_table, $returnUserID=false)
	{
		$Fields2in1 = array();
		
		
		foreach($this->FieldArray as $val){
			
			# Cater Date Format - remove time
			if($val=='DateOfBirth'){
				$thisfield = "DATE_FORMAT(T1.DateOfBirth, '%Y-%m-%d') ";
				array_push($Fields2in1, $thisfield.' as '.$val);
			}
			else{
				array_push($Fields2in1, 'T1.'.$val.' as '.$val);
				
			}
		}
		
		foreach($this->BounsFieldArray as $val){
			if(strtolower($val) == 'stayovernight')
			{
				$thisfield = 'if( T2.stayOverNight = 1,\'Y\',\'N\')';
				array_push($Fields2in1, $thisfield.' as '.$val);
			}else
			{
				array_push($Fields2in1, 'T2.'.$val.' as '.$val);
			}
		}
		
		$Filter = str_replace('UserID','T1.UserID',$this->FilterSQL);
		
		$sql = stripslashes("SELECT ".implode(',',$Fields2in1)." FROM ".$this->Table." T1 LEFT JOIN ".$target_table." T2 ON T1.UserID = T2.UserID ".$Filter);
		
		# Need UserID Array
		if($returnUserID){
			$sql_UserID = stripslashes("SELECT T1.UserID FROM ".$this->Table." T1 LEFT JOIN ".$target_table." T2 ON T1.UserID = T2.UserID ".$Filter);
			return array($this->returnArray($sql,sizeof($this->BounsFieldArray)), $this->returnArray($sql_UserID,sizeof($this->BounsFieldArray)));
		}
		# Normal return
		else{
			return $this->returnArray($sql,sizeof($this->BounsFieldArray));
		}
	}
	
	function getDumpDataWithSeparator()
	{
		$field_names = $this->FieldArray;
		
		for ($i=0; $i<sizeof($field_names); $i++)
		{
			$tmp_arr = explode(" AS ", $field_names[$i]);
			if (sizeof($tmp_arr)>1)
			{
				$field_names[$i] = trim($tmp_arr[1]);
			}
		}
		$x  = implode(",", $field_names)."\n";
		$row = $this->getDumpData();
		for($i=0; $i<sizeof($row); $i++)
		{
			for($j=0; $j<GetColumnSize($row[$i]); $j++)
			{
				$row2[$i][$j] = "\"".$this->undo_htmlspecialchars($row[$i][$j])."\"";
			}
			$x .= implode(",", $row2[$i])."\n";
		}
		return $x;
	}
	
	
	function getDumpDataWithSeparator_utf($WithParent=0, $WithGuardian=0, $returnArray=0, $WithStudentInfo=0, $TabID=0, $WithGroup=0, $WithRole=0, $WithGuardianInfo=0)
	{
		global $Lang,$plugin;
		$field_names = $this->FieldArray;
		for ($i=0; $i<sizeof($field_names); $i++)
		{
			$tmp_arr = explode(" AS ", $field_names[$i]);
			if (sizeof($tmp_arr)>1)
			{
				$field_names[$i] = trim($tmp_arr[1]);
			}
		}
		
		
		//                  $title_header  = implode("\t", $field_names)."\r\n";
		
		if($TabID==2){ #Student
			//Get the setting and classify the fields
			$field_names_ps = $Lang['AccountMgmt']['AddnPersonalSettings'];
			
			if($plugin['medical'])
			{
				$field_names_ps = array_merge($field_names_ps,array('stayOverNight'));
			}
			if($plugin['SDAS_module']['KISMode']){
				$field_names_ps = array_merge($field_names_ps,array('KISClassType'));
			}
			
			$field_names_ps = array_diff($field_names,array_diff($field_names, $field_names_ps));
			$field_names = array_diff($field_names, $field_names_ps);
			$this->setFieldArray($field_names);
			$this->setBounsFieldArray($field_names_ps);
			
			# Export Guardian Info
			if($WithGuardianInfo)
			{
				list($row, $UserID_row) = $this->getDumpDataWithBouns('INTRANET_USER_PERSONAL_SETTINGS', $returnUserID=$WithGuardianInfo);
				$UserID_row = Get_Array_By_Key($UserID_row, 'UserID');
			}
			# Normal
			else
			{
				$row = $this->getDumpDataWithBouns('INTRANET_USER_PERSONAL_SETTINGS');
			}
			
			//Reverse the classification for export
			$field_names = array_merge($field_names,$field_names_ps);
			$this->setFieldArray($field_names);
			
		}else{
			$row = $this->getDumpData();
			
		}
		
		# append parent / guardian
		if($WithParent)
		{
			$field_names[$i++] = "Parent";
		}
		if($WithGuardian)
		{
			$field_names[$i++] = "Guardian";
		}
		if($WithGroup)
		{
			$field_names[$i++] = "Group";
		}
		if($WithRole)
		{
			$field_names[$i++] = "Role";
		}
		if($WithGuardianInfo)
		{
			$field_names[$i++] = "Guardian Info (Chinese Name || English Name || Relationship || Emergency Phone No.)";
		}
		
		$max_StudentInfo = 0;
		for($i=0, $i_max=sizeof($row); $i<$i_max; $i++)
		{
			if($WithParent || $WithGuardian || $WithStudentInfo)
			{
				# retrieve Student UserID
				if($row[$i]['UserLogin'])
				{
					$sql = "select UserID from INTRANET_USER where UserLogin='". $row[$i]['UserLogin'] ."'";
				} else if($row[$i]['UserEmail'])
				{
					$sql = "select UserID from INTRANET_USER where UserEmail='". $row[$i]['UserEmail'] ."'";
				}
				
				$TmpStudentUserIDAry = $this->returnVector($sql);
				$TmpStudentUserID = $TmpStudentUserIDAry[0];
				$TmpUserID = $TmpStudentUserID;
			}
			if($WithGuardianInfo){
				$TmpStudentUserID = $UserID_row[$i];
			}
			
			# append parent / guardian data
			if($WithParent)
			{
				$sql = "select ParentID from INTRANET_PARENTRELATION where StudentID=$TmpStudentUserID";
				$TmpParentUserIDAry = $this->returnVector($sql);
				$TmpParentUserIDStr = implode(",", $TmpParentUserIDAry);
				$sql = "select EnglishName, ChineseName from INTRANET_USER where UserID in ($TmpParentUserIDStr)";
				$ParentNameAry= $this->returnArray($sql);
				$ParentField = "";
				$separator = "";
				foreach($ParentNameAry as $k=>$d)
				{
					$ParentField .= $separator . Get_Lang_Selection($d['ChineseName'],$d['EnglishName']);
					//$separator = "\r";
					$separator = ", ";
				}
				$ParentRow = GetColumnSize($row[$i]);
				$row[$i][$ParentRow] = $ParentField;
				$row[$i]["Parent"] = $ParentField;
			}
			
			# Guardian & Guardian Info
			if($WithGuardian || $WithGuardianInfo)
			{
				global $eclass_db, $ec_guardian;
				
				# Field for Guardian Info
				$extraInfoSql = $WithGuardianInfo? ", Relation, EmPhone" : "";
				
				$sql = "select EnName, ChName $extraInfoSql from $eclass_db.GUARDIAN_STUDENT where UserID = $TmpStudentUserID";
				$GuardianNameAry= $this->returnArray($sql);
				
				$GuardianField = "";
				$GuardianInfoField = "";
				$separator = "";
				$InfoSeparator = " || ";
				$GuardianCount = 0;
				# For every Guardian
				foreach($GuardianNameAry as $k=>$d)
				{
					$GuardianField .= $separator . Get_Lang_Selection($d['ChName'],$d['EnName']);
					
					# Guardian Info: 2 Guardian only
					if($WithGuardianInfo && $GuardianCount < 2)
					{
						$GuardianInfoField .= $separator;
						$GuardianInfoField .= ($d['ChName'] != null && trim($d['ChName']) != "")? $d['ChName'] : "--";
						$GuardianInfoField .= $InfoSeparator;
						$GuardianInfoField .= ($d['EnName'] != null && trim($d['EnName']) != "")? $d['EnName'] : "--";
						$GuardianInfoField .= $InfoSeparator;
						$GuardianInfoField .= ($ec_guardian[$d['Relation']] != null && trim($ec_guardian[$d['Relation']]) != "")? $ec_guardian[$d['Relation']] : "--";
						$GuardianInfoField .= $InfoSeparator;
						$GuardianInfoField .= ($d['EmPhone'] != null && trim($d['EmPhone']) != "")? $d['EmPhone'] : "--";
					}
					
					$separator = ", ";
					$GuardianCount++;
				}
				
				# Guardian
				if($WithGuardian){
					$GuardianRow = GetColumnSize($row[$i]);
					$row[$i][$GuardianRow] = $GuardianField;
					$row[$i]["Guardian"] = $GuardianField;
				}
				# Guardian Info
				if($WithGuardianInfo){
					$GuardianInfoRow = GetColumnSize($row[$i]);
					$row[$i][$GuardianInfoRow] = $GuardianInfoField;
					$row[$i]["GuardianInfo"] = $GuardianInfoField;
				}
			}
			
			if($WithStudentInfo)
			{
				include_once("libuser.php");
				$lu = new libuser();
				$this_child_list = $lu->getChildrenList($TmpUserID);
				if(!empty($this_child_list))
				{
					$ThisColumn = GetColumnSize($row[$i]);
					
					foreach($this_child_list as $child_no=>$child_info)
					{
						$max_StudentInfo = ($child_no>$max_StudentInfo) ? $child_no : $max_StudentInfo;
						
						list($this_StudentID, $this_StudentName) = $child_info;
						$sql = "SELECT u.EnglishName,yc.ClassTitleEN as ClassName,IF(yc.YearClassID IS NULL,'',ycu.ClassNumber) as ClassNumber  
								FROM INTRANET_USER as u 
								INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=u.UserID 
								INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."'
								WHERE u.UserID='$this_StudentID'  ";
						$result = $this->returnArray($sql);
						if(count($result)==0){
							$sql = "select EnglishName, ClassName, ClassNumber from INTRANET_USER where UserID='$this_StudentID'";
							$result = $this->returnArray($sql);
						}
						if(count($result)>0)
						{
							$row[$i][$ThisColumn] = $result[0]['EnglishName'];
							$row[$i]["StudentEngName".($child_no+1)] = $result[0]['EnglishName'];
							$ThisColumn++;
							
							$row[$i][$ThisColumn] = $result[0]['ClassName'];
							$row[$i]["StudentClassName".($child_no+1)] = $result[0]['ClassName'];
							$ThisColumn++;
							
							$row[$i][$ThisColumn] = $result[0]['ClassNumber'];
							$row[$i]["StudentClassNumber".($child_no+1)] = $result[0]['ClassNumber'];
							$ThisColumn++;
						}
					}
				}
			}
			
			if($WithGroup)
			{
				$lo = new libgrouping();
				$gp = $lo->getGroupInfoByUserID($TmpStudentUserID, " AND u.GroupID!=2 AND g.AcademicYearID='".Get_Current_Academic_Year_ID()."'");
				if(sizeof($gp)>0) {
					$groupList = "";
					$delim = "";
					for($j=0; $j<sizeof($gp); $j++) {
						$groupList .= $delim."(".$gp[$j][0].") ".$gp[$j][1]."";
						$delim = ", ";
					}
				} else {
					$groupList = "---";
				}
				$GroupField = $groupList;
				$GroupRow = GetColumnSize($row[$i]);
				$row[$i][$GroupRow] = $GroupField;
				$row[$i]["Group"] = $GroupField;
			}
			
			if($WithRole)
			{
				$lrole = new librole();
				$role = $lrole->getRoleInfoByUserID($TmpStudentUserID);
				if(sizeof($role)>0) {
					$roleInfo = "";
					$delim = "";
					for($j=0; $j<sizeof($role); $j++) {
						$roleInfo .= $delim.$role[$j][0];
						$delim = ", ";
					}
				} else {
					$roleInfo = "---";
				}
				$RoleField = $roleInfo;
				$RoleRow = GetColumnSize($row[$i]);
				$row[$i][$RoleRow] = $RoleField;
				$row[$i]["Role"] = $RoleField;
			}
			
			
			
			for($j=0; $j<GetColumnSize($row[$i]); $j++)
			{
				
				// $row2[$i][$j] = $this->undo_htmlspecialchars($row[$i][$j]);
				
				$row2[$i][$j] = "\"".str_replace("\"", "\"\"", html_entity_decode(str_replace(array("\n", "\r", "\t"), array(" ", " ", ""), $row[$i][$j]), ENT_QUOTES))."\"";
				
			}
			$x .= implode("\t", $row2[$i])."\r\n";
			
		}
		
		if($WithStudentInfo)
		{
			# append Student Info column title
			$tmp_ary = array();
			for($i=0;$i<=$max_StudentInfo;$i++)
			{
				$tmp_ary[] = "StudentEngName".($i+1);
				$tmp_ary[] = "StudentClassName".($i+1);
				$tmp_ary[] = "StudentClassNumber".($i+1);
			}
			$field_names = array_merge($field_names, $tmp_ary);
		}
		$header_title  = implode("\t", $field_names)."\r\n";
		$x = $header_title . $x;
		
		if($returnArray)
		{
			return (sizeof($row2)>0) ? array_merge(array($field_names), $row2) : array($field_names);
		}
		else
		{
			return $x;
		}
	}
	
	# ------------------------------------------------------------------------------------
}
?>