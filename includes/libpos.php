<?php
// Editing by 
/*
 * 2020-08-25 (Cameron): modify updateAlipayTransactionID() to use TempTransactionID instead of OutTradeNo
 *                      - call updateAlipayTransactionID() in actionAfterPaidByAlipay() to link POS and Alipay
 * 2020-08-20 (Cameron): add function getAlipayTransactionByID() and actionAfterPaidByAlipay()
 * 2020-05-07 (Ray): Modified Get_POS_Sales_Report_Data add YearName
 * 2020-04-15 (Cameron):- add parameter $allVoid to Get_POS_Transaction_Log_Data() to handle case of all items are void
 *                      - modify Get_POS_Transaction_Detail(), should deduct void qty when calculate GrandTotal
 *                      - modify getPickupDetailByStudent(), allow to show void items but not void order which grand total is zero in parent app
 *                      - modify getReplaceItemsByTransaction() to order by ItemLogID (replacement time)
 *                      - modify getVoidItemsByTransaction(), group by ItemID
 *                      - modify Change_POS_Transaction_By_Item(), update ItemQty and ReceiveAmount instead of insert  
 *                          if change to item already exist in the transaction 
 * 2020-04-14 (Cameron): modify Get_Management_Inventory_Index_Sql() and Get_Management_Inventory_Log_Sql() 
 *      to use safe sql to avoid eror when input apostrophe in keyword field
 * 2020-04-08 (Cameron): modify Get_Manage_Pickup_Detail_Sql(), break void and change item into different sql, show them by transaction time instead of group by item 
 * 2020-04-07 (Cameron): add parameter $ActionFrom to Void_POS_Transaction_By_Item(), deduct ReceiveAmount if the item
 *      is void after pick up
 * 2020-04-04 (Cameron): modify Get_Manage_Pickup_Detail_Sql() to concat remark 
 * 2020-04-02 (Cameron): modify Change_POS_Transaction_By_Item(), should retrieve latest record with ItemID and RefLocID
 *                      - fix Get_Manage_Pickup_Detail_Sql() to show user input Remark for void item
 *                      - exclude VoidAmount when counting total Amount of a transaction in Get_Manage_Transaction_Index_Sql(), Amount should not less than VoidAmount
 *                      - fix getPickupDetailByStudent(), should show pickup record in parent app for case of void partial after pickup all items 
 * 2020-04-01 (Cameron):- add condition (ppdr.ItemQty>0) to Get_Log_Item() 
 *                      - retrieve TransactionTime in Get_Manage_Pickup_Detail_Sql()
 *                      - modify $amountConds in Get_POS_Transaction_Log_Data() to fix case of void item only but not void order
 * 2020-03-24 (Cameron): add $itemAmountConds to Get_Manage_Pickup_Detail_Sql() to fix case of unable to get record if repeated swap two items more than one time
 *   
 * 2020-03-17 (Tommy): modified Get_POS_Student_Report_Sql(), add Times and LastTransactionTime to sql Case (#P180125)
 * 
 * 2020-01-10 (Cameron): add parameter $hasAvailableItem to Get_POS_Transaction_Purchase_Detail(), retrieve original qty if not $hasAvailableItem
 *                      fix: update table POS_TRANSACTION only in Void_POS_Transaction for case of all return item is zero
 * 2019-11-29 (Cameron): modify Process_POS_TRANSACTION(), allow negative balance to process if $sys_custom['ePOS']['AllowNegativeBalance']=true
 *                       fix update OutTradeNo in addAlipayTransaction()
 * 2019-11-04 (Cameron): add function getClassNameByStudentID(), modify isTransactionBelongToUser() to accept teacher to view his/her own receipt
 * 2019-10-29 (Henry): add CostPrice at SQL select statement in Get_Management_Inventory_Log_Sql()
 * 2019-10-16 (Henry): add flag enablePurchaseAndPickup in checkAppAccessRight() to control open webview or not
 * 2019-10-15 (Cameron): add parameter $receiveAmount to getLastReceiveTime()
 * 2019-10-10 (Cameron): add flag exclude_webview in checkAppAccessRight() to control open webview or not
 * 2019-09-02 (Henry): modified Get_Manage_Pickup_Detail_Sql() to fix the change item bugs
 * 2019-08-28 (Cameron): add function updateAlipayTransaction(), updateAlipayTransactionID(), getAlipayTransaction(), getAlipayTransactionItems()
 * 2019-08-27 (Cameron): add function getAlipayHashRef(), addAlipayTransaction(), cancelAlipayTransaction()
 * 2019-08-20 (Cameron): add checking teacher access right in checkAppAccessRight()
 *                       add function getStudentListByClass(), getFirstStudentInPendingCart()
 * 2019-08-15 (Carlos): Modified Get_POS_Transaction_Report_Table_Sql() and Get_POS_Student_Report_Sql() optimize the query not to join INTRANET_USER and INTRANET_ARCHVIE_USER with OR condition as it would scan all rows and very slow.
 * 						Modified Get_POS_Transaction_LogID_By_Date(), no need to join INTRANET_USER and INTRANET_ARCHVIE_USER.
 * 2019-07-25 (Cameron): retrieve ItemID in Get_Manage_Pickup_Detail_Sql() if $ShowCheckbox = false
 * 2019-07-09 (Cameron): add function checkAppAccessRight()
 * 2019-02-20 (Henry): Modified Get_POS_Transaction_LogID_By_Date(), Get_POS_Transaction_Log_Data(), Get_Management_Inventory_Log_Sql(), Get_POS_Transaction_Log_Data()
 * 2019-01-03 (Henry): Added Get_Manage_Pickup_Index_Sql(), Get_Manage_Pickup_Detail(), Get_Manage_Pickup_Detail_Sql()
 * 2018-07-20 (Anna): Added report mapping by barcode
 * 2018-07-20 (Anna): Modified Get_POS_Transaction_Report_Table_Sql(),
 *                    Get_POS_Transaction_Report_Data(),
 *                    Get_POS_Transaction_Log_Data(),
 *                    Get_POS_Transaction_LogID_By_Date(),  Added $void 
 * 2018-02-06 (Carlos): Added setting "ConfirmBeforePay" in Get_POS_Settings().
 * 2017-08-18 (Carlos): Added field TerminalStatus to Get_Terminal_List(). Added Get_Terminal_Records($map) and Update_Terminal_Info().
 * 2017-03-17 (Carlos): Modified the following functions below to fix sorting number type data, print and export versions to follow UI sorting field and order.
 * 						Get_POS_Item_Report_Table_Sql(), Get_POS_Item_Report_Data()
 *						Get_POS_Transaction_Report_Table_Sql(), Get_POS_Transaction_Report_Data()
 * 2017-02-10 (Carlos): Modified Get_POS_Sales_Report_Data(), added student name.
 * 2015-11-24 (Carlos): Modified Get_POS_Sales_Report_Data(), Get_POS_Transaction_Report_Table_Sql(), Get_POS_Transaction_Log_Data(), 
 * 						Get_POS_Transaction_LogID_By_Date(), Get_POS_Item_Report_Table_Sql(), Get_POS_Item_Report_Detail_Data(), 
 * 						Get_POS_Student_Report_Sql(), Get_POS_Student_Report_Detail_Data() -  get and count partial voided but non-voided transactions items.
 * 2015-08-06 (Carlos): Modified Get_POS_Transaction_Report_Table_Sql(), Get_POS_Item_Report_Table_Sql(), added unit price data info.
 * 2015-03-04 (Carlos): [ip2.5.6.3.1] Modified Process_POS_TRANSACTION(), log transaction details to PAYMENT_OVERALL_TRANSACTION_LOG.TransactionDetails
 * 2015-03-02 (Carlos): Modified Get_Management_Inventory_Index_Sql(), added optional parameter $NoFunctionButtons=false for print and export version
 * 2015-02-12 (Carlos): Modified Process_POS_TRANSACTION(), log item names and quantity to transaction log detail. Update terminal name as last updated terminal user name.
 * 2015-02-04 (Carlos): Modified Get_Manage_Transaction_Index_Sql(), add InvoiceNumber to the link for sorting by the number, not LogID
 * 2014-12-11 (Bill): $sys_custom['SupplementarySmartCard'] - modified Process_POS_TRANSACTION(), add find user by matching CardID4
 * 2014-11-25 (Carlos): [ip2.5.5.12.1] Added time component 23:59:59 to end date of Get_Manage_Transaction_Index_Sql() 
 * 2014-11-24 (Carlos): [ip2.5.5.12.1] Modified Void_POS_Transaction(), can refund partial amount according to selected refunding item quantity
 * 2014-11-20 (Carlos): [ip2.5.5.12.1] Modified Get_Invoice_Number(), use sprintf() to format the four digit invoice number
 * 2014-10-17 (Carlos): [ip2.5.5.10.1] Modified Get_POS_Item_Report_Detail_Data(), added data field UserName
 * 2014-02-11 (Carlos): Modified Get_POS_Transaction_Log_Data(), Get_POS_Item_Report_Table_Sql(), Get_POS_Item_Report_Detail_Data(), Get_POS_Student_Report_Detail_Data() - get CategoryName
 * 2014-01-23 (Carlos): Modified Get_POS_Settings() - added setting [PaymentMethod]
 * 2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Modified Process_POS_TRANSACTION(), find user by matching CardID or CardID2 or CardID3
 */

include_once("libdb.php");

class libpos extends libdb
{
    var $MaxLength;
    var $CurrencySign;
    var $Settings;
    var $DecimalPlace;

    function libpos()
    {
        parent::libdb();

        $this->MaxLength['CategoryCode'] = 20;
        $this->MaxLength['CategoryName'] = 25;
        $this->MaxLength['ItemBarcode'] = 100;
        $this->MaxLength['ItemName'] = 50;
        $this->CurrencySign = '$';
        $this->DecimalPlace = 2;
    }

    function Update_Terminal_Settings($SettingArr)
    {
        if (is_array($SettingArr) == false || count($SettingArr) == 0)
            return false;

        $libGS = new libgeneralsettings();
        $Success = $libGS->Save_General_Setting('ePOS', $SettingArr);

        return $Success;
    }

    function Get_POS_Settings()
    {
        include_once("libgeneralsettings.php");

        $libGS = new libgeneralsettings();
        $Setting[] = "'AllowClientProgramConnect'";        //Allow Client Program Connention Settings
        $Setting[] = "'IPAllow'";
        $Setting[] = "'TerminalIndependentCat'";
        $Setting[] = "'PaymentMethod'";
        $Setting[] = "'ConfirmBeforePay'";

        $this->Settings = $libGS->Get_General_Setting('ePOS', $Setting);
        return $this->Settings;
    }

    function Get_Management_Inventory_Index_Sql($CategoryID = '', $Keyword = '', $NoFunctionButtons = false)
    {
        global $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;

        $namefield = getNameFieldByLang("iu."); // last modified inventory user

        $cond_categoryID = '';
        if ($CategoryID != '')
            $cond_categoryID = " And cat.CategoryID = '" . $CategoryID . "' ";

        $cond_keyword = '';
        if ($Keyword != '') {
            $safeKeyword = $this->Get_Safe_Sql_Like_Query($Keyword);
            $cond_keyword .= " 	And 
								(
									cat.CategoryName Like '%" . $safeKeyword . "%'
									Or
									item.ItemName Like '%" . $safeKeyword . "%'
									Or
									item.Barcode Like '%" . $safeKeyword . "%'
								)
							";
        }

        $sql = "Select
						item.ItemName,
						cat.CategoryName,
						if (item.Barcode is Null Or item.Barcode = '', '--', item.Barcode) as Barcode,
						item.ItemCount,
						CONCAT('$', item.UnitPrice) as UnitPrice,
						IF (item.PhotoExt Is Null Or item.PhotoExt = '',
							CONCAT('--'),
							CONCAT(	";
        if ($NoFunctionButtons) {
            $sql .= "'<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pic.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">'";
        } else {
            $sql .= "'<a href=\"javascript:newWindow(\\'" . $PATH_WRT_ROOT . "home/eAdmin/GeneralMgmt/pos/setting/category_and_item/view_photo.php?ItemID=', item.ItemID, '\\', 10);\" class=\"tablelink\" title=\"{$Lang['ePOS']['ViewPhoto']}\" >',
								'<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pic.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">',
							 '</a>'";
        }
        $sql .= ")
						) as PhotoLink,
						if (item.InventoryDateModify is Null Or item.InventoryDateModify = '', '--', item.InventoryDateModify) as InventoryDateModify,
						if (item.InventoryModifyBy is Null Or item.InventoryModifyBy = '', '--', $namefield) as InventoryModifyBy, ";
        if ($NoFunctionButtons) {
            $sql .= "'' as ToolIcons ";
        } else {
            $sql .= "CONCAT(
								'<div class=\"table_row_tool row_content_tool\" style=\"float:left\">',
									'<a class=\"add_dim thickbox\" onclick=\"js_Show_Edit_Invertory_Layer(', item.ItemID, ', 1);\" title=\"{$Lang['ePOS']['IncreaseInventory']}\" href=\"#TB_inline?height=450&width=750&inlineId=FakeLayer\" />',
									'<a class=\"subtract_dim thickbox\" onclick=\"js_Show_Edit_Invertory_Layer(', item.ItemID, ', 0);\" title=\"{$Lang['ePOS']['DecreaseInventory']}\" href=\"#TB_inline?height=450&width=750&inlineId=FakeLayer\" />',
								'</div>',
								'<div style=\"float:right\">',
									'<a href=\"javascript:js_View_Log(', item.ItemID, ')\" class=\"tablelink\" title=\"{$Lang['ePOS']['ViewLog']}\" >',
										'<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">',
									'</a>',
								'</div>'
						) as ToolIcons ";
        }
        $sql .= "From
						POS_ITEM as item
						Inner Join
						POS_ITEM_CATEGORY as cat
						On (item.CategoryID = cat.CategoryID)
						Left Outer Join
						INTRANET_USER as iu
						On (item.InventoryModifyBy = iu.UserID)
				Where
						1
						$cond_categoryID
						$cond_keyword
				";
        return $sql;
    }

    function Get_Manage_Pickup_Index_Sql($FromDate = '', $ToDate = '', $ClassName = '', $RecordStatus = '', $keyword = '', $IsDBTable = false, $field = '', $order = '', $void = '')
    {
        global $intranet_session_language, $Lang, $image_path, $LAYOUT_SKIN;

        $namefield = getNameFieldWithClassNumberByLang("u.");
        if ($intranet_session_language == "b5" || $intranet_session_language == "gb") {
            #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
            $archive_namefield = "au.ChineseName";
        } else
            $archive_namefield = "au.EnglishName";
        # $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";


        if ($void == '1') {
            $amountConds = "AND IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) <= ppdr.ItemQty";
            $voidConds = "AND pt.VoidBy IS NOT NULL AND pt.VoidLogID IS NOT NULL";
        } else {
            $amountConds = "AND IF(pil.ItemLogID IS NULL,0, pil.Amount) < ppdr.ItemQty";
            $voidConds = "AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL";
        }


        $date_cond = " pt.ProcessDate between '$FromDate' and '$ToDate 23:59:59' ";

        if (trim($ClassName) != "")
            $user_cond .= " AND (u.ClassName like '%" . $ClassName . "%') ";

        if ($RecordStatus == 'C') {
            $status_cond .= " HAVING SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)) - SUM(ppdr.ReceiveAmount) <= 0 ";
        } else if ($RecordStatus == 'NC') {
            $status_cond .= " HAVING SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)) - SUM(ppdr.ReceiveAmount) > 0 ";
        }

        $sql = "SELECT";
        $sql .= "
				CASE 
					WHEN ppdr.InvoiceNumber Is NOT NULL AND ppdr.InvoiceNumber <> '' THEN ppdr.InvoiceNumber 
					ELSE '" . $Lang['ePayment']['NoInvoiceNumber'] . "' 
				END as InvoiceNumber,";
        $sql .= "
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
					CAST(IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as SIGNED) as ClassNumber,
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
					GROUP_CONCAT(CONCAT(pit.ItemName,' " . $this->CurrencySign . "',ROUND(ppdr.ItemSubTotal," . $this->DecimalPlace . "),' x ', IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)) SEPARATOR '<br>') as Detail,";
        if ($IsDBTable) {
            $sql .= "
				CONCAT('" . $this->CurrencySign . "',ROUND(SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)*ppdr.ItemSubTotal)," . $this->DecimalPlace . ")) AS GrandTotal,";
        } else {
            $sql .= "
				ROUND(SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)*ppdr.ItemSubTotal)," . $this->DecimalPlace . ") AS GrandTotal,";
        }
        $sql .= " IF(potl.LogID IS NOT NULL, potl.REFCode, potl2.REFCode) as REFCode,
					ppdr.DateInput,
					CONCAT( 
					    '<span style=\"white-space: nowrap;\">',
					    IF(SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)) - SUM(ppdr.ReceiveAmount) <= 0, '" . $Lang['ePOS']['Completed'] . "', '" . $Lang['ePOS']['NotCompleted'] . "'),
					    '</span>'
					) as Status,
					CONCAT(
							'<div>',
								'<a href=\"detail.php?void=" . $void . "&LogID=',ppdr.TransactionLogID,'\" class=\"tablelink\" title=\"{$Lang['ePOS']['ViewLog']}\" >',
									'<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">',
								'</a>',
							'</div>'
					) as ToolIcons,
					ROUND(SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)*ppdr.ItemSubTotal)," . $this->DecimalPlace . ") as GrandTotalForSort 
				from 
					POS_TRANSACTION as pt
					inner join
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on 
						$date_cond ";
        //$sql .= " AND (pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL ) ";
        $sql .= " AND pt.LogID = ppdr.TransactionLogID 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl 
						on pt.LogID = potl.LogID AND (pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL ) 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
						on potl2.LogID=pt.VoidLogID AND (pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL) 
					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
					LEFT JOIN POS_ITEM as pit ON pit.ItemID=ppdr.ItemID 
					LEFT JOIN INTRANET_USER as u 
					on potl.StudentID = u.UserID OR potl2.StudentID = u.UserID
					LEFT JOIN INTRANET_ARCHIVE_USER as au 
					on potl.StudentID = au.UserID OR potl2.StudentID = au.UserID
				WHERE 
					pil.ReplaceItemLogID IS NULL
					$amountConds
					AND (
						u.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						u.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						u.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						u.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
						potl.RefCode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
						potl2.RefCode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
						ppdr.InvoiceNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'  OR
                        pit.Barcode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
					)
					$user_cond 
                    $voidConds
				Group By
					pt.LogID $status_cond
				";
// 						$sql .= " AND pt.LogID = ppdr.TransactionLogID
// 					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl
// 						on pt.LogID = potl.LogID
// 					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2
// 						on potl2.LogID=pt.VoidLogID
// 					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID
// 					LEFT JOIN POS_ITEM as pit ON pit.ItemID=ppdr.ItemID
// 					LEFT JOIN INTRANET_USER as u
// 					on potl.StudentID = u.UserID OR potl2.StudentID = u.UserID
// 					LEFT JOIN INTRANET_ARCHIVE_USER as au
// 					on potl.StudentID = au.UserID OR potl2.StudentID = au.UserID
// 				WHERE
// 					(pil.ItemLogID IS NULL OR (pil.ItemLogID IS NOT NULL AND pil.Amount < ppdr.ItemQty))
// 					AND (
// 						u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						u.ClassName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						u.ClassNumber LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						au.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						au.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						au.ClassName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						au.ClassNumber LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						potl.RefCode LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						potl2.RefCode LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						ppdr.InvoiceNumber LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
// 						)
// 						$user_cond
// 						$voidConds
// 						Group By
// 						pt.LogID
// 						";
        if (!$IsDBTable) {
            if ($field != '' && $order != '') {
                $fields = array("InvoiceNumber", "ClassName", "ClassNumber", "Name", "Detail", "GrandTotalForSort", "REFCode", "DateInput");
                $field = max(0, min($field, count($fields) - 1));
                $sql .= " ORDER BY " . $fields[$field] . " " . ($order == 1 ? "" : " DESC ");
            } else {
                $sql .= " ORDER BY Name ";
            }
        }
//  		debug_pr($sql);
        return $sql;
    }

    function Get_Manage_Pickup_Detail_Sql($LogID, $LogType = '', $ShowCheckbox = true)
    {
        global $intranet_session_language, $PATH_WRT_ROOT, $Lang;

        $namefield = getNameFieldWithClassNumberByLang("u.");
        if ($intranet_session_language == "b5" || $intranet_session_language == "gb") {
            #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
            $archive_namefield = "au.ChineseName";
        } else
            $archive_namefield = "au.EnglishName";
        # $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

        $itemAmountConds = "";
        $transactionTimeSelecton = "";
        if ($LogType == 'Void') {

//            $ItemRemarksSelection = ",IF(pil.ReplaceItemLogID,CONCAT('" . $Lang['ePOS']['ChangeTo'] . ": ',pi2.ItemName),CONCAT('" . $Lang['ePOS']['Writeoff'] . "',IF(GROUP_CONCAT(DISTINCT pvl.Remark)<>'',CONCAT(' (',TRIM(BOTH ',' FROM GROUP_CONCAT(DISTINCT pvl.Remark)),')'),''))) as Remarks";
            
            $sql = "SELECT
    					c.CategoryName as CategoryName,
    					pi.ItemName as ItemName,
                        void.Amount as ItemQty,
                    	ROUND(ppdr.ItemSubTotal,2) as ItemSubTotal,
                    	ROUND(ppdr.ItemSubTotal* void.Amount,2) as GrandTotal,
                    	CONCAT('".$Lang['ePOS']['Writeoff']."',IF(void.Remark<>'',CONCAT(' (',void.Remark,')'),'')) as Remarks,
                    	void.DateInput as TransactionTime
    					" . ($ShowCheckbox ? ",CONCAT('<input type=\"checkbox\" name=\"" . $LogType . "ItemID[]\" id=\"" . $LogType . "ItemID\" value=\"',ppdr.ItemID,'\" onclick=\"\">')" : ",ppdr.ItemID") . "
    				FROM
                    	POS_ITEM as pi 
                    	inner join POS_ITEM_CATEGORY as c on c.CategoryID=pi.CategoryID 
                    	inner join PAYMENT_PURCHASE_DETAIL_RECORD as ppdr on pi.ItemID = ppdr.ItemID 
                    	inner join POS_TRANSACTION as pt on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber 
                    	inner join (SELECT 
                    					potl.LogID,
                    					potl.StudentID,
                    					pvl.TransactionLogID,
                    					pvl.Remark,
                    					pil.Amount,
                    					pil.ItemID,
                    					pil.DateInput
                    				FROM
                    					PAYMENT_OVERALL_TRANSACTION_LOG as potl
                    					INNER JOIN POS_VOID_LOG pvl ON pvl.VoidLogID=potl.LogID
                    					INNER JOIN POS_ITEM_LOG pil ON pil.RefLogID=pvl.VoidLogID
                    				WHERE 
                    					pil.LogType=2
                    				) as void on void.TransactionLogID=pt.LogID AND void.ItemID=ppdr.ItemID
                    	LEFT JOIN INTRANET_USER as u on void.StudentID = u.UserID
                    	LEFT JOIN INTRANET_ARCHIVE_USER as au on void.StudentID = au.UserID 
                    WHERE 
    					pt.LogID='$LogID'

                        UNION

                    SELECT
    					c.CategoryName as CategoryName,
    					pi.ItemName as ItemName,
                        pil.Amount as ItemQty,
                    	ROUND(ppdr.ItemSubTotal,2) as ItemSubTotal,
                    	ROUND(ppdr.ItemSubTotal* pil.Amount,2) as GrandTotal,
                    	CONCAT('".$Lang['ePOS']['ChangeTo'].": ',pi2.ItemName) as Remarks,
                    	pil.DateInput as TransactionTime
    					" . ($ShowCheckbox ? ",CONCAT('<input type=\"checkbox\" name=\"" . $LogType . "ItemID[]\" id=\"" . $LogType . "ItemID\" value=\"',ppdr.ItemID,'\" onclick=\"\">')" : ",ppdr.ItemID") . "
    				FROM
                    	POS_ITEM as pi 
                    	inner join POS_ITEM_CATEGORY as c on c.CategoryID=pi.CategoryID 
                    	inner join PAYMENT_PURCHASE_DETAIL_RECORD as ppdr on pi.ItemID = ppdr.ItemID 
                    	inner join POS_TRANSACTION as pt on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber 
                    	inner JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl ON potl.LogID=pt.LogID
                    	inner join POS_ITEM_LOG pil ON pil.RefLogID=pt.LogID and pil.ItemID=ppdr.ItemID AND pil.ReplaceItemLogID IS NOT NULL
                    	inner JOIN POS_ITEM_LOG as pil2 ON pil.ReplaceItemLogID = pil2.ItemLogID
                    	inner JOIN POS_ITEM as pi2 on pi2.ItemID = pil2.ItemID
                    	LEFT JOIN INTRANET_USER as u on potl.StudentID = u.UserID
                    	LEFT JOIN INTRANET_ARCHIVE_USER as au on potl.StudentID = au.UserID
                    WHERE 
                        pt.LogID='$LogID'";
//debug_pr($sql);            
    					
        } else {
            if ($LogType == 'Complete') {
                $voidConds = "AND IF(ppdr.ReceiveAmount,ppdr.ReceiveAmount,0) > 0";
                $transactionTimeSelecton = ",ppdr.ReceiveAmountDateModify as TransactionTime";
                $ItemCompleteSelection = ",'<img src=\"" . $PATH_WRT_ROOT . "/images/2009a/icon_tick_green.gif\" alt=\"\">' as Complete";
            } else if ($LogType == 'NotComplete') {
                $havingConds = "having ItemQty > 0 ";
                $itemAmountConds = " AND ppdr.ItemQty > 0";
//				$voidConds = "AND IF(ppdr.ReceiveAmount,ppdr.ReceiveAmount,0) < (ppdr.ItemQty-SUM(IF(pil.Amount,pil.Amount,0)))";
            }
//			$amountConds = "AND SUM(pil.Amount) < ppdr.ItemQty";
//			$voidConds .= " AND potl2.LogID IS NULL";


            $sql = "SELECT 
    					c.CategoryName as CategoryName,
    					pi.ItemName as ItemName,
    					" . ($LogType == 'Complete' ? " IF(ppdr.ReceiveAmount,ppdr.ReceiveAmount,0) " : " ppdr.ItemQty-SUM(IF(pil.ReplaceItemLogID IS NOT NULL,0,IF(pil.Amount,pil.Amount,0))) - IF(ppdr.ReceiveAmount,ppdr.ReceiveAmount,0) ") . " as ItemQty,
    					ROUND(ppdr.ItemSubTotal," . $this->DecimalPlace . ") as ItemSubTotal,
    					ROUND(ppdr.ItemSubTotal*" . ($LogType == 'Complete' ? " IF(ppdr.ReceiveAmount,ppdr.ReceiveAmount,0) " : " (ppdr.ItemQty-SUM(IF(pil.ReplaceItemLogID IS NOT NULL,0,IF(pil.Amount,pil.Amount,0))) - IF(ppdr.ReceiveAmount,ppdr.ReceiveAmount,0)) ") . "," . $this->DecimalPlace . ") as GrandTotal
    					$ItemCompleteSelection
    					$transactionTimeSelecton
    					" . ($ShowCheckbox ? ",CONCAT('<input type=\"checkbox\" name=\"" . $LogType . "ItemID[]\" id=\"" . $LogType . "ItemID\" value=\"',ppdr.ItemID,'\" onclick=\"\">')" : ",ppdr.ItemID") . "
    				from 
    					POS_ITEM as pi 
    					inner join POS_ITEM_CATEGORY as c on c.CategoryID=pi.CategoryID 
    					inner join PAYMENT_PURCHASE_DETAIL_RECORD as ppdr on pi.ItemID = ppdr.ItemID {$itemAmountConds}
    					inner join POS_TRANSACTION as pt on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber 
                        left join POS_VOID_LOG as pvl on pvl.TransactionLogID = pt.LogID 
                        left join PAYMENT_OVERALL_TRANSACTION_LOG as potl on pt.LogID = potl.LogID
                        left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2
                        on potl2.LogID=pvl.VoidLogID AND (pvl.VoidBy IS NOT NULL AND pvl.VoidDate IS NOT NULL AND pvl.VoidLogID IS NOT NULL)
                        LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND (pil.LogType=2 AND pil.RefLogID=potl2.LogID OR (pil.RefLogID=potl.LogID AND pil.ReplaceItemLogID IS NOT NULL))
                        LEFT JOIN INTRANET_USER as u on potl.StudentID = u.UserID OR potl2.StudentID = u.UserID
                        LEFT JOIN INTRANET_ARCHIVE_USER as au on potl.StudentID = au.UserID OR potl2.StudentID = au.UserID
    					$replaceItemLogConds
    				WHERE 
    					pt.LogID='$LogID' 
                        AND (pil.ItemLogID IS NULL OR (pil.ItemLogID IS NOT NULL $amountConds))
                        $voidConds group by pi.ItemID$groupCond $havingConds";
        }
        return $sql;
    }

    function Get_Management_Inventory_Log_Sql($ItemID, $StartDate = '', $EndDate = '', $Keyword = '', $Order = '', $LogType = '')
    {
        global $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;

        $namefield = getNameFieldByLang("iu."); // last modified inventory user

        $safeKeyword = $this->Get_Safe_Sql_Like_Query($Keyword);
        $cond_keyword = '';
        if ($Keyword != '')
            $cond_keyword .= " And (
                                    $namefield Like '%" . $safeKeyword. "%' 
									Or 
									log.Remark Like '%" . $safeKeyword. "%'
									) ";

        $cond_startdate = '';
        if ($StartDate != '')
            $cond_startdate = " And log.DateInput >= '" . $StartDate . "' ";

        $cond_enddate = '';
        if ($EndDate != '') {
            $EndDate = $EndDate . ' 23:59:59';
            $cond_enddate = " And log.DateInput <= '" . $EndDate . "' ";
        }

        $order_by_str = '';
        if ($Order != '') {
            $OrderStr = ($Order == 1) ? 'Asc' : 'Desc';
            $order_by_str = " Order By log.DateInput $OrderStr ";
        }

        $LogTypeFilter = '';
        if ($LogType == '') {
            $LogTypeFilter = '3,4';
        } else {
            $LogTypeFilter = '1,2,3,4,5';
        }

        $sql = "Select
						log.DateInput,
						$namefield as UpdatedUserName,
						IF (log.LogType in (2,3,5), log.Amount, '--') as IncreaseCount,
						IF (log.LogType in (1,4), log.Amount, '--') as DecreaseCount,
						log.CountAfter,
						IF (log.CostPrice IS NULL, '--', CONCAT('" . $this->CurrencySign . "',ROUND(log.CostPrice," . $this->DecimalPlace . "))) as CostPrice,
						CASE 
							WHEN log.LogType = 2 THEN '" . $Lang['ePOS']['TransactionVoid'] . "' 
							WHEN log.logType = 1 THEN '" . $Lang['ePOS']['TransactionMaking'] . "' 
							WHEN log.LogType = 3 THEN CONCAT('" . $Lang['ePOS']['InventoryAdd'] . " - ',log.Remark) 
							WHEN log.LogType = 4 THEN CONCAT('" . $Lang['ePOS']['InventoryDeduct'] . " - ',log.Remark) 
							WHEN log.LogType = 5 THEN '" . $Lang['ePOS']['TransactionItemChange'] . "'
							ELSE log.Remark 
						END as Remark 
				From
						POS_ITEM_LOG as log
						Left Outer Join
						INTRANET_USER as iu
						On (log.InputBy = iu.UserID)
				Where
						ItemID = '" . $ItemID . "'
						And 
						log.LogType in (" . $LogTypeFilter . ") 
						$cond_keyword
						$cond_startdate
						$cond_enddate
				$order_by_str
				";
        //debug_r($sql);
        return $sql;
    }

    // General Function
    function getExportAmountFormat($amount)
    {
        return round($amount, $this->DecimalPlace);
    }

    // Health Ingredient
    function Get_HealthIngredient_List($HealthIngredientID = '')
    {

        if ($HealthIngredientID != '')
            $cond = " AND HealthIngredientID = '$HealthIngredientID' ";
        $sql = "SELECT * FROM POS_HEALTH_INGREDIENT WHERE 1 $cond ORDER BY DisplayOrder";
        $result = $this->returnArray($sql);

        return $result;
    }

    function InsertHealthIngredient($Value)
    {
        $HealthIngredientCode = $this->Get_Safe_Sql_Query($Value['HealthIngredientCode']);
        $HealthIngredientName = $this->Get_Safe_Sql_Query($Value['HealthIngredientName']);
        $UnitName = $this->Get_Safe_Sql_Query($Value['UnitName']);
        $StandardIntakePerDay = $Value['StandardIntakePerDay'];
        $RecordStatus = 1;

        $sql = "
			INSERT INTO	
				POS_HEALTH_INGREDIENT
				(
					HealthIngredientCode,
					HealthIngredientName,
					UnitName,
					StandardIntakePerDay,
					RecordStatus
				)
			Values
				(
					'$HealthIngredientCode',
					'$HealthIngredientName',
					'$UnitName',
					'$StandardIntakePerDay',
					'$RecordStatus'
				)
		";

        $success = $this->db_db_query($sql);
        return $success ? 1 : 0;
    }

    function UpdateHealthIngredient($ValueArr, $HealthIngredientID)
    {
        foreach ($ValueArr as $Key => $tmpValue)
            $SetValues[] = "$Key = '" . $tmpValue . "'";

        $SetValuesStr = implode(",", $SetValues);

        $sql = "
			UPDATE	
				POS_HEALTH_INGREDIENT
			SET
				$SetValuesStr
			WHERE
				HealthIngredientID = '$HealthIngredientID'
		";

        $success = $this->db_db_query($sql);
        return $success ? 1 : 0;
    }

    function DeleteHealthIngredient($HealthIngredientID)
    {
        $sql = "DELETE FROM 
					POS_HEALTH_INGREDIENT
				WHERE 
					HealthIngredientID = '$HealthIngredientID' ";
        $success = $this->db_db_query($sql);
        return $success ? 1 : 0;
    }

    function IsHealthIngredientDeletable($HealthIngredientID)
    {
        $sql = "SELECT COUNT(*) FROM 
					POS_ITEM_HEALTH_INGREDIENT 
				WHERE 
					HealthIngredientID = '$HealthIngredientID' ";
        $result = $this->returnVector($sql);
        return $result[0] > 0 ? 0 : 1;
    }

    function IsHealthIngredientExist($CheckField, $Value, $HealthIngredientID = '')
    {
        if ($HealthIngredientID != '')
            $cond = " AND HealthIngredientID <> '$HealthIngredientID' ";

        $sql = "
			SELECT
				COUNT(*)
			FROM	
				POS_HEALTH_INGREDIENT
			WHERE
				$CheckField = '$Value'
				$cond
		";

        $exist = $this->returnVector($sql);
        return $exist[0] >= 1 ? 1 : 0;
    }

    # Item Health Ingredient Mapping
    function Get_Item_Health_Ingredient_List($ItemID, $HealthIngredientID = '')
    {
        if (!empty($HealthIngredientID))
            $cond .= "AND pihi.HealthIngredientID = '$HealthIngredientID' ";

        $sql = "
			SELECT 	
				phi.HealthIngredientID,
				phi.HealthIngredientName,	
				phi.UnitName,	
				phi.StandardIntakePerDay,	
				pihi.IngredientIntake	
			FROM
				POS_ITEM_HEALTH_INGREDIENT pihi
					INNER JOIN 
				POS_HEALTH_INGREDIENT phi 
					ON pihi.HealthIngredientID = phi.HealthIngredientID
			WHERE 
				pihi.ItemID = '$ItemID'
				$cond
			ORDER BY
				phi.DisplayOrder
			";

        return $this->returnArray($sql);
    }

    function InsertItemHealthIngredient($Value)
    {
        $ItemID = $Value['ItemID'];
        $HealthIngredientID = $Value['HealthIngredientID'];
        $IngredientIntake = $Value['ItemIntake'];

        $sql = "
			INSERT INTO	
				POS_ITEM_HEALTH_INGREDIENT
				(
					ItemID,
					HealthIngredientID,
					IngredientIntake
				)
			Values
				(
					'$ItemID',
					'$HealthIngredientID',
					'$IngredientIntake'
				)
		";

        $success = $this->db_db_query($sql);
        return $success ? 1 : 0;
    }

    function UpdateItemHealthIngredient($ValueArr, $ItemID, $HealthIngredientID)
    {
        foreach ($ValueArr as $Key => $tmpValue)
            $SetValues[] = "$Key = '" . $tmpValue . "'";

        $SetValuesStr = implode(",", $SetValues);

        $sql = "
			UPDATE	
				POS_ITEM_HEALTH_INGREDIENT
			SET
				$SetValuesStr
			WHERE
				ItemID = '$ItemID'
				AND HealthIngredientID = '$HealthIngredientID'
		";

        $success = $this->db_db_query($sql);
        return $success ? 1 : 0;
    }

    function DeleteItemHealthIngredient($ItemID, $HealthIngredientID)
    {
        $sql = "
			DELETE FROM	
				POS_ITEM_HEALTH_INGREDIENT
			WHERE
				ItemID = '$ItemID'
				AND HealthIngredientID = '$HealthIngredientID'
		";

        $success = $this->db_db_query($sql);
        return $success ? 1 : 0;
    }
    /*
	//Reports
	function Get_POS_Transaction_Report_Table_Sql($FromDate='',$ToDate='',$ClassName='',$keyword='',$IsDBTable=false)
	{
		global $intranet_session_language, $Lang;

		$namefield = getNameFieldWithClassNumberByLang("u.");
		if($intranet_session_language=="b5"||$intranet_session_language=="gb")
		{
		 	#$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
		    $archive_namefield="au.ChineseName";
		}
		else
			$archive_namefield ="au.EnglishName";
			# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

		$date_cond = " pt.ProcessDate between '$FromDate' and '$ToDate 23:59:59' ";

		if (trim($ClassName) != "")
			$user_cond .= " AND (u.ClassName like '%".$ClassName."%') ";

		$sql  = "SELECT";
		if ($IsDBTable) {
			$sql .= "
					CASE
						WHEN ppdr.InvoiceNumber Is NOT NULL AND ppdr.InvoiceNumber <> '' THEN CONCAT('<a href=\"detail.php?LogID=',pt.LogID,'\" target=\"_blank\" class=\"tablelink\">',ppdr.InvoiceNumber,'</a>')
						ELSE CONCAT('<a class=\"contenttool\" href=\"detail.php?LogID=',ppdr.TransactionLogID,'\" target=\"_blank\" class=\"tablelink\">".$Lang['ePayment']['NoInvoiceNumber']."</a>')
					END as InvoiceNumber,";
		}
		else {
			$sql .= "
					CASE
						WHEN ppdr.InvoiceNumber Is NOT NULL AND ppdr.InvoiceNumber <> '' THEN ppdr.InvoiceNumber
						ELSE '".$Lang['ePayment']['NoInvoiceNumber']."'
					END as InvoiceNumber,";
		}
		$sql .= "
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
					GROUP_CONCAT(CONCAT(pit.ItemName,' ".$this->CurrencySign."',ROUND(ppdr.ItemSubTotal,".$this->DecimalPlace."),' x ',ppdr.ItemQty) SEPARATOR '<br>') as Detail,";
		if ($IsDBTable) {
			$sql .= "
				CONCAT('".$this->CurrencySign."',ROUND(SUM(ppdr.ItemQty*ppdr.ItemSubTotal),".$this->DecimalPlace.")) AS GrandTotal,";
		}
		else {
			$sql .= "
				ROUND(SUM(ppdr.ItemQty*ppdr.ItemSubTotal),".$this->DecimalPlace.") AS GrandTotal,";
		}
		$sql .= "
					potl.REFCode,
					ppdr.DateInput
				from
					POS_TRANSACTION as pt
					inner join
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on
						$date_cond
						AND (pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL )
						AND pt.LogID = ppdr.TransactionLogID
					inner join PAYMENT_OVERALL_TRANSACTION_LOG as potl
						on pt.LogID = potl.LogID
					LEFT JOIN POS_ITEM as pit ON pit.ItemID=ppdr.ItemID
					LEFT JOIN INTRANET_USER as u
					on potl.StudentID = u.UserID
					LEFT JOIN INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID
				WHERE
					(
						u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
						u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
						u.ClassName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
						u.ClassNumber LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
						au.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
						au.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
						au.ClassName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
						au.ClassNumber LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
						potl.RefCode LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
						ppdr.InvoiceNumber LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
					)
					$user_cond
				Group By
					ppdr.InvoiceNumber, potl.RefCode
				";
		if (!$IsDBTable) {
			$sql .= "
				ORDER BY
					Name
				";
		}

		return $sql;
	}
	*/
    //Reports
    function Get_POS_Transaction_Report_Table_Sql($FromDate = '', $ToDate = '', $ClassName = '', $RecordStatus = '', $keyword = '', $IsDBTable = false, $field = '', $order = '', $void = '')
    {
        global $intranet_session_language, $Lang, $LAYOUT_SKIN, $image_path;

        $namefield1 = getNameFieldWithClassNumberByLang("u1.");
        $namefield2 = getNameFieldWithClassNumberByLang("u2.");
        if ($intranet_session_language == "b5" || $intranet_session_language == "gb") {
            #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
            $archive_namefield1 = "au1.ChineseName";
            $archive_namefield2 = "au2.ChineseName";
        } else {
            $archive_namefield1 = "au1.EnglishName";
            $archive_namefield2 = "au2.EnglishName";
            # $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";
        }

        if ($void == '1') {
            $amountConds = "AND IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) <= ppdr.ItemQty";
            $voidConds = "AND pt.VoidBy IS NOT NULL AND pt.VoidLogID IS NOT NULL";
        } else {
            $amountConds = "AND IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) < ppdr.ItemQty";
            $voidConds = "AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL";
        }


        $date_cond = " pt.ProcessDate between '$FromDate' and '$ToDate 23:59:59' ";

        if (trim($ClassName) != "")
            $user_cond .= " AND (u1.ClassName like '%" . $ClassName . "%' OR u2.ClassName LIKE '%" . $ClassName . "%') ";

        if ($RecordStatus == 'C') {
            $status_cond .= " HAVING SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)) - SUM(ppdr.ReceiveAmount) <= 0 ";
        } else if ($RecordStatus == 'NC') {
            $status_cond .= " HAVING SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)) - SUM(ppdr.ReceiveAmount) > 0 ";
        }

        $sql = "SELECT";
        if ($IsDBTable) {
            $sql .= "
					CASE 
						WHEN ppdr.InvoiceNumber Is NOT NULL AND ppdr.InvoiceNumber <> '' THEN CONCAT('<a href=\"detail.php?void=" . $void . "&LogID=',pt.LogID,'\" target=\"_blank\" class=\"tablelink\">',ppdr.InvoiceNumber,'</a>') 
						ELSE CONCAT('<a class=\"contenttool\" href=\"detail.php??void=" . $void . "&LogID=',ppdr.TransactionLogID,'\" target=\"_blank\" class=\"tablelink\">" . $Lang['ePayment']['NoInvoiceNumber'] . "</a>') 
					END as InvoiceNumber,";
        } else {
            $sql .= "
					CASE 
						WHEN ppdr.InvoiceNumber Is NOT NULL AND ppdr.InvoiceNumber <> '' THEN ppdr.InvoiceNumber 
						ELSE '" . $Lang['ePayment']['NoInvoiceNumber'] . "' 
					END as InvoiceNumber,";
        }
        $sql .= "
					IF(potl.LogID IS NOT NULL, 
						IF(u1.UserID IS NULL,CONCAT('<i>',au1.ClassName,'</i>'),u1.ClassName), 
						IF(u2.UserID IS NULL,CONCAT('<i>',au2.ClassName,'</i>'),u2.ClassName)
					) as ClassName,
					IF(potl.LogID IS NOT NULL, 
						CAST(IF(u1.UserID IS NULL,CONCAT('<i>',au1.ClassNumber,'</i>'),u1.ClassNumber) as SIGNED), 
						CAST(IF(u2.UserID IS NULL,CONCAT('<i>',au2.ClassNumber,'</i>'),u2.ClassNumber) as SIGNED)
					) as ClassNumber,
					IF(potl.LogID IS NOT NULL, 
						IF(u1.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield1,'</i>'), IF(au1.UserID IS NULL AND u1.UserID IS NULL,'<font color=red>*</font>',$namefield1)),
						IF(u2.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield2,'</i>'), IF(au2.UserID IS NULL AND u2.UserID IS NULL,'<font color=red>*</font>',$namefield2))
					) as Name,
					GROUP_CONCAT(CONCAT(pit.ItemName,' " . $this->CurrencySign . "',ROUND(ppdr.ItemSubTotal," . $this->DecimalPlace . "),' x ', IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)) SEPARATOR '<br>') as Detail,";
        if ($IsDBTable) {
            $sql .= "
				CONCAT('" . $this->CurrencySign . "',ROUND(SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)*ppdr.ItemSubTotal)," . $this->DecimalPlace . ")) AS GrandTotal,";
        } else {
            $sql .= "
				ROUND(SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)*ppdr.ItemSubTotal)," . $this->DecimalPlace . ") AS GrandTotal,";
        }
        $sql .= " IF(potl.LogID IS NOT NULL, potl.REFCode, potl2.REFCode) as REFCode,
					ppdr.DateInput,
					IF(SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)) - SUM(ppdr.ReceiveAmount) <= 0, '" . $Lang['ePOS']['Completed'] . "', '" . $Lang['ePOS']['NotCompleted'] . "') as Status,
					CONCAT(
							'<div>',
								'<a href=\"pickup_detail.php?void=" . $void . "&LogID=',ppdr.TransactionLogID,'\" class=\"tablelink\" title=\"{$Lang['ePOS']['ViewLog']}\" >',
									'<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">',
								'</a>',
							'</div>'
					) as ToolIcons,
					ROUND(SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)*ppdr.ItemSubTotal)," . $this->DecimalPlace . ") as GrandTotalForSort 
				from 
					POS_TRANSACTION as pt
					inner join
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on 
						$date_cond ";
        //$sql .= " AND (pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL ) ";
        $sql .= " AND pt.LogID = ppdr.TransactionLogID 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl 
						on pt.LogID = potl.LogID AND (pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL ) 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
						on potl2.LogID=pt.VoidLogID AND (pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL) 
					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
					LEFT JOIN POS_ITEM as pit ON pit.ItemID=ppdr.ItemID 
					LEFT JOIN INTRANET_USER as u1 on potl.StudentID = u1.UserID 
					LEFT JOIN INTRANET_ARCHIVE_USER as au1 on potl.StudentID = au1.UserID 
					LEFT JOIN INTRANET_USER as u2 on potl2.StudentID = u2.UserID 
					LEFT JOIN INTRANET_ARCHIVE_USER as au2 on potl2.StudentID = au2.UserID 
				WHERE 
					pil.ReplaceItemLogID IS NULL
					$amountConds ";
        if (trim($keyword) != '') {
            $sql .= " AND (
						u1.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						u1.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						u1.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						u1.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au1.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au1.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au1.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au1.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
						u2.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						u2.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						u2.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						u2.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au2.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au2.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au2.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au2.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
						potl.RefCode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
						potl2.RefCode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
						ppdr.InvoiceNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'  OR
                        pit.Barcode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
					) ";
        }
        $sql .= " $user_cond 
                    $voidConds
				Group By
					pt.LogID $status_cond
				";
// 						$sql .= " AND pt.LogID = ppdr.TransactionLogID
// 					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl
// 						on pt.LogID = potl.LogID
// 					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2
// 						on potl2.LogID=pt.VoidLogID
// 					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID
// 					LEFT JOIN POS_ITEM as pit ON pit.ItemID=ppdr.ItemID
// 					LEFT JOIN INTRANET_USER as u
// 					on potl.StudentID = u.UserID OR potl2.StudentID = u.UserID
// 					LEFT JOIN INTRANET_ARCHIVE_USER as au
// 					on potl.StudentID = au.UserID OR potl2.StudentID = au.UserID
// 				WHERE
// 					(pil.ItemLogID IS NULL OR (pil.ItemLogID IS NOT NULL AND pil.Amount < ppdr.ItemQty))
// 					AND (
// 						u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						u.ClassName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						u.ClassNumber LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						au.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						au.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						au.ClassName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						au.ClassNumber LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						potl.RefCode LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						potl2.RefCode LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
// 						ppdr.InvoiceNumber LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
// 						)
// 						$user_cond
// 						$voidConds
// 						Group By
// 						pt.LogID
// 						";
        if (!$IsDBTable) {
            if ($field != '' && $order != '') {
                $fields = array("InvoiceNumber", "ClassName", "ClassNumber", "Name", "Detail", "GrandTotalForSort", "REFCode", "DateInput");
                $field = max(0, min($field, count($fields) - 1));
                $sql .= " ORDER BY " . $fields[$field] . " " . ($order == 1 ? "" : " DESC ");
            } else {
                $sql .= " ORDER BY Name ";
            }
        }
//  		debug_pr($sql);
        return $sql;
    }

    function Get_POS_Transaction_Report_Data($FromDate = '', $ToDate = '', $ClassName = '', $RecordStatus = '', $keyword = '', $field = '', $order = '', $void = '')
    {
        global $Lang, $intranet_session_language;

        $sql = $this->Get_POS_Transaction_Report_Table_Sql($FromDate, $ToDate, $ClassName, $RecordStatus, $keyword, false, $field, $order, $void);
        //echo $sql; die;
        $Result = $this->returnArray($sql);
        return $Result;
    }

    /*
	function Get_POS_Transaction_Log_Data($LogID)
	{
		global $intranet_session_language;

		$namefield = getNameFieldWithClassNumberByLang("u.");
		if($intranet_session_language=="b5"||$intranet_session_language=="gb")
		{
	        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
	        $archive_namefield="au.ChineseName";
		}
		else
			$archive_namefield ="au.EnglishName";
		# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

		$sql  = "SELECT
					ppdr.InvoiceNumber,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
					pi.ItemName,
					ppdr.ItemQty,
					ROUND(ppdr.ItemSubTotal,".$this->DecimalPlace.") as ItemSubTotal,
					ROUND(ppdr.ItemSubTotal*ppdr.ItemQty,".$this->DecimalPlace.") as GrandTotal,
					potl.RefCode,
					ppdr.DateInput,
					c.CategoryName
				from
					POS_ITEM as pi
					inner join POS_ITEM_CATEGORY as c on c.CategoryID=pi.CategoryID
					inner join
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt
					on ppdr.InvoiceNumber = pt.InvoiceNumber
					inner join
					PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pt.LogID = '".$LogID."' and pt.LogID = potl.LogID
					LEFT JOIN
					INTRANET_USER as u
					on potl.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID
				WHERE
					pt.VoidBy IS NULL
					AND pt.VoidDate IS NULL
					AND pt.VoidLogID IS NULL
					";
                //echo $sql; die;

		$Result = $this->returnArray($sql);

		return $Result;
	}
	*/
    function Get_POS_Transaction_Log_Data($LogID, $void = '', $allVoid=false)
    {
        global $intranet_session_language;

        $namefield = getNameFieldWithClassNumberByLang("u.");
        if ($intranet_session_language == "b5" || $intranet_session_language == "gb") {
            #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
            $archive_namefield = "au.ChineseName";
        } else
            $archive_namefield = "au.EnglishName";
        # $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

        if ($void == '1') {
            $amountConds = "AND IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) <= ppdr.ItemQty";
            $voidConds = "AND pt.VoidBy IS NOT NULL AND pt.VoidLogID IS NOT NULL";
        } else {
            if ($allVoid) {
                $amountConds = "AND IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) <= ppdr.ItemQty AND ppdr.ItemQty>0";
            }
            else {
                $amountConds = "AND IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) < ppdr.ItemQty AND ppdr.ItemQty>0";
            }
            $voidConds = "AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL";
        }

        $sql = "SELECT
					ppdr.InvoiceNumber,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
					pi.ItemName,
					IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount) as ItemQty,
					ROUND(ppdr.ItemSubTotal," . $this->DecimalPlace . ") as ItemSubTotal,
					ROUND(ppdr.ItemSubTotal*IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)," . $this->DecimalPlace . ") as GrandTotal,
					IF(potl.LogID IS NOT NULL, potl.REFCode, potl2.REFCode) as RefCode,
					ppdr.DateInput,
					c.CategoryName,
					ppdr.ReceiveAmount as ReceiveAmount,
					pt.LogID as LogID,
					pi.Barcode,
					c.CategoryCode
				from 
					POS_ITEM as pi 
					inner join POS_ITEM_CATEGORY as c on c.CategoryID=pi.CategoryID 
					inner join 
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt
					on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber  
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pt.LogID = potl.LogID AND pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
					on potl2.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
					LEFT JOIN
					INTRANET_USER as u
					on potl.StudentID = u.UserID OR potl2.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID OR potl2.StudentID = au.UserID
				WHERE 
					pt.LogID='$LogID' 
                    $amountConds
                    $voidConds";
        //debug_pr($sql);
        //echo $sql; die;

        $Result = $this->returnArray($sql);

        return $Result;
    }

    /*
	function Get_POS_Transaction_LogID_By_Date($FromDate,$ToDate)
	{
		global $intranet_session_language;

		$sql  = "SELECT
					pt.LogID
				from
					POS_ITEM as pi
					inner join
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt
					on ppdr.InvoiceNumber = pt.InvoiceNumber
						AND ppdr.DateInput between '$FromDate' and '$ToDate 23:59:59'
					inner join
					PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pt.LogID = potl.LogID
					LEFT JOIN
					INTRANET_USER as u
					on potl.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID
				WHERE
					pt.VoidBy IS NULL
					AND pt.VoidDate IS NULL
					AND pt.VoidLogID IS NULL
				Group By
					pt.LogID
					";
                //echo $sql; die;

		$Result = $this->returnVector($sql);

		return $Result;
	}
	*/
    function Get_POS_Transaction_LogID_By_Date($FromDate, $ToDate, $void = '')
    {
        global $intranet_session_language;


        if ($void == '1') {
            $amountConds = " IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) <= ppdr.ItemQty";
            $voidConds = "AND pt.VoidBy IS NOT NULL AND pt.VoidLogID IS NOT NULL";
        } else {
            $amountConds = " IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) < ppdr.ItemQty";
            $voidConds = "AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL";
        }

        $sql = "SELECT
					pt.LogID 
				from 
					POS_ITEM as pi
					inner join
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt 
					on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber 
						AND ppdr.DateInput between '$FromDate' and '$ToDate 23:59:59'
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pt.LogID = potl.LogID AND pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
					on potl2.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
				WHERE
					$amountConds
                    $voidConds
				Group By 
					pt.LogID
					";
        //echo $sql; die;
        $Result = $this->returnVector($sql);

        return $Result;
    }

    /*
	function Get_POS_Item_Report_Table_Sql($FromDate='', $ToDate='', $keyword='', $IsDBTable=false)
	{
		$date_cond = "a.DateInput between '$FromDate' and '$ToDate 23:59:59' ";

		$sql  = "SELECT
						ic.CategoryName, ";
		if ($IsDBTable)
			$sql .= "	CONCAT('<a href=\"#\" onclick=\"Get_Item_Detail(this,', b.ItemID ,');\" return false;\" class=\"tablelink\">',b.ItemName,'</a>') as ItemName,";
		else
			$sql .= "	b.ItemName,";
		$sql .= "CONCAT('".$this->CurrencySign."',ROUND(b.UnitPrice,".$this->DecimalPlace.")) as UnitPrice, SUM(ItemQty) as TotalSold,";
		if ($IsDBTable)
			$sql .= "	CONCAT('".$this->CurrencySign."',ROUND(SUM(ItemSubTotal*ItemQty),".$this->DecimalPlace.")) as TotalPrice ";
		else
			$sql .= "	ROUND(SUM(ItemSubTotal*ItemQty),".$this->DecimalPlace.") as TotalPrice ";
		$sql .= "	from
								POS_TRANSACTION pt
								INNER JOIN
								PAYMENT_PURCHASE_DETAIL_RECORD a
								ON
									$date_cond
									and
									(pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL )
									and
									pt.LogID = a.TransactionLogID
								INNER JOIN POS_ITEM b
								ON a.ItemID = b.ItemID
								INNER JOIN POS_ITEM_CATEGORY ic
								on b.CategoryID = ic.CategoryID
					    WHERE
								b.ItemName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							Group By
								b.ItemID
							";
		if (!$IsDBTable) {
			$sql .= "Order By
								ic.sequence, b.sequence
							";
		}

		return $sql;

	}
	*/
    function Get_POS_Item_Report_Table_Sql($FromDate = '', $ToDate = '', $keyword = '', $IsDBTable = false, $field = '', $order = '')
    {
        $date_cond = " ppdr.DateInput between '$FromDate 00:00:00' and '$ToDate 23:59:59' ";

        $sql = "SELECT 
						ic.CategoryName, ";
        if ($IsDBTable)
            $sql .= "	CONCAT('<a href=\"#\" onclick=\"Get_Item_Detail(this,', b.ItemID ,');\" return false;\" class=\"tablelink\">',b.ItemName,'</a>') as ItemName,";
        else
            $sql .= "	b.ItemName,";
        $sql .= "CONCAT('" . $this->CurrencySign . "',ROUND(b.UnitPrice," . $this->DecimalPlace . ")) as UnitPrice, SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)) as TotalSold,";
        if ($IsDBTable)
            $sql .= "	CONCAT('" . $this->CurrencySign . "',ROUND(SUM(ppdr.ItemSubTotal*IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount))," . $this->DecimalPlace . ")) as TotalPrice ";
        else
            $sql .= "	ROUND(SUM(ppdr.ItemSubTotal*IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount))," . $this->DecimalPlace . ") as TotalPrice ";
        $sql .= ",ROUND(SUM(ppdr.ItemSubTotal*IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount))," . $this->DecimalPlace . ") as TotalPriceForSort ";
        $sql .= "	from 
						POS_TRANSACTION pt 
						INNER JOIN 
						PAYMENT_PURCHASE_DETAIL_RECORD as ppdr 
						ON 
							$date_cond 
							and pt.LogID = ppdr.TransactionLogID 
						INNER JOIN POS_ITEM b 
						ON ppdr.ItemID = b.ItemID 
						INNER JOIN POS_ITEM_CATEGORY ic 
						on b.CategoryID = ic.CategoryID 
						left join PAYMENT_OVERALL_TRANSACTION_LOG as potl 
						on pt.LogID = potl.LogID AND pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL  
						left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
						on potl2.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
						LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
			    WHERE
					((pil.ItemLogID IS NULL AND ppdr.VoidAmount < ppdr.ItemQty) OR (pil.ItemLogID IS NOT NULL AND pil.Amount < ppdr.ItemQty)) 
					AND (b.ItemName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'  OR
                         b.Barcode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%') 
					Group By
						b.ItemID
							";
        if (!$IsDBTable) {
            if ($field != '' && $order != '') {
                $field = max(0, min($field, 4));
                $fields = array("CategoryName", "ItemName", "b.UnitPrice", "TotalSold", "TotalPriceForSort");
                $sql .= " ORDER BY " . $fields[$field] . " " . ($order == 1 ? "" : " DESC ");
            } else {
                $sql .= "Order By 
								ic.sequence, b.sequence
							";
            }
        }
        //debug_r($sql);
        return $sql;

    }

    function Get_POS_Item_Report_Data($FromDate = '', $ToDate = '', $keyword = '', $field = '', $order = '')
    {
        $sql = $this->Get_POS_Item_Report_Table_Sql($FromDate, $ToDate, $keyword, false, $field, $order);

        $result = $this->returnArray($sql);
        return $result;
    }

    /*
	function Get_POS_Item_Report_Detail_Data($FromDate='', $ToDate='', $ItemName='', $ItemID='')
	{
		global $intranet_session_language,$Lang;

		$namefield = getNameFieldWithClassNumberByLang("u.");
		$namefield_without_class = getNameFieldByLang2("u.");
		if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
		        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
		        $archive_namefield="au.ChineseName";
		}else  $archive_namefield ="au.EnglishName";
		# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

		if($ItemID != ''){
			$cond_ItemID = " pi.ItemID='$ItemID' AND ";
		}

		$sql  = "SELECT
					CASE
						WHEN ppdr.InvoiceNumber Is NOT NULL AND ppdr.InvoiceNumber <> '' THEN ppdr.InvoiceNumber
						ELSE '".$Lang['Payment']['NoInvoiceNumber']."'
					END as InvoiceNumber,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
					ppdr.ItemQty,
					ROUND(ppdr.ItemSubTotal,".$this->DecimalPlace.") as ItemSubTotal,
				 	ROUND(ppdr.ItemSubTotal*ppdr.ItemQty,".$this->DecimalPlace.") as GrandTotal,
					potl.RefCode,
					pt.ProcessDate,
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield_without_class)) as UserName
				from
					POS_ITEM as pi
					inner join
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt
					on ppdr.InvoiceNumber = pt.InvoiceNumber
					inner join
					PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pi.ItemName = '".ereg_replace("'", "\\'", $ItemName)."'
					  	and
					  	pt.LogID = potl.LogID
					  	and
					  	pt.ProcessDate between '$FromDate' and '$ToDate 23:59:59'
					LEFT JOIN
					INTRANET_USER as u
					on potl.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID
				WHERE $cond_ItemID
					pt.VoidBy IS NULL
					AND pt.VoidDate IS NULL
					AND pt.VoidLogID IS NULL
				Order By
					ppdr.DateInput
							";
		                //echo $sql; die;
		$Result = $this->returnArray($sql);
		return $Result;

	}
	*/
    function Get_POS_Item_Report_Detail_Data($FromDate = '', $ToDate = '', $ItemName = '', $ItemID = '')
    {
        global $intranet_session_language, $Lang;

        $namefield = getNameFieldWithClassNumberByLang("u.");
        $namefield_without_class = getNameFieldByLang2("u.");
        if ($intranet_session_language == "b5" || $intranet_session_language == "gb") {
            #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
            $archive_namefield = "au.ChineseName";
        } else  $archive_namefield = "au.EnglishName";
        # $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

        if ($ItemID != '') {
            $cond_ItemID = " pi.ItemID='$ItemID' AND ";
        }

        $sql = "SELECT
					CASE 
						WHEN ppdr.InvoiceNumber Is NOT NULL AND ppdr.InvoiceNumber <> '' THEN ppdr.InvoiceNumber 
						ELSE '" . $Lang['Payment']['NoInvoiceNumber'] . "' 
					END as InvoiceNumber,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
					IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount) as ItemQty,
					ROUND(ppdr.ItemSubTotal," . $this->DecimalPlace . ") as ItemSubTotal,
				 	ROUND(ppdr.ItemSubTotal*IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)," . $this->DecimalPlace . ") as GrandTotal,
					IF(potl.LogID IS NOT NULL, potl.RefCode, potl2.RefCode) as RefCode,
					pt.ProcessDate,
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield_without_class)) as UserName 
				from
					POS_ITEM as pi 
					inner join
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt 
					on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber
					left join 
					PAYMENT_OVERALL_TRANSACTION_LOG as potl 
					on pt.LogID = potl.LogID 
						and pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL  
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
					on potl2.LogID=pt.VoidLogID 
						AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
					LEFT JOIN 
					INTRANET_USER as u
					on potl.StudentID = u.UserID OR potl2.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID OR potl2.StudentID = au.UserID 
				WHERE $cond_ItemID 
					(pil.ItemLogID IS NULL AND ppdr.VoidAmount < ppdr.ItemQty OR (pil.ItemLogID IS NOT NULL AND pil.Amount < ppdr.ItemQty)) 
					and pt.ProcessDate between '$FromDate' and '$ToDate 23:59:59' ";
        if ($cond_ItemID == '') $sql .= " and pi.ItemName = '" . ereg_replace("'", "\\'", $ItemName) . "' ";
        $sql .= " Order By 
					ppdr.DateInput
							";
        //echo $sql; die;
        $Result = $this->returnArray($sql);
        return $Result;

    }

    /*
	function Get_POS_Student_Report_Sql($FromDate='',$ToDate='',$keyword='',$IsDBTable=false,$ClassName="")
	{
		global $intranet_session_language;

		$namefield = getNameFieldWithClassNumberByLang("u.");
		if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
		        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
		        $archive_namefield="au.ChineseName";
		}else  $archive_namefield ="au.EnglishName";
		# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

		$date_cond = " pt.ProcessDate between '$FromDate' and '$ToDate 23:59:59' ";

		$sql  = "
			SELECT";
		if ($IsDBTable) {
			$sql .= "
				CASE
					WHEN u.UserID IS NULL THEN CONCAT('<a href=\"#\" onclick=\"Get_Student_Detail(',potl.StudentID,'); return false;\" class=\"tablelink\">','<font color=red>*</font><i>',$archive_namefield,'</i>','<a>')
					ELSE CONCAT('<a href=\"#\" onclick=\"Get_Student_Detail(',potl.StudentID,'); return false;\" class=\"tablelink\">',$namefield,'</a>')
				END as StudentName,";
		}
		else {
			$sql .= "
				CASE
					WHEN u.UserID IS NULL THEN CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>')
					ELSE CONCAT($namefield)
				END as StudentName,";
		}
		$sql .= "
				IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
				IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,";
		if ($IsDBTable) {
			$sql .= "
				CONCAT('".$this->CurrencySign."',ROUND(SUM(ppdr.ItemQty*ppdr.ItemSubTotal),".$this->DecimalPlace.")) AS GrandTotal ";
		}
		else {
			$sql .= "
				ROUND(SUM(ppdr.ItemQty*ppdr.ItemSubTotal),".$this->DecimalPlace.") AS GrandTotal ";
		}
		$sql .= "
			from
				POS_TRANSACTION as pt
				INNER JOIN
				PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
				on
					$date_cond
					AND (pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL)
					AND pt.LogID = ppdr.TransactionLogID
				inner join
				PAYMENT_OVERALL_TRANSACTION_LOG as potl
				on pt.LogID = potl.LogID
				LEFT JOIN
				INTRANET_USER as u
				on potl.StudentID = u.UserID
				LEFT JOIN
				INTRANET_ARCHIVE_USER as au
				on potl.StudentID = au.UserID
			WHERE
				(
					 u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
					 u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
					 u.ClassName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
					 u.ClassNumber LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
					 au.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
					 au.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
					 au.ClassName LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
					 au.ClassNumber LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
					 potl.RefCode LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' OR
					 ppdr.InvoiceNumber LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
				)
				AND
				(
					u.ClassName LIKE '%".$this->Get_Safe_Sql_Like_Query($ClassName)."%' OR
					au.ClassName LIKE '%".$this->Get_Safe_Sql_Like_Query($ClassName)."%'
				)
			Group By
				potl.StudentID
			";
		if (!$IsDBTable) {
			$sql .= "
				Order By
					StudentName";
		}
		//echo $sql; die;
		return $sql;
	}
	*/
    function Get_POS_Student_Report_Sql($FromDate = '', $ToDate = '', $keyword = '', $IsDBTable = false, $ClassName = "", $field = "", $order = "")
    {
        global $intranet_session_language;

        $namefield1 = getNameFieldWithClassNumberByLang("u1.");
        $namefield2 = getNameFieldWithClassNumberByLang("u2.");
        if ($intranet_session_language == "b5" || $intranet_session_language == "gb") {
            #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
            $archive_namefield1 = "au1.ChineseName";
            $archive_namefield2 = "au2.ChineseName";
        } else {
            $archive_namefield1 = "au1.EnglishName";
            $archive_namefield2 = "au2.EnglishName";
            # $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";
        }
        $date_cond = " pt.ProcessDate between '$FromDate' and '$ToDate 23:59:59' ";

        $sql = "
			SELECT";
        if ($IsDBTable) {
            $sql .= "
				IF(potl.LogID IS NOT NULL, 	
					CASE 
						WHEN u1.UserID IS NULL THEN CONCAT('<a href=\"#\" onclick=\"Get_Student_Detail(',potl.StudentID,'); return false;\" class=\"tablelink\">','<font color=red>*</font><i>',$archive_namefield1,'</i>','<a>') 
						ELSE CONCAT('<a href=\"#\" onclick=\"Get_Student_Detail(',potl.StudentID,'); return false;\" class=\"tablelink\">',$namefield1,'</a>') 
					END,
					CASE 
						WHEN u2.UserID IS NULL THEN CONCAT('<a href=\"#\" onclick=\"Get_Student_Detail(',potl2.StudentID,'); return false;\" class=\"tablelink\">','<font color=red>*</font><i>',$archive_namefield2,'</i>','<a>') 
						ELSE CONCAT('<a href=\"#\" onclick=\"Get_Student_Detail(',potl2.StudentID,'); return false;\" class=\"tablelink\">',$namefield2,'</a>') 
					END 
				) as StudentName,";
        } else {
            $sql .= "
				IF(potl.LogID IS NOT NULL, 
					IF(u1.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield1,'</i>'), IF(au1.UserID IS NULL AND u1.UserID IS NULL,'<font color=red>*</font>',$namefield1)),
					IF(u2.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield2,'</i>'), IF(au2.UserID IS NULL AND u2.UserID IS NULL,'<font color=red>*</font>',$namefield2))
				) as StudentName,";
        }
        $sql .= "
				IF(potl.LogID IS NOT NULL, 
					IF(u1.UserID IS NULL,CONCAT('<i>',au1.ClassName,'</i>'),u1.ClassName), 
					IF(u2.UserID IS NULL,CONCAT('<i>',au2.ClassName,'</i>'),u2.ClassName)
				) as ClassName,
				IF(potl.LogID IS NOT NULL, 
					CAST(IF(u1.UserID IS NULL,CONCAT('<i>',au1.ClassNumber,'</i>'),u1.ClassNumber) as SIGNED), 
					CAST(IF(u2.UserID IS NULL,CONCAT('<i>',au2.ClassNumber,'</i>'),u2.ClassNumber) as SIGNED)
				) as ClassNumber,";
        $orderCount = "SELECT count(p.LogID) as Times FROM PAYMENT_OVERALL_TRANSACTION_LOG as p WHERE p.StudentID = IF(u1.UserID IS NULL, u2.UserID, u1.UserID) 
                       AND p.TransactionType = '3' AND (p.TransactionTime between '". $FromDate ."' AND '". $ToDate ."') GROUP BY p.StudentID";

        $sql .= " ( ".$orderCount." ) as Times, ";
        $sql .= " max(potl.TransactionTime) as LastTransactionTime, ";
        if ($IsDBTable) {
            $sql .= "
				CONCAT('" . $this->CurrencySign . "',ROUND(SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)*ppdr.ItemSubTotal)," . $this->DecimalPlace . ")) AS GrandTotal ";
        } else {
            $sql .= "
				ROUND(SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)*ppdr.ItemSubTotal)," . $this->DecimalPlace . ") AS GrandTotal ";
        }
        $sql .= ",ROUND(SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)*ppdr.ItemSubTotal)," . $this->DecimalPlace . ") AS GrandTotalForSort ";
        $sql .= "
			from 
				POS_TRANSACTION as pt 
				INNER JOIN 
				PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
				on 
					$date_cond 
					AND pt.LogID = ppdr.TransactionLogID 
				left join
				PAYMENT_OVERALL_TRANSACTION_LOG as potl
				on pt.LogID = potl.LogID AND pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL 
				left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
				on potl2.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
				LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
				LEFT JOIN INTRANET_USER as u1 on potl.StudentID = u1.UserID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au1 on potl.StudentID = au1.UserID 
				LEFT JOIN INTRANET_USER as u2 on potl2.StudentID = u2.UserID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au2 on potl2.StudentID = au2.UserID 
                LEFT JOIN POS_ITEM as pit ON pit.ItemID =  ppdr.ItemID
			WHERE 
				(pil.ItemLogID IS NULL AND ppdr.VoidAmount < ppdr.ItemQty OR (pil.ItemLogID IS NOT NULL AND pil.Amount < ppdr.ItemQty)) ";
        if (trim($keyword) != '') {
            $sql .= " AND (
					 u1.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 u1.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 u1.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 u1.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 au1.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 au1.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 au1.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 au1.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
					 u2.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 u2.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 u2.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 u2.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 au2.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 au2.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 au2.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
					 au2.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
					 potl.RefCode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
					 potl2.RefCode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
					 ppdr.InvoiceNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
                     pit.Barcode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
				) ";
        }
        if (trim($ClassName) != '') {
            $sql .= " AND 
					(
						u1.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($ClassName) . "%' OR
						au1.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($ClassName) . "%' OR 
						u2.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($ClassName) . "%' OR
						au2.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($ClassName) . "%'                    
					) ";
        }
        $sql .= " Group By 
				IF(potl.LogID IS NOT NULL,potl.StudentID,potl2.StudentID)
			";
        if (!$IsDBTable) {
            if ($field != "" && $order != "") {
                $fields = array("StudentName", "ClassName", "ClassNumber", "GrandTotalForSort");
                $field = max(0, min($field, count($fields) - 1));
                $sql .= " ORDER BY " . $fields[$field] . " " . ($order == 1 ? "" : " DESC ");
            } else {
                $sql .= "
				Order By 
					StudentName";
            }
        }
        //echo $sql; die;
        return $sql;
    }

    function Get_POS_Student_Report_Data($FromDate = '', $ToDate = '', $keyword = '', $ClassName = "", $field = "", $order = "")
    {
        global $intranet_session_language;

        $sql = $this->Get_POS_Student_Report_Sql($FromDate, $ToDate, $keyword, false, $ClassName, $field, $order);
        //echo $sql; die;
        $Result = $this->returnArray($sql);

        return $Result;
    }

    /*
	function Get_POS_Student_Report_Detail_Data($FromDate='',$ToDate='',$StudentID='')
	{
		global $intranet_session_language;

		$namefield = getNameFieldWithClassNumberByLang("u.");
		if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
		        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
		        $archive_namefield="au.ChineseName";
		}else  $archive_namefield ="au.EnglishName";
		# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

		$sql  = "SELECT
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					pi.ItemName,
				SUM(ppdr.ItemQty) as Quantity,
				 	ROUND(SUM(ppdr.ItemSubTotal*ppdr.ItemQty),".$this->DecimalPlace.") as GrandTotal,
					c.CategoryName
				from
					POS_ITEM as pi
					inner join POS_ITEM_CATEGORY as c on c.CategoryID=pi.CategoryID
					inner join
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt
					on ppdr.InvoiceNumber = pt.InvoiceNumber
					inner join
					PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pt.LogID = potl.LogID
					  	and
					  	potl.StudentID = '".$StudentID."'
					  	and
					  	pt.ProcessDate between '$FromDate' and '$ToDate 23:59:59'
					LEFT JOIN
					INTRANET_USER as u
					on potl.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID
				WHERE
					pt.VoidBy IS NULL
					AND pt.VoidDate IS NULL
					AND pt.VoidLogID IS NULL
				Group By
					pi.ItemName
							";
		                //echo $sql; die;
		$Result = $this->returnArray($sql);

		return $Result;
	}
	*/
    function Get_POS_Student_Report_Detail_Data($FromDate = '', $ToDate = '', $StudentID = '')
    {
        global $intranet_session_language;

        $namefield = getNameFieldWithClassNumberByLang("u.");
        if ($intranet_session_language == "b5" || $intranet_session_language == "gb") {
            #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
            $archive_namefield = "au.ChineseName";
        } else  $archive_namefield = "au.EnglishName";
        # $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

        $sql = "SELECT
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					pi.ItemName,
					SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)) as Quantity,
				 	ROUND(SUM(ppdr.ItemSubTotal*IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount))," . $this->DecimalPlace . ") as GrandTotal,
					c.CategoryName 
				from 
					POS_ITEM as pi 
					inner join POS_ITEM_CATEGORY as c on c.CategoryID=pi.CategoryID 
					inner join 
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt 
					on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber 
					left join 
					PAYMENT_OVERALL_TRANSACTION_LOG as potl 
					on pt.LogID = potl.LogID 
					  	and 
					  	potl.StudentID = '" . $StudentID . "' 
					  	AND pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
						on potl2.LogID=pt.VoidLogID and potl2.StudentID = '" . $StudentID . "' 
						AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
					LEFT JOIN 
					INTRANET_USER as u
					on potl.StudentID = u.UserID OR potl2.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID OR potl2.StudentID = au.UserID
				WHERE 
					(potl.StudentID='$StudentID' OR potl2.StudentID='$StudentID') 
					AND (pil.ItemLogID IS NULL AND ppdr.VoidAmount < ppdr.ItemQty OR (pil.ItemLogID IS NOT NULL AND pil.Amount < ppdr.ItemQty)) 
					AND pt.ProcessDate between '$FromDate 00:00:00' and '$ToDate 23:59:59' 
				Group By 
					pi.ItemName
							";
        //echo $sql; die;
        $Result = $this->returnArray($sql);

        return $Result;
    }

    function Get_POS_Sales_Report_Data($FromDate = '', $ToDate = '')
    {
        global $Lang;

        $student_name_field = getNameFieldWithClassNumberByLang("u.");
        $archived_student_name_field = getNameFieldWithClassNumberByLang("au.");

        $sql = "
					SELECT
						pt.LogID,
						CASE 
							WHEN pt.InvoiceNumber Is NOT NULL AND pt.InvoiceNumber <> '' THEN pt.InvoiceNumber 
							ELSE '" . $Lang['ePayment']['NoInvoiceNumber'] . "' 
						END as InvoiceNumber,
						ic.CategoryID,
		        ic.CategoryName,
		        ppdr.ItemID,
		        i.ItemName,
						ppdr.ItemQty,
						ROUND(ppdr.ItemSubTotal," . $this->DecimalPlace . ") as ItemSubTotal,
						pt.ProcessDate,
						IF(u.UserID IS NOT NULL,$student_name_field,CONCAT('<span class=\"red\">*</span>',$archived_student_name_field)) as StudentName,
						IF(u_y.YearName IS NOT NULL, u_y.YearName, au_y.YearName) as YearName 
					FROM
		        POS_TRANSACTION AS pt
		        INNER JOIN
						PAYMENT_PURCHASE_DETAIL_RECORD ppdr
		        on pt.LogID = ppdr.TransactionLogID
		          and pt.ProcessDate between '" . $FromDate . "' and '" . $ToDate . " 23:59:59'
		          and
		          (pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL) 
		        INNER JOIN
		        POS_ITEM i
		        on ppdr.ItemID = i.ItemID
		        INNER JOIN
		        POS_ITEM_CATEGORY ic
		        on i.CategoryID = ic.CategoryID 
				INNER JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl ON potl.LogID=pt.LogID 
				LEFT JOIN INTRANET_USER as u ON u.UserID=potl.StudentID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=potl.StudentID 
				
				LEFT JOIN YEAR_CLASS_USER as u_ysu ON u_ysu.UserID=u.UserID
				LEFT JOIN YEAR_CLASS as u_yc ON u_yc.YearClassID=u_ysu.YearClassID
				LEFT JOIN YEAR as u_y ON u_y.YearID=u_yc.YearID

				LEFT JOIN YEAR_CLASS_USER as au_ysu ON au_ysu.UserID=au.UserID
				LEFT JOIN YEAR_CLASS as au_yc ON au_yc.YearClassID=au_ysu.YearClassID
				LEFT JOIN YEAR as au_y ON au_y.YearID=au_yc.YearID
				
				WHERE pt.ProcessDate between '" . $FromDate . "' and '" . $ToDate . " 23:59:59' 
				GROUP BY
						pt.LogID,
						ppdr.ItemID 
		      Order by
		        ic.Sequence,i.Sequence
					";
        $Result = $this->returnArray($sql);

        // Get partial voided but not totally voided transactions, not voided quantities are also counted
        $sql = "SELECT 
					pt.LogID,
					CASE 
						WHEN pt.InvoiceNumber Is NOT NULL AND pt.InvoiceNumber <> '' THEN pt.InvoiceNumber 
						ELSE '" . $Lang['ePayment']['NoInvoiceNumber'] . "' 
					END as InvoiceNumber,
					ic.CategoryID,
					ic.CategoryName,
					ppdr.ItemID,
					i.ItemName,
					IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount) as ItemQty,
					ROUND(ppdr.ItemSubTotal," . $this->DecimalPlace . ") as ItemSubTotal,
					pt.ProcessDate,
					IF(u.UserID IS NOT NULL,$student_name_field,CONCAT('<span class=\"red\">*</span>',$archived_student_name_field)) as StudentName,
					IF(u_y.YearName IS NOT NULL, u_y.YearName, au_y.YearName) as YearName  
				FROM POS_TRANSACTION AS pt 
				INNER JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl ON potl.LogID=pt.VoidLogID 
				INNER JOIN PAYMENT_PURCHASE_DETAIL_RECORD ppdr
		        on pt.LogID = ppdr.TransactionLogID 
		          and pt.ProcessDate between '" . $FromDate . "' and '" . $ToDate . " 23:59:59'
		          and 
		          (pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL) 
				LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl.LogID
				INNER JOIN POS_ITEM i on ppdr.ItemID = i.ItemID
		        INNER JOIN POS_ITEM_CATEGORY ic on i.CategoryID = ic.CategoryID 
				LEFT JOIN INTRANET_USER as u ON u.UserID=potl.StudentID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=potl.StudentID 
				
				LEFT JOIN YEAR_CLASS_USER as u_ysu ON u_ysu.UserID=u.UserID
				LEFT JOIN YEAR_CLASS as u_yc ON u_yc.YearClassID=u_ysu.YearClassID
				LEFT JOIN YEAR as u_y ON u_y.YearID=u_yc.YearID

				LEFT JOIN YEAR_CLASS_USER as au_ysu ON au_ysu.UserID=au.UserID
				LEFT JOIN YEAR_CLASS as au_yc ON au_yc.YearClassID=au_ysu.YearClassID
				LEFT JOIN YEAR as au_y ON au_y.YearID=au_yc.YearID
				
				WHERE (pt.ProcessDate between '" . $FromDate . "' and '" . $ToDate . " 23:59:59') AND (pil.ItemLogID IS NULL AND ppdr.VoidAmount < ppdr.ItemQty OR (pil.ItemLogID IS NOT NULL AND pil.Amount < ppdr.ItemQty))
				GROUP BY
						pt.LogID,
						ppdr.ItemID 
				Order by ic.Sequence,i.Sequence";

        $VoidedResult = $this->returnArray($sql);
        $Result = array_merge($Result, $VoidedResult);

        for ($i = 0; $i < sizeof($Result); $i++) {
            list($LogID, $InvoiceNumber, $CategoryID, $CategoryName, $ItemID, $ItemName, $ItemQty, $UnitPrice, $DateInput) = $Result[$i];
            $ItemSubTotal = $UnitPrice * $ItemQty;
            $returnAry['TransactionDetail'][$LogID]['ItemDetail'][$ItemID] = array("ItemQty" => $ItemQty, "ItemSubTotal" => $ItemSubTotal);
            $returnAry['TransactionDetail'][$LogID]['TransactionDetail'] = array("InvoiceNumber" => $InvoiceNumber, "TransactionTime" => $DateInput, "StudentName" => $Result[$i]['StudentName'], "YearName"=>$Result[$i]['YearName']);
            $returnAry['Category'][$CategoryID] = $CategoryName;
            $returnAry['Item'][$CategoryID][$ItemID] = $ItemName;
            $returnAry['ProcessDate'][$LogID] = strtotime($DateInput);
        }
        if (is_array($returnAry['ProcessDate'])) {
            asort($returnAry['ProcessDate']);
        }

        //debug_r($returnAry);
        return $returnAry;
    }

    // Start of API functions
    function Check_POS_Client_IP()
    {
        $Setting = $this->Get_POS_Settings();

        if (trim($Setting['IPAllow']) != "") {
            $IPAllow = explode("\n", $Setting['IPAllow']);

            foreach ($IPAllow AS $key => $ip) {
                $ip = trim($ip);
                if ($ip == "0.0.0.0") {
                    return true;
                } else if (testip($ip, getRemoteIpAddress())) {
                    return true;
                }
            }
        }

        return false;
    }

    function Process_POS_TRANSACTION($CardID, $amount, $SiteName, $TransactionDetail, $InvoiceMethod, $TargetUserID = '', $ReturnTransactionLogID = false, $PaymentTime = '', $isAlipay=false)
    {
        global $sys_custom;
        $refCode_prefix = "SALES";
        $refCode_length = 9;
        $target_code = "";
        $username = $SiteName;

        $payment_time_val = $PaymentTime != '' ? "'" . date("Y-m-d H:i:s", strtotime($PaymentTime)) . "'" : 'NOW()';

        $this->Start_Trans_For_Lock_Table_Procedure();
        # Lock tables
        $sql = "LOCK TABLES 
						INTRANET_USER as u READ
						,PAYMENT_ACCOUNT WRITE 
						,PAYMENT_ACCOUNT as pa READ
						,PAYMENT_OVERALL_TRANSACTION_LOG WRITE
						,PAYMENT_PURCHASE_DETAIL_RECORD WRITE
						,POS_TRANSACTION WRITE
						,POS_ITEM WRITE 
						,POS_ITEM_LOG WRITE
						";
        $this->db_db_query($sql);

        # Retrieve UserID/ account balance
        $sql = "SELECT 
					u.UserID,
					u.EnglishName,
					u.ChineseName,
					u.ClassName,
					u.ClassNumber,
					pa.Balance 
				FROM  
					INTRANET_USER u
					INNER JOIN 
					PAYMENT_ACCOUNT pa 
					on u.UserID = pa.StudentID 
				WHERE ";
        if ($TargetUserID != '') {
            $sql .= " u.UserID = '$TargetUserID' ";
        } else {
            $sql .= " (CardID = '$CardID'" . ($sys_custom['SupplementarySmartCard'] ? " OR CardID2 = '$CardID' OR CardID3 = '$CardID' OR CardID4 = '$CardID'" : "") . ") ";
        }
        $sql .= "  AND 
					(RecordType = 1 OR RecordType = 2) 
					AND RecordStatus = 1 ";

        $temp = $this->returnArray($sql, 6);

        list($uid, $t_engName, $t_chiName, $t_className, $t_classNum, $balance) = $temp[0];
        if ($uid == "" || $uid == 0) {
            return -1; // user not found
        }

        if (($balance < $amount) && !$isAlipay && !$sys_custom['ePOS']['AllowNegativeBalance'])        # Not enough balance
        {
            $x = "0###" . number_format($balance, $this->DecimalPlace);
            $x .= "###" . date('Y-m-d H:i:s');
            return $ReturnTransactionLogID ? 0 : $x;
        }
        $LogDetail = $SiteName;
        $TransactionDetails = '';
        if (sizeof($TransactionDetail) > 0) {
            // Invoice Method
            $InvoiceNumber = $this->Get_Invoice_Number($InvoiceMethod);
            $NegativeItem = array();
            $delim_br = '';
            for ($i = 0; $i < sizeof($TransactionDetail); $i++) {
                $sql = 'select 
						ItemName,
	  					(ItemCount-' . $TransactionDetail[$i]['Quantity'] . ') as remain 
	  				from 
	  					POS_ITEM 
	  				where 
	  					ItemID = \'' . $TransactionDetail[$i]['ItemID'] . '\'';
                $Temp = $this->returnResultSet($sql);

                if ($Temp[0]['remain'] < 0) {
                    $NegativeItem[] = $TransactionDetail[$i]['ItemID'];
                }

                $ItemRemain[$TransactionDetail[$i]['ItemID']] = $Temp[0]['remain'];
                $more_values = "";
                if($sys_custom['ePOS']['PaymentMethod']) {
					$more_values .= ",'".$TransactionDetail[$i]['PaymentMethod']."'";
					$more_values .= ",'".$this->Get_Safe_Sql_Query($TransactionDetail[$i]['Remark'])."'";
				}
                $PurchaseDetailValues .= "$delim ('|--TransactionLogID--|','" . $TransactionDetail[$i]['ItemID'] . "','" . $TransactionDetail[$i]['Quantity'] . "','" . $TransactionDetail[$i]['UnitPrice'] . "',now(),now(),'$InvoiceNumber' $more_values)"; // detail purchase record insert values for later transaction
                $delim = ",";

                $TransactionDetails .= $delim_br . $Temp[0]['ItemName'] . "(" . $TransactionDetail[$i]['Quantity'] . ")";
                $delim_br = "<br>";
            }

            if (sizeof($NegativeItem) > 0) {
                return -4; // Not enough Inventory
            }
        } else {
            return -6; // Not item detail
        }


        // all checking ok, start transaction
        # Deduct balance
        if (!$isAlipay) {
            $sql = "UPDATE PAYMENT_ACCOUNT SET 
	  					Balance = Balance - $amount,
	  					LastUpdateByAdmin = NULL,
	  					LastUpdateByTerminal='" . $this->Get_Safe_Sql_Query($username) . "',
	  					LastUpdated = NOW()
						WHERE 
							StudentID = $uid";
            $Result['DeductAccountBalance'] = $this->db_db_query($sql);
        }

        # Insert Transaction Record
        $balanceAfter = $balance - $amount;
        $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG (
	  					StudentID, 
	  					TransactionType, 
	  					Amount, 
	  					RelatedTransactionID, 
	  					BalanceAfter, 
	  					TransactionTime, 
	  					Details,
						TransactionDetails 
	  					)
	          VALUES (
	          	$uid, 
	          	3,
	          	'$amount',
	          	NULL,
	          	'$balanceAfter',
	          	$payment_time_val,
	          	'" . $this->Get_Safe_Sql_Query($LogDetail) . "',
				'" . $this->Get_Safe_Sql_Query($TransactionDetails) . "')";
        $Result['InsertTransactionLog'] = $this->db_db_query($sql);
        $TransactionLogID = $this->db_insert_id();

        # Update RefCode
        $str_len = strlen($TransactionLogID);
        #$code_prefix = $refCode_prefix;

        if ($str_len < $refCode_length) {
            for ($i = 0; $i < ($refCode_length - $str_len); $i++) {
                $code_prefix .= "0";
            }
        }
        $RefCode = $code_prefix . $TransactionLogID;
        if (strlen($RefCode) > $refCode_length) {
            $RefCode = substr($RefCode, 0 - $refCode_length);
        }
        $RefCode = $refCode_prefix . $RefCode;
        $sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET 
	  					RefCode = '$RefCode' 
	  				WHERE 
	  					LogID = '$TransactionLogID'";
        $Result['InsertRefCode'] = $this->db_db_query($sql);

        $PurchaseDetailValues = str_replace('|--TransactionLogID--|', $TransactionLogID, $PurchaseDetailValues);

        $more_fields = "";
        if($sys_custom['ePOS']['PaymentMethod']) {
			$more_fields .= ",PaymentMethod";
			$more_fields .= ",Remark";
        }
        $sql = "INSERT IGNORE INTO PAYMENT_PURCHASE_DETAIL_RECORD (
							TransactionLogID, 
							ItemID, 
							ItemQty, 
							ItemSubTotal, 
							DateInput, 
							DateModified, 
							InvoiceNumber
							$more_fields
							)
						VALUES 
					 		" . $PurchaseDetailValues;
        //debug_r($sql);
        $Result['InsertPurchaseDetailRecord'] = $this->db_db_query($sql);

        $sql = "INSERT INTO POS_TRANSACTION (
							LogID,
							InvoiceNumber,
							ProcessBy,
							ProcessDate
							)
						values (
							'" . $TransactionLogID . "',
							'" . $InvoiceNumber . "',
							'" . $uid . "',
							NOW()
							)";
        //debug_r($sql);
        $Result['InsertPOSTransaction'] = $this->db_db_query($sql);

        for ($i = 0; $i < sizeof($TransactionDetail); $i++) {
            $sql = 'update POS_ITEM set 
  							ItemCount = ItemCount - ' . $TransactionDetail[$i]['Quantity'] . ',
  							ItemSalesTotal = ItemSalesTotal + ' . $TransactionDetail[$i]['Quantity'] . ' 
  						where 
  							ItemID = \'' . $TransactionDetail[$i]['ItemID'] . '\'';
            $Result['UpdateItemDetail:' . $TransactionDetail[$i]['ItemID']] = $this->db_db_query($sql);

            $sql = 'insert into POS_ITEM_LOG (
  							ItemID,
  							LogType,
  							RefLogID,
  							Amount,
  							CountAfter,
  							DateInput,
  							InputBy
  							)
  						values (
  							\'' . $TransactionDetail[$i]['ItemID'] . '\',
  							\'1\',
  							\'' . $TransactionLogID . '\',
  							\'' . $TransactionDetail[$i]['Quantity'] . '\',
  							\'' . $ItemRemain[$TransactionDetail[$i]['ItemID']] . '\',
  							NOW(),
  							\'' . $uid . '\'
  							)';
            $Result['InsertToItemLog:' . $TransactionDetail[$i]['ItemID']] = $this->db_db_query($sql);
        }

        $x = "1###" . number_format($balanceAfter, $this->DecimalPlace); // successfull transaction
        $x .= "###" . date('Y-m-d H:i:s');
        # Student information
        $x .= "###$t_engName###$t_chiName###$t_className###$t_classNum";
        $x .= "###$RefCode";
        $x .= "###$InvoiceNumber";

        if (!in_array(false, $Result)) {
            $this->Commit_Trans();
        } else {
            $this->RollBack_Trans();
        }

        $sql = "UNLOCK TABLES";
        $unlockResult = $this->db_db_query($sql);

        //debug_r($Result);
        if (!in_array(false, $Result)) {
            return $ReturnTransactionLogID ? $TransactionLogID : $x;
        } else {
            return -5; // sql error
        }
    }

    function Get_Invoice_Number($InvoiceMethod)
    {
        switch ($InvoiceMethod) {
            case 0: // YYYYMMDDXXXX: XXXX (0001 - 9999)
                $sql = "Select 
  								count(1)
  							From 
  								POS_TRANSACTION 
  							where 
  								ProcessDate like '" . date('Y-m-d') . "%' 
  							";
                $CurrentRecord = $this->returnVector($sql);

                $InvoiceNumber = date('Ymd');
                if ($CurrentRecord[0] == 0) {
                    $InvoiceNumber .= '0001';
                } else {
                    //	for ($i=4; $i > strlen($CurrentRecord[0]); $i--)
                    //		$InvoiceNumber .= '0';
                    //	$InvoiceNumber .= ($CurrentRecord[0]+1);
                    $InvoiceNumber .= sprintf("%04d", $CurrentRecord[0] + 1);
                }
                break;
            default:
                $sql = "Select 
  								count(1)
  							From 
  								POS_TRANSACTION 
  							where 
  								ProcessDate like '" . date('Y-m-d') . "%' 
  							";
                $CurrentRecord = $this->returnVector($sql);

                $InvoiceNumber = date('Ymd');
                if ($CurrentRecord[0] == 0) {
                    $InvoiceNumber .= '0001';
                } else {
                    //	for ($i=4; $i > $CurrentRecord[0]; $i--)
                    //		$InvoiceNumber .= '0';
                    //	$InvoiceNumber .= ($CurrentRecord[0]+1);
                    $InvoiceNumber .= sprintf("%04d", $CurrentRecord[0] + 1);
                }
                break;
        }

        return $InvoiceNumber;
    }

    // End of API functions

    function Get_Manage_Transaction_Index_Sql($Keyword, $FromDate, $ToDate)
    {
        global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $intranet_session_language;

        $namefield = getNameFieldByLang("u."); // transaction owner
        $pnamefield = getNameFieldByLang("pu."); // prcess by
        $vnamefield = getNameFieldByLang("vu."); // void by
        if ($intranet_session_language == "b5" || $intranet_session_language == "gb") {
            $archive_namefield = " IF(au.ChineseName IS NULL,au.EnglishName,au.ChineseName)";
            $parchive_namefield = " IF(pau.ChineseName IS NULL,pau.EnglishName,pau.ChineseName)";
            $varchive_namefield = " IF(vau.ChineseName IS NULL,vau.EnglishName,vau.ChineseName)";
        } else {
            $archive_namefield = " IF(au.EnglishName IS NULL,au.ChineseName,au.EnglishName)";
            $parchive_namefield = " IF(pau.EnglishName IS NULL,pau.ChineseName,pau.EnglishName)";
            $varchive_namefield = " IF(vau.EnglishName IS NULL,vau.ChineseName,vau.EnglishName)";
        }
        $sql = "SELECT
							CASE 
								WHEN pt.VoidLogID IS NOT NULL AND pt.InvoiceNumber Is NOT NULL AND pt.InvoiceNumber <> '' THEN pt.InvoiceNumber 
								WHEN pt.VoidLogID IS NOT NULL AND (pt.InvoiceNumber Is NULL OR pt.InvoiceNumber = '') THEN CONCAT('" . $Lang['ePayment']['NoInvoiceNumber'] . "') 
								WHEN pt.VoidLogID IS NULL AND pt.InvoiceNumber Is NOT NULL AND pt.InvoiceNumber <> '' THEN CONCAT('<a class=\"tablelink\" href=\"" . $PATH_WRT_ROOT . "/home/eAdmin/GeneralMgmt/pos/report/pos_transaction_report/detail.php?InvNo=',pt.InvoiceNumber,'&LogID=',pt.LogID,'\" target=\"_blank\">',pt.InvoiceNumber,'</a>') 
								ELSE CONCAT('<a class=\"tablelink\" href=\"" . $PATH_WRT_ROOT . "/home/eAdmin/GeneralMgmt/pos/report/pos_transaction_report/detail.php?LogID=',pt.LogID,'\" target=\"_blank\">" . $Lang['ePayment']['NoInvoiceNumber'] . "</a>') 
							END as InvoiceNumber,
							IF(u.UserID IS NULL,CONCAT('<i>',IF(au.ClassName IS NULL OR au.ClassName = '','--',au.ClassName),'</i>'),IF(u.ClassName IS NULL OR u.ClassName = '','--',u.ClassName)) as ClassName,
							IF(u.UserID IS NULL,CONCAT('<i>',IF(au.ClassNumber IS NULL OR au.ClassNumber = '','--',au.ClassNumber),'</i>'),IF(u.ClassNumber IS NULL OR u.ClassNumber = '','--',u.ClassNumber)) as ClassNumber,
							IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
						  CONCAT('" . $this->CurrencySign . "',ROUND(SUM((ppdr.ItemQty-(IFNULL(ppdr.VoidAmount,0)))*ppdr.ItemSubTotal)," . $this->DecimalPlace . ")) AS GrandTotal,
						  potl.REFCode,
						  IF(pu.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$parchive_namefield,'</i>'), IF(pau.UserID IS NULL AND pu.UserID IS NULL,'--',$pnamefield)) as ProcessName,
						  pt.ProcessDate,
						  IF(vu.UserID IS NULL,IF(vau.UserID IS NULL,'--',CONCAT('<font color=red>*</font><i>',$varchive_namefield,'</i>')), IF(vau.UserID IS NULL AND vu.UserID IS NULL,'--',$vnamefield)) as VoidName,
						  IF(pt.VoidDate IS NULL OR pt.VoidDate = '','--',pt.VoidDate) as VoidDate,
						  IF (pt.VoidLogID IS NULL,
						  concat('<div class=\"table_row_tool\">
				    		<a href=\"#TB_inline?height=450&width=750&inlineId=FakeLayer\" class=\"thickbox delete\" title=\"" . $Lang['ePOS']['Void'] . "\" onclick=\"Get_Void_Transaction_Form(\'',pt.LogID,'\'); return false\"></a>
				    	</div>'),CONCAT('<a title=\"" . $Lang['Btn']['Edit'] . " " . $Lang['ePOS']['Remark'] . "\" class=\"thickbox\" href=\"#TB_inline?height=450&width=750&inlineId=FakeLayer\" onclick=\"Get_Void_Transaction_Form(\'',pt.LogID,'\'); return false;\"><img height=\"20\" width=\"20\" border=\"0\" align=\"absmiddle\" src=\"" . $PATH_WRT_ROOT . "/images/" . $LAYOUT_SKIN . "/icon_remark.gif\"></a>')) as FunctionLink, 
				    	ROUND(SUM(ppdr.ItemQty*ppdr.ItemSubTotal)," . $this->DecimalPlace . ") AS SortGrandTotal 
						from 
							POS_TRANSACTION as pt 
							inner join 
						  PAYMENT_PURCHASE_DETAIL_RECORD as ppdr 
						  on pt.LogID = ppdr.TransactionLogID
						  inner join
						  PAYMENT_OVERALL_TRANSACTION_LOG as potl
						  on ppdr.TransactionLogID = potl.LogID
						  LEFT JOIN
						  INTRANET_USER as u
						  on potl.StudentID = u.UserID
						  LEFT JOIN
						  INTRANET_ARCHIVE_USER as au
						  on potl.StudentID = au.UserID 
						  LEFT JOIN 
						  INTRANET_USER as pu 
						  on pt.ProcessBy = pu.UserID 
						  LEFT JOIN
						  INTRANET_ARCHIVE_USER as pau
						  on pt.ProcessBy = pau.UserID 
						  LEFT JOIN 
						  INTRANET_USER as vu 
						  on pt.VoidBy = vu.UserID 
						  LEFT JOIN
						  INTRANET_ARCHIVE_USER as vau
						  on pt.VoidBy = vau.UserID 
	         WHERE
					(pt.ProcessDate between '" . $FromDate . " 00:00:00' and '" . $ToDate . " 23:59:59') 
                    AND ppdr.ItemQty>=(IFNULL(ppdr.VoidAmount,0)) ";
        if (trim($Keyword) != "") {
            $sql .= " AND (
							 u.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 u.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 u.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 u.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 au.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 au.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 au.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 au.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR 
							 pu.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 pu.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 pu.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 pu.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 pau.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 pau.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 pau.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 pau.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR 
							 vu.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 vu.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 vu.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 vu.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 vau.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 vau.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 vau.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR
							 vau.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR 
							 potl.RefCode LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%' OR 
							 pt.InvoiceNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%'  
							) ";
        }
        $sql .= "Group By 
						pt.LogID
						";
//  		debug_r($sql);
        return $sql;
    }

    function Void_POS_Transaction($TransactionLogID, $Remark, $InventoryReturn = array())
    {
        $this->Start_Trans_For_Lock_Table_Procedure();

        $sql = 'LOCK TABLES 
							PAYMENT_OVERALL_TRANSACTION_LOG WRITE,
							PAYMENT_ACCOUNT WRITE,
							POS_TRANSACTION WRITE,
							PAYMENT_PURCHASE_DETAIL_RECORD WRITE,
							POS_ITEM WRITE,
							POS_ITEM_LOG WRITE';
        $this->db_db_query($sql);

        if (sizeof($InventoryReturn) > 0) { // void transaction
            $sql = 'select 
								StudentID,
								Amount,
								RefCode 
							from 
								PAYMENT_OVERALL_TRANSACTION_LOG 
							where 
								LogID = \'' . $TransactionLogID . '\'';
            $Temp = $this->returnArray($sql);
            $StudentID = $Temp[0]['StudentID'];
            $OriginalAmount = $Temp[0]['Amount'];
            $RefCode = $Temp[0]['RefCode'];

            $refundAmount = 0.0;

            $sql = 'select 
						ItemID, 
						ItemQty - VoidAmount as ItemQty,
						ItemSubTotal 
					from 
						PAYMENT_PURCHASE_DETAIL_RECORD 
					where 
						TransactionLogID = \'' . $TransactionLogID . '\' AND ItemQty - VoidAmount > 0';
            $ItemDetail = $this->returnArray($sql);

            for ($i = 0; $i < count($ItemDetail); $i++) {
                if (isset($InventoryReturn[$ItemDetail[$i]['ItemID']]) && $InventoryReturn[$ItemDetail[$i]['ItemID']] > 0) {
                    $refundAmount += $InventoryReturn[$ItemDetail[$i]['ItemID']] * $ItemDetail[$i]['ItemSubTotal'];
                }
            }

            if ($refundAmount > 0.0) {
                $sql = 'select Balance from PAYMENT_ACCOUNT where StudentId = \'' . $StudentID . '\'';
                $Temp = $this->returnVector($sql);
                $AccountBalance = $Temp[0];

                $sql = 'update PAYMENT_ACCOUNT set 
									Balance = Balance + ' . $refundAmount . ' 
								where 
									StudentID = \'' . $StudentID . '\'';
                $Result['UpdateAccountBalance'] = $this->db_db_query($sql);

                $sql = 'insert into PAYMENT_OVERALL_TRANSACTION_LOG (
									StudentID,
									TransactionType,
									Amount,
									BalanceAfter, 
									TransactionTime,
									Details,
									RefCode
									)
								values (
									\'' . $StudentID . '\',
									\'11\',
									\'' . $refundAmount . '\',
									\'' . ($AccountBalance + $refundAmount) . '\',
									NOW(),
									\'Transaction Void Refund\',
									\'' . $RefCode . '\'
									)';
                $Result['InsertRefundTransactionLog'] = $this->db_db_query($sql);
                $VoidTransactionLog = $this->db_insert_id();

                $sql = 'update POS_TRANSACTION set 
									VoidBy = \'' . $_SESSION['UserID'] . '\', 
									VoidDate = NOW(), 
									VoidLogID = \'' . $VoidTransactionLog . '\', 
									Remark = \'' . $this->Get_Safe_Sql_Query($Remark) . '\'
								where 
									LogID = \'' . $TransactionLogID . '\'';
                $Result['UpdateVoidInformation'] = $this->db_db_query($sql);
                /*
				$sql = 'select
									ItemID,
									ItemQty
								from
									PAYMENT_PURCHASE_DETAIL_RECORD
								where
									TransactionLogID = \''.$TransactionLogID.'\'';
				$ItemDetail = $this->returnArray($sql);
				*/
                for ($i = 0; $i < sizeof($ItemDetail); $i++) {
                    $sql = 'select 
										ItemCount 
									from 
										POS_ITEM 
									where 
										ItemID = \'' . $ItemDetail[$i]['ItemID'] . '\'';
                    $Temp = $this->returnVector($sql);
                    $OriginalItemCount = $Temp[0];

                    $sql = 'update POS_ITEM set 
										ItemCount = ItemCount + ' . $InventoryReturn[$ItemDetail[$i]['ItemID']] . ',
										ItemSalesTotal = ItemSalesTotal - ' . $ItemDetail[$i]['ItemQty'] . ' 
									where 
										ItemID = \'' . $ItemDetail[$i]['ItemID'] . '\'';
                    //debug_r($sql);
                    $Result['ResetItemCount:' . $ItemDetail[$i]['ItemID']] = $this->db_db_query($sql);

                    if ($InventoryReturn[$ItemDetail[$i]['ItemID']] > 0) {
                        $sql = 'insert into POS_ITEM_LOG (
											ItemID,
											LogType,
											RefLogID,
											Amount,
											CountAfter,
											DateInput,
											InputBy 
											)
										values (
											\'' . $ItemDetail[$i]['ItemID'] . '\',
											\'2\',
											\'' . $VoidTransactionLog . '\',
											\'' . $InventoryReturn[$ItemDetail[$i]['ItemID']] . '\',
											\'' . ($OriginalItemCount + $InventoryReturn[$ItemDetail[$i]['ItemID']]) . '\',
											NOW(),
											\'' . $_SESSION['UserID'] . '\'
											)';
                        //debug_r($sql);
                        $Result['InsertItemChangeLog:' . $ItemDetail[$i]['ItemID']] = $this->db_db_query($sql);

                        //update the void amount [Henry Added 20190227]
                        $sql = 'update PAYMENT_PURCHASE_DETAIL_RECORD set 
										VoidAmount = VoidAmount + ' . $InventoryReturn[$ItemDetail[$i]['ItemID']] . ',
										VoidAmountDateModify = NOW(),
										VoidAmountModifyBy = \'' . $_SESSION['UserID'] . '\'
									where 
										ItemID = \'' . $ItemDetail[$i]['ItemID'] . '\' AND TransactionLogID = \'' . $TransactionLogID . '\'';
                        //debug_r($sql);
                        $Result['UpdateVoidAmount:' . $ItemDetail[$i]['ItemID']] = $this->db_db_query($sql);
                    }
                }
            } else {
//                $Result['RefundAmount'] = false;
                # case when all item to return is zero (e.g. damage), no refund, just void the transaction, add dummy VoidLogID=-1 to indicate the status is void
                $sql = 'update POS_TRANSACTION set 
									VoidBy = \'' . $_SESSION['UserID'] . '\', 
									VoidDate = NOW(),
									VoidLogID = -1, 
									Remark = \'' . $this->Get_Safe_Sql_Query($Remark) . '\'
								where 
									LogID = \'' . $TransactionLogID . '\'';
                $Result['UpdateVoidInformation'] = $this->db_db_query($sql);
            }
        } else {
            $sql = 'update POS_TRANSACTION set 
								Remark = \'' . $this->Get_Safe_Sql_Query($Remark) . '\'
							where 
								LogID = \'' . $TransactionLogID . '\'';
            $Result['UpdateVoidRemarks'] = $this->db_db_query($sql);
        }

        if (!in_array(false, $Result))
            $this->Commit_Trans();
        else
            $this->RollBack_Trans();
        //$this->RollBack_Trans();

        $sql = 'UNLOCK TABLES';
        $this->db_db_query($sql);

        return !in_array(false, $Result);
    }

    function Get_POS_Transaction_Detail($TransactionLogID)
    {
        global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;

        $namefield = getNameFieldWithClassNumberByLang("u."); // transaction owner
        $pnamefield = getNameFieldWithClassNumberByLang("pu."); // prcess by
        $vnamefield = getNameFieldWithClassNumberByLang("vu."); // void by
        if ($intranet_session_language == "b5" || $intranet_session_language == "gb") {
            $archive_namefield = " IF(au.ChineseName IS NULL,au.EnglishName,au.ChineseName)";
            $parchive_namefield = " IF(pau.ChineseName IS NULL,pau.EnglishName,pau.ChineseName)";
            $varchive_namefield = " IF(vau.ChineseName IS NULL,vau.EnglishName,vau.ChineseName)";
        } else {
            $archive_namefield = " IF(au.EnglishName IS NULL,au.ChineseName,au.EnglishName)";
            $parchive_namefield = " IF(pau.EnglishName IS NULL,pau.ChineseName,pau.EnglishName)";
            $varchive_namefield = " IF(vau.EnglishName IS NULL,vau.ChineseName,vau.EnglishName)";
        }
        $sql = "SELECT
							CASE 
								WHEN pt.InvoiceNumber Is NOT NULL AND pt.InvoiceNumber <> '' THEN pt.InvoiceNumber 
								ELSE '" . $Lang['ePayment']['NoInvoiceNumber'] . "' 
							END as InvoiceNumber,
							IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
						  ROUND(SUM((ppdr.ItemQty-ppdr.VoidAmount)*ppdr.ItemSubTotal)," . $this->DecimalPlace . ") AS GrandTotal,
						  potl.REFCode,
						  IF(pu.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$parchive_namefield,'</i>'), IF(pau.UserID IS NULL AND pu.UserID IS NULL,'--',$pnamefield)) as ProcessName,
						  pt.ProcessDate,
						  IF(vu.UserID IS NULL,IF(vau.UserID IS NULL,'--',CONCAT('<font color=red>*</font><i>',$varchive_namefield,'</i>')), IF(vau.UserID IS NULL AND vu.UserID IS NULL,'--',$vnamefield)) as VoidName,
						  IF(pt.VoidDate IS NULL OR pt.VoidDate = '','--',pt.VoidDate) as VoidDate,
						  pt.Remark,
						  pt.VoidLogID,
						  CASE 
						  	WHEN pt.VoidLogID IS NULL THEN '0' 
						  	ELSE '1' 
						  END as IsTransactionVoided 
						from 
							POS_TRANSACTION as pt 
							inner join 
						  PAYMENT_PURCHASE_DETAIL_RECORD as ppdr 
						  on pt.LogID = ppdr.TransactionLogID
						  inner join
						  PAYMENT_OVERALL_TRANSACTION_LOG as potl
						  on ppdr.TransactionLogID = potl.LogID
						  LEFT JOIN
						  INTRANET_USER as u
						  on potl.StudentID = u.UserID
						  LEFT JOIN
						  INTRANET_ARCHIVE_USER as au
						  on potl.StudentID = au.UserID 
						  LEFT JOIN 
						  INTRANET_USER as pu 
						  on pt.ProcessBy = pu.UserID 
						  LEFT JOIN
						  INTRANET_ARCHIVE_USER as pau
						  on pt.ProcessBy = pau.UserID 
						  LEFT JOIN 
						  INTRANET_USER as vu 
						  on pt.VoidBy = vu.UserID 
						  LEFT JOIN
						  INTRANET_ARCHIVE_USER as vau
						  on pt.VoidBy = vau.UserID 
						where 
							pt.LogID = '" . $TransactionLogID . "' 
						Group By 
							pt.LogID 
					 ";
        //debug_r($sql);
        return $this->returnArray($sql);
    }

    function Get_POS_Transaction_Purchase_Detail($TransactionLogID, $hasAvailableItem=true)
    {
        if ($hasAvailableItem) {
            $qtyField = "pdr.ItemQty - pdr.VoidAmount";
            $condition = " AND pdr.ItemQty - pdr.VoidAmount > 0 ";
        }
        else {
            $qtyField = "pdr.ItemQty";
            $condition = "";
        }

        $sql = 'select 
							pdr.ItemID,
							i.ItemName,
							i.Barcode,
							'.$qtyField.' as ItemQty,
							pdr.ItemSubTotal, 
							ic.CategoryName 
						from 
							PAYMENT_PURCHASE_DETAIL_RECORD pdr 
							inner join 
							POS_ITEM i 
							on pdr.ItemID = i.ItemID 
							inner join 
							POS_ITEM_CATEGORY ic 
							on i.CategoryID = ic.CategoryID 
						where 
							TransactionLogID = \'' . $TransactionLogID . '\''.$condition.'  
						order by 
							ic.sequence, i.sequence 
							';
        return $this->returnArray($sql);
    }

    function Get_POS_Transaction_Void_Detail($TransactionLogID)
    {
        $sql = 'select 
							i.ItemID,
							i.Barcode, 
							i.ItemName,
							il.Amount 
						from 
							POS_ITEM_LOG il 
							inner join 
							POS_ITEM i 
							on il.RefLogID = \'' . $TransactionLogID . '\' 
								and 
								il.LogType = \'2\'
								and
                il.ItemID = i.ItemID';
        //debug_r($sql);
        $Temp = $this->returnArray($sql);

        for ($i = 0; $i < sizeof($Temp); $i++) {
            $Return[$Temp[$i]['ItemID']] = $Temp[$i];
        }

        return $Return;
    }

    function Get_Terminal_List($Keyword = "", $TemplateID = "")
    {
        global $Lang;

        if ($TemplateID == "") {
            $TemplateCond = 'TemplateID = \'' . $TemplateID . '\' 
											 OR 
											 TemplateID IS NULL 
											';
        } else {
            $TemplateCond = 'TemplateID = \'' . $TemplateID . '\' ';
        }
        $sql = 'select 
							TerminalID,
							SiteName,
							MacAddress,
							IPAddress,
							LastConnected,
							CASE 
								WHEN LastPhotoSync IS NULL OR LastPhotoSync = \'\' THEN \'' . $Lang['ePOS']['SynconizingImages'] . '\' 
								ELSE LastPhotoSync 
							END as LastPhotoSync,
							TerminalStatus 
						from 
							POS_TERMINAL_INFO 
						where 
							' . $TemplateCond . '
							AND 
							(
							SiteName like \'%' . $this->Get_Safe_Sql_Like_Query($Keyword) . '%\' 
							OR 
							IPAddress like \'%' . $this->Get_Safe_Sql_Like_Query($Keyword) . '%\' 
							OR 
							MacAddress like \'%' . $this->Get_Safe_Sql_Like_Query($Keyword) . '%\' 
							)
					 ';
        //debug_r($sql);
        return $this->returnArray($sql);
    }

    function Get_Terminal_Records($map)
    {
        $cond = "";
        if (isset($map['Terminal'])) {
            $terminal_id = IntegerSafe($map['TerminalID']);
            $conds .= " AND TerminalID" . (is_array($terminal_id) ? " IN (" . implode(",", $terminal_id) . ") " : "='$terminal_id' ");
        }
        if (isset($map['SiteName'])) {
            $site_name = $map['SiteName'];
            if (is_array($site_name)) {
                $cond .= " AND SiteName IN (";
                $sep = "";
                foreach ($site_name as $this_site_name) {
                    $cond .= $sep . "'" . $this_site_name . "'";
                    $sep = ",";
                }
                $cond .= ") ";
            } else {
                $cond .= " AND SiteName='" . $this->Get_Safe_Sql_Query($site_name) . "' ";
            }
        }
        if (isset($map['MacAddress'])) {
            $mac_address = $map['MacAddress'];
            if (is_array($mac_address)) {
                $cond .= " AND MacAddress IN (";
                $sep = "";
                foreach ($mac_address as $this_mac_address) {
                    $cond .= $sep . "'" . $this_mac_address . "'";
                    $sep = ",";
                }
                $cond .= ") ";
            } else {
                $cond .= " AND MacAddress='" . $this->Get_Safe_Sql_Query($mac_address) . "' ";
            }
        }
        if (isset($map['IPAddress'])) {
            $val = $map['IPAddress'];
            if (is_array($val)) {
                $cond .= " AND IPAddress IN (";
                $sep = "";
                foreach ($val as $this_val) {
                    $cond .= $sep . "'" . $this_val . "'";
                    $sep = ",";
                }
                $cond .= ") ";
            } else {
                $cond .= " AND IPAddress='" . $this->Get_Safe_Sql_Query($val) . "' ";
            }
        }
        if (isset($map['TemplateID'])) {
            $id = IntegerSafe($map['TemplateID']);
            $conds .= " AND TemplateID" . (is_array($id) ? " IN (" . implode(",", $id) . ") " : "='$id' ");
        }
        if (isset($map['TerminalStatus'])) {
            $val = $map['TerminalStatus'];
            $conds .= " AND TerminalStatus" . (is_array($val) ? " IN ('" . implode("','", $val) . "') " : "='$val' ");
        }

        $sql = "SELECT * FROM POS_TERMINAL_INFO WHERE 1 " . $cond;
        $records = $this->returnResultSet($sql);
        return $records;
    }

    function Get_Terminal_Template_List($Keyword = "")
    {
        global $Lang;
        $sql = 'select 
							ptt.TemplateID,
							ptt.TemplateName,
							count(ptcl.TemplateID) as CategoryAssociated
						from 
							POS_TERMINAL_SITE_TEMPLATE ptt 
							LEFT JOIN 
							POS_TERMINAL_CATEGORY_LINKAGE ptcl
							on ptt.TemplateID = ptcl.TemplateID 
						where 
							TemplateName like \'%' . $this->Get_Safe_Sql_Like_Query($Keyword) . '%\' 
						Group By
							ptt.TemplateID 
						order by 
							ptt.TemplateName
							';
        //debug_r($sql);
        return $this->returnArray($sql);
    }

    function Log_Terminal_Connection_Info($SiteName, $MacAddress, $TerminalID = "")
    {
        if ($TerminalID == "") {
            $sql = 'select 
								count(1) 
							from 
								POS_TERMINAL_INFO 
							where 
								SiteName = \'' . $this->Get_Safe_Sql_Query($SiteName) . '\' 
								and 
								MacAddress != \'' . $this->Get_Safe_Sql_Query($MacAddress) . '\'';
            $SiteNameCrush = $this->returnVector($sql);
            $SiteNameCrush = $SiteNameCrush[0];
        } else {
            $SiteNameCrush = 0;
        }

        if ($SiteNameCrush > 0) {
            return false;
        } else {
            $sql = 'insert into POS_TERMINAL_INFO (
								SiteName,
								MacAddress,
								IPAddress,
								LastConnected 
								)
							values (
								\'' . $this->Get_Safe_Sql_Query($SiteName) . '\', 
								\'' . $this->Get_Safe_Sql_Query($MacAddress) . '\', 
								\'' . getRemoteIpAddress() . '\',
								NOW()
								) 
							on duplicate key update 
								TerminalID = LAST_INSERT_ID(TerminalID), 
								IPAddress = \'' . getRemoteIpAddress() . '\',
								SiteName = \'' . $this->Get_Safe_Sql_Query($SiteName) . '\', 
								MacAddress = \'' . $this->Get_Safe_Sql_Query($MacAddress) . '\', 
								LastConnected =  NOW()';
            //debug_r($sql);
            $Result = $this->db_db_query($sql);
            $TerminalID = $this->db_insert_id();

            if ($Result) {
                return $TerminalID;
            } else {
                return false;
            }
        }
    }

    function Get_Item_Category_Info_For_Client_Program($MacAddress)
    {
        if ($this->Settings['TerminalIndependentCat'] == '1') {
            $sql = 'select 
								c.CategoryID,
								c.CategoryCode,
								c.CategoryName,
								c.PhotoExt as CatExt,
								i.ItemID,
								i.ItemName,
								i.Barcode,
								i.UnitPrice,
								IFNULL(i.ItemCount,\'0\') ItemCount,
								i.PhotoExt as ItemExt 
							from 
								POS_TERMINAL_INFO pti 
								INNER JOIN 
								POS_TERMINAL_CATEGORY_LINKAGE ptcl 
								on pti.MacAddress = \'' . $this->Get_Safe_Sql_Query($MacAddress) . '\' 
									and 
									pti.TemplateID = ptcl.TemplateID 
								INNER JOIN 
								POS_ITEM_CATEGORY c 
								on ptcl.CategoryID = c.CategoryID 
								LEFT JOIN
								POS_ITEM i 
								on c.CategoryID = i.CategoryID 
									and 
									i.RecordStatus = \'1\' 
							order by 
								c.Sequence, c.CategoryName, i.Sequence, i.ItemName
							';
        } else {
            $sql = 'select 
								c.CategoryID,
								c.CategoryCode,
								c.CategoryName,
								c.PhotoExt as CatExt,
								i.ItemID,
								i.ItemName,
								i.Barcode,
								i.UnitPrice,
								IFNULL(i.ItemCount,\'0\') ItemCount,
								i.PhotoExt as ItemExt 
							from 
								POS_ITEM_CATEGORY c 
								LEFT JOIN
								POS_ITEM i 
								on c.CategoryID = i.CategoryID 
									and 
									i.RecordStatus = \'1\' 
							where 
								c.RecordStatus = \'1\' 
							order by 
								c.Sequence, c.CategoryName, i.Sequence, i.ItemName
							';
        }

        //debug_r($sql);
        return $this->returnArray($sql);
    }

    function Check_Terminal_Need_To_Sync_Photo($MacAddress)
    {
        $sql = 'select 
							count(1) 
						from 
							POS_TERMINAL_INFO 
						where 
							MacAddress = \'' . $this->Get_Safe_Sql_Query($MacAddress) . '\' 
							and 
							(
              	(
	              LastPhotoSync IS NULL
	              AND
	              LastPhotoRequest IS NOT NULL
              	)
              	OR
								LastPhotoSync < LastPhotoRequest
							)';
        //debug_r($sql);
        $Temp = $this->returnVector($sql);

        return ($Temp[0] > 0);
    }

    function Check_Terminal_Template_Name($TemplateName, $TemplateID = "")
    {
        $sql = 'select 
							count(1) 
						from 
							POS_TERMINAL_SITE_TEMPLATE 
						where 
							TemplateID != \'' . $TemplateID . '\' 
							and 
							TemplateName = \'' . $this->Get_Safe_Sql_Query($TemplateName) . '\'';
        $Temp = $this->returnVector($sql);

        return ($Temp[0] > 0) ? false : true;
    }

    function Save_Terminal($SiteName, $TerminalID = "")
    {
        if ($TerminalID == "") {
            $sql = 'insert into POS_TERMINAL_INFO (
								SiteName 
								)
							values (
								\'' . $this->Get_Safe_Sql_Query($SiteName) . '\'
								)';
        } else {
            $sql = 'update POS_TERMINAL_INFO set 
								SiteName = \'' . $this->Get_Safe_Sql_Query($SiteName) . '\' 
							where 
								TerminalID = \'' . $TerminalID . '\'';
        }
        //debug_r($sql);
        return $this->db_db_query($sql);
    }

    function Save_Terminal_Template($TemplateName, $TemplateID = "")
    {
        if ($TemplateID == "") {
            $sql = 'insert into POS_TERMINAL_SITE_TEMPLATE (
								TemplateName,
								InputDate,
								InputBy
								)
							values (
								\'' . $this->Get_Safe_Sql_Query($TemplateName) . '\',
								NOW(),
								\'' . $_SESSION['UserID'] . '\'
								)';
        } else {
            $sql = 'update POS_TERMINAL_SITE_TEMPLATE set 
								TemplateName = \'' . $this->Get_Safe_Sql_Query($TemplateName) . '\', 
								ModifyDate = NOW(), 
								ModifyBy = \'' . $_SESSION['UserID'] . '\' 
							where 
								TemplateID = \'' . $TemplateID . '\'';
        }
        //debug_r($sql);
        return $this->db_db_query($sql);
    }

    function Get_Terminal_Detail($TerminalID)
    {
        global $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;

        $namefield = getNameFieldByLang("u."); // last modified inventory user
        if ($intranet_session_language == "b5" || $intranet_session_language == "gb") {
            $archive_namefield = " IF(au.ChineseName IS NULL,au.EnglishName,au.ChineseName)";
        } else {
            $archive_namefield = " IF(au.EnglishName IS NULL,au.ChineseName,au.EnglishName)";
        }

        $sql = 'select 
							pti.SiteName,
							pti.MacAddress,
							pti.IPAddress,
							pti.LastConnected,
							pti.LastPhotoSync, 
							pti.LastPhotoRequest,
							pti.TemplateID,
							CASE 
								WHEN pti.LastRequestBy IS NULL THEN \'\' 
								WHEN u.UserID IS NULL AND au.UserID IS NULL THEN \'<font style="color:red;">*</font>\' 
								WHEN u.UserID IS NULL THEN CONCAT(\'<font style="color:red;"><i>\',' . $archive_namefield . ',\'</i></font>\') 
								ELSE ' . $namefield . ' 
							END as LastRequestBy
						from 
							POS_TERMINAL_INFO as pti 
							LEFT JOIN 
							INTRANET_USER as u 
							on pti.LastRequestBy = u.UserID 
							LEFT JOIN 
							INTRANET_ARCHIVE_USER as au 
							on pti.LastRequestBy = au.UserID 
						where 
							TerminalID = \'' . $TerminalID . '\'';
        //debug_r($sql);
        return $this->returnArray($sql);
    }

    function Get_Terminal_Template_Detail($TemplateID)
    {
        $sql = 'select 
							TemplateName 
						from 
							POS_TERMINAL_SITE_TEMPLATE 
						where 
							TemplateID = \'' . $TemplateID . '\'';
        //debug_r($sql);
        return $this->returnArray($sql);
    }

    function Clear_Terminal_Linkage($TerminalID)
    {
        $sql = 'update POS_TERMINAL_INFO set 
							MacAddress = NULL,
							IPaddress = NULL,
							LastConnected = NULL,
							LastPhotoSync = NULL,
							ModifyDate = NOW(),
							ModifyBy = \'' . $_SESSION['UserID'] . '\' 
						where 
							TerminalID = \'' . $TerminalID . '\'';
        return $this->db_db_query($sql);
    }

    function Toggle_Category_Terminal_Linkage($TemplateID, $CategoryID, $Active)
    {
        if ($Active == "1") {
            $sql = 'insert into POS_TERMINAL_CATEGORY_LINKAGE (
								TemplateID,
								CategoryID,
								DateInput,
								InputBy
								)
							values (
								\'' . $TemplateID . '\',
								\'' . $CategoryID . '\',
								NOW(),
								\'' . $_SESSION['UserID'] . '\'
								)
							on duplicate key update 
								DateInput = NOW(),
								InputBy = \'' . $_SESSION['UserID'] . '\'';
        } else {
            $sql = 'delete from POS_TERMINAL_CATEGORY_LINKAGE 
							where 
								TemplateID = \'' . $TemplateID . '\' 
								and 
								CategoryID = \'' . $CategoryID . '\'';
        }

        return $this->db_db_query($sql);
    }

    function Delete_Terminal($TerminalID)
    {
        $sql = 'delete from POS_TERMINAL_INFO 
						where 
							TerminalID = \'' . $TerminalID . '\'';
        $Result['RemoveTerminalInfo'] = $this->db_db_query($sql);

        $sql = 'delete from POS_TERMINAL_PHOTO_SYNC_HISTORY 
						where 
							TerminalID = \'' . $TerminalID . '\'';
        $Result['RemoveTerminalUpdateLog'] = $this->db_db_query($sql);

        //debug_r($Result);
        return !in_array(false, $Result);
    }

    function Delete_Terminal_Template($TemplateID)
    {
        $sql = 'delete from POS_TERMINAL_CATEGORY_LINKAGE 
						where 
							TemplateID = \'' . $TemplateID . '\'';
        $Result['RemoveCatRelation'] = $this->db_db_query($sql);

        $sql = 'update POS_TERMINAL_INFO set 
							TemplateID = NULL 
						where 
							TemplateID = \'' . $TemplateID . '\'';
        $Result['ReassignStation'] = $this->db_db_query($sql);

        $sql = 'delete from POS_TERMINAL_SITE_TEMPLATE 
						where 
							TemplateID = \'' . $TemplateID . '\'';
        $Result['DeleteTemplate'] = $this->db_db_query($sql);

        return !in_array(false, $Result);
    }

    function Save_Terminal_Template_Selection($TerminalID, $TemplateID)
    {
        $sql = 'update POS_TERMINAL_INFO set 
							TemplateID = \'' . $TemplateID . '\' 
						where 
							TerminalID = \'' . $TerminalID . '\'';

        return $this->db_db_query($sql);
    }

    function Sync_Terminal_Photo($TerminalID = "")
    {
        $sql = 'insert into POS_TERMINAL_PHOTO_SYNC_HISTORY (
							TerminalID,
							RequestDate,
							RequestBy
							)
						values (
							\'' . $TerminalID . '\',
							NOW(),
							\'' . $_SESSION['UserID'] . '\'
							)';
        //debug_r($sql);
        $Result['InsertHistory'] = $this->db_db_query($sql);

        $sql = 'update POS_TERMINAL_INFO set 
							LastPhotoRequest = NOW(), 
							LastRequestBy = \'' . $_SESSION['UserID'] . '\' ';
        if ($TerminalID != "") {
            $sql .= '
						where 
							TerminalID = \'' . $TerminalID . '\'';
        }
        //debug_r($sql);
        $Result['UpdateTerminalInfo'] = $this->db_db_query($sql);

        //debug_r($Result);
        return !in_array(false, $Result);
    }

    function Update_Terminal_Last_Photo_Sync($MacAddress)
    {
        $sql = 'update POS_TERMINAL_INFO set 
							LastPhotoSync = NOW() 
						where 
							MacAddress = \'' . $MacAddress . '\'';
        return $this->db_db_query($sql);
    }

    function Update_Terminal_Info($TerminalID, $DataMap)
    {
        $allow_fields = array('SiteName', 'MacAddress', 'IPAddress', 'LastConnected', 'LastPhotoSync', 'LastPhotoRequest', 'LastRequestBy', 'TemplateID', 'TerminalStatus');

        $sql = "UPDATE POS_TERMINAL_INFO SET ";
        $sep = "";
        foreach ($DataMap as $key => $val) {
            if (!in_array($key, $allow_fields)) continue;

            $sql .= $sep . "$key='" . $this->Get_Safe_Sql_Query($val) . "'";
            $sep = ",";
        }
        $sql .= " ,ModifyDate=NOW(),ModifyBy='" . $_SESSION['UserID'] . "' WHERE TerminalID='" . $TerminalID . "'";
        $success = $this->db_db_query($sql);
        return $success;
    }

    function Get_Health_Personal_Data($StartDate, $EndDate, $StudentID)
    {
        $TimeStart = strtotime($StartDate);
        $TimeEnd = strtotime($EndDate);

        $TotalDay = (($TimeEnd - $TimeStart) / 86400) + 1;
        $namefield = getNameFieldByLang();
        $sql = "select 
							UserID,
							" . $namefield . " as Name,
							ClassName,
							ClassNumber 
						from 
							INTRANET_USER 
						where 
							UserID in (" . implode(',', $StudentID) . ") 
						order by 
							ClassName,ClassNumber";
        $StudentList = $this->returnArray($sql);

        $sql = "select
						  l.StudentID,
						  ihi.HealthIngredientID,
						  ROUND(SUM(ihi.IngredientInTake)/" . $TotalDay . "," . $this->DecimalPlace . ") as AvgIntake 
						from 
						  POS_TRANSACTION t
						  inner join
						  PAYMENT_OVERALL_TRANSACTION_LOG l
						  on
						    t.LogID = l.LogID
						    and
						    l.StudentID in (" . implode(',', $StudentID) . ") 
						    and
						    (t.VoidBy IS NULL or t.VoidBy = '')
						    and
						    ProcessDate between '" . $StartDate . "' and '" . $EndDate . " 23:59:59'
						  inner join
						  PAYMENT_PURCHASE_DETAIL_RECORD dr
						  on
						    t.LogID = dr.TransactionLogID
						  inner join
						  POS_ITEM_HEALTH_INGREDIENT ihi
						  on dr.ItemID = ihi.ItemID 
						group by
						  l.StudentID, ihi.HealthIngredientID";
        //debug_r($sql);
        $AvgStat = $this->returnArray($sql);
        for ($i = 0; $i < sizeof($AvgStat); $i++) {
            list($StudentID, $HealthIngredientID, $AvgIntake) = $AvgStat[$i];

            $StudentIntakeInfo[$StudentID][$HealthIngredientID] = $AvgIntake;
        }

        return array($StudentList, $StudentIntakeInfo);
    }

    function Pickup_Item($LogID, $ItemQtyArray = '')
    {
        $success = array();
        if ($LogID > 0 && !$ItemQtyArray) {
            $sql = "UPDATE POS_TRANSACTION as pt
					inner join 
				PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
				on pt.LogID = ppdr.TransactionLogID 
				left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
						on potl2.LogID=pt.VoidLogID AND (pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL)
				LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
				SET ppdr.ReceiveAmount = IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount),
					ReceiveAmountDateModify=NOW(),ReceiveAmountModifyBy='" . $_SESSION['UserID'] . "'
				WHERE ppdr.TransactionLogID = '" . $LogID . "'";
//				debug_pr($sql);
            $success[] = $this->db_db_query($sql);
        } else if ($LogID > 0 && $ItemQtyArray) {
            foreach ($ItemQtyArray as $ItemID => $ItemQty) {
                $sql = "UPDATE PAYMENT_PURCHASE_DETAIL_RECORD
				SET ReceiveAmount = ReceiveAmount + " . $ItemQty . ",
					ReceiveAmountDateModify=NOW(),ReceiveAmountModifyBy='" . $_SESSION['UserID'] . "'
				WHERE TransactionLogID = '" . $LogID . "' AND ItemID = '" . $ItemID . "'";
                $success[] = $this->db_db_query($sql);
            }
        }


        return !in_array(false, $success);
    }

    function Get_Log_Item($LogID, $ItemIDs = '')
    {
        global $intranet_session_language;

        $namefield = getNameFieldWithClassNumberByLang("u.");
        if ($intranet_session_language == "b5" || $intranet_session_language == "gb") {
            #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
            $archive_namefield = "au.ChineseName";
        } else
            $archive_namefield = "au.EnglishName";
        # $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

        //$voidConds = "AND IF(ppdr.ReceiveAmount,ppdr.ReceiveAmount,0) < ppdr.ItemQty";
        //$amountConds = "AND pil.Amount < ppdr.ItemQty";
        $voidConds .= " AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL";
        
        //ppdr.ReceiveAmount as ReceiveAmount
        $sql = "SELECT
					pi.ItemID as ItemID,
					pi.ItemName as ItemName,
					ROUND(ppdr.ItemSubTotal," . $this->DecimalPlace . ") as ItemSubTotal,
					IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount) as ItemQty,
					IF(ppdr.ReceiveAmount,ppdr.ReceiveAmount,0) as ReceivedItemQty,
					IF(ppdr.VoidAmount,ppdr.VoidAmount,0) as VoidAmountQty
				from 
					POS_ITEM as pi 
					inner join POS_ITEM_CATEGORY as c on c.CategoryID=pi.CategoryID 
					inner join 
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt
					on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber  
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pt.LogID = potl.LogID AND pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
					on potl2.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
					LEFT JOIN POS_ITEM_LOG as pil ON pi.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
					LEFT JOIN
					INTRANET_USER as u
					on potl.StudentID = u.UserID OR potl2.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID OR potl2.StudentID = au.UserID
					$replaceItemLogConds
				WHERE 
					pt.LogID='$LogID' AND pi.ItemID IN (" . $ItemIDs . ")
                    AND (pil.ItemLogID IS NULL OR (pil.ItemLogID IS NOT NULL $amountConds))
                    AND ppdr.ItemQty>0
                    $voidConds";
        //debug_pr($sql);
        return $this->returnArray($sql);
    }

    function Void_POS_Transaction_By_Item($TransactionLogID, $Remark, $ItemID, $ItemQty, $ActionFrom='NotTake')
    {
        $this->Start_Trans_For_Lock_Table_Procedure();

        $sql = 'LOCK TABLES 
							PAYMENT_OVERALL_TRANSACTION_LOG WRITE,
							PAYMENT_ACCOUNT WRITE,
							POS_TRANSACTION WRITE,
							PAYMENT_PURCHASE_DETAIL_RECORD WRITE,
							POS_ITEM WRITE,
							POS_ITEM_LOG WRITE
							POS_VOID_LOG WRITE';
        $this->db_db_query($sql);

        if ($ItemID > 0) { // void transaction
            $sql = 'select 
								StudentID,
								Amount,
								RefCode 
							from 
								PAYMENT_OVERALL_TRANSACTION_LOG 
							where 
								LogID = \'' . $TransactionLogID . '\'';
            $Temp = $this->returnArray($sql);
            $StudentID = $Temp[0]['StudentID'];
            $OriginalAmount = $Temp[0]['Amount'];
            $RefCode = $Temp[0]['RefCode'];

            $refundAmount = 0.0;

//             $sql = 'select 
// 						ItemID, 
// 						ItemQty - VoidAmount as ItemQty,
// 						ItemSubTotal 
// 					from 
// 						PAYMENT_PURCHASE_DETAIL_RECORD 
// 					where 
// 						TransactionLogID = \'' . $TransactionLogID . '\' AND ItemID = \'' . $ItemID . '\' AND ItemQty - VoidAmount > 0';
            $sql = 'select
						ItemID,						
						ItemSubTotal
					from
						PAYMENT_PURCHASE_DETAIL_RECORD
					where
						TransactionLogID = \'' . $TransactionLogID . '\' AND ItemID = \'' . $ItemID . '\' AND ItemQty - VoidAmount > 0';
            
            $ItemDetail = $this->returnArray($sql);
            for ($i = 0; $i < count($ItemDetail); $i++) {
                if (isset($ItemQty) && $ItemQty > 0) {
                    $refundAmount += $ItemQty * $ItemDetail[$i]['ItemSubTotal'];
                }
            }

            if ($refundAmount > 0.0) {
                $sql = 'select Balance from PAYMENT_ACCOUNT where StudentId = \'' . $StudentID . '\'';
                $Temp = $this->returnVector($sql);
                $AccountBalance = $Temp[0];

                $sql = 'update PAYMENT_ACCOUNT set 
									Balance = Balance + ' . $refundAmount . ' 
								where 
									StudentID = \'' . $StudentID . '\'';
                $Result['UpdateAccountBalance'] = $this->db_db_query($sql);

                $sql = 'insert into PAYMENT_OVERALL_TRANSACTION_LOG (
									StudentID,
									TransactionType,
									Amount,
									BalanceAfter, 
									TransactionTime,
									Details,
									RefCode
									)
								values (
									\'' . $StudentID . '\',
									\'11\',
									\'' . $refundAmount . '\',
									\'' . ($AccountBalance + $refundAmount) . '\',
									NOW(),
									\'Transaction Void Refund\',
									\'' . $RefCode . '\'
									)';
                $Result['InsertRefundTransactionLog'] = $this->db_db_query($sql);
                $VoidTransactionLog = $this->db_insert_id();

                $sql = 'insert into POS_VOID_LOG (
									TransactionLogID,
									VoidBy,
									VoidDate,
									VoidLogID, 
									Remark
									)
								values (
									\'' . $TransactionLogID . '\',
									\'' . $_SESSION['UserID'] . '\',
									NOW(),
									\'' . $VoidTransactionLog . '\',
									\'' . $this->Get_Safe_Sql_Query($Remark) . '\'
									)';
                $Result['InsertVoidLog'] = $this->db_db_query($sql);

//				$sql = 'update PAYMENT_PURCHASE_DETAIL_RECORD set
//							ReceiveAmount = ReceiveAmount - '.$ItemQty.',
//							ReceiveAmountDateModify=NOW(),
//							ReceiveAmountModifyBy=\''.$_SESSION['UserID'].'\'
//						where
//							TransactionLogID = \''.$TransactionLogID.'\' AND ItemID = \''.$ItemID.'\'';
//				$Result['UpdateReceiveAmount'] = $this->db_db_query($sql);

                for ($i = 0; $i < sizeof($ItemDetail); $i++) {
                    $sql = 'select 
										ItemCount 
									from 
										POS_ITEM 
									where 
										ItemID = \'' . $ItemDetail[$i]['ItemID'] . '\'';
                    $Temp = $this->returnVector($sql);
                    $OriginalItemCount = $Temp[0];

//                     $sql = 'update POS_ITEM set 
// 										ItemCount = ItemCount + ' . $ItemQty . ',
// 										ItemSalesTotal = ItemSalesTotal - ' . $ItemDetail[$i]['ItemQty'] . ' 
// 									where 
// 										ItemID = \'' . $ItemDetail[$i]['ItemID'] . '\'';
                    $sql = 'update POS_ITEM set
										ItemCount = ItemCount + ' . $ItemQty . ',
										ItemSalesTotal = ItemSalesTotal - ' . $ItemQty. '
									where
										ItemID = \'' . $ItemDetail[$i]['ItemID'] . '\'';
                    
                    //debug_r($sql);
                    $Result['ResetItemCount:' . $ItemDetail[$i]['ItemID']] = $this->db_db_query($sql);

                    if ($ItemQty > 0) {
                        $sql = 'insert into POS_ITEM_LOG (
											ItemID,
											LogType,
											RefLogID,
											Amount,
											CountAfter,
											DateInput,
											InputBy 
											)
										values (
											\'' . $ItemDetail[$i]['ItemID'] . '\',
											\'2\',
											\'' . $VoidTransactionLog . '\',
											\'' . $ItemQty . '\',
											\'' . ($OriginalItemCount + $ItemQty) . '\',
											NOW(),
											\'' . $_SESSION['UserID'] . '\'
											)';
                        //debug_r($sql);
                        $Result['InsertItemChangeLog:' . $ItemDetail[$i]['ItemID']] = $this->db_db_query($sql);

                        //update the void amount [Henry Added 20190227]
                        $sql = 'update PAYMENT_PURCHASE_DETAIL_RECORD set 
										VoidAmount = VoidAmount + ' . $ItemQty . ',
										VoidAmountDateModify = NOW(),
										VoidAmountModifyBy = \'' . $_SESSION['UserID'] . '\'';
                        // void item after pickup
                        if ($ActionFrom == 'Take') {
                            $sql .= ',ReceiveAmount = ReceiveAmount - ' . $ItemQty . ',
                                    ReceiveAmountDateModify = NOW(), 
                                    ReceiveAmountModifyBy = \'' . $_SESSION['UserID'] . '\'';
                        }
                        $sql .= ' where 
										ItemID = \'' . $ItemDetail[$i]['ItemID'] . '\' AND TransactionLogID = \'' . $TransactionLogID . '\'';
                        //debug_r($sql);
                        $Result['UpdateVoidAmount:' . $ItemDetail[$i]['ItemID']] = $this->db_db_query($sql);
                    }
                }
            } else {
                $Result['RefundAmount'] = false;
            }
        } else {
            $sql = 'update POS_TRANSACTION set 
								Remark = \'' . $this->Get_Safe_Sql_Query($Remark) . '\'
							where 
								LogID = \'' . $TransactionLogID . '\'';
            $Result['UpdateVoidRemarks'] = $this->db_db_query($sql);
        }

        if (!in_array(false, $Result))
            $this->Commit_Trans();
        else
            $this->RollBack_Trans();
        //$this->RollBack_Trans();

        $sql = 'UNLOCK TABLES';
        $this->db_db_query($sql);

        //debug_r($Result);
        return !in_array(false, $Result);
    }

    function Change_POS_Transaction_By_Item($TransactionLogID, $ItemChangeID, $ItemID, $ItemQty)
    {
        $sql = 'LOCK TABLES 
							POS_ITEM WRITE,
							POS_ITEM_LOG WRITE,
							PAYMENT_PURCHASE_DETAIL_RECORD WRITE';
        $this->db_db_query($sql);

        if ($ItemID > 0) { // void transaction
            $sql = "select Amount from POS_ITEM_LOG where RefLogID = '" . $TransactionLogID . "' AND ItemID = '" . $ItemID . "' ORDER BY ItemLogID DESC";
            $Temp = $this->returnVector($sql);
            $Amount = $Temp[0];
            if ($Amount >= $ItemQty) {
                $sql = 'update POS_ITEM set 
	  							ItemCount = ItemCount - ' . $ItemQty . ',
	  							ItemSalesTotal = ItemSalesTotal + ' . $ItemQty . ' 
	  						where 
	  							ItemID = \'' . $ItemChangeID . '\'';
                $Result['UpdateItemDetail:' . $ItemChangeID] = $this->db_db_query($sql);

                $sql = 'update POS_ITEM set 
	  							ItemCount = ItemCount + ' . $ItemQty . ',
	  							ItemSalesTotal = ItemSalesTotal - ' . $ItemQty . ' 
	  						where 
	  							ItemID = \'' . $ItemID . '\'';
                $Result['UpdateItemDetail:' . $ItemID] = $this->db_db_query($sql);

                $sql = 'select 
						ItemName,
	  					ItemCount as remain 
	  				from 
	  					POS_ITEM 
	  				where 
	  					ItemID = \'' . $ItemChangeID . '\'';
                $Temp = $this->returnResultSet($sql);

                $ItemChangeRemain = $Temp[0]['remain'];

                $sql = 'select 
						ItemName,
	  					ItemCount as remain 
	  				from 
	  					POS_ITEM 
	  				where 
	  					ItemID = \'' . $ItemID . '\'';
                $Temp = $this->returnResultSet($sql);

                $ItemRemain = $Temp[0]['remain'];

//	  			if($Amount > $ItemQty){
                $sql = 'select 
								InvoiceNumber, ItemSubTotal, ItemQty, ReceiveAmount
							from 
								PAYMENT_PURCHASE_DETAIL_RECORD 
							where 
								TransactionLogID = \'' . $TransactionLogID . '\' AND ItemID = \'' . $ItemID . '\'';
                $ItemDetail = current($this->returnArray($sql));

                $NumberOfReceiveAmountForChangeItem = $ItemQty > $ItemDetail['ReceiveAmount'] ? $ItemDetail['ReceiveAmount'] : $ItemQty;

                $sql = "SELECT 
                                RecordID 
                        FROM 
                                PAYMENT_PURCHASE_DETAIL_RECORD 
                        WHERE 
                                TransactionLogID='" . $TransactionLogID . "'
                                AND ItemID='" . $ItemChangeID . "'";
                $changeToItemAry = $this->returnResultSet($sql);
                if (count($changeToItemAry) > 0) {       // item record exist, update 
                    $recordID = $changeToItemAry[0]['RecordID'];
                    $sql = "UPDATE 
                                    PAYMENT_PURCHASE_DETAIL_RECORD 
                            SET 
                                    ItemQty = ItemQty + ".(intval($ItemQty)).",
                                    ReceiveAmount = ReceiveAmount + ".(intval($NumberOfReceiveAmountForChangeItem)).",
                                    ReceiveAmountDateModify = NOW(),
                                    ReceiveAmountModifyBy = '" . $_SESSION['UserID'] . "'
                            WHERE
                                     RecordID='".$recordID."'";
                    $Result['UpdatePurchaseDetailRecord'] = $this->db_db_query($sql);
                }
                else {
                    $sql = "INSERT IGNORE INTO PAYMENT_PURCHASE_DETAIL_RECORD (
    								TransactionLogID, 
    								ItemID, 
    								ItemQty, 
    								ItemSubTotal, 
    								DateInput, 
    								DateModified, 
    								InvoiceNumber,
    								ReceiveAmount,
    								ReceiveAmountDateModify,
    								ReceiveAmountModifyBy)
    							VALUES (
    								'" . $TransactionLogID . "',
    								'" . $ItemChangeID . "',
    								'" . $ItemQty . "',
    								'" . $ItemDetail['ItemSubTotal'] . "',
    								now(),
    								now(),
    								'" . $ItemDetail['InvoiceNumber'] . "',
    								'" . $NumberOfReceiveAmountForChangeItem . "',
    								NOW(),
    								'" . $_SESSION['UserID'] . "'
    							)";
                    $Result['InsertPurchaseDetailRecord'] = $this->db_db_query($sql);
                }
                
                $sql = 'update PAYMENT_PURCHASE_DETAIL_RECORD set 
						ItemQty = \'' . ($Amount - $ItemQty) . '\',
						ReceiveAmount = ReceiveAmount - ' . $NumberOfReceiveAmountForChangeItem . ',
						ReceiveAmountDateModify = NOW(),
						ReceiveAmountModifyBy = \'' . $_SESSION['UserID'] . '\',
						DateModified = NOW()
					where 
						TransactionLogID = \'' . $TransactionLogID . '\' AND ItemID = \'' . $ItemID . '\'';
                $Result['UpdatePurchaseDetailRecord'] = $this->db_db_query($sql);
//	  			}
//	  			else{
//  					$sql = 'update PAYMENT_PURCHASE_DETAIL_RECORD set
//						ItemID = \''.$ItemChangeID.'\',
//						DateModified = NOW()
//					where
//						TransactionLogID = \''.$TransactionLogID.'\' AND ItemID = \''.$ItemID.'\'';
//  					$Result['UpdatePurchaseDetailRecord'] = $this->db_db_query($sql);
//	  			}

                $sql = 'insert into POS_ITEM_LOG (
						ItemID,
						LogType,
						RefLogID,
						Amount,
						CountAfter,
						DateInput,
						InputBy
						)
					values (
						\'' . $ItemChangeID . '\',
						\'1\',
						\'' . $TransactionLogID . '\',
						\'' . $ItemQty . '\',
						\'' . $ItemChangeRemain . '\',
						NOW(),
						\'' . $_SESSION['UserID'] . '\'
						)';
                $Result['InsertToItemLog:' . $ItemChangeID] = $this->db_db_query($sql);
                $NewItemLog = $this->db_insert_id();

                $sql = 'select 
						ItemName,
	  					ItemCount as remain 
	  				from 
	  					POS_ITEM 
	  				where 
	  					ItemID = \'' . $ItemID . '\'';
                $Temp = $this->returnResultSet($sql);

                $ItemRemain = $Temp[0]['remain'];

                $sql = 'insert into POS_ITEM_LOG (
						ItemID,
						LogType,
						RefLogID,
						Amount,
						CountAfter,
						ReplaceItemLogID,
						DateInput,
						InputBy
						)
					values (
						\'' . $ItemID . '\',
						\'5\',
						\'' . $TransactionLogID . '\',
						\'' . $ItemQty . '\',
						\'' . $ItemRemain . '\',
						\'' . $NewItemLog . '\',
						NOW(),
						\'' . $_SESSION['UserID'] . '\'
						)';
                $Result['InsertToItemLog:' . $ItemChangeID] = $this->db_db_query($sql);
                $NewItemLog = $this->db_insert_id();

//				$sql = 'update POS_ITEM_LOG set
//  							ReplaceItemLogID = \''.$NewItemLog.'\',
//							DateModify = NOW(),
//							ModifyBy = \''.$_SESSION['UserID'].'\'
//  						where
//  							RefLogID = \''.$TransactionLogID.'\' AND ItemID = \''.$ItemID.'\'';
//  				$Result['UpdateItemDetail:'.$ItemID] = $this->db_db_query($sql);
            }
        }

        if (!in_array(false, $Result))
            $this->Commit_Trans();
        else
            $this->RollBack_Trans();
        //$this->RollBack_Trans();

        $sql = 'UNLOCK TABLES';
        $this->db_db_query($sql);

        //debug_r($Result);
        return !in_array(false, $Result);
    }

    function Get_Pickup_Export_Data($FromDate = '', $ToDate = '', $ClassName = '', $keyword = '', $ExportOrderListOrder = '')
    {
        global $Lang, $intranet_session_language;

        $sql = $this->Get_Pickup_Export_Table_Sql($FromDate, $ToDate, $ClassName, $keyword, $ExportOrderListOrder);
        //echo $sql; die;
        $Result = $this->returnArray($sql);
        return $Result;
    }

    function Get_Pickup_Export_Table_Sql($FromDate = '', $ToDate = '', $ClassName = '', $keyword = '', $ExportOrderListOrder = '')
    {
        global $intranet_session_language;

        $date_cond = " pt.ProcessDate between '$FromDate' and '$ToDate 23:59:59' ";

        if (trim($ClassName) != "")
            $user_cond .= " AND (u.ClassName like '%" . $ClassName . "%') ";

        $namefield = getNameFieldWithClassNumberByLang("u.");
        if ($intranet_session_language == "b5" || $intranet_session_language == "gb") {
            #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
            $archive_namefield = "au.ChineseName";
        } else
            $archive_namefield = "au.EnglishName";
        # $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

        $amountConds = "AND IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) < ppdr.ItemQty
						AND IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount) > ppdr.ReceiveAmount";
        $voidConds = "AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL";

        if ($ExportOrderListOrder == 1) {
            $orderBy = 'Order By ppdr.InvoiceNumber, ClassName, ClassNumber';
        } else if ($ExportOrderListOrder == 2) {
            $orderBy = 'Order By ClassName, ClassNumber, ppdr.InvoiceNumber';
        }

        $sql = "SELECT
					ppdr.InvoiceNumber,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
					pi.ItemName,
					IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount) as ItemQty,
					ROUND(ppdr.ItemSubTotal," . $this->DecimalPlace . ") as ItemSubTotal,
					ROUND(ppdr.ItemSubTotal*IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)," . $this->DecimalPlace . ") as GrandTotal,
					IF(potl.LogID IS NOT NULL, potl.REFCode, potl2.REFCode) as RefCode,
					ppdr.DateInput,
					c.CategoryName,
					IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount) - ppdr.ReceiveAmount as NotTakenAmount,
					pt.LogID as LogID
				from 
					POS_ITEM as pi 
					inner join POS_ITEM_CATEGORY as c on c.CategoryID=pi.CategoryID 
					inner join 
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt
					on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber and $date_cond 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pt.LogID = potl.LogID AND pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
					on potl2.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
					LEFT JOIN
					INTRANET_USER as u
					on potl.StudentID = u.UserID OR potl2.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID OR potl2.StudentID = au.UserID
				WHERE 
					(
						u.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						u.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						u.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						u.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au.EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au.ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au.ClassName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR
						au.ClassNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
						potl.RefCode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
						potl2.RefCode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%' OR 
						ppdr.InvoiceNumber LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'  OR
                        pi.Barcode LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
					)
					$user_cond 
                    $amountConds
                    $voidConds
					$orderBy";
        //echo $sql; die;

        return $sql;
    }


    ###############################################################################################
    ## function start for eclass App and web
    function checkAppAccessRight()
    {
        global $Lang, $plugin, $indexVar;

        $leClassApp = $indexVar['leClassApp'];
        $ret = true;

        if ($_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] && !$_SESSION['SSV_PRIVILEGE']['sys_custom']['ePOS']['exclude_webview'] && $_SESSION['SSV_PRIVILEGE']['sys_custom']['ePOS']['enablePurchaseAndPickup']) {
            if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) {
                // pass
            } else {
                if ($_SESSION['UserType'] == USERTYPE_PARENT) {
                    if ($plugin['eClassApp'] && $leClassApp->isSchoolInLicense('P')) {
                        // pass
                    } else {
                        $ret = false;
                    }
                } elseif ($_SESSION['UserType'] == USERTYPE_STUDENT) {
                    if ($plugin['eClassStudentApp'] && $leClassApp->isEnabledStudentApp() && $leClassApp->isSchoolInLicense('P')) {
                        // pass
                    } else {
                        $ret = false;
                    }
                } elseif ($_SESSION['UserType'] == USERTYPE_STAFF) {
                    if ($plugin['eClassTeacherApp'] && $leClassApp->isSchoolInLicense('T')) {
                        // pass
                    } else {
                        $ret = false;
                    }
                } else {
                    $ret = false;
                }
            }
        } else {
            $ret = false;
        }
        if (!$ret) {
            No_Access_Right_Pop_Up();
        }
        return $ret;
    }

    function getChildren($parentID)
    {
        $studentName = getNameFieldByLang2('u.');
        $className = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
        if ($parentID) {
            $sql = "SELECT      
                            u.UserID,
                            CONCAT(" . $studentName . ", ' ('," . $className . ",'-',ycu.ClassNumber,')') AS StudentName
                    FROM
                            INTRANET_USER u
                            INNER JOIN YEAR_CLASS_USER ycu on ycu.UserID=u.UserID
                            INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
                            INNER JOIN INTRANET_PARENTRELATION r ON r.StudentID=u.UserID
                    WHERE   
                            r.ParentID='" . $parentID . "'
                            AND yc.AcademicYearID='" . Get_Current_Academic_Year_ID() . "'                    
                            AND u.RecordStatus<>'3'
                            AND u.RecordType='" . USERTYPE_STUDENT . "'
                            ORDER BY yc.ClassTitleEN, ycu.ClassNumber";
            $retAry = $this->returnArray($sql);     // use returnArray for building children selection
            return $retAry;
        } else {
            return false;
        }
    }

    function isChild($parentID, $childID)
    {
        if ($parentID && $childID) {
            $sql = "SELECT      s.UserID
                    FROM
                                INTRANET_USER s
                    INNER JOIN
                                INTRANET_PARENTRELATION r ON r.StudentID=s.UserID
                    WHERE       r.ParentID='" . $parentID . "'
                    AND         r.StudentID='" . $childID . "'
                    AND         s.RecordStatus<>'3'
                    AND         s.RecordType='" . USERTYPE_STUDENT . "'";
            $retAry = $this->returnResultSet($sql);
            return count($retAry) == 1 ? true : false;
        } else {
            return false;
        }
    }

    // show child name
    function getUserToDisplay($userID)
    {
        $dislayName = '';
        $name = getNameFieldByLang();
        $sql = "SELECT " . $name . " AS Name FROM INTRANET_USER WHERE UserID='" . $userID . "'";
        $rs = $this->returnResultSet($sql);
        if (count($rs)) {
            $displayName = $rs[0]['Name'];
        } else {
            $sql = "SELECT " . $name . " AS Name FROM INTRANET_ARCHIVE_USER WHERE UserID='" . $userID . "'";
            $rs = $this->returnResultSet($sql);
            if (count($rs)) {
                $displayName = $rs[0]['Name'] . "<span color=red>*</span>";
            }
        }
        return $displayName;
    }

    // return unpickup item array by student
    function getUnpickupItemByStudent($studentID)
    {
        $returnAry = array();
        $sql = "SELECT
                    pt.LogID,
                    ppdr.ItemID
                FROM 
                    PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
                    INNER JOIN POS_TRANSACTION as pt ON pt.LogID = ppdr.TransactionLogID AND ppdr.InvoiceNumber = pt.InvoiceNumber 
                WHERE 
                    pt.ProcessBy='" . $studentID . "'
                    AND pt.VoidLogID IS NULL
                    AND ppdr.ItemQty-ppdr.VoidAmount-ppdr.ReceiveAmount>0
                    ORDER BY pt.LogID, ppdr.ItemID";
        $returnAry = $this->returnResultSet($sql);
        return $returnAry;
    }

    function getUnpickupDetailByStudent($studentID)
    {
        $returnAssoc = array();
        $unpickupItemAry = $this->getUnpickupItemByStudent($studentID);
        $unpickupLogIDAry = array_unique(Get_Array_By_Key((array)$unpickupItemAry, 'LogID'));
        $unpickupItemIDAssoc = BuildMultiKeyAssoc((array)$unpickupItemAry, array('LogID'), array('ItemID'), 1);

        if (count($unpickupLogIDAry)) {
            $sql = "SELECT
                        pt.LogID,
                        ppdr.InvoiceNumber,
                        SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)) as ItemQty,
                        SUM(ROUND(ppdr.ItemSubTotal*IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)," . $this->DecimalPlace . ")) as GrandTotal,
                        DATE_FORMAT(ppdr.DateInput,\"%Y-%m-%d\") as DateInput
                    FROM 
                        PAYMENT_PURCHASE_DETAIL_RECORD as ppdr 
                        INNER JOIN POS_TRANSACTION as pt ON pt.LogID = ppdr.TransactionLogID AND ppdr.InvoiceNumber = pt.InvoiceNumber  
                        LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl ON potl.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL AND potl.StudentID ='" . $studentID . "'
                        LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl.LogID 
                    WHERE 
                        pt.ProcessBy='" . $studentID . "'
                        AND pt.LogID IN ('" . implode("','", $unpickupLogIDAry) . "')
                        AND IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) < ppdr.ItemQty
                        AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL 
                    GROUP BY 
                        pt.LogID
                    ORDER BY 
                        ppdr.DateInput DESC";
            $rsAry = $this->returnResultSet($sql);
            $returnAssoc = BuildMultiKeyAssoc($rsAry, array('LogID'));
            foreach ((array)$returnAssoc as $_logID => $_rsAry) {
                $returnAssoc[$_logID]['ItemID'] = $unpickupItemIDAssoc[$_logID];        // get first unpicked item for displaying item image
            }
        }
        return $returnAssoc;
    }

    function getFirstPickedItemIDByLogID($logID)
    {
        if (!is_array($logID)) {
            $logID = array($logID);
        }
        $sql = "SELECT  a.LogID, 
                        b.RecordID,
                        b.ItemID
                FROM (
                    SELECT 
                        ppdr.TransactionLogID AS LogID, 
                        MIN(ppdr.RecordID) AS MinRecordID
                    FROM
                        PAYMENT_PURCHASE_DETAIL_RECORD ppdr
                    WHERE
                        ppdr.TransactionLogID IN ('" . implode("','", (array)$logID) . "')
                        AND ppdr.ItemQty-ppdr.VoidAmount-ppdr.ReceiveAmount=0
                        AND ppdr.ReceiveAmount>0
                        GROUP BY ppdr.TransactionLogID
                    ) AS a
                INNER JOIN (
                    SELECT 
                        ppdr.RecordID,
                        ppdr.ItemID
                    FROM
                        PAYMENT_PURCHASE_DETAIL_RECORD ppdr
                    WHERE
                        ppdr.TransactionLogID IN ('" . implode("','", (array)$logID) . "')
                        AND ppdr.ItemQty-ppdr.VoidAmount-ppdr.ReceiveAmount=0
                        AND ppdr.ReceiveAmount>0
                    ) AS b ON b.RecordID=a.MinRecordID
                ORDER BY a.LogID";
        $rsAry = $this->returnResultSet($sql);
        $returnAssoc = BuildMultiKeyAssoc($rsAry, array('LogID'), array('ItemID'), 1);
        return $returnAssoc;
    }

    function getPickupDetailByStudent($studentID)
    {
        $returnAssoc = array();
        $sql = "SELECT
                    pt.LogID
                FROM 
                    PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
                    INNER JOIN POS_TRANSACTION as pt ON pt.LogID = ppdr.TransactionLogID AND ppdr.InvoiceNumber = pt.InvoiceNumber
                    LEFT JOIN (
                        SELECT
                            pt.LogID
                        FROM 
                            PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
                            INNER JOIN POS_TRANSACTION as pt ON pt.LogID = ppdr.TransactionLogID AND ppdr.InvoiceNumber = pt.InvoiceNumber 
                        WHERE 
                            pt.ProcessBy='" . $studentID . "'
                            AND pt.VoidLogID IS NULL
                            AND ppdr.ItemQty-ppdr.VoidAmount-ppdr.ReceiveAmount>0
                            GROUP BY pt.LogID                    
                    ) as unpick ON unpick.LogID=pt.LogID 
                WHERE 
                    pt.ProcessBy='" . $studentID . "'
                    AND pt.VoidLogID IS NULL
                    AND ppdr.ReceiveAmount>=ppdr.ItemQty-ppdr.VoidAmount
                    AND unpick.LogID IS NULL
                    GROUP BY pt.LogID
                    ORDER BY pt.LogID";
        $rs = $this->returnResultSet($sql);
        
        $pickupLogIDAry = Get_Array_By_Key($rs, 'LogID');
        $pickedItemIDAssoc = $this->getFirstPickedItemIDByLogID($pickupLogIDAry);

        $sql = "SELECT
                    pt.LogID,
                    ppdr.InvoiceNumber,
                    SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)) as ItemQty,
                    SUM(ROUND(ppdr.ItemSubTotal*IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)," . $this->DecimalPlace . ")) as GrandTotal,
                    DATE_FORMAT(ppdr.DateInput,\"%Y-%m-%d\") as DateInput
                FROM 
                    PAYMENT_PURCHASE_DETAIL_RECORD as ppdr 
                    INNER JOIN POS_TRANSACTION as pt ON pt.LogID = ppdr.TransactionLogID AND ppdr.InvoiceNumber = pt.InvoiceNumber  
                    LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl ON potl.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL AND potl.StudentID ='" . $studentID . "'
                    LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl.LogID 
                WHERE 
                    pt.ProcessBy='" . $studentID . "'
                    AND pt.LogID IN ('" . implode("','", (array)$pickupLogIDAry) . "')
                    AND ppdr.ReceiveAmount>=ppdr.ItemQty-ppdr.VoidAmount
                    AND IF(pil.ItemLogID IS NULL, ppdr.VoidAmount, pil.Amount) <= ppdr.ItemQty
                    AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL
                GROUP BY 
                    pt.LogID
                    HAVING SUM(ppdr.ReceiveAmount) >= SUM(IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount))
                ORDER BY 
                    ppdr.DateInput DESC";
        $rsAry = $this->returnResultSet($sql);
//debug_pr($sql);        
        $returnAssoc = BuildMultiKeyAssoc($rsAry, array('LogID'));
        foreach ((array)$returnAssoc as $_logID => $_rsAry) {
            $returnAssoc[$_logID]['ItemID'] = $pickedItemIDAssoc[$_logID];
        }
        return $returnAssoc;
    }

    function getPendingCart($studentID)
    {
        $sql = "SELECT 
                    i.ItemID,
                    i.ItemName,
                    ROUND(i.UnitPrice," . $this->DecimalPlace . ") AS UnitPrice,
                    i.ItemCount AS RemainQty,
                    c.PurchaseQty,
                    c.Sequence
                FROM
                    POS_ITEM i
                INNER JOIN
                    POS_PENDING_CART c ON c.ItemID=i.ItemID
                WHERE c.StudentID='" . $studentID . "'
                    ORDER BY c.Sequence";
        $returnAry = $this->returnResultSet($sql);
        return $returnAry;
    }

    // total qty and total amount of a user
    function getPendingCartSum($studentID)
    {
        $sql = "SELECT 
                    SUM(c.PurchaseQty) AS TotalQty,
                    ROUND(SUM(i.UnitPrice * c.PurchaseQty)," . $this->DecimalPlace . ") AS TotalPrice
                FROM
                    POS_ITEM i
                INNER JOIN
                    POS_PENDING_CART c ON c.ItemID=i.ItemID
                WHERE c.StudentID='" . $studentID . "'";
        $returnAry = $this->returnResultSet($sql);
        if (count($returnAry)) {
            $returnAry = current($returnAry);
        }
        return $returnAry;
    }

    function isSetAlipay()
    {
        global $sys_custom;
        return $sys_custom['ePayment']['Alipay'] ? true : false;
    }

    function addPendingCart($studentID, $itemIDAry, $qtyAry)
    {
        $result = false;
        if (count($itemIDAry)) {
            $valueAry = array();
            for ($i = 0, $iMax = count($itemIDAry); $i < $iMax; $i++) {
                $valueAry[] = "('" . $studentID . "','" . $itemIDAry[$i] . "','" . $qtyAry[$i] . "','" . ($i + 1) . "',NOW(),'" . $_SESSION['UserID'] . "')";
            }
            $sql = "INSERT INTO POS_PENDING_CART (
                        StudentID, 
                        ItemID, 
                        PurchaseQty, 
                        Sequence, 
                        DateModified,
                        ModifiedBy )
                    VALUES ";
            $sql .= implode(",", $valueAry);
            $result = $this->db_db_query($sql);
        }
        return $result;
    }

    function isItemInPendingCart($studentID, $itemID)
    {
        $sql = "SELECT ItemID FROM POS_PENDING_CART WHERE StudentID='" . $studentID . "' AND ItemID='" . $itemID . "'";
        $result = $this->returnResultSet($sql);
        return count($result) ? true : false;
    }

    function deletePendingCart($studentID, $itemID = '')
    {
        $sql = "DELETE FROM POS_PENDING_CART WHERE StudentID='" . $studentID . "'";

        if ($itemID) {
            $sql .= " AND ItemID='" . $itemID . "'";
        }

        $result = $this->db_db_query($sql);
        return $result;
    }

    function updatePendingCart($studentID, $itemID, $qty)
    {
        $sql = "UPDATE POS_PENDING_CART SET PurchaseQty='" . $qty . "' WHERE StudentID='" . $studentID . "' AND ItemID='" . $itemID . "'";
        $result = $this->db_db_query($sql);
        return $result;
    }

    function getUserBalance($studentID)
    {
        $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID='" . $studentID . "'";
        $returnAry = $this->returnResultSet($sql);
        return count($returnAry) ? $returnAry[0]['Balance'] : 0;
    }

    function getLastReceiveTime($transactionLogID, $receiveAmount=false)
    {
        $sql = "SELECT ReceiveAmountDateModify FROM PAYMENT_PURCHASE_DETAIL_RECORD WHERE TransactionLogID='" . $transactionLogID . "'";
        if ($receiveAmount) {
            $sql .= " AND ReceiveAmount>0";
        }
        $sql .= " ORDER BY ReceiveAmountDateModify DESC LIMIT 1";

        $result = $this->returnResultSet($sql);
        return $result;
    }

    function setAutocommit()
    {
        $result = mysql_query("SET autocommit = 1");
        return $result;
    }

    function getVoidItemsByTransaction($transactionLogID)
    {
        $sql = "SELECT
                    pi.ItemID,
                    pi.ItemName,
                    SUM(pil.Amount) AS ItemQty,
                    ROUND(ppdr.ItemSubTotal," . $this->DecimalPlace . ") AS UnitPrice,
                    ROUND(SUM(ppdr.ItemSubTotal*pil.Amount)," . $this->DecimalPlace . ") AS GrandTotal
                FROM
                    POS_ITEM pi
                    INNER JOIN PAYMENT_PURCHASE_DETAIL_RECORD ppdr ON ppdr.ItemID=pi.ItemID
                    INNER JOIN POS_VOID_LOG pvl ON pvl.TransactionLogID=ppdr.TransactionLogID
                    INNER JOIN POS_ITEM_LOG pil ON pil.RefLogID=pvl.VoidLogID AND pil.ItemID=ppdr.ItemID
                WHERE 
                    ppdr.TransactionLogID='" . $transactionLogID . "'
                    AND pil.LogType=2
                    GROUP BY pi.ItemID
                    ORDER BY ppdr.RecordID";
        $result = $this->returnResultSet($sql);
        return $result;
    }

    function getReplaceItemsByTransaction($transactionLogID)
    {
        $sql = "SELECT
                    pi.ItemID,
                    pi.ItemName,
                    pil.Amount AS ItemQty,
                    ROUND(ppdr.ItemSubTotal," . $this->DecimalPlace . ") AS UnitPrice,
                    ROUND(ppdr.ItemSubTotal*pil.Amount," . $this->DecimalPlace . ") AS GrandTotal,
                    pi2.ItemName AS ReplaceTo
                FROM
                    POS_ITEM pi
                    INNER JOIN PAYMENT_PURCHASE_DETAIL_RECORD ppdr ON ppdr.ItemID=pi.ItemID
                    INNER JOIN POS_ITEM_LOG pil ON pil.RefLogID=ppdr.TransactionLogID AND pil.ItemID=ppdr.ItemID
                    INNER JOIN POS_ITEM_LOG pil2 ON pil2.ItemLogID=pil.ReplaceItemLogID
                    INNER JOIN POS_ITEM pi2 ON pi2.ItemID=pil2.ItemID
                WHERE 
                    ppdr.TransactionLogID='" . $transactionLogID . "'
                    AND pil.LogType=5
                    ORDER BY pil2.ItemLogID";
        $result = $this->returnResultSet($sql);
        return $result;
    }

    // $userID can be parent, student or teacher
    function isTransactionBelongToUser($transactionLogID, $userID)
    {
        $sql = "SELECT StudentID FROM PAYMENT_OVERALL_TRANSACTION_LOG WHERE LogID='" . $transactionLogID . "'";
        $transactionLogAry = $this->returnResultSet($sql);
        if (count($transactionLogAry)) {
            $studentID = $transactionLogAry[0]['StudentID'];
        } else {
            return false;
        }

        if ($_SESSION['UserType'] == USERTYPE_PARENT) {
            return $this->isChild($userID, $studentID) ? true : false;
        } elseif ($_SESSION['UserType'] == USERTYPE_STUDENT) {
            return ($studentID == $userID) ? true : false;
        } elseif ($_SESSION['UserType'] == USERTYPE_STAFF) {
            return ($studentID == $userID) ? true : false;
        } else {
            return false;
        }
    }

    function getStudentListByClass($className)
    {
        $studentName = getNameFieldByLang2('u.');
        $sql = "SELECT 
                        u.UserID,
                        CONCAT(ycu.ClassNumber,' '," . $studentName . ") AS StudentName
                FROM
                        INTRANET_USER u
                        INNER JOIN YEAR_CLASS_USER ycu on ycu.UserID=u.UserID
                        INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
                WHERE
                        yc.AcademicYearID='" . Get_Current_Academic_Year_ID() . "'
                        AND yc.ClassTitleEN='" . $className . "'
                        ORDER BY ycu.ClassNumber";
        $result = $this->returnArray($sql);     // use returnArray for option selection
        return $result;
    }

    function getFirstStudentInPendingCart($teacherID)
    {
        $sql = "SELECT 
                    c.StudentID,
                    yc.ClassTitleEN
                FROM
                    POS_PENDING_CART c 
                    INNER JOIN INTRANET_USER u ON (u.UserID=c.StudentID AND u.RecordType='" . USERTYPE_STUDENT . "')
                    INNER JOIN YEAR_CLASS_USER ycu on ycu.UserID=u.UserID
                    INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
                WHERE
                    yc.AcademicYearID='" . Get_Current_Academic_Year_ID() . "'                    
                    AND c.ModifiedBy='" . $teacherID . "'
                    ORDER BY c.DateModified DESC LIMIT 1";
        $returnAry = $this->returnResultSet($sql);
        return $returnAry;
    }

    function getAlipayHashRef($tempTransactionID, $studentID)
    {
        $hash = sha1($tempTransactionID . '::' . $studentID . date("Y-m-d H:i:s"));
        $ref = 'AT-' . $tempTransactionID . '-' . $studentID . '-' . substr($hash, 0, 10);
        return $ref;
    }

    function cancelAlipayTransaction($studentID)
    {
        $result = true;
        if ($studentID) {
            $sql = "UPDATE POS_ALIPAY_TRANSACTION SET RecordStatus=2 WHERE StudentID='" . $studentID . "' AND RecordStatus=0";
            $result = $this->db_db_query($sql);
        }
        return $result;
    }

    function addAlipayTransaction($totalAmount, $transactonDetailAry, $studentID)
    {
        $result = array();
        $returnAry = array();
        $returnAry['TempTransactionID'] = '';
        $returnAry['OutTradeNo'] = '';

        $nrItem = count($transactonDetailAry);
        if ($nrItem) {
            $this->Start_Trans();

            $result['CancelPendingTransaction'] = $this->cancelAlipayTransaction($studentID);

            $sql = "INSERT INTO POS_ALIPAY_TRANSACTION (
                        StudentID,
                        Amount,
                        DateModified,
                        ModifiedBy) 
                    VALUES (
                        '" . $studentID . "',
                        '" . $totalAmount . "',
                        NOW(),'".$_SESSION['UserID'] . "')";
            $res = $this->db_db_query($sql);
            $result['AddTempTransaction'] = $res;

            if ($res) {
                $tempTransactionID = $this->db_insert_id();
                $outTradeNo = $this->getAlipayHashRef($tempTransactionID, $studentID);
                $sql = "UPDATE POS_ALIPAY_TRANSACTION SET OutTradeNo='".$outTradeNo."' WHERE TempTransactionID='".$tempTransactionID."'";
                $result['UpdateOutTradeNo'] = $this->db_db_query($sql);

                $valueAry = array();
                for ($i = 0; $i < $nrItem; $i++) {
                    $valueAry[] = "('" . $tempTransactionID . "','" . $transactonDetailAry[$i]['ItemID'] . "','" . $transactonDetailAry[$i]['UnitPrice'] . "','" . $transactonDetailAry[$i]['Quantity'] . "','" . $transactonDetailAry[$i]['Sequence'] . "',NOW(),'" . $_SESSION['UserID'] . "')";
                }
                $sql = "INSERT INTO POS_ALIPAY_ITEMS (
                            TempTransactionID, 
                            ItemID,
                            UnitPrice, 
                            PurchaseQty, 
                            Sequence, 
                            DateModified,
                            ModifiedBy )
                        VALUES ";
                $sql .= implode(",", $valueAry);
                $result['AddAlipayItem']  = $this->db_db_query($sql);
            }

            if (!in_array(false,$result)) {
                $this->Commit_Trans();
                $returnAry['TempTransactionID'] = $tempTransactionID;
                $returnAry['OutTradeNo'] = $outTradeNo;
            }
            else {
                $this->RollBack_Trans();
            }
        }
        return $returnAry;
    }

    function updateAlipayTransaction($outTradeNo, $recordStatus, $tradeNo='')
    {
        $updateExtra = '';
        if ($tradeNo != '') {
            $updateExtra .= "TradeNo='".$tradeNo."',";
        }
        $sql = "UPDATE POS_ALIPAY_TRANSACTION 
                    SET RecordStatus='".$recordStatus."',
                        {$updateExtra} 
                        StatusUpdatedOn=NOW() 
                WHERE OutTradeNo='".$outTradeNo."'";
        $result = $this->db_db_query($sql);
        return $result;
    }

    function updateAlipayTransactionID($tempTransactionID, $transactionID)
    {
        $sql = "UPDATE POS_ALIPAY_TRANSACTION SET TransactionID='".$transactionID."' WHERE TempTransactionID='".$tempTransactionID."'";
        $result = $this->db_db_query($sql);
        return $result;
    }


    function getAlipayTransaction($outTradeNo)
    {
        $sql = "SELECT * FROM POS_ALIPAY_TRANSACTION WHERE OutTradeNo='".$outTradeNo."'";
        $result = $this->returnResultSet($sql);
        return count($result) ? $result[0] : array();
    }

    function getAlipayTransactionItems($tempTransactionID)
    {
        $sql = "SELECT  pai.ItemID,
                        pai.UnitPrice,
                        pai.PurchaseQty,
                        pai.Sequence,
                        pi.ItemName,
                        pi.ItemCount-pai.PurchaseQty AS RemainQty
                FROM 
                        POS_ALIPAY_ITEMS pai
                        LEFT JOIN POS_ITEM pi ON pi.ItemID=pai.ItemID
                WHERE 
                        pai.TempTransactionID='".$tempTransactionID."' 
                        ORDER BY pai.Sequence";
        $result = $this->returnResultSet($sql);
        return $result;
    }

    function getClassNameByStudentID($studentID)
    {
        $sql = "SELECT 
                        yc.ClassTitleEN as ClassName
                FROM
                        YEAR_CLASS_USER ycu 
                        INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
                WHERE
                        yc.AcademicYearID='" . Get_Current_Academic_Year_ID() . "'
                        AND ycu.UserID='" . $studentID . "'";
        $result = $this->returnResultSet($sql);
        return count($result) ? $result[0]['ClassName'] : '';
    }

    function getAlipayTransactionByID($tempTransactionID)
    {
        $sql = "SELECT * FROM POS_ALIPAY_TRANSACTION WHERE TempTransactionID='".$tempTransactionID."'";
        $result = $this->returnResultSet($sql);
        return count($result) ? $result[0] : array();
    }

    // $status: 1 - success    2 - fail
    function actionAfterPaidByAlipay($tempTransactionID, $status, $trade_no)
    {
        global $Lang;

        if ($status == -1) {
            return false;
        }

        $alipayTransaction = $this->getAlipayTransactionByID($tempTransactionID);
        if (count($alipayTransaction)) {
            $out_trade_no= $alipayTransaction['OutTradeNo'];
            $result = array();
            $result['updateAlipayTransaction'] = $this->updateAlipayTransaction($out_trade_no, $status, $trade_no);

            $paymentTimeVal = $alipayTransaction['StatusUpdateOn'];
            $targetUserID = $alipayTransaction['StudentID'];
            $amount = $alipayTransaction['Amount'];

            $alipayTransactionItems = $this->getAlipayTransactionItems($tempTransactionID);

            $transactionDetailAry = array();
            for($i=0,$iMax=count($alipayTransactionItems);$i<$iMax;$i++) {
                $transactionDetailAry[$i]['ItemID'] = $alipayTransactionItems[$i]['ItemID'];
                $transactionDetailAry[$i]['Quantity'] = $alipayTransactionItems[$i]['PurchaseQty'];
                $transactionDetailAry[$i]['UnitPrice'] = $alipayTransactionItems[$i]['UnitPrice'];
            }

            $cardID = '';
            $siteName = $Lang['ePOS']['PaidByAlipay'];
            $InvoiceMethod = 0;
            $returnTransactionLogID = true;
            $isAlipay = true;
            $transactionLogID = $this->Process_POS_TRANSACTION($cardID, $amount, $siteName, $transactionDetailAry, $InvoiceMethod, $targetUserID, $returnTransactionLogID, $paymentTimeVal, $isAlipay);
            $result['posTransaction'] = $transactionLogID;

            $result['autoCommitResult'] = $this->setAutocommit();   // must reset autocommit to 1, otherwise cannot commit the sql execution

            $result['updateAlipayTransactionID'] = $this->updateAlipayTransactionID($tempTransactionID, $transactionLogID);

            // remove pending cart
            $result['removePendingCart'] = $this->deletePendingCart($targetUserID);

            return !in_array(false, $result) ? true : false;
        }
        else {
            return false;
        }

    }

    ## function end for eclass App and web
    ###############################################################################################
}
?>